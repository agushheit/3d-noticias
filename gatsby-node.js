/* eslint-disable @typescript-eslint/no-var-requires */
const path = require('path')
const { createFilePath } = require('gatsby-source-filesystem')
const { fmImagesToRelative } = require('gatsby-remark-relative-images')
const createPaginatedPages = require('gatsby-paginate')
const {
  estadoRealQuery,
  agendaCiudadanaQuery,
  elCampoQuery,
  laCiudadQuery,
  analisisYOpinionQuery,
  protagonistasQuery,
  denunciasQuery,
  lectoresQuery,
  deportesQuery,
  culturaQuery
} = require('./gatsby-queries')

exports.createPages = async ({ actions, graphql }) => {
  const { createPage } = actions
  const result = await graphql(`
    {
      ${estadoRealQuery}
      ${agendaCiudadanaQuery}
      ${elCampoQuery}
      ${laCiudadQuery}
      ${analisisYOpinionQuery}
      ${protagonistasQuery}
      ${denunciasQuery}
      ${lectoresQuery}
      ${deportesQuery}
      ${culturaQuery}
    }
  `)
  if (result.errors) {
    console.log({ result })
    result.errors.forEach((e) => console.error(e))
    throw result.errors
  }

  const paginables = [
    {
      page: 'estado-real',
      dataProperty: 'estadoReal',
      pageLength: 6,
    },
    {
      page: 'agenda-ciudadana',
      dataProperty: 'agendaCiudadana',
      pageLength: 6,
    },
    {
      page: 'el-campo',
      dataProperty: 'elCampo',
      pageLength: 6,
    },
    {
      page: 'la-ciudad',
      dataProperty: 'laCiudad',
      pageLength: 6,
    },
    {
      page: 'opinion',
      dataProperty: 'ultimoAnalisis',
      pageLength: 1,
    },
    {
      page: 'protagonistas',
      dataProperty: 'ultimaEntrevista',
      pageLength: 1,
    },
    {
      page: 'denuncias-ciudadanas',
      dataProperty: 'denunciasCiudadanas',
      pageLength: 2,
    },
    {
      page: 'nuestros-lectores',
      dataProperty: 'cartasDeLectores',
      pageLength: 2,
    },
    {
      page: 'deportes',
      dataProperty: 'deportes',
      pageLength: 6,
    },
    {
      page: 'cultura',
      dataProperty: 'cultura',
      pageLength: 1,
    },
  ]
  paginables.map(each => {
    try {
      const rawEdges = result.data[each.dataProperty].edges
      const edges = rawEdges.length === 0 ? [{ node: { mocked: true } }] : rawEdges
      edges.forEach((edge) => {
        const id = edge.node.id
        if (edge.node.frontmatter && edge.node.frontmatter.layout) {
          const layoutTemplateMap = {
            'Noticia con imagen': 'noticia',
            'Noticia con video': 'noticia-video',
            carta: 'carta',
            analisis: 'analisis',
            Cultura: 'cultura',
            'Denuncias Ciudadanas': 'noticia',
          }
          const templateFile = layoutTemplateMap[String(edge.node.frontmatter.layout)]
          if (templateFile) {
            const componentPath = `src/templates/${templateFile}.tsx`
            const pageData = {
              path: edge.node.fields.slug,
              tags: edge.node.frontmatter.tags,
              component: path.resolve(componentPath),
              context: {
                id,
              },
            }
            createPage(pageData)
          }
        }
      })
      createPaginatedPages({
        edges,
        createPage,
        pageTemplate: `src/paginated-pages/${each.page}.tsx`,
        pageLength: each.pageLength,
        pathPrefix: each.page,
      })
    } catch (error) {
      console.error(error)
    }
  })
}

exports.onCreateNode = ({ node, actions, getNode }) => {
  const { createNodeField } = actions
  fmImagesToRelative(node) // convert image paths for gatsby images

  if (node.internal.type === 'MarkdownRemark') {
    const value = createFilePath({ node, getNode })
    createNodeField({
      name: 'slug',
      node,
      value,
    })
  }
}
