---
layout: Denuncias Ciudadanas
category: Denuncias Ciudadanas
date: 2020-12-03T10:00:13Z
title: "\U0001F327 Lo que dejó la lluvia en la ciudad"
entradilla: "Las últimas lluvias, acaecidas en días recientes, dejaron en evidencia
  la falta de limpieza y mantenimiento de las bocas de tormenta. Causantes, entre
  otras, de las calles anegadas  en la ciudad de  Santa Fe.\n\n\U0001F4F7 \U0001F6A9
  Esquina entre calles Sargento Cabral y Marcial Candioti"
thumbnail: https://assets.3dnoticias.com.ar/EoP5pxpXYAEvLvR.jpg

---
