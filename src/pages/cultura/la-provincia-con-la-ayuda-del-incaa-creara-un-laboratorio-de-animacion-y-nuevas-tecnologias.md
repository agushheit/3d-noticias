---
category: Cultura
date: 2021-06-17T09:01:23-03:00
thumbnail: https://assets.3dnoticias.com.ar/cine.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: La provincia, con la ayuda del INCAA, creará un laboratorio de animación
  y nuevas tecnologías
title: La provincia, con la ayuda del INCAA, creará un laboratorio de animación y
  nuevas tecnologías
entradilla: El ministro de Cultura, Jorge Llonch, y el presidente de la institución
  federal, Luis Puenzo, acordaron la instalación del citado espacio y la puesta en
  valor de las dos escuelas de enseñanza superior de cine locales.

---
Los titulares de Cultura de la provincia y del Instituto Nacional de Cine y Artes Audiovisuales (INCAA) dialogaron en torno al desarrollo conjunto de un laboratorio de animación y nuevas tecnologías, destinado a las áreas vinculadas con los videojuegos, realidad aumentada, animación y realidad virtual, entre otras disciplinas.

El INCAA pondrá a disposición el equipamiento y los docentes, y la provincia proveerá el espacio físico que, según anticipó Llonch, “estará ubicado, seguramente, en algún lugar de la Franja del Río, en Rosario”.

Para el ministro de Cultura, “es muy importante que el laboratorio esté destinado a áreas como los videojuegos, animación, realidad virtual y realidad aumentada, porque están enmarcados en la vanguardia de las industrias creativas audiovisuales, y será un verdadero hito en la potencia de la industria audiovisual de la provincia”.

Llonch, asimismo, destacó: “Quiero remarcar la colaboración invalorable de Alejandro Puente, productor ejecutivo del organismo nacional, gestor de este encuentro y le estamos muy agradecidos, porque su intervención fue muy productiva”. 

**Escuelas de cine puestas en valor e interconectadas**

En otro orden, otro de los acuerdos alcanzados en el encuentro que protagonizaron Llonch y Puenzo, está fundado en la asistencia técnica que llevará adelante el Incaa para poner en valor las dos escuelas de enseñanza superior de cine de la provincia.

“La idea es establecer –en conjunto con el Consejo Federal de Cultura y la Asamblea Federal de la institución– una red de vinculación entre todas las escuelas de cine de la Argentina”, indicó el titular de Cultura luego de finalizada la reunión virtual con el presidente del INCAA.

Los establecimientos de enseñanza superior son la Escuela Provincial de Cine y Televisión, ubicada en Rosario y el Instituto Superior de Cine y Artes Audiovisuales, en la ciudad de Santa Fe.