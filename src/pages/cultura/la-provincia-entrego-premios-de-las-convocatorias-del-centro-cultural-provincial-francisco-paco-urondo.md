---
category: Cultura
date: 2021-07-14T08:28:11-03:00
layout: Noticia con imagen
author: Prensa Gobierno de Santa Fe
resumen: La provincia entregó premios de las convocatorias del Centro Cultural Provincial
  Francisco “Paco” Urondo
title: La provincia entregó premios de las convocatorias del Centro Cultural Provincial
  Francisco “Paco” Urondo
entradilla: El ministro Jorge Llonch otorgó diplomas a ganadores del “Todas las Artes
  al Cultural”. El acto se realizó este martes en el teatro del espacio ubicado en
  la capital provincial.
thumbnail: https://assets.3dnoticias.com.ar/cultura.jpg
link_del_video: ''

---
El ministro de Cultura, Jorge Llonch, y el secretario de Gestión Cultural, Jorge Pavarin, entregaron este martes las distinciones a las y los ganadores de “Todas las Artes al Cultural”, una propuesta integral y transversal de trabajo para el ciclo 2021 con la que el Centro Cultural Provincial Francisco “Paco” Urondo, dependiente de la cartera de Cultura, aspira a reconocer a las y los hacedores locales, representantes de las artes escénicas santafesinas.

Al respecto, Llonch señaló que “desde su inicio nos pareció un proyecto que brinda un aprovechamiento de los más genuinos para el ámbito de las artes, y que el talento sigan generando recursos y movimiento”.

Asimismo, el titular de la cartera cultural confirmó: “Vamos a trabajar para que este concurso se extienda a toda la provincia, y que el Centro Cultural Provincial Francisco “Paco” Urondo sea la plataforma. Es un desafío enorme y sería maravilloso que participen nuestras 365 localidades”.

El ciclo está conformado por cuatro convocatorias que integran diversas disciplinas y hasta la fecha se llevaron a cabo dos: los concursos de Dramaturgia y Producción de Obra.

**Convocatoria de Concurso de Dramaturgia**

Se lanzó en enero y tuvo su cierre el 28 de febrero de 2021. La convocatoria tuvo gran participación de dramaturgos y escritores de Santa Fe y la región, y se recepcionaron 26 guiones, de los cuales resultaron ganadores “La luna en el aljibe”, de Claudia Montenegro, y “Caranchos en el cuerpo”, de José Serralunga. Ambos ganadores recibieron el premio de $50.000 pesos otorgados por el Ministerio de Cultura de la Provincia.

El jurado decidió también otorgar menciones, sin orden de mérito, a las siguientes obras: “Ácida noche”, de Edgardo Dib; “Todo lo que sale de mí llega al mar”, de Alberto Ramos Mexia; “Siempre es poco”, de Norma Cabrera; “The shine y todo eso”, de Julieta Vigo; y “Las sumergidas”, de Rocío Celeste Reyna.

El jurado estuvo integrado por tres reconocidas personalidades del teatro y la dramaturgia argentina: Patricia Suárez, Darío Levin y Sandra Franzen, quienes tuvieron a su cargo seleccionar las dos obras ganadoras y destacaron el alto nivel de los trabajos recibidos.

**Convocatoria de Concurso de Producción de Obra**

En abril se llamó a la segunda convocatoria, Producción de Obra. La propuesta para esta siguiente etapa estuvo centrada en que los grupos de artistas santafesinos presentaran su proyecto de producción de cualquiera de las dos obras ganadoras para ser llevada al escenario.

El grupo seleccionado por el jurado, conformado por tres reconocidas personalidades del teatro, las artes escénicas y la dramaturgia argentina –Jorge Dubatti, Cristina López Pianello y Rodrigo Cuesta– finalmente otorgó el premio a la adaptación de “La luna en el aljibe”, de Claudia Montenegro.

La propuesta presentada por Mónica Marrafa cuenta con la dirección de Mariana Mosset, con las actuaciones de Yanina Bileisis, Mónica Marraffa, Antonela González, Karen Temperini, y tendrá la música original de Candela Fernández.

Las ganadoras recibieron el premio único de $220.000, y prevé un adicional equivalente al 20 por ciento del premio, ya que el montaje incluye música original de autores santafesinos.

**Lo que viene en “Todas las Artes al Cultural”**

Esta invitación propone abordar el diverso y maravilloso universo creativo. Desde la escritura de una pieza de dramaturgia, la producción integral de la obra y, finalmente, el encuentro con el público.

Las artes plásticas y la fotografía otorgarán otro sello que permitirá mostrar diversos reflejos y percepciones. Las dos primeras etapas ya están cumplidas y el grupo ganador de producción de obra ya se encuentra ensayando. Aún faltan dos convocatorias vinculadas a las artes plásticas y fotográficas que se darán a conocer prontamente para completar la llegada de “Todas las Artes al Cultural”.