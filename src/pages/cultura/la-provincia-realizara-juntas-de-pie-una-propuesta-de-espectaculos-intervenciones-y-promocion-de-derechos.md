---
category: Cultura
date: 2021-03-05T06:01:28-03:00
thumbnail: https://assets.3dnoticias.com.ar/juntas.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: La provincia realizará “Juntas de Pie”, una propuesta de espectáculos, intervenciones
  y promoción de derechos
title: La provincia realizará “Juntas de Pie”, una propuesta de espectáculos, intervenciones
  y promoción de derechos
entradilla: Será este domingo, en el marco del Día Internacional de las Mujeres y
  el Día de la Visibilidad Lésbica.

---
En el marco del Día Internacional de las Mujeres y el Día de la Visibilidad Lésbica, la provincia propone “Juntas de Pie”, una iniciativa cargada de actividades culturales que incluyen recitales de Ayelén Beker y Sara Hebe.

El Gobierno de Santa Fe, a través de la Secretaría de Estado de Igualdad y Género y el Ministerio de Cultura, llevará adelante un conjunto de actividades culturales este domingo en el marco del Día Internacional de las Mujeres y el Día de la Visibilidad Lésbica. Las propuestas se llevarán a cabo a partir de las 18 horas, con epicentro en las ciudades de Rosario y Santa Fe y propuestas en los 19 departamentos, alcanzando a más de 40 localidades.

En todos los casos también participarán ATR Juventudes, Boleto Educativo Gratuito, el Ministerio de Gobierno, Derechos Humanos y Diversidad con la iniciativa Santa Fe Más Cerca; los Ministerios de Salud (testeos de VIH, Programa de Cáncer, entrega de preservativos, el LIF entregará alcohol y repelente ); Producción,  de Desarrollo Social y Programa Santa Fe Más, entre otros.

En la ciudad de Rosario, las actividades tendrán lugar en el Galpón 17, Parque Nacional a la Bandera. Con la conducción de Gisela Bernardini, se presentarán Las Mostras “Payasas en Varieté Humor para todas y todos” y un espectáculo Poético musical a cargo de Sofia Pasquinelli y Florencia Croci, con el cierre de la cantante Ayelen Beker en el escenario principal.

Paralelamente, en la capital provincial, en el mismo horario las actividades se realizarán en el espacio La Redonda Arte y Vida Cotidiana, con la conducción de Belén Degrossi y Victoria Stéfano. Se presentarán La Centella, entre otras propuestas. El cierre de la jornada estará a cargo de la cantautora Sara Hebe. También habrá puestos gastronómicos y el ingreso al predio será solo hasta las 19 horas por cuestiones de protocolo.

En paralelo, en los 19 departamentos de la provincia habrá espectáculos e intervenciones, ferias de emprendedoras, talleres y demás, alcanzando a más de 40 localidades también con instalaciones de los distintos Ministerios de la Provincia.

**CUPOS LIMITADOS**

Los encuentros son con entrada libre y gratuita, respetando los protocolos de higiene y seguridad establecidos por el Ministerio de Salud. 

Para las propuestas en el Galpón 17, de Rosario, las entradas están disponibles en [https://abiertoalcielo.com.ar/detalle/JUNTAS-DE-PIE/](https://abiertoalcielo.com.ar/detalle/JUNTAS-DE-PIE/ "https://abiertoalcielo.com.ar/detalle/JUNTAS-DE-PIE/")