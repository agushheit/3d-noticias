---
category: Cultura
date: 2021-06-29T09:31:16-03:00
thumbnail: https://assets.3dnoticias.com.ar/eoa.png
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: Finalizó la segunda etapa de cursos de la Escuela de Oficios Artísticos
title: Finalizó la segunda etapa de cursos de la Escuela de Oficios Artísticos
entradilla: El ministro Jorge Llonch agradeció la participación de más de 2.700 alumnas
  y alumnos residentes en los 9 departamentos de la provincia que se capacitaron en
  Sonido, Maquillaje Artístico y Ecodiseño.

---
El Ministerio de Cultura de la provincia cerró la segunda etapa de la Escuela de Oficios Artísticos, conformada por los cursos de capacitación en Sonido, a cargo de Jorge Ojeda y Guillermo Palena; Maquillaje Artístico, llevado a cabo por Yamila Gutiérrez; y Ecodiseño con Romina Palma como capacitadora.

La actividad estuvo encabezada por el ministro de Cultura, Jorge Llonch, quien agradeció a todo el equipo que forma parte de la Escuela, como así también a los alumnos y alumnas que se anotaron, en una convocatoria que duplicó la cantidad de cupos disponibles.

En ese sentido, el ministro expresó: “La cantidad de anotados nos indica que la idea de llevar a cabo esta propuesta fue acertada, porque la transferencia de conocimientos en pandemia es una de las bases fundamentales para que todos los habitantes de esta provincia puedan desarrollarse”.

Tras agradecer el apoyo y acompañamiento de la Universidad Tecnológica Nacional (UTN) y del Consejo Federal de Inversiones (CFI), el titular de la cartera de Cultura resaltó que “la posibilidad de tener cada vez más herramientas para sumar al universo artístico es algo que todos necesitamos”.

“Cuando tuvimos que suspender las actividades por la pandemia, nos pusimos a pensar en la manera de reformular el Ministerio de Cultura para poder llegar a todos los rincones de los 19 departamentos con transferencia de conocimientos. Viendo la cantidad de gente que se anotó y se sigue anotando a los cursos, creo que estamos por el buen camino”, agregó Llonch, y concluyó: “Vamos a seguir sumando cursos y seguramente quienes no pudieron participar van a tener una oportunidad para sumarse a otros cursos”.

Del acto virtual también participaron la directora provincial de Innovación Sociocultural, Florencia Lattuada; Eva Menardi, de la Subsecretaría de Industrias Creativas a cargo de Félix Fiore; Hugo Sanguinetti, Marilé Difilippo y Anabela Alemano, del equipo de Coordinación de la Escuela; Laura Bruzzo, Natalí Pellegrini y Alejo Gurrea como responsables de la gestión tecnológica; el equipo de comunicación de la Escuela (integrado por Laura Pizzorno, Celina Giménez y Martín Caffaratti); Guillermo Palena y Jorge Ojeda (docentes del curso de Sonido), Romina Palma (a cargo de Ecodiseño) y Yamila Gutiérrez, además de tutores y tutoras de los 19 departamentos de la provincia.

El encuentro de cierre de la segunda etapa de cursos se transmitió a través del canal de Youtube del Ministerio de Cultura, y fue el lugar elegido por muchas de las personas que se capacitaron para agradecer la posibilidad de sumar conocimientos y destacar el trabajo de cada docente, de tutores y tutoras.

Como parte del acto, el coordinador de la Escuela, Hugo Sanguinetti, destacó la participación de santafesinas y santafesinos y recordó que de alguna manera el aprendizaje fue para todos, porque la virtualidad fue algo que nadie había pensado.

“Yo soy un estudiante más en esto de la virtualidad, la Escuela de Oficios estaba pensada para la presencialidad y no abandonamos esa idea, pero estos tiempos nos obligaron a pensar y cambiar. Estoy muy contento de formar parte de este equipo, con el cual pudimos llevar adelante una tarea que nos dio la posibilidad de llegar a toda la provincia”, sostuvo el coordinador.

“Vamos a intentar mantener la virtualidad y que, cuando se vuelva a la actividad presencial, que convivan ambas instancias”, agregó. Esta frase fue repetida por las y los docentes, y también por Lattuada, quien sostuvo que “seguramente en el 2022 tengamos un formato de bimodalidad. Ojalá se pueda dar”.

Por su parte, Guillermo Palena, docente del curso de Sonido, indicó que “fue un gran desafío llegar a cada una de las personas que tomaron parte del curso de forma virtual. Intentamos darle una impronta en la cual poder dar con conceptos básicos y que sean de rápida implementación”.

“No somos docentes, somos trabajadores de este oficio, somos sonidistas”, agregó Palena, quien dictó el curso con Jorge Ojeda. “La industria cultural es muy importante y la pandemia la dejó casi a cero y se tuvieron que buscar muchas alternativas. Este curso fue una herramienta muy valiosa. Haber podido llegar a 207 localidades me parece fabuloso. Somos trabajadores de la cultura. Es importante entender eso, creo que esta gestión lo entiende y lo apoya y nosotros tenemos que seguir aportando”, añadió.

Por su parte, Romina Palma, docente de Ecodiseño, coincidió en que “fue un gran desafío llevar al formato digital lo que uno habitualmente hacía en forma presencial. Y también fue un desafío para quienes estaban en cada lugar de la provincia. Creo que fue una gran instancia de aprendizaje y que hemos aprendido todos”.

Mientras que Yamila Gutiérrez, docente de Maquillaje Artístico, describió a la instancia de capacitación como un “súper desafío, algo nuevo e impensado, que se den clases de maquillaje virtualmente fue bastante extraño. Tenía miedo de no poder atender todas las exigencias de los alumnos, pero creo que lo logré. Ojalá que el arte esté más que nunca presente en toda Santa Fe”.

El acto completo puede verse ingresando al enlace: [https://www.youtube.com/watch?v=jR2C_YyA6Bw&ab_channel=MinisteriodeCulturadelaprovinciadeSantaFe](https://www.youtube.com/watch?v=jR2C_YyA6Bw&ab_channel=MinisteriodeCulturadelaprovinciadeSantaFe "https://www.youtube.com/watch?v=jR2C_YyA6Bw&ab_channel=MinisteriodeCulturadelaprovinciadeSantaFe")

**SOBRE LAS ETAPAS 2 Y 3 DE LA EOA**

La segunda etapa de los cursos contó con 5.700 inscripciones. De ese total, se capacitaron 2706 personas de 207 localidades de la provincia en 19 aulas virtuales, una por departamento. Desde esta semana, quienes completaron el cursado recibirán sus diplomas de manera virtual.

Mientras tanto, la tercera etapa de la Escuela de Oficios Artísticos, que incluye a los cursos de Iluminación, Artes visuales y Carnaval, murgas y comparsas, comenzará en la segunda mitad de julio. La inscripción cerró el 20 de junio, con más de 4 mil postulaciones, de las cuales el 70 por ciento corresponde a mujeres y disidencias.