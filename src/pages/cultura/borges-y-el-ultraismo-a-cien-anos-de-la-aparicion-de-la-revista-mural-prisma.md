---
category: Agenda Ciudadana
date: 2022-11-20T18:26:41-03:00
thumbnail: https://assets.3dnoticias.com.ar/potada-para-nota-sobre-revista-mural.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: " con información de www.ahira.com.ar."
resumen: 'Borges y el ultraísmo: a cien años de la aparición de la Revista Mural Prisma.'
title: 'Borges y el ultraismo: a cien años de la aparición de la Revista Mural Prisma.'
entradilla: '"Noté cómo se colocaban anuncios en las paredes de la calle, y se me
  ocurrió la idea de que podríamos imprimir también una revista mural"'

---
El Archivo Histórico de Revistas Argentinas (www.ahira.com.ar) es un proyecto  que estudia la historia de las revistas argentinas en el siglo veinte;  quienes las dirigieron, escribieron e ilustraron; y el impacto  que lograron con respecto al periodismo masivo, la literatura, la música y los lectores.

Una de la revistas que podemos encontrar en ese magnifico archivo, son los únicos dos números de la Revista Mural Prisma, en la que participaron  Nora y Jorge Luis Borges, Guillermo Juan, Eduardo González Lanuza y Guillermo de Torre. Fueron publicadas en noviembre de 1921 y abril de 1922.

Borges recuerda que “Nuestro pequeño grupo ultraísta estaba ansioso de poseer una revista propia, pero una verdadera revista era algo que estaba más allá de nuestros medios. Noté cómo se colocaban anuncios en las paredes de la calle, y se me ocurrió la idea de que podríamos imprimir también una revista mural, que nosotros mismos pegaríamos sobre las paredes de los edificios, en diferentes partes de la ciudad”. El programa ultraísta, en palabras de Borges, consistían en la “reducción de la lírica a su elemento primordial: la metáfora”; “la tachadura de las frases medianeras, los nexos y los adjetivos inútiles”; la “abolición de los trebejos ornamentales, el confesionismo, las prédicas y la nebulosidad rebuscada”; la “síntesis de dos o más imágenes en una, que ensancha de ese modo su facultad de sugerencia”. 

El ultraísmo, fue un movimiento literario que se inicio en 1918 en España, que se oponía al modernismo. El primer manifiesto ultraísta, fue publicado en ese año, en la revista Cervantes.  Reproducimos el manifiesto: 

_L**os que suscriben, jóvenes que comienzan a realizar su obra, y que por eso creen tener un valor pleno de afirmación, de acuerdo con la orientación señalada por Cansinos-Assens en la interviú que en diciembre último celebró con él X. Bóveda en El Parlamentario, necesitan declarar su voluntad de un arte nuevo que supla la última evolución literaria: el novecentismo.**_

_R**espetando la obra realizada por las grandes figuras de este movimiento, se sienten con anhelos de rebasar la meta alcanzada por estos primogénitos, y proclaman la necesidad de un ultraísmo, para el que invocan la colaboración de toda la juventud literaria española.**_

**_Para esta obra de renovación literaria reclaman, además, la atención de la Prensa y de las revistas de arte._**

**_Nuestra literatura debe renovarse; debe lograr su ultra como hoy pretenden lograrlo nuestro pensamiento científico y político._**

**_Nuestro lema será ultra y en nuestro credo cabrán todas las tendencias sin distinción, con tal que expresen un anhelo nuevo. Más tarde, estas tendencias lograrán su núcleo y se definirán. Por el momento, creemos suficiente lanzar este grito de renovación y anunciar la publicación de una revista que llevará este título de Ultra, y en la que solo lo nuevo hallará acogida._**

_J**óvenes, rompamos por una vez nuestro retraimiento y afirmemos nuestra voluntad de superar a los precursores. —Xavier Bóveda.—César A. Comet.—Fernando Iglesias.—Guillermo de Torre.— Pedro Iglesias Caballero.—Pedro Garflas.— J. Rivas Penedas.— J. de Aroca.**_

![](https://assets.3dnoticias.com.ar/manifiestro ultraista.png)