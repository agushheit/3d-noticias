---
category: Agenda Ciudadana
date: 2021-03-31T06:27:08-03:00
thumbnail: https://assets.3dnoticias.com.ar/teatro.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: 'La provincia y el Instituto Nacional del Teatro firmaron un convenio de
  cooperación titulado: “Intervenciones Escénicas a Cielo Abierto”'
title: 'La provincia y el Instituto Nacional del Teatro firmaron un convenio de cooperación
  titulado: “Intervenciones Escénicas a Cielo Abierto”'
entradilla: A través del acuerdo, rubricado este martes en la capital provincial,
  el Ministerio de Cultura y la entidad articularán acciones para llevar adelante
  producciones teatrales a 14 ciudades santafesinas.

---
El convenio “Intervenciones Escénicas a Cielo Abierto”, refrendado en el Café de El Molino, Fábrica Cultural, plantea presentar obras del “catálogo” del INT en los espacios que la cartera cultural tiene bajo su órbita en la capital santafesina y el Gran Rosario.

El convenio entre la provincia y la entidad representativa del teatro nacional, comenzó a gestarse el año pasado, cuando un contrato similar fue firmado pero debió cancelarse a causa de la pandemia de coronavirus.

“Estamos muy contentos de celebrar este convenio con el INT”, expresó el ministro Jorge Llonch al encabezar el acto. En ese sentido, recordó que el acuerdo ya había sido celebrado en 2020 pero debió ser suspendido por la emergencia sanitaria. No obstante, el titular de la cartera provincial resaltó: “Todo lo que hicimos juntos el año pasado, hoy lo vamos a poder presentar en varias ciudades”.

Por su parte, la representante local del INT, Gabriela Bertazzo remarcó la importancia del convenio ya que “formaliza la relación” entre las partes a través de la cual ha sostenido un contacto fluido y se ha podido “analizar la realidad de la actividad teatral de Santa Fe por la que queda mucho por hacer”. Y añadió: “El convenio tiene como fin la visibilización del teatro pero sobre todo volvernos a encontrar con la gente y llegar a nuevos públicos”.

En cuanto a la disposición del programa, explicó que en cada una de las 14 ciudades apuntadas se priorizarán las obras de grupos locales para evitar grandes desplazamientos de compañías. “Vamos a respetar los protocolos vigentes”, subrayó pero aclaró que “a lo sumo, se moverán a algunas localidades cercanas”, aunque la premisa es “cuidar la salud de los artistas y del público”.

La directora de Programas Socioculturales de Rosario, Carla Saccani, comentó que en la zona del Gran Rosario los lugares elegidos para “Intervenciones Escénicas a Cielo Abierto” serán Los Aleros de Villa Gobernador Gálvez, el espacio CasArijón, y el Galpón 17, a la vera del río Paraná. Asimismo, puso el acento en que habrá “una programación muy nutrida” los días viernes, sábado y domingo.

Además, la funcionaria anticipó una novedad: “El convenio también habilita que podamos realizar la Fiesta Provincial del Teatro acá en la ciudad de Santa Fe”. La icónica actividad, también, había sido pensada para 2020, pero quedó en suspenso.

Por último, Saccani informó que el convenio marco en cuestión contendrá a otros programas como el denominado “INT Presenta” para que desembarque en la provincia. “Es conocido por los teatreros y teatreras como el ‘cátálogo’”, sostuvo y completó: “Vamos a poder programar desde el Misniterio obras de todo el país en Santa Fe”.

En tanto, el director de Programación Artística del Ministerio, Claudio Cherep, indicó que “es muy importante la firma del convenio con un organismo prestigioso, como el INT, que además estuvo presente durante la parte más dura de la pandemia, asistiendo al sector”.

El funcionario remarcó “la enorme voluntad política de la entidad, lo cual nos hace refrendar nuestro compromiso con el INT, y encaminarnos a poner en escena las obras financiadas a través del Plan Podestá en la mayor cantidad de localidades de nuestra provincia”.