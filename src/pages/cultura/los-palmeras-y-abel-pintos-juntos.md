---
category: Cultura
date: 2021-03-09T06:11:49-03:00
layout: Noticia con imagen
author: con información de Diario Uno
resumen: Los Palmeras y Abel Pintos juntos
title: Los Palmeras y Abel Pintos juntos
entradilla: El cantautor publicó en instagram una foto con Cacho Deicas y Marcos Camino,
  líderes de Los Palmeras, en su paso por Santa Fe con su gira Quiero Cantar
thumbnail: https://assets.3dnoticias.com.ar/abel-pintos-pic.webp
link_del_video: ''

---
"Tuvimos una visita de lujo en nuestro estudio de grabación, amigos", –en la ciudad de Santa Fe–, dice la publicación en la cuenta de Instagram de Los Palmeras, mítica banda de cumbia santafesina, junto a la foto de Abel Pintos, Cacho Deicas y Marcos Camino frente a un micrófono. Por su parte, el cantante fue más misterioso en su mensaje: "En #santafe pasan cosas".

![](https://assets.3dnoticias.com.ar/abel.png)

El cantautor está recorriendo el país con su gira Quiero Cantar, y en su paso por Rosario desembarcó para ofrecer una serie de recitales con localidades agotadas en el Anfiteatro Municipal Humberto de Nito, algo que desde el inicio de la pandemia vienen esperando con muchas ansias.

En su cuenta oficial de Instagram Abel Pintos asombró a sus seguidores al publicar una foto con Los Pameras y un posteo enigmático. Sin dar mayores detalles, desató la locura de sus fanáticos que celebraron efusivamente el encuentro. En la cuenta oficial de la banda @lospalmerasoficial agregaron: "Tenemos una sorpresa junto a @abelpintos". ¿Se viene una colaboración?

**Despiertan pasiones**

Abel Pintos enciende el amor de sus fans con cada sorpresa que da. Es que el cantante es imparable y no solo graba singles nuevos y saca discos, sino que se suma a cada propuesta artística y solidaria que se le cruza.

El músico sorprendió la mañana del martes de marzo a los integrantes de las bandas infanto-juveniles de centros municipales durante un ensayo en el Teatro Municipal La Comedia.

Los jóvenes tocaban la canción "Cómo te extraño", de Abel Pintos, cuando de repente comenzó a escucharse la voz del cantante, luego se levantó el telón, y se reveló la sorpresa. Abel Pintos había llegado para cantar con ellos y, como un chico más, se sumó a la interpretación de su hit "Cómo te extraño".

Por otro lado, este fin de semana Los Palmeras vivieron una situación tensa en Córdoba, cuando se retiraron de un club sin tocar por falta de pago. "El público no nos dejaba irnos, nos rompieron un parabrisas", contó Marcos Camino, el líder del grupo musical.

Los Palmeras debieron suspender su actuación minutos antes de arrancar el show porque les prometieron un cheque que nunca apareció. Esto genero la irritación del público que intentó impedir que la banda se vaya y generó incidentes en el predio del Jockey Club de la localidad de Cruz del Eje.