---
category: Cultura
date: 2022-12-16T13:07:36-03:00
thumbnail: https://d7kqr73uznmf6.cloudfront.net/2020-10-07.jpg
layout: Noticia con video
link_del_video: https://youtu.be/QsExuCMlj_o
author: ''
resumen: Disco solista para Emiliano Brancciari, cantante y guitarrista de NTVG
title: Disco solista para Emiliano Brancciari, cantante y guitarrista de NTVG
entradilla: Se estrena hoy  “Cada segundo dura una eternidad” el primer disco solista
  de Emiliano.

---
![](https://assets.3dnoticias.com.ar/emilliano ntvg.webp)Se estrena hoy “Cada segundo dura una eternidad” el primer disco solista de Emiliano, nacido en Argentina, en Munro, pero adoptado por Uruguay, según reza la info en su biografía de Twitter. Fue grabado en enero 2022 en Long Island, NY, con Héctor Castillo y Carlos Imperatori.

El disco tiene 12 canciones y ya esta disponible en tiendas digitales.

Compartimos “QUISE” que es el tercer sencillo del álbum. Junto a KOREA y RUFIÁN completan el trío de adelantos del disco.