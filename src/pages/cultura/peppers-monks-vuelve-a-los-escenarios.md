---
category: La Ciudad
date: 2021-04-20T08:35:55-03:00
thumbnail: https://assets.3dnoticias.com.ar/pepper-monks.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Peppers Monks vuelve a los escenarios
title: Peppers Monks vuelve a los escenarios
entradilla: 'La banda santafesina, tributo a Red Hot Chili Peppers, cumple su segundo
  aniversario y celebran con show presencial y entradas agotadas. '

---
La banda santafesina se forma en abril de 2019 cuando el bajista Hernán Gorosito (ex Astro bonzo) contacta a Pablo Menendez (guitarrista de “Nada Más y Nada Menos”) y le plasma la idea de armar un homenaje a la mítica banda de Funk Rock: Red Hot Chili Peppers. Juntos comienzan a darle forma al proyecto, el cual se completa con: Emilio Marchi (Baterísta de “NMyNM”) y Charles Bovino (vocalista de "Los Cuervos"), todos músicos con amplia trayectoria en la escena local.

Peppers Monks tiene como premisa inicial emular a la banda californiana, no solamente en su audio, sino también en la performance en vivo en cada una de sus presentaciones. La banda recorrió diversos lugares y eventos de Santa Fe, Gálvez, Paraná con intenciones de ampliar este circuito.

En diciembre de 2019, a ocho meses de su formación, Peppers Monks fue ternada y premiada en la categoría " Mejor banda Tributo" de los "Poquet Awards", de la revista Poquet.

Actualmente, y con el motivo de la celebración de su segundo año como banda, vuelven a los escenarios con 2 fechas en la ciudad de Santa Fe y una en Paraná. Prometen nuevo repertorio y estrenan cantante.

**Próximas Fechas**

* Miércoles 21 de Abril, 21hs - Tribus Club de Arte
* Jueves 22 de Abril, 21 hs - Tribus Club de Arte

Reservas al [Instagram ](https://www.instagram.com/tribusclubdearte/)o [Whatsapp ](https://api.whatsapp.com/send?phone=5493424218187)de Tribus

![Puede ser una imagen de 4 personas y texto que dice "LOS MEJORES SHOWS ABIERTOS DE 18 12:00 12:00HS NIN PEPPERS MONKS TRIBUTO A RED HOT CHILI PEPPERS MIÉRCOLES 21/04- 21 hs. TRIBUS CUBBEARTE DERECHO ESPECTÁCULO CON RESERVA PREVIA Quilmes PedidosYa cablevideo bionik"](https://scontent.faep8-1.fna.fbcdn.net/v/t1.6435-9/174477540_737662310261315_2946510865359021916_n.jpg?_nc_cat=103&ccb=1-3&_nc_sid=a26aad&_nc_ohc=XYrV-E-LDFgAX-owmNk&_nc_ht=scontent.faep8-1.fna&oh=ab967df2b5beeeefd5d35506d06d9f3e&oe=60A2053C)

* Viernes 7 de MAyo, 21 hs - Limbo Pub&Rock (Paraná)

Entradas en venta en el [Facebook de Limbo](https://www.facebook.com/limbopna/)

![Puede ser una imagen de 4 personas y texto](https://scontent.faep8-1.fna.fbcdn.net/v/t1.6435-9/166221683_728879057806307_4927641103713428471_n.jpg?_nc_cat=105&ccb=1-3&_nc_sid=a26aad&_nc_ohc=NyhUJe_NSO0AX_EmU6Y&_nc_ht=scontent.faep8-1.fna&oh=d6b34b0074220451b89595c47e278c0b&oe=60A32B6F)