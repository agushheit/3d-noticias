---
category: Cultura
date: 2021-03-30T07:34:55-03:00
layout: Noticia con imagen
author: Prensa Municipalidad de Santa Fe
resumen: Actividades para hacer en la ciudad el feriado largo
title: Actividades para hacer en la ciudad el feriado largo
entradilla: A partir del jueves, habrá eventos recreativos y culturales en Santa Fe.
  Consultá las propuestas.
thumbnail: https://assets.3dnoticias.com.ar/feria1.jpeg
link_del_video: ''

---
La ciudad de Santa Fe ofrece para Semana Santa una agenda de actividades recreativas, deportivas y culturales para disfrutar en distintos espacios públicos con acceso libre y gratuito. La grilla diagramada por el municipio incluye ferias, eventos culturales y recorridos.

**Manzana Jesuítica**

Se pueden realizar las visitas regulares a la Manzana Jesuítica, que cuenta con la modalidad autoguiada, de miércoles a sábados, de 9 a 12 y de 16.30 a 19.30 horas; y con guía el miércoles a las 10 y a las 18 horas. **Este recorrido comienza en la puerta principal de ingreso del tradicional colegio Inmaculada Concepción, en San Martín 1540.**

Con esta visita se intenta recuperar la tarea educativa y científica de la Compañía de Jesús, una de las órdenes católicas más antiguas afincadas en la capital provincial, y también da cuenta del paso de destacados personajes nacionales e internacionales por sus instalaciones como el Papa Francisco y Jorge Luis Borges, entre otros.

La visita estará a cargo de personal municipal especializado que, como siempre, estará identificado con las sombrillas verdes.

**Museos abiertos**

Los museos municipales invitan cada semana a disfrutar de propuestas ligadas a la historia de la ciudad y su patrimonio artístico. Para poder garantizar las condiciones sanitarias en cada uno de los espacios, se permitirá el ingreso de público exclusivamente con reserva previa. **La entrada es gratuita.**

En el casco histórico se puede visitar el **Museo del Colegio Inmaculada** (San Martín 1540), ubicado en la Manzana Jesuítica: de jueves a domingo, de 9 a 12 y de 17 a 20. Las reservas se deben realizar escribiendo un correo electrónico a la dirección museo.manzanajesuitica@santafeciudad.gov.ar

El **Museo de la Constitución Nacional** (Avenida Circunvalación y 1º de Mayo) abre los jueves de 9 a 13 y de 15 a 19; los viernes, sábados y domingos, de 15 a 19. Los turnos se pueden pedir por mail turnos.museo@santafeciudad.gov.ar; o por whatsapp al teléfono 3425118351, que atiende de martes a viernes, de 9 a 13 y de 15 a 19.

En el **Museo Municipal de Artes Visuales** “Sor Josefa Díaz y Clucellas”, se pueden realizar visitas a la muestra patrimonial “Remontar un río. Salones Litoral y Modernidad (1940/1980)”, que permanecerá abierta hasta el 4 de abril: de jueves a sábados, de 10.30 a 12.30 y de 18 a 20 horas; y los domingos, de 18 a 20 horas. Las reservas se pueden realizar por teléfono, al número (0342) 4571886, entre las 8 y las 12 horas; o por correo electrónico a la dirección mmav.reservarturnos@gmail.com

**Mi Ciudad como Turista**

Mi ciudad como Turista es una propuesta que ofrece, por un lado, visitas guiadas gratuitas a santafesinos y visitantes con modalidad free tour y; por el otro, recorridos autoguiados que se pueden llevar a cabo sin la necesidad de asistir a un horario específico.

En cualquiera de sus dos modalidades se podrá conocer puntos emblemáticos de la ciudad a través del relato vivencial de quienes los transcurren diariamente, con distancias cortas (entre los 800 a 1.500 metros).Para escuchar y/o descargar las audioguías se puede ingresar a la página web de la ciudad, o también se pueden escuchar desde el perfil de Santa Fe Capital en Spotify. Vale destacar que en ambas plataformas se encuentran disponibles los recorridos de Paseo del Puerto, Paseo Costanera, Paseo Peatonal San Martín, Manzana Jesuítica, Paseo Boulevard, Casco Histórico, entre otros.

Las audioguías son un desarrollo de la Municipalidad orientado a generar herramientas de autogestión útiles para la ciudadanía y los visitantes que permitan reforzar el compromiso como municipio sustentable, generando políticas amigables con el ambiente.