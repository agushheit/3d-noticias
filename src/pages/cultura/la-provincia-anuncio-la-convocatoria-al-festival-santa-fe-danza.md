---
category: Cultura
date: 2021-04-02T08:04:53-03:00
layout: Noticia con imagen
author: Prensa Gobierno de Santa Fe
resumen: La provincia anunció la convocatoria al festival “Santa Fe Danza”
title: La provincia anunció la convocatoria al festival “Santa Fe Danza”
entradilla: Se realizará durante cinco semanas. Además se informó el avance en el
  Senado del proyecto de creación del Instituto Santafesino de Danza (ISD), que ya
  fue aprobado por la Cámara de Diputados.
thumbnail: https://assets.3dnoticias.com.ar/danza.jpg
link_del_video: ''

---
La vicegobernadora de la provincia, Alejandra Rodenas, acompañada por funcionarios y funcionarias del Ministerio de Cultura y representantes del Movimiento Santafesino de Danza (MSD), lanzó formalmente la propuesta en Casa Arijón, de Rosario, una de sus cinco sedes.

La titular del Senado destacó: “Nada ocurre si antes no sucedió en el espíritu y en el alma de los artistas. El Estado se hace cargo de formalizar una idea que subyace, que está antes y esta es la gran virtud del gobierno”.

Rodenas subrayó “la importancia de la realización de actividades culturales” y, en sintonía con la actividad, anunció los progresos en torno del tratamiento de la iniciativa que postula la creación del ISD, una demanda histórica de este sector artístico propuesta por el diputado Ricardo Olivera, y que ya recibió aprobación en la Cámara de Diputados”.

En esa línea, la vicegobernadora sentenció: “El instrumento normativo es esencial. Lo que hace una ley es recoger una inquietud y una preexistencia. Esta ley provincial reconoce la historia, la trama, un deseo y un sentir. Me comprometo a que la Cámara de Senadores se conmocione con esta ley”.

Rodenas expresó que “se trata de tomar cuestiones que están en el espíritu de los artistas y llevarlas a políticas públicas. Esto ocurre a partir de que el Gobierno ha federalizado y territorializado el deseo de quienes están involucrados. Se trata de un federalismo hacia adentro también. Esta es una provincia muy extensa y sumamente heterogénea. No existe un pensamiento ni un modo de sentir único ni siquiera en la danza. Por eso que el festival tenga cinco sedes, habla de todas las tradiciones”.

Para finalizar sintetizó: “La cultura cumple un rol fundamental, porque donde un chico, una chica, un joven, encuentra un espacio de concordia y de diálogo es muy raro que decida encarar un día violento”.

**EL ESPÍRITU DE "SANTA FE DANZA"**

Por su parte, la directora del Programas Socioculturales, Carla Saccani, observó que el festival se desarrollará “durante cinco semanas, en cinco ciudades, una actividad que permitirá llevar a escena una propuesta artística inédita, que va a visibilizar toda la versatilidad y pluralidad de este género tan afectado por la pandemia”.

En tanto, las representantes del MSD, Lis Marconi y Patricia Ghisoli demostraron su emoción frente al impulso del proyecto que trata el Senado: “Una ley santafesina de danza que cree un Instituto Santafesino de Danza no es solamente importante para financiar y fomentar la danza, también es fundamental para el acceso democrático a un bien cultural, y a un derecho cultural como lo es la danza para todas y todos los santafesinos”.

En esa línea, reflexionaron: “Un derecho cultural que viene postergado en relación a otras artes y que se merece tener presencia. Por eso valoramos los caminos de escucha que tenemos habilitados con el gobierno. Queremos esta ley que nos invite a quedarnos en nuestra provincia a trabajar y no sólo tener oportunidades de trabajo en Buenos Aires o fuera del país. Danza es arte, es cultura, es trabajo, es salud y es educación”. Y destacaron “la labor del gobierno por ser escuchados y darles acogida a sus inquietudes y a la realización de este festival de danza”.

**EL FESTIVAL**

“Santa Fe Danza” se realizará entre el 8 de mayo y el 12 de junio próximos, y se trata de un evento único e inédito en el país, en forma conjunta entre el Ministerio de Cultura de la Provincia de Santa Fe y el MSD, y la convocatoria –sin precedentes– estará dirigida a bailarinas, bailarines y grupos de todo el territorio provincial.

“La invitación es dirigida a representantes de diferentes lenguajes de la danza, para seleccionar un total de 250 artistas que se presentarán en escena en funciones programadas en cinco sedes: Rafaela, Reconquista, Rosario, Santa Fe y Venado Tuerto”, indicó Saccani.

El secretario de Gestión Cultural, Jorge Pavarin, sostuvo que “la modalidad de los espectáculos será presencial, con entrada gratuita y cupo de público según el protocolo sanitario vigente. Y agregó: “Estamos proponiendo un evento inédito, con un importante alcance territorial, y que pone en valor la danza santafesina y su pluralidad, desde criterios democráticos y federales”.

“Asimismo –puntualizó el director de Programación Artística, Claudio Cherep– en cada sede se llevarán adelante charlas sobre políticas públicas culturales para el sector de la danza, en virtud de la crisis por la que viene atravesando la actividad desde la declaración de la pandemia de Covid-19”.

El funcionario agregó: “En caso de sancionarse esta ley, la provincia de Santa Fe pasaría a la vanguardia porque hay una sola provincia que tiene ley de danzas y es Misiones”.

**CRONOGRAMA**

* 8 de mayo Rosario - Casa Arijón
* 15 de mayo - Reconquista - Paseo del Bicentenario
* 29 de mayo - Santa Fe Capital – La Redonda
* 5 de junio - Rafaela - Cine Teatro Municipal \`Manuel Belgrano´
* 12 de junio - Venado Tuerto – Teatro Ideal | Centro Cultural Provincial.

**ACERCA DEL MOVIMIENTO SANTAFESINO DE DANZA**

El Movimiento Santafesino de Danza surge como grupo de trabajadores de la danza de la provincia en 2019, en pos de la visibilización, el fomento y la ampliación de derechos del sector en todo el territorio provincial.

Como organización ha realizado acciones a corto, mediano y largo plazo. En este marco impulsa la aprobación del proyecto de ley que postula la creación del Instituto Santafesino de Danza, cuya media sanción en la Cámara de Diputados implica un precedente histórico de reconocimiento y de fomento para el sector.