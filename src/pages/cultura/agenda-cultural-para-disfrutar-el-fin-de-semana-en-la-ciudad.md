---
category: Cultura
date: 2021-02-27T09:00:00-03:00
layout: Noticia con imagen
author: 3D Noticias
resumen: Agenda cultural para disfrutar el fin de semana en la Ciudad
title: Agenda cultural para disfrutar el fin de semana en la Ciudad
entradilla: Espacios lúdicos, cine, danza, música, ferias y mucho más. Todas las propuestas
  son con entradas libres y gratuitas.
thumbnail: https://assets.3dnoticias.com.ar/picnic.jpg
link_del_video: ''

---
Febrero se despide en la capital provincial con una agenda nutrida de actividades para toda la familia, organizadas por la Municipalidad con entrada libre y gratuita.

La programación se puso en marcha este jueves, con encuentros para celebrar el 80 aniversario del nacimiento del poeta y ceramista Héctor Rolando “Kiwi” Rodríguez. También se desarrolla entre jueves y viernes una nueva edición de Santa Fe Jazz Campus, que busca potenciar la escena santafesina del jazz y contará con la participación de invitados especiales. Habrá conciertos en el renovado Anfiteatro del Parque del Sur y en la Estación Belgrano, como parte de esta propuesta que organiza la Municipalidad junto a Santa Fe Jazz Campus y La Brava Producciones.

Este viernes a las 20 horas habrá noche de jazz al aire libre. Con entrada libre y gratuita, se presentará el dúo de la cantante Flopa Suksdorf y el contrabajista Cristian Bórtoli; seguido por Richard Nant acompañado por un Ensamble de percusión y trío de jazz. Será en la Estación Belgrano, en el estacionamiento sobre Avellaneda. Acompañando la música en vivo habrá propuestas gastronómicas.

**Viernes en Loyola Sur**

La programación del viernes incluye los encuentros de Vereda para Jugar, de 15.30 a 18.30 horas, en José Pedroni y Furlong. El punto de encuentro para las familias del barrio es la vereda que une la Estación y el Jardín de Loyola Sur, donde la Municipalidad realizó una serie de intervenciones lúdicas que se proyectaron junto a la Red de Instituciones del barrio.

La rayuela, un tablero de ajedrez y otro de ta-te-tí, son algunos de los juegos disponibles -con sus respectivas piezas artesanales- que fueron diseñados en baldosas de cerámica por la Escuela de Diseño y Artes Visuales del Liceo Municipal.

**Pantalla Pública**

El ciclo de cine de verano finaliza el viernes 26, a las 19.30, con la proyección de “El verano de Kikujiro” (1999), en el Auditorio Federal del Museo de la Constitución. La entrada es gratuita, con capacidad limitada de la sala, por lo que deberá reservarse previamente en la sección Pantalla Pública, de la plataforma Capital Cultural.

El largometraje de Takeshi Kitano retrata el verano de Masao, un niño que tiene que pasar las vacaciones con su abuela. Aburrido, ya que todos sus amigos se marcharon a la playa, decide buscar a su madre, a la que nunca ha visto. Con muy poco dinero, una fotografía y una dirección como únicas referencias, y acompañado por Kikujiro –un antiguo yakuza- el plan del niño parece condenado al fracaso.

**Teatro en La Marechal**

El ciclo de teatro que se desarrolló durante esta temporada en la Sala Marechal del Teatro Municipal “1º de Mayo” finaliza este fin de semana con dos funciones. El viernes 26, a las 21, el Grupo Trotea presenta “El Bizco”, escrita por Marta Degracia con dirección de Sergio Gullino. En la obra, “cambios de sentido, recuerdos y olvidos revelan en los personajes el paso del tiempo y la cercanía a la vejez”.

El laboratorio de composición MERAKI, dirigido por Julieta Taborda, estrena “Sinfonía ritual” el sábado 27, a las 21. La puesta es el resultado del trabajo que realizaron el año pasado, como parte de los espacios formativos que organizó la Secretaría de Educación y Cultura de la Municipalidad a partir de una convocatoria abierta. De esa manera “se gestó de manera colectiva una obra donde se cruzan la danza, las artes visuales, audiovisuales, la música y la fotografía, con la intención de abrirnos a nuevas experiencias y poner en tensión los diversos lenguajes y saberes”. Desde la producción destacaron además el estreno online esa misma noche, a las 23, en el canal de YouTube MERAKI Transdisciplinar

Las entradas para estos espectáculos están a la venta en la boletería, a 200 pesos. Pueden adquirirse de martes a viernes, de 17 a 21 horas. Atendiendo al protocolo vigente para el funcionamiento de la sala, el cupo será limitado a 55 entradas, por lo que se recomienda adquirirlas con anticipación.

**Danza en la Playa**

El ciclo de danza finaliza el viernes 26 de febrero desde las 20, en el Centro de Deportes (Av. Almte. Brown 5200), con entrada libre y gratuita. Clap Producciones presentará un show con bailarines en escena y música en vivo. Bboy Angel (Ángel Speranza), Bgirl Noe (María Noelia Arias) y Bgirl Looli (Lucila Castillo) interpretarán las instrumentales creadas por Rodrigo Castillo (aka Gogo Clap) y Valentín Fabris (aka Vaf).

La danza contemporánea estará representada en esta fecha por la Compañía Ham que presentará una intervención urbana, en la que vuelven a pensar “la distancia que nos une, percutiendo otras formas de lenguaje”. El elenco dirigido por Cecilia Romero Kucharuk, está integrado por Mariel Barcos, Azul Lazzaroni, Emanuel Virgilio, Julieta Albamonte, David Leonhardt, Rosina Heldner, Esmeralda Dotti y Nicolas Bordón.

**Música en vivo en los paradores**

Los recitales para disfrutar en los paradores de playa también se despiden este fin de semana con dos fechas. El viernes 26 de febrero, a las 21, Cecilia Arellano se presenta en el Parador Santa Fe, ubicado en la Costanera Este. Su propuesta para esa noche llega de la mano de Ancestral, un show con canciones autorales inéditas que pertenecen a su último disco “Apego” en torno al género Jazz-Brasil-latinoamericano y elementos experimentales. El grupo está formado por artistas destacados en sus instrumentos: Oscar Castellanos en guitarras, Alexander Russel-White en bajo y efectos, y Pablo Minen en batería.

En el escenario de Santa Kite (Alte. Brown 6880), el encuentro será el sábado 27 de febrero a las 19, con ÑÑÑÑ, definido por sus integrantes como un proyecto que se ubica entre una instalación y una banda, rompen las fronteras de la canción, la improvisación y el remix, todo se fragmenta y se abstrae.

**Barrio Abierto en Yapeyú**

La Municipalidad continúa trabajando junto a las Redes de Instituciones Barriales, en esta oportunidad, de Yapeyú. La invitación es para todas las vecinas y los vecinos del barrio: el sábado 27 de febrero desde las 19, en Avenida 12 de Octubre y Chubut.

Durante el encuentro se podrá disfrutar de una feria de emprendedoras y emprendedores del barrio y presentaciones a cargo de artistas invitados y expresiones vinculadas al trabajo comunitario que se desarrolla en el barrio. En paralelo a la feria habrá numerosas presentaciones que comenzarán con una muestra de reggaeton y ritmos urbanos, a cargo de integrantes de la academia de baile Inpacto Dance, que coordinan Nicolás Farías, Nahir Gómez y Uriel Salinas.

Luego será el turno de la Escuela de Patín Fantasía, de Valeria Pereyra. La grilla continuará con el violinista Dylan Villanueva, Simón Gómez y Yonatan Alzugaray (rap), Bastian Hernán Sal Domínguez (solista de órgano); e integrantes y profesores del Taller de Música de la Vecinal Ceferino Namuncurá, de barrio Yapeyú, que coordina Diego Tacundo. El cierre será a pura cumbia santafesina, con el grupo Habana Combo.

**Carnavales en los Barrios**

Hasta el 5 de marzo continúan los festejos organizados por las agrupaciones barriales y el Movimiento de Organizaciones Murgueras del Oeste (MOMO), con el apoyo de la Municipalidad de Santa Fe y del Gobierno de la provincia.

El sábado 27, de 19 a 21 horas, habrá tres encuentros en simultáneo: en el Playón Municipal de Varadero Sarsotti (Tacuarita y Gallareta); y en El Alero (bulevar French 1785) de barrio Coronel Dorrego, impulsadas por el MOMO; y en barrio San Roque, en la intersección de Avellaneda y Quintana.

El domingo 28, también entre las 19 y las 21, la celebración popular será con los Carnabarriales de barrio San Lorenzo, que finalizarán como cada año con la quema del dios Momo, en las inmediaciones del Centro Social y Cultural El Birri.

**Verano Capital en el Parque Garay**

El domingo 28, desde las 17.30, se presentan artistas del Circo Litoral, en el Parque Garay. En esta última fecha del ciclo también se podrá disfrutar del espectáculo humorístico “El show de Naza y Lari”, que sus intérpretes resumen diciendo: “Naza (Nazareno Baldo) se está preparando para el show de su vida. Quiere llamar la atención del público entero. Lari (Larisa Sánchez) tratará de mostrar también su talento, pero Naza querrá que ella sea solo su asistente. ¿Podrán juntos mostrar sus bailes, sus poemas y canciones?”.

**Estrenos en Capital Cultural**

La plataforma de contenidos educativos y culturales Capital Cultural también presenta opciones para disfrutar desde casa. El sábado 27, a las 19, se podrá ver el cuarto capítulo de la serie que recupera los 60 años del Taller de La Guardia. Recorre los inicios del trabajo de la cerámica en la zona costera, desde las primeras fábricas que se asentaron en el lugar en la década de 1940 hasta el nacimiento del taller como espacio de recuperación de las tradiciones ancestrales del territorio.

También el sábado, pero a las 21, estará disponible el último concierto de Santa Fe Jazz Sessions, con la presentación de Latitudes, registrada en la Sala Maggi del Foro Cultural Universitario. Se podrá ver en el canal de YouTube de Santa Fe Jazz Sessions, promocionado también desde los sitios web Capital Cultural, Agenda Cultural UNL y redes sociales de las instituciones organizadoras.

**Artesanías en la Plaza Pueyrredón**

Como cada fin de semana, el sábado 27 y domingo 28 de febrero, desde las 17 horas, artesanos y las artesanas que participan de la Feria del Sol y la Luna se reúnen en la Plaza Pueyrredón (Bv. Gálvez 1600), en un encuentro que es Patrimonio Cultural y Artístico de la capital santafesina.

**Deportes al sol**

Las actividades deportivas gratuitas continúan durante este fin de semana en la capital santafesina en seis espacios a cielo abierto. Se trata de las sedes ubicadas en las playas de la Costanera Este, la del Centro de Deportes de la Costanera Oeste, las del Espigón I y II y los Parques Sur y Garay.

Habrá Beach Voley el sábado, de 18 a 20, en el Centro de Deportes de la Costanera Oeste y de 17 a 18 en Playa Este, mientras que el domingo, de 17 a 18, será el turno de los Espigones de la Costanera Oeste.

Por otra parte, habrá Beach Tenis el sábado, de 9 a 12, en el Centro de Deportes de la Costanera Oeste.

La propuesta de Ritmos se llevará a cabo el sábado de 18 a 19 en el Centro de Deportes de la Costanera Oeste, y el sábado y domingo de 18 a 19 en Playa Este.

Además, el sábado, a las 19 horas, habrá ajedrez en barrio Yapeyú. La propuesta incluye piezas gigantes y clases gratuitas en la vía pública para grandes y chicos.

El sábado en la playa ubicada en la Dirección de Deportes, habrá una jornada amistosa de Beach Hockey, con partidos desde las 15 hasta las 19 horas. El encuentro corresponde a Primera Damas y Sub 16 y está organizado por la Asociación de Hockey Social y Deportes de Santa Fe Capital.

Para el domingo, en el playón principal de la Dirección de Deportes de la costanera, se realizará una exhibición de boxeo recreativo a cargo de Claudio Martinet, desde las 17 y con futuros púgiles de 12 a 18 años.

Y, por último, Beach Handball, se disputará el domingo desde las 17 en el Centro de Deportes de la Costanera Oeste.

Cabe señalar que además de los 90 guardavidas que supervisarán las playas, la Municipalidad dispuso la presencia de más de 100 profesores de Educación Física que estarán a cargo de las actividades deportivas y recreativas.

**Visita del Seleccionado Nacional de Softbol**

Este viernes 26, desde las 17, la Dirección de Deportes y el Club Ferroviario recibirán al Seleccionado Nacional de Softbol, actual campeón del mundo. Será una jornada inolvidable, que contará con un entrenamiento especial, respetando el distanciamiento y protocolo, y destinada a todos los interesados e interesadas en la disciplina.

El equipo dirigido por el entrerriano Julio Gamarci selló una campaña inolvidable en el último mundial, con 9 victorias y una sola derrota ante Japón (4-8) en la etapa clasificatoria. Los otros éxitos del flamante campeón del mundo en el Mundial de República Checa fueron los siguientes: en la etapa clasificatoria, 8-0 a Botswana, 15-0 a Filipinas, 6-3 a República Checa, 3-2 a Cuba, 11-1 a México y 6-4 a Nueva Zelanda; en los cuartos de final, 9-0 a los Estados Unidos, en la semifinal, 7-0 a Canadá y en la final, derrotando a Japón por 3 a 2.

El plantel estuvo integrado por Nicolás Carril, Federico Eder, Gustavo Godoy, Manuel Godoy, Román Godoy, Ladislao Malarczuk, Huemul Mata, Pablo Migliavacca, Teo Migliavacca, Mariano Montero, Bruno Motroni, Gonzalo Ojeda, Federico Olheiser, Alan Peker, Juan Potolicchio, Gian Marcos Scialcomo y Juan Cruz Zara.

**Recorridos turísticos gratuitos**

La propuesta “Mi ciudad como Turista”, que invita a recorrer espacios emblemáticos de la ciudad para conocer historias, anécdotas y curiosidades, continúa este sábado 23 de enero. La convocatoria es a las 19 horas en el Teatro Municipal “1º de Mayo” (San Martín 2020) para realizar el Paseo Peatonal.

La modalidad de estos recorridos es pedestre, guiada y gratuita e invita a la ciudadanía a jugar, a ser turista en su ciudad, a ver y recorrer aquellos lugares por los que se transita diariamente con ojos de turista.

Los guías estarán identificados con paraguas verdes con el logo de Santa Fe Capital y, si bien el recorrido es al aire libre, se recuerda que es necesario respetar las medidas de cuidado personal que dictan los protocolos de prevención de coronavirus: uso de tapabocas, mantener la distancia social y usar alcohol en gel frecuentemente. Además, al ser recorridos pedestres, se suspenderán en caso de lluvia y serán oportunamente reprogramados.

Por otra parte, se pueden realizar las visitas regulares a la Manzana Jesuítica, que cuenta con la modalidad autoguiada de miércoles a sábados de 9 a 12 y de 16.30 a 19.30; y con guía los mismos días a las 10 y a las 18 horas. Este recorrido comienza en la puerta principal de ingreso del tradicional colegio Inmaculada Concepción, en San Martín 1540 y recupera la tarea evangelizadora, educativa y científica de la Compañía de Jesús, una de las órdenes católicas más antiguas afincadas en la capital provincial, y también da cuenta del paso de destacados personajes nacionales e internacionales por sus instalaciones como el Papa Francisco y Jorge Luis Borges, entre otros. La visita estará a cargo de personal municipal especializado que, como siempre, estará identificado con las sombrillas verdes.

Para escuchar y/o descargar las audioguías se puede ingresar a la página de la ciudad: [https://santafeciudad.gov.ar/secretaria-de-produccion-y-desarrollo-economico/turismo/mi-ciudad-como-turista/](https://santafeciudad.gov.ar/secretaria-de-produccion-y-desarrollo-economico/turismo/mi-ciudad-como-turista/ "https://santafeciudad.gov.ar/secretaria-de-produccion-y-desarrollo-economico/turismo/mi-ciudad-como-turista/"), o también se pueden escuchar desde el perfil de Santa Fe Capital en Spotify: [https://open.spotify.com/show/0TghWaTuOn4LAnbja2YuMC](https://open.spotify.com/show/0TghWaTuOn4LAnbja2YuMC "https://open.spotify.com/show/0TghWaTuOn4LAnbja2YuMC").

**Ferias**

Paralelamente, se realizan distintas ferias de artesanos, emprendedores y recicladores de antigüedades en la ciudad. En Colastiné Norte, se llevará a cabo la Feria Renace y Emprende el sábado 27, de 16 a 21 horas, a la altura de la ruta 1 km 2.7 sobre colectora oeste, calle Duraznos entre Padre Mario Mendoza y Los eucaliptus (Colastiné Norte).

También se desarrollarán las siguientes ferias: Emprendedores del Sur funcionará el sábado 27 de 13 a 20 horas en Av.Illia y 9 de julio; Sueño del Norte el sábado 27 de 17.30 a 21, en Baigorrita 9300; Feria Parque Federal el domingo 28 de 17 a 21 en el Parque Federal; Feria Incuba el domingo 28 de 18 a 21 en Plaza Constituyentes; Paseo Costanera Oeste el domingo 28 de 16 a 21 en Almirante Brown al 6000; Feria del Boulevard el sábado 27 de 17 a 21 en Pedro Vittori y Bv. Gálvez; Feria Mujeres Agroecológicas el sábado 27 de 8 a 13 en Iturraspe y 4 de enero; Paseo Capital, el jueves 25 de 18 a 21 en Nicolás Rodríguez Peña e Independencia; Feria Por Amor al Barrio el sábado 27 de 17 a 21 en Plaza Central Centro Comercial Barrio El Pozo; y Emprendedores Unidos el sábado 27 de 17 a 21 en Pasaje Sarmiento y Gorostiaga.

**Música y gastronomía**

Para el viernes 26, a las 21.15, en la Brasería Ovidio, está prevista la presentación de Luciano Stizzoli en piano y Manuel Ponzo en saxo, continuando con el ciclo de jazz que comenzó semanas atrás en ese espacio gastronómico. Las reservas se pueden realizar al teléfono 0342 412-4800.

**Paseos Náuticos**

El Catamarán Costa Litoral se encuentra realizando sus paseos desde el Dique I del Puerto de la Ciudad de Santa Fe, los días sábados y domingo, a las 17 horas. El paseo es un recorrido por las islas de Santa Fe y tiene una duración de dos horas.