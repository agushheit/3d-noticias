---
category: Cultura
date: 2021-03-29T06:00:34-03:00
layout: Noticia con imagen
author: 3D Noticias
resumen: 'Reabrió el Mercado Progreso: un gran patio para la cultura santafesina'
title: 'Reabrió el Mercado Progreso: un gran patio para la cultura santafesina'
entradilla: La Municipalidad puso a punto el Mercado Progreso, que volvió a abrir
  sus puertas este viernes para recibir a artistas, feriantes y performers, junto
  al público santafesino.
thumbnail: https://assets.3dnoticias.com.ar/mercado.jpeg
link_del_video: ''

---
El patio del Mercado Progreso volvió a encenderse en la noche del viernes, junto a performers, feriantes, gastronomía y música en vivo. El intendente Emilio Jatón asistió a la reapertura, que llegó luego de la obra pública que comenzó en mayo de 2020. La celebración continúa durante las noches del sábado y el domingo, con una programación enmarcada por el Mes de las Mujeres y las Disidencias y en cumplimiento del cupo femenino vigente en la ciudad.

Desde el recuperado espacio, Jatón afirmó: “Es una noche de estreno para el Mercado Progreso, un lugar que es un sueño, pero estaba perdido. Donde había un tabique y escombros hemos recuperado un espacio cultural para la ciudad de Santa Fe, con mano de obra propia y por eso estamos felices, junto a los artistas y los feriantes”.

“Sabemos que falta mucho, pero hoy festejamos la primera etapa y que tenemos otra vez el Mercado Progreso. En este contexto necesitamos más que nunca de estos encuentros y estos espacios, que son propios de la comunidad, pero que primero tienen que estar en condiciones”, agregó Jatón.

Por su parte, el secretario de Educación y Cultura, Paulo Ricci, precisó que la programación de lecturas, performances y música en vivo se inscribe dentro del Mes de las Mujeres y las Disidencias, que atraviesa todas las programaciones públicas del municipio, a lo que se suman emprendedores y gastronomía. “En los últimos dos meses volvimos a abrir las puertas de tres espacios emblemáticos de la ciudad, como lo son el Anfiteatro Juan de Garay del Parque Sur, el Museo Municipal Sor Josefa Díaz y Clucellas y ahora el Mercado Progreso, lugares fundamentales para la cultura y las actividades de toda la ciudadanía; fruto de decisiones tomadas, apenas asumió el Intendente, de hacer las obras necesarias”, celebró el funcionario.

**Espacios recuperados para la ciudadanía**

Tal como precisó Ricci, el proceso de obras en el Mercado Progreso se llevó a cabo durante casi todo el 2020 y sólo se detuvo en el lapso en que no estaba habilitada la obra pública por la pandemia. Comenzó por la recuperación del patio, que estaba habilitado sólo hasta la mitad, donde se limpiaron escombros y se hizo un nuevo piso de concreto.

Se instaló a su vez un escenario de 8,54 por 4,88 metros, concluyendo en una ampliación del 40% del espacio habilitado. Se renovó la instalación eléctrica, en ambas galerías, con la colocación de dos reflectores led y una caja térmica con tensión eléctrica por columna, 20 cajas de tensión eléctrica con su térmica y 40 reflectores led. Por último, se colocó una unidad container de baños con cloacas, con tres baños con lavatorios individuales y un baño acondicionado para personas con discapacidad.

En cuanto a la programación que volverá a desarrollarse en el Mercado, el titular de Educación y Cultura de la Municipalidad adelantó que “se va a trabajar en distintos horarios del día con distintas propuestas y, en la medida en que sea posible por el contexto sanitario, vamos a hacer peñas de distintos géneros, convocando a algunos estilos que no tienen otro lugar donde expresarse”. “Confiamos en que todas y todos van a poder encontrar aquí un gran patio para la cultura santafesina”, expresó el secretario.