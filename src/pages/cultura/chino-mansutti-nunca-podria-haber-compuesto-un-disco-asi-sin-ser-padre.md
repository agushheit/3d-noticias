---
layout: Noticia con imagen
category: Cultura
resumen: Chino Mansutti
date: 2021-02-20T16:15:00-03:00
author: Juan Almará
entradilla: En diálogo con 3D Noticias, Mansutti anticipa lo que se podrá ver este domingo en el recital presentación de "Ese mundo sin tiempo", a la vez que reflexiona sobre el proceso creativo en una etapa muy especial de su vida.
title: 'Chino Mansutti: “Nunca podría haber compuesto un disco así sin ser padre”'
thumbnail: https://assets.3dnoticias.com.ar/https___cdn.evbuc.com_images_124650701_289564815664_1_original.jpg

---

**_ATENCIÓN: SHOW REPROGRAMADO 21/03/2021_**

A cuatro meses de haber sido editado, finalmente ve la luz sobre las tablas "Ese mundo sin tiempo", cuarto disco solista de Gonzalo “Chino” Mansutti. La cita es este domingo 21 a las 19 en la terraza de Paradiso Puerto (Francisco Miguens 180, Puerto de Santa Fe) con capacidad limitada. Las entradas pueden conseguirse en www.evenbrite.com.ar o a través de la APP de Paradiso.

Con una fuerte impronta bailable y electrónica fue producido por Leo Costa y tiene como invitados a Zoe Gotusso y Santi Celli (ex Salvapantallas) y Pablo Di Nardo de Mi Amigo Invencible. En tanto “Los Cronopios”, la banda que acompaña a Mansutti está integrada por Fabrizio Galoppo (guitarra), Nehemías Berón (bajo y coros), Rafael Duré (batería y programaciones) y Julián Gervasoni (teclado y coros).

**-"Ese mundo sin tiempo" llegó a las plataformas digitales en octubre del año pasado, pero recién ahora podés llevarlo al vivo. ¿Cómo vivís un show en un momento como este?**

\-Lo vivo de forma diferente a las tres presentaciones anteriores. En algún momento pensé que nunca iba a poder mostrarlo. Cuando se extendió tanto el confinamiento y decidí sacarlo igual, creí que iba a ser un disco particular porque íbamos a seguir metidos adentro. Y de pronto se dio la oportunidad, así que para mí es super valioso. Por eso mismo decidí tirar la casa por la ventana, por la incertidumbre que genera la pandemia. No sabemos si más adelante nos tendremos que guardar otra vez y se cortarán los toques. Todos nos volvimos terrenales y tratamos de disfrutar el presente. A nivel producción es uno de los shows más importantes de mi vida, y eso se va a replicar sobre el escenario. Tuve un año y medio para prepararlo, así que estoy muy feliz con lo que va a suceder.

**-La presentación viene acompañada por la previa de Interfaces y una apuesta visual muy importante ¿Qué podés adelantar de lo que va a pasar el domingo?**

\-Interfaces es una banda de amigos que está arrancando y me encanta. Son dos chicos muy brillantes, ellos van a abrir la noche. Después hay cantantes invitadas muy talentosas que van a aparecer en nuestro show. La puesta en escena es inédita en mis años de música. Me encontré con un lugar que tiene una pantalla con escenario, y no al revés. Ese espacio se usa normalmente para funciones de cine y tenía que aprovecharlo. Decidimos llamar a Mauro García, un VJ que está haciendo cosas geniales. Creamos unas visuales alucinantes. Es difícil de contarlo porque las visuales audio rítmicas, que van al tempo de la música y se tocan en vivo, son toda una experiencia. Va a estar buenísimo y me pone muy ansioso, va a ser un gran porcentaje del espectáculo.

**-Este es tu cuarto trabajo solista, ¿cómo lo podés definir musicalmente y en qué momento de tu vida personal llega?**

\-Es bastante particular, es diferente a los otros, como ellos lo son entre sí también. Pero este tiene la particularidad de ser hecho en otras circunstancias sentimentales. Es mi primer disco como papá. Y eso cambió mucho mi visión de la vida y de la forma de hacer canciones. Nunca podría haber compuesto un disco así sin ser padre. De hecho nunca creé una obra con esta suerte de energía y positivismo antes de la paternidad. Antes escribí en otro tipo de situaciones, quizás más tortuosas o marcadas por la nostalgia. Este se hizo en un contexto de estabilidad emocional importante y única. Musicalmente se encaró desde una idea diferente. Por eso quizás sea mi disco más bailable, enérgico y más _up_. El mundo sin tiempo es también el de la paternidad, habla de la fugacidad y de lo resolutivos que nos volvemos cuando el tiempo desaparece de nuestras vidas. Y está atravesado por muchos géneros musicales con los que tenía ganas de coquetear desde hace tiempo. Básicamente la música electrónica y sus primeros sonidos de la década del 80’. Escucho muchas bandas de esa época y hasta ahora no había podido citarlas. También son canciones muy simples, y eso tiene que ver con haberme reencontrado con la infancia y con la simpleza de lo pequeño.

**-¿Qué otros proyectos tenés para este año?**

\-El plan inmediato es volver a la dinámica pre-pandemia. Tenemos que terminar una gira provincial que se cortó por el encierro. Y me gustaría salir a tocar mucho porque este disco está hecho para el vivo. Ya estoy haciendo música nueva, como siempre, pero creo que “Ese mundo sin tiempo” merece un año y algo más de rodaje. Salió en octubre y tuvo que aguardar unos meses para ser mostrado.