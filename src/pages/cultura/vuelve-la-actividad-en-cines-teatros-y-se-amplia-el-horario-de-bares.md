---
category: Cultura
date: 2021-07-09T09:33:28-03:00
layout: Noticia con imagen
author: ''
resumen: Vuelve la actividad en cines, teatros y se amplía el horario de bares
title: Vuelve la actividad en cines, teatros y se amplía el horario de bares
entradilla: 'Las nuevas medidas estarán vigentes entre el 9 y el 23 de julio. '
thumbnail: https://assets.3dnoticias.com.ar/teatros.png
link_del_video: ''

---
La provincia anunció este jueves las nuevas medidas de convivencia que regirán en la provincia de Santa Fe a partir de la hora 0 del 9 de julio hasta el 23 de julio inclusive.

Las modificaciones se tomaron tras analizar la curva epidemiológica y aunque la meseta de contagios sigue siendo alta, según indicó la ministra Sonia Martorano, "hay pequeños índices que hablan de mejoría".

"Son dos los números que miramos de cerca: la incidencia y la ocupación de camas en el sistema de salud. La provincia está con un porcentaje de ocupación de camas del 83%. El ideal es 80% o menos. Es un muy buen índice que debemos sostenerlo con la baja de contagios”.

**Las nuevas autorizaciones en todo el territorio provincial:**

* En cuanto a los locales gastronómicos (comprendidos por bares, restaurantes, heladerías y otros autorizados a funcionar como tales, con concurrencia de comensales), deberán ajustarse a las siguientes disposiciones: los días viernes y sábados, podrán funcionar entre las 6 horas y las 23 horas. El resto de los días de la semana, entre las 6 horas y las 21 horas. Fuera de los horarios establecidos, sólo podrán realizar actividad en las modalidades de reparto a domicilio y de retiro, siempre que esta última se realice en locales de cercanía.


* Gimnasios, natatorios y establecimientos afines, por turnos y en grupos de hasta 10 participantes. Dichas actividades podrán realizarse entre las 7 y las 22 horas y en su desarrollo no podrá excederse el límite del treinta por ciento (30%) de ocupación de la superficie disponible.


* Actividad en hipódromos y agencias hípicas, organizando turnos para desarrollar las actividades de cuidado y entrenamiento de los animales y de mantenimiento de las instalaciones; sin la asistencia de espectadores, incluso si se desarrollaren carreras.


* Se autoriza en todo el territorio provincial la realización de actividades en cines, en teatros y en salas de espectáculos de centros culturales a los efectos del desarrollo de artes escénicas, con y sin asistencia de espectadores.

Para conocer el decreto completo ingresá [AQUÍ]()

Las autoridades municipales y comunales podrán disponer en sus respectivos distritos, en consulta con el Ministerio de Salud, mayores restricciones que las establecidas en el presente decreto.