---
category: Cultura
date: 2021-04-01T08:11:09-03:00
layout: Noticia con imagen
author: Prensa Municipalidad de Santa Fe
resumen: Propuestas para disfrutar la ciudad en Semana Santa
title: Propuestas para disfrutar la ciudad en Semana Santa
entradilla: La ciudad se prepara para recibir visitantes de distintos puntos del país
  durante el fin de semana largo. La agenda incluye propuestas turísticas, culturales
  y recreativas.
thumbnail: https://assets.3dnoticias.com.ar/turismo1.jpg
link_del_video: ''

---
Durante el fin de semana largo de Semana Santa, la ciudad de Santa Fe ofrecerá una variada agenda turística público-privada con opciones para disfrutar en familia o con amigos. Visitas guiadas gratuitas, circuitos religiosos, recorridos en bicicleta, astroturismo, paseos náuticos y hasta promociones gastronómicas son algunas de las propuestas.

El intendente Emilio Jatón destacó que “estamos ante un gran desafío porque hablamos de un fin de semana de mucha apertura turística y tenemos muy cerca la posibilidad de un segundo brote (de coronavirus). Vamos a necesitar que este fin de semana pongamos todo, desde el municipio pero también desde el sector privado”.

En ese sentido, el intendente destacó que la ciudad cuenta con el Sello de Destino Seguro del Consejo Mundial de Viajes y Turismo con el Sello de Destino, por el trabajo que se hizo en la elaboración de protocolos, pero aseguró que “es fundamental que tomemos el compromiso de poner lo mejor este fin de semana y luego volver a sentarnos para pensar juntos de aquí al futuro”. 

En cuanto a la oferta turística que ofrece la capital provincial, Jatón destacó la variedad de propuesta y la innovación en las actividades, siempre respetando los protocolos de cuidado vigentes. En ese sentido,  el intendente dijo que esto es posible gracias a “todos los que se sumaron a esta apuesta, a esta Santa Fe la construimos entre todos”. Habrá tres propuestas turísticas innovadoras con paseos guiados y gratuitos. 

El secretario de Producción y Desarrollo Económico, Matías Schmüth, resaltó la “amplia oferta” que habrá en la ciudad. “Las propuestas que pensamos desde la Municipalidad, que son gratuitas, son las relacionadas al programa Mi Ciudad como Turista que viene consolidándose y que permite que un montón de santafesinos conozcan la historia de los lugares que atraviesan cotidianamente y que sin lugar a dudas es un atractivo para la persona que viene a visitar la ciudad.  A esto se suma la oferta de las agencias de viajes y turismo receptivo, que son parte activa del Safetur”, explicó.

“Estamos muy ilusionados, creemos que habrá buena cantidad de turistas en la ciudad y tenemos que estar preparados para recibirlos y mostrar todas las bondades que tenemos a quienes vengan a visitarnos”, cerró el funcionario.

En la conferencia que se hizo en el Quincho de Chiquito también estaba presente el secretario de Educación y Cultura, Paulo Ricci. “Desde diciembre que venimos llevando adelante una política cultural pública descentralizada. En este último tiempo hemos reabierto y recuperado espacios como el Anfiteatro, el Museo Municipal de Artes Visuales Sor Josefa y hace apenas una semana el Mercado Progreso, que este fin de semana va brindar una oferta durante tres días (viernes, sábado y domingo). Habrá una gran variedad de propuestas, nuevos géneros, folklore, música en vivo todos los días, feria de emprendedoras; y el domingo vamos a dedicar la programación exclusivamente a las infancias con teatro y música para los más chicos”, destacó Ricci. 

**Novedades**

El 2 de abril, que además corresponde al feriado que honra a los Veteranos y Caídos en la Guerra de Malvinas, se realizará el “Bicitour de la Soberanía y el Patriotismo”, a las 17. El punto de encuentro será el Molino Franchino (bulevar Gálvez y Pedro Vittori) y la idea es recorrer, utilizando la ciclovía que allí se ubica, el camino hasta el espacio cultural La Redonda. A lo largo del recorrido se podrá conocer y aprender más sobre la historia de la Santa Fe industrial y ferroviaria. Además habrá una visita de sitio al museo que se encuentra en el Centro de Ex Soldados Combatientes de Malvinas, ubicado en el Parque Federal. Para realizar este paseo es necesario contar con bicicleta. El personal a cargo podrá ser identificado con las sombrillas verdes.

Otra de las propuestas innovadoras es el paseo “Astroturismo: el cielo de Buenaventura Suarez”. La idea es recorrer las terrazas del Colegio Inmaculada, en la Manzana Jesuítica (San Martín 1540) y avistar el firmamento con telescopios. Esta iniciativa es posible gracias al Centro de Observadores del Espacio (CODE) y se llevará a cabo el sábado 3, desde las 19.30.

También el sábado 3, pero desde las 10, se realizará el “Circuito de arte religioso”, que invita a recorrer las iglesias coloniales de la ciudad para descubrir las obras de arte que allí se resguardan. El punto de encuentro será la Plaza 25 de Mayo, frente a la Catedral Metropolitana (General López y San Jerónimo). El personal a cargo podrá ser identificado con las sombrillas verdes.

![](https://assets.3dnoticias.com.ar/turismo.jpg)

**Manzana Jesuítica y paseos gratuitos**

La oferta turística de la ciudad, cuenta también con la serie de paseos guiados y espacios turísticos que se pueden visitar de manera gratuita.

Durante Semana Santa, la Manzana Jesuítica y el Paseo del Papa Francisco tendrán horarios especiales: todos los días de 9.30 a 13 y de 16 a 19.30. Las visitas guiadas serán a las 10, 11.30, 16.30 y 18 horas.

El recorrido de la Manzana Jesuítica propone remontarse hasta los orígenes de la ciudad colonial, luego aprender sobre su rol fundamental durante la etapa de organización nacional en la Confederación Argentina, hasta llegar a 1960, cuando Jorge Bergoglio se desarrolló como Maestrillo en el Colegio Inmaculada Concepción, cumpliendo una de las etapas en la formación jesuita para, finalmente, llegar al presente como Institución educativa.

Vale destacar que el histórico solar fue postulado por la Municipalidad para integrar el Camino de los Jesuitas, un producto turístico que une puntos del país como Córdoba y Misiones con Paraguay y Brasil, es decir, todos aquellos lugares donde el trabajo de la orden Compañía de Jesús se puede vislumbrar en instituciones, edificios, tradiciones y costumbres.

Además, dentro del programa “Mi ciudad como turista”, se ofrecerán dos paseos guiados: el jueves 1°, a las 18 horas, se realizará la versión reducida del Camino de la Constitución, que recorre espacios fundamentales de la ciudad en la gesta constitucionalista de 1853 y las sucesivas reformas; el punto de encuentro será la Plaza 25 de Mayo. En tanto el viernes 2 será el turno del Paseo Boulevard, también a las 18, desde la plaza Pueyrredón (bulevar Gálvez al 1600). En ambas propuestas el personal encargado de guiar el recorrido podrá ser identificado por las sombrillas verdes.

Vale destacar que todas las propuestas de Mi ciudad como turista se pueden realizar de forma autónoma, utilizando las audioguías elaboradas por la Municipalidad que están disponibles en el sitio web del municipio o en el perfil de Spotify “Santa Fe Capital”.

**Museos, teatro y feria**

A la oferta de paseos se suman los museos, cuyo protocolo exige a los residentes de la ciudad reservar turnos. En el caso particular de las personas que no residen en Santa Fe, sólo deberán presentarse en la puerta del museo que les interese visitar con el DNI donde acredite que no viven en la capital provincial.

Durante Semana Santa se podrá visitar el Museo Municipal de Artes Visuales Sor Josefa Díaz y Clucellas (San Martín 2068) donde se exhibe la muestra “Remontar un río. Salones Litoral y Modernidad (1940-1980)”. Los días y horarios son: jueves 1°, de 10.30 a 12.30 y de 18 a 20 horas; viernes 2, sábado 3 y domingo 4, de 18 a 20 horas.

También se podrá recorrer la muestra de sitio del Museo de la Jesuitas, ubicado en la Manzana Jesuítica (San Martín 1540), el jueves 1°, de  9 a 12 y 17 a 20 horas; viernes 2, sábado 3 y domingo 4, de 17 a 20 horas.

Finalmente, el Museo de la Constitución abrirá sus puertas, el jueves, de 9 a 13 y de 15 a 19 horas, mientras que el viernes, sábado y domingo se podrá visitar de 15 a 19 horas.

En tanto, en el Teatro Municipal 1° de Mayo (San Martín 2020) se podrá disfrutar del recorrido guiado por títeres “Una aventura tras bambalinas”. Será sábado 3 y domingo 4 a las 19, 20 y a las 21 horas. Vale aclarar que las visitas se realizan por cupos, por lo que se debe reservar lugar retirando entradas gratuitas en las boleterías del Teatro, de lunes a sábados de 9 a 13 y de 17 a 21 horas, y los domingos, de 10 a 12 y de 17 a 21.

Finalmente, para quienes disfrutan de las exposiciones de artesanías, la clásica Feria del Sol y de la Luna, en Plaza Pueyrredón (bulevar Gálvez 1600) funcionará el viernes 2, el sábado 3 y el domingo 4, desde las 17 horas.

**Más opciones para disfrutar**

La oferta de la ciudad se potencia con las propuestas de los espacios gastronómicos, las agencias de turismo receptivo y los prestadores de servicios.

Durante Semana Santa habrá diferentes propuestas comercializadas por el sector privado que van desde promociones en productos típicos gastronómicos como picadas y lisos santafesinos hasta el tour cervecero, y desde paseos náuticos hasta recorridos por los clubes de la ciudad.

Las actividades con más demanda son los paseos náuticos. Durante el fin de semana largo volverá a navegar La Ribereña, una propuesta que permite disfrutar del paisaje isleño que rodea a la ciudad y conocer las historias de los barrios costeros a través de los relatos de sus propios habitantes. La Ribereña es comercializada por el Pool de Agencias de Turismo Receptivo y se puede contratar el servicio a través del mail receptivosantafe@gmail.com. También, la empresa Costanera 241 - Mirá Santa Fe, ofrece opciones para recorrer la zona de islas.

Además de las propuestas de naturaleza habrá city tours, visitas guiadas a las instalaciones del Club Atlético Colón (Sabaleros tour) y recorridos por la Santa Fe Cervecera. El jueves, en comercios adheridos a la Asociación Empresaria Hotelera Gastronómica de Santa Fe, habrá promociones de lisos y picada para turistas.

**Centros de informes**

La Municipalidad cuenta con Centros de Información Turística ubicados en puntos estratégicos de la ciudad. Durante el fin de semana largo funcionarán con horarios especiales:

El Centro ubicado en Terminal de Ómnibus (Belgrano 2910) funcionará todos los días de 8 a 20 horas

El Centro de Informes ubicado en la Cabecera del Dique 1 del Puerto abrirá jueves y viernes de 12 a 18 horas, el sábado de 10 a 18 y el domingo de 9 a 13.

El Centro de Informes ubicado en la peatonal San Martín (San Martín y Juan de Garay) funcionará, desde el jueves 1 de abril hasta el sábado 3, de 9 a 12.30 y de 16 a 20.