---
category: Cultura
date: 2021-04-07T08:05:56-03:00
layout: Noticia con imagen
author: Telam
resumen: Dos concursos de fotografía y literatura buscan contar la pandemia
title: Dos concursos de fotografía y literatura buscan contar la pandemia
entradilla: Un concurso de fotografía y video busca registrar esa experiencia, mientras
  que otro certamen se propone construir otras lógicas por fuera del canon al convocar
  a autores y autoras Lgtbi+.
thumbnail: https://assets.3dnoticias.com.ar/concurso.jpeg
link_del_video: ''

---
Con premios que van de los 75.000 a los 300.000 pesos, se lanzó la convocatoria del concurso de fotografía y video para jóvenes artistas de todo el país, hasta el próximo 14 de mayo con la intención de generar un registro artístico de la pandemia desde las miradas de generaciones que van de los 18 a los 35 años.

Bajo el título "Imágenes de una nueva normalidad", el certamen organizado por la Fundación Bunge y Born está destinado a artistas, fotógrafos, creadores y estudiantes de carreras afines de todo el país para que "expresen su mirada sobre el contexto actual a partir de la pandemia: el encierro y la reconfiguración de la vida, los nuevos modos de vincularse, el trabajo, la escuela, y el valor de la solidaridad y el trabajo colaborativo".

Dividido en dos categorías -fotografía y video-, el concurso otorgará un primer premio de 300.000 pesos, uno segundo de 200.000 y un tercero de 150.000. Además, habrá tres menciones de honor de 75.000, y cuatro clases de mentoría brindadas por una persona especializada en cada categoría.

Los responsables de seleccionar y premiar, en el caso de Fotografía, son Fabiana Barreda, Matilde Marín y Santiago Porter, mientras que en la categoría video el jurado está compuesto por Florencia Levi, Ana Claudia García y Jorge La Ferla. Para inscripciones se debe completar este formulario: [http://bit.ly/FBB-Imagenes.](http://bit.ly/FBB-Imagenes. "http://bit.ly/FBB-Imagenes.")

Editorial y librería se unen en proyecto de publicación para autores Lgtbi+

La librería y editorial Liberteca lanza un proyecto para quienes estén buscando publicar su primer libro u ofrecer el que ya editaron, orientado a escritoras y escritores Lgtbi+. La iniciativa surge de la premisa de que el 70 por ciento de los libros publicados anualmente por la industria editorial son "de autores varones, cis, blancos, hétero", según el relevamiento del espacio.

Con el objetivo de "ayudar a equilibrar e incluso subvertir esas normas", Liberteca abre un espacio para comercializar los libros en formato digital, contando con distintas posibilidades de adquisición a través de la página (descarga gratuita, donación o pago del importe completo) porque la idea es que las autoras y los autores puedan ver una devolución económica a cambio de su trabajo.

Aquellas personas interesadas pueden escribir a liberteca.argentina@gmail.com.