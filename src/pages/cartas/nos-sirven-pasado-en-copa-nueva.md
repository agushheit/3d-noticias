---
layout: carta
category: lectores
date: 2020-10-23T16:37:10.235Z
author: José Batt
title: Nos sirven pasado... en copa nueva
entradilla: "Tanto en lo político como en lo económico venimos transitando
  tiempos complejos: un tipo de cambio que llega hasta valores históricos, un
  parate económico que nos lleva a repetir el PBI de 1994 con casi la mitad de
  la población bajo la línea de pobreza y un aumento de la desigualdad económica
  entre el 5% más rico con el 5% más pobre, que llega al máximo en los últimos
  35 años."
thumbnail: https://assets.3dnoticias.com.ar/lectores.jpg
---
Además, a este sombrío panorama social se agregan algunas organizaciones o colectivos que suman más incertidumbre a un gobierno acorralado.Tenemos un movimiento que usurpa terrenos en el gran Buenos Aires -nos referimos a Guernica-, al que ya han ordenado desalojar, mientras el gobierno de la provincia juega un dudoso galimatías con organizaciones de ultra izquierda (MST / PO ) para evitar el desalojo. Tenemos otro movimiento que asume el ropaje de una supuesta “reivindicación” histórica de los pueblos originarios tomando tierras privadas, del Ejército Argentino y parte de un Parque nacional. Envueltos de la pátina chamánica, ya que una “machi” indicó que el lugar que le dictaban sus alucinaciones son esas tierras, desconociendo las leyes argentinas y su Constitución Nacional.

Ahora, ¿qué fino hilo conjuga estas tomas que se repiten a lo largo del país?

Podemos repasar algunos nombres: el de Fernando Vaca Narvaja junto a la toma de tierras por parte de los mapuches y el de Roberto Perdia, liderando desde las sombras de Guernica, en asociación con los movimientos de ultra izquierda, quienes protestaron hasta con un mortero cuando se aprobó la fórmula de actualización previsional.

Como tercera figura en esto de las usurpaciones aparece Juan Grabois, un católico ortodoxo ultramontano y antidemocrático, que dice ser vocero de Bergoglio.

Dos cosas en común tienen estos movimientos o colectivos:

Primero, en la teoría de foquismo guevarista. El propio Che teorizaba respecto de empezar con focos de insurrección civil, preferentemente en lugares de acceso difícil para las fuerzas de seguridad. En la teoría guevarista estos focos revolucionarios van preparando el campo para llegar a las grandes ciudades con acciones armadas que vayan preparando el clima revolucionario. Durante los años 70, el terrorismo armado de Montoneros y el ERP discutieron sobre los focos y así fue cómo el ERP fue acorralado y todos sus hombres muertos en la selva tucumana. Montoneros intentó un foco en Formosa y fue repelido por 20 calimbas un sábado de 1975. El propio Perdia se enorgulleció hace poco de la logística fenomenal que organizaron para la implantación de ese foco.

Lo segundo es que jamás se escuchó una autocrítica y la disposición de estos criminales de jugar con las reglas de la democracia en una sociedad abierta como la que construimos desde 1983. Todo lo contrario, cada vez que muestran sus caras son repudiados por las urnas y solo les queda imponer la violencia y hacerle la vida imposible a un gobierno que está acorralado.

En fin, nos sirven ideas de un pasado de 50 años… pero, eso sí, lo disfrazan en una "Kopa" nueva.