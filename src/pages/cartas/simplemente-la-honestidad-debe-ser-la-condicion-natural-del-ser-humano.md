---
layout: carta
category: lectores
date: 2021-07-13T13:40:45-03:00
author: Nicolás Rabosto
title: "“Simplemente la honestidad debe ser la condición natural del ser humano”"
entradilla: A 30 años del fallecimiento de Enrique Muttis, es indispensable recordarlo
  desde ciertos principios comunes- en los que miles de santafesinos se reflejan-;
  valores los cuales se vieron ejemplificados en lo que éste gran Intendente supo
  hacer en su trayectoria de vida.
thumbnail: https://assets.3dnoticias.com.ar/muttis.jpg

---
Este santafesino por elección, nacido en Elortondo un 4 de mayo de 1934, encontró en la capital provincial el lugar indicado para desarrollar su joven militancia demócrata-progresista y su inclaudicable tarea docente -que supo complementar a la perfección con su labor periodística- como Profesor de Historia.

Para la sociedad santafesina, y especialmente para los dedicados a la actividad política, es imprescindible contar con estos testimonios de vida, marcados por conductas ejemplares que deben ser el faro hacia una política local austera y fundamentalmente reconciliadora entre los vecinos y las instituciones de la Ciudad. 

En su paso por la gestión pública municipal, se destacó por ser la contracara de un modelo agotado por la corrupción que lideraba su antecesor Carlos Aurelio Martínez, quien había sido destituido por sus irregularidades comandando el Ejecutivo Municipal.

Este aire de transparencia dejó una enseñanza –casi docente- de lo que debe ser el compromiso ciudadano, el gobierno austero y la importancia de contar con cuentas públicas sanas y responsables. La gestión del demócrata progresista se caracterizo por sobreponerse a las adversidades, incluso sin apoyo de los gobiernos provincial y nacional de tintes peronistas, con un férreo Concejo dominado por el Justicialismo. Enrique Muttis demostró que es posible ser buena persona y practicar la política partidaria incluso desde la gestión.

A pesar del difícil contexto, aquel famoso profesor -reconocido por su tarea en Notitrece/Canal 13 desde 1966- supo sanear las finanzas municipales, a tal punto que al año de gestión, los empleados cobraron sueldos y aguinaldos: una cuestión inimaginable luego de tantos años de crisis para las arcas santafesinas.

Desde mi prematuro interés por las campañas políticas, conservo pegado en el placard un folleto con la famosa insignia “Simplemente la honestidad debe ser la condición natural del ser humano”, que hasta el día de hoy es bandera de algunos miembros del mítico partido centenario “PDP” y, a 30 años del fallecimiento de este santafesino destacado, desde Santa Fe en Red aspiramos a que se convierta en un estandarte de la sociedad santafesina en general.  

**Nicolás Rabosto**

_Referente Santa Fe en Red_

nicorabosto@gmail.com

DNI 41359277