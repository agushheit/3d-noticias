import React from 'react'
import { useDispatch } from 'react-redux'
import styled from 'styled-components'

import PageBase from '~src/components/PageBase'
import creditos from '~src/constants/creditos'
import { getWeatherData } from '~src/constants/state'

const Container = styled.div`
  margin: 50px 10%;
`

const PrivacidadPage = () => {
  const dispatch = useDispatch()
  dispatch(getWeatherData())
  creditos()
  return (
    <PageBase>
      <Container>
        <h1>Políticas de Privacidad</h1>
        <p>
          LEA ESTO CON DETENIMIENTO. Este documento indica cómo
          <b> 3dnoticias.com.ar </b>
          utilizará y protegerá sus datos personales mientras navega en
          este sitio Web, usted automáticamente acepta las normas de uso, protección y
          seguridad aquí establecidas.
        </p>

        <h2>Seguridad y Protección de sus Datos Personales</h2>
        <p>
          La seguridad de los datos personales es una prioridad para
          <b> 3dnoticias.com.ar</b>
          . Este sitio web se esfuerza por ofrecer el más alto nivel de seguridad para lo cual
          se utiliza tecnología de avanzada. Adherimos a los requerimientos de la Ley
          Nacional de Protección de Datos Personales N° 25.326 y sus normas
          complementarias.
        </p>

        <h2>Su privacidad</h2>
        <p>
          <b>3dnoticias.com.ar </b>
          respeta su privacidad. Toda la información que usted nos
          proporcione se tratará con sumo cuidado y con la mayor seguridad posible, y sólo
          se utilizará de acuerdo con los límites establecidos en este documento.
        </p>
        <p>
          Cómo reúne la información
          <b> 3dnoticias.com.ar</b>
          : solamente reúne sus datos
          personales cuando usted los proporciona en forma directa y con su
          consentimiento expreso e informado.
        </p>
        <p>
          <b> 3dnoticias.com.ar </b>
          utiliza la información que usted proporciona para: expandir
          ofertas de comercialización y para publicar productos y servicios que podrían ser
          de su interés, para personalizar y mejorar nuestros servicios y para fines
          estadísticos de
          <b> 3dnoticias.com.ar</b>
          .
        </p>

        <h2>¿Quién tiene acceso a la información?</h2>
        <p>
          <b>3dnoticias.com.ar </b>
          siempre está comprometido a presentar nuevas soluciones
          que mejoren el valor de sus productos y servicios. La información no identificable
          y estadística también podrá ser compartida con socios comerciales.
        </p>

        <h2>Seguridad y Protección de sus Datos Personales</h2>
        <p>
          La seguridad de los datos personales es una prioridad para 3dnoticias.com.ar.
          Este sitio web se esfuerza por ofrecer el más alto nivel de seguridad para lo cual
          se utiliza tecnología de avanzada. Adherimos a los requerimientos de la Ley
          Nacional de Protección de Datos Personales, N° 25.326 y sus normas
          complementarias.
        </p>
      </Container>
    </PageBase>
  )
}

export default PrivacidadPage
