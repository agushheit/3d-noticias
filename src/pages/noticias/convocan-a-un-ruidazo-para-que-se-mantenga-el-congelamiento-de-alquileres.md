---
category: Agenda Ciudadana
date: 2021-03-17T05:15:38-03:00
thumbnail: https://assets.3dnoticias.com.ar/alquiler.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de LT10
resumen: Convocan a un ruidazo para que se mantenga el congelamiento de alquileres
title: Convocan a un ruidazo para que se mantenga el congelamiento de alquileres
entradilla: Será el 29 de marzo. Desde la Federación de Inquilinos piden que se extienda
  el congelamiento y la suspensión de desalojos. Además, un plan de desendeudamiento
  y el cumplimiento de la nueva ley de alquileres.

---
El 31 de marzo vence el decreto nacional nº 320 que establece el congelamiento de los alquileres y la suspensión de desalojos. Esta situación complica a los inquilinos que deben afrontar precios “desmesurados” en los arrendamientos.

Además, denuncian que no se cumple la nueva ley de alquileres sancionada en año pasado. Es por eso que, previo al vencimiento de la disposición nacional, el 29 de marzo a las 20  las asociaciones de inquilinos convocan a un ruidazo.

En diálogo con LT10, Sebastián Artola referente de la Federación de Inquilinos Nacional en la provincia de Santa Fe, explicó que se trata de una manifestación “contra los abusos del mercado inmobiliario” y planteando tres necesidades “urgentes” que tienen que ver con la extensión del decreto nº320. Además piden  “un plan de desendeudamiento que permita a los inquilinos ponerse al día con el pago del alquiler” y por el cumplimiento de la nueva ley de alquileres.

En este sentido, expresó que “la realidad de los inquilinos en nuestro país viene siendo muy difícil desde hace años” pero con la pandemia quedó en evidencia la realidad del difícil acceso a la vivienda y los altos costos que deben afrontar quienes deben alquilar, frente a la indiferencia del mercado inmobiliario.