---
category: Agenda Ciudadana
date: 2021-12-24T06:00:41-03:00
thumbnail: https://assets.3dnoticias.com.ar/bono.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: Kicillof anunció un bono de 20 mil pesos para trabajadores de la administración
  pública provincial
title: Kicillof anunció un bono de 20 mil pesos para trabajadores de la administración
  pública provincial
entradilla: El bono también alcanzará a docentes, personal de la policía y servicio
  penitenciario y de carrera hospitalaria.

---
En línea con el beneficio que otorgará este mes el Gobierno nacional para empleados estatales, el gobernador bonaerense, Axel Kicillof, anunció hoy un bono de 20.000 pesos para trabajadores de la administración pública provincial, docentes, personal de la policía y servicio penitenciario y de carrera hospitalaria.

El anuncio tuvo lugar de la reunión que mantuvo en Casa de Gobierno con autoridades de los gremios estatales bonaerenses, en el marco de un encuentro en el que también participaron el ministro de Hacienda y Finanzas, Pablo López; y su par de Trabajo, Mara Ruiz Malec.

"El objetivo de esta medida es aprovechar la mejora que se observa en la recaudación para distribuir los resultados del crecimiento económico con todos los trabajadores”, fundamentó Kicillof.

El mandatario kirchnerista destacó que el esfuerzo fiscal para los trabajadores estatales fue "posible gracias a que al término de este año ya estaremos recuperando lo perdido durante la pandemia, que es una situación excepcional".

El bono extraordinario es un concepto no remunerativo en un pago único que se depositará en las cuentas de los beneficiarios antes de que concluya diciembre.

"Nuestro programa es ir recuperando en la medida de lo posible todo lo que se deterioró el ingreso durante los cuatro años anteriores", afirmó Kicillof, y al respecto agregó: “Nos comprometimos a que los salarios le iban a ganar a la inflación y, con el cierre de la paritaria en un promedio del 54%, este año se estarían recuperando más de cuatro puntos de salario real".

Por su parte, Ruiz Malec explicó que "después de una etapa en la que se perdió un 25% de salario real, empezamos un periodo de recuperación aún en un contexto muy difícil para todos"

Y continuó: "Tras cerrar la paritaria, este bono busca aliviar los bolsillos y reconocer el inmenso esfuerzo realizado por todos y todas las trabajadoras en estos dos años de pandemia".

Entre los gremialistas presentes estuvo el titular del Sindicato Unificado de Trabajadores de la Educación, Roberto Baradel, quien aseguró: "Estamos orgullosos de los trabajadores de todas las áreas del Estado por el inmenso esfuerzo que hicieron durante la pandemia". "

Además participaron de la reunión por parte del movimiento obrero: los secretarios generales provinciales de ATE, Oscar de Isasi; y de UPCN, Carlos Quintana; los titulares de la Unión de Docentes de la Provincia de Buenos Aires (UDOCBA), Alejandro Salcedo; de la Asociación del Personal de los Organismos de Control (APOC), Pedro Fernández; del Sindicato de Salud Pública (SSP), Miguel Ángel Zubieta; de la Federación de Educadores Bonaerenses (FEB), Mirta Petrocini; del Sindicato Argentino de Docentes Privados (SADOP), María Inés Busso; de la Unión de Docentes Argentinos (UDA), Edith Contreras; de ATE Ensenada, Francisco Banegas; de la Asociación Sindical de Profesionales de la Salud de la Provincia (CICOP), Pablo Maciel; de la Asociación Judicial Bonaerense (AJB), Pablo Abramovich; de la Asociación de Empleados de Casinos Nacionales (AECN), Marcos Labrador; de la Asociación Gremial de Administración, Maestranza y Servicios de Casinos (AMS), Anibal Settino; el secretario de Capacitación y de Médicos de Reciente Graduación de AMRA, Jorge Ouviña; el secretario adjunto de la Asociación del Magisterio de Enseñanza Técnica (AMET), Ariel Guede; y el interventor del Sindicato de Obreros de la Minoridad y Educación (SOEME), Julio César Simón.