---
category: La Ciudad
date: 2021-03-16T07:27:54-03:00
thumbnail: https://assets.3dnoticias.com.ar/cantero.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: "“Alicia” Cantero en el País de las Maravillas⁉"
title: "“Alicia” Cantero en el País de las Maravillas⁉"
entradilla: Padres ORGANIZADOS responde a la Ministra Adriana Cantero sobre sus dichos
  en nota periodística

---
La agrupación Padres Organizados Santa Fe le respondió a la Ministra Adriana Cantero en una carta abierta.

La ministra fue entrevistada por un medio colega el pasado domingo 14 de marzo y la agrupación de padres no tardó en responder. El comunicado realiza el siguiente punteo:

"✔Se sorprende que los docentes no acepten los sueldos como profesionales de la educación a un día del comienzo de clases… PODRIAN HABER PRESENTADO UNA PROPUESTA ANTES? ¿Esa negociación no se podría haber llevado a cabo en el mes de Febrero? De estar tan interesados en la educación mejorarían las propuestas.

✔ Los relevamientos sobre capacidad e infraestructura de los establecimientos escolares estatales están desactualizados en muchos casos, y en otros casos se hicieron tarde.

✔ “A lo mejor hay algunos a los que todavía le restan arreglos, sobre los que nos comprometemos a seguir invirtiendo…” queremos saber concretamente cuantos establecimientos NO llegaron con las condiciones edilicias para dar clases.

✔Faltan más que arreglos para garantizar la educación y los derechos de todos los NNyA 

✔El Ministerio está atrasado en su gestión sobre organización escolar y comunicó dos semanas antes del comienzo del ciclo una resolución general a las escuelas.

✔No tocar los protocolos es un capricho político que carece de fundamentos científicos y no responde a una cuestión sanitaria discriminando la actividad en el ámbito escolar del resto de las actividades escudándose en “presencialidad cuidada”. NO MENCIONA QUE DATAN DE JULIO 2020, fueron ratificados por CFE pero no ACTUALIZADOS como propone la norma por jurisdicción.

✔Con esta metodología es imposible garantizar los 180 días de clases para TODOS los NNyA.

Las cuentas de la Ministra no coinciden con la realidad. Muy pocos NNyA están accediendo a la presencialidad 100% y los que no tienen acceso ni siquiera están siendo efectivamente asistidos para lograrlo.

✔No acepta que está comprobado científicamente que los NNyA no son el principal factor de riesgo y sus actividades no modificaron la curva epidemiológica

✔No puede garantizar reemplazos por cada docente frente a un grupo por licencias por Covid

✔Rechaza por ideología política la opinión científica. Investiga siempre donde han estudiado quienes emiten la opinión científica??? Por qué no analiza el contenido? … “pertenecientes en su mayoría a las facultades de Farmacia y Bioquímica de la Universidad de Rosario, y entre los que se encuentran también algunos exfuncionarios de la gestión anterior”. Desacredita caprichosamente todo aporte académico como por ejemplo la grilla de horarios de 1:30 minutos de clases cuya ineficacia está asegurada por diferentes profesionales y especialistas 

✔Sigue en una postura cerrada y hasta siniestra, carente de flexibilidad para llegar a la presencialidad completa de TODOS LOS NNyA

✔Parece que los restaurantes, clubes y shopping no son un precedente y antecedente para abrir los comedores escolares y cantinas y sigue con entrega de raciones

✔Hay soluciones prácticas que ni considera como que cada NNyA tenga su propio sanitizante para agilizar ingresos y egresos

✔Mantiene una postura radicalmente política ni sanitaria ni pedagógica"

La entrevista cuestionada: [https://www.ellitoral.com/index.php/id_um/287443-cantero-lamento-el-paro-deamsafe-es-una-de-las-mejores-propuestas-del-pais-entrevista-con-la-ministrade-educacion-educacion-entrevista.html](https://www.ellitoral.com/index.php/id_um/287443-cantero-lamento-el-paro-deamsafe-es-una-de-las-mejores-propuestas-del-pais-entrevista-con-la-ministrade-educacion-educacion-entrevista.html "https://www.ellitoral.com/index.php/id_um/287443-cantero-lamento-el-paro-deamsafe-es-una-de-las-mejores-propuestas-del-pais-entrevista-con-la-ministrade-educacion-educacion-entrevista.html")