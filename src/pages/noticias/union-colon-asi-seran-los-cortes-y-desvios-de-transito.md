---
category: Deportes
date: 2021-12-10T06:15:21-03:00
thumbnail: https://assets.3dnoticias.com.ar/SUPERCLASICO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'Unión - Colón: así serán los cortes y desvíos de tránsito'
title: 'Unión - Colón: así serán los cortes y desvíos de tránsito'
entradilla: Las interrupciones en la circulación comenzarán el sábado a las 12 y finalizarán
  entre las 21 y 22 horas. Algunos cortes se levantarán al finalizar el primer tiempo

---
Para el clásico santafesino entre Unión y Colón, que jugarán este sábado a las 17 en el estadio "15 de Abril", se apronta un gran operativo de control, que empezará a las 12 y finalizará entre las 21 y 22 horas. 

 La División de Operaciones de la URI diagramó, en conjunto con la Municipalidad, el mapa para establecer las calles que estarán restringidas a la circulación vial en la previa, durante y después del encuentro.

 Desde San Jerónimo y Bulevar Pellegrini, hacia el estadio de Unión, el tránsito permanecerá bloqueado al menos hasta la finalización del primer tiempo. Lo mismo sucederá desde la intersección de calle Suipacha y Av. Freyre, hacia el 15 de Abril. En el sector norte al estadio, los operativos iniciarán desde Pasaje Irala, Pasaje Maipú y también se bloqueará Mariano Comas y Urquiza. 

 Mientras que las calles circundantes y próximas al estadio de Unión estarán restringidas durante el tiempo que dure el operativo. 

 **¿Qué pasará con la previa?**

 En virtud del reclamo de vecinos de la zona de Bulevar Pellegrini y 1° de Mayo, punto de encuentro de los hinchas de Unión previo a cada partido, que dio a conocer El Litoral esta semana, se consultó al municipio qué acciones de prevención llevarán adelante. 

 "Desde hace varios partidos que se está tratando de resolver el movimiento de los hinchas alrededor de los estadios de Santa Fe. Hay un mayor control sobre la venta de alcohol en comercios pero también es cierto que muchos hinchas de se llevan sus propias bebidas", indicaron fuentes municipales. 