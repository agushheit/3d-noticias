---
category: Agenda Ciudadana
date: 2022-01-17T06:00:31-03:00
thumbnail: https://assets.3dnoticias.com.ar/marga.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: Stolbizer reclamó "ordenar el funcionamiento interno" de JxC y la inclusión
  del GEN y Manes en la Mesa Nacional
title: Stolbizer reclamó "ordenar el funcionamiento interno" de JxC y la inclusión
  del GEN y Manes en la Mesa Nacional
entradilla: '"En la provincia de Buenos Aires nos fue bien porque hubo un esquema
  de ampliación", subrayó la diputada nacional.

  '

---
La diputada de Juntos por el Cambio Margarita Stolbizer solicitó "ordenar el funcionamiento interno" del frente opositor y, en ese marco, manifestó la necesidad de incluir en la Mesa Nacional del espacio a su partido, el GEN, y a su colega del radicalismo Facundo Manes.

"Se hace necesario ordenar el funcionamiento interno, no hay un grupo de Whatsapp del interbloque de diputados de Juntos por el Cambio, no hay comunicación interna entre los presidentes de los bloques del espacio, no hubo reuniones previas a las sesiones con los nuevos miembros del bloque que asumieron en diciembre", se quejó la dirigente.

En declaraciones a NA, Stolbizer insistió: "Ingresamos muchos diputados nuevos (tras las elecciones de noviembre) y hoy el interbloque tiene una composición diferente a la de antes".

En ese marco, la legisladora dijo que "el desorden del interbloque" se traslada también a la composición de la Mesa Nacional de Juntos por el Cambio, que se suele reunir los lunes de manera virtual para debatir los temas importantes de la coyuntura.

Al respecto, señaló: "El GEN es un partido nacional y no logramos que nos incorporen a la Mesa de Juntos por el Cambio. En la provincia de Buenos Aires nos fue bien en las elecciones porque hubo un esquema de ampliación con la llegada de Facundo Manes, pero eso no está volcado en la conformación de la Mesa Nacional. Manes no tiene lugar en la Mesa y yo me pregunto de dónde surge la representación de cada integrante".

"Es como si las últimas elecciones no hubieran sucedido y eso que fue positiva porque ganamos", señaló Stolbizer a esta agencia. En ese sentido, la diputada sostuvo que el ordenamiento interno que pide servirá también para definir "una estrategia común en un año complejo, y en el que hay expectativas grandes de un sector de la sociedad que acompaño con los votos a JxC".

Stolbizer remarcó que "la falta de debate interno hace que cada uno haga las cosas a su manera", por lo que es necesario "definir en conjunto las reglas sino cada cual conversa con quien quiere".

Consultada sobre cómo ve el acercamiento de la titular del PRO, Patricia Bullrich, al diputado de La Libertad Avanza Javir Milei, manifestó: "Ese es otro resultado de este desorden del que hablo". Sobre si considera que el economista libertario debería estar dentro de JxC en las elecciones de 2023, respondió: "Para mí, no".

En tanto, minimizó los cortocircuitos internos que se vinieron expresando en las últimas semanas en la coalición opositora: "No me preocupa, son tensiones razonables y no hay que dramatizar", evaluó.

Sobre el perfil que viene exhibiendo el presidente de la UCR y gobernador jujeño, Gerardo Morales, diferenciándose de los dirigentes más "duros" de la coalición, Stolbizer dijo que "es razonable su posición" y que comparte varias "posturas", pero aclaró que "tampoco se trata que lo que él diga es lo que todos tienen que seguir" dentro de JxC.

Acerca de la chance de que Cristian Ritondo sea el próximo jefe del interbloque, la dirigente sostuvo: "Ese tema lo resuelven los bloques grandes, nosotros no tenemos incidencia. Generalmente lo ponía el radicalismo".

Sobre el sorteo que hizo Milei de su dieta, la titular del GEN expresó: "No me gusta, soy una representante de un política más tradicional. Los legisladores tienen que tener su salario, lo dispone así la Constitución nacional. Es un derecho y una obligación".

"Después si cada uno hace una donación... yo una vez que se dispuso un aumento de la dieta con el que no estaba de acuerdo, comencé a donar ese aumento", completó.

De la polémica en torno a la titular del PAMI, Luana Volnovich, quien veraneó en el caribe mexicano, Stolbizer dijo que "no hay que detenerse en el lugar donde va de vacaciones", sino que "el debate que hay que dar es el de los privilegios de los sectores de poder en relación al resto de las personas y la brecha cada vez más grande entre los representantes y sus representados".