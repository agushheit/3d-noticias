---
category: Estado Real
date: 2021-03-24T07:40:17-03:00
thumbnail: https://assets.3dnoticias.com.ar/memoria.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: " La Secretaría de Derechos Humanos y Diversidad instaló la señalización
  del Espacio de Memoria Ex Comisaría Cuarta"
title: " La Secretaría de Derechos Humanos y Diversidad instaló la señalización del
  Espacio de Memoria Ex Comisaría Cuarta"
entradilla: El acto fue encabezado por el ministro Sukerman y la secretaria Lucila
  Puyol.

---
El ministro de Gobierno, Justicia, Derechos Humanos y Diversidad, Roberto Sukerman, y la secretaria de Derechos Humanos de la provincia, Lucila Puyol, encabezaron este martes el acto de señalización del Espacio de Memoria Ex Comisaría Cuarta. La actividad contó con la presencia de organizaciones de DDHH, ex presos políticos y familiares.

En la oportunidad, también, se realizó una intervención artística y se procedió a la plantación de árboles en el patio, cumpliendo con la consigna de Abuelas de Plaza de Mayo, “Plantamos Memoria”.

Al respecto, Sukerman destacó que “es una decisión del gobernador Omar Perotti jerarquizar un lugar simbólico, con el claro objetivo de mantener viva la Memoria”; y detalló que “estamos trabajando para poder reforzar las tareas del personal especializado en la clasificación de los documentos encontrados y mantenerlos como corresponde. En los próximos días se van a entregar los legajos encontrados a personas que estuvieron detenidas en el lugar”, precisó.

Por su parte, Puyol manifestó que “este es un sitio emblemático para esta ciudad y para el centro-norte de la provincia, donde estuvieron muchas víctimas, mujeres y varones, durante la última dictadura cívico-militar”; al tiempo que recordó que “era un reclamo permanente del organismo de Derechos Humanos y de la Comisión del ex Servicio de Informaciones el traspaso de este sitio, y finalmente el año pasado el gobernador Omar Perotti firmó el decreto de traspaso del Ministerio de Seguridad al Ministerio de Gobierno, para que la Secretaría de Derechos Humanos trabajara en este sitio”.

“Aquí funciona el Archivo Provincial de la Memoria”, indicó la secretaria; y precisó que “entre la documentación valiosa que encontramos, aquí hay legajos de detenidos que vamos a entregar con el ministro a las víctimas sobrevivientes o familiares en el caso de que no estén con vida”, concluyó Puyol.