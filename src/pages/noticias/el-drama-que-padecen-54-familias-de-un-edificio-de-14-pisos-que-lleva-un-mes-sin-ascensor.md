---
category: La Ciudad
date: 2020-12-17T10:57:10Z
thumbnail: https://assets.3dnoticias.com.ar/1712-edificio-pilay.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Diario UNO'
resumen: El drama que padecen 54 familias de un edificio de 14 pisos que lleva un
  mes sin ascensor
title: El drama que padecen 54 familias de un edificio de 14 pisos que lleva un mes
  sin ascensor
entradilla: Desde el 19 de noviembre que vienen reclamando una solución urgente, haciendo
  hincapié en el calvario cotidiano que viven las familias al llegar a sus hogares.

---
Vecinos de un **edificio de 14 pisos con domicilio en Monseñor Zazpe 3050** actualmente están viviendo el drama de vivir **sin ascensores desde hace casi un mes**. 

La causa original habría sido un accidente de trabajo de parte de un empleado de la EPE, en el que se habrían dañado los medidores de energía, lo que provocó un cortocircuito en gran parte del edificio.

El hecho en cuestión sucedió el **día jueves 19 de noviembre**, momento desde el cual **los dos ascensores del edificio quedaron inutilizados**. Luego del accidente, la empresa consorcista Inmogroup S.R.L. elevó a la EPE el reclamo por lo sucedido.

Actualmente, **en el edificio funciona un ascensor con un tablero provisorio que abastece a las 54 familias del edificio**. Los vecinos que habitan el edificio San Jerónimo 65 en barrio Sur de la ciudad manifiestan su preocupación, indicando que "la situación no da para más".

"**Somos 14 pisos con 4 departamentos por piso y un ascensor provisorio**. Es más que obvio que falle, lo dejaron a media máquina casi inoperable, está totalmente colapsado".

***

![](https://assets.3dnoticias.com.ar/1712-edificio-pilay1.jpg)

***

**Al mismo tiempo, al colapsarse por el uso excesivo, el ascensor provocó numerosos atascos de vecinos**. Incluso, hubo un caso en el que quedaron atrapados un padre y una bebé, quienes permanecieron en su interior durante varios minutos: "ya dos veces nos pasó en 7 días, y esta vez fue la peor. No tomo más ese ascensor", contó la víctima.

En la misma sintonía, se manifestó una mujer quien tiene viviendo a su madre mayor de edad en el piso 14 del edificio: "Vivo con miedo por mi mamá. Ella cuando va en el ascensor provisorio casi que va rezando, vive en el piso 14. Solo dos veces subió y bajó por escaleras, fue tomando agua y se iba sentando en los descansos".

Así mismo, **el cortocircuito original quemó numerosos artefactos del edificio**, como manifiesta un vecino: "en mi caso me quemó las entradas de audio y vídeo del TV, además de que derritió todos los cables internos".

El monto total presupuestado para el arreglo de los ascensores es de 2 millones de pesos. La empresa proveedora habría solicitado al consorcista Inmogroup el 50% del dinero para comenzar la reparación de los ascensores, a lo que dicha empresa habría aludido que "no tiene respaldo económico para afrontarlo actualmente".

***

![](https://assets.3dnoticias.com.ar/1712-edificio-pilay2.jpg)

***