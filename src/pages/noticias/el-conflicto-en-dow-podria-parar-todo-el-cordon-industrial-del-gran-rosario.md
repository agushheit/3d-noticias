---
category: Agenda Ciudadana
date: 2021-08-24T06:00:09-03:00
thumbnail: https://assets.3dnoticias.com.ar/DOW.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Hernán Álvarez. El Litoral
resumen: El conflicto en Dow podría parar todo el cordón industrial del Gran Rosario
title: El conflicto en Dow podría parar todo el cordón industrial del Gran Rosario
entradilla: 'Dow Argentina tiene decidido cerrar su fábrica de plásticos en Puerto
  General San Martín en mayo de 2022. '

---
Desde hace unos días, se hizo pública la determinación de la firma Dow de cerrar su planta productora de productos plásticos de Puerto General San Martín en mayo de 2022. 120 trabajadores están empleados en esta empresa del norte del Gran Rosario.

La conciliación obligatoria dictada por el Ministerio de Trabajo de la Provincia fue acatada y se continúa con la producción. Está prevista otra reunión entre el sindicato, autoridades provinciales y representantes de esta compañía esta semana. Desde el gremio se reunieron también con la CGT y de seguir el conflicto, se podría realizar un paro en todo el cordón industrial.

Desde Mirador Provincial se intentó contactar a algún responsable de la firma en territorio santafesino sin respuesta. En tanto, Mauricio Brizuela, secretario general del Sindicato Obreros Empleados Petroquímicos Unidos (Soepu), habló con este medio. Hasta el momento, la decisión de la empresa extranjera de cerrar el año próximo y de no vender es irreversible. “Van apagando las máquinas, los compañeros por la misma angustia y la misma situación empiezan a buscar algunos acuerdos individuales como retiros voluntarios y ese tipo de cosas. No creo que la producción llegue a mayo de ninguna manera”, comentó Brizuela.

Son 120 puestos de trabajo directos que están en juego. “Además lo que más preocupa es que es un producto totalmente esencial para las colchoneras, para las de línea blanca, parte de la automotriz, de la construcción -afirmó el gremialista-. Es un producto con valor agregado y que agrega más en la cadena de valor en el resto de la industria que lo consume. Por eso la importancia de que se siga produciendo acá”.

Con un insumo importado que se descarga directamente desde los barcos en los muelles de la empresa Dow produce poliuretano, vulgarmente llamado gomaespuma, y solventes hidrogenados. Estos componentes se usan para otras industrias radicadas en el país. “La materia prima es óxido de propileno. Dow tiene un circuito cerrado, pero hay varios productores en el mundo de este óxido”, comentó el sindicalista.

Los motivos del cierre son una reestructuración a nivel mundial que implica achicar un 6 por ciento los gastos. “Tienen una capacidad ociosa en una planta de mucha más capacidad en Brasil. Cuando el mercado argentino no les resulte rentable o no, van a dejar de importar y eso nos va a llevar al cierre de muchas más empresas.”, aseguró Mauricio Brizuela.

Desde el sindicato piden el auxilio del sector político. La diputada provincial Silvana Di Stéfano y la senadora nacional María de los Ángeles Sacnun, entre otros, han hablado con los trabajadores. El secretario del Soepu afirmó: “Estuvimos con diputados de todos los partidos que se han acercado y que nos han escuchado. También con senadores. El jueves vamos a ser recibidos en la Cámara de Senadores en Santa Fe. Lo que nos está faltando es el ministro de Producción a nivel nacional (Matías Kulfas, ministro de Desarrollo Productivo) que se le pidió una reunión. Todavía no ha contestado”.

La posibilidad de formar una cooperativa con todos los empleados está totalmente descartada por el costo alto de los insumos. “Es casi imposible. Son productos altamente costosos, la logística no es nada sencilla. Sí hay capitales interesados en comprar. Es un producto que tiene mercado, que es altamente rentable. El tema es que Dow quiere demoler y no quiere vender. Fue bien claro. Lo ratificó el otro día en el Ministerio de Trabajo”, comentó Brizuela. “Si se quieren ir por una decisión global, que se vayan, pero la transferencia tecnológica y la necesidad de ese producto esencial se tiene que seguir haciendo acá. Con otros capitales o (con un sistema) mixto”, agregó.

El gremialista comentó también que desde el sindicato se realizó un informe donde se asentó la cantidad de toneladas que se consumen del producto terminado y las empresas que son clientes en el país.