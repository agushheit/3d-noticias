---
category: Agenda Ciudadana
date: 2021-06-08T08:03:35-03:00
thumbnail: https://assets.3dnoticias.com.ar/equipos-robadosjpg.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de  LT10
resumen: '"Sacá la pistola y quemalo": la amenaza a un camarógrafo antes de robarle
  sus herramientas'
title: '"Sacá la pistola y quemalo": la amenaza a un camarógrafo antes de robarle
  sus herramientas'
entradilla: Javier contó que cuando escuchó ese grito atinó a tirarse al piso y agarrarse
  la cabeza. Los delincuentes dejaron abandonada una moto, que era robada, y salieron
  corriendo.

---
Javier Lanzillotto es un reconocido camarógrafo de la ciudad de Santa Fe y el fin de semana sufrió en carne propia la inseguridad que golpea todos los días a esta ciudad capital.

"Estaba trabajando en lo que fue la cobertura de los festejos de los hinchas de Colón. Cuando termino de realizar los registros audiovisuales me voy a mi lugar físico de trabajo para descargar y editar el material para el medio de comunicación para el cual trabajo", relató.

"Cuando terminó mi actividad, en barrio Sur, Saavedra entre Juan de Garay y Corrientes, me encuentro con una moto y dos jóvenes que estaban adelante de mi vehículo particular. Alcanzo a ver que tenían una pertenencia mía, mi mochila", continuó.

Javier recordó que de forma "instantánea, sin pensarlo" salió corriendo hacia ellos y a gritar «me están robando»".

Precisó que en ese momento, los delincuentes tiran la moto al piso con algunos de los objetos robados. Ahí uno sale corriendo y el otro de los malvivientes le grita a su cómplice: "Sacá la pistola y quemalo".

Javier relató que en ese momento "de sensación indescriptible", optó por tirarse al piso y agarrarse la cabeza. "No sabés si te están diciendo la verdad o una mentira; no había luz, no vi si tenían un arma".

"Salieron corriendo, con la moto tirada y algunas pertenencias mías que se fueron cayendo por el camino. Llamé al 911, viniveron en dos minutos, constataron que la moto era robada", expresó en declaraciones a la emisora LT10.

Cuando pudo subir a su vehículo, Javier pudo constatar lo que le faltaba: "Mi billetera con absolutamente toda la documentación, tarjetas, DNI, carné de conducir, credenciales. Y mis elementos de trabajo, menos la cámara que había bajado para realizar el trabajo de edición. En el bolso había micrófonos corbateros, auriculares, baterías de la cámara, cables, iluminador LED".