---
category: La Ciudad
date: 2020-12-31T11:30:15Z
thumbnail: https://assets.3dnoticias.com.ar/3112-cobem.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Comenzó la vacunación al personal del Cobem
title: Comenzó la vacunación al personal del Cobem
entradilla: 'Ayer inició la campaña de vacunación contra el Covid-19 al personal de
  salud del Centro de Operaciones, Brigadas y Emergencia Municipal. '

---
El intendente Emilio Jatón estuvo en el Cemafé durante la vacunación al personal del Cobem. **Se trata de los primeros 10 trabajadores que accedieron a la vacunación contra el Covid-19**, durante el segundo día de la campaña nacional. En ese contexto, ayer se cumplieron dos meses del fallecimiento por coronavirus del exdirector del Cobem, Ramón Gamarra. Con el recuerdo presente de todo el personal, los trabajadores de emergencias municipales iniciaron una nueva etapa desde lo sanitario.

El intendente Emilio Jatón, junto al secretario de Salud de la provincia, Jorge Prieto, brindaron una conferencia de prensa en la cual detallaron el alcance de la vacunación al personal del Cobem. En ese marco, el mandatario indicó: «hoy han venido hasta el Cemafé, que es uno de los 5 puntos de vacunación que hay en la ciudad, personal de salud del Cobem. Entendemos que están en la primera línea de trabajo. Sabemos que este es un proceso muy largo y que hoy la vacunación nos da un poco de luz para seguir trabajando».

Por otra parte, Jatón hizo referencia al año complejo que tuvieron los trabajadores de la salud: «fue un año muy duro para ellos y me parece que la vacunación es una luz de esperanza para seguir adelante. Vemos que se están vacunando todos y eso es muy importante porque nos permite seguir trabajando sobre otros aspectos».

***

![](https://assets.3dnoticias.com.ar/3112-cobem1.webp)<br/>

<span style="font-family: Helvetica, sans-serif; font-size: 1.2em; font-weight: 700">**«Seguir cuidándonos»**</span>

«Lo que hay que hacer es seguir cuidándonos», consignó el intendente y añadió: «mantener el distanciamiento y seguir usando el tapaboca, porque el proceso de vacunación será largo».

Por su parte, Prieto indicó que el Cobem fue el primer equipo de trabajo que se vacunó (fuera de los trabajadores de la salud de los hospitales). En tal sentido, el funcionario provincial indicó: «entendemos que quienes están a cargo del Cobem son también esenciales, es una herramienta fundamental porque hoy se encuentran transitando las calles de Santa Fe y exponiéndose tal cual el resto del equipo de salud».

<br/>

<span style="font-family: Helvetica, sans-serif; font-size: 1.2em; font-weight: 700">**Recuerdo y futuro**</span>

Uno a uno fueron inoculándose los primeros 10 trabajadores de Cobem. En cada uno de los casos, el recuerdo de Ramón Gamarra estuvo más presente que nunca: «estábamos esperando la vacunación», expresó aliviado Ismael Galván, encargado del área de emergencia. «Cuando nos fuimos anoticiando de que iba a llegar la vacuna expresamos el deseo de vacunarnos. Pensamos mucho en Ramón durante estos». Asimismo, Galván indicó que hubo mucha aceptación del personal para colocarse la vacuna «principalmente porque estamos en la primera línea», aseguró.

«Nos vamos a vacunar los que estamos en la calle, los que estamos en riesgo continuamente», añadió Marcos Quintana, subdirector del Cobem, quien no pudo ocultar la emoción por el recuerdo de su compañero: «lamentablemente llegó ahora la vacuna, si no lo tendríamos todavía a Ramón».

![](https://assets.3dnoticias.com.ar/3112-cobem2.webp)