---
category: Agenda Ciudadana
date: 2021-07-25T06:00:29-03:00
thumbnail: https://assets.3dnoticias.com.ar/SEQUIA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Anticipan que "La Niña" se mantendrá en la primavera, por lo que el río seguirá
  bajo
title: Anticipan que "La Niña" se mantendrá en la primavera, por lo que el río seguirá
  bajo
entradilla: El Servicio Meteorológico Nacional analizó la falta de lluvias en Brasil,
  donde desde hace 22 años las condiciones de precipitación están por debajo del promedio.

---
El Servicio Meteorológico Nacional (SMN) realizó un informe para dar cuenta de las causas y perspectivas sobre la bajante histórica que persiste en el río Paraná. “El agua escasea y aparecen los problemas: la navegación fluvial, las tomas de agua urbana, la generación de energía, la fauna íctica, el riesgo de incendios y la modificación de cauces y paisaje. Los impactos son múltiples, extendidos y costosos”, observan desde el SMN.

Acerca de las causas, el organismo nacional se lo atribuye a una serie de aspectos, entre los que enumera: la falta de lluvias; el aumento de la demanda hídrica; la explotación de la tierra; y principalmente, la prolongada sequía severa que afecta la parte alta de la Cuenca del Paraná, en el sur de Brasil. En el comunicado detallan que desde hace 22 años las condiciones de precipitación están por debajo del promedio (con excepción del 2010 y el 2015) y la situación empeoró desde el 2018, lo que ha dado como resultado una situación de sequía que pasó de severa a excepcional.

“Estamos sufriendo los efectos de una sequía que empezó en junio de 2019. Es un ciclo que no terminó todavía y no se tiene claro cuándo va a terminar. Esta sequía nos comenzó a llamar la atención en marzo del 2020, cuando se empezó a manifestar de una manera irreversible, y sobre todo cuando la alta cuenca del Paraná, en Brasil, empezó a mostrar una disminución de su aporte muy significativo. Disminución que obligó a un seguimiento hora a hora, porque nos puso adelante el grave problema que tiene la Argentina con las tomas de agua urbana”, dijo Juan Borús, hidrólogo del Instituto Nacional del Agua (INA).

La serie de datos de la cuenca alta muestra varios ciclos secos y lluviosos a lo largo de las décadas desde 1900, con la sequía más severa registrada entre diciembre de 1968 y septiembre de 1971, alcanzando su punto máximo en marzo de 1969. Sin embargo, es importante señalar que, aunque hoy se registran valores similares, en ese momento la demanda de agua en la cuenca del río Paraná era mucho más baja que la actual.

**Precipitaciones por debajo de lo normal**

En Argentina se está dando una situación de déficit de lluvias similar a la que ocurre en Brasil. “Las provincias del Litoral y centro-norte argentino también registran precipitaciones por debajo de lo normal desde mediados del 2020, lo que agrava aún más el problema. El déficit no es solo a nivel hidrológico -ríos- sino que el suelo también sufre de escasez de agua”, describe el SMN.

Respecto a las causas de bajas precipitaciones, el Dr. Alejandro Godoy del SMN, mencionó: “La falta de lluvias en nuestro país y en la porción de la cuenca alta y media del río Paraná y sus afluentes, por ejemplo el río Iguazú y río Paraguay, se dieron por diferentes causas. Un factor importante fue que se estableció un evento de La Niña que redujo las chances de lluvias abundantes desde la primavera del 2020, cuando comienza la estación lluviosa, hasta el inicio del otoño 2021. Por lo tanto, los ríos no pudieron recargarse lo suficiente antes del comienzo del invierno, que es el período de menores lluvias del año. Además, durante el año se produjeron pocas situaciones favorables para lluvias intensas que suelen darse cuando hay un ingreso de aire húmedo proveniente de Brasil que se combina con los pasajes de sistemas frontales en el noreste de nuestro país”.

El SMN resaltó la situación adversa en relación a la sequía que afecta a algunas provincias argentinas. “Las lluvias acumuladas en los últimos 12 meses en provincias como Misiones o parte de Corrientes son realmente alarmantes. Algunas estaciones meteorológicas registraron en el último año entre 400 y 600 mm menos que en un año normal, lo que representa aproximadamente el 50% de la lluvia anual”.

**Perspectivas poco alentadoras**

¿Hasta cuándo persistirá la bajante extrema del río Paraná? Es la pregunta que todos se hacen. Sin embargo, las expectativas no son alentadoras, según pronostica el SMN.

La temporada de lluvias en la zona central de Brasil comienza a mediados de octubre, pero desde Cemaden (Centro Nacional de Monitoramento e Alertas de Desastres Naturais) indican que todavía no es posible anticipar si la temporada de lluvias comenzará temprano o si tendrá precipitaciones por encima o por debajo del promedio. Sin embargo, aclaran que un factor que eventualmente puede dificultar el inicio de la temporada de lluvias es la actual situación de sequía prolongada, que implica valores bajos de humedad del suelo (y, por tanto, de humedad del aire) necesarias para la formación de las primeras precipitaciones.

El organismo nacional de meteorología resaltó que, en la Región Sur de Brasil, la situación actual de sequía puede estar relacionada con el fenómeno de “La Niña”, que estuvo en vigencia entre junio de 2020 y abril de 2021, pero que tiene una cierta influencia en el comportamiento de la atmósfera hasta unos meses después de su fin. “La Niña es un factor modulador de lluvias en el sur de Brasil y el Litoral argentino, que reduce la cantidad de precipitaciones en esa región”, sostuvieron.

Sobre sus tendencias climáticas para el trimestre julio-agosto-septiembre, el SMN expresó: “Estos patrones de lluvia se repiten, tanto en la cuenca brasileña, como en las zonas de los ríos argentinos. Y aunque todavía es muy pronto para confirmarlo y los pronósticos presentan una alta incertidumbre a tan largo plazo, existe la posibilidad de un nuevo evento de La Niña a partir de la próxima primavera”.