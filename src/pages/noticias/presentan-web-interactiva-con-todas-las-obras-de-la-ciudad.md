---
category: La Ciudad
date: 2021-11-02T06:00:40-03:00
thumbnail: https://assets.3dnoticias.com.ar/WEBINTERACTIVA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Presentan web interactiva con todas las obras de la ciudad
title: Presentan web interactiva con todas las obras de la ciudad
entradilla: 'La Municipalidad de Santa Fe puso en marcha el portal digital Santa Fe
  Capital en Obras donde se muestra en tiempo real el avance de infraestructura urbana
  y otras acciones en el territorio. '

---
Desde el comienzo de la actual gestión en diciembre de 2019 el intendente Emilio Jatón puso en marcha un proceso de transparencia y gobierno abierto con el objetivo de promover información pública de calidad y control ciudadano sobre el Estado municipal. Con esa condición, la Secretaría de Gobierno se abocó a desarrollar herramientas específicas.

Ya en diciembre de 2020 la Municipalidad de Santa Fe habilitó la web de Transparencia (www.santafeciudad.gov.ar/transparencia) en la cual figuran los datos del gobierno: estructura, personal, normativas, ejercicios presupuestarios, compras, transferencias, auditoría e información pública, entre otras cosas. Ahora, después de un arduo trabajo de articulación interna, se presentó Santa Fe Capital en Obras.

Dicha web ya se puede visitar y navegar desde la web del municipio (www.santafeciudad.gov.ar). Es una plataforma de código abierto que fue adaptada para brindar información fidedigna a la ciudadanía. Dicho proceso estuvo a cargo de la Secretaría de Gobierno, a través de las subsecretarías de Innovación Institucional y Gobierno Abierto y de Comunicación Social, y la Gerencia de Tecnologías para la Gestión.

Se optó por una herramienta digital evolutiva en la cual también participó personal de la municipalidad, es decir, se usaron las capacidades instaladas y los recursos propios. Con Santa Fe Capital en Obras se cumple con una de las premisas del intendente Emilio Jatón luego de crear el Programa de Transparencia y Datos Abiertos, favorecer la publicidad de los actos de gobierno.

La página web contiene el monto total de inversión en obras públicas que ejecuta el municipio, siendo a la fecha $2.038.404.119. Además, muestra que se realizaron o están en construcción un total de 275 obras de distintas características y magnitudes, y que hay más de 3.100 obreros que están siendo ocupados en la actividad de la construcción. La actualización de la información es automática.

Santa Fe Capital en Obras contiene la tipología de las acciones y las distribuye en seis áreas: obras hídricas, infraestructura urbana (bacheo, pavimentación y ripiado), iluminación, hábitat, espacios verdes y recuperación edilicia. Todas esas obras están georreferenciadas en el mapa de la ciudad, a su vez dividida en sus ocho distritos.

Cada una de las 275 obras muestra quién ejecuta (personal municipal o empresas), el monto de inversión, la fuente de financiamiento, el barrio en el que se hace, la cantidad aproximada de vecinos y vecinas beneficiadas y la secretaría municipal a cargo. No obstante, el secretario de Gobierno, Federico Crisalle, destacó que esta es una primera etapa de la web y que se sumará más contenido.

“El evolutivo de la web nos permite incorporar mucha más información de gestión, además de sumarle fotos y videos de cada acción. No menos importante es que logramos estandarizar la carga de la información para todas las secretarías que ejecutan obras: cómo y qué relevar; eso ayuda a la transparencia, pero también al control ciudadano que destacamos como significativo en nuestra gestión”, explicó el funcionario.

Por último, Crisalle indicó que “hasta ahora ninguna otra administración en la ciudad de Santa Fe implementó un sistema de gestión de obras, donde haya un registro claro y público de las obras ejecutadas por la municipalidad”. Y destacó que la web es “sencilla, ágil y liviana, tanto para navegarla como para su mantenimiento”.