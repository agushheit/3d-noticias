---
category: Agenda Ciudadana
date: 2021-02-18T07:11:46-03:00
thumbnail: https://assets.3dnoticias.com.ar/pusineri.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de LT10
resumen: 'Volvió a reunirse la paritaria docente: los cuatro puntos acordados'
title: 'Volvió a reunirse la paritaria docente: los cuatro puntos acordados'
entradilla: Funcionarios santafesinos y dirigentes gremiales abordaron cuestiones
  vinculadas al quehacer de los maestros en el marco de la pandemia. El ofrecimiento
  de aumento salarial se diseñará tras la paritaria docente nacional.

---
Volvió a reunirse este miércoles la paritaria docente en Santa Fe. Posteriormente, el ministro de Trabajo, Juan Manuel Pusineri, dio detalles del encuentro que matuvo con dirigentes de Amsafe, Sadop, UDA y AMET.

En primer lugar, admitió que aún no hubo ofrecimiento de incremento de sueldos para los maestros de la provincia. No obstante, aseguró que el gobierno de Omar Perotti no busca “dilatar la cuestión”, sino todo lo contrario: “la idea nuestra es concluir la cuestión salarial lo antes posible”, garantizó.

En ese sentido, indicó que se está aguardando “tener algún indicio”, el cual surgiría este jueves, cuando se realice la paritaria docente nacional. “Nos estamos acercando”, confió.

**Puntos acordados**

Según indicó Pusineri al móvil de LT10, durante la reunión de hoy se acordaron cuatro puntos esenciales:

1\. Los docentes que están contemplados en los grupos de riesgo por la pandemia de coronavirus, o que tienen hijos menores a cargo, van a seguir exceptuados de cumplir con sus tareas.

2\. “Se va a garantizar el hisopado a todos los docentes que voluntariamente deseen hacérselo y de acuerdo a la logística que vaya disponiéndo el Ministerio de Salud en toda la provincia”.

3\. “Todo docente que se inscriba al boleto educativo gratuito va a tener acceso al mismo, con independencia de sus ingresos”.

4\. “Se va a habilitar un mecanismo de notificación por inexistencia de transporte público, para que los docentes puedan notificar esta circunstancia”.

El próximo encuentro de la paritaria docente, anticipó Pusineri, será el jueves 25 de febrero a las 10:00.