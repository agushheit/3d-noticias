---
category: Agenda Ciudadana
date: 2020-11-27T11:55:00Z
thumbnail: https://assets.3dnoticias.com.ar/./gonet.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Acción Climática Provincial
title: Gonnet participó de la reunión de la Comisión de Ambiente de Diputados
entradilla: La funcionaria provincial fue invitada para dar detalles del proyecto
  de Ley de Acción Climática Provincial elaborado por el Ejecutivo y abordar, además,
  diversos temas ambientales.

---
La ministra de Ambiente y Cambio Climático, Erika Gonnet, participó de la reunión de Comisión de Medio Ambiente y Recursos Naturales de la Cámara de Diputados de la provincia, donde ante las legisladoras y los legisladores que integran la misma presentó el proyecto de Ley de Acción Climática Provincial que el gobierno provincial ingresó el pasado 20 de octubre a la Legislatura.

“Nuestro proyecto propone acción, territorio y transversalidad; y además tiene el objetivo de popularizar la temática”, dijo Gonnet durante el encuentro. Y en ese sentido, enfatizó en la idea de “dar tratamiento conjunto a las diversas propuestas que se encuentran en la Legislatura para lograr la mejor Ley posible”.

“La única forma de lograr una acción concreta, de mitigar los efectos y adaptar los territorios al cambio climático, es hacerlo entre todos y todas; dándole un rol protagonista a las juventudes como lo impulsamos con la mesa que llevamos adelante desde el Ministerio desde hace unos meses, y contando con la participación de intendentes y presidentes comunales”, amplió la funcionaria.

“En esta temática, la sociedad está avanzada por sobre el rol de la clase política; tenemos que estar a la altura de las demandas y entender que debemos avanzar fuertemente para resolver estas problemáticas porque, como lo vengo diciendo, la acción climática es ahora”, agregó.

Durante el encuentro, se trataron diversos temas entre los que se encuentran controles ambientales, incendios, estado de situación de evaluaciones ambientales, periurbanos y humedales, entre otros.

“Por iniciativa de la diputada Matilde Bruera, mantuvimos este encuentro donde pudimos detallar el complejo estado de situación con el que nos encontramos en el Ministerio; las iniciativas impulsadas por el gobierno de Santa Fe en el marco del tratamiento en el Congreso Nacional sobre el proyecto de ley de Humedales; y el accionar en el contexto de los incendios donde los que más sufrimos fuimos las y los santafesinos”, puntualizó Gonnet al finalizar el encuentro.

“Fue un buen marco para que los legisladores conozcan que desde el gobierno provincial impulsamos una consulta a más de 20 universidades, instituciones, organizaciones de la sociedad civil y colegios profesionales, pidiendo una opinión técnica sobre los proyectos de Ley de Humedales que se discutían en la Cámara de Diputados de la Nación, y que esos abordajes formaron parte del material que analizaron antes de lograr el dictamen unificado que salió hace unos días de la Comisión de Recursos Naturales”, amplió.

  
 Junto a Gonnet, participaron el secretario de Políticas Ambientales, Oreste Blangini; el subsecretario de Cambio Climático, Marcelo Gallini; el subsecretario de Legal y Técnica, Guillermo Carro; y el subsecretario de Tecnologías para la Sostenibilidad, Ing. Franco Blatter.