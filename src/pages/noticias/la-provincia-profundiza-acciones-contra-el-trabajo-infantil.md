---
category: Estado Real
date: 2021-08-19T06:00:24-03:00
thumbnail: https://assets.3dnoticias.com.ar/TRABAJOINFANTIL.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La provincia profundiza acciones contra el trabajo infantil
title: La provincia profundiza acciones contra el trabajo infantil
entradilla: El Ministerio de Trabajo, Empleo y Seguridad Social, a través de la Dirección
  de Empleo Digno, articulará el trabajo de organizaciones públicas y privadas con
  el objetivo de prevenir y erradicar el trabajo infantil.

---
Para ello se firmó un convenio entre el Ministerio, la Comisión Provincial Para La Prevención Y Erradicación Del Trabajo Infantil (COPRETI) y un conjunto de empresas y entidades santafesinas para constituir la “Red de Empresas contra el Trabajo Infantil”, cuyo objetivo será el de generar y/o dar apoyo a programas, planes y proyectos tendientes a la prevención y erradicación del trabajo infantil. La COPRETI brindará asesoramiento técnico para el diseño y/o aprobación de programas y acciones vinculadas con la temática.

Juan Manuel Pusineri, ministro de la cartera laboral, expresó que la actual mirada sobre esta problemática debe ser integral: “Este acuerdo involucra también la cadena de comercialización o provisión de insumos, ya que existe un compromiso por parte del estado de no contratar empresas que utilicen trabajo infantil, que tengan denuncias de trabajo infantil, o que se encuentren en instancias que contradigan la legislación vigente. El objetivo es que las y los chicos estén en las escuelas y en eso hay un compromiso no solo del estado provincial nacional, de los municipios y de los sindicatos, sino también particularmente de las empresas, las cuales están incorporando estas preocupaciones como parte de su responsabilidad social.”

Asimismo, se presentó la segunda etapa del Diagnóstico sobre Trabajo Infantil en la provincia, tarea a cargo de los técnicos del Centro de Estudios Scalabrini Ortiz (CESO), cuyo primer avance se publicó en el mes de febrero.

La directora de Empleo Digno del MTEySS, Fernanda Medina, expresó que “contar con estos datos nos permite diseñar nuestras políticas de gobierno sobre bases serias y reales, prestando atención a aquellos puntos geográficos y actividades económicas en los cuales el trabajo infantil aparece con mayor frecuencia. Esto nos permite direccionar los recursos de apoyo a las familias, chicas y chicos que lo necesitan, así como los de prevención y control en aquellos casos que así lo requieran.”

La Organización Internacional del Trabajo (OIT) ha declarado a 2021 como “Año Internacional para la Erradicación del Trabajo Infantil”. Se entiende por Trabajo Infantil “toda actividad económica y/o estrategia de supervivencia, remunerada o no, realizada por niñas y niños, por debajo de la edad mínima de admisión al empleo o trabajo, o que no han finalizado la escolaridad obligatoria o que no han cumplido 18 años si se trata de trabajos peligrosos” (Plan Nacional para la Prevención y Erradicación del Trabajo Infantil - CONAETI 2006).