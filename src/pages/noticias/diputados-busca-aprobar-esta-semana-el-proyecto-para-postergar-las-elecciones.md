---
category: Agenda Ciudadana
date: 2021-05-17T06:00:57-03:00
thumbnail: https://assets.3dnoticias.com.ar/PASO.png
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Diputados busca aprobar esta semana el proyecto para postergar las elecciones
title: Diputados busca aprobar esta semana el proyecto para postergar las elecciones
entradilla: Se trata de un proyecto consensuado entre el Frente de Todos y Juntos
  por el Cambio para diferir por un mes las primarias abiertas, simultáneas y obligatorias
  (PASO) y las elecciones generales .

---
La Cámara de Diputados buscará aprobar esta semana un proyecto consensuado entre el Frente de Todos y Juntos por el Cambio para postergar por un mes las primarias abiertas, simultáneas y obligatorias (PASO) y las elecciones generales debido a la segunda ola de la pandemia de coronavirus.

La iniciativa, impulsada por el Gobierno nacional, propone trasladar las PASO del 8 de agosto al 12 de septiembre y las generales del 24 de octubre al 14 de noviembre, en el marco de la renovación legislativa de medio término.

La intención de la bancada del oficialismo, que conduce Máximo Kirchner, es convocar para este miércoles a una sesión especial, en la que además se buscará convertir en ley el Consenso Fiscal firmado entre el presidente Alberto Fernández y los mandatarios provinciales, informaron fuentes parlamentarias.

También está previsto debatir una iniciativa sobre el impuesto a las Ganancias para las sociedades, que reduce del 30 por ciento al 25 por ciento la alícuota que pagan las pequeñas y medianas empresas, y aumenta al 35 por ciento la tasa para las grandes compañías, con la intención de instrumentar un sistema progresivo.

Será la primera deliberación luego que el presidente de la Cámara de Diputados, Sergio Massa, acordará con los bloques parlamentarios la renovación del protocolo de funcionamiento hasta el 23 de junio, lo que permitirá emitir dictamen y llevar a cabo la sesiones a través de una modalidad mixta-presencial, y remota para los grupos de riesgo.

El tema clave de la sesión será la postergación de las PASO del 8 de agosto hasta el 12 de septiembre y las generales del 24 de octubre al 14 de noviembre, un aplazamiento que en caso de aprobarse implicará que los nuevos legisladores electos estarán definidos a mediados de noviembre.

El recambio legislativo debe concretarse el 10 de diciembre.

Mientras el Congreso se apresta a modificar las fechas del llamado a las urnas, el Ejecutivo ya puso en marcha el calendario electoral, a través de un decreto, con el cronograma previsto inicialmente.

Sin embargo, en la Casa Rosada aguardan la sanción de la ley para correr los plazos que ya están rigiendo y amoldarlos a la norma que se apruebe en el Parlamento.

El proyecto para postergar un mes las PASO y las generales fue gestado a partir de un acuerdo general del oficialismo, en el que trabajaron varios funcionarios y referentes legislativos del Frente de Todos con la conducción de Juntos por el Cambio, que en Diputados encabeza el diputado Mario Negri (UCR-Córdoba).

Una de las condiciones que hizo posible el consenso entre las partes fue la incorporación de una cláusula, pedida por la oposición, en la que se establece que este año no podrá volver a modificarse el calendario electoral.

**Qué dice el proyecto**

En su artículo 6, el proyecto fija que "la presente ley no podrá ser modificada ni derogada durante el año calendario en curso, en tanto regula un derecho público subjetivo de los partidos políticos, instituciones fundamentales del sistema democrático, a elegir sus candidatos a los cargos electivos previstos en la Constitución Nacional".

Ese acuerdo pudo observarse el jueves pasado en la última reunión de la comisión de Asuntos Constitucionales, en la que se emitió un dictamen que fue firmado por la mayoría de los partidos políticos.

En esa ocasión, el titular de la comisión, el diputado Hernán Pérez Araujo (Frente de Todos-La Pampa), destacó "el consenso y el diálogo profundo al que se arribó, luego de algunas idas y vueltas, y del que surge este proyecto con un cariz de racionalidad en el marco de esta pandemia".

Y en el mismo sentido agregó: "Existe una gran responsabilidad al poner en tratamiento este proyecto, que es producto del fruto y del consenso de todas las fuerzas de la cámara: por eso, por esta única vez, y sólo en este contexto de pandemia, se trasladarán las fechas de las PASO y de las generales".

En representación del interbloque de Juntos por el Cambio, el radical Gustavo Menna (UCR-Chubut) expresó: "Coincidimos en el proyecto, pero lamentamos que no se haya avanzado con la boleta única de papel, para mantener este sistema arcaico y perimido".

En sus fundamentos, el proyecto que posterga por un mes el cronograma electoral plantea que "el aplazamiento de la celebración de los comicios a meses con temperaturas más elevadas permitirá optimizar la ventilación de los locales de votación y la implementación de los protocolos sanitarios".

La iniciativa también hace referencia a que "durante ese lapso de tiempo adicional se continuará avanzando con la masiva campaña de vacunación que lleva adelante el Estado Nacional junto a las veinticuatro jurisdicciones, coadyuvando a un mejor cuidado de la mayor cantidad posible de argentinos y argentinas que deben cumplir con su deber cívico".

Sobre las potestades y mayorías legislativas que se requieren para hacer cambios en el calendario de elecciones, el proyecto que se tratará esta semana recuerda que en base al artículo 77 de la Constitución "es potestad exclusiva del Poder Legislativo decidir las modificaciones que se realicen al régimen electoral y de partidos políticos" y asimismo subraya que los eventuales cambios deben ser aprobados "por mayoría absoluta total de los miembros de ambas Cámaras del Congreso".

**Comienza el debate por la reforma del Ministerio Público Fiscal**

**PAG12**

El oficialismo buscará avanzar con el proyecto de reforma del ministerio público fiscal que el modifica el cargo vitalicio del Procurador General de la Nación, y lo limita a un período de cinco años.

El oficialismo buscará avanzar con el proyecto de reforma del ministerio público fiscal que el modifica el cargo vitalicio del Procurador General de la Nación, y lo limita a un período de cinco años. Se tratará en el plenario de las comisiones de Justicia y de Asuntos Constitucionales el martes para que el pleno de la cámara de Diputados pueda tratarlo en la sesión de la próxima semana. El proyecto también impulsa que la elección del Procurador sea elegida por mayoría absoluta del Senado, en vez de la forma en la que se elige actualmente que es mediante los dos tercios de los presentes.

El proyecto enviado por el ministro de Justicia, Martín Soria, modificará la composición de los integrantes del Jury de enjuiciamiento, que se adaptará a los tiempos que corren y contempla la garantía de que se cumpla con la paridad de género, además de los cambios para impulsar la destitución del Procurador en caso de ser necesario. de aprobarse el proyecto el tribunal quedaría compuesto por un representante del Poder Ejecutivo, dos integrantes de la Comisión Bicameral parlamentaria repartido en un lugar para el oficialismo y uno por la mayoría y uno por la minoría. Además, habrá un representante del Consejo Interuniversitario Nacional, un abogado y dos fiscales, un varón y una mujer. Una de las modificaciones que obligará al proyecto a volver a ser tratado en el Senado es la inclusión de un representante por el Ministerio Público Fiscal, lugar que pertenecía a los legisladores de la mayoría que en el documento inicial le asignaba dos representantes.

El proyecto es uno de los objetivos principales con los que el rionegrino Soria desembarcó en el ministerio de Justicia, después del paso de Marcela Losardo al inicio de la gestión del presidente Alberto Fernández. El primer tratamiento del proyecto lo tendrá el martes a partir de las 19 el plenario de las comisiones de Justicia, que preside el diputado del Frente de Todos, Rodolfo Tailhade, y la de Asuntos Constitucionales en manos también del oficialismo con Hernán Pérez Araujo a la cabeza.

La propuesta contempla además la elección del jefe de los fiscales por mayoría absoluta del Senado en vez de los dos tercios de los presentes y pone un plazo de cinco años, aunque permite que el funcionario sea reelecto por un período más. Para el proceso de destitución del Procurador establece que deberán reunir al menos cinco votos, que representa los dos tercios del Jury, y determina cuatro votos para el resto de las decisiones.

Es resistido por la oposición de Juntos por el Cambio (JxC), quienes el último viernes, al momento de retomar la iniciativa se retiraron planteando diferencias e la metodología que se adoptó para el tratamiento en la cámara. La mesa de conducción de la alianza opositora macrista y radical se reunía la noche del domingo para definir los pasos que tomarán en los próximos días. Evaluaban si asistirán o no a la reunión del martes o qué tipo de postura llevarán al Parlamento.

JxC rechazará el proyecto, pero el oficialismo confía en tener los votos necesarios para lograr un dictamen favorable que permita tratar el proyecto la semana que viene en el recinto. Algunos de los bloques provinciales como el de Unidad Federal para el Desarrollo, que lidera José Luis Ramón, ya adelantaron su aval para tratar el proyecto. El mendocino mantuvo reuniones con el ministro de Justicia con quien analizó proyectos de ley que procuran mejoras en el servicio de justicia. En el plenario del viernes pasado, Ramón propuso incorporar la creación de una Procuraduría especializada en Defensa de las y los Consumidores y Usuarios en el ámbito del Ministerio Público Fiscal al proyecto del oficialismo.

La elección del Procurador quedó en el centro de debate político luego de que el actual funcionario, Eduardo Casal, llegara al cargo de forma interina como consecuencia de la renuncia de su antecesora Alejandra Gils Carbó, que había sido electa mediante la reglamentación anterior por el Senado, mientras que Casal asumió ante la vacante por ser el procurador ante la Corte más antiguo.

Soria recordó días atrás que "la ley actual fue producto de una decisión coyuntural que se sancionó en el Congreso en 1997", y que por ello "debe ser modificada ante el profundo cambio de paradigma que supone el paso al sistema acusatorio qué estamos llevando adelante". El funcionario destacó que "no se han podido lograr los consensos para elegir Procurador (General de la Nación) y por eso seguimos transitando el interinato más largo de la historia", que ya lleva tres años en el cargo y que había sido ratificado en el cargo en 2017 por el entonces presidente Mauricio Macri.