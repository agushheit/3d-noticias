---
category: Estado Real
date: 2021-07-06T08:23:39-03:00
thumbnail: https://assets.3dnoticias.com.ar/licitación.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: Durante julio la provincia licitará la construcción de 313 viviendas para
  ocho localidades
title: Durante julio la provincia licitará la construcción de 313 viviendas para ocho
  localidades
entradilla: Con fondos provinciales y nacionales, se construirán soluciones habitacionales
  en Tostado, Rafaela, San Javier, El Trébol, Casilda, Cañada de Gómez, Hipatía y
  Gálvez. La inversión supera los 1.300 millones de pesos.

---
El Ministerio de Infraestructura, Servicios Públicos y Hábitat de la provincia licitará la construcción de 235 viviendas, y por administración 78 viviendas, todas con infraestructura básica, en Tostado, Rafaela, San Javier, El Trébol, Gálvez, Cañada de Gómez, Hipatía y Casilda. La inversión total será de $ 1.309.433.780.

La implementación de este conjunto de viviendas, se disgrega en 287 unidades que se ejecutarán con fondos nacionales bajo el programa Casa Propia - Construir Futuro, perteneciente al Ministerio de Desarrollo Territorial y Hábitat de la Nación. Mientras que las restantes 26 viviendas corresponden a la terminación y nueva construcción de prototipos en la ciudad de San Javier, que se ejecutarán con fondos provinciales.

Al respecto, la ministra de Infraestructura, Silvina Frana, señaló: "Esto forma parte de programas de viviendas tanto provinciales como nacionales que llevamos adelante para integrar y equilibrar el territorio. A pesar del momento que estamos atravesando, redoblamos los esfuerzos y trabajamos de forma coordinada con el gobierno Nacional, con el firme objetivo de darle respuestas a las necesidades de nuestra gente”.

Asimismo, añadió que "poder llevar adelante estas licitaciones responde a una política concreta, de generar trabajo y concretar sueños a partir de la vivienda y el hábitat. Son obras nuevas que se pondrán en marcha con la apertura de los sobres. Conocemos la situación y la demanda habitacional de muchas familias que hace años quieren acceder a su casa propia, proyectar su futuro, y arraigarse en su localidad".

Por su parte, el secretario de Hábitat de la provincia, Amado Zorzón, sostuvo que “lo importante es seguir trabajando articuladamente, con una política federal de vivienda y hábitat, buscando siempre responder y solucionar la demanda habitacional de la provincia, lo que para nosotros representa un compromiso".

Finalmente, el director provincial de Vivienda y Urbanismo, José Kerz manifestó que "el tema de la vivienda es una prioridad para el Gobierno de la Provincia, y la presencia del Estado es fundamental para financiar la construcción”, dijo el funcionario, quien agregó que “en el caso particular de San Javier, vamos a terminar 20 viviendas y construir 6 nuevas unidades, con fondos provinciales. Todas contarán con infraestructura básica, y serán de dos y tres dormitorios".

**GRUPO DE LICITACIONES**

>> TOSTADO: se licitarán 45 viviendas, con un presupuesto oficial de $ 191.794.814,64.

>> EL TRÉBOL: se construirán 41 unidades distribuidas en cuatro procesos licitatorios: uno de 6 viviendas (presupuesto oficial de $ 25.673.729,93), otro de 19 viviendas (presupuesto oficial $ 81.001.475,75), y dos licitaciones de 8 viviendas cada una, uno para la Manzana 188 y otro para la Manzana 252 (cada licitación cuenta con un presupuesto oficial de $ 34.185.690,83).

>> RAFAELA: se harán 45 viviendas cuya construcción también contará con cuatro licitaciones: una para 28 unidades en el barrio 2 de Abril (presupuesto oficial $ 119.305.299,79), 3 unidades en el barrio Italia (presupuesto oficial $ 12.767.941,34), 6 viviendas en el barrio Mora (presupuesto oficial $ 25.535.882,69) y otras 8 en el Zaspe (presupuesto oficial: $ 31.738.488,06).

>> CAÑADA DE GÓMEZ: se licitarán 24 viviendas con un presupuesto oficial de $ 102.281.378.

>> CASILDA: se licitarán 52 viviendas con un presupuesto oficial de $ 242.206.824,47.

Estas 207 viviendas de Tostado, El Trébol, Rafaela, Cañada de Gómez y Casilda contarán con dos dormitorios, cocina – comedor, baño y lavadero y espacio para cochera, rondando los 65 m² de superficie cubierta y 5 m2 de superficie en galería, en promedio, con una platea ejecutada para ampliar la vivienda con un tercer dormitorio. 

Asimismo, cada vivienda estará dotada de instalaciones y conexiones de agua potable, cloacas y energía eléctrica, e instalaciones aptas para gas envasado. Los desagües pluviales irán a la vía pública, y cada unidad contará con un termotanque solar.

>> GÁLVEZ: para las 78 viviendas destinadas a Gálvez se realizarán dos licitaciones, una de 52 unidades para el barrio Margarita (presupuesto oficial $ 214.083.747,28) y otra para 26 viviendas en el barrio Oeste (presupuesto oficial $ 102.859.920,87).

>> HIPATÍA: se licitarán 5 viviendas con un presupuesto oficial de $ 19.355.439,30.

Estas 83 viviendas para Gálvez e Hipatía tendrán dos dormitorios, cocina – comedor, baño y lavadero y espacio para cochera, rondando los 64 m² de superficie cubierta y 6 m2 de superficie en galería, en promedio, con una platea ejecutada para ampliar la vivienda con un tercer dormitorio. 

Asimismo, cada vivienda estará dotada de instalaciones y conexiones de agua potable, cloacas y energía eléctrica, e instalaciones aptas para gas envasado. Los desagües pluviales irán a la vía pública, y cada unidad contará con un termotanque solar.

>> SAN JAVIER: se licitarán 26 viviendas con un presupuesto oficial de $ 72.457.456,55. De esas unidades, 20 son de dos y tres dormitorios y deben ser finalizadas, en tanto que se construirán otras 6 (de dos dormitorios e infraestructuras básicas).