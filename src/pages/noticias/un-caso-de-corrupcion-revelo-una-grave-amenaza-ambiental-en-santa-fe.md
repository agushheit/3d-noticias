---
category: Agenda Ciudadana
date: 2021-01-19T09:00:29Z
thumbnail: https://assets.3dnoticias.com.ar/bomba_quimica.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Germán de los Santos para La Nación
resumen: Un caso de corrupción reveló una grave amenaza ambiental en Santa Fe
title: Un caso de corrupción reveló una grave amenaza ambiental en Santa Fe
entradilla: La Justicia investiga un presunto pacto para cerrar una causa contra el
  empresario Tasselli por la desaparición de material tóxico

---
**ROSARIO**.- El interrogante sobre el impacto de los casos de corrupción se hace palpable en la causa judicial que envuelve a la Petroquímica Bermúdez, una planta situada sobre el río Paraná, a cinco kilómetros del límite con Rosario. "Es una bomba ambiental", definió el fiscal Luis Schiappa Pietra, que llevará a una audiencia imputativa al empresario Sergio Tasselli,a su hermano Máximo y al exjefe de los fiscales de Rosario Patricio Serjal, a los que acusará de "pactar" el archivo de una causa en la que se investigaba el desastre ambiental que se originó en la planta de insumos químicos, de 20 hectáreas.

De ese predio, que Tasselli -actualmente procesado en la causa de los cuadernos de las coimas- adquirió en 1998 y cerró en 2013, como otra decena de empresas que compró en los 90, desaparecieron 850 tubos de gas de cloro de una tonelada cada uno, una sustancia altamente tóxica, según el acta 197 del Ministerio de Medio Ambiente que consta en la causa, a la que accedió LA NACION. El escape de gas de tan solo uno de esos tambores provocaría la evacuación de las poblaciones de ciudades cercanas, como Capitán Bermúdez, Granadero Baigorria y el norte de Rosario, señala uno de los informes.

Nadie sabe adónde fueron a parar esos contenedores de gas cloro, trágicamente conocido por su uso durante la Primera Guerra Mundial como arma química. Se sospecha que podrían haberse tirado al río Paraná o haberse enterrado, algo peor -según señalan los especialistas- porque no se tiene ningún control del daño que podrían causar las sustancias. "Desaparecieron", apuntó el fiscal.

"Es un desastre ambiental que podría tener dimensiones impensadas", explicó Cecilia Bianco, de la ONG Taller Ecologista, entidad que hace cinco años puso en alerta a las autoridades sobre lo que ocurría con el desmantelamiento de Petroquímica Bermúdez. "Detectamos que durante la noche entraban y salían camiones de la planta, con lo cual la principal sospecha es que estaban sacando las bombonas de cloro", advirtió.

En la planta de Petroquímica Bermúdez se hallaron 1000 tanques de cloro gaseoso. Desaparecieron 850, de una tonelada cada unoEn la planta de Petroquímica Bermúdez se hallaron 1000 tanques de cloro gaseoso. Desaparecieron 850, de una tonelada cada uno

Además del cloro gaseoso, se detectaron otras sustancias altamente tóxicas que contaminaron las napas freáticas por los escapes permanentes de residuos, como cloroformo, benceno, mercurio y lindano (conocido como Gammexane), un insecticida que se fabricó en ese predio. "El peligro es que no se sabe si las napas de los barrios vecinos están contaminadas con estas sustancias, altamente cancerígenas", señaló un exfuncionario que investigó el caso.

Cinco días antes de que se decretara el aislamiento por la pandemia, los pobladores de la zona se reunieron en el club Vecinos Unidos, en el barrio Celulosa, para pedir a las autoridades que intervengan ante esta "bomba ambiental". La pendiente de las napas va hacia el barrio El Espinillo, con lo cual el peligro es que esas sustancias hayan penetrado en el área donde hay consumo de agua de la población.

La planta química se llamaba Electroclor y se inauguró en 1946. En ese momento, los accionistas eran Duperial y Celulosa Argentina, que cerraron la fábrica en 1993, donde antiguamente se aprovechaban y procesaban los residuos de cloro que en ese momento usaban en la papelera.

En 1998, el grupo Tasselli adquirió la planta, donde en esa época solo se producía cloro, acetileno y tricloroetileno, y la nombró Petroquímica Bermúdez. Cinco años después, en julio de 2013, la planta quedó paralizada por un conflicto laboral que terminó en un intento de toma de la fábrica por parte de los trabajadores del gremio químico, que fueron sacados, según denunciaron en ese momento, por una patota. La estrategia que la empresa usó en esta ciudad del Gran Rosario se repitió en otros emprendimientos que fueron desmantelados, como Río Turbio, Altos Hornos Zapla y Agrinar, entre otros.

La Justicia investiga un presunto pacto para cerrar una causa contra el empresario Tasselli por la desaparición de material tóxico; en la Petroquímica Bermúdez desaparecieron 1000 tubos de gas cloro, altamente tóxico, de una tonelada cada uno. La Justicia investiga un presunto pacto para cerrar una causa contra el empresario Tasselli por la desaparición de material tóxico; en la Petroquímica Bermúdez desaparecieron 1000 tubos de gas cloro, altamente tóxico, de una tonelada cada uno Crédito: Télam

¿Por qué este empresario había comprado una planta química para después cerrarla? La sospecha es que se buscó vender chatarra, según quienes conocen la investigación. El problema es que no era una chatarra cualquiera, sino residuos altamente contaminados y peligrosos.

Desde ese momento, el Ministerio de Medio Ambiente de Santa Fe comenzó a exigirle "un cierre seguro" de la planta. Pero Tasselli, según fuentes de la anterior administración de Santa Fe consultadas, "nunca cumplió". Ante ese escenario, a través de la Fiscalía de Estado, en 2016 se presentó una denuncia penal en el Ministerio Público de la Acusación, que recayó en la Unidad Fiscal de Delitos Ambientales.

En 2017, una auditoría detectó "la presencia de diversos productos químicos peligrosos que ameritan acciones inmediatas para evitar una emergencia ambiental con la afectación de la población". "Ante cualquier emergencia que libere productos tóxicos a la atmósfera, es altamente probable que la nube de gases sea arrastrada por el viento hacia poblaciones que rodean las instalaciones de Petroquímica Bermúdez, hacia el sur, el norte y el oeste", señala el informe, firmado por el ingeniero Andrés Salum.

![](https://assets.3dnoticias.com.ar/bomba_quimica1.jpg)

**Causa cerrada, fiscal preso**

Cuando la causa penal avanzaba sobre la responsabilidad de Tasselli, según indicaron fuentes judiciales comenzó a gestarse una maniobra para clausurarla, que ahora va a terminar en la imputación de los Tasselli y del exjefe de los fiscales de Rosario Serjal, actualmente preso por el cobro de sobornos en una causa ligada al juego clandestino.

Los fiscales Schiappa Pietra y Matías Edery, que encabezan la Unidad de Crimen Organizado, empezaron a relevar las causas que había cerrado Serjal. Se encontraron con que el 8 de marzo de 2019 el fiscal de San Lorenzo Juan Carlos Ledesma había pedido el archivo de la causa de Petroquímica Bermúdez en una audiencia ante el juez Juan José Tutau. "Es la instrucción de mi superior", dijo, en alusión a Serjal.

La sospecha es que el pago de sobornos en este caso podría haber sido de más 180.000 dólares, revelaron las fuentes. Con el archivo del caso se terminaba la causa penal que preocupaba a Tasselli y a su hermano Máximo, presidente de Petroquímica Bermúdez.

Solo en Off. El Tramposo, El Arrogante y El Contrabandista, los curiosos nombres de los vinos que lanzó Vicentin	

En la causa, según describió el fiscal Schiappa Pietra, sobraban elementos para seguir la investigación e imputar a los empresarios. Serjal pidió el archivo de la causa porque la Fiscalía de Estado de Santa Fe también había desistido de la acción penal luego de firmar en 2018 un acuerdo de "remediación" -que incluía el traslado seguro de las sustancias contaminantes- de la planta de Capitán Bermúdez, que Tasselli nunca cumplió. Por eso, otro de los llamados a declarar es el fiscal adjunto de la Inspección General de Personas Jurídicas (IGPJ), Gustavo Luzzini, que desistió de seguir con la causa.

El 23 de diciembre pasado, Schiappa Pietra y Edery recibieron la novedad de que el juez de San Lorenzo Eugenio Romanini había anulado la imputación al considerar que los Tasselli ya habían sido juzgados cuando se archivó la causa, por pedido del exfiscal que está preso por cobrar sobornos.

El 31 de diciembre, sin embargo, la jueza de Cámara Georgina Depetris avaló el pedido de recusación del juez y, tras la finalización de la feria, los Tasselli deberán enfrentar la imputación, que probablemente sea acompañada, según las fuentes judiciales, de un pedido de detención. "Mientras tanto, los vecinos de la zona conviven con una bomba ambiental", afirmó Cecilia Bianco, del Taller Ecologista.