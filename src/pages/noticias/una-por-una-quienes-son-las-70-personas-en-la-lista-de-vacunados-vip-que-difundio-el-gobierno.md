---
category: Agenda Ciudadana
date: 2021-02-23T07:11:18-03:00
thumbnail: https://assets.3dnoticias.com.ar/vacuna-vip.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: DS
resumen: Una por una, quiénes son las 70 personas en la lista de vacunados VIP que
  difundió el Gobierno
title: Una por una, quiénes son las 70 personas en la lista de vacunados VIP que difundió
  el Gobierno
entradilla: Quién es quién en el listado de funcionarios, empresarios, parientes y
  dirigentes que se vacunaron en el Posadas y en el Ministerio de Salud. Nombres vinculados
  a Ginés y a los ministros Guzmán y Vizzotti.

---
Nombres célebres y personas desconocidas. Adultos con edad de riesgo y jóvenes de 27 o 32 años. Funcionarios esenciales y de los otros. Amigos del exministro Ginés González García y subordinados de su sucesora, Carla Vizzotti.

Los 70 nombres de los beneficiados por el escándalo del vacunatorio VIP en el Ministerio de Salud, que reveló el Gobierno, incluye funcionarios, empresarios, parientes, dirigentes y hasta periodistas.

1- **Alberto Fernández**: 61 años. Presidente de la Nación.

2- **Julio Vitobello**: 63 años. Secretario general de Presidencia.

3- **Juan Pablo Biondi**: 48 años. Vocero presidencial.

4- **Marcelo Martín**: 51 años. Vacunado como parte "de la burbuja presidencial". Subsecretario de Comunicación y Prensa de la Nación.

5- **Esteban Collazo**: 33 años. Fotógrafo presidencial. Antes trabajó en Télam y en Aubasa. Vacunado por ser parte "de la burbuja presidencial".

6- **Nicolás Ritacco**: 27 años. Trabaja en Ceremonial de Presidencia. Miembro de "la burbuja presidencial".

7- **Felipe Solá**: 70 años. Canciller.

8- **Jorge Neme**: 71 años. Secretario de Relaciones Económicas Internacionales de Cancillería.

9- **Martín Guzmán**: 38 años. Ministro de Economía.

10- **Melina Mallamace**: 32 años. Economista, fue alumna de Martín Guzmán y hoy es jefa de Asesores del Ministerio de Economía.

11- **Vera Voskanyan**: 32 años. Secretaria del ministro Martín Guzmán. Fue prensa y community manager en el Ministerio de Producción, según su perfil de Linkedin. Se definía como "armenia, bailarina y periodista, en ese orden", en Twitter. Luego lo borró.

12- **Sergio Chodos**: 51 años. Representante por el Cono Sur en el directorio del Fondo Monetario Internacional (FMI). Intermediario entre el organismo y el Gobierno en la negociación por la deuda.

13- **Pablo Salinas**: 38 años. Asesor del Ministerio de Economía. Hace las veces de secretario privado del ministro Guzmán. Politólogo, del instituto Gestar. También asesora en la Cámara de Diputados.

14- **Maia Colodenco**: 38 años. Economista. Titular de la Unidad Ejecutora Especial Temporaria de "Coordinación y Gestión de Asuntos Internacionales", dependiente del Ministerio de Economía.

15- **Daniel Scioli**: 64 años. Embajador en Brasil. Ex vicepresidente y gobernador bonaerense.

16- **Oscar Domingo Peppo**: 62 años. Embajador en Paraguay. Ex gobernador de Chaco.

17- **Carlos Zannini**: 66 años. Procurador del Tesoro de la Nación.

![](https://assets.3dnoticias.com.ar/carlos-zannini-vacunas-vip.webp)

###### Carlos Zannini es Procurador del Tesoro, pero lo hicieron pasar por "personal de salud"

<br/>

18- **Ginés González García**: 75 años. Exministro de Salud.

19- **Lisandro Amelio Bonelli**: 44 años. Sobrino de Ginés González García y su jefe de Gabinete de asesores. Este lunes renunció al cargo. Diputado provincial bonaerense en licencia de su mandato.

20- **Judit Díaz Bazán**: 58 años. Subsecretaria de Calidad, Regulación y Fiscalización del Ministerio de Salud. Médica cirujana e infectóloga.

21- **Marcelo Ariel Guille**: 43 años. Empleado del Ministerio de Salud y mano derecha de Ginés González García. En gacetillas lo presentaban como "experto" y "asesor". También vacunaron a su padre, amigo de Ginés.

22- **Mauricio Alberto Monsalvo**: 40 años. Subsecretario de Gestión Administrativa del Ministerio de Salud.

23- **Martín Horacio Sabignoso**: 44 años. Secretario de Equidad en Salud, del Ministerio de Salud.

24- **Arnaldo Medina**: 58 años. Secretario de Calidad en Salud del Ministerio de Salud. Vicerrector de la Universidad Arturo Jauretche y exdirector del Hospital El Cruce.

25- **Claudio Miguel D'Amico**: 55 años. Empleado del PAMI, trasladado en "comisión de servicios" a la Secretaría de Equidad en Salud.

26- **José Corchuelo Blasco**: 75 años. Exdiputado nacional y exministro de Salud de Chubut. Amigo de Ginés González García, era su asesor ad honorem en el Ministerio y fue socio fundador de la Fundación Isalud, think tank del ahora exministro.

27- **Héctor Barrionuevo**: 63 años. Director nacional de Salud Mental y Adicciones, dependiente del Ministerio de Salud.

28- **Patricia Gallardo**: 47 años. Directora del Instituto Nacional de Lucha contra el Cáncer, dependiente del Ministerio de Salud.

29- **Mariano Alberto Fontela**: 53 años. Subsecretario de Integración de los Sistemas del Ministerio de Salud.

30- **Andrés Joaquín Leibovich**: 68 años. Subsecretario de Política, Regulación y Fiscalización del Ministerio de Salud. También trabaja para el Instituto de Previsión Social de la Provincia de Buenos Aires.

31- **Horacio Insúa**: secretario privado del exministro Ginés González García.

32- **Eugenio Daniel Zanarini**: 70 años. Superintendente de Servicios de Salud y socio comercial de Ginés González García en bodegas.

33- **Analía Rearte**: directora nacional de Epidemiología, del Ministerio de Salud.

34- **Juan Pablo Saulle**: coordinador logístico de la Secretaría de Acceso a la Salud, que comandaba Carla Vizzotti.

35- **Analía Aquino**: asesora en la Secretaría de Acceso a la Salud, en la gestión de Carla Vizzotti.

36- **Alejandro Costa**: subsecretario de Estrategias Sanitarias del Ministerio de Salud de la Nación.

37- **Juan Castelli**: director nacional de Control de Enfermedades Transmisibles del Ministerio de Salud de la Nación.

38- **María de los Ángeles Domínguez**: empleada del Ministerio de Salud.

39- **Graciela Miranda**: sin datos.

40- **Filomena Marta Burgo**: esposa del exintendente Hugo Curto.

41- **"Páez D'Alessandro"**: no figura su nombre de pila.

42- **Irene López**: se referiría a Irene Ibon López, de 44 años, ciudadana extranjera.

43- **Néstor Mandraccio**: 59 años. Fue empleado de Irene López.

44- **Gabriel Michi**: 53 años. Periodista de C5N. Dijo que averiguó para vacunarse por un viaje a Manaos, una zona en Brasil con mucho riesgo de contagio.

45- **Jorge Héctor Devoto**: "El Topo", 65 años. Dirigente kirchnerista, director de cine y autor de un libro sobre Néstor Kirchner.

46- **Patricia Alsúa**: 62 años. Esposa de Carlos Zannini. Exfuncionaria, estuvo al frente de la Casa de Santa Cruz en Buenos Aires.

47- **Lorenzo Antonio Pepe**: 89 años. Exdiputado y dirigente sindical peronista.

48- **Marcelo Jorge Duhalde**: 72 años. Exfuncionario de Ginés González García en su anterior paso por el Ministerio, en tiempos de Néstor Kirchner. Hermano del exsecretario de Derechos Humanos, el fallecido Eduardo Luis Duhalde. Periodista.

49- **Hugo Curto**: 82 años. Exintendente de Tres de Febrero, uno de los "barones del conurbano".

50- **Eduardo Duhalde**: 79 años. Expresidente.

51- **Hilda González**: "Chiche", 74 años. Esposa de Eduardo Duhalde, exministra y senadora.

52- **Carlos Mao**: secretario personal del expresidente Duhalde.

53- **María Eva Duhalde**: 49 años. Hija de Eduardo Duhalde.

54- **Juliana Duhalde**: 46 años. Hija de Eduardo Duhalde.

55- **Carlos Ariel Gilardi**: 44 años. Jefe de Planificación Estratégica del Ministerio de Salud.

56- **Yael Morichetti**: empleada en el Ministerio de Salud.

57- **Carlos Alberto Avella**: empleado en el Ministerio de Salud.

58- **Juan Souto**: sin datos.

59- **Camilo Martelletti**: 22 años. Hijo de María Julieta Martínez, una mujer vinculada a una congregación de Hermanas de San Antonio de Padua.

60- **Miguel Ángel González**: sin datos.

61- **Seza Manukian**: 77 años. Empresario jubilado.

62- **Horacio Verbitsky**: periodista, director del CELS y del portal El Cohete a la Luna. Fue echado de radio El Destape por el escándalo. Publicaron mal escrito su apellido como "Verbinsky".

63- **Lourdes Noya Aldrey**: familiar de Aldrey Iglesias.

64- **Matilde Noya Aldrey**: familiar de Aldrey Iglesias.

65- **Dolores Noya Aldrey**: familiar de Aldrey Iglesias.

66- **Félix Eulogio Guille**: padre de Marcelo Guille, funcionario también en la lista de vacunados. Sería amigo de Ginés González García.

67- **Florencio Aldrey Iglesias**: empresario marplatense, dueño del diario La Capital de esa ciudad y del Hotel Hermitage, entre otros negocios.

68- **Jorge Enrique Taiana**: senador nacional y excanciller. Esposo de Bernarda Llorente, presidenta de la agencia de noticias Télam.

69- **Salomón Schachter**: médico, amigo de Ginés González García.

70- **Eduardo Valdés**: diputado nacional.