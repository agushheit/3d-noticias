---
category: Agenda Ciudadana
date: 2021-01-18T12:55:18Z
thumbnail: https://assets.3dnoticias.com.ar/omar.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: José Villagrán
resumen: 'Cambios en el gabinete de Omar Perotti: asumen Sukerman, Pusineri, Corach
  y Toniolli para darle “otro aire” a la gestión '
title: 'Cambios en el gabinete de Omar Perotti: asumen Sukerman, Pusineri, Corach
  y Toniolli para darle “otro aire” a la gestión'
entradilla: En el comienzo del fin de semana, el gobernador los confirmó en sus recientes
  funciones. ¿Qué es lo que pretende el mandatario de cara a un año donde se evaluará
  su gobierno?

---
El viernes, cerca del mediodía, Omar Perotti confirmó algo que la mayoría de los que estamos vinculados al mundo político santafesino sabíamos: los reemplazos y corrimientos dentro de la estructura del Ejecutivo para cubrir los “cargos vacantes” que se dejaron y ajustar la tuerca en el funcionamiento de la gestión.

De esta manera, el rafaelino pone en funciones a dirigentes que le han mostrado lealtad y se han ganado su confianza. De esta manera, Roberto Sukerman deja el ministerio de Trabajo para asumir, nada más y nada menos, en el “superpoderoso” ministerio de Gobierno, Justicia y Derechos Humanos (que desde el mes de noviembre lo ocupaba Ruben Michlig tras la salida de Esteban Borgonovo), transformándose así en una pieza fundamental en el nuevo armado perottista. En su lugar, en la cartera laboral se produce el corrimiento de puestos y asume Juan Manuel Pusineri, actual secretario. 

Junto a Sukerman llega Eduardo Toniolli, que deja su banca en el Concejo Municipal de Rosario para empezar a ser el “segundo”. 

El cuarto movimiento de este ajedrez lo completa Marcos Corach, que llegó al gobierno a mediados de 2020 y ya se empieza a encargar del ministerio de Gestión Pública que deja Ruben Michlig para ser el jefe de asesores del gobierno. 

Hay algunos lugares dentro de la estructura gubernamental que faltan completar, como por ejemplo el lugar de vocero que dejó vacante Leonardo Ricciardino y el cual le fue ofrecido al actual secretario de turismo, Alejandro Grandinetti. 

**¿Quiénes son los nuevos ministros y qué busca Omar Perotti de ellos?**

**Roberto Zukerman** está haciendo sus “primeras armas” en un ejecutivo bajo el mando perottista. Comenzó siendo concejal de la ciudad de Rosario en 2011 y 2017. Bajó la conducción de Agustín Rossi, inició su recorrida en la política en el espacio kirchnerista “La Corriente”, quien terminó cerrando un acuerdo electoral para apoyar, en la interna primero y en la elección general después, la candidatura del rafaelino en 2019. Como ministro de Trabajo y Seguridad Social empezó con una actividad importante, tratando de destrabar conflictos gremiales y laborales, además de encargarse de cerrar las primeras paritarias del nuevo gobierno. En el último tiempo, su labor pasó a un segundo plano cuando los rumores de cambios se acentuaron cada vez más. La misión de Sukerman es levantar el perfil político del gobierno; esto es, mejorar la comunicación con intendentes y presidentes comunales, pero además tratar de mejorar la relación con la Legislatura, donde los lazos tendidos siempre estuvieron tirantes. No la tiene fácil el rosarino. En el territorio tiene que eludir la presencia de algunos senadores departamentales con los que el gobierno entró en “distanciamiento” político. En las cámaras, tiene que arreglárselas con no tener mayoría en ninguna de las dos (7 en Diputados y 4 “fieles” senadores). 

![](https://assets.3dnoticias.com.ar/sukerman.jpeg)

Con él llega otro rosarino. **Eduardo Toniolli** es un ya dirigente y militante político acostumbrado al roce y el vértigo de las discusiones. Militante del “Movimiento Evita” supo ser diputado provincial y actualmente es representante de los vecinos de la ciudad del sur provincial. La tarea de Toniolli se enmarca en contribuir al vínculo con los representantes territoriales y trabajar en las autonomías municipales en distintos grados, y en un contexto diferente a partir del fallo de Corte Suprema de Justicia en el caso de la Festram, que reafirmó la autonomía de los municipios.

Dos elecciones mirando a la militancia y la territorialidad. Distintas a las otras dos que se dan. 

**Juan Manuel Pusineri** asume oficialmente un puesto que, en los hechos, ya ocupa. Abogado laboralista y de familia peronista, el santotomesino conoce el ámbito en el que batallará. Durante el 2020 fue el encargado de ser la voz del Ejecutivo en las discusiones salariales.  Fue blanco de algunos dardos gremiales, pero logró sostener las riendas de un año complicado por el covid. Conoce a Perotti desde la última gestión de Jorge Obeid (2003 – 2007) cuando desarrolló la tarea de secretario General y Técnico del Ministerio Coordinador. Sabe que la tarea no será sencilla. Se tendrá que ocupar de destrabar problemas, desactivando conflictos sindicales que incluso exceden lo estrictamente salarial como lo venía haciendo. Pero en el horizonte más próximo se le vienen las paritarias docentes, en un contexto donde se discute la vuelta a la presencialidad de las clases, y cerrar el acuerdo salarial con los trabajadores de la salud, que es la única que le falta al gobierno. 

La otra asunción política lo tiene como protagonista a **Marcos Corach**, un perottista de la primera hora. Llegó al gobierno en julio del año pasado como secretario de Articulación de Políticas Públicas dentro del ministerio de Gestión Pública. Pero la relación con el actual mandatario data de los años 90, cuando Perotti llegó a la intendencia de la ciudad de Rosario. Los caminos políticos se dividieron, pero no sucedió lo mismo con el vínculo, que con el paso del tiempo se fue afianzando. Quienes conocen a ambos afirman que “se conocen de memoria” los rafaelinos. El gobernador quiso sumarlo desde el inicio de su mandato, pero por pedido de Luis Castellano (intendente de Rafaela) decidió esperarlo un poco más. Así llega a ocupar el ministerio para darle mayor presencia en territorio y altura para mantener lazos constantes con quienes se encargan de gestionar a lo largo de toda la bota santafesina. Tanta es la confianza que le tiene el gobernador que Corach formó parte, junto al ministro de economía Walter Agosto y el secretario de Comunas José Luis Freyre, de la reunión que se dio entre Omar Perotti y el intendente Pablo Javkin vente días antes de asumir para acordar acuerdos para la ciudad de Rosario. Ese es el nivel de confianza que manejan. 

![](https://assets.3dnoticias.com.ar/corach.jpeg)

**Un gabinete con “perfil más alto”**

Desde el entorno de Omar Perotti evitan hablar de “relanzamiento” del gobierno. Pero si reconocen un “cambio de aire” y una “apertura del gobierno que es necesario”. Y ese cambio se da en el comienzo de un año que al gobierno lo encuentra mirando a distintos ángulos al mismo tiempo. 

Las asunciones dejan en claro con qué sectores del peronismo hace acuerdos Perotti y a quien les mantiene cerradas las puertas (por ahora) dentro de “la unidad peronista”. El ascenso de Sukerman marca la confirmación de lo que fue la reunión entre el gobernador y el ministro de Defensa Nacional, Agustín Rossi, diez días después que Esteban Borgonovo abandone el gobierno y dijera que “no tenía el apoyo del gobernador”. Tras ese encuentro, Rossi dijo que “hay que fortalecer la gestión, apoyar al gobierno de Omar y garantizar la unidad peronista (...) Siempre es bueno parar un poco la pelota y reflexionar”. 

Desde el espacio que lidera el dirigente nacional ven con buenos ojos las nuevas designaciones, pero plantean que “no tiene que ser un cambio de figuritas así nomás”. Piden que haya un cambio en la gestión, cambio que cada vez se reprochaba aún más. 

“Es cierto, tenemos que tener un gabinete con perfil alto, me gusta eso”, sostuvo el gobernador en las últimas horas al hablar del recambio. El gobernador le ha transmitido a sus funcionarios la necesidad de hacer eje en la conducta social e individual en esta etapa de nuevos contagios.

Sin dudas que la cuestión sanitaria está todavía en la preocupación máxima del ejecutivo provincial. Pero también tiene en claro cuáles son los objetivos a alcanzar en este año: perfil más alto, mayor presencia en el territorio y evitar que la oposición le imponga la agenda de gobierno. Y para eso necesita un gobierno en donde comiencen a aparecer otros actores brindando información y tomando contacto con los diferentes sectores de la sociedad.

Todo esto mirando las elecciones de medio término de este año. Las nuevas designaciones apuntan también a "cerrar filas" con una estrategia de peronismo unido. “La construcción de unidad partidaria y en paridad; apoyo a las gestiones de Omar Perotti y Alejandra Rodenas en Santa Fe y Alberto Fernández y Cristina Fernández en la Nación; articulación permanente con funcionarios/as y legisladores/as del Frente de Todos en provincia y nación, escuela de formación política, diálogo fluido con afiliados/as y diversos espacios de militancia y trabajo conjunto con el Partido Justicialista Nacional, son algunos de los puntos que el Consejo Ejecutivo del Peronismo Provincial llevará a la reunión de proclamación de autoridades del PJ el próximo sábado 6 de febrero a las 10 horas”,