---
category: Agenda Ciudadana
date: 2021-06-06T06:00:34-03:00
thumbnail: https://assets.3dnoticias.com.ar/PRECIOS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Actualizan Precios Máximos y se espera el lanzamiento de una nueva canasta
title: Actualizan Precios Máximos y se espera el lanzamiento de una nueva canasta
entradilla: El Gobierno nacional prepara el lanzamiento de una canasta de 120 productos
  a precios congelados, mientras que el martes próximo concluye el acuerdo actual
  del Programa Precios Máximos.

---
El Gobierno nacional prepara para los próximos días el lanzamiento de una canasta de 120 productos a precios congelados, mientras que se conocerá cómo continuará el programa de Precios Máximos.

El martes próximo concluye el acuerdo actual del Programa Precios Máximos, que en su última prórroga el 14 de mayo pasado tuvo modificaciones y se redujo el listado con la decisión de la Secretaría de Comercio Interior de desenlistar 36 productos de las categorías de alimentos, artículos de higiene, perfumería y limpieza.

**Precios Máximos**

Precios Máximos se inició el 19 de marzo de este año, a partir de una resolución que congeló los valores de unos 300 productos y obligó a retrotraerlos al 6 de marzo, no obstante, lo cual en julio y en setiembre se autorizaron aumentos de entre 2% y 6% cada vez.

Desde el área que conduce Paula Español decidieron que "se empezará por desenlistar los productos más suntuarios, aquellos que no están en la canasta básica, como bebidas alcohólicas y otros no esenciales".

En la última actualización se quitó de la lista 36 categorías: aceite de oliva y otros aceites, tostadas y grisines, edulcorantes, saborizadores, arroz (excepto largo fino), encurtidos, aceitunas y pickles, harinas leudantes y premezclas, horneables y gelificables, pastas frescas, milanesas de soja, productos congelados en base a vegetales procesados y suplementos vitamínicos.

También salieron del programa tapas de empanadas y tartas, óleo calcáreo, apósitos y protectores mamarios, amargos, jugos, leche Infantil, queso rallado y queso crema, crema de leche, dulce de leche, manteca y margarina, yogur, postres y flanes.

Otras familias de productos son esponjas y guantes, trapos y paños, destapacañerías, toallas húmedas desinfectantes, perfumes y colonias, coloración del cabello, pañales para adultos, algodones e hisopos, esmaltes y quitaesmaltes, preservativos y geles Íntimos, productos para depilación, helado y hielo, y otros accesorios de limpieza.

De este modo, se mantiene el rumbo hacia el objetivo oficial de avanzar en una salida progresiva, ordenada y sustentable del programa Precios Máximos, por lo que el martes podría haber una nueva actualización en ese sentido.

**Nueva canasta de 120 productos**

Por otra parte, en los próximos días se espera el lanzamiento de la nueva lista en la que trabaja la Secretaría de Comercio Interior que comprende a 120 productos de la canasta básica, y que incluyen alimentos, bebidas y productos de higiene personal, y tendrá precios congelados por seis meses.

De acuerdo con información oficial, el acuerdo ya está prácticamente cerrado con las empresas que participan y que forman parte de la Coordinadora de Industrias de Productos Alimenticios (Copal).

Una de las novedades será que los precios acordados estarán impresos en los envases y, a diferencia de Precios Cuidados y Precios Máximos que son canastas que los consumidores pueden conseguir en cadenas de supermercados de todo el país, ahora el objetivo es que esta canasta esté disponible también en los comercios de cercanía o barriales.

Se espera que esta canasta sea acotada, con menos de la cuarta parte de productos que Precios Cuidados, pero no por eso menos representativa del consumo local y básico, y que el precio esté impreso en la etiqueta, de modo que no se generen distorsiones y se respete el programa.

Para el miércoles 16 de junio se espera la difusión del Índice de Precios al Consumidor (IPC) correspondiente a mayo, un dato fundamental como referencia para los futuros acuerdos de precios que se lleven a cabo con las cámaras empresarias.