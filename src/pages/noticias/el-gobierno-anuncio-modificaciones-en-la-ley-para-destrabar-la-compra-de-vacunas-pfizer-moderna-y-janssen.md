---
category: Agenda Ciudadana
date: 2021-07-03T06:15:02-03:00
thumbnail: https://assets.3dnoticias.com.ar/03012021-covid.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: El Gobierno anunció modificaciones en la ley para destrabar la compra de
  vacunas Pfizer, Moderna y Janssen
title: El Gobierno anunció modificaciones en la ley para destrabar la compra de vacunas
  Pfizer, Moderna y Janssen
entradilla: Así lo confirmó este viernes la secretaria Legal y Técnica, Vilma Ibarra,
  en una conferencia de prensa junto a la ministra de Salud, Carla Vizzotti. El DNU
  se publicará este sábado en el Boletín Oficial.

---
El presidente Alberto Fernández firmó este viernes un Decreto de Necesidad y Urgencia (DNU) para regular el mecanismo de adquisición de nuevas vacunas para inmunizar a la población contra el coronavirus, el que será publicado mañana en el Boletín Oficial.

El anuncio fue hecho por la secretaria Legal y Técnica de la Nación, Vilma Ibarra, quien añadió que el DNU dispone la creación de un Fondo de Compensación Covid-19 "para indemnizar a quienes puedan ser dañados por las vacunas".

La funcionaria ofreció una conferencia de prensa en la Casa de Gobierno junto a la ministra de Salud, Carla Vizzotti, quien manifestó que la medida "da la perspectiva de avanzar con contratos bilaterales, obtención de vacunas dentro del mecanismo Covax y a través de donaciones".

Ibarra dijo que la instrucción para la negociación es "compatibilizar" la llegada de nuevos fármacos con los intereses que tiene que proteger el Estado nacional y anunció la creación del Fondo de Compensación Covid-19 "para indemnizar a quienes puedan ser dañados por las vacunas".

Asimismo, afirmó que el DNU incluirá modificaciones a la ley de 27.573, que regula la adquisición de vacunas contra el coronavirus, y sostuvo que una de ellas "suprime la negligencia como supuesto de responsabilidad para los fabricantes de vacunas Covid 19", un término mencionado como uno de los obstáculos por potenciales laboratorios proveedores.

En ese sentido, precisó que el Estado establecerá "el modo en que recibe conforme las vacunas", mientras que se suprimirá la consideración de "maniobras fraudulentas o conductas maliciosas" de la ley y se utilizará "conductas dolosas", un concepto más claro y que es el que utiliza el Código Civil y Comercial.

Ibarra aseguró que el Gobierno nacional "negoció para tener la mayor cantidad de vacunas y proveedores sin interrumpir las conversaciones" y destacó que el DNU que se publicará mañana "es fruto de mucho esfuerzo" realizado para "cuidar los intereses de la Nación".

Así, aseguró que el Estado nacional "conversa, dialoga y escucha a todos los potenciales proveedores" de vacunas contra el coronavirus para evaluar si se puede "llegar a un acuerdo" y aclaró que "son muchos los interlocutores", más allá de los laboratorios estadounidenses.

La secretaria Legal y Técnica de la Nación dijo que esperaba que el Congreso de la Nación trate pronto la ley de regulación de adquisición de vacunas, ya que es una norma que "resguarda los intereses del Estado nacional, al tiempo que permite abrir puertas".

Por otra parte, advirtió que "hay bienes soberanos que no se puede tocar" si el Estado incumple sus compromisos establecidos en el DNU sobre vacunas contra el coronavirus, mientras que Vizzotti aclaró que "todavía son confidenciales las conversaciones" con los laboratorios para la compra de las vacunas y destacó que el DNU que firmará el Presidente "facilita" la adquisición de todas las inmunizaciones.

Ibarra, asimismo, aseguró que una vez que se aprobaron las vacunas pediátricas fue un objetivo del Estado nacional obtenerlas y, en ese contexto, es que se trató de modificar el "marco legal", pero protegiendo los intereses de los argentinos.

La funcionaria pidió no hacer especulaciones con las vacunas al afirmar: "Uso político no" y aclaró, ante una consulta periodística, que "si se votaba" lo que propuso ayer la oposición en el Congreso para cambiar la ley "no podíamos contratar" nuevas vacunas.

En ese marco, manifestó que la medida se adoptará por decreto porque el trámite en el Congreso "lleva mucho tiempo" y "los tiempos de la pandemia" son otros.