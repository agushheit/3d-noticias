---
category: Agenda Ciudadana
date: 2021-04-17T06:00:42-03:00
thumbnail: https://assets.3dnoticias.com.ar/ALBERTO.png
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: 'El Presidente ratificó las medidas: "Con el virus no se negocia"'
title: 'El Presidente ratificó las medidas: "Con el virus no se negocia"'
entradilla: Advirtió que "la saturación de camas en la ciudad de Buenos Aires es realmente
  preocupante" ante la segunda ola de contagios y afirmó que con el virus "no se negocia".

---
El presidente Alberto Fernández ratificó la vigencia del decreto de necesidad y urgencia que limitó la circulación de personas y la presencialidad de las clases presenciales en el Área Metropolitana de Buenos Aires durante las próximas dos semanas, dijo que las medidas nos son "antojadizas" y afirmó que con el virus "no se negocia" sino que "hay que ser inflexibles".

El jefe de Estado encabezó este mediodía una conferencia de prensa en la residencia de Olivos que se concretó luego de una reunión que mantuvo por más de una hora y media, con "vocación de encontrar salidas" con el titular del Ejecutivo porteño, Horacio Rodríguez Larreta.

Fernández dijo que el encuentro fue un ámbito en el que se pudieron "intercambiar miradas" y en la que el jefe de Gobierno porteño manifestó su preocupación por la suspensión de clases presenciales, algo que el Presidente comparte pero sobre lo que decisión avanzar ante el crecimiento exponencial de los casos de coronavirus en el ámbito metropolitano.

"El problema no ocurre efectivamente en los colegios", dijo el Presidente pero aclaró que, por fuera de las aulas, se da "un movimiento social que incrementa mucho la circulación ciudadana" y que aumenta en un "25 por ciento" la cantidad de personas en las calles.

Sobre ese punto, además, señaló que, desde el comienzo de la educación presencial, en marzo pasado, fue "exponencial" el crecimiento de contagios en las personas entre 9 y 18 años en la Ciudad.

“Desde que volvieron las clases fue exponencial aumento de casos de 9 a 18 años”, dice Fernández

"Hemos superado en la Ciudad, largamente, el pico más alto que reconocimos en la primera ola. Han vuelto a incrementarse el número de bajas", dijo.

Por ello, Alberto Fernández dijo entender "el deseo y la preocupación" por las clases presenciales manifestado por Larreta: "Pero tengo una responsabilidad y la voy a hacer cumplir", señaló.

Aunque la mirada se mantiene distante sobre ese punto, aclaró que hubo un acuerdo para coordinar la presencia de las fuerzas de seguridad para verificar el cumplimiento de las medidas dispuestas para esta etapa y para monitorear la evolución en el nivel de ocupación de las camas de internación en la ciudad de Buenos Aires.

El miércoles por la noche, Fernández anunció que tomaría diversas medidas para mitigar la circulación del coronavirus, entre ellas, la limitación de los horarios comerciales y la suspensión de la presencialidad escolar en el AMBA durante 15 días.

El jueves al mediodía, el jefe de Gobierno porteño criticó públicamente las medidas, las calificó de inconsultas y anunció que concurriría a la justicia para cuestionar la decisión presidencial.

De hecho, el Gobierno de la Ciudad realizó esa presentación judicial este viernes, minutos antes de la reunión entre Fernández y Rodríguez Larreta.

La acción declarativa de inconstitucionalidad, que incluye un pedido de no innovar, apunta contra el artículo 2 del Decreto 241/2021 publicado en el Boletín que entró en vigencia la pasada medianoche con el argumento de que allí se viola "la autonomía de la Ciudad e implica una virtual intervención federal porque se arroga potestades sobre competencias eminentemente locales, en salud y educación".

**La reunión**

El presidente Alberto Fernández se reunió a solas con el jefe de Gobierno porteño, Horacio Rodríguez Larreta, para analizar lasnuevas medidas implementadas desde este viernes por el gobierno nacional en el Área Metropolitana Buenos Aires (AMBA)para mitigar la expansión de la segunda ola de coronavirus.

Rodriguez Larreta y el vicejefe Diego Santilli arribaron a las 9.55 a la residencia de Olivos, y de inmediato comenzó el encuentro entre el Presidente y el titular del Ejecutivo porteño, a solas.

En tanto, Santilli permaneció con el secretario General de la Presidencia, Julio Vitobello, en otra sala.

El encuentro se produce en el marco de las nuevas medidas implementadas en el AMBA, que incluyen la suspensión de las clases presenciales por 15 días a partir del lunes, medida con la que Rodríguez Larreta expresó el jueves su desacuerdo y que derivó en la presentación, antes del inicio de la reunión en Olivos, de una acción declarativa de inconstitucionalidad ante la Corte Suprema de Justicia de la Nación por parte del Ejecutivo porteño.

El jueves,Rodríguez Larreta había hecho un pedido público para reunirse con el mandatario, en el marco de una conferencia de prensa.

La reunión fue acordada el jueves por la tarde, luego de que Santilli se comunicara con Vitobello para pedir la audiencia con el primer mandatario, que se confirmó una hora después, añadieron las fuentes.