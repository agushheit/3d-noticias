---
category: Estado Real
date: 2021-01-23T09:37:15Z
thumbnail: https://assets.3dnoticias.com.ar/sayago.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de la Provincia de Santa Fe
resumen: La provincia reacondicionará el sector de mamografías del hospital Sayago
title: La provincia reacondicionará el sector de mamografías del hospital Sayago
entradilla: "“Es imprescindible poner en funcionamiento el sector, porque tenemos
  el mamógrafo, pero en las condiciones actuales no lo podemos utilizar”, lamentó
  la directora del efector."

---
El Gobierno Provincial, a través del Ministerio de Infraestructura, Servicios Públicos y Hábitat, licitó las obras de refuncionalización del sector de mamografía y demolición del tanque de reserva de agua -en desuso- del hospital Provincial “Dr. Gumersindo Sayago” de la ciudad de Santa Fe.  
El acto licitatorio contó con dos ofertas y estuvo encabezado por la ministra de Infraestructura, Servicios Públicos y Hábitat, Silvina Frana; la secretaria de Arquitectura y Obras Públicas, Leticia Battaglia; y la directora del hospital Sayago, Claudia Mairone.  
Al respecto, la ministra manifestó que “cuando asumimos la gestión observábamos que había muchos hospitales y centros de salud en la provincia que requerían una intervención inmediata, como el caso del viejo hospital Iturraspe, que tuvimos que acondicionarlo en el marco de la pandemia. Y en este caso, el hospital Sayago precisa una intervención profunda, por eso comenzamos licitando una primera etapa de esta refuncionalización que estamos planteando, para mejorar el servicio de mamografía que no está funcionado porque las condiciones edilicias lo impiden, teniendo en cuenta que hay un mamógrafo en condiciones de ser usado”, aseguró.  
“Esta es otra de las cuestiones que dejó en evidencia la pandemia, la necesidad de mejorar muchos espacios de salud que requieren intervenciones integrales. Y en este sentido, quiero destacar que hay una firme decisión del gobernador Omar Perotti de seguir fortaleciendo el sistema sanitario, para ofrecerles a los santafesinos y santafesinas la atención que se merecen”concluyó Frana.  
En relación a las tareas a ejecutar, la secretaria de Arquitectura y Obras Públicas explicó que “esta es una primera etapa de obras para comenzar a reacondicionar el hospital de manera integral, que contempla unos 90 días de trabajos y un presupuesto oficial con base a julio de 2020, de casi 11 millones de pesos”.  
“Esta situación de pandemia, nos obligó a modificar los paradigmas de construcción, pero en un trabajo coordinado y mancomunado con los directivos y el personal del hospital, vamos a poder desarrollar las tareas, y sucesivamente, todas las obras necesarias hasta lograr el hospital que queremos”, señaló Battaglia.  
Finalmente, la directora del hospital Sayago indicó que “para nosotros es muy importante esta obra y desde ya estamos agradecidos al gobernador Omar Perotti y a la ministra Silvina Frana que siempre nos apoyaron en el pedido que hacíamos de refaccionar el hospital. Teniendo en cuenta que el nosocomio hace muchísimos años que no recibe ninguna ayuda, ni reformas. Es un hospital con muchas carencias y presta atención a una zona muy populosa del norte de la ciudad. Estamos entusiasmados con que las obras comiencen, porque el tanque de agua que está en desuso es un peligro y hay que demolerlo, pero también es imprescindible poner en funcionamiento el sector de mamografías, porque tenemos el mamógrafo, pero en las condiciones actuales no lo podemos utilizar”, destacó.  
  
**OFERTAS**  
Se presentaron dos oferentes para la refuncionalización del sector de mamografía y demolición del tanque de reserva de agua en desuso del hospital Sagayo. La primera oferta correspondió a la firma MT SRL, que cotizó la suma de $ 15.304.575. Mientras que la segunda y última oferta, fue presentada por la empresa ALAM-CO SA, por $ 15.278.810.  
  
**PRESENTES**  
Participaron del acto , el subsecretario de Planificación, Omar Romero; el director de la DIPAI, Damian Elaskar; el secretario ministerial, Andrés Dentesano; la vicedirectora del hospital Sayago, Eva Arguilar; y representantes de las empresas oferentes.