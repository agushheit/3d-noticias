---
category: La Ciudad
date: 2021-02-11T06:49:20-03:00
thumbnail: https://assets.3dnoticias.com.ar/playa-norte.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Avanza la urbanización de Playa Norte
title: Avanza la urbanización de Playa Norte
entradilla: "Esta semana finalizó la primera etapa que comprende relevamientos sociales,
  censos y estado habitacional del barrio. \n\n"

---
En el marco del Plan de urbanización del barrio Playa Norte de la ciudad de Santa Fe, concluyó esta semana el censo social - en conjunto con las organizaciones Manzanas Solidarias, CANOA y TRAMAS, la municipalidad de Santa Fe y representantes del gobierno nacional - que corresponde al programa "Argentina Unida por los Barrios Populares", dependiente del Ministerio de Infraestructura, Servicios Públicos y Hábitat de la provincia.

Se llevaron adelante reuniones informativas con vecinos y vecinas del barrio para poner en su conocimiento los aspectos más relevantes del proyecto de urbanización. En este sentido, el secretario de Hábitat, Vivienda y Urbanismo, Amado Zorzon, indicó: “Estamos trabajando con mucho entusiasmo para poder entregar a Nación el proyecto ejecutivo general de Playa Norte, para que pueda avanzar la urbanización y que estas familias puedan vivir con dignidad”.

![](https://assets.3dnoticias.com.ar/playa-norte2.jpg)

En esta instancia, concluye la "etapa social" del proyecto ejecutivo general de Playa Norte, que se encuadra en el programa nacional "Argentina Unida por los Barrios Populares", dependiente de la Secretaría de Integración Socio Urbana de la Nación. Cabe recordar que durante octubre de 2020, Nación, Provincia, el municipio santafesino y las organizaciones sociales firmaron un convenio en el que se comprometieron a ejecutar estas acciones.

 El censo se llevó a cabo en el barrio el lunes y martes de esta semana y arrojó que allí viven unas 123 familias (114 en Playa Norte y 9 en Bajo Judiciales). Muchas de ellas habitan precarias viviendas. La idea es contemplar esta demanda una vez iniciada la urbanización integral.

Al respecto, Ignacio Rico, subsecretario de Hábitat, Vivienda y Urbanismo, afirmó que “Playa Norte es un ejemplo de lo que un barrio popular organizado puede lograr. El Estado, después de tantos años de lucha de organizaciones sociales y vecinos, está interviniendo para reconocer derechos humanos básicos: como el acceso al agua, un hábitat digno, un lugar donde los pibes puedan jugar sin enfermarse”, aseguró.

![](https://assets.3dnoticias.com.ar/playa-norte1.jpg)