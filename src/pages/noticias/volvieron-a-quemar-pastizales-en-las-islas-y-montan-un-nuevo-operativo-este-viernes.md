---
category: La Ciudad
date: 2021-09-17T06:15:28-03:00
thumbnail: https://assets.3dnoticias.com.ar/INCENDIOS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Volvieron a quemar pastizales en las islas y montan un nuevo operativo este
  viernes
title: Volvieron a quemar pastizales en las islas y montan un nuevo operativo este
  viernes
entradilla: Apareció un gran foco de incendio en islas al sur de Sauce Viejo, que
  se hizo visible desde Santa Fe. Ante las quemas intencionales desde Protección Civil
  sostienen que "estamos siempre en la misma".

---
Luego de 15 días sin focos de incendios en las islas cercanas a Santa Fe, volvió a aparecer una gran columna de humo producto de la quema de pastizales al sur de Sauce Viejo. Desde Protección Civil afirman que se trata de un foco grande, por lo que se está organizando un operativo de acción de parte de la Brigada de Atención de Emergencias para combatir el fuego desde este viernes.

Roberto Rioja, quien es secretario de Protección Civil de la provincia, manifestó: "Hay un foco grande al sur de Sauce Viejo que es en el que queremos trabajar. Estamos organizando todo para armar un operativo ahí mañana, solo nos falta coordinar que nos llegue un helicóptero".

Los últimos incendios que se habían detectado en los humedales cercanos a la ciudad se extinguieron hace 15 días con la llegada de las lluvias, aunque se extendieron alrededor de 10 jornadas. Además de la brigada de reacción inmediata contra el fuego se habían involucrado bomberos zapadores para colaborar con la contención de los incendios que se propagaron por toda la zona de islas cercana a Alto Verde.

"Si podemos contar con el helicóptero mañana a primera hora llevaremos a cabo un operativo en ese lugar", afirmó Rioja sobre el plan de acción dispuesto por Protección Civil a través de la Brigada de Atención de Emergencias.

Con "todo listo" para el operativo, aguardan la llegada de alguno de los dos helicópteros que están en el sur provincial, también avocados a la quema de pastizales en ese lugar. "Veremos si podemos traer uno de los helicópteros afectados a los operativos del sur de la provincia y traerlo acá", afirmó Rioja.

Como historia repetida, el gran foco de incendio originado en los humedales al sur de Sauce Viejo se trata de una nueva quema de pastizales intencional. Con este panorama, el secretario de Protección Civil manifestó: "Estamos siempre en la misma. Yo creo que la mano del hombre está siempre en el medio, por dolo, por intención o no pero siempre está. Nosotros nos tenemos que avocar a evaluar cómo podemos contener los incendios".

Consultado por si en el transcurso de las dos semanas posteriores a la gran cantidad de incendios registrados en los humedales santafesinos hubo otros focos activos, Rioja advirtió que "solo hubo un par de columnas chicas que las controlamos sin problemas, pero el punto que se encendió ahora es un punto al que debemos atacarlo para que no se agrande".

Según informaron, la base operativa puede volver a ser montada en unas tres o cuatro horas en caso que se repitan episodios de quema de pastizales, aunque advirtieron que "si llueve el helicóptero se va a replegar" por la falta de seguridad para su uso.