---
category: Agenda Ciudadana
date: 2020-12-22T10:32:44Z
thumbnail: https://assets.3dnoticias.com.ar/2212incendios.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: El Litoral / Télam'
resumen: El año en el que más de un millón de hectáreas fueron arrasadas en el país
  por incendios forestales
title: El año en el que más de un millón de hectáreas fueron arrasadas en el país
  por incendios forestales
entradilla: Los incendios forestales reportados en 2020 quemaron más de un millón
  de hectáreas en distintas provincias de país, el 95% de ellos producidos por la
  acción humana y en un contexto de sequía prolongada.

---
«Hubo un máximo de **14 provincias con incendios en simultáneo y se quemaron más de un millón de hectáreas**. Hubo una sequía prolongada y el efecto que produce La Niña, que eleva las temperaturas y hace que los períodos de lluvia sean menos frecuentes, hizo que más cantidad de masa boscosa o pastizales se secaran con mayor rapidez y en más superficie», dijo  Alberto Seufferheld, director del Servicio Nacional de Manejo del Fuego (SNMF).

La superficie estimada afectada por incendios reportados entre el 1 de enero y el 15 de diciembre fue de 1.080.846,98 hectáreas en todo el país.

**Las provincias más comprometidas fueron Córdoba y Entre Ríos, mientras que la única que no presentó problemas de este tipo fue San Juan**.

En Buenos Aires se quemaron 1.104 hectáreas; en Catamarca 31.219; en Chaco 85.000; en Chubut 6,263; en Córdoba 326.800; en Corrientes 7.876; en Entre Ríos 309.460; en Formosa 20.459; en Jujuy 37.240; en La Pampa 5.695; en La Rioja 2.396; en Mendoza 13.908; en Misiones 1.731; en Neuquén 15.540; en Río Negro 36.357; en Salta 71.869; en San Luis 47.642; en Santa Cruz 302; en Santa Fe 19.058; en Santiago del Estero 29.532; en Tierra del Fuego 0,5; y en Tucumán 11.389.

«En condiciones normales, hay un porcentaje alto de afectación por el hombre, pero en condiciones húmedas no hay grandes superficies de fuego. Con las condiciones extremas que tuvimos, que estuvieron por encima de los márgenes históricos de temperatura y sequedad, se complicó», explicó Seufferheld.

Según el SNMF, hay causas naturales que producen el fuego, donde la más común es la caída de rayos producidos por tormentas eléctricas.

No obstante, **el 95 % de los incendios son por causas antrópicas, es decir por la acción humana y negligencia**, como fogones mal apagados o fuegos intencionales para deforestar.

«Los motivos del fuego dependen del lugar. En el Norte mucho tuvo que ver con el uso de la quema para sacar la caña en la cosecha y los rastrojos. En algunos lugares por necesidad de mejoramiento de pastura. En el Delta del Paraná fue para el ganado. Y después tuvo que ver con negligencias, como ahora en la Patagonia que se dio por la quema de residuos forestales sin cuidados», aclaró el director del SNMF.

Desde las organizaciones socioambientales se advirtió sobre los efectos del cambio climático en los incendios y se hizo un llamamiento a rever el modelo productivo por las quemas intencionales para el agro.

«Fue una de las peores temporadas de incendios en décadas. El avance de la crisis climática y ecológica, con todo lo que eso implica en el aumento de temperatura y cambios más bruscos del clima, produjo que los ecosistemas vean perjudicada su resiliencia, ya que se alteran los equilibrios de los bosques, humedales o selvas que son muy frágiles», aseguró Eyal Weintraub, integrante de Jóvenes Por el Clima.

«Estamos en una crisis económica gigantesca y se está buscando apoyarse en la agroindustria y la exportación de materias primas para poder generar las divisas para levantar la economía. Apoyarse en el modelo agroexportador significa aumentar la cantidad de hectáreas de soja o ganadería y ser menos rigurosos con el campo», alertó sobre las quemas.

Weintraub valoró como «positiva» la sanción de la Ley de Fuego, que impide realizar actividad agrícola, emprendimientos inmobiliarios o cualquier otro tipo de actividad que sea distinta al uso y destino que la superficie tenía al momento de iniciarse el fuego.

«No soluciona integralmente el problema, pero es un avance. La política nunca ha logrado responder o incorporar los reclamos ambientales, avanza poco a poco, pero por la urgencia de la crisis climática», afirmó el activista, quien advirtió que «no sabemos cuántas hectáreas se van a quemar el año que viene, esta tendencia va en alza».

En tanto, Seufferheld opinó que «**hay que cambiar la cultura, el uso del fuego como herramienta tiene que tener cierta formación**. Eso arranca desde lo local, pensar qué hacer con los depósitos de los residuos forestales y evitar los accidentes».

«Que este año no hayamos logrado ni siquiera una media sanción de la Ley de Humedales es una decepción, teniendo en cuenta que fue el año que más pegaron los incendios. Tenemos que invertir más en políticas de adaptación al cambio climático que preparen a nuestros territorios urbanos y rurales», concluyó el integrante de Jóvenes Por El Clima.