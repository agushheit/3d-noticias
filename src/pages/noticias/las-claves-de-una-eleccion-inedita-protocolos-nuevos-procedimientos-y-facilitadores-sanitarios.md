---
category: Agenda Ciudadana
date: 2021-09-06T06:15:21-03:00
thumbnail: https://assets.3dnoticias.com.ar/COMOCIOS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Las claves de una elección inédita: protocolos, nuevos procedimientos y
  facilitadores sanitarios'
title: 'Las claves de una elección inédita: protocolos, nuevos procedimientos y facilitadores
  sanitarios'
entradilla: ' "Es la primera elección que se va a dar en el marco de una pandemia
  y esperemos que sea la última", dijo a Télam la secretaria de Asuntos Políticos
  del Ministerio del Interior, Patricia García Blanco.'

---
A una semana de las primarias abiertas simultáneas y obligatorias (PASO), el gobierno nacional y las fuerzas políticas afinan los detalles para la realización de los comicios que se efectuarán en un marco inédito por la pandemia de coronavirus, lo que obligó a elaborar protocolos para evitar contagios y reformular los procedimientos, con la inclusión de la figura de unos 17 mil facilitadores sanitarios.

"Es la primera elección que se va a dar en el marco de una pandemia y esperemos que sea la última", dijo en una entrevista con Télam la secretaria de Asuntos Políticos del Ministerio del Interior, que gestiona las herramientas de política nacional electoral, Patricia García Blanco.

Ante los cambios de los procedimientos en el comicio, producto de las medidas sanitarias y de cuidado ante la situación epidemiológica, la funcionaria solicitó "primordialmente a todos los ciudadanos que controlen en el padrón sus nuevos lugares de votación" a través de la página web padrón.gob.ar,

Se dispuso una reducción de la cantidad de votantes en cada establecimiento, con un máximo de ocho mesas

Es que este año, como resultado de las medidas de prevención contra el coronavirus, la Cámara Nacional Electoral (CNE) dispuso una reducción de la cantidad de votantes en cada establecimiento, con un máximo de ocho mesas, por lo que se sumó un 25 % por ciento más de lugares, que de alrededor de 14.800 pasaron a unos 17.000 en todo el país, con más de 100.000 mesas habilitadas.

Igualmente, se colocarán más de 8 mesas en los lugares que lo permitan por su amplitud, y se evitarán las largas filas en los pasillos, donde se exigirá una distancia social de dos metros, con separaciones entre entrada y salida, mientras los ciudadanos deberán aguardar en el exterior del edificio e irán ingresando de acuerdo al ritmo que se produzca la votación.

Si aún hubiera personas afuera de los establecimientos aguardando su turno cuando a las 18:00 termine el horario de los comicios, se les dará un número "hasta el último que estuvo a esa hora, y van a seguir votando, o sea que lo que se va a demorar es el cierre de las mesas, porque habrá que esperar a que todos los electores que llegaron antes de las 18:00, voten", explicó la funcionaria.

Los integrantes de grupos de riesgo tendrán horario prioritario de dos horas por la mañana, y el resto de los votantes deberá cederles el turno, aunque en la ciudad de Buenos Aires se anunció que los mayores de 70 años tendrán la prioridad durante toda la jornada de votación.

Al chequear el padrón, García Blanco señaló que también "es muy útil para agilizar la votación recordar el número de orden en el que figuren, para que así las autoridades de mesa y los fiscales no tengan que rastrearlos por el número de DNI".

Entre otras sugerencias, se propone a los votantes que lleven su propia lapicera para firmar el padrón tras sufragar y que no utilicen saliva para cerrar los sobres con las boletas.

Además, se creó la figura de los facilitadores sanitarios, que serán integrantes del Comando General Electoral, y por lo tanto será un miembro de las fuerzas armadas o de seguridad nacionales.

Se propone a los votantes que lleven su propia lapicera para firmar el padrón tras sufragar y que no utilicen saliva para cerrar los sobres

Entre sus funciones, estarán las de ordenar el ingreso de votantes para no saturar la capacidad interna del local, constatar el uso de tapabocas, hacer respetar el distanciamiento social de 2 metros entre las personas y controlar que se cumpla la capacidad máxima de concurrencia en los locales de votación.

Las personas afectadas o aisladas por presentar síntomas o haber sido contacto estrecho de pacientes con coronavirus estarán eximidas de votar el 12 de septiembre. "La recomendación general es que no lo hagan, pero nosotros no podemos prohibir a nadie que ejerza sus derechos", dijo, y remitió las decisiones finales a la justicia electoral.

En ese sentido, la jueza federal con competencia electoral en la ciudad de Buenos Aires, María Servini, dictaminó que esas personas "no deben ir a votar" y que "deben continuar con el aislamiento obligatorio", lo cual será "causa suficiente para justificar la no emisión del voto" en los 70 días posteriores a la celebración del comicio.

También el juez electoral de la provincia de Buenos Aires, Alejo Ramos Padilla, se expidió en la misma línea y señaló que "todos los contagiados con Covid tienen prohibición de circular de acuerdo al DNU 494/2021, vigente hasta el 1° de octubre de 2021, lo que les impide ir a votar".

**Un sistema electoral robusto**

En tanto, García Blanco destacó la "transparencia, la participación y el control que tienen las fuerzas políticas del sistema electoral argentino" al que calificó como "muy robusto", dadas las certificaciones que deben aprobar los documentos "en todo el trámite del proceso, tanto en el escrutinio provisorio como el definitivo, que hace a la solidez del sistema" y lo tornan "muy fuerte".

La funcionaria de la cartera de Interior manifestó: "Cuando empezamos el tema de las elecciones, en plena pandemia el año pasado, el ministro del Interior (Eduardo "Wado" de Pedro) nos pidió el diálogo y la interacción con todas las fuerzas políticas, por cual hablamos con los apoderados de los partidos políticos de todos los distritos para conocer cuáles eran sus realidades y qué estaba pasando institucionalmente".

"Eso nos llevó a presentar el proyecto de suspender las caducidades de los partidos políticos, que sancionó luego el Congreso Nacional, y que fue muy importante porque como esta elección es de distrito, si no hubiéramos podido sacar esa ley, no podrían haber participado por haber caducado al no tener la cantidad de afiliados mínima y no haber elegido sus autoridades, agregó.

Recordó que "en el 2019 se cambiaron las formas de transmitir los datos y se estableció que en los lugares que tengan conectividad se permitirá pasar las planillas de datos, a través de un kit suministrado por el Correo (Centros de Transmisión de Establecimientos de votación - CTE), a la sede de esa empresa en el barrio porteño de Barracas, donde esa información se traslada a la empresa que hace el recuento".

En los establecimientos que no posean conectividad, los telegramas son recolectados por un "rondín del Correo, que los lleva a las denominadas Sucursales Electorales Digitales (SED), que agrupan a varios lugares de la jurisdicción, desde donde se transmiten, igual que en el 2019", señaló la funcionaria.

En ambos lugares, "hay fiscales informáticos de las agrupaciones políticas que participan de la elección quienes pueden sacar fotos, pueden filmar la transmisión, o sea que está fiscalizada esa transmisión", según precisó.

Para la prueba de estos sistemas, se realizaron varios simulacros de funcionamiento general de los procesos.

Para la prueba de estos sistemas, se realizaron varios simulacros de funcionamiento general de los procesos.

Para la prueba de estos sistemas, se realizaron varios simulacros de funcionamiento general de los procesos, los mecanismos de detección de errores y se documentaron posibles fallas, con la participación de los representantes de las diversas fuerzas políticas.

"El 28 de agosto pasado se realizó la última prueba, que resultó satisfactoria, tras corregirse una falla proveniente de un protocolo de intercambio de datos que había entre los sistemas de transmisión y de recuento, e incluso, por pedido de los fiscales informáticos partidarios, se hizo el simulacro de la puesta cero, que comprueba que ambos sistemas están a cero, no tienen datos previos cargados", concluyó la funcionaria.