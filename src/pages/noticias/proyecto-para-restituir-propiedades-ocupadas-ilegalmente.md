---
category: Agenda Ciudadana
date: 2022-06-21T08:17:59-03:00
thumbnail: https://assets.3dnoticias.com.ar/juanchi.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El parlamentario
resumen: Proyecto para restituir propiedades ocupadas ilegalmente
title: Proyecto para restituir propiedades ocupadas ilegalmente
entradilla: La iniciativa es impulsada por diputados de Juntos por el Cambio, que
  proponen modificar el Código Procesal Penal.

---
Diputados nacionales de Juntos por el Cambio, encabezados por **Juan Martín** (UCR Santa Fe), presentaron un proyecto de ley para modificar el Código Procesal Penal de la Nación y proveer de una herramienta concreta para reintegrar de manera inmediata propiedades ocupadas ilegalmente y poner límite a usurpaciones y ocupaciones ilegales de inmuebles privados y públicos.

La iniciativa prevé la restitución del terreno o inmueble “en cualquier estado del proceso y aun sin dictado de auto de procesamiento o de remisión a juicio, con la salvedad de que el derecho invocado por el damnificado fuere verosímil”.

“Proponemos herramientas concretas que coadyuven en el proceso judicial, pero también interpelamos a todos los niveles del Estado a trabajar para que esta situación no se siga replicando: Nación, provincia y sobre todo los municipios deben tener como eje evitar usurpaciones”, destacó Juan Martín, autor del proyecto.

Agregó: “Este accionar constante y deliberado contra la propiedad privada individual requiere la elaboración de instrumentos procesales ágiles, que piensen en garantizar una justicia temprana que repare el accionar de los usurpadores con el restablecimiento inmediato del derecho de posesión y tenencia del bien apropiado, desalentando de esta forma las tomas ilegales”.

“El déficit habitacional real y objetivo que nos interpela, no puede ser resuelto a expensas de los derechos de otros habitantes, como el de propiedad privada, también garantizado constitucionalmente, siendo su solución competencia de otras áreas y organismos del Estado. En última instancia, la ausencia o ineficacia de políticas públicas para resolver el déficit habitacional, no habilita a los particulares a que tomen por vías de hecho la propiedad inmueble estatal o de terceros”, advirtió el diputado santafesino.

“Resulta paradójico que titulares registrales de inmuebles deban emprender un litigio de desalojo contra los ocupantes ilegales y someterse a acciones dilatorias y a tiempos de un juicio ordinario para recuperar algo que le es propio y cuya titularidad surge de manera evidente de la documentación que acredita su propiedad”, señaló Juan Martín.

Indicó que “el número de denuncias por ocupación de inmuebles en todo el país creció particularmente en los últimos años y no hay respuestas eficientes por parte de las autoridades, que por momentos consienten y apañan ésta actitud delictiva”.

El legislador resaltó que “nada justifica que se usurpen terrenos ni inmuebles, se trate de terrenos privados o propiedad del Estado. La forma de exigir el cumplimiento del ejercicio del derecho a la vivienda no puede ser por mano propia sino de una política gubernamental que la reglamente equitativamente, considerando todos las prerrogativas e intereses en juego”.

Acompañan esta iniciativa los diputados de JxC **Mario Negri, Miguel Bazze, Fabio Quetglas, Mario Barletta, Manuel Aguirre, Lidia Ascárate, Gerardo Milman, Pablo Torello, Gabriela Lena, Pamela Verasay, Soledad Carrizo, Gerardo Cipollini, Francisco Monti, Roberto Sánchez, Aníbal Tortoriello, Carlos Zapata, Hugo Romero, Ximena García, Lisandro Nieri, Gustavo Bohuid, Marcela Coli, Juan Carlos Polini, Jorge Vara, Jorge Rizzotti, Marcos Carasso, Miguel Nanni, Karina Bachey, Carolina Castets** y **Alberto Asseff**.