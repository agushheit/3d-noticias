---
category: Agenda Ciudadana
date: 2021-09-12T06:15:48-03:00
thumbnail: https://assets.3dnoticias.com.ar/paso.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Cómo justificar el no voto por Covid y de cuánto es la multa por no votar
title: Cómo justificar el no voto por Covid y de cuánto es la multa por no votar
entradilla: Este domingo 12 de septiembre se celebrarán las Elecciones Paso que son
  obligatorias. Pero este año de pandemia hay nuevas excepciones y también hay multas
  por no cumplir con el deber cívico.

---
Este domingo 12 de septiembre se celebrarán las Elecciones Paso que son obligatorias. Pero este año de pandemia hay nuevas excepciones y también hay multas por no cumplir con el deber cívico.

Quienes no puedan ir a votar por una cuestión sanitaria, ya sea por aislamiento por contacto estrecho, síntomas o estar cursando la cuarentena por el virus, deberán justificar la eximición del sufragio. Una vez terminados los comicios, la Cámara Nacional Electoral habilitará la página de internet para que en el plazo de 60 días, la persona cargue sus datos y justificativo, y luego la Cámara evaluará dependiendo del certificado presentado, si ingresa o no al registro de infractores.

En la provincia de Santa Fe la persona sintomática o que se encuentre con sospechas de Covid no debe asistir a la votación, sino llamar inmediatamente al 0800-555-6549. Si algún votante llega al lugar de votación con un estado febril u otros síntomas se lo apartará de la fila y se lo mandará a su casa con indicación de llamar al call center de la provincia.

El Código Nacional Electoral estipula una multa de 50 a 500 pesos al elector mayor de 18 y menor de 70 años que no emita su voto y no lo justifique ante la Justicia nacional electoral en el plazo de dos meses luego de la respectiva elección. El incluido en el registro de infractores al deber de votar no podrá ser designado para desempeñar funciones o empleos públicos durante tres años.