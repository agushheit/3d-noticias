---
category: La Ciudad
date: 2021-02-17T06:53:54-03:00
thumbnail: https://assets.3dnoticias.com.ar/turismo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad de Santa Fe
resumen: Positivo balance del primer fin de semana largo en la ciudad
title: Positivo balance del primer fin de semana largo en la ciudad
entradilla: Se registraron picos del 100% en ocupación hotelera y un importante número
  de personas participó de las propuestas organizadas por la Municipalidad.

---
La Municipalidad de Santa Fe realizó un balance sobre los porcentajes de ocupación hotelera durante el fin de semana largo. Cabe destacar que, atentos a la situación epidemiológica, los hoteles funcionan al 50% de la capacidad total.

En hotelería de 4 estrellas, el promedio fue de 85% de ocupación y se llegó a picos de 100%. En tanto, en los de 3 estrellas, el porcentaje fue de 80% y se registraron picos del 100%.

Al respecto, el Intendente Emilio Jatón destacó que “estamos muy contentos con los resultados de este fin de semana, fundamentalmente porque para que esto sea una realidad, trabajamos conjuntamente todos los sectores que intervienen en el desarrollo de la actividad turística, en el marco del Safetur”. En ese sentido, valoró “el compromiso de todos los sectores: el hotelero, las agencias de viajes y turismo, y los prestadores de servicios entre otros, para cumplir con los protocolos establecidos para cada rubro, como así también la responsabilidad de recordar a los visitantes el deber de cumplir con las medidas de autocuidado. Esto hace que nos consolidemos como un destino seguro”, afirmó.

La ciudad de Santa Fe ofreció una variada agenda con propuestas turísticas, culturales y deportivas para todos los públicos. La misma fue elaborada conjuntamente entre los sectores público y privado, e incluyó desde visitas guiadas y actividades recreativas gratuitas en diversos espacios de la ciudad hasta paseos turísticos ofrecidos por operadores privados locales. Vale destacar que esta agenda se promocionó a nivel regional y provincial, de forma mancomunada con el sector privado.

Los resultados obtenidos este fin de semana generan buena expectativa para el resto del año, que estará marcado por la reactivación del turismo a nivel mundial, regional y local. Esta etapa de nueva normalidad en el marco de la pandemia por Coronavirus, exige el cumplimiento de los diversos protocolos existentes para resguardar la salud de los visitantes y la ciudadanía en general. En particular, la ciudad de Santa Fe cuenta con una serie de protocolos que posibilitaron su reconocimiento como “Destino seguro” por el Consejo Mundial de Viajes y Turismo.

Una muestra del movimiento turístico en la capital de la provincia fue la convocatoria que tuvieron las visitas guiadas gratuitas del programa “Mi ciudad como turista”. El sábado se realizó el  Camino de la Constitución, en su versión abreviada, y contó con la participación de 50 personas entre las que se registraron turistas de Córdoba, Rosario y Capital Federal. En tanto, la Manzana Jesuítica, que funcionó con horarios especiales, también fue una de las atracciones gratuitas más visitadas por turistas y ciudadanos. El paseo recibió más de  70 personas en total y entre los turnos para visitas guiadas, se registraron turistas de Rosario, Buenos Aires, Entre Ríos, Catamarca y Colombia.

Se destaca, además, la gran afluencia de excursionistas y turistas en los paradores de playa, donde se realizaron actividades de kayaks, kitesurf, paddle surf y otros deportes náuticos.

Por su parte, las agencias de viajes y turismo dieron cuenta de un buen flujo de demanda sobre los productos ofrecidos como city tours, paseos náuticos, jornadas de pesca, recorridos gastronómicos y cerveceros, entre otros. Consultadas sobre el nivel de demanda en paseos náuticos, el Pool de Agencias de Viajes y Turismo, expresó que La Ribereña funcionó a un 100% de cupo, de acuerdo a lo establecido por los protocolos vigentes. El retorno al agua de La Ribereña durante el fin de semana de Carnaval es el resultado del trabajo conjunto entre el estado Municipal, Prefectura Naval Argentina y el Pool de Agencias, con el objetivo de sumar servicios turísticos de calidad a la oferta local.

Finalmente, el Catamarán Costa Litoral fue otro de los servicios más requeridos durante el fin de semana largo. Desde la empresa destacaron que todas las salidas de la embarcación desde el Dique 1 del Puerto local fueron con un 100% de la capacidad, siempre teniendo en cuenta que existen protocolos específicos para cada rubro, en el marco de la pandemia por Coronavirus.

**Consultas turísticas**

En promedio, alrededor de un 30% de los visitantes se acercó a realizar consultas en los Centros de Información Turística de la Municipalidad. El mayor número de requerimientos fue recibido en el centro de informes de la Terminal.