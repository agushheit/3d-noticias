---
category: Agenda Ciudadana
date: 2021-11-23T06:15:44-03:00
thumbnail: https://assets.3dnoticias.com.ar/VACAS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Frigoríficos locales hablan de subas "estacionales" y reconocen que hubo
  un "alza exagerada"
title: Frigoríficos locales hablan de subas "estacionales" y reconocen que hubo un
  "alza exagerada"
entradilla: 'Los aumentos en algunas carnicerías de la ciudad se acercan al 20%. El
  gobierno busca moderar el impacto de la suba de los valores de la hacienda vacuna
  en pie.

'

---
El gobierno nacional intenta moderar el impacto de la suba de los valores de la hacienda vacuna en pie en los precios de la carne a nivel minorista. Sin embargo, y desde la semana pasada, algunas carnicerías de la ciudad ya comenzaron a trasladar algunas subas. Antonio D'Angelo vicepresidente de la Cámara de Frigoríficos de Santa Fe, advirtió "que en este país no solo aumenta la carne, sino que aumenta absolutamente todos".

Explicó y diferenció dos situaciones: "A mediados de noviembre, como todos los años, hay aumento de la carne". Habló de un "aumento estacional, anterior a las fiestas, y que sucede siempre. En noviembre y diciembre hay una tendencia al alza que puede estar en el uno y dos por ciento. Así como para las pascuas aumenta el chocolate, para las fiestas (a partir de noviembre) aumenta la carne".

Lo cierto es que los últimos incrementos, algunos de los cuales ya se trasladaron a la góndola, estuvieron muy por encima de las subas típicas que mencionó D'Angelo. En ese sentido, reconoció: "Lo que pasó la semana pasada no sabemos por qué ocurrió, ha sido un alza exagerada. Hay que ver si es aceptada por los consumidores".

Las explicaciones esgrimidas por los diferentes eslabones de la cadena cárnica respecto de los aumentos en el precio de la hacienda, consideran una multiplicidad de factores que impulsaron los aumentos.

Uno de ellos es la merma en la oferta de hacienda "gorda" para faena por parte de los feedlots, que hace ya un tiempo, debido a los problemas para reponer animales en los corrales por cuestiones de rentabilidad y encarecimiento de la hacienda, llevaron a que hoy la ocupación de los mismos se ubique por debajo de lo normal (menos del 65%).

A esto se suma una baja de oferta estacional de animales frente a un aumento de la demanda por fin de año, y cuestiones básicamente referidas a la inflación que produjeron un retraso en los valores de la hacienda.

D'Angelo sostuvo que en el inicio de la cadena se producen los grandes aumentos. "Si quieren saber si aumentó o no el precio de la carne escuchen mañana a la mañana qué precio hace la hacienda en pie en Liniers, en el mercado formador de precios del país. Y ahí van a saber si hay aumentos o no. Ese aumento, al término de tres o cuatro días se traslada al interior y se traslada a las carnicerías, que es el último eslabón de la cadena", sostuvo en declaraciones al programa "De 10" que se emite por "LT10"

La luz de alerta se prendió semanas atrás, luego del salto registrado en los precios de la hacienda en pie en el Mercado de Liniers, entre el viernes, 12 de noviembre, y el martes 16. Entre ambas jornadas, los precios de las principales categorías de consumo tuvieron un salto de entre 10% y 15%.

Según informó la agencia oficial de noticias Télam, el precio promedio del novillo pasó de $ 208,43 el kilo a $ 221,57, mientras que el novillito creció de $ 214,79 a $ 247,10 y la vaquillona de $ 208,46 a $ 237,13. Este incremento a nivel mayorista puede llegar a traducirse en un aumento de entre 20% y 25% en las carnicerías.

Está previsto que esta semana continúen las conversaciones entre el gobierno y el sector minorista de comercialización con el objetivo de arribar a una solución consensuada que garantice la estabilidad del precio de las carnes, evitando sobresaltos que vuelvan a golpear a los consumidores en el último tramo del año.

Consultado sobre las nuevas medidas que podría tomar el gobierno, el vicepresidente de la Cámara de Frigoríficos de Santa Fe comentó: "En la Cámara no tenemos ninguna información. Son palabras que salen de funcionarios pero que no se plasman en un papel, ni hay reuniones para saber en definitiva la medida que se adoptó. Es cierto que el día jueves a la tarde se reunió el ministro de la Producción Julián Domínguez con el secretario de Comercio Interior para tratar entre otros temas el aumento de la carne. No sabemos cuáles son las medidas que se van a adoptar en definitiva"

Con respecto a la posibilidad de que la nación aumente las retenciones al sector, D’Angelo apuntó: "Por supuesto que preocupa. Es una de las herramientas que tiene todo gobierno para manejar mercado interno e internacional". Igualmente, admitió que "esa herramienta bien usada puede ser más eficiente que la clausura o cierre de la exportación".