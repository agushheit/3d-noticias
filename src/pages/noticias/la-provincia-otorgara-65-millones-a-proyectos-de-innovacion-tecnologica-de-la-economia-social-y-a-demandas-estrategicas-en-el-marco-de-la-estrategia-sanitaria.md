---
category: Estado Real
date: 2021-06-13T06:00:36-03:00
thumbnail: https://assets.3dnoticias.com.ar/INNOVACION.png
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Provincia otorgará $65 millones a proyectos de innovación tecnológica,
  de la economía social y a demandas estratégicas en el marco de la estrategia sanitaria
title: La Provincia otorgará $65 millones a proyectos de innovación tecnológica, de
  la economía social y a demandas estratégicas en el marco de la estrategia sanitaria
entradilla: 'El monto corresponde a 46 proyectos seleccionados en las convocatorias:
  INNOVAR, POES y DEMES, de la Agencia Santafesina de Ciencia, Tecnología e Innovación.'

---
En el marco de las acciones que la Secretaría de Ciencia, Tecnología e Innovación del Ministerio de Producción, Ciencia y Tecnología viene realizando en post de estimular, promocionar e invertir en ciencia, tecnología e innovación en las áreas estratégicas que generen una transformación de Santa Fe, se realizará la adjudicación de los proyectos presentados en las convocatorias 2020: Innovar Santa Fe, Proyectos orientados a la economía social (POES) y Demandas estratégicas en el marco de la emergencia sanitaria (DEMES).

En estos llamados el monto total a otorgar es de más de 65 millones de pesos que corresponden a 46 proyectos seleccionados: 21 en INNOVAR, 13 en POES y 12 de DEMES.

**INNOVAR**

Este instrumento busca fortalecer las capacidades competitivas de las PyMEs de la provincia de Santa Fe, a través del incentivo para la realización de proyectos de Desarrollo Tecnológico e Innovación que contribuyan a mejorar su productividad, a la incorporación de recursos humanos calificados y a fortalecer las alianzas y/o vínculos con los diferentes actores del sistema de Ciencia, Tecnología e Innovación de la Provincia de Santa Fe.

En el llamado 2020 fueron seleccionados 21 proyectos para los cuales se ha asignado un financiamiento total de $ 44.276.050.

Para acceder al listado de proyectos seleccionados para su financiamiento en la convocatoria Innovar Santa Fe 2020, ingresar aquí.

**POES**

A través de este instrumento se busca financiar proyectos que tengan como meta la implementación de nuevas prácticas tecnológicas para impulsar procesos de desarrollo social y territorial, con eje en poblaciones vulnerables, pequeños productores, microemprendimientos, y/o regiones de la provincia con capacidades productivas poco desarrolladas.

En el llamado 2020 fueron seleccionados 13 proyectos para los cuales se ha asignado un financiamiento total de $ 10.092.575.

Para acceder al listado de proyectos seleccionados para su financiamiento en la convocatoria Innovar Santa Fe 2020, ingresar aquí.

**DEMES**

Este instrumento busca fomentar, dentro del sistema de ciencia, tecnología e innovación de la provincia de Santa Fe, la implementación de proyectos de investigación y desarrollo orientados a atender las problemáticas socio productivas que se generaron a partir del nuevo contexto de la pandemia COVID-19. Los proyectos deberán desarrollar actividades de I+D aplicada y de innovación tecnológica, que estén orientados a resolver demandas relacionadas a las problemáticas económicas, sociales y productivas de la provincia de Santa Fe, explicitando la aplicabilidad y transferencia de los resultados esperados.

En el llamado 2020 fueron seleccionados 12 proyectos para los cuales se ha asignado un financiamiento total de $ 11.157.739