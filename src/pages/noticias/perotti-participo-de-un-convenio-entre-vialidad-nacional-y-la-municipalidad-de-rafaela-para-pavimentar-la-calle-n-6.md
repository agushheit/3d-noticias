---
category: Estado Real
date: 2021-01-15T04:33:52Z
thumbnail: https://assets.3dnoticias.com.ar/firma-convenio-rafaela.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Convenio entre Vialidad Nacional y la Municipalidad de Rafaela para pavimentar
  la Calle nº 6
title: Convenio entre Vialidad Nacional y la Municipalidad de Rafaela para pavimentar
  la Calle nº 6
entradilla: "«De esta forma vamos concretando la estructura productiva como vínculo
  importante, con la variante que le va a dar a la ciudad la alternativa de incorporar
  suelo industrial», afirmó el gobernador durante el acto."

---
El gobernador Omar Perotti participó este jueves de la firma de un convenio entre Vialidad Nacional y el municipio de Rafaela, en el marco del «Programa de sostenimiento de la red troncal con aplicación en jurisdicciones locales».

En la oportunidad, se avanzó sobre la obra «Pavimentación Calle N° 6, entre Ruta Nacional N° 34 y variante Ciudad de Rafaela», cuya inversión estimada asciende a $124.300.000.

Perotti afirmó:  «es una muy buena noticia para Rafaela y la región. De esta forma vamos concretando la estructura productiva como vínculo importante, con la variante que le va a dar a la ciudad la alternativa de incorporar suelo industrial y la comunicación, que potencia la seguridad vial al moverse el tránsito pesado por vías distintas a las de la movilidad diaria de la ciudad».

Esta obra, largamente ansiada por las fuerzas productivas de esa ciudad, había tenido un fuerte impulso desde 2017, cuando el actual gobernador Perotti se desempeñaba como senador nacional.

Durante el acto, el gerente de Regiones de Vialidad Nacional, Patricio Gracia, afirmó que la relación «con el distrito vial Santa Fe hace que podamos ir avanzando con otros temas y ser operativos. Vamos a cumplir con el compromiso de la cantidad de kilómetros que prometimos».

A su turno, el intendente de Rafaela, Luis Castellano, valoró la «velocidad con quienes estamos aquí y venimos gestionando hace mucho tiempo y, por supuesto, la variante de la ruta 34 y también los caminos 5 y 6 para la generación de esa vinculación muy importante para todo nuestro sector industrial, pero también para la sociedad en general».

«Es una muy buena noticia, fue rápido, ha sido expeditivo. Son muchos años que hemos venido haciendo esto y nos parece increíble que hayamos tenido una reunión sobre la ruta, inauguramos un tramo y ahora estamos firmando. Eso se debe a la voluntad política que tiene el presidente Fernández y el gobernador Perotti», insistió Castellano.

En tanto, el presidente del Centro Comercial e Industrial de Rafaela y las Regiones, Diego Castro, expresó su agradecimiento a las autoridades y remarcó la celeridad que se tuvo desde la reunión.

A través de esta iniciativa, Vialidad Nacional firma convenios con las administraciones locales para implementar una política pública que contemple y abarque las necesidades locales, en los ejes de conectividad, productividad, integración y seguridad con el sentido de propiciar un impacto en la economía local de la jurisdicción, resolviendo de manera inmediata problemáticas de generación de mano de obra intensiva, reactivación de la construcción y desarrollo económico.

Con el mejoramiento de los caminos se busca maximizar los recursos del Estado y reducir los riesgos de incidentes viales, proponiendo caminos más seguros.

<br/>

### **EL CONVENIO**

El administrador de Vialidad Nacional, Gustavo Arrietta; y la Municipalidad de Rafaela, a través del intendente Luis Castellano, acuerdan aunar esfuerzos para resolver necesidades que requieran soluciones viales dentro de las jurisdicciones locales, con el objetivo de mejorar la transitabilidad, seguridad vial, confort y conectividad, prestando especial interés en los accesos a las ciudades, desvíos, conexiones a las demás localidades y a la Red Vial Nacional, circunvalaciones y promoción de proyectos para caminos de fomento agropecuario, minero, forestal, industrial y turístico, siempre y cuando resulten obras complementarias a la red troncal nacional.

En este sentido las partes involucradas analizarán la viabilidad de proyectos encaminados a:

\- Financiar la realización de obras viales conexas a rutas nacionales, o su bacheo o puesta en valor;

\- Financiar la realización de obras sobre calles de conectividad sustancial y relación directa con troncales nacionales, o relacionadas con procesos productivos locales y regionales de incidencia en la generación económica, como así también estamentos estatales de uso intensivo o estratégico.

\- Financiar la conectividad sobre caminos rurales que unan troncales nacionales, en sostenimiento de la vinculación con la producción agroganadera y de las economías regionales.

\- Financiar la mejora de los elementos de señalización vertical y horizontal de las arterias que se encuentran dentro del ámbito de relación directa con rutas nacionales.

\- Financiar la reparación o nueva instalación de luminaria de las arterias conexas a rutas nacionales, en el marco de mejorar la visibilidad y seguridad del entorno;

\- Financiar el mantenimiento de banquinas y caminos rurales que sirvan de conexión directa con rutas nacionales.

\- Financiar obras de arte conexas a rutas nacionales.

El plazo de duración del presente acuerdo será de un (1) año contado a partir de la fecha de suscripción del presente, renovándose por períodos iguales a su vencimiento.