---
category: Agenda Ciudadana
date: 2021-02-15T06:30:42-03:00
thumbnail: https://assets.3dnoticias.com.ar/camaradiputados.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Diputados podrá abrir el debate del Impuesto a las Ganancias en los próximos
  días
title: Diputados podrá abrir el debate del Impuesto a las Ganancias en los próximos
  días
entradilla: La intención del Frente de Todos es sancionar el proyecto en los primeros
  días del período de sesiones ordinarias, que será inaugurado el lunes 1º de marzo
  por el presidente Alberto Fernández

---
La Cámara de Diputados sancionará como primer tema del período de sesiones ordinarias que empieza el 1º de marzo el proyecto de reforma del Impuesto a las Ganancias que promueve el oficialismo, con el objetivo de que más de un millón de trabajadores no paguen ese tributo a partir de la sanción de la ley.

Si bien la idea es que el debate se realice en el recinto en los primeros días de marzo, la iniciativa se comenzaría a debatir en la Comisión de Presupuesto en los próximos días, ya que el Jefe de Gabinete, Santiago Cafiero, adelantó que el Presidente habilitará su tratamiento en el período extraordinario de sesiones, que concluye el 28 de febrero.

El titular de la Cámara de Diputados, Sergio Massa, presentó hace una semana la propuesta de reformar el Impuesto a las Ganancias, que fue diseñada juntos a los equipos de la Jefatura de Gabinete, del Ministerio de Economía y de la AFIP.

El proyecto eleva el mínimo no imponible a 150.000 pesos brutos mensuales, con lo cual a los trabajadores que cobren hasta 124 mil pesos netos de sueldo no se les descontará el impuesto en la Cuarta Categoría de Ganancias.

Esta medida beneficiará a 1.267.000 trabajadores del total de 2 millones de asalariados registrados que hoy pagan ese tributo, según estimaron los autores del proyecto.

Además, la iniciativa establece que aquellos que cobren hasta 173 mil pesos brutos también sufrirán menores descuentos que en la actualidad, mientras que quienes cobran los salarios más altos de la pirámide pagarán los mismos valores que en la actualidad.

Uno de los puntos a resolver será como se compensará a las provincias el menor ingreso fiscal que presupone esta actualización del impuesto, ya que se trata de un tributo coparticipable.

La sanción de la iniciativa está asegurada ya que la propuesta recibió el respaldo, aparte del FdT, de la mayoría de los interbloques opositores, léase Juntos por el Cambio, Federal, Unidad y Equidad Federal, y las bancadas de Acción Federal y el Movimiento Popular Neuquino.