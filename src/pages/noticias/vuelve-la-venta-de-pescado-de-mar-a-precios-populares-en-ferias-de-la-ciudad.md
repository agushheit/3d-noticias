---
category: Agenda Ciudadana
date: 2022-02-22T10:35:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/fish-g21a241324_1920.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: Vuelve la venta de pescado de mar a precios populares en ferias de la ciudad
title: Vuelve la venta de pescado de mar a precios populares en ferias de la ciudad
entradilla: La Municipalidad informa los lugares donde se instalará el puesto de venta,
  durante esta semana. Se trata de productos de muy buena calidad y a precios accesibles.

---
La venta de pescado de mar a precios populares vuelve a Santa Fe Capital, con la intención de acercar productos de calidad a precios accesibles a más vecinos y vecinas. Según se indicó, durante esta semana, el puesto se podrá encontrar en: 

Miércoles 23 de febrero: Feria de las 4 Vías (Mariano Comas y Facundo Zuviría) 

Viernes 25 de febrero: Feria Cordial del Parque Garay (Presidente Perón y Caputto) 

Sábado 26 de febrero: Feria de La Verdecita (Balcarce y Alberdi) 

En todos los casos, el horario de atención es de 8 a 13. Además, se puede abonar con todos los medios de pago, incluida la Tarjeta Alimentar. 

La lista de precios es la siguiente: merluza HGT a $199 el kilo, filet de merluza a $650 el kilo, medallones a $450 las 10 unidades, cornalitos a $650 el kilo, corvina parrillera a $550, pollo de mar a $750, y calamar a $650 el kilo. 

Cabe recordar que la iniciativa se inscribe en el acuerdo alcanzado entre la Municipalidad y la Dirección Nacional de Políticas Integradoras del Ministerio de Desarrollo Social de la Nación, con la intención de complementar los productos de la economía social y popular que se ofrecen en las ferias de la ciudad.