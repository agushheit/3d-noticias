---
category: La Ciudad
date: 2021-01-25T09:37:36Z
thumbnail: https://assets.3dnoticias.com.ar/cuidacoches.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Con información de Diario El Litoral
resumen: Plantearán modificar la norma que regula a los cuidacoches para "ajustarla
  a la realidad"
title: Plantearán modificar la norma que regula a los cuidacoches para "ajustarla
  a la realidad"
entradilla: 'Se relevaron a la fecha 140 cuidadores de vehículos. El municipio adelantó
  que hará propuestas modificatorias de la normativa para que su implementación en
  la práctica sea más factible. '

---
A fines de mayo de 2019, el Concejo sancionó la ordenanza N° 12.635 con la que creó el programa inclusivo de cuidadores de vehículos. El objetivo era iniciar un proceso de incorporación de estas personas a la economía formal. Desde ese entonces y hasta ahora, basta con recorrer un poco la ciudad para ver que los cuidacoches están por todos lados, sin diferenciar si es zona de estacionamiento medido o no (la ordenanza prohíbe que estos trabajadores informales estén en las áreas medidas por el Seom). 

El municipio aseguró que se continuaron con los relevamientos en calle sobre estas personas y con la identificación de las zonas del ejido urbano donde más se asientan los cuidacoches. Pero además, "en los meses siguientes se elaborarán algunas propuestas de modificación de la ordenanza vigente para poder ajustarla más a la realidad de la actividad y asignar sobre el registro definitivo las zonas, horarios, límites y funciones", adelantó Mariano Granatto, secretario de Integración y Economía Social.

Las dudas siempre pasaron, en efecto, sobre la implementación efectiva en la práctica de la ordenanza.  Pero, ¿qué establece? En términos generales, la secretaría de Control de la Municipalidad será la encargada de crear el registro de "cuidadores de vehículos"; fijar el número de credenciales habilitantes; delimitar la zona en que los cuidadores de vehículos pueden ejercer su labor; asignar a cada cuidador de vehículo al menos un sector de una calle y/o espacio, la cual se indicará en la credencial correspondiente; y modificar previo aviso la asignación de calle, cuando se produzcan disposiciones o indicaciones que impacten en el desarrollo de la movilidad urbana. 

 

**Antecedentes**

En octubre de 2019 se abrió el registro reglamentado por el Ejecutivo Municipal. Se anotaron unas 30 personas de entre 30 y 65 años. Los interesados tenían tiempo de inscribirse hasta el 31 de enero de 2020. Incluso luego se extendió unas semanas más. Pero en el medio, llegó la pandemia del coronavirus: a partir del 20 de marzo del año pasado empezó a regir el Aislamiento Social, Preventivo y Obligatorio (Aspo) en todo el país, y ese registro quedó en algo así como un stand by.

El 20 de agosto de 2020, a instancias de la concejala Luciana Ceresola (Pro-Juntos por el Cambio), el Legislativo local le solicitó al Ejecutivo que informe sobre cumplimiento de la ordenanza referida al Programa Inclusivo de Cuidadores de Vehículos. "Existe un Registro de Cuidadores y queremos saber si se está implementando; necesitamos informar al ciudadano las pautas implementadas para mejorar la seguridad en las calles, y por ello solicitamos se nos informe la cantidad de credenciales otorgadas, las zonas habilitadas para realizar la actividad entre otros puntos", expresó la edila.

 "La retracción de la economía ha provocado que numerosas personas opten por esta actividad provocando conflictos en varias zonas de nuestra ciudad", añadió. Y en noviembre, se sancionó una nueva solicitud de informes, este último motorizado -sobre todo- por la agresión sufrida por un vendedor de flores por parte de un grupo de cuidacoches.

 

**Respuesta municipal**

Granatto compartió un informe con El Litoral respecto de lo relevado tras la asunción del gabinete de Emilio Jatón. "Al inicio de la gestión nos encontramos con un registro nominal de cuidacoches de la cuidad. El mismo se realizó durante el año 2019, y contaba sólo con un relevamiento de datos básicos. Además, según la información que se pudo constatar, en 2019 se realizó la entrega de algunos carnets que habilitaban la actividad", explicó el funcionario.

"Luego, durante el verano de 2020, se abrió la convocatoria para presentar la documentación requerida por la Ordenanza N° 12.635 que crea el programa inclusivo de cuidadores de vehículos. Dicha documentación es: Certificado médico y de vecindad, registro en plataforma nacional, dos fotos 4x4, constancia de no estar inscriptos en el registro de Deudores alimentarios morosos y certificado de buena conducta. Del registro inicial en el que había 700 inscriptos, pero sólo 75 completaron la documentación" exigida por la norma vigente.

"Observando la situación inicial descripta -prosiguió el secretario- , se definió realizar un relevamiento en calle. Se llevó a cabo durante 3 días en el mes de octubre: Jueves 22, Viernes 23 y Lunes 26 entre las 8 y las 13 horas. Tuvo lugar en Zona Sur, en el perímetro de Juan de Garay, Cruz Roja Argentina, J.J. Paso y Av. Freyre; Zona Centro: entre Juan de Garay, Av. , Pellegrini y Av. Freyre y La Costanera, incluyendo también calle Javier de la Rosa y Gral. Paz".

"El total de relevados en esa última oportunidad fue de 119: 7 mujeres y 112 hombres. Las edades oscilan entre los 19 como la menor y los 72 años como la mayor y en promedio la edad es de 41 años. De los entrevistados en calle, 51 comenzaron el registro en la anterior gestión municipal, y sólo 17 completaron la entrega de documentación", prosiguió Granatto.

"En líneas generales, la actividad de relevada se concentra en la zona que tiene con eje vertebral las calles Rivadavia, 25 de mayo y San Martín entre Bulevar ni como límite norte y General López como límite sur. En Zona Sur la concentración se visualiza en el Centro Cívico de la ciudad (Casa de Gobierno, Tribunales y Legislatura). Por otro lado, sobre Avenida la concentración se da en la zona del hospital Cullen y de la Clínica de Nefrología. En tanto que la Zona de la Terminal de Ómnibus y el Sanatorio Santa Fe representan otro polo de concentración de la actividad", puntualizó.

"Tras el relevamiento en calle, se continuó registrando en la oficina que es sede de la subdirección de Políticas de Convivencia. Allí se anotaron a 13 personas que realizan la actividad en diferentes zonas de la ciudad. También se cruzaron datos con un relevamiento de situación de calle que realizó la Dirección de Acción Social. El total de relevados hasta la fecha (sin profundizar turno tarde y noche, además de fines de semana), es de 140".