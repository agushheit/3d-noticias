---
category: Estado Real
date: 2021-01-23T08:40:45Z
thumbnail: https://assets.3dnoticias.com.ar/prieto.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de la Provincia de Santa Fe
resumen: La provincia solicitó a los santafesinos que retornan de sus vacaciones que
  realicen aislamiento social por 10 días
title: La provincia solicitó a los santafesinos que retornan de sus vacaciones que
  realicen aislamiento social por 10 días
entradilla: "“Es por el número de casos que estamos teniendo y el alto porcentaje
  de positividad en personas jóvenes y de mediana edad”, precisó el secretario de
  Salud, Jorge Prieto."

---
El secretario de Salud, Jorge Prieto, reiteró este viernes a los santafesinos y santafesinas que al retornar de sus vacaciones “es muy importante hacer un aislamiento social durante 10 días”.

En este sentido, precisó que “en este verano tan esperado, después de un largo año, al momento de decidir nuestras vacaciones es necesario preservarnos, cuidarnos y aislar el núcleo familiar”; y señaló que “durante nuestra estadía es importante tener en cuenta cualquier sintomatología, porque si enfermamos en nuestras vacaciones, cuando volvamos a casa vamos a traer el problema”.

Asimismo, Prieto detalló que ante “el número de casos que estamos teniendo y el alto porcentaje de positividad en personas jóvenes y de mediana edad que se está detectando en los chequeos que se están haciendo a través del programa Verano Cuidado, es muy importante hacer un aislamiento social cuando volvemos de nuestras vacaciones durante 10 días”; y en caso de tener sintomás “asistir al médico de cabecera o comunicarse al 0800 para tener el asesoramiento preciso”.

**Indicaciones para viajeros**

Antes del viaje, el Ministerio de Salud recomienda que se realice un aislamiento social de 10 días, sobre todo si se viaja con personas que se encuentran en edad de riesgo, para poder tener un habitáculo seguro y cuidado.

Durante el viaje es importante recordar que continuamos transitando una etapa de distanciamiento social, preventivo y obligatorio y que debemos seguir con los cuidados: uso de barbijo, distanciamiento social y lavado de manos.

Por otro lado, se deberá viajar con todos los permisos que se requieran, tanto desde Nación como de la provincia a la que uno se dirige. Además, es muy importante respetar las burbujas sanitarias y los espacios cuidados, y mantener los protocolos durante toda la estadía.