---
category: La Ciudad
date: 2021-07-18T06:00:36-03:00
thumbnail: https://assets.3dnoticias.com.ar/CASTRACIONESjpg.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Castraciones: cómo sigue la campaña gratuita en distintos puntos de la ciudad'
title: 'Castraciones: cómo sigue la campaña gratuita en distintos puntos de la ciudad'
entradilla: Con el objetivo de acercar el servicio a toda la ciudad, la Municipalidad
  de Santa Fe informa el cronograma de la próxima semana.

---
La Municipalidad de Santa Fe estableció cómo continúa el cronograma de la campaña de castración móvil, destinada a animales de compañía. Se recuerda que el servicio es gratuito y se ofrece a todos los vecinos y vecinas de la ciudad, con la intención de controlar la población felina y canina.

La atención se brinda con turnos programados que pueden obtenerse en la web oficial www.santafeciudad.gov.ar/turnos. Allí se debe buscar el apartado denominado “Castraciones”, se elige la opción “Solicitar un nuevo turno” y se completa el formulario con los datos pedidos. Posteriormente se escoge el día, el horario y la sede a la cual concurrir. El servicio se presta de lunes a viernes, excepto feriados, en el horario de 8 a 12, en todos los casos.

En algunos lugares el servicio ya arrancó la semana pasada y concluye el viernes próximo, en tanto que en los distritos La Costa y Noroeste, el cronograma se extiende hasta el 30 de julio.

– Distrito Suroeste: Fonavi San Jerónimo (Manzana 6) prosigue hasta el 23 de julio.

– Distrito Noroeste: La Tablada (Teniente Loza 6970), desde el 19 al 30 de julio.

– Distrito Norte: Vecinal Las Flores (Azopardo 7380) desde el 19 al 23 de julio.

– Distrito Centro/Este: Club Monteagudo (Regis Martínez 3002) desde el 19 al 23 de julio.

– Distrito Oeste: Los Hornos-Los Sin Techo (Gdor. Freyre 5678) hasta el 23 de julio.

– Distrito Este-Noreste: Vecinal Altos del Valle (Los Pinos 2492), hasta el 23 de julio.

– Distrito La Costa: Copa de leche de Los Sin Techos (Manzana 7) desde el 19 al 30 de julio.

– Instituto Municipal de Salud Animal: Parque Garay (Obispo Gelabert 3691).

– Instituto Municipal de Salud Animal: Jardín Botánico (San José 8400).