---
category: Agenda Ciudadana
date: 2021-09-27T06:00:52-03:00
thumbnail: https://assets.3dnoticias.com.ar/LAMMNES.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: 'Lammens: "El PreViaje es un éxito tremendo, estamos dando vuelta la página"'
title: 'Lammens: "El PreViaje es un éxito tremendo, estamos dando vuelta la página"'
entradilla: Desde el Ministerio de Turismo y Deportes señalaron que la segunda edición
  del programa tuvo, en 10 días de implementado, un ingreso de facturas por más de
  $10.000 millones.

---
El programa PreViaje llegó a "más de $ 10.000 millones de facturación en los primeros diez días" de su implementación, destacó el ministro de Turismo y Deportes, Matías Lammens.  
  
"PreViaje es un éxito tremendo. Esto tiene que ver con la nueva etapa: volver a viajar, a disfrutar y reencontrarnos", sostuvo Lammens sobre la segunda edición de la iniciativa.  
  
Desde la cartera de Turismo señalaron que en menos de dos semanas se ingresaron facturas en el sitio oficial del programa por el mismo monto total que lo ocurrido en los tres meses que duró el programa en su primera edición.  
  
"Cuando con el presidente Alberto Fernández decidimos lanzar esta segunda etapa de PreViaje fue convencidos de que este programa iba a seguir impulsando la reactivación que ya estamos viendo en la calle", explicó el ministro lammens.  
  
"Estamos dando vuelta la página y queremos que los argentinos y argentinas puedan volver a viajar y conocer las maravillas que ofrece nuestro país", agregó.  
Según datos oficiales, del total de facturas cargadas, el 53% corresponde a compras en agencias de viajes, 30% a alojamiento, 14% a transporte aéreo y un 3% a otros servicios.  
  
Los destinos más elegidos hasta el momento son Río Negro, Buenos Aires, Santa Cruz, Mendoza y Neuquén.  
  
De cara a la temporada de verano, Lammens afirmó: "Nos llena de entusiasmo los mensajes que recibimos de todo el país: los operadores, ministros y ministras de Turismo de las provincias ya están manejando niveles de reserva altísimos y nos hablan de la expectativa por el gran éxito que tendremos durante la temporada".

![Unas 600.000 personas ya participan de la segunda etapa del  programa, presentado en Puerto Iguazú por el presidente Alberto Fernández.](https://www.telam.com.ar/advf/imagenes/2021/09/6150a1a22bd1a.08.2021%20captura%20565_1004x565.jpg "Unas 600.000 personas ya participan de la segunda etapa del  programa, presentado en Puerto Iguazú por el presidente Alberto Fernández.")Unas 600.000 personas ya participan de la segunda etapa del programa, presentado en Puerto Iguazú por el presidente Alberto Fernández.

  
Alrededor de 600.000 turistas ya están participando de la segunda edición de PreViaje, una inversión histórica del Estado nacional para reactivar al sector luego de su mayor crisis global.  
  
Según datos oficiales, se trata de una facturación de $10.368.870.867, una cifra récord para los primeros 10 días de su implementación.  
  
El Presidente había sido el encargado de lanzar esta segunda edición del PreViaje el mes último, desde Iguazú, en Misiones.  
  
Como en la primera edición, el sistema de compra anticipada de servicios apunta a impulsar la demanda de turismo interno, dinamizar la economía a través de ingresos genuinos al sector gracias a esta operatoria, y fomentar la distribución de recursos en forma federal hacia los actores de esa cadena.

![Jubilados y afiliados al PAMI tedrán con el PreVviaje un reintegro del 70% de sus gastos. Foto Nacho Corbalan.](https://www.telam.com.ar/advf/imagenes/2021/08/61261e31d6d46_1004x565.jpg "Jubilados y afiliados al PAMI tedrán con el PreVviaje un reintegro del 70% de sus gastos. Foto Nacho Corbalan.")Jubilados y afiliados al PAMI tedrán con el PreVviaje un reintegro del 70% de sus gastos. Foto Nacho Corbalan.

Como parte de la misma idea, el viernes último se lanzó el programa PreViaje para jubilados y afiliados al PAMI, cuyo eje central es un reintegro del 70% sobre los gastos por compra anticipada de viajes turísticos por Argentina desde noviembre próximo y hasta diciembre 2022, y que incluye rubros como alojamiento, alquiler de automóviles, atractivos turísticos; excursiones y gastronomía, ente otros ítems.  
  
Desde el PAMI precisaron que "la devolución del 70 % de los gastos en crédito para usar en toda la cadena turística, tendrá una aplicación automática y retroactiva a noviembre".  
  
"Una vez que un afiliado o afiliada de PAMI se registra en PreViaje, el sistema computa la bonificación correspondiente", señalaron.  
  
A la hora de explicar cómo funciona este programa, se destacaron seis pasos: Se debe ingresar al sitio PreViaje y registrarse con la cuenta de Mi Argentina, luego se selecciona un prestador turístico que debe estar inscripto en el programa.  
  
Para efectuar las compras, el beneficiario crea "tu viaje", carga los datos de origen, destino, fecha de salida y regreso, entre otros.  
  
Luego, deberá cargar los comprobantes, se aceptan facturas y/o recibos B y C, así como también boletos de transporte; crédito a favor del beneficiario y, una vez validados los comprobantes, recibirá el crédito en una tarjeta precargada o a través de BNA+.  
  
El beneficiario podrá utilizar su crédito desde la realización del viaje hasta el 31 de diciembre de 2022, en toda la cadena turística.  
  
Respecto a las compras que generan crédito, desde el PAMI aclararon que son "las realizadas a prestadores turísticos inscriptos, entre el 12 de agosto y el 31 de diciembre de 2021, para utilizar en territorio nacional desde la fecha de comienzo del viaje y hasta el 31 de diciembre de 2022".