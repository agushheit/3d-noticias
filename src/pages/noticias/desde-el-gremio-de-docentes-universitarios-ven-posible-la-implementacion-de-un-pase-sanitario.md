---
category: La Ciudad
date: 2022-01-27T06:00:26-03:00
thumbnail: https://assets.3dnoticias.com.ar/unl.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Desde el gremio de docentes universitarios ven posible la implementación
  de un pase sanitario
title: Desde el gremio de docentes universitarios ven posible la implementación de
  un pase sanitario
entradilla: 'Las universidades de Santa Fe discutirán el pase sanitario entre febrero
  y principios de marzo. El sindicato de docentes de la UNL da luz verde al debate

  '

---
Ya son cuatro las universidades en Argentina que anunciaron la implementación del pase sanitario de cara al ciclo lectivo 2022. El paso lo dieron la Universidad Nacional de La Plata (UNLP), la Universidad Nacional de La Matanza (UNLAM), dos sedes de la Universidad Tecnológica Nacional (UTN), y la Universidad Nacional de Rosario. Mientras tanto en la ciudad de Santa Fe tanto la UTN como la Universidad Católica y la Universidad Nacional del Litoral (UNL) continúan con receso y las primeras reuniones para comenzar a hablar del tema están planeadas para mediados de febrero. Las definiciones llegarían la primera semana de marzo, según se pudo conocer en diálogo con las diferentes autoridades universitarias y con los docentes.

Por su parte desde la Asociación de Docentes de la UNL (Adul) ya comienzan a analizar el pase sanitario para el inicio de clases, aunque con cautela. Al consultar sobre si resulta posible la implementación de esta medida el secretario adjunto del sindicato, Oscar Vallejos, adelantó: "Sí, entendemos que es posible y que ese era el sentido en un contexto anterior, cuando discutimos en septiembre. En las últimas asambleas de 2021 que tuvimos, los docentes universitarios plantearon que los estudiantes estuvieran vacunados en las instancias presenciales. Pero en ese momento no había una presencialidad masiva en la universidad. El contexto era otro con una presencialidad organizada por los docentes cuando aún no estaba la idea del pase sanitario".

En esta línea destacó la necesidad de que las universidades lleguen a tomar esa decisión en conjunto con los gremios docentes y no docentes. Sobre los anuncios de la implementación del pase en cuatro universidades, Vallejos opinó: "Son posiciones muy apresuradas y que en todo caso son decisiones en las que los gremios deben participar. Cuando discutíamos con nuestros compañeros docentes, algunos nos decían que necesitamos volver a la presencialidad y que desde el gremio se trabaje para garantizar eso. Y hemos trabajado en ese sentido para los docentes de la UNL. Creemos que va a haber dos debates este año: el de los preuniversitarios y el universitario. Son distintos".

"La semana que viene lo vamos a evaluar junto a las autoridades de la UNL en una comisión especial para condiciones de trabajo. En esa primera reunión se comenzará a delinear cuáles serán las condiciones y en función de eso haremos asambleas para consultar y tener la opinión de las bases", confirmó el secretario. Calculan que el 70 por ciento del estudiantado está vacunado, "pero no sabemos quién sí o quién no, entonces ahí va a haber una tensión seguramente".

Además destacó que no se debe acordar solo el pase sanitario, sino la presencialidad en general. "Hasta ahora nos hemos puesto de acuerdo en casi todos los puntos porque se respetan las condiciones que establecemos y creo que en esto también nos pondremos de acuerdo", comentó. Y evaluó que el pase sanitario aceleró las vacunaciones. "No por cuestiones laborales, sino sociales, por las vacaciones. No sabemos cómo está distribuido en la población universitaria pero debe ser importante sobre todo en estudiantes", agregó.

En relación a las formas de implementación del pase sanitario en la UNL, Vallejos advirtió: "Es difícil de implementar el control, estamos hablando de 5.000 personas por día en la ciudad universitaria. Es una cantidad de estudiantes dando vueltas enorme tanto en la ciudad de Santa Fe como en Esperanza y otras localidades. Hay que discutirlo. Los resguardos que hay que tomar para que nadie se contagie en los lugares de trabajo y después las posibilidades operativas de que esos controles se hagan".

"Veremos ahora cómo nos arreglamos. El problema es que si hay presencialidad y hay estudiantes que no se quieren vacunar hay que garantizar clases. Entonces la pregunta es si habrá que dar clases virtuales para quienes no estén vacunados, eso cambia las condiciones de trabajo. No podés dar clases presenciales en el aula y virtuales al mismo tiempo. Hasta ahora estuvimos dando clases en la presencialidad en la universidad la gran mayoría, salvo las instancias que requieren entrenamiento físico. Para eso hay que pensar políticas para atender que esa población no vacunada tenga clases".

Por último, sobre el plantel docente que no se vacunó, Vallejos respondió: "Hay docentes que también se han comunicado con nosotros que no se han vacunados, las razones por las que no se vacunan son diversas. Como gremio apoyamos la vacunación y que es una forma de cuidado colectivo. La perspectiva es que es una cantidad mínima de docentes. La normativa dice que deben informar, hacer una presentación sobre que no se han vacunado. Y el centro de salud de la UNL los tiene que citar, deben tener una entrevista y allí se les da las razones de por qué es importante la vacunación y después darles un tiempo para que se vacunen. Eso la UNL lo ha hecho en algunos casos pero aún falta".