---
category: Agenda Ciudadana
date: 2021-06-07T06:00:44-03:00
thumbnail: https://assets.3dnoticias.com.ar/AGUINALDO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Cómo se pagará el aguinaldo a los empleados públicos santafesinos
title: Cómo se pagará el aguinaldo a los empleados públicos santafesinos
entradilla: Liquidación del Sueldo Anual Complementario y sueldo de junio.

---
La Asociación Trabajadores del Estado (ATE) informó cómo será la liquidación del medio aguinaldo y del sueldo de junio para los trabajadores estatales.

Al respecto, el gremio informó que el Sueldo Anual Complementario se abonará en el transcurso de junio (aún no hay fecha de pago) y para el mismo se tomará como base de cálculo el sueldo de este mes, que incluye el 8% de aumento.

También serán tenidas en cuenta las sumas no remunerativas y no bonificables, tal como se acordó en las paritarias.

En tanto, el sueldo de junio incluirá el segundo tramo del aumento acordado, es decir un 8 % de incremento sobre todos los conceptos, y también teniendo en cuenta las sumas no remunerativas y no bonificables.