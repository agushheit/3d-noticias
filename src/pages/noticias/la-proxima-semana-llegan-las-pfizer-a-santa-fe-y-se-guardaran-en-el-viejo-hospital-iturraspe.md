---
category: La Ciudad
date: 2021-09-10T06:00:10-03:00
thumbnail: https://assets.3dnoticias.com.ar/1112.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: La próxima semana llegan las Pfizer a Santa Fe y se guardarán en el viejo
  hospital Iturraspe
title: La próxima semana llegan las Pfizer a Santa Fe y se guardarán en el viejo hospital
  Iturraspe
entradilla: Este lote de vacunas será distribuido solo en Santa Fe y Rosario. El ministerio
  de Salud diagrama la logística para aplicarla en dos horas y esperan instrucciones
  sobre la población objetivo.

---
Con el arribo de las primeras 100.000 vacunas Pfizer al país la ministra de Salud provincial, Sonia Martorano, anunció que casi con toda seguridad la próxima semana arribe el primer cargamento a la provincia de Santa Fe. Desde Salud afirmaron a UNO que en principio, la distribución de los primeros lotes en territorio santafesino se hará solo para las ciudades de Santa Fe y Rosario.

Al respecto, la ministra sostuvo: “La semana que viene llegaría la primera partida y se comenzarán a utilizar. Vamos a esperar las indicaciones a nivel nacional porque pueden ser aplicadas como segundas dosis de Sputnik, que es una muy buena opción, o si se comienza con el grupo de 17 años sin factores de riesgo”.

La razón por la que se distribuirán solo en las dos ciudades más grandes de la provincia está directamente ligado al hecho de ser una vacuna de conservación compleja, debiéndose almacenar en un entorno de hasta 80 grados bajo cero para mantenerse útil.

Buscando mantener la cadena de frío necesaria para la conservación de la vacuna, al momento en que los inmunizantes lleguen a la capital santafesina serán almacenados en cuatro ultrafreezers. De estos cuatro, dos están dispuestos en una sala de frío especial montada en el viejo hospital Iturraspe y los dos restantes se encuentran en la sede del Centro Único de Ablación e Implante de Órganos (Cudaio) contigua al nosocomio.

Estos dispositivos fueron adquiridos por la provincia, gestionados en conjunto por el ministerio de Producción y Salud, especialmente para el operativo Pfizer. Con sus 360 litros de capacidad total, los ultrafreezer podrán albergar todo el primer lote que arribe a la ciudad cordial, aunque al desconocerse el tamaño en sí de las vacunas no se puede asegurar cual será la capacidad de almacenamiento de dosis total.

Teniendo en cuenta la compleja logística que acarrea la conservación y aplicación de la vacuna de Pfizer, desde la cartera sanitaria se está diagramando un operativo en el que se puedan aplicar las dosis en tan solo dos horas. Aún resta definirse cuáles serán los vacunatorios destinados a inmunizar con estas dosis.

**Población objetivo**

Según afirmaron a UNO fuentes del ministerio de Salud provincial, el primer cargamento que arribe a la provincia traerá muy pocas vacunas de Pfizer. Además, resta saber cual será la población objetivo a la que se le aplicarán estas dosis, puesto que desde la Conain se está evaluando si se inoculará a menores de 17 años sin comorbilidades o se utilizarán para completar esquemas iniciados con Sputnik V o AstraZeneca.

Sobre la prioridad de inmunización, Martorano afirmó que “hoy lo prioritario es completar segundas dosis, es decir que las personas tengan los esquemas completos de inmunización, y de la mano de la llegada de Sputnik 1 vamos inoculando a aquellos que se siguen inscribiendo”.

"Al ritmo que vamos, hace más de diez días que estamos con un promedio de 45 mil turnos diarios de la mano de llegada de vacunas. Hoy se enviaron 49.398 turnos, vamos a tener un septiembre fuerte de segundas dosis y ya para mediados de octubre vamos a tener esto completo”, concluyó la ministra.