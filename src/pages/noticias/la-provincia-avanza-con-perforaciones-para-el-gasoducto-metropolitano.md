---
category: Estado Real
date: 2021-03-10T06:22:48-03:00
thumbnail: https://assets.3dnoticias.com.ar/gasoducto.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: La provincia avanza con perforaciones para el Gasoducto Metropolitano
title: La provincia avanza con perforaciones para el Gasoducto Metropolitano
entradilla: Se iniciaron los trabajos de sondeo sobre la laguna Setúbal para conocer
  las características geológicas para el futuro cruce del Gasoducto Metropolitano
  que permitirá llevar el servicio a más de 100 mil usuarios.

---
El ministerio de Infraestructura, Servicios Públicos y Hábitat, a través de la empresa provincial ENERFE junto a la Universidad Nacional del Litoral, con el asesoramiento de los equipos técnicos de la Facultad de Ingeniería y Ciencias Hídricas (FICH), comenzaron los trabajos de sondeos en tierra sobre la Costa Este con extracción de muestras a profundidades inéditas en la localidad de Monte Vera para ejecutar el cálculo de las perforaciones que permitirán la investigación del lecho de la laguna Setúbal.

De esta forma se relevarán las características geológicas para el cruce del futuro Gasoducto que llevará el servicio de gas natural a la zona de la Costa, desde la comuna de Ángel Gallardo.

Al respecto, el vicepresidente de Enerfe, Juan Cesoni, se refirió a este avance importante para la futura obra: "Estamos realizando las perforaciones, dos de ellas sobre la ribera a 15 metros de profundidad, y otras dos sobre el cauce de la propia laguna a 50 metros de profundidad. Estos estudios nos va permitir conocer las características geológicas y tomar las mejores decisiones. Esto es lo que nos pidió el gobernador Omar Perotti y la ministra Silvina Frana, volcar todos nuestros esfuerzos y avanzar con esta obra que beneficiará a unos 100 mil usuarios de seis localidades del área metropolitana santafesina. Además abastecería a más de 7 mil industrias y comercios, y a unas 1.700 instituciones".

Este proyecto va a suministrar de gas natural a las localidades de Recreo, Monte Vera, Ángel Gallardo, Arroyo Aguiar, Santa Fe, San José del Rincón -incluyendo Colastiné Norte-, Arroyo Leyes, y recientemente se ha incorporado la ciudad de Esperanza.

Más adelante, Cesoni destacó el aporte técnico que darán lugar a muestras de suelo que revelarán a los santafesinos el pasado y presente de la geología local y de la paleontología: "Estas investigaciones van a servir el día de mañana como material de estudio para todos aquellos que pasen por la facultad de Ciencias Hídricas, por el valor científico y cultural de los descubrimientos fósiles de enormes mamíferos en su subsuelo. Van a poder contar con las muestras que nosotros realizamos para hacer estudios de suelos, proyecciones, y además por la profundidad, ya que no hay muchos antecedentes de perforaciones a tanta profundidad", subrayó el vicepresidente de ENERFE.

**EQUIPO INTERDISCIPLINARIO**

El equipo de especialistas que lleva adelante este trabajo está integrado por técnicos de la empresa provincial ENERFE, docentes y personal de la UNL, encabezado por el geólogo Carlos Ramonell, profesor titular e investigador en la FICH UNL.

"Conocer cada detalle de la traza del gasoducto es primordial para un eficaz suministro del servicio, un presupuesto acorde y que la obra se haga en tiempos coherentes a las necesidades de todo el sector metropolitano", señaló el vicepresidente de ENERFE, Juan Cesoni.

Está previsto realizar nuevas perforaciones en el lecho de la laguna, a una profundidad de 50 metros.