---
category: Agenda Ciudadana
date: 2021-10-13T06:15:19-03:00
thumbnail: https://assets.3dnoticias.com.ar/VIZZOTTI.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: 'Vizzotti, sobre la tercera dosis: "Estamos evaluando un refuerzo de todas
  las vacunas"'
title: 'Vizzotti, sobre la tercera dosis: "Estamos evaluando un refuerzo de todas
  las vacunas"'
entradilla: La titular de la cartera de Salud precisó que se encuentra en estudio
  esa posibilidad para los grupos inmunocomprometidos y los que recibieron Sinopharm,
  mayores de 60.

---
La ministra de Salud, Carla Vizzotti, aseguró que se evalúa dar una tercera dosis "de todas las vacunas" contra la Covid-19, a modo de "refuerzo", "empezando por los grupos inmunocomprometidos y los que recibieron Sinopharm, mayores de 60", especificó.  
  
"Estamos evaluando un refuerzo de todas las vacunas, porque hay personas que a principio del año que viene (2022) van a cumplir un año de la vacunación. Empezando por los grupos inmunocomprometidos y los que recibieron Sinopharm y son mayores de 60, que van a ser considerados en esa evaluación", detalló Vizzotti en diálogo con radio Urbana Play.  
  
La ministra, al referirse a la inoculación de una tercera dosis, subrayó que "desde un principio se supo que se iba a necesitar algún refuerzo" y luego comparó la vacuna contra el Covid con la antigripal, que se aplica regularmente, todos los años.  
  
."Su inmunidad no es de por vida, y si nos enfermamos la inmunidad natural tampoco es de por vida", remarcó.  
  
En esa sentido, afirmó que desde la cartera de salud están trabajando "con expertos, las áreas técnicas del ministerio y los ministros" para evaluar "antes de fin de año la necesidad de un refuerzo en función de los riesgos".  
  
Además, adelantó que se pondrá el foco en el personal de salud "que se empezó a vacunar a fines de diciembre" del 2020, ya que en febrero próximo ese grupo poblacional "va a estar completando los 12 meses" de haber recibido la segunda dosis.  
  
En el caso de la vacuna Sinopharm, la funcionaria explicó que "al ser vacunas inactivadas, la respuesta inmune no dura tanto tiempo".  
  
Sin embargo, destacó que hay que tener en cuenta "dos cosas importantes" en relación al inmunizante contra el coronavirus desarrollado en China.  
  
"Primero -puntualizó Vizzotti-, la mayoría de las vacunas Sinopharm la recibieron menores de 50 años y docentes. Segundo, la situación epidemiológica: estamos teniendo menos de mil casos por día, entonces el riesgo es bajo", resaltó.  
  
Sobre el avance de la campaña de vacunación, la funcionaria evaluó: "Entre octubre y noviembre aspiramos a avanzar fuertemente con las segundas dosis".  
  
"Hemos recuperado bastante con la combinación de vacunas. Hay un millón y medio de dosis de Moderna que han sido muy importantes para completar Sputnik", describió.  
  
Y para completar el balance, agregó: "Ahora han llegado más de 3 millones de vacunas de AstraZeneca, porque en el último mes se había acumulado gente en plazo para la segunda dosis y es lo que estamos resolviendo en este momento".  
  
Por último, detalló: "Las vacunas que se están distribuyendo de Richmond, del componente 2 de Sputnik, que son 1.6 millones, nos van a permitir también durante esta semana avanzar muchísimo".