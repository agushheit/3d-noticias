---
category: Agenda Ciudadana
date: 2021-01-03T11:07:32Z
thumbnail: https://assets.3dnoticias.com.ar/03012021-IVE.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Télam'
resumen: Mujeres criminalizadas por abortos dentro de las 14 semanas deberán ser sobreseídas
title: Mujeres criminalizadas por abortos dentro de las 14 semanas deberán ser sobreseídas
entradilla: Rige para mujeres con causas penales en curso por abortos practicados
  dentro de las 14 semanas del proceso gestacional. La Justicia deberá proceder automáticamente
  en cuanto entre en vigencia la ley 27610 (IVE).

---
Las mujeres con causas penales en curso por abortos practicados dentro de las 14 semanas del proceso gestacional deberán ser sobreseídas automáticamente por la Justicia en cuanto entre en vigencia la flamante ley 27610 de interrupción voluntaria del embarazo (IVE), sancionada el pasado miércoles por el Senado, aseguraron especialistas consultadas por Télam.

La abogada feminista Soledad Deza dijo que las mujeres criminalizadas por aborto dentro del primer trimestre de gestación «inmediatamente debieran quedar sobreseídas».

«No es lo mismo que se archive la causa, hay que sobreseerlas», enfatizó la profesional, que fue abogada de mujeres que pasaron por esta situación, como Belén, la joven tucumana que estuvo presa por un aborto espontáneo y de quien Deza logró la libertad y el sobreseimiento.

La ley votada la semana pasada establece que el aborto se permitirá hasta la semana 14 inclusive del proceso gestacional.

Voceras del Ministerio de las Mujeres, Géneros y Diversidad (MGyD) coincidieron con la abogada.

«Siempre que los abortos sean compatibles con lo que se estipula ahora en la ley (antes de la semana 14 e inclusive), juezas y jueces que intervienen en esas causas tienen que actuar de oficio, es decir, no lo tiene que pedir nadie, y deben aplicar la ley penal más benigna, que es la nueva ley donde ya no es delito cometer un aborto antes de la semana 14», detallaron.

En mismo sentido se pronunció una funcionaria del Ministerio Público Fiscal: «En esos casos no debería haber dudas», enfatizó.

Desde el MGyD aclararon que la ley se aplica «a las causas en curso, donde hay personas imputadas por cometer, consentir o realizar un aborto, y también en aquellas donde ya se condenó a las personas».

También indicaron que «se extingue la acción penal por inexistencia de delito, es decir, que ya no se puede perseguir a esas personas y se libera a las que estén privadas de la libertad».

Deza, profesora de Feminismos Jurídicos de la Facultad de Derecho de la Universidad Nacional de Tucumán, integrante de la Fundación Mujeres X Mujeres y de la Campaña Nacional por el Derecho al Aborto Legal, Seguro y Gratuito, insistió en el sobreseimiento.

«Las causas se archivan si no hay sobreseimiento y el prontuario queda abierto. Entonces las mujeres no obtienen, por ejemplo, un certificado de buena conducta para acceder a empleos», destacó.

Desde la ONG Mujeres X Mujeres, Deza, junto a Alejandra Iriarte y Mariana Álvarez, publicó Jaque a la Reina, un libro que investiga la criminalización del aborto en Tucumán, entre los años 2013 y 2019, una experiencia inédita en el país que actualizarán para 2021.

En la investigación relevaron 534 causas de abortos «y solo un 1% de sobreseimientos», señaló la profesional tucumana.

Sin embargo, no hay datos oficiales sobre la cantidad de mujeres criminalizadas por aborto en el país, por lo que los aportes vienen desde las ONG como en el caso tucumano.

Otra indagación desde la sociedad civil es la del Centro de Estudios Legales y Sociales (CELS), que comenzó a hacer un relevamiento en 2019, y que en la actualización de la semana pasada detectó 1.532 mujeres que afrontan causas penales por aborto y 37 por eventos obstétricos criminalizados en 17 provincias argentinas.

Este monitoreo, hecho junto a la Campaña Nacional por el Derecho al Aborto Legal Seguro y Gratuito, el Centro Universitario San Martín (Cusam) y las profesionales María Lina Carrera, Natalia Saralegui Ferrante y Gloria Orrego-Hoyos, abarcó el período 2012-2020.

Se hizo sobre la base de notas periodísticas y pedidos de acceso a la información a Ministerios Públicos Fiscales y Cortes de Justicia provinciales.

Incluyó datos de Santa Fe, Río Negro, Chaco, Chubut, Córdoba, Neuquén, Formosa, Catamarca, Corrientes, Tucumán, Mendoza, Salta, Buenos Aires, Jujuy, San Luis, La Pampa y la Justicia Nacional que tiene a cargo estas causas en el territorio de la Ciudad de Buenos Aires.

Desde el MPF confirmaron que «tienen pensado» hacer un relevamiento oficial de este tipo de causas «para ver bien qué casos hay abiertos aún y en cuáles las mujeres están imputadas o presas».

En lo que se refiere a quienes están procesadas en este contexto, Deza explicó que al haberse disminuido la pena en la nueva ley de IVE «hay muchas que deberían obtener el sobreseimiento» porque la nueva norma modifica el artículo 85 del Código Penal.

**La nueva redacción establece:**

«El o la que causare un aborto será reprimido: 1º) Con prisión de TRES (3) a DIEZ (10) años, si obrare sin consentimiento de la persona gestante. Esta pena podrá elevarse hasta QUINCE (15) años si el hecho fuere seguido de la muerte de la persona gestante. 2º) Con prisión de TRES (3) meses a UN (1) año, si obrare con consentimiento de la persona gestante, luego de la semana CATORCE (14) de gestación y siempre que no mediaren los supuestos previstos en el artículo 86».

Para la abogada «es improbable que el Estado actúe de oficio, que los sobreseimientos sean automáticos, así que las y los defensores y activistas tendremos que ocuparnos».

Una vez publicada en el Boletín Oficial, la ley entrará en vigencia al octavo día.