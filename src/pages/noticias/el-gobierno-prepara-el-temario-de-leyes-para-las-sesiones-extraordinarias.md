---
category: Agenda Ciudadana
date: 2021-12-27T06:00:18-03:00
thumbnail: https://assets.3dnoticias.com.ar/juradioutados.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: El Gobierno prepara el temario de leyes para las sesiones extraordinarias
title: El Gobierno prepara el temario de leyes para las sesiones extraordinarias
entradilla: 'La Casa Rosada aún no confirmó la fecha de la convocatoria, pero se baraja
  la segunda quincena de enero como primera posibilidad. El Plan Plurianual figura
  entre los temas a analizar

'

---
El Gobierno Nacional prepara por estas horas el temario de leyes que incluirá en la convocatoria a sesiones extraordinarias que realizará para que el Congreso se pueda reunir durante los meses de verano.

Hasta el 31 de diciembre rige la prórroga de las sesiones ordinarias dictada por el presidente Alberto Fernández, que ahora tiene previsto convocar a extraordinarias para extender el período legislativo.

Las sesiones ordinarias comienzan el 1 de marzo y finalizan el 30 de noviembre. Durante diciembre el Congreso funcionó con una prórroga de las ordinarias, momento en que los legisladores pueden tener un temario a elección, más allá de las leyes que manda el Poder Ejecutivo.

Ahora, el Gobierno decidió convocar a extraordinarias, lo que implica que el PEN será el que defina la lista de proyectos que podrá tratar el Parlamento.

Entre esas iniciativas se encuentra el anunciado plan plurianual que buscará avanzar con las negociaciones con el Fondo Monetario Internacional (FMI).

Fernández había dicho que lo iba a presentar durante los primeros días de diciembre, pero la idea se fue postergando y por el momento no estaba previsto que el envío al Congreso se produjera en los próximos días, por lo que se lo aguarda durante el verano.

"En algún momento del verano. Vamos a convocar a extraordinarias, esto ya lo habíamos anticipado, y vamos a trabajar el Plan Plurianual en ese momento. Probablemente será en los primeros meses del 2022″, afirmó la vocera presidencial Gabriela Cerruti días atrás.

Si bien no hay precisiones sobre la fecha, podría ser después de la primera quincena de enero, o directamente en febrero.

Otro de los temas que está pendiente de tratamiento y que deberá ser analizado en los primeros meses de 2022 es la reforma del Consejo de la Magistratura.

Días atrás, la Corte Suprema de Justicia declaró inconstitucional la actual composición del órgano que se encarga de selección, castigar y remover jueces, y también le encomendó al Congreso dictar otra ley, algo que pone ese tema entre los pendientes del año próximo.

Además, podrían ingresar en el temario oficial la Ley de Hidrocarburos, la de Desarrollo Agroindustrial, la de Compre Argentino, de Electromovilidad y la de Industria Automotriz.