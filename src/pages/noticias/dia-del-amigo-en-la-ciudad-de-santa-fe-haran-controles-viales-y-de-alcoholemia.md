---
category: La Ciudad
date: 2021-07-20T06:15:24-03:00
thumbnail: https://assets.3dnoticias.com.ar/soplemijo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'Día del Amigo: en la ciudad de Santa Fe harán controles viales y de alcoholemia'
title: 'Día del Amigo: en la ciudad de Santa Fe harán controles viales y de alcoholemia'
entradilla: 'Este martes el municipio realizará supervisiones del tránsito en puntos
  transitados de esta capital. También, fiscalizaciones en el cumplimiento de protocolos
  y de los horarios de cierres de locales nocturnos. '

---
El municipio hará controles de tránsito y nocturnidad este martes, jornada muy particular porque se celebra el Día del Amigo y, como se espera, muchos grupos de personas saldrán a festejar en bares, restaurantes y espacios públicos tradicionales, como la Costanera santafesina, el Parque del Sur y el Parque Garay, entre muchos otros.

“Se realizarán operativos de tránsito como tests de alcoholemia, controles a taxis y remises, fiscalización y monitoreo del cumplimiento de los protocolos y de los horarios de cierre en bares y restaurantes, kioscos y comercios. La GSI estará haciendo sus controles habituales”, le dijeron a El Litoral fuentes del área de Control y Convivencia ciudadana del gobierno local.

Pero, además, como novedad “se contará con las ‘naranjitas’ y con agentes del Departamento de Educación Vial, quienes estarán recorriendo diferentes arterias de la ciudad con un mensaje de prevención y de ordenamiento”, apuntaron desde la secretaría de Control.

También, se planificó un trabajo articulado con la policía provincial para poder profundizar su presencia en espacios públicos de habitual concurrencia pública y en corredores transitados de esta capital.

“Es un día muy especial. Pero en este contexto de pandemia, debemos transmitir un mensaje de cuidado y de prevención. El control es cuidado. Destacamos que, en este día del Amigo, entre amigos nos cuidamos”, subrayaron las mismas fuentes municipales.