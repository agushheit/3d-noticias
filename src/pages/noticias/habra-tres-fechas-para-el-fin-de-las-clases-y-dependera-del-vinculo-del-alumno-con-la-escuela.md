---
category: Agenda Ciudadana
date: 2021-09-28T06:00:11-03:00
thumbnail: https://assets.3dnoticias.com.ar/VACUANDO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Habrá tres fechas para el fin de las clases y dependerá del "vínculo" del
  alumno con la escuela
title: Habrá tres fechas para el fin de las clases y dependerá del "vínculo" del alumno
  con la escuela
entradilla: Estipulan dos fechas en diciembre para quienes hayan logrado un vínculo
  "sostenido" o "intermitente". En tanto Habilitarán el período febrero/marzo para
  quienes hayan tenido baja intensidad de vinculación con la escuela.

---
La fecha de finalización del ciclo escolar 2021 dependerá de la trayectoria de cada estudiante. En un año particular, marcado por la pandemia, eesde el Ministerio de Educación de la provincia se planificaron tres alternativas posibles para la terminación de las clases, de acuerdo a la vinculación que los alumnos hayan mantenido con la escuela y las actividades propuestas por sus docentes.

Según publica La Capital, quienes hayan logrado mantener un vínculo "sostenido" con la escuela deberán asistir a clases hasta el próximo 3 de diciembre, en cambio para quienes hayan tenido una relación "intermitente" el cursado se extenderá hasta el 17 o 20 de diciembre. Y los alumnos con una "baja intensidad" en la conexión con las instituciones educativas se proyectan instancias en febrero y marzo, después del receso de verano.

"Todas estas medidas y proyecciones se hacen con un criterio de flexibilidad, en función de la evolución epidemiológica", advierte el secretario de Educación provincial, Víctor Debloc, antes de desplegar el cronograma que seguirá la finalización de clases en las escuelas santafesinas.

La titular de la cartera educativa provincial, Adriana Cantero, ya había explicado que la promoción del año escolar seguirá los criterios acordados a nivel nacional. La semana pasada, la funcionaria había adelantado que “la evaluación será cualitativa, conforme al acuerdo nacional firmado por todos los ministros de Educación del país, que propone un proceso. Se marcan parámetros que tienen que ver con la mirada integral del porcentaje de vinculación e interacción con la escuela. Este último trimestre tiene un enorme peso en la evaluación”, aclaró y sostuvo que el ciclo lectivo seguiría hasta el 21 de diciembre “y tal vez en febrero se convoque a algún grupo prioritario, según el diagnóstico que hagan las escuelas sobre el nivel de aprendizaje de los alumnos y alumnas”, precisó.

De acuerdo a esos objetivos generales, explicó Debloc, se trazó un calendario de finalización de clases escalonado que abarca a los tres niveles de escolarización obligatoria. Es decir, tanto al jardín y preescolar, como al primario y secundario que terminarán las clases en distintas etapas, de acuerdo a la trayectoria de los alumnos durante un ciclo lectivo marcado por la bimodalidad y la alternancia educativa, hasta fin de agosto.

En todos los casos, la fecha de finalización de clases tendrá que ver con el diagnóstico que hagan los docentes sobre el aprendizaje de sus alumnos en relación a los contenidos priorizados para el ciclo escolar 2021.

¿Quiénes tendrán clases hasta fin de diciembre?

Por ejemplo, los alumnos que hayan tenido una trayectoria "sostenida" podrán terminar las clases en la primera semana de diciembre. Se considera una trayectoria sostenida la de aquellos niños y adolescentes que hayan tenido una conexión estable con la escuela, que les haya permitido participar en la mayor parte de las actividades propuestas por la institución. Son estudiantes que, además, han incorporado el 70 por ciento de los contenidos priorizados en la curricula de este ciclo escolar.

Los alumnos que no hayan podido lograr una participación regular en las actividades propuestas por sus docentes, pero sin perder la continuidad en el vínculo con la escuela tendrán dos semanas más de clases. Para quienes alcanzaron una trayectoria "intermitente" y entre un 40 y 50 por ciento de contenidos aprobados, se planificó un proceso para fortalecer e intensificar esos aprendizajes que se extenderá hasta el 20 de diciembre.

Para el tercer grupo, los niños y adolescentes con trayectorias de "baja intensidad" en su relación con la escuela, aquellos que no alcancen un 40 por ciento de los contenidos priorizados, se podrá habilitar encuentros en febrero y marzo, si es necesario, luego del reintegro de los docentes del receso de verano.

En todos los casos, si se mantienen las actuales condiciones sanitarias, las actividades se desarrollarán en forma presencial. De todas formas, aclaró Decloc, los criterios son "flexibles" en función a la evolución de la pandemia.