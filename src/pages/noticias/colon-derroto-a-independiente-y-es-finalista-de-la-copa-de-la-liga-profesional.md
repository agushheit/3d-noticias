---
category: Deportes
date: 2021-06-01T08:02:36-03:00
thumbnail: https://assets.3dnoticias.com.ar/Colon-a-semifinales-copa-de-la-liga.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Diario El Litoral
resumen: Colón derrotó a Independiente y es finalista de la Copa de la Liga Profesional
title: Colón derrotó a Independiente y es finalista de la Copa de la Liga Profesional
entradilla: El "Sabalero" se impuso por 2 a 0 en el estadio del Bicentenario con goles
  de "Pulga" Rodríguez, de penal, y de Santiago Pierotti. El próximo viernes buscará
  el título ante Racing. 

---
Colón de Santa Fe derrotó este lunes por la noche a Independiente de Avellaneda y es finalista de la Copa de la Liga Profesional. 

El "Sabalero" se impuso por 2 a 0 en el estadios del Bicentenario de San Juan y buscará el título el próximo viernes ante Racing en el mismo escenario. 

El primer gol del encuentro lo marcó Luis Miguel "Pulga" Rodríguez, a los 21 minutos, mediante un tiro penal.

En el complemento, a los 67', Santiago Pierotti amplió la ventaja para el equipo dirigido por Eduardo Domínguez. 

 De esta manera el rojinegro definiría el título frente a la "Academia", que venció por penales a Boca 4 a 2. 