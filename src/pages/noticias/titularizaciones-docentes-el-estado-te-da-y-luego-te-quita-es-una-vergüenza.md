---
layout: Noticia con imagen
author: "Fuente: LT10"
resumen: Titularizaciones docentes
category: Agenda Ciudadana
title: 'Titularizaciones docentes: "El Estado te da y luego te quita, es una
  vergüenza"'
entradilla: Maestros y profesores cuyas titularizaciones fueron revocadas
  aseguran que no son escuchados por el Ministerio y que los funcionarios
  provinciales buscan "entreverar a la gente" y desprestigiar a los docentes.
date: 2020-11-20T11:43:42.370Z
thumbnail: https://assets.3dnoticias.com.ar/docentes.jpg
---
Este lunes, el Ministerio de Educación anunció el revocamiento de las últimas titularizaciones de la gestión anterior. La medida provocó la inmediata reacción de los docentes, que este jueves llevaron su reclamo a la explanada de la Casa Gris.

"Es una amargura total y es una falta de respeto al trabajador que nos enteremos por los medios de comunicación", manifestó una profesora de la EMPA Nº1.330 al móvil de Litus y LT10.

"Soy representante de una de las escuelas más grandes de la provincia, donde tenemos 5.000 estudiantes. Es una falta de respeto que toda la planta de docentes esté con este revocamiento de titularización. Los estudiantes entraron en pánico", contó, y agregó: "el Estado te da y hoy el Estado te quita, es una vergüenza".

"Tuvieron tres meses para revisar en el Tribunal de Cuentas si había alguna irregularidad para notificar. Que dejen de hablar mentiras y que la ministra de Educación (Adriana Cantero) nos dé una respuesta", exigió.

Otra colega recordó que "cada concurso se discute y se arregla en paritaria" y que "la paritaria es ley para los trabajadores y para los gobiernos". Admás, criticó a los funcionarios santafesinos: "ellos se agarran de cualquier pavada para entreverar a la gente y para dejarnos mal parados a nosotros", acusó.

Finalmente, una tercera maestra calificó la decisión de la cartera educativa como "una irresponsabilidad".

"Somos trabajadores del Ministerio de Educación. Merecemos el respeto de que alguien se siente con nosotros, y lo reclamamos durante todo el año. Hace 11 meses que no tenemos una sola respuesta respecto a cómo seguir nuestro trabajo".