---
category: Estado Real
date: 2021-07-10T06:00:04-03:00
thumbnail: https://assets.3dnoticias.com.ar/INDEPENDENCIA.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: El Gobernador Omar Perotti encabezó el acto por el 205° aniversario de la
  declaración de la independencia
title: El Gobernador Omar Perotti encabezó el acto por el 205° aniversario de la declaración
  de la independencia
entradilla: "“Nos sentimos orgullosos de construir juntos la Santa Fe que queremos
  y soñamos, y la que le va a dar a nuestros hijos e hijas la oportunidad de vivir
  y realizar aquí sus sueños”, aseguró el gobernador desde Rafaela."

---
El gobernador Omar Perotti, junto al intendente de Rafaela, Luis Castellano, encabezó este viernes, en esa ciudad, el acto oficial con motivo del 205º aniversario de la Declaración de la Independencia Argentina.

“Declarar la independencia es un acto que afirma y define la existencia de la Nación. Es un momento histórico que no se agota en sí mismo. La independencia de los pueblos es algo más profundo, más rico que un momento histórico y representa la libertad de un país que dejó atrás la imposición de valores, de políticas y de reglas externas para iniciar como pueblo el camino de la auto determinación”, señaló el gobernador.

Y agregó: “En el Congreso de Tucumán los diputados representantes de las distintas provincias sellaron nuestro destino en aquel hecho histórico, poniéndole fin a siglos de dominación española”, dijo, y si bien recordó que Santa Fe no envió a sus representantes porque integraba la Liga Federal, “sin dudas había un objetivo común, que era impulsar la independencia”, aseguró.

El mandatario explicó que en la actualidad “la necesidad de colonizar va también adaptándose a las circunstancias, y aunque no son tan rudimentarias como en el pasado, muchas veces las imposiciones son culturales o económicas”, y ante este escenario planteó: “Debemos ser un país insertado en el mundo, pero sin perder independencia”.

**DRAGONES DE LA INDEPENDENCIA**

En el acto, el gobernador anunció que desde este viernes las mujeres que forman parte de la unidad “Dragones de la Independencia”, se sumarán a la guardia de honor del gobernador y formarán parte de todos los actos oficiales. “En ellas, nuestro reconocimiento a todas las mujeres que trabajan y han trabajado en la seguridad de nuestra provincia”, indicó.

“Así como aquel proceso histórico necesitó de la convicción y la fuerza de todas aquellas personas que creían que podían cambiar y trasformar la realidad de este territorio, hoy en este contexto que nos toca afrontar, sabemos que necesitamos estar unidos, necesitamos de cada uno y de cada una de ustedes, para salir lo más pronto posible de este momento que ha generado angustia, dolor, pérdida de seres cercanos y queridos, y nos ha modificado la vida. Es importante siempre entender que se trata de un proceso”, dijo Perotti.

Además, recordó las figuras de José de San Martín, Manuel Belgrano, Martín de Güemes, y mujeres como María Remedios del Valle, Juana Azurduy, Juana Moro.

Y agregó que “no tenemos que luchar en guerras por nuestra independencia, pero tenemos el desafío de luchar contra un virus que puso en riesgo la libertad del mundo, y hay que entender que es una lucha de todos”.

**CAMPAÑA DE VACUNACIÓN Y ASISTENCIA ECONÓMICA**

En otro tramo de su discurso, el gobernador dio detalles de la campaña de vacunación, y de las acciones para asistir económicamente a las personas y sectores afectados por la pandemia.

“Lo que hicimos fue importante, y sé que falta, por eso desde este lugar que hoy nos toca ocupar, reforzamos el compromiso que asumimos como gestión, vamos a seguir cuidando a los santafesinos y santafesinas, en estos días vamos a llevar en todo el territorio a colocar 2 millones de dosis”, señaló, y añadió que “las vacunas no solo significan cuidar la salud, también son más libertad, y mayor independencia para desarrollarnos, y seguir siendo como provincia el corazón productivo del país”.

En referencia a la asistencia y ayudas, se comprometió a “seguir acompañando a cada santafesino y santafesina que busca a cuidar su bolsillo, y a cada comerciante que anhela volver a revitalizarse, por eso los acompañamos con la Billetera Santa Fe y el Boleto Educativo, para que no haya ningún obstáculo para la educación, seguiremos estando al lado de los que se comprometen con esta provincia, de quienes invierten y generan empleo, y confían en la capacidad de nuestras trabajadoras y trabajadores”.

“Los santafesinos no bajamos los brazos, no vamos a claudicar, seguiremos poniendo a la provincia de pie, trabajando como hoy en cada rincón, en cada pueblo y cada ciudad hay obras que impulsan movimiento y trabajo, para generar las condiciones de recuperación que necesita toda nuestra provincia”, destacó Perotti.

“Sabemos lo que representamos para nuestro país, lo que representamos para nuestra independencia económica, por eso, en esta conmoración, reafirmamos nuestro compromiso de entregar lo mejor de nosotros y nosotros. Hemos hecho muchas cosas, aun en pandemia, y queda tarea por delante, nos sentimos orgullosos de construir juntos la Santa Fe que queremos y soñamos, y la que le va a dar a nuestros hijos e hijas la oportunidad de vivir en realizar aquí sus sueños”, destacó.

Y propuso: “Que Santa Fe sea el corazón productivo de la Argentina, que haya oportunidades para todos y todas es la motivación que nos empuja a ser mejores, y seguir luchando por un país libre e igualitario, por una provincia que acepta las diferencias, pero no las desigualdades”.

**“LA HISTORIA ES MAESTRA DE VIDA”**

Por su parte, Castellano señaló que “la historia es maestra de vida, una afirmación cierta siembre y cuando sepamos ejercer la memoria. La independencia va más de una fecha, un lugar o una reunión, más allá del congreso de Tucumán, es un proceso incesante, que llega a nuestros días, nos determina y nos hace responsable de construirla”.

Y agregó que “la libertad que tanto nos costó conseguir y recuperar cada vez que la perdimos se reafirma en cada acción que emprendemos; somos un mismo pueblo y la única salida es cuidarnos entre todos, porque nadie se salva solo”, aseguró en relación a la pandemia por Covid-19.

“No bajamos, ni bajaremos los brazos, superaremos la pandemia porque está en nuestra idiosincrasia no dejarnos vencer”, indicó el intendente, quien agradeció la labor del personal de salud, y concluyó: “En Rafaela ya aplicamos 70 mil dosis, y en Santa Fe se superó 1,5 millones, en esos números está representado nuestro esfuerzo y esperanza; hoy nuestras armas son las vacunas y nos estamos vacunando”.

Previamente, las autoridades izaron la Bandera Argentina en la Plaza 25 de Mayo, donde se realizó el acto, y entonaron las estrofas del Himno Nacional Argentino, que fue representado por el cantante local, Gerardo Mechler.

**PRESENTES**

Del acto conmemorativo participaron también la vicegobernadora Alejandra Rodenas; los ministros de la Producción, Ciencia y Tecnología, Daniel Costamagna; de Gestión Pública, Marcos Corach; de Trabajo, Juan Manuel Pusineri; de Igualdad y Género, Celia Arena; y de Infraestructura, Servicios Públicos y Hábitat, Silvina Frana; el senador por el departamento Castellanos, Alcides Calvo; los diputados provinciales Juan Argañaraz y Gabriel Chumpitaz, y el presidente del Concejo Municipal local, Germán Bottero, entre otras autoridades.