---
category: Agenda Ciudadana
date: 2021-06-25T07:14:19-03:00
thumbnail: https://assets.3dnoticias.com.ar/alberto.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: El gobierno nacional prorroga las restricciones a la espera de una baja de
  contagios
title: El gobierno nacional prorroga las restricciones a la espera de una baja de
  contagios
entradilla: El DNU presidencial ya lleva dos prórrogas de restricciones desde que,
  a principios de mayo, ingresara al Congreso la ley de Emergencia Covid.

---
El Gobierno prorrogará mañana el DNU que fija restricciones para mitigar la segunda ola de coronavirus, con la idea de que se consolide la tendencia a la baja de contagios registrada en las últimas tres semanas, mientras analiza cómo podrían impactar las vacaciones de invierno en la futura situación epidemiológica y apuesta a sostener los controles migratorios para impedir el ingreso al país de nuevas cepas, como la Delta.

La idea es avanzar con cautela en eventuales nuevas aperturas, aún con el "optimismo" que impera en las autoridades nacionales por el descenso de casos y de camas de terapia intensiva ocupadas por Covid-19, a partir del avance del plan de vacunación y de la certeza que "dieron resultado" las medidas adoptadas a tiempo para limitar la circulación y los contagios.

El presidente Alberto Fernández y su Gabinete tienen en análisis las advertencias formuladas semanas atrás por el Comité de Expertos sobre el peligro que podrían representar las dos semanas consecutivas de vacaciones de invierno y su posible correlato en una tercera ola de contagios. Sin embargo, como el receso escolar se inicia recién el 19 de julio próximo, en principio el decreto, que tendrá vigencia durante las próximas dos o tres semanas, no contempla medidas referidas al turismo, como la reapertura de rutas interjurisdiccionales para las vacaciones de invierno, adelantaron voceros oficiales.

El texto de la norma establece un marco con parámetros de riesgo epidemiológico y sanitario conocido como "semáforo sanitario", con una estructura similar al proyecto de ley que tiene media sanción del Senado y está a la espera de su tratamiento en la Cámara de Diputados, que en principio sería el martes próximo. En tanto, el Presidente no tiene previstas rondas de asesoramiento con el Comité de Expertos porque los científicos mantienen "reuniones semanales" con la ministra de Salud, Carla Vizzotti, explicó un vocero, así como tampoco con gobernadores.

Es que la norma "tiene un piso de restricciones y, a partir de allí y eventualmente por la baja de casos y de ocupación en unidades de terapia intensiva (UTI), cada jurisdicción podrá determinar el modo de desarrollo de cada actividad", indicaron las fuentes. El DNU de restricciones que será prorrogado mañana "tiene el mismo texto que el proyecto de ley y no hay un escenario para aplicarle alguna medida excepcional", agregaron.

Por otra parte, como contracara del descenso de contagios, el alto número de fallecimientos diarios es interpretado en el Gobierno como consecuencia de los "picos" que hubo en jornadas que registraron "35.000 casos" por día, dijeron las fuentes y remarcaron: "Más casos son más muertes".

El Gobierno tiene puestas sus expectativas en que en "las próximas dos o tres semanas de vacunación habrá un escenario más auspicioso". Hoy se inició la distribución de algo más de 1,2 millones de vacunas en todas las jurisdicciones, en el marco del Plan Estratégico de Vacunación, que ya lleva entregadas esta semana poco más de 4 millones de vacunas a las provincias.

Con la distribución de 1.232.000 dosis de la vacuna Sinopharm arribadas al país en los últimos días -que llegarán entre mañana y el sábado a sus destinos- sólo en esta semana ya se habrán entregado 4.016.900 sueros a todos los rincones de la Argentina. La celeridad es parte de la impronta del plan estratégico de inmunización que despliega el Gobierno nacional para combatir la Covid-19.

En los últimos cuatro días arribaron a la Argentina 3.139.000 dosis: 2.000.000 del laboratorio Sinopharm y 1.139.000 de AstraZeneca que fueron producidas localmente y terminadas en la planta AMRI de Alburquerque, Estados Unidos. Desde el inicio de la campaña de inmunización, suman un total de 23.816.145 las vacunas recibidas.

Según los datos del Monitor Público de Vacunación, hasta esta mañana fueron distribuidas en todas las jurisdicciones 21.327.590 vacunas, de las cuales fueron aplicadas 18.921.680: 15.128.961 de personas recibieron la primera dosis y 3.792.719 ya cuentan con el esquema completo. Además, el ministro de Ciencia, Tecnología e Innovación, Roberto Salvarezza, consideró hoy "absolutamente alcanzable y posible" que el 60 o 70% de los mayores de 18 años estén vacunados para septiembre próximo con la primera dosis contra el coronavirus. "Ya superamos los 15 millones de vacunados y estamos por arriba del 50% de los mayores de 18 años. El objetivo de superar el 60 o 70% es absolutamente alcanzable y posible", dijo hoy en diálogo con Radio Nacional.