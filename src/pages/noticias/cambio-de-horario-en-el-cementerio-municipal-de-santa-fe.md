---
category: La Ciudad
date: 2021-04-03T06:00:35-03:00
thumbnail: https://assets.3dnoticias.com.ar/CEMENTERIO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Cambio de horario en el Cementerio Municipal de Santa Fe
title: " Cambio de horario en el Cementerio Municipal de Santa Fe"
entradilla: Desde este jueves 1° de abril comienza a regir el horario de invierno
  en la necrópolis local.

---
A partir de este jueves 1° de abril comienza a regir el horario invernal en el Cementerio Municipal de la ciudad de Santa Fe.

Por tal motivo, la Municipalidad comunicó que las visitas se podrán hacer de lunes a sábados, de 9 a 16 horas.

En tanto, los domingos y feriados el horario será de 8 a 12 horas.