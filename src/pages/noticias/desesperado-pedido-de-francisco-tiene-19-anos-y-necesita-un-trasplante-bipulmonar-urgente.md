---
category: La Ciudad
date: 2021-08-16T06:15:36-03:00
thumbnail: https://assets.3dnoticias.com.ar/FRANCISCO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'Desesperado pedido de Francisco: tiene 19 años y necesita un trasplante
  bipulmonar urgente'
title: 'Desesperado pedido de Francisco: tiene 19 años y necesita un trasplante bipulmonar
  urgente'
entradilla: Francisco Burlando es el menor de tres hermanos. Cuando nació fue diagnosticado
  con fibrosis quística, recorrió un largo camino junto a su familia, pero en estos
  momentos y, a tan corta edad, su vida corre riesgo.

---
Después de 19 años de lucha frente a esta afección, Francisco está en estado crítico y necesita un trasplante bipulmonar. Actualmente se encuentra internado desde hace 20 días, en la Clínica de Nefrología de la capital santafesina, y depende de un respirador para seguir viviendo.

Para salvar su vida, necesita ser trasladado a la Fundación Favaloro de manera urgente. Su mamá Marta explica que “si ingresa en la institución, sería debidamente evaluado por los médicos y tendría la posibilidad de entrar en la lista de urgencias para recibir el órgano”.

El joven espera junto a su familia por el trasplante que tanto necesita. “Si bien la obra social nos acompañó durante 19 años, ahora les rogamos que se pongan de acuerdo para lograr el traslado del cual depende la vida de mi hijo”, asegura Marta.

Por último, su mamá hace un pedido a la sociedad. "Necesitamos el apoyo de la gente para que Francisco pueda recibir el trasplante que necesita”.

La campaña por el traslado a la Fundación Favaloro, se traspoló a las redes sociales en las que se pide desesperadamente difusión.

En Facebook se puede encontrar como " Pulmones para Francisco: 19 años luchando para poder respirar”

En Instagram se puede encontrar como @pulmonesparafrancisco

En Twitter como @pulmonparafran