---
category: La Ciudad
date: 2021-11-20T06:00:21-03:00
thumbnail: https://assets.3dnoticias.com.ar/SOBRANIA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Servicios municipales para el fin de semana largo por el Día de la Soberanía
  Nacional
title: Servicios municipales para el fin de semana largo por el Día de la Soberanía
  Nacional
entradilla: Con motivo del feriado del lunes 22 de noviembre, el municipio informa
  los horarios de los servicios de recolección de basura, cementerio, transporte,
  estacionamiento medido e información turística.

---
**Transporte y estacionamiento**

Por otra parte, para el lunes 22, el transporte urbano de colectivos tendrá frecuencias similares a los días domingo. En tanto, durante el fin de semana largo, se reforzarán los servicios de taxis y remises.

Con respecto al sistema de estacionamiento medido, por ser feriado nacional no estará operativo. Vale recordar que, como es habitual, tampoco estará operativo el día domingo.

**Cementerio**

El lunes, los horarios de visita serán de 7.30 a 12.30, con motivo del día feriado. En tanto, los trámites para inhumaciones se podrán realizar desde las 7.30 hasta las 11.30.

**Mercado Norte**

El Mercado Norte, ubicado en la intersección de Santiago del Estero y Urquiza, abrirá durante el fin de semana largo en los horarios habituales, a excepción del lunes feriado, que lo hará en el horario de 9 a 13 horas.

**Tribunal de Faltas**

Durante el lunes, la guardia del Tribunal de Faltas funcionará de 8 a 13 horas, en el Juzgado ubicado en avenida Presidente Perón 3801.