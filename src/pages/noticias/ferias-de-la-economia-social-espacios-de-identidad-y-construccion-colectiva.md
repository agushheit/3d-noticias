---
category: La Ciudad
date: 2021-08-18T06:00:44-03:00
thumbnail: https://assets.3dnoticias.com.ar/FERIAS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Ferias de la economía social: espacios de identidad y construcción colectiva'
title: 'Ferias de la economía social: espacios de identidad y construcción colectiva'
entradilla: La Municipalidad acompaña de distintas maneras, con apoyo organizativo
  o con infraestructura, a las propuestas de ferias diseminadas por la ciudad.

---
Guirnaldas de estridentes colores, un castillo inflable para saltar y jugar, la murga del barrio a puro redoblante y tambores, feriantes con disfraces y ornamentos para celebrar a las infancias. Con ese escenario, se realizó la feria barrial “Paseo Capital” en el barrio Centenario, uno de los espacios que la Municipalidad ayudó a organizarse y crecer en el marco de su política de promoción de la economía social.

El municipio está presente en 30 ferias locales, cada una con sus particularidades, identidades y necesidades propias. A algunas las está fortaleciendo con infraestructura, otras con gestión y en lo organizativo, y todas están invitadas a las mesas de reflexión y de diálogo que se hacen desde la Municipalidad para pensar esta política en clave de un horizonte asociativo y en común.

La feria del Centenario en particular comenzó en diciembre pasado, fue ganando en frecuencia y cada vez crece más de la mano de sus impulsores y con el apoyo del municipio, que de forma mancomunada hacen de este espacio una construcción colectiva: en la actualidad son unos 30 puestos los que participan de esta feria que promueve el encuentro. La misma se inscribe en el marco de las Ferias del Mercado Santafesino, que busca fortalecer los espacios existentes y promover nuevos, con el objetivo de desplegar en toda Santa Fe Capital más espacios de visibilización de la economía social y popular.

Ayelén Dutruel, secretaria de Integración y Economía Social del municipio estuvo acompañando a los feriantes y compartió que “para nosotros las ferias son muy importantes en dos sentidos: por un lado porque el contexto en el que estamos nos invita a generar nuevos espacios de comercialización, nuevas formas de entender el trabajo con otros, en un colectivo y de manera asociativa. Y, por otro lado, además de ser un espacio de comercialización, son espacios que mejoran la integración en la ciudad”.

En ese contexto, señaló que “se piensa en la distribución de las ferias de manera equilibrada y equitativa en toda la ciudad, para que todo trabajador o trabajadora de la economía popular pueda acceder a esos espacios de comercialización y, por qué no, acompañarlos en otros procesos necesarios”.

**Ferias con identidad propia**

La secretaria definió que “cada feria tiene su particularidad y su identidad. Esto, de algún modo, lo que hace es que nos encontremos con las instituciones del barrio, con las asociaciones civiles para pensar un poco también esos momentos importantes para los barrios, como es en este caso el festejo del Día del niño. Así, vamos organizando distintos eventos que hacen a la identidad de cada una de las ferias y de cada uno de los barrios donde estamos”.

**Con arraigo**

Tejidos, trabajos de herrería, alimentos dulces, bijou, inciensos, cosmética, juguetes, ropa de ajuar, objetos, trabajos en maderas, jabones naturales y productos de aromatización se pueden encontrar en la feria ubicada en Nicolás Rodríguez Peña e Independencia, en el extremo sur de la ciudad.

Sabrina Rene se crió en barrio Centenario, se fue un tiempo, pero hace unos meses regresó para quedarse. Desde diciembre participa de esta feria vendiendo objetos de decoración, sahumerios y cascadas de humo y cuadros artesanales, elaborados con sus propias manos con su emprendimiento Mi Rincón Tienda de regalos. “Estoy en la organización de la feria en conjunto con el municipio y los chicos emprendedores del barrio. A mí me parece ideal. Todo este año, que fue muy complicado para todo el sector de ferias, el único que nos ha dado una mano fue el municipio. Así que estamos contentos de recibir su apoyo”.

Walter Aguirre llegó a Centenario desde Arroyo Leyes con sus trabajos en herrería y madera a través de su emprendimiento Regional de la Costa. Este emprendimiento familiar ofrece para la venta dos veces por semana en distintas ferias locales tablas de madera, parrillas y accesorios para asar. Para Walter, estos espacios de visibilización que genera el municipio “son fundamentales porque al no contar con venta al público, para nosotros el local es la feria. Entonces, siempre estamos atentos a las ferias que se pueden participar y, siempre que nos autoricen, estamos atentos para venir y estar. Este es nuestro local y es la forma más directa que tenemos para estar en relación con el público, más allá del vínculo a través de las redes sociales”.