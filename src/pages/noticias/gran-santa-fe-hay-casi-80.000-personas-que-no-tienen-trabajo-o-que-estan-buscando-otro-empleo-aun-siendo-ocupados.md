---
category: La Ciudad
date: 2021-09-27T06:15:50-03:00
thumbnail: https://assets.3dnoticias.com.ar/DESEMPLEO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Gran Santa Fe: hay casi 80.000 personas que no tienen trabajo o que están
  buscando otro empleo aún siendo ocupados'
title: 'Gran Santa Fe: hay casi 80.000 personas que no tienen trabajo o que están
  buscando otro empleo aún siendo ocupados'
entradilla: La cantidad de santafesinos ocupados que están buscando activamente otro
  trabajo es la mayor de los últimos años, multiplicándose 660% respecto al segundo
  trimestre de 2020.

---
Publicado el último informe del mercado del trabajo de parte del Instituto Nacional de Estadística y Censos (Indec), la tasa de desempleo en el Gran Santa Fe arrojó un valor de 8,9% en el segundo trimestre de 2021, lo que marcó una leve merma respecto al primer trimestre y un valor 11 puntos menor que el registrado hace exactamente un año (20,3% en el segundo trimestre de 2020), con 22.000 desocupados menos.

Sin embargo, al contrastar el resto de los indicadores del mercado laboral con informes anteriores del organismo se divisan situaciones que resultan históricas para el aglomerado del Gran Santa Fe.

Esto es así, en la medida en la que la cantidad de santafesinos ocupados que están buscando activamente otro trabajo es la mayor de los últimos años, multiplicándose 660% respecto al segundo trimestre de 2020. A su vez, la cantidad de subocupados que quisieran trabajar más horas y no lo consiguen se multiplicó 640% en un año, pasando de 3.700 a 23.700 santafesinos en esta situación.

En esta situación se encuentran 33.000 personas residentes en el aglomerado santafesino, registrando una tasa del 13,5% según la última Encuesta Permanente de Hogares (EPH) realizada por el Indec. A mediados del segundo trimestre de 2020, el mismo índice había registrado solo 5.000 trabajadores (2,4%), unos 28.000 ocupados demandantes menos. La última variación trimestral resulta a su vez llamativa, reflejándose unos 9.000 nuevos ocupados que buscan un nuevo empleo en Santa Fe.

Entre las posturas, se planteó la influencia de la reactivación económica producto de las flexibilizaciones laborales en algunos rubros como también la falta de poder adquisitivo de los ingresos salariales.

En este sentido, Pusineri argumentó: "Lo que trabajamos como hipótesis es que hay actividades que antes no demandaban empleo y ahora lo hacen a medida que el cuadro sanitario evoluciona favorablemente. Como ejemplo están la industria, la construcción, el comercio, la gastronomía que empezó a funcionar de forma más plena. Al modificarse el cuadro de oferta laboral también se modifica el cuadro de los que tienen intención de buscar un nuevo empleo o de acompañar con otro el que ya tienen".

El ministro de Trabajo de Santa Fe destacó "dos cuestiones que impactan de lleno en el empleo: una tiene que ver con las restricciones sanitarias y otra con el cuadro macroeconómico. Este cuadro macroeconómico en actividades que venían muy golpeadas comenzó a revertirse. Al haber restricciones nadie salía a buscar empleo porque lo consideraban infructuoso".

Además, Pusineri subrayó que "los niveles de empleo están alcanzando los niveles de la prepandemia". Y agregó: "Hay sectores que crecieron mucho como construcción o industria y otros que tienen que ver con turismo, gastronomía, eventos, todas actividades en donde el crecimiento es mas heterogéneo. En la medida que cesaron las restricciones sanitarias en esos rubros va a estar más sostenida la creación de empleo.

Por su parte, el exministro de Trabajo Julio Genesini destacó que el 8,9% de desocupados "es un número alto que se ajusta a la realidad". Sobre la gente en búsqueda de otro empleo manifestó: "Los ocupados demandantes pueden estar buscando otro trabajo porque los ingresos están ajustados y tratan de complementarlos. A su vez, la tasa de subocupación sube o baja por el grado de dificultad que hay para sostener los trabajos más precarios".

"La crisis incide y uno lo nota. Cuando existen recesiones prolongadas o lugares en donde no varían las posibilidades de trabajo y el mercado laboral está estabilizado la gente no busca trabajo. En lugares donde no hay una gran dinámica del mercado de trabajo la gente no lo busca y la tasa da baja, lo que no quiere decir que haya menos gente sin trabajo. Estos índices están atados a la posibilidad concreta de conseguir trabajo en ese lugar", continuó el exfuncionario.

A la luz de las cifras arrojadas en el último reporte trimestral del Indec, se puede afirmar que un 30% de la población activa del Gran Santa Fe tiene problemas relacionados al trabajo, ya sea no pudiendo conseguirlo como en búsqueda de otro empleo. En esta franja se incluyen desempleados, subocupados demandantes y ocupados demandantes, lo que abarcan 78.700 santafesinos según los datos que cotejó UNO a partir del informe del Indec.

En tanto, la población económicamente activa cotejada en cada trimestre resulta un valor de referencia fundamental para hacer un análisis. Con respecto a esto, en el último año se registró un aumento del 18% en este índice, sumándose unos 43.000 santafesinos a la actividad económica respecto al segundo trimestre de 2020 (247.000 en el Gran Santa Fe). Esto puede explicar parcialmente algunos datos previamente descriptos, aunque los salarios con cada vez menor poder adquisitivo juegan un papel importante.