---
category: Estado Real
date: 2021-06-05T06:15:46-03:00
thumbnail: https://assets.3dnoticias.com.ar/PEROTTIPANDEMICO.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Perotti anunció la continuidad de las medidas de convivencia vigentes en
  la provincia en el marco de la pandemia
title: Perotti anunció la continuidad de las medidas de convivencia vigentes en la
  provincia en el marco de la pandemia
entradilla: Regirán desde el 7 al 11 junio. “Es muy importante que sostengamos el
  esfuerzo que se viene haciendo hasta aquí. La provincia ha reforzado el nivel de
  asistencia a los sectores afectados”, afirmó el gobernador.

---
El gobernador Omar Perotti y la ministra de Salud, Sonia Martorano, anunciaron este sábado las medidas de convivencia que regirán en el territorio provincial desde el lunes 7 al viernes 11 de junio.

En ese marco, el gobernador afirmó que “es muy importante que sostengamos el esfuerzo que se viene haciendo hasta aquí y del cual somos profundamente agradecidos. Esta semana continuaremos con las mismas medidas que la semana anterior. Eso nos tiene que permitir evaluar”.

En base a esas medidas, el gobernador Perotti señaló que “la provincia ha reforzado el nivel de asistencia a cada uno de los sectores de servicios, de comercios, gastronomía y hotelería, con una asistencia de tres mil millones de pesos. De ese total, mil cuatrocientos millones de pesos son de asistencia directa y mil seiscientos millones que tienen que ver con temas impositivos, a los que hay que sumarles la asistencia en la tarifa eléctrica de la Empresa Provincial de la Energía y la misma tarifa en el tema de Aguas”, explicó.

El mandatario provincial consignó que “tenemos el desafío de ponerle el mayor ritmo a la vacunación sin perder el nivel de atención. Hasta anoche, tenemos un total de 1.044.744 dosis aplicadas en la provincia de Santa Fe. Hemos recibido hasta aquí 1.300.000 dosis. Ayer recibimos el mayor número de vacunas desde que comenzó el programa de vacunación. Eso nos va a permitir estar asignando turnos por primera vez en una semana completa. Es decir, los santafesinos y santafesinas van a recibir 210.000 turnos en estos días. Este es el ritmo que queremos mantener con el tema de vacunación” subrayó Perotti.

“La vacuna está cerca y funciona, hagamos el esfuerzo para llegar todos los santafesinos y santafesinas y ser integrantes y partícipes de ese proceso de vacunación, el más grande que ha tenido la provincia de Santa Fe”, finalizó el gobernador.

Por su parte, Martorano explicó que “hemos extendido el sistema sanitario al máximo, tenemos que trabajar en la variante de bajar la tasa de contagios. En este sentido, las medidas de restricción se deben sostener y, sobre todo, vamos tener que continuar con la no presencialidad en esta semana, pero sí sosteniendo la educación en la distancia”.

“Esta semana se va a realizar una mirada especial en las localidades rurales y pequeñas, evaluando el número de casos. Pero en líneas generales tenemos toda la provincia, los 19 departamentos, en alto riesgo, alguno en alarma. Esto nos lleva a continuar con estas medidas de no presencialidad y sostener la distancia”, finalizó la ministra de Salud.