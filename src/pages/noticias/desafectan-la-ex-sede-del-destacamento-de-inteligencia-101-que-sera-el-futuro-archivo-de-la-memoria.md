---
category: Agenda Ciudadana
date: 2021-06-14T06:00:17-03:00
thumbnail: https://assets.3dnoticias.com.ar/EXSEDE.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Desafectan la ex sede del Destacamento de Inteligencia 101 que será el futuro
  Archivo de la Memoria
title: Desafectan la ex sede del Destacamento de Inteligencia 101 que será el futuro
  Archivo de la Memoria
entradilla: Se trata de la vieja casona desde donde se daban órdenes de desaparición
  y secuestro de personas durante la última dictadura, que será transformada en la
  sede del Archivo Provincial de la Memoria.

---
El ex Destacamento de Inteligencia 101, una vieja casona ubicada en el centro de la ciudad de La Plata desde donde se impartían órdenes de desaparición y secuestro de personas durante la última dictadura, será transformado en la sede del Archivo Provincial de la Memoria.

"Constituye un acto de reparación y resignificación de un lugar clave del esquema represivo", coincidieron funcionarios nacionales y provinciales.

El vicepresidente de la Agencia de Administración de Bienes del Estado (AABE), Juan Debandi, dijo a Télam que la desafectación del inmueble se basa en "la mirada que tiene el Estado nacional sobre los bienes del Estado, en la creencia de que cumplen un rol estratégico, tanto para las políticas de memoria como también para la producción, para la economía, para la salud, y para la educación".

Añadió que se busca "poner en valor la tierra como un fin estratégico" en un trabajo "coordinado con las distintas dependencias, en beneficio de la sociedad", algo "muy diferente de lo que puede ser una mirada especuladora, basada por ejemplo en desarrollos inmobiliarios y privatizaciones", apuntó.

La ex sede del Destacamento de Inteligencia 101 está emplazada en la Calle 55 Nº 619, abarca una superficie total de 655,35 m2, y fue señalizado en el año 2014.

El subsecretario de Derechos Humanos de la Provincia de Buenos Aires, Matías Moreno, remarcó a Télam que "recién ahora se pudo avanzar en la desafectación del inmueble, lo que constituye un acto de reparación y resignificación".

Moreno recordó "la desafectación de la casa como dependencia del Destacamento de Inteligencia 101 había sido ordenada en la sentencia en el Juicio de "La Cacha" -el ex centro clandestino de detención que funcionó detrás del penal de Olmos- en el año 2014, a pedido de las querellas, pero en los cuatro años posteriores del macrismo, donde hubo un total desinterés por las políticas de derechos humanos, no se pudo avanzar".

Añadió que "por eso, ni bien asumimos, nos pusimos en contacto con la AABE con quienes estuvimos trabajando un año y medio para que esto sea posible".

En octubre de 2014, el Tribunal Oral Federal 1 de La Plata condenó a 15 acusados por delitos de lesa humanidad cometidos en La Cacha, nueve de ellos militares que se desempeñaron en el Destacamento de Inteligencia 101 del Ejército.

Por primera vez se estableció la responsabilidad de los oficiales de inteligencia no sólo en la identificación de personas, sino también en su secuestro e interrogatorio. En el fallo se indicó al Poder Ejecutivo Nacional y al Provincial, la adopción de medidas necesarias para desafectar los inmuebles donde funcionaron el Destacamento 101 y La Cacha, para ser Sitios de Memoria.

El miércoles pasado fue publicada en el Boletín Oficial la Resolución 40/2021 firmada por el titular de la AABE, Martín Cosentino, y el vicepresidente Debandi, que desafectó de la jurisdicción del Ministerio de Defensa el inmueble a fin de transformarlo en asiento del Archivo de la Memoria de la Provincia de Buenos Aires.

Moreno indicó que "en diciembre pasado, el gobernador Axel Kicillof decidió que por primera vez en la historia la Provincia de Buenos Aires contara con un archivo propio", que actualmente funciona en oficinas de la subsecretaría de Derechos Humanos.

**Recuperación y archivo**

"Pero ahora, que se ordenó la desafectación del ex Destacamento, empezarán las obras para la recuperación del lugar que se encuentra en mal estado por el tiempo, y luego podremos instalar allí la sede del archivo", apuntó Moreno.

Sumó que la conformación de un archivo bonaerense "era una deuda ya que hay muchas provincias que contaban con uno, y además obedece a la Ley Nº 12.498, que crea el Registro Único de la Verdad y dictamina como valor primordial el derecho a la verdad de una comunidad".

Moreno valoró la "decisión política" de constituirlo y recordó que su conformación se sustenta también "en las normativas internacionales a las que nuestro país adhiere y que tienen que ver con la desafectación de los lugares desde donde se ejerció la represión ilegal".

"Así, un lugar oscuro, donde existió el horror se transformará ahora un lugar abierto, de memoria, donde albergar el acervo documental de lo que fue la dictadura, en un trabajo articulado también con los municipios", sumó.

Debandi consideró que "alcanzar esta meta simbólica es muy bueno" porque muestra que "el Estado puede transformar un sitio que representó una de las etapa más cercana y difícil de nuestro país, invirtiendo los roles, y dándole una mirada de reparación, de inclusión y de contención para toda la sociedad. Eso, es tremendamente valioso para nosotros.

La creación del Archivo Provincial de la Memoria fue establecida por el Gobierno provincial mediante el decreto 37/2020 con el objetivo de "unificar en un solo organismo centralizado las tareas de supervisar, diseñar y evaluar políticas para la recopilación, conservación y archivo de materiales vinculados a la actuación del terrorismo de Estado en el ámbito de la Provincia de Buenos Aires, gestionando lo relativo a la aplicación y observancia de las citadas leyes provinciales, contribuyendo así al proceso de Memoria, Verdad y Justicia".

Además de permitir la consulta permanente funciona como soporte y resguardo de este acervo documental en tanto prueba para los procesos judiciales respecto de las responsabilidades del accionar del terrorismo de estado en la última dictadura cívico-militar.

El Destacamento 101 del Ejército cumplió un rol clave durante la dictadura en el esquema represivo de la región, ya que desde allí se recolectaba información para planificar secuestros, torturas, desapariciones y asesinatos.

Fue el centro neurálgico del sistema represivo de gran parte de la Provincia de Buenos Aires, una unidad de inteligencia del Ejército dependiente del Jefatura II de Inteligencia del Estado Mayor del Ejército, que funcionó en pleno centro comercial y político de La Plata.

Se trató de la base de operaciones desde la cual se planificaron y ejecutaron secuestros, interrogatorios bajo tortura, asesinatos y otras acciones represivas ilegales del Estado en el partido de La Plata y otras localidades bonaerenses a partir de 1975.

Desde 1976 esas acciones se sistematizaron y multiplicaron en el marco del plan represivo de la dictadura.

Desde el Destacamento 101 se desplegaron tareas de inteligencia principalmente en jurisdicción de la Subzona 11, que abarcaba a la zona sur del conurbano bonaerense y San Andrés de Giles, Luján, Mercedes, General Rodríguez, Marcos Paz, General Las Heras, Navarro, Lobos y Cañuelas. Además, tenía injerencia sobre las ciudades de San Nicolás, Mar del Plata y Junín. En todo ese vasto territorio funcionaron 18 centros clandestinos de detención, entre los que sobresalieron el Pozo de Banfield, el Pozo de Quilmes, la Brigada de Investigaciones de La Plata, el Pozo de Arana, El Banco, Vesubio y La Cacha.