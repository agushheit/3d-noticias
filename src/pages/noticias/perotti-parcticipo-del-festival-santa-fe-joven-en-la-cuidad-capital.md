---
category: Estado Real
date: 2021-11-01T06:00:20-03:00
thumbnail: https://assets.3dnoticias.com.ar/santajoven.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Perotti parcticipó del festival Santa Fe Joven en la cuidad capital
title: Perotti parcticipó del festival Santa Fe Joven en la cuidad capital
entradilla: Se trató de un encuentro en ocasión de cumplirse un año de la creación
  del programa Boleto Educativo Gratuito (BEG). “Es una conquista para los estudiantes
  y los docentes”, dijo el gobernador.

---

El gobernador Omar Perotti participó este domingo en el centro cultural La Redonda, del Festival Santa Fe Joven que se llevó a cabo en ocasión de cumplirse un año de la creación del programa Boleto Educativo Gratuito (BEG).

En la oportunidad, Perotti expresó que se trató de “un hermoso festival, con mucha gente que vuelve a encontrarse después de mucho tiempo y eso nos hace muy bien”.

Sobre el Boleto Educativo Gratuito, el gobernador dijo que “es una conquista para los estudiantes, para los docentes y para los asistentes escolares. Había que invertir en educación y esta es una inversión, no puede haber ningún obstáculo entre el que quiere ir a estudiar y poder hacerlo”.

Luego, Perotti remarcó que “en momentos de crisis el costo de boleto va dejando gente de lado, y también, te va alejando de los tuyos porque no podés volver a tu casa. Por eso me parece un muy buen festejo de algo que tiene que quedarse, ya que los jóvenes y los docentes lo han incorporado”.

“Como todo, en el inicio, hay cosas para ajustar. Hay que recordar que no estaba este programa, es la primera vez que funciona. Entonces, no es fácil acomodar la misma modalidad de transporte en una provincia tan heterogénea, con lugares donde hay mucho transporte y lugares donde no hay”, añadió el gobernador.

En ese sentido, destacó “la implementación del Boleto Educativo Rural en lugares donde hay que armar el transporte porque no hay línea, y muchos chicos, cuando terminan la primaria en alguna de las escuelas rurales, el mayor problema viene en cómo van a ir a la secundaria. Y este es una herramienta que le da la posibilidad de hacerlo, ayudando a que una provincia, como siempre decimos, acepte las diferencias, pero no las desigualdades”.

Por último, Perotti dijo que “si no tenemos las mismas oportunidades para formarnos, para ser parte del sistema educativo, sin dudas que allí se nos queda alguien afuera, alguien con menos posibilidades. Es por eso que vale la pena el festejo, vale la pena esta lucha, conseguirlo y que lo disfruten plenamente a favor de la educación”.

Por su parte, el director provincial de Boleto Educativo y Franquicias, Juan Rober Benegui, mencionó que “el balance del primer año es más que positivo, con más de 240.000 inscriptos en toda la provincia, de los cuales 19.000 son usuario del Boleto Educativo Rural”. A su vez, destacó que “el programa representa una ayuda concreta al bolsillo de las familias santafesinas y queremos que se convierta en una política de Estado porque este tipo de programa, que está demostrado que tiene buenos resultados, debe permanecer en el tiempo”.

Para finalizar, Benegui expresó que “cuando lanzamos el programa lo hicimos para una generalidad de casos, sabiendo que, una vez en marcha, teníamos que realizar adaptaciones a realidades particularidades, y fue así como incorporamos a Centros de Educación Física, Bibliotecas Pedagógicas, Cocinas Centralizadas, Talleres de Educación Manual y, seguramente, va a haber un montón de cosas más para ir ampliando y perfeccionando el programa”.

Del evento participaron artistas locales, hubo una feria de emprendedores, propuestas gastronómicas e intervenciones artísticas de distintos géneros y, además, estuvieron presentes diferentes áreas del gobierno de la provincia de Santa Fe exponiendo y brindando información respecto a los diferentes programas o políticas destinadas a las y los jóvenes de todo el territorio santafesino.