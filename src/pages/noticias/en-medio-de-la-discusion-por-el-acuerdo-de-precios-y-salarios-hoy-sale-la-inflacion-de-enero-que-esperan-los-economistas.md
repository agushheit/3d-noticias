---
category: Agenda Ciudadana
date: 2021-02-11T07:07:25-03:00
thumbnail: https://assets.3dnoticias.com.ar/alza-precios.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Clarín
resumen: 'En medio de la discusión por el acuerdo de precios y salarios, hoy sale
  la inflación de enero: qué esperan los economistas'
title: 'En medio de la discusión por el acuerdo de precios y salarios, hoy sale la
  inflación de enero: qué esperan los economistas'
entradilla: Se aguardan alzas en alimentos y turismo. Las mediciones de los privados
  la ubican entre 3,5% y 4%, el nuevo piso para el ritmo al que suben los precios.

---
Después de la suba inflacionaria del 4% en diciembre -la más alta del 2020- los economistas prevén que en enero esa cifra puede reeditarse e incluso puede ser más alta dada la “descompresión” que están teniendo buena parte de los precios de la economía. Esta situación pone mayor presión sobre los alimentos que siguen aumentando por encima del nivel general de la inflación.

Hoy se conocerá a las 16 horas el dato del IPC de enero que divulgará el Indec. Ferreres midió 3,6% de suba en enero, Ecolatina 3,7% y el IPC online de Bahía Blanca 5,2%

Pablo Goldin, economista de la consultora Macroview, contó a Clarín días atrás que: “en enero nos esta dando una inflación muy alta. Claramente mas alta que la de diciembre y hasta podría llegar al 5%”, señala. Este resultado, según el analista, tiene que ver con que “la mitad de los precios regulados o controlados por el Gobierno está aumentando a un ritmo del 2%, mientras que la otra mitad, lo hace a un ritmo del 5%”. La excepción, según estos registros, son los alimentos que crecen al 7%, según Goldin.

El rubro alimentos y bebidas (además del Turismo), es uno de los que lideran las subas del mes y están centradas en la carne, la fruta y las verduras, Para la consultora LCG, que mide los alimentos en forma semanal, "en las últimas cuatro semanas, carnes (9,1%) y frutas (8%) sostienen los mayores aumentos" consignó en su último informe. 

"En la cuarta semana de enero la suba de los alimentos promedió 1,2%, es decir se desaceleraron 0,6 puntos respecto a la semana anterior. Pero el índice del rubro presentó una inflación mensual promedio de 4,7% en las últimas 4 semanas y 5,5% medida punta a punta en las mismas semanas.  Estos niveles de precios dejan un arrastre para el mes de febrero de 2,1%", explicó.

Para la economista de Abeceb, Soledad Perez Duhalde, la inflación de enero va a dar entre 3,9% y 4,4% " incluso puede superar ese nivel", advirtió. "Con una inflación minorista que supera el 4% , el paso a precios (pass through) genera incertidumbre", dice. En realidad, según la economista, "el primer trimestre del año se va a presentar "caliente" en materia inflacionaria (3,4% promedio, por el impacto de naftas, peajes, telecomunicaciones, subtes, colegios, etc.) con un enero más intenso en este sentido, comentó.

Según la economista Victoria Giarrizzo, "enero podría haber sido peor en ajustes porque el consumo sigue caído", apuntó. De todas maneras, sus previsiones sobre el IPC son altos: entre 3,7 y 4% sin contar la última semana del mes. En tanto, los alimentos venían subiendo 4,1%. "En el sector de bienes lo que se está viendo son aumentos fuertes en algunos segmentos de productos que se están reacomodando porque se vieron atrasados el año pasado", explicó.

Por su parte, el economista Martin Vauthier de la consultora Eco Go, agrega: "estamos proyectando entre 3,5% y 4%, aunque mas cerca de 4% a juzgar por las ultimas mediciones", apuntó.

Finalmente, para los economistas incluidos en el Relevamiento de Expectativas de Mercado (REM) que elabora el Banco Central la inflación de enero será del 3,9%.