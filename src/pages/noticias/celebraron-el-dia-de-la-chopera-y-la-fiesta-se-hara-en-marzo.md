---
category: La Ciudad
date: 2022-01-24T06:15:26-03:00
thumbnail: https://assets.3dnoticias.com.ar/chopera.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Celebraron el día de la chopera y la fiesta se hará en marzo
title: Celebraron el día de la chopera y la fiesta se hará en marzo
entradilla: 'Anunciaron que están abiertas las inscripciones para el concurso de choperas.
  La fiesta será el 5 y 6 de marzo

  '

---
Cada 22 de enero se celebra en la ciudad de Santa Fe, el Día de la Chopera. A pesar de la tradición, la fiesta anual se vio suspendida dos años seguidos por la pandemia. Este 2022 se resolvió que será festejado el 5 y 6 de marzo.

Para no dejar pasar la fecha, este viernes  hubo una pequeña celebración entre los organizadores de la fiesta de la chopera, previos ganadores del concurso y autoridades locales. Gabriel Andruszczyszyn, dijo "Gracias al acompañamiento del Concejo Municipal celebramos el día de la chopera que se festeja los 22 de enero, estamos muy contentos. Es algo simbólico, es un prelanzamiento de la fiesta de la chopera que reúne un montón de valores que tenemos los santafesinos".

**"La Gringa", una chopera ganadora de ediciones pasadas.**

"Así como el liso, el barril, tenemos este objeto que es la chopera que también nos identifica. El día de la chopera tiene la impronta porque era el cumpleaños de mi papá y cada 22 de enero festejábamos con un barril en casa. Era la forma de expresar el sentimiento que tenemos los santafesinos, relacionados con el calor. Lo sacamos a la luz, después que ya pasaron ocho años de la primera fiesta. Tenemos ocho campeones y tenemos el día declarado de la chopera. Nos llena de orgullo. Que esa semilla que plantamos diez años atrás dio sus frutos", agregó Andruszczyszyn.

En esa línea invitó a cualquier persona a registrar su chopera a nivel nacional. Se pueden inscribir a la novena edición en [fiestadelachopera.com.ar](http://www.fiestadelachopera.com.ar/) para la próxima fiesta. El premio es 15 litros de cerveza, una remera y un gorro.

"Llevamos dos años sin poder hacer la fiesta, pero es algo que en el fondo no se pierde. La idiosincrasia cervecera santafesina que tanto soñamos cada día es más fuerte, somos referentes nacionales y eso nos llena de orgullo", sostuvo el impulsor de esta efeméride.

Entre los presentes de la modesta celebración que se hizo en la Estación Belgrano, estuvo uno de los ganadores de las ediciones pasadas, Walter Oggier que viajó desde el interior de la bota santafesina para formar parte de este día: "Son sentimientos. Es algo muy grande para nosotros. Siendo de San Jerónimo Norte es algo muy arraigado a lo nuestro que lo llevamos muy dentro. A los futuros participantes les digo que se animen a anotarse si son fieles seguidores de las choperas. Al que le guste que se anote, va a ser muy experiencia muy linda".

Entre las choperas que se exhibieron hoy estaba la chopera de roble de Oggier a la que nombró "La Gringa": "Le hicimos una serpentina nueva, porque era vieja, y la tallamos con escudos uno alemán y otro suizo porque yo y mi mujer de ahí venimos".

**Pionero**

Cabe recordar que por Resolución del Concejo Municipal, a instancias de un proyecto de Adriana "Chuchi" Molina, se estableció el 22 de enero de 2022 como el Día de la Chopera en el ámbito de la ciudad de Santa Fe, en homenaje al natalicio de Enrique Andruszczyszyn, pionero en el rubro de choperas y venta de barriles en Santa Fe desde 1962. El arraigo de la chopera es tan fuerte en Santa Fe que se constituye como un bien material en el hogar de los santafesinos y es parte del acervo cultural y popular de la ciudad.