---
category: Agenda Ciudadana
date: 2021-02-19T07:13:36-03:00
thumbnail: https://assets.3dnoticias.com.ar/tarjetas.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Agencias
resumen: Tener una tarjeta de crédito pasó a costar hasta más de $ 1000 por mes
title: Tener una tarjeta de crédito pasó a costar hasta más de $ 1000 por mes
entradilla: El Banco Central autorizó una suba de comisiones del 9% en enero y otro
  9% en febrero. Las entidades no suelen venderla como mono producto, sino dentro
  de un paquete. Los bancos públicos son los más baratos.

---
Luego de que el Banco Central autorizara una suba de comisiones del 9% en enero y otro 9% en febrero, tener una tarjeta de crédito puede llegar a costar hasta más de $ 1000 por mes.

El tema es que los bancos no suelen ofrecerla como un mono producto, sino tratan de venderla como un paquete, donde dan también cuenta corriente y caja de ahorro en dólares.

Por el paquete básico, que es el más barato de todos, los bancos privados más grandes del país cobran más de $ 1000: Galicia $ 1180, BBVA $ 1163, Santander $ 1100, Patagonia $ 1095 y Supervielle $ 1087, de acuerdo al cuadro que muestra la web del BCRA en las comparaciones de comisiones. Siempre por tarjetas básicas que permitan consumos en el exterior.

Los más baratos son los públicos, ya que el Provincia cobra $ 380 y el Nación $ 638.

Si se toma como ejemplo una tarjeta de crédito internacional, de manera aislada, sin el paquete, hay bancos donde sale más de $ 1000 por mes, como el caso del Hipotecario, que cobra $ 6975 por la renovación anual y $ 527 por la administración y mantenimiento de cuenta mensual. Eso sí, que no se vaya a perder la tarjeta, porque la reposición o reimpresión por robo o extravío cuesta $ 714.

Galicia, por su parte, cobra $ 3900 de renovación anual y $ 220 de mantenimiento mensual, mientras en Santander vale $ 3755 por año y $ 206 por mes, más $ 563 en caso de que se pierda y haya que hacerla devuelta.

En este sentido, los bancos públicos también son los más económicos: Ciudad cobra $ 2624 por año y $ 171 por mes (más $ 434 si se pierde), Nación $ 2052 anuales más $ 222 mensuales ($ 369 por pérdida) y Provincia $ 1475 al año, $ 118 al mes y $ 262 en caso de pedir una reimpresión.

En el Bapro destacan que bonifican un 100% sobre consumos anuales superiores a $ 75.000, y en el Ciudad sobre consumos superiores a $ 213.000.

Por lo general, también lo tienen bonificado quienes cobran su sueldo a través del banco, y sino el empleado puede cambiarse a otra entidad, pese a que su empresa tenga un banco de cabecera.

Los bancos privados también bonifican por determinado nivel de consumos, o en varias ocasiones, si el cliente se lo pide por determinado motivo hacen algún descuento o una bonificación por un período determinado. En muchos casos, sólo vale pedirlo, porque al que no lo pide, no se lo dan.

Así, en el primer mes del año, las operaciones a través de tarjetas de crédito, registraron un saldo de $930.732 millones, lo cual significa una suba de 2,0% respecto al cierre del mes pasado, unos $ 17.848 millones por encima de diciembre marcando una desaceleración en lo que respecta al uso y compras financiadas.

Luego de un último trimestre del año 2020 marcado por una fuerte aceleración de los consumos, encontramos un techo en la actividad: los Bancos se muestran cautelosos a la hora de ampliar los límites de crédito debido a los efectos negativos de la pandemia sobre la situación financiera de los usuarios en general", aseguró a ElCronista Guillermo Barbero, socio de First Capital Group.

En cuanto a las operaciones con tarjetas de crédito en dólares, las mismas registraron una baja interanual del 44,0%, aunque con un comportamiento mensual irregular. En enero crecieron 11,3% respecto al mes anterior. Sin embargo, "estos valores NO son significativos en función de la totalidad de operaciones del rubro", finalizó Barbero.