---
category: La Ciudad
date: 2021-12-17T06:00:18-03:00
thumbnail: https://assets.3dnoticias.com.ar/JAVIERDELAROSA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'Barrio Transporte: licitaron obras de pavimento y desagües pluviales por
  unos $ 161 millones'
title: 'Barrio Transporte: licitaron obras de pavimento y desagües pluviales por unos
  $ 161 millones'
entradilla: ".Además, en breve se comenzará una intervención similar sobre calle 1°
  de Mayo: habrá una inversión total de $ 400 millones para ese sector de la ciudad.
  Los fondos son nacionales y provinciales.\n\n"

---
Este jueves se conocieron las ofertas económicas para comenzar un proceso de mejora urbana de barrio Transporte. Los trabajos incluirán pavimentación, desagües y mejora de la iluminación sobre calle Javier de la Rosa. Las obras, financiadas en el marco del Plan Argentina Hace, tienen un presupuesto oficial de $ 161.244.515,94 pesos (el plazo de obra será de 180 días corridos) y se suman a las tareas que se realizarán sobre calle 1° de Mayo, con financiamiento del gobierno provincial.

 En el acto de apertura de sobres se presentaron las siguientes empresas: Mundo Construcciones S.A., que cotizó los trabajos en $ 175.580.289,62; Winkelmann S.R.L, que ofertó por $ 159.590.943,74, y Pilatti S.A., que propuso concretar la obra por un monto de $ 202.585.276,69.

 Con todo, a partir de los proyectos presentados por la Municipalidad y trabajados de manera articulada con los gobiernos provincial y nacional, se alcanza una inversión total de más de 400 millones de pesos en el barrio. Es una primera etapa de trabajo en el marco de una intervención integral que está desarrollando el municipio y que incluye, también, la regularización de la vecinal.

 Al finalizar el acto, Jatón resaltó que estas políticas públicas llegan a un sector de la ciudad que fue muy postergado durante muchas gestiones municipales. "El barrio Transporte empieza a abandonar el olvido del Estado por más de 50 años, por eso hoy es un día histórico", valoró el primer mandatario local.

 "Frente a esa postergación -prosiguió-, lo que hicimos fue empezar a proyectar, a programar y a gestionar", indicó el intendente, y destacó que "hoy es un día clave para eso, porque acabamos de licitar una obra que es un trabajo integral. Barrio Transporte tendrá un corazón bien estructurado, con desagües con luces led, con asfalto, y a partir de allí, la vida de los vecinos y vecinas empezará a cambiar".

 Por otra parte, Jatón anticipó que están avanzadas las gestiones para que en Villa Elsa -un espacio cercano que estaba usurpado hace años- lleguen obras de agua potable y cloacas.

 Por su parte, Juan Carlos Trovetti, presidente de la vecinal del barrio Transporte, indicó que "después de 50 años tenemos la posibilidad de que se hagan obras por cerca de 400 millones en nuestro barrio, como dijo el intendente. Esto nos pone contentos pero no nos olvidamos que estuvimos postergados durante 50 años". El vecinalista dijo que las obras programadas "modificarán sin dudas al barrio, esto es un inicio que aplaudimos, nos pone muy contentos y vamos a acompañar", concluyó.

 **La obra, paso por paso**

 Paula Nardín, directora ejecutiva de Coordinación Técnica municipal, brindó detalles de los trabajos que transformarán ese sector. Las tareas consisten en el pavimento de hormigón de 8 cuadras de Javier de la Rosa, desde Facundo Zuviría hasta Aristóbulo del Valle, con los desagües pluviales correspondientes, rampas de accesibilidad y el recambio de luminarias a led. Las tareas también incluyen obras forestación.

 Cabe recordar que ya se está por adjudicar otra obra de igual alcance en otra arteria muy importante para el barrio, como es 1° de Mayo. "En ese caso es pavimento y desagües desde Gorriti hasta calle Risso, y se trata de una inversión de 220 millones de pesos, lo que totaliza cerca de 400 millones de pesos que se invertirán en barrio Transporte", indicó Nardín.