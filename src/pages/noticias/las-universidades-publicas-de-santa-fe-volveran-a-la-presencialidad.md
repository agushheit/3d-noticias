---
category: Agenda Ciudadana
date: 2021-03-13T06:00:05-03:00
thumbnail: https://assets.3dnoticias.com.ar/unl.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Las universidades públicas de Santa Fe volverán a la presencialidad
title: Las universidades públicas de Santa Fe volverán a la presencialidad
entradilla: Se firmó un acuerdo entre el gobierno provincial y las cinco universidades
  públicas de la provincia, a fin de retomar las actividades presenciales en las aulas
  de forma gradual.

---
Las universidades públicas nacionales radicadas en la provincia firmaron un acuerdo con el gobierno provincial para retomar gradualmente las actividades presenciales en las aulas. En este sentido, se anunció que las actividades se irán planificando y habilitando de acuerdo a las prioridades establecidas en cada carrera, dependiendo de su grado de dependencia a la presencialidad.

Se tendrá en cuenta tanto el desarrollo de la situación sanitaria como la del transporte público -local, media y larga distancia para la vuelta a las actividades. Además, se prestará especial atención a la simultaneidad de las actividades para llevar a cabo el retorno gradual a la presencialidad en las universidades.

Se remarcó, además, que las actividades deberán desarrollarse teniendo en cuenta el cumplimento de los protocolos correspondientes, los aforos de los edificios y siguiendo las medidas adoptadas por el Ministerio de Educación de la Provincia de Santa Fe.

De parte de la provincia, el documento lleva la firma de la ministra de Salud de Santa Fe, Sonia Martorano y del Ministro de Trabajo, Juan Manuel Pusineri. Los referentes de las Universidades de la ciudad que firmaron fueron el rector de la Universidad Nacional del Litoral (UNL), Enrique Mammarella y por la Universidad Tecnológica Nacional (UTN) sede Santa Fe, el decano Rudy Omar Grether.

El acuerdo suscripto sostiene: “ante el comienzo del ciclo lectivo 2021 en instituciones universitarias se establece que la apertura de las mismas se regirá por protocolo conforme Resolución N°386/2021 y Anexo del Consejo Federal de Educación, aprobado por el Ministerio de Salud”.

Al respecto el rector de la UNL, Enrique Mammarella, valoró que “este nuevo acuerdo es producto del trabajo coordinado y en conjunto entre las universidades santafesinas y el Gobierno provincial que venimos desarrollando de manera sostenida desde el comienzo de la pandemia con el objetivo de incrementar, gradual y sostenidamente, la presencialidad necesaria en las Universidades para que se puedan realizar todas las actividades sustantivas”.