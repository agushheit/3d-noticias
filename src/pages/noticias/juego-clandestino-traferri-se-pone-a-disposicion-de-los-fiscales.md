---
category: Agenda Ciudadana
date: 2020-12-10T11:18:42Z
thumbnail: https://assets.3dnoticias.com.ar/traferri.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: El Litoral'
resumen: 'Juego clandestino: Traferri se pone a disposición de los fiscales'
title: 'Juego clandestino: Traferri se pone a disposición de los fiscales'
entradilla: 'El senador tomó la decisión después de reunirse con su abogado. Niega
  las imputaciones de Gustavo Ponce Asahad y entregará un escrito a los fiscales de
  la causa. '

---
Las derivaciones institucionales y coletazos políticos que generará la declaración del ex fiscal Gustavo Ponce Asahad son aún difíciles de mensurar en la provincia. 

Por lo pronto, el jefe de la bancada oficialista en el Senado se reunió ayer durante varias horas con su abogado para definir los pasos a seguir. 

Armando Traferri, representante del departamento San Lorenzo, fue sindicado por el fiscal declarante como uno de los supuestos promotores de la asociación ilícita que regenteaba el juego clandestino en el sur provincial. 

El legislador conversó con su letrado, el Dr. José Luis Vázquez, y según pudo saber El Litoral, resolvió presentar este miércoles a primera hora un escrito ante los fiscales Matías Edery y Luis Schiappa Pietra para ponerse a disposición. 

Ambos llevan adelante la investigación sobre juego clandestino y frente a ellos, de hecho, volvió a declarar el viernes Ponce Asahad. Fuentes irrefutables consultadas por este diario dijeron también que Traferri entregará luego copia de ese mismo escrito al resto de sus pares. 

El senador rechaza las imputaciones vertidas por el destituido fiscal Ponce Ashad, y sobre esa misma base, asegura que no tiene previsto ni hay razones para un alejamiento siquiera temporal de su banca. En tal sentido se descartó, por ejemplo, un eventual pedido de licencia del legislador.

<br/>

## **"Interna, no"**

Si bien diferentes voces de la oposición han intentado instalar que el escándalo de proporciones que se avizora en la provincia encuentra su explicación en "la interna peronista", el propio gobernador Omar Perotti se encargó de refutar esa hipótesis. 

Desde el peronismo se recuerda, incluso, que el envío del pliego y la designación de Ponce Asahad se dieron durante un gobierno socialista. En ese contexto y en declaraciones al medio porteño Infobae, el gobernador de Santa Fe dijo tener "tranquilidad" frente a la situación. "Todos deben sentirse con la libertad de investigar a quien deban investigar – dijo el mandatario-. Es la única manera de que se acabe la impunidad. 

Que le toque a quien le toque. Ahora tal vez entiendan el sentido del discurso que expresé al asumir en diciembre del año pasado. Duermo tranquilo. Santa Fe tiene una historia de calidad institucional que merece recuperarse. Esto no es una interna del Justicialismo – sentenció -. 

Es corrupción versus decencia. Sé que para lograr seguridad hay que cortar los vínculos con el delito. Esto será una divisoria de aguas brava y dura, entre propios y extraños. No vine para que las cosas queden igual. Voy hasta el hueso", aseveró.

<br/>

## **Estrategia**

En otro orden, Luis Rossini, abogado del empresario de juego clandestino Leonardo Peiti, adelantó que también el miércoles se presentará ante la justicia por la mención que realizó Ponce Asahad en relación con un encuentro filmado en una cochera.

En declaraciones a LT10, Rossini reparó en la condición bajo la que declaró el ex fiscal; lo hizo "como imputado, y no como arrepentido, porque eso le permite mentir", disparó. "Es una estrategia del ex fiscal y alguien se beneficia desde el punto de vista de la política. Peiti no tiene socios en la política que yo sepa", agregó. 

"Ponce Asahad declaró en la causa de la organización de juego que protegía a fiscales y sectores de la política santafesina. El ex fiscal manifestó que la reunión que tuvo en la cochera fue una cama armada entre un senador y yo. Es por esto que ya le anticipé a su abogado las acciones legales que tengo previsto seguir. Si Ponce Asahad tuvo contacto con un senador, pongo a disposición mis teléfonos y voy a iniciar una acción de daños y perjuicios. Voy a pedir inhibir sus bienes porque me involucra en algo que no es", sentenció.