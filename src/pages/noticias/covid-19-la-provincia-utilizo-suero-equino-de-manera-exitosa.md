---
category: Estado Real
date: 2021-01-13T07:29:47Z
thumbnail: https://assets.3dnoticias.com.ar/hospital-Cullen.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: 'Covid-19: la provincia utilizó suero equino de manera exitosa'
title: 'Covid-19: la provincia utilizó suero equino de manera exitosa'
entradilla: La innovación estuvo a cargo del área Clínica Médica del hospital José
  María Cullen e impidió que los pacientes deban ser trasladados a terapia intensiva.

---
El Ministerio de Salud de la provincia informó que el 2 de enero comenzó a aplicar, con buenos resultados, suero equino hiperinmune anti-SARS-CoV-2 (coronavirus) en cuatro pacientes con cuadros moderados de Covid, internados en sala general, con requerimiento de oxígeno en el hospital José María Cullen de la ciudad de Santa Fe.

Al respecto, el jefe del servicio de Clínica Médica del nosocomio, Aníbal Gastaldi manifestó: «observamos que esto detuvo el proceso de enfermedad, y que no pasaron a terapia intensiva porque no progresó la dificultad respiratoria. En estos cuatro casos tuvimos muy buenos resultados porque cada vez requirieron menos oxígeno».

En este sentido, cabe señalar que el área de Clínica Médica atiende a personas que transitan estadios leves, o internación en sala general, con cuadros moderados de la enfermedad. Si en ese momento la evolución no es buena, los pacientes son derivados a la Unidad de Terapia Intensiva (UTI). «Este suero está indicado en la etapa virémica de la enfermedad, antes de los 10 días de evolución, es de fácil administración y no provocó efectos adversos», agregó el especialista.

Cabe recordar que el suero equino se utiliza como antitoxina para otras enfermedades, por ejemplo, para síndrome urémico hemolítico, como también para suero antitetánico, y contra venenos de serpientes.

<br/>

## **APROBADO POR LA ANMAT EN SUS TRES FASES**

En otro orden, Gastaldi recordó que la aprobación de la Anmat fue luego de un estudio clínico que evaluó la seguridad y eficacia del suero en 242 pacientes adultos (18 a 79 años), con enfermedad moderada a severa causada por la infección del SARS-CoV-2, confirmada por PCR, dentro de los diez días del inicio de síntomas y que requerían hospitalización. Se realizó en 19 hospitales y clínicas de AMBA, Neuquén y Tucumán. El promedio de edad fue de 54 años.

«Nosotros comenzamos a utilizarlo en pacientes con cuadros moderados que están en sala general, dentro de los ocho a diez días de la enfermedad y que requieren oxígeno (fase virémica). No ya en la fase inmunológica, en la cual el virus está a nivel genético (celular) y comienza a dañar severamente al organismo mediante diversas reacciones inmunológicas», profundizó el profesional.

Por último Gastaldi expresó que «lo que ocurrió es muy alentador porque si bien estábamos utilizando el plasma humano de convalecientes que también es útil, el suero equino hiperinmune tiene una concentración de anticuerpos mucho mayor que el humano».

<br/>

## **REDUCCIÓN DE LA INTERNACIÓN Y LA MORTALIDAD**

Cabe señalar que tal, como informara el Ministerio de Salud de la Nación, el desarrollo de este suero terapéutico «es fruto del trabajo de articulación público-privada encabezado por el laboratorio Inmunova y el Instituto Biológico Argentino (BIOL); la Administración Nacional de Laboratorios e Institutos de Salud “Dr. Carlos G. Malbrán” (ANLIS), con la colaboración de la Fundación Instituto Leloir (FIL), Mabxience, CONICET, y la Universidad Nacional de San Martín (UNSAM)».

Este tratamiento basado en anticuerpos policlonales, se fundamentó en los resultados positivos del estudio clínico de Fase 2/3, los cuales mostraron que la aplicación de esta terapia, que demostró ser segura, redujo la mortalidad casi a la mitad (45%) en los pacientes con COVID-19.

Además, las personas tratadas demostraron una reducción de la internación del 24% y del requerimiento de asistencia respiratoria mecánica de 36%, frente al placebo.