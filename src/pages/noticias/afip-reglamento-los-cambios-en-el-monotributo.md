---
category: Agenda Ciudadana
date: 2021-06-02T09:02:56-03:00
thumbnail: https://assets.3dnoticias.com.ar/monotributo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: AFIP reglamentó los cambios en el monotributo
title: AFIP reglamentó los cambios en el monotributo
entradilla: Los nuevos valores de las categorías de monotributo fueron actualizados
  de acuerdo a la variación del haber mínimo garantizado.

---
La Administración Federal de Ingresos Públicos (AFIP) reglamentó el Régimen de Sostenimiento e Inclusión Fiscal para Pequeños Contribuyentes, una ley que alcanza a los más de 4 millones de monotributistas.

La normativa introduce un conjunto de modificaciones y beneficios fiscales que armonizan la transición hacia el régimen general (autónomos), tanto en términos administrativos como en los montos de las obligaciones que deben afrontar los contribuyentes.

Los cambios reglamentados por la AFIP benefician a todos los monotributistas ya que establecen un régimen permanente que hace menos gravoso pasar al régimen general. La normativa sancionada por el Congreso habilitó un “puente” que permite a los monotributistas que se conviertan en autónomos experimentar un ahorro significativo en su carga tributaria con relación al monto que deberían pagar si no se introducen los cambios propuestos.

“La nueva ley es una herramienta fundamental para restablecer la equidad y la promoción de un régimen de inclusión tributaria y financiera”, destacó la titular de la AFIP, Mercedes Marcó del Pont.

“La norma estimula el paso voluntario al régimen general, con beneficios. Se subsidia el paso del monotributo a los autónomos”, destacó la funcionaria.

Los beneficios previstos en la normativa también contemplan reducciones del IVA y Ganancias por tres años para los contribuyentes que pasen de régimen.

**Nuevos valores del monotributo**

Los nuevos valores de las categorías de monotributo fueron actualizados de acuerdo a la variación del haber mínimo garantizado. La actualización de los parámetros y los nuevos valores de las categorías a ingresar –impuesto integrado y cotizaciones previsionales-, tendrán efectos a partir del período enero de 2021. Las nuevas categorías se encuentran disponibles para consulta en el sitio de la AFIP monotributo.afip.gob.ar

![](https://assets.3dnoticias.com.ar/monotributo.jpeg)

**Categorización retroactiva**

A partir de la actualización de los parámetros y valores del monotributo, todos los contribuyentes del régimen simplificado fueron categorizados en forma retroactiva por única vez. El procedimiento refleja la categoría que le correspondía encontrarse encuadrado a partir del 1 de febrero de 2021.

Así, las y los monotributistas se encuentran encuadrados en la categoría que les corresponda en función de la información oportunamente declarada y aquella con la que cuenta el organismo, considerando los valores de los parámetros de ingresos brutos y alquileres devengados.

La nueva categoría asignada puede consultarse a través del sitio web del organismo. Los monotributistas podrán solicitar la modificación de la categoría hasta el día 25 de junio, inclusive.

**Pago de las diferencias**

Las diferencias en concepto de impuesto integrado, cotización previsional y obra social, en virtud de la actualización de los valores de las obligaciones mensuales y/o de la categoría, correspondientes a los períodos enero, febrero, marzo, abril y mayo de 2021, podrán ingresarse a través de alguna de las distintas modalidades de pago disponibles hasta el día 20 de julio de 2021.

Las diferencias podrán consultarse desde el 1 de julio de 2021 en el portal “Monotributo” seleccionando la opción “Estado de cuenta” o ingresando al servicio denominado “CCMA – Cuenta Corriente de Monotributistas y Autónomos”, accediendo con Clave Fiscal. Los pequeños contribuyentes podrán regularizar las diferencias a través de un plan de facilidades de pago.