---
category: La Ciudad
date: 2020-12-24T11:24:11Z
thumbnail: https://assets.3dnoticias.com.ar/2412municipalidad.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Servicios municipales para esta Navidad
title: Servicios municipales para esta Navidad
entradilla: 'El jueves 24 se realizará la recolección de residuos en horario diurno
  y el viernes 25, se hará por la noche. En cuanto al barrido, habrá operativos especiales. '

---
Los servicios municipales que se prestarán durante el jueves 24 y el viernes 25, sufrirán algunas modificaciones por los festejos navideños.

En cuanto a la recolección de residuos, las empresas prestarán el servicio el jueves 24 en el horario diurno, hasta las 19 horas. En tanto, el viernes 25, feriado por la celebración religiosa, funcionará el servicio de recolección nocturno que se retoma a partir de las 21 horas. El sábado 26 y domingo 27 se retoma el cronograma habitual de fines de semana.

<br/>

<span style="font-family: helvetica; font-size: 1.35em; font-weight: bold;">Barrido</span>

En cuanto al servicio de barrido, para el viernes 25, a partir de las 7, la Municipalidad tiene previsto un operativo especial en diferentes zonas, como Recoleta, las costaneras Este y Oeste, bulevar Pellegrini y Gálvez, entre otras. Además, se prestará el servicio de barrido mecánico en las grandes avenidas como Alem y 27 de Febrero, Aristóbulo del Valle, Freyre, Facundo Zuviría, López y Planes, entre otras.

En cuanto a los residuos, destacan que los generados en estas fechas son de gran valor para la asociación que trabaja en la planta de reciclaje. Es por eso que se recomienda que se saquen adecuadamente y en bolsas para que sean debidamente recolectados; luego serán llevados al complejo ambiental por las empresas prestatarias del servicio.

<br/>

<span style="font-family: helvetica; font-size: 1.35em; font-weight: bold;">**Transporte y estacionamiento**</span>

En cuanto al transporte público de pasajeros, el jueves 24 tendrá una frecuencia habitual; y a las 21 saldrán de las paradas los últimos colectivos hasta finalizar el recorrido, excepto las líneas 2 y 9, que lo harán a las 20.

El viernes 25 el servicio se retomará desde las 5 con frecuencias de domingos y feriados. El sábado y domingo las frecuencias serán las habituales de cada fin de semana.

Por otra parte, el Sistema de Estacionamiento Medido (SEOM) será liberado el jueves 24; el viernes 25 es feriado y por lo tanto no operará; y retomará el servicio el sábado, de 7 a 13.

<br/>

<span style="font-family: helvetica; font-size: 1.35em; font-weight: bold;">**Cementerio**</span>

Con respecto al Cementerio Municipal, el horario de visita durante los dos días, tanto el jueves 24 como el viernes 25, será de 8 a 12.

En tanto, a partir del lunes 28 de diciembre, regirán los horarios de verano: de lunes a viernes, de 10 a 12 y de 16 a 19; sábados, de 16 a 19; domingos de 8 a 12.

<br/>

<span style="font-family: helvetica; font-size: 1.35em; font-weight: bold;">**Mercado Norte**</span>

El Mercado Norte, ubicado en la intersección de Santiago del Estero y Urquiza, abrirá el jueves 24 en el horario habitual de las 8, pero se extenderá hasta las 18. El viernes 25 permanecerá cerrado.