---
category: Agenda Ciudadana
date: 2021-10-15T06:00:26-03:00
thumbnail: https://assets.3dnoticias.com.ar/kaufman.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: La provincia aprobó proyectos para obras en municipios  comunas por mas de
  112 millones de pesos
title: La provincia aprobó proyectos para obras en municipios  comunas por mas de
  112 millones de pesos
entradilla: Durante un nuevo encuentro de la Comisión de Seguimiento de la ley Nº
  12.385, también se autorizaron fondos de Emergencia Climática por 5,1 millones de
  pesos.

---
El secretario de Integración y Fortalecimiento Institucional, José Luis Freyre, encabezó una nueva reunión de la Comisión de Seguimiento, ley Nº 12.385, durante la cual se aprobaron proyectos de afectación de fondos de Obras Menores y de Emergencia Climática; que en total representan una inversión del gobierno provincial de $ 117.364.402,74.

Los proyectos autorizados por la comisión corresponden a los años 2018, 2019, 2020 y 2021, ahora serán elevados al Ministerio de Gestión Pública para su aprobación final.

Del encuentro también participaron el secretario de Coordinación Administrativa y Financiera CPN Marcelo Andrés Toselli; los subsecretarios de Comunas, Carlos Kaufmann, y de Planificación, Omar Romero; los senadores Rodrigo Borla, Osvaldo Sosa y Rubén Pirola; y el diputado Juan Cruz Cándido.

**OBRAS MENORES**  
La comisión aprobó proyectos para la ejecución de obras o adquisición de maquinaria, por un total de $ 112.264.402,74

>> Las Parejas, departamento Belgrano, $ 7.821.346,44, para la adquisición de una retroexcavadora

>> Sanford, departamento Caseros, $ 892.527,03, para el recambio de luminarias fluorescentes por luminarias led

>> Frontera, departamento Castellanos, 1.710.359,20, para alumbrado público sobre el ejido urbano en el sector norte de la ciudad; y $ 1.269.187,00, para la puesta en valor del alumbrado público sobre calle 1

>> Sunchales, departamento Castellanos, $ 7.886.082,78, para la adquisición de equipos de bombeo y cañería para desagües cloacales

>> Tacural, departamento Castellanos, $ 3.751.358,87, para la construcción de pavimento de hormigón en calzadas urbanas

>> Cañada Rica, departamento Constitución, $ 1.367.498,71, para la construcción de baños en el Real Cine

>> Colonia Mascias, departamento Garay, $ 1.061.264,59, para el extendido de la iluminación en la ruta N° 1

>> Amenabar, departamento General López, $ 570.346,91, para la colocación de cámaras de seguridad y la ampliación de la central de monitoreo

>> Villa Cañas, departamento General López, $ 3.713.559,88, para la compra de un camión usado con equipo de riego

>> Malabrigo, departamento General Obligado, $ 6.989.150,60, para la adquisición de terrenos para urbanización y soluciones habitacionales

>> Reconquista, departamento General Obligado, $ 21.896.705,54, para la repavimentación de calle 47, entre calles Roca y Fray A. Rossi

>> Villa Ocampo, departamento General Obligado, $ 6.204.360,81, para la pavimentación de 5 cuadras en barrios Norte y Ocampo Samanes

>> Guadalupe Norte, departamento General Obligado, $ 3.944.204,62, para la construcción de veredas perimetrales en plaza central y estabilizado granular

>> Las Garzas, departamento General Obligado, $ 2.677.263,72, para la ejecución de estabilizado granular

>> Correa, departamento Iriondo, $ 1.682.973,73, para obras de cordón cuneta en calle Rafael Obligado, entre calle Santa Fe y 25 de Mayo

>> Candioti, departamento La Capital, $ 81.684,44, para dos columnas alumbrado publico

>> Monte Vera, departamento La Capital, $ 6.210.197,51, para la ejecución de pavimento de hormigón

>> Pujato Norte, departamento Las Colonias, $ 1.128.170,47, para la compra de una desmalezadora de arrastre

>> María Luisa, departamento Las Colonias, $ 747.646,58, para la construcción de estabilizado granular

>> San Jerónimo del Sauce, departamento Las Colonias, $ 167.225,38, para la ejecución de platea de hormigón armado para parada de colectivo

>> San Agustín, departamento Las Colonias, $ 2.762.520,74, para la colocación de aberturas en el polideportivo

>> Acebal, departamento Rosario, $ 2.561.143,54, para la pavimentación de calles urbanas con concreto Asfáltico

>> Coronda, departamento San Jerónimo, $ 1.853.900,00, para la adquisición de una pala niveladora de arrastre

>> Desvío Arijón, departamento San Jerónimo, $ 1.709.555,00, para ampliación y recambio de luminarias LED del alumbrado público

>> Colonia Esther, departamento San Justo, $ 4.205.567,81, para la adquisición de un tractor

>> María Susana, departamento San Martín, $ 5.524.113,72, para la fabricación de columnas de alumbrado publico

>> Toba, departamento Vera, $ 1.973.632,96, para la construcción de ripio y cordón cuneta

>> La Gallareta, departamento Vera, $ 560.306,54, para alumbrado público; y $ 909.578,42, para mejorado de ripio de calles públicas.

>> Santa Margarita, departamento 9 de Julio, $ 1.948.431,13, para obras de cordón cuneta

>> Montefiore, departamento 9 de Julio, $ 2.554.876,06, para la adquisición de un tractor

**EMERGENCIA CLIMÁTICA**  
Por último, se autorizaron fondos para atender Emergencias Climáticas del Fondo de Emergencia de la Ley 12.385 por un total de $ 5.100.000:

>> Chabas, departamento Caseros, $ 800.000  
>> Ingeniero Chanourdie, departamento General Obligado, $ 500.000  
>> La Pelada, departamento Las Colonias, $ 500.000  
>> Colonia Clara, departamento San Cristóbal, $ 500.000  
>> Colonia Rosa, departamento San Cristóbal, $ 800.000

>> Coronda, departamento San Jerónimo, $ 1.500.000

>> Gregoria Pérez de Denis, departamento 9 de Julio, $ 500.000