---
category: Agenda Ciudadana
date: 2021-07-10T06:00:31-03:00
thumbnail: https://assets.3dnoticias.com.ar/maisvacunas.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: " Este sábado parte un nuevo vuelo de Aerolíneas a buscar vacunas Sputnik
  V"
title: " Este sábado parte un nuevo vuelo de Aerolíneas a buscar vacunas Sputnik V"
entradilla: El Airbus 330-200, bajo el número de vuelo AR1064, tiene previsto partir
  con destino al Aeropuerto de Sheremetievo, en la capital rusa, a las 7.30 desde
  el Aeropuerto Internacional de Ezeiza.

---
Otro vuelo de Aerolíneas Argentinas partirá el sábado a Moscú en búsqueda de una nueva partida de vacunas Sputnik V, según informó el presidente de la compañía, Pablo Ceriani, a través de su cuenta de Twitter.

“Queremos anunciar que mañana 7.30am parte un nuevo vuelo de @Aerolineas_AR a Moscú para traer más dosis de @sputnikvaccine. Es la operación número 31 que realizamos en búsqueda de más dosis para la mayor campaña de vacunación de la historia”, escribió Ceriani en la red social.

El Airbus 330-200, matrícula LV-GIF, bajo el número de vuelo AR1064, tiene previsto partir con destino al Aeropuerto de Sheremetievo, en la capital rusa, a las 7.30 desde el Aeropuerto Internacional de Ezeiza y su regreso se encuentra programado para alrededor de las 22 del domingo.

Este nuevo servicio a Moscú, se suma al vuelo previsto, también para mañana, después del mediodía, con destino a Beijing y que forma parte del mega operativo que comenzó la semana pasada y que le permitirá a la Argentina recibir 8 millones de vacunas Sinoparhm en los próximos diez días, sobre un total de 24 millones acordada con el China National Pharmaceutical Group Corp, que llegarán entre julio y septiembre.

En tanto, esta tarde, está arribando el AR1073 desde Beijing con 768.000 dosis de la vacuna de Sinopharm, en lo que es la trigésima operación de este tipo realizada por Aerolíneas Argentinas.

“Son dos vuelos en menos de 48 horas y eso se traduce en decenas de miles de personas que van a recibir su vacuna en los próximos días”, señaló Pablo Ceriani, titular de Aerolíneas Argentinas.

Aerolíneas Argentinas lleva completados un total de 21 vuelos a Moscú en los que se transportaron 11.263.375 dosis de la vacuna producida por el Instituto Gamaleya. Además, desde Beijing, llegaron al país un total de 5.963.200 en 8 vuelos. De esta manera, en 29 operaciones completas se trajeron un total de 17.226.575 de dosis.