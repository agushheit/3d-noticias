---
category: Agenda Ciudadana
date: 2021-05-24T06:00:52-03:00
thumbnail: https://assets.3dnoticias.com.ar/SAPPIA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: El radicalismo propone una reforma tributaria para terminar con la inflación
title: El radicalismo propone una reforma tributaria para terminar con la inflación
entradilla: A través de un comunicado, la Comisión de Economía de la Convención Nacional
  de la UCR sostiene que un cambio en el sistema de contribuciones podría ayudar a
  terminar con la suba de precios.

---
La Comisión de Economía de la Convención Nacional del radicalismo sostuvo este domingo que la "inflación es un impuesto más" y por ende el "círculo vicioso de alto déficit fiscal, alta inflación y endeudamiento creciente" que solo podrá romperse con una profunda "reforma tributaria establecida sobre la base de un amplio consenso político y social".

"La inflación es un impuesto más, el más regresivo y el más injusto por afectar con mayor virulencia a los que menos tienen, que es aplicada por el Estado ante su incapacidad para lograr el equilibrio fiscal y recaudar de manera eficiente", explicó a través de un comunicado la Comisión de Economía de la Convención Nacional de la UCR que preside Jorge Sappia.

En ese contexto, se planteó la necesidad de "atacar de raíz las causas de la inflación, terminar con esta regresividad y poder iniciar un círculo virtuoso de estímulo a la inversión, la producción y la creación de empleo digno".

"El círculo vicioso de alto déficit fiscal, alta inflación y endeudamiento creciente solo puede romperse con una reforma profunda que además de una nueva estructura impositiva establezca una política cambiaria coherente que permita implementar una política industrial competitiva, creadora de empleos y fuertemente exportadora".

Pero una "reforma tributaria de tal dimensión solo sería posible sobre la base de un amplio consenso político y social, difícil de imaginar en un año electoral como este", argumentaron en el documento que además de Sappia firma el coordinador de la Comisión de Economía, Miguel Ponce.

También se analizó el resultado de la gira del presidente Alberto Fernández y del ministro de Economía, Martín Guzmán, por Europa, y se concluyó que trajeron "varios anuncios que pueden considerarse auspiciosos".

En el comunicado hicieron un punteo dando cuenta que consiguieron "apoyos más o menos explícitos de cuatro países a lograr una mayor flexibilidad por parte del FMI; el relanzamiento de las negociaciones con ese organismo por un programa de facilidades extendidas; una mirada más benevolente ante la aplicación de sobre tasas la deuda existente con el FMI; la hipótesis de evitar intereses punitorios por parte del club de Paris y la emisión de un monto importante de Derechos Especiales de Giro por parte del FMI en agosto".

Sobre este último punto consideraron que los Derechos Especiales de Giro implicarían "el agregado del equivalente a unos 4.200 millones de dólares a las reservas nominales del Banco Central".

Desde la Comisión de Economía sostienen que el FMI reclama al Gobierno "un ancla creíble para la inflación".

Y reclaman al Gobierno que elabore en consenso con la oposición "un plan económico sustentable y plurianual que comience por plantear todas las medidas necesarias para que haya convergencia con las previsiones del presupuesto 2021 aprobado por el Congreso Nacional".