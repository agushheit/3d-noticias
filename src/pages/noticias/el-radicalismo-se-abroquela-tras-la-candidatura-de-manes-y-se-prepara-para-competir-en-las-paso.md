---
category: Agenda Ciudadana
date: 2021-06-21T06:15:46-03:00
thumbnail: https://assets.3dnoticias.com.ar/RADICALES.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: El Radicalismo se abroquela tras la candidatura de Manes y se prepara para
  competir en las PASO
title: El Radicalismo se abroquela tras la candidatura de Manes y se prepara para
  competir en las PASO
entradilla: '"Si el radicalismo se pone de pie, lidera, se abre y convoca a todos
  los sectores, voy a acompañar", analizó el neurocirujano, una figura que atrae tanto
  dentro de las filas de la UCR como a potenciales aliados.

'

---
La aparición del neurocientífico Facundo Manes como eventual candidato de la UCR en suelo bonaerense -después de una reunión celebrada esta semana donde la cúpula partidaria le hizo el ofrecimiento- abroqueló a todo el radicalismo de cara a las próximas elecciones y entusiasmó a algunos eventuales aliados.

"Si el radicalismo se pone de pie, lidera, se abre y convoca a todos los sectores, voy a acompañar", fue la frase que eligió Manes para referirse al ofrecimiento, que los radicales interpretaron como un sí.

Tras esa respuesta, las autoridades del partido -con su titular, el mendocino Alfredo Cornejo a la cabeza- los gobernadores, los titulares de la UCR en Capital y provincia de Buenos Aires, los jefes de los bloques del Congreso y las principales figuras partidarias, dieron un apoyo cerrado a Manes.

Además, algunos dirigentes independientes, pero con pasado en el radicalismo, a los que Juntos por el Cambio buscaba seducir para armar un frente más amplio del actual, dieron algunas muestras de apoyo.

Una de ellas fue la fundadora del GEN, Margarita Stolbizer, que dijo que la llegada de Manes le daba "ilusión" y "esperanza", lo que podría ser una primera señal de confluencia.

"Voy a ser un poco audaz: para mí, hoy hay una lucecita y una ilusión que es la posibilidad de que Facundo Manes ingrese a la política de la mano del radicalismo", sorprendió en las últimas horas Stolbizer en diálogo con la radio Once Diez. Y agregó que se trata de "una persona que puede generar esta ilusión de recuperación de una esperanza hacia afuera de la política".

El otro efecto del acercamiento de Manes a la UCR fue una suerte de desafío abierto al PRO, que durante la gestión de Cambiemos había dominado las listas y ubicado a integrantes de su partido en las candidaturas más importantes.

"Juntos por el Cambio en los últimos tiempos se ha convertido en una telenovela del PRO y en un 'te quiero, no te quiero'", señaló hace tres días el gobernador de Jujuy, Gerardo Morales para transmitir el malestar del radicalismo.

"Hasta acá se ponen y se sacan entre ellos y se habían olvidado del radicalismo", siguió Morales esta semana, en referencia a la lista de nombres, todos del PRO, que en las últimas semanas circulan para las elecciones en CABA y Provincia.

**Las PASO contra el PRO**

A partir de este nuevo estado de cosas, todo indica que los socios del Juntos por el Cambio (UCR, PRO, Coalición Cívica, más su pata peronista) definirán sus candidaturas en las elecciones primarias, abiertas, simultáneas y obligatorias (PASO) a falta de un acuerdo previo sobre nombres.

"La competencia nos parece bien, y es un criterio unificado de toda la coalición", definió Cornejo apenas terminó el encuentro con Manes.

Se le sumó Morales, quien adelantó: "Vamos a ir a competir en las PASO en la provincia de Buenos Aires contra el PRO".

También el senador nacional Martín Lousteau (UCR-CABA) expresó "ojalá tengamos flor de primaria", en referencia a la PASO en el distrito bonaerense, después de participar de la reunión con Manes.

**La reacción del PRO**

Del lado de PRO por ahora callan. Primero esperarán que el "sí" de Manes sea definitivo, y allí tendrán que recalcular.

Mientras tanto, el vicejefe de Gobierno porteño Diego Santilli, quien podría ser uno de los competidores de Manes si se cumple el deseo de Horacio Rodríguez Larreta de mandarlo como su delfín a suelo bonaerense, le sacó dramatismo al tema y aceptó públicamente las primarias.

"¿Por qué no competir si las primarias están para eso? Para que la oposición se consolide, amplíe su espacio y compita", dijo, además de definir al neurocientífico como "un hombre importantísimo, alguien que se suma".

**Cornejo pidió unidad**

En medio de esta ebullición de nombres y nuevos egos que deberán congeniar, Cornejo hizo un pedido por la unidad: "Juntos por el Cambio tiene que estar unido para lograr dos objetivos: un equilibrio de poder en 2021 y una alternancia en el 2023", apuntó, pero siempre con el nombre de Manes como cabeza de lista.

Y también envió un mensaje hacia las filas del PRO, que este año se ve atravesado por la tensión entre el liderazgo emergente de Rodríguez Larreta y el objetivo de Mauricio Macri de sostenerse como jefe político.

Incluso Elisa Carrió, por ahora testigo de las peleas ajenas, convocó a la calma.

"Quiero hacer un llamado a toda la dirigencia política. Es un llamado desesperado a la unidad de la oposición para que tenga grandeza. Estoy mediando, pero no doy abasto", se sinceró.

También habló de un "destrato" por parte del PRO hacia el radicalismo y hacia su propio partido, la Coalición Cívica, dentro de JxC. "No quiero un cargo. Es mi último llamado desesperado a la unidad", concluyó Carrió, quien había elogiado la postulación de Manes pero al mismo tiempo mantiene un diálogo fluido con Rodríguez Larreta.