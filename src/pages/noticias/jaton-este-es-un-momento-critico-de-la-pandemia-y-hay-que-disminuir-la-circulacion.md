---
category: La Ciudad
date: 2021-04-23T06:43:30-03:00
thumbnail: https://assets.3dnoticias.com.ar/jaton.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad de Santa Fe
resumen: 'Jatón: “Este es un momento crítico de la pandemia y hay que disminuir la
  circulación”'
title: 'Jatón: “Este es un momento crítico de la pandemia y hay que disminuir la circulación”'
entradilla: |2-

  El intendente de Santa Fe dijo que lo central es reducir el movimiento de personas para evitar los contagios, sin afectar la economía ni las escuelas y adhirió a lo dispuesto por el gobierno provincial.

---
Frente a la nueva ola de contagios del coronavirus, el intendente Emilio Jatón anunció esta mañana que la ciudad de Santa Fe aplicará las restricciones dispuestas por el gobierno provincial (Decreto N° 386/21), a partir de este viernes 23 de abril y hasta el 2 de mayo. Entre las medidas, no se podrá circular en vehículos particulares entre las 21 y las 6 horas y habrá horario corrido en los comercios para reducir el traslado de su personal.

En conferencia de prensa realizada en la Estación Belgrano, el intendente remarcó que “hace días que venimos hablando con el gobierno provincial y los expertos y hay una mirada de que este es un momento crítico y hay que disminuir la circulación”. Pero consideró: “Si hay algo que hemos pedido al gobierno provincial es que en esa disminución de circulación no se vea afectada la actividad productiva ni el normal desenvolvimiento de las escuelas; los cuales hoy no se sienten afectados”.

En este sentido, Jatón agregó: “Muchas de las medidas que aparecieron en el decreto (provincial) fueron articuladas con distintos sectores; y también tienen una mirada en el sistema de salud. Los expertos han sido muy claros: estamos en un momento crítico y hay que disminuir la circulación. Las camas críticas en la ciudad de Santa Fe, sobre todo en el hospital Cullen, están llegando a un límite y hay que tomar medidas, junto con el gobierno provincial y las demás municipalidades del departamento”.

Con relación a los controles en los espacios públicos, el mandatario dijo -en otra parte de la conferencia- que se están reuniendo funcionarios del Ministerio Público de la Acusación, de Seguridad y de la Municipalidad para acordar cómo va a ser el procedimiento durante este fin de semana en esos lugares. “Seguramente va a haber nuevos procedimientos, nuevas metodologías porque el objetivo es disminuir la circulación y la aglomeración en los espacios públicos”, aclaró.

El intendente dio datos de la situación actual de la ciudad con relación al Covid-19. “Mientras el año pasado a esta altura los casos se contaban con una mano, hoy superan los 31.000 en la ciudad, y los 41.000 en el departamento La Capital”, indicó. Pero también aclaró que hay más de 75.000 personas vacunadas en la ciudad de Santa Fe, según datos de hace tres días.

 

**Las medidas en la ciudad**

Por su parte, el secretario de Producción y Desarrollo Económico, Matías Schmüth, reiteró que “en esta etapa es importante resguardar las actividades productivas y, en ese sentido, desde la Municipalidad de Santa Fe venimos trabajando junto a los distintos sectores en mesas que armamos desde principios de la gestión y las seguimos sosteniendo”. Por eso,  explicó que antes de tomar definiciones “tuvimos reuniones con distintos sectores, como ayer con el sector comercial”.

Luego, el funcionario repasó punto por punto cuáles son las nuevas restricciones que se aplicarán desde el viernes:

**Circulación:**

* Según indica el decreto de la provincia, no se podrá circular en vehículos particulares entre las 21 y las 6 horas. Las únicas personas exceptuadas son aquellas que realizan trabajos esenciales como los médicos y enfermeros, por ejemplo.
* Entre las 21 y la medianoche, la gente que quiera circular deberá hacerlo a pie o en el transporte público.

 

**Comercios:**

De acuerdo a lo previsto por el gobierno provincial, se cumplirá con la restricción horaria que busca disminuir la circulación y evitar la saturación del sistema de transporte en el horario de ingreso escolar. Por eso:

* Los comercios podrán funcionar entre las 9 y las 19 horas.
* En el caso del macrocentro (zona comprendida entre Rivadavia, Urquiza, General López y Suipacha) deberá ser sí o sí en horario corrido.
* En el resto de la ciudad podrán abrir de 9 a 19 pero con la opción de cerrar algunas horas a la siesta. “En las avenidas no se genera mucha circulación porque los consumidores y trabajadores de estos comercios son de los barrios aledaños a las avenidas comerciales”, dijo Schmüth.
* Los supermercados y comercios que vendan alimentos podrán funcionar hasta las 20 horas. “Es muy importante el aval del gremio de empleados de comercio que está dispuesto a acompañar estas medidas, porque esto reduce la circulación de los empleados que en vez de cuatro, harán dos viajes diarios”, añadió.

 

**Gastronómicos, heladerías, y casa de comida:**

* En función de la normativa provincial, el ingreso de clientes será hasta las 23 horas.
* Deberán cerrar a la medianoche.
* El aforo se mantiene en el 30% si es un lugar cerrado y puede estar a la capacidad máxima si es al aire libre.

 

**Actividades prohibidas**

En el marco de las definiciones provinciales, en Santa Fe estarán prohibidas las siguientes actividades:

* las competencias deportivas
* los cines
* las ferias (excepto que sean de productos alimenticios)
* todo tipo de evento cultural
* las actividades hípicas
* En el shopping podrán funcionar los comercios minoristas y los locales gastronómicos con ingreso exterior independiente. Todo lo demás estará cerrado.