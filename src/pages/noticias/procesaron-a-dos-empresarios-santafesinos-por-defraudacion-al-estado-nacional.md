---
category: Agenda Ciudadana
date: 2020-12-15T12:50:53Z
thumbnail: https://assets.3dnoticias.com.ar/nation.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Diario UNO'
resumen: Procesaron a dos empresarios santafesinos por defraudación al Estado nacional
title: Procesaron a dos empresarios santafesinos por defraudación al Estado nacional
entradilla: Se trata de Domingo Marcos Valentino y Jorge Alberto Minella, quienes
  están acusados de ocupar y usufructuar de manera ilegítima un predio estatal ubicado
  en calle Las Heras entre Eva Perón y Avenida Leandro N. Alem.

---
El titular del Juzgado Federal n°1 de la ciudad de Santa Fe, Reinaldo Rodríguez, procesó al ex director del Organismo Nacional de Administración de Bienes (ONABE) -actual Agencia de Administración de Bienes del Estado (AABE)-, Fernando Miguel Suárez, y a los empresarios Jorge Alberto Minella y Domingo Marcos Valentino (este último titular de la firma Nation S.A.), respectivamente, por la administración fraudulenta de inmuebles de propiedad del Estado nacional que generó un perjuicio económico que, en principio, rondaría los 47,5 millones de pesos, al no haberse actualizado el monto del canon mensual que debía ser abonado como contraprestación al uso del predio cedido a aquellas personas.

El monto fue calculado por el titular de la Fiscalía Federal N°1 de esa ciudad, Jorge Onel, en su escrito de denuncia y en el que dio origen al incidente de medidas cautelares. 

La instrucción fue realizada por el representante del MPF, en función de la delegación de la investigación dispuesta por el juez. El caso se había iniciado con una investigación preliminar de la fiscalía a partir de haber tomado intervención en la demanda de desalojo iniciada por el AABE respecto del concesionario Minella en 2019.

En la pesquisa, Onel pudo determinar que desde fines de 2001, fecha de vencimiento de los contratos de concesión, los empresarios habrían ocupado y usufructuado de manera ilegítima un predio ubicado en calle Las Heras entre Eva Perón y Avenida Leandro N. Alem que había pertenecido a Ferrocarriles Argentinos (ex estación Talleres Santa Fe Ramal Fe).

Por la utilización de los inmuebles abonaron mensualmente, sin fundamento contractual aparente, sumas pactadas de 450 pesos, en el caso de Minella, y de 500, en el de la firma Nation.

En la resolución que hizo lugar a los procesamientos solicitados por la fiscalía, el juez Rodríguez precisó que los elementos recabados en la investigación permitieron concluir respecto a los terrenos ocupados por Minella y por la firma Nation que desde el vencimiento del contrato de concesión en el año 2001, tanto Minella como Valentino siguieron utilizando en forma ilegal el inmueble, por encontrarse vencidas las condiciones por las que se habían originado las respectivas concesiones. Precisó que ambas firmas realizaron en el lugar actividades lucrativas, ajenas a cualquier fin o interés público o colectivo, condición necesaria para usufructuar esos bienes del Estado.

Los empresarios habrían ocupado y usufructuado de manera ilegítima un predio ubicado en la avenida General Las Heras entre Eva Perón y Avenida Leandro N. Alem que había pertenecido a Ferrocarriles Argentinos (ex estación Talleres Santa Fe Ramal Fe).

Los empresarios habrían ocupado y usufructuado de manera ilegítima un predio ubicado en la avenida General Las Heras entre Eva Perón y Avenida Leandro N. Alem que había pertenecido a Ferrocarriles Argentinos (ex estación Talleres Santa Fe Ramal Fe).

Al respecto, el juez destacó que los organismos encargados de administrar los bienes públicos deben procurar que su utilización tenga una “finalidad de utilidad pública específica”, y que en este caso los terrenos ocupados hasta 2018 por Minella y la firma Nation forman parte de emprendimientos privados inicialmente fijados con un término perentorio de dos años, no obstante lo cual hasta la fecha la ocupación se mantiene.

El juez puso de relieve que por la utilización de los inmuebles abonaron mensualmente, sin fundamento contractual aparente, sumas pactadas de 450 pesos, en el caso de Minella y de 500 en el de la firma Nation, lo que “deviene en irrisorio si se tiene en cuenta la evolución inflacionaria y cambiara que sufrió este país en los últimos veinte años y el tamaño y la ubicación de los terrenos en cuestión”. 

En efecto, la resolución tiene en cuenta que la firma Nation celebró un convenio con el Estado en junio de 2018 a través del cual se actualizó el valor de retribución mensual por el uso del inmueble por 114 mil pesos mensuales, con efectos a partir de finales de 2015.

# **Administración fraudulenta**

El juez procesó a Suárez como autor del delito de defraudación en perjuicio de la administración pública bajo la modalidad de administración fraudulenta de los inmuebles en cuestión, entre el 4 de junio de 2003 y el 18 de febrero de 2009, periodo en el cual el imputado se desempeñó como director de la ex ONABE (Suárez fue titular de ese organismo hasta 2012, pero en 2009 los bienes en cuestión pasaron a la órbita de la Administración de Infraestructura Ferroviaria Sociedad del Estado, ADIF por sus siglas).

El juez consideró que es responsable del hecho en razón de las designaciones otorgadas por la autoridad competente por el cual le correspondió un deber de vigilancia y protección de los bienes, por lo que entendió pertinente la atribución de responsabilidad a título de dolo eventual ya que “debió comprender que había un elevado índice de probabilidad de que en caso de no verificar el estado de cumplimiento de las condiciones y la vigencia de las concesiones otorgadas a los distintos actores que contratan con el Estado, en este caso para ocupar sus terrenos, ocasionaría dicho resultado” y que “la más elemental actuación de control que se hubiese realizado sobre tal orden de cosas, hubieran permitido detectar la situación irregular, no solo en cuanto al tiempo de ocupación de los terrenos encontrándose vencidas las cláusulas contractuales fijadas en 1999, sino también lo desactualizado que estaban los montos que, mensualmente y en forma ininterrumpida, abonaban las concesionarias”.

# **El perjuicio al erario público calculado para el periodo 2003-2009 supera los 12 millones de pesos**

El magistrado explicó en su resolución que “el fin de lucro indebido que habrían procurado los agentes estatales para con los empresarios, se configuró al tener una actitud indiferente con respecto al perjuicio eventual que se le causó al Estado Nacional por el dinero que dejó de percibir en razón de la no actualización de los montos cuya retribución habilitaría la utilización de los terrenos por parte de terceros, o directamente por no haber reclamado la restitución de tales bienes una vez finalizados sendos contratos de concesión”.

En ese sentido, Rodríguez destacó que, de forma provisoria y hasta que concluyan los peritajes contables dispuestos, “el monto del perjuicio ocasionado al erario público adjudicable a Fernando Miguel Suarez asciende a un total por el uso de los dos terrenos que nos ocupan, de doce millones doscientos ochenta y cinco mil pesos ($12.285.000) aproximadamente”.

Por otro lado, el juez consideró que Minella y Valentino actuaron como partícipes necesarios del delito de defraudación en perjuicio de la administración pública, bajo la modalidad de administración fraudulenta. 

Según la resolución, ambos empresarios “han sido beneficiarios de la administración fraudulenta realizada por Miguel Suarez, ya que se les permitió que siguiesen ocupando los predios en cuestión abonando por más de quince años el mismo monto fijado en los contratos de concesión, cuya vigencia venció en el año 2001. Sin este aporte, el delito no habría podido ser perpetrado de parte del imputado Suarez”.

Tras haber calculado de forma preliminar el daño en 12.285.000 pesos, dispuso el embargo de Suárez en 6.100.000 pesos, el de Valentino en 3.250.000 pesos y el de Minella en 2.925.000 pesos.

El juez puso de relieve que en el caso de la firma Nation (Valentino) “se mantuvo la ocupación del terreno a cambio del pago de quinientos pesos mensuales desde el vencimiento del contrato en el año 2001 hasta fines del año 2015”, fecha a partir del cual tuvo efectos el convenio de desocupación, mientras que en el caso de Vitacar (Minella) se mantuvo el orden de cosas desde el 2001 hasta el año 2018, cuando se produjo la desocupación del bien, con la particularidad de que en este último caso, el concesionario Minella cedió parcialmente el bien a otro emprendimiento privado.

Sin embargo, el juez decidió que a los dos empresarios les imputaría la conducta durante el período en el que Suárez cumplió su mandato a cargo del ONABE, esto es, entre el 4 de junio de 2003 y el 18 de febrero de 2009, porque “en modo alguno se les puede endilgar la participación en la comisión de un delito por más tiempo que el que se le achaca a quién fue acusado en calidad de autor”.

Por tal motivo, y tras haber calculado de forma preliminar el daño en 12.285.000 pesos, dispuso el embargo de Suárez en 6.100.000 pesos, el de Valentino en 3.250.000 pesos y el de Minella en 2.925.000 pesos.

El juez dictó asimismo -de conformidad con la opinión precedente de la fiscalía- la incapacidad sobreviniente -y la consecuente suspensión del trámite de la causa- del ex funcionario a cargo de los bienes del Estado que sucedió a Suárez, Antonio Alberto Vulcano. Este último se desempeñó como presidente del AABE, a partir de su creación en agosto de 2012. Este organismo, desde su creación, tiene a su cargo la administración de los bienes objeto de conflicto.

# **Apelación del fiscal**

Ante la reducción del período de responsabilidad de los empresarios Valentino y Minella y la consecuente modificación en el monto de los embargos, la fiscalía interpuso hoy recurso de apelación exclusivamente sobre esos aspectos de la resolución.

El fiscal Onel señaló que el procesamiento de Valentino debe abarcar el período comprendido entre mediados de 2003 y diciembre de 2015, mientras que respecto de Minella debe circunscribirse al lapso que va entre mediados de 2003 y finales de 2018.

“Los imputados en trato se han beneficiado por el lapso que la fiscalía les atribuye. Es por tal razón que la responsabilidad penal del partícipe no depende de la del autor de manera absoluta”, consideró el fiscal, teniendo en consideración los principios de accesoriedad de la participación e individualización personal de la culpabilidad contenidos en el artículo 48 del Código Penal (accesoriedad limitada), de acuerdo a los cuales existe participación en tanto y en cuanto se tenga por configurado el injusto penal, con prescidencia de la imputación a los autores. 

Argumentó que en el caso se produjo “una participación necesaria en la comisión de los hechos descriptos por los períodos señalados, en tanto y en cuanto sin el aporte de ambos el hecho no podría haberse cometido, toda vez que aparecieron como los principales beneficiarios de la maniobra por el período señalado en el presente, al llevar adelante en los inmuebles en cuestión, actividades empresariales lucrativas de gran envergadura, como fue de público conocimiento”.

En tal sentido, el representante del MPF solicitó la modificación de los montos de embargo, como “lógica e ineludible consecuencia” de una resolución que dé cauce al recurso de apelación. En tal sentido, indicó que deben fijarse los montos originariamente impuestos en el incidente de medidas cautelares.

Según el fiscal, debe tomarse como base el valor real de concesión de los bienes involucrados al año 2018, al menos aquel valor que uno de los concesionarios (Nation), convino como cánon mensual con el Estado Nacional en el convenio de desocupación suscripto con efectos retroactivos a abril de 2016. Según ese convenio, se pactó por un año y medio un canon de concesión mensual de 114 mil pesos, y un canon de compensación de 2,5 millones de pesos (desde abril de 2016 a junio de 2018), lo que equivaldría a 1.666.666,67 pesos anual. 

Ello determina como hipótesis de mínima, precisó Onel, un valor actualizado (desde 2003 a 2015), de 20 millones de pesos totales por doce años ($1.666.666,67 multiplicado por quince) por cada uno de los lotes, aproximadamente, monto al que debería restarse el canon de 500 y 400 pesos mensuales efectivamente abonados desde el vencimiento del contrato hasta 2016 por Nation y hasta 2018 por Minella, respectivamente.