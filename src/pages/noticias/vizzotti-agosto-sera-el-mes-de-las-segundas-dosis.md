---
category: Agenda Ciudadana
date: 2021-07-28T06:15:42-03:00
thumbnail: https://assets.3dnoticias.com.ar/VIZZOTTIAGOSTO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: 'Vizzotti: "Agosto será el mes de las segundas dosis"'
title: 'Vizzotti: "Agosto será el mes de las segundas dosis"'
entradilla: La ministra de Salud adelantó que se fijó el objetivo de alcanzar al 60
  por ciento de la población mayor de 50 años vacunada con las dos dosis en el transcurso
  del próximo mes.

---
La ministra de Salud, Carla Vizzotti, anunció que agosto "será el mes de las segundas dosis" en el que se completarán los esquemas de vacunación de gran parte de la población contra el coronavirus.

La funcionaria dijo también que en la reunión del Consejo Federal de Salud (Cofesa) se fijó el objetivo de alcanzar al 60 por ciento de la población mayor de 50 años vacunada con las dos dosis en el transcurso del próximo mes.

**La reunión**

El Cofesa se reunió para definir, además, el inicio de la vacunación contra el coronavirus a adolescentes de entre 12 y 17 años con comorbilidades con dosis del laboratorio estadounidense Moderna, en el marco del Plan Estratégico de Vacunación contra la Covid-19.

La reunión, de la que participaron de manera virtual los ministros de Salud de todas las jurisdicciones del país, comenzó a las 9 en el Palacio San Martín de la Cancillería.

Al finalizar, se informó que "las ministras y ministros de Salud acordaron priorizar la aplicación de segundas dosis" contra el Covid-19 durante agosto.

"El objetivo es que en el transcurso del próximo mes el 60 por ciento de quienes tienen 50 años y más cuenten con las dos dosis", se precisó en un comunicado.

En el encuentro, la ministra expresó que "consensuamos que agosto será el mes de las segundas dosis para tener un impacto en la disminución de la mortalidad y en las internaciones ante el potencial riego de presentar circulación persistente de la variante Delta".

Vizzotti agregó que "si dedicamos las cuatro semanas de agosto para completar esquemas, es muy posible que lleguemos a cubrir el 22 por ciento de los mayores de 50 que resta para alcanzar el objetivo, ya que a la fecha hemos alcanzado el 37% de coberturas con esquemas completos en este grupo, comenzando con los que más tiempo de intervalo tienen".

En la misma dirección, el subsecretario de Estrategias Sanitarias, Juan Manuel Castelli, informó que hasta este martes el 73,8% de las personas de 18 años y más ya tienen una dosis de vacuna contra el virus SARS-CoV-2 al igual que el 90% de las personas con 60 o más años.

Además, se agregó, completaron su esquema el 62,1% de los mayores de 70 años, el 50,4% de las personas con 60 y más años y el 37,4% de los que tienen 50 y más años.

**Más vacunas para el país**

En este sentido, se informó que tanto las vacunas de AstraZeneca como los 8 millones de dosis Sinopharm que llegarán en agosto estarán destinadas a ese fin.

"Todas las vacunas que lleguen de AstraZeneca se van a destinar a segundas dosis -afirmó Vizzotti-, y se va a priorizar la vacunación con segundas dosis de AstraZeneca".

En relación con el segundo componente de Sputnik V, se abordará en tres ejes: la recepción de dosis de la Federación Rusa, el fortalecimiento de la producción local del laboratorio Richmond, así como también la definición de la estrategia de intercambiabilidad de plataformas para lograr este objetivo.

Esta relación a este último punto en particular será discutida cuando se cuente con más información específica el próximo lunes, en un encuentro virtual del Cofesa.

En este sentido, la ministra destacó "el trabajo que se está haciendo para la producción local con ya 143 mil dosis formuladas para tener el control de calidad el 1 de agosto, 880 mil dosis el 9 de agosto y también esperando la sustancia activa para poder producir en Argentina más estudios".

Y puntualizó que se espera recibir "más datos e información sobre la posibilidad de generar intercambiabilidad y completar los esquemas de vacunación de quienes recibieron la primera dosis de vacunas Sputnik V con alguna otra vacuna de otra plataforma".

Para avanzar en las investigaciones sobre intercambiabilidad ayer el gobierno anunció que incluirá a la vacuna de Moderna en el estudio "colaborativo y federal de evaluación de esquemas combinados de vacunación" contra el coronavirus que coordina junto a las jurisdicciones participantes e investigadores del Conicet.

El objetivo del estudio, ya en marcha, es contar con evidencia propia para la toma de decisiones a partir del análisis de inmunogenicidad y seguridad de la utilización de diferentes dosis en un mismo esquema de las vacunas disponibles al momento en el país, que son AstraZeneca, Moderna, Sinopharm y Sputnik-V.

En otro orden, Vizzotti destacó en la conferencia de prensa que durante la reunión del Consejo Federal de Salud se reconoció el logro de que la Argentina está demorando la circulación predominante de la variante Delta.

Aseguró que "no tenemos circulación predominante de la variante Delta, tenemos ya 60 aislamientos de viajeros y contactos estrechos convivientes y no convivientes, pero de ninguna manera es predominante" ya que en el país "tenemos circulación de Manaos, del Reino Unido y variante andina".

Sobre este tema la titular de la Dirección Nacional de Migraciones, Florencia Carignano remarcó hoy que el cupo de pasajeros "ayudó muchísimo a contener el ingreso de la variante Delta" a la Argentina ya que "ha servido mucho que los argentinos ingresen en cuotas" y destacó que solo estamos "en un 10 por ciento el incumplimiento de la cuarentena “de 7 días que deben hacer quienes arriban del exterior.

Para finalizar, Vizzotti mencionó que el Consejo Federal de Salud también se encargará de abordar otros ejes como el seguimiento de los pacientes post Covid, trabajos de municipios saludables y médicos comunitarios, entre otras cuestiones.