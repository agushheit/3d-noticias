---
category: La Ciudad
date: 2021-12-31T06:00:11-03:00
thumbnail: https://assets.3dnoticias.com.ar/BOMBEROS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'Incendios: los Bomberos luchan contra vientos "calientes" de hasta 27km/h
  en Santa Fe y la región'
title: 'Incendios: los Bomberos luchan contra vientos "calientes" de hasta 27km/h
  en Santa Fe y la región'
entradilla: "Hay siete focos ígneos simultáneos en la ciudad capital, Santo Tomé,
  Arroyo Leyes, y Cayastá.\n\n"

---
Con vientos del sur de hasta 27 km/h según indica este jueves por la tarde en Santa Fe y el área metropolitana weather.com, sumado al calor, la capital provincial se ha convertido en un "infierno", dijeron los ciudadanos . 

 Además, el viento propaga el humo generado por las quemas que se registran en el área y complica la labor de las autoridades que continúan al frente de las tareas de combate de las llamas.

 "En estos momentos, estamos trabajando en la zona de Bv. French, en el norte de la ciudad de Santa Fe; tenemos tres incendios en distintos puntos de la ciudad de Santo Tomé, otro en la Calle 98 de Arroyo Leyes y estamos terminando de sofocar un incendio en Ruta 168 a la altura del Club Velocidad y Resistencia", detalló a este medio el Jefe de Bomberos de Santa Fe, Crio. Claudio Arias. 

  "Obviamente que este viento complica, no solamente acá sino también en los alrededores porque en la zona de Cayastá se propagó otro incendio y hay tres dotaciones de Bomberos Voluntarios trabajando en el lugar", añadió Arias. 

 