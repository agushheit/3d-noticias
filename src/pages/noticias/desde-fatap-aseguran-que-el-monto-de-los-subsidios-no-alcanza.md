---
category: Agenda Ciudadana
date: 2022-06-28T07:42:47-03:00
thumbnail: https://assets.3dnoticias.com.ar/colectivojpeg.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: LT 10 Radio Universidad
resumen: Desde Fatap aseguran que el monto de los subsidios no alcanza
title: Desde Fatap aseguran que el monto de los subsidios no alcanza
entradilla: Según indican, la cifra fijada para empresas del interior es insuficiente
  ya que solo cubre el 50% del pago de sueldos y aguinaldo.

---
El recorte de subsidios al transporte y el reparto de los fondos hasta octubre que se oficializó este lunes en el Boletín, sorprendió a los empresarios del sector que también se mostraron preocupados porque los empuja ahora a decidir si pagan el suelo o el aguinaldo ya que aseguran que para las dos cosas no alcanza.

Así lo manifestó por **LT10** Gerardo Ingaramo titular de la Federación Argentina de Transportadores por Automotor de Pasajeros (Fatap). **“Para julio pedimos 6.500 millones, en caso contrario no podemos hacer frente a sueldos y aguinaldo”, remarcó** en disconformidad con la cifra que destinará el gobierno nacional que es 3.850 millones, según la resolución de este lunes. "Las cuentas no dan", insistió.

Los empresarios tampoco confían en que el dinero se envíe en tiempo y forma ya que, según advirtió el propio Ingaramo, hay localidades pendientes del cobro de los subsidios nacionales de junio.

**“Es todo una falacia”, advirtió sobre la resolución de este lunes que detalló cómo será el reparto de 16 mil millones de pesos al Fondo Compensador.**

Ingaramo agregó que tienen expectativas en que avance el reajuste a 59.500 mil millones de pesos que dictó la semana pasada la comisión de Transporte de la Cámara de Diputados y que ahora deberá ser estudiada en la comisión de Presupuesto. Sin embargo, **aseguró que la clave está en trabajar sobre una verdadera ley federal que equilibre los subsidios.**