---
category: Agenda Ciudadana
date: 2021-02-06T07:29:45-03:00
thumbnail: https://assets.3dnoticias.com.ar/inflación.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Infobae
resumen: 'Inflación: será del 3,9% en enero y del 50% en 2021, según analistas económicos'
title: La inflación será del 3,9% en enero y del 50% en 2021
entradilla: Según el Relevamiento de Expectativas de Mercado (REM) que publicó el
  BCRA, el incremento del Índice de Precios al Consumidor en 2021 se ubicará muy por
  encima del 29% estipulado por el gobierno en la ley de Presupuesto.

---
**La inflación será del 3,9% en enero y del 50% para todo 2021, según los analistas económicos consultados por el Banco Central en el Relevamiento de Expectativas de Mercado (REM) correspondiente al primer mes del año.** 

El informe no mostró grandes diferencias con respecto a la edición del mes anterior en ninguno de los indicadores, pero confirmó la visión de los consultores en cuanto a que el incremento del Indice de Precios al Consumidor en 2021 se ubicará muy por encima del 29% estipulado por el gobierno en la ley de Presupuesto.

“Los analistas del mercado proyectaron que la inflación minorista para diciembre de 2021 se ubicará en 50,0% interanual, aumentando en 0,2 p.p. los pronósticos provistos a fines del mes anterior (49,8% i.a.)” apuntó el informe.

“Para diciembre de 2020 la mediana de las estimaciones de quienes participaron en la encuesta previa del REM sugería una inflación de 4,0% mensual, coincidiendo con el dato observado en dicho mes. **Para el mes de enero de 2021 la mediana de las estimaciones se ubicó en 3,9%**”, agregó.

***

![](https://assets.3dnoticias.com.ar/grafico-expectativas-inflacion.webp)

***

Según los consultores, luego del 3,9% de enero **la inflación evolucionará en forma levemente descendente hasta ubicarse en el 3,2% en junio, con un promedio del 3,5% para el primer semestre del año**. “Con respecto a la última encuesta las estimaciones para los próximos meses en su mayoría fueron ajustadas levemente a la baja o se mantuvieron sin cambios. **Se proyecta para enero una variación de 3,9% mensual (-0,1 p.p.), en febrero de 3,6% (-0,1 p.p.), en marzo 3,9% (sin cambios respecto de la encuesta previa), 3,5% en abril (sin cambios), 3,4% en mayo (sin cambios) y en junio 3,2% (sin cambios)**”, detalló el BCRA.

Un sendero similar aunque con valores levemente superiores se registró en las proyecciones de la **inflación núcleo**, aquella que no contempla los precios estacionales y los regulados, como las tarifas de los servicios públicos. En ese caso, **las proyecciones de los consultores se ubican en una escala que parte del 4,2% en enero y desciende hasta un 3,4% en julio.**

***

![](https://assets.3dnoticias.com.ar/grafico2-expectativas-inflacion.webp)

***

Para diciembre de 2021, los consultores proyectaron que la inflación alcanzará 50,0%, levemente por encima del 49,8% de diciembre. En las proyecciones para los dos años siguientes, las variaciones con respecto al mes anterior fueron mínimas. Para 2022, la previsión se situó en el 39,0%, solo 0,2 p.p. por debajo del REM previo, y en el 30,2% para 2023, apenas 0,1 p.p. por encima de la encuesta anterior.

Con respecto al dólar, **el REM ubica el tipo de cambio mayorista para diciembre de 2021 en $125,00 por dólar, mientras que para fines de 2022 espera un valor de $172,00 por dólar.** Esos datos reflejan una suba interanual del 51,3% en el primer caso y del 37,6% i.a. para el año próximo. “Respecto de la encuesta previa, se evidenciaron correcciones a la baja en la mayoría de los períodos pronosticados, a excepción de los guarismos correspondientes a los próximos 12 meses y fines de 2022 que aumentaron”, puntualizó el informe.

***

![](https://assets.3dnoticias.com.ar/grafico3-expectativas-inflacion.webp)

***

Los analistas económicos que participan del relevamiento del Banco Central no variaron sus proyecciones del mes anterior en materia de crecimiento del Producto Bruto Interno (PBI). **Se espera un rebote de la economía del 5,5% en 2021 y de 2,5% tanto para 2022 como para 2023, exactamente los mismos pronósticos que en la edición del REM de diciembre**.

***

![](https://assets.3dnoticias.com.ar/grafico4-expectativas-inflacion.webp)

***

Otro de los indicadores analizados por el REM es la tasa Badlar, utilizada para los plazos fijos a 30 días en bancos privados superiores a $1 millón, en el que los analistas esperan subas moderadas. Los consultores esperan una tasa para febrero del 34,94%, que sería 82 puntos básicos mayor que el promedio registrado durante enero (34,12%). “Para los próximos meses se prevé un sendero mensual creciente, alcanzando un nivel de 36,72% en el mes de julio de 2021, para luego ascender a 38,00% en diciembre de 2021″, añadió el informe.

La edición de enero del REM fue respondida entre los días 27 y 29 de enero por 41 entidades participantes, entre quienes se cuentan 27 consultoras y centros de investigación locales, 13 entidades financieras de Argentina y 1 analista extranjero.