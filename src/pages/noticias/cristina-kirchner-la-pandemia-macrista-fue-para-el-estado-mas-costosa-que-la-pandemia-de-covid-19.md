---
category: Agenda Ciudadana
date: 2022-01-19T06:00:35-03:00
thumbnail: https://assets.3dnoticias.com.ar/criwstina.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: 'Cristina Kirchner: "La pandemia macrista fue para el Estado más costosa
  que la pandemia de Covid-19"'
title: 'Cristina Kirchner: "La pandemia macrista fue para el Estado más costosa que
  la pandemia de Covid-19"'
entradilla: "A través de una carta publicada en sus redes sociales, la vicepresidenta
  comparó los gastos que debe afrontar el Estado para pagar al FMI con los que requiere
  la atención de la situación sanitaria. \n"

---
La vicepresidenta Cristina Kirchner volvió a hablar hoy de "pandemia macrista" en referencia al acuerdo que el ex presidente Mauricio Macri firmó con el Fondo Monetario Internacional (FMI) y aseguró que "fue para el Estado Nacional incluso más costosa que la pandemia Covid-19".

"Dicen no pocos científicos, que la pandemia del Covid-19 estaría llegando a su fin. Ojalá! Igualmente sigamos cuidándonos. Lo que nosotros sí sabemos es que en Argentina lo que nunca se va acabar es lo que nos pasó -y nos pasa- por la pandemia macrista, cuando en el año 2018 Macri trajo al FMI de vuelta a la Argentina", señaló la ex mandataria.

En una carta abierta publicada en sus redes sociales, Cristina Kirchner apuntó a los gastos del Estado para afrontar los vencimientos de deuda con el FMI y los comparó con el costo que supuso la pandemia de Covid-19, tras lo cual indicó que la "pandemia macrista" representa el "1,1% del PBI" mientras que la del coronavirus implica al "0,9% del PBI".

La vicepresidenta se refirió primero al acuerdo entre la gestión de Macri y el FMI al señalar que "le dieron un préstamo excepcional de 57.000 millones de dólares para salvarle el gobierno y ayudarlo a ganar las elecciones".

Y con ironía, agregó: "No sólo no ganó las elecciones, sino que además no se sabe donde están esos dólares. ¿Alguien los vio? En todo caso, por favor llamen al 911".

La ex mandataria aclaró que sus dichos no pretenden "burlarse de la tragedia de la pandemia" y puntualizó: "Al contrario. Pero por favor, mirá lo que tuvo que pagar Argentina, tu país, sólo en el año 2021".

Seguidamente, bajo el subtítulo "La pandemia macrista", Cristina Kirchner señaló en su carta que en 2021 el país "pagó -entre capital e intereses- 5.160 millones de dólares al FMI por los vencimientos de ese año correspondientes al préstamo" otorgado en 2018 y remarcó: "Ese monto representa, nada más ni nada menos que el 1,1% del PBI... ¡Pero en dólares!".

Y debajo del subtítulo "La pandemia COVID-19", indicó que en el mismo año la Argentina "pagó 420.000 millones de pesos que fueron destinados estrictamente a las medidas tendientes a mitigar los efectos de la pandemia".

Según detalló, "allí están incluidas las vacunas COVID ($124.000 M), REPRO II ($90.000 M), Asistencia al turismo – Previaje ($31.000 M) y reducción de contribuciones patronales a empleadores de REPRO II y Sector Salud ($51.000 M), además de la Ampliación de Políticas Sociales", tras lo cual destacó: "Ese monto representa el 0,9% del PBI... ¡Pero en pesos!".

"De esta manera se puede advertir con mucha facilidad que, en el año 2021, la pandemia macrista fue para el Estado Nacional incluso más costosa que la pandemia COVID-19", concluyó la vicepresidenta.

Por último, agregó: "Y con una pequeña yapa: la pandemia macrista nos quita las divisas que tanto necesitamos como país porque al FMI hay que pagarle completa y exclusivamente en dólares... porque por más que le insistimos no nos acepta pesos".

La vicepresidenta renovó así sus críticas a la gestión de Macri y de la coalición Juntos por el Cambio, en medio de los cuestionamientos de la oposición al Gobierno por la negociación con el FMI.

Cristina Kirchner publicó su carta horas después de que la principal coalición opositora le reclamara al presidente Alberto Fernández "un plan económico" que contribuya a alcanzar el buscado acuerdo con el organismo de crédito internacional