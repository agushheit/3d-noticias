---
category: La Ciudad
date: 2020-12-23T10:45:11Z
thumbnail: https://assets.3dnoticias.com.ar/2312bares1.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: El 24 y el 31 se amplía la cantidad de clientes por mesa en bares y restaurantes
title: El 24 y el 31 se amplía la cantidad de clientes por mesa en bares y restaurantes
entradilla: Se extiende a 10 el número de comensales por mesa y se permitirá únicamente
  durante esas dos fechas. El horario de apertura será hasta la 1:30 horas.

---
La Municipalidad comunica que durante el **24 y 31** de diciembre -y únicamente esos días-, **se ampliará hasta 10 la cantidad de comensales reunidos por cada mesa, en bares y restaurantes**. Cabe recordar que actualmente, solo se pueden reunir hasta cuatro personas por mesa.

Asimismo, se autorizará para esos mismos días -24 y 31- el horario de apertura hasta la 1:30 horas, que actualmente rigen para los días viernes y sábados. También **se ampliará al 50% la ocupación en espacios cerrados de los locales gastronómicos** (es de 30%).

El intendente Emilio Jatón indicó que «llegamos a esta decisión luego de consensuarla con el sector gastronómico, con el cual venimos haciendo el seguimiento del impacto de la emergencia por el Covid en la ciudad y el cumplimiento de los protocolos vigentes. También tuvimos en cuenta la definición provincial que amplió las reuniones familiares a 15 personas».

Se recuerda que proseguirán vigentes los protocolos sanitarios en el marco de la pandemia y que lo dispuesto por la Municipalidad solo tendrá vigencia durante los dos días mencionados.