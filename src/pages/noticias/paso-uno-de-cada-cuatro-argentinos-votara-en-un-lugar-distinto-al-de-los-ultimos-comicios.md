---
category: Agenda Ciudadana
date: 2021-09-01T06:15:21-03:00
thumbnail: https://assets.3dnoticias.com.ar/PADRONES.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: 'PASO: uno de cada cuatro argentinos votará en un lugar distinto al de los
  últimos comicios'
title: 'PASO: uno de cada cuatro argentinos votará en un lugar distinto al de los
  últimos comicios'
entradilla: Los cambios en el padrón fueron realizados en virtud del protocolo que
  se utilizará tanto en las PASO del 12 de setiembre como en las elecciones legislativas
  del 14 de noviembre.

---
La Dirección Nacional Electoral (DINE) recomendó verificar el lugar de votación en el padrón, de cara a las PASO del 12 de setiembre próximo, ya que uno de cada cuatro argentinos votará en un establecimiento diferente al de los últimos comicios.

Los cambios en el padrón fueron realizados en virtud del protocolo que se utilizará tanto en las PASO del 12 de setiembre como en las elecciones legislativas del 14 de noviembre, debido a la situación epidemiológica por el coronavirus y ante la necesidad de evitar aglomeraciones en los establecimientos de sufragio.

Así lo informó la titular de la Dirección Nacional Electoral (DINE), Diana Quiodo, en un comunicado del ministerio del Interior,

Por ello, aconsejó ingresar a las páginas web www.padron.gob.ar o www.argentina.gob.ar/elecciones para chequear el lugar de votación.

“Es muy importante verificar los lugares de votación, ya que estamos muy acostumbrados a asistir adonde siempre nos tocó votar, y creemos que esta situación no sufrió modificaciones, pero en este contexto tan especial de pandemia se generaron reubicaciones”, explicó la funcionaria.

En ese plano, señaló que la Justicia Electoral “ha puesto un tope de ocho mesas por establecimiento, por lo que muchas mesas serán relocalizadas para cumplir con esta normativa y evitar aglomeraciones durante el acto electoral”.

Quiodo recordó además que, debido a la situación sanitaria, para estas elecciones se implementará la figura del facilitador sanitario, que será un integrante del Comando General Electoral, y por lo tanto un miembro de las Fuerzas Armadas o de las fuerzas de seguridad nacionales.

“Esta figura -según detalló- será la encargada de ordenar el flujo de gente ingresando y saliendo de las escuelas, dado que esta vez las filas se realizarán fuera de los establecimientos de votación, y por ello desde la DINE insistimos en que las personas verifiquen dónde votan”.

“La idea es evitar la confluencia de gente que llega, no sabe cuál es su mesa y se pone a revisar los padrones pegados localizados afuera de los establecimientos. Para evitar esto, solicitamos un chequeo previo”

Quiodo insistió en que “el chequeo del lugar de sufragio es fundamental, y también que cada votante averigüe y lleve anotado de antemano el número de mesa y de orden, de modo de permitir que el facilitador los guíe rápidamente al llegar al establecimiento”.

“La idea es evitar la confluencia de gente que llega, no sabe cuál es su mesa y se pone a revisar los padrones pegados localizados afuera de los establecimientos. Para evitar esto, solicitamos un chequeo previo”, afirmó la directora de la DINE.

En agosto, la Cámara Nacional Electoral (CNE) impulsó la Acordada Extraordinaria N° 83/21, en la que se estableció la reglamentación y los protocolos sanitarios que regirán las elecciones legislativas de este año, en el marco de la pandemia de la Covid-19.

Entre otras medidas, estableció un incremento en la cantidad de centros de votación para evitar aglomeraciones.

Asimismo, en otros de los puntos se pone el énfasis en que haya alrededor de ocho mesas de votación por establecimiento, lo que hará que se pase de los 14.800 lugares de votación habilitados durante la elección presidencial de 2019 a alrededor de 17.000 en todo el país.