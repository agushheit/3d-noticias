---
category: Agenda Ciudadana
date: 2021-10-05T06:00:39-03:00
thumbnail: https://assets.3dnoticias.com.ar/BILLETERAPROMOS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Billetera Santa Fe: oficializaron beneficios para el Día de la Madre'
title: 'Billetera Santa Fe: oficializaron beneficios para el Día de la Madre'
entradilla: Para la campaña del Día de la Madre, los rubros indumentaria, calzado,
  regalería, perfumería y marroquinería tendrán reintegros desde el 11 al 17 de octubre.

---
Este lunes, el gobierno de la Provincia anunció el inicio de la cuarta etapa del programa Billetera Santa Fe y sus beneficios para el Día de la Madre. Lo hizo mediante un acto público realizado en la ciudad de Rosario, que contó con la participación del secretario de Comercio Interior y Servicios, Juan Marcos Aviano, su par de Turismo, Alejandro Grandinetti, y el presidente de la Asociación Empresaria de Rosario (AER), Ricardo Diab, entre otros representantes de entidades comerciales y de la empresa PlusPagos.

En esta oportunidad, se oficializó la continuidad del mismo esquema de rubros, días y topes de reintegro, con la particularidad de que los rubros indumentaria, calzado, regalería, perfumería y marroquinería mantendrán vigentes sus beneficio del 30% de reintegro durante toda la semana previa al Día de la Madre, comenzando el 11 de octubre y extendiéndose hasta el día 17 inclusive.

En ocasión de este acto, Aviano afirmó: "Esta etapa se extenderá hasta el 31 de diciembre. Se ratifican todos los beneficios y para el Día de la Madre, los reintegros de ciertos rubros que corrían los lunes, martes y miércoles, estarán vigentes hasta el domingo 17".

“En estas recorridas aprovechamos para dialogar con los comerciantes y recolectar inquietudes respecto del programa. Hay rubros que quieren incorporarse y resta también sumar la opción de adherirse mediante tarjeta de crédito”, agregó.

En este sentido, respecto de futuras modificaciones en el esquema de beneficios que ofrece la Billetera Santa Fe, Aviano explicó que a fin de año se hará un balance del programa y recién entonces se evaluarán cambios que ayuden a seguir potenciando a la actividad comercial santafesina.

Por su parte, el titular AER, Ricardo Diab, aseguró: “Desde el primer momento apoyamos este proyecto porque nos parece un hecho muy genuino para incentivar el consumo inyectando dinero en los consumidores. Billetera Santa Fe estira 5 mil pesos cada salario, lo cual se ve volcado al consumo. Por eso difundimos este programa en todos los centros comerciales. Desde el Día del Niño el comercio fue creciendo y esperamos una campaña similar para el Día de la Madre y las fiestas de Fin de Año. Van a ser fechas que van a estabilizar la actividad comercial. La apertura permitirá. por ejemplo, volver consumir indumentaria formal. Es una nueva fisonomía del comercio”.

###### Articulación con turismo

El titular de la Secretaría de Turismo de la Provincia, Alejandro Grandinetti, destacó los beneficios que Billetera Santa Fe implica para el turismo santafesino: “Venimos trabajando para que la plataforma Viví Santa Fe se integre a la Billetera Santa Fe y se use la tecnología para beneficiar el ecommerce, se amplíen beneficios a los consumidores santafesinos y que se integre con el plan nacional PreViaje. Luego de la crisis sanitaria sobreviene una crisis económica. Nosotros entendemos que debe ayudarse a la demanda para recuperar actividad económica comercial e industrial”.

“El turismo es uno de los rubros que más va a crecer. Estamos frente a un fin de semana largo con una muy buena tasa de ocupación. Vamos a pensar en promociones exclusivas para el turismo a través de Billetera Santa Fe, tanto para alojamientos como para espectáculos culturales”, destacó.

###### Billetera Santa Fe

Es un programa de beneficios dispuesto por la Provincia de Santa Fe, para incentivar la demanda de bienes y/o servicios mediante el otorgamiento de reintegros de dinero a consumidores finales, por las compras que estos realicen en comercios radicados en la Provincia de Santa Fe que se hayan adherido al Programa Billetera Santa Fe.

Actualmente se encuentra operativo en casi 300 localidades de la provincia. Tiene 25 mil comercios adheridos y más de 1,1 millón de usuarios.