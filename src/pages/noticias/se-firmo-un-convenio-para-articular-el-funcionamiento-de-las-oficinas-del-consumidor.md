---
category: La Ciudad
date: 2021-04-03T06:00:05-03:00
thumbnail: https://assets.3dnoticias.com.ar/Jaton.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Se firmó un convenio para articular el funcionamiento de las oficinas del
  consumidor
title: Se firmó un convenio para articular el funcionamiento de las oficinas del consumidor
entradilla: La Municipalidad y el Ministerio de la Producción de la provincia rubricaron
  un acuerdo para que los reclamos de los usuarios iniciados en el ámbito local, puedan
  continuarse en la provincia.

---
La Municipalidad firmó un convenio con el gobierno provincial con el fin de articular el funcionamiento de sus respectivas oficinas de Protección y Defensa al Consumidor y Usuario a los fines de velar por el cumplimiento de lo establecido por la Ley Nacional Nº 24.240 de Defensa del Consumidor. La rúbrica se celebró entre el intendente Emilio Jatón y el secretario de Comercio Interior y Servicios dependiente del Ministerio de la Producción, Juan Marcos Aviano.

La rúbrica se celebró entre el intendente Emilio Jatón y el secretario de Comercio Interior y Servicios dependiente del Ministerio de la Producción, Juan Marcos Aviano. Con este acuerdo, los consumidores de la ciudad de Santa Fe podrán optar por iniciar su reclamo ante la Oficina Municipal de Protección y Defensa al Consumidor y Usuario o ante la Dirección Provincial de Promoción de la Competencia y Defensa del Consumidor, dependiente de la secretaría ya mencionada.

Para el caso de los reclamos iniciados ante la Municipalidad, la audiencia de conciliación que se celebre en dicha instancia se considerará como agotamiento de la etapa conciliatoria prevista en el procedimiento provincial, a los fines de evitar dilaciones y la duplicidad de instancias en el reclamo de los usuarios o consumidores.

**Simplificación**

En los casos en que no se logre un acuerdo entre las partes de la relación de consumo en la instancia conciliatoria en el seno de la Municipalidad, se considerará dicha audiencia como agotamiento de la instancia conciliatoria prevista en el procedimiento provincial. Esto es para brindar a los consumidores y usuarios una atención y resolución de conflictos más ágil y eficaz.

En ese caso, las actuaciones administrativas serán remitidas por la Oficina Municipal de Protección y Defensa al Consumidor y Usuario a la Secretaría de Comercio Interior y Servicios dependiente del Ministerio de Producción, en un plazo no mayor a 15 días hábiles, para su respectivo análisis y eventual instrucción sumarial.

En consecuencia, los expedientes sin acuerdo de partes, derivados por la Municipalidad a la provincia habilitan el procedimiento administrativo destinado a aplicar las sanciones pertinentes en caso de violación al Estatuto de Defensa del Consumidor, conformado por el conjunto de normas aplicables en la materia.

**Agilizar los reclamos**

El director de Derechos y vinculación Ciudadana, Franco Ponce de León, destacó la importancia del convenio “porque lo que hace es agilizar los reclamos por parte de los usuarios y consumidores y evita la duplicidad”. En esta línea, explicó: “Esto significa que aquellos que hagan un reclamo o denuncia en la oficina municipal, en ella se llevarán a cabo las instancias administrativas correspondientes y, si se llega a un acuerdo, se resolverá el tema, pero en el caso de que no se arribe a una solución se deberá remitir al organismo provincial”.

Luego amplió: “Lo que pasaba antes era que el usuario o consumidor debía iniciar nuevamente el proceso de conciliación, es decir que se generaba una doble instancia. Con este convenio se simplifica, y todo lo actuado en la oficina de defensa del consumidor municipal, se derivará a la provincia y allí se toman las medidas sancionatorias correspondientes, ya que es el órgano de aplicación”.

**Evitar el doble camino**

Por su parte, Aviano manifestó que “lo importante de este convenio es que hoy se termina ese doble camino que los vecinos y vecinas de la ciudad de Santa Fe tienen que hacer porque vienen a la oficina municipal a hacer el reclamo, no hay audiencia de conciliación o la empresa no se presenta, entonces tienen que volver a iniciar el trámite en la oficina provincial, y lo que se hace ahora es simplificar esa instancia. Lo que tenemos que hacer es descentralizar funciones y recursos”.

Este tipo de convenios ya se hizo con otras ciudades santafesinas con la idea de dar esa respuesta que el vecino necesita a través de la simplificación de la gestión. “Invitamos al municipio santafesino a que se sume a la red provincial de oficinas municipales del consumidor y de esa manera podamos trabajar articuladamente con las más de 25 ciudades de la provincia y hoy incorporamos a Santa Fe”, agregó el funcionario provincial.

Para finalizar, el secretario de Comercio Interior y Servicios comentó: “Esta adhesión del municipio a la red nos permite, en el caso de solicitarlo, hacer aportes económicos para fortalecer el funcionamiento de las oficinas, para que mejoren su equipamiento, su estructura y para que refuercen el recurso humano que pueda requerirse para dar información al consumidor. También permite trabajar articuladamente con las áreas de inspección, por ejemplo, con el tema de precios, entre otros”.