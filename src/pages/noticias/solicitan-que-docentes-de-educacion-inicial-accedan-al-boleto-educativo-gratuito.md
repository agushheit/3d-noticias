---
category: La Ciudad
date: 2021-06-25T08:03:45-03:00
thumbnail: https://assets.3dnoticias.com.ar/boleto-gratuito.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Concejo Municipal Santa Fe
resumen: Solicitan que docentes de educación inicial accedan al boleto educativo gratuito
title: Solicitan que docentes de educación inicial accedan al boleto educativo gratuito
entradilla: El Concejo Municipal aprobó un proyecto para solicitar que el personal
  docente del sistema municipal de educación inicial también pueda hacer uso del boleto
  gratuito, al igual que el personal docente de otros niveles.

---
El Boleto Educativo Gratuito -vigente en nuestra provincia desde este año- tiene como objetivo garantizar que estudiantes, docentes y asistentes escolares puedan acceder a sus establecimientos educativos. Desde su puesta en marcha, este beneficio alcanza a alumnos/as regulares de los niveles -inicial, primario, secundario, terciario y universitario- y a docentes y asistentes escolares de establecimientos educativos provinciales.

Por este motivo, el Concejo Municipal aprobó un proyecto -unificado de dos iniciativas de los concejales Lucas Simoniello y Luciana Ceresola- en el que solicita al Gobierno Provincial la inclusión de docentes del sistema municipal de educación inicial al beneficio del boleto educativo gratuito. El sistema municipal está conformado por jardines de gestión estatal y de gestión particular.

Al respecto, Simoniello afirmó que “entendiendo que el propósito del Boleto Educativo Gratuito es ‘eliminar una potencial barrera de acceso a la educación’, todo el personal docente debería estar en condiciones de solicitar el beneficio”. A su vez, destacó que “si se pretende abordar todos los niveles y modalidades educativas, no debe excluirse, entonces, a las docentes de jardines de infantes o maternales que desempeñan su actividad en la ciudad”.

Por su parte, Ceresola remarcó “que el personal docente y no docente que se desempeña en los jardines también pueda hacer uso del boleto gratuito es reconocer de alguna manera la importancia que tiene la educación inicial para los niños de entre 45 días y 4 años”. A su vez, destacó que “los jardines no solo son espacios de cuidado, sino que son los primeros inicios de la vinculación con las instituciones y la socialización para los más chicos”.