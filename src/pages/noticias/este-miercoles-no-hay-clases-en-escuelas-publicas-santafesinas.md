---
category: Agenda Ciudadana
date: 2021-10-13T06:15:15-03:00
thumbnail: https://assets.3dnoticias.com.ar/MEDIDADEFUERZA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Este miércoles no hay clases en escuelas públicas santafesinas
title: Este miércoles no hay clases en escuelas públicas santafesinas
entradilla: Pese a la decisión del gobierno provincial de descontar el día, el gremio
  de maestros estatales ratificó las medidas de fuerza para el 13, el 20 y el 21 de
  octubre.

---
Los docentes nucleados en Amsafe realizarán un paro de actividades en las escuelas este miércoles en rechazo a la oferta salarial del gobierno santafesino, en el marco de las paritarias del sector. El gremio es el único que se opuso al incremento del 17% en tres tramos, que se suma al acuerdo del mes de marzo y que redondea un aumento anual del 52%. En los establecimientos privados habrá clases normalmente.

 Por unos 2.500 mil votos, los docentes públicos santafesinos votaron la semana pasada en contra de la oferta y anunciaron que, en caso de que las negociaciones no prosperen, realizarán una doble jornada de protesta los días 20 y 21 de octubre.

  En Rosario, la moción de rechazo y movilización se votó por encima del 60 por ciento. En tanto, la seccional La Capital, donde había ganado la aceptación, se plegará a la medida de fuerza y realizará un acto frente a la regional IV a media mañana bajo la consigna "Nuestra fortaleza es la Unidad".

 **A descuento**

 La respuesta gremial fue calificada como "desproporcionada" por parte del gobierno provincial. El ministro de Trabajo, Juan Manuel Pusineri, anunció la semana pasada que se descontarán los días no trabajados, que no habrá nueva una propuesta y que sólo cobrarán el aumento aquellos gremios hayan aceptado.

 Tras las declaraciones del funcionario, la secretaria general del gremio, Sonia Alesso, rechazó los dichos del ministro y fundamentó el rechazo. "Hubo varios elementos y actores que influyeron; uno es el salarial donde se plantea el tema de los tramos y de que una parte del aumento será recién el año que viene. La votación fue muy numerosa, con más de 28 mil docentes que participaron; fueron votaciones divididas en casi todos los departamentos. Tanto en las mociones de aceptación como de rechazo, estaban planteados estos temas. Esto más el pedido de una nueva convocatoria definió la votación y el plan de lucha", describió en diálogo con El Litoral.

 Por otra parte, el ministro Pusineri deslizó que el rechazo a la propuesta es fruto de una interna sindical. "Está claro que en el caso de Amsafe hay un problema de política interna; hay determinados sectores de la dirigencia (básicamente de la departamental Rosario) que habitualmente rechaza toda propuesta que se formule. Por ello, sea del 50, del 60, del 70 ó del 90, la respuesta siempre va a ser del rechazo", planteó.

 Sobre este tema, la titular de Amsafe contestó tajantemente: "me parece que no corresponde que un funcionario opine sobre las cuestiones de los gremios, menos el ministro de Trabajo que debería ser neutral y que es quien convoca a las paritarias y tiene que hacer lo posible para que si hay un conflicto, se destrabe. No tiene que ver con una cuestión interna; hay que hacer otros análisis sobre temas más profundos".

 