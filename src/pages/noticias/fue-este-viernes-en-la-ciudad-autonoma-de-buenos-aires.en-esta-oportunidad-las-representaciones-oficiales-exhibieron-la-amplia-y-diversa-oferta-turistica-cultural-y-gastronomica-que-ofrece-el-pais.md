---
category: Estado Real
date: 2021-11-28T06:00:08-03:00
thumbnail: https://assets.3dnoticias.com.ar/monchito.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Santa Fe participó de la 9° edición de la Noche de las Casas de Provincia
title: Santa Fe participó de la 9° edición de la Noche de las Casas de Provincia
entradilla: Fue este viernes, en la Ciudad Autónoma de Buenos Aires. En esta oportunidad
  las representaciones oficiales exhibieron la amplia y diversa oferta turística,
  cultural y gastronómica que ofrece el país.

---
El gobierno provincial participó este viernes en la Ciudad Autónoma de Buenos (CABA) de la 9° edición de La Noche de las Casas de Provincia, organizada por el Consejo Federal de Representaciones Oficiales de Casas de Provincia (Confedro), con el objetivo de promover el federalismo y la integración regional, poniendo en relevancia el potencial productivo y la promoción de productores locales. La actividad se desarrolló en cada una de las Casas de provincia.

Durante la jornada, las representaciones oficiales abrieron sus puertas para mostrar la amplia y diversa oferta turística, cultural y gastronómica, visibilizando a los y las artistas y a las diferentes expresiones culturales, y mostrando al público una experiencia única, representativa de la diversidad e integración que ofrece la Argentina.

**EN LA CASA DE SANTA FE**

En la oportunidad, la ministra de Igualdad, Género y Diversidad de la provincia de Santa Fe, Celia Arena, manifestó que “es muy importante poder mostrar en CABA todo lo que Santa Fe tiene: productos regionales, stands del gobierno para brindar información acerca de los distintos programas provinciales; la participación de artistas locales; y una oferta cultural, productiva y turística que nos enorgullece”.

Por su parte, la secretaria de Gestión Federal de la provincia, Candelaria González del Pino, expresó que “es un placer tener esta fiesta y mostrar en Buenos Aires nuestros productos, empresas y artistas. Y acá los tenemos reunidos, representando a todo el territorio santafesino. Estamos muy contentos y podemos ver que la gente también está muy contenta”, indicó.

“La idea fue mostrar las riquezas culturales, turísticas, gastronómicas de cada provincia. En 2020 el evento no se pudo llevar a cabo por la pandemia y, finalmente, todas las provincias, junto con Confedro, decidimos hacerlo porque creemos que la gastronomía, el turismo y los diversos lugares de nuestras provincias, son el motor del desarrollo económico que hoy estamos necesitando”, completó González del Pino.

Finalmente, el autor y compositor santafesino, “Monchito” Merlo, señaló “que se haya cortado la calle; estar en este lugar rodeado de bancos, frente a la Casa de Santa Fe; y encontrarnos con autoridades de la cultura de la provincia y con esta respuesta de la gente, fue un momento maravilloso”.

Emplazada en 25 de Mayo 178, La Casa de Santa Fe presentó shows en vivo de artistas santafesinos como “Monchito” Merlo y su conjunto, DJ Vulva Soul, Ballet Yeroki, Luisina Mathieu, y María Emilia Prono y Emanuel Silva.

Asimismo, el público participó de sorteos; degustó “Productos de mi Tierra” y otros sabores típicos de grandes empresas de alimentos de la provincia; y visitó la muestra “Patria Gaucha”, del Negro Fontanarrosa. Además, en el frente del edificio se proyectó un video animado –mapping- con parte de la historia santafesina.

De la actividad participaron autoridades nacionales y provinciales, junto público en general integrado por vecinos de CABA y ciudadanos de distintos lugares del país.