---
category: La Ciudad
date: 2021-11-03T06:00:04-03:00
thumbnail: https://assets.3dnoticias.com.ar/cementerio.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: En el Cementerio Municipal funcionará un espacio interreligioso
title: En el Cementerio Municipal funcionará un espacio interreligioso
entradilla: " Se trata de un lugar de oración, reflexión, meditación y recogimiento
  para todas las personas, más allá de la creencia."

---
En el Oratorio del Cementerio Municipal el intendente Emilio Jatón, junto a representantes de distintas religiones, habilitaron un espacio de oración, reflexión, meditación y recogimiento para todas las personas que tengan o no alguna creencia. En este lugar se colocará una atril que reflejará una frase representativa de cada uno de los credos, también se confeccionaron audioguías y se harán visitas a este lugar para que la comunidad sepa y conozca este espacio que quedó habilitado y que estará abierto a la comunidad en los mismos horarios que los está el cementerio, de lunes a domingo y feriados de 7.30 a 12.30.

Además de Emilio Jatón, estuvo el director de Derechos y Vinculación Ciudadana del Municipio, Franco Ponce de León y los concejales, Laura Mondino y Leandro González; y como representantes de las iglesias que integran la mesa de diálogo interreligioso estuvieron Rut Ríos Brunet, pastora del misterio evangélico Bautista Presencia de Dios; el reverendo Miguel Mathey, presidente de pastores evangélicos de la ciudad de Santa Fe; y José Przepiorka, seminarista de la comunidad israelita de Santa Fe.

También María Eugenia Flores, discípula coordinadora espiritual Escuela Científica Basilio; Marwan Gill, presidente de la comunidad musulmana Ahmadia; Carlos Zanutinni, presbítero regional de la iglesia evangélica metodista argentina; Néstor Capriotti, teniente del Ejército de Salvación; Luciano Rossi, de la iglesia Gnóstica Cristiana; Juan José y Mónica Levrino de la iglesia de Jesucristo de los Santos de los Últimos Días; y Axel Arguinchona, presbítero de la iglesia católica.

**Un lugar para todos**

El director de Derechos y Vinculación Ciudadana del Municipio, Franco Ponce de León destacó la importancia de la creación de este espacio y de la apertura del Oratorio, que si bien fue restaurado en los últimos años, estaba cerrado. “A través del diálogo con la mesa interreligiosa, de una iniciativa de la concejala Laura Mondino en el Concejo Municipal y por una decisión del intendente Emilio Jatón se abren las puertas del Oratorio que ahora cumplirá una función”, dijo.

Luego continuó: “La idea es que todas las personas que necesiten un momento de recogimiento de cualquier credo o requieran un ámbito para poder reflexionar, sea este oratorio, ese ámbito. Vamos a seguir generando acciones en diálogo con la mesa interreligiosa para que todos y todas puedan sentirse parte más allá de la religión que profesen”.

Ponce de León, recordó que si bien este oratorio fue restaurado hace unos años, “permanecía cerrado y solo era usado como un lugar de guarda de elementos para el cementerio”, dijo y luego agregó: “Por eso es un paso muy importante porque se abren las puertas, en todo sentido, y se abren para todos y para todas, que sea un lugar que nos una a todos, sin importar el credo”.

**Espacio común**

Por otro lado, uno de los integrantes de esta mesa de diálogo interreligiosa, José Przepiorka, seminarista de la comunidad israelita de Santa Fe, también valoró la apertura de este espacio para toda la sociedad santafesina porque “más allá del credo que cada uno tenga o la religión que practique habrá un espacio común para rezar, elevar las oraciones o simplemente para recordar a los seres queridos”, agregó.

Por su parte, Mónica Levrino, coordinadora de la mesa de diálogo interreligiosa dijo que “se trata del primer oratorio interreligioso de la ciudad y este es un paso muy importante”. Luego anticipó: “El sábado vamos a tener una intervención en el cementerio, con visitas guiadas, con las diferentes iglesias contando sus puntos de vista sobre la muerte y lo que pasa después”. En este marco, se hace la invitación a toda la comunidad a sumarse a esta actividad.