---
category: Agenda Ciudadana
date: 2021-09-30T06:15:02-03:00
thumbnail: https://assets.3dnoticias.com.ar/JUBILACION.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: La jubilación anticipada alcanzaría a unos 30.000 argentinos
title: La jubilación anticipada alcanzaría a unos 30.000 argentinos
entradilla: "El gobierno nacional anunció que las mujeres de 55 y 60 años y los hombres
  de entre los 60 y 65 con treinta años de aportes que están desocupados podrán acceder
  a la mínima nacional.\n\n"

---
Según anunciaron este miércoles el jefe de Gabinete, Juan Manzur, y la directora ejecutiva de la Administración Nacional de la Seguridad Social (Anses), Fernanda Raverta, el Gobierno prevé jubilar de manera anticipada a las personas que ya cuenten con los 30 años de aportes pero que no tengan la edad estipulada para acceder al beneficio previsional.

 En tal sentido, el abogado previsionalista Eduardo Cóceres explicó que se trata del “mismo modelo que implementó Néstor Kirchner desde 2005 a 2007”: “la prestación estaría fijada en un monto que iría desde el cincuenta hasta el ochenta por ciento, pero también se está vislumbrando que reciban la jubilación mínima, que hoy está en 25 mil pesos”, indicó el especialista.

 “Es una realidad que no podemos negar -admitió Cóceres-, existen personas que tienen treinta años de servicio con aportes pero no tienen la edad para jubilarse y están desocupados; entonces, el problema que plantean ellos es “qué hago, no puedo aportar y no me puedo jubilar”. “La idea es darles la posibilidad de hacerlo entre los 55 y 60 años a las mujeres y a los hombres entre los 60 y los 65”, añadió.

 Sin embargo, el abogado aclaró que “esto es un anuncio, está en estudio, pero indudablemente va a salir en el paquete que el gobierno haga cuando inicie la campaña electoral, junto con la posibilidad del aumento con el bono, por única vez, de seis mil pesos”.

 **Beneficiarios**

En tanto, Cóceres estimó “entre veinte y treinta mil” la cantidad de personas que podrían entrar en esta jubilación a nivel nacional, “con lo cual no se modificaría mucho la situación”.