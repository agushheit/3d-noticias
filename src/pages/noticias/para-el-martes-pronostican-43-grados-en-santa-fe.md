---
category: La Ciudad
date: 2022-01-09T06:15:04-03:00
thumbnail: https://assets.3dnoticias.com.ar/oladecalor.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Para el martes pronostican 43 grados en Santa Fe
title: Para el martes pronostican 43 grados en Santa Fe
entradilla: 'El pronóstico para Santa Fe y alrededores del Centro de Informaciones
  Meteorológico (CIM) de la UNL adelanta unas máximas muy altas.

'

---
El CIM de la UNL presentó el pronóstico para Santa Fe y alrededores, y adelantó temperaturas máximas superiores a los 40 grados para la semana que viene. El día más caluroso será el martes que alcanzará los 43 grados.

Para este domingo se prevén temperaturas entre los 22 y 40 grados: "Cielo despejado o con leve nubosidad. Condiciones estables. Temperaturas en suave ascenso. Vientos moderados predominando del sector este/noreste rotando al este por momentos".

La mínima este lunes está prevista 24 y la máxima en 42. "Cielo mayormente despejado o con leve nubosidad. Condiciones estables. Temperaturas en ascenso. Vientos leves de direcciones variables, predominando del sector nor/noreste", dice el pronóstico.

El martes habrá una máxima de 43 grados y una mínima de 25. "Cielo despejado, con alguna nubosidad por momentos. Condiciones estables. Temperaturas en ascenso. Vientos leves predominando del sector nor/noreste, rotando al este/sureste por momentos", informa el CIM UNL.