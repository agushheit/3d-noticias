---
category: Estado Real
date: 2021-03-06T07:30:18-03:00
thumbnail: https://assets.3dnoticias.com.ar/cines.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Provincia trabaja en la reapertura de las salas de cine
title: La Provincia trabaja en la reapertura de las salas de cine
entradilla: 'El ejecutivo nacional autorizó la actividad en Santa Fe a partir de una
  solicitud realizada por el gobierno provincial. '

---
A raíz de una solicitud del Gobierno de Santa Fe, el ejecutivo nacional autorizó la reapertura de las salas de cine en todo el territorio provincial a través de la resolución 179/2021.

Al respecto, el ministro de Trabajo, Empleo y Seguridad Social, Juan Manuel Pusineri, señaló que “es una de las actividades que más comprometida estuvo durante la pandemia”, y recordó que a principios del 2020 la cartera de trabajo intervino frente a casos de suspensiones y despidos para proteger las fuentes laborales.

La ocupación de las salas se realizará a través de burbujas con un distanciamiento de dos metros, sumado a un esquema de ventilación e higiene permanente: “Estamos hablando de una actividad que nuclea alrededor de 500 personas en toda la provincia, de los cuales la mitad trabaja en los denominados multi pantalla, que son tres cadenas de Rosario y una de Santa Fe. Esos trabajadores durante este tiempo percibieron un salario de suspensión, equivalente al 75% de la remuneración, y que a su vez proviene de una paritaria del 2019”, explicó el funcionario.

Además, indicó que “el protocolo nacional establece una utilización de hasta el 50% de las salas”, frente a la recomendación las autoridades de salud de la provincia que aconsejan hasta un 30%. “Eso todavía es motivo de evaluación, lo que está claro es que cualquier resolución que tomemos estará sujeta a un monitoreo”, remarcó.

En tanto, a partir de la publicación del decreto y en forma inmediata la actividad estará autorizada, “aunque el inicio dependerá del giro comercial de cada una de las cadenas”, estimó el ministro.