---
category: La Ciudad
date: 2021-09-02T06:15:32-03:00
thumbnail: https://assets.3dnoticias.com.ar/LINEA18.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Tras cinco años, la Línea 18 vuelve a entrar a Santa Rosa
title: Tras cinco años, la Línea 18 vuelve a entrar a Santa Rosa
entradilla: Luego de poner en condiciones las calles, el servicio ingresará al barrio
  Santa Rosa de Lima después de que en 2012 se interrumpió la normal prestación.

---
La Línea 18 de colectivos vuelve a entrar, después de cinco años, a barrio Santa Rosa de Lima, según anunció el director de Transporte de la Municipalidad, Lucas Crivelli. Esto se pudo lograr de un trabajo en conjunto entre la Red de Instituciones del barrio y el municipio, que trabajó en el arreglo de calles para que las unidades pudieran efectivizar el recorrido.

"Desde la Municipalidad y tras mucho esfuerzo, pudimos colaborar para poner en condiciones la red vial del barrio, sobre todo por calle Estrada, quedando pendiente algunos trabajos sobre Padre Quiroga. Pero así se pudo dar una solución a un pedido de los vecinos, para que tengan de nuevo el servicio de la Línea 18 de colectivos con el ingreso al barrio que se logró con una inversión millonaria ".

El funcionario además confirmó que todos los coches de la Línea van a ingresar. "Para los vecinos es una gran noticia ya que desde 2012 de manera intermitentes el servicio comenzó con cortes debido al mal estado de las calles. Además, también hablamos con los vecinos para poder brindarle la seguridad necesaria para que no solo ellos, sino los trabajadores, puedan estar seguros".

En cuanto al recorrido, confirmó que provisoriamente será por Padre Quiroga hasta pasar a calle Mendoza y llegando hasta Juan de Garay donde desviará a la izquierda para llegar hasta Juan Manuel Estrada; y de Juan Manuel Estrada hasta Mendoza, para retomar el recorrido habitual.