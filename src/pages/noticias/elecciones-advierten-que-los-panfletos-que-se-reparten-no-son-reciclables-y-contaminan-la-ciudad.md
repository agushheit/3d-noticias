---
category: La Ciudad
date: 2021-08-30T06:00:56-03:00
thumbnail: https://assets.3dnoticias.com.ar/basuraelectoral.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Elecciones: advierten que los panfletos que se reparten no son reciclables
  y contaminan la ciudad'
title: 'Elecciones: advierten que los panfletos que se reparten no son reciclables
  y contaminan la ciudad'
entradilla: Una especialista en gestión ambiental de la UNL señala que estos panfletos
  publicitarios están hechos casi en su totalidad de material látex que no es recuperable.

---
Desde hace un mes, con el inicio de las campañas políticas de cara a las elecciones crecen las propuestas partidarias en la ciudad de Santa Fe. Con una amplia oferta de precandidatos para concejales, diputados y senadores nacionales se reproducen diferentes tipos de publicidades, entre ellas, la folletería. En este contexto, es habitual en algunas zonas de la ciudad encontrarse con panfletos de todos los frentes en los que se muestran sus caras, nombres y, a veces, propuestas. El problema es que no siempre son entregados en mano y quedan libres al viento en veredas, plazas y la vía pública.

"Los panfletos utilizados actualmente para las campañas políticas, los cuales se disponen en los buzones y hoy vemos distribuidos por todos los espacios públicos, son erróneamente llamados «de papel», pero no lo son. Están formados por una capa muy fina de fibras celulósicas, la cual se recubre por ambos lados con pinturas tipo látex, las cuales provienen de los combustibles fósiles, en una máquina estucadora y/o por ploteo", explica.

La especialista además detalla que "debido a la imposibilidad de separar ambas capas (papel + pintura plástica + aditivos), ya que estos folletos no tienen un valor comercial, los panfletos no se pueden reciclar, y no se reciclan", al mismo tiempo que apunta que las personas que se dedican al trabajo de juntar materiales para el reciclado de manera habitual en la ciudad no se llevan estos panfletos ya que no se pueden recuperar de ninguna manera y terminan sueltos a la suerte del viento. Y advierte que todos los productos que provienen de combustibles fósiles (petróleo) generan gases de efecto invernadero, que son contribuyentes a que aumente la temperatura en el planeta. Es decir, al calentamiento global.

"Tienen un impacto social ya que al ser distribuidos indiscriminadamente ensucian los espacios públicos de toda la ciudad, veredas, plazas, espacios verdes, vuelan en el viento, generando mayor carga de trabajo para las y los trabajadores de la limpieza urbana. Tienen una consecuencia económica, ya que los distintos partidos políticos mal-invierten en una forma de comunicación, de dudosa eficacia a la hora de modificar el voto ciudadano", agrega Pino.

Y sostiene: "Principalmente tienen un impacto ambiental negativo: en caso de ocurrencia de lluvias, estos panfletos no sólo aumentan el riesgo de obstruir los desagües pluviales, sino que, además, los mismos terminarán en los cursos de agua: ríos, arroyos y luego el océano. En este momento, en que estamos atravesando una emergencia hídrica, evidenciada en la bajante extraordinaria del Paraná, y con un fenómeno de La Niña que anticipa escasez de lluvias para los próximos meses, tal vez el efecto pueda parecer menor, pero no lo es, dado que el uso de estos panfletos es una metodología habitual, y porque los plásticos no son biodegradables, es decir no poseen la capacidad de ser degradados por los microorganismos presentes en los ecosistemas acuáticos. Tampoco en el suelo".

"Ya sea en el suelo o en el agua, los plásticos son compuestos químicos persistentes, es decir permanecen mucho tiempo inalterados en su composición química. Y lo que suele ocurrir, es que principalmente por efectos físicos, se particionen, se rompan en partículas cada vez más pequeñas, hasta llegar a un tamaño micro: es lo que se conoce habitualmente como micro plásticos", describe al ser consultada sobre las consecuencias de esta práctica.

En esta misma línea sostiene que en los ecosistemas acuáticos diferentes seres vivos los confunden con alimento. "Existen actualmente estudios del impacto en los sistemas endocrinológico, hormonal, y reproductivo de peces, por ingesta de plástico. Y esto no termina ahí, sino que cuando los seres humanos nos alimentamos con especies de río o mar, estamos incorporando a nuestro organismo dichos micro plásticos, con consecuencias similares a las mencionadas, pero con el agregado de que son las mujeres en general y aquellas que están en período de lactancia, las que se ven más afectadas. En la naturaleza no existe el concepto de residuos, y en este caso, vemos como se cierra un círculo, y vuelve a nosotros lo que desechamos al ambiente", alerta la magister.

Sobre las alternativas que se pueden disponer para no contaminar, Pino opinó para estas elecciones: "Todas las acciones del ser humano tienen impacto, lo importante es conocerlos e intentar evitar tanto la contaminación, como la generación de residuos, minimizando los riesgos a la salud humana y del ambiente, que es el primer principio de la Química verde. Por otra parte, vivimos en una era de comunicación digital, que nos brinda alternativas comunicacionales de menor impacto al de los panfletos plásticos".