---
category: Estado Real
date: 2021-03-19T07:11:25-03:00
thumbnail: https://assets.3dnoticias.com.ar/consumo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: Provincia y Nación articulan acciones para fomentar el comercio y el consumo
title: Provincia y Nación articulan acciones para fomentar el comercio y el consumo
entradilla: A través de la Secretaría de Comercio Interior y Servicios y la Anses,
  se brindaron detalles sobre las herramientas disponibles para beneficios y promociones.

---
El Gobierno de la provincia, a través de la Secretaría de Comercio Interior y Servicios del Ministerio de Producción, Ciencia y Tecnología, y la Administración Nacional de la Seguridad Social (ANSES), desarrollaron este jueves en Santa Fe una reunión informativa con entidades del sector comercial sobre las herramientas de acceso a beneficios y promociones para consumidores santafesinos en comercios locales.

Tras la actividad, que se realizó en la Federación de Centros Comerciales de la Provincia de Santa Fe (FECECO), el secretario de Comercio Interior y Servicios, Juan Marcos Aviano, indicó: “Iniciamos en conjunto con la Anses una ronda de encuentros para acercar a los vecinas y los vecinos, que están incluidos en el sistema previsional argentino, con algunos de los aportes a esta red de comercios que otorgan beneficios, descuentos y promociones en sus compras. Por eso, la idea es dar a conocer al máximo estos programas del gobierno nacional hacia el sector comercial santafesino y poder empalmar todas las acciones de fomento al comercio y al consumo que desde Nación y Provincia estamos llevando adelante”.

“Esta es la primera de una ronda anual que realizaremos con representantes del sector comercial en un momento en el que sabemos que hay que llevar adelante diversas acciones para que se repongan de una situación muy difícil”, agregó.

Por su parte, el director Regional de ANSES, Diego Mansilla, sostuvo: “Estamos convocando a los distintos sectores del comercio de la provincia para que se sume a nuestro programa Beneficios que busca incrementar el poder adquisitivo de los beneficiarios de nuestras prestaciones y potenciar el volumen de venta y el consumo en los comercios que se adhieran. Hay más de 300 comercios adheridos pero queremos llegar a todos y a todas, especialmente a los comercios de cercanía. Por eso, queremos resaltar el trabajo en conjunto que hacemos con el gobierno de la provincia para convocar a todos los sectores comerciales y, de esa manera, poder cumplir con nuestros objetivos”.

Finalmente, el presidente de Fececo, Eduardo Taborda, manifestó: “Nos sentimos orgullosos y celebramos estas acciones, ya que venimos trabajando con la Secretaría de Comercio Interior con Billetera Santa Fe y la de Anses es una herramienta más, que beneficia a todos los comercios. Permite fortalecer al sector y también al consumidor, dinamiza la economía y hace sustentable a la actividad”.

**PROGRAMAS PRESENTADOS**

Durante la jornada se destacaron las presentaciones del Programa Billetera Santa Fe, a partir del cual la provincia busca incentivar la demanda de bienes y/o servicios mediante el otorgamiento de reintegros de dinero a consumidores finales, por las compras que estos realicen en comercios radicados en la provincia que se hayan adherido al Programa; y el Programa Nacional “Beneficios ANSES”, el cual puso en marcha en todo el país una red de descuentos para acompañar a los beneficiarios y beneficiarias, y para que puedan hacer rendir mejor su dinero.

La herramienta nacional estipula, si se cobra una prestación de la ANSES, poder acceder a beneficios como la devolución del 10% de todas las compras realizadas en los comercios adheridos, usando la tarjeta de débito con la que se cobra la prestación. El tope máximo de devolución es de hasta $1000 por cada compra y no hay límites en la cantidad de operaciones que se puede realizar.