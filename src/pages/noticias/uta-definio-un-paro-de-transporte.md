---
category: Agenda Ciudadana
date: 2021-04-23T07:51:54-03:00
thumbnail: https://assets.3dnoticias.com.ar/colectivos.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: UTA definió un paro de transporte
title: UTA definió un paro de transporte
entradilla: Es por la falta de respuestas a los reclamos salariales. La medida de
  fuerza será el lunes 26 por 24 horas.

---
La Unión Tranviarios Automotor (UTA) anunció un paro para el lunes próximo debido a los escasos avances que tuvieron las negociaciones sobre los reclamos salariales que el sector demandaba a los empresarios del rubro.

En un comunicado mencionaron que tras la cantidad de audiencias sin resultados positivos llevadas adelante en el Ministerio de Trabajo de Nación resolvieron un cese de actividades en todo el país. La medida se concretará el lunes 26 de abril por 24 horas.

En medio de las nuevas restricciones donde desde las 21 horas en la provincia de Santa Fe no pueden funcionar los autos particulares y siendo la opción utilizar el transporte público de pasajeros para trasladarse el paro podría complicar el regreso a los hogares del conjunto de trabajadores de comercio, docentes y la población en general.

El mensaje de UTA finaliza exigiendo al Ministerio de Trabajo y a los empresarios que tengan presente que "el conflicto salarial no se resuelve sin sensibilidad y justicia social".