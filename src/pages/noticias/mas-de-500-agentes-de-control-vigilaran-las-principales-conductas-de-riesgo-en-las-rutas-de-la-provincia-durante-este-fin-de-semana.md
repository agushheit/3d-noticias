---
category: Estado Real
date: 2020-12-24T11:15:20Z
thumbnail: https://assets.3dnoticias.com.ar/2412seguridad-vial.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Más de 500 agentes de control vigilarán las rutas de la provincia durante
  este fin de semana
title: Más de 500 agentes de control vigilarán las rutas de la provincia durante este
  fin de semana
entradilla: La APSV implementará una mayor cantidad de acciones de fiscalización debido
  al aumento de los desplazamientos previstos por la celebración de la Navidad.

---
La Agencia Provincial de Seguridad Vial (PSV), dependiente del Ministerio de Seguridad, reforzará la prevención en rutas de la provincia e incrementará los operativos y las recomendaciones a los conductores para los traslados que se realicen a partir de este miércoles por la tarde con motivo del inicio de un fin de semana largo debido a la celebración de Nochebuena y Navidad.

La presencia en rutas de las fuerzas de seguridad provinciales y municipales tienen por objetivo reforzar las medidas de prevención para evitar siniestros viales.

<br/>

<span style="font-family: helvetica; font-size: 1.35em; font-weight: 600;">**Controles**</span>

La APSV dispuso una mayor cantidad de controles en los ingresos y egresos a zonas de quintas y casas de fin de semana, así como a salidas hacia lugares turísticos de otras provincias, tanto de región norte como sur, en vista de que se incrementará la circulación producto de la habilitación de los traslados dentro de la provincia y el ingreso de conductores desde otras zonas del país.

Los operativos incluyen 18 puestos fijos de control como parte del **Operativo Verano**, más los puestos móviles que se distribuyen en toda la provincia, las 24 horas, durante todo el fin de semana. Este diagrama incluirá la participación de más de 300 agentes de la Policía de Seguridad Vial y 150 inspectores de municipios locales.

**Se llevarán a cabo más de 60 operativos exclusivos de alcoholemia**, si bien en cada punto de control esta conducta se mide en forma rutinaria a todos los conductores fiscalizados, es decir, que es un control sistemático e integrado a otros dispositivos de prevención.

El Subsecretario de APSV, Osvaldo Aymo, expresó que «este fin de semana tendremos un importante incremento de la circulación vehicular en rutas, tanto de turismo interno como de personas que seguramente nos visitarán desde otras provincias, ya sea por turismo o para pasar las fiestas con familiares, por ello hemos aumentado los esfuerzos y recursos para poder detectar ciertas conductas antes de que pongan en riesgo la seguridad de los conductores».

«Como siempre decimos, esto es una tarea en equipo, colectiva, el Estado tienen una gran responsabilidad de cuidar a los ciudadanos, pero todos debemos comprometernos con la vida y el cuidado de nuestras familias», agregó el funcionario provincial.

<br/>

<span style="font-family: helvetica; font-size: 1.35em; font-weight: 600;">**Operativo Verano**</span>

La APSV diagramó un amplio esquema de controles en rutas para todo el verano que incluye 18 puestos fijos de fiscalización, muchos de ellos ubicados en zonas de turismo, como el acceso a ciudades que bordean la costa santafesina.

En este marco, Osvaldo Aymo, detalló que «habrá 18 puestos de control en todos los corredores viales, fundamentalmente a la vera del río Paraná».

Además, especificó que para las familias que ingresen a nuestra provincia como turistas se solicita una serie de requisitos de circulación para georreferenciar su recorrido y poder prever situaciones sanitarias frente al Covid.

Así, se solicita a los viajeros contar con la aplicación Covid-19 Santa Fe; tramitar el Certificado Verano que se obtiene en la web www.argentina.gob.ar/verano, y poseer documentación respaldatoria, donde conste el alojamiento de alquiler donde las personas se van a instalar por los días que permanezcan en la provincia, o ser propietario de un inmueble ubicado en territorio santafesino.

<br/>

<span style="font-family: helvetica; font-size: 1.35em; font-weight: 600;">**Concientización**</span>

Por otro lado la APSV reforzará la difusión de las principales recomendaciones que permitan tener un viaje seguro. El objetivo es brindar información sobre distintas situaciones que pueden presentarse durante el viaje, y cómo actuar ante cada una de ellas para evitar riesgos y aumentar la seguridad vial.

Entre las recomendaciones la APSV destaca revisar el estado mecánico general del vehículo y especialmente comprobar los frenos; programar el viaje teniendo en cuenta las personas que viajan, si hay niños o personas mayores, etc., a fin de establecer paradas y tiempos de descanso y no ingerir alcohol.

Es indispensable que todos los pasajeros lleven abrochado el cinturón de seguridad y que los niños viajen siempre atrás en los SRI (Sistemas de Retención Infantil) correspondientes a cada edad. Se deben llevar encendidas las luces bajas las 24 hs; tomar las precauciones de distancia y visibilidad antes de realizar un adelantamiento; respetar los límites de velocidad reglamentarios y no consumir alcohol antes ni durante la conducción.

<br/>

<span style="font-family: helvetica; font-size: 1.35em; font-weight: 600;">**La documentación reglamentaria que se debe portar es la siguiente:**</span>

\-Licencia de conducir vigente

\-Cédula única de identificación del automotor

\-Póliza de seguro en vigencia

\-Última patente del año en curso paga

\-DNI

\-RTO

\-Si el vehículo funciona con GNC, se debe contar con autorización para utilizar este tipo de combustible

Se puede consultar el estado del tránsito en las rutas  a través de la información que difunde la APSV en sus cuentas de Facebook [/seguridadvialsantafe](https://www.facebook.com/SeguridadVialSantaFe/) y Twitter [@redsegvial](https://twitter.com/redsegvial).