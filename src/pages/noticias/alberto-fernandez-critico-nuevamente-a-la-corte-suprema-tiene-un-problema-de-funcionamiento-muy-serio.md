---
category: Agenda Ciudadana
date: 2022-01-11T06:15:34-03:00
thumbnail: https://assets.3dnoticias.com.ar/MINISTROSPREMADEPOLLO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: 'Alberto Fernández criticó nuevamente a la Corte Suprema: "Tiene un problema
  de funcionamiento muy serio"'
title: 'Alberto Fernández criticó nuevamente a la Corte Suprema: "Tiene un problema
  de funcionamiento muy serio"'
entradilla: 'El Presidente apuntó contra el rol del máximo tribunal. "Cuando la Justicia
  funciona mal, no es que funciona mal para un ex presidente o ex ministro, funciona
  mal para los ciudadanos", advirtió.

'

---
El presidente Alberto Fernández volvió a cuestionar hoy a la Corte Suprema de Justicia, al afirmar que tiene "un problema de funcionamiento muy serio" y sostuvo que "empezó a degradarse la credibilidad" del tribunal durante la gestión del ex mandatario Mauricio Macri.

En medio de una referencia al caso de la dirigente kirchnerista Milagro Sala, detenida desde hace casi seis años en la provincia de Jujuy, Fernández cuestionó el rol del máximo tribunal en diversos temas y evaluó: "Creo que en la Corte Suprema hoy en día hay un problema de funcionamiento muy serio".

"La verdad es que yo soy el Presidente y quiero ser respetuoso de las instituciones, pero también soy un hombre que respeta el estado de derecho y que entiende que la Justicia es uno de los tres poderes de la República. Hay algo que está funcionando mal. Que hoy todavía Milagro Sala esté detenida es que hay algo que está funcionando mal", sostuvo el jefe de Estado en declaraciones radiales.

Fernández recordó la reforma de la Corte que se llevó a cabo en 2003, durante el gobierno de Néstor Kirchner, y puntualizó: "No buscamos jueces que fueran afines a nosotros. Así llegaron Elena Highton, (Ricardo) Lorenzetti, (Eugenio) Zaffaroni, Carmen Argibay, que fue tan importante, y se quedaron inclusive tres miembros que merecían todo nuestro respeto, que eran De Lucio, Petracchi y Carlos Fayt".

"A mí me parece que desde el momento que el gobierno de Macri propuso nombrar en comisión a dos jueces de la Corte (Horacio Rosatti y Carlos Rosenkrantz), empezó a degradarse la credibilidad. Cuando uno mira el funcionamiento actual, el tema es muy preocupante", afirmó el Presidente.

Fernández cuestionó el fallo de la Corte que declaró la inconstitucionalidad de la ley que rige al Consejo de la Magistratura y ordenó su reforma, al que calificó como "increíble" y agregó: "Un alumno de primer año sabe que una ley que ha sido derogada no puede nunca recuperar su utilidad por una sentencia. Necesita otra ley que la vuelva a poner en vigencia".

El mandatario desvinculó sus cuestionamientos de los procesos judiciales que enfrenta la vicepresidenta Cristina Kirchner y explicó: "Lo que yo proponía nada tiene que ver con Cristina. La Corte Suprema jamás trató una queja de las 14 quejas que Cristina presentó, jamás".

"Cuando la Justicia funciona mal, no es que funciona mal para un ex presidente o ex ministro, funciona mal para los ciudadanos. Cuando los despiden y hacen un juicio, si la Justicia no funciona bien, pierden. El problema de la Justicia es muy agudo y por eso hay que asumirlo, para que funcione como lo que es: un servicio", agregó el mandatario.

En el marco de sus críticas al Poder Judicial, Fernández señaló que "el abuso que se hizo de la prisión preventiva durante los cuatro años del gobierno anterior es increíble", al tiempo que también apuntó contra el uso de la figura de "asociación ilícita" en varios casos, entre ellos el de Sala.

"Es sabida mi posición sobre el tema Milagro Sala. Es un tema preocupante por la forma en que los procesos se desarrollaron según dicen muchos organismos de derechos humanos", señaló el Presidente.

En este sentido, indicó: "Días atrás, en una de esas causas de espionaje en los tribunales federales, la Cámara Federal rápidamente se ocupó de quitar la figura de asociación ilícita sobre los que aparecían involucrados. Eso no ocurrió en los años de Macri, cuando se hizo un abuso de la figura de asociación  
ilícita increíble".

"El delito más grave que según la Justicia jujeña cometió Milagro Sala es el de administración fraudulenta. La pena máxima de ese delito es seis años y uno debería llegar a la conclusión de que ha cumplido con esa pena. ¿Por qué sigue detenida? Porque le agregaron la asociación ilícita", agregó. En este sentido, Fernández remarcó que "en marzo la Corte va a hacer dos años que tiene en tratamiento el fallo de Milagro Sala y no pasa nada".

Pese a las críticas, Fernández evitó confirmar si tiene previsto impulsar un proyecto de ley para reformar la Corte Suprema de Justicia y tomó distancia de la posibilidad de que el oficialismo promueva algún juicio político.

No obstante, sus críticas al máximo tribunal se dieron luego de que el viceministro de Justicia, Martín Mena, apoyara la movilización que un sector del kirchnerismo convoca para el 1 de febrero a las puertas de la Corte Suprema y afirmara que "hay mucha gente con hartazgo".

Por otro lado, se refirió al escándalo por el video que difundió la Agencia Federal de Inteligencia (AFI) sobre el supuesto armado de causas judiciales contra el ex titular de la UOCRA de La Plata Juan Pablo "Pata" Medina.

"Cristina Caamaño (interventora de la AFI) me informó que había encontrado eso y me lo mandó por escrito. Después me mostró el video y le dije que hiciera la denuncia", contó el mandatario, en referencia a la grabación en la que el ex ministro de Trabajo bonaerense Marcelo Villegas dijo que "si pudiera tener una Gestapo una fuerza de embestida para terminar con los gremios, lo haría".