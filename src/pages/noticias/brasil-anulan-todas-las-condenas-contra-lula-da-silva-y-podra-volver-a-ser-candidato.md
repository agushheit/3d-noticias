---
category: Agenda Ciudadana
date: 2021-03-09T05:54:16-03:00
thumbnail: https://assets.3dnoticias.com.ar/lula.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: EFE, AFP, 3D Noticias
resumen: 'Brasil: anulan todas las condenas contra Lula da Silva y podrá volver a
  ser candidato'
title: 'Brasil: anulan todas las condenas contra Lula da Silva y podrá volver a ser
  candidato'
entradilla: Lo hizo el juez del Supremo Tribunal de Brasil, Edson Fachin. Y ordenó
  comenzar nuevamente la investigación en otros fueros debido a la supuesta parcialidad
  de la Fiscalía y el exjuez y exministro Sergio Moro.

---
Un juez del Supremo Tribunal de Brasil, Edson Fachin, anuló todas las condenas contra el expresidente Luiz Inácio Lula da Silva por corrupción y ordenó comenzar nuevamente la investigación en otros fueros debido a la supuesta parcialidad de la Fiscalía y el exjuez y exministro Sergio Moro.

Fachin tomó esta decisión al hacer lugar a un habeas corpus impuesto por la defensa del expresidente Lula en el marco de las causas impulsadas por la hoy cuestionada Operación Lava Jato.

La decisión monocrática de Fachin, un juez conocido por haber estado alineado a las denuncias y casos impulsados por la Operación Lava Jato, se produjo luego del escándalo generado por la filtración de mensajes que desnudaron lo que Lula y el PT siempre denunciaron: el trabajo conjunto y secreto de los fiscales y el entonces juez Moro para condenar al exmandatario antes de que pudiera competir en las últimas elecciones presidenciales.

Lula pasó 580 días preso en la ciudad de Curitiba, capital del estado de Paraná, y tras ser detenido en medio de un operativo que fue transmitido en el mundo entero, tuvo que bajar definitivamente su candidatura.

El ex juez Moro más tarde se convirtió en el primer ministro de Justicia del presidente que triunfó en esas elecciones, el actual mandatario Jair Bolsonaro.

**El Lava jato**

La Lava Jato (lavado de vehículos) comenzó por azar en una gasolinera de Brasilia que blanqueaba dinero, pero a medida que las autoridades fueron tirando del hilo de la madeja destaparon un complejo engranaje de corrupción de dimensiones continentales.

La rutinaria investigación que se inició el 17 de marzo de 2014 pronto se convirtió en un gigantesco escándalo que salpicó de lleno algunas de las más importantes empresas del país, entre ellas la petrolera estatal Petrobras y el imperio Odebrecht.

La Lava Jato abrió la caja de Pandora de una gigantesca red de corrupción en Brasil y sus hallazgos condujeron a prisión a poderosos ejecutivos y políticos de todo el arco partidario que durante décadas parecieron ser intocables.

El arresto más mediático en el país fue el del expresidente Luiz Inácio Lula da Silva (2003-2010), quien responde al proceso en libertad tras pasar 1 año y 7 meses entre rejas en la sede de la Policía Federal en Curitiba, capital de Paraná.

**En el continente**

Las confesiones de los implicados en la trama de corrupción, principalmente la de los ejecutivos del grupo Odebrecht, tuvo un efecto dominó que traspasó las fronteras de Brasil y sacudió los cimientos del sistema en más de una decena de países de Latinoamérica.

El escándalo, que pronto pasó a ser llamado "caso Odebrecht", salpicó a presidentes y expresidentes del continente acusados de haber participado en una de las mayores redes de sobornos de la historia.

Entre los investigados figuraba el exmandatario peruano Alan Garcia (1985-1990 y 2006-2011), quien se suicidó en abril de 2019 con un disparo en la cabeza cuando iba a ser detenido por presunto lavado de activos provenientes de coimas de Odebrecht.

Sospechas

En los siete años en los que estuvo vigente la operación fueron llevadas a cabo 1.450 órdenes de allanamiento, 211 conducciones coercitivas, 132 mandatos de prisión preventiva y 163 de arresto temporal.

En total fueron devueltos a las arcas públicas más de 4.300 millones de reales (unos 811 millones de dólares) gracias a 209 acuerdos de colaboración con algunos de los acusados de participar en tramas corruptas.

La Lava Jato, sin embargo, no ha estado exenta de críticas. Algunos de los fiscales de la operación han sido cuestionados por su supuesta falta de imparcialidad en las investigaciones.

Las sospechas surgieron, sobre todo, a partir de unos reportajes del portal The Intercept Brasil, que publicó intercambios de mensajes entre los fiscales de la Lava Jato y el juez encargado de esas investigaciones en primera instancia, Sergio Moro.

Esas conversaciones escritas generaron un enorme revuelo y pusieron en tela de juicio la imparcialidad de los fiscales, pues insinuaban que, de forma ilegal, Moro coordinó acciones del proceso que llevó a Lula a la cárcel en uno de los casos de corrupción.