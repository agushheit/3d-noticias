---
category: Agenda Ciudadana
date: 2021-06-03T07:51:40-03:00
thumbnail: https://assets.3dnoticias.com.ar/jaton1.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad de Santa Fe
resumen: 'Jatón en barrio Cabal: “Estas son obras que cambian la realidad de la gente”'
title: 'Jatón en barrio Cabal: “Estas son obras que cambian la realidad de la gente”'
entradilla: 'El intendente recorrió este miércoles los trabajos que se realizan en
  el Noroeste de la ciudad. Se trata de mejorado granular, cordón cuneta y desagües
  pluviales. '

---
“Estas son obras que cambian la realidad de la gente, la realidad de los barrios”, indicó este miércoles, el intendente Emilio Jatón luego de recorrer los trabajos viales y de desagües pluviales que se llevan adelante en el barrio Cabal. Las obras, enmarcadas en el Plan Integrar, contemplan la transformación de ese sector de la ciudad.

“Estas son obras postergadas, que los vecinos y vecinas reclamaron por décadas”, indicó el intendente, y puntualizó que se trata de calles que se anegan durante los días de lluvia, son calles que todavía tienen zanjas en el frente de las casas. “Por eso estamos haciendo lo que habíamos prometido. Los vecinos nos decían que recuerdan que ya sus padres pedían que se hicieran estos trabajos, ahora están viendo que esto es una realidad”, destacó.

Cabe señalar que la obra que se realiza puntualmente en este sector de Cabal, abarca 1700 metros de cordón cuneta, mejorado y desagües que incluyen las calles Estanislao Zeballos entre Bernardo de Irigoyen y Vieyra (unos 1400 metros) y los metros restantes están en Bernardo de Irigoyen, Boneo hasta Pasaje Santa Fe.

“Se trata de una obra compleja que va a llevar entre 7 y 8 meses, pero que avanza a buen ritmo. Ya llevamos un 22 por ciento de obra. Debajo del cordón cuneta están los caños que van a permitir que en cada esquina haya bocas de tormenta y las calles ya no se inunden como lo hacían”, detalló Jatón y añadió: “Lo importante es hacer las cosas y cumplir con la palabra”.

**Zona de obras**

Rodolfo Kehn se asomó a la vereda para ver cómo avanzan los trabajos que se realizan a lo largo de su barrio: “Estamos muy contentos, son obras que estábamos esperando desde hace rato porque en esta zona se inundaba en muchos lugares y ahora con este desagüe lindo que están haciendo, el agua se va a escurrir enseguida. Está quedando muy bien todo”.

Por otra parte, Rodolfo destacó que la obra “va ligero, están haciendo casi dos cuadras por día. Cambió mucho el barrio con todo esto, va a quedar como un bulevar”.

En tanto, Sara Ortiz destacó: “Estoy muy orgullosa por las obras que se están haciendo en el barrio. Muy agradecida de que hayan venido a barrio Cabal, porque estaba muy olvidado”. En tal sentido, agregó: “Decía que estoy orgullosa porque yo vivo acá desde hace años, de cuando eran todas quintas. Los vecinos agradecemos que se ocupen de este barrio. Hace poco arreglaron las luces. Además, era una zona que se inundaba mucho, pero con esta obra que está haciendo la Municipalidad yo pienso que salimos adelante. Y vamos por más”.

Otra de las vecinas que observaba desde la vereda el avance de los trabajos es Elsa Choquet. “Estoy chocha”, aseguró al ver el movimiento de máquinas y obreros que se replica con el correr de los días. “Estoy agradecida y emocionada. Va a quedar hermoso todo lo que están haciendo. Se está trabajando en Cabal, en Las Lomas, así que estamos muy contentas, sobre todo por los desagües. La lluvia era un problema grande porque todos nos inundábamos. Era todo barro esto, el agua entraba hasta las casas”, recordó.

El desagüe trae aparejado otro trabajo que modifica la fisonomía de los barrios: la erradicación de las zanjas. “Que nos tapen las zanjas es lo mejor que hay: las hojas que caen, la basura que tiran, todo hace que se tapen”, graficó Sara.

**Obra integral**

Las obras que la Municipalidad lleva adelante con fondos de la Nación (en el marco del Argentina Hace II), en los barrios Cabal y Las Lomas tienen como objetivo mejorar las condiciones de transitabilidad de peatones, ciclistas y vehículos, mediante la ejecución de obras viales e hídricas. Tienen un presupuesto oficial de $ 97,5 millones.

Las tareas incluyen pavimento, cordón cuneta, mejorado con estabilizado granular y desagües pluviales. Los beneficiarios directos de las obras son 5.049 habitantes de ambos barrios, a los que se suman, de forma indirecta, 55.392 personas residentes en el distrito Noroeste de la ciudad.

Los trabajos se concretarán en cuatro tramos: en Bernardo de Irigoyen, entre Castelli y Boneo; en Boneo, entre Bernardo de Irigoyen y pasaje Santa Fe; en Estanislao Zeballos, entre Bernardo de Irigoyen y Menchaca; y en Estanislao Zeballos entre Menchaca y Vieyra.

En los primeros tres tramos, que corresponden al barrio Cabal, se realizará cordón cuneta con estabilizado granular y bocacalles con badenes de hormigón. En tanto, en el último tramo, en Las Lomas, se hará una carpeta de concreto asfáltico entre cordones cunetas.

También se incluyen obras complementarias como rampas para accesibilidad, baldosas perimetrales de aviso, guías para personas con discapacidad visual, veredas de hormigón peinado en ochavas, cinta verde en ambas veredas y forestación en toda la traza. Asimismo, a lo largo de todo el recorrido se concretarán desagües pluviales, con cañería pluvial de H°A°, bocas de registro, bocas de tormenta, cámaras de captación y muros cabezales para acometidas de cunetas.