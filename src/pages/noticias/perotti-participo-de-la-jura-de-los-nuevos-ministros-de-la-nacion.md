---
category: Estado Real
date: 2021-09-22T06:00:23-03:00
thumbnail: https://assets.3dnoticias.com.ar/PEROTTIJURA.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Perotti participó de la jura de los nuevos ministros de la nación
title: Perotti participó de la jura de los nuevos ministros de la nación
entradilla: El gobernador de Santa Fe estuvo presente este lunes en la toma de juramento
  que realizó el presidente Alberto Fernández. Además, mantuvo reuniones con Eduardo
  De Pedro, Juan Zabaleta y Aníbal Fernández.

---
El gobernador Omar Perotti participó este lunes de la toma de juramento que el presidente Alberto Fernández realizó a los nuevos ministros del gabinete nacional. Luego, mantuvo reuniones de trabajo con los ministros del Interior, Eduardo “Wado” De Pedro; de Desarrollo Social, Juan Zabaleta; y de Seguridad, Aníbal Fernández.

Tras el encuentro con el flamante titular de la cartera de Seguridad, Perotti señaló que “esta primera reunión con el nuevo ministro fue una necesaria puesta en conocimiento sobre el trabajo de este tiempo de la provincia con la Nación, de la situación particular que se vive hace unos meses y de porqué la Nación tiene que tener una presencia fuerte en el territorio santafesino”.

Además, agregó que el "el ministro Fernández tiene mañana una reunión con todas las fuerzas y eso es clave porque Fuerzas Nacionales tienen efectivos en el territorio santafesino y de esas evaluaciones saldremos con un plan concreto de acción para actuar rápidamente", y destacó el compromiso de Alberto Fernández al sostener que “hay una firme decisión del presidente cuando ha dicho que la Argentina no puede permitir que estas cosas pasen en su territorio. Confiamos plenamente en que las acciones del ministro van a ir en esa dirección”, sostuvo el gobernador.

En este sentido, Perotti indicó que “para abordar integralmente esta situación tenemos la necesidad de reforzar la Justicia Federal y coordinar las acciones cuando tenemos varias fuerzas”.

Asimismo, señaló que se están desarrollando “juicios importantes a bandas de narcotráfico y estas situaciones no se dan en cualquier provincia”; por este motivo es necesaria la “presencia de Fuerzas Federales, para ayudarnos transitoriamente mientras mejoramos y fortalecemos con más uniformados nuestra propia institución, y en segundo lugar para garantizar la seguridad y la protección de fiscales y jueces, quienes tienen que tener la garantía de estar cuidados para cumplir plenamente su cometido”, finalizó Omar Perotti.

**Continuidad de las reuniones**

Este martes, el mandatario santafesino mantendrá nuevos encuentros de trabajo para continuar abordando la agenda de la provincia con los ministros de la Nación. En ese sentido, se reunirá con el titular de Obras Públicas, Gabriel Katopodis; el flamante jefe de Gabinete, Juan Manzur; de Agricultura, Ganadería y Pesca, Julián Domínguez; y de Transporte, Alexis Guerrera.