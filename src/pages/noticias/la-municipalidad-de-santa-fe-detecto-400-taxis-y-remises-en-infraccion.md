---
category: La Ciudad
date: 2021-12-19T06:15:33-03:00
thumbnail: https://assets.3dnoticias.com.ar/remises.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: La Municipalidad de Santa Fe detectó 400 taxis y remises en infracción
title: La Municipalidad de Santa Fe detectó 400 taxis y remises en infracción
entradilla: Además se retuvieron 19 vehículos por utilizar aplicaciones para viajes
  que no están habilitadas en la ciudad.

---
Hasta noviembre de 2021, la Municipalidad de la ciudad de Santa Fe, en 145 operativos realizados en la vía pública, inspeccionó unos 4.315 vehículos entre taxis, remises y transportes escolares de los cuales, un total de 400 taxis y remises estaban en infracción.

 El secretario de Control, Guillermo Álvarez, expresó que “hemos chequeado más de 4 mil remises en lo que va del año, en casi 150 operativos y encontrado casi 400 vehículos en infracción entre taxis y remises”.

 En esa línea indicó que dentro de las irregularidades se encuentran “taxis con las habilitaciones vencidas o sin habilitación directamente, sin carné profesional, remises ilegales” es decir, “de las más variadas”.

 **Aplicaciones**

Dentro del abanico de irregularidades también aparece la circulación de automóviles que funcionan a través a aplicaciones para transporte de pasajeros que, en la ciudad de Santa Fe en particular, “no tienen una regulación que las habilite a funcionar. No tienen una norma que las avale”, indicó Álvarez.

Y detalló que “dentro de los 145 operativos, en 14 detectamos vehículos de las apps circulando, por lo cual se labró el acta y se retuvieron 19 vehículos en total”.

 Asimismo indicó que “la multa que se hace es similar o igual a la de los remises ilegales y ronda los 30 mil pesos. Hemos tenido pagos de hasta 60 mil pesos dependiendo de las multas que tenia antes el infractor”.

 **Falta de coches**

 Por otro lado, el funcionario municipal descartó que la detección de 400 infractores sea una de las causas de la falta de coches tanto en taxis como remises sino que, según manifestó, éste es un problema “multifactorial”.

 El primero tiene que ver con “los efectos colaterales que generó la pandemia. Hubo muchos meses donde los servicios fueron casi nulos porque la gente tenia que estar encerrada y esto generó cambios de hábitos”, sostuvo Álvarez y detalló que “durante el 2020 realizaron un 20 % menos viajes de los que se realizaban antes de la pandemia y- además- muchos choferes tuvieron que buscar otros trabajos para subsistir”.

 Desde la Sociedad de Taximetristas aseguran que hay faltante de choferes para subir a las licencias y- de acuerdo al análisis realizado por el funcionario- puede tener que ver con “el segundo factor que es el de la inseguridad”, es decir, “hay que pensar qué lleva a los choferes a no querer trabajar de noche”.

 Además “la gente tiene miedo y prefiere tomarse un taxi o remis y por eso también hay una demanda más alta”.

 En la ciudad Capital de la provincia, unos “370 móviles tienen los botones de alerta que sirven para que si los choferes observan una situación sospechosa de los pasajeros tocan el botón y el móvil de la policía se acerca para hacer la inspección. Esto hay que seguir replicándolo en los móviles para tratar de disminuir la inseguridad y que es el principal motivo por el que no quieren circular”.

 Por último, como tercer factor determinante para la falta de móviles, Álvarez dijo que influye la “tramitación de la licencia” ya que “de las mil licencias que hay posibles, unas 800 están en condiciones y unas 200 no se pudieron destrabar administrativamente porque tienen que cumplir una serie de requisitos. Muchas veces pasa que no completan la documentación y no se destraba la licencia”.