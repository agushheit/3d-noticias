---
category: Agenda Ciudadana
date: 2021-03-30T07:31:28-03:00
thumbnail: https://assets.3dnoticias.com.ar/vizzotti-1.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: El Gobierno define los próximos pasos tras una reunión con expertos
title: El Gobierno define los próximos pasos tras una reunión con expertos
entradilla: El jefe de Gabinete, Santiago Cafiero, encabezará una reunión interministerial
  en la tarde de este lunes, en Casa Rosada, de la que también participará el comité
  asesor de epidemiólogos.

---
El Gobierno nacional analiza este lunes el avance de contagios por coronavirus en algunas zonas del país, con el objetivo de reforzar los controles y los protocolos sanitarios de cara al fin de semana largo turístico que se inicia el jueves.

El jefe de Gabinete, Santiago Cafiero, encabezará una reunión interministerial prevista para las 17, en Casa Rosada, de la que también participará el comité asesor de expertos epidemiólogos.

Del encuentro también tomarán parte los ministros de Salud, Carla Vizzotti; de Turismo, Matías Lammens; de Transporte, Mario Meoni; y del Interior, Eduardo De Pedro.

En sintonía, las carteras de Salud y Turismo mantendrán reuniones con las cámaras de turismo, de transporte, y también las aerolíneas, para "fortalecer los protocolos de cuidado" y se convocará a un Consejo Federal de Salud y de Turismo, conjunto, para "trabajar de manera consensuada y federal".

Anoche, Cafiero y Vizzotti, anunciaron medidas y recomendaciones para "minimizar las posibilidades de transmisión de la Covid-19 ante la preocupante situación epidemiológica por un aumento sostenido del número de casos".

 Ante ese panorama, decidieron que desde este lunes hasta el miércoles inclusive el sector público nacional quedará eximido de asistir al lugar de trabajo y cumplirá sus tareas de modo remoto o por teletrabajo, y se invitó a hacer lo mismo a todas las jurisdicciones y sector privado.

Asimismo, ante la proximidad de Semana Santa, dijeron que "es fundamental redoblar los esfuerzos para fortalecer los cuidados y sostener el turismo con cuidado, evitando las actividades de alto riesgo".

Respecto de la educación, los funcionarios anunciaron que se mantendrá la presencialidad, aunque aclararon que ello "no debe confundirse con las actividades sociales que puedan derivar del encuentro de los niños, niñas y adolescentes".