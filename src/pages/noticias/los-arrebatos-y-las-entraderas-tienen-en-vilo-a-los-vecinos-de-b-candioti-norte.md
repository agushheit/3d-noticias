---
category: La Ciudad
date: 2021-04-24T06:00:15-03:00
thumbnail: https://assets.3dnoticias.com.ar/CHOREO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Los arrebatos y las entraderas tienen en vilo a los vecinos de B° Candioti
  Norte
title: Los arrebatos y las entraderas tienen en vilo a los vecinos de B° Candioti
  Norte
entradilla: 'Advierten que la mayoría de los robos se efectúan en motos y que ocurren
  a toda hora. Piden mayor presencia policial y una poda sistematizada para mejorar
  la iluminación.  '

---
"Precaución. Zona de robos: entraderas y arrebatos", expresa entre signos de exclamación un mensaje que distribuyen entre los vecinos de barrio Candioti Norte y que pegaron en las cuadras del barrio. En el comunicado aconsejan: "Mantener puertas y ventanas todo el tiempo cerradas, incluso estando dentro de la casa", además a los transeúntes les advierten que caminen sin objetos en la mano, "y si es posible, acompañados. Evitar transitar a la siesta y a la noche, aunque no hay horario seguro".

El Litoral dialogó con Orlando, vecino y comerciante del barrio, quien mencionó la vasta cantidad de robos que sufren tanto en la vía pública como en los domicilios. "El mensaje es para que propios y extraños estén más atentos, y los vecinos refuercen la seguridad de sus casas", resaltó Orlando acerca de la intención del panfleto que colocaron en la zona.

"La modalidad más tradicional es la del motochorro, que con la asistencia de un compañero arrebatan a quienes caminan por la calle. También llamó la atención dos robos muy seguidos, donde los domingos a la siesta, cuando no anda nadie, saltaron las rejas de una casa y se metieron a la casa; lo mismo pasó en calle Lavalle, donde esperaron que la señora de la casa se vaya y se metieron. Eso te da a entender que hay un estudio previo", aseveró el vecino.

Más allá del daño material por los robos, se suma el daño psicológico y físico que sufre la víctima. "Han estropeado gente. Hace un tiempo le robaron a una mujer de 80 años, que tenía una vida social activa y por robarle la hicieron derrapar varios metros con la moto, le quebraron la cadera, se deprimió y al tiempo falleció", lamentó Orlando.

**Más presencia policial y alarma comunitaria**

El vecino mencionó que en los últimos días notaron mayor patrullaje, pero advierte que no alcanzan. "Pasa dos o tres veces por la noche, pero antes no pasaban nunca", sostuvo.

Para atacar el problema de raíz, los vecinos piden que se incrementen los operativos de motos, porque entienden que los robos, en su mayoría, se cometen por motochorros. "Cuando pedimos los operativos, solo los hicieron por bulevar, y una persona que está sin patente y tiene una situación irregular, no va a pasar por bulevar. Por eso pedimos que se hagan de forma esporádica y en distintos lugares", reclamó Orlando.