---
category: Agenda Ciudadana
date: 2021-03-17T06:10:55-03:00
thumbnail: https://assets.3dnoticias.com.ar/amsafe.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de LT10
resumen: Amsafe valora el diálogo pero espera una nueva propuesta
title: Amsafe valora el diálogo pero espera una nueva propuesta
entradilla: El ministro de Trabajo convocó a los docentes públicos para este miércoles.
  Desde el gremio esperan que el encuentro lleve a una oferta salarial que las expectativas
  de los docentes.

---
El ministro de Trabajo, Empleo y Seguridad Social de la Provincia Juan Manuel Pusineri convocó a Amsafe para este miércoles 17 de marzo a las 10:00. 

De esta forma, el funcionario santafesino respondió al pedido público del gremio docente. En conferencia de prensa, la secretaria general Sonia Alesso había reclamado al gobierno “voluntad de diálogo”. “La resolución del conflicto no es vía amenazas”, dijo. 

Pusineri salió a responder que “a pesar de las medidas dispuestas por el gremio, el gobierno siempre ha demostrado tener vocación de diálogo y de resolución de problemas”. 

“Vamos a continuar haciendo lo mismo con todas las entidades sindicales que discuten paritarias en el sector público. De hecho, en la aceptación de la propuesta, las organizaciones que dieron su anuencia a la oferta han planteado diferentes cuestiones para seguir conversando”, argumentó el ministro.

Por su parte, Rodrigo Alonso manifestó que valoran la convocatoria “entendemos que la solución es poder discutir”, pero dejó en claro que esperan “que el resultado de la discusión sea una nueva propuesta paritaria” para someter a votación y que cubra las expectativas de los docentes.

La convocatoria se da en medio de una fuerte tensión entre las partes, generada por el rechazo de Amsafe a la propuesta salarial y la decisión de iniciar un plan de lucha que impidió el inicio normal de clases. Ante esto, la Provincia ratificó el ofrecimiento y hasta sugirió que no se descarta descontar los días no trabajados a los maestros.