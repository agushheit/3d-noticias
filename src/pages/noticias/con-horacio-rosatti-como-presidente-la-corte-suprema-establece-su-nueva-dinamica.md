---
category: Agenda Ciudadana
date: 2021-11-01T06:15:53-03:00
thumbnail: https://assets.3dnoticias.com.ar/horaciorosatti.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Con Horacio Rosatti como presidente, la Corte Suprema establece su nueva
  dinámica
title: Con Horacio Rosatti como presidente, la Corte Suprema establece su nueva dinámica
entradilla: 'A partir de ahora, el máximo tribunal -integrado por Rosatti, Carlos
  Rosenkrantz, Juan Carlos Maqueda y Ricardo Lorenzetti- tendrá su acordada semanal
  sin un día fijo. '

---
La Corte Suprema entrará la próxima semana en una nueva dinámica sin Elena Highton de Nolasco, a quien el Ejecutivo le aceptó oficialmente la renuncia el último viernes a través del decreto 747, y con un Horacio Rosatti ya acomodado y dispuesto a "modernizar" y "flexibilizar" al más alto tribunal de justicia del país.  
  
Como primera medida, las reuniones de acuerdos entre los ministros que por la acordada 38 de 1990 se celebraban sólo los martes, tendrán ahora frecuencia semanal pero sin un día fijo, es decir que los ministros se reunirán para discutir sus posturas sobre los expedientes, o firmar sus votos, cuando lo crean conveniente o el presidente Rosatti así lo considere.  
  
En la Corte aseguran que no hay ninguna intención detrás de este cambio más que la necesidad de acabar con "costumbres de otra época" y dotar al tribunal de los reflejos necesarios para funcionar de manera eficiente.  
  
Los acuerdos podrán ser presenciales o virtuales, ya que la firma electrónica con token de seguridad admite que se ratifiquen las decisiones desde cualquier lugar en el que se encuentren los distintos ministros del tribunal.  
  
No obstante los métodos, la principal preocupación de los jueces es revertir cierta idea instalada de "inacción" de la Corte en los últimos años que, según evalúan en el cuarto piso del Palacio de Tribunales, es más un relato periodístico que afecta a todo el sistema de Justicia y que está influenciado por la polarización político que agudiza las críticas a las instituciones, ya que por el contrario -plantean desde el tribunal- la actividad productiva se amplió.

> En 2019, la Corte procesaba unas 250 decisiones por semana, entre fallos, acordadas, resoluciones y otras medidas; este año ese promedio fue creciendo hasta llegar a unas 600 decisiones semanales

  
  
En cuanto al perfil del tribunal, en la Corte consideran que esa discusión todavía está pendiente, en particular la definición de si debe ser, como hasta ahora, la última instancia de la Justicia y procesar por año miles de expedientes que los tribunales inferiores pueden tomar o no, o si por el contrario se debe limitar a unas pocas causas cuya definición siente jurisprudencia para todo el sistema judicial.  
  
Los supremos saben que un eventual cambio de estructura y función de la Corte debe ser decidido en el Congreso y como producto de un acuerdo político: por esa razón, para el corto plazo no hay expectativa de que pueda ser posible una reforma, con lo cual los esfuerzos se centran en dinamizar la salida de los fallos.  
  
Un ejemplo de ello son los temas pendientes de resolución en procesos por delitos de lesa humanidad contra represores, cuyo análisis se hizo más lento con la llegada de Carlos Rosenkrantz a la presidencia.  
  
La llamada Comisión Interpoderes, creada por la misma Corte e integrada por los organismos de Derechos Humanos y el Gobierno nacional para supervisar el juzgamiento de los delitos imprescriptibles del terrorismo de Estado, protestó en mayo del año pasado por lentitud en la tramitación y las pocas definiciones tomadas en la materia, a lo que el máximo tribunal respondió apurando los expedientes.  
  
La Corte Suprema, tal como estuvo integrada hsta el viernes pasado, el último día de Elena Highton de Nolasco en ese tribunal.

  
La semana pasada, que fue la última de Highton en el tribunal, la Corte resolvió dieciséis expedientes mayormente de sentencias apeladas por represores, solicitudes de arresto domiciliario e incidentes procesales.  
  
De todos modos, como saben en la Corte, la manta de la Justicia es siempre corta y los temas pendientes muchos; por citar un ejemplo, el pedido de que el máximo tribunal se pronuncie sobre el plazo de prescripción de los abusos en las infancias.  
  
Si bien el tratamiento y las penas para los delitos los fijan los legisladores, la misma Oficina de la Mujer de la Corte y la Unidad Fiscal Especializada en Violencia contra las Mujeres (UFEM) que encabeza Mariela Labozetta reclaman que la Corte falle y establezca un precedente que luego puedan aplicar los tribunales inferiores.  
  
Desde la UFEM, por caso, advierten que el riesgo de la prescripción penal en el delito de abuso en las infancias es que "genere impunidad" dado que muchos abusos fueron cometidos por las personas responsables de niños y niñas abusadas, y porque, como advirtió Labozetta el viernes a través de un hilo de Twitter, "a las víctimas les lleva tiempo entender que fueron abusadas".  
  
Con la intención de evitar que los abusos queden impunes por el paso del tiempo, el Congreso sancionó en 2011 y 2015 dos leyes (la 26705, de Prescripción de los delitos sexuales contra menores, y la 27206, de Respeto de los Tiempos de las víctimas) en las que se estableció que la prescripción queda suspendida hasta que la víctima alcance la mayoría de edad y se extendió el inicio del proceso para formular la denuncia.  
  
Lo que plantean desde la UFEM es qué hacer con los casos de abuso sexual anteriores a la sanción de ambas leyes, por lo cual plantean que para evitar que "un enorme número de casos quede impune" se considere que no prescribieron en base a los tratados internacionales a los que adhirió la Argentina que obligan a proteger los derechos de niños y adolescentes y que tienen jerarquía superior al principio de la ley más benigna.  
  
Otra propuesta es que por la gravedad de los hechos denunciados se realicen "juicios de la verdad", que no concluyan con una sanción penal pero que igual los casos de abuso sexual sobre las infancias sean juzgados en los Tribunales al implicar graves violaciones a los derechos humanos.  
  
Rosatti, en particular, y la justicia en general, tomó nota de la urgencia de las cuestiones de género en el sistema, tanto de la formación de los magistrados y magistradas en una perspectiva igualitaria, como de la adecuación de las leyes, y por ese motivo desde que llegó a la presidencia tomó contacto con las segundas líneas de la Oficina de la Mujer para darle continuidad a las políticas de género en el Poder Judicial.  
  
La productividad, el género, el escenario político y la percepción social del tribunal, son algunos de los desafíos que encarará Rosatti en la nueva etapa con un presupuesto importante para la estructura de la Corte pero magro y siempre insuficiente para todos los demás tribunales.  
  
Quizás allí radique el verdadero desafío para la cabeza del Poder Judicial: cómo administrar en un sentido superador una justicia pobre desde una Corte Suprema rica.