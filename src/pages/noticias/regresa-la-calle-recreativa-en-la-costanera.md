---
category: La Ciudad
date: 2021-09-25T06:15:03-03:00
thumbnail: https://assets.3dnoticias.com.ar/COSTANERA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Regresa la calle recreativa en la Costanera
title: Regresa la calle recreativa en la Costanera
entradilla: 'La avenida Alte. Brown, desde bulevar Muttis hasta Salvador del Carril,
  será el escenario para una nueva propuesta. Será este sábado de 14 a 18. '

---
Este sábado, de 14 a 18, la Av. Alte. Brown desde bulevar Muttis hasta Salvador del Carril, será el escenario de una nueva Calle Recreativa. Esta vez será en el marco de la Semana de la Movilidad Sustentable, que se desarrolla desde el 19 al 26 de septiembre, y por eso se proponen actividades para promover la caminabilidad y el uso de medios de movilidad seguros y amigables con el medio ambiente, y que sean saludables para la población.

Cecilia Leguizamón, directora de Movilidad de la Municipalidad realizó la invitación a todos los santafesinos y santafesinas a llegarse hasta la avenida Costanera y que lo hagan “con las bicicletas o caminando y con los chicos para disfrutar de este espacio que se va a dar allí”, dijo y luego agregó: “La intención es que la gente no se amontone en un determinado lugar, por eso es un espacio extenso, comienza en bulevar Muttis y llega hasta Salvador del Carril, son varias cuadras con muchas propuestas y actividades deportivas”.

Por su parte, Guillermo Álvarez, subsecretario de Convivencia Ciudadana de la Municipalidad, comentó: “La intención es recuperar una marca que tenía la gestión y que tomaba la Secretaría de Control y Convivencia Ciudadana pero que ahora abarca otras áreas del municipio como la dirección de Movilidad, integrantes del Imusa con juegos y adopción de animales, también del Cobem realizando concientización y capacitación de RCP y obstrucción de las vías áreas, entre otras alternativas”.

**Distintas opciones**

Habrá una escuela de ciclismo que se pensó para aquellas personas que no saben andar en bicicleta, los profesores de Educación Física de la Dirección de Deportes plantean una actividad para enseñarles. Habrá disponible bicicletas del sistema “Subite a la Bici” y también podrán acercarse personas con sus propias bicis.

Una de las opciones será “Dejá las rueditas”. La propuesta es para que niños y niñas que utilicen “rueditas” como apoyo a su circulación en bici puedan aprender a hacerlo sin ellas, y la intención es que los profes y padres acompañen a las infancias.

A estas actividades, se sumarán propuestas deportivas-lúdicas que se desarrollarán frente a la Dirección de Deportes. Entre ellas habrá básquet, vóley, fútbol tenis, ajedrez y hockey con escobas.

Además de las actividades deportivas, se sumarán las Naranjitas con el programa de Educación y Seguridad Vial con un gazebo para concientizar sobre la importancia de la calle segura, mejores prácticas y el cumplimiento de las normas de tránsito. Se realizarán actividades y juegos de la temática para niños.

Es importante destacar que personal de Tránsito realizará los cortes de calles necesarios para realizar esta actividad. Personal caminante de GSI recorrerá en forma permanente la zona de corte y de presencia del público asistente.

**Los animales, como protagonistas**

El Programa Cuidado Responsable de Animales de Compañía tiene el propósito de interactuar mediante la oralidad y la práctica con el público en general sobre el conocimiento y cuidado de los animales; y en este marco, se instalarán integrantes del Instituto Municipal de Salud Animal (Imusa).

Con el objetivo de educar como una herramienta importante en la salud pública, se brindarán conceptos básicos sobre los animales de compañía y las principales enfermedades zoonóticas. En esta intervención, además el Imusa realizará actividades variadas como juegos, stand de adopciones responsables, campaña de vacunación antirrábica y desparasitación, entre otras.

Entre los juegos, se pueden mencionar: pasear al perro con collar y correa, fomentando la responsabilidad del paseo con el animal atado y no libre en la vía pública; peinar al perro con cepillos, cardinas o peines; sogas y pelotas para jugar interactuando los niños con los animales; pintar animalitos, tatetí perro-gato, completar línea de puntos, rompecabezas, cuento, sopas de letras, crucigrama, laberinto, rompecabezas, entre otros.