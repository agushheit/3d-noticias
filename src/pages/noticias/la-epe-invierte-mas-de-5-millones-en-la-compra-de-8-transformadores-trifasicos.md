---
category: Estado Real
date: 2021-02-01T06:46:53-03:00
thumbnail: https://assets.3dnoticias.com.ar/epe.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: La EPE invierte más de $ 5 millones en la compra de 8 transformadores trifásicos
title: La EPE invierte más de $ 5 millones en la compra de 8 transformadores trifásicos
entradilla: La adquisición se enmarca en el programa EPE Social. Se presentaron siete
  ofertas.

---
La Empresa Provincial de la Energía (EPE) abrió los sobre con las ofertas económicas para la compra de 8 transformadores trifásico de distribución, en el marco del  programa EPE Social. La inversión de la empresa es de $ 5.646.991,77.

En la oportunidad, se presentaron siete empresas oferentes. La primera fue Artrans SA, que cotizó por $ 6.399.302,80; la segunda Argeltra SA, por $ 5.707.144,08; la tercera Anif SA, por $ 5.998.357,20; la cuarta Inelpa Transformadores SA, por $ 5.690.262,16; la quinta Fohama Electromecánica SA, por $ 6.183.530,76; la sexta Tadeo Czerweny SA, por $ 5.436.927,84; y la séptima firma fue Mayo Transformadores SRL, que ofertó $ 5.655.443,20.

Al respecto, el presidente de la EPE, Mauricio Caussi, explicó que “los materiales estarán destinados a obras de infraestructura en barrios populares de los centros urbanos y tienen como finalidad mejorar la calidad de vida de los vecinos, para lo cual es vital la prestación de servicios públicos como la energía eléctrica”.

Caussi remarcó que “EPE Social es uno de los programas centrales de esta gestión, que tiene como propósito impulsar obras de infraestructura eléctrica en esas zonas; realizar trabajos de mantenimiento que requieran tendido eléctrico; promover la participación de las organizaciones barriales en el proceso; y efectuar la normalización técnica y comercial con tarifa social provincial”, detalló.

Los transformadores se utilizarán en un nuevo tendido eléctrico de baja tensión, con cables preensamblado, subestaciones transformadoras y conexiones seguras a las viviendas. El objetivo de estos trabajos es mejorar el servicio eléctrico.