---
category: La Ciudad
date: 2021-12-06T06:15:21-03:00
thumbnail: https://assets.3dnoticias.com.ar/BASURAATOPE.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Este lunes el Concejo define qué se hará con la basura: los santafesinos
  desechan 1 kilo por día'
title: 'Este lunes el Concejo define qué se hará con la basura: los santafesinos desechan
  1 kilo por día'
entradilla: 'El municipio envió al cuerpo un proyecto de ordenanza para prorrogar
  el relleno sanitario por otros siete años, donde ingresan 600 toneladas de basura
  por día

'

---
Este lunes, en una nueva sesión del Concejo Municipal se tratará el proyecto de ordenanza para prorrogar la concesión del relleno sanitario, el cual la Municipalidad de Santa Fe envió al cuerpo para ser aprobado. Es así como en el recinto de calle Salta se decidirá que pasará en torno al tratamiento de desechos en la ciudad de aquí a los próximos siete años, plazo por el cual se deja explícito la extensión de la concesión.

Al predio ubicado a orillas de la circunvalación oeste en la desembocadura de la calle Hernandarias llegan actualmente 15.000 toneladas de basura por mes, más del doble de cuando comenzó a funcionar hace ya 11 años, momento en el que se pensó para 7.000 toneladas por mes. Este número es posible por las 600 toneladas de desechos que ingresan al relleno cada día.

 Edgardo Seguro, secretario de Medioambiente del municipio, se refirió a la ordenanza propuesta: "Presentamos un proyecto muy abarcativo, con la ampliación de la operación en toda su magnitud teniendo en cuenta que la infraestructura existente en el relleno es muy importante y hay que aprovecharla al máximo, sobre todo habiendo terrenos suficientes como para poder seguir prolongando la vía del relleno".

"El mayor problema fue a principios de la gestión cuando nos encontramos con el relleno sanitario llegando al final de su vida útil y solo con cuatro hectáreas de tierras municipales. La pandemia y el arreglo de la deuda que quedó con las tres empresas de residuos nos llevaron a estar esta situación de tener que avanzar con un pedido de prórroga", continuó el funcionario de la Municipalidad de Santa Fe.

La prórroga para invertir en el relleno sanitario extendiendo su funcionamiento contempla una posibilidad de entre siete y nueve años. Según Seguro, "el cálculo normal establece la vida útil en siete años, pero si está bien operado como es el caso del relleno actual podría tener un poco más de vida".

**600 toneladas por día**

Un dato revelador que grafica la importancia de un tratamiento sustentable de los desechos radica en que en la ciudad de Santa Fe cada habitante produce 1 kg de basura por día en promedio. Si bien el índice es mayor al de localidades más pequeñas, el número es menor al registrado en ciudades más grandes como Rosario, donde se desechan 1.200 kg diarios por persona y Buenos Aires, que llega a los 2 kg por habitante debido a un mayor uso de material descartable.

De las 600 toneladas diarias de basura que llegan al relleno sanitario de circunvalación oeste cada día, el 79% corresponde a la ciudad de Santa Fe. Luego, un 11% corresponde a desechos de otras localidades del departamento La Capital y el restante 10% lo completan los residuos de empresas del área metropolitana que traen los residuos asimilables a urbanos.

**Planta de compostado**

Otro de los puntos destacados que contiene el proyecto de ordenanza es la planificación de invertir en una planta de compostado, destinada a transformar residuos en material estabilizado que se utilizará para la tapa del relleno sanitario. Según las estimaciones del municipio, esta planta podrá tratar 100 toneladas diarias de residuos para reconvertirlas en 50 toneladas de material estabilizado

"Lo mas importante está establecido en proyectos a largo plazo que aparecen en la ordenanza, como la planta de compost o la expropiación de los terrenos, para que las próximas gestiones tengan mayor libertad en la forma en la que encaran este tipo de cosas. Para la planta de secos está planificada la mejora y la restitución de su equipamiento, en estos diez años ha sufrido muchísimo por falta de mantenimiento", destacó el funcionario municipal.