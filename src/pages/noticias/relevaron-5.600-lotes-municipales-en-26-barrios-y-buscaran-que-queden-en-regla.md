---
category: La Ciudad
date: 2021-04-05T06:00:05-03:00
thumbnail: https://assets.3dnoticias.com.ar/unnamed.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Relevaron 5.600 lotes municipales en 26 barrios y buscarán que queden "en
  regla"
title: Relevaron 5.600 lotes municipales en 26 barrios y buscarán que queden "en regla"
entradilla: El municipio intenta generar "más celeridad" en los procesos de escrituración
  en terrenos que le son propios, y sobre los cuales familias edificaron sus viviendas

---
En la ciudad de Santa Fe hay 5.600 lotes municipales sin escriturar, de los cuales 2.800 están aún sin subdividir y mensurar, todo ello identificado en 26 barrios del ejido urbano. Y es el dato -previo relevamiento- que sirvió de base para que desde el municipio se decida poner "en mayor escala" el programa de regularización dominial.

¿Qué implica este plan? Que aquellas familias que edificaron sus viviendas sobre terrenos municipales sin tener el título de propiedad sobre los lotes, puedan hacerlo y entrar en la formalidad administrativa. La escritura no es un documento cualquiera: ese título de propiedad le da a una persona la garantía legal de que es propietario de su lote para poder, luego y con los papeles legales en mano, acceder a créditos sociales para ampliación, mejoramiento y refacción de su vivienda.

Desde la segunda mitad del año pasado y hasta la fecha, "se entregaron 150 escrituras (en San Agustín, San Lorenzo, Yapeyú y Nueva Pompeya, entre otros barrios del norte y noroeste). Además, ya tenemos subdivididos y mensurados 600 lotes municipales de los barrios Marcos Bobbio, 29 de abril y Barranquitas", le dijo a El Litoral Paola Pallero, directora ejecutiva del Ente Autárquico Santa Fe Hábitat.

"Si bien nos encontramos en 2020 con una pandemia de por medio que dificultó los relevamientos socio-ocupacionales, se avanzó con aquellas familias que ya estaban adelantadas en los expedientes administrativos (por ejemplo, de aquéllas que ya habían abonado el avalúo fiscal)", añadió. En este sentido, un paso adelante fue la firma de convenios con los colegios de profesionales (de Escribanos de la 1° Circunscripción y de Agrimensores). El trabajo de estos profesionales permite agilizar los procesos administrativos.

La proyección "de máxima" es poder regularizar la situación de esos 5.600 lotes municipal de 26 barrios de la ciudad (ver mapa), pero será muy difícil lograr que se escrituren todos: "Ese número es una medida de 'lo ideal' -aclaró la directora-. Ocurre que puede haber cambios de familias (divorcios, por ejemplo); o que no tengan (esas familias) la voluntad de escriturar, o que no paguen nunca el avalúo fiscal. La proyección es poder escriturar todo, pero el número efectivo, por las razones antes mencionadas, será mucho menor", estimó.

**Cómo es el proceso**

El primer paso del plan de regularización dominial son los relevamientos socio-ocupacionales sobre cada una de las familias que habitan sin regularización de lote municipal. Desde la Agencia se trabaja con las redes de instituciones y con referentes territoriales de cada barrio. "Se hicieron a la fecha mil relevamientos socio-ocupacionales en Agustín, San Lorenzo, Yapeyú, Barranquitas, Liceo Norte y Colastiné Sur", afirmó Pallero.

Luego, se realizan los legajos con la documentación de cada familia que cumplimenten los requisitos establecidos en la Ordenanza N° 11.631 (de Regularización Dominial). En este aspecto, cada grupo familiar debe acreditar la posesión efectiva, ininterrumpida y pacífica de ese lote, y que sea de vivienda única y permanente (es decir, que no tenga otra vivienda).

El proceso continúa con la confección de los planos de mensura, subdivisión y loteo, y la confección de los legajos que serán remitidos al Colegio de Escribanos a los fines de que, finalmente, se otorguen las escrituras correspondientes. "El municipio toma a su cargo los honorarios profesionales de agrimensores y escribanos. Lo que se les exige a los vecinos es el pago del avalúo fiscal del lote", expresó Pallero. El avalúo tiene un valor social, aseguró la funcionaria. "Debe tenerse en cuenta que hablamos de escrituras sociales de barrios muy vulnerables".

Este proceso de regularización dominial forma parte de las políticas enmarcadas en el Plan Integrar Santa Fe, que el municipio puso en marcha para generar condiciones de integración social y urbana de calidad, particularmente en las zonas más vulnerables de la ciudad.

![](https://assets.3dnoticias.com.ar/TERRENOS.jpg)