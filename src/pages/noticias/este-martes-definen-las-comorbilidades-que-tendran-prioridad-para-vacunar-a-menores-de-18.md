---
category: Agenda Ciudadana
date: 2021-07-27T06:15:56-03:00
thumbnail: https://assets.3dnoticias.com.ar/MODERNA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Este martes definen las comorbilidades que tendrán prioridad para vacunar
  a menores de 18
title: Este martes definen las comorbilidades que tendrán prioridad para vacunar a
  menores de 18
entradilla: A partir de las ocho se reunirá el Consejo Federal de Salud que congrega
  a todos los ministros del país. Establecerán quienes serán los primeros en recibir
  la vacuna Moderna.

---
Desde las 8 de este martes sesionará el Consejo Federal de Salud, encuentro que congrega a todos los ministros del área del país. Se espera que en el cónclave se definan las comorbilidades que tendrán prioridad para vacunar a menores de entre 12 y 17 contra el coronavirus.

De esta manera, el Ministerio de Salud de la Nación -en consenso con las ministras y ministros de todo el país- buscará ampliar el alcance del Plan Estratégico de Vacunación contra la COVID-19 a adolescentes que tienen factores de riesgo.

Días atrás, la cartera sanitaria detalló en un comunicado que "los factores de riesgo que se evalúa incorporar pertenecen a enfermedades cardíacas, neurológicas, renales, respiratorias, obesidad, algunos tipos de discapacidad y otras condiciones de vulnerabilidad que se especificarán en la reunión" de este martes.

La decisión de inmunizar a esta parte de la población llegó la semana pasada, luego de la aprobación del ente regulador europeo del inmunizante del laboratorio estadounidense Moderna para esa franja etaria.

La ministra de Salud de la Nación, Carla Vizzotti, consideró que "esta recomendación es un paso muy importante que nos va a permitir vacunar en forma priorizada con esquema completo a jóvenes de entre 12 y 17 años con factores de riesgo, que podría alcanzar a 900.000 personas y para lo que se requeriría al menos 1.8 millones de dosis de esta vacuna permitiendo asegurar esquemas completos”.

En la provincia de Santa Fe, estiman que son 80 mil los jóvenes en esa franja etaria con comorbilidades y que aguardan por la vacuna.

Desde el domingo, el Gobierno nacional mantiene en reserva las 3.500.000 de vacunas Moderna donadas por los Estados Unidos,

**Sobre Moderna**

Su vacuna cuenta con ARN mensajero (ARNm), una molécula que lleva instrucciones para producir la proteína Spike presente en el SARS-CoV-2 causante de la Covid-19, con el objetivo de preparar al cuerpo para defenderse en caso de estar en contacto con el virus.

Los estudios presentados al Comité de Medicamentos Humanos de la EMA permitieron elevar un dictamen positivo vinculante recomendando la vacuna en niños de 12 a 17 años, especialmente en aquellos que presentan mayor probabilidad de desarrollar formas graves de la enfermedad dado que los beneficios superan los riesgos.

La semana pasada, en una videoconferencia entre autoridades de la cartera sanitaria nacional y los integrantes de la CoNaIn, representantes del laboratorio Moderna presentaron los resultados de sus ensayos clínicos, la efectividad de la vacuna contra las nuevas variantes, la estrategia de refuerzo, y los estudios llevados a cabo en adolescentes.

En ese marco, los miembros de la comisión se expresaron positivamente en recomendar el uso de vacunas con plataforma ARNm en personas de 12 a 17 años, priorizando aquellos que pertenecen a los grupos de riesgo.

Por otro lado, el Ministerio de Salud confirmó la inclusión de la vacuna de plataforma ARN mensajero de Moderna en el estudio prueba de concepto sobre la evaluación de esquemas heterólogos de vacunas contra el coronavirus.

Con esta decisión la Argentina se encuentra entre los pocos países que iniciaron esta estrategia como Estados Unidos, Canadá, China, Chile, Israel, Alemania, Francia, Italia, Hungría, Polonia, Rumania, Singapur y Dubái.