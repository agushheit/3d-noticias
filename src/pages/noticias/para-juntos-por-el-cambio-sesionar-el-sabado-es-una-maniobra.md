---
category: Agenda Ciudadana
date: 2021-03-25T07:31:03-03:00
thumbnail: https://assets.3dnoticias.com.ar/cantard.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de LT10
resumen: Para Juntos por el Cambio, sesionar el sábado es una maniobra
title: Para Juntos por el Cambio, sesionar el sábado es una maniobra
entradilla: Desde el oficialismo dicen que no se puede tratar Ganancias el jueves
  porque Juntos por el Cambio pidió tratar Biocombustibles, pero aseguran que el otro
  proyecto se podría tratar a posteriori.

---
Un nuevo cortocircuito se dio en Diputados entre oficialismo y oposición ya que se convocaron a dos sesiones distintas por Ganancias. Juntos por el Cambio solicitó que se cite a sesión especial para tratar Ganancias y régimen de Monotributo el jueves a las 14:00, y Massa dio lugar al pedido. Sin embargo, ya se había oficializado la convocatoria -a pedido del oficialismo- para el sábado a las 11:00, con el mismo temario.

Si bien Juntos por el Cambio es consciente que no reunirá quórum para habilitar su propia sesión por Ganancias, la decisión de solicitar la convocatoria tiene que ver con rebatir el argumento del oficialismo que instaló la idea de que la oposición no quería tratar el "alivio fiscal", dado que el Frente de Todos había elegido el mismo jueves como fecha para sesionar en torno a ese tema.

La oposición emitió un comunicado rechazando esa interpretación y aclarando que la sesión por biocombustibles había sido pedida tanto por la oposición como por el interbloque Federal con "diez días de antelación".

En ese sentido, destacaron que el oficialismo ya estaba advertido de la fecha elegida por la oposición para debatir Biocombustibles, y además había dejado trascender que la fecha para sesionar con Ganancias iba a ser este martes, y no el jueves. 

Con respecto a esta maniobra del kirchnerismo en la Cámara Baja, el diputado nacional santafesino, Albor Cantard, dijo por LT10 que si bien el debate por Ganancias es "interesante" calificó de "mamarracho" el modo en que se está manejando la presidencia de la Cámara baja.

Y argumentó que el tratamiento de la Ley de Biocombustible es muy importante para el sector productivo del país, principalmente para Santa Fe. La iniciativa ya tiene media sanción en el Senado, y según el legislador en Diputados "hay falta de iniciativa" para su debate.

Sobre la sesión del sábado, señaló que se encontraron de manera "sorpresiva y caprichosa" con una convocatoria para el sábado "de manera arbritraria".

De todos modos, confirmó la asistencia el sábado del sector para participar de la sesión, aunque insistió en que no tiene sentido cuando se puede debatir Ganancias luego de los otros temas.