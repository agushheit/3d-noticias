---
category: Agenda Ciudadana
date: 2021-03-08T19:00:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/sputnik.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Salud comienza la distribución de 375 mil dosis del segundo componente de
  Sputnik V
title: Salud comienza la distribución de 375 mil dosis del segundo componente de Sputnik
  V
entradilla: |2-


  La logística de las partidas fue preparada este domingo y el Ministerio de Salud las distribuirá desde el lunes hasta el miércoles entre las provincias y la Ciudad Autónoma de Buenos Aires de manera proporcional.

---
Las provincias y la Ciudad Autónoma de Buenos Aires comenzarán a recibir a partir de este lunes un total de 375.805 dosis del segundo componente de la vacuna Sputnik V contra el coronavirus, con la finalidad de completar los esquemas de vacunación del Gobierno nacional.

 Según informó Presidencia, la ministra de Salud, Carla Vizzotti, explicó que "la entrega de las dosis comenzó a prepararse logísticamente este domingo para ser enviadas, a partir de mañana (por el lunes), a cada uno de los destinos establecidos".

Así, "sumadas a la partida de 406.800 vacunas chinas Sinopharm que llegó en los últimos días y las 2.972.260 registradas esta tarde en el Monitor Público, las dosis distribuidas se acercan a las 3,5 millones", añadió.

Las vacunas serán enviadas este lunes y distribuidas entre los días martes y miércoles entre las provincias de manera proporcional.

En tanto, el Monitor Público de Vacunación de la cartera sanitaria registraba hasta el mediodía del domingo un total de 1.539.482 vacunas aplicadas en todo el territorio nacional.  
  
En ese contexto, 1.195.854 personas recibieron la primera dosis y 343.628 fueron inoculadas con ambas.