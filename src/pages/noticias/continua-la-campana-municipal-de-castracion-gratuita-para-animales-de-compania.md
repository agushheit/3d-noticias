---
category: La Ciudad
date: 2021-03-30T07:19:57-03:00
thumbnail: https://assets.3dnoticias.com.ar/castracion.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad Santa Fe
resumen: Continúa la campaña municipal de castración gratuita para animales de compañía
title: Continúa la campaña municipal de castración gratuita para animales de compañía
entradilla: Los turnos son programados y pueden obtenerse de manera on line. En dos
  semanas y media se realizaron 3.800 atenciones, entre castraciones, vacunación y
  consultas varias.

---
Durante el mes de abril, la Municipalidad continuará con la campaña de castración móvil, destinada a animales de compañía. También se ofrece servicio de vacunación y se presta atención veterinaria, todo de manera gratuita. En dos semanas y media -desde el 3 al 19 de marzo- se llevan realizadas 1.436 castraciones, 1.921 vacunaciones, y 469 consultas y atenciones.

Los turnos son programados y pueden obtenerse en la web oficial www.santafeciudad.gov.ar/turnos. Allí se debe buscar el apartado denominado “Castraciones”, se elige la opción “Solicitar un nuevo turno” y se completa el formulario con los datos pedidos. Posteriormente se elige el día, el horario y la sede a la cual concurrir, debido a que las prestaciones están disponibles en los ocho distritos, según el siguiente cronograma:

**Del 5 al 16 de abril:**

* Distrito Noreste: Vecinal La Esmeralda, La Esmeralda 2471
* Distrito de la Costa: Vuelta del Paraguayo, salón parroquial
* Distrito Centro/Este: Vecinal Fomento 9 de Julio, Pedro Ferre 2928

**Del 5 al 23 de abril:**

* Distrito Oeste: Vecinal Ciudadela Norte, Gorostiaga 3955
* Distrito Noroeste: Club Cabal, Servando Bayo 6730
* Distrito Norte: Vecinal Pompeya Oeste, San José 7756
* Distrito Suroeste: Vecinal Santa Rosa, Tucumán 4550

**Del 19 al 30 de abril:**

* Distrito de la Costa: La Casa del Nogal, Ruta 1 kilómetro 2
* Distrito Centro/Este: Club Echeverría, Sarmiento 4068
* Distrito Noroeste: APAM, Alfonsina Storni 2500

**Del 26 de abril al 7 de mayo:**

* Distrito Oeste: Vecinal Barranquitas Unión y Progreso, López y Planes 4069
* Distrito Noroeste: Vecinal Ceferino Namuncurá, 12 de octubre 9501
* Distrito Norte: Vecinal Nueva Pompeya, French 3878
* Distrito Suroeste: Vecinal Barrio Roma, Tucumán 3957

En todos los casos, la atención es de 8 a 12 horas.

Cabe mencionar que el Instituto Municipal de Salud Animal (IMUSA) continúa con la atención de lunes a viernes en las sedes del Parque Garay, de 8 a 17 horas; y del Jardín Botánico, de 8 a 13 horas. En ambos casos, el turno se obtiene a través de la web www.santafeciudad.gov.ar/turnos.

Se recuerda a los vecinos y las vecinas que, en el marco de la pandemia por Covid-19, quienes lleven el animal a la consulta deben utilizar el cubrebocas. Antes de ingresar al lugar, se tomará la temperatura y se proveerá de alcohol en gel. Dentro de los espacios, se garantiza el cumplimiento de los protocolos correspondientes.

**Recomendaciones**

Las castraciones están destinadas a perros y gatos de ambos sexos que tengan más de 6 meses de edad. Los animales deben ser llevados por una persona mayor de edad y cumplir con un ayuno de 12 horas antes de la intervención. También es importante que no estén enfermos ni preñados, incluso que hayan pasado 60 días de posparto, como mínimo.

La castración temprana, es decir antes del primer celo, disminuye en un 90% las posibilidades de la presentación de tumores hormona-dependientes, que son muy frecuentes en hembras. Con esta intervención, se evita además el contagio de enfermedades de trasmisión sexual.

Vale aclarar que el procedimiento no incide en el temperamento o el instinto del animal, por lo cual, sigue protegiendo su territorio como lo hacía habitualmente. También evita peleas entre machos y los extravíos de estos, en busca de una hembra en celo.