---
category: Estado Real
date: 2020-12-01T11:24:30.000+00:00
thumbnail: https://assets.3dnoticias.com.ar/OPERATIVO-FIESTASS.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: 'Operativo Fiestas Seguras: acciones de prevención en centros comerciales'
title: 'Operativo Fiestas Seguras: la provincia planifica acciones de prevención en
  centros comerciales'
entradilla: “Vamos a reforzar patrullaje con caminantes, motorizados, y con más policías
  presentes en las calles", explicaron desde el Ministerio de Seguridad.

---
El gobierno de la provincia de Santa Fe, a través del Ministerio de Seguridad, llevó adelante este lunes la Mesa Operativa Rosario, con el objetivo de evaluar las acciones realizadas hasta la fecha y profundizar, en base a los resultados, las medidas a tomar en el marco de las fiestas de fin de año.

En este sentido, se repasó el Operativo Fiestas Seguras, que tiene como objetivo “reforzar patrullaje con caminantes, motorizados, y con más policías presentes en calles y zonas comerciales en horarios donde la concentración de gente es más intensa, para que puedan tener tranquilidad a la hora de comprar", explicó el subsecretario de Prevención y Control Alberto Mongia.

"Algunos operativos son dinámicos y otros más estáticos, vamos a replicar estas herramientas en ciudades como VGG, Funes y alrededores de Rosario, donde ya se viene trabajando en las Mesas de seguridad con gobiernos locales, y ahora le sumamos Fiestas Seguras, para que se puedan movilizar al centro comercial de su localidad".

##   
 **Intervención Estratégica**

Los operativos tienen por objetivo lograr mayor seguridad en la población, incrementando la presencia policial sobre distintos sectores barriales de la ciudad mediante chequeos de personas y vehículos, en forma conjunta y simultánea con la colaboración de recursos y efectivos de la Dirección General de Policía de Acción Táctica (PAT), Dirección General de Policía Comunitaria, Dirección General de Seguridad Rural y Dirección General de Policía de Seguridad Vial.

En tanto, los operativos de Interceptación de Delitos Predatorios (IDP) establecen patrullas de prevención dentro de un esquema dinámico, que están en condiciones de alcanzar rápidamente puntos ubicados fuera de la planificación diaria. Las nuevas capacidades informáticas permiten generar mapas interactivos, actualizados minuto a minuto, de flujos de circulación de circuitos delictivos.

Del encuentro llevado a cabo este lunes en Rosario, participaron, la jefa de Policía Emilce Chimenti; la coordinadora de la subsecretaria de Prevención y Control, Priscila Villalobos; de la Central de Información Criminal, Carlos Portis; el jefe de la Unidad Regional 2, Daniel Acosta, y la jefa de Operaciones, Analía Herrera, entre otros.