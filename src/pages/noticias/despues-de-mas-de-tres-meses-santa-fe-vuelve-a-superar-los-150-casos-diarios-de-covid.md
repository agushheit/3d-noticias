---
category: La Ciudad
date: 2021-12-15T06:15:02-03:00
thumbnail: https://assets.3dnoticias.com.ar/covixho.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Después de más de tres meses Santa Fe vuelve a superar los 150 casos diarios
  de Covid
title: Después de más de tres meses Santa Fe vuelve a superar los 150 casos diarios
  de Covid
entradilla: 'Este martes se registraron 172 contagios, manteniendo la curva ascendente
  de casos positivos durante los últimos días. La provincia, en alerta por la variante
  Delta y la irrupción de Ómicron.

'

---
El respiro de los últimos meses en cuanto a la situación sanitaria en Santa Fe, con el notorio descenso de los casos diarios reportados podría estar llegando a su fin. En las últimas jornadas se está apreciando un repunte de los contagios registrados en cada reporte del Ministerio de Salud, en alerta frente al impacto que pueda tener la contagiosa variante Ómicron y la actual predominante variante Delta.

Respecto a la evolución de infectados en los últimos días las cifras distan de ser alarmantes, aun que se aprecian valores exponencialmente superiores cada vez con mayor asiduidad. Esto es así en la medida de que este martes 14 de diciembre se registró la mayor cantidad de casos positivos registrados en la provincia en los últimos tres meses, con 172 positivos. La anterior cifra corresponde al 9 de septiembre, cuando se habían confirmado 161 infectados.

A nivel nacional, las cifras se siguen acrecentando. Este martes se registró la mayor cantidad de nuevos contagios en 100 días, luego de que el Ministerio de Salud nacional haya confirmado 4.555 casos en todo el país.

Esto se da en el marco del regreso de los eventos masivos luego de lo que fueron largos meses de restricciones. Con la vuelta a las canchas, recitales, fiestas o eventos masivos los números de contagios se mantuvieron bajos por tres meses, lo que dio lugar a un estado de relajación general e incluso ya pensando en una posible post pandemia. La actualidad se muestra todavía con buenos números, aunque en franco ascenso.

Hablando de cifras registradas en la provincia hasta el momento, en total se confirmaron 472.703 contagios de Covid en toda la bota santafesina, 55.402 en la ciudad de Santa Fe. Respecto a las víctimas fatales, hasta el momento murieron 8.632 santafesinos a causa del Covid.

La dinámica de los contagios de Covid registrados muta mientras va pasando cada semana epidemiológica, con la coincidencia de que cada fin de semana es notoria la baja en la comunicación de casos debido a los pocos testeos realizados en estos días. Lo mismo sucede en días feriados.

Con la llegada de las fiestas se plantea el interrogante sobre el impacto que las reuniones y eventos festivos podrían alcanzar en el marco de un aumento de contagios de Covid. Otros países ya se vieron forzadas a volver a implementar restricciones a eventos masivos, puntualmente en Europa y Estados Unidos. Las autoridades sanitarias reiteran que el mayor peligro lo correrán quienes no estén vacunados, por una u otra razón.