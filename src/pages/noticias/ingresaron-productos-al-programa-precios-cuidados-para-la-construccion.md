---
category: Agenda Ciudadana
date: 2021-09-07T06:15:21-03:00
thumbnail: https://assets.3dnoticias.com.ar/construccion.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Ingresaron productos al programa Precios Cuidados para la Construcción
title: Ingresaron productos al programa Precios Cuidados para la Construcción
entradilla: Entre los rubros se encuentran aberturas, arena, sanitarios, cerámicos,
  yeso, grifería, ladrillo, pinturas, placas, productos de aislación e impermeabilización,
  así como de electricidad y de iluminación.

---
La Secretaría de Comercio Interior del Ministerio de Desarrollo Productivo de la Nación anunció la renovación de Precios Cuidados para la Construcción. El programa contará con 89 productos de primeras marcas en 13 rubros de obra fina. Esta nueva etapa contiene una actualización trimestral de los precios del 6,5%, que se mantienen fijos hasta el mes de noviembre de este año, inclusive.

Precios Cuidados para la Construcción fue lanzado en septiembre de 2020. Tiene una vigencia anual, con renovaciones trimestrales, fruto del acuerdo con empresas proveedoras y comercializadoras de insumos para la obra fina. Por su parte, el programa de obra fina se complementa con el de obra gruesa que consta de un acuerdo de precios de referencia de materiales e insumos.

Entre los rubros de productos con valores de referencia se encuentran aberturas, arena, sanitarios, cerámicos y porcelanatos, yeso, grifería, ladrillo, pinturas, placas de cemento y de yeso, productos de aislación e impermeabilización, productos de electricidad y de iluminación.

Participan los 30 proveedores más importantes del sector, como Ferrum, Roca, FV, Hidromet, Peirano, Saint Gobain, Durlock, Osram, Philips, Cerámica Alberdi y Cerámica Cañuelas, entre otras empresas. Asimismo, los consumidores pueden encontrar los productos del programa en más de 500 bocas de expendio en todo el país de las cadenas Easy, Sodimac y Blaisten, y distintas cadenas de pinturerías, como Prestigio, Rex, Sagitario, Pintecord, Universo, Giannoni, entre muchas otras.

Los precios de los artículos tienen el objetivo que los consumidores tengan disponible una orientación a la hora de realizar sus compras, por ese motivo los precios son valores de referencia y no de oferta.

Entre los productos que se destacan en esta nueva etapa del programa, se encuentran la placa de yeso a $ 745,60, la placa de cemento a $ 1.473,10 y pinturas Polacrin a $ 5.219,40 y Performa a $ 4.895,30. En cuanto a instalaciones eléctricas, resaltan las lámparas led a $ 2.44,10, el interruptor térmico unipolar a $ 313,30, el interruptor de combinación a $ 2.10,30 y el interruptor con tomacorriente a $ 445,80. Por su parte, en sanitarios y griferías se pueden conseguir, entre otros, grifería de bidet a $ 7.759,60, grifería de cocina a $ 9.824,50, depósito para inodoro a $ 9.398,20 y las cerámicas a $ 738,40.

Estos productos se pueden financiar con Ahora 12 en cada uno de los comercios adheridos. Los materiales y herramientas para la construcción están incluidos en los planes de 12, 18 y 24 cuotas del programa de fomento al consumo y la producción