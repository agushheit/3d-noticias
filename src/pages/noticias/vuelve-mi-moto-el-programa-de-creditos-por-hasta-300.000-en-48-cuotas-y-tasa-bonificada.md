---
category: Agenda Ciudadana
date: 2022-04-18T09:04:49-03:00
thumbnail: https://assets.3dnoticias.com.ar/motos.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Telam'
resumen: Vuelve Mi Moto, el programa de créditos por hasta $300.000 en 48 cuotas y
  tasa bonificada
title: Vuelve Mi Moto, el programa de créditos por hasta $300.000 en 48 cuotas y tasa
  bonificada
entradilla: El programa que regresa este lunes incluye 42 modelos de motos nacionales
  de 13 marcas participantes (Corven, Bajaj, Brava, Keller, Gilera, Honda, Mondial,
  Motomel, Suzuki, Guerrero, Okonoi, Zanella y Kiden).

---
Este lunes 18 de abril comienza una nueva etapa del programa Mi Moto, que permite el acceso a créditos por hasta $ 300.000 a través del Banco Nación (BNA) para la adquisición de motos de fabricación nacional, en 48 cuotas y con tasas bonificadas por parte del Ministerio de Desarrollo Productivo.  
  
Será la décimo segunda etapa del programa que estará habilitado **tanto para clientes como para no clientes del BNA** y que incluye **42 modelos de motos nacionales de 13 marcas participantes** (Corven, Bajaj, Brava, Keller, Gilera, Honda, Mondial, Motomel, Suzuki, Guerrero, Okonoi, Zanella y Kiden).  
  
El monto máximo a financiar por usuario será de **hasta $300.000, a un plazo único de 48 meses**, sistema de amortización francés, y alcanza a todos los usuarios, aunque el importe a financiar no incluye los costos y gastos adicionales accesorios a la operatoria (patentamiento, fletes, sellados, etc.).  
  
"Respecto a **la tasa de interés, para quienes cobren sus haberes a través del BNA, será de 19,50%, en tanto que para el resto de los usuarios será de 29,50%"**, detallaron a Télam desde el Banco nación.

> Como ejemplo de cuota, una moto de $150.000 pagaría una cuota inicial de $4.180,46 en el caso de un cliente que perciba sus haberes en el BNA, mientras que para el resto de los clientes la misma ascendería a $5.322,73.

  
  
La garantía es a sola firma y **se financia el 100% de la motocicleta.** Para iniciar la gestión del crédito hay que ingresar a través de la web del Banco Nación, donde el cliente será precalificado, luego podrá reservar la motocicleta mediante el link informado en el correo electrónico, lo cual permitirá el acceso a [**Tienda BNA.**](https://www.bna.com.ar/Personas)  
  
Una vez culminada la reserva deberá formalizar la solicitud del préstamo de forma presencial en la sucursal.  
  
Las modelos de motocicletas disponibles para el programa podrán ser consultadas en el sitio [**tiendabna.com.ar/mi-moto**](http://tiendabna.com.ar/mi-moto).  
  
El patentamiento de motos se ubicó en marzo en torno a las 45.000 unidades, con un crecimiento de 30% en relación al mismo mes de 2021, según datos de la Cámara de Fabricantes de Motovehículos (Cafam), mientras que en todo 2021 se patentaron 393.059 unidades, 44,9% más que en el 2020, cuando las restricciones para combatir la pandemia afectaron a toda la economía.