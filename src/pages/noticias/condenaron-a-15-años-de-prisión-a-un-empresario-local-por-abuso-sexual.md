---
layout: Noticia con imagen
author: "Por Marisa Lemos "
resumen: "Condena para Dolinsky "
category: La Ciudad
title: Condenaron a 15 años de prisión a un empresario local por abuso sexual
entradilla: El tribunal de juicio impuso sentencia para Alberto Hector Dolinsky
  de 71 años, quien deberá cumplir la pena bajo la modalidad domiciliaria. El
  juicio comenzó el mes pasado y fue suspendido en dos oportunidades.
date: 2020-11-18T12:55:03.118Z
thumbnail: https://assets.3dnoticias.com.ar/tribunales.jpg
---
El empresario maderero Alberto Héctor Dolinsky (71) fue condenado ese martes por la pena de 15 años de prisión de cumplimiento efectivo, bajo la modalidad domiciliaria, por el abuso sexual de una nena de 11 años cometido en enero del 2014, en el solárium y la habitación que alquilaba en un hotel céntrico de la ciudad de Santa Fe.

Los jueces del tribunal pluripersonal Sergio Carraro, Rosana Carrara y Rodolfo Mingarini, impusieron la elevada pena, tras catapultarlo como autor de los delitos de “abuso sexual gravemente ultrajante, agravado por ser cometido por dos personas”; y “promoción de la corrupción de menores, agravado por ser la víctima menor de 13 años de edad”.

La pena impuesta fue la misma que solicitaron los fiscales de la Unidad de Violencia de Género, Familiar y Sexual, Alejandra Del Río Ayala y Matías Broggi, en conjunto con el abogado demandante, José Ignacio Mohamad. Por su parte, la defensa de Dolinsky, a cargo de los Dres. Raúl Berizo y Germán Corazza, postuló la inocencia de su cliente, y se espera que recurra la condena, para tener así una nueva oportunidad en segunda instancia.

Para refrescar la memoria, y para los que no saben el motivo por el cual se lo acusa, **todo comenzó el mes de enero del año 2014,** donde (según el testimonio de la menor) Lindaci (pareja en ese entonces de su papá) la llevó a tomar un helado y se comunicó con el acusado. Cuando volvieron al hotel en la zona de las piletas le dieron de tomar whisky y la mujer se propasó  con la menor. Más tarde fueron al departamento de Dolinsky, donde se cometió ese aberrante hecho.

En dicha declaración, el padre recuerda esa noche ya que cuando se despertó en plena madrugada no vio a la niña ni a su cónyuge. En ese momento salió a buscarlas por todos lados.

Cabe destacar que, debido al hecho, la niña de 11 años dio un giro de 360° en su comportamiento: antes del suceso era una niña alegre y extrovertida y pasó a estar continuamente deprimida,  a tener problemas en su entorno escolar y con sus notas e incluso le tocó ser mamá a muy temprana edad.

La denuncia se radicó dos años después -en 2016-, cuando la víctima pudo contar lo que le ocurrió aquel verano.