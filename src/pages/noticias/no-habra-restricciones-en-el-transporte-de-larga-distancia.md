---
category: Agenda Ciudadana
date: 2021-01-09T11:42:13Z
thumbnail: https://assets.3dnoticias.com.ar/090121-transporte.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: '3DNoticias - Fuente: Télam'
resumen: No habrá restricciones en el transporte de larga distancia
title: No habrá restricciones en el transporte de larga distancia
entradilla: El titular de la CNRT, José Ramón Arteaga, dijo que se intensificarán
  los controles. Además, aseguró que la demanda de pasajes es muy inferior a la esperada
  y que «el factor de ocupación no llega ni al 50 por ciento».

---
El titular de la Comisión Nacional de Regulación de Transporte, José Ramón Arteaga, dijo que «por ahora, no habrá restricciones en el transporte de larga distancia en horario nocturno» y destacó que, debido al aumento de contagios de casos por coronavirus, los controles se intensificarán.

«La autoridad nos pide intensificar los controles, pero no restringir. **Por ahora no habrá restricciones en el transporte de larga distancia en horario nocturno**», dijo Arteaga en declaraciones a Radio Provincia.

En ese marco, explicó que «el actual estado de situación hace necesario intensificar los controles en el transporte interurbano y, también, en el transporte de larga distancia, en función de las horas de permanencia de los usuarios y pasajeros en cada una de las unidades y terminales».

«Encontramos mucha responsabilidad en el uso de barbijos y en la distancia. Sin embargo, falta mejorar los controles de temperatura en las terminales. La premisa es seguir insistiendo con las recomendaciones», indicó.

En otro tramo de la conversación, Arteaga resaltó que «la demanda de pasajes es muy inferior de la esperada para una temporada de verano» y precisó que «el factor de ocupación no llega ni al 50 por ciento».

«Los controles son precisos y están siendo articulados con Gendarmería, fuerzas federales y seguridad vial», puntualizó el funcionario.