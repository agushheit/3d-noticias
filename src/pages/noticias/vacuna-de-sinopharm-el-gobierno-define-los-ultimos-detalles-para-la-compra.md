---
category: Agenda Ciudadana
date: 2021-01-10T10:47:50Z
thumbnail: https://assets.3dnoticias.com.ar/100121-vacuna-sinopharm.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: '3DNoticias - Fuente: Télam'
resumen: 'Vacuna de Sinopharm: el Gobierno define los últimos detalles para la compra'
title: 'Vacuna de Sinopharm: el Gobierno define los últimos detalles para la compra'
entradilla: La operación podría alcanzar el millón de dosis y se espera para los últimos
  diez días de enero. Serán trasladadas por un vuelo de Aerolíneas Argentinas.

---
**Con más de 100 mil argentinos ya inmunizados con la Sputnik V y la próxima llegada de más dosis desde Rusia**, el gobierno nacional define los últimos detalles de la **compra de otro lote importante de vacunas**, las producidas por la compañía farmacéutica estatal china Sinopharm.

La vacuna de la también conocida como China National Biotech Group, la empresa más grande de su rubro en ese país, se espera para los últimos diez días de enero, trasladada por un vuelo de Aerolíneas Argentinas, según revelaron a Télam fuentes que conocen la negociación.

**La operación de compra podría alcanzar el millón de dosis**, una cantidad muy importante que requerirá acondicionar especialmente el avión de Aerolíneas que transporte las vacunas a través de la ruta Buenos Aires-Beijing. Inicialmente, el viaje está programado para el 20 de enero, dijeron a Télam fuentes oficiales, que explicaron que el vuelo deberá contar con una serie de autorizaciones muy rígidas debido al fuerte control de la actividad aerocomercial en la capital china desde el inicio de la pandemia.

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.15em; font-weight: 700">La negociación </span>**

La negociación con Sinopharm para la adquisición de un número tan significativo de vacunas, en un contexto de puja y fuerte demanda internacional, se produjo luego de un intercambio epistolar entre el presidente Alberto Fernández y el mandatario de China, Xi Jinping.

En uno de los tramos de la carta dirigida al presidente, el jefe de Estado chino subrayó la disposición de su país para reforzar la cooperación con Argentina en la aplicación de vacunas contra la Covid-19, según publicó el lunes la agencia de noticias oficial del país asiático, Xinhua.

El jueves, desde el Instituto Malbrán, el ministro de Salud, Ginés González García, informó públicamente sobre el avance de las conversaciones con Sinopharm.

«Estamos negociando con China un millón de dosis para enero y (también) con Brasil, que está produciendo otra de las vacunas de desarrollo chino», anunció, en alusión a las vacunas desarrolladas por Sinopharm y por otro laboratorio chino, Sinovac, que fabrica en Brasil otra inmunización contra el coronavirus, en este caso junto al instituto Butantan del estado de San Pablo.

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.15em; font-weight: 700"> El traslado</span>**

El viaje de Aerolíneas en búsqueda del lote de vacunas de Sinopharm tendrá que sortear complejidades administrativas y regulatorias, ya que la aerolínea de bandera nunca operó la ruta directa Buenos Aires-Beijing.

Los vuelos sanitarios con el Airbus 330-200 en abril de 2020 en busca de insumos sanitarios fabricados en China (respiradores y barbijos, centralmente) se realizaron a través de la ruta Buenos Aires-Shanghai con escala intermedia en Auckland, Nueva Zelanda.

Desde que se inició la pandemia, China concentró, por cuestiones de seguridad, todo el intercambio aerocomercial con el extranjero en las ciudades costeras de Shanghai y Guangzhou, salvo excepciones.

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.15em; font-weight: 700">La vacuna de Sinopharm </span>**

La vacuna desarrollada por Sinopharm, cuya sede central está en la propia Beijing, es identificada por las siglas BBIBP-CorV, funciona a partir de un virus inactivado (utiliza la versión muerta del germen que causa la enfermedad). 

En comparación con otras inmunizaciones tiene una particularidad que podría ser considerada una ventaja logística: **para su almacenamiento, conservación y transporte no necesita una refrigeración menor a 0 grados centígrados, sino que requiere temperaturas de 2 a 8 grados, condiciones que puede suministrar una heladera común**.

Por otro lado, los ensayos clínicos de fase 3 de la vacuna creada por el Sinopharm Group se desarrollaron al mismo tiempo en Bahrein, Emiratos Árabes Unidos, Egipto, Marruecos, Serbia, Jordania, Perú, Pakistán y también en la Argentina.

**A principios de diciembre, la vacuna de origen chino fue aprobada y registrada en los Emiratos Árabes Unidos (EUA)** en el marco de los estudios de tercera fase en ese país, y **el ministerio de Salud de esa nación informó que había demostrado una eficacia del 86%** y avaló el inicio de una inmunización masiva.

En la Argentina, los ensayos de la vacuna de Sinopharm contaron con 3.008 voluntarios que participaron de lo que se llama «estudio aleatorizado», es decir que reciben vacuna o placebo por azar, sin que el voluntario o el equipo investigador lo sepan al momento de la inoculación.

Estos ensayos corrieron por cuenta de la red Vacunar y la Fundación Huésped, asociados al laboratorio ELEA como patrocinador local, empresa que forma parte del Grupo Insud del empresario Hugo Sigman.

En relación con esas pruebas, el médico especializado Pedro Cahn, de la Fundación Huésped, aseguró a Télam que los estudios de la vacuna de Sinopharm con voluntarios argentinos se mostraron «extremadamente seguros».

«Sobre la eficacia todavía no puedo contestar, porque aún no tenemos los resultados, aunque los estudios hechos en China y en Emiratos Árabes Unidos dan cuenta de una **eficacia del 79,8% de inmunización**», señaló.

Sin embargo, «de lo que sí puedo hablar es de la seguridad, y <span style="color: #EB0202;">**hasta ahora no hemos visto efectos adversos serios que estén vinculados a la vacuna. Solo registramos los efectos adversos simples que se dan en todas las vacunaciones</span>**, como dolor en el sitio de inyección, febrícula o una sensación de estado gripal que dura menos de 24 horas», detalló Cahn.

Además, el infectólogo y director científico de la Fundación Huésped contó que la entidad está coordinando el ensayo médico de una segunda vacuna contra la Covid-19 desarrollada por una empresa china, aunque en este caso asociada con el Consejo Nacional de Investigación de Canadá, de allí el nombre CanSino Biologics, que funciona a partir de «una plataforma de adenovirus idéntica al de la segunda dosis de la vacuna creada por el Instituto Gamaleya», en alusión a la Sputnik de Rusia.

En la Argentina, la aprobación de emergencia de la vacuna de Sinopharm, dado el contexto de pandemia, podría concretarse en los próximos días, pero **la validación oficial dependerá de la forma en que se concrete la compra del millón de dosis**.

En el caso de que la adquisición se concretara a través de la compra a un laboratorio farmacéutico que opera en la Argentina, la aprobación se formalizaría en el marco de lo previsto en el Anexo I, ítem 5, de la disposición ANMAT 705/05, que autoriza el registro de vacunas en situación de emergencia.

Si la compra se realizara de Estado a Estado, la autorización la dará el Ministerio de Salud, con recomendación en ese sentido del ANMAT, en cumplimiento de la ley 27573 (conocida como ley de vacunas destinadas a generar inmunidad contra la Covid-19). Esto último fue lo que ocurrió con la vacuna Sputnik, creada por el Centro Gamaleya de Rusia.