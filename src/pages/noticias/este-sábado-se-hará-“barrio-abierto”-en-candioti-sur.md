---
layout: Noticia con imagen
author: Nota de la redacción de 3DNoticias
resumen: "Barrio Abierto: Candioti Sur"
category: La Ciudad
title: Este sábado se hará “Barrio Abierto” en Candioti Sur
entradilla: Se trata de una propuesta que surgió de la Red de instituciones del
  barrio y que la Municipalidad lleva a la práctica para disfrutar del espacio
  público. A partir de las 17 de este sábado 21.
date: 2020-11-21T12:28:29.329Z
thumbnail: https://assets.3dnoticias.com.ar/plano-candioti.jpg
---
**La invitación es a reunirse en la intersección de Necochea y Seguí, junto a ferias de arte y emprendimientos, circuitos de bike y skate y los cuidados sanitarios necesarios.**

Este sábado en Candioti Sur comienza Barrio Abierto, una propuesta para encontrarse en la calle y las veredas de una forma distinta. A partir de las 17, en la intersección de Necochea y Seguí, se montará una feria de emprendedores de la Economía Social y una feria de arte con la participación de Fronda Territorio Fotográfico, Delta Espacio y Made.

Entre las obras, objetos y emprendimientos, hay algunos que son producción de las vecinas y los vecinos del barrio. Participará también la Biblioteca y Espacio Cultural Lola Mora, con algunos de sus libros en versión ambulante e información sobre los talleres y otras propuestas que funcionan en el espacio.

Además de la pista del Candioti Park, habrá rampas para que los más pequeños puedan practicar bike y skate junto a Nicolás Miract, tallerista de la Secretaría de Educación y Cultura. Habrá campanas de recolección de residuos secos, donde se podrán depositar vidrios, plásticos, cartones, metales y papel, entre otros. A la par, estará abierto el bar Origen, ubicado en esa misma esquina, con su propuesta gastronómica y de bebidas.

Estarán cortadas las calles Necochea entre Calchines y Rosalía Centro y Seguí entre Sarmiento y Marcial Candioti.

“Cuando decimos que construimos las decisiones con la gente nos referimos a esto, justamente. Esta propuesta surge de las redes de instituciones, el espacio potenciado dentro del Plan Integrar que busca discutir, debatir e intercambiar sobre las estrategias de intervención en los barrios”, explicó el intendente Emilio Jatón.

Y siguió: “Cuando decimos que queremos trabajar en red no es sólo para pensar las obras que se necesitan sino para hablar de todas las acciones requeridas para construir ciudadanía y mejorar la convivencia. Esta propuesta surgió de un grupo de vecinos que participan de la red. Ahí la trabajaron, sumaron ideas de los demás participantes y mañana, con el apoyo de la Municipalidad, vamos a tener una propuesta para disfrutar juntos y cuidándonos”.

## **Volver a encontrarse en la vereda**

Paulo Ricci, el secretario de Educación y Cultura de la Municipalidad, explicó que la iniciativa “surgió de una propuesta que la Vecinal presentó en la Red de instituciones de Candioti Sur y que tomamos desde el área junto a Desarrollo Urbano, Control e Integración y Economía Social. Acercaron la idea de hacer algo al aire libre y propusimos convertirlo en parte de la programación que ofrece la ciudad a la ciudadanía durante el verano”.

“Esta será la primera experiencia de un ciclo que iremos enriqueciendo en los distintos barrios y en medida de las restricciones vigentes, para acercar propuestas más amplias. La premisa es que recuperemos el espacio público para la convivencia, el juego y el aprendizaje y también para visibilizar la labor de artistas y emprendedores y las propuestas gastronómicas de cada lugar”, puntualizó Ricci.

Por su parte, Pedro Nieva, presidente de la Asociación Vecinal Candioti Sur, expresó el apoyo de la institución para el comienzo de un nuevo aprovechamiento y disfrute y del espacio público. “En un año que nos maltrata mucho en todo sentido, iniciativas de este tipo nos permiten de alguna manera reconciliarnos con la vida”, aseguró Nieva.

## **Artista del barrio**

Desde la Red de instituciones del barrio surgió también la idea de recordar, aprovechando la ocasión, el nombre de Ana Niel. Se trata de una artista plástica y docente santafesina, nacida el 6 de junio de 1948, que vivió en Candioti Sur y cuya familia continúa desarrollándose en la zona.

“Ana fue una artista notable, y por esas cosas que se dan, se perdió la continuidad de su obra, y esperamos que su nombre pueda volver a instalarse en el imaginario colectivo”, explicó Nieva.

Entre otras de las actividades que desarrolló en su carrera, Niel estuvo a cargo desde 1984 y durante más de quince años de un taller de artes visuales para niños y adolescentes en Las Flores II y otros barrios, que expuso sus producciones en el Museo Municipal de Artes Visuales “Sor Josefa Díaz y Clucellas”.