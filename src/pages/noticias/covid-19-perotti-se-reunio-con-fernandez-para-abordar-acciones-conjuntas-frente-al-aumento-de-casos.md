---
category: Estado Real
date: 2021-01-07T09:43:55Z
thumbnail: https://assets.3dnoticias.com.ar/070121-reunion-Perotti-AF.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: 'Covid–19: Perotti se reunió con Fernández para abordar acciones conjuntas
  frente al aumento de casos'
title: 'Covid–19: Perotti se reunió con Fernández para abordar acciones conjuntas
  frente al aumento de casos'
entradilla: Participaron, además, ministros y el resto de los gobernadores. «Lo único
  que disminuye las posibilidades de contagio es quitar circulación, por lo tanto,
  las medidas irán en esa dirección», detalló el gobernador.

---
El gobernador Omar Perotti se reunió este miércoles mediante videoconferencia con el presidente de la Nación, Alberto Fernández, el jefe de Gabinete, Santiago Cafiero, ministros y secretarios del gabinete nacional, y el resto de los gobernadores para **abordar acciones conjuntas frente a la pandemia de COVID-19**.

Luego del encuentro, el gobernador afirmó: «durante la reunión con el presidente hemos abordado la marcha de la vacunación y los nuevos cuadros que plantea la presencia de una segunda ola hoy frente al número de casos que ha crecido en todo el país».

En ese sentido, Perotti indicó que «la provincia se sumará a las medidas que tome la Nación, resguardando a aquellos lugares que no han tenido casos». Y agregó: «Santa Fe va tomando medidas en localidades donde la situación ya se ha planteado en estos términos, avanzamos en disminución de movilidad y cuidados adicionales según la característica de cada localidad».

Además, sostuvo que «**es fundamental que podamos desplegar con la mayor eficiencia y compromiso posible la campaña de vacunación lo antes posible**. Para que eso ocurra es fundamental que todos tomemos cuidados para que podamos llegar a la vacuna. De la misma manera que hicimos un esfuerzo por llegar a las fiestas, es clave hacer un esfuerzo para que todos lleguemos a la vacuna y, además, para que podamos llegar a iniciar las clases con el personal sanitario, de seguridad y docentes vacunados».

Por último, señaló: «tenemos un verano distinto, con cada una de las actividades que tienen su protocolo, pero nos tenemos que cuidar». Y finalizó: «está comprobado que **lo único que disminuye las posibilidades de contagio es quitar circulación, por lo tanto, las medidas irán en esa dirección**».

Por su parte, la ministra de Salud provincial, Sonia Martorano, detalló que «hubo un consenso que en todo el país se está viendo un aumento de casos. Por ello, estamos pensando que estamos frente a una segunda ola. De manera tal que es muy importante que toda la ciudadanía aumente el nivel de cuidados».

«Esto nos lleva a una de las principales conclusiones del encuentro: **no se van a generar restricciones, sino que lo que vamos a limitar es la circulación** y así podemos disminuir la circulación del virus. Ahora estamos evaluando cómo hacerlo, seguramente reduzcamos los horarios de circulación», concluyó la ministra.

Participó, además, del encuentro virtual, el secretario de Salud provincial, Jorge Prieto.