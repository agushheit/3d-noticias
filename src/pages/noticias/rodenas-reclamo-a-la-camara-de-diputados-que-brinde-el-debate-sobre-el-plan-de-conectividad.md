---
category: Estado Real
date: 2021-02-01T06:50:07-03:00
thumbnail: https://assets.3dnoticias.com.ar/rodenas.png
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: Rodenas reclamó a la Cámara de Diputados “que brinde el debate sobre el Plan
  de Conectividad”
title: Rodenas reclamó a la Cámara de Diputados “que brinde el debate sobre el Plan
  de Conectividad”
entradilla: 'La vicegobernadora destacó que “la pandemia desnudó profundas desigualdades”
  y resaltó la importancia de la iniciativa enviada por el gobernador Omar Perotti
  a la Legislatura. '

---
La titular del Senado provincial reclamó el tratamiento del proyecto del Ejecutivo para brindar conectividad a toda la provincia de Santa Fe, que ingresó en octubre pasado a la Cámara baja, luego de recibir media sanción en Senadores.

En declaraciones a la prensa a propósito del lanzamiento de un ciclo realizado por distintos colectivos de música, Alejandra Rodenas subrayó la importancia del Programa Estratégico de Conectividad –por ejemplo– “para la difusión y llegada de la cultura a toda la provincia”, y en ese marco lamentó que la iniciativa no avance en la Cámara de Diputados.

“Qué importante sería que santafesinas y santafesinos tuvieran la posibilidad de disfrutar que este tipo de eventos llegue a toda la provincia a través de la conectividad”, manifestó la vicegobernadora, y agregó: “Son los sectores más vulnerables los que se ven más afectados, el proyecto oficial de conectividad permite alcanzar mayores niveles de igualdad, y eso no puede ser soslayado”.

Asimismo, Rodenas insistió en que “la pandemia desnudó y desnuda profundas desigualdades. Una de ellas es la imposibilidad de ciertos territorios de acceder a una conexión de calidad. En Santa Fe sólo el 40% accede a este servicio esencial y derecho humano fundamental en la sociedad actual”.

La presidenta del Senado expresó: “El Covid además afectó a diversos sectores –en este caso vemos cómo el Estado sale a apoyar a artistas y actores culturales–, pero pienso en las y los docentes, alumnas y alumnos, los padres y madres, que están entre quienes más sufren la falta de acceso a una conectividad digna, sobre todo en los rincones más alejados de los grandes centros urbanos”.

Para la vicegobernadora hay algo que está claro: “la pandemia nos hizo ver la brecha digital y con ella la necesidad de avanzar en la accesibilidad a las nuevas tecnologías y en la inclusión digital de todas las santafesinas y los santafesinos. En Santa Fe iniciamos este camino desde el Ejecutivo, y se planteó llevar adelante el plan de conectividad”.

Acto seguido, Rodenas recordó que el proyecto “tiene ya media sanción del Senado provincial desde el 22 de octubre del año pasado. Ese mismo día ingresó a la Cámara de Diputados para su tratamiento y (la iniciativa) se registró bajo el número 40.955. Desde esa fecha esperamos que se tome la decisión de discutir, de dar el debate”.

La vicegobernadora manifestó en el mismo orden de cosas que “este tema debe ser parte de las discusiones públicas, porque una Santa Fe conectada nos abre nuevas posibilidades de desarrollo cultural, productivo, económico, etc.”.

Finalmente, Alejandra Rodenas consignó que “principalmente nos habilita a un nuevo diálogo con todos los territorios de nuestra provincia, tan diversa en su extensión. Nosotros creemos que estamos ante una gran oportunidad, la de diagramar políticas inclusivas y justas para todos los habitantes de Santa Fe. Esperamos poder contar con el debate de nuestros diputados y diputadas en este sentido”, concluyó la titular del Senado provincial.