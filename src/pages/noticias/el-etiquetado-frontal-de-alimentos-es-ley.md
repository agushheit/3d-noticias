---
category: Agenda Ciudadana
date: 2021-10-28T06:15:25-03:00
thumbnail: https://assets.3dnoticias.com.ar/ETIQUETADO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: El etiquetado frontal de alimentos es ley
title: El etiquetado frontal de alimentos es ley
entradilla: " La iniciativa busca advertir a los consumidores sobre los excesos de
  grasas, sodio y azúcares en los productos con el fin de ayudar a combatir la obesidad,
  la hipertensión y los riesgos cardíacos."

---
La Cámara de Diputados convirtió la noche del martes en ley el proyecto de etiquetado frontal de alimentos, ideado para advertir a los consumidores sobre los excesos de grasas, sodio y azúcares en los productos con el fin de ayudar a combatir la obesidad, la hipertensión y los riesgos cardíacos.  
  
El proyecto sumó 200 votos a favor; 22 en contra y 16 abstenciones, en tanto que se registraron 18 diputados ausentes al momento de la votación.  
  
La iniciativa comenzó a ser debatida pasadas las 16 y su análisis se agotó pasadas las 23, tras la intervención de más de 70 diputados, que participaron de la primera reunión presencial tras la metodología de trabajo mixto aplicado durante los primeros meses de la pandemia de Covid.  
  
Al abrir el tratamiento del proyecto, la presidenta de la comisión de Legislación General, Cecilia Moreau (Frente de Todos), afirmó que "el Estado argentino no va a mirar para otro lado" en un contexto de "enfermedades crónicas no transmisibles" como la obesidad, la hipertensión y los problemas cardíacos, que afectan a su población y que se pueden prevenir a partir de la alimentación.  
  
Por eso, marcó la importancia de destacar la información nutricional en las etiquetas de los productos alimenticios, debido a que en la actualidad muchas "son ilegibles".  
  
Su compañera de bloque, Liliana Schwindt, ponderó la ley como un "hito para consumidores y usuarios", pero también "para la producción alimentaria argentina".  
  
"Esta ley es poner en lo más alto a los consumidores, que hace años reclaman información clara, precisa y veraz", dijo.  
  
  
Desde su condición de presidente de la comisión de Salud y con su pertenencia al Frente de Todos, el tucumano Pablo Yedlin mostró sus diferencias parciales: "El azúcar no es un veneno, es un producto natural. Muchos edulcorantes, en cambio tendrán que explicar cuán saludables son. Vamos a acompañar el proyecto proponiendo mejoras".  
  
La también tucumana Beatriz Ávila (Frente de la Justicia Social) advirtió que el etiquetado color "negro remite a la idea de muerte", y sostuvo: "Esta ley de etiquetado frontal, tal como está, va a generar más pobreza y mayor desempleo para la región. Creo que todos los diputados de la región debemos unirnos y hacer una mejor ley, que no deje las economías regionales".  
  
Por el Frente de la Concordia Misionero, Flavia Morales dio otro punto de vista: "A la provincia de Misiones hay cuestiones que le afectan con la yerba y el té, pero vamos a acompañar la ley y esperamos que en la reglamentación pueda arreglarse".  
  
Desde otro espacio provincial, el médico Luis Di Giácomo, de Juntos Somos Río Negro, remarcó: "Esta ley es más que bienvenida. Datos matan relato; según trabajos científicos en Uruguay hubo modificaciones en las decisiones de compra en base al rotulado y en Chile no hubo disminución del trabajo a partir de esta medida como muchos dicen".  
  
La diputada de Juntos por el Cambio Brenda Austin (UCR) celebró el tratamiento de esta ley que "derriba mitos" y destacó que logró no "haber caído en la grieta", sino que se ponderó la "defensa del derecho a la salud".  
  
"Esta ley es necesaria porque estamos frente a una gran epidemia: la del sobrepeso", dijo, y agradeció a organizaciones de la sociedad civil que "ayudaron a respaldar con evidencia, información y acciones que derribaron mitos".  
  
En cambio, la diputada del PRO Carmen Polledo planteó críticas a la iniciativa al afirmar que se debe "pensar en un sistema de etiquetado integral y completo" y dijo que por ese motivo se propuso en un dictamen "una alternativa que cumpla con el propósito de modificar los hábitos alimenticios".  
  
El cierre del debate, estuvo a cargo de la kirchnerista Florencia Lampreabe, quien destacó: "Esta ley es una herramienta de soberanía alimentaria. Detrás de lo que consumimos, está también el modelo de producción que alentamos. Para poder decidir primero hay que saber, una condición que hoy se encuentra empañada porque las empresas no van de frente y ocultan información básica”.  
  
La precedió en la palabra el radical Alejandro Cacace, quien argumentó: "Hay un enorme aumento de la obesidad, una prevalencia del exceso de peso, una clarísima y abundante evidencia que marca la relación entre el incremento de los alimentos procesados y ultraprocesados con ese fenómeno de la obesidad y exceso de peso y la relación que tiene con las enfermedades no transmisibles, con la prevalencia de las enfermedades cardíacas, respiratorias, el cáncer y la diabetes”.  
En ese sentido, se preguntó: "¿Entonces qué estamos esperando para regular esta problemática de salud?"

La sesión

El tratamiento del proyecto comenzó con una demora de más de tres horas debido a que varios legisladores, en su mayoría de Juntos por el Cambio, presentaron pedidos de apartamiento del reglamento para incluir otros temas, aunque esas peticiones en su totalidad fueron rechazadas y se mantuvo el temario acordado entre los bloques políticos.  
  
La sesión, que se inició a las 12.35, fue observada por diferentes organizaciones de la sociedad civil que vienen solicitando la aprobación de la ley de etiquetado frontal para que la sociedad conozca los excesos de grasas, sodios y azúcares que tienen algunos productos de los alimentos.  
  
El dictamen de mayoría prevé la incorporación en el frente de los envases de los productos ultraprocesados una etiqueta con forma de octógono negro con letras blancas que advierta el exceso de nutrientes críticos para la salud, como azúcares, sodio, grasas saturadas, grasas totales y calorías.  
  
Además, determina que los productos que contengan entre sus ingredientes edulcorantes o cafeína tendrán que informar que su consumo no se recomienda en niñas y niños.  
  
Por otra parte, el proyecto establece la prohibición de emitir publicidad comercial -dirigida a niños, niñas y adolescentes- de productos con sellos de advertencia.  
  
Además, dispone que los productos que tengan más de un sello de advertencia no podrán incluir dibujos animados, personajes, figuras públicas, regalos ni elementos que llamen la atención de niños, niñas y adolescentes.  
  
Por otra parte, determina que, ante iguales condiciones, el Estado deberá priorizar la compra de alimentos sin estos sellos de advertencia.