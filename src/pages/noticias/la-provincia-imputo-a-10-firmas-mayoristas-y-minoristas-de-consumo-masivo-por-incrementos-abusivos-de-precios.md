---
category: Estado Real
date: 2021-02-01T06:48:30-03:00
thumbnail: https://assets.3dnoticias.com.ar/supermercados.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: La provincia imputó a 10 firmas mayoristas y minoristas de consumo masivo
  por incrementos abusivos de precios
title: La provincia imputó a 10 firmas mayoristas y minoristas de consumo masivo por
  incrementos abusivos de precios
entradilla: 'Se registraron aumentos de hasta 30% más, sobre el precio máximo autorizado
  por la normativa nacional.

'

---
El Ministerio de Producción, Ciencia y Tecnología, a través de la Secretaría de Comercio Interior y Servicios, imputó a 10 supermercados e hipermercados por aumentos indebidos en alimentos y otros artículos esenciales.

Dichas acciones se llevaron adelante durante inspecciones de oficio que realiza la Secretaría como autoridad de aplicación de las normativas nacionales de comercio y defensa del consumidor.

En tal sentido, el secretario de Comercio Interior y Servicios, Juan Marcos Aviano, indicó que “en el marco de las leyes de comercialización dentro de las cuales tenemos actuación, entendemos que inspeccionar, imputar, sancionar y corregir las situaciones que están a nuestro alcance, es una obligación como funcionarios y como Estado”.

Los aumentos indebidos registrados van entre el 6% en productos de higiene femenina, 8% en leche, 21% en azúcar,y hasta el 35% en puré de tomates. También se registraron en aceites, harinas, fideos, mate cocido, té, y crema de leche, entre otros bienes esenciales de consumo.

“Es importante aclarar que el porcentaje que hemos registrado es un aumento no autorizado. Esos productos ya tuvieron incrementos, en promedio, del 5%, que fueron autorizados por el gobierno nacional; y el supermercado no sólo incumplió eso, sino que los aumentó hasta un 30% más”, aclaró el funcionario.

Las firmas imputadas, con bocas de expendio en las ciudades de Rosario, Santa Fe y Venado Tuerto, entre otras, “son referencia para otras firmas de las principales ciudades de la provincia, marcando tendencia en el segmento de consumo masivo hacia todo el sector", señaló Aviano, y agregó que “por ello estamos convencidos de que a la par de políticas de referencia de precios, y el sostenimiento de la demanda, debemos seguir insistiendo con la necesidad de que el sector empresario acompañe al consumidor y al país en este difícil momento”.

**PARA DENUNCIAR**

Finalmente, Aviano convocó “a los consumidores de la provincia a seguir denunciando los aumentos y abusos que las vecinas y vecinos de nuestros pueblos y ciudades consideran indebidos”.

Las denuncias pueden realizarse al 0800-555-6768 (opción 3), o mediante el canal digital que opera las 24 horas [https://aswe.santafe.gov.ar/denuncia_precios/](https://aswe.santafe.gov.ar/denuncia_precios/ "https://aswe.santafe.gov.ar/denuncia_precios/")