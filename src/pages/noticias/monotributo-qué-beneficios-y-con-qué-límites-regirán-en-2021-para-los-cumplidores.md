---
layout: Noticia con imagen
author: .
resumen: "Monotributo: beneficios"
category: Agenda Ciudadana
title: "Monotributo: qué beneficios y con qué límites regirán en 2021 para los
  cumplidores"
entradilla: Habrá beneficios para los monotributistas que hayan cumplido con
  todos sus pagos desde enero de 2017 y hasta el 26 de agosto de este año.
date: 2020-11-10T15:03:50.803Z
thumbnail: https://assets.3dnoticias.com.ar/afip.jpg
---
Los monotributistas que hayan cumplido con todos sus pagos desde enero de 2017 y hasta el 26 de agosto de este año estarán liberados, durante el primer semestre de 2021, de pagar el componente impositivo de su aporte entre dos y seis meses, dependiendo de la categoría en la que estén ubicados. El beneficio de una exención por un período de hasta medio año fue incluido en la ley 27.562, con la cual el Congreso amplió los alcances de la moratoria impositiva que se había aprobado a fines de 2019. Ahora, la AFIP definió mediante la resolución general 4855 (que se publicaría mañana en el Boletín Oficial) cuáles son los períodos por los que regirán las exenciones.

La ley de de ampliación de moratoria, votada en agosto, previó, a pedido de legisladores de la oposición, algunas disposiciones que implican un premio a tres categorías de contribuyentes cumplidores; es decir, para quienes no necesitan entrar al plan de pagos de deudas con condonación de intereses y sanciones. Los tres grupos son: monotributistas, autónomos y micro y pequeñas empresas. Para acceder a los beneficios, según anunció la AFIP, habrá que solicitarlos haciendo un trámite en la página del propio organismo, que estará habilitado solo hasta el 30 de este mes.

En el caso de los contribuyentes adheridos al régimen impositivo simplificado, la cantidad de cuotas que no se pagarán depende de la categoría en la que se esté. Los ubicados en los escalones más bajos de la escala, en las categorías A y la B, los meses con exención serán de enero a junio del año próximo. Para las categorías C y D, el beneficio regirá de enero a mayo; en las categorías E y F no se abonarán las cuotas mensuales entre enero a abril, en tanto que quienes, por su nivel de facturación, estén en las categorías G y H tendrán tres meses con exención. Y, finalmente, para los ubicados en los niveles I, J y K (solo actividades de comercio, no de prestación de servicios) estarán liberados de los pagos de enero y febrero, aunque de manera limitada.

La exención temporal no alcanza a los aportes previsionales y a la obra social (esos pagos sí deberán hacerse). Y, además, regirá un límite para todos los casos de $17.500, según establece la ley. Por eso, en las categorías más elevadas el beneficio no contemplará la totalidad del pago correspondiente a la cantidad de meses dispuesta, porque en la práctica se superará el tope previsto.

En rigor, todavía no se sabe de cuánto serán las cuotas del monotributo durante 2021. En los últimos años y por una disposición legal, la actualización tanto de los montos a pagar mensualmente como de las cifras topes de facturación de cada categoría, se hizo en función del porcentaje en que habían aumentado en el año previo las jubilaciones y pensiones de la Anses, de acuerdo con la fórmula de movilidad. Pero este año no rigió la fórmula de cálculo, sino que el Poder Ejecutivo dispuso recomposiciones a los haberes otorgadas de forma discrecional. Y el porcentaje no fue para todos por igual, sino que varió según el nivel de ingresos. Por ahora, no se conoce qué actualización se hará de las variables.

"La ley fijó un tope de $17.500 que no es actualizable, y el diferimiento de la reglamentación tiene como efecto que eso cada vez represente menos", sostuvo la contadora Florencia Fernández Sabella, en referencia al deterioro del valor de una suma en pesos, por efecto de la inflación. La cifra, de hecho, se aprobó agosto de este año y regirá para el primer semestre de 2021, cuando los valores de pago del monotributo serán, seguramente, más elevados que los actuales.

En el caso de los autónomos, lo establecido por la ley es que para el ejercicio anual 2020 tengan una deducción especial, es decir, un monto que se restará de la base imponible, y que en la práctica equivale a $61.930,58. La ley dice, específicamente, que esos contribuyentes "tendrán derecho a deducir, por un período fiscal, de sus ganancias netas un importe adicional equivalente al cincuenta por ciento (50%) del previsto en el artículo 30, inciso a) de la Ley de Impuesto a las Ganancias". Ese inciso se refiere a la ganancia no imponible, hoy de $123.861,17.

Para las micro y pequeñas empresas que haya cumplido con sus obligaciones de declaraciones y pagos desde enero de 2017 y hasta agosto pasado, se habilitará la posibilidad de hacer amortizaciones aceleradas por inversiones en bienes muebles y obras de infraestructura que se hagan en el período comprendido entre el 26 de agosto pasado y el 31 de diciembre de 2021.

[Boletín Oficial de la República Argentina](https://www.boletinoficial.gob.ar/detalleAviso/primera/237132/20201110)