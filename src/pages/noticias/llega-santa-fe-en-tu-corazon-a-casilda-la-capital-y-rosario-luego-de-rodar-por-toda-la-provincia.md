---
category: Estado Real
date: 2021-11-06T06:00:39-03:00
thumbnail: https://assets.3dnoticias.com.ar/LITTO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Llega "Santa Fe en tu corazón" a Casilda, La Capital y Rosario luego de rodar
  por toda la provincia
title: Llega "Santa Fe en tu corazón" a Casilda, La Capital y Rosario luego de rodar
  por toda la provincia
entradilla: El programa itinerante del Ministerio de Cultura viene de recorrer cada
  rincón del territorio a través de la música y sus voces más populares, convocando
  a artistas consagrados.

---
Este viernes, “Santa Fe en tu corazón” llega a Casilda, Santa Fe y Rosario. El ciclo está programado para ser realizado de manera itinerante, los fines de semana hasta fin de año, y con una agenda especial para el verano. Para el mismo serán convocados artistas de renombre nacional, figuras locales, regionales y los elencos municipales tienen su lugar en diferentes escenarios de toda la provincia.

**CASILDA CIERRA CON LITO NEBBIA Y LOS TIPITOS**

La capital del departamento Caseros ya vibra con la llegada del camión escenario al que se subirán este viernes, propuestas artísticas realmente importantes.

Así, en el espacio ubicado en Lisandro de la Torre y Sarmiento, desde las 18:00, el público podrá disfrutar de los Mercenarios del Zanjón; el Colectivo El Revuelto de Gálvez, y tendrá la oportunidad de reencontrarse con dos verdaderos platos fuertes: Lito Nebbia y la banda Los Tipitos.

**“SANTA FE EN TU CORAZÓN” LLEGA A LA CAPITAL DE LA PROVINCIA**

El sábado 6 de noviembre, el escenario móvil e itinerante estará en La Redonda, donde la propuesta incluye, también desde las 18:00, una Feria de Artesanos y Emprendedores; espectáculos de danza; Cuerda de Tambores del Solar; Los Micromundos de Brass, y un cierre a todo ritmo con la Delio Valdez.

**CIERRE DE FINDE CON ESTELARES**

Rosario ya se prepara también para recibir a “Santa Fe en tu Corazón”. La ciudad está lista para disfrutar el programa, que en este caso se desarrollará el domingo 7 en el Parque Nacional a la Bandera, desde las 17:00.

Habrá Feria de Diseño y Arte Impreso; expondrán puestos del Mercado de Frutos Culturales, y actuarán Diego Arolfo; el Dúo Kalbermatten, de Granadero Baigorria, los renombrados integrantes de la Trova Fabián Gallardo y Rubén Goldín; el ex líder de Mortadela Rancia, Gonzalo Aloras; la banda Liver Bird, de Funes, y el cierre con la banda Estelares.