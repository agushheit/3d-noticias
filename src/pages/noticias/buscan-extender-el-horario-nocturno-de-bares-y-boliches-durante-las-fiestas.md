---
category: La Ciudad
date: 2021-12-16T06:00:09-03:00
thumbnail: https://assets.3dnoticias.com.ar/bares.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Buscan extender el horario nocturno de bares y boliches durante las Fiestas
title: Buscan extender el horario nocturno de bares y boliches durante las Fiestas
entradilla: 'La medida es evaluada en la Secretaría de Control de la Municipalidad.
  El proyecto de un concejal propone estirar el horario hasta las 6

'

---
La Municipalidad de Santa Fe está evaluando hasta qué hora podrían extender el horario de bares, boliches, salones de eventos y la circulación en general, durante las fiestas de fin de año. Desde Juntos por el Cambio formalizaron un pedido para que en Navidad y Año Nuevo el límite sea las 6 del día siguiente.

En diálogo con el programa Ahí Vamos (de 9 a 12 por FM 106.3 La Radio de UNO) el Subsecretario de Convivencia Ciudadana, Guillermo Álvarez, anunció que “la idea” es extender el horario nocturno durante las fiestas pero se estará definiendo en estos días.

“Con el horario que tenemos hoy de cortar a las 3, sería prácticamente imposible poder desarrollar cualquier actividad comercial nocturna”, explicó Guillermo Álvarez. “Desde el municipio entendemos que va a haber que extender los horarios”.

El intendente Emilio Jatón se reunirá estos días con el ministro de Gestión Pública de la Provincia de Santa Fe para consultar si habrá una decisión administrativa provincial, pero según el subsecretario, si no es así “lo sacaremos directamente nosotros desde el municipio”.

“Recordemos que el último decreto del gobierno provincial establece que hasta el 31 de diciembre el horario de cierre de muchas de estas actividades que tienen que ver con la nocturnidad es a las 3 de la madrugada”.

En esta línea, también por la Radio de UNO, el concejal de Juntos por el Cambio Sebastián Mastropaolo anunció que desde su partido presentaron un pedido formal para extender el horario hasta las 6.

“Gracias a Dios tenemos una situación controlada donde todavía no tenemos una casuística importante, por lo cual buscamos por diferentes factores liberar el esparcimiento, liberar alguna actividad nocturna, que no se corte a las 3 de la mañana si no darle continuidad hasta las 6, y esto obviamente lo que conlleva es mayor trabajo para muchos de los sectores que han sido muy golpeados por la pandemia; sabemos lo que ha sido para los restaurantes, para los boliches, para los salones de eventos”, explicó el legislador.

Mastropaolo aseguró que con esta medida esperan beneficiar a las familias y negocios que viven de la gastronomía y la nocturnidad santafesina, pero además para garantizar un mejor control del esparcimiento.

“Sabemos muy bien que la juventud y las personas que salgan a divertirse esas dos noches no se van a ir a dormir a las 3 de la mañana porque es el horario límite que hoy se ejerce, entonces a lo que tenemos miedo y lo queremos prever es que justamente no vayan en búsqueda de fiestas clandestinas, eventos privados, de lugares que no tienen ningún tipo de control”, argumentó.

Y concluyó: “Nos parece mucho más inteligente tratar de extender este horario y que las personas se queden en los eventos que han sido habilitados por el municipio con los protocolos correspondientes, con todo lo que conlleva hoy la ley y además para que no se circule a las tres, cuatro de la mañana por toda la ciudad en búsqueda de diferentes eventos, que no van a ser habilitados y con la inseguridad y el peligro que conlleva que mucha gente circule a esa hora en un estado que normalmente no sería el recomendado para estar circulando”.

**Controles durante las fiestas**

Por otro lado, en la Secretaría de Control Municipal ya comenzaron los preparativos ante los dos temas centrales durante las fiestas: la pirotecnia y la circulación vehicular.

“Hoy nos vamos a sentar con el Ministerio de Seguridad a diagramar y empezar a charlar el tema de Navidad y Año Nuevo y cómo vamos a fortalecer los operativos que no tienen que ver con pirotecnia y que sí tienen que ver con el tránsito, con la alcoholemia y con el control de la nocturnidad”, dijo el subsecretario Álvarez.

Con respecto a la pirotecnia, recordó que la Ordenanza 12.429 está en plena vigencia desde 2019, por lo que comenzaron a realizar acciones de concientización y sensibilización. Santa Fe es territorio libre de pirotecnia ya que “la combustión de la cohetería muchas veces ocasiona daños y perjuicios a distintos grupos como son los niños con autismo, como los animales”, sintetizó.

“Las fiestas pasadas decomisamos más de dos toneladas de pirotecnia, tuvimos el decomiso más importante en la historia de Santa Fe en este rubro”, contó. “Fue un trabajo muy intensivo, llegamos a más de 80 lugares e hicimos más de 30 decomisos”.

Y continuó: “En todos los controles que hicimos en la Ruta 1, los cuales hicimos hasta la tardecita, no encontramos un solo puesto ambulante de pirotecnia, que era histórico en Santa Fe. En las últimas fiestas no encontramos un solo puesto ni para Navidad ni Año Nuevo”.

“Creo que esto habla a las claras de que los controles en esta materia están, solo que tratamos de hacer mucho hincapié desde la Secretaría de Control en lo que tiene que ver con la comercialización de la pirotecnia”, agregó. Esto significa que el objetivo es llegar a los comercios que alguna vez estuvieron habilitados, como los cotillones, y también los kiosquitos o librerías de barrio que suelen pasar inadvertidos. “En esos casos les labramos las actas de infracción respectivas y les decomisamos la mercadería”.

Finalmente, mencionó que también en 2020 se procedió, por primera vez, a realizar denuncias ante el Ministerio Público de la Acusación de personas o perfiles que comercializaban pirotecnia a través de las redes sociales. “Desde el municipio no tenemos acceso a que nos pueda brindar información sobre estos perfiles, en cambio la Justicia le puede requerir a Google la información de que le den determinado perfil para poder luego seguir una investigación y llegar hasta esta gente que está realizando esta actividad irregular escondida a través de las redes sociales”.