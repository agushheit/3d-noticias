---
category: La Ciudad
date: 2020-12-10T11:24:36Z
thumbnail: https://assets.3dnoticias.com.ar/disena.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Marcas locales para las compras navideñas en LA DISEÑA 2020
title: Marcas locales para las compras navideñas en La Diseña 2020
entradilla: 'La edición navideña de La Diseña contará con la participación de 160
  marcas y podrá visitarse del 16 al 22 de diciembre, de 16 a 21, con entrada libre
  y gratuita. '

---
**Del 16 al 22 de diciembre abrirá sus puertas La Diseña**, el ya clásico encuentro con las marcas de diseño santafesino para las compras de las fiestas de fin de año.

Será en la **Estación Belgrano (Bv. Gálvez 1150), de 16 a 21**, **con entrada libre y gratuita**. Frente a la situación sanitaria y las dificultades que el sector atravesó durante el año debido a la pandemia, se privilegió la participación de diseñadoras y diseñadores locales.

Serán **160 marcas de Santa Fe y su área metropolitana y Paraná**, que forman parte de la convocatoria 2019-2020 que rige hasta diciembre y en función de ello no deberán enfrentar al pago de canon, ya que en esta oportunidad los costos del montaje y seguridad serán asumidos totalmente por el municipio.

Se suma la participación del Taller de La Guardia, con un stand de venta y otro de exhibición celebratorio de su 60° aniversario.

La Municipalidad elaboró y presentó ante el Ministerio de Trabajo de la provincia el protocolo correspondiente para garantizar el funcionamiento de la feria en cumplimiento de todas las condiciones de higiene y distanciamiento social necesarias.

El secretario de Educación y Cultura, Paulo Ricci, resaltó que “es un compromiso de la gestión que encabeza el intendente Emilio Jatón darle la oportunidad a todos los emprendedores y las emprendedoras de tener su encuentro anual, y no queríamos dejar de encontrar una manera innovadora, original y cuidadosa de hacerlo”.

El funcionario especificó que no habrá la cantidad habitual de puestos ni de actividades por razones de la pandemia, pero sí participarán todos los emprendedores que se manifestaron en condiciones de poder hacerlo.

**La oferta gastronómica se montará al aire libre, sobre calle Dorrego**, para que el público no permanezca en los andenes luego de haber realizado sus compras. Serán cervecerías artesanales y emprendimientos vinculados a Capital Activa.

“Fue un desafío muy grande haber encontrado los consensos necesarios y trabajar durante los últimos tres meses para generar las condiciones de posibilidad de un evento que suele ser muy masivo y multitudinario, y que este año va a tener características distintas conforme a la realidad epidemiológica”, concluyó Ricci.

<br/>

## <span style="color: #25282C; font-family: arial, helvetica; font-weight: bold;">**Apuesta por la economía local**</span>

En octubre, la Municipalidad conformó la Mesa de Diseño Santafesino, establecida por la ordenanza 12656/19, con integrantes de la Secretaría de Educación y Cultura y la de Producción; representantes de los distintos rubros del sector; de la Facultad de Arquitectura, Diseño y Urbanismo de la Universidad Nacional del Litoral y concejales.

En ese ámbito y de manera conjunta con la Cámara de Diseño, con la que el municipio tiene un convenio de colaboración, se articularon las estrategias necesarias para realizar La Diseña de la manera más provechosa para todas las marcas dentro de la coyuntura de emergencia.

Julia Cáceres es diseñadora de Mundo Pichón, una marca local de indumentaria infantil. Junto a los emprendedores convocados, expresó: “nos encontramos muy agradecidos de poder participar en La Diseña, más aún este año tan particular en medio de la pandemia mundial que nos sacudió, cuestionó nuestros rumbos y hasta influyó en nuestras ganas de seguir”.

Agregó que en un año donde predominó la virtualidad y los emprendimientos debieron reinventarse y adaptarse a las nuevas modalidades de venta, “reencontrarnos sin pantallas de por medio significa una alegría enorme: es una oportunidad única para hacerle saber al público que no solo se va a llevar un producto original, sino una parte de todo nuestro esfuerzo, horas sin dormir, malabares por mantenernos en medio de tanta incertidumbre y por apostar a la economía local”.

<br/>

## <span style="color: #25282C; font-family: arial, helvetica; font-weight: bold;">**Ingreso y protocolo**</span>

**Se contará con un único ingreso por bulevar Gálvez 1150**, en donde se tomará la temperatura y se higienizarán las manos de cada persona que ingrese. **Habrá dos salidas: una por el estacionamiento de calle Avellaneda y otra por los portones del extremo norte de los andenes de la Estación Belgrano**.

Para evitar la aglomeración, **los ingresos serán en grupos de hasta cuatro personas**, según los egresos se vayan sucediendo para mantener el cupo máximo de cuatrocientas personas visitando los andenes. Como en toda actividad en el espacio público, **el uso de barbijo y/o tapaboca será obligatorio** y se pedirá que todas las personas guarden entre sí una distancia mínima de 2 metros y no toquen la mercadería exhibida hasta el momento de comprarla. Se pedirá también no permanecer en el recinto durante más de una hora.

<hr style="border:2px solid red"> </hr>

<span style="color: #25282C; margin: 35px;">Para controlar el flujo de personas y evitar aglomeraciones, se sugiere al público asistente que **anticipen sus pedidos** a través de la galería virtual alojada en **Capital Cultural**</span>

<span style="font-family: arial, helvetica; font-size: 14px; font-weight: 500; background: #EBEDEF; margin: 35px; overflow: hidden; padding: 10px;"> [www.santafeciudad.gov.ar/capitalcultural ](https://www.santafeciudad.gov.ar/capitalcultural/ "Capital Cultural")</span>

<hr style="border:2px solid red"> </hr>

Para garantizar el funcionamiento del protocolo de prevención de contagios y el plan de contingencia ante casos sospechosos de Covid-19, la Dirección de Salud de la Municipalidad brindará una capacitación a las y los diseñadores, de manera virtual.