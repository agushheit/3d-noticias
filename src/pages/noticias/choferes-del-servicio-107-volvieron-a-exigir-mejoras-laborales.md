---
category: La Ciudad
date: 2021-11-11T06:00:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/107.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Choferes del Servicio 107 volvieron a exigir mejoras laborales
title: Choferes del Servicio 107 volvieron a exigir mejoras laborales
entradilla: 'Reclaman la recategorización como choferes de ambulancia de emergencia,
  ya que actualmente figuran como servicios generales. '

---
Este miércoles, los choferes de la Subsecretaría de Emergencias y Traslados (SIES 107), tras los reiterados reclamos por su situación laboral, se concentraron en la Plaza del Soldado, para luego movilizarse al Ministerio de Salud hasta la Casa de Gobierno.

 Al respecto, Estanislao Romero, referente del sector, explicó que el pedido concreto que se viene realizando hace 40 años es la creación de un agrupamiento de choferes. “ Queremos que se nos recategorizare como choferes de ambulancia de emergencia y no como estamos ahora, que es servicio generales”.

 En este sentido, Romero precisó que la convocatoria se ha hecho extensiva a los distintos sectores de la provincia que comprende a los trabajadores de la salud porque “chequeamos que en la pluralidad hay más igualdades que diferencias”.

En esta oportunidad, los trabajadores además señalaron, que se encuentran reclamando por los compañeros contratados por Covid, cuyo contrato se termina el 31 de diciembre. Se trata de unos 200 choferes, y algo de 6.000 personas en la provincia. “Queremos enfatizar que sin esos contratos, muchos de nuestro servicios van a ser inviables, ya que ellos llegaron en plena pandemia para cubrir los lugares que estaban vacíos”.

 Siguiendo en esta linea de reclamos, Romero recordó que “en la pandemia a nosotros se nos llamó “escensiales”, y hoy cuando por decreto se termina la emergencia sanitaria, pasamos a ser descartables”.

 Ante esta preocupante situación, desde el sector reiteraron la necesidad de la recategorización de chofer, “para que se regularize la situación, ya que hay gente que está precarizada, manejando la ambulancia que le paga la comuna, o se les paga de otra manera”, señaló el referente Mauricio Acosta.

 Al tiempo que agregó que esto “no solo va a hacer lo remunerativo lo que nos favorecerá, si no que también la regularización de todo el servicio de emergencia”, puntualizó.