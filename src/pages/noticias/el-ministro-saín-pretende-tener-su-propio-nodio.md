---
layout: Noticia con imagen
author: "Fuente: Prensa Mario Barletta"
resumen: El NODIO de Saín
category: Agenda Ciudadana
title: El Ministro Saín pretende tener su propio NODIO
entradilla: El gobierno kirchnerista creó un observatorio para ejercer el
  control de la información, las ideas y las opiniones. Saín, imitando esta
  práctica autoritaria, pretende tener su propio NODIO.
date: 2020-11-25T12:10:09.126Z
thumbnail: https://assets.3dnoticias.com.ar/barletta.jpg
---
Un propio NODIO  desde donde ejercer con total discrecionalidad y absoluto desprecio por la división de poderes, funciones ejecutivas y judiciales. La suma del poder en un ministro. Él pretende controlar todo y a todos, pero ¿quién lo controla a él?

**Lo que se propone es concentrar poder y ganar impunidad para ejercer control sobre los disidentes.** Se trata de un avance inusitado sobre las instituciones que abre la peligrosa posibilidad de armar causas a discreción; el conocido carpetazo, sin la previa dirección fiscal y decisión de un juez que autorice -de ser necesario- la vulneración legal de garantías y derechos constitucionales en el marco de una investigación.

De esta manera, posibilita la utilización arbitraria de la persecución penal y el uso sin control de herramientas de investigación puestas a disposición de intereses particulares. Todo esto, en el marco de un Estado democrático de derecho, es imposible de justificar.

Esta actitud no debería sorprendernos. Su gestión se caracteriza, entre otras cosas, por tener su propia central de inteligencia sin regulación alguna, actuando como fiscal o investigador autónomo, arrogándose facultades que no tiene y que, además -y no es un detalle menor- es un delito en el ejercicio de su función.

El hecho de difundir públicamente información sobre investigaciones criminales, además de no proteger la investigación ni la garantía de los justiciables. El hecho de asegurar que tiene información sobre ciertas actividades ilegales, debería implicar su responsabilidad de denunciar como funcionario público, no alardear de tener acceso a esa información, rodeando de misterio la forma en que la obtuvo.

Ni hablar de las escuchas ilegales a políticos y periodistas, a propios y ajenos. O de la confrontación constante con la policía: no la conduce, la destrata, no la equipa ni la fortalece, lanza acusaciones al aire sin pruebas. En una palabra, no apuesta a la profesionalización de la policía.

Todo lo que viene realizando de esta manera no mejoró ni uno de los indicadores con los que se mide la violencia y la inseguridad. De la paz y el orden que Perotti prometió en campaña, todavía no hay rastros. Muy por el contrario, **tenemos el lamentable récord de un homicidio por día en lo que va del año**.

## **La respuesta de la Legislatura Provincial**

El pasado jueves, la Legislatura Provincial sancionó una ley que busca establecer un marco regulatorio de los denominados "gastos reservados" del Ministerio de Seguridad y del MPA. Estas leyes apuntan al control y transparencia, de cómo se ejecutan los gastos reservados en la Provincia de Santa Fe.

También la legislatura santafesina sancionó una norma que reforma dos artículos de la Ley Orgánica del Ministerio Público de la Acusación (13.013), uno del Servicio Público provincial de la Defensa Pública (13.014) y cinco del Organismo de Investigaciones (13.459). En ella se ratifica el régimen de incompatibilidades y trata de evitar el paso de personal desde el Poder Ejecutivo a esos cuerpos. Ningún funcionario del poder judicial puede ser funcionario del poder ejecutivo y viceversa.

Estas leyes que surgieron tras un acuerdo de distintas fuerzas políticas buscan garantizar una mayor independencia del poder judicial y generar herramientas de transparencia.

A casi un año de asumir el Gobierno de Perotti, podemos decir con claridad que no tiene un Plan para dar más seguridad a los santafesinos. Las leyes enviadas a la legislatura son sólo un reordenamiento de normas y decretos que ya existían y se venían implementando. Y eso no es un Plan de seguridad ciudadana.

## **Alternativa**

**Un observatorio del delito es fundamental, pero debe ser autónomo y autárquico para que cumpla su verdadera función**. Un observatorio debería ayudar a mejorar las políticas, pero también a fiscalizar al Estado en su función de seguridad ciudadana. Debería servir para una mejor rendición de cuentas de la gestión pública. Un observatorio no puede ser un servicio de inteligencia del ministro de seguridad.

Hay que aprender de la experiencia comparada y aplicar políticas basadas en evidencia.

Actualmente existe consenso en torno a la necesidad de desarrollar políticas, programas e iniciativas basadas en evidencia. Es decir, contar con información rigurosa. Esto es, datos de calidad analizados por personas capacitadas técnicamente y garantizando la mayor participación ciudadana, y también el mayor control crítico por parte de universidades, ONG, centros de estudios, etc. de las políticas implementadas.

Por ello, **es imperioso generar mecanismos participativos y de involucramiento de la sociedad en el control del servicio público de seguridad ciudadana**.

Cabe preguntarse de qué manera cumpliría esta importante función una oficina ministerial (el Observatorio) en la que quienes allí se desempeñan tienen dependencia administrativa, técnico-funcional y económica del ministro. Quien tiene que implementar las políticas que deben ser observadas es precisamente el jefe de los observadores.