---
category: La Ciudad
date: 2020-12-03T10:57:34Z
thumbnail: https://assets.3dnoticias.com.ar/terminal.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Volvió a operar la Terminal de Ómnibus Manuel Belgrano
title: Volvió a operar la Terminal de Ómnibus Manuel Belgrano
entradilla: Con un protocolo diseñado por la Municipalidad, este martes arribaron
  los primeros colectivos a la estación, en el marco de la vuelta del transporte de
  media y larga distancia.

---
Este martes 1 de diciembre, la Estación Terminal de Ómnibus Manuel Belgrano volvió a recibir colectivos del transporte de media y larga distancia, luego de que el Gobierno nacional autorizara la reanudación del servicio. 

L**a actividad pudo retomarse en la capital de la provincia**, gracias a que la Municipalidad diseñó y puso en marcha los protocolos necesarios, siguiendo las normativas vigentes.

A pocas horas de la reapertura, **ya funcionan las empresas que cubren el trayecto de la Ruta 1, desde Reconquista**. Se suman, también, aquellas que unen la capital de la provincia con Rafaela, Rosario, Venado Tuerto y las correspondientes zonas de influencia.

Las firmas que deseen volver a operar desde y hacia Santa Fe deben, en primera instancia, solicitar el permiso a la provincia. Posteriormente tienen que informar a la administración de la Terminal con 48 horas de anticipación (por nota o correo electrónico) sus horarios y destinos. 

Del mismo modo deben presentar una declaración jurada en la que conste el cumplimiento de los protocolos. Por otra parte, se comprometen a exhibir la nómina de pasajeros, en caso de ser requerida por cualquier organismo público.

El subsecretario de Movilidad y Transporte municipal, Lucas Crivelli, confirmó que “a partir del martes se ha restablecido el transporte de media y larga distancia desde y hacia la ciudad de Santa Fe, por lo que paulatinamente, las empresas avanzan en este proceso”. 

Según mencionó, hasta el momento son seis las firmas que tramitaron ante la secretaría de Transporte de la provincia las autorizaciones para los viajes, dado que este es el organismo responsable en materia de movimientos dentro del ámbito provincial”.

Para el caso de los viajes interprovinciales, mencionó que se requieren autorizaciones de las dos provincias, además de la correspondiente a la Comisión Nacional de Regulación del Transporte (CNRT). “Una vez que tienen esa autorización, nos notifican con dos días de anticipación el horario de llegada o partida y automáticamente están habilitadas para concretar el viaje. Para ello, la Municipalidad emitió una autorización amplia a la Secretaría de Transporte de la provincia para que, cuando ésta se asegura de que las empresas cumplimentaron los protocolos, la Terminal ya tenga todo adaptado para la recepción”, relató.

Crivelli recordó que las firmas de transporte deben respetar las medidas concernientes al tipo de unidad asignada al traslado y la cantidad máxima de asientos a ocupar por coche, como así también la aireación y sanitización de los vehículos. Además, añadió que cada compañía tiene a su cargo el registro de los usuarios con datos personales, el control de la temperatura al momento del ascenso, la trazabilidad de cada pasajero y la constatación del permiso habilitante para circular.

“Las empresas son las responsables de llevar adelante todo el protocolo de ascenso al colectivo, deben tener el listado de las personas con los correspondientes certificados de circulación y los requerimientos que cada lugar de destino disponga para poder viajar. Al ingresar a la terminal, llevamos adelante un testeo de temperatura dentro del protocolo establecido mientras que, para los descensos, hay un espacio determinado a tal fin con el objetivo de que no haya entrecruzamiento de quienes llegan y quienes salen”, agregó el subsecretario.

**Respecto de las personas que pueden viajar**, señaló que **los decretos nacional y provincial apuntan al personal esencial**, como así también a quienes hayan tramitado los permisos de circulación necesarios.

Sobre el funcionamiento de las boleterías que expenden pasajes, la apertura está sujeta a la decisión de cada empresa, aunque **se recomienda priorizar la venta virtual**, de modo que la gente no deba trasladarse para adquirir su ticket. “Cada compañía define la manera de comercializar los pasajes; todas las boleterías están habilitadas y es la empresa la que establece los horarios de apertura”, explicó Crivelli. No obstante, sugirió a los viajeros que antes de acceder a la Terminal para la compra del boleto, se contacten con la empresa para conocer el modo de expendio elegido”.

En tanto, los negocios ubicados dentro de la estación se rigen por la normativa municipal que les concierne, en función de las habilitaciones tramitadas.

![](https://assets.3dnoticias.com.ar/terminal1.jpg)

## **El protocolo**

**El ingreso al edificio se hace por el hall central**, mientras que el egreso es por el ala sur. También están organizadas las dársenas de modo tal que el ascenso de pasajeros se concreta entre las plataformas 1 y 17 identificadas con el color verde agua, mientras que los descensos se hacen entre la 18 y la 28, de color anaranjado.

![](https://assets.3dnoticias.com.ar/terminal1bis.jpg)

Tanto en el interior del inmueble como en las dársenas de salida y entrada, **se colocaron tótems sanitizantes con la solución líquida indicada por el Ministerio de Salud**. Además, se concreta una desinfección exhaustiva permanente no solo de las zonas de tránsito sino también de los baños, donde se garantizan las medidas de higiene y seguridad. Como medida adicional, se instalaron vinilos de separación en las boleterías y los comercios, y se colocó cartelería informativa.

**Las personas que entran al predio lo hacen con tapabocas sin excepción** y son sometidas al control de temperatura. Se les recomienda ingresar a la Terminal no más de 15 minutos antes de la partida del micro y sin acompañantes, ya que no se permite la permanencia a quienes no cuenten con pasaje adquirido.

En las dársenas, en tanto, se verifica el cumplimiento del distanciamiento y el uso de barbijo, evitando la aglomeración de personas. Adentro, la Comisión Nacional de Regulación del Transporte (CNRT) cuenta con un espacio con vista directa a las plataformas, a fin de mejorar el control sobre los coches.

Finalmente, se habilitó una sala exclusiva de aislamiento para casos sospechosos de COVID-19.

![](https://assets.3dnoticias.com.ar/terminal2.jpg)