---
category: Agenda Ciudadana
date: 2021-09-01T06:00:41-03:00
thumbnail: https://assets.3dnoticias.com.ar/COMERCIO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: "¿Cuánto cobrarán los empleados de comercio desde septiembre?"
title: "¿Cuánto cobrarán los empleados de comercio desde septiembre?"
entradilla: Los empleados de comercio cerraron un acuerdo paritario en abril, y luego
  una recomposición en agosto.

---
El comercio cerró un nuevo acuerdo paritario para acercar los salarios a los índices de inflación previstos para el cierre de año y, además de los nuevos pisos salariales, se acordó un bono de $4.000 que se pagó en el mes de agosto. Pero en septiembre aumentan los sueldos y volverán a ajustarse en el mes de noviembre (9% más, no remunerativo). ¿Cuánto pasarán a cobrar los empleados de comercio?

Con las subas pactadas inicialmente, en septiembre el salario básico para la categoría “Maestranza A” pasa a ser de $56.615,96, más un adicional no remunerativo de $9.058,55, lo que da un total de $65.674.51.

En el caso del “Personal Auxiliar A”, el salario inicial será de $57.436,17; se suma un adicional no remunerativo de $9.189,79, y en total el salario en septiembre quedaría en $66.625,96. El mismo monto le correspondería a un “Vendedor”, según las escalas publicadas por la Federación Argentina de Empleados de Comercio.

En tanto, que un “Administrativo A”, cobrará de básico en septiembre $57.231,15 más un adicional de $14.307,79 no remunerativo, que llevará el salario a $71.538,94 (sin antigüedad).

Un “Auxiliar especializado”, que recién se inicia debería cobrar desde septiembre $57.928,58 de básico, más $4.634,29 para alcanzar $62.562,87; y la categoría “Cajero” recibirá un sueldo inicial de $58.092,62, más $4.647,41 (no remunerativo), para alcanzar $62.740,03.