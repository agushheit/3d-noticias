---
category: La Ciudad
date: 2021-12-12T06:00:26-03:00
thumbnail: https://assets.3dnoticias.com.ar/CLAUSURA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Clausuraron dos bares este viernes y explican los motivos
title: Clausuraron dos bares este viernes y explican los motivos
entradilla: 'El funcionario Guillermo Álvarez afirmó que desde el municipio clausuraron
  bares por incumplimientos y ruidos molestos.

'

---
Continúa el eco de lo sucedido en un bar ubicado en Bulevar Gálvez, en barrio Candioti, donde el accionar violento de vecinos disparó que se discuta desde el municipio la nocturnidad. Guillermo Álvarez, subsecretario de Convivencia Ciudadana de la Municipalidad de Santa Fe, confirmó que este viernes fueron clausurados dos bares en la misma zona y en la costanera.

El primer negocio en ser clausurado fue un parador que queda en la costanera este. Este bar tenía acumuladas varias infracciones, la mayoría relacionadas con ruidos molestos y también por realizar eventos sin autorización municipal.

El segundo bar que sufrió una clausura fue un local de cervezas artesanales ubicado en Sarmiento y Maipú. Desde la Municipalidad informaron que este negocio pidió autorización el jueves a la noche, por ventanilla de eventos, colocar DJ en la vereda. Esta solicitud no fue admitida por el municipio, pero lo hicieron igual. Por este motivo fueron sancionados por colocar un DJ en la vereda a las 00:30hs sin autorización municipal, y por ruidos molestos también.

Las multas van de 80 a 800 Unidades Fijas, que tiene un valor de 45.78 pesos, aproximadamente. Por lo que deberán pagar entre 3.662 y 36.624 pesos. El monto lo dispone el Tribunal de Faltas, que abre el lunes. Cuando el responsable de cada bar abone el dinero correspondiente, los titulares comerciales deben acercarse a la Secretaria de Control a firmar un acta compromiso donde se acuerdan pautas de cumplimiento de la normativa vigente y de convivencia. Una vez realizado esto la Secretaria de Control y Convivencia Ciudadana puede disponer el levantamiento de la clausura.