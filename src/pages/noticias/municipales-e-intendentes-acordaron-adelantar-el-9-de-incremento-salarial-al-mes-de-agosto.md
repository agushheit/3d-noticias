---
category: La Ciudad
date: 2021-08-25T06:00:14-03:00
thumbnail: https://assets.3dnoticias.com.ar/FESTRAM.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa FESTRAM
resumen: Municipales e intendentes acordaron adelantar el 9% de incremento salarial
  al mes de agosto
title: Municipales e intendentes acordaron adelantar el 9% de incremento salarial
  al mes de agosto
entradilla: La medida permitirá dejar los salarios por encima de la inflación. Además,
  no habrá descuentos en las remuneraciones a los trabajadores por las medidas de
  fuerza adoptadas en el reclamo.

---
Este martes los representantes de intendentes y presidentes comunales y la Federación de Sindicatos de Trabajadores Municipales (Festram) acordaron adelantar al mes de agosto el 9% de incremento salarial fijado en las paritarias.

Esta modificación de la fecha de vigencia del tercer tramo de aumento de la política salarial vigente en cada jurisdicción, permitió que los representantes de los trabajadores municipales levanten las medidas de fuerza que se habían dispuesto para esta semana.

“En virtud del acuerdo se adelanta el incremento del 9% a partir del 1° de agosto, lo que resulta que el salario municipal estará sobre el índice inflacionario”, indicó Festram en un comunicado.

Asimismo, se ratificó “el acuerdo alcanzado en marzo, con lo cual se hará la revisión de la situación salarial en función de la evolución de las variables inflacionarias en la fecha establecida en el acta. Se acordó además que no habrá descuentos en las remuneraciones a los trabajadores por las medidas de fuerza adoptadas en el reclamo. El aumento pactado (9%) se trasladará a los jubilados y pensionados del sector”.

Del encuentro participaron representantes de las autoridades locales de Rafaela, Esperanza, Rosario, Santa Fe, Cañada Rosquín y por los paritarios de Festram de los sindicatos de Rosario, Rafaela, Santo Tomé y Capitán Bermúdez.

Luego del encuentro entre los sectores, se llevó a cabo el plenario de secretarios generales, convocado virtualmente, del que participaron 39 sindicatos de Trabajadores Municipales de la Provincia de Santa Fe, miembros de Festram, quienes avalaron unánimemente el acuerdo y dejaron sin efecto las medidas de acción directa dispuestas para los días 26 y 27.