---
category: Agenda Ciudadana
date: 2022-01-14T06:00:10-03:00
thumbnail: https://assets.3dnoticias.com.ar/LARRETA3.png
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: Cruces entre Nación y Ciudad por el home office a estatales para reducir
  el consumo eléctrico
title: Cruces entre Nación y Ciudad por el home office a estatales para reducir el
  consumo eléctrico
entradilla: '"No podemos decir que no vayan a trabajar", afirmó el jefe de Gobierno
  porteño, Horacio Rodríguez Larreta.

  '

---
La decisión de la Casa Rosada de impulsar el home office en los empleados estatales para los próximos dos días para intentar reducir el consumo energético provocó un cruce con la Ciudad, ya que el jefe de Gobierno porteño, Horacio Rodríguez Larreta, rechazó implementar esa decisión y afirmó que tiene a "la gran mayoría de la gente abocada al cuidado de la pandemia".

Ante lo que se espera que sea el pico de la ola de calor, el Gobierno fijó el trabajo remoto para los empleados de la Administración Pública Nacional a partir del mediodía este jueves y viernes "a los fines de reducir el consumo de energía eléctrica".

La medida fue cuestionada por el referente del PRO, quien al ser consultado sobre el tema, afirmó: "No podemos decir que no vayan a trabajar". "Tenemos a la gran mayoría de la gente abocada al cuidado de la pandemia. Ésa es nuestra prioridad hoy y estamos metidos en eso, trabajando todos ahí", agregó Rodríguez Larreta durante la conferencia de prensa tras presentar a Carlos "Chapa" Retegui como nuevo secretario de Deportes de la Ciudad.

Minutos después, la portavoz presidencial, Gabriela Cerruti, pidió que los cortes de energía "no se conviertan en un tema de enfrentamiento ni de grieta". "Esperamos que el Gobierno de la Ciudad se haga cargo de la responsabilidad que tiene con respecto al suministro de la energía en la Ciudad. En este tema queremos ser muy responsables y queremos apelar a la solidaridad y no convertirlo en un tema de enfrentamiento ni de grieta", planteó la vocera de Balcarce 50.

Y concluyó: "Son esos temas en los que todos los argentinos y las diferentes fuerzas políticas tienen que entender que son situaciones en las que todos tenemos que colaborar para que suceda lo mejor para los ciudadanos".

La polémica surgió luego de que el presidente Alberto Fernández firmara el Decreto 16/2022, publicado este jueves en el Boletín Oficial, en medio de la ola de calor que afecta al país.

El decreto estableció que "a los fines de reducir el consumo de energía eléctrica, los días 13 y 14 de enero de 2022, a partir de las doce (12) horas, los agentes de todas las jurisdicciones, organismos y entidades del Sector Público Nacional a los que se refiere el artículo 8° de la Ley N° 24.156 deberán realizar la prestación de servicios mediante la modalidad de prestación de trabajo a distancia, en caso de que ello resulte posible, y se abstendrán de permanecer o concurrir a sus lugares habituales de trabajo".

En la normativa difundida, se señaló que se brindó la instrucción a los distintos organismos para que "implementen las medidas pertinentes a fin de mantener las guardias necesarias para preservar la continuidad de los servicios esenciales".