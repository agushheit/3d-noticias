---
category: La Ciudad
date: 2021-05-31T06:15:58-03:00
thumbnail: https://assets.3dnoticias.com.ar/CODE.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Licitan las obras pendientes en el CODE
title: Licitan las obras pendientes en el CODE
entradilla: La obra tiene un presupuesto oficial de $ 60.624.606,41, con un plazo
  de 180 días.

---
Este lunes a primera hora, desde las 8.30, el gobierno provincial llevará a cabo la licitación pública de las obras de remodelación pendientes del Centro de Observadores del Espacio (CODE), ubicado sobre la Costanera santafesina.

La licitación está a cargo del Ministerio de Infraestructura, Servicios Públicos y Hábitat y el acto se desarrollará en dichas dependencias ubicadas sobre Avda. Almirante Brown 4751, a unos 200 metros del observatorio. El mismo será encabezado por el gobernador Omar Perotti, junto a la ministra del área, Silvina Frana.

El planetario digital será erigido en el predio actual y estará integrado con el edificio del Code: será una cúpula geodésica de 9 metros de diámetro en la que cabrán 50 butacas reclinables para poder observar las imágenes. El proyector emitirá imágenes especialmente hechas para la representación del cielo (sin distorsión del horizonte al cenit) a fin de que el espectador esté en el centro, entre los planetas y los asteroides, y que con las vibraciones del sonido se sienta dentro de la escena. La obra también plantea una intervención de las instalaciones para provocar nuevos recorridos, generar nuevas áreas de proyección en la planta baja, alrededor del planetario digital, y habilitar el patio central como articulador de actividades. El perímetro se transformará con una piel de vidrio, con permeabilidad visual, que invite al uso de los espacios. La terraza será mejorada para hacer observaciones de cielo.