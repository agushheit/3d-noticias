---
category: Agenda Ciudadana
date: 2021-09-13T06:00:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/aescuelaeleccionjpg.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: El protocolo sanitario se cumplió con normalidad y sin mayores contratiempos
title: El protocolo sanitario se cumplió con normalidad y sin mayores contratiempos
entradilla: Las elecciones transcurrieron con algunas imágenes inusuales, como las
  largas filas fuera de los establecimientos donde se votaba para evitar la concentración
  de gente al interior y los adolescentes sin acompañamiento.

---
El protocolo sanitario de prevención de la Covid-19 se aplicó durante la jornada electoral de este domingo sin contratiempos durante las primarias abiertas, simultáneas y obligatorias (PASO) que se desarrollaron en todo el país en el marco de la pandemia, con algunas imágenes inusuales, como las largas filas fuera de los establecimientos donde se votaba para evitar la concentración de gente al interior, como principal contraste comparado con comicios anteriores.  
  
El buen clima en la mayoría del territorio nacional, sobre todo en el área metropolitana de Buenos Aires, donde se concentra más del 40 por ciento del padrón, permitió una buena puesta en práctica del protocolo establecido, en el cual la mayor diferencia con elecciones anteriores fueron las colas de ciudadanos en las calles para evitar la concentración dentro de los lugares de votación y la incorporación de los llamados facilitadores sanitarios.  
  
El "Protocolo sanitario de prevención Covid-19- Elecciones nacionales 2021" elaborado en forma conjunta por la Dirección Nacional Electoral (DNE) del Ministerio de Interior y el Ministerio de Salud incluyó varias medidas de prevención y organizativas e incorporó la figura de facilitadores sanitarios.  
  
En los lugares de votación, las autoridades de mesa solicitaron que las personas mostraran sus DNI y los apoyaran sobre la mesa, en tanto que la mayoría de los votantes cumplió con la premisa de no salivar el sobre para sellar su cierre.  
  
Fueron mayoría quienes optaron por introducir la solapa dentro del cuerpo principal del sobre, mientras que otros, munidos con su plasticola, decidieron pegarlo para asegurarse que la boleta no se saliera del mismo.  
  
En algunas ocasiones las autoridades de mesa hicieron uso de la facultad que les dio el protocolo para pedirles a algunos votantes que se bajaran momentáneamente el barbijo para corroborar su identidad.  
  
A diferencia de otras elecciones, los jóvenes acudieron en la mayoría de los casos solos a los lugares de votación, un hecho que en elecciones anteriores formaba parte del folcklore, ya que iban acompañados de un adulto que los fotografiaba o los filmaba.  
  
De todos modos, no faltaron aplausos para los debutantes de este acto democrático, de parte de las autoridades de mesa y también de los pocos votantes que se encontraban en las filas, ya que para evitar la concentración, no había más de dos personas haciendo fila frente a la mesa.  
  
“Solo en el caso de que sea estrictamente necesario, se permitía el ingreso con acompañantes a los lugares de votación, como parte de las disposiciones que se implementan para evitar la aglomeración de personas”, señalaba el protocolo, que mayoritariamente fue cumplido por el electorado.  
  
En la jornada de hoy se realizó un despliegue de unos 17 mil facilitadores sanitarios, una figura creada en el marco del Protocolo que se encargó de ordenar el ingreso de los votantes, constatar el correcto uso de tapabocas, hacer respetar el distanciamiento social de dos metros y limitar la presencia de personas en los lugares de votación.  
  
La función la cumplió personal de las Fuerzas Armadas o de las Fuerzas de Seguridad nacionales, que en su uniforme llevaban una placa que decía "Facilitador Sanitario" o "FS" para su clara identificación.  
  
Además, cada escuela tuvo la tarea de facilitar el sufragio a los mayores de 60 años y, cada vez, que una mesa se encontraba sin votantes en espera dentro del establecimiento, recorrían las filas convocando a quiénes tuvieran que sufragar en esa urna, lo que era celebrado ya que de esa forma, el votante adelantaba varios lugares.  
  
También se formaron filas con diferentes direcciones para mesas pares e impares, como para que primara el orden.  
  
Los facilitadores tuvieron un rol muy importante para asistencia y para garantizar el orden, impidiendo que se concentrara la gente y se perdiera la conciencia respecto al contexto inusual de pandemia.  
  
Por ejemplo, un policía que estaba a cargo de la seguridad en la puerta de un establecimiento en la ciudad bonaerense de Temperley recibió a un hombre con muletas que rápidamente fue colocado en el primer lugar de la fila para votar por el facilitador sanitario.  
  
En el marco del protocolo se estableció, y tuvo muy buena aceptación, una franja horaria prioritaria para personas que integren grupos de riesgo, entre las 10.30 y las 12.30.  
  
También se cumplió lo previsto por el protocolo en cuanto a la utilización de elementos de protección personal e higiene y los votantes llevaron en su mayoría sus propias biromes para firmar la planilla de votación, aunque en todas las mesas se contó con lapiceras debidamente sanitizadas por si el votante optaba por no llevar la suya.  
  
Las agrupaciones políticas también cumplieron con lo propuesto previamente en lo referente a que, con el objeto de reducir el aforo de los establecimientos de votación, mantuvieran "simultáneamente dentro de cada establecimiento de votación la cantidad mínima necesaria de fiscales partidarios".  
  
Sobre el funcionamiento de la jornada en el contexto atípico de la pandemia, el ministro del Interior, Eduardo ‘Wado’ De Pedro, destacó el "consenso alcanzado en el Congreso nacional para correr cinco semanas la elección" de este domingo y las legislativas de noviembre, debido a la pandemia, lo que "permitió que 12 millones de personas se puedan vacunar" contra el coronavirus en estas últimas semanas.  
  
"De esta manera pudimos ir a votar con más tranquilidad y menos miedo", dijo el funcionario y agradeció a los "millones de argentinos que fueron a votar" durante la jornada de hoy, lo que "refuerza el espíritu democrático y de participación que hay en la sociedad argentina".