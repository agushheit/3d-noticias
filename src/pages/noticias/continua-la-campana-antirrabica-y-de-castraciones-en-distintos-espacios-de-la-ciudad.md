---
category: La Ciudad
date: 2021-07-28T06:15:09-03:00
thumbnail: https://assets.3dnoticias.com.ar/VACUCanes.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Continúa la campaña antirrábica y de castraciones en distintos espacios de
  la ciudad
title: Continúa la campaña antirrábica y de castraciones en distintos espacios de
  la ciudad
entradilla: 'La Municipalidad ya inoculó a 374 animales de compañía en el Parque Federal.
  Esta acción en las plazas, plazoletas y paseos se puso en marcha en marzo y abril
  y ya se aplicaron 1.315 dosis en diferentes espacios. '

---
La Municipalidad avanza de forma sostenida con la campaña de salud animal. Este fin de semana, se instaló un puesto de vacunación antirrábica en el Parque Federal junto a la feria de emprendedores y se inocularon unos 374 animales, en tres horas. Esta propuesta, que se realiza de forma paralela a las castraciones, se lleva a cabo en distintos espacios verdes y públicos de la ciudad y ya se aplicaron 1.315 dosis.

La vacunación se replicó también en las plazas Constituyentes, Pueyrredón, frente a la basílica de Guadalupe y en la plazoleta Fragata Sarmiento. En cada uno de los lugares se instaló un puesto donde médicos veterinarios realizaron las inoculaciones a los santafesinos que se acercaron con sus animales de compañía. El próximo sábado se ubicarán en la plaza Ciudad del Rosario (Entre Ríos y San Jerónimo), desde las 14.

Esta cantidad de aplicaciones se hicieron durante los fines de semana de marzo y abril, luego se suspendieron por las restricciones debido a la pandemia por Covid-19 y se retomaron este fin de semana en el Parque Federal. Es importante destacar que todos los animales a partir de los tres meses deben colocarse la vacuna antirrábica y renovarla anualmente.

El Instituto Municipal de Salud Animal (Imusa) reacondicionó el sector de las instalaciones, ubicadas en Obispo Gelabert 3691, que están destinadas a restituir el sistema antirrábico en la ciudad. Se repusieron las salas de aislamiento, de necropsia, caniles y un registro de seguimiento animal. En ese sentido, contar con un registro de control y seguimiento del animal es fundamental para prevenir esta enfermedad peligrosa para los seres humanos.

**Castraciones**

Paralelamente, la Municipalidad de Santa Fe continúa con el cronograma de la campaña de castración móvil, destinada a animales de compañía. Se recuerda que el servicio es gratuito y se ofrece a todos los vecinos y vecinas de la ciudad, con la intención de controlar la población felina y canina.

La atención se brinda con turnos programados que pueden obtenerse en la web oficial www.santafeciudad.gov.ar/turnos. Allí se debe buscar el apartado denominado “Castraciones”, se elige la opción “Solicitar un nuevo turno” y se completa el formulario con los datos pedidos. Posteriormente se escoge el día, el horario y la sede a la cual concurrir. El servicio se presta de lunes a viernes, excepto feriados, en el horario de 8 a 12, en todos los casos.

**El cronograma prosigue en los siguientes lugares:**

– Distrito Noroeste: La Tablada (Teniente Loza 6970), hasta el 6 de agosto.

– Distrito La Costa: Copa de leche de Los Sin Techos (Manzana 7) hasta el 30 de julio.

– Distrito Suroeste: Vecinal Centenario (Zavalía 721) hasta el 6 de agosto

– Distrito Noreste: Vecinal Altos del Valle (Los Pinos 2492) hasta el 30 de julio.

– Distrito Oeste: Vecinal Barranquitas Sur (Juan Díaz de Solís 4020), hasta el 30 de julio.

– Instituto Municipal de Salud Animal: Parque Garay (Obispo Gelabert 3691), de 8 a 17 horas.

– Instituto Municipal de Salud Animal: Jardín Botánico (San José 8400), de 8 a 17 horas.