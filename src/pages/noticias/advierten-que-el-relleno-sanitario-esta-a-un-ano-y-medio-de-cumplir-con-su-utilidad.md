---
category: La Ciudad
date: 2021-01-05T11:10:31Z
thumbnail: https://assets.3dnoticias.com.ar/050121-relleno-sanitario.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: El Litoral'
resumen: Advierten que el relleno sanitario está a un año y medio de cumplir con su
  utilidad
title: Advierten que el relleno sanitario está a un año y medio de cumplir con su
  utilidad
entradilla: 'Lo advirtieron desde la organización RedCicladores. Plantean la necesidad
  de generar alternativas para el tratamiento de residuos y su disposición final. '

---
«La gestión de residuos tiene muchas aristas en las ciudades, particularmente en lo referido al tema del relleno sanitario. Hay que repensar un modelo alternativo que implique realizar un tratamiento de reducción de los residuos a la mínima expresión», sostuvo Joaquín Azcurrain, coordinador de RedCicladores, una Red de Promotores Ambientales articulada desde el espacio Igualdad Santa Fe y que la conforman organizaciones ambientales, instituciones vecinales, ciudadanos independientes y los recuperadores urbanos de la Asociación Dignidad y Vida Sana.

«Hay plantas que hoy trabajan todo lo que reciben y logran reutilizar un 70% de residuos orgánicos y secos. El Complejo Ambiental (donde se ubica el relleno sanitario) tiene la planta clasificadora, donde se clasifican y compactan vidrios, plásticos, metales por trabajadores de Dignidad y Vida Sana. Lo que está faltando es una planta mecánica de tratamiento de residuos orgánicos, los cuales se pueden compostar y tener diferentes usos: hay municipios en el país que los utilizan hasta para rellenar caminos», analizó Azcurrain.

Otra cuestión que plantea el coordinador de RedCicladores se refiere a la gestión de los líquidos lixiviados. «Una planta de estos líquidos es muy importante. Hasta ahora no se reutiliza ese líquido y se desecha. Es un líquido que puede reutilizarse, obviamente no para consumo humano, pero sí para otros fines, de esa manera se empieza a cerrar el círculo del tratamiento de residuos», señaló.

<br/>

<span style="font-family: Helvetica, sans-serif; font-size: 1.1em; font-weight: 700">**Cerca de cumplir su vida útil**</span>

«Sin dudas, el Complejo Ambiental de nuestra ciudad, popularmente conocido como "relleno sanitario", emplazado en Circunvalación Oeste, constituye una de las plantas más importantes de la región, diseñada para depositar 1,5 millones de toneladas de residuos. Pero sigue estando en debate su vida útil, debido a que este tipo de complejos tienen una duración de no más de 10 años y en 2021 estaría cumpliendo 11 años de vida», indicaron desde la red.

Según fuentes oficiales del Complejo Ambiental, en 2016 más de la mitad de las hectáreas ya estaban en uso. «Si bien la Municipalidad de Santa Fe informó en 2018 que el lugar todavía dispone de celdas para la recepción de residuos hasta 2022, la capacidad hoy se encuentra al límite. Por eso es importante empezar a analizar soluciones usando nuevas tecnologías y pensando una readecuación y/o ampliación de la actual planta», resaltaron desde la red.

«El relleno sanitario está a un año y medio de cumplir con su utilidad», señaló Azcurrain, y agregó que «uno ve esa montaña de basura y asusta». Al ser consultado sobre la propuesta de RedCicladores, el coordinador indicó: «estamos trabajando en una propuesta de gestión integral, que se empieza por la separación en origen; una mejora del servicio de recolección; es decir, un tratamiento en planta que abandone el concepto de relleno sanitario».

El primer paso es generar conciencia ciudadana a través de una fuerte campaña de concientización. Pero también, según Azcurrain, es importante que «haya una ordenanza que implique al sector privado, desde las empresas hasta los comercios, para que se hagan responsables de la cantidad de plásticos que utilizan y generan».

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.1em; font-weight: 700">Balance positivo</span>**

La red, que se creó a finales de julio de este año, tiene dos objetivos: promover la separación domiciliaria de residuos para el cuidado del ambiente, con una perspectiva de inclusión social; y la recuperación de espacios públicos a escala barrial, desde la participación ciudadana genuina.

A modo de balance, desde la RedCicladores informaron que en cinco meses recolectaron aproximadamente 15 toneladas de residuos reciclables en los cuatro puntos que dispone la red, los cuales fueron trasladados a la planta clasificadora. 

Además, realizaron un mapeo colaborativo de 110 microbasurales geo referenciados en todo Santa Fe; y recuperaron más de 10 espacios que eran «tiraderos"» de basura en diferentes barrios de la ciudad (Colastiné, Las Delicias, Acería, Las Flores, Mayoraz, Pompeya, Santa Rosa de Lima, Parque Federal y demás).

Respecto a lo que fue el mapeo de microbasurales, indicaron que recibieron más de 100 denuncias de vecinos de diferentes barrios de la ciudad. «Si bien la dispersión de micro y mega basurales es pareja en todo el territorio urbano, el cordón oeste y noroeste es el más afectado», comentaron desde la red.

«La Municipalidad informó en 2018 que el lugar todavía dispone de celdas para la recepción de residuos hasta 2022, pero hoy la capacidad se encuentra al límite. Por eso es importante analizar soluciones usando las tecnologías y pensando en una readecuación o ampliación de la actual planta», dijeron desde la red.

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.1em; font-weight: 700">Ley de Basura Cero</span>**

El tema del tratamiento de residuos también tiene su dimensión provincial. En este sentido la diputada Agustina Donnet y el diputado Rubén Giustiniani presentaron un pedido de informes relacionado con los avances en el cumplimiento de la Ley Provincial de Basura Cero, que fue aprobado en la penúltima sesión ordinaria de la Cámara de Diputados.

Donnet se refirió a esta solicitud y dijo: «nos interesaba saber si el gobierno Provincial viene velando por el cumplimiento de la Ley de Basura Cero, si los municipios cumplen con la separación de origen y si la disposición de residuos sólidos urbanos se viene realizando según los mecanismos de clasificación que establece la norma". Además, solicitó "contar con información precisa sobre si municipios y comunas informan la existencia de basurales a cielo abierto y cuáles son las acciones que se están tomando para su erradicación».