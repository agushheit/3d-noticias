---
category: Agenda Ciudadana
date: 2020-12-14T11:38:26Z
thumbnail: https://assets.3dnoticias.com.ar/monotributo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Infobae'
resumen: Anticipan un aumento del 31% en las escalas del monotributo desde enero
title: Anticipan un aumento del 31% en las escalas del monotributo desde enero
entradilla: Cómo quedarían los valores y las cuotas mensuales. Se actualizarán según
  la suba de las jubilaciones 2020. Estudian un esquema de “transición” entre las
  categorías más altas y el régimen general de IVA y Ganancias.

---
**Esta semana se confirmó que desde enero el mínimo no imponible y las escalas del impuesto a las Ganancias aumentarán un 35,38%**, por lo que un trabajador soltero con un salario de bolsillo de $74.810 pagará el tributo, mientras que un empleado casado y con dos hijos comenzará a pagarlo a partir de un ingreso neto de $ 98.963.

Pero **aparte de asalariados en blanco, en la Argentina hay 3,6 millones de monotributistas inscriptos**. Para ellos, desde el primer día de 2021 también habrá una actualización de las escalas y las cuotas del monotributo.

Si bien aún no fue comunicado oficialmente, el régimen simplificado se actualizará por el porcentaje acumulado de los aumentos jubilatorios de 2020. Según cálculos privados, esa cifra sería levemente superior al 31%. De esta manera, el incremento sería menor a la inflación de ese período.

La suba exacta alcanzaría el 31,02% precisó a Infobae el contador Marcos Felice. De esta manera, la escala más baja del régimen simplificado (la A), pasará a tener un límite de facturación anual de $273.453, lo que representaría ingresos mensuales promedio de $22.787.

La categoría B pasaría a tener un techo de $410.180, es decir unos $34.181 por mes.

Más adelante, las categorías C y D tendrían, respectivamente, $546.907 y $820.361 de facturación máxima. Para cada caso, implicaría un límite de ingresos mensuales de $45.575 y de $68.363.

La escala más alta de régimen simplificado (la K), contemplaría un techo de facturación anual de $3.418.170. Es decir, que **el ingreso mensual máximo para permanecer dentro del monotributo sería levemente inferior a los $285.000**. **Superada esa cifra, el contribuyente quedaría alcanzado por el régimen general de IVA y Ganancias**.

[![Abrir](https://assets.3dnoticias.com.ar/infogram.jpg)](https://infogram.com/escalas-de-monotributo-2021-1h1749vv377ll6z)

El pago mensual de la cuota de monotributo estará en un rango de entre $2.562 en la categoría inicial (tanto para quienes son proveedores de servicios como para la venta de cosas muebles) y termina en los $19.280 para la escala K. Junto con los topes de facturación, también se actualizarán desde enero un 31% los montos de alquileres devengados y los importes del llamado impuesto integrado a ingresar así como las cotizaciones previsionales.

Según anticipan los especialistas, la actualización de las escalas del régimen simplificado por debajo del nivel de inflación del período implicará que muchos inscriptos deberán pasar a una categoría superior a pesar de no haber facturado más en términos reales. 

“Dado que la actualización ya fue muy baja en 2020 tendremos nuevamente el efecto de atraso en las escalas del monotributo, lo que genera incongruencias dado que un monotributista actualiza sus precios por inflación”, explicó el tributarista Iván Sasovsky. 

El pago mensual de la cuota de monotributo estará en un rango de entre $2562 en la categoría inicial (tanto para quienes son proveedores de servicios como para la venta de cosas muebles) y termina en los $19280 para la escala K El Gobierno analiza algún tipo de modificación al régimen de monotributo para evitar la subdeclaración de ingresos. 

Para eso, buscaría un esquema “puente” que sirva como transición entre las escalas más altas y el régimen general de impuestos, que implica para el contribuyente una carga tributaria mucho mayor. 

“El monotributo habla sobre la discusión de la equidad porque sabemos que muchas veces se ha transformado en un mecanismo de subdeclaración. 

Por eso es necesario generar condiciones para que sea un factor de inclusión, pero también para que ese famoso puente al régimen general no sea tan gravoso para el contribuyente”, dijo a principios de diciembre la titular de AFIP Mercedes Marcó del Pont en un evento organizado por la Unión Industrial Argentina (UIA). 

En la última moratoria amplia puesta en marcha por el Gobierno nacional como parte de las medidas para mitigar el impacto del coronavirus, la AFIP reglamentó en paralelo beneficios para monotributistas que hayan cumplido con el pago de cuotas y de presentación de declaraciones juradas en los últimos tres períodos fiscales. 

Para los monotributistas, **la eximición del componente impositivo del pago mensual se efectuará a partir del período fiscal enero de 2021 y se realizará en cuotas mensuales y consecutivas con un tope de $17.500.** 

**El beneficio varía según la categoría:**

Para las A y B la exención serán enero a junio de 2021, para las categorías C y D se exime 5 cuotas, de enero a mayo; en las E y F serán 4 cuotas de enero a abril, para las categorías G y H se contemplarán 3 cuotas de enero a marzo y para las escalas más altas (I, J y K) se incluirán las de enero y febrero.