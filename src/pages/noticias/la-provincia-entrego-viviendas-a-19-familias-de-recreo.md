---
category: Estado Real
date: 2021-06-02T08:56:48-03:00
thumbnail: https://assets.3dnoticias.com.ar/viviendas-recreo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: La provincia entregó viviendas a 19 familias de Recreo
title: La provincia entregó viviendas a 19 familias de Recreo
entradilla: Sin acto y con protocolos de sanidad, se adjudicaron las unidades habitacionales
  que demandaron una inversión superior a los 70 millones de pesos.

---
El Ministerio de Infraestructura, Servicios Públicos y Hábitat, a través de la Dirección Provincial de Vivienda y Urbanismo (DPVyU), adjudicó las 19 viviendas sin acto protocolar, con el objeto de desalentar la circulación de los vecinos en las inmediaciones.

Tras la entrega, la ministra de Infraestructura, Servicios Públicos y Hábitat, Silvina Frana, transmitió el saludo del gobernador Omar Perotti e indicó: “Esta es una obra que se adjudicó en noviembre de 2019, sobre el final de la gestión anterior, y fue desarrollada y pagada por este gobierno porque lo entendemos como una política de Estado, que va más allá de los colores políticos de los municipios y comunas. Porque el déficit habitacional es grande y se requiere de una firme decisión política para comenzar a revertirlo”.

"Con todas las complicaciones que conlleva la pandemia, tenemos en ejecución, y proyectadas para este año, más de 4 mil unidades habitacionales, lo que demuestra que la vivienda es uno de los principales ejes de la gestión", destacó Frana.

Con relación al compromiso en el seguimiento de la obra, la ministra subrayó que "en todo el proceso de la obra, hubo un seguimiento constante de la presidenta del concejo Noelia Formento y del senador Marcos Castelló para que las tareas avancen. Y eso habla del compromiso y de entender a la vivienda como un derecho que debe garantizarse como política de Estado", concluyó.

Por su parte, el senador Marcos Castelló celebró la decisión política de continuar con la obra desde el inicio de la gestión, a pesar de la pandemia: “Hoy tenemos a 19 familias que van a tener la oportunidad de cambiar su vida y claramente la unidad transformadora es la política. Cuando en campaña hablábamos de que Santa Fe vuelva a ser protagonistas, hablábamos de esto, de devolverle dignidad a la gente con las vivienda, mejorando su calidad de vida. Y esta es la decisión política que nuestro gobernador Omar Perotti ha tomado desde el primer día de su gestión”.

Finalmente, el intendente Omar Colombo celebró la entrega de viviendas a la cual calificó de “histórica para la ciudad ya que hace muchos años que no se entregan viviendas. En noviembre de 2019 comenzaron las obras y hoy se entregan, con una demora debido a la pandemia. Lo importante es que el gobierno de Omar Perotti terminó la obra y se entrega a cada una de las familias”.

**LAS VIVIENDAS**

Las 19 unidades fueron construidas según el proyecto oficial de la dirección provincial de Vivienda y Urbanismo (DPVyU). Tienen aproximadamente 60 metros cuadrados de superficie; constan de dos dormitorios, cocina - comedor, lavadero y baño, con instalaciones de agua fría y caliente en baño, cocina y lavadero, con tanque de reserva de 500 litros instalado en una torre contigua a la vivienda.

Dieciocho fueron desarrolladas en dúplex (planta baja y panta alta), en terrenos de 5 metros de frente y, aproximadamente, 20 metros de fondo, de dos dormitorios. La vivienda es de una sola planta y está ubicada en un terreno esquina, de aproximadamente 10 metros de frente por 20 metros de fondo, adaptada para discapacidad motriz con desplazamiento en silla de ruedas.

Todas las unidades poseen instalación eléctrica en todos los ambientes, instalación de gas natural para cocina, calefón y calefactor, instalación para agua potable (fría y caliente) y para desagües cloacales e instalación pluvial a cordón. Cuentan, además, con instalación de calefón solar.

Asimismo, el baño se entrega completamente instalado, con piso y revestimiento cerámico; la mesada de la cocina es de acero inoxidable; la pared posterior (sobre la mesada) posee revestimiento, al igual que la pared del lavadero, detrás de la pileta de lavar, la que también se entrega con la vivienda. En tanto, las aberturas exteriores son de aluminio pintado, las interiores puertas placa-y el techo de chapa galvanizada ondulada.

Cabe destacar que la vivienda se entrega totalmente pintada, con revestimiento texturado en sus paredes exteriores.