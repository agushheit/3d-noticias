---
category: La Ciudad
date: 2021-08-20T19:00:25-03:00
thumbnail: https://assets.3dnoticias.com.ar/MARCUCCI.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Hugo Marcucci
resumen: “Si nosotros estuviéramos en el municipio en estos momentos, si Mario hoy
  fuera Intendente, los jardines estarían funcionando a pleno"
title: “Si nosotros estuviéramos en el municipio en estos momentos, si Mario hoy fuera
  Intendente, los jardines estarían funcionando a pleno"
entradilla: El candidato a concejal de Juntos por el Cambio, Hugo Marcucci, presentó
  esta mañana su propuesta en Educación.

---
El candidato a concejal de Juntos por el Cambio, Hugo Marcucci, presentó esta mañana su propuesta en Educación, que entre otros puntos plantea declarar la esencialidad de la actividad, reponer la jerarquía de Secretaría dentro de la órbita municipal, ampliar y fortalecer el sistema de Jardines municipales y el Liceo, reeditar el programa SOS Música y el programa Aula Ciudad, y promover fuertemente la formación laboral y de oficios en diferentes barrios de la ciudad. 

Esta mañana, en el Jardín municipal de barrio Coronel Dorrego, el candidato a concejal Hugo Marcucci presentó una serie de iniciativas en Educación. Lo hizo acompañado del candidato a Diputado Nacional y ex intendente de Santa Fe, Mario Barletta, de la referente del sector educativo y ex Secretaria municipal de Educación, Rossana Ingaramo, y de integrantes de la Lista “Un modo de hacer”. 

Durante la presentación a la prensa, Marcucci remarcó que “estamos con la educación desde el primer día. Con estas palabras inauguraba su gestión municipal Mario Barletta allá por 2007”, y agregó: estamos convencidos de que es la principal herramienta para garantizar igualdad de oportunidades.” 

“Sabemos que las ciudades también pueden y deben ocuparse de la educación. Ya lo hicimos y hoy volvemos a hacerlo con una propuesta integradora, para la educación formal e informal, que requiere del compromiso político, público y activo. Y que también concierne a las familias, a las escuelas, a las asociaciones, las industrias culturales, las empresas, y otras instituciones y colectivos, porque la educación es tarea de todos y cada uno”, indicó Marcucci. 

**La propuesta, punto por punto** 

\- Declarar a la Educación como actividad esencial. - Reponer la jerarquía de la Secretaría de Educación. 

\- Sostener y ampliar la propuesta del Sistema de Educación Inicial Municipal. “Nos comprometemos firmemente a trabajar para que no se abandone el acceso a la educación temprana de nuestros niños y niñas. Se trata de un proyecto educativo con un fuerte impacto como política de integración”, señaló quien encabeza la lista respaldada por Mario Barletta. 

\- Velar para que se efectúen los depósitos de las partidas presupuestarias correspondientes a este Fondo de Asistencia Educativa. 

\- Fortalecer el Liceo Municipal, en tanto espacio de desarrollo, enseñanza, contención. y capacitación. 

\- Reeditar el proyecto SOS Música que concibe a la música como una opción de integración social y acceso cultural, a partir de la creación y fortalecimiento de orquestas infanto–juveniles. “La experiencia nos muestra que participaron en diversas ediciones más de 200 niños y jóvenes de cuatro escuelas de la ciudad: Gálvez, Falucho, Malvinas Argentinas y Juan Manuel de Rosas, que han brindado conciertos junto a la Banda Sinfónica Municipal”, recordó Marcucci. 

\- Continuar con propuestas pedagógicas y de contenidos que cuenten con información y actividades relativas a la cultura local, en articulación con una red de circuitos e itinerarios urbanos. En este sentido, reeditar Aula Ciudad, que durante años fue un vínculo con las escuelas, a través de la producción de fascículos a distribuir entre los chicos y los docentes.

 - Promover Derechos ciudadanos realizando acciones y campañas informativas y formativas de promoción de derechos en toda la ciudad. “Queremos también llevar adelante programas educativos en las escuelas, de seguridad vial, discriminación, educación ambiental, tenencia responsable de mascotas, y de todas aquellas temáticas que implican proyectos de cualquier índole para la ciudad y que requieren involucramiento y educación ciudadana”, agregó Marcucci. 

\- Vinculación Trabajo – Educación- Barrios. Para generar cambios sustantivos en la situación laboral de los desempleados en general y de los jóvenes en particular, se requiere desarrollar una coordinación permanente entre la Municipalidad y las escuelas técnicas y de formación laboral, el Ministerio de Trabajo de la Nación, el de la Provincia, sindicatos, organizaciones empresarias, y crear en los espacios barriales, ya constituidos, lugares de encuentro, de armado de REDES, de preparación para el trabajo, capacitación en oficios de rápida salida laboral, etc. 