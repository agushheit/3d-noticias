---
category: La Ciudad
date: 2021-07-13T08:38:47-03:00
thumbnail: https://assets.3dnoticias.com.ar/castraciones.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad de Santa Fe
resumen: Continúa la campaña municipal de castraciones
title: Continúa la campaña municipal de castraciones
entradilla: Con el objetivo de acercar el servicio a toda la ciudad, la Municipalidad
  informa los lugares que se incorporan al cronograma desde hoy y hasta el viernes
  23 de julio.

---
La Municipalidad estableció el cronograma de castración móvil hasta el 23 de julio, destinada a animales de compañía. Se recuerda que el servicio es gratuito y se ofrece a todos los vecinos y vecinas de la ciudad, con la intención de controlar la población felina y canina.

La atención se brinda con turnos programados que pueden obtenerse en la web oficial www.santafeciudad.gov.ar/turnos. Allí se debe buscar el apartado denominado “Castraciones”, se elige la opción “Solicitar un nuevo turno” y se completa el formulario con los datos pedidos. Posteriormente se escoge el día, el horario y la sede a la cual concurrir. El servicio se presta de lunes a viernes, excepto feriados, en el horario de 8 a 12, en todos los casos.

En algunos lugares el servicio ya arrancó la semana pasada y concluye el viernes próximo. Para el resto, el esquema previsto desde el lunes 12 al viernes 23 de julio es el siguiente:

– **Distrito Oeste**: Los Hornos-Los Sin Techo (Gdor. Freyre 5678) desde el 12 hasta el 23 de julio.

– **Distrito Noroeste:** Vecinal Scarafia (Alberti 5501) hasta el 16 de julio

– **Distrito La Costa:** Vecinal Alto Verde (Manzana 5) se viene haciendo desde el 5 y se extiende hasta el 16 de julio.

– **Distrito Norte**: Vecinal Favaloro (Lamadrid 10550) hasta el 16 de julio.

– **Distrito Este-Noreste**: Vecinal Guadalupe Oeste (Risso 1745) hasta el 16 de julio; Vecinal Altos del Valle (Los Pinos 2492), desde 12 hasta el 23 de julio.

– **Distrito Suroeste**: Manzana 6 del Fonavi San Jerónimo, hasta el 16 de julio.

– **Instituto Municipal de Salud Animal**: Parque Garay (Obispo Gelabert 3691).

– **Instituto Municipal de Salud Animal:** Jardín Botánico (San José 8400).