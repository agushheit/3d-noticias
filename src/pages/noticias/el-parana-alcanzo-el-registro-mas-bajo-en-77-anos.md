---
category: La Ciudad
date: 2022-01-12T06:00:31-03:00
thumbnail: https://assets.3dnoticias.com.ar/setubalseca.png
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: El Paraná alcanzó el registró más bajo en 77 años
title: El Paraná alcanzó el registró más bajo en 77 años
entradilla: 'Así lo informó el Instituto Nacional del Agua a través de sus nuevos
  pronósticos. Este martes al mediodía, prefectura midió a la altura de Santa Fe -0,22
  metros

'

---
Este martes al mediodía se produjo un hecho histórico. El Paraná alcanzó, a la altura de Santa Fe, la menor marca en 77 años. En el puerto local, prefectura midió -0,22 metros, un registro que solo es superado por los -0,28 metros anotados en 1945 y por el récord de 1944 cuando se midió -1,03 metros.

La bajante histórica no solo está marcada por sus registros negativos, sino también por la extensión en el tiempo, ya que el fenómeno viene prevaleciendo desde hace dos años.

La laguna Setúbal padece la situación. La bajante modificó por completo el tradicional espejo de agua de esta capital; alteró el paisaje, su fauna y la actividad náutica. Los pilotes del Colgante lucen casi desnudos y amenazan con mostrar su base.

En un mes, el Paraná descendió 1,26 metros en el puerto local. El proceso se aceleró en enero; en lo que va de este año (once días) cayó 0,58 metros. Ello se debe a que el 1 de enero midió 0,36 metros, contra los 0,22 anotados este martes al mediodía.

El escenario actual no es bueno y el nuevo pronóstico informado del Instituto Nacional del Agua (INA) no es alentador. Este martes, el organismo advierte en sus previsiones que "no se espera una recuperación en las próximas semanas" y que "prevalece una condición general de disminución de los caudales entrantes al tramo argentino del río Paraná. Los niveles fluviales continuarán en la franja de aguas bajas. La tendencia climática al 31 de marzo es desfavorable".

El INA pronostica como peor escenario para el 18 de enero, un altura de -30 metros; una medida que podría mantenerse en la última semana del mes.

En el último informe sobre "Posibles escenarios hidrológicos en la cuenca del Plata" durante el primer trimestre, el INA señala con respecto a la cuestión hidrológica: "La bajante del río Paraná, de características extraordinarias por su persistencia, seguirá siendo motivo de especial monitoreo. La perspectiva al 31/marzo/2022 no permite esperar un rápido retorno a la normalidad, con probabilidad de extenderse durante el próximo otoño".

Los profesionales señalan que la actual perspectiva climática obliga a revisar permanentemente las previsiones. En ese sentido, alertan que "si bien la situación actual mantiene distancia de la observada en la sequía / bajante del año 1944, no se descarta la posibilidad de un acercamiento a tales condiciones extremas en los próximos meses".

El documento destaca con relación al Paraná en Santa Fe que "prevalece la condición de niveles extremadamente bajos. Promedio observado mensual: 8.000 m3/s, este valor continúa muy por debajo de lo normal. En las próximas semanas continuará con el mismo comportamiento y con perspectiva de mantener valores muy inferiores durante todo el verano. La tendencia climática, con horizonte en el 31/marzo próximo, no permite esperar una mejora sostenida en las lluvias regionales, además de niveles inferiores a los medios correspondientes a esta altura del año.