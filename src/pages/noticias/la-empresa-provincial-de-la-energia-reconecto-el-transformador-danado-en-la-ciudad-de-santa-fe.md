---
category: La Ciudad
date: 2021-12-22T06:00:45-03:00
thumbnail: https://assets.3dnoticias.com.ar/reansformador.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Empresa Provincial de la Energía reconectó el transformador dañado en
  la ciudad de Santa Fe
title: La Empresa Provincial de la Energía reconectó el transformador dañado en la
  ciudad de Santa Fe
entradilla: 'Se trata de un nuevo transformador que llegó del sur provincial para
  reemplazar el que se incendió este martes por la madrugada. Aseguran que por la
  noche estará normalizado el servicio.

'

---
Este martes por la madrugada, el incendio de un transformador en la Estación Transformadora Santa Fe Norte, ubicada en la esquina de Gorriti y Aristóbulo del Valle, dejó una gran parte del norte de la ciudad sin luz.

Luego de una ardua labor de los bomberos, que trabajaron en el lugar para sofocar el fuego , las cuadrillas de la EPE ingresaron al predio de la Estación Transformadora para realizar los trabajos necesarios y restablecer el servicio.

Personal de EPE ya puso en servicio el transformador n° 1 de la estación Norte. Ahora el área distribución, comenzará en forma progresiva, con la rehabilitación del servicio eléctrico.