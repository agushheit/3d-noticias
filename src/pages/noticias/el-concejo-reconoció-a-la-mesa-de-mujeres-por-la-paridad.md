---
layout: Noticia con imagen
author: "Fuente: Concejo Municipal de la Ciudad de Santa Fe"
resumen: Mesa de Mujeres por la Paridad
category: Estado Real
title: El Concejo reconoció a la Mesa de Mujeres por la Paridad
entradilla: El Concejo distinguió la labor de este espacio multisectorial en
  favor de la sanción de la Ley provincial de Paridad de Género.
date: 2020-10-30T19:43:49.135Z
thumbnail: https://assets.3dnoticias.com.ar/mesa-mujeres-paridad.jpg
---
En la última sesión, el Concejo Municipal realizó un reconocimiento a las integrantes de la Mesa de Mujeres por la Paridad, entidad que nuclea a políticas de los distintos espacios con representatividad en la provincia, y que fue clave en la lucha para la sanción de la Ley de Paridad de Género provincial.

La iniciativa fue de la concejala Laura Mondino (FPCyS) y en el acto estuvieron presentes integrantes de la Mesa, algunas de manera presencial y otras de modo virtual; así como funcionarias de los estados provincial y municipal.

En la oportunidad, el cuerpo legislativo le otorgó un reconocimiento a la acción de la Mesa de Mujeres por la Paridad por su invalorable participación y promoción en la lucha por la efectiva conquista de los derechos de las mujeres en todos los espacios de la vida pública a través de la Ley de Paridad de Género.

La Mesa por la Paridad nació en 2016. Se trata de un espacio horizontal y pluripartidario que acompañó las distintas iniciativas legislativas y diseñó estrategias para lograr la aprobación definitiva. Al respecto, la concejala Mondino consideró que “esta ley es histórica porque representa un paso hacia una sociedad más igualitaria. Y porque es la demostración efectiva de que las mujeres, cuando nos proponemos cosas, somos capaces de sortear las diferencias y tejer un entramado transversal para conseguirlas”.

“Esto es un triunfo del movimiento de mujeres que repercutirá en una construcción democrática más justa y equilibrada. Una ley que fue cajoneada durante años en el Senado. Y que ante cada cajoneo, ante cada ninguneo que cosechó a lo largo de este tiempo, sembró más y más semillas de lucha en las mujeres que hacemos política”, consideró Mondino y agregó: “Cuando hablamos de una percepción feminista del mundo hablamos de eso: de otros modos de construir, también en la política. La sororidad es una herramienta política. Nos queda muchísimo camino por recorrer. Pero hoy estamos siendo reconocidas como iguales. Es una deuda histórica la que ha sido saldada”, finalizó.

Por otra parte, la concejala Jorgelina Mudallel destacó que “no hay más que palabras de agradecimiento para la lucha del movimiento de mujeres que han dado durante tantos años. Hoy dimos un paso hacia la igualdad, hoy tenemos una provincia más democrática. Es una lucha transversal que trasciende todos los colores políticos y que hoy finalmente podemos celebrar esta Ley”.

De la misma manera, la concejala Luciana Ceresola agregó que “en esta lucha solo se ve la punta del iceberg pero no se ve todo lo que hay debajo del agua, el trabajo, la militancia, las frustraciones. No es fácil el ámbito de la política, no es fácil para las mujeres en política y esta conquista llega en un año muy complicado pero demuestra que cuando la lucha es legítima no importa el contexto sino que siempre se va a llegar a un buen resultado. Siempre que haya más mujeres en política será una mejor política”.

En tanto, la concejala Valeria López Delzar se pronunció y sostuvo que “es un gran avance de Santa Fe hacia la igualdad. Esta Ley exige la paridad no solo para todos los poderes del estado sino también para los partidos políticos, las asociaciones, los colegios profesionales, para la integración de cada uno de estos espacios. Este es un logro que se debe a la militancia y a la lucha colectiva del movimiento de mujeres y en especial a esta Mesa de Mujeres por la Paridad Santa Fe que nació allá por 2016. Esta sanción es producto del trabajo sororo, colectivo, multipartidario de las distintas mujeres”.

Asimismo, la concejala Inés Larriera sumó su palabra para destacar el trabajo de todas las mujeres después de “tantos años de lucha. Las abrazo, felicitaciones en nombre de todas las mujeres, estamos muy emocionadas por este reconocimiento. Y también con la esperanza de que esto sienta un precedente y pueda permear en el ámbito de todas las instituciones y organizaciones. Esto abre una puerta no solo para el ámbito político y legislativo, sino que será un mojón para poder avanzar en todos los espacios que las mujeres queremos, debemos y merecemos participar porque es nuestro derecho”.

Por su parte, la concejala Mercedes Benedetti afirmó: “No solo felicitarlas sino dejar sentado que estos reconocimiento no hacen más que poner en papel o en palabras lo que es lucha y un proceso de muchísimo tiempo. No olvidamos que hubo muchas compañeras que lucharon desde el principio y que no lograron ver este final, que no es un final sino un mojón para seguir. Creo que estos procesos son largos, a veces son angustiantes, frustrantes, dolorosos pero en este momento festejamos a lo que se llegó. Es un cambio que nos llevó mucho tiempo pero con más razón reconocer como un logro de la hermandad, de la sororidad, del compañerismo, de la lucha, del compromiso. Abrazarlas y agradecerles por representar a tantas mujeres”.

En la misma línea, la concejala Laura Spina resaltó que es “una injusticia menos eso es lo que significa paridad. Las mujeres hemos estado tradicionalmente sub-representadas en el espectro político y esto hace que haya mayor igualdad en la República Argentina y en nuestra provincia. Este es un triunfo del colectivo de mujeres para todas las mujeres. Felicitaciones y una gran celebración por este logro que lleva más de 20 años de lucha”.