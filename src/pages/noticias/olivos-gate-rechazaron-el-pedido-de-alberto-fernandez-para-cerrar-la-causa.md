---
category: Agenda Ciudadana
date: 2021-12-02T06:15:19-03:00
thumbnail: https://assets.3dnoticias.com.ar/OLIVOSGATE.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: 'Olivos Gate: rechazaron el pedido de Alberto Fernández para cerrar la causa'
title: 'Olivos Gate: rechazaron el pedido de Alberto Fernández para cerrar la causa'
entradilla: 'El Presidente está investigado por la celebración del cumpleaños de su
  pareja, Fabiola Yañez, durante la cuarentena estricta.

'

---
El juez federal de San Isidro, Lino Mirabelli, rechazó el planteo del Presidente, Alberto Fernández, para cerrar tempranamente la causa por el "Olivos Gate" por inexistencia de delito.

Mediante una resolución de 21 páginas, el magistrado dispuso la continuidad de la investigación por violación a las disposiciones de prevención del contagio por el Covid-19.

Alberto Fernández está investigado por la realización en la Quinta de Olivos de la celebración del cumpleaños de su pareja, Fabiola Yáñez, durante la cuarentena estricta.

“El sumario se encuentra todavía en una etapa preliminar en la que se recopilan elementos para despejar la materialidad de los hechos y sus posibles repercusiones jurídicas, sin que concurra el carácter manifiesto y evidente que es necesario para la procedencia de la excepción por atipicidad”, sostuvo el juez.

El presidente Fernández había planteado una ·”excepción de falta de acción por inexistencia de delito”, argumentando que como no se habían producido contagios por la reunión, el “peligro abstracto” de la conducta no había derivado en un perjuicio concreto.

Pero el juez Mirabelli replicó que en este tipo de situaciones se trata de hechos “peligrosos para determinados valores jurídicos de la sociedad, sin que se exija, desde el punto de vista normativo, la producción material de ese resultado de peligro”.

“Lo que no podría admitirse es que en los delitos de peligro abstracto falte el tipo siempre que se pruebe que a posteriori no resultó peligro concreto, pues ello contradiría el fundamento político-criminal de los delitos de peligro abstracto, que ha de verse en la conveniencia de no dejar a juicio de cada cual la estimación de la peligrosidad de acciones que con frecuencia son lesivas”, insistió.

Para el juez, que desestimó un planteo de Alberto Fernández –profesor de Derecho Penal de la UBA- “la absoluta exclusión de la peligrosidad típica no se presenta evidente ni manifiesta en el hecho denunciado (un evento con la concurrencia de varias personas durante la vigencia del ASPO)”.

“A partir de un análisis preliminar, su peligrosidad, objetivamente considerada, no podría ser descartada –en forma palmaria– sino que, eventualmente, merecería ser contrastada y/o debatida a través de elementos que exceden los límites propios de este mecanismo y se ajustan a otro ámbito procesal”.

Por esa razón, rechazó al cierre anticipado de la causa y despejó el camino para avanzar con la investigación, que podrá –o no- descartar la existencia de delito.

“En el estado preliminar que atraviesa la encuesta, la materialidad, así como la eventual adecuación jurídica del hecho no aparecen claramente definidos –de manera palmaria e inequívoca– en el sentido que requiere la excepción deducida”, subrayó Mirabelli.