---
category: Estado Real
date: 2021-06-16T08:54:45-03:00
thumbnail: https://assets.3dnoticias.com.ar/billetera.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: 'Billetera Santa Fe: La Provincia anunció una campaña de promoción de ventas
  por el día del padre'
title: 'Billetera Santa Fe: La Provincia anunció una campaña de promoción de ventas
  por el día del padre'
entradilla: Además el programa, que ya cuenta con más de 500 mil usuarios, suma a
  las perfumerías entre los rubros que tienen beneficios pagando con la app.

---
En el marco del Programa Billetera Santa Fe, el Gobierno provincial, a través del Ministerio de Producción, Ciencia y Tecnología anunció este martes una campaña de promoción de ventas con motivo del día del padre. Esta instancia prevé beneficios de reintegro del 30% en locales de calzado, indumentaria, regalería, marroquinería y perfumerías durante todos los días hasta el domingo 20 de junio.

El anuncio fue realizado por el ministro Daniel Costamagna; y el secretario de Comercio Interior y Servicios, Juan Marcos Aviano, en un shopping de la ciudad de Santa Fe, donde también informaron la incorporación del rubro perfumerías al programa.

Durante la presentación, Costamagna expresó que “para esta fecha emblemática, que tiene que ver con la familia, y en un contexto de dificultades, desde esta gestión de Gobierno se hace un gran esfuerzo compartido con el comercio”. Agregando también: “Hemos ampliado para esta fecha el rubro perfumerías, que era un viejo anhelo, ya que varios comercios de la provincia lo habían solicitado y además el sector de regalería que resulta muy útil para un momento como este. Sin lugar a dudas, siempre estamos abiertos a escuchar opiniones y propuestas, y la adhesión de perfumerías tiene que ver justamente con eso. Estamos constantemente articulando con las entidades comerciales de toda la provincia”.

Por otra parte, el ministro destacó: “Al igual que los programas que tienen que ver con la producción, empleo, arraigo, en todas sus instancias, se delinearon a largo plazo, por lo que no tienen un tiempo de caducidad”.

“Billetera Santa Fe está llegando a los 7.000 millones de pesos en transacciones y cuenta con más de 500 mil usuarios que ya han utilizado el sistema y casi 10 mil comercios adheridos”, subrayó.

“Es una forma muy fuerte de traccionar la producción y de acompañar al sector comercial que fue de los más afectados en todo este contexto. La idea es acompañar, estar presentes y esto nos parece una manera concreta de tratar de ayudar a mitigar esta situación”, concluyó.

Por su parte, Aviano confirmó que “se han detectado conductas abusivas en rubros de productos no esenciales, pero estamos llevando adelante los reclamos correspondientes con la directora de Defensa del Consumidor”.

Seguidamente destacó que “la provincia ya lleva invertidos 1.500 millones de pesos en los reintegros, pero 300 millones de pesos los ha puesto el comercio que también hace un esfuerzo en este contexto difícil de pandemia. Por esto entendemos que se está acompañando al sector con esfuerzo del estado provincial”.

Además de los mencionados funcionarios, de la actividad participaron la directora provincial de Promoción de la Competencia y Defensa del Consumidor, María Betania Albrecht, autoridades de Plus Pagos, del Nuevo Banco de Santa Fe y representantes del sector comercial.

**BILLETERA SANTA FE**

Billetera Santa Fe es un programa de beneficios que busca incentivar la demanda de bienes y servicios mediante el otorgamiento del 30 por ciento de reintegro en cada compra realizada en comercios adheridos al programa, en los rubros alimentos, indumentaria, calzados, juguetería, bares, restaurantes, farmacias, perfumerías, turismo y un 20 por ciento en electrodomésticos.

El reintegro es solventado por el gobierno de Santa Fe en un 85 por ciento, y el 15 por ciento restante está a cargo del comercio. El dinero se acredita a las 24 horas hábiles de realizada la compra en el "Saldo Virtual de la Billetera", con un tope de reintegro de 5.000 pesos por mes.

Los interesados deben registrarse en [https://www.santafe.gob.ar/ms/billeterasantafe/](https://www.santafe.gob.ar/ms/billeterasantafe/ "https://www.santafe.gob.ar/ms/billeterasantafe/") para obtener un usuario y contraseña.