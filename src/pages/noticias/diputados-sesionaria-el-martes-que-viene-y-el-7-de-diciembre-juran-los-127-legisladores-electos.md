---
category: Agenda Ciudadana
date: 2021-11-26T06:00:03-03:00
thumbnail: https://assets.3dnoticias.com.ar/camara.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: Diputados sesionaría el martes que viene y el 7 de diciembre juran los 127
  legisladores electos
title: Diputados sesionaría el martes que viene y el 7 de diciembre juran los 127
  legisladores electos
entradilla: 'Este viernes saldría la convocatoria para tratar la ley para el desarrollo
  del cannabis medicinal y el cáñamo industrial, y la prórroga hasta el 23 de noviembre
  de 2025 de la ley de Emergencia de Tierras Indígenas. '

---
La Cámara de Diputados sesionaría la semana que viene con un temario consensuado entre el oficialismo y la oposición, a partir de proyectos que ya tienen dictamen, en tanto que el 7 de diciembre fue convocada la sesión preparatoria para la jura de los legisladores electos el 14 de noviembre pasado y para la designación de las autoridades del cuerpo para el nuevo período parlamentario.

Según supo NA de fuentes parlamentarias de Juntos por el Cambio, este viernes se oficializará la convocatoria a una sesión en la que se trataría, entre otros proyectos, la ley que crea un "Marco regulatorio para el desarrollo de la industria del cannabis medicinal y el cáñamo industrial" y la prórroga hasta el 23 de noviembre de 2025 de la ley de Emergencia de Tierras Indígenas, que permite frenar los desalojos de poblaciones aborígenes.

El Frente de Todos pretende a su vez incorporar el tratamiento en la sesión de la ley enviada por el Poder Ejecutivo sobre "presupuestos mínimos de protección ambiental para la gestión integral de envases y reciclado inclusivo", pero esta iniciativa despierta el rechazo del grueso de Juntos por el Cambio.

De hecho, en la comisión de Presupuesto y Hacienda los representantes de la oposición firmaron un dictamen de minoría por sus objeciones respecto del valor de la tasa ambiental ("Un impuestazo encubierto") que fijó el oficialismo a ser abonada por las y los productores responsables de los envases puestos en el mercado.

Es por eso que la inclusión de esta iniciativa en el temario podría hacer caer la sesión si el Frente de Todos no se asegura el quórum previamente con sectores minoritarios de la oposición, e inclusive con legisladores propios que terminan ahora su mandato y que podrían eventualmente ausentarse.

Hay otras iniciativas con dictamen que no tendrían inconvenientes en ser parte de la agenda de la sesión como la nueva ley de VIH, que incorpora dentro de los tratamientos gratuitos a las hepatitis virales, la tuberculosis y las infecciones de transmisión sexual (ITS), y que incluye nuevos derechos previsionales, como jubilación anticipada o prestaciones especiales para quienes padezcan estas enfermedades.

También podría ver luz verde el proyecto que extiende por 50 años el fomento de industrias culturales a través de fondos especiales que surgen de asignaciones específicas de  
ciertos impuestos, así como la iniciativa para crear un régimen transitorio de reintegros impositivos para la adquisición de las unidades que prestan el servicio de vehículos automotores de alquiler con taxímetro.

Asimismo, podría votarse en el recinto el proyecto que crea una campaña nacional para la promoción de la donación voluntaria de plasma inmune proveniente de personas que padecieron Fiebre Hemorrágica Argentina (FHA) en la zona endémica del territorio Nacional Argentino.

El martes 7 de diciembre a las 12:00 fue convocada la sesión preparatoria en la que tomarán juramento los 127 diputados nacionales electos en los comicios intermedios recientes, para la designación de las autoridades del cuerpo y de cada una de las bancadas para el período parlamentario que se iniciará el 10 de diciembre.

Fuentes parlamentarias del oficialismo adelantaron a NA que durante diciembre (mes hasta el cual serán extendidas las sesiones ordinarias del Congreso) el Frente de Todos impulsa el tratamiento de una agenda de temas económicos que incluyen incentivos fiscales para la construcción, una ley para que los créditos hipotecarios se ajusten por variación salarial y no por UVA, una nueva ley de Hidrocarburos, un proyecto para la promoción del turismo y la gastronomía, una iniciativa vinculada al beneficios impositivos para la agroindustria, una ley para la industria automotriz y autopartista; y posiblemente la ley de Presupuesto 2022 y una nueva ley de alquileres.