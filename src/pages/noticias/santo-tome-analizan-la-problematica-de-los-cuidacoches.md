---
category: La Ciudad
date: 2021-07-22T06:00:26-03:00
thumbnail: https://assets.3dnoticias.com.ar/CUIDACOCHES.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Santo Tomé: analizan la problemática de los cuidacoches'
title: 'Santo Tomé: analizan la problemática de los cuidacoches'
entradilla: Distintos sectores sociales y políticos analizaron herramientas para mejorar
  las condiciones de vida de los cuidacoches y su relación con los santotomesinos.

---
Este miércoles al mediodía, se realizó una nueva reunión de la comisión de estudio sobre la problemática de los cuidacoches del Concejo municipal de Santo Tomé. La misma se llevó a cabo en la Plaza Libertad y contó con la participación de diferentes estamentos de la sociedad civil y del Estado.

Quién convocó el encuentro, fue la presidenta del cuerpo legislativo y concejala por el PS, Gabriela Solano. "Con esta instancia dimos curso a lo que, en su momento, fue el objetivo de esta comisión, el estudio de la problemática y también a la contención social de los cuidacoches", sostuvo la edil.

"Está fue una fase de intercambio, reflexión y diálogo que busco darle visibilidad a la situación de los trapitos y contribuir a tener una mejor convivencia social en la ciudad de Santo Tomé", continuó agregando Solano.

Participaron del encuentro Susana Luque, de la Oficina de Empleo del municipio; el párroco José Milesi, de la parroquia Inmaculada Concepción; Matias Carpinetti, de la organización comunitaria C.A.M.co y un nutrido grupo de cuidacoches.

"Desde los diferentes estamentos se ofrecieron herramientas que permitirán mejorar las condiciones de vida de los cuidacoches. Desde C.A.M.co ofrecieron ayuda profesional para la rehabilitación psicosocial; la Oficina de Empleo Municipal puso a disposición el acceso a capacitaciones y desde el concejo tomamos nota de aquellas personas que necesitan ayuda social", detalló Gabriela Solano y finalizó: "Desde nuestra banca consideramos que es muy importante que legislemos con políticas y acciones que incluyan a todos los habitantes".