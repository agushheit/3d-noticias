---
category: Agenda Ciudadana
date: 2021-08-12T06:15:18-03:00
thumbnail: https://assets.3dnoticias.com.ar/CRISISHIDRICA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Tomás Rico para El Litoral
resumen: 'Crisis del agua: los tres semáforos de riesgo que mira Nación y cuál es
  el plan de acción'
title: 'Crisis del agua: los tres semáforos de riesgo que mira Nación y cuál es el
  plan de acción'
entradilla: " Se desplegarán cuatro Comités de Operaciones en la región litoral por
  si se complejiza la provisión del recurso. La mayor preocupación es el abastecimiento
  de agua a poblaciones lejanas al río."

---
La bajante histórica del río Paraná, producto de una sequía prolongada, trajo consecuencias importantes para la provisión de agua potable y desafía a los gobiernos a que encuentren planes de acción y así garantizar el suministro del recurso vital. Tal es así que desde finales de julio el gobierno Nacional decretó la emergencia hídrica para la región de la cuenca del Paraná.

"La problemática del abastecimiento de agua potable puede afectar a poblaciones ribereñas que bordean al Paraná, pero también tenés las poblaciones interiores de las provincias, esas son mi mayor preocupación porque se proveen de arroyos que son afluentes del Paraná y hay localidades que potabilizan agua por perforaciones, por ejemplo, Curuzú Cuatiá (en Corrientes). O en el Impenetrable Chaqueño donde no se puede suministrar agua de perforaciones porque el agua tiene mucho arsénico, por eso hay que armar una logística para proveer de agua a esas poblaciones con camiones cisternas", explicó Gabriel Gasparutti subsecretario de Gestión de Riesgo y Protección Civil de la Nación, en una entrevista con El Litoral.

Por este motivo y para estar preparados ante situaciones críticas que puedan ocurrir, el organismo nacional realiza controles diarios y permanentes a través del Sistema Nacional de Monitoreo de Emergencia, que funciona en la Secretaría de Gestión de Riesgo y Protección Civil. En este paso de monitoreo, las autoridades recopilan datos del Instituto Nacional del Agua; Prefectura Naval Argentina; el Servicio Meteorológico Nacional; de la Secretaría de Obras Hidráulicas del Ministerio de Obras Públicas.

En estas últimas semanas, Gasparutti recorrió localidades ribereñas como Paraná, Posadas, Puerto Iguazú, Resistencia, Corrientes, entre otros puntos. "Tengo un panorama completo de cada lugar. Para el viernes estamos convocados en Casa Rosada por el Jefe de Gabinete (Santiago Cafiero) para seguir evaluando la situación de emergencia hídrica", mencionó el subsecretario, quien es oriundo de Reconquista, y señaló que el propio Cafiero les solicitó que le planteen tres escenarios posibles para analizar el riesgo, sobre todo para la provisión de agua potable.

"Se hizo un relevamiento de la capacidad de logística de las Fuerzas de Seguridad y las Fuerzas Armadas por si necesitamos desplazar recursos, como camiones cisternas, plantas móviles de potabilización de agua", resaltó el subsecretario de Gestión de Riesgo y Protección Civil de la Nación.

**Tres escenarios**

Gasparutti denominó a estos escenarios como "el semáforo" para la previsión del riesgo. "Son escenarios que no tienen cambios bruscos como un incendio o un terremoto, sino que se mueve muy lentamente, pero hay que monitorearlo permanentemente", aseguró.

Las tres situaciones que se prevén son: que la situación se mantenga como hasta ahora; el segundo es que empeore a niveles de que haya población en riesgo que se pueda quedar sin agua; y el tercero es un escenario de catástrofe.

¿Qué se hará si esto ocurre? "Se hizo un relevamiento de la capacidad de logística de las Fuerzas de Seguridad y las Fuerzas Armadas por si necesitamos desplazar recursos, como camiones cisternas, plantas móviles de potabilización de agua", contó Gasparutti y mencionó que se han establecido cuatro comandos de operaciones para desplegar en territorio (denominados Comité Único de Operaciones -CUO-) en Corrientes, que permite desplazarse a Chaco, Formosa y Misiones; otro estará en Santa Fe capital, "donde la provincia tiene una realidad menos compleja por el sistema de acueductos que tiene; otro en Paraná porque Entre Ríos tiene una situación distinta, sin sistema de acueductos y con muchas poblaciones interiores que se abastecen de pequeños afluentes que se están quedando sin agua; y otro comando de equipos de operaciones para Zárate, ya que está cerca de Buenos Aires y ahí se pueden desplegar recursos rápidamente", enumeró el funcionario.

Estos CUO ya están diagramados y planificados para cuando la situación del "semáforo de riesgo" se ponga en "rojo" y haya que actuar de forma rápida.

**Obras y financiamiento**

El Ministerio de Obras Públicas de la Nación puso a disponibilidad 1.000 millones de pesos para obras de infraestructura de captación de agua, algunas de las obras ya se están ejecutando y otras que han sido solicitadas, se evalúan en el Enohsa (Ente Nacional de Obras Hídricas de Saneamiento), a la espera de su aprobación.

Santa Fe ya presentó los requerimientos de fondos para ejecutar obras de infraestructura. "Algunas ya se están ejecutando en Rosario, con pontones flotantes, bombas flygt, Assa (Aguas Santafesina) está trabajando en esto", informó el subsecretario de Gestión de Riesgo y Protección Civil de la Nación.

Respecto a la falta de planes de contingencia para responder ante sequías extraordinarias, teniendo en cuenta que el país está acostumbrado y preparado para actuar frente a inundaciones, Gasparutti expresó: "Hay disponible un crédito del BID (Banco Interamericano de Desarrollo) de 300 millones de dólares para emergencia de terremotos e inundaciones. Ahora se está negociando y gestionando para que esa disponibilidad se pueda utilizar en un escenario de sequía, gestiones en las que está trabajando Cancillería".