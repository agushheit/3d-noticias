---
layout: Noticia con imagen
author: "Fuente: Notife"
resumen: Temporada de playas
category: La Ciudad
title: Guardavidas advierten que no se respeta la postergación del inicio de
  temporada de playas
entradilla: La Municipalidad y la Provincia acordaron que la temporada inicie en
  diciembre, pero el calor hizo que la gente concurra igual a las playas. Sin
  guardavidas, ¿Quién controla el cumplimiento de los protocolos?
date: 2020-11-12T13:56:55.925Z
thumbnail: https://assets.3dnoticias.com.ar/playas2.jpg
---
Con el foco puesto en la seguridad sanitaria de la población, y en un momento álgido de la pandemia por Covid-19 en la ciudad debido al aumento de casos positivos, la semana pasada la Municipalidad de Santa Fe y el gobierno provincial decidieron postergar el inicio de la temporada de verano, que cada año se realiza el 15 de noviembre, con motivo del aniversario de la ciudad. Sin una fecha definida, se dijo que sería en el mes de diciembre próximo.

En la reunión llevada a cabo entre las partes, de la que participaron el intendente Emilio Jatón y la ministra de Salud provincial, Sonia Martorano, entre otros, se anunció que este 2021 habrá playas, colonias de vacaciones, clubes, natatorios, actividades al aire libre y espectáculos en Santa Fe, pero todo estará supeditado al comportamiento epidemiológico del virus por la pandemia. También se informó que por el momento se están evaluando cada uno de los protocolos para las habilitaciones.

En ese contexto, pese a la preocupante situación sanitaria, como es habitual cada año cuando se avecina la temporada de playas, muchos santafesinos ya comenzaron a asistir a los balnearios y solarium de la ciudad. La Municipalidad concesionó los paradores y todavía se están realizando las tareas de acondicionamiento de cada lugar. Pero ello poco les importa a quienes concurren a tomar sol o a bañarse en la laguna Setúbal.

**La preocupación de los guardavidas**

Esta situación preocupa a los guardavidas que, por lo antedicho, todavía no iniciaron sus tareas habituales en las playas santafesinas. Ellos son, cada año, los encargados de proteger a turistas y demás asistentes a las playas y balnearios de la ciudad. “Aparte de la cadena de contagios masiva que puede darse luego de lo sucedido durante el último fin de semana, en el que muchas personas asistieron a las playas de la ciudad y del resto de las ciudades de la provincia, sobre todo a la Rambla de Rosario, sin respetar ningún protocolo y sin contar con seguridad, puede haber peligros, debido a la baja altura del río”, advirtió Sergio Berardi, desde el Sindicato de Guardavidas (Sugara Santa Fe). “La Rambla colapsó y hubo poco respeto por los protocolos y los controles”, fue el titular del diario La Capital de Rosario.

Por ese motivo es que los guardavidas reclaman que se anticipe su presencia en las playas y poder evitar así el descontrol y los accidentes por la imprudencia de la ciudadanía. “La gente ya inauguró la temporada y se apropió del espacio público”, advirtió Berardi. “A nosotros nos preocupan los posibles ahogamientos, más allá de que estamos a favor de frenar la cadena de contagios del virus”.

“Vemos muy lejos el mes de diciembre, por lo que debería haber una apertura gradual antes para garantizar la seguridad de la gente. Tenemos por delante tres fines de semana intensos. ¿Qué opción le queda al Estado? ¿Poner policías y generar conflicto con los jóvenes?”, se preguntó luego el dirigente que representa a los guardavidas.

**El protocolo**

La Municipalidad de Santa Fe ya elevó a la Provincia el protocolo local a aplicar en playas y solarium para que sea evaluado, ajustado y definido por las máximas autoridades sanitarias. Asimismo, el lunes se conoció que ya fueron aprobados los protocolos a nivel nacional. “Ante el nuevo escenario que dispone esta emergencia sanitaria global y su impacto económico y social en la actividad turística, el Ministerio de Turismo y Deportes de la Nación, junto con las provincias a través del Consejo Federal de Turismo, elaboraron la “**[Guía de pautas y recomendaciones COVID-19 para la gestión de playas públicas](https://assets.3dnoticias.com.ar/Protocolo-de-playas.pdf)**”, se informó.

Los principales objetivos de dicho protocolo nacional son: “Implementar medidas para resguardar la salud y bienestar de las personas; mitigar y evitar la propagación del virus; acompañar las medidas sociales que las autoridades recomienden o establezcan; y garantizar la continuidad de la actividad turística”.

“Creo que hay una disociación entre lo que plantean los gobiernos y lo que está ocurriendo en la realidad”, señaló Berardi, quien agregó luego también “la incertidumbre laboral que tienen los guardavidas respecto de la temporada”.

**¿Sin balnearios?**

Debido a la bajante de la laguna Setúbal (río Paraná), la temporada pasada no fueron habilitadas las playas como balnearios. Sólo se pudieron disfrutar como solarium, sin ingresar al agua. Esto fue debido a la presencia de pozos profundos en las orillas. Este año se repite el mismo panorama, al que se le suma la pandemia. Y todavía no se anunció la modalidad de habilitaciones.

**\-¿Qué medidas sanitarias deberán tener en cuenta los guardavidas?**

\-Los guardavidas deben respetar y hacer respetar el distanciamiento social. También deben contar con mayores elementos de protección personal y capacitarse para utilizarlos -comenzó respondiendo Berardi, y anticipó: “La semana que viene realizaremos un taller práctico -dijo-. Además, cambia la forma de prestación de los primeros auxilios y un eventual RCP”.

**\-¿Cómo se actúa en un rescate por ahogamiento con el protocolo Covid?**

\-Se debe diferenciar primero entre el guardavida húmedo y el seco. El que realiza el salvamento trata de no enfrentar a la víctima cara a cara. Se debe mantener de espaldas en todo momento y establecer con los brazos una distancia prudente, dentro de las posibilidades. Mientras que el guardavida seco debe alistarse con el equipo de protección en la orilla y continúa con los primeros auxilios. Todo ello es lo que vamos a entrenar la próxima semana.

Luego, mencionó Berardi que los torpedos (boyas de rescate) deberán ser reemplazados por tablas de rescate con manijas (similares a las de surf, pero más largas) con capacidad para dos personas. “Los torpedos ya no sirven más en este contexto”, indicó.