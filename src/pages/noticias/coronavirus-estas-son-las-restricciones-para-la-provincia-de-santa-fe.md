---
category: Estado Real
date: 2021-04-09T07:26:48-03:00
thumbnail: https://assets.3dnoticias.com.ar/covid-amba.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Coronavirus: Estas son las restricciones para la Provincia de Santa Fe'
title: 'Coronavirus: Estas son las restricciones para la Provincia de Santa Fe'
entradilla: Se mantendrá la presencialidad en las escuelas con todos los protocolos
  vigentes teniendo en cuentas que la provincia ya vacunó a la totalidad de docentes
  inscriptos.

---
El gobierno de Santa Fe, informa las nuevas medidas de prevención y contención sanitaria en el marco del decreto de necesidad y urgencia nacional, que comenzará a regir a partir del día 9 de abril y hasta el 30 de abril inclusive.

Esta medida establece para todo el territorio provincial, como acción preventiva ante el aumento de contagio, la suspensión momentánea hasta el 30 de abril de las siguientes actividades:  
\-Actividades y reuniones sociales en domicilios particulares, salvo para las personas convivientes y para la asistencia de personas que requieran especiales cuidados.

\-Actividades y reuniones sociales en espacios públicos al aire libre de más de veinte (20) personas.

\-La práctica recreativa de cualquier deporte en lugares cerrados donde participen más de diez (10) personas o que no permita mantener el distanciamiento mínimo de dos (2) metros entre participantes; con la salvedad en el caso de las competencias oficiales, las que podrán desarrollarse siempre y cuando cuenten con protocolos aprobados por las autoridades sanitarias competentes.

\- Discotecas, salones de fiestas y actividades de salas de juego en casinos y bingos.

Se permiten con un coeficiente máximo de ocupación del treinta por ciento (30%) de las superficies cerradas en los establecimientos dedicados a las siguientes actividades oportunamente habilitadas con sus correspondientes protocolos:  
\- Realización de todo tipo de eventos culturales, sociales, recreativos y religiosos.  
\- Cines, teatros, clubes, centros culturales y otros establecimientos afines.  
\- Locales gastronómicos (bares, restaurantes, etc.).  
\- Gimnasios.

La totalidad de las actividades que están permitidas deberán ajustar sus horarios de desarrollo a los fines de posibilitar el cumplimiento de lo dispuesto en el Decreto Nacional, en cuanto establece el permiso de circulación vehicular en la vía pública a la estrictamente necesaria para realizar actividades definidas como esenciales, entre las cero (0) y las seis (6) horas, todos los días de la semana.

**LOCALES GASTRONÓMICOS**  
En el caso de los locales gastronómicos (comprensivo de bares, restaurantes, heladerías, y otros autorizados a funcionar como tales, con concurrencia de comensales), se permitirá el ingreso hasta las veintitrés (23) horas, a los fines del cumplimiento de la previsión indicada en el párrafo anterior.

**MUNICIPIOS Y COMUNAS**  
Las autoridades municipales y comunales podrán disponer en sus respectivos distritos, en consulta con el Ministerio de Salud, otras medidas respecto de los horarios de circulación nocturna que las establecidas. Sin perjuicio de ello, las autoridades provinciales podrán disponerlas, como así también la suspensión temporaria de actividades habilitadas, cuando las condiciones epidemiológicas así lo exijan para localidades determinadas o para la totalidad del territorio provincial.