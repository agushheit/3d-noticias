---
category: La Ciudad
date: 2021-06-13T06:00:24-03:00
thumbnail: https://assets.3dnoticias.com.ar/DREI.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Cerca de 1.400 contribuyentes estarán exceptuados de la tasa municipal y
  el Drei
title: Cerca de 1.400 contribuyentes estarán exceptuados de la tasa municipal y el
  Drei
entradilla: Se exceptuará del Drei y la tasa municipal a varios sectores que representan
  1.400 contribuyentes de actividades como gimnasios y otros.

---
El intendente Emilio Jatón envió al Concejo Municipal de Santa Fe un paquete de medidas de alivio para los sectores más golpeados por la pandemia y las actuales restricciones. Se estima que alcanzaría los 30 millones de pesos y que abarcaría a casi 1.400 contribuyentes, entre los que se encuentran gastronómicos, agentes de viajes, profesores deportivos y transportistas escolares.

El secretario de la Producción de la Municipalidad de Santa Fe, Matías Schmuth, explicó que este mensaje tiene que ver con una segunda etapa de algunas medidas tomadas en octubre del año pasado para llegar a los sectores que momentáneamente estaban impedidos de trabajar o que lo podían hacer de manera reducida.

“Muchos de los sectores que hoy se van a ver beneficiados por esta medida se vieron beneficiados en ese momento porque no cambió para ellos mucho la situación en materia de desarrollo de su actividad”.

En este sentido dijo: “Nos parece que es una medida necesaria que requiere un esfuerzo muy grande del municipio. Las arcas municipales sienten esta situación que se está atravesando y el esfuerzo que se hace para poder poner a disposición de quienes se están perjudicando es muy grande, pero nos parecía que era el momento de hacerlo y que había que llegar a los sectores más golpeados”.

El secretario de la Producción explicó que algunas acciones se pueden estimar en cifras superiores a 30 millones de pesos pero que hay otras que son variables, como la del Derecho de Registro e Inspección (Drei), que dependen de la actividad económica y no es un número concreto que se pueda tener.

“Lo importante de esto es que se llega a 1.400 contribuyentes que es un número importante para la cantidad de padrones que tenemos activos en la ciudad y que ronda el 15%, y que son los sectores que vienen más golpeados”, dijo Matías Schmuth.

**Alivio en medio de la segunda ola**

El funcionario reiteró: "Lo que queremos todos es que las actividades vuelvan pero mientras tanto hacemos un gran esfuerzo para que ellos puedan tener una carga menos”.

Según Schmuth, la extensión de la tasa tendrá lugar desde enero a junio. “Cuando hablamos de las extensiones de tasa estamos llegando a los rubros hoteleros, a las agencias de viaje de turismo, a los gimnasios, al fútbol 5. Lo venimos trabajando mucho con las distintas cámaras y las asociaciones porque era necesario”.

“Otros rubros que se van viendo afectados por cómo ha estado la situación son los transportes escolares, que sin la presencialidad de los colegios se los va a exceptuar de la tasa de fiscalización que ellos abonan”, dijo el funcionario, y agregó: “Tenemos por ejemplo el rubro gastronómico que puede trabajar, pero en una modalidad que le reduce mucho sus posibilidades. Se los va a exceptuar en la tasa bromatológica y en el uso de espacio público”.

También se refirió a los gimnasios y canchas de fútbol 5. “Mas allá de que vuelva o no la actividad, estamos dándole a los gimnasios la exención en todos los tributos al municipio, porque ellos pagan tasa y Drei y no lo van a hacer. Aunque tenga sabor a poco, es el esfuerzo que puede hacer el municipio”.

"Elegimos el Ministerio de Salud porque parece ser que los que toman las decisiones son ahí y queremos que nos digan cuál es el fundamento, por qué se cierran los gimnasios", dicen los representantes de la Cámara de Gimnasios de Santa Fe.

**¿Se retoma la actividad?**

Con respecto a la reapertura de las actividades ante la finalización del último DNU, Schmuth aseguró: “Estamos convencidos que las actividades tienen que desarrollarse con los protocolos que hemos preparado”, y considera que “se aprendió este año y hay que aplicarlos”.

“Hay alternativas para los gimnasios y espacios cerrados de medidores de dióxido de carbono que se están utilizando, más el uso del espacio público. Cuando se pudo hacer uso del espacio público rápidamente nos juntamos con ellos (los gimnasios) y sacamos una resolución para que les permita hacerlo de manera ordenada”, remarcó.

Finalmente dijo: “Esperemos que pronto podamos estar hablando de que las actividades están funcionando de buena manera porque eso va a significar también que la cuestión está mejor y que el plan de vacunación avanza”.