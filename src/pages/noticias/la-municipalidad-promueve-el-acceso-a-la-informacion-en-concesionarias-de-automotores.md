---
category: La Ciudad
date: 2021-08-12T06:00:17-03:00
thumbnail: https://assets.3dnoticias.com.ar/CONCESIONARIAS.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Municipalidad promueve el acceso a la información en concesionarias de
  automotores
title: La Municipalidad promueve el acceso a la información en concesionarias de automotores
entradilla: La intención es que los consumidores conozcan sus derechos a la hora de
  comprar un vehículo.

---
La Municipalidad de Santa Fe, desde la Oficina de Información y Protección del Usuario y el Consumidor, trabaja para acompañar, asesorar y defender los derechos de los santafesinos y las santafesinas. En ese sentido, se recuerda que la Ordenanza Nº 12.644 determina la obligación de comunicar de forma clara y precisa, los productos o servicios que se prestan en los concesionarios de automotores en sus diferentes formas (nuevos/usados).

Los locales que comercializan vehículos deben exhibir con carácter obligatorio y en un lugar visible para el público, un cartel que consigne el derecho a una información clara y precisa sobre los productos. También, las modalidades de contratación y la forma de pago o el sistema de financiación o ahorro ofrecido por la empresa.

Vale destacar que el formato y el texto están disponibles en la página web del municipio [https://santafeciudad.gov.ar/wp-content/uploads/2021/07/Derecho-Consumidor_Afiche-30](https://santafeciudad.gov.ar/wp-content/uploads/2021/07/Derecho-Consumidor_Afiche-30 "https://santafeciudad.gov.ar/wp-content/uploads/2021/07/Derecho-Consumidor_Afiche-30")×30-1.pdf, a fin de facilitar el proceso y cumplir con la exhibición. El letrero consigna la leyenda que establece la ordenanza referida y los datos de contacto de la oficina municipal de información y protección del usuario y el consumidor.

Con ese objetivo, responsables de la Dirección de Derechos y Vinuclación Ciudadana recorrieron por diferentes empresas concesionarias, a las cuales se les hizo entrega del cartel a exhibir. Además, se generó un encuentro con ACARA, la institución que representa a todos los Concesionarios Oficiales de Automotores en la República Argentina, para redoblar esfuerzos y promover articuladamente información clara y precisa.

Franco Ponce de León, Director de Derechos y Vinculación Ciudadana, mencionó que “es de suma importancia conocer la información de manera clara y accesible. Por ello, desde la Municipalidad estimulamos acciones para el empoderamiento de los usuarios y consumidores”. Asimismo, destacó “la buena predisposición de las autoridades de ACARA para la convocatoria y el diálogo, y la puesta en marcha de esta norma que favorece el acceso a la información”.

Para obtener mayor información, los ciudadanos pueden dirigirse a la Dirección de Derechos y Vinculación Ciudadana de la Municipalidad de lunes a viernes de 7.15 a 13.00 hs. en Salta 2840, comunicarse a través de Whatsapp 54 3425315450, por teléfono al 4574119 y 0800 444 0442, o por mail a of.inquilinos@santafeciudad.gov.ar.