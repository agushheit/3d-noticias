---
category: La Ciudad
date: 2020-12-14T13:20:22Z
thumbnail: https://assets.3dnoticias.com.ar/fluviales.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Diario UNO'
resumen: 'Vuelven los colectivos a Paraná a partir del lunes: horarios y protocolos'
title: 'Vuelven los colectivos a Paraná a partir del lunes: horarios y protocolos'
entradilla: 'Se reanuda el servicio entre Santa Fe y Paraná. Los protocolos, los permisos
  y certificados que se exigirán para viajar. '

---
Se pone fin a casi nueve meses de corte total de comunicaciones interprovinciales debido a la pandemia del Covid-19.

**A partir del lunes regresa el servicio de transporte de pasajeros a nivel nacional**. Será bajo protocolos sanitarios y con servicios que irán progresivamente recuperándose en función de la demanda. Entre otras rutas y recorridos, se recupera así el servicio que une la ciudad de Paraná con la de Santa Fe.

# **Servicio de colectivos Santa Fe - Paraná**

Un dato para tener en cuenta es la capacidad de los colectivos que pueden ser utilizados, según una resolución del gobierno entrerriano: “Será en un 100% de ocupación en el recorrido que une Paraná y Santa Fe. Ahí podrán viajar todos los usuarios sentados y con ocupación plena de la unidad. En cambio, para el resto de los recorridos será en un 80%”, graficó Néstor Landra, secretario de Transporte de la Provincia de Entre Ríos.

Al respecto, las empresas prestadoras del servicio (**Etacer y Fluviales**) indicaron que en las páginas web o Facebook se podrán consultar los horarios.

En días hábiles, complementándose ambas prestaciones, habrá frecuencias de micros cada 30 minutos, ya que cada una de ellas operará cada una hora. Habrá diferencias en las grillas de horarios, para los días sábado y domingo.

<br/>

***

Los pasajeros deberán contar con elemento de protección que cubra boca, nariz y mentón sin excepción y deberán portar o exhibir en su dispositivo móvil el Certificado Único Habilitante para Circulación – Emergencia Covid-19 y/o el permiso emitido por la aplicación Cuidar.

***

<br/>

**Etacer** brindará su primer servicio desde Paraná, el lunes a las 4.30. De Santa Fe partirá a las 5.20 para llegar a las 6.10. Cada 60 minutos se pondrá a disposición un nuevo servicio hasta las 18.30. Por la noche el servicio se retoma a las 20 y 22. El último colectivo desde Santa Fe parte a las 22.50.

Para el sábado y domingo, el primer servicio desde la cabecera paranaense se da a las 6, con una alternancia de dos horas entre cada servicio hasta las 22. El último coche desde Santa Fe partirá a las 22.50.

En tanto que **Fluviales del Litoral SA** ofrecerá su primer servicio desde Paraná desde las 5. De Santa Fe partirá a las 5.50 para llegar a las 6.40. Cada 60 minutos se pondrá a disposición un nuevo servicio hasta las 19. Por la noche el servicio se retoma a las 21 y a las 23, El último colectivo desde Santa Fe partirá a las 23.50. Para el sábado y domingo, el primer servicio desde la cabecera paranaense será a las 5, con una alternancia de dos horas entre cada servicio hasta las 23. El último coche desde Santa Fe partirá a las 23.50.

“**La recomposición de los servicios nacionales es algo que se irá dando progresivamente**. Estamos satisfechos de poder recuperar en cierta forma la normalidad, para que la gente pueda disponer de esta actividad y los empresarios vuelvan a poner en funcionamiento toda su infraestructura”, manifestó el ministro de Planeamiento, Infraestructura y Servicios de Entre Ríos, Marcelo Richard.

# **Los permisos necesarios**

La autorización de los servicios estará limitada para el uso exclusivo de personas habilitadas a la circulación o tránsito; o que deban acompañar a familiares o personas allegadas por razones de salud o para realizar algún trámite particular, todo en esto en cumplimiento a la normativa de emergencia producida por la autoridad sanitaria nacional o provincial.

Para ello **los usuarios deberán tramitar el certificado de circulación correspondiente o portar documento que acredite la necesidad del traslado**.

También se podrá acreditar el traslado con fines turísticos a través de las certificaciones que establezcan las autoridades nacionales.

Por su parte, las empresas prestatarias del servicio de transporte público interurbano deberán informar a las secretarías de Transporte provinciales los horarios de los servicios y frecuencias que dispondrán en el marco de la emergencia sanitaria.