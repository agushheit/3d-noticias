---
category: Agenda Ciudadana
date: 2021-01-25T09:20:15Z
thumbnail: https://assets.3dnoticias.com.ar/alberto.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Infobae
resumen: 'La suspensión de las PASO y el indulto a los presos políticos: la pulseada
  interna que exhibe las diferencias en el Gobierno'
title: 'La suspensión de las PASO y el indulto a los presos políticos: la pulseada
  interna que exhibe las diferencias en el Gobierno'
entradilla: 'Los gobernadores del PJ se plantaron ante el kirchnerismo y le pidieron
  a Alberto Fernández que se suspendan las elecciones primarias debido a la pandemia. '

---
La convivencia es compleja. Nadie lo niega dentro del Frente de Todos. Lo asumen con naturalidad, aunque, la mayoría de las veces, buscan bajar el tenor de los conflictos internos relativizando la importancia de las diferencias que tienen. El último fin de semana en el Gobierno volvieron a exponerse dos caminos distintos y bien marcados. Un mismo gobierno, diferentes posiciones y un Presidente que busca el equilibrio con obsesión.

El viernes a la noche, Alberto Fernández incluyó en el temario de las sesiones extraordinarias del Congreso el proyecto de ley que suspende las PASO. Fue un guiño directo a los gobernadores - de todos los frentes políticos -, que venían pidiendo con tenacidad el debate para bajar las elecciones, un hecho que el kirchnerismo mira con reparos porque pretenden que se desarrollen con normalidad.

Detrás del proyecto para suspender las PASO existe una pulseada entre dos socios estratégicos dentro del Frente de Todos: los gobernadores del PJ y La Cámpora. Los mandatarios tienen sobrados motivos para no querer que se lleven adelante los comicios. Algunos políticos y otros epidemiológicos. Algunos que fueron planteados frente al Presidente y otros que circulan en las conversaciones privadas entre ellos.

En el plano político consideran que la suspensión de las PASO evita la polarización en las elecciones generales y los beneficia como oficialismos provinciales. Desde que se instalaron en el sistema electoral, las primarias se convirtieron en una especie de encuesta que anticipa con fidelidad cuál será el resultado de la elección general. En segundo término, entienden que si sacan las PASO obligan a la oposición a discutir una candidatura de unidad para hacer frente al oficialismo. Esa discusión puede - ya han ocurrido casos - derivar en dos candidaturas del mismo espacio político.

![Alberto Fernández junto a Sergio Uñac, uno de los gobernadores que impulsan la suspensión de las PASO (Presidencia)](https://www.infobae.com/new-resizer/BIYYORcYLlJ4HRMQs9I1ZostADI=/420x236/filters:format(jpg):quality(85)//cloudfront-us-east-1.images.arcpublishing.com/infobae/YJFUCWAFMNDPVAAR5KNC3PWJIE.jpg)

En el plano sanitario, advierten que la circulación masiva de personas para cumplir con el acto electoral multiplicará el riesgo de contagio. Para agosto, cuando se llevarían a cabo las elecciones, Argentina estaría en pleno proceso de vacunación. Un operativo llevado adelante durante el invierno, cuando las enfermedades respiratorias vuelvan a recrudecer, ya es de por sí una complicación logística. Sobre todo teniendo en cuenta que la intención del Gobierno es dejar atrás, a través del avance de la vacunación, la posibilidad de implementar aislamientos estrictos, lo que implica que el movimiento de gente sea casi normal.

Además, se agrega la necesidad de reducir los gastos que implica la logística del sistema electoral. En el caso de que se hagan las PASO, el gasto es doble. Los mandatarios le pidieron al Presidente suspender las elecciones en dos oportunidades. Se plantaron en la escena política con unas posición conjunta. En el peronismo la postura es mucho más que una simple objetivo estratégico o sanitario. Hay una disputa de fondo con el kirchnerismo, que pretende que los comicios se hagan sin contratiempos. Ellos también tienen sus motivos. Quieren ganar lugares en los distintos escalones del Estado y crecer territorialmente a nivel federal.

Los gobernadores del PJ y La Cámpora son socios por conveniencia, no por convicción. No creen en la misma forma de hacer política. Tienen distintas miradas sobre la política exterior que debería llevar adelante Argentina e interpretan el vínculo con el círculo rojo con matices bien diferentes, por poner solo dos ejemplos. Son dos proyectos políticos diferentes que se unieron para poder desbancar a Mauricio Macri del poder. El objetivo se cumplió. Pero, como sabían de antemano que iba a suceder, la convivencia se complica con el paso de los meses.

Los gobernadores no hubiesen intentado expropiar la empresa Vicentin, ni buscarían definiciones condescendientes para describir al gobierno autoritario de Nicolás Maduro en Venezuela. Tampoco cuestionarían con crudeza a los empresarios en forma pública ni librarían una batalla contra el sector agropecuario. No pedirían la libertad de Amado Boudou, que está condenado en diferentes instancias judiciales, ni hablarían de presos políticos. Son dos miradas diferentes sobre una misma realidad. Son dos gobiernos dentro de uno.

![El líder del Frente Renovador y presidente de la Cámara de Diputados, Sergio Massa (Maximiliano Luna)](https://www.infobae.com/new-resizer/LT--ZpixOw7-Gd8WEnZUIEwse9E=/420x236/filters:format(jpg):quality(85)//cloudfront-us-east-1.images.arcpublishing.com/infobae/Y34OVLXEMFHE3FNVDTSU2H6AAU.jpg)

Un año antes de las elecciones cerca de 10 gobernadores, junto a Sergio Massa, crearon Alternativa Federal, un espacio político, con sello peronista, que buscaba transformarse en una alternativa política a Cambiemos, pero sin tener el liderazgo de Cristina Kirchner. Los mandatarios provinciales se encolumnaron en ese esquema con la intención clara de dejar sepultada en el pasado a la actual vicepresidenta. No compatibilizan con sus formas ni con sus ideas. Todos la respetan intelectualmente, pero no se sienten kirchneristas de Cristina.

La decisión de elegir a Alberto Fernández como presidente fue determinante para que esos mismos gobernadores respaldaran a la fórmula del Frente de Todos. Fueron pragmáticos, aún habiendo librado pequeñas batallas con el kirchnerismo en sus provincias para la conformación de las listas locales y en los comicios. En esta oportunidad se pararon en la vereda de enfrente al kirchnerismo y están esperando que el Presidente los respalde. No quieren que La Cámpora les cope las provincias.

La discusión por la suspensión de las PASO fue uno de los puntos de conflictos en la coalición durante el fin de semana. El otro lo tuvo Massa como protagonista. El líder del Frente Renovador y dueño de un porcentaje importante de acciones en la coalición de gobierno, marcó la cancha con una definición sobre el indulto. “Una amnistía para los casos de corrupción sería absurdo y un gran error”, sostuvo en una entrevista con El País.

La declaración fue un mensaje bien claro para el sector del kirchnerismo que presiona para que sean liberados el ex vicepresidente Amado Boudou y la líder de la agrupación Tupac Amaru, Milagro Sala. “De mi parte no hay ninguna chance ni de poner en discusión una amnistía así como creo que el Presidente no tiene ninguna vocación de discutir un indulto. La justicia es quien absuelve o condena”, afirmó. Sentó una postura importante dentro del Frente de Todos para contrarrestar el peso de la voluntad K. Un juego de palabras y discursos reflejado en los medios.

![La protesta que llevaron adelante organizaciones vinculadas al kirchnerismo pidiendo la liberación de Milagro Sala (Nicolas Stulberg)](https://www.infobae.com/new-resizer/tnchFQS2wY8KtUd-Uss-q5QYRY0=/420x280/filters:format(jpg):quality(85)//cloudfront-us-east-1.images.arcpublishing.com/infobae/SH5JUPEX4NAMVIRRWDDMKVIDXU.jpg)

Quince días atrás dirigentes del kirchnerismo, ex presidentes de distintos países de Latinoamérica, actores, periodistas y funcionarios del gobierno nacional firmaron una solicitada para reclamar la libertad de Boudou. “La persecución política contra militantes y dirigentes de la experiencia kirchnerista persiste en la Argentina a pesar del triunfo popular en las últimas elecciones”, indicaron en la carta que fue publicada y que llevó la firma, entre otros, de Mayra Mendoza, Jorge Ferraresi, Luana Volnovich, Hugo Yasky, Leopoldo Moreau, Andrés “Cuervo” Larroque, Eduardo Valdés, Norma Durango y Daniel Gollán.

El texto reclamaba “la libertad de Amado Boudou y de todos los presos y presas políticas” para que “en Argentina rija en plenitud la justicia y se respeten los derechos humanos”. La condena del ex vicepresidente por el caso Ciccone fue confirmada por la Corte Suprema el 3 de diciembre del año pasado. El 31 del mismo mes el juez de Ejecución Penal Daniel Obligado ordenó su regreso a la cárcel. La decisión desató el enojo en el sector K.

Antes de las fiestas agrupaciones kirchneristas pidieron una “navidad sin presos políticos” y reclamaron la liberación de ex funcionarios de los gobiernos de Néstor y Cristina Kirchner detenidos por causas de corrupción. Más cerca en el tiempo, durante la semana pasada, organizaciones sociales y políticas vinculadas al kirchnerismo hicieron una marcha a los Tribunales para pedir la liberación de Milagro Sala. Fue bajo la consigna “La Corte, basurero de la democracia”. Por eso decidieron arrojar cientos de bolsas de basura en la entrada de la sede judicial.

![Alberto Fernández junto a Máximo Kirchner y Sergio Massa ](https://www.infobae.com/new-resizer/2g1B6PiOJMLrKalGCnN4luwdvG0=/420x236/filters:format(jpg):quality(85)//cloudfront-us-east-1.images.arcpublishing.com/infobae/ZXZPRYD2E5B3TJCCMSCP6TV4VE.jpg)

En ese contexto se debe interpretar la definición de Sergio Massa, que, al igual que los gobernadores, siempre ha tenido grandes diferencias con el sector K. La decisión de explicar públicamente su posición sobre una posible amnistía no es más que un puñal para las ambiciones del kirchnerismo más duro. Cada uno fija su postura en el terreno político y trata de influir en la opinión o, de mínima, evitar que la imagen de todo el gobierno quede asociada a una sola idea.

Alberto Fernández ya se pronunció sobre la amnistía a lo que en el kirchnerismo consideran que son presos políticos. Fue a principio de enero. “Yo a la palabra le asigno un valor central y yo hice campaña diciendo que los jubilados no iban a pagar más los remedios y lo cumplí, prometí la ley de interrupción voluntaria del embarazo y lo cumplí, y dije que no iba a dictar indultos y lo voy a cumplir”, precisó. Fue contundente para intentar dar el tema por cerrado.

Las diferencias dentro del Frente de Todos volvieron a florecer en dos temas centrales de la agenda política. Se multiplicarán a lo largo de los próximos años. Lo importante no son las miradas distintas, sino cuáles prevalecen en el andar de la gestión. Esas señales son, en definitiva, las que define la identidad del gobierno y el proyecto político que intenta plasmar el Presidente.