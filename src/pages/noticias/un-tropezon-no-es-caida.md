---
category: Agenda Ciudadana
date: 2021-04-06T05:38:55-03:00
thumbnail: https://assets.3dnoticias.com.ar/union.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Marisa Lemos
resumen: UN TROPEZON NO ES CAíDA
title: UN TROPEZON NO ES CAÍDA
entradilla: 'La derrota de Unión ante Vélez lo dejó sin invicto, lo alejó de los puestos
  de clasificación y sin varios titulares de la defensa para recibir a Boca. '

---
Unión se quedó sin invicto tras caer frente a Vélez, por 4-1, en el José Amalfitani, por la octava fecha de la Copa de la Liga Profesional. En tanto que también se alejó de los puestos de clasificación para llegar a la fase final y encima perdió a dos titulares en la defensa para recibir a Boca. El equipo del Vasco estuvo "en partido" hasta los 39 minutos del segundo tiempo. Vélez aprovechó los espacios para liquidarlo en el final, cuando daba la impresión de que Unión estaba tan cerca de empatarlo como de sufrir lo que, en definitiva, ocurrió.

En la semana previa al duelo ante Vélez ya Juan Azconzábal había perdido a un valuarte en la defensa como Federico Vera, quien reportó positivo de coronavirus y seguramente no estará en condiciones para jugar ante Boca, el próximo domingo desde las 18.30, en el estadio 15 de Abril.