---
category: Deportes
date: 2021-04-26T06:00:42-03:00
thumbnail: https://assets.3dnoticias.com.ar/union.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: EMPATE CON SAZON A DERROTA
title: EMPATE CON SAZON A DERROTA
entradilla: En un partido aburrido y con pocas llegadas al arco, Unión empato ante
  Independiente en el 15 de abril por la fecha 11 de la zona B.

---
Unión dejó pasar una chance más como para meterse en zona de clasificación y ante un rival directo. Si el Tate le ganaba a Independiente lo pasaba en la tabla y se metía entre los cuatro primeros equipos. Pero no lo hizo, se dedicó a defender y especular con algún error del rival. En 90' pateó un solo tiro al arco.

El equipo involucionó futbolísticamente, perdió 3-0 ante Talleres jugando muy mal y esta vez si bien mantuvo el arco en cero, de mitad de cancha hacia adelante fue realmente improductivo. Debía ganar, pero no arriesgó, se conformó con no perder y el punto le sirve de muy poco, por decir nada.

El inicio del partido entre Unión e Independiente resultó parejo, con ambos equipos demostrando sus dificultades para prosperar en ataque. Tanto al Tate como al Rojo le costaba generar opciones de gol. En los metros finales carecían de sorpresa como para sorprender al rival.

En esos primeros 45', unión no pateó al arco. Fue realmente muy malo el primer tiempo, Independiente intentó un poco más y fue el que manejó el balón, pero le faltó creatividad. El equipo dirigido por Juan Manuel Azconzábal dispuso un planteo muy similar al partido ante Boca.

El arranque del segundo tiempo mantuvo la misma tónica, el partido continuaba siendo chato e Independiente era el que buscaba un poco más, pero sin ideas. Y Unión con su mismo plan de juego, apostando por alguna equivocación del equipo visitante.

A los 14' llegaría la más clara del partido, pero a partir de un error. Nelson Acevedo quiso jugar hacia atrás y no hizo otra cosa que dejar mano a mano a Silvio Romero con Moyano. El arquero de Unión achicó bien, obligó al delantero a ir hacia un costado y terminó neutralizando el remate, mandando el balón al córner.

Pero Unión iba a responder con la primera y única chance de gol hasta el momento, a los 23' Federico Vera metió un centro perfecto para Gastón Comas quien dentro del área no logró conectar bien. El balón le quedó a García quien debajo del arco terminó rematando por arriba.

Unión tenía la obligación de ganar, pero no hizo ni un mérito como para lograrlo. Se mostró cauteloso, especuló todo el tiempo y no arriesgó jamás. Se preocupó más porque no le conviertan, que por mirar el arco rival. Y eso se tradujo en un [empate](https://www.unosantafe.com.ar/empate-a37285.html) con sabor a derrota.

El "Tatengue" mantuvo el invicto en su casa al sumar dos éxitos y cuatro empates. Los de Avellaneda al menos pudieron cortar la racha adversa de tres derrotas consecutivas de visitantes.

Unión cerrará esta primera etapa de la competición ante Defensa y Justicia y el clásico ante Colón, ambos en condición de visitante.

**DERROTA LEJOS DE CASA**

Colon perdió 2 a 1 contra Racing en Avellaneda y acumula 4 partidos sin triunfos

El "Sabalero" se adelantó mediante un cabezazo de Luis Miguel "Pulga" Rodríguez, tras una serie de rebotes dentro del área, a los 40 minutos de la primera mitad. Pero justo antes del descanso, la "Academia" lo igualó gracias a Ignacio Piatti, tras una escalada de Juan Cáceres por la derecha.

En el final del partido, Juan José Cáceres cabeceó un córner en el primer palo que se metió cruzado, aunque con polémica porque Darío Cvitanich estaba en off side pese a que no tocó el balón, pero sí interfirió en la visión del arquero Leonardo Burián.

Con este resultado, el equipo dirigido por Eduardo Domínguez dejó pasar la oportunidad de quedar al borde de la clasificación a los cuartos de final. A su vez, el conjunto rojinegro acumula cuatro partidos sin triunfos, tras un comienzo arrasador en el torneo.