---
category: Estado Real
date: 2021-11-08T06:00:18-03:00
thumbnail: https://assets.3dnoticias.com.ar/horse.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: El gobernador Perotti  recorrió los talleres ferroviarios en San Cristobal
title: El gobernador Perotti  recorrió los talleres ferroviarios en San Cristobal
entradilla: Además participó de la 54° Fiesta Nacional del Caballo  y dijo que “las
  fiestas populares son parte del fortalecimiento de instituciones y la provincia
  las acompaña dentro de un esquema de Turismo”

---
El gobernador Omar Perotti, participó este domingo de la 54° edición de la Fiesta Nacional del Caballo que se realizó en la Sociedad Rural de San Cristóbal. Además, en el evento que se realiza desde el jueves 4 al lunes 8 de noviembre, se llevó a cabo la 80 Exposición Regional de Équidos, y el 38 Torneo Interprovincial de Rodeo por Equipo.

En la oportunidad, el gobernador sostuvo que “estar presente en esta edición es estar acompañando el esfuerzo de cada uno de ustedes por mantener viva a la muestra, y es una señal clara de mantener nuestras convicciones y nuestras costumbres”.

Además, agregó que “estamos encontrándole la salida a esta pandemia, que no nos quede ninguno sin vacunar, desde los 3 años en adelante, que es lo que está posibilitando volver a encontrarnos”.

El mandatario santafesino también destacó que “las fiestas populares son parte del fortalecimiento de instituciones, y de difusiones culturales de tradiciones, y la provincia quiere acompañarlas a todas dentro de una estrategia de Turismo”.

“Queremos que haya un acompañamiento a toda la región, a pesar de la pandemia en el sector rural se pudieron hacer actividades, no podíamos avanzar en las reuniones, pero sí en los programas y en la búsqueda del financiamiento de los Caminos de la Ruralidad, para que pase de 100 a 1.000 millones y lo podamos desplegar hoy en toda la provincia. El trabajo de la lechería también tiene que ver con la infraestructura, la logística y garantizar la salida de la producción y la llegada a las usinas lácteas”.

Con respecto al cierre de las exportaciones de carne, Perotti destacó que “todos éramos conscientes que el precio de la carne aumentaba y que podríamos generar un acuerdo sin lesionar las exportaciones. Nosotros estamos convencidos que eso se resuelve con mucha más producción y aliento al que produce”, añadió el gobernador.

Por su parte, el presidente de la Sociedad Rural de San Cristóbal, Francisco Mayoraz, agradeció el acompañamiento del gobernador Omar Perotti y la participación de las agrupaciones gauchas, criadores y expositores. “Después de la pandemia, poder ver lo que es este año la fiesta, nos llena de orgullo. Se pudieron exhibir más de 150 ejemplares”, dijo Mayoraz.

A su turno, el intendente de la ciudad, Horacio Pascual Rigo, felicitó “a las agrupaciones, centros tradicionalistas y criadores que apuestan al trabajo y pudieron reconocer el trabajo del personal de salud de San Cristóbal”.

Cabe destacar que la Fiesta concentra a los mejores ejemplares de los establecimientos de las provincias de Santa Fe, Córdoba, Santiago del Estero, de la mayoría de las razas equinas. La cantidad y calidad hacen de la fiesta que sea la preferida por los vecinos y visitantes de toda la región.

Además, estuvieron presentes en la muestra, el diputado provincial Marcelo González, la Secretaria de Coordinación de Políticas Públicas de la Provincia, Luisina Giovanini, el sub Secretario de Riesgo de Trabajo, Rubén Alemi, el presidente del Concejo local, Carlos Catáneo, miembros de ese cuerpo deliberativo, autoridades municipales, directivos de la Sociedad Rural, así como representantes de instituciones de la región.

**Recorrida por los talleres de Trenes Argentinos**

Posteriormente, junto al secretario general de la Unión Ferroviaria, Sergio Sasia, el gobernador Perotti recorrió el taller de San Cristóbal, donde se recuperan partes de los pares montados usados de trenes para hacer nuevos equipamientos.

“Este proyecto generará un desarrollo que esta vez es realidad. Hace más de un año estuvimos acá contando lo que se iba a hacer, y hoy se llegó a los mil pares trabajados, y eso es una clara demostración que se puede seguir creciendo y acompañando en forma directa al ferrocarril”, agregó el gobernador.

Por último, indicó que “es un antes y un después, hablar del ferrocarril en San Cristóbal, y eso es lo que entusiasma, motiva, y nosotros los acompañamos para que esta realidad se traslade a toda la provincia ante la Nación, para poder seguir teniendo este círculo virtuoso y avanzar en obras de rutas, y en esta recuperación ferroviaria”.