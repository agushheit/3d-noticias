---
category: Estado Real
date: 2021-07-27T06:00:07-03:00
thumbnail: https://assets.3dnoticias.com.ar/MANTOVANI.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La provincia finalizó el anexo de la Escuela de Artes Visuales “Juan Mantovani”
  de la ciudad de Santa Fe
title: La provincia finalizó el anexo de la Escuela de Artes Visuales “Juan Mantovani”
  de la ciudad de Santa Fe
entradilla: " Hay muchísimas gestiones implicadas en la concreción de este sueño que
  es la ampliación de la escuela”, destacó la directora, Vilma Horn."

---
El gobernador de la provincia de Santa Fe, Omar Perotti, inaugurará este martes el nuevo anexo de la escuela de Artes Visuales “Juan Mantovani” de la ciudad de Santa Fe, una obra que “es un anhelo, por el que hemos demorado muchísimo tiempo. Hay muchísimas gestiones implicadas en la concreción de este sueño que es la ampliación de la escuela”, destacó la directora del establecimiento, Vilma Horn.

El nuevo anexo es “una ampliación, que está pegada a la escuela, y dónde pensamos focalizar todo el proceso de enseñanza-aprendizaje de nuestra escuela secundaria”, precisó la directora.

La escuela Mantovani es una de los primeros establecimientos educativos de artes que se fundaron en el país, data de 1940, y su primer emplazamiento fue en calle Urquiza y Tucumán. En este sentido, Horn recordó que “debido a que ya tenía un deterioro importante, en la década del 70, 80, se la traslada a este edificio, que en ese momento estaba cumpliendo la función de archivo histórico provincial”.

Al respecto, la directora recordó que el edificio actual “no está adaptado para el uso de una escuela por eso era tan importante el anexo, para descomprimir la circulación, porque ya este edificio fue declarado patrimonio histórico nacional, debido a la importancia histórica y artística”.

La escuela secundaria cuenta con dos terminalidades, una audiovisual y una visual. El nuevo edificio anexo cuenta con un SUM en planta baja, para exposiciones, intervenciones artística e instalaciones, adaptado para el quehacer artístico; en el primer piso el área de producción audiovisual; y el segundo y tercer piso cuentan con 4 aulas, cada uno, para el dictado de clases. En el edificio histórica continuarán las aulas de cerámica, escultura, pintura.

**Detalles de la obra**

Con un presupuesto oficial de $179.135.450,55, las obras comprendieron la construcción del edificio Anexo de la Escuela Provincial de Artes Visuales Prof. Juan Mantovani, así como la re funcionalización de un pequeño sector del edificio existente ubicado en 9 de Julio 1845 de la ciudad de Santa Fe.

El Edificio Anexo está dedicado mayormente a aulas, pero también incluye una sala de exposiciones y una cafetería, en planta baja, y talleres en el primer piso. Todo esto con los núcleos sanitarios correspondientes.

Para esto se ejecutaron 1.000m2, aproximadamente, de obra nueva. Esta obra consiste en una estructura independiente de hormigón armado, con tabiques de cerramiento exterior, también de hormigón armado. La práctica totalidad de los elementos de hormigón armado quedaron con su materialidad natural a la vista.

Las terminaciones interiores están dadas por las aberturas de aluminio anodizado natural, los pisos de hormigón llaneado y los cielorrasos de placas de roca de yeso sobre perfilería de acero galvanizado. La tabiquería interior está proyectada en bloques de hormigón celular, con sus revoques y pintura. En los sectores sanitarios y de apoyo, los pisos son de mosaicos graníticos, así como los revestimientos en muros.

La cubierta se resuelve con una losa de hormigón y una azotea “seca”. La fachada es acristalada en su totalidad, mientras que la contrafachada alterna paños ciegos de hormigón armado a la vista, con parasoles de aluminio anodizado.