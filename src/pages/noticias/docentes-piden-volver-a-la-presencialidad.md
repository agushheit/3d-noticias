---
category: Agenda Ciudadana
date: 2021-02-05T06:58:43-03:00
thumbnail: https://assets.3dnoticias.com.ar/escuela.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Docentes piden volver a la presencialidad
title: Docentes piden volver a la presencialidad
entradilla: |-
  Desde el espacio "Abramos las Escuelas" manifiestan la necesidad de volver a las clases presenciales.
  Exigen certezas sobre el ciclo lectivo 2021 y exigen planes de contingencia en caso de una segunda ola del COVID-19.

---
"Abramos las Escuelas" surgió durante el 2020 y se trata de un grupo de docentes a favor de la vuelta a clases.

Desde hace varios meses vienen luchando contra la idea instalada de que son los docentes quienes oponen resistencia al regreso presencial de las clases.

Recientemente emitieron un comunicado a través de su sitio web donde manifiestan su preocupación por la falta de certezas sobre el ciclo lectivo 2021 y piden volver a las aulas.

_"Durante 2020 expresamos como docentes nuestra preocupación por la situación y pedimos la vuelta a las clases presenciales._

_En aquel momento advertimos sobre las graves consecuencias que generaba mantener las escuelas cerradas desde el DNU presidencial de marzo y durante todo 2020._

_A medida que fue avanzando el año esos problemas se agravaron, mientras la evidencia científica confirmaba una y otra vez que abrir las escuelas no era especialmente riesgoso para la propagación del virus. La visibilidad que le pudimos dar al tema entre padres, docentes y otros sectores fue construyendo una clara mayoría social que hoy reclama con firmeza y convicción que es imprescindible volver a las clases presenciales._

_Este reclamo tiene tal magnitud que, en las últimas semanas, muchos de los que deben decidir al respecto (y que en su momento habían decidido el cierre) cambiaron de opinión y ahora defienden la presencialidad._

_La cuarentena iniciada en marzo de 2020 tenía como objetivo, según se dijo, ¨preparar el sistema de salud para que no se sature¨. Nosotros pensábamos que, además, había que preparar el sistema educativo para reabrir las escuelas lo antes posible. Hoy vemos que, en la mayoría de los casos, eso no sucedió._

_Necesitamos tener un plan claro para que se retomen, lo antes posible, las clases con un 100% de presencialidad en todo el país._

_Si bien la vacunación no es condición para volver a las aulas, creemos que hacen falta certezas. No es bueno hacer anuncios y después quedar, como estamos viendo, muy lejos de cumplir con lo prometido._

_La mayor o menor presencialidad no debe ser una variable de la negociación de la paritaria nacional docente._

_Todos los ciudadanos, y esto incluye a los docentes, tenemos que enseñar en libertad y opinar sin restricciones, en algunos lugares del país esto no sucede y la situación es especialmente preocupante._

_Necesitamos protocolos que den un marco claro, y que esta vez sí sea considerada la situación de cada provincia, ciudad y escuela._

_Hace falta un proyecto de revinculación según el contexto de cada escuela para que los equipos docentes puedan buscar la reincorporación de los chicos que pueden abandonar el sistema educativo y rearmar los programas para recuperar el tiempo perdido._

_Planificar la manera de afrontar una posible nueva ola que obligue a cerrar por algunas semanas en algunos lugares puntuales, no se puede volver a improvisar al respecto._

_Nuestro país necesita que, lo antes posible, todos los chicos vuelvan a la escuela._

_Ese es nuestro compromiso como docentes y para eso vamos a trabajar, incansablemente, hasta lograrlo._

_Docentes de Ciudad de Buenos Aires y provincias de Buenos Aires, Santa Fe, Córdoba, Entre Ríos, Catamarca, Salta, Chaco, Neuquén, Santiago del Estero, Río Negro, Jujuy, Tucumán, Mendoza, Misiones, La Pampa"_

<br/>

[![Mira la nota completa](https://assets.3dnoticias.com.ar/entrevista-docentes-abramos-escuelas.webp)](https://youtu.be/sTTgY93wnXA "Reproducir")

Mirá la nota completa en nuestro canal de [ youtube](https://www.youtube.com/watch?v=sTTgY93wnXA)