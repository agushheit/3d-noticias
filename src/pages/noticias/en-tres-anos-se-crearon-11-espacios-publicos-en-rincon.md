---
category: La Ciudad
date: 2021-08-15T06:00:45-03:00
thumbnail: https://assets.3dnoticias.com.ar/RINCON.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: En tres años, se crearon 11 espacios públicos en Rincón
title: En tres años, se crearon 11 espacios públicos en Rincón
entradilla: 'Desde fines de 2017 el municipio intervino en las 29 plazas, plazoletas
  y paseos con que cuenta la ciudad. '

---
Con el objetivo de generar espacios verdes de calidad y fortalecer la apropiación de la ciudadanía, la Municipalidad de San José del Rincón desarrolla de forma planificada y participativa la creación y jerarquización de los espacios públicos. Desde que comenzó la actual gestión municipal encabezada por el intendente Silvio González se crearon 13 espacios verdes.

"Hoy la ciudad dispone de 29 plazas. De esas 29, 13 hemos creado e inaugurado durante nuestra gestión. Esto no es producto de los últimos meses, sino que venimos haciéndolo durante toda la gestión desde el primer año en que iniciamos. Esas ocho restantes las hemos puesto en valor", sintetizó el intendente. Además, a través del Fondo de Obras Menores, el municipio logró equipar seis plazas con módulos de juegos infantiles como mangrullo trepador, tobogán y hamaca triple, además de mobiliario urbano diseñado por el Taller Municipal de Carpintería.

"Para nosotros es fundamental porque las plazas son espacios de integración, de participación, porque todas han sido creadas en el marco de la participación de los vecinos donde han decidido qué tipo de espacio verde quieren, han decidido el nombre, y muchas de ellas, porque muchas veces no se puede hacer el proyecto completo, han diseñado un proyecto del espacio que determina diferentes etapas que deberían ir cumpliéndose en próximas gestiones", explicó González al dar cuenta de la dinámica de trabajo para intervenir cada espacio verde de San José del Rincón. Con esta metodología, este mes se inauguró la plaza Che Reta en Rincón Norte y este sábado será el turno de Las Lechucitas en Lomás de San José, en barrio Villa Añatí.

El mandatario rinconero aseguró que jerarquizar los espacios verdes "ha sido uno de nuestros ejes de gestión y creo que, en una ciudad verde, que pretende transformarse y proyectarse en materia turística por sus bellezas naturales, lo que no puede descuidar son sus espacios verdes, sus plazas y paseos".

**Espacios recuperados**

En los últimos tres años se sumaron 13 espacios verdes nuevos para el disfrute y apropiación de las vecinas y vecinos: se trata de las plazas Los Gladiolos, Julio Migno, Che Reta y Pueblos Originarios en Rincón Norte; El Eucaliptal en Acería; de Pancha Reduello en Cina Cina. Además, en Los Espinillos se habilitó el espacio Amanda Samaniego, y en Villa Añatí el Mercadito Costero, se recuperó el ex basural San Martín y se creron las plazas Arcoiris y Las Lechucitas. En Villa California se concretó la puesta en valor de las plazas 20 de Junio, 9 de Julio y 25 de Mayo. En La Lonja, se generó la Plazoleta de Los Pájaros.

Este mes se inauguró la plaza Che Reta en Rincón Norte.Foto: Gentileza Municipalidad de San José del Rincón

Además, con un cronograma planificado se realiza de forma continua el mantenimiento y puesta en valor de plazas y plazoletas, con la incorporación de mobiliario urbano, reparación de luminarias y colocación de cartelería, entre otras intervenciones. Tal es el caso de las plaza San Martín y las plazoletas Acería y Evita en Cina Cina; las plazoletas Piquino Mendieta y Malvinas Argentinas en Los Espinillos; la Plaza Brigadier López, el Camping Municipal Julio Migno y el Balneario Luis Beney, el Paseo de Los Homenajes en barrio Centro. En Villa Añatí se intervino en Plaza Arco Iris de San Martín y Los Ceibos, en el espacio verde situado en calle Hipólito Lartiga y en Canal San Martín, mientras que en La Lonja se trabajó en el predio que aloja en Centro de Habilitación de Conductores y el Centro de Monitoreo, entre otras dependencias municipales.

Camila Mosset Iturraspe, directora de Desarrollo Territorial municipal, expresó que "esta gestión se caracteriza por ir revalorizando los espacios públicos de la ciudad. Tenemos más de 50 espacios verdes y hemos venido trabajando con vecinales y organizaciones sociales para ir detectando esos espacios para ir poniéndolos en valor".

**Puesta en valor**

Entre otros trabajos abordados, sel gobierno local llevó adelante la puesta en valor del Polo Social y Productivo "Ingeniero Miguel Lifschitz" que aloja el Comedor Comunitario Municipal y El Refugio de la Imaginación. Estas medidas de recuperación patrimoniales también incluyen la puesta en valor del Museo Regional y Tradicional de la Costa, que funciona en San Martín y Santa Rosa, un lugar de visita obligada para quienes visitan la ciudad.

La ciudad cuenta con 16 espacios verdes que el municipio cuida y mantiene continuamente para favorecer su limpieza y preservación. La cifra incluye la plaza Julio Migno en Rincón Norte, barrio que además tiene cinco espacios más de propiedad municipal en los que se realizan tareas de desmalezado y limpieza continua como parte de un cronograma planificado.

En tanto, en los barrios Cina-Cina, Los Espinillos y Acería hay un total de cinco espacios, situados en zona de terraplén, reservorios y en torno a la sede de la vecinal La Cina que el municipio también interviene con tareas de mantenimiento y limpieza. Por su parte, en barrio Centro tiene a su cuidado dos espacios verdes ubicados en Busaniche casi Bonsembiante.

Finalmente, en barrio Villa California existen un total de cinco espacios de propiedad municipal: se trata de dos lotes frente a Plaza 25 de Mayo, del Paseo Las Estrellas, del espacio verde ubicado en calle del Sol y Espadañas y del Puentecito.