---
category: Estado Real
date: 2021-02-25T06:00:47-03:00
thumbnail: https://assets.3dnoticias.com.ar/reunionpartidos.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: El Gobierno de Santa Fe se reunió con el Partido Socialista en el marco de
  la ronda de diálogo con los partidos políticos de la oposición
title: El Gobierno de Santa Fe se reunió con el Partido Socialista en el marco de
  la ronda de diálogo con los partidos políticos de la oposición
entradilla: El Gobierno de Santa Fe continuó de esta manera con la agenda abierta
  que se inició el pasado 5 de febrero con la Unión Cívica Radical.

---
El ministro de Gobierno, Justicia, Derechos Humanos y Diversidad, Roberto Sukerman, encabezó este miércoles el segundo encuentro de la ronda de consulta a partidos políticos de la provincia, en el marco del diálogo con agenda abierta que promueve el Gobierno de Santa Fe. El encuentro se realizó en la ciudad de Santa Fe con autoridades del Partido Socialista.

Participaron del encuentro como representantes del PS, el secretario general y diputado nacional del Bloque, Enrique Estévez; el secretario adjunto, Rubén Galassi; los diputados provinciales Joaquín Blanco, Lionella Catallini y Pablo Farías; y la concejala por la ciudad de Santa Fe Laura Mondino.

El ministro Sukerman destacó la importancia del encuentro y señaló que “nos parece importante tener reuniones que vayan más allá de la agenda legislativa que es la que generalmente se tiene con los bloques parlamentarios. Poder estar con las autoridades partidarias implica hablar otros temas que quizás no están en el debate parlamentario todavía”.

En este sentido, detalló que “se habló de las elecciones de este año, sobre la opinión que tienen sobre las PASO y sobre hacer coincidir, como tradicionalmente se hace, la elección nacional con las elecciones locales”; y también surgieron temas “que nos interesan y sobre los que tenemos puntos en común”, como la autonomía municipal y una futura reforma constitucional. En este sentido, destacó que “el propio Miguel Lifschitz se expresó a favor de esta reforma, que todavía está pendiente”.

Asimismo, señaló que “también se conversó sobre la situación de la seguridad, la pandemia, el operativo de vacunación y la vuelta a clases”.

Con relación al planteo del PS sobre la convocatoria a la Junta de Seguridad, el ministro Sukerman señaló que hay que destacar que, independientemente de la Junta, “permanentemente el equipo de Seguridad se viene reuniendo con los distintos sectores, lo importante es que se haga el trabajo, para mejorar, más allá de que sea en el marco de una Mesa de Trabajo, de Comisiones, o de una Junta, lo fundamental es que el tema se resuelva”, concluyó.

Por su parte, el secretario general del PS, Enrique Estévez, expresó que “siempre el diálogo es positivo, vinimos a esta convocatoria institucional e hicimos público con anterioridad los temas que nos preocupan, fundamentalmente la violencia y la inseguridad en la provincia y la necesidad de la convocatoria a la Junta Provincial de la Seguridad, que es un ámbito que funcionó en gobiernos anteriores con la participación de poderes del Estado y que tiene estabilidad para analizar un tema tan preocupante”.

El titular del PS agregó que “también conversamos del plan de vacunación, que es un tema que también le preocupa y está la ciudadanía muy pendiente de eso”.