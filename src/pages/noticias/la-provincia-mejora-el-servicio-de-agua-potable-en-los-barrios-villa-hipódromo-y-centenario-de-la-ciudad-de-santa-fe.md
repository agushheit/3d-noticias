---
layout: Noticia con imagen
author: --
resumen: "Servicio de agua potable "
category: La Ciudad
title: La provincia mejora el servicio de agua potable en los barrios Villa
  Hipódromo y Centenario de la ciudad de Santa Fe
entradilla: Las tareas benefician a unos 700 vecinos y demandan una inversión
  superior a los 5 millones de pesos.
date: 2020-11-17T16:38:38.456Z
thumbnail: https://assets.3dnoticias.com.ar/balagu%C3%A9.jpg
---
El gobierno de la provincia, a través de Aguas Santafesinas (Assa), está ejecutando una nueva red de agua potable en el barrio Villa Hipódromo y ya finalizó las tareas en el barrio Centenario, tareas que mejoran el servicio a unos 700 vecinos de la ciudad de Santa Fe.

El presidente de Assa, Hugo Morzán, comentó que “en ambos barrios se vienen ejecutando los tendidos de más de 1.000 metros de nuevas cañerías distribuidoras y se prevén más de 150 nuevas conexiones domiciliarias, con una inversión que supera los 5 millones de pesos”.

“En esta ocasión estamos trabajando en coordinación con la Municipalidad de Santa Fe que realiza tareas de limpieza y mejoras urbanísticas con una retroexcavadora, mientras que desde Assa se desarrolló el proyecto, se aporta la mano de obra, su conducción y los materiales para las nuevas redes y conexiones domiciliarias en ejecución”, agregó.

## **VILLA HIPÓDROMO**

En Villa Hipódromo se están tendiendo más de 770 metros de nuevas cañerías distribuidoras y se prevé la ejecución de más de 100 nuevas conexiones domiciliarias. Para su habilitación será necesario vincular las nuevas conexiones a las redes existentes por medio de 7 empalmes.

A su vez, se realizará también la colocación de 7 válvulas esclusas de sectorización de la red y 3 hidrantes para limpieza de la red.

## **CENTENARIO**

En el barrio del sur de la ciudad ya se tendieron más de 310 metros de nuevas redes de agua potable sobre calle Raúl Tacca y se completó un tramo de la red faltante sobre calle Colón.