---
category: Agenda Ciudadana
date: 2022-12-20T10:08:41-03:00
thumbnail: https://assets.3dnoticias.com.ar/ahora12.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: Extienden el programa Ahora 12
title: Extienden el programa Ahora 12
entradilla: La Secretaría de Comercio, prorrogó hasta el 30 de junio de 2023 la vigencia
  del programa Ahora 12.

---
La Secretaría de Comercio, a cargo de Matías Tombolini, prorrogó hasta el 30 de junio de 2023 la vigencia del programa Ahora 12, la iniciativa de fomento al consumo que brinda la posibilidad de financiar compras en 3, 6, 12, 18 y hasta 24 cuotas fijas con tarjeta de crédito bancarias, todos los días de la semana, en todo el país y con tasas inferiores a las del mercado. Además, en esta renovación se volvió a incluir teléfonos celulares al listado de rubros que pueden financiarse con este programa.

Mediante la resolución 144/2022 publicada en el Boletín Oficial, se extendió el plan Ahora 12 desde el 31 de enero de 2023 hasta el 30 de junio del mismo año con el objetivo de promover el crecimiento económico y el desarrollo, incentivando la inversión productiva y la demanda de bienes y servicios a través del otorgamiento de facilidades de pago en cuotas fijas.

Ahora 12 abarca 25 categorías de productos, entre los que se destacan los de línea blanca, electrodomésticos, indumentaria, calzado y marroquinería, materiales para la construcción, muebles, bicicletas y motos. Algunos de estos productos también pueden financiarse en hasta 30 cuotas fijas en locales de electrodomésticos y grandes supermercados adheridos de todo el país.

Incorporación de celulares

Además, en esta renovación del programa volvieron a nuevamente se incorporó el rubro celular. En este caso, se trata de 163 modelos y formatos de equipos móviles con tecnología 4G que podrán adquirirse en 3, 6 y 12 cuotas.

Más información en [https://www.argentina.gob.ar/ahora-12](https://www.argentina.gob.ar/ahora-12 "https://www.argentina.gob.ar/ahora-12")