---
category: Estado Real
date: 2021-04-16T08:31:07-03:00
thumbnail: https://assets.3dnoticias.com.ar/monte-vera.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: El Ministerio de Desarrollo Social y Anses realizaron un operativo conjunto
  en Monte Vera
title: El Ministerio de Desarrollo Social y Anses realizaron un operativo conjunto
  en Monte Vera
entradilla: Los organismos provincial y nacional se hicieron presentes con sus oficinas,
  con el objetivo de posibilitar la realización de distintos trámites en un sólo lugar.

---
El Ministerio de Desarrollo Social, a través de la Dirección Provincial de Desarrollo Territorial, realizó un operativo conjunto con la Administración Nacional de la Seguridad Social (Anses) en la localidad de Monte Vera, con el objetivo de acercar a los vecinos la posibilidad de realizar distintos trámites en un sólo lugar.

En la oportunidad, los vecinos realizaron trámites o consultas sobre Billetera Santa Fe, Boleto Educativo Gratuito, Asignación Universal por Hijo y Embarazo, escolaridad, Monotributo Social, Programa Hogar, Progresar y la oferta programática del Ministerio de Desarrollo Social.

El operativo, que tuvo lugar en el Club Gimnasia y Esgrima de Monte Vera, también se realizó en las localidades de Nelson y Arroyo Aguiar; y continuará en las próximas semanas en otras localidades del departamento La Capital.

Al respecto, el ministro de Desarrollo Social, Danilo Capitani, afirmó que “estamos acercando las oficinas del Estado al barrio para atender las necesidades concretas de las familias. Que los vecinos y vecinas puedan acceder de manera ágil y gratuita a múltiples servicios que ofrece provincia y nación concentrados en un solo lugar, sin tener que trasladarse, también es contribuir a mejorar la calidad de la vida de los santafesinos”.

Por su parte, la directora provincial de Desarrollo Territorial, Romina Sonzogni, expresó que "en la localidad de Monte Vera llevamos adelante un nuevo operativo integral de los estados nacional y provincial para acercar las políticas públicas a las personas, de manera directa, ofreciendo asesoramiento, información y tramitación. Esta es la tarea que nos convoca, potenciar los territorios, recorriendo cada municipio y cada comuna", finalizó la funcionaria.

**PRESENTES**

También participaron, el senador nacional Roberto Mirabella, el senador departamental, Marcos Castello; el jefe regional de ANSES, Diego Mansilla; el gerente de UDAI Santa Fe, Luis López; y representantes del Banco de Santa Fe.