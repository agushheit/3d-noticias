---
category: Agenda Ciudadana
date: 2021-12-10T06:15:17-03:00
thumbnail: https://assets.3dnoticias.com.ar/LARRATA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: La comunidad educativa rechaza la iniciativa de Larreta de pasantías obligatorias
title: La comunidad educativa rechaza la iniciativa de Larreta de pasantías obligatorias
entradilla: "Estudiantes y gremios rechazan la imposición inconsulta de prácticas
  laborales no rentadas y obligatorias para alumnos que cursen el último año de la
  secundaria, a partir del ciclo lectivo 2022. \n\n"

---
Luego de que el jefe de Gobierno de la ciudad de Buenos Aires, Horacio Rodríguez Larreta, anunciara que a partir del año próximo y de forma obligatoria, los alumnos del último año del nivel secundario de las escuelas públicas y privadas de la ciudad de Buenos Aires deberán realizar prácticas educativas en ámbitos laborales como condición para aprobar su cursada, tanto representantes de centros de estudiantes como gremios docentes cuestionaron la medida a la que calificaron de “nefasta”.   
  
Así, desde el ámbito estudiantil, Amparo López, vocera del centro de estudiantes del Instituto de Enseñanza Superior en Lenguas Vivas "Juan Ramón Fernández", quien acaba de pasar a quinto año, aseguró que esta iniciativa es un "guiño a los empresarios" y que no harán más que "generar mayor precarización".  
  
La estudiante Amparo López sostuvo que estas prácticas "nefastas convierten a los pibes en mano de obra gratuita". Foto Archivo Alejandro Santa Cruz

   
Por su parte, Luz Schiffmacher, vocal del centro de estudiantes del Colegio Nacional de Buenos Aires (CNBA), aseguró que -contrario a lo que se argumentó desde el Ejecutivo porteño- esta "no es una respuesta válida" ante la demanda de empleo para las juventudes.  
  
"El problema del desempleo joven no está en la falta de formación, sino en que no se está creando empleo porque siempre buscan abaratar la mano de obra", agregó.  
  
Ambas representantes de estudiantes consultadas por Télam coincidieron en que las prácticas laborales obligatorias son "unas pasantías nefastas que convierten a los pibes en mano de obra gratuita" y que, como todo "ataque a la educación pública", se encontrará con "una gran lucha y organización estudiantil resistiendo".  
  
Según anunció Rodríguez Larreta, un total de 29.400 alumnos y alumnas deberán realizar prácticas laborales no rentadas en el sector productivo (privado o público); el sector de gestión de políticas públicas; el ámbito cultural y comunitario o instituciones de la educación superior y el área científico-académico y tendrán una duración de 120 horas cátedras que se distribuirán a lo largo del año.  
  
"Todos los estudiantes de la escuela secundaria de la Ciudad van a tener prácticas de trabajo de carácter obligatorio, ya que los chicos terminan la escuela con mucha incertidumbre, tienen dudas de qué quieren hacer y estudiar y con las prácticas queremos que tengan un panorama más claro, hacia dónde quieren caminar en su futuro", argumentó el Jefe de Gobierno porteño, al realizar el anuncio durante una conferencia de prensa.  
  
Desde el ámbito gremial, se refirieron a la imposición como "trabajo precarizado" y tanto UTE-Ctera como Ademys sostuvieron que la decisión del gobierno de CABA "no contribuye al desarrollo de los jóvenes", advirtieron que "la escuela secundaria no tiene este propósito" y acusaron al jefe de Gobierno de querer impulsar el trabajo precarizado de estudiantes.

 "Desde el Ministerio (de Educación) impulsan el trabajo precarizado de estudiantes y la escuela secundaria no tiene este propósito. Tendrían que estar trabajando en la orientación e inclusión de los estudiantes en los circuitos de educación superior", dijo a Télam Angélica Graciano, secretaria General de la Unión de Trabajadores de la Educación (UTE-Ctera).  
  
Ambos gremios criticaron lo que entienden como “improvisación” sin tener prevista la implementación. Además, indicaron que no tuvieron "ninguna reunión ni trabajo en este sentido" previamente. “Está hecho para producir de titulares", aseguraron.  
  
Marisabel Grau, secretaria de prensa de la Asociación Docente Ademys, dijo a Télam que desde el gremio rechazan el "intento del gobierno de la Ciudad de llevar adelante prácticas laborales y pasantías en las escuelas secundarias desde el 2017 con la implementación de la Secundaria del Futuro".  
  
Según había remarcado el mandatario porteño, las prácticas educativas en los ámbitos laborales "van a ser claves para el futuro porque combinan el saber de la escuela con la práctica, donde hay que conectar a los chicos con las herramientas que despierten su vocación".  
  
El Gobierno porteño había afirmado que, a diferencia de las pasantías, las prácticas laborales "forman parte del diseño curricular de la Secundaria del Futuro" y comenzarán el año próximo en coincidencia con la primera camada de alumnos de 5° año, desde su creación en 2018.

> "Hay improvisación. Están haciendo un anuncio y no tienen previsto nada de la implementación. Está hecho para producir de titulares"Angélica Graciano

  
  
"De ninguna manera entendemos que la prioridad tiene que ser la inserción en el mercado laboral. Toda la reforma de la Secundaria del Futuro apunta a que los chicos se adapten a un mercado laboral precarizado, en el que no se requieren conocimientos específicos", agregó Grau.  
  
En este sentido, sostuvo que la escuela secundaria debe apuntar a potenciar "el pensamiento crítico y el conocimiento que contribuya al desarrollo de personas que puedan transformar la sociedad y aspirar efectivamente a un acceso a la Universidad y a los institutos terciarios".  
  
"Esto de ninguna manera contribuye a ello, sino todo lo contrario. Lo que hace es transformar a los jóvenes en mano de obra gratuita para distintas instancias laborales", destacó.  
  
El programa alcanzaría a los 11.250 alumnos de 5° año de las 116 escuelas de gestión pública y los 18.150 alumnos del último año de los 326 establecimientos de nivel secundario de gestión privada, que funcionan en territorio capitalino. Pero no formarían parte de esta iniciativa los 3.000 estudiantes de las 52 Escuelas Técnicas del distrito (tanto de gestión estatal como privada puesto que ya implementan prácticas profesionalizantes en 5° y 6° año.  
  
En este sentido, desde el gobierno porteño se habló de cuatro prioridades: "Primero en lo que respecta a tecnología; segundo más horas de clase; tercero la capacitación docente, porque para formar a los estudiantes del futuro necesitamos docentes del futuro; y cuarto la transformación de la escuela secundaria, para que tengan prácticas educativas en el mundo del trabajo".  
  
Por su parte, la ministra de Educación de la Ciudad, Soledad Acuña, remarcó que las prácticas formativas serán de 120 horas cátedras que se suman a 30 horas de un espacio de capacitación y formación sobre educación financiera.  
  
Al respecto, Graciano respondió: "Hoy se vota el presupuesto educativo y no se prevén incrementos en el nivel secundario. Este tipo de prácticas necesitan de aumentos presupuestarios para el proceso de sistematización -considerando que lo más probable es que esto deba realizarse a contraturno-, nuevos puestos de trabajo, el traslado de los estudiantes, además de que creo que los estudiantes deberían recibir una asignación por la compra del material necesario, como ropa".  
  
"Hay improvisación. Están haciendo un anuncio y no tienen previsto nada de la implementación. Está hecho para producir de titulares", concluyó Graciano.