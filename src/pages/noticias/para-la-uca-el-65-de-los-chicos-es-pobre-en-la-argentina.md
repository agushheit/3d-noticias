---
category: Agenda Ciudadana
date: 2021-12-08T06:00:27-03:00
thumbnail: https://assets.3dnoticias.com.ar/NIÑOSPOBREZA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: Para la UCA, el 65% de los chicos es pobre en la Argentina
title: Para la UCA, el 65% de los chicos es pobre en la Argentina
entradilla: 'Así surge del relevamiento presentado por el Observatorio de la Deuda
  Social elaborado por esa universidad.

'

---
El 43,8% de los argentinos se encontraba bajo la línea de la pobreza hacia octubre último, mientras que el 65% de los chicos permanecía en esa condición.

Así surge del relevamiento presentado hoy por el Observatorio de la Deuda Social elaborado por la UCA.

El dato representa una leve baja respecto de 2020, cuando la pobreza arrojaba 44,7%, pero sigue por encima del 39,8% de 2019.

Entre julio y octubre de 2021, el 43,8% de las personas eran pobres, mientras el 8,8% eran indigentes.

Sin los planes, la pobreza subiría del 43,8% a 48,9%, de acuerdo con el estudio.

Los niveles de indigencia se encuentran en valores relativamente similares a los de la prepandemia: la pobreza por ingresos es aún superior a la que se registraba en 2019, cuando llegaba al 39,8%.

Salvia señaló que los planes sociales son "fundamentales para evitar que se dispare la indigencia", y sostuvo que sin estas ayudas, ese índice subiría del 8,8% al 18%.

Los expertos de la UCA alertaron que la franja etaria de los más jóvenes es la única en la que la pobreza volvió a subir, al presentar los nuevos datos del barómetro de la deuda social.

Así, la medición tiene registros mejores a los de 2020, aunque advierten que persiste el deterioro socioeconómico, en buena medida por la inflación.

Según el relevamiento, entre el tercer trimestre de 2021 y el mismo período del 2020, la pobreza se redujo del 44,7% al 43,8%, mientras el desempleo retrocedió del 14,2% al 9,1%.

Por el efecto del confinamiento severo que generó la pandemia, entre 2019 y 2020 la tasa de desocupación se incrementó de 10,6% a 14,2%.

Tanto en 2020 como este año, impactó fuerte el "efecto desaliento", de desocupados que abandonaron la búsqueda de empleo al ver que disminuían las chances de conseguir trabajo.

La UCA estima que sin ese fenómeno la desocupación se habría incrementado a niveles cercanos al 28,5% en 2020, y al 12,5% en 2021.

En 2021 sólo el 42,1% de la población económicamente activa tenía un empleo pleno de derechos, mientras el 29,6% tenía uno precario, el 19,2% un subempleo inestable y el 9,1% estaba desocupado.

Esta situación, unida al efecto de la inflación, hizo que entre 2019 y 2021 el poder adquisitivo del total de los ocupados disminuyera 7,4%.

En 2021, la media de los ingresos mensuales del total de los ocupados fue de $50.534.

Para trabajadores con empleo pleno el ingreso medio fue de $68.973 y para los que tienen un subempleo inestable fue de $18.637.

Agustín Salvia, responsable del trabajo, dijo que el empleo se recupera más en el sector informal, pero allí a su vez golpea fuerte la inflación.

Dijo que observaron un "empobrecimiento de las clases medias con capacidades de ahorro".  
  
**El impacto sobre la niñez.**  
  
El relevamiento estimó que en 2021 el 64,9% de los niños/as y adolescentes vivían en hogares con ingresos por debajo de la línea de pobreza, y el 14,7% en viviendas con ingresos por debajo de la  
frontera de la indigencia.

En 2020, la pobreza había llegado al 44,7%, y no escaló más allá por el impacto de medidas como el Ingreso Familiar de Emergencia (IFE), con tres bonos de $10.000 para 9 millones de personas.

Salvia alertó que a la Argentina le tomará varios años volver a los niveles de pobreza del 2017, cuando el indicador estaba en 28%.