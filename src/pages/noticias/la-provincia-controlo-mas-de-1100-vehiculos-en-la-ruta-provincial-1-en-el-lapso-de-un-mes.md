---
category: Agenda Ciudadana
date: 2020-11-29T13:02:01Z
thumbnail: https://assets.3dnoticias.com.ar/CONTROL-MOTOS.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Operativos Ruta Provincial 1
title: La provincia controló más de 1100 vehículos en la Ruta Provincial 1 en el lapso
  de un mes
entradilla: La APSV lleva a cabo operativos especiales para monitorear el cumplimiento
  del respeto a la señal de semáforo rojo entre los kilómetros 0 y 10.

---
La Agencia Provincial de Seguridad Vial (APSV) informó que se llevaron a cabo 74 operativos de control en la Ruta Provincial 1, entre las localidades de Santa Fe y San José del Rincón, en virtud de problemáticas puntuales en la zona y que requerían mayor presencia del Estado para prevenir siniestros viales. Se fiscalizaron más de 1100 vehículos y se labraron actas por no respetar el semáforo, entre otras transgresiones.

El Subsecretario de APSV, Osvaldo Aymo, valoró la articulación entre los municipios para que la presencia del Estado en las rutas sea más eficaz y sostuvo que “se implementaron operativos conjuntos y se implementó una modalidad innovadora de control que son los patrullajes conjuntos de las brigadas motorizadas de provincia y municipios.

La colaboración excede el control ya que también es vital la participación de los juzgados de faltas locales”. En este marco, se llevaron a cabo 74 operativos desde el 24 de octubre al 24 de noviembre, se fiscalizaron 1122 vehículos y se labraron 640 actas de infracción, de las cuales 66 corresponden a creces de semáforos en rojo.

Esto originó que a 35 personas se les retuviera la licencia en forma preventiva por considerar que sus conductas representan un gran riesgo para sí mismo y para otros.

Por otro lado, se detectaron 6 conductores de moto con alcoholemia positiva con valores 0, 31; 1,43; 0,90; 0,89; 0,90 y 1, 81 g/l.

También se labraron actas por ausencia de licencia de conducir; falta de casco; maniobras imprudentes como giros o sobrepasos indebidos; conducir utilizando celular, entre otras. El funcionario provincial agregó que “a raíz de estos controles se evidencia una reducción de la cantidad de infracciones por no respetar las indicaciones de los semáforos. Es importante mencionar que además se logró la sincronización de los semáforos lo cual incidió en un mayor acatamiento”.

Las acciones entre las municipalidades de Santa Fe y San José del Rincón en el corredor vial de la ruta 1 tienen como propósito también resguardar la seguridad vial de la gran cantidad de santafesinos que esta temporada estival optarán por la zona de Colastiné, Villa California y Rincón para veranear. Para ello se están delineando acciones con vistas a implementar en la zona el Operativo verano provincial.