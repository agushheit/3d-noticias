---
category: Estado Real
date: 2021-10-10T06:00:09-03:00
thumbnail: https://assets.3dnoticias.com.ar/ASSA.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: El gobernador Perotti inauguró la nueva estación de bombeo de la empresa
  ASSA en Reconquista
title: El gobernador Perotti inauguró la nueva estación de bombeo de la empresa ASSA
  en Reconquista
entradilla: 'En el acto, el gobernador dijo que “la principal obra de salud es el
  agua potable, es tener los servicios sanitarios de cloacas en toda la población”. '

---
La obra tiene como objetivo hacer uso de la mayor producción de agua potable del Acueducto Reconquista, y la incorporación escalonada al servicio de agua potable de 30.000 vecinos en diversos barrios de la ciudad.

En la oportunidad, el gobernador Perotti sostuvo que “es una alegría estar nuevamente en Reconquista, poder estar en el cumpleaños de la declaración de ciudad, y un cumpleaños de nada menos que 100 años merece obras importantes. Quiero destacar el esfuerzo enorme de Hugo Morzán y todo el equipo al frente de la empresa, para poder poner en marcha esta y otras obras en la provincia de Santa Fe, pero no son otras obras, sino las mayores obras en la historia de la empresa en la provincia de Santa Fe”, dijo.

Más adelante, el mandatario indicó que “no hay dudas que se está pudiendo realizar esta tarea con un trabajo coordinado con la Nación en la asignación de muchos de esos recursos, y por eso me acompaña Roberto Mirabella, nuestro senador que junto a Marilín Sacnun, nos ayudan en todas las gestiones que hacemos. Se firma un convenio con los ministros, se tiene que instrumentar la parte técnica, donde intervienen los equipos de la empresa, y ese día a día con los lugares lo hacen nuestros senadores nacionales que son parte del equipo. Por eso Roberto lo hizo hasta aquí y Marcelo Lewandowski, que es parte también del equipo en la provincia, proyectándose para no detenernos y seguir avanzando en esta relación con la Nación”, expresó Perotti.

Sobre el final, el gobernador expresó que “son obras de gran magnitud, inversiones que no se ven, porque son caños que se entierran; muchos le escapan porque les gusta más mostrar algo donde alguien pasa adelante todos los días, pero estas obras son de muchos millones, donde uno puede abrir una canilla y tener agua o tener cloacas en su calle. Entonces nosotros tenemos una decisión tomada, la principal obra de salud es el agua potable, es tener los servicios sanitarios de cloacas en toda la población. Este antes y después es un salto enorme de calidad de vida para todos los habitantes de Reconquista, y bien merecidos para la ciudad en 100 años. Pero allí no termina esto, porque estas dos inversiones, esta terminada en 250 millones, y la planta depuradora con un presupuesto de 1.400 millones, donde se están analizando las ofertas, son obras de inversiones millonarias que nos van a permitir empezar a programar la extensión en los barrios”, concluyó Perotti.

**Un antes y un después en la historia de Reconquista**

Mientras tanto, el Intendente de Reconquista, Amadeo Enrique Vallejos, indicó que “hoy vamos a cantar los 100 años de Reconquista declarada ciudad, y cuando vine a esta ciudad a trabajar como médico, lo hacía en un centro de salud al oeste de la ciudad, y uno de los principales problemas lo tenían los niños, como consecuencia de no tener agua potable. Allí hicimos un trabajo muy importante donde analizamos el agua y ese estudio nos ayudó para hacer un cálculo de cuánto gastaba el Estado en atención de la salud de los niños, en los centros de salud, en los hospitales; y descubrimos que se gastaba más de lo que salía darle agua potable a esa zona; por lo cual el agradecimiento inmenso de todo lo que se pudo hacer en conjunto y que hoy podamos estar viviendo esta realidad, algo que no podíamos concebir era que, a pesar de que teníamos el acueducto, muchos barrios no podían acceder al agua potable si no se terminaba esta estación de bombeo, tan esperada para la ciudad”.

También Vallejos, destacó “que otro caso similar era el del servicio del gas natural, con el gasoducto del NEA, que pasaba por la ciudad, y estas dos obras, por decisión de este gobierno provincial, en conjunto con el gobierno nacional, que en estos 100 años podamos tener la infraestructura para que el 100 por ciento tenga agua potable, y que se haya licitado la planta de tratamiento de efluentes cloacales, va a ser un antes y un después en la historia de Reconquista”.

**Un día muy especial para la empresa y la ciudad**

A su turno, el presidente de la empresa Aguas Santafesinas, Hugo Morzán, brindó precisiones sobre la obra que se inauguró y señaló que “este día es muy especial porque cuando tuvimos la visita del gobernador, planteamos cuáles eran las obras que necesitábamos para poder prestar el servicio que Reconquista se merece, y a partir de allí el gobernador tomó la determinación que se lleve adelante esta obra que permite poder llevar agua al 100 por ciento de la ciudad. Eso significa para nosotros un esfuerzo muy grande porque va a requerir poner en funcionamiento el sistema de forma óptima y comenzar la incorporación de los diferentes barrios en forma paulatina”.

“Esto parece un sueño que se concretó en un corto plazo, son tres bombas de 900 m3 por hora que van a permitir incorporar los barrios al servicio, y cuenta con grupo electrógeno que prevé la generación propia para brindar el servicio”, finalizó Morzán.

También estuvieron presentes en el acto, el secretario de Empresas y Servicios Públicos, Carlos Maina, el subsecretario de Gestión de Riesgo y Protección Civil de la Nación, Gabriel Gasparutti, el secretario de Hábitat, Urbanismo y Vivienda, Amado Zorzón, la sub secretaria de Fortalecimiento Territorial, Igualdad, Género y Diversidad, Soledad Zalazar, el ministro de Gestión Pública, Marcos Corach, el senador nacional Roberto Mirabella, el senador provincial Marcelo Lewandowski, otras autoridades, y trabajadores de la nueva estación de bombeo.