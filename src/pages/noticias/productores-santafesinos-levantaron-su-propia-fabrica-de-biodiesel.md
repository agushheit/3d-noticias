---
category: Agenda Ciudadana
date: 2022-06-30T07:54:54-03:00
thumbnail: https://assets.3dnoticias.com.ar/biodiesel.gif
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Revista CREA
resumen: PRODUCTORES SANTAFESINOS LEVANTARON SU PROPIA FÁBRICA DE BIODIESEL
title: PRODUCTORES SANTAFESINOS LEVANTARON SU PROPIA FÁBRICA DE BIODIESEL
entradilla: Cuatro empresarios del centro de Santa Fe montaron una planta que permite
  elaborar hasta 250.000 litros de biocombustible por año. Todo lo producido es para
  consumo propio.

---
El desabastecimiento de gasoil viene ocasionando grandes dificultades por estos días. De hecho este miércoles comenzó una serie de medidas de protestas en rutas y autopistas por parte de transportistas que reclaman por la falta de gasoil.

En medio de este contexto adverso, está la historia de cuatro empresarios del centro de Santa Fe que montaron una planta que permite elaborar hasta 250.000 litros de biocombustible por año y les permite abastecerse del combustible que necesitan.

“Con el proyecto buscábamos generar valor agregado en origen y asegurar el abastecimiento de combustible para el parque de maquinarias propio”, contó Diego Lescano, integrante del CREA Elisa- Humberto Primo (región Santa Fe Centro) y uno de los cuatro socios de la planta de biodiésel.

Según contó el empresario santafesino en una nota publicada por la revista Crea, “ya ocurrió desabastecimiento de gasoil en otras épocas y sabíamos que era una cuestión de tiempo que un fenómeno similar volviese a repetirse; entonces decidimos hacer algo al respecto”. Hoy puede asegurar que la inversión realizada fue una decisión acertada.

Luego de visitar a varios proveedores de plantas elaboradoras de biodiésel y hacer los números de la nueva unidad de negocio con el asesor CREA, finalmente decidieron invertir en conjunto unos 60.000 dólares en total (valuados al tipo de cambio oficial) en una unidad montada por una empresa cordobesa localizada en la zona de Villa María.

El diseño se planificó para elaborar hasta unos 250.000 litros de biodiésel por año, que es la cifra consumida por los tractores y cosechadoras de las cuatro empresas socias. El principal insumo empleado es aceite de soja desgomado, el cual es comprado a la fábrica aceitera que Agricultores Federados Argentinos (AFA) tiene en la zona de Los Cardos, a 180 kilómetros de donde se encuentra la planta de biodiésel.

La planta estuvo lista en marzo de 2020 –pleno inicio de la pandemia de Covid-19– y los primeros meses no fueron fáciles por las dificultades logísticas presentes por entonces. Recién hacia mediados de ese año la planta comenzó a funcionar en función de lo previsto.

“Cuando empezamos, el valor del biodiésel producido era un 30% inferior al precio del gasoil en surtidor, lo que representaba una negocio muy favorable para los socios del emprendimiento”, relató Diego. Sin embargo, en 2021 el precio internacional del aceite de soja comenzó a experimentar subas considerables y las relaciones de precio se tornaron desfavorables para el biodiésel de propia elaboración.

“Entendimos que toda crisis representa una oportunidad si uno se mantiene activo, por lo cual comenzamos a emplear parte del aceite de soja para elaborar aceite metilado para uso en pulverizaciones de fitosanitarios, el cual nos permitió compensar las pérdidas económicas generadas por la producción de biodiésel”, dijo.

Sin embargo, nunca dejaron de fabricar y usar biodiésel de propia producción porque, desde el día uno, los socios se propusieron mantener siempre activa la unidad más allá de lo que sucediese con las relaciones de precios.

El tiempo mostró que esa estrategia resultó acertada, porque, cuando en marzo pasado comenzaron a registrarse problemas para abastecerse de gasoil, todos los vehículos de los socios del emprendimiento permanecieron ajenos a esa crisis. Los especialistas en mercados energéticos suelen decir que “no existe energía más cara que aquella que no se tiene cuando se la necesita”. Y los empresarios santafesinos pueden dar cuenta de eso.

¿Cuál es la rentabilidad de la planta?

Respecto a la pregunta sobre la rentabilidad de la planta, el empresario aseguró que no existe una respuesta única porque los números –en una situación macroeconómica tan caótica– cambian mes tras mes. Pero en febrero de este año, por ejemplo, salieron empatados, es decir, las ganancias generadas por el aceite metilado para usar como coadyuvante en aplicaciones de fitosanitarios, valuado en 2,0 u$s/litro, permitieron compensar las pérdidas generadas por el mayor valor relativo del biodiésel respecto del precio del gasoil.

A partir de marzo las cuentas se tornaron más complejas, porque, además del desabastecimiento de gasoil, los precios cobrados por ese combustible en diferentes zonas del centro santafesino comenzaron a registrar variaciones muy importantes, que en algunas situaciones superaban por lejos el valor de la salida de planta del biodiésel de propia elaboración (155 $/litro).

La planta y su funcionamiento

La planta de biodiésel se montó sobre un predio, gestionado por uno de los socios, que tiene acceso a una toma trifásica de electricidad. “Si bien la unidad consume poca energía eléctrica, necesita una fuente de energía potente que pueda atender los pulsos intensos requeridos para calentar el aceite durante unas dos horas”, explicó el empresario CREA.

Una vez que el aceite de soja desgomado ingresa la planta de producción de biodiésel, se mezcla con un reactivo –provisto por la misma empresa cordobesa que montó la unidad– que rompe las cadenas carbonadas del aceite para alivianarlo. Como resultado de esa reacción química se obtiene biodiésel en una proporción del 85%, mientras que el 15% corresponde a glicerol. Ambos productos se destinan a un tanque cónico de decantación donde se elimina el glicerol por gravedad al ser más pesado.

El glicerol o glicerina es comprado por uno de los socios del emprendimiento, que lo emplea como aditivo energético en las raciones diseñadas para un feedlot propio. Un grupo de alumnos de la Facultad de Ciencias Agrarias de la Universidad Nacional del Litoral (UNL) investigó las propiedades nutricionales del producto en novillos y determinó que el mismo es adecuado como aporte energético. De hecho, están elaborando una tesina de grado al respecto para la cátedra de nutrición animal. “La planta de biodiésel, por lo tanto, no genera ningún desperdicio, lo que la hace un ejemplo de la denominada economía circular”, apunta Diego.

Cada socio dispone de una parte proporcional del capital de la planta de biodiésel y asimismo tiene derecho al uso del 25% del biocombustible generado, aunque mes tras mes los consumos realizados entre uno y otro van variando y se realizan luego ajustes periódicos de cupos.

La planta es gestionada por un empleado que trabaja a tiempo completo de lunes a viernes y se encarga tanto el proceso de elaboración como de los análisis químicos del aceite de soja recibido y del biodiésel producido, además de llevar registros de las operaciones y los números del negocio.

El encargado de la planta no tenía experiencia previa en el tema –antes de incorporarse era empleado administrativo–, pero fue capacitado por técnicos de la firma encargada de montar la unidad. Para el puesto, si bien no se requieren conocimientos previos, dado que los mismos pueden ser adquiridos, sí se necesita a una persona criteriosa y responsable.

El proceso de producción es continuo, de manera tal que el biocombustible se almacena en períodos estacionales de baja demanda para ser empleado en las épocas de siembra y cosecha. Todo el producto elaborado se emplea para consumo propio (no se comercializa).

El biodiésel se emplea puro en un 100% en todos los tractores y cosechadoras de las empresas propietarias de la planta. “No existe ningún problema en el uso de B100 (biodiésel puro al 100%); de hecho, a fines de 2020 adquirí un tractor nuevo que jamás uso gasoil de origen fósil”, comenta Diego.

“El sistema de la planta de biodiésel es muy sencillo y requiere una inversión baja si se hace en sociedad. Si bien, obviamente, una gestión eficiente de la misma es importante, la clave reside en considerarla no como un negocio, sino como una solución logística para el parque de maquinaria agrícola y, en definitiva, para el buen funcionamiento de los procesos y plazos de trabajo establecidos”, resume.

\*Fuente: Revista CREA