---
category: Agenda Ciudadana
date: 2022-01-19T06:00:22-03:00
thumbnail: https://assets.3dnoticias.com.ar/INDUSTRIAS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: " A pesar de las medidas sanitarias, el ausentismo en la industria de Santa
  Fe se elevó al 25%"
title: " A pesar de las medidas sanitarias, el ausentismo en la industria de Santa
  Fe se elevó al 25%"
entradilla: 'El nuevo protocolo de aislamiento sobre contactos estrechos fue bienvenido
  en el sector, pero crecen los infectados sintomáticos. La falta de testeos en menores
  de 60, genera inquietud en empresarios.

  '

---
Hace una semana, el Ministerio de Salud provincial dispuso nuevos criterios de aislamiento para contactos estrechos. Se esperaba que la medida tenga impacto en el sector productivo, aumentando el presentismo; sin embargo, eso no ocurrió y el ausentismo continuó creciendo y alcanzando, a este martes, el 25 por ciento en el sector industrial.

Así lo indicó el presidente de la Unión Industrial de Santa Fe, Alejandro Taborda, quien valoró "buena voluntad de las autoridades provinciales como para que la gente pueda trabajar".

Sin embargo, el referente industrial advirtió que hay "quejas de algunos industriales por los menores de 60 años sin comorbilidades que no se están testeando y, por lo tanto, con la aplicación del Cuidar, quedan aislados".

Indicó que "al no testearse uno no tiene un control (epidemiológico) y quien lo quiere tener, lo debe hacer de forma privada". Taborda definió esta situación como "lo único que quedó en el aire" y que genera inquietud entre los empresarios. "Todas las otras, son medidas consensuadas, estamos de acuerdo y apoyamos la iniciativa", graficó.

"Todavía no se ve una mejora importante porque el nivel de contagios es muy alto, estamos con un ausentismo de arriba del 25 por ciento". Taborda aseguró que la situación no mejoró en materia de presentismo laboral, aunque reconoció que "no es debido a las medidas; al contrario, creo que podría haber sido peor. Lo que hay es un nivel de contagios muy importante, mucha gente con síntomas"

En declaraciones al programa "De 10" que se emite por "LT 10", insistió con plantear la situación que se está dando entre los menores de 60 con síntomas: "No tenemos control, salvo que la empresa le pague un test particular. Hay muchas empresas que lo están haciendo, pero son las más importantes. En una Pyme, cuando faltan cuatro o cinco empleados de diez que tiene, realmente está complicado; no tienen la estructura para hacerlo".

Cabe recordar el nuevo protocolo establece que quienes tengan colocadas 2 dosis de la vacuna contra el Covid-19 con menos de 5 meses, hayan recibido las tres dosis con más de 14 días transcurridos o que hayan tenido Covid en los últimos 90 días, podrán regresar a sus actividades laborales.

Con respecto a los testeos, los nuevos criterios sanitarios establecen que quienes deben hisoparse son las personas con dos o más síntomas, con factores de riesgo o mayores de 60 años. En tanto, quienes padezcan dos o más síntomas, con o sin, contacto estrecho identificado (se consideran positivos).