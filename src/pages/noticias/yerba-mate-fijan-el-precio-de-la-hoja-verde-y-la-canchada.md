---
layout: Noticia con imagen
author: 3DNoticias
resumen: 'Yerba Mate: fijan el precio '
category: El Campo
title: 'Yerba Mate: fijan el precio de la hoja verde y la canchada'
entradilla: Los nuevos valores se extenderán hasta el 31 de marzo de 2021, y serán
  de $24.390 la tonelada de hoja verde puesta en secadero, y en $92.690 la tonelada
  de yerba mate canchada.
date: 2020-10-22T16:45:57.027+00:00
thumbnail: https://assets.3dnoticias.com.ar/yerba.jpg
link_del_video: ''

---
La Secretaría de Agricultura, Ganadería y Pesca, dependiente del Ministerio de Agricultura, Ganadería y Pesca de la Nación, fijó en 24.390 pesos la tonelada de hoja verde puesta en secadero, y en 92.690 pesos la tonelada de yerba mate canchada, también puesta en secadero.

Según lo establece la Resolución 113/2020 publicada en el Boletín Oficial de la Nación, los nuevos valores entrarán en vigencia a partir del 23 de octubre de 2020 y se extenderán hasta el día 31 de marzo de 2021.

Cabe resaltar que, en el caso de la hoja verde entregada por el productor, de un ajuste del 59,9% respecto de los valores fijados un año atrás, mientras que en el caso de la yerba canchada elaborada por molinos el mismo es de 59,5%.

En septiembre pasado, según el último dato disponible, el valor del paquete de medio kilo de yerba mate en la ciudad de Buenos Aires fue medido por el Indec en 124,2 $/kg, una cifra 48,2% superior a la registrada en el mismo mes de 2019.

#### 

Según los últimos datos publicados por Instituto Nacional de Yerba Mate (INYM) en los primeros ocho meses de 2020, se cosecharon 711,7 millones de kilogramos de hoja verde con ventas internas de yerba mate por 179,6 millones y exportaciones por 28,1 millones de kilos.