---
category: Agenda Ciudadana
date: 2021-08-10T06:00:13-03:00
thumbnail: https://assets.3dnoticias.com.ar/festrm.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Festram va al paro este jueves en reclamo de la apertura paritaria
title: Festram va al paro este jueves en reclamo de la apertura paritaria
entradilla: Lo resolvió el plenario de municipales de Festram que apunta a una nueva
  recomposición de los haberes en virtud de que la inflación superó en junio la pauta
  salarial acordada por el sector en marzo pasado.

---
En el mes de junio la inflación nuevamente superó la pauta salarial del sector municipal, situación que ya había ocurrido en abril respecto del primer tramo del acuerdo paritario. Por ese motivo desde la Federación de Sindicatos de Trabajadores Municipales de la Provincia de Santa Fe (Festram) les solicitaron a los intendentes y presidentes comunales que anticipen en julio la última cuota del aumento acordado y la revisión de la política salarial vigente, en consonancia con la política económica del Gobierno Nacional que ha ratificado que “el salario debe ganarle al índice inflacionario”.

En ese sentido, Cristina Fernández y Sergio Massa otorgaron un aumento superior al 40% a los empleados del Congreso, lo que luego el Presidente de la Nación ratificó personalmente firmando en testimonio la revisión del convenio salarial bancario con una pauta superior al 45%.

En este contexto -y tal como se viene informando- las cuentas fiscales transitan una etapa de amplia holgura con superávit fiscal, afirman desde la Federación de empleados municipales. "Al inicio de la pandemia los depósitos bancarios de Provincia y municipios y los montos que mantenían en plazos fijos y cuenta corriente superaban al sector privado. Los $ 6.732 millones de los 365 Municipios y Comunas conforman la liquidez más alta en la última década", sostuvieron desde Festram, que a su vez señala que cerrado el total de la liquidación 2020 de la coparticipación se ratifican las estimaciones que tenía la entidad "respecto de un ahorro de 10 mil millones de pesos en el sector municipal, sobre la base de la política salarial de ese año".

En ese contexto, argumentan que los gastos por la lucha contra el Covid-19 fueron cubiertos en su totalidad por el Tesoro Nacional, a lo que se sumarán repartos de ATN para compartir la carga fiscal obtenida del Aporte Solidario y Extraordinario de las Grandes Fortunas. Además -y siempre de acuerdo a lo que señalan desde el gremio-, el gobierno provincial tomó de los recursos municipales y comunales, más de 200 millones de pesos del Impuesto a los Ingresos Brutos para destinarlos al comercio, la industria y la agricultura.

Todo esto pone de manifiesto que el ajuste de las cuentas fiscales se hace sobre la caída del salario y la pérdida del poder adquisitivo de los trabajadores, sentencian.

Según datos del IPEC (Instituto Provincial de Estadísticas y Censos), en la Provincia de Santa Fe la inflación interanual se colocó en el 52,6%. Para el Centro de Estudios Económicos y Sociales –CESO- en junio “un adulto necesitó $12.183 para cubrir sus necesidades alimentarias básicas y una familia tipo de 4 integrantes necesitó $37.646 para no caer en la indigencia”. El mismo centro de estudios estableció que una familia tipo en Rosario, "necesita más de 100 mil pesos para satisfacer sus condiciones de vida mínimas”.

"Lamentablemente, la Justicia Social que se predica resulta infructuosa sin el correspondiente diálogo. Esta ausencia de convocatoria nos obliga a reclamar por nuestros salarios ejerciendo el derecho constitucional de huelga en reclamo de la apertura del ámbito paritario municipal", sostienen desde Festram.