---
category: La Ciudad
date: 2022-01-16T06:15:02-03:00
thumbnail: https://assets.3dnoticias.com.ar/santafe.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Un estudio mide cuál es el barrio más caliente de la ciudad de Santa Fe
title: Un estudio mide cuál es el barrio más caliente de la ciudad de Santa Fe
entradilla: 'El trabajo hace una comparación entre la situación en 1988 y 2019. Compara
  la temperatura de superficie terrestre con mediciones satelitales. La desigualdad
  en la distribución de los espacios verdes.

  '

---
En el marco de una ola de calor con temperaturas récord en la ciudad, el Observatorio Social y Económico Integrar difundió un trabajo que realizó con mediciones satelitales sobre la planta urbana de Santa Fe y comparó la situación de 1988 y la de 2019 respecto de las islas de calor. Esa investigación permite saber cuáles son los barrios más calientes y cuáles las zonas más frescas de la ciudad en términos de temperatura de superficie terrestre (TST).

Para medir la TST (que muestra cómo se calientan las cosas con el sol, por lo que es diferente de la temperatura ambiental que se mide todos los días pero incide en ella) se clasificó el territorio en tres grandes clases de cobertura: construcción urbana, vegetal y suelo (sin cobertura).

La comparación de las imágenes satelitales permiten analizar cómo se ocupó la superficie en la planta urbana de la ciudad de Santa Fe en los últimos 30 años.

La comparación de las imágenes satelitales permiten analizar cómo se ocupó la superficie en la planta urbana de la ciudad de Santa Fe en los últimos 30 años.

El análisis de cuáles son los puntos calientes o fríos en la planta urbana permite diseñar políticas públicas para planificar un "desarrollo sustentable de la ciudad, en lo que respecta al ahorro de energía, por nuestra propia economía y en pos de reducir el impacto humano en el clima", sostiene el informe titulado Árboles, plazas y calor urbano: la desigualdad verde en la ciudad de Santa Fe y que lleva la firma del investigador del Observatorio, Mauricio Savarino.

Las imágenes obtenidas por los satélites Landsat 5 y 8 corresponden a los días 23 de noviembre de 1988 y 29 de noviembre de 2019. Hace más de 30 años los puntos más calientes en el casco urbano se encontraban en el distrito Centro donde las temperaturas alcanzan más de 37ºC, mientras que los más fríos se encuentran en La Costa, rondando los 23ºC.

En ese momento el 45% del territorio de la ciudad estaba urbanizado, mientras que el 42% se componía de coberturas vegetales y sólo el 4.6% no tenía cobertura, y el restante era agua. Mientras que en 2019 la cobertura vegetal de la ciudad mostraba una reducción de superficie, cubriendo solo el 38% del territorio y una mayor superficie construida, que llegó al 54%. Además, la TST promedio se ubicó en 33.5ºC, 1.4º más que en 1988.

**Los espacios verdes, cantidad y calidad**

Según el informe de Integrar, en la ciudad de Santa Fe, teniendo en cuenta la población censada en 2010, hay 4,86 metros cuadrados por habitante de espacios verdes diseñados, como plazas y parques. En ese sentido agrega que la Ciudad de Buenos Aires, en 2018, reportó 6 m2/hab. Mientras que algunas recomendaciones internacionales sugieren al menos 10 m2/hab.

La distribución de los espacios verdes diseñados, como plazas y parques, se concentran en la superficie del sur, centro, este y noreste de la ciudad de Santa Fe.

La distribución de los espacios verdes diseñados, como plazas y parques, se concentran en la superficie del sur, centro, este y noreste de la ciudad de Santa Fe.

En la ciudad, de estos espacios verdes diseñados, casi el 4% está compuesto de vegetación densa (árboles), el 57% de vegetación rala, baja (como por ejemplo pasto) o en crecimiento, el 30% de superficies construidas, 6% de superficies sin cobertura y el restante agua. "Si bien importa la cantidad, también importa la calidad de los espacios verdes, los cuales deben contar con más cobertura vegetal densa", agrega el documento.

En cuanto a la distribución de estos espacios, el informe indica que en los distritos Centro y Este una vecina o vecino tiene más de un 80% de probabilidades de vivir a menos de 400 metros de una plaza o parque, a diferencia de una vecina o vecino del Oeste, Sudeste o Norte que tiene menos de 50% de probabilidades de vivir a 400 metros de un espacio verde diseñado; y desde 20% y menos, vecinos y vecinas de los distritos Noreste, Noroeste y Costa. "De esta forma comprendemos cómo se vive la desigualdad de acceso a los espacios verdes diseñados", apunta.

Las zonas celestes marcan los sectores de la ciudad de Santa Fe que tienen a menos de 400 metros los espacios verdes diseñados. 

Las zonas celestes marcan los sectores de la ciudad de Santa Fe que tienen a menos de 400 metros los espacios verdes diseñados.

**Los barrios con superficie más caliente**

Entre 1988 y 2019, los distritos Noroeste, Norte y Noreste son los que mostraron un mayor crecimiento de las coberturas de superficies construidas. "A la par de este fenómeno, los barrios del interior de estos distritos y que han crecido en superficies construidas desde finales de los 80 son los que muestran mayores temperaturas medias. Esto significa que no cuentan con coberturas vegetales con temperaturas más bajas, son islas de calor casi en sentido estricto, han crecido solo en superficie construidas. Al ser más calurosos pueden exigir mayores tasas de consumo energético y, en consecuencia, menos ahorro de energía", se argumenta en el informe.

"Los espacios verdes se encuentran concentrados en mayor medida en el centro-este-sureste", agrega el estudio e indica que "la integración de la ciudad también puede medirse en términos de temperaturas de superficie, donde la distribución de la sombra y los espacios verdes públicos diseñados se han llevado a cabo de modo desigual" y eso termina influyendo en las temperaturas de los distintos barrios. "Eso puede dar una pauta de dónde dirigir las políticas públicas en torno a la generación y diseño de espacios verdes de calidad", se concluye en el informe.

El mapeo de la ciudad de Santa Fe de acuerdo a las temperaturas de superficie terrestre es un insumo clave para diseñar políticas públicas para una ciudad sustentable.

El mapeo de la ciudad de Santa Fe de acuerdo a las temperaturas de superficie terrestre es un insumo clave para diseñar políticas públicas para una ciudad sustentable.

De la comparación entre 1988 y 2019 surge que el crecimiento de la ciudad hacia el norte y la disminución de las coberturas vegetales naturales en detrimento de mayor cantidad de superficies construidas exponen a esas zonas de la ciudad a mayores temperaturas de superficie terrestres. Hoy los barrios con promedios de temperaturas de superficie más elevados están en el oeste y norte de la ciudad de Santa Fe.

Según el estudio, barrio Piquete Las Flores es el lugar de mayor TST en la ciudad, seguido por los barrios Facundo Quiroga, Del Tránsito y San Martín. Mientras que la zona más fresca de la ciudad es La Boca de Alto Verde y a ese lugar le siguen Colastiné Sur, La Vuelta del Paraguayo y San Ignacio de Loyola Sur.

"Detectar las islas de calor es particularmente importante para diseñar políticas públicas ambientales de reforestación, ya que menor cantidad y calidad de estos espacios verdes significa no solo aumento de TST y mayor consumo de energía por el aumento de esas temperaturas, sino también pocos espacios de dispersión al aire libre, menos sombra, menos absorción del agua de lluvia, peor calidad de aire por la ausencia de árboles y por ende riesgos para la salud física y mental de las vecinas y vecinos de la ciudad", finaliza el trabajo del observatorio Integrar.