---
category: Deportes
date: 2021-11-30T06:00:39-03:00
thumbnail: https://assets.3dnoticias.com.ar/MESSIGOLD.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: 'Messi ganó su séptimo Balón de Oro: "Quiero compartirlo con mis compañeros
  de la Selección"'
title: 'Messi ganó su séptimo Balón de Oro: "Quiero compartirlo con mis compañeros
  de la Selección"'
entradilla: '"Siempre que me daban este premio sentía una espina por no conseguir
  algo con mi país y hoy puedo disfrutarlo", expresó la Pulga en Paris.

'

---
Lionel Messi ganó este lunes su séptimo Balón de Oro al ser elegido como el mejor futbolista de la temporada 2021, en un evento realizado por  
la revista France Football en la ciudad de Paris, y expresó su deseo de compartirlo con sus compañeros de la Selección argentina.

"Me preguntaban cuándo me iba a retirar, hoy me toca estar feliz en París y feliz de estar acá. Tengo muchas ganas de seguir peleando por nuevos retos, no sé cuánto me queda pero ojalá que sea mucho porque amo el fútbol", expresó Messi.

Y agregó: "Quiero agradecerles a todos mis compañeros del Barcelona y del París. Y especialmente a mis compañeros de la Selección argentina y al cuerpo técnico”.

En ese sentido, continuó: "Siempre que me daban este premio sentía una espina por no conseguir algo con mi país y hoy puedo disfrutarlo. Este premio es por lo que hicimos en la Copa América y quiero compartirlo con todos mis compañeros de la Selección".

La "Pulga" ya había sido galardonado con el Balón de Oro en seis oportunidades -2009, 2010, 2011, 2012, 2015 y 2019- y es el jugador que más veces lo ganó en la historia, aunque hasta el momento siempre había sucedido con el rosarino representando al Barcelona de España.

Ahora, el futbolista del Paris Saint Germain de Francia era el candidato número uno de una larga lista en la que Robert Lewandowski culminó segundo, Jorginho tercero, Karim Benzema cuarto y Ngolo Kanté quinto, además de su compatriota y compañero de la Selección argentina Lautaro Martínez, que terminó en el puesto 21.

"Quiero decirle a Robert que es un honor pelear con él. Te merecés tu Balón de Oro, el año pasado todo el mundo estaba de acuerdo en que fuiste el ganador y France Football debería darte tu Balón de Oro. Ojalá pueda otorgártelo y lo tengas en tu casa porque fuiste un justo ganador. No se pudo hacer por la pandemia, pero tenés que tenerlo en tu casa vos también", le dijo Messi a Lewandowski desde el escenario.

El uruguayo Luis Suárez, ex compañero y amigo del capitán del seleccionado albiceleste, fue el encargado de entregarle el Balón de Oro y luego la Pulga apretó el botón rojo en el Teatro del Châtelet para encender un cartel junto a la iluminada Torre Eíffel en la ciudad parisina.

En 2021, Messi fue campeón, mejor jugador y goleador de la Copa América 2021 con la Selección argentina -su primer título en la mayor-, Pichichi de LaLiga de España por octava vez con 30 goles en el Barcelona -donde además ganó la Copa del Rey- y en la actualidad se desempeña en la Ligue 1 con el PSG.

El premio es seleccionado mediante una votación en la que participan 180 periodistas especializados, uno por país, que otorgan un ranking de cinco nominados.

El primero de la lista recibe seis puntos, el segundo cuatro, el tercero tres, el cuarto dos y el quinto uno, por lo que el jugador que cosecha más unidades es quien se queda con el trofeo.

Messi es el futbolista que más veces obtuvo el premio, seguido por Cristiano Ronaldo (cinco), Michel Platini, Marco Van Basten y Johan Cruyff (tres) y Franz Beckenbauer, Ronaldo, Alfredo Di Stéfano, Kevin Keegan y Karl-Heinz Rummenigge (dos).

El evento también contó con homenajes para Diego Armando Maradona, a poco más de un año de su fallecimiento, y para el alemán Gerhard Müller, cuya partida física se dio en agosto de 2021.