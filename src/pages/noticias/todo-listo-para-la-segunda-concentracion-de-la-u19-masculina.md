---
category: Deportes
date: 2021-05-19T07:22:08-03:00
thumbnail: https://assets.3dnoticias.com.ar/basquet.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa CAB
resumen: Todo listo para la segunda concentración de la U19 masculina
title: Todo listo para la segunda concentración de la U19 masculina
entradilla: Con 18 preseleccionados, el equipo que conduce Daniel Farabello continúa
  su preparación para el Mundial de Letonia. El grupo se entrenará desde este jueves
  en el Cenard.

---
Comienza una nueva etapa para la Selección U19 masculina. Del jueves 20 al domingo 30 de este mes, en las instalaciones del Cenard, se llevará a cabo la segunda concentración de cara al Mundial de la categoría, que se jugará en Letonia del 3 al 11 de julio.

El cuerpo técnico que encabeza Daniel Farabello dispuso la citación de 18 jugadores. Luego de la primera convocatoria realizada en abril, se redujo el grupo inicial y, para esta ocasión, se sumarán cuatro de los siete jugadores convocados que militan en el exterior: Juan Fernández -recientemente ascendido a la LEB Plata en España-, Silvano Merlo, Pedro Rossi y Gonzalo Corbalán.

La idea del cuerpo técnico una vez finalizada esta etapa es realizar una tercera convocatoria en junio, donde se espera que se sumen los jugadores que están compitiendo en Europa y Estados Unidos. La misma será más extensa que las anteriores y servirá para definir el plantel y dejar todo listo para emprender el viaje a Riga y Daugavpils, las ciudades letonas en las que se jugará el Mundial.

Vale recordar que, tras el sorteo oficial de FIBA, quedó confirmado que Argentina integrará el Grupo C junto a Francia, España y Corea.

LISTA - PRESELECCIÓN NACIONAL U19

Tomás Allende

Clase: 2002

Equipo: Quimsa

Alejo Azpilicueta

Clase: 2002

Equipo: Bahía Basket

Nicolás Burgos

Clase: 2003

Equipo: Boca Juniors

Esteban Cáffaro

Clase: 2003

Equipo: Trebolense

Ian Cerino

Clase: 2002

Equipo: San Lorenzo de Almagro

Juan Cruz Conte Grand

Clase: 2002

Equipo: Boca Juniors

Federico Copes

Clase: 2002

Equipo: Platense

Gonzalo Corbalán

Clase: 2002

Equipo: Lubbock University (EEUU)

Jeremías Diotto

Clase: 2002

Equipo: San Isidro de San Francisco

Juan Francisco Fernández

Clase: 2002

Equipo: Fuenlabrada (ESP)

Alejo Maggi

Clase: 2002

Equipo: Ferro Carril Oeste

Franco Méndez

Clase: 2002

Equipo: San Martín de Corrientes

Silvano Merlo

Clase: 2002

Equipo: C.B. Ciudad de Ponferrada (ESP)

Bautista Ott Cywarlie

Clase: 2002

Equipo: Ameghino de Villa María

Mateo Pérez

Clase: 2002

Equipo: Libertad de Sunchales

Manuel Rodríguez Ortega

Clase: 2002

Equipo: Boca Jrs

Pedro Rossi

Clase: 2002

Equipo: DME Academy (EEUU)

Facundo Tolosa

Clase: 2002

Equipo: Bahía Basket

DT: Daniel Farabello

AT: Juan Manuel Gattone

AT: Mariano Junco

AT: Juan Gatti

PF: Matías Suhurt