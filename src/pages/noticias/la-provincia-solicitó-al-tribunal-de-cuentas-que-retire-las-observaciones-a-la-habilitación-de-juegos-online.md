---
layout: Noticia con imagen
author: 3DNoticias
resumen: Juegos Online
category: Estado Real
title: La Provincia solicitó al Tribunal de Cuentas que retire las observaciones
  a la habilitación de juegos online
entradilla: "Fiscal de Estado Rubén Weder:  “hay una serie de ilegitimidades e
  irregularidades en el acto de observación” de los vocales del alto tribunal."
date: 2020-11-26T12:50:05.537Z
thumbnail: https://assets.3dnoticias.com.ar/juego-clandestino.jpg
---
El Poder Ejecutivo provincial le solicitó al Tribunal de Cuentas a través del decreto Nº1451 refrendado por el gobernador Omar Perotti, que retire por ilegítimas e improcedentes, las observaciones realizadas a la habilitación de los juegos online.

El fiscal de Estado, Rubén Weder, brindó este miércoles una conferencia de prensa en el Salón Blanco de la Casa de Gobierno, donde señaló que: “El gobernador Perotti, a través de un decreto firmado el día 20, le pide al Tribunal de Cuentas el retiro del acto de observación legal sobre la autorización complementaria a los casinos para que se habiliten los juegos online”.

Weder fundamentó su pronunciamiento señalando que se trata de “una insistencia en este tema al entender que hay ciertas ilegitimidades o irregularidades en el acto de observación del Tribunal de Cuentas, que tienen que ser retiradas porque el gobernador entiende que el juego online está permitido, de acuerdo a la interpretación de la Ley vigente. Lo que no está permitido es la modalidad, una cosa es el juego y otra la modalidad”.

En ese sentido, explicó que lo que hizo con el vicepresidente de la Caja de Asistencia Social (Rodolfo Cattáneo), “es autorizar una nueva modalidad en base a potestades que le da la Ley para adecuar los juegos a las nuevas realidades y modalidades, como es el tema del avance tecnológico”.

Además, indicó que otro punto que se tuvo en cuenta para habilitar los juegos online fue “la quiniela clandestina, que le quita fondos al Estado para que éste los use para atender a las demandas sociales”.

Otra cuestión que remarcó Weder fue “la extemporaneidad en la decisión del Tribunal de Cuentas”. Al respecto, el fiscal de Estado fue claro: “Decimos extemporaneidad porque cuando el vicepresidente de la Caja de Asistencia Social dicta las resoluciones 163, 164 y 165, las remite al Tribunal de Cuentas, dónde son recibidas y no les hacen ninguna observación, y después cuando llega el decreto lo observan. Lo único que hace el decreto es aprobar las resoluciones no observadas del Tribunal de Cuentas. Esto nos parece ciertamente inoportuno”.

Weder cuestionó en términos concretos y fundados el funcionamiento del Tribunal de Cuentas en este caso: “Para el Poder Ejecutivo provincial, el Tribunal de Cuentas, desconoce las opiniones previas que brindaron cinco reparticiones técnicas que habían avalado la gestión diciendo que no había observaciones para hacer. Esto puede verse en el decreto Nº 998/20 donde emitieron su opinión favorable al mismo la Dirección General de Casino y Bingos, la Coordinación Técnico Legal de la Dirección General de Casinos,la Dirección General de Asesoría Jurídica de la CAS, la Dirección General de Asesoría Letrada del Ministerio de Economía y la Fiscalía de Estado”.

Para el Fiscal de Estado, hay un momento clave que debe ser analizado puntualmente: “Cuando el decreto llega para su control en el Tribunal de Cuentas –no digo en el Ejecutivo- en el propio Tribunal de Cuentas, interviene el Delegado Fiscal que es quien recibe primero el expediente, y dice “sin observación” y lo eleva al Fiscal General del Área 2, que dice textualmente “analizando el decreto no hay observaciones para hacer” y se trata de organismos propios del Tribunal de Cuentas que emiten esta opinión”.

Weder se detuvo a marcar un punto concreto: “Cuando llega a la vocalía del Dr. Villar y sin siquiera mencionar todos esos antecedentes, propone la observación legal que luego es asumida por tres integrantes del Tribunal de Cuentas. Nosotros con esto estamos objetando el funcionamiento de este tribunal”. En ese sentido el fiscal expresó que “el Tribunal de Cuentas funciona en acuerdos plenarios y funciona en salas. La ley establece que para que exista acuerdo plenario, tienen que estar presentes la totalidad de los integrantes del Tribunal de Cuentas. Para las observaciones, la ley establece que para el acuerdo plenario tienen que estar presentes todos los integrantes, es decir, con sus cinco integrantes. La resolución del Tribunal de Cuentas que objeta al Ejecutivo apareció con la firma solamente de tres de sus integrantes”.

“Esto en derecho se llama se llama ´ir contra la teoría de los actos propios´, cuando uno realiza una conducta, el otro prevé que va a seguir actuando de esa manera, por eso creemos que ahí hay un vicio en el acto”, finalizó Weder.

**DECRETO, HABILITACIÓN Y OBSERVACIÓN**

* El Decreto del Poder Ejecutivo Nº 324/20 facultó a la Caja de Asistencia Social (CAS) a “autorizar la modalidad del juego on-line”.


* El pasado 11 de septiembre de 2020 la Caja de Asistencia Social dictó las resoluciones 163, 164 y 165 donde adecuó, dentro de los contratos de concesión vigente y por el plazo faltante, los juegos habilitados a la modalidad online sin extenderse más allá del plazo original.


* El 14 de septiembre la Delegación Fiscal del Tribunal de Cuentas receptó las resoluciones de la Caja de Asistencia Social sin hacer el más mínimo reparo.


* El 21 se septiembre el Poder Ejecutivo dictó el Decreto Nº 998/20 donde aprobó las resoluciones de la Caja de Asistencia Social, que no habían recibido ningún tipo de cuestionamiento por parte del Tribunal de Cuentas. Ningún criterio en sentido contrario, ningún reparo puesto en este decreto.


* El 6 de noviembre el Tribunal de Cuentas, con la presencia de sólo tres de sus integrantes y no de los cinco como requiere la ley, reitero, tres de cinco integrantes, observó legalmente el Decreto Nº 998/20.