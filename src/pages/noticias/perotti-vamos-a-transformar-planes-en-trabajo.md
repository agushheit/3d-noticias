---
category: Agenda Ciudadana
date: 2021-10-11T06:00:12-03:00
thumbnail: https://assets.3dnoticias.com.ar/perotti.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Perotti: "Vamos a transformar planes en trabajo"'
title: 'Perotti: "Vamos a transformar planes en trabajo"'
entradilla: |2-

  El gobernador Perotti encabezó en Reconquista un encuentro con dirigentes políticos de diferentes ciudades y comunas de departamentos del norte provincial.

---
El gobernador Omar Perotti, destacó este sábado en Reconquista que, en lo que va de su gestión, "la decisión política de cuidar la salud de los santafesinos y las santafesinas durante la pandemia; y la de acompañar a quienes producen, invierten y generan trabajo, fueron dos ejes clave para posicionar a Santa Fe como uno de los distritos del país con mayores indicadores de crecimiento económico en la actualidad" resaltó el mandatario provincial.

Perotti encabezó en esa ciudad un encuentro con dirigentes, referentes políticos, candidatos y candidatas del espacio Hacemos Santa Fe del norte de la provincia, que se llevó a cabo en la sede del Sindicato de Camioneros. Del encuentro participaron, entre otros, el senador nacional, Roberto Mirabella; el senador provincial, Marcelo Lewandowski; el intendente de Reconquista, Amadeo Enrique Vallejos; y la secretaria de Desarrollo Humano de la Municipalidad, María Haydee Maggio.

A la tarde el mandatario celebró la presencia de intendentes y presidentes comunales de localidades de los departamentos 9 de Julio, General Obligado, Vera, San Justo y San Javier, a quienes les recordó que “no hubo período como este en la historia de la provincia, donde se ha registrado la mayor transferencia de recursos para los pueblos y ciudades”.

Perotti agregó: “Claro que sí. Hubo acciones de discriminación reales y concretas que se vivieron en etapas anteriores y que no fueron menores. No hacer las obras complementarias para la llegada del gas o el agua potable aquí, en Reconquista, fue una de ellas. Nosotros estamos convencidos de que hoy, al retomar el ritmo de la obra pública, podemos continuar y empezar a saldar deudas de años anteriores”.

En ese sentido mencionó que su gestión comenzó con una deuda de 6.000 millones de pesos “que tuvimos que juntarlos peso por peso para culminarlas, porque se habían licitado sin plata. Y a los pocos meses, aparece la pandemia, y también pusimos en marcha el mayor programa de asistencia alimentaria para llegar al que más lo necesitaba. La oposición quería hacernos enojar, pero nosotros respondimos de la mejor manera, porque cada uno puso lo mejor de si para llegar a este momento de encontrarnos otra vez”, expresó el mandatario provincial.

El gobernador Perotti encabezó en Reconquista un encuentro con dirigentes políticos de diferentes ciudades y comunas de departamentos del norte provincial. Habló de trabajo en la provincia.

Perotti señaló más adelante que la predisposición del Gobierno Nacional, a través del presidente Alberto Fernández, fue “vital para la llegada de respiradores artificiales y el montaje de camas críticas, para hacer frente a la pandemia de coronavirus en el período más complicado. Por eso tomamos la decisión de equipar al hospital de Reconquista, pero también ahora San Javier y Vera cuentan con una terapia, lo que facilita la atención de la salud”, manifestó.

Y continuó profundizando el tema al señalar que “cuando empezaron a llegar las vacunas, iniciamos una campaña de vacunación con más de 300 vacunatorios en toda la provincia. A la par, seguimos acompañando a quien produce, invierte y genera trabajo. Y todo fue posible porque pudimos gestionar los recursos de cada uno de ustedes con austeridad y compromiso”.

“Hay una capacidad de reacción que tiene el peronismo para poder escucharnos -continuó-, porque ponemos en juego lo que estamos haciendo, lo que año tras año no se hacía. Nosotros firmamos convenios, gestionamos acuerdos con el Gobierno Nacional. Pero necesitamos de nuestros legisladores para seguir haciendo con recursos nacionales y provinciales”, precisó Perotti.

Más adelante, el gobernador de Santa Fe afirmó: “Se vio en las últimas elecciones PASO que hay muchos interesados en que no se discutan cuestiones de fondo, sobre quién nos dejó esta tremenda deuda internacional, o quién nos dejó sin Ministerio de Salud. Ahora se escandalizan, cuando fueron ellos quienes triplicaron los planes sociales mientras cerraban las industrias. Ellos convirtieron el trabajo en planes, y nosotros tenemos convertir esos planes en trabajo”, sentenció Perotti.

**“Además de escuchar y hablar, hacemos”**

A su turno, la secretaria de Desarrollo Humano de Reconquista y candidata a diputada nacional, María Haydee Maggio, expuso que “lo importante acá es que además de escuchar y hablar, hacemos, y eso es lo que tenemos que hacer, seguir haciendo, y hablar de lo que hacemos. Mejor identificación no podemos tener con Hacemos Santa Fe; escuchaba a algunos que decían que este gobierno de Omar Perotti tiene en cuenta a cada rincón, no solamente las ciudades grandes. Pero tenemos que saber que depende de cada uno que gestionemos, aquel que gestionó en la provincia tuvo una respuesta, y es importante escuchar, que conozcan nuestra realidad”, dijo Maggio.

Y agregó: “Ese Todos quiere decir que estamos construyendo con el otro. Cuando miramos que está pasando desde que estamos gobernando esta provincia, tenemos el orgullo de tener un gobernador que escucha y permite esta construcción colectiva, donde cada uno desde su lugar demanda lo que necesitan sus ciudadanos, pero todos tenemos el mismo objetivo: queremos que nuestra provincia tenga cada vez más educación, más salud, más producción, porque sabemos que, desde el desarrollo local, nuestros jóvenes van a tener trabajo. Esto es lo que nos tiene que identificar, esta libertad que le tenemos que dar al joven con su trabajo”, subrayó.

**Defender el modelo político**

Por su parte, el senador nacional Roberto Mirabella, afirmó durante el acto llevado adelante en Reconquista que “es importante que entendamos lo que sucedió en esta coyuntura electoral, y entender la realidad, para saber cómo enfrentar esta coyuntura el próximo 14 de noviembre. Después de lo que nos tocó, arrancar una transición tormentosa, comenzar el gobierno de una manera complicada porque no había plata para pagar sueldos y aguinaldos, la pandemia, una legislatura boicoteando permanentemente al gobernador. A pesar de todo eso, se está haciendo una cantidad de cosas enorme, en cada rincón de la provincia”, manifestó el legislador nacional.

Mirabella finalizó su alocución señalando que “hay que sobresaltar que se ordenaron las cuentas públicas, porque sino no se hubiesen podido hacer la cantidad de cosas que hoy se están haciendo. Se pagan los sueldos del 1 al 6 de cada mes. Esto se puede hacer porque se está administrando como corresponde. Este modelo político es lo que tenemos que defender puerta a puerta. Nosotros somos de acá, de Fortín Olmos, de Villa Ocampo, de Calchaquí, de Florencia, Reconquista, de Esperanza, de Rafaela, de Las Parejas, de Gato Colorado; no tenemos que explicar nada, cada uno de nosotros le miramos la cara a la gente y caminamos por la calle tranquilos”, concluyó el candidato a Diputado Nacional por Santa Fe.

Saldar deudas históricas

A su turno, el intendente de Reconquista, Amadeo Enrique Vallejos, manifestó que “hoy, después de la pandemia, poder fortalecer cada uno de nuestros espacios locales, es poder seguir creciendo con todo lo que se está demostrando desde nuestro gobierno provincial, y allí creemos que es la tarea de todos: poder demostrar a los ciudadanos que en este corto tiempo se pudieron concretar obras históricas como la planta de bombeo, que posibilitará que toda la ciudad pueda tener agua potable”, recordó.

Vallejos agregó a la par que “estamos conectando el gas natural, hace pocos días se licitó la planta de tratamiento de efluentes cloacales y eso posibilitará contar con el saneamiento básico. Son obras que a veces no se ven pero que se sienten y hoy el peronismo está saldando una deuda histórica en toda la provincia”, concluyó Vallejos.

“Esta capacidad que tiene el peronismo de poder cambiarle la calidad de vida a la gente, también debe ser percibida por la ciudadanía, porque estamos hablando de cosas concretas que ayudan en el dia a día a cada ciudadano. No será sencilla esta etapa que nos queda, y seguramente allí tendremos que dar lo que esté a nuestro alcance y un poco más, porque solo el peronismo sabe que hoy puede llegar a cada uno de los ciudadanos y mirarlos a la cara, y pedirles que en esta nueva etapa vuelvan a acompañarnos, simplemente haciéndoles ver lo que se está haciendo con obras históricas, proyectos que parecían soñados y que hoy son realidad”, finalizó Vallejos.