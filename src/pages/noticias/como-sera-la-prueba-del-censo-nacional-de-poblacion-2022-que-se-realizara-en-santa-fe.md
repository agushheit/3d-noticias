---
category: Agenda Ciudadana
date: 2021-12-11T06:15:38-03:00
thumbnail: https://assets.3dnoticias.com.ar/CENSO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Cómo será la prueba del Censo Nacional de Población 2022 que se realizará
  en Santa Fe
title: Cómo será la prueba del Censo Nacional de Población 2022 que se realizará en
  Santa Fe
entradilla: 'La prueba piloto en la provincia se realizará en la ciudad de Gálvez,
  junto con otras dos comunas de Capital Federal. El Censo Nacional comenzará el 16
  de marzo y se extenderá hasta el 18 de mayo.

'

---
La ciudad santafesina de Gálvez fue elegida junto a dos comunas de la Ciudad Autónoma de Buenos Aires para la realización del Censo Experimental del 12 de diciembre, que pondrá a prueba los instrumentos que se usarán en el Censo Nacional de Población, Hogares y Viviendas 2022.

Este relevamiento de datos corresponde a la prueba general "antes de salir a escena". "Estamos probando en pequeñas zonas el Censo para poner ajustar detalles sobre, entre otras cosas, la logística, el formulario, el tiempo de demora", contó Marco Lavagna, director del Instituto Nacional de Estadística y Censos, en De 12 a 14 (El Tres).

Lavagna explicó que el Censo 2022 Experimental contará de dos etapas: "La primera donde la gente se podrá autocensar y después el puerta por puerta dónde se hará una simulación que será este domingo". Se trata de 600 mil personas afectadas en el país, mientras que en Santa Fe serán 42 mil personas recorriendo la provincia.

Por su parte, el Censo Nacional de Población, Hogares y Viviendas 2022 comenzará el 16 de marzo hasta el 18 de mayo, que será Feriado Nacional para que los ciudadanos estén en sus hogares ante la llegada de los censistas. A esta medición se sumarán "cuestiones de identidad de género, autorreconocimiento étnico (pueblos indígenas y afrodescendientes) y se actualizan preguntas".

El director del Indec mencionó que aquellos que completen el formulario digitalmente solo deberán presentar un código numérico al censista "lo que agilizará el tramite".