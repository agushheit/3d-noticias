---
category: Agenda Ciudadana
date: 2021-12-04T06:15:29-03:00
thumbnail: https://assets.3dnoticias.com.ar/UTA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Sin acuerdo en paritarias, UTA advierte medidas de fuerza para la semana
  que viene
title: Sin acuerdo en paritarias, UTA advierte medidas de fuerza para la semana que
  viene
entradilla: 'También se le suma el pago de haberes de noviembre, que debería realizarse
  el próximo lunes. "Esperemos de cobrarlo para no llegar a tener un tipo de medida",
  dijo Agrafogo, titular del sindicato de choferes en Santa Fe

'

---
Desde el gremio de los choferes de colectivos UTA advirtieron que aun no alcanzaron un acuerdo paritario con los empresarios y no descartan nuevas medidas de fuerza para la semana que viene.

Desde el sindicato a nivel local señalaron que el próximo lunes (cuarto día hábil) los choferes deberían cobrar el sueldo de noviembre; algo que de no ocurrir también podría activar un paro.

"No, no ha habido avance con las paritarias. Todavía no se pudo cerrar en el interior del país; sí en Capital Federal y Gran Buenos Aires, lo que es el Amba. Pero no en el interior", manifestó Osvaldo Agrafogo, secretario General de UTA Santa Fe.

La medida de fuerza pautada para el viernes pasado que finalmente fue desactivada "se levantó por un compromiso con el estado nacional de agilizar los subsidios. Ahora tienen la promesa de que los subsidios lo van a empezar a cobrar. Pero hasta ahora no se ha firmado la paritaria".

"Había más de ocho mil millones pesos que estaban atrasados y era lo que aducían los empresarios que no estaban cobrando. Bueno, se hicieron todos los pasos a nivel nacional para que esto lo pudieran cobrar y en estos días estarían cobrando una parte y dentro de 15 días cobrarían la otra parte. Calculamos que al recibir eso se podría llegar a cerrar la paritaria", apuntó en declaraciones a la emisora LT 10.

Por otro lado, Agrafogo advirtió: "También tenemos el vencimiento de los salarios del mes de noviembre el día lunes. Esperemos de cobrarlo para no llegar a tener un tipo de medida. Si al cuarto día hábil no están los salarios habrá medidas de fuerza. Esto lo saben los empresarios. Entendemos y comprendemos que a veces no cobran los subsidios pero los salarios de los trabajadores lo tienen que cumplir en tiempo y forma".