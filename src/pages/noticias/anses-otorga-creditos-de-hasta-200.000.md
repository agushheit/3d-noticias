---
category: Agenda Ciudadana
date: 2021-07-21T06:00:06-03:00
thumbnail: https://assets.3dnoticias.com.ar/ANSES.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Anses otorga créditos de hasta $ 200.000
title: Anses otorga créditos de hasta $ 200.000
entradilla: El organismo dirigido por Fernanda Raverta autorizó a tres grupos de beneficiarios
  a recibir un préstamo, a pagar hasta en 48 cuotas.

---
La Administración Nacional de la Seguridad Social (ANSeS) lanzó una línea de préstamos de hasta $ 200.000 para tres grupos de beneficiarios. Pueden acceder a estos montos jubilados, pensionados, personas con invalidez o madres de siete hijos.

En este caso, aquellos titulares que reciban los subsidios Asignación Universal por Hijo (AUH) o Asignación Universal por Embarazo (AUE) no tendrán acceso a estos préstamos. El crédito se puede solicitar 100% online y antes de pedirlo se puede simular la cuota. En cuanto a la tasa, el préstamo a pagar en 24 meses tiene un costo financiero total de 34,59%, a 36 es de 35,53% y a 48 es de 36,03%.

**Quiénes pueden acceder a los créditos ANSeS de hasta $ 200.000:**

Jubilados y pensionados,

Beneficiarios de la Pensión Universal para el Adulto Mayor (PUAM) o pensión no contributiva por vejez.

Quienes cobran pensiones no contributivas: Invalidez y Madre de 7 hijos

**Características y requisitos por grupo beneficiario**

Para jubilaciones y pensiones: Desde $ 5.000 hasta $ 200.000 en 24, 36, ó 48 cuotas La cuota no puede exceder el 20% del ingreso mensua.l El crédito se deposita en la cuenta bancaria dentro de los 5 días hábiles

Para Pensión Universal para el Adulto Mayor o pensión no contributiva por vejez: Desde $ 5.000 hasta $ 70.000 en 24, 36, ó 48 cuotas. La cuota no puede exceder el 20% del ingreso mensual. El crédito se deposita en la cuenta bancaria dentro de los 5 días hábiles

Para pensiones no contributivas por invalidez o madre de 7 hijos: Desde $ 5.000 hasta $ 70.000 en 24, 36, ó 48 cuotas. La cuota no puede exceder el 20% del ingreso mensual. El crédito se deposita en la cuenta bancaria dentro de los 5 días hábiles

**Cuál es la documentación requerida para acceder a los créditos**

Para acceder a los créditos de ANSES es necesario contar con Documento Nacional de Identidad (DNI). También, quien lo solicita debe tener un CBU de una cuenta bancaria propia, que se obtiene en el banco, cajero automático o home banking.

**Cómo se solicita el crédito:**

1 - Entrar a Mi ANSES con el CUIL y Clave de la Seguridad Social

2 - Luego, hay que dirigirse al “Menú” y seleccionar la opción "Créditos ANSES > Solicitar crédito"

3 - A continuación, se debe seleccionar por cual beneficio se quiere solicitar el crédito

4 - Se deben completar los datos, seleccionar el monto y la cantidad de cuotas

5 - Enviar la solicitud

6 - Descargar el comprobante