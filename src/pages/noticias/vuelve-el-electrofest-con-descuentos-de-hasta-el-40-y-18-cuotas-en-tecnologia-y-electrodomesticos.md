---
category: Agenda Ciudadana
date: 2021-08-25T06:00:30-03:00
thumbnail: https://assets.3dnoticias.com.ar/ELECTROFEST.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Vuelve el Electrofest, con descuentos de hasta el 40% y 18 cuotas en tecnología
  y electrodomésticos
title: Vuelve el Electrofest, con descuentos de hasta el 40% y 18 cuotas en tecnología
  y electrodomésticos
entradilla: Se trata de la sexta edición del evento que organizan las principales
  cadenas de retail de la Argentina y se desarrollará desde el 30 de agosto al 1 de
  septiembre.

---
El lunes 30 de agosto comenzará una nueva edición de Electrofest, el evento para la compra de artículos de tecnología y electrodomésticos con descuentos de hasta el 40% y planes de pago de hasta 18 cuotas sin interés que organizan las principales cadenas de retail de la Argentina.

Será la sexta edición del evento, que se extenderá hasta el miércoles 1 de septiembre y al que podrá accederse tanto de forma presencial como online o telefónica a una amplia variedad de productos que pueden consultarse en www.electrofest.com.ar.

Habrá ofertas en categorías como LED TV, telefonía celular, aires acondicionados, computadoras y electrodomésticos, entre otros.

Surgido en 2019 como propuesta conjunta de las principales cadenas de venta de electrodomésticos del país, en esta edición tendrá como empresas participantes a Cetrogar, Frávega, Musimundo, Megatone, Naldo y Pardo.

"Esta nueva edición del evento nos encuentra más preparados y con muchas ofertas disponibles. Es un evento para aprovechar tanto en sucursales como en nuestra web y encontrar ofertas en las principales categorías”, afirmó Juan Manuel Almeida, gerente de Marketing de Cetrogar.

"Vamos a tener una propuesta imperdible con descuentos agresivos y cuotas sin interés en las principales marcas de tecnología y electrodomésticos, con la posibilidad de comprar desde cualquiera de nuestras sucursales o desde Fravega.com, con entrega inmediata en nuestro pick up, entrega a domicilio en 48hs y amplia variedad de productos", expresó Juan Martín Romero, director ejecutivo de Negocios & Tecnología de Frávega.

**Las ediciones anteriores**

La primera edición del 2021 tuvo lugar en marzo de este año y registró aumentos de hasta 120% en las ventas, con un marcado crecimiento de las operaciones por el canal de e-commerce.

La facturación por la venta de productos electrónicos durante el primer trimestre de 2021 fue de $ 59.247,1 millones, lo que representa un aumento del 97,4% respecto del mismo período de 2020, según la última encuesta de "Comercios de electrodomésticos y artículos para el hogar" del Indec.

Los artículos que tuvieron mayor volumen de ventas fueron “Teléfonos celulares”, con 525.094 unidades (+39,3%); “Pequeños electrodomésticos”, con 492.289 unidades (+7,6%); “Televisores”, con 246.532 unidades (+3,6%); “Ventiladores”, con 226.091 unidades (-16,4%); “Equipos de aire acondicionado”, con 148.960 unidades; y “Heladeras con y sin freezer”, con 116.951 unidades (-7,3%).

Por detrás le siguieron “Lavarropas”, con 95.387 unidades (+18,6%); “Impresoras y scanners”, con 80.428 unidades (+31,1%); “Calefones y termotanques”, con 78.471 unidades (+19,4%); y “Computadoras (PC, notebook, all in one, etc)”, con 71.303 unidades (+57,6%).