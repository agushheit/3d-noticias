---
category: Agenda Ciudadana
date: 2022-11-15T23:13:37-03:00
thumbnail: https://assets.3dnoticias.com.ar/gnc.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Surtidores.com.ar'
resumen: 'Las Estaciones de Servicio argentinas y la movilidad que se viene: ¿GNC
  o electricidad?'
title: 'Las Estaciones de Servicio argentinas y la movilidad que se viene: ¿GNC o
  electricidad?'
entradilla: Ante la falta de un marco normativo que fomente la electro movilidad,
  el sector sugiere que las expendedoras miren al gas natural para el corto plazo
  y que poco a poco se preparen para instalar cargadores eléctricos.

---
El Gobierno nacional insiste en que la principal oportunidad de la movilidad eléctrica radica en el transporte urbano de pasajeros mediante la existencia de un plan escalonado hacia el futuro, como hizo con la oficialización del Plan Nacional de Transporte Sostenible.

**Marcelo Torres**, subsecretario de Proyectos Estratégicos y Desarrollo Tecnológico del Ministerio de Transporte de la Nación, reconoció que “_se debe tratar de entender lo que es desarrollar un plan de transporte y movilidad urbana por cada una de las unidades políticas e institucionales que tiene el país_” y que “_ese segmento es el aliado más importante para la electromovilidad y la transición energética_”.

Sin embargo, hoy en día **los costos para que una Estación de Servicio instale un surtidor eléctrico para el transporte pesado rondan los USD 250.000-300.000**, sin tener en cuenta la potencia contratada que se debe pedir a la empresa de energía para cargar el 100 por ciento de la batería. Y si se agrega que **no está aprobada la ley de promoción a la movilidad sustentable**, que **tampoco hay un fomento para los vehículos livianos** y que se desconoce si el mercado avanzará hacia la electricidad o el gas natural, **las bocas de expendio se mantienen en vilo para saber hacia dónde virar.**

_“Algunas Estaciones de Servicio observan el rumbo del negocio, pero como todavía no está definido, aún analizan si invierten en ambas tecnologías o en una sola, al menos por ahora_”, sostuvo **Guillermo Torres**, comercial marketing manager de Alter Energy, en conversación con **surtidores.com.ar.**

“_El debate también se da porque las exigencias para los surtidores de GNC son mucho más altas ya que se deben cumplir ciertas normativas para instalarlos en lugares donde también se despacha combustible. Y muchas Estaciones de Servicio tienen poca superficie disponible; pero por otro lado, no se sabe si se podrá vender energía eléctrica_”, complementó.

Ante ello, _surge el interrogante de qué tecnología puede ser más factible y qué se sugiere para el futuro de las expendedoras del país_, considerando que a nivel mundial el transporte cada vez resulta más sustentable, pero que en Argentina las variables son más complejas para que la electromovilidad explote en el corto o mediano plazo.

_“Personalmente sugiero ir por ambas tecnologías, es decir, un surtidor de gas natural a corto plazo y poner un cargador eléctrico de 22 kW, como hicieron algunas otras estaciones, ya que la transición e integración energética son puntos clave para tener una matriz lo más diversificada posible_”, respondió Torres, dejando lugar a que **las Estaciones de Servicio opten por la alternativa que mejor consideren y que poco a poco se preparen para las nuevas tendencias de la movilidad.**

_“Aunque se debe aclarar que ya hay pliegos para cargadores eléctricos y así se visibiliza la temática, siendo el primer paso para avanzar en la incorporación de la electromovilidad y la creación de puntos de carga. Y esperemos que cuando se apruebe la ley de promoción a la movilidad sustentable, se le pueda dar un marco jurídico para que las empresas inviertan fuertemente_”, concluyó.