---
category: Agenda Ciudadana
date: 2021-05-21T08:34:08-03:00
thumbnail: https://assets.3dnoticias.com.ar/pymes-dosjpg.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa CAME
resumen: Apoyamos el proyecto de Ley del Diputado Menna que busca alivio fiscal para
  las pymes
title: Apoyamos el proyecto de Ley del Diputado Menna que busca alivio fiscal para
  las pymes
entradilla: Como representantes del sector pyme, consideramos que esta propuesta constituye
  un pilar fundamental en el sostenimiento del empleo.

---
Desde la Confederación Argentina de la Mediana Empresa (CAME) enviamos una misiva al diputado nacional Gustavo Menna a fin de expresarle nuestro apoyo y acompañamiento al proyecto de ley de su autoría, Expediente 1988-D-2021, que propicia reconocer un crédito fiscal a favor de los empleadores a cuenta de IVA, Ganancias y contribuciones de la seguridad social, por el monto de los salarios que se pagan sin contraprestación laboral a los empleados dispensados de concurrir a sus lugares de trabajo por ser grupos de riesgo.

Como representantes del sector pyme, consideramos que esta propuesta constituye un pilar fundamental en el sostenimiento del empleo y la actividad en los distintos sectores de nuestra economía, y que contribuirá a un mejor cumplimiento de las obligaciones, evitando moras y adeudamientos que se han provocado producto de la actual coyuntura.

Para acceder a la carta enviada, haga [clic aquí](https://www.redcame.org.ar/advf/documentos/2021/05/60a3df9331e53.%20gustavo%20menna.pdf).