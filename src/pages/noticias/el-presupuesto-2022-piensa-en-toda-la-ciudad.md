---
category: La Ciudad
date: 2021-10-18T06:00:24-03:00
thumbnail: https://assets.3dnoticias.com.ar/PRESUPUESTO.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: El Presupuesto 2022 piensa en toda la ciudad
title: El Presupuesto 2022 piensa en toda la ciudad
entradilla: El proyecto del intendente Emilio Jatón consolida el Plan Integrar que
  se puso en marcha en 2020, con un amplio paquete de obras estratégicas en los distintos
  barrios.

---
La Municipalidad de Santa Fe envió al Concejo el Presupuesto para el próximo período por un monto total de $ 23.009.562.654, de los cuales 4.253.088.235 pesos serán girados por Nación y provincia para obras gestionadas por el intendente Jatón. Por su parte, con recursos propios se completará un ambicioso plan de infraestructura en toda la ciudad por $ 5.976.065.544.

Solo para el plan de bacheo e iluminación se destinarán más de $1.400 millones con el objetivo de que la ciudad empiece a tener otro estándar de calidad urbana al que históricamente tuvo. Por caso, 835 millones de pesos en iluminación led, 471 millones de pesos en bacheo de avenidas y calles de alto tránsito, 350 cuadras de ripio para consolidar la trama urbana en barrios populares y la puesta en valor de espacios públicos y plazas por 100 millones de pesos.

“Esta decisión es el resultado de las decenas de conversaciones que mantuvimos con los y las vecinas de la ciudad, ya que escuchamos las demandas y proponemos soluciones integrales. Además, tenemos un horizonte claro que es el proyecto de ciudad que estamos llevando adelante junto con Emilio Jatón”, indicó el secretario General del municipio, Mariano Granato.

“El próximo año habrá un shock de recursos para la gestión urbana, que servirá para mejorar la transitabilidad y los espacios públicos de Santa Fe. No habrá barrio sin intervención de la Municipalidad y esto es producto de que desarrollamos ideas, generamos los proyectos ejecutivos y salimos a conseguir financiamiento para obras que le cambiarán la vida a nuestros vecinos y vecinas”, explicó Granato.

El funcionario remarcó que “tocó transitar una época difícil para las gestiones locales, como consecuencia de una pandemia que acentuó los desequilibrios que tiene la ciudad”. Por tal motivo, valoró el esfuerzo del intendente para ratificar con recursos el rumbo trazado: ampliar el Plan Integrar para llegar con más y mejores obras y servicios a todos los barrios.

Este programa prevé obras por 3.800 millones de pesos. Es una decisión política sin precedentes la de incrementar la infraestructura básica y también de calidad en distritos que históricamente tuvieron menos recursos por parte del Estado municipal. Todo ello sin descuidar avenidas troncales, parques y plazas de la ciudad, que son los espacios comunes y de mayor concentración de vecinos.

En ese sentido, al sumar todas las obras que ejecutará la Municipalidad de Santa Fe, el próximo año la inversión total alcanzará los 5.976 millones de pesos. Para poner en contexto el número, la actual gestión aumentó los gastos de capital (26% en inversión en obras, maquinarias y vehículos contra 21% en 2021) y redujo la participación de los gastos corrientes (del 71% al 68%).

“Somos una gestión que no se endeuda para pagar sueldos ni sus compromisos con proveedores, ni siquiera aquellos que dejó la gestión pasada y que aún hoy estamos pagando. Esto marca, a las claras, la responsabilidad presupuestaria que hemos tenido en estos años de mucha incertidumbre económica para manejar con prolijidad los recursos de los y las santafesinas”, explicó el secretario General.

Asimismo, Granato reiteró que “esta Municipalidad se caracteriza por controlar el gasto sin ajustar el rubro personal, ni hacer que el costo de la crisis recaiga sobre tener menos servicios para los vecinos. Es un esfuerzo muy grande el de los santafesinos para cumplir con sus compromisos y el intendente Jatón no hace más que replicar en el Estado lo que día a día hacen los ciudadanos”, manifestó.

“Vinimos a integrar la ciudad y no hay vuelta atrás”, indicó el secretario General y explicó que el 25,9% del total del gasto de la Municipalidad de Santa Fe se utilizará para gestión urbana y obras públicas. En definitiva, el proyecto de Presupuesto 2022 no solo contempla un pool de obras significativas sino también los recursos específicos para los distintos servicios municipales.

Uno de ellos tiene que ver con las políticas sociales de calidad en la ciudad: 17 Estaciones barriales con propuestas sociales, lúdicas, deportivas, de cuidado y de acompañamiento pedagógico. La recuperación edilicia del Teatro Municipal y la descentralización de las propuestas culturales a través del programa Barrio Abierto y las políticas ambientales (ecocanje, recolección de pilas y puntos verde de reciclado en espacios públicos, entre otros).

A su vez, valoró la importancia de sostener los programas de castración y control sanitario de animales de compañía, las ferias populares y las propuestas de jardines municipales y el Liceo, que contarán con los recursos suficientes para su funcionamiento

**Inversión real**

Entre las obras de infraestructura que se prevén hacer con financiamiento nacional están la finalización de la primera etapa de Camino Viejo, un compromiso asumido por el intendente y que continuará con otros tramos hasta 2023, la pavimentación y desagües pluviales de Javier de la Rosa en barrio Transporte, la remodelación integral de la Plaza del Soldado y de la Plaza de las Banderas.

También, la restauración de Plaza España y de las Tres Culturas, obras integrales en el Botánico, nuevo paseo de J.J. Paso y pavimentación y desagües pluviales para calle Beruti, Pedro de Vega y 1º de Mayo (barrio Transporte). A su vez, con recursos propios se revitalizará toda avenida Galicia a partir de la generación de un nuevo paseo para que disfruten los y las santafesinas.

También es importante destacar que habrá obras integrales de mejoramiento urbano en los barrios Yapeyú, Loyola Norte, San Agustín y La Ranita ($347.710.493); Varadero Sarsotti ($165.674.829); Las Lomas ($20.327.686); Los Hornos ($446.880.230); 29 de Abril ($458.606.194); Transporte ($287.090.554); Barranquitas Oeste ($13.715986). Además, cloacas para los barrios Atilio Rosso y Estada ($192.502.364).

El presupuesto habilita a tomar deuda por hasta 400 millones de pesos para infraestructura vial, nueva iluminación y equipamiento de maquinarias, herramientas y otros bienes que permiten arreglar y mantener la infraestructura de la ciudad. Con ese monto, más lo que ya está previsto con financiamiento propio, el 28% de las obras lo pagará el municipio, el 57% Nación y el otro 15% provincia.

“Si la situación sanitaria continúa como se prevé, en 2022 vamos a tener la oportunidad de gestionar la ciudad por fuera de la crisis que generó la pandemia y de los problemas económicos que recibimos. Tenemos la expectativa y mucho entusiasmo porque será la primera vez que ya no estaremos tan condicionados y podremos seguir haciendo una ciudad mejor”, concluyó el secretario General.