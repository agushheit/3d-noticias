---
category: Agenda Ciudadana
date: 2021-09-25T06:00:03-03:00
thumbnail: https://assets.3dnoticias.com.ar/JUBILACION.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Confirman un proyecto de jubilación para hombres de más de 60 años y mujeres
  de más de 55
title: Confirman un proyecto de jubilación para hombres de más de 60 años y mujeres
  de más de 55
entradilla: Se trata de una política dirigida a personas que se encuentren desempleadas,
  reúnan 30 años de aportes y les falten cinco años o menos para cumplir la edad jubilatoria.

---
La directora ejecutiva de la Administración Nacional de la Seguridad Social (Anses), Fernanda Raverta, confirmó este viernes que el Gobierno está trabajando en la elaboración de un decreto de necesidad y urgencia (DNU) para implementar una jubilación anticipada.  
  
Se trata de una política dirigida a personas que se encuentren desempleadas, reúnan 30 años de aportes y les falten cinco años o menos para cumplir la edad jubilatoria: entre 55 y 60 años para las mujeres y entre 60 y 65 para los varones.  
  
Según estimaciones de la Anses, la medida alcanzará en una primera instancia a una población aproximada de entre 20.000 y 30.000 personas, de las cuales el 90% son hombres.

“Estamos construyendo una política que da respuesta a un problema que tenemos las argentinas y argentinos”, destacó Raverta.  
En declaraciones a la TV Pública, señaló que “hay personas que trabajaron toda su vida, que tienen sus 30 años de aportes y que, en los últimos años del gobierno anterior o incluso en la pandemia, perdieron su trabajo, no se pueden jubilar todavía y por su edad tienen una enorme dificultad para reincorporarse al mercado formal de trabajo”.  
  
Así, la funcionaria destacó que se están “terminando de estudiar los detalles, para que sea una medida seria y para que podamos ofrecer, una vez más, una solución a un problema que tenemos las argentinas y argentinos”.  
  
En ese sentido, sostuvo que la iniciativa es “una respuesta a un problema que tenemos los argentinos y las argentinas, que tiene que ver con personas que han trabajado toda su vida y tienen sus 30 años de aporte y han quedado desempleadas en los últimos años del Gobierno anterior o incluso en la pandemia”.  
  
“Son personas, en su mayoría varones, que cumplen con los requisitos de aportes y de haber trabajado y que aún no tienen la edad para jubilarse pero, al mismo tiempo, presentan una enorme dificultad de incorporarse en el mercado formal de trabajo porque son grandes” en edad, precisó la funcionaria.  
  
"Sobre la posibilidad de que la medida se formalice prontamente, Raverta indicó que “será una certeza cuando el Presidente firme el decreto de necesidad y urgencia"

  
Sobre la posibilidad de que la medida se formalice prontamente, Raverta indicó que “será una certeza cuando el presidente (Alberto Fernández) firme el decreto de necesidad y urgencia (DNU) y podamos empezar a recibir a estas personas en nuestras oficinas”.  
  
“El presidente lo está terminando de evaluar”, agregó la titular de la Anses.  
  
Según explicó la funcionaria, el proyecto contempla que los beneficiarios perciban el equivalente al 80% de sus haberes jubilatorios hasta alcanzar la edad jubilatoria (65 años hombres, 60 años las mujeres).  
  
Por ejemplo, un hombre de 62 años cobrará desde ese momento -y hasta que cumpla 65 años- el 80% del haber jubilatorio; luego, una vez alcanzada la edad de 65 años que establece la ley para poder jubilarse, pasará a cobrar el 100% del haber que le corresponda.  
  
En línea con el espíritu de esta iniciativa, en julio pasado el Gobierno nacional estableció a través del Decreto 475/21 publicado en el Boletín Oficial que las mujeres podrán sumar de uno a tres años de servicios computables para su jubilación por cada hijo.  
  
La medida rápidamente presentó una fuerte adhesión y, semanas atrás, ya contaba con más de 21.000 mujeres inscriptas.  
"Esta política pública reivindica lo que las madres hacemos con absoluta naturalidad pero le otorga un reconocimiento del Estado. Es conmovedor porque gestamos una nueva Argentina donde las mujeres y la actividad que realizamos en nuestros hogares es reconocida”, sostuvo Raverta en aquel entonces.  
  
De acuerdo con estadísticas de la Administración Nacional de la Seguridad Social (Anses), en marzo de este año Argentina contaba con 5.159.630 jubilados y 1.666.655 pensionados, lo que arroja un total de 6.826.285 de beneficiarios del Sistema Integrado Previsional Argentino (SIPA), establecido por la Ley Nº 26.425 y administrado por la Anses.