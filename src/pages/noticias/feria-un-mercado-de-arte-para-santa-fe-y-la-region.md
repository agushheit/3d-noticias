---
category: Cultura
date: 2021-11-26T06:00:55-03:00
thumbnail: https://assets.3dnoticias.com.ar/belgrano.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: "+Feria: un mercado de arte para Santa Fe y la región"
title: "+Feria: un mercado de arte para Santa Fe y la región"
entradilla: " El encuentro organizado por la Municipalidad reunirá a artistas, curadores,
  galerías, coleccionistas de diversas ciudades de la región y público en general.
  Será el sábado 27 y el domingo 28 de noviembre."

---
Para visibilizar la tarea de las galerías de arte locales y fomentar un circuito para la venta de producciones artísticas, la Municipalidad organiza +Feria Arte Contemporáneo Santa Fe. Será un encuentro de dos días para reunir al sector local y potenciar los vínculos entre artistas, curadores, galerías y coleccionistas de diversas ciudades de la región. Participarán 13 galerías de Santa Fe, Rosario, Paraná y Unquillo, y editoriales especializadas. Además habrá presentaciones, conversatorios y una subasta de obras.

El sábado y el domingo, de 17 a 22.30 horas, en la planta alta de la Estación Belgrano, se podrán recorrer con entrada libre y gratuita, los stands de las galerías Fuga, Delta Espacio, AG Arte, TODA, Zurbarán y Púrpura, de Santa Fe; Diego Obligado Galería de Arte, Estudio G, Gabelich Contemporáneo y Crudo, de Rosario; ESAA, de Unquillo, Córdoba; La Taller y La Portland, de Paraná. Muchas de ellas participan habitualmente en encuentros como ArteBA, la Feria de Arte de Córdoba o la MicroFeria de Arte Rosario, pero no tenían hasta ahora la posibilidad de encontrarse en Santa Fe, en un evento común que ponga en valor su tarea y la acerque a nuevos públicos.

**Para agendar**

A partir de esta primera edición, que continuará en 2022, la Municipalidad convoca a coleccionistas, instituciones y empresas invitadas para promover la venta de obras de arte como un mercado más dentro de la ciudad, abierto al público en general. Durante la feria se podrán adquirir las obras, de acuerdo con los medios de pago de cada espacio, y el domingo a las 21 horas se realizará una subasta en la que cada galería pondrá una obra a la venta.

Habrá una mesa de las editoriales Fronda, el proyecto de fotografía contemporánea y libros especializados que coordina Aimé Luna; Azogue Libros, de Paraná; e Iván Rosado, de Rosario, que el sábado a las 20 horas presentará el libro “Transparencia y misterio de las lacas” que editó este año con reproducciones a color y textos de Beatriz Vallejos sobre su práctica artística.

El domingo a las 19 horas, se realizará el conversatorio “Escenas locales”, con la participación de las galerías que exponen en +Feria para compartir sus experiencias, estrategias y retratar el circuito que crece por fuera de Buenos Aires. Como galerías invitadas a esta instancia participarán Luogo, de Rafaela; Subsuelo, de Rosario; y Piedras, de Buenos Aires.

La activista, artista visual y de performance Natacha Voliakovsky, invitada por Galería ESAA, realizará una obra performática que se registrará en video y se proyectará durante la feria. El domingo a las 18 participará de la charla “En la emergencia la salida es colectiva”.

**Galerías y Museos Abiertos**

Además de las actividades que tendrán lugar en la Estación Belgrano, habrá un Circuito de Galerías y Museos Abiertos, el sábado y domingo desde las 10 horas, para que las personas invitadas a +Feria y el público en general puedan recorrer esos espacios: Delta Espacio (Necochea 3799), AG Arte Galería (Boulevard Gálvez 1514), TODA (Mercado Norte, Santiago del Estero 3166), Museo de la Constitución Nacional (Avda Circunvalación y 1º de Mayo), Museo Provincial de Bellas Artes “Rosa Galisteo de Rodríguez” (4 de Enero 1510) y el Museo Municipal de Artes Visuales “Sor Josefa Díaz y Clucellas” (San Martín 2068). El domingo se sumará el Museo de Arte Contemporáneo de la Universidad Nacional del Litoral (Boulevar Gálvez 1578).