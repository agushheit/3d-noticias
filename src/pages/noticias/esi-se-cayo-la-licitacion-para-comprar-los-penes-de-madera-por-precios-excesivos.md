---
category: Agenda Ciudadana
date: 2021-08-07T06:00:56-03:00
thumbnail: https://assets.3dnoticias.com.ar/PENES.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'ESI: se cayó la licitación para comprar los penes de madera por "precios
  excesivos"'
title: 'ESI: se cayó la licitación para comprar los penes de madera por "precios excesivos"'
entradilla: En total, el gobierno nacional había recibido 4 ofertas que, entre otros
  puntos, no presentaban garantía de la oferta en el plazo estipulado, ofrecían precios
  hasta 700% más caros y/o tenían deudas con la AFIP.

---
La compra de 10 mil penes de madera, entre otros elementos destinados a la educación sexual, se cayó, según figura en el proceso de compra en la página oficial. La adquisición de esos materiales había generado controversia debido a que la autorización tenía un monto estimado de 13 millones de pesos.

Además de penes de madera, el Estado planeaba comprar dispensers de preservativos y maletines para ser distribuidos en los Centros de Atención Primaria de la Salud (CAPS), regiones sanitarias, programas provinciales, municipales, es decir, en establecimientos públicos, principalmente vinculados a la salud, de todo el país, que serían destinados “a la población en general y al personal de salud”.

Pero este viernes se conoció el fracaso de esa negociación. En total, el gobierno nacional había recibido 4 ofertas que, entre otros puntos, no presentaban garantía de la oferta en el plazo estipulado, ofrecían precios hasta 700% más caros y/o tenían deudas con la AFIP. Por ejemplo, Marketing Dimension S.A ofreció penes de madera, a $ 1430 por unidad. La oferta fue rechazada “debido a que su precio unitario cotizado excede al precio unitario referencial estimado por la unidad requirente en un 717,07%.”.

Melenzane S.A también sólo ofreció penes de madera a $ 882 por unidad, pero solo por 5000 penes de madera, por un total de $ 4.411.850. Pero tampoco presentó “el certificado pertinente de la garantía individualizada en la oferta físicamente dentro del plazo establecido”.