---
category: Agenda Ciudadana
date: 2021-07-31T06:00:39-03:00
thumbnail: https://assets.3dnoticias.com.ar/PARITARIAS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Convocan a la paritaria nacional docente para el mes de agosto
title: Convocan a la paritaria nacional docente para el mes de agosto
entradilla: El gobierno convocó a los gremios para discutir aumentos salariales, que
  adelantaron que reclamarán una suba del 45 por ciento. También los universitarios
  recibieron el llamado a la negociación.

---
El gobierno convocó a los gremios docentes a una reunión el próximo 9 de agosto en el marco de la paritaria nacional del sector, en tanto el 12 se abrirá también la negociación con docentes y no docentes de universidades nacionales.

El propio ministro de Educación, Nicolás Trotta, confirmó que, "a partir del diálogo con todos los sindicatos, el Gobierno nacional tomó la decisión de convocar a la paritaria nacional docente el lunes 9 de agosto y esa misma semana, el 12, habrá también reunión paritaria con los docentes y no docentes universitarios".

Trotta explicó que dialogó sobre esta convocatoria a paritaria con representantes de los distintos sindicatos docentes, entre ellos la Confederación de Trabajadores de la República Argentina (Ctera), la Unión de Docentes Argentinos (UDA) y la Confederación de Educadores Argentinos (CEA).

"Estamos hablando con todos los sindicatos para convocarlos el día 9 y también para debatir las leyes que estamos proponiendo, lo que incluye una ley de justicia educativa, que es la de financiamiento educativo", dijo Trotta.

Por su parte, la secretaria general de Ctera, Sonia Alesso, dijo que los docentes irán a las paritarias con un reclamo de incremento salarial del 45%. "Nosotros a principio de año habíamos sacado el 35 por ciento y ahora estamos planteado discutir el 45 por ciento como mínimo, y evaluar la situación con un monitoreo permanente en función de cómo está la inflación y de lo desfasados que quedaron los sueldos", señaló Alesso.

Además, Alesso planteó que también hay que abordar "el desfase que hubo en la jubilación de los docentes que cobran de cajas del Estado nacional, que son 11 provincias, que han quedado muy atrasados con el porcentaje de inflación".

**Proyectos para financiamiento**

La ley de Financiamiento que propone el gobierno nacional apunta a incrementar el presupuesto en Educación, pasando del actual 6% del PBI a 8% del PBI, lo que implica 6 puntos para la educación obligatoria y 2 puntos para la superior, que incluye universidades, terciarios e institutos docentes.

El ministro Trotta explicó también que se trabaja en otras leyes, que fueron presentadas en la última reunión del Consejo Económico y Social, sobre tecnología para la educación, que implica institucionalizar el programa Juana Manso, ampliar la distribución de computadoras y conectar a todas las escuelas a Internet. También, impulsa una ley de formación docente y otra sobre Educación Superior que "el gobierno busca discutir con todas las universidades nacionales".

En tanto, la secretaria general de Ctera aseguró que Trotta adelantó que discutirán sobre proyectos de ley para vincular la formación docente con las metas educativas, la construcción de escuelas y la conectividad.

"El ministro se comprometió a convocarnos el 9 de agosto, y también le planteamos la preocupación por la infraestructura escolar a partir de lo ocurrido en Neuquén, donde falleció una maestra y dos trabajadores y por lo cual ayer hicimos una jornada de protesta", indicó Alesso.