---
category: Estado Real
date: 2021-08-14T06:00:02-03:00
thumbnail: https://assets.3dnoticias.com.ar/GASODUCTO.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Provincia y nación suscribieron convenios para reactivar y culminar obras
  de gas por más de $ 3.700 millones
title: Provincia y nación suscribieron convenios para reactivar y culminar obras de
  gas por más de $ 3.700 millones
entradilla: "“Estamos construyendo futuro y corrigiendo el pasado”, dijo el gobernador
  Omar Perotti sobre la reactivación del gasoducto Regional Centro y las ampliaciones
  del Regional Sur, el Metropolitano y la red en Fighiera."

---
El gobernador Omar Perotti y el secretario de Energía de la Nación, Darío Martínez, encabezaron este viernes la firma de un convenio entre la provincia y el gobierno central, para reactivar y culminar obras de gas por más de 3.700 millones de pesos, que incluyen la reactivación del gasoducto Regional Centro y las ampliaciones del Regional Sur, Metropolitano y la red en la localidad de Fighiera.

“El objetivo es llegar a la mayor cantidad de lugares porque el gas es una de las alternativas claras para el desarrollo y la calidad de vida”, dijo Perotti, y aseguró que “estamos construyendo el futuro y corrigiendo el pasado”.

“Cuando las obras se interrumpen o se abandonan, volver a encausarlas requiere de mucha voluntad y fuerte decisión política”, explicó el gobernador, al tiempo que sumó: “Tenemos una clara convicción de trabajo la Nación y la provincia juntos, es una instancia virtuosa que viene dando resultados día a día”.

En otro tramo de su discurso, destacó la labor de los senadores nacionales María de los Ángeles Sacnun y Roberto Mirabella, y de los legisladores provinciales Marcelo Lewandowski y Marcos Castelló. “Ha sido fundamental para nosotros la tarea de ustedes en ayudar; para gobernar necesitamos eso, que nos ayuden a transitar los pasillos de los ministerios, de las distintas áreas, que le den continuidad al día a día de estas gestiones que a veces se ponen engorrosas”, les dijo.

En tanto, indicó que, para la provincia, poder aumentar este servicio “es un paso importantísimo porque necesitamos seguir expandiendo nuestra red de gas, y seguir acercando una fuente de energía que mejora los costos, tanto en lo domiciliario como en lo industrial”.

Y sumó: “Nuestro profundo deseo de poner en marcha estas obras lo antes posible de tener la participación de empresas que le pongan buen ritmo a la construcción y que nos sumen jóvenes en ese plantel de trabajadores que movilizará una obra tan importante como las que estamos haciendo en cada una de las regiones”.

Finalmente, destacó que las suscripciones de los convenios son “otra señal clara y contundente de cómo combinando nuestros esfuerzos, podemos generar acciones positivas. Lo decimos siempre: lo que Santa Fe recibe lo potencia y lo devuelve multiplicado, y no me cabe dudas que, con este financiamiento importantísimo de la Nación, nuestras familias lo devolverán con creces en su calidad de vida y satisfacción, y nuestras empresas multiplicando la producción y el empleo, para el bien de la provincia y de la Argentina”.

**TRABAJO CONJUNTO**

Por su parte, Martínez, señaló que se trató de “un arduo trabajo recomponer y hacer lo que corresponde para avanzar en estas obras que aportan calidad de vida”, y agregó que “con mucha alegría podemos sentarnos con un gobernador que siempre está gestionando y resolviendo cuestiones que tienen que ver la vida de santafesinos y santafesinas”.

En tanto, Sacnun recordó que “junto con Roberto Mirabella venimos trabajando, y gestionando permanentemente aquellas cosas que son importantes para nuestra provincia, siguiendo también muchas cuestiones que nos encomienda el gobernador”. Y se refirió a la importancia de reactivar el Gasoducto Sur, “que se vio interrumpido y que gracias a la decisión del gobierno nacional y provincial se volvió a retomar. Lo que se retoma es un modelo de nación y un modelo de provincia que van de la mano”.

**GASODUCTO A REACTIVAR Y CULMINAR**

Las tareas a realizar incluyen la ampliación red de gas natural en la localidad, de Fighiera, que comprende la ejecución de cañerías para la ampliación y refuerzo de la red de media presión de la zona urbana, generando entre sus beneficios la inclusión de 500 nuevos servicios domiciliarios.

Por otra parte, la ampliación del Gasoducto Regional Sur, que permitirá desarrollar el sistema gasífero actual en la localidad de Venado Tuerto y las comunas de Carmen, Murphy, Chovet, Firmat, Casilda, Melincué y Teodelina, con una inversión de 811 millones de pesos.

Otro es el Gasoducto Metropolitano, que permitirá la provisión de gas por redes a un importante sector de las ciudades de Santa Fe, Recreo y San José del Rincón y a las localidades de Monte Vera, Arroyo Aguiar y Arroyo Leyes, todas ellas del Departamento La Capital, ampliando de esta manera la cobertura del servicio de gas por redes, con una inversión por más de 2.300 millones de pesos.

Además, se anunció la reactivación del Gasoducto Regional Centro II que beneficiará a toda la cuenca lechera, desde Esperanza hasta Sunchales, con un impacto mayor a 34.000 usuarios, con inversión de más de 650 millones de pesos.

El encuentro, desarrollado de manera virtual, también contó con la presencia de la ministra de Infraestructura, Servicios públicos y Hábitat, Silvina Frana; el secretario de Empresas y Servicios Públicos, Carlos Maina; el presidente de Enerfe, Juan D'Angelosante; la secretaria de Gestión Federal, Candelaria González del Pino; la subsecretaria de Hidrocarburos de la Nación, Maggie Videla; y el presidente de IEASA, Agustín Jerez, entre otros.