---
category: Agenda Ciudadana
date: 2021-01-23T09:24:57Z
thumbnail: https://assets.3dnoticias.com.ar/clases.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: "‘La vuelta a las escuelas en la modalidad presencial es imprescindible’ "
title: "‘La vuelta a las escuelas en la modalidad presencial es imprescindible’ "
entradilla: La Sociedad de Pediatría elaboró un “Documento conjunto de posicionamiento
  para la vuelta a las escuelas”.

---
Ante la cercanía de un nuevo ciclo lectivo y la continuidad de la pandemia por coronavirus, la Sociedad Argentina de Pediatría (SAP) difundió un informe donde alerta sobre la situación de los niños y adolescentes en edad escolar.

‘El cierre de las escuelas en el contexto de la pandemia debe reconocerse como un factor que marcó claramente disparidades sociales. Muchos adolescentes son incapaces de completar su tarea escolar por falta de dispositivos o de conectividad, lo cual hace que los modelos de aprendizaje virtual resulten de difícil implementación, dando lugar a la aparición de una brecha tecnológica entre niños, niñas y adolescentes muy difícil de resolver. La vuelta a las escuelas en la modalidad presencial es imprescindible’. Así lo afirmaron desde la Sociedad Argentina de Pediatría (SAP), aclarando que cada establecimiento deberá adaptar su nivel de reapertura de acuerdo a la realidad epidemiológica local.

“Es indiscutible que la escuela es fundamental para el desarrollo y el bienestar de las niños, niñas y adolescentes (NNyA), no sólo para la adquisición de conocimientos sino también para el fortalecimiento de aspectos emocionales y sociales, el cuidado de aspectos nutricionales, de la salud y la realización de la actividad física”, puede leerse en el texto donde también se resalta el rol de la escuela como “un sitio seguro para los NNyA mientras sus padres trabajan, contribuyendo así a la actividad económica de la sociedad y es claramente una herramienta de equidad social indispensable particularmente para los grupos sociales más vulnerables”.

El texto, con consejos y pautas que deberían ser tenidas en cuenta para el beneficio de los más pequeños, fue elaborado de forma conjunta entre los distintos comités que conforman la Sociedad, entre los cuales se abordan temas como Familia y Salud Mental, Infectología, Medicina del Deporte, Pediatría Ambulatoria, Derechos del Niño, Epidemiología, Discapacidad y Salud Escolar.

Como conclusión, el documento indica que la Sociedad Argentina de Pediatría (SAP) “cree que la vuelta a las escuelas en la modalidad presencial es imprescindible. Por supuesto que la observación de las diferentes realidades epidemiológicas existentes en nuestro país y el cumplimiento estricto de los protocolos sanitarios son aspectos insoslayables a tener en cuenta para el regreso a las aulas”.

“Creemos que en el proyecto de regreso a las aulas es fundamental cuidar la salud de los NNyA con las medidas adecuadas y destacamos que los integrantes del plantel docente y no docente de los colegios deben ser considerados trabajadores esenciales, como los trabajadores de la salud, de las fuerzas de seguridad y del transporte público para hacerlo realmente viable”, indicaron.

**El texto destaca el trabajo realizado en Dinamarca.**

Cabe recordar que desde el Gobierno nacional se confirmó que habrá un sistema dual para permitir la presencialidad en las aulas. Lo mismo sucede a nivel provincial y en la ciudad de Buenos Aires. Hasta el momento, quienes se niegan a regresar a las escuelas sin tener las vacunas correspondientes y protocolos estrictos son algunos gremios docentes, por lo que el inicio del ciclo lectivo, que debería comenzar el próximo 17 de febrero, todavía no está definido.

Lee el informe completo ingresando al [enlace](https://www.scribd.com/document/491770249/Pedido-de-Vuelta-a-Clases)