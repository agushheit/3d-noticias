---
category: Agenda Ciudadana
date: 2021-08-27T06:15:04-03:00
thumbnail: https://assets.3dnoticias.com.ar/CANNABIS.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La provincia y el Inta firmaron un convenio para la investigación del uso
  medicinal del cannabis
title: La provincia y el Inta firmaron un convenio para la investigación del uso medicinal
  del cannabis
entradilla: El programa tiene por objetivo desarrollar evidencia científica sobre
  diferentes alternativas terapéuticas a problemas de salud de la población.

---
La provincia de Santa Fe, a través de los Ministerios de Salud, y de Producción, Ciencia y Tecnología, firmó un convenio con el Instituto Nacional de Tecnología Agropecuaria (INTA) para la investigación del uso medicinal del Cannabis.

El mismo tiene por objetivo el desarrollo de evidencia científica sobre diferentes problemas de salud, que no se abordan los tratamientos médicos convencionales y para los cuales el Cannabis y sus derivados cada vez son más prescriptos por los médicos.

Además, busca precisar con rigor los fines terapéuticos de la planta de Cannabis, como así también de sus derivados.

Asimismo, se busca evaluar la eficacia de los mismos para cada indicación terapéutica, que permita el uso adecuado y la universalización del acceso al tratamiento; conocer efectos secundarios potenciales; establecer la seguridad y las limitaciones para su uso; y promover el cuidado y la mejora de la calidad de vida de la población.

Al respecto, el ministro de Producción, Ciencia y Tecnología de la provincia, Daniel Costamagna, expresó que la firma de este acuerdo “tiene una fuerte impronta interdisciplinaria entre el INTA, el LIF y los ministerios de Salud y Ciencia para el diseño, desarrollo y la implementación de acciones conjuntas en la investigación y producción de Cannabis medicinal”.

Asimismo, “que el plan de trabajo permitirá la capacitación a través de profesionales del INTA. Además, articular grupos de investigación a los fines de generar ensayos experimentales que apunten a una trazabilidad integral, vertical y transversal, del cultivo con fines terapéuticos”.

“Este es un primer paso para que la provincia de Santa Fe genere conocimientos técnicos para lograr un cultivo de calidad medicinal”, enfatizó finalmente Costamagna.

**Todo el proceso para garantizar el acceso**

Por su parte, la ministra de Salud, Sonia Martorano, destacó que mediante el trabajo interministerial e interinstitucional se realizará “una rigurosa investigación orientada al desarrollo del Cannabis con fines terapéuticos que irá desde la selección de las semillas más adecuadas, pasando por la selección de las tecnologías y del proceso en su totalidad. Es decir que la provincia seleccionará qué semillas comprar, las que serán sembradas en terrenos del INTA y estarán bajo su cuidado”.

“Posteriormente –destacó Martorano– el Ministerio de Salud, a través del LIF, no solo intervendrá en el fraccionamiento del fármaco, que es algo que ya viene realizando, sino en la producción de los compuestos farmacológicos derivados del Cannabis; algo muy importante para las personas que tienen algún problema de salud, ya que se ampliará la accesibilidad y disponibilidad en el ámbito público”.

Finalmente, el director del Centro Regional del INTA Santa Fe, Alejandro Longo, expresó su entusiasmo y consideró que “la firma del convenio es un primer y gran paso para el cultivo de esta planta con fines medicinales, que permitirá formalizar y aplicar las normas vigentes al respecto de una manera articulada y segura, entre distintos estamentos del Estado, como se realiza ya en otras provincias del país”.