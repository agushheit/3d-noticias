---
layout: Noticia con imagen
author: .
resumen: "Agricultura "
category: El Campo
title: Agricultura comienza la implementación de las compensaciones económicas
  para pequeños y medianos productores de soja
entradilla: Con la medida se pone en marcha el sistema de segmentación y
  redistribución de beneficios. Para ello se destinarán 11550 millones de pesos.
date: 2020-11-05T12:58:21.812Z
thumbnail: https://assets.3dnoticias.com.ar/agricultura.jpg
---
El Ministerio de Agricultura, Ganadería y Pesca de la Nación puso en marcha hoy el Programa de Compensación y Estímulo para los productores de soja de pequeña escala, dando cumplimiento a la premisa del Gobierno Nacional de beneficiar a los pequeños y medianos productores de todo el país.

De esta manera, el Ministerio comienza la implementación del sistema segmentación y redistribución de beneficios que destinará la suma de 11.550 millones de pesos para el sector, atendiendo las demandas de cada zona del país con un sentido de equidad territorial.

Sobre el Programa, el ministro Luis Basterra destacó: "Estamos cumpliendo con la palabra empeñada por el presidente Alberto Fernández generando un modelo de compensación para los pequeños y medianos productores”.

“Hemos diseñado un mecanismo que nos permite avanzar en un proceso de redistribución en aquellos sistemas productivos de menor escala, agregó el Ministro y enfatizó: "un 40% del total de productores va a recibir una compensación en un modelo de equidad territorial social".

En este sentido, subrayó: “Agradecemos la inestimable colaboración del Ministerio de Economía de la Nación, en las personas del ministro Martín Guzmán y su equipo, para que este esquema tenga un modelo de aplicación transparente, equitativo y certificado”.

La medida beneficia a todas aquellas personas físicas y jurídicas que hayan declarado una superficie sembrada de hasta 400 hectáreas destinadas al cultivo y que hayan facturado durante 2019 un máximo de 20 millones de pesos. Con dicho fin, se decidió extender hasta el 30 de septiembre de este año como fecha límite de declaración para habilitar mayor volumen de ventas a los productores que van a recibir las compensaciones, en este primer tramo de la instrumentación del esquema.

En concordancia con lo dispuesto, podrán ser beneficiarios y beneficiarias de la compensación aquellos productores que hayan registrado ventas en el Sistema de Información Simplificado Agrícola (SISA), según los montos, superficies y fechas establecidas.

Para la implementación del Programa, el Ministerio diseñó un sistema escalonado por cantidad de hectáreas destinado a que las compensaciones cumplan con el objetivo redistributivo y federal propuesto en el espíritu de la Ley de Solidaridad Social y Reactivación Productiva. En esta dirección, y en respuesta a la solicitud del sector, la cartera agropecuaria dispuso contemplar dos grandes zonas del mapa argentino en relación con la distancia de los campos con los principales puertos (pampeana y extrapampeana), con cuatro estratos de escala productiva cada una, conforme a los registros del SISA.

![](https://assets.3dnoticias.com.ar/agricultura1.jpg)

**Repercusiones del anuncio**

La medida, que se publicará mañana en el Boletín Oficial, bajo Decreto Nº 786/20, fue adelantada hoy por el ministro Basterra, en el marco de una reunión virtual que contó con la presencia de medio centenar de productores, representantes de entidades agropecuarias y cooperativas, legisladores, ministros provinciales y demás funcionarios.

“Esto es algo soñado y muy anhelado, una gran herramienta, donde cada uno ha hecho su aporte. Factor determinante de esta política destinada a que el entramado rural se pueda preservar y el productor pueda seguir produciendo”, destacó el presidente de Federación Agraria Argentina (FAA), Carlos Achetoni. “Y se produce en un día tan especial donde Agricultores Federados Argentinos (AFA) está cumpliendo sus 88 años de vigencia”, agregó.

“Lo que estás anunciando honra la palabra empeñada. Fue un trabajo mancomunado que no quedó solo en la palabra de la Ley (Ley N° 27.541/19 de Solidaridad Social y Reactivación Productiva en el Marco de la Emergencia Pública) sino que hoy se está haciendo realidad”, señaló la senadora nacional, María de los Ángeles Sacnun.

“Es necesario contar con productores produciendo y no alquilando sus campos. No es lo mejor para el país la concentración económica, por lo que no me queda más que felicitarlos por la medida. Es un buen primer paso para un país con más productores produciendo”, dijo Gonzalo del Piano, gerente General de Agricultores Federados Argentinos (AFA).

“Hoy es un día muy especial para los pequeños y medianos agricultores por el cual peleamos mucho. Por eso hay muchos cooperativitas aquí presentes”, destacó el presidente de Federación de Cooperativas Federadas (FECOFE), Juan Manuel Rossi.

Acompañaron a Basterra durante el anunció el secretario de Agricultura, Ganadería y Pesca, Julián Echazarreta; el subsecretario de Coordinación Política, Ariel Martínez; el subsecretario de Ganadería, Delfo Buchaillot; y demás funcionarios y representantes del sector.

Además estuvieron presentes el senador nacional, Adolfo Rodríguez Saá; la jefa de Gabinete del Ministerio de Economía, Melina Mallamace; los ministros de Desarrollo Agrario bonaerense, Javier Rodríguez, de Industria de Chaco, Sebastián Lifton; de Agricultura y Ganadería de Córdoba, Sergio Busso; de la Producción, Recursos Naturales, Forestación y Tierras de Santiago del Estero, Miguel Mandrile; de Producción, Turismo y Desarrollo Económico de Entre Ríos, Juan José Bahillo; el secretario de Agricultura y Ganadería del Gobierno de Entre Ríos , Lucio Amavet; la secretaría de Desarrollo Agropecuario de Salta, Milagros Patrón Costas; y el director de Agricultura y Ganadería de Santiago del Estero, Wilson Michelini; Santiago Gamulin, Santiago Sanger y David Chiurchiú de Asociación de Cooperativas Argentinas (ACA); Elvio Guía, Pablo Paillole Alexis Misaña de FECOFE; Isaías Ghio, Omar Príncipe, Marcelo Banchi , Andrea Sarnari, Dardo Alonso, Jorge Solmi, Ricardo Garzia y Marisa Boschetti de FAA; Silvina Campos Carles y Elbio Laucirica de Coninagro; Sergio Vigliano de la Federación de Cooperativas Lácteas (FECOLAC), y demás funcionarios y representantes del sector.