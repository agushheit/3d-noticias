---
category: Agenda Ciudadana
date: 2021-10-03T06:00:27-03:00
thumbnail: https://assets.3dnoticias.com.ar/ALBERTOENLUJAN.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: El Presidente refuerza la nueva modalidad de gestión con visitas sorpresivas
  al conurbano
title: El Presidente refuerza la nueva modalidad de gestión con visitas sorpresivas
  al conurbano
entradilla: El mandatario visitó la Casa de los Misioneros de Francisco de Lujan,
  horas antes de la tradicional peregrinación a la Virgen de esa ciudad.

---
El presidente Alberto Fernández combinó esta semana la realización de actos públicos con visitas sorpresivas a distintos puntos del conurbano bonaerense, en una nueva modalidad de gestión que se apuntala también con la nutrida actividad diaria que realiza el jefe de Gabinete, Juan Manzur, especialmente en reuniones con ministros y gobernadores.

"Estamos reconstruyendo el Gobierno. La gente reclama eso, no campaña", dijo a Télam un hombre territorial cercano al Presidente, y ejemplificó: "El que maneja un colectivo necesita ver un gobierno que se esté ocupando todos los días de las soluciones, y de mejorar, resolver y activar" la gestión.

En ese marco, el Presidente combinó esta semana los actos de gestión con recorridas en el territorio, una modalidad que inauguró la semana pasada con visitas a las localidades bonaerenses de Pilar, Ituzaingó y Dock Sud.

Este jueves, Fernández participó junto a la vicepresidenta de un acto en el Museo del Bicentenario de Casa Rosada donde se presentó el proyecto de ley del Régimen de fomento al desarrollo agroindustrial

Esta semana, el mandatario se reunió con trabajadores de Aerolíneas Argentinas y de Aeropuertos Argentina 2000 en el comedor del aeroparque Jorge Newbery; también con personas que pasaron por la cárcel y que hoy trabajan en cooperativas en Lanús, con trabajadores en Ensenada, y recorrió la nueva costanera de Punta Lara.

Este viernes, para cerrar la semana, visitó la Casa de los Misioneros de Francisco de Lujan, horas antes de la tradicional peregrinación a la Virgen de esa ciudad, y -libreta en mano- fue anotando los reclamos y necesidades de la gente con quien se sentó a charlar.

Entre los actos oficiales, este jueves se mostró junto a la vicepresidenta Cristina Fernández de Kirchner en un acto en el Museo del Bicentenario de Casa Rosada, donde se presentó el proyecto de ley del "Régimen de fomento al desarrollo agroindustrial".

El martes, participó de manera virtual del Diálogo de Alto Nivel convocado por el secretario general de la ONU, António Guterres, y la OIT para debatir la respuesta internacional coordinada al impacto del coronavirus en el mundo del trabajo y, en ese marco, reclamó "impulsar un pacto que permita extender los plazos para atender los pagos de los endeudamientos y la aplicación de menores tasas".

También esta semana, encabezó una reunión de gabinete sobre el Mercosur con la idea de "preservar la unidad del bloque regional" y de la que participaron el canciller Santiago Cafiero; los ministros de Economía, Martín Guzmán, y de Desarrollo Productivo, Matías Kulfas.

Además, el miércoles recibió a las primeras beneficiarias de las jubilaciones por Reconocimiento de Aportes de Tareas de Cuidado y almorzó este viernes con representantes sindicales nucleados en la Confederación General del Trabajo (CGT), en el que también participaron el jefe de Gabinete, Juan Manzur; y los ministros del Interior, Eduardo de Pedro; y de Trabajo, Claudio Moroni.

Esta semana, el Presidente encabezó una reunión del Mercosur de la que participaron el canciller Santiago Cafiero y los ministros Kulfas y Guzmán

Otros anuncios fueron delegados por el Presidente en sus ministros, luego de mantener reuniones con ellos, fortaleciendo la idea de mostrar un gabinete activo con anuncios en las distintas áreas.

El ministro de Agricultura, Julián Domínguez, anticipó la reanudación las exportaciones de carne a China; mientras que la ministra de Mujeres, Géneros y Diversidad, Elizabeth Gómez Alcorta; y el de Trabajo, Claudio Moroni, hicieron lo propio con el Programa Registradas para reducir la informalidad de las trabajadoras de casas particulares.

Este viernes el ministro de Ciencia, Tecnología e Innovación, Daniel Filmus anunció públicamente inversiones para obras en el Consejo Nacional de Investigaciones Científicas y Técnicas (Conicet) en 21 provincias del país; luego de que el Presidente se reuniera con un grupo de científicos y, por la tarde, la ministra de Salud, Carla Vizzotti; y el de Educación, Jaime Perczyk; hicieron lo propio con el inicio de los esquemas de vacunación con Sinopharm para niños de entre 3 y 11 años.

En el Gobierno aseguran que los cambios en el gabinete "son un impulso para reactivar la agenda nacional", y se esperanzan en que estos meses la apertura de la economía va a ser un eslabón más que permitirá llegar mejor parados, porque habrá más actividades.

"Los que estamos en el territorio, porque hablamos con la gente todos los días, vemos que pasaron muchas cosas", indicó el vocero consultado.

En el Gobierno saben que hay "demandas muy concretas" por parte de la sociedad, y "lo mismo pasa con cada provincia, con cada gobernador".

En una activa agenda de trabajo, que se inicia a las 7 de cada día en su despacho de la Casa Rosada, el jefe de Gabinete continuó esta semana con sus reuniones con los ministros de las distintas áreas del Gobierno y mandatarios provinciales.

Manzur se reunió por separado con los ministros de Transporte, Alexis Guerrera; y de Desarrollo Productivo, Matías Kulfas; y también recibió en distintas audiencias a los gobernadores de Santa Fe, Omar Perotti; de Entre Ríos, Gustavo Bordet; y de San Luis, Alberto Rodríguez Saá.

Junto a la directora ejecutiva de la Administración Nacional de Seguridad Social, Fernanda Raverta, presentó la implementación de la Jubilación Anticipada para personas que ya cuentan con los 30 años de aportes requeridos, con cinco años o menos de edad para jubilarse y desocupadas; y con el ministro de Educación entregó computadoras en Gregorio Laferrere, en el partido bonaerense de La Matanza.

Junto a Vizzotti; y los ministros Turismo y Deportes, Matías Lammens; y de Seguridad, Aníbal Fernández; habilitó el regreso del público a los estadios de fútbol; y con Vizzotti y De Pedro informó la aprobación de un financiamiento del Banco Mundial para la Argentina por 500 millones de dólares destinado a cubrir el abastecimiento de vacunas contra la Covid-19 y detalló las medidas de apertura para las fronteras y turismo internacional.