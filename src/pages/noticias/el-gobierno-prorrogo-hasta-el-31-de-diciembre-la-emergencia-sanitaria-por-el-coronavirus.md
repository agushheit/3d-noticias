---
category: Agenda Ciudadana
date: 2021-03-12T06:59:14-03:00
thumbnail: https://assets.3dnoticias.com.ar/covid-amba.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: El Gobierno prorrogó hasta el 31 de diciembre la emergencia sanitaria por
  el coronavirus
title: El Gobierno prorrogó hasta el 31 de diciembre la emergencia sanitaria por el
  coronavirus
entradilla: El decreto se publicó este jueves en horas de la tarde en el Boletín Oficial.

---
El Gobierno nacional prorrogó este jueves hasta el próximo 31 de diciembre la emergencia sanitaria establecida por la pandemia de coronavirus.

Tal como se había anticipado, el decreto se publicó este jueves en horas de la tarde en el Boletín Oficial.

La emergencia sanitaria fue declarada por el Ejecutivo el pasado 12 de marzo mediante el decreto N° 260, en el que amplió "por el plazo de UN (1) año, la emergencia pública en materia sanitaria establecida por Ley N° 27.541, en virtud de la pandemia declarada por la Organización Mundial de la Salud (OMS) en relación con el coronavirus COVID-19".

El decreto de marzo de 2020 estableció "la emergencia pública en materia sanitaria por el plazo de un año", y faculta al Ministerio de Salud "como autoridad de aplicación". Ahora, el Gobierno decidió ampliar ese estado en todo el país hasta fin de año, llegando así a los 22 meses.

"Durante el tiempo transcurrido desde el inicio de las políticas adoptadas en pos de la protección de la salud de la población, el Estado Nacional no solo ha mejorado la capacidad de atención en el sistema de salud e incrementado la adquisición de insumos y equipamientos necesarios, sino que simultáneamente instrumentó medidas para morigerar el impacto económico y social causado por la pandemia de COVID-19", indicó el DNU.

El Gobierno volvió a "recomendar restricciones de viajes desde o hacia las zonas afectadas y desde o hacia las zonas afectadas de mayor riesgo, dando intervención a las instancias competentes para su implementación".

Además, autorizó a "contratar a ex funcionarios o ex funcionarias o personal jubilado o retirado, exceptuándolos o exceptuándolas temporariamente del régimen de incompatibilidades vigentes para la administración pública".

"Establecer, ante una situación sanitaria epidemiológica crítica, un régimen de matriculación y/o certificación de especialidad provisoria para quienes no cuenten con el trámite de su titulación finalizado, reválida de título o certificación de pregrado, grado o posgrado en ciencias de la salud, previa intervención del Ministerio de Salud y con certificación de competencias a cargo de los establecimientos asistenciales que los requieran", agregó el decreto.

El pasado 8 de marzo, el director general de la OMS, Tedros Adhanom Ghebreyesus, declaró la "emergencia" sanitaria para toda Latinoamérica, luego de hallarse una nueva variante de COVID-19 en Brasil.

"La situación es muy seria y estamos muy preocupados. Las medidas sanitarias que Brasil tome deberían ser agresivas, al mismo tiempo que avanza en la vacunación", indicó el director general de la OMS en declaraciones periodísticas. 

Además, indicó que "la preocupación no gira tan solo en torno a Brasil, sino también en torno a los vecinos de Brasil", y precisó: "Es casi América Latina en su conjunto".