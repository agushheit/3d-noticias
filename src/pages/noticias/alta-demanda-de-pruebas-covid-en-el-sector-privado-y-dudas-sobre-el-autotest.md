---
category: La Ciudad
date: 2022-01-14T06:15:26-03:00
thumbnail: https://assets.3dnoticias.com.ar/AUTOTEST.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Alta demanda de pruebas Covid en el sector privado y dudas sobre el autotest
title: Alta demanda de pruebas Covid en el sector privado y dudas sobre el autotest
entradilla: "Con un pico en el último fin de semana, se realizan las dos pruebas recomendadas
  para detectar el virus\n\n"

---
"Hay una excesiva demanda", respondió Mario Osti, Presidente de la Federación de Bioquímicos de Santa Fe, apenas consultado sobre el aumento de pedidos para test de Covid-19. "El sector privado está respondiendo con la realización, por un lado, de PCR que es la técnica más sensible y más costosa y, por el otro, con la prueba de antígenos que tiene una respuesta más rápida", aportó

 Como en el sector público, el número de pedidos para análisis aumenta en los laboratorios privados: "Se reciben llamados todos los días para pruebas a gente que está con síntomas o es contacto estrecho". Y si la demanda es alta desde los días posteriores al 31 de diciembre, el pico fue en el último fin de semana.

 En cuanto a la positividad, es tan alta como la que se verifica en efectores públicos "y en algunos casos superior porque la población que consulta tiene un sesgo particular: acude a hacerse los análisis porque se siente con síntomas y necesita viajar o participar de una reunión; no es un muestreo al azar"

\- ¿Las obras sociales están reconociendo estos test?

 - De a poco los están reconociendo en Santa Fe. En el caso de la PCR unos 6 meses desde del comienzo de la pandemia hubo muchas obras sociales que empezaron a hacerlo. La disparidad de precio es de acuerdo a qué decida el ente financiador sobre qué reconocer y cómo pagarlo.

La recomendación a la gente es que antes de tomar una decisión consulte con su obra social para ver qué le reconoce y cómo lo hace.

 **Autotest**

 Entre las novedades, cambios de criterio y nuevas recomendaciones en materia de Covid-19, en los últimos días se conoció la autorización de Anmat sobre la utilización de autotest, práctica que viene siendo cuestionada desde la disciplina profesional de Osti, quien es también integrante de la mesa directiva de la Confederación Unificada Bioquímica de la República Argentina (Cubra). "No estamos de acuerdo con implementar el autotest, no como herramienta que puede ser válida porque el principio técnico es similar a los que se usan para medir antígenos". "Lo que a nosotros nos hace mucho ruido es cómo se ha diagramado el uso de esas pruebas" cuya reglamentación fue publicada el martes.

 Para el profesional, el "gran tema" es cómo se van a usar y quiénes, qué va a hacer la gente porque en principio la muestra no es nasofaríngea, sino nasal o por saliva. "Por lo tanto debe existir una carga viral muy grande para que el test dé positivo".

Por otra parte, "los bioquímicos no conocemos aún la eficacia de estos test. Como ocurre con otras pruebas, cada marca tiene su propia eficacia y su propio desempeño: ¿Cómo lo detectamos?, después de un tiempo largo en función de usar criterios analíticos para ejecutar resultados pero también porque ponemos en juego controles de calidad" con una respuesta que otorga la experiencia. "Ese aspecto junto con el seguimiento de los pacientes y la posibilidad de que no hagan el descargo que corresponda para hacer la estadística epidemiológica, nos preocupa".

 La otra pregunta es cómo se informan los resultados de las pruebas. "Los bioquímicos que prestan este servicio tienen que estar inscriptos para informar al Sistema Integrado de Salud (Sisa)". En cuanto a los autotest y "según indica la resolución, cada casete de reactivo debe tener un código de barras o un QR para identificar que se usó y debe ser informado al Sistema Nacional de Vigilancia de la Salud del que participan los farmacéuticos. Luego, si es positivo o negativo el farmacéutico va a 'levantar' la información al sistema Sisa".

 En lo personal, Osti se muestra preocupado por esa expresión de "test orientativos". Y lo explica: "Desde el punto de vista clínico esa clasificación no existe: hay 6 clasificaciones de test bioquímico pero la de 'orientativo' no existe".

 En definitiva, "no apoyamos los autotest por dos razones: la pérdida de trazabilidad de información en epidemiología y por la toma de muestras que no creemos que se pueda hacer en forma adecuada por parte del usuario".

 Una vez más a título personal, opina que "son medidas que se toman para calmar a la gente pero que tienen una aplicación relativa. Porque no contamos ni con los medios ni las seguridades de parte informática", indica Osti para quien "la pandemia puso en relieve la importancia del diagnóstico bioquímico de las patologías. Este es un ejemplo de que nos necesitan, y mucho".

 