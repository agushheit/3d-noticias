---
category: La Ciudad
date: 2022-01-11T06:00:34-03:00
thumbnail: https://assets.3dnoticias.com.ar/LAREDONDA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'Vacunación Covid: mucha gente y todas las edades en el mismo lugar'
title: 'Vacunación Covid: mucha gente y todas las edades en el mismo lugar'
entradilla: Este lunes se habilitó la vacunación libre a menores de 18 que se sumaron
  a la demanda de mayores de edad. ¿El resultado? Largas filas en el inicio de una
  semana que se anticipa agobiante

---
Este lunes fue habilitada la vacunación libre, es decir sin turno previo, para niños, ñiñas y adolescentes de 3 a 17 años, grupo que a su vez está dividido en dos franjas: de 3 a 11 para recibir Sinopharm y de 12 a 17 para Pfizer. Así fue que un integrantes de esta comunidad etaria concurrió esta mañana a La Redonda y a La Esquina Encendida, los dos espacios habilitados para ese fin. Allí se encontró con más gente, mayor de 18, que fue a recibir su dosis de vacuna anti Covid.

El resultado fue una gran aglomeración de personas en una jornada de mucho calor donde la sombra que podía recibirse al lado de una pared o cerca de una carpa era el bien más codiciado.

  Algunas personas que concurrieron al vacunatorio ubicado sobre Salvador del Carril advirtieron que la rutina que hasta ahora no significaba demasiadas complicaciones: hacer fila, pasar el triaje, ingresar, recibir la vacuna, aguardar por si se produce algún efecto adverso y salir, se complicaba o demoraba más de lo habitual. Una explicación posible es que toda la fila confluía en la misma entrada, sin distinción de edades, y que, según quienes contaron la experiencia desde dentro, no era tan numeroso el grupo de voluntarias y voluntarios que habitualmente organizan a la población previo al acto vacunal.

 Como sea, hoy hubo mucha gente y eso era comprobable a simple vista.

 Jesús Taborda, del Ministerio de Igualdad, Género y Diversidad y uno de los encargados del vacunatorio de La Redonda, consignó que "tenemos una cantidad inusual de personas porque además de la cantidad de gente que viene por turnera, está la demanda espontánea de niños y niñas".

 Sobre el segundo punto, la merma de voluntarios, señaló que como en todos lados "tenemos algunas personas aisladas porque con este pico de contagios en todas las áreas estamos viendo que hay personal aislado. Y acá no es la excepción". Aún así, "no es una reducción tan importante de personal, como para que obstaculice el funcionamiento de ese espacio: tenemos la cantidad de gente necesaria para que el operativo funcione de manera eficaz. Eso es lo importante.

Mientras tanto, se colocaban allí terceras dosis a mayores de 60 años, mientras que adultos mayores de 18 años concurrieron para recibir primeras o segundas dosis, según el caso con el fin de completar esquemas. Y la vacunación libre a menores de 3 a 17 años.

 Además, como ya es habitual, se montó una carpa para atender a las personas con movilidad reducida, de manera que puedan recibir su dosis desde un vehículo.

 Llegando al mediodía y con una temperatura que seguía en aumento se estaba instalando un sistema de refrigeración destinado a aliviar el ambiente en la sala de espera.