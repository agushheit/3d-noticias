---
category: La Ciudad
date: 2021-09-25T06:00:54-03:00
thumbnail: https://assets.3dnoticias.com.ar/RECORRIDO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Operarán cambios de recorrido en tres líneas de colectivos
title: Operarán cambios de recorrido en tres líneas de colectivos
entradilla: A partir de este lunes, las líneas 1 y 2 dejarán de circular por calle
  Moreno. En tanto, la línea 5 volverá a su antiguo recorrido, en el norte de la ciudad.

---
La Municipalidad informa que desde el lunes 27 de septiembre, las líneas 1, 2 y 5 del transporte urbano de pasajeros por colectivo modificarán un tramo de sus respectivos recorridos.

A partir de esta medida, las líneas 1 y 2 dejarán de pasar por calle Moreno. En el caso de la línea 1, se informó que en el centro-sur de la ciudad, el circuito será: 3 de Febrero, 4 de Enero, Juan de Garay y 9 de Julio, a recorrido habitual. La línea 2, en tanto, tomará Amenábar, 4 de Enero, Juan de Garay y 9 de Julio, a recorrido habitual.

El subsecretario de Movilidad y Transporte del municipio, Lucas Crivelli, detalló que “la decisión se tomó en el marco de un reordenamiento del tránsito y para descongestionar algunas cuadras que se ven sobrecargadas con el transporte de pasajeros por colectivos. Por ello se definió que las líneas 1 y 2 no circulen más por Moreno, pero sin alejarse demasiado”.

Por otra parte, en lo que respecta a la línea 5, gracias al avance en la obra de calle Beruti, volverá a su recorrido habitual, en la zona norte de la capital. De este modo, el circuito de ida será por Hugo Wast, Cafferata, Beruti, Rosas, French, Blas Parera a recorrido habitual; mientras que el de vuelta será por Blas Parera, Beruti, Cafferata a recorrido habitual.

Al respecto, Crivelli recordó que cuando se inició la obra en calle Beruti hubo que concretar desvíos por cortes de tránsito. Ahora, los avances de obra permiten retomar el recorrido que habitualmente realizaba la línea 5.

**Refuerzo**

Por otra parte, desde la Municipalidad se informa que continúan las gestiones para aumentar las frecuencias de la línea 2 en la zona de barrio El Pozo. En ese sentido, Crivelli confirmó que continuará el servicio de coches adicionales en horario matutino. De este modo, a las 6.35 y a las 7 horas se sumarán dos unidades adicionales que partirán desde el megamercado ubicado sobre la Ruta Nacional 168, con destino al centro de la capital provincial.

El subsecretario agregó que también quedó definido el sentido de circulación de los colectivos por las calles internas de El Pozo, el cual debió modificarse por la gran cantidad de arreglos que en estos momentos concreta la empresa Aguas Santafesinas S.A. y dificultan la circulación de las unidades. De este modo, tanto la línea 2 como la línea 9 hacen su ingreso por calle Laureano Maradona, Estévez Boero, Rector Costés Piá, Jiménez de Asúa, Laureano Maradona al centro de la ciudad y en sentido exactamente inverso para la vuelta.