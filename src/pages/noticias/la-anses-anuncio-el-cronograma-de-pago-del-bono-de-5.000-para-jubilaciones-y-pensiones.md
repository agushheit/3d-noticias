---
category: Agenda Ciudadana
date: 2021-07-28T06:00:37-03:00
thumbnail: https://assets.3dnoticias.com.ar/BONOANSES.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: La Anses anunció el cronograma de pago del bono de $ 5.000 para jubilaciones
  y pensiones
title: La Anses anunció el cronograma de pago del bono de $ 5.000 para jubilaciones
  y pensiones
entradilla: Será de 5.000 pesos para quienes tengan ingresos de hasta dos haberes
  mínimos (46.129,40 pesos) y para quienes perciben entre 46.129,41 y 51.129,39 pesos,
  será el equivalente hasta alcanzar este último monto".

---
La Administración Nacional de la Seguridad Social (Anses) informó este martes el cronograma de pago del bono de $ 5.000 junto a los haberes de agosto para jubilados, jubiladas, pensionados y pensionadas que perciban hasta dos haberes mínimos, es decir, de hasta $ 46.129,40.

Las liquidaciones se harán según terminación de documento y serán del 2 al 6 de agosto para titulares de Pensiones No Contributivas (PNC); del 9 al 23 para titulares de pensiones y jubilaciones que no superen los $ 25.923; y del 24 al 30 para los que sí la superen.

"El bono será de 5.000 pesos para las personas que tengan ingresos de hasta dos haberes mínimos (46.129,40 pesos) y para las que perciben entre 46.129,41 y 51.129,39 pesos, será el equivalente hasta alcanzar este último monto", explicó el Anses en un comunicado.

Según la Anses, cerca del 83% jubilados y pensionados cobrará efectivamente el bono de $5.000 en sus salarios de agosto.

El presidente Alberto Fernández había anunciado el pago del bono el pasado 13 de julio, pero no fue sino hasta este lunes que la decisión se hizo oficial con su publicación en el Boletín Oficial, a través del Decreto 481/2021.

El bono estará destinado a "los beneficiarios de las prestaciones previsionales del Sistema Integrado Previsional Argentino (SIPA) a que refiere la Ley N° 24.241, sus modificatorias y complementarias", además de los beneficiarios de la "Pensión Universal para el Adulto Mayor, instituida por el artículo 13 de la Ley N° 27.260 y sus modificatorias", detalló el decreto.

También está dirigido a los beneficiarios de "pensiones no contributivas por vejez, invalidez, madres de siete hijos o hijas o más, y demás pensiones no contributivas y pensiones graciables cuyo pago se encuentra a cargo de la ANSES”.

"Para percibir el presente subsidio extraordinario los beneficios deben encontrarse vigentes en el mismo mensual en que se realice su liquidación", agregó el documento oficial.

Este bono de $5.000 será el tercero que se otorga en 2021 a las jubilaciones mínimas -los anteriores fueron de $1.500 en abril y mayo- aunque el primero que incluye a los haberes que duplican el haber mínimo ($23.065).