---
category: Estado Real
date: 2021-01-25T09:31:16Z
thumbnail: https://assets.3dnoticias.com.ar/epe.png
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Lt10
resumen: La EPE tercera en la lista de deudores con Cammesa
title: La EPE tercera en la lista de deudores con Cammesa
entradilla: El Gobierno da facilidades para que distribuidoras cancelen sus deudas
  con Cammesa. Se determinó adoptar los regímenes de regularización de las deudas
  y de créditos creados por la Ley de Presupuesto Nacional 2021.

---
El Gobierno puso en marcha un plan para que las distribuidoras de energía eléctrica se pongan al día con las deudas que mantienen con la Compañía Administradora del Mercado Mayorista Eléctrico (Cammesa) tras las restricciones y normativas que se aplicaron a partir de la pandemia de coronavirus. La Empresa Provincial de la Energía está tercera en el monto adeudado luego de Edenor y Edesur. Es así que la Empresa Provincial de Energía (EPE) de Santa Fe, adeuda $15.356 millones.

A raíz de las importantes deudas que se registran actualmente, la Secretaría de Energía formalizó el régimen especial de regularización de obligaciones acumuladas hasta el 30 de setiembre de 2020 por las distribuidoras de energía eléctrica con Cammesa, a través de la resolución 40/2021 publicada este viernes en el Boletín Oficial.

También se determinó un régimen especial de créditos para aquellas distribuidoras, administraciones o empresas provinciales distribuidoras de energía eléctrica que, al 30 de setiembre de 2020, no tengan deuda o su nivel sea considerada dentro de valores razonables con Cammesa o con el Mercado Eléctrico Mayorista (MEM).

La regularización se hará hasta en cinco años, con un semestre de gracia, y una tasa de interés de la mitad que cobra el MEM.

La decisión de ordenar el mercado eléctrico se oficializó un día después de que el Ente Nacional Regulador de la Electricidad (ENRE) anunciara el inicio del proceso de análisis, discusión y determinación de una nueva tarifa residencial, industrial y comercial de la energía eléctrica.

Esta tarifa debería comenzar a regir a mediados de marzo próximo, y será de transición hasta que concluya la nueva Revisión Tarifaria Integral.

En marzo del año pasado, luego de la llegada de la pandemia al país y la decisión del Gobierno de ir a un aislamiento preventivo, se dispuso que las empresas prestadoras de los servicios de energía eléctrica, entre otras, no podían disponer la suspensión o el corte de los mismos a los usuarios de sectores vulnerables, jubilados, pensionados, beneficiarios de otras pensiones y planes sociales, monotributistas sociales y de las categorías más bajas, y empleadas domésticas.

Las distribuidoras entonces no podían cortar el suministro en caso de mora o falta de pago de hasta tres facturas consecutivas o alternas, en una primera instancia, que en diciembre último se extendió a siete.

Esto generó que a partir de marzo de 2020 se produjera un incremento de la morosidad de los pagos de la facturación emitida por el Organismo Encargado de Despacho (OED) en su calidad de administrador del MEM por parte de las distribuidoras.

La resolución actual de Energía destacó que “resulta necesario establecer un procedimiento especial para la normalización de la cadena de pagos en el MEM evitando riesgos de desabastecimiento, acompañando la producción y el empleo, garantizando el derecho al acceso a la energía eléctrica y redundando en una mejora de la calidad de vida por parte de los ciudadanos”.

En este marco se determinó adoptar los regímenes de regularización de las deudas y de créditos creados por el artículo 87 de la Ley de Presupuesto Nacional 2021, para que las distribuidoras eléctricas puedan refinanciar sus deudas históricas, “ya sean por consumos de energía, potencia, intereses o penalidades”.

En este sentido, se resolvió que los créditos del Tesoro Nacional que reciba cada empresa podrán ser equivalentes hasta cinco veces la factura media mensual del último año de la compañía o al 66% de la deuda existente con Cammesa.

El resto del pasivo deberá ser pagado en hasta 60 cuotas, con 6 meses de gracia y una tasa de interés equivalente a la mitad de la vigente en el MEM.

La deuda de las distribuidoras con Cammesa, de acuerdo con su último informe de cobranza, alcanza los $142.000 millones, y las cifras mayores corresponden a las distribuidoras del área metropolitana Edesur, con $20.800 millones; y Edenor, con $18.000 millones.

Le siguen la Empresa Provincial de Energía (EPE) de Santa Fe, con $15.356 millones; Electricidad de Misiones (EMSA), con $12.623 millones; la chaqueña Secheep, con $10.000 millones; Electricidad de Mendoza (Edemsa), con $9.813 millones; Empresa Provincial de Energía de Córdoba (EPEC), $6.758 millones.

En Chubut, la Dirección Nacional de Servicios Públicos y las cooperativas Comodoro Rivadavia, Puerto Madryn, Rawson, Sarmiento y Trelew, acumulan en conjunto una deuda que supera los $ 12.000 millones.

En tanto que Cooperativa Bariloche, Energía San Juan y Empresa Jujeña de Energia (Ejesa) son las únicas distribuidoras de todo el país que están al día con Cammesa.

La Secretaría de Energía destacó que “con el objeto de disminuir el impacto de la situación, el Estado realizó aportes del Fondo Unificado al Fondo de Estabilización para permitir saldar las acreencias de los agentes del MEM en plazos y formas compatibles con el contexto actual”.

Además, puntualizó que “el tratamiento que se debe adoptar con las distribuidoras con el objetivo de regularizar las obligaciones debe incorporar las particularidades de cada una de las empresas prestadores del servicio público de distribución eléctrica y su área de concesión, para otorgar una solución de sostenibilidad de la deuda a la vez que se garantice la calidad del servicio público de energía eléctrica”.