---
category: Estado Real
date: 2021-01-19T09:00:43Z
thumbnail: https://assets.3dnoticias.com.ar/conocimiento_activo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de la provincia de Santa Fe
resumen: 'Conocimiento Activo: Se lanzó la convocatoria para realizar charlas y actividades
  científicas, tecnológicas y de innovación en ámbitos pedagógicos'
title: 'Conocimiento Activo: Se lanzó la convocatoria para realizar charlas y actividades
  científicas, tecnológicas y de innovación en ámbitos pedagógicos'
entradilla: La iniciativa es impulsada por la Secretaría de Ciencia, Tecnología e
  Innovación y tiene como objetivo difundir el conocimiento científico y tecnológico
  entre estudiantes de escuelas primarias y secundarias.

---
En el marco del dispositivo “Verano Activo”, lanzado por el Ministerio de Educación de la provincia, la Secretaría de Ciencia, Tecnología e Innovación del Ministerio de Producción, Ciencia y Tecnología presentó “Conocimiento Activo”, una iniciativa para impulsar la realización de charlas y actividades presenciales para que estudiantes de escuelas primarias y secundarias conozcan y experimenten más sobre el mundo científico, tecnológico y de innovación en todo el territorio santafesino.

La convocatoria está destinada a investigadores, becarias y becarios, o personal de apoyo de otras instituciones científicas y docentes universitarios, estudiantes avanzados o graduadas y graduados, que se desempeñen en disciplinas pertinentes a los objetivos del programa (física, química, biología, diseño, ciencias sociales, astronomía, ciencias de la vida, robótica, mecatrónica y disciplinas afines).

A través de “Conocimiento Activo” se realizarán encuentros presenciales para desarrollar actividades disruptivas al aire libre, de simple interpretación, educativas, que despierten el imaginario y el interés en ciencia, tecnología e innovación, que puedan estimular el talento santafesino para la generación de nuevas habilidades para el futuro y que más chicas y chicos se vinculen con estos ámbitos.

Desde la cartera científica, se brindarán materiales descargables con historias y actividades desarrollados en colaboración con Ciencia Maravilla, para que puedan realizarse en los encuentros junto a los estudiantes.

El plazo de inscripción a la convocatoria está abierto hasta el 22 de enero. Aquellos interesados en contribuir en la revinculación escolar de chicas, chicos y jóvenes por medio de la ciencia, la tecnología y la innovación, pueden obtener mayor información y acceder a las bases y condiciones haciendo CLICK ACÁ.

**VERANO ACTIVO**

Verano Activo es una iniciativa del Ministerio de Educación de la provincia que busca identificar y recuperar el vínculo pedagógico de los y las estudiantes, cuyas trayectorias escolares hayan sido interrumpidas o que estén en riesgo de interrupción y que logren, según los casos, acreditar los conocimientos adquiridos durante el verano.

Son acciones coordinadas entre la provincia y los gobiernos locales con la participación del sistema educativo y múltiples sectores del Estado como Salud, Trabajo, Desarrollo Social, Producción, Cultura e Igualdad y Género. También participan distintas organizaciones de la sociedad civil.

Los principales actores que participan de Verano Activo son los equipos recreativos; docentes departamentales; referentes culturales locales; estudiantes voluntarios de nivel superior; equipos socio-educativos; centros de estudiantes; delegados regionales; y coordinadores pedagógicos.