---
category: Agenda Ciudadana
date: 2021-10-10T06:00:35-03:00
thumbnail: https://assets.3dnoticias.com.ar/ESMA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: La Secretaría de DDHH implementa un plan para "acelerar" juicios de lesa
  humanidad
title: La Secretaría de DDHH implementa un plan para "acelerar" juicios de lesa humanidad
entradilla: La iniciativa fue presentada esta semana en un encuentro con la prensa
  que se desarrolló en la sede del Archivo Nacional de la Memoria, ubicado en el predio
  de la Ex Esma.

---
La Secretaría de Derechos Humanos implementa un plan estratégico que pretende "visibilizar y acelerar" las causas y juicios de lesa humanidad en todo el país con medidas periciales, presentaciones en nuevas querellas y la apertura de investigaciones sobre casos de complicidad civil empresarial con la última dictadura militar y sobre el aparato de inteligencia del terrorismo de Estado.  
  
La iniciativa fue presentada esta semana en un encuentro con la prensa que se desarrolló en la sede del Archivo Nacional de la Memoria, ubicado en el predio de la Ex Esma, donde se difundió un documento que tiene en cuenta los aportes del Archivo, del Centro Ulloa para la protección de las víctimas, la Unidad de Delitos de Responsabilidad empresarial y la Dirección de Comunicación de la Secretaría.  
  
"Queremos escuchar a las víctimas y darle más visibilidad a las causas y a los juicios para reforzar el proceso de Memoria, Verdad y Justicia. Durante los cuatro años del neoliberalismo, la secretaría llegó a recibir a los familiares y a los representantes de los genocidas. Queremos dejar atrás esa etapa y recuperar el retraso que se originó en las investigaciones como consecuencia de la pandemia de coronavirus", señaló el secretario de Derechos Humanos, Horacio Pietragalla Corti durante la presentación.  
  
En ese sentido, Pietragalla aseguró que su gestión potenció el rol de los trabajadores de la secretaría y vinculó áreas de investigación con el propósito de dar una respuesta ante la necesidad de acelerar las condenas y reparar a las víctimas.  
  
En cumplimiento de esos objetivos, en los últimos meses se incrementaron "las acciones de seguimiento y denuncia frente a situaciones de demoras judiciales y para garantizar celeridad en las causas", se informó desde la Secretaría.  
  
Se incorporaron peritos para las juntas médicas y se impulsan los procesos judiciales vinculados con la responsabilidad empresarial y articulaciones con el Ministerio Público Fiscal.

  
También se incorporaron peritos para las juntas médicas y se impulsan los procesos judiciales vinculados con la responsabilidad empresarial y articulaciones con el Ministerio Público Fiscal.  
  
Evitar las demoras que se producen en la resolución de muchas causas que están acumuladas en Casación y la Corte Suprema es un objetivo de la secretaría y con ese propósito se realizaron 20 presentaciones en los últimos meses ante esas instancias.  
  
En causas como Blaquier, Ponce de León, Olmedo, Ford, Ledo y Aredez, se presentaron escritos para solicitar sentencias de la Corte y Casación, según los casos.  
  
Además, la secretaría solicitó a Casación la celebración de audiencias, presenciales o virtuales para que las víctimas sean escuchadas por los magistrados y se reemplacen los procedimientos escritos.  
  
Pietragalla afirmó además que en la actualidad "no están dadas las condiciones para una reunión" con la Corte Suprema en el marco de la Comisión Interpoderes de seguimiento de las causas de lesa humanidad que depende del máximo tribunal, porque "no se pueden convalidar" las demoras que se produjeron en varios expedientes en los últimos años.  
  
La política del plan pasa por articular con las querellas y este año se sumaron 36 nuevas presentaciones y hay 258 representaciones legales del Estado.  
  
"Planificar acciones para acelerar las causas y el proceso de juzgamiento es la prioridad. Pasaron 45 años del último golpe de Estado y hay causas que están paradas en instrucción, Casación y la Corte. Ese es el objetivo de este Plan", aseguró la subsecretaria de Protección y Enlace Internacional en Derechos Humanos, Andrea Pochak.  
  
En ese línea, la Secretaría creó en agosto pasado un cuerpo de peritos y control de las prisiones domiciliarias de los acusados, procesados y condenados por crímenes de lesa humanidad, y se mantiene desde el organismo "una participación activa" en la verificación de los cumplimientos de las penas de libertad condicional.  
  
También se "fortalecieron" los equipos de investigaciones de programas como el de Verdad y Justicia, creado en 2006, y que este año elaboró informes sobre los Fondos Secretos del Batallón 601, los Libros históricos del III Cuerpo de Ejército y los vuelos de la muerte en Entre Ríos, y "se contestan de forma permanente oficios y requerimientos que son solicitados desde el poder judicial".  
  
Investigar el rol que cumplieron los actores económicos con el terrorismo de Estado y la represión ilegal en causas como Molinos, Acindar, la Veloz del Norte, Blaquier, Chavanne Grassi y Ford, es otros de los lineamientos de esta iniciativa, según se consignó.  
  
En ese sentido, desde la secretaría se apunta "a alentar la conformación y puesta en funcionamiento de la Comisión Bicameral de la Verdad, la Memoria, la Justicia, la Reparación y el Fortalecimiento de la Democracia", creada por una ley y que aún no se constituyó.  
  
Preservar los materiales y registros audiovisuales de los juicios de lesa humanidad es otro de los objetivos de este Plan, y se le pidió a la Corte que facilite el acceso a copias de esos videos para que el Archivo Nacional de la Memoria pueda preservarlos.  
  
La secretaría también lanzó este año un sitio web que reúne datos de los juicios y una sección de microrrelatos audiovisuales con declaraciones de sobrevivientes, víctimas y familiares que declaran en los juicios.  
  
"Estamos en un camino para evitar la impunidad biológica. No queremos que los genocidas queden sin sentencias y las víctimas sin Justicia", subrayó Pietragalla.