---
category: Agenda Ciudadana
date: 2021-12-21T06:00:30-03:00
thumbnail: https://assets.3dnoticias.com.ar/los3mosqueteros.png
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: Tras el revés en Diputados por el Presupuesto 2022, habrá sesiones extraordinarias
title: Tras el revés en Diputados por el Presupuesto 2022, habrá sesiones extraordinarias
entradilla: 'Fernández, Massa y el bloque oficialista buscan unificar el discurso
  respecto de "los perjuicios que sufrirán especialmente las provincias y municipios"
  por haber fracasado la aprobación de la norma en la última sesión.

'

---
Luego del revés que sufrió el oficialismo en su intento por aprobar el proyecto de Presupuesto 2022 en la sesión de la semana pasada, cuando la oposición se unió en contra de la iniciativa, el Gobierno definió la convocatoria a sesiones extraordinarias durante enero y febrero.

El titular de la Cámara de Diputados, Sergio Massa, y el bloque oficialista que comanda Máximo Kirchner se reunían esta noche en la Quinta de Olivos con el presidente Alberto Fernández para terminar de pulir la agenda de proyectos que el Poder Ejecutivo incorporará al temario, al tiempo que se buscará unificar el discurso respecto de "los perjuicios que sufrirán especialmente las provincias" por haber fracasado la aprobación de la norma.

A través de un informe, el propio Massa ya había advertido sobre las consecuencias que padecerán provincias y municipios a raíz de la no aprobación del Presupuesto, al señalar que se perderán la posibilidad de recibir fondos coparticipables.

La oposición cruzó duramente al titular de Diputados y consideró que el mensaje encubría una "amenaza" a los gobernadores.

Un encumbrado miembro del bloque oficialista defendió a Massa y destacó que "el informe es descriptivo, no es una amenaza"

"El daño más grande lo sufren provincias y municipios. Es la consecuencia inexorable de una acción política. Estamos describiendo resultados. Los municipios pierden fondos educativos. Las provincias pierden fondos coparticipables. La Nación pierde facultades en cuanto al régimen de retenciones. Festejaste el gol, el público te aplaudió pero después hay consecuencias institucionales", lanzó.

Entre esas consecuencias, mencionó además que el impasse del Presupuesto "retrasó el acuerdo con el Fondo Monetario Internacional", que implicará el aval de un "programa de metas" consensuada con ese organismo.

Según supo NA, el Presupuesto no estaría dentro de los temas a tratar durante enero y febrero, y todo parece indicar que la cuestión se resolverá con un decreto que prorrogará la vigencia del Presupuesto 2021.

"En el 2020 funcionamos con prórroga; lo mismo sucedió en 2011", señaló la fuente consultada, al recordar el bloqueo del llamado "Grupo A" al proyecto presupuestario impulsado por el entonces Gobierno de Cristina Kirchner.

En estas circunstancias, una de las preocupaciones del Frente de Todos tiene que ver con qué hacer con "todas las cosas que se cayeron" a partir del rechazo al proyecto.

Por ejemplo, un tema que inquieta tiene que ver con los subsidios al transporte, que de acuerdo a la norma rechazada por la oposición elevaba las partidas para las provincias de 27.000 millones de pesos a a 46.000 millones, y destacó que el Consenso Fiscal acordado durante el macrismo y con vigencia en la actualidad limita la posibilidad de establecer cambios por decreto.

Sobre las razones de que haya naufragado la aprobación del Presupuesto en la última sesión, la fuente consultada señaló que "un problema serio" es que "la oposición tenga diez bloques" y haya que negociar con cada uno por separado.

En ese sentido, reveló que en la previa a la sesión hubo distintas instancias de negociación con el PRO, la UCR y Evolución para incorporar mejoras en el texto, como fondos adicionales para universidades, descuentos en el IVA para clínicas y paquetes de obras.

Según explicó, estos bloques avanzaron con las negociaciones con el oficialismo porque contaban con que la Coalición Cívica se iba abstener, lo cual hubiera facilitado el número al Frente de Todos para la aprobación.

Cuando la Coalición Cívica modificó su postura y decidió rechazar el proyecto, el resto de los bloques de Juntos por el Cambio recalcularon y cerraron filas en contra de la iniciativa, dejando sin efecto las negociaciones previas para enviar el proyecto a comisiones para implementar allí una serie de cambios.

"Esta oposición que reclamaba por el déficit fiscal habilitó la prórroga de un presupuesto que estipula un 4,5 de déficit", cuestionó.

"Todavía están en campaña, se olvidaron que ya terminó la elección. Uno supone que cuando termina la elección ya se deja la campaña. No es lo que pasó con la oposición. El voto (de ellos) es político. Es demostrarle que ganaron y que ese es el resultado", concluyó.