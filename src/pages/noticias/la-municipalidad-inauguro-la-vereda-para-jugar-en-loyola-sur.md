---
category: La Ciudad
date: 2020-12-16T11:51:37Z
thumbnail: https://assets.3dnoticias.com.ar/loyola1.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: La Municipalidad inauguró la “Vereda para jugar” en Loyola Sur
title: La Municipalidad inauguró la “Vereda para jugar” en Loyola Sur
entradilla: Es la primera de una serie de intervenciones que se proyectan para mejorar
  el entorno incorporando al espacio público una propuesta cultural y educativa para
  todas las edades.

---
Las acciones del Plan Integrar vuelven a ser noticia esta semana, después de la reciente presentación del **Programa Estaciones**. En barrio Loyola Sur, los vecinos junto al intendente Emilio Jatón, dejaron habilitada esta mañana la primera “**Vereda para jugar**”, una intervención que mejora el espacio público con la incorporación de juegos en la vereda y nuevas propuestas al interior de la Estación y el Jardín Municipal de ese barrio, ubicado en el norte de la ciudad.

Lo hicieron recorriendo la vereda que une esos dos espacios, participando de distintas propuestas lúdicas, disfrutando de intervenciones de los artistas de Circo Litoral y música en vivo con Gerardo Aznar y Francisco Cecchini.

La vinculación entre los dos espacios de cuidado gestionados por el municipio se concreta materialmente mediante la Vereda para Jugar y se trasladan también a acciones para integrar a todas las infancias. Por eso, la propuesta es fortalecerlos durante todo 2021 como espacios de pertenencia, participación y convivencia, con actividades y alternativas culturales destinadas no solo a la primera infancia sino a niñas y niños de todas de todo el barrio.

Así lo indicó el secretario de Educación y Cultura, Paulo Ricci, quien consideró este proyecto como “una manera de poner en sistema el programa Estaciones que presentó ayer el intendente. A partir de la cercanía y de la proximidad de las Estaciones con los Jardines Municipales, vamos a seguir generando estos playones de juego o veredas que son una manera también de darle al barrio un espacio público recuperado de calidad para que las infancias puedan disfrutar, aprender y crecer”.

Además, Ricci anticipó que la propuesta en Loyola Sur tendrá continuidad durante el verano, los jueves y viernes, de 15 a 18.30, con actividades coordinadas por integrantes de la Secretaría de Educación y Cultura. Los encuentros se desarrollarán atendiendo a los protocolos sanitarios vigentes, por lo que se recuerda el uso obligatorio de barbijo y mantener la distancia física.

![](https://assets.3dnoticias.com.ar/loyola.jpg)

# **Trabajo territorial**

“Vereda para jugar” surgió del entramado territorial entre la Municipalidad y la Red de Instituciones de Loyola Sur. La Escuela de Diseño y Artes Visuales del Liceo Municipal también fue parte de ese trabajo conjunto, aportando el diseño y la realización de las baldosas cerámicas que se usaron para intervenir las dos veredas sobre calle Pedroni y Furlong. Además, se comenzó a trabajar con jóvenes de la Red de Instituciones para la realización de un mural en un patio interno del Jardín.

La realización del proyecto fue posible con fondos obtenidos del Plan Nacional de Primera Infancia, que tramitó el municipio después de completar una serie de gestiones que estaban inconclusas del anterior ciclo lectivo.

# **Mejoras para el barrio**

Liliana Benítez participa de la Red de Instituciones como integrante de la Vecinal Loyola Sur. Valoró la creación de la red durante la primera mitad de este año, y a pesar de que la pandemia dificultó las reuniones presenciales, contó que acordaron mejoras para el barrio, que consideraban prioritarias como el arreglo de calles de acceso y la “Vereda para jugar”. “Esto nos alegra mucho porque en estos barrios que son muy postergados, los chicos no tienen plazas para divertirse y jugar, por eso nos parece lindo que lo disfruten, que vengan con sus papás a pasar la tarde”, dijo.

A la rayuela, el ajedrez y el ta-te-ti, que transformaron el entorno del Jardín y la Estación, se sumaron una serie de dispositivos que invitan a circular entre los dos lugares, y a experimentar a partir de distintas ideas, materiales y lenguajes artísticos en dispositivos como las mesas de papel, del barro, la mesa para construir títeres, Escuela Rodante, rondas de lectura de cuentos y juegos cooperativos. Para promover la alimentación saludable, se comparten licuados y frutas durante la jornada.

Inés Lucero vive desde hace 21 años en Loyola Sur y por eso consideró que esta propuesta “es una buena idea, ya que no tenemos plazas cerca para llevar a los chicos, sobre todo en este año que estuvieron tanto tiempo encerrados sin saber a qué jugar”.

En representación del Movimiento Los Sin Techo, Andrea Cuevas dijo que acompañan siempre ideas para mejorar los barrios donde trabajan, como Loyola Sur y Jesuitas. Por eso consideró que la Vereda para Jugar “es maravillosa para los chicos de la villa que no tienen un espacio lúdico, para jugar, para sentirse dueños. El movimiento que hay en el barrio no se había visto antes. Cuando empezamos a participar esto daba la impresión de ser un lugar abandonado. Está bueno verlo colorido y lleno de vida, y cómo se enganchan los chicos con las actividades”.

![](https://assets.3dnoticias.com.ar/loyola2.jpg)

# **Presentes**

También estuvieron esta mañana, la secretaria de Obras y Espacio Público, Griselda Bertoldi; el secretario de Integración y Economía Social, Mariano Granato; la secretaria de Políticas de Cuidado y Acción Social, Victoria Rey; la subsecretaria de Gestión Cultural y Educativa, Huaira Basaber, y la concejala Mercedes Benedetti, entre otras autoridades e integrantes de la Red de Instituciones del barrio.