---
layout: Noticia con imagen
author: .
resumen: "#Campo: Agromakers"
category: El Campo
title: La provincia participó del cierre de Agromakers, maratón de ideas que
  promueve el talento de jóvenes estudiantes
entradilla: "Con la presentación de 7 proyectos, se llevó adelante la “Peña de
  Pitch” del evento que reúne a escuelas de la provincia para plantear
  respuestas a desafíos de la sociedad. "
date: 2020-11-19T12:55:34.061Z
thumbnail: https://assets.3dnoticias.com.ar/campo.jpg
---
Más de 120 estudiantes pasaron por esta edición y presentaron 45 soluciones innovadoras a problemáticas planteadas.

En el marco del programa **Agromakers**, **una iniciativa que busca generar y desarrollar la cultura emprendedora tecnológica en jóvenes de escuelas agrotécnicas y técnicas de la provincia**, se realizó un encuentro con 64 estudiantes de la provincia, quienes presentaron 7 proyectos que fueron mencionados y ganadores de las ediciones anteriores.

La actividad se realizó de forma virtual y fue organizada conjuntamente por la Fundación de la Bolsa de Comercio de Rosario, la Universidad Austral de Rosario y la Secretaría de Ciencia, Tecnología e Innovación del Ministerio de Producción, Ciencia y Tecnología de la provincia de Santa Fe.

Más de 120 estudiantes y docentes de toda la provincia participaron de distintas instancias con el propósito de que jóvenes de los últimos años de escuelas secundarias agrotécnicas y técnicas desarrollen propuestas innovadoras ante los desafíos que atravesamos como sociedad, en el marco de una jornada intensiva de co-creación, incentivándolos a desarrollar emprendimientos que generen transformaciones de valor agregado en sus comunidades.

Durante el programa se llevaron a cabo dos peñas para compartir información y experiencias en la previa a los hackatones, que los estudiantes se saquen dudas a la hora de desarrollar sus ideas-proyectos. También hubo música y se recitó poesía.

Luego se desarrollaron las maratones de ideas, donde **se presentaron 45 proyectos emprendedores que se alinearon con los desafíos propuestos: soluciones para el ahorro, consumo y producción responsable de energía; soluciones para combatir el cambio climático y sus efectos; soluciones productivas y sostenibles para reducir los efectos del COVID-19.**

Las instituciones educativas que formaron parte son de las localidades de Tacuarendi, Las Toscas, El Rabón, Villa Guillermina “Obraje San Juan”, Campo Fiant, Ambrosetti, Chabás, Ceres, Villa Minetti, Santa Teresa y María Juana.

## **LA PEÑA**

Esta última instancia, llamada “La Peña del Pitch”, reunió a 7 proyectos que recibieron menciones especiales durante los dos hackatones, que se presentaron ante un grupo de expertos de oratoria de la Cámara de Jóvenes Internacional Rosario (JCI). 

Hubo soluciones en torno al turismo rural local como una propuesta sostenible ante los desafíos productivos emergentes por el Covid 19, un túnel sanitizante, harina a partir de grillo, propuestas de huertas sustentables, módulos agroecológicos de generación de energía eléctrica a través de ejercicio físico, un club de compost y producción y comercialización de descartables biodegradables a partir de deshechos del desmonte de algodón.

Además, se presentó en vivo el grupo musical Ñaupa Cunan para cerrar el encuentro.