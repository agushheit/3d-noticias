---
category: Agenda Ciudadana
date: 2021-08-04T06:15:54-03:00
thumbnail: https://assets.3dnoticias.com.ar/PROGRESAR.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Las Becas Progresar pagarán entre $ 21.600 y $ 58.000
title: Las Becas Progresar pagarán entre $ 21.600 y $ 58.000
entradilla: El incentivo abarca a todos los niveles educativos. Requisitos para acceder
  al beneficio.

---
Desde la Administración Nacional de la Seguridad Social (Anses) se confirmó la extensión en el tiempo de las becas Progresar, destinadas a todos los estudiantes de todos los niveles que viven en el país. También se encuentra abierta la convocatoria para agosto y se informaron los montos disponibles para cada nivel de educación.

El organismo que dirige Fernanda Raverta, otorgará a los inscriptos que cumplan con los requisitos un monto de dinero específico para garantizar el acceso a la educación y que los jóvenes puedan terminar sus estudios, desde el primario hasta el nivel universitario.

Link para inscribirse: [https://www.argentina.gob.ar/educacion/progresar](https://www.argentina.gob.ar/educacion/progresar "https://www.argentina.gob.ar/educacion/progresar")

**¿Qué son las becas Progresar?**

Impulsadas desde el Ministerio de Educación de la Nación, las Becas Progresar son una ayuda económica para miles de estudiantes, a los que se busca motivar y acompañar, desde el Estado, para que continúen con su formación.

**Estos son los requisitos de postulación**

\- Argentinas/os nativas/os; naturalizadas/os o extranjeras/os, con residencia legal de dos (2) años en el país y contar con DNI.

\- Alumnas/os regulares de una institución educativa.

\- Personas de entre 18 y 24 años cumplidos.

\- Se extiende hasta 35 años de edad a las personas con hijas/os menores de 18 años pertenecientes a hogares monoparentales.

\- Sin límite de edad para personas trans, de pueblos indígenas, con discapacidad o refugiadas

**¿Cuánto se cobra con las becas Progresar?**

\- Los interesados en finalizar los estudios escolares (primaria y secundaria) o los cursos de Formación Profesional recibirán un monto total, por 6 meses, de $ 3.600. 2/8/2021 L

\- Para los que quieren terminar los estudios universitarios, las becas variarán según la complejidad de la carrera que cursen y el nivel en el que se encuentren: arranca desde los $ 3.600 hasta $ 4.600.

\- Quienes cursen el nivel terciario podrían recibir $ 3.800.

\- Para los estudiantes de enfermería la cantidad de dinero es distinta y quienes lo realizan en terciarios van a cobrar desde $ 5.000 hasta $ 8.000. Mientras tanto, quienes cursen la carrera universitaria van a cobrar desde $ 5.000 hasta $ 9.700. De este grupo, los estudiantes del último año van a cobrar hasta $ 58.200, monto que corresponde a la suma de los 6 meses.

¿Cuáles son los motivos para quedar excluidos de la convocatoria? Según lo que se cuenta en la página oficial, hay varios motivos por los que se puede generar la exclusión de las Becas Progresar y los mismos son:

1)- Hubiera finalizado una carrera de grado, profesorado o tecnicatura.

2)- Se encontrará suspendido/a o se hubiera declarado el cese de una beca del programa por causal que le fuera imputable.

3)- Recibiera u obtuviera otra beca de estudios por parte del Estado, Institución u Organización Pública o Privada, con o sin fines de lucro, entendiéndose por tales a cualquier sistema de transferencias dinerarias directas.

4)- Se encontrará el/la aspirante a obtener la beca o cualquier miembro de su grupo familiar inscripto/a en el Régimen de Impuesto a las Ganancias.

5)- Estuviera excedido/a dos (2) años o más en el tiempo de duración de la carrera, según el correspondiente plan de estudios.

6)- Le restara para la finalización de la carrera cursar dos (2) o menos materias al momento de la primera (1°) inscripción al programa y, menos de dos (2) materias para el caso de los/as becarios/as que hubieran sido becarios/as con anterioridad.

7)- Si adeudara sólo exámenes finales y/o la realización de tesis o prácticas profesionales.

8)- No cumpliera con las demás condiciones establecidas en el presente reglamento