---
category: Estado Real
date: 2021-02-15T06:00:31-03:00
thumbnail: https://assets.3dnoticias.com.ar/fondoperotti.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Provincia Transfirió Más De $85 Millones A Gobiernos Locales Para Su Aplicación
  En Educación, Ciencia Y Tecnología
title: La Provincia Transfirió Más De $85 Millones A Gobiernos Locales Para Su Aplicación
  En Educación, Ciencia Y Tecnología
entradilla: Los aportes corresponden al Fondo de Financiamiento Educativo e incluyó,
  también, a los gobiernos locales que estaban al día con sus rendiciones y a los
  que garantizaron las clases presenciales a fin de 2020.

---
El gobierno de la provincia, a través del ministerio Gestión Pública, transfirió 82.242.406 pesos a los municipios y comunas con destino específico para gastos en educación, ciencia y tecnología del Fondo de Financiamiento Educativo. Además, lo hizo con más de 3 millones de pesos a las comunas que garantizaron el traslado de docentes, asistentes escolares y alumnos a las escuelas rurales donde el año pasado se dictaron clases presenciales.

“Esto fue fue posible merced al trabajo conjunto de las autoridades locales con los ministerios de Gestión Pública y de Educación de la provincia”, sostuvo Carlos Kaufmann, subsecretario de Comunas, y amplió: “Las transferencias se efectuaron a aquellos gobiernos del interior que estaban al día con sus rendiciones en concepto de Financiamiento Educativo a marzo de 2018”.

“La entrega corresponde a la primera cuota de este fondo de 2020 teniendo en cuenta que el año pasado en forma excepcional y atendiendo la pandemia, el Ministerio de Gestión Pública de la provincia decidió transferir todos los aportes a pesar de que había muchos municipios que adeudaban rendiciones”, graficó el funcionario provincial.

“Este año, el total de lo devengado de fondos en la primera cuota es de más de 149 millones. El resto de lo que no se transfirió fue porque municipios y comunas adeudan esas rendiciones a marzo de 2018”, informó Kaufmann.

**Firma De Acuerdos**

A su vez, la Secretaría de Integración y Fortalecimiento Institucional del Ministerio de Gestión Pública, en forma conjunta con el Ministerio de Educación, firmaron acuerdos con aquellas comunas que a fines de 2020 reiniciaron las clases presenciales en 133 escuelas a lo largo y a lo ancho de la provincia.

Allí, el gobierno provincial “garantizó el traslado gratuito de docentes, asistentes escolares y alumnos, habida cuenta de que el mismo lo afrontaron las comunas durante 2020 y ahora la provincia les transfirió los fondos por esos gastos, que sumaron más de 3 millones de pesos”, finalizó el Subsecretario de Comunas.