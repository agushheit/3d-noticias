---
layout: Noticia con imagen
author: "Fuente: Diario UNO"
resumen: Aniversario de Santa Fe
category: La Ciudad
title: "Por qué la ciudad de Santa Fe de la Vera Cruz lleva ese nombre: su origen"
entradilla: En un nuevo aniversario de la ciudad, acercamos un poco de historia
  para conocer o recordar en esta fecha especial.
date: 2020-11-15T15:15:21.947Z
thumbnail: https://assets.3dnoticias.com.ar/santa-fe.jpg
---
Este 15 de noviembre la ciudad de Santa Fe cumple 447º desde su fundación. Una fecha importante para todos los santafesinos y un momento para recordar o conocer algo de parte de esa historia. En esta oportunidad te acercamos cuál fue el origen del nombre Santa Fe para nuestra ciudad. La respuesta la buscamos en la web del Parque Arqueológico de Santa Fe La Vieja, el lugar en que se fundó la ciudad, antes de su traslado adonde está emplazada ahora.

Según se informa en el portal parte fundamental del proceso de colonización española en América era nombrar las ciudades fundadas. La toma de posesión de la tierra estaba íntimamente ligada al acto de nombrar, como dotación de sentido.

Juan de Garay nombra Santa Fe a la ciudad fundada a orillas del Río Quiloazas, tomando el nombre que los Reyes de España habían puesto a la ciudad fundada frente a Granada, antes de su caída. Desde allí se encaminó el encuentro con el rey musulmán y las condiciones de entrega de la capital musulmana en la península ibérica. Por lo tanto, este nombre representa la lucha y la defensa de la fe católica, razón por la cual hay otras ciudades en América que también recibieron el mismo nombre.

**¿Por qué los Reyes de España nombraron de esta manera a aquella ciudad?**

Algunos historiadores sostienen que fue en honor a una santa virgen y mártir de la edad media, nacida en Agen, Francia, y cuyas reliquias se veneran en una de las iglesias del “camino francés” que jalonaban con el “camino de Santiago”. Sin embargo, otros relatos indican que este culto había sido prohibido por la Iglesia y que el nombre se debía a la defensa de la fe como virtud teologal.