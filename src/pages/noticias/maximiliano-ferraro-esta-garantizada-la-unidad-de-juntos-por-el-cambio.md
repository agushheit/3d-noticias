---
category: Agenda Ciudadana
date: 2020-12-23T09:28:35Z
thumbnail: https://assets.3dnoticias.com.ar/2312ferraro.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: El Litoral'
resumen: 'Maximiliano Ferraro: «Está garantizada la unidad de Juntos por el Cambio»'
title: 'Maximiliano Ferraro: «Está garantizada la unidad de Juntos por el Cambio»'
entradilla: El legislador subrayó que unidad no es uniformidad ni unanimidad. Asumieron
  las autoridades partidarias de Santa Fe encabezadas por Lucila Lehmann.

---
Después de dos años y medio de intervención, la Coalición Cívica ARI de Santa Fe tiene nueva conducción encabezada por Lucila Lehmann, así como su congreso partidario y los delegados al comité nacional. El acto de asunción de las nuevas autoridades se hizo este lunes y contó con la presencia de Maximiliano Ferraro, presidente a nivel nacional de la fuerza que tiene como figura política a Elisa Carrió.

Ferraro, porteño, diputado nacional por CABA, recalcó la importancia de la unidad de las tres fuerzas principales que conforman Juntos por el Cambio: Pro, UCR y CC ARI.

<br/>

## **Fragmentos del diálogo con Ferraro:**

«\[Estamos\]muy contentos de haber logrado la normalización en Santa Fe después de un período de mucho trabajo de la intervención y del equipo que quedó de la Coalición Cívica y que encabeza Lucila Lehmann. La intervención fue más que nada por diferencias que hacían a la definición de la estrategia nacional que venía teniendo la CC. En 2015 tomamos la decisión de constituir Cambiemos y después Juntos por el Cambio con el radicalismo y el Pro como partidos nacionales. En Santa Fe, la intervención tuvo que ver más que nada por los lineamientos que hacían a las estrategias nacionales del partido. Lo de ayer es un logro muy importante tanto de la mesa ejecutiva nacional como de todos los que asumieron como autoridades partidarias en la provincia tras dos años y pico de arduo trabajo que además tuvo que ver con el armado de alianzas tanto provinciales en el acompañamiento en su momento a José Corral como candidato a gobernador y el armado de lo que fueron las alianzas de 2017 y 2019. La CC tiene un potencial muy importante, tiene dos diputadas: Lucila Lehmann y Carolina Castets y una banca (Sebastián Julierac) que nos fue arrebatada a nivel provincial. Esta normalización es muy importante para el sostenimiento y la construcción de la unidad de Juntos por el Cambio en la provincia».

**¿La estrategia es definida a nivel nacional?**

«Eso está claro. Es importante tener la personería en cada una de las provincias y ya son 17. Lo primero que tiene la Coalición es la decisión de poder sostener una estrategia nacional, estrategia que después se derrama en cada uno de los distritos. Todas las provincias y las autoridades de Santa Fe lo tienen bien claro: primero sostenemos y defendemos una estrategia nacional y esto tiene que ver con el fortalecimiento de la Coalición Cívica pero para consolidar el espacio de Juntos por el Cambio».

**¿Los tres partidos están en eso?**

«La Unidad en Juntos por el Cambio está garantizada y es una unidad que sostenemos el Pro, la CC y la Unión Cívica Radical. Es una unidad que tenemos que sostener y construir todos los días. Es lo que hacemos con la mesa nacional de Juntos por el Cambio y es el desafío que tienen los tres partidos en la provincia. La unidad la pudimos sostener durante los cuatro años de gobierno nacional de 2015 y 2019, y desde el 10 de diciembre de 2019 a la fecha».

**Tienen dirigentes de mucho peso en sus expresiones como, por ejemplo, Carrió**

«Y está bien que así sea. En Juntos por el Cambio hay que poner el valor unidad que no es lo mismo que unanimidad o uniformidad. Esto más que una debilidad es una fortaleza. También es una fortaleza que podamos tener distintos tipos de legitimidades y de liderazgos. Hoy en Juntos por el Cambio no hay un único liderazgo, tenemos distintos tipos y de diferentes legitimidades».

**Se habla de un frente de frentes en Santa Fe. ¿Lo desvela o debe ser un proceso natural que se pueda sumar el socialismo?**

«Desde la CC siempre tuvimos vocación de ampliación. En 2007 fuimos en espacio más amplio donde Lilita compartió la fórmula con Rubén Giustiniani del socialismo donde se incorporaron Alfonso Prat Gay, Enrique Olivera, Patricia Bullrich. En 2009, tuvimos vocación de ampliación del Acuerdo Cívico y Social; después con UNEN y más adelante ampliar más y conformar Cambiemos. Tenemos voluntad de ampliación, pero se tiene que dar en el sostenimiento de una estrategia; en qué podamos definir en para qué esa ampliación, para poder sostener una estrategia a nivel nacional. Si es en ese sentido siempre estamos predispuestos a generar los espacios de amplitud que haya que generarse».

**¿Es difícil ser presidente de un partido donde la figura es Carrió?**

«No es fácil, pero es un gran aprendizaje en este gran equipo con la propia Lilita corrida de la cotidianeidad del Parlamento y del partido sigue siendo un liderazgo importantísimo en la CC y en la política argentina. Valoro muchísimo el trabajo en equipo, la libertad y la confianza que se nos da para sostener un partido que el año que vienen cumple 20 años. Es uno de los partidos post crisis 2001, fundado por una mujer en La Emilia (provincia de Buenos Aires) y poco a poco fuimos consolidando el equipo. Ahora presentamos una mesa en Santa Fe como lo fuimos haciendo en otras provincias. No nos callamos cuando creemos que hay hacerlo, y eso es parte de nuestro ADN».

<br/>

## **«Ajuste de 130 mil millones»**

Ferraro ratificó que mañana los diputados de Juntos por el Cambio firmarán un dictamen pidiendo la derogación del DNU sobre sistema previsional para volver a la fórmula vigente desde 2017. Y agregó: «rechazamos este nuevo ajuste y manotazo al bolsillo de los jubilados. Estamos convencidos de que la fórmula que logramos en 2017 era una fórmula que beneficiaba mucho más a los jubilados que esta que propone el Senado. En lo que va del 2020, el ajuste que se le hizo a los jubilados asciende a 130 mil millones. Hay un problema de sostenibilidad del sistema pero a su vez de ajuste y manotazo en el bolsillo de jubilados».

El legislador señaló que «el oficialismo (Frente de Todos) decidió dictaminar  en comisiones. Vamos a ir por el rechazo y derogación del DNU que suspendió la fórmula del 2017 después de la tonelada de piedras. El 29 vamos al recinto a oponernos al ajuste».

## **Mesa**

La actual diputada nacional Lucila Lehmann es la nueva presidente de la Coalición Cívica ARI de Santa Fe acompañada por Mauricio Amer y Mariano González en las vicepresidencias; Sebastián Julierac como secretario general y Fabiana Retamar como tesorera.

Julierac será el presidente del congreso provincial partidario y por otra parte se designaron los 18 delegados al comité nacional.

En el acto también asumieron los titulares partidarios en seis departamentos de la provincia, entre ellos La Capital y Rosario.