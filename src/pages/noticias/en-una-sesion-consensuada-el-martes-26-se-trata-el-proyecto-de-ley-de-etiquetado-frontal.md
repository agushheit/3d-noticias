---
category: Agenda Ciudadana
date: 2021-10-18T06:15:54-03:00
thumbnail: https://assets.3dnoticias.com.ar/sesionetiquetado.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: En una sesión consensuada, el martes 26 se trata el proyecto de Ley de Etiquetado
  Frontal
title: En una sesión consensuada, el martes 26 se trata el proyecto de Ley de Etiquetado
  Frontal
entradilla: " Fuentes parlamentarias confiaron que el martes 26 sería la última sesión
  antes de las elecciones generales del 14 de noviembre."

---
El consenso alcanzado entre las diferentes fuerzas políticas que integran la Cámara de Diputados posibilitará que el plenario del cuerpo retome el martes 26 de octubre su actividad en el recinto con el tratamiento de los proyectos de Ley de Etiquetado Frontal y de políticas para personas en situación de calle, entre otras iniciativas, tras la fallida sesión de hace dos semanas.  
  
Con la realización de esta sesión, la Cámara volverá a funcionar en plenitud luego de tres meses y medio: el 8 de julio había escuchado el informe de gestión del entonces jefe de Gabinete, Santiago Cafiero, y una semana antes se había realizado la última sesión especial con el tratamiento de proyectos.  
  
El escenario de campaña electoral, el receso invernal y la demora en la renovación del protocolo de sesiones mixtas (presencial y virtual) vencido desde el 12 de julio, condicionaron la vuelta al trabajo del plenario del cuerpo, que finalmente será presencial según lo dispuesto a fines de septiembre por la Presidencia de la Cámara.  
  
La vuelta a la metodología de trabajo de la pre-pandemia se da con algunos complementos sanitarios como hisopados, uso de tapabocas y limitación en la cantidad de asesores de los legisladores, por lo que para la puesta en marcha de una sesión se requiere de una mayor planificación previa.  
  
En ese contexto, el Frente de Todos intentó el pasado martes 5 llevar adelante una sesión especial para tratar proyectos como la Ley de Etiquetado Frontal de Alimentos, la de políticas para personas en situación de calle y un par de proyectos con beneficios al sector viñatero, pero fracasó en su intento ya que sólo consiguió sentar en las bancas a 122 diputados, siete menos que el quórum de 129.  
  
La oposición, encabezada por Juntos por el Cambio, argumentó ese día su ausencia en el desacuerdo con las formas en las que el Frente de Todos convocó a la sesión, ya que, afirmaron, "lo hizo el viernes anterior a última hora de la tarde y sin consultar al resto de los bloques".  
  
Además, los diputados de JxC conducidos por el radical Mario Negri aclararon ese día que coincidían en la necesidad de abordar esos temas, pero que además se debían llevar al recinto la denominada Ley Ovina, e iniciar el debate en comisiones de proyectos como Ley de Alquileres, Boleta única y Emergencia Educativa.  
  
La aclaración de Juntos por el Cambio se fundamentó en que desde el Frente de Todos se insistía en que la principal bancada opositora se negaba a tratar la Ley de Etiquetado Frontal, cuando la mayoría de los diputados radicales había acompañado el dictamen de mayoría.  
  
Tras una serie de reuniones entre los principales referentes parlamentarios del Frente de Todos, de Juntos por el Cambio y del resto de los bloques minoritarios desarrolladas en los últimos diez días, finalmente se saldaron las diferencias que hicieron fracasar esa sesión y se acordó la realización de la del martes 26 al mediodía.  
  
Además de los tres temas incluidos en la fallida sesión, se acordó que la agenda que se llevará ese día al recinto incluirá la prórroga de la Ley Ovina y el proyecto enviado por el Senado que autoriza la entradas de tropas extranjeras al territorio nacional y la salida del mismo de fuerzas nacionales para participar en los ejercicios del programa de ejercitaciones combinadas a realizarse del 1° de septiembre de 2021 al 31 de agosto de 2022.  
  
Otros expedientes contemplados son el de alivio fiscal para fortalecer la salida económica y social de la pandemia generada por el Covid-19; la ratificación de la Convención Interamericana contra Toda Forma de Discriminación e Intolerancia, celebrada Guatemala en 2013; y el Régimen de Protección Integral del niño, niña y adolescentes con cáncer.  
  
En esta oportunidad, el pedido de sesión fue firmado por los presidentes de los bloques del Frente de Todos; Juntos por el Cambio; Consenso Federal, Córdoba Federal; Frente de la Concordia de Misiones; Juntos Somos Río Negro; Movimiento Popular Neuquino; Progresistas; Socialista y Acción Federal.  
  
En la solicitud sólo faltan las firmas de los bloques Justicialista, Unidad para el Desarrollo, Justicia Social de Tucumán y la Izquierda.  
  
Fuentes parlamentarias estimaron que la semana próxima se podría avanzar en los dictámenes pendientes de los proyectos que fueron incluidos en el temario, como el de alivio fiscal para fortalecer la salida económica y social de la pandemia, elaborado por el titular de la Cámara, el oficialista Sergio Massa.  
  
A su vez, fuentes parlamentarias confiaron a Télam que la del martes 26 sería la última sesión antes de las elecciones generales del 14 de noviembre, y que entre esa fecha y el 10 de diciembre (prórroga de sesiones ordinarias mediante), se desarrollaría una intensa actividad que incluiría, entre otros temas, el tratamiento del Presupuesto 2022.  
  
En la sesión del martes 26 también está prevista la jura de los diputados que reemplazarán al fallecido Eduardo Brizuela del Moral (Frente Cívico de Catamarca-Juntos por el Cambio) y a los renunciantes Facundo Moyano, Cristina Alvarez Rodríguez y Gabriela Cerruti, los tres del Frente de Todos.