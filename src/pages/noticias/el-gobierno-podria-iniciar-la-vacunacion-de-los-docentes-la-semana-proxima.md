---
category: Agenda Ciudadana
date: 2021-02-27T09:01:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/trotta.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: El Gobierno podría iniciar la vacunación de los docentes la semana próxima
title: El Gobierno podría iniciar la vacunación de los docentes la semana próxima
entradilla: Fue confirmado por el ministro de Educación, Nicolás Trotta, quién aseguró
  que el proceso será de manera escalonada, y habrá que afrontar el "desafío de la
  logística" que propone un operativo de esa envergadura.

---
El ministro de Educación, Nicolás Trotta, aseguró que el Gobierno pretende iniciar la semana próxima la vacunación de los docentes contra el coronavirus, de manera escalonada, y afrontar el "desafío de la logística" que propone un operativo de esa envergadura.  
  
En una conferencia de prensa luego del acuerdo paritario logrado con los gremios docentes, Trotta reclamó a las provincias la "supervisión constante de los protocolos" y que todos los distritos "asuman el desafío de cuidar la salud de los docentes".  
  
El titular de la cartera de Educación aseguró que el Gobierno nacional pretende iniciar el proceso de vacunación a todo el personal docente y no docente la "semana próxima de manera escalonada" a partir de observar cómo "responde la logística" para ese fin en las 24 jurisdicciones del país.  
  
"Queremos dejar en claro el mayor compromiso del Estado nacional en poder desplegar la vacuna, iniciándose por los maestros de la educación inicial, de la educación especial, el primer ciclo de la escuela primaria: primero, segundo y tercer grado, y los directivos de las escuelas", afirmó.  
  
Por otra parte, Trotta enfatizó que el plan de vacunación para los docentes no debe alterar el cuidado de los protocolos en las instituciones educativas del país, en el marco de la vuelta a la presencialidad.  
  
En ese sentido, remarcó que desde la Nación se "trabajará codo a codo" con las 24 jurisdicciones del país para que "todos asuman el desafío de cuidar la presencialidad y cuidar a nuestros docentes" en cada establecimiento educativo y agregó: "por eso la decisión del presidente Alberto Fernández de priorizar la vacunación de nuestros maestros y maestras".  
La partida de 904 mil vacunas Sinopharm producidas en China que llegó este jueves al país, comenzó a ser distribuida en todo el territorio nacional, mientras que el domingo arribarán otras 96 mil dosis para completar el millón acordado.  
El jefe de Gabinete, Santiago Cafiero, destacó que "la llegada de las 904 mil dosis de Sinopharm permitirá empezar a inmunizar al personal docente".  
El funcionario explicó a través de su cuenta de Twitter que el Gobierno busca "garantizar el mayor grado de presencialidad posible (en las escuelas), de forma segura y cuidada, y para lograrlo la vacunación es un paso muy importante".