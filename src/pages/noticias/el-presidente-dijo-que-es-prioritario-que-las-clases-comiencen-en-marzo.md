---
category: Agenda Ciudadana
date: 2021-01-17T10:31:44Z
thumbnail: https://assets.3dnoticias.com.ar/escuelas.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: LT10
resumen: El presidente dijo que "es prioritario" que las clases comiencen en marzo
title: El presidente dijo que "es prioritario" que las clases comiencen en marzo
entradilla: Alberto Fernández sostuvo que pidió al ministro de Educación, Nicolás
  Trotta, que "el plan de estudio sea revisado".

---
El presidente Alberto Fernández dijo que el dictado de clases es "prioritario" y aseguró que "en marzo" próximo se iniciarán las clases presenciales en las escuelas y será con "los cuidados del caso" por la pandemia de coronavirus.

"Nosotros hemos decidido que las clases vuelvan con los cuidados del caso, por eso estamos en condiciones de confirmar que en marzo las clases se iniciarán", afirmó el jefe de Estado en una entrevista con el sitio Data Clave.

En ese sentido, dijo que "le he pedido al ministro que el plan de estudios sea revisado para que en el transcurso de este año se puedan recuperar los contenidos que durante 2020 pudieron haber quedado soslayados producto de las restricciones".

El Presidente señaló que están "elaborando esa planificación", que presentarán "en los próximos días".

Asimismo, señaló que "el dictado de clases es para mi un aspecto prioritario en este año" y argumentó que "hay razones de desarrollo de los chicos que así lo exigen, pero también hay razones de desarrollo social que lo hacen imperativo".

"Perder un año de educación y conocimiento es muy grave para cualquier sociedad", destacó.

El primer mandatario manifestó, por otra parte, que tiene previsto que los docentes "reciban la vacuna junto al personal de seguridad, porque es esencial que ellos estén protegidos de los contagios".

En ese sentido, Fernández señaló que el objetivo "es minimizar en ellos cualquier posibilidad de que contraigan la enfermedad y que puedan trabajar tranquilos y seguros".

"Entiendo que a partir de esta cobertura de inmunización no debería haber ninguna oposición por parte de ellos", completó el Presidente en alusión a los condicionamientos manifestados por los gremios docentes para volver al dictado de clases presenciales.

Consultado sobre el pedido realizado días atrás por el expresidente Mauricio Macri para la vuelta a clases presenciales en las escuelas, el Presidente dijo que "el retorno a las aulas es una necesidad de toda la sociedad" y agregó que "muchos no lo entienden así. Convierten el problema en un acto de oportunismo electoral que se traduce en un mero discurso".

"Pero en realidad cuando se refieren de este modo, nos hacen recordar que son los mismos que cuando gobiernan descreen de la educación pública, maltratan a los docentes y solo han demostrado su desprecio por la educación reduciendo la inversión del Estado en esa área", destacó el Presidente.

Asimismo, sostuvo que "por eso debemos pensar en ello con los cuidados debidos para los alumnos y con las garantías sanitarias para los maestros y profesores".

El jueves último, Macri publicó una carta en su cuenta de la red social Facebook en la que pidió la apertura de las establecimientos educativos en el inicio del ciclo lectivo 2021, al fundamentar que "desde noviembre pasado la Organización Mundial de la Salud (OMS) defiende la necesidad de mantener las escuelas abiertas, porque se ha comprobado que los niños y adolescentes no son vectores principales de contagio".

"Vamos a perder el potencial de una generación de jóvenes si las aulas siguen vacías", había manifestado Macri en la misiva, días después de que el Gobierno nacional, a través del ministro de Educación, Nicolás Trotta, ratificara que el ciclo lectivo 2021 volverá a la presencialidad.

"La presencialidad debe volver a ser el ordenador de nuestro sistema educativo en 2021", indicó Trotta, y afirmó que "toda la Argentina, con los cuidados necesarios y los protocolos adecuados, está preparada para el comienzo de clases presenciales".