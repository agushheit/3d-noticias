---
category: La Ciudad
date: 2021-04-24T06:00:57-03:00
thumbnail: https://assets.3dnoticias.com.ar/palascasa.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Buen acatamiento a las restricciones de circulación nocturna en la ciudad
title: Buen acatamiento a las restricciones de circulación nocturna en la ciudad
entradilla: 'La Municipalidad notificó a bares sin distanciamiento entre mesas y se
  desactivó el viernes una fiesta clandestina en el centro. '

---
Este viernes por la noche entraron en vigencia las medidas dispuestas por el gobierno provincial, a las que adhirió la Municipalidad santafesina, para restringir la circulación nocturna en un intento por aplacar la segunda ola de contagios por Covid-19.

En ese sentido, el objetivo del área de Control y Convivencia del municipio era constatar el cumplimiento de los protocolos de funcionamiento dispuestos por el gobierno de la provincia para las distintas actividades. Para ello la gestión local cooperó con el Ministerio de Seguridad en diversas acciones que se concretaron desde el viernes a las 21 -primera jornada de restricciones- y recorrió negocios y bares de la ciudad.

Desde el municipio reconocieron que -en esta primera noche- hubo un alto acatamiento de los y las vecinas de Santa Fe, ya que luego de las 21 hubo muy pocos vehículos en la calle, que es uno de los principales requerimientos del gobierno de la provincia para evitar la circulación de personas en todo el territorio.

**Actuaciones**

A su vez, la Brigada de Nocturnidad del municipio recorrió distintos puntos de esta capital para hacer las correspondientes inspecciones. Al respecto, debieron hacer notificaciones a bares que no tenían el adecuado distanciamiento entre sus mesas y colaboraron con la Policía para desactivar una fiesta clandestina que se estaba realizando en 1º de Mayo al 3600.

El lugar fue desalojado y a la propietaria organizadora las fuerzas de seguridad de la provincia se la llevaron detenida, a la vez que se le dio intervención al Ministerio Público de la Acusación.

Además, fueron desplegados distintos controles de circulación por la ciudad. Uno de ellos se montó en bulevar Gálvez y Rivadavia, otros en la Costanera, en la zona del Faro y del Puente Colgante, y algunos controles móviles que fueron realizados según los reportes de movilidad vehicular, informó el Municipio.

Estos controles continuarán durante el resto del fin de semana y entre semana, para mantener las medidas de aislamiento preventivo en la ciudad.

Como se sabe, las medidas adoptadas por el municipio local tendrán vigencia hasta el 2 de mayo inclusive y van de la mano del decreto provincial que, en el mismo sentido, dio a conocer el miércoles por la noche el gobernador Omar Perotti.

Entre otras consideraciones, vale la pena recordar, que la circulación por la ciudad estará restringida entre las 21 y las 6 hasta esa fecha.

**El silencio abrumador**

Al igual que ya había ocurrido durante el período de aislamiento social preventivo y obligatorio (aspo) el año pasado, otra vez las calles volvieron a estar desiertas. Esta postal de una ciudad aislada por el coronavirus es un signo de época. Las avenidas sin automóviles, los bares vacíos y un silencio son una señal de que hay una sociedad tratando de cuidarse, para evitar nuevos contagios del virus que generó la pandemia y acecha por estas horas al sistema de atención de salud. Lo que se intenta hacer es evitar el colapso. Que nadie se quede sin atención ante una emergencia. Y para ello hay que quedarse en casa.