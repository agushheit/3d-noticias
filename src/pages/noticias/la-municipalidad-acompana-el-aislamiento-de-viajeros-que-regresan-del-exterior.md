---
category: La Ciudad
date: 2021-09-21T06:00:32-03:00
thumbnail: https://assets.3dnoticias.com.ar/COVIDEXTRAN.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Municipalidad acompaña el aislamiento de viajeros que regresan del exterior
title: La Municipalidad acompaña el aislamiento de viajeros que regresan del exterior
entradilla: 'Desde el mes pasado se concretó el seguimiento de cerca de 200 santafesinos
  y santafesinas que reingresaron a la ciudad. '

---
Desde el mes pasado, la Municipalidad lleva adelante un acompañamiento a los vecinos y vecinas de la capital provincial que regresan de un viaje al exterior. Desde entonces, y mediante una labor que lleva adelante la Dirección de Salud del municipio, ya se tomó contacto con cerca de 200 personas en esa situación.

La línea de trabajo surgió en acuerdo con el Gobierno de la provincia. Según explica el director de Salud municipal, César Pauloni, “inició, como la pandemia nos tiene acostumbrados, con la intención de armar herramientas en lo inmediato, por la urgencia de la situación”.

Para concretar la tarea, se armó un circuito a partir del cual se recibe el listado de personas que ingresan a la ciudad, provenientes del exterior, que son entre 10 y 20 por día en promedio. Posteriormente, desde la Municipalidad se realiza el primer contacto telefónico para conocer el estado de salud general y luego un segundo, en el cual se le otorga el turno para el hisopado. Una vez que estos pasos fueron realizados, sobreviene un seguimiento cada 48 horas para ver cómo están, si presentaron síntomas, si tuvieron algún vínculo, cómo atraviesan el aislamiento y si requieren asistencia.

Para el caso de un pasajero que refiera síntomas, el propio personal municipal a cargo del contacto se comunica con el 0800 de provincia para seguir los pasos pertinentes. Al pasajero se le refuerza el aislamiento y se lo convoca a concurrir a un centro de testeo inmediatamente para tener el diagnóstico cuánto antes y tomar las medidas de prevención acordes.

Todo el trabajo es concretado por personal de enfermería de la Dirección de Salud municipal que también se encarga de volcar la información a una base de datos. Estos números se incluyen en el análisis que regularmente concreta la Sala de Situación.

**Declaración jurada**

Pauloni explicó que “desde el mes pasado tenemos en camino esta línea de trabajo que consta de hacer un seguimiento y un acompañamiento a todas aquellas personas que regresan del extranjero y deben realizar el aislamiento en sus domicilios”. Según explicó, las direcciones físicas que se toman en cuenta son aquellas informadas en la Declaración Jurada firmada por cada pasajero en su ingreso al país, en los aeropuertos internacionales.

El director municipal señaló que el aislamiento se debe desarrollar por un lapso de 7 días, hasta realizar el hisopado PCR y que el mismo sea negativo. En ese sentido, aseguró que “el objetivo de esta línea de trabajo es detectar cuanto antes algún potencial caso de las nuevas variantes circulantes, de abordaje más complejo, por lo que hemos visto en el mundo”. Al respecto, observó que “en este momento, la variante Delta sería la más compleja y la que puede complicarnos en este momento que atravesamos en la ciudad de Santa Fe, con baja cantidad de casos”.

El funcionario comentó que el personal sanitario municipal a cargo del operativo se ocupa de recordar a los pasajeros cuáles son los cuidados que deben tener, los protocolos a respetar y las medidas a tomar en sus domicilios. Para un mejor asesoramiento, se solicita información sobre el núcleo familiar conviviente, los vínculos más cercanos y algunas características de su residencia.

Pauloni aseguró que, hasta el momento, con más de 150 casos analizados, no se detectó ninguno positivo. Consultado sobre el lugar del cual proviene la mayor cantidad de pasajeros, mencionó que Estados Unidos y algunos países europeos aparecen como los principales.

**Operativos de tránsito**

En otro orden, desde la secretaría de Control y Convivencia Ciudadana del municipio, se informó que la municipalidad de Santa Fe prestó apoyo a la provincia en más de 600 operativos, en el marco de la pandemia por Covid-19.

En ese sentido, se informó que las brigadas compuestas por inspectores de tránsito y agentes de la Guardia de Seguridad Institucional entre otros, continúan colaborando en los centros de vacunación y de testeo, dispuestos en diferentes puntos de la capital provincial.