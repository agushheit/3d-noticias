---
category: Deportes
date: 2021-06-12T06:00:13-03:00
thumbnail: https://assets.3dnoticias.com.ar/BASCKET.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: CABB
resumen: 'Hazaña en Puerto Rico: Argentina ganó con nueve jugadoras'
title: 'Hazaña en Puerto Rico: Argentina ganó con nueve jugadoras'
entradilla: En una actuación memorable que quedará en la historia del básquet nacional,
  el conjunto de Gregorio Martínez venció por 64 a 46 a República Dominicana en su
  debut en la AmeriCup.

---
Primero fue la épica y después la emoción. El debut argentino en la AmeriCup estuvo cargado de sentimientos en una tarde inolvidable para la Selección Mayor que conduce Gregorio Martínez. Después de la incertidumbre de las últimas horas y la tristeza por la baja de tres integrantes fundamentales de su estructura, el diezmado combinado albiceleste dio una demostración de carácter y dejó el corazón sobre el parquet para reponerse a las incontables adversidades, enfocarse en su presentación y vencer por 64-46 a República Dominicana en un triunfo fundamental para cimentar sus chances de meterse en la próxima ronda dado que cuatro de los cinco integrantes del grupo clasificarán los cuartos de final.  
  
Fue un triunfo colectivo, de la unión de un grupo que después de semanas de extenuante entrenamiento recibió un cachetazo inesperado con las bajas por coronavirus de su capitana Melisa Gretter, su subcapitana Agostina Burani y la desequilibrante Andrea Boquete. Las tres eran pilares, por experiencia, jerarquía y talento, en la rotación nacional. Entre el impacto de la noticia, la incertidumbre respecto a nuevos contagios y la dificultad de afrontar un duelo con apenas nueve jugadoras el escenario no era demasiado optimista pero Argentina entregó una producción conmovedora para derrotar con contundencia a su primer rival.  
  
Salvo por los primeros minutos de juego, Argentina dominó de principio a fin un partido en el que construyó una actuación memorable a partir de su asfixiante defensa. Dominicana apenas quedó limitada a 45 puntos únicamente con Elemy Caridad Colome por encima de la decena. Pese a la corta rotación con la que disponía Gregorio Martínez, la Selección no dosificó nunca sus esfuerzos y ejerció una presión asfixiante para dejar a su rival en bajísimos porcentajes de cancha (24.5%) como de tres puntos (22.2%).  
  
Argentina construyó su ofensiva a partir de su solidez. Corriendo la cancha, aprovechó el desconcierto en el retroceso dominicano y castigó gracias a la coral capacidad anotadora de un combinado que acumuló cuatro jugadoras por encima de la decena de puntos. El elenco de Martínez ganó la batalla de los rebotes (49 a 38) y plasmó su superioridad en ataque a partir del pase extra: las albicelestes finalizaron con 19 asistencias frente a las ocho del conjunto caribeño.  
  
Más allá de los destacados rendimientos individuales, fue una jornada que el básquet argentino atesorará por los obstáculos sorteados. Cada una de las jugadoras, cada uno de los integrantes del cuerpo técnico, desempeñó un rol trascendental en la presentación de una Argentina que ahora sueña con repetir este sábado frente a Puerto Rico.  
  
Fue un triunfo con sabor a desahogo para todas pero fue aún más especial para Macarena Rosset, quien hasta tres horas antes no sabía si iba a poder jugar: después de recibir la autorización casi a la hora del partido, fue la goleadora del trámite con 14 puntos además de 6 asistencias y 5 rebotes. La joven Camila Suárez, en su debut absoluto con la camiseta celeste y blanca, se hizo cargo de la base tras la salida de Gretter y redondeó un partidazo con 13 puntos, 5 asistencias, 4 rebotes y 3 robos.  
  
Después de haber acumulado dos faltas en los primeros minutos, Julieta Mungo apareció en todo su esplendor durante el último parcial para frenar la arremetida de una República Dominicana que amenazaba con la remontada. Victoria Llorente, con 12 puntos y 7 rebotes, fue puro sacrificio más allá de su goleo. Cada una de ellas interpretó su rol a la perfección en ambos costados de la cancha, con el corazón caliente pero la cabeza fría para cumplir con inteligencia y disciplina el plan trazado por Gregorio Martínez. Agustina García, Victoria Gauna, Sol Castro, Julieta Ale y Diana Cabrera -máxima rebotera con 12 capturas- fueron decisivas en defensa.  
  
También merece reconocimiento el entrenador y su equipo de trabajo, quienes fueron capaces de diseñar un plan que funcionó aún pese a la situación crítica que representó afrontar un encuentro de primer nivel con un reducido grupo de nueve jugadoras. Más allá de los méritos dentro de la cancha, su tarea para sostener al plantel y evitar que la frustración impactara en sus jugadoras fue trascendental para que Argentina pudiera quedarse con el primer triunfo en una tarde para la emoción.