---
category: Agenda Ciudadana
date: 2021-12-12T06:15:40-03:00
thumbnail: https://assets.3dnoticias.com.ar/MARIUVIDAL.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: María Eugenia Vidal arremetió contra el Gobierno
title: María Eugenia Vidal arremetió contra el Gobierno
entradilla: '"La política está tan alejada de la realidad, festejan en Plaza de Mayo,
  discuten reelección indefinida de intendentes, candidaturas para 2023. Eso no está
  en la agenda de la gente", aseguró la diputada nacional.

'

---
La diputada nacional de Juntos por el Cambio en la Ciudad María Eugenia Vidal apuntó contra el Gobierno por el acto de este viernes en Plaza de Mayo y afirmó que "la oposición nunca fue llamada al diálogo" por el oficialismo.

"La vicepresidenta ayer atacó a los jueces, a la oposición y a los medios", se quejó la ex gobernadora de Buenos Aires, al hacer referencia al discurso de Cristina Kirchner en Plaza de Mayo en medio del Festival por la Democracia.

Según resaltó, "la oposición nunca fue llamada al diálogo" por el gobierno del presidente Alberto Fernández durante los dos primeros años de mandato.

Vidal también cuestionó la realización del acto porque "costó más de 40 millones de pesos" y evaluó que "es una mala señal que no se haya incluido a los que no piensan como el Gobierno".

La ex mandataria bonaerense también rechazó los dichos de Cristina Kirchner y consideró que "no hubo fuga porque los capitales se van del país frente a la falta de confianza" y, en ese sentido, remarcó que "endeudamiento e inflación son hijos del déficit".

"La política está tan alejada de la realidad, festejan en Plaza de Mayo, discuten reelección indefinida de intendentes, candidaturas para 2023. Eso no está en la agenda de la gente", indicó.

En declaraciones radiales, la diputada nacional sostuvo que lo de Cristina Kirchner "más que apelaciones a la oposición hubo descalificaciones, como lo que hizo con la Unión Cívica Radical"

"Ayer hubo discursos, no realidad", sentenció la flamante legisladora, que detalló que está "trabajando en proyectos como la derogación de la ley de alquileres o reformas educativas".

Sobre el acuerdo que está negociando el Gobierno con el Fondo Monetario Internacional (FMI), Vidal señaló: "Las personas que trabajamos en la oposición somos responsables y sabemos que hay que acordar con el Fondo".

"Esperemos que el lunes el ministro de Economía nos de detalles de la negociación con el FMI", remarcó, y afirmó que "este Gobierno también se ha endeudado".