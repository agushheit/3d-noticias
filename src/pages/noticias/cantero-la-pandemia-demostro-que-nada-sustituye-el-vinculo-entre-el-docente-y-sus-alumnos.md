---
category: Estado Real
date: 2021-01-17T10:17:46Z
thumbnail: https://assets.3dnoticias.com.ar/cantero.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: "\t  Cantero: “La pandemia demostró que nada sustituye el vínculo entre el
  docente y sus alumnos”"
title: "\t  Cantero: “La pandemia demostró que nada sustituye el vínculo entre el
  docente y sus alumnos”"
entradilla: La ministra de Educación se refirió al trabajo que realiza la provincia
  para volver a la presencialidad y a los preparativos para la campaña de vacunación
  masiva a docentes y asistentes escolares.

---
La ministra de Educación, Adriana Cantero, se refirió al trabajo que realiza la provincia para volver a la presencialidad y a los preparativos para la campaña de vacunación masiva a docentes y asistentes escolares.

En primer lugar, expresó que “trabajamos por la vuelta a la presencialidad con un criterio de flexibilidad que permita abrir y cerrar los dispositivos conforme al dinamismo que el despliegue de esta pandemia nos vaya indicando como necesidad. El objetivo es lograr un formato que combine presencialidad con trabajo en casa y que podamos trabajar en pequeños grupos, con distanciamiento social y con los protocolos de cuidado”.

“El regreso a la presencialidad en las escuelas es una meta largamente esperada, ansiada por todos los que integramos los sistemas educativos”. 

En ese sentido, Cantero manifestó que “este año de pandemia nos ha demostrado que nada sustituye a ese vínculo humanizante y humanizador de la presencia e interacción de un educador con sus alumnos”.

“También sabemos que el cuidado de la vida es esencial y, por lo tanto, la mirada sobre esa prioridad también tiene que jugarse en las decisiones que hay que tomar en su momento”, aclaró la ministra.

Al respecto, recordó que “en Santa Fe desplegamos una línea que se llama Escuelas Seguras por la cual trabajamos en la adecuación de los edificios, desde el punto de vista sanitario y en el control de todos los canales de vacunación de todos los alumnos, para que tengan el resguardo de estar con todas las vacunas que preservan también de otras enfermedades estacionales”.

**VACUNACIÓN MASIVA A DOCENTES**

“Estamos determinando los centros de vacunación para que, ni bien lleguen las dosis a la provincia, podamos iniciar esa gran campaña de vacunación”, expresó Cantero y luego añadió que “seguramente habrá un tiempo donde estemos trabajando con los modelos presenciales con alternancia mientras se va desplegando el proceso de vacunación”.

**CICLO LECTIVO 2021**

Por último, la ministra mencionó que “el 17 de febrero se reintegra el personal docente a las escuelas y, a partir de ese momento, vamos a trabajar con los grupos prioritarios (7º grado de Primaria, 5º de Secundaria, 6º de Técnica) en un período de intensificación pedagógica, hasta el 10 de marzo, complementando y asegurando las competencias básicas del nivel”.

“A partir del 15 de marzo se inicia el ciclo lectivo 2021 para todos los niveles y modalidades y esperamos que en ese momento la curva epidemiológica en Santa Fe nos permita recuperar presencialidad con el formato bimodal en todo el territorio provincial”, finalizó la ministra.