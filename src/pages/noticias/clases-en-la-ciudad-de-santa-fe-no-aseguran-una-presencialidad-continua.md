---
category: Agenda Ciudadana
date: 2021-03-04T00:53:57-03:00
thumbnail: https://assets.3dnoticias.com.ar/clases.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Diario Uno
resumen: 'Clases: en la ciudad de Santa Fe, no aseguran una presencialidad continua'
title: 'Clases: en la ciudad de Santa Fe, no aseguran una presencialidad continua'
entradilla: Así lo manifestó hoy la ministra de Educación Adriana Cantero. "Las clases
  marchan hacia un sistema bimodal", dijo

---
A menos de dos semanas del arranque formal del ciclo lectivo 2021 en la provincia y frente al reclamo de padres para que vuelvan las clases presenciales plenas, la ministra de Educación provincial, Adriana Cantero, reconoció que “será difícil” que en las escuelas de las grandes ciudades como Santa Fe o Rosario se vuelva a la modalidad previa a la pandemia de presencialidad, y ratificó que todo marcha hacia un sistema bimodal o flexible que combine trabajo a distancia con actividades en los establecimientos.

“Para poner en funcionamiento un sistema tan grande y complejo como el educativo, tenemos que ser muy cuidadosos con los criterios sanitarios. El cuidado de la vida y de la salud está primero”, señaló la titular de la cartera educativa.

En declaraciones a LT8 de Rosario, Adriana Cantero señaló que los protocolos y la organización aprobados para el sector educativo “contempla tres alternativas: la virtualidad o trabajo a distancia, la presencialidad y la bimodalidad", y explicó: "Esos tres modelos pueden coexistir en distintos momentos conforme a las condiciones que garanticen el cuidado de la salud con protocolos”.

Citó como ejemplo: “El año pasado hubo escuelas en localidades del centro-norte de la provincia que tuvieron presencialidad plena porque había pocos alumnos y contaban con espacios amplios y bien ventilados para mantener el distanciamiento y cumplir con el resto de los protocolos. Mientras tanto, otras escuelas en poblaciones vecinas que no contaban con esas condiciones, trabajaron con la bimodalidad. Y en otros lugares directamente se cerraban las escuelas por la curva epidemiológica y trabajaron en la distancia”.

**Prevención por el Covid-19**

“Esa flexibilidad del sistema educativo está descrita en documentos nacionales y forma parte del nuevo escenario nacional. Esto no tiene que pedirlo la comunidad. Eso lo planifica el sistema educativo, que tiene su propio funcionamiento. No es un manual a la carta”, señaló la ministra de Educación, en relación con el reclamo que grupos de padres de alumnos realizaron en los últimos días para que vuelvan las clases presenciales plenas.

Cantero estimó que en las escuelas de las grandes ciudades será “muy difícil” mantener las medidas de distanciamiento. “Igualmente estamos muy atentos al análisis de todas las alternativas. Los grandes conglomerados ofrecen dificultades para garantizar una de las medidas de prevención más importantes contra el Covid-19. Por eso, el modelo organizativo de todo el país tiene contemplado la bimodalidad, es decir dividir los grupos. Así se asegurará presencialidad para todos, pero con frecuencia o alternancia”, concluyó.