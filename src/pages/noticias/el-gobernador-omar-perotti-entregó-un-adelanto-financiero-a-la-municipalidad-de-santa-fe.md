---
layout: Noticia con imagen
author: .
resumen: Ley de Emergencia Económica
category: La Ciudad
title: El gobernador Omar Perotti entregó un adelanto financiero a la
  municipalidad de Santa Fe
entradilla: Fue este viernes, en el marco de la Ley de Emergencia Económica, que
  habilita a el Ejecutivo santafesino a refinanciar deudas y adelantar aportes.
date: 2020-11-06T20:02:00.000Z
thumbnail: https://assets.3dnoticias.com.ar/rexumen-noviembre.jpg
---
El gobernador Omar Perotti entregó este viernes, en la ciudad capital, un adelanto financiero de más de 60 millones de pesos al intendente de Santa Fe, Emilio Jatón, en el marco de la Ley de Emergencia Económica, que habilita al municipio santafesino a refinanciar deudas y adelantar aportes solicitados oportunamente por los gobiernos locales.

Al término de la entrega, el gobernador Perotti manifestó que “tuvimos no sólo una reunión de trabajo sino, también, formalizamos la entrega de recursos por más de 60 millones de pesos para la municipalidad de Santa Fe”.

“El gobierno provincial financia y acompaña a los municipios y comunas para ponernos al día respecto de todas las deudas que había desde 2017 y hasta 2019 en relación con obras menores, otro tipo de transferencias y de la ley que habilita a realizar anticipos de coparticipación”, explicó el mandatario santafesino.

“Son momentos duros, de caída de la recaudación, por lo cual, las actividades y servicios a prestar al vecino las realizan mayoritariamente los municipios y las comunas. De ahí surge la decisión de ir cumpliendo con cada uno de los gobiernos locales para que los servicios de cercanía puedan desarrollarse de la mejor manera”, afirmó el titular del Ejecutivo provincial.

A su turno, Jatón indicó los fondos recibidos provienen de la “Ley de Adelanto de Coparticipación, por lo cual vamos a devolver ese dinero. Con el gobernador veníamos trabajando esto desde hace un tiempo. Estos recursos permitirán cubrir muchas necesidades que tiene el municipio en el día a día para la prestación de servicios básicos”, anticipó.

**SEGUIMOS CUIDÁNDONOS**

Respecto del Covid-19, el gobernador insistió: “Los cuidados no los podemos abandonar y hasta aquí depende de cada uno de nosotros, fundamentalmente en el día a día. El Estado ha desplegado todo su potencial de infraestructura, de equipamiento, el sistema sanitario con cada uno de los médicos y enfermeros puestos a disposición y al máximo de lo que se pueden expandir nuestras camas críticas, vinculando el trabajo entre nación - provincia – municipios y comunas”.

Para finalizar, Perotti recalcó que “el resto lo pone cada uno y lo máximo que cada uno está poniendo, que es mucho, creo que es lo que nos va a dar la posibilidad cierta de tener una buena Navidad y un buen fin de año”.