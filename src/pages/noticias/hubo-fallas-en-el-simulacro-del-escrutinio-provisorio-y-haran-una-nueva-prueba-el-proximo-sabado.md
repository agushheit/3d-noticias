---
category: Agenda Ciudadana
date: 2021-08-22T06:15:04-03:00
thumbnail: https://assets.3dnoticias.com.ar/ESCRUTINIOS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Hubo fallas en el simulacro del escrutinio provisorio y harán una nueva prueba
  el próximo sábado
title: Hubo fallas en el simulacro del escrutinio provisorio y harán una nueva prueba
  el próximo sábado
entradilla: " Se analizó el despliegue de personal, la digitalización y transmisión
  de telegramas, así como la difusión de los datos, ítem en el que, advierten, el
  12 de septiembre podría haber demoras."

---
El Gobierno realizó hoy un simulacro electoral general para probar el sistema del escrutinio provisorio que se utilizará en las primarias del 12 de septiembre, que será repetido dentro de siete días.

En ese marco, anticipó que la difusión de datos podría demorarse, debido a que el cumplimiento del protocolo ante la pandemia haría que la jornada de votación se extienda pasadas las 18:00 y eso provocaría que el cierre de mesas, recuento de votos, elaboración del telegrama, su digitalización y transmisión se haga más tarde de lo normal.

Asimismo, este sábado se exigió al sistema al máximo y hubo demoras, por lo que en una semana se realizará una prueba similar.

La prueba fue hecha por la Dirección Nacional Electoral (DINE) en la sede central del Correo, aunque también se analizó el despliegue de personal, la digitalización y transmisión de telegramas, así como la difusión de los datos.

Este simulacro general electoral fue el mayor operativo de despliegue técnico y logístico previo a las elecciones Primarias Abiertas, Simultáneas y Obligatorias (PASO) del 12 de septiembre, ya que implicó la movilización de más de 20.000 personas abocadas al proceso (13.663 operadores de transmisión; 2.456 supervisores; 6.400 trabajadores de Correo que brindarán soporte técnico, administrativo y de logística).

Además, durante la jornada, 1.170 personas participaron como digitadores de los telegramas que fueron cargados en el sistema de recuento provisorio, así como también hubo más de 2.000 apoderados y fiscales informáticos de todas las fuerzas políticas.

El operativo también involucró el despliegue de 3.826 vehículos para el traslado de kits y operadores de transmisión a los establecimientos.

En línea con los acordado entre el Ministerio del Interior y la Cámara Nacional Electoral, durante este simulacro de alcance nacional se aplicaron los protocolos de prevención y protección sanitarias en el marco de la actual situación provocada por el Covid-19, con distribución de elementos de cuidado para todo el personal involucrado, así como insumos y material de protocolo preventivo en cada sede de Correo.

Asimismo, se probó la carga de datos de la totalidad de los telegramas en el sistema de recuento provisorio de resultados a cargo de la DINE, que se instalaron para su operación en dos centros de cómputos con instalación redundante en las sedes de Correo en el barrio porteño de Barracas, y en Monte Grande, provincia de Buenos Aires.

En el lugar estuvieron la titular de la DINE, Diana Quiodo, y la secretaria de Asuntos Políticos, Patricia García Blanco.

"La pandemia modificó todo el trabajo del proceso electoral. La idea de hoy era simular lo más cercano a una situación real de elección", señaló Quiodo en declaraciones a la prensa.

De todos modos, la funcionaria nacional subrayó que en el día de los comicios podrá cambiar la situación, por lo que evitó dar precisiones sobre el horario en que habrá una tendencia definitiva en los porcentajes de votos.

De acuerdo a la legislación vigente, recién tres horas después de la finalización de los comicios, es decir a las 21:00, se pueden comenzar a difundir datos del escrutinio provisorio.

"Es probable que la jornada de votación se extienda más allá de las 18:00, por las filas, el distanciamiento", lo que demorará el cierre de las mesas, el recuento de votos y la transmisión y carga de telegramas, explicó la titular de la DINE.

Por su parte, García Blanco destacó la participación de fiscales y apoderados de las alianzas y remarcó que en la semana se analizarán con ellos los resultados del simulacro.

El simulacro es el último paso antes de las elecciones primarias, que tuvo un capítulo previo el 7 de agosto: en esa ocasión, se había llevado a cabo una prueba de recuento de votos en la que se transmitieron más de 100 mil telegramas (un volumen similar al de las PASO), mientras que para el recuento de resultados se digitalizaron más de 6.000 telegramas con configuraciones de candidaturas y cargos diferentes de Chaco, La Pampa, San Luis, Santa Cruz y Tierra del Fuego.

El próximo sábado se repetirá la prueba del sistema, para terminar de corroborar su buen funcionamiento.