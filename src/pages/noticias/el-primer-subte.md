---
category: Agenda Ciudadana
date: 2022-12-01T09:55:57-03:00
thumbnail: https://assets.3dnoticias.com.ar/subte.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Ricardo Miguel Fessia
resumen: El primer subte.
title: El primer subte.
entradilla: A las 15,25 horas del lunes 1 de diciembre de 1913 partió oficialmente
  la primera formación de la línea “A” de subtes de Buenos Aires desde la estación
  ubicada Av. de Mayo 575

---
I – A las 15,25 horas del lunes 1 de diciembre de 1913 partió oficialmente la primera formación de la línea “A” de subtes de Buenos Aires desde la estación ubicada Av. de Mayo 575, justo enfrente al imponente edificio de estilo Beaux Arts de París inaugurado en 1898 para que funcione el diario “La Prensa”. Eran sus pasajeros el vice presidente de la Nación, Victorino de la Plaza y el intendente de la ciudad, Joaquín Samuel del Corazón de Jesús de Anchorena. También había ministros del gabinete nacional y funcionarios de la municipalidad.

Era el primer servicio en toda Latinoamérica y solamente superado, en el continente, por el subte de Nueva York, que funcionaba desde 1904.

II – En una ciudad que crecía a ritmo sostenido, hacia fines de la década de 1890 se comenzó un nuevo proceso de expansión de la red de comunicación con el planeamiento de cuatro nuevas líneas. Los debates sobre la necesidad de construir un sistema de transportes subterráneos en Buenos Aires comienzan a fines del siglo XIX, en directa relación con el sistema de tranvías. Estos operaban desde 1870, pero a poco de ello comenzaron a mostrar signos de colapso por la cantidad de personas que lo utilizaban. A ello había que anotar que los medios estaban en manos de empresas que monopolizaron el servicio y por lo tanto no estaban tan dispuestas a las inversiones.

Algunos proyectos se ofrecen para instalar un servicio de transporte subterráneo. El primero es en 1886 para cubrir el trayecto desde la Estación Central del Ferrocarril, ubicada en las inmediaciones de la actual Casa Rosada y la Plaza Once. Poco después, en 1889, Ricardo Norton presentó un proyecto para construir un ferrocarril soterrado entre la Estación Central hasta Plaza Lorea, y otra vía desde ahí hasta Once, tomando a su cargo los gastos y con una concesión a perpetuidad. El otro proyecto pretendía vincular Plaza Constitución con la intersección de Lima y Avenida de Mayo. Eran proyectos más desarrollados, con doble vía y servicio de electrificación.

Ese mismo año el empresario Barrabino, presentó un proyecto similar pero ante el Concejo Deliberante, para construir una nueva línea de tranvía que en algunos tramos circulaba en forma subterránea. Respecto a este proyecto se presentó un tema de competencia ya que intervino el Ministerio del Interior que se opuso a que la ciudad pueda concesionar el subsuelo.

En 1894 el Estado nacional decidió la construcción de un edificio para alojar al Congreso nacional y para ello se escogió el solar de Rivadavia y Entre Ríos. El nuevo emplazamiento debía estar acompañado de toda una estructura de comunicación de una importante avenida y se creyó que un transporte público directo facilitaría el desplazamiento.

Para 1909, el Concejo Deliberante de Buenos Aires aprobó el contrato entre el intendente Manuel Güiraldes y la Compañía de Tranvías Anglo Argentina para que esta construyera y explotara por ochenta años tres líneas de subterráneos: de Plaza de Mayo a Primera Junta parte de la actual Línea A, de Constitución a Retiro actual Línea C y de Plaza de Mayo a Palermo parte de la actual Línea D. Pero por distintas circunstancias, la empresa solamente pudo construir la que hoy es Línea A.

Era cada vez más urgente solucionar el transporte en la ciudad que avanzaba. En 1903 había 895.000 habitantes y 1913 1.457.000.

III - Existían cuestiones de jurisdicción presente ya que en 1909 el Congreso sancionó el 29 de septiembre la ley 6700, autorizando al Ferrocarril del Oeste a extenderse bajo tierra hasta el puerto de Buenos Aires, en tanto que la Municipalidad de Buenos Aires aprobaba una ordenanza que permitía a la Compañía de Tranvías Anglo-Argentina construir y explotar una red de subterráneos que, en una primera etapa, uniría Plaza de Mayo con Primera Junta.

Las diferencias entre las dos empresas tuvieron arreglos ya que ambas eras del mismo origen. El Ferrocarril del Oeste construiría un túnel de una sola vía hasta el puerto, para ser usado exclusivamente para trenes de carga, y a una profundidad de 15 metros.

En la intersección de Rivadavia y Sánchez de Bustamante, se encuentra la entrada a un túnel de cinco kilómetros de largo que termina en la avenida Madero y la calle Sarmiento, cercano al dique 3 de Puerto Madero. Fue excavado a pico y pala, con el único auxilio de un tren que se llevaba la tierra. Inaugurado en 1916, a mediados de los cuarenta se intentó usarlo como transporte de pasajeros, aunque no prosperó, ya que una sola vía dificultaba el esquema de los viajes de ida y vuelta.

A la Anglo-Argentina le tocaría diseñar –por encima de ese túnel- otro de doble vía destinado al transporte de pasajeros. El proyecto incluía una estación de transferencia en Once de Septiembre, que funcionaría como empalme de ambos servicios. De esta manera, los usuarios del tren podrían hacer un rápido transbordo al subterráneo para llegar al centro de la ciudad.

IV - El viernes 15 de septiembre de 1911 comenzaron las obras en la esquina de las calles Hipólito Yrigoyen y Balcarce. Ahí, con la presencia del presidente Roque Sáenz Peña, del intendente municipal Joaquín de Anchorena y de los ministros de Interior y de Guerra se realizó el primer pozo, sacando cada uno una palada simbólica de tierra.

De la excavación se retiraron miles de toneladas de tierra que se usaron para rellenar los terrenos linderos al Cementerio de Flores y la avenida Vélez Sarsfield. Se emplearon 1500 operarios para construir un túnel a doble vía de siete kilómetros de largo, 31 millones de ladrillos, 108.000 barricas de 170 kilos de cemento, 13 mil toneladas de tirantes de hierro y 90 mil metros cuadrados de capa aisladora.

En cada una de las estaciones había una hilera de azulejos de un color determinado, que permitía a los pasajeros que no supieran leer identificar sin problemas las estaciones. Los trabajos se extendieron por más de tres años y un desmoronamiento se llevó la vida de seis obreros.

V - El 1 de diciembre de 1913 en el primer tren que partió, engalanado para la ocasión, fueron el vicepresidente de la República, Victorino de la Plaza, en representación del presidente Roque Sáenz Peña, ya enfermo. Estuvo acompañado por el intendente municipal Joaquín de Anchorena, el presidente de la Compañía de Tranvías Anglo Argentina, Samuel Hale Pearson, ministros nacionales, concejales, y miembros de la iglesia. Detrás, partieron dos formaciones más con invitados especiales.

La formación se detuvo en la estación Congreso, donde las autoridades descendieron para recorrer las instalaciones. Cuando arribaron a la estación Once de Septiembre, decorada con banderas, fue el turno de los discursos. El intendente como dueño de casa, se regodeó de la “victoria del trabajo en una de sus manifestaciones más simpáticas”.

VI - Por ese año tenían subtes Londres, desde 1863; Atenas desde 1869: Estambul desde 1874; Glasgow desde 1897: Viena desde 1898; París desde 1900; Boston desde 1901, Berlín desde 1902; Nueva York desde 1904; Filadelfia desde 1907 y Hamburgo desde 1912.

La referencia pretende entregar algunos insumos para los que se preguntan dónde nos perdimos la historia.

El día 2 de diciembre, el primero del servicio, viajaron 147.457 personas. Tal fue la novedad que desde el interior llegaba gente para poder sentir la sensación de viajar de esa forma. La frecuencia que separaba una formación de otra era de tres minutos. El crecimiento y la demandan eran tal que ocho meses después, el 1 de julio del año siguiente, el subte llegaba a Caballito.

Durante muchos años, el servicio de esta primera línea de subterráneos estuvo compuesto por un total de 50 coches de procedencia belga. Se los conoció como “las brujas”, ya que la fábrica La Brugeoise et Nicaise et Delcuve funcionaba en dicha ciudad. Cada uno tenía un valor de 50 mil pesos moneda nacional y los primeros modelos estuvieron diseñados para funcionar tanto como vagones de subterráneo como tranvías. Hasta que la red fue ampliada, cuando el tren subterráneo llegaba a la última estación, se desenganchaban dos vagones y continuaban circulando por la superficie.

Recuerdo muy bien esos “vagones de madera” que mostraba la esbeltez de su tiempo en par con el paso de los años. Cada vista a Buenos Aires, por los años setenta, un viajecito en subte era parte del recorrido porteño, cualquiera sea el motivo de la visita.

Recién para el año 2013 fueron sacados de circulación los tradicionales vagones que circulaban con bamboleo con sus detalles de bronce y tulipas que fueron el símbolo de un tiempo de progreso.

Llegaron otros tiempos, menos venturosos, los de la verdadera década infame donde el intendente porteño Carlos Grosso tomó la decisión de ceder al Ministerio de Economía de la Nación la red de subterráneos para incluirlos en el proceso de privatización ferroviaria. A la luz del decreto 2074/90, firmado el 3 de octubre de 1990, la concesión de explotación de los servicios del subte se concesionaron por veinte años con la firma del presidente de esos tristes años. Se les otorgó al consorcio formado por Benito Roggio e hijos S.A., Cometrans S.A., Burlington Northern RR. Co., Morrison Knudsen Corporation Inc. y S.K.F. SACCIFA., quienes formarían la empresa Metrovías S.A.

CABA, 1 de diciembre de 2022.