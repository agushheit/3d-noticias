---
category: La Ciudad
date: 2021-11-12T06:00:13-03:00
thumbnail: https://assets.3dnoticias.com.ar/circunvalacion.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'Baches, basura, piedrazos y oscuridad: circunvalar Santa Fe, un viaje de
  terror'
title: 'Baches, basura, piedrazos y oscuridad: circunvalar Santa Fe, un viaje de terror'
entradilla: Las quejas de los usuarios de la Circunvalación Oeste llegan desde todos
  los sectores. Faltan banquinas, se roban los cables de iluminación y los ataques
  son cada vez más frecuentes.

---
La Circunvalación Oeste de la ciudad de Santa Fe parece tierra de nadie. Los choferes de transportes de carga advierten que los ataques con piedrazos y otros objetos que ocurren son al menos uno por día. Lo mismo les sucede a los choferes de micros y a los automovilistas particulares. Pero los problemas no terminan allí. Empiezan.

Transitar los aproximadamente 20 kilómetros de distancia de circunvalación de la ciudad que van desde la zona del Puerto de Santa Fe, pasando por todo el noroeste hasta la altura del acceso al Mercado de Abasto, es una ruta imposible. O, al menos, de mucho riesgo vial. Es que además de los ataques con fines delictivos, la ruta (que no termina allí, sino que continúa al norte hasta la localidad de Candioti) no tiene banquinas y en algunos tramos las huellas de los vehículos son una verdadera trampa vial para quienes precisan detener la marcha a un costado.

La Circunvalación Oeste es parte de la traza de la ruta nacional 11 (que antes pasaba por avenida B. Parera). Por ello, las obras de mantenimiento y posibles mejoras dependen de Vialidad Nacional, más allá de tratarse de la circunvalación de la ciudad.

**Viajar de noche**

Otro gran problema es viajar de noche por la zona. Una boca de lobos. Hay muchos tramos de circunvalación a oscuras. Ya sea por desperfectos eléctricos como también producto del vandalismo y robo de los elementos de iluminación, sobre todo los cables. Y cada vez que la iluminación se repone, al poco tiempo vuelve a ser vandalizada, y así reiteradamente. De nunca acabar.

A todo ello hay que sumarle la desidia de muchos vecinos de la ciudad que eligen las banquinas de la Circunvalación Oeste para descartar allí los residuos de todo tipo. Ramas, electrodomésticos, residuos húmedos y escombros que "les sobran" en sus hogares. Algo que resulta incomprensible.

Y un problema más son los precarios asentamientos que avanzan en distintas zonas del cordón oeste de la ciudad hacia la circunvalación, sobre todo a la altura del acceso por calle Mendoza, al fondo del barrio Santa Rosa de Lima. La presencia de niñas y niños que habitan esas viviendas en la zona son un riesgo para todos. Principalmente para ellos. Este es un problema social que excede a la seguridad vial. Y que tiene como prioridad la necesidad de mejoras en la vida cotidiana de esas personas. La inclusión. Una demanda imperiosa a resolver.

**Cansados y sin respuestas**

Desde la coordinación de las cámaras de transportistas de cargas de la provincia, Mario Ramello advierte que "la situación se agravó en el último año". Y argumenta en ese sentido que "los ataques a camiones que circulan por la Circunvalación Oeste ocurren ahora una vez por día". También agrega que "ya no son piedrazos, nos tiran de todo: palos, adoquines, lo que sea, con tal de que el chofer frene para robarle".

Lo mismo les ocurre a quienes viven en Santo Tomé, sobre todo en la zona de los countries, quienes deben viajar varias veces la día a Santa Fe y son blanco de los ataques con piedras. La semana pasada me estallaron el parabrisas del auto", dice Martín, un vecino de Santo Tomé. "Esto no da para más. Era al atardecer, todavía había luz del día y me atacaron a la altura del CARD", cuenta. "Ahora prefiero perder más tiempo atravesando la ciudad, pero a la circunvalación no vuelvo".

Los camioneros tienen bien en claro cuáles son los puntos críticos a donde se repiten los ataques: el "rulo" de Cilsa (cruce con el puente Carretero), el acceso por calle Mendoza, el Hipódromo, el Mercado de Abasto y llegando a Recreo. "El tema es muy preocupante", dice Ramello. "Se han hecho los reclamos correspondientes a las autoridades de Seguridad, al intendente, y la respuesta siempre es la misma: presenten las denuncias, para que podamos tomar otro tipo de medidas. Pero allí se plantea otro problema: uno es que a veces los choferes tienen que ir hasta la Comisaría, y otro es que muchas veces no quieren perder el tiempo".

**Competencias**

"Entendemos que no hay patrulleros suficientes para controlar la seguridad sobre la Circunvalación, entendemos que hay un gris respecto de la jurisdicción, de a quién le corresponde controlar esa zona, que es nacional pero a partir de tal punto pasa a la provincia, que le corresponde al municipio, y en realidad patean el problema para adelante sin buscarle una solución concreta", se queja Ramello, y advierte: "Esperemos que esto no pase a mayores, porque los dos últimos ataques fueron muy graves" (un camión brasilero que tuvo que cruzarse a la otra mano y un chofer al que deben operar por una fractura de mandíbula).

Lo cierto es que el mantenimiento depende de Vialidad Nacional. Desde ese organismo dicen que la iluminación depende de la Municipalidad de Santa Fe. Y la Municipalidad les devuelve la pelota. A su vez, la Nación firmó recientemente un convenio con la Provincia para que Vialidad Nacional realice obras de recuperación del sistema de iluminación actual entre el "rulo" de Cilsa y el Hipódromo (por $ 5 mil millones); al tiempo que la Provincia realizará otras obras de mejoras de barandas, calzadas y la iluminación hasta el cruce con la ruta 70.

**Mantenimiento**

Mientras tanto, desde fines de octubre Vialidad Nacional viene realizando trabajos de perfilado de banquinas en la zona que va desde el Hipódromo Las Flores hasta el cruce con Avenida Teniente Loza, en el Mercado de Abasto. El despeje de los bordes de la calzada forma parte de las intervenciones para reforzar la demarcación horizontal de la doble vía de circulación, con el pintado posterior de las líneas correspondientes sobre la traza.

A propósito de estas tareas, la semana pasada dos operarios fueron víctimas de un incidente de tránsito. Los mismos pertenecen a la empresa RAVA SA que mantiene el corredor en la zona bajo un contrato con el organismo. "Las tareas se desarrollaban con el debido operativo de seguridad y advertencia a los conductores, con cartelería preventiva y conos de seguridad, en tanto los operarios trabajaban en el corte de pasto sobre el cantero central, fuera de la calzada", detalló el organismo.

"Aparentemente, por una mala maniobra el conductor del rodado que produjo el siniestro invadió la zona del cantero central e impactó con uno de los operarios, el que su vez fue despedido sobre otro de los trabajadores. Ambos lesionados fueron trasladados al Hospital Cullen", informó entonces Vialidad.