---
category: La Ciudad
date: 2022-01-10T06:15:53-03:00
thumbnail: https://assets.3dnoticias.com.ar/BILETERA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Suspendieron los cambios en Billetera Santa Fe y todo sigue igual
title: Suspendieron los cambios en Billetera Santa Fe y todo sigue igual
entradilla: 'Será al menos por esta semana. Los comerciantes esperan mayores precisiones
  del gobierno provincial. Alimentos, bebidas y farmacias continúan con el 30 por
  ciento de reintegro de lunes a lunes

'

---
El inicio de la quinta etapa del programa Billetera Santa Fe quedó suspendida de forma momentánea y los porcentajes de los reintegros y los días donde rigen las promociones quedan como se venían prestando hasta el momento. Es decir que el rubro alimentos y bebidas seguirá teniendo un reintegro del 30 por ciento de lunes a lunes.

Según el diario La Capital, tanto voceros gubernamentales como fuentes del sector privado, específicamente del supermercadismo rosarino confirmaron la decisión que se tomó en las últimas horas en la Casa Gris. Ahora, la confusión se apoderó de los comerciantes y referentes de los sectores frente a lo acotado de la información que recibieron del gobierno provincial. "Hasta acá no se sabe nada, pero la empresa Plus Pagos nos dijo que este lunes sigue todo igual, pero no hay confirmación ni para un lado ni para el otro", dijo desconcertado el titular de los almaceneros rosarinos Juan Milito. "Todo igual hasta nuevo aviso, por esta semana sigue todo como antes", insistió el funcionario de alto rango.

De acuerdo con una resolución firmada el 31 de diciembre por el ministro de Economía de la provincia, Walter Agosto, la devolución del 30% se mantendría en alimentos y bebidas, farmacias, bares y restaurantes, indumentaria, calzado y marroquinería, juguetería y librería, turismo, mueblería y colchonería, bazar y artículos de limpieza y perfumerías. Mientras tanto, electrodomésticos y electrónica tendrán un reintegro del 20% y en las compras en los vinotecas y licorerías el beneficio será del 15%.

Pero ayer reinó la incertidumbre con voces que negaban su puesta en marcha a partir de este lunes. "Lo que sé es que no arranca mañana (por este lunes) la quinta etapa. Esto es lo que sabemos. Su inicio está suspendido hasta aviso oficial", dijo una supermercadista ante la consulta de este diario. "Todo igual hasta nuevo aviso. Por esta semana sigue todo igual", insistió el vocero con llegada al despacho del gobernador Omar Perotti.

**Tiempo de definiciones**

La polémica se desató el viernes pasado cuando trascendió que la modificación sobre Billetera Santa Fe empezaría a regir este lunes con las restricciones anunciadas. Esto implicaría que en supermercados, autoservicios y farmacias la devolución se aplicaría solo en las compras efectuadas lunes, martes y miércoles, mientras que en el resto de los rubros tendrá lugar solo lunes y miércoles.

La resolución 752 remarca que “la recepción por parte de ciudadanos y comercios ha superado las expectativas, generando un círculo virtuoso en la economía provincial, paliando los efectos económicos perjudiciales de la pandemia”.

La noticia de la semana pasada no había caído bien en el rubro de supermercadistas y almaceneros. El presidente del centro que agrupa al segundo sector en Rosario, Juan Milito, había manifestado su malestar a autoridades provinciales . “En lo que respecta a nuestro rubro, la gente compra todos los días con este sistema”, había señalado.

Además, Milito reclamó que el soporte técnico no venía funcionando bien. “El tema de los cambios se filtró en las últimas horas y no quedó otra que confirmarlos”, añadió.

“Esta es una decisión unilateral de la provincia que dispone todos los cambios en el sistema de Billetera [Santa Fe](https://www.unosantafe.com.ar/santa-fe-a2403.html) y nosotros no tenemos absolutamente nada que ver. No es algo deseado por los supermercadistas y autoservicios”, dijeron, por su parte, referentes de los supermercadistas de la capital provincial a UNO Santa Fe.

Uno de los temas que repercuten en la sustentabilidad tal como se lanzó Billetera Santa Fe es la discusión por el presupuesto 2022, que terminó con una nueva grieta entre oficialismo y oposición y pasó el tema para la agenda de febrero. Un punto a discutir es con los números de 2021 y con el uso de hasta el 85 por ciento de las ventas en los aglomerados urbanos, es su posibilidad de encontrar un equilibrio y disponer de partidas suficientes para absorber los descuentos. Ahora llegan momentos de definiciones para el programa Billetera Santa Fe.