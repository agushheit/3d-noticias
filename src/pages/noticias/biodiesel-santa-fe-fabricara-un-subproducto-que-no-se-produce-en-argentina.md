---
category: La Ciudad
date: 2021-09-05T06:00:11-03:00
thumbnail: https://assets.3dnoticias.com.ar/biodisel.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Biodiésel: Santa Fe fabricará un subproducto que no se produce en Argentina'
title: 'Biodiésel: Santa Fe fabricará un subproducto que no se produce en Argentina'
entradilla: Un proyecto del Conicet en fase experimental producirá propilenglicol
  a partir de un subproducto del biodiésel, buscando reemplazar su importación

---
El Instituto de Investigaciones en Catálisis y Petroquímica (Incape), está por finalizar la construcción de una planta piloto destinada a la transformación de recursos renovables en compuestos químicos en Santa Fe. Permitirá obtener, a partir de la producción de biodiésel, 100 toneladas por año de propilenglicol, un compuesto utilizado en alimentos, cosmética y medicina, que aún no se produce en la Argentina, ahorrando millones de pesos en productos importados.

Raúl Comelli, biotecnólogo y director del proyecto, explicó que: “El 80% de la producción instalada del biodiésel en Argentina sale de Santa Fe; el 10% del producto total es glicerol. Lo que estuve investigando fue la transformación del glicerol en propilenglicol, un producto que el país importa 12.000 toneladas al año, no se produce localmente; entonces había una cuestión estratégica en cuanto a políticas de desarrollo”, dijo el investigador del Conicet.

El proyecto se inició en 2007 en el Incape, un instituto de doble dependencia del Conicet y de la Universidad Nacional del Litoral. “En el 2013 hubo una convocatoria desde la Agencia Nacional de Ciencia y Tecnología que promovía proyectos que estuvieran desarrollados en la escala laboratorio, y como teníamos los resultados se conformó un consorcio asociativo público-privado”, relató Comelli. “A través de un financiamiento especial se otorgaron los fondos para poder escalar a planta piloto el proyecto que veníamos desarrollando en los laboratorios de Incape”.

El desarrollo demandó cinco años y una inversión de más de 22 millones de pesos –de los cuales el 57% fue financiado por el Fondo Argentino Sectorial de la Agencia I + D + i, dependiente del Ministerio de Ciencia, Tecnología e Innovación de la Nación; culminando en diversas patentes y en la construcción de la planta piloto.

La planta piloto fue diseñada para 100 toneladas al año de propilenglicol y deberá validar los resultados de lo que se obtuvo a escala laboratorio con la idea de instalar una planta industrial de 30.000 toneladas al año. “El consumo de esas 12.000 toneladas al año representan algo así como 18 a 20 millones de dólares que están saliendo del país, cuando a la materia prima para obtenerlo la tenemos comparativamente en condiciones ventajosas”.

“El glicerol se va de Argentina al exterior para que en el exterior lo transformen en productos que después volvemos a importar”, remarcó Comelli.

La construcción de la planta piloto está en un 90% y en el término de seis meses ya podría estar operativa. La empresa constructora es Varteco Química Puntana, radicada en la localidad santafesina de Monte Vera, pero la fábrica fue montada en la provincia de San Luis.

Los nichos específicos para la exportación de productos verdes obtenidos a partir de fuentes renovables, la ausencia de producción nacional de ambos glicoles y la capacidad de producción de biodiésel en Argentina, son datos concretos que transforman a este desarrollo tecnológico logrado localmente en una excelente oportunidad productiva para el país.