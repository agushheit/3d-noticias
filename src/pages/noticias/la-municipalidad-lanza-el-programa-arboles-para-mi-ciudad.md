---
category: La Ciudad
date: 2020-11-28T14:00:00Z
thumbnail: https://assets.3dnoticias.com.ar/arboles.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Arboles para mi ciudad
title: La Municipalidad lanza el programa “Árboles para mi ciudad”
entradilla: Se trata de una iniciativa que contempla la entrega de semillas y la capacitación
  a vecinos y vecinas con el objetivo de que siembren sus propios ejemplares.

---
El próximo 5 de diciembre, la Municipalidad lanzará el programa “Árboles para mi ciudad” que contempla la entrega de semillas de árboles autóctonos, acompañada por capacitaciones, para que vecinos y vecinas de la capital provincial logren sus propios ejemplares y puedan plantarlos en diferentes puntos del ejido urbano. La iniciativa se mantendrá en el tiempo, debido a que lograr un árbol requiere de cuidados y acompañamiento prolongados.

El plan se basa en la Ley Provincial N° 13.836 denominada “Ley del Árbol”, sancionada el 29 de noviembre de 2018, que declara al arbolado como de “interés público” y promueve la implantación de nuevos ejemplares tanto en ámbitos públicos como en privados.

La norma especifica que, en las ciudades, el arbolado tiene múltiples beneficios y presta servicios ambientales para mejorar la calidad de vida de los ciudadanos y la calidad ambiental de las urbes. 

Ayudan al embellecimiento del paisaje, a moderar la temperatura, a mitigar los efectos del cambio climático, a amortiguar el impacto de las precipitaciones, a purificar el aire, a absorber ruidos ambientales, a proveer de sombra y oxigenación, y a mitigar todos los impactos urbanos ambientales.

“Árboles para mi ciudad” pretende implantar nuevos ejemplares de flora nativa, con el fin de transformar o enriquecer espacios y lograr un sistema de corredores verdes.

Además, se considera que la actividad será de suma importancia para valorizar y comprometer a la ciudadanía en la conservación y el cuidado del patrimonio natural y cultural del ambiente urbano, promoviendo la relación del hombre en armonía con su entorno.

El secretario de Ambiente del municipio, Edgardo Seguro, señaló que “la idea es trabajar con la ciudadanía en un programa para aumentar la cantidad de árboles de la ciudad, comprometiendo a vecinos y vecinas en ese objetivo”.

Según explicó, “la iniciativa tiende a aumentar el arbolado de la ciudad porque eso mejora las condiciones climáticas, y, por otro lado, trabajar sobre la concientización y la educación ambiental”.

Seguro mencionó que “si una persona logró un árbol desde la semilla va a cuidar no sólo a ese ejemplar sino a todos los árboles porque va a entender que es un ser vivo, que lleva mucho tiempo poder lograrlo”.

## Modalidad

Desde la Secretaría de Ambiente del municipio se explicó que la implementación comenzará para los barrios Fomento 9 de Julio y Alvear, para extenderse luego a otras zonas de la ciudad. Según se indicó, cada vecino que forme parte del programa recibirá un kit de siembra y el acompañamiento necesario para la producción de árboles mediante capacitaciones y seguimientos quincenales.

Para formar parte, los interesados deben completar la [inscripción](https://docs.google.com/forms/d/e/1FAIpQLSd3hqas7WivhKFm7tDRLuX0JHske8d6RgGl5AACltPe9bUGKQ/viewform "Inscripción")

La modalidad de trabajo será por medio de un proceso de adopción del árbol o plantas nativas desde su simiente, generando conciencia y compromiso desde la siembra.

Para lograrlo, se establecerá una relación de compromiso a través de mecanismos de participación ciudadana.

Asimismo, será necesario promover diferentes estrategias de capacitación y entrenamiento de los ciudadanos y asociaciones que contribuyan a consolidar una relación armónica con la naturaleza en general, y la restauración ambiental de áreas degradadas.

El secretario de Ambiente acotó que, una vez logrado el árbol, cada participante podrá trasplantarlo al lugar que desee. En ese sentido, mencionó la posibilidad de ubicarlo en su propio terreno, en la vía pública, en las veredas, en pequeñas plazoletas, en el club del barrio o en la vecinal: “Un árbol es un árbol; no importa dónde esté, va a generar una mejora ambiental por sus servicios ecosistémicos”, dijo.

## Cronograma

Este viernes por la tarde se brindará la capacitación introductoria “Siembra y trasplante de árboles nativos”. La misma será con modalidad virtual, mediante la plataforma ZOOM, y estará a cargo de Carlos Chiarulli, subdirector Ejecutivo de Recursos Naturales Urbanos de la Municipalidad.

El sábado 5 de diciembre, en tanto, se concretará la entrega de ejemplares a los inscriptos en el programa. Será en las vecinales Alvear y Fomento 9 de Julio, desde la hora 9.

Luego, el viernes 11 de diciembre, se realizará la segunda capacitación, titulada “Cuidados y rusificación de árboles nativos”. También será mediante la plataforma ZOOM, a partir de las 18 horas y estará a cargo de Carlos Chiarulli.

Por último, el viernes 18 de diciembre, se concretará un encuentro virtual de seguimiento.