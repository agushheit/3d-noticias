---
category: Agenda Ciudadana
date: 2021-09-05T06:00:10-03:00
thumbnail: https://assets.3dnoticias.com.ar/vaccine.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Argentina superó los 57 millones de dosis de vacunas recibidas
title: Argentina superó los 57 millones de dosis de vacunas recibidas
entradilla: A partir de la llegada de más vacunas, el objetivo del Gobierno para setiembre
  es seguir completando los esquemas

---
Gracias a los avances en la campaña de inmunización, el país alcanzó 14 semanas de descenso sostenido de casos y 12 semanas consecutivas de disminución en el número de muertes.

La Argentina superó los 57 millones de vacunas contra el coronavirus recibidas desde el inicio del plan estratégico de inoculación que el Gobierno nacional despliega en todo el país, con la llegada de un nuevo cargamento de más de 1,7 millones de dosis de Sinopharm, mientras se espera para la semana próxima el arribo de las primeras dosis de Pfizer.

**El Plan de vacunación**

A partir de la llegada de más vacunas, el objetivo del Gobierno para septiembre es seguir completando los esquemas de vacunación de la población y, en ese sentido, ya se adelantó que la idea es aplicar 7 millones de segundas dosis durante todo el mes, tal como ocurrió en agosto.

"En agosto hubo un avance muy importante para avanzar con los esquemas y en septiembre continuaremos con esta tarea. La Argentina ya tiene más personas con dos dosis que con una. Hay más de 15 millones de personas que completaron su esquema de vacunación", dijo esta semana la asesora presidencial Cecilia Nicolini.

Además adelantó que la semana próxima estaría llegando al país el primer envío con 260.000 dosis del laboratorio Pfizer y se espera que "las 20 millones de dosis que contrató el Estado Nacional vengan consecutivamente hasta diciembre", dijo Nicolini.

Mientras tanto, un cargamento con más de 1,7 millones de vacunas Sinopharm, arribó al aeropuerto internacional de Ezeiza.

El embarque llegó en el vuelo LH8264, de la línea aérea área Lufthansa, que aterrizó a las 18,20 con 1.736.000 dosis de Sinopharm en sus bodegas.

La nueva partida se suma a un total de 3.248.000 que la Argentina recibió por parte de la misma firma, para completar un total de 4.984.000 vacunas arribadas durante esta semana del mismo laboratorio.

Además, el martes último llegó un vuelo de Air Canada con 1.654.500 dosis de AstraZeneca, que ya están siendo distribuidas a todas las jurisdicciones del país,

De esta manera, en los últimos cinco días llegaron al país 6.638.500 dosis, mientras que hasta el momento y, desde el inicio de la campaña, ya son 57.207.120 las vacunas arribadas al país.

Según los datos publicados en el Monitor Público de Vacunación, ya se distribuyeron en todo el país 51.604.874 dosis de vacunas, al tiempo que las aplicaciones totalizan 44.401.437, de las cuales 28.306.772 corresponden a primera dosis y 16.094.665 ya cuentan con el esquema completo.

El avance en la vacunación se produce mientras el país alcanzó ayer 14 semanas de descenso sostenido de casos de coronavirus y una disminución de más del 88 por ciento del número de infectados entre el pico del mes de mayo y la semana pasada, según se informó oficialmente.

De esta manera, mientras que en la semana epidemiológica 20 –la de mayor cantidad de infectados– alcanzó los 225.938 casos según fecha de inicio de síntomas, en la semana pasada el número llegó a 26.346, lo que implica una reducción del 88,4 por ciento.

La Argentina registra además 12 semanas consecutivas de disminución del número de muertes por Covid-19, así como un descenso en la ocupación de camas UTI que llega a la cifra más baja registrada desde fines de agosto de 2020.

Según las estimaciones del ministerio de Salud, los casos vienen en disminución en todas las regiones del país.

Ya se distribuyeron en todo el país 51.604.874 dosis de vacunas, al tiempo que las aplicaciones totalizan 44.401.437, de las cuales 28.306.772 corresponden a primera dosis y 16.094.665 ya cuentan con el esquema completo

El promedio diario en junio fue de 20.261, en julio de 13.312 y en agosto de 6.416 y no se registran departamentos en situación de alarma epidemiológica, al tiempo que, entre los departamentos con más de 40.000 habitantes, solo el 3,5% se encuentra en alto riesgo epidemiológico.

Como resultado de la vacunación, continúa la tendencia a la baja de la cantidad de pacientes con Covid-19 internados en terapia intensiva. Así, al 3 de septiembre hay una reducción del 68,2% en la cantidad de internaciones en cuidados intensivos en comparación con el pico de la semana 23 (7.839 contra 2.497).

Actualmente la letalidad de Covid-19 en el país es de 2,1%. Con respecto al año pasado, en el 2021 se observa una disminución de la letalidad en todos los grupos de edad.

La distribución de casos confirmados según sexo y edad en el año 2021 se mantiene sostenida en el tiempo, el 49,1% es masculino y 50,9% femenino.

Según los últimos registros, el 84,9% de la población total mayor de 18 años recibió al menos una dosis de la vacuna, y el 46,3% completó su esquema, mientras que el 78,3% de la población de 60 y más años recibió ya las dos dosis de la vacuna contra el SARS-CoV-2.