---
category: La Ciudad
date: 2022-01-20T06:00:28-03:00
thumbnail: https://assets.3dnoticias.com.ar/AGUA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Críticas al servicio de Aguas Santafesinas en la audiencia pública por el
  68% de aumento
title: Críticas al servicio de Aguas Santafesinas en la audiencia pública por el 68%
  de aumento
entradilla: " Por sufrir Covid-19 no pudo hablar el titular de Assa, Hugo Morzán y
  lo hizo la vicepresidenta, Marisa Gallina. Un usuario señaló que tampoco se inscribieron
  ni como participantes otros directores y gerentes."

---
El pedido de aumento de tarifas por parte de Aguas Santafesinas Sociedad Anónima cumplió este miércoles 19 con la celebración de su audiencia pública, con la libre participación de cualquier interesado además de expositores por parte de la compañía estatal. No fue un trámite, como se suele simplificar, ni una mera formalidad. Hubo duras críticas, más a la calidad de la prestación del servicio y las demoras en las inversiones en infraestructura sanitaria, que a los niveles tarifarios medidos en pesos.

 Si bien la mayoría de los expositores participantes se manifestaron en contra de la suba planteada, las voces más fuertes se ocuparon de señalar que se necesita una empresa "más eficiente" con más y sobre todo mejores prestaciones.

Un ciudadano se quejó por los altos sueldos de la conducción política de la empresa y señaló que ni siquiera esos funcionarios figuraban inscriptos en la lista de participantes. Osvaldo Gauna, quijote santafesino de causas perdidas, siempre presente en las audiencias públicas, mezcló sus prejuicios para con la clase política en general con asuntos privados pero dijo también lo que otros no. Preguntó: "¿no les interesa a los señores gerentes y directores lo que tenemos para decirles los usuarios?".

**Tensión en la pantalla**

Hubo una suerte de efecto enero que se manifestó a dos puntas. Por una parte, la participación de ciudadanos interesados en el tema sin representación institucional fue baja. Sólo se anotaron 30 participantes y de ellos 6 diputados provinciales, 3 concejales y un par de funcionarios municipales. También algunos integrantes de entidades de consumidores siempre certeros respecto de la necesidad de actualizar el marco regulatorio, y un interesante par de vecinalistas que en otras oportunidades no habían estado.

 Por otra parte, el terrible enero hizo que pesara la bajante de los ríos de los últimos meses de 2021 y sus graves efectos en las fuentes superficiales de captación que, por más esfuerzos que se hagan en las plantas, llegan a las canillas. Aunque potable, al agua siempre excelente de Assa últimamente se la extraña, en paladar y hasta a la vista: tantas veces sale turbia. Y eso cuando se tiene el servicio, porque hubo media docena de los 13 expositores que describieron lo que pasa cuando los tanques quedan vacíos y de los grifos no sale una gota.

 En cuanto a los números, además de los históricos reclamos de entidades de usuarios y consumidores para que haya más micromedición y así imponer el uso racional del agua, que es más factible cuando se paga por lo que se usa, los críticos de Assa dijeron que la suba que pretende la empresa en dos escalones del 40% primero y del 20% en una segunda etapa significaría un impacto final del 68% por su efecto acumulativo, que desde la empresa nadie negó. 

 Sobre el mismo tema, tal como era de prever, la prestadora expresó con énfasis sus dos argumentos más fuertes: que la última vez que se discutieron tarifas corría el año 2018 con un dólar que rondaba los 35 pesos, que la última actualización del 32% tuvo lugar en 2020 y que desde entonces no se ha tocado. El otro punto a su favor es que mientras los fondos para que haya agua potable y cloacas no salgan de las tarifas que pagan los usuarios la empresa debe financiarse con aportes del tesoro provincial. Y que estos han trepado producto del congelamiento al 46% el año pasado y alcanzarán casi el 70% en 2022 si no hay un aumento. 

 El problema es que no todos los santafesinos tienen agua o cloacas provistas por Aguas. Hay un millón que vive fuera de los distritos cubiertos por la empresa, y también aporta.

 **Termómetro I**

 El presidente del directorio de Aguas, Hugo Morzán, sufrió un cuadro febril y se le diagnosticó Covid este martes 18, por lo que no estuvo en condiciones para exponer mientras se repone de esa dolencia. En su lugar, la vicepresidenta Dra. Marisa Gallina dijo que en la empresa "celebramos la audiencia pública como mecanismo de participación ciudadana para la expresión de todas y todos los actores respecto del tema que nos convoca". Y recordó: "La última revisión tarifaria reconoció los incrementos en la estructura de costos sobre la base de los estados contables del año 2018". 

 "Además de la pandemia tuvimos la bajante extraordinaria de río y esto obligó a Assa a atender con inversiones y gastos de emergencia conjuntamente con el Estado Provincial, consiguiendo resultados satisfactorios en materia de producción, calidad y abastecimiento del servicio público esencial de agua potable y cloacas".

 A la exposición en detalle de los números, de los costos y de los planes de Aguas Santafesinas para 2022 la asumió su vocero, Guillermo Lanfranco, siempre didáctico para exponer el punto de vista de la prestadora. Cerró el pedido de un aumento con una definición que parece todavía estar en discusión a juzgar por algunas de las ponencias posteriores. El pago de la tarifa de los servicios sanitarios "no es un impuesto, sino una contraprestación del usuario que cumple con niveles de calidad que controla el ente regulador. Sirve para sostener el servicio", recordó.

 Guillermo Lanfranco, vocero de Aguas, dijo que en los dos últimos años se ha dado "un nuevo perfil" a la empresa para llevar agua a sectores marginados en las dos grandes ciudades de la provincia.Foto: Gentileza

 Antes se extendió sobre la suba de insumos imprescindibles, de la energía eléctrica, y de gastos que no son normales en la empresa que habitualmente goza de las mejores fuentes de captación a orillas de Paraná o su valle de inundaciones y ahora debió sumar bombas y kilovatios para tener agua cruda. Describió el impacto que tendrá el aumento en el 75% de los hogares provistos por Aguas. Por mes serán apenas unos 800 pesos más (si no se cuentan los impuestos y las tasas que encarecen el servicio). 

 **Termómetro II**

 Las audiencias públicas son un termómetro del humor social respecto de los prestadores que inician pedidos para aumentar tarifas, pero esta vez el "efecto enero" citado más arriba influyó en las quejas de vecinalistas y usuarios de la ciudad de Santa Fe que les recordaron a la empresa y al ente regulador que en barrio Candioti salió agua que no era apta para consumo humano por las cañerías, así como de la recurrente falta de agua en la zona norte de la capital provincial por estos días.

 Más estridentes fueron las quejas por la falta de agua en Rafaela, en Esperanza y en Villa Gobernador Gálvez, según las exposiciones de vecinalistas santafesinos y de ediles y legisladores que hablaron de las demás ciudades.

 **Acceso**

 El histórico drama de la falta de acceso a la red formal de agua o su total carencia también fue parte de las exposiciones. Y hubo un detallado informe provisto por un funcionario de la Oficina del Consumidor de la Municipalidad de Rosario que otros expositores posteriormente mencionaron. Como siempre, las pérdidas de agua en la calle sirvieron para graficar la ineficiencia que se achaca a la empresa.

En el mismo sentido se expidió, por la Defensoría del Pueblo, Jorge Henn, que pidió que la audiencia pública sea vinculante.

 **Tiempos**

 La directora del Enress, Anahí Rodríguez, tuvo a su cargo la apertura de la Audiencia Pública, junto a su par Leonel Marmiroli, que sin demoras explicaron la mecánica habitual del debate y la decisión de hacerlo solo remoto, sin encuentros presenciales.

 Ambos actuaron como coordinadores y moderadores, aunque en muchos casos no hubo la necesidad de pedir que los expositores (que finalmente fueron 13) se ajustaran a los diez minutos establecidos para quienes hablaron desde un lugar de representación o los cinco para los usuarios.

 **DOS SANTAFESINOS EN EL DEBATE POR LA SUBA DEL GAS** 

La audiencia pública para la "Adecuación transitoria de la tarifa del servicio público de transporte y distribución de gas natural" convocada por el Ente Nacional Regulador del Gas (Enargas) cuenta solo con dos santafesinos entre sus inscriptos.

La convocatoria coincidió en fecha y casi en el horario con la audiencia pública para la adecuación de las tarifas de Aguas Santafesinas SA.

La mayoría de los expositores en la audiencia en el ámbito nacional corresponde a las distribuidoras del fluido, entre ellas Litoral Gas que actúa en la Provincia de Santa Fe y el norte de Buenos Aires. También se anotaron como oradores funcionarios de las defensorías del pueblo de la Ciudad Autónoma de Buenos Aires, de esa provincia y de Córdoba.

 María Victoria Noriega, presidenta de la Cooperativa Setúbal de la ciudad de Santa Fe participa de la audiencia, en nombre de la Asociación de Subdistribuidores de Gas de la República Argentina. Y también como asistente Gabriel Savino en representación de la Defensoría del Pueblo de la provincia de Santa Fe.