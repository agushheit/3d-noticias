---
category: Agenda Ciudadana
date: 2021-05-12T07:19:04-03:00
thumbnail: https://assets.3dnoticias.com.ar/Futbol-5.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de LT10
resumen: 'Sectores afectados: darán asistencia y será un monto superior al de 2020'
title: 'Sectores afectados: darán asistencia y será un monto superior al de 2020'
entradilla: Así lo afirman desde el gobierno provincial. La medida abarcará a gimnasios,
  centros de pilates, yoga, canchas de fútbol 5 y centros de entrenamiento. Los interesados
  deben inscribirse llenando un formulario.

---
Este martes el gobierno provincial adelantó que habrá asistencia para los sectores afectados por las nuevas restricciones  por quince días impuestas en medio de la pandemia de coronavirus que transita la provincia.

En una reunión del Comité Operativo de Emergencia (COE) se tomó como medida la asistencia asistencia económica directa para gimnasios, centros de pilates, yoga, canchas de fútbol 5 y centros de entrenamientos de distintas disciplinas en espacios cerrados.

La asistencia será para los 14 departamentos donde las actividades se encuentran suspendidas y según adelantaron funcionarios que participaron de la reunión de COE la asistencia será mayor a la brindada en 2020 pero no aseguraron el monto.

El secretario de Comercio Interior y Servicios, Juan Marcos Aviano declaró que "se está trabajando para que a partir del fin de semana esté disponible en la página de la provincia el formulario de inscripciones”.

“En muchos casos va a ser un monto superior al que habíamos otorgado el año pasado”, adelantó el funcionario provincial.

La reunión del Comité Operativo de Emergencia fue la primera de una ronda de encuentros previstos para este martes y que además incluye una agenda con intendentes y presidentes comunales y, representantes de sectores afectados por la pandemia covid-19.

**La preocupación de los sectores**

En diálogo con LT10, Alejandro Gaveggia , empresario referente de salones de fiestas y evento explicó que "son muchas las personas que se quedaron si trabajo y la situación es desesperante". 

"No conseguimos que nos atienda el teléfono el intentente Emilio Jatón en 14 meses", declaró y ante la consulta sobre cuáles son las razones por las que no le permitan volver a la actividad el empresario dijo que "los políticos están aterrorizados que en el distrito se baile".

Siguiendo con la explicación Gaveggia dijo que "en esta ciudad se permitieron los gimnasios, andar en lancha, hacer deportes, pero no se permitieron fiestas ni cumpleaños donde la gente pueda moverse o bailar en lugares que siguen todos los protocolos". Luego siguió: "nos habilitaron como modo bar que es como jugar al tenis sin raqueta o jugar al fútbol sin hacer goles". 

Para finalizar el empresario explicó que los "estigmatizaron" y que, si bien no pueden asegurar que la gente no se contagie, sí están seguros que puede pasar de la misma manera que ocurre en una reunión familiar, en una plaza o en un gimnasio.