---
category: Agenda Ciudadana
date: 2020-12-14T11:31:01Z
thumbnail: https://assets.3dnoticias.com.ar/traferri.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Infobae'
resumen: Cuál será la estrategia de defensa del senador santafesino acusado de proteger
  el juego ilegal
title: Cuál será la estrategia de defensa del senador santafesino acusado de proteger
  el juego ilegal
entradilla: Armando Traferri intentará hoy demostrar que las imputaciones que le hicieron
  de ser el jefe de una organización son un show y que intentan amedrentarlo.

---
“Si no fuera un legislador con fueros, una persona común estaría presa, le allanarían su casa, la de sus hijos, la de sus hermanos y, si están vivos, la de su mamá, la de su papá y la de su abuelita, y le romperían la puerta para que todo sea bien escandaloso”. 

Con esa idea, aunque tal vez sin la alusión explícita a aquella famosa amenaza de Pablo Escobar, el senador provincial por Santa Fe Armando Traferri (PJ) intentará hoy demostrar que las imputaciones que le hicieron de ser el jefe de una organización dedicada a proteger el juego clandestino en la provincia son “un show montado sin fundamentos y con la estrategia de amedrentarlo”.

Entre esas pruebas denunciará que el ex fiscal Gustavo Ponce Asahad, detenido y confeso de recibir coimas para esa misma red de encubrimiento, lo involucró en el caso siguiendo una estrategia guionada por su principal enemigo en el gobierno de Omar Perotti, el ministro de Seguridad Marcelo Sain. Según Traferri, los fiscales que lo investigan son “delfines” que cumplen directivas del funcionario provincial.

“Va a ser una conferencia de prensa explosiva. De Perotti para abajo va a mencionar a todos los responsables de esta operación política”, prometen quienes asesoran al senador. Traferri dice tener filmado a los fiscales cuando le reconocen que se reunieron el 27 de noviembre último con el ex fiscal Gustavo Ponce Asahad, una semana antes de que este se presentara para ampliar su declaración indagatoria. Según el legislador, en ese encuentro se armó el relato que lo incrimina.

“No hay una sola grabación, una sola escucha, una sola foto. Solo los dichos de Ponce Asahad”, aseguran después de haber revisado el fin de semana las tres mil páginas que les dieron con las evidencias. No obstante, reconocen que Traferri se reunía con “mucha gente, porque ese es su trabajo” y que ha invitado a comer a su casa a varios dirigentes políticos, jueces y fiscales. “Comés, charlás y hablás de las cosas. No es algo anormal”, responden.

***

![](https://assets.3dnoticias.com.ar/exfiscal.jpeg)

***

También se preguntará el senador por San Lorenzo por qué la Fiscalía no llama a indagatoria al actual diputado provincial Maximiliano Pullaro (UCR), exministro de Seguridad de Miguel Lifschitz, a quien el ex fiscal nombra en el mismo relato en el que lo acusa. O por qué no se convocó a declarar al otro ex fiscal detenido, el regional Patricio Serjal, que formaría parte del mismo esquema de recaudación. Según dijo Ponce Asahad, el dinero que recibía era para entregárselo a su jefe.

En el entorno de Traferri insistirán con una duda que tienen y que para ellos justifica las inexactitudes que ven en la declaración del exfiscal: cómo es que no declaró bajo la figura del “arrepentido”, que le permitiría, si se prueban sus dichos, conseguir que le descuenten a la mitad la pena que le toque; pero que si dice mentiras, se la duplican. Creen que gracias a eso pudo decir más de lo que se va a poder probar. Los fiscales argumentaron que no quisieron abusar de la figura del arrepentido.

“Tengo una ventaja: -dice el senador peronista- yo no borré ningún WhatsApp de los que tengo con Ponce Asahad. Evidentemente él parece que sí porque no están entre las pruebas. Yo los tengo y los puedo mostrar, quién llamó a quién y por qué tema”, aseguran que dirá. Cuentan que en los otros casos que hay en la causa figuran todos los mensajes de WhatsApp y las fotos compartidas.

# **Contar la verdad**

Traferri declaró el viernes ante los fiscales en Rosario y ratificó un escrito en el que sostiene que todas las reuniones y llamadas telefónicas que tuvo con los ex fiscales ahora presos y el empresario del juego Leonardo Peiti, también detenido, fueron de “carácter institucional”. Ponce Asahad aseguró que el senador era el nexo entre los fiscales y el empresario del juego.

Tras salir del Centro de Justicia Penal no habló con la prensa argumentando que antes quería ver “todas” las pruebas que tienen para acusarlo. Por eso citó a los medios para hoy a las 11 en la delegación del Senado en Rosario. Allí promete “hablar con las pruebas, contestar todo y contar su verdad. Vamos a dar todos los detalles del procedimiento que vamos a denunciar”, asegura enigmático su equipo.

Mientras los fiscales no dudan en tener evidencias para imputarlo apenas renuncie a sus fueros o el Senado se los quite, Traferri se niega a facilitar ese procedimiento aduciendo desconfianza en los fiscales que lo investigan.

***

![](https://assets.3dnoticias.com.ar/traferri1.jpeg)

***

# **Un disco vacío**

En ese recorrido de dudas menciona lo que denomina una “trapisonda” de parte de los fiscales Luis Schiappa Pietra y Matías Edery cuando les pidieron las pruebas completas que hay en contra del senador.

“Habían acordado que se las daban, las miraba y contestaba, pero no hubo tiempo de nada. Cuando nos fuimos al estudio del abogado que lo asesora al senador a ver esas pruebas, el primer disco estaba vacío y al otro supuestamente lo estaban preparando. Pero cuando volvimos a reclamar nos encontramos a los fiscales ya anunciándoles a los medios que iban a pedir el desafuero para imputarlo”, relatan enojados.

“No es serio. Nos dijeron que nos daban la posibilidad de mirar las pruebas y contestarlas para ver si los convencíamos de no pedir el desafuero”, señalan los letrados que asesoran a Traferri, que se quejan porque les dijeron una cosa e hicieron otra. 

“Por eso ahora pedimos todas las pruebas, si no cada día las van a ir cambiando. Van a allanar media Argentina para ver si lo encuentran culpable”, deslizan desde ese entorno, sugiriendo que al exfiscal Ponce Asahad lo convencieron para que nombre al senador a cambio a algún beneficio todavía no del todo claro, como podría ser la prisión domiciliaria. También les reprochan las demoras que hubo para que el Senado reciba una copia de los videos con la declaración del exfiscal.

Un segundo hecho del que se sorprenden ocurrió este fin de semana cuando allanaron la casa del ex diputado provincial Darío Scataglini. “Fue a declarar 72 horas antes y le rompen la puerta y le allanan la casa. Uno allana para encontrar cosas para después citar a declarar. No al revés. Es para que esté en todos los diarios. Lo allanan a Scataglini y hablan una vez de él y diez de Traferri. Todo por una llamada a la secretaria de Ponce Asahad en la que dijo que era de parte del senador”.

En la conferencia de prensa de hoy van a insistir con que las acusaciones a Traferri “son una clara maniobra del Ministerio Público de la Acusación. Hicieron todo en un fin de semana largo. El senador llegó al miércoles ya condenado por la opinión pública. Si hubiesen podido lo metían preso. Hay un excesivo poder en el MPA, te pueden allanar cuando quieren. El ciudadano común está indefenso. Debería ser el órgano por excelencia que garantice justicia y se usa para apretar”, justifican.

# **Lejos del desafuero**

“El desafuero está muy lejos”, dicen cerca del legislador provincial, al que solo podrían imputar si el Senado le quita esa inmunidad parlamentaria. De lo contrario, mientras dure su mandato (2023) no puede ser sometido a proceso penal, y mucho menos detenido.

Por los antecedentes de la Legislatura provincial, y a nivel nacional con Cristina Kirchner, es probable que el desafuero no se apruebe, salvo que aparezcan pruebas muy contundentes. En 2006 el Senado provincial rechazó un pedido similar contra otro de sus miembros: Felipe Michlig (UCR). Para que salga se necesita sumar los dos tercios de los votos de los presentes. Si estuvieran los 19 senadores, serían 13 votos, y el PJ tiene 12. En este contexto, existe la posibilidad de que hoy el bloque del PJ se rompa en dos.

“Las inmunidades parlamentarias no fueron creadas como refugio corporativo de los legisladores, sino como garantía funcional de independencia frente a otros poderes institucionales”, sostiene el doctor en derecho constitucional Oscar Blando.

Para buscar una comparación de cuánto tiempo puede llevar ese trámite, en la Legislatura santafesina se menciona el caso del fiscal de Venado Tuerto Mauro Blanco, que insumió seis meses de deliberaciones antes de ser suspendido, acusado de vínculos con un narco.

En el caso de habilitarse un proceso penal, tendría que iniciarse una investigación en la que deberían ser analizadas todas las evidencias que apuntan a demostrar que Traferri es el responsable de una organización ilegal para recaudar ofreciendo protección al juego clandestino.

Sus íntimos dicen que el senador está lejos de preocuparse. “No tengo dudas que salgo bien, porque no tengo nada que ver. No sé adónde quieren llegar, pero la voy a dar vuelta de taquito”, le escucharon decir al “Pipi” (tal su apodo) mientras preparaba el asado del domingo.