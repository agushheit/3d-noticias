---
category: Estado Real
date: 2022-02-21T11:14:59-03:00
thumbnail: https://assets.3dnoticias.com.ar/juanchi.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3d noticias
resumen: Diputados de JXC piden interpelar al ministro Perczyk.
title: Diputados de JXC piden interpelar al ministro Perczyk.
entradilla: Diputados nacionales de Juntos por el Cambio, encabezados por el radical
  Juan Martín, piden que se interpele al ministro de Educación, Jaime Perczyk, ante
  el crítico escenario educativo generado durante la pandemia.

---
Además quieren saber cuántos estudiantes dejaron las aulas y cómo se recuperarán contenidos. “Queremos un inicio de ciclo lectivo en 2022 con todos nuestros niños, niñas y jóvenes en las aulas. Por eso solicitamos que el gobierno detalle qué está haciendo para que eso ocurra. Hoy no hay ningún dato oficial certero”, alertó el legislador.

Ante la ausencia de información oficial respecto de la situación educativa en el país tras dos años de pandemia, con cierres totales o parciales de las escuelas que han afectado críticamente los procesos de aprendizaje, los diputados solicitan la presencia del ministro Perczyk en el Congreso de la Nación.

“Faltan cifras oficiales para que las políticas de revinculación sean realmente eficaces, queremos que el Ministro informe cuántos son, qué se hizo y que se hará para que todos esos alumnos vuelvan a las aulas. La ausencia de información nos vuelve a sumergir en un debate que muestra a la educación como objeto de la puja política, sin evidencias, sin experiencias comparadas y sin estadísticas serias necesarias para transformar esta pugna en un debate sensato”, indicó el diputado.

Además, piden saber “cómo se van a recuperar los aprendizajes perdidos en estos dos años de improvisaciones y resistencias a volver a las aulas, poner en marcha un apoyo integral centrado en los niños marginados de cada comunidad, que incluya clases de recuperación, apoyo en salud mental, nutrición y protección, entre otros. Hace falta un monitoreo serio y eficiente de las estrategias desplegadas por el Estado Nacional y nuevas herramientas que permitan abordar la situación crítica que significan casi dos años de desvinculación para cientos de miles de estudiantes del sistema educativo”.

Acompañan esta iniciativa los diputados nacionales de JXC Facundo Manes, Mario Barletta, Maximiliano Ferraro, Federico Angelini, Victoria Tejeda, Ximena García, Gabriela Lena, Fabio Quetglas, Carolina Castets, Lidia Ascárate, Roberto Sánchez, Fernando Carbajal, Pedro Galimberti, Dina Rezinosvky, Camila Crescimbeni, Jorge Vara, Federico Frigerio, Sebastián Salvador, Germana Figueroa Casas, Gerardo Cipolini, Gabriel Chumpitaz y José Núñez.