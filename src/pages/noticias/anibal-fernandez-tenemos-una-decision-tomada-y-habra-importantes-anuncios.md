---
category: Agenda Ciudadana
date: 2021-09-26T06:15:55-03:00
thumbnail: https://assets.3dnoticias.com.ar/ANIBAL.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'Aníbal Fernández: "Tenemos una decisión tomada y habrá importantes anuncios"'
title: 'Aníbal Fernández: "Tenemos una decisión tomada y habrá importantes anuncios"'
entradilla: El ministro de Seguridad de la Nación dijo que la próxima semana se conocerán
  los detalles de las medidas dispuestas

---
El ministro de Seguridad, Aníbal Fernández, adelantó hoy que esta semana "va a ser importante en anuncios", porque "ya se tomó una decisión" sobre acciones que se llevarán a cabo para encarar la situación que se vive en la ciudad de Rosario.

"Todo lo que es Rosario fue fruto de un análisis con Marcelo Sain (director del Organismo de Investigaciones de Santa Fe), con quien nos conocemos mucho", dijo el funcionario en declaraciones al programa Toma y Daca que se emite por la radio AM 750.

En ese sentido, sostuvo que "esta semana va a ser importante en anuncios", porque "ya tenemos tomada una decisión al respecto", pero no adelantó esas medidas.

Por otra parte, sostuvo que con el ministro de Seguridad de la provincia de Buenos Aires, Sergio Berni, "hemos charlado mucho, somos amigos desde hace muchos años".

En relación al encuentro que mantuvo esta semana con el ministro de Justicia y Seguridad de la Ciudad de Buenos Aires, Marcelo D'Alessandro, dijo que "no soy amigo, pero tengo relación hace muchos años también".

"Conversamos de muchos tópicos, dejamos en claro que el Gobierno Federal ya no tiene incidencia sobre la calle en la Ciudad de Buenos Aires, eso es de la Policía de la Ciudad", expresó el ministro.

En ese sentido, sostuvo que "entonces, lo que nos pusimos de acuerdo es colaborar, pero la responsabilidad es de la Policía de la Ciudad".

Respecto de la Corte Suprema de Justicia, señaló que su "posición personal, que no es la del Gobierno, es que se debería empezar con una ampliación" de sus integrantes.

"En 1853 era de nueve, después en 1860, cuando se incorpora la provincia de Buenos Aires y se confirmó la Nación Argentina, se transformó en una Corte de cinco y creo que habiendo tantos países con tanta cantidad de miembros de la Corte podría ayudar a solucionar temas", expresó.

En ese sentido, señaló que "el impacto de uno solo de los ministros hace que esté en juego el 20 por ciento de la totalidad de la composición, y eso es algo que no debería ser tan contundente".

Consultado sobre el resultado adverso que el Frente de Todos (FdT) obtuvo en las elecciones primarias abiertas, simultáneas y obligatorias (PASO), Fernández consideró que "hubo gestos y acciones que no fueron contundentes para ser comprendidos por los segmentos de quienes nos acompañan".

"Hay que seguir trabajando en el mismo sentido que hicimos siempre, respetando a todos aquellos que todavía no encuentran solución a su tema por esos cuatro años de catástrofe que le tocó vivir a la Argentina y por la pandemia", subrayó.