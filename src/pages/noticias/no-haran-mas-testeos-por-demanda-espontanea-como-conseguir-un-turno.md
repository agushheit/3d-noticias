---
category: La Ciudad
date: 2022-01-08T06:00:17-03:00
thumbnail: https://assets.3dnoticias.com.ar/cemafe.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'No harán más testeos por demanda espontánea: cómo conseguir un turno'
title: 'No harán más testeos por demanda espontánea: cómo conseguir un turno'
entradilla: "Se habilitó un “turnero” en la página web del gobierno provincial. También
  podrán solicitarse por el 0800. \n\n"

---
Frente a la demanda de testeos que existe en la ciudad de Santa Fe y que crece a diario, desde el Ministerio de Salud informaron que a partir de este viernes para ir a hisoparse en las unidades de testeo territorial habrá que solicitar turno previo y que no se atenderá más por “demanda espontánea”. 

 Aquellas personas con al menos un síntoma vinculado al Covid-19 y deseen hacerse el test en los puntos fijos que están habilitados en Santa Fe y Rosario, previamente deberán gestionar su turno a través de la web de la provincia www.santafe.gob.ar, en la sección “Turnos Hisopado Covid – Santa Fe”; o comunicándose al 0800-555-6549.  

 **Cómo sacar un turno**

 Esta medida se tomó con el fin de organizar los centros de testeos y evitar grandes aglomeraciones  (en la capital provincial este jueves se realizaron más de 3 mil testeos con una positividad del 50 %). “Estábamos teniendo inconvenientes no solo con las personas que se iban a testear sino que también con los trabajadores de salud, por eso ésta es una herramienta para ordenarse”, resaltó Romina Carrizo, subsecretaria de Equidad de la Provincia.

 Al mismo tiempo informó que “hay una capacidad de 2.200 turnos por día”, y agregó que “los turneros están habilitados hoy (viernes) y empezarán a otorgar turnos para mañana (sábado). El 0800 seguirá funcionando y turnará para testearse en el Cemafe”.

 **Solicitar un turno**

 En la opción trámite elegir "COVID - turnos hisopados"

 Acto seguido, debe completar el formulario para que sea asignado un turno en alguno de los centros habilitados, sea en Santa Fe o en Rosario. 

 

 

 