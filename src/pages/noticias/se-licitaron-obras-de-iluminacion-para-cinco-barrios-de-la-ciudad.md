---
category: Agenda Ciudadana
date: 2022-06-25T11:42:08-03:00
thumbnail: https://assets.3dnoticias.com.ar/2022-06-24NID_275085O_1.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: Se  licitaron  obras de iluminación para cinco barrios de la  ciudad
title: Se  licitaron  obras de iluminación para cinco barrios de la  ciudad
entradilla: La provincia realizará una inversión total cercana a los 250 millones
  de pesos. Será para los barrios Guadalupe Central,Transporte, Belgrano, Alberdi
  y Unión y Trabajo, en el marco del Plan Incluir.

---
El gobernador Omar Perotti, junto al intendente de Santa Fe, Emilio Jatón, abrieron este viernes en el Centro Gallego, los sobres con las ofertas económicas de las licitaciones para la readecuación de los sistemas de iluminación de los barrios Guadalupe Central, Transporte, Belgrano, Alberdi y Unión y Trabajo, en el marco del Plan Incluir. La inversión total es de más de 248 millones de pesos.

En la oportunidad, Perotti expresó que “hoy es una fecha importante por los montos que se licitan, por los objetivos que se buscan, y porque tiene que ver con uno de los programas provinciales en el que estamos poniendo especial atención para mejorar la iluminación de las ciudades y también su seguridad”.

En esa línea, el gobernador señaló que “la acción de prevención es una de las demandas más fuertes; y la iluminación, el desmalezamiento, la limpieza, son elementos esenciales en cada barrio. Hemos advertido que, en las dos grandes ciudades de la provincia especialmente, hay un retraso importante en lo que refiere a la iluminación, y la necesidad de ayudar a los municipios a que puedan ponerse a tono, es clave. La mayor demanda que reciben los intendentes cuando se habla de seguridad, tiene que ver con mejorar la iluminación y generar corredores seguros, con más presencia de móviles y de efectivos policiales. Y en esa línea vamos”, destacó.

“Si mejoramos la iluminación se transmite más tranquilidad, y todo el esfuerzo que se hace en colocar cámaras se aprovecha mejor porque se obtienen mejores imágenes. Sin dudas que es una tarea local cuidar la iluminación y hacer el mantenimiento correspondiente; pero necesitamos hacerlo y en el menor tiempo posible. Por ello, poner a tono a las ciudades y, fundamentalmente, en muchos de los sectores donde el retraso en iluminación es grande, permite mejorar la seguridad, gestionando juntos”, afirmó el mandatario.

En ese marco, el titular del Ejecutivo provincial recordó que “el Programa Incluir incorporó un refuerzo para trabajar en los temas de iluminación en las principales ciudades de la provincia. Si mejoramos la iluminación y el cuidado del ambiente, estamos sumando otro elemento importantísimo”, aseguró.

Finalmente, el gobernador agradeció a “los vecinos organizados en su vecinal, porque es una instancia comunitaria de alto valor, es el que quita tiempo a su familia, a sus amigos, a sus momentos de ocio, para trabajar por los vecinos y por este barrio como lo más importante, y siempre el reclamo más importante es el de su barrio, el que debería estar primero”.

Perotti concluyó su alocución señalando que “le toca al intendente y a los funcionarios ordenar esas prioridades, pero es bueno que cada vecino sienta que su barrio es el primero, ahí empieza a hacerse una ciudad, a construirse cada una de las posibilidades para que una ciudad funcione mejor”.

IMPACTO EN LOS BARRIOS

A su turno, Jatón expresó que “hoy no es un día más en la ciudad de Santa Fe, sino uno de esos que se esperan. No estamos hablando de cambiar un foco, sino de seguridad, de vivir mejor y de que el nuevo sistema lumínico de la ciudad va a ser distinto, llevándolo a un 65% de iluminación Led a fin de año”.

El intendente agregó que “hoy estamos licitando estas obras con fondos provinciales, a partir de un acuerdo que consensuamos con el gobernador que demandan una inversión de 250 millones de pesos. Esto va a impactar directamente en los barrios santafesinos. Muchas veces se habla de las diferencias, de las miradas distintas, pero aquí confluyen esas miradas, con la provincia y el municipio al servicio de mejorar la calidad de vida de la gente”.

Del acto, realizado en el Centro Gallego de la ciudad de Santa Fe, participaron también el ministro de Gestión Pública de la provincia, Marcos Corach; el secretario de Integración y Fortalecimiento Institucional, José Luis Freyre; el diputado nacional, Roberto Mirabella; el director de Gestión Urbana de la Municipalidad de Santa Fe, Matías Pons Estel; y la concejala local Jorgelina Mudallel, entre otras autoridades provinciales, municipales y representantes de asociaciones vecinales.

LICITACIÓN

Los trabajos licitados se ejecutarán en el marco del Plan Incluir, y del programa municipal Santa Fe Se Ilumina 2022, y en total representarán una inversión de más de 248 millones de pesos.

La primera licitación comprende la readecuación del Sistema de Iluminación de las vecinales Unión y Trabajo, Alberdi y Guadalupe Central, con la instalación de 940 columnas, 15 tableros y 964 artefactos Led. El presupuesto oficial es de $ 160.058.858,36.

Para esta obra se presentaron tres oferentes: Electromecánica Tacuar SRL, que cotizó los trabajos en $ 198.425.231; Bauza Ingenieria SRL, $ 195.014.671,68; y Tecsa SA, $ 222.903.838,99.

En tanto que la segunda licitación incluye la readecuación del Sistema de Iluminación de los barrios Transporte y Belgrano, con la colocación de 493 columnas, 9 tableros y 525 artefactos Led. El presupuesto oficial es de $ 88.469.744,78.

Para estos trabajos se presentaron dos propuestas: Electromecánica Tacuar SRL ofertó $ 104.395.483; y Bauza Ingeniería SRL, $ 109.052.971,08.