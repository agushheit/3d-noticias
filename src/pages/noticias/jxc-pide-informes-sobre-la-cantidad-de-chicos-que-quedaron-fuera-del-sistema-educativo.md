---
category: Agenda Ciudadana
date: 2022-02-01T11:54:24-03:00
thumbnail: https://assets.3dnoticias.com.ar/juanchi.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NCN
resumen: JxC pide informes sobre la cantidad de chicos que quedaron fuera del sistema
  educativo
title: JxC pide informes sobre la cantidad de chicos que quedaron fuera del sistema
  educativo
entradilla: Encabezados por Juan Martín, pidieron se  informe la cantidad de alumnos
  que se desvincularon del sistema educativo en la pandemia y que detalle qué se está
  haciendo para que vuelvan a clases.

---
Solicitan estadísticas para saber cuántos niños quedaron fuera del sistema en cada uno de los niveles desde el año 2020, desagregando modalidades y niveles de enseñanza, jurisdicción educativa y sector de gestión. También que se consigne el estado de situación de las acciones y programas implementados para la revinculación de quienes perdieron el vínculo escolar durante la pandemia. Los programas a los que se hace referencia son el Sistema Integral de Información Digital Educativa; Acompañar. Puentes de Igualdad y Volvé a la Escuela. Además, conocer qué nuevas estrategias se están diseñando desde el Ministerio de Educación para continuar con la revinculación y equiparación curricular con aquellos que permanecieron en el sistema.

 Recuerdan las advertencias que se plantearon para evitar el cierre sin planificación de las escuelas, entre ellas las formuladas por las fundaciones de JXC, Pensar, Alem y Hannah Arendt, en el documento “Por el Regreso a las aulas”, donde se anticipaba que «mantener las escuelas cerradas implicaría perder el potencial de toda una generación”.

 “Hoy vemos con claridad lo que advertimos oportunamente: la política educativa de la pandemia fracasó, si es que entendemos por política colocarse delante de los acontecimientos, planificar, atender las diversidades del contexto, mirar lo que pasa en el mundo, articular y generar consensos”, cuestionó el diputado Juan Martín.

 “Esta verdadera tragedia educativa se detiene con hechos, no con indignación y mientras más tiempo pasan los chicos alejados de la escuela, más chances hay de que no vuelvan. Por eso exigimos que el ciclo lectivo 2022 empiece con todos los chicos en las aulas. Hay estimaciones que ubican en 1,5 millones los chicos que han dejado las escuelas. Pero faltan cifras oficiales para que las políticas de revinculación sean realmente eficaces”, subrayó el diputado nacional.

 

Advierte, que la tarea no se termina con conocer los datos requeridos: “se debe resolver cómo se van a recuperar los aprendizajes perdidos en estos 2 años de improvisaciones y resistencias a volver a las aulas. Para eso hace falta un monitoreo serio y eficiente de las estrategias desplegadas por el Estado Nacional para que los niños vuelvan a las aulas”, destacó Juan Martin.

Acompañan esta iniciativa los diputados de JXC Roberto Sánchez, Fernando Carbajal, Miguel Bazze, Sebastián Salvador, Roxana Reyes, Maximiliano Ferraro, Gerardo Cipollini, Lidia Ascárate, Pedro Galimberti, Alejandro Cacace, Gabriela Lena, Fabio Quetglas, Ximena García, Dina Rezinovsky, Federico Frigerio, Francisco Monti, Florencia Klipauka, Camila Crescimbeni y Álvaro Martínez.