---
category: La Ciudad
date: 2021-01-23T09:03:08Z
thumbnail: https://assets.3dnoticias.com.ar/puerto.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Diario El Litoral
resumen: El río Paraná crece de forma sostenida y en febrero llegaría a los dos metros
title: El río Paraná crece de forma sostenida y en febrero llegaría a los dos metros
entradilla: 'El hidrómetro del Puerto Santa Fe midió este viernes 1.55 metros y marcó
  así un aumento de 86 centímetros en una semana. El pronóstico del INA es favorable
  gracias a las lluvias en la Cuenca Alta del Plata. '

---
El 2021 trajo consigo las esperadas lluvias en el sur de Brasil, es decir en la Cuenca Alta del Plata, desde donde nace y se alimenta el río Paraná. Estas precipitaciones empezaron a reflejarse en las represas (Itaipú, Capanema y Guairá), en los puertos del río Iguazú y en los de la provincia de Corrientes. También es importante destacar a las lluvias locales que acontecieron en los últimos días y ayudaron al incremento del caudal. 

Esa "bendita" agua fluyó e hizo crecer al río Paraná y a sus afluentes. A la altura del Puerto de Santa Fe, el hidrómetro marcó este viernes 1.55 mts., 86 centímetros más que el registro del 14 de enero. De esta manera, y teniendo en cuenta el sufrido 2020 en cuestiones hidrométricas, este repunte hace pensar en una notable mejoría de los niveles, respecto a las primeras semanas de este año.

Las mejoras en la altura del río llegan en un momento oportuno para que el abastecimiento de agua potable no sufra las consecuencias que apareja la bajante extrema, sobre todo durante la temporada estival, cuando el consumo de agua crece de forma exponencial. 

 

**Tendencias auspiciosas**

En su informe semanal, el Instituto Nacional del Agua (INA) pronosticó que para la próxima semana el río Paraná en el puerto local mediría 1.65 m. Mientras que para los primeros días de febrero, la altura rondaría entre los 1.73 y 2.05 metros. 

De cumplirse la proyección y superar los dos metros, el nivel en el Puerto Santa Fe llegará a una marca que no tenía desde el 23 de marzo del 2020. Ahora la incógnita que se abre es ¿la bajante empieza a quedar atrás? Por lo pronto las proyecciones son buenas, todo se definirá si continúan las precipitaciones importantes en el sur brasilero. 

 

**Situación en puertos vecinos**

En las estaciones portuarias de las ciudades más importantes de Santa Fe y Entre Ríos, que bordean el Paraná, también se registraron mejoras hidrométricas en los últimos días. En Rosario, el nivel se elevó a 78 centímetros este viernes, frente a los 33 cm del pasado 18 de enero; en el puerto de San Lorenzo la marca actual alcanzó el metro y mostró un repunte de medio metro respecto al inicio de esta semana. 

 En la otra orilla del río, en la capital entrerriana (también afectada por la bajante y con una emergencia hídrica decretada por el municipio) el nivel creció a 1.30 metros, lo que representa una mejora de 68 centímetros en comparación al lunes pasado (62 cm). Mientras que en Diamante (Entre Ríos) el registro actual es de 1.53 metros, también con un importante repunte en pocos días. 