---
category: Agenda Ciudadana
date: 2021-09-10T06:00:59-03:00
thumbnail: https://assets.3dnoticias.com.ar/CUARTOOSCURO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de El Litoral
resumen: Las escuelas santafesinas afectadas a las elecciones no tendrán clases el
  lunes
title: Las escuelas santafesinas afectadas a las elecciones no tendrán clases el lunes
entradilla: El ministerio de Educación dispuso que no habrá actividad escolar en el
  turno mañana de los establecimientos afectados a los comicios, para realizar tareas
  de ordenamiento y limpieza.

---
El ministerio de Educación de Santa Fe dispuso, en una resolución dada a conocer este jueves, que no habrá clases en el turno mañana del día lunes en los establecimientos educativos de la provincia -tanto públicos como privados- que se encuentren afectados a la realización de las elecciones primarias, abiertas, simultáneas y obligatorias durante este domingo 12 de septiembre.

La resolución ministerial, que lleva la firma de la ministra Adriana Cantero, indica: “La Dirección General de Recursos Humanos señala que corresponde a aquel personal docente y asistente escolar que fuera designado como jefe de Departamento, jefe de Fracción, delegado, subdelegado, asistente escolar sanitario, autoridad de mesa (presidente y 1º y 2º vocales) dentro de la estructura operativa de designación y capacitación de autoridades de mesa, el otorgamiento de una franquicia compensatoria, para el día hábil inmediato posterior, tanto para los comicios primarios, abiertos, simultáneos y obligatorios del 12 de septiembre de 2021 como para los comicios generales del 14 de noviembre de 2021”.

El motivo es que ese tiempo será destina a concretar tareas de ordenamiento y limpieza de rutina tras la celebración de los comicios. Vale recordar que, debido a la pandemia, se incrementaron la totalidad de mesas de escrutinio en todo el territorio, a los fines de poder cumplir con los protocolos sanitarios.