---
category: Agenda Ciudadana
date: 2021-03-16T07:59:12-03:00
thumbnail: https://assets.3dnoticias.com.ar/amsafe.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Continúa la tensión entre el Gobierno y los docentes públicos
title: Continúa la tensión entre el Gobierno y los docentes públicos
entradilla: 'El ministro de Trabajo, Empleo y Seguridad Social pidió a los docentes
  de Amsafé que revean la medida de fuerza. Amsafé sostiene que no va a permitir amenazas.  '

---
La tensión entre el Gobierno Provincial y el gremio de los docentes públicos AMSAFE continúa creciendo. 

El ciclo lectivo 2021 en la provincia de Santa Fe comenzó con un paró total de actividades en escuelas públicas con un acatamiento del 100%. La medida de lucha se da luego del ofrecimiento del 35% de aumento en 3 tramos que el gobierno presentó en la mesa paritaria.

El ministro de Trabajo, Empleo y Seguridad Social, Juan Manuel Pusineri, expresó  sobre el paro de docentes que se lleva adelante en toda la provincia de Santa Fe: “Le solicitamos a Amsafé que revea la medida de fuerza. La oferta paritaria fue una propuesta elaborada durante un mes, la fuimos construyendo, supera la paritaria nacional docente, se ubica por encima de otras provincias”.

“La mesa de negociación está abierta, no existen motivos para llevar adelante una medida de fuerza en este contexto y dejar a alumnos sin clases”, agregó el funcionario. “La convocatoria puede ser el miércoles u hoy dentro de una hora, tenemos diálogo permanente, pero necesitamos hacerlo sin medidas de fuerza. Sadop, UPCN y ATE aceptaron, pero plantearon algunas cuestiones que vamos a seguir trabajando”.

“La situación del rechazo permanente es francamente irresponsable, no privilegia los intereses de los trabajadores y está realizada por dirigentes que no pagan ningún costo por ello”, sostuvo.

Por último, disparó: “Estamos analizando descontar los días de clases a los docentes que no se presenten a trabajar. ¿Por qué hacen esta medida de fuerza si la propuesta es buena como los mismos dirigentes de Amsafé dijeron?”.

**La postura del Gremio**

El delegado de la Seccional Amsafé La Capital, Rodrigo Alonso, se refirió a la medida de fuerza que llevan adelante los docentes de escuelas públicas en toda la provincia de Santa Fe. Aseguró que el acatamiento en el departamento capital fue total y se mostró en contra de las declaraciones del ministro de Trabajo, Juan Pusineri.

"Estamos convencidos que el camino nunca es la amenaza. Es el diálogo, la convocatoria, que en el marco de la paritaria podamos discutir y se presente una nueva propuesta para mejorar lo que se ha ofrecido inicialmente".

Más allá de las intervenciones que se puedan hacer con organismos externos, creemos que la definición tiene que ser la voluntad política del gobierno provincial convocándonos a una mesa paritaria. Por eso la definición que tomó la Amsafé tiene que ver con esto, con un paro de 48 horas para hoy y mañana. El próximo paro de 48 horas está supeditado a que el gobierno nos convoque y nos presente una nueva propuesta", agregó Alonso.

"Entendemos que la propuesta se podría mejorar tanto en términos salariales como en condiciones de trabajo. Esperamos una propuesta que pueda acortar el cronograma de traslado y titularización en distintas niveles y modalidades; que pueda reducir los tres tramos propuestos salarialmente y que contemple la pérdida de poder adquisitivo que tuvimos los trabajadores durante el 2020".

Y concluyó: "El acatamiento en el departamento La Capital fue total. Nos llena de orgullo porque tiene en cuenta las definiciones que los y las trabajadoras tomamos. En los conflictos entramos todos juntos y salimos todos juntos. En un departamento donde la mayoría votó por otra estrategia, estamos llevando adelante y sosteniendo el plan de lucha de la mayoría provincial".