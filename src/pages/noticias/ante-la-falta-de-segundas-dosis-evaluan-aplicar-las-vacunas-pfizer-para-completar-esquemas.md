---
category: Agenda Ciudadana
date: 2021-09-09T06:15:19-03:00
thumbnail: https://assets.3dnoticias.com.ar/PFIZER.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Ante la falta de segundas dosis, evalúan aplicar las vacunas Pfizer para
  completar esquemas
title: Ante la falta de segundas dosis, evalúan aplicar las vacunas Pfizer para completar
  esquemas
entradilla: Desde la Conain sostienen que las vacunas norteamericanas deben aplicarse
  para completar esquemas ante el faltante de Sputnik y AstraZeneca. Este jueves habrá
  una reunión clave.

---
Con la llegada del primer cargamento de 100.000 dosis de Pfizer al país durante este miércoles los expertos sanitarios debatirán cual será la mejor estrategia para aplicar estas vacunas. En este marco, la Comisión Nacional de Inmunizaciones (Conain) se reunirá el próximo jueves para definir como será suministrado el inmunizante norteamericano.

Hasta el momento existen dos grandes grupos de posibilidades sobre la mesa: utilizar las Pfizer para seguir inoculando a menores de 17 años o para combinar con esquemas iniciados bajo Sputnik V o AstraZeneca.

La infectóloga y ex ministra de Salud de la provincia de Santa Fe, Andrea Uboldi, quien también forma parte de la Conain como miembro del núcleo central. Se refirió a el uso de la vacuna estadounidense y afirmó: "El dilema es que si no hay previsibilidad sobre segundas dosis de Sputnik se completen esquemas de adultos aplicando Pfizer como segundo componente o si se avanza sobre la población joven".

La cuestión relacionada a la falta de dosis no escapa a Santa Fe, donde son numerosos los casos en los que se aplicó la primera dosis de Sputnik V o AstraZeneca y luego de cuatro meses no fueron llamados para la segunda dosis. En este sentido, la credibilidad de las autoridades sanitarias está en juego ante la consecutiva postergación del esquema completo.

Con este panorama, la infectóloga destacó que "desde el punto de vista de la campaña de vacunación primero hay que cerrar segundas dosis". Y agregó: "En jóvenes y niños de 12 a 17 años el riesgo de complicaciones e internación es bajísimo comparado con una persona mayor. Hay mucha gente que ya pasó cuatro meses desde su primera dosis y no tiene segunda".

"Viendo que la variante Andina y la variante Manaos afectó a gente joven sana el objetivo tiene que ser cubrir con esquema completo de dos dosis a la gente que tiene factores de riesgo para luego desescalar. Si hubiera una constante en la que el virus no muta se puede extender más la segunda dosis. Ahora hay una amenaza de la variante Delta que en algún momento va a explotar, por lo que si la gente está protegida con dos dosis habrá menos estrés en el sistema de Salud", continuó la experta de la Conain.

La ministra de Salud, Carla Vizzotti se expresó sobre esto precisando que "estas vacunas serán utilizadas para iniciar esquemas de vacunación de adolescentes sin comorbilidades en forma universal, empezando con los de 17 años, o bien, en función de los planes provinciales, para completar esquemas de vacunación".

La opción de combinar Sputnik V con AstraZeneca permanecería en stand by, a causa de las pocas dosis del inmunizante inglés disponibles en el país. Desde la Conain se acordó que la vacuna inglesa no se combine con Sputnik V porque no eran abundantes las dosis de AstraZeneca para completar ese esquema.

**Pfizer con AstraZeneca**

Así mismo, desde la Conain se planteará la necesidad de combinar vacunas de Pfizer no solo con la vacuna rusa sino también con AstraZeneca. Sobre esto, Uboldi puntualizó: "Si no entran las dosis de AstraZeneca se debe combinar con Pfizer o con Moderna, lo que son combinaciones buenísimas. Dosis de Moderna no quedan por lo que se podría dar una combinación de AstraZeneca y Pfizer".

Esto ya se realizó en gran parte de Europa, al momento en que se definió la negativa de aplicar la segunda dosis de AstraZeneca por los estudios que indicaban un mínimo riesgo de trombosis. Los expertos plantean que "las combinaciones de vacunas vectoriales con las de ARN mensajero son interesantes" para una mayor inmunización.

**Sputnik V + CanSino**

El pasado lunes arribaron al país 200.000 vacunas monodosis de CanSino, destinadas en principio a poblaciones de difícil acceso (situación de calle, migrantes, refugiados y otros colectivos dispersos).

En este marco, desde la Conain se planteará que en los estudios de combinación de vacunas que se están llevando a cabo a nivel nacional se incorpore a las pruebas la vacuna CanSino. El objetivo es evaluar su viabilidad para combinarla con Sputnik V. Al respecto de esto, Andrea Uboldi afirmó que esta monodosis "se asemeja mucho al segundo componente de la vacuna rusa Sputnik V".