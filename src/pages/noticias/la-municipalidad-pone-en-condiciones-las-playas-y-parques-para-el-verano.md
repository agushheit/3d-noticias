---
layout: Noticia con imagen
author: "Fuente: Prensa Municipalidad"
resumen: Playas y parques
category: La Ciudad
title: La Municipalidad pone en condiciones las playas y parques para el verano
entradilla: Interviene en las costaneras Este y Oeste y también en los Parques
  del Sur y Juan de Garay.  Mientras tanto, se licitan los paradores que se
  instalarán en distintos lugares.
date: 2020-11-05T14:41:37.247Z
thumbnail: https://assets.3dnoticias.com.ar/playas.jpg
---
Con el objetivo de llegar en las mejores condiciones a la temporada de verano 2020-2021, la Municipalidad realiza distintas tareas para acondicionar las playas y sectores de recreación. Se trabaja en las costaneras Este y Oeste, donde se realizan movimientos de la arena, despeje de vegetación de las playas, recuperación de veredas y trabajos de pintura; y también se ponen a puntos los piletones y todo el sector recreativo de los Parque del Sur y Garay.

“Estamos trabajando, por un lado, en los protocolos de utilización del espacio público con fines recreativos. Y, por el otro, desde hace tiempo, en la recuperación de muchos parques y paseos en distintos puntos de la ciudad y, también, en la puesta a punto de nuestras playas para que los santafesinos puedan contar con espacios adecuados”, dijo el intendente Emilio Jatón.

El titular del Ejecutivo municipal recorrió este miércoles, junto a la secretaria de Obras y Espacios Públicos, Griselda Bertoni, las tareas que se llevan a cabo frente a la Universidad Tecnológica Nacional (UTN), en la Costanera Oeste. Allí se trabaja con una motoniveladora para poner a punto la zona de la arena -con el fin de llegar hasta la playa- y también despejando la vegetación.

En esta línea, una retropala trabaja en el juntado de la arena que está en el camino bajo que tenía varios metros cúbicos y una parte será trasladada hasta el Parque del Sur porque -en forma paralela- se realizan obras en ese espacio verde de la ciudad. Máquinas más grandes acomodarán la arena hasta llegar a la zona del agua.

Por otro lado, personal de las delegaciones centro-este y también de La Guardia trabajan en la limpieza, desmalezado y paisajismo del bosque nativo que está en la Costanera Este. Entre otras tareas, se levantan escombros y se perfila la arena para dejar en condiciones ese lugar para todos los santafesinos.

**En condiciones**

En este sentido, la secretaria de Obras y Espacios Públicos dio más detalles de estos trabajos. “Una vez que esté en condiciones la arena, se recuperará el equipamiento. Como el lijado y la pintura de bancos, de los cestos de basura, entre otros. También se trabaja en la recuperación de la iluminación con la colocación de reflectores para que se pueda disfrutar de la playa también de noche”, dijo Bertoni.

Además en esta parte alto de la Costanera Oeste se colocarán nuevos cestos de basura, bebederos en todo el corredor. “En la Este ya se recuperó todo el mobiliario, se reacondicionó el área deportiva en la cabecera de esa costanera. También se intervino en ese camino, se colocó nueva iluminación y se hicieron todas las demarcaciones de las distintas áreas, entre otras acciones”, agregó la funcionaria.

En esta línea, anticipó que aún se siguen esperando los resultados de los análisis del agua para ver si se puede utilizar en esta temporada. “Mientras tanto trabajamos para dejar en condiciones todo el entorno y las áreas de playa para que los santafesinos disfruten deportiva y familiarmente de las dos costaneras”, destacó Bertoni.

![](https://assets.3dnoticias.com.ar/playas1.jpg)

**Espacios renovados**

Con un avance de obra superior al 80%, la Municipalidad concreta mejoras en el Parque del Sur a los fines de solucionar problemas estructurales. El último fin de semana se realizaron pruebas en el sistema de chorros ubicado en los tres piletones, los cuales se impermeabilizaron por completo, reparando rajaduras y demoliendo pequeñas estructuras de adoquines ubicadas alrededor de las fuentes.

En esa línea, se generarán playas de arena en sectores delimitados. Asimismo se demarcará un sector para encuentros recreativos y deportes playeros. Los juegos de plaza, en tanto, fueron restaurados por completo. También se realizaron mejoras en parquización e iluminación, para ampliar el horario de uso. Se colocaron cestos de basura de hormigón apostados por pares, para diferenciar residuos secos de húmedos.

También en ese sector habrá una oferta gastronómica para esta temporada. “Se trabajó en toda la zona de solariums, deportiva, de sombra y recreación, pero la intervención en el parque no está toda terminada sino que seguirá durante la temporada y también durante el próximo año”, detalló la secretaria de Obras y Espacios Públicos.

Del mismo modo, ya se inició la intervención del Parque Garay, en la zona de los piletones. Allí se hacen tareas de rellenado en las fisuras, para que no pierda agua o suba arena y luego se va a pintar. También se hará toda una área nueva gastronómica que tal vez no esté terminada para esta temporada, para que puedan disfrutar todos los santafesinos, entre otras intervenciones.