---
category: Agenda Ciudadana
date: 2020-12-06T12:01:04Z
thumbnail: https://assets.3dnoticias.com.ar/lomanegra.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: NA'
resumen: 'Loma Negra está paralizada: peligra la provisión de cemento en el país'
title: 'Loma Negra está paralizada: peligra la provisión de cemento en el país'
entradilla: Es por un conflicto entre el sindicato de trabajadores mineros y una empresa
  proveedora. Loma Negra es la primera cementera de la Argentina, con el 45% de las
  ventas totales.

---
Un conflicto entre el sindicato de trabajadores mineros AOMA y la empresa Minerar, que abastece la piedra necesaria para producir cemento, obligó este sábado a la compañía Loma Negra a paralizar sus dos hornos en Olavarría, y en los próximos días deberá frenar los despachos de ese insumo clave para la construcción.

Loma Negra es la primera empresa cementera en el país, con aproximadamente el 45% de las ventas totales. **Desde el sector de la construcción manifestaron preocupación, al considerar que este desabastecimiento impactará negativamente en la economía, que se venía recuperando tras la pandemia**.

El conflicto y la suspensión de producción de cemento se dan en una de las plantas en Olavarría donde Loma Negra está realizando obras de ampliación por 350 millones de dólares.

Este hecho se produjo a pocos días de que el Gobierno nacional anunciara la decisión de monitorear el abastecimiento de materiales para la construcción, para asegurar el flujo a obras privadas y públicas.

Las negociaciones entre AOMA y Minerar se iniciaron en la última semana de octubre e incluyeron una conciliación obligatoria del Ministerio de Trabajo de la Nación. La cartera conducida por Claudio Moroni dictó una nueva conciliación obligatoria el jueves último, pero el sindicato decidió no acatarla.

  
 