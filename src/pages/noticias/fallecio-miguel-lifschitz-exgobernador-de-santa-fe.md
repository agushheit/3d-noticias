---
category: Agenda Ciudadana
date: 2021-05-09T22:00:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/010421-Lifschitz.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Falleció Miguel Lifschitz, exgobernador de Santa Fe
resumen: Falleció Miguel Lifschitz, exgobernador de Santa Fe
title: Falleció Miguel Lifschitz, exgobernador de Santa Fe
entradilla: El ex gobernador de Santa Fe  falleció este domingo a sus 65 años tras
  permanecer internado desde el 19 de abril en terapia intensiva a causa de complicaciones
  por un cuadro de coronavirus.

---
Miguel Lifschitz, ex gobernador de la provincia de Santa Fe, falleció este domingo a los 65 años por complicaciones en su cuadro de coronavirus. El dirigente socialista estaba internado en terapia intensiva con respirador en un sanatorio privado de la ciudad de Rosario, donde había ingresado el lunes 19 de abril.

Roberto Miguel Lifschitz nació en Rosario el 13 de septiembre de 1955. Descendiente de inmigrantes, llegó a ser ingeniero civil y un prominente político argentino perteneciente al Partido Socialista, por el que se desempeñó como gobernador de la provincia de Santa Fe durante el período 2015-2019. Anteriormente, fue intendente de la Ciudad de Rosario durante dos mandatos consecutivos (2003-2007 y 2007-2011), y senador provincial de Santa Fe por el departamento Rosario durante el período 2011-2015. Actualmente se desempeñaba como el presidente de la Cámara de Diputados de la Provincia de Santa Fe, desde el 11 de diciembre de 2019.