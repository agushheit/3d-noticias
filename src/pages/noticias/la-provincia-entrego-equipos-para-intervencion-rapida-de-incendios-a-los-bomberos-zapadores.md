---
category: Estado Real
date: 2022-01-13T06:00:24-03:00
thumbnail: https://assets.3dnoticias.com.ar/zapadores.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La provincia entregó equipos para intervención rápida de incendios a los
  Bomberos Zapadores
title: La provincia entregó equipos para intervención rápida de incendios a los Bomberos
  Zapadores
entradilla: Se trata de seis equipos denominados Combate 500, que serán distribuidos
  en los departamentos La Capital, Rosario, San Lorenzo, Rafaela y Vera.

---
El gobierno de la provincia de Santa Fe, a través del Ministerio de Seguridad, entregó este miércoles nuevos equipos de intervención rápida contra incendios, que serán distribuidos entre las cinco agrupaciones de Bomberos Zapadores de la provincia.

Durante el acto, que se realizó en la ciudad de Rosario, el ministro de Seguridad, Jorge Lagna, destacó que “seguimos equipando a los Bomberos de nuestra provincia, una de las áreas más postergadas de nuestras fuerzas de seguridad cuando asumimos la conducción del Ministerio”, y agregó: “Estos equipos son fundamentales en estos tiempos porque son una herramienta más en la lucha contra los incendios forestales y como primer interviniente ante otros eventos porque son fáciles de transportar ya que están montados sobre camionetas”.

“En una época en la que se necesita reforzar el equipamiento contra incendios, nosotros seguimos invirtiendo, porque sabemos la importancia que tienen los Bomberos en las comunidades, que actúan arriesgando su vida para salvar la de otros”, finalizó Lagna.

Los nuevos equipos de intervención rápida están dotados de motor a explosión y un tanque con 500 litros de agua, que permitirá atacar pequeños incendios, y al estar montados sobre camionetas podrán ingresar en lugares donde las autobombas no pueden ingresar por su tamaño.

De la entrega participaron además, la subsecretaria de Políticas de Seguridad Pública, Jassi Capitanelli; la jefa de Policía de la provincia, Emilce Chimenti; los directores de Bomberos Zapadores de la provincia de Santa Fe, Andrés Lastorta; y General de Policía, Daniel Almada; y el fabricante de los equipos Combate 500, Hugo Viñale.