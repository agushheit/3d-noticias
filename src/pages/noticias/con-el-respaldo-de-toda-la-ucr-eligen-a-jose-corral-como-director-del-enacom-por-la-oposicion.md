---
category: Agenda Ciudadana
date: 2021-05-07T09:28:08-03:00
thumbnail: https://assets.3dnoticias.com.ar/corral1.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa José Corral
resumen: Con el respaldo de toda la UCR, eligen a José Corral como director del ENaCom
  por la oposición
title: Con el respaldo de toda la UCR, eligen a José Corral como director del ENaCom
  por la oposición
entradilla: 'La Comisión Bicameral eligió por unanimidad a José Corral para integrar
  -en representación de la oposición- el directorio del Ente Nacional de Comunicaciones
  (ENaCom). '

---
El exintendente de Santa Fe agradeció a las autoridades partidarias por la nominación, y se comprometió “a trabajar por la libertad de expresión en el país, por la mayor conectividad en todo el territorio nacional, por el acceso a la información -que es uno de los pilares de la democracia- y apoyar a las Pymes, especialmente las del interior, todo en el marco del diálogo y los consensos que la Argentina necesita”.

A propuesta de la Unión Cívica Radical (UCR), José Corral fue elegido este jueves con voto unánime por la Comisión Bicameral para integrar -en representación de la oposición- el directorio del Ente Nacional de Comunicaciones (ENaCom).

“Vamos a proponer, tal como lo hicimos el 11 de septiembre en una nota elevada a la Comisión, que se designe a José Corral” para integrar el directorio del ENaCom en nombre de la oposición, anticipó la diputada nacional de la UCR, Karina Banfi, durante la reunión de la Bicameral. “Ha sido intendente de la ciudad de Santa Fe, con un amplio curriculum y una historia de vida ligada principalmente a la gestión y a la toma de decisiones. Y ha cumplido cargos también en medios públicos locales y provinciales”, dijo en referencia al cargo que ocupara José Corral entre 2002 y 2007 al frente del directorio de LT10.

“Creemos que esto puede ser un antecedente importante para todo el aporte que se pueda hacer en esta tarea casi titánica que tenemos con el marco de la pandemia desde el ENaCom de garantizar no solamente todo el sistema audiovisual de la Argentina, sino también la tecnología y la conectividad”, resumió Banfi, al postular a José Corral para el cargo, y cuya nominación fue aprobada por unanimidad en la Bicameral. 

José Corral manifestó su agradecimiento “al respaldo de toda la UCR, a través especialmente de Alfredo Cornejo y Gerardo Morales, que son las autoridades partidarias; Mario Negri y Luis Naidenoff, como autoridades de los bloques legislativos; y el trabajo en la Comisión de Karina Banfi y Miguel Bazze, para esta designación que me compromete a trabajar por la libertad de expresión en el país, por la mayor conectividad para todos los compatriotas en todo el territorio nacional, para el acceso a la información, que es uno de los pilares de la democracia, y para apoyar especialmente a las Pymes que brindan servicios de comunicación y de conectividad de datos, fundamentalmente las del interior, todo en el marco del diálogo, el consenso y los acuerdos que el país necesita”.

El excandidato a gobernador por Cambiemos aseguró que “estamos convencidos de la necesidad de compartir esfuerzos del Estado y de los privados para una mejor comunicación y una mejor conectividad, que son bases para la convivencia democrática y para el desarrollo económico que necesitamos en el país”.