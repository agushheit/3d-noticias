---
layout: Noticia con imagen
author: "-"
resumen: Plan de inversión policial
category: Estado Real
title: "Plan de inversión policial: más tecnología y trabajo con gobiernos locales"
entradilla: El gobierno de la provincia, a través del ministerio de Seguridad,
  presentó hoy el proyecto de inversión de tres mil millones de pesos aportados
  por el gobierno nacional.
date: 2020-10-27T16:54:23.192Z
thumbnail: https://assets.3dnoticias.com.ar/policia.jpg
---
En el marco de la Mesa Institucional de Seguridad de Rosario, se presentó el proyecto de inversión de tres mil millones de pesos aportados por el gobierno nacional a través del convenio de Cooperación y Asistencia Financiera entre la jefatura de gabinete de ministros y la provincia de Santa Fe.

Al respecto, el ministro de Seguridad, Marcelo Sain aseguró que "en el marco de la Mesa de Coordinación Institucional que fueron creadas como un lugar de coordinación para todos lo municipios, hemos funcionado y fuimos construyendo un ámbito de confianza e intercambio para trabajar en conjunto".

A raíz del aporte de Nación para la provincia, Sain detalló la inversión al aclarar que "la inversión va a estar destinada exclusivamente a la seguridad preventiva sobre la base de la adquisición de tres grandes componentes: los puestos de mando que van a hacer al Centro Operacional Policial para Rosario y gran Rosario, el teléfono que va a portar cada uno de los efectivos que son computadoras que van referenciar su posición para diagramar y controlar en tiempo real el despliegue y por último, un sistema de videovigilancia, porque todo esto se va a montar sobre la base de la integración del sistema de videovigilancia municipal y provincial para garantizar respuesta inmediata; eso no lo hubiésemos podido hacer sin la inversión del gobierno nacional".

"Tenemos que poner el ojo en la forma del trabajo policial (continuó el funcionario), ya no sirve el patrullamiento aleatorio sin ningún tipo de evaluación de la problemática criminal o los acontecimientos de lo que ocurren en la ciudad; el grueso de los delitos en los espacios urbanos son reiterativos. Por eso hay que enseñarle a la policía a enfocar problemas específicos en determinados espacios y lugares, con tecnología acorde a estos tiempos que implique el gerenciamiento de la operatividad policial".

La tecnología que estamos presentando tiene que acompañar estos cambios que nos estamos proponiendo, tiene que estar en la planificación, en los recursos, en el mapa policial y es ahí donde necesitamos a los gobiernos locales, para que vayan creando espacios de gestión", enfatizó el titula del cartera de Seguridad.

**Centros de Análisis y Operaciones Policiales**

En cuanto a la focalización de la labor policial, y en el marco del proceso de modernización tecnológica y reforma integral del sistema de seguridad pública de Santa Fe llevado adelante por el gobierno provincial, por [Resolución Ministerial N° 1138/2020](/uploads/res-1138-2020.pdf) se establecieron en el ámbito de la Policía de la Provincia siete Centros de Análisis y Operaciones Policiales (COP), a los efectos de desarrollar estrategias y acciones de seguridad de manera eficiente.

En este sentido, el gobierno provincial ha priorizado la implementación inicial de estos COP en tres grandes centros urbanos: Santa Fe, Rosario y Rafaela.

Los COP son núcleos de carácter netamente analítico-operacional, y tendrán la misión de producir análisis criminal, planificar las operaciones e intervenciones policiales focalizadas y evaluar el desempeño operacional policial posterior. Estarán conformados funcionalmente por tres componentes: una Oficina de Análisis Criminal, una Oficina de Operaciones Policiales y una Oficina de Evaluación Operacional Policial.

Cada COP comprende una integración de sistemas tecnológicos y de comunicaciones, tales como aquellos aplicativos que recaban información criminal en la provincia (las denuncias al 911, Comisarías y Centros Territoriales de Denuncias); el Sistema de Cámaras de Videovigilancia de provincia, municipios y comunas; además de los sistemas de GPS, información generada por Drones y cámaras portátiles personales y en vehículos. De tal modo, el COP permitirá planificar, dirigir y evaluar la operación policial en todos sus aspectos.

**Estuvieron presentes**

El secretario de Seguridad Pública, Germán Montenegro, la subsecretaria de Programación y Articulación Legislativa, Silvia La Ruffa, el subsecretario de Intervenciones Federales, Luis Morales, el Director de Planificación Operativa y Monitoreo, Juan Scagna, el intendente de la ciudad de Rosario, Pablo Javkin, el intendente de la ciudad de Pérez, Pablo Corsalini, el intendente de Villa Gobernador Gálvez, Alberto Ricci, el Subsecretario de Seguridad Preventiva, Diego LLumá y el jefe de la URII, Daniel Acosta.