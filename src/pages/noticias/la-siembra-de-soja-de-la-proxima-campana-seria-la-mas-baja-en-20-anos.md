---
category: El Campo
date: 2021-07-25T06:00:15-03:00
thumbnail: https://assets.3dnoticias.com.ar/SOJA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: La siembra de soja de la próxima campaña sería la más baja en 20 años
title: La siembra de soja de la próxima campaña sería la más baja en 20 años
entradilla: En el ciclo productivo 2021/22, la superficie cultivada de soja caería
  entre 5% y 20% y en la zona núcleo se recortarían unas 250 mil hectáreas, según
  GEA.

---
La soja dejó de ser la estrella de la agricultura. Después de años de sumar hectáreas y ganárselas a otros cultivos, se prevé que en la campaña 2021/22 el área de siembra con la oleaginosa sea la más baja de los últimos 20 años.

Un informe que difundió este viernes la Guía Estratégica para el Agro (GEA) de la Bolsa de Rosario, indica que, revisando los datos de los últimos 18 años, "el hectareaje de la soja 2021/22 sería el más bajo de la serie por lejos".

Detalló que el ciclo pasado el área sojera perforó la barrera de los 5 millones de hectáreas (M/ha), precisamente 4,82 millones. "En la 2021/22 se estima que se sembrarían 4,57 M/ha, un 5% menos", agregó GEA.

Pero, además, advirtió que las hectáreas sembradas "podrían aún ser menos y terminar por agujerear la marca de los 4,5 M/ha". De hecho, en gran parte de la región núcleo "ya se señala un 10% menos de área respecto al ciclo previo".

El relevamiento que realiza GEA en las sus distintas estaciones ubicadas a lo largo de la zona núcleo productiva, también recoge la opinión de ingenieros y técnicos que trabajan en los territorios.

El informe de ayer fue taxativo: "La soja ya no está a la altura del maíz", dijo y planteó que "el nivel de rindes quedó estancado en los últimos 8 a 10 años”.

Por eso GEA estima que la caída en área sería de un 5% respecto al 2020, pero puede ser mayor, ya que en numerosas zonas de la región ya hablan de un recorte de 10%.

**Las razones de la caída**

¿Por qué se desmorona la soja? es el principal interrogante. Entre las respuestas que encuentran los especialistas está se encuentran la pérdida de márgenes brutos y la poca respuesta del cultivo frente a dos situaciones clave: en condiciones ambientales adversas los rindes son menos estables; y en condiciones buenas, los rindes se han estancado frente al maíz.

"En las encuestas de esta semana, al preguntar por qué el productor ha dejado de elegir a la reina de los cultivos, responden que hay peores márgenes, peores resultados en campañas anteriores, menores avances genéticos y mayor presión impositiva", indicó GEA, con lo cual "la reina ha perdido su trono".

En la zona de General Pinto (Buenos Aires), los ingenieros lo explican muy claro: “El año anterior pesa mucho ya que el maíz anduvo mejor en términos de rindes y también en precio". Por la falta de agua hubo sojas que rindieron 600 kg/ha, pero el maíz dejó 6.000 kg/ha.

"El que hizo soja perdió plata, el que hizo maíz, no”, agregaron los agrónomos. También señalaron que “la soja está estancada en el nivel de rindes desde los últimos 8 a 10 años”.

"Desde hace varias campañas se observa un estancamiento que se atribuye al uso de variedades consideradas viejas”, explicaron los especialistas de la zona de San Antonio de Areco.