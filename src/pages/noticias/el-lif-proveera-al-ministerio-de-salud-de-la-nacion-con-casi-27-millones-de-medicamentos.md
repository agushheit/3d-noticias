---
category: La Ciudad
date: 2021-11-01T06:15:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/lif2.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: El LIF proveerá al Ministerio de Salud de la Nación con casi 27 millones
  de medicamentos
title: El LIF proveerá al Ministerio de Salud de la Nación con casi 27 millones de
  medicamentos
entradilla: Los comprimidos producidos por el laboratorio público de Santa Fe serán
  distribuidos en su mayoría por el Programa Remediar.

---
El Laboratorio Industrial Farmacéutico (LIF), perteneciente al sistema sanitario público de la provincia de Santa Fe, proveerá al Ministerio de Salud de la Nación con casi 27 millones de comprimidos de medicamentos para tratar diversas patologías, que en su mayoría serán distribuidos por el Programa Remediar, informaron este domingo fuentes oficiales. 

  La gestión se realizó a través de la Agencia Nacional de Laboratorios Públicos (Anlap) y se efectivizará a través de la firma de tres convenios para la provisión de 26.990.140 comprimidos de 6 especialidades medicinales. 

  El entendimiento más importante es con el Programa Remediar, que recibirá 25.200.020 comprimidos de 3 medicamentos para la Atención Primaria de la Salud del país: Metformina 500mg (10.800.000 comprimidos), Cefalexina 500mg (7.200.002 comp.) y Amoxicilina 500mg (7.200.018 comp.). 

  Además, se firmará un convenio para abastecer al Instituto Nacional del Cáncer con 1.304.100 comprimidos de 2 tipos de analgésicos opioides: Morfina 10 mg (915.300 comp.) y Metadona 5mg (388.800 comp.). 

  Finalmente, el tercer contrato se rubricará con las autoridades de la Dirección Nacional de Salud Sexual y Reproductiva, con 486.000 unidades de Misoprostol de 200 mcg comprimidos vaginales. 

  El LIF indicó que el compromiso “implica un gran desafío, debido a que representan un aumento significativo en la actividad productiva, en un contexto marcado por las consecuencias de atender las demandas de la pandemia”, a la vez que continúa “el habitual abastecimiento al sistema público de salud de la provincia de Santa Fe”. 

  El programa nacional Remediar provee en forma gratuita y mensual a más de 8.100 centros de salud de todo el país de botiquines de medicamentos esenciales, que dan respuesta al 80% de las consultas de atención primaria de salud y alcanza a 16 millones de argentinos.