---
category: Agenda Ciudadana
date: 2020-11-30T11:21:34Z
thumbnail: https://assets.3dnoticias.com.ar/operativo-fiestas.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Fuerzas Provinciales y Federales comenzaron a diagramar acciones de cara
  a las fiestas de fin de año
title: Fuerzas Provinciales y Federales comenzaron a diagramar acciones de cara a
  las fiestas de fin de año
entradilla: Además, se evaluaron resultados de la puesta en marcha de acciones implementadas
  en el marco del Operativo Verano Seguro 20/21.

---
El gobierno de Santa Fe, a través del Ministerio de Seguridad, estableció un nuevo encuentro, en el marco de la Mesa Institucional de Seguridad, con representantes de fuerzas federales y la Municipalidad de Rosario, con el objetivo de evaluar los resultados de las acciones implementadas en el Operativo Verano Seguro 20/21, en tanto se comenzaron a diagramar dispositivos de cara a las fiestas de fin de año.

En referencia al Operativo Verano, tanto en Rosario como en las localidades del área metropolitana, el subsecretario de Prevención y Control Urbano, Alberto Mongia expreso: “Los resultados son más que aceptables, y a raíz de eso fuimos convocados por fiscales para comentarnos la situación, ya que hubo muchas personas demoradas y automóviles remitidos. 

Entonces, eso quiere decir que estamos haciendo un buen trabajo conjunto, entre los municipios, las fuerzas federales y la Policía de Santa Fe”.

Por otra parte, al dar detalles de las acciones con fuerzas federales, el funcionario agregó: “Estamos trabajando muy coordinados, este fin de semana seguiremos llevando adelante los operativos especiales por la temporada de verano, también junto a los municipios. 

Hace tiempo se dejaron de hacer planes de forma unilateral, ahora estamos planificando de manera conjunta, y los resultados están a la vista y son muy aceptados por la ciudadanía”.

###   
**OPERATIVO FELICES FIESTAS**

Además, las autoridades planificaron el operativo Felices Fiestas, para establecer acciones especiales relacionadas con las festividades de fin de año, para que “tanto los comerciantes como la ciudadanía, al momento de hacer las compras, se sienta seguro y más tranquilo”, precisó Mongia.

Por su parte, el coordinador de Fuerzas Federales del Ministerio de Seguridad de Nación, Santiago Faga, destacó que “estamos teniendo muy buenos resultados y mucha aceptación por parte de la ciudadanía, trabajando en diferentes puntos de Rosario, Funes, Ibarlucea, Granadero Baigorria y Pueblo Esther, donde podemos dar cuenta de resultados positivos”.

Del encuentro participaron también la coordinadora de la subsecretaría de Prevención y Control Priscila Villalobos; el subsecretario de Control y Convivencia de la Municipalidad de Rosario, Rodolfo Gorosito; el jefe de la URII, Daniel Acosta, y representantes de Gendarmería Nacional Argentina, Policía de Seguridad Aeroportuaria, y Prefectura Naval Argentina.