---
category: La Ciudad
date: 2020-12-14T13:10:41Z
thumbnail: https://assets.3dnoticias.com.ar/brindis.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Diario UNO'
resumen: Gastronómicos, a la espera de que habiliten más comensales para las fiestas
title: Gastronómicos, a la espera de que habiliten más comensales para las fiestas
entradilla: Se acerca la fecha de reuniones familiares por Navidad y año nuevo y desde
  la Asociación Gastronómica esperan flexibilizaciones para el rubro.

---
Llega fin de año, momento de despedidas, de juntarse con la familia, y si bien este año fue muy especial y por la cuarentena muchos estuvieron separados durante un largo tiempo, frente a las flexibilizaciones de los últimos tiempos en Santa Fe, la reapertura de actividades y la llegada de los festejos de Navidad y año nuevo, el sector gastronómico está expectante con lo que pueda ocurrir.

Desde el 28 de noviembre pasado, bares y restoranes pudieron ampliar su capacidad de atención. Se habilitó que de viernes a sábados puedan permanecer abiertos hasta las 1.30 y de domingo a jueves hasta las 0.30, algo que celebraron, especialmente ahora con la llegada del calor, y según el sector, el momento más fuerte del año, cuando la gente sale de su casa.

A semanas de la llegada de las fiestas desde el sector gastronómico ya tuvieron reuniones informales con el municipio, solicitando poder ampliar la cantidad de comensales permitidos, que por protocolo y hasta este momento son cuatro por mesa. "Aguardamos una decisión, en los próximos días, y tenemos la esperanza que el número de personas para compartir la mesa sea entre 8 y 10", manifestó al UNO, Joaquín Echagüe, vicepresidente de la Asociación Gastronómica local.

**En cuanto a la capacidad saben que seguirá siendo igual. El 30% de los espacios cerrados y el 50% en los espacios abiertos.** Si bien son pocas las consultas hasta el momento, ya que todavía no hay muchas especificaciones en cuanto a protocolo, los comedores y restoranes tradicionales de la ciudad ya están preparando sus propuestas, que de seguro serán presentadas la próxima semana, para que los santafesinos puedan conocer la oferta.

# **Lo que la pandemia dejó**

La llegada del calor y la extensión horaria beneficiaron al sector, que desde mediados de noviembre tiene su "temporada alta" debido a las despedidas y reuniones. "Por suerte este año también la gente acompañó", dijo Echagüe quien al mismo tiempo aseguró que aún la facturación del sector no es la esperada.

Según el último relevamiento realizado por la Asociación –que nuclea unos 150 locales–, tras la apertura de la actividad en la ciudad, unos 15 locales gastronómicos no volvieron a abrir sus puertas, de un total de 200 que existen. 

Pero para el sector se viene una nueva preocupación que haría peligrar la actividad y es la quita de ATP. "Si bien veníamos trabajando con capacidades limitadas, para el pago de sueldos se estaba utilizando la Asistencia de Emergencia al Trabajo y la Producción (ATP), que era la ayuda más importante por parte del Estado además de las prórrogas de impuestos, pero sabemos que este plan se termina y si no podemos funcionar al 100 por 100, será una nueva preocupación y alarma para el sector", finalizó el integrante de la Asociación Gastronómica local.