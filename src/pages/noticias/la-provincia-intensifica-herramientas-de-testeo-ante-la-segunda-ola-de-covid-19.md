---
category: Estado Real
date: 2021-05-24T06:00:17-03:00
thumbnail: https://assets.3dnoticias.com.ar/DETECTARjpeg.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Provincia intensifica herramientas de testeo ante la segunda ola de Covid
  19
title: La Provincia intensifica herramientas de testeo ante la segunda ola de Covid
  19
entradilla: Actualmente se realizan 6 mil hisopados por día en todo el territorio
  santafesino. Se implementan puestos fijos de test y operativos DetecAR.

---
La provincia de Santa Fe, a través del Ministerio de Salud, despliega herramientas territoriales para cortar la cadena de contagios de Coronavirus en los habitantes de todo el territorio santafesino, a través de una estrategia amplia de abordaje.

“Para hacer frente a la situación en esta segunda ola –inicialmente en las grandes urbes que más casos positivos e internaciones concentran–, y como una herramienta de detección y abordaje precoces, mientras avanzamos sin descanso en la vacunación contra el Covid-19, la provincia reactivó puestos fijos de testeo”, afirmó la ministra de Salud, Sonia Martorano.

La titular de la cartera sanitaria provincial subrayó que “en Rosario ya son siete y en Santa Fe se sumó el puesto ubicado en el Hospital Iturraspe”.

Del mismo modo, la implementación de centros de testeos se llevó adelante gracias a la instalación de nuevos laboratorios de Biología Molecular. “Desde el inicio de la pandemia trabajamos para poder descentralizar el procesamiento de testeos y pasamos de 2 a 7 laboratorios, y próximamente se sumarán otros dos en Reconquista y Venado Tuerto”, destacó en esa línea Martorano.

Cabe señalar que asimismo se desplegaron puestos fijos de testeo en los efectores de segundo y tercer nivel de atención, a los que se agregaron otros para ampliar la capacidad de análisis. De esta forma, diariamente, en la provincia de Santa Fe se realizan 6 mil hisopados.

**Operativo Detectar**

A los puestos fijos de testeo ubicados en las ciudades más grandes de la provincia, se sumaron los operativos DetecAR. En ese sentido, el coordinador de los Dispositivos Territoriales, Sebastián Torres, afirmó: “En abril y mayo se retomaron los operativos semanales todos los días hábiles del Plan DetectAR en distintas ciudades y localidades del sur provincial, realizando intervenciones simultáneas en dos o más lugares”. Y agregó: “Por mes, se están realizando 20 a 30 operativos mensuales desde que comenzamos a transitar la segunda ola”.

Es loable señalar que además de los operativos DetectAr, en cada una de las intervenciones se realizan capacitaciones para los equipos de salud locales, lo que garantiza potenciar el abordaje de cada caso de Coronavirus y actuar rápidamente para cortar la cadena de contagios.

**Puestos fijos en rosario**

Cabe recordar que los testeos en puntos fijos en la ciudad de Rosario se realizan en conjunto con el Ministerio de Desarrollo Social de la provincia y con la colaboración de voluntarios, estudiantes de las carreras de Bioquímica, Enfermería y Medicina.

Los centros de testeos de Rosario funcionan de lunes a viernes de 9 a 15 hs en

\-Monumento a la Bandera

\- Plaza del Che (27 y Bs As)

\- Oroño y Uriburu

\- Bv. Seguí y Solis

\- Granadero a Caballos y Boedo

\- Plaza O'higgins (Av. Del Rosario y Castro barros)

\- Circunvalación y Córdoba

Asimismo, los fines de semana de 9 a 17hs continúa el centro de testeos que se encuentra frente al Monumento a la Bandera.