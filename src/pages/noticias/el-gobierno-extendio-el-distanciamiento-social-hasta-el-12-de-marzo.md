---
category: Agenda Ciudadana
date: 2021-03-01T07:30:13-03:00
thumbnail: https://assets.3dnoticias.com.ar/DISPO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: El Gobierno extendió el distanciamiento social hasta el 12 de marzo
title: El Gobierno extendió el distanciamiento social hasta el 12 de marzo
entradilla: A través del Decreto 125/2021, publicado este domingo en el Boletín Oficial,
  el presidente Alberto Fernández anunció que seguirán vigentes las medidas sanitarias
  del último tiempo.

---
Justo cuando se estaba por vencer el plazo de vigencia del Distanciamiento Social, Preventivo y Obligatorio (DISPO), el Gobierno anunció que continuará rigiendo en todo el país, con las mismas restricciones que hasta ahora. Por el momento, será hasta el próximo 12 de marzo inclusive. Lo hizo a través del Decreto 125/2021, publicado este domingo en el Boletín Oficial.

Por esta razón, el Poder Ejecutivo consideró necesario que continúe vigente en todas las jurisdicciones argentinas la modalidad del DISPO, que vencía este lunes. Además, volvió a ratificar la potestad de las provincias para implementar restricciones de circulación si lo consideran necesario.

Así se mantendrá la libre circulación en todo el territorio nacional, aunque los gobernadores y el jefe de gobierno de la Ciudad Autónoma de Buenos Aires, podrán reglamentarla como consideren, estableciendo días, horas y zonas por razones epidemiológicas.

**Actividades permitidas:**

Las reuniones sociales seguirán estando permitidas cuando se realicen en espacios públicos al aire libre con una distancia mínima de dos metros, utilización de tapabocas y cumplimiento de los protocolos y a las recomendaciones e instrucciones de las autoridades sanitarias locales y nacionales.

También permanecerán funcionando todos los sectores económicos, industriales, comerciales o de servicios, en tanto posean un protocolo de funcionamiento aprobado por la autoridad sanitaria de las jurisdicciones (según las recomendaciones de la autoridad sanitaria nacional).

No obstante, para los establecimientos gastronómicos del Área Metropolitana de Buenos Aires (AMBA), la ocupación de superficies cerradas será de un máximo del 30% del aforo, mientras que en el reto del país será de hasta el 50 por ciento.

En cuanto a los deportes y actividades artísticas, podrán realizarse con cumplimiento de las pautas de higiene y cuidado y respetando los protocolos de cada una de ellas. En lugares cerrados, la concurrencia no podrá ser superior a 10 personas.