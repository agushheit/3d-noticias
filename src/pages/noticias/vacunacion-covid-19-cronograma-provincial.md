---
category: Estado Real
date: 2021-02-04T03:38:25-03:00
thumbnail: https://assets.3dnoticias.com.ar/vacuna.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: 'Vacunación COVID-19: Cronograma provincial'
title: 'Vacunación COVID-19: Cronograma provincial'
entradilla: Siguiendo lo estipulado por Nación, el Ministerio de Salud provincial
  anunció el cronograma estratégico de vacunación COVID-19, el cual será de manera
  escalonada según criterio epidemiológico de riesgo.

---
El Ministerio de Salud provincial informó el plan estratégico de vacunación contra el COVID-19 que llevará adelante de manera escalonada. Cabe destacar que este esquema es dinámico, flexible, sujeto a la situación epidemiológica y a lo acordado a nivel nacional.

En la primera etapa se comenzó con el personal de Salud y concluida la misma, se continuará con mayores de 70 años y geriátricos.

Seguidamente se inoculará a adultos de 60 a 69 años y luego continuarán en el cronograma las fuerzas armadas, de seguridad y personal penitenciario.

En otra etapa seguirán los adultos de 18 a 59 años con comorbilidades y concluirá el plan estratégico el personal docente y no docente de los niveles iniciales, primario y secundario.

Resaltamos que todas las personas con factores de riesgo que forman parte del personal estratégico serán vacunadas previamente.

Todo esto suma 1.200.000 personas que son la población objetivo. Una vez concluidas todas estas etapas, se comenzará con la vacunación para la población en general.

Es importante destacar que Santa Fe ya se encuentra preparada para vacunar. Hoy la provincia ya distribuyó a lo largo de todo el territorio provincial freezers y tiene garantizadas las condiciones necesarias para inocular a la población.

**REGISTRO DE VACUNACIÓN**

Desde el Estado provincial se está trabajando en la implementación de una página donde los ciudadanos y ciudadanas de la provincia puedan inscribirse y expresar su intención de vacunación.

El registro tiene dos propósitos: uno de ellos es saber cuántas personas se desean vacunar; y el segundo es brindarle al santafesino o santafesina que se registró, la posibilidad de elegir el lugar donde vacunarse donde se le asignará, cuando llegue el momento, un turno.