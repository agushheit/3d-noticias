---
category: Agenda Ciudadana
date: 2021-07-02T09:03:48-03:00
thumbnail: https://assets.3dnoticias.com.ar/terapiajpg.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: 'Anunciaron "un estímulo económico" al personal de Salud: ¿de cuánto será
  el bono?'
title: 'Anunciaron "un estímulo económico" al personal de Salud: ¿de cuánto será el
  bono?'
entradilla: Será por única vez y se abonará junto con los haberes del mes de junio

---
El gobierno provincial dispuso el otorgamiento de un estímulo económico a todos los miembros de la comunidad hospitalaria por las tareas asumidas en el marco de la emergencia sanitaria producto de la pandemia.

Al respecto, el ministro de Trabajo, Empleo y Seguridad Social, Juan Manuel Pusineri, sostuvo que “esta iniciativa tiene por objeto reconocer la enorme tarea que el personal de Salud viene desarrollando en esta pandemia que puso al límite nuestras capacidades, pero que también evidenció la calidad y entrega de quienes están en la primera línea de acción”.

“Su trabajo merece el reconocimiento de la sociedad en su conjunto y apostamos a que sea valorado y acompañado con nuestros cuidados individuales y colectivos, así como con el cumplimiento de las normas recomendadas por los expertos”, añadió el funcionario.

En tanto, la ministra de Salud, Sonia Martorano, indicó que “desde el gobierno de la provincia de Santa Fe queremos agradecer a todos los equipos de salud por el trabajo y la entrega que han dado en esta pandemia; y especialmente el esfuerzo durante esta segunda ola”, y señaló que este estimulo es “un reconocimiento a ello, un agradecimiento enorme a todos los trabajadores por ese esfuerzo inmenso que han hecho”.

**Escala de pagos**

El pago se efectivizará automáticamente, en forma conjunta con los haberes correspondientes al mes de junio, de acuerdo a la siguiente escala:

>> Director de Hospital, 30.000 pesos

>> Subdirector de Hospital, 20.000 pesos

>> Médicos con 24 horas o mayor carga horaria, 12.000 pesos

>> Enfermeros Cuidados Intensivos, 10.000 pesos

>> Enfermeros, 9.000 pesos

>> Profesional, 8.000 pesos

>> Técnico, 8.000 pesos

>> Personal de Mantenimiento, 7.000 pesos

>> Personal de Servicios Generales, 7.000 pesos

>> Administrativos, 7.000 pesos.