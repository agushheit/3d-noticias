---
category: La Ciudad
date: 2021-03-04T00:12:58-03:00
thumbnail: https://assets.3dnoticias.com.ar/suarez.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Carlos Suarez
resumen: "“Los vecinos de la ciudad están abandonados ante la inseguridad”"
title: "“Los vecinos de la ciudad están abandonados ante la inseguridad”"
entradilla: De esta manera se refirió el concejal de la UCR-Juntos por el Cambio Carlos
  Suárez, quien además volvió a solicitar que se reúna el Consejo de Seguridad. “El
  cuerpo aprobó de manera unánime la convocatoria a este espacio

---
El concejal de UCR-Juntos por el Cambio Carlos Suárez, volvió a solicitar a través del Concejo Municipal, que se reúna el Consejo de Seguridad de la Ciudad. El edil ingresó un proyecto de resolución, donde pide que se dé cumplimiento a la comunicación aprobada de manera unánime por el Cuerpo, que establece la convocatoria a este espacio. 

“Los vecinos están abandonados y desprotegidos ante la inseguridad. Sin dudas, y no nos cansamos de repetir, que este es el principal problema que tenemos los santafesinos. Por eso, hay que dejar de especular y dilatar tiempos y todos quienes tenemos responsabilidades legislativas y de gestión, debemos hacer nuestra parte para solucionar este flagelo”, señaló el edil. 

En esta misma línea, Carlos Suárez recordó que en el mes de octubre del año pasado, el Concejo Municipal aprobó de manera unánime la convocatoria al Consejo de Seguridad. “Este espacio permite delinear políticas para abordar esta preocupante realidad. Por eso insistimos en la importancia de instrumentarlo de manera urgente, para tener un trabajo articulado que lleve alivio a los vecinos”, indicó el concejal de UCR-Juntos por el Cambio.

El edil confió en que el Ejecutivo finalmente convoque al encuentro que reunirá a autoridades del gobierno provincial, la justicia, funcionarios municipales y legisladores. “La seguridad es una demanda diaria de los vecinos que quieren vivir tranquilos. Nuestro deber es escucharlos y actuar”, afirmó Carlos Suárez.