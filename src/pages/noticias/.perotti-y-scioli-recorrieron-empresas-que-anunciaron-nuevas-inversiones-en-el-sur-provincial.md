---
category: Estado Real
date: 2021-05-15T06:00:18-03:00
thumbnail: https://assets.3dnoticias.com.ar/PEROTTISCIOLI.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Perotti y Scioli recorrieron empresas que anunciaron nuevas inversiones en
  el sur provincial
title: Perotti y Scioli recorrieron empresas que anunciaron nuevas inversiones en
  el sur provincial
entradilla: "“Son firmas que tienen muy buenas noticias: desde la ampliación de sus
  plantas y la sustitución de importaciones, hasta la posibilidad de sumar exportaciones”,
  afirmó el gobernador junto al embajador en Brasil."

---
El gobernador de la provincia Omar Perotti, y el embajador de Argentina en Brasil, Daniel Scioli, recorrieron este viernes cuatro empresas del sur provincial ubicadas en Pérez, Rosario, Villa Gobernador y Alvear, las cuales anunciaron la generación de nuevos puestos de trabajo e inversiones por 20 millones de dólares.

“Son firmas que tienen muy buenas noticias: desde ampliación de sus plantas y sustitución de importaciones, hasta la posibilidad de sumar exportaciones”, dijo el gobernador al visitar las empresas Vipal Cauchos, Bioceres, Metalsur-Marcopolo y Randon.

"Tenemos en esta zona personal calificado. Y esta es una de nuestras características importantes: la capacidad de nuestros trabajadores en la región para incorporarse a estas plantas”, afirmó Perotti.

Respecto de la firma Bioceres, el gobernador santafesino destacó que se trata de "una señal clara de cómo hay que trabajar acompañando a las empresas cuando tienen que expandirse y abrir posibilidades de crecimiento y negocios en el mundo. Es estar a la par con nuestra Embajada, acompañar a esta empresa que tiene que abrir caminos. Es poner todo el personal técnico a disposición”.

“Entendemos que tenemos que estar cerca de nuestras empresas para que puedan expandirse y estar atentos a la captación de inversiones”, aseguró Perotti. Y subrayó: “Tenemos en el embajador Daniel Scioli a alguien que puso a disposición su equipo y cada uno de los consulados en la búsqueda de oportunidades y de negocios para Argentina. En este caso puntual y particular, para nuestra provincia”.

**Oportunidades para Santa Fe**

Por su parte, Daniel Scioli coincidió en afirmar que se trata de “emprendimientos de vanguardia y orgullo para toda la Argentina; de innovación y soluciones modernas a los distintos desafíos del presente y el futuro”.

“Nuestra agenda es muy amplia y no sólo intensa desde el punto de vista comercial, sino también de aspectos que tienen que ver con el intercambio tecnológico, de infraestructura y energía. Todo esto es posible gracias a que desde el primer momento que nos pusimos en contacto con el gobernador Omar Perotti hicimos un relevamiento de las oportunidades de la provincia de Santa Fe para con Brasil", recordó el embajador.

Finalmente, referido a su labor en el vecino país, Scioli explicó: "Mi trabajo en Brasil tuvo dos etapas: la primera fue recomponer el diálogo institucional al máximo nivel con nuestro gobierno; y ahora tiene que ver con sustituir importaciones o lograr inversiones productivas que impacten positivamente en la generación de nuevos puestos de trabajo".

**Empresa por Empresa**

En primer término, las autoridades visitaron Vipal Cauchos, en la localidad de Pérez, donde fueron recibidos por su gerente general Adalberto Fernández y el intendente local, Pablo Corsalini. Esta planta es la primera de Vipal instalada fuera de Brasil y produce bandas de rodamiento premoldeados para el renovado de neumáticos, que abastece al mercado argentino y de exportación.

La empresa generará más de 270 puestos de trabajo a través de una inversión de más de 10 millones de dólares para trasladar una línea de reparación de neumáticos y cámaras de aire, y otra de producción de goma industrial.

Luego, recorrieron las instalaciones de Bioceres, en Rosario, donde el CEO Federico Trucco les dio la bienvenida. Se trata de una empresa líder en biotecnología agropecuaria que tiene como horizonte la neutralidad en emisiones de carbono. Junto a Havanna, anunciaron recientemente un acuerdo de complementariedad científico-productivo para el desarrollo de productos con menor huella ambiental en Argentina y Brasil a partir del trigo HB4 tolerante a sequía, un desarrollo de Bioceres, la Universidad Nacional del Litoral y el CONICET que permite un uso más eficiente del agua y una mayor fijación de CO2 (Dióxido de Carbono) que un trigo convencional.

Seguidamente, Perotti y Scioli visitaron Metalsur–Marcopolo en Villa Gobernador Gálvez, cuyo gerente general Rubén Barandarian les dio la bienvenida. Comercio Exterior está desarrollando una ronda de negocios para sustitución de importaciones y desarrollo de proveedores locales para estas empresas. La firma tiene un proyecto de expansión por inversión de casi 10 millones de dólares, donde pasarán de 300 a 600 operarios en un año. Prevén bajar de 40% al 10% el uso de insumos importados.

Por último, recorrieron las instalaciones de la empresa Randon, en Alvear, donde fueron recibidos por su gerente general Diego Strafaccio. Con 27 años en el país, Randon es una de las principales fábricas de acoplados y semirremolques de Argentina, forma parte del Grupo de Empresas Randon que se encuentra entre las 10 principales fábricas del mundo. Es el principal exportador de Argentina de acoplados y semirremolques.

También participaron de las actividades el jefe de Gabinete del embajador, Julián Colombo; la secretaria de Cooperación Internacional e Integración Regional, Julieta de San Félix; el secretario de Industria, Claudio Mossuz; y el secretario de Comercio Exterior, Germán Burcher, entre otros.