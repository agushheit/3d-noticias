---
category: Agenda Ciudadana
date: 2021-06-20T06:15:40-03:00
thumbnail: https://assets.3dnoticias.com.ar/CAMARA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: El Frente de Todos intentará aprobar la reforma del Monotributo y la ley
  emergencia Covid
title: El Frente de Todos intentará aprobar la reforma del Monotributo y la ley emergencia
  Covid
entradilla: 'El oficialismo quiere realizar la sesión entre martes y miércoles ya
  que el 23 vence el Protocolo de Funcionamiento Remoto, cuya renovación siempre es
  motivo de negociación con Juntos por el Cambio. '

---
El Frente de Todos buscará acordar con bloques de la oposición un temario para realizar la semana próxima una sesión especial en la Cámara de Diputados donde se incluyan los proyectos de Monotributo y de "Emergencia Covid", la iniciativa que establece un marco normativo a partir de parámetros epidemiológicos y sanitarios para la adopción de medidas para mitigar el coronavirus.

El oficialismo quiere realizar la sesión entre martes y miércoles ya que el miércoles 23 vence el Protocolo de Funcionamiento Remoto, cuya renovación siempre es motivo de negociación entre el FdT y Juntos por el Cambio, informaron fuentes parlamentarias.

Uno de los inconvenientes para realizar la sesión el martes es que aún, un día antes, se deben hisopar los legisladores para participar en forma presencial y aun no se oficializó la citación a la reunión del pleno del cuerpo.

El proyecto de "Emergencia Covid" aprobado por el Senado sistematiza con rango de ley los criterios epidemiológicos

De todos modos, la mirada del oficialismo está puesta en las negociones que tiene con los bloques provinciales para que colaboren en la conformación del quórum para poder sancionar el proyecto de Emergencia Covid, previo a que venza el viernes 25 el DNU que rige actualmente con restricciones.

**Ley de emergencia Covid**

Ese proyecto, que ya tiene media sanción del Senado, establece un marco normativo a partir de parámetros epidemiológicos y sanitarios para mitigar el impacto de la segunda ola de coronavirus.

La bancada del Frente de Todos -que conduce Máximo Kirchner- quiere sancionar en esa sesión especial los proyectos de Monotributo, que elimina el pago del retroactivo a enero pasado, y de Emergencia Covid, pero en este último tema el interbloque opositor Juntos por el Cambio no dará quorum.

Por ese motivo, el oficialismo debe reunir los 129 legisladores necesarios para abrir la sesión sin la ayuda de la principal fuerza opositora, con lo cual deberá buscar la ayuda de los bloques provinciales.

El Frente de Todos suma 118 diputados -incluido el presidente de la Cámara, Sergio Massa, que vota sólo en caso de desempate, aunque su presencia sí se tiene en cuenta para conformar el quórum-, por lo que necesita 11 diputados más para alcanzar el piso mínimo de 129 legisladores para iniciar la sesión.

El oficialismo tiene el aval de los seis diputados que integran el bloque Unidad Federal para el Desarrollo, a cargo del mendocino José Luis Ramón, que darán quorum y votarán a favor del proyecto que tuvo dictamen hace diez días de las comisiones de Asuntos Constitucionales y Acción Social y Salud Pública.

En el interbloque Federal hay posiciones divididas ya que los diputados del bloque Justicialista -Eduardo "Bali" Bucca (Buenos Aires) y Andrés Zottos (Salta)- están de acuerdo, mientras que los cuatro legisladores de Córdoba Federal rechazan la iniciativa, pero aun no informaron si van a dar o no quórum en la sesión.

Consenso Federal, que tiene tres diputados también, tiene su propio dictamen de minoría que fue presentado por Graciela Camaño, donde rechaza el proyecto del oficialismo, pero aun no informó si va a dar o no quórum en la sesión. Tampoco lo hicieron los socialistas.

En ese contexto, en el FdT esperan avanzar en conversaciones con Acción Federal y el Movimiento Popular Neuquino, ya que si tiene el aval de los tres diputados de esas dos bancadas podrían alcanzar el quórum dado que tienen 118 del FDT, seis del interbloque de Unidad Federal para el Desarrollo y dos del bloque justicialista.

El proyecto de "Emergencia Covid" aprobado por el Senado sistematiza y busca darle un rango de ley a los criterios epidemiológicos sostenidos en los DNU emitidos por el Ejecutivo desde el inicio de la pandemia.

La iniciativa hace hincapié en el control del número de camas de terapia intensiva y el dictado de clases presenciales y sobre todo establece una serie de parámetros para definir las medidas restrictivas en los centros urbanos de acuerdo con cuatro categorías de riesgo sanitario: bajo, mediano, alto y alarma epidemiológica y sanitaria.

Estas cuatro categorías se definen a partir de tres indicadores: razón, incidencia y la tasa de ocupación de las camas de terapia intensiva en cada jurisdicción.

Determina que en las zonas declaradas en alarma epidemiológica deben implementarse medidas restrictivas a la circulación y concentración de personas, como la suspensión de la actividad de centros comerciales y ferias, al igual que de locales gastronómicos, y además queda vedada la práctica recreativa de deportes grupales de contacto en espacios al aire libre y en gimnasios.

En la iniciativa también se determina que en las zonas de alarma epidemiológica quedará prohibida la circulación entre las 19 y las seis de la mañana.

Otro de los puntos del proyecto es la suspensión, para las localidades y departamentos en alarma epidemiológica, de las reuniones sociales en domicilios particulares y en espacios públicos al aire libre si superan las diez personas; también se prohíbe la práctica recreativa de deportes en establecimientos cerrados y las actividades de casinos, bingos, discotecas y salones de fiestas, como asimismo la realización de todo tipo de eventos culturales, sociales, recreativos y religiosos que impliquen concurrencia de personas.

Por último, el texto que se aprobó en el Senado, establece que los gobernadores y el jefe de Gobierno porteño, según corresponda, podrán suspender en forma temporaria las clases presenciales y las actividades educativas no escolares presenciales conforme a la evaluación del riesgo epidemiológico.