---
category: La Ciudad
date: 2021-10-27T06:00:41-03:00
thumbnail: https://assets.3dnoticias.com.ar/colegios.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Los aumentos de cuota en colegios privados de Santa Fe llegarán al 52% en
  un año
title: Los aumentos de cuota en colegios privados de Santa Fe llegarán al 52% en un
  año
entradilla: El aumento salarial docente rubricado en el decreto provincial firmado
  por Perotti impactará en un cuarto incremento del arancel en las escuelas privadas
  provinciales, que será del 17% hasta enero.

---
Con la propuesta de aumento salarial del 17% en tres etapas que acercó el gobierno provincial a los docentes, automáticamente los colegios privados podrán incrementar el precio de su cuota. Los aumentos sucederán con el mismo porcentaje de aumento salarial de los maestros y al momento de entrar en vigencia. Se espera que el impacto en los aranceles de noviembre sea del 10%.

Luego de que el gobernador Omar Perotti firme el decreto 2176/22 que dispone el aumento salarial del 17% a pagarse en tres tramos al que adhirieron todos los gremios excepto Amsafé, dando formalmente por cerrada la paritaria la disposición 018/03 rubricada en el año 2003 autoriza a los colegios privados provinciales a actualizar el monto de su cuota. Esto, previa notificación a la dirección provincial de Educación Pública de Gestión Privada, permite actualizar la grilla de aranceles teniendo como base el sueldo de una maestra de grado que recién se inicia ($71.489,53).

Su director, Rodolfo Fabucci, dialogó con UNO Santa Fe al respecto y manifestó: "A partir del decreto firmado por el gobernador, la disposición 18 va en función al aumento salarial, pero eso debe estar en el decreto previamente habilitando a los colegios a hacer aumentos con un tope estrictamente basado en el aumento docente".

En cuanto a los aumentos en la cuota de los casi 248.000 alumnos de colegios privados santafesinos, Fabucci indicó: "Hasta ahora hemos tenido incrementos que llegaron hasta el 35% en este período y a fin de año llegará al 50%, con un 10% de octubre y un 5% más en diciembre, sumado al 2% de enero 2022 que acumularía un 52% acumulado en todo el período".

Con estos aumentos, actualmente la escala de cuotas vigente en las 850 escuelas privadas de la provincia es: escuelas con 100% de aporte estatal un máximo de $3.574, escuelas con aporte estatal del 80% el tope es de $9.651, las del 66% de aportes $12.868 de tope, 60% de aportes estatales con $13.940 de máximo y aquellos colegios con 40% de aportes del estado pueden cobrar hasta $16.085 de cuota.

Decreto sin Amsafé

Sobre la decisión del ejecutivo en cuanto a resolver la firma del decreto de aumento salarial sin tener el aval de Amsafé, Fabucci dijo que esto "es una atribución que tiene el gobernador mas allá del acuerdo o no que pueda haber con Amsafé".

En virtud de esto, cabe aclarar que luego del decreto que estipula el aumento acordado con Sadop, aceptando la oferta del 10% en octubre, 5% diciembre (impactando en aguinaldo) y 2% en enero, desde la Casa Gris se emitió otro decreto para vetar del goce del aumento salarial a los docentes públicos nucleados en Amsafé, visiblemente en contra de la propuesta que se mantiene vigente.