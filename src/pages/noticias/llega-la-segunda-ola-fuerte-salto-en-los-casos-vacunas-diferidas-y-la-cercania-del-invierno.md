---
category: Agenda Ciudadana
date: 2021-03-27T06:00:08-03:00
thumbnail: https://assets.3dnoticias.com.ar/segundaola.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: "¿Llega la segunda ola?: fuerte salto en los casos, vacunas diferidas y la
  cercanía del invierno"
title: "¿Llega la segunda ola?: fuerte salto en los casos, vacunas diferidas y la
  cercanía del invierno"
entradilla: 'En las últimas 24 horas se registraron 12.936 nuevos contagios por COVID-19
  en la Argentina, el número más alto para una jornada en más de dos meses. '

---
Según indica el parte epidemiológico, de momento, en Argentina hay 3.508 personas con coronavirus internadas en terapia intensiva. El porcentaje de ocupación de camas a nivel nacional es del 55,1% y en el Área Metropolitana de Buenos Aires (AMBA) del 58,6%.

Con estas cifras, cada vez se confirma más el hecho que la segunda ola del coronavirus llegó al país. La proximidad cada vez más inminente del invierno hace que el cuadro sea más preocupante para las autoridades sanitarias, que en las últimas horas informaron a través del Consejo Federal de Salud (COFESA) que se diferirán las segundas dosis de las vacunas para inocular a la mayor cantidad posible de personas y aminorar el impacto de la segunda ola de coronavirus, lo cual volvió a abrir el debate científico aún latente alrededor de la estrategia de espaciar la segunda dosis de las vacunas contra el COVID-19.

Los casos diarios aumentaron en el último mes, en la ciudad de Buenos Aires, se dispararon un 48% en sólo 19 días. También hay positivos reportados en las provincias de Buenos Aires, Tucumán, Entre Ríos, Corrientes, San Juan, Santa Cruz, Santa Fe, Santiago del Estero, y Tierra del Fuego.

Según comentó a Infobae el investigador en bioinformática del Conicet y la Universidad Nacional de Córdoba, el doctor Rodrigo Quiroga, que analiza los datos de vigilancia epidemiológica del Ministerio de Salud de la Nación, “son casi cuatro semanas de crecimiento de casos confirmados en 10 jurisdicciones. La curva va aumentando lentamente y de manera consistente”.

En este contexto, este medio consultó a infectólogos para pedirles su valoración en relación a cómo analizan la situación epidemiológica actual y de qué manera está parado el sistema sanitario argentino para hacerle frente al virus SARS-CoV-2 a un poco más de un año de la irrupción de la enfermedad pandémica y ante la ya instalada segunda ola.

De acuerdo al doctor Mariano Sasiain, médico infectólogo del Hospital Militar Central Cirujano Mayor Dr. Cosme Argerich, “Argentina está ante un incremento de casos constante. Esto se puede ver en las últimas semanas, en donde no hace muchos días atrás estábamos manejando entre 4 a 5 mil casos, la semana pasada un promedio de 8 mil y ahora tuvimos hoy un registro de casi 13 mil casos. Es evidente y esperable este incremento, acorde a lo que ha pasado en otros países y regiones del mundo en los meses previos, por lo cual sabíamos que esto podía ocurrir”.

“Teniendo en cuenta que se ha flexibilizado mucho la conducta social en relación a las medidas de cuidado ya que hoy hay muchas reuniones, mucha gente en la calle, en comercios, muchas veces sin utilizar mascarilla, esta relajación justifica en parte este incremento de casos y representa una preocupación grande porque uno puede esperar que si no se toman medidas a tiempo el ascenso sea aún mayor”, reflexionó el profesional.

Para Sasiain, “hoy por hoy tenemos un nivel de ocupación de camas ya sea de salas de internación general como de terapia intensiva tanto en el sector público como en el privado bastante alto y sin dudas este aumento puede verse reflejado en las próximas semanas y el riesgo es el colapso sanitario”.

“Por otro lado, la vacunación lamentablemente en nuestro país todavía viene siendo lenta, como para alcanzar niveles de protección adecuados, cosa de que se permita reducir la circulación del virus. Tendremos que volver a hablar y a reforzar como el año pasado la importancia de las medidas de cuidado, de aislamiento social y de lavado de manos porque va a ser la única medida para contener al coronavirus este año, en la medida que la vacunación no avance a los niveles que deseamos”, opinó Sasiain.

Pablo Bonvehí, médico infectólogo jefe de la Sección Infectología y Control de Infecciones del CEMIC alertó: “Argentina está efectivamente en un ascenso en el número de casos que ya podemos considerar que es una segunda ola de COVID-19, por la forma que está ascendiendo en las últimas semanas y particularmente hoy”.

“El sistema sanitario indudablemente está mejor preparado que el año pasado, con mucho más entrenamiento, más conocimiento del manejo de los pacientes, aunque aún por ahora prácticamente sin ningún tratamiento efectivo salvo el uso de corticoides, pero también se sabe cómo proceder desde el punto de vista respiratorio en los pacientes más graves, lo cual no quiere decir que la situación no se pueda desbordar.

Para el infectólogo, “el elemento que ayuda indudablemente es que gran parte del personal de salud está vacunado. Eso va a permitir que haya menos casos en este sector, y que las bajas que teníamos por esta problemática sean considerablemente menos. De todas formas, si sigue aumentando el número de casos y personas internadas podría generarse un desborde por la necesidad de camas de internación. Por eso es muy importante tratar de vacunar lo antes posible a las personas de riesgo: adultos mayores y aquellos con factores de riesgo”.