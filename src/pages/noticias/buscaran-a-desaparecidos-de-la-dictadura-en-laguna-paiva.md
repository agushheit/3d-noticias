---
category: La Ciudad
date: 2021-10-17T06:00:55-03:00
thumbnail: https://assets.3dnoticias.com.ar/SANPEDRO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Buscarán a desaparecidos de la dictadura en Laguna Paiva
title: Buscarán a desaparecidos de la dictadura en Laguna Paiva
entradilla: Se inspeccionará el campo San Pedro donde 11 años atrás se realizó el
  hallazgo de una fosa clandestina con los restos de ocho militantes asesinados.

---
Este lunes se llevará adelante la primer etapa de sondeos, en búsqueda de restos de desaparecidos en el Campo Militar San Pedro, ubicado en Laguna Paiva en el departamento La Capital, según informó el Foro Contra la Impunidad y por la Justicia de Santa Fe.

De acuerdo a la organización, el Equipo Argentino de Antropología Forense (EAAF) contará con el apoyo de geólogos de la Universidad de La Plata, quienes realizaron previamente un escaneo aéreo del lugar con un equipo LIDAR. También los organismos de Derechos Humanos realizaron su aporte con información proveniente de nuevos y valiosos testimonios brindados por peones rurales de la zona.

"Consideramos que todo esto nos ubica en las mejores condiciones como para poder obtener resultados favorables, luego de los 11 años transcurridos desde el hallazgo de una fosa clandestina con los restos de ocho militantes asesinados y desaparecidos por la dictadura", expresaron en un comunicado.

Asimismo adelantaron que se prevén varias intervenciones de este tipo en lo que queda del año. "Los testimonios presentados a la Justicia, reafirman que el Campo San Pedro fue utilizado por el Ejército como un verdadero centro regional de exterminio y enterramietos clandestinos, sin sobrevivientes", agregaron.

"Aspiramos, y seguiremos luchando para recuperar los restos de todos los compañeros y compañeras, los mejores de la generación diezmada, que aun permanecen desaparecidos por la dictadura genocida y vendepatria", concluyeron.