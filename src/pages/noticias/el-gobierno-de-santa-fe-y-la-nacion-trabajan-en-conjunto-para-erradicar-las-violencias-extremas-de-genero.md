---
category: Estado Real
date: 2021-02-25T09:00:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/violencia.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: El Gobierno de Santa Fe y la Nación trabajan en conjunto para erradicar las
  violencias extremas de género
title: El Gobierno de Santa Fe y la Nación trabajan en conjunto para erradicar las
  violencias extremas de género
entradilla: Autoridades provinciales se reunieron con representantes de los ministerios
  nacionales de Seguridad; de las Mujeres, Géneros y Diversidad; de Seguridad; y de
  Justicia y Derechos Humanos.

---
Autoridades del gobierno de la provincia de Santa Fe junto a integrantes de los distintos poderes del Estado se reunieron este martes con funcionarias de los ministerios de Mujeres, Géneros y Diversidad; de Seguridad; y de Justicia y Derechos Humanos de la Nación. El encuentro tuvo como fin articular acciones para prevenir y abordar la problemática de la violencia con motivos de género en la provincia y en el país.

Santa Fe es la primera provincia en la cual el Ministerio de Mujeres, Géneros y Diversidad presentó su Plan Nacional de Acción contra las violencias por motivos de género.

"La reunión fue muy importante, ya que es la primera que hacemos en 2021 con una de las jurisdicciones con las que estamos trabajando", aseguró Sabrina Calandrón, subsecretaria de Derechos, Bienestar y Género del Ministerio de Seguridad de la Nación. "Presentamos las líneas principales de trabajo y evaluamos la situación de la provincia y sus intereses, poniendo especial atención en las estrategias de prevención y el abordaje integral de la problemática. La recepción fue muy buena y pudimos explicar de forma acabada nuestros objetivos", concluyó la funcionaria de la cartera que encabeza Sabina Frederic.

Mientras tanto, Celia Arena, secretaria de Estado de Igualdad y Género de la provincia, sostuvo que “la idea es continuar el trabajo en conjunto que ya venimos llevando a cabo y articular acciones en el marco del Programa Interministerial de abordaje integral de las violencias extremas por motivos de género. Destacamos que Nación haya elegido a Santa Fe entre las primeras para trabajar en este abordaje que se presentará en todo el país. Por nuestra parte, es un compromiso del gobernador Omar Perotti trabajar para que cada mujer y persona de la diversidad sexual pueda vivir una vida libre de violencias”.

Por su parte, la subsecretaria de Programas Especiales contra la Violencia por Razones de Género del Ministerio de las Mujeres, Géneros y Diversidad, Carolina Varsky, analizó: “Existe la necesidad de garantizar el acceso a la Justicia y mejorar tanto el sistema de toma y tramitación de denuncias como la velocidad de respuesta en los casos de alto riesgo". Luego agregó: "Creamos una estrategia federal unificada y diseñada para el abordaje de estas violencias con la idea de potenciar la capacidad de las agencias del Estado tanto nacionales, como provinciales y municipales encargadas de diseñar las políticas públicas para las problemáticas de las mujeres y de las personas del colectivo LGBT+”.

En la misma línea se expresó Pablo Barbuto, subsecretario de Política Criminal del Ministerio de Justicia y Derechos Humanos, quien explicó que “el Ministerio de Justicia y Derechos Humanos tenía ya acciones concretas en la materia, pero a partir de este programa, vamos a poder articular y hacer más eficientes las políticas públicas”.

Representando al gobierno de la provincia de Santa Fe estuvieron presentes las subsecretarias de Mujeres, Géneros y Diversidades de la Secretaría de Estado de Igualdad y Género, Florencia Marinaro; de Bienestar y Género en la Policía del Ministerio de Seguridad provincial, Natacha Guala; de Relaciones Institucionales, Lorena Battilana; y la coordinadora del Equipo de Protección Integral Contra las Violencias, Ornela Grossi.

En representación del Ministerio de Seguridad, además, asistieron las directoras nacionales de Políticas de Género, Carolina Justo Von Lurzer, y de Estadística Criminal, Ángela Oyhandy. Por parte del Ministerio de las Mujeres, Géneros y Diversidad también estuvieron presentes Laurana Malacalza, subsecretaria de Abordaje Integral de las Violencias por Razones de Género; Ana Clara Piechestein, directora de Abordaje Integral de Casos de Femicidios, Travesticidios, Transfemicidios y Delitos contra la Integridad Sexual; Lorena Balardini, directora Técnica de Registros y Bases de Datos. Y por el Ministerio de Justicia y Derechos Humanos de la Nación, intervinieron Nadia Rivas y Hernán Olaeta.