---
category: La Ciudad
date: 2021-12-31T06:00:49-03:00
thumbnail: https://assets.3dnoticias.com.ar/LIMPIEZA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Así serán los operativos para Año Nuevo en la ciudad de Santa Fe
title: Así serán los operativos para Año Nuevo en la ciudad de Santa Fe
entradilla: Desde la Municipalidad detallaron cómo se desarrollarán los respectivos
  trabajos para la noche del 31 de diciembre y el 1° de enero.

---
Tomando como base los operativos realizados durante la Noche Buena y Navidad en la ciudad de Santa Fe, desde la Municipalidad dieron detalles de cómo se llevarán a cabo los controles reforzados para este fin de año.

 En relación a la seguridad y el tránsito, Fernando Peverengo, secretario de Control de la municipalidad indicó que “será el mismo operativo de base, pero reformulando puntos sensibles”. Respecto a esto, hizo alusión a inconvenientes ocurridos en la zona de Calcena y avenida Almirante Brown, donde se colocará mayor cantidad de personal policial.

 Sobre el tránsito, detalló que los cortes serán similares a los de la noche del 24 y la mañana del 25. La costanera oeste, este y el Puente Colgante permanecerán peatonalizados desde las 22:00 horas del 31 hasta las 08:00 del 1°.

 Por otro lado, el sendero seguro hacia los boliches volverá a estar habilitado y aseguraron que habrá también mayor presencia de agentes con el objetivo de prevenir siniestros viales, principalmente en los horarios de salida de los locales bailables.

 Al igual que en Navidad, se realizarán operativos de alcoholemia en diferentes puntos de la capital provincial, articulando trabajos con la Agencia Provincial de Seguridad Vial para la zona de la Ruta 1 durante toda la noche. Asimismo, Peverengo indicó que se realizarán test de alcoholemia diurnos el 31 y el 1°.

 **Cómo será el operativo de limpieza**

 Teniendo en cuenta una posible mayor convocatoria de personas para las festividad del próximo fin de semana, el Secretario de Ambiente y Cambio Climático de Santa Fe, Franco Ponce de León, detalló que se incluirán contenedores en puntos claves de la ciudad.

 Además, sobre la costanera este se colocarán campanas, motivo por el que Ponce de León le pidió a la ciudadanía que “se apropien del espacio público, pero a la hora de consumir las bebidas, luego las coloquen en los contendores para poder disfrutar”.

 Los operativos de limpieza “post año nuevo” iniciarán desde las 7:00 horas del 1° de enero, por lo que se coordinó con la Unidada Regional 1 que para dicho horario se libere completamente la calzada pavimentada en las zonas de cada convocatoria.