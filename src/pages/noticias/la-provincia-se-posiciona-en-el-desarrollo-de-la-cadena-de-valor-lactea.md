---
category: El Campo
date: 2021-03-05T05:59:16-03:00
thumbnail: https://assets.3dnoticias.com.ar/lacteo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: " La provincia se posiciona en el desarrollo de la cadena de valor láctea"
title: La provincia se posiciona en el desarrollo de la cadena de valor láctea
entradilla: "“Ante el escenario internacional, Santa Fe se coloca en una posición
  de privilegio, con perspectiva de mejorar la ecuación económica de toda la cadena
  de valor”, destacó Abel Zenklusen."

---
El Ministerio de Producción, Ciencia y Tecnología, a través de la Dirección Provincial de Lechería y Apicultura de la Secretaría de Agroalimentos, en conjunto con la Secretaría de Comercio Exterior, y por indicación del gobernador Perotti, lleva adelante una fuerte política de promoción para que las industrias lácteas comercialicen sus productos en el mercado externo, instancia que prevé desarrollar todos los eslabones de la cadena de valor.

En ese marco, entre las iniciativas impulsadas, se destaca el Programa de Asociatividad para Exportar orientado a pequeñas y medianas empresas que tiene por objetivo que Pymes con potencial para exportar o eventualmente exportadoras, dispongan de asesoramiento personalizado por parte de un asistente técnico para contribuir a su proceso de internacionalización.

También, se llevan adelante rondas virtuales internacionales de negocios entre empresas santafesinas del sector lácteo e importadores de otros países, iniciativas que durante 2020 previeron rondas con países como China y Chile, con muy buenos resultados. Esta propuesta, la primera y única en todo el país, se continuará realizando con otros mercados.

El director provincial de Producción Lechera y Apícola, Abel Zenklusen, destacó que “las exportaciones santafesinas de productos lácteos lideran el mercado nacional, principalmente impulsadas por el crecimiento en un 70% de los despachos de leche entera en polvo, con un volumen de 42.9 toneladas, y un valor de 129 millones de dólares”.

“Este producto -continuó-, en los últimos días, llegó al precio más alto en los últimos cinco años a nivel mundial, 4.364 dólares por tonelada. En ese escenario, la provincia se coloca en una posición de privilegio, con perspectiva de mejorar la ecuación económica de toda la cadena de valor y le brinda un horizonte esperanzador al productor primario, que hoy se encuentra en un escenario complicado”.

Además, Zenklusen indicó que “uno de los principales objetivos, de la Secretaría de Agroalimentos, tal como nos encomendó el ministro Daniel Costamagna, es aumentar las exportaciones lácteas. En Santa Fe se produce el 28% de la leche del país, estamos embarcados en una cuenca sumamente rica y con gran potencial, que además tiene alto impacto en la economía y en el arraigo territorial. En la provincia existen hoy 3.600 tambos y más de 100 empresas que industrializan leche, distribuidas de forma heterogénea en la provincia y en gran parte de perfil pequeñas y medianas”.