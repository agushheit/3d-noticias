---
category: La Ciudad
date: 2021-08-10T06:15:07-03:00
thumbnail: https://assets.3dnoticias.com.ar/badcops.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'Nuevo reclamo de vecinos de Siete Jefes: se incrementó la violencia y la
  inseguridad'
title: 'Nuevo reclamo de vecinos de Siete Jefes: se incrementó la violencia y la inseguridad'
entradilla: Elevaron una nota a la Municipalidad y al Ministerio de Seguridad de la
  provincia solicitando que se efectivicen diversas medidas.

---
En una nota dirigida a funcionarios policiales, municipales y provinciales, la Vecinal del Barrio Siete Jefes reclamó que se cumplan una serie de medidas ante los numerosos hechos de inseguridad que azotan a ese sector de la ciudad, los que se repiten desde hace mucho tiempo.

“Con mucha preocupación y tristeza vemos un aumento en el número de casos de inseguridad de nuestro barrio, así como también un encarnizamiento cada vez mayor en el daño que se provoca a las víctimas de estos hechos”, expresa el comunicado.

En los últimos meses, “desde la vecinal hemos tomado varias medidas con respecto a este tema: visibilizando nuestra problemática en los medios de comunicación, elevando notas a las autoridades correspondientes, realizando un mapa delictual que facilite el diseño de estrategias adecuadas por parte de la policía y los funcionarios a cargo de la seguridad, denunciando en el Ministerio Público de la Acusación (MPA) los tiroteos”, no obstante lo cual “todo parece poco a la hora de que nuestra situación tenga una solución seria”.

En ese sentido, los vecinos reclamaron la necesidad “urgente” que “el gobierno cumpla con sus obligaciones y concrete acciones con respecto a la inseguridad”, y alegan que “el Estado no puede alegar la falta de recursos ni incapacidad”.

“Nos resistimos a normalizar un panorama en el que las personas mayores son arrastradas y lastimadas, en la que nuestros niños viven situaciones traumáticas en la calle cuando tienen derecho a una infancia tranquila y protegida, a que las mujeres no puedan caminar en paz y de que el hecho de trasladarse en bicicleta por nuestro barrio se haya convertido en un riesgo de vida”, expresaron.

Por todo esto, redactaron una nota a la Mesa de Seguridad de la ciudad de Santa Fe, la que cuenta con la firma de 869 vecinos y en la que manifiestan los siguientes reclamos:

**A LA MUNICIPALIDAD**

· Mayor control sobre motovehículos.

· Sanciones más severas a aquellos conductores de motos que circulen sin patente o papeles del vehículo

· Recuperación de la policía comunitaria en los barrios

· Iluminación en la zona de la ciclovía.

· Actualización y puesta en funcionamiento de alarmas comunitarias y cámaras de videovigilancia

**AL MINISTERIO DE SEGURIDAD**

· Mayor circulación de policías, que trabajen junto con la vecinal diseñando tareas sobre todo en aquellas zonas y horarios más peligrosos.

· Solución inmediata del problema de usurpaciones ilegales en los terrenos del ferrocarril, así como también el impulso de las investigaciones correspondientes acerca de los participantes en los tiroteos ocurridos dentro de nuestro barrio. (Expediente N° 21 08589257 3 – Denuncia realizada por la vecinal de barrio 7 Jefes)

· Parquización, patrullaje y limpieza de los predios del ferrocarril.

· El otorgamiento de mayores instrumentos y equipamientos a miembros de la seccional 3ra. a fines de que puedan realizar mejor sus funciones.

· Apoyo al gobierno municipal en cuanto a la denuncia y el secuestro de vehículos sin papeles.