---
category: La Ciudad
date: 2020-12-11T11:30:24Z
thumbnail: https://assets.3dnoticias.com.ar/parque-industrial.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: El Litoral'
resumen: Piden cerrar el Parque Industrial por contaminación peligrosa para la salud
title: Piden cerrar el Parque Industrial por contaminación peligrosa para la salud
entradilla: El pedido de clausura preventiva lo realizó el fiscal federal Walter Rodríguez
  debido a que se detectó que hay nueve empresas que contaminan. Hay 26 personas citadas
  a indagatoria.

---
La Fiscalía Federal N° 2 de Santa Fe, a cargo del fiscal Walter Rodríguez, solicitó en las últimas horas la declaración indagatoria de 26 personas por el delito de contaminación ambiental peligrosa para la salud, y además que se clausure de manera preventiva el Parque Industrial de Sauce Viejo, ubicado a unos 22 kilómetros de la capital de esa provincia.

**La presentación fue realizada ante el Juzgado Federal N°2 de esa jurisdicción, y según se informó, los hechos imputados tienen origen en la actividad desarrollada por nueve empresas con asiento en ese Parque Industrial.**

No obstante, fuentes cercanas al juez subrogante Marcelo Baialaque aclararon que la decisión no es inminente y no hay un pedido urgente de la Fiscalía, en tanto que la semana próxima el magistrado se abocará a analizar la cuestión.

**La investigación, según publicó el Ministerio Público Fiscal de la Nación, se inició en 2016** tras una denuncia en la que se hizo alusión al vertido de distintos productos químicos en el río Coronda, sin tratamiento previo, provenientes del Parque Industrial.

![](https://assets.3dnoticias.com.ar/parqueindustrial.jpg)

La misma situación había sido manifestada anteriormente por vecinas y vecinos del Barrio Jorge Newbery, quienes alegaron la existencia de fuertes olores y el deterioro de los techos de sus casas provocado por sustancias arrojadas al desagüe que se encuentra sobre el lateral norte, dentro del Parque, que desemboca en el río Coronda.

Durante la investigación se tomaron muestras de los efluentes líquidos provenientes de las actividades del Parque entre el 10 y 12 de diciembre de 2019, y en esa ocasión los interesados fueron invitados a presenciar el acto y a ofrecer profesionales de su confianza para controlar la prueba.

Según se indicó, los estudios arrojaron como resultado la existencia en el río Coronda de elementos cuyas concentraciones fueron muy superiores a las permitidas por la [Ley 24.051 de Residuos Peligrosos](http://servicios.infoleg.gob.ar/infolegInternet/anexos/0-4999/450/texact.htm "Texto de la ley").

**Para la fiscalía federal, los sucesos investigados exceden a un individuo aislado y afectan a toda la comunidad que está en contacto con el medio ambiente.**