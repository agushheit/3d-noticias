---
category: Agenda Ciudadana
date: 2020-12-04T11:29:47Z
thumbnail: https://assets.3dnoticias.com.ar/PRESIDENTE.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: El Litoral'
resumen: 'Fernández en Santa Fe: sin anuncios oficiales y con dudas sobre la "nueva
  normalidad"'
title: 'Fernández en Santa Fe: sin anuncios oficiales y con dudas sobre la "nueva
  normalidad"'
entradilla: 'El presidente de la Nación, Alberto Fernández, llegó este jueves a la
  provincia de Santa Fe, luego de reprogramar su agenda por la muerte de Diego Maradona. '

---
De esta manera, cumplió con su primera visita oficial a la ciudad de Venado Tuerto, en el departamento General López, para mostrarse cerca de dos sectores claves para la reactivación económica del país: el agro y la industria.

Sobre el mediodía del jueves, el Tango 11 (Learjet 60) aterrizó en la pista del aeródromo “Tomás B. Kenny”, donde el mandatario argentino arribó acompañado de los ministros de Economía, Martín Guzmán; de Desarrollo Social, Daniel Arroyo; y de Agricultura, Luis Basterra. Previamente, lo había hecho el embajador suizo en el país, Heinrich Schellenberg.

Fernández fue recibido por la vicegobernadora, Alejandra Rodenas; el intendente Leonel Chiarella; y el senador Lisandro Enrico, entre otras autoridades, quienes lo flanquearon en su recorrido que incluyó una visita a una reconocida planta industrial motopartista, y el desembarco en el predio de una multinacional vinculada a la tecnología agraria. Como dato relevante, hay que mencionar que no hubo (como sí se había anunciado en su anterior visita) manifestaciones en rechazo a su presencia.

## **Anuncios por otro lado**

Si bien en las primeras horas de la jornada se conocieron una serie de anuncios vinculados a la visita, públicamente no se hizo mención alguna. El primero de ellos, tiene que ver con la disponibilidad de 980 millones de pesos para la ampliación de Corven Motos, para la incorporación de una línea de montaje con la cual podrán iniciar la fabricación local de la marca Kawasaki, que generará nuevos puestos de trabajo.

El otro dato, es que las compañías Syngenta y Sinograin Oils Corporation van a vender a China 1,2 millones de toneladas de soja originaria de Argentina y Uruguay, por un monto superior a los 500 millones de dólares. También se anticipó un plan de inversiones en el país por 25 millones de dólares hasta 2022.

## **Normalidad injusta**

A las 13:30 Fernández se presentó a cielo abierto en el predio de Syngenta, a la vera de la Ruta 8, donde puso el eje de su discurso en los desafíos presentes y futuros del país. Afirmó que hoy fue un día singular, porque “estamos terminando el peor momento de la pandemia”, para luego agradecer el trabajo de los médicos en su día. Tras un aplauso cerrado de autoridades y funcionarios, habló de un “esfuerzo enorme de los profesionales de la salud”, a quienes entregó su gratitud.

En este orden, recordó que también fue el Día Internacional de las Personas con Discapacidad, en el que se pone en valor “reclamar una argentina que brinde oportunidades a todos y los trate del mismo modo”.

Fernández, sostuvo luego que al país y al mundo les pasó “algo que no les pasa habitualmente”, porque el planeta se “desplomó” con un virus que hasta el día de hoy nadie sabe cómo curarlo. En este orden, destacó que “toda nuestra vida se alteró” y en esa alteración “muchos reclamaban volver a la normalidad”.

“Yo me pregunto siempre si quieren volver a esa normalidad injusta. ¿Qué es volver? ¿A un mundo donde la concentración de la riqueza está en manos de pocos y la pobreza en millones? No tengo ningún interés en volver a la normalidad que conocimos porque hizo un mundo central y otro periférico; y una argentina central y otra periférica”, destacó.

En esta línea, aclaró que la pandemia de COVID-19 nos dio la oportunidad de construir “otra normalidad” y “otra argentina para todos”, que no deje a nadie en la vereda. “Y se construye del modo que dicen todos. El desafío es otro y el conocimiento es central. Por eso acá estamos en el lugar donde la investigación, la ciencia y la tecnología se aplica a la agricultura que es la base del alimento”, agregó.

## **Educación y sueños**

En otro tramo, valoró haberse formado en la educación pública e instó a que la mayoría siga ese camino. De hecho, acotó que la formación es “central” para que podamos crecer. “El Estado tiene que estar presente, presuroso y ágil para que a ningún argentino le falte la atención. Tenemos que encontrar en este nuevo mundo un lugar donde desarrollarnos. De eso se trata el gran desafío que tenemos”, valoró.

También, apuntó a seguir exportando producción y que para eso vino al sur santafesino. “Estamos aquí para reafirmar una vez más eso. ‘Con la democracia se come y se educa”, decía Raúl Alfonsín. Y tenía razón. Pero es una deuda que tenemos, tal vez nunca lo logramos porque no fuimos capaces de pensar un mundo juntos. La pandemia nos permite pensar en esa importancia. El dilema es crecer o no crecer. Podemos hacerlo si nos damos cuenta que eso está en nuestras manos”, ponderó.

Y para cerrar, invitó a todos a “soñar”, parafraseando a Antonio Cafiero: “El que sueña solo, solo sueña. El que sueña con otros, hace historia. Soñemos juntos”.

## **Vinculados con el exterior**

El director general de Syngenta para Latinoamérica Sur, Antonio Aracre, admitió que resultó muy “emocionante e histórico” para la empresa este marco, en el mes que cerraron el vigésimo aniversario de la empresa de tecnología para el agro desde Suiza, donde está la casa matriz. “Hace 3 años tenemos nueva identidad por la empresa química China más grande del mundo, que nos abrió las puertas a nuevos mercados que piden alimentos de calidad de un planeta que requiere de eso de manera sustentable”, explicó.

El empresario, admitió que la Embajada Argentina en Suiza, acompañó y los incorporó como productores de granos. De esta manera, señaló que los productores argentinos entraron al mercado chino y que se “mejoró la competitividad”, garantizando un tamaño eficiente al Estado en ingresos y egreso de divisas.

“Tenemos grandes expectativas de crecimiento en los próximos años para muchos argentinos. Por eso estamos invirtiendo más de 25 millones de dólares en las dos plantas de Venado Tuerto. Es una inversión tangible y otra en gastos de investigación y desarrollo buscando soluciones cada vez más innovadoras. Venado es la cuna y casa del campo más competitivo, algo que no ocurre en otros países del mundo”.

Por otra parte, subrayó que en miras al futuro va a haber una “revolución de trabajo” nueva en la argentina y el mundo. “Esto me lleva a pensar que estamos cumpliendo con las nuevas generaciones. La respuesta es ciencia y tecnología”.

## **Desde casa**

El gobernador Omar Perotti, estuvo presente a la distancia a través del sistema de videoconferencia. Agradeció la visita de Fernández y la “buena semblanza de estos días”, tras la reunión con industriales en Rosario del miércoles, de la que participó el ministro de Desarrollo Productivo, Matías Kulfas. “Tuvimos en Santa Fe un capítulo de la Unión Industrial, que agradeció el acompañamiento de este año. Más del 60 por ciento recuperaron el nivel de actividad pre pandemia y dejaron de lado las dicotomías. Esto nos lleva al pleno convencimiento que hay que sumar y apostar al crecimiento de Argentina”, destacó.

Perotti, puso énfasis en la capacidad de los industriales para transformar la materia prima, porque “tenemos algo muy valioso que es la capacidad y conocimiento de nuestra gente”. “La Argentina es un jugador fuerte. Estamos en el corazón del clúster semillero y este compromiso va más allá de la inversión, que es valiosa. El compromiso es generar una vinculación con el exterior. Hay una provincia que quiere ser referente”, sostuvo.

Tras cartón, agradeció a los empresarios presentes por “invertir en Argentina y Santa Fe”, poniendo de relieve que son “una empresa que tiene tecnología y mano de obra intensiva”. “Si conseguimos sumar día a día, los momentos que nos van tocando se tornan en esperanza. En Santa Fe estamos sembrando futuro. Las señales son positivas y en ese camino estamos”.

## **De color**

El paso de Fernández por primera vez en Venado Tuerto, dejó dos claras notas de color. La primera de ellas, tiene que ver el intendente Leo Chiarella, que en representación de la comunidad venadense, le ofrendó a Fernández la partitura de la Marcha San Lorenzo en versión guitarra, tomando nota de la pasión del presidente por la música y el instrumento de cuerdas.

La emblemática partitura musical de la marcha que regaló Chiarella al presidente de los argentinos fue realizada por el maestro y director de la Banda Municipal, Ezequiel Fernández. El presente en su interior contiene la historia de la Marcha San Lorenzo y de su creador, Cayetano Silva. También incluye un pen drive con tres versiones de la histórica marcha: una instrumental de la Banda Municipal, otra del Coro local Juan Sebastian Bach, y la tercera, en guitarra, ejecutada por Diego Castelli.

La otra anécdota, tiene que ver con una familia que aguardó por su llegada desde las 9 de la mañana en el aeródromo. Con paciencia y casi “por favor” ante la fuerte custodia oficial, lograron llegar hasta Fernández para hacerle entrega de un regalo más que especial: dos camas para los perros del presidente.

Ambas colchonetas, llevan los nombres de las mascotas “oficiales”: Dylan y Procer, y según la mujer, fueron confeccionadas por ella misma con ayuda de su hija y esposo. Visiblemente emocionada, la mujer se fundió en un abrazo con Alberto Fernández y le dejó un saludo super especial para la Primera Dama, Fabiola Yañez. “Dígale a Fabiola que la amo”, cerró la señora.