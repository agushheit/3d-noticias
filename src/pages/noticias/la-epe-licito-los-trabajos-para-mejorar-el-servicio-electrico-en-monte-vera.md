---
category: Estado Real
date: 2021-06-01T08:32:01-03:00
thumbnail: https://assets.3dnoticias.com.ar/epe.png
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa EPE
resumen: La EPE licitó los trabajos para mejorar el servicio eléctrico en Monte Vera
title: La EPE licitó los trabajos para mejorar el servicio eléctrico en Monte Vera
entradilla: La obra, que cuenta con un presupuesto oficial superior a los $ 56 millones,
  beneficiará a los vecinos de la localidad y estimulará el desarrollo productivo
  de la zona.

---
El directorio de la Empresa Provincial de la Energía (EPE) celebró de manera virtual, el acto de apertura de los sobres para la construcción de obras eléctricas que beneficiarán a usuarios de la zona sur de Monte Vera, en el departamento La Capital, obra que cuenta con un presupuesto oficial de 56.984.909,62 pesos.

**CUATRO OFERTAS**

En la oportunidad, cuatro empresas presentaron sus ofertas: MEM Ingeniería, cotizó los trabajos en 65.050.180,71 pesos; Bauza Ingeniería, en $ 75.647.204,41 pesos; Cocyar, en $ 72.871.778,49 pesos; y Electromecánica Tacuar, en 78.383.947,62 pesos.

El proceso continuará con el análisis de las propuestas técnicas y económicas por parte de una comisión interdisciplinaria que sugerirá la empresa adjudicatoria de los trabajos.

**LA OBRA**

La EPE remodelará las líneas de media tensión de 13,2 kV existentes, a través del cambio de conductores, postaciones nuevas por las que están en mal estado y demás materiales necesarios para incrementar la confiabilidad y capacidad del servicio eléctrico, atendiendo al gran crecimiento de la demanda.

Los trabajos proyectados contemplan intervenir en 17,4 kilómetros de líneas de media tensión, construir nuevos puestos aéreos de transformación y remodelar 650 metros de las instalaciones de baja tensión.

**BENEFICIOS PARA EL SECTOR PRODUCTIVO**

El presidente de la EPE, Mauricio Caussi, destacó que “esta obra es importante para el sector productivo de la zona, para apuntalar a las economías regionales y es un esfuerzo que hace la empresa para mejorar la calidad del servicio eléctrico”.

“Estamos respondiendo a la premisa planteada por el gobernador (Omar Perotti) de acercar la EPE al sector productivo” aseguró Cuassi, y valoró “el acompañamiento de las firmas contratistas en este proceso de inversión”.

Por su parte, el senador provincial, Marcos Castello, remarcó que “hace un tiempo que venimos bregando para que Monte Vera sea considerada ciudad, para lo cual es importante garantizar la infraestructura indispensable”.

Respecto de la obra, el legislador consideró que “va a permitir mejores condiciones para los vecinos y la radicación de emprendimientos productivos. Esta gestión es de territorio, no de oficina. Esta predisposición, permite rápidamente modificar situaciones y son las noticias que queremos transmitir a los vecinos”, concluyó.

**FONDO DE ELECTRIFICACIÓN RURAL**

Cabe recordar que la Ley Nº 13.414 creó el Fondo de Electrificación Rural (FER) con el objeto de dotar del servicio eléctrico al sector rural, en particular eN las regiones y pobladores más aislados del territorio santafesino, “que sean declaradas de electrificación rural obligatoria y en las que exista un estado de necesidad de infraestructura eléctrica”, sean pobladores rurales con o sin explotaciones agropecuarias, plantas industriales radicadas en la zona rural y centros urbanos pequeños.