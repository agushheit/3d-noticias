---
category: Agenda Ciudadana
date: 2021-12-05T06:00:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/democraciaday.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Cómo serán los festejos por el Día de la Democracia
title: Cómo serán los festejos por el Día de la Democracia
entradilla: "La convocatoria será el próximo viernes 10 de diciembre en la Plaza de
  Mayo y tendrá proyección regional ya que el acto culminará con tres protagonistas
  centrales: Alberto Fernández, Cristina Kirchner y Lula da Silva.\n\n"

---
El Gobierno prepara para el próximo viernes 10 de diciembre una jornada de festejos en la Plaza de Mayo para celebrar el Día de los Derechos Humanos, por los 38 años del regreso de la democracia al país y el fin de la dictadura cívico-militar, y en coincidencia con el día en que se cumplen dos años de la asunción del presidente Alberto Fernández.  
  
La convocatoria culminará con tres protagonistas centrales que hablarán desde un escenario montado ante la Casa Rosada: el exmandatario brasileño y probable candidato en las presidenciales del año próximo, Luiz Inácio Lula Da Silva; la vicepresidenta Cristina Fernández de Kirchner, y el propio jefe de Estado como orador de cierre.  
  
La jornada, cuyos detalles fueron confirmados a Télam por fuentes calificadas del Ejecutivo, prevé presentaciones musicales en vivo de artistas de Argentina y de Brasil e incluirá un momento de homenaje y reivindicación a personalidades ligadas a los derechos humanos cuando se entreguen los Premios Azucena Villaflor.  
  
Se trata de una distinción honorífica creada por la Secretaría de Derechos Humanos que fue suspendida durante el gobierno de Mauricio Macri pero que volvió a ser organizada con la gestión del Frente de Todos y cuya edición anterior, de 10 de diciembre de 2020, se realizó en la exESMA, a cielo abierto por la pandemia, con la presencia del Presidente y de la vicepresidenta, entre otros funcionarios.  
  
Esta vez, la presencia de Lula será la figura excluyente de la jornada, tanto por el panorama electoral de Brasil, con sondeos que lo muestran como protagonista ineludible para los comicios del año próximo, como por la relación de amistad que el líder del Partido de los Trabajadores mantiene con el presidente Fernández.  
  
Ese vínculo humano explica por qué hace dos semanas Lula llamó por teléfono desde Madrid para proponerle al jefe de Estado compartir un acto juntos en la Argentina, con una concurrencia similar a la del 17 de noviembre, que él había visto por televisión: "Quiero ir a Buenos Aires y hacer algo así con vos", fue la propuesta del expresidente de Brasil, según confiaron a Télam fuentes que manejan detalles de la jornada y de sus preparativos.  
  
Fernández y Lula se conocen desde hace veinte años y mantienen una relación cercana que en julio de 2019 sumó un episodio determinante, cuando el entonces candidato a presidente del FdT -hoy al mando del Ejecutivo- viajó al penal de Curitiba para visitar junto al excanciller brasileño Celso Amorim al fundador del PT, por entonces preso, y señalar a su salida de la cárcel que el exmandatario de Brasil era "víctima de una prisión arbitraria".  
  
Fernández se pronunció en varias oportunidades sobre la situación de Lula mientras el exgremialista de los metalúrgicos de San Pablo estuvo encarcelado, e incluso se le atribuye haber hecho gestiones para que el brasileño, una vez recuperada la libertad, fuera recibido por el Papa Francisco en el Vaticano en febrero de 2020.  
  
Hace quince días, cuando Lula transmitió su deseo de visitar la Argentina para compartir un acto con las figuras centrales del FdT, desde la Casa Rosada le dijeron que el momento más adecuado para hacerlo era el 10 de diciembre, porque en esa fecha se conmemora "el día de la Democracia y los Derechos Humanos", efeméride que el peronismo suele recordar con festivales musicales en la Plaza de Mayo.  
Cuando la propuesta comenzó a tomar forma, desde el Gobierno plantearon que la próxima llegada de Lula -se lo espera para el jueves 9- tendrá un componente simbólico dado que el propio expresidente de Brasil "es un representante de ambas cosas", en referencia a los valores de la democracia y los derechos humanos.  
  
La expectativa con la que el FdT apuesta a los festejos del 10 de diciembre en Plaza de Mayo quedó en evidencia este sábado, luego de que el jefe del bloque del oficialismo en Diputados, Máximo Kirchner, exhortara a "reventar en serio la Plaza" el próximo viernes para "abrazar a alguien que, como Cristina, sufrió la persecución judicial y que volverá a ser presidente de Brasil", en referencia a Lula.  
  
La jornada del viernes, previo a los discursos, se extenderá por varias horas con la participación de artistas y el espíritu de un festival, y en un momento de la tarde se galardonará a personalidades comprometidas en la defensa de los DDHH y en la lucha por Memoria, Verdad y Justicia, como Estela de Carlotto, Taty Almeyda, el exsenador y abogado defensor de presos políticos Hipólito Solari Yrigoyen, un dirigente de extracción radical que en 1973 sufrió un atentado de la Triple A, entre otros.  
  
En cuanto a la repercusión de la visita de Lula, sobre todo en el actual gobierno brasileño que encabeza Jair Bolsonaro, desde el oficialismo señalaron que el referente del PT que entre 2003 y fines de 2010 fue presidente de ese país se limitará a "sumarse a un acto que ya estaba armado".