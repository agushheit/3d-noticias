---
category: Agenda Ciudadana
date: 2021-11-03T06:00:42-03:00
thumbnail: https://assets.3dnoticias.com.ar/terrotorio.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: El gobierno de Santa Fe financiará la incorporación de abogados a los Centros
  Territoriales de Denuncia
title: El gobierno de Santa Fe financiará la incorporación de abogados a los Centros
  Territoriales de Denuncia
entradilla: "La medida, ratificada este martes a través del decreto 2360 firmado por
  el gobernador Omar Perotti, abarca las circunscripciones de Santa Fe, Rosario, Venado
  Tuerto, Rafaela y Reconquista. \n\n"

---
El gobierno de la provincia de Santa Fe aprobó a través del Decreto 2360 -firmado este martes 2 de noviembre por Omar Perotti- los convenios entre el Ministerio de Seguridad y los colegios de abogados de las cinco circunscripciones judiciales y autorizó a la Dirección General de Administración del Ministerio de Seguridad a liquidar y pagar a a favor de cada uno de los Colegios de Abogado las sumas que corresponden como contraprestación por la ejecución de cada uno de los convenios.

 Según los convenios, firmados el 27 de agosto último por representantes del ministerio de Seguridad provincial y de los colegios profesionales de Santa Fe, Rosario, Venado Tuerto, Reconquista y Rafaela,  a fines de conformar un equipo de trabajo integrado por el personal administrativo y/o profesional dispuesto por el cartera, los foros de abogados remitirán al Ministerio un listado de Abogados matriculados que cumplan con una serie de requisitos previstos (ver los acuerdos en el PDF a continuación).

 Los colegios realizarán la preselección indicada y la remitirán al Ministerio, que verificará el cumplimiento de los requisitos y realizará entrevistas personales a los efectos de defínir un orden de mérito que permitirá ir incorporando profesionales a medida que sean necesarios. 

 Los profesionales seleccionados deberán desarrollar una jornada diaria mínima de 6 horas, de lunes a viernes, en dos turnos con horarios de 8 a 14  y de 14 a 20, por el término improrrogable de 12 meses, en los centros territoriales de denuncias asignados o con posibles traslados, según las necesidades de la Dirección de los Centros Territoriales de Denuncias.

 Como contraprestación, el Ministerio abonará a los colegios una suma mensual equivalente a cuatro unidades JUS por cada profesional que preste funciones en los Centros Territoriales de Denuncias y cinco unidades JUS por cada profesional que se desempeñe como coordinador.