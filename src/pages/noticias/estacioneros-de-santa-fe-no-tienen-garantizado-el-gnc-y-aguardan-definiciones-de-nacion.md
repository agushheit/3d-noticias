---
category: La Ciudad
date: 2021-04-07T07:55:37-03:00
thumbnail: https://assets.3dnoticias.com.ar/gnc.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Diario Uno
resumen: Estacioneros de Santa Fe no tienen garantizado el GNC y aguardan definiciones
  de nación
title: Estacioneros de Santa Fe no tienen garantizado el GNC y aguardan definiciones
  de nación
entradilla: Hay preocupación entre los empresarios del sector por lo que sucederá
  a partir de mayo. Finalizan los contratos anuales y no saben si contarán con el
  suministro y el precio de comercialización

---
Hay preocupación e inquietud en las estaciones de GNC de la provincia de Santa Fe. Los empresarios del sector no saben si contarán con el suministro a partir de mayo, ni tampoco el precio al cual deberán comercializarlo.

"Estamos urgidos, estamos a 20 días de que a nuestro producto no lo tengamos asegurado; ni en cantidad, ni en precio", así lo aseguró Dalmiro Saux, integrante de la Cámara de Expendedores de Combustibles y Afines de Santa Fe, a UNO.

"Nosotros tenemos abastecimiento contractuado hasta fin de abril. Lo que va a pasar en mayo no lo sabemos", añadió.

Más allá de la incertidumbre que inunda a los estacioneros, el referente se mostró optimista con que el problema se pueda sortear en las próximas semanas y así evitar que un millón de conductores de autos se queden sin gas.

El empresario comentó que "normalmente, a esta altura del año nosotros ya teníamos firmado los contratos de abastecimientos para todo el año". Explicó que el "gas se saca de tres cuencas: dos australes (fueguina y neuquiina) y una del norte (NOA), que proviene que Bolivia, sobre la cual hay poco suministro y no se sabe precio".

Destacó que, por "un equilibrio de los gasoductos", Santa Fe y Córdoba se deben abastecer de ambas cuencas. Dicha situación condiciona a la provincia de Santa Fe ya que el gasoducto del NOA no está entregando. "No hay gas y no hay precio", afirmó Saux.

Además, advirtió que "el gobierno debe realizar acuerdos con Bolivia para garantizar la provisión. Ieasa, empresa estatal, debe comprarle gas a Bolivia a precio internacional y venderlo a precio interno. La otra opción son los buques gasificadores".

El actual escenario obliga a los estacioneros a salir del servicio tarifado (no le pueden comprar a las empresas distribuidoras como por ejemplo Litoral Gas, sino que deben comprar en "comercializadoras". En este sentido, las empresas del sur del país aseguran el gas proveniente de los gasoductos australes pero no así quienes deberían comercializar el gas del norte (por la faltante antes mencionada).

El empresario consultado explicó que "gas del sur no va a venir, si es que no llega del NOA. Por eso, necesitamos que el Estado, el gobierno nacional, acuerde con Bolivia o con los barcos gasificadores y que brinde, a través de Ieasa, el gas de la cuenca NOA".