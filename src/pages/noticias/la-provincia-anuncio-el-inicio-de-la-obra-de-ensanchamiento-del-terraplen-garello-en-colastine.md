---
category: Estado Real
date: 2021-07-13T07:35:22-03:00
thumbnail: https://assets.3dnoticias.com.ar/terraplen-garello.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: La provincia anunció el inicio de la obra de ensanchamiento del terraplén
  Garello, en Colastiné
title: La provincia anunció el inicio de la obra de ensanchamiento del terraplén Garello,
  en Colastiné
entradilla: Los trabajos comenzarán la semana que viene. Las tareas comprenden engrosamiento
  del terraplén y mejoras de las condiciones de seguridad de la defensa ante eventuales
  crecidas. La obra demandará tres meses de ejecución.

---
El Ministerio de Infraestructura, Servicios Públicos y Hábitat, a través de la Secretaría de Recursos Hídricos, anunció el inicio de la obra de ensanchamiento del terraplén de la Defensa Garello, del barrio Colastiné en la ciudad de Santa Fe, en el sector de la toma de agua y del tramo colapsado de la protección.

Con esta acción se logrará una sección del terraplén más robusta, con un coronamiento más amplio, lo que contribuirá a mejorar la seguridad de la defensa ante crecidas, hasta que se concrete la obra de estabilización de margen, de la cual, el ensanchamiento se constituye en la primera etapa.

En este sentido, el secretario de Recursos Hídricos, Roberto Gioria, brindó detalles de esta primera etapa de la obra definitiva de la protección del terraplén: “Hasta que se concrete la obra principal del terraplén, lo que hacemos en esta primera etapa es actuar dando una respuesta en el corto plazo, aumentando el cuerpo del terraplén. Esto consiste en un ensanchamiento en la zona colapsada, aumentando la seguridad del terraplén ante alguna crecida del río Paraná que pudiese venir en el mientras tanto. Al coronamiento se lo va a ensanchar alrededor de 12 a 15 metros según el lugar”.

Con respecto del proyecto ejecutivo de la protección completa de la obra principal, el secretario Gioria informó que “la consultora contratada a través del Consejo Federal de Inversiones (CFI), ya entregó el informe final, el cual contiene el proyecto ejecutivo. Ahora, este informe está en un período de análisis por parte de nuestra Secretaría y del CFI, al cual se le harán observaciones o se lo aprobará directamente, según lo que corresponda”.

**LOS TRABAJOS**

Las tareas que deberán realizarse en esta primera etapa corresponden a la limpieza de la zona de obra extrayendo la vegetación del paramento seco y la remoción el suelo cemento del canal de guarda. Luego se efectuará una ampliación del cuerpo del terraplén sobre el paramento seco y coronamiento de la obra de defensa contra inundaciones con compactación. Posteriormente se recubrirá con suelo humífero, y se ejecutarán tareas para reconstruir el canal de guarda y se repondrá y/o se construirá el alambrado en caso de ser necesario.

Cabe recordar que el gobierno provincial licitó la obra, con un presupuesto oficial de 36.693.366,48 millones de pesos, donde se presentaron 10 ofertas y la obra se adjudicó a la firma Río Salado S.R.L. que cotizó $ 25.686.177,93.