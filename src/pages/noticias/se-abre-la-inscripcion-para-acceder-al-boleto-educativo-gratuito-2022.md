---
category: Agenda Ciudadana
date: 2022-02-16T10:34:23-03:00
thumbnail: https://assets.3dnoticias.com.ar/BOLETOEDUCATIVO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3d noticias
resumen: Se abre la inscripción para acceder  al Boleto Educativo Gratuito 2022
title: Se abre la inscripción para acceder  al Boleto Educativo Gratuito 2022
entradilla: Comenzará el lunes 21 de febrero. Además, hasta que se complete la inscripción
  al presente año, a partir de dicha fecha podrán acceder al boleto gratuito quienes
  hayan sido beneficiarios en el año 2021.

---
l gobierno de la provincia de Santa Fe informó que el lunes 21 de febrero se abrirá la inscripción para acceder al Boleto Educativo Gratuito 2022. En ese marco, el director provincial de Boleto Educativo y Franquicias, Rober Benegui, describió que “la particularidad de este año es que deben inscribirse todas aquellas personas que quieran ingresar al programa, tanto quienes no lo hayan usado el año pasado como también quienes hayan tenido Boleto Educativo Gratuito en el 2021. El objetivo es actualizar datos”.

Por otro lado, en simultáneo con la apertura de inscripción al programa para este año, “a partir del lunes 21 de febrero, van a poder viajar de modo gratuito todos aquellos beneficiarios que hayan tenido la franquicia activa durante el año 2021, mientras se verifican los datos de la inscripción al año 2022”, agregó.

El Boleto Educativo Gratuito tiene por objeto garantizar la llegada de estudiantes, docentes y asistentes escolares de los establecimientos educativos de toda la provincia, eliminando así una potencial barrera de acceso a la educación. A partir de su implementación, el costo completo del viaje lo abona la provincia de Santa Fe.

Son beneficiarios del Boleto Educativo Gratuito los estudiantes de todos los niveles: inicial, primario, secundario, terciario, universitario, tanto públicos como privados. Además, en una reivindicación de la labor docente, que también son beneficiarios del programa junto con los asistentes escolares.

En el caso de los estudiantes, el requisito es ser alumno regular de los establecimientos educativos declarados y los estudiantes universitarios tienen que haber aprobado una materia en el último año. En el caso de los docentes tienen que estar prestando efectivamente servicios.

La inscripción se realiza de forma 100% online, a través de la página web del gobierno de la provincia de Santa Fe www.santafe.gob.ar/boletoeducativo o a través de la aplicación Boleto Educativo en el celular. “No es necesario presentar ninguna documentación en ninguna oficina. Sí en Rosario hay tres oficinas de atención al usuario para acompañar en el trámite de inscripción: sede de Gobierno, Terminal de Ómnibus y CasArijón. En la ciudad de Santa Fe tenemos dos centros de atención al usuario: Fábrica Cultural El Molino y en la Terminal de Ómnibus”, concluyó Benegui.