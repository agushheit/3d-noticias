---
category: Agenda Ciudadana
date: 2021-01-17T10:37:11Z
thumbnail: https://assets.3dnoticias.com.ar/monotributo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Diario Uno
resumen: La Afip extendió el plazo para la recategorización en el monotributo hasta
  el 31 de enero
title: La Afip extendió el plazo para la recategorización en el monotributo hasta
  el 31 de enero
entradilla: La resolución general que será dictada en los próximos días no afectará
  la obligatoriedad de la recategorización. Los contribuyentes deberán registrarse
  en la categoría que se adecúe a sus parámetros.

---
La Afip extendió el plazo previsto para la recategorización en el monotributo hasta el 31 de enero de 2021. La resolución general que será dictada en los próximos días no afectará la obligatoriedad de la recategorización. Los contribuyentes deberán registrarse en la categoría que se adecúe a sus parámetros.

El objetivo es ofrecer alivio ante la situación de incertidumbre frente a la recategorización con las tablas vigentes.

Las categorías y alícuotas solo pueden adecuarse por una norma con rango de ley. Esa adecuación está prevista en un proyecto de ley remitido al Congreso.

Todos los potenciales perjuicios de los que se hicieron eco distintos actores serán subsanados en el proyecto de ley

Durante el debate parlamentario se incorporarán modificaciones que resuelven tales cuestiones con el fin de garantizar que ningún monotributista se vea impedido de acceder a los beneficios y previsiones del proyecto.