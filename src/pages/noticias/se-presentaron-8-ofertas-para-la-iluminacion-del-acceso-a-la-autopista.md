---
category: Agenda Ciudadana
date: 2021-04-18T06:15:53-03:00
thumbnail: https://assets.3dnoticias.com.ar/ILUMIN.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Se presentaron 8 ofertas para la iluminación del acceso a la autopista
title: Se presentaron 8 ofertas para la iluminación del acceso a la autopista
entradilla: Con un presupuesto provincial superior a 187 millones de pesos, la obra
  completará un tramo de 8172 metros, entre ruta Nacional 19 y el puente sobre el
  río Salado.

---
La ministra de Infraestructura, Servicios Públicos, y Hábitat, Silvina Frana, abrió este viernes los sobres con las ofertas económicas para la iluminación de la autopista AP01 “Brigadier General Estanislao López” en el ingreso a la ciudad de Santa Fe. La obra se ejecutará desde el kilómetro 146, en la intersección con ruta Nacional Nº19, acceso a Santo Tomé, hasta el kilómetro 154, en el estribo oeste del puente sobre el río Salado.

Para la realización de los trabajos, que tienen un presupuesto oficial de $187.551.623,22, se presentaron 8 empresas oferentes.

En la oportunidad, Frana destacó la realización del acto, en un lugar al aire libre y público, lo que “nos permite hacer cada vez más transparente las gestiones, en este esquema que nos indica permanentemente el gobernador Omar Perotti”.

Asimismo, señaló que “el área metropolitana que circunda Santa Fe tuvo un crecimiento exponencial hacia este lugar y esto requiere la respuesta de los Estados con obras concretas, como el intercambiador o la iluminación, que hoy estamos licitando. No tengan dudas que el gobierno de Santa Fe todos los días redobla sus esfuerzos para que las cosas sucedan y la gente tenga las respuestas que necesita”.

En este sentido, Frana destacó que la obra “se enmarca en un conjunto de licitaciones, que van a demandar un compromiso financiero del Estado provincial de más de 2500 millones de pesos, en distintas áreas: obras viales, viviendas, desagües, cloacas”.

Por último, la ministra agradeció “al sector empresario que sigue confiando en un Estado que demostró que cuando se compromete, cumple”; y destacó “este vínculo entre lo privado y lo público que genera empleos y respuestas”.

**Las ofertas**

Las empresas que ofertaron fueron Bauza Ingeniería SRL, que cotizó por $ 271.575.158,82; MEM Ingeniería SA, por $ 230.771.710,85; Tecsa SA, por $ 273.015.460,87; Coemyc SA, por $ 256.327.053,85; Nestor Julio Guerechet SA, por $ 328.267.244,82; Mundo Construcciones SA - Guillermo Andrés Duarte, UT Iluminacion Autopista, por $ 241.297.333,18; la UT conformada por Rovial SA y Del Sol Constructora SA, por $ 274.887.176,51; y la última correspondió a Proyección Electroluz SRL, por $ 224.558.173,07.

**La obra**

El proyecto ejecutivo fue elaborado por la Dirección Provincial de Vialidad, dependiente del Ministerio de Infraestructura, Servicios Público y Hábitat, y contó con informe de factibilidad de la Empresa Provincial de Energía.

La obra tiene un plazo de ejecución de 10 meses y entre las tareas a ejecutar, se va a intervenir la línea aérea de media tensión, instalar transformadores de potencia, retirar y reubicar columnas existentes, realizar desbosque y limpieza de terreno, colocar barandas metálicas de defensa y conformar estabilizado granular.

Cabe destacar, que actualmente existen luminarias en el kilómetro 153 y en inmediaciones del estribo oeste del puente sobre el río Salado; y que la obra en desarrollo sobre el intercambiador del acceso Norte a Santo Tomé (km 151) contempla la construcción de un nuevo sistema, de tal manera en el desarrollo se completará el segmento entre los kilómetros 146 y 154.

Participaron también del acto el asesor del Ministerio de Infraestructura, Servicios Públicos y Hábitat, Daniel Ricotti; y representantes de las empresas oferentes.