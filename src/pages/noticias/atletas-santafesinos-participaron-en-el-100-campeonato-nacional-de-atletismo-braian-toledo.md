---
category: Deportes
date: 2020-12-26T12:45:08Z
thumbnail: https://assets.3dnoticias.com.ar/atletismo.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: 'Atletas santafesinos participaron en el 100° Campeonato Nacional  de Atletismo
  «Braian Toledo» '
title: 'Atletas santafesinos participaron en el 100° Campeonato Nacional  de Atletismo
  «Braian Toledo» '
entradilla: La provincia de Santa Fe ofició como organizadora y protagonista en las
  competencias.

---
Con la participación de 200 inscriptos de todo el país, se realizó el 100° Campeonato Nacional de Mayores «Braian Toledo» en el Estadio Municipal Jorge Newbery de la ciudad de Rosario.

**La provincia de Santa Fe ofició como organizadora y  protagonista en las competencias.**

El ministro de Desarrollo Social, Danilo Capitani expresó que «torneos de atletismo como al que estamos asistiendo, y el cual estamos apoyando, son los que permiten a muchos de los jóvenes de nuestra provincia mostrar su preparación y su talento de cara a eventos futuros, ya que son la plataforma para competencias a nivel superior e internacional».

Por su parte, la Secretaria de Deportes, Claudia Giaccone manifestó: «Desde el gobierno de la provincia, y específicamente de la cartera de Deportes, venimos trabajando para fomentar las actividades deportivas y recreativas, con una mirada inclusiva puesta en todos los jóvenes, ya que no solo los vemos como las futuras promesas, sino también como generadores de valores que deben de expandirse e imitarse».

<br/>

<span style="font-family: helvetica; font-size: 1.35em; font-weight: 800;"> **Provincia de Atletas**</span>

**Santa Fe es una de las provincias que más atletas aporta a las delegaciones de Argentina que participan en torneos internacionales** y no es casualidad, existe un gran trabajo de sus instituciones y sus deportistas y entrenadores denotan gran esfuerzo y perseverancia para mejorar día a día, incluso en tiempos de pandemia.

A la primera medalla de oro del evento se la llevó la atleta local Carolina Lozano en los 3.000 metros con obstáculos, y en la misma prueba y tras un final vibrante, el santafesino Carlos Johnson quedó segundo por centésimas detrás del entrerriano Julián Molina. Por su parte, el joven lanzador de martillo oriundo de La Sarita y representante argentino en los Juegos Olímpicos de la Juventud Buenos Aires 2018, Julio Nóbile, consiguió también una plateada.

En salto en alto, Celina Harte, y en 20.000 metros marcha Sofía Kloster, Santa Fe sumó dos medallas de oro. En tanto, en la rama masculina de esta última prueba, el atleta de Nicanor Molina, Sebastián Giuliani fue medalla de plata.

En salto con garrocha, Germán Chiaraviglio ganó su 13er campeonato nacional y la rama femenina tuvo en lo más alto del podio a las santafesinas Luciana Gómez Iriondo seguida de Aldana Garibaldi, lo que marca un gran desarrollo de la disciplina en nuestro territorio.

José Zabala, medio fondista de Humboldt, quedó segundo en 1.500 metros y tercero en 5.000, y Daniel Penta logró la presea de plata en 800 metros. Por su parte, Carolina Lozano en los 1.500 metros, el venadense Pablo Zuliani en 200 y 400 metros llanos y la rosarina Celina Moyano en 400 fueron medallas de bronce.

Además, Ignacio Fontana quedó primero en Decatlón, y Brian López en salto en largo logró la medalla de bronce al igual que el rosarino Juan Pablo Sale en lanzamiento de bala.

<br/>

<span style="font-family: helvetica; font-size: 1.35em; font-weight: 800;"> **El Certamen**</span>

Las actividades, que se realizaron entre el 18 al 20 de diciembre en el Estadio Municipal Jorge Newbery de la ciudad de Rosario, llevó el nombre del atleta olímpico Braian Toledo, de 26 años, quien murió producto de un accidente de tránsito en el mes de febrero, en Marcos Paz, su localidad natal. Medalla de oro en los Juegos Olímpicos de la Juventud Singapur 2010, fue finalista olímpico en Río 2016 y se estaba preparando para participar en Tokio 2020.