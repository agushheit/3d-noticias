---
category: Estado Real
date: 2021-02-02T06:39:08-03:00
thumbnail: https://assets.3dnoticias.com.ar/colectivos.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: La Provincia autorizó a las empresas de transporte interurbano de pasajeros
  a actualizar sus tarifas
title: La Provincia autorizó a las empresas de transporte interurbano de pasajeros
  a actualizar sus tarifas
entradilla: El último aumento se produjo en agosto de 2019.Teniendo en cuenta la crisis
  originada por la pandemia mundial Covid 19 y sus consecuencias económicas y sociales,
  la provincia fijo un tope de incremento del 30 por ciento.

---
A partir de la hora cero de este miércoles 3 de febrero, las empresas de transporte interurbano de pasajeros de la provincia están autorizadas a incrementar las tarifas en hasta un 30 por ciento. Respecto de esta disposición, desde la Secretaría de Transporte de la provincia recordaron que el último aumento se produjo en agosto de 2019.

Desde esa la fecha, tomando en cuenta el incremento de todos los costos vinculados al sector, la suba de la tarifa debería alcanzar un 63 por ciento, siendo el combustible el principal insumo que sufrió aumentos importantes en el último tiempo. A fin de sostener la tarifa durante 2020, el gobierno santafesino incrementó los subsidios al transporte interurbano de pasajeros en un 40 por ciento. 

No obstante, teniendo en cuenta la crisis originada por la pandemia del Covid 19, que produjo una importante caída de los recursos, menor actividad económica, menor recaudación, y teniendo en cuenta el contexto social por el que atraviesa la población, la provincia dispuso que este incremento no supere el 30 por ciento.