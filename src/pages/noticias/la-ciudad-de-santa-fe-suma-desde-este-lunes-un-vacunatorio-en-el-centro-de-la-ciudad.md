---
category: La Ciudad
date: 2022-01-17T06:15:56-03:00
thumbnail: https://assets.3dnoticias.com.ar/nuevovacunatorio.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: La ciudad de Santa Fe suma desde este lunes un vacunatorio en el centro de
  la ciudad
title: La ciudad de Santa Fe suma desde este lunes un vacunatorio en el centro de
  la ciudad
entradilla: 'El gobierno provincial trabaja con entidades sindicales para sumar lugares
  de vacunación con la idea de acelerar la aplicación de terceras dosis.

  '

---
El gobierno de [**Santa Fe**](https://www.unosantafe.com.ar/santa-fe-a21902.html) habilitará a partir del lunes un nuevo [**vacunatorio**](https://www.unosantafe.com.ar/vacunatorio-a54197.html) en pleno centro de la capital de la provincia, que en principio recibirá a trabajadores de la Empresa Provincial de la Energía (EPE), de Aguas Santafesinas y a personal de [**Seguridad**](https://www.unosantafe.com.ar/seguridad-a38307.html).

Voceros del Ministerio de Salud señalaron que el nuevo local funcionará en las instalaciones de la Obra Social de los Empleados de Comercio y Actividades Civiles (Osecac), situado en avenida Rivadavia 2898.

Desde la cartera sanitaria indicaron que la idea es que ese vacunatorio sea utilizado en el futuro para cualquier persona que resida o trabaje en las cercanías.

Algo similar ocurrirá en la ciudad de Rosario, en ese caso en la sede del Sindicato de Luz y Fuerza, ubicada en Paraguay 1135, donde la ministra de Salud, Sonia Martorano, anticipó que también se inoculará en una primera etapa a los trabajadores de Aguas, la EPE y de la Policía.

El gobierno provincial trabaja con otras entidades sindicales para sumar lugares de vacunación con la idea de acelerar la aplicación de terceras dosis a afiliados y a la comunidad en general.

En cuanto a la actividad de hoy, funcionaron los tres principales vacunatorios de la provincia: en La Rural de Rosario se aplicaron 2.000 unidades de Pfizer (terceras dosis), en tanto en la capital provincial hubo actividad en La Esquina Encendida (1.160 terceras dosis de Moderna) y en La Redonda (920 terceras dosis de Pfizer).

El último dato oficial sobre la campaña indica que el 83% de la población general de Santa Fe alcanzó las dos dosis de vacunas y que el 20% tiene la terceras dosis.