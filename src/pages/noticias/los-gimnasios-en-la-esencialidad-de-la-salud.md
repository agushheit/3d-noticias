---
category: Agenda Ciudadana
date: 2021-04-14T07:35:55-03:00
thumbnail: https://assets.3dnoticias.com.ar/simnasios.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Ximena García
resumen: " Los gimnasios en la esencialidad de la salud"
title: " Los gimnasios en la esencialidad de la salud"
entradilla: La diputada nacional Ximena García presentó un proyecto para que se reconozca
  como actividades esenciales a las prácticas físicas y deportivas desarrolladas en
  gimnasios, natatorios y clubes.

---
La diputada nacional Ximena García presentó un proyecto para que se reconozca como actividades esenciales a las prácticas físicas y deportivas desarrolladas en gimnasios, natatorios y clubes por ser fundamentales para el goce pleno del derecho a la salud.

“Estamos hablando de espacios en los que se realizan actividades deportivas, se promueven hábitos saludables, se previenen enfermedades, se fomentan los lazos sociales, muchos de los componentes que contribuyen al desarrollo de una vida plena”, explica García, apuntando que dichas actividades encuadran en la definición de la Organización Mundial de la Salud (OMS) que determina que “la salud es un estado de completo bienestar físico, mental y social, y no solamente la ausencia de afecciones o enfermedades”.

No es la primera vez que la diputada García habla sobre esta temática, ya en el 2020, había presentado un proyecto con una serie de medidas en apoyo al sector, en los difíciles meses que atravesaba a mediados de la cuarentena.

La iniciativa busca mantener abiertas las puertas de los más de 7900 gimnasios y clubes deportivos que dan trabajo a 80.000 personas en relación de dependencia, y emplean a más de 180.000 profesores de educación física e instructores de diversas disciplinas. Al respecto la legisladora puntualizó: “Estamos muy preocupados por esta situación, comprendemos la gravedad del panorama que atraviesa el país con una segunda ola de infecciones, pero esta vez nos encontramos mejor preparados para enfrentarla, los protocolos de higiene y distanciamiento llevan un año ya, desde su implementación y tanto la ciudadanía como los dueños y dueñas de establecimientos han aprendido a convivir en esta nueva normalidad, aceptando las pautas de cuidado”.

Cabe destacar que en algunos casos la actividad deportiva en gimnasios de algunas de algunas provincias jamás se retomó desde hace más de un año, y en aquellas que si lo hicieron, estos estuvieron sujetos a los vaivenes propios de las coyunturas, donde ante la aparición de un caso positivo de COVID-19 se ha vuelto atrás en varias ocasiones. “Esta situación, solo profundiza la crisis económica que atraviesan las PyMEs y los monotributistas en nuestro país, y vemos caer una industria que hoy ayuda a miles de personas a sobrevivir. Por ello, solicitamos que se reconozca de una vez por todas, a la actividad deportiva como esencial e intrínseca a la salud humana”, concluyó la diputada García.