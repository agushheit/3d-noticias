---
category: Agenda Ciudadana
date: 2021-03-17T07:49:43-03:00
thumbnail: https://assets.3dnoticias.com.ar/2021-03-16-Barletta.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Mario Barletta presentó un Recurso de Amparo contra el Gobierno Provincial
title: Mario Barletta presentó un Recurso de Amparo contra el Gobierno Provincial
entradilla: '"Siento la obligación de recurrir a la justicia para que las escuelas
  no se cierren y obligue al gobierno al retorno de la presencialidad 100%".'

---
En la mañana de hoy, en los Tribunales de la ciudad de Santa Fe, en compañía de la Diputada Nacional Ximena García y el Diputado Provincial Alejandro Boscarol el ex intendente de la ciudad de Santa Fe y ex embajador argentino en Uruguay Mario Barletta presentó un recurso de amparo contra el Gobierno de la Provincia de Santa Fe. 

"Siento la obligación de recurrir a la justicia para que las escuelas no se cierren y obligue al gobierno al retorno de la presencialidad 100%. El modelo de semipresencialidad segmenta la socialización de los alumnos y alumnas, además de las diferencias horarias que impiden en muchos casos la organización del grupo familiar. Los perjuicios familiares son enormes, tanto en la organización del hogar y del trabajo de cada uno de los padres, como de las rutinas de los chicos" sostuvo el ex mandatario local. "Son muchos los padres que se han organizado durante 2020 y que continúan su lucha para exigir la presencialidad 100%. Hoy pongo a disposición de ellos este Recurso de Amparo para que lo presenten en cada uno de los lugares de la provincia."

El expediente 21-02024250-4 - BARLETTA MARIO DOMINGO C/ PROVINCIA DE SANTA FE S/ RECURSO DE AMPARO fue recepcionado en el Juzgado de 1ra. Instancia en lo Civil y Comercial 1ra. Nominación de SANTA FE a cargo de la Jueza Viviana Marin, quien dio lugar al pedido por lo que el gobierno provincial tiene 5 días para responder.

¿Volverán las clases 100% presenciales? Habrá que esperar para ver como se resuelve la situación.

[Ver Recurso de Amparo ](https://es.scribd.com/document/498978839/Recurso-de-Amparo-presencialidad-100)

![](https://assets.3dnoticias.com.ar/aceptacion-amparo.jpg)