---
category: Agenda Ciudadana
date: 2020-12-19T11:46:07Z
thumbnail: https://assets.3dnoticias.com.ar/1912-traferri.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: El Senado rechazó el desafuero a Traferri
title: El Senado rechazó el desafuero a Traferri
entradilla: 'Con apenas cuatro votos a favor del desafuero, fue rechazado el pedido
  de los fiscales del Ministerio Público de la Acusación contra el jefe del bloque
  Juan Domingo Perón, Armando Traferri. '

---
Hubo dos abstenciones de los radicales Lisandro Enrico y Rodrigo Borla. Todos los demás senadores se expresaron en contra de la solicitud del MPA.

La celeridad prometida en el Senado para resolver el pedido de desafuero sobre el senador Armando Traferri (PJ-JDP-San Lorenzo) se cumplió en solo tres jornadas (el miércoles, jueves y este viernes) en medio de uno de los diciembres más calientes de la política santafesina.

Al cierre de esta edición, se desarrollaba la sesión en la que naufragó el pedido de los fiscales Matías Edery y Luis Schiappa Pietra cuyas explicaciones ayer a las comisiones de Asuntos Contitucionales y de Peticiones y Juicio Político no fueron convincentes para la mayoría.

Casi no se demoró el reinicio de la sesión -previsto para las 10 horas-, luego del cuarto intermedio que leyó el temario y ganó 24 horas más para seguir conversando sobre la Ley Tributaria pedida por el gobierno provincial, que está cerca de la unanimidad.

Las divisiones –en especial puertas adentro del peronismo en el Senado- mostraron que solo cuatro senadores justicialistas de Lealtad, encabezados por Alcides Calvo (más Marcelo Lewandowski, Ricardo Kaufmann y Marcos Castelló) votaron a favor de autorizar la imputación; que en cambio todo el bloque Juan Domingo Perón, de seis integrantes, la rechazó; y que lo mismo hicieron los otros dos senadores peronistas que no forman filas en ninguna de esas dos bancadas del PJ: Cristina Berra (San Martín) y Eduardo Rosconi (Caseros) votaron contra el pedido judicial.

Dos integrantes del radicalismo prefirieron no votar. Se abstuvieron Lisandro Enrico (presidente de la Comisión de Juicio Político) y Rodrigo Borla. Ambos son abogados.

Con esos números, los dos tercios de los presentes que exige la Constitución para un desafuero quedaron muy lejos de los 13 votos necesarios.

# **Abstención y defensa en primera persona**

El senador Armando Traferri, que obtuvo un  fuerte respaldo de la Cámara alta, habló en forma extensa de su carrera política y su trayectoria a partir de la década del ’80.

El jefe del bloque Juan Domingo Perón lo hizo luego de pedir la abstención para no votar una cuestión que lo involucraba en primera persona: el pedido de desafuero en su contra, pedido por dos fiscales de Rosario, que apenas lograron cuatro apoyos sobre diecinueve bancas.

En su discurso, subrayó que «para mantenerse en política hay que tener conducta, hay que tener palabra y hay que ser consecuente con lo que se dice».

«No es mi fuerte la oratoria. Sí son muy fuertes mis principios y mis verdades, ojalá los medios reflejen con exactitud lo que voy a exponer a mis pares, los primeros a los que me debo. Y a mi familia, por supuesto, que viene pasando horas muy difíciles», siguió.

Repasó sus comienzos y llegó al contexto actual en el que «todos los senadores teníamos asegurada la victoria», antes de la elección de gobernador en la que se impuso Omar Perotti.

«Los senadores justicialistas nos hicimos cargo de todo y nos cargamos la fórmula al hombro. Nos hicimos cargo de la campaña y, en mi caso particular, San Lorenzo fue el mejor departamento que ganaron Perotti y Rodenas, gracias al compromiso que tomaron los intendentes y los presidentes comunales, muchísimos dirigentes y este senador. No es que me creo que gané yo… Lo hicimos entre todos», comentó en un claro mensaje a la Casa Gris.

Hablar de la trayectoria política de un dirigente es «hablar de quién es uno», resumió. Y advirtió: «es demasiado ingrato escuchar a dirigentes del justicialismo, que le hicieron la vida imposible a gobernadores peronistas, que hoy como funcionarios estén cuestionando la honestidad de este senador».

«Si al ministro de Seguridad o jefe de inteligencia, como yo le digo, lo sale a defender Roy López Molina… bueno, eso no es el peronismo», comparó tras repasar su militancia en el PJ.

«Se armó una operación política hace mucho tiempo contra este Senado… Recuerden, en 2015 y 2016 se nos denunciaba por el reparto de los subsidios, que están autorizados por la Constitución Provincial: había que desprestigiar a este Senado. Esta Cámara tiene muy clara la independencia de Poderes, escucha al que piensa distinto y no impone nada, ni se deja imponer nada».

De inmediato dijo que los fiscales que entendían en aquella causa (luego fueron apartados) dijeron que «si metemos preso a un senador estamos como para presentarnos a gobernador». Más adelante aseguró: «los fiscales no quieren que los controlen».

«Voy a exponer mis pruebas, yo no hago conjeturas como los fiscales», y recordó todas sus intervenciones en las que denunció públicamente, desde su banca, hechos delictivos y jefes del narcotráfico en la ciudad de San Lorenzo. «Yo denuncié la convivencia policial con el narcotráfico y se lo llevé al entonces ministro Corti, hoy asesor del ministro de Seguridad y jefe de inteligencia Sain». 

Citó las actas judiciales en las que lo hizo, además de las versiones taquigráficas de sus pedidos de informes ante el jefe de los fiscales Jorge Baclini, durante años anteriores.