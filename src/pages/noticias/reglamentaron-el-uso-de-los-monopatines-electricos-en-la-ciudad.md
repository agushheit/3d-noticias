---
category: La Ciudad
date: 2020-12-23T11:36:48Z
thumbnail: https://assets.3dnoticias.com.ar/2412suarez1.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Reglamentan el uso de los monopatines eléctricos en la ciudad
title: Reglamentaron el uso de los monopatines eléctricos en la ciudad
entradilla: 'El Concejo Municipal aprobó un proyecto de ordenanza de Carlos Suárez
  y Luciana Ceresola, que determina las condiciones de uso y seguridad para este medio
  de movilidad. '

---
El edil de UCR-Juntos por el Cambio recordó que la iniciativa se elaboró con los integrantes del «Ciclo ciudad».

En la sesión de este miércoles, **el Concejo Municipal aprobó una iniciativa de Carlos Suárez y Luciana Ceresola que** **regula el uso de los monopatines eléctricos**. El proyecto de ordenanza fue elaborado por los concejales de Juntos por el Cambio, con el aporte de los integrantes de «Ciclo Ciudad», una agrupación local que fomenta la movilidad sustentable y el uso del espacio público.

«Tanto en el encuadre de la definición del tipo de vehículo, como en las condiciones de uso y seguridad,  trabajamos junto al grupo Ciclo Ciudad, que trabaja y tiene experiencia en la movilidad sostenible y el espacio público. Ellos nos sugirieron muchas de las condiciones que incorpora este proyecto, como así también, la posibilidad de que se permita a los usuarios poder subir a colectivos con los dispositivos y espacios aptos para el estacionamiento en la vía pública», explicó Suárez.

La norma aprobada incorpora a los monopatines eléctricos en la categoría de Vehículos Terrestres del Reglamento General de Tránsito de la ciudad. Además, define este tipo de vehículo como: «Dispositivo de movilidad personal de una y hasta tres ruedas, dotados de una única plaza y propulsados exclusivamente por motores eléctricos, equipados por un asiento si están dotados por sistema de autobalanceo. Se excluyen de esta definición los vehículos sin sistema de autobalanceo y con asiento, los concebidos para competición y aquellos para personas con movilidad reducida».

Asimismo, la normativa establece requisitos para la seguridad de los vehículos, como un sistema de frenos que actúe sobre sus ruedas, base de apoyo para pies, uso de casco protector, timbre o bocina que permitan llamar la atención, elementos reflectantes adheridos al vehículo, base para apoyo de los pies, una potencia máxima de motor de 500 watts y una velocidad máxima de 25 km. por hora.

Además, estipula la creación de un registro de usuarios de vehículos de movilidad personal y establece que cumplida su vida útil, estos vehículos sean considerados como residuos electrónicos. «Cada vez vemos más santafesinos que utilizan este tipo de vehículos sustentables para la movilidad personal, por eso consideramos fundamental avanzar en una regulación que legitime su uso y establezca las condiciones de seguridad necesarias», indicó Carlos Suárez.

<br/>

<span style="font-family: helvetica; font-size: 1.35em; font-weight: 800;">**Movilidad sustentable**</span>

En la normativa sancionada, se incorpora a los ciclomotores eléctricos y se especifica que **la edad permitida para circular en monopatines eléctricos es 16 años** o más.

«Vemos una amplia tendencia hacia la movilidad basada en medios eléctricos o alternativos a los medios tradicionales. Estos vehículos de movilidad personal permiten cubrir de forma más rápida que una bicicleta distancias cortas desde nuestros hogares o trabajos, con una gran facilidad de estacionamiento, lo cual aporta un atractivo más. Pero **lo más importante es que se trata de un medio sustentable de transporte**, que no genera emisión de gases, lo que tiene un efecto altamente positivo para nuestro ambiente», afirmó el concejal de la UCR-Juntos por el Cambio.