---
category: La Ciudad
date: 2021-01-19T09:00:31Z
thumbnail: https://assets.3dnoticias.com.ar/palomar.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad
resumen: 'Palomar: funcionarios municipales se reunieron con proteccionistas de animales'
title: 'Palomar: funcionarios municipales se reunieron con proteccionistas de animales'
entradilla: Esta mañana se concretó el cuarto encuentro entre ambas partes. Se comunicaron
  las tareas de saneamiento en los nidos y bebederos, además de obras estructurales
  para mejorar el tradicional espacio verde.

---
El secretario de Ambiente municipal, Edgardo Seguro, junto a funcionarios del área, se reunieron esta mañana con Gabriela Salzmann, presidenta de la Asociación Contra la Crueldad y el Maltrato Animal (ACCMA) y con Luisa Gómez, proteccionista independiente. El encuentro se realizó en la Estación Belgrano y allí se anunció que en los próximos 15 días se iniciarán tareas de saneamiento.

Al respecto, Seguro confirmó que la Municipalidad de Santa Fe está trabajando en una recuperación progresiva del Palomar y remarcó que nunca estuvo en los planes oficiales cerrar o trasladar este histórico lugar. Durante la reunión con los proteccionistas el funcionario municipal explicó que, además, habrá una campaña informativa en relación a las palomas y sus cuidados.

“Desde el municipio analizamos la situación y vamos a trabajar a partir de informes técnicos y de investigadores del Conicet, que tienen que ver con la situación animal, medioambiental y edilicia”, indicó Seguro. Por eso motivos, el tema no solo involucra a la Secretaría de Ambiente sino también a Obras y Espacios Públicos y Desarrollo Urbano, entre otros organismos municipales.

El edificio del Palomar hace más de 25 años que no tiene un mantenimiento adecuado en su infraestructura y los problemas detectados no son menores, porque incluye pisos, revestimientos, techo, caños de agua y desagües. Readecuar ese espacio público, a su vez, involucra una planificación de todo el entorno de la plaza y su posterior mantenimiento.

En esta primera etapa se iniciará el saneamiento integral del lugar, pero ya se están definiendo plazos y montos de los demás trabajos. Se debe retirar la reja que tiene el palomar y hacer un piso totalmente nuevo, porque actualmente ese pavimento está socavado y requiere un tratamiento especial para no afectar la estructura. La labor no es sencilla y demandará el máximo cuidado para no dañar a las palomas.

“El piso está tan deteriorado que se debe levantar por completo y hacerlo a nuevo. A eso debemos sumar una limpieza profunda de los nidos para cuidar la población de aves”, explicó Seguro, y finalizó: “El vandalismo es otro factor determinante porque se han roto desagües y eso generó inundaciones en toda la plaza, por lo tanto, vamos a intensificar el control”.

![](https://assets.3dnoticias.com.ar/palomar1.jpg)