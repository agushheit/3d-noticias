---
category: Agenda Ciudadana
date: 2021-10-09T06:15:47-03:00
thumbnail: https://assets.3dnoticias.com.ar/turismo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Importante movimiento turístico supera los niveles de prepandemia
title: Importante movimiento turístico supera los niveles de prepandemia
entradilla: Lo indició el ministro Lammes. Hay gran afluencia de visitantes y una
  importante ocupación de plazas hoteleras en los principales destinos turísticos
  del país.

---
  
  
En este contexto, el intendente del Partido de la Costa, Cristian Cardozo, resaltó las oportunidades que brinda para el sector turístico el fin de semana largo y dijo que implica "una especie de nuevo comienzo" que "llena de esperanza tanto a los prestadores de servicios como a quienes disfrutan de la costa".

Aseguró que "las restricciones pasadas en los últimos 18 meses, producto de la pandemia, hacen que esta temporada sea distinta porque el ritmo de vacunación en toda la provincia de Buenos Aires es muy bueno".  
  
Cardozo indicó que, para el sector gastronómico y hotelero, ello "es una gran tranquilidad" y apuntó que se espera que "este fin de semana podamos superar los picos de ocupación hotelera, especialmente durante los días sábado y domingo".  
  
En la provincia de Córdoba, en tanto, se registró un gran movimiento turístico en los principales valles de serranos, con promedios de ocupación del 95% de las plazas disponibles, según los datos de la Agencia Córdoba Turismo.  
  
Las terminales de ómnibus de la capital provincial y de Villa Carlos Paz mostraron actividad desde las primeras horas del día.  
  
También los peajes de las rutas hacia las serranías de los valles de Punilla, Calamuchita, Traslasierra y las sierras chicas registraron intenso tránsito vehicular, informaron desde la Policía Caminera provincial.