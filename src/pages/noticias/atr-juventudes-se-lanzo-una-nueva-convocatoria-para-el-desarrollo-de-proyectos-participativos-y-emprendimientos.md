---
category: Estado Real
date: 2021-08-18T18:00:16-03:00
thumbnail: https://assets.3dnoticias.com.ar/ATR.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'ATR juventudes: se lanzó una nueva convocatoria para el desarrollo de proyectos
  participativos y emprendimientos'
title: 'ATR juventudes: se lanzó una nueva convocatoria para el desarrollo de proyectos
  participativos y emprendimientos'
entradilla: "“Hay un deseo de abrir más puertas al protagonismo de los jóvenes”, dijo
  el gobernador Omar Perotti al encabezar el acto junto a la ministra de Igualdad,
  Género y Diversidad, Celia Arena. "

---
El gobernador Omar Perotti y la ministra de Igualdad, Género y Diversidad, Celia Arena, presentaron este martes en Rosario una nueva convocatoria destinada a jóvenes de todo el territorio provincial, en el marco del programa ATR Juventudes. La iniciativa incluye dos líneas: Proyectos Participativos y Emprendimientos “Impulsate”.

“La presencia de ustedes y de una segunda edición entusiasma y motiva, son motivos más que suficientes para estar con mucho ánimo para esta edición que seguramente tendrá la experiencia, los aportes y el aprender de los errores; ir abriendo caminos para que cada vez más lo sepan", dijo Perotti, y agregó: “Hay que comunicar que hay posibilidades, y un deseo de abrir puertas al protagonismo de los jóvenes”.

El gobernador recordó a los presentes que tuvo posibilidad de ser intendente a los 31 años, y eso lo llevó “a estar convencido de que no es una buena idea tener un gabinete joven: yo estoy convencido de que los jóvenes tienen que estar en el gabinete, que es muy distinto", dijo.

En otro tramo de su discurso, Perotti se refirió a la puesta en marcha del Boleto Educativo Gratuito (BEG), e insistió en la necesidad de poder contar con un programa de conectividad, para lo cual es necesaria la sanción del proyecto en la Legislatura provincial: “Necesitamos que esta provincia se equilibre poblacionalmente, que los jóvenes que decidan quedarse tengan oportunidades; ya deberíamos tener kilómetros de fibra óptica abriendo caminos”, lamentó.

“Lo ideal es no solo llegar a las escuelas, sino a las plazas, a cada barrio. Tenemos que estar atentos a esas cosas para que no se vuelvan a interrumpir, porque en el camino nos queda mucha gente sin posibilidades”, argumentó el gobernador de la provincia.

“El deseo es que tengamos muchos aportes, para poder seguir poniendo de pie a la provincia con más fortaleza y así generar más oportunidades”, indicó el mandatario provincial, e invitó: “Tengan la plena y absoluta libertad para crear y desafiar desde cada uno de los proyectos. Los necesitamos con su mirada fresca. Lo fundamental es que muestren voluntad de aportar a una provincia que los tiene como protagonistas, con aportes y contribuciones”, concluyó Perotti.

**Proyectos diversos**

En tanto, la ministra Celia Arena dio detalles de la propuesta y señaló “que la cabeza piensa donde los pies pisan”, en relación a la “impronta” del gobernador Perotti de entender que “no hay imposibles. Aquí hay proyectos de los más diversos, siempre con perspectiva de género, porque para nosotros no es una moda sino una forma de trabajar y gestionar”, señaló.

“Estamos siempre con el oído atento y la decisión de trabajar en territorio, construyendo diálogo con los gobiernos locales y organizaciones, codo a codo, en esa alianza tan importante”, manifestó.

Por su parte, Milagros Monserrat, responsable de ATR Juventudes, destacó la importancia de la iniciativa “que piensa y construye oportunidades laborales. “Pensamos la política a largo plazo y emprendimientos sostenibles en el tiempo”, afirmó, al tiempo que convocó a los y las jóvenes a “cambiar y transformar la realidad”.

El acto de lanzamiento tuvo lugar en el Acuario del Río Paraná, de la ciudad de Rosario. Del mismo participaron también la directora provincial del programa “Santa Fe Más”, Julia Irigoitía, además de coordinadores y coordinadoras de ATR Juventudes de toda la provincia.

El evento contó con la actuación Frank TFF y Laimxx, del “Proyecto Fifty-fifty”, impulsado por dos jóvenes que participaron de la primera convocatoria de ATR con una competencia de freestyle con paridad de género.

**¿Cómo participar?**

Los interesados e interesadas en concursar dentro de Proyectos Participativos deben tener entre 15 y 29 años, las temáticas incluidas son: Género y Diversidad, Ambiente, Nuevas Tecnologías, Salud, Deportes, Cultura y Redes Solidarias.

En tanto, para participar dentro de la línea de Emprendimientos “Impulsate”, es necesario tener entre 18 y 29 años y pueden presentarse propuestas de emprendimientos de: Diseño sustentable, Innovación tecnológica, Carpintería, Accesorios y talabartería, Alimentación saludable, Textiles y calzado, Programación y Produccción agraria.

Los proyectos deben presentarse a través del Formulario Online disponible desde el miércoles 18 de agosto en la página web del Gobierno de la Provincia de Santa Fe (www.santafe.gob.ar), donde también están las bases y condiciones. Los planes seleccionados serán financiados con un monto máximo de $70.000.

**Jóvenes y políticas transversales**

A través de ATR Juventudes Santa Fe, el Ministerio de lgualdad, Género y Diversidad se encarga de generar políticas públicas transversales orientadas a los y las jóvenes de la provincia de Santa Fe, haciendo foco en la articulación interministerial, con la importancia de priorizar que las juventudes encuentren, con el acompañamiento del Estado, las herramientas para desarrollar sus proyectos de vida, expresar sus ideas y construir sus sueños.

En este sentido, las políticas públicas están basadas en cuatro líneas fundamentales: Autonomía, Transformación, Desarrollo y Territorialidad, buscando fortalecer la participación de los y las jóvenes en todo el proceso. Desde un enfoque transversal e integral, se trabaja en pos de darle perspectiva joven a las políticas que piensen las distintas áreas del gobierno.