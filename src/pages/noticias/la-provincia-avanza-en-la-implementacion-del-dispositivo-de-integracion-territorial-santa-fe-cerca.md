---
category: Estado Real
date: 2021-02-13T06:00:36-03:00
thumbnail: https://assets.3dnoticias.com.ar/santafecerca.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: La Provincia Avanza En La Implementación Del Dispositivo De Integración Territorial
  “Santa Fe + Cerca”
title: La Provincia Avanza En La Implementación Del Dispositivo De Integración Territorial
  “Santa Fe + Cerca”
entradilla: La Secretaría de Justicia dependiente del Ministerio de Gobierno, Justicia,
  Derechos Humanos y Diversidad, asesora a los ciudadanos y ciudadanas sobre trámites,
  derechos y asistencia.

---
El gobierno provincial, a través de La Secretaría de Justicia dependiente del Ministerio de Gobierno, Justicia, Derechos Humanos y Diversidad continúa con la implementación del Dispositivo de Integración Territorial “Santa Fe + Cerca”, de manera conjunta con otras reparticiones provinciales.

El dispositivo se encuentra funcionando en barrios de la ciudad de Santa Fe, y el mismo consiste en asesorar a los ciudadanos y ciudadanas sobre distintos trámites, derechos y asistencia, que brindan diversas áreas del Gobierno de la Provincia de Santa Fe. Para su implementación, participan de manera integral la Subsecretaría de Registros, Registro Civil, Registro de la Propiedad, RUAGA, IGPJ, la Subsecretaría de Acceso a la Justicia, la Dirección de Asistencia Judicial y, La Secretaría de Estado de Igualdad y Género de la Provincia.

“La iniciativa de la Secretaría de Justicia pretende llegar a la ciudadanía. Para ello, el gobierno de la provincia acerca sus servicios a los barrios, al territorio. Estamos muy contentos que ahora incorporamos a la Secretaría de Estado de Igualdad y Género para que Santa Fe + Cerca, sea cada vez más grande, más abarcativo y brinde cada vez más servicios y reconocimiento de derechos a los ciudadanos y ciudadanas” manifestó Francisco Dallo, subsecretario de Registros sobre Personas Humanas y Jurídicas.

De esta intervención también participa la Secretaría de Estado de Igualdad y Género, realizando distintas acciones. “Desde Igualdad y Género nos sumamos a esta iniciativa de Santa Fe + Cerca con actividades de promoción de derechos, con entrega de folletos y también con un taller que propone pensar los vínculos. También, la prevención de situaciones de violencia de género. Hay una decisión del gobierno provincial de bajar el Estado al territorio, y acercarlo a la gente, hacerlo accesible a ciudadanos y ciudadanas”, agregó Nerea Tacarí, directora provincial de Políticas de Igualdad de la mencionada cartera.

**Acciones De Santa Fe Más Cerca**

El dispositivo brinda una serie de actividades que incluye los Operativos Documentarios, que consisten en el asesoramiento sobre el programa “Protejamos Nuestra Vivienda”, con el objetivo de declarar inembargable la vivienda única a través de un trámite gratuito.

En este sentido, además se realizó el lanzamiento del Programa Documentario llamado “Viernes Joven”, que consiste en dar prioridad a los mayores de 14 años que obligatoriamente deben realizar la renovación de su DNI.

"Este trámite tiene un costo de $ 300, sin embargo, por gestiones realizadas desde el dispositivo, una trabajadora social del Centro de Asistencia Judicial extenderá certificados de escasos recursos, para justificar la gratuidad, para quienes no puedan solventar el costo de la renovación", informó Dallo.

A su vez, la Subsecretaría de Acceso a la Justicia y la Dirección de Asistencia Judicial, continuarán con el asesoramiento, apoyo y contención a víctimas y testigos de delitos, víctimas de violencia familiar y violencia de género.

Por su parte, el Registro Único de Aspirantes a Guarda con fines Adoptivos continúa brindando información respecto de los requisitos para futuros adoptantes, como así también la modalidad de inscripción, y los talleres informativos previos y obligatorios para nuevos aspirantes.

En tanto, la Subsecretaría de Inspección General de Personas Jurídicas continúa con el asesoramiento para trámites sobre asociaciones civiles, y nuevos mecanismos de acceso a información sobre las mismas, a través de la creación del usuario de entidad, para que cada entidad pueda tener acceso a su legajo y a sus trámites en forma virtual.

En este marco, cabe mencionar que dicho sistema integral comenzó a trabajar el pasado viernes en la sede de la Asociación Vecinal Cultural, Social y Deportiva República Del Oeste. En el transcurso de esta semana, el Dispositivo Territorial empezó a funcionar en la Vecinal Yapeyú Oeste.

El recorrido continuará en el sur de la Provincia, en el departamento de General López: los días 18 y 19 de febrero se realizarán operativos en Firmat (lugar a confirmar). Posteriormente, el dispositivo volverá a la ciudad de Santa Fe, los días 22, 24 y 26 del corriente mes, en la Vecinal Sargento Cabral (Avenida Gral. Paz 5371).

Por otra parte, se confirmarán los días, lugares y horarios para la ciudad de Rosario, y en el interior de la Provincia, ya que se tienen previstos operativos en distintas ciudades en las que se están coordinando las acciones del dispositivo.