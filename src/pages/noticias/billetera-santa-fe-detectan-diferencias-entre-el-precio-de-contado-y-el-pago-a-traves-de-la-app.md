---
category: Agenda Ciudadana
date: 2021-07-19T06:15:32-03:00
thumbnail: https://assets.3dnoticias.com.ar/BILETERA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Billetera Santa Fe: detectan diferencias entre el precio de contado y el
  pago a través de la app'
title: 'Billetera Santa Fe: detectan diferencias entre el precio de contado y el pago
  a través de la app'
entradilla: El gobierno de la provincia llevó adelante operativos de fiscalización
  a comercios adheridos. Advierten que quienes reincidan podrían recibir sanciones
  y ser pasibles de exclusiones del programa.

---
El gobierno de la provincia llevó adelante operativos de fiscalización a comercios adheridos a la Billetera Santa Fe. Los principales rubros fiscalizados fueron locales de venta de alimentos, de indumentaria, farmacias y perfumerías, en donde efectivamente se encontraron diferencias entre el precio de contado y el precio a abonar con la app

Las fiscalizaciones se realizaron en comercios de las localidades de Reconquista, Santa Fe, Rafaela, Humberto Primo, Laguna Paiva, Lehmann, Gálvez, Rosario, Cañada de Gómez y Firmat. En esas localidades, se visitaron comercios que recibieron denuncias por presuntas prácticas abusivas.

Los controles se realizaron a través de la Dirección Provincial de la Promoción de la Competencia y Defensa del Consumidor del Ministerio de Producción, Ciencia y Tecnología.

La finalidad de dichos operativos es realizar un seguimiento del uso de la herramienta y obtener aportes del sector comercial en general. También, alertar sobre su correcto uso poniendo el énfasis en la prohibición de efectuar recargos en los precios por el pago con Billetera Santa Fe.

Al respecto, la directora de Promoción de la Competencia y Defensa del Consumidor, María Betania Albrecht, destacó: “El precio de los bienes es único. No puede haber diferencias para el pago de contacto efectivo, con tarjeta de débito, con Billetera Santa Fe o con tarjeta de crédito en una sola cuota”.

“Ésta es una herramienta que ha venido a acompañar al sector comercial, a promover el consumo y fundamentalmente, a cuidar el bolsillo de las y los consumidores. Debemos cuidar sus intereses económicos, garantizados por la Constitución Nacional. Para ello, debemos proteger y hacer valer esta herramienta Billetera Santa Fe como corresponde, a través de este programa las y los consumidores están eligiendo comprar en sus comercios de cercanía, lo cual ayuda enormemente al ecosistema local y al desarrollo territorial para que no tengan que movilizarse hacia otros lugares. Esto aporta al circuito económico local y permite que el dinero quede allí, en la región, y se reinvierta”, agregó la directora.

Asimismo, Albrecht señaló: “Puntualmente en rubro medicamentos, es importante resaltar que el beneficio de la Billetera Santa Fe no reemplaza al descuento que corresponde por obra social. Si una farmacia cuenta con Billetera Santa Fe, debe utilizar dicho medio de pago para todas las compras, y sin ningún tipo de recargo”.

Los inspectores indicaron a los comercios la manera correcta de exhibir los precios y la modalidad de operar con los diferentes medios de pago, teniendo como referencias las normativas que competen en la materia. Además, explicaron que, ante reincidencias en dichas conductas, podrían recibir sanciones y ser pasibles de exclusiones del Programa Billetera Santa Fe.

En cuanto a las fiscalizaciones Albrecht aclaró: "En esta oportunidad hemos detectado 13 comercios que aplican recargo. Lo que sucede es que muchos de estos incrementos se detectan una vez realizada la compra, por lo que la infracción se detecta insitus. Cabe aclarar que, por tal motivo, estamos incentivando los controles a fin de defender los intereses de las y los usuarios”.

“Ante los reclamos recibidos por los usuarios del programa Billetera Santa Fe estamos realizando las inspecciones necesarias para garantizar el buen funcionamiento de la herramienta, evitando prácticas abusivas. Además, recordó que “el programa cuenta con más de 600.000 usuarias y usuarios y que ya es un hecho, como indicó el Gobernador Omar Perotti, que Billetera Santa Fe seguirá durante toda la gestión. Por ello debemos garantizar que se cumplan las condiciones del mismo”, concluyó la funcionaria

**Dónde denunciar**

Ante reclamos por inconvenientes técnicos en el uso de la App, las y los consumidores deben dirigirse a info@pluspagos.com, y ante potenciales conductas abusivas pueden reclamar en:

La Dirección Provincial de Promoción de la Competencia y Defensa del Consumidor, al 0800-555-6768 (opción 3) o ingresando al siguiente sitio del gobierno [https://www.santafe.gob.ar/index.php/tramites/modul1/index?m=descripcion&id=127547](https://www.santafe.gob.ar/index.php/tramites/modul1/index?m=descripcion&id=127547 "https://www.santafe.gob.ar/index.php/tramites/modul1/index?m=descripcion&id=127547")

Las Oficinas Municipales de Información al Consumidor (OMIC) [https://www.santafe.gov.ar/index.php/web/content/download/259414/1365404/file/OMICS%2016SEP2020.pdf](https://www.santafe.gob.ar/index.php/tramites/modul1/index?m=descripcion&id=127547 "https://www.santafe.gob.ar/index.php/tramites/modul1/index?m=descripcion&id=127547")