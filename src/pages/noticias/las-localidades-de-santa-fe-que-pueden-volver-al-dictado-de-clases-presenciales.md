---
category: Agenda Ciudadana
date: 2021-06-13T06:15:56-03:00
thumbnail: https://assets.3dnoticias.com.ar/CLASES.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Algunas localidades de Santa Fe ya pueden volver al dictado de clases presenciales
title: Las localidades de Santa Fe que pueden volver al dictado de clases presenciales
entradilla: Será desde este lunes 14 para el nivel inicial y primario. En las ciudades
  con altos niveles de contagio continúa la modalidad a distancia.

---
Este viernes, en el marco de las restricciones implementadas por el gobierno de Santa Fe para atenuar el contagio de coronavirus en la provincia, el Ministerio de Educación emitió la circular n° 15 en la que habilita al dictado de clases presenciales en el nivel inicial y primario de localidades que hayan presentado menos de 10 contagios en los últimos días.

La medida, al igual que las restricciones que rigen en toda la provincia, se extiende entre el 14 de junio y el 25 de este mes.

En contraposición, los distritos que evidencien altos niveles de contagio continuarán bajo la modalidad de educación virtual o a distancia. En ese sentido, la comunicación oficial aclara: “Los establecimientos educativos permanecerán abiertos con asistencia del personal escolar, docentes y asistentes escolares, sin el dictado de clases presenciales, pero trabajando el vínculo pedagógico en la distancia entre educadores y estudiantes”.

Además, la circular detalla: “Para el trabajo en la distancia cada institución organizará los equipos docentes para desarrollar: clases virtuales cuando sea posible, comunicaciones con telefonía celular, distribución de materiales de lectura, diversos recursos impresos y guías de actividades para orientar las experiencias de aprendizaje de los alumnos, conforme el conocimiento de cada contexto, cada grupo en su nivel y modalidad, sosteniendo comunicación y relación en el proceso educativo. En razón de disponer de soportes digitales adecuados y mejor conectividad se podrá autorizar a los docentes a desarrollar actividades desde su domicilio particular en formato teletrabajo”.