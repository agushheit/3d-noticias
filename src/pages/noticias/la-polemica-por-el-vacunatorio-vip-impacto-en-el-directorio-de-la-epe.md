---
category: Agenda Ciudadana
date: 2021-06-10T07:58:48-03:00
thumbnail: https://assets.3dnoticias.com.ar/lotes-las-primeras-dosis-sputnik-v-llegaron-al-policlinico-diciembre.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Diario La Capital
resumen: La polémica por el vacunatorio VIP impactó en el directorio de la EPE
title: La polémica por el vacunatorio VIP impactó en el directorio de la EPE
entradilla: Una denuncia alertó sobre presuntas irregularidades en el Hospital Eva
  Perón. Salud evalúa si interviene el centro asistencial. Alberto Joaquín hizo su
  descargo

---
El jefe interino del Departamento Enfermería del Hospital Eva Perón de Granadero Baigorria, Ariel Pérez, denunció que entre enero, febrero y marzo funcionó un “vacunatorio VIP” en ese centro de salud adonde acudieron el vicepresidente de la Empresa Provincial de la Energía (EPE), Alberto Joaquín, (quien quedó muy comprometido en su continuidad); el intendente de Granadero Baigorria, Adrián Maglia, y el delegado de San Lorenzo de UPCN, José Luis Babaya; cuando en aquel momento las dosis estaban reservadas para el personal esencial del efector. El enfermero apuntó contra el director del hospital, Jorge Kilstein, como el principal responsable y consideró que se trató de una “irregularidad grave”.

Ayer se inició una investigación administrativa en el Ministerio de Salud santafesino que puede derivar en una intervención del hospital.

En diálogo con La Capital, Pérez confirmó que su denuncia por escrito la efectuó ante la Delegación local del Ministerio de Salud el 21 de mayo.

El enfermero habló de “adelantamientos” durante los meses de enero, febrero y marzo por parte de sindicalistas, dirigentes, funcionarios y hasta familiares directos del propio director del Eva Perón.

“Si alguien cometió un error debe hacerse cargo. No es tan difícil. Esto es responsabilidad directa del director y del consejo de administración”, dijo para recordar que se desempeña hace 30 años en el efector “sin ningún sumario, ni una falta”.

La Capital accedió a documentación de la denuncia en la que Joaquín (78 años) figura como vacunado con la primera dosis de Sputnik el 2 de febrero pasado, y con la segunda el 11 de marzo.

Mientras tanto, Alberto Joaquín hizo su descargo a título personal, en el que destaca sus problemas de salud. "A los 78 años asumí la vicepresidencia de la EPE, ad honorem. Pese a todas las dificultades que la pandemia generaba fui cumpliendo con las múltiples obligaciones que el cargo exigía. A mediados de Enero de este año tuve una recaída en mi condición de hipertenso. Como esto potenciaba los riesgos que ya estaba corriendo, y perteneciendo la empresa a un sector esencial, solicité que se me vacunara para poder continuar cumpliendo con las exigencias que el cargo demanda", indicó.

Y continuó: "Esto se realizó en el Hospital Eva Perón de Granadero Baigorria los días 2 de Febrero y 11 de Marzo. Facilité mi documento y pedí que mi acto fuera agendado como corresponde y se me entregara el carnet correspondiente".

"Muy lejos está de mi persona la supuesta participación en un «vacunatorio VIP» que muy livianamente me han adjudicado", concluyó.

En tanto, Maglia (69 años) está mencionado en planilla con la primera dosis el 27 de enero y con el segundo componente de Sputnik V, el 24 de febrero.

Babaya (62 años) aparece en la documentación como vacunado el 27 de enero con primera dosis de Sputnik V y el 17 de febrero con la segunda dosis.

Ayer por la mañana en declaraciones a La Ocho Pérez había indicado que las primeras dosis de las vacunas que llegaron al Hospital Eva Perón “nunca estuvieron destinadas a personal por fuera del nosocomio”

Para darle crédito a su denuncia afirmó que “está todo documentado. Los últimos nombres todavía están en mi poder”, agregó Pérez.

Al respecto, el director del Tercer Nivel de Salud, Rodrigo Mediavilla, reveló que hay un sumario en curso. Y de igual manera se pronunció el secretario de Salud, Jorge Prieto.

“Si las vacunas fueron aplicadas, hay un registro, así que se buscará la trazabilidad y las pruebas necesarias como se ha hecho en otros efectores. En Reconquista, donde una vez que se hicieron las actuaciones sumariales el fiscal de Estado determinó la intervención. Las consecuencias penales son paralelas a lo administrativo”, explicó Prieto, quien no descartó una intervención.

“Los funcionarios de la Subsecretaría Legal y Técnica están investigando. Puedo asegurar que hay un expediente administrativo en curso para establecer fehacientemente qué sucedió”, dijo Mediavilla para agregar: “Todos los vacunados figuran en el registro del Sicap. Todas las enfermeras desde Reconquista hasta Venado Tuerto pueden acceder al registro. Ahí está todo. No hay nada para ocultar. Si hay que investigar, se investigará este adelantamiento y se tomarán las cartas en el asunto. Pero la Provincia es transparente”, aseguró.

**En la mira**

Las acusaciones de Pérez apuntaron contra el director del Hospital, Jorge Kilstein. “Armó un vacunatorio VIP dentro de la Dirección, porque el resto del personal se vacunó en el anfiteatro, respetando el distanciamiento y con todo bien armado como corresponde”, precisó el enfermero quien acotó: “Yo presenté seis nombres, pero tengo más en mi poder. Y el comentario de las vacunadoras indican que habría más gente. Si abrieran una investigación como corresponde, deberían ser el director y el Consejo de Administración quienes deberían responder cuántas personas se vacunaron por fuera”.

Ante este diario, Pérez consideró que “por más que la esposa del director del Eva Perón es médica (se menciona a Silvia Kilstein) nunca trabajó acá. Las vacunas eran para los empleados” y acusó al director a quien consideró debe hacerse responsable ante “una irregularidad grave”. La Capital intentó en vano obtener una respuesta del director.

Mediavilla remarcó que la ministra (de Salud Sonia Martorano) se ocupó personalmente, afirmó que hay que investigar y esperar que la Justicia se expida. “No hay que apresurarse. Si hay una irregularidad, hay que investigarla y aplicar las sanciones que correspondan”, sentenció.

**En la Legislatura**

El caso tuvo repercusión en la Legislatura provincial, donde Del Frade presentó un pedido de informe para que se esclarezca el caso. “Desde que comenzó la vacunación al personal de salud, se hicieron presentes personas ajenas al Hospital Eva Perón, y el director da la orden a las enfermeras que se dirijan a la oficina de la dirección del nosocomio para vacunar a personas recomendadas por él”, expresó en los argumentos el diputado.

**Dos casos que generaron revuelo y renuncias**

La denuncia hecha por el enfermero Ariel Pérez en el hospital Eva Perón de Granadero Baigorria tiene dos antecedentes en Santa Fe. El más gravoso fue la intervención decretada por el gobierno al hospital de Reconquista tras detectarse que los familiares del Consejo de Administración se habían vacunado en forma indebida, lo que provocó una investigación judicial. El otro fueron los 800 empleados del Ministerio de Cultura que al figurar en el padrón del Ministerio de Educación fueron citados a vacunarse. El error fue advertido, pero aún así, 85 de ellos recibieron las dosis.

En Reconquista, familiares de tres consejeros accedieron al vacunatorio VIP lo que desató el pedido de renuncia inmediata de su director, Fabían Nuzarello y la intervención del nosocomio. En el caso de Cultura, una alta fuente oficial explicó que no hubo privilegios sino error o negligencia. Al confeccionar los listados del Ministerio de Educación quedó un archivo remanente de cuando Cultura y esta cartera estaban fusionados.