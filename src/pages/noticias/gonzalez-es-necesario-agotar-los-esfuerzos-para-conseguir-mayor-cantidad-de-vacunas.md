---
category: La Ciudad
date: 2021-03-28T06:00:06-03:00
thumbnail: https://assets.3dnoticias.com.ar/GONZALEZ.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'González: "Es necesario agotar los esfuerzos para conseguir mayor cantidad
  de vacunas"'
title: 'González: "Es necesario agotar los esfuerzos para conseguir mayor cantidad
  de vacunas"'
entradilla: En el recinto de sesiones, concejales se manifestaron en torno a la necesidad
  de aumentar la cantidad de vacunas en la ciudad y establecer las gestiones necesarias
  para alcanzar la mayor población posible-

---
El presidente del Concejo, Leandro González se pronunció en el recinto de sesiones y manifestó su preocupación ante la escasez de vacunas contra el Covid-19 en el país previo a la segunda ola de contagios anunciada para los próximos meses.

En el recinto de sesiones, concejales se manifestaron en torno a la necesidad de aumentar la cantidad de vacunas en la ciudad y establecer las gestiones necesarias para alcanzar la mayor población posible y estar mejor preparados para la segunda ola de contagios que se aproxima.

Al respecto, el presidente del Concejo, Leandro González, explicó que “es necesario que desde el Gobierno Nacional se agoten los esfuerzos para, en la antesala de la segunda ola, poder conseguir la mayor cantidad de vacunas posibles, una gestión también importante desde el Gobierno provincial. Sin embargo, no debemos olvidar la responsabilidad que todos tenemos de seguir cuidándonos con las medidas sanitarias que ya conocemos y no alimentar pronósticos complejos, sino trabajar para que no haya nuevas dificultades ante las nuevas cepas circulantes en el país”.

Y continuó González: “Hay una realidad mundial crítica en la cual es necesario hacerse valer y agotar las gestiones: 18 países concentran el 80% de las vacunas y 15 recibieron más del 10% de lo que pidieron. Argentina recibió cerca del 7% (estadística previa a la llegada de las 306.000 dosis de Sputnik V). Santa Fe recibió un total de 243.541 vacunas, pero cuenta con 800 mil personas inscriptas para vacunarse contra el coronavirus. Más del 50% son adultos mayores de 60 años. Y del total del personal escolar santafesino se anotó el 83% y ya recibió la primera dosis”.

“Sin embargo -continuó- lo preocupante de la situación son los adultos mayores de 60 años que no podrán ser vacunados de acuerdo al cronograma por falta de dosis en nuestra jurisdicción provincial. Por eso es fundamental priorizar y gestionar el acceso a más vacunas para nuestra ciudad”.

Por último, González concluyó que “el Gobierno Nacional ha equivocado las estrategias para la obtención de vacunas, sabemos que es un problema internacional, pero esto no quita un error estratégico. Si hace un mes se planteaba a través del ex ministro Ginés González García que en agosto iban a estar todos los argentinos vacunados, evidentemente algo falló. Lo que planteamos, en este tiempo, es profundizar las acciones para obtener la mayor cantidad de vacunas y darle tranquilidad a la población de cara a la segunda ola de contagios”.