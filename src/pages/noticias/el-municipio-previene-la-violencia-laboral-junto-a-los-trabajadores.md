---
category: La Ciudad
date: 2021-12-21T06:00:18-03:00
thumbnail: https://assets.3dnoticias.com.ar/violencialaboral.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: El municipio previene la violencia laboral junto a los trabajadores
title: El municipio previene la violencia laboral junto a los trabajadores
entradilla: "La Dirección de Empleo y Trabajo Decente lleva adelante diversos talleres
  con el objetivo de  identificar y combatir conductas violentas en los ámbitos laborales.\n\n"

---
La violencia en el lugar de trabajo, sea física o psicológica, se ha convertido en un problema mundial que atraviesa fronteras, contextos de trabajo y grupos profesionales. Si bien siempre se ha reconocido la existencia de violencia física personal en lugares de trabajo, la violencia psicológica se ha subestimado durante largo tiempo y sólo ahora recibe la debida atención.

En este marco, desde la Dirección de Empleo y Trabajo Decente del municipio, se llevan adelante diversas actividades con el objetivo de identificar y combatir estas conductas, con el objetivo de erradicar la violencia en los ámbitos laborales.

De este modo, durante la semana se concretaron talleres en áreas de la Secretaría de Hacienda, sumando cerca de 10 actividades de este tipo, organizadas durante el segundo semestre del año, para un total de 100 agentes.

**Santa Fe, el primer municipio del país**

Con un decreto de diciembre de 2020, el intendente Emilio Jatón creó un Protocolo contra la violencia laboral en el municipio. Para la confección del mismo, se contó con el asesoramiento de la Organización Internacional del Trabajo y la participación del Sindicato de Trabajadores Municipales. Así, Santa Fe se constituyó en el primer municipio del país en establecer un plan de acción en línea con las disposiciones del Convenio Nº 190 de la OIT, al que el país adhirió recientemente.

El instrumento establece un procedimiento de actuación temprana y eficaz para prevenir, sancionar y erradicar conductas de violencia y acoso en el ámbito del trabajo o con conexión con el mismo y que tengan lugar en el seno de la Administración Pública Municipal. Del mismo modo, regula la actuación de las diferentes áreas municipales involucradas en su atención.

Entre sus puntos destacados, la iniciativa propone llevar a cabo políticas preventivas y campañas de formación, sensibilización y difusión destinadas tanto a empleados y empleadas como a funcionarios y funcionarias de la administración pública municipal. La intención es visibilizar, desnaturalizar y erradicar la violencia y el acoso en el trabajo.

Cabe destacar, que desde que se puso en marcha el Protocolo para denuncias de trabajadores municipales, 60 personas recibieron atención y asesoramiento en la Dirección de Empleo y Trabajo Decente. De los datos recabados se desprende que de los denunciantes, 83,3% son mujeres y 16,6% son varones.

Por otra parte, se establece que 91,6% de los casos corresponde a violencia de tipo psicológica o mobbing y 8,6% configuró también violencia laboral de género. En cuánto al perfil los/as acosadores/as, 66,6% pertenece al género masculino y 91,66% de los/as acosadores/as ocupan una posición jerárquica superior.

Finalmente, y desde el punto de vista de la salud, 66,6% de los/as denunciantes está bajo algún tipo de tratamiento (médico, psicológico, psiquiátrico, etc.) y vinculó esa situación al malestar generado por la situación denunciada.

**Legislación vigente**

En la administración pública de la provincia de Santa Fe existe una primera Ley de Violencia Laboral en el ámbito público que fue sancionada en el año 2005 (Ley 12.434). En tanto, en la ciudad capital, la Ordenanza N° 12.675 del año 2019 incorpora al Estatuto del Personal Municipal y prevé sanciones para funcionarias y funcionarios y/o empleadas y empleados de la administración municipal que ejerzan sobre otras u otros, conductas que encuadren en la definición de violencia laboral.

En la norma, se entiende a la violencia laboral como “toda forma de abuso de poder que tiene por finalidad someter o excluir a una trabajadora o un trabajador de su lugar de trabajo. La violencia laboral configura una violación a los derechos laborales y humanos. Puede presentarse como agresión física, acoso sexual y/o violencia o maltrato psicológico”, se asegura.

En 2019, la Conferencia General de la Organización Internacional del Trabajo adoptó el Convenio Nº 190, que se configura como un nuevo mandato global sobre las relaciones en el trabajo, reconociendo por primera vez el derecho al trabajo libre de violencia y acoso, asociándolo al trabajo decente planteado en los Objetivos de Desarrollo Sostenible. En Argentina, el Convenio 190 de la OIT fue ratificado mediante la Ley N° 27.580, el 15 de diciembre de 2020, convirtiéndose en el cuarto país a nivel mundial, en adherir a la norma.