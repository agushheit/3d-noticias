---
layout: Noticia con imagen
author: "Fuente: Diario UNO"
resumen: Estafas Inmobiliarias
category: La Ciudad
title: Un escribano, un contador y un odontólogo, los organizadores de varias
  estafas inmobiliarias
entradilla: "Fueron imputados de organizar una asociación ilícita con la que
  estafaron con un loteo en Recreo. También se imputó a un comerciante como
  miembro. "
date: 2020-11-16T13:35:04.160Z
thumbnail: https://assets.3dnoticias.com.ar/tribunales.jpeg
---
Cuatro personas fueron imputadas en el marco de una investigación que tramita en la Unidad Fiscal Especial de Delitos Complejos del MPA a una asociación ilícita dedicada a la realización de estafas relacionadas al loteo de un inmueble de 614 hectáreas ubicado en jurisdicción de la ciudad de Recreo.

La atribución delictiva fue realizada por las fiscales Mariela Jiménez, María Laura Urquiza y Bárbara Ilera en una audiencia ante el juez Gustavo Urdiales que se llevó a cabo este mediodía en los tribunales de la ciudad de Santa Fe.

A tres de los cuatro imputados se les atribuyó ser organizadores de la asociación ilícita. Se trata de un escribano y un contador oriundos de Santa Fe, de 47 y 48 años cuyas iniciales son EAL y OML, respectivamente. El tercer hombre imputado como organizador es un odontólogo de 59 años oriundo de Miramar (provincia de Buenos Aires). Mientras que la cuarta persona imputada es un comerciante de Recreo, de 51 años, cuyas iniciales son LOG.

Las fiscales Jiménez e Ilera les atribuyeron a los cuatro imputados la coautoría de los delitos de asociación ilícita –a tres en calidad de organizadores y a uno como miembro–; estafas reiteras y falsedad ideológica.



**Modalidad delictiva**

“Si bien los hechos ilícitos comenzaron a cometerse en julio del año 2000, la actividad de la asociación ilícita que investigamos se inició en septiembre de 2015”, precisaron las fiscales Jiménez e Ilera. “Las nueve personas que tenemos identificadas –cuatro de las cuales fueron imputadas hoy– formaban parte de esta organización de carácter permanente, estable y organizada que tenía como objetivo darle apariencia legal a la estructura jurídica del fideicomiso creado bajo el nombre de “Barrio Las Mercedes de Recreo”, agregaron.

> Las funcionarias del MPA aclararon que “a través de ese fideicomiso disponían de los lotes que conforman el inmueble titularidad de ‘Las Mercedes de Recreo SA’”.

“Ya en julio de 2000 comenzaron los hechos ilícitos”, advirtieron las fiscales. **“Mediante engaños y maniobras ardidosas ante la Inspección General de Personas Jurídicas y Registro Público de Comercio, obtuvieron la inscripción de la sociedad ‘Las Mercedes de Recreo SA’”**, señalaron y detallaron que “lo hicieron simulando la calidad de socios, mediante el uso de actas de contenido falso, perjudicando el patrimonio de la sociedad y de los accionistas, mediante la administración y disposición del inmueble que es el único capital social de la empresa”.



“Las maniobras fueron numerosas y de distinto tipo. Incluyen la simulación de una cesión de acciones, la venta de 100 lotes a terceros de buena fe y la autorización para dar en pago 600 lotes más, sin tener el derecho para hacerlo”, subrayaron las fiscales. También hicieron hincapié en que “simularon otros actos administrativos y procesales; adulteraron copias, insertaron datos falsos y confeccionaron actas con contenido falso siempre con el objetivo de simular el cumplimiento de los requisitos legales, procesales y fiscales exigidos por el Estado”.

De acuerdo a lo resuelto hoy, el próximo martes se realizará la audiencia de medidas cautelares. **Las fiscales del MPA solicitarán la prisión preventiva de los cuatro imputados.**