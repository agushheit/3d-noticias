---
category: La Ciudad
date: 2021-09-18T06:15:40-03:00
thumbnail: https://assets.3dnoticias.com.ar/COLUMNA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Una gran columna de humo atravesó Santa Fe y brigadistas actuarán este sábado
title: Una gran columna de humo atravesó Santa Fe y brigadistas actuarán este sábado
entradilla: Los focos de incendio estarían ubicados en los humedales cercanos a Colastiné
  y San José del Rincón. El viento dejó postales de humo atravesando el cielo santafesino
  en la tarde del viernes.

---
Se volvieron a encender los humedales de Santa Fe luego de dos semanas sin incendios registrados, en este caso, con una gran columna de humo que atravesó el cielo en la tarde santafesina. Los focos ígneos estarían ubicados en humedales próximos a Colastiné y San José del Rincón, dejando postales de la costa en llamas y siendo motivo de preocupación.

En este marco, desde la Secretaría de Protección Civil confirmaron a UNO que durante este sábado está previsto que la Brigada de Atención de Emergencias dispuesta para hacer frente al avance del fuego en las islas actúe en el lugar conforme a los focos que se detectaron.

Durante este viernes, los brigadistas estuvieron avocados a luchar contra los incendios en islas al sur de Sauce Viejo, con un gran foco ígneo que se encendió el pasado jueves y que fue visible desde la capital provincial por su magnitud. En este marco, la brigada trabajó con el helicóptero que se trasladó desde el sur provincial para el transporte hacia las zonas afectadas.

Como historia repetida, todo se encamina a que se trate de una nueva quema de pastizales intencional. Con este panorama el secretario de Protección Civil, Roberto Rioja, manifestó a UNO: "Estamos siempre en la misma. Yo creo que la mano del hombre está siempre en el medio, por dolo, por intención o no pero siempre está. Nosotros nos tenemos que abocar a evaluar cómo podemos contener los incendios".

Los últimos incendios que se habían detectado en los humedales cercanos a la ciudad se extinguieron hace 15 días con la llegada de las lluvias, aunque se extendieron alrededor de 10 jornadas. Además de la brigada de reacción inmediata contra el fuego se habían involucrado bomberos zapadores para colaborar con la contención de los incendios que se propagaron por toda la zona de islas cercana a Alto Verde.