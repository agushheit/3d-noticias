---
category: Agenda Ciudadana
date: 2021-11-25T06:00:15-03:00
thumbnail: https://assets.3dnoticias.com.ar/MORONI.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: 'Moroni: en diciembre "vamos a recuperar todo el empleo perdido durante la
  pandemia"'
title: 'Moroni: en diciembre "vamos a recuperar todo el empleo perdido durante la
  pandemia"'
entradilla: "El titular de la cartera laboral destacó el crecimiento de la economía
  argentina y pronosticó una fuerte suba del empleo en todo el país durante un acto
  que compartió con el ministro de Educación.\n\n"

---
El ministro de Trabajo, Claudio Moroni, afirmó este miércoles que la economía crecerá 10% este año, con lo cual volverá a los niveles previos a la Covid-19, y que "se recuperará todo el empleo perdido durante la pandemia".  
  
"Este año vamos a crecer 10% cuando muchas consultoras, incluida la OCDE, dijeron que íbamos a tardar cinco años en recuperar lo perdido durante la pandemia", subrayó el ministro al hablar en la Escuela Técnica Roberto Rocca, propiedad del Grupo Techint, en la localidad bonaerense de Campana.  
  
Allí también indicó que "cuando podamos medir la situación de diciembre próximo, que la publicaremos en febrero, vamos a haber recuperado la totalidad del empleo perdido durante la pandemia, como mínimo".  
  
Al respecto, destacó que "hay sectores, como la manufactura y la construcción, que están arriba de los empleos que tenían antes de la pandemia".

> "Cuando podamos medir la situación de diciembre próximo, que la publicaremos en febrero, vamos a haber recuperado la totalidad del empleo perdido durante la pandemia, como mínimo"Claudio Moroni

  
Moroni participó del relanzamiento del Departamento de Educación y Formación Profesional de la Unión Industrial Argentina (UIA), junto con su par de Educación, Jaime Perczyk, el presidente del Grupo Techint, Paolo Rocca, y el titular de la entidad fabril, Daniel Funes de Rioja.  
  
El titular de la cartera laboral sostuvo que "los puestos de trabajo requieren cada vez más habilidades", y subrayó que "el desafío es cómo dotar a la gente de esas habilidades".  
  
El ministro consideró que "la educación profesional no puede ser un remedo de la educación formal", y evaluó que "replicar un esquema rígido en la educación profesional, lleva a repetir los problemas de siempre".  
  
Puntualizó que "el desafío es cómo validar y certificar estas múltiples formas de transmisión de conocimiento".  
  
Además indicó que "otro desafío es reconocer las habilidades adquiridas en los lugares de trabajo", y consideró que para eso es preciso "trabajar en la empleabilidad, en la formación".  
  
Al respecto advirtió que "si no se acompaña todo esto con un proyecto de desarrollo, y vamos a un proyecto de desaparición de la industria y de las empresas, entonces se conduce al vacío".  
  
"Esto requiere además de estos detalles, un proyecto político de desarrollo legitimado por todos. Me preocupa a veces, cuando se vuelve a ideas que mostraron que no sirvieron para el desarrollo", señaló Moroni.  
  
En la misma línea, Perczyk afirmó que "la formación profesional y la mejora de los trabajadores y las trabajadoras es central" para el desarrollo, y destacó que "hay un objetivo que es cumplir con la ley de educación técnico profesional, con los recursos para capacitar, equipar y formar".  
Sostuvo que "la gestión pública tiene que resolver los problemas de ahora y los históricos y hacerlos a la vez", y remarcó que es preciso también "resolver el problema de la escuela secundaria para que no se sigan yendo los chicos: los que se fueron durante la pandemia y también los que lo hicieron antes".  
  
"Estamos trabajando en otro modelo de escuela secundaria para chicos que se fueron. Para que se integren alrededor de la formación profesional y de una salida laboral", indicó el ministro, quien subrayó que el objetivo es que los y las estudiantes puedan "insertarse en la escuela y en el mundo del trabajo".  
  
Por su parte, Rocca aseguró que "hay un consenso total sobre la educación" entre los privados y el Estado, porque afirmó que "la educación transforma las sociedades, los individuos, y es el factor esencial para permitir la movilidad social".  
  
En ese sentido, sostuvo que "el progreso civil económico y social de una comunidad está estrictamente relacionado con la educación, pero en particular con la educación técnica", porque puntualizó que "la educación técnica enseña a comprender la realidad".  
  
Asimismo, consideró que para el proyecto educativo del Grupo Techint "el mérito y el desempeño merecen ser reconocidos", y añadió que "hay un esfuerzo que merece un reconocimiento".  
  
"Nos focalizamos en los sectores de menores ingresos. Que cualquier chico de cualquier nivel social y que tenga talento, pueda realizarlo", remarcó Rocca, quien precisó que "la educación, la educación técnica, el reconocimiento al mérito y la promoción de los talentos son los valores fundamentales de los programas de educación del Grupo Techint".  
  
Al respecto, sostuvo que "la relación entre la educación y el mundo del trabajo contribuye a la productividad de las personas y las empresas", y puso de relieve que "es un factor fundamental y reduce el espacio de los que no estudian ni trabajan, y permite a todos tener oportunidades en la vida".  
  
Por su lado, Funes de Rioja, propuso "ir a la cultura profesional, no de la confrontación sino de la idoneidad".  
  
"No estamos para competir por proyectos, sino para trabajar ensambladamente en proyectos. Lo que tenemos como nación es un desafío en común: hacer que nuestros jóvenes, muchos de ellos desalentados, miren el desafío del futuro y lo miren en el país, con la esperanza y la promesa de que sus méritos y su esfuerzo los van a llevar a un escenario de progreso", afirmó el titular de la UIA.  
  
A su criterio, "ese es el camino al desarrollo inclusivo", y remarcó que "es con las pymes, las micro, las pequeñas y las medianas, junto con las grandes".  
  
"Y con la inclusión social a través del empleo, la dignidad del trabajo y el progreso social, que permita una sociedad homogénea de un país industrial desarrollado", concluyó el dirigente gremial empresario.