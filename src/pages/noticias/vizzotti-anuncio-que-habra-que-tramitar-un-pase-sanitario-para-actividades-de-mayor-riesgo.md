---
category: Agenda Ciudadana
date: 2021-11-27T06:00:26-03:00
thumbnail: https://assets.3dnoticias.com.ar/VIZZOTTIAGOSTO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: Vizzotti anunció que habrá que tramitar "un pase sanitario para actividades
  de mayor riesgo"
title: Vizzotti anunció que habrá que tramitar "un pase sanitario para actividades
  de mayor riesgo"
entradilla: 'La ministra de Salud detalló que el pase sanitario será otorgado a través
  de la app Cuidar para aquellos mayores de 13 años que tengan esquema de vacunación
  completo al menos 14 días antes de tramitar el permiso.

'

---
La ministra de Salud de la Nación, Carla Vizzotti, anunció hoy que habrá que tramitar "un pase sanitario para actividades de mayor riesgo", entre los que se incluyen "eventos masivos y en espacios cerrados".

"Otro consenso del Cofesa (Consejo Federal de Salud) fue un pase sanitario para actividades de mayor riesgo", resaltó Vizzotti, y detalló que será otorgado a través de la app Cuidar para aquellos mayores de 13 años que tengan esquema de vacunación completo al menos 14 días antes de tramitar el permiso.

Durante una conferencia de prensa en la Casa Rosada, precisó que el pase sanitario es "una medida que va hacer que quienes demoraron su esquema tengan el estímulo para dar ese pasito para percibir la importancia que tiene completar el esquema de vacunación".

Al ser consultada sobre los detalles del "pase sanitario", Vizzotti pidió "paciencia" y dijo que "hay que instrumentar la medida".

En ese marco, la funcionaria nacional afirmó que "la pandemia no terminó", y puntualizó que se van a "postergar vuelos directos con el continente africano" ante la aparición de una nueva variante del coronavirus en esa región, la cual consideró que "no hay que subestimar".

"Íbamos a reiniciar los vuelos a África de forma directa a mediados de diciembre, pero decidimos postergar ese inicio de los vuelos directos", agregó.

Además, la ministra de Salud puntualizó: "Hay personas que pudieron haber estado en el continente africano en los últimos 14 días y entran por tierra o a través de cruceros, deben hacer PCR y aislamiento de 14 días".

En tanto, la funcionaria informó que se recomienda "levantar la PCR" para el ingreso de personas procedentes de "los países limítrofes en los puntos de ingreso terrestres y marítimos que estén vacunadas", en las situaciones que "tengan una incidencia menor de 75 casos por 100 mil habitantes en los últimos 14 días".

En otro tramo de la conferencia, Vizzotti indicó que "el primer consenso" logrado en el marco del Cofesa es que "diciembre será el mes de las segundas dosis" para que la población "complete su esquema de vacunación".

En ese sentido, estimó que "hay 7 millones de personas en condiciones de recibir la segunda dosis", pero aún no lo hicieron, y reforzó la idea de que asistan a completar los esquemas.

Respecto de la situación sanitaria, consideró que Argentina está en momento "favorable" con una "cantidad de casos bajos", pese a la "circulación de la variante Delta".

"Estamos viendo lo que está pasando en Europa. Argentina no es comparable, tiene características de su campaña de vacunación diferente. Tenemos una situación más favorable y una oportunidad muy grande de avanzar con el programa de vacunación", argumentó.

Al referirse a la nueva variante de coronavirus detectada en Sudáfrica, bautizada como "Omicron", Vizzotti indicó que se trata de una cepa "de preocupación".

"Se está estudiando una nueva variante de preocupación. Aún no tiene información sobre la letalidad y su respuesta a las vacunas", concluyó.