---
category: Agenda Ciudadana
date: 2021-11-07T06:15:15-03:00
thumbnail: https://assets.3dnoticias.com.ar/hidrogeno.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Hidrógeno verde, el combustible del futuro
title: Hidrógeno verde, el combustible del futuro
entradilla: 'El doctor en ingeniería química del Conicet Santa Fe, Pío Antonio Aguirre
  brindó precisiones sobre el hidrógeno verde, combustible que se producirá en Argentina

'

---
La empresa energética australiana Fortescue Future Industries (FFI) confirmó este lunes una millonaria inversión en Río Negro. Sus autoridades indicaron que invertirán 8.400 millones de dólares y crearán más de 15.000 puestos de trabajo directos en la Argentina para producir hidrógeno verde, un combustible que proveerá energía "renovable".

En diálogo con el programa Ahí Vamos (de 9 a 12 por FM 106.3 La Radio de UNO) el ingeniero químico Pío Antonio Aguirre aclaró que el color verde no es el color del gas sino que es una denominación técnica para identificar la forma en que se puede producir el hidrógeno.

“Verde está asociado a una forma ambientalmente benévola y se produciría a partir de electrones en el agua”, precisó. La obtención de hidrógeno se hace sobre el agua, aplicando energía que proviene de la electricidad para separar la molécula de agua en moléculas de hidrógeno y moléculas de oxígeno. Así “se obtiene hidrógeno en una forma que en el ambiente técnico se decidió darle el nombre de verde, a diferencia de otras formas de tener hidrógeno que no serían ambientalmente tan benévolas”, explicó Aguirre.

La generación de hidrógeno se realiza en la actualidad a partir de otras fuentes que motivan distintas definiciones según la fuente original: cuando es a partir de petróleo o carbón se llama hidrógeno negro; cuando se utiliza gas natural es hidrógeno gris; y cuando se hace a partir de gas pero con tecnología de captura y almacenamiento de carbono se le dice hidrógeno azul.

“En Argentina se está trabajando hace mucho tiempo en el tema de hidrógeno, tanto en el sistema científico, Conicet, las universidades, hay distintos grupos”, dijo el ingeniero. “En Latinoamérica, en el caso de la energía eólica, el país más beneficiado es Argentina. Este tema del hidrógeno verde a partir de energía eólica creo que es el que más ventaja lleva”.

**El mundo apunta al hidrógeno**

No es una coincidencia que el anuncio lo haya dado el presidente Alberto Fernández desde la ciudad escocesa de Glasgow, donde se desarrolla la cumbre sobre cambio climático COP26.

"En poco tiempo nuestro país podrá convertirse en uno de los proveedores mundiales de este combustible que va a permitir reducir drásticamente las emisiones de carbono en el mundo", dijo el jefe de Estado.

La principal fuente de gases de efecto invernadero es el carbón y, según un informe que se conoció esta semana, el uso de ese combustible fósil recuperó niveles récord tras una reducción por los confinamientos por el coronavirus y la parálisis de la economía global en 2020.

“El objetivo final es el uso del hidrógeno como combustible de vehículos para la parte de transporte”, puntualizó Aguirre, calculando que el desarrollo se dará en etapas, entre las cuales se incluye la producción de fertilizantes a partir de amoníaco. Pero la meta es el reemplazo de los combustibles fósiles.

Más de 40 países en la conferencia climática COP26 acordaron eliminar gradualmente el uso de energía a carbón, el combustible fósil que más contribuye al calentamiento global, por encima del petróleo y el gas.

**¿Por qué en Río Negro?**

Este lunes se conoció el acuerdo entre Argentina y la empresa australiana Fortescue para producir hidrógeno verde en Río Negro. El proyecto permitirá convertir a la provincia patagónica en un polo mundial exportador de hidrógeno verde en 2030, con una capacidad de producción de 2,2 millones de toneladas anuales.

Fortescue ya comenzó los estudios en los alrededores de la ciudad de Sierra Grande para analizar la cantidad y calidad de los vientos, la fuente energética principal para la producción.

“Hay una serie bastante importante de factores que tienen que ver con la producción de hidrógeno, sobre todo usando energía eléctrica, utilizando también esa energía provista de alguna forma renovable”, dijo el investigador del Conicet. “La patagonia es uno de los mejores lugares del mundo para este tipo de emprendimientos; y el norte de Argentina tiene energía solar que también puede producir electricidad”.

El proceso de producción de hidrógeno verde requiere contar con tres variables ineludibles y abundantes, ya que demanda viento o luz solar para la generación eléctrica renovable que se aplica al proceso de electrólisis; agua para separar el hidrógeno del oxígeno y una locación para instalar generadores eólicos o paneles solares.

“El agua tiene que tener una calidad bastante específica para esta aplicación, o sea que se necesita una planta de tratamiento de agua para poder producir hidrógeno”, dijo Aguirre. “No puede ser agua de cualquier calidad y tiene que tener una cercanía con algún sistema eléctrico, con alguna red troncal, porque el movimiento eléctrico a través de las redes de alta tensión es un factor clave”.

Según el Ministerio de Ambiente, la tecnología del hidrógeno verde suscita cada vez más interés "como una estrategia de transición energética”. El desarrollo de esta alternativa se presenta para Argentina como una oportunidad para desarrollar proveedores competitivos en toda la cadena, crear nuevos empleos y aumentar significativamente las exportaciones, a la vez que se protege el ambiente local y global.