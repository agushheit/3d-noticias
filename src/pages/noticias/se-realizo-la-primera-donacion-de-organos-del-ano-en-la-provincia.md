---
category: Estado Real
date: 2021-01-05T11:50:39Z
thumbnail: https://assets.3dnoticias.com.ar/hospital-Cullen.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Se realizó la primera donación de órganos del año en la provincia
title: Se realizó la primera donación de órganos del año en la provincia
entradilla: Se llevó adelante en el Hospital Cullen de Santa Fe, hecho que posibilita
  que cuatro personas de distintos lugares del país puedan mejorar su salud.

---
El Ministerio de Salud, a través del Centro Único de Donación, Ablación e Implantes de Órganos y tejidos (Cudaio) informó que el primer fin de semana del año presentó actividades en el ámbito de la donación y trasplantes de órganos a partir de un acto de donación que tuvo lugar en el Hospital Cullen de la capital santafesina. Este hecho posibilitó que cuatro personas de distintos lugares del país hayan recibido un trasplante de órganos.

En esta oportunidad se ablacionaron corazón, renopáncreas, hígado y riñón. Cabe destacar que, en poco más de 30 horas que duró el procedimiento de donación, **participaron tres equipos de trasplante de las provincias de Mendoza, Buenos Aires y Santa Fe**.

La médica de la Unidad de Procuración y Trasplante del Hospital, Eugenia Chamorro, destacó «la complejidad del procedimiento, en tanto los desafíos propios realizarlo en pandemia, donde es necesario tomar todas las medidas de higiene y seguridad correspondientes, sumado a un fin de semana largo, donde los tiempos y formas suelen ser más extendidos que en un día de semana regular. En este contexto, es más remarcable que nunca, la profesionalidad y el compromiso del personal sanitario».

Además, agregó que «el hospital tiene una unidad de procuración consolidada y ejemplar, y un personal de terapia intensiva comprometido con esta problemática de salud y consciente de las dificultades de que con esta pandemia la generación de donantes conlleva un gran desafío».

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.1em; font-weight: 700">581 SANTAFESINOS EN ESPERA </span>**

En la actualidad, la lista de espera nacional está conformada por 9333 personas que necesitan un trasplante de órganos y tejidos, de los cuales 581 corresponden a la provincia de Santa Fe.

Las vías para expresar la voluntad hacia la donación de órganos son completando un acta de donación en las oficinas de CUDAIO, o vía web en el portal [www.cudaio.gob.ar](https://cudaio.gob.ar/)