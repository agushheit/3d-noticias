---
category: La Ciudad
date: 2021-11-24T06:00:21-03:00
thumbnail: https://assets.3dnoticias.com.ar/chaqito.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'El Chaquito: Monte Vera pide un operativo de seguridad especial para el
  verano'
title: 'El Chaquito: Monte Vera pide un operativo de seguridad especial para el verano'
entradilla: "El presidente Comunal reclamó mayor presencia policial y un nuevo destacamento.
  El lunes, en presencia de grupos familiares y de jóvenes, asesinaron a un muchacho
  de 18 años. \n"

---
El presidente comunal de Monte Vera, Alberto Pallero exigió un operativo de seguridad especial para el verano en El Chaquito. Según dijo, el pedido de mayor vigilancia y presencial policial en la zona es reiterado; y no solo para popular paraje, sino en todo el pueblo. En ese sentido, insistió con la necesidad de un nuevo destacamento que se sume a la comisaría 20ª

El hecho de sangre del lunes, a plena luz del día y ante la mirada incrédula de familias y jóvenes que se encontraban disfrutando de la playa, conmovió a los vecinos de la zona quienes desde hace tiempo vienen padeciendo cierto descontrol en el lugar.

Xavier Alan Emanuel Romero de 18 años recibió entre cinco y siete disparos. El asesinato superó a cualquier episodio violento y peligroso que suelen ocurrir en la zona: picadas de motos, derrapes de autos y cuatriciclos; fiestas clandestinas, mugre y basura sin juntar; y hasta accidentes fatales.

"Es una preocupación, cada vez mayor", le dijo a UNO Santa Fe el titular de la comuna. "Yo vengo diciendo que Monte Vera necesita otro destacamento, más personal. Nos mandaron un vehículo nuevo, bienvenido sea. Pero no alcanza, no alcanza porque es un pueblo que está creciendo a paso agigantados", señaló.

Reconoció que mantiene poco contacto y diálogo con las actuales autoridades de la comisaría y dijo desconocer la cantidad de personal con que cuenta en la actualidad. "Voy a seguir golpeando puertas hasta que el ministerio las abra y nos atienda", afirmó.

"Conozco las 7.525 hectáreas; las conozco a todas y nunca pasó lo que está pasando últimamente ahora" subrayó el presidente comunal, al mismo tiempo que pidió "soluciones" en materia de seguridad.

Consideró que con operativos de seguridad los fines de semana, similares a los que suelen orfanizarse en las playas de la ciudad de Santa Fe, la situación podría mejorar. En ese sentido, recordó que El Chaquito es una de los espacios más concurridos durante los fines de semana de primavera y verano.

"Hemos comprados dos motos (La comuna) para que los inspectores salgan a la calle en moto y acompañados con policías para poder hacer más fáciles los recorridos. Con la presencia de ellos se podrían evitar muchísimas cosas", destacó.

Con respecto al asesinato del lunes, señaló: "Posiblemente se trató de un ajuste de cuentas. Pero como lo hicieron en la playa, lo pudieron haber hecho en el centro del pueblo o en Santa Fe (capital). Pero si hubiese habido algún móvil dando vueltas o alguna moto de la policía o algo por el estilo no hubiese pasado".

Asombrado por el suceso, apuntó: "Esto pasó entre el tumulto de gente que simplemente sale a tomar un poco de oxígeno en estas playas hermosas; que están limpias, ordenadas". Advirtió que las balas podrían haber alcanzado a cualquier inocente que se encontraba en el lugar.