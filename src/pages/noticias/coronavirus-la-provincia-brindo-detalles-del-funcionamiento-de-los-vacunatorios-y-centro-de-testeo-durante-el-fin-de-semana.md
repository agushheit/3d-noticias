---
category: Estado Real
date: 2021-11-13T06:15:04-03:00
thumbnail: https://assets.3dnoticias.com.ar/VACUNASFINDE.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Coronavirus: la provincia brindó detalles del funcionamiento de los vacunatorios
  y centro de testeo durante el fin de semana'
title: 'Coronavirus: la provincia brindó detalles del funcionamiento de los vacunatorios
  y centro de testeo durante el fin de semana'
entradilla: El Ministerio de Salud dio a conocer el cronograma correspondiente a las
  ciudades de Santa Fe y Rosario.

---
El gobierno provincial, a través del Ministerio de Salud, brindó detalles sobre el funcionamiento de los puestos de testeo y la continuidad del operativo de vacunación para el Covid-19 el fin de semana, al igual que el lunes 15 de noviembre, asueto provincial.

En la ciudad de Santa Fe los puestos de testeo de Coronavirus funcionarán el sábado en horario habitual de 9 a 13 horas, en la Estación Belgrano, El Alero Dorrego, Estación Mitre, Playón Municipal, Viejo Hospital Iturraspe y CEMAFE.

En tanto que el domingo estarán cerrados el hospital Iturraspe y CEMAFE, funcionando con normalidad el resto de los puestos.

Asimismo, el lunes 15 de noviembre, en el marco del asueto provincial por el aniversario de la fundación de la ciudad de Santa Fe, todos los espacios funcionarán con normalidad.

Por otra parte, en la ciudad de Rosario se pueden realizar test de Coronavirus en los efectores provinciales, y además en el estacionamiento del Galpón 13, de 9 a 17 horas, tanto el sábado como el lunes. El domingo sólo estarán disponibles los hospitales.

**VACUNACIÓN EN SANTA FE Y ROSARIO**  
Respecto a la vacunación en la capital provincial, el sábado se aplicarán 2.280 dosis de Pfizer para personas de 12 a 17 años en la Esquina Encendida (de 9 a 16.30 horas); y 1.440 segundas dosis de Sinopharm para personas de 3 a 11 años en La Redonda (de 8 a 15.30 horas).

El lunes se vacunarán en los mismos lugares, y en el primer vacunatorio fueron citados 2.510 personas de 12 a 17 años para vacunas Pfizer, mientras que al segundo irán 1.440 personas de 3 a 11 años para dosis de Sinopharm.

A su vez, en la ciudad de Rosario, el sábado la vacunación se desarrollará en el hospital de Niños Zona Norte (de 8:30 a 11;30 horas) y fueron citadas 350 personas de 3 a 11 años. En la Rural se vacunarán tanto el sábado como el lunes, de 8:30 a 16 horas; el sábado se aplicarán 5.504 vacunas Pfizer para personas de 12 a 17 años, y el lunes han sido citados 4.000 personas de 3 a 11 años para recibir dosis Sinopharm.