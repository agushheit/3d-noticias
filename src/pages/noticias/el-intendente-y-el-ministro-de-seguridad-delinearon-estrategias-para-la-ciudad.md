---
category: La Ciudad
date: 2021-03-31T07:17:56-03:00
thumbnail: https://assets.3dnoticias.com.ar/seguridad.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad Santa Fe
resumen: El intendente y el ministro de Seguridad delinearon estrategias para la ciudad
title: El intendente y el ministro de Seguridad delinearon estrategias para la ciudad
entradilla: Emilio Jatón fue recibido por el nuevo titular de la cartera, Jorge Lagna.
  Intercambiaron datos, analizaron el mapa del delito y definieron una serie de reuniones
  en los próximos días con representantes de las vecinales.

---
En la sede del Ministerio de Seguridad de la provincia, el intendente Emilio Jatón se reunió esta mañana con el nuevo ministro del área, Jorge Lagna, para analizar y delinear políticas a aplicar en la ciudad de Santa Fe. Intercambiaron datos, analizaron el mapa del delito y definieron una serie de reuniones que se van a concretar en los próximos días con representantes de las vecinales. Además, el mandatario local anticipó que en pocas semanas convocará al Consejo de Seguridad.

Al salir del encuentro, el intendente aseguró que la Municipalidad pondrá a disposición todas las herramientas a su alcance para colaborar con la seguridad, entre ellas las 170 cámaras de videovigilancia que posee la ciudad en distintos puntos, y destacó la importancia de comenzar a implementar el sistema de comunicaciones Tetra. De esa manera, mejorará la comunicación entre los operadores del 0800 de la Municipalidad y la Policía de Santa Fe. 

“A la seguridad hay que abordarla de muchos lugares y uno de ellos es charlando con los vecinos; por eso, en los próximos días van a comenzar las reuniones con las distintas vecinales -de los dos equipos-, escuchando, hablando y proponiendo”, anticipó Jatón, y en esta línea agregó: “Hoy le trajimos al ministro propuestas y además cruzamos datos. Vamos a poner a disposición las 170 cámaras que tenemos en la ciudad para monitorear, y también nuestros empleados municipales para colaborar en los distintos procedimientos ciudadanos que hagamos”. 

El mandatario local consideró que “la reunión fue interesante, había que hacerla y sacar conclusiones que sirvan para dar respuestas reales a nuestros vecinos”. “Los delitos mutan, sabemos lo que pasa con las motos, con los robos, ya no hay un solo lugar, sino que la inseguridad está en gran parte de la ciudad y por eso estamos trabajando en acciones más precisas”, dijo más adelante Jatón. 

En cuanto a los controles y operativos que se van a llevar a cabo, el intendente contó que “hay una mesa de coordinación que va a empezar a trabajar esta semana para detectar los lugares y así actuar en esos sectores específicos. Sabemos donde transita la mayor cantidad de autos, la entrada y salida de los barrios, en las avenidas por donde pasan las motos y sobre todo en eso se va a trabajar”.

En cuanto a las cámaras, Jatón expresó: “Los técnicos dicen que es posible un nuevo sistema de videovigilancia, donde se compatibilicen ambas herramientas, por eso se están reuniendo y le van a encontrar la vuelta; es bueno sumarle las cámaras municipales para que la policía monitoree y le vamos a sumar un sistema de comunicación que se llama Tetra y así tener un vínculo más rápido con la policía”.

 

**Trabajo conjunto**

Por su parte, el ministro de Seguridad le agradeció al intendente de Santa Fe por el encuentro: “Fue uno de los primeros que me llamó cuando asumí para trabajar en conjunto”, dijo, y de inmediato agregó: “Emilio está totalmente comprometido con el tema de la seguridad de los ciudadanos. Fue una reunión de trabajo en la que se fijaron líneas de acción”. 

“Nosotros le contamos al intendente lo que viene para Santa Fe, puntualmente, en cuanto al recurso humano y a la tecnología. En poco tiempo vamos a tener muchos más agentes en las calles. Estamos trabajando para sacar más gente de los escritorios y traerlas a la calle, donde la ciudadanía la requiere”, anticipó. 

En esta línea, detalló que “en septiembre habrá un egreso de 400 agentes más, y están llegando alrededor de 30 motos para Santa Fe”. Luego agregó: “También le explicamos a Emilio el trabajo que estamos haciendo con la incorporación de más tecnología, a ese recurso humano le vamos a agregar mucha tecnología en lo que denominamos los Centros Operativos Policiales. Santa Fe, Rosario y Rafaela serán los primeros centros que se van a habilitar”.

 

**En la calle** 

Más adelante, Lagna dijo que se decidió hacer un trabajo conjunto entre los equipos técnicos para unificar los sistemas de cámaras de seguridad. “Hay dos sistemas de videovigilancia, municipal y provincial, ya están trabajando los técnicos de ambos equipos y vamos a crear un trabajo conjunto más fuerte”, explicó, en relación a compatibilizar ambas herramientas técnicas.

Luego, el ministro de Seguridad de la provincia dijo: “Desde mañana se empiezan a intensificar los operativos conjuntos de motos, vamos a hacer la experiencia de Rosario, es decir operativos de saturación preventivos en distintos barrios de la ciudad, según lo que indica el mapa del delito. Vamos a atacar esos lugares, sin resentir el patrullaje que se hace siempre”. 

“El delito predatorio, que es el que más molesta a la gente en las zonas comerciales, va a estar mucho más cubierto con la incorporación de las 30 motos. Vamos a ir juntos con Emilio a hablar con el fiscal Baclini, para trabajar en una acción multiagencial que hoy sólo se está haciendo en Las Lomas y Cabal”, manifestó más adelante. 

Destacó la preocupación de las armas en la calle y en este sentido, dijo que se hará un fuerte control en las armerías. “Nuestra agencia de armas va a trabajar con personal municipal. Ustedes saben que el año pasado fue récord en secuestro de armas. La Municipalidad está muy preocupada por la cantidad de armas que hay en la calle, así que vamos a trabajar en eso. En la inspección de las armerías, como así también de motos y remises. Una serie de medidas concretas que vamos a empezar a implementar a partir de mañana mismo”, detalló.

Antes de finalizar, Lagna contó: “Arrancamos un programa de vecinales: los equipos de Seguridad y de la Municipalidad vamos a recorrer todas las semanas una vecinal de la ciudad. La gente hay veces que pide más policías, pero también iluminación, bacheo, vamos a ser un solo equipo entre el ministerio y la Municipalidad”.

**Presentes**

En el encuentro también estuvieron los secretarios municipales de Control, Virginia Coudannes, y de Integración y Economía Social, Mariano Granato; el director ejecutivo de Actividades, Vía Pública y Transporte Municipal, Guillermo Álvarez; y el subsecretario de Convivencia Ciudadana, Fernando Peverengo. Y por el lado de Provincia participaron el secretario de Política y Gestión de la Información del Ministerio de Seguridad, Jorge Fernández; el secretario de Seguridad Pública, Germán Montenegro; el coordinador de Seguridad Social y Territorial de Santa Fe, Facundo Bustos; y las jefas de la Policía de la provincia, Emilce Chímenti, y de la URI, Marcela Muñoz.