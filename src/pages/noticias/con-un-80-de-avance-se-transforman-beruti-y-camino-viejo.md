---
category: La Ciudad
date: 2021-12-24T06:00:04-03:00
thumbnail: https://assets.3dnoticias.com.ar/beritti.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Con un 80% de avance, se transforman Beruti y Camino Viejo
title: Con un 80% de avance, se transforman Beruti y Camino Viejo
entradilla: El intendente Emilio Jatón recorrió los trabajos que se ejecutan en este
  sector de la ciudad. Pavimento, bacheo, desagües pluviales e iluminación led son
  algunas de las obras que se concretan.

---
“Ya se ven los cambios”, dijo el intendente Emilio Jatón al recorrer las obras que se ejecutan sobre Camino Viejo y Beruti. En la primera ya se ejecutó el cantero central, se colocaron las columnas con la iluminación led y se instalaron las garitas en las paradas del transporte público, mientras restan algunos detalles para completar el proyecto. En la segunda, ya se concretaron los trabajos que incluyen bacheo, pavimento y desagües pluviales. El avance es del 80%, pero ya se ve la transformación y los vecinos lo destacan a cada paso.

Jatón recordó que en Camino Viejo, “antes había un zanjón de unos 5 metros de ancho y de profundidad nadie sabía cuánto. Estaba allí de toda la vida y hoy ya no está. Estamos haciendo un bulevar iluminado con luces led que aún no está terminado. Falta un poco todavía, pero está bueno mirarlo y decir que cuando uno promete este tipo de obras y la hace, estamos cumpliendo”, resumió.

“Estamos haciendo lo que tenemos que hacer, por lo menos en este lugar de la ciudad. Sabemos que falta mucho pero había que empezar por algún lugar y lo hicimos por este Camino Viejo y la famosa calle Beruti. La política sirve para transformar la vida de la gente. A veces no se puede llegar a todos los lugares, pero a esto lo habíamos prometido y hoy ya no es un sueño”, insistió.

Con respecto a calle Beruti y la transformación que tendrá, el intendente dijo que “ya estamos pensando en colocar reductores de velocidad, en ver cómo convivimos con estas nuevas obras, en evaluar cómo van a hacer las líneas de colectivos para ingresar a este lugar. Estamos cerca, más cerca de lo prometido y la gente lo reconoce porque le cambia la vida”, afirmó.

El monto de inversión en estas obras ronda los $500 millones y, en este sentido, Jatón manifestó que “el dinero destinado es mucho pero era fundamental terminar con un atraso de más de 50 años. Eran trabajos necesarios y los estamos haciendo”.

**Un antes y un después**

Ricardo Ochaval, Hugo Salteño y Daniel Benitez son tres vecinos de la zona de Camino Viejo y Beruti. Con entusiasmo recibieron al intendente y recorrieron las calles que ahora tienen pavimento. “La obra por Beruti era muy necesaria, sobre todo los días de lluvia, porque se hacían unos pozos enormes. Lo de Camino Viejo fue un regalo, pero Beruti era urgente. Todo este sector se transformó, cambió mucho la cara; mejoró para el tránsito y los desagües. Es una obra que hace mucho tiempo estaba prometida y no pasaba nada”, recordó Ochaval.

Por su parte, Salteño aseguró que “es un antes y un después” y que “la obra de calle Beruti le viene muy bien al barrio. Teníamos muchos problemas los días de lluvia; es una obra muy bien realizada, con desagües, no pensábamos que tuviera esta magnitud. Y Camino Viejo fue un regalo que tampoco esperábamos. El intendente nos contó que se va a ampliar hacia autopista y así va a quedar increíble. Le hace muy bien al barrio porque en los últimos años, esta zona tuvo un gran crecimiento y era necesaria una obra de esta magnitud”.

“Ahora cada vez que llueva vamos a estar más tranquilos. Antes nos quejábamos por la tierra o por el barro, ahora eso no va a estar más. Le agradecemos al intendente que vino, nos prometió y lo cumplió”, concluyó.

Por último, Benitez, que hace más de 40 años que vive en barrio Los Troncos, calificó a la obra como “magnífica. No hay palabras para definir esto porque de 42 años que estoy acá nunca vi una máquina trabajar. Nos hicieron un asfaltito de brea que duró muy poco tiempo y era sufrir porque el colectivo no quería entrar, los vecinos tenían que caminar sobre el barro para salir. Tenemos un complejo educativo muy grande y cada vez que llovía no podían acceder a las escuelas por el barro”, relató, sobre la realidad que vivían los vecinos.

Para cerrar, dijo: “Hoy estamos muy contentos y orgullosos por lo que hicieron. No hay palabras para definirlo, esto es magnífico. Tenemos luz, paradas de colectivo con garitas, no lo podemos creer. Es un cambio al cien por ciento. Felicito al intendente y a todos los que se pusieron las pilas para darnos este beneficio”, finalizó.

**Las obras**

Son más de 15 mil vecinas y vecinos los que se beneficiarán con estos trabajos sobre calle Beruti que incluyen bacheo, pavimento y desagües pluviales. La obra se enmarca en el Plan Incluir del gobierno provincial, que cuenta con un presupuesto de 234 millones de pesos. En tanto, el proyecto y el control de obras son de la Municipalidad.

En una primera etapa, entre Arzeno y Cafferata se realizaron trabajos de bacheo. En tanto, entre Cafferata y Camino Viejo: se colocó pavimento de hormigón de 10 metros de ancho junto a los desagües pluviales. Y la segunda etapa, entre Camino Viejo y Pasaje Geneyro, contempla pavimento de hormigón a dos calzadas de 7 metros y un cantero central de 4 metros con los correspondientes desagües pluviales. En tanto, entre Pasaje Geneyro y Furlong, se demolió la carpeta asfáltica existente y se colocó un nuevo pavimento de hormigón y desagües.

Por otra parte, los habitantes de los barrios Los Troncos, Santo Domingo, Acería, Juventud del Norte y San Ignacio de Loyola Sur se beneficiarán directamente con la obra que transformará Camino Viejo de manera integral. El presupuesto oficial asciende a $ 204.406.284 y se ejecuta dentro del Plan Integrar, con financiamiento del programa nacional Argentina Hace II.

En la zona se intervinieron 700 metros con un cantero central de siete metros que incluye ciclovía y vereda peatonal. El nuevo trazado vial también contempla tanto la conexión con los bulevares proyectados en la intersección con Gorriti y Beruti, como los dos cruces de calles a nivel de vereda del cantero central, en pasaje Guevara y Matheu. Con respecto a la iluminación urbana, comprende la provisión y colocación de 48 columnas y 72 artefactos tipo led sobre el cantero central.

El proyecto de desagüe pluvial consistió en la ejecución de dos colectores: uno secundario en Camino Viejo para la cuenca Gorriti y un desagüe pluvial en la cuenca Flores. Se complementará con elementos que permitan las captaciones, la inspección, la limpieza y el mantenimiento.