---
category: Agenda Ciudadana
date: 2021-11-11T06:15:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/BERNI.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: 'Kicillof puso en duda la continuidad de Sergio Berni post elecciones: “Tendremos
  que ver”'
title: 'Kicillof puso en duda la continuidad de Sergio Berni post elecciones: “Tendremos
  que ver”'
entradilla: El gobernador bonaerense adelantó que tras los comicios del domingo anunciará
  medidas, y aseguró que “va a haber novedades”.

---
Los rumores que afirman que el ministro Seguridad de la provincia de Buenos Aires Sergio Berni no seguiría en el Frente de Todos, una vez pasada las elecciones, aumentan día a día. Esta vez, el gobernador Axel Kicillof, consultado por la continuidad de su funcionario, y a pesar de haber manifestado "conformidad" con su labor, abonó a la idea que cada día cobra más fuerza.

"Y... después de las elecciones tendremos que ver en términos del Gobierno, voy a anunciar medidas y va a haber novedades, pero puedo decir que estamos conformes y tocó una etapa complicada", sostuvo en declaraciones radiales.

Al momento el titular de la cartera de Seguridad cruzó varios entredichos con su par nacional Aníbal Fernández, a pesar de mantener una relación de amistad. En la misma sintonía, son incontables las oportunidades en las que Berni descalificó al presidente Alberto Fernández a quien tildó de "hipócrita".

Por otro lado, el exministro de economía hizo referencia a la situación de la inseguridad que se vive en la provincia: “Es un tema estructural y muy complejo”. En torno al atraco que derivó en el homicidio del kiosquero en Ramos Mejía, Kicillof aseguró que se trató de “una muerte dolorosísima e injustificable”, y destacó la labor de "la policía atrapó a los responsables".

También, manifestó que "se debe trabajar mucho en la reincidencia, que es muy grave", y agregó: "Los delitos deben ser perseguidos y los que los cometen deben ser castigados".

Por último, el gobernador criticó y cuestionó la gestión del expresidente Mauricio Macri, y la exgobernadora María Eugenia Vidal: "Cuando uno mira 2019 contra lo que ocurrió en 2020 en la provincia sobre el egreso del sistema penitenciario, en 2018 salieron 40.000, en 2019 fueron 42.000 y 2020, 32.000. Más allá de que eso no lo decide el Poder Ejecutivo sino el Judicial, se ha mentido mucho. Con respecto a la seguridad, hablaban de la lucha contras las mafias pero bajaron 25% el sueldo a la policía y los dejaron sin chaleco y patrulleros", y concluyó: "No lo digo para decir 'ah pero Macri', sino para ver desde dónde habla cada uno".