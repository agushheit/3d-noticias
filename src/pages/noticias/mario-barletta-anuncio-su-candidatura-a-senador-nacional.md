---
category: Agenda Ciudadana
date: 2021-05-17T06:15:58-03:00
thumbnail: https://assets.3dnoticias.com.ar/BARLETTA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Mario Barletta anunció su candidatura a senador nacional
title: Mario Barletta anunció su candidatura a senador nacional
entradilla: El ex intendente de la ciudad de Santa Fe convocó a conformar un frente
  opositor “para ponerle freno al kirchnerismo”.

---
Mario Barletta anunció este domingo su candidatura a senador nacional en las próximas elecciones legislativas.

El ex intendente de la ciudad de Santa Fe (2007-2011) lo comunicó en un hilo de Twitter en el que convocó a conformar un frente opositor “para ponerle freno al kirchnerismo. La situación social, económica e institucional del país lo está reclamando”.

“Nuestra provincia necesita de dirigentes que asuman un mayor protagonismo y defiendan los intereses de las y los santafesinos. Tenemos que sumar. Por eso, no voy a descansar un solo minuto tratando de construir un gran frente que reúna a toda la oposición. Es lo que pensábamos hacer junto a Miguel Lifschitz y otros dirigentes”, manifestó el radical.

“Podemos ser una verdadera alternativa de poder que busca la unidad en la diversidad, respetando la constitución, la división de poderes, los derechos, la igualdad de oportunidades y no acepta la corrupción, la impunidad y los privilegios”, continuó. “Por eso, es clave articular las necesidades de la sociedad en un programa de reconstrucción para proteger a los que sufren, acompañar a los que aspiran a tener un trabajo digno y promover a los que quieren producir y apostar a la educación como la clave del futuro”, agregó.

El ex rector de la Universidad Nacional del Litoral (UNL) concluyó el comunicado reiterando la convocatoria a conformar un frente opositor: “Este es el gran desafío. Por eso convoco sin especulaciones, sectarismos ni divisiones a todos los partidos, especialmente al socialismo, al Pro y a la Coalición Cívica para que, junto a la Unión Cívica Radical, nos unamos para un nuevo tiempo”.