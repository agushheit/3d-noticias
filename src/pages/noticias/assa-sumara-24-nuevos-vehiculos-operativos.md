---
category: La Ciudad
date: 2021-01-11T11:31:07Z
thumbnail: https://assets.3dnoticias.com.ar/2021-unidades-assa.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: '3DNoticias - Fuente: El Litoral'
resumen: Assa sumará 24 nuevos vehículos operativos
title: Assa sumará 24 nuevos vehículos operativos
entradilla: La adquisición de estas unidades demandó una inversión provincial superior
  a los 70 millones de pesos

---
Aguas Santafesinas S.A. (Assa) sumará nuevos vehículos para la atención de los servicios sanitarios, destinados a diferentes áreas operativas de las 15 localidades a cargo de la empresa estatal en forma directa, más los sistemas de grandes acueductos.

**La totalidad de las unidades demandó una inversión provincial superior a los 70 millones de pesos.**

Los 24 nuevos vehículos reemplazarán a otros que serán radiados de servicio e incluyen doce utilitarios multifunción; ocho camionetas _pickup_ doble cabina y doble tracción para atención de los sistemas de acueductos, además de tres camionetas de menor porte también doble cabina para personal técnico y un camión con equipo especial para mantenimiento integral de redes cloacales.

De este modo, «se optimiza la capacidad de respuesta de la empresa para atender la gran diversidad de tareas que exige la prestación de los servicios sanitarios y poder satisfacer los requerimientos de sus usuarios», aseguraron desde la prestataria.

Todos fueron rotulados con la identidad visual de la empresa y el número del nuevo canal de atención de usuarios por WhatsApp: 341-6-950008.

<br/>

# **Camión cloacal para la ciudad**

Entre las unidades adquiridas se cuenta un camión sobre el que se montó un moderno equipo combinado para desobstrucción y aspiración de grandes conductos, que permite reforzar el mantenimiento preventivo y correctivo de las redes cloacales y estaciones elevadoras cargo de la empresa en la ciudad de Santa Fe.

La inversión provincial correspondiente a la unidad rondó los 20 millones de pesos.

Este equipo se suma a otros dos existentes con las mismas prestaciones para otras localidades de la zona Norte y otro en Rafaela, y que junto a otros cuatro de menor porte en la ciudad de Santa Fe posibilitan la atención de los requerimientos de los usuarios respecto al mantenimiento sistemático de las conexiones domiciliarias del servicio cloacal.

El acto de entrega de los vehículos será este lunes 11, a la hora 11 en la sede de la Planta Potabilizadora de Santa Fe, en calle Gob. Candioti entre Lavalle y Sarmiento (allí se interrumpirá el tránsito vehicular ese día entre las 8 y la hora 12, por ese acto formal). Estará encabezado por el secretario de Empresas y Servicios Públicos de la Provincia, Carlos Maina, y el presidente de la empresa, Hugo Morzan.