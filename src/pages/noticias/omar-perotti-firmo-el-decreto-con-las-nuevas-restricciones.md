---
category: Estado Real
date: 2021-05-20T09:15:27-03:00
thumbnail: https://assets.3dnoticias.com.ar/perotti21.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: Omar Perotti firmó el decreto con las nuevas restricciones
title: Omar Perotti firmó el decreto con las nuevas restricciones
entradilla: Las nuevas restricciones ordenadas por el gobernador Omar Perotti se ponen
  en vigencia este jueves.

---
Este miércoles a la noche, el gobierno de la provincia dio a conocer el [decreto](https://www.unosantafe.com.ar/decreto-a50417.html) que detalla como se desarrollará cada actividad contemplada en las nuevas disposiciones sanitarias por la pandemia de coronavirus. El decreto de [**Omar Perotti**](https://www.unosantafe.com.ar/omar-perotti-a22156.html) abarca a la totalidad del territorio provincial. Entra en vigencia desde la cero (0) hora de este jueves 20 de mayo de 2021 y hasta el 31 de mayo de 2021.

Las actividades suspendidas, son las siguientes, según señala el decreto:

    https://www.scribd.com/document/508541466/Nuevas-Restricciones