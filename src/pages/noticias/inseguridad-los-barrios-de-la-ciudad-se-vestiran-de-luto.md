---
category: La Ciudad
date: 2021-08-31T06:15:57-03:00
thumbnail: https://assets.3dnoticias.com.ar/SEGURIDAD.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Inseguridad: los barrios de la ciudad se vestirán de luto'
title: 'Inseguridad: los barrios de la ciudad se vestirán de luto'
entradilla: La Red de Vecinales por Seguridad convocó a los vecinos de la ciudad a
  salir a las calles este miércoles y jueves para visibilizar el reclamo de seguridad.

---
A raíz de los reiterados episodios de inseguridad que se suceden a diario en diversos barrios de la ciudad y con distintas modalidades delictivas, la Red de Vecinales por Seguridad convocó este miércoles y jueves a un reclamo popular en las calles de la ciudad: mostrar a Santa Fe de luto.

"Por nuestras familias, por nuestros amigos, por nuestros vecinos; por una convivencia en paz. Levaremos un listón negro o remera negra: ¡Comprometámonos entre todos!", reza la convocatoria de la Red de Vecinales por Seguridad a los vecinos de la ciudad para salir a las calles.

Susana Spizzamiglio, presidenta de la Asociación Vecinal de Fomento 9 de Julio, adelantó que para el 1 y 2 de septiembre realizarán un “enlutamiento” de la ciudad en el que invitan a poner un listón negro en las puertas de cada vivienda y local.

“Vamos a lanzar esta proclama de dolor y gran incertidumbre por la inseguridad que hay en Santa Fe”, dijo Susana y continuó agregando: “Hace unas semanas hicimos la reunión de la Red de Vecinales por Seguridad donde participaron más de 17 vecinales muy preocupadas por la ola de delitos que azota la ciudad de Santa Fe, la cantidad de homicidios y los episodios predatorios en distintos puntos de la ciudad. No hay barrio que no esté atravesado por esa situación”.

**Santa Fe, de luto**

La semana pasada, en la vecinal Fomento 9 de Julio, se realizó una reunión para determinar la estrategia de trabajo en toda la ciudad para este miércoles 1 y jueves 2 de septiembre. “Esperamos que todos los comerciantes, vecinos, taxistas, remiseros, cualquier persona que se solidarice con las víctimas, se unan al enlutamiento de la ciudad de Santa Fe”, dijo la referente.

“Yo creo que la ciudadanía, para preservar la vida y por la salud psíquica, va a responder a nuestra convocatoria”. Según manifestaron desde la Red de Vecinales por Seguridad, de las 38 entidades barriales que componen la red, "ya existe una gran adhesión" a la iniciativa de vestir de luto a la ciudad de Santa Fe por los hechos de inseguridad.

Cada vecinal llevará adelante la metodología de reclamo de una forma diferente. En algunos barrios, desde este miércoles, ya se verán los árboles de las veredas con listones negros de luto; en otras zonas se colgarán globos negros y muchos vecinos saldrán a las calles con remeras negras. En barrio Barranquitas, por ejemplo, los comerciantes vestirán sus comercios de negro. La vecinal de Alto Verde tendrá a media asta su bandera. En barrio Sur los vecinos se manifestarán el jueves a las 18 frente a Legislatura con carteles y remeras negras. Los delivery circularán ambos días con vestimenta negra para visibilizar la situación.

Susana explicó que además de las acciones de asistencia que realizan las vecinales, como comedores, copas de leche y entrega de bolsones de alimentos, también tienen que canalizar los reclamos por más seguridad en las calles. “Es un eje de trabajo que nos tenemos que imponer porque los mismos vecinos están muy preocupados y acongojados por esta situación de inseguridad, que psicológicamente los golpea mal porque siempre hay un hecho que se repite a las dos cuadras o a la vuelta”, expresó.