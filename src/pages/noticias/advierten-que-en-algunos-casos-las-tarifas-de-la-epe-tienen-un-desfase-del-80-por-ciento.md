---
category: La Ciudad
date: 2021-01-08T09:48:25Z
thumbnail: https://assets.3dnoticias.com.ar/080121-EPE.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Diario UNO'
resumen: Las tarifas de la EPE tienen un desfase de hasta un 80 por ciento
title: Advierten que en algunos casos las tarifas de la EPE tienen un desfase del
  80 por ciento
entradilla: Así lo sostuvo el titular de Empresa Provincial de la Energía, Mauricio
  Caussi, quien además sostuvo que de haber aumentos será con tarifas segmentadas
  y al alcance de los santafesinos

---
Mauricio Caussi, titular de la Empresa Provincial de la Energía, manifestó que evalúan implementar un cuadro tarifario segmentado. El funcionario dijo que el retraso llega hasta un 80 por ciento en algunos casos, debido al desfase por los aumentos de los insumos con los que trabaja a empresa.

«Estamos conformes por cumplir la promesa del gobernador de sostener las tarifas. Lo hicimos durante todo el 2020 en un contexto de mayor exigencia por la pandemia», dijo Caussi, quien además remarcó: «cuando miramos los números, si nos comparamos con Entre Ríos y Córdoba, que son provincias vecinas y comparables, las tarifas como mínimo están un diez por ciento por debajo. Pero en otros casos, como en los consumos de 3.500 o cuatro mil kilovatios para arriba, bimestrales, la diferencia llega hasta un 80 por ciento».

En ese sentido, el funcionario agregó: «Tenemos un atraso tarifario importante. Nuestros insumos y equipamientos se incrementaron el año pasado entre un 50 y 100 por ciento. Estos aumentos se dieron en los componentes importados. La realidad es que estamos cada vez más ajustados. Cada es más difícil sostener esta situación tarifaria. No obstante, la decisión política del gobernador es de alineamiento con la que lleve adelante el gobierno nacional».

«Si llega a haber un ajuste tarifario, de ninguna manera va a ir en contra de las posibilidades de los santafesinos. Se realizará con criterios de diferenciación que nos coloque en mejores términos de eficiencia y equidad», añadió.