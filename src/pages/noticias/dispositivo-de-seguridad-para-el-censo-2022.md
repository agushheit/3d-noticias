---
category: Agenda Ciudadana
date: 2022-05-17T16:06:55-03:00
thumbnail: https://assets.3dnoticias.com.ar/2022-05-17NID_274715O_1.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: Dispositivo de seguridad para el censo 2022
title: Dispositivo de seguridad para el censo 2022
entradilla: Serán 280 efectivos afectados a toda la provincia según lo solicitado
  por el IPEC.

---
El gobierno provincial, a través del Ministerio de Seguridad, diagramó el operativo policial para el Censo Nacional 2020 en función de los requerimientos del IPEC, de los pedidos de jefas y jefes de radio que requirieron presencia policial, en dos formatos:

>> Por un lado, se garantizarán efectivos policiales en las escuelas requeridas, durante todo el día, que son los lugares donde se reciben los cuestionarios y se ordenan para su posterior envío.

>> Y por otro lado, se requirieron binomios de policía caminantes, que patrullan a pie acompañando a los censistas en los recorridos.

Ambos pedidos cambian según cada departamento de la provincia.

En el caso de la ciudad de Rosario, el IPEC pidió custodia en las 97 sedes donde se reúnen los cuestionarios y además la Unidad Regional reforzará con patrullaje a pie de 50 binomios de la PAT, la policía comunitaria y de la Unidad Regional.

Hay 280 policías afectados especialmente al operativo del censo, pero toda la policía de la provincia estará a disposición para que el operativo estadístico más grande del país se desarrolle de manera ordenada. Todos los policías contestaron el censo con anterioridad a fin de asistir a los censistas en la labor.

### 