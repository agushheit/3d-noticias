---
category: Agenda Ciudadana
date: 2021-02-12T08:00:42-03:00
thumbnail: https://assets.3dnoticias.com.ar/senado-2.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: " El Senado arrancó el año picante contra Sain"
title: " El Senado arrancó el año picante contra Sain"
entradilla: Hubo cruces fuertes entre los senadores con la seguridad como telón de
  fondo. Volvieron a reclamar que el Ejecutivo incluya los vetos a dos leyes claves
  en el temario de extraordinarias.

---
El Senado tuvo este jueves la primera sesión extraordinaria del 2021 y casi todos los cañones apuntaron contra el ministro de Seguridad, Marcelo Sain. La disputa no es nueva, pero este año los senadores quieren terminar con lo que entienden es una dilación del Poder Ejecutivo para sostener a ese funcionario en su cargo.

Por ese motivo, los senadores del PJ-NES y del radicalismo –que juntos son mayoría en la Cámara alta– reiteraron el pedido al Ejecutivo para que incluya en extraordinarias los vetos a las leyes de incompatibilidad de funciones y de gastos reservados del Ministerio de Seguridad. La solicitud la habían hecho cuando se inició el período de sesiones extraordinarias en diciembre del año pasado y esos temas no habían sido incluidos por Omar Perotti en el temario.

Son leyes que en el gobierno provincial se entendieron como normas que la Legislatura sacó a fines del año pasado para complicar la situación del ministro de Seguridad de la provincia, Marcelo Sain.

En la primera se establece que los funcionarios de un Poder del Estado no pueden tener un cargo en otro Poder. Todos los legisladores aseguran que no es una ley hecha a medida para sacar a Sain del Ministerio de Seguridad o como director del Organismo de Investigaciones del MPA, cargo en el que pidió licencia para asumir en el Ejecutivo. Pero, por otro lado, aseguran que en ese caso la incompatibilidad es muy clara.

El senador radical por General López, Lisandro Enrico, aseguró que el Ejecutivo está trabando la discusión para ganar tiempo. La legislatura sancionó la ley a fines del año pasado, Perotti la vetó y comenzó el período de sesiones extraordinarias donde sólo se pueden tratar las leyes que el gobernador decide. Recién a partir del 1º de mayo, cuando comienzan las sesiones ordinarias el tema quedará a disposición de los senadores si el gobernador no lo habilita antes.

Sin embargo, Enrico advirtió que, ante un tema de gravedad institucional, como entiende que sucede con el caso de la doble función de Sain, aunque no simultánea, las cámaras de la Legislatura se pueden autoconvocar para tratar el veto del Ejecutivo. "La incompatibilidad de cargos entre el Poder Judicial y el Poder Ejecutivo es para que ningún funcionario judicial haga política. Deben tener imparcialidad. Esun incumplimiento de la ley y una desprolijidad institucional. Sain incumple y Perotti tolera y ampara", aseguró el radical que también cuestionó al fiscal General de la provincia, Jorge Baclini, por sostener con una licencia el cargo que Sain tiene en el MPA.

En la lista de reclamos al Ejecutivo provincial, los legisladores radicales también avanzaron sobre dos temas gruesos: educación y varios temas que involucran a la Empresa Provicnial de la Energía (EPE). Por eso pidieron al presidente provisional del Senado, Rubén Pirola, que convoque a la ministra Adriana Cantero y al presidente del directorio de la EPE, Mauricio Caussi. Ambos funcionarios serán invitados a reuniones de trabajo el 24 de febrero en horario a confirmar.

Los senadores por Castellanos, Alcides Calvo y por Rosario, Marcelo Lewandowski, fueron quienes defendieron la gestión de Perotti. Calvo dando los números de las inversiones en Educación, mientras que su par del sur provincial dijo enumeró una serie de obras que se realizaron en materia educativa y aseguró que está muy avanzado un proyecto consensuado con la diputada socialista Clara García sobre energías renovables.

Luego se metió en el tema seguridad y dijo que Sain debería poder manifestarse ante todos en la Legislatura y que el narcotráfico no comenzó en diciembre del 2019 en la provincia, sino que "se instalaron hace 12 años", en referencia al momento en el que comenzó a gobernar Santa Fe el Frente Progresista. "No podemos dejar que quienes gobernaron durante 12 años y dejaron instalar este flagelo nos vengan a enseñar qué es lo que tenemos que hacer", dijo el rosarino.

Mientras que Calvo, en un tono más conciliador, dijo que no estaba mal que se hable de seguridad en el Senado y destacó que este gobierno provincial tiene más afinidad que los anteriores con la Nación y "eso es un avance", afirmó.

Por su parte, Michlig volvió a cargar contra la política actual de seguridad y mirando a Lewandowski le dijo que en 2020 los homicidios subieron un 25 por ciento respecto al año anterior. "Es lamentable el circo que está montando Sain (quien decidió no ir a una reunión con la Comisión de Seguridad de Diputados porque la prensa no podía estar presente) cuando podría contar todo en una conferencia de prensa. Nos gustaría que el lema paz y orden se haga realidad", agregó y le recordó que mientras el Frente gobernaba la provincia el kirchnerismo fue gobierno nacional durante ocho años y que el narcotráfico es un delito federal.

Varios de los senadores presentes se refirieron al tema para referenciar que los problemas en seguridad no se circunscriben a las grandes ciudades sino a todo el territorio provincial. Pero el cierre lo dio Traferri que recordó que el año pasado hubo 1.534 personas heridas en hechos policiales y 375 homicidios. "Seguimos tuiteando, armando causas (en referencia a la investigación por juego clandestino por la que se le intentó quitar los fueros) y no sabemos cuál es el plan de seguridad para la provincia", cuestionó y recordó que Sain trabaja en cuestiones de seguridad en la provincia desde 2016.

Los cruces en el Senado con la seguridad como tema principal se daban al mismo tiempo que los diputados que integran la Comisión de Seguridad de la Cámara baja anunciaban en conferencia de prensa que iban a insistir con la convocatoria al ministro Sain que este jueves por la mañana anunció que no iba a concurrir a la reunión de este viernes porque no era un encuentro abierto que pudieran presenciar los periodistas. Hoy la discusión política de chicanas volvió a dejar como telón de fondo al debate sobre cómo mejorar un tema acuciante para los santafesinos como el de la inseguridad.