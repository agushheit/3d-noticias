---
category: La Ciudad
date: 2021-04-14T07:30:03-03:00
thumbnail: https://assets.3dnoticias.com.ar/guadalupe.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad de Santa Fe
resumen: Con protocolos y restricciones, se organiza la festividad de la Virgen de
  Guadalupe
title: Con protocolos y restricciones, se organiza la festividad de la Virgen de Guadalupe
entradilla: El próximo 17 y 18 de abril se realizará la tradicional celebración religiosa,
  sin peregrinaciones ni ferias. Las misas serán el sábado y domingo, en el templo
  habilitado para unas 200 personas.

---
Como es tradicional, después de la Semana Santa, se realizará en Santa Fe la Fiesta de la Virgen de Guadalupe y esta edición será el próximo sábado 17 y domingo 18. A diferencia del año pasado, en donde todo debió quedar suspendido por la pandemia por Covid-19, este año se llevarán a cabo algunas actividades, ajustadas a protocolos. Si bien no se realizará ninguna peregrinación, el templo estará abierto el fin de semana para las misas.

Este martes, el secretario de Educación y Cultura de la Municipalidad, Paulo Ricci, y el párroco de la Basílica, Olidio Panigo, dieron detalles de cómo se llevará a cabo esta celebración. Es de destacar que también estarán suspendidas las ferias que habitualmente había en esta fiesta.

El templo abrirá sus puertas, tanto el sábado como el domingo, a las 7, y cerrará cuando finalice la última misa prevista para las 21. Las celebraciones serán a las 7.30, 9, 10.30, 12, 15, 16.30, 18, 19.30 y 21. La capacidad habilitada será de unas 200 personas aproximadamente, con estrictos protocolos (no habrá misas afuera ni cortes de calles).

El camarín de la Virgen no estará accesible al público; y se colocará una imagen en la cara Oeste de la Basílica, mirando hacia el exterior, para que se la pueda apreciar desde la plaza del Folclore, sin necesidad de ingresar. Por otra parte, la única peregrinación prevista es la de los taxistas que se hará en los vehículos, desde la calle 1° de Mayo al 2600. Se podrán sumar los santafesinos en sus vehículos particulares. Será el domingo 18 a las 16 horas. Los autos pasarán frente a la Basílica pero no podrán parar ni descender personas de los vehículos.

**Trabajo coordinado**

“Este año también va  ser diferente”, dijo -por su parte- Ricci, quien destacó la coordinación con el padre Panigo y con el equipo de la comisión organizadora para poder realizar esta celebración con los protocolos necesarios. “Decidimos que habrá muchas menos actividades que las habituales en la fiesta de Guadalupe. No habrá peregrinaciones convocadas por instituciones, tampoco ferias ni ventas ambulantes en el entorno. Vamos a concentrar la actividad solo a las misas”, comentó.

Desde la Municipalidad trabajan de manera coordinada las distintas áreas como Control, Economía Social e Integración “para colaborar en el ordenamiento de las personas que se acerquen a participar de las misas”, dijo el funcionario. Y aprovechó para invitar a los feligreses a participar de manera remota a través de las redes sociales por donde se van a transmitir todas las misas, sobre todo, para que las personas de riesgo y los adultos mayores eviten acercarse al templo.

De todas maneras, Ricci anticipó: “Habrá un operativo importante en el entorno para organizar la llegada de la gente, y los ingresos a las misas. También en el ala Oeste de la Basílica, frente a la plaza del Folklore habrá una imagen de la Virgen para quienes quieran verla no tengan que entrar a la nave central y no se genere tanta concentración y circulación de gente en el espacio cerrado”.

Y luego recordó que “las misas van a funcionar con los protocolos habituales, con distanciamiento y burbujas sociales que se acomodarán dentro de la nave”. Hay 130 bancos y la metodología será por burbujas. “Si hay una sola persona se sienta en un solo banco y si vienen en familia dos por banco, no más de eso para así evitar la concentración de gente”, detalló.

**Cumplir con los protocolos**

En referencia a las misas, el párroco Panigo recordó que “serán cada 90 minutos, cada una dura casi una hora, y de esta manera hay un poco más de media hora para liberar la Basílica y que puedan ingresar los próximos”. Y en esta línea, dijo: “Se van a seguir los protocolos que están establecidos, pero les pedimos a las personas que se acerquen, que lo hagan con responsabilidad para cuidarnos entre todos”.

Más adelante, manifestó: “Vamos asegurar que a la Basílica ingrese solo la cantidad de gente que está permitido para tener una buena aireación. El templo tiene puertas y ventanas en la parte inferior, que se pueden abrir por lo que habrá circulación de aire, también habrá distanciamiento social, pero les pedimos acercarse con el barbijo. A cada persona se le colocará alcohol en las manos como para tener los recaudos necesarios. Lo primero que tenemos que cuidar es la salud”.

La participación de las misas será por orden de llegada. Una vez que se cubra la capacidad de la Basílica, ya no van a poder ingresar más personas y van a tener que esperar a la próxima misa. De todas maneras, “habrá una imagen para que puedan saludarla porque lo que queremos es evitar todo tipo de concentración no solo en el interior sino también en el exterior, por eso se suspenden las peregrinaciones, solo está prevista la de los taxistas”, aclaró Panigo.

**Donaciones**

En cuanto a las donaciones, Olidio Panigo anticipó que “Cáritas no estará presente para evitar esa concentración de gente, pero aquellos que quieran hacer donaciones, les pedimos que las acerquen a las Cáritas parroquiales para que sea también un día de solidaridad”.

El sacerdote también aprovechó para pedirle a “aquellas personas que son de riesgo, a los adultos mayores y la gente del interior, que sigan la transmisión en vivo a través de la página oficial de facebook de la Basílica”; y aclaró que “no habrá una misa central sino que todas serán iguales” por lo tanto la sugerencia es evitar las concentraciones “para cuidarnos entre todos”.