---
category: La Ciudad
date: 2021-04-06T07:31:52-03:00
thumbnail: https://assets.3dnoticias.com.ar/suarez.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Carlos Suarez
resumen: "“El Municipio debe resolver el descontrol en la Costanera”"
title: "“El Municipio debe resolver el descontrol en la Costanera”"
entradilla: 'De esta manera se refirió el concejal de UCR-Juntos por el Cambio, Carlos
  Suárez, con respecto a la denuncia de los vecinos de la zona por ruidos molestos
  y picadas de motos y autos. '

---
“Nos preocupa porque estas situaciones se dan además en horarios en que no está permitido circular ni desarrollar ninguna actividad, salvo que sea esencial; y en todos los casos se pone en riesgos a vidas”, indicó.

El concejal de UCR-Juntos por el Cambio Carlos Suárez, solicitó a través de un proyecto de comunicación que el Municipio resuelva el descontrol que denuncian los vecinos en la Costanera. “Hemos recibido muchos mensajes de quienes viven en la zona, quienes también denunciaron en los medios de comunicación, acerca de ruidos molestos por actividades que se desarrollan fuera del horario permitido. También los vecinos nos alertaron de que las avenidas Almirante Brown y Siete jefes se convirtieron en 'pistas oficiales' para correr picadas de autos y motos, actividad que había disminuido por los controles que se realizaban frecuentemente”, afirmó. 

El legislador solicitó saber además si la Municipalidad realizó mediciones de decibeles en la zona, o se labraron actas de infracción por este motivo. También preguntó si se realizan controles de vehículos en las Costaneras, con qué frecuencia y la cantidad de actas labradas y retenciones en este marco. 

“Es importante que podamos conocer, además, qué medidas preventivas se llevan adelante para evitar la repetición de estos hechos que en todos los casos, sea por la pandemia o por el manejo peligroso de vehículos, ponen en riesgo la vida de las personas”, afirmó Carlos Suárez. En la misma línea, el concejal de UCR-Juntos por el Cambio agregó que es necesario saber si el municipio coordina operativos de control con la Agencia de Seguridad Vial de la provincia, la Policía y el Ministerio Público de la Acusación. 

Carlos Suárez recordó que días atrás acompañaron la solicitud de que la secretaria de Control y Convivencia Ciudadana, Virginia Coudannes, asista al Concejo a detallar las  acciones que llevan adelante. “Es fundamental para la tarea que desarrollamos como legisladores conocer en detalle, y de primera mano, el trabajo que se realiza; pero también para trasladarles las inquietudes y denuncias de los vecinos y tratar de lograr respuestas concretas para ellos”, agregó.