---
category: Agenda Ciudadana
date: 2021-12-27T06:15:33-03:00
thumbnail: https://assets.3dnoticias.com.ar/leytributaria.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Se vienen días clave para saber si la provincia tendrá presupuesto y ley
  tributaria
title: Se vienen días clave para saber si la provincia tendrá presupuesto y ley tributaria
entradilla: 'La ley de leyes fue aprobada por el senado y aún no ingresó a Diputados.
  Los legisladores del Frente aseguran que sólo se aprobará si se hacen modificaciones

'

---
El Senado aprobó el presupuesto y la ley tributaria en las últimas horas del jueves 16 de diciembre. Como Diputados no sesionó desde esa fecha, el mensaje aún no ingresó formalmente a la Cámara baja y sólo quedan días para que termine el año. Los legisladores del Frente Progresista aseguran que si no se introducen algunas modificaciones a lo sancionado por el Senado, no habrá luz verde en la Cámara baja para la ley de leyes.

A pesar de esa situación, para este martes está convocada la reunión de la Comisión de Presupuesto de la Cámara de Diputados. El encuentro es a las 10. La intención de los diputados es empezar a trabajar en la iniciativa a pesar de que el proyecto aún no ingresó.

El martes pasado, luego de la visita del ministro de Economía de la provincia, Walter Agosto, a la comisión de Presupuesto de Diputados Clara García (Partido Socialista) aseguró que "hay vocación" para que la provincia tenga presupuesto para 2022, pero también señaló que Agosto se fue con muchas preguntas por responder y hasta el viernes pasado no había novedades.

Eso abre dos escenarios posibles: uno es sesionar el martes 28 de diciembre para darle ingreso al mensaje que llega desde el Senado y girarlo a comisiones para tratarlo en el recinto al día siguiente. La otra opción es que se trabaje para llegar a un gran acuerdo político –que debería incluir a los senadores– que permita directamente tratarlo sobre tablas el miércoles 29. Si no hay acuerdo, la sesión del 29 serviría para ingresar el proyecto, enviarlo a comisiones y tratarlo en febrero, porque en enero habrá receso legislativo.

La intención de varios diputados es reunirse el martes y elevar al ministro de Economía de la provincia, Walter Agosto, por escrito todas las requisitorias de información que se le hicieron el martes pasado y que aún no recibieron respuesta. "El ánimo es aprobar el presupuesto, pero desde el martes hubo cero información", se quejó un diputado que pidió reserva y aseguró que lo que no va a suceder es que se apruebe el mismo mensaje que salió del Senado. "Va a haber modificaciones", aseguró.

**Los puntos a discutir**

Para 2022 la provincia prevé recursos por 851.018.185.000 pesos, con un superávit de 572.790.000 pesos, con una inflación del 33 por ciento y un tipo de cambio de 131,10 pesos. Pero a pesar de esas cifras hay diputados que advierten sobre la delicada situación de los municipios y comunas y exponen los problemas que tuvieron para que la provincia les transfiera recursos en los últimos años.

"Hay gobiernos locales que están esperando la transferencia del Fondo de Obras Menores porque ya tienen obras licitadas y hay otros que lo esperan para pagar aguinaldo", dijo una legisladora que recordó que todos los años la Legislatura aprueba una ley para habilitar al uso del 50 por ciento de es fondo para utilizarlo en gastos corrientes.

La situación de Santa Fe y Rosario es similar, también, a la de los últimos años. Desde hace más de un lustro quienes fueron intendentes de ambas ciudades pelearon para que sean incluidas en el Fondo de Obras Menores (FOM). Pero los senadores se vienen negando sistemáticamente a esa petición.

El FOM fue creado por ley para compensar a los municipios y comunas que no percibían el fondo del conurbano, que nació en 1995, y que sí perciben las dos ciudades más grandes de la provincia. Pero ese fondo nunca fue actualizado y luego de la devaluación quedó fijo en 30 millones de pesos. Mientras que el FOM se actualiza todos los años porque "se constituirá anualmente con una cantidad equivalente al uno por ciento (1%) del Cálculo de Recursos para la Administración Central en el Presupuesto General de Gastos y Cálculo de Recursos inicial correspondiente al año anterior", reza la ley 12.385.

Este domingo el intendente de Rosario, Pablo Javkin, salió públicamente a ponerle cifras a esa inequidad. "Hoy la foto del presupuesto con media sanción es: 4.080 millones para el resto y 30 millones para Rosario y Santa Fe", dijo a Rosario3.

Otro punto a resolver son las partidas para el convenio que tiene la ciudad de Rosario para atender su sistema sanitario municipal. Eso, al menos a priori, se quedó fuera del presupuesto. No hay fondos específicos para atender esa erogación en un año donde todavía se padece la pandemia del Covid y los senadores sólo incorporaron un artículo donde se prevé que parte del excedente de recaudación vaya a cubrir esa situación que es clave para la ciudad del sur provincial.

Hoy Santa Fe y Rosario están dentro del Plan Incluir. Sin embargo, algunos diputados y varios senadores se quejaron de la distribución discrecional de los fondos de ese programa que alcanza a todos los pueblos y ciudades de la provincia. Pero, además, señalaron que casi no se actualizaron los montos para el año que viene respecto del ejercicio 2021 cuando hubo una inflación que rondó el 50 por ciento. Hoy esa partida debería llegar a los 7.000 millones de pesos y no alcanza los 5.000 millones de pesos.

Otro de los señalamientos fueron el tope que le pusieron a las chances de los municipios de salir a buscar endeudamiento. Eso es un condicionamiento porque los municipios necesitan el aval de la provincia, primero, y de la Nación después para salir a buscar crédito.

El otro gran problema que vienen exponiendo intendentes y presidentes comunales tiene que ver con los límites (de un 30 por ciento promedio) que la provincia le está poniendo a un tributo como el de la Patente, que se coparticipa en un 90 por ciento a municipios y comunas.

En política nada es casualidad. El gobernador decidió que el proyecto de presupuesto ingrese por el Senado, que luego de introducir algunas modificaciones –entre las que se garantiza una partida de 599 millones para el Fondo de Fortalecimiento Institucional, que utilizan los 19 senadores, y 131 millones para los 50 Diputados– lo aprobó por unanimidad. Al ser la Cámara de ingreso del proyecto, es la que tiene la última palabra, salvo que Diputados no trate el proyecto.