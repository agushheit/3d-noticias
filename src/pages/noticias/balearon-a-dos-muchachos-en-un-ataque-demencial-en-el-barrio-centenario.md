---
category: La Ciudad
date: 2020-12-16T11:20:16Z
thumbnail: https://assets.3dnoticias.com.ar/baleados-barrio-centenario.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Diario UNO'
resumen: Balearon a dos muchachos en el barrio Centenario
title: Balearon a dos muchachos en el barrio Centenario
entradilla: Sucedió a las 15:50 en Pérez y O'Higgins. Los dos baleados fueron Alexis
  Joel Sureda de 20 años y Joaquín Pasculli de la misma edad. Secuestraron vainas
  servidas de pistola calibre 9 mm.

---
Esta tarde de martes, alrededor de las 16 en inmediaciones de la esquina que forman las calles Pérez y O'Higgins del barrio Centenario, se produjo un cruento ataque a balazos contra dos muchachos que resultaron baleados y fueron llevados de urgencia al hospital Cullen.

**Ambos salvaron providencialmente sus vidas**. La Policía hizo un operativo en todo el sector, y fueron secuestradas las imágenes de las cámaras de videovigilancia para identificar a los agresores.

# **Denuncia**

Los vecinos de la zona fueron los que denunciaron los sucesos y al lugar llegaron oficiales y suboficiales de la Subcomisaría 1° del barrio Centenario, que fueron los responsables de secuestrar **11 vainas servidas de pistola calibre 9 milímetros**. No hubo detenidos, pero dialogaron con testigos y esa información fue girada a los operadores de las cámaras de videovigilancia existentes en la zona, para constatar la identidad de los atacantes. No hubo detenidos relacionados con el ataque criminal.

# **Baleados**

En tanto, los dos muchachos baleados, fueron atendidos por los médicos de la Emergentología del hospital Cullen. Se trata de Joaquín Pasculli de 20 años, a quien constataron los profesionales que presentaba cinco balazos en la zona torácica.

El otro herido, Alexis Joel Sureda de 20 años también, presentaba quebradura de tibia en la pierna de derecha por impacto de bala. Ambos recibieron curaciones y fueron derivados a la sala 6° de Hombres para seguir con su atención.

# **Investigación**

informaron la novedad sobre la ocurrencia del violento suceso a la Jefatura de la Unidad Regional I La Capital de la Policía de Santa Fe, y estos hicieron lo propio con el fiscal en turno del MPA que ordenó profundizar la investigación sobre el ataque criminal, y el secuestro de imágenes de las cámaras de videovigilancia de la zona.