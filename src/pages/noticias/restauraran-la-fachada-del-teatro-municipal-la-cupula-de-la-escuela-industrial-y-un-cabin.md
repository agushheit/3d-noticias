---
category: La Ciudad
date: 2021-10-07T06:15:49-03:00
thumbnail: https://assets.3dnoticias.com.ar/teatro.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Restaurarán la fachada del teatro Municipal, la cúpula de la Escuela Industrial
  y un cabín
title: Restaurarán la fachada del teatro Municipal, la cúpula de la Escuela Industrial
  y un cabín
entradilla: 'El municipio firmó un acuerdo de cooperación con la ciudad francesa para
  la puesta en valor del patrimonio arquitectónico de inspiración francesa que se
  encuentra en Santa Fe. '

---
Este miércoles, el intendente Emilio Jatón y la alcaldesa de la ciudad de Poitiers (Francia), Léonore Moncond'huy, firmaron un acta acuerdo de cooperación para la puesta en valor de patrimonio de inspiración francesa que existe en la capital de la provincia. El acto se desarrolló en el Teatro Municipal "1° de Mayo", en comunicación virtual y simultánea con las autoridades de Poitiers.

 Además de Jatón, en la mesa estuvieron la embajadora de Francia en la Argentina, Claudia Scherer-Effosse -quien visitó la ciudad por el evento-; el rector de la Universidad Nacional del Litoral, Enrique Mammarella; el presidente de la Cámara Argentina de la Construcción delegación Santa Fe, Sergio Winkelmann; y el presidente del Colegio de Arquitectos de la Provincia, Distrito 1, Julio Cavallo.

 Tras la firma, el intendente Emilio Jatón aseguró que "fue un arduo trabajo que hicimos durante mucho tiempo con la embajada de Francia en la Argentina, con la ciudad hermana de Poitiers y esto habla a las claras de que se puede trabajar internacionalmente y que Santa Fe está en un lugar de preponderancia".

 Luego dio detalles sobre el acuerdo: "Se firmó un convenio para trabajar en algunos lugares con historia francesa en la ciudad, que implica la restauración, pero también la formación de profesionales para que trabajen y conozcan cómo restaurar nuestros bienes patrimoniales".

 Por otro lado, Jatón puntualizó que resta elaborar un cronograma de trabajo y que "los artesanos franceses lleguen a Santa Fe y empiecen la capacitación. Hay que dejar claro que son tres puntos específicos: un cabín ferroviario, la cúpula de la Escuela Industrial y el frontis del Teatro Municipal; sobre esos tres lugares se va a trabajar con los artesanos franceses".

 Con respecto a quienes van a acceder a esas capacitaciones, el mandatario indicó que "del conjunto de socios con los que cuenta este proyecto saldrán las personas que se capacitarán con los profesionales franceses".

 **Intercambio**

 En simultáneo, en la ciudad de Poitiers se desarrolló un evento similar, que contó con la participación de autoridades francesas. De este modo, ambas ceremonias se concretaron paralelamente y los participantes se comunicaron por videoconferencia.

 En este sentido, la alcaldesa Moncond'huy, comenzó su alocución de manera online y aseguró: "Con este convenio se realiza el lanzamiento de nuestro proyecto sobre la protección y valorización del patrimonio histórico y cultural argentino de inspiración francesa. Esto muestra la relación reciente y duradera entre las ciudades de Poitiers y Santa Fe".

 En esta línea, recordó que los primeros intercambios entre esta ciudad francesa y Santa Fe se hicieron en 2012 y el primer proyecto comenzó en 2016. "Lo que me gusta es que valoriza una historia compartida desde hace mucho más tiempo entre nuestros países. Está anclado en una historia hecha por migraciones, de intercambios arquitectónicos, culturales y políticos; y me alegro que nos pongamos en esta larga historia y la hagamos revivir".

 Jatón agradeció a todo el equipo y colaboradores por permitir "el éxito de nuestro proyecto", y destacó a los aprendices y profesionales franceses que "permiten este intercambio entre Poitiers y Santa Fe".

 También mencionó a la Agencia Francesa de Desarrollo por el financiamiento y sostén del montaje de este proyecto y a otras asociaciones y entidades que apoyan la propuesta y además a la embajada que siempre estuvo a disposición.

 **Para replicar**

 Con respecto a la importancia del convenio para ambas ciudades, la embajadora de Francia en la Argentina, indicó que la iniciativa "significa para nosotros construir un porvenir para proteger la herencia común que tenemos. En este proyecto hay muchos aspectos, hay muchos socios, escuelas y asociaciones. Además de haber ejes de cooperación técnicos, también apuntalando a la juventud, con la idea de intercambiar información y sensibilización con respecto al medio ambiente. Es un proyecto muy complejo y ejemplar".

 "Voy a hacer hincapié en lo que se realiza en Santa Fe para replicar en el resto de la Argentina, porque esta estructura de centro de capacitación y aprendizaje me parece muy interesante para desarrollar el empleo en este país, porque son formaciones técnicas que me parecen muy interesantes para replicar. Esto es algo innovador", destacó Scherer-Effosse.

 **El proyecto**

 Enmarcado en una línea de financiamiento de la Agencia Francesa de Desarrollo (AFD), insta a las colectividades francesas, en este caso Grand Poitiers, a buscar socios en países en desarrollo, para llevar adelante sus proyectos. De este modo, el plan consiste en la puesta en valor del patrimonio arquitectónico con reminiscencia francesa que hay en la ciudad, para lo cual se prevé la realización de capacitaciones.

 En esas instancias, profesionales y artesanos de Poitiers instruirán de manera virtual y presencial, a santafesinos y santafesinas en tres técnicas de restauración: cubierta, yeso/estuco y carpintería. En cuanto a las formaciones presenciales, consistirán en cuatro misiones de los especialistas a Santa Fe, donde capacitarán durante dos semanas a 18 alumnos seleccionados por la Secretaría de Desarrollo Urbano del municipio y tres entidades que se considerarán socias en el plan: la Cámara Argentina de la Construcción, el Colegio de Arquitectos de la provincia y la Universidad Nacional del Litoral.

 La iniciativa también prevé la financiación de la compra de materiales, herramientas y equipamientos para llevar adelante las capacitaciones en edificios de la ciudad como el Teatro Municipal, los cabines del ferrocarril y las mansardas de la Escuela Industrial Superior.

 El objetivo principal del programa es difundir la cooperación entre ambas comunidades, para lo cual se producirá una exposición fotográfica itinerante. Además, la intención es contribuir al desarrollo económico de la restauración patrimonial reforzando las capacidades de los profesionales. Del mismo modo, se busca implicar al conjunto de actores en un proyecto de cooperación y valorizar a escala internacional la experticia en términos de restauración y puesta en valor del patrimonio.

 **SOCIOS LOCALES**

Tanto en Santa Fe como en Grand Poitiers, el proyecto cuenta con socios locales. En ese sentido, la Municipalidad cuenta con el Colegio de Arquitectos de la provincia, Distrito 1, quién tendrá a su cargo la promoción del dispositivo ante los profesionales de la construcción, participa en la selección de las personas a formar y sensibiliza sobre la importancia de la protección del patrimonio.

Otra asociada será la Cámara Argentina de la Construcción, delegación de Santa Fe, que definirá las necesidades de formación de las empresas y coordinará nuevas relaciones con compañías locales que desean desarrollar sus competencias en materia de patrimonio e integración. Y, por último, la Universidad Nacional del Litoral, movilizando a actores como la Escuela Industrial Superior, la Facultad de Arquitectura, Diseño y Urbanismo y el laboratorio LatMat.