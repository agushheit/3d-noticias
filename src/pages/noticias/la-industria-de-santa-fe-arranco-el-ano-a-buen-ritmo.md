---
category: Agenda Ciudadana
date: 2021-03-06T07:00:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/INDUSTRIA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La industria de Santa Fe arrancó el año a buen ritmo
title: La industria de Santa Fe arrancó el año a buen ritmo
entradilla: Respecto a enero del año pasado la actividad creció un 19 por ciento.
  Ayudó a la recuperación la inactividad de diciembre en el polo aceitero por los
  paros.

---
La actividad industrial de la provincia de Santa Fe arrancó el año con un aumento del 19.1% en comparación con igual mes del año pasado, según informó este viernes la Federación de Industriales de Santa Fe (Fisfe). La suba registrada en enero se explica en parte por la reanudación de las actividades fabriles en el complejo oleaginoso que en diciembre estuvo parada por paros gremiales y por la mayor producción alcanzada en varios e importantes sectores, entre ellos: fiambres y embutidos, productos lácteos, madera y subproductos, papel y productos de papel, pinturas, detergentes y jabones, manufacturas de plástico, minerales no metálico, siderúrgica, fundición de metales, productos de metal y servicios de trabajo, maquinaria agropecuaria y de uso general, aparatos de uso doméstico, remolques, autopartes y motocicletas.

“De manera equivalente a lo manifestado en los anteriores dos meses, en enero de 2021 el 58% de las ramas industriales en Santa Fe presentó en términos productivos una dinámica interanual positiva, consolidando de esta manera la actual fase de recuperación”, señala Fisfe.

**Las ramas de la industria que crecieron en enero contra el mismo mes del año pasado son:**

Maquinaria agropecuaria: +107,2%

Siderúrgica: +87,4%

Remolques: +79,5%

Molienda de oleaginosas: +45,4%

Manufacturas de plástico: +16,1%

Productos de metal y servicios de trabajo metales: +12,4%

Autopartes +7,0%

Maquinaria de uso general: +5,1%

Productos lácteos: +5%

Fiambres y embutidos: +4,0%

Papel y productos de papel: +2,2%.

**Por el contrario, los sectores que cayeron en enero fueron:**

Vehículos automotores: -29,4%

Muebles y colchones: -24,9%

Productos metálicos para uso estructural: -24,0%

Faena carne vacuna: -16,7%

Maquinaria de uso especial: -13,6%

Prendas de vestir: -3,0%

Edición e impresión: -1,2%

Más allá de los números, algunos puntos que resalta el informe:

1) Impulsada por la mayor demanda del agro y la construcción, la producción de acero en la provincia de Santa Fe registró en enero de 2021 una importante mejora de 87,5% interanual. De igual manera, en productos laminados el nivel el nivel de actividad se posicionó muy por encima en relación al mismo mes del año pasado.

2) Por sexto mes consecutivo, en enero de 2021, el complejo metalúrgico en la provincia de Santa Fe registró mayor nivel de actividad en relación a igual período del año anterior. Por ejemplo, en el sector metalúrgico en enero de 2021 se destacaron los siguientes resultados interanuales: fundición (+7,6%), productos de metal y servicios de trabajo (+12,4%9, maquinaria de uso general: (+5,1%), maquinaria agropecuaria (+107,2%), otra maquinaria de uso especial (-13,6%), aparatos de uso doméstico (+27,4%), equipos y aparatos eléctricos: (-5,1%), remolques y semirremolques: (+79,5%), autopartes:(+7,0%) y motocicletas(+185,5%) Dos terceras partes de los sectores de este rubro presentó resultados positivos situación que refleja la paulatina recuperación de la industria respecto a lo verificado entre marzo y agosto cuando la mayor parte mostraba bajas de producción.

3) El consumo total de cemento presentó en Santa Fe, en enero de 2021, y por quinto mes consecutivo, una nueva e importante mejora de 29,6% interanual. Además, en Rosario, Rafaela, San Lorenzo y Santo Tomé la superficie a construir autorizada por los permisos de edificación otorgados para la ejecución de obras privadas registró a comienzos de 2021 importantes subas, permitiendo anticipar el mayor nivel de actividad de la construcción.

4) En enero de 2021 el nivel de producción de la industria automotriz en Santa Fe presentó una baja de 29,4% interanual.

5) El nivel de actividad de otros productos de caucho muestra desde agosto de 2020 una moderación de sus retrocesos, mientras que en manufacturas de plástico alcanza resultados positivos en las últimas seis mediciones mensuales.

6) La faena bovina en la provincia de Santa Fe presentó en enero y febrero de 2021 sendas caídas de 16,7% y 1,2% interanual respectivamente. De esta manera, en el primer bimestre del año la actividad sectorial presentó una contracción de 12,3% en relación al igual período del año pasado. El valor de las exportaciones santafesinas de carne bovina congelada presentó al finalizar 2020 una baja de 25,9% (555 millones de dólares) y de 14,7% en volumen (131,8 mil toneladas) en relación al año 2019. Las ventas externas de carne refrigerada mostraron en el período enero-diciembre de 2020una contracción de 10,3% en valor (160 millones de dólares) y un volumen levemente inferior al año anterior (20,4 mil toneladas).