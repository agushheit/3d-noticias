---
category: Agenda Ciudadana
date: 2021-05-07T08:27:17-03:00
thumbnail: https://assets.3dnoticias.com.ar/xime2.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Ximena García
resumen: Por frecuentes hecho de inseguridad, reclaman luces y mantenimiento en la
  circunvalación de Santa Fe
title: Por frecuentes hecho de inseguridad, reclaman luces y mantenimiento en la circunvalación
  de Santa Fe
entradilla: 'La Diputada Nacional Ximena García presentó una iniciativa para que se
  realicen tareas de instalación y mantenimiento del alumbrado público, y obras de  señalización
  en la Circunvalación de la ciudad de Santa Fe. '

---
El proyecto se hace eco del reclamo constante de los vecinos de la ciudad capital, quienes destacan el alto riesgo de sufrir accidentes viales o ser víctimas de hechos delictivos producto del estado en que se encuentra la infraestructura vial, y de la inexistencia de luminarias en numerosos sectores de este corredor.

“Pastizales altos, exceso de residuos, guardarrails en mal estado, luminarias descompuestas y hasta tramos en los que no existe ningún tipo de iluminación, conforman el paisaje cotidiano que muestra la desidia y el abandono que caracteriza este importante corredor vial” señaló la legisladora, y puntualizó que “diariamente se informa a las autoridades policiales sobre hechos de  vandalismo, obstrucciones voluntarias en dicha vía,  y daños en los vehículos que circulan, a partir de proyectiles arrojados por grupos de personas que aguardan a que los autos se detengan para robar a sus conductores”.

Cabe destacar que un tramo importante de la Circunvalación pasó a manos de Vialidad Nacional quien es responsable de su mantenimiento en coordinación con las autoridades de la Municipalidad de Santa Fe, que continúan sin dar respuestas a la ciudadanía.

![](https://assets.3dnoticias.com.ar/xime.jpeg)

![](https://assets.3dnoticias.com.ar/xime1.jpeg)