---
category: Estado Real
date: 2021-03-03T06:55:30-03:00
thumbnail: https://assets.3dnoticias.com.ar/epe.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: La Empresa Provincial de la Energía brindó un informe a la Cámara de la Construcción
title: La Empresa Provincial de la Energía brindó un informe a la Cámara de la Construcción
entradilla: El titular de la EPE, Mauricio Caussi, recibió a autoridades de la CAC
  Santa Fe.

---
El presidente de la Empresa Provincial de la Energía, Mauricio Caussi, recibió a los representantes de la Cámara Argentina de la Construcción, Delegación Santa Fe: Sergio Winkelmann, presidente, además de Roberto Pilatti, Adrián Dip y Fabio Arredondo.

En ese marco, Caussi brindó un detallado informe de la situación en la que recibieron la empresa y su situación financiera. A su vez mencionó dos proyectos que se vienen llevando adelante: por un lado, el programa EPE Digital, que tiene como objetivo principal contar con una empresa más dinámica, eficiente y cercana a sus usuarios, a partir de la incorporación estratégica de nuevas tecnologías. Asimismo, tiene el fin de monitorear procesos e información en línea, reduciendo gestiones manuales y redistribuyendo el capital humano en trabajos que generen mayor valor. Al respecto Caussi, comentó: “Queremos aprovechar las tecnologías de la información y comunicación para beneficio no solo de nuestra empresa, sino también de nuestros usuarios, al mismo tiempo de trabajar contra el robo de energía". 

En el encuentro, también se trató la creación del programa EPE Social, el cual tiene como objetivo principal abordar de forma integral y coordinada las problemáticas sociales de los municipios y comunas de la provincia de Santa Fe. En relación a ello, el titular de la EPE manifestó que “se está haciendo una gran inversión en barrios populares que necesitan una provisión segura, con la infraestructura adecuada e incorporando a santafesinas y santafesinos al sistema comercial de la empresa, con tarifa social”. 

Luego, Caussi informó “que se está trabajando un proyecto para modificar el status jurídico de la empresa, que permita tener una gestión más eficaz y eficiente. Desde la cámara se plantearon temas referidos a los procesos licitatorios, listado de obras a ejecutar y de gestión de las redeterminaciones de precios”.

Caussi solicitó un detalle de los temas que desde la delegación consideran se deben mejorar. Para finalizar, Sergio Winkelmann agradeció a los funcionarios la atención recibida.