---
category: La Ciudad
date: 2021-10-28T06:00:58-03:00
thumbnail: https://assets.3dnoticias.com.ar/BASQUTBIS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: El mejor básquet del país vuelve a elegir a Santa Fe capital como sede
title: El mejor básquet del país vuelve a elegir a Santa Fe capital como sede
entradilla: 'Esta mañana se presentó oficialmente un nuevo torneo de la Liga Argentina
  de Básquet y del cual el club Atlético Colón es sede. Allí se disputarán desde hoy
  y hasta el domingo tres partidos por días. '

---
El gimnasio Roque Otrino del Club Atlético Colón de Santa Fe será sede del grupo Conferencia Norte de la Liga Argentina de Básquet. Esta mañana, en la Sala 2 de la Estación Belgrano, se dieron detalles del evento deportivo que hoy se pone en marcha y es la primera etapa de la competencia oficial de la temporada 2021-2022.

Las jornadas de encuentros son desde hoy -miércoles- y hasta el domingo, en tres horarios porque son tres partidos por día. Del grupo participan, además del equipo del club local, Balsuar Central Argentino Olímpico de Ceres y Club Deportivo Libertad de Sunchales de la provincia de Santa Fe, Independiente de Santiago del Estero, Villa San Martín de Resistencia (Chaco), Echagüe de Paraná y San Isidro de San Francisco.

De la conferencia participaron, el secretario General de la Municipalidad, Mariano Granato; Carlos Fertonani, miembro de la Subcomisión de Básquet del Club Colón e integrante del comité organizador; y el subsecretario de Deporte de la provincia, Julio Clement. También estuvieron la concejala Laura Mondino; el subsecretario de Turismo de la provincia, Santiago Sánchez; entre otros funcionarios locales y provinciales, además de los equipos que participan.

**Trabajo mancomunado**

Granato transmitió el compromiso de la Municipalidad de Santa Fe y expresó el “orgullo de ser por segundo año consecutivo sede de un evento tan importante de la élite del deporte nacional como es el básquet, y que además se recupere una tradición como es el básquet en la ciudad”.

También destacó la sinergia entre lo público-privado y la articulación entre los distintos niveles del Estado tanto local como provincial “con el fin de trabajar mancomunadamente y lograr que esta ciudad sea un lugar de eventos”. Antes de finalizar, Granato dijo: “Tenemos muchas expectativas, ganas de seguir creciendo, reconociendo también un gran compromiso por parte del club, esfuerzo que están poniendo todas las instituciones en general para salir de la pandemia que aún estamos atravesando”.

Por su parte, Fertonani dio la bienvenida pero también agradeció la participación de todos los clubes. “Para nosotros es un orgullo que el club Colón sea nuevamente sede de la Liga Nacional de Básquet. Siempre dijimos que el básquet volvía para quedarse y no solo en la liga sino con todas sus formativas. Esto no se hace solo sino que hay mucha gente que nos está apoyando, es un trabajo público-privado porque además de las empresas el aporte del estado es importante para hacer un evento de estas características”, cerró.