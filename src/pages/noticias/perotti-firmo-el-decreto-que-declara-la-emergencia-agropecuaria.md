---
category: Estado Real
date: 2022-01-19T06:15:38-03:00
thumbnail: https://assets.3dnoticias.com.ar/emergenciaagro.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Perotti firmó el decreto que declara la emergencia agropecuaria
title: Perotti firmó el decreto que declara la emergencia agropecuaria
entradilla: 'Como consecuencia de la sequía, la medida se extenderá hasta el próximo
  30 de junio en toda la provincia, a excepción del departamento General López. '

---
La emergencia, cuya norma lleva también las firmas de los ministros de Producción, Ciencia y Tecnología, Daniel Costamagna, y de Economía, Walter Agosto, abarca desde el 1 de enero hasta el 30 de junio de 2022, a las explotaciones agrícolas extensivas y a la ganadería en general afectadas por la sequía.

La decisión se tomó luego de la visita del ministro de Agricultura, Ganadería y Pesca de la Nación, Julián Domínguez, quien el pasado sábado, acompañado por el gobernador, recorrió algunos de los establecimientos rurales más afectados por este fenómeno; y de la reunión de la Comisión Provincial de Emergencia Agropecuaria, que este lunes analizó el impacto que ha tenido la falta de precipitaciones y las altas temperaturas en la producción santafesina y, a partir de dicho diagnóstico, se planificaron medidas a adoptar para paliar sus consecuencias.

Tras firmar el decreto, el gobernador explicó que la emergencia se extenderá inicialmente hasta mediados de marzo porque “es el período en el que todos los productores damnificados pueden presentar su solicitud. Entendemos que es la forma de estar acompañando este momento duro y difícil que muchos productores santafesinos están sufriendo”.

Los productores deberán iniciar los trámites a través del Sistema Santafesino de Gestión de Situaciones de Emergencia Agropecuaria (Sisagea). Además, la norma establece que hasta el 15 de marzo pueden presentar a través de la página web del gobierno provincial, [www.santafe.gob.ar](http://www.santafe.gob.ar/), los formularios de declaraciones juradas que disponga el Ministerio de Producción, Ciencia y Tecnología.

A continuación, Perotti indicó que “la recorrida del fin de semana con el ministro (Julián Domínguez) tuvo ese objetivo, que las autoridades nacionales puedan ver directamente cómo es la magnitud de la situación en el territorio provincial, y acompañar al productor en este momento”.

Al respecto, el mandatario provincial aseguró que “en los momentos duros el Estado tiene que estar a la par, y esta es la forma en la que sentimos acompañar a los sectores de la producción y el trabajo. Que nadie lo dude: allí va a estar siempre el gobierno de la provincia”.

Por último, el decreto prorroga el vencimiento de las cuotas 1, 2 y 3 de los impuestos inmobiliarios rural y urbano, a los productores que posean certificados de emergencia agropecuaria.