---
category: Agenda Ciudadana
date: 2021-04-25T06:00:52-03:00
thumbnail: https://assets.3dnoticias.com.ar/LIFSCHITZjpg.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Desmejoró la salud del exgobernador de Santa Fe Lifschitz
title: Desmejoró la salud del exgobernador de Santa Fe Lifschitz
entradilla: El actual diputado provincial permanece internado en sala de cuidados
  intensivos cursando la patología de Covid-19.

---
El exgobernador de Santa Fe Miguel Lifschitz permanece internado en Rosario en una sala de cuidados intensivos cursando la patología de Covid-19. Según el último reporte, "debido a una desmejoría de su cuadro pulmonar, el equipo médico indicó su ingreso a asistencia mecánica respiratoria, recibiendo todos los soportes terapéuticos correspondientes".

El legislador santafesino y ex intendente de Rosario fue internado el lunes 19 de abril para la realización de estudios de su cuadro de coronavirus. En una primera instancia fue ingresado para que se le realizaran "estudios clínicos y controles con el objetivo de reevaluar su estado en el marco de la patología que cursa".

En un primer momento, Lifschitz se mantuvo conectado en las redes sociales y hasta desde su cuenta oficial de Twitter felicitó a la ex intendenta de Rosario, Mónica Fein, por el triunfo en las elecciones internas del socialismo a nivel nacional. "Felicitaciones a la primera presidenta del PS a nivel nacional", posteó.

El parte médico emitido ayer señalaba que Lifschitz permanecía internado en sala de cuidados intensivos, por un cuadro de compromiso pulmonar compatible con Covid 19". "El paciente se encuentra estable, respondiendo a las medidas terapéuticas instauradas, en buen estado general", señalaba.

Referentes de diferentes partidos políticos expresaron en los últimos días en redes sociales el deseo de que el ex titular de la Casa Gris tenga una pronta recuperación.