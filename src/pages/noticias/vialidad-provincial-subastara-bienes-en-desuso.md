---
category: Agenda Ciudadana
date: 2020-12-05T12:59:02Z
thumbnail: https://assets.3dnoticias.com.ar/VIALIDAD.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Vialidad Provincial subastará bienes en desuso
title: Vialidad Provincial subastará bienes en desuso
entradilla: La subasta pública se realizará el 15 de diciembre en la sede del Camping
  de Empleados Viales en San Javier.

---
El Ministerio de Infraestructura, Servicios Públicos y Hábitat, a través de la Dirección Provincial de Vialidad, anunció una subasta pública Nº 01/2020, donde **se rematarán maquinarias viales, camionetas, camiones, casillas, chatarras, restos de hierros varios, entre otros bienes en desuso**.

**La subasta se realizará el día 15 de diciembre del corriente**, desde las 9 horas, **en la sede Camping de Empleados Viales en San Javier**, ubicado en Ruta Provincial Nº 1, Km 140,5.

Se cumplimentará con el protocolo de higiene y cuidado establecido por las autoridades provinciales en el marco de la pandemia por el virus SARS-CoV-2. Res. 41/2020 del Ministerio de Trabajo, Empleo y Seguridad Social.

Para mayor información, se adjunta el pliego y el volante informativo.

###### 🗄  [Descargar el Pliego de Condiciones](https://assets.3dnoticias.com.ar/PLIEGOVIALIDAD.pdf "Pliego de condiciones")

###### 🗄  [Descargar el Listado de lotes para subasta](https://assets.3dnoticias.com.ar/LISTADOVIALIDAD.pdf "Listado de lotes para subasta")

###### 🗄  [Descargar el Volante informativo](https://assets.3dnoticias.com.ar/VOLANTEVIALIDAD.pdf "Volante informativo")