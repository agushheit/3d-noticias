---
category: Deportes
date: 2021-01-24T10:21:25Z
thumbnail: https://assets.3dnoticias.com.ar/jo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Marisa Lemos
resumen: EL COMITÉ PARALÍMPICO INTERNACIONAL NIEGA LA SUSPENSIÓN DE TOKIO 2021
title: EL COMITÉ PARALÍMPICO INTERNACIONAL NIEGA LA SUSPENSIÓN DE TOKIO 2021
entradilla: Tanto el Gobierno nipón como el Comité Organizador dicen estar decididos
  a organizar los juegos el próximo verano.

---
El Gobierno de Japón insistió este viernes (22.01.2021) en su idea de celebrar los JJ.OO. previstos para este verano en la capital nipona, a pesar de los rumores sobre su posible cancelación y de la gravedad de la situación de la pandemia en el país y en el resto del mundo.

"Estoy decidido a organizar unos Juegos seguros mientras colaboramos estrechamente con el gobierno metropolitano de Tokio, el comité organizador (de 2020) y el COI", dijo hoy el primer ministro de Japón, Yoshihide Suga, durante su comparecencia en una sesión de la cámara alta del parlamento nipón.

Suga reiteró que los Juegos deben servir "como prueba de la victoria de la humanidad contra la COVID-19", el mensaje que las autoridades japonesas vienen enarbolando desde que en marzo pasado se decidió a raíz de la pandemia posponer un año los Juegos, inicialmente previstos para el verano de 2020.

**Retraso o cancelación**

El líder japonés se pronunció así en medio de las voces crecientes que tanto desde Japón como desde el exterior hablan de un posible retraso o incluso de una cancelación de los Juegos de Tokio debido a la crisis sanitaria global, que en estos momentos es peor en términos de contagios diarios que el pasado marzo.

En la víspera, el diario británico The Times publicó una información que señalaba que el Ejecutivo japonés habría ya llegado a la conclusión de que celebrar los Juegos este verano sería imposible, a partir de fuentes de la coalición gobernante.

**Hashimoto también desmiente**

La ministra nipona a cargo de los Juegos, Seiko Hashimoto, desmintió esta información en declaraciones realizadas a los medios de su país este viernes.

"Desde el Gobierno de Japón haremos todos los esfuerzo hacia la realización de los Juegos Olímpicos y Paralímpicos de este verano", dijo al ser preguntada por el tema Hashimoto, quien añadió que Japón "está haciendo todo lo posible por controlar la pandemia" y "tomando las medidas necesarias" para la celebración de los Juegos.

El comité organizador de los Juegos se expresó este viernes en la misma línea, al afirmar que el Gobierno de Japón "lidera una serie de reuniones de coordinación sobre las medidas contra la covid-19" y "está implementando extensas acciones para prevenir infecciones con objeto de poder celebrar los Juegos".

Tanto las autoridades niponas implicadas en los Juegos como el Comité Olímpico Internacional (COI) y el Paralímpico (IPC) "están plenamente centrados en organizar los Juegos este verano", recalcaron en un comunicado los responsables de Tokio 2020.

Mike Peters, director general del IPC, envió una carta a todos los comités nacionales. El Comité Olímpico Internacional también desestimó los rumores de cancelación.

![](https://assets.3dnoticias.com.ar/1.jpg)

![](https://assets.3dnoticias.com.ar/2.jpg)

![](https://assets.3dnoticias.com.ar/3.jpg)