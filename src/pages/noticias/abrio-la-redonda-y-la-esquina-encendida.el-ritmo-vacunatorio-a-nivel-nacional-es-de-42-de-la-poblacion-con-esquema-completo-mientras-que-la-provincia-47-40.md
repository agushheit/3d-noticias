---
category: La Ciudad
date: 2021-09-10T06:00:30-03:00
thumbnail: https://assets.3dnoticias.com.ar/VACUNACION.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Se colocaron 4.500 segundas dosis de Astrazeneca en la ciudad de Santa Fe
title: Abrió La Redonda y la Esquina Encendida. El ritmo vacunatorio a nivel nacional
  es de 42% de la población con esquema completo, mientras que la provincia 47,40
  %
entradilla: Abrió La Redonda y la Esquina Encendida. El ritmo vacunatorio a nivel
  nacional es de 42% de la población con esquema completo, mientras que la provincia
  47,40 %

---
Este jueves se retomó la vacunación con segundas dosis de Astrazeneca en el cronograma santafesino de inoculación contra el coronavirus.

En la ciudad de Santa Fe se turnaron a 4.500 vecinos para recibir la segunda dosis de la vacuna de origen británico. De ese total, 3.500 fueron citados a la Esquina Encendida de 8 a 16.30. Mientras que los 1000 restantes harán lo propio en La Redonda, de 8 a 12.30.

Además, cabe recordar que el gobierno provincial ultima detalles para empezar esta semana con la colocación de vacunas Pfizer a menores de 18 sin comorbilidades.

**Ritmo**

De acuerdo a los datos que brinda el Monitor Público de Vacunación del gobierno nacional, a la fecha recibieron una dosis del antígeno contra el coronavirus 28.879.464 ciudadanos, lo que equivale al 63,64% de la población; mientras que 19.057.498 completaron el esquema, es decir el 42% de la ciudadanía.

A nivel provincial, en Santa Fe el ritmo de vacunación es levemente superior. 2.359.413 ya recibieron la primera dosis (66,76%), mientras que 1.675.343 completaron el esquema, lo que se traduce en un 47,40% de la población.