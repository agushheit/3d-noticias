---
category: Agenda Ciudadana
date: 2021-03-26T09:11:56-03:00
thumbnail: https://assets.3dnoticias.com.ar/vizzoti.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Clarín
resumen: 'Testeos pagados por los viajeros y sin vuelos desde Brasil, Chile y México:
  el Gobierno definió las nuevas restricciones para prevenir la segunda ola'
title: 'Testeos pagados por los viajeros y sin vuelos desde Brasil, Chile y México:
  el Gobierno definió las nuevas restricciones para prevenir la segunda ola'
entradilla: Las medidas regirán desde este sábado y son por tiempo indeterminado.

---
Tras largas deliberaciones, en las primeras horas de este viernes se publicará en el Boletín Oficial la decisión administrativa de Jefatura de Gabinete con las nuevas restricciones oficiales para desalentar los viajes hacia y desde el exterior y prevenir la segunda ola de Covid-19. Hay cerca de 27 mil argentinos en el extranjero. Los viajeros que regresen al país deberán pagar su test al llegar al país y uno 7 días días después del regreso. El sábado se suspenderán los vuelos procedentes de Brasil, Chile y México. Hasta ahora solo estaban prohibidos los vuelos a Gran Bretaña. 

En el último DNU que extendió el DISPO hasta el 9 de abril se acordó restringir los vuelos desde los países de la región, Europa, México y los Estados Unidos. Finalmente el Ejecutivo decidió ir más lejos y endurecer las medidas. Estados Unidos quedó fuera de la lista, porque -según explicaron fuentes oficiales- no se registraron contagios masivos ni circulación de las cepas más agresivas, aunque allí se registró, además de la cepa de manaos, la sudafricana.  

En caso de que los testeos en el aeropuerto arrojen resultados positivos, deberán realizar otro testeos de secuenciación genómica -para saber de qué cepa de coronavirus se trata- y aislarse junto con sus contactos estrechos en lugares dispuestos por el Gobierno. La estadía en esos lugares, correría por cuenta del pasajero.

Aun los pasajeros cuyos testeos sean negativos estarán obligados a aislarse 10 días, desde el chequeo anterior y obligatorio para abordar el vuelo en el país de procedencia. La cuarentena será controlada por cada una de las 24 jurisdicciones. 

El Gobierno trabajaba desde hace dos semanas en medidas para desalentar los viajes al exterior, donde ya se registran transmisión comunitaria de las nuevas cepas de coronavirus.

Los errores comunicacionales del Gobierno se acumularon; sobre todo por las dilaciones oficiales. El Ejecutivo desmintió otra vez al canciller Felipe Solá, que por la mañana sugirió en declaraciones radiales que los argentinos que regresen al país con síntomas de Covid-19 tendrán que pagarse un hotel para cumplir el aislamiento. Desde la Casa Rosada relativizaron sus dichos, pero finalmente la Decisión Administrativa de jefatura de Gabinete le dio la razón al ministro de relaciones exteriores.

Algo similar había ocurrido con la propia ministra de Salud Carla Vizzotti, quien el miércoles sugirió que se analizaban restricciones a la circulación interna por horarios. Voceros de Casa Rosada la desmintieron. En los despachos oficiales no quieren entorpecer el rebote de la economía y monitorean obsesivamente el humor social. 

En tanto, Vizzotti; y sus pares de Ciudad, Fernán Quirós; y Provincia, Daniel Gollan; se refirieron a la posibilidad de diferir la segunda dosis de las vacunas contra el coronavirus (con la excepción de la Sputnik V). El consejo Federal de Salud (COFESA), que reúne a los 24 ministros del área del país, lo analizará este viernes, pero se adivina el consenso entre las 24 jurisdicciones y la Nación.