---
category: Agenda Ciudadana
date: 2021-06-19T06:00:22-03:00
thumbnail: https://assets.3dnoticias.com.ar/BARBIJO.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Barbijos santafesinos: balas de plata contra el Covid'
title: 'Barbijos santafesinos: balas de plata contra el Covid'
entradilla: Una pyme santafesina desarrolló barbijos con nanotecnología, cuyas partículas
  de plata matan a virus y bacterias. Se consiguen más baratos que los del Conicet.

---
La empresa santafesina Nanotek se dedica al desarrollo de nanotecnología aplicada a diversos productos de uso profesional y médico. Hace aproximadamente un mes lanzaron al mercado, con la aprobación de Anmat, los barbijos Ion Positivo, cuyas nanopartículas de plata y zinc matan bacterias y virus como el del Covid-19.

En comunicación con UNO, el ingeniero Gerardo López explicó que el barbijo funciona como un filtro, como cualquier otro barbijo, pero los microorganismos que quedan atrapados en la tela mueren por la acción de las nanopartículas de plata incluidas en la formulación del material.

“Los barbijos de tela se lavan porque los jabones matan a los virus y bacterias”, explicó el experto. “El barbijo de Nanotek es autosanitizante, no es necesario lavarlo porque actúa por sí mismo como un desinfectante”.

“Está diseñado para que lo puedas usar de manera continua durante ocho horas por día, cinco días de la semana y sin necesidad de lavarlo, manteniendo la capacidad antiviral”, explicó, agregando que en el laboratorio probaron que se mantendría por 15 días pero que la recomendación comercial es de 40 horas.

Antes de la pandemia las características antivirales y antibacteriales de la nanoplata eran aprovechadas en otras aplicaciones, como pinturas, sanitizantes para mascotas y telas hipoalergénicas. De hecho, cuando en 2009 apareció el virus del SARS 1 y se popularizó el uso del alcohol en gel, la empresa santafesina creó su versión con nanotecnología.

“La ventaja del alcohol en gel con respecto a los otros es igual que la del barbijo. Vos usas el alcohol en gel convencional y una vez que se evapora el alcohol los microorganismos ya pueden volver a crecer. En cambio, con estos, cuando te aplicas el producto te queda una capa invisible de material con nanoplata en las manos durante seis a ocho horas y tus manos siguen limpias”, contó el ingeniero.

Este alcohol en gel con nanopartículas se comercializó en lugares donde se elaboran alimentos para garantizar que durante la jornada de trabajo las manos de los trabajadores estén desinfectadas. Poco antes de que comenzara la pandemia del coronavirus salió al mercado su forma doméstica, llamada NanoAsepsis.

**Matar al coronavirus**

Gerardo López, ingeniero y profesor durante 30 años de la UTN, afirmó que la nanotecnología “es la quinta revolución industrial”. Pero, ¿de qué se trata?

“Es la manipulación de la materia en una escala sumamente pequeña –explicó. Las nanopartículas de plata que usamos para el barbijo son cinco veces más chicas que un virus, o 50 veces más chicas que un microbio”.

En este sentido, describió que los microorganismos después de estar en contacto con las partículas de nanoplata se ven con agujeritos. “Lo que hace es impedir que el microorganismo pueda fabricar la pared que lo recubre y controle lo que entra y lo que sale”.

**Gentileza**

La manipulación de la materia en esas dimensiones potencia todo tipo de efectos porque así la materia exhibe características que no se ven a simple vista. “En el caso de la plata se potencia su propiedad bactericida y antiviral sin que eso implique problemas desde el punto de vista de las células comunes del cuerpo humano. Si ves al virus, parece acribillado con nanoplata”, dijo López.

“Hoy estamos con la nanotecnología como estaba la informática en la década del 80 del siglo pasado. Es todo para crecer. Los estudios sociológicos que hablan de incorporar tecnología están parados en la ciencia de la informática y la tecnología de la comunicación; esa tecnología ya está madura y distribuida en todo el mundo. La nanotecnología es la que sigue”.

**Química sustentable**

“Nosotros usamos balas de nanoplata para matar virus, no nos tiramos tiros en el pie”, bromeó el ingeniero, fundador de Nanotek. El cuidado del medio ambiente no es una consigna de moda sino un precepto por el cual se rige la empresa en particular y el desarrollo de la nanotecnología en general.

Según López, la nanotecnología es “limpia” por tres razones:

Todos los procesos son de emisión cero: no sale humo ni se desechan líquidos o sólidos. Todo lo utilizado para sintetizar nanopartículas queda en el producto.

Se trabaja con materiales amigables para el ambiente para la síntesis química. “Para hacer nanoplata y nanohierro utilizamos glucosa, azúcares, almidones; y reactivos como vitamina C. A esos productos y una sal de plata o zinc, los convertimos en nanopartículas.

Se usa poca cantidad de materiales, medidas en miligramos por producto comercial. “Eso hace que el recurso esté disponible por un tiempo mucho más prolongado para la humanidad porque lo que utilizás son mínimas reacciones”.

“El objetivo es pasar de reactivos químicos impactantes, como ácidos, a estos productos orgánicos amigables con el ambiente. Es lo que se llama química sustentable o green chemestry (química verde)”, dijo López. “Venimos de una formación académica en la que fueron inculcados todos estos aspectos y no solo la productividad”.

**Ciencia del litoral**

Nanotek es una pyme de capital argentino nacida de investigaciones hechas en el Conicet, en universidades, y con el aporte de un capital privado. “En este momento el área de desarrollo está alojada en el parque tecnológico del litoral centro, en el barrio El Pozo, ya hace dos años estamos ahí”, contó Gerardo López.

“Trabajamos desarrollando estos nanomateriales, que fabricamos nosotros, y después probamos su inclusión en distintos tipos de materiales de uso común para tratar de mejorarlos. Tanto en los barbijos como en otros productos que elaboramos la estrategia de Nanotek es la siguiente: probamos nuestros nanomateriales con algún sustrato, vemos que tiene buenas características y nos ponemos en contacto con alguna empresa que esté en el rubro”.

“En este caso nos pusimos en contacto con un fabricante de barbijos que se llama Siete Ideas, de la provincia de Buenos Aires, y le propusimos trabajar en conjunto. Ellos fabrican el barbijo y nosotros le incorporamos la nanotecnología”, precisó.

El barbijo comenzó a gestarse hace medio año y desde su lanzamiento ha tenido un crecimiento explosivo. “En general hemos trabajado con reinversión del capital propio, algunos subsidios y premios. La base siempre de lo que hacemos es conocimiento al mismo nivel internacional de todos los organismos de ciencia y técnica que publican revistas internacionales”.

El ingeniero contó que la empresa no tiene todo el equipamiento que hace falta para caracterizar los materiales que realizan y que trabajan con convenios con la universidad, con el Conicet y con institutos de la zona. “El lunes estábamos haciendo los ensayos con microscopio electrónico de Diamante, Entre Ríos. También tenemos una conexión importante con la trama de ciencia y técnica de la región. Es una de las ventajas que nos da el hecho de estar en el parque tecnológico”.

Finalmente, Gerardo López opinó que a pesar de que no es prioritaria desde el punto de vista económico “la calidad de la educación argentina por lo menos a nivel universitario sigue siendo excelente”.

“Todos los que trabajamos en Nanotek salimos de universidades argentinas. Yo fui docente 30 años en la UTN y estamos en contacto directo. De hecho, incorporé el tema de nanotecnología en la materia de Ciencias de los Materiales justamente para mantenernos actualizados. Pero esto es algo que muchos de los docentes universitarios hacen llevando lo que es su conocimiento en investigación y desarrollo a volcarlo también a las aulas”. Y concluyó: “Hay mucho compromiso con lo educativo que tendría que ser mejor reconocido desde el punto de vista de la remuneración”.