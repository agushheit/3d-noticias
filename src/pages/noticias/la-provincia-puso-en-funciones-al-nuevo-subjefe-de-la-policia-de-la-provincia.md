---
category: Estado Real
date: 2021-08-25T06:00:17-03:00
thumbnail: https://assets.3dnoticias.com.ar/SUBJEFE.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La provincia puso en funciones al nuevo subjefe de la policía de la provincia
title: La provincia puso en funciones al nuevo subjefe de la policía de la provincia
entradilla: El ministro de Seguridad, tomó juramento a Diego Germán González, que
  se desempeñará como subjefe de la Policía provincial.

---
El Gobierno provincial, a través del Ministerio de Seguridad, designó al Director General de Policía, Abogado Diego Germán González como el nuevo subjefe de la Policía de la provincia de Santa Fe en un acto llevado a cabo en la sede de Recreo del ISEP.

En ese marco, el ministro Jorge Lagna, remarcó la importancia de contar con Diego González en la subjefatura de Policía por su "doble rol, como Policía y abogado, porque necesitamos más trabajo en conjunto, la justicia y el Ministerio. Necesitamos tener protocolos de actuación conjunta entre ambos. Porque la sociedad es la que sufre cuando hay cortocircuitos entre nosotros", remarcó Lagna.

"Hoy nos toca conducir este Ministerio de Seguridad y creo que somos un equipo, junto con la Policía provincial. Nuestro objetivo es más capacitación para la fuerza, más policías y que estén mejor remunerados", sostuvo el funcionario provincial, y agregó: "Vamos a jerarquizar el ISEP, porque nuestra Policía tiene que mantener en alto el orgullo de ser policía frentea una sociedad con delitos cada vez más complejos. Por eso necesitamos fuerzas más preparadas para prevenir, contener el delito y proteger a la gente", finalizó Jorge Lagna.

Estuvieron presentes la jefa de Policía provincial, Lic. Emilce Chimenti; el secretario de Policía y Gestión de la Información, Abog. Jorge Fernández;

el director general del ISEP, Lic. Gabriel Leegstra; el director general de AIC, Carlos Zoratti; la Subdirectora de AIC, Lic. Doris Del Valle Abdala; y oficiales de la Policía provincial.