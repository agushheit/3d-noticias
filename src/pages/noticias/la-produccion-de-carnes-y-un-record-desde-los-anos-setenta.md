---
category: El Campo
date: 2021-01-09T11:50:50Z
thumbnail: https://assets.3dnoticias.com.ar/090121-carne.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: '3DNoticias - Fuente: El Litoral'
resumen: La producción de carnes y un récord desde los años setenta
title: La producción de carnes y un récord desde los años setenta
entradilla: 'La mejora viene con yapa: menos consumo interno bovino, más exportaciones
  y crecimiento de las alternativas aviar y porcina en las mesas nacionales.'

---
«Los mercados de carnes (bovina, aviar y porcina) mostraron un desempeño satisfactorio en el 2020, que se destaca particularmente en el difícil contexto que debieron atravesar todas las actividades productivas y la economía argentina en general: un año de pandemia y de una política de control sanitario que impuso importantes restricciones al funcionamiento y la operatoria de las empresas».

Con este párrafo abre el informe que realizaron Juan Manuel Garzón y Nicolás Torre para Fundación Mediterránea - Ieral, sobre la base de datos oficiales y estimaciones propias. 

El informe revela que la producción de las tres carnes habría llegado a 6,04 millones de toneladas, unos 133,6 kilos promedio por habitante, mientras que las exportaciones a 1,19 millones (26,4 kilos per cápita).

«Son levemente superiores a los del 2019, pero además **son niveles récord en los últimos 40 años**; desde mediados de la década de los setenta hasta el presente, nunca habíamos producido ni exportado tanta proteína animal».

**El consumo medio de las tres carnes se habría ubicado en 108,0 kilos por habitante, quedando levemente por debajo de la cifra del año 2019 (108,8 kilos**). 

El dato quizás más relevante aquí, por su impacto simbólico, es el consumo de carne bovina, que se habría ubicado en 49,7 kilos per cápita, el registro más bajo desde hace décadas (al menos 50/60 años).