---
category: La Ciudad
date: 2021-08-03T06:15:12-03:00
thumbnail: https://assets.3dnoticias.com.ar/LASETUBAL.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Por qué es peligroso caminar por la Laguna Setúbal
title: Por qué es peligroso caminar por la Laguna Setúbal
entradilla: Pese a las advertencias que hizo la Municipalidad y los especialistas,
  todavía hay gente que camina sobre los bancos de arena. Los agentes los obligan
  a retirarse. Pero al rato aparecen nuevos curiosos.

---
"No se puede", "está prohibido", "es un peligro", "se recuerda a la población que no se debe...". Los llamados de atención y las advertencias sobre el riesgo cierto de transitar sobre el lecho de la laguna Setúbal se repiten en muchos títulos y con todos los adjetivos desde que la bajante dejó expuesta la superficie antes cubierta por agua. No obstante, la postal que se observa desde uno y otro lado contradice todo aviso.

Hay personas que deciden emprender la caminata sobre terreno desconocido y poco firme.

En ese sentido, la Secretaría de Asuntos Hídricos y Gestión de Riesgos municipal viene advirtiendo de esta situación, al tiempo que personal de Protección Civil llama la atención desde la orilla a través de altavoces.

Los argumentos para desalentar esta práctica son claros:

\- El fondo de la laguna está formado principalmente por materiales limosos y arcillas.

\- El suelo no es homogéneo, sino que presenta zonas con sedimentos antiguos y resistentes y otras con fangos muy blandos de depositación reciente.

\- El fondo está interrumpido por las fosas de dragado que se hicieron en su momento para los alteos de barrio El Pozo, Parque Tecnológico, Ciudad Universitaria y Costanera Oeste. Estos pozos constituyen un grave peligro para las personas, ya que los cambios en la profundidad son muy abruptos, e inesperadamente el lecho pasa de tener una profundidad baja a muy profunda.

\- El lecho de la laguna es un área natural no preparada para su tránsito y la intromisión de las personas en estos momentos conlleva riesgos de contaminación del mismo con diversas sustancias como combustible, aceites, residuos de neumáticos o de frenos, así como deshechos aportados por los visitantes o vehículos que la transiten, lo cual aporta al deterioro sistemático del sistema natural.

Ante cualquier consulta se pueden comunicar al teléfono de Atención Ciudadana: 0800-555-7000.