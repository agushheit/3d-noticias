---
category: Agenda Ciudadana
date: 2021-03-03T06:59:43-03:00
thumbnail: https://assets.3dnoticias.com.ar/clases-presenciales.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias con información de LT10
resumen: Este jueves comienza la vacunación de los docentes
title: Este jueves comienza la vacunación de los docentes
entradilla: Tras el arribo de las 37.800 vacunas de Sinopharm el Ministerio de Salud
  confirmó que este jueves inicia el cronograma. Comenzarán con el nivel inicial,
  1º, 2º, 3º grado, y en escuelas especiales.

---
El ministerio de Salud de Nación confirmó el arribo a Santa Fe de las 37.800 vacunas contra el coronavirus de Sinopharm. En este sentido, confirmaron que el jueves iniciará la campaña de vacunación para los docentes y asistentes escolares. 

De este modo, en las regiones de Salud Rosario, Santa Fe, Rafaela, Venado Tuerto y Reconquista ya están dispuestos los lugares y la logística necesaria para inocular, en una primera etapa, a docentes de escuelas públicas y privadas que se desempeñan en el nivel inicial, 1º, 2º, 3º grado, y en escuelas especiales; según lo establecido por el Ministerio de Salud de la Nación.

En nuestra ciudad el operativo se realizará en el CEF 29. Están dispuestos 20 puestos simultáneos y según precisó Jorge Prieto secretario de Salud se darán 1.800 turnos por día.

Al respecto, la ministra de Salud Sonia Martorano explicó que la vacuna Sinopharm “tiene la ventaja que no requiere ser conservada a temperaturas bajo cero, sino entre los 2 y 8 grados centígrados, lo que facilita su distribución y aplicación”.

Precisó, además, “que se compone de dos dosis, con un intervalo mínimo de 28 días de aplicación entre una y otra, y que está indicada para personas de 18 a 59 años”.

**Continuidad de los grupos** 

Personal de apoyo a la enseñanza, todo otro personal sin designación docente pero que trabaja en establecimientos educativos de la educación obligatoria en distintas áreas y servicios.

Docentes de nivel primario, del segundo ciclo de 4° a 6°/7° grados.  

Docentes de nivel secundario, de educación permanente para jóvenes y adultos en todos sus niveles e instructores de formación profesional.

En el último grupo estarán los docentes y no docentes de institutos de educación superior y universidades.

**Expectativa de los gremios**

Antes de la confirmación, desde Amsafé La Capital, Rodrigo Alonso manifestó que aguardaban "más precisiones" para el proceso de inoculación a los maestros. 

Además, consideró que es esencial la vacunación docente para poder diagramar un ciclo lectivo que sea atravesado  por la presencialidad. Sin embargo, aclaró que esa asistencia presencial debe tener condiciones, y "una muy importante es la vacunación".