---
category: Agenda Ciudadana
date: 2021-04-11T06:00:02-03:00
thumbnail: https://assets.3dnoticias.com.ar/PREPAGAS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La medicina prepaga aumenta en abril y mayo
title: La medicina prepaga aumenta en abril y mayo
entradilla: La Superintendencia de Servicios de Salud ha sugerido al Ministerio incrementos
  escalonados en las cuotas mensuales que abonan los afiliados a las Entidades de
  Medicina Prepaga.

---
El Ministerio de Salud autorizó a todas las Entidades de Medicina Prepaga inscriptas en el Registro Nacional (RNEMP) un aumento general de hasta 4,5% a partir del 1 de abril, y de hasta 5,5% a partir del 1 de mayo de 2021.

Así lo establece la resolución 987 del Ministerio de Salud publicada este sábado en el Boletín Oficial, con la firma de la titular del área, Carla Vizzotti,

De este modo, los Agentes del Seguro de Salud y las Entidades de Medicina Prepaga deberán incrementar los valores retributivos de las prestaciones médico-asistenciales en un 4,5% a partir del 1 de abril en relación con los valores vigentes al 1 de marzo de 2021, y en un 5,5% adicional a partir del 1 de mayo próximo,

En los incrementos de los valores de cuotas mensuales a las Entidades de Medicina Prepaga, se tuvieron en cuenta los distintos factores que confluyen a la estructura de costos

En los considerandos de la resolución publicada este sábado, se menciona que la Cámara de Instituciones Médico Asistenciales de la República Argentina (Cimara) y la Asociación de Entidades de Medicina Privada (Ademp) han informado en agosto pasado "el impacto que les ha causado el incremento de los costos del sector desde la fecha del aumento de cuotas autorizado a las Entidades de Medicina Prepaga en diciembre de 2019".

Especialmente en el delicado contexto de atención de la pandemia mundial suscitada a principios de 2020, "y que ha demandado esfuerzos inusitados del personal de salud, y en función de ello han requerido oportunamente a la Superintendencia de Servicios de Salud que se sirva promover la autorización de nuevos aumentos que permitan recomponer el financiamiento para afrontar tales costos, a aplicar eventualmente en tramos".

Tras un pormenorizado análisis del informe técnico, la Superintendencia de Servicios de Salud ha sugerido al Ministerio incrementos escalonados en las cuotas mensuales que abonan los afiliados a las Entidades de Medicina Prepaga.

En los incrementos de los valores de cuotas mensuales a las Entidades de Medicina Prepaga, se tuvieron en cuenta los distintos factores que confluyen a la estructura de costos, en especial aquellos referidos a la incidencia que los requerimientos de recursos humanos imponen, tales como incrementos fijos o cumplimiento de paritarias vigentes.

Además, cuestiones de oportunidad, mérito y conveniencia instruidas por el propio Poder Ejecutivo Nacional han suspendido o dejado sin efecto parcialmente algunos de los incrementos, en mérito a factores macroeconómicos y consideraciones de incidencia colectiva, de abordaje necesario y prioritario.