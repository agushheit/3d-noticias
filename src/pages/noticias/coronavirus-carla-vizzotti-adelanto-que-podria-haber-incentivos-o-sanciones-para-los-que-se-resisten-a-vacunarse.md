---
category: Agenda Ciudadana
date: 2021-08-08T06:15:38-03:00
thumbnail: https://assets.3dnoticias.com.ar/vacunatecareta.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'Coronavirus: Carla Vizzotti adelantó que podría haber incentivos o sanciones
  para los que se resisten a vacunarse'
title: 'Coronavirus: Carla Vizzotti adelantó que podría haber incentivos o sanciones
  para los que se resisten a vacunarse'
entradilla: Frente al rechazo de algunas personas a aplicarse la vacuna contra el
  covid 19, la ministra de Salud de la Nación adelantó que no se descarta aplicar
  medidas para revertir la situación.

---
La ministra de Salud, Caral Vizzotti, adelantó hoy en declaraciones radiales que desde el Gobierno nacional no descartan tomar medidas para las personas que no quieren vacunarse, o como suelen denominarse, los antivacunas. Piensan en algunos incentivos, pero tampoco descartan sanciones.

La Argentina atraviesa una necesidad imperiosa de completar el esquema de vacunación a quienes ya se colocaron la primera dosis, pero también arrastra el dilema de qué hacer con las personas que no quieren vacunarse, ya sea por motivos religiosos, de escepticismo o simplemente falta de información para acceder a los turnos.

Ya sea por uno u otros motivos, Vizzotti señaló que las personas que "se vacunan no solo reciben un beneficio individual, sino que aportan al bien colectivo. Así que tienen que tener una ventaja en relación a quien decide no aplicarse una dosis en medio de una emergencia sanitaria". Al respecto, detalló que "todavía no estamos en una instancia" que requiera una normativa legal para fortalecer la vacunación, pero "no lo descartamos".

Por otra parte, aseguró que la mayoría de las personas sí quiere vacunarse y para ello argumentó que "casi el 80% de los mayores de 18 años recibieron al menos una dosis" y que "más del 50% de los mayores de 60 tienen el esquema completo de vacunación".

Explicó también que la falta de vacunación por razones de decisión personal conlleva a que la lucha contra el covid se haga mucho más difícil todavía, lo que los obliga a profundizar la campaña de persuasión para alcanzar el mayor número de personas vacunadas posible. "Lo que no podemos tener es un sistema de salud colapsado como pasó en varias ciudades del mundo. Tenemos que saber que este virus ha llegado para quedarse", puntualizó al respecto.