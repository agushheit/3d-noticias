---
category: Agenda Ciudadana
date: 2021-03-12T07:04:17-03:00
thumbnail: https://assets.3dnoticias.com.ar/nafta.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de canal 3 Rosario
resumen: Los combustibles aumentarían este viernes y la nafta premium rozaría los
  $100 en Santa Fe
title: Los combustibles aumentarían este viernes y la nafta premium rozaría los $100
  en Santa Fe
entradilla: 'Así lo señalaron referentes estacioneros de la ciudad. Se trataría del
  sexto amento del año.

'

---
Este viernes, y por sexta vez en lo que va del año, volverían a subir los combustibles. Así lo anticiparon a UNO Santa Fe referentes de estaciones de servicio de esta capital.

La nueva suba estaría vinculada al aspecto impositivo del suministro que estaba previsto se produzca a principios de mes pero que el gobierno postergo para el día 12.

Aunque sin mayores precisiones, adelantaron que el nuevo incremento podría estar en el orden del 3 por ciento. Si eso sucede, la nafta premium de YPF (Infinia) que actualmente se consigue en la ciudad a $ 93.50 se acercaría a los 100 pesos ($96.30).

Cabe recordar que el último aumento se efectivizó el 15 de febrero y elevó a los otros combustibles a los siguientes valores: súper $81,70; infinia diesel $86,70 y diesel $74,50

Un empresario de la ciudad recordó con relación al componente impositivo que "el cuarto trimestre del año pasado no hubo actualización (se actualizan trimestralmente); se iba a hacer efectivo el primero de marzo pero un decreto lo trasladó para el día 12 de marzo".

Cabe recordar en ocasiones anteriores, las petroleras absorbieron el incremento por impuestos. "Este no será el caso", el dijo a UNO la fuente consultada, y subrayó que el "atraso que aducen las petroleras es del 15 por ciento".

En un año electoral, los estacioneros prevén (como en años anteriores) que los incrementos se den en los meses previos al inicio de la campaña. "Es probable que los aumentos se den ahora, para que luego se vuelvan a congelar", le dijo el referente del sector a este medio.

Daniel Gribone, de la cámara de estaciones de Servicio de Rosario explicó en la misma línea: "El gobierno trasladó para el 12 de marzo el aumento que debería haberse implementar el primero de marzo. Son impuestos internos, al dióxido de carbono y a la transferencia de los combustibles. Entendemos que va a repercutir, en el caso de las naftas en dos pesos por litro y en el gasoil 1,20 pesos".

Comentó en declaraciones al canal "El Tres" de Rosario que las actualizaciones se dan "cada tres meses y en base al índice de costo de vida. Esto lo fija la AFIP y el poder ejecutivo tiene la potestad de poder postergarlo en el tiempo pero en algún momento hay que hacerlos".

Por otro lado, no descartó que a dicho incremento se le pueda sumar un aumento vinculado a la suba que tuvo el barril de crudo que volvió a subir y cerró este jueves a 68,10 dólares.