---
category: Agenda Ciudadana
date: 2021-10-15T06:00:45-03:00
thumbnail: https://assets.3dnoticias.com.ar/cgt.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: La CGT ratificó la marcha del lunes bajo el lema "Desarrollo, Producción
  y Trabajo"
title: La CGT ratificó la marcha del lunes bajo el lema "Desarrollo, Producción y
  Trabajo"
entradilla: La movilización del acto peronista del lunes se concentrará en las avenidas
  Independencia y Paseo Colón y contará con la participación de todos los sectores
  del sindicalismo alineados en la central.

---
La CGT ratificó este jueves la movilización del lunes próximo hacia el Monumento al Trabajo para conmemorar los 76 años del 17 de Octubre de 1945 bajo el lema "Desarrollo, Producción y Trabajo", informó este jueves la central obrera en un documento de su consejo directivo.  
"El movimiento obrero organizado marchará el lunes próximo hacia el Monumento al Trabajo para conmemorar los 76 años del Día de la Lealtad Peronista bajo la consigna 'Desarrollo, Producción y Trabajo', en lo que será la primera gran movilización pospandemia de coronavirus", expresó la CGT, que colideran Héctor Daer y Carlos Acuña.  
La CGT convocó de manera oficial a la movilización y el acto peronista del lunes próximo en las Avenidas Independencia y Paseo Colón, que comenzará a las 14 y en el que participarán todos los sectores del sindicalismo alineados en la central, como la Corriente Federal de Trabajadores (CFT) y el Movimiento de Acción Sindical Argentino (MASA).  
La CFT es liderada por el titular de la Asociación Bancaria (AB) y candidato a diputado nacional bonaerense por el Frente de Todos (FdT), Sergio Palazzo, y el MASA es conducido por el exsecretario general del gremio de los taxistas, Omar Viviani.  
La central obrera detalló que el punto de partida de la movilización será la intersección de la Avenida Independencia y Defensa, y que también participarán los movimientos sociales.  
  
No habrá oradores y se leerá un documento político que elaboraron las organizaciones sindicales convocantes, en tanto las columnas de trabajadores se dirigirán hacia la Avenida Paseo Colón y girarán hacia la de Belgrano hasta el lugar del acto.  
  
A su vez, el Sindicato Argentino de Docentes Privados (Sadop), que lidera Jorge Kalinger e integra la Corriente Federal, adhirió este jueves a la movilización cegetista "en defensa de los derechos adquiridos" y en conmemoración del Día de la Lealtad Peronista.  
  
Los docentes de gestión privada se concentrarán desde las 12 en la Avenida 9 de Julio y México y marcharán por Independencia hacia el Monumento al Trabajo, y confirmaron que "se movilizarán hacia el lugar las seccionales porteña y Buenos Aires" del sindicato.