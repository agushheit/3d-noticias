---
category: La Ciudad
date: 2021-07-29T06:15:01-03:00
thumbnail: https://assets.3dnoticias.com.ar/ENRESS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: El Enress hace hasta cuatro controles por día del agua
title: El Enress hace hasta cuatro controles por día del agua
entradilla: La presidenta del Enress, Anahí Rodríguez, dijo que el agua es apta para
  consumo humano y que eso "se evalúa día a día".

---
La extraordinaria bajante del río Paraná generó que la empresa Aguas Santafesinas SA (Assa) tenga que redoblar los esfuerzos para sostener la calidad del agua potable para que sea apta para consumo humano. Al respecto, la presidenta del Ente Regulador de Servicios Sanitarios (Enress), Anahí Rodríguez, le dijo a UNO Santa Fe que desde el ente "se realizan entre tres y cuatro controles diarios" tanto en la toma de agua como en la red de distribución.

"Estamos reforzando los controles de calidad sobre el agua que estamos consumiendo. en este marco de bajante extraordinaria de nuestro río reforzamos los controles diarios en la toma y también en la red. Estamos monitoreando permanentemente la calidad", afirmó.

"El agua que consumimos es apta para el consumo humano. Hoy, cerca de las 7.30 tomamos las primeras muestras del día en las plantas de Assa, medimos los valores y se encuentran dentro de los parámetros que nosotros establecemos, incluso algunos valores que se habían elevado un poco ahora están a la baja", aseguró la presidenta del Enress y agregó: "El mensaje más importante que tenemos que dar es que el agua es apta. Entendemos que dadas algunas condiciones que están relacionadas con la bajante, se puede llegar a captar agua con mayor concentración de minerales, algo que es propio de la situación que estamos viviendo, los usuarios puedan notar un sabor más salado. Eso es algo que puede suceder, pero aun así, es apta para consumo".

Al ser consultada sobre cuán lejos se está de que el agua no sea apta para consumo humano, Rodríguez respondió: "Es un día a día. Estamos controlando no solo el agua, sino también manteniendo un diálogo con la empresa y la gerencia de control de calidad del Enress está reunida con el área de control de calidad de Assa para informarnos día a día de lo que la empresa pública está haciendo. No podemos dar un pronóstico porque seríamos irresponsables".

Rodríguez aseguró que se está atravesando el momento más crítico de la bajante ya que se están cumpliendo los pronósticos que emitió el Instituto Nacional del Agua. Al respecto adelantó que esta situación se sostendría hasta noviembre y después comenzará un período estival donde el consumo del agua siempre se eleva y los problemas son otros. "Esto es un día a día. Nosotros reforzamos los controles y les pedimos a los usuarios que tengan total certeza que el consumo racional doméstico que cada uno de nosotros hagamos en nuestros hogares ayuda a mitigar este impacto negativo que estamos teniendo por la bajante. Todos podemos ayudar a cuidar el agua, que es un recurso limitado y que hoy lo estamos viendo como nunca antes en los últimos 50 años", sostuvo.

En tanto, ante la pregunta sobre la gran cantidad de fugas que tiene la empresa y que se observan en la vía pública, Rodríguez dijo: "Ese es un tema que surgió porque en la medida que les pedimos a los usuarios que cuiden el agua, también le pedimos a la empresa en una reunión que tuvimos hace un par de días que refuercen el servicio para dar respuesta inmediata a cualquier rotura o pérdida que pueda haber en la vía pública. Sabemos que, en el marco de la pandemia, de las burbujas y la reducción de trabajadores este tema se puede dificultar. Pero a la empresa le pedimos permanentemente que muevan sus equipos para tener una respuesta inmediata y acotar cualquier derroche de agua potable en la vía pública".