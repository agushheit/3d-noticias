---
category: La Ciudad
date: 2021-01-04T10:12:15Z
thumbnail: https://assets.3dnoticias.com.ar/040121-sies107.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: El Litoral'
resumen: 'Crítico saldo de Año Nuevo: el Sies 107 debió asistir a 9 accidentados y
  4 heridos por armas'
title: 'Crítico saldo de Año Nuevo: el Sies 107 debió asistir a 9 accidentados y 4
  heridos por armas'
entradilla: Personas heridas por elementos cortantes, pacientes con convulsiones y
  hasta una mujer caída de altura. «Muchos son los factores que contribuyen, pero
  sin dudas el alcohol es el principal protagonista», advirtieron.

---
El Sistema Integrado de Emergencias Sanitarias (Sies) 107 reportó en un comunicado que **en la noche del 31 de diciembre de 2020 y la madrugada del primer día de 2021**  –lapso de apenas diez horas– debió realizar, en la ciudad de Santa Fe, **un total de cuarenta y dos servicios de urgencias médicas**, entre los que se registraron nueve asistencias a personas accidentadas (ocho que se movilizaban en motos y un peatón) y cuatro heridos por arma (dos de fuego y dos de arma blanca).

A su vez, en la discriminación de «otras atenciones», el sistema de emergencias médicas provincial asistió a cuatro personas heridas por elementos cortantes (dos hombres y dos mujeres), una persona con convulsiones, una mujer por caída de altura y otra mujer por trabajo de parto y, por derivaciones y controles de urgencia, doce hombres y diez mujeres.

No se reportaron personas con quemaduras ni intentos de suicidio (sobre este último punto, en la víspera de Navidad sí hubo un servicio de asistencia del 107).

«A la hora del balance de lo sucedido con los festejos en las fiestas de fin de año, siempre se hace referencia a las atenciones en las guardias de los hospitales, pero para que esas personas sean atendidas en dichos efectores de salud se necesita del servicio de emergencias 107», recordaron.

En el caso de la ciudad capital, el 107 dispuso un operativo sanitario especial «que consistió en **postas sanitarias en la vía pública**, a través de sus ambulancias ubicadas estratégicamente para arribar al lugar de requerimiento asistencial lo más rápido posible», expresaron.

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.1em; font-weight: 700;">El alcohol, principal protagonista</span>**

También en la víspera de Navidad el servicio fue muy demandado: de la noche del 24 de diciembre a la madrugada del 25, el Sies debió realizar un total de cincuenta y cuatro servicios en un período de solo diez horas: diez incidentes de tránsito, cuatro heridos por arma de fuego y dos por arma blanca y en el ítem «otras atenciones», treinta y ocho servicios de urgencia médica.

«No es grato informar incidentes en fechas tan sensibles como son las reuniones familiares para celebrar las fiestas de fin de año. Lamentablemente, los casos se siguen presentando con una alta demanda de asistencia por parte del servicio de emergencias 107», indicaron desde el sistema de emergencias médicas provincial.

«Analizando lo sucedido el 24 y 31 de diciembre vemos con preocupación la alta demanda de asistencia. Muchos son los factores que contribuyen, pero sin lugar a dudas, el alcohol es el principal protagonista», advirtieron.

Finalmente, **desde la dirección del Sies elevaron «un reconocimiento especial a la dotación de las ambulancias (choferes, enfermeros y médicos) y a la central de comunicaciones (radioperadores, médicos evaluadores y reguladores). A ellos,  quienes con su invalorable vocación de servicio estuvieron y están para ayudar a la gente que los necesita, un agradecimiento inmenso»**, subrayaron.