---
category: Agenda Ciudadana
date: 2020-12-30T12:05:35Z
thumbnail: https://assets.3dnoticias.com.ar/3012-francisco.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Infobae'
resumen: El mensaje del papa Francisco tras la sanción de la ley del aborto en Argentina
title: El mensaje del papa Francisco tras la sanción de la ley del aborto en Argentina
entradilla: El Sumo Pontífice destacó hoy que «vivir es, ante todo, haber recibido
  la vida», al encabezar en el Vaticano su última audiencia general del año.

---
Luego de la sanción de la ley de legalización del aborto en el Congreso, el papa Francisco planteó esta mañana que «todos nacemos porque alguien ha deseado para nosotros la vida», al encabezar en el Vaticano su última audiencia general del año.

«Para nosotros cristianos el dar las gracias ha dado nombre al Sacramento más esencial que hay: la Eucaristía. La palabra griega, de hecho, significa precisamente esto: acción de gracias», afirmó el pontífice durante la catequesis que brindó desde la Biblioteca del Palacio Apostólico del Vaticano.

«Los cristianos, como todos los creyentes, bendicen a Dios por el don de la vida. Vivir es ante todo haber recibido la vida. Todos nacemos porque alguien ha deseado para nosotros la vida», agregó en el mensaje escrito y que forma parte de una serie de catequesis sobre el agradecimiento que comenzó en diciembre.

![](https://assets.3dnoticias.com.ar/3012-francisco1.webp)

«Y esto es solo la primera de una larga serie de deudas que contraemos viviendo. Deudas de reconocimiento. En nuestra existencia, más de una persona nos ha mirado con ojos puros, gratuitamente», añadió el Papa.

Según el pontífice, «a menudo se trata de educadores, catequistas, personas que han desempeñado su rol más allá de la medida pedida por el deber. Y han hecho surgir en nosotros la gratitud».

Ayer, horas antes de que arranque el debate en el Senado del proyecto de legalización del aborto impulsado por el gobierno de Alberto Fernández, el Papa difundió un mensaje en sus redes sociales en el que advirtió: «Toda persona descartada es un hijo de Dios».

![](https://assets.3dnoticias.com.ar/3012-francisco2.webp)

Mañana jueves, el papa Francisco cerrará sus actividades de 2020 con la celebración de las Vísperas y el Tedeum de agradecimiento en la Basílica de San Pedro.