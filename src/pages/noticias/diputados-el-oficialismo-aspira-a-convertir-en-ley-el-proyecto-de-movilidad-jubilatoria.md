---
category: Agenda Ciudadana
date: 2020-12-29T11:23:10Z
thumbnail: https://assets.3dnoticias.com.ar/2912-movilidad-jubilatoria.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: NCN'
resumen: 'Diputados: El oficialismo aspira a convertir en ley el proyecto de movilidad
  jubilatoria'
title: 'Diputados: El oficialismo aspira a convertir en ley el proyecto de movilidad
  jubilatoria'
entradilla: Con el respaldo de bloques provinciales, se busca convertir en ley el
  proyecto de movilidad previsional que propone una fórmula que combina en un 50%
  la recaudación de la Anses y en otro 50% la variación salarial.

---
El proyecto, que ya tiene sanción del Senado, será debatido el martes -a partir de las 11- en una sesión especial, la última del año, que se realizará bajo la movilidad presencial con las excepciones de conexión remota para aquellos diputados que integran grupos de riesgo, ya sea por cuestiones de edad o de salud.

La iniciativa recibió dictamen el miércoles pasado en el marco de un plenario de las comisiones de Previsión y Seguridad Social, que preside Marcelo Casaretto; y de Presupuesto, a cargo de Carlos Heller; ambos del Frente de Todos.

El plenario de comisiones avaló el texto por el cual las jubilaciones se ajustarán con una fórmula que combina en un 50% la recaudación de la Anses y en otro 50% la variación salarial, surgida de la que resulte más alta entre las medidas por el Indec y por el Ministerio de Trabajo, con base en el índice de Remuneración Imponible Promedio de los Trabajadores Estables (Ripte).

La fórmula, que surgió de una propuesta elaborada sobre la base del trabajo de una comisión mixta que el Poder Ejecutivo envió al Senado, comenzará a utilizarse desde 2021, cuando venza el actual período de emergencia en la materia.

El nuevo índice está basado en la fórmula aplicada desde 2009 por el gobierno de Cristina Fernández de Kirchner, que luego el expresidente Mauricio Macri dejó sin efecto y estableció su propio esquema jubilatorio que, según los análisis, perjudicó los haberes del sector pasivo.

En su paso por el Senado y a sugerencia del Frente de Todos, se le introdujeron cambios sustanciales para que, entre otros aspectos, los aumentos previsionales sean trimestrales, con lo cual habrá cuatro incrementos por año.

El proyecto original establecía que los cambios en la fórmula de movilidad se iban a producir cada seis meses.

Esa modificación fue adoptada para «empalmar» los meses de «enero, febrero y marzo», que estaban «afuera» de la actualización, porque la fórmula del anterior Gobierno de Mauricio Macri se aprobó en diciembre de 2017, pero empezó a regir en marzo de 2018.

El oficialismo se aseguró la sanción del proyecto en el recinto, luego de que el Frente de Todos logró el respaldo de los diputados de Córdoba Federal, Pablo Cassineiro, del Frente Misionero de la Concordia Diego Sartori, y de Unidad Federal para el Desarrollo, José Luis Ramón.

El respaldo de los bloques provinciales es esencial para la bancada del oficialismo -conducida por Máximo Kirchner-, ya que tiene 117 votos y requiere del apoyo de otros bloques para construir una mayoría.

De esta manera, el apoyo de seis diputados del Interbloque de Unidad Federal para el Desarrollo y cuatro de Córdoba Federal, ya le garantiza al oficialismo un piso de 127 votos para poder para convertir en ley el proyecto impulsado por el Gobierno Nacional.

Fuentes del Frente de Todos confían en que también votarán a favor los dos diputados del bloque peronista que integran el interbloque Federal, dos de Acción Federal y uno del Movimiento Popular Neuquino.

En tanto, Juntos por el Cambio, Consenso Federal, y la izquierda objetaron que no se incluya a la inflación en la fórmula de ajuste a las jubilaciones propuesta por el Gobierno Nacional, con lo cual adelantaron su rechazo al texto.

La iniciativa que modifica la Ley de Movilidad Jubilatoria, establece cuatro aumentos anuales para el sector (uno cada tres meses), con un índice basado en la suba de la recaudación de la Anses y los ajustes salariales.

En la sesión del martes, los diputados buscarán aprobar además el proyecto que autoriza al Presidente de la Nación para ausentarse del país durante el año 2021 y el que aprueba el acuerdo entre Argentina y Qatar para evitar la doble imposición y prevenir la evasión fiscal en materia de la renta y patrimonio.

<br/>

[<span style="background-color: #212F3D; color: #ffffff; font-family: arial, helvetica, sans-serif; font-size: 0.9em; font-weight: bold;"> &nbsp; LEER EL DICTAMEN &nbsp;</span>](https://assets.3dnoticias.com.ar/2912-Dictamen-Movilidad-Jubilatoria.pdf "PDF")