---
category: Estado Real
date: 2021-02-14T06:00:04-03:00
thumbnail: https://assets.3dnoticias.com.ar/PEROTTIESCUELAS.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: El Gobierno Provincial Entregó 44 Millones De Pesos Para Mejoras Edilicias
  En Escuelas
title: El Gobierno Provincial Entregó 44 Millones De Pesos Para Mejoras Edilicias
  En Escuelas
entradilla: Los fondos de Asistencia de Necesidades Inmediatas fueron asignados a
  83 establecimientos educativos.

---
El gobierno provincial, a través del Ministerio de Educación, entregó partidas provenientes del Fondo de Asistencia de Necesidades Inmediatas (FANI) a 83 escuelas de los departamentos 9 de Julio, Vera, General Obligado, San Cristóbal, San Justo, Castellanos, Las Colonias, La Capital, San Martín, San Jerónimo, Belgrano, Iriondo, San Lorenzo, Rosario y General López. El monto total de los aportes fue de $ 44.342.194,12.

Al respecto, funcionarios de la cartera educativa señalaron que “existe una inversión muy importante de la provincia para poner en condiciones a todos los edificios escolares y prepararlos para recibir a los estudiantes, educadores y asistentes escolares en esta vuelta a clases tan ansiada”.

“En ese sentido, en el año 2020 los aportes FANI duplicaron lo entregado en el año 2019 y en lo que va de este 2021 ya se ha realizado la primera entrega por un monto de más de 44 millones de pesos para llegar a la mayor cantidad de escuelas posibles y garantizar que los establecimientos escolares sean un espacio seguro para nuestras niñas, niños y adolescentes en esta ansiada a la vuelta a la presencialidad”, concluyeron.

Los aportes serán destinados a la instalación y adecuación de redes eléctricas; adquisición de tanques de agua; adquisición y colocación de aberturas; colocación tanque cisterna y de reserva; construcción y reacondicionamiento de sanitarios y construcción de bachas lavamanos; impermeabilización de cubiertas y construcción de sobre techos; instalación, colocación y provisión de agua; rehabilitación e instalación de red de gas; reparaciones generales en edificios: galerías, aulas, sanitarios, muros, piletas de natación, cocinas y comedores.