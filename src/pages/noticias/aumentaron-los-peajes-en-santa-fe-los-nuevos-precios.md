---
category: Agenda Ciudadana
date: 2021-01-21T08:55:06Z
thumbnail: https://assets.3dnoticias.com.ar/peaje.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Lt10
resumen: 'Aumentaron los peajes en Santa Fe: los nuevos precios'
title: 'Aumentaron los peajes en Santa Fe: los nuevos precios'
entradilla: El incremento se aplicó tanto a la Autopista Santa Fe-Rosario como a los
  corredores provinciales que administran municipios y comunas.

---
A partir del martes 19 de enero, aumentaron los valores del peaje de la autopista Santa Fe - Rosario que ahora cuesta $80 en cada cabina del corredor, lo que significa que un viaje de ida y vuelta entre ambas ciudades tiene un costo total es de $320.

En el caso de quienes hayan adherido al abono del telepeaje que ofrece Vial Santa Fe, el fideicomiso que administra la autopista, los aumentos son menores: 56 pesos en los troncales y 14 en los accesos, lo que significa un ahorro del 30%. 

![](https://www.lt10.com.ar/multimedia/in1611144459100.jpeg)

El gobierno de Santa Fe también actualizó el cuadro tarifario en los corredores provinciales que administran municipios y comunas en las rutas 14, 18, 6 y 70, esta última que conecta a Santa Fe con Esperanza y Rafaela. En las rutas 6 y 70 el peaje será de $35. Y en la 14 y la 18 el doble: $65.