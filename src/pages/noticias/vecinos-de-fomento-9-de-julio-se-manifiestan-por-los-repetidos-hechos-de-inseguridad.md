---
category: La Ciudad
date: 2021-11-08T06:00:41-03:00
thumbnail: https://assets.3dnoticias.com.ar/fomento.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Vecinos de Fomento 9 de Julio se manifiestan por los repetidos hechos de
  inseguridad
title: Vecinos de Fomento 9 de Julio se manifiestan por los repetidos hechos de inseguridad
entradilla: 'Este viernes motochorros intentaron robar una moto pero lo impidieron
  los vecinos. Uno de los ladrones trató de evitar el "arresto ciudadano" a los tiros.

'

---
La inseguridad azota las calles de la ciudad a toda hora y en todos los barrios. Un violento episodio delictivo que se registró el viernes por la noche en barrio Fomento 9 de Julio, colmó la paciencia de los vecinos, que este lunes por la tarde se manifestarán en la esquina de Iturraspe y San Jerónimo.

Si bien la situación de inseguridad que se vive a diario tiene en vilo a todos los santafesinos, puntualmente lo que ocurrió el viernes por la noche en barrio Fomento 9 de Julio, tuvo condimentos de violencia extrema que de milagro no tuvo heridos de gravedad.

Dos motochorros persiguieron a un vecinos por algunas calles en contramano para robarle la moto. Lo cierto es que en el momento justo de la ejecución de la acción delictiva, intervinieron varios vecinos que pudieron finalmente impedir el hecho y detener a uno de los delincuentes. Pero allí no quedó todo, porque el ladrón que logró zafar de la situación de "arresto ciudadano", sacó un arma y comenzó a disparar contra los vecinos para que liberen a su cómplice de delito.

Minutos más tarde llegó la Policía y se llevó al motochorro detenido por los mismos vecinos, pero la situación de disparos puso en alerta a los ciudadanos sobre la violencia extrema con la que los delincuentes salen a las calles a robar.

Por este último hecho de inseguridad narrado, más los que ocurren a diario en la zona, es que los vecinos de barrio Fomento 9 de Julio saldrán este lunes por la tarde, a las 18, a manifestar su malestar por la situación y a volver a pedir seguridad.