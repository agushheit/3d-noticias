---
category: La Ciudad
date: 2021-07-24T06:00:19-03:00
thumbnail: https://assets.3dnoticias.com.ar/MAPEO.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Municipalidad presentó el mapeo educativo a la Comisión del FAE
title: La Municipalidad presentó el mapeo educativo a la Comisión del FAE
entradilla: 'Se localizaron todas las escuelas de la ciudad en un mapa, con información
  y fotos, para poder establecer cuántas instituciones y complejos educativos hay. '

---
Este martes, la Municipalidad presentó ante la Comisión del Fondo de Asistencia Educativa (FAE), el Mapeo Educativo. Se trata de un sondeo de las escuelas ubicadas en la ciudad, que busca responder a las demandas de las instituciones de manera ágil. El objetivo es optimizar los recursos municipales distribuyendo las tareas de manera organizada.

Para concretar el trabajo, se dividió a la ciudad en ocho zonas, con la intención de contar con un registro fotográfico de cada institución educativa. De este modo, el municipio puede proyectar el desarrollo de las tareas de mantenimiento como corte de césped, poda de árboles, pintura, arreglo de veredas, colocación de alarmas y reparaciones tanto eléctricas como de mobiliario.

La subsecretaria de Gestión Cultural y Educativa municipal, Huaira Basaber, explicó que “el Mapeo Educativo consiste en localizar todas las escuelas de la ciudad para poder establecer cuántas instituciones hay y cuántos complejos educativos funcionan”. De este modo se localizaron 184 complejos educativos (aunque la verificación continúa) y 331 escuelas. Con ese número preciso, comienza a llevarse a cabo “un trabajo ordenado, distribuido en el territorio de manera de poder aprovechar los recursos”, detalló.

Basaber agregó que la intención es “ir a una zona para abarcar todos los complejos educativos y hacer todas allí las refacciones que se necesiten, antes de pasar a la siguiente zona”. Asimismo, mencionó que el mapeo “también permite tener un contacto con todos los directores y, a la vez, un contacto integrador entre los complejos educativos”.

En ese marco, se efectuó también un relevamiento de las 108 instituciones con espacios verdes, que albergan un total de 241 escuelas en 444.000 metros cuadrados.

La subsecretaria insistió en que “todo este trabajo nos permite tener un plano, una foto de cada una de las escuelas que hasta el momento no teníamos. Además, el relevamiento minucioso y metódico quedará como insumo tanto para el municipio como para las mismas escuelas y la Regional IV de Educación de la provincia”.

La Comisión del FAE consideró como muy valioso el trabajo realizado ya que permitirá contar con insumos para conocer la realidad de cada escuela. De este modo, los integrantes se manifestaron satisfechos por este logro, y por el trabajo en conjunto que resultó del aporte de todos los representantes.

**Dónde inscribirse**

Las instituciones que deseen solicitar servicios, obras o trabajos varios deben hacerlo a través de las planillas que se encuentran disponibles en la web del Municipio.

Basaber aprovechó la oportunidad para “hacer un llamado a las escuelas que aún no saben que el Fondo de Asistencia Educativa está funcionando, de modo que puedan hacer la presentación correspondiente, a través del formulario, de cuáles son sus necesidades”.

Vale recordar que el FAE -constituido por la Federación de Cooperadoras Escolares, la Municipalidad de Santa Fe y el Ministerio de Educación de la provincia- tiene como finalidad asegurar el mantenimiento, la ampliación y la refacción de todos los edificios escolares de propiedad provincial, municipal o comunal cuya ejecución no lleve a cabo el Poder Ejecutivo Provincial.

A través de sus acciones, el Fondo contribuye al equipamiento y mantenimiento de las escuelas ubicadas en su jurisdicción. La Dirección de Obras y Asistencia Educativa (DOAE), por su parte, realiza muchas de las tareas de mantenimiento permanente de los establecimientos educativos de la ciudad, con un equipo de personas a cargo.

En el marco del programa Escuelas de mi ciudad, se llevarán adelante las primeras licitaciones para el desarrollo de las obras de puesta en valor de la fachada y la mejora de la accesibilidad en establecimientos de los barrios La Esmeralda y Santa Rosa de Lima.