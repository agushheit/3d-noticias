---
category: La Ciudad
date: 2021-10-30T06:00:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/quilla.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Celia Arena: "Es lógico y sano que se planteen diferencias"'
title: 'Celia Arena: "Es lógico y sano que se planteen diferencias"'
entradilla: Así lo expresó la ministra de Igualdad, Género y Diversidad de Santa Fe,
  tras la polémica por los carteles contra la discriminación y el machismo en El Quillá.

---
"Soy varón y se que el machismo no va más", "soy varón y se que puedo ser sensible", "soy varón y me gusta otro varón", son algunos de los carteles con mensajes que se instalaron en las paredes del club Náutico El Quillá en la ciudad de Santa Fe, a partir de una iniciativa de la dirección del establecimiento deportivo y la municipalidad para desterrar estereotipos de género.

Tras conocerse la acción, la situación despertó la ira de un grupo de padres que expresó "con mis hijos no te metas" y amenazaron a la comisión directiva con desafiliarse del club.

Este viernes por la mañana, en la inauguración del Domo Micaela en la explanada de El Molino, la ministra de Igualdad, Género y Diversidad de Santa Fe, Celia Arena, se refirió al respecto: "Este taller realizado por la municipalidad de Santa Fe que lleva adelantes en distintos espacios educativos, tiene que ver con el objetivos de repensarnos y poder preguntarnos en que mundo vivimos; considero que la acción de instalar dicha cartelería es producto del diálogo y la construcción".

Carteles en El Quillá. La iniciativa surgió de la Secretaría de Integración y Economía Social de la municipalidad de Santa Fe en el marco de una reflexión sobre las masculinidades.

"Es lógico y muy sano que se planteen diferencias porque se trata de poder interpelarnos y de observar que pasa con la presencia de esos carteles. Esto es la puerta y el camino para poder abrir un debate, para que las familias también se proyecten una perspectiva diferente a la de su vida cotidiana y a la de su entorno; esto es una obligación que tenemos los adultos y las adultas", subrayó la ministra de Igualdad, Género y Diversidad de Santa Fe, Celia Arena.

"Tenemos que comprender que cada uno tiene el derecho de elegir como vive y es el Estado el que tiene que proteger esto en todos los sentidos, defender a todos aquellos que tienen una elección diferente a la del resto", continuó agregando la ministra.

"A todo este debate lo veo como algo muy sano porque se abre una puerta para poder construir un consenso diferente, lo que no quiere decir que nadie tenga que cambiar de opinión sin no poder abrirse a ver estas realidades", finalizó Celia Arena respecto al debate que surgió tras la colocación de carteles en el club El Quillá contra la discriminación y el machismo.

**Sobre el Domo Micaela**

Durante los diez días que permanecerá el Domo en la ciudad de Santa Fe, a través de seis experiencias interactivas y lúdicas que fueron diseñadas para personas de todas las edades, el público podrá participar de actividades de descubrimiento, reflexión y acción para encontrar algunas respuestas y muchos nuevos interrogantes.

“Palabras sobre el cuerpo”, “Mitos”, “¿Cerebros sobrecargados?”; “Glosario”, “La calle y la mujeres” y “Microcine por la igualdad” son las seis estaciones pensadas como experiencias de juego, interacción y descubrimiento. Además, en paralelo habrá diferentes talleres a cargo de los equipos del Ministerio de Igualdad, Género y Diversidad.