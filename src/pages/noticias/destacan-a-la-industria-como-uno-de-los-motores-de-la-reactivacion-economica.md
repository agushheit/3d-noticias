---
category: Agenda Ciudadana
date: 2021-09-08T06:00:46-03:00
thumbnail: https://assets.3dnoticias.com.ar/INDUSTRIAUP.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Destacan a la industria como uno de los motores de la reactivación económica
title: Destacan a la industria como uno de los motores de la reactivación económica
entradilla: Cámaras y empresarios pyme ponderaron “el crecimiento sostenido de la
  industria manufacturera”, que se ubica en niveles superiores a los prepandémicos
  y pronosticaron incrementos de la inversión y la demanda.

---
Cámaras y empresarios pyme ponderaron este martes “el crecimiento sostenido de la industria manufacturera”, que se ubica en niveles superiores a los de 2019, y pronosticaron una continuidad en la tendencia positiva con incrementos de la inversión y la demanda.

El presidente de Industriales Pymes Argentinos (IPA), Daniel Rosato, aseguró, en diálogo con Télam, que “el crecimiento viene siendo sostenido y sustentable, en promedio 4% arriba respecto al 2019” y “se ve en los meses venideros una mayor reactivación por la plena reapertura de la economía después de la segunda ola, que producirá un efecto dominó al generar más demanda”.

Las apreciaciones del empresario coinciden con datos del Indec difundidos este martes, que indicaron que en julio el Indicador Sintético de la Actividad de la Construcción (ISAC) creció 2,1% mensual sin estacionalidad, la segunda suba consecutiva, y alcanza un nivel 25,3% superior a la prepandemia, en febrero 2020.

En la comparación interanual, aumentó 19,8%, registrando así la novena suba consecutiva, luego de las fuertes caídas al inicio de la pandemia.

Entre enero y julio, la actividad acumula un crecimiento de 53,3% interanual, superando también en 1,1% al período enero-julio de 2019.

Al respecto, Rosato señaló que “las pymes están produciendo y tomando créditos para adquirir nuevas maquinarias. Es decir, las firmas están invirtiendo para aumentar capacidad de producción, lo que indica una expectativa de que el mercado se va a seguir recomponiendo”.

En relación a la inversión, un informe del Centro de Estudios para la Producción (CEP XXI) del Ministerio de Desarrollo Productivo, consideró que “la mejora de la actividad industrial -junto con la agropecuaria- viene impulsando la inversión productiva”.

En el primer trimestre de 2021, la inversión superó en 14,3% al promedio de 2019, y fue el componente de la demanda agregada de mayor ritmo de crecimiento, según datos oficiales.

En el sector textil, por caso, en los primeros seis meses de 2021 el nivel de producción se incrementó en un 42,5% interanual y en prendas y calzado el número alcanzó el 60,1%, de acuerdo a registros de la Fundación ProTejer.

La utilización de la capacidad instalada en la industria textil pasó del 53,5% en junio del 2019 al 62,1% en igual mes de este año.

Los datos oficiales de empleo también confirman la recuperación: la industria lleva 12 meses consecutivos creando trabajo y hoy tiene 25.000 empleos más que en 2019.

No obstante, en el sector industrial pyme “el empleo no viene al ritmo de la reactivación industrial, y las firmas no están generando mano de obra en términos generales, aunque sí hay ampliación de horarios del personal ya contratado”, explicó el titular de IPA.

Además, Rosato advirtió sobre algunos problemas para conseguir personal calificado en rubros como tecnologías digitales 4.0 y programación y en oficios como electromecánica y tornería.

Según el Indec, en julio la industria creció un 13% interanual pero tuvo una caída del 2,6% respecto al mes de junio

En ese sentido, el empresario pyme indicó que “hay que trabajar mucho para formar gente en esos sectores” y destacó el programa de Empleo Joven lanzado por el Gobierno meses atrás ya que “ayuda a que las pymes tomen y capaciten a chicos sin experiencia. Hay más de 20 mil puestos para ocupar”, concluyó.

En la misma línea se ubica Argentina Programa, un plan del Gobierno que ya capacitó en conocimientos básicos de programación a más de 7.000 jóvenes y pretende formar a 60.000 más hasta fin de año.

El Índice de Producción Industrial (IPI) del Indec informó este martes que en julio la industria creció un 13% interanual pero tuvo una caída del 2,6% respecto al mes de junio.

El CEP XXI, días atrás, adelantó el freno y se lo adjudicó a que “junio había sido extraordinariamente elevado” y también a “lo ocurrido en la industria automotriz, que produjo 8.100 vehículos menos por paradas por vacaciones de invierno y por faltante de insumos (semiconductores) a nivel global”.