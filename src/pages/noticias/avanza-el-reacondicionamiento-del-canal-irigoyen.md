---
category: Estado Real
date: 2021-01-15T04:42:01Z
thumbnail: https://assets.3dnoticias.com.ar/canal-irigoyen.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Avanza  el reacondicionamiento del Canal Irigoyen
title: Avanza  el reacondicionamiento del Canal Irigoyen
entradilla: Las obras permitirán un mejor escurrimiento en las zonas productivas de
  los distritos Bernardo de Irigoyen, San Fabián, Casalegno, Barrancas, Díaz y Monje,
  en el departamento San Jerónimo.

---
El secretario de Recursos Hídricos, Roberto Gioria y el subsecretario de Desarrollo Hídrico y Comités de Cuencas, Mariano Diez, supervisaron junto a los presidentes comunales de la zona, los trabajos que se realizan en las obras de reacondicionamiento del Canal Irigoyen, perteneciente a la cuenca del arroyo Monje, desembocando este a su vez a la Cuenca del río Paraná.

Se trata de una obra de 32,5 km de extensión, que mejorará el escurrimiento de las aguas de seis localidades del departamento San Jerónimo, atravesando las rutas provinciales N°65, N°40-S y N°41-S -de norte a sur- para finalmente desembocar en el arroyo Monje.

Sobre el particular, el secretario de Recursos Hídricos comentó que «durante los períodos de mayores precipitaciones y napas freáticas altas, las zonas atravesadas por el canal se ven afectadas, excediéndose su capacidad, con las consecuentes complicaciones en los campos aledaños».

Agregó que «el reacondicionamiento del canal Irigoyen busca garantizar el desagüe de toda esta zona rural y productiva» y explicó que «los trabajos comprenden la excavación, con maquinaria de la provincia -de un volumen estimado de 120 mil metros cúbicos- y la construcción de once alcantarillas de diversas secciones, coincidentes dos de las mismas, con las rutas provinciales N°40-S y N°41-S».

Gioria adelantó que «son acciones de gran impacto que beneficiarán a vecinos y productores del área, mejorando su calidad de vida y alentando la producción desde las obras de infraestructura prioritarias, y a la vez, generando empleos, tal como nos lo vienen pregonando permanentemente, tanto el gobernador Omar Perotti como la ministra Silvina Frana», concluyó.

Por su parte, Diez puntualizó que «la obra se desarrollará en tres etapas y optimizará el escurrimiento de una vasta zona del departamento San Jerónimo».

Detalló que «en la primera etapa haremos una excavación desde el kilómetro 0 hasta el 11,5, con equipamiento propio, que demandará unos 5 meses de trabajo. Luego, una segunda etapa, que comprenderá el llamado a licitación -durante este año-, para la construcción de seis alcantarillas que serán ubicadas aguas abajo, -empezando desde abajo hacia arriba- estimando una inversión cercana a los 150 millones de pesos. Mientras que la tercera etapa, está prevista para ser concretada hacia fines de este año, a través de un nuevo llamado a licitación para construir las cinco alcantarillas restantes. Estas dos últimas etapas también incluyen movimiento de suelos», finalizó.