---
category: Agenda Ciudadana
date: 2021-02-08T08:07:41-03:00
thumbnail: https://assets.3dnoticias.com.ar/trotta.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Clarín
resumen: 'Nicolás Trotta habló de los objetivos del Gobierno con las clases: “La mitad
  de los días en la escuela y la otra mitad en casa”'
title: 'Nicolás Trotta habló de los objetivos del Gobierno con las clases: “La mitad
  de los días en la escuela y la otra mitad en casa”'
entradilla: El ministro de Educación explicó los dos modelos en los que trabajan para
  lograrlo y ratificó que “dependerá de cada jurisdicción”.

---
Con la vuelta a clases en cuenta regresiva, el Ministro de Educación de la Nación, Nicolás Trotta, dejó en claro la postura gubernamental con respecto al tema: "Para nosotros, la media a aplicar sería la mitad de los días en la escuela y la otra mitad en casa", dijo.

Además, admitió que dicho arranque será desde el 17 de febrero y "de manera escalonada", según la jurisdicción: "Hay escuelas que podrán tener un regreso pleno que son las rurales porque tienen espacio de sobra y se pueden respetar los protocolos establecidos", explicó.

"La mayoría de nuestras escuelas indica que una parte tendrá presencialidad y otra no -continuó el funcionario nacional-. Y habrá alternancia entre una y otra para tener una presencialidad cuidada. Este es el sistema con el que vamos a tener que convivir hasta que superemos la pandemia. Pero eso lo definirá cada jurisdicción", aclaró en declaraciones al canal de noticias A24.

El ministro de Educación especificó que "hay dos modelos en los que se irá trabajando: uno, de bloques semanales que es el que definieron Santa Fe y Córdoba. Es decir, una semana en la escuela y la siguiente en el hogar; con un proyecto de tareas a realizar durante la semana que se está en la casa. Y hay otro modelo que plantea la alternancia, diariamente".

"Las 24 jurisdicciones educativas son las que tienen que decidir qué hacer en cada momento y dependerá de la situación sanitaria y también de las comodidades con que cuente cada establecimiento. No se puede tomar a Argentina como un todo", indicó.

Se sabe que algunos colegios ya diseñan las burbujas y planean estrategias. Otros, sólo informaron las fechas de inicio del ciclo lectivo, pero no se precisó sobre cómo será la organización. El 17 comenzará en la Ciudad de Buenos Aires, Jujuy, Santiago del Estero y Santa Fe. Sin embargo, hay una gran porción del país que tendrá su inicio a principios de marzo.

Trotta se encuentra en situación de aislamiento por haber tenido en una de sus reuniones, contacto estrecho con un caso positivo. Y aunque el primer hisopado arrojó "negativo", el funcionario está a la espera del resultado de un segundo testeo para retomar su agenda.

"Nos quedan reuniones importantes aún como para definir los sistemas. Tuve que posponer reuniones en La Pampa y con Axel Kicillof en la Provincia de Buenos Aires que se reprogramaron para la semana que viene. Esto sucederá si el segundo testeo da negativo. Lo propio, para reunirme este lunes con Rodríguez Larreta para Capital Federal", confirmó.