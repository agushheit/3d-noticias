---
category: Deportes
date: 2020-12-14T11:59:07Z
thumbnail: https://assets.3dnoticias.com.ar/unión.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Diario UNO'
resumen: Unión tocó fondo ante el muleto de Defensa
title: Unión tocó fondo ante el muleto de Defensa
entradilla: Unión perdió ante Defensa por 3-1, en el 15 de Abril. Gabriel Hachén,
  Washington Camacho y Miguel Merentiel anotaron los goles. Descontó Franco Troyansky.

---
Unión arrancó su participación en la zona Complementación A de la Copa Diego Maradona recibiendo a Defensa y Justicia, que preservó a los titulares para el desquite ante Bahía de Brasil por los cuartos de final de la Copa Sudamericana. 

El Tate estiró su mal momento tras caer por 3-1 en el estadio 15 de Abril, cotejo que contó con arbitraje de Nazareno Arasa. Gabriel Hachen (PT 2') y Washington Camacho (PT 22') y Miguel Merentiel (ST 16') anotaron para el Halcón. Descontó Franco Troyansky (PT 41').

Unión arrancó con la intención de hacerse cargo del partido y luego de una aproximación por la izquierda llegó un contragolpe donde la pelota le quedó por el centro a Gabriel Hachen, quien no recibió la cobertura a tiempo de Brian Blasi, y desde afuera del área sacó un latigazo que hizo imposible la estirada de Sebastián Moyano. Baldazo de agua fría y al igual que en el cotejo ante Atlético de Rafaela el equipo recibió un duro golpe en el amanecer del partido.

En los minutos siguientes a Unión le costó recuperarse de ese golpe, aunque nunca perdió su compostura táctica, que es la que viene disponiendo Juan Azconzábal desde que llegó, con el 4-3-3. Con el tanto a favor Defensa aprovechó el envión, se adueñó de la pelota, mientras que el Tatengue arrimó peligro por derecha con Javier Cabrera, aunque sin encontrar un destinatario en el área.

El Tate la tuvo con un remate desde afuera del área de Claudio Corvalán, tras una trepada de Cabrera por derecha, y la pelota se fue muy cerca del palo izquierdo de Marcos Ledesma. A los 15 probó Rodrigo Herrera desde afuera del área, y Moyano esta vez respondió con mucha seguridad. Unión no salía del desconcierto y no podía superar el golazo que significó el gol de Hachen, cuando todavía no se había acomodado en el partido.

A los 21 la tuvo Washington Camacho con un remate desde afuera del área que generó una gran reacción de Sebastián Moyano para mandar la pelota al córner. Sin embargo, de ese tiro de esquina llegó el segundo de Defensa, con un cabezazo implacable del uruguayo, para hacer estéril la estirada de Moyano.

Unión estaba para el golpe de nocaut, que estuvo a punto de dárselo Defensa con una gran jugada por derecha, la pelota llegó al segundo palo donde Hachen se perdió el tercero solo frente a Moyano. En la siguiente Nazareno Arasa se comió un claro penal que le cometieron a Fernando Elizari cuando se perfilaba para rematar.

Fue entonces que llegaron los mejores minutos de Unión en el primer tiempo, ya que a los 39 estuvo a punto de descontar, tras un tiro de esquina que llegó desde la derecha, García la peinó en el primer palo y Matías Nani solo ingresando al área chica remató fuerte y se perdió increíblemente el descuento. En la siguiente la tuvo García, pero su remate terminó en las manos del arquero Ledesma, mientras que luego nuevamente el goleador la tuvo tras un centro desde la derecha, pero la mandó muy por arriba del arco de Defensa.

Hasta que a los 41' lo encontró Unión, con una gran jugada de Fernando Elizari, quien habilitó a Troyansky para que marque el descuento, con el cual se terminaron yendo al descanso. El equipo de Azconzábal jugó uno de los peores primeros tiempos de la temporada, ya que un equipo plagado de suplentes de Defensa desnudó todas sus fragilidades defensivas, las cuales podría haber aprovechado más todavía aunque no lo pudo hacer por algunas intervenciones de Moyano y en otras por mala puntería de sus delanteros.

En el arranque del complemento Unión estuvo muy cerca de marcar el empate, tras una gran contra que agarró mal parada a la defensa del Halcón. Sin embargo, luego de una gran habilitación de Fernando Elizari a Javier Cabrera, el uruguayo se demoró en rematar y cuando lo hizo encontró el bloqueo de Nahuel Gallardo quien se inmoló para evitar la caída de su arco.

Unión se mostraba un poco más seguro en el fondo e iba con todo en busca del empate, ante un Defensa que tampoco resignaba su vocación ofensiva, con lo cual el partido se hacía de ida y vuelta. Hasta que a los 16', tras una gran jugada, donde se movió la pelota en la zona de volantes de derecha a izquierda, la pelota le llegó a Camacho, quien habilitó a Miguel Merentiel y con un latigazo sentenció a Moyano.

Juan Azconzábal comenzó a mover el banco, y fue así que mandó al campo de juego a Gastón Comas, Ezequiel Cañete y Gabriel Carabajal, aunque Unión no pudo cambiar la ecuación del juego. En tanto que también apostó sobre la parte final con Kevin Zenón y Fernando Márquez, buscando la heroica, pero a esa altura del partido Defensa a pesar de no haber resignado su vocación ofensiva ya tenía prácticamente cerrada la victoria, más por impotencia de su rival que por virtudes propias.

Los últimos minutos, a pesar de seguir yendo, con más ganas que ideas, estuvieron de más, ya que Unión luego del gol de Merentiel se había dado por vencido. De esta manera, el Tate jugó uno de sus peores partidos de la temporada, sumó su segunda derrota al hilo en el 15 de Abril (la tercera en serie en la Copa Diego Maradona), profundizó su crisis deportiva y más que nunca todas las miradas apuntan a Juan Azconzábal y los dirigentes.

***

# **Síntesis**

🔴⚪ **Unión (1)**: 1-Sebastián Moyano; 36-Federico Vera, 17-Brian Blasi, 6-Matías Nani y 3-Claudio Corvalán; 40-Leonel Bucca, 30-Juan Nardoni y 18-Fernando Elizari; 7-Javier Cabrera, 11-Juan Manuel García y 22-Franco Troyansky. DT: Juan Manuel Azconzábal.

🟢🟡 **Defensa y Justicia (3**): 1-Marcos Ledesma; 14-Pedro Ramírez, 25-Néstor Breitenbruch, 13-Rodrigo Herrera y 33-Nahuel Gallardo; 11-Washington Camacho, 19-Gabriel Hachen, 16-Tomás Escalante y 32-Maximiliano Luayza Koot; 7-Nicolás González y 9-Miguel Merentiel. DT: Hernán Crespo.

**Gol:** PT 2' Gabriel Hachén (D), 22' Washington Camacho, de cabeza (U) y 41' Franco Troyansky (U). ST 16' Miguel Merentiel (D).

**Cambios:** ST 20' Gabriel Carabajal x Elizari (U), Ezequiel Cañete x Bucca (U) y Gastón Compas x Nardoni (U); 29' Juan Cruz Villagra x Hachen (D); 33' Jonathan Farías x Luaysa (D); 35' Fernando Márquez x García (U) y Kevin Zenón x Corvalán (U); 38' Julio González x Sienra (D) y Nicolás Leguizamón x Merentiel (D).

**Amonestados:** García, Blasi y Corvalán (U).

**Árbitro:** Nazareno Arasa.

**Estadio:** 15 de Abril.

***

  
 