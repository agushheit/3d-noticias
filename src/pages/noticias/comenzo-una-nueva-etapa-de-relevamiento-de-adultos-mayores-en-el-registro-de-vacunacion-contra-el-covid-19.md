---
category: Agenda Ciudadana
date: 2021-04-06T06:27:20-03:00
thumbnail: https://assets.3dnoticias.com.ar/covid.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: Comenzó una nueva etapa de relevamiento de adultos mayores en el registro
  de vacunación contra el Covid-19
title: Comenzó una nueva etapa de relevamiento de adultos mayores en el registro de
  vacunación contra el Covid-19
entradilla: El operativo se realiza en la ciudad de Santa Fe junto con organizaciones
  sociales.

---
Los ministerios de Salud y Desarrollo Social provinciales continuaron esta mañana en la plaza “Arenales” (Padre Quiroga 2200) del barrio Santa Rosa de Lima de la ciudad de Santa Fe con el relevamiento de personas mayores que no se han podido inscribir en el registro provincial para ser vacunados contra el Covid-19.

Al respecto, el director de la Región de Salud Santa Fe, Rodolfo Roselli, precisó que “en la ciudad contamos con 57 barrios populares y desde hace unos días comenzamos con el rastreo de estas personas, detectando adultos mayores +80 y +90 con diferentes problemáticas ya sea por una dificultad de acceso a la tecnología o por no contar con alguna persona que los asista”.

Asimismo, recordó que “los equipos que salen a realizar la actividad recibieron capacitación previa para realizar el relevamiento de adultos mayores que no han podido registrarse en la web provincial y por lo tanto no han logrado inscribirse en el formulario digital para la vacunación contra el Covid”.

“En los próximos días, se procederá a realizar la vacunación en el territorio, es decir que estas personas no se trasladarán a los vacunatorios sino que teniendo la disponibilidad de vacunas se procederá con una logística que permitirá la inoculación domiciliaria”, concluyó Roselli.

Por su parte, la directora provincial de Personas Mayores, Lucía Billoud, resaltó que “gracias a la articulación entre ambos ministerios, podemos acercarnos a cada vecino y vecina a través de las organizaciones sociales que son indispensable en esta tarea donde nuestro propósito es cumplir con la vacunación de todas las personas mayores”.

**EL ROL DE LAS ORGANIZACIONES SOCIALES**

Teniendo en cuenta este contexto, la Región de Salud Santa Fe se reunió con Organizaciones Sociales que trabajan en distintos barrios populares de la ciudad de Santa Fe. De esta forma, se llevará adelante un relevamiento “casa por casa” con el objetivo de detectar personas que han quedado excluidas de la inscripción a la vacuna por cualquiera de los motivos antes mencionados.

Entre las organizaciones sociales participantes, se encuentran CCC, EVITA, MOB, MTE, MTL, MUP, Nuestra América y Arroyito Seco; las cuales pusieron a disposición alrededor de 50 relevadores por organización llegando a un total de 400.

Al respecto, el representante del Movimiento de Organizaciones Barriales, Rubén Sala, explicó que “desde las organizaciones nos encontramos con adultos que no podían acceder al registro de vacunación ni quien les reciba el turno una vez inscriptos, rápidamente nos pudimos organizar y lo estamos haciendo con el acompañamiento del Estado provincial”.

“Al momento de ir a vacunar a los domicilios, nosotros los vamos a estar acompañando ya que conocemos a cada uno de las personas registradas y esa es la idea que llevó a cabo el Ministerio de Salud al haber convocado a las organizaciones de la unión de la economía popular”, finalizó Sala.