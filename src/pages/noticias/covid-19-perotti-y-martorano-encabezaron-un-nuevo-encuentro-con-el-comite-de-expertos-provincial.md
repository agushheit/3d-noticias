---
category: Agenda Ciudadana
date: 2021-09-05T06:00:45-03:00
thumbnail: https://assets.3dnoticias.com.ar/COMITÉ.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: 'Covid-19: Perotti y Martorano encabezaron un nuevo encuentro con el comité
  de expertos provincial'
title: 'Covid-19: Perotti y Martorano encabezaron un nuevo encuentro con el comité
  de expertos provincial'
entradilla: '"Hay 2.300.000 santafesinos y santafesinas vacunados con una dosis; y
  1.600.000 con dos dosis. Esto habla a las claras del compromiso de la población",
  afirmó el gobernador.'

---
El gobernador Omar Perotti y la ministra de Salud, Sonia Martorano, encabezaron este viernes un nuevo encuentro de trabajo con el Comité de Expertos provincial, para analizar la situación epidemiológica actual, el plan de vacunación y posibles nuevas habilitaciones.

Luego del encuentro, Perotti explicó: “Terminamos una nueva reunión con el grupo de expertos que nos viene acompañando desde el inicio de la pandemia y los directores de nuestros hospitales. Hoy evaluamos la situación epidemiológica, que tiene que ver con el nivel de casos y el número de camas, afortunadamente en descenso, con la prevalencia de las distintas cepas y el cuidado a tener sobre la variante Delta, de la que se siguen monitoreando los diez casos que tuvo la provincia", dijo.

Además, el gobernador destacó que "no hay que dejar de tener una comunicación importante con nuestra población acerca de los cuidados que tenemos que seguir manteniendo. Esto es fundamental, a la par de algo que viene mostrando muy buenos resultados, como es el proceso de vacunación", remarcó.

Perotti sostuvo que "hay 2.300.000 santafesinos y santafesinas vacunados con una dosis; y 1.600.000 con dos dosis. Eso habla a las claras del compromiso de la población, porque más del 90% manifestó su deseo de ser vacunado. Eso nos lleva a tener un altísimo porcentaje ya con una dosis y un nivel de vacunación que nos tiene que llevar a fines de septiembre con el desafío de estar superando las 2.000.000 de personas con el ciclo completo", explicó el gobernador.

Y agregó: "En el mundo, la tendencia está marcando que donde el porcentaje de vacunación es alto, el nivel de hospitalización es más bajo. Por eso, no nos tiene que quedar ningún santafesino ni ninguna santafesina sin inscribirse para vacunarse", afirmó Perotti.

Además, expresó que "el proceso ya lleva también a tener vacunados a todos los menores de 12 a 18 años que manifestaron el interés de hacerlo, teniendo enfermedades prevalentes o con discapacidad. Estamos esperando la llegada de Pfizer para comenzar con los menores de 18 años que no tienen ninguna enfermedad prevalente".

Por último, el gobernador agradeció "a todos los integrantes del Comité de Expertos, quienes nos vienen acompañando con sus sugerencias y aportes. En los próximos días seguiremos evaluando la situación de la provincia", anunció.

**Consulta permanente**

Por su parte, la ministra de Salud, Sonia Martorano, describió que "fue un muy buen balance. El Comité de Expertos es una herramienta de consulta permanente durante toda la pandemia, que sigue en funcionamiento, que nos sirve para validar acciones y para generar los consensos necesarios en momentos tan dificultosos como estos, que nos tocó atravesar".

"Hoy presentamos un informe sobre la situación epidemiológica actual, sobre la situación de vacunación y las preguntas que surgen sobre cómo puede impactar la llegada de la variante Delta, si es que llega; cómo puede llegar a ser una tercera ola y comenzamos a conversar sobre lo que puede llegar a ser un pase sanitario y nuevas habilitaciones, como viajes de egresados”, agregó la ministra Martorano.

En ese marco, destacó que "todos pusieron de relieve el trabajo hecho por la provincia de Santa Fe, con un 99% de las vacunas recibidas colocadas, ya próximos a llegar al 50% de esquemas completos de vacunación en la población total y un 70% en la población objetivo. Fue algo muy valorado por todos, esto es una construcción colectiva”, indicó la funcionaria.

Martorano destacó, por último, que en el territorio santafesino el 90% de la población adhirió al plan provincial de vacunación. Somos la provincia que más personas tiene inscriptas con su deseo y su consentimiento de vacunarse. La salida de esta pandemia es la vacunación, más los cuidados necesarios", subrayó.

**Datos de vacunación actualizados**

**Santa Fe (provincia)**

2\.341.884 (89,50%) población mayor de 18 años vacunada con 1 dosis

1\.589.143 (60,73%) población mayor de 18 años vacunada con 2 dosis

66,53% total de la población con 1 dosis (2.370.578)

44,93% total de la población con 2 dosis (1.600.932)

**Santa Fe (ciudad)**

296\.566 (92,18%) población mayor de 18 años vacunada con 1 dosis

214\.437 (66,65%) población mayor de 18 años vacunada con 2 dosis

68,46% total de la población con 1 dosis (299.923)

49,44% total de la población con 2 dosis (216.596)

**Rosario**

737\.247 (97,38%) población mayor de 18 años vacunada con 1 dosis

514\.975 (68,02%) población mayor de 18 años vacunada con 2 dosis

72,21% total de la población con 1 dosis (744.451)

50,31% total de la población con 2 dosis (518.637)