---
category: Estado Real
date: 2020-12-22T10:37:24Z
thumbnail: https://assets.3dnoticias.com.ar/2212assa.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: ASSA abrió los sobres para ejecutar nueva infraestructura cloacal en la ciudad
  de Santa Fe
title: ASSA abrió los sobres para ejecutar nueva infraestructura cloacal en la ciudad
  de Santa Fe
entradilla: Las obras beneficiarán a más de 43 mil vecinos y vecinas, y demandarán
  una inversión de 200 millones de pesos.

---
El Gobierno de Santa Fe, a través de Aguas Santafesinas SA, realizó el acto de apertura de sobres de la licitación correspondientes a obras de infraestructura básica que posibilitarán futuras ampliaciones del servicio cloacal de la ciudad de Santa Fe.

Las obras en licitación beneficiarán a más de 43.000 vecinos y vecinas; y demandarán una inversión del Estado Nacional a través de fondos del Ente Nacional de Obras Hídricas de Saneamiento (ENOHSA) estimada en $ 200 millones.

El acto estuvo encabezado por la ministra de Infraestructura, Servicios Públicos y Hábitat, Silvina Frana; el secretario de Servicios y Empresas Públicas, Carlos Maina; el intendente de Santa Fe, Emilio Jatón; y el presidente del Directorio de Aguas, Hugo Morzán.

Estas obras forman parte del convenio firmado por el gobernador Omar Perotti y el ministro de Obras Públicas de la Nación, Gabriel Katopodis, para realizar trabajos de acceso al agua potable y saneamiento cloacal en nueve ciudades de la provincia, con un presupuesto de más de 1.450 millones de pesos.

## **DIGNIDAD PARA LA GENTE**

Al respecto, la ministra Frana indicó que estas obras «tienen que ver, como dice el gobernador de la Provincia, Omar Perotti, con generar condiciones saludables de vida. Cuando llega el agua potable, cuando llegan las cloacas, se generan las condiciones de vida saludable».

Frana también mencionó que licitaciones como estas «se desarrollan a lo largo y ancho de la provincia. La semana pasada estuvimos en Santo Tomé, no se distinguen colores políticos porque estamos convencidos de que cuando hablamos de la dignidad de la gente los acuerdos con las diferentes fuerzas políticas tienen que llegar».

Asimismo, hizo referencia a que «ustedes saben que yo soy de Santa Fe y me parece mentira estar anunciando obras que hace años la ciudad no tenía. La intendenta de Esperanza dijo que hace 12 años que esperaban obras como estas».

«Ojalá podamos seguir con este ritmo y cuando nos vayamos de la gestión sepamos que hay más dignidad de vida de la gente», concluyó Frana.

En tanto, Morzán destacó que «son obras fundamentales para continuar con la expansión de redes cloacales y el desarrollo urbano de la ciudad en dos sectores que aún no disponen de estos servicios, el noroeste y el noreste»; y puso de relieve la coordinación y celeridad con que las diferentes áreas de gobierno y el personal de la empresa desarrollaron los proyectos «entendiendo que formamos parte de un Estado que tiene que dar respuesta a la gente».

Por su parte, Jatón subrayó que «es un buen día para la ciudad de Santa Fe, es de esos días que se siente que se están haciendo las cosas bien. Por primera vez se pone el foco en el sector Noroeste de la ciudad que no tiene cloacas».

## **LAS OBRAS**

Por un lado, incluye obras de infraestructura básica para expansiones del servicio cloacal en los barrios Ciudadela, Schneider, Piquete Las Flores y San Martín.

Las obras incluyen el tendido de 2.300 metros de nuevas cañerías, una estación elevadora completa con todo el equipamiento electromecánico, automatismos y sistemas de telegestión, como también la construcción de una cañería de impulsión para el transporte de los efluentes desde dicha instalación.

Para los trabajos se presentaron tres ofertas. La primera de Mundo Construcciones SRL, que cotizó por $ 65.246.938,57 + IVA; la segunda de Winkelmann SRL, por $ 83.297.406,19 + IVA; y la tercera de Dinale SA, por $ 66.576.799,69 + IVA.

Por otra parte, se planificó una obra denominada «Colector Noreste» que permitirá admitir futuras expansiones de redes cloacales en dicho sector de la ciudad de Santa Fe.

Consiste en el tendido de más de 1.800 metros de cañerías, la ejecución de 15 bocas de registro y dos cámaras especiales de hormigón armado para el acceso.

Se presentaron tres ofertas. La primera de Mundo Construcciones SRL, por $ 66.258.264,12 + IVA; la segunda de Winkelmann SRL, por $ 76.998.611,46 + IVA; y la tercera de Dinale SA, por $ 87.068.906,30 + IVA.