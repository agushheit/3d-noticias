---
category: Agenda Ciudadana
date: 2021-07-26T06:00:55-03:00
thumbnail: https://assets.3dnoticias.com.ar/paso.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Unas PASO signadas por el consenso en el Frente de Todos y competencia abierta
  en Juntos por el Cambio
title: Unas PASO signadas por el consenso en el Frente de Todos y competencia abierta
  en Juntos por el Cambio
entradilla: Los comicios que se celebrarán el próximo 12 de septiembre determinarán
  los nombres de los candidatos que se postularán para las 127 bancas en disputa en
  la Cámara baja y para las 24 del Senado

---
Las elecciones de medio término definirán el 14 de noviembre la renovación de casi la mitad de la Cámara de Diputados y un tercio del Senado, tras pasar el test nacional de las PASO del 12 septiembre al que, con la oficialización de las precandidaturas en las últimas horas, el Frente de Todos llegará con acuerdos de consenso y Juntos por el Cambio con una competencia abierta en la mayoría de los distritos.

Lo que suceda en las primarias, por lo tanto, no solo determinará los nombres de los candidatos que se postularán para las 127 bancas en disputa en la Cámara baja (este año se renovarán 127 del total de 257) y para los 24 del Senado sino que también redefinirá los equilibrios internos y la conducción futura de JxC, ya que en esa coalición opositora se abrió una puja entre el PRO y la UCR en una cantidad significativa de provincias.

Una vez que se conozcan los ganadores de la competencia dentro de cada alianza o frente político, el 12 de septiembre por la noche, llegará el momento de la pelea de fondo: la puja de cada coalición por ampliar la cantidad de bancas propias.

El resultado definitivo del 14 de noviembre incidirá sobre el ejercicio legislativo de reunir quórums (en el caso de Diputados, 129 legisladores) y mayorías para aprobar leyes -o para tratar de impedirlo, en el caso de la oposición-, tópico que desde el inicio del gobierno del FdT estuvo en el centro de los análisis de la Cámara baja ya que en ese cuerpo el oficialismo cuenta con 119 legisladores, 10 menos que el quórum.

En el Senado, donde hoy cuenta con 41 sobre el total de 72, la coalición oficial está menos urgida pero igualmente debe renovar 15 bancas sobre 24 que se ponen en juego, correspondientes a Catamarca, Chubut, Córdoba, Corrientes, La Pampa, Mendoza, Santa Fe y Tucumán.

Mientras la campaña se perfila hacia la definición en las urnas de quiénes serán finalmente los candidatos de cada fuerza, unos cuarenta diputados nacionales con mandatos próximos a vencer -culminan en diciembre de 2021- se encuentran ante el desafío de obtener su reelección en la renovación parlamentaria del 14 de noviembre.

Algunos deberán primero atravesar el filtro de la competencia interna en las PASO, porque en algunas jurisdicciones se presentarán dos, tres y hasta cuatro listas dentro de cada conglomerado de partidos: este escenario de tensión, suspenso y puja de estructuras territoriales se repite especialmente en JxC.

Por ejemplo, uno de los detalles de la elección es que el presidente de la UCR, el exgobernador de Mendoza y actual diputado Alfredo Cornejo, competirá como precandidato a senador nacional por su provincia y para eso deberá ganar las primarias locales.

En el caso del FdT, que en Diputados conduce Máximo Kirchner, el objetivo es sumar unos 10 diputados más en todo el país para finalmente alcanzar el quórum propio de 129 legisladores y ya no depender de los acuerdos circunstanciales con los bloques provinciales.

**Provincia de Buenos Aires**

El FdT, que en provincia de Buenos Aires lleva como primeros candidatos a Victoria Tolosa Paz y Daniel Gollan, propone la reelección de Leopoldo Moreau, a quien se le vence su actual mandato en diciembre.

Moreau, con origen en el radicalismo y reconocido como un gran orador por propios y extraños, está al frente de la Comisión Bicameral de Fiscalización de Organismos y Actividades de Inteligencia, de mucha actividad en el último año.

El oficialismo también impulsa la reelección de los titulares de las comisiones de Legislación del Trabajo, Vanesa Siley; de Derechos Humanos, el gremialista Hugo Yasky; y de Mujeres y Diversidad, Mónica Macha.

También postula para un nuevo mandato al gremialista Walter Correa y Claudia Bernazza, aunque ellos dos, al igual que Macha, no están en los primeros lugares de la lista y una renovación de mandato asoma compleja en todos estos casos, aunque el FdT gobierna y a veces algunos legisladores electos pueden ser convocados a una función ejecutiva y eso promover a los candidatos que estaban más atrás.

En tanto, el escenario para los diputados nacionales de JxC de la provincia de Buenos Aires con mandatos próximos a concluir incluye una dificultad adicional, porque tendrán que competir en las PASO en una puja muy competitiva en la que participarán dos listas: una encabezada por Diego Santilli, del PRO, y otra por el medico neurocirujano Facundo Manes, de la UCR.

Los legisladores de JxC que buscan un nuevo mandato de la lista de Santilli son la macrista Graciela Ocaña; el titular del bloque de la Coalición Cívica, Juan Manuel López, y su par de bancada, la vicepresidenta de la comisión de Legislación Penal, Marcela Campagnoli.

Los diputados de la UCR que quieren su reelección en la lista impulsada por Manes son los radicales Fabio Quetlas y la joven dirigente Josefina Mendoza.

**Ciudad de Buenos Aires**

En la Ciudad de Buenos Aires, los diputados del FdT que tendrán la chance de competir por su reelección son la periodista Gisela Marziotta y el titular de la comisión de Presupuesto, el economista Carlos Heller, convertido en el último año y medio en unas de las principales espadas económicas del oficialismo además de impulsor del proyecto de Aporte Solidario a las Grandes Fortunas.

Desde el universo de JxC, que en la CABA presentará tres listas, la nómina que lleva como primera candidata a la exgobernadora María Eugenia Vidal impulsará la reelección de la diputada de la Coalición Cívica Paula Oliveto, del macrista Fernando Iglesias y de la radical Carla Carrizo, muy cercana a Martín Lousteau.

Otro diputado en funciones, pero con mandato próximo a vencer, el radical Facundo Suárez Lastra, intentará su reelección desde la lista de JxC que promueve un sector del radicalismo y que postula al exministro y secretario de Salud Adolfo Rubinstein.

**Córdoba**

En Córdoba, otro distrito donde se espera una PASO muy disputada dentro de JxC, la lista que impulsa como senador a Luis Juez empujará la reelección del exárbitro de fútbol Héctor Baldassi mientras que la nómina rival que encabeza como precandidato al Senado al titular del interbloque en Diputados Mario Negri, propone un nuevo mandato para la diputada radical Soledad Carrizo.

Los diputados nacionales que pueden llegar a tener la chance de reelegir son alrededor de cuarenta, proceden de distintas provincias, integran distintas fuerzas y tendrán distintas posibilidades; algunos deberán esperar el resultado de las primarias para ver si estarán en condiciones de competir el 14 de noviembre.

En cualquier caso, lo individual estará atado a la definición de cosas más importantes, como lo que suceda en la disputa por las candidaturas en los distritos dentro de cada coalición, sobre todo en JxC.

Se trata, en el caso de la coalición opositora, de un primer paso que ya se perfila hacia una puja abierta y cada vez más desencadenada por la jefatura de la alianza y por las fórmulas de 2023.