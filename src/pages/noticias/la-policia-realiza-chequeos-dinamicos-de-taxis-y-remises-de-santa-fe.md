---
category: La Ciudad
date: 2021-11-20T06:00:41-03:00
thumbnail: https://assets.3dnoticias.com.ar/chequeo.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La policia realiza chequeos dinámicos de taxis y remises de Santa Fe
title: La policia realiza chequeos dinámicos de taxis y remises de Santa Fe
entradilla: En las últimas semanas fueron identificadas 899 personas y 1609 vehículos.

---
A raíz de una reunión que mantuvo el ministro de Seguridad, Jorge Lagna, con los sindicatos de taxistas y remiseros de la ciudad de Santa Fe, la Policía provincial implementó chequeos dinámicos a remises y taxis con el objetivo de prevenir hechos delictivos. La medida comenzó el 5 de noviembre y se realiza en distintos puntos de la ciudad de Santa Fe, con controles aleatorios a choferes y pasajeros de taxis y remises.

Los operativos están a cargo de la Unidad Regional I, a cargo de Martín García, y se desarrollan las 24 horas, con especial énfasis en horarios de 14 a 18 horas y de las 00 a 06 horas. En las últimas semanas fueron identificadas 899 personas y 1609 vehículos.