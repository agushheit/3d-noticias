---
category: Agenda Ciudadana
date: 2021-03-02T05:40:33-03:00
thumbnail: https://assets.3dnoticias.com.ar/progresar.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa ANSES
resumen: 'ANSES: comenzó la inscripción a las Becas Progrear de hasta $ 14.000'
title: 'ANSES: comenzó la inscripción a las Becas Progrear de hasta $ 14.000'
entradilla: "El monto otorgado por el Ministerio de Educación difiere según la carrera
  elegida y el año en el que se encuentre cursando cada alumno\n\n"

---
A partir de este lunes 1 de marzo comienza la inscripción para acceder a las Becas Progresar 2021 que otorga el Ministerio de Educación destinado a jóvenes que deseen finalizar sus estudios primarios, secundarios y nivel universitario.

Los interesados tendrán tiempo para inscribirse hasta el 31 de marzo, y se entregarán en total 750.000 becas.

 

**¿Cuál es el monto de las Becas Progresar?** 

El monto mínimo es de $ 2.100 y el máximo de $ 14.000. Sin embargo, se cobra el 80% y si se cumple con los objetivos, cobra el 20% acumulado de cada mes a fin de año. Se entrega durante 10 meses, es decir, sólo se paga durante el ciclo lectivo. Los valores varían según la carrera elegida y el año en el que se encuentre cada alumno. 

Perciben el monto mayor, alumnos que estén por finalizar carreras consideradas estratégicas: aquellas relacionadas con alimentos, el ambiente, la computación e informática, la energía, el gas, la minería, la movilidad, el transporte, el petróleo, entre otras.

 

**Becas Progresar: Requisitos**

* La edad requerida para solicitar la prestación es de 18 a 24 años
* Se extiende a los 30 dependiendo del grado de avance académico.
* Madres solteras se extendió el tope de edad a los 30 años
* No hay un límite para las personas trans y travestis.
* la suma de los ingresos de su grupo familiar no debe superar los tres salarios mínimos.

 

**Becas.progresar inscripción**

**Opción 1**

Ingresar a: [https://progresar.educacion.gob.ar/](https://progresar.educacion.gob.ar/ "https://progresar.educacion.gob.ar/")

Se deberá seleccionar si la solicitud es para:

Inscripción Progresar Nivel Superior

Nivel Obligatorio

Para Instituciones

 

Una vez ingresado los datos, el solicitante deberá esperar a que el Ministerio de Educación realice una evaluación para conocer si cumple con los requisitos.

Dentro de los 60 días hábiles, los inscriptos serán informados si lograron acceder a las Becas Progresar.

 

**Opción 2**

Ingresar desde Mi Anses, seleccionar la pestaña "Becas Progresar", ingresar CUIL y clave personal de la Seguridad Social.

Descargar e imprimir el Formulario de Inscripción.

El instituto donde se esté estudiando debe completar y firmar la sección 2 del formulario "Datos de Educación".

Finalmente deberán subirlo a Anses desde [https://servicioscorp.anses.gob.ar/clavelogon/logon.aspx?system=progresar2](https://servicioscorp.anses.gob.ar/clavelogon/logon.aspx?system=progresar2 "https://servicioscorp.anses.gob.ar/clavelogon/logon.aspx?system=progresar2")

 

**Ampliación de las Becas Progresar**

Durante este mediodía, en la Apertura de sesiones ordinarias 2021, el presidente Alberto Fernández resaltó el aumento presupuestario de 173% para las Becas Progresar, con las que se ampliará "más de 50% el número de estudiantes beneficiados". Dicho incremento, había sido adelantado por el ministro de Educación, Nicolás Trotta, quién adelantó que el número de beneficiarios del programa pasarán de 550 mil a 750 mil alumnos. 

"Habrá una actualización del monto que se cobra, que será anunciado por el Presidente, y también un aumento de la cobertura, que llegará a 750 mil beneficiarios. Es un derecho y un acompañamiento a la trayectoria educativa de todos estos jóvenes", remarcó el ministro.