---
category: Agenda Ciudadana
date: 2021-12-19T06:00:47-03:00
thumbnail: https://assets.3dnoticias.com.ar/pj.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: Con la presencia de Alberto Fernández, Máximo Kirchner asumió la presidencia
  del PJ bonaerense
title: Con la presencia de Alberto Fernández, Máximo Kirchner asumió la presidencia
  del PJ bonaerense
entradilla: '"Quiero agradecerle a cada uno de los compañeros esta responsabilidad
  de llevar adelante al justicialismo de la provincia de Buenos Aires", enfatizó el
  jefe del bloque del Frente de Todos en la Cámara de Diputados.

'

---
El jefe del bloque del Frente de Todos (FdT) en la Cámara de Diputados, Máximo Kirchner, asumió este sábado la presidencia del Partido Justicialista (PJ) bonaerense, a horas de haber sido apuntado por dirigentes de la oposición como el culpable de que fracasaran las negociaciones para aprobar el Presupuesto 2022.

"Quiero agradecerle a cada uno de los compañeros esta responsabilidad de llevar adelante al justicialismo de la provincia de Buenos Aires", enfatizó Kirchner durante el acto que se desarrolló en la Quinta de San Vicente, junto al presidente Alberto Fernández.

En ese marco, el flamante titular del PJ bonaerense deseó "poder construir un tiempo diferente" para la Argentina, y continuó: "Han sido tiempos muy duros para nuestra Argentina. No sólo los cuatro años que estuvo en la Presidencia de Mauricio Macri y el fracaso de sus ideas económicas".

"La construcción del Frente de Todos fue titánica. Hubo que trabajar y hablar con muchos compañeros con los que no hablábamos hace muchísimo tiempo. Y con la tranquilidad, en mi caso, de no haber dicho nada de nadie, salvo manifestar el disenso político", subrayó.

Tras asumir en un puesto que provocó duros tiras y aflojes con algunos intendentes que buscaban tomar el control del partido a nivel provincial, Máximo apuntó: "Nunca acudí a ningún Palacio Judicial a denunciar a ningún compañero que está en el Frente de Todos, siempre tuve una postura democrática".

"Mis diferencias y las diferencias en Argentina debemos aprender a saldarlas políticamente y no en el Poder Judicial, porque eso es el peronismo", manifestó.

De esta manera, el diputado nacional hizo referencia a la batalla judicial que llevó adelante el intendente de Esteban Echeverría, Fernando Gray, quien rechazó este año la convocatoria a elecciones anticipadas y acudió a la Justicia.

Tras recordar la jura del ex presidente Néstor Kirchner, apuntó contra el ex mandatario Mauricio Macri por la toma de deuda: "Cuando vi que Macri volvía con un desparpajo increíble al FMI me acordé mucho de mi viejo, del tiempo que a él le había tocado vivir, y de lo que había trabajado para sacar al país adelante y desendeudarlo".

"Néstor era intendente de la capital de una provincia (Santa Cruz) pero que parece, tranquilamente podía ser un pueblo del interior de la provincia de Buenos Aires", expresó.

En ese sentido, completó: "En el momento en el que él fue intendente de la ciudad de Río Gallegos, aquella provincia tenía 30.000, 40.000 habitantes, no como los gigantes que suelen gobernar los amigos ¿no? con quinientos mil, cuatrocientos, trescientos mil habitantes, sino del interior".

"Para nosotros el interior de la provincia de Buenos Aires va a ser central. Vamos a ir distrito por distrito a hablar con cada compañero y compañera. También con todas las cámaras del comercio, con toda la gente que tiene sus inversiones y desarrollos en el interior de una provincia", enfatizó.

En el marco de la presentación de autoridades, Máximo afirmó que "el peronismo de la provincia de Buenos Aires es central para el Frente de Todos y que no hay que cerrarlo", sino que "hay que abrirlo".

"Muchos compañeros se han sumado a lo largo y ancho de la provincia para poder encontrar una síntesis superadora", señaló el flamante titular del PJ bonaerense, y reconoció: "Es muy difícil la situación, pero les puedo asegurar que se puede salir para adelante porque ya lo hicimos una vez".

En otro tramo de su discurso, el diputado nacional aseguró: "El país que queremos construir no es una utopía, los argentinos supimos construir una patria mejor. Tenemos que darnos la oportunidad otra vez de creer en nosotros mismos y lo vamos a sacar adelante cueste lo que cueste".

En un capítulo dedicado a la Justicia, Kirchner advirtió que el Poder Judicial "lo tiene bajo permanente acoso al Gobierno y que incluso volteó el Consejo de la Magistratura el jueves, el mismo día otro poder del Estado, el Poder Legislativo, le niega el presupuesto al Presidente".

"No es a un hombre, es a las ideas que expresa un hombre y a las ideas que expresamos nosotros. Eso es lo que tenemos que tener claro: quieren un país para pocos y nosotros queremos un país para todas y para todos", concluyó.