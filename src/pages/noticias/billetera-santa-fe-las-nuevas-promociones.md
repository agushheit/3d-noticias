---
category: Agenda Ciudadana
date: 2021-07-03T06:15:30-03:00
thumbnail: https://assets.3dnoticias.com.ar/BILLERETA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Billetera Santa Fe: las nuevas promociones'
title: 'Billetera Santa Fe: las nuevas promociones'
entradilla: El secretario de Comercio de la provincia, Juan Marcos Aviano, dijo que
  se vienen importantes descuentos en gastronomía, jugueterías y librerías.

---
La app Billetera Santa Fe sigue sumando adherentes no solo entre los santafesinos y santafesinas sino que además suma nuevos beneficios: este viernes, el secretario de Comercio de la provincia, Juan Marcos Aviano, anunció que para los días del amigo, el niño y el estudiante se lanzarán promociones en comercios que oscilan entre el 30 y el 40% de reintegro en los rubros gastronomía, juguetería y librerías.

Billetera Santa Fe es un programa de beneficios y reintegros que instrumenta el gobierno provincial para aumentar el poder de compra de los santafesinos y promover la actividad del comercio minorista, que ya alcanzó los 590 mil santafesinos y santafesinas y más de 12 mil comercios adheridos.

En declaraciones al programa "El primero de la mañana", de LT8, Aviano brindó precisiones sobre las nuevas promociones, similares a las lanzadas para el día del Padre, y explicó que este jueves "arrancó una nueva etapa del programa, que va a estar durante toda la gestión y donde trimestralmente el comercio va ratificando los términos y condiciones que tiene en su rubro, o para los nuevos comercios que se quieran adherir. Ahora hemos definido el calendario de campañas o promociones especiales, que va a ser el 40% de reintegro en el rubro gastronomía para el día del Amigo, el mismo porcentaje para el Día del Estudiantes y, desde el 9 al 15 de agosto, van a funcionar librerías y jugieterías todos los días al 30%, así como lo hicimos para el día del Padre".

Tras recordar que el tope de reintegro mensual es de 5.000 pesos, el funcionario se mostró muy entusiasmado con los resultados que está logrando la adhesión a la Billetera entre la población: "Estamos viendo un crecimiento sostenido en Billetera Santa Fe. Ya estamos en alrededor de 590 mil santafesinos y santafesinas adheridos en toda la provincia. El ritmo de registración diario continúa y por eso somos muy responsables con el programa porque queremos ahora apuntar a más comercios chicos y barriales y a los sectores de menor poder adquisitivo. Hoy lo usan todos los niveles socio económicos".

Aviano graficó como se había incrementado el crecimiento de adherentes, tanto a nivel de la población como de locales, el titular del área de Comercio provincial explicó que mayo fue el mejor mes: "Allí explotó todo, porque ese mes tuvimos unas 8.000 registraciones diarias. En cuanto a los comercios, hoy estamos en 12 mil adheridos. Esperábamos llegar a 10.000 en junio y hoy estamos en casi 12 mil. La demanda del programa está actuando sobre la oferta para que el comercio se adhiera. Hoy el cliente va y pregunta en los locales si tienen Billetera Santa Fe". Además, comentó que rubros como "ferreterías, pinturerías, ópticas, veterinarias y hasta peluquerías pretenden ingresar y ya les daremos cabida".