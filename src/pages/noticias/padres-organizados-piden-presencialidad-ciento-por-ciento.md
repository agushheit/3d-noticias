---
category: La Ciudad
date: 2021-02-15T07:30:53-03:00
thumbnail: https://assets.3dnoticias.com.ar/padres-organizados-ministerio-1jpg.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Padres Organizados piden presencialidad ciento por ciento
title: Padres Organizados piden presencialidad ciento por ciento
entradilla: A través de un documento dieron a conocer sus fundamentos luego de lo
  anunciado por la ministra santafesina y ratificado en el Consejo Federal de Educación.

---
El viernes se dio a conocer una circular donde se brindaban precisiones del regreso a las aulas en la provincia de Santa Fe con un sistema de alternancia y con un estricto protocolo. En tanto, el Consejo Federal de Educación, donde se reunieron ayer los ministros y el presidente Alberto Fernández, ratificó el retorno a las clases presenciales en todo el país, alternadas con la virtualidad, de acuerdo a la situación epidemiológica de cada región y con condiciones de seguridad sanitaria que garanticen el cuidado de la salud.

Por su parte la asociación santafesina de Padres Organizados elaboró un documento donde ponen de manifiesto su oposición a dicho sistema exigiendo que la presencialidad sea 100 por ciento. A través de una serie de puntos plantearon los fundamentos del porqué de su negativa.

Sostuvieron que la apertura de jardines maternales, colonias de vacaciones, clubes, actividades deportivas y recreativas para niños y jóvenes son un antecedente de la necesidad de volver a las aulas de manera continua y sin riesgos, ya que no hubo en ningún caso situaciones de desborde epidemiológico siguiendo los cuidados y protocolos de distanciamiento e higiene razonables. Esta conclusión se logró al hacer una encuesta en los espacios que retomaron sus tareas desde el mes de diciembre.

En cuanto a la posibilidad de alternar los grupos de chicos para la asistencia a clases la agrupación de padres manifestó: “La asistencia a clases semana por medio no asegura el desarrollo de contenidos académicos, ni motiva el proceso de aprendizaje. Es imposible comprimir en tres horas los contenidos y muy difícil formar un vínculo de confianza con la escuela”.

Además expresaron que “la alternancia rompe con la organización familiar aún más en el caso de las familias con dos o más hijos, que necesitarán que uno de sus padres se quede en la casa para asistirlos”.

Los padres sugirieron que se habiliten espacios abiertos o lugares grandes para que las escuelas con gran cantidad de alumnos y aulas reducidas puedan dar las clases.

También manifestaron que no se han tenido en cuenta los datos epidemiológicos por zonas o localidades. Todo se resume en alternancia y baja carga horaria, sin coherencia ni lógica. "Se baja una sola línea para toda la provincia, sin tener en cuenta las realidades físicas y sanitarias de cada ciudad, barrio y escuela".

"Por último, niegan totalmente la opinión de científicos, letrados, académicos, educadores, padres, transportistas y asistentes que forman parte del problema tomando una resolución arbitraria y unilateral".

Para cerrar la misiva manifiestan estar a disposición de las autoridades para aportar lo necesario para garantizar el 100% de presencialidad. "Alzamos la voz por nuestros hijos y seguimos esperando la oportunidad para plantear nuestra idea, sin ninguna bandera política. Nuestra bandera es la escuela".

***

[![Facebook](https://assets.3dnoticias.com.ar/padres-organizados.webp)](https://www.facebook.com/padresorganizadosSF/posts/202481521625002 "@padresorganizadosSF")

***