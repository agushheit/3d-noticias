---
category: La Ciudad
date: 2021-01-15T04:09:41Z
thumbnail: https://assets.3dnoticias.com.ar/obrasbarrios.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: '3DNoticias - Fuente: El Litoral'
resumen: Realizarán obras para recuperar dos plazas en los barrios Sacarafía y Los
  Troncos
title: Realizarán obras para recuperar dos plazas en los barrios Sacarafía y Los Troncos
entradilla: Se dotarán de mobiliario urbano, juegos, veredas e iluminación.

---
Este jueves se realizó la apertura de sobres del segundo llamado a licitación pública para la recuperación de dos espacios públicos en los barrios Los Troncos y Scarafía. 

Las obras se enmarcan en el Plan Integrar con la meta de propiciar espacios públicos generadores de encuentro social y garantizadores de derechos ciudadanos, particularmente el derecho al juego. Con ese objetivo, ambas plazas serán dotadas de juegos, veredas, plaza húmeda e iluminación, entre otras intervenciones.

Los trabajos tienen un presupuesto oficial de 26.892.101 pesos, financiados por el gobierno nacional a través del **Plan Argentina Hace**. 

Para su realización se presentaron tres oferentes: EFE Construcciones propuso concretar las obras por un monto de $ 32.600.000, BR Construcciones SRL por un total de $ 46.456.268,33, y Winkelmann por $ 47.359.428,24.

El acto fue encabezado por la secretaria de Obras y Espacio Público municipal, Griselda Bertoni, en la sede del Palacio Municipal.

<br/>

## **Detalle de las obras**

La plaza del barrio Scarafía está emplazada en Sarmiento Oeste, Padre Genesio, Chiclana y Risso. En tanto, la del barrio Los Troncos es un terreno ubicado en Azcuénaga entre Pje. Troncoso y Furlong. 

Las tareas que se licitaron para la concreción de estas dos plazas, consisten en la adecuación de niveles, terraplenamiento y relleno de los lotes con el objetivo de asegurar que no se inunden.

La construcción de bordillos de hormigón premoldeado para delimitar las áreas verdes de las secas, la construcción de veredas y otros solados de hormigón armado ferrocementado, vados peatonales para la circulación segura y universal. 

En cuanto a las áreas de juegos, tendrán pisos de césped sintético y plaza húmeda con humidificador. Está prevista la instalación eléctrica completa, con provisión y colocación de columnas de iluminación.

El mobiliario incluye bancos, cestos de residuos, bicicleteros y bebederos. Además de la forestación completa, brindando sombra a las circulaciones cómo a las áreas de descanso, con especies de herbáceas, árboles y arbustos.

Se trata de la segunda licitación pública que se lleva a cabo para concretar los trabajos, luego de que la primera -realizada hace un mes- tuviera una sola oferta que quedara desestimada por superar ampliamente el presupuesto oficial.