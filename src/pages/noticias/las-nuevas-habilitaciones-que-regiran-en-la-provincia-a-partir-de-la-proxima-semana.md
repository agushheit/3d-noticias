---
category: Agenda Ciudadana
date: 2020-11-27T12:00:00Z
thumbnail: https://assets.3dnoticias.com.ar/2.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Nuevas habilitaciones
title: Las nuevas habilitaciones que regirán en la provincia a partir de la próxima
  semana
entradilla: El nuevo cronograma incluye a comercios mayoristas y minoristas, actividades
  deportivas, jardines de gestión privada y eventos culturales.

---
Esta jueves por la tarde se llevó a cabo una reunión entre autoridades del gobierno provincial. En el encuentro se analizaron posibles reaperturas y nuevas habilitaciones de actividades, las cuales se concretarán a partir de la próxima semana.

Con funcionarios de las áreas de gestión pública, turismo, deporte, cultura y salud, se estableció un cronograma de aperturas y habilitaciones que incluye a comercios mayoristas y minoristas, actividades deportivas, jardines de gestión privada y eventos culturales.

En cuanto a los comercios mayoristas y minoristas, se dispondrá la extensión de horarios y la restricción de la circulación. El horario de atención al público será hasta las 21 horas. Los días viernes y sábados los bares, restaurantes, heladerías y otros podrán permanecer abiertos hasta la 1:30 hora del siguiente día. Con respecto a la circulación, se dispuso que de domingo a jueves sea entre las 0:30 y las 6 horas, mientras que los viernes y sábados de 1:30 a 6 horas.

Además, desde el sábado se autorizará la práctica de deportes donde los participantes interactúen y tengan contacto físico entre sí. Esta autorización incluye al “fútbol 5”.

Desde el 1 de diciembre, la Provincia habilitará el funcionamiento de los jardines maternales privados. Sin embargo, la ocupación no deberá superar el 50%.

Asimismo, se podrán organizar eventos culturales y recreativos relacionados con la actividad teatral y la música en vivo, tanto al aire libre como en teatros, centros culturales y otros lugares cerrados.