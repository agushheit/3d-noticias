---
category: Agenda Ciudadana
date: 2021-07-31T06:15:10-03:00
thumbnail: https://assets.3dnoticias.com.ar/CONVINETA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: La combinación de vacunas no reporta efectos adversos
title: La combinación de vacunas no reporta efectos adversos
entradilla: 'Según el estudio, el 41,6% de voluntarios no tuvo ninguna sintomatología
  relacionada con la vacunación y solo se detectaron casos de febrícula o dolor en
  la zona de aplicación. '

---
La combinación de las vacunas Sputnik V con Sinopharm y Astra Zeneca no tiene efectos adversos graves, según un estudio preliminar difundido este viernes por el Ministerio de Salud bonaerense.

La cartera sanitaria detalló que el 41,6%de las personas que se aplicaron esas combinaciones no tuvieron ninguna sintomatología relacionada a la vacunación, el 8,4% refirió febrícula y dolor de espalda y el 50% dolor en la zona de aplicación.

De la población estudiada, el 28,1% fueron mujeres y el 71,8%, varones con una media de edad de 49 años procedentes de los partidos bonaerenses de La Plata, Quilmes, Berazategui, Escobar, Florencio Varela, Lanús, San Fernando, San Isidro, San Miguel y Vicente López.

La provincia de Buenos Aires fue una de las elegidas por el Gobierno nacional para evaluar la respuesta del uso de vacunas de distinta procedencia en una misma persona y, para ello, el 7 de este mes convocó a voluntarios.

Esta convocatoria se amplió esta semana para voluntarios del Gran La Plata, con quienes se estudiarán nuevas combinaciones.

El Ministerio aseguró que la investigación busca "multiplicar las posibilidades de inmunización" y aseguró que, con esta clase de investigaciones, que ya se realizan en otras partes del mundo, "se podrá contar con información rigurosa y específica en base a las vacunas que se aplican en el país".

Explicó que los voluntarios se dividen en grupos aleatoriamente para combinaciones que pueden ser heterólogas (de distintos laboratorios) u homólogas (del mismo laboratorio) para formar parte de lo que se conoce como “grupo control”.