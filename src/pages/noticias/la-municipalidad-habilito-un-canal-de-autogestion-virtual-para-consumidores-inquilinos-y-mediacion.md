---
category: La Ciudad
date: 2021-04-21T07:18:18-03:00
thumbnail: https://assets.3dnoticias.com.ar/covid.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad de Santa Fe
resumen: La Municipalidad habilitó un canal de autogestión virtual para consumidores,
  inquilinos y mediación
title: La Municipalidad habilitó un canal de autogestión virtual para consumidores,
  inquilinos y mediación
entradilla: A partir de ahora se podrán realizar trámites web referidos a derechos
  y vinculación ciudadana a través de la Oficina Virtual de la Municipalidad de Santa
  Fe.

---
Con el objetivo de facilitar el acceso a los trámites y a distintas herramientas para solicitudes y reclamos de consumidores, la Municipalidad de Santa Fe habilitó nuevas gestiones a través de la Oficina Virtual. A partir de ahora, las y los santafesinos podrán solicitar la intervención de la Oficina Municipal del Inquilino, realizar denuncias a Defensa del Consumidor y pedir una mediación comunitaria sin necesidad de hacerlo de manera presencial.

El director de Derechos y Vinculación Ciudadana, Franco Ponce de León, expresó que “con el foco puesto en brindar atención más allá de la presencialidad, y con el compromiso de adaptarnos a las realidades de los nuevos tiempos, se desarrollaron canales de comunicación y de escucha virtuales”.

En este sentido, se incorporó la posibilidad de autogestión a través de Oficina Virtual para solicitudes de mediación comunitaria, como también, formularios de reclamos para inquilinos, usuarios y consumidores. “Esta es una herramienta más, que permitirá a estos grupos realizar sus consultas, solicitudes, reclamos y denuncias, de manera ágil y sencilla, facilitándose la cercanía con el municipio”, indicó el funcionario.

Para iniciar la gestión se deberá ingresar a la Oficina Virtual y allí en el catálogo por servicios o en el buscador se encontrarán cada una de las planillas de trámite correspondiente, con una hoja informativa que explica el servicio y la base legal en la materia.

Luego, se podrá ir monitoreando todo el expediente en soporte digital e incorporando en la etapa oportuna los documentos, informes o pruebas sin necesidad de la manipulación de papel, lo que contribuye en el cuidado del medio ambiente así como en la agilidad de los trámites.

Se pueden acceder a los distintos trámites en los siguientes enlaces:

* Solicitud de intervención de la Oficina Municipal del Inquilino.
* Denuncia Defensa del Consumidor.
* Solicitud de Mediación Comunitaria.

Cabe destacar que esta propuesta se realizó con la colaboración de la Subsecretaría de Innovación Institucional y Gobierno Abierto. Además, la Dirección de Derechos y Vinculación Ciudadana se encuentra trabajando en el desarrollo de expedientes completamente digitales, lo que afianzará la virtualización de los trámites más comunes, que reflejan a su vez transparencia, agilidad y accesibilidad en la gestión de reclamos o solicitudes.

Para mayor información, las personas interesadas pueden acercarse de lunes a viernes de 7.15 a 13.00 hs a Salta 2840, comunicarse por whatsapp (3425315450) o llamar al 4574119 o 0800 444 0442.