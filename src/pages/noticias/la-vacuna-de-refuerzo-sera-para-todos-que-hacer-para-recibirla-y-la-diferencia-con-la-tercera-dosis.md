---
category: Agenda Ciudadana
date: 2021-11-17T06:15:58-03:00
thumbnail: https://assets.3dnoticias.com.ar/refuerzos.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'La vacuna de refuerzo será para todos: qué hacer para recibirla y la diferencia
  con la tercera dosis'
title: 'La vacuna de refuerzo será para todos: qué hacer para recibirla y la diferencia
  con la tercera dosis'
entradilla: 'Comenzó la campaña para los mayores de 70 años y este miércoles arrancará
  entre los trabajadores de salud. El requisito clave para poder recibirla.

'

---
Este martes comenzó en Santa Fe la colocación de dosis de refuerzo contra el coronavirus a personas de más de 70 años y este miércoles continuará con el personal de salud, primero, por los profesionales que se desempeñan en las áreas de terapia intensiva.

Se trata de la estrategia definida por la Comisión Nacional de Inmunizaciones, del comité de expertos y del Ministerio de Salud de comenzar a colocar una dosis más entre todos aquellos que ya completaron el esquema de vacunación (de dos dosis). En todos los casos, para poder recibirla se deberán cumplir con un requisito: que hayan pasado seis meses de la segunda dosis.

El secretario de Salud de la provincia, Jorge Prieto, insistió en continuar y profundizar la campaña de vacunación. Comentó que si bien el país no padeció la tercera ola; advirtió sobre lo que está ocurriendo en países de Europa, quienes atraviesan por un crecimiento de infectados y de muertes por Coronavirus.

Detalló que el refuerzo de vacunas será "para aquellos que hayan completado los esquemas de vacuna por encima de los seis meses, cualquiera sea el esquema de vacunación que tengan. Estamos colocando Astrazéneca, que es una de las vacunas aconsejadas en las combinaciones".

Explicó que "tercera dosis" no es lo mismo que "dosis de refuerzo". En ese sentido, profundizó: "Tercera dosis viene a completar esquemas. Es decir, en lugar de dos dosis de vacunas necesitaremos una tercera; esto tiene que ver con la respuesta inmunológica que determina cada uno de los organismos".

En la provincia de Santa Fe, quienes fueron convocados para la tercera dosis fueron los mayores de 50 que recibieron Sinopharm, las personas inmunosuprimidas desde los 3 años en adelante y que hayan recibido cualquiera de los esquemas disponibles (Sputnik, Sinopharm, Pfizer, Moderna, AstraZéneca).

En cambio, las "dosis de refuerzo iniciarían un nuevo esquema, para que tenga mayor duración, como un calendario anual de vacunación".

"Tenemos una población de más de 70 años cercana a 300 mil. Hoy tenemos 1100 turnos (en La Redonda); la mayoría ha elegido la vacunación directamente en el automóvil", graficó Prieto.

En esa línea, afirmó que la dosis de refuerzo "será para todos". Y reiteró: "Nosotros estamos priorizando (además de los mayores de 70) lo hace al personal de salud. Vamos a comenzar con aquellos que estén en terapia intensiva; después el resto del personal".

En diálogo con la prensa, comentó que la idea es que todos tengan la dosis de refuerzo y dar la tercera dosis a quien corresponda; "Seguramente, quien tenga una tercera dosis va a requerir en algún momento del refuerzo", amplió.

Con respecto al mecanismo de citación para recibir dosis de refuerzo, el funcionario amplió: "La gente fue citada automáticamente. Sí solicitamos a los trabajadores de la salud que se inscriban en la página de gobierno".

Y continuó: "Quienes están inscriptos en la página reciben directamente el turno como hasta ahora. Lo único que pedimos es la inscripción de aquellas personas independientes de salud que no hayan sido vacunados en ninguno de los efectores de salud. A ellos sí les recomendamos que se inscriban para poder ser citadas. Porque si bien hay un registro (Sicap) que es el registro provincial, a través del mismo no podemos hacer la turnera y no ingresarían al sistema general".