---
category: Agenda Ciudadana
date: 2021-06-17T09:06:16-03:00
thumbnail: https://assets.3dnoticias.com.ar/rio-parana.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de LT10
resumen: El río Paraná descendería al nivel cero en el puerto de Santa Fe
title: El río Paraná descendería al nivel cero en el puerto de Santa Fe
entradilla: Estiman que habrá una “reducción muy drástica de caudales”. Se espera
  para julio una situación "crítica".

---
El caudal del río Paraná vuelve a ser noticia. Luego del repunte después de la bajante histórica del año pasado se prevé que el nivel comience a descender rápidamente en mayo. Durante el 2020 se mantuvo por debajo del metro y en el puerto de Santa Fe la altura mínima llegó a los 50 centímetros. 

Sin embargo, comenzó a subir  y en el mes de febrero recuperó algo de nivel alcanzando los 3,41 mts. para fines de ese mes. Esa recuperación, según explicó por LT10 Juan Borus, subgerente de Sistemas de Información y Alerta Hidrológico del INA, fue debido a un solo evento de lluvias con una reacción de respuesta muy rápida "que fue una sorpresa dentro del escenario de sequía y de bajante", agregó.

**Panorama actual y futuro**

El ingeniero señaló que la bajante actual del río Paraná “es la continuidad de la del año pasado. Es el mismo escenario” y trazó un panorama muy preocupante de cara a los próximos meses. "El mes de julio será especialmente crítico, con afectación a todos los usos del recurso hídrico".

En ese sentido, explicó que ante el pronóstico de falta de lluvias, “la perspectiva es peor que el año pasado. Se asemeja a lo que pasó en 1944 que fue el año de referencia crítica”.

En cuanto al pronóstico Borus indicó que “se estimaba que en el puerto de Santa Fe, a la altura de la ciudad, el registro hacia fines de julio estará a 25 cm de cero. Sin embargo, lo que creemos ahora es que estaría sobre el cero en escala”.

“Tendremos una reducción muy drástica de los caudales en julio. Es posible, que sea de tal magnitud la disminución que  hacia fines de dicho mes y principios de agosto tengamos lecturas de escala claramente por debajo de cero y lo más cercano será 25 centímetros por debajo de cero”, estimó el especialista, quien insistió en que estamos frente a "una situación extrema".