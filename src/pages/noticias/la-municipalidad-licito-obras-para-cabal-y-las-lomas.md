---
category: La Ciudad
date: 2020-12-31T10:50:51Z
thumbnail: https://assets.3dnoticias.com.ar/3112-plan-integrar.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: La Municipalidad licitó obras para Cabal y Las Lomas
title: La Municipalidad licitó obras para Cabal y Las Lomas
entradilla: En el marco del Plan Integrar, se abrieron los sobres para concretar trabajos
  de pavimentación, mejorado de calles y desagües pluviales para más de 5000 habitantes
  de esos barrios.

---
Este miércoles, en la sede del Club Cabal, el intendente Emilio Jatón y el gobernador de la provincia, Omar Perotti, encabezaron la apertura de sobres con los oferentes para realizar trabajos en los barrios Cabal y Las Lomas. Las obras de mejorado de calle, pavimentación y desagües pluviales se extenderán a lo largo de 1600 metros, con un ancho entre cordones de 8 metros, cuentan con financiamiento nacional y se inscriben en el Plan Integrar, puesto en marcha por la Municipalidad.

Jatón definió que Cabal «es un barrio ligado a la historia del Hipódromo, ligado a las quintas. No es nuevo, sino que en la década del 40 ya tenía vida propia y en la del 50, más todavía». En ese contexto, consideró que «parece mentira que hoy estemos hablando de terminar con las cunetas en Estanislao Zeballos y llegar, por primera vez, a trabajar con políticas de cohesión, de cómo llegar a lugares para hacer una ciudad más equilibrada». En ese sentido, afirmó que «por primera vez, con esta licitación estamos haciendo historia, porque va a llegar el asfalto a barrio Las Lomas».

Se trata de la ejecución de pavimento, cordón cuneta, mejorado con estabilizado granular y desagües pluviales sobre calle Estanislao Zeballos, entre Bernardo de Irigoyen y Vieyra. De las mejoras serán beneficiarios directos 5.049 habitantes de Cabal y Las Lomas a los que se suman, de forma indirecta, 55.392 personas residentes en el distrito Noroeste de la ciudad.

Los trabajos tienen un presupuesto oficial de $ 97.447.698.06, correspondientes al programa nacional «Argentina Hace 2». En total se presentaron cuatro ofertas: Mundo Construcciones S.A., con $ 102.493.843,26; COEMYC S.A., con $ 110.295.831,76; Winnkelman S.R.L., con $ 93.178.112,01; y EFE Construcciones, con $ 108.000.000.

Para el intendente, este momento «es histórico y es pensar en el otro, en dignidad y en el plan de gobierno que estamos estructurando. Estamos haciendo lo que dijimos: hoy estamos aquí para licitar, estamos aquí para dar dignidad, para brindar ciudadanía y cumplir con la palabra en un año tan difícil».

También agradeció a quienes acompañaron en «este proceso que ha sido largo y que hemos iniciado hace mucho tiempo, con la ministra (Silvina) Frana y con el equipo del municipio. Muchas veces uno llega a este momento y lo disfruta», indicó, sobre las obras licitadas que fueron planificadas hace ocho meses.

Finalmente, insistió en que «este es un trabajo en equipo, que es la única forma de hacer las cosas: Nación, provincia y municipio»; y convocó a los presentes a celebrar porque «este es un gran día».

Por su parte, Perotti remarcó que las obras licitadas «tienen un origen previo a la pandemia, cuando firmamos un convenio con la Nación, en el que el intendente Jatón nos estuvo acompañando», integrado al programa «Argentina Hace».

El gobernador coincidió con Jatón en la importancia de «trabajar coordinadamente para presentar los proyectos a partir del trabajo técnico del municipio y la vinculación con Nación para que esta etapa de recursos llegue a la provincia y a la ciudad de Santa Fe. Esta es una tarea que hay que hacer coordinadamente», acotó.

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.2em; font-weight: 700">«Mucha falta»</span>**

Mónica Parente asistió a la apertura de sobres en representación de los vecinos autoconvocados de Cabal. Sobre las obras previstas, consideró que «hacen mucha falta en el barrio» y destacó que «nuestra expectativa es que las obras se hagan, que se cumplan todos los tramos y se pueda continuar con otras calles que también son importantes».

Por su parte Larisa Sánchez, secretaria de la Comisión Directiva de Cabal Bochas Club, destacó la importancia de los trabajos: «Como institución me parece positivo que se haga la apertura de sobres acá, para visibilizar a nuestro club. Estamos contentos por las obras que se van a hacer, es un avance», valoró.

Con los trabajos previstos, el club barrial podrá contar con mejor accesibilidad. Al respecto, Sánchez compartió que es «por lo que venimos peleando. Estamos muy adentro del barrio y las calles no son muy accesibles. Creo que mejorar Estanislao Zeballos va a ser una buena opción para venir al club», señaló.

**<span style="font-family: Helvetica, sans-serif; font-size: 1.2em; font-weight: 700"> Las obras</span>**

Los trabajos se concretarán en cuatro tramos: en Bernardo de Irigoyen, entre Castelli y Boneo; en Boneo, entre Bernardo de Irigoyen y pasaje Santa Fe; en Estanislao Zeballos, entre Bernardo de Irigoyen y Menchaca; y en Estanislao Zeballos entre Menchaca y Vieyra.

En los primeros tres tramos, que corresponden al barrio Cabal, se realizará cordón cuneta con estabilizado granular y bocacalles con badenes de hormigón. En tanto, en el último tramo, en Las Lomas, se hará una carpeta de concreto asfáltico entre cordones cunetas.

También se incluyen obras complementarias como rampas para accesibilidad, baldosas perimetrales de aviso, guías para personas con discapacidad visual, veredas de hormigón peinado en ochavas, cinta verde en ambas veredas y forestación en toda la traza. Asimismo, a lo largo de todo el recorrido se concretarán desagües pluviales, con cañería pluvial de H°A°, bocas de registro, bocas de tormenta, cámaras de captación y muros cabezales para acometidas de cunetas.

El objetivo es mejorar las condiciones de transitabilidad de peatones, ciclistas y vehículos, mediante la ejecución de obras viales e hídricas. Con ese fin, las tareas en cuestión modificarán la fisonomía del noroeste de la capital provincial. Además, ponen el eje en la integración social mediante la mejora de las condiciones urbano-ambientales y la accesibilidad a los barrios, a través de la provisión de infraestructura y servicios.

**<span style="font-family: Helvetica, sans-serif; font-size: 1.2em; font-weight: 700">Participantes</span>**

Participaron de la licitación, junto con el intendente santafesino y el gobernador, la ministra de Infraestructura, Servicios Públicos y Hábitat de la provincia, Silvina Frana; el secretario de Integración y Economía Social del municipio, Mariano Granato; la secretaria de Obras y Espacio Público municipal, Griselda Bertoni; y el director de Derechos y Vinculación Ciudadana, Franco Ponce de León, entre otras autoridades municipales y provinciales. También asistieron las concejalas Laura Mondino, Mercedes Benedetti y Jorgelina Mudallel, que participaron junto a representantes del club anfitrión y vecinas y vecinos de los barrios beneficiados.