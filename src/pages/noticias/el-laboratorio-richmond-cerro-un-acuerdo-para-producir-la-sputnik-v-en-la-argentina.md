---
category: Agenda Ciudadana
date: 2021-02-27T08:02:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/vacuna.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: El laboratorio Richmond cerró un acuerdo para producir la Sputnik V en la
  Argentina
title: El laboratorio Richmond cerró un acuerdo para producir la Sputnik V en la Argentina
entradilla: El acuerdo fue suscripto en Moscú por Tagir Sitdekov, en representación
  RDIF, y Marcelo Figueiras, presidente de Richmond.

---
El laboratorio Richmond de la Argentina firmó un memorándum de entendimiento con el Fondo Ruso de Inversión Directa (RDIF) para producir la vacuna Sputnik V contra el coronavirus en el país, desarrollo en el que participará el laboratorio indio Hetero, encargado de transferir la tecnología.  
  
El presidente de Richmond, Marcelo Figueiras, afirmó a Télam que el acuerdo, "no resuelve la emergencia hoy, pero nos permite a futuro fabricar en nuestro país estas vacunas y otras, ampliará la capacidad de producción y nos permitirá generar nuestros propios recursos sanitarios".  
  
"Empezamos a trabajar en un acuerdo de partes", dijo Figueiras, y agregó que la producción en el país "permitirá enfrentar futuras pandemias".  
  
El empresario señaló que Richmond "ampliará la capacidad productiva porque el que no produzca a futuro va a seguir sufriendo lo que ocurre en este momento, que es la escasez y donde se usan los recursos en el país que se generan".  
  
"Estamos apuntando a tener plantas de vacunación porque pensamos que lamentablemente este tipo de pandemia llegó para quedarse", dijo la directora de Asuntos Técnicos y Científicos del laboratorio Richmond, Elvira Zini, sobre el acuerdo y remarcó que la decisión "nos garantiza un futuro de abastecimiento" de dosis para inmunización.

El acuerdo fue suscripto en Moscú por Tagir Sitdekov, en representación RDIF, y el presidente de Richmond, según una nota divulgada el viernes por la empresa local.  
Tras conocerse el anuncio, Figueiras explicó que la fabricación de la Sputnik V "se realizará con la participación de Hetero Labs Limited, laboratorio establecido en India, con quien Richmond posee una alianza estratégica desde hace más de 25 años".

"Es un proceso de un año, por lo menos, con lo cual no es para crear expectativas ahora sino el día de mañana no caer de nuevo en esperar a que lleguen vacunas de afuera y tener una producción local", precisó el presidente de Richmond en otras declaraciones realizadas más temprano a radio Mitre.  
  
El laboratorio, que tiene una planta en el municipio bonaerense de Pilar, construirá otra para la fabricación de la Sputnik, lo que demandará al menos un año.