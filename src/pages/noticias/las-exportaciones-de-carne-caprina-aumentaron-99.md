---
layout: Noticia con imagen
author: .
resumen: Aumentaron las exportaciones
category: El Campo
title: Las exportaciones de carne caprina aumentaron 99%
entradilla: Se enviaron 424 toneladas a Sri Lanka, Vietnam, Bahamas y Angola, en
  los primeros diez meses del año.
date: 2020-11-03T13:17:19.802Z
thumbnail: https://assets.3dnoticias.com.ar/campo.jpg
---
El Ministerio de Agricultura, Ganadería y Pesca de la Nación registró, entre enero y octubre de 2020, la exportación de 424 toneladas de carne caprina (cabra y chivitos), lo que representa un aumento del 99% respecto al mismo período del año pasado, cuando se comercializaron 213,3 toneladas.

De acuerdo a los datos del Servicio Nacional de Sanidad y Calidad Agroalimentaria (Senasa), estas carnes, faenadas en frigoríficos ubicados en Santiago del Estero, Chaco, La Pampa, Mendoza y Córdoba; tuvieron por destino a Sri Lanka, 171,64 toneladas; Vietnam, 135,20 toneladas; Bahamas, 91,33 toneladas; y Angola, 25,64 toneladas.

Además de los 4 destinos mencionados, el Senasa tiene registrados actualmente otros 32 mercados abiertos para enviar carne caprina desde la Argentina, como la Unión Europea, la Unión Económica Euroasiática, y Singapur, entre otros. La producción de cabras en Argentina está ligada tradicionalmente a agricultores familiares orientados especialmente a la producción de carne para subsistencia y el mercado interno.

Nuestro país cuenta con 18 frigoríficos habilitados por el Senasa para faenar y exportar carnes caprinas. Seis de ellos están ubicados en la provincia de Buenos Aires; tres en Mendoza; dos en la Ciudad de Buenos Aires; y uno en Córdoba, Chaco, La Pampa, Río Negro, Santiago del Estero; Santa Fe y Santa Cruz, respectivamente.

Para asegurar la inocuidad de este tipo de carne, el Servicio de Inspección Veterinaria del Senasa realiza en cada frigorífico, un examen a todos los animales al arribo de la tropa al establecimiento, antes y luego de la faena.