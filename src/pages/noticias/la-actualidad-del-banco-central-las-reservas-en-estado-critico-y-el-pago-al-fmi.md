---
category: Agenda Ciudadana
date: 2022-01-25T06:15:42-03:00
thumbnail: https://assets.3dnoticias.com.ar/BANCOCENTRAL.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: 'La actualidad del Banco Central: las reservas en estado crítico y el pago
  al FMI'
title: 'La actualidad del Banco Central: las reservas en estado crítico y el pago
  al FMI'
entradilla: 'El economista Daniel Marx explicó en detalle los vencimientos de deuda
  que le corresponde abonar a la Argentina en los próximos meses, y detalló cuándo
  dinero hay en la entidad financiera.

  '

---
En el marco de la constante negociación con el FMI, el economista Daniel Marx detalló los pagos a la entidad correspondientes para los próximos meses y anticipó el número de las reservas del Banco Central.

"Esta semana hay que pagarle al Fondo 720 millones. Después hay otro vencimiento dentro de un mes que es de algo más de 300 millones de dólares, y en marzo hay un vencimiento más grande, que es de 2.900 millones de dólares aproximadamente", afirmó Marx en diálogo con Radio Rivadavia.

Para el economista. el Gobierno apuntaba a presentar un programa vigente para el mes de marzo que habilite "el desembolso para el pago". “Marzo era un poco el objetivo, no solo por el monto sino porque el mismo Gobierno expresó que se estaba apuntando a tener un programa vigente para el mes de marzo que habilite un desembolso para el pago”, sostuvo.

Por otra parte, señaló que las reservas brutas publicadas por el Banco Central son de "alrededor de 39 mil millones de dólares", y diferenció 29 mil millones de dólares que corresponden a un swap de monedas acordados con el Banco Central de China, que no pueden ser utilizados para pagos debido a su liquidez.

"Hay casi dos mil millones de dólares que corresponden a la liquidez de los bancos que respaldan los depósitos en dólares", afirmó Marx, y sentenció: “Si uno redujese los importes a las reservas brutas, se queda alrededor de cero”.

Asimismo, el economista aseguró que los países emergentes “tienen niveles de reservas superiores computados en referencia a los pagos que deben que hacer”. “Estamos pasando por la escases de reservas, a la espera del acuerdo con el FMI que nos permita repuntar para adelante y no generar cuestiones de incertidumbre sobre los niveles de reservas”, ponderó, y sumó: “No es fácil cerrar el acuerdo antes de marzo, pero el Gobierno lo busca y parece que el Fondo lo desea”.

También, destacó que el FMI “siempre pide más” de lo que los países ofrecen y muestran como límites propios. “Están viendo el programa general, cómo se relaciona la cuestión fiscal con la evolución de la oferta de dinero y las consecuencias sobre la inflación”, declaró. Por último, celebró la visita de Cafiero a Blinken y definió la negociación de la deuda como una gestión “trabajosa y compleja”.