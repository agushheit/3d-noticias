---
category: Agenda Ciudadana
date: 2021-03-13T06:00:46-03:00
thumbnail: https://assets.3dnoticias.com.ar/censo.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Provincia presentó los resultados del censo policial 2020
title: La Provincia presentó los resultados del censo policial 2020
entradilla: Consistió en un cuestionario obligatorio y online con respuestas encriptadas.

---
El gobierno provincial, a través del Ministerio de Seguridad, presentó este viernes los resultados del Censo Policial 2020, que se realizó con el objetivo principal de conocer en profundidad la opinión y las necesidades de los efectivos que integran la Policía de Santa Fe.

En conferencia de prensa, el secretario de Seguridad Pública de la provincia, Germán Montenegro, subrayó que "una de las preguntas del censo indaga sobre el uso del arma y el 82% dijo que nunca disparó un arma. Esto indica que no necesariamente el uso del arma es algo que sea permanente en la policía. En la concepción policial la cuestión del arma es una cuestión central, pero en la práctica observamos que no necesariamente esto es así. Esto indica que la policía, en el trabajo cotidiano, utiliza otras herramientas para la gestión de los asuntos de la seguridad y nos abre un espacio para trabajar mucho más en el perfeccionamiento y la profesionalización".

“Hay mucha realidad cotidiana que no implica el uso del arma, pero lo interesante del censo es que nos abre una serie de temas importantes, con información veraz, sobre la cual nosotros tenemos que trabajar teniendo política para atender esta temática", enfatizó Montenegro.

Asimismo, el funcionario explicó que "hay que poner el foco en lo urgente. Es decir, nosotros hacemos mucho hincapié con relación al personal en todas aquellas cuestiones que tienen que ver con el bienestar del personal policial. Creemos que una policía bien remunerada, bien atendida, con los problemas sociales y económicos relativamente contenidos y con una buena perspectiva profesional, mejora la calidad de los servicios. Un policía destratado da un mal servicio de seguridad o comete abusos. Entonces creemos que este censo nos pone luz sobre lo que piensa la policía, los cuerpos policiales son cuerpos jerárquicos y en gran medida es una característica del trabajo que hacen. Este tipo de censo es una herramienta fundamental para conocer en detalle la problemática".

Al respecto, la subsecretaria del Observatorio de Seguridad Pública, Luciana Ghiberto, aseguró que “el 35% del personal son mujeres y hemos preguntado además si les parecía adecuado que incrementen la cantidad de mujeres dentro de la fuerza policial provincial. Un 70% dijo que sí y también nos pareció interesante hacer un módulo específico sobre mujeres a las cuales le preguntamos a ellas mismas cuáles eran las experiencias con sus compañeros varones dentro de la institución policial".

Por su parte, el director del programa de delito y Sociedad de la Universidad del Litoral, Máximo Sozzo, indicó que: "En primer lugar se puede decir que buena parte de los policías identifican a la corrupción como un problema importante dentro de la institución policial, 8 de cada 10 policías consideran que es un problema grave o muy grave. Al mismo tiempo, una buena parte de los y las policías al ser consultados sobre si en sus lugares de trabajo han experimentado trazos vinculados a mecanismos corruptos, responde que eso no es así. Sin embargo, 1.7 de cada 10 afirman que sí. Lo que nos da una muestra de la presencia de este problema donde hay que operar".

Por otro lado, "la mitad de los policías señalan que denunciar hechos de corrupción puede ser objeto de represalia. Hay una cierta desconfianza sobre la posibilidad de denunciar, nuevamente ahí podemos ver el medio vaso lleno o el medio vaso vacío. La otra mitad considera que no existe esa posibilidad de la represalia, pero evidentemente ese también es otro componente fundamental que instala la cuestión de cómo construir política de control de la actividad policial que sean realmente efectivas y genere confianza para que los policías honestos puedan señalar la existencia de esos rastros o rasgos de corrupción".

El censo consistió en un cuestionario obligatorio y online que los efectivos pudieron completar en cualquier momento del día, desde una computadora o celular y las respuestas eran encriptadas.

**Temáticas**

El censo indagó sobre la instrucción recibida durante la carrera policial, el funcionamiento general de la Institución Policial, las condiciones socioeconómicas y la composición familiar del personal y las condiciones laborales. Asimismo, también contenía un eje de preguntas orientado a las experiencias de los/as policías ante posibles situaciones que hacen al riesgo de la actividad policial, la importancia asignada a su arma reglamentaria para el desarrollo de su trabajo y sobre la posibilidad de utilización de armas no letales para el cumplimiento de sus funciones. Qué piensan los/as policías respecto de los cambios institucionales y las reformas en materia de seguridad que se dieron en la provincia en los últimos años y qué piensan sobre ciertas problemáticas sociales que afectan a la comunidad en general.

Había un módulo específico dirigido a las mujeres que forman parte de la fuerza sobre las tareas de cuidado, la lactancia, los estereotipos de género dentro la policía, la asignación de funciones y tareas por género y sobre situaciones de abuso sexual. También se buscó rescatar las percepciones de las mujeres policías del trato recibido por sus compañeros y superiores varones, así como el impacto o importancia que ha adquirido la violencia de género dentro del marco general de situaciones y problemáticas con las que trabaja la policía y el rol de las Comisarías de la Mujer en tal abordaje.