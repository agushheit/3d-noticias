---
layout: Noticia con imagen
author: 3DNoticias
resumen: Obras de bacheo
category: La Ciudad
title: Harán obras de bacheo por $15 millones en el centro de la ciudad
entradilla: Este miércoles se concretó la primera de cuatro licitaciones. Los
  trabajos comenzarán este año y se realizarán en el micro y macrocentro
  santafesino.
date: 2020-11-26T13:02:28.906Z
thumbnail: https://assets.3dnoticias.com.ar/licitacion.jpeg
---
Este miércoles, el intendente Emilio Jatón encabezó el acto de licitación para trabajos de bacheo en el micro y macrocentro de la ciudad.

Con un presupuesto oficial de $14.654.863,58, se abrió el sobre correspondiente a la empresa Rava S.A. de Construcciones que presentó una oferta por $17.552.058,33. Ahora, una comisión especial estudiará y evaluará la propuesta.

Se trata de la primera de un total de cuatro licitaciones para completar obras de pavimentación, las cuales se concretarán en lo que queda del año y los primeros meses de 2021. En esta oportunidad, las tareas se centrarán en el micro y macrocentro de la capital provincial, en el cuadrante delimitado por Boulevard, las avenidas Freyre y J.J. Paso, y calle Belgrano.

Jatón aseguró que con la licitación de este miércoles se pone en marcha "un ambicioso plan que va a abarcar gran parte de la ciudad" y que continuará en los meses venideros con tres licitaciones más.

### **TIEMPOS DE TRABAJO**

Por su parte, la secretaria de Obras y Espacio Público del Municipio, Griselda Bertoni, detalló que los trabajos de bacheo sobre pavimento asfáltico que se licitaron este miércoles comenzarán luego de las fiestas de fin de año y estarán concluidos para marzo próximo, si las condiciones climáticas lo permiten.

En el área señalada, especificó que “la empresa adjudicataria hará el bacheo y por administración municipal, la dirección de Mantenimiento Vial va a cumplimentar con hormigón, ya que en esa zona hay algunos baches históricos que vamos a resolver en el mismo momento”.

### **PRÓXIMAS LICITACIONES**

Respecto del resto de las licitaciones, la funcionaria mencionó que todas estarán completas para el primer semestre del año próximo. “Son tres licitaciones más de bacheo en hormigón para los barrios principales de la ciudad, sobre todo aquellos por los que circula gran cantidad de vehículos y transporte urbano”, afirmó.

Una vez concluido el acto formal, Bertoni aseguró que “nuestra ciudad está muy mal a nivel de bacheo así que estas tres licitaciones tomarán una parte del total a resolver. Por eso decidimos con el intendente Emilio Jatón hacerlo en las vías principales”. Específicamente indicó que serán licitaciones por terceros, por montos cercanos a los 15 millones de pesos y, en paralelo, la Municipalidad hará por administración y con recursos propios, las tareas de ripiado, bacheo asfáltico y bacheo de hormigón.

En cuanto a las tareas para el año próximo, la secretaria recordó que el Presupuesto 2021 oficializado ya por la gestión municipal, asigna casi un 60% de los recursos al área de infraestructura y aclaró que “una parte de eso se destinará a bacheo”.