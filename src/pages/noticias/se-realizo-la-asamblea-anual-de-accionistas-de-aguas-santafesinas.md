---
category: Estado Real
date: 2021-06-07T18:00:44-03:00
thumbnail: https://assets.3dnoticias.com.ar/AGUAS.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Se realizó la asamblea anual de accionistas de Aguas Santafesinas
title: Se realizó la asamblea anual de accionistas de Aguas Santafesinas
entradilla: "“El balance de trabajo es ampliamente favorable, más en tiempos de pandemia”,
  afirmó la ministra de Infraestructura, Servicios Públicos y Hábitat, Silvina Frana."

---
Aguas Santafesinas S.A. realizó este viernes su asamblea anual de accionistas, encabezada por el presidente del Directorio, Hugo Morzán, y con la participación de la ministra de Infraestructura, Servicios Públicos y Hábitat, Silvina Frana, en representación del Estado provincial, accionista mayoritario de la empresa.

La asamblea se realizó por primera vez en modalidad virtual a través de Zoom, en el marco de la pandemia de Covid-19.

En la oportunidad, Frana destacó que “después de un año con muchas dificultades, de pandemia y atípico desde todo punto de vista, incluso con una bajante histórica del río, esta asamblea nos permite repasar el año de gestión y no hace más que valorar el trabajo de la empresa en su totalidad y felicitar a la persona de Hugo Morzán y al esfuerzo y compromiso de los trabajadores y trabajadoras que pudieron sortear obstáculos. Supieron trabajar eficientemente, porque trabajaron en lo normal y habitual y, además, pusieron un gran empuje en el desarrollo de obras con este respaldo que logramos de Nación para fortalecer los sistemas de agua y cloacas de toda la provincia, que vamos a seguir fortaleciendo”.

Además, resaltó que “más allá de los números y de las cuestiones formales, si se mira el desempeño en este año de la empresa, el balance es más que favorable y valorarlo doblemente porque no fue en cualquier momento, fue en un año de pandemia”.

Finalmente, puso en relieve que “cumplimos con lo que nos pide el gobernador de la provincia todos los días, duplicar el esfuerzo y de alguna manera hacer honor a quienes están en la trinchera de la pandemia, que es el personal de salud, a nosotros nos obliga a duplicar los esfuerzos para que las otras cosas, las otras que no tienen que ver con la pandemia, sucedan y sucedan mejor”.

El capital societario de Aguas está constituido en un 51 % por el Estado provincial; 39 % por municipios que forman parte de su área de servicio y 10 % por los trabajadores de la empresa.

El encuentro sirvió para cumplir con lo establecido por la ley de sociedades comerciales en la que se enmarca la actual figura jurídica de la empresa, incluyendo la renovación de aquellas autoridades que cumplieron su mandato.

De este modo, los accionistas renovaron el mandato de Oscar Barrionuevo como director obrero, mientras que fueron designados como síndicos titulares por las acciones clase “A” Diego Moñoa y Guillermo Vitali, y como suplentes Juliana Armendáriz y Alejandro Varinia; mientras que por las acciones clase "B" son síndicos titulares Delvis Ruíz Bodoira y Sabrina María Belén Arcamone y como suplentes Víctor Donadello y Hugo Gabriel Rosti; en tanto que como síndicos titulares por las acciones clase “C” Néstor Capodano y Yamila Georgina Schierson.

También se aprobó la Memoria y Balance del período 2020, con los respectivos estados contables y situación patrimonial de la sociedad.

Los representantes de los municipios y los sindicatos que integran la sociedad pudieron conectarse en forma remota por la plataforma mencionada.