---
category: Agenda Ciudadana
date: 2021-06-03T08:00:55-03:00
thumbnail: https://assets.3dnoticias.com.ar/contenedor.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Diario Uno
resumen: Instalaron en el Cullen un contenedor refrigerado por un posible colapso
  de la morgue
title: Instalaron en el Cullen un contenedor refrigerado por un posible colapso de
  la morgue
entradilla: Así lo informaron desde el Ministerio de Salud. Si bien no está en funcionamiento
  aún, describieron la acción como una "medida anticipatoria".

---
Este miércoles por la mañana, sorprendió en el interior del hospital José María Cullen, la presencia de un contenedor refrigerado instalado como "medida anticipatoria" a un posible colapso de la morgue del nosocomio público.

Rápidamente, desde el Ministerio de Salud, informaron que se trata de "una acción de anticipo, de contingencia ante cualquier situación de colapso que se pueda llegar a dar con la morgue del hospital Cullen o con demoras por parte del servicio de las funerarias".

"El contenedor no está funcionando aún y al igual con lo que sucedió con el hospital militar reubicable instalado en el predio del Liceo Militar, tiene como objetivo brindar la mejor atención posible en el marco de un alto stress sanitario que estamos transitando", indicaron a 3D Noticias desde la cartera sanitaria.

Recordaron también desde el Ministerio de Salud en relación al contenedor refrigerado instalado en el Cullen, "que la misma situación se vivió en Granadero Baigorria durante la primera ola de contagios en 2020 producto de la baja capacidad de las morgues de los hospitales públicos".

Esta semana, el director del hospital José María Cullen, Juan Pablo Poletti, volvió a mostrar su preocupación por el colapso del del sistema sanitario en la ciudad. "Desde hace 15 meses nos venimos preparando y hoy la pandemia está poniendo en jaque a todo el sistema con el aumento de casos y con la ocupación de camas, no solo críticas sino también de las generales y seguimos estirándolo lo más posible. Para cualquiera que esté en el sistema sanitario, sea del equipo que sea, ver lo de anoche realmente preocupa", subrayó Poletti.

"La buena noticia es que las restricciones nacionales que habían durado hasta el 31 de mayo hicieron que en el hospital Cullen, que es un hospital que atiende todo tipo de emergencias del centro norte de la provincia y no solo covid, donde normalmente tenemos un ingreso promedio de 12 a 15 accidentados por día", sostuvo Poletti.