---
category: La Ciudad
date: 2021-06-20T06:00:46-03:00
thumbnail: https://assets.3dnoticias.com.ar/RANCHOS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Residuos plásticos se transformarán en materiales para viviendas de Santa
  Fe
title: Residuos plásticos se transformarán en materiales para viviendas de Santa Fe
entradilla: Es un proyecto de extensión de la UNL, articulando el trabajo con vecinales
  de Santa Fe donde serán intervenidas las viviendas con residuos plásticos reciclados

---
En el marco del proyecto de extensión e integración social de la UNL, se está realizando un ciclo de capacitaciones que apunta a la manipulación de residuos plásticos para transformarlos en materia prima destinada a viviendas precarias de la ciudad. Se trata de un proyecto multidisciplinar en conjunto con distintas vecinales barriales de Santa Fe, donde se encuentran las viviendas que serán intervenidas con los materiales reciclados.

Se trata del proyecto interdisciplinar “Reciclado de Residuos Plásticos: la FIQ promoviendo cambios de hábitos y transformando vidas”. UNO Santa Fe dialogó con la directora del programa, Diana Estenoz, quien es docente de la UNL, investigadora del Conicet y directora del Departamento de Ciencia de los Materiales de la Facultad de Ingeniería Química (FIQ).

Al respecto, la directora del proyecto manifestó: "Nuestra idea es usar los residuos plásticos como una forma económica de poder producir materiales para usarlos en construcción o para aumentar el confort de las familias en general. Estos residuos no tienen ningún valor económico".

"Hemos hecho algunos estudios preliminares de las vecinales con las que vamos a trabajar, pero en realidad es algo común en todas las zonas vulnerables y humildes. Estos son problemas de basura y de vivienda, relacionados a la construcción de la vivienda, el confort y el bienestar de la gente que vive allí", aseguró Estenoz sobre la preocupante situación de los sectores marginados en Santa Fe.

En este sentido, el proyecto cuenta con varios actores intervinientes. Por un lado se encuentra la universidad, a través de sus unidades académicas, los estudiantes, docentes y personal no docente. En segundo lugar, organizaciones sociales en donde intervienen dos vecinales de barrios populares de la ciudad y una cooperativa. La tercera parte del proyecto la conforma una empresa local que procesa materiales plásticos.

Las numerosas carencias que sufren en sus viviendas miles de santafesinos en condiciones de pobreza se relacionan en gran medida con el padecimiento de las temperaturas extremas dependiendo la estación del año. Ya sea el frío invernal o el calor abrasador en verano, estas personas sufren a diario las consecuencias del clima debido a no contar con ningún tipo de aislación térmica en sus hogares.

"Uno de los mayores problemas que tiene la gente humilde en sus viviendas es la parte térmica, pasando frío en invierno y calor en verano. Poder contar con sistemas de aislación le mejoraría la calidad de vida, entonces uno puede pensar por ejemplo en realizar placas aislantes, las características del polímero permitirían ese tipo de aplicación", afirmó al respecto la investigadora del Conicet.

Además, el decano de la FIQ, Adrián Bonivardi, expresó: "El objetivo de las capacitaciones está relacionado con el propio proyecto de extensión, para vincular el trabajo hecho en el ámbito universitario tratando de dar respuestas a problemas de interés social".

A su vez, el decano de la FIQ sostuvo: "Una de las alternativas con las que se trabaja en este proyecto, conjuntamente entre la Facultad de Ingeniería Química y la Facultad de Diseño y Urbanismo (Fadu), es pensar en un aprovechamiento de la generación de materiales de tipo constructivo para viviendas sociales. En este caso en particular se puede ver lo social desde distintos lugares con estas prácticas. De por sí el residuo es un problema y el desafío es brindar una alternativa a sectores sociales carenciados que termine siendo una solución".

"Si uno piensa en el porcentaje de gente que vive en esas condiciones hoy en día es muy alto. La idea es usar los residuos como una materia prima, transformando los residuos en recursos a muy bajo valor, para obtener productos en una manera económica y ambientalmente sustentable para mejorar la calidad de la vida de la gente", concluyó Estenoz.