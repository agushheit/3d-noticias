---
category: La Ciudad
date: 2021-08-11T06:00:11-03:00
thumbnail: https://assets.3dnoticias.com.ar/CASATOMADA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Juan Ignacio Rodríguez para El Litoral
resumen: Usurparon la antigua casa de Sor Josefa
title: Usurparon la antigua casa de Sor Josefa
entradilla: 'En los últimos años hubo intentos para tratar de restaurarla, pero todo
  quedó en la nada. '

---
Desde hace más de 20 años, la antigua casona que perteneciera a Sor Josefa Díaz y Clucellas (ubicada en la intersección de las calles La Rioja y San Luis, pleno macrocentro capitalino), pasa de "mano en mano": de la Provincia a la Municipalidad, y viceversa.

Ese "ida y vuelta" es lo menos conveniente para una estructura que necesita una restauración profunda, y que fue declarada por ley Monumento Histórico Provincial y de interés cultural de la ciudad por un decreto municipal.

Pero más allá de las cuestiones edilicias, ahora (y desde hace un buen tiempo), la preocupación pasa por otro lado: los vecinos y comerciantes de la zona se sienten inseguros, porque el histórico inmueble está "ocupado por familias enteras, es decir, vive gente de todas las edades. Entran, salen, cargan agua, se observan colchones...", comentó una de las vecinas a El Litoral.

En 2018, se publicó un artículo que hizo visible el estado de abandono de la casa y, de esa manera, el tema volvió a la agenda pública. Así fue como la Provincia rescindió el comodato con la Municipalidad para volver a tomar posesión del inmueble y restaurarlo. Algo que nunca ocurrió, a excepción de algunos trabajos de apuntalamiento que se hicieron en 2019 para evitar desmoronamientos. Luego cambió el gobierno provincial, crisis, pandemia y como resultado, la obra nunca se inició.

Desde ese entonces, aseguran los vecinos, empezó a haber más problemas en la zona, como, por ejemplo, el robo íntegro al depósito de un comercio.

"Nos movilizamos todo lo que pudimos. Defensoría del Pueblo, Centro Territorial de Denuncia, policía, con medios de comunicación. Esta gente es muy peligrosa para todo el barrio. En septiembre del año pasado también hicimos una movida, pero no pasó nada", expresaron.

La gente continuó alzando la voz para hacerse escuchar: "Hará unos dos meses atrás, colocaron unas vallas (serían los chapones que hoy lucen las publicidades), pero lo que terminaron haciendo fue acomodar unas cosas para que la gente siga viviendo. Tienen agua, tal vez gas. También se sienten olores muy feos de ahí".

Sobre calle San Luis, claramente se observa el lugar que debería ser el paso para que entre y salga gente para mantener la casona, lo que sería la "puerta" de ingreso para los "ocupas" que cuando colocan candados, se encargan de romperlos.

Sobre el final, un pedido claro: "Ya no sabemos dónde más acudir. Es una zona de comercios, pero también mucha gente que vive, y todos están muy preocupados".