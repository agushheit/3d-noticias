---
category: La Ciudad
date: 2021-09-11T06:00:52-03:00
thumbnail: https://assets.3dnoticias.com.ar/REGISTROCIVIL.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: El Registro Civil de Santa Fe abrirá sábado y domingo para retiro de DNI
title: El Registro Civil de Santa Fe abrirá sábado y domingo para retiro de DNI
entradilla: Este domingo 12 de septiembre se realizan las Primarias Abiertas, Simultáneas
  y Obligatorias (Paso)

---
En la provincia de Santa Fe se elegirán los candidatos que van a competir por 12 bancas en el Congreso de la Nación en la elección general del 14 de noviembre. Además, se realizan las internas para definir los candidatos que aspiran a los puestos de autoridades provinciales que culminan sus mandatos al frente de intendencias, concejos municipales y comunas.

El Gobierno provincial informó que las oficinas del Registro Civil de Santa Fe estarán abiertas para el retiro de DNI durante las jornadas del sábado y domingo, con motivo del acto eleccionario.

Al respecto, detallaron que el horario de atención será:

Sábado 11: de 9 a 12 hs.

Domingo 12: de 9 a 16 hs.

Recordamos que en la capital santafesina la oficina está ubicada en San Luis 2950 y en Rosario en Salta 2752.