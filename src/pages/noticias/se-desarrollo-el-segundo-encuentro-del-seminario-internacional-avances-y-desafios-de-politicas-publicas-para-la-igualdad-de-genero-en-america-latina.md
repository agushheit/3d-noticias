---
category: Estado Real
date: 2021-06-11T08:41:39-03:00
thumbnail: https://assets.3dnoticias.com.ar/genero.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: Se desarrolló el segundo encuentro del Seminario Internacional “Avances y
  desafíos de políticas públicas para la Igualdad de Género en América Latina”
title: Se desarrolló el segundo encuentro del Seminario Internacional “Avances y desafíos
  de políticas públicas para la Igualdad de Género en América Latina”
entradilla: Organizado por el Programa de Cooperación Triangular Costa Rica – España,
  el Instituto Nacional de las Mujeres de Costa Rica y el Gobierno de la Provincia
  de Santa Fe.

---
El Gobierno de Santa Fe, a través de la Secretaría de Estado de Igualdad y Género, el Instituto Nacional de las Mujeres (INAMU), y la Secretaría de Cooperación Internacional e Integración Regional, llevaron adelante el segundo encuentro del Seminario Internacional “Avances y desafíos de políticas públicas para la Igualdad de Género en América Latina”. Esta jornada abordó las políticas de cuidados y del trabajo para la autonomía económica de las mujeres.

El INAMU y la Secretaría de Estado de Igualdad y Género de la provincia de Santa Fe, iniciaron un proceso de cooperación para la construcción y seguimiento de Políticas Públicas para la Igualdad de Género que concluirá con la formulación del Plan Estratégico Provincial de Igualdad 2020-2030 de Santa Fe. Este proyecto de cooperación triangular se enmarca en la Cuarta Fase del Programa de Cooperación Triangular Costa Rica-España para el fortalecimiento de las capacidades de la Provincia de Santa Fe.

Durante el segundo encuentro expusieron Karina Batthyány, secretaria Ejecutiva del Consejo Latinoamericano de Ciencias Sociales; Francisco Delgado Jiménez, viceministro Desarrollo Humano e Inclusión Social de Costa Rica; y Juan Manuel Pusineri, ministro de Trabajo, Empleo y Seguridad Social de la Provincia de Santa Fe.

Al momento de disertar, Juan Manuel Pusineri expresó: “El desempleo golpea con mucha más fuerza a las mujeres que a los hombres. En particular a las mujeres jóvenes. La remuneración que reciben las mujeres, en general, es sensiblemente inferior a la que perciben los hombres. Los puestos directivos en instituciones públicas o privadas son mayormente ocupados por hombres.En el ámbito provincial se lleva adelante, a través del Ministerio de Trabajo, Empleo y Seguridad Social, la puesta en funcionamiento de los centros de cuidado infantil en todo el territorio provincial”, continuó el Ministro. En ese sentido, “estamos llevando adelante el programa Futuras, destinado a mujeres jóvenes, que son la población que claramente está siendo identificada como la más golpeada por el desempleo y la situación de precariedad”, finalizó.

Karina Batthyány comenzó explicando que “el cuidado designa la acción de ayudar a un niño, una niña o una persona dependiente en el desarrollo y en el bienestar de su vida cotidiana. Afirmar esto quiere decir que reconocemos dentro del cuidado tres grandes dimensiones. Una primera dimensión de corte material, que implica un trabajo; una segunda de corte económica, que implica un costo y una tercera dimensión de índole subjetiva, que implica el vínculo que se establece en las relaciones de cuidado”. Luego agregó: “Hay muchas investigaciones que se han llevado a cabo en América Latina que muestran una desigual distribución en términos de la organización social del cuidado, que determina que las responsabilidades del cuidado recaigan en los hogares y, dentro de los hogares, en las mujeres”. Finalmente señaló que “hay una relación entre políticas de cuidado y la igualdad de género. El cuidado llega a la agenda pública de la mano de la agenda de género y es necesario que ambas agendas se encuentren juntas”.

Por su parte, Francisco Delgado Jiménez destacó el rol que ha tenido el Instituto Nacional de las Mujeres en posicionar el tema de los cuidados en diferentes momentos sosteniendo que “en la política de igualdad y equidad de género del 2007 se hacía énfasis en la necesidad de contar con servicios de cuidado y desarrollo infantil. En la política de igualdad efectiva de mujeres y hombres del 2018 se vuelve a colocar el tema, pero desde la perspectiva de las situaciones de dependencia”. En efecto, “se espera que en el 2050 la demanda de servicios de empleados para cuidados de personas mayores va a ser cuatro veces mayor que la que tenemos actualmente. Sabemos que las actividades de cuidado se realizan de manera no remunerada en el 90% de los casos y 7 de cada 10 personas cuidadoras en nuestro país son mujeres”, indicó Delgado Jiménez.“Hay muchas mujeres que podrían vincularse al mercado de trabajo en sus áreas de especialidad y que no lo pueden hacer porque la sociedad les ha impuesto la tarea de cuidar. Al existir un sistema de cuidados, se liberan recursos que estaban en los hogares realizando esas tareas y pueden ser vinculadas en otras actividades o en otras industrias”, concluyó.

**Acerca del programa**

El proyecto Cooperación Triangular Costa Rica - España - Argentina forma parte de la IV fase del Programa Cooperación Triangular Costa Rica-España que es co-financiado con el apoyo de la Agencia Española de Cooperación para el Desarrollo (AECID) e implementado por el Instituto Nacional de las Mujeres (INAMU) y el Gobierno de Santa Fe. El Programa Cooperación Triangular Costa Rica-España promueve la Cooperación Sur-Sur en el ámbito latinoamericano y caribeño, e incentiva, particularmente, la cooperación desde un país de renta media como Costa Rica hacia otros países del istmo y Caribe.

En el marco del proyecto de Cooperación Triangular Costa Rica-España-Argentina , todos los miércoles de junio se llevará a cabo el Seminario Internacional: Avances y desafíos de políticas públicas de Igualdad de Género en América Latina. Es pretensión del Seminario que se adquieran insumos para el mejoramiento de las políticas así como, material valioso para la elaboración del Plan Estratégico Provincial de Igualdad 2020-2030 de Santa Fe.

En este seminario destacan cuatro ejes temáticos que fueron definidos en función de asuntos que se consideran medulares para alcanzar la igualdad y que se trabajan en la región en el marco de las políticas de igualdad de género y que hoy en día cobran mayor importancia debido a la afectación por la pandemia mundial por la COVID 19, principalmente en el ámbito económico, de bienestar y seguridad personal.

Del próximo encuentro participarán Celia Arena, secretaria de Estado de Igualdad y Género de la provincia de Santa Fe; y Roxana Arroyo Vargas, consultora permanente del Programa Mujer, Justicia y Género del Instituto Latinoamericano de Naciones Unidas para la Prevención del Delito y Tratamiento del Delincuente (ILANUD). Moderará Ornela Grossi, directora Provincial de Protección Integral contra las Violencias de la Secretaria de Estado de Igualdad y Género de Santa Fe.

Se transmitirá en vivo por el Facebook de la Secretaría de Estado de Igualdad y Género todos los miércoles a las 17 hs.