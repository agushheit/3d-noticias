---
category: Agenda Ciudadana
date: 2021-03-23T07:28:36-03:00
thumbnail: https://assets.3dnoticias.com.ar/relleno-sanitario.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Diario UNO
resumen: 'Santo Tomé: el 15 de abril será licitada la obra de saneamiento del relleno
  sanitario'
title: 'Santo Tomé: el 15 de abril será licitada la obra de saneamiento del relleno
  sanitario'
entradilla: '"Este es el paso imprescindible para tomar las decisiones que nos permitan
  avanzar hacia una solución en el tratamiento y disposición final de los residuos",
  explicó Daniela Qüesta.'

---
La intendenta Daniela Qüesta confirmó que el municipio de Santo Tomé prosigue con el proceso de adjudicación de la obra de saneamiento del predio de disposición final de residuos de la localidad, más conocido como relleno sanitario. En tal sentido, adelantó que el próximo 15 de abril se realizará el segundo llamado a licitación de dicho emprendimiento, ya que el primero quedó desierto. 

En diciembre de 2018, la firma Cotreco SA, que tenía a su cargo el Centro de Selección, Recuperación, Reciclado y Disposición Final de los Residuos Sólidos Urbanos de Santo Tomé (también denominado Centro de Recolección de Residuos, o simplemente relleno sanitario), se retiró de la planta de tratamiento de la basura ubicada a la altura del kilómetro 4.7 de la Autovía Nº 19, y dejó de realizar el servicio integral y diferenciado que prestaba desde el año 2015. 

Consultado sobre el proyecto de esta obra, el secretario municipal de Servicios Públicos, Ricardo Méndez, destacó que consistirá en la limpieza general y saneamiento de todo el predio. Méndez explicó que el sistema que utilizaba Cotreco estaba basado en la compactación y el embolsado de los residuos, por medio de una procesadora especial, que era la que producía los llamados "caramelos", que en principio se estivaban en pilas armadas ordenadamente. 

Al retirarse del lugar, la ex administradora dejó muchos de esos "caramelos" esparcidos por el terreno, desordenados, sin estivar, por lo que están en el piso y hay que levantarlos, para luego depositarlos como corresponde, para sanear todo el sector.  

Se trata de un movimiento que deberá hacerse adentro del predio (ordenar los "caramelos", armar las pilas, revestirlas, taparlas), para el cual se necesitará un equipamiento acorde y máquinas pesadas (bateas, retropalas, topadoras, motoniveladoras). Originalmente, la obra estaba presupuestada en unos 10 millones de pesos (todavía no se ha dado a conocer el presupuesto para el segundo llamado).

 

**Dentro de un plan ambiental**

En su discurso de apertura de las sesiones ordinarias del año 2021 del Concejo local, la jefa municipal destacó que el llamado a licitación por la obra del relleno sanitario "es una acción importante", porque implica "el paso imprescindible para tomar las decisiones que nos permitan avanzar hacia una solución en cuanto al tratamiento y disposición final de los residuos sólidos urbanos de la ciudad".

 Qüesta explicó, además, que el equipo de Gestión Ambiental de la Municipalidad de Santo Tomé "viene trabajando en distintas propuestas, para conformar un plan ambiental para la ciudad, con el objetivo de hacer de nuestra ciudad un territorio capaz de desarrollarse amigablemente con el medio natural que lo rodea". El saneamiento del relleno sanitario forma parte de ese trabajo integral, que, además está compuesto por una serie de programas de acción, a su vez vinculados con cuatro ejes de trabajo: gestión integral de los residuos sólidos urbanos; arbolado público y áreas naturales; gestión del cambio climático y educación ambiental. 

En cuanto al primer eje, el programa de gestión de residuos, Qüesta aclaró que, justamente, va a comenzar con el saneamiento del relleno sanitario, "proyecto que, como dijimos, se encuentra en proceso de licitación", remarcó. Una vez saneado el sitio, agregó, "nos proponemos refuncionalizarlo, es decir, aprovechar las instalaciones y la superficie del predio (que son 9 hectáreas), sectorizarlo para que sirva, además, como centro de transferencia de residuos, centro de compostaje y acopio de materiales de valor comercial".

Otras acciones de este programa son la recolección diferenciada de la basura; la recolección en los denominados Puntos Verdes; la Gestión Integral de Aceites Vegetales Usados y la Gestión Ambientalmente Sustentable de Neumáticos Fuera de Uso.

 

**65 TONELADAS**

La ciudad de Santo Tomé genera 65 toneladas por día -aproximadas- de basura, las que desde 2019 están siendo derivadas al relleno sanitario de la ciudad de Santa Fe, según datos brindados por Hernán Reynoso, miembro de la cooperativa de trabajo Desafío EcoSantoto. Esta entidad solicitó ante el Concejo Municipal la conformación de una mesa territorial para el cuidado del medioambiente.

 

**Seguimiento** 

A lo largo de los últimos años, El Litoral realizó una extensa y continua cobertura del tema del relleno sanitario y el problema surgido ante el retiro de la firma Cotreco SA, que lo administraba. 

En tal sentido, a través de la web del diario pueden leerse las siguientes notas: "Conflicto con los residuos de Santo Tomé" (20 de diciembre de 2018); "Santo Tomé se quedó sin empresa que se ocupe de los residuos que genera" (21 de diciembre de 2018); "Destinan $ 2,3 millones para la gestión de residuos urbanos" (21 de febrero de 2019); "Requieren la presencia de la intendenta para que informe sobre el tema residuos" (4 de noviembre de 2019); "Pedirán casi $ 10 millones para reactivar el Centro de Residuos", y "En Santo Tomé reclaman por la falta de gestión de los residuos urbanos" (13 de febrero de 2021).