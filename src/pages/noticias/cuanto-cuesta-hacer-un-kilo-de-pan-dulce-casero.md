---
category: Agenda Ciudadana
date: 2022-12-18T08:33:17-03:00
thumbnail: https://assets.3dnoticias.com.ar/stollen-1768907_1920.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: "¿Cuánto cuesta hacer un kilo de pan dulce casero?"
title: "¿Cuánto cuesta hacer un kilo de pan dulce casero?"
entradilla: 'Este cronista, se aventuró a las artes gastronómicas desde muy pequeño,
  pero nunca había hecho un pan dulce. Experto en budines y pan casero, creyó que
  era el momento de dar ese gran paso.  Y contarlo.  '

---
Harina 000. Para la receta que utilizaré, solo necesito 500 gramos. El precio varía según las marcas y también el lugar donde se compre, pero vamos a elegir los insumos, comprando en un supermercado local grande.

Para empezar, hacemos una “esponja”: dos cucharadas de harina, una de azúcar, un chorrito de leche tibia y 30 gramos de levadura fresca. Batir bien hasta que se disuelva la levadura, y dejar reposar unos minutos hasta que doble su volumen. La levadura que usamos es el cubito de Calsa, que pesa 50 gramos.

Mientras, en un bol, vertemos el medio kilo de harina, hacemos un hueco en el medio, que quede como una corona. En el medio, echamos dos huevos, 60 gramos de azúcar, 80 gramos de manteca pomada, es decir, blanda. Una cucharadita de sal, 200 cc de leche tibia, ralladura de un limón, una cucharadita de esencia de vainilla y una cucharadita de esencia de pan dulce, esto último es opcional. Y empezamos a amasar, no menos de 15 minutos. Lo más caro en este párrafo, es sin dudas la manteca. Un paquete chico de manteca pesa 100 gramos. Usamos una marca que está en oferta.

NO COMETAN EL ERROR DE PONERLE MAS HARINA A LA MASA. Es así, blandita, medio pegajosa, pero ligera. Una vez terminado, la dejamos en un bol tapado con film, que repose por media hora, no mucho más. Depende el ambiente. No la mala onda, ni nada de eso, sino la temperatura.

Mientras tanto, pesamos la fruta abrillantada, pasas de uva, nueces y chips de chocolates, porque a mi hija le gusta así. Para el kilo de pan dulce, yo usé 400 gramos en total para el relleno.

Pasada la media hora, estiramos la masa sobre la mesada, de forma tal que quede rectangular de mas o menos 2 centímetros de espesor. Le agregamos los rellenos, enrollamos y volvemos a bollar la masa. Yo compré un molde, pero si no tenés, hacelo sin molde, tipo panetone. Dejamos que doble su volumen, y luego, a horno bajo, 180 grados, por 45 minutos. Para no errarle, con un cuchillo lo pinchamos en el medio, y si sale seco, ya esta listo.

Finalmente, con tres cucharadas de azúcar impalpable, y una de limón, batimos para hacer el glasé que va a cubrir el pan dulce.

**_El costo total fue de $ 575, pero pesó un kilo doscientos gramos._**

**_Y así fue como pude pasar la tarde del sábado pensando en otra cosa que no sea la final._**

**_¡Vamos Argentina carajo!_**