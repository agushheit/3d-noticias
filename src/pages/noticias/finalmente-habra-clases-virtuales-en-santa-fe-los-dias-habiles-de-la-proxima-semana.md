---
category: Agenda Ciudadana
date: 2021-05-23T06:15:38-03:00
thumbnail: https://assets.3dnoticias.com.ar/clases.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Finalmente, habrá clases virtuales en Santa Fe los días hábiles de la próxima
  semana
title: Finalmente, habrá clases virtuales en Santa Fe los días hábiles de la próxima
  semana
entradilla: El Ministerio de Educación emitió una nueva circular para informar que
  quienes tengan conectividad podrán brindar actividades a través de las plataformas
  disponibles, siempre que no implique movilidad docente.

---
Con la firma del Director Provincial de Educación Privada, Rodolfo Fabucci, el Ministerio de Educación de Santa Fe emitió este sábado una nueva circular (n° 19) para complementar lo dispuesto en la Circular 12 emitida el viernes, que no permitía a las escuelas ni siquiera dictar clases virtuales y tanto rechazo generó en los colegios santafesinos, ahora se prevén modificaciones en las actividades académicas para las jornadas escolares de los días 26, 27 y 28 de Mayo.

En cuanto al sostenimiento del vínculo pedagógico, se recupera la modalidad de tareas en el marco de la educación en la distancia teniendo en cuenta:

a) Para quienes tengan problemas de conectividad, entregar cuando vayan a buscar la asistencia alimentaria actividades para cubrir los días 26, 27 y 28, recordando lo hagan restringiendo al máximo la circulación y permanencia en los lugares de las Escuelas para tal fin.

b) Para quienes tengan conectividad se podrá brindar durante estos tres días material y/o actividades de consulta para los alumnos a través de las plataformas que dispongan, siempre que no implique movilidad docente.

"Reiterando la necesidad de cuidar y cuidarnos, y la preocupación de cumplir con las normativas del Poder Ejecutivo para bajar la curva epidemiológica, pedimos que exhorten a las familias de sus educandos el mayor compromiso para que la salud de todas y de todos se enriquezca con la solidaridad y alteridad para afrontar esta pandemia", concluye el documento.