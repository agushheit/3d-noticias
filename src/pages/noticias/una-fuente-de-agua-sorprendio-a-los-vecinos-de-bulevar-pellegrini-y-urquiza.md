---
category: La Ciudad
date: 2021-04-20T06:31:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/una-fuente-agua-pleno-bulevar-la-situacion-se-origino-los-trabajos-aguas-santafesinas-el-lugar.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Diario Uno
resumen: Una "fuente de agua" sorprendió a los vecinos de Bulevar Pellegrini y Urquiza
title: Una "fuente de agua" sorprendió a los vecinos de Bulevar Pellegrini y Urquiza
entradilla: La situación se generó a raíz de un trabajo que Aguas Santafesinas (ASSA)
  realiza en la zona desde hace ya una semana.

---
Este lunes por la tarde, vecinos, transeúntes y conductores que circulaban por la zona de Bulevar Pellegrini y Urquiza, en barrio Mariano Comas, se sorprendieron con la aparición por algunos minutos de una " fuente de agua" de casi 5 metros de alto en dicha intersección.

Lo cierto es que la insólita situación se debió a una serie de arreglos que realiza Aguas Santafesinas (ASSA) en la zona desde hace ya una semana. Desde la empresa, informaron a UNO Santa Fe que la pérdida de agua a presión se solución minutos después de las 18.30.

El problema se originó en la instalación de una cañería distribuidora de agua. Apenas se originó el inconveniente, "los operarios en el lugar cerraron las válvulas para trabajar sin presión", informaron desde Aguas Santafesinas.

Producto de la situación, desde ASSA también comunicaron la falta de presión en el servicio de agua potable en la zona.