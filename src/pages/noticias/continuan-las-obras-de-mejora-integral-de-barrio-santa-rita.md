---
category: La Ciudad
date: 2021-06-19T18:00:07-03:00
thumbnail: https://assets.3dnoticias.com.ar/SANTARITA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Continúan las obras de mejora integral de barrio Santa Rita
title: Continúan las obras de mejora integral de barrio Santa Rita
entradilla: En el marco del Plan Integrar y luego de los trabajos hídricos y del cambio
  de la iluminación, avanzan las tareas de ripio, que se realizan en unas 30 cuadras.
  El intendente Emilio Jatón recorrió la obra con los vecinos.

---
Se llevan a cabo los trabajos de ripiado en unas 30 cuadras del barrio Santa Rita. Las obras se enmarcan en las mejoras integrales que se están realizando en ese sector de la ciudad, que incluyó obras hídricas que estaban pendientes de gestiones anteriores y mejoras en la iluminación. Cada una de las tareas surge de un orden de prioridades acordado con las vecinas y vecinos a través de la Red de instituciones barriales.

El intendente Emilio Jatón recorrió las calles este jueves y supervisó las tareas que allí se hacen junto con los vecinos, quienes le agradecieron la intervención y destacaron el cambio que está teniendo el barrio, donde hace más de 20 años no ingresaba una máquina. Ahora es incesante el ir y venir de los camiones con piedras, mientras la motoniveladora las acomoda en la calle.

Matías Pons Estel, secretario de Obras y Espacio Público municipal, destacó los avances y el cambio que ya se aprecia en diferentes sectores de la ciudad. En esta línea ponderó el rol activo de los vecinos: “Estamos trabajando en un frente continuo de obras; y se trata de un trabajo muy fuerte con los vecinos y la red de instituciones con los representantes del Estado, donde pudimos establecer un cuadrante a intervenir. Era importante llegar al barrio y ver qué había que hacer, sobre todo en las calles troncales que son Chaco y Florencio Fernández”.

En necesario destacar que la intervención sobre Florencio Fernández ya finalizó y ahora hay un equipo sobre Chaco y “de esta manera se da accesibilidad completa al barrio”, dijo el funcionario, pero al mismo tiempo destacó que “otro equipo trabaja en las calles interiores de Santa Rita”. Vale recordar también que previamente se hizo una intervención para recuperar los sistemas hídricos que se encontraban, en su gran mayoría, obstruidos.

“Se trata de una obra integral con el fin de que el barrio quede en mejores condiciones. Dentro de la política de Estado con acciones que se vienen llevando a cabo en cada uno de los barrios, es muy importante, por un lado, el trabajo con el vecino quien nos determina las prioridades y problemáticas”, destacó Pons Estel, quien aseguró que en dos semanas estarían culminando las tareas.

**Alegría y emoción**

Mercedes Ojeda vive sobre calle Montiel y este jueves se levantó con el ruido de la máquina que desparramaba el ripio frente a su casa. “Estoy muy feliz porque ahora por lo menos vamos a poder salir. El mejorado es tan importante porque no podíamos ni cruzar la calle para comprar algo. Esto que están haciendo nos alegra mucho, nos cambió la cara a todos los vecinos del barrio. Todavía están trabajando, sabemos que no se va a terminar ni hoy ni mañana, pero es un comienzo y seguro llegarán más cosas para Santa Rita”, dijo emocionada.

Antes de finalizar, manifestó: “Sabemos que no hay mucho dinero, pero las cosas se están haciendo. Ahora vamos a poder salir sin quedarnos empantanados, para los que tienen auto; y los que salen caminando, no embarrarse o correr el riesgo de caerse. Por eso es muy importante el ripio y estamos muy agradecidos y felices”.

Verónica Varela se acercó al intendente para agradecerle la obra y manifestarle lo importante que es para el barrio el ripio. “Estamos re contentos, era algo muy esperado. Hace 15 años que vivo acá y siempre reclamamos por el mejorado. Hace un mes y medio tuvimos siete horas esperando que la ambulancia llegara para atender a mi suegra porque las calles estaban todas rotas y tuvimos que llevarla en brazos hasta la esquina; hasta eso tuvimos que padecer”, relató.

“Hoy salimos a la vereda y no lo podemos creer. Faltan cosas, pero esto era lo más importante; ahora podemos transitar con dignidad la calle, podemos salir a trabajar o hacer otra actividad caminando o en auto tranquilos, bien porque antes era imposible. Ahora esto es otra cosa, la realidad empezó a cambiar para Santa Rita”, finalizó también emocionada como su vecina.