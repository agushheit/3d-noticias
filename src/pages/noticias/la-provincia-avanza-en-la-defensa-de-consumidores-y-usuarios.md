---
category: Estado Real
date: 2020-12-26T11:53:48Z
thumbnail: https://assets.3dnoticias.com.ar/consumidores.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: La provincia avanza en la defensa de consumidores y usuarios
title: La provincia avanza en la defensa de consumidores y usuarios
entradilla: Se realizó el primer conversatorio del Ciclo Conversaciones sobre Consumo
  y Derechos.

---
El secretario de Comercio Interior y Servicios, Juan Marcos Aviano, y la directora Provincial de Promoción de la Competencia y Defensa del Consumidor, María Betania Albrecht, encabezaron el primer conversatorio de un ciclo llamado «Conversaciones sobre Consumo y Derechos», instancia en la que se abordaron diferentes temáticas que tienen que ver con la prevención de conflictos y la promoción de derechos de consumidores y usuarios, siendo tema de la primera jornada «Sociedad de la Información. El consumo en tiempo de Internet».

Albrecht destacó: «En contexto de pandemia, se aceleraron los tiempos en la evolución del modo de interrelación entre la ciudadanía y las compras y consumos a través de internet, los cuales crecieron exponencialmente y, aparejado a ello, también lo hicieron los conflictos de consumo».

En virtud de esta situación, explicó que «han surgido normativas que tienden a proteger a consumidores y usuarios en el ámbito de las contrataciones a distancia, ejemplo de ello es el botón de arrepentimiento o el botón de baja, entre otras. En ese marco, nos pareció una importante temática para abordar en este primer conversatorio, aportando ideas y creatividad para resolver colectivamente los inconvenientes que surjan».

Asimismo, la funcionaria provincial informó: «Durante 2021 continuaremos con estos conversatorios, apostando a jerarquizar la protección de los derechos de consumidores y usuarios. En esta primera jornada, además de la intervención de integrantes de la Secretaría de Comercio Interior, se contó con las disertaciones de la Dra. Valería Vaccaro y la Dra. Lucía Aguilar, de la Asociación Civil Unión de Usuarios y Consumidor, entidad que se dedica a la defensa de derechos de consumidores en la provincia de Santa Fe».

«Desde el comienzo de la pandemia, la Secretaría se encuentra trabajando en la mejora de los sistemas de gestión de reclamos vía online, debido a la necesidad de evitar aglomeramientos para cuidar la salud de los usuarios, esperando iniciar el año próximo con un sistema aún más ágil», concluyó Albrecht.

El conversatorio contó con importante participación de público entre los cuales se encontraban representantes de asociaciones de defensa de consumidores, integrantes de oficinas municipales de información al consumidor, operadores jurídicos, gestores de reclamos de consumidores e integrantes de cátedras de derecho del consumidor de universidades de la provincia.