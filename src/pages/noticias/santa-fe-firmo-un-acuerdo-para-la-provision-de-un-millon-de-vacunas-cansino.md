---
category: Agenda Ciudadana
date: 2021-06-12T06:15:53-03:00
thumbnail: https://assets.3dnoticias.com.ar/vacunascansinojpg.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Santa Fe firmó un acuerdo para la provisión de un millón de vacunas CanSino
title: Santa Fe firmó un acuerdo para la provisión de un millón de vacunas CanSino
entradilla: Así lo informó el gobernador Omar Perotti a través de redes sociales.
  Se trata de las vacunas chino-canadienses, de una dosis.

---
El gobernador de Santa Fe Omar Perotti confirmó que la provincia firmó un acuerdo para la provisión de un millón de vacunas CanSino. Se trata de la vacuna chino-canadienses, de una dosis.

A través de Twitter, el mandatario informó que "el acuerdo de abastecimiento firmado por nuestra provincia, está acompañado por el laboratorio argentino Cassará".

El anuncio de Perotti llega luego de que el gobierno nacional autorice "el uso de emergencia" de dicha vacuna. "Este convenio será complementario al proceso que viene llevando adelante la Nación. Entre todos, estamos buscando más vacunas", añadió.

"En medio de la complejidad actual y la escasez de dosis, tenemos acuerdos para la provisión de vacunas con Bharat Biotech International (vacuna Covaxin), con Jhonson & Jhonson (vacuna Janssen) y con el Laboratorio G42 de Emiratos Árabes (vacuna Sinopharm)", escribió Perotti.

**Uso de emergencia**

El Ministerio de Salud que conduce Carla Vizzotti "autorizó hoy con carácter de emergencia la vacuna Convidecia" del laboratorio chino Cansino, por lo que se sumará al plan de inmunización que lleva adelante el Gobierno nacional.

"Se autoriza con carácter de emergencia la vacuna Convidecia de Cansino (Recombinant Novel Coronavirus Vaccine (Adenovirus Type 5 Vector) del laboratorio Cansino Biologics Inc (Beijing institute of Biotechnology)", indicó la resolución publicada por la cartera sanitaria.

Agregó que esta autorización se da "de conformidad con las recomendaciones de la Administración Nacional de Medicamentos, Alimentos y Tecnología Médica (Anmat)", que "autorizó" el cumplimiento de los estándares requeridos por la autoridad regulatoria de las plantas elaboradoras, el desarrollo y la elaboración de los productos, su certificación en el país de origen y el cumplimiento de los estándares de calidad.

Para dar su autorización, la Anmat accedió a información sobre su seguridad y eficacia de la vacuna, así como a la que indica que no se han presentado eventos adversos graves, ni se han identificado diferencias significativas en la eficacia observada en los diferentes grupos etarios que participaron de los ensayos clínicos, según precisa la resolución.

El Ministerio de Salud, por su parte, a través de un Plan de Gestión de Riesgos creado especialmente para las vacunas en su calidad de adquirente, "recolectó información relacionada a la seguridad y eficacia del producto, tal como lo hace con todas las vacunas, y el registro de posibles efectos adversos o clínicamente significativos, acorde a los establecido en los esquemas vigentes".

La adquisición de los inmunizantes se da en el marco de la "emergencia sanitaria" y "en un contexto internacional que limita el acceso a otras alternativas" de dosis "por la disputa global de vacunas", se recordó en la resolución.

Agregó que "dado que los beneficios son superiores a los riesgos potenciales para la salud de la población, la Anmat ha recomendado otorgar la Autorización de Emergencia de la vacuna Convidecia".

La autorización de esta vacuna, como la del resto de las que ya se aplican en el país, tiene como marco la ley 27.491, que entiende a la inmunización como "una estrategia de salud pública preventiva y altamente efectiva y la considera como bien social, sujeta a principios de gratuidad, interés colectivo, disponibilidad y amplia participación, declarándola de interés nacional", dice la resolución en sus considerandos.

En Argentina ya se aplican las vacunas Sputnik V, Sinopharm, AstraZeneca y en breve se comenzará con la Covaxin.

En la resolución se recuerda que la pandemia por la Covid-19 producida por el coronavirus SARS-Cov-2 "ha causado la pérdida de cientos de miles de vidas en el mundo" y que "el desarrollo y despliegue de una vacuna segura y eficaz para prevenir la Covid-19 es determinante para lograr controlar el desarrollo de la enfermedad, ya sea disminuyendo la morbimortalidad o bien la transmisión del virus".

"Por ello, contar con una vacuna no solo permitirá mejorar sustancialmente el cuidado de la vida y la salud de los y las habitantes del país, sino también permitirá ir restableciendo en plenitud las actividades económicas y sociales", sigue la resolución.

La vacuna posee, según determinó la Dirección de Evaluación y Control de Biológicos y Radiofármacos (DECBR) "autorización de comercialización condicional emitida por la autoridad sanitaria de la República Popular China" (NMPA) y "se encuentra autorizada para uso de emergencia por la autoridad sanitaria Hungarian National Institute of Pharmacyand Nutrition de Hungría, la Comisión Federal para la Protección contra Riesgos Sanitarios (COFEPRIS) de México y la autoridad Sanitaria de Pakistán".

Estas entidades, en conjunto con la Comisión Nacional de Seguridad en Vacunas, "ha desarrollado un sistema de vigilancia que permite detectar los eventos supuestamente atribuidos a vacunas e inmunizaciones (ESAVI) y realizar un correcto análisis y clasificación de los mismos, a fin de poder contar con una herramienta que garantice la seguridad de las vacunas utilizadas y permita su adecuada vigilancia".