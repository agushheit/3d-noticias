---
category: Agenda Ciudadana
date: 2021-07-24T06:15:21-03:00
thumbnail: https://assets.3dnoticias.com.ar/sputnikVACINE.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Santa Fe es la provincia que más necesita segundas dosis de Sputnik V
title: Santa Fe es la provincia que más necesita segundas dosis de Sputnik V
entradilla: La provincia aplicó un 23% de segundas dosis a mayores de 60 años y lidera
  el ranking de distritos con menor porcentaje de aplicación de segundos componentes,
  según el sitio web Chequeado.

---
Con el escenario de falta de segundas dosis de Sputnik V en el país, Santa Fe aparece como la provincia que más necesita segundos componentes de la vacuna rusa. Según precisó el portal web Chequeado, a más del 76% de las personas mayores de 60 años que iniciaron su esquema de inmunización con Sputnik V en la provincia aún no se les aplicó la segunda dosis.

La incertidumbre crece entre las 279.290 personas mayores de 60 años que fueron inoculadas en la provincia con el primer componente del inmunizante ruso, puesto que en su gran mayoría se están cumpliendo el plazo de 12 semanas pactado como ventana entre una y otra dosis pero no se está turnando por falta de disponibilidad.

El sitio web Chequeado elaboró un gráfico en donde se muestran los porcentajes de personas vacunadas con primeras y segundas dosis en todas las provincias del país. Santa Fe lidera el ranking en cuanto al distrito con menor porcentaje de aplicación de segundas dosis a personas mayores en la Argentina.

"En valores absolutos hay un total de 1.073.887 personas mayores de 60 años en la Provincia de Buenos Aires que están esperando la segunda dosis de Sputnik V; en CABA, 332.148; y en Santa Fe, 279.290", publica el portal web Chequeado.

Hasta el momento en Santa Fe se vacunaron 704.414 personas con primera dosis de Sputnik V, la que fue destinada principalmente a inocular a la población objetivo de mayores de 60 años. Con respecto a la segunda dosis, solo se pudieron colocar 180.467 inmunizantes, atado a la falta de stock en las vacunas rusas a nivel país.

Según el informe de Chequeado, en el país "del total de mayores de 60 años vacunados con la Sputnik V, 3.968.296 recibieron la primera dosis y 1.436.407 las 2 dosis. Esto significa que hay 2.531.889 adultos mayores de 60 años que esperan aún contar con el esquema de vacunación completo. Este número representa el 83,2% del total de segundas dosis pendientes, considerando las demás vacunas".

La ficha técnica original de la vacuna decía que el intervalo entre dosis de la Sputnik V debía ser 21 días, lo que fue modificado a 12 semanas por el Cofesa meses atrás previendo la falta de segundas dosis. Esto nuevamente fue cambiado en la última reunión del Cofesa, pasando a ocho semanas la ventana entre primer y segundo componente aplicado ante la amenaza de la variante Delta.