---
category: Estado Real
date: 2020-12-24T10:44:29Z
thumbnail: https://assets.3dnoticias.com.ar/2412coro-polifonico.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: El Coro Polifónico Provincial de Santa Fe estrenó una obra para estas fiestas
title: El Coro Polifónico Provincial de Santa Fe estrenó una obra para estas fiestas
entradilla: El organismo, que depende del Ministerio de Cultura, mandó a componer
  la pieza al prestigioso músico argentino Valentín Pelisch. El material audiovisual
  puede verse en las redes de la citada cartera.

---
Para dar marco a su iniciativa, el Coro Polifónico Provincial de Santa Fe (Cppsf) expresó a través de un texto de salutación de fin de año los motivos profundos y el contexto que se vivió a lo largo de 2020.

Además, brindó una reseña tanto de la obra que estrena como de su autor, quien también realizó la edición de sonido y video.

<br/>

<span style="font-family: helvetica; font-size: 1.3em; font-weight: bold;">**Un estreno luego de un año difícil**</span>

«La pandemia –con el consiguiente aislamiento– obligó a repensar y recrear las prácticas artísticas grupales y a tomar decisiones en cuanto al modo de orientar la producción. Los teléfonos celulares y otros dispositivos fueron los medios para generar materiales en aislamiento que pasando por un proceso de edición fueron finalmente compartidos con el público en redes sociales», expresó el organismo en el texto que acompaña al estreno.

«El Cppsf tomó como premisa el hecho de que un material audiovisual no es el mero reemplazo de un concierto sino un objeto de arte con un lenguaje propio en el que los valores tanto de la música como de la imagen debían ser atendidos especialmente, y así, en cada oportunidad, la producción del material visual fue encomendada a distintos artistas».

«En este caso, coronando la tarea del año, se pensó en realizar una obra con características especiales, que diera cuenta del particular momento que el organismo está pasando, en el que no se puede sonar si no se “suena juntos”, y por otro lado evoque el tema de la Navidad y los rituales familiares de preparación para una reunión».

La obra, basada en distintas grabaciones de audio de un coral navideño de Michael Praetorius (1571-1621) y en imágenes generadas por los propios integrantes del CPPSF, fue comisionada al compositor argentino Valentín Pelisch.

Pelisch es compositor y editor audiovisual. Su obra explora la materia sonora, visual y conceptual de la música de cámara. Estudió composición con Gerardo Gandini y Marcelo Delgado en Buenos Aires, donde también cursó la Licenciatura en Composición con Medios Electroacústicos de la Universidad Nacional de Quilmes.

Diferentes ensambles y performers han presentado sus obras en espacios de América, Europa y Asia. Fue compositor residente de KulturKontakt-Austria, compositor invitado del Festival Internacional MusicArte de Panamá y compositor residente de la plataforma internacional para la creación en teatro musical auspiciada por el Goethe Institut Buenos Aires y la Bienal de Múnich.

También participó como compositor residente de la 7th International Young Composers Academy de Tchaikovsky City, en Rusia. Durante 2018 y 2019 su música fue programada en festivales y teatros de Estados Unidos, Rusia, Austria, España, Lituania y Argentina.

<br/>

<span style="font-family: helvetica; font-size: 1.2em; font-weight: bold;">**Enlaces para acceder a la obra estrenada por el Coro Polifónico Provincial de Santa Fe:**</span>

**YouTube:** [https://www.youtube.com/MinisteriodeCulturadelaprovinciadeSantaFe](https://www.youtube.com/watch?v=l3okByPBMoU&ab_channel=MinisteriodeCulturadelaprovinciadeSantaFe "https://www.youtube.com/")

**Facebook:** [https://www.facebook.com/CulturadeSantaFe](https://www.facebook.com/CulturadeSantaFe/videos/3537229222981320/?app=fbl "https://www.facebook.com/CulturadeSantaFe/")