---
category: Agenda Ciudadana
date: 2020-11-30T11:17:35Z
thumbnail: https://assets.3dnoticias.com.ar/NAVIDAD.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Telam'
resumen: 'Fiestas: prevén aumento de hasta 70% y caída de ventas'
title: 'Fiestas: prevén aumento de hasta 70% y caída de ventas'
entradilla: El Gobierno nacional negocia con asociaciones de supermercadistas y cámaras
  empresarias la creación de una canasta navideña a precios accesibles.

---
Los productos tradicionales de la mesa navideña registran una variación en los precios que va de 30% a 70% interanual mientras que la perspectiva de ventas es menor a otros años por las potenciales restricciones que podrían existir para la realización de reuniones numerosas en medio de la pandemia de coronavirus, según distintas fuentes consultadas por Télam.

En este marco, el Gobierno nacional negocia con asociaciones de supermercadistas y cámaras empresarias la creación de una canasta navideña a precios accesibles, que será anunciada en los próximos días.

## Precios estimados


De acuerdo con un estudio de Focus Market en 670 puntos de todo el país, el precio ponderado de garrapiñadas se ubica este año en $40,4 con una suba de 49% interanual; budines $63,7 (33,3%); maní con chocolate $69,7 (61%); pionono $88,6 (36%); vinos frizzantes $95,6 (26%); pan dulce $131,7 (32%); sidras $140,4 (56%); y champagne $324,2 (37%).

De este modo, una canasta de estos ocho productos mencionados tendrá este año un precio medio de $954,3 frente a $682,1 el año pasado. "Hay una estacionalidad de precios de estos productos que tienden a elevarse en esta época del año", señaló el director de Focus Market, Damián Di Pace.

Observó que "tenemos aumentos muy fuertes, pero hay que ver cómo está actuando la demanda; el precio de equilibrio lo vamos a ver en las próximas semanas, si se convalidan ventas con este aumento de precio".

Si las ventas no acompañan los puntos de venta "tendrán que hacer liquidación de stock" y aparecerán con anticipación las promociones de descuentos por dos o más unidades, señaló. 

Proyectó que en los primeros días de diciembre "se destinará el sueldo a stockearse y adelantar compra navideña" dado que "el consumo masivo viene cayendo salvo el mayorista en el que la gente está stockeando la alacena por actitud preventiva". En tanto, el presidente del Instituto de Estudio de 

Consumo Masivo (Indecom), Miguel Calvete, dijo que realizaron un relevamiento "sobre un portfolio de productos básicos panificados, pan dulce, confituras, sidras y variedad de fizz y lo que encontramos son variaciones de precios que van entre 52 y 71% interanual".

Indecom relevó una canasta de cinco productos de calidad media que incluyen ananá fizz a $230, pan dulce con frutas de 400 gramos $280, turrón de maní de 80 gramos $107, budín de vainilla 180 gramos $189 y garrapiñada de maní de 80 gramos $70, con un valor total de $876 frente a $507 en 2019, con una suba de 72%. 

Calvete indicó que "hubo caída muy fuerte en la expectativa de venta y los productores están produciendo menos; la industria tiene una capacidad ociosa de 50% porque para esta Navidad hay una previsión de caída en torno a 50% de ventas".