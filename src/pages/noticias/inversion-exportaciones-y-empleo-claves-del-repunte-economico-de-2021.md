---
category: Agenda Ciudadana
date: 2021-11-08T06:00:19-03:00
thumbnail: https://assets.3dnoticias.com.ar/agroexortacion.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: 'Inversión, exportaciones y empleo: claves del repunte económico de 2021'
title: 'Inversión, exportaciones y empleo: claves del repunte económico de 2021'
entradilla: Entre otros ítems, en agosto el trabajo registrado creció 2,8% en un año;
  los salarios aumentaron por encima de la inflación en los primeros ocho meses del
  2021.

---
Récord de exportaciones, aumento del nivel de inversión privada y suba del empleo registrado son algunos de los factores que reforzaron la recuperación de la actividad económica sobre el cierre de 2021 que, en muchos casos, reflejan índices mejores que en 2019, según datos de consultoras privadas y organismos oficiales.  
  
En ese sentido, en agosto el trabajo registrado creció 2,8% respecto de igual mes del año anterior, mientras que el nivel de inversión creció 20,4% anual en septiembre y las exportaciones superaron los US$ 58.000 millones en los primeros nueve meses del año, el valor más alto desde 2013.  
  
Se trata de factores que explican el fuerte repunte de la actividad después de la grave crisis mundial que significó el Covid-19 en 2020 y que, de acuerdo con proyecciones de funcionarios del Gobierno Nacional, llevará al PBI a un incremento "del 9% o más" para 2021.  
  
En cuanto al empleo, la cantidad de personas con trabajo registrado alcanzó los 12,158 millones en agosto, un aumento del 0,5% en términos desestacionalizados en relación con el mes anterior (54.600 personas más) y del 2,8% respecto de un año atrás (+326.900 trabajadores).  
  
A nivel interanual, la expansión fue de 2,2% (+211.600 trabajadores) en personas asalariadas y de 4,9% (+115.300 trabajadores) en el caso de trabajadores independientes que se explicaron, principalmente, por el crecimiento de 5,4% de personas registradas en el monotributo (+87.100 personas) y de 42,4% de altas en monotributo social (+11.600 personas).  
  
De hecho, en relación a febrero de 2020 -últimos datos previos a la pandemia- la mitad de las provincias recuperó los niveles previos de empleo y las de mayor dinamismo fueron Tierra del Fuego (6,3%), Misiones (3,3%), Formosa (2,5%) y San Luis (2,2%).  
  
Mientras tanto los salarios empiezan a recuperar poder adquisitivo luego de tres años consecutivos de caída luego de que la remuneración nominal bruta promedio llegara a los $96.213 (+55,4% interanual) y la mediana de la remuneración bruta fuera de $75.231 (+55,6% interanual)  
  
Según datos del Indec, los salarios crecieron por encima de la inflación en los primeros ocho meses del año: el Índice de Salarios total creció en agosto pasado el 3,2% mensual, por encima de la inflación registrada en el mes (2,5% mensual).  
  
"Ahora alentamos la inversión productiva, y no la especulación financiera", reafirmó el presidente Alberto Fernández durante la visita a diversos emprendimientos industriales del conurbano bonaerense en los últimos días.  
  
De hecho, la inversión bruta interna en septiembre se ubicó un 20,4% por encima del mismo mes de 2020, un 16,3% arriba de 2019 y un 8,4% superior a las inversiones de 2018, mientras que acumula un crecimiento de 26,2% respecto a 2020 en los primeros nueve meses del año, según datos del Centro de Estudios Económicos de Orlando Ferreres.  
  
Para Ferreres, este incremento revela que, durante 2021, "la inversión está mostrando una marcada recuperación, tanto al comparar con 2020 como con los dos años anteriores, marcados por la recesión económica".  
  
De hecho, esta semana el Presidente acompañó durante la Cumbre de Cambio Climático COP26 en Glasgow el anuncio de la inversión más grande del siglo en la Argentina: US$ 8.400 millones para la construcción de una planta en la localidad rionegrina de Sierra Grande para producir hidrógeno verde -que generará más de 50.000 puestos de trabajo, entre directos e indirectos- de la empresa australiana Fortescue.  
  
No serán, sin embargo, las únicas inversiones en energía: YPF invertirá US$ 3.500 millones en 2022, un 30% más que las de este año (US$ 2.700 millones) para la exploración y explotación de nuevos proyectos, a la vez que anunció el compromiso con Shell de invertir también US$ 300 millones en Vaca Muerta para una planta de procesamiento de 15.000 barriles diarios de petróleo y 2 millones de metros cúbicos de gas diarios y avanzarán en la perforación de 16 nuevos pozos en el área.  
  
En cuanto a las exportaciones, en los primeros nueve meses del año ya superaron los US$ 58.000 millones, el valor más alto desde 2013, mientras que el intercambio comercial (exportaciones más importaciones) aumentó 51,8% en septiembre relación con igual mes del año anterior, y alcanzó un monto de US$ 13.439 millones.  
  
La balanza comercial registró un superávit de 1.667 millones de dólares. Las exportaciones aumentaron 59,8% respecto a igual mes de 2020 (2.826 millones de dólares), debido a un incremento de los precios de 29,7% y de las cantidades de 23,3%.  
  
El comercio bilateral con Brasil -que volvió a ser el principal socio comercial de la Argentina- alcanzó los US$ 19.229 millones en los primeros 10 meses del año luego de que las exportaciones fueran de US$ 1.218 millones (+50,5% interanual) y el mayor monto mensual desde septiembre de 2014.  
  
También en octubre el complejo agroexportador vendió un récord al mundo de US$ 2.400 millones, mientras que el acumulado de los primeros 10 meses del año superó los US$ 28.0000 millones, récord absoluto para el período desde comienzos de este siglo, según datos de las cámaras exportadoras CIARA y CEC.  
  
Por último, las exportaciones de carne crecieron 24,7% interanual en septiembre y alcanzaron un valor de US$ 310,4 millones, cifra que representó un incremento de 24,7% respecto de los US$ 249 millones de septiembre de 2020.