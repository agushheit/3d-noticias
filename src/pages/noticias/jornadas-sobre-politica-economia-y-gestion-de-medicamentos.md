---
category: La Ciudad
date: 2021-11-04T06:00:35-03:00
thumbnail: https://assets.3dnoticias.com.ar/fefra.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Jornadas sobre política, economía y gestión de medicamentos
title: Jornadas sobre política, economía y gestión de medicamentos
entradilla: Este jueves 4 de noviembre a las 9, en el centro de convenciones Los Maderos
  del puerto de la ciudad de Santa Fe, comenzarán las 18° Jornadas Federales, 17°
  Internacionales y 7° Jornadas Farmacéuticas.

---
Vuelven las Jornadas de Política, Economía y Gestión de Medicamentos organizadas por la Federación Farmacéutica Argentina (FEFARA).

 En esta oportunidad, se trata de una jornada de dos días, bajo el lema “Política, Economía y Gestión de Medicamentos”. “Todos los años tratamos de analizar el mercado del medicamento en sí y cómo evoluciona, el mejor acceso para la población y estas jornadas son las 18°, y están atravesadas por la pandemia en todos los temas”, explicó a El Litoral Damián Sudano, presidente de la federación.

 “Además de jornadas de profesionales farmacéuticos, donde hay presentación de trabajos y un poco discutir el sistema de salud y cómo fue todo este año el tema de la pandemia y cómo se desempeñó cada actor del sistema de salud, obviamente está enfocado en la producción farmacéutica, tanto en el ámbito comunitario, que estuvimos en la primera línea y siempre trabajando para hacer llegar a la población el medicamento a la población de forma oportuna y segura, y más el ámbito hospitalario, donde el farmacéutico integró el equipo de salud interdisciplinario que trabajó mucho con toda la gente que hubo internada en este tiempo”, añadió Sudano.

 “Las jornadas son el el salón Los Maderos del puerto, y desde el Estado, a través del Ministerio de Salud va a haber tres ministros hablando sobre cómo vivió cada una de sus provincias la pandemia y lo que tuvo que hacer para poder enfrentarla. También va a estar el PAMI, va a haber una discusión sobre la sustentabilidad del sistema de salud, si se puede rediseñar, hay una mensa interesante de vacunas y una última mesa de farmacéuticos, tanto hospitalarios como comunitarios, que van a dar su visión sobre cómo se vivió la pandemia”, indicó el farmacéutico.