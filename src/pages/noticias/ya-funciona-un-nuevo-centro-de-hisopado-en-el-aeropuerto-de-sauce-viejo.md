---
category: Agenda Ciudadana
date: 2021-05-06T08:48:53-03:00
thumbnail: https://assets.3dnoticias.com.ar/covid.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Ya funciona un nuevo centro de hisopado en el Aeropuerto de Sauce Viejo
title: Ya funciona un nuevo centro de hisopado en el Aeropuerto de Sauce Viejo
entradilla: 'Con el objetivo de descomprimir los puestos de Santa Fe y evitando que
  los vecinos de la zona tengan que trasladarse. Estará operativo martes y viernes
  por la tarde y los turnos los otorga el 0-800 coronavirus.

'

---
Se trata de un acontecimiento de vital importancia para los vecinos de Sauce Viejo, ya que a partir de ayer, podrán realizarse hisopados en la sede del Aeropuerto. A partir de esta inauguración, los habitantes de Sauce Viejo no tendrán necesidad de recurrir a Santo Tomé o Santa Fe para hacerse los testeos.

El centro de testeos se encuentra ubicado en la amplia playa de estacionamiento que tiene la aeroestación local. Los hisopados se realizarán sin la necesidad de bajarse de los vehículos. Los operativos están previstos para los días martes y viernes a partir de las 14 30 horas y hasta las 17, debiendo contar con turnos previamente asignados.

Los resultados se les darán a quienes sean hisopados en tan sólo 15 minutos y para pedir los turnos deberán comunicarse con el 0-800 coronavirus. 

Por su parte, Leandro Aranda, el director de salud de la comuna de Sauce Viejo manifestó su preocupación por el incremento de casos que se han registrado en la localidad en las últimas semanas, con lo cual se ha tomado la decisión de reforzar los controles en las rutas, principalmente con los tests de anosmia. Pidió responsabilidad social e hizo un llamado a los vecinos a denunciar cualquier reunión o fiesta clandestina.