---
category: Agenda Ciudadana
date: 2021-03-12T06:56:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/viaje.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Agencias
resumen: Desde este sábado habrá más restricciones para los vuelos al exterior
title: Desde este sábado habrá más restricciones para los vuelos al exterior
entradilla: El Gobierno implementará una serie de medidas para desalentar a los argentinos
  a viajar a países con alta tasa de circulación y donde circulen nuevas cepas.

---
Para intentar prevenir una segunda ola de contagios en la Argentina y que no se expanda aquí la nueva cepa de Londres ni la de Manaos, en Brasil, el Gobierno decidió una serie de medidas que incluyen desalentar el turismo al exterior así como disminuir los vuelos internacionales, en especial los regionales.

Además, se desalentará a los argentinos a viajar a países con alta tasa de contagios y mortalidad, como son al día de hoy Brasil, Chile y Paraguay. Pero también se pedirá que no se viaje, a menos que sea por fuerza mayor, a naciones donde circula la cepa de Londres.

Respecto al tránsito aéreo, se restringirá aún más los vuelos. Desde febrero, los arribos y las partidas se redujeron un 50% con Brasil y se recortaron en un 30% con Estados Unidos y Europa.

Fuentes oficiales también adelantaron que se profundizarán los controles sobre las personas que llegan desde el exterior: PCR como hasta ahora y seguimiento del aislamiento, que se ampliará a 14 días en lugar de los 7 actuales.

Las provincias además reforzarán los controles para aquellos argentinos que regresen del exterior. El Gobierno nacional y los provinciales trabajarán en conjunto con información migratoria de los países para controlar que los argentinos que vuelvan además de tener un PCR negativo realicen una cuarentena obligatoria en sus hogares.

Se mantendrá la prohibición del ingreso de extranjeros no residentes al país y también seguirán suspendidos los vuelos a Reino Unido e Irlanda.