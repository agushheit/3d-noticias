---
category: El Campo
date: 2021-12-06T06:15:57-03:00
thumbnail: https://assets.3dnoticias.com.ar/ESPOCARNE.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: 'Semana clave para el campo: ¿se logrará flexibilizar la exportación de carnes
  para el 2022?'
title: 'Semana clave para el campo: ¿se logrará flexibilizar la exportación de carnes
  para el 2022?'
entradilla: "El martes es la reunión de los técnicos de la Mesa de Enlace y del ministerio
  de Agricultura y el jueves, la reunión política entre Domínguez y los presidentes
  de las agrupaciones del campo. Se esperan definiciones. \n"

---
Definir el modelo exportador para la carne del 2022 es el objetivo de las reuniones que se retoman esta semana. Por un lado, el Gobierno, por el otro la Mesa de Enlace. Ambos mantienen intereses distintos: El Gobierno tiene está preocupado en primer término mantener una canasta de precios congelados en especial considerando las Fiestas de diciembre (tema a cargo del secretario de Comercio Interior, Roberto Feletti) y por garantizar el abastecimiento interno. Por el otro, el deseo de las entidades el campo de lograr una mayor apertura en las exportaciones del programa vidente hasta el 31 de diciembre próximo.

Con estas posiciones se reunirán el martes los cuadros técnicos y el jueves el ala política.

Si bien el pedido del campo es abrir totalmente las exportaciones, las expectativas de alcanzarlo son casi nulas. En especial porque según la visión del Gobierno ese objetivo está cruzado por el propio de abastecer el mercado interno para que los argentinos puedan comprar carnes a precios accesibles.

La propuesta oficial contempla “garantizar las exportaciones de cuota Hilton, 481, EEUU, Israel, Colombia y Chile y también la de 1 millón de vacas y toros categoría D y E”, según establece un documento de trabajo que la cartera de Agricultura acercó a las entidades.

Las entidades no la aceptarían e insistirán con la apertura total del mercado. "No es fácil pero trataremos de que confíen en lo que les llevamos", dijo a NA el presidente de una de las entidades que componen la Mela de Enlace. ¿Qué le llevan?, "números más reales en lo que respecta a stock de ganado y de volúmenes certeros de exportación de carne no de res con hueso", dijo.

El Gobierno sabe que en la carne tiene a uno de sus grandes "aliados" para contener la evolución de los precios, el IPC. En este indicador la carne tiene una ponderación alta que alcanza al 8% de la canasta y por ello los esfuerzos en contener los valores. De hecho al comienzo de las restricciones se consiguió frenar la escalada de los precios de la carne, meta que a poco se fue corriendo y hoy el kilo promedio del asado en la Argentina superó los 1.000 pesos.

"SI liberas todo, la tensión de precios sigue; si se cierra todo se va a tener otro problema. Lo que se está buscando es una fórmula de equilibrio, en buena medida lo que se logró entre julio y noviembre, que los precios se estabilicen. No hay duda que en 2021 los niveles de exportación van a ser más bajos que en 2020 pero estarán por encima de los años anteriores, 2019. 2018, 2017", señaló en estricto off the record un funcionario nacional.

El funcionario reconoció que "el problema principal de la Argentina es que no sube la producción. Argentina tiene el mismo nivel de producción de carne desde hace 4 décadas y la población crece un punto por año", por ello el objetivo del plan ganadero que busca incentivar el mercado.

En línea con la proyección oficial un estudio de la consultora AZ Group señala que el 2021 terminará con unas 800.000 toneladas de carne embarcadas lejos del millón que se preveía a comienzo de año. Con este resultado Argentina se perdió

Francisco Ravetti, analista de a consultora reconoce que con este nivel es un buen año exportador pero destaca que Argentina perdió una oportunidad de combinar una exportación record con precios de la carne vacuna en alza, En 2021, los valores FOB de octubre fueron 43% mayores respecto de octubre 2020, mientras que el promedio de los 10 meses transcurridos de 2021 es 6% superior al 2020. “El daño vino por lo que podría haber sido y no fue”, apuntó Ravetti.

En el trabajo Ravetti advierte que la medida no sirvió para reducir el precio del ganado en pie, por lo menos de manera permanente, y tampoco tuvo un efecto duradero sobre los valores al consumidor, que si bien durante julio y octubre acumuló una baja del 3%, ya quedó totalmente desarticulada ante la última suba de la hacienda.