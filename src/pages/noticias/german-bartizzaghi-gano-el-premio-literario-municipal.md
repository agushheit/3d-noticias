---
category: La Ciudad
date: 2022-11-16T07:46:59-03:00
thumbnail: https://assets.3dnoticias.com.ar/book-gaed938057_1920.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: Germán Bartizzaghi ganó el Premio Literario Municipal
title: Germán Bartizzaghi ganó el Premio Literario Municipal
entradilla: Es por su obra “Unos metros de soga”, del género narrativa-cuento. Además,
  los integrantes del Jurado decidieron otorgar menciones a los trabajos que presentaron
  Rafael Arce y Gustavo Paolucci.

---
La edición 2022 del Premio Literario que organiza la Municipalidad de Santa Fe ya tiene su obra ganadora. Se trata de “Unos metros de soga”, el libro de cuentos que Germán Bartizzaghi presentó con el seudónimo “Félix”. La decisión del jurado fue por unanimidad, después de evaluar 27 obras del género narrativa-cuento presentadas por escritoras y escritores de Santa Fe, Santo Tomé, Sauce Viejo, San José del Rincón y Empalme San Carlos.

El escritor y editor Sergio Delgado presidió el jurado y lo integró en representación del Departamento Ejecutivo Municipal; junto a Miguel Ángel Molfino, por las sociedades de escritores con jurisdicción en el ámbito municipal; y Ariel Aguirre, que fue ganador de la edición 2016 del Premio Literario con su libro de relatos “Weekend”. En su evaluación, destacaron la calidad de los trabajos presentados: “Se trata de libros de cuentos muy distintos en su forma y estilo, pero que coinciden en su concepción del hacer literario, en la coherencia de sus apuestas estéticas, en la intensidad de sus búsquedas, y sobre todo, en la calidad de su escritura”.

La obra elegida recibirá un premio dinerario y será editada por la Municipalidad, con un tiraje mínimo de 300 ejemplares.

Además de éste primer premio, se otorgaron las siguientes menciones honoríficas: la primera fue para la obra “Los tlönianos”, de Rafael Arce, presentada con el seudónimo de Artis Blair; y la segunda para “Cosas que se dicen en la Costa (Cuentos nacidos en Colastiné)”, de Gustavo Paolucci, presentada con el seudónimo de Alejandra Bezos.

**Sobre Germán Bartizzaghi**

Nació en Pilar, Santa Fe, en 1987. En 2006 llegó a la ciudad de Santa Fe para cursar estudios en Ciencias Económicas y en Comunicación.

Ha obtenido, entre otros, los premios JUNINPAIS 2011; el galardón absoluto del XXVI Certamen para autores jóvenes Noble Villa de Portugalete, España; y el de la Sociedad de Escritores de General Alvarado por su novela breve “Tiempo muerto”, que también fue distinguida con Faja de Honor por la Asociación Santafesina de Escritores.

Producto de esos certámenes ganados, se han editado sus libros: “Historias recuperadas” (Cuentos, Ed. De las tres lagunas, 2012), “Tiempo muerto” (Nouvelle, Ed. Martín, 2016) y “Simón dice: ¡cuéntales!” (Cuentos, Ed. Letras Cascabeleras, España, 2017).

En 2021 fue seleccionado como cuentista para el Festival Internacional de Narrativa Ciudad de Guatemala por su texto “Otra metamorfosis”.

**En perspectiva**

El concurso 2022 se extendió entre agosto y septiembre. En esta oportunidad convocó a presentar obras inéditas de narrativa-cuento, ya que en cada edición se alterna la convocatoria con los géneros ensayo y poesía; y con la dramaturgia desde el año pasado, a partir de una nueva ordenanza sancionada por el Concejo Municipal (N° 12.773/21) que permitió ampliar los alcances del concurso en ese y otros aspectos.

Ese proceso para fortalecer al premio como un estímulo para la producción literaria se inició en 2020, durante el primer año de la pandemia, con cambios que permitieron darle continuidad al certamen que no se había realizado en 2019, y que apuntaron a consolidarlo con un primer premio dinerario, además de la acostumbrada edición de la obra ganadora. Esa edición convocó obras del género ensayo y el primer premio fue para Silvia Susana Beltrán por “Síndrome de Asperger: los más solos de la tierra”, cuya publicación se presentó durante la XXVII Feria del Libro de Santa Fe. Además, se entregaron dos menciones a los trabajos de Miguel Gavilán por “El país de Mateo Booz”, y a Susana Beatriz Andereggen por “Hacer la América. Un anhelo utópico entre objetos y sujetos”.

En 2021 se incorporó por primera vez la dramaturgia como género del concurso y la escritora premiada fue Marianela Alegre por “…y en medio el río”. Las menciones fueron para las obras “Apego”, de Cecilia Arellano; “Nunca es basta”, de Lautaro Ruatta; y “Los esenciales”, de Julieta Vigo. Los cuatro trabajos se incluyeron en la edición que realizó la Municipalidad que se presentará próximamente, en la Sala Mercado Editorial -ubicada en el Mercado Progreso-, junto con una programación musical.