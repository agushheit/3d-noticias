---
category: La Ciudad
date: 2021-08-14T06:00:52-03:00
thumbnail: https://assets.3dnoticias.com.ar/JatonBAIRES.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: Jatón fue a Buenos Aires a gestionar obras y mejoras en los servicios para
  la ciudad
title: Jatón fue a Buenos Aires a gestionar obras y mejoras en los servicios para
  la ciudad
entradilla: 'El intendente de Santa Fe mantuvo una serie de reuniones con los ministros
  nacionales de Obras Públicas, Transporte y Ambiente. '

---
El intendente Emilio Jatón tuvo un par de días con una cargada agenda de trabajo y reuniones con ministros nacionales en Buenos Aires, con el objetivo de gestionar obras y mejoras en los servicios para la capital provincial. En ese sentido, entre la tarde de ayer y esta mañana, el mandatario local mantuvo encuentros con los ministros nacionales Gabriel Katopodis (de Obras Públicas), Alexis Guerrera (de Transporte) y Juan Cabandié (de Ambiente y Desarrollo Sustentable).

“Vinimos con las carpetas a mostrar lo que tenemos proyectado para la ciudad. Este es el camino, hay que iniciar las gestiones en Buenos Aires para que los santafesinos nos veamos beneficiados”, dijo Jatón, quien aseguró que “ya lo hicimos así en otras dos oportunidades y vinimos con resultados”.

Para el intendente, el balance de los encuentros es “sumamente positivo”. Y añadió: “Las obras empiezan con la entrega de la carpeta, una charla y con un ‘medio sí’ que nos traemos de los funcionarios cuando les interesan los proyectos. Eso es lo que tenemos que hacer, gestionar para Santa Fe de forma directa”.

**Temas de transporte**

Con respecto al encuentro con el ministro de Transporte, el mandatario santafesino indicó: “Estamos con proyectos para mejorar nuestra Terminal de Ómnibus, duplicar los kilómetros de ciclovía que hay en la ciudad, y mejorar el sistema de bicicletas públicas. Ése es un proyecto que fue muy bien recibido en Nación porque se trata de nuevas maneras de movilidad en la ciudad”, adujo.

En relación a la Terminal, la Municipalidad pretende avanzar en la puesta en valor del edificio y su entorno inmediato.

Con respecto a una posible interconexión con la vecina ciudad de Santo Tomé, Jatón remarcó: “Hablamos con Alexis Guerrera sobre la posibilidad de los trenes de cercanía; hace tiempo que hemos puesto en agenda este tema y, con ese objetivo, estamos arreglando la Estación Mitre. Este proyecto incluye los 5 km que une a Santa Fe con Santo Tomé. Volvimos a hablar con el ministro de este proyecto y nos pasaron los contactos de Trenes Argentinos, así que estamos trabajando en esta opción posible”, amplió.

**Planta de reciclaje**

Por otra parte, al ministro Cabandié, el intendente le presentó la propuesta de ampliación de la planta de reciclaje donde trabaja la Asociación Dignidad y Vida Sana, que nuclea a unas 65 familias que viven de recuperar materiales que llegan al relleno sanitario. “Fuimos con un pedido concreto de inversiones. Hay que reparar y ampliar las instalaciones sanitarias, el comedor, el salón de usos múltiples, colocar cercos, mejorar las cintas transportadoras de residuos. En definitiva, presentamos un proyecto para mejorar toda la planta. Estamos siguiendo de cerca todo lo que ocurre con el relleno sanitario”, amplió Jatón.

**Infraestructura y terrenos**

Con el ministro de Obras Públicas, el intendente dialogó sobre la posibilidad de inversión para seguir dotando de infraestructura a los barrios más postergados de la ciudad. “Estamos conversando sobre los proyectos para nuevas plazas, para nuevas intervenciones en servicios y obras en los barrios donde el Estado nunca ha llegado”, dijo.

Jatón recordó que en Santa Fe se están haciendo obras importantes con fondos nacionales, como, por ejemplo, en Camino Viejo, en los barrios Cabal-Las Lomas, en las plazas de Scarafía y Los Troncos. “Hicimos una presentación sobre cómo marchan esos trabajos y fuimos por más obras para Santa Fe”, destacó.

La serie de encuentros prosiguió además con Martín Cosentino, presidente de la Agencia de Administración de Bienes del Estado (AABE). La idea es que los terrenos ferroviarios ubicados en la ciudad y que pertenecen a la agencia, pasen a manos del municipio y se puedan concretar acciones. Finalmente, el intendente tenía previstas reuniones para conseguir fondos para el Teatro Municipal y los museos.