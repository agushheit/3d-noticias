---
category: La Ciudad
date: 2021-08-05T06:00:13-03:00
thumbnail: https://assets.3dnoticias.com.ar/LOSHORNOS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Jatón: “Los Hornos ya no va a estar olvidado”'
title: 'Jatón: “Los Hornos ya no va a estar olvidado”'
entradilla: " Los trabajos tienen un monto total de 97 millones de pesos e incluye
  la pavimentación y desagües en calle Pedro de Vega, y la incorporación de 1.200
  artefactos led en las arterias de mayor circulación del barrio."

---
Esta mañana, el intendente Emilio Jatón y el gobernador Omar Perotti rubricaron convenios, en el marco del Plan Incluir, que brindarán obras de pavimentación y desagües en calle Pedro de Vega, entre avenida Peñaloza y Gobernador Freyre; y la incorporación de aproximadamente 1.200 artefactos led en las arterias de mayor circulación de distintos barrios de la ciudad.

En diálogo con la prensa, el intendente repasó la historia del barrio: “Los Hornos tiene una realidad muy particular, por un lado, un sector planificado, pensado y, por otro lado, otro sector que está absolutamente olvidado. Hoy estamos en una de esas calles olvidadas”, indicó el intendente y repasó que el proyecto para concretar las obras que están a punto de realizarse “surge hace muchos años atrás, cuando éramos concejales con Laura Mondino, ahí empezamos a discutir el tema y finalmente hoy pudimos firmar un convenio con la provincia, después de un trabajo técnico importante”.

“Los Hornos ya no va a estar olvidado como antes”, aseguró el mandatario, y adelantó que “estamos trabajando con Nación y provincia en más proyectos para mejorar el barrio. No se puede trabajar en forma descoordinada: la Municipalidad hace los proyectos técnicos, hablamos con los funcionarios provinciales y de allí salen los financiamientos, hay un proceso de trabajo que lleva meses, es un trabajo conjunto y de esa manera estamos concretando muchas de las obras de la ciudad. En este espacio, en esta calle, confluyen los planes Incluir e Integrar”.

Por otra parte, durante el acto, el intendente hizo un anuncio que despertó el aplauso espontáneo de las vecinas y vecinos: “Antes de que me olvide les voy a dar una buena noticia, en Lavaisse y Gobernador Freyre hay un bache histórico, ustedes lo saben mejor que nadie. Bueno, entró en el plan de bacheo y en septiembre los vamos a arreglar”.

Por su parte, Perotti destacó el trabajo que se lleva delante de manera conjunta entre la provincia y la Municipalidad: “Estamos trabajando en muchos de los barrios que habían sido dejados de lado durante años y años. Tenemos que priorizar los trabajos, por eso también vamos a invertir en mejorar el Centro de Salud del barrio”.

**Pavimento y desagües**

El primero de los convenios rubricados incluye trabajos de pavimentación y desagües en calle Pedro de Vega, entre avenida Peñaloza y Gobernador Freyre. El monto oficial de la licitación -que se concretará el 11 de agosto- es de $ 52.670.787,35. Las tareas tienen como finalidad mejorar la movilidad y accesibilidad urbana en el entorno del Centro de Salud de Barrio Los Hornos. Se construirán aproximadamente 480 metros de pavimento de hormigón en una calzada de 8 metros de ancho (con las respectivas obras de desagüe), rampas peatonales, señalización e iluminación led.

**Más de 1100 luces led**

El otro convenio, que se enmarca en el marco del Plan de Iluminación municipal, ya se licitó y consiste en la colocación de 1.135 nuevas luces led en las arterias de penetración y de mayor circulación a los distintos barrios de la ciudad. El presupuesto oficial es de $ 44.682.793,50 y se presentaron tres empresas oferentes. El fin es contribuir a una homogeneización del sistema de iluminación, por eso se proyecta abordar las avenidas de penetración a los barrios, con mejoras que permitan el desplazamiento de cada vecino y vecina, priorizando las vías de circulación del transporte público, las áreas con inmuebles públicos en salud, seguridad y educación, y finalmente las arterias de mayor circulación.

**La voz de los vecinos**

Pablo Maldonado es el presidente de la vecinal del barrio Los Hornos, al finalizar el acto destacó que “para nosotros que hace muchos años vivimos el abandono y la desidia de gestiones municipales anteriores, es sumamente importante que se concreten estas obras. La pavimentación de estas arterias no solo garantiza la circulación de Este a Oeste, sino que se resolverá un problema gravísimo que es el anegamiento permanente frente al centro de salud Padre Cobos”.

Asimismo, Maldonado aseguró que “esta es la parte más olvidada del barrio, los vecinos estábamos descreídos, por eso nos parece muy importante que el intendente nos haya dado fechas concretas: que en 10 días se licita, que en 40 días se inician las obras. Las mejoras siempre son construcciones colectivas, que los gobiernos de la provincia y el municipio junto a las instituciones y los vecinos podamos planificar y ver la integralidad del barrio es fundamental; esperemos que este sea el inicio porque tenemos muchas deudas pendientes. Así que le agradecemos al gobernador y al intendente por estos trabajos”.