---
category: Cultura
date: 2021-08-08T06:00:28-03:00
thumbnail: https://assets.3dnoticias.com.ar/PREMIOLITORAL.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Premio Salón Litoral 2021: convocatoria a artistas visuales de todo el país'
title: 'Premio Salón Litoral 2021: convocatoria a artistas visuales de todo el país'
entradilla: 'La Municipalidad de Santa Fe convoca a artistas visuales de todo el país
  a participar del Premio Municipal de Artes Visuales “Salón Litoral” 2021. '

---
Las obras seleccionadas formarán parte de una muestra en el Museo Municipal de Artes Visuales, donde se hará entrega del Premio adquisición “Municipalidad de la Ciudad de Santa Fe” y otros premios estímulo. Inscripciones abiertas hasta el 12 de septiembre.

La Municipalidad de Santa Fe, a través de la Secretaría de Educación y Cultura, abrió la convocatoria, desde este viernes 6 de agosto, para participar del Premio Municipal de Artes Visuales “Salón Litoral” 2021. Las obras seleccionadas formarán parte de una muestra en el Museo Municipal de Artes Visuales “Sor Josefa Díaz y Clucellas” (MMAV) y podrán ser elegidas por el jurado para recibir el Premio adquisición “Municipalidad de la Ciudad de Santa Fe” de ciento cincuenta mil pesos, así como otros premios estímulo acompañados de una dotación dineraria.

La exposición será inaugurada el 15 de noviembre, en el aniversario de la fundación de la ciudad, retomando la tradición de los salones anuales municipales que iniciaron su actividad en la década del ’40 en el museo y se sostuvieron durante 15 ediciones. El certamen nace de las raíces de aquellos salones que marcaron punta en la historia del arte regional y, luego de tres décadas de pausa, reanuda su actividad y se extiende a todo el país, con el objetivo de estimular y difundir la producción artística contemporánea local, regional y nacional.

**El certamen**

El llamado está abierto hasta el 12 de septiembre a artistas visuales de todo el país de manera individual o colectiva para presentar obras realizadas en diferentes variantes formales y conceptuales (sean bidimensionales o tridimensionales, audiovisuales, digitales o performáticas), con fecha de realización posterior a 2018. Se seleccionarán hasta 30 obras para participar de la muestra, que recibirán una asignación estímulo de cinco mil pesos.

El jurado de selección y premiación está conformado por María Teresa Constantin, José Luis Roces y Lila Siegrist.

**Las raíces**

El Salón del Litoral se inauguró en 1942, con el antecedente del ‘I Salón Municipal de Bellas Artes’ de 1940, resultado de la por entonces recientemente creada Comisión Municipal de Bellas Artes. Resultaba, en opinión de la prensa de la época, “representativamente necesario” del esfuerzo de los artistas y se caracterizó por no tener restricciones de ninguna naturaleza en lo referente a tema y edad, albergando obras de Entre Ríos, Santa Fe, Corrientes, Misiones, Chaco, Córdoba, Santiago del Estero y Tucumán.

En la apertura de la temporada 2021 del MMAV, luego de las obras de recuperación realizadas, se presentó la muestra “Remontar un río. Salones Litoral y Modernidad (1940/1980)”, una selección de obras patrimoniales premiadas en los 15 Salones. Como jurados y como artistas expositores pasaron por la muestra Mele Bruniard, Berni, Gambartes, José Sedlacek, Nanzi Vallejo, César Fernández Navarro, Cesar López Claro, Enrique Estrada Bello y Ricardo Supisiche, por mencionar sólo algunos, así como una gran variedad de artistas jóvenes de la región.

**Sobre la inscripción**

Para inscribirse es necesario completar datos personales y datos de la obra en un formulario de inscripción disponible en la sección Convocatorias de la Secretaría de Educación y Cultura en la web de la Municipalidad, donde también se pueden consultar se podrán consultar las bases completas del certamen: [https://santafeciudad.gov.ar/secretaria-de-educacion-y-cultura/convocatorias-vigentes/.](https://santafeciudad.gov.ar/secretaria-de-educacion-y-cultura/convocatorias-vigentes/. "https://santafeciudad.gov.ar/secretaria-de-educacion-y-cultura/convocatorias-vigentes/.")

Para realizar consultas, se deberá escribir al correo electrónico: convocatorias2021sf@gmail.com

Las obras seleccionadas deberán ser entregadas hasta el día viernes 22 de octubre en el horario que se comunique oportunamente en el MMAV (San Martín 2068).

Podrán presentarse artistas visuales mayores de 18 años, de nacionalidad argentina o extranjera con un mínimo de tres (3) años de residencia en el país; así como colectivos de artistas visuales. En ese caso, los requisitos de edad y nacionalidad o residencia deberán ser cumplidos por la mayoría de los integrantes y la postulación deberá nombrar a un representante. Se podrá postular hasta una obra por artista individual o colectivo de artistas; y quienes realizan una postulación individual podrán a su vez formar parte de otra postulación como integrante de un colectivo.