---
category: Agenda Ciudadana
date: 2021-01-17T10:05:07Z
thumbnail: https://assets.3dnoticias.com.ar/internet.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Diario Uno
resumen: Cómo tramitar la Prestación Básica Universal y Obligatoria de internet
title: Cómo tramitar la Prestación Básica Universal y Obligatoria de internet
entradilla: Desde el 1 de enero rige la resolución que instaura una tarifa básica
  para servicios de telefonía móvil y fija, Internet y la TV paga

---
El 1 de enero de este año comenzó a regir la Resolución 1467/2020, mediante la cual el gobierno nacional impulsó una Tarifa Básica Universal para los servicios de Internet, televisión por cable y telefonía móvil. Esta iniciativa declara a los servicios TIC como públicos esenciales y en competencia, con el objetivo de garantizar el derecho humano de acceso a las Tecnologías de la Información y las Comunicaciones por cualquiera de sus plataformas.

En ese sentido el Ente Nacional de Comunicaciones (ENACOM), estableció abonos básicos que las empresas prestadoras deberán brindar a los usuarios y usuarias que lo soliciten, siempre y cuando pertenezcan al universo de beneficiarios/as detallados en la página de Prestación Básica Universal y Obligatoria ([https://www.enacom.gob.ar/prestacion-basica-universal_p4792](https://www.enacom.gob.ar/prestacion-basica-universal_p4792 "https://www.enacom.gob.ar/prestacion-basica-universal_p4792"))

De esta manera se ofrece al usuario y la usuaria, una mayor variedad de servicios a precios más bajos, sin perjuicio del principio de competencia, por lo que se procuró generar eficiencia y rentabilidad económica para las empresas prestadoras.

**¿Quiénes pueden solicitar la PBU?**

* Beneficiarios y beneficiarias de la Asignación Universal por Hijo (AUH) y la Asignación por Embarazo, como así también sus hijos-as/tenencia de entre DIECISÉIS (16) y DIECIOCHO (18) años y miembros de su grupo familiar (padre/madre, cónyuge/conviviente).
* Beneficiarios y beneficiarias de Pensiones no Contributivas que perciban ingresos mensuales brutos no superiores a DOS (2) Salarios Mínimos Vitales y Móviles como así también sus hijos-as/tenencia de entre DIECISÉIS (16) y DIECIOCHO (18) años.
* Usuarios inscriptos y usuarias inscriptas en el Régimen de Monotributo Social como así también sus hijos-as/tenencia de entre DIECISÉIS (16) y DIECIOCHO (18) años.
* Jubilados y jubiladas; pensionadas y pensionados; y trabajadores y trabajadoras en relación de dependencia que perciban una remuneración bruta menor o igual a DOS (2) Salarios Mínimos Vitales y Móviles como así también sus hijos-as/tenencia de entre DIECISÉIS (16) y DIECIOCHO (18) años.
* Trabajadores y trabajadoras monotributistas inscriptos e inscriptas en una categoría cuyo ingreso anual mensualizado no supere DOS (2) Salarios Mínimos Vitales y Móviles como así también sus hijos-as/tenencia de entre DIECISÉIS (16) y DIECIOCHO (18) años.
* Usuarios y usuarias que perciban seguro de desempleo como así también sus hijos-as/tenencia de entre DIECISÉIS (16) y DIECIOCHO (18) años.
* Usuarios y usuarias incorporados e incorporadas en el Régimen Especial de Seguridad Social para Empleados de Casas Particulares (Ley N° 26.844) como así también sus hijos-as/tenencia de entre DIECISÉIS (16) y DIECIOCHO (18) años.
* Usuarios y usuarias que perciban una beca del programa Progresar.
* Personas que se encuentren desocupadas o se desempeñen en la economía informal, como así también sus hijos-as/tenencia de entre DIECISÉIS (16) y DIECIOCHO (18) años.
* Beneficiarias y beneficiarios de programas sociales, como así también sus hijos-as/tenencia de entre DIECISÉIS (16) y DIECIOCHO (18) años.
* Clubes de Barrio y de Pueblo que se encuentren registrados conforme lo dispuesto por la Ley N° 27.098.

**¿Cómo acceder al beneficio?**

· Las personas que quieran acceder a las tarifas subsidiadas para uno o varios de esos servicios tendrán que tramitar ante la propia empresa prestadora la conexión a cada uno de los servicios.

· Deben completar una declaración jurada que se puede descargar de la página de Enacom y presentarla a la empresa prestadora. ([https://www.enacom.gob.ar/declaracion-jurada-prestacion-basica-universal_p4798](https://www.enacom.gob.ar/declaracion-jurada-prestacion-basica-universal_p4798 "https://www.enacom.gob.ar/declaracion-jurada-prestacion-basica-universal_p4798"))

¿Si no estoy comprendido dentro de este universo, puedo también acceder de alguna forma al beneficio?

No, la opción PBU es para aquellos usuarios detallados en el punto anterior que no puedan afrontar económicamente los planes brindados por las empresas prestadoras.

¿Están obligadas las empresas a brindarme el plan solicitado?

Sí. Ante la solicitud por parte del usuario la empresa prestadora debe garantizar el servicio solicitado con el costo final aprobado por ENACOM.

¿Qué sucede si la empresa no me garantiza la PBU?

· En caso que la empresa no garantice la prestación de la PBU, los solicitantes deberán iniciar el reclamo frente a la prestadora.

· Si persiste el incumplimiento, las y los beneficiarios podrán enviar el formulario online informativo, ([https://formularioenacom.gob.ar/formulario.html](https://www.enacom.gob.ar/declaracion-jurada-prestacion-basica-universal_p4798 "https://www.enacom.gob.ar/declaracion-jurada-prestacion-basica-universal_p4798")), el cual no reviste carácter de trámite o reclamo oficial, sino de declaración informativa ante ENACOM para el seguimiento de la solicitud.