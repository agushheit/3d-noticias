---
category: Agenda Ciudadana
date: 2021-05-04T08:20:07-03:00
thumbnail: https://assets.3dnoticias.com.ar/amsafe.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de LT10
resumen: AMSAFE continúa reclamando que vacunen a los docentes de las escuelas de
  artes
title: AMSAFE continúa reclamando que vacunen a los docentes de las escuelas de artes
entradilla: Se trata de más de 140 docentes que pertenecen a instituciones que están
  bajo la órbita del ministerio de Cultura de la provincia (Matovani, CREI, Escuela
  de Música, de Teatro, de Cine, entre otras).

---
Durante la jornada de este lunes, AMSAFE La Capital le exigió al gobierno provincial que vacune a 144 docentes que prestan servicios en las escuelas de artes de la ciudad de Santa Fe , que dependen del ministerio de cultura.  

Mientras cientos de docentes están recibiendo la segunda dosis de las vacunas, estos trabajadores de la educación no fueron accedieron a la primera dosis.

“No puede ser que estos 144 docentes de la Escuela de Cine, de Teatro, de la Mantovani, del CREI, del coro y de la orquesta sinfónica no hayan recibido la primera dosis. Ya le enviamos al ministerio de Educación tres veces el listado (además que están anotados como corresponde). Le exigimos al gobierno provincial que los convoquen los vacunen de manera urgente porque estos docentes están sosteniendo la presencialidad”, remarcó Rodrigo Alonso.

También, LT10 recibió reclamos de docentes de la Escuela de Educación Secundaria Orientada Particular Incorporada N° 8.245 “Nuestra señora de Guadalupe” (que está en el puerto de Santa Fe) que tampoco recibieron la primera dosis y están sosteniendo la presencialidad.