---
category: La Ciudad
date: 2020-12-18T11:45:18Z
thumbnail: https://assets.3dnoticias.com.ar/1812peatonal.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Noche de compras
title: Noche de compras
entradilla: Será este domingo de 17 a 22 horas. El objetivo es que los comercios puedan
  abrir sus puertas en horario extraordinario para evitar la aglomeración de clientes
  en las franjas habituales.

---
**La Municipalidad confirmó la realización de La noche de compras**, una iniciativa que busca fomentar las ventas de comercios de rubros no esenciales con atención al público. Será este **domingo 20 de diciembre, en el horario de 17 a 22 horas** y alcanza a todos los locales de la capital provincial.

La intención es que los vecinos y las vecinas de la ciudad puedan **programar sus compras y organizarlas, concretándolas en un día y un horario no habitual**, bajo el estricto cumplimiento de los protocolos sanitarios y los criterios establecidos por los gobiernos nacional y provincial. De este modo, se evita la aglomeración de personas en las franjas ordinarias, mientras se brinda a los locales la posibilidad de incrementar sus ventas, en un año que fue complejo para la actividad comercial.

La decisión quedó plasmada en la Resolución Interna N° 77, con fecha del 17 de diciembre. La norma justifica la organización del evento, según lo establecido en los decretos nacional 956/20 y provincial 1527/20, que establece que en el departamento La Capital, la actividad comercial podrá realizarse en los horarios establecidos por las autoridades municipales y comunales.

El secretario de Producción y Desarrollo Económico del municipio, Matías Schmüth, indicó que «mediante el trabajo articulado que venimos haciendo con el Centro Comercial y las asociaciones de calles, nos pusimos de acuerdo para **reeditar La noche de compras para poder darle una oportunidad a los comercios que vienen de un año muy duro** y que siempre esperan aumentar sus ventas para las fiestas».

Según señaló, «la idea es fomentar que los comercios abran el domingo previo a la Navidad entre las 17 y las 22 horas para brindar una oportunidad más a quienes estén ultimando las compras para las fiestas». Asimismo, mencionó que la iniciativa permite «a través de distintas convocatorias que hacen los centros comerciales, invitar a los santafesinos a elegir el comercio local y el comercio de cercanía, aportando al crecimiento de un sector que viene golpeado por la crisis y por las caídas en el consumo».

Schmüth recordó que la realización de actividades de este tipo «son el resultado de las conversaciones mantenidas en las mesas sectoriales organizadas por la Municipalidad con los distintos sectores, en este caso, con el comercio. Son alternativas que buscamos en conjunto entre ambas partes, apoyando siempre a la articulación público-privada, que es la base del desarrollo económico de la ciudad», concluyó.