---
category: Agenda Ciudadana
date: 2020-12-15T12:11:10Z
thumbnail: https://assets.3dnoticias.com.ar/agua.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: El Litoral'
resumen: Autorizan un aumento del 32% de la tarifa de agua en Santa Fe
title: Autorizan un aumento del 32% de la tarifa de agua en Santa Fe
entradilla: La medida lleva la firma de la ministra Silvina Frana y se aplica a partir
  del bimestre enero/febrero de 2021.

---
Este martes por la mañana se confirmó que **el 2021 comenzará con un aumento en la tarifa del servicio de agua potable** en la provincia de Santa Fe.

Desde el ministerio de Infraestructura, Servicios Públicos y Hábitat, que conduce Silvina Frana, se autorizó un incremento del 32 por ciento a partir del bimestre enero / febrero de 2021.

Cabe recordar que, a fines de noviembre, desde Aguas Santafesinas SA reclamaban una suba de la tarifa de acuerdo a lo establecido en la resolución 1.048 de 2018, que planteaba un aumento del 60 por ciento en dos veces para ese año, y que solo se aplicó un 28 por ciento, por lo que quedaría un remanente de suba del 32 por ciento.

"Estamos viendo el desfasaje que se nos produjo desde aquel pedido de 2018 a lo que significan las tarifas de 2021. Se nos hace importante poder contar con un incremento de tarifa para poder seguir sosteniendo el funcionamiento de la empresa", dijo semanas atrás el titular de la empresa, Hugo Morzán.