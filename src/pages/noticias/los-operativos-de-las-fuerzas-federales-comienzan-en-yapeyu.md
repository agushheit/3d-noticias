---
category: La Ciudad
date: 2021-10-26T06:15:25-03:00
thumbnail: https://assets.3dnoticias.com.ar/GENDARMERIA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Los operativos de las fuerzas federales comienzan en Yapeyú
title: Los operativos de las fuerzas federales comienzan en Yapeyú
entradilla: Este lunes inician las tareas de prevención en conjunto entre la Policía
  de Santa Fe y las fuerzas federales que llegan a la ciudad.

---
Este lunes por la tarde comienzan los operativos especiales en el marco de las tareas de prevención que llevarán adelante en conjunto en la ciudad capital, la Policía de Santa Fe con los 102 efectivos de las fuerzas federales que arribaron, entre Gendarmería, Prefectura y Policía Federal. El puntapié de los controles se dará en el cordón noroeste de la ciudad, más precisamente en la zona de barrio Yapeyú.

 El accionar de las fuerzas federales en la capital provincial, tiene como eje "brindar seguridad ciudadana" en los barrios más calientes en materia de delitos. Los 102 efectivos que arribaron a Santa Fe, entre Gendarmería Nacional, Prefectura Nacional y Policía Federal Argentina, llegan para completar la suma de 509 agentes federales.

Los nuevos 102 efectivos que se suman desde este lunes al trabajo en conjunto en las calles de la ciudad con la Policía de Santa Fe, "no se quedarán en la ciudad a dormir, sino que al ser de la región, van a poder ir y venir desde sus puestos fijos en otras localidades", subrayaron a UNO Santa Fe fuentes de Seguridad y continuaron: "La idea es ir moviéndolos por diversos barrios de la ciudad en distintos horarios".

En diálogo este lunes por la mañana con la emisora radial LT10, Javier Enrique Sardo, jefe de Gendarmería de la Agrupación 21, contó que "en principio llegan 60 efectivos de gendarmería, junto a Policía (federal) y Prefectura para sumar un número de 102. Van a ir rotando, se van a diagramar operativos de distinta duración, en distintos lugares y eso se va a ir viendo; conforme a la evolución del mapa del delito y de lo que surja de las reuniones operativas".

"Las fuerzas federales ya vienen cumpliendo en la ciudad de Santa Fe la tarea de seguridad ciudadana y el apoyo a la Policía de la provincia de Santa Fe. En las reuniones operativas diarias iremos diagramando los lugares de trabajo como lo venimos haciendo en base a un mapa de calor que determina los lugares más conflictivos; donde vamos a trabajar todas las fuerzas para tratar de combatir esta situación que se está dando en Santa Fe", manifestó Sardo.

"El mapa de calor que tiene la Policía de Santa Fe en base al 911, también se alimenta de las denuncias en las cuales interviene el Ministerio Publico de la Acusación. También existe un mapa de calor que posee el municipio local con vecinales vecinales. La seguridad ciudadanía es función del gobierno provincial. Nosotros venimos a apoyar a la Policía que tiene esa responsabilidad", resaltó el jefe de Gendarmería de la Agrupación 21.

"La ciudad de Santa Fe tiene un problema de delitos predatorios, claramente. Nosotros vivimos en Santa Fe, sabemos lo que pasa. Vemos que hay ciertos tipos de delitos vinculados a peleas entre bandas, ajustes de cuentas en donde los vecinos muchas veces quedan en el medio y sufren estos hechos. Por eso, nuestra función también está orientada a luchar contra el narcomenudeo, contras esos kioscos de droga que en los barrios que causan mucho daño y atentan contra la seguridad ciudadana", finalizó Enrique Sardo.