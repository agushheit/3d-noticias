---
category: Estado Real
date: 2021-07-06T07:25:38-03:00
thumbnail: https://assets.3dnoticias.com.ar/justiciaa.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: La Secretaría de Justicia firmó un acuerdo con el Foro Previsional para facilitar
  el acceso a trámites del Registro Civil    Se podrán solicitar actas de nacimientos,
  matrimonios, uniones convivenciales y defunciones.
title: La Secretaría de Justicia firmó un acuerdo con el Foro Previsional para facilitar
  el acceso a trámites del Registro Civil    Se podrán solicitar actas de nacimientos,
  matrimonios, uniones convivenciales y defunciones.
entradilla: Se podrán solicitar actas de nacimientos, matrimonios, uniones convivenciales
  y defunciones.

---
La Subsecretaria de Registros sobre Personas Humanas de la provincia y el Registro Civil firmaron un convenio de colaboración con el Foro Previsional Provincial. El acuerdo tiene por objeto la facilitación de actas de nacimientos, matrimonios, uniones convivenciales y defunciones entre las instituciones de manera ágil y segura, evitando las demoras en la obtención por parte de los interesados así como la afluencia del público a las oficinas en tiempos de pandemia. 

En este sentido, desde la Secretaria de Justicia provincial recordaron que desde ese ámbito se trabaja en la idea de un Estado integral e integrado, funcionando de forma coordinada entre sus áreas para favorecer los tiempos administrativos, de modo que la gestión sea más sencilla y más rápida hacia los ciudadanos.

El convenio fue suscripto por el subsecretario de Registros sobre Personas Humanas y Jurídicas, Francisco Dallo; el director del Registro Civil, Mariano Gálvez; y el director de la Caja de Jubilaciones y Pensiones de Santa Fe, Humberto Giobergia, representando al Foro Previsional de Santa Fe.

**FORO PREVISIONAL**

El Foro Previsional está integrado por la Caja Municipal de Jubilaciones y Pensiones de Santa Fe, el Instituto Municipal de Previsión Social de Rosario, la Caja Municipal de Previsión Social de Esperanza, la Caja de Jubilaciones y Pensiones de la Municipalidad de Cañada de Gómez, la Caja Municipal de Rufino, el Instituto Municipal de Previsión Social de Venado Tuerto, la Caja de Seguridad Social para los Profesionales del Arte de Curar, la Caja de Previsión Social de los Profesionales de la Ingeniería de la 1ª y 2ª Circunscripción, la Caja de seguridad Social para los Profesionales en Ciencias Económicas de la Provincia, y la Caja de Seguridad Social de Abogados y Procuradores de la Provincia.