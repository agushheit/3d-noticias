---
category: La Ciudad
date: 2021-12-24T06:00:54-03:00
thumbnail: https://assets.3dnoticias.com.ar/POZOS.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: '"Son pozos inmensos": el reclamo de los vecinos de la zona de countries
  por el mal estado de las calles'
title: '"Son pozos inmensos": el reclamo de los vecinos de la zona de countries por
  el mal estado de las calles'
entradilla: Además, la queja también recae sobre la carretera de salida hacia la autopista
  ubicada en la zona de La Tatenguita. 

---
A simple vista desde la Autopista 01 que conecta Santa Fe con Rosario se observan los barrios privados que pertenecen a la ciudad de Santo Tomé. Sin embargo, a pesar de ser lindantes a esta carretera, las calles de ingreso y egreso de los countries “son intransitables”, según los vecinos. 

  Un habitante de Aires del Llano,  afirmó que “el reclamo se lo pasan de un lado a otro y nadie se hace cargo”.

La calle a la vera de la autopista que llega hasta la puerta de Aires del Llano pertenecía hace años a un particular, pero luego fue cedido y actualmente es dependiente de la Municipalidad de Santo Tomé. Sin embargo, “ni la Municipalidad ni la Provincia toman el reclamo”, según el testimonio del vecino.

 Además del mal estado de las calles, en la actualidad se encuentran maquinarias trabajando todo el tiempo en la construcción de los nuevos barrios privados en dicha zona, y la calzada se reduce aún más. “Los vehículos se suben al ripio o a la tierra, donde no está el camino, para esquivar máquinas y autos que vienen del otro lado”, cuenta el vecino.

A su vez, el enojo se duplica por los elevados impuestos y tasas que deben abonar los habitantes de los countries. “No son baratos como para tener un camino así”, afirman, relatando también que la zona de la rotonda y la calle que pasa por La Tatenguita, única salida hacia la autopista para llegar a Santa Fe por el momento, está muy deteriorada.

 