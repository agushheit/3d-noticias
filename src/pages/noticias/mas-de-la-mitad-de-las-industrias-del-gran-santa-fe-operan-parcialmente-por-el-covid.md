---
category: La Ciudad
date: 2022-01-06T06:15:13-03:00
thumbnail: https://assets.3dnoticias.com.ar/indistria.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Más de la mitad de las industrias del Gran Santa Fe operan parcialmente por
  el Covid
title: Más de la mitad de las industrias del Gran Santa Fe operan parcialmente por
  el Covid
entradilla: Los datos se desprenden de un estudio de la Unión Industrial local, Camsfe
  y el Parque Industrial de Sauce Viejo. En el comercio los empleados aislados rondan
  el 10 por ciento

---
La tercera ola de Covid generó una suba de contagios sin precedentes en los últimos días. Este martes la ciudad de Santa Fe superó los 1.000 casos en 24 horas y eso se está viendo con preocupación en las diferentes actividades económicas. Tanto en el comercio como en la industria se extremaron los protocolos para los cuidados y así evitar que los trabajadores se contagien y se terminen afectando las tareas diarias.

Según datos preliminares de un informe elaborado por la Unión Industrial de Santa Fe, la Cámara de Industriales Metalúrgicos y Autopartistas de Santa Fe (Camsfe) y el Parque Industrial de Sauce Viejo, el 51,9% de las industrias del Gran Santa Fe está operando parcialmente, y el 48,1% opera con normalidad.

Asimismo se indicó que el 61,5% de las empresas registran empleados con aislamiento preventivo. Mientras que el 38% de éstas, posee entre siete y cuatro empleados en situación de riesgo sanitario.

Además, el trabajo aclara que de las empresas encuestadas, el 54% es del rubro metalúrgico y autopartista; luego se dividen en papeleras, alimenticias, software y servicios, entre otros. Mientras que la distribución geográfica muestra que el 52% es de Santa Fe; el 28% de Sauce Viejo; y el porcentaje restante se dividen en Recreo, Santo Tomé y Monte Vera.

## La industria refuerza los protocolos

A pesar de esa situación, el presidente de la Unión Industrial de Santa Fe, Alejandro Taborda, aseguró que "hoy por hoy no hay un desmadre de casos" en la industria local y eso le permite a esa rama de la actividad económica sostener los niveles de producción sin mayores inconvenientes. "Hasta ahora no estamos teniendo complicaciones para poder cumplir con los turnos. No pasa lo mismo en otros lugares como Córdoba o provincia de Buenos Aires que tienen complicaciones. En esta zona todavía estamos trabajando bien".

Taborda recordó que el jueves de la semana pasada mantuvieron una reunión con el ministro de la Producción, Daniel Costamagna; con la ministra de Salud, Sonia Martorano; y con el de Trabajo, Juan Manuel Pusineri. En ese encuentro los funcionarios les advirtieron a los empresarios que la suba de contagios estaba siendo muy rápida y que cada vez era más alto el índice de positividad en los testeos que se estaban realizando.

"Nos pidieron reforzar los protocolos, pero en la industria nunca dejamos de usarlos. Hoy estamos teniendo problemas con los aislados por contactos estrechos, más que por empleados que se contagien. Al principio de la pandemia sí tuvimos contagios, pero ahora hay personas que se aíslan porque su hijo o su hija dieron positivo", explicó el referente de la industria local.

Taborda afirmó que en la industria se siguen extremando los cuidados tanto en los lugares de trabajo como en los espacios compartidos para los almuerzos y los sanitarios. E insistió: "En la industria nunca dejamos de usar los protocolos y eso nos permitió no tener problemas de contagios dentro de las plantas. El año pasado tuvimos personal que tuvo síntomas y que no venía a trabajar. Mientras que ahora los que faltan es porque son contactos estrechos de un caso positivo".

**El comercio extrema cuidados**

Desde el Centro Comercial se hizo un relevamiento puntual que se basó sobre todo en supermercados y algunas tiendas grandes con varias sucursales. "La cantidad de empleados contagiados ronda el 10 por ciento. Por ahora no se está resintiendo la atención en el comercio santafesino por personas contagiadas de Covid", aseguró  Martín Salemi, presidente de la entidad.

Luego agregó: "La cepa no es tan peligrosa porque hoy no tenemos la cantidad de personas internadas como sí las hubo en otro momento. Hasta ahora lo que vemos es que es como una gripe fuerte y la gente se tiene que aislar. No es como la anterior que dejaba a muchas personas en terapia intensiva".

"Los protocolos los estamos reforzando por motus propio. En todos los comercios siguen pidiendo los barbijos, reforzaron el uso de alcohol y la distancia. Más que eso hoy no podemos hacer. Estamos viendo que subieron los contagios pero no hay internaciones y eso es un alivio", expresó.

La suba de contagios en la ciudad sigue la línea de lo que sucede en la provincia y en el país donde se siguen batiendo récords de casos. En ese contexto este miércoles el ministro de Trabajo de la provincia señaló que desde el gobierno se descartan más restricciones que las impuestas desde el viernes pasado, aunque sí se solicitará reforzar todos los protocolos para evitar que siga creciendo la ola de contagios.