---
category: La Ciudad
date: 2021-04-15T07:40:22-03:00
thumbnail: https://assets.3dnoticias.com.ar/vacunación.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Diario Uno
resumen: Esta semana finaliza la vacunación a docentes y los cierres de escuelas serán
  focalizados
title: Esta semana finaliza la vacunación a docentes y los cierres de escuelas serán
  focalizados
entradilla: Así lo confirmó la ministra de Educación, Adriana Cantero. Advirtió que
  la decisión de suspender la presencialidad será "sanitaria". Pidió extremar los
  cuidados fuera de los establecimientos escolares.

---
Este martes, se reunió el Consejo Federal de Educación ampliado, encuentro del cual participó la titular de la cartera educativa de Santa Fe, Adriana Cantero. Comentó que el cónclave sirvió no solo para analizar “la situación crítica por la que pasa la Argentina desde el punto de vista epidemiológico, sino también para abordar la planificación de las otras tareas del año lectivo”.

En conferencia de prensa, destacó que en la reunión se establecieron “criterios que organicen el sistema frente a la eventualidad de tener que aplicar alguna restricción mayor; siempre pensando en la temporalidad y en que la prioridad es mantener el vínculo presencial”.

Señaló que el cierre de establecimientos educativos seguirá la lógica del análisis epidemiológico y no educativo. Y explicó: “Si en algún momento las autoridades sanitarias indican una mayor restricción en alguna comunidad, que involucre la vida escolar, esa decisión se tomará en ese momento y focalizadamente".

Sostuvo que el sistema debe ser "flexible; es decir, estar dispuestos a cerrar y abrir conforme la curva epidemiológica vaya poniendo condiciones". Insistió en que esa decisión "va a seguir siendo siempre una definición sanitaria y no pedagógica”.

Afirmó que la escuela, en ningún caso, será la determinante de la restricción; sino que antes estarán otras actividades o sectores. “La actividad de la escuela no ha sido el detonante del crecimiento, hay otros factores", dijo.

Igualmente, reconoció que "la actividad escolar incrementa los desplazamientos. En el momento en que nuestras autoridades sanitarias digan que se requiere una restricción, nosotros estaremos dispuestos a aportar desde el sistema educativo el acompañamiento y cuidado más intensivo que se proponga”.

Sobre el planteo de Amsafé, de suspender la presencialidad en los lugares de riesgo, Cantero destacó que los dirigentes gremiales participaron de la reunión del Consejo Federal que ratificó “el cuidado de la presencialidad todo lo que sea posible”.

Sobre la vacunación a docentes, la ministra adelantó que “está finalizando en esta semana la vacunación de los docentes registrados, con voluntad de ser vacunados”. Aseguró que Santa Fe se convertirá en “la primera provincia que culmine con esa meta”.

Con relación a los contagios detectados entre docentes y alumnos, Cantero afirmó que “en relación a los informes sanitarios, no hay un incremento significativo en el porcentaje de contagios de niños”.

Reconoció que la detección de casos en la escuela crece pero que “tiene que ver con el crecimiento (de circulación) que se produce en toda la comunidad”

En esa línea, apuntó: “Se comprobó en el todo el territorio nacional que los contagios no se producen en la escuela; que la escuela sigue siendo un lugar cuidado si se aplican los protocolos. Pero obviamente, si no nos cuidamos fuera de la escuela esos casos repercuten en la vida institucional educativa”.

La ministra pidió que los cuidado se extremen “en la cotidianeidad, puertas afuera de la escuela, para que podamos sostener la mayor presencialidad posible”.