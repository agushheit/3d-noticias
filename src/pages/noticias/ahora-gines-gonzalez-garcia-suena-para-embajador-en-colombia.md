---
category: Agenda Ciudadana
date: 2021-03-05T07:13:30-03:00
thumbnail: https://assets.3dnoticias.com.ar/colombia.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Clarín
resumen: Ahora Ginés González García suena para embajador en Colombia
title: Ahora Ginés González García suena para embajador en Colombia
entradilla: 'El ex ministro echado por el vacunatorio VIP está entre los nombres que
  impulsa Alberto Fernández, pero hay dudas de su conveniencia, a causa del escándalo. '

---
Afirman en el Gobierno que por estas horas Alberto Fernández ya habría dado su aval para que se nombre un embajador en Colombia. 

Una versión de fuentes oficiales indica que su amigo y ex ministro de Salud, Ginés González García, despedido por el escándalo del Vacunatorio VIP, es uno de los favoritos del Presidente a ocupar ese puesto. González García fue  embajador en Chile por largos años cuando al asumir su presidencia, Cristina Kirchner buscó alejarlo de su Gabinete.

Otra versión en Bogotá indicaba que ese nombre se habría cambiado por otro -no conocido por Clarín-, luego de que consejeros presidenciales convencieran de no consolar a Ginés con la "dulzura" de un destino diplomático confortable, como suele ocurrir en la política argentina. El Presidente suele insistir en su "dolor" por haber tenido que desprenderse de Ginés.

Lo cierto es que desde la partida de Marcelo Stubrin, dirigente radical de larga trayectoria y embajador político en Bogotá hasta diciembre de 2019, la embajada de uno de los países más importantes de Latinoamérica ha estado sin jefatura.

Fuentes diplomáticas confiaron que la razón por la que Fernández no ha querido nombrar embajador es "simplemente ideológica".

Porque considera a Iván Duque un gobierno de derecha y porque mantiene con él importantes diferencias sobre la situación de Venezuela, principalmente dentro del espacio del Grupo de Lima.

También hubo choques por la reelección de Luis Almagro en la OEA -que Argentina rechazó- y por la elección de presidente del estadounidense Mauricio Claver Carone al frente del Banco Interamericano de Desarrollo.

Otra razón no confesa es que Mauricio Macri fluía con Duque y con Juan Manuel Santos. El ex presidentes viajó tres veces a Colombia. Una en 2016 cuando hizo una visita de Estado. Ese mismo año fue a Cartagena a la firma del Acuerdo de Paz con la guerrilla colombiana de Santos. Y en agosto de 2018 fue a la asunción del hoy presidente.

Clarín consultó en Bogota por esta situación que consideran "inexplicable". Dijeron: "Nadie entiende", y remarcaron que mientras que Fernández dispuso tener embajador en Brasil pese a sus malos vínculos con Jair Bolsonaro, a Colombia no se le ocurre quitar al embajador en Cuba pese a que son gobiernos de diferente corte ideológico. 

Señalaron también que Colombia mantiene en Buenos Aires a su embajador Alvaro Pava Camelo, quien aún durante la cuarentena puso andar un mecanismo virtual en la sede diplomática, que no dejó de funcionar ni en los días de sus festividades.

"Pero en el caso nuestro (de Argentina hacia Colombia) hay una relación respetuosa, con desacuerdos como pasa muchas veces pero no entendemos lo que está pasando . Es nunca visto", dijeron a este diario desde la capital colombiana.

Fernández y Duque mantuvieron una tensa aunque cuidadosa reunión el 8 de noviembre pasado. Fue en La Paz, Bolivia, durante la asunción de Luis Arce como presidente. Luego se han visto en encuentros virtuales y nunca escalaron sus diferencias.

Afirman que ahora el apuro por nombrar un embajador argentino en Bogotá es porque se viene  la Copa América, y esta se jugará en Argentina y Colombia. Duque debe venir a la inauguración y  Fernández a la clausura. Otro impulso a la normalización del vínculo habría sido el diálogo telefónico que mantuvieron Duque y el presidente Joe Biden, que mostró a los kirchneristas la importancia que tienen los colombianos para el demócrata.

Pero el trato dispensado por la Casa Rosada a la Casa Nariño fue la misma que le dieron a Bolivia y Ecuador. Alberto F. no quiso nombrar embajador ante Jeanine Añez por considerar que su gobierno era de facto. Al asumir Luis Arce el poder, decidieron enviarle al dirigente kirchnerista Ariel Basteiro.

En Ecuador lo mismo. Lenin Moreno, les rechazó dar el plácet a un dirigente de apellido Vila, y entonces no hay embajador desde hace un año y tres meses, a la espera del triunfo del correista Andres Arauz.