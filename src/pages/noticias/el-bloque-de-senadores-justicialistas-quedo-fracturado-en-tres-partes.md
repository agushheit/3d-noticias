---
category: Agenda Ciudadana
date: 2020-12-15T12:06:12Z
thumbnail: https://assets.3dnoticias.com.ar/legislatura.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Diario La Capital'
resumen: El bloque de senadores justicialistas quedó fracturado en tres partes
title: El bloque de senadores justicialistas quedó fracturado en tres partes
entradilla: El quiebre del espacio mayoritario en el cuerpo no hizo más que visibilizar
  las diferencias entre los afines a Perotti y los leales al sanlorencino.

---
Todavía sin sesionar en extraordinarias, la de ayer fue otra jornada agitada en el seno de la Cámara alta santafesina. A la **previsible ruptura del bloque justicialista, que finalmente quedó partido en tres**, se sumó la expectativa por el anunciado pedido de desafuero de los fiscales rosarinos Matías Edery y Luis Schiappa Pietra para avanzar en la investigación contra el peronista Armando Traferri. Según consignaron fuentes parlamentarias a La Capital, ese trámite ingresará formalmente hoy al Senado.

Un ojo de los senadores estuvo puesto en la conferencia de prensa que ofreció Traferri a los medios de comunicación en la delegación rosarina del Senado y el otro en la mesa de entadas de la Legislatura santafesina.

**A la misma hora que el sanlorencino exponía su defensa, al despacho de la vicegobernadora y presidenta del Senado, Alejandra Rodenas, ingresaba una nota en la que cuatro legisladores comunicaban formalmente su decisión de escindirse del bloque mayoritario y conformar una nueva bancada del PJ**.

Se trata de los senadores Marcelo Lewandowski (departamento Rosario), Alcides Calvo (Castellanos), Ricardo Kaufmann (Garay) y Marcos Castelló (La Capital), quienes comunicaron formalmente su decisión de conformar el bloque Lealtad.

"Es una cuestión política que se viene dando desde hace varios meses y que se desencadenó luego de las dos leyes que se sacaron contra el ministro de Seguridad (Marcelo Sain). Eso fue un quiebre, pero la situación venía de arrastre. No tiene que ver exclusivamente con la pulseada entre Traferri y Sain", deslizaron a este diario en los pasillos de la Legislatura.

En rigor, la fractura del bloque mayoritario no hizo más que blanquear la disidencia de varios de sus integrantes (cuatro, por el momento) con la conducción ejercida por Traferri al frente de la bancada oficialista Juan Domingo Perón.

Los cuatro senadores ahora emigrados, además de los representantes de Caseros, Eduardo Rosconi, y de San Martín, Cristina Berra, votaron en reiteradas oportunidades y a lo largo del año legislativo a favor de las iniciativas promovidas por la Casa Gris y a contramano de las posiciones de los restantes seis justicialistas que responden a Traferri.

Por su parte, tanto Berra como Rosconi siguieron el mismo camino de abandonar la bancada del Nuevo Espacio Santafesino (NES), que lidera el senador por San Lorenzo, pero no se sumaron a Lealtad. Armarán bloque propio, también en sintonía con Perotti.

"Dejamos abierta la posibilidad de incorporar otros senadores", suscribieron Lewandowski, Castelló, Calvo y Kaufmann. Y, fuera de micrófono, acotaron: "Berra y Rosconi no firmaron la nota porque apoyan al gobernador, pero no Sain".

Paralelamente —y también en voz baja—, legisladores del oficialismo y de la oposición se manifestaron contrarios "a abrir en lo inmediato, y sin que envíen todos los elementos que los fiscales dicen haber recolectado en la causa", un procedimiento de desafuero contra Traferri.

"Hay que estudiar seriamente el caso y también evaluar si eso puede hacerse durante el período extraordinario", opinó un legislador del PJ.

En la bancada opositora se animaron a redoblar la apuesta: "Nos gustaría escuchar las opiniones de Perotti y de Rodenas, porque la investigación judicial involucra a un legislador de su partido", comentó un senador de la UCR. De ese modo, adelantó el picante debate que se avecina en el Palacio de la avenida General López.

***

## **Ponce Asahad**

El ex fiscal Gustavo Ponce Asahad seguirá detenido con prisión preventiva efectiva hasta el 2 de febrero de 2021. Ponce Asahad está acusado de integrar la asociación ilícita en la causa por coimas provenientes del juego clandestino que roza al Poder Judicial y a sectores de la política santafesina.