---
category: Estado Real
date: 2020-12-27T12:11:58Z
thumbnail: https://assets.3dnoticias.com.ar/2712luminarias.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: La provincia entregó luminarias LED para Escuelas de la Familia Agrícola
title: La provincia entregó luminarias LED para Escuelas de la Familia Agrícola
entradilla: Son artefactos amigables con el ambiente, que reducen costos de consumo
  y mejoran la luminosidad.

---
El gobierno de Santa Fe entregó, a la Unión de Escuelas de la Familia Agrícola, 70 reflectores LED que distribuirán en 12 establecimientos. Los mismos se encuentran distribuidos en los departamentos San Cristóbal, San Javier, Vera, General Obligado, 9 de julio, San Justo e Iriondo.

A través del Ministerio de Ambiente y Cambio Climático se dio respuesta a esta demanda con equipamiento sustentable, que genera un beneficio económico, ambiental y social, ya que las instituciones se encuentran en zonas rurales y son referencias en cada localidad.

«Como siempre decimos, la acción climática es ahora y estamos convencidos de que tenemos que hacerlo todas y todos juntos, con la participación de todos los actores cada uno aportando desde su lugar; con las escuelas EFA venimos trabajando desde hace tiempo y esta era una de las solicitudes que habíamos recibido», contó la ministra de Ambiente y Cambio Climático, Erika Gonnet.

«Los reflectores LED ahorran un 50 por ciento de la energía respecto de los reflectores tradicionales, ahorrando energía, costos energéticos y emisiones de CO₂», resaltó el Ing. Franco Blatter, subsecretario de Tecnologías para la Sostenibilidad. Y agregó: «A su vez tienen una vida útil mayor, por lo que son una tecnología de iluminación más sustentable».

«Las escuelas EFA, por ser escuelas rurales, requieren iluminación exterior para delimitar senderos de acceso seguro, por lo que estos reflectores suman infraestructura comunitaria sostenible a las escuelas. Las inversiones en tecnología eficiente muchas veces es dificultosa para las instituciones sociales o educativas, como en el caso de las EFA, en este sentido es importante que el gobierno de Santa Fe continúe llevando tecnología sostenible a estos sectores tan importantes de nuestro tejido social», concluyó Blatter.

Junto a Gonnet y Blatter, participaron de la entrega el secretario de la Unión de EFA, Mario Zapata; el director de la EFA N.º 8209 «San Martín Norte», Horacio Cortina; el presidente comunal de San Martín Norte, Juan Testa, y el asesor del ministerio, Maximiliano Aimaretti.

  
 