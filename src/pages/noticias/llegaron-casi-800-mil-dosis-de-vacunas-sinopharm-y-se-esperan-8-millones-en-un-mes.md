---
category: Agenda Ciudadana
date: 2021-07-05T06:00:25-03:00
thumbnail: https://assets.3dnoticias.com.ar/SINOPHARM.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Llegaron casi 800 mil dosis de vacunas Sinopharm y se esperan 8 millones
  en un mes
title: Llegaron casi 800 mil dosis de vacunas Sinopharm y se esperan 8 millones en
  un mes
entradilla: 'El cargamento corresponde a inyecciones del laboratorio chino que integran
  un total contratado de 24 millones de dosis. '

---
Argentina superó los 27,6 millones de vacunas recibidas deUna partida de 768 mil vacunas de la empresa china Sinopharm llegó este domingo a la Argentina desde Beijing, en el marco de un operativo que implicará el arribo diario de vuelos toda esta semana y que permite contar hasta ahora con más de 27,6 millones de dosis procedentes de varios laboratorios para combatir el coronavirus.

El Airbus 330-200, matrícula LV-FVH, con el número de vuelo AR1071, aterrizó a las 18.11 en el aeropuerto internacional de Ezeiza, luego de haber despegado ayer a las 16.21 desde la capital china y tras una escala técnica para reabastecimiento, este domingo poe la mañana, en el aeropuerto de Barajas, en Madrid.

El avión fue recibido por Alejandro Collia, subsecretario de Gestión de Servicios e Institutos del Ministerio de Salud de la Nación, quien se manifestó "muy contento" por la llegada de este nuevo cargamento que se suma al plan de vacunación del Gobierno nacional.

"Siguen llegando vacunas, hoy estamos recibiendo 768 mil dosis de Sinopharm en uno de los 10 vuelos que durante el mes de julio estarán trayendo 8 millones de vacunas desde China, que forman parte del contrato de 24 millones que firmó el Gobierno con el laboratorio chino", dijo el funcionario.

Destacó, además, que "ya se distribuyeron más de 25 millones de dosis y se aplicaron más de 22 millones, de las cuales 17.800.000 corresponden a la primera dosis y 4.400.000 a la segunda, con la cual se completó el esquema de vacunación".

"Está claro que el presidente de la Nación, Alberto Fernández, y la ministra de Salud, Carla Vizzotti, están trabajando para que lleguen más vacunas", enfatizó.

Este fue el primer servicio de un operativo que se inició el viernes con la partida de este mismo avión, bajo el número AR1070, que salió de Ezeiza y aterrizó en Beijing el sábado.

Desde el miércoles de la semana próxima, 7 de julio, comenzarán a despegar diariamente sucesivos vuelos, hasta el 15 de julio, cuando partirá el ultimo de este operativo.

Los regresos de estos vuelos están previstos entre los días 9 y 17 de julio, fecha en que arribará el último de esta primera etapa, completando en ese momento 8 millones de dosis del acuerdo.

**Los operativos en los próximos meses**

Durante agosto y septiembre se ejecutarán operativos similares para transportar otros 8 millones cada mes hasta completar el convenio por 24 millones firmado por el Gobierno nacional con el laboratorio chino.

Para el operativo se utilizarán tres aviones Airbus 330-200 de la flota de largo alcance de la compañía, que irán haciendo los recorridos de forma casi ininterrumpida durante los próximos tres meses.

Entre el 9 y el 15 de julio habrá en determinados momentos del día tres vuelos de Aerolíneas Argentinas circulando entre Buenos Aires, Madrid y Beijing y viceversa, dos de los cuales se estarán cruzando en Europa y otros dos en Brasil, en la medida en que se mantengan los horarios previstos de despegue y aterrizaje.

"Estos 10 vuelos son una gran noticia porque, en un período de pocos días, la Argentina contará con una enorme cantidad de nuevas dosis, lo cual va a permitir acelerar aún más esta campaña de vacunación sin precedentes" señaló Pablo Ceriani, titular de Aerolíneas.

El segmento de 8 millones de vacunas forma parte del acuerdo recientemente sellado con el China National Pharmaceutical Group Corp, mediante el cual llegarán 24 millones.

Aerolíneas Argentinas lleva completados 29 vuelos para el transporte de vacunas desde el inicio de la pandemia, de los cuales 8 tuvieron como destino a Beijing, de donde se trajeron 5.963.200 dosis.

Además, se concretaron hasta ahora 21 vuelos a Moscú en los que se transportaron 11.263.375 dosis de la vacuna Sputnik V, producida por el Instituto Gamaleya, por lo que, en el total de operaciones realizadas por la línea de bandera, se trasladaron 17.226.575 dosis.

Hasta este domingo arribaron al país 27.615.730 dosis, de las cuales 11.265.830 corresponden a Sputnik V, (9.375.670 del componente 1 y 1.890.160 del componente 2); 6.768.000 a las Sinopharm; 580.000 a AstraZeneca-Covishield, 1.944.000 a AstraZeneca por el mecanismo COVAX de la OMS y 7.057.900 a las de AstraZeneca y Oxford cuyo principio activo se produjo en la Argentina.

La asesora presidencial Cecilia Nicolini destacó que el reciente DNU del presidente Alberto Fernández busca "avanzar en la firma de nuevos contratos" para, por ejemplo, "adquirir vacunas pediátricas" y aseguró que "las exigencias que los laboratorios tenían" cambiaron y eso se plasmó en la medida.

"El objetivo del Presidente de firmar este DNU es de alguna manera ampliar el marco legal para que nos permita en primera instancia poder adquirir vacunas pediátricas, para niños, niñas y adolescentes frente a una demanda creciente que tenemos que suplir", dijo.

Ratificó, además, que se espera "esta semana poder recibir un nuevo cargamento de segunda dosis de Sputnik V" desde el inicio de la campaña de inmunización.