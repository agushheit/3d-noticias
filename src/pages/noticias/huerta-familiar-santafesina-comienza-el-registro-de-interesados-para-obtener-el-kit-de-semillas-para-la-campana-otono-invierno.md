---
category: Estado Real
date: 2021-05-23T06:00:18-03:00
thumbnail: https://assets.3dnoticias.com.ar/HUERTA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Huerta Familiar Santafesina: comienza el registro de interesados para obtener
  el kit de semillas para la campaña otoño invierno'
title: 'Huerta Familiar Santafesina: comienza el registro de interesados para obtener
  el kit de semillas para la campaña otoño invierno'
entradilla: Inicia una nueva etapa del programa que busca fortalecer la soberanía
  alimentaria y la promoción de hábitos saludables en la alimentación de los santafesinos
  y santafesinas.

---
El Ministerio de Producción, Ciencia y Tecnología, a través de las secretarías de Agroalimentos y de Desarrollo Territorial y Arraigo informa que se encuentra abierto el registro de interesados en obtener el kit de semillas para la campaña otoño-invierno del Programa Huerta Familiar Santafesina, iniciativa del Gobierno Provincial que estimula a las familias y otros actores como escuelas y organizaciones sociales de la provincia a producir sus propios alimentos en huertas urbanas, comunitarias, escolares o rurales.

De esta forma se inicia la tercera etapa del Programa, herramienta que se complementa con el programa Prohuerta del Instituto Nacional de Tecnología Agropecuaria (INTA) y que superó ampliamente las expectativas en las dos primeras etapas, previendo continuar a través del tiempo para fortalecer la soberanía alimentaria y la promoción de hábitos saludables en la alimentación de los santafesinos y santafesinas.

En esta ocasión el kit de semillas contiene siete cultivos y una especie aromática: acelga, achicoria, cebolla morada, caléndula, habas, lechuga de invierno, remolacha y rúcula. En tanto que, una vez culminada la instancia de inscripción, los días y puntos de entrega serán informados a través de las redes sociales oficiales del Gobierno Provincial y del Programa (Facebook: Huerta Familiar Santafesina e Instagram: @huerta_familiar_santafesina). Los cultivos seleccionados presentan hábitos de crecimiento rápido con adecuado perfil sanitario y contribuyen a acceder a una alimentación saludable con gran aporte nutricional.

El secretario de Desarrollo Territorial y Arraigo, Fabricio Medina, indicó que “este Programa se está articulando con distintas carteras ministeriales, como Desarrollo Social, Educación, Secretaría de Estado de Género, organizaciones, secretarías de producción de los gobiernos locales, entre otros para potenciar su alcance y llegar a la mayor cantidad de santafesinos”

Por otra parte, la subsecretaria de Coordinación Agroalimentaria, María Eugenia Carrizo destacó que “esta política pública es clave en este momento de aislamiento dispuesto por el gobierno nacional y provincial para que las familias puedan construir en sus hogares una huerta agroecológica y consecuentemente poder producir sus propios alimentos, generar un ahorro económico y una actividad compartida entre los miembros de las familias.

Para registrar la solicitud de semillas hacer click en el siguiente enlace: www.santafe.gob.ar/huertafamiliar/registrosolicitante. Dado la gran cantidad de ingresos se recomienda intentar reiteradas veces cuando no se puede acceder.

**Números del programa**

A partir del programa Huerta Familiar Santafesina, en dos etapas llevadas adelante durante 2020, desde el Ministerio de Producción, Ciencia y Tecnología se otorgaron más de 100 mil kits de semillas, alcanzando a más de 400 mil beneficiarios en los 19 departamentos de la provincia y permitiendo un ahorro de 500 millones de pesos en alimentos por la producción de alimentos en huertas familiares urbanas o rurales, comunitarias y escolares.

Además, en el marco del programa, se realizaron capacitaciones sobre cómo confeccionar una huerta y campañas de comunicación de consejos para aquellos que recibieron las semillas. Por contactos, consultas o dudas, los interesados pueden escribir a: huertafamiliarsantafesina@santafe.gov.ar o visitar las redes sociales antes mencionadas.