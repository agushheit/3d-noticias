---
category: La Ciudad
date: 2021-12-20T06:15:36-03:00
thumbnail: https://assets.3dnoticias.com.ar/nowater.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: El martes habrá un corte de agua en la ciudad de Santa Fe
title: El martes habrá un corte de agua en la ciudad de Santa Fe
entradilla: 'Es por un corte programado para conectar nuevos equipos adquiridos por
  la empresa que suministra el servicio de agua a la ciudad. Será durante dos horas

'

---
Desde Aguas Santafesinas S. A. (Assa) informan a los usuarios de la ciudad de Santa Fe que en la madrugada del martes 21, entre las 3 y las 5 estará interrumpido el servicio de agua potable en toda la ciudad.

Estos trabajos programados permiten conectar nuevos equipos adquiridos como parte del plan de mejoras y renovación de las instalaciones de potabilización. Los trabajos fueron programados en horario nocturno para minimizar las molestias.

Posteriormente podría registrarse algún episodio de turbiedad, la cual se supera dejando circular agua por unos minutos.

**Lugares donde puede haber reducción del tránsito en la ciudad**

La empresa Assa recomendaron que desde el jueves 16 y hasta el 30 de diciembre, de 6 a 0 se proseguirá con el plan de mantenimiento y rehabilitación de válvulas de cañerías primarias de la red de distribución. Mientras se desarrollen dichas tareas estará reducido el tránsito vehicular. Circular con precaución.

Atención al Usuario 24 horas por WhatsApp: +54 341 695-0008 o en [www.aguassantafesinas.com.ar](https://www.aguassantafesinas.com.ar/).