---
category: La Ciudad
date: 2021-09-08T06:15:05-03:00
thumbnail: https://assets.3dnoticias.com.ar/MANGA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: La manga del Cullen seguirá montada sobre Av. Freyre
title: La manga del Cullen seguirá montada sobre Av. Freyre
entradilla: 'El Dr. Juan Pablo Poletti destacó que sin la conexión por la avenida
  no puede funcionar. La decisión ministerial, por el momento, es no desarmar las
  estructuras sanitarias para contener la pandemia.  '

---
Los comerciantes, que tienen locales en la zona del hospital Cullen, presentaron notas dirigidas al director Dr. Juan Pablo Poletti y al Ministerio de Salud de la Provincia para que se revea la colocación de la maga que une al nosocomio y al hospital de campaña, ubicado al cruzar Av. Freyre en el Liceo Militar.

A pesar de la insistencia y las molestias que genera la manga al cortar el tránsito en una zona muy transitada de la ciudad, no será removida. Poletti, quien recibió a los comerciantes el viernes pasado, dialogó con El Litoral y contó los porqués de esta decisión de mantener instalada la manga y el hospital modular.

"Fue una reunión amena, pero quizás ellos se fueron con las respuestas que no esperaban. Les di una respuesta desde el punto de vista sanitaria, de lo que estamos viviendo y lo que vamos a vivir, y en base a una decisión ministerial que es por ahora no desarmar nada de lo armado para la pandemia, ya que la respuesta que tuvo a nivel ciudad fue excelente", resaltó el director del Cullen.

Consultado por si hay posibilidades de que la estructura sanitaria en el Liceo Militar funcione sin la manga, Poletti afirmó que es "inviable" y explicó: "Para nosotros el hospital militar es una sala más del hospital. En junio tuvo un 73%, 83% en julio, y 78% en agosto, es decir que nunca bajó del 75% de ocupación. Hay en promedio 26 pacientes internados y por cada paciente hay que llevar remedios, tubos de oxígeno, recambio de sábanas, ropa sucia, residuos patológicos y comunes, y también la comida cuatro veces al día. Además, del total de los pacientes hay algunos que van a quirófano, por lo que si se quita la manga habría que cruzarlos en un sillón de ruedas o en ambulancia que debe dar la vuelta hasta Juan de Garay".

A su vez sostuvo que la manga es clave para el funcionamiento logístico y que permite en menos de dos minutos estar en la Guardia central del Cullen. "Desde el punto de vista logístico es inviable retirar la manga", remarcó.

**Situación actual**

En salas generales, tanto Covid y no Covid, el hospital Cullen cuenta actualmente con una ocupación del 83%. Mientras que en terapia intensiva están ocupadas el 79% de las camas, "hay 12 camas libres sobre 55 camas críticas. En salas de terapia Covid la ocupación es del 62%, entre sospechosos y positivos".

De este panorama, el director también indicó que hay 13 camas libres generales de hombres, pero que esas camas libres con las que se cuenta están ubicadas en la carpa militar. "Hay 21 ocupadas y 13 libres en el modular, es decir que si hoy no estaría la carpa militar no tendría camas libres en la estructura de cemento del Cullen", admitió.

Más allá de la baja de los casos e internaciones Covid, en el Cullen sigue habiendo salas generales y de terapia destinadas exclusivamente a pacientes Covid o sospechosos, tal es así que el director ejemplificó con la Sala 7, un pabellón con 24 camas generales está montado con una terapia intensiva de 8 camas Covid.

"El Consejo y la Dirección del hospital somos los primeros que queremos que se cierre la carpa porque todo es una logística que nos implica trabajar con más camas. Pero no podemos levantar la carpa y que en 15 o 20 días venga la cepa Delta y no tenga camas y tengamos que internar gente en los pasillos. Los números justifican que para nosotros la carpa debe seguir montada", concluyó Poletti.