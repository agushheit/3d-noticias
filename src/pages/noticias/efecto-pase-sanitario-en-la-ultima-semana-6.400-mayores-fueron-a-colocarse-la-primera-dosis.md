---
category: Agenda Ciudadana
date: 2021-12-23T06:00:10-03:00
thumbnail: https://assets.3dnoticias.com.ar/CARNET.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Efecto pase sanitario: en la última semana, 6.400 mayores fueron a colocarse
  la primera dosis'
title: 'Efecto pase sanitario: en la última semana, 6.400 mayores fueron a colocarse
  la primera dosis'
entradilla: 'La medida que comenzó a regir este martes, sumado al crecimiento de los
  contagios y a la aparición de la variante ómicron, provoca un crecimiento en el
  nivel de vacunación.

'

---
La confirmación y puesta en marca del pase sanitario, sumado a la suba de los contagios (con la amenaza de una nueva ola) y la aparición de la nueva variante ómicron provocaron que creciera la campaña de vacunación entre aquellos que, por diversos motivos, optaron por no vacunarse.

Los datos revelan que la semana pasada (del 13/12 al 17/12), en la cual el gobierno oficializó el inicio del pase sanitario, el Ministerio de Salud de Santa Fe registró 6.400 nuevos vacunados, mayores de 18, con primera dosis.

Si se lo compara con la última semana completa de noviembre, cuando todavía no estaba en agenda el debate entorno al pase sanitario, el crecimiento es de más del 22 por ciento, ya que entre el 22/11 y el 26/11 se vacunaron con primera dosis un total de 5.242 mayores (1.158 más)

Otro dato relevante son las segundas dosis colocadas, teniendo en cuenta que el pase sanitario exige el esquema completo de vacunación. La semana pasada se pusieron 1.900 segundas dosis, contra apenas 300 de la última semana de noviembre.

Según pudo saber este medio, a nivel provincial hay aproximadamente 220 mil personas que no completaron el esquema de vacunación; es decir, no concurrieron al turno para recibir el segundo componente. De ese total, el 72 por ciento tienen menos de 33 años.

Desde la cartera sanitaria entienden que el aumento de las personas que se fueron a inmunizar obedece a varios fenómenos y no es exclusivo del Pase Sanitario. En esa línea, destacan como otros motivos que pudieron haber alentado a la vacunación la suba de contagios registrada en las últimas semanas, sumado a los interrogantes que plantea la nueva variante ómicron.

Datos relevado al 21 de diciembre reflejan que en la provincia de Santa Fe hay un total de 3.064.068 vacunados (con al menos una dosis), de los cuales 1.170.156 tienen entre 18 y 59 años y no padecen comorbilidades. Con esquema completo de vacunación hay 2.750.221; en tanto que 213.363 recibieron una dosis adicional y 136.451 dosis de refuerzo.

Es que la situación epidemiológica vuelve a encender las alarmas en la provincia de Santa Fe. Si bien la evolución de contagios con síntomas importantes e infectados que requieren internación se restringe a casos puntuales, los centros de testeos en Santa Fe ya advierten una escalada progresiva en las personas asistidas.

Datos de la Región Santa Fe de Salud reflejan que la tasa de positividad se mantuvo durante semanas entorno al 2,5%. Ahora, según los últimos registros, ese nivel es del 8% y en paulatino ascenso.

En la ciudad de Santa Fe hay seis puestos fijos de testeo masivo contra el coronavirus ubicados en distintos barrios y que atienden de lunes a lunes de 8 a 12. Se puede asistir sin turno previo y presentando uno o más síntomas compatibles con coronavirus o habiendo sido contacto de casos confirmados.

**Están ubicados en**

\-Estación Belgrano, Bulevar Gálvez 1150

\-Estación Mitre, General López 3698

\-El Alero de Coronel Dorrego, French 1701

\-Playón Municipal de Villa Hipódromo, Ángel Cassanello 4200

\-Cemafé, Mendoza 2419, (de 8.30 a 12.30)

\- Ex-Iturraspe, Bulevar Pellegrini 3551

**Debut del pase sanitario**

El 21 de diciembre entró en vigencia el pase sanitario para la realización de actividades que son consideradas de riesgo epidemiológico por lo que las personas mayores de 13 años deben acreditar haber completado los esquemas de vacunación contra el coronavirus para poder participar de actividades de alto riesgo de contagio.

Las actividades consideradas de mayor riesgo sanitario son viajes grupales de egresados, de estudiantes, jubilados, o similares; actividades en discotecas, locales bailables o similares que se realicen en espacios cerrados; actividades en salones de fiestas para bailes, bailes o similares que se realicen en espacios cerrados; y eventos masivos organizados de más de 1.000 personas que se realicen en espacios abiertos, cerrados o al aire libre.

También será exigible el esquema completo de vacunación a mayores de 13 años para la concurrencia a centros culturales, teatros, cines y gimnasios; participación en reuniones o celebraciones sociales; asistencia a casinos y bingos; y concurrencia a atracciones turísticas, en predios delimitados con controles para el ingreso de la concurrencia.

Entre las dependencias provinciales que ya lo solicitan, se destacan: el Registro Civil, oficinas de Aguas Santafesinas y la Empresa Provincial de la Energía (EPE); en Casa de Gobierno a quienes vayan a realizar trámites; en API y Catastro, y en la obra social de Iapos. Además, ya lo están exigiendo en el ingreso al Casino, Bingo y shopping en el Puerto.