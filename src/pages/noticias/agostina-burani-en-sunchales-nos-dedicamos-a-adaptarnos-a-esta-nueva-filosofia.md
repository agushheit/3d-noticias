---
category: Deportes
date: 2021-05-18T08:26:54-03:00
thumbnail: https://assets.3dnoticias.com.ar/basquet.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa CAB
resumen: 'Agostina Burani: “En Sunchales nos dedicamos a adaptarnos a esta nueva filosofía”'
title: 'Agostina Burani: “En Sunchales nos dedicamos a adaptarnos a esta nueva filosofía”'
entradilla: Designada como subcapitana del combinado nacional, la pivote analizó la
  concentración de la Selección en Santa Fe y definió los próximos objetivos de su
  equipo.

---
Si hay que hablar de jugadoras experimentadas en la Selección, Agostina Burani es una de las más destacadas. Determinante, se ganó su lugar como emblema no solo por sus años defendiendo los colores albicelestes sino también por su solidez y maduración en su juego, en un puesto clave para los sueños de Argentina. Está en una edad justa, plena, siendo una de las líderes dentro y fuera de la cancha del equipo dirigido por Gregorio Martínez que el domingo finalizó su concentración en Sunchales y retornó a Buenos Aires después de una última práctica matutina. El viernes, en el Cenard, el conjunto nacional volverá a reunirse para reiniciar sus entrenamientos.  
  
Con una trayectoria de más de una década representando los colores albicelestes, Burani es una de las jugadoras que formaron parte de la recordada medalla de bronce en el Mundial U19 de Tailandia. Además de ser subcampeona tres veces en torneos FIBA Américas, fue plata en la AmeriCup 2017 y ganó el oro del Sudamericano del 2018.  
  
Todos estos pergaminos hicieron que la interna fuese elegida como la nueva subcapitana nacional. La pivote de 29 años que jugó la última temporada en el Estudiantes de Madrid de la Liga Endesa de España secundará a Melisa Gretter como una de las líderes del equipo y dio sus sensaciones al respecto de su nombramiento: "_Lo recibí bien. A las más chicas les transmito lo que aprendí durante todos estos años en la Selección, lo que en su momento las grandes me enseñaron a mí. Acá todas somos iguales y ninguna es más que otra. Es un orgullo estar en la Selección. Yo siempre me preparo para estar en la mejor forma en el mes de la Selección. La capitanía o subcapitanía creo que es sólo un título, porque acá todas tenemos importancia. Si podemos ser una referente o líder para las más chicas que vienen abajo bienvenido sea. Todas somos iguales y no por eso tenemos otros privilegios"_.  
  
También se refirió a esta etapa de preparación que realizó la selección en Sunchales, atravesando una fase clave pensando en la FIBA Women's AmeriCup que se desarrollará a partir del 11 de junio en la ciudad puertorriqueña de San Juan. Allí, Argentina buscará hacerse con uno de los cuatro boletos rumbo a los clasificatorios del Mundial de Australia 2022.  
  
_"En Sunchales nos dedicamos plenamente a nosotras, en adaptarnos a esta nueva filosofía, en ponernos también en forma para lo que se viene y creo que ya para la próxima concentración vamos a estar mucho más enfocadas en el torneo, viendo rivales y pensando más de lleno en el Premundial",_ dijo la pivote, entendiendo que están en el medio de un proceso de maduración en cuanto al juego.  
  
Además, detalló las sensaciones que comparte el grupo de jugadoras respecto a la idea de juego de Martínez: "_Hacía tiempo que no nos juntábamos y teníamos esos nervios de volver a reunirnos como así también esas ganas de estar de nuevo todas juntas en la Selección, algo que para nosotras es un orgullo. Tenemos entrenador nuevo, por ende nueva filosofía de juego, y estamos adaptándonos a eso. Hay muchas chicas nuevas así que estamos conociéndonos. Los entrenamientos están siendo muy intensos y estamos agarrando el ritmo de juego que Gregorio quiere y eso es muy bueno"_.  
  
Para finalizar, Burani habló de su muy buena temporada en su regreso a Europa, donde en la pasada 2020/21 militó en el Estudiantes de Madrid de la Liga Endesa de España. Fue uno de los pilares de un equipo que sorprendió porque no aparecía dentro de los pronósticos iniciales pero se convirtió en una de las revelaciones de la campaña pasada: _"La oportunidad que me llegó para jugar la Liga Endesa fue inesperada, y encima hacerlo con Meli con quien nunca lo habíamos hecho en Europa, terminó siendo un sueño cumplido. Todo salió de maravillas porque era un equipo recién ascendido y quedamos 5° en la Liga. Se dio todo como esperábamos. Feliz del momento que estoy viviendo hoy, más madura, con mayor experiencia y ahora a pensar únicamente en la Selección"_.