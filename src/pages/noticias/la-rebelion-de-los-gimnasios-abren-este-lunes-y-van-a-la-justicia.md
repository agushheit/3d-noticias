---
category: Agenda Ciudadana
date: 2021-05-10T06:00:06-03:00
thumbnail: https://assets.3dnoticias.com.ar/Gimnasios.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: LT10
resumen: 'La rebelión de los gimnasios: abren este lunes y van a la Justicia'
title: 'La rebelión de los gimnasios: abren este lunes y van a la Justicia'
entradilla: 'Desde la Cámara de Gimnasios de Santa Fe sostienen que la decisión del
  gobernador atenta contra la salud de sus clientes y contra el sustento de los dueños
  y empleados. '

---
Tras el anuncio del gobernador Omar Perotti en el cual se suspendieron hasta el 21 de mayo las actividades en gimnasios, en 14 departamentos de la provincia por la situación sanitaria, la Cámara de Gimnasios Santa Fe realizó una reunión virtual de 2hs con sus asociados y decidió que en las ciudades de Santa Fe, Santo Tomé, Esperanza, Rosario y Rafaela abrirán sus puertas con normalidad este lunes.

Desde la entidad, sostienen en sus redes sociales que repudian el decreto del gobernador que atenta contra la salud de sus clientes y contra el sustento de los dueños y empleados.

Además, la Cámara de Gimnasios de Santa Fe le exigen al gobierno provincial que dé marcha atrás con la medida y no atente contra estas empresas.

Ernesto Capozzolo, integrante de la Cámara de Gimnasios de Santa Fe, manifestó por LT10 que "vamos a apoyar a todos los gimnasios que decidan abrir sus puertas este lunes y vamos a presentar un recurso de amparo porque esta decisión va a contramano de lo que dicen los médicos y la Organización Mundial de la salud. La actividad física le hace frente a la pandemia de Covid y a la de sedentarismo que viene desde hace décadas".

Esta situación de rebeldía genera una gran incertidumbre sobre qué actitud tomarán las autoridades de aplicación del decreto que son las municipalidades.