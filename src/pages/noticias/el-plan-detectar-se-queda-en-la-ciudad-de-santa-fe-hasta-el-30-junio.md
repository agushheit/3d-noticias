---
category: La Ciudad
date: 2021-06-28T06:15:27-03:00
thumbnail: https://assets.3dnoticias.com.ar/Detectar.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: El plan DetectAR se queda en la ciudad de Santa Fe hasta el 30 junio
title: El plan DetectAR se queda en la ciudad de Santa Fe hasta el 30 junio
entradilla: El camión de Nación recorrerá la ciudad y zona para la realización de
  testeos de manera gratuita. Se suma a los cuatro puntos fijos que están en la ciudad
  de Santa Fe. El cronograma completo

---
El camión del plan DetectAR federal estará nuevamente esta semana en la ciudad de Santa Fe y los alrededores colaborando con la búsqueda activa en territorio de casos de Covid-19 en distintos barrios y localidades del departamento La Capital.

Según lo que se informó este lunes llegará a la ciudad de Recreo, al centro de salud del barrio Mocoví donde se estarán realizando testeos en el horario de 10 a 14.

En tanto el martes en Monte Vera, en el Corsódromo, ubicado en E. López al 6000 y el miércoles llegará a barrio Las Flores, en la intersección de Regimiento 12 de infantería y Europa.

A estos lugares se puede asistir de modo espontáneo sin turno previo, presentando uno o más síntomas compatibles con Covid-19 o habiendo sido contacto de casos confirmados.

**Puntos fijos en la ciudad de Santa Fe**

La ciudad cuenta con cuatro puntos fijos en donde se realizan testeos gratuitos a los que se pueden acercar de 9 a 13, todos los días. Estos están ubicados en:

\-la Estación Mitre

\-la Estación Belgrano

\-el Alero de Acería

\-el Alero de Coronel Dorrego