---
category: Agenda Ciudadana
date: 2021-08-26T06:15:03-03:00
thumbnail: https://assets.3dnoticias.com.ar/FUTBOLCONGENTE.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: 'Futbol: El Gobierno define el protocolo y los dirigentes resisten al PCR'
title: 'Futbol: El Gobierno define el protocolo y los dirigentes resisten al PCR'
entradilla: 'La "prueba piloto", según definieron los funcionarios, contará con un
  aforo del 30 por ciento, que junto con el uso del barbijo son por ahora las dos
  medidas establecidas. '

---
El anuncio del Gobierno Nacional del regreso del público a la cancha el próximo 9 de septiembre en el partido entre Argentina y Bolivia, por las Eliminatorias Sudamericanas al Mundial de Qatar 2022, tomó por sorpresa a los dirigentes del fútbol argentino, que decidieron acompañar la medida pero con algunos reparos.

"Es un primer paso para que no solamente vuelva el público a los espectáculos deportivos, no solamente al fútbol, sino a todos los espectáculos masivos”, indicó Matías Lammens, Ministro de Turismo y Deportes, en la conferencia del martes, acompañado por Carla Vizzotti, Ministra de Salud.

La "prueba piloto", según definieron los propios funcionarios, contará con un aforo del 30 por ciento, que junto con el uso del barbijo son por ahora las dos medidas establecidas mientras se trabaja el resto del protocolo.

**Silencio desde la AFA**

Desde la Asociación del Fútbol Argentino (AFA) se optó por el silencio total, sin manifestaciones públicas de Claudio Tapia, presidente de la entidad, y con breves definiciones por parte de Nicolás Russo -presidente de Lanús y mano de derecha de "Chiqui"- y de Amor Ameal, titular de Boca.

Según le explicaron a Télam, la línea dirigencial será mantenerse callados hasta que se firme el Decreto de Necesidad y Urgencia que revierta la situación actual sin gente en las tribunas y acompañar las medidas del Gobierno Nacional como "sucedió desde el principio".

La línea de la AFA será mantenerse callados hasta que se firme el DDNU que revierta la situación actual sin gente en las tribunas y acompañar las medidas del Gobierno

Es por eso que las redes sociales de los dirigentes de mayor peso del fútbol nacional no hicieron mención todavía al tema y posiblemente se mantengan de la misma manera por varios días.

En el caso de Russo y Ameal, ambos coincidieron en lo "imposible" de solicitar una prueba PCR para quienes asistan a los estadios por una cuestión de logística y costos.

"Queremos colaborar con el Gobierno. El hisopado sale 6 mil pesos en un privado, es complicado. No sabemos todavía nada del protocolo, creemos que se pedirán las dos dosis", manifestó Ameal en ESPN.

Y en esa línea coincidió Russo: "Lo del PCR no veo muy fácil para implementarlo en un partido común. Una cosa es el público que va a ver a la Selección que no es el habitual”.

De hecho, en la misma charla con CNN radio, Russo pidió "esperar un poco más" para la vuelta de los hinchas argentinos a los encuentros del fútbol local.

Todo esto se da en el marco de la baja consecutiva por 11 semanas de los casos de coronavirus en la Argentina y el avance de las segundas dosis de vacunación en mayores de 50 años -se pasó el objetivo del 60 por ciento trazado para este mes-.

**Definen el protocolo**

Sin embargo, la propia Vizzotti en diálogo con TyC Sports adelantó que "todavía" no está definido el pedido de la prueba de PCR negativo para entrar a los estadios, como tampoco el certificado de vacunación.

"Lo que anunciamos con Lammens fue la decisión de empezar a trabajar para que eso suceda. Estamos en una situación epidemiológica buena y esto es una prueba piloto. Lammens está trabajando con la AFA para presentar una propuesta y nosotros estamos trabajando con los equipos de salud para definir", apuntó la funcionaria, que este lunes regresó de Moscú, donde se reunión con el Fondo de Inversión Ruso para hablar sobre las diferentes intercambiabilidades de vacunas y sus porcentajes de efectividad.

Y de hecho, al momento de aclarar el protocolo, al igual que lo hizo este miércoles Inés Arrondo -Secretaria de Deportes de la Nación-, dijo que "no está definido".

**Con aforo, volverá el público a los estadios en el partido entre Argentina y Bolivia**

"No está definido si se va a pedir una prueba PCR. Sí por supuesto se va a hacer con un aforo del 30%, eso está definido. En un ámbito de aire libre con una distancia importante, el riesgo es bajo en el contexto epidemiológico en el que estamos", explicó.

"En cuanto tengamos la definición para informarle a la población y cómo se va a implementar esto, con la AFA y River, que es donde se va a jugar el partido, se comunicará. Todavía no están definidos exactamente los requisitos y estamos trabajando en eso, pero como es el 9 de septiembre, se anunció para empezar a trabajar en el protocolo", continuó la Ministra de Salud.

El último partido con público en la Argentina se jugó el 10 de marzo de 2020 en la Bombonera, por la segunda fecha de la fase de grupos de la Copa Libertadores de América.

En esa ocasión, Boca Juniors le ganó 3 a 0 a Independiente Medellín de Colombia con dos goles de Eduardo "Toto" Salvio y uno de Emanuel "Bebelo" Reynoso.

**Prueba piloto**

La prueba piloto se hará el 9 de septiembre en el encuentro entre Argentina y Bolivia, por la décima fecha de las Eliminatorias Sudamericanas rumbo al Mundial de Qatar 2022, con un 30 por ciento de aforo, en la primera presentación del seleccionado argentino en el país tras el título de la Copa América logrado en Brasil.

En este encuentro se deberán dejar "dos o tres asientos libres entre las personas", al tiempo que Vizzotti remarcó que el "rol individual será clave" para avanzar con el plan de retorno a los espectáculos masivos.

"No deberán abrazarse, ni acercarse en un gol, y el barbijo deberán usarlo todo el tiempo", concluyó.