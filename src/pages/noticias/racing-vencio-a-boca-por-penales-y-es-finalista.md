---
category: Deportes
date: 2021-06-01T07:34:33-03:00
thumbnail: https://assets.3dnoticias.com.ar/racing-finaljpg.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Diario Uno
resumen: Racing venció a Boca por penales y es finalista
title: Racing venció a Boca por penales y es finalista
entradilla: Tras igualar 0-0 en los 90', Racing superó por penales 4-2 a Boca y jugará
  la final de la Copa de la Liga Profesional en San Juan

---
Tras igualar 0-0 en los 90' reglamentarios, Racing fue más efectivo y se impuso a Boca por 4-2 en definición por penales y se metió en al final de la Copa de la Liga Profesional del próximo viernes, a las 19, en el estadio Bicentenario de San Juan.

El primer tiempo fue un trabado duelo táctico en el que los únicos que ganaron fueron los arqueros, ya que tuvieron muy poco trabajo. Tampoco ayudaba el campo de juego, con el césped muy seco, lo que hacía más lento el juego.

La más clara de la etapa fue para Sebastián Villa, quien volvía a ser el siempre desequilibrante que suele tener tendencia a tomar malas decisiones en los últimos metros. Gastón Gómez le tapó un mano a mano a los 33 minutos.

Si la primera etapa fue mala, la segunda fue aún peor, porque hasta los entrenadores colaboraron con modificaciones pensadas más en sostener el cero que en ganarlo. Sobre el final, Gómez sacó sin grandes problemas los últimos tibios intentos de Boca para ir hacia los inevitables penales.

Desde los doce pasos, la puntería estuvo del lado de Racing. Más allá de la ejecución que Agustín Rossi le tapó a Fabricio Domínguez, el disparo al travesaño de Carlos Tevez y el penal que Gómez le atajó a Diego González le dieron la victoria a la Academia, que lo definió con un remate de Enzo Copetti.

La síntesis del partido de la Copa de la Liga Profesional:

**Racing**: Gastón Gómez; Iván Pillud, Leonardo Sigali, Nery Domínguez, Lucas Orban; Ignacio Piatti, Leonel Miranda, Aníbal Moreno, Tomás Chancalay; Enzo Copetti y Darío Cvitanich. Director técnico: Juan Antonio Pizzi.

**Boca**: Agustín Rossi; Nicolás Capaldo, Carlos Zambrano, Lisandro López, Carlos Izquierdoz, Frank Fabra; Jorman Campuzano, Alan Varela; Edwin Cardona; Sebastián Villa y Carlos Tevez. Director técnico: Miguel Ángel Russo.

**Definición por penales:**

Para Racing convirtieron Rojas, Melgarejo, Chancalay y Copetti, y falló Domínguez.

Para Boca convirtieron Villa y Pavón, y fallaron Tevez y González.

**Cambios en el segundo tiempo:** en el inicio, Juan José Cáceres por Pillud (R); a los 12 minutos, Fabricio Domínguez por Cvitanich (R); a los 22, Diego González y Gonzalo Maroni por Campuzano y Cardona (B); a los 28, Facundo Gutiérrez por Piatti (R); a los 47, Matías Rojas y Lorenzo Melgarejo por Moreno y Orban (R); a los 48, Cristian Pavón por Capaldo (B).

**Árbitro:** Darío Herrera.

**Estadio:** San Juan del Bicentenario (semifinal Copa de la Liga Profesional).