---
category: Agenda Ciudadana
date: 2021-04-30T12:18:24Z
thumbnail: https://assets.3dnoticias.com.ar/escuelas.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Padres Organizados
resumen: De 15 mil alumnos encuestados, solo se contagiaron 50
title: De 15 mil alumnos encuestados, solo se contagiaron 50
entradilla: "Se trata de un relevamiento realizado por la filial santafesina de la
  Sociedad Argentina de Pediatría y Padres Organizados. En ninguna de las escuelas
  hubo brote de contagio.\n\n"

---
Clases presenciales sí, clases presenciales no. El dilema que se arrastra desde el año pasado cuando comenzó la circulación del coronavirus en Argentina e impidió que durante 2020 haya presencialidad en las escuelas se reavivó en el inicio del actual ciclo lectivo, y aún más luego del conflicto entre Nación y CABA por la suspensión de la asistencia dentro de las recientes restricciones.

Para tener datos certeros sobre los contagios en los establecimientos escolares y la implicancia de la presencialidad en el aumento de casos, desde la Sociedad Argentina de Pediatría filial Santa Fe y Padres Organizados encararon un relevamiento en escuelas de Santa Fe y Santo Tomé.

La encuesta, que era voluntaria y anónima, contiene datos del primer mes de clases, y fue respondida por estudiantes y docentes, detalló a LT10 María Elena Malachevsky pediatra e integrante de la entidad.

Contestaron 15 mil alumnos entre ambas ciudades y solo 50 se contagiaron de coronavirus, es decir, el 0,36% de la muestra.  De esos positivos el 80% corresponden a estudiantes del nivel secundario.

"Los menores de 10 años tienen menos probabilidades de contagiarse y transmitir la enfermedad, según la bibliografía científica publicada", dijo la pediatra, quien agregó que "los adolescentes se comportan más como la población adulta respecto al contagio y tienen más vida social que los niños".

Malachevsky precisó que de los docentes que participaron del sondeo los contagios fueron del 1,9% "pero no se puede decir que se contagiaron en las escuelas porque no hubo brotes dentro de los establecimientos educativos".

"Estos datos son concretos, es una estadística que sirve de soporte y de ayuda para los funcionarios que tienen que tomar decisiones. Los casos en las escuelas siempre aumentan cuando aumentan en la comunidad y no al revés", concluyó la médica. Por lo cual remarcó que  "la sociedad tiene que cuidarse para no afectar la presencialidad de los niños que sí cumplen muy bien los protocolos dentro de las esuelas".