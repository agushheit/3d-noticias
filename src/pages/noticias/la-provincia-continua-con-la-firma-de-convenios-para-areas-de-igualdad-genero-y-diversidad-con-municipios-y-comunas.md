---
category: Estado Real
date: 2021-09-01T06:00:44-03:00
thumbnail: https://assets.3dnoticias.com.ar/GENERO.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La provincia continúa con la firma de convenios para áreas de igualdad, género
  y diversidad con municipios y comunas
title: La provincia continúa con la firma de convenios para áreas de igualdad, género
  y diversidad con municipios y comunas
entradilla: La actividad tuvo lugar en Rosario, con la presencia de la ministra de
  Igualdad, Género y Diversidad, Celia Arena, y de intendentes/as y presidentes/as
  comunales.

---
El Gobierno de Santa Fe, a través del Ministerio de Igualdad, Género y Diversidad, suscribió convenios de fortalecimiento y/o creación de áreas locales de igualdad, género y diversidad con localidades del departamento Rosario.

La actividad tuvo lugar en el patio de la Sede de Gobernación y, posteriormente, se realizó un taller con áreas locales en la Sala Rodolfo Walsh. Se firmaron convenios con Acebal, Alvear, Coronel Bogado, Coronel Domínguez, Fighiera, Granadero Baigorria, Ibarlucea, Pérez, Pueblo Esther, Soldini, Villa Amelia, Villa Gdor Gálvez y Zavalla.

"Firmar y llevar adelante esta articulación, que hoy se formaliza en la firma, es muy importante porque también es comenzar a trabajar con un criterio diferente. Nosotros a principio de año invitamos a cada municipio y comuna de los 365 de toda la provincia a que nos presenten su proyecto institucional, porque lo que queremos es construir esa política de género y diversidad, en conjunto con ese diálogo multinivel al que incorporamos también a la nación. Tenemos un trabajo muy fluido con el Ministerio de Mujeres de la Nación y con el resto de los ministerios que tienen incidencia. En esta firma de convenio estamos haciendo una transferencia económica para apoyar eso que cada gobierno local nos planteó. Además, estamos trabajando con cada equipo de género de cada municipio y comuna para hacer un taller, que tiene que ver justamente con eso, con cómo vamos abordando e integrando el trabajo de los equipos”, expresó la ministra de Igualdad, Género y Diversidad, Celia Arena.

En tanto, en representación de las autoridades locales estuvieron presentes, los/as presidentes/as comunales Daniel Siliano (Acebal), Leandro Tavares (Alvear), Roberto Delorenzi (Coronel Bogado), Mónica Villegas (Coronel Dominguez), Rodolfo Stangoni (Fighera), Jorge Masson (Ibarlucea), Javier Ruggeri (Villa Amelia), Guillermo Adrian Rajmil (Zavalla), Gustavo Piccirilli (Miguel Torres); y los intendentes Adrián Maglia (Granadero Baigorria), Pablo Corsalini (Pérez), Martín Gherardi (Pueblo Esther), Alejandro Luciani (Soldini), Alberto Ricci (Villa Gdor Gálvez).

**Sobre los convenios**

El Ministerio lleva adelante el programa de Fortalecimiento y creación de áreas de Igualdad, Género y Diversidad para que municipios y comunas desarrollen un proyecto institucional de políticas vinculadas a la promoción de derechos; la prevención y atención en violencias por motivos de género; la diversidad sexual; y los cuidados, de acuerdo a las necesidades de cada territorio, impulsando las visiones de los gobiernos locales.

A comienzos de este año se convocó a participar de este programa a los 365 municipios y comunas de la provincia. Esto implica la recepción de recursos técnicos y económicos para trabajar políticas concretas, adaptadas a cada territorio, que aporten a la construcción de sociedades más igualitarias.