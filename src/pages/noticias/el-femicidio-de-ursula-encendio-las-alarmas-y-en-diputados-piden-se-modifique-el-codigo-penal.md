---
category: Agenda Ciudadana
date: 2021-02-13T06:00:39-03:00
thumbnail: https://assets.3dnoticias.com.ar/URSULA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: El femicidio de Úrsula encendió las alarmas y en Diputados piden se modifique
  el Código Penal
title: El femicidio de Úrsula encendió las alarmas y en Diputados piden se modifique
  el Código Penal
entradilla: El femicidio de Úrsula Bahillo, una chica de 18 años que recibió al menos
  15 puñaladas por parte de su ex novio, un funcionario de la policía bonaerense,
  encendió las alarmas en la sociedad y la Justicia.

---
El femicidio de Úrsula Bahillo, una chica de 18 años que recibió al menos 15 puñaladas por parte de su ex novio, un funcionario de la policía bonaerense, encendió las alarmas en la sociedad, la Justicia y, sobre todo, dentro de la dirigencia oficialista, que incluso ha admitido que la política de género implementada no es suficiente para proteger a las víctimas de violencia. En ese sentido, al Congreso ingresó un proyecto de ley para que se modifique el Código Penal.

La iniciativa la presentó la diputada nacional por Entre Ríos Carolina Gaillard (Frente de Todos) y no solo toca a los ciudadanos comunes sino también a funcionarios.

En los hechos, propone modificar el Código Penal y agravar las penas para cuando un agresor incumpla con la restricción perimetral impuesta en una causa por violencia de género y cuando un funcionario demore la disposición de las medidas.

«Buscamos llegar antes de que el femicidio ocurra y salvaguardar la vida de la mujer: un minuto es un montón cuando se está siendo acosada y amenazada», dijo la diputada, que también es presidenta de la Comisión de Legislación Penal de la Cámara baja.

El proyecto al que adhirieron otros 40 diputadas y diputados nacionales, busca «resolver la dilación en las causas que se tramitan por denuncias por violencia de género» y establecer «celeridad en la respuesta ante el incumplimiento de medidas de protección».

La norma modifica el Código Penal y la ley 26.485 de ‘Protección Integral para Prevenir, Sancionar y Erradicar la Violencia contra las Mujeres’ para «agravar también la pena a los jueces y autoridades que incumplan sus funciones».

Si el agresor incumple la restricción perimetral y el juez o funcionario «no dispone la detención del agresor y ocurriere el femicidio, es una falta grave», apuntó Gaillard.

Además, el proyecto dice que el funcionario judicial que en denuncias por violencia de género o familiar demore maliciosamente la adopción de medidas de protección de la víctima será reprimido con uno a seis años de prisión.