---
category: La Ciudad
date: 2021-06-20T06:15:25-03:00
thumbnail: https://assets.3dnoticias.com.ar/Detectar.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: DetectAR en la ciudad, para realizar testeos sin turno
title: DetectAR en la ciudad, para realizar testeos sin turno
entradilla: 'El DetectAR volvió a la ciudad de Santa Fe y se quedará hasta el miércoles,
  sumando a los cuatro puntos fijos de testeos en distintos barrios. '

---
El camión del plan nacional DetectAR volvió este sábado a la ciudad de Santa Fe después de haber recorrido otras localidades de la región, realizando testeos rápidos para la prevención y la rápida detección y posibilidad de aislamiento ante posibles casos de coronavirus.

El operativo se desarrolla en los siguientes lugares y días en la ciudad

\-el sábado Colastiné, en Ruta 1 y calle Los Eucaliptos

\-el domingo 20 de junio estará en Alto verde, en Bustamante y Galarza

\-el lunes 21 en El Alero de Acería, en Cafferata y Roca

\-el martes 22 en Altos de Noguera, en Malaver (entre Juan Díaz de Solís y Lamadrid)

\-el miércoles 23 en el barrio El Pozo, en la plaza Laureano Maradona.

Además, en la ciudad hay distintos centros de testeos territoriales fijos, que funcionan todos los días de 9 a 13, sin necesidad de turno previo. Los mismos están ubicados en El Alero (French 1701); en la Estación Belgrano (Bulevar Gálvez y Dorrego); en Estación Mitre (Doctor Zavalla 1586); en la Costanera (Almirante Brown 5294 - Centro Deportivo Municipal).

Al lugar pueden acercarse quienes tengan síntomas, hayan sido contacto estrecho de algún caso positivo o sospechen que poseen la enfermedad. Se les realizará un test rápido de antígenos y si se es persona de los grupos de riesgo se les realizará además un PCR cuyo resultado les será informado a la brevedad, teniendo que permanecer aislados hasta ello.