---
category: Agenda Ciudadana
date: 2021-03-22T06:00:51-03:00
thumbnail: https://assets.3dnoticias.com.ar/ciberestafa.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Las recomendaciones del Banco Central para operar con seguridad y evitar
  estafas bancarias
title: Las recomendaciones del Banco Central para operar con seguridad y evitar estafas
  bancarias
entradilla: Las estafas van desde ofertas a promociones exclusivas y soluciones a
  inconvenientes operativos, beneficios con el objetivo de pedir contraseñas con las
  que roban el dinero depositado o solicitan préstamos inmediatos.

---
El Banco Central (BCRA) difundió una serie de recomendaciones para operar con seguridad y evitar las estafas bancarias, una forma de delito que creció exponencialmente durante los últimos años, y que se practica a través de internet, de llamados telefónicos o de mensajes de texto.

En ese sentido, los estafadores atraen a clientes bancarios con supuestas ofertas que van desde promociones exclusivas y soluciones a inconvenientes operativos, hasta regalos o beneficios con el objetivo de pedir contraseñas con las que roban el dinero depositado o solicitan préstamos inmediatos.

Por ese motivo, el BCRA dio una serie de recomendaciones para evitar caer en estas estafas.

La primera de ella fue que, si se recibe un aviso sobre un supuesto error al realizar una transferencia bancaria, no se debe responder a estos mensajes y, ante cualquier duda, se debe comunicar telefónicamente con el banco".

"Tiene que ser el cliente el que llame y nunca aceptar una llamada supuestamente originada en la entidad. Es una nueva práctica ilegal detectada que simula un error en una transferencia bancaria por la compra de un bien ofrecido", indicó la entidad monetaria.

También señaló que nunca se debe acudir a un cajero automático, abrir la app o acceder al home banking cuando se recibe una llamada supuestamente proveniente de la entidad bancaria y que el cliente debe ser el que origina la llamada.

En esa línea, pidió no brindar datos personales (usuarios, claves, contraseñas, pin, Clave de la Seguridad Social, Clave Token, DNI original o fotocopia, foto, ni ningún tipo de dato) por teléfono, correo electrónico, red social, WhatsApp o mensaje de texto.

Otra de las recomendaciones fue no ingresar datos personales en sitios utilizando enlaces que llegan por correo electrónico, ya que podrían ser fraudulentos, y tener cuidado con los enlaces sospechosos y asegurarse siempre de estar en la página legítima antes de ingresar información de inicio de sesión.

"Utilizar contraseñas fuertes mezclando mayúsculas, minúsculas y números", apuntó el BCRA, además de "no usar la misma clave para distintas aplicaciones, cuentas, plataformas o sitios".

Además, recomendó "no usar equipos públicos o de terceras personas para acceder a aplicaciones, redes sociales o cuentas personales", así como tampoco usar redes de wi-fi públicas para acceder a sitios que requieran contraseñas.

En cuanto al uso de Internet y dispositivos recomendó "mantener actualizado el navegador, el sistema operativo de los equipos y las aplicaciones" además de "aprender a diferenciar un perfil verdadero de uno falso en redes sociales".

En caso de detectar un fraude virtual o engaño recordó que es posible comunicarse con la Unidad Fiscal Especializada en Ciberdelincuencia (UFECI) por teléfono (5071-0040/0041) o correo electrónico a través de: denunciasufeci@mpf.gov.ar