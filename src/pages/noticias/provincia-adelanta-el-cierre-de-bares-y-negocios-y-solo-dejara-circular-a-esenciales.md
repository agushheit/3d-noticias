---
category: Estado Real
date: 2021-05-19T09:11:24-03:00
thumbnail: https://assets.3dnoticias.com.ar/0001.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Provincia adelanta el cierre de bares y negocios y sólo dejará circular a
  esenciales
title: Provincia adelanta el cierre de bares y negocios y sólo dejará circular a esenciales
entradilla: Será desde este jueves. Los locales comerciales estarán abiertos hasta
  las 17 y los gastronómicos hasta las 19

---
Desde las cero horas de este jueves y hasta el 31 de mayo próximo la provincia restringirá la circulación vehicular, salvo para los trabajadores esenciales. Además, adelantará el horario de cierre de los comercios y bares. De esa manera, el gobierno intentará lograr un drástico descenso en los contagios de casos de Covid para poder salir de la situación apremiante en la que se encuentra el sistema de salud.

A modo de adelanto, desde este jueves solo podrán transitar personas consideradas trabajadores esenciales en vehículos particulares. Sí se permite la circulación del resto de la población en transporte público. La restricción se extiende a lo largo de las 24 horas.

En tanto, se dispuso que los negocios cerrarán sus puertas a las 17 mientras que los bares y restaurantes lo harán a las 19.

El gobernador Omar Perotti no hizo anuncio televisivo sobre las nuevas medidas sanitarias dispuestas en el marco del colapso sanitario. Durante la jornada ya se dio a conocer la suspensión de las clases presenciales en todos los niveles educativos.

Este martes a la tarde mantenía conversaciones con intendentes para terminar de definir las restricciones para los departamentos más afectados por la segunda ola de coronavirus.

Como queda el nuevo panorama:

![](https://assets.3dnoticias.com.ar/0002.jpg)

![](https://assets.3dnoticias.com.ar/0003.jpg)

![](https://assets.3dnoticias.com.ar/0004.jpg)

![](https://assets.3dnoticias.com.ar/0005.jpg)

![](https://assets.3dnoticias.com.ar/0006.jpg)

![](https://assets.3dnoticias.com.ar/0007.jpg)