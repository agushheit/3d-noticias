---
category: Agenda Ciudadana
date: 2021-08-15T06:15:46-03:00
thumbnail: https://assets.3dnoticias.com.ar/IVE.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Cuáles son las pautas para aplicar la ley IVE reglamentada por el Gobierno
title: Cuáles son las pautas para aplicar la ley IVE reglamentada por el Gobierno
entradilla: El decreto 516/2021, publicado este sábado en el Boletín Oficial, reglamenta
  la ley 27610 sancionada el 30 de diciembre de 2020 y promulgada el 14 de enero de
  este año.

---
La reglamentación de la ley de interrupción voluntaria del embarazo (IVE) que se publicó en el Boletín Oficial reafirma que no es necesaria autorización judicial para acceder a la práctica, explica que la objeción de conciencia no puede aplicarse en caso de emergencia y fija una serie de pautas que garantizan la aplicación de la norma en todo el territorio argentino.

El decreto 516/2021 publicado este sábado en el BO reglamenta la ley IVE 27610, sancionada el 30 de diciembre de 2020, y promulgada el 14 de enero de este año.

En tanto, el 28 de mayo último se publicó en el Boletín el Protocolo para la Atención Integral de las personas con derecho a la Interrupción Voluntaria y Legal del Embarazo adecuado a la normativa.

Con estos antecedentes, sólo estaba pendiente la reglamentación que respalda el Protocolo y establece "la responsabilidad institucional" para garantizar el acceso a la IVE hasta la semana 14 de gestación, y a la Interrupción legal del embarazo (ILE) por causales: peligro para la vida, para la salud de la mujer o violación.

De esta manera se garantiza el derecho a las mujeres y personas con otras identidades de género con capacidad de gestar en todo el territorio de la Argentina "en condiciones de igualdad".

Por lo tanto, el sector público de la salud, las obras sociales, las entidades de medicina prepaga "tienen responsabilidades y obligaciones" para garantizar el acceso a la interrupción del embarazo y la atención posaborto.

Para cumplirlas, deben asegurar los recursos, mecanismos y personal de salud adecuado para su realización.

Hasta la semana 14 inclusive del proceso gestacional se puede acceder a la IVE con el requisito de prestar un consentimiento informado.

En un documento oficial al que tuvo acceso Télam se detallan los ejes del decreto reglamentario, que reafirma que todas las mujeres y personas con capacidad de gestar tienen derecho a interrumpir el embarazo sin necesidad de autorización judicial.

Se aclara que hasta la semana 14 inclusive del proceso gestacional se puede acceder a la IVE con el requisito de prestar un consentimiento informado que debe tener un formato accesible y estar disponible en braille, digital, audio "y cualquiera que resulte adecuado" a la persona solicitante.

Ante situaciones de violencia sexual se requerirá, además del consentimiento informado, una declaración jurada de las personas mayores de 13 años, y antes de esa edad no se debe exigir ningún documento adicional ni denuncia.

En casos de peligro para la vida o la salud de la mujer o persona gestante, se requerirá el consentimiento informado y la constancia de la causal en la historia clínica.

También "se robustece" el deber de respetar el derecho a la confidencialidad por parte de todo el personal sanitario, y se aclara que no podrá entregarse información clínica, salvo que exista orden judicial expresa que releve de este deber en una causa judicial.

Sobre el derecho a la autonomía de la libertad fija que "en ningún caso" el personal de salud "puede obstaculizar, presionar o interferir engañosamente" en la decisión de las personas gestantes.

Y contempla que, ante situaciones de violencia de género, el personal de salud deberá entregar información sobre sus derechos a las víctimas, como también sobre los recursos de atención y canales para realizar una denuncia penal y contar con asesoramiento.

Todas las mujeres y personas con capacidad de gestar tienen derecho a interrumpir el embarazo sin necesidad de autorización judicial.

En cuanto al acceso a la interrupción del embarazo de personas con capacidad restringida la reglamentación señala que "estas limitaciones son excepcionales y expresamente establecidas".

Cuando la persona hubiera sido declarada incapaz judicialmente para este tipo de actos deberá implementar el sistema de apoyo que haya sido designado judicialmente, o a falta o ausencia de esta, la de una persona allegada, en los términos del artículo 59 del Código Civil y Comercial de la Nación, detalla el decreto.

Al referirse a la objeción de conciencia deja en claro que el personal de salud "no podrá negarse" a la realización de la interrupción del embarazo en caso de emergencia, cuando la práctica deba realizarse en forma urgente "pues su no realización inmediata pondría en riesgo la salud física o la vida" de la persona gestante.

Asimismo, el reglamento de la ley IVE indica que el Ministerio de Salud de la Nación y los ministerios provinciales y de la Ciudad Autónoma de Buenos Aires deberán definir indicadores "que permitan evaluar la consecución" de los programas de capacitación.