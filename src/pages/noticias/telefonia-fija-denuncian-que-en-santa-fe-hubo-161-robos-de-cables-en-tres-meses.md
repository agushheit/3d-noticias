---
category: La Ciudad
date: 2021-03-18T07:38:51-03:00
thumbnail: https://assets.3dnoticias.com.ar/telefono.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Diario Uno
resumen: 'Telefonía fija: denuncian que en Santa Fe hubo 161 robos de cables en tres
  meses'
title: 'Telefonía fija: denuncian que en Santa Fe hubo 161 robos de cables en tres
  meses'
entradilla: El fenómeno afecta a cientos de usuarios de esta ciudad que se quedaron
  sin servicio desde hace meses y que todavía aguardan la reposición del mismo.

---
En la ciudad de Santa Fe, en los últimos tres meses, se registraron 161 robos de cables de cobre. La denuncia es realizada por Telecom, empresa que brinda el servicio de telefonía fija que se ve severamente afectada por este delito.

Las denuncias de usuarios se multiplican en diversos barrios de la ciudad. Este martes, funcionarios de la municipalidad de Santa Fe se reunieron con representantes de la compañía, quienes les presentaron un plan de acción tendientes a restablecer la telefonía fija en barrios que, desde hace meses, permanecen sin servicio.

Franco Ponce de León, director de Derechos y Vinculación Ciudadana de la capital provincial, señaló que la empresa manifestó que "incrementaron las cuadrillas y operarios, a través de tercerización, para ir solucionando estos problemas".

"Hicimos entrega de los mas de 150 reclamos que tenemos en la oficinas, más allá de que ya fueron notificados formamente a través de las distintas acciones administrativas", subrayó.

Señaló además que lograron "generar un mecanismo para agilizar los reclamos que vayamos tomando por este tema a través de la oficina de defensa del consumidor".

Telecom informó que en las próximas semanas "estará trabajando, especialmente, en los barrios Villa Hipódromo, Barranquitas y Coronel Dorrego".

Desde la compañía destacaron que "han realizado las denuncias pertinentes que se están tramitando en sede judicial. La empresa está trabajando en conjunto con el Ministerio Publico de la Acusación y el Ministerio de Seguridad provincial quienes han puesto en marcha sus mecanismos para atender este inconveniente".

A principios de enero, Telecom comunicó que durante el primer semestre del año pasado se registraron "a nivel nacional, robos de más de 12.000 metros de cable-fibra y 501 baterías". En ese momento, la empresa informó que había "4.600 clientes afectados" y que a nivel nacional, "mensualmente más de 5000 clientes promedio, han visto afectadas por esta situación".