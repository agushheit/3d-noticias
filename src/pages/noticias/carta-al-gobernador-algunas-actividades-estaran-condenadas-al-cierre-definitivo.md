---
category: Agenda Ciudadana
date: 2021-04-15T06:49:15-03:00
thumbnail: https://assets.3dnoticias.com.ar/fececo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa FeCeCo
resumen: 'Carta al gobernador: "Algunas actividades estarán condenadas al cierre definitivo"'
title: 'Carta al gobernador: "Algunas actividades estarán condenadas al cierre definitivo"'
entradilla: Así se titula la carta dirigida al gobernador Perotti de la Federación
  de Cámaras del Comercio y otras actividades de la provincia (Fececo)

---
La Federación de Cámaras del Comercio y otras actividades de la provincia (Fececo) envió una carta al gobernador Omar Perotti a efectos de manifestarle al mandatario la “gran preocupación por la situación de todos aquellos sectores que aún se encuentran con sus actividades suspendidas o restringidas”.

“La reducción del coeficiente máximo de ocupación de las superficies cerradas en diferentes establecimientos que oportunamente fueron habilitados con sus correspondientes protocolos, tiene un fuerte impacto negativo en los mismos debido a la importante disminución de asistentes permitidos, que se agrega a la inconveniencia de utilizar los espacios abiertos por las bajas temperaturas de las jornadas que se avecinan”, dice el texto.

En el caso de aquellas actividades que nuevamente deben suspender sus actividades “sufren el golpe de gracia que seguramente los condenará al cierre definitivo”, se advierte desde Fececo.

“En ambas situaciones los costos fijos como alquiler, servicios, impuestos, personal, mantenimiento del local, como así también los compromisos con proveedores, entidades financieras deberán afrontarse como si estuvieran en actividad plena”.

“El Estado, sea de la jurisdicción que fuese, continuará exigiendo el cumplimiento de todas las obligaciones como si los ingresos con los cuales deben afrontarse pudiesen generarse con actividad reducida o nula. Los funcionarios del sector público, a diferencia del privado, no tienen estas dificultades, a fin de cada mes reciben sus sueldos a pesar de las angustiantes circunstancias de la sociedad. El esfuerzo de funcionarios y Estado han sido prácticamente nulos. La pesada mochila de las consecuencias de la cuarentena eterna no la cargan todos”.

Solicitamos, asimismo, que se arbitren medidas que equiparen el esfuerzo de los ciudadanos y contribuyentes, compartiendo con el Estado el costo de esta situación. “Sería equitativo y justo que aquellos ciudadanos y contribuyentes que estén imposibilitados de desarrollar actividad económica plena reciban una disminución equitativa en sus obligaciones impositivas, tarifas de servicios, contribuciones patronales y demás. Por ejemplo aquellos que solamente pueden trabajar al 30% de su capacidad, que abonen el 30% de sus compromisos y aquellos que no pueden trabajar, queden eximidos del pago respectivo.

“Si los ingresos públicos disminuyen y el Estado prevé no contar con los fondos suficientes deberá disminuir sus gastos y racionalizar sus recursos como desde hace más de un año lo hacen las familias y el sector productivo”, concluye la entidad mediante un comunicado que lleva las firmas del presidente, Eduardo Taborda, y el secretario, Adrián Schuck.

Lee la nota presentada [acá](https://fececo.org.ar/wp-content/uploads/2021/01/Perotti-Equidadporaforos-20210412.pdf)