---
category: La Ciudad
date: 2021-02-05T06:40:56-03:00
thumbnail: https://assets.3dnoticias.com.ar/jardines.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad de Santa Fe
resumen: 'Jardines Municipales: se ultiman detalles para la vuelta a clases'
title: 'Jardines Municipales: se ultiman detalles para la vuelta a clases'
entradilla: Esta semana culminan las entrevistas con los familiares para confirmar
  la continuidad de los alumnos. En tanto, las preinscripciones comenzarán el 8 de
  febrero, de 9 a 11 y de 14 a 16.

---
El intendente Emilio Jatón visitó esta mañana las instalaciones de uno de los 17 jardines municipales donde se llevan adelante las reinscripciones para el ciclo lectivo 2021. En este caso, el mandatario recorrió la sede del Jardín Roca, emplazado en el barrio 29 de Abril. En ese marco, se realizan entrevistas con los familiares de las y los alumnos que cursaron anteriormente para saber si continuarán asistiendo este ciclo.

Huaira Basaber, subsecretaria de Gestión Cultural y Educativa de la Municipalidad, detalló cómo será el proceso de  inscripciones a los jardines: “Hoy estuvimos en el Jardín Roca junto al intendente determinando cómo será la apertura de los establecimientos en función de los protocolos. En ese marco, conversamos con los docentes y las familias de los alumnos que se están reinscribiendo”.

Asimismo, Basaber detalló que en esta primera instancia “les preguntamos a las familias si van a comenzar el ciclo lectivo. Este proceso lo iniciamos en diciembre  y estamos culminado esta semana”.

**Preinscripciones**

“En tanto, a partir del próximo lunes 8, comenzarán las preinscripciones”, anticipó la funcionaria de la cartera de educación. En ese sentido, los horarios de preinscripción serán de 9 a 11 y de 14 a 16, en cada uno de los jardines. “Aquellas familias que quieran preinscribir a sus hijos e hijas lo pueden hacer. Luego se realizará una entrevista y un trabajo socioeconómico para determinar cuáles serán los ingresantes”, indicó Basaber.

Con respecto al cupo disponible, la funcionaria consignó: “El cupo será el de siempre, pero teniendo en cuenta las normativas vigentes por la pandemia, que implica desdoblar los grupos”.

**Volver a clases**

Luego de conversar con el intendente y las docentes, Fernanda Acevedo, mamá de uno de los alumnos del jardín Roca indicó: “Recién tuve una entrevista para la reinscripción. Además me explicaron todos los protocolos que se van a utilizar para la vuelta a clases. Estoy muy conforme con los protocolos. Ya me adelantaron que el lunes 22 empiezan las clases, con sala reducida y con horarios progresivos”.

En consonancia, Fernanda comentó: “Mi hijo viene desde los 11 meses, ahora vinimos para reincorporarlo para que siga el camino de la educación. Está bueno que vuelvan las clases. Sirve para que ellos se vuelvan más independientes, que compartan con otros chicos”.