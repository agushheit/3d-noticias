---
category: La Ciudad
date: 2020-12-02T10:55:18Z
thumbnail: https://assets.3dnoticias.com.ar/PLAYA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: 'La ciudad habilitó sus playas: habrá círculos sobre la arena'
title: 'La ciudad habilitó sus playas: habrá círculos sobre la arena'
entradilla: Es para garantizar que se cumpla con el distanciamiento social. Funcionarán
  solo como solarium, sin la posibilidad de bañarse en el río por la bajante y porque
  el agua no está en condiciones sanitarias. 

---
El municipio apuesta al compromiso de los ciudadanos. Los piletones de Parque del Sur y Parque Garay estarán listos el fin de semana.

"Hoy se dará la apertura formal, porque se sabe qué hace un tiempo largo hay 45 guardavidas trabajando, la gente es dueña de este lugar como debe ser", señaló el intendente Emilio Jatón durante una conferencia de prensa desarrollada en la Costanera Oeste, a la altura de la Dirección de Deportes, que sirvió para explicar cómo serán los protocolos a respetar para disfrutar de las playas de la ciudad.

Para cumplir con el protocolo de distanciamiento social, en las playas hay círculos demarcados sobre la arena para delimitar los sectores a ocupar por grupos reducidos de personas. Este año, en medio de una bajante extraordinaria y al estar el agua sin cumplir con los índices sanitarios para bañarse, los balnearios solo podrán usarse como solarium y no estará permitido ingresar al agua.

"Parece mentira, pero el 1° de diciembre en Santa Fe empieza una nueva normalidad, con verano, playas, espacios culturales, jardines maternales abiertos también con protocolo. Empieza una nueva etapa con el Covid-19 porque comienzan a abrirse un montón de situaciones que incluye el verano, pero depende de cómo nos adaptemos y convivamos con el virus", aseveró Jatón.

Además de los círculos dispuestos sobre la playa, cada parador concesionado deberá colocar sus reposeras a distancia para evitar las aglomeraciones. A lo largo y ancho de las playas también están ubicadas duchas para refrescarse y evitar el ingreso al agua.

## **Control y emergencias**

El municipio prevé que este verano será distinto y que por el Covid muchos santafesinos van a elegir la ciudad para pasar el verano, en consecuencia, las playas y demás espacios públicos recibirán miles de personas.

"Apostamos a la convivencia, por eso la presencia del Cobem, de la GSI, de 90 guardavidas, los profes, todo eso forma parte de un trabajo conjunto de las distintas áreas, porque sabemos y explicamos en distintos carteles cuáles son los síntomas de Covid. Habrá ambulancias previendo cualquier problema de salud; los agentes de la GSI no están para multar, sino para tratar de que se comprenda la situación. Apostamos al diálogo", sostuvo el intendente.

## **Piletones: el fin de semana**

Como informó el Gobierno Provincial, a partir del 5 de enero se habilitarán las piletas, colonias de vacaciones, entre otras actividades. En tal sentido, el Gobierno local ultima detalles para la habilitación de los piletones del Parque Garay y del Parque del Sur.

"Serán habilitados entre el viernes y sábado. En el Parque del Sur hay baños que no existían; hay paradores que no existían; hay limpieza e iluminación. Así que el fin de semana, si el tiempo nos acompaña, tendremos habilitados los piletones, también con protocolos que serán explicados el viernes", destacó el intendente.

El intendente Jatón presentó la temporada estival denominada "Verano Capital", junto a los secretarios Paulo Ricci (Cultura) y Mariano Granato (Integración y Economía Social).