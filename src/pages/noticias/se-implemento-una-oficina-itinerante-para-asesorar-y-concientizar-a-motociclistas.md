---
category: La Ciudad
date: 2021-01-23T08:20:39Z
thumbnail: https://assets.3dnoticias.com.ar/oficina_movil.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad de Santa Fe
resumen: Se implementó una oficina itinerante para asesorar y concientizar a motociclistas
title: Se implementó una oficina itinerante para asesorar y concientizar a motociclistas
entradilla: La Municipalidad puso en marcha una iniciativa que busca asesorar, generar
  un registro y promover el uso responsable de motovehículos.

---
¿Qué trámites hay que hacer para sacar una moto a la calle? ¿Cuáles son las medidas de seguridad requeridas? Desde esta mañana, una oficina móvil recorre la ciudad brindando respuestas a los conductores o futuros conductores de motos. La iniciativa de la Municipalidad tiene como objetivo asesorar, generar un registro y promover la concientización y capacitación sobre el uso de motovehículos.

Virginia Coudannes, secretaria de Control y Convivencia Ciudadana, indicó que esta mañana “ponemos en funcionamiento la Oficina Itinerante de Motovehículos destinada, principalmente, a quienes sacan la licencia por primera vez. Esta oficina recorrerá todos los espacios públicos de la ciudad y los edificios municipales”.

En consonancia, Coudannes indicó que la propuesta se extenderá a las redes de instituciones y las vecinales, para que las capacitaciones se puedan ampliar a todo el territorio. Por otra parte, la funcionaria indicó que “si bien los controles siempre son necesarios (ya realizamos más de 2.000 operativos de marzo a diciembre del 2020), hay que trabajar desde la prevención, la educación y seguridad vial, que es un eje transversal para la Municipalidad”.

La oficina itinerante recorrerá la ciudad tres veces a la semana de mañana y de tarde. También estará disponible los fines de semana que se realicen eventos al aire libre.

**Simplificar la atención**

La propuesta de la Secretaría de Control y Convivencia Ciudadana consiste en asistir a los ciudadanos y ciudadanas con la presencia itinerante de una oficina móvil que asesore en materia de documentación, disposición digital de material de concientización e información sobre trámites requirentes y gestión de turno para licencia. Asimismo, invitará a aquellos que se registren a una charla de formación y concientización para quienes usen o busquen usar un motovehículo y promover a su vez el uso de dispositivos de seguridad, otorgando a quien cumpla la jornada con un casco.

A través de esta interacción con la ciudadanía, se presenta la posibilidad de remarcar la importancia del uso de dispositivos de seguridad e iniciar el camino hacia la regularización en todo sentido de quienes son usuarios de motovehículos.

**Casco de regalo**

A manera de un compromiso para la conducción responsable, la Municipalidad obsequia un casco a quienes terminan todos los trámites para la primera licencia y se inscriben a una charla de sensibilización. “Recibir un casco fue una sorpresa. La verdad es que no me lo esperaba”, indicó María del Carmen. “La charla fue excelente, recibimos mucha información sobre las reglamentaciones y lo que pasa cuando no se cumplen. En ese sentido, hubo testimonios de familiares que perdieron a seres queridos en siniestros viales”.

Juan Manuel es uno de los vecinos que se acercó esta mañana a la oficina móvil emplazada, en este caso, en la Estación Belgrano. “Fue una experiencia muy buena, me asesoro por responsabilidad ya que tengo mi moto y realmente quiero andar en regla”, indicó, luego de interiorizarse sobre las normativas y escanear el código QR con toda la información.

Con respecto al curso de concientización que recibió, Juan Manuel dijo que “fue muy bueno, las inspectoras me dieron todas las recomendaciones y fue muy importante para los que andamos día a día en la calle”. En referencia al casco que recibió por haber realizado la charla y sacar la licencia por primera vez, el vecino añadió: “Ya tengo casco, pero lo sumo para el acompañante, siempre es bueno tener uno de más”.

**A quién está dirigido**

La propuesta de la Municipalidad está dirigida a personas que posean motovehículos con Licencia de Conducir vigente o vencida o por adquirir primera licencia. Además, a personas con licencia vigente que no posean el resto de documentación en regla y necesiten asesoramiento. Y a quienes se registren para recibir la charla de capacitación.

![](https://assets.3dnoticias.com.ar/curso_carnet.jpg)