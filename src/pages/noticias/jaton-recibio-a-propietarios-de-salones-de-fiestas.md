---
category: La Ciudad
date: 2021-09-08T06:00:19-03:00
thumbnail: https://assets.3dnoticias.com.ar/REUNIONSALONES.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Jatón recibió a propietarios de salones de fiestas
title: Jatón recibió a propietarios de salones de fiestas
entradilla: El intendente convocó a los empresarios del rubro para conversar sobre
  la compleja situación que atraviesan y buscar alternativas que les permitan volver
  próximamente a la actividad habitual.

---
Este martes, el intendente Emilio Jatón recibió a un grupo de propietarios de salones de fiesta para evaluar la realidad del sector que aún hoy no pudo retomar las actividades, en el marco de las medidas dispuestas por la pandemia. En la reunión, se brindó un panorama sobre la situación sanitaria de la capital de la provincia y se evaluó de qué forma, el rubro puede volver al trabajo.

En ese sentido, los empresarios se mostraron preocupados ante la imposibilidad de realizar fiestas sin espacios de baile, para lo cual elaboraron algunas alternativas. Una de las opciones es realizar una prueba piloto por 45 días durante los cuales se celebren cumpleaños durante el día, es decir, de 17 a 1 AM, tal como lo permiten las restricciones impuestas por la autoridad sanitaria provincial.

De este modo, se comprometen a cumplir con todos los protocolos, entre ellos, a que asistan solamente personas mayores vacunadas. Según expresan, son las mismas condiciones -e incluso más- en que se desarrolla la vida social en la ciudad. Desde la Municipalidad, en tanto, se ofreció generar una mesa de diálogo con reuniones periódicas en la cual abordar la situación y evaluar las distintas posibilidades.

El secretario de Producción y Desarrollo Económico municipal, Matías Schmüth, explicó que se busca “una propuesta que permita el desarrollo más normal de una fiesta”. Según mencionó, ambas partes trabajarán con ese objetivo, siempre teniendo en cuenta el respeto por todos los protocolos actuales o aquellos que puedan aplicarse en el futuro.

Schmüth insistió en que para la Municipalidad es preocupante la situación que atraviesa el rubro, ya que es uno de los que aún no pudo retomar sus actividades a pleno. En ese sentido, expresó que el Ejecutivo local se comprometió a acompañarlos en sus presentaciones ante la autoridad sanitaria provincial, que tiene a su cargo las decisiones.

Alejandro Caveggia, uno de los representantes de los empresarios que participaron del encuentro, se mostró “muy conforme con esta reunión” a la que calificó como “extremadamente positiva”. En ese sentido expresó que “si bien hace un año y medio estamos sin posibilidad de trabajar de nuestra actividad, siempre tuvimos un trato cordial y fuimos contenidos por la Municipalidad”.

Según informó Caveggia, este martes “nos pusimos de acuerdo en la visión que tenemos de la pandemia y la situación de nuestro trabajo. Tratamos de establecer lo que sucede en la ciudad, por ejemplo, en torno a la vacunación que por suerte está muy avanzada, y las habilitaciones de otros rubros que tienen posibilidad de trabajar con cierta normalidad”. Frente a ello, indicó que “el intendente quiere que volvamos al trabajo pero, obviamente, dentro de los parámetros y teniendo en cuenta que todavía estamos en pandemia”.

Del encuentro también participó el secretario general del municipio, Mariano Granato.