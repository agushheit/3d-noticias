---
category: La Ciudad
date: 2021-11-19T06:15:03-03:00
thumbnail: https://assets.3dnoticias.com.ar/taCHOS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Los taxistas piden que el precio inicial del viaje iguale al litro de nafta
  premium
title: Los taxistas piden que el precio inicial del viaje iguale al litro de nafta
  premium
entradilla: De ocurrir, el aumento en la "bajada de bandera" podría rondar el 40%.
  En un contexto de crisis del sector, con caída de choferes y menos coches circulando,
  señalan que una suba podría impactar favorablemente el servicio

---
Los taxistas enviaron una nota a la municipalidad solicitando una actualización tarifaria. El sector pide que el precio inicial del viaje, lo que se conoce como bajada de bandera, iguale al precio al litro de nafta premium, que en estaciones YPF de Santa Fe se consigue a $ 113,70.

De otorgarse la suba mencionada, el incremento sería superior al 40 por ciento, ya que el valor actual es de $80. El servicio suele tener dos aumentos anuales y este podría ser el segundo (el anterior se dio en mayo). Por otro lado, en 2019 -y por razones vinculadas a la pandemia- hubo una sola actualización anual.

El pedido fue realizado el viernes de la semana pasada al ejecutivo municipal. Desde la Sociedad de Taximetristas aguardan definiciones y una posible reunión con el municipio que se podría darse en los próximos días.

LEER MÁS: Taxis y remises: la caída de licencias llega casi del 20%

La solicitud llega en un momento particular. Es que el subsistema de transporte atraviesa una crisis marcada por diversos factores: la pandemia por coronavirus, una caída de coches circulando por la ciudad, una menor cantidad de choferes al frente del volante y vehículos que no se encuentran en buenas condiciones brindando servicio.

Damián Cóceres, presidente de la Sociedad Taximetristas Unidos de Santa Fe, le dijo a UNO que la ordenanza que regula a los taxis se rige por el precio de la nafta. "Nosotros estamos pidiendo un litro de nafta premium", afirmó.

"El último aumento fue en mayo. Nosotros hacemos dos subas por año, y en 2019 aumentamos una sola vez por la pandemia. O sea, venimos atrasados. También hay que sumarle el esfuerzo del 20 por ciento de descuento que se hizo por la pandemia", añadió.

"La realidad es que la nafta va a aumentar nuevamente en los próximos días y quedaremos atrasados. Siempre vamos detrás de los aumentos", reflexionó.

Dijo que una suba podría impactar favorablemente y mejorar la situación del sector. "Estamos necesitando choferes. Y la tarifa, como está, no le rinde al chofer. Creemos que es un incentivo, en el caso que la tarifa aumente, para conseguir choferes y mejorar el servicio", le dijo a este medio Cóceres.

"Estamos trabajando de todos lados como para poder brindar un mejor servicio. Esperamos una respuesta que salga a fines de este mes, para trabajar con la nueva tarifa a partir de diciembre. Sabemos además que enero y febrero son meses malos", finalizó.