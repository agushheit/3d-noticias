---
category: Agenda Ciudadana
date: 2021-06-23T09:19:24-03:00
thumbnail: https://assets.3dnoticias.com.ar/terreno.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Diario Uno
resumen: 'Santo Tomé: vuelve a resonar el malestar de vecinos de la zona de barrios
  cerrados'
title: 'Santo Tomé: vuelve a resonar el malestar de vecinos de la zona de barrios
  cerrados'
entradilla: Argumentan un "olvido total" por parte de la municipalidad en materia
  de servicios. "Estamos totalmente aislados de Santo Tomé", manifestaron los vecinos.

---
En el mes de febrero del año pasado, [**_UNO Santa Fe _**](https://www.unosantafe.com.ar/)puso de relieve una problemática vecinal y urbanística que vive la ciudad de [**Santo Tomé**](https://www.unosantafe.com.ar/santo-tome-a48279.html)

con el Distrito de Urbanizaciones Especiales (DUE), popularmente conocido como la zona de [**barrios cerrados**](https://www.unosantafe.com.ar/barrios-cerrados-a56366.html) y countries que se ubica a la vera de la autopista Santa Fe-Rosario. Por aquel entonces, debido a "la carencia de gestión comunitaria adecuada por parte del municipio santotomesino", los [**vecinos**](https://www.unosantafe.com.ar/vecinos-a22383.html) del sector pretendían independizarse y conformar una nueva localidad.

Finalmente, dicho proyecto no prosperó, pero tomó trascendencia en el debate social y político en relación a una zona poblacional en constante crecimiento demográfico, que **según estimaciones, tiene cerca de 15 mil personas viviendo actualmente de forma permanente, tanto en el área de complejos habitacionales privados en funcionamiento (y otros en plena construcción), como en los 3 barrios y asentamientos informales del norte de Santo Tomé: El Chaparral, Costa Azul y Santo Tomás de Aquino.**

Pese a la repercusión pública que tuvo la exposición mediática del proyecto de independencia del sector poblacional del norte de Santo Tomé, los vecinos no sintieron una respuesta por parte del municipio. **"El malestar pasa por la falta de servicios que la municipalidad de Santo Tomé debería brindar en el sector en función de la tasa general de inmueble que cobra. Es lamentable el mantenimiento de los caminos de accesos y colectoras de la zona, sumado a la falta de desmalezamiento, limpieza de cunetas y sostenimiento del alumbrado público.** Este verano, por ejemplo, no existió la fumigación", resaltó a [UNO Santa Fe](https://www.unosantafe.com.ar/), Darío Claría, uno de los referentes vecinalistas de sector identificado como DUE para la municipalidad de Santo Tomé.

Vecinos de la zona de barrios cerrados y countries de Santo Tomé, reclaman una prestación de servicios acordes al pago de impuestos del sector identificado por la municipalidad como Distrito de Urbanizaciones Especiales (DUE).

**"Hay un olvido casi total por parte del municipio, lo que nos hace pensar que estamos aislados de la ciudad, sin conexión con la zona urbana santotomesina"**, sostuvo Claría y continuó agregando: **"Transcurrió muchísimo tiempo desde que comenzaron los primeros intentos de urbanización de la zona y hasta el día de hoy nos sentimos como vecinos ajenos a Santo Tomé. Lo único que nos une es el pago de impuestos y nada más; lamentablemente no observamos la retribución en servicios acordes para la zona"**.

Vale remarcar que, según la evaluación del proyecto vecinal que originalmente analizó la posibilidad de emanciparse del municipio santotomesino, son cerca de tres mil los trabajadores que diariamente prestan servicio en la zona de barrios cerrados y countries, además de empresas de la zona (empleados de seguridad, obreros, personal administrativo, de servicio doméstico, entre otros). **"La proyección para los próximos años es de 20 mil pobladores estables y en la actualidad no vemos una actitud municipal acorde al impulso demográfico de la zona. Estamos totalmente aislados de la ciudad de Santo Tomé, sin conexiones de transporte.** Todos los trabajadores del área en cuestión provienen de la ciudad de Santa Fe", sostuvo el referente vecinal.

Otro de las situaciones que expone la necesidad de mayor presencia municipal por parte de los vecinos en la zona, es el importantísimo flujo vehicular que a diario circula por el acceso norte santotomesino y por las colectoras de ingreso y egreso a los barrios cerrados y countries: **"Es una vergüenza el mantenimiento de los caminos, cuyos baches son muchísimos y de grandes dimensiones; los mismos son tapados solamente con tierra y piedras en ciertas ocasiones"**, sentenció Claría.

Vecinos de la zona de barrios cerrados y countries de Santo Tomé, reclaman una prestación de servicios acordes al pago de impuestos del sector identificado por la municipalidad como Distrito de Urbanizaciones Especiales (DUE).

"Fueron muchos años sin prácticamente ninguna asistencia del Estado municipal de Santo Tomé a la zona; nunca atendió de forma integral las necesidades y servicios de un sector que es el de mayor expansión edilicia y poblacional de la región, que además, le aporta un importante porcentaje de ingresos por impuestos al municipio; ver la realidad que vivimos y leer que la municipalidad asiste con plan de obras y servicios al sector, es realmente una burla para todos los vecinos", finalizó Darío Claría, uno de los referentes vecinalistas de la zona de barrios cerrados y countries.

![](https://media.unosantafe.com.ar/p/d7530657745d601610df1022cc5ca6b1/adjuntos/204/imagenes/027/953/0027953126/santo-tomejpg.jpg?0000-00-00-00-00-00 =642x417)