---
category: Estado Real
date: 2022-07-01T09:32:47-03:00
thumbnail: https://assets.3dnoticias.com.ar/2022-06-29NID_275147O_1.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: LT 10 Radio Universidad
resumen: 'Deuda de Nación: para la oposición el acuerdo perjudica a la provincia'
title: 'Deuda de Nación: para la oposición el acuerdo perjudica a la provincia'
entradilla: Esto se debe a que se aceptó un pago a la AFIP del 1,9% que se realizará
  en efectivo todos los meses, mientras que la deuda de coparticipación se pagará
  en bonos.

---
El gobernador Omar Perotti y el ministro de Economía, Martín Guzmán, anunciaron este miércoles el acuerdo para pago de la deuda de Nación a provincia de Santa Fe tras el fallo de la Corte por los fondos descontados en forma indebida en concepto de coparticipación federal.

Será una actualización del monto a 151.800.000.000 de pesos “que serán cancelados a través de bonos actualizables por el CER” (Coeficiente de Estabilización de Referencia), el índice de ajuste diario, elaborado por el Banco Central.

Pablo Olivares, exsecretario de Finanzas de la provincia aseguró que a largo plazo (unos 16 años), lo que se va a dar es un "empate financiero". Esto se debe a que con el acuerdo aceptado ayer, se estableció el pago de la deuda que tiene Nación con la provincia en bonos, pero al mismo tiempo la aceptación voluntaria del pago, "colaboración al presupuesto de la Afip del 1,9% de la recaudación de impuestos nacionales que se coparticipan y que no estaba obligado a hacerlo, pero ahora, la provincia aceptaría cobrar en bonos y comenzar a pagar en efectivo para el presupuesto de la Afip, un monto que ronda los 8 mil millones de pesos al año".

Entre las repercusiones de esta situación, también se expresó Gonzalo Saglione ex ministro de Economía, quien aseguró que "ese 1.9% de la Afip no puede retenerse sin el consentimiento de la provincia, y ni Bonfati ni Binner la dieron, y ahora parece que el gobernador habría dado el consentimiento. Si calculamos que el acuerdo es por 150 mil millones en 10 años, 15 mil millones por año, con lo de la Afip vamos a estar perdiendo la mitad, 8 mil millones por año".

"Este era un día para nosotros histórico, y sin embargo hoy estamos más preocupados que contentos" concluyó

Por su parte, la diputada provincial Clara Garcia dijo que "en una suerte de caballo de Troya, adentro de esto que es una muy buena noticia viene una detracción, es que van a quitar de la recaudación de la Afip un 2% ¿Es el federalismo al revéz? ¿Santa Fe y las demás provincias vamos a sostener a la Afip? Asi como Binner fue el padre del federalismo fiscal, esta firma de Perotti es la responsable de la entrega al centralismo fiscal".

Otro de los que se manifestó luego de la firma del acuerdo fue el senador provincial Felipe Michlig, quien aseguró que “no hemos sido consultados, una vez más hay una medida y un procedimiento unilateral, inconsulto, del gobernador Omar Perotti". Además, agregó que "algo que podría ser una oportunidad para los santafesinos la anula y hace un acuerdo que no beneficia a los santafesinos".

Michlig detalla que con este acuerdo “estamos cobrando solamente el 25% de los casi 600 mil millones de pesos si lo actualizaríamos por inflación. Estos bonos, hoy el valor real tienen apenas un poco menos del 50% del valor nominal”. En este sentido, también agregó que en su momento no se firmó el acuerdo y se llevó a juicio por el tema del aporte para el funcionamiento de la Afip.