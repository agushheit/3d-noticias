---
category: La Ciudad
date: 2021-12-15T06:15:40-03:00
thumbnail: https://assets.3dnoticias.com.ar/pasegym.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: La Cámara de Gimnasios de la ciudad se manifestó a favor del pase sanitario
title: La Cámara de Gimnasios de la ciudad se manifestó a favor del pase sanitario
entradilla: '"No sería difícil de implementar", admitió uno de los referentes del
  sector. La provincia define de qué forma implementará la medida nacional que comenzaría
  a regir el 1 de enero

'

---
Mientras la provincia define de qué forma implementará el pase sanitario y cuáles serán las actividades que se verán afectadas en Santa Fe, algunos sectores comenzaron a expresarse al respecto.

Desde la Cámara de Gimnasios de la ciudad se manifestaron a favor de la medida. Ernesto Capozzolo, referente de la entidad señaló fue "un tema que estuvimos charlando con los colegas".

Si bien admitió que "el pase sanitario va a generar una grieta con respecto a la gente que está de acuerdo o no con la vacunación", comentó que la entidad definió apoyar "este tipo de medidas sanitarias y lo que defina el Ministerio de Salud".

Dijo que "implementar el pase sanitario no sería dificultoso. La gente que visita los gimnasios está registrada. Hay una continuidad en el ingreso. O sea, no sería difícil de implementar y si es una medida para poder terminar con todo esto (por la pandemia), bienvenido sea".

Con respecto a su implementación, profundizó: "Lo que habría que hacer en una primera instancia es pedirle a toda la gente que está asociada al gimnasio el carné de vacunación al día. Cada cliente nuevo o viejo que se reinscriba se le pedirá el carné de vacunación. El mecanismo de control de acceso que tienen los gimnasios está informatizado. No es como otro tipo de comercio o servicios donde va rotando mucho la cantidad de gente. Con lo cual la implementación no nos parece que sería una dificultad".

En declaraciones a la emisora LT10, sostuvo que "mayoritariamente" la genta optó por vacunarse. Con relación a quienes eligen no colocarse la vacuna, comentó que regirá el derecho de admisión. Y reflexionó: "Me parece que existirá mucha presión puertas para adentro de la gente que ya está vacunada de que no se le permita el ingreso a quienes no tengan el esquema completo".

"Nosotros siempre dijimos que somos promotores de salud y en esa línea vamos a seguir", definió Capozzolo.