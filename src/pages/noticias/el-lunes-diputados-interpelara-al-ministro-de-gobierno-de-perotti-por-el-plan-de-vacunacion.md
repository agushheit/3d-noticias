---
category: Agenda Ciudadana
date: 2021-06-11T08:48:15-03:00
thumbnail: https://assets.3dnoticias.com.ar/sukerman.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Diario Uno
resumen: El lunes Diputados interpelará al ministro de Gobierno de Perotti por el
  plan de vacunación
title: El lunes Diputados interpelará al ministro de Gobierno de Perotti por el plan
  de vacunación
entradilla: Roberto Sukerman fue citado para el 14 de junio, a las 10, para dar respuestas
  sobre el plan de vacunación de la provincia luego del escándalo de la vacunación
  del vicepresidente de la EPE, Alberto Joaquín

---
Luego que se conoció que Alberto Joaquín –quien fue hasta este jueves vicepresidente de la Empresa Provincial de la Energía (EPE)– se vacunó en febrero y marzo pasado contra el Covid burlando el esquema previsto en la provincia, la Cámara de Diputados de la provincia decidió interpelar al ministro de Gobierno, Justicia, Derechos Humanos y Diversidad, Roberto Sukerman. Desde el bloque del Frente Progresista como del Frente Social y Popular e Igualdad y Participación aseguraron que la citación a Sukerman se debe a la necesidad de tener "respuestas políticas" sobre el plan de vacunación que lleva adelante la provincia.

Desde el bloque del PJ se opusieron a la figura a la que se espera que dé respuestas el lunes que viene, a las 10. Tanto Leandro Busatto, como Ricardo Olivera y Luis Rubeo aseguraron que la funcionaria indicada para dar las respuestas era la ministra de Salud, Sonia Martorano. Pero, además, cuestionaron la modalidad de la convocatoria y dijeron que eso dejaba en claro que había un trasfondo que buscaba un rédito político más que respuestas a lo que sucedió en Granadero Baigorria.

No es lo mismo una interpelación que una citación. Los términos y el tenor, son otros. En el primer caso, que fue el elegido por la mayoría de los diputados y diputadas y que se contempla en el Artículo Nº 45 de la Constitución Provincial, es la Cámara la que fija el día y horario en el que el ministro debe concurrir. Es una obligación para el funcionario del Ejecutivo. Mientras que en una citación, la llegada del representante del gobierno provincial se acuerda y hasta se puede llegar a posponer en el tiempo.

Rubéo, quien fue presidente de la Cámara baja cuando Antonio Bonfatti era gobernador, recordó que cuando el PJ fue mayoría en Diputados hubo citaciones a ministros del Frente Progresista pero "siempre fue obviando el recurso de interpelación", dijo y agregó: "Parece que hay sectores políticos que quieren tener cierto rédito".

Joaquín Blanco, presidente de la bancada del Partido Socialista, fue el encargado de justificar la interpelación y dijo que "la Cámara de Diputados debe funcionar como amplificadora de las explicaciones oficiales" y que las respuestas que se esperan deben ser políticas.

Además, dijo que se debe saber cómo se va a sancionar a los responsables porque lo peor es que quede la sensación de impunidad. "Lo que pasó en Baigorria es la punta del iceberg. Por eso le queremos preguntar al ministro cuáles son las garantías para los enfermeros que quieran contar lo que vieron en los vacunatorios. Nosotros perdimos al presidente de la Cámara", recordó en referencia al fallecimiento de Miguel Lifschitz.

"Este escándalo de los vacunatorios Vip se llevó puesto a un sanitarista como Ginés González García. Está saliendo a la luz la podredumbre en Santa Fe sobre la campaña de vacunación", fustigó Blanco quien mencionó todos los hechos poco transparentes que sucedieron en la provincia y aseguró que no son casualidad sino que "hay un hilo conductor donde el Estado no puede dar transparencia a un proceso de vacunación".

A su turno, Olivera señaló: "Para nada voy a defender a nadie y soy sumamente crítico con todos aquellos que sacan alguna ventaja de esto, pero el gobierno nunca tuvo una actitud pasiva en esto ni buscó proteger a nadie. La ministra el mismo 21 de mayo, cuando se hizo la denuncia, inició una investigación". El diputado provincial, que además es presidente del PJ a nivel provincial, también se quejó porque "no se mide todo con la misma vara" y mencionó los casos de la vacunación del intendente de Rosario, Pablo Javkin, y de funcionarios municipales de la ciudad de Suardi.

El caso de la vacunación de Alberto Joaquín, quien renunció en la tarde de este jueves a su cargo en la EPE, se conoció a partir de la denuncia del jefe de Enfermería del hospital Eva Perón de Granadero Baigorria, Ariel Pérez.

"La denuncia surge de la fenomenal valentía de un hombre simple que en un momento muy difícil", dijo Carlos del Frade (FSP) y agregó para responderle a Olivera: "Ariel Pérez me llamó el fin de semana desesperado porque no tenía apoyo alguno. Fue tan fuerte la presión que recibió que la familia se tuvo que ir de Granadero Baigorria".

"El privilegio en democracia es intolerable y en medio de una pandemia eso es mucho peor", argumentó Del Frade y concluyó: "Estamos convencidos de que la respuesta es política. El Estado es el garante de la transparencia y la democracia, no debe ser colonizado por intereses privados".

Los diputados y diputadas tendrán hasta este viernes a las 12 para acercar a la presidencia de la Cámara las preguntas que quieran hacerle al ministro de Gobierno el próximo lunes. El presidente de Diputados fijó ese límite para poder remitir con tiempo al despacho de Sukerman todo el cuestionario que esperan que el lunes responda.