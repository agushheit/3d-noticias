---
category: La Ciudad
date: 2021-05-26T08:04:03-03:00
thumbnail: https://assets.3dnoticias.com.ar/genero.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad de Santa Fe
resumen: 'Restricciones por COVID-19: la Municipalidad no interrumpe la asistencia
  a víctimas de violencia de género'
title: 'Restricciones por COVID-19: la Municipalidad no interrumpe la asistencia a
  víctimas de violencia de género'
entradilla: Desde la dirección de Mujeres y Disidencias recuerdan que quienes sufran
  situaciones de violencia de género pueden dirigirse a las oficinas municipales a
  solicitar asistencia.

---
Atendiendo a las nuevas disposiciones de aislamiento establecidas por los decretos nacional y  provincial, en el marco de la pandemia de COVID-19, la Municipalidad recordó que las víctimas de violencia de género están exceptuadas de las restricciones de circulación, en caso de que necesiten dirigirse a radicar la denuncia correspondiente o pedir ayuda. Desde la dirección de Mujeres y Disidencias del municipio, se brinda asesoramiento y acompañamiento jurídico y psicológico, gratuito y especializado ante estos casos, así como también asistencia integral.

La directora de Mujeres y Disidencias municipal, Soledad Artigas, destacó que, en relación a las nuevas disposiciones del Gobierno provincial, “todas aquellas mujeres que estén atravesando violencias están exceptuadas de las restricciones de circulación, es decir que pueden moverse por la ciudad y acercarse a los organismos pertinentes para realizar denuncias o buscar acompañamiento y asesoramiento”.

Del mismo modo, hizo extensiva la convocatoria a vecinos y vecinas de la capital de la provincia que conocen a una víctima de violencia o sospechan de una situación de este tipo: “Les ofrecemos asesoramiento a esas personas, ya que muchas veces están al tanto y no saben cómo actuar”, describió.

Cabe recordar que la dirección de Mujeres y Disidencias funciona desde diciembre de 2019 en el ámbito de la Municipalidad de Santa Fe. Mediante un abordaje interdisciplinario, el área acompaña a quienes atraviesan situaciones de violencia de género y solicitan asistencia. Para ello, cuenta con un equipo de psicólogas, abogadas, trabajadoras sociales y psicólogas sociales que reciben a las víctimas y evalúan su situación particular, para buscar una salida.

**Vías de contacto**

La Dirección de Mujeres y Disidencias funciona en el ex Predio Ferial, ubicado en Las Heras 2883, luego de los trabajos que realizara el municipio para refuncionalizar el inmueble situado frente a la Terminal de Ómnibus “Manuel Belgrano”. El lugar fue adaptado para recibir a quienes requieran completar trámites o solicitar asistencia.

Los vecinos y vecinas de la capital provincial que necesiten hacer uso de los servicios pueden acercarse personalmente a Las Heras 2883, de lunes a viernes, 8 a 18 horas. También pueden comunicarse telefónicamente, al 4571525 y 4571666, de lunes a viernes de 8 a 18 horas. En tanto, la Línea de Atención Ciudadana del municipio atiende durante las 24 horas, todos los días del año, en el número 0800 777 5000.