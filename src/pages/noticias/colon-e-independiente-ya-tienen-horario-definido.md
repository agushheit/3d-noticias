---
category: Deportes
date: 2021-05-27T07:00:38-03:00
thumbnail: https://assets.3dnoticias.com.ar/colon.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: Colón e Independiente ya tienen horario definido
title: Colón e Independiente ya tienen horario definido
entradilla: La Liga Profesional confirmó los horarios de las semifinales, donde Colón
  chocará con Independiente el próximo lunes en San Juan.

---
Este miércoles la Liga Profesional llevó adelante el sorteo para determinar el orden de las semifinales de la Copa de la Liga Profesional, donde en el estadio Bicentenario de San Juan, Colón chocará ante Independiente. En tanto que la otra llave la protagonizarán Boca y Racing.

En principio, se dispuso que los encuentros de Colón-Independiente y Boca-Racing se disputaran el próximo lunes 31 de mayo, a partir de las 16, en sede por confirmar, aunque todo parecía indicar que todos los encuentros se disputarán en Buenos Aires. Pero faltaba la aprobación de parte del gobierno. Terminó entonces primando la decisión de la Liga Profesional de que ambas semifinales se disputen en el estadio Bicentenario de San Juan.

Y el martes quedó determinado que las semifinales entre Colón-Independiente y Boca-Racing se disputen el 31 de mayo serán a las 15 y 19, respectivamente. La diferencia entre uno y otro tiene que ver con varias cuestiones que la organización quería manejar con la mayor previsión posible: prevenciones sanitarias, recambio de personal, planteles y periodistas.

Este miércoles la Liga Profesional llevó a cabo el sorteo de las semifinales, donde quedó determinado que Boca y Racing chocarán el próximo lunes 31 de mayo a las 15, en tanto que Colón e Independiente chocarán a partir de las 19.

La gran final del certamen se jugará finalmente el viernes 4 de junio a partir de las 19. Después de varias versiones sobre el lugar en el que se desarrollarían los tres cotejos que definirán al campeón de la competencia, ayer también quedó establecido que la sede elegida fue el estadio Bicentenario de San Juan.

"Ambas instancias se jugarán en el estadio San Juan del Bicentenario. Las semifinales se disputarán el lunes 31 de mayo: a las 15 horas se jugará Racing-Boca; y a las 19 horas comenzará Independiente-Colón. Los horarios de estos partidos fueron definidos en un encuentro virtual de las autoridades de la LPF con representantes de los cuatro clubes semifinalistas", informó la Liga Profesional

A la vez que confirmó: "Mientras tanto, la gran final de la Copa de la Liga se jugará en el mismo estadio de San Juan, el viernes 4 de junio a las 19 horas".

**LAS SEMIFINALES DE LA COPA LIGA PROFESIONAL**

15: BOCA vs. RACING

19: INDEPENDIENTE vs. COLÒN