---
category: La Ciudad
date: 2022-01-25T06:00:27-03:00
thumbnail: https://assets.3dnoticias.com.ar/tractormartes.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Trabajos de fumigación, iluminación y bacheo previstos para este martes
title: Trabajos de fumigación, iluminación y bacheo previstos para este martes
entradilla: La Municipalidad informa el avance del cronograma de tareas previstas
  en los planes de fumigación, de bacheo y Santa Fe se Ilumina.

---
La Municipalidad avanza con el calendario semanal de fumigaciones en distintos sectores de la capital provincial. Como es habitual, se utilizan insecticidas de baja toxicidad. Según se informó, se aplican larvicidas en las zonas donde hay zanjas y zanjones, y adulticida en parques y paseos.

Para este martes está prevista la realización de trabajos en el Parque del Sur y todos los espacios verdes y paseos cercanos. Vale recordar que en caso de lluvia, se suspende el servicio.

**Iluminación**

En el marco del plan Santa Fe se Ilumina, el municipio concreta tareas tendientes a recuperar el alumbrado existente en toda la ciudad, para llegar a fin de año al 40% de la ciudad con luces led. Cuadrillas municipales trabajan en la colocación de nuevas columnas de iluminación y artefactos led en distintos barrios y avenidas, como así también en el mantenimiento de la red actual de la ciudad.

En ese sentido, se trabajará en la colocación de nuevas luminarias en los barrios:

* Barrios 7 Jefes, Roma y Fomento: conexionado de alumbrado para alimentación de nuevas luminarias


* Barrio Guadalupe: confección de pretiles en calle ayacucho entre Italia y Tacuarí
* Calle Pedro de Vega: se realizan bases para la colocación de 16 columnas para alumbrado público, entre avenida Peñaloza y Gobernador Freyre

También se realizará el mantenimiento del sistema eléctrico, entre otros puntos, en:

* Distrito de la Costa
* Barrio El Pozo
* Cantero central de bulevar Gálvez
* Plaza Pueyrredón
* Reparación de falla subterránea en Costanera Oeste
* Reparación de sectores apagados en zona norte

**Bacheo**

Por otra parte, el municipio avanza con el plan de bacheo diseñado para reparar más de 12.000 metros cuadrados de calles de la capital provincial. El programa se desarrolla por fases, priorizando las zonas de la ciudad que evidencian mayor circulación.

Actualmente se trabaja en:

* 25 de Mayo y Santiago del Estero
* Aristóbulo del Valle e Iturraspe
* Aristóbulo del Valle, entre Regis Martínez y Milenio de Polonia
* Lavaisse y Gobernador Freyre
* Europa y Boneo
* Calles internas de barrio Candioti Norte

Según se informa, todas las tareas se realizan siempre en sentido del tránsito. Puede haber cortes de circulación y desvíos de colectivos a medida que avanzan las obras. Además, se detalla que los trabajos están sujetos a las condiciones climáticas.