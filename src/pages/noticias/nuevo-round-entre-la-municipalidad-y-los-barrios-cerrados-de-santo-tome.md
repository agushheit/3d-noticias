---
category: La Ciudad
date: 2021-07-12T06:00:22-03:00
thumbnail: https://assets.3dnoticias.com.ar/ROUND2.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Nuevo round entre la Municipalidad y los barrios cerrados de Santo Tomé
title: Nuevo round entre la Municipalidad y los barrios cerrados de Santo Tomé
entradilla: Un nuevo proyecto inmobiliario en la zona de countries renovó el reclamo
  de vecinos que quieren prioridad en servicios como cloacas, agua potable y pavimento-

---
La agrupación Pai Sume, vecinos de la zona de countries en Santo Tomé, presentaron un escrito en el que reclaman la falta de servicios básicos ante el advenimiento del proyecto de crecimiento desarrollado por la firma Pilay “Santomás Pueblo Verde”.

Uno de los vecinos firmantes, Simón González, explicó que el malestar “se dispara a partir de una nota que salió hace 10 días en uno de los diarios de la ciudad en donde se plantea que el municipio de Santo Tomé está haciendo un crecimiento planificado de la zona de countries”.

“Plantean un sinnúmero de servicios y beneficios que va a tener la zona a partir de un desarrollo inmobiliario puntual de una empresa muy reconocida de la ciudad de Santa Fe”, dijo González. “Pero nuestra gran pregunta es: ¿Qué pasa con todos los otros barrios y vecinos de esta zona, que algunos habitamos hace más de 40 años, y no tenemos ni agua potable, ni cloacas, prácticamente los caminos están destruidos?”.

En este sentido, comentó: “En cierto punto aplaudimos que se hayan tomado su tiempo para tener en cuenta este nuevo desarrollo y que contemple todos los servicios que hoy no presta el municipio para que sean incorporados en ese nuevo desarrollo”.

El nuevo proyecto no era desconocido por los habitantes del Distrito de Urbanizaciones Especiales (DUE 23), y el vocero aseguró que “no están en contra de la iniciativa” pero anticipan que hará “un gran impacto” en el territorio. “Se está hablando de que va a sumar unas 40.000 personas a esta zona; casi la población de Esperanza”, dijo González.

Por su parte, el secretario de Servicios de la Municipalidad de Santo Tomé, Ricardo Méndez, le dijo a Ahí Vamos: “Cada uno de los barrios cerrados, cuando se establecen en el sector, está obligado a construirse sus sistemas e infraestructuras. Hace 15 años era una zona rural donde no había nada”.

“Haciendo un poquito de historia, la zona del DUE se creó en 2011 a través de una ordenanza. En esa zona había ya tres barrios: El Paso, El Pinar y La Tatenguita”, contó el funcionario. “Realmente lo que se hizo es tratar de ordenar ese crecimiento a través de una ordenanza que se modificó en 2014 y en 2018. Eso implica que el crecimiento de ese sector hizo necesaria primero la creación de la ordenanza y luego de la modificatoria, porque es un sector urbano que va en evolución y se prevé que puede llegar a unas 40.000 personas dentro de 15 años”.

El arquitecto subrayó que la zona es exclusivamente de barrios cerrados por lo que el municipio no puede ir a hacer un loteo convencional. Aclaró que son urbanizaciones especiales, que ese sector es exclusivamente residencial y que “esos barrios se afincaron en ese lugar con la condición de construir sus redes de servicios, es decir, sus cloacas, sus desagües fluviales, su accesibilidad, la energía eléctrica”.

“A la infraestructura están obligados los barrios a hacerla porque se entiende que dentro del urbano el municipio tiene un montón de redes hechas y llevar las redes a este sector sería una erogación casi imposible de afrontar por el municipio”, dijo Méndez.

En este sentido afirmó que Santo Tomé sí está dando acompañamiento al crecimiento sostenido de las rutas 19 y 11, y ahora sobre la autopista. “La última ordenanza incorporó los condominios, que no estaban previstos con anterioridad y que son producto de esta evolución como todos los sistemas humanos”, puntualizó el funcionario.

Mencionó además que la repavimentación es un tema que hace años están tratando y que han logrado regularizar los accesos, incorporar otro carril y seguir con la iluminación.

Frente a esto, salió a responder otro de los vecinos de la agrupación, Luis Felipe Agramunt: "En el momento en el que se hace esta intervención, por designarlo de alguna manera, dice que había tres barrios, que no es verdad, había muchos más: Aires del Llano, Dos Lagunas, Las Almenas, Altos de La Ribera, Clubes de Fútbol. Toda una realidad que viene desde el año 80".

"Veinte años después la Municipalidad de Santo Tome toma nota de que existimos. El acceso norte tan famoso que hoy nos sirve a todos lo financió la provincia. La Municipalidad de Santo Tome no hizo nada por nosotros, hasta el 2011 que nos aumenta más de un mil por ciento la tasa de retributiva de servicios. Y agregó: "Pareciera que nos toman por gente tonta" porque "una tasa retributiva de un servicio es una tasa que vos pegás por algo que te dan".

"Este señor dice que nosotros no queremos crear nuestros propios servicios. ¿Sabes por qué pagamos? Primero para que ellos controlen que los administradores de los consorcios que trabajamos gratuitamente hagamos las cosas bien. Pagamos tasas retributivas iguales al centro de Santo Tome y no tenemos nada. No queremos tampoco que nos regalen nada, pero no queremos pagar dos veces por lo mismo", dijo Agramut.

En tanto Simón González como representante de los vecinos en disconformidad con la gestión de Santo Tomé sostuvo que “estamos pidiendo lo básico que tiene todo el resto de la ciudad”.

En este sentido, relató: “Hace 40 años nosotros vivíamos con alambrado de campo, ni siquiera estaban los alambrados que hay hoy separando los countries. Era una zona totalmente rural. Toda la infraestructura fue pensada para una zona rural, era solamente campo”.

“Lo único que se hizo fue asfaltar para la Copa América (en 2011) las calles colectoras: fue pensado para una copa y hoy lo están usando 7.000 vecinos que vivimos en la zona, más todos los camiones que tienen que entrar materiales a cada uno de los desarrollos que se están haciendo”. Y agregó: “Hoy recién se está empezando a hacer el rulo de conexión de esta zona a la autopista que, en vista de la cantidad de vecinos que se plantea que van a incorporar, ya está quedando chico”.

En este sentido, remarcó que la carpeta asfáltica no fue pensada para el tránsito que hay en la zona y que el único colectivo que llega rompe el pavimento constantemente.

Por otro lado, afirmó que la zona genera entre 2.000 y 3.000 puestos de trabajo, pero “la mayoría de los empleados son de la ciudad de Santa Fe porque no existen medios de comunicación con Santo Tomé”.

“Nos cuestionamos seriamente qué atención está prestando el municipio a pesar de que hemos ido a hablar en reiteradas oportunidades con la intendenta, que nos ha recibido, pero a la hora de resultados no vemos avances”, dijo González.

En esta línea, Agramunt contó que ya tienen una zona con cloacas conectadas al caño principal de Santo Tome, pagada por los vecinos. "Otra zona se ofreció a ceder una parte de terreno y a hacer la obra. Estamos dispuestos a hacer la obra nosotros, pero el tema es que el servicio no lo podemos prestar, no estamos autorizados".

"Si instalo un foco en el acceso norte no podemos cambiarlo. Tenemos que mandar una nota a la Municipalidad diciendo que se cambie un foco, esperar un tiempo prudencial, ir cambiarlo y mandar otra nota diciendo lo cambiamos a costa nuestra. Así funciona lo más mínimo", describió.

Y agregó: "En el barrio donde yo vivo hicimos una obra hidráulica, hace dos años. Relevamos información nosotros, se la dimos a la Municipalidad porque había que hacer obras afuera del complejo para que esa agua que escurría por adentro y saliera bien. Dos años pasaron y ahora estamos yendo con una retroexcavadora nuestra y vamos a pagarlo nosotros. Demoraron dos años en que nos dijeran 'gasten la plata de nuevo ustedes'".

**Nuevos proyectos**

El secretario de Servicios de la Municipalidad de Santo Tomé dijo que la ciudad tiene una posición estratégica antes de llegar a la gran ciudad y que “conecta con Buenos Aires, con Córdoba y con el norte”. “Todos los flujos que tenemos en la ciudad, tanto vehiculares como ferroviaria, pasan por nuestra ciudad y el crecimiento se da de esta forma también”, destacó.

“Hay varios nuevos emprendimientos y hay áreas comerciales que van a aparecer que vienen a llenar ese vacío que tiene la ciudad, y que no tienen ningún tipo de servicios porque se están creando. Esos servicios van apareciendo en la medida en que las inversiones, fundamentalmente privadas, se hagan”, expresó.

Santomás se define como "un desarrollo inmobiliario integral y sustentable emplazado en 150 hectáreas que van desde la Autopista Santa Fe-Rosario hasta el río Salado. Emplazado en el corazón de la zona de countries, a sólo 5 minutos de Santa Fe y Santo Tomé".

Por último, mencionó el proyecto llamado Canal San Martin, impulsado por el municipio y que, según él, hace 20 años se está tramitando. “Lleva las aguas de todo el sector oeste de la ciudad a través de un canal que se arma dentro del espacio de la autopista y desagota frente a estos barrios cerrados, que van a beneficiar en un 90%. Está licitado y estamos esperando a que se adjudique para poder materializar la obra”.

De los vecinos de los countries los reclamos son claros: “Pedimos mayor presencia del gobierno local, una mayor prestación de servicios, una conexión vial con el resto de la ciudad. Sabemos que el municipio tiene restricciones y que nosotros no somos prioridad, pero aportamos casi un 35% en la tasa y somos el 10% de la población de Santo Tomé. Queremos que algo de eso vuelva a la zona”.

González resaltó: “La Municipalidad le cobra a estos proyectos por aprovechar un desarrollo, que el desarrollo lo venimos haciendo los vecinos que vivimos en esta zona, porque el municipio no puso un peso. Lo que pedimos es que parte de esa plusvalía vuelva a esta zona en algún tipo de servicio, en mejora del asfalto, en mejoras de la iluminación, en algún servicio que nos beneficie y responda al sinnúmero de pedidos que tenemos realizados como cloacas o agua”.

“Son servicios básicos, ni siquiera pedimos que el municipio haga la inversión, sino que nos cobre a nosotros por contribución por mejoras como cualquier otra ciudad, pero son obras que son encaradas desde un organismo del Estado”.

Por su parte Agramunt dijo: "No hay un colectivo que una con el casco urbano. No tenemos una parada de taxi, no hay una posta sanitaria. Lo único que hay es un paisaje lunar lleno de cráteres, no exagero".

González concluyó con que el distrito no tiene ninguna dependencia del municipio ni de ningún órgano del Estado, y que “dentro de poco vamos a tener la cantidad de población suficiente para ser ciudad y no hay ningún espacio público reservado para futuro teniendo en cuenta el desarrollo de la zona. Es decir, un espacio para que en algún momento haya una escuela, un dispensario, una plaza”.

"Hay una cuestión muy patética para mí", dijo Agramunt para finalizar. "Cuando viene alguien a visitarnos, le preguntas de dónde venís y te contesta 'vengo de Santa Fe' o 'vengo de Santo Tome'. Nosotros no somos Santo Tome, la gente no nos identifica como santotomesinos".