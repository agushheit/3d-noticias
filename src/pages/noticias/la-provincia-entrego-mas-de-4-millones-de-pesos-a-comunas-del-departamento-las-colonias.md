---
category: Estado Real
date: 2021-04-28T08:48:21-03:00
thumbnail: https://assets.3dnoticias.com.ar/aportes.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: La provincia entregó más de 4 millones de pesos a comunas del departamento
  Las Colonias
title: La provincia entregó más de 4 millones de pesos a comunas del departamento
  Las Colonias
entradilla: Los aportes corresponden al Fondo de Obras Menores. Los recibieron Franck,
  Grutly, Progreso, Rivadavia y Las Avispas.

---
El gobierno provincial entregó aportes correspondientes a la ley N° 12.385 del Fondo de Obras Menores por $ 4.844.371 a las localidades de Franck, Grutly, Progreso, Rivadavia y Las Avispas, del departamento Las Colonias. Las entregas fueron encabezadas por la secretaria de Coordinación de Políticas Publicas de la Provincia, Luisina Giovannini y el secretario de Comunas, Carlos Kaufmann.

“Comenzamos la gestión pagando deudas del Fondo de Obras Menores que en algunos casos databan del 2014, y hoy estamos saldando asignaciones del año 2020 y en algunos casos pagando ya el 2021, al día”, afirmó Kaufmann.

“La provincia hace un gran esfuerzo administrativo para llegar en tiempo y forma con los fondos de obras menores en estos momentos difíciles que vivimos, con el objetivo de mantener el mayor nivel de actividad económica posible, con la obra pública que es generadora de trabajo”, agregó.

**APORTES**

Cabe señalar que la comuna de Franck recibió $ 2.457.878 para la pavimentación del acceso al polideportivo local. En tanto, que Grutly obtuvo 250 mil pesos para gastos de ejecución urgente, por emergencia climática.

En tanto, la comuna de Progreso recibió $ 937.564 para obras de cordón cuneta y estabilizado granular; Rivadavia, $ 536.728 para un equipo de riesgo; y por último Las Avispas obtuvo $ 662.200 para la adquisición de una pala hidráulica.

**Ley Nº 12385**

A través de la ley 12385, el gobierno provincial otorga aportes no reintegrables a municipios y comunas, para la realización de obras, o la adquisición de equipamiento o rodados.