---
category: La Ciudad
date: 2021-11-15T06:00:33-03:00
thumbnail: https://assets.3dnoticias.com.ar/sauce.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Sauce Viejo: Papaleo se perfila como el primer intendente de su historia'
title: 'Sauce Viejo: Papaleo se perfila como el primer intendente de su historia'
entradilla: 'Con más del 70 por ciento de las mesas computadas, Mario Papaleo derrota
  al actual presidente comunal, Pedro Uliambre.

'

---
La localidad de Sauce Viejo tiene a tres candidatos que luchan por la primera intendencia de su historia, de los cuales Papaleo y Uliambre se disputan cabeza a cabeza la banca del ejecutivo; más atrás, Leonardo Lizarraga, quedó a mitad camino.

El candidato del Frente Progresista Cívico y Social, Mario Claudio Papaleo, con más del 70 por ciento de las mesas computadas, se perfila como el primer intendente de la historia de Sauce Viejo, sacando 3.200 votos, el 50 por ciento de votos.

Más abajo, el actual presidente comunal, Pedro Uliambre, del Frente de Todos, logra 2.800 votos, que representa el 44 por ciento del padrón electoral.

En las PASO, con un total de electores de 12.320 y una participación superior al 65 por ciento, el actual presidente comunal, Pedro Uliambre, ganó ampliamente la interna del Frente de Todos sobre Maximiliano Rodríguez Baraona, con un contundente 94 por ciento de votos (2.421).

Pero en el total de votos, Uliambre quedó apenas debajo del candidato del Frente Progresista Cívico y Social, Mario Claudio Papaleo, quien sin disputar internas logró la suma de 2.655 sufragios.

Por su parte, y ya en tercera instancia, quedó el candidato de Primero Santa Fe, Leonardo Lizarraga, con con la suma de 437 votos.