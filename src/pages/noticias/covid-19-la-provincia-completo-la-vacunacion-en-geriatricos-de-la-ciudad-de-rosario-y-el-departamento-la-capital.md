---
category: Estado Real
date: 2021-02-26T06:28:08-03:00
thumbnail: https://assets.3dnoticias.com.ar/geriatrico.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: 'COVID-19: La provincia completó la vacunación en geriátricos de la ciudad
  de Rosario y el departamento La Capital'
title: 'COVID-19: La provincia completó la vacunación en geriátricos de la ciudad
  de Rosario y el departamento La Capital'
entradilla: El operativo comenzó el pasado lunes y se espera inocular a 15 mil personas
  en todo el territorio provincial.

---
A partir de la llegada de vacunas Covishield contra el Coronavirus, el Ministerio de Salud de la provincia inició el pasado lunes el operativo de vacunación en geriátricos, siendo que a la fecha se completaron en la ciudad de Rosario y el departamento La Capital.

En ese marco, esta semana se abordaron más de 300 geriátricos en diferentes departamentos de la provincia, siendo así que en la ciudad de Rosario se completó la vacunación en los 161 geriátricos registrados y, en el departamento La Capital, en 150.

Al respecto, la ministra de Salud, Sonia Martorano, destacó el desempeño de los equipos y afirmó que “nuestros grupos están haciendo un excelente trabajo, recorriendo cada residencia para vacunar a todas las personas que se encuentran allí”.

Del mismo modo, la funcionaria resaltó: “Salir a los geriátricos es un hito porque llegamos a los más vulnerables, es un hecho histórico”.

Cabe señalar que desde mayo del 2020 el Ministerio de Salud llevó adelante una auditoría en todos los geriátricos de la provincia para desarrollar los protocolos necesarios en caso de contagios por Covid. Tras esto, en diciembre se solicitó la nómina de personal y residentes para diagramar el operativo que comenzó el lunes 22 de febrero, tras la llegada de la vacuna Covishield de Astrazeneca/Oxford.

Desde la cartera sanitaria aseguraron que entre el viernes y el lunes concluirían todos los geriátricos habilitados de la provincia de Santa Fe, inoculando a más de 15 mil personas.