---
category: Agenda Ciudadana
date: 2021-09-21T06:15:24-03:00
thumbnail: https://assets.3dnoticias.com.ar/kicilove.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Kicillof tomará juramento este martes a los nuevos ministros bonaerenses
title: Kicillof tomará juramento este martes a los nuevos ministros bonaerenses
entradilla: El acto se realizará en el Teatro Metro de La Plata, a partir a las 16.
  El gobernador bonaerense ya se reunió previamente con los integrantes del nuevo
  gabinete para desarrollar los pasos a seguir.

---
El gobernador bonaerense, Axel Kicillof, tomará juramento este martes a los nuevos ministros que se incorporan al gabinete provincial, durante un acto que se realizará a partir de las 16 en La Plata.

Según informó la Gobernación, el acto no se realizará en la Casa de Gobierno provincial sino en el Teatro Metro, ubicado en la calle 4 entre 51 y 53.

Kicillof se reunió este lunes con el Jefe de Gabinete, Carlos Bianco, que a partir del martes será Jefe de Asesores, y con el actual intendente de Lomas de Zamora, Martín Insaurralde, que lo reemplazará en el cargo.

"Muchas gracias @Carli_Bianco por este tiempo de enorme trabajo y entrega. Por delante tenés el gran desafío de coordinar asuntos estratégicos para la transformación que estamos llevando adelante en la Provincia", posteó el gobernador en Twitter.

Por la mañana había hecho lo mismo con la ministra de Gobierno saliente, Teresa García, y con la diputada nacional Cristina Álvarez Rodríguez, quien la reemplazará en el cargo, en el marco de los cambios dispuestos en su gabinete.

"Analizamos el trabajo realizado y los desafíos que tenemos por delante en esta área tan importante de la gestión de cara a la pospandemia", dijo el mandatario bonaerense en su cuenta de Twitter y compartió una foto junto a ambas dirigentes políticas.

Según dijo la Gobernación, también programó una reunión con el jefe comunal de Malvinas Argentinas, Leonardo Nardini, designado para reemplazar a Agustín Simone en el Ministerio de Infraestructura y Servicio Públicos.