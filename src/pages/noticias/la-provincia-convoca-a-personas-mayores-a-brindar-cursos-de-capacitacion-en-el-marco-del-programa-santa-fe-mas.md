---
category: Agenda Ciudadana
date: 2020-12-03T10:19:52Z
thumbnail: https://assets.3dnoticias.com.ar/santafe-mas.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: La provincia convoca a personas mayores a brindar cursos de capacitación
  en el marco del programa Santa Fe Más
title: La provincia convoca a personas mayores a brindar cursos de capacitación en
  el marco del programa Santa Fe Más
entradilla: El Ministerio de Desarrollo Social abrió una convocatoria a personas mayores
  de 60 años para brindar formaciones en oficios como carpintería, artesanías, construcción,
  mantenimiento de vehículos, entre otras.

---
**El objetivo es conectar generaciones para acompañar la inserción de jóvenes en el mundo del trabajo**.

La iniciativa promueve el rol activo de las personas mayores en nuestra sociedad revalorizado sus conocimientos, contribuyendo a la **recuperación de oficios tradicionales o artesanales y rescatando sus saberes y experiencias para transmitirlas a los y las jóvenes**.

El ministro de Desarrollo Social, Danilo Capitani, indicó que “la experiencia acumulada en una vida de trabajo puede canalizarse a través de prácticas que conviertan a las personas mayores en protagonistas activos de la sociedad, al mismo tiempo que los y las jóvenes adquieran nuevas herramientas para su preparación con herramientas y oportunidades concretas para incorporarse al mercado laboral”.

La directora provincial de Adultos Mayores, Lucía Billoud, manifestó que “la promoción de este intercambio de prácticas y saberes entre personas de distintas generaciones permite recuperar y fortalecer oficios tradicionales, mejorar las capacidades de trabajo de los participantes del proyecto como así también revalorizar la experiencia de las personas mayores”

Por su parte, la directora provincial de Inclusión Socio-productiva, Julia Irigoitia, expresó que “la participación de personas mayores como capacitadores del programa Santa Fe Más es **una apuesta a construir espacios intergeneracionales**. Implica todo un desafío que se puedan descubrir las particularidades de cada generación y fusionarlas en pos de que emerjan nuevas formas de pensarnos.

Y agregó que “además de enseñar cuestiones particulares de sus oficios, los adultos mayores podrán transmitirles a las chicas y los chicos que integran los talleres experiencias de vida y valores como el compromiso, la perseverancia, la solidaridad y el trabajo en equipo. Creemos que estos valores son los que completan el desarrollo de la formación de las y los jóvenes”.

Las personas interesadas pueden comunicarse de lunes a viernes de 8 a 14hs al teléfono 342- 4815564

## **Santa Fe Más**

El programa provincial Santa Fe Más acompaña el recorrido formativo y educativo de los y las jóvenes. Apunta a la restitución de sus derechos e incluye a personas, de entre 16 y 30 años, que se encuentran fuera del mercado laboral formal o hayan abandonado la escolaridad.

Consta de dos etapas:  una acompaña el trayecto formativo y la otra consta de una estrategia de egreso que puede desencadenar en una inserción laboral definitiva, el reinicio de las actividades escolares o el comienzo de una etapa productiva.

**También propone establecer lazos socio-afectivos y generar el sentido del trabajo colectivo, el esfuerzo y la solidaridad**.