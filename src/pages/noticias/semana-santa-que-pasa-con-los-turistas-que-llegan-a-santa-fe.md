---
category: Agenda Ciudadana
date: 2021-04-02T07:09:18-03:00
thumbnail: https://assets.3dnoticias.com.ar/control.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de LT10
resumen: 'Semana Santa: qué pasa con los turistas que llegan a Santa Fe'
title: 'Semana Santa: qué pasa con los turistas que llegan a Santa Fe'
entradilla: Serán estrictos los controles en los principales corredores por donde
  ingresan los turistas. También se preparan controles para los regresos del domingo.
  Habrá 70 puestos de control de alcoholemia.

---
Ante la llegada de Semana Santa, que coincide con el inicio de la segunda ola de coronavirus en el país, incrementan los controles en relación al turismo en la provincia de Santa Fe para evitar contagios teniendo en cuenta las nuevas cepas que circulan en otras provincias del país.

Para ello el gobierno santafesino implementará operativos de control y fiscalización en 70 puntos de la provincia donde se realizará ordenamiento del tránsito, detención de vehículos para control de documentación, de exceso de pasajeros y uso de dispositivos de seguridad.

En estos puestos, pedirán el permiso de circulación del gobierno nacional, la reserva del alojamiento pero también la aplicación Covid 19 Santa Fe que permite realizar el seguimiento.

En el caso de los autos particulares, se controlará la ventilación, y el uso del barbijos y del cinturón de seguridad. Sólo en el caso en el que los pasajeros formen parte de una misma burbuja familiar, se permitirá que no se use el tapabocas dentro del habitáculo.

Por otra parte, habrá más los controles de alcoholemia con el propósito de desestimar el consumo de alcohol al conducir y evitar el riesgo generado por los efectos de esta sustancia en el organismo.

 

**Vuelven los controles en el túnel subfluvial**

Hasta el domingo, antes de ingresar a Santa Fe por el túnel subfluvial se realizarán controles a los automovilistas que circulen por el viaducto  ya que se espera mayor circulación por la Semana Santa en medio de un aumento de casos de coronavirus en nuestra provincia.  

A los conductores se les practicará el test de anosmia (olfato)  y temperatura. Además, se les solicitará el permiso Único Habilitante para Circulación o el Certificado Verano.

Los vehículos podrán circular con un pasajero por ventanilla, es decir, en un auto común solamente podrán viajar 4 personas, que deberán usar mascarillas aunque sean familiares, según detalló a LT10 Karina Rotela.