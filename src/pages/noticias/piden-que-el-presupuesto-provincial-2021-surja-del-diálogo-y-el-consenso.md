---
layout: Noticia con imagen
author: "FUENTE: La Capital"
resumen: Presupuesto provincial 2021
category: Agenda Ciudadana
title: Piden que el presupuesto provincial 2021 surja del diálogo y el consenso
entradilla: Llamamiento del jefe del bloque de senadores santafesinos de la UCR,
  Felipe Michlig, al gobierno de Omar Perotti.
date: 2020-11-16T13:23:14.864Z
thumbnail: https://assets.3dnoticias.com.ar/michlig.jpeg
---
**El jefe del bloque de senadores provinciales de la UCR, Felipe Michlig,** instó a que el presupuesto 2021 que elaboró la Casa Gris “surja del diálogo y un amplio consenso político”, al tiempo que **pidió al gobernador Omar Perotti que convoque de modo “urgente” a una mesa de conversaciones con los partidos políticos.**

Acerca del inminente debate del presupuesto 2021 (que se encuentra a consideración del Senado provincial), Michlig indicó: “Buscamos que sea equilibrado, justo y que surja del diálogo y un amplio consenso legislativo y político, que tenga una visión territorial equitativa en la que contemple obras prioritarias a lo largo y a lo ancho de Santa Fe, además de las necesidades de todos los municipios y las comunas, sin distinciones partidarias”.

“Nuestra posición es siempre propositiva. Planteamos al Ejecutivo que sería muy útil poder conformar una mesa de diálogo y trabajo conjunto, de modo urgente, para tratar de encontrar los consensos necesarios para avanzar en temas fundamentales para los santafesinos, no sólo el presupuesto, en consonancia con lo que se habló en algunos encuentros con el gobernador y teniendo en cuenta cómo se componen las mayorías en las Cámaras legislativas”, señaló. El oficialismo tiene mayoría en el Senado y el Frente Progresista (FPCyS) en Diputados.

En esa línea, recordó: “Días atrás mantuvimos un encuentro con el ministro de Economía provincial, Walter Agosto, y Marcos Corach (integrante del Ministerio de Gestión Pública). Junto al senador Lisandro Enrico ratificamos la posición de la UCR en ese sentido, más allá de que **les trasladamos muchos planteos que, a diario, recogemos de los gobiernos locales en distintos puntos de la provincia y que también vienen surgiendo del Foro de Intendentes Radicales**”.