---
category: Estado Real
date: 2021-06-24T08:36:47-03:00
thumbnail: https://assets.3dnoticias.com.ar/trabajo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: La provincia ofrece nuevos cursos de formación laboral
title: La provincia ofrece nuevos cursos de formación laboral
entradilla: Se renueva la propuesta del Programa “Santa Fe Capacita”, cuyo objetivo
  es mejorar la empleabilidad de las y los santafesinos a través de propuestas presenciales
  y virtuales.

---
La provincia de Santa Fe, a través del programa del Ministerio de Trabajo, Empleo y Seguridad Social “Santa Fe Capacita” informa el inicio de nuevas opciones de formación laboral gratuitas.

Se trata de una oferta de más de 40 cursos diseñados junto a instituciones intermedias. Su meta es brindar herramientas para las y los santafesinos que buscan un nuevo empleo o para quienes ya se encuentran trabajando y desean incorporar nuevas competencias y/o saberes.

Hay trayectos formativos tanto en modalidad presencial como virtual. Las temáticas abarcan administración y contabilidad; computación e informática; construcción; cuidado de personas, marketing y comunicación; oficios tradicionales y no tradicionales y empleos verdes.

La oferta completa de cursos, así como los links de inscripción pueden consultarse en [www.santafe.gob.ar/trabajo.]()