---
category: Agenda Ciudadana
date: 2020-12-19T11:21:26Z
thumbnail: https://assets.3dnoticias.com.ar/1912-cooperativas.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Proyecto de ley de «Cooperativas en Contexto de Encierro»
title: Proyecto de ley de «Cooperativas en Contexto de Encierro»
entradilla: La iniciativa que presentó el gobierno provincial tiene como objetivo
  la capacitación y reinserción de los internos penitenciarios.

---
El gobierno provincial presentó este jueves en la ciudad de Santa Fe el proyecto de ley de «Cooperativas en Contexto de Encierro» que se refiere a la posibilidad de conformar estos espacios para que los internos puedan salir a trabajar, cumpliendo con los deberes y derechos que tienen, consagrados en el artículo 106 de la Ley de Ejecución Penal de la provincia.

Al respecto, el secretario de Asuntos Penitenciarios Walter Gálvez, manifestó que «este es un evento muy importante» que tiene como objetivo «la capacitación y reinserción de los internos».

«Se ha trabajado mucho para lograr la conformación de una cooperativa para que el penitenciario pueda salir a trabajar», expresó Gálvez y remarcó que «aquí se pueden ver elementos que se están construyendo y que van a servir para mejorar y dar un paso adelante en la estructura del Servicio Penitenciario provincial en un tiempo de coyuntura muy difícil».

Finalmente, el funcionario recordó que «los internos estuvieron todo el año encerrados sin visitas, sin salidas transitorias y mucho menos laborales. Con esta iniciativa empezamos, de a poco, a normalizar el Servicio Penitenciario».

Por su parte, el subsecretario del Servicio Penitenciario, Héctor Acuña, expresó que «hablar de cooperativismo es hablar de formación y educación, significa que los internos tengan un oficio y una salida. Tener un oficio es una herramienta para poder defenderse y eso significa cumplir con la ley».

«Tenemos una potencialidad de 6700 empleados intramuro y después vamos a trabajar con internos con oficios judiciales, que son aquellos que tienen salidas transitorias. El nivel de potencialidad de trabajo es muy grande y nada mejor para la reinserción que enseñar la cultura de trabajo», finalizó Acuña.

La actividad se realizó en la plaza «Ciudadano del Rosario» y contó también con la presencia del director del Instituto Autárquico de Industrias Penitenciarias (Iapip), Claudio Marino.

***

## **OBRAS EN LA UNIDAD PENAL N°9 DE RECREO**

Por otra parte, Gálvez se refirió a las obras que el gobierno provincial planea realizar en la Unidad Penal N°9 de Recreo. «Entendemos que no se pueden construir más pabellones ni en Las Flores ni en Coronda, por lo cual hay que pensar en construcciones nuevas. Las Flores tiene casi 40 años y Coronda casi 80, ese es el plazo en que no se ha construido nada de cero en los alrededores de la ciudad de Santa Fe».

«El Servicio Penitenciario cuenta con 9 hectáreas en la Unidad Penal Nº 9 de Recreo, a esa unidad la vamos a intervenir constructivamente bajo un proyecto de avanzada y novedoso para la última parte de la pena y va a servir para menguar la urgencia que tenemos en esta zona de la provincia», añadió el funcionario.

«Pensamos en diferentes etapas, primero la construcción de una Cárcel de Mujeres y, luego, dos mini penales que sirvan para la reinserción del interno, evitando que sigan amontonados en lugares de encierro con carencias y dificultades», finalizó Gálvez.

***

## **PRESENTES**

Participaron también de la actividad: la secretaria de Estado de Igualdad y Género, Celia Arena; el secretario de Desarrollo Territorial y Arraigo, Fabricio Medina; el secretario de Prácticas Socio Comunitarias, Ignacio Martínez Kerz; el diputado provincial Leandro Busatto; el intendente de la ciudad de Santa Fe, Emilio Jatón; y la concejal Jorgelina Mudallel; entre otras autoridades.