---
category: La Ciudad
date: 2021-03-28T06:15:43-03:00
thumbnail: https://assets.3dnoticias.com.ar/GUADALUPE.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Preocupación por usurpación de terrenos en Guadalupe
title: Preocupación por usurpación de terrenos en Guadalupe
entradilla: Varios vecinos ampliaron sus patios con muros consolidados sobre una lonja
  de tierra que pertenece al Estado. Nadie los frenó. Y los avances continúan.

---
Varios vecinos de barrio Guadalupe avanzaron sobre terrenos fiscales en la ampliación de sus patios. Esta intrusión se concretó durante los últimos meses de pandemia. Y todavía se pueden observar en el lugar restos de obra y de los imponentes tapiales que algunos levantaron en el fondo de sus casas.

El avance de vecinos sobre terrenos fiscales es un problema de larga data en la ciudad. El año pasado el foco estuvo en los terrenos usurpados detrás del CIC de Facundo Zuviría, donde personas que atraviesan una grave situación social habían levantado precarias viviendas ocupando un terreno destinado a un futuro plan habitacional. Tras varios meses de tensión e incertidumbre, finalmente las autoridades lograron deponer la actitud de los ocupantes y el terreno fiscal quedó en custodia de la Provincia hasta que se comience a ejecutar el plan habitacional tipo Procrear, con fondos nacionales.

Ahora el foco está puesto en estos terrenos de Guadalupe, en lo que algunos vecinos llaman "ocupación VIP". Otros dicen, en cambio, que ampliaron sus patios "para evitar que venga alguien y levante una precaria vivienda". Estos no quieren perder el apacible clima social de la villa junto a la laguna Setúbal, en un sector exclusivo de la ciudad. Lo cierto es que el tema despertó la preocupación de varias instituciones y organizaciones del lugar.

Por parte del Estado nacional participó de la reunión el representante local de la Agencia de Administración de Bienes del Estado (Aabe), Sergio Decoud; por Provincia, el secretario de Hábitat, Urbanismo y Vivienda, Amado Zorzón, junto al director provincial de Localización y Adquisición de Tierras, Francisco Alda; mientras que por parte del municipio estuvo la directora ejecutiva del Ente Autárquico Santa Fe Hábitat, Paola Pallero, entre otros.

En la reunión, los vecinos mencionaron que, durante 2020, mientras todo el mundo permanecía aislado en sus hogares por la pandemia, se concretaron al menos seis avances de obra sobre estos terrenos fiscales. "Antes era más lento, pero en los últimos meses tomó un fuerte impuso", dijo uno de ellos (se reserva su identidad).

_Con información de El Litoral_