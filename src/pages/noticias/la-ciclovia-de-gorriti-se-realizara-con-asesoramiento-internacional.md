---
category: La Ciudad
date: 2020-11-28T14:02:59Z
thumbnail: https://assets.3dnoticias.com.ar/CICLOViA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Ciclovía de calle Gorriti
title: 'La ciclovía de Gorriti se realizará con asesoramiento internacional '
entradilla: 'El proyecto de la Municipalidad ganó la convocatoria denominada “Ciclovías
  emergentes para una América Latina Resiliente”. '

---
El proyecto de la Ciclovía de calle Gorriti, impulsado por la Municipalidad de Santa Fe, fue seleccionado para recibir acompañamiento técnico de la consultora en movilidad sostenible y espacio público, BikeNcity, que lanzó la convocatoria “Ciclovías emergentes para una América Latina Resiliente”, destinada a ciudades de la región y el Caribe.

En ese marco, había que presentar proyectos avanzados y BikeNcity junto al Banco Interamericano de Desarrollo (BID) escogieron a las mejores propuestas, entre las que se encuentra la ciclovía de calle Gorriti.

En consonancia, la Municipalidad recibirá asistencia técnica para la implementación de una ciclovía a ambos lados del cantero central de Gorriti en un tramo de 3,3 km, desde Av. Blas Parera hasta Av. Aristóbulo del Valle.

Además del asesoramiento técnico, también recibirá talleres relacionados a fomentar una ciudad más sustentable. Las capacitaciones comienzan este mes -sobre estrategias para ciclovías emergentes- y finalizarán a fines de enero. Además del proyecto santafesino, fueron elegidas otras dos ciclovías colombianas. Una de la ciudad de Fusagasugá y la otra de Tunja.

## Iniciativa internacional

La convocatoria que en la que fue seleccionado el proyecto local se enmarca en la Acción Climática Colaborativa, el BID y la Cooperación Alemana para el Desarrollo Sustentable (GIZ), la cual trabaja por mandato del Ministerio Federal de Medio Ambiente, Protección de la Naturaleza y Seguridad Nuclear (BMU, por sus siglas en alemán) que convocó a las ciudades de América Latina y el Caribe a participar en el proceso de selección para recibir asistencia técnica por parte de BikeNcity, para la implementación de ciclovías emergentes. 

El objetivo es brindar las herramientas que faciliten el proceso de planeación, implementación, operación, monitoreo y evaluación a tres gobiernos locales para promover el uso de la bicicleta y de otros vehículos de micromovilidad, la recuperación verde e incentivar el desarrollo sustentable en la región.

## Un proyecto que nació de la participación

Cabe recordar que la Municipalidad realizó diversos encuentros con representantes de vecinales del Norte santafesino, instituciones y colectivos de ciclistas urbanos, con el objetivo de definir aspectos de lo que será una conexión fundamental para las vecinas y vecinos que utilizan la bicicleta como medio de transporte.

Algunos de los representantes de instituciones que aportaron sus miradas a lo que será la ciclovía de Gorriti fueron: integrantes de las vecinales de Peñaloza Norte, barrio Favaloro, Nueva Pompeya y Altos de Noguera, Asociación Civil Caracoles y Proyecto Ciclociudad. 

Por parte de la Municipalidad, estuvieron presentes las y los secretarios de Desarrollo Urbano, Javier Mendiodo; Control y Convivencia Ciudadana, Virgina Coudannes; Integración y Economía Social, Mariano Granato; y Educación y Cultura, Paulo Ricci.

Oportunamente, Javier Mendiondo, destacó el aporte y la participación de los vecinos para diagramar en conjunto una conexión segura para los ciclistas urbanos, “con la confluencia de voces, se construyen mejores proyectos para la ciudad”, valoró.  “Como nos pidió el intendente Emilio Jatón, la intención de la Municipalidad es poder desarrollar una red de ciclovías no solamente en el centro de la ciudad (que es donde se concentra la mayor parte de esta infraestructura) sino que además queremos garantizar el uso de las bicicletas en los diferentes barrios del Norte santafesino”, amplió Mendiondo.