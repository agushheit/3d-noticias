---
category: La Ciudad
date: 2021-09-10T06:15:18-03:00
thumbnail: https://assets.3dnoticias.com.ar/FERIAPLAZA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Se suma la venta de productos frescos en la feria de la Plaza Pueyrredón
title: Se suma la venta de productos frescos en la feria de la Plaza Pueyrredón
entradilla: 'En el marco del programa Mercado Santafesino, todos los sábados se ofrecen
  elaboraciones derivadas de la carne de pescado, pollo y cerdo en La Verdecita que
  funciona en la plaza Pueyrredón, de 7 a 13. '

---
La Municipalidad puso en marcha durante la pandemia el programa Mercado Santafesino, con el fin de acompañar a las unidades productivas, emprendimientos y cooperativas de producción de alimentos que se encontraban dentro de la Economía social y popular, mientras duraba el distanciamiento obligatorio.

Ahora, y con el fin de fortalecer las ferias populares que venden productos alimenticios como La Verdecita que funciona en la plaza Pueyrredón, durante los sábados de septiembre se suma una camioneta refrigerante con alimentos frescos que complementen la canasta básica y sumen a la oferta de verduras y otros productos que se ofrecen en este espacio.

Allí se podrán encontrar productos elaborados con pescado, cerdo y pollo como hamburguesa, milanesas, empanadas y también algunos cortes de cerdos. Según explicaron desde la Secretaría de Integración y Economía Social, la idea es que empiece a tomar escala e instalar esta camioneta de frío en otras ferias alimenticias de la ciudad, en algunas que ya funcionan y en otras nuevas como la que va a empezar a funcionar en el Parque Garay el próximo viernes 17.

Algunas ofertas son: milanesas de pescado a $660 el kilo; 1 kilo de milanesas de pollo y tres arrolladitos a $750; 6 empanadas de carne, más 6 de verduras, más 6 de pescado a $800; y milanesas de pollo a $440, entre otras propuestas.