---
category: Agenda Ciudadana
date: 2021-01-13T07:36:40Z
thumbnail: https://assets.3dnoticias.com.ar/dioxido-cloro.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: '3DNoticias - Fuente: Télam'
resumen: 'Dióxido de Cloro: califican de temerario su uso y avanza la judicialización
  del caso en el Otamendi'
title: 'Dióxido de Cloro: Califican de temerario su uso y avanza la judicialización
  del caso en el Otamendi'
entradilla: "«Se puede hablar de un accionar temerario por parte del profesional médico
  que prescribió una sustancia química que no demostró ningún beneficio», dijo el
  médico Sergio Saracco. "

---
Toxicólogos consultados por Télam coincidieron en calificar de «temerario» el uso de dióxido de cloro para tratar la Covid 19, mientras se profundiza la judicialización del caso del hombre de 93 años fallecido en el Sanatorio Otamendi, con denuncias promovidas contra la clínica, pero también contra el juez que habilitó la prescripción de la sustancia.

La medida cautelar a la que hizo lugar el juez federal Javier Pico Terrero para que se le administrara a Oscar Jorge García Rúa dióxido de cloro por vía intravenosa, a partir de la prescripción de su médico, llevó este martes a 45 sociedades científicas a pronunciarse en repudio de esta decisión judicial, a avalar el accionar del sanatorio, que se negó a suministrarle la sustancia, y a advertir que es un tratamiento no autorizado por la ANMAT.

«Se puede hablar de un accionar temerario por parte del profesional médico, que prescribió una sustancia química que no demostró ningún beneficio», dijo a Télam el médico toxicólogo Sergio Saracco.

Integrante del comité científico de la Asociación Toxicológica Argentina (ATA), el médico mendocino explicó que tampoco cumple con otro requisito básico de cualquier medicamento, que es la seguridad, por eso no está autorizada para uso en humanos.

> **SERGIO SARACCO: «El dióxido de cloro es un potente oxidante que actúa desnaturalizando las proteínas sobre las que toma contacto, de manera general e inespecífica»**

De allí su utilidad como desinfectante de superficies inertes, porque si se aplica sobre cualquier estructura que tenga composiciones proteicas -sea un virus, una bacteria o una célula-, las destruye. Pero al ser administrado a un organismo vivo, daña las estructuras orgánicas con las que toma contacto, como las mucosas en caso de ser ingerido, pudiendo causar cuadros de esofagitis, gastritis erosiva y también náuseas, vómitos o diarreas.

«A su vez, cuando pasa al torrente sanguíneo produce una destrucción de los glóbulos rojos o incapacitarlos para transportar oxígeno», explicó. Aplicado de manera endovenosa a un paciente crítico por Covid 19, esta sustancia "agravará aún más su problema de oxigenación".

«Si en la necropsia se detecta que hubo hemólisis o metahemoglobinemia, habría una causalidad directa entre suministro de esta sustancia y el compromiso de la oxigenación», dijo.

Respecto al argumento del abogado patrocinante, en el sentido de que se trata de un compuesto registrado en la ANMAT, Saracco explicó que también lo son sustancias como la lavandina, la soda cáustica o el silica gel, que no se pueden prohibir por sus múltiples utilidades, pero en todos los casos se trata de sustancias que resultan tóxicas para su ingestión.

Saracco explicó que, en el caso de pacientes desahuciados, puede suceder que los familiares presionen a los médicos para que le suministre sustancias no autorizadas que parecen soluciones mágicas, pero que terminan siendo un engaño motorizado por intereses económicos.

«Si fuera tan exitoso, ya lo estarían promoviendo o industrializando los laboratorios que no dejarían escapar este negocio», sostuvo.

<br/>

# **«No está aprobada como medicamento»**

Es lo que sostiene la jefa de la Unidad de Toxicología del Hospital Gutiérrez, Elda Cargnel. La profesional recalcó que, además de ser una sustancia no aprobada como medicamento y cuya aplicación en pacientes con Covid no tiene ningún asidero científico, hay que tener en claro que se trata de un oxidante y no de un oxigenante, es decir, que en vez de favorecer la oxigenación, la dificulta.

«Entiendo que la familia está desesperada, pero hay que desalentar este tipo actitudes porque solo conducen a mayor caos, habiendo hoy ya herramientas y experiencia en los médicos de terapia en el manejo de estos pacientes», dijo.

Cargnel cuestionó además el rol del Poder Judicial, y cuestionó con qué aval científico el juez tomó la decisión, habiendo antecedentes de muertos por la ingesta de esta sustancia.

«Me produce mucha preocupación porque están induciendo a utilizar una herramienta no aprobada de manera coercitiva», agregó.

Por otro lado, Cargnel se lamentó de que, en estos casos, los médicos y centros de salud tengan que afrontar la amenaza cruzada de demandas por homicidio culposo o desobediencia por no aplicar el dióxido de cloro ordenado por vía judicial pero también, eventualmente después, por haberlo aplicado con resultados negativos.

> **TOXICÓLOGA ELDA CARGNEL: «A los médicos nos hacen juicio de mala praxis, ¿y la praxis jurídica?»**

Por su parte, Saracco consideró que el médico no corre riesgos si su accionar se ve guiado siempre por el principio de la seguridad para el paciente y está respaldado por evidencia científica, ya que «cuando se haga la investigación, va a quedar a la vista la verdad».

<br/>

# **La internación de Oscar Jorge García Rúa**

En tanto, el abogado de García Rúa anunció que radicarán una denuncia en el fuero penal para que se investigue la posible comisión del delito de homicidio culposo, pero también por la desobediencia en la que incurrió el sanatorio Otamendi.

En declaraciones a C5N, el letrado Martín Sarubbi explicó que todo comenzó a fines de diciembre cuando su cliente de 93 años, fue hospitalizado por coronavirus.

> **ABOGADO SARUBBI: «El cuadro era crítico y el médico neurocirujano Dante Converti recomendó como método paliativo y humanitario, la realización de un tratamiento con ibuprofeno inhalado y dióxido de cloro».**

En ese contexto, su estudio presentó un recurso de amparo en el fuero Civil, Comercial y Federal N° 7 que obtuvo una resolución positiva, pero como el sanatorio Otamendi se negó a realizar la práctica, hubo que ampliar el recurso para autorizar al doctor Converti a efectuarla.

«Una vez que se hizo, el cuadro del paciente evolucionó muy favorablemente, pero finalmente falleció, no a causa de Covid, sino por una infección inhospitalaria», dijo el abogado, aunque las autoridades del Otamendi no se manifestaron al respecto.

<br/>

# **Denuncias cruzadas**

En tanto, se conoció esta mañana que Pico Terrero será denunciado ante el Consejo de la Magistratura, y que fue denunciado penalmente esta tarde por el abogado Vadim Mischanchuk.

La denuncia, presentada ante el Juzgado Nacional en lo Criminal y Correccional N° 58, acusa a Pico Terrero por intromisión en decisiones que son ajenas al derecho y propias de la ciencia médica, al haber admitido como válida la medida cautelar.

La presentación pide que se investigue la posible comisión de una mala praxis jurídica por intromisión en decisiones que son ajenas al derecho y propias de la ciencia médica.

Según la denuncia, la mala praxis jurídica pudo haber tenido injerencia en el fallecimiento de García Rúa «a partir de la orden de aplicar tratamientos médicos no autorizados por la Anmat, sin estudios científicos de eficacia comprobada y sin ensayos clínicos conforme lo establece la legislación vigente en nuestro país».

Por otra parte, otro juez rechazó en segunda instancia una acción de amparo similar en Salta y otro tanto había pasado hace dos meses en Ushuaia.

A inicios de agosto pasado, la Anmat relevó que el dióxido de cloro no cuenta con estudios que demuestren su eficacia y no posee autorización alguna por parte de este organismo para su comercialización y uso.

Al respecto, Sarubbi reseñó que la Anmat prevé un mecanismo de excepción para tratamientos paliativos, que es un trámite administrativo que no se pudo realizar por la gravedad del cuadro del paciente fallecido, por eso se acudió a la Justicia.