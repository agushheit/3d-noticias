---
category: Agenda Ciudadana
date: 2021-11-10T06:00:51-03:00
thumbnail: https://assets.3dnoticias.com.ar/ALBERTAAAA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: En medio de los reclamos por mayor seguridad, el Frente de Todos prepara
  un cierre de campaña "austero"
title: En medio de los reclamos por mayor seguridad, el Frente de Todos prepara un
  cierre de campaña "austero"
entradilla: '"El acto del Frente de Todos sigue firme", resaltaron fuentes oficiales,
  luego de versiones de que Juntos por el Cambio podría suspender el acto en el conurbano
  por el crimen del kiosquero en el partido de La Matanza.

'

---
En medio de los reclamos por mayor seguridad tras el crimen del kiosquero de Ramos Mejía, en Casa Rosada indicaron hoy que el acto de cierre de campaña del Frente de Todos "sigue firme" para el próximo jueves en el partido bonaerense de Merlo.

Fuentes partidarias precisaron que "el acto de Merlo se va a hacer el jueves, pero será muy austero", y puntualizaron: "Va a ser un cierre enfocado a la provincia de Buenos Aires".

"En un principio estaba pensado como un cierre nacional, donde estuvieran todos los gobernadores del espacio, pero vamos a hacer un acto austero. Claramente no hay un clima festivo", confiaron a Noticias Argentinas.

En tanto, altas fuentes de la Casa de Gobierno afirmaron que desde Ejecutivo nacional expresan su "solidaridad con los familiares y amigos de la víctima".

No obstante, consideraron que el problema de la inseguridad es "un tema desde hace décadas", pero aclararon que "se trabaja para resolverlo".

Asimismo, recordaron que el Ejecutivo "viene trabajando en este problema", motivo por el cual se enviaron mayores recursos al Conurbano bonaerense.

"La persona fue detenida inmediatamente. Esperamos que la Justicia haga lo que tiene que hacer. El Gobierno no suelta presos. Todo lo define la Justicia", enfatizaron altas fuentes de la coalición gobernante.

Por su parte, el ministro de Seguridad Nacional, Aníbal Fernández, afirmó esta mañana que casos de inseguridad suceden "en todos los lugares del mundo".

"No tenemos ni mano dura ni mano blanda, no le vamos a pegar a la gente ni tirarle escopetazos", resaltó el funcionario nacional en declaraciones a la prensa al ingresar al Ministerio.

En esa línea, Aníbal Fernández agregó: "Nunca voy a estar del lado de la represión. Estuve cinco años en la fuerza y me fui sin un muerto".

A cinco días de las elecciones legislativas, en Casa Rosada reina un "moderado optimismo" respecto de revertir el resultado electoral de las PASO, pese a que fuentes cercanas a las socios de la coalición gobernante consideran que "la derrota electoral es irreversible".

En el sprint final de la campaña electoral, el jefe de Estado volverá a recorrer mañana un nuevo distrito del Conurbano bonaerense.

"Hay chances de revertir el resultado en la provincia de Buenos Aires. Desde las PASO intensificamos las recorridas para escuchar las demandas de los bonaerenses", señalaron a Noticias Argentinas fuentes partidarias.

Por último, afirmaron que son "conscientes de que falta mucho por hacer", pero destacaron: "Necesitamos el voto de confianza de quienes nos eligieron en 2019".