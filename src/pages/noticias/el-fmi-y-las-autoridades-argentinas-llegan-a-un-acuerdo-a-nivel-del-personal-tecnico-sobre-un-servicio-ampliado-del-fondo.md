---
category: Estado Real
date: 2022-03-04T09:33:09-03:00
thumbnail: https://assets.3dnoticias.com.ar/packs-gd70de83f5_1280.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: El FMI y las Autoridades Argentinas Llegan a un Acuerdo a Nivel del Personal
  Técnico sobre un Servicio Ampliado del Fondo
title: El FMI y las Autoridades Argentinas Llegan a un Acuerdo a Nivel del Personal
  Técnico sobre un Servicio Ampliado del Fondo
entradilla: Un equipo del Fondo Monetario Internacional (FMI), encabezado por Julie
  Kozack, Directora Adjunta del Departamento del Hemisferio Occidental, y Luis Cubeddu,
  Jefe de Misión para Argentina, emitió un comunicado.

---
 Un equipo del Fondo Monetario Internacional (FMI), encabezado por Julie Kozack, Directora Adjunta del Departamento del Hemisferio Occidental, y Luis Cubeddu, Jefe de Misión para Argentina, emitió hoy la siguiente declaración:

“El personal del FMI y las autoridades argentinas han llegado a un acuerdo a nivel del personal técnico sobre las políticas económicas y financieras respaldadas por un Acuerdo del Servicio Ampliado del Fondo (SAF) de 30 meses. El SAF, con acceso solicitado de DEG 31.914 millones (equivalente a US$ 45.000 millones o 1000 por ciento de la cuota), tiene como objetivo proporcionar a Argentina apoyo presupuestario y de balanza de pagos para abordar los desafíos económicos más apremiantes del país y mejorar las perspectivas de todos los argentinos mediante la implementación de medidas diseñadas para promover el crecimiento y la protección de programas sociales esenciales.

El acuerdo a nivel del personal técnico aún está sujeto a la aprobación del Directorio Ejecutivo del FMI, que ha sido informado de manera informal sobre los elementos del programa propuesto. Se espera que el Directorio Ejecutivo analice la solicitud del programa respaldado por el FMI después de que el Congreso de la Nación Argentina apruebe el programa económico y financiero incorporado en el Memorándum de Políticas Económicas y Financieras y los documentos relacionados que las autoridades compartirán con los legisladores. Esta consideración legislativa es requerida por el derecho interno argentino.

Los profundos desafíos socioeconómicos de Argentina se han visto exacerbados por la pandemia global. El personal técnico del FMI y las autoridades argentinas han llegado a un acuerdo sobre un programa económico pragmático y realista, con políticas creíbles para fortalecer la estabilidad macroeconómica y empezar a mejorar las condiciones necesarias para comenzar a atender los profundos desafíos para un crecimiento sostenible en Argentina.

El programa busca abordar de manera duradera la alta inflación persistente a través de una estrategia múltiple que involucra una reducción del financiamiento monetario del déficit fiscal y un nuevo marco para la implementación de la política monetaria para generar tasas de interés reales positivas para respaldar el financiamiento interno, que junto con otras medidas, ayudará a promover una reducción continua de la inflación a lo largo del tiempo.

Igualmente importante será el énfasis del programa en mejorar de manera creíble las finanzas públicas. Esto se basará en un conjunto equilibrado de políticas de ingresos—con énfasis en la progresividad, la eficiencia y el cumplimiento tributario— al igual que políticas de gastos, que reduzcan los subsidios energéticos no focalizados y reorienten el gasto hacia inversiones sociales y de infraestructura más productivas, para fortalecer la sostenibilidad de la deuda y al mismo tiempo apoyar la recuperación.

El programa también buscará fortalecer la balanza de pagos de Argentina a través de políticas que apoyen la acumulación de reservas y las exportaciones netas, y que allanen el camino para un eventual reingreso de Argentina a los mercados internacionales de capital. Dichas políticas incluirán las políticas monetarias y fiscales prudentes ya descritas, así como políticas para mantener un tipo de cambio efectivo real competitivo en el contexto del régimen de paridad móvil.

Además, el programa incluirá elementos para mejorar el crecimiento y la resiliencia a través de políticas para movilizar el ahorro interno, fortalecer aún más la gobernabilidad y la transparencia, y fomentar la inclusión laboral, de género y financiera. También se tomarán medidas para alentar la inversión y promover la sostenibilidad y la eficiencia de los sectores económicos estratégicos, incluida el energético. En conjunto, tales medidas serán fundamentales para comenzar a abordar las restricciones para un crecimiento más sostenible e inclusivo. Por último, también se espera que el programa catalice apoyo adicional financiero internacional.

Esperamos continuar nuestro trabajo con las autoridades para ayudar a todos en Argentina”.