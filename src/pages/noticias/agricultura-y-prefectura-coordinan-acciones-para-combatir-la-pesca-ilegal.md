---
layout: Noticia con imagen
author: .
resumen: Pesca Ilegal
category: El Campo
title: Agricultura y Prefectura coordinan acciones para combatir la pesca ilegal
entradilla: Trabajo interinstitucional y anticipatorio para luchar contra la
  problemática en defensa de los recursos estratégicos del Mar Argentino.
date: 2020-11-05T13:08:25.532Z
thumbnail: https://assets.3dnoticias.com.ar/pesca-ilegal.jpg
---
En el marco del fortalecimiento de las acciones de control sobre la actividad pesquera de buques de bandera extranjera, que lleva adelante el Ministerio de Agricultura, Ganadería y Pesca a través de la Subsecretaría de Pesca y Acuicultura, el director Nacional de Coordinación y Fiscalización Pesquera, Julián Suarez, mantuvo una reunión con el Prefecto Nacional Naval, Mario Farinón.

Durante el encuentro se abordaron temas relacionados a la defensa de la soberanía y los recursos estratégicos en el área contigua a la Zona Económica Exclusiva Argentina, de cara a la nueva temporada de pesca de calamar y con el seguimiento de buques posicionados cerca de la milla 201.

"Desde el inicio de la gestión, en articulación con la Prefectura Naval Argentina y la Armada Argentina, hemos reforzado el patrullaje y el control para la protección de nuestra Zona Económica Exclusiva.

Actualmente nos encontramos intensificando estas acciones anticipándonos a la zafra del calamar que está próxima a iniciarse", destacó Suarez.

"La zafra del calamar de este año ha tenido resultados muy positivos ubicándose los desembarques en un 76% mayor con relación al año 2019 y eso ha contribuido sustancialmente a que el sector pesquero exporte al 31 de agosto del corriente por 1.106 millones de dólares. Aproximadamente el 90% de la facturación del sector se destina a la exportación generando así el ingreso de divisas al país. La actividad pesquera genera miles de puestos de trabajo de forma directa e indirecta y el proceso productivo desarrolla la agregación de valor en origen y por todo ello se vuelve imprescindible la protección de los recursos de nuestro mar", destacaron desde la Subsecretaría.

![](https://assets.3dnoticias.com.ar/pesca-ilegal1.jpg)

Asimismo, durante la reunión se abordó la modificación del Régimen Federal Pesquero (Ley N° 24.922) que entró en vigor el pasado 1 de octubre, el cual establece, entre otros puntos destacables, montos actualizados de las multas cada seis meses. A partir de entonces, y por modificación del artículo 51 bis de la mencionada ley, la sanción de multa se establece en unidades de valor denominadas Unidades Pesca (UP), equivalentes al precio de un (1) litro de combustible gasoil grado 2. En este sentido, vale enfatizar que en la presente gestión se pasó de un monto máximo de multa de 10 millones de pesos a 3 millones de Unidades Pesca, equivalentes hoy en día a 162 millones de pesos según el valor actual del mencionado combustible.

En este marco, se subraya, asimismo, que en mayo de este año la captura y aplicación de multas correspondientes a 140 millones de pesos más gastos de procedimiento a 3 buques extranjeros, dos de procedencia china y una portuguesa, con la intervención de Prefectura Naval Argentina y Armada Argentina. Esto marca un hito en el accionar sobre la defensa de la soberanía, teniendo en cuenta que se produjeron dentro del período de una sola zafra y más aún si se tiene en cuenta que en los 4 años anteriores solo se capturaron 2 buques extranjeros.

Del encuentro que tuvo lugar en el Edificio Guardacostas, sede de la Prefectura Naval Argentina, en la Ciudad Autónoma de Buenos Aires, participaron además el director de Operaciones de Prefectura y el jefe Policía Auxiliar Pesquero.

La reunión, a su vez, forma parte de los objetivos planteados en la Mesa Interministerial de Pesca, encabezada por el jefe de Gabinete de Ministros de la Nación, Santiago Cafiero, con la participación activa de las carteras nacionales de Agricultura, Ganadería y Pesca; de Defensa y de Seguridad.