---
category: Agenda Ciudadana
date: 2021-04-15T07:35:02-03:00
thumbnail: https://assets.3dnoticias.com.ar/alberto-fernandez.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Diario Uno
resumen: 'Amba: restringen la circulación y suspenden las clases'
title: 'Amba: restringen la circulación y suspenden las clases'
entradilla: La medida fue anunciada este miércoles por la noche por el presidente
  de la Nación, Alberto Fernández. La gastronomía estará cerrada por la noche

---
El presidente Alberto Fernández anunció hoy la restricción de circulación entre las 20 y las 6 en el Área Metropolitana de Buenos Aires (AMBA); la suspensión de las actividades sociales, culturales, deportivas, religiosas y recreativas en lugares cerrados, el cierre de comercios desde las 19 y la suspensión presencial de clases en los tres niveles educativos.

La suspensión de las clases se producirá a partir del lunes próximo y hasta el 30 de abril, mientras las medidas con impacto en el resto de las actividades regirán desde pasado mañana, viernes, y por dos semanas.

En un mensaje grabado en la Quinta Presidencial de Olivos, donde Fernández mantendrá hasta el jueves su aislamiento por el coronavirus, el mandatario dijo que las clases, en los tres niveles educativos, "se suspenderán desde el lunes por dos semanas" en la ciudad de Buenos Aires y el conurbano bonaerense.

En tanto las actividades gastronómicas funcionarán en modalidad de "entrega a domicilio" desde las 19, informó el mandatario sobre las nuevas medidas, que saldrán en otro Decreto de Necesidad y Urgencia (DNU).

"En el AMBA estamos viviendo la mayor velocidad de aumento de casos desde el inicio de la pandemia. Por lo tanto, es nuestra obligación tomar medidas adicionales y convocar a la población a un cambio, para lograr que avance el plan de vacunación y evitar la saturación del sistema de salud", expresó Fernández.

El Presidente contó que "el mayor riesgo de transmisión se produce en las actividades sociales y recreativas nocturnas, donde no hay dos metros de distancia, se producen aglomeraciones, se usa escasamente el barbijo y también en espacios cerrados sin ventilación adecuada".

Y remarcó que el AMBA "constituye un único aglomerado urbano, uno de los más poblados de América", que "tiene una única realidad epidemiológica, con un gobernador y un jefe de gobierno y dos docenas de municipios".

Alertó que "estamos monitoreando día a día evolución de la pandemia" y "hace un mes, acumulamos 45.498 casos de contagios, mientras que la semana que acaba de terminar acumulamos 122.468 casos y la que va transcurriendo en este momento seguramente va a superar esta cifra".

Fernández consideró que el contagio de Covid "no está en las fábricas, no está centralmente en los negocios que con distancia social pueden atender a los clientes", sino "en las reuniones sociales donde la gente se distiende y en ese momento de distracción, de esparcimiento, es mucho más fácil contraer el virus".

El mandatario apuntó a que los Gobiernos provinciales y municipales "fiscalicen las decisiones" que toma el Gobierno central "y hagan cumplir las decisiones", porque "tal como ocurrió con las medidas dispuestas hace una semana, el resto de las jurisdicciones puede adherir".

"Espero que los gobernadores y los intendentes que entienden que deben acompañarme en este momento difícil, lo hagan", pidió Fernández, y agregó: "Lo que más necesitamos es que ustedes, argentinos, argentinas, entiendan que el cuidado individual es central, no solo para que no nos contagiemos nosotros, sino para no contagiar al otro".

"Seguimos negociando y hablando con todos los proveedores de vacunas para cerrar acuerdos y acelerar el acceso a ellas", manifestó el Presidente, y subrayó que este fin de semana "estarán llegando más vacunas para continuar con el Plan de Vacunación".

Fernández destacó que, al igual que en 2020, el Gobierno nacional "trabaja en la reorganización del sistema de salud para dar prioridad a la atención de la enfermedad ante el ascenso de casos de coronavirus en el marco de la segunda ola" y enumeró que el año pasado "las camas de terapia intensiva pasaron de 8.521 a 12.501, lo que representó un incremento del 47 por ciento de la capacidad del sistema para absorber la demanda aumentada de cuidados intensivos ante la pandemia".

Además, reseñó que, en articulación con el Ministerio de Obras Públicas, se instalaron 12 hospitales modulares de emergencia y 19 Centros Modulares Sanitarios "en lugares estratégicos del país para fortalecer la respuesta sanitaria", y más de cinco millones de personas "ya recibieron la primera dosis" en el marco del plan de vacunación que lleva adelante el Gobierno nacional para combatir el coronavirus.

El cumplimiento de las medidas "estará a cargo de las fuerzas federales", informó Fernández, mientras que las Fuerzas Armadas colaborarán "con la atención sanitaria donde sea necesario", consignó.

"Como Presidente de la Nación preservaré la salud de todos los argentinos y todas las argentinas y procuraré su cuidado al máximo", afirmó, y subrayó: "No me mueve ningún interés político en lo que hoy estoy proponiéndoles, no estoy acá planteando estas cosas para ver de qué modo lucro políticamente, lo único que me importa es preservar la salud de los argentinos".

El Presidente enfatizó que "la distención social es un gesto de irresponsabilidad magnífico, que se vuelve intolerable" y concluyó: "Como dice el papa Francisco, nadie se salva solo".

Para delinear las medidas Fernández se reunió esta tarde en Olivos con el jefe de Gabinete, Santiago Cafiero, y la ministra de Salud, Carla Vizzotti, en coordinación con la secretaria Legal y Técnica, Vilma Ibarra, quien permaneció en Casa Rosada.