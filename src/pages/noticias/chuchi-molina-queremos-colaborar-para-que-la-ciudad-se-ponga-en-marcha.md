---
category: La Ciudad
date: 2021-07-06T08:20:52-03:00
thumbnail: https://assets.3dnoticias.com.ar/chuchi-jose.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: 'Chuchi Molina: “Queremos colaborar para que la ciudad se ponga en marcha”'
title: 'Chuchi Molina: “Queremos colaborar para que la ciudad se ponga en marcha”'
entradilla: 'Encabezará la lista de concejales por el espacio que lidera José Corral
  en Juntos por el Cambio. '

---
Adriana “Chuchi” Molina encabezará en las próximas elecciones a concejales la propuesta de Juntos por el Cambio en el marco del espacio que lidera José Corral en la provincia. “La ciudad necesita ponerse en marcha. Para eso quiero colaborar desde el Concejo, aportando toda la energía, la fuerza, la experiencia que este momento difícil requiere. Porque Santa Fe puede estar mucho mejor”, aseguró en declaraciones a la prensa la dirigente, que estará acompañada en la lista por Carlos Pereira -que en diciembre concluye su actual mandato como concejal-, y Angeles Alvarez, proveniente de la Juventud Radical.

Exconcejal y exsecretaria de Gobierno durante una de las gestiones de José Corral como intendente de la ciudad, Chuchi señaló que “en la charla con los vecinos y vecinas, lo que surge es que vivimos momentos en los que hace falta tomar decisiones con firmeza; que la ciudad necesita un liderazgo, contar con equipos para poder planificar y generar certezas en un marco de tanta incertidumbre como las que se han generado durante la pandemia”. Y remarca: “Más allá de lo que haga el presidente o el gobernador, desde la ciudad se puede hacer mucho para aliviar la situación de nuestros vecinos y vecinas. Porque hoy la demanda es por determinaciones claras, para que el comerciante sepa a tiempo si hace o no la compra al proveedor; si el dueño del gimnasio va a poder trabajar o no la semana siguiente y bajo qué protocolos; el gastronómico si compra o no la carne para sus clientes el fin de semana, si los padres mandarán o no sus chicos a la escuela el lunes. Esos son los problemas reales y concretos en los que tiene que trabajar todos los niveles del estado”.

Chuchi Molina agrega que “algo que nos llega de todos lados, de cada barrio que visitamos, es que el Municipio debe resolver los problemas básicos: cambiar focos, cortar yuyos, la limpieza, los controles de tránsito, el transporte público. No puede quedar la ciudad a la buena de Dios. Además hay una gran preocupación por la seguridad, que es un tema central e inquietante y que debe ser abordado de manera integral”. 

Y para enfrentar estos desafíos cuenta con el acompañamiento del equipo de José Corral: “Chuchi es una líder natural, empática, comprometida, con experiencia y trayectoria. Fue concejal, secretaria de gobierno, es profesora en la universidad y una referente en la lucha de las mujeres, una persona de diálogo, siempre activa, que conoce la ciudad y las demandas de los vecinos y vecinas”, asegura por su parte el dos veces intendente de la capital provincial.

**Planificar para crecer**

Para Chuchi “hay que pensar ya en la ciudad pospandemia. Los paradigmas urbanos están cambiando en todo el mundo a un ritmo muy veloz. Los usos de los espacios están cambiando. Las costumbres, también. Hay cambios que llegaron para quedarse, otros que se aceleraron. La ciudad tiene que estar preparada para eso y nosotros estamos trabajando en ese sentido”. 

Chuchi subrayó que el equipo del que es parte ya ha enfrentado desafíos similares y ha proyectado y concretado transformaciones en la ciudad: “Los vecinos nos dicen que a lo largo de los 12 años de gobierno que tuvimos realmente la ciudad cambió para bien, avanzó, se creció mucho. Se logró obviamente desde la gestión y en conjunto con los vecinos y vecinas de la ciudad, atendiendo sus demandas y necesidades. Durante décadas nos dijeron que Santa Fe era chata y gris, una ciudad que no iba a crecer, que siempre sería azotada por las inundaciones, o que solo tenía destino de capital administrativa. Sin embargo, se hicieron obras de infraestructura gracias a las cuales hoy la ciudad está mejor preparada para afrontar crecientes de los ríos o lluvias extraordinarias, e incluso Santa Fe ha sido reconocida internacionalmente por eso”, recordó.

También apuntó: “Se nos dijo también durante años que Santa Fe no podía tener desarrollo industrial: hoy tenemos Los Polígonos, un área destinada al desarrollo industrial; se decía que Santa Fe no era una ciudad turística, sin embargo, se transformó en una ciudad de eventos, con actividades todas las semanas. Y también hubo acciones para generar oportunidades de progreso para todos, y en ese marco se crearon las escuelas de trabajo y los jardines municipales”, repasó.