---
category: El Campo
date: 2020-12-27T12:39:24Z
thumbnail: https://assets.3dnoticias.com.ar/2712-campo3.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Infobae'
resumen: 'Sequía, nevadas e incendios:  un año de extremos climáticos'
title: 'Sequía, nevadas e incendios:  los productores agropecuarios cierran un año
  de extremos climáticos'
entradilla: El 2020 estuvo marcado por la falta de lluvias, intensas nevadas e incendios.
  La campaña de trigo sufrió duras pérdidas y se plantea un escenario dificultoso
  para la soja y el maíz por el fenómeno climático de La Niña.

---
Sin lugar a dudas, el clima durante 2020 no fue un gran aliado del campo argentino, donde se conjugaron una fuerte y larga sequía en la franja central y norte del área agrícola nacional que condicionó de sobremanera la campaña de trigo, a lo que se sumaron heladas e incendios, y copiosas nevadas en la zona cordillerana y de la meseta patagónica, que afectó a la ganadería de la zona.

A este escenario de «extremos climáticos» y de déficit hídrico en vastas zonas del país, se le sumó el fenómeno climático de La Niña, con precipitaciones por debajo de la media histórica, que está llegando a su pico y que se prevé, se extenderá en los primeros meses del año. Este panorama podría representar un duro escollo para la campaña gruesa que se está desarrollando, en especial en lo que respecta a soja y maíz, cuyos rendimientos podrían verse afectados por la falta de agua.

Esta semana, los integrantes de la Guía Estratégica para el Agro de la Bolsa de Comercio de Rosario (BCR), plantearon que «con una Niña muy seca hasta marzo, los cultivos deberán transitar un verano con mucha volatilidad de lluvias, que serán similares al nivel de precipitaciones que se registró en la primavera de 2019, que fue la más seca de los últimos diez años. El verano comenzó, y si bien el aporte de agua trajo tranquilidad a los productores de maíz, esto no quiere decir que habrá un cambio de tendencia, ya que el clima seco se impone».

Por su parte, el director del Centro de Investigación de Recursos Naturales (CIRN) del Instituto Nacional de Tecnología Agropecuaria (INTA), Pablo Mercuri, consideró que lo «más marcado para el campo son los extremos climáticos, que son los verdaderos limitantes de techos productivos que podemos alcanzar. Entre ellos los más destacados del año fueron la sequía y las grandes nevadas en el sur».

En este sentido, subrayó que«si miramos lo que es todo el país, el 2020 ha estado surcado por un profundo déficit hídrico y el otro evento son las nevadas en toda la Patagonia hasta la mitad de Mendoza, que sin embargo no deja de ser un año deficitario en cuanto al agua. En altas cumbres, del Aconcagua hacia al norte hubo muy poca nieve y toda el agua de los oasis productivos del oeste se basa en agua de deshielos. En el este, el río Paraná está en una situación de mayor persistencia de récord de bajante, que se mantiene hace un año y medio».

Así, Mercuri destacó que «este año la variabilidad climática se expresó con un marcado déficit hídrico con un promedio de un 20% al 40% de la zona central con menos lluvias que lo normal, agravándose en el norte del país. Ahí estuvo el 50% del déficit». De los principales cultivos que se siembran a lo ancho y largo del país, el de trigo fue uno de los más castigados por la falta de agua sobre la franja central y el norte del área agrícola.

La implantación del cultivo de invierno por excelencia del país comenzó con buenos perfiles de humedad en los pisos, lo que hacía prever una expansión interanual de la siembra con respecto a la campaña pasada y alcanzar un récord productivo. De hecho, la Bolsa de Comercio de Rosario estimaba por esos meses más de 7 millones de hectáreas dedicadas al cereal y una producción que podría haberse ubicado entre los 21 o 22 millones de toneladas.

No obstante el optimismo inicial, la falta de lluvias se empezaron a profundizar a partir de marzo en las regiones ya mencionadas, que en algunos casos se extendieron hasta octubre, y los cultivos comenzaron a sufrir el estrés hídrico, con caída en sus rendimientos y vastos lotes que no se pudieron sembrar, se perdieron o fueron abandonados. De hecho, la siembra nacional culminó en un contexto donde se estaba desarrollando la peor sequía en 10 años.

Productores del norte de la Argentina, agrupados en la Asociación Civil de Productores Agrícolas y Ganaderos del Norte (Apronor), dijeron que «la campaña de cultivos de invierno arrancó con un perfil de humedad de suelo muy pobre. El panorama pintaba para desastre pero, como siempre decimos en el campo, la esperanza es lo último que se pierde. Con ese sentimiento transitamos el invierno, con la ilusión de que la naturaleza revierta el mal comienzo, cosa que no sucedió».

![](https://assets.3dnoticias.com.ar/2712-campo2.jpg)

Además, comentaron: «la implantación de la mayoría de los lotes de Tucumán y zonas de influencia se hace fundamentalmente para mantener los campos limpios de malezas y evitar un control químico en invierno. La campaña está entrando en su tramo final y hablar de cosecha va a ser solo para algunos privilegiados productores, la gran mayoría solo aspira a recuperar algo de semilla para poder volver implantar en la próxima campaña».

A la fecha, y con el 80% del trigo ya recolectado, la BCR espera una producción de 16,5 millones de toneladas a recolectarse en total de 6,5 millones de hectáreas aptas. A modo de ejemplo, y para poder apreciar la magnitud de la sequía, la producción en el norte argentino retrocedió 69,2% respecto al ciclo anterior y en Córdoba se prevé una cosecha 60% menor, para constituirse en la peor de la década.

La agricultura no fue la única que fue afectada por el clima. A mediados de julio se produjeron fuertes nevadas en la Patagonia que llegaron a depositar 1,5 metros de nieve en una sola tormenta y se registraron temperaturas de 20 grados bajo cero. Esto impactó de lleno en la ganadería ovina, mientras que en el norte, la extrema sequía no solo dejó a los animales sin pasturas, sino que también los incendios pusieron en jaque a los productores vacunos.

<br/>

<span style="font-family: helvetica; font-size: 1.3em; font-weight: 800;">**«La Niña» y el desafío para la gruesa**</span>

Si le faltaba algo al duro año climático del campo argentino, era el advenimiento del fenómeno climático de La Niña, que alcanzará su pico en las próximas semanas por el enfriamiento del océano Pacífico y que promete extenderse en los próximos meses. Esto supone un doble desafío para los cultivos de verano, en especial para la soja y el maíz, ya que parten de un escenario de déficit hídrico que podría extenderse.

A la fecha, la siembra de soja se ubica en el 77% de los 17,2 millones de hectáreas proyectadas para el cultivo, mientras que la implantación de maíz se posiciona en el 61% de los 6,2 millones de hectáreas estipuladas para el cereal, según la Bolsa de Cereales de Buenos Aires (BCBA). En ambos casos se están llevando adelante la incorporación de variedades tardías y la necesidad de lluvias se vuelve imperante en el corto plazo tanto para continuar con los trabajos, como para la definición de rendimientos de los lotes tempranos.

![](https://assets.3dnoticias.com.ar/2712-campo 1.jpg)

Al respecto, Sofía Corina, especialista de la Bolsa de Rosario, señaló en uno de sus últimos informes publicados, que «el maíz en la región centro deberá superar las escasas lluvias registradas y una Niña que no se ve tan moderada, tal como se esperaba en los últimos meses. El maíz recibió un alivio la semana pasada, y llegó justo a tiempo cuando el 40% de la región núcleo estaba en floración. Fue un alivio importante, sobre todo para Córdoba».

Y agregó: «Venimos con una deuda hídrica de invierno y una primavera de lluvias escasas, y al atravesar este verano, la necesidad de agua se acrecienta cada vez más. Sin embargo, se pasaría a tener una Niña ya instalada: no será tan moderada».

Pablo Mercuri del INTA, también se refirió a la problemática de La Niña: «El pico de enfriamiento (del Pacífico) siempre ocurre hacia fin de año y principios del otro. En este momento, acorde a los modelos que miden las temperaturas del océano Pacífico y los modelos que simulan cómo va a ser ese enfriamiento, estaría ocurriendo el pico y después empezaría a debilitarse, con perspectivas de que en otoño estemos en una fase neutral. La expectativa es que a medida que se debilite el enfriamiento tengamos más chances de precipitaciones», aseguró.

«El contexto en general, a medida que termine cada período de los cultivos, es que tengamos déficit. O sea, que no se haya cubierto la cantidad de milímetros óptimos para el desarrollo del cultivo. Va a haber zonas donde se alcance el potencial, pero la perspectiva climática es que haya un déficit en la cantidad de milímetros esperados», advirtió Mercuri.

Sin embargo, el especialista planteó la posibilidad de que con el correr del tiempo se puedan producir lluvias durante el verano, debido a que, una vez alcanzado el pico de enfriamiento del Pacífico, comience a calentarse, junto con otros factores que podrían propiciar las precipitaciones. En ese sentido es posible que en el verano haya más eventos de precipitaciones que los que están ocurriendo ahora, debido a que posiblemente se debilite la tendencia de enfriamiento y que existen otros forzantes océano-atmosféricos que podrían interactuar y favorecer el desarrollo de lluvias, por ejemplo el calentamiento del Atlántico Sur, lo que favorece el ingreso de aire húmedo.