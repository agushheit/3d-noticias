---
category: Agenda Ciudadana
date: 2021-03-12T07:06:49-03:00
thumbnail: https://assets.3dnoticias.com.ar/supermercados.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de INDEC
resumen: La inflación subió un 3,6% en febrero y llegó a un acumulado de 7,8% en 2021
title: La inflación subió un 3,6% en febrero y llegó a un acumulado de 7,8% en 2021
entradilla: Así lo informó el Instituto Nacional de Estadística y Censos (Indec).
  De esta forma, el nivel general del Índice de precios al consumidor acumuló un alza
  de 7,8% en los dos primeros meses del año.

---
El Índice de Precios al Consumidor (IPC) aumentó 3,6% durante febrero y así acumuló un alza de 7,8% en los dos primeros meses del año, informó el Instituto Nacional de Estadística y Censos ( Indec).

Desde el Palacio de Hacienda destacaron que el IPC de enero fue menor al de los últimos dos meses previos, lo que llevó a que la variación interanual quedara en 40,7%, muy por debajo del 50,3% interanual registrado en el mismo mes de 2020.

Este retroceso respondió principalmente a los precios de los productos regulados (electricidad, gas, etc), que aumentaron 2,2% mensual (vs. 5,1% mensual en enero), con una fuerte desaceleración de Comunicaciones (1,8% vs. 15,1% en enero).

A su vez, los productos estacionales se mantuvieron en 3,1% mensual (vs. 3,0% en enero), con subas en verduras, indumentaria y hoteles y restaurantes, por la temporada de verano.

En tanto, la inflación Núcleo subió 4,1% mensual (vs. 3,9% en enero).

![](https://assets.3dnoticias.com.ar/indec.jpg)

Por división, impulsó la desaceleración de la inflación en febrero el rubro Alimentos y Bebidas, que recortó su tasa de aumento hasta 3,8% mensual contra el 4,8% promedio registrado en enero.

Dentro del rubro se destaca la desaceleración de Carnes y derivados hasta 2,8% mensual (vs. 7,9% en enero) por la implementación de un acuerdo con frigoríficos y de Frutas hasta 6,7% mensual (vs. 9,7% en enero).

Otros sectores que contribuyeron a la desaceleración de la inflación mensual fueron Bebidas alcohólicas y tabaco (3,6% vs. 4,5% en enero), Recreación y cultura (2,3% vs. 4,8% en enero) luego de crecer 5% mensual en promedio los últimos tres meses por apertura de actividades, y Educación (0,1% vs. 0,6% en enero).

La mayor suba del mes se dio en Restaurantes y hoteles, que volvió a crecer 5,4% mensual (igual a enero), debido a cuestiones estacionales y a la habilitación de actividades de recreación y turismo tras el ASPO.

Le siguen Transporte, que subió 4,8% mensual (vs. 4,6% en enero) por el aumento autorizado en combustibles y subas en adquisición de vehículos, y Equipamiento y mantenimiento del hogar, que aumentó 4,6% mensual (vs. 3,0% en enero).