---
category: La Ciudad
date: 2021-12-02T06:00:34-03:00
thumbnail: https://assets.3dnoticias.com.ar/procrearII.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'Procrear II: abren las inscripciones para sortear viviendas en Santa Fe
  y otras 13 provincias'
title: 'Procrear II: abren las inscripciones para sortear viviendas en Santa Fe y
  otras 13 provincias'
entradilla: Los interesados podrán anotarse hasta el próximo 20 de diciembre, y el
  lunes 27 de diciembre se realizará el sorteo. Hay unidades del Parque Federal, en
  la capital provincial, y Sunchales. 

---

El Ministerio de Desarrollo Territorial y Hábitat anunció que este jueves se abrirá la inscripción para sortear más de 900 viviendas, en Desarrollos Urbanísticos de Procrear II en 14 provincias.

 De acuerdo con un comunicado de la cartera que conduce Jorge Ferraresi, podrán inscribirse familias de Buenos Aires, Chaco, Chubut, Córdoba, Corrientes, La Pampa, Mendoza, Misiones, Neuquén, Salta, San Luis, Santa Fe, Tierra del Fuego y Tucumán.

 En esta ocasión, se habilitará la inscripción para acceder al sorteo de 902 unidades funcionales ubicadas en los predios de Bahía Blanca, Bolívar, Canning, Carmen de Patagones, Ciudad Evita, Ezeiza, Ituzaingó, Lincoln, Morón, San Antonio de Areco, San Miguel, San Nicolás, Suipacha, Tandil y Tigre (Buenos Aires).

 Asimismo, se sitúan en Resistencia (Chaco); Puerto Madryn (Chubut); Barrio Liceo, Cavanagh, Morrison, San Francisco (Córdoba); Ciudad de Corrientes (Corrientes); Santa Rosa (La Pampa); Maipú, Malargüe, Mendoza Capital, San Martín, San Rafael (Mendoza); Posadas (Misiones); Zapala (Neuquén); Tartagal (Salta); Ciudad de San Luis (San Luis); Parque Federal, Sunchales (Santa Fe); Ushuaia (Tierra del Fuego) y Yerba Buena (Tucumán).

 Los interesados podrán inscribirse hasta el próximo 20 de diciembre, y el lunes 27 de diciembre se realizará el sorteo para adjudicar viviendas disponibles para esta nueva etapa.

 La línea Desarrollos Urbanísticos de Procrear II, en el marco del programa federal Casa Propia, brinda a las familias el acceso a crédito hipotecario para la compra de viviendas nuevas en uno de los predios donde el Estado Nacional lleva adelante estos Desarrollos Urbanísticos, según se indicó. A través de este programa, además, se atiende a la diversidad de las demandas habitacionales, así como a la particularidad de cada provincia y municipio donde se lleva adelante.

 "Con estas políticas públicas estamos llevando adelante soluciones habitacionales para los argentinos y las argentinas", afirmó Ferraresi, y agregó que "de esta manera, no sólo cumplimos el sueño de la casa propia, sino que generamos trabajo en la construcción y se demandan insumos de industria nacional".