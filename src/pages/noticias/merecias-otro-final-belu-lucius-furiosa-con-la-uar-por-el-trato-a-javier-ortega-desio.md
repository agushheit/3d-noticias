---
category: Deportes
date: 2021-03-19T06:43:38-03:00
thumbnail: https://assets.3dnoticias.com.ar/desio.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de La Nación
resumen: "“Merecías otro final”: Belu Lucius, furiosa con la UAR por el trato a Javier
  Ortega Desio"
title: "“Merecías otro final”: Belu Lucius, furiosa con la UAR por el trato a Javier
  Ortega Desio"
entradilla: "“Merecías otro final”: Belu Lucius, furiosa con la UAR por el trato a
  Javier Ortega Desio"

---
La Unión Argentina de Rugby (UAR) anunció este martes que termina su relación contractual con el forward Javier Ortega Desio, quien vistió la camiseta de Los Pumas y de Jaguares. Ante la salida de su marido, la influencer Belu Lucius se descargó en redes sociales, donde apuntó al “trato” hacia su pareja y elogió su carrera deportiva hasta el momento.

“Seis años de fidelidad a Jaguares. 57 test con la celeste y blanca. Dos mundiales. Final del Super Rugby”, empezó el posteo de Lucius, quien repasó la trayectoria de Ortega Desio en el rugby argentino. Y continuó: “Solo tu familia y tus amigos saben todo lo que pusiste a un lado y resignaste todo estos años para poder jugar en ese lugar”.

**JAGUARES. EL MISTERIO POR LA SALIDA DE ORTEGA DESIO Y LA EXPLICACIÓN DE LA UAR**

El entrerriano que salió de Estudiantes de Paraná tenía contrato con la UAR hasta diciembre de 2022, pero las alarmas saltaron cuando el Head Coach de Jaguares VX, Ignacio Fernández Lobbe, lo dejó afuera de los convocados para la Superliga Americana de Rugby, que comenzó el pasado martes 16 en Chile. Ortega Desio era el más experimentado de los candidatos para el equipo pero el entrenador consideró que quería darle lugar a los más jóvenes que potencialmente pudieran jugar en un futuro en Los Pumas.

Otro antecedente llamativo fue la decisión de Mario Ledesma de no tenerlo en cuenta para Los Pumas que disputaron el Tri Nations a fines de 2020 en Australia.

Por todo esto, la instagrammer Belu Lucius manifestó su enojo y dolor en las redes. “Siempre fiel a la Unión y a sus proyectos”, lanzó mencionando explícitamente a la UAR. “Merecías otro final. Merecías otro trato. No hay más nada para decir”, concluyó la actriz y cerró el posteo con un “te amamos”.

Asimismo, varios seguidores de Lucius lanzaron comentarios de apoyo al rugbier como “¡Vamos, un grande para siempre!”, “Gracias por representarnos” o “Lejos, uno de los mejores jugadores argentinos”.

Ortega Desio, en tanto, se manifestó al respecto en redes sociales con un video en el que aclara que no se retira del rugby y agradece todos los mensajes de apoyo.

Tanto él como su esposa se muestran siempre muy abocados a la familia y se apoyan mutuamente, como el rugbier lo hizo con la instagrammer cuando fue eliminada de MasterChef Celebrity.

**El comunicado de la UAR**

Por su parte, la institución deportiva argentina informó el martes: “La Unión Argentina de Rugby comunica que Javier Ortega Desio finalizó su contrato de común acuerdo tras seis temporadas. Entre sus principales hitos, durante estos años del vínculo el forward entrerriano fue titular en la final del Super Rugby 2019 con Jaguares y disputó dos mundiales con Los Pumas (2015 y 2019)”.

Sin más detalles al respecto, el comunicado concluyó: “La UAR agradece a Javier por todos estos años compartidos con grandes momentos vividos y le desea el mayor de los éxitos para sus próximos desafíos”.