---
category: Agenda Ciudadana
date: 2021-12-08T06:15:07-03:00
thumbnail: https://assets.3dnoticias.com.ar/DIPUTADOS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: Juraron los 127 diputados electos y arranca una nueva etapa en la Cámara
  Baja
title: Juraron los 127 diputados electos y arranca una nueva etapa en la Cámara Baja
entradilla: 'La Cámara de Diputados incorporó a sus nuevos integrantes en el marco
  de una sesión preparatoria marcada por el buen clima y la camaradería entre el oficialismo
  y la oposición al momento de las juras. .

'

---
Los 127 diputados electos el pasado 14 de noviembre por los distintos partidos políticos juraron hoy por sus bancas para los próximos cuatro años, en medio de un clima distendido y de camaradería en el reciento, lo que marca el inicio de una nueva etapa en la Cámara baja.

Pasadas las 12.30, con media hora de retraso, comenzó la sesión preparatoria, con todo listo para la jura de los legisladores. Daniel Ferreyra, como el diputado de mayor edad del cuerpo, se sentó en la Presidencia hasta elegir a las autoridades.

"Espero que podamos conciliar entre todos", pidió el legislador del Frente de Todos por Jujuy, que aprovechó su momento de mayor exposición para dejar un mensaje político a la oposición y el oficialismo.

El líder del Frente Renovador, Sergio Massa, volvió a ser electo como presidente de la Cámara baja, y rápidamente regresó a su lugar en el estrado para tomar los juramentos.

La primera tanda fue integrada por el radical Facundo Manes, y luego siguieron Gerardo Milman, Danya Tavela y Fabio Quetglas, entre otros.

Los diputados de la izquierda Nicolás del Caño y Romina del Plá, por su parte, juraron por "echar al FMI" del país, en tanto que Hugo Yasky lo hizo por "los 30.000 desaparecidos y la libertad de Milagro Sala".

Por la Ciudad de Buenos Aires, Miryam Bregman y Fernando Iglesias compartieron ronda con Martín Tetaz y Carla Carrizo.

Bregman juró por los "30.000 desaparecidos y los desaparecidos en democracia", a lo que Iglesias criticó: "Espero que respetemos la fórmula de reglamento de jura que establece la Constitución".

Javier Milei tuvo su turno junto a Ricardo López Murphy, quien juró por "la libertad y la República".

Los tres diputados electos por el Frente de Todos en la Ciudad, Gisela Marziotta, Leandro Santoro y Carlos Heller, pasaron juntos para su momento de asumir.

Pasadas las 13.45, cuando ya habían pasado los 127 de todas las provincias, se procedió a la elección de autoridades.

El jefe del bloque del Frente de Todos, Máximo Kirchner, habló primero e hizo un repaso sobre los aprendizajes que dejó sesionar durante la pandemia de coronavirus, algo a lo que luego adhirió el titular del PRO, Cristian Ritondo.

Ambos le agradecieron a Massa por el trabajo durante ese momento, al igual que Mario Negri y el resto de los bloques -menos del Frente de Izquierda-, que acompañaron su designación como presidente de la Cámara de Diputados.

A su turno, la bonaerense Graciela Camaño, sentada junto al recién llegado Florencio Randazzo, acompañó a Massa, pero reclamó a se comprometa "a que todos los miércoles haya sesión".

"Estoy casi segura de que nuevamente nos vamos a enfrentar al espectáculo de todos los hombres presidiendo esta casa. Por eso, aquello por lo que decimos que venimos bregando, va a estar absolutamente ausente, nos quedamos sin discurso. De qué nos sirve ponerle la A o la E o la letra que quieran si desde este lugar no tenemos la capacidad de reconocer la cualidad políticas de nuestras compañeras mujeres. Los llamo a los bloques de la primera, segunda, tercera minoría, que por favor reflexionen y pongan una mujer", resaltó Camaño.

En la vicepresidencia primera quedó el sanjuanino del Frente de Todos, José Luis Gioja, en la segunda el mendocino del PRO Omar De Marchi y en la tercera el mendocino de la UCR Julio Cobos.

Antes de la sesión preparatoria, se desarrolló una reunión de Labor Parlamentaria, de la que participaron los jefes de bloque, entre ellos algunos recién llegados.

Milei aprovechó para sacarse fotos y empezar a conocer a los empleados de la Cámara, en tanto que Espert saludó uno por uno con la mano a todos los presentes en el salón.

En la previa, los nuevos socios Emilio Monzó y Margarita Stolbizer se mostraron muy cercanos, y luego se sentaron juntos en el recinto, donde integran el flamante bloque Encuentro Federal.