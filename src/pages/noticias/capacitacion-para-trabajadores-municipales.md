---
category: La Ciudad
date: 2021-11-25T06:00:57-03:00
thumbnail: https://assets.3dnoticias.com.ar/municipalidad.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Trabajos de bacheo previstos para este jueves
title: Capacitación para trabajadores municipales
entradilla: En este caso, es para los tramos superiores y de supervisión (categoría
  16 a 24). Por primera vez, la formación se realizará íntegramente dentro de la propia
  Municipalidad.

---
El próximo martes 30 de noviembre, en el Museo de la Constitución, se dictará el primer módulo de la Escuela Superior de Administración Municipal (EAM), espacio destinado a la formación y capacitación de los agentes municipales, enmarcada en la Ordenanza N° 12123/14. El tema de esta entrega será “Pensar la ciudad y el rol de la gestión pública municipal”.

Vale destacar que por primera vez, desde su creación en 2014, la Escuela Superior de Administración Municipal para personal jerárquico, se llevará adelante íntegramente dentro de la propia Municipalidad. De esa manera, se evita la tercerización de la formación, lo que en muchas ocasiones generaba que se pierda el foco en lo local.

Además, se especificó que el dictado de todos los módulos se hará con modalidad mixta (presencial y virtual) para favorecer el cursado.

El director Ejecutivo de Función Pública del municipio, Federico De los Reyes, destacó que luego del parate obligado por la pandemia “hemos podido relanzar la EAM y, en conjunto con la Asociación de Obreros y Empleados Municipales (Asoem), con la cual conformamos una mesa paritaria, pensamos en un programa de formación específico que prevea las dificultades y desafíos de la gobernanza local, rescatando los saberes del personal municipal y reforzando sus competencias”.

**Mejorar las capacidades**

Cabe mencionar que la Escuela de Administración Municipal prevé formación en tres tramos: el de capacitación continua, estímulos y becas, y el de formación municipal para los tramos superiores y supervisión (desde la categoría 16 hasta la 24). “Lo que pretendemos con esta iniciativa es volver a poner en el centro de la escena al trabajador municipal, entendiendo que es la herramienta primordial para trasladar todas las acciones de gobierno impulsadas por el intendente Emilio Jatón”, concluyó el funcionario.

El nuevo programa consta de ocho módulos en total. El primero de ellos inicia el martes 30 de noviembre, de 18 a 20 horas en el Museo de la Constitución, y concluirá los últimos días de diciembre. Posteriormente, a partir de 2022, se dictarán los siete módulos restantes.