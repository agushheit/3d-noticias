---
category: Agenda Ciudadana
date: 2021-12-09T06:00:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/omarperottijpg.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Omar Perotti confirmó que habrá más cambios en su gabinete
title: Omar Perotti confirmó que habrá más cambios en su gabinete
entradilla: 'En una entrevista Perotti confirmó que antes de fin de año relanzará
  su equipo de gobierno y que la plata de la deuda se utilizará para obras de infraestructura

'

---
El gobernadror Omar Perotti se refirió en una entrevista, realizada en Radio 2 de Rosario, al fallo de la Corte Suprema de Justicia de la Nación y destacó que los cerca de $100.000.000.000 se invertirán en obras de infraestructura en todo el territorio provincial. Además, anticipó que antes de fin de año hará cambios en su equipo de gobierno.

“Tenemos la satisfacción de saber que hay una acreencia que tenía la provincia y esos recursos vuelven. A partir de allí, vamos a comenzar a delinear las utilizaciones cuando se cancelen. Es una señal de que cuando trabajamos juntos, las cosas llegan y no hay que bajar nunca los brazos”, remarcó.

Con respecto a la importancia del fallo, el mandatario santafesino explicó que la Corte definió cómo se calcula la deuda histórica, algo que hasta la fecha no se había podido acordar entre las gestiones nacionales y provinciales. “En estos dos años llevamos instancias de negociación, particularmente desde que asumió Manzur”, reconoció.

“Claramente ahora la instancia de cumplimiento es de obligatoriedad y de plazo. No es un tema ajeno al gobierno (nacional) o algo que no hemos conversado. De hecho, en el patio de la facultad de Derecho, durante la campaña electoral, entre los compromisos que asumió con la provincia, está la cancelación de la deuda. No es un tema que se desayunó ayer”, aseguró.

Una vez que el dinero esté en la provincia, el gobernador aseguró que la decisión es que se invierta en obras de infraestructura para todo el territorio. “Hay muchas obras para definir, nosotros hemos avanzado hasta aquí con obras básicas para poner al día en la provisión de agua potable y cloacas a las principales ciudades”, sostuvo el funcionario santafesino.

“Buscamos proyectos que generen hoy mano de obra, pero que también generen empleo una vez finalizadas. Tenemos que tener la inteligencia de utilizar los recursos para que impacten el doble”, insistió el gobernador.

**Cambios en el gabinete**

De cara a los últimos dos años de su mandato, Omar Perotti confirmó que antes de fin de año se realizarán algunas modificaciones en su equipo de gobierno.

Si bien no dio nombres, el gobernador aseguró en Radio 2 que uno de los ministros que cambiará es el de Gobierno, puesto que actualmente ocupa Roberto Sukerman. Una mujer, en especial Celia Arena, hoy titular de Igualdad, Género y Diversidad, sonaba como posible reemplazante. De hecho, en la reunión de la semana pasada en la que el gobernador abordó la problemática de la seguridad con los presidentes de bloque de la Cámara de Diputados, Arena estuvo sentada entre sus principales colaboradores.

Perotti mantuvo una reunión con legisladores y los intendentes de Santa Fe y Rosario. En el encuentro sentó a Celia Arena entre sus principales colaboradores.

"Dijimos que íbamos a trabajar en diciembre en la evaluación de todas las áreas en la necesidad de cambios y en los ajustes de funcionamiento que hay que hacer. Esto va más allá de los nombres, muchas veces hay que ajustar adentro de las áreas porque el funcionamiento no es siempre de los funcionarios de primera línea", señaló.