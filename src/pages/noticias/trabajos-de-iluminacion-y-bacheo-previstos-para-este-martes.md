---
category: La Ciudad
date: 2021-10-05T06:00:43-03:00
thumbnail: https://assets.3dnoticias.com.ar/ILUBACHEO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Trabajos de iluminación y bacheo previstos para este martes
title: Trabajos de iluminación y bacheo previstos para este martes
entradilla: La Municipalidad informa el avance del cronograma de tareas previstas
  en los planes Santa Fe se Ilumina y de Bacheo. A medida que se desarrollan los trabajos,
  puede haber cortes de circulación y desvíos de colectivos.

---
En el marco del plan Santa Fe se Ilumina, la Municipalidad concreta tareas tendientes a recuperar el alumbrado existente en toda la ciudad, para llegar a fin de año al 40% de la ciudad con luces led. La Municipalidad se encuentra trabajando en la colocación de nuevas columnas de iluminación y artefactos led en distintos barrios y avenidas, como así también en el mantenimiento de la red actual de la ciudad.

En ese sentido, se trabajará en la colocación de nuevas luminarias en:

* Los barrios: Siete Jefes, Guadalupe y Fomento 9 de Julio
* “Avenidas de tu Barrio”: Dr. Zavalla, avenida Mosconi, Monseñor Zaspe y Mendoza

También se realizará el mantenimiento del sistema eléctrico, entre otros puntos, en los siguientes lugares:

* Los Alisos, Timbúes y Los Ubajay
* Parque del Sur, Espigón I y II y Costanera Oeste
* Blas Parera y Azcuénaga
* Ruta Nacional 168
* Plaza Islas Malvinas (Llerena 3400)

**Bacheo**

Por otra parte, el municipio avanza con el plan de bacheo diseñado para reparar más de 12.000 metros cuadrados de calles de la capital provincial. El programa se desarrolla por fases, priorizando las zonas de la ciudad que evidencian mayor circulación.

Actualmente se trabaja en:

* 25 de Mayo y Santiago del Estero
* General Lopez al 3200
* Necochea y Sargento Cabral
* Necochea e Ituzaingó
* Marcial Candioti y Gobernador Candioti

Según se informa, todas las tareas se realizan siempre en sentido del tránsito. Puede haber cortes de circulación y desvíos de colectivos a medida que avanzan las obras. Además, se detalla que los trabajos están sujetos a las condiciones climáticas.