---
category: Agenda Ciudadana
date: 2021-07-27T06:15:27-03:00
thumbnail: https://assets.3dnoticias.com.ar/VIOLETA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: La provincia de Santa Fe, en alerta violeta por bajas temperaturas
title: La provincia de Santa Fe, en alerta violeta por bajas temperaturas
entradilla: Se espera que en la región las temperaturas mínimas alcancen los -6° C.

---
El Servicio Meteorológico Nacional advirtió este lunes que el área comprendida por el todo el territorio santafesino - así como el de las provincias vecinas de Córdoba, Entre Ríos, Buenos Aires y La Pampa - será afectado por bajas temperaturas, por debajo de valores normales, al menos hasta la mañana del jueves 29. Se espera que las temperaturas mínimas estén entre -6 °C y 6 °C, mientras que las temperaturas máximas oscilarán entre 10 °C y 17 °C.

En tal sentido, el organismo recomendó

1- Tomar abundante cantidad de líquido caliente.

2- Mantener las normas de higiene para evitar el contagio de enfermedades como la gripe.

3- Mantener los ambientes ventilados para evitar la inhalación de monóxido de carbono.

4- Usar ropa adecuada para bajas temperaturas.

5- Mantenerse informado por autoridades.

Por su parte, la Secretaría de Protección Civil de Santa Fe emitió una serie de recomendaciones ante la ola de frío polar que afectará a la región haciendo hincapié en el correcto uso de la calefacción y recordó que dolores de cabeza, mareos, debilidad, dolor en el pecho y confusión son síntomas de intoxicación por monóxido de carbono.