---
category: Agenda Ciudadana
date: 2022-12-13T08:20:26-03:00
thumbnail: https://assets.3dnoticias.com.ar/apple-1867461_1920.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: La Banca Móvil representan el 60% de la cantidad de operaciones realizadas
title: La Banca Móvil representan el 60% de la cantidad de operaciones realizadas
entradilla: Los datos surgen del último Informe Mensual de Pagos Minoristas elaborado
  por el Banco Central.

---
Mes a mes, el uso del teléfono celular para realizar pagos va ganando nuevos adeptos, al punto tal que hoy es la plataforma más elegida por los usuarios de servicios financieros a la hora de hacer una transferencia de dinero. La tendencia viene aumentando con fuerza desde 2021, y va de la mano del crecimiento exponencial que en términos de cantidades están experimentando los pagos con transferencia.

En concreto, en octubre se realizaron 162,8 millones de transferencias, un aumento de 7,1% con respecto a septiembre y más del doble de los 79,7 millones que se habían realizado en octubre de 2021. Los datos surgen del último Informe Mensual de Pagos Minoristas elaborado por el Banco Central, que muestran que los pagos realizados desde la Banca Móvil representan el 60% de la cantidad de operaciones realizadas, cuando en agosto era del 57% y a fin del año pasado apenas superaba el 53%.

Si bien el proceso de transformación digital que fue realizando el sistema financiero contribuyó a consolidar esta tendencia, el rol que tuvo el BCRA, a través de distintas medidas tomadas por el Directorio, fue clave para agilizar las operaciones de la Banca Móvil y así motorizar su crecimiento.

Una de esas medidas, por ejemplo, fue a fines de 2020, cuando lanzó Transferencias 3.0, un sistema innovador que incorporó la interoperabilidad de los códigos QR, de modo que un usuario de servicios financieros pueda realizar pagos con transferencia utilizando cualquier código QR, sólo con un teléfono y con cualquier billetera virtual o aplicación de banco que ofrezca ese servicio.

El nuevo ecosistema abierto y universal contribuyó a estimular una mayor competencia entre los diferentes actores del sistema que redundó en mejores condiciones para usuarios y comercios. El objetivo, de hecho, fue promover la formación de un ecosistema digital de pagos abierto y universal, y conseguir una mayor inclusión de aquellos sectores que aún no utilizan los servicios financie-ros, uno de los ejes de gestión del organismo monetario.

Otra de las medidas que el Directorio del BCRA tomó para desarrollar con fuerza el uso de los pagos a través de celular fue en febrero de 2022 cuando elaboró nuevos requisitos técnicos para los Prestadores de Servicios de Pago (PSP) y las entidades financieras que ofrecen el servicio de billetera digital, que apuntaron a reforzar las medidas para mitigar el fraude en las operaciones con billeteras digitales. En rigor, se agregó un proceso técnico de seguridad a la ya implementada autenticación del cliente y autorización ante instrucción de pago, para complementar y reforzar las medidas de seguridad previamente adoptadas por el organismo monetario.

También en febrero creó el Registro de Billeteras Digitales Interoperables, para que las entidades financieras y los PSP que presten el servicio de billetera digital se inscriban y obtengan la certificación para brindar el servicio. El registro, que desde julio ya está disponible en la página web del BCRA, permite a las personas usuarias contar con la información de aquellas billeteras que pueden ser utilizadas para realizar Pagos con Transferencia (PCT) mediante la lectura de cualquier código QR.

Todas estas medidas generaron el marco propicio para que cada vez sean más los usuarios que se vuelcan a la Banca Móvil a la hora de realizar un pago, y explican en gran modo que hoy en día de cada 10 de transferencias 6 se realicen desde un teléfono celular. Además, medidas como la interoperabilidad de código QR explican en gran parte que en octubre el 54% de las transferencias inmediatas (88 millones de operaciones) haya tenido como origen o destino una billetera virtual, ya que en ellas participó una Clave Virtual Uniforme (CVU).

El protagonismo de la Banca Móvil en cantidad de operaciones, sin embargo, aún no se refleja en el monto operado por transferencias, ya que actualmente el 79% del monto corresponde a Transferencias de Alto Valor (TAV) realizadas entre empresas. A través de este tipo de operaciones se realizaron 10,5 millones de transferencias por más de 16 billones de pesos en octubre, lo que en cantidad de operaciones representa apenas el 6% del total.

De todos modos, y si bien en monto las transferencias inmediatas representan el 18% del total (esto incluye a la Banca Móvil, Home Banking y ATM), de a poco se ve cómo van ganando terreno. En un año, por ejemplo, se registró un alza del 122% en los montos, al pasar de 1,69 millones de pesos a los 3,76 millones de pesos que se movieron en octubre de 2021.

En paralelo a esta mayor adopción del teléfono celular para hacer transferencias inmediatas los datos muestran que desde hace prácticamente un año, el uso de las tarjetas de débito está virtualmente estancado.

Según los últimos datos del Banco Central, en septiembre de este año se realizaron 166,45 millones de transacciones con tarjetas de débito, 0,78% menos que los 167,75 millones de agosto y 9,35% por debajo de las 183,6 millones registradas en julio. El gráfico que forma parte del Informe Mensual de Pagos Minoristas de octubre es más que elocuente: desde diciembre del año pasado hasta ahora que este número se mantiene en un rango de entre 160 y 180 millones de operaciones mensuales.