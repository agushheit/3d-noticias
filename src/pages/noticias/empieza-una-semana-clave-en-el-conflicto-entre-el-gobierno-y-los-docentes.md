---
category: Agenda Ciudadana
date: 2021-10-18T06:15:03-03:00
thumbnail: https://assets.3dnoticias.com.ar/OTRAVEZSOPA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Empieza una semana clave en el conflicto entre el gobierno y los docentes
title: Empieza una semana clave en el conflicto entre el gobierno y los docentes
entradilla: Amsafé hará un paro de 48 horas el miércoles y jueves. El gobierno, que
  dijo que ya hizo la mejor oferta posible, no los convocará hasta que terminen las
  medidas de fuerza

---
Este miércoles y jueves Amsafé llevará adelante un paro de 48 horas con lo que completará el mandato de sus bases que definieron un plan de lucha iniciado la semana pasada con un paro de 24 horas. Los docentes públicos le pidieron al gobierno que mejore la oferta salarial del 17 por ciento en tres tramos para la segunda mitad del año.

Del otro lado del mostrador, el gobernador Omar Perotti dijo que el 10 por ciento en octubre, el 5 por ciento en diciembre y el 2 por ciento en enero es lo mejor que pueden ofrecer y que no habrá otra propuesta. Además, funcionarios provinciales advirtieron que no convocarán a los docentes mientras haya medidas de fuerza.

Por otro lado, el gobierno también amenazó con el descuento de los días de paro y con no pagar el aumento que ya aceptaron los gremios de la administración central y Sadop, el de los docentes privados.

Pese a las tensiones, Amsafé llevó adelante el paro de 24 horas el miércoles pasado y aseguró que hubo un acatamiento total a esa medida de fuerza. Además, en la ciudad de Santa Fe realizó una convocatoria frente a la sede de la Regional IV de Educación que juntó a decenas de maestros que resistieron a una fuerte lluvia. Lo mismo se replicó en la ciudad de Rosario, pero frente a la sede de la gobernación.

En los actos los maestros le pidieron al gobierno que no amenace y que reabra la mesa de diálogo. Los docentes quieren que se mejore la oferta salarial y que se cumplan con las promesas entre las que se encuentra el blanqueo de las sumas no remunerativas, entre otras demandas.

**Diálogo en suspenso**

"Está puesto sobre la mesa lo mejor que tenemos para los sectores, pero tenemos que resguardar el equilibrio", dijo el gobernador el viernes pasado en la ciudad de Rosario donde compartió algunas actividades con el ministro de Educación de la Nación Jaime Perczyk.

"Creemos que la magnitud de la propuesta realizada va en consideración de saber que tenemos un salario con poder adquisitivo importante y hoy la provincia está en condiciones de hacer ese esfuerzo, hubo momentos en que no pudimos ni pagar en tiempo y forma los sueldos", analizó Perotti.

Pero los docentes creen que hay lugar para el diálogo y reclama voluntad política al gobierno para resolver el conflicto. Luego de el paro de 48 horas previsto para esta semana

“No tuvimos ninguna convocatoria. El gobierno tiene que demostrar voluntad para resolver el conflicto. Ayer fue un paro total en todas las escuelas de la provincia en el que se reclamó a una mejora de la propuesta salarial y la solución de temas no salariales y que están planteados en la asamblea”, afirmó el jueves Sonia Alesso, secretaria General de Amsafé.

Alesso le pidió al gobierno “que mejore la propuesta salarial y en ese sentido hay varias alternativas, pero hasta ahora no tenemos ninguna convocatoria para sentarnos a una mesa como debería ser, en el marco de la paritaria, para resolver la situación”.

La discusión paritaria se da a menos de un mes de las elecciones generales de medio término. Ese es un elemento más que se suma a la tensión propia del conflicto. Esta semana será clave en la resolución del conflicto porque si el gobierno no llama al diálogo a los docentes de Amsafé hasta después de la finalización de las medidas de fuerza, recién el viernes podría haber novedades.