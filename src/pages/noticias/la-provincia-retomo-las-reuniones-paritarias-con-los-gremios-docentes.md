---
category: Estado Real
date: 2021-03-18T07:34:09-03:00
thumbnail: https://assets.3dnoticias.com.ar/cantero.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: La provincia retomó las reuniones paritarias con los gremios docentes
title: La provincia retomó las reuniones paritarias con los gremios docentes
entradilla: Los ministros de Educación, Adriana Cantero, y de Trabajo, Empleo y Seguridad
  Social, Juan Manuel Pusineri, se reunieron con representantes de Amsafe y UDA.

---
La ministra de Educación, Adriana Cantero, y su par de Trabajo, Empleo y Seguridad Social, Juan Manuel Pusineri, mantuvieron este miércoles un encuentro paritario con representantes de Amsafe y UDA.

En la oportunidad, Cantero señaló que “la propuesta salarial está ratificada. Lo que vamos a seguir conversando son cuestiones puntuales para ver posicionamientos y posibilidades de aproximación en algunos aspectos que son de la política pedagógica del Ministerio pero que tal vez podamos seguir analizando, cada uno con sus respectivos criterios”.

Asimismo, la ministra de Educación aclaró que “todos los gremios, aun los que aceptaron, han hecho algunos planteos y eso podrá ser motivo de nuestra reflexión, pero la política salarial está ratificada porque es, en el sector educación, la mejor del país en el marco de lo que fijó como rumbo la paritaria nacional docente”.

Respecto a las medidas de fuerza anticipadas por el gremio Amsafe para 23 y 25 de marzo, la titular de la cartera educativa expresó que “nosotros estamos intentando conversar en estos días en que no hay paro, viendo cuales son las alternativas que tenemos, porque nuestro trabajo no para en relación a la pretensión que tenemos de que todas las chicas y los chicos tengan clases”.

Por último, Cantero hizo mención a la importancia del regreso a las aulas precisando que “la evaluación es altamente positiva, si recorren las escuelas verán el clima de los chicos, de la familia y también de los propios educadores que reafirma la importancia de volver al espacio presencial, que es cuidado y tiene protocolos porque estamos en una pandemia”.

“Por ello, a quienes nos piden que volvamos a poner todos los chicos en un mismo espacio todo el tiempo les respondemos que vamos a seguir cuidando la salud y la vida de nuestro chicas y chicos, y de nuestras educadoras y educadores”, concluyó la ministra.

Por su parte, Pusineri remarcó que “estamos hablando de una política salarial que supera la paritaria nacional docente y la de las jurisdicciones comparables a Santa Fe. De manera tal que llamamos a la reflexión a la docencia y los dirigentes gremiales teniendo en cuenta que al no aceptar la propuesta se imposibilita el pago del aumento en el mes de marzo”.

El ministro de Trabajo se refirió a los canales de comunicación con los sindicatos docentes señalando que “el diálogo está abierto lo que no podemos hacer, y desde el gobierno lo venimos explicando, es modificar situaciones sobre las que estamos convencidos, más aún cuando la propia dirigencia ha calificado la propuesta como buena”.