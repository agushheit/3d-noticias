---
category: La Ciudad
date: 2021-02-15T06:30:16-03:00
thumbnail: https://assets.3dnoticias.com.ar/jardinesmunicipales.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Vuelta a clases en los jardines municipales con grupos reducidos y "salitas
  burbuja"
title: Vuelta a clases en los jardines municipales con grupos reducidos y "salitas
  burbuja"
entradilla: 'Habrá cuatro días intercalados con presencialidad por semana, y otro
  para actividades virtuales. Estrictos protocolos y salas con señalética del tipo
  "burbuja" para garantizar las distancias sociales. '

---
Tras un año de parate por la pandemia, el próximo 17 de febrero se reanudarán las clases presenciales en los 17 jardines municipales de la ciudad. Comenzarán los alumnos y alumnas de sala de dos y tres años reinscriptos en 2020: son 1.527 alumnos de todos esos centros educativos de la primera infancia, pero con una modalidad específica y adaptada a los protocolos. El resto de los ingresos serán de los que se vienen anotando desde el lunes pasado: ese retorno será "progresivo y escalonado", según aseguraron desde el municipio local.

Esta vuelta a las aulas "reales" se ciñe a un esquema mixto diseñado para la presencialidad, y de las medidas de bioseguridad que se adoptarán. En primer lugar, se conformaron grupos de alumnos que van a ir dos días cada uno de éstos: lunes y miércoles un grupo, martes y jueves el otro. Y el viernes será un día actividades virtuales. Con los grupos desdoblados y días rotativos, se garantiza que los chicos tengan dos días completos en el jardín, en el turno mañana -de 8 a 12- y en el de tarde, de 13.30 a 17.

Cada sala, según la edad, tiene un máximo de ocupación permitido por ordenanza que va de 12 a 18 niños. Como se puede trabajar al 50%, ahora los grupos presenciales en cada salita y por cada docente oscilarán entre 6 y 10 niños y niñas, promedio. "Hablamos de grupos presenciales rotativos y desdoblados, no una semana sí y la siguiente no (como anunció Provincia para los niveles educativos formales), sino dos días fijos, así las familias se organizan en sus actividades cotidianas y laborales. El acompañamiento virtual seguirá", le dice a El Litoral Paulo Ricci, secretario de Educación y Cultura municipal.

**Garantizar las distancias**

Se evitarán las aglomeraciones: las familias dejarán a los chicos e inmediatamente se retirarán. Hacia adentro de las aulas, habrá señalética que serán los nuevos "espacios a habitar" para cada niño, en modo "burbujas". Es que el tamaño de los espacios áulicos son clave para garantizar los distanciamientos sociales: incluso se estudiaron los planos de los 17 jardines (que en total tienen 112 salas) para ver las dimensiones físicas, y para que en ese desdoblamiento de grupos haya distancias sociales.

Habrá cartelería con medidas antiCovid; toma de temperatura a docentes y asistentes a cada ingreso de turno, bandejas sanitizantes y alcohol el gel distribuidos por todas las instalaciones. El personal tendrá que cambiarse (deberá llevarse una muda de ropa personal). Si alguien presenta síntomas, no pueden ir. Y si un niño o niña tiene síntomas asociados con la enfermedad, no debe ser enviado al jardín por sus padres.

"Hay un protocolo de cierre del grupo o aislamiento, dependiendo del caso (si es contacto estrecho, por ejemplo). Incluso se está hablando, ante la eventualidad de que, si tenemos que aislar a docentes, de contar con otros reemplazantes listos para ocupar esos lugares. Además, los docentes que integran población de riesgo harán la parte de acompañamiento pedagógico virtual", indica Ricci.

Se desinfectará todos los días, tres veces: antes de que se abran las puertas de los jardines (a primera hora de la mañana), entre un turno y otro (al mediodía) y al cierre, es decir, a la tarde, después de las 17.

El plantel total es de unos 190 docentes y asistentes escolares. Estos últimos estarán en el aula, al lado del o la docente, asistiendo cuando -por ejemplo- a un chico hay que higienizarlo, cambiarlo o darle de comer.

**Lactantes**

Con respecto a las salas de los lactantes (el sistema de jardines del Estado local recibe niños de 45 días a 3 años, cabe recordar), "de los 17 jardines sólo cuatro tienen salas habilitadas para lactantes. Sí: va a haber ingresos, y hay que contemplar que son muy pocos cupos, justamente porque son cuatro salas habilitadas (seis bebés por sala, que serían tres al mismo tiempo)", expresaron Huaira Basaber y Paulo Ricci, de la subsecretaria de Gestión Cultural y Educativa de la Municipalidad.