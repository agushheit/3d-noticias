---
category: Agenda Ciudadana
date: 2021-03-07T05:30:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/GANANCIAS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: El Congreso comienza a tratar la baja de Ganancias este martes
title: El Congreso comienza a tratar la baja de Ganancias este martes
entradilla: La Cámara de Diputados abrirá este martes en comisión el debate del proyecto
  de reforma al gravamen que busca aliviar del pago de ese tributo a más de un millón
  de personas.

---
La Cámara de Diputados abrirá este martes en comisión el debate del proyecto de reforma al impuesto a las Ganancias que busca aliviar del pago de ese tributo a más de un millón de personas, con las exposiciones del ministro de Trabajo, Claudio Moroni, y de la titular de la AFIP, Mercedes Marco del Pont.  
La intención del oficialismo es realizar una ronda de consultas a lo largo de toda la semana y emitir dictamen si es posible, y si no firmar el despacho entre el 15 y el 16 de marzo, previo a su tratamiento en el recinto de sesiones previsto para el miércoles 17 o jueves 18, según informaron fuentes parlamentarias.  
De esta manera, los diputados avanzarán con el pedido del presidente Alberto Fernández, quien en la Asamblea Legislativa de inauguración de las sesiones ordinarias solicitó al Congreso la sanción del proyecto de ley que exime del impuesto a las Ganancias a los trabajadores que perciben hasta 150 mil pesos de salario bruto.  
El plenario de comisiones de Presupuesto y Hacienda y de Legislación del Trabajo -que presiden los legisladores del Frente de Todos Carlos Heller, y Vanesa Siley- fue citado para el martes a las 15 para abrir el debate y recibir informes de los funcionarios del Poder Ejecutivo.  
A través del sistema de videoconferencia expondrán y responderán preguntas Moroni; Marcó del Pont, y el secretario de Política Tributaria, Roberto Arias.  
El proyecto que se pondrá a consideración fue redactado por el presidente de la Cámara de Diputados, Sergio Massa, y fue firmado por la mayoría de los legisladores del Frente de Todos y de los interbloques opositores Federal, y Unidad Federal para el Desarrollo, y la bancada Acción Federal.

También respaldan en general el proyecto Juntos por el Cambio y Consenso Federal, que piden ampliar esos beneficios a los autónomos.  
El secretario de la Comisión de Presupuesto, Luis Pastori, dijo a Télam que "en principio vamos a escuchar las explicaciones de los funcionarios y cuáles son los cambios propuestos".  
  
**Algunos puntos del proyecto**

El proyecto que se buscará sancionar modifica el artículo 30 para aumentar la deducción especial a partir del cual se calcula el impuesto, con el fin de que no paguen ese gravamen los trabajadores que cobren hasta 150 mil pesos brutos.  
A los trabajadores que cobren hasta 124 mil pesos netos de sueldo no se les descontará el impuesto.  
Esta medida beneficiará a 1.267.000 personas, del total de dos millones de asalariados registrados que hoy pagan ese tributo.  
El costo fiscal del impuesto coparticipable entre la Nación y las provincias se estima en 40 mil millones de pesos, según señaló Siley.  
Otro de los artículos del proyecto también busca a eximir a la mayoría de los jubilados del pago del tributo, al establecer que quienes perciban hasta ocho jubilaciones mínimas quedarán exentos del pago de Ganancias.  
El oficialismo además aceptará introducir cambios propuestos desde el sindicalismo que piden que queden exceptuados del pago del impuesto a las Ganancias las horas extras, viáticos, el aguinaldo, pago de guardería y los bonos productivos.  
  
**Reunión con sindicalistas**

Massa, junto con Heller y Siley, recibieron el martes pasado a una veintena de gremialistas encabezados por las autoridades de la CGT que efectuaron los pedidos para que esos adicionales queden excluidos del pago de Ganancias.  
El titular del cuerpo se reunirá este martes nuevamente con sindicalistas y el diputado Facundo Moyano -autor de otra iniciativa para reformar el impuesto a las Ganancias-, que le acercarán propuestas de modificación al proyecto del tributo.  
Participarán Pablo Moyano (Camioneros), Mario Manrique (Smata), Omar Plaini (Canillitas), Raúl Durdo (SOMU, Marítimos); Sergio Sánchez (Peajes); Daniel Ricci (Fedum); Miguel Díaz (Udocba); Diego Corvalán (SUPA) y Juan Carlos Moreyra (Ceramistas), Pablo Flores (empleados de AFIP); Juan Pablo Brey (Aeronavegantes); Alberto Wehbe (obreros del vidrio) y Pablo Palacio (recibidores de granos, Urgara), entre otros.  
Otro punto de la iniciativa que será motivo de debate es el artículo que establece que aquellos que cobren hasta 173 mil pesos brutos también tendrán menores descuentos que en la actualidad, y los montos serán definidos por el Poder Ejecutivo.  
En cambio, los salarios más altos de la pirámide pagarán los mismos valores que en la actualidad, ya que no se reforma la deducción especial ni otras deducciones, con lo cual se les descontarán los mismos valores que se aplican desde el 1 de enero, cuando se incrementó el 35% el Mínimo No imponible.