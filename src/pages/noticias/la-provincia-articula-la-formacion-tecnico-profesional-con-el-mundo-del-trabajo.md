---
category: Agenda Ciudadana
date: 2021-09-17T18:00:40-03:00
thumbnail: https://assets.3dnoticias.com.ar/PARQUEINDUSTRIAL.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La provincia articula la formación técnico profesional con el mundo del trabajo
title: La provincia articula la formación técnico profesional con el mundo del trabajo
entradilla: Funcionarios provinciales y de la Nación visitaron el Parque Industrial
  de Sauce Viejo.

---
La ministra Adriana Cantero, junto al ministro de Trabajo, Seguridad y Empleo Social, Juan Manuel Pusineri y el secretario general del Consejo Federal de Educación, Mario Oporto, visitó el Parque Industrial de Sauce Viejo con el objetivo de fortalecer el nexo entre la educación, el trabajo y la producción.

En ese contexto, los funcionarios provinciales y nacionales, y representantes del sector industrial resaltaron el trabajo de mapeo en el territorio para conocer las necesidades o demandas de capacitación por un lado, y su contraparte en el relevamiento de ofertas formativas, con el objetivo de acercar ambas mediante la implementación de dispositivos pedagógicos acordes a la modalidad técnica.

En relación a la visita del Mario Oporto, la ministra Cantero señaló que “estamos trabajando una vinculación entre educación, trabajo y producción con la presencia del secretario del Consejo Federal de Educación, que es quién impulsa a nivel federal este gran proyecto de que el sistema educativo se vincule con los sectores de la producción y el trabajo para poder aportar al crecimiento y al desarrollo de la nación y de cada una de las regiones”.

En ese sentido, Cantero puntualizó que “en este caso Santa Fe es una provincia que crece y produce y que tiene su desarrollo basado precisamente en esta posibilidad y que necesita un sistema educativo actualizado que pueda cumplir, fundamentalmente con los niveles secundario y terciario, con un conocimiento que circule en las aulas y que pueda ser aplicado a los distintos formatos de la producción y del trabajo”.

Por su parte, en relación a la vinculación entre la formación técnico profesional y el mundo laboral, el titular del Consejo Federal expresó lo siguiente: “Vinimos a dialogar con los industriales en lo que hace a los programas de Educación para el trabajo y para el desarrollo y aquí es fundamental la presencia de los ministros tanto de Educación como de trabajo porque así es como se trabaja, de modo interministerial en un proyecto donde además del Estado participan los industriales y los trabajadores para poder dar una oferta no solo capacitadora sino también productivista y federal a la necesidad que tiene nuestro país de capacitar para el trabajo, para la producción y para el desarrollo sustentable”.

Asimismo, en el diálogo entre autoridades e industriales, se destacó la conformación del Consejo Provincial de Educación, Trabajo y Producción ( CoPETyP ), que cuenta con mesas y agendas de trabajo con las cuales se pueden abordar principalmente temáticas vinculadas a la educación del nivel secundario en escuelas técnicas, como las prácticas profesionalizantes, entornos formativos y diseños curriculares.

De la recorrida por el espacio industrial, también participaron el director provincial de Educación Técnica, Fernando Hadad y la gerente del Parque Industrial de Sauce Viejo y de la Cámara de Industriales Metalúrgicos y Autopartistas de Santa Fe (CAMSFE); Angelina Macua.