---
category: Agenda Ciudadana
date: 2021-07-09T09:37:54-03:00
thumbnail: https://assets.3dnoticias.com.ar/inundacionalejandrovillar.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: Declaran el 29 de abril Día de la Memoria y Solidaridad de la Inundación
title: Declaran el 29 de abril Día de la Memoria y Solidaridad de la Inundación
entradilla: Es para conmemorar la inundación del año 2003 en la ciudad de Santa Fe
  y alrededores. También establece ese día como una “jornada de acción, de limpieza,
  cuidado y preservación de los anillos de defensa”.

---
Resulta paradójico, pero el día en que inhumaron los restos del exgobernador Carlos Reutemann, los legisladores santafesinos aprobaron la declaración del 29 de abril como "Día de la Memoria y Solidaridad de la Inundación de Santa Fe y alrededores". 

La ley sancionada es la 14.037, que también establece ese día como una “jornada de acción, de limpieza, cuidado y preservación de los anillos de defensa”.

Además, el ministerio de educación deberá incluir al 29 de Abril dentro del calendario escolar de los distintos niveles del sistema educativo provincial.

Será con el objetivo de promover la difusión y toma de conciencia respecto a la importancia de la construcción de memoria sobre estos hechos.