---
layout: Noticia con imagen
author: "Fuente: Diario UNO"
resumen: Vuelta a las aulas
category: Estado Real
title: Punto por punto, cómo será la vuelta a las aulas desde el 1 de diciembre
  en Santa Fe
entradilla: Será tanto para el primario como el secundario. El receso o las
  vacaciones comenzarán el 15 de diciembre hasta el 17 de febrero, cuando
  comience el ciclo 2021.
date: 2020-11-26T12:57:02.883Z
thumbnail: https://assets.3dnoticias.com.ar/vuelta-a-clases.jpg
---
Si bien la organización dependerá de cada institución, la ministra de Educación Adriana Cantero brindó los lineamientos para la vuelta de los encuentros entre docentes y estudiantes.

Desde el próximo martes 1 de diciembre y hasta el 11, los alumnos de todos los cursos, primaria y secundaria, deberán asistir a la escuela en grupos de como máximo ocho estudiantes.

“No es que volvemos a dar clases; lo que hacemos es, antes de las vacaciones, antes del receso de verano, y con motivo de entrega de materiales que van a estar regulando las actividades de aprendizaje del tiempo que va a seguir, tener pequeños encuentros cuidados, con protocolos”, advirtió Cantero.

En ese sentido, indicó que a partir del martes “se darán pequeños reencuentros deseados entre los chicos y sus educadores”, y explicó que la intención es que los docentes “puedan poner un poco de mirada, ver cómo están, intercambiar algunas apreciaciones con ellos, que permitan de algún modo ver quiénes necesitan alguna consulta en el mes de febrero, a quiénes a lo mejor hay que acompañar más y planificar lo que va a seguir”.

La ministra de Educación sostuvo que se tratará “de pequeños encuentros que pongan gradualidad para ir pensando alguna posible vuelta a la presencialidad, si es que la pandemia nos da tregua”.

## **Cómo será la vuelta a clases** 

* El martes 1º de diciembre próximo los chicos volverán a la escuela en pequeños grupos de no más de ocho alumnos por curso.


* Las clases se extenderán hasta el 11 de diciembre para los niveles primario y secundario.



* Cada institución organizará cómo se constituirá cada grupo y en qué horario y por cuánto tiempo permanecerán en el aula.


* Los alumnos no podrán pasar toda la jornada escolar en la escuela ya que deberán dejar el lugar a los siguientes grupos.



* Durante el tiempo en el aula, recibirán de parte de su maestros y profesores un cuaderno de trabajo elaborado por el Ministerio de Educación para dar continuidad a la escuela también durante los meses siguientes.



* Los establecimientos educativos deberán garantizar el distanciamiento social, las prácticas de higiene como el lavado de manos y el uso del barbijo.

## **Cómo serán las vacaciones**

* Las vacaciones comenzarán el 15 de diciembre y el 17 de febrero de 2021 se producirá el retorno a clases de los 7º grados de las escuelas primarias y los 5º y 6º años de la escuela secundaria del ciclo 2020. En ese momento se evaluará si se hará de manera presencial, virtual o mixta, según cómo se encuentre la situación sanitaria en la provincia.


* En esos días se habilitarán consultas para aquellos alumnos que tuvieron trayectorias más difíciles. Esta cursada será hasta el 12 de marzo, fecha en que culminará el ciclo escolar y podrán egresar de los distintos niveles.

## **Actos de colación**

* No habrá actos de fin de curso en diciembre y serán evaluados para su realización en marzo de 2021, "cuando efectivamente haya graduados", según indicó el Ministerio de Educación provincial

## **Cuándo arranca el ciclo 2021**

* El 15 de marzo arrancará oficialmente el ciclo escolar 2021 para todos los cursos y culminará el 20 de diciembre, de modo de garantizar 180 días de clases.


* A esa fecha, los edificios escolares deberán realizar una limpieza profunda y desinfectar las aulas antes de que entre cada grupo.


* Además, se deberán armar los espacios que compartirán los docentes con los alumnos a partir de los protocolos aprobados, para que se garantice el distanciamiento.