---
category: Estado Real
date: 2020-12-16T12:43:23Z
thumbnail: https://assets.3dnoticias.com.ar/registro-civil.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Sistema de Partidas Online en el Registro Civil
title: Sistema de Partidas Online en el Registro Civil
entradilla: Con esta modalidad se pueden gestionar diferentes documentos a través
  de la página web, de forma rápida y sencilla, y sin necesidad de ir a las oficinas
  del organismo.

---
**El gobierno provincial anunció la incorporación del Sistema de Partidas on line del Registro Civil, por el cual los interesados podrán ingresar a la página web del organismo y gestionar allí diferentes documentos sin necesidad de solicitarlos de manera presencial, brindando mayor rapidez, de manera cómoda y sencilla**.

Al respecto, el secretario de Justicia de la provincia, Gabriel Somaglia, sostuvo que “el Registro cuenta con más de 13 millones de documentos en los distintos libros y oficinas, y la posibilidad de acercar la gestión y los trámites que se realizan en el Registro a la comunidad, fue todo un desafío”.

En ese sentido, el funcionario reconoció que “la pandemia nos puso un escollo e hizo que agudicemos el proceso de digitalización e informatización, tanto de esta oficina como de su gestión hacia la comunidad, y hoy anunciamos la puesta en marcha de este Sistema, que es el acceso a los documentos que cualquier ciudadano, en lugar de solicitar los mismos de manera presencial, es decir, trasladándose a las oficinas del Registro, puedan hacerlo a través de una computadora o un teléfono, desde su casa”.

Para ello, “se puede acceder a la página web del Registro e ingresando los datos requeridos se pueden obtener la mayoría de las partidas en menos de 72 horas. En caso que esos documentos no están digitalizados, la demora va a ser de unos días más”, explicó Somaglia, quien adelantó que “se tratará de culminar en el 2021 con la digitalización completa de todos los documentos que restan”.

## “**ESTO ES DAR RESPUESTAS A LA GENTE”**

Mientras, la directora provincial de Registro Civil, Luisina Giovannini, reconoció que con este avance se trata de “completar una deuda existente que había desde la entidad ante la comunidad y que nos puso en evidencia en este contexto de pandemia”.

“Para nosotros es un trabajo muy grande, muy duro, de un gran equipo que existe en el Registro, con un fuerte apoyo de la Secretaría de Justicia en el acompañamiento y en toda la puesta en marcha de la digitalización a demanda del Archivo Provincial”, indicó la funcionaria provincial.

“**Lo que implica digitalizar 13 millones de documentos y que la gente pueda pedir desde casa su partida de nacimiento, pagarla y recibirla, para nosotros es dar una respuesta concreta a esta demanda**”, concluyó Giovannini.

Finalmente, el subsecretario de Registros sobre Personas Humanas y Jurídicas, Francisco Dallo, dijo que esta “es la concreción de un largo trabajo, bajo la premisa de lograr conectividad y virtualidad en todas las áreas de la Secretaría de Justicia”.

Por otro lado, Dallo indicó que la puesta en funcionamiento de este Sistema “no implica que estén todos los documentos digitalizados, por lo que aquellos que no lo están, tendrán una demora mayor en su entrega respecto de las partidas que ya están en el Sistema”, y mencionó sí se encuentran digitalizados todos los nacimientos desde 1970 en adelante.

Respecto de los documentos aún sin digitalizar, se está desarrollado “un proyecto a gran escala con financiación del Consejo Federal de Inversiones, para en uno o dos años tener digitalizada e indexada la documentación”.

## **CÓMO FUNCIONA**

El trámite se realiza con el registro de los ciudadanos en la página del Gobierno de la Provincia, www.santafe.gob.ar, gestionando un usuario y contraseña; luego se debe ingresar y solicitar la partida que necesite, de nacimiento, matrimonio, unión convivencial y/ o defunción.

**Los pasos para obtener las partidas a través del Sistema de Solicitud de Partidas Web son las siguientes:**

#### 1- REGISTRO

Se realiza por única vez desde el portal: [www.santafe.gob.ar](https://sso.santafe.gov.ar/service-auth/login?service=https://www.santafe.gob.ar/idciudadana/login "Registro")

#### 2- SOLICITUD

Ingresando al portal: [www.santafe.gob.ar/rcdigital](https://www.santafe.gob.ar/rcdigital/ "Solicitud") y siguiendo las instrucciones del sistema.

#### 3- PAGO

En la misma plataforma se encuentra el botón de pago electrónico pudiendo abonar la partida con tarjeta de débito. Costos **desde $ 112,50** en adelante según la opción elegida.

#### 4- RETIRO: Opciones (previa validación del pago)

* **Partida con firma digital**: se encontrará disponible en la plataforma de solicitud.
* **Partida en formato papel**: disponible en el Archivo Provincial del Registro Civil o con envío a domicilio por correo postal.