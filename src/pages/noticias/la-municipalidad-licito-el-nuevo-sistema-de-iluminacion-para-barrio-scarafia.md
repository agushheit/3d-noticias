---
category: La Ciudad
date: 2021-02-13T06:00:13-03:00
thumbnail: https://assets.3dnoticias.com.ar/licitacionscaraffia.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Municipalidad licitó el nuevo sistema de iluminación para barrio Scarafía
title: La Municipalidad licitó el nuevo sistema de iluminación para barrio Scarafía
entradilla: Las tareas, que se enmarcan en el Plan de Iluminación, contemplan la colocación
  de 100 columnas con artefactos LED a lo largo de 32 cuadras.

---
En el marco del Plan de Iluminación implementado por la Municipalidad, este viernes se licitaron las tareas para colocar 100 columnas de iluminación LED en más de 30 cuadras del barrio Scarafía. El presupuesto oficial para concretar los trabajos es de $ 11.895.107. En total se presentaron cuatro ofertas económicas.

Las empresas que participaron de la licitación fueron: Bauza Ingeniería SRL., que cotizó por $ 10.753.697; Electromecánica Tacuar SRL, por $ 11.295.829; Coemyc SA, por $ 15.124.929; e Ingeser Construcciones SRL, la cual quedó desestimada por no adjuntar la póliza de garantía.

Luego del acto de licitación, Griselda Bertoni, secretaria de Obras y Servicios Públicos municipal, detalló el alcance de los trabajos. “Dentro de las diferentes acciones que realizará la Municipalidad este año en materia de iluminación, la de hoy fue la primera licitación para dotar de luminarias a barrios completos”, indicó Bertoni.

En tal sentido, la funcionaria consignó que los trabajos que se realizarán en 32 cuadras del barrio Scarafía consisten en “la colocación de columnas con iluminación LED y un nuevo tendido eléctrico sobre pasaje Santa Fe, Alberti, Guanella, Padre Genesio y las calles transversales norte-sur que cortan pasaje Santa Fe”.

**Barrio por barrio**

Cabe recordar que el Plan de Iluminación incluye la recuperación del alumbrado público existente, la actualización tecnológica de las luminarias y la colocación de un nuevo sistema, mediante columnas con equipos LED en varios barrios de la ciudad.

Esta primera etapa, que comienza con barrio Scarafía, continúa en Liceo Norte, donde se instalarán 61 columnas con una inversión de $ 7.500.000; Santa Marta, con 84 columnas con una inversión de $ 9.500.000; Siete Jefes, cuyo proyecto consiste en colocar 310 columnas con un monto de inversión cercano a los 31 millones de pesos; y Guadalupe al noreste, donde se instalarán 405 columnas con sus equipos LED, cableado y tableros, con una inversión cercana a los $ 40 millones.

A esto se suman las inversiones en columnas con artefactos LED en barrio Roma, con un monto de $ 9.000.000; Fomento 9 de Julio, por $ 10.000.000; y en el entorno de los hospitales de la ciudad, por $ 5.000.000. En paralelo se desarrollan los proyectos para La Vuelta del Paraguayo, los barrios Candioti Norte y Candioti Sur, y el cuadrante sur de la ciudad, estudiándose en cada caso, la forma pertinente de financiamiento.

Por otra parte, el municipio presentó un proyecto ante la Nación para recibir el financiamiento que permita dotar de iluminación al barrio San Agustín. El plan, en el marco de la iniciativa “Argentina Hace I”, fue aprobado y se espera la comunicación oficial de Nación para avanzar en la licitación, por un monto que ronda los 26 millones de pesos.