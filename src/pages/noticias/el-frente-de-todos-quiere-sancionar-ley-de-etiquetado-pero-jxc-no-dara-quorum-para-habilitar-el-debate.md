---
category: Agenda Ciudadana
date: 2021-10-05T06:15:13-03:00
thumbnail: https://assets.3dnoticias.com.ar/etiquetado.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: El Frente de Todos quiere sancionar ley de etiquetado pero JXC no dará quórum
  para habilitar el debate
title: El Frente de Todos quiere sancionar ley de etiquetado pero JXC no dará quórum
  para habilitar el debate
entradilla: Así lo informó Florencia Lampreabe, diputada nacional por el Frente de
  Todos, quien pidió a la oposición que sea "coherente" y que "cuiden lo que comen
  los argentinos".

---
El oficialismo de la Cámara de Diputados buscará reunir el quórum en la sesión especial que impulsa para este martes a fin de convertir en ley el proyecto de etiquetado frontal que advierte sobre los excesos de grasas, sodio y azúcar en los alimentos mientras que Juntos por el Cambio anticipó que no colaborará para reunir el número reglamentario, aunque sí participará del debate en el caso de que el Frente de Todos consiga juntar los 129 legisladores requeridos para abrir la discusión.  
  
De este modo, si el oficialismo no consigue el respaldo de otros bloques, la primera sesión presencial convocada en la Cámara baja tras un año y medio de funcionamiento virtual por la pandemia de coronavirus corre el riesgo de fracasar.  
  
La sesión fue solicitada por la bancada del Frente de Todos, que encabeza Máximo Kirchner, para debatir un conjunto de iniciativas consensuadas con la mayoría de los bloques después de tres meses sin deliberar, debido a la campaña electoral y a la falta de acuerdo entre los distintos espacios parlamentarios.  
  
El temario de la convocatoria impulsada por el oficialismo incluye, además, otras iniciativas, como la que propone políticas transversales para las personas en situación de calle y otro referido a medidas vinculadas con la actividad de la vitivinicultura.  
**Los temas**

"La ley de etiquetado frontal es un buen ejemplo para demostrar que, aun en medio de un proceso electoral, los diferentes espacios políticos podemos avanzar con proyectos que benefician a la gente. Es una pena que algunos estén más preocupados por quedarse con la presidencia de la Cámara que en hacer cosas para la gente. No sorprende, porque es lo que hicieron cuando gobernaron", afirmó Máximo Kirchner en declaraciones radiales, al referirse a la sesión de mañana.  
  
De esta manera, el legislador hizo referencia a las afirmaciones de la primera candidata a diputada de Juntos por el Cambio en la Ciudad de Buenos Aires, María Eugenia Vidal, quien días atrás había anticipado que la coalición opositora reclamaría la presidencia de la Cámara de Diputados, que hoy está en manos del diputado oficialista Sergio Massa, si se repiten los resultados de las PASO.  
  
En ese contexto, Kirchner cuestionó a JxC por amenazar con no dar quórum, y les pidió responsabilidad para poder tratar "una buena ley" que, dijo, "va a ayudar a seguir mejorando el sistema de salud", y sostuvo que, si bien "surgen controversias, no está prohibiendo nada, sino que va a permitir ver qué consumimos, no que las dejemos de consumir".  
  
Kirchner formuló estas declaraciones esta mañana, entrevistado por El Destape radio, cuando aún JxC no había anunciado su decisión de no dar quórum, aunque algunos legisladores opositores ya habían deslizado que el oficialismo debía encargarse de reunir el número ya que el temario no se había consensuado.  
La coalición opositora, que en la Cámara baja encabeza el radical cordobés Mario Negri, decidió esta tarde no dar quórum al considerar que la sesión fue convocada por el FdT y propuso realizar una sesión la próxima semana en la que se incluya un temario consensuado entre todos los bloques parlamentarias y se incorpore el proyecto de etiquetado frontal, informaron fuentes parlamentarias.  
  
A lo largo de la jornada, la UCR mantuvo varias reuniones para definir su postura ya que la mayoría de sus diputados respalda el proyecto de etiquetado frontal de alimentos, pero finalmente se prefirió consensuar con el PRO y la Coalición Cívica -los otros socios de la coalición- la postura común de no dar quórum y abrir una negociación con el oficialismo para acordar un temario para la semana próxima que incluya la emergencia educativa y la ley ovina.  
  
De todas maneras, desde el radicalismo ratificaron que si el FdT reúne quórum JxC participará del debate.  
  
Fuentes de la bancada opositora señalaron que no estaban al tanto de la sesión, ni tampoco fueron consultados por el oficialismo, y dijeron que "tampoco nos abrieron el temario para incorporar temas de interés, así que suponemos que deben tener el quórum", para luego subrayar: "La sesión es especial, así que el quórum lo consigue el que la pide".  
  
El proyecto que busca aprobar el FdT propicia advertir a la sociedad sobre los productos que tienen un alto contenido de grasas, sodios y azúcares, con el fin de ayudar a combatir la obesidad, la hipertensión y los riesgos cardíacos.  
  
La iniciativa es rechazada por algunas empresas de la alimentación y genera resistencias entre legisladores de las provincias del norte, sin distinción de alineamientos políticos, ya que desde esa región del país sostienen que el etiquetado frontal perjudicará a la producción azucarera, por lo que promueven introducir modificaciones en el texto que se aprobó en el Senado.  
  
A fines de septiembre pasado, organizaciones de la sociedad civil enviaron cartas a los presidentes de todos los bloques de la Cámara de Diputados para reclamar el tratamiento urgente, antes el riesgo de que pierda estado parlamentario.  
  
"No hay motivos válidos para retrasar más la aprobación de la ley. El proyecto cuenta con amplio apoyo de la sociedad civil y académica, tanto de Argentina como de la región; con el respaldo de la mejor evidencia científica libre de conflicto de interés y con el aval de la Cámara de Senadores. Es hora de que los y las diputadas pongan fecha para sesionar y aprueben el proyecto sin más demoras", reclamó en aquel momento Victoria Tiscornia, investigadora de la Fundación Interamericana del Corazón (FIC).  
  
Además de FIC Argentina, las cartas fueron firmadas por Federación Argentina de Graduados en Nutrición (Fagran), Consumidores Argentinos, Fundeps, Sanar y Consciente Colectivo.  
  
A esos reclamos se sumó también el pedido de casi 160 mil personas que, con su firma, adhirieron a una petición motorizada a través de la plataforma de Change.org ([http://change.org/malcomidos](http://change.org/malcomidos "http://change.org/malcomidos")) por la periodista Soledad Barruti, autora de los libros "Mal comidos" y "Mala leche".  
  
"Las estadísticas de enfermedad provocadas por la mala alimentación en Argentina son terribles. Vivimos en uno de los países con mayor número de muertes atribuibles directamente al consumo de bebidas azucaradas (74 muertes por millón). Alrededor de 10 niños cada 100 mil en nuestro país desarrollarán diabetes y el 80% de los niños tienen caries", advierte la petición al reclamar la urgente sanción de la ley de etiquetado en la Cámara de Diputados.