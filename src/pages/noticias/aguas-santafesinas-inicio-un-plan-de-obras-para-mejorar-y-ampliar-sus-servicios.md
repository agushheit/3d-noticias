---
category: La Ciudad
date: 2021-01-04T09:17:37Z
thumbnail: https://assets.3dnoticias.com.ar/040121-ASSA.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Aguas Santafesinas inició un plan de obras para mejorar y ampliar sus servicios
title: Aguas Santafesinas inició un plan de obras para mejorar y ampliar sus servicios
entradilla: Más de cien mil santafesinos serán beneficiados por estas obras de expansión
  de redes o mejora de la prestación de agua potable y cloacas, emprendimientos que
  serán realidad a lo largo del 2021.

---
**El presidente de Aguas Santafesinas, Hugo Morzan, realizó un balance del primer año de gestión y destacó el valor de las gestiones provinciales ante el gobierno nacional.** 

«Hoy tenemos obras en ejecución o en proceso de licitación en nueve ciudades de la provincia, por más de 2.247 millones de pesos», indicó.

«En nuestro primer año de gestión enfrentamos desafíos inéditos, no solo en los 15 años de vida de la empresa sino en toda la historia del servicio de agua potable de la provincia, y los superamos para bien de todos nuestros usuarios», aseguró Morzan, en referencia a las consecuencias de la pandemia COVID-19 y de la extraordinaria bajante del río Paraná.

«**Sabemos que este es un servicio esencial y, como tal, debemos darle continuidad y confiabilidad**. Por eso el gobernador Perotti y la ministra Silvina Frana desde un primer momento lo pusieron entre las prioridades de la gestión provincial», dijo Morzan.

Así fue como, ya en abril, el gobernador Omar Perotti logró un acuerdo con el gobierno nacional, a través del ministro de Obras Públicas, Gabriel Katopodis, para encarar un intenso plan de obras de acceso y mejora de agua potable y saneamiento, realizando inversiones que son fundamentales para la mejora del servicio.

«Hoy **tenemos obras en ejecución o en proceso de licitación en nueve ciudades de la provincia**, con un presupuesto nacional de más de 2.247 millones de pesos», enfatizó el presidente de Aguas, resaltando «el desempeño de nuestro personal que elaboró en pocos meses y en tiempo récord proyectos de ingeniería que fueron aprobados por el Ente Nacional de Obras Hídricas de Saneamiento (ENOHSA)».

**Más de cien mil santafesinos serán beneficiados por estas obras de expansión de redes o mejora de la prestación de agua potable y cloacas, emprendimientos que serán realidad a lo largo del 2021.**

Mientras tanto, la extraordinaria bajante de más de un año de duración afecta en mayor o menor medida a las siete plantas potabilizadoras que la empresa tiene sobre el sistema río Paraná, tantos en ciudades abastecidas en forma directa (Santa Fe, Reconquista, Rosario etc.) como de acueductos regionales.

«Nunca antes hubo una bajante tan prolongada en el tiempo; y entonces, lo que normalmente podían ser medidas extraordinarias, debieron transformarse en reformas permanentes de nuestros sistemas de captación», detalló el presidente de Aguas Santafesinas.

**Gracias a inversiones directas del Gobierno provincial, superiores a los cien millones de pesos, las medidas adoptadas (incremento de la capacidad de captación con más bombas) lograron atender la emergencia sin afectar el servicio.** 

«Y no quiero olvidarme del compromiso de todo el personal, técnico, operativo, administrativo y de conducción, que afrontó este desafío inédito, logrando mantener la calidad y cantidad de siempre del servicio», agregó Morzan.

Sin dudas, el otro gran desafío del año fue mantener la calidad del servicio en el marco del COVID-19. 

«Desde el inicio de la pandemia, en Aguas Santafesinas nos fijamos objetivos claros: Asegurar la prestación de un servicio esencial para la prevención del COVID-19, como es el agua potable, a más de dos millones de santafesinos en 40 ciudades. Y preservar a nuestros trabajadores por ser el recurso imprescindible para la prestación del servicio. Ambos los cumplimos ampliamente», aseguró el directivo.

Y eso gracias a que se trabajó para la puesta en marcha de más de 30 protocolos con medidas de prevención, tanto para empleados como para proveedores, usuarios etc.; medidas para garantizar los insumos necesarios de producción y distribución de agua potable, así como mantenimiento de plantas, redes y sistemas; el cumplimiento estricto de las medidas de prevención, en particular las distintas fases de cuarentena establecidas por las autoridades; el seguimiento constante de los casos de contagio que se dieron entre el personal, cumpliendo medidas de aislamiento inmediato para evitar la propagación del virus  y abriendo nuevos canales de atención para los usuarios, como WhatsApp, página web, redes sociales, etc.

Finalmente, Morzan dijo que «este verano no es fácil para nuestro servicio, teniendo en cuenta que persiste la bajante del río; la pandemia sigue siendo una amenaza; y que las obras de mejora que iniciamos todavía están en ejecución. Por eso **apelamos al compromiso de nuestros usuarios para realizar un uso solidario y responsable del agua potable**, porque la capacidad de producción de nuestras plantas tiene límites que no podemos superar. Pero **los santafesinos pueden estar tranquilos porque estamos trabajando para mejorar en todos los aspectos de nuestras prestaciones**, gracias al compromiso de una conducción provincial a cargo de Omar Perotti, que conoce bien la necesidad de llegar con agua potable y saneamiento de calidad a todos los santafesinos».