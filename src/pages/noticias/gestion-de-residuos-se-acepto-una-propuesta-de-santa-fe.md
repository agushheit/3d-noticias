---
category: Agenda Ciudadana
date: 2021-07-20T06:00:04-03:00
thumbnail: https://assets.3dnoticias.com.ar/gestionresiduos.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Gestión de residuos: se aceptó una propuesta de Santa Fe'
title: 'Gestión de residuos: se aceptó una propuesta de Santa Fe'
entradilla: La iniciativa de la provincia hace recomendaciones a municipios y comunas
  para usar diferentes colores en las bolsas para mejorar la gestión de residuos.

---
La provincia de Santa Fe, a través del Ministerio de Ambiente y Cambio Climático, realizó una propuesta al Consejo Federal de Medio Ambiente (Cofema) para la creación de un código unificado de colores y terminología para los residuos. La iniciativa fue votada por unanimidad por las provincias, y el nuevo código de clasificación facilitará a los ciudadanos la tarea, fortaleciendo la cultura general en lo relacionado a la gestión de residuos y fomentando la economía circular.

Antes de la propuesta realizada, ni la Argentina ni la provincia de Santa Fe contaban con un código unificado para la identificación de los distintos tipos de residuos sólidos urbanos, por lo que la separación de estos desechos no se realizaba de manera homogénea en las distintas jurisdicciones. Además, el sistema no unificado creaba confusión en los usuarios de los sistemas de separación en origen de distintas localidades, con el consiguiente riesgo de pérdida de eficiencia en las soluciones de valorización posterior.

Santa Fe fue la primera provincia del país en adherir a la Resolución Nacional a través de la Resolución Provincial Nº 040/21. La iniciativa recomienda a todos los municipios y comunas la adopción de un sistema de gestión de residuos que asegure, como mínimo, una separación binaria de los residuos sólidos urbanos generados en sus territorios, promoviendo una disposición inicial selectiva y posterior recolección diferenciada que contemple, por un lado, los residuos reciclables secos, y por otro, los residuos considerados basura. Los colores utilizados, en Santa Fe y a nivel nacional, serán el verde para residuos reciclables secos, y el negro para la basura.

Una vez lograda la disposición inicial selectiva, la Resolución también recomienda a las autoridades competentes que, paulatinamente y de acuerdo con sus posibilidades, puedan adoptar un Plan de Transición Progresiva y optimizar sus sistemas de gestión a través de la identificación y separación de los residuos sólidos urbanos generados en sus territorios de forma más detallada. Para eso, se utilizará el color marrón para los residuos orgánicos compostables, el color amarillo para los plásticos, el color azul para papel y cartón, el blanco para los vidrios, y el gris para los metales.

Desde la cartera ambiental se encuentran brindando un acompañamiento para aquellos municipios y comunas de la provincia que ya vienen trabajando en la gestión integral de residuos, como así también para quienes se han incorporado recientemente. En ese sentido, se buscará adaptar la nueva resolución de colores y terminología de forma estratégica y en el marco de una transición, teniendo en cuenta la situación de cada localidad respecto de esta temática.