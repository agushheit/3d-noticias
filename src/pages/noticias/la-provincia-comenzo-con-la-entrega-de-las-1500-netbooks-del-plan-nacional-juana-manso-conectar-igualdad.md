---
category: Estado Real
date: 2021-12-28T06:00:40-03:00
thumbnail: https://assets.3dnoticias.com.ar/NOTEBOOKS.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'La provincia comenzó con la entrega de las 1500 netbooks del Plan Nacional
  Juana Manso: Conectar Igualdad'
title: 'La provincia comenzó con la entrega de las 1500 netbooks del Plan Nacional
  Juana Manso: Conectar Igualdad'
entradilla: Los dispositivos están destinados a estudiantes de escuelas secundarias
  públicas y privadas de Rafaela, Santa Fe, Rosario, Capitán Bermúdez, Granadero Baigorria
  y San Lorenzo.

---
El Ministerio de Educación de la provincia comenzó con la entrega de computadoras portátiles que recibió en el marco del “Plan Nacional Juana Manso: Conectar Igualdad”. Son un total de 1.500 unidades que serán distribuidas en instituciones educativas de Rosario, Santa Fe, Rafaela, Granadero Baigorria, San Lorenzo y Capitán Bermúdez.

En Rafaela se entregaron 133 netbooks a las escuelas de Educación Técnico Profesional Nº 495 y Nº 654. Los dispositivos fueron recibidos por el personal directivo de las instituciones y serán destinados a los alumnos que cursaron el 1er año del nivel Secundario.

En ese marco, la secretaria de Gestión Territorial Educativa de la provincia, Rosario Cristiani, manifestó: “Estamos empeñados en borrar las asimetrías, y entregar computadoras a estudiantes de escuelas secundarias nos ayuda a eso, porque crea mejores condiciones para el aprendizaje y para que todas y todos tengan las herramientas de estudio. Desde nuestra gestión trabajamos en la línea de aceptar las diferencias pero no las desigualdades. Por eso, es tan importante la llegada de estos dispositivos.”

“Todos estos dispositivos serán entregados en comodato a los estudiantes”, agregó Cristiani, quien indicó que “la provincia tiene un sector técnico que será el encargado de acondicionarlas para su uso. En esta tanda las computadoras vienen nominadas, eso quiere decir que vienen pensadas y destinadas para alumnas y alumnos de 1er. año. Seguramente la inversión del gobierno nacional va a abrir otras posibilidades que abarque a una mayor cantidad de estudiantes”.

Por su parte, el senador provincial por el departamento Castellanos, Alcides Calvo, destacó el programa: “Creo que es un hecho sumamente importante y va de la mano con la línea que viene planteando el gobierno provincial con la Ley de Conectividad y con el Plan de Fortalecimiento en Educación Rural haciendo entrega de teléfonos celulares a alumnas y alumnos de escuelas rurales”.

“Esto es un volver a empezar, porque tuvimos un lapso de 4 años que ha generado un detrimento al sistema educativo en general. Quedaron muchos recuerdos de aquellas netbooks que se entregaron en su momento y que lamentablemente se perdieron, porque muchas veces no tenían la asistencia técnica ni el asesoramiento de uso. Por eso, la vuelta de este Plan viene a saldar esa deuda que teníamos con los estudiantes” concluyó Calvo.

De la actividad participaron también la secretaria de Educación de la Municipalidad de Rafaela, Mariana Andereggen; el delegado de la Región III de Educación, Gerardo Cardoni; coordinadoras pedagógicas y personal directivo de las instituciones.