---
category: Estado Real
date: 2020-12-27T12:06:30Z
thumbnail: https://assets.3dnoticias.com.ar/2712pymes.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: 'Pymes santafesinas: más facilidad para ingresar al sistema financiero'
title: 'Pymes santafesinas: más facilidad para ingresar al sistema financiero'
entradilla: El Ministerio de Producción, Ciencia y Tecnología presentó un documento
  que compila todas las herramientas de financiamiento disponibles para el sector
  productivo santafesino.

---
Durante 2020, desde la mencionada área de Gobierno, se llevó adelante una importante tarea de diálogo y articulación con el sector productivo santafesino, de estas acciones surgió que una de las mayores demandas es el acceso al financiamiento productos de dos factores, altas tasas de intereses y plazo de devolución cortos y dificultad en contar con las garantías para el respaldo necesario que requiere el sistema financiero.

En ese marco, la cartera productiva instrumentó diversas herramientas entre la que se destacaron dos fondos que suman 600 millones de pesos, y que permiten a las micro, pequeñas y medianas empresas contar con 2,4 mil millones de pesos en certificados de garantía. Esto significa acceso a créditos bancarios en mejores condiciones, evitando depósitos prendarios e hipotecarios. 

Se trata de la renovación y ampliación del fondo específico de la provincia de Santa Fe en Garantizar SGR a 100 millones de pesos y la constitución de un fondo específico en Fogar, en conjunto con el Ministerio de Desarrollo Productivo de 500 millones. Cabe destacar que Santa Fe fue la primera provincia en constituir este fondo específico.

Otro punto a destacar es el **Programa Santa Fe de Pie**, un paquete de financiamiento disponible en el Nuevo Banco de Santa Fe, de 500 millones de pesos y con garantía Fogar, para las actividades afectadas por la pandemia. En detalle, se trata de capital de trabajo a 18 meses con 6 meses de gracias a tasas: 0%, 10% y 12% según la actividad y el grado de afectación por DISPO y ASPO; y para bienes de capital a 24% fija y en peso a 36 meses. También se previó una línea para prefinanciación de exportaciones a 3,5% de interés, en dólares.

Para mayor información adjuntamos el [documento que resume todas las líneas disponibles para el sector productivo](https://assets.3dnoticias.com.ar/Santa-Fe-de-Pie.pdf "Descargalo"). Desde el Ministerio también se comunica que otras entidades financieras que deseen participar deberán enviar su información a [unidaddegestion@santafe.gov.ar](mailto:unidaddegestion@santafe.gov.ar)