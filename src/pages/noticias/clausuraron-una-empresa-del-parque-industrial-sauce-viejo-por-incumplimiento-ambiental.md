---
category: Agenda Ciudadana
date: 2021-07-08T09:05:33-03:00
thumbnail: https://assets.3dnoticias.com.ar/parque-industrial.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Santo Tomé al día
resumen: Clausuraron una empresa del Parque Industrial Sauce Viejo por incumplimiento
  ambiental
title: Clausuraron una empresa del Parque Industrial Sauce Viejo por incumplimiento
  ambiental
entradilla: Se trata de ESPRO S.A. La medida fue tomada por el Ministerio de Ambiente
  y Cambio Climático, debido a que no se realizaron las presentaciones correspondientes.

---
La provincia de Santa Fe, a través del Ministerio de Ambiente y Cambio Climático, continúa avanzando en procedimientos sancionatorios con el objetivo de hacer cumplir de forma correcta los protocolos ambientales en industrias.

En ese sentido, se llevó adelante un procedimiento de clausura en la fábrica industrial de alimentos concentrados ESPRO S.A, debido a que la empresa no había presentado el informe ambiental de cumplimiento ante el Ministerio, y además, contaba con irregularidades en el manejo de efluentes. Durante el fin de semana, incluso, se había producido un incendio dentro del establecimiento.

La clausura realizada prevé un cese total del proceso productivo y se desarrolló en el marco de las estrategias planteadas desde la Secretaría de Políticas Ambientales. Por tal motivo, participaron de la intervención personal de las direcciones de Gestión Ambiental y de Desarrollo Sustentable del Ministerio de Ambiente y Cambio Climático.

**Otros controles**

Con el objetivo de incrementar de manera continua la fiscalización ambiental, la provincia viene desarrollando procedimientos de control en empresas de otras localidades santafesinas.

En ese sentido, se realizaron inspecciones en Bombal, San Justo, San Lorenzo, Las Garzas y Sauce Viejo. Algunas de las recorridas fueron desarrolladas en forma conjunta con el Ministerio de Ambiente y Desarrollo Sostenible de la Nación. Dependiendo del tipo de empresa y localidad, durante los controles, entre otras acciones, se procede a la constatación y monitoreo de la calidad de aire y de ruidos molestos, como así también se toman muestras de efluentes líquidos.