---
category: Agenda Ciudadana
date: 2022-06-22T07:45:26-03:00
thumbnail: https://assets.3dnoticias.com.ar/construccion.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: El  Costo de la Construcción creció un 6.7%.
title: El  Costo de la Construcción creció un 6.7%.
entradilla: |+
  El costo de la construcción (CC) en la ciudad de Santa Fe correspondiente a mayo de 2022
  registra un aumento de 6,7% en relación al mes anterior.


---
Este resultado surge como consecuencia del alza de 3,6% en el capítulo “Materiales”, de 10,0% en el capítulo “Mano de obra” y de 15,9% en el capítulo “Gastos generales.”

En el capítulo “Materiales”, los grupos con los aumentos más relevantes son: Zinguería, 9,9%; Agregados finos y gruesos, 8,4%; Pisos y revestimientos varios, 8,0%; Maderas, 4,8%; Pinturas, 4,6%; Chapas, 3,7%; Perfiles metálicos, 3,0%; Ladrillos, 2,8%; Cementos, 2,51%; Aceros nervados, 1,6%. El capítulo ‘’Mano de Obra’’ incorpora los nuevos valores establecidos por las Resoluciones Nº 2022-889-APNST-MT para las categorías laborales previstas en el Convenio Colectivo de Trabajo Nº76/75, estableciendo un aumento del 10,0% rige a partir del 1º de Mayo de 2022, aplicable sobre los valores vigentes al 31 de marzo de 2022. El alza de “Gastos generales” se debió principalmente al grupo Honorarios profesionales.

El costo total  del metro cuadrado es de $ 81,095.40.-

El Costo de la Construcción (CC), es un indicador de corto plazo, que permite establecer el valor promedio mensual del costo de la construcción por metro cuadrado de una vivienda tipo en la ciudad de Santa Fe. Se refiere de una Vivienda Unifamiliar; siendo la superficie total 69,50 m2. (Cubierta 65,64 m2 y semi cubierta 3,86 m2).