---
category: Agenda Ciudadana
date: 2021-03-08T07:15:53-03:00
thumbnail: https://assets.3dnoticias.com.ar/Alberto.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Fernández y los gobernadores firmarán el acta compromiso contra la violencia
  de género
title: Fernández y los gobernadores firmarán el acta compromiso contra la violencia
  de género
entradilla: El presidente Alberto Fernández encabezará hoy, la firma del acta compromiso
  "Acuerdo Federal para una Argentina Unida contra la violencia de género", junto
  a los gobernadores y el jefe de Gobierno porteño.

---
El presidente Alberto Fernández encabezará hoy la firma del acta compromiso "Acuerdo Federal para una Argentina Unida contra la violencia de género", junto a los gobernadores y el jefe de Gobierno porteño, Horacio Rodríguez Larreta.

 El acto está previsto a las 11.30 en el Museo del Bicentenario de Casa Rosada, en el marco del Día Internacional de la Mujer, se informó en forma oficial.

La actividad contará con la participación de los gobernadores de todo el país y el jefe de Gobierno de la Ciudad de Buenos Aires.

En ese marco, también estará presente la ministra de las Mujeres, Género y Diversidad, Elizabeth Gómez Alcorta, y el ministro del Interior, Eduardo "Wado" de Pedro, entre otros funcionarios.

 La iniciativa forma parte del Plan Nacional de Acción Contra las Violencias por Motivos de Género 2020-2022, que "constituye un cambio de paradigma en el abordaje de las violencias por motivos de género desde una mirada integral y transversal".