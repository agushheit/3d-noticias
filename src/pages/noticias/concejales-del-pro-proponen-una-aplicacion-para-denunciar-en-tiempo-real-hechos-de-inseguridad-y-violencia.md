---
category: La Ciudad
date: 2021-08-27T10:30:01-03:00
thumbnail: https://assets.3dnoticias.com.ar/ceresola2.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Ceresola
resumen: Concejales del PRO proponen una aplicación para denunciar en tiempo real
  hechos de inseguridad y violencia
title: Concejales del PRO proponen una aplicación para denunciar en tiempo real hechos
  de inseguridad y violencia
entradilla: Luciana Ceresola, precandidata a concejal del PRO (JxC), en la lista Santa
  Fe Nos Une, y el concejal del PRO, Sebastián Mastropaolo, presentaron en la Vecinal
  Guadalupe Noreste el proyecto Ojos en Alerta.

---
**Ojos en Alerta**, es un programa de seguridad ciudadana propuesto por los ediles, que consiste en una aplicación que permite dar aviso en tiempo real de situaciones de inseguridad o emergencia.

El proyecto permite dar aviso de delitos, emergencias médicas, disturbios en la vía pública, peligro, violencia doméstica o de género y enviar ubicación en tiempo real.

Este plan ha sido elaborado por profesionales propios y de otros distritos con el asesoramiento de los equipos técnicos de la ex Ministra de Seguridad Patricia Bullrich, y con el invalorable aporte de vecinos de la ciudad.

De esta manera, el vecino es participe activo de la comunicación con el centro de monitoreo urbano. "Los vecinos no tendrían que estar involucrados en lo que es la seguridad en un primer frente, pero estamos ante una situación que no ofrece más dilaciones. Y los vecinos quieren involucrarse. Obviamente sin una reforma policial, penitenciaria, sin las autoridades judiciales que realmente actúen con rapidez y celeridad para dejar a los vecinos en la calle y a los delincuentes adentro, no vamos a poder solucionarlo. Estas son herramientas que estamos ofreciendo para colaborar en la batalla contra la inseguridad" dijo Ceresola.

"Sabemos que no es una discusión fácil ni simpática ni popular, pero tenemos que cortar con la ideología y empezar a pensar en la seguridad de todos los santafesinos", agregó.

Mastropaolo contó a los vecinos sobre la reunión que ambos mantuvieron con funcionarios del Ministerio de Seguridad y dijo que se expuso sobre la situación complicada de los santafesinos y que se escucharon propuestas. "La situación de la ciudad se complica día a día. En la reunión con Seguridad hablamos de que estamos viviendo una violencia a la que no estamos acostumbrados, extrema. Lamentablemente nos acostumbramos a los arrebatos, pero no a un ingreso a un domicilio o a los golpes".

La denuncia que se realiza en la aplicación, no reemplaza a la oficial. Es complementaria al 911, 107 o a la policial. Y queda asentada en el centro de monitoreo municipal y el teléfono del vecino.

El Plan engloba también otras herramientas y programas integrales, cuyas propuestas ya fueron presentadas al Concejo, como ser: Coordinación de Cámaras de seguridad del 911 y de la municipalidad, operativos conjuntos de secuestro de motovehiculos en infracción entre Municipalidad y Provincia, operativos de control de asentamientos en zonas ferroviarias, entre otros.

Ceresola y Mastropaolo visitarán las vecinales de la ciudad explicando el alcance y características de este plan integral de seguridad para que, de esta manera, los vecinos lo conozcan y lo tomen como otra herramienta para combatir la ola de inseguridad que aqueja a nuestra ciudad en particular y a la provincia de Santa Fe en general.![](https://assets.3dnoticias.com.ar/ceresola1.jpeg)![](https://assets.3dnoticias.com.ar/ceresola3.jpeg)