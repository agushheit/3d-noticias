---
category: Estado Real
date: 2021-07-22T06:00:04-03:00
thumbnail: https://assets.3dnoticias.com.ar/SANJUSTO.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Funcionarios de educación recorrieron escuelas del departamento San Justo
title: Funcionarios de educación recorrieron escuelas del departamento San Justo
entradilla: Las autoridades de la cartera educativa verificaron el avance de obras
  del programa Escuela Segura.

---
Funcionarios del Ministerio de Educación recorrieron escuelas del departamento San Justo para verificar el estado de obras encaradas por la gestión educativa en las instituciones escolares. En esta oportunidad, el secretario de Educación, Victor Debloc, acompañado por la delegada de la Región IV de Educación, Mónica Henny, y por técnicos del área de Infraestructura, visitó la escuela primaria 1374 “Elso Ferrero” y el Jardín de Infantes 94 “Profesor Juan Mantovani”, ambas instituciones de la ciudad de San Justo.

En la escuela primaria 1374 se están construyendo dos aulas, una cocina y un baño para discapacitados, a los que se ha incorporado una galería que unifique las nuevas áreas y se conectará directamente con el ala de aulas actual y con el hall de entrada. Al respecto, Debloc manifestó que “esta galería es de gran importancia, ya que la escuela no cuenta con espacios semicubiertos para el uso de los estudiantes y de este modo garantizamos el esquema de escuelas seguras que plantea la cartera educativa”.

Por su parte, en el Jardín 94 se avanza en la construcción de un aula con baños y galería en el marco del programa Escuela Segura para la construcción de 23 aulas para 22 edificios, con una inversión total de 60.950.000 pesos.

En ambos casos, las obras tienen un 45 % de avance y se han adquirido la mayoría de los materiales destinados a la finalización de obra.

**Distrito La Camila**

Posteriormente Debloc y Henny visitaron la escuela secundaria orientada 532 del distrito La Camila para recorrer las instalaciones de la escuela que alberga una matrícula de 13 alumnos.

En la oportunidad, se verificaron las dificultades de filtraciones en la cubierta del establecimiento y, para ello, las autoridades ministeriales entregaron a la directora y a la supervisora seccional un aporte de FANI por $ 735.062 para la reparación de techos y canaletas.

En su visita a los establecimientos educativos, el secretario de Educación mantuvo conversaciones con directivos y docentes acerca del funcionamiento de la modalidad de alternancia y sobre las necesidades de fortalecer el segundo semestre en términos de calidad y de mayor potencialidad para los alumnos de la educación obligatoria.