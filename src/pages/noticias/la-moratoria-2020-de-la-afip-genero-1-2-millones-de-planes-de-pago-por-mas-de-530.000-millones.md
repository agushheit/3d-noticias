---
category: Agenda Ciudadana
date: 2021-01-11T11:20:18Z
thumbnail: https://assets.3dnoticias.com.ar/AFIP_Argentina.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: '3DNoticias - Fuente: Télam'
resumen: 'AFIP: la moratoria 2020 generó 1,2 millones de planes de pago por más de
  $530.000 millones'
title: La moratoria 2020 de la AFIP generó 1,2 millones de planes de pago por más
  de $530.000 millones
entradilla: La información disponible en las bases de datos de la AFIP estima además
  que los contribuyentes que ingresaron a la Moratoria 2020 accedieron a una quita
  promedio del 28,6% de sus deudas.

---
La Moratoria 2020 de la Administración Federal de Ingresos Públicos (AFIP) generó 1,2 millones de planes de pago que permitieron regularizar $533.000 millones en deuda impositiva, previsional y aduanera vencida, según informó el organismo.

El monto total de las obligaciones ingresadas en las dos etapas del plan de facilidades representa el 71,4% de las deudas registradas en los sistemas de la AFIP.

<br/>

## **Las cifras**

A través de sus dos etapas, «la Moratoria 2020 ofreció un instrumento para hacer frente a las dificultades económicas que arrastraba la economía argentina en 2019 y amortiguar el impacto de la pandemia de la Covid-19 sobre todo el entramado productivo», señalaron desde el ente fiscal.

Más adelante, desde la AFIP remarcaron que «el plan de facilidades vigente entre septiembre y diciembre del año pasado generó 813.606 acogimientos por un monto superior a los $426.000 millones. A ese universo se suman las 384.649 adhesiones por 107.000 millones a la moratoria para las micro, pequeñas y medianas empresas establecida a comienzos de 2020 por la Ley de Solidaridad y Reactivación Productiva».

La información disponible en las bases de datos de la AFIP permite estimar que aquellos contribuyentes que ingresaron a la Moratoria 2020 accedieron a una quita promedio del 28,6% de sus deudas, cifra que supone un esfuerzo fiscal superior a los $213.000 millones.

«Con 1,2 millones de planes y más de 533.000 millones de pesos en deuda regularizada, la Moratoria 2020 genera un alivio para que las empresas encaren el proceso de recuperación económica», manifestó la responsable de la AFIP, Mercedes Marcó del Pont.

<br/>

## **Las características de la moratoria**

La Moratoria 2020 fue uno de los instrumentos diseñados por el Gobierno argentino para ofrecer alivio a las familias, comercios, profesionales, pymes, grandes empresas, monotributistas y entidades sin fines de lucro.

La irrupción de la pandemia de la Covid-19 junto con las distintas medidas tomadas para cuidar a todos los argentinos se sumaron a las dificultades económicas que arrastraba la economía y alteraron la situación de todo el entramado productivo.

Ante ese escenario extraordinario, en agosto del año pasado el Congreso de la Nación aprobó la Ley N° 27.562 que amplió el alcance de la moratoria establecida por la Ley de Solidaridad Social y Reactivación Productiva para incluir aquellos contribuyentes -personas humanas y personas jurídicas- que originalmente no habían sido contemplados.

<br/>

## **Para las pymes**

Las pymes explican más del 69% de toda la deuda regularizada durante las dos etapas de la Moratoria 2020 La ampliación de la moratoria no solo extendió el universo de contribuyentes beneficiados, sino que también contempló la posibilidad de regularizar deudas con la AFIP generadas durante la pandemia del Covid-19. Para eso, permitió la inclusión de obligaciones impositivas, previsionales y aduaneras vencidas hasta el 31 de julio de 2020.

<br/>

## **El período de adhesión a la Moratoria 2020 finalizó el 15 de diciembre de 2020**

La medida permitió universalizar la herramienta a todo el entramado productivo, pero las principales beneficiarias de ambas etapas fueron las micro, pequeñas y medianas empresas.

Las empresas de mayor envergadura, segmento que estuvo habilitado a adherirse a partir de la ampliación de la Moratoria 2020, representaron el 13,9% del total.

Por su parte, las entidades sin fines de lucro como clubes de barrio, cooperativas, universidades públicas y asociaciones civiles sumaron el 7% de todas las obligaciones vencidas ingresadas.

En tanto, los pequeños contribuyentes explicaron el 3,3% de los montos ingresados.

<br/>

## **Alcance federal**

La Moratoria 2020 tuvo un alcance federal. La distribución regional de los planes y la deuda regularizada en ambas etapas guarda una estrecha relación con la localización del entramado empresario argentino.

La provincia de Buenos Aires concentró el 32,5% de las presentaciones realizadas y el 25,4% del monto consolidado total. La Ciudad de Buenos Aires explica el 20,7% del total de los planes generados y el 32,2% de la deuda regularizada. Las cifras para ambas jurisdicciones son consistentes con su mayor participación en la cantidad de empresas a nivel nacional.

Las entidades sin fines de lucro generaron cerca de 26.000 planes para regularizar deudas vencidas por unos $37.000 millones.

Las provincias de la Región Centro (Córdoba, Santa Fe, Entre Ríos y La Pampa) explicaron el 23% de las adhesiones y el 17,9% del total de las obligaciones ingresadas.

El universo restante de los planes generados en la Moratoria 2020 se distribuyó entre la región Noroeste (6,8% de las adhesiones y 6,0% de la deuda), Cuyo (6,2% y 5,8%), Patagonia (5,9% y 8,3%) y Noreste (5,0% y 4,4%).

Desde una perspectiva sectorial los resultados de ambas etapas de la Moratoria 2020 evidenciaron el impacto de la pandemia, así como las profundas dificultades económicas registradas a finales de 2019 por todo el entramado productivo.

La industria manufacturera fue el sector que más deuda presentó durante ambas instancias de la moratoria. Las firmas industriales ingresaron obligaciones vencidas por más de $109 mil millones, una cifra que representa el 20,5% del monto total consolidado.

El segundo rubro en relevancia fue Comercio y reparaciones con un monto superior a los $88 mil millones.