---
category: Agenda Ciudadana
date: 2021-05-28T08:37:42-03:00
thumbnail: https://assets.3dnoticias.com.ar/colectivo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Viernes sin colectivos: continúa la medida de fuerza de UTA'
title: 'Viernes sin colectivos: continúa la medida de fuerza de UTA'
entradilla: Los choferes del servicio de transporte urbano e interurbano siguen de
  paro en reclamo de un aumento salarial.

---
Este viernes tampoco habrá servicio de colectivos urbanos e interurbanos en Santa Fe. Se debe a la medida de fuerza llevada adelante por UTA en todo el interior del país, por 48 horas que se cumplen mañana a las 0.

De este modo, no circularán por la ciudad y aquellos trabajadores esenciales, debido a las nuevas restricciones por el coronavirus, deberán buscar otras alternativas de traslado para cumplir con la jornada laboral.

El gremio, argumenta que desde enero vienen solicitando un incremento salarial pero no reciben ninguna propuesta por parte del sector empresarial.

Luego de haber transcurrido el periodo de la Conciliación Obligatoria ordenado por el Ministerio de Trabajo y después de "cuatro meses de reuniones sin ninguna respuesta de los empresarios" fue definido el paro.

Sin vistas de un acuerdo que levante la medida de fuerza, Sebastián Alen, vocero de la UTA local, confirmó a 3D Noticias que sigue vigente la continuidad de la medida de fuerza para el día de mañana.