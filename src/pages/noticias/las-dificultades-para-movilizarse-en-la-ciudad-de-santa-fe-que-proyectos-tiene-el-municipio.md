---
category: La Ciudad
date: 2022-01-24T06:15:14-03:00
thumbnail: https://assets.3dnoticias.com.ar/restriccionesjpg.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'Las dificultades para movilizarse en la ciudad de Santa Fe: qué proyectos
  tiene el Municipio'
title: 'Las dificultades para movilizarse en la ciudad de Santa Fe: qué proyectos
  tiene el Municipio'
entradilla: "Continúa firme la intención de licitar este año el servicio de colectivos,
  en plena debacle. Se sigue buscando financiamiento para implementar un nuevo sistema
  público de bicicletas y sumar ciclovías. \n"

---
El servicio de colectivos es deficiente. Cuesta conseguir taxis y remises. Cada vez hay más motos, y junto con los automóviles pelean por un espacio en las calles. Faltan sendas para bicicletas y ya no están disponibles las del programa Subite a la Bici. Pero los ciudadanos deben trasladarse igual: a trabajar, a estudiar, a hacer trámites y a disfrutar de momentos de esparcimiento. 

 Sin un plan sostenido de movilidad, con metas claras, realizables y que se ajusten a la realidad de Santa Fe, estos problemas tenderán a agravarse. En una entrevista, la nueva directora de Movilidad de la Municipalidad, Andrea Zorzón, analizó estos inconvenientes y planteó qué ideas quiere instrumentar en los dos años que quedan a esta gestión.

 - ¿Cuáles son para Ud. los principales problemas de movilidad que tiene la ciudad de Santa Fe?

\- La pandemia ha profundizado las problemáticas de los sistemas de transporte de personas por taxis y remises y por colectivos, cambiando la forma en que nos movemos en la ciudad, haciendo necesario que repensemos los sistemas de forma más integrada y sustentable con el objetivo de mejorar la calidad de las prestaciones. Además, es necesario trabajar para hacer más seguros los desplazamientos en bicicleta y caminando, con una mirada equitativa sobre el uso del espacio público. Por ello, es importante la decisión del intendente de jerarquizar el área para poder abordar esta agenda, construyendo datos sobre los hábitos de movilidad de los santafesinos y santafesinas y en articulación con los actores de cada uno de los subsistemas.

 La pandemia ha profundizado las problemáticas de los sistemas de transporte de personas por taxis y remises y por colectivos, cambiando la forma en que nos movemos en la ciudad, haciendo necesario que repensemos los sistemas de forma más integrada", dijo Andrea Zorzón, directora de Movilidad del municipio. Foto: Gentileza

 **Colectivos**

 - ¿Qué problemas tiene detectado el Municipio en el sistema de colectivos? ¿Cuál es para Ud. la solución viable para encaminar ese servicio? 

\- El Sistema presenta dificultades operativas que se profundizaron con la pandemia, vinculadas especialmente a la cantidad de unidades de colectivos que integran la flota activa, lo que se refleja en la frecuencia. Además, hay cuestiones relacionadas al estado general de esas unidades que debe mejorar, como la limpieza, o el funcionamiento de la aplicación Cuando Pasa que se relaciona con los GPS de cada uno de los coches. Pero también otras que se vinculan con el financiamiento de sistema, en particular no tenemos previsibilidad sobre el esquema de subsidios nacionales y por lo tanto de los provinciales que dependen en gran medida de ellos y que sumados a los aportes que hace el municipio, representan casi el 70% de los recursos que componen la tarifa en la actualidad. 

Además, como consecuencia de la pandemia otra de las dificultades que tenemos es la reducción de pasajeros. En el año 2021 hemos tenido la mitad de las transacciones del 2019. 

 - ¿Se están preparando los pliegos para licitar este servicio? ¿Habrá más carriles exclusivos para colectivos?

\- En este marco, con el objetivo de mejorar la calidad del servicio, la decisión del intendente es la de licitar el transporte público de colectivos. Algo que no sucede desde finales de la década del 90. En esta definición, que no ha sido tomada en anteriores gestiones, estamos trabajando. La idea es hacerlo de forma articulada con las Universidades de la ciudad quienes ya vienen trabajando en una Mesa de Movilidad, analizando y evaluando distintas opciones, donde un aspecto a considerar serán los carriles exclusivos, entre otros como la intermodalidad. 

 **Taxis**

\-¿Por qué hay pocos taxis en la ciudad? ¿Por qué se atrasó la entrega de licencias? ¿Cómo van a hacer para aumentarlos en poco tiempo para dar respuesta a la demanda ciudadana? 

\- La pandemia ha generado dificultades también en este servicio. Muchos choferes, en virtud de las restricciones a la circulación, debieron reconfigurar la economía de sus familias y buscar otras fuentes de empleo. Además, hay otras cuestiones de escala nacional que tienen que ver, por ejemplo, con la compra de nuevos vehículos y los repuestos para los que ya están en actividad, que también afectan al servicio y exceden la agenda local. 

Ante esta situación, en una mesa de trabajo integrada por representantes de las distintas agencias y de los choferes de taxis y remises -con las particularidades de cada subsistema- estamos avanzando en una agenda de trabajo. Por un lado, de forma articulada con la oficina de empleo municipal y la dirección de seguridad vial vamos a realizar en el mes de marzo una capacitación para personas que quieran ser choferes de taxis y remises, coordinando también el otorgamiento del carnet de conducir correspondiente. 

Y, en relación a las licencias, alrededor de 60, por diversos motivos no se encuentra en explotación y se viene implementando el procedimiento para volver a adjudicarlas. Asimismo, evaluaremos el otorgamiento de nuevas licencias en relación a la cantidad de población que ha aumentado desde el último censo.

 **Bicicletas**

\- ¿Qué pasó con el sistema de bicicletas públicas que había en la ciudad cuando asumió esta gestión? Al comienzo de la pandemia se destinaron al personal de salud, pero nunca regresaron. ¿Dónde están hoy? 

\- Las bicis del Programa Subite a la Bici se otorgaron en comodato a trabajadores de la salud en el marco de la pandemia y aún las encuentran utilizando. 

 - ¿Siguen gestionando financiamiento para instalar un sistema público e inteligente de bicicletas ¿En qué consiste?

\- Efectivamente el intendente viene gestionando fondos para adquirir bicicletas públicas con el objetivo de instalar un sistema de transporte público de bicicletas que sea inteligente y se articule con los restantes subsistemas de movilidad, pensándolos de forma más integrada.

 -¿Se ampliará este año la red de ciclovías. ¿Dónde? ¿Cuántos kilómetros?

\- Hemos presentado proyectos para obtener financiamiento y en la medida que avancen lo vamos a estar anunciando. Nuestro objetivo es ampliar la red de vías seguras para ciclistas y mantener en condiciones la infraestructura que tenemos. 

 **Motos**

\- El año pasado se patentaron más motos que autos, una señal de que a la gente le está resultando más cómodo y económico que el colectivo, pero a la vez, es un problema para el tránsito. ¿Qué planifica para mitigar sus efectos negativos (más inseguridad, más accidentes, más muertes en accidentes)?

\- Al igual que en todo el país muchas personas optan por este medio de movilidad. Es evidente que el parque de motovehículos aumenta y claramente tiene que ver con planes de fomento que se vienen implementando a nivel nacional. 

 Desde el área estamos trabajando articuladamente con la Secretaria de Control para capacitar y sensibilizar en materia de educación vial y se vienen realizando operativos de control en conjunto con la policía para que quienes circulan en la ciudad lo hagan cumpliendo con todos los requisitos. Una agenda de trabajo importante tenemos, además con las concesionarias y empresas que venden motovehículos para que cumplan con la normativa vigente especialmente en relación al otorgamiento de cascos. 