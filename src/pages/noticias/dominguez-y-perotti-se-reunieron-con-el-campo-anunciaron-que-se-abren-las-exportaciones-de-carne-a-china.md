---
category: Agenda Ciudadana
date: 2021-09-29T06:00:55-03:00
thumbnail: https://assets.3dnoticias.com.ar/EXPOCHINA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Domínguez y Perotti se reunieron con el campo anunciaron que se abren las
  exportaciones de carne a China
title: Domínguez y Perotti se reunieron con el campo anunciaron que se abren las exportaciones
  de carne a China
entradilla: El gobernador de Santa Fe participó de la conferencia de prensa donde
  el ministro de Agricultura de la Nación hizo anuncios para el sector.

---
El gobernador Omar Perotti, junto al ministro de Agricultura, Ganadería y Pesca de la Nación, Julián Domínguez, se reunieron con la Mesa de Enlace de Entidades agropecuarias. Luego del encuentro, durante una conferencia de prensa, Domínguez anunció que a partir del próximo lunes se liberan las exportaciones de vaca conserva y manufactura a China.

“Se está exportando con absoluta normalidad y los problemas que quedaban comenzaron a solucionarse”, dijo Domínguez y adelantó la decisión de considerar la situación de los frigoríficos que hicieron nuevas inversiones y no pudieron participar en la distribución de cupos de exportación.

Además, el ministro de Agricultura dijo que el presidente Alberto Fernández le pidió "poner en funcionamiento las cosas que la pandemia nos ha impedido hacer, entre otras cosas avanzar en los entendimientos" en su área de competencia.

Manzur, por su parte, aseguró que la reunión con los representantes de la Mesa de Enlace fue "extremadamente productiva" y afirmó que "la agenda de trabajo fue resulta en su totalidad a través del diálogo".

Tras destacar que "las provincias aportaron la posibilidad de acercar posiciones", Manzur reiteró que se trató de una "reunión fructífera" y adelantó que "se van a intensificar la agenda de trabajo con los equipos técnicos".

Además de Manzur; Domínguez estuvo acompañado por el ministro del Interior, Eduardo De Pedro; los gobernadores Axel Kicillof de la provincia de Buenos Aires; Gustavo Bordet de Entre Ríos; Sergio Zilioto, de la Pampa; Omar Perotti, de Santa Fe; y Gerardo Zamora. de Santiago del Estero, tras la reunión con los representantes de la Mesa de Enlace.

Por la Mesa de Enlace estuvieron el presidente de la Sociedad Rural Argentina (SRA), Nicolás Pino; de Confederaciones Rurales (CRA), Jorge Chemes; de Federación Agraria (FAA), Carlos Achetoni; y de Coninagro, Elbio Laucírica.