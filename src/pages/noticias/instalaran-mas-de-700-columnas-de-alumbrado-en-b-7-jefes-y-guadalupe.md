---
category: La Ciudad
date: 2021-08-19T23:25:39-03:00
thumbnail: https://assets.3dnoticias.com.ar/7JEFES.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Instalarán más de 700 columnas de alumbrado en B° 7 Jefes y Guadalupe
title: Instalarán más de 700 columnas de alumbrado en B° 7 Jefes y Guadalupe
entradilla: 'La colocación de la luminaria led iniciará en el plazo de 15 y 30 días.
  El plan total de iluminación de la Municipalidad tiene un presupuesto que ronda
  los $ 330 millones. '

---
En diferentes barrios de la ciudad uno de los cuestionamientos principales de los vecinos es el de la falta o mal estado de la luminaria pública. En mayo de este año la propia vecinal de 7 Jefes había elaborado un mapa para denunciar las deficiencias en el alumbrado, donde ubicaron 130 luminarias que estaban fuera de servicio en el sector comprendido entre Bulevar Gálvez, al sur; Salvador del Carril al norte; Av. 7 Jefes y Costanera al este y Vélez Sarsfield al oeste.

Para revertir esta situación y continuar con la puesta a punto de la ciudad entorno a la iluminación, la Municipalidad de Santa Fe presentó los trabajos que se ejecutarán para la colocación de luces led en los barrios 7 Jefes y Guadalupe., en el marco del plan "Santa Fe se ilumina".

Este jueves en la Estación Belgrano, el intendente Emilio Jatón procedió a la firma de los contratos de licitación con los cuales se darán inicio a las tareas en los barrios Guadalupe (donde se pondrán 405 columnas) y 7 Jefes (con 310 columnas nuevas), trabajos que llevará adelante la empresa santafesina Tacuar SRL y que comenzarán en el plazo de 15 y 30 días.

"Estamos en una primera etapa y el año que viene, al igual que el bacheo, esto va a continuar. Debe ser una política de Estado iluminar la ciudad y arreglar sus calles", destacó en rueda de prensa el intendente Emilio Jatón, y resaltó: "Queremos que Santa Fe se ilumine enserio".

**40% con luces led**

El objetivo que planteó el mandatario santafesino fue que para fin de año Santa Fe tenga un 40% de su luminaria con luces led. "Esta iluminación nos hace bajar el consumo y reducir el impacto ambiental, queremos empezar a hacer una ciudad sustentable. Hoy estamos por abajo del 20% y el objetivo es claro para esta primera etapa", señaló. Según informaron desde el municipio, el presupuesto total para el plan de iluminación ronda los $ 330 millones e incluirá más de 3.000 columnas nuevas de alumbrado.

Ante las críticas que recibió hace unos meses la actual gestión municipal de parte de algunos concejales opositores respecto al deficiente potencial lumínico de la ciudad, Jatón respondió: "Algunos deberían acordarse que los focos no se quemaron en dos meses, sino que teníamos un solo camión para reparar (las luminarias), no teníamos elementos para cambiar focos, pero todo eso ya pasó. Ahora somos responsables nosotros y por eso pusimos en marcha este plan y que va a dar sus frutos en un par de meses".

**Cifras**

De las obras contempladas en el plan de iluminación, las primeras concluidas fueron las correspondientes a las grandes avenidas y a las zonas que mayor transitabilidad presentan, tal es el caso de 25 de Mayo, los bulevares Gálvez y Pellegrini, y Rivadavia. En todas éstas se reemplazaron los artefactos de manera individual y se avanza actualmente en el ingreso a la ciudad por Circunvalación a la altura del hipódromo, y en el extremo sur, por avenida Alem. De igual modo, se completaron las tareas en callejón Roca, que presenta ya su nuevo sistema de iluminación y en Las Flores I, cuyo tendido se encontraba obsoleto.

Respecto de las que se encuentran en marcha, los barrios Scarafía y Liceo Norte presentan el mayor grado de avance; con el 90% de los trabajos ejecutados, cada uno de estos barrios contará en un corto plazo con 100 columnas con artefactos led, cableado y tablero nuevo. En tanto, en Santa Marta y Ceferino Namuncurá se desarrolla la primera etapa de las tareas, con 86 columnas.

Lo mismo sucede en Yapeyú Oeste, donde se trabaja en la instalación de 285 luminarias, en este caso en uno de los barrios que más requiere de iluminación pública. Por otra parte, en los próximos días iniciarán las obras correspondientes en Siete Jefes, Guadalupe Noreste y Central Guadalupe, Fomento 9 de Julio y barrio Roma.

A su vez, desde el municipio mencionan que los trabajos de infraestructura iniciados, como es el caso de la transformación integral de Camino Viejo y de calle Beruti, fueron diagramados teniendo en cuenta el nuevo sistema de iluminación. Cada una de estas obras contempla, por ejemplo, la instalación de los tableros correspondientes para los sistemas de iluminación correspondientes, incluyendo térmicas, disyuntores y contactores.

**En avenidas y más barrios**

Lo último que el municipio puso en marcha es una licitación para los trabajos denominados "Avenidas de tu barrio", para dotar con 1.135 luces led las calles de acceso y de mayor transitabilidad, haciendo recambio de las actuales de sodio en muchos casos para lograr entornos seguros. Este tramo de obras del Plan de Iluminación se lleva adelante con fondos del Plan Incluir del gobierno provincial.

El objetivo de este tramo de los trabajos es abordar las avenidas de penetración a los barrios, con mejoras que permitan el desplazamiento de cada vecino y vecina, priorizando las vías de circulación del transporte público, las áreas con inmuebles públicos en salud, seguridad y educación, y finalmente las arterias de mayor circulación.

Por otra parte, se está trabajando en etapas de ampliación del plan. Está en proceso de relevamiento de necesidades de iluminación en los entornos de las escuelas, obra que se hará en conjunto con el Fondo de Asistencia Educativa (FAE). Y también en cuatro barrios -Los Hornos, 29 de Abril, Varadero Sarsotti y Transporte-, en el marco del Renabap (Registro Nacional de Barrios Populares) que son obras que serán financiadas por la Secretaría de Integración Socio Urbana de la Nación, y que forman parte del Plan Integrar municipal.

**Reemplazo y mantenimiento**

El plan de iluminación también implica el mantenimiento del alumbrado público existente, a través de la dirección de Alumbrado Público y Electromecánica del municipio. "Se procedió a equipar el área con los correspondientes repuestos, con un presupuesto cercano a los 40 millones de pesos", destacaron desde el gobierno local.

También informaron que, para facilitar las tareas de recambio, se adquirieron dos nuevos vehículos que en los próximos días serán entregados a la dirección de Alumbrado Público y Electromecánica. Se trata de camionetas cero kilómetro con hidro grúa, que vienen a paliar el déficit automotriz del que padece la municipalidad. Esto permitirá atender la demanda diaria de luminaria a reemplazar.