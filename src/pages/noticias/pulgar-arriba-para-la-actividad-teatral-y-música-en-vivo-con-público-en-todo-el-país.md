---
layout: Noticia con imagen
author: "Fuente: EL Litoral"
resumen: Teatros y música en vivo
category: Agenda Ciudadana
title: '"Pulgar arriba" para la actividad teatral y música en vivo con público
  en todo el país'
entradilla: Se habilitó en todo Argentina la realización de artes escénicas con
  y sin asistencia de espectadores. También, las competencias de motociclismo
  deportivo.
date: 2020-11-13T19:39:17.718Z
thumbnail: https://assets.3dnoticias.com.ar/teatros.png
---
A través de la Decisión Administrativa 2045/2020, publicada este viernes en el Boletín Oficial, se exceptuó del aislamiento a las personas afectadas a las actividades relativas al desarrollo de artes escénicas con y sin asistencia de espectadores.

De acuerdo a la resolución, "las actividades autorizadas deberán desarrollarse dando cumplimiento al Protocolo General para la Actividad Teatral y Música en Vivo con Público, aprobado por la autoridad sanitaria (...)".

Dando más detalles, se destaca que la autorización alcanza "a las personas que desarrollen las actividades autorizadas en virtud de lo dispuesto por el artículo 1º de la presente, en lo relativo a la realización de eventos culturales y recreativos relacionados con la Actividad Teatral y Música en Vivo, en espacios públicos o privados que impliquen concurrencia de personas y a la actividad de teatros y centros culturales".

Se agrega que "en todos los casos se deberá garantizar la higiene y, cuando correspondiere, la organización de turnos de modo tal que se garanticen las medidas de distanciamiento necesarias para disminuir el riesgo de contagio de COVID-19".

**Cada provincia debe reglamentarla**

Según el Boletín Oficial, "cada Provincia y la Ciudad Autónoma de Buenos Aires podrán dictar las reglamentaciones necesarias para el desarrollo de la actividad referida en el artículo 1°, estableciendo la fecha para su efectiva reanudación, pudiendo implementar gradualmente, suspender o reanudarla, en el ámbito de su competencia territorial y conforme la situación sanitaria, pudiendo limitar el alcance de la excepción a determinadas áreas geográficas o a determinados municipios, o establecer requisitos específicos para su desarrollo que atiendan a la situación epidemiológica local y a las características propias del lugar, con el fin de minimizar el riesgo de propagación del virus".

Y agrega: "Las excepciones otorgadas a través del artículo 1º podrán ser dejadas sin efecto por la autoridad local, en el marco de su competencia territorial, en forma total o parcial, en virtud de las recomendaciones de la autoridad sanitaria provincial, y conforme la evolución epidemiológica de la pandemia de COVID-19, debiendo comunicar tal decisión al Jefe de Gabinete de Ministros".

**El protocolo y los alcances:** [Ver Protocolo](https://assets.3dnoticias.com.ar/484107937-anexo-6160321-1.pdf)

**La resolución:** [Ver Resolución](https://assets.3dnoticias.com.ar/aviso_237264.pdf)

**Motociclismo deportivo**

Por otra parte, a través de la Decisión Administrativa 2044/2020, se autorizó la organización de competencias de motociclismo deportivo.

**La resolución:** [Ver Resolución](https://assets.3dnoticias.com.ar/aviso_237265.pdf)