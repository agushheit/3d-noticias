---
category: Estado Real
date: 2021-01-27T09:53:52Z
thumbnail: https://assets.3dnoticias.com.ar/villano.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: Continúa la vacunación contra el COVID19 para mayores de 60 años
title: Continúa la vacunación contra el COVID19 para mayores de 60 años
entradilla: El director de la Región Santa Fe de Salud y del Hospital Iturraspe se
  aplicaron la vacuna Sputnik V.

---
Durante la mañana del martes el director de la Región Santa Fe, Rodolfo Rosselli y su par del Hospital Iturraspe, Francisco Villano junto a otros trabajadores de salud se aplicaron la vacuna Sputnik V, dado que la ANMAT aprobó recientemente esa franja etaria.

Ante esto, Rodolfo Rosselli expresó: “Estamos muy contentos, esto implica cuidarme y cuidar a los demás, hoy somos un grupo de mayores de 60 años que tenemos la posibilidad de acceder a la primera dosis de la vacuna Sputnik V y nos alegra mucho”.

Del mismo modo, Francisco Villano agregó, “Es un día especial para las personas que nos podemos aplicar esta vacuna porque en este tipo de enfermedades a través de la vacuna, tenemos la forma más efectiva de cuidarnos, con la generación de anticuerpos, sin enfermarnos ni poner en riesgo a otras personas”.

**Expectativa por la llegada de más dosis para el Iturraspe**

“En el hospital se recibieron 650 primeras dosis que fueron aplicadas, van a ingresar 450 primeras dosis más y esta semana comenzamos a inocular con segundas dosis también.

Estamos avanzando positivamente porque es muy buena la recepción de la vacuna, el hecho de que no han presentado efectos indeseables y esto genera que se espere con ansias”, destacó Villano.

**La situación epidemiológica de Santa Fe**

Consultado sobre la disminución de casos en la ciudad, el director de la Región Santa Fe de Salud reconoció que “el hecho de realizar el test a todos los casos que son contacto estrecho -anteriormente, si la persona presentaba síntomas compatibles con el Covid se lo consideraba positivo- es decir la realización del hisopado y no el positivo por nexo epidemiológico, puede ser una de las razones de la reducción en el número de casos”.

“De todas maneras -destacó- continuamos con números altos, seguimos con todas las medidas de detección temprana, estamos trabajando con el operativo Verano Seguro, en diferentes puntos de la ciudad de Santa Fe, realizando los test de anosmia, estos números altos presentan una tendencia a sostenerse, es decir con más casos de los deseados”.

“Los factores de estos números son múltiples, la época del año, las fiestas, las vacaciones y los cuidados personales en los cuales insistimos permanentemente”, remarcó el director de la Región Santa Fe de Salud.