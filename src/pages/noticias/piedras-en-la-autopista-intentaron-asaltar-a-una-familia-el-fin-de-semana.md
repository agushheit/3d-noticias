---
layout: Noticia con imagen
author: "Fuente: LT10"
resumen: Piedras en la Autopista
category: La Ciudad
title: "Piedras en la Autopista: intentaron asaltar a una familia el fin de semana"
entradilla: Repitieron un método que ya causó muchos accidentes y robos en la
  zona. Afortunadamente no hubo que lamentar daños mayores.
date: 2020-11-17T14:17:38.465Z
thumbnail: https://assets.3dnoticias.com.ar/autopista.jpeg
---
Este fin de semana se repitió un escenario ya casi común en la ciudad de Santa Fe. Otra vez, pedazos de piedras en la Autopista Santa Fe- Rosario facilitaron la emboscada y el intento de robo a una familia.

Ocurrió este fin de semana en el kilómetro 145, a la altura del barrio Las Vegas de la ciudad de Santo Tomé, donde una familia constituida por un hombre de 41 con su mujer de 37 y sus dos hijos, uno de 16 y otro de 11, fueron asaltados luego de que impactaran con el vehículo familiar con las piedras en el medio de la calzada.

Si bien el auto sufrió roturas como parte del tren delantero, su conductor no perdió el control del mismo, pero si tuvo que maniobrar hasta terminar en el cantero central de la autopista.

Al descender, lo emboscaron cuatro delincuentes que lo lesionaron en uno de sus tobillos por los delincuentes. Ante la intervención y resistencia de los hijos del matrimonio, los delincuentes desistieron y se dieron a la fuga.

**Denuncian que el puesto policial que solía estar en la zona -por los constantes episodios de inseguridad- ya no está.**