---
category: Agenda Ciudadana
date: 2021-11-23T06:00:29-03:00
thumbnail: https://assets.3dnoticias.com.ar/mix.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: 'Milei se vacunó contra el coronavirus: “Necesito facturar dando conferencias
  en el exterior”'
title: 'Milei se vacunó contra el coronavirus: “Necesito facturar dando conferencias
  en el exterior”'
entradilla: "Más allá de haber dicho que no pensaba hacerlo, el diputado libertario
  electo por CABA finalmente lo hizo. \n"

---
El diputado nacional electo por la Ciudad de Buenos Aires Javier Milei confirmó hoy que recibió las dos dosis de vacunas Sinopharm, más allá de que durante la campaña electoral había planteado su negativa a ser inmunizado contra el virus del Covid-19.

“Me tuve que vacunar porque no vivo de la política ni del Estado. Para facturar necesito dar conferencias en todo el país y en el exterior”, señaló.

Y expresó: “No soy un parásito del Estado. De hecho, a partir del 9 de diciembre renunciaré a mi trabajo actual en el sector privado para que no haya conflictos éticos ni malos entendidos. Y como prometí en campaña, tampoco cobraré mi dieta, ya que sortearé la misma entre los ciudadanos”.

“¿De qué viviré? Como también prometí en campaña se creará un mecanismo para que sean los mismos ciudadanos quienes decidan cuánto cobre cada mes. Esto será con un proceso absoluto de transparencia y en el marco de la ley de ética pública. No le saldré un peso a los argentinos”, resaltó Milei en declaraciones a Infobae.

Y agregó: “Son los costos que tengo que pagar para seguir defendiendo las ideas de la libertad más allá de la Argentina”.

El economista tiene previsto brindar conferencias en Miami, Nueva York y Washington DC, como así también dará charlas en Uruguay, Chile, México, Colombia, Ecuador y España.