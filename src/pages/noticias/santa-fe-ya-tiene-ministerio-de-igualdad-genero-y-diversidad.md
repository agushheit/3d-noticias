---
category: Estado Real
date: 2021-05-28T08:18:44-03:00
thumbnail: https://assets.3dnoticias.com.ar/genero.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: Santa Fe ya tiene Ministerio de Igualdad, Género y Diversidad
title: Santa Fe ya tiene Ministerio de Igualdad, Género y Diversidad
entradilla: La Legislatura aprobó el proyecto de Ley enviado por el gobernador Omar
  Perotti para transformar a la Secretaria de Estado de Igualdad y Género en Ministerio.

---
La actual secretaria de Estado de Igualdad y Género de la provincia, Celia Arena expresó: “Este es solo un paso más en el camino que demuestra la voluntad política de esta gestión de hacer de esta provincia un lugar donde se aceptan las diferencias pero no la desigualdades”.

Y agregó: “La igualdad y la lucha contra la violencia por motivos de género constituyen una política de Estado para esta gestión y así lo dejó en claro el gobernador Omar Perotti en su discurso de apertura de sesiones. Tenemos la firme convicción de abordar estas problemáticas desde sus causas estructurales, con políticas institucionalizadas, transversales y que tengan llegada a cada rincón de la provincia y en ese camino nos encontramos” 

**Sobre el proyecto de Ley**

Durante la visita de la Ministra Elizabeth Gómez Alcorta el pasado martes 13 de abril el gobernador anunció su intención de enviar el proyecto de Ley para que la Secretaría de Estado de Igualdad y Género se transforme en Ministerio, hecho que se concretó días después. 

El 29 de abril el proyecto recibió la media sanción de la Cámara de Senadores de la provincia y finalmente este jueves fue ley la creación del nuevo Ministerio de Igualdad, Género y Diversidad de la provincia de Santa Fe.