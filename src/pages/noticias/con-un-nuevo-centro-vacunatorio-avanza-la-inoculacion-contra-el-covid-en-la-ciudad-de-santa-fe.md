---
category: La Ciudad
date: 2022-01-27T06:00:48-03:00
thumbnail: https://assets.3dnoticias.com.ar/VACUNATORIOATE.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Con un nuevo centro vacunatorio, avanza la inoculación contra el covid en
  la ciudad de Santa Fe
title: Con un nuevo centro vacunatorio, avanza la inoculación contra el covid en la
  ciudad de Santa Fe
entradilla: Esta ubicado en el Centro de salud ATE, Rivadavia 3325. Se mantienen La
  Redonda, La Esquina Encendida y la sede del CUEC. Casi 12 millones de vacunados
  con tres dosis en el país.

---
Este jueves el gobierno provincial abre un nuevo centro vacunatorio en la ciudad de Santa Fe. Se trata del Centro Integral de Salud del gremio ATE, ubicado en calle Rivadavia al 3325 en el macrocentro de la capital provincial. 

 De la mano de esta apertura, el ministerio de salud santafesino citó para la jornada a 6.356 ciudadanos para recibir terceras dosis de la vacuna contra el Covid-19. 

 Según se informó, en La Esquina Encendida (Estanislao Zeballos y Facundo Zuviría) se turnaron a 3.500 vecinos de 8 a 14.30; allí se colocará tercera dosis de Sputnik V. 

 Mientras que en La Redonda (Salvador del Carril y Belgrano), se citaron a 2.506 santafesinos para la colocación de la tercera dosis con la vacuna Moderna, también de 8 a 14.30. 

 El cronograma de este jueves también incluye 230 turnos otorgados para recibir la tercera dosis contra el coronavirus en el Centro Integral de ATE, de 8 a 19 hs. Allí se colocará Moderna. Mientras que otros 120 lugares fueron otorgados a la sede gremial del CUEC, ubicada en San Martín 2800. 

 **Aplicadas**

 De acuerdo a la información que brindó este miércoles el gobierno provincial, a la fecha los números de la vacunación contra el coronavirus en Santa Fe es la siguiente: 

 1era dosis: 3.164.993

2da dosis: 2.924.219

3era dosis: 938.210

 A nivel país, los números informados por Nación son los siguientes:

 1era dosis: 39.494.515

2da dosis: 34.745.698

3era dosis: 11.911.737