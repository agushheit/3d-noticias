---
category: Estado Real
date: 2021-11-23T21:44:06-03:00
thumbnail: https://assets.3dnoticias.com.ar/fondofidu.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Provincia gestionó terrenos fiscales para sumar 204 viviendas PROCREAR
  en a cuidad de Santa Fe
title: La Provincia gestionó terrenos fiscales para sumar 204 viviendas PROCREAR en
  a cuidad de Santa Fe
entradilla: Se trata de 20 mil metros cuadrados que en 2020 fueron usurpados y posteriormente
  desalojados. Parte de esos terrenos serán transferidos al Fondo Fiduciario Público.

---
El Ministerio de Infraestructura, Servicios Públicos y Hábitat, a través de la Secretaría de Hábitat, inició gestiones en 2020, ante el gobierno Nacional, para construir viviendas en el marco del plan Procrear en los terrenos fiscales ubicados en la zona norte de la ciudad de Santa Fe, entre Facundo Zuviría y Aristóbulo del Valle, cercanos al Centro Integrador Comunitario de Facundo Zuviría al 8000.

Al respecto, la ministra de Infraestructura, Servicios Públicos y Hábitat, Silvina Frana, recordó que “al inicio de la gestión, en febrero de 2020, estos terrenos fiscales fueron usurpados por un numeroso grupo de familias, donde el gobierno de la provincia se puso al frente del problema para buscar una solución conjunta entre las autoridades locales, los vecinos y las fuerzas policiales y nacionales. Lo que primero se hizo es que la policía impidiera el avance de más gente sobre el terreno usurpado, y mientras la justicia analizaba la situación, los equipos sociales censaron a las familias que se habían asentado en el lugar”.

“A raíz de ese trabajo que desarrollamos aplicando el ´protocolo de actuación ante casos de usurpación´, que elaboramos interministerialmente, detectamos que hubo venta ilegal de terrenos y que de las 200 familias que habían tomado ese terreno, luego del censo y relevamiento que hicimos, quedaron 15, sobre las cuales hicimos un trabajo interdisciplinario para encontrar una solución a su déficit habitacional”, precisó la ministra.

Y agregó: “Desde entonces, a través de la Secretaría de Hábitat, comenzamos a gestionar para que esas tierras sean destinadas a la construcción de viviendas. Y hoy, a través de la resolución del ABBE comienza a concretarse este trabajo que nos dimos en lo peor de la pandemia, para que 204 familias accedan al sueño de la casa propia”, concluyó Frana.

Por su parte, el secretario de Hábitat, Urbanismo y Vivienda, Amado Zorzón, indicó que “el ABBE publicó la Resolución 181/2021 en el Boletín Oficial de la Nación, indicando que se destinan estos terrenos para la construcción de Desarrollos Urbanísticos, lo cual nos llena de alegría, porque 204 familias podrán acceder a una vivienda con todos los servicios”.

“Desde el primer día de la gestión, el gobernador Omar Perotti nos pidió trabajar intensamente para dar respuestas al enorme déficit habitacional que existe en nuestra provincia, y así lo estamos haciendo, construyendo más de 4 mil viviendas y proyectando muchas más, articulando con el gobierno nacional y desde la Dirección Provincial de Viviendas”, finalizó.

**Las viviendas**  
Se trata de 204 unidades habitacionales que se construirán como Desarrollo Urbanístico y tendrán planta baja y dos pisos, ocupándose cuatro medias manzanas para la construcción de viviendas y el resto, será utilizado como espacio verde.

El proyecto actualmente se encuentro en etapa de desarrollo y una vez finalizado, se realizará el llamado a licitación para ejecutar la obra.