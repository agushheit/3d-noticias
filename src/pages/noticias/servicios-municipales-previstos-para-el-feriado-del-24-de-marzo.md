---
category: La Ciudad
date: 2021-03-24T06:44:35-03:00
thumbnail: https://assets.3dnoticias.com.ar/turismo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad Santa Fe
resumen: " Servicios municipales previstos para el feriado del 24 de marzo"
title: " Servicios municipales previstos para el feriado del 24 de marzo"
entradilla: |2-

  La Municipalidad informa sobre los servicios que se garantizan a la población durante este miércoles. También se comunica el cambio en los horarios en el Cementerio Municipal que regirán a partir de abril.

---
Con motivo del feriado nacional por el Día de la Memoria, la Verdad y la Justicia, que se conmemora este miércoles 24 de marzo, la Municipalidad informa los horarios de los servicios de recolección de residuos, cementerio, transporte público y estacionamiento medido.

**–Recolección de residuos:**

Este miércoles, no se presta el servicio de recolección de residuos, por lo que se solicita a la población no sacar la basura durante esa jornada.

**-Cementerio:**

Las puertas permanecerán abiertas de 8 a 12 horas, con motivo del feriado, para las visitas del público.

En tanto, a partir del 1° de abril, regirán cambios en los horarios del Cementerio Municipal para los visitantes:

De lunes a sábados: de 9 a 16 horas

Domingos y feriados: de 8 a 12 horas

**-Transporte público**

Las líneas del transporte público brindarán su servicio con frecuencias reducidas, similares a las de un día domingo.

**-SEOM**

El Sistema de Estacionamiento Medido (SEOM) no estará operativo.

**-Centros de información turística**

Por  consultas sobre información y actividades municipales, los santafesinos y/o quienes visiten la ciudad este feriado podrán acercarse hasta los siguientes centros de informes:

Terminal de Ómnibus (Belgrano 2910): de 8 a 20 horas.

Puerto: de 18 a 18 horas.