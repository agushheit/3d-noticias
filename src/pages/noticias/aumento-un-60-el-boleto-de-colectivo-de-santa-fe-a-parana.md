---
category: La Ciudad
date: 2021-10-19T06:00:30-03:00
thumbnail: https://assets.3dnoticias.com.ar/etaceer.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Aumentó un 60% el boleto de colectivo de Santa Fe a Paraná
title: Aumentó un 60% el boleto de colectivo de Santa Fe a Paraná
entradilla: Las dos empresas que brindan servicio, Fluviales y Etacer, ya actualizaron
  sus tarifas para viajar entre Santa Fe y Paraná. Los nuevos costos del pasaje.

---
A partir de esta semana rige un incremento de casi un 60 por ciento en el boleto de transporte público que une Santa Fe con la ciudad de Paraná y viceversa.

Las dos empresas que brindan servicio, Fluviales y Etacer, aumentaron el precio del pasaje que conecta a ambas capitales provinciales; una conexión clave en la región.

De esta forma, y desde hoy, el costo del pasaje sale 55 pesos a través de la tarjeta Sube. Cabe recordar que costaba 34,90 pesos; lo que representa una suba del 57,59 por ciento.

De acuerdo a lo manifestado por las empresas, a pesar del proceso inflacionario, hace más de dos años que no se actualizaba la tarifa. Vale señalar que los reclamos de los usuarios por el funcionamiento de este servicio es constante, por la disminución de frecuencias respecto a la prepandemia.

Cabe aclarar que al tratarse de un servicio nacional, quien regula, controla y define la tarifa es el Ministerio de Transporte de la Nación.