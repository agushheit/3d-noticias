---
layout: Noticia con imagen
author: "Fuente: Noticias del Gobierno de la provincia de Santa Fe"
resumen: Actividades náuticas y pesca
category: Estado Real
title: La provincia habilitó la pesca y las actividades náuticas recreativas
entradilla: "A partir de este sábado, en las localidades de los departamentos:
  Rosario, La Capital, General López, Caseros, Constitución, San Lorenzo, Las
  Colonias y Castellanos."
date: 2020-10-31T01:35:00.000Z
thumbnail: https://assets.3dnoticias.com.ar/actividad-n%C3%A1utica-6370-.png
---
A través del decreto N° 1186, el gobierno provincial habilitó la **pesca y las actividades náuticas recreativas** desde las 0 horas de este sábado en las localidades de los departamentos: Rosario, La Capital, General López, Caseros, Constitución, San Lorenzo, Las Colonias y Castellanos; que se encuentran en aislamiento social preventivo y obligatorio de acuerdo a lo que establece el gobierno nacional.

La norma permite la **pesca deportiva** y recreativa, en la modalidad desde costa y embarcados,  y la **navegación recreativa**; en ambos casos, con un 50 % de la capacidad del transporte o menor de no garantizarse el distanciamiento social.

Asimismo, habilita las actividades de los clubes deportivos vinculados a las actividades  mencionadas anteriormente  y las actividades de guarderías náuticas, a los fines del retiro y depósito de las embarcaciones, previa presentación de los protocolos sanitarios correspondientes ante el Ministerio de Trabajo, Empleo y Seguridad Social.

En todos los casos, las actividades podrán realizarse de 7 a 19 horas. Las actividades grupales continúan suspendidas, inclusive al aire libre: no están permitidas las actividades grupales de varias embarcaciones o reuniones sociales dentro de las mismas.

Por último, el decreto establece que quienes realicen estas actividades  no deberán trasladarse a más de treinta kilómetros de su lugar de residencia habitual, debiendo tramitar, si ese fuera el caso, el Certificado Único Habilitante para Circulación - Emergencia Covid-19, a excepción de los desplazamientos entre las localidades comprendidas en los aglomerados urbanos Gran Rosario y Gran Santa Fe.