---
category: Agenda Ciudadana
date: 2021-09-02T06:15:17-03:00
thumbnail: https://assets.3dnoticias.com.ar/EXPOCARNE.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: El límite a las exportaciones terminará cuando el mercado esté "plenamente
  estabilizado"
title: El límite a las exportaciones terminará cuando el mercado esté "plenamente
  estabilizado"
entradilla: '"Estamos saliendo de una crisis y pensando en todos los argentinos. El
  Gobierno quiere proteger tanto a la industria para que exporte como a los precios
  del mercado interno", dijo el ministro de Trabajo, Claudio Moroni.'

---
El ministro de Trabajo, Claudio Moroni, afirmó este miércoles que el mercado local "se está acomodando" y que cuando esté "plenamente estabilizado" el Gobierno abrirá las exportaciones de carne vacuna "en su totalidad".

En el sector "no están pasando un mal momento. Sí puede ser que porque los precios se acomodaron un poco internamente, estén ganado un poco menos, es probable. Pero bueno, estamos saliendo de una crisis y pensando en todos los argentinos", sostuvo Moroni en diálogo con la prensa en Casa de Gobierno.

"El Gobierno quiere proteger tanto a la industria para que exporte como a los precios del mercado interno. El mercado local se está acomodando y cuando veamos que está plenamente estabilizado serán abiertas las exportaciones en su totalidad", enfatizó el ministro de Trabajo.

Moroni fue uno de los funcionarios nacionales que respaldaron este miércoles la decisión de extender hasta el 31 de octubre las restricciones para exportar carne vacuna, al considerar que la medida "está rindiendo sus frutos".

Desde el sector privado, el Consejo Agroindustrial Argentino (CAA) y la Mesa de las Carnes rechazaron la medida oficial y advirtieron las "implicancias negativas" que esta prórroga puede tener en la cadena.

Por su parte, la secretaria de Comercio Interior, Paula Español, dijo que la medida que limita las exportaciones al 50 % de lo despachado el año pasado, sin que la cuota Hilton, 481, la de Estados Unidos y la carne kosher a Israel se vean afectadas, logró "contener el precio de la carne efectivamente en el mes de julio y se mantiene esta misma tendencia”.

Además, la funcionaria destacó una recuperación en el consumo interno, ya que se "está viendo una recuperación en los kilos por cápita de carne que ya están superando los 52 kilos en este último mes”.

Según Español, la medida “está rindiendo sus frutos” y, por ese motivo, se necesita que “se extienda un tiempo más” para, de esa forma, “seguir cuidando la mesa de los argentinos”.

“Lo que es importante es que esta contención y leve reducción de precios se da en los distintos eslabones de la cadena desde la producción primaria a la media res, y luego eso se traduce en las góndolas”, evaluó.

Respecto a la decisión de las entidades agropecuarias que integran la Mesa de Enlace de llevar a cabo "acciones" entre las cuales toma fuerza un cese de comercialización en rechazo a la medida oficial, Español opinó que “no están dadas las condiciones ni hay razón para tener este tipo de medidas”.

En este sentido, los convocó para “trabajar en conjunto” con el objetivo de “mejorar la producción y la productividad del sector”, y “cuidar que la carne llegue a todos y todas al precio que corresponde”.

“Los convocamos a seguir trabajando en medidas estructurales importantes que nos permitan pensar en un Plan Ganadero y una solución estructural para el sector”, añadió.

Por su parte, el CAA y la Mesa de las Carnes se acoplaron al rechazo de las entidades agropecuarias y del Consorcio de Exportadores de Carne ABC.

El CAA, entidad que mantiene un diálogo fluido con el Gobierno nacional y que está integrada por 64 asociaciones agroindustriales, sostuvo -en una carta enviada el presidente Alberto Fernández- que con la extensión de la medida se "prolonga el impacto negativo en la cadena poniendo en riesgo el sostenimiento del empleo, el futuro desarrollo productivo, y la credibilidad con nuestros clientes del exterior."

Por último, el CAA reiteró "la vocación de diálogo pero conducente a la búsqueda de soluciones rápidas que promuevan el levantamiento de las medidas restrictivas actuales, así como acuerdos en relación al abastecimiento local, principios básicos para construir en conjunto un plan de desarrollo y crecimiento de la cadena bovina en la Argentina con visión de mediano y largo plazo".

En la misma sintonía, la Mesa de las Carnes, integrada por 34 entidades del sector, sostuvo que "decir que la medida tuvo resultado positivo es querer sostener un error con una mentira" y se preguntó si tiene sentido "seguir con una medida que afecta negativamente a trabajadores, a la inversión, a eslabones productivos, a la recaudación tributaria y a la liquidación de divisas de exportación".