---
category: Agenda Ciudadana
date: 2021-08-18T06:00:52-03:00
thumbnail: https://assets.3dnoticias.com.ar/INDUSTRIA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: La industria creció 3,9% en julio y se empiezan a recuperar sectores más
  golpeados por la pandemia
title: La industria creció 3,9% en julio y se empiezan a recuperar sectores más golpeados
  por la pandemia
entradilla: Los indicadores de consumo relevados por la muestra manifestaron signos
  de recuperación en los rubros más afectados, como actividades turísticas e indumentaria
  y calzado.

---
La actividad manufacturera en julio creció 3,9% por encima del mismo mes de 2019 y un 5,2% por encima del promedio de aquel año en que aún no se registraba la afectación de la pandemia de coronavirus, de acuerdo al informe de Panorama Productivo del Centro de Estudios para la Producción (CEP XXI).

"Estos datos reafirman la tendencia positiva de junio, mes en el cual la actividad industrial alcanzó el mayor nivel desde mayo de 2018", señaló en la tarde de este martes el Ministerio de Desarrollo Productivo al dar a conocer el informe del CEP XXI.

Al mismo tiempo, los indicadores de consumo relevados por la muestra manifestaron signos de recuperación en los rubros más afectados por la pandemia, como actividades turísticas e indumentaria y calzado.

El informe elabora un índice anticipado de actividad en base al consumo de energía en las fábricas, e informa una suba del 3,9% en julio frente al mismo mes de 2019, y del 5,2% por encima del promedio de 2019.

De 1.020 plantas industriales, el 52,8% consumió más energía que en julio de 2019, en tanto que las exportaciones registraron en junio su nivel más alto desde 2014 ya que crecieron 41,7% en comparación con el mismo mes de 2019; en tanto, la inversión productiva también superó los niveles de hace dos años.

El informe destacó que sectores como químicos, maquinarias y equipos y automotriz lideraron el crecimiento de la industria. El sector automotriz en julio produjo 31.935 unidades, lo que representa un 47,5% más que dos años atrás.

Así, en el acumulado de los primeros siete meses la producción fue 23,3% mayor que en el mismo período de 2019, período en el que maquinaria y equipo tuvo una suba del 35,4% en junio respecto al mismo período de 2019, según Indec.

El reporte también destacó que las fábricas productoras de materiales para la construcción retomaron el crecimiento tras la desaceleración de los primeros meses de 2021: los despachos de cemento tuvieron el mejor julio desde 2015.

El informe elabora un índice anticipado de actividad en base al consumo de energía en las fábricas

En un trabajo comparativo, se reseñó que la Argentina se destacó en el promedio industrial general frente a lo ocurrido en Brasil, Chile, España, Francia, Italia, México, Estados Unidos, India, Alemania y Japón, y que en 9 de 11 sectores el desempeño fue mejor al promedio de esos países.

En paralelo, la mejora de la actividad industrial viene impulsando la inversión productiva y en el primer trimestre de 2021, la inversión superó en 14,3% al promedio de 2019, impulsada por las maquinarias y equipos, particularmente de origen nacional.

"La mayor demanda por bienes de capital está impactando positivamente en el empleo de los fabricantes nacionales de maquinarias", se explicó.

Así, comparando abril de 2021 contra abril de 2019, el empleo formal en las empresas productoras de maquinarias se expandió en un 4,5%, lo que equivale a 2.200 puestos de trabajo solo en este sector.

Comparando abril de 2021 contra abril de 2019, el empleo formal en las empresas productoras de maquinarias se expandió en un 4,5%, lo que equivale a 2.200 puestos de trabajo

Más de la mitad de esa mejora la explicó la rama de maquinaria agrícola, que expandió su nómina en un 9,8% en el mismo período (+1.200 puestos).

Por su parte, en junio el saldo comercial fue positivo en US$ 1.067 millones, sobre todo por las ventas de manufacturas industriales y agropecuarias, y por mayores precios de exportación.

Se trata del sexto superávit consecutivo tras el déficit de diciembre, en tanto que "otro dato sobresaliente es que las exportaciones registraron en junio su nivel más alto desde 2014".

"Los consumos empiezan a parecerse más a los de la prepandemia", se resaltó al señalar que las ventas de indumentaria y calzado retornaron a niveles similares a los de principios de marzo de 2020

Crecieron 41,7% en comparación con el mismo mes de 2019 y 79,1% en la medición frente a 2020.

Finalmente, el informe del CEP XXI detalló que, desde mediados de junio, las ventas con el programa Ahora 12 en valores constantes se ubican en torno al 12% por encima de la primera quincena de marzo de 2020.

"Los consumos empiezan a parecerse más a los de la prepandemia", se resaltó al señalar que las ventas de indumentaria y calzado retornaron a niveles similares a los de principios de marzo de 2020 y la facturación ligada al turismo con Ahora 12 alcanzó el mayor nivel desde 2019.