---
category: La Ciudad
date: 2021-11-13T06:15:46-03:00
thumbnail: https://assets.3dnoticias.com.ar/badboys.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Se entregaron 11 patrulleros para el departamento La Capital
title: Se entregaron 11 patrulleros para el departamento La Capital
entradilla: 'Es la segunda tanda de entrega de nuevos patrulleros de un total de 216
  que ya fueron presentados durante las últimas semanas para la Policía.

'

---
El gobierno provincial, a través del Ministerio de Seguridad, incorporó a la Policía de Santa Fe, 11 nuevos patrulleros que forman parte de un total de 216 que se fueron incorporando en el transcurso de los últimas días.

Los vehículos que se destinaron a la Unidad Regional I de Policía son 9 camionetas. Las dos restantes fueron a la Guardia Rural Los Pumas. Con estas, se completan las 34 camionetas que se entregaron para el Departamento.

Al respecto, el secretario de Política y Gestión de la Información, Jorge Fernández, indicó que “estamos entregando estas camionetas en el marco de un plan que tiene como objetivo brindarle a la policía de Santa Fe las mejores herramientas para organizar las tareas de prevención, conjuración e investigación del delito”.

“Estos vehículos 0 km forman parte de un programa mucho mayor de modernización de la Policía de Santa Fe, que contempla vehículos, tecnología y equipamiento más moderno y adecuado”, finalizó el funcionario provincial.