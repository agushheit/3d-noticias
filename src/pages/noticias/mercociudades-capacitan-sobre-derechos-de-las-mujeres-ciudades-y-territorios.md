---
category: La Ciudad
date: 2021-03-27T06:15:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/mercocitysg.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Mercociudades: capacitan sobre derechos de las mujeres, ciudades y territorios'
title: 'Mercociudades: capacitan sobre derechos de las mujeres, ciudades y territorios'
entradilla: 'Se encuentran abiertas las inscripciones capacitaciones de proyectos
  urbanísticos con perspectiva de género. '

---
La Municipalidad de Santa Fe impulsa la capacitación “Derechos de las mujeres, ciudades y territorios”. La iniciativa comenzará el 31 de marzo y se extenderá a lo largo de un semestre con el desarrollo de encuentros virtuales. La iniciativa, que en su lanzamiento dará cierre a las actividades locales por el Mes de las Mujeres y Disidencias, cuenta con el apoyo de Mercociudades.

La propuesta formativa incluye una instancia de capacitación y, en paralelo, la concreción de un manual para el diseño de proyectos urbanísticos con perspectiva de género, el cual será realizado en conjunto entre distintas áreas del municipio y las Redes de instituciones que forman parte del Plan Integrar. Como corolario, en un espacio que será elegido por las mismas Redes locales, se desarrollará una intervención que incluirá la elaboración del proyecto ejecutivo y la obra.

La capacitación está destinada a profesionales vinculados a la arquitectura, el urbanismo y los estudios en género, como es el caso de funcionarios y agentes del Estado, profesionales agrupados o que desempeñan sus tareas en organizaciones sociales, cámaras y colegios, además de público en general con interés y abordaje en estas temáticas. Para inscribirse, la persona interesada debe completar un formulario en el siguiente enlace.

**Proyecto integral**

La guía que elaborará el municipio para el diseño de proyectos urbanísticos con perspectiva de género se inscribe en la propuesta denominada “Mujeres en la ciudad: herramientas para reflexionar y diseñar el espacio público desde una perspectiva de género Inter seccional”, que fue seleccionada por el Programa de Cooperación Sur de la Red de Mercociudades, al que Santa Fe se postuló en septiembre de 2020, en sociedad con la ciudad de Asunción.

El proyecto tiene por objetivo la incorporación de la mirada de género Inter seccional a la visión del urbanismo y al diseño de los espacios públicos, para lograr ciudades más igualitarias e inclusivas. Para el armado de este proyecto, comandado por la Secretaría de Obras y Espacio Público, intervinieron otras áreas municipales que abordan temáticas de género, inclusión, discapacidad y ciclos de vida, además de la Agencia de Cooperación, Inversiones y Comercio Exterior.

Grisela Bertoni, secretaria de Obras y Espacio Público municipal, detalló que “ciudad, género, sustentabilidad y ciudades resilientes son los temas que Mercociudades está tomando como ejes de su gestión. Nosotros postulamos, con Asunción como ciudad socia, una propuesta para generar un manual para el diseño del espacio público con perspectiva de género. La propuesta fue evaluada positivamente por Mercociudades, por lo que aporta los fondos para que la concreción del manual sea real. La ciudad de Santa Fe co - colabora, a través de sus recursos, con el proyecto”.

**Formadores de amplia trayectoria**

Dos organizaciones acompañarán al municipio en el desarrollo de la propuesta: el Centro de Intercambio y Servicios Cono Sur Argentina (CISCSA) con sede en la provincia de Córdoba y la cooperativa de profesionales de la arquitectura Punto 6, con sede en Barcelona.

CISCSA es organización sin fines de lucro que desde 1985 promueve al fortalecimiento de las voces y organizaciones de mujeres y a la incidencia en políticas públicas vinculadas al derecho de las mujeres a la ciudad y el hábitat desde una perspectiva crítica y feminista. Este espacio liderado por Ana Falú tendrá a su cargo el dictado de las capacitaciones.

En tanto Punto 6 aportará su mirada para la elaboración de la guía de buenas prácticas para el diseño de proyectos urbanísticos con perspectiva de género. Integrando disciplinas como la arquitectura, el uranismo y la sociología, desde 2005 este colectivo se propone repensar los espacios desde la experiencia cotidiana para una transformación feminista. Con ese objetivo, plantea un urbanismo estratégico y táctico, con la ciudadanía como punto de partida para la planificación.