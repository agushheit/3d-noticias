---
category: Agenda Ciudadana
date: 2021-03-02T05:32:28-03:00
thumbnail: https://assets.3dnoticias.com.ar/covid.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de la OMS
resumen: Cuáles son los síntomas que indican que ya se tuvo coronavirus
title: Cuáles son los síntomas que indican que ya se tuvo coronavirus
entradilla: En algunas personas, las señales pueden persistir o volver a aparecer
  por semanas o meses luego de la recuperación inicial. Esto también puede ocurrir
  a pacientes con una enfermedad leve

---
Una reciente investigación publicada en la revista Jama Network Open ha tratado de arrojar más luz sobre lo que se conoce como Covid larga, persistente o síndrome poscovid.

Sobre ella también se expresó en su día la Organización Mundial de la Salud (OMS: "En algunas personas, algunos síntomas pueden persistir o volver a aparecer por semanas o meses luego de la recuperación inicial. Esto también puede ocurrirle a personas con una enfermedad leve", aseguró este organismo en un documento el pasado mes de diciembre, según recoge CNN.

Estos son los síntomas o señales que, según el reciente estudio, podrían ser indicadores de que ya se ha pasado la enfermedad.

La fatiga es uno de los más comunes que delatan la infección por coronavirus, pero también uno de los que más persisten tras haberla superado, sostiene esta investigación.

> Más del 30% de las personas que participaron en este estudio, desarrollado por investigadores de la Universidad de Washington, aseguró tener síntomas persistentes nueve meses después de haber tenido la enfermedad. Este porcentaje apenas varió en función de la edad de todos los grupos analizados, si bien aumenta hasta el 43,3% en participantes mayores de 65 años.

La fatiga fue el más común en todos ellos, seguido por la pérdida de olfato y gusto. Ambas secuelas fueron reportadas por un 13,6% de los participantes que presentaron síntomas persistentes.

Un 13% informó además de haber tenido otros síntomas como la confusión mental. Sobre este efecto, la doctora Allison Navis, de la Escuela de Medicina Icahn del Hospital Monte Sinai en Nueva York, ha aclarado que "es un síntoma, no un diagnóstico. Y significa muchas cosas distintas para diferentes personas. Con frecuencia es una combinación de problemas de memoria a corto plazo, concentración o dificultad para expresarse", apunta.

Navis sostiene que este síntoma no está directamente relacionado con haber pasado la Covid de manera grave, ser de mayor edad o tener factores de riesgo. Es más, Navis indica que los médicos observaron este síntoma en pacientes jóvenes y sanos que pasaron la enfermedad de forma leve.

En otro estudio realizado en Wuhan a más de 1.700 pacientes de Covid-19, el 76% de ellos tuvo al menos un síntoma seis meses después de haber recibido el alta hospitalaria. Los más comunes fueron fatiga (63%) y dificultades para dormir (23%).

"Cuando miramos a los síntomas a largo plazo, las grandes cosas que vemos son fatiga, letargo y trastornos del sueño. Y eso representa probablemente más de la mitad de lo que vemos", sostiene, por su parte el doctor Christian Sandrock, profesor en la Facultad de Medicina en la Universidad de California.

El estudio de Wuhan también descubrió que muchos pacientes experimentaron secuelas psicológicas duraderas, como ansiedad o depresión. Sobre este último caso, los Centros para el Control y la Prevención de Enfermedades de Estados Unidos ya incluyeron la depresión como uno de los síntomas persistentes que puede dejar la Covid. Estos son los síntomas a largo plazo que describe la CDC:

* Fatiga
* Dificultad para respirar
* Tos
* Dolor en las articulaciones
* Dolor de pecho

Asimismo, los CDC también incluyen estos otros síntomas notificados por personas que ya pasaron la enfermedad:

* Dificultad para pensar y concentrarse (confusión mental)
* Depresión
* Dolor muscular
* Dolor de cabeza
* Fiebre intermitente
* Corazón que late rápido o muy fuerte (palpitaciones)