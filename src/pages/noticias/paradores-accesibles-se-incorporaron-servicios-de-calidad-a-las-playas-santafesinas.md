---
category: La Ciudad
date: 2022-01-16T06:00:46-03:00
thumbnail: https://assets.3dnoticias.com.ar/plasha.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Paradores accesibles: se incorporaron servicios de calidad a las playas
  santafesinas'
title: 'Paradores accesibles: se incorporaron servicios de calidad a las playas santafesinas'
entradilla: "Más de 35 duchas, humidificadores, rampas para garantizar accesibilidad
  y la extensión de la red de agua potable y cloacas, son algunas de las mejoras que
  se concretaron. \n\n"

---
Los paradores y playas de la ciudad incorporan servicios para mejorar la atención de los vecinos y turistas que lleguen a la ciudad. En tal sentido, se incorporaron nuevas baterías de baños, rampas que brinden accesibilidad a los distintos espacios, obras de cloaca y agua potable. Vale recordar que estos son espacios públicos concesionados. Las obras son producto de los contratos de concesión de los espacios y demandaron una inversión del sector privado de más de 15 millones de pesos que quedarán para la ciudad.

Esta mañana los responsables de los paradores y Matías Schmüth, secretario de Producción y Desarrollo Económico municipal, recorrieron las obras realizadas, en este caso, en el sector de la Costanera Este, y destacaron los beneficios que se observan.

En esa línea, Matías Schmüth indicó que “los paradores de la Costanera Este y Oeste son espacios públicos que fueron concesionados, con un contrato que exigía a los privados hacer diferentes obras para potenciar los servicios”.

En consonancia, el funcionario enumeró que entre la cantidad de mejoras se observa la concreción de rampas “para hacer las playas más accesibles, en el caso de la Costanera Este son dos las rampas que se incorporaron y una más en Playa Grande”.

Por otra parte, Schmüth destacó: “Nos parece importante mostrar todos los servicios que se están brindando, que van acompañados de infraestructura y de espacios gastronómicos, culturales, deportivos. Por lo tanto invitamos a las personas a que se acerquen a disfrutar de ofertas diversas, como la colocación de humidificadores para refrescarse, más de 35 duchas disponibles para que puedan disfrutar del solárium y de todo lo que implica las playas para la ciudad”.

**Obras de calidad**

“Se pudo concretar un viejo reclamo, que era la incorporación de baños de calidad en la Costanera Este”, indicó Schmüth y añadió que el pedido para que se concreten dichas obras era tanto del sector turístico, como de los propios vecinos. “Hoy tenemos una obra que es digna de mostrar, al igual que en el Paseo Néstor Kirchner, además están en construcción baños en el Espigón I, y pudimos extender la red de agua potable y completar toda la Costanera Este con una obra de cloacas que representa una inversión millonaria y fundamental”.

Por su parte, Carlos Fertonani, titular del parador Santa Fe, indicó que desde el sector turístico “estamos muy contentos, los que trabajamos en turismo podemos decir que hoy tenemos la oferta que tanto deseábamos los santafesinos. Estas obras son fundamentales para los turistas, pero también para los santafesinos que se fueron de vacaciones y cuando vuelven, quieren seguir teniendo los mismos servicios en sus playas”.

“Al problema de la bajante de la laguna lo contrarrestamos con la cantidad de duchas que tenemos. En definitiva, le dimos vida a estos espacios, los dotamos de servicios, no hay nada que envidiar a los paradores de otros lugares, la gente lo valora y se ve con la llegada de turistas”, repasó Fertonani.

**Detalle de los trabajos**

En el parador Santa Fe situado en la costanera Este, se incorporó una nueva batería de baños, incluyendo un baño accesible, como así también una rampa que permite acceder a la zona de arena y un nuevo ingreso a la playa, además de incorporar 16 duchas.

El parador Sunset ubicado en el extremo norte de la Costanera Este, mejoró su batería sanitaria y concluyó obras fundamentales para la zona: la extensión del servicio de cloaca y la red de agua potable, además de contar con un ingreso accesible a la zona del parador principal.

En el parador Playa Grande, se restauraron los baños existentes, se construyó una escalera para ingreso a la zona de arena, como así también una rampa que permite brindar accesibilidad a la playa. Además, incorporó humidificadores sobre la arena.

Por otra parte, en la Costanera Oeste, a la altura del Lawn Tenis, se remodelaron los baños. En tanto, en el Espigón I se incorporó un nuevo parador sobre la zona de arena y está en construcción una nueva batería de baños sobre la Costanera, que tendrán las mismas prestaciones que el incorporado en la Costanera Este.