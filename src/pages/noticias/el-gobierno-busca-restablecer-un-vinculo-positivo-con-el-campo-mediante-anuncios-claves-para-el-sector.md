---
category: Agenda Ciudadana
date: 2021-10-04T06:15:54-03:00
thumbnail: https://assets.3dnoticias.com.ar/campo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: El Gobierno busca restablecer un vínculo positivo con el campo mediante anuncios
  claves para el sector
title: El Gobierno busca restablecer un vínculo positivo con el campo mediante anuncios
  claves para el sector
entradilla: Con la idea de buscar una solución "federal" a los problemas que se plantean
  en el sector, se realizaron dos anuncios para la cadena productiva.

---
El Gobierno buscó relanzar su relación con el campo en las últimos días con dos anuncios clave para la cadena productiva: la reapertura de las exportaciones de carne de vaca conserva a China y el envío al Congreso de un proyecto de ley de fomento de inversiones para la agroindustria.  
A esto se sumó la decisión del Ministerio de Agricultura, Ganadería y Pesca de profundizar el diálogo con los gobernadores y las entidades representativas de los productores, con la idea de buscar una solución "federal" a los problemas que se plantean en el sector.  
"El Presidente (Alberto Fernández) me pidió que resuelva todos los problemas pendientes del sector, en los cuales la pandemia había paralizado el diálogo y que había que recomponerlo, básicamente, porque es la condición para poder progresar, crecer y tener un horizonte de previsibilidad", dijo el ministro Julián Domínguez el martes, tras anunciar el levantamiento de las restricciones a las exportaciones de carne vacuna a China y la puesta en marcha de líneas de financiamiento para los productores.  
La cartera que conduce Domínguez, que reemplazó en el cargo a Luis Basterra, adquirió una nueva impronta y retomó el manejo de temas del área que anteriormente se discutían en otros ministerios.  
El martes pasado, Domínguez, junto al jefe de Gabinete, Juan Manzur, y al ministro del Interior, Eduardo "Wado" de Pedro, y cinco gobernadores de provincias ganaderas, anunció, entre otras medidas, la liberación de la exportación de la vaca conserva a China, una de los principales reclamos de la cadena cárnica.  
Dos días después, en el Museo del Bicentenario junto al presidente Alberto Fernández, y la vicepresidenta Cristina Fernández de Kirchner, fue parte del anuncio del envío al Congreso del proyecto de ley que crea el Régimen de Fomento para la Agroindustria, trabajado en conjunto con el Consejo Agroindustrial Argentino (CAA), que comprende una serie de beneficios fiscales e incentivos en pos de aumentar la producción y el empleo en el sector.  
Fuentes oficiales consultadas por Télam aseguraron que Domínguez tiene la "firme" decisión de impulsar la sanción de la norma por el Congreso y que confía en que el sector hará lo propio en "apoyo y defensa" del proyecto cuando se dé la discusión de la misma.  
Domínguez expresó que "las decisiones se van resolver de manera federal: son las provincias quienes van a decidir las políticas para sus provincias"

"La nueva gestión buscará darle un carácter más federal, no solo a la cartera, sino también a la toma de decisiones", agregaron las mismas fuentes.  
En este sentido, Domínguez -en diálogo con la prensa- expresó que "las decisiones se van resolver de manera federal: son las provincias quienes van a decidir las políticas para sus provincias. Hay un excesivo centralismo en Argentina y si somos un país federal, las políticas para las provincias, sus objetivos e instrumentación las resolverán ellas".  
Además, buscarán imprimirle más territorialidad a la gestión: "Se baja al territorio y se explican las razones de las medidas que se toman a favor de los productores. Hay que salir al encuentro del otro, tratar de solucionar todos los problemas que se pueda desde la conducción del Estado. Es estar con los productores y los gobernadores para generar mesas de diálogo", señalaron las fuentes.  
  
En este sentido, Domínguez visitó el sábado La Pampa y en los próximos días estará en Santa Fe, Entre Ríos y Córdoba.  
Desde el sector privado, el presidente de la Federación de la Industria Molinera (FAIM) y uno de los impulsores del Consejo Argentino de Agroindustria, Diego Cifarelli, remarcó la importancia del proyecto de fomento diseñado con el Gobierno, al que lo consideró como un "primer paso" y de "relanzamiento de la relación" por parte del Gobierno para con el sector. ​  
"Es un buen primer paso en este relanzamiento en la relación del Gobierno con una parte de la agroindustria. Creo que sin dudas, este buen primer paso es el inicio para buscar políticas integradas que garanticen una mayor producción. Todos, tanto el Gobierno como los privados estamos detrás de una mayor producción y este proyecto va a en este sentido", indicó a Télam Cifarelli.  
En la visión del dirigente empresario, el sector debe "asumir el compromiso de seguir trabajando para dar los otros pasos, en el que, obviamente, están los derechos de exportación, que son un freno de mano para muchos sectores que son altamente exportadores".  
"Me quedo con una mirada positiva y que se dio un primer paso histórico. Ahora tenemos que poner el foco en que se trate la ley en el Congreso, contarle a todo el arco político la importancia de este y, vez terminado esto, dar el segundo paso", concluyó Cifarelli.