---
layout: Noticia con imagen
author: 3DNoticias
resumen: Plan de conectividad
category: Agenda Ciudadana
title: "Plan de conectividad: José Corral propone coordinar esfuerzos con el
  sector privado"
entradilla: El dirigente radical y excandidato a gobernador de Juntos por el
  Cambio ve con buenos ojos el programa Santa Fe+Conectada, pero destaca “la
  necesidad de coordinar esfuerzos” del Estado con el sector privado.
date: 2020-11-24T12:17:48.184Z
thumbnail: https://assets.3dnoticias.com.ar/conectividad.jpeg
---
José Corral, dirigente radical y referente de Juntos por el Cambio en la provincia, señaló que el programa “Santa Fe+Conectada” que impulsa la gestión de Omar Perotti “va en el buen camino”, pero también **advirtió que es importante que el Estado pueda coordinar esfuerzos con el sector privado**.

“El acceso a las tecnologías de la información y la comunicación es un derecho básico. Las redes de datos son en el siglo 21 lo que el ferrocarril en el siglo 19, o las rutas para los autos en el siglo 20”, destacó. Y en esa línea, analizó que “la pandemia aceleró los cambios que ya se venían produciendo de cada vez un mayor uso de la conectividad y las redes de datos para la familia, la cultura, la educación, las compras, la vida cotidiana de las personas; pero también puso sobre la mesa la enorme brecha digital entre santafesinos que tienen acceso a las redes y aquellos que no”.

Por eso, el excandidato a gobernador de Juntos por el Cambio en las pasadas elecciones consideró que “la iniciativa de Perotti con el programa “Santa Fe+Conectada” va en el buen sentido: llegar con buena conectividad a cada rincón de la provincia para permitir ese acceso que hoy es un derecho básico para la vida de las personas”. Sin embargo, advirtió que “destacamos la necesidad de coordinar esfuerzos del Estado con el sector privado”y apuntó que “la inversión privada ha permitido ampliar la cobertura de datos a buena parte de la provincia. El Estado tiene que acompañar, regulando, promoviendo, generando condiciones para que los actores privados estén estimulados a continuar con esas inversiones que se vienen realizando en el país y en particular en la provincia de Santa Fe”.

Finalmente, recordó que durante su gestión al frente del gobierno local en la ciudad de Santa Fe -de la que fue intendente entre 2011 y 2019-, “tuvimos una activa política de promoción de inversiones y de regulación para incentivar la ampliación de las redes existentes y una mayor cobertura y de opciones para las santafesinas y santafesinos de todos los barrios de la capital provincial”.

En ese sentido, **recordó que durante los 8 años de gestión en la ciudad se mejoró la conectividad celular en las zonas donde ya había, y se amplió la red a otros 35 barrios**.

Ese desarrollo estuvo impulsado por la ordenanza sobre telefonía celular que fuera elevada por el Ejecutivo con asesoramiento de las universidades. Se trata de normativa que ha sido tomada luego como referencia para muchas localidades de la provincia de Santa Fe.

También **tuvo una expansión similar el tendido de fibra óptica, que ahora cubre gran parte de la superficie urbanizada**.