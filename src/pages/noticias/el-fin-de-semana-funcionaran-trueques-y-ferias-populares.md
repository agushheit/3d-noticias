---
category: La Ciudad
date: 2021-06-18T09:04:54-03:00
thumbnail: https://assets.3dnoticias.com.ar/trueque-mitre.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: El fin de semana funcionarán trueques y ferias populares
title: El fin de semana funcionarán trueques y ferias populares
entradilla: 'Será por el Día del Padre: el viernes la del CIC de Facundo Zuviría;
  el sábado en la Estación Mitre y durante el fin de semana la feria de la Costanera
  oeste'

---
Por el fin de semana del Día del Padre volverán a funcionar en la ciudad de Santa Fe los trueques y algunas ferias de las denominadas de la economía popular, según informó el secretario de Prácticas Socio Comunitarias del Ministerio de Desarrollo Social de Santa Fe, Ignacio Martínez Kerz.

El viernes podrán regresar los trabajadores del CIC de Facundo Zuviría, el sábado harán lo mismo quienes se desempeñan en la Baulera de la Estación Mitre y los emprendedores del Paseo de la Costanera oeste. El horario establecido será de 9 a 17 y con aforo reducido, además de acatar los protocolos que ya están establecido para este tipo de actividades. Kerz dijo además que el desarrollo de la actividad será bajo la atenta mirada de agentes de la Guardia de Seguridad Institucional del municipio y la Policía provincial. Se proveerá a los trabajador y asistentes los elementos necesarios para la higiene, como por ejemplo el alcohol en gel.

En la Estación Mitre estará instalado uno de los cuatros centros de testeos masivos y gratuitos que dispuso el Ministerio de Salud, que funcionará de 9 a 13, para todos aquellos que quieran realizarse un test rápido para saber si tienen o no coronavirus. No es necesario tener turno previo.

"Sin dudas esto será una prueba para ver qué ocurre con las ferias, cómo es su funcionamiento y concurrencia para evaluar la vuelta de la actividad, principalmente porque ya son varios los meses que los trabajadores no pueden desarrollar la actividad y no están incluidos en ningún tipo de ayuda económica", destacó Kerz.