---
category: Agenda Ciudadana
date: 2021-03-25T08:28:10-03:00
thumbnail: https://assets.3dnoticias.com.ar/publicidad-electoral.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: La Justicia declaró inconstitucional el artículo que obliga a canales y radios
  a difundir publicidad electoral gratis
title: La Justicia declaró inconstitucional el artículo que obliga a canales y radios
  a difundir publicidad electoral gratis
entradilla: La juez Servini resolvió un planteo de un grupo de medios, que apuntaban
  contra un apartado de la Ley de Financiamiento de Partidos Políticos.

---
La jueza federal con competencia electoral en la Ciudad de Buenos Aires, María Romilda Servini, declaró inconstitucional un artículo de la Ley de Financiamiento de Partidos Políticos que obliga a canales y radios a difundir publicidad electoral gratis de los partidos políticos.

El planteo había sido por el abogado Tomás Pérez Virasoro en su carácter de apoderado de América TV S.A, de la Red Celeste y Blanca S.A y de Radio Libertad S.A (con participación accionaria en las radios Blue 101.7 y AM Belgrano). La resolución de la magistrada, según publicó Infobae, abre la posibilidad a otros grupos de medios a solicitar una decisión similar en la antesala de las elecciones legislativas nacionales que se realizarán este año.

El artículo cuestionado es el 43 quater de la ley 26.215, que establece: "De acuerdo a lo establecido en la Ley de Servicios de Comunicación Audiovisual 26.522, los servicios de comunicación y de televisión por suscripción están obligados a ceder en forma gratuita el cinco por ciento (5%) del tiempo total de programación para fines electorales. A partir del año 2020, del porcentaje mencionado en el párrafo anterior, la mitad será cedida a título gratuito y la otra mitad será considerada pago a cuenta de impuestos nacionales".

La cuestión técnica gira en torno a los artículos 14 y 17 de la Constitución Nacional, que resguarda la propiedad privada. Tras analizar el planteo de los medios, la reconocida magistrada federal concluyó que la cesión en sí de los espacios no es inválida, pero sí resulta de "dudosa constitucionalidad" la ausencia de una contraprestación económica y además precisó que debería ser el Estado quien se haga cargo de estos costos y no las empresas privadas de radio y televisión.

"Entiendo que durante las campañas electorales, al verse obligadas a ceder espacios de su programación diaria gratuitamente –los que habitualmente serían otorgados a terceros, a título oneroso-, las empresas licenciatarias dejan de percibir una determinada suma de dinero y ello redunda en un perjuicio económico. Que dicho perjuicio haya sido conocido y aceptado en el momento mismo de ser concedida la licencia por parte del Estado, no obsta a que se las coloque en una situación de desigualdad respecto de los demás intervinientes en el proceso eleccionario", explicó Servini.

Y agregó: "En efecto, el perjuicio no es sólo afirmado por las demandantes sino también reconocido indirectamente por el Estado Nacional, cuando al reformar el artículo 43 quater de la ley 26.215 se dispone que aquella originaria cesión del 10 por ciento, completamente gratuita, se reducirá al 5 por ciento y que, a partir del año 2020, este porcentaje disminuirá en un cincuenta por ciento mientras que la otra mitad será considerada un desembolso a modo de cancelación, a cuenta de impuestos nacionales".

A partir de la sanción de la Ley de Medios, la Dirección Nacional Electoral (Dine) cede espacios gratuitos a partidos políticos en canales y radios: la distribución se realiza a través de un sorteo público. Las emisoras afectadas son de TV Abierta, TV por cable, señales nacionales, radios AM y FM habilitadas por Enacom, quienes están obligadas a ceder el 5 por ciento de 12 horas de transmisión diarias, es decir 36 minutos por día.