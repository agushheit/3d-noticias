---
category: La Ciudad
date: 2021-09-07T18:00:36-03:00
thumbnail: https://assets.3dnoticias.com.ar/HERRAMIENTAS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: El Banco de Herramientas Municipal entregará elementos a nuevas unidades
  productivas
title: El Banco de Herramientas Municipal entregará elementos a nuevas unidades productivas
entradilla: 'En el barrio Santa Rosa de Lima se iniciaron las Asambleas de la Economía
  Popular con referentes de unidades productivas. '

---
La Municipalidad comenzó con un nuevo proceso de Asambleas de la Economía Popular en Santa Rosa de Lima. En la Estación Mediateca, se reunieron los representantes de unidades productivas de los barrios Santa Rosa de Lima, San Lorenzo, 12 de Octubre, Varadero Sarsotti y Barranquitas, para tener una primera aproximación al procedimiento que culminará con la entrega de elementos de trabajo, provenientes del Banco de Herramientas Municipal.

Hasta el momento hay 16 emprendimientos involucrados. No obstante, aún hay tiempo para acercarse a participar, ya que no existen requisitos para las unidades productivas que deseen formar parte. Sólo es necesario acercarse a la Estación Municipal del barrio para conocer el cronograma de reuniones.

El objetivo es mejorar técnicamente los procesos de trabajo de unidades productivas que ya funcionan. Por eso, desde la Secretaría de Integración y Economía Social del municipio se realizan todas las gestiones tendientes a convocar a la mayor cantidad de vecinos y vecinas, de modo de multiplicar los alcances de la propuesta.

De este modo, la intención es que la Municipalidad acompañe los procesos, propiciando espacios de intercambio. Así se mejorará el trabajo en esas unidades productivas y se potenciará la comercialización de sus productos y servicios.

Cabe recordar que esta política de fomento de la Economía Social fue iniciada por el municipio en la Estación San Agustín, en mayo pasado. Allí se convocó a emprendedores de cuatro barrios, con los que se conversó sobre el plan. Tras varias reuniones, se logró avanzar con 17 unidades productivas informales, conformadas por 110 santafesinos y santafesinas. De este modo, los emprendimientos dedicados a los rubros ladrillería, carpintería, herrería, vivero, bloquera, panadería y textil recibieron herramientas por un total de $ 1,5 millones.

**Una mirada común**

La secretaria de Integración y Economía Social municipal, Ayelén Dutruel, mencionó que durante las asambleas “se abordan varios temas, empezando por aquello que entendemos como economía social y cómo nos percibimos los trabajadores y trabajadoras de la economía popular en la ciudad”.

Al respecto, indicó que “esta experiencia invita a trabajadores, trabajadoras y unidades productivas con alguna trayectoria a involucrarse en la discusión de una mirada respecto de la economía social en la ciudad. Y eso, lo que implica, es transitar por varias temáticas y reflexiones que de algún modo se vinculen con pensar una mirada común, una idea común y algunos objetivos en común”.

Respecto de las asambleas, Dutruel mencionó que “consisten en ocho encuentros con formato taller, que implican pensar una mirada común de la economía social y de la política de la economía social de la ciudad”. Luego, al finalizar el esquema de talleres, formación, discusión y evaluación técnica -que hace todo el equipo de trabajo-, se le entregan las herramientas necesarias a cada unidad productiva para fortalecer sus procesos.

En caso de que se mantengan las 16 unidades productivas que ya están participando del proceso, en un mes y medio, el mismo estará culminado. Actualmente se concreta la evaluación técnica de cada uno de los emprendimientos que integran la asamblea, por lo que, hasta el momento, el municipio estima que la inversión necesaria del Banco de Herramientas rondará los 2 millones de pesos, para satisfacer las demandas.