---
category: El Campo
date: 2021-01-04T10:22:22Z
thumbnail: https://assets.3dnoticias.com.ar/reclamo-04012021.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Diario UNO'
resumen: El campo evalúa un paro y Carsfe le pide a Fernández no volver «con las mismas
  recetas del pasado»
title: El campo evalúa un paro y Carsfe le pide a Fernández no volver «con las mismas
  recetas del pasado»
entradilla: La mesa de Enlace de Santa Fe elaboró un comunicado luego de que el gobierno
  prohíba las exportaciones de maíz. Reunión clave este lunes para definir un posible
  plan de lucha.

---
La Mesa de Enlace del campo, integrada por Confederaciones Rurales Argentinas (CRA), la Sociedad Rural Argentina, los cooperativistas de Coninagro y la Federación Agraria Argentina, se reunirá este lunes en forma virtual para **analizar la respuesta a la reciente decisión del gobierno nacional de prohibir las exportaciones de maíz** con el argumento de «garantizar el abastecimiento para quienes transforman este grano en carnes y huevos».

**«¿Se repite la historia?»** Así titula la Confederación de Asociaciones Rurales de la Provincia de Santa Fe un comunicado que publicó en sus redes sociales.

«Desde la Mesa de Enlace Provincial vemos con gran preocupación la decisión inconsulta del gobierno nacional de cerrar el registro de exportaciones de maíz, como forma de intervenir el mercado para bajar el precio», describe la nota.

Y añade: «La cotización del maíz ha sufrido aumentos debido a una fuerte demanda y poca oferta, por la probable disminución de la producción, a raíz de las escasas lluvias de este año».

«**Argentina necesita que exportemos, para que se produzca el ingreso de divisas que tanta falta le hace.** En lugar de aprovechar esta oportunidad circunstancial, el gobierno nacional vuelve a implementar políticas que fracasaron en años anteriores. Dando un claro mensaje hacia los productores para que no siembren este cultivo, lo que termina siendo menos producción, menos exportaciones y divisas para el país», destaca la nota.

Desde Carsfe entienden que «bajar el precio del maíz no tendrá una baja significativa en el precio de los alimentos, pero sí generará un desaliento en la futura producción». Y agregan: «los alimentos que consumimos tienen alrededor de un 40% de impuestos. **La política correcta sería bajar los impuestos a algunos alimentos transitoriamente para que sí tengan un impacto en la reducción del precio que paga el consumidor**; a la vez,  seguir exportando y así aumentar los ingresos de divisas al país».

«Sr. Presidente, no vuelva con las mismas recetas del pasado que tanto daño hicieron, desalentando la producción, exija a sus ministros y colaboradores ideas nuevas que fomenten la producción y las exportaciones, que disminuyan los costos, reduzcan impuestos. La Argentina no está en situación para que los que gobiernan vuelvan a equivocarse», concluyen.

***

![](https://assets.3dnoticias.com.ar/Comunicado-040121.webp)

***