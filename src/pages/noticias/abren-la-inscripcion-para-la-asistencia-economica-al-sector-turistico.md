---
category: Agenda Ciudadana
date: 2021-06-10T07:06:55-03:00
thumbnail: https://assets.3dnoticias.com.ar/turismo2.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: Abren la inscripción para la asistencia económica al sector turístico
title: Abren la inscripción para la asistencia económica al sector turístico
entradilla: Los aportes no reintegrables serán a cabañas, bungalows, hosteles, residenciales,
  campings privados, alojamientos rurales, guías de turismo y guías de pesca; agencias
  de viajes y turismo; y a transportes de pasajeros.

---
Ante las restricciones impuestas en el marco de la nueva modalidad de convivencia por la pandemia de Covid 19, el gobierno de la provincia dispuso una nueva y más amplia asistencia económica para los sectores del turismo afectados por la inactividad.

En el marco del Programa de Asistencia Económica de Emergencia creado por el decreto N° 472/20, y mediante la resolución 213/21 del Ministerio de Producción, Ciencia y Tecnología se instrumentará la Asistencia Económica a cabañas, bungalows, hosteles, residenciales, campings privados, alojamientos rurales, guías de turismo y guías de pesca, ayuda que prevé otorgar un aporte no reintegrable para los titulares de este tipo de emprendimientos turísticos de la provincia.

Dichos aportes se efectuarán en tres cuotas iguales, mensuales y consecutivas, y los aportes serán de un promedio de 50 mil pesos dependiendo la categoría impositiva.

**Agencias y transportes**

Además, en esta oportunidad serán beneficiarias también las agencias de viajes y turismo. Éstas últimas podrán acceder al aporte de tres cuotas iguales, mensuales y consecutivas de $30.000 por empresa, más una suma adicional de $10.000 por empleado debidamente registrado.

Asimismo, a través del decreto Nº 576/21, se amplió la nómina de beneficiarios del «Programa de Asistencia Económica de Emergencia» a fin de otorgar también una asistencia económica para el sector de Transportes Turísticos de Pasajeros. Dicha asistencia consiste en un aporte no reembolsable para todos los titulares de empresas de Servicios de Transporte Automotor de Pasajeros para el Turismo de Jurisdicción Nacional, con asiento en la provincia de Santa Fe. Será una suma fija por persona humana o jurídica.

**Inscripciones**

Las inscripciones podrán realizarse hasta el 20 de junio inclusive completando los formularios correspondientes en la web. Dichos formularios se encuentran en el siguiente enlace: [https://www.santafe.gob.ar/ms/covid19/programa-asistencia-economica-de-emergencia-turismo/](https://www.santafe.gob.ar/ms/covid19/programa-asistencia-economica-de-emergencia-turismo/ "https://www.santafe.gob.ar/ms/covid19/programa-asistencia-economica-de-emergencia-turismo/")