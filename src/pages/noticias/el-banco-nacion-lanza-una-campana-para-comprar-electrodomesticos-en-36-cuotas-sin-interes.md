---
category: Agenda Ciudadana
date: 2021-09-08T06:15:49-03:00
thumbnail: https://assets.3dnoticias.com.ar/BANCONACION.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: El Banco Nación lanza una campaña para comprar electrodomésticos en 36 cuotas
  sin interés
title: El Banco Nación lanza una campaña para comprar electrodomésticos en 36 cuotas
  sin interés
entradilla: " La promoción comenzará el próximo lunes 13 y se extenderá hasta el 15
  de septiembre."

---
El Banco Nación (BNA) lanzará la próxima semana la campaña “Promo Línea Blanca " para la compra en 36 cuotas sin interés de electrodomésticos como calefones, cocinas, heladeras, secarropas centrífugos, termotanques y ventiladores, entre otros, informaron a Télam desde la entidad.

La campaña comenzará el próximo lunes 13 y se extenderá hasta el miércoles 15 de septiembre, e incluirá esquemas de pago en tramos de 3, 6, 9, 12, 15, 18, 24 y hasta 36 cuotas sin interés a través de TiendaBNA, la plataforma de comercio electrónico creada entre Nación Servicios y el Banco Nación, a la que se puede ingresar en el sitio [https://tiendabna.com.ar/.](https://tiendabna.com.ar/. "https://tiendabna.com.ar/.")

Una vez seleccionado el producto será necesario ingresar los datos personales (Nombre y apellido, DNI, correo electrónico y domicilio), calcular el costo del envío e ingresar los datos de la tarjeta.

Es importante recordar que, para acceder a las cuotas sin interés es necesario contar con una tarjeta de crédito Nativa Mastercard o Nativa Visa, ambas expedidas por el Banco Nación.

Actualmente está en vigencia una campaña para compras de celulares y smartphones en hasta 18 cuotas sin interés y con descuentos del 35%, con 300 modelos para elegir y más de 35.000 unidades en oferta.

La promoción para la compra de celulares estará en vigencia hasta este miércoles y es la tercera edición de los días de oferta para estos productos a través de TiendaBNA en lo que va del 2021.

La última edición fue en junio, que cerró con más de 11.000 unidades vendidas por más de $ 500 millones y la contratación de unos 1.000 seguros para teléfonos, y la anterior a principios de marzo, cuando se vendieron unas 15.000 unidades con un ticket promedio de $ 40.000 con la misma promoción.