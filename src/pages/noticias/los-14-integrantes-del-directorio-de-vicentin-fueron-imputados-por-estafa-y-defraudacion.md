---
category: Agenda Ciudadana
date: 2021-10-27T06:00:42-03:00
thumbnail: https://assets.3dnoticias.com.ar/vicentin.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Los 14 integrantes del directorio de Vicentin fueron imputados por estafa
  y defraudación
title: Los 14 integrantes del directorio de Vicentin fueron imputados por estafa y
  defraudación
entradilla: El fiscal Moreno imputó a los directivos de Vicentin por el delito de
  balance falso en concurso real por defraudación y estafa en seis hechos cometidos
  contra bancos extranjeros damnificados por el default de la empresa.

---
Los 14 miembros del directorio de la agroexportadora Vicentin, que llegó a alcanzar los primeros puestos en el ranking de embarques de granos al exterior, fueron imputados esta martes por estafa, defraudación y balances falsos por el fiscal de Rosario, Miguel Moreno, quien pedirá en la próxima audiencia fijada para el jueves la prisión preventiva de algunos de esos directivos.  
  
Moreno, fiscal de la Unidad de Delitos Económicos de Rosario, imputó a los directivos de Vicentin por el delito de balance falso en concurso real por defraudación y estafa en seis hechos cometidos contra bancos extranjeros damnificados por el default de la empresa, en la audiencia que se llevó a cabo en el Centro de Justicia Penal.  
  
También imputó a todos los ejecutivos por el delito de estafas en carácter de coautor contra las acopiadoras de quienes recibió granos hasta el momento de declararse en estrés financiero y posterior cesación de pagos, precisaron fuentes judiciales santafesinas.  
  
Los 14 imputados son Daniel Néstor Buyatti (presidente de Vicentin), Alberto Julián Macua (vicepresidente), Roberto Alejandro Gazze (director financiero), Máximo Javier Padoan, Cristián Andrés Padoan, Martín Sebastián Colombo, Sergio Roberto Vicentin, Pedro Germán Vicentin, Roberto Oscar Vicentin y Yanina Colomba Boschi, Raul González Arcelus, Omar Adolfo Scarel (síndico) y Miguel Vallazza.  
  
Buyatti fue imputado además en forma personal por el delito de apropiación indebida de tributos, y junto al resto de los directivos recibió la imputación por el delito de estafa contra el Banco Macro, una de las instituciones financieras locales afectadas por las maniobras de Vicentin, junto a los bancos Nación y Provincia de Buenos Aires.  
  
El juez que interviene en el caso, Román Lanzón, deberá decidir este jueves si dicta la prisión preventiva de algunos directivos, medida que será solicitada por el fiscal en el entendimiento de que pueden influir para alterar el proceso, o si acepta la oferta de fianza ofrecida por la empresa por una suma de US$ 5 millones.  
  
Al concluir la audiencia, el fiscal Moreno señaló que "si bien la pena contemplada por estafa es de seis años, por la acumulación de delitos imputados estamos en condiciones de pedir la pena máxima, que son hasta 50 años".  
  
Entre los directivos de Vicentin, sólo Máximo Padoan hizo declaraciones a la prensa al retirarse y manifestó su "desconcierto" por la imputación.  
  
Tras el cuarto intermedio, en el tramo final de la investigación que Moreno encaró desde hace casi un año y medio, durante la audiencia del jueves los defensores de los imputados harán su descargo, ya que señalaron que tienen que analizar en detalle una evidencia electrónica de 36 millones de archivos que les llegaron a último momento.  
  
Además de las diligencias de los principales acreedores y bancos perjudicados, la semana pasada el Banco Nación pidió que se investigue a la consultora que auditaba los balances de Vicentin, dados los elementos que permitieron deducir la falsificación documental, aún cuando los auditores adujeron que la caída de la empresa en 2019 fue una crisis que arrastraba desde años anteriores.  
  
La encuesta del fiscal analizó la actividad desarrollada por los representantes del órgano de dirección y fiscalización de Vicentin, que “habrían celebrado contratos de compra venta y recibiendo mercadería, sabiendo y conociendo la imposibilidad de afrontar los pagos de la misma”.  
  
A los imputados se les atribuye haber confeccionado, aprobado y utilizado los balances anuales con cierre a octubre de 2017 y octubre 2018 que contenían información financiera falsa consistente en ocultar el verdadero pasivo de la empresa.  
  
La fiscalía sostiene que también confeccionaron y presentaron ante los bancos internacionales, informes trimestrales correspondientes a los períodos enero 2017, abril 2017, julio 2017, enero 2018, abril 2018, julio 2018, enero 2019, abril 2019 y julio 2019.  
  
Esos informes exhibían una aparente sólida situación patrimonial de Vicentin “con el fin de ocultar su realidad económica financiera, engañar a las entidades financieras y así, provocaron bajo error disposiciones patrimoniales que superan los US$ 500 millones en virtud de líneas de créditos acordada”.  
  
Los bancos extranjeros denunciantes son la Corporación Financiera Internacional (CFI, del Grupo Banco Mundial); los holandeses Nederlandse Financierings-Maatschappij voor Ontwikkelingslanden N.V., Coöperatieve Rabobank U.A e ING Bank; y Natixis New York Branch, de Estados Unidos.  
  
El fiscal Moreno definió las maniobras de Vicentin como “una puesta en escena de empresa solvente y confiable”, a fin de generar confianza en los productores, vendedores de granos, acopios, cooperativas y corredores, “quienes, engañados, entregaron toneladas de granos bajo la modalidad de contratos con precio a fijar, con el pleno conocimiento de los directores y síndicos de la imposibilidad de cumplir las obligaciones asumidas”.  
  
Identificó entre los afectados a las firmas locales Cuatro Hojas S.A, Olzen Industria y Comercio de Calzados S.A., Maniagro S.A., La Clementina S.A., Commodities S.A, a las que ofrecieron mejores precios que la competencia y plazos muy superiores para fijar precio.