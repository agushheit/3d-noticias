---
category: Deportes
date: 2021-06-15T08:10:29-03:00
thumbnail: https://assets.3dnoticias.com.ar/argentina-chile.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Diario El Litoral
resumen: Argentina no pudo mantener la ventaja y empató con Chile en el debut
title: Argentina no pudo mantener la ventaja y empató con Chile en el debut
entradilla: El equipo de Lionel Scaloni volverá a jugar el viernes ante Uruguay, por
  la segunda fecha del Grupo A. 

---
La Selección Argentina empató este lunes ante Chile en su debut en la Copa América Brasil 2021. 

Fue 1 a 1 en el estadio "Nilton Santos" de Río de Janeiro. 

Con un golazo de tiro libre de Lionel Messi, el conjunto nacional abrió el marcador a los 33 minutos del primer tiempo. 

Ya en el complemento, a los 8', Eduardo Vargas lo igualó tras un penal cobrado por el colombiano Wilmar Barrios a instancias del VAR. 

El seleccionado "albiceleste", 14 veces campeón del torneo sudamericano, jugará la segunda fecha con Uruguay, el viernes 18 en Brasilia, y Chile -ganador 2015 y 2016- se enfrentará ese mismo día con Bolivia en Cuiabá.