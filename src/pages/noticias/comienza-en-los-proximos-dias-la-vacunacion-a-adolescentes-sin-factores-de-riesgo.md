---
category: Agenda Ciudadana
date: 2021-09-16T06:15:20-03:00
thumbnail: https://assets.3dnoticias.com.ar/PFIZER.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Comienza en los próximos días la vacunación a adolescentes sin factores de
  riesgo
title: Comienza en los próximos días la vacunación a adolescentes sin factores de
  riesgo
entradilla: Tras la distribución de más de cien mil vacunas del laboratorio Pfizer
  en todo el país autoridades provinciales informaron que los operativos de inmunización
  se ampliarán a personas de 17 años sin factores de riesgo.

---
Autoridades de salud de las provincias confirmaron este miércoles que la campaña de vacunación contra el coronavirus se extenderá en los próximos días a adolescentes de 17 años sin factores de riesgo tras la distribución de más de cien mil vacunas del laboratorio Pfizer en todo el país y la llegada de un segundo lote de 160 mil de esas dosis.

Asimismo, continuaban los operativos de vacunación con sistemas de presentación espontánea para mayores de edad, búsqueda de personas no vacunadas para instarlas a recibir sus dosis o completar esquemas vacunatorios y la entrega de turnos de acuerdo a los registros de cada jurisdicción en base a la disponibilidad de los productos de al menos cinco laboratorios de distintos países.

El Consejo Federal de Salud, que integran todos los ministros del área de las provincias y la ministra nacional Carla Vizzotti, ratificó en su reunión virtual de ayer la decisión de avanzar con la vacunación contra COVID-19 a adolescentes sin comorbilidades para lo cual los distintos distritos convocaron a anotarse en las páginas oficiales a los interesados en recibir vacunas de Pfizer o Moderna, las únicas autorizadas para los menores de edad.

**Provincia de Buenos Aires**

En Buenos Aires, el ministro de Salud, Nicolás Kreplak, destacó que en la provincia se alcanzaron "las 16 semanas de descenso sostenido de casos de #COVID, con una reducción del 93% desde el pico de la segunda ola" y sostuvo que el avance de la campaña de vacunación en el distrito "con la aplicación récord de dosis durante julio y agosto son el principal motivo de este notorio descenso".

Tras indicar que este miércoles se habían enviado 500 mil nuevos turnos, el funcionario señaló que "tenemos que ser muy cuidadosos con las nuevas aperturas y que la vuelta a la normalidad sea de manera progresiva".

En ese sentido, el asesor del Ministerio de Salud bonaerense, Jorge Rachid, dijo que la provincia estaba "cerca de terminar (de vacunar) a los mayores de 18 años con una dosis".

En Bahía Blanca, el director de la Región Sanitaria Primera, Maximiliano Nuñez Fariña, dijo que estaban "confiados del trabajo que se viene haciendo con la vacunación y con la concientización del sistema sanitario y de la gente que está dando sus frutos con la baja de casos y de contagios".

**Córdoba**

En Córdoba, el Ministerio de Salud provincial anunció que el sábado comenzará con la inoculación de la primera dosis a los adolescentes de 17 años sin comorbilidades a quienes se les aplicarán productos de los laboratorios Moderna y Pfizer.

Según los datos oficiales, son aproximadamente 39.000 los inscriptos de esa franja etaria.

La última partida de vacunas que recibió la provincia fue ayer, con el envío del Gobierno nacional de 8.190 de Pfizer, 44.000 de AstraZeneca, 33.180 de Cansino, 12.880 de Moderna y 63.00 de la segunda dosis de Sputnik V, en tanto este mipercoles se espera la llegada de un nuevo cargamento de Pfizer.

Esta semana continúa la aplicación sin turnos de la segunda dosis a las personas que no pudieron hacerlo en el día asignado, embarazadas y puérperas hasta 45 días. Los mayores de 18 años que aún no hayan recibido el primer componente también son convocados a concurrir sin turnos a los centros de vacunación.

**Santa Fe**

En Santa Fe, las actividades de esta semana se centraban en la capacitación del personal sanitario para la aplicación de la Pfizer, ya que requiere formas de conservación, manipulación e inoculación distintas a las otras vacunas.

La coordinadora del programa provincial de Inmunizaciones, Soledad Guerrero, explicó que la de Pfizer tiene "características particulares en cuanto a la cadena de frío y al modo de preparación, dado que se trata de una vacuna que se debe diluir con suero fisiológico".

Santa Fe, que ya recibió desde el Estado nacional más de 4 millones de dosis, se prepara para inmunizar con la vacuna CanSino, de la que llegaron en las últimas horas las primeras 18.480 unidades.

Sobre esa vacuna, la directora de Territorios Saludables, Ana Paula Milo, explicó que es "de fácil conservación y se aplica de una sola vez (monodosis), lo que ofrece múltiples ventajas que ampliarán la accesibilidad", ya que se puede conservar a temperatura de heladera para llegar a zonas apartadas.

**Mendoza**

Mendoza recibió este mipercoles unas 4.600 dosis de la vacuna de Pfizer para aplicarla en adolescentes y esperaba el arribo de unas 17.000 dosis de vacunas de CanSino y 33.000 de Sputnik V, del segundo componente, informó el Ministerio de Salud local.

Fuentes de Salud dijeron a Télam que "hay un 84,70 % de vacunados con primeras dosis y 50,23% con la segunda" en la provincia y afirmaron que la estrategia se centrará en continuar vacunando a menores de edad de entre 12 y 17 años inclusive con enfermedades preexistentes, completar esquemas y la búsqueda activa de personas que no se han inmunizado.

**San Juan**

San Juan también anunció que comenzará este jueves a vacunar a adolescentes jóvenes menores de 18 años sin comorbilidades con la vacuna Pfizer-BioNTech, para continuar con el Plan Provincial de Vacunación.

El Ministerio de Salud Pública informó que "los adolescentes con comorbilidades inscriptos recibieron la vacuna Moderna" y el nuevo grupo sin enfermedades de base "comenzó a inscribirse semanas atrás y hasta el momento alrededor de 7.000 jóvenes se han registrado".

"La vacunación comenzará este jueves 16 de septiembre para todas las personas de este grupo con turno previo, convocados a través de mensaje de texto", añadió la cartera sanitaria y recordó que "ya tenemos 342.829 los sanjuaninos con dos dosis aplicadas y son 491.338 los que tienen la primera".

**Neuquén**

En Neuquén, el gobernador Omar Gutiérrez anunció que "con mucho entusiasmo, hoy comienza el registro de vacunación para personas de 12 a 17 años sin factores de riesgo" a través de la página del Ministerio de Salud de la provincia.

Además, informó que este miércoles recibían "5.880 dosis de Cansino, vacunas monodosis que serán destinadas a personas que residen en lugares de difícil acceso o áreas rurales" y también arribarán "7.800 dosis de la vacuna AstraZeneca para completar esquemas, mañana "llegarán 2.240 dosis de Moderna para menores con condiciones priorizadas y 11.250 Sputnik II para completar esquemas".

**Salta**

En Salta, en los próximos días comenzará la inmunización contra el coronavirus a adolescentes de 17 años sin comorbilidades y para ello, el ministro de Salud Pública, Juan José Esteban, dijo que esos adolescentes deberán inscribirse a través de la página de vacunación para recibir los biológicos de Pfizer, de los cuales ayer llegaron 3.510 dosis a la provincia, y Moderna.

La jefa del programa de Inmunizaciones, Adriana Jure, manifestó que trabajan para que en los próximos días se habiliten los turnos a través de la página oficial.

**Santiago del Estero**

En Santiago del Estero, con la llegada de las primeras 2.340 dosis de vacuna Pfizer, la ministra de Salud, Natividad Nassif, dijo que en forma conjunta entre Nación y provincia "hemos resuelto iniciar la vacunación de adolescentes sin comorbilidades en forma universal, empezando con los de 17 años".

La jefa del programa de Inmunizaciones, Florencia Coronel, explicó que a partir de mañana podrán comenzar a registrarse los que tengan los 17 años cumplidos en la aplicación vacunarse.sde.gob.ar y a partir del viernes se otorgarán los turnos.

Recordó que los adolescentes entre 12 a 17 años, con condiciones de riesgo continuarán recibiendo las dosis de Moderna para la inmunización contra Covid-19.

La ministra Nassif explicó que "en la población objetivo de la provincia tenemos el 90% con la primera dosis y con la segunda dosis al 60%", por lo tanto agregó que se está a un paso de cumplir con la "inmunidad del rebaño".

**Tucumán**

En Tucumán, el secretario Ejecutivo del Sistema de Salud, Luis Medina Ruiz, afirmó que según lo acordado en la reunión del Cofesa "septiembre es un mes para completar esquemas y en octubre está previsto comenzar con la vacunación contra el Covid-19 de jóvenes entre 12 y 17 años etaria sin comorbilidades".

Medina Ruíz contó que "en agosto en la provincia se comenzó la vacunación de pacientes de esta franja etaria que presentaban alguna enfermedad de base, a partir de la llegada de la vacuna Moderna y luego con la Pfizer, ya que representan una población de riesgo".

Tras la reunión con sus colegas de otras provincias, reportó que "entre el 60 y 80% de los viajeros que vienen del exterior ingresan al país con la variante Delta, por lo que se seguirán extremando las medidas de control con el objetivo de postergar la circulación comunitaria".

**Entre Ríos**

En Entre Ríos, 495.071 personas ya recibieron las dos dosis del esquema de vacunación, lo que representa el 36% del total de personas inoculadas.

Además, ayer llegaron las primeras 12.180 dosis de la vacuna china Cansino que será utilizada "en regiones de difícil acceso, ya que se facilita la logística para la vacunación", explicó el subsecretario de Redes Integradas de Servicios de Salud, Marcos Bachetti.

También arribaron dosis de las vacunas Astrazeneca, Moderna y Sputnik V, de fabricación argentina; y ayer llegaron las primeras 3.510 dosis de Pfizer.

**San Luis**

En San Luis, la aplicación de vacunas contra el coronavirus superó las 600 mil dosis y sigue la convocatoria para mayores de 18 años y adolescentes de 12 a 17 años en situación de salud priorizada.

**Río Negro**

En Río Negro, la gobernadora Arabela Carreras indicó que este miércoles comenzaba "la inscripción de vacunación para jóvenes entre 12 y 17 años sin factores de riesgo" dado que "llegarán a la provincia 1.170 dosis de Pfizer que se destinarán a este sector de la población",

El Ministerio de Salud indicó que la vacunación comenzará con las personas que tengan entre 16 y 17 años y continuará de manera escalonada por edades, mientras se esperan otras 2.660 dosis de Moderna para reforzar la vacunación a esos grupos.

**Chubut**

En Chubut, donde cerca de 220.000 están vacunados con las dos dosis y más de 362.000 tienen al menos una, el ministro de Salud, Fabián Puratich, indicó: "Estamos conformes con la campaña de vacunación y con el despliegue que se realiza en toda la provincia, llegando a todos los barrios de las ciudades y las poblaciones apartadas del interior".

En la actualidad se están aplicando todas las vacunas sin turno, es decir a sola presentación de los pobladores en los vacunatorios, salvo para el caso de la segunda dosis de Sputnik V que responde al calendario.

**Santa Cruz**

En Santa Cruz, el ministro de Salud y Ambiente, Claudio García, aseguró que la provincia logró un 82% de vacunación con la primera dosis a mayores de edad y destacó que "si tomamos cierta cantidad de personas que se vacunaron con la primera dosis y que hoy ya tienen el esquema completo de vacunación, estamos hablando de haber superado el 62% de las personas que ya tienen el esquema completo de vacunación".

García anticipó que Santa Cruz recibirá en los próximos días dosis de la vacuna Pfizer, que puede ser utilizada en adolescentes.

"Como sabemos antes de fin de año, están llegando 20 millones de dosis a la Argentina que serán destinadas principalmente a personas de entre 12 y 17 años de edad, y así estamos cubriendo a uno de los factores de riesgo más importantes como son las personas no vacunables como son los menores de edad", consideró.

"También estaremos recibiendo vacunas Cansino, que tienen la característica de ser de una sola dosis. Las mismas serán usadas dentro de lo que es la estrategia del primer nivel de atención, es decir, con una estrategia de descentralización del proceso vacunatorio y con la idea de llegar a la mayor cantidad de gente posible que aún no se ha vacunado por distintos motivos, como el de creer que la vacuna no tiene un efecto beneficioso, etc.", amplió.

García apuntó que "la idea es avanzar rápidamente con las personas que aún no se han vacunado y comenzar a vacunar a las personas menores de 18 años con las vacunas que actualmente están autorizadas, a sabiendas que es un modo experimental entre 12 y 17 años, ya disponemos en la Argentina Pfizer y Moderna".

**Tierra del Fuego**

El gobierno de Tierra del Fuego informó que había vacunado con dos dosis a 85.149 personas, lo que implica el 47,30% del total de la población.

La ministra de Salud, Judith Di Giglio, confirmó que si se considera a las personas mayores de 18 años, el porcentaje de vacunados con dos dosis se eleva al 60% y resaltó que se implementó un "nuevo dispositivo" para vacunar a más gente, con una convocatoria abierta, sin turno previo asignado.

El operativo incluye convocatorias especiales a jóvenes de más de 18 años y a menores de edad con las remesas de vacunas habilitadas para esa franja etaria, en especial la estadounidense Pfizer.

**Jujuy**

En Jujuy 270.763 habitantes completaron el esquema de vacunación, informaron este miércoles fuentes del Ministerio de Salud provincial y destacaron que se continuaba la vacunación con Sputnik V para completar el esquema.

También señalaron que ayer arribaron a Jujuy 1.170 dosis de la vacuna Pfizer que serán destinadas a adolescentes próximamente.

A la par de todo esto se desarrolla el operativo "puerta a puerta" y el VacuMóvil, de forma libre, para la aplicación de primera y segunda dosis de las vacunas disponibles.