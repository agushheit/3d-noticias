---
category: Estado Real
date: 2021-11-30T06:00:15-03:00
thumbnail: https://assets.3dnoticias.com.ar/perottiinseguro.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Perotti y Lagna junto a los jefes policiales;" Ratificamos y profundizamos
  el rumbo"
title: Perotti y Lagna junto a los jefes policiales;" Ratificamos y profundizamos
  el rumbo"
entradilla: El gobernador y el ministro de Seguridad reafirmaron el compromiso de
  sumar más presencia policial y equipamientos, para lo que plantearon clave contar
  con una Ley de Emergencia Económica.

---
El gobernador Omar Perotti, y el ministro de Seguridad, Jorge Lagna, encabezaron este lunes un encuentro con toda la cúpula policial, donde impartieron la orden de “ratificar y profundizar el rumbo” trazado “entre el delito y el Estado”.

"Nuestro mensaje fue claro, tanto el gobernador como el mío, ratificamos el apoyo total al policía que cumple sus funciones con mucho compromiso y atento al orden legal establecido", sentenció Lagna tras la reunión llevada a cabo en Rosario con la jefa de la Policía de la provincia, Emilce Chimenti; los jefes de las Unidades Departamentales, y jefes de secciones especiales.

"Estamos viviendo un momento de la Argentina, y de Santa Fe en particular, con efectos no deseados en materia de seguridad, que la gente está padeciendo, el diagnóstico no lo negamos", dijo el ministro y enumeró algunas de las causas: "El estado de pandemia posterior a la pobreza, la reacción de bandas criminales ante medidas concretas del gobierno, en esta línea que ha trazado entre delito y Estado, como los operativos en la calle junto a las fuerzas federales, y la investigación y esclarecimiento de hechos de delitos complejos".

En este sentido, indicó que se viven "raras situaciones" de intimidación pública, que "aparentemente no tienen un móvil definido", y ratificó: "El compromiso concreto, que está en marcha, de una inversión millonaria en equipamiento, comunicaciones, patrullajes, refuerzos y mayores remuneraciones. Por eso pedimos la Ley de Emergencia Económica, es una herramienta que necesitamos para acortar los plazos de compra de equipamientos y poder otorgar suplementos a los policías".

En el encuentro, Perotti y Lagna pidieron a las fuerzas de seguridad "reafirmar el trabajo en la calle, necesitamos seguir sumando presencia, e intensificar el trabajo conjunto con el municipio, y con las fuerzas federales".

Finalmente, el ministro señaló que el objetivo es "lograr mayor tranquilidad de los vecinos a través de mayor y más presencia. Los recursos presupuestarios están, al igual que el recurso humano: van a ingresar 857 nuevos policías en pocos días, y la mayoría se van a volcar a las grandes urbes como Rosario, donde tenemos la mayor problemática del delito".