---
category: Agenda Ciudadana
date: 2021-03-07T05:30:22-03:00
thumbnail: https://assets.3dnoticias.com.ar/MORATORIA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Lanzan nueva moratoria de impuestos provinciales
title: Lanzan nueva moratoria de impuestos provinciales
entradilla: Lo dispuso API y abarca todos los impuestos provinciales, en dos tipos
  de deuda ya sean pre pandemia y pandemia.

---
El titular de la Administración Provincial de Impuestos (API), Martín Avalos, recordó que “desde el 1 de marzo y hasta el 31 de mayo se encuentra vigente la nueva moratoria de tributos provinciales que hemos implementado para este año”.

Esta nueva moratoria “abarca a todos los impuestos, tasas y contribuciones provinciales: Impuesto Inmobiliario, Urbano y Rural, Patente, Ingresos Brutos, Impuesto de Sellos; también, alcanza al impuesto del Instituto Becario”, enumeró el funcionario.

Avalos insistió en que la medida comprende “a todos los tributos provinciales con la particularidad que está fragmentada en dos tipos de deuda: las deudas que se pueden denominar pre pandemia, que son las devengadas hasta el 29 de febrero de 2020; y las deudas de pandemia, que son las que se generaron entre el 1 de marzo y el 31 de octubre de 2020”.

**Formas de pago**

“Las deudas pre pandemia, hasta el 29 de febrero, tienen los beneficios de una reducción de intereses de liquidación; también, en el caso de pago contado se hace una condonación del 80 por ciento de los intereses. Y a su vez se puede pagar de manera financiada en 12, 24, 36 y 48 cuotas con intereses de financiación muy bajos, y también reducción de intereses que van entre el 1 y el 2,5 por ciento mensual de financiación”, aclaró Avalos.

Además, el titular de la API añadió a todo ello “la condonación del 100 por ciento de las multas que sean tanto por incumplimientos formales como por incumplimientos materiales”.

Con relación a las deudas tributarias generadas en la pandemia, Avalos subrayó que “se van a pagar a valores históricos, sin ningún tipo de interés de financiación” y que se podrán abonar “también hasta en 48 cuotas, sin intereses”.