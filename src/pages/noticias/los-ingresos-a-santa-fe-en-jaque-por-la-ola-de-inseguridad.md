---
category: La Ciudad
date: 2021-01-14T09:22:39Z
thumbnail: https://assets.3dnoticias.com.ar/14121-emboscados-la-autopista.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: '3DNoticias - Fuente: UNO Santa Fe'
resumen: Los ingresos a Santa Fe, en jaque por la ola de inseguridad
title: Los ingresos a Santa Fe, en jaque por la ola de inseguridad
entradilla: En las últimas horas se registraron diversos ataques a conductores en
  distintas rutas y accesos que llegan a la ciudad capital. Una problemática que no
  es nueva y que no encuentra solución.

---
Ingresar o salir de la ciudad de Santa Fe se convirtió en una peligrosa odisea para miles de conductores que circulan a diario por las rutas del área metropolitana. En las últimas horas, se registraron feroces ataques a automovilistas que terminaron perdiendo el control de sus vehículos, para luego ser interceptados por delincuentes. 

La problemática de la inseguridad y atracos en los accesos a Santa Fe no es un tema nuevo, y en la actualidad, continúa sin una resolución por parte de las autoridades de Seguridad.

Fuentes de la cartera de Seguridad, indicaron que, a raíz de los acontecimientos delictivos registrados en los ingresos a la ciudad, que tomaron repercusión pública en las últimas horas, **«se comenzarán a realizar patrullajes dinámicos con mayor presencia policial en las zonas con más reporte de episodios de inseguridad».**

La Circunvalación Oeste con su falta de iluminación en un largo trayecto entre la zona del Hipódromo y el intercambiador de la Ruta 70; los ingresos y egresos a la ciudad por autopista Santa Fe-Rosario, entre el puente del río Salado y la intersección de Iturraspe y avenida Perón; la Circunvalación a la altura de calle Mendoza en la zona de barrio Santa Rosa de Lima y también en el área del cruce con el puente peatonal entre los barrios Varadero Sarsotti y Centenario; la Ruta Nacional 168 a la altura de barrio La Guardia, en su intersección con el ingreso a Ruta Provincial 1; todos ingresos principales a la capital de la provincia donde se registran y se registraron hechos de inseguridad.

Las historias y casos de inseguridad son diversos y llegan a la opinión pública cuando, en un corto período de tiempo, se concretan en cantidad. Pero la realidad indica que la problemática de los ataques a conductores que transitan por las rutas de ingreso a la ciudad de Santa Fe, se manifiesta a diario. Muchos santafesinos, que ya conocen la situación y son víctimas de dichas agresiones, directamente eligen continuar su marcha (siempre y cuando puedan hacerlo) y bregar por su seguridad.

Esto se observa de forma cotidiana con los miles de vehículos que circulan por el ingreso oeste a la ciudad, por la autopista Santa Fe-Rosario. El sector santotomesino comprendido por la zona de barrios cerrados y countries, que según estimaciones tiene más de 10 mil habitantes permanentes, se convirtió en el último tiempo en un claro ejemplo de la inseguridad reinante, con autos apedreados y cristales rotos de forma frecuente, normalmente en el trayecto entre el puente del Salado y la intersección de calle Iturraspe y avenida Perón.

Lo mismo ocurre con la gente que vive en Sauce Viejo y viaja a diario por la autopista. La zona que circunda el barrio Las Vegas en Santo Tomé, también fue un sector gravemente y salvajemente afectado. De hecho, años atrás, se cercó con un tejido perimetral la zona para evitar los ataques a conductores. Según lectores de UNO Santa Fe, el patrullaje existente en el ingreso y egreso a la autopista Santa Fe-Rosario a la altura de Sauce Viejo, dejó de verse con cotidianidad.

Los últimos casos de ataques a conductores en los ingresos a Santa Fe tuvieron el mismo _modus operandi_: arrojar objetos contundentes en el medio de la carpeta asfáltica como piedras, adoquines o postes de hormigón o cemento, con el objetivo de frenar la marcha de los vehículos por el impacto con los elementos contundentes, para luego realizar ataques «piraña» a punta de armas de fuego. Esto fue lo que ocurrió este martes con la familia paranaense que viajaba por la autopista Santa Fe-Rosario desde Venado Tuerto con destino a Paraná.

Otros de los accesos a la capital provincial más transitados, y que también durante muchos años sufrió episodios de inseguridad con los automovilistas y colectivos que circulaban por la zona, es la Ruta Nacional 168. En las últimas horas, los vecinos de Colastiné y La Guardia, denunciaron el incremento y recrudecimiento de ataques a vehículos.

En definitiva, y a la luz de los reiterados hechos de ataques que se registraron en los últimos días, desde el Ministerio de Seguridad analizan por estas horas aumentar los patrullajes en los sectores más «calientes», apostando al factor sorpresa de los chequeos e inspecciones.

No obstante, los episodios de inseguridad contra automovilistas continúan registrándose a diario, y por fortuna, todavía no se reflejaron circunstancias fatales; hoy en día, se habla de vuelcos de vehículos, piedrazos y rotura de cristales en autos y colectivos, ataques piraña y hasta balazos en los laterales de autos; mientras tanto, las certezas en materia de seguridad no llegan, y a los conductores que circulan por las zonas de ingreso a la ciudad de Santa Fe, no les queda otra opción que contar con la fortuna necesaria para no convertirse en una víctima más.