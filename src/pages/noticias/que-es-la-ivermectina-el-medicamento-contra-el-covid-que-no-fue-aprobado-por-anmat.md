---
category: Agenda Ciudadana
date: 2021-01-26T09:21:01Z
thumbnail: https://assets.3dnoticias.com.ar/ivermectina.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: Qué es la ivermectina, el medicamento contra el covid que no fue aprobado
  por ANMAT
title: Qué es la ivermectina, el medicamento contra el covid que no fue aprobado por
  ANMAT
entradilla: Es una droga antiparasitaria que se aplicaría durante la incubación del
  virus para acelerar su eliminación. La SADI había advertido sobre su ineficacia,
  pero algunas provincias la aplican.

---
La droga antiparasitaria ivermectina comenzará a aplicarse en las provincias de La Pampa, Misiones, Corrientes y Chaco para el tratamiento contra el covid-19, aunque no ha sido autorizada por la Administración Nacional de Alimentos, Medicamentos y Tecnología Médica (ANMAT) como método de tratamiento. 

La ivermectina, usada tanto en medicina humana como veterinaria, se aplica para acelerar la eliminación del virus cuando se inicia un tratamiento durante la etapa temprana de infección hasta cinco días desde el inicio de los síntomas, con una dosis diaria de 0,6 miligramos por kilo de peso durante cinco días. 

El antiparasitario sería, además, un potencial inhibidor de la replicación viral del SARS-CoV-2. A pesar de que en octubre pasado la Sociedad Argentina de Infectología (SADI) había advertido sobre la ineficacia en el tratamiento del coronavirus, distintas provincias del país lo considerarán para el tratamiento de la enfermedad mientras aguardan a que lleguen las dosis restantes de la Sputnik V. 

En ese informe, la SADI explica que la ivermectina "es un potencial inhibidor de la replicación viral del SARS-CoV-2. Sin embargo, la evidencia disponible in vitro sugiere que para alcanzar niveles efectivos de ivermectina se necesitarían importantes aumentos y potencialmente tóxicos de la dosis". 

El comunicado precisó que incluso si se aplicara en cantidades "hasta 10 veces mayores que las aprobadas, no alcanzarían las concentraciones efectivas" para combatir al coronavirus.

**La aplicación de ivermectina en las provincias** 

La Pampa se suma a la "rebeldía sanitaria" de otras provincias como Misiones, Chaco y Corrientes, donde ya se utiliza la ivermectina como método complementario para pacientes de coronavirus, ya que actualmente en Misiones se utiliza plasma de convaleciente y antirretrovirales. 

El ministro de Salud pampeano Mario Rubén Kohan, anunció que la provincia comenzará a utilizar este jueves la droga como tratamiento para los pacientes con coronavirus: "Si no hay inconvenientes de logística, vamos a contar con el medicamento para iniciar el tratamiento", detalló el funcionario durante una conferencia de prensa. 

El funcionario había declarado hace dos semanas que reconocía la advertencia de la SADI y desaconsejó inmediatamente su uso para combatir el Covid-19, pero remarcó que "luego de los nuevos análisis publicados en el mundo, y el último en particular, encargado por la Organización Mundial de la Salud (OMS)", es "un camino adecuado" el que inicia su administración con la aplicación de la ivermectina. 

Por su parte, Misiones se incluyó en el listado de provincias que utilizan este medicamento. Días atrás, el ministro de Salud de Misiones, Oscar Alarcón, firmó un documento a través del cual se autoriza el tratamiento con ivermectina en pacientes afectados por coronavirus. 

Alarcón explicó que en el "complejo contexto epidemiológico nacional y provincial, corresponde la búsqueda de alternativas terapéuticas para el restablecimiento de la salud de los pacientes afectados con el virus SARS-COV-2 (COVID-19)". Misiones se utilizaba plasma de convaleciente y antirretrovirales. 