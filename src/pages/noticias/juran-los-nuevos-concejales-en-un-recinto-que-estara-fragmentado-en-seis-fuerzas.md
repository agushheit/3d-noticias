---
category: La Ciudad
date: 2021-12-09T06:15:27-03:00
thumbnail: https://assets.3dnoticias.com.ar/concejo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Juran los nuevos concejales en un recinto que estará fragmentado en seis
  fuerzas
title: Juran los nuevos concejales en un recinto que estará fragmentado en seis fuerzas
entradilla: 'Este jueves, a las 10, está previsto el acto. Con la jura de nuevos concejales
  y quienes debieron renovar su banca, el Concejo abre un nuevo período con seis espacios
  políticos, contando los "outsiders"

'

---
El Concejo Municipal enfrentará durante los próximos dos años una notoria fragmentación en su interior. El cuerpo a cargo de legislar por la ciudad contará con seis fuerzas distintas con al menos una banca. Las elecciones generales de noviembre modificaron el plano político en Santa Fe, donde hubo ganadores y perdedores de cara a la representatividad en el Concejo y la carrera por la intendencia en 2023.

Frente por frente, se vislumbra una mayoría que retuvo el oficialismo del Frente Amplio Progresista, aunque fue el único espacio que perdió una banca respecto al período 2019 - 2021, contando con seis ediles luego de las elecciones generales. Tenía siete representantes previo a las elecciones y no logró renovar los tres concejales a los que se les vencía el período.

Este bloque lo conforman los concejales Leandro González (último presidente del Concejo), quien renovó la banca junto con María Laura Mondino. También lo componen Valeria López Delzar, Lucas Simoniello, Mercedes Benedetti y Julio Garibaldi. La concejala María Laura Spina no compitió por renovar su banca y su lugar no pudo ser remplazado por la tercera candidata de la lista del FPCyS, Virginia Coudannes.

La segunda minoría la conforma el espacio Juntos por el Cambio, estatus que mantuvo gracias a los resultados electorales de noviembre, cuando fue la más votada de todas las fuerzas por amplio margen. Este bloque renovó tres bancas y contará con cinco concejales de aquí a 2023.

La fuerza está conformada por los nuevos ediles recientemente electos Adriana Molina, Hugo Marcucci y Carlos Pereira (quien debía renovar su banca). A los previamente mencionados se les suman Sebastián Mastropaolo e Inés Larriera, a quienes se les vence su período en 2023.

El Frente de Todos quedó con tres representantes en el Concejo, manteniendo la misma cantidad de ediles que tenía previo a la elección puesto que solo Jorgelina Mudallel debía renovar su banca y lo logró, aunque fue el único espacio que restó votos respecto a las Paso de septiembre (6.321 sufragios menos). El bloque justicialista lo conforman la mencionada Mudallel, Federico Fulini y Juan José Saleme, ambos con período hasta 2023.

El resto del cuerpo quedó conformado por tres fuerzas, todas con un representante. Guillermo Jerez quedó como el único concejal del espacio Barrio 88, ya que no pudo obtener otra banca a la que se postulaba Eliana Ramos durante las elecciones generales. Teniendo en cuenta que el Frente Renovador pierde a su único representante en el Concejo, Sebastián Pignata, los otros dos espacios los completan los dos outsiders que dieron la sorpresa.

Saúl Perman estará en el Concejo Municipal con el partido Mejor y su lista "La Causa". El activista que merodea por las calles de Santa Fe con su bicicleta y reparte abrazos será una de las caras nuevas del Concejo, de la mano de los casi 20.000 votos que obtuvo en las elecciones generales.

La sexta y última fuerza estará encabezada por el flamante concejal Juan José Piedrabuena. El cantante de cumbia ingresó al cuerpo con su espacio Unión Federal, logrando una gran elección tanto en las Paso como en las generales.