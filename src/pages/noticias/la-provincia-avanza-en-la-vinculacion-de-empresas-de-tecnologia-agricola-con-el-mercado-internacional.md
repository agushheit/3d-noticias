---
category: El Campo
date: 2021-02-06T07:13:37-03:00
thumbnail: https://assets.3dnoticias.com.ar/camo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: La provincia avanza en la vinculación de empresas de tecnología agrícola
  con el mercado internacional
title: La provincia avanza en la vinculación de empresas de tecnología agrícola con
  el mercado internacional
entradilla: Se busca potenciar al sector y posicionar a Santa Fe como líder nacional
  y mundial en tecnología agrícola. Hubo un encuentro virtual entre autoridades y
  la firma australiana Bioheuris dedicada al control de malezas.

---
El Ministerio de la Producción de la provincia, a través de la Subsecretaría de Comercio Exterior y Nuevas Tecnologías, trabaja en la vinculación de empresas santafesinas que se dedican al desarrollo de tecnologías para el agro, con fondos de inversión y aceleradoras internacionales.

Según expresó el subsecretario del área, Lucas Candioti, se busca “potenciar al sector y posicionar a Santa Fe como líder nacional y mundial en AgTech (tecnología agrícola). Asimismo, generar sinergias y oportunidades de negocios -continuó- que sean más accesibles para los emprendedores de la provincia, como parte de una estrategia público/privada para potenciar su internacionalización”.

**Encuentro virtual**

En esta línea, la Agencia Santafesina de Inversiones y Comercio Exterior Santa Fe Global, organizó en conjunto con la Embajada Argentina en Australia, un encuentro virtual con la empresa Bioheuris SA. El objetivo de la reunión fue buscar una estrategia de vinculación con fondos y aceleradoras australianas, a través de contactos directos con potenciales inversores y/o asociaciones en dicho país.

Bioheuris SA se dedica al desarrollo de sistemas sustentables de control de malezas. La misma está enfocada en el diseño y obtención de tecnologías que le permitan al productor optimizar su rendimiento, retrasando la aparición de malezas resistentes y reduciendo la carga de herbicidas en el ambiente.

Dicha firma cuenta con laboratorios de biología molecular e instalaciones para manejo de plantas en el Centro Científico Tecnológico de Rosario (Argentina), donde se encuentran en funcionamiento las plataformas de Biología Sintética (HEURIS™) y de Edición Génica (SWAP™).

Además de Lucas Candioti, participaron del encuentro el ministro encargado de Negocios de la Embajada en Australia, Leandro Waisma; el socio fundador y director de Estrategia de Bioheuris, Carlos Pérez y Victoria Nagel de la Aceleradora Litoral, y la asesora técnica Delfina López.