---
category: La Ciudad
date: 2021-05-30T06:15:32-03:00
thumbnail: https://assets.3dnoticias.com.ar/terapiajpg.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Médicos: al estrés por el pico de la pandemia, se le suma la presión por
  demandas judiciales'
title: 'Médicos: al estrés por el pico de la pandemia, se le suma la presión por demandas
  judiciales'
entradilla: Desde el Colegio de Médicos expresaron su preocupación por la judicialización
  de muertes por Covid-19. Aseguran ser "respetuosos de la justicia".

---
Crece la preocupación en el Colegio de Médicos de Santa Fe por las demandas judiciales hacia profesionales de la salud realizadas por familiares de pacientes fallecidos por Covid-19.

"Nosotros no queremos más aplausos porque sabemos que después de los aplausos vienen las demandas", afirmó Carlos Alico, presidente de la entidad. Informó que los médicos ya están "teniendo demandas por muertes por COVID" y recordó que en el país ya "se murieron 75 mil personas" por la enfermedad.

"Hasta nosotros mismos todavía no conocemos bien la fisiopatología de la enfermedad, cómo se comporta. Se está comportando de una manera errática. Sabemos algunas cosas y nos faltan otras; hacemos lo que sabemos y lo que podemos", definió.

El caso de Lara Arreguiz no sería el único judicializado. Cabe recordar que la muerte de la joven de 22 años de Santa Fe se encuentra bajo investigación luego de la denuncia realizada por la familia a todo el personal de los tres hospitales por donde transitó la chica.

Disgustado por el tratamiento que le dieron medios nacionales al caso, Alico sostuvo que se generaron "situaciones de extremo temor, pánico y de preocupación en las personas".

El referente de los profesionales médicos señaló que se anima a decir "lo que quieren decir otros y que probablemente no han tenido la posibilidad de hacerlo". Y apuntó: "Nosotros no queremos más aplausos porque sabemos que después de los aplausos vienen las demandas. Estamos teniendo demandas por muertes por COVID, pero en Argentina se murieron 75 mil personas por COVID".

"Creo que esto se va a profundizar cada vez más; haciendo el trabajo de todos los días podés tener una demanda", añadió, y volvió a insistir con el planteo realizado por varios medios de comunicación de Buenos Aires: "No puede ser que por nuestro trabajo estemos sometidos a esta situación, que además te juzga cualquiera y empiezan a decir cualquier cosa y generar más dudas para hacer una noticia con mayor audiencia. Es una realidad muy peligrosa".

Subrayó que "en esta instancia seguimos siendo respetuosos de la justicia. No es que pretendemos que no se nos juzgue. Lo que queremos es igualdad, y en nuestra profesión no existe la igualdad".

Comentó que la atención se le está dando "a todos y cada uno, acompañando a las familias, acompañándolos en sus angustias, con COVID o sin. Nosotros estamos teniendo otra pandemia enorme que es la social, económica, familiar, psicológica que también tenemos que asistirla. No es solamente que tenemos un bicho y el resto no tenemos ningún tipo de problemas. Hay un montón de gente que hoy en el día a día no puede trabajar y eso es un problema".

**Sin camas, ni recurso humano**

El titular del Colegio de Médicos de Santa Fe destacó en diálogo con este medio que los profesionales sienten la necesidad de "transmitir la realidad de una situación que es muy grave".

"La gente está verdaderamente padeciendo la enfermedad y cuando nosotros miramos los porcentajes de personas que se enferman y hacemos un análisis matemático, decimos que el 5 por ciento es altamente probable que requiera camas críticas. Estamos preocupados porque no tenemos más camas críticas y lo peor: no tenemos recurso humano", agregó.

"A nadie le va a gustar que a un familiar o a uno mismo, que requiera atención en cualquiera de los efectores de salud, no te la puedan dar porque no exista la posibilidad", afirmó Alico.

Como lo informó este medio la semana pasada, en la ciudad de Santa Fe hay 32 médicos terapistas. En ese sentido, Alico aclaró que "de todas maneras esta actividad también la están desarrollando los clínicos, los médicos internistas, algún cardiólogo, que son profesionales que están capacitados para realizar actividades de terapia intensiva de adultos. Hay muchísimos terapistas que están trabajando en sus trabajos fijos, pero a su vez están trabajando en otras instituciones tratando de dar una mano, sabiendo que se necesita la ayuda, aunque la remuneración sea muy mala".

Por último, apuntó: "Todo este tipo de cosas genera un agotamiento muy grande, esto es una verdadera guerra. Si nosotros hoy nos metemos en contra de aquellos que están trabajando denodadamente, porque me consta, aun poniendo en juego su propio físico, su propio cuerpo y su propia familia, y nos tiramos en contra de ellos, me parece que no construimos nada". Alico reclamó por información "veraz, constructiva, propositiva, porque de lo contrario termina siendo peligrosa".