---
category: Estado Real
date: 2021-04-30T11:09:06Z
thumbnail: https://assets.3dnoticias.com.ar/corach.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: La provincia mantuvo nuevos encuentros con intendentes, presidentes comunales
  e integrantes del comité de expertos provincial
title: La provincia mantuvo nuevos encuentros con intendentes, presidentes comunales
  e integrantes del comité de expertos provincial
entradilla: Los ministros de Gestión Pública, Marcos Corach, y de Salud, Sonia Martorano
  encabezaron sendas reuniones virtuales. El objetivo fue analizar la situación epidemiológica
  actual y las últimas medidas implementadas.

---
El ministro de Gestión Pública, Marcos Corach; y la ministra de Salud, Sonia Martorano, mantuvieron este jueves encuentros con intendentes, presidentes comunales e integrantes del Comité de Expertos provincial. 

Por un lado, Corach, junto al secretario de Integración y Fortalecimiento Institucional, José Luis Freyre, se reunieron con intendentes y presidentes comunales de la provincia. Al respecto, el ministro afirmó que “les brindamos detalles sobre la reunión que mantuvo el gobernador con el presidente de la Nación e hicimos una puesta en común sobre la situación sanitaria actual en cada una de las comunas y municipalidades”.

“Estamos teniendo en cuenta dos cosas: por un lado, el nuevo decreto nacional y, por el otro, que el 2 de mayo vence el decreto provincial. Por lo cual, lo que planteamos en la reunión con los presidentes e intendentes comunales es poder evaluar el estado de situación para poder ajustar el texto del decreto que estaremos emitiendo en los próximos días”, puntualizó Corach.

Por su parte, los intendentes y presidentes comunales valoraron las medidas aplicadas por el gobierno provincial, remarcando que una gran parte de la ciudadanía las ha acatado y las está cumpliendo. Asimismo, remarcaron su preocupación por las reuniones sociales; valoraron el accionar policial mucho más marcado durante el fin de semana donde se vieron mayores actuaciones. 

**Reunión con el Comité de Expertos**

Por otro lado, Martorano mantuvo un nuevo encuentro virtual con el Comité de Expertos que la asesoran, durante el cual repasaron los indicadores de la provincia, haciendo foco en la creciente ocupación de camas. 

Los asesores expresaron su preocupación respecto a las pocas plazas que hay en la provincia, tanto en el sector público como privado. Asimismo, plantearon la falta de cuidados en ciertos sectores de la sociedad, lo que genera que se evalúe tomar medidas más duras y restrictivas. 

En este sentido, se hizo hincapié en la necesidad de sostener las actividades productivas, reforzar la restricción de horario y los controles con el objetivo de evitar que sigan aumentando los contagios.