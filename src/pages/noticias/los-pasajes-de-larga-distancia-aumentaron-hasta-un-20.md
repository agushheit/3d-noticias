---
category: La Ciudad
date: 2021-12-12T06:00:30-03:00
thumbnail: https://assets.3dnoticias.com.ar/micros.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Los pasajes de larga distancia aumentaron hasta un 20%
title: Los pasajes de larga distancia aumentaron hasta un 20%
entradilla: "Los boletos provinciales mantuvieron la tarifa vigente. El aumento es
  habitual en esta época del año, según las agencias de turismo.\n\n"

---
A partir de este jueves comenzó a regir un aumento en los pasajes nacionales de colectivos. Según pudo consignar , la tarifa acrecentada está vigente desde hace unos días pero recién ahora empezó a implementarse.

Un vendedor de una de las agencias nacionales que operan en la Terminal de Ómnibus de Santa Fe confirmó que "el aumento en la tarifa se da siempre en ésta época del año y que luego de la temporada de verano los precios vuelven a bajar".

Con esta suba en la tarifa, un pasaje a Buenos Aires en coche semicama que antes costaba $ 3200 ahora sale $ 4000.

Según testimonios brindados, la gente no está enterada de este aumento pero desde las agencias aseguran que entienden la medida debido a que el país ingresó en la temporada alta de turismo.

Por su parte, según confirmó una agencia de viajes de corta distancia a este medio, la tarifa de estos pasajes aún no se vio afectada por los aumentos.