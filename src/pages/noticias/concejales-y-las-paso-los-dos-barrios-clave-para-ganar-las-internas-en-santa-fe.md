---
category: La Ciudad
date: 2021-09-08T06:00:14-03:00
thumbnail: https://assets.3dnoticias.com.ar/mapa.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Concejales y las PASO: los dos barrios clave para ganar las internas en
  Santa Fe'
title: 'Concejales y las PASO: los dos barrios clave para ganar las internas en Santa
  Fe'
entradilla: En un solo distrito se concentra el 20% de los electores habilitados para
  votar en la ciudad. Con récord de listas de concejales, los barrios toman protagonismo
  para definir quién pasa a las generales.

---
De cara a las PASO 2021, los precandidatos a concejales de la ciudad palpitan lo que serán unas elecciones con récord de listas para ganar sus internas y participar de los comicios generales. Hacerse fuerte en sectores clave de la ciudad puede ser determinante para posicionar la boleta entre las que participarán en las elecciones definitivas de noviembre, en las que buscarán ocupar una de las ocho bancas vacantes en el Concejo.

Según datos del Tribunal Electoral, son 312.927 los santafesinos habilitados como electores en la ciudad, sumado a 933 mesas de votación según el padrón electoral. Se sumaron 1.105 personas habilitadas para votar desde las pasadas elecciones generales de 2019, cuando se registraban 311.852 votantes.

Un dato importante para tener en cuenta es que una sola seccional electoral de las 18 que se dispusieron en la ciudad representa un 20% de la cantidad total de votantes habilitados para emitir sufragio en Santa Fe. Esto podría definir una elección en la que, con el récord de 43 listas, la reñida contienda que esperan los precandidatos a concejales que compiten en sus internas podrían sacar una ventaja en estos distritos.

**Seccionales con más votantes**

La Seccional Décima es el distrito con mayor cantidad de votantes habilitados para votar este domingo en las PASO, abarcando en gran parte a todo barrio Las Flores y otros barrios del sector noroeste de la ciudad. En total, la seccional cuenta con 63.136 personas habilitadas para sufragar, contando además con 186 mesas electorales, también el mayor número.

Para poner en perspectiva esta cifra, el edil José Garibaldi del Frente Progresista Cívico y Social fue el candidato más votado para las elecciones para renovar bancas de concejales en 2019 con un total de 64.022 votos, solo 900 más que la totalidad de votantes de la Seccional Décima. Del mismo espacio político, la candidata que ingresó al concejo con la menor cantidad de votos obtenidos fue Mercedes Benedetti, con 16.005 votos, solo un 25% del total de personas habilitadas a votar en el distrito del noroeste de la ciudad.

En la lista de seccionales con mayor cantidad de votantes le sigue de lejos la Seccional Séptima, abarcando en mayor parte a barrio María Selva. Cuenta en total con 29.777 electores que podrán votar en 87 mesas. En tercer lugar, se encuentra el distrito de Guadalupe, con 26.618 votantes y 79 mesas habilitadas.

Entre los distritos con menor cantidad de electores habilitados en estas elecciones se encuentran La Guardia (2.010 personas), el distrito de la Subcomisaría 7ma de barrio El Pozo (4.414 personas) y el barrio costero de Colastiné (6.006 personas).

**Participación en jaque**

Teniendo en cuenta la cantidad de electores habilitados según el padrón (312.927 personas) entra en juego la variable de la participación electoral. Se avizora que a raíz de la pandemia y la apatía generalizada estas PASO pueden ser los comicios con menor participación de votantes de la historia. En 2019, durante las últimas elecciones, de 311.852 votantes habilitados votaron 207.145, un 66,42%.

El factor de la distribución poblacional y geográfica de los votantes de Santa Fe puede ser trascendental para los espacios políticos que deban competir en las internas. El domingo se deberán definir siete contiendas partidarias de concejales entre los siguientes espacios: Juntos Por el Cambio, Frente de Todos, Primero Santa Fe, Frente de Izquierda, Frente Federal Vida y Familia, Unite por la Libertad y la Dignidad, e Igualdad y Participación.