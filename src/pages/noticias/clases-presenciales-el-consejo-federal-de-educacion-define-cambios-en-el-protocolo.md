---
category: Agenda Ciudadana
date: 2021-08-27T06:15:33-03:00
thumbnail: https://assets.3dnoticias.com.ar/CONSEJO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: 'Clases presenciales: el Consejo Federal de Educación define cambios en el
  protocolo'
title: 'Clases presenciales: el Consejo Federal de Educación define cambios en el
  protocolo'
entradilla: Los ministros de Educación de las provincias y la ciudad de Buenos Aires
  se reunirán para validar las modificaciones propuestas por el Gobierno nacional.
  Buscan implementarlos a partir del 1 de septiembre.

---
El Consejo Federal de Educación se reunirá este jueves para analizar y validar los cambios en el protocolo de la asistencia a clases en los niveles primario y secundario en todo el país, que fueron definidos por el Ministerio y sus asesores, para "intensificar la presencialidad a partir del 1 de septiembre", según adelantó el ministro de Educación Nicolás Trotta.

El Consejo, que agrupa a todos los ministros de Educación de las provincias y la ciudad de Buenos Aires, se reunirá a partir de las 15:00, encabezado por Trotta, quien previamente participará de la presentación de un informe sobre relevamiento y fortalecimiento de políticas socioeducativas en la post pandemia, según la agenda oficial.

Este miércoles, Trotta se reunió con el Consejo Asesor para el Regreso a las Clases Presenciales, y se definió la propuesta con las modificaciones de los protocolos sanitarios para aumentar la presencialidad en las escuelas.

"Terminamos de consensuar los cambios en el protocolo" porque "para nosotros es importante siempre el regreso seguro a las aulas. Escuchamos a representantes de la Sociedad Argentina de Pediatría, la Organización Panamericana de la Salud, la Organización Mundial de la Salud, Unicef y el Ministerio de Salud nacional", indicó el funcionario tras la reunión.

A menos de un mes de la llegada de la primavera y de los días más cálidos, Trotta adelantó que "la idea es que, a partir del miércoles 1 de septiembre, todos los alumnos y alumnas -del nivel primario y secundario- vayan a la escuela todos los días, todas las semanas".

Estos cambios propuestos responden a la mejora de los indicadores epidemiológicos junto al avance en el proceso de vacunación y el aumento de las temperaturas en el territorio argentino, lo cual ofrece una posibilidad de dar paso a nuevas instancias que continúen fortaleciendo la presencia en las aulas, informó en un comunicado la cartera educativa.

La propuesta definida contempla tres escenarios posibles y refiere a condiciones mínimas, sobre las cuales cada jurisdicción podrá adicionar otras medidas que considere convenientes: - Condición óptima: En el caso de que las escuelas puedan asegurar la presencialidad completa manteniendo un distanciamiento físico de 1.5 m entre estudiantes, sin dejar de ventilar, asegurar el uso de mascarillas y la higiene de manos. Para mantener esta distancia es necesario aprovechar al máximo el mobiliario escolar en toda su extensión.

\- Condición admisible: En el caso de que no sea posible asegurar el distanciamiento de 1.5 m para una presencialidad plena, se tomará una distancia física de 0.90 m entre estudiantes en las aulas, manteniendo el requerimiento de 2 m en los espacios comunes y con el cuerpo docente.

En este caso, aumenta el requisito de ventilación, no solamente manteniendo abiertas ventanas y puertas sino incrementando el tiempo de ventilación entre clases.

\- Excepciones: Solo en el caso de que no sea posible mantener un distanciamiento físico de 0.90 m entre estudiantes, se podrá mantener una distancia menor; y las excepciones podrán aplicarse en contextos de bajo riesgo epidemiológico y con adecuada cobertura de vacunación en la población general.

No obstante, estas excepciones deberán ser acompañadas de las siguientes medidas obligatorias: mantener e incrementar el resto de las medidas implementadas; la distancia entre estudiantes y docentes, así como entre docentes, se deberá mantener en 2 m; el distanciamiento en zonas de uso común, incluyendo comedores, no podrá ser menos a los 2 m y agregar estrategias preventivas adicionales.