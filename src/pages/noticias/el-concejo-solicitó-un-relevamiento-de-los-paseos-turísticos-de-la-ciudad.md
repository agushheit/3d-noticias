---
layout: Noticia con imagen
author: "Fuente: Prensa Carlos Suarez"
resumen: Paseos turísticos de la ciudad
category: La Ciudad
title: El Concejo solicitó un relevamiento de los paseos turísticos de la ciudad
entradilla: "Se trata de un proyecto del concejal de UCR-Juntos por el Cambio,
  Carlos Suárez, que fue acompañado por la totalidad del cuerpo. "
date: 2020-11-07T14:57:35.703Z
thumbnail: https://assets.3dnoticias.com.ar/ciudad1.jpg
---
“Este año se va a imponer la metodología del turismo de proximidad, por eso es importante saber cómo están estos corredores y ponerlos en condiciones”, afirmó el edil.

En la última sesión del Concejo Municipal se aprobó un proyecto de resolución del concejal de UCR-Juntos por el Cambio, Carlos Suárez, que insta al municipio a realizar un relevamiento de los sitios turísticos tradicionales de la ciudad, a fin de determinar su estado y condiciones de infraestructura. “Esta temporada estival el turismo va a ser de proximidad, inevitablemente. Si queremos que la ciudad esté a la altura de la situación, será necesario tener en condiciones esos lugares propios y emblemáticos, que son elegidos por la ciudadanía santafesina como lugar de recreación y divertimento”, afirmó el edil.

Entre los paseos que deberán ser relevados y puestos en condiciones se encuentran: la personal Martín en toda su extensión; el Paseo Bulevar; el circuito de la Constitución; el casco histórico de la ciudad; el entorno Basílica de Guadalupe; los museos municipales.

En estos espacios se evaluarán las condiciones edilicias estructurales, la infraestructura de accesibilidad, si el entorno permite garantizar el distanciamiento social, la disponibilidad de lugares para la instalación de sistemas de protección e higiene, la existencia de cestos para residuos, entre otros aspectos.

“Nuestra ciudad se caracteriza por tener espacios turísticos relacionados a la historia, la religión y el esparcimiento. Estos lugares tienen que ser protegidos y mantenidos desde el Estado, por ser íconos para Santa Fe. Ahí también está la importancia de poder determinar las condiciones en las que se encuentran y qué medidas son necesarias de llevar adelante, en el marco de la nueva normalidad en la que nos encontramos”, afirmó Carlos Suárez.