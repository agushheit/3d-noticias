---
category: Estado Real
date: 2021-01-19T09:00:37Z
thumbnail: https://assets.3dnoticias.com.ar/licitacion.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de la Provincia de Santa Fe
resumen: La provincia licitó la construcción de 50 soluciones habitacionales para
  Santa Fe
title: La provincia licitó la construcción de 50 soluciones habitacionales para Santa
  Fe
entradilla: Se presentaron cuatro ofertas para construir viviendas cuyo objetivo será
  resolver situaciones de emergencia que exijan una respuesta habitacional inmediata.
  Las mismas están destinadas para la capital provincial.

---
El Gobierno de la Provincia, a través del Ministerio de Infraestructura, Servicios Públicos y Hábitat, realizó la primera de dos licitaciones para la provisión y montaje de paneles prefabricados, para la construcción de cien soluciones habitacionales -cincuenta en Santa Fe e igual cantidad en Rosario- para atender situaciones que se consideren de emergencia y /o alto grado de vulnerabilidad social.

Al respecto, el secretario de Hábitat, Urbanismo y Vivienda, Amado Zorzón explicó que “se trata de soluciones habitacionales de rápida construcción y entrega, con el propósito de contar con viviendas para situaciones de contingencia, como reubicaciones de familias por riesgo de inundaciones, incendios, trazado de calles o caminos y / o atención de casos de personas con alto grado de vulnerabilidad social. Cada caso se analiza para concretar la solución habitacional”.

“Nosotros hemos definido tres puntos claves para comenzar a abordar esta situación, uno es la ciudad de Santa Fe y 100 kilómetros a su alrededor, lo mismo para el caso de la ciudad de Rosario, donde este jueves celebraremos la apertura de sobres. Y próximamente, estaremos llamando a licitación en la ciudad de Reconquista, de manera de poder abarcar distintas zonas de la provincia y donde hay mayor concentración de habitantes, para poder tener esta reserva de viviendas que nos permita actuar en forma inmediata”, agregó el funcionario.

En este sentido, Zorzón recordó que el hábitat y la vivienda es una de las prioridades que ha marcado el gobernador Omar Perotti, ya que la necesidad es dinámica y constante. “A través de la gestión del gobernador y la ministra Silvina Frana, con el Gobierno Nacional se han conseguido 4.242 viviendas para construir este año, más las que desde la Provincia ya estamos construyendo, creo que vamos a estar superando holgadamente las 7 mil unidades, porque además hay viviendas de Procrear que estaban paralizadas y se reactivarán para su finalización y entrega”, concluyó.

**LAS OFERTAS**

Se presentaron cuatro oferentes para la provisión y montaje de paneles prefabricados, para la construcción de cincuenta soluciones habitacionales, cuyo presupuesto oficial alcanza los $87.000.000,00. La primera oferta correspondió a la empresa MUNDO CONSTRUCCIONES S. A.; la segunda a COEMYC S. A., la tercera a un consorcio de cooperación conformado por PREMOLDEADOS BERTONE S. R. L. y PILATTI S. A. y la cuarta y última oferta presentada fue de ORION INGENIERÍA S. R. L.

A propuesta de la Dirección Provincial de Auditoria, dichas ofertas económicas permanecerán cerradas y estarán en custodia en la Escribanía Mayor de Gobierno, hasta tanto la Comisión de Evaluación de la Secretaría de Hábitat, Urbanismo y Vivienda, seleccione los prototipos idóneos a construir, según especifica el pliego de bases y condiciones. Por lo que una vez aprobados los prototipos de viviendas propuestos por las empresas, se procederá a la apertura de las ofertas económicas correspondientes a las mismas, y de esta forma se procederá a evaluar las ofertas.

**PRESENTES**

Participaron de la apertura de sobres el director de Vivienda y Urbanismo, José Manuel Kerz; el director Provincial de Localización y Adquisición de Tierras, Francisco Alda: la concejala de la ciudad de Santa Fe, Jorgelina Mudallel, oferentes y público general.