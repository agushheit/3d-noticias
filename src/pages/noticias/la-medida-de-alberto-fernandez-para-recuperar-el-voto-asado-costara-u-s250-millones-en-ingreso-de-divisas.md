---
category: Agenda Ciudadana
date: 2021-05-19T08:48:04-03:00
thumbnail: https://assets.3dnoticias.com.ar/vaca.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Iprofesional
resumen: La medida de Alberto Fernández para recuperar el "voto asado" costará u$s250
  millones en ingreso de divisas
title: La medida de Alberto Fernández para recuperar el "voto asado" costará u$s250
  millones en ingreso de divisas
entradilla: Es lo que no se liquidará en el país durante los 30 días de cese de exportaciones.
  Malestar y preocupación por desabastecimiento y precios más caros

---
El cierre de exportaciones de carnes por 30 días que dispuso informalmente el Gobierno le costará al país alrededor de u$s250 millones. Ese es el cálculo que hacen en el sector que este martes oficializó el paro de comercialización en rechazo de las medidas tomadas por el oficialismo. Dicen que las exportaciones de carne ingresan u$s3.000 millones al año y que debido a estas medidas "se perderán mercados que serán difíciles de recuperar": 

Claramente si el Gobierno está necesitado de que las divisas lleguen al país, el cierre de exportaciones va por el camino contrario. A la prohibición de exportar le podría haber antecedido un aumento en las retenciones. Actualmente el Gobierno se lleva parte importante de la renta del campo vía las retenciones. Según la Confederación Rural Argentina (CRA), el Estado se queda con el 63% del precio de la soja que se exporta, el 51% del trigo y el 48% del maíz. "La apropiación de la renta del sector es récord histórico y se ha transformado en un feroz robo institucional al tejido productivo del interior", dice un comunicado de CRA. 

**El paro como respuesta y negociación**

De ahí que el malhumor del sector exportador con esta medida provocó el paro que se realizará en los próximos días. La Mesa de Enlace lo convocó para todas las categorías de hacienda vacuna desde las cero horas del jueves 20 de mayo hasta las 24 del viernes 28 de mayo. 

Para muchos en el sector, el Gobierno busca -a su manera- tender puentes de negociación como sucedió con el cese de exportaciones de maíz del año pasado que también derivó en un paro. En ese momento las medidas de fuerza sirvieron para sentarse a negociar con el oficialismo pero poco tuvo como efecto en términos de una caída de los precios.  

De hecho, la medida que lanza el Gobierno no hará que los precios de la carne en el mostrador baje. Sino más bien, todo lo contrario. Miguel Schiariti, titular de la Cámara de la Industria y Comercio de Carnes (Ciccra), dice que lo más probable es que al haber un paro haya faltante de productos y encarecimiento de precios. O sea, el efecto contrario al buscado por el Ejecutivo. 

**El "voto asado" y las elecciones**

"El Gobierno suspende exportaciones de carne por 30 días, decidido a recuperar el 'voto asado', que está en mínimos porque prometió algo que no cumplió", dice el economista Fernando Marull, director de FMyA. El comentario viene a cuento de que no es necesariamente que el precio de la carne haya subido tanto (de hecho lo hizo menos que otros productos), sino que el salario real cayó notablemente. De ahí que el Gobierno intenta hacer una puesta en escena precisamente en un año electoral.  

Los especialistas no creen que las medidas como el cierre de las exportaciones haga que los precios bajen. Y creen que el efecto se verá en los próximos días con un aumento o directamente desabastecimiento. Y para peor, se percibirán menos divisas tan necesarias para la Argentina.

La liquidación de los mega precios de las materias primas es lo único que posibilita la paz cambiaria, con la soja en torno a u$s600 la tonelada. "No tiene sentido generar este clima en el campo porque los productores van a contestar. Y el Gobierno sienta un mal precedente si cree que frenando exportaciones va a controlar los precios. Es una señal de que no saben como parar la inflación que ellos mismos generaron", afirman en el sector.