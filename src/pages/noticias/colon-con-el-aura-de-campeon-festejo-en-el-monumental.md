---
category: Deportes
date: 2021-07-19T06:00:39-03:00
thumbnail: https://assets.3dnoticias.com.ar/COLON.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Colón con el aura de campeón festejó en el Monumental
title: Colón con el aura de campeón festejó en el Monumental
entradilla: Colón superó a River en el Monumental por 2-1 con goles de Rodrigo Aliendro
  y Yeiler Góez. Fue expulsado a los 13' del primer tiempo Gonzalo Piovi

---
Colón edificó un triunfo enorme ante River dejando en claro porque es el campeón del fútbol argentino. Jugó un gran primer tiempo y se fue a los vestuarios ganando por dos goles. Pero además desde los 13' de esa primera etapa jugó con un hombre menos por la expulsión de Gonzalo Piovi.

Se observó un equipo ordenado, con fluidez en el manejo del balón que jugó sin complejos, pese a que enfrente estaba River, aunque con una formación alternativa. No obstante, el Rojinegro mostró el libreto de siempre y por eso consumó una victoria notable para seguir revalidando este presente exitoso que lo tiene bien arriba n el fútbol argentino.

El inicio del partido no podía ser mejor para Colón ya que a los 2' se puso en ventaja. Y con una fórmula repetida, centro de Facundo Mura y gol de Rodrigo Aliendro. Al igual que lo sucedido ante Racing en la final. Aunque en esta ocasión, el volante se anticipó a Jonathan Maidana y con una media vuelta superó la resistencia de Armani.

Un arranque a pedir del elenco rojinegro, haciendo gala de su contundencia, en la primera llegada a fondo terminó facturando. Y River respondió de manera casi inmediata, pero no contó con la misma eficacia. A los 5' Jorge Carrascal escapó por derecha pero su remate encontró las manos de Ignacio Chicco.

El equipo local tenía más la pelota, pero el Sabalero no sufría en defensa. Y estuvo cerca de anotar el segundo gol, se escapó por derecha Christian Bernardi y el volante cordobés asistió a Facundo Farías quien ingresando por el sector izquierdo remató por arriba del travesaño.

Sin embargo, a los 13' se escapó Carrascal y Gonzalo Piovi lo derribó cuando el colombiano quedaba cara a cara con Chicco. El árbitro Darío Herrera no dudó y le mostró la tarjeta roja por último recurso. Por ello, Eduardo Domínguez mandó a la cancha a Bruno Bianchi en lugar de Alexis Castro.

Y pudo llegar al empate River, pero el cabezazo de José Paradela terminó rebotando en el caño derecho y el balón se fue al córner, en la llegada más clara del equipo local. Con uno menos Colón se replegaba y era el Millonario el que se mostraba más peligroso.

Pero Colón pese a jugar con un hombre menos nunca había perdido el orden ni la solidez y por ello, a los 33' Yeiler Góez marcó un golazo. Primero quitó, enganchó y asistió a Farías, quien le ganó en velocidad a Javier Pinola y metió el centro atrás, para que el colombiano con un toque sutil estableciera el 2-0.

Sin delanteros, Colón cuando llegaba golpeaba, aprovechaba la dinámica de sus volantes ante una defensa de River que ofrecía todo tipo de ventajas. Era muy marcada la diferencia de velocidad entre los futbolistas sabaleros y los defensores millonarios.

Colón terminó rendondeando un primer tiempo casi perfecto, ya que la única cuestión negativa fue la expulsión de Piovi, lo que significó que debiera redoblar esfuerzos. Pero lo hizo y por ese motivo terminó justificando el resultado, en base al orden y la eficacia, atributos made in Colón con Eduardo Domínguez en el banco.

Para el inicio del segundo tiempo, Marcelo Gallardo mandó a la cancha a Gonzalo Montiel, Matias Suárez y Federico Girotti y esas variantes mejoraron al equipo. A los 4' pudo descontar el Millonario con un remate de Girotti que tapó Chicco con el pie.

Pero a los 6' de contra pudo aumentar el marcador el Sabalero, la encabezó Bernardi quien cedió a Aliendro y el volante remató pero su disparo fue interceptado por Pinola enviando la pelota al córner. De haber estado más preciso, Colón podría haber marcado el tercer gol.

A los 16' pudo descontar River con un cabezazo de Maidana, pero de manera espectacular Facundo Garcés se interpuso con su cabeza y envió el balón al córner, cuando Chicco nada podía hacer. A esa altura el partido se jugaba exclusivamente en campo sabalero.

A los 28' nuevamente Chicco salvó a Colón con una enorme tapada, ante un remate de Zuculini enviando el balón al tiro de esquina. Y a los 30' Brian Romero saltó solo dentro del área pero su cabezazo se fue cerca del palo. A esa altura, el triunfo de Colón por dos goles era difícil de explicar.

Chicco, la gran figura de Colón tuvo una brillante atajada ante un remate de Montiel que se desvió en Lértora y en una reacción espectacular levantó el balón por encima del horizontal. En tiempo de descuento, llegó el gol de River con un cabezazo de Suárez para establecer el 2-1.

Y ya en el final estuvo cerca de empatarlo River con un media de vuelta de Girotti que se fue al tiro de esquina y luego con un remate cruzado de Romero que se fue cerca. Colón sufrió y mucho pero se terminó quedando con un triunfo enorme que se sustentó en la eficacia y el trabajo defensivo con Chicco como su principal figura.

**Síntesis**

River: 1-Franco Armani; 4-Jonathan Maidana, 2-Robert Rojas, 22-Javier Pinola; 16-Alex Vigo, 5-Bruno Zucculini, 13-Enzo Fernández, 26-José Paradela, 3-Fabrizio Angileri; 10-Jorge Carrascal y 27-Agustín Fontana. DT: Marcelo Gallardo.

Colón: 17-Ignacio Chicco; 4-Facundo Mura, 33-Facundo Garcés, 3-Gonzalo Piovi, 24-Nahuel Gallardo; 14-Federico Lértora, 8-Yeiler Góez; 23-Cristian Bernardi, 29-Rodrigo Aliendro, 11-Alexis Castro; y 35-Facundo Farías. DT: Eduardo Domínguez.

Goles: PT 2' Rodrigo Aliendro (C), 33' Yeiler Góez (C), ST 46' Matías Suárez (R),

Cambios: PT 17' Bruno Bianchi x Castro (C), 29' Brian Romero x Rojas (R), ST 0' Federico Girotti x Fontana (R), Gonzalo Montiel x Vigo (R), Matías Suárez x Paradela (R), 24' Nicolás De La Cruz x Fernández (R), 35' Mauro Formica x Farías (C), 40' Tomás Moschión x Aliendro (C).

Amonestados: Farías (C), Rojas, Vigo y Carrascal (R).

Expulsados: Piovi (C).

Árbitro: Darío Herrera.

Estadio: Vespucio Liberti.