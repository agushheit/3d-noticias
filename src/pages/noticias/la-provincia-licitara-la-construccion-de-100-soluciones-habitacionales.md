---
category: Estado Real
date: 2021-01-07T10:14:03Z
thumbnail: https://assets.3dnoticias.com.ar/070121-licitacion-provincia.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: La provincia licitará la construcción de 100 soluciones habitacionales
title: La provincia licitará la construcción de 100 soluciones habitacionales
entradilla: Se trata de construcciones sólidas, de 40 metros cuadrados, destinadas
  a resolver situaciones de emergencia social que exijan una respuesta inmediata.

---
El Gobierno de la Provincia, a través del Ministerio de Infraestructura, Servicios Públicos y Hábitat, realizará dos licitaciones para la provisión y montaje de paneles prefabricados, para la construcción de cien soluciones habitacionales –cincuenta en Santa Fe e igual cantidad en Rosario– para atender situaciones que se consideren de emergencia y /o alto grado de vulnerabilidad social.

Al respecto, el secretario de Hábitat, Urbanismo y Vivienda, Amado Zorzón explicó: «se trata de soluciones habitacionales de rápida construcción y entrega, que podrán dar respuestas urgentes a situaciones complejas como reubicaciones de familias por riesgo de inundaciones, incendios, trazado de calles o caminos y / o atención de casos de personas con alto grado de vulnerabilidad social. Cada caso se analiza para concretar la solución habitacional».

Y agregó: «estas son dos licitaciones, cuyos montos superan los 170 millones de pesos, y tienen que ver con la firme decisión política del gobernador Omar Perotti y la ministra Silvina Frana, de estar preparados para poder brindar soluciones concretas a situaciones de emergencia que requieran una solución habitacional».

La apertura de sobres se efectuará en dos actos a celebrarse el 18 y el 21 de enero del corriente, en Santa Fe y Rosario, respectivamente.

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.1em; font-weight: 700">LAS LICITACIONES</span>**

Se realizarán dos actos licitatorios, uno en Santa Fe y otro en Rosario, para la provisión y montaje de paneles prefabricados para la construcción de cien soluciones habitacionales, prototipos de 40 metros cuadrados, con un dormitorio, cocina y baño.

El 18 de enero se abrirán los sobres en Santa Fe, a las 10 horas, en la sala de reuniones del Ministerio de infraestructura, Servicios Públicos y Hábitat, sito en calle 3 de febrero 2649, planta baja (oficina 8). Mientras que el 21 de enero, a las 11 horas, será en la sede del Gobierno de la Provincia de Santa Fe en la ciudad de Rosario, sita en calle Santa Fe 2950.