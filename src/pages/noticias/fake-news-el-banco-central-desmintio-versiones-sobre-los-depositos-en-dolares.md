---
category: Agenda Ciudadana
date: 2021-11-30T06:15:02-03:00
thumbnail: https://assets.3dnoticias.com.ar/BANCOCENTRAL.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: '"Fake news": el Banco Central desmintió versiones sobre los depósitos en
  dólares'
title: '"Fake news": el Banco Central desmintió versiones sobre los depósitos en dólares'
entradilla: 'En las últimas horas, circuló un mensaje de WhatsApp que instaba a retirar
  depósitos en dólares, basado en información falsa sobre una medida que tomó el BCRA
  sobre la tenencia de moneda extranjera de los bancos.

'

---
Una noticia falsa sobre un eventual “corralito encubierto” comenzó a circular este fin de semana en las redes sociales, solventada en la comunicación A 7405 del Banco Central de la República (BCRA) que dispuso que los bancos deberán reducir del 4 al 0% la Posición Global Neta (PGN) de moneda extranjera. Ante esta situación, la autoridad monetaria salió a desmentir esa posibilidad y precisó que “las decisiones que tomó la semana pasada referidas a la posición de cambio de las entidades financieras no tiene ningún efecto sobre los depósitos en dólares en el sistema ni con los activos que los respaldan”.

A punto de cumplirse 20 años de la crisis de 2001 que derivó en un corralito financiero, la fake news generó desconcierto entre los ahorristas que poseen ahorros en dólares en el sistema bancario argentino.

La Comunicación A 7405 estableció que los bancos deberán reducir del 4 al 0% la Posición Global Neta (PGN) de moneda extranjera, una medida que se tomó con el objetivo de blindar las reservas.

De ese modo, desde el próximo miércoles próximo las entidades deberán adecuar su posición cambiaria, lo que permitiría una recompra importante de dólares por parte del Central, que rondaría los u$s 600 millones.

Esta disposición no afecta en nada a los ahorristas, y había sido implementada en 2018 durante el gobierno de Mauricio Macri mientras el presidente del Banco Central era Federico Sturzenegger, mediante la comunicación "A" 6.501 que estableció ese instrumento.

![El Comunicado del Banco Central](https://media.unosantafe.com.ar/p/a01ed09a45deeea52d9f3e2486b02aa1/adjuntos/204/imagenes/030/764/0030764126/642x0/smart/banco-centraljpg.jpg =642x0)

**El Comunicado del Banco Central**

La Comunicación del BCRA está vinculada exclusivamente a la Posición Global Neta (PGN) en moneda extranjera que se mide contra la responsabilidad patrimonial computable. "Los bancos deben tener una posición de cambio neutra, justamente los depósitos al ser un pasivo para las entidades deben contar con respaldo en inversiones en esa moneda", de modo que “todos los depósitos en moneda extranjera cuentan con activos en la misma moneda que los respaldan”, indicó el BCRA en un comunicado.

"Adicionalmente existe una normativa específica con más de 20 años de vigencia que exige particularmente que los depósitos en dólares estén respaldados con activos en dólares", agregó el comunicado del BCRA.

"Las entidades financieras, además, cuentan con una liquidez récord en dólares y en pesos", concluyó la misiva del Central.

Cuando hay sobreoferta de dólares, el BCRA eleva la posibilidad de tenencia de los bancos para que absorban dólares y eviten una apreciación del peso y cuando faltan hace que los bancos bajen esa tenencia para que se desprendan de dólares.

Puntualmente en este caso, las entidades tenían hasta noviembre la posibilidad de comprar 1.000 millones de dólares.

A principios de este mes, algunos habían alcanzado un nivel máximo y otros no y, debido a eso, se decidió en esta oportunidad equilibrar el sistema y que todos los bancos tengan la misma posición. Es decir, el 0 representa que pueden tener tantos dólares como deuda en dólares tengan los bancos.

Por esa situación y ante un mensaje que circuló por redes sociales hablando del "corralito encubierto” e instando a los ahorristas a retirar los depósitos en dólares, el BCRA desmintió esa posibilidad, e incluso la falsa información fue considerada una "fake" por economistas de Juntos por el Cambio, como Martín Tetaz, quien en sus redes sociales así la calificó.

**El falso mensaje**

El falso mensaje decía textualmente: “Recibido hace unos minutos, lo comparto: Es de suma importancia comunicarles que se oficializó, mediante el Comunicado “A7405 y A7407″, ambos del Banco Central de la República Argentina (BCRA) que, con vigencia a partir del día 01/12/2021 (miércoles). Los bancos y/o entidades financieras enmarcadas dentro del circuito financiero argentino, no podrán tener en sus reservas divisas extranjeras. Toda posición financiera o equivalente en dólares, deberá ser liquidada y remitida a las reservas del BCRA. En otras palabras, un corralito encubierto. Por lo que le recomendamos a aquellos que posean dólares en cuentas bancarias, que el día lunes y martes de esta semana entrante, saquen turno y/o se apersonen a su banco para retirar esos saldos”.