---
category: Agenda Ciudadana
date: 2022-06-28T08:02:04-03:00
thumbnail: https://assets.3dnoticias.com.ar/bacheo8.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: Trabajos de bacheo, iluminación y arbolado previstos para este martes
title: Trabajos de bacheo, iluminación y arbolado previstos para este martes
entradilla: La Municipalidad informa el avance del cronograma de tareas previstas
  en los planes de bacheo y Santa Fe se Ilumina. Además, se suman los trabajos programados
  en el arbolado público.

---
La Municipalidad avanza con el plan de bacheo diseñado para reparar más de 12.000 metros cuadrados de calles de la capital provincial. El programa se desarrolla por fases, priorizando las zonas de la ciudad que evidencian mayor circulación.

Actualmente se trabaja en:

* Santiago del Estero, entre Rivadavia y San Luis
* Santiago del Estero, entre San Luis y Belgrano
* Belgrano, entre Santiago del Estero e Ituzaingó
* Belgrano e Ituzaingó
* Avenida Peñaloza, desde Zeballos hasta Gorriti, en sentido sur-norte
* Lavalle y Córdoba
* Mitre, entre Córdoba e Iturraspe

Cabe recordar que, en caso de lluvia, se suspenden las tareas.

**Iluminación**

En el marco del plan Santa Fe se Ilumina, el municipio se aboca a recuperar el alumbrado existente en toda la ciudad. Cuadrillas municipales trabajan en la colocación de nuevas columnas de iluminación y artefactos led en distintos barrios y avenidas, como así también, en el mantenimiento de la red actual de la ciudad.

En ese sentido, se realizará el mantenimiento del sistema, entre otros puntos, en:

* Circunvalación Oeste
* Plaza Alberdi
* Plazoleta 3 de Junio
* Zona del ISEF
* Fuentes de la ciudad
* Distrito de la Costa

**Trabajos en el arbolado público**

La Municipalidad recuerda que los trabajos de poda del arbolado público sólo se concretan a los efectos de despejar cámaras de seguridad y facilitar la visualización, o ante la colocación de nuevas columnas de alumbrado. Del mismo modo, esta tarea recibe la denominación de poda sanitaria cuando los ejemplares poseen ramas secas y/o colgantes que representan un peligro para los peatones o el tránsito.

Mañana se realizarán labores de este tipo en la vecinal General Alvear y en barrio Candioti Sur.