---
category: Estado Real
date: 2021-01-06T10:34:33Z
thumbnail: https://assets.3dnoticias.com.ar/060121-repavimentacion.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Avanza la repavimentación de la Ruta Provincial Nº 22
title: Avanza la repavimentación de la Ruta Provincial Nº 22
entradilla: Es en el tramo Huanqueros-Esteban Rams. Los trabajos comenzaron en agosto
  y muestran un avance del 30%

---
El Ministerio de Infraestructura, Servicios Públicos y Hábitat, a través de la Dirección Provincial de Vialidad, continúa realizando **la reconstrucción de la calzada sobre la Ruta Provincial Nº 2, en unos 19 kilómetros** que se extienden desde la localidad de Huanqueros (departamento San Cristóbal), hacia el norte en jurisdicción de Esteban Rams, ingresando al departamento 9 de Julio.

Desde la inspección de la obra corroboraron que durante el mes de diciembre se finalizó con la conformación de la sub base inferior, de 20 centímetros de espesor en la totalidad del trayecto, que tiene su comienzo en el kilómetro 241 de la traza y finaliza en el 260. Desde la última semana, se viene trabajando con el agregado de piedra para la sub base superior en los primeros 9000 metros de la intervención.

Al respecto, el administrador de Vialidad Provincial, Oscar Ceschi, explicó: «sabemos de los pedidos de la gente sobre esta ruta, por la seguridad y la importancia que tiene este corredor vial productivo. **Tiene un plazo de ejecución de 10 meses y vamos a un ritmo de trabajo muy bueno**, respetando todas las instancias. La ruta 2 es parte del sistema productivo de la provincia y es el acceso de muchas localidades del norte a corredores importantes como la Ruta Nacional Nº 11».

De acuerdo a la planificación, se seguirá con la etapa de reciclado de sub base superior para luego avanzar con el riego de emulsión asfáltica, concretando así la impresión de la calzada. Dentro de las obras complementarias, se realizarán tareas de reconstrucción de defensas metálicas, alcantarillas y señalizaciones.