---
category: Estado Real
date: 2021-12-03T06:00:01-03:00
thumbnail: https://assets.3dnoticias.com.ar/perottiinseguro2.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Seguridad: Perotti s reunió con referentes d sectores empresariales, gremiales,
  sociales, académicos y religiosos.'
title: 'Seguridad: Perotti s reunió con referentes d sectores empresariales, gremiales,
  sociales, académicos y religiosos.'
entradilla: "“El objetivo es escuchar, recabar críticas, información, y contarles
  cuáles son nuestros proyectos en el corto y mediano plazo”, explicó el ministro
  Corach."

---
El gobernador Omar Perotti y los ministros de Gestión Pública, Marcos Corach, y de Seguridad, Jorge Lagna, se reunieron este jueves en Rosario con miembros del Consejo Económico y Social, y de organizaciones sociales y de trabajadores, con el objetivo de recepcionar iniciativas y propuestas para el abordaje de la seguridad ciudadana, y enumerar acciones de gobierno.

“El objetivo fue escuchar, recabar críticas, información, contarles cuáles son nuestros proyectos en el corto y mediano plazo en materia de seguridad”, explicó Corach, y destacó “la predisposición de todos los sectores”.

En ese sentido, valoró que “nadie se está manteniendo al margen, todos entienden que no es un problema del gobernador o del gobierno exclusivamente. Todos adentro para aportar, porque entienden que tienen una responsabilidad para que todos los santafesinos y santafesinas podamos vivir un poco mejor”.

En tanto, Lagna indicó que se trató de un encuentro “muy positivo”, donde se escuchó “a la sociedad civil y surgieron propuestas importantes para continuar trabajando, en tanto explicamos que entendemos a la seguridad como un trabajo multiagencial que excede a lo policial y tiene múltiples causas”.

“Coincidimos en los diagnósticos, en la forma de abordarlo, y en la necesidad de que, entre todos, como se está saliendo de la pandemia, y que fue una muestra de trabajo conjunto entre el gobierno y la sociedad civil, suceda lo mismo con la seguridad”, amplió el titular de esa cartera.

**APORTE DE LA SOCIEDAD CIVIL**  
Por su parte, presidente de la Bolsa de Comercio de Rosario, Miguel Simioni destacó que “fue un encuentro que agradecemos; nos parece muy importante la convocatoria ante la situación que se está viviendo, no solo en Rosario sino en la provincia. Y nos pusimos a disposición para lo que el gobernador crea conveniente, porque estamos convencidos que es el momento de llamar a toda la comunidad, al sector privado y público, a estar juntos, porque la situación es muy complicada”.

A su turno, Mónica Alesso, de la Universidad Abierta Interamericana, ponderó que “se visualiza una buena oportunidad para colaborar y acompañar en este cambio que necesita nuestra ciudad. Nos parece sumamente importante que las instituciones privadas participen de estas decisiones. Para nosotros es fundamental acompañar y ser parte de este grupo que va a colaborar”.

En tanto, el sacerdote Fabián Belay valoró el encuentro “porque genera esperanza, y el deseo de toda la sociedad de sumar fuerzas para salir adelante. Es un tema muy complejo, pero nuestra sociedad se merece que hagamos el esfuerzo para que niños y adolescentes tengan un futuro mejor”.

En los encuentros, llevados a cabo en el Salón Blanco de la Sede local de Gobierno, también estuvieron presentes el ministro de Desarrollo Social, Danilo Capitani; el secretario de Justicia, Gabriel Somaglia; la secretaria de Asuntos Estrategicos, Mariana Foglia; y la jefa de la Policía de Santa Fe, Emilce Chimenti, entre otros.