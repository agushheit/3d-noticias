---
category: Agenda Ciudadana
date: 2021-03-22T06:15:42-03:00
thumbnail: https://assets.3dnoticias.com.ar/electricidad.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Piden sumar al norte de Santa Fe a las tarifas eléctricas diferenciadas
title: Piden sumar al norte de Santa Fe a las tarifas eléctricas diferenciadas
entradilla: La iniciativa será presentada por el senador nacional por la provincia
  de Santa Fe Roberto Mirabella

---
El senador por Santa Fe Roberto Mirabella presentará un proyecto para que se incorpore a los seis departamentos del norte de Santa Fe, al esquema de tarifas eléctricas diferenciadas para el Noroeste (NOA) y Noreste (NEA).

La iniciativa será presentada por Mirabella, en el marco de las conversaciones que llevan adelante gobernadores de las diez provincias norteñas y el Poder Ejecutivo Nacional, para establecer una tarifa eléctrica diferencial en esos distritos.

En las últimas semanas Chaco, Formosa, Corrientes, Misiones, Jujuy, Salta, Tucumán, Catamarca, La Rioja y Santiago del Estero avanzaron en conversaciones con el Gobierno Nacional y equipos técnicos del Ministerio de Economía para establecer una tarifa eléctrica diferenciada.

Esa tarifa, "está basada en estudios climáticos y expresa una situación de igualdad con otros esquemas similares como el que tiene la Patagonia en época invernal", señaló el senador Mirabella a Télam.

En este marco, a través de una iniciativa que presentará en la cámara alta, el senador peronista, solicitará que se incluya a los departamentos 9 de Julio, Vera, General Obligado, San Cristóbal, San Justo y San Javier, de Santa Fe en este esquema.

Al respecto, Mirabella indicó que "el norte de nuestra provincia tiene mayores similitudes socioeconómicas, ambientales, climáticas, geográficas y estructurales con el norte argentino que con el sur de Santa Fe”, y que por ese motivo "es pertinente y necesario" incluir a los departamentos mencionados en su iniciativa, porque según dijo Mirabella "son una unidad geográfica del territorio y las localidades mencionadas son electrodependientes".

"Esta es una realidad que debe contemplarse si se quiere cumplir con el criterio de equidad en la determinación del precio de energía", destacó por último el legislador.