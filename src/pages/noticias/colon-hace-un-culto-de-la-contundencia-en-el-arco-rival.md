---
category: Deportes
date: 2020-12-06T12:17:45Z
thumbnail: https://assets.3dnoticias.com.ar/colon.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Diario UNO'
resumen: Colón hace un culto de la contundencia en el arco rival
title: Colón hace un culto de la contundencia en el arco rival
entradilla: La mayor virtud de Colón es su eficacia. En seis partidos remató 28 tiros
  al arco y anotó 11 goles, es decir que anotó un tanto cada dos llegadas y media.

---
El poder de fuego que tiene Colón es indiscutible, al equipo de Eduardo Domínguez se le caen los goles de los bolsillos. Marcó 11 tantos en seis partidos, pero lo más llamativo es su eficacia. Precisó de 28 remates al arco para anotar 11 goles, es decir un gol cada dos remates y medio al arco rival.

Esa virtud le permitió clasificar como primero en su zona, con 13 puntos y una diferencia de gol de +8 como consecuencia de los 11 tantos a favor y apenas tres en contra.

El Sabalero no necesita tener el balón ni tampoco llegar demasiado, para ser contundente. Y además marcó goles en todos los partidos.

Le basta con escasas acciones para anotarse en la red y luego manejar el juego. De hecho, en cinco de los seis partidos hizo el primer gol y casualmente la única derrota fue contra Independiente cuando el rival anotó primero. 

Con el resultado a su favor Colón despliega con mayor eficiencia su plan de juego.

Es un equipo que está más acostumbrado a esperar que a proponer y por ello se siente más cómodo cuando tiene la chance de contragolpear. De allí que es imprescindible que se ponga arriba en el marcador para luego aprovechar la desesperación de su adversario.

Por la característica de sus jugadores, Colón está pensando para tener poco el balón. De hecho, hay una particularidad que describe de manera concreta esta situación. En el único partido que el Sabalero tuvo más la pelota que su rival terminó perdiendo.

Contra Independiente en el Brigadier López la tenencia fue del 57% contra 43% del Rojo, sin embargo, apenas marcó un gol en tiempo de descuento y le anotaron por única vez en el torneo, dos tantos. De poco le sirvió manejar más la pelota que su oponente.

Mientras que, en los cinco partidos restantes, prescindió del balón, en algunos de manera muy llamativa, como los encuentros ante Defensa y Justicia que fueron un calco. Colón la tuvo el 30% y el Halcón de Varela el 70%, pero el global en los dos encuentros fue para el Rojinegro por 5-0. Remató nueve tiros al arco y anotó cinco goles.

Los dos cotejos con Central Córdoba fueron muy similares, en el Brigadier López el Ferroviario tuvo una tenencia del 53% contra 47% de Colón y en Santiago del Estero fue de 58% contra 42. Pero en los 180' el Sabalero terminó ganando 4-0. De esta manera, pateó 10 veces al arco rival y marcó cuatro tantos.

Y contra Independiente es donde menor eficacia tuvo y los resultados lo demuestran, ya que empató uno y perdió el restante. En Avellaneda remató cuatro tiros al arco y anotó un gol. Mientras que en el Brigadier López fueron cinco y también un gol. En consecuencia, en nueve intentos, apenas marcó dos.