---
category: Estado Real
date: 2021-09-06T06:00:51-03:00
thumbnail: https://assets.3dnoticias.com.ar/RUTAS.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La provincia licitará obras para rutas por un monto superior a los 375 millones
  de pesos
title: La provincia licitará obras para rutas por un monto superior a los 375 millones
  de pesos
entradilla: Se trata de obras de señalización horizontal y vertical que buscarán mejorar
  la seguridad de las rutas provinciales.

---
El Ministerio de Infraestructura, Servicios Públicos y Hábitat por intermedio de la Dirección Provincial de Vialidad, realizará este lunes 6, la apertura de sobres para concretar la ejecución de obras de señalización horizontal y vertical en las rutas de la zonas norte y sur del territorio.

Las obras, estarán distribuidas utilizando la Ruta Nacional N°19 como límite, estableciendo para cada una de ellas un presupuesto oficial de $187.813.681,50 y un plazo de ejecución de 12 meses. Desde allí, se extenderán hasta la provincia del Chaco y Buenos Aires, respectivamente.

Cabe destacar, que la traza seleccionada, es un corredor transversal que en la provincia se desarrolla entre la Ruta Nacional N°11 (jurisdicción de Santo Tomé) y el acceso a la localidad de Frontera, en el límite con la provincia de Córdoba.

Al respecto, el administrador general de la Dirección Provincial de Vialidad, Oscar Ceschi, destacó que "los trabajos son fundamentales para el mantenimiento de cada ruta de nuestro territorio. Así como nos enorgullece cada camino que se pavimenta, nos genera la misma sensación tomar la decisión de invertir para mejorar la infraestructura que ya posee la provincia". Además, agregó que "desde el gobierno de Omar Perotti y la gestión en infraestructura de la ministra Silvina Frana se demostró esta determinación de generar mayores condiciones de seguridad para los usuarios, es por eso que también licitamos obras nuevas en puentes, ensanches, reconstrucciones, reparaciones, alcantarillas o bacheos en distintos lugares de la provincia".

**Licitaciones**

Los actos de apertura de sobres con ofertas, se realizarán este lunes 6, a las 10 y 10:30 horas, en la Dirección Provincial de Vialidad, Boulevard Muttis 880, Santa Fe.

**Las obras**

Con la necesidad de mejorar la circulación sobre las rutas provinciales, se dotará de elementos de seguridad, prevención, reglamentación y orientación para los conductores, alentando que perciba con anticipación las maniobras a realizar.

En las intervenciones, no se darán tramos concretos de rutas donde se deberá actuar, sino que a través de las inspecciones de obra se indicarán los trazados. En los tramos, entre otras tareas, se demarcará el eje central con pintura termoplástica reflectante; se ejecutarán bandas ópticas sonoras; grafología sobre pavimento en escuelas, cruces ferroviarios, intersecciones y pasos peatonales; además de tachas reflectivas en zonas de bifurcaciones o previo a canalizaciones. En tanto a señalización vertical, se instalarán nuevas piezas o se reemplazarán las existentes que pudieran estar fuera de las reglamentaciones vigentes.