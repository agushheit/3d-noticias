---
category: Deportes
date: 2021-05-16T06:00:07-03:00
thumbnail: https://assets.3dnoticias.com.ar/Basquet.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: " Santa Fe Capital vuelve ser la vidriera del básquet"
title: " Santa Fe Capital vuelve ser la vidriera del básquet"
entradilla: La ciudad se convirtió nuevamente en escenario para la Conferencia Norte
  de la Liga Argentina de Básquetbol. La nueva fase comenzó este viernes en el Gimnasio
  Roque Otrino, del Club Atlético Colón.

---
Desde este viernes, la capital de la provincia vuelve a ser mini sede de la Liga Argentina de bàsquetbol, segunda categoría del baloncesto nacional. Este tramo del certamen se desarrolla en el gimnasio Roque Otrino del Club Atlético Colón, con la participación del representativo anfitrión, el Club Atlético Unión de Santa Fe, Sportivo América de Rosario, Atlético Echagüe Club de Paraná, Salta Basket, Estudiantes de Tucumán y el Club Atlético Riachuelo de La Rioja.

Cabe recordar que la ciudad fue sede del partido inaugural de la primera burbuja nacional, con la disputa del clásico entre Unión y Colón, en el estadio mundialista “Ángel P. Malvicino”.

El director de Deportes de la Municipalidad, Adrián Alurralde, ratificó la “decisión de acompañar a los equipos de la capital” y destacó el “esfuerzo enorme” realizado por los clubes para poner en condiciones sus estadios. “Destacamos el trabajo en conjunto entre las dos instituciones con el mismo objetivo de engrandecer el deporte”, aseguró.

Por su parte, el director de Turismo municipal, Franco Arone, lamentó que no se puedan recibir turistas para el evento, pero mencionó que “las delegaciones arribadas a la capital de la provincia podrán visitar algunos lugares y recorrerla. La ciudad está preparada y altamente calificada a nivel nacional e internacional para ratificarnos como epicentro de eventos importantes y de jerarquía”, indicó. Felicito a las dos instituciones por el esfuerzo que realizan”.

**Fixture**

Los partidos iniciaron el viernes, con Echagüe y Riachuelo; Sportivo América y Salta Basket, y Unión vs. Estudiantes de Tucumán. Este sábado será el turno a las 11 horas, de Riachuelo y Sportivo América; a las 15:30 se medirán Estudiantes de Tucumán y Echagüe; el cierre será a las 18 horas con Colón enfrentando a Salta Basket.

Tras el descanso del domingo, el lunes se reanudará la serie: a las 11, Sportivo América con Estudiantes de Tucumán; a las 15.30, Unión con Salta Basket y a las 18 horas, Colón con Riachuelo. La jornada final del martes comenzará a las 11 horas con Salta Basket y Echagüe; seguirá a las 15:30 con Unión vs. Riachuelo; y concluirá a las 18 horas con Colón frente a Estudiantes de Tucumán.