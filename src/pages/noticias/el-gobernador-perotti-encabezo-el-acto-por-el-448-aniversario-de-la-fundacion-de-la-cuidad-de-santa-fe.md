---
category: Agenda Ciudadana
date: 2021-11-16T06:00:58-03:00
thumbnail: https://assets.3dnoticias.com.ar/cayasta.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: El gobernador Perotti encabezó el acto por el 448° aniversario de la fundación
  de la cuidad de Santa Fe
title: El gobernador Perotti encabezó el acto por el 448° aniversario de la fundación
  de la cuidad de Santa Fe
entradilla: "“Queremos una provincia que abra sus puertas a quien trabaja, invierte
  y produce; cada puerta que abrimos desde allí es una puerta que se le cierra al
  delito”, dijo el gobernador en Cayastá. "

---
El gobernador Omar Perotti presidió este lunes, en Cayastá, el acto por el 448° Aniversario de la Fundación de la Ciudad de Santa Fe. La ceremonia se realizó en el Pórtico de Ingreso del Parque Arqueológico.

En su discurso, el gobernador santafesino sostuvo que “es una alegría poder compartir este momento”, y agradeció “a cada una de las instituciones que mantuvieron vivo, vigente y renovado el pensamiento y el rescate de la historia desde la fundación hasta aquí, y se empeñaron en difundir y resguardar la tarea de Zapata Gollán”.

Más adelante, Perotti expresó que “tengo que agradecer a todos haber sido parte de los santafesinos y santafesinas que voluntariamente se inscribieron para recibir las vacunas. Si pudimos desplegar la mayor campaña de vacunación, si pudimos colocar esta enorme cantidad de vacunas hasta aquí, que nos va permitiendo salir, es porque cada santafesino y santafesina puso su brazo”, recordó el gobernador.

Y agregó: “Eso nos da una de las provincias con mayor porcentaje de voluntarios para hacerlo. Y en esta salida que vamos viendo, tenemos en la vacuna y en los cuidados, el camino que nos ha llevado hasta aquí, que no quede nadie sin vacunarse, están las vacunas para todos, desde los tres años en adelante, y ya comenzamos con las terceras dosis, que nos va a posibilitar que volvamos a poder disfrutar esas cosas que añoramos”, sostuvo.

En otro tramo del discurso, Perotti felicitó “a todos los que participaron en la elección de ayer, a los que han sido elegidos por el voto popular, el deseo de éxitos de cada uno, porque sumará al éxito de la provincia, y el deseo de que podamos trabajar juntos y unidos. La provincia lo necesita y este camino de recuperación que se va dando, se va a solidificar mucho mejor si estamos todos codo a codo trabajando por nuestra querida provincia”, expresó el gobernador.

Sobre el hecho histórico, el mandatario señaló que “Santa Fe hoy tiene todos sus desafíos por delante en su ubicación actual y Cayastá las tiene aquí, y cada uno va moldeando desde estos nuevos lugares, estas nuevas realidades en su futuro, que uno ve como muy promisorio. Cayastá consolidándose con peso propio, con desarrollo, y con identidad propia; nuestra Capital generando acciones que la van perfilando como una ciudad que reconoce la historia común y le agrega todos los días una página nueva. Es valiosísima la tarea de quienes posibilitaron el rescate de nuestra historia, porque consiguieron transmitir lo que aquí sucedió y rescatar cada uno de los capítulos de esa historia, quienes aquí llegaron entrelazando vidas y generando historias, siendo la raíz de lo que tenemos”, añadió Perotti.

Por último, indicó que “el desafío de cada uno de nosotros es hacer una provincia mejor, tratar que nuestras obras permitan subir un escalón. Queremos una provincia que abra sus puertas a quien trabaja, invierte y produce; cada puerta que abrimos desde allí, es una puerta que se le cierra al delito. Queremos generar más oportunidades para trabajar en la educación de nuestra gente, vivan donde vivan tienen que tener las mismas oportunidades y a partir de allí tendremos una provincia que todos los días mejore y eleve la calidad de vida de su gente”, finalizó el gobernador de la provincia.

A su turno, la presidenta comunal de Cayastá, María Verónica Devia, hizo una breve reseña histórica desde la primera fundación de Santa Fe hasta el descubrimiento de sus ruinas y de los antiguos pobladores que habitaban esa zona. “Somos el resultado de esos sucesos, que fueron forjando nuestra identidad. Seguimos fortaleciendo los vínculos, adaptándonos para seguir viviendo y conviviendo en nuestra querida provincia”, sostuvo Devia, quien además aprovechó para agradecer al gobernador las gestiones para la puesta en valor del comedor de la costa y los recientes trabajos de repavimentación sobre la ruta provincial 1.

Por su parte, el senador provincial por el departamento Garay, Ricardo Kaufmann, rindió un homenaje a Agustín Zapata Gollán, quien en 1949 inició las excavaciones de las ruinas y la recuperación de Santa Fe La Vieja. “A él se le debe el primer impulso para rescatar del olvido a ese pasado que nacía aquí. Se le debe esa inspiración y esa paciencia para plasmar en su obra el descubrimiento, un legado que supo construir muchas veces desde la soledad”, sostuvo el legislador.

**Danzas folklóricas y recorrido virtual**

En el desarrollo de las actividades, en primer lugar, se procedió a la lectura de la ley N° 13.155, que establece la sede del gobierno provincial en “Santa Fe la Vieja”. Posteriormente se izó la Bandera Nacional, se entonó el himno, se leyó el decreto de honor y se realizó una ofrenda floral a la figura de Juan de Garay y del Dr. Agustín Zapata Gollán, historiador, periodista, escritor y arqueólogo, director del Departamento de Estudios Etnográficos y Coloniales, que inició las excavaciones que pusieron a la luz los vestigios de “Santa Fe la Vieja”.

Cabe destacar que el 25 de marzo de 1957, las ruinas de Cayastá, adonde fueron encontradas las ruinas, fueron declaradas "Monumento Histórico Nacional". Desde 1973 funciona allí el Museo del Descubrimiento y Población del Río de la Plata, que depende del Museo Etnográfico de Santa Fe.

Luego de los discursos de las autoridades, se llevó a cabo un espectáculo de danzas a cargo del Ballet del Taller de Folclore comunal. Las actividades continuaron en el auditorio del Museo de Sitio, donde se procedió a la presentación, por parte de la narradora Marta Coutaz, de una síntesis de “El río de las Congojas”, cuya autora es Libertad Demitrópulos. El tema relata la historia de “Santa Fe la Vieja” hasta la mudanza desde la óptica de personas del pueblo, amores y mitos.

Luego se realizó la presentación del Recorrido Virtual del Parque Arqueológico, en el marco del Programa Cultura 360. Además, se entregó un subsidio a la Asociación Amigos de Santa Fe La Vieja, para iniciar la adquisición de equipamiento y puesta en marcha del nuevo Guión Museológico del lugar.

Además del gobernador Omar Perotti, también estuvieron presentes en el acto los ministros de Gestión Pública, Marcos Corach; de Igualdad Género y Diversidad, Celia Arena; y de Cultura, Jorge Llonch; el presidente de la Cámara de Diputados, Pablo Farías; el intendente de Santa Fe, Emilio Jatón; el secretario de Gestión Cultural, Jorge Pavarín; el presidente de la Junta Provincial de Estudios Históricos, Alejandro Damianovich; la secretaria, Ana María Cecchini de Dallo; representantes de la Asociación Santa Fe La Vieja; el coordinador del Museo Etnográfico, Gabriel Cocco; instituciones y fuerzas vivas de la región; docentes, abanderados de escuelas y agrupaciones tradicionalistas.