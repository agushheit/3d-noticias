---
category: Agenda Ciudadana
date: 2021-01-06T10:31:07Z
thumbnail: https://assets.3dnoticias.com.ar/060121-restricciones-covid.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Télam'
resumen: Ya son siete las provincias que dispusieron restricciones y medidas ante
  el aumento de casos
title: Ya son siete las provincias que dispusieron restricciones y medidas ante el
  aumento de casos
entradilla: Chaco, Formosa, La Pampa y municipios de Buenos Aires y Santa Fe restringen
  la circulación. Santiago del Estero exige hisopados para ingresar al distrito y
  la ciudad correntina de Mercedes limita el horario de ingreso.

---
Chaco, Formosa, La Pampa y municipios de Buenos Aires y Santa Fe dispusieron en las últimas horas restricciones a la circulación de personas ante el aumento de contagios de coronavirus, mientras que Santiago del Estero resolvió exigir hisopados para ingresar al distrito y la ciudad correntina de Mercedes decidió limitar el horario de entrada en los accesos.

La decisión de estos gobiernos provinciales responde a la creciente preocupación de las autoridades locales por el aumento de contagios registrado en los últimos días, algo que también es seguido de cerca por el gobierno nacional, el bonaerense y el porteño, ante el aumento de circulación de personas durante los meses de verano.

<br/>

**<span style="font-family: arial, helvetica; font-size: 18px; font-weight: 700; color: #ffffff; background: #000000; margin: 35px; overflow: hidden; padding: 10px; border-radius:5px;">   Chaco  </span>**

<br/>

Este martes, la provincia decretó una «alarma sanitaria» hasta el 21 de enero de 0 a 6 horas, excepto de jueves a domingo, período en que los bares y restaurantes podrán funcionar hasta las 2 horas.

Asimismo, el Decreto Provincial 1/2021 establece que se podrá permanecer en espacios públicos hasta las 22 horas. A partir de entonces, las autoridades podrán adoptar «medidas para evitar la concentración de personas».

También se suspenden los eventos y reuniones sociales, culturales, recreativos, musicales y empresariales en espacios abiertos, cerrados, públicos y privados.

Además, se establecerán puestos de control en los puntos limítrofes de la provincia y durante los sábados y domingos no se permitirá la circulación entre localidades.

<br/>

[![Ver en Twitter](https://assets.3dnoticias.com.ar/tweet-chaco-de-todos0.webp)](https://twitter.com/ChacoDeTodos/status/1346213221330546688/photo/1 "Ver en Twitter")

<br/>

[![Ver en Twitter](https://assets.3dnoticias.com.ar/tweet-chaco-de-todos-1.webp)](https://twitter.com/ChacoDeTodos/status/1346213221330546688/photo/2 "Ver en Twitter")

<br/>

**<span style="font-family: arial, helvetica; font-size: 18px; font-weight: 700; color: #ffffff; background: #000000; margin: 35px; overflow: hidden; padding: 10px; border-radius:5px;">   La Pampa  </span>**

<br/>

Rigen nuevas restricciones hasta el 18 de enero y, según informó el gobierno provincial vía Twitter, entre la 1 y las 6.30 solo podrán circular los trabajadores esenciales.

A su vez, se extiende la suspensión de los encuentros sociales y las actividades deberán cesar a la medianoche, con una tolerancia de media hora para los locales gastronómicos.

<br/>

[![Ver en Twitter](https://assets.3dnoticias.com.ar/tweet-la-pampa.webp)](https://twitter.com/GobiernoLaPampa/status/1346188144056655873/photo/1 "Ver en Twitter")

<br/>

**<span style="font-family: arial, helvetica; font-size: 18px; font-weight: 700; color: #ffffff; background: #000000; margin: 35px; overflow: hidden; padding: 10px; border-radius:5px;">   Formosa  </span>**

<br/>

La provincia informó que, a partir del brote detectado en su ciudad capital, «se dispuso el bloqueo sanitario y el Aislamiento Social Preventivo y Obligatorio» en esa ciudad desde este mediodía hasta el 19 de enero.

El Consejo de Atención Integral a la Emergencia Covid-19 comunicó que «las personas deberán permanecer en sus residencias», con circulación limitada para «comprar alimentos, medicamentos y productos de limpieza» y, además, «quedan suspendidas las actividades de flexibilización dispuestas en la provincia».

<br/>

**<span style="font-family: arial, helvetica; font-size: 18px; font-weight: 700; color: #ffffff; background: #000000; margin: 35px; overflow: hidden; padding: 10px; border-radius:5px;">   Buenos Aires  </span>**

<br/>

El municipio de Rojas dispuso una restricción para circular desde la medianoche hasta las 6 –salvo viernes y sábado, cuando se podrá hacerlo hasta las 2– y multas de hasta 50 mil pesos para quienes organicen fiestas clandestinas.

«La libertad fue la piedra angular para nosotros, ya que implica más riesgos, y, por lo tanto, más responsabilidad individual y colectiva, por eso hay medidas para reforzar la capacidad de respuesta del sistema de salud y otras que tienen que ver con la restricción del horario nocturno», explicó el intendente Claudio Rossi (Juntos por el Cambio) a través de sus redes sociales.

<br/>

**<span style="font-family: arial, helvetica; font-size: 18px; font-weight: 700; color: #ffffff; background: #000000; margin: 35px; overflow: hidden; padding: 10px; border-radius:5px;">   Santa Fe  </span>**

<br/>

También en la ciudad de Tostado las autoridades dispusieron que, por 14 días, los residentes deberán permanecer en sus domicilios y solo desplazarse para abastecerse o por razones de fuerza mayor.

La ciudad tiene poco más de 15 mil habitantes y en los últimos días experimentó un marcado crecimiento de los casos positivos.

<br/>

[![Ver en Twitter](https://assets.3dnoticias.com.ar/tweet-santa-fe.webp)](https://twitter.com/GobSantaFe/status/1346243036762828801 "Ver en Twitter")

<br/>

**<span style="font-family: arial, helvetica; font-size: 18px; font-weight: 700; color: #ffffff; background: #000000; margin: 35px; overflow: hidden; padding: 10px; border-radius:5px;">   Corrientes  </span>**

<br/>

Mercedes cerró el lunes sus accesos por una semana, de 23 a 6, y custodiará el santuario del Gauchito Gil, para evitar aglomeraciones y disminuir la circulación en ocasión de la conmemoración del viernes próximo, en momentos que la localidad atraviesa un rebrote de coronavirus, informó el intendente Diego Caram.

La medida se complementa con un operativo de seguridad más riguroso que se activará en varios puntos de los alrededores a la ciudad y del santuario, entre el martes y el viernes próximo, ya que la cantidad de personas que participa todos los años en la conmemoración del Gaucho Gil se calcula en 300 mil.

<br/>

**<span style="font-family: arial, helvetica; font-size: 18px; font-weight: 700; color: #ffffff; background: #000000; margin: 35px; overflow: hidden; padding: 10px; border-radius:5px;">   Santiago del Estero  </span>**

<br/>

Se dispuso exigir desde el próximo sábado para ingresar a la provincia un test PCR negativo –de hasta 72 horas cómo máximo de realizado– más la declaración jurada o el registro en la aplicación CuidarSE.

También se aclaró que «el convenio de libre circulación entre Salta, Jujuy, Catamarca, La Rioja y Tucumán sigue vigente y, por lo tanto, no se exige hisopado en la región por parte de la provincia».

En el caso de pasajeros por vía área, si llegan sin el hisopado, se les realizará uno en forma gratuita en el aeropuerto; mientras que si un santiagueño necesita ir a una provincia que no participa del acuerdo turístico (como Córdoba, Santa Fe, por ejemplo) podrá hisoparse gratis antes de salir y reingresar al territorio provincial en un plazo no mayor a 72 horas.

«Existe una importante circulación comunitaria del virus y es imprescindible cumplir los protocolos preventivos, sobre todo las personas de grupos de riesgo», sostuvo el gobernador Gerardo Zamora a través de sus redes sociales.

<br/>

[![Ver en Facebook](https://assets.3dnoticias.com.ar/sgo-del-estero-tweet.webp)](https://www.facebook.com/ministeriosaludsantiago/photos/a.377636345764247/1482962021898335 "Ver en Facebook")

***