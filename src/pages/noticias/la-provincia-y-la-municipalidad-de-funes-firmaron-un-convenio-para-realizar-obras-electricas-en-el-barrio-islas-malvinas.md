---
category: Estado Real
date: 2021-01-06T10:25:46Z
thumbnail: https://assets.3dnoticias.com.ar/060121-firma-convenio.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: 'Funes: obras eléctricas en el barrio Islas Malvinas'
title: La provincia y la municipalidad de Funes firmaron un convenio para realizar
  obras eléctricas en el barrio Islas Malvinas
entradilla: Se construirán casi 2 kilómetros de red de media tensión, una subestación
  transformadora urbana y tendidos domiciliarios.

---
El presidente de la Empresa Provincial de la Energía (EPE), Mauricio Caussi y el Intendente de Funes, Roly Santacroce, firmaron este martes un convenio para **dotar de nueva infraestructura eléctrica al barrio Islas Malvinas**. 

La actividad se llevó a cabo en el marco del **plan EPE Social**, programa que puso en marcha esta gestión, donde se priorizan esquemas de trabajo de aportes conjunto de la provincia, la EPE y el municipio.

El titular de la empresa dijo que «este convenio nos permite avanzar en el desarrollo de nuevas redes con cable preensamblado, que brindarán mejor calidad en la prestación del servicio eléctrico y mayor seguridad a los vecinos de Funes. De esta manera, se salda una vieja deuda con un grupo importante de vecinos de Funes, que solicitaban desde hace tiempo la concreción de estas obras», agregó Caussi.

Señaló, además, que los trabajos demandarán una inversión de $ 14.600.000 y consisten en la construcción de casi 2 kilómetros de líneas de media tensión de 33 kV, una subestación transformadora urbana y la provisión de mano de obra y materiales para el tendido de la red eléctrica en el sistema de baja tensión, que comenzarán a la brevedad, con un plazo de ejecución de 6 meses.

Caussi consideró que «acompañar a los municipios y comunas de nuestra provincia es el pedido del gobernador, que quiere una empresa muy cerca de las necesidades de la gente. Estas gestiones terminan beneficiando a un conjunto de familias de la localidad».

Por su parte, Roly Santacroce manifestó que «el municipio reintegrará el importe por los trabajos en 18 cuotas lo relacionado con las obras del sistema de baja tensión, con aportes provenientes de la ley provincial 7797 y se compromete a obtener del Concejo Deliberante los instrumentos necesarios para cumplir con estos objetivos en beneficio de la ciudadanía».