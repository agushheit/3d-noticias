---
category: La Ciudad
date: 2021-07-30T06:15:10-03:00
thumbnail: https://assets.3dnoticias.com.ar/LUCES.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Municipalidad licitó el recambio de la iluminación en las calles de acceso
  a los barrios
title: La Municipalidad licitó el recambio de la iluminación en las calles de acceso
  a los barrios
entradilla: 'En el marco del Plan de Iluminación, se colocarán 1.135 nuevas luces
  led en las arterias de penetración y de mayor circulación a los barrios. '

---
El fin es lograr mejoras que permitan el desplazamiento de los vecinos, priorizando el transporte y el acceso a los servicios y establecimientos públicos. Se presentaron tres empresas a la licitación realizada este miércoles.

En el marco del Plan de Iluminación que puso en marcha la Municipalidad de Santa Fe, se llevó a cabo una nueva licitación, denominada “Avenidas de tu barrio”, para dotar con luces led las calles de acceso y de mayor transitabilidad. Vale destacar que el objetivo del municipio es recuperar el alumbrado existente en toda la ciudad, lo que contempla el mantenimiento de la red actual pero también implica dotar de nuevas zonas que actualmente poseen un servicio deficiente.

En este caso, el fin es contribuir a una homogeneización del sistema de iluminación, por eso se proyecta abordar las avenidas de penetración a los barrios, con mejoras que permitan el desplazamiento de cada vecino y vecina, priorizando las vías de circulación del transporte público, las áreas con inmuebles públicos en salud, seguridad y educación, y finalmente las arterias de mayor circulación. De esta manera se incorporarán 1.135 nuevos artefactos led en las arterias principales, mientras sigue desarrollándose el Plan de Iluminación en diferentes barrios.

Es importante destacar que mejorar la red de alumbrado público a través de la actualización tecnológica permite optimizar la respuesta a las necesidades de los vecinos y vecinas, reducir los reclamos por deficiencias en el servicio y finalmente producir ahorros de consumo.

Para esta tarea se presentaron tres empresas para concretar el recambio. El presupuesto oficial es de $ 44.682.793,50: y la primera empresa es Electromecánica Tacuar SRL que ofertó por $ 52.948.885; la segunda es Bauza Ingeniería SRL y cotizó por $ 52.157.473,87; y la tercera es Base Ingeniería y propuso hacer las tareas por $ 56.389.685.

**Diferentes barrios**

Estos 1.135 artefactos led se distribuirán en las arterias principales de distintos barrios de la ciudad. En un sector de Barranquitas, en Fomento 9 de Julio, Los Hornos, y Ciudadela Norte se pondrán alrededor de 264 nuevas luminarias que se distribuirán en Iturraspe desde Av. Pte. Perón hasta Aristóbulo del Valle; y en San Jerónimo desde Larramendi hasta J.M. Zuviría.

También en Cochabamba desde Iturraspe hasta F. C. Rodríguez; en Gobernador Freyre desde Pedro Ferré hasta Estanislao Zeballos; en Pte. Roca desde la avenida Aristóbulo del Valle hasta la avenida Facundo Zuviría; en Gorostiaga desde P. Hudson hasta Urquiza; y en Gorostiaga desde Gobernador Freyre hasta Pasaje Santa Fe.

También se van a colocar 95 artefactos led en los barrios Guadalupe Oeste y Transporte. En estos sectores se intervendrá en las calles Javier de la Rosa desde la avenida General Paz hasta Facundo Zuviría; en Regimiento 12 de Infantería desde avenida Facundo Zuviría hasta la avenida Ángel Peñaloza. Además, en el barrio Villa Setúbal se colocarán 44 nuevas luces led sobre calle Vélez Sársfield desde Martín Zapata hasta Ángel Cassanello.

Por otro lado, se instalarán alrededor 147 artefactos led en los barrios San Martín, Villas Las Flores y Las Flores II. Allí se colocarán a lo largo de Larrea desde la avenida Blas Parera hasta la avenida Ángel Peñaloza; en Azopardo desde Larrea hasta Millán Medina; en Millán Medina desde Azopardo hasta la avenida Blas Parera; en Pavón desde Aguado hasta la avenida Ángel Peñaloza; y en Estrada desde Pavón hasta Castelli.

**En otros sectores**

Dentro del distrito Noroeste, se cambiarán 139 artefactos en los barrios Los Troncos, Acería y Del Tránsito. En estos sectores se trabajará en French desde la avenida Blas Parera hasta calle Santa Fe; y en Beruti desde la avenida Blas Parera hasta Paredes.

Mientras tanto, en los barrios Chalet y San Lorenzo se repondrán 76 luminarias led en Dr. Zavalla desde J.J. Paso hasta Monseñor Zazpe; y en San José desde Mariano Cabal hasta Monseñor Zazpe. Y en barrio Centenario y en el Fonavi San Jerónimo se pondrán 67 artefactos nuevos en Roque Sáenz Peña desde J.J. Paso hasta Raúl Tacca; en Zavalía desde Cabal hasta Raúl Tacca; en Tarragona desde Cabal hasta Cristóbal Colón; en Pietranera desde Roque Sáenz Peña hasta Francia; y en Saavedra desde J.J. Paso hasta Pietranera.

Y por último se cambiarán cerca de 303 luces led en los barrios Roque Sáenz Peña, Roma y Santa Rosa de Lima. En estos sectores se trabajará en Monseñor Zazpe desde avenida Freyre hasta avenida Mosconi; en avenida Mosconi desde Monseñor Zazpe hasta Mendoza; en Lamadrid desde Mendoza hasta Naciones Unidas.

También en Corrientes desde avenida Freyre hasta Gaboto; en Juan de Garay desde avenida Freyre hasta Gaboto; en Mendoza desde avenida Freyre hasta Circunvalación; en Moreno desde Mitre hasta Padre Quiroga; en Estrada desde Monseñor Zazpe hasta Mendoza; y en Aguado desde Mendoza hasta Gobernador Vera.