---
category: Deportes
date: 2021-09-05T06:15:19-03:00
thumbnail: https://assets.3dnoticias.com.ar/ESCANDALO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Brasil: el plantel argentino abandonó el estadio Arena Corinthians rumbo
  al Aeropuerto'
title: 'Brasil: el plantel argentino abandonó el estadio Arena Corinthians rumbo al
  Aeropuerto'
entradilla: El embajador Daniel Scioli ya espera a la Selección en el aeropuerto para
  garantizar que todos los jugadores puedan embarcar el vuelo de regreso al país.

---
El plantel del seleccionado argentino de fútbol, luego de la controversia generada que obligó a la suspensión del partido de eliminatorias ante el local Brasil, abandonó el estadio Arena Corinthians de San Pablo, rumbo al Aeropuerto Internacional de Guarulhos.

Luego de tres horas y unos minutos más respecto del momento en que se suspendió el cotejo de la clasificación continental rumbo a Qatar 2022, la delegación albiceleste dejó la cancha a las 19.19, en un micro de dos pisos de la empresa Trans Wolff.

Los jugadores Emiliano ‘Dibu’ Martínez, Cristian Romero, Giovani Lo Celso y Emiliano Buendía también formaron parte de la nómina que se trasladó hacia la estación aérea paulista.

En un automóvil particular, el embajador argentino, Daniel Scioli, junto a un grupo de colaboradores, acompañó a la delegación albiceleste con el propósito “de facilitar los trámites migratorios”, según confiaron a Télam allegados al equipo del DT Lionel Scaloni.

Los cuatro futbolistas mencionados fueron objeto de cuestionamiento de parte de la Agencia Nacional de Vigilancia Sanitaria (Avinsa) por “falsear sus declaraciones juradas al ingreso” a Brasil y no expresar que procedían del Reino Unido con posterior estadía en Caracas, Venezuela.

Las autoridades sanitarias brasileñas exigen a los pasajeros que llegan desde Gran Bretaña y otros territorios europeos “una cuarentena mínima” de 14 días, a raíz de la pandemia del coronavirus.

Sin embargo, desde la AFA, la Confederación Brasileña de Fútbol (CBF) y la propia Conmebol se argumenta que esa solicitud “no es necesaria” porque las competencias futbolísticas que se realizan en territorio sudamericano, se rigen bajo otro tipo de protocolos sanitarios.