---
category: Agenda Ciudadana
date: 2021-09-13T06:15:06-03:00
thumbnail: https://assets.3dnoticias.com.ar/vidal.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Vidal se impuso en la interna porteña de Juntos por el Cambio
title: Vidal se impuso en la interna porteña de Juntos por el Cambio
entradilla: La exgobernadora de la provincia de Buenos Aires se impuso sobre sus contrincantes
  y las tres listas sumadas conseguían el 48,36 por ciento de los votos.

---
Juntos por el Cambio, con María Eugenia Vidal como la candidata más votada en la puja interna de la oposición, se impuso en las PASO de la ciudad de Buenos Aires por sobre la lista del Frente de Todos que encabezó Leandro Santoro, mientras Javier Milei, de Libertad Avanza, se ubicó en el tercer lugar.  
  
Escrutadas el 97,26% de mesas escrutadas, la alianza de Juntos por el Cambio (JxC) obtenía el 48,23% de los votos -sumando las tres listas que fueron a la interna-; mientras que el Frente de Todos (FDT) alcanzaba un 24,62% de los sufragios y Libertad Avanza un 13,66%.  
  
Detrás, quedó el Frente de Izquierda y de los Trabajadores con el 6,22% y Autodeterminación y Libertad con 2,65%  
  
De esta manera, en el marco de unos comicios inéditos por las medidas de seguridad adoptadas por la pandemia de coronavirus, fueron seis de las 17 listas que se presentaron las agrupaciones las que lograron superar el piso requerido para participar de las generales el 14 de noviembre en la categoría legislativa.  
  
En el próximo llamado electoral, la Cámara de Diputados renovará 13 escaños, de los cuales el FdT sólo arriesga tres bancas, mientras que los restantes 10 corresponden a JxC: cinco del PRO, tres de la UCR y dos de la Coalición Cívica.  
  
En Juntos por el Cambio, en tanto, en la interna en la que participaron tres listas quedaron habilitadas las encabezadas por Vidal, con 567.278 votos; y por López Murphy con 193.905, mientras que la liderara por Rubinstein quedó sin chance al no alcanzar el piso requerido.  
  
La lista definitiva de candidatos de la alianza opositora se conformará mediante el sistema D´Hont entre las dos agrupaciones. No obstante, lo armado no se ajustará al reglamento original firmado en el acta constitutiva ya que el esquema fue modificado en la última semana.  
  
El cambio respondió a la queja del exministro de Economía por la posibilidad de quedar en una posición en la lista con poca chance de lograr el ingreso a la Cámara Baja ante la obligatoriedad a respetar la paridad de género, por lo cual se alteró el mecanismo para que ello no ocurra.  
  
Por el Frente de Todos, se consagró la lista Celeste y Blanca K, con 423.846 votos, y llevará como candidatos a candidatos a diputados nacionales a Santoro, acompañado por Gisela Marziotta.  
  
Asimismo, Libertad Avanza, encabezada por el economista de derecha Milei, obtuvo 235.204 votos.  
  
La otra interna que se resolvió en estas elecciones fue la del Frente de Izquierda y de Trabajadores en la que quedó habilitada para ir a las generales la nómina de Myriam Bregman; mientras que también logró meterse en las generales Autodeterminación y Libertad con Luis Zamora.  
  
Pasadas las 22, Vidal salió a hablar desde el búnker de JXC en Costa Salguero, donde estuvo acompañada por el ex presidente Mauricio Macri; Horacio Rodríguez Larreta, Patricia Bullrich, y sus adversarios en la interna.  
  
"Gracias por darnos esta nueva oportunidad que sabemos que es una tarea y una obligación", aseguró la exgobernadora desde el escenario y dijo que "esta noche se siente un ´basta´ atronador en las urnas".  
  
"Sabemos que esta no es una elección definitiva, pero hoy dimos el primer paso, empezamos a recuperar algo que creíamos que habíamos perdido", sostuvo y llamó "a no quedarse al costado del camino" a los que no fueron a votar en las PASO.  
  
Por su parte, Milei, desde el comando de campaña en Balvanera, manifestó que "los números" de la elección "muestran el hartazgo de esta casta política que nos empobrece" y remarcó que "si no hacemos un cambio de 180 grados, en 50 años vamos a ser la villa miseria mas grande del mundo".  
  
En tanto, no alcanzaron el piso necesario para pasar a las generales, unas 11 nóminas, entre ellas, la de Adelante Ciudad que encabezó Rubinstein; Revolucionemos la Izquierda liderada por Celeste Fierro; Autodeterminación; el Movimiento al Socialismo con Federico Winokur y Libres del Sur con Martín Hourest.  
  
Tampoco accedieron Partido Aptitud Renovadora que llevó como precandidato a Juan Pablo Chiesa; el Partido Autonomista con Eduardo Awad; Dignidad Popular-Frente Patriota con Héctor Jaime; el Partido Federal con Julio Bárbaro; el Partido Renovador Federal con Lucas Jaszewski; el Partido Socialista Auténtico con Silvia Vázquez y, finalmente, Política Obrero con Marcelo Ramal.