---
category: La Ciudad
date: 2021-02-13T06:00:50-03:00
thumbnail: https://assets.3dnoticias.com.ar/anfiteatro.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Jatón inauguró las obras en el anfiteatro del Parque del Sur
title: Jatón inauguró las obras en el anfiteatro del Parque del Sur
entradilla: El emblemático espacio cultural ubicado en avenida J.J. Paso y San Jerónimo
  fue habilitado por el intendente, este viernes a las 20 horas.

---
Las luces del escenario del anfiteatro “Juan de Garay” del Parque del Sur vuelvan desde este viernes a encenderse y las antiguas gradas devenidas en butacas, a recibir espectadores después de arduos trabajos de recuperación que demandaron a la Municipalidad de Santa Fe seis meses. Su reapertura fue encabezada por el intendente Emilio Jatón a las 20 horas, en una jornada en la que hubo espectáculos musicales para toda la familia.

El histórico espacio público recuperó su esplendor con importantes obras de mejoras en camarines y baños, y sumó butacas para mayor comodidad del público. Los trabajos se suman a la intervención integral que se realiza en el parque y las calles aledañas.

En su reciente recorrida por el anfiteatro, a comienzo de mes, Jatón aseguró que con la recuperación de este patrimonio arquitectónico “estamos cumpliendo los objetivos con los espacios públicos y eso es muy importante. Éste es un espacio para la comunidad, para los santafesinos”.

**Agenda cultural**

Artistas locales se presentarán entre este viernes 12 y el domingo 14, para celebrar la recuperación de este patrimonio cultural de Santa Fe Capital, con espectáculos para todas las edades: desde las 20 horas, el primer día se subirán al escenario la “Sonora D’Irse” y “Hugo & los gemelos”, en tanto, el sábado será el turno de “Los Ranser” y Diana Ríos. El domingo cerrarán, a partir de las 19 horas, “La Gorda Azul” y “Canticuénticos”.

Actualmente, la capacidad es de 1.800 espectadores, pero en el marco de los protocolos vigentes por la pandemia de Covid-19, sólo podrá utilizarse el 50% del espacio.

**Mejora integral**

En una primera etapa, con personal municipal, se realizaron tareas de limpieza y reparación del espacio y su entorno.

Luego se licitaron obras de accesibilidad y seguridad para poner en óptimas condiciones el espacio. Las tareas fueron ejecutadas por Alanco S.A. y demandaron una inversión municipal de $ 2.789.663,11. Se recuperaron los baños, los camarines y las distintas salas del inmueble.

En la última etapa se colocaron las butacas, que fueron adquiridas con fondos donados por el Banco Santa Fe y se realizaron las tareas de pintura de todo el predio, con el apoyo de Pinturerías Universo.

En paralelo, se concretan tareas de bacheo en las calles que rodean al anfiteatro y, antes del inicio de la temporada de verano, se hicieron trabajos de mejoras integrales en distintos puntos del Parque del Sur para adecuarlo a las necesidades de las personas que disfrutan de ese espacio.