---
category: La Ciudad
date: 2021-03-16T07:56:11-03:00
thumbnail: https://assets.3dnoticias.com.ar/asoem.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Diario Uno
resumen: Asoem definió un paro de 48 horas para esta semana
title: Asoem definió un paro de 48 horas para esta semana
entradilla: La medida de los municipales de la ciudad de Santa Fe será este miércoles
  y jueves. Asoem definió que sea sin asistencia a los lugares de trabajo.

---
Este lunes, y tras el primer encuentro del año de la Comisión Directiva y el Cuerpo de Delegados de la Asociación Sindical de Obreros y Empleados de la Municipalidad de Santa Fe (Asoem), quedó definido un paro de actividades por 48 horas este miércoles 17 y jueves 18, en la jurisdicción de Santa Fe, Rincón, Recreo, Arroyo Leyes y Monte Vera.

El paro de 48 horas de los empleados municipales se va a ejecutar sin asistencia a los lugares de trabajo y con mantenimiento de las guardias mínimas en los servicios esenciales.

"Si bien se trataron diferentes temáticas, la más determinante tiene que ver con el incumplimiento de los intendentes y presidentes comunales al no plantear una definición con respecto a la política salarial para el año 2021. Ante esa inercia, desde Asoem decidimos realizar un paro los días 17 y 18 de marzo", explicó el Secretario General de la Asociación Sindical de Obreros y Empleados de la Municipalidad de Santa Fe , Juan Medina.

Además, desde Asoem indicaron que le reclamarán a los intendentes y presidentes comunales “redefinir las asignaciones familiares que significan un incremento para los compañeros que la cobran”.

Juan Medina, añadió que también solicitarán a las autoridades de municipios y comunas adheridas a la Caja Municipal de Jubilaciones y Pensiones de Santa Fe, "remitir los fondos correspondientes para pagar los bonos de septiembre, octubre y noviembre de 2020, que fueran oportunamente acordados en la Paritaria Provincial".

“Estamos totalmente consustanciados con la lucha de los trabajadores y creemos que la fuerza de los mismos está en la unidad de la jurisdicción de la Asoem”, concluyó el Secretario General, Juan Medina.

Por su parte, los trabajadores municipales de la provincia de Santa Fe, anunciaron la semana pasada un paro de actividades por 48 horas también para este miércoles y jueves ante la "falta de una propuesta concreta en porcentuales, tramos y meses de pago", en el marco de la continuidad de las paritarias con los intendentes y presidentes comunales.

Así lo informó La Federación de Sindicatos de Trabajadores Municipales (Festram) que resolvió "ejecutar el mandato del plenario de secretarios generales, que los faculta a determinar acciones en caso de no arribar a un acuerdo".