---
category: Agenda Ciudadana
date: 2021-02-21T07:30:15-03:00
thumbnail: https://assets.3dnoticias.com.ar/lUCILA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Lucila Lehmann pide información para saber si hubo aplicaciones indebidas
  de vacunas en Santa Fe
title: Lucila Lehmann pide información para saber si hubo aplicaciones indebidas de
  vacunas en Santa Fe
entradilla: 'La iniciativa surgió ante los sucesivos abusos por parte del gobierno
  nacional y provincial respecto a la distribución de vacuna. '

---
“De ser cierta la información que fue brindada por medios de prensa, en relación a que el gobernador de Santa Fe Omar Perotti, vacunó a su familia en la ciudad de Rafaela, accionaremos penalmente por el incumplimiento de deberes como funcionario público y abuso de autoridad", destacó la diputada de la Coalición Cívica.

A raíz de la mala administración y abuso por parte de autoridades nacionales y provinciales en cuanto a la distribución de vacunas contra el Covid-19, la diputada nacional por la Coalición Cívica, Lucila Lehmann, solicitó acceso a información pública para conocer cuál es la cantidad de dosis de vacunas Sputnik–V recibidas en la provincia a la fecha.

También se pide información sobre la totalidad de establecimientos habilitados por la provincia para la aplicación de dichas dosis y la cantidad total de vacunas recibidas en cada uno de ellos; la cantidad total de personas vacunadas en cada uno de los centros habilitados como así también aclaración de la cantidad de aquellos que hayan recibido una segunda dosis de la vacuna.

La diputada solicitó nómina de personas vacunadas para determinar si hubo beneficiarios que no se encontraban dentro de la categoría de "personal esencial". A su vez, se informe cuáles son las medidas sanitarias y de seguridad adoptadas por la provincia para la conservación y distribución de las vacunas.

Enviaremos pedido de acceso a la información pública, para que el Gobierno de Omar Perotti nos informe la lista de vacunados. Por cosas como éstas, presentamos un proyecto para penalizar a los que abusan de poder en la distribución y aplicación de vacunas", enfatizó Lehmann.

"Anticipamos que haremos pública la información requerida y contestada para la toma de conocimiento de cualquier interesado. De ser cierta la información que fue brindada por medios de prensa, en relación a que el gobernador de Santa Fe Omar Perotti, vacunó a su familia en la ciudad de Rafaela, accionaremos penalmente por el incumplimiento de deberes como funcionario público y abuso de autoridad", concluyó la diputada.