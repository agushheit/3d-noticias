---
category: Estado Real
date: 2021-05-10T06:00:22-03:00
thumbnail: https://assets.3dnoticias.com.ar/POLICIA.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Autoridades de seguridad reclamaron que se traten las leyes para modernizar
  el sistema policial provincial
title: Autoridades de seguridad reclamaron que se traten las leyes para modernizar
  el sistema policial provincial
entradilla: 'El secretario de Seguridad Pública de la provincia, Germán Montenegro,
  remarcó: “Estamos dispuestos a ir a debatir a la Legislatura santafesina el contenido
  de las mismas”.'

---
El gobierno de la provincia, a través del Ministerio de Seguridad, envió en octubre de 2020 a la Legislatura, los proyectos para modernizar el sistema de Seguridad Pública y el sistema Policial. En la actualidad, la Policía de la Provincia de Santa Fe cuenta con bases doctrinarias vetustas y anacrónicas; una organización rígida y conformada por un conglomerado de piezas agregadas sin sentido organizacional integral; y modalidades de trabajo ineficientes frente a la complejidad de las problemáticas criminales.

En este marco, el secretario de Seguridad Pública de la provincia, Germán Montenegro, remarcó que es “fundamental para la gestión del gobernador Omar Perotti y del ministro Jorge Lagna el tratamiento y la eventual aprobación de los proyectos de Ley”, y destacó: “Estamos dispuestos a ir a debatir a la Legislatura santafesina el contenido de las mismas, pero es fundamental llevar adelante un proceso de modernización de la Policía de la provincia de Santa Fe, porque la institución, tal como está organizada y funcionando actualmente, no tiene ya resto para enfrentar los desafíos del crimen organizado, la violencia, y los problemas delictivos contemporáneos”, remarcó el funcionario.

Cabe señalar que en la Asamblea Legislativa del pasado 1º de mayo, el gobernador Omar Perotti reclamó a diputados y senadores que discutan los proyectos: “Les pido a ustedes, legisladoras y legisladores, que demos el debate. Seamos artífices de esta reforma que amerita del trabajo en común con el Poder Legislativo y el Judicial para asegurarle entre todos a la gente que la podemos cuidar mejor”, sostuvo, y agregó: “Tenemos un sistema normativo añejo, pensado hace más de 40 años. Necesitamos modernizarlo para mejorar nuestras herramientas de actuación, por eso hablamos de la importancia de tres proyectos de ley que enviamos a la Legislatura para su debate e incorporación de las mejoras que ustedes consideren convenientes”.

“Su discusión, mejora, corrección y aprobación nos permitirá modernizar las bases jurídicas e institucionales del sistema de Seguridad; para tener una policía más profesionalizada, democrática y con perspectiva de género; con los debidos controles para la investigación de las actividades ilegales”, finalizó el mandatario provincial.

**Sobre las iniciativas**

En octubre de 2020, el Gobierno Provincial, a través del Ministerio de Seguridad, presentó los proyectos para modernizar el sistema de Seguridad Pública y el sistema Policial. En la actualidad, la Policía de la Provincia de Santa Fe cuenta con bases doctrinarias vetustas y anacrónicas; una organización rígida y conformada por un conglomerado de piezas agregadas sin sentido organizacional integral; y modalidades de trabajo ineficientes frente a la complejidad de las problemáticas criminales.

Dichos proyectos establecen las bases jurídicas e institucionales para fortalecer las capacidades de gobierno en materia de seguridad y de conducción política del sistema policial provincial. Asimismo, busca mejorar las condiciones de trabajo, promover la igualdad de género dentro de la Institución Policial y profundizar la formación y capacitación del personal.

Refieren a Seguridad Pública; Control del Sistema Policial y reforma del Sistema Policial de la Provincia de Santa Fe. Con las tres iniciativas se busca reordenar la estructura de conducción de las fuerzas y liberar de las tareas administrativas a los agentes policiales. El modelo hacia el que se aspira que vaya la Policía reemplazará las 19 unidades regionales por seis superintendencias que regionalizarán a la Policía de la provincia. El objetivo es que, a partir de la incorporación de tecnología, con una gran inversión que está haciendo el gobierno santafesino, se reemplace el patrullaje aleatorio por uno que será orientado de acuerdo a los datos que arrojen los mapas del delito.

Asimismo, dichos proyectos también dan forma a los cuatro grandes componentes que formarán la estructura de la Policía de la provincia con una Policía de Seguridad Preventiva, la Agencia de Investigación Criminal, la Agencia de Control Policial y las Tropas de Operaciones Especiales, y en todas se contempla el bienestar policial. Los otros aspectos relevantes son la importancia que se le dará a la seguridad local con la participación de los jefes comunales, y la formalización de la creación del Observatorio de Seguridad Pública y la Agencia de Control de Armas de Fuego, organismos que ya se encuentran trabajando en el ámbito del Ministerio de Seguridad provincial.