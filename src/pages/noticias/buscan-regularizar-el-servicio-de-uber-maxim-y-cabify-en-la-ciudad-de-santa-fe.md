---
category: La Ciudad
date: 2021-04-20T07:27:46-03:00
thumbnail: https://assets.3dnoticias.com.ar/app-taxi.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Diario Uno
resumen: Buscan regularizar el servicio de Uber, Maxim y Cabify en la ciudad de Santa
  Fe
title: Buscan regularizar el servicio de Uber, Maxim y Cabify en la ciudad de Santa
  Fe
entradilla: ' "Las plataformas de viajes ya están funcionando en la ciudad y es vital
  una normativa eficaz respecto a requisitos del vehículo, conductor y circulación".'

---
Este lunes por la mañana los concejales santafesinos del PRO, Luciana Ceresola y Sebastián Mastropaolo, ingresaron un proyecto de ordenanza en el Concejo Municipal con el objetivo de regularizar la realidad del servicio de transporte privado de pasajeros por plataformas digitales que ya funciona en la ciudad de Santa Fe como Uber y Maxim. Sobre Cabify, aún no se confirmó su prestación oficial de servicio.

A mediados del 2020, en medio de la pandemia por Covid-19 y luego de muchos rumores y suposiciones sobre el desembarco de la plataforma Uber en la ciudad capital, la misma compañía internacional de vehículos de transporte ratificaba su arribo con el servicio Uber Flash. Ya en el mes de octubre, la app que ofrece viajes y traslados de personas, comenzó a ofrecer su servicio a aquellos usuarios que necesitaban realizar "viajes esenciales" y al "personal de salud a trasladarse de manera segura desde y hacia los hospitales y centros de salud".

Rápidamente, taxistas y remiseros se manifestaron enérgicamente frente al Municipio y al Concejo local pidiendo "la regulación urgente" del servicio. A una semana de su desembarco, Uber informaba que "más de 180 personas se anotaron para ser choferes". Ya a principios de este 2021, otra empresa internacional de viajes urbanos e interurbanos, Maxim, oficializaba su llegada a Santa Fe.

Así la situación, la Municipalidad de Santa Fe por medio de la Secretaría de Control, salió a realizar inspecciones en la calles de la ciudad y a retener vehículos vinculados con dichas plataformas de servicio. Pero lo cierto es que cada vez más choferes ponen a disposición sus autos para trabajar y cada vez más santafesinos utilizan las plataformas para pedir viajes.

En ese contexto, el proyecto de ordenanza ingresado este lunes en el Concejo municipal, persigue como objetivo "a través de la regulación del transporte privado concertado con la intermediación de plataformas electrónicas, garantizar la seguridad de los usuarios santafesinos de dicho sistema", sostuvo la concejal del PRO Luciana Ceresola y continuó agregando: "De hecho las plataformas de servicio ya están funcionando en la ciudad; no podemos tapar el sol con un dedo y negar su existencia; tenemos que regularla y reconocerla. Es vital una normativa eficaz respecto a requisitos del vehículo, conductor y circulación".

"Se trata de una ordenanza para regular este servicio que ya está presente en el mundo, en Argentina y en la ciudad comienza a aparecer ya de manera irregular. Es necesario un marco legal para que funcionen con las mismas exigencias que el transporte público por taxis y remises. La realidad existe y no la podemos negar, sobre todo en un contexto de pandemia donde la tecnología está más cerca de las personas, no solo en materia de transporte, sino también en la intermediación de distintos bienes", manifestó Ceresola.

"La existencia de estas aplicaciones tienen que ver con el servicio de transporte privado de personas que viene a cubrir una demanda no cubierta por el servicio de transporte público de pasajeros. En ese contexto tenemos que pedir todos los requisitos necesarios,, tanto personales como fiscales, para que estos servicios como Uber, Maxim o Cabify puedan seguir funcionando en la ciudad. Muchísimos santafesinos ya están usando estos servicios sin ningún tipo de regulación", subrayó la concejala Luciana Ceresola.

"Somos conscientes de la crisis del transporte que atraviesa nuestra ciudad, con un alto nivel de demanda insatisfecha de personas que requieren servicios de transporte, más aun luego de un año marcado por la emergencia sanitaria declarada en virtud del SARS-CoV-2, es por esto que es necesario regular el servicio de intermediación del transporte privado de personas que es ejercido a través de plataformas electrónicas y es lo que buscamos con este proyecto. Esta regulación, además, busca generar un aumento de la oferta del transporte (en este caso privado) en una ciudad donde el servicio público de transporte en general es deficitario", finalizaron los concejales del PRO en la ciudad de Santa Fe a través de un comunicado.