---
category: Estado Real
date: 2021-06-30T08:38:18-03:00
thumbnail: https://assets.3dnoticias.com.ar/industria.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno Santa Fe
resumen: La provincia eximirá del pago de impuestos a más de 20 sectores productivos
  afectados por la pandemia
title: La provincia eximirá del pago de impuestos a más de 20 sectores productivos
  afectados por la pandemia
entradilla: El titular de API, Martín Ávalos, destacó que el objetivo “es generar
  una contención a todos los sectores que por las medidas sanitarias se han visto
  afectados en su actividad habitual”.

---
Ante la aprobación del proyecto de ley enviado por el gobierno provincial a la Legislatura, más de 20 sectores productivos, que se vieron afectados por la medidas sanitarias adoptadas ante la emergencia sanitaria por Covid-19, quedaron eximidos del pago de los ingresos brutos, de sellos y tasas retributivas de servicios, de los meses de abril a septiembre de 2021.

Al respecto, el administrador Provincial de Impuestos, Martín Avalos, precisó que el objetivo “es generar una contención a todos los sectores que por las medidas sanitarias se han visto afectados en su actividad habitual. Es un esfuerzo fiscal muy importante, que sin lugar a dudas genera un beneficio directo e inmediato a todos estos sectores económicos”.

“Además, esta medida alcanza a todos los pequeños contribuyentes del régimen simplificado, cualquiera sea la actividad; y a los pequeños comerciantes minoristas, que se van a ver eximidos del pago del saldo de declaraciones juradas”, especificó.

Asimismo destacó que “es un proyecto que ha sido remitido por el Poder Ejecutivo, pero también la Legislatura ha trabajado, se ha logrado un consenso importante, y tuvo una acogida positiva. Creo que el abanico del los sectores políticos está conforme con la medida adoptada”.

**SECTORES BENEFICIARIOS**

El beneficio rige para las actividades debares, restaurantes y similares; servicios de alojamiento, hotelería, residenciales, campings y similares; servicios de agencias de viaje y turismo y similares; servicios de transporte automotor de pasajeros para el turismo, de excursiones y similares; servicios profesionales y personales vinculados al turismo; servicios de explotación de playas y parques recreativos; venta al por menor de artículos o artesanías regionales; servicios vinculados a la organización de ferias, congresos, convenciones o exposiciones y similares; servicios para eventos infantiles; organización de eventos; servicios de soporte y alquiler de equipos, enseres y sonido para eventos; alquiler temporario de locales para eventos; servicios de peloteros; alquiler de canchas y demás establecimientos para práctica de deportes; jardines maternales, centros de atención de desarrollo infantil, servicios de salones de baile y discotecas, sala de teatros, de cinematografía, eventos culturales, gimnasios, servicios de peluquería, agencias de lotería, clínicas y sanatorios privados que presten servicios de internación y empresas de emergencias médicas en la medida que se encuentren domiciliados en la provincia de Santa Fe. Cabe destacar que la mayoría de estas actividades económicas están exentas del pago de impuestos provinciales desde septiembre de 2020.

Ávalos resaltó que además de los sectores señalados, también se encuentran exentos la totalidad de los pequeños contribuyentes adheridos al régimen simplificado del impuesto sobre los ingresos brutos -independientemente del sector al que pertenezcan- para las cuotas de los meses de abril a septiembre de este año y, recordó que las cuotas de septiembre de 2020 a marzo de 2021 ya habían sido eximidas, beneficiando así a más de 130.000 pequeños contribuyentes. Asimismo, informó que los pequeños comerciantes minoristas locales, en la medida que no se encuentren entre los declarados esenciales, quedan eximidos de pagar los saldos de sus declaraciones juradas correspondientes a los meses de junio a septiembre de éste año.

Por otra parte, se ha dispuesto una nueva prórroga por 60 días del régimen de regularización tributaria que se encontraba vigente, posibilitando que todas las deudas devengadas al 31 de octubre de 2020 puedan ser canceladas con importantes reducciones de intereses y condonación de sanciones, pero lo más importante es el nuevo plan de facilidades de pago para las deudas generadas desde noviembre del año pasado hasta abril inclusive del corriente año. En ese sentido el funcionario explicó que el nuevo plan de facilidades contempla la posibilidad de abonar las deudas impositivas a valores históricos, financiándola hasta en 6 cuotas sin intereses o hasta en 12 cuotas con un interés del 0,5% mensual sobre saldo.

Señaló Ávalos que todas estas medidas, de suma importancia para la contención de los sectores más perjudicados por la situación sanitaria que nos encontramos atravesando, vienen a complementar todo un conjunto de disposiciones que se fueron adoptando durante el año como por ejemplo la prórroga de los vencimientos de las declaraciones juradas del Impuesto sobre los Ingresos Brutos y de las cuotas del Régimen Simplificado de los meses de abril a junio/2021, considerándolas ingresadas en término, hasta diciembre/2021, conforme se estableció por Res. Gral. N° 21/202; o la quita del tope máximo del monto "saldo a favor" para pedir las Constancias de exclusión de retenciones y/o percepciones de Ingresos Brutos (Res. Gral. 27/2021) y la suspensión por 90 días mediante Res. Gral. 29/21 de la iniciación y/o prosecución de juicios de apremios y medidas cautelares para determinadas actividades.

También, por Res. Gral. N° 028/21 se prorrogó hasta el 30/06/2021 el régimen de regularización de deudas tributarias para las devengadas con anterioridad al 31 de octubre de 2020 y, respecto a las deudas devengadas entre el 1 de noviembre de 2020 y el 30 de abril de 2021, se estableció, mediante Decreto N° 837 reglamentado por Res. Gral. 30/2021, un plan especial de pagos con 0 (cero) tasa de interés resarcitorio.