---
category: La Ciudad
date: 2021-05-13T09:54:31-03:00
thumbnail: https://assets.3dnoticias.com.ar/parente.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Diario Uno
resumen: El peor final para Roberto Parente
title: El peor final para Roberto Parente
entradilla: " La Agencia de Investigación Criminal halló el cuerpo de Roberto Parente
  en aguas del río frente al parque de la Constitución Nacional."

---
Este miércoles por la tarde, alrededor de las 16, pesquisas de Trata de Personas de la Agencia de Investigación Criminal (AIC) de la Policía de Santa Fe, en una embarcación que fue cedida por un particular y mientras rastrillaban la zona de río frente al Parque Biblioteca de la Constitución Nacional y de los tres poderes del Estado (Ejecutivo, Legislativo y Judicial), hallaron un cuerpo en el agua, que por la vestimenta y la complexión física fue reconocido como el de Roberto Parente de 67 años.

Luego, sucedió el trabajo de levantamiento y reconocimiento del cadáver, y la comunicación a la Jefatura de la AIC, y éstos hicieron lo propio con las autoridades del Ministerio de Seguridad provincial y la fiscalía abocada al caso del Ministerio Público de la Acusación.

Inmediatamente después llegó la jefa de Trata de Personas y Violencia Familiar y Sexual de la Agencia de Investigación Criminal AIC, Analía Noval, que convocó al médico de la repartición policial para dar legalidad a todo el trámite de rescate y de reconocimiento como también informó debidamente al fiscal del Ministerio Público de la Acusación, Ignacio Suasnábar. Ahora, se aguarda la realización del resto de los peritajes mientras concurren al lugar los oficiales y suboficiales de Buzos Tácticos de la Policía de Santa Fe y los de la Prefectura Naval Argentina Puerto de Santa Fe.

**Derrotero de Parente**

Tal como fue publicado por UNO Santa Fe en las últimas horas, existió una marcada preocupación por parte de familiares y de amigos de Roberto Parente de 67 años, desde el último viernes cuando salió de su casa ubicada sobre calle Pedro Zenteno al 2100 en el barrio General Alvear de la capital santafesina, y sobre cuyo paradero se desconoce toda información, más allá de que las últimas imágenes que se tienen de él fueron el mismo día por bulevar Gálvez al 1.100 y por bulevar Gálvez al 927 (casi Laprida) por la vereda de enfrente al Diario UNO de Santa Fe, en ambos casos en el barrio Candioti.

Informaron la novedad sobre la ocurrencia del hallazgo como la identificación de Roberto Parente de 67 años a la Jefatura de la Agencia de Investigación Criminal AIC, y éstos hicieron lo propio con las autoridades del Ministerio de Seguridad provincial de quienes dependen como de la fiscalía abocada al hallazgo de la persona buscada y con el cúmulo de circunstancias que rodean a este caso.