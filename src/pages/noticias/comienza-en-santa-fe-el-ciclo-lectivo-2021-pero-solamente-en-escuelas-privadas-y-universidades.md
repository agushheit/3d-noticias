---
category: Agenda Ciudadana
date: 2021-03-15T06:10:45-03:00
thumbnail: https://assets.3dnoticias.com.ar/aulas.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Comienza en Santa Fe el ciclo lectivo 2021, pero solamente en escuelas privadas
  y universidades
title: Comienza en Santa Fe el ciclo lectivo 2021, pero solamente en escuelas privadas
  y universidades
entradilla: Mientras tanto, los alumnos de las escuelas públicas deberán esperar hasta
  el miércoles cuando finalice el paro docente de 48 horas.

---
Los docentes de las instituciones educativas públicas votaron un paro de actividades para los días 15 y 16 de marzo y también para la próxima semana, tanto para el 23 como para el 25 de marzo. La oferta del gobierno provincial había sido de un 35%, dividido en un 18% para marzo, 8% julio y 9% septiembre.

El mismo ministro de Trabajo de Santa Fe, Juan Manuel Pusineri, explicó horas después del rechazo a la oferta salarial realizada por el Gobierno santafesino, que la misma "fue construida con los sindicatos durante todo el mes de febrero y que se ubica por encima de cualquier otra en el país". En cuanto a la votación, Amsafé rechazó la propuesta con un total de 24.331 votantes que optaron por no aceptar el aumento con el 52,02% (12.657 votos) en contra y un 47,58% (11.577 votos) a favor.

Por otra parte, desde el Ministerio de Trabajo, Empleo y Seguridad Social, indicaron que "la provincia de Santa Fe recurrirá a los mecanismos legales que estén a su alcance para garantizar la educación en el territorio provincial durante el ciclo lectivo 2021".

El formato de clases se dará bajo una bimodalidad que consistirá en un complemento de educación a distancia y con presencialidad de una semana en la escuela y otra semana con los alumnos aprendiendo desde sus hogares. Las jornadas se dividirán en cinco momentos: ingreso con protocolos sanitarios; desarrollo curricular; un recreo largo; actividades lúdicas y recreativas a cargo de los docentes especiales (plástica, educación física y música); otro momento de desarrollo curricular; y un cierre.