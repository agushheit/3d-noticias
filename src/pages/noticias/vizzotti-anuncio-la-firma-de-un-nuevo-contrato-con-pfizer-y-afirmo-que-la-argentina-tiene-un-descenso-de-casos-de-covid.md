---
category: Agenda Ciudadana
date: 2022-01-21T06:00:08-03:00
thumbnail: https://assets.3dnoticias.com.ar/VIZZOTTI.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: Vizzotti anunció la firma de un nuevo contrato con Pfizer y afirmó que la
  Argentina tiene "un descenso" de casos de Covid
title: Vizzotti anunció la firma de un nuevo contrato con Pfizer y afirmó que la Argentina
  tiene "un descenso" de casos de Covid
entradilla: '"Pudimos firmar un nuevo contrato, una enmienda, para ampliar la vacunación.
  Hemos adquirido 18,5 millones de dosis para el año 2022, que incluye vacuna pediátrica",
  señaló la ministra.

  '

---
La ministra de Salud de la Nación, Carla Vizzotti, anunció que el Gobierno firmó un nuevo contrato con el laboratorio norteamericano Pfizer para la compra de 18,5 millones de dosis contra el coronavirus, y afirmó que la Argentina tiene "un descenso" de casos de Covid-19.

"Pudimos firmar un nuevo contrato, una enmienda, para ampliar la vacunación. Hemos adquirido 18,5 millones de dosis para el año 2022, que incluye vacuna pediátrica", resaltó Vizzotti, luego de que la ANMAT autorizó la utilización de la vacuna pediátrica Pfizer en chicos de 5 a 11 años.

Durante una conferencia de prensa en la Casa Rosada, la funcionaria nacional recordó que los contratos firmados con Pfizer eran por "20 millones de dosis para recibir durante 2021, que se estarán recibiendo también en enero y febrero de este año".

"El total de dosis del laboratorio Pfizer es 38,6 millones. Hasta ahora hemos recibido 15,3 millones destinadas a adultos, que es una vacuna diferente a la de pediatría", puntualizó.

Además, precisó que de las vacunas que estará recibiendo la Argentinas "5 millones son para chicos de entre 5 y 11 años", y completó: "Vamos a recibir el primer millón y medio entre febrero y marzo, y los segundo 3,5 millones en el segundo trimestre entre abril y junio".

"Durante 2022, nuestro país recibirá un total de 24,9 millones de dosis de Pfizer para el avance del Plan Estratégico de Vacunación", señaló el Ministerio de Salud de la Nación en un comunicado.

En otro tramo de la conferencia, Vizzotti descartó la exigencia de un pase sanitario para que los niños puedan concurrir a las escuelas, y reconoció que "se evaluó" esa posibilidad, pero se consideró que "no es necesaria" dado "la demanda de vacunas y el avance de la vacunación".

Consultada sobre un proyecto de ley presentado por un diputado del oficialismo para establecer la obligatoriedad de la inmunización contra el Covid-19, la funcionaria consideró que "la incorporación de una vacuna al calendario es para consolidar un derecho adquirido y que no dependa de una voluntad política".

No obstante, aclaró que hay una resolución ministerial al respecto porque aún "no está dada la situación respecto al escenario epidemiológico, cuál va a ser la variante que circulará y cuál será la población objetivo para incorporar una nueva vacuna al calendario".

Asimismo, adelantó que "en la semana del 7 de febrero se efectuará un Consejo Federal conjunto" entre las áreas de Salud y Educación de todo el país para revisar la "estrategia para mejorar los protocolos del inicio de clases" y "coordinar el trabajo de todo el ciclo lectivo".

Respecto de la situación epidemiológica, la funcionaria nacional detalló: "Desde el punto de vista epidemiológico, es la primera semana en la que tenemos un descenso comparado con las tres anteriores. Esto se ve en las provincias en las que iniciaron los casos en una primera instancia".

"Por primera vez, además de una desaceleración, también vemos una baja, 18% menos en el AMBA y en la zona central llega al 31%", manifestó Vizzotti, y precisó que también se han producido "menos fallecimientos con tres veces más de casos" que la ola anterior.

En ese marco, la ministra indicó que se registró "un descenso en el número de casos" de coronavirus, y consideró que los indicadores que marcan esta desaceleración "son favorables y optimistas".

"Estamos viendo un aumento exponencial de casos, no solo en la Argentina sino en el mundo, debido a la variante Ómicron", manifestó la ministra, y calificó como una "buena noticia desde el punto de vista epidemiológico" que esta semana hubo un "descenso del número casos comparado con las últimas tres".

Al referirse al encuentro que tuvo este mediodía en la Residencia de Olivos con el presidente Alberto Fernández, Vizzotti indicó que dialogaron sobre la flexibilización de "los requisitos para el ingreso" a la Argentina desde países limítrofes.

En esa línea, reveló que habló con el jefe de Estado sobre la posibilidad de eliminar la solicitud de "un test de ingreso para simplificar la circulación" entre los países vecinos.

Por último, afirmó que Fernández le pidió que se avance para que "los y las argentinas que vienen desde el exterior o residentes, y estén vacunados, tampoco necesiten realizarse un test diagnóstico".