---
category: La Ciudad
date: 2021-02-16T08:41:34-03:00
thumbnail: https://assets.3dnoticias.com.ar/nafta.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Con información de Diario Uno
resumen: Nuevo aumento de la nafta en la ciudad
title: Nuevo aumento de la nafta en la ciudad
entradilla: 'El litro de nafta promedio aumentó entre 1 peso y 80 centavos, según
  la petrolera. No se descartan nuevos aumentos de YPF y el resto. '

---
En las últimas horas todas las petroleras que prestan servicios en la ciudad de Santa Fe, aumentaron los precios de todos sus combustibles. El mismo se debe a la readecuación de los precios de las naftas.

Se trata de la quinta actualización de este año en el precio de los combustibles se concretó luego de que Yacimientos Petrolíferos Fiscales (YPF) aplicara en sus tablas un aumento del 1,26 % en promedio en gran parte del país.

Por su parte, desde la Federación Argentina de Expendedores de Nafta del Interior (Faeni) informaron que este aumento responde a la carga impositiva correspondiente al cuarto trimestre del 2020. Además no se descarta nuevas suba ya que se espera que el precio de la nafta alcance los $100.

Según consultó UNO Santa Fe, la suba promedio de los combustibles en YPF es de un peso por litros, en Shell, 0,85 centavos y en Axion apenas superó lo 0,10.

**Cómo quedaron los precios en Santa Fe**

_YPF_

•INFINIA: $93,50

• SÚPER: $81,70

• INFINIA DIESEL : $86,70

• DIESEL $74,50

![](https://assets.3dnoticias.com.ar/ypf.jpg)

_SHELL_

• SÚPER: $83

• V-POWER NAFTA: $95,40

• FÓRMULA DIESEL: $77,20

• V-POWER DIESEL: $88,40

![](https://assets.3dnoticias.com.ar/shell.jpg)

_AXION_

• SÚPER: $82

• QUANTIUM: $93,90

• DIESEL: $76,20

• QUANTIUM DIESEL: $87,40

![](https://assets.3dnoticias.com.ar/axion.jpg)