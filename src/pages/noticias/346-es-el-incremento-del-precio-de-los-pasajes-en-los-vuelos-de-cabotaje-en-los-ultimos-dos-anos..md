---
category: Estado Real
date: 2022-01-30T10:29:49-03:00
thumbnail: https://assets.3dnoticias.com.ar/airplane-gc81a7b3ae_1920.png
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3d noticias
resumen: '346% es el incremento del precio de los pasajes en los vuelos de cabotaje
  en los últimos dos años.  '
title: '346% es el incremento del precio de los pasajes en los vuelos de cabotaje
  en los últimos dos años.  '
entradilla: "“En Argentina, la cantidad de viajes en avión por habitante es la cuarta
  parte que la de Colombia y Chile. Fuente @FACVE_ARG”"

---
La cuenta de Twitter Argentina en datos (@arg_endatos), publicó estos datos obtenidos de fuentes oficiales. Así, se puede ver en el gráfico como los precios de los pasajes, tanto internacionales como de cabotaje, en estos últimos dos años, se incrementaron muy por encima de los incrementos de los precios al consumidor. 

“En los últimos dos años, los precios de los vuelos internacionales y a destinos locales aumentaron más que la inflación. Los pasajes al exterior se incrementaron 171% y los de cabotaje 346%, mientras que los precios al consumidor subieron 105%. Gráfico: @BolsaCordoba https://t.co/SuWdlkOYTD”

En lo que respecta a cantidad de pasajeros., en el sitio web [https://www.aviacionline.com](https://www.aviacionline.com, "https://www.aviacionline.com,") se recopilan y comparan las estadísticas del mercado aerocomercial. Entra las principales conclusiones, con respecto a la cantidad de pasajeros, dan cuenta de que “en los primeros cinco meses del año 2019 se movilizaron por vía aérea un total de 12.859.369 pasajeros. De estos, 6.423.283 correspondieron a vuelos internacionales, y los restantes 6.436.086, a vuelos de cabotaje.

Durante el mismo período de 2020, en tanto, transitaron por los aeropuertos de Argentina 6.421.781 pasajeros (3.353.322 cabotaje y 3.068.459 internacionales). En estas cifras ya se vislumbra el impacto del Decreto 297/2020 que estableció desde el 20 de marzo el Aislamiento Social Preventivo y Obligatorio.

El total de tráfico de 2021, por su parte, llegó a 2.233.755 personas. De estos, 517.667 fueron pasajeros de vuelos internacionales, y 1.716.088 de vuelos de cabotaje”.

Los diez aeropuertos que más tráfico de pasajeros perdieron, comparando mes a mes entre 2020 y 2021, son los siguientes: El Palomar (que cesó sus operaciones como aeropuerto comercial en marzo de 2020), Formosa (-98% de tráfico de pasajeros), La Rioja (-95%), Puerto Madryn (-90%), Rosario, que cae 81%, Iguazú (-73%), Aeroparque y Córdoba (ambos perdieron 71% de su tráfico), Paraná (-69%), Bahía Blanca (-67%) y Mar del Plata (-64%). (Fuente: ANAC – Elaboración: Aviacionline.com)