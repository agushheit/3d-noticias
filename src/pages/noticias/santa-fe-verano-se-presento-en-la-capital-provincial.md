---
category: Estado Real
date: 2021-01-22T03:56:43Z
thumbnail: https://assets.3dnoticias.com.ar/turismo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de la provincia de Santa Fe
resumen: "“Santa Fe Verano” se presentó en la capital provincial"
title: "“Santa Fe Verano” se presentó en la capital provincial"
entradilla: Se trata del operativo interministerial que lleva adelante el gobierno
  de la provincia, en distintas localidades.

---
El gobierno de la provincia presentó este miércoles el operativo “Santa Fe Verano” a intendentes, presidentes comunales y representantes de entidades de la ciudad de Santa Fe, Sauce Viejo y de zonas costeras santafesinas.

Se trata del programa multiagencial en el que participan los ministerios de Salud, Seguridad y Cultura, junto con la Secretaría de Turismo y la Agencia de Desarrollo (Ader). El objetivo es brindar información turística de todo el territorio santafesino, cuidar la temporada estival y realizar diferentes actividades. Dado el contexto de pandemia, se realizarán Test de Anosmia y Detectar, como así también el control de las nuevas medidas de restricción a la circulación nocturna.

La propuesta se llevará adelante en Sauce Viejo, sobre la ruta nacional Nº 11 y calle Los Fresnos; en la ciudad de Santa Fe, en las Costaneras Este y Oeste; en la ruta provincial N° 1, a la altura del kilómetro 0,5 y en Cayastá, en la entrada del museo local.

**TRABAJO TRANSVERSAL**

La actividad, que se desarrolló a bordo del Catamarán Costa Litoral, estuvo encabezada por el ministro de Gobierno, Roberto Sukerman, quien destacó que “desde el primer momento de la pandemia, el gobernador Omar Perotti nos pidió que trabajemos de manera trasversal con los distintos ministerios y secretarías avocándonos a volver a la actividad económica. Así lo hicimos con el Ministerio de Trabajo, con una misión que implicaba trabajar en conjunto con los trabajadores, las empresas y las asociaciones. Luchamos en conjunto, en primer lugar porque no se podía volver a la actividad turística y hotelera, y la gastronómica tenía restricciones. Los distintos niveles del Estado trabajaron en conjunto con Nación, con los Municipios y con los legisladores”

“Sabíamos que no había otra forma de avanzar más que con una actividad económica en concreto, porque el Estado puede hacer un acompañamiento y aportar recursos pero la actividad económica genuina era lo que necesitábamos. Estar acá, ver cómo la temporada avanza y se pueden generar actividades para nosotros es trascendente, es un momento histórico porque tenemos la posibilidad de disfrutar de nuestros recursos naturales, de nuestra costa, mostrarlos a los santafesinos y santafesinas y poder crecer”, remarcó el ministro.

En tanto, el senador provincial Marcos Castelló afirmó que “cuando la pandemia empezaba a dar los primeros golpes, era imposible imaginar una temporada como la que estamos dando comienzo y agradezco a todos quienes la hacen posible. Esta pandemia nos pone en la situación de empezar a enamorarnos más de Santa Fe y lo digo porque muchos van a descubrir el turismo interno a partir de ahora. Esto tiene que despertarnos el sentido de pertenencia, hacernos dueños del patrimonio que tenemos. Este paraíso es nuestro, es santafesino”.

**EL TURISMO COMO GENERADOR DE TRABAJO**

Por su parte, el secretario de Turismo, Alejandro Grandinetti, hizo hincapié en la necesidad de “tener un proyecto identitario de la provincia de Santa Fe. Nos cuesta integrarnos y esa es la tarea principal del turismo: enamorarnos de lo nuestro. Necesitamos las herramientas para reconocer el valor que tenemos para desarrollar el sector. En el año tan difícil que pasó, los empresarios mostraron resistencia, no solamente estaba el futuro y la responsabilidad por su propia familia y empresa, sino también mantener la plantilla de cada uno de los trabajadores. Por ello desde el principio el gobernador Omar Perotti pensó en el turismo para generar trabajo, recursos y actividad económica; y tiene la idea de generar obras de infraestructura que nos permitan estar preparados ya que somos una de las provincias más importantes de la Argentina”.

Además, el presidente de Ader, Nicolás Cabo, manifestó que “la agencia está próxima a cumplir 15 años y siempre trabajó en potenciar e impulsar a los sectores productivos. Después de un año tan duro, en particular para el turismo, consideramos que tener una acción directa y exclusiva para esta industria era necesaria”.

“Queremos destacar y agradecer la convocatoria del gobernador Omar Perotti para trabajar junto al gobierno provincial en este operativo. Intervenimos en un sector costero que va desde Sauce Viejo a Cayastá, en cada uno de los puestos trabajamos con la Cámara de Guías de Turismo, quienes hacen un trabajo excepcional. La idea es atender a todos los turistas de nuestra provincia y de otras y, en este contexto de pandemia, trabajamos en programas de prevención y cuidado, por eso agradecemos el acompañamiento del Ministerio de Salud. Celebramos esta articulación público- privada, también que podamos desarrollar acciones con el sector turístico y decirles que queremos sumarnos al trabajo de las distintas cámaras y asociaciones. Tenemos un potencial muy grande en Santa Fe y la Agencia tiene la decisión de trabajar en este sentido”, sintetizó.

**INVERSIÓN PROVINCIAL**

A su turno, el secretario de Producción de la Municipalidad de Santa Fe, Matías Schmuck, expresó que “es muy importante el trabajo de articulación entre los gobierno provincial y municipal con las entidades como Apyme, Centro Comercial y Unión Industrial; todos los pusimos a trabajar desde el minuto cero. Lograr que la provincia invierta en la agencia y el trabajo del Municipio demostró los resultados, lo que permitió que hoy estemos hablando del desarrollo del turismo, y que este trabajo que hace la Agencia se sume a lo que el Gobierno de la provincia y la Municipalidad hacen. Desde un primer momento, cuando surgió esta posibilidad, pusimos a disposición lo que se viene trabajando desde Safetur, que es un ente que vincula lo público con lo privado, articulando acciones que es una prueba que viene funcionando muy bien. Cada acción que se hace desde los distintos lugares es importante para potenciar el turismo y poner a Santa Fe como un destino buscado por quienes tienen ganas de disfrutar de sus vacaciones”.

Participaron, también, el secretario de Desarrollo Territorial y Arraigo, Fabricio Medina; la concejala Jorgelina Mudallel, el presidente del Centro Comercial de Santa Fe, Martín Salemi; el titular de la Asamblea de Pequeños y Medianos Empresarios (Apyme), Mario Galizzi; los presidentes comunales de Sauce Viejo, Pedro Uliambre; de Cayastá, Verónica Devia, y representantes de la comuna de Santa Rosa de Calchines y del Ente Autárquico Municipal Safetur.