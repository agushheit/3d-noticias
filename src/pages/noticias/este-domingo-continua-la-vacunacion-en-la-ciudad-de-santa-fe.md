---
category: La Ciudad
date: 2022-01-23T06:15:10-03:00
thumbnail: https://assets.3dnoticias.com.ar/VACUNACION.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Este domingo continúa la vacunación en la ciudad de Santa Fe
title: Este domingo continúa la vacunación en la ciudad de Santa Fe
entradilla: Se aplicarán terceras dosis en La Redonda y en La Esquina Encendida.

---
Este domingo continuará la vacunación en la ciudad de Santa Fe con la aplicación de terceras dosis.

 Más de 3.400 personas serán inoculadas en la lucha contra el coronavirus. Para La Esquina Encendida fueron dados unos 2.000 turnos entre las 8 y las 11.30.

 Mientras que en La Redonda se espera a 1.432 personas también de 8 a 11.30.

 En Santa Fe se llevan aplicadas 6.855.899 dosis, mientras que en el país 84.614.437.

En total ya se distribuyeron 100 millones de vacunas contra el Covid. 95.809.681 de dosis se entregaron a las 24 jurisdicciones del país y 5.083.000 fueron donadas a países con bajas coberturas de vacunación.

 