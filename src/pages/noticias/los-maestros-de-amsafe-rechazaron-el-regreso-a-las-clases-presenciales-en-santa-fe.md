---
category: Agenda Ciudadana
date: 2020-11-28T13:10:31Z
thumbnail: https://assets.3dnoticias.com.ar/ESCUELAS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Amsafé rechaza regresar a las aulas
title: Los maestros de Amsafé rechazaron el regreso a las clases presenciales en Santa
  Fe
entradilla: A través de un comunicado, sostienen que no están dadas las condiciones
  epidemiológicas, sanitarias ni edilicias para concretar esa posibilidad.

---
La Asociación del Magisterio de Santa Fe (Amsafé) rechazó el regreso a las clases presenciales, previstas por el Ministerio de Educación para la primera quincena de diciembre de este año.
Según la resolución, establecida en el plenario de delegados, sostuvieron que "no están dadas las condiciones epidemiológicas ni sanitarias" para ese retorno a las aulas, al argumentar que aún persiste la circulación comunitaria de coronavirus.

A través de un comunicado, también cuestionaron que las definiciones "fueron tomadas de manera unilateral por parte del gobierno provincial desconociendo los acuerdos paritarios nacionales que hablan de instar al diálogo permanente y la negociación colectiva".

A su vez, agrega: "Esta situación tan compleja puso en valor el rol del docente en esta Escuela Pública que jamás cerró porque llevó alimento y contención y siguió enseñando. Un docente siempre presente, por convicción y compromiso".

También señalaron que "desde el inicio del aislamiento preventivo las y los trabajadores de la educación sostuvimos el vínculo con los y las alumnas y la comunidad, buscando distintas estrategias, con la sobrecarga laboral que ello implica.

También estuvimos de manera presencial en los comedores, copa de leche y acercando materiales impresos, entre otras cosas". Y destacaron que a partir de esa situación "tan compleja" se puso en valor el rol del docente de la escuela pública, que "jamás cerró porque llevó alimento y contención y siguió enseñando. Un docente siempre presente, por convicción y compromiso".

### ![](https://assets.3dnoticias.com.ar/ESCUELAS1.jpg)

## Los puntos salientes de la resolución de Amsafé

Quite de crédito laboral, que consiste en no realizar actividades presenciales con alumnos y alumnas, con los siguientes fundamentos:

* El derecho a la educación se garantiza comenzado por cuidar la salud y la vida de los y las que enseñan y aprenden.
* No están dadas las condiciones epidemiológicas, sanitarias ya que sigue habiendo circulación comunitaria de Covid-19.
* No fueron analizadas las situaciones de las localidades.
* No están garantizadas las condiciones básicas de infraestructura escolar, 12 puntos de acuerdo paritario del año 2011, con adaptación a la situación de pandemia, para el cumplimiento de las normas generales que evitan el contagio de Covid-19.
* No está garantizado el transporte ni las licencias con reemplazantes para los trabajadores pertenecientes a los grupos de riesgo o con niños y niñas a cargo.
* Las definiciones fueron tomadas de manera unilateral por parte del gobierno provincial, desconociendo los acuerdos paritarios nacionales que hablan de instar al diálogo permanente y la negociación colectiva.
* El Ministerio de Educación define un retorno a la presencialidad sin proveer de los elementos mínimos de higiene y seguridad.
* Aún se adeudan las partidas de comedor, copa de leche y gastos de funcionamiento de los meses de octubre y noviembre.
* Continuar con las actividades escolares a distancia, atención de comedor y copa de leche.
* Pago inmediato de comedor, copa de leche y gastos de funcionamiento.
* Defender legal y gremialmente a supervisores, directivos y docentes.
* No volver a la presencialidad hasta tanto no estén dadas las condiciones sanitarias, edilicias y de trabajo.
* Exigir la urgente convocatoria a paritaria docente para discutir salario y condiciones de trabajo.