---
category: La Ciudad
date: 2021-11-14T06:15:52-03:00
thumbnail: https://assets.3dnoticias.com.ar/VECINOS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Arrestos civiles en Santa Fe: ¿cuál es el límite?'
title: 'Arrestos civiles en Santa Fe: ¿cuál es el límite?'
entradilla: Un santafesino referente en materia penal dio su mirada sobre los arrestos
  civiles de vecinos en la vía pública, marcando lo permitido y el "exceso punible"

---
"Vecinos arrestaron a un delincuente": la frase que redunda con cada vez mayor frecuencia en la diaria de Santa Fe. Solo en las últimas dos semanas se registraron al menos cinco episodios de este tipo, en donde santafesinos se unen para actuar ante distintos hechos de inseguridad que vivencian y que no se restringen a un barrio o zona de la ciudad en particular. En este marco, un reconocido abogado penalista santafesino especializado en Criminología dialogó con UNO dando su mirada desde el punto de vista jurídico en torno a cuáles son las facultades, cuáles son las limitaciones y cuáles son las consecuencias para el accionar ciudadano ejerciendo arrestos civiles.

Cabe destacar que el Código Procesal Penal de Santa Fe, en su Artículo 212 cuando habla de "Aprehensión", especifica que "la policía deberá aprehender a quien sorprenda en flagrancia en la comisión de un delito de acción pública. En la misma situación, cualquier persona puede practicar la aprehensión entregando inmediatamente el aprehendido a la policía".

"El requisito es que esa persona sea sorprendida en flagrancia, que sea sorprendida en el momento en el que comete el delito o en el momento en el que se encuentra huyendo del lugar del delito cuando todavía lleve encima cosas que haya robado o pruebas indudables de ese delito", fundamenta la referencia jurídica con respecto a los arrestos civiles.

**Cinco casos en 12 días**

En torno a estas situaciones, el jurista afirmó que "esto no es un mandato que el Código Procesal Penal le da a los civiles sino que es una autorización". Esta autorización a la que hace mención el abogado penalista se puso de manifiesto en los cinco arrestos civiles que sucedieron en Santa Fe en los últimos 12 días.

En plena peatonal de Santa Fe ocurrieron dos de los cinco arrestos civiles, con un intento de robo de una moto en San Martín e Irigoyen Freyre al que un comerciante frenó a los empujones, además de una mujer que fue retenida por una multitud luego de amenazar con un cuchillo y robar en una tienda de ropa, tras descartar el arma a la vista de todos.

Otros arrestos civiles se vivieron en barrio María Selva, cuando vecinos detuvieron a dos violentos que intentaron asaltar a una adolescente de 16 años y en barrio Fomento 9 de Julio, donde el delincuente apresado en el arresto civil logró zafarse y disparó contra quienes lo habían detenido.

No son pocos los casos en los que estas situaciones culminan con una agresión de parte de los vecinos o los damnificados en el acto delictivo, con golpizas, desnudos o linchamientos. Este fue el caso que se vivió hace pocos días en barrio Barranquitas, cuando un grupo de vecinos amenazaban con linchar a un delincuente que fue atrapado luego de intentar robar una motocicleta a punta de cuchillo.

**Uso de la fuerza y el "exceso punible"**

En esta línea, el catedrático consultado puntualizó que "cuando se habla de la fuerza tiene que haber un cuidado". Y sobre esto completó: "Debe ejercer la fuerza solo para arrestarlo, la fuerza mínima e imprescindible para detenerlo y nada más, de la misma manera que un policía. Un agente policial no está autorizado para pegarle a cualquier persona que arresta sino usarla solo para reducirlo y detenerlo, lo mismo que para un civil".

Frente a los arrestos civiles con reacciones violentas, el referente jurídico indicó: "Ha habido casos en los cuales vecinos han arrestado a alguien, le han pegado, desnudado y en el peor de los casos lo han matado, esos son excesos y pueden tener consecuencias penales de la misma manera que un policía que se excede y nos pega a cualquiera de nosotros cuando nos está deteniendo. Si los vecinos hacen eso con nosotros aunque estemos actuando en flagrancia cometen un exceso y el exceso es punible".

"Una vez arrestado se lo debe entregar de forma inmediata a la policía y ahí tiene que actuar el Estado para evaluar si se lo condena o no", agregó el jurista. Y agregó: "Incluso en estos casos hay que tener muchísimo cuidado: puede haber un error en la identificación del delincuente, deteniendo a una persona porque alguien dijo “aquel es el chorro” y esto ha ocurrido. Hubo personas que fueron perseguidas y no eran los delincuentes", concluyó el reconocido penalista santafesino.