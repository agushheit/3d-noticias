---
category: Agenda Ciudadana
date: 2021-04-12T06:00:55-03:00
thumbnail: https://assets.3dnoticias.com.ar/carla-vizzotti.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Vizzotti: "Ahora tenemos la vacuna, no habrá un aislamiento como el año
  pasado"'
title: 'Vizzotti: "Ahora tenemos la vacuna, no habrá un aislamiento como el año pasado"'
entradilla: La ministra de Salud dijo que la segunda ola encuentra al país "con un
  sistema sanitario más robusto y con el equipo mejor preparado".

---
La ministra de Salud, Carla Vizzotti, afirmó que la segunda ola de coronavirus “nos encuentra con un sistema sanitario más robusto, con el equipo mejor preparado” y destacó que ahora “estamos en una situación distinta a la de marzo del año pasado porque tenemos la vacuna” por lo que “no habrá un ASPO (Aislamiento Social Preventivo y Obligatorio) como el año pasado”.

“Los expertos dicen que las segundas olas son más importantes que las primeras y hubo sistemas de salud en el mundo que han sido desbordados. A nosotros nos toca con un sistema más robusto, con el equipo mejor preparado y con un personal más cansado, aunque el 90 por ciento tiene la primera dosis y el 60% la segunda” de la vacuna, dijo Vizzotti. La ministra añadió que “no habrá un ASPO como el año pasado, ahora tenemos la vacuna”.

La funcionaria destacó que con el sistema de vacunación en marcha “vamos a tener protegida a la población de riesgo” e insistió con la necesidad de tomar conciencia de la responsabilidad individual en los cuidados higiénicos al advertir que “no necesitar medidas extra va a depender de cómo nos vaya en estas tres semanas”.

Vizzotti destacó que en esta segunda ola se han tomado “medidas los más específicas posibles para atrasar los contagios” y afectando al mínimo posible la economía. “El mundo esperaba más dosis de vacunas” pero “el paso de la teoría a la práctica fue más dificultoso de lo que se esperaba”, dijo Vizzzotti y añadió que hubo “dificultades en la producción, no es un problema de Argentina, sino del mundo”.

Tras afirmar que a medida que llegan las vacunas, “se aplican bien y rápido”, detalló: “Tenemos la expectativa de poder recibir varios millones durante abril, hay 2 millones de Sinopharm que tienen que llegar, 900 mil de AstraZeneca y tenemos que seguir recibiendo de Rusia, y contamos con el mecanismo Covax donde tenemos unos 9 millones de dosis” programadas.

Consideró que en este momento de la pandemia “la fuente de infección ahora son algunas situaciones sociales” y remarcó que con las nuevas medidas de restricción de circulación nocturna “se busca minimizar los riesgos”. Para Vizzotti “es imposible no tener una mirada subjetiva y no enojarse con que no llegan tantas vacunas como nos gustaría, el presidente (Alberto Fernández) mismo se enoja por eso y nos pide que lleguen más vacunas” pero insistió con que el problema del faltante es mundial.

“Hay una pandemia, están aumentando los casos y se está muriendo la gente”, remarcó Vizzotti y afirmó que ante ello “tanto el presidente como cada ministro estamos trabajando para minimizar el impacto de la pandemia, hablamos con cada ministro de las 24 jurisdicciones” del país.