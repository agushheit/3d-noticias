---
category: La Ciudad
date: 2021-01-30T09:29:10Z
thumbnail: https://assets.3dnoticias.com.ar/frana.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: La provincia finalizó la apertura de ofertas para la construcción de 220
  viviendas en la ciudad de Santa Fe
title: La provincia finalizó la apertura de ofertas para la construcción de 220 viviendas
  en la ciudad de Santa Fe
entradilla: Este viernes se abrieron los sobres de la tercera licitación, que comprende
  la ejecución de 72 unidades habitacionales.

---
El Gobierno de la provincia de Santa Fe, a través del Ministerio de Infraestructura, Servicios Públicos y Hábitat, abrió los sobres con las ofertas para la construcción de 72 viviendas, las que forman parte de de las 220 unidades habitacionales, que se construirán en el loteo Esmeralda Este II de la ciudad de Santa Fe, en las que se invertirá en total $ 750 millones.

El acto estuvo encabezado por el Director Provincial de Vivienda y Urbanismo, José Kerz, que indicó que "luego de un año muy complicado, iniciamos éste 2021 con obras muy importantes, como este grupo de licitaciones. Son todas viviendas individuales, tipología de dúplex y otras adaptadas para discapacitados motrices desarrolladas en planta baja. Todas van a contar con la infraestructura básica, es decir, con las instalaciones de agua, luz, cloaca y gas".

**LAS OFERTAS**

Para la construcción de 72 viviendas, que cuenta con un presupuesto oficial de $ 247.127.575,32,  se presentaron 14 oferentes. La UT conformada por Mundo Construcción SA y Capitel SA cotizó por $ 240.442.090,20; Pirámide SA, por $ 266.443.871,76; Pecam SA, por $ 279.250.026,84; Cocyar SA, por $ 283.297.665,24; Edeca SA, por ofertó $269.360.446,68; Sanimat SRL, por $ 248.342.698,44; Pilatti SA, por $ 237.266.566,91; Coirini SA, por $ 210.307.680,36; Coemyc SA, por $ 236.222.907,12; VFM SA, por $ 263.000.000,24; EPRECO SRL, por $ 244.456.183,08; la UT conformada por Constructora Comercial Arq. Juan Manuel Campana y Arq. Mario E. Iglesias Construcciones ofertó $ 247.265.182,87; Tecsa SA, por $ 246.984.276,60; y la última oferta de la firma Dinale SA, por $ 299.766.229,74.

Posteriormente, se abrieron los sobres para licitar el reacondicionamiento de la infraestructura básica y la ejecución de la red cloacal, con un presupuesto oficial de $ 43.687.876,66.

Las empresas oferentes fueron Mem Ingeniería SA, que cotizó por $ 38.896.261,61; Pirámide Constructora SA, por $ 55.000.269,53; Edeca SA, por $ 49.894.915,52; Alamco SA, por $ 63.795.650; Efe Construcciones de Carlos A. Fierro, por $ 48.400.000; Dinale SA, por $ 53.445.877,57; Pilatti SA, por $ 45.194.072,62; Coirini SA, por $ 40.309.019,59; Brumont SA, por $ 28.874.733,74+IVA; Coemyc, por $ 49.434.398,24; la UT conformada por Orion Ingeniería SRL y MT SRL, por $ 55.690.000; y la última oferta fue de la firma Winkelmann SRL, por $ 43.889.102,60.

**LAS OBRAS**

Las viviendas se desarrollarán en dúplex y las adaptadas para discapacitados motrices desarrolladas en planta baja. Los prototipos varían de acuerdo al ancho del terreno de emplazamiento, proyectándose en 3.60m, 4,00m y 5,00, según las características propias de la manzana.

Las viviendas tipo dúplex constan en planta baja de estar comedor, cocina con mesada, lavadero y baño; en planta alta 2 dormitorios y baño completo. poseen aberturas de aluminio exterior y de madera interior, cielorrasos de placa de roca de yeso y termotanque solar. Se entregan totalmente terminadas para ser habitadas, pintadas y con todas las instalaciones de los servicios de agua, luz, cloaca y gas en funcionamiento.

También se incluye el prototipo VCD de 2 dormitorios, que está destinado para discapacitados motrices, que se desarrolla íntegramente en planta baja cumpliendo con todos los requerimientos para este fin.