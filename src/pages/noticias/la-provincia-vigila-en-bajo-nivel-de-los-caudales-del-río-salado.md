---
layout: Noticia con imagen
author: 3DNoticias
resumen: Río Salado
category: Agenda Ciudadana
title: La provincia vigila en bajo nivel de los caudales del Río Salado
entradilla: De persistir la escasez de precipitaciones, el aumento de
  temperaturas y la disminución de caudales desde aguas arriba, podría
  producirse el agotamiento de la reserva de agua para ser tratada en Tostado.
date: 2020-11-26T13:43:32.673Z
thumbnail: https://assets.3dnoticias.com.ar/baja-rio.jpeg
---
Desde la Secretaría de Recursos Hídricos del Ministerio de Infraestructura, Servicios Públicos y Hábitat se emitió un informe acerca del estado de situación del río Salado, en el tramo Colonia Dora (provincia de Santiago del Estero) y Tostado (provincia de Santa Fe), donde se advierte que de continuar la escasez de precipitaciones, el aumento de las temperaturas y la disminución de caudales en el tramo del río Salado comprendido entre el Azud “La Niveladora” -situado en la zona de Colonia Dora- y la sección hidrométrica de la ruta Nacional N°95 -situado aguas abajo del Azud Nivelador de Tostado-, podría tornarse crítico el acceso al agua en dicha ciudad.

En este sentido, el Secretario de Recursos Hídricos, Roberto Gioria explicó que "necesitamos un determinado caudal en el límite interprovincial y en este momento su cantidad ha descendido. De persistir este estado, vamos a entrar en una situación compleja, en la que todavía no estamos", aclaró. Y continuó: "No nos están cumpliendo las provincias de aguas arriba, que son Salta y Santiago del Estero, con las pautas de provisión de caudal del río Salado".

Asimismo, Gioria informó que “hasta hace unas semanas, la escasez de caudales propios del río Salado en el tramo aguas abajo de la ciudad de Santiago del Estero, era compensada con el aporte de agua desde la cuenca del río Dulce, a través del canal Jume Esquina. Pero en la última reunión del Comité Interjurisdiccional, celebrada hace unos quince días, la provincia de Santiago del Estero anunció el cese de los aportes del canal Jume Esquina, debido a la escasez de agua en su región. Mientras que la provincia de Salta, se comprometió a aumentar los aportes hacia aguas abajo, incrementando el caudal en el límite interprovincial con Santiago del Estero, en 5 m3/s”.

![](https://assets.3dnoticias.com.ar/rio-salado-1.jpg)

## **INFORME TÉCNICO**

En relación a esto, la Secretaría de Infraestructura y Política Hídrica – Subsecretaría de Obras Hidráulicas de la Nación realizó, entre el 10 y el 13 de noviembre, una campaña de mediciones de caudales en numerosos sitios de interés de las tres provincias involucradas. Es así que se constató un aumento de los caudales en el límite de Salta y Santiago del Estero, pero también se verificó una importante reducción de caudales en el tramo del río Salado, comprendido entre el azud "La Niveladora" y Tostado.

Ante esta situación, el 19 y 20 del corriente, la Secretaría de Recursos Hídricos de la provincia realizó una campaña de aforos y de verificación de alturas hidrométricas del río Salado en los tramos mencionados. Donde se verificaron bajos caudales provenientes de Colonia Dora (1,80 m3/s), agravándose la situación hacia aguas abajo. Tan es así que en Malbrán (provincia de Santiago del Estero), se midieron solamente 0,35 m3/s. Y en el límite interprovincial entre Santiago del Estero y Santa Fe se registraron caudales de 1,02 m³/s, y de 0,68 m3/s aguas abajo del Azud de Tostado.

Como consecuencia de los resultados obtenidos, el 20 de noviembre se le solicitó al Comité Interjurisdiccional de la Cuenca del Río Juramento – Salado, una reunión con carácter de “urgente” para analizar cómo se revierte esta situación, ya que se necesita asegurar el ingreso de agua del río Salado a la provincia, que garantice el abastecimiento de agua potable y la conservación del ecosistema del río.

De acuerdo a la cantidad de agua que ingresa actualmente, se hará necesario demandar que las presas de aguas arriba eroguen mayores caudales y que haya un manejo de las derivaciones que permitan que escurran mayores caudales hacia Santa Fe. Mientras tanto, la Provincia implementó un Estado de Vigilancia Permanente de Caudales.