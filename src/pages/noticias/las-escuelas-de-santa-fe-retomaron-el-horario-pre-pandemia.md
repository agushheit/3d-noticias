---
category: Agenda Ciudadana
date: 2021-08-03T06:15:09-03:00
thumbnail: https://assets.3dnoticias.com.ar/CLASES.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Las escuelas de Santa Fe retomaron el horario pre-pandemia
title: Las escuelas de Santa Fe retomaron el horario pre-pandemia
entradilla: 'Luego del receso de invierno, comienza el nuevo formato dispuesto para
  el sistema educativo provincial, que recupera la jornada simple completa: una hora
  más en inicial y primario y dos para secundaria. '

---
Luego de haber regresado la semana pasada a las aulas después de las vacaciones de invierno, todos los niveles educativos de la provincia comenzaron desde este lunes a aplicar el nuevo formato dispuesto por el ministerio de Educación, con el objetivo de extender la presencialidad durante lo que resta de este año.

En la medida que las condiciones sanitarias lo permitan, las escuelas santafesinas dictarán desde agosto la jornada simple completa, tal como lo hacían antes de la pandemia. Esto significa más tiempo de clases dentro de los establecimientos para alumnos y alumnas, aunque continuarán asistiendo bajo el sistema de burbujas con bimodalidad, alternando entre actividades presenciales y trabajos a distancia.

“Todo lo aprendido en el primer semestre en términos de organización escolar y modos de gestionar las propuestas curriculares priorizadas para cada nivel y modalidad, será la base de este nuevo formato que gradualmente avanza en la procura de mejores oportunidades de escolarización para todos los estudiantes santafesinos en este tiempo excepcional que vamos transitando”, explicó la cartera educativa santafesina en la circular n°20, emitida el pasado 22 de julio.

**Adaptación de las escuelas**

Para poder adaptarse a la ampliación de los horarios y a la vez seguir cumpliendo con los protocolos por la pandemia, las escuelas debieron redoblar sus esfuerzos. Por ello, la primera semana fue de un fuerte trabajo previo de reacondicionamiento de espacios, organización de las currículas y horarios de docentes y personal de maestranza. Una vez coordinadas el nuevo formato, las autoridades de cada establecimiento deben enviar comunicaciones a las familias para anunciar los cambios de horarios.

“Prolongar el tiempo de permanencia en la escuela es muy importante. Vamos logrando más horas en la jornada escolar diaria y más tiempo extendido en los acompañamientos necesarios para asegurar para todas y todos los estudiantes, mayores posibilidades de garantizar los aprendizajes prioritarios”, sintetiza el ministerio en la circular sobre el espíritu del nuevo formato.

El impacto concreto de esta modificación será de una hora cátedra más en los niveles inicial y primario y dos horas cátedra más en lo que es educación secundaria.

De este modo, la jornada para este segundo semestre en la primaria tendrá tres bloques horarios de 70 minutos y dos espacios de recreación, para ventilar aulas, de 15 minutos cada uno. Mientras que en la secundaria se impartirán tres bloques horarios de 70 minutos y uno de 35 minutos, alternados con recreos para descanso y ventilación.

En la primera parte del día, se considerarán ejes que se irán trabajando en la jornada y que, luego, se cerrarán en el último tramo con actividades desde la percepción de los mismos alumnos sobre lo aprendido.

**EL 100%**

La cartera habilitó a unas 600 escuelas rurales y semirurales de la provincia a que puedan tener presencialidad total. Además, dispuso que puedan funcionar sin bimodalidad los establecimientos que cuenten con una matrícula reducida y con una infraestructura que garantice la implementación de los protocolos sanitarios.

**Piden que sea completa**

La agrupación “Padres Organizados” insiste por la vuelta a la presencialidad completa en las aulas de la provincia.

"Con algunos minutos más de clases se pretende demostrar avance cuando en realidad la presencialidad plena sigue siendo postergada", sostuvieron al enviar una nota a senadores provinciales para que intercedan ante el pedido de audiencia con el Gobernador y la ministra de Educación.

Además, solicitan que se rediseñen los protocolos y que se tome a todo el grado como una burbuja única, ya que muchos alumnos no pueden acceder a la virtualidad para cumplir con sus deberes escolares.