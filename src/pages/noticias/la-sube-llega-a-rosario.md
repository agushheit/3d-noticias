---
category: Agenda Ciudadana
date: 2022-12-07T07:56:45-03:00
thumbnail: https://assets.3dnoticias.com.ar/sube.png
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: La SUBE llega a  Rosario.
title: La SUBE llega a  Rosario.
entradilla: 'Firmarán un convenio a partir del cual se genera una mesa de trabajo
  para avanzar en el proceso de implementación '

---
El próximo miércoles 14 de diciembre en la ciudad de Rosario, el Ministro de Transporte de la Nación, Diego Giuliano, junto al intendente local, Pablo Javkin, **_firmarán el convenio a partir del cual se genera una mesa de trabajo para avanzar en el proceso de implementación_** del sistema SUBE en la ciudad, donde se verán beneficiados más de 1 millón de usuarios y usuarias del transporte público.

Desde la red social Twitter, Pablo Javkin comentó la noticia: “Buena noticia. La llegada de la SUBE es un paso muy importante para la recuperación del Sistema de Transporte, tan necesario para nuestros vecinos. ¡El camino siempre es con más federalismo y más igualdad!”.

La futura llegada del Sistema Único de Boleto Electrónico (SUBE) a la ciudad de Rosario, responde a la decisión de expandir la política pública federal de la cartera que dirige Diego Giuliano como parte del Plan de Modernización del Transporte, para que los ciudadanos y ciudadanas de todo el país viaje de una manera más ágil, cómoda y moderna.

Vale subrayar que la ciudad de Rosario cuenta con 720 unidades de colectivos, de las cuales 31 son trolebuses. En ese sentido, la cantidad de unidades de gestión SUBE y terminales automáticas (TAS), en esta instancia se trabajarán con el municipio para establecer cuál es la necesidad para satisfacer la demanda de las 1.017.415 usuarios y usuarias que se verán beneficiadas en la utilización del transporte público.

Al respecto, el ministro Diego Giuliano dijo: “La llegada del Sistema Único de Boleto Electrónico a la ciudad de Rosario, responde a la decisión de expandir esta política federal del Ministerio de Transporte”

“La SUBE posibilita que las y los ciudadanos accedan al atributo social y a un sistema moderno y trasparente”, afirmó.

Vale recordar que a través de SUBE, el Estado Nacional otorga el beneficio para viajar con 55% de descuento en el transporte público incluye a jubilados, pensionados, excombatientes y trabajadoras domésticas, entre otros.