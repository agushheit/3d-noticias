---
category: La Ciudad
date: 2021-06-27T06:00:25-03:00
thumbnail: https://assets.3dnoticias.com.ar/wifi.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Quieren que las viejas cabinas telefónicas ofrezcan wifi y permitan la recarga
  de celulares
title: Quieren que las viejas cabinas telefónicas ofrezcan wifi y permitan la recarga
  de celulares
entradilla: Estiman que en la ciudad hay alrededor de cien. El Concejo aprobó una
  ordenanza para reconvertirlas en puntos de información digital y que permitan recargar
  bicicletas eléctricas.

---
Este jueves el Concejo Municipal aprobó un proyecto de ordenanza para el reciclamiento o reconversión de las aproximadamente cien cabinas de telefonía pública existentes en la ciudad, hoy en desuso; para otros usos, gratuitos u onerosos.

La iniciativa destaca que, según sea el nuevo marco jurídico que se establezca, el servicio podría ser mediante administración municipal o concesionados.

El proyecto prevé que el Departamento Ejecutivo Municipal, a través de la Secretaría que corresponda, elabore un estudio de factibilidad técnica y económica para llevar adelante la reconversión.

La idea, autoría de la concejala Laura Spina (UCR-FCyS), es adaptarlas a las necesidades actuales y darles distintas utilidades como puntos de recarga limpios o ecológicos para celulares, puntos de acceso wifi públicos, centros de información digital avanzados; puntos de recarga de vehículos eléctricos.

También para que funcionen como una herramienta de comunicación en emergencia para los barrios que todavía tienen mala conectividad a la red de telefonía.

Además, se plantean que funcionen como puntos para obtener agua caliente para el mate por medio de paneles solares sobre la cabina; bibliotecas y videotecas de donación comunitaria, para que los vecinos puedan dejar libros o videos, entre otros que sean propuestos por el Ejecutivo Municipal.

“Al tener conectividad y electricidad se podría dar wifi gratuito, sobre todo para uso escolar, que tanta falta hace. También servirían para su reconversión para suministro de bicicletas eléctricas. Otra posibilidad es dotar a éstas de energías renovables como la solar” explicó Spina.

“Otros municipios ya lo han hecho, tanto en Argentina como en otras partes del mundo, de allí surgen las sugerencias sobre posibles usos de las cabinas” afirmó.

“En estos temas hay que ver lo que ha hecho el resto del mundo” aseguró la concejala y puntualizó: “Algunos países como Estonia, República Checa o Finlandia hace mucho que optaron por retirar las cabinas telefónicas y otros, como por ejemplo Francia, han decidido recientemente hacer lo propio y excluir las cabinas del servicio universal, lo que permitirá empezar a desinstalarlas.”

“Otros como Estados Unidos o Reino Unido han decidido reemplazar las clásicas cabinas por estaciones wifi con una conexión a Internet gratuita. Incluso hay lugares donde reciclaron las clásicas cabinas para convertirlas en, por ejemplo, estaciones para enviar emails, SMS, para recargar el saldo de los teléfonos móviles, para cargar la batería de los vehículos eléctricos en incluso los hay quienes las han convertido en pequeñas bibliotecas de acceso público para donar libros y CD’s de música o películas, esto último puede verse en París, por ejemplo” agregó.

“En definitiva, la mayoría de países han modificado los decretos que les obligaban a asegurar cabinas telefónicas en las ciudades y han optado por ofrecer puntos wifi públicos y estaciones wifi, para que tanto los turistas como los habitantes de las regiones puedan realizar llamadas por Internet o consultar cualquier dato cuando más lo necesiten” finalizó.

Para finalizar, aclaró: “En general pertenecen a la empresa Telecom, como telefónica incumbente en la ciudad y región, por lo cual será necesario consensuar el destino futuro”.