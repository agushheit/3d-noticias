---
category: Agenda Ciudadana
date: 2021-09-07T06:00:19-03:00
thumbnail: https://assets.3dnoticias.com.ar/CANSINO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Nueva etapa del plan de inmunización e implementación de la acreditación
  de vacunas
title: Nueva etapa del plan de inmunización e implementación de la acreditación de
  vacunas
entradilla: A partir de la llegada de unas 200 mil de Cansino este martes, destinadas
  a poblaciones de difícil acceso, el Gobierno nacional continúa la campaña de vacunación.

---
El Gobierno nacional puso en marcha una nueva etapa del plan estratégico de vacunación, a partir de la llegada de unas 200 mil monodosis contra el coronavirus de Cansino destinadas a personas en situación de calle, migrantes, refugiados y otros colectivos y con la implementación de la credencial digital Mi Argentina, un documento que acredita en el ámbito internacional de la aplicación de la vacuna.

Por un lado, más de 200 mil vacunas del laboratorio CanSino, que se administran en una sola dosis y serán destinadas a poblaciones de difícil acceso, llegarán este martes al país para sumarse a la campaña de vacunación que el Gobierno despliega en todo el país y que, con este nuevo cargamento, alcanzará las 57.407.120 dosis recibidas desde el inicio de la pandemia.

El embarque arribará a las 2.45 al aeropuerto Internacional de Ezeiza, en el vuelo QR8157 de la compañía Qatar Airways.

Con estas dosis, el Gobierno apuesta a vacunar a poblaciones de "difícil acceso", entre ellas personas en situación de calle, migrantes, refugiados y otros colectivos dispersos.

Con esa meta fueron reservadas las vacunas del laboratorio CanSino, que, al administrarse en una sola dosis, logran reducir las barreras logísticas.

El arribo de este martes se sumará a los 1.736.000 sueros de Sinopharm que llegaron el sábado último desde Pekín, en el vuelo LH8264 de la línea aérea Lufthansa Cargo.

De este modo, en sólo una semana Argentina contará con 6.838.500 vacunas: 200 mil de Cansino, 4.984.000 de la firma estatal Corporación Grupo Farmacéutico Nacional Chino y 1.654.500 de AstraZeneca.

Del total de 57.407.120 dosis que el país adquirió desde el inicio de la campaña, 15.000.920 son Sputnik V: 10.955.260 del primer componente (y de ellas 1.179.625 producidas por Richmond) y 4.045.660 del segundo componente (702.500 del laboratorio argentino).

De la firma AstraZeneca, el país ya cuenta con 15.594.200 vacunas: 580.000 de Covishield; 1.944.000 recibidas por el mecanismo Covax; 11.859.200 provistas directamente por el laboratorio; 400.000 donadas por España y 811.000 por México.

En tanto, 22.912.000 son del laboratorio Sinopharm, 200.000 de Cansino y 3.500.000 de Moderna, donadas por EE.UU.

De acuerdo con el Monitor Público de Vacunación, hasta este lunes por la mañana se distribuyeron 51.604.874 dosis en todo el territorio, al tiempo que las aplicaciones totalizan 44.731.271.

A su vez, 28.356.254 personas recibieron la primera dosis y 16.375.017 ya cuentan con dos aplicaciones.

Por su parte, la credencial digital Mi Argentina funcionará desde este lunes como documento oficial de acreditación en el ámbito internacional de la aplicación de la vacuna contra el coronavirus, informó el Gobierno.

Así será comunicado a las cancillerías de los países, a direcciones de migraciones y a autoridades de frontera, reportó el Ministerio de Salud.

Además, se solicitó a los países que envíen sus modelos de credencial para verificar la documentación de las personas que deseen ingresar al país.

Mi Argentina está disponible para todas aquellas personas que se hayan vacunado, ya que se genera a partir de los datos cargados en el Registro Federal de Vacunación Nominalizado (Nomivac).

Según indicaron, el carnet digital está disponible en español y en inglés con el objetivo de ser presentado en el exterior por aquellas personas que deban desplazarse entre países.

La implementación de la credencial digital internacional fue informada por el Ministerio de Relaciones Exteriores, Comercio Internacional y Culto mediante una circular enviada a consulados y embajadas argentinas en el exterior.

Fue desarrollada por la Secretaría de Innovación Pública de la Jefatura de Gabinete de Ministros y presentada en julio pasado.

Tras un trabajo conjunto entre varios ministerios, la Dirección de Migraciones, la Administración Nacional de Aviación Civil (ANAC) y Cancillería, se desarrolló el carnet digital bilingüe con un código QR que remite al sitio oficial de la cartera sanitaria nacional, donde se accede a la información respaldatoria de la vacunación contra la Covid-19.

En tanto, la mitad de la población mayor de 18 años en la Argentina completó su esquema de vacunación contra el coronavirus en lo que va de septiembre, que "será el mes dedicado a intensificar la aplicación de la segunda dosis, al igual que agosto", subrayó este lunes el Ministerio de Salud.

A su vez, la cartera detalló que también completaron su esquema de vacunación contra el coronavirus el 75% de los mayores de 50, el 80,6% de las personas con 60 años o más y el 82% de los mayores de 70.

Por otra parte, reportó que el 61,9% de la población inició su esquema de vacunación.

El registro oficial de personas vacunadas con una dosis por edad es el siguiente: 85,6% de la población mayor de 18 años; 91,1% del grupo entre 40 y 44 años; 88,4% de quienes tienen entre 45 y 49 años; 91,9% de las personas con 50 años o más; 90,6% de los de entre 50 y 54 años; 92,5% de las personas de entre 55 y 59 años; y 92,2% de quienes tienen 60 años o más.