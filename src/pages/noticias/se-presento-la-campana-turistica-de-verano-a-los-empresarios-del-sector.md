---
category: La Ciudad
date: 2020-12-17T11:13:54Z
thumbnail: https://assets.3dnoticias.com.ar/1712-jaton.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Se presentó la campaña turística de verano a los empresarios del sector
title: Se presentó la campaña turística de verano a los empresarios del sector
entradilla: Nueve audioguías a través de Spotify permitirán descubrir la ciudad; y
  además habrá una extensa propuesta deportiva, cultural, recreativa y gastronómica.

---
En las instalaciones de Piedras Blancas y con el Puente Colgante de fondo, el intendente Emilio Jatón junto al secretario de Integración y Economía Social, Mariano Granato y el director de Turismo, Franco Arone, dieron detalles de la campaña turística de verano a los empresarios del sector que prestan sus servicios en la ciudad.

“Estamos cumpliendo con algo que los empresarios del sector estaban pidiendo desde hace mucho tiempo que era juntarnos y ver qué hacíamos con el turismo en Santa Fe porque teníamos un evento por año y veíamos cómo se iba la gente. Hoy tenemos un plan, elaborado a través del **Safetur**, y me parece que ese es el lugar donde tenemos que discutir, planificar y pensar qué hacer con la ciudad”, manifestó en primer lugar el mandatario local.

En este marco, se detalló qué son las audioguías, un sistema novedoso que desde la web oficial de la Municipalidad y desde el canal de Spotify permite acceder a distintos recorridos turísticos. También se anticipó que durante enero, febrero y marzo se harán paseos guiados y gratuitos de “Mi ciudad como turista” por atractivos culturales, históricos y naturales, sobre todo para las y los santafesinos.

También se ofrecerán paseos y actividades náuticas en kayaks, kitesurf, catamarán, lanchas, paddle surf; circuitos gastronómicos vinculados a la cerveza y a la gastronomía de río; recorridos excepcionales por el Museo de la Constitución y la Manzana Jesuítica; ferias de artesanías y emprendedores al aire libre; paseos y centros comerciales; entre otras opciones.

“Hoy estamos llegando a un lugar que los empresarios estaban pidiendo y lo hemos construido juntos, lo importante es eso. Ahora tenemos que mostrarlo a la región porque sabemos que vamos a tener turismo interno, vamos a tener el doble de gente en enero y febrero y nos preparamos para eso”, agregó Jatón.

# **Llegar a la región**

Para impulsar esta campaña, se armó un **material específico para difundir en la región, en ciudades como Esperanza, San Carlos y Rafaela, entre otras**, “porque sabemos que ellos a veces vienen para acá y no tienen opciones. Lo que vamos a hacer es trabajar con todo el equipo de turismo y el Safetur para que toda la zona pueda llegar a Santa Fe y empezar a disfrutar de una ciudad distinta”, dijo Jatón.

Luego, contó: “Hemos evolucionado junto a la tecnología porque vamos a presentar a través de un llavero y Spotify nueve audioguía para conocer la ciudad, y este es un avance importante porque Buenos Aires por ejemplo no lo tiene. A los hoteles y al casino les vamos a entregar unos acrílicos para que el turista pueda acceder a toda la información desde el celular o cualquier dispositivo móvil”.

Antes de finalizar, manifestó: “Hoy Santa Fe está muy bonita y el espíritu es que nos sintamos orgullosos de la ciudad que tenemos. No nos olvidemos del santafesino que no conoce Santa Fe y por eso la idea es trabajar hacia adentro para que se transformen en embajadores de la ciudad. Para quererla la tenemos que conocer”.

***

# ![](https://assets.3dnoticias.com.ar/1712-jaton1.jpg)

# **Propuesta novedosa**

Por su parte, Arone contó: “Las audioguías son innovadoras porque permiten romper las barreras físicas y corporales que por ahí impiden disfrutar de la ciudad. En total son nueve y son distintos lugares de la ciudad que tienen mucha historia, valor patrimonial, sentimiento e impronta santafesina y que pudimos desarrollar para la web y Spotify para que cada establecimiento turístico lo tenga y cada santafesino pueda acceder desde el celular y vivir Santa Fe”.

Las audioguías llevan al oyente “de paseo” por el Casco histórico; Museos y lugares históricos; Paseo Boulevard; Paseo Costanera; Peatonal San Martín; Paseo Manzana Jesuítica; Paseo del Puerto; Paseo Cementerio; y Camino de la Constitución. Estos circuitos fueron diseñados para que se puedan hacer caminando o en bicicleta. Ingresando en cada propuesta está el tiempo de duración, y la cantidad de cuadras que incluye, un pequeño mapa de referencia y los audios de cada uno de los atractivos.

En el repaso de las propuestas, Arone aprovechó para contar que “hoy Santa Fe es parte de algo que se está gestando a nivel nacional que es el camino de los jesuitas”. Vale recordar que luego de las gestiones ante el ministerio de Turismo de la Nación por parte del intendente Jatón, Santa Fe va a aparecer en el mapa de Argentina y el mundo con la experiencia santafesina sobre la misión en la ciudad.

Por otro lado, el director de Turismo contó que empresas de viajes diseñaron propuestas para Santa Fe. “También como lo pidió el intendente y en el marco del Safetur, hemos construido experiencias turísticas en coordinación con las agencias de turismo. Son cuatro las que armaron paquetes turísticos desde la ciudad de Santa Fe. Hay uno para los propios santafesinos que se llama ‘Santafesinos tours’ que a partir de las audioguías se animaron a armar propuestas para recorrer la ciudad. También hay paseos náuticos, al casco histórico, circuitos gastronómicos, entre otros”, enumeró Arone.

Por último recordó que **los centros de información turística fueron refuncionalizados y que siguen siendo tres: el de la Peatonal San Martín, el del Puerto y el de la terminal de ómnibus**. “Ya están funcionando con atención presencial, con todas las medidas de seguridad, incorporamos micrófonos para evitar contacto, con una nueva estética y todas las herramientas que hacen que el turista pueda acceder a todas esta información”, cerró Arone.