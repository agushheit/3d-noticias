---
category: Agenda Ciudadana
date: 2021-07-17T06:15:28-03:00
thumbnail: https://assets.3dnoticias.com.ar/VACUNASCOMING5.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: La Argentina superará este viernes los 36 millones de vacunas recibidas
title: La Argentina superará este viernes los 36 millones de vacunas recibidas
entradilla: En la madrugada de este sábado llegarán al país 3,5 millones de la empresa
  Moderna, donadas por Estados Unidos, que se suman a las 768.000 que arribaron esta
  tarde en vuelo de Aerolíneas proveniente de China.

---
La Argentina superará los 36 millones de dosis de vacunas contra el coronavirus recibidas desde el inicio del plan de inmunización, con las 768 mil dosis de Sinopharm que llegaron desde China y los 3,5 millones de dosis de Moderna donadas por los Estados Unidos, que arribarán antes de la medianoche.

El Airbus 330-200, matrícula LV-FVI, aterrizó en el aeropuerto internacional de Ezeiza a las 17.30 luego de haber partido el jueves a la tarde desde Beijing y tras haber realizado una escala técnica en el aeropuerto de Barajas, Madrid, para reabastecimiento de combustible.

Se trata del sexto de los diez viajes previstos para julio hasta completar los ocho millones de dosis, en el marco del contrato suscripto con el China National Pharmaceutical Group Corp por un total de 24 millones de vacunas hasta septiembre.

El jueves llegó el quinto de esos vuelos, proveniente de Beijing, también con 768.000 dosis de Sinopharm.

En tanto, las dosis desarrolladas por la empresa estadounidense de biotecnología Moderna llegarán esta noche al país en dos vuelos de Aerolíneas Argentinas, enviadas por el Gobierno de Estados Unidos como parte de las donaciones que realiza la administración del presidente Joe Biden a países en desarrollo, según informó la embajada norteamericana en Argentina.

El presidente Alberto Fernández celebró la donación y agradeció a su par estadounidense la "decisión solidaria de ayudar a los demás países del mundo" y "el trabajo conjunto en la lucha por la pandemia", lo que marca "un camino de cooperación", se informó hoy oficialmente.

Según se precisó, esta entrega solidaria de vacunas es la más numerosa que se realiza a un solo país de la región por parte de los Estados Unidos.

Esa donación fue posible por el decreto de necesidad y urgencia (DNU) que firmó el Poder Ejecutivo el 3 de julio pasado, que permitió la firma de un acuerdo con el laboratorio Moderna Inc. para el suministro de 20 millones de dosis de su fármaco contra la Covid-19 o las dosis equivalentes como refuerzo.

Se trata de dos aeronaves Airbus 330-200 que viajaron a la ciudad de Memphis para traer desde allí los 3,5 millones de dosis de la vacuna Moderna, con lo cual la compañía de bandera sumará un nuevo cargamento a los más de 32 millones de dosis ya trasladadas a la Argentina desde el inicio de la pandemia.

El primero de los vuelos, matrícula LV-FNL, partió el miércoles a las 19.12 desde Ezeiza y aterrizó en Memphis a las 3.27 del jueves, mientras que el segundo, matrícula LV-FVH, partió desde Ezeiza ayer a las 8.59 y tocó pista en la ciudad norteamericana a las 17.57.

Ambas aeronaves llevaron una tripulación acotada debido a que, por disposición de las autoridades estadounidenses, debían estar con anticipación en el destino de carga de las vacunas, para adecuar convenientemente las bodegas, dado que las dosis deben ser transportadas a una temperatura de entre menos 15 y menos 50 grados, aunque luego de ser abiertas pueden ser conservadas a temperaturas de entre 2 y 8 grados.

Sumando las dosis que llegaron este viernes de China y las de Moderna -que marcarán un récord de vacunas recibidas en un lapso 24 horas- la Argentina alcanzará un total de 36.291.730 vacunas desde el inicio del plan de inoculación de la población.

Hasta el momento, en total, llegaron 32.791.730 dosis, de las cuales 11.868.830 corresponden a Sputnik V, (9.375.670 del componente 1 y 2.493.160 del componente 2); 10.608.000 a Sinopharm; 580.000 a AstraZeneca-Covishield, 1.944.000 a AstraZeneca por el mecanismo Covax de la OMS, y 7.790.900 a las de AstraZeneca y Oxford cuyo principio activo se produjo en la Argentina.

De acuerdo al Monitor Público de Vacunación, hasta la mañana de este viernes se distribuyeron 28.283.544 dosis de vacunas en todo el territorio, al tiempo que se aplicaron 26.511.672.

De ese total, hay 21.317.004 personas inoculadas con la primera dosis, y 5.194.668 cuentan con el esquema completo de vacunación.

Por su parte, más de 1,4 millones de dosis se estarán distribuyendo entre hoy y mañana en los 24 distritos del país para continuar con el avance del Plan Estratégico de Vacunación que despliega el Gobierno nacional para combatir el Covid-19, informaron fuentes oficiales.

De acuerdo a las fuentes, se están distribuyendo 1.468.000 dosis (768 mil de Sinopharm y 700 mil dosis de AstraZeneca) y en cuanto a los sueros de AstraZeneca se trata de los que arribaron la madrugada del lunes último en un vuelo de la empresa LATAM Cargo, mientras que en el caso de la Sinopharm pertenecen al cargamento arribado el martes pasado y que forma parte del total de 10 viajes a China previstos durante julio hasta llegar a los 8 millones de dosis.

"La pandemia es una advertencia y, al mismo tiempo, una oportunidad para avanzar hacia sociedades más equitativas, más inclusivas y más justas", sostuvo el presidente Alberto Fernández, al referirse hoy a la donación del Gobierno de estados Unidos y destacó que "es una contribución muy importante que marca un camino de cooperación".

A su vez la Embajada de Estados Unidos en Argentina, a través de su encargada de Negocios, MaryKay Carlson, señaló en un comunicado de prensa que "Estados Unidos y Argentina están trabajando juntos como socios para luchar contra esta pandemia global".

"Estamos orgullosos de poder entregar estas vacunas, de reconocida seguridad y efectividad, al pueblo argentino. Este es un momento único en la historia y requiere liderazgo y trabajo en conjunto", agregó.

La vacuna contra el coronavirus desarrollada por la empresa estadounidense de biotecnología Moderna fue diseñada a través de la tecnología de ARN mensajero para que el sistema inmunológico aprenda a repeler parte de la proteína del virus, y demostró una alta tasa de eficacia entre los adolescentes de 12 a 17 años.

En tanto, ayer, el presidente Fernández informó que el instituto ruso Gamaleya aprobó las primeras 140.625 dosis del componente 1 de la vacuna Sputnik V que fueron producidas en el país por Laboratorios Richmond.

Mientras tanto, la ministra de Salud, Carla Vizzotti, le transmitió a las autoridades de AstraZeneca la disposición de la Argentina para producir esa vacuna en el país "en su ciclo completo", en un encuentro que mantuvo con el vicepresidente ejecutivo de esa empresa, el neurocientífico Menelao Pangalos, con quien recorrió el nuevo centro de investigación y desarrollo de la empresa, ubicado en la ciudad de Cambridge.

Vizzotti estuvo en las instalaciones de AstraZeneca junto a la asesora presidencial Cecilia Nicolini en el marco de la visita que realiza al Reino Unido para intercambiar conocimiento y experiencias sobre desarrollos científicos y políticas sanitarias ante la pandemia de Covid-19.