---
category: La Ciudad
date: 2021-01-08T10:00:28Z
thumbnail: https://assets.3dnoticias.com.ar/080121-mujeres-emprendedoras.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: El Litoral'
resumen: En busca de la Mujer Empresaria 2020
title: En busca de la Mujer Empresaria 2020
entradilla: Podrán presentarse como candidatas todas las empresarias pertenecientes
  a instituciones adheridas a Fececo.

---
Esta distinción impulsada por la Federación de Centros Comerciales de la Provincia (Fececo) persigue como objetivo la motivación de la mujer en el mundo empresarial, reconociendo su trayectoria y proyectos que incentiven la actividad pyme. Por ello, está destinada a destacar aquellas mujeres empresarias que puedan demostrar la sustentabilidad en el tiempo de sus emprendimientos habiendo realizado un aporte al posicionamiento de género, empleando para ello su condición de liderazgo empresarial, social y/o gremial.

En cuanto a las bases del concurso, podrán presentarse como candidatas todas las empresarias pertenecientes a instituciones adheridas a Fececo.

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.15em; font-weight: 700">Requisitos</span>**

\-La sede de la empresa debe estar situada en la Provincia de Santa Fe.

\-Ser propietaria de la empresa y/o dirigir o gestionar la misma

\-Demostrar una antigüedad no menor a 3 años al 31/12/2020.

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.15em; font-weight: 700">Premiaciones</span>**

Se determinarán tres menciones de entre las cuales se elegirá la «Mujer Empresaria del Año».

\-Mención «Empresas Resilientes».

\-Mención «Responsabilidad Social Empresaria».

\-Mención «Innovación».

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.15em; font-weight: 700">Candidaturas y plazos</span>**

Las candidaturas las realizarán las entidades adheridas a Fececo a las vicepresidencias regionales que les correspondan. Presentarán una breve reseña de actividades y CV de las empresarias, acompañando las inscripciones en AFIP, API y Municipalidad o Comuna.

Cada entidad podrá nominar una sola postulante, encuadrándola en la categoría correspondiente. Podrán agregar optativamente un video en el que se muestre su actividad y que no dure más de 3 minutos.

En cada Región se elegirán dos de las mujeres presentadas. Deberán estar en diferentes categorías. Cada Región determinará el jurado que analizará las candidaturas y seleccionará las ganadoras.

Las dos candidatas por cada Región serán elevadas a Fececo que elegirá las 3 menciones especiales y entre ellas a la Mujer Empresaria del año.

Sobe los límites de la presentación, el 31/01/2020 - Presentación de las Entidades a la vicepresidencia regional; 06/02/2020 - Selección en cada Región de sus dos postulaciones; 20/02/2020 - Selección de las ganadoras por parte de Fececo.

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.15em; font-weight: 700">Jurado</span>**

El jurado de Fececo estará conformado por el presidente y el secretario de Fececo, los vicepresidentes regionales, un representante del Ministerio de la Producción y la secretaria General de Mujeres Empresarias Came, Marita Moyano.