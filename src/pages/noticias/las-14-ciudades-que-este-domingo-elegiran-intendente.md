---
category: La Ciudad
date: 2021-11-12T06:15:57-03:00
thumbnail: https://assets.3dnoticias.com.ar/INTENDENTES.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Las 14 ciudades que este domingo elegirán intendente
title: Las 14 ciudades que este domingo elegirán intendente
entradilla: 'Dos localidades designarán por primera vez intendente y a sus primeros
  seis concejales. ¿Cuáles son y quiénes son los candidatos?

'

---
Este domingo se desarrollarán las elecciones generales y de los 59 municipios que posee la provincia de Santa Fe, en 14 se elegirán intendente; con la particularidad de que dos lo harán por primera vez.

Uno es el caso de Sauce Viejo, ciudad del departamento La Capital y el otro es San Vicente, distante a 103 km de esta capital provincial y que se encuentra en el departamento Castellanos.

En estas dos localidades también se definirán los primeros concejales (seis) mientras que en las restantes (12) se elegirán tres ediles por cuatro años.

En tanto, las 306 localidades restantes designarán nuevos representantes comunales; 148 son comunas de cinco miembros (una es nueva: María Susana, del departamento Castellanos). Las comunas de tres miembros son las de 158 localidades.

En total se elegirán 14 intendentes; 210 concejales titulares y 184 suplentes; 1.214 miembros comunales titulares y la misma cantidad de suplentes; y 980 contralores de cuentas, tanto titulares como suplentes.

Con respecto a la elección nacional, el secretario Electoral de la Provincia, Pablo Ayala recordó que “en los 365 distritos de la provincia votamos senadores y diputados nacionales".

Y explicó: "Tenemos dos sistemas de escrutinio provisorio, uno nacional, que es el primero que se realiza en la mesa, por orden de importancia, primero senador, diputado después; luego empezamos con el escrutinio de mesa de las categorías provinciales; se labran las actas y el certificado del escrutinio”.

“Una vez que está terminada la mesa se transfieren los datos a los centros de procesamiento, tanto de Rosario, como de Santa Fe, ingresa por las dos bocas de acceso, y si coinciden los datos impacta en sistema de escrutinio provisorio”, detalló el secretario Electoral.

### El protocolo a respetar el domingo

El funcionario provincial repasó algunas cuestiones que los electores deberán tener en cuenta este domingo al celebrarse las Elecciones Generales provinciales y nacionales.

Ayala se refirió, entre otros temas, a los protocolos que habrá que respetar a la hora de ir a sufragar. En ese sentido, reconoció que “la cuestión sanitaria acompañó, bajaron los índices de contagio, lo que permitió tomar algunas flexibilidades respecto de las elecciones Paso”, y agregó que “ya no va a haber filas fuera de los establecimientos, sino que los electores podrán ingresar e ir hasta su mesa de votación”.

No obstante, insistió en que “hay que usar barbijo y que esté bien colocado, lo que se controlará al ingreso de los establecimientos; y respetar el distanciamiento mientras se espera el turno para votar”.

Ayala indicó también que el procedimiento a la hora de sufragar será similar al de las Paso: primero, “para la elección nacional, en la mesa le van a pedir al votante que retire un sobre ya firmado e ingrese al cuarto oscuro. Cuando sale, tras introducir el sobre en la urna, le dan la boleta única para las elecciones provinciales, irá al box de votación y luego colocará esa boleta en la urna”.

Otro aspecto que remarcó el secretario Electoral es que durante toda la jornada electoral, de 8 a 18 horas, “las personas con alguna discapacidad, las embarazadas y los mayores de 60 años pueden solicitar prioridad para votar”.

Asimismo, el funcionario mencionó que “quienes no votaron en la Paso igualmente están habilitados para sufragar en las elecciones Generales”, en tanto que este 11 de noviembre “vence el plazo para que los que no votaron en las Paso justifiquen el no voto”.