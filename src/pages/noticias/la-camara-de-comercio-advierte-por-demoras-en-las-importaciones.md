---
category: El Campo
date: 2021-03-26T07:13:55-03:00
thumbnail: https://assets.3dnoticias.com.ar/kulfas.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Clarín
resumen: La Cámara de Comercio advierte por demoras en las importaciones
title: La Cámara de Comercio advierte por demoras en las importaciones
entradilla: La entidad envió una nota al ministro Matías Kulfas en la que manifiesta
  preocupación por las demoras en la aprobación de las licencias de importación.

---
La Cámara Argentina de Comercio y Servicios (CAC), advirtió mediante una carta enviada al ministro de Desarrollo Productivo, Matías Kulfas, sobre demoras registradas en la aprobación de licencias de importación.

"Nuestra entidad, representante del sector Comercio y Servicios a nivel nacional, ha recibido en las últimas semanas cuantiosos planteos por parte de instituciones y empresas asociadas de todo el país, referidos a las crecientes dificultades para obtener las aprobaciones señaladas. Según manifestaciones de nuestros socios, numerosas PyMEs efectuaron compras externas por debajo de las proyecciones de importación para el año 2021 oportunamente presentadas y aun así se ven impedidas de efectuar estas operaciones", explicó la CAC al ministro.

"En nuestro país prácticamente la totalidad de las actividades económicas  dependen en algún grado de las compras externas (de bienes finales, insumos, bienes de capital, etc.) para operar con normalidad. Es por ello que las marcadas restricciones a las importaciones generan notorios perjuicios, tanto a las empresas directamente afectadas como a otras firmas vinculadas a aquellas por ser proveedores o clientes", explicó.

Finalmente, el sector advirtió que "la falta de aprobación de las licencias de importación se traduce en escasez de insumos para la producción nacional, desabastecimiento e incumplimiento de contratos, dado que muchos de los bienes que se ven afectados por las trabas carecen de sustitutos nacionales de iguales características (o bien los productores locales no están en condiciones de generar un volumen de abastecimiento suficiente)"

Por otra parte, la Cámara de Comercio recordó en su reclamo que "esto se da en un contexto económico con múltiples dificultades adicionales, como ser las derivadas de la pandemia de coronavirus y de una recesión de casi tres años, lo que potencia los efectos adversos sobre las firmas", concluyó.

Hace pocos días también la Eurocámara, que agrupa a todas las cámaras bilaterales europeas radicadas en la Argentina, advirtió a la autoridades sobre el tema. Entre otros puntos, la entidad pidió  "agilizar el proceso de autorización de licencias no automáticas del Sistema Integral de Monitoreo de las Importaciones para que las empresas puedan honrar sus acuerdos comerciales con clientes locales que dependen de la importación de insumos y materiales para la cadena productiva nacional".