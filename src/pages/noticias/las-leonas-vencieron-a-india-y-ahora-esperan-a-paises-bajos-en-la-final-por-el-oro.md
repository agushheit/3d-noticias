---
category: Deportes
date: 2021-08-05T06:15:31-03:00
thumbnail: https://assets.3dnoticias.com.ar/LEONAS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Las Leonas vencieron a India y ahora esperan a Países Bajos en la final por
  el oro
title: Las Leonas vencieron a India y ahora esperan a Países Bajos en la final por
  el oro
entradilla: El seleccionado argentino femenino de hockey sobre césped, Las Leonas,
  vencieron hoy a India por 2-1 y el viernes a las 7 jugarán la final de los Juegos
  Olímpicos de Tokio 2020 ante Países Bajos.

---
El seleccionado femenino de hockey sobre césped de la Argentina, Las Leonas, venció hoy a India por 2-1 y el próximo viernes a las 7 jugará la final de los Juegos Olímpicos de Tokio 2020 ante Países Bajos, en busca de su primera medalla de oro.

La capitana Noel Barrionuevo marcó los dos goles argentinos, de córner corto, a los 18 y 36 minutos, respectivamente.

En una durísima semifinal disputada en el campo norte del estadio Oi de Tokio, el equipo dirigido por Carlos Retegui se repuso del gol tempranero de India, convertido por Gurjit Kaur (2m.), y con goles de la histórica Barrionuevo, en su cuarto Juego Olímpico, ganó y se aseguró la quinta medalla de la historia.

Las Leonas jugarán su tercera final olímpica y buscarán ante Países Bajos la revancha de Londres 2012 cuando se quedaron con la medalla de plata.

Luego del séptimo puesto en Río 2016, el combinado argentino volverá al podio olímpico al que ya se subió en Sidney 2000 (plata), Atenas 2004 (bronce), Beijing (2008) y Londres 2012 (plata).

En un equipo con muchos talentos jóvenes, en la semifinal apareció la experiencia de la capitana Noel Barrionuevo con dos goles claves y las atajadas de Belén Succi para defender la ventaja.

Después de una preparación atípica por la pandemia de coronavirus, Las Leonas superaron todos los obstáculos y pusieron de nuevo en lo más alto al hockey sobre césped argentino.

No fue la mejor actuación del equipo del "Chapa" Retegui en el torneo y deberá mejorar para competir contra el seleccionado neerlandés, que aplastó a Gran Bretaña por 5-1 en la primera semifinal.

Sin embargo, el equipo tuvo una gran respuesta anímica luego del gol tempranero de India a los 2 minutos del partido luego de un córner corto.

En el primero a favor, el conjunto asiático se puso en ventaja luego de la arrastrada de Gurjit Kaur que prendió las alarmas de la Argentina.

Luego de un primer cuarto complicado, la respuesta de la Argentina después del golpe duro de la ventaja en contra llegó en el segundo con una mayor presión y más presencia en ataque con una imparable Agustina Albertario.

En la tercera oportunidad de córner corto, Las Leonas pudieron llegar al empate con una gran definición abajo a la izquierda de arrastrada de Noel Barrionuevo.

La capitana se hizo cargo del córner corto por la ausencia de Agustina Gorzelany, quien era atendida afuera del campo de juego por un corte en la frente, y consiguió la ansiada igualdad.

En un partido muy parejo, con posesiones casi igualadas, la arquera Belen Succi tuvo su primera gran aparición con una importante atajada en el segundo córner corto de India, a cuatro minutos para el final del primer tiempo.

El final de la primera etapa fue con sufrimiento ya que Argentina estuvo cerca de tener un córner corto en contra por una jugada en la que Agostina Alonso se quedó con la máscara luego de un córner corto pero el video ref lo desestimó.

El inicio del segundo tiempo fue lo mejor de Las Leonas en el partido porque presionaron a India y generaron dos córneres cortos seguidos.

En la segunda ocasión, nuevamente apareció Barrionuevo para marcar el 2-1 pero el festejo fue con suspenso ya que el video ref chequeó un rebote en Sushila Chanu por una supuesta jugada peligrosa.

El último cuarto fue a pura tensión y Las Leonas defendieron con todo la mínima ventaja pero el sufrimiento fue hasta el final ya que en una de las últimas jugadas la gigante Succi sacó otra bocha difícil.

Los corazones argentinos se detuvieron cuando a 17 segundos para el final, las árbitras chequearon de nuevo con el video ref la posibilidad de un córner corto para India que finalmente no fue cobrado.

Con el final llegó el festejo del equipo que dirige Carlos Retegui que por la pandemia tuvo una preparación atípica, sin competencia y con largas concentraciones en el Cenard, Pinamar, Cariló, Mar del Plata, San Diego (EEUU), Córdoba y Valencia (España).

En un Juego Olímpico especial, con modo burbuja, la delegación está hace más de un mes encerrado en busca de un objetivo: la ansiada medalla de oro.

**Síntesis**

Argentina: Belén Succi; Agustina Gorzelany, Valentina Raposo, Noel Barrionuevo, Victoria Sauze, Agostina Alonso, Sofía Toccalino, Rocío Sánchez Moccia, Eugenia Trinchinetti, Delfino Merino y Agustina Jankunas. DT: Carlos Retegui.

India: Savita; Udita, Gurjit Kaur, Deep Grace Ekka, Navjot Jaur, Monika, Nisha, Neha, Rani, Navneet Kaur, Vandana Katariya. DT: Wayne Patrick Lombard.

Goles: 2m. Gurjit Kaur (I); 18m. y 36m. Noel Barrionuevo (A)

Ingresaron en Argentina: María José Granatto, Agustina Albertarrio, Micaela Retegui, Victoria Granatto.

Árbitras: Amber Church (Nueva Zelanda) y Sarah Wilson (Gran Bretaña).