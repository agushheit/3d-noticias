---
category: La Ciudad
date: 2021-02-02T07:23:21-03:00
thumbnail: https://assets.3dnoticias.com.ar/tienda.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de LT10
resumen: Bajaron las ventas en comercios minoristas santafesinos
title: Bajaron las ventas en comercios minoristas santafesinos
entradilla: Según el informe mensual del Centro Comercial, tuvieron una disminución
  respecto a enero del 2020. Solo un 27% de los encuestados las incrementó.

---
En enero del 2020 aún no había en el país contagios por coronavirus y la economía se encontraba dentro de un marco de normalidad, perdido en los meses siguientes como consecuencia del freno en las actividades ocasionado por la cuarentena. 

Los efectos del cierre de comercios durante meses o el cambio de modalidad de ventas afectaron los números en los principales corredores comerciales de la ciudad de Santa Fe.

Así lo refleja, el informe mensual del Observatorio del Centro Comercial local que indica que respecto a enero del año pasado, el 54,5% de los comerciantes encuestados disminuyó sus ventas.

En tanto, el 18% respondió que lograron mantenerlas, y solo el 27 incrementarlas.

Todos los rubros se vieron afectados por la baja de ventas, "pensábamos que con la familia que iba a hacer menos viajes por la pandemia el comercio iba a tener mayor actividad, pero los números arrojan que no ha sido así", señaló por LT10 Martín Salemi, presidente del Centro Comercial.   

**Expectativas**

Consultados sobre sus expectativas del 2021, el 45,5% de los comerciantes cree que será mejor. Un 36,4% no vislumbra cambios en el nivel de actividad, y un 18,2% cree que será peor aún.