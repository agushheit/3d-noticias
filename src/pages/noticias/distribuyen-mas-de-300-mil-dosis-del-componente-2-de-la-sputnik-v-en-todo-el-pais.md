---
category: Agenda Ciudadana
date: 2021-09-02T06:00:11-03:00
thumbnail: https://assets.3dnoticias.com.ar/SPUTNIK21.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Distribuyen más de 300 mil dosis del componente 2 de la Sputnik V en todo
  el país
title: Distribuyen más de 300 mil dosis del componente 2 de la Sputnik V en todo el
  país
entradilla: Las vacunas fueron producidas por el laboratorio argentino Richmond y
  llegarán a todas las provincias argentinas y a la Ciudad Autónoma de Buenos Aires.

---
Más de 300 mil dosis del componente 2 de la vacuna Sputnik V, serán distribuidas entre este miércoles y jueves en todo el país para completar los esquemas de inmunización de la población, en el marco del Plan Estratégico de Vacunación contra la Covid-19, se informó oficialmente.

Según el criterio dispuesto por el Ministerio de Salud de la Nación en base a la cantidad de población de cada distrito, 118.875 dosis corresponderán a la provincia de Buenos Aires; 20.250 a la Ciudad de Buenos Aires; 2.750 a Catamarca; 8.125 a Chaco; 4.500 a Chubut; 25.500 a Córdoba; 7.625 a Corrientes; 9.500 a Entre Ríos; 4.125 a Formosa.

A Jujuy le corresponderán 5.250; 2.500 a La Pampa; 2.750 a La Rioja; 13.500 a Mendoza; 8.625 a Misiones; 4.500 a Neuquén; 5.000 a Río Negro; 9.750 a Salta; 5.250 a San Juan; 3.375 a San Luis; 2.500 a Santa Cruz; 23.875 a Santa Fe; 6.750 a Santiago del Estero; 1.125 a Tierra del Fuego, y 11.500 a Tucumán.

Estas dosis, que superaron el control de calidad realizado por el Centro Nacional Gamaleya de Epidemiología y Microbiología de la Federación Rusa, permitirán a la Argentina avanzar hacia el objetivo de completar el esquema de vacunación de la población en todo el territorio nacional.

De acuerdo al Monitor Público de Vacunación, hasta esta tarde se distribuyeron 49.240.699 dosis de vacunas en todo el país, de las cuales ya fueron aplicadas 43.125.947. De ese total, hay 28.076.313 personas inoculadas con la primera dosis, y 15.049.634 cuentan con el esquema completo de inmunización.