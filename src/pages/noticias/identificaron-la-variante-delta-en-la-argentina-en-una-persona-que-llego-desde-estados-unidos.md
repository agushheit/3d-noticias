---
category: Agenda Ciudadana
date: 2021-06-21T06:00:20-03:00
thumbnail: https://assets.3dnoticias.com.ar/DELTA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Identificaron la variante Delta en la Argentina en una persona que llegó
  desde Estados Unidos
title: Identificaron la variante Delta en la Argentina en una persona que llegó desde
  Estados Unidos
entradilla: 'Se trata de un hombre de 26 años que arribó al país el día 4 de junio. '

---
La variante Delta -identificada originariamente en la India- fue detectada en nuestro país en una persona que llegó desde Estados Unidos, informó este domingo el Ministerio de Salud.

La cartera sanitaria indicó que recibió el resultado del Laboratorio Nacional de Referencia Anlis-Malbrán de la detección de esa variante.

Se trata de un hombre de 26 años que llegó al país el día 4 de junio procedente de Estados Unidos y al momento del arribo, el pasajero presentaba una PCR negativa realizada 48 horas previas al viaje.

Al ingresar al país, en los operativos de testeo que se realizan de manera sistemática a todos los viajeros, presento test de antígeno positivo y se derivó la muestra al laboratorio de referencia para el análisis genómico.

Desde la detección al ingreso al país, el pasajero realizó un aislamiento de 10 días en un hotel de la Ciudad de Buenos Aires (CABA) y su acompañante -con testeo negativo en dos oportunidades- realizó aislamiento domiciliario. Se informó además que “ambos se encuentran en buen estado de salud”.

Delta es una de las variantes de preocupación que se encuentran bajo vigilancia por las autoridades sanitarias nacionales, señaló el Ministerio.