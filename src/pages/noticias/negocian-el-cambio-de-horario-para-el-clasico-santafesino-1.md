---
category: Deportes
date: 2021-05-07T08:39:19-03:00
thumbnail: https://assets.3dnoticias.com.ar/clasicojpg.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Diario Uno
resumen: Negocian el cambio de horario para el Clásico Santafesino
title: Negocian el cambio de horario para el Clásico Santafesino
entradilla: La provincia, las autoridades de la Liga Profesional y la TV analizan
  una modificación en el horario para el Clásico Santafesino del próximo domingo.

---
El horario del Clásico Santafesino fue uno de los temas que tuvo en vilo a la provincia y la Liga Profesional, como consecuencia de los inconvenientes de cada lado en cuanto a la programación del mismo. Es que como ocurrió en Rosario, Omar Perotti quería que Colón y Unión se enfrenten a las 21 (o 20 como muy tarde) para que finalice en los horarios de restricciones, en tanto que por reglamento los partidos que definen posiciones en la última jornada deben disputarse en simultáneo.

Este reglamento ostenta contra las restricciones impuestas en Buenos Aires, donde los partidos no pueden terminar luego de las 20. En tanto que de los equipos que definen posiciones también se encuentran argentinos jugando las Copas Sudamericana y Libertadores, que no pueden según el Sindicato jugar con menos de 48 horas de descanso.

Por este motivo, en el horario que se oficializó el pasado martes se conoció que el Clásico Santafesino arrancaría a las 17.15, aunque se hizo un asterisco, donde se aclaró que el mismo seguía negociándose con el gobierno de la provincia de Santa Fe.

"Por motivos de Seguridad y Salud Pública de la Provincia de Santa Fe, se seguirá gestionando a fin de confirmar los horarios definitivos de los partidos indicados", aclaró la Liga Profesional sobre el Clásico Santafesino que animarán el próximo domingo Colón y Unión en el Brigadier López.

Es así que se siguió negociando y en las próximas horas finalmente se comunicaría que el partido arrancaría a las 18 o 18.15, con lo cual terminaría en un horario donde en nuestra provincia hay restricciones para la circulación, con lo cual en parte se contentará al gobierno de la provincia.

Sin embargo, sobre lo que no hubo éxito en las negociaciones fue con la televisación sin codificar, cuestión que quería el gobierno para evitar que la gente se junte en un domicilio en particular, donde tengan pago el pack fútbol, o en bares de la ciudad. Incluso se conoció que la cadena encargada de televisarlo es TNT Sports, quien no tuvo ningún ánimo de negociar al respecto.