---
category: La Ciudad
date: 2021-07-24T06:00:10-03:00
thumbnail: https://assets.3dnoticias.com.ar/BONDI.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Subsidios a Santa Fe para el transporte por colectivos
title: Subsidios a Santa Fe para el transporte por colectivos
entradilla: 'La nueva partida a distribuir en todo el interior del país es de $ 8.500
  millones. Sin precisiones sobre cuándo, cuánto y cómo llegará el dinero a la ciudad. '

---
El Gobierno Nacional autorizó oficialmente este viernes la distribución de subsidios a las empresas de transporte automotor de pasajeros del interior del país, por un total de $ 8.500 millones.

A través de la Resolución 248/2021 del Ministerio de Transporte, publicada este jueves en el Boletín Oficial, se formalizó la distribución parcial correspondiente al "Fondo de compensación al transporte público de pasajeros por automotor urbano del interior del país".

Ese Fondo se creó a partir de la Ley Nº 27.467, con el fin de compensar los desequilibrios financieros del transporte público automotor urbano y suburbano, tanto provinciales como municipales.

Según lo dispuesto, un total de $ 4.500 millones serán destinados a compensar a los servicios de transporte público automotor urbano y suburbano, provinciales y municipales, en montos mensuales de $ 1.500 millones correspondiente a junio, julio y agosto de este año.

De la misma manera las empresas recibirán un refuerzo adicional por un total de $ 4.000 millones de pesos, que serán distribuidos en cuotas mensuales de $ 2.000 millones para junio, $ 1.000 millones en julio y $ 1.000 millones en agosto.

Los subsidios forman parte de un acuerdo que el Gobierno firmó con las provincias por un total de $ 20.000 millones, cifra que será distribuida a lo largo de 2021, para asistir financieramente al transporte automotor de pasajeros del interior.

En el primer trimestre de este año, el Ministerio de Transporte había transferido $ 6.000 millones.

De acuerdo con el texto oficial, para acceder a la asistencia económica cada provincia tiene que firmar un convenio en el que se compromete a continuar transfiriendo los montos a cada una de las empresas de su jurisdicción.

Las provincias también están obligadas a informar las bajas de empresas y servicios, ramales, líneas, frecuencias, y las novedades vinculadas al personal de los trabajadores, parque móvil, kilómetros recorridos de los servicios, entre otros ítemes.

Además, el 20 de agosto será la nueva fecha límite para completar el proceso de presentación de la información requerida para la determinación de los coeficientes de distribución de los subsidios.

En tanto, las jurisdicciones que son parte del "Fondo de compensación al transporte público de pasajeros por automotor urbano y suburbano del interior del país" que aún no se hayan incorporado al Sistema Único de Boleto Electrónico (SUBE), deberán manifestar su aceptación al cronograma propuesto por el Ministerio de Transporte.

En 2020, el Estado nacional había desembolsado un total de $ 16.500 millones en subsidios para estas empresas: este año, el monto a distribuir es un 20% superior.

**Qué pasa en la ciudad de Santa Fe**

Tras la novedad, El Litoral consultó al subsecretario de Movilidad y Transporte de la ciudad, Lucas Crivelli, quien señaló que “hay algunas cuestiones de la Resolución que todavía no están del todo claras, sobre todo en cuanto a los recursos que las provincias además deben comprometer, si son los que estaban previstos o si son otros nuevos”. Y agregó luego: “Tampoco está clara cómo será la distribución de estos $ 8.500 millones”.

Más allá de estas incertidumbres “esperamos que éste sea el principio de la solución” al problema que viene acarreando el servicio de transporte por colectivos en la ciudad, al igual que ocurre en el resto del interior del país. “Al menos para llegar a fin de año y replantear luego el sistema de subsidios”, dijo Crivelli.

**-¿Qué soluciona este subsidio?**

\-Esto viene a cubrir el desfasaje salarial respecto a Buenos Aires, para poder pagar el incremento acordado con la UTA (Unión Tranviaria Automotor) y para palear en aproximadamente un 15 por ciento el déficit del subsistema.

**Vuelta a clases**

En ese sentido, el funcionario municipal puso las expectativas en “que se incremente la cantidad de pasajeros a partir del mes de agosto y septiembre, con la vuelta a clases presenciales” para que “medianamente el sistema se pueda mantener, con los costos operativos vigentes”.

“La idea de mínima es poder volver a los 60 a 65 mil pasajes por día”, transportados en los colectivos de la ciudad, dijo Crivelli, ya que “desde junio ronda en los 30 mil”. Cabe señalar que este abrupto descenso está relacionado de forma directa con el nuevo aislamiento por la pandemia que rigió en Santa Fe y la consecuente no presencialidad en las aulas.

En definitiva, “si llegamos a los 70 mil pasajeros por día, con los costos salariales con el incremento otorgado totalmente cubiertos, y parcialmente también los costos operativos, no tendremos un sistema totalmente normalizado pero por lo menos alcanzará para llegar al año que viene y gestionar luego un nuevo esquema de subsidios para el 2022”, señaló el funcionario de Transporte municipal.

\-¿Cuándo llegará el subsidio a la ciudad?

\-Nosotros calculamos que, si ya está la Resolución emitida por el Ministerio de Transporte, la semana que viene debería estar efectivizado con la provincia, y luego tendrá algunos pasos administrativos más. Así que, si todo va normal, estimo que en 20 días debería estar todo normalizado, con la transferencia de recursos.