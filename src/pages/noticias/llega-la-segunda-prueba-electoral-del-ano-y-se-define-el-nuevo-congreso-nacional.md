---
category: Agenda Ciudadana
date: 2021-11-07T06:15:31-03:00
thumbnail: https://assets.3dnoticias.com.ar/eleccionesnov.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Llega la segunda prueba electoral del año y se define el nuevo Congreso Nacional
title: Llega la segunda prueba electoral del año y se define el nuevo Congreso Nacional
entradilla: 'El próximo domingo 34 millones de personas podrán votar el recambio para
  127 bancas de la Cámara de Diputados y 24 del Senado que se renovarán en diciembre. '

---
Los argentinos elegirán el próximo domingo a quienes ocuparán las 127 bancas de la Cámara de Diputados y las 24 del Senado que se renovarán en diciembre, en comicios que constituirán la segunda prueba electoral del año tras las primarias abiertas, simultáneas y obligatoria (PASO) de septiembre.  
  
A una semana de estas elecciones de medio término, en las que podrán votar 34 millones de personas, el Gobierno ya anunció cambios en el protocolo para agilizar el trámite y acortar la espera de los electores en los centros de votación, a partir de la mejoría sanitaria que exhibe el país con respecto al coronavirus.  
  
Las elecciones de medio término del domingo 14 definirán la correlación de fuerzas en la composición del Congreso Nacional para el segundo tramo de la gestión del presidente Alberto Fernández.  
  
El desafío del Frente de Todos (FdT) será mejorar los resultados obtenidos en las PASO del 12 de setiembre y acortar la ventaja que logró la coalición opositora Juntos por el Cambio (JxC) en distritos clave como Buenos Aires, Córdoba, Santa Fe y CABA.  
  
El nuevo período legislativo será clave para el acompañamiento de las políticas que impulse el Poder Ejecutivo, en su plan de reactivación del país tras los estragos causados por la pandemia de coronavirus.  
  
Para JxC, en tanto, aumentar su presencia en el Congreso Nacional constituye la plataforma necesaria para ordenar los liderazgos internos y consolidar sus aspiraciones presidenciales hacia el 2023.  
  
Las PASO representaron victorias para el oficialismo en Catamarca, Formosa, La Rioja, San Juan, Santiago del Estero y Tucumán.  
  
Sin embargo, la disputa en la provincia de Buenos Aires, la de mayor peso electoral del país por representar el 37% del padrón, le dio una ventaja de casi 10 puntos a Juntos sobre el FdT y en Córdoba, el segundo distrito de mayor volumen, la brecha fue todavía mayor.  
  
El Frente de Izquierda-Unidad cosechó un buen caudal de sufragios en las primarias, que le permitió ubicarse en el tercer puesto en la provincia de Buenos Aires y dejar en cuarto lugar a Avanza Libertad, algo que deberá revalidar en las parlamentarias con el objetivo de asegurar su representación en la Cámara baja.  
  
Además de los 13 millones de electores de la provincia de Buenos Aires, serán gravitantes también en los comicios del domingo 14 los votos de 3 millones de cordobeses, 2,7 millones de santafesinos y 2,5 millones de porteños, que en conjunto superan la mitad del volumen electoral del país.  
**  
El Congreso**

  
Del total de 127 bancas que se ponen en juego en la Cámara de Diputados, 60 están en poder de JxC y 51 en manos del FdT.  
  
Si se repitieran los resultados de las PASO, la coalición opositora alcanzaría la primera minoría con 188 bancas y el oficialismo podría perder nueve lugares y quedarse con 111 diputados, por lo que dependería de acuerdos con fuerzas provinciales para alcanzar el quórum de 129 legisladores.  
  
La competencia por las plazas del Senado, en tanto, estará marcada por la aspiración del oficialismo a retener el quórum propio en el recambio de este año de las plazas de Corrientes, Santa Fe, Córdoba, Mendoza, La Pampa, Chubut, Catamarca y Tucumán.  
  
En las primarias de septiembre, poco después de conocerse los resultados, el presidente Alberto Fernández pronunció un discurso en el que aseguró que había tomado nota del mensaje de la ciudadanía en las urnas.  
  
"Escuchamos con respeto y mucha atención el veredicto de la gente; hay errores que no debimos haber cometido; de los errores aprendemos. Hay una demanda y a partir de mañana vamos a prestarle atención y resolver el problema que la gente nos plantea", expresó entonces Fernández.  
  
Luego de algunas tensiones internas, el oficialismo dispuso cambios en la estructura del Gabinete y dinamizó acciones para reactivar la economía, golpeada por las consecuencias del impacto del coronavirus en el país.  
  
Con Victoria Tolosa Paz a la cabeza de la lista para la provincia de Buenos Aires y Leandro Santoro al tope de la nómina porteña, el FdT reforzó las actividades en los territorios, en lo que llamó una campaña de cercanía, con diálogo directo entre postulantes, militantes y ciudadanos.  
  
JxC, por su parte, volvió a apoyar su agenda electoral en cuestionamientos a la gestión de Gobierno y centró sus acciones en la provincia de Buenos Aires, detrás del candidato a diputado nacional Diego Santilli, acompañado por el jefe comunal porteño, Horacio Rodríguez Larreta.  
  
Para los próximos días, las fuerzas tienen previsto clausurar la actividad de campaña con actos que mostrarán a las figuras centrales de sus espacios, tras dos meses de actividades proselitistas, que incluyeron debates televisivos de candidatos.  
  
Con un protocolo sanitario más flexible y casi 27 millones de argentinos vacunados contra el coronavirus con dos dosis, la votación del domingo 14 mantendrá el esquema ampliado de centros de votación -al igual que en las PASO- y la participación de 17 mil facilitadores sanitarios en 101.457 mesas de todo el país.