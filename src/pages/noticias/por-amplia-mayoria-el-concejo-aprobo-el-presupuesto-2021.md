---
category: La Ciudad
date: 2020-12-16T12:07:27Z
thumbnail: https://assets.3dnoticias.com.ar/1.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Por amplia mayoría el Concejo aprobó el presupuesto 2021
title: Por amplia mayoría el Concejo aprobó el presupuesto 2021
entradilla: El Concejo Municipal trató y aprobó este martes el Mensaje del Presupuesto
  de Recursos y Gastos de la Administración Central 2021 y la ordenanza Tributaria.

---
El Concejo Municipal aprobó en la sesión de este martes los Mensajes N° 16/20 y 17/20 enviados por el Ejecutivo Municipal, sobre el Presupuesto 2021 y la Ordenanza Tributaria. Según consta en el Mensaje elevado al legislativo local, la Ordenanza Presupuestaria 2021 se proyecta en el contexto de crisis económica-social que vive nuestro país y que se ha visto agravada durante el presente ejercicio por la declaración por parte de la Organización Mundial de la Salud (OMS) de la Pandemia COVID-19.

# **Intervenciones urbanas, iluminación y bacheo**

Del total de $13.664.317.516, el Presupuesto prevé $411.000.000 para el Programa Alumbrado Público, que contiene los proyectos de Sistema de Alumbrado Público, sistema de semaforización y nuevas obras de iluminación, entre las que se destacan los barrios Scarafía, Liceo Norte y Santa Marta.

Asimismo, $98.000.000 para bacheo en distintos puntos de la ciudad, lo que abarca 5.000 metros cuadrados de bacheo asfáltico y 7.300 metros cuadrados de bacheo de hormigón, destinando en total para el Programa de Infraestructura Vial $270.062.330.

Por otra parte, $600.000.000 para el Plan Integrar contemplando el desarrollo de obras y la instalación de Estaciones en los barrios más vulnerados de la ciudad.

Se prevé también en el Presupuesto, $70.000.000 en mantenimiento e intervenciones urbanas, $76.000.000 para mejorar y reparar el Cementerio Municipal, $23.000.000 destinados a asuntos hídricos y gestión de riesgo.

Además, $174.000.000 dentro de Agencia Santa Fe Habitat para generar lotes con servicios y para el Plan Integrar Barrio Mío se prevén $160.000.000 para la urbanización de barrios y generar espacios verdes públicos. En tanto, $150.000.000 para promover el acceso a la vivienda a santafesinos y santafesinas.

# **Avenida Galicia, agua potable en Colastiné e iluminación en barrios**

De esta manera, se contempla un conjunto de programas con el objetivo de continuar sentando las bases de un Estado inclusivo y participativo con propuestas de integración mirando al espacio público como lugar de representación y cohesión ciudadana.

Por tanto, el Concejo autorizó al Departamento Ejecutivo Municipal a contraer empréstitos y/o a celebrar contratos de fideicomiso hasta el límite de $213.000.000. Los fondos obtenidos en virtud de la autorización dispuesta tendrán como destino las siguientes obras y adquisiciones:

Renovación de Av. Galicia, $61.320.000, para la recuperación del cantero central y la mejora en la iluminación a lo largo de 1500 metros de extensión de la Avenida. Asimismo, el objetivo es generar una adecuada movilidad peatonal, mejorar la accesibilidad, incorporar espacios de descanso y mejorar la forestación existente.

Iluminación de Barrio Siete Jefes, $38.055.000, obra que incluye la colocación de 335 columnas metálicas con artefactos LED en el sector comprendido por Bv. Gálvez hacia el Sur, Diagonal Matturo al Norte, Siete Jefes al Este y Vélez Sarsfield al Oeste abarcando a 90 cuadras de todo el barrio.

Iluminación de Guadalupe Central y Noroeste, $48.375.000, con la colocación de 490 columnas metálicas con artefactos LED en el sector incluido por las calles Javier de La Rosa, Beruti, Dorrego y la Laguna Setúbal.

Provisión de agua potable para Colastiné Sur, $32.250.000, con la instalación de un sistema de captación de agua (Subterránea); impulsión (bombas y cañerías); reserva elevada; tratamiento e instalaciones complementarias; red de distribución y conexiones domiciliarias.

Por medio de un estudio hidrogeológico se ubicó una zona con agua subterránea de buena calidad, ubicada al Norte del Club de Caza y Pesca. Se realizaron análisis de calidad y ensayos de bombeo que permitieron el diseño del pozo de explotación. El agua de ésta forma extraída, será conducida por un sistema de impulsión hasta una reserva elevada que se ubicará en el predio de la escuela, al ingreso del barrio.

Asimismo, se prevé para la iluminación Barrio Fomento 9 de Julio, $10.000.000; para la iluminación Barrio Roma, $10.000.000; para la iluminación entorno de los Hospitales de Niños “Dr. Orlando Alassia”, “Dr. José María Cullen”, “Sayago”, “Mira y López”, Hospital “José María Iturraspe”, $5.000.000. Y para el primer tramo de Bicisenda Hipódromo-Fomento (desde Iturraspe y 4 de Enero a Peñaloza y Diagonal Goyena), $8.000.000.

# **Declaraciones**

<span style="text-decoration: underline;">**Leandro González**</span>**:** “Agradecer en líneas generales a los distintos bloques que han acompañado el presupuesto que nos parece un piso de respeto y democracia, de entendimiento general de la política, en este caso, un cuerpo legislativo a un Ejecutivo Municipal. Entiendo las posturas políticas pero creo que en un año en que vivimos con tantas dificultades, en medio de una pandemia, por eso agradecemos la vocación política y democrática en este contexto de crisis. Necesitamos un estado fuerte, que de respuestas y que esté a la altura de las circunstancias de lo que va a venir. Nadie es fundacional, ningún gobierno lo es y si alguien piensa que se va a salvar solo, está equivocado, acá está en discusión el rol del estado, si tenemos un estado fuerte o jugamos a debilitar las instituciones. Hoy creo que lo que aprobamos sintetiza esa vocación democrática de propuestas entendiendo el momento social, económico y político en el que estamos”.

***

<span style="text-decoration: underline;">**Laura Mondino**</span>**:** “Este presupuesto refleja la idea de trabajar por una ciudad integrada. Las obras que se incluyen van a cambiar seguramente la vida de muchas personas, como es el caso de la provisión de agua potable para Colastiné Sur con una inversión de 32 millones de pesos. ¿Cómo podemos pensar en una convivencia pacífica en una ciudad donde todo relucía entre bulevares mientras que en algunos barrios, diez minutos más allá, no había ni siquiera agua potable?”.

La edila subrayó, entre los puntos salientes, el sistema de Estaciones y los 939 millones que se van a invertir en obras de bacheo, mejorado de calles, iluminación y obras hídricas en barrios del cordón oeste. “Venimos de la historia de un gobierno municipal que prometió irresponsablemente y anunció inversiones que nunca llegaron, o que llegaron a medias, como las luces LED de Bulevar que iban a durar diez años y que al año y medio ya no funcionaban más. Como la obra de Avenida Freyre, que se había anunciado pero nunca se terminó. Como la obra de cloaca incompleta en Santa Rosa de Lima o el Jardín Municipal de barrio Santa Marta, que se anunció y nunca se hizo”.

***

<span style="text-decoration: underline;">**Carlos Suárez**</span>**:** “En lo particular, desde esta banca, siempre le hemos dado al presupuesto la relevancia que debe tener la norma madre y hemos hecho los esfuerzos para que el intendente cuente con esta herramienta. Acompañamos este presupuesto por el cual tenemos muchas críticas pero entendemos no es lo mismo para un Ejecutivo, o para una ciudad, comenzar un año, que se avecina muy difícil, con un presupuesto aprobado o no. Y en ese entendimiento hemos trabajado desde el Concejo para mejorar, en lo que fue posible, en el marco de las charlas y negociaciones entre los bloques, este presupuesto. Sin dudas los puntos más flojos es el vinculado con las obras y la seguridad. Es un presupuesto muy difícil de leer, tiene muy poco nivel de detalle y aclaración”.

***

<span style="text-decoration: underline;">**Federico Fulini**</span>**:** “Es una buena oportunidad para realizar balance. Estamos votando el segundo presupuesto de esta gestión. Se ven aquí cuáles son las prioridades y cuáles no lo son. Queda evidenciado el perfil del gobierno y es en este Concejo donde las distintas voces políticas podemos emitir nuestra opinión al respecto. El presupuesto deja siempre una sensación de insatisfacción porque además de mostrarnos todo lo que la municipalidad pretende hacer durante un ejercicio, también quedan expuestas todas las cosas que no se van a hacer. Con honestidad intelectual entendemos que la pandemia ha desvirtuado el año, por eso el Partido Justicialista, hemos puesto como prioridad acompañar al Gobierno Municipal en este duro momento que estamos viviendo como sociedad, sin dejar de decir las cosas con las cuáles no estamos de acuerdo”.

***

<span style="text-decoration: underline;">**Carlos Pereira**</span>**:** “Nosotros tuvimos el criterio de abstención por diversos motivos pero hay una cuestión en los presupuestos que es básica y esencial donde ponemos la lupa y es en las obras. Hay un porcentaje de obras que se van a realizar con fondos propios pero el grueso de las obras se van a realizar por endeudamiento, después de que tanto se habló y criticó la deuda. Hoy vemos que aparece el discurso contrario. Hay obras que tienen un financiamiento provincial y otras nacional, el Ejecutivo nos acercó el convenio de dos obras, una en barrio Scarafía y otra en Las Lomas, pero del resto de las obras no hay una sola documentación que acredite que esas obras se van a realizar”.

***

<span style="text-decoration: underline;">**Guillermo Jerez**</span>**:** “Celebro que aprobamos un endeudamiento donde se consideraron algunas propuestas que hicimos y que, a nuestro entender, van a tener un impacto. Se incorporó la bicisenda desde Colodrero a Peñaloza, un kilómetro y medio de senda, no solo para el disfrute de los vecinos de los barrios Pro Adelanto Barranquitas, Fomento 9 de Julio, Ciudadela, Schneider, sino para muchos trabajadores y trabajadoras que usan la bicicleta como medio de transporte van a poder venir del norte y llegar al macrocentro de la ciudad. Eso nos parece justicia espacial. Y entendemos muchas de las obras que apuntan a los distintos sectores de la ciudad”.

***

<span style="text-decoration: underline;">**Laura Spina**</span>**:** “Gracias al Cuerpo porque tenemos Presupuesto. El Municipio tiene presupuesto para el año 2021. Creo que es un presupuesto austero pero también realizable y responsable. El financiamiento para las obras previstas está, ya sea por el endeudamiento o por convenios celebrados. Cuando un gobierno tiene su primer año, las obras que se inauguran son obras que se van terminando del gobierno anterior y eso no ocurrió. Otra cuestión, creo que todos hemos visto el problema global que hemos tenido durante este año, entonces tenemos que considerar todas esas situaciones y lo más importante es que las personas tuvieron asistencia social y sanitaria para seguir adelante, esto ha sido la prioridad durante el año, no las obras públicas porque no se pudo trabajar durante gran parte del año. Ha sido un año crítico, es muy difícil hacer previsiones ambiciosas, mejor ser responsables y austeros”.

***

<span style="text-decoration: underline;">**Inés Larriera**</span>**:** “La ciudad ha retenido cientos de miles de oportunidades con el cierre de escuelas de trabajo y jardines municipales. Justamente se habla de empleo genuino y las escuelas de trabajo eran eso, eran espacios de oportunidades y vinculación. La verdad que nosotros decidimos abstenernos en acompañar el presupuesto de este año porque estamos sumamente preocupados por diferentes áreas. Respecto a las inversiones en salud, en un momento que sabemos tenemos por delante por lo menos seis meses de pandemia, está previsto el 0,26 del gasto municipal, 35 millones de pesos, de los cuales ya 16 millones de pesos están destinados a higiene y seguridad de los empleados municipales”.

***

<span style="text-decoration: underline;">**Valeria López Delzar**</span>**:** “Me voy a enfocar pensando a futuro, en el 2021 que se avecina. Respecto del rumbo de este presupuesto, en primer lugar manifestar que este presupuesto que aprobamos está priorizando el escenario que estamos atravesando, no solo como ciudad sino a nivel mundial, y lo que busca es mitigar las consecuencias sociales que va a dejar esta crisis sanitaria. En este contexto, el rol del municipio ha sido poner el énfasis y las prioridades en las políticas sociales. Los indicadores sociales nos están mostrando un escenario extremadamente complejo en términos de pobreza, indigencia y desempleo. Por eso este presupuesto ha puesto la prioridad en fortalecer la presencia del estado en los territorios más olvidados, en los barrios más vulnerables de esta ciudad. En este sentido, cobra importancia el Plan Integrar y la puesta en valor el desarrollo de una economía social y solidaria promoviendo la formación y fortalecimiento de sus actores y actoras.

***

<span style="text-decoration: underline;">**Sebastián Mastropaolo**</span>**:** “Creo que lo que tiene una misma línea y tenor es que a varios concejales de este Cuerpo el presupuesto nos da un sabor a poco, un sabor a que falta, a que esperábamos más. Lo digo desde un lugar que siempre hemos tratado de construir, de ser críticos pero acompañar diferentes posturas en un año que ha sido crítico y duro para todos. Me parece que había que hacer un esfuerzo más, que había que buscar un camino distinto y ser innovadores. Nosotros tenemos una gran preocupación en la ciudad de Santa Fe y es la seguridad, después el empleo y luego la salud, y no veo refrendados ni remarcados estos puntos en el presupuesto, lo cual marca que las prioridades son distintas, que valora cuestiones que no todo el público está teniendo en cuenta”.

***