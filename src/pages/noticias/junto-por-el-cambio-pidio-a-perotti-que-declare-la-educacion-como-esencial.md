---
category: Agenda Ciudadana
date: 2021-04-20T08:33:49-03:00
thumbnail: https://assets.3dnoticias.com.ar/cantard.png
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de LT10
resumen: Junto por el Cambio pidió a Perotti que declare la educación como esencial
title: Junto por el Cambio pidió a Perotti que declare la educación como esencial
entradilla: El diputado nacional Albor Cantard explicó que intentan prevenir una situación
  como la de CABA. En cuanto a este conflicto sostuvo que se deberá a acatar lo que
  defina la Corte Suprema.

---
La Corte Suprema de Justicia resolvió este lunes que el planteo del gobierno de la Ciudad de Buenos Aires contra el decreto de necesidad y urgencia que suspendió las clases presenciales por dos semanas es de "competencia originaria" de ese tribunal.

Eso significa que sólo la Corte Suprema puede entender en el planteo que formuló la administración de Horacio Rodríguez Larreta contra el artículo segundo del decreto 241/2021, que fue el que interrumpió las clases presenciales.

Así lo dispusieron los jueces Carlos Rosenkrantz, Juan Carlos Maqueda, Ricardo Lorenzetti y Horacio Rosatti. La quinta integrante del tribunal, Elena Highton de Nolasco, votó en disidencia.

En este sentido, el diputado Albor Cantard de Juntos por el Cambio por LT10 explicó que se trata de un conflicto federal en el que debe intervenir la Corte Suprema. "La Corte se expedirá y habrá que acatar lo que definan nos guste o no nos guste, porque a algunos les va a gustar más y a otros no", consideró.

En tanto, remarcó que el presidente Alberto Fernández critica a la Justicia porque "confunde y sigue hablando de Capital Federal como si la reforma del '94 no hubiese existido. Justamente el jefe de Gobierno de Ciudad Autónoma tiene las mismas facultades que un gobernador".

**Pedido a Perotti**

Ante la posibilidad que la situación se replique en la provincia de Santa Fe en algún momento debido al aumento de contagios, el legislador nacional comentó que atentos al tema desde Juntos por el Cambio enviaron un pedido al gobernador Omar Perotti para que declare a la Educación un servicio esencial.

"No queremos esperar que el hecho ocurra para reaccionar y encontrarnos en la misma situación que el Área Metropolitana hoy después de un año sin clases presenciales del que no alcanzamos a dimensionar el daño que esto produjo en los niños y niñas", dijo Cantard, quien indicó que no es algo cuantificable como la cantidad de comercios cerrados o los puestos de trabajo perdidos".