---
category: Estado Real
date: 2021-06-14T06:00:47-03:00
thumbnail: https://assets.3dnoticias.com.ar/AGENDA.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Agenda Ambiental: La Provincia continúa fortaleciendo la gestión integral
  de residuos sólidos urbanos con localidades de todo el territorio'
title: 'Agenda Ambiental: La Provincia continúa fortaleciendo la gestión integral
  de residuos sólidos urbanos con localidades de todo el territorio'
entradilla: A través del Ministerio de Ambiente y Cambio Climático, se desarrollaron
  una serie de encuentros con representantes de Bigand, Luis Palacios y Labordeboy
  para articular acciones conjuntas.

---
La provincia de Santa Fe, a través del Ministerio de Ambiente y Cambio Climático, continúa fortaleciendo la Gestión Integral de Residuos Sólidos Urbanos (GIRSU) y el rumbo hacia la transición energética. En esa línea, se llevaron adelante tres encuentros con representantes de las localidades de Bigand, Luis Palacios y Labordeboy.

Las reuniones fueron encabezadas por la ministra de Ambiente y Cambio Climático, Erika Gonnet; junto a la directora de Economía Circular, Saida Caula; y el subsecretario de Tecnologías para la Sostenibilidad, Franco Blatter. El objetivo de los encuentros fue potenciar la agenda ambiental a través de la economía circular y el empleo verde.

En el caso de la localidad de Labordeboy, los representantes de la comuna expusieron su labor cotidiana en torno a la gestión de residuos. Además, se trazaron lineamientos para proyectos futuros en común en esa materia, con especial énfasis en la incorporación de recuperadores urbanos para lograr la inclusión social. En ese sentido, cabe resaltar que el Ministerio actualmente se encuentra gestionando un aporte para la citada localidad, con el fin de afianzar su sistema de gestión de residuos.

Otro punto a destacar es que la cartera trabaja en un modelo de ordenanza para avanzar en la transición energética en dicho territorio. Es por esto que, en el encuentro, también, se acordó el acompañamiento del Ministerio que le permitirá a Labordeboy acceder al uso de energías renovables.

“Junto a los jefes territoriales tenemos el compromiso de fortalecer acciones climáticas vinculadas a la gestión de residuos y al uso de energías renovables, priorizando iniciativas que generan un impacto positivo desde lo ambiental, lo social y lo económico”, resumió la ministra Erika Gonnet. “Por eso hacemos énfasis en la incorporación de los recuperadores urbanos, en la economía, la perspectiva de género y el empleo verde”, continuó.

Por otra parte, se llevó a cabo una videoconferencia con representantes de Bigand, en la que se coordinaron acciones de trabajo conjunto asociadas a la gestión de residuos. Los integrantes de la localidad presentaron sus planes para dicha temática y también para cuestiones vinculadas al arbolado público.

“Por decisión del gobernador Omar Perotti hemos avanzando en un aporte para la localidad de Bigand que fortalecerá la gestión de residuos sólidos urbanos”, contó Gonnet. Y en ese sentido, destacó que “en situación similar concretamos una inversión para la localidad de Luis Palacios que en breve estarán equipando su sistema de GIRSU”.

En cada uno de los encuentros estuvieron presentes los presidentes comunales, Bruno Lommi por Labordeboy, Alejandro Ruggieri por Bigand y Adrián Biyovich por Luis Palacios.