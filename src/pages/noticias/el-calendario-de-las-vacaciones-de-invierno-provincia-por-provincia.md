---
category: Agenda Ciudadana
date: 2021-07-05T06:15:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/CALENDARIOESCOLAR.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: El calendario de las vacaciones de invierno, provincia por provincia
title: El calendario de las vacaciones de invierno, provincia por provincia
entradilla: En Santa Fe las vacaciones de invierno arrancan el 12 de julio, pero por
  el feriado del Día de la Independencia el último día de clases será el jueves 8
  de julio.

---
A partir de la segunda semana de julio, la mayoría de las provincias iniciará el receso escolar de invierno en medio de la pandemia de coronavirus que obligó a implementar sistemas mixtos para el dictado de clases y que, según autoridades educativas, podría recuperar presencialidad si los índices epidemiológicos lo permiten. En Santa Fe las vacaciones de invierno arrancan el 12 de julio.

De acuerdo con el cronograma reportado por las corresponsalías de la agencia Télam en cada provincia, dieciséis distritos iniciarán sus vacaciones el 12 de julio, tras el feriado largo del viernes 9 por el Día de la Independencia; mientras que la Ciudad de Buenos Aires, Buenos Aires, Jujuy, Santa Cruz, Santiago del Estero y Tierra del Fuego lo harán el 19 de julio. Este lunes 5 ya entrarán en receso Río Negro y San Juan.

Las autoridades educativas definieron las fechas de las vacaciones en los últimos días de acuerdo con las condiciones climáticas y su actualidad epidemiológica, luego de que el ministro de Educación de la Nación, Nicolás Trotta, explicara que el Consejo Federal dispuso que “la decisión estará determinada por los indicadores sanitarios de cada una de las jurisdicciones”.

Trotta también adelantó que esperaban “que después del receso invernal sigamos robusteciendo la presencialidad” en las escuelas, pero para eso, advirtió, “tenemos que ser muy cuidadosos y sostener todos los recaudos necesarios” para que tras las vacaciones “los indicadores epidemiológicos no se hayan descompuesto”.

El 12 de julio iniciarán las vacaciones de invierno en Santa Fe, al igual que en Catamarca, Córdoba, Corrientes, Chaco, Chubut, Entre Ríos, Formosa, La Pampa, Mendoza, Misiones, Neuquén, Salta, San Luis, La Rioja y Tucumán.

Santa Fe también extenderá el receso escolar del 12 y hasta el 23 de julio. Sin embargo, debido al feriado nacional por el Día de la Independencia, el último día de clases de la primera parte del calendario escolar será el jueves 8 de julio.

![](https://assets.3dnoticias.com.ar/MAPA.jpg)