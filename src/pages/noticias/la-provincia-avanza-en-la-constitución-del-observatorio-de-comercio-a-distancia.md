---
layout: Noticia con imagen
author: .
resumen: "Observatorio de Comercio  "
category: Estado Real
title: La provincia avanza en la constitución del Observatorio de Comercio a Distancia
entradilla: Se busca relevar información actualizada para conocer las formas en
  las que se desarrolla este tipo de comercio, identificando las necesidades del
  sector con el fin de impulsar acciones y proyectos que lo favorezcan.
date: 2020-11-18T13:33:36.453Z
thumbnail: https://assets.3dnoticias.com.ar/comercio-a-distancia.jpeg
---
En el marco del Programa Provincial de Modernización del Sector de Comercio y Servicios, el Gobierno de Santa Fe, a través de la Secretaría de Comercio Interior y Servicios del Ministerio de Producción, Ciencia y Tecnología, realizó este lunes una videoconferencia en la cual se presentó el **proyecto del Observatorio de Comercio a Distancia**. 

Se trata de una **herramienta que permitirá relevar información actualizada para conocer las formas en las que se desarrolla este tipo de comercio**, identificando las necesidades del sector con el fin de impulsar acciones y proyectos que lo favorezcan.

“Estamos presentando el Observatorio a todas las entidades comerciales y áreas de producción de la provincia. Se trata de un proyecto que apunta a recolectar la mayor cantidad de información posible para verificar el nivel de introducción que tienen los distintos sectores de la actividad comercial con el comercio electrónico, con el comercio a distancia y la transformación digital”, destacó luego del encuentro, el secretario de Comercio Interior y Servicios, Juan Marcos Aviano.

Entre los principales objetivos del Observatorio se encuentra el de **construir una muestra primaria como prueba piloto en los sectores representativos del comercio provincial e implementar una muestra sobre el total de los casos en la provincia**.

Asimismo, Aviano indicó: “En un trabajo articulado entre el Instituto Provincial de Estadísticas y Censos (IPEC) y el Ministerio de Producción, Ciencia y Tecnología, podremos llevar adelante toda la información que nos permita contar con esos elementos que diseñen los proyectos del año que viene, que tienen que ver con el Programa Provincial de Modernización del Sector de Comercio y Servicios”.

Además del mencionado funcionario provincial,  participaron en la actividad: la directora Provincial de Promoción de la Competencia y Defensa del Consumidor, María Betania Albrecht, el director Provincial de Estadísticas y Censos, Gabriel Frontons y representantes de entidades comerciales y áreas de producción de municipios y comunas de la provincia.