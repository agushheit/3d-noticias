---
category: Estado Real
date: 2021-03-24T07:36:51-03:00
thumbnail: https://assets.3dnoticias.com.ar/patente.png
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: 'Patente automotor: cómo abonar los próximos vencimientos'
title: 'Patente automotor: cómo abonar los próximos vencimientos'
entradilla: 'La Administración Provincial de Impuestos (API) difundió instructivos
  para que los contribuyentes puedan pagar el tributo utilizando la credencial que
  figura en las boletas distribuidas. '

---
La Administración Provincial de Impuestos (API) recordó que los contribuyentes podrán abonar el tributo sobre la patente automotor con la misma boleta ya distribuida, utilizando la credencial de pago. La misma se encuentra en el vértice inferior derecho de cada boleta. También es posible bajar la credencial desde la página oficial del gobierno santafesino (www.santafe.gov.ar/api), en el apartado “trámites”.

El impuesto a pagar no superará el 40% con relación al impuesto determinado en 2020, en tanto para los vehículos 0 kilómetros el límite de incremento será de 30% respecto de iguales modelos del año anterior.

En todos aquellos casos donde las liquidaciones originarias no superaban dicho tope, el monto a pagar será el mismo que se encuentra reflejado en la boleta, salvo en aquellos casos donde los municipios y comunas hayan efectuado modificaciones por diferencias detectadas de años y/o modelos de los rodados, ya sea de oficio o a requerimiento del titular.

Además, continúan vigentes todos los medios electrónicos de pago. Quienes se encuentren adheridos al débito automático verán registrado el débito del nuevo valor en las fechas de vencimientos y quienes abonen por medio de su homebanking verán reflejada en su agenda de pagos el nuevo valor liquidado.

Asimismo, los contribuyentes que deseen controlar el valor de las cuotas o el pago total anual podrán hacerlo por medio de la página de API (www.santafe.gov.ar/api), generando la liquidación a través de la credencial de pago.

**Compensación automática**

En relación a quienes hayan abonado cuotas en exceso, la API reiteró que las diferencias se compensarán automáticamente con la cuota tercera, cuarta y subsiguiente si aún persiste el saldo a favor del contribuyente.

Todos aquellos que realizaron el pago del total anual, podrán solicitar la devolución de lo abonado de más ingresando en el box destacado (“Patente Única sobre Vehículos – Solicitud Reintegro Pago Total Anual y Proporcional 2021”) de la página web del organismo. El aplicativo dispuesto requiere que cada solicitante informe sus datos particulares y registre una CBU donde la Administración pueda transferir los fondos. También, podrán solicitar la devolución mediante la presentación por escrito ante las distintas dependencias del API, sin necesidad de turnos previos.

**Vencimientos**

Mediante el dictado de la Resolución General - API Nº 16/2021, se han determinando los nuevos vencimientos de las cuotas 1 a 6 y del pago total anual:

* Cuota 1: entre el 5 y 9 de abril / 2021
* Cuota 2: entre el 16 y 23 de junio / 2021
* Cuota 3: entre el 18 y 24 de agosto / 2021.
* Cuota 4: entre el 16 y 22 de septiembre / 2021
* Cuota 5: entre el 16 y 23 de noviembre / 2021
* Cuota 6: entre el 15 y 21 de diciembre / 2021

El vencimiento del pago total anual opera entre el 5 y 9 de abril / 2021.

[Instructivo credencial de pago](https://www.santafe.gov.ar/noticias/recursos/documentos/2021/03/2021-03-23NID_270392O_1.pdf)

[Instructivo reintegro de pago total anual 2021](https://www.santafe.gob.ar/noticias/recursos/documentos/2021/03/2021-03-23NID_270392O_2.pdf)