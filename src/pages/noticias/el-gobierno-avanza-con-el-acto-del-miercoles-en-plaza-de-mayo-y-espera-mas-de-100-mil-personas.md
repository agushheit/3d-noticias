---
category: Agenda Ciudadana
date: 2021-11-16T06:00:16-03:00
thumbnail: https://assets.3dnoticias.com.ar/MILITANCIA.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: El Gobierno avanza con el acto del miércoles en Plaza de Mayo y espera "más
  de 100 mil personas"
title: El Gobierno avanza con el acto del miércoles en Plaza de Mayo y espera "más
  de 100 mil personas"
entradilla: '"Estamos de pie y unidos", será uno de los mensajes del acto por el Día
  de la Militancia, que incluirá a la CGT, organizaciones sociales y el kirchnerismo.

'

---
Aliviado tras recortar el margen de la derrota electoral, el Gobierno cerró hoy los detalles del acto del miércoles en Plaza de Mayo por el Día de la Militancia con la CGT y los movimientos sociales, que al final incluirá al ala más kirchnerista y se realizará bajo el lema: "Estamos de pie y unidos".

En una reunión en la sede del PJ nacional en la calle Matheu, se reunieron dirigentes de todos los sectores del Frente de Todos para acordar la organización de la marcha que cerrará el presidente Alberto Fernández con un discurso frente a la Rosada.

La ceremonia será a las 15:00, con el jefe de Estado como único orador y los dirigentes peronistas esperan más de 100 mil personas en el lugar, ante las cuales se difundirá un documento conjunto haciendo referencia al discurso del domingo del Presidente sobre la segunda etapa de gobierno.

Se definió además que en Diagonal Sur concentrarán los integrantes de los gremios, en Avenida de Mayo lo harán los movimientos sociales y en Diagonal Norte los intendentes y La Cámpora.

De la reunión participaron el jefe de Gabiente, Juan Manzur; el ministro del Interior, Eduardo de Pedro; el canciller Santago Cafiero; los miembros del nuevo triunvirato de la CGT, Héctor Daer, Carlos Acuña y Pablo Moyano; Andrés Larroque; Hugo Yasky; Julio Vitobello; Emilio Pérsico; Daniel Menéndez y Andrés Rodríguez, entre otros.

El resultado de las elecciones del domingo, que achicaron la diferencia de la derrota, modificó el sentido original de la marcha que había sido motorizada por la CGT y los movimientos para "blindar" al Presidente frente al sector kirchnerista en momentos en que se esperaba una caída electoral por un margen más amplio.

Ahora, derivó en una movilización con todos los sectores del Frente de Todos para poner en marcha lo que aseguran que será un relanzamiento del gobierno.