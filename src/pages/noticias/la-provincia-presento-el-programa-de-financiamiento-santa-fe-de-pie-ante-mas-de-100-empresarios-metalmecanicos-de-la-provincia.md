---
category: Estado Real
date: 2021-07-12T06:00:21-03:00
thumbnail: https://assets.3dnoticias.com.ar/SANTAPIE.png
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Provincia presentó el programa de financiamiento Santa Fe de Pie ante
  más de 100 empresarios metalmecánicos de la provincia
title: La Provincia presentó el programa de financiamiento Santa Fe de Pie ante más
  de 100 empresarios metalmecánicos de la provincia
entradilla: Del encuentro participaron todas las cámaras y entidades que aglutinan
  a representantes del sector.

---
El ministro de Producción, Ciencia y Tecnología, Daniel Costamagna encabezó este jueves un encuentro, a través de una videoconferencia, en que se presentaron las líneas de financiamiento del Programa Santa Fe de Pie que están disponible para el sector industrial de todo el territorio provincial.

Santa Fe de Pie es una herramienta que tiene como objetivo acompañar a los sectores más afectados en el marco de la pandemia COVID-19 y al mismo tiempo reactivar la economía santafesina, incentivando las inversiones y el crecimiento del entramado productivo. Entre sus puntos más destacados el Programa presenta distintas líneas de financiamiento destinada a la inversión productiva para la compra de bienes de capital en los sectores industrial, agroindustrial y de servicios.

Durante el encuentro, Costamagna detalló: “Presentamos un Programa de Financiamiento que no tiene precedentes en la provincia de Santa Fe, no sólo por el monto, 26.500 millones de pesos, sino también por la particularidad del mismo, que tiene que ver con generación de arraigo y empleo, con la inclusión y con el aumento de la productividad. Tal como lo dice nuestro gobernador Omar Perotti, no hay mejor política social que la generación de un puesto de trabajo. Es por esto que tomamos la fehaciente decisión de acompañar a todos los sectores, con inversiones como estas”.

“Uno de nuestros principios, como gestión, es poder acompañar a los sectores con este tipo de herramientas financieras, en esto se refleja el espíritu de este tipo de programas que son sustanciales para el desarrollo, el crecimiento y la transformación de la producción”, agregó.

Por su parte, el secretario de Industria, Claudio Mossuz, indicó: “Es un orgullo poder estar hoy aquí plasmando esto a todas las cámaras metalúrgicas de nuestra provincia que, por suerte, son muchas. Esto demuestra la capacidad productiva que tiene Santa Fe. Hoy, en plena pandemia, que tengamos liderazgo a nivel nacional como provincia demuestra la importancia de las pymes en el desarrollo de nuestra economía”.

De la actividad participaron también el subsecretario de Pequeña y Mediana Industria e Innovación, Marcelo Comelli; los directores provinciales de Infraestructura y Fortalecimiento Industrial, Marcelo Cogno, y de Innovación Productiva, Enrique Bertini; y representantes de la Asociación Industriales Metalúrgicos de Rosario; del Centro Industrial de Las Parejas; de la Cámara de Industriales Metalúrgicos de Esperanza; de la Cámara de Industrias Metalúrgicas del Centro de la Provincia de Santa Fe; del Centro Económico de Firmat; de la Red de Centros Tecnológicos ADIMRA; y de la Cámara de Industriales Metalúrgicos de Rafaela.

**HERRAMIENTAS DE FINANCIAMIENTO**

En la presentación desde el equipo de la Secretaría de Industria brindaron detalles del financiamiento que el Gobierno Provincial puso en marcha a partir de un convenio con el Banco Nación. Específicamente se abordaron las líneas destinadas al sector industrial, como la instrumentada para la construcción de naves industriales (Programa Construyendo Industria) en parques, áreas industriales y zonas industriales o habilitadas, determinadas por la comunas o municipios y para la adquisición de tecnología y/o equipamiento industrial necesario para incrementar la producción de la actividad que registra.

Además, desde la mencionada área, también se informó sobre la recientemente lanzada línea crediticia para pymes. Se trata de un financiamiento puesto en marcha en conjunto con el Ministerio de Desarrollo Productivo de la Nación y el Nuevo Banco de Santa Fe; destinado a la inversión productiva para la compra de bienes de capital en los sectores industrial, agroindustrial y de servicios. Dichos préstamos cuentan con un fondo total de 400.000.000 de pesos y un monto máximo por pyme 20.000.000; las garantías son otorgadas a través de FOGAR y tiene plazos de hasta 61 meses, incluido el período de gracia, de 6 meses. La tasa de referencia es del 30% con bonificación de tasas a cargo del Fondep: 8% anuales, durante los primeros veinticuatro (24) meses del préstamo. Tasa final: 22% en los primeros 24 meses.