---
category: Agenda Ciudadana
date: 2021-12-05T06:00:32-03:00
thumbnail: https://assets.3dnoticias.com.ar/VIZZOTTIDELTA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: Carla Vizzotti aseguró que es “inevitable” que la variante Ómicron se propague
  en la Argentina
title: Carla Vizzotti aseguró que es “inevitable” que la variante Ómicron se propague
  en la Argentina
entradilla: 'La ministra de Salud también habló sobre la posibilidad de que la vacuna
  contra en COVID-19 sea obligatoria.

'

---
La ministra de Salud, Carla Vizzotti, habló este sábado sobre la variante Ómicron, dominante en África y que preocupa a todo el mundo. Tan pronto como la OMS la declaró como una “variante de preocupación”, la mayoría de los países comenzaron con una serie de restricciones para evitar su propagación.

En el caso de la Argentina, la funcionaria nacional declaró que es “inevitable que llegue al país y se propague como lo hizo la Delta”. Respecto a esto, mencionó la posibilidad de que la vacuna contra el COVID-19 sea obligatoria. Aunque también destacó que el país se encuentra en una “situación epidemiológica favorable gracias a la alta tasa de vacunación”.

Respecto a la variante Ómicron, comentó que todavía no hay tanta información al respecto: “La evolución natural del virus es que sea más transmisible y menos letal. Va a ser un virus estacional que va a pasar a ser como la influenza”. Y aclaró que puede haber un rebrote en marzo, pero que no depende de un solo factor: “Algunos factores son externos como el caso de las nuevas cepas en otros países. Si bien hay un aumento de casos, es leve gracias a la vacunación”.

Para la titular de la cartera sanitaria, es fundamental vacunar a quienes no han cumplido con el esquema todavía y dar las terceras dosis a quienes ya tienen dos. “Esperamos para marzo podamos tener a la mayor parte de la población inmunizada y que no necesitemos ninguna acción más”, agregó.

Por otra parte, la integrante del Gabinete se refirió al pase sanitario y a la posibilidad de que se establezca la obligatoriedad de la vacunación contra el Sars-Cov-2: “No es la idea que el Pase Sanitario termine en una vacunación obligatoria. La idea es que sea una herramienta para las provincias y municipios; los distritos locales pueden ampliar el espectro de la medida”.

Actualmente, el Ministerio de Salud está trabajando para estimular la vacunación y disminuir el peligro en los lugares de mayor riesgo, que son los eventos masivos. “Siete millones de personas están en condiciones de recibir la segunda dosis, y son clave, porque se movilizan y son factores de contagio”, subrayó.

El Pase Sanitario es un certificado en el que consta que una persona mayor a 13 años ha recibido las dosis de vacunas necesarias y habilita al ciudadano a poder asistir a eventos masivos, lugares cerrados y espacios de mayor participación. Esto será necesario para megaeventos, tanto abiertos como cerrados. “Hoy tenemos 100% de aforo, canchas, recitales, y la manera de tener todo esto es el Pase Sanitario”, explicó Vizzoti.

**La variante Ómicron llega a Brasil, cerca del límite con Argentina**

El país vecino ha confirmado un nuevo caso de una mujer que estuvo recientemente en Sudáfrica. Actualmente, se encuentra en aislamiento en su residencia de Santa Cruz do Sul, estado que limita con la Argentina.

La mujer contaba con dos dosis de la vacuna contra el COVID-19 y presentó fiebre como síntoma. Asimismo, las autoridades sanitarias están en busca de los contactos estrechos con los que mantuvo en los últimos días para analizarlos y evitar la propagación.

Además de Brasil, México es otro de los países que detectó la nueva cepa en su región. La Organización Mundial de la Salud la ha calificado como "muy preocupante", mientras que expertos han afirmado que es de alta contagiosidad.