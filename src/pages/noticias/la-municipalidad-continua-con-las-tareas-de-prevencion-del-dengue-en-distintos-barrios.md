---
category: La Ciudad
date: 2021-02-02T05:15:23-03:00
thumbnail: https://assets.3dnoticias.com.ar/dengue.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad de Santa Fe
resumen: " La Municipalidad continúa con las tareas de prevención del dengue en distintos
  barrios"
title: " La Municipalidad continúa con las tareas de prevención del dengue en distintos
  barrios"
entradilla: 'La campaña de difusión y descacharrado asistido se realizó en Alberdi,
  San Martín, Belgrano y Pompeya, y seguirá por San Agustín, Chalet, Centenario y
  en otros sectores de la ciudad. '

---
Durante todo enero, se continuó con la acción de descacharrado asistido en el barrio Alberdi en María Selva, Belgrano, Pompeya y San Martín, en el marco de la campaña de prevención del dengue en territorio. Está previsto que esta tarea siga en San Agustín, Chalet y Centenario y otros sectores de la ciudad. Además se realizan jornadas de capacitación para vecinos y profesionales.

Previamente al trabajo en el lugar, funcionarios de las Secretarías de Políticas de Cuidado y de Ambiente de la Municipalidad coordinaron las acciones con los promotores ambientales que llevarán adelante las tareas de prevención junto a personal municipal. Se trata de vecinas y vecinos que trabajan en asociaciones y cuadrillas de higiene urbana de los distintos barrios y que fueron capacitados con anterioridad.

Esta tarea es una continuación de las que se vienen realizando a lo largo de todo el año, pero se intensifican los trabajos de acuerdo a la temporada de altas temperaturas porque conviene eliminar los reservorios de huevos y larvas de Aedes Aegypti, el mosquito transmisor que convive en los hogares y no en los espacios públicos.

El subdirector de Promoción de la Salud, Juan Picato, contó que “se trata de un trabajo conjunto entre las áreas de Políticas de Cuidados y Acción Social, y de Ambiente; y también participan los coordinadores de distrito y de las instituciones del barrio”. Además aclaró que estos primeros barrios donde se inició esta acción fueron elegidos porque el sistema de monitoreo del aedes que tiene la Municipalidad “nos está dando mayor presencia del vector”, dijo.

En cuanto a qué se charla con los vecinos en el lugar, Picato contó: “La acción tiene una parte que es la consejería en cuanto a cuidados y prevención dándoles información al vecino y a la vecina sobre la característica general del aedes, cómo puede transmitir la enfermedad del dengue, donde se cría, en qué cosas tiene que estar atento para evitar eso; y si la persona nos permite ingresar al domicilio, lo hacemos con ellos y les explicamos en qué consiste el descacharrado”.

Picato anticipó que actualmente no hay circulación viral, pero “ya tenemos los primeros casos en el norte del país; entonces le damos esa información al vecino para que esté atento por si tiene que viajar o recibe personas de esos lugares”.

**Trabajo conjunto**

La Municipalidad no detiene esta acción durante el año y Picato aseguró que el vecino y la vecina viene internalizando todos los mensajes de cuidados, por lo que la acción se torna más eficaz y sencilla. “Somos una ciudad que hemos estado afectados por dengue en los dos últimos años. Se vienen desarrollando desde la Municipalidad fuertes intervenciones de prevención y vemos que eso empieza a dar resultado”, dijo Picato.

En esta línea, manifestó: “Para que las acciones de prevención de dengue sean eficaces se necesitan tanto las políticas públicas activas que son las que estamos desarrollando desde la Municipalidad con los equipos de promoción, con las tareas de desmalezamiento, con las acciones conjuntas con las áreas de Ambiente, pero también se necesita un fuerte involucramiento de la ciudadanía y eso se está notando”.

A la par de las acciones de descacharrado se llevan adelante jornadas de capacitación sobre dengue tanto en los barrios como con profesionales. La semana pasada se realizó en barrio Centenario y esta semana, el jueves, está prevista en La Guardia. Además, también el jueves pero de manera virtual, se realizará un webinar acerca de la puesta en escena y actualización de dengue como diagnóstico diferencial en tiempos de pandemia destinado a profesionales de la salud, que se realiza con el Colegio de Médicos de Santa Fe.

**Promotores ambientales**

Este año, la novedad es que se cuenta con la colaboración de los promotores ambientales para prevenir el dengue. En ese sentido, Picato destacó el trabajo con los integrantes de las cuadrillas de higiene urbana, que trabajan con los vecinos limpiando su barrio y tienen un diálogo muy fluido con ellos y eso posibilita llegar a todos los sectores. Cabe recordar que en total son 26 las microempresas sociales de higiene urbana que se encuentran trabajando en 27 barrios de la ciudad.

“Esas asociaciones van a incorporar la práctica de descacharrado y la van a hacer durante todo el año de manera sistemática”, contó. Cabe mencionar que una vez que se recorren las casas y se quitan los objetos que puedan acumular agua, camiones del municipio recogen la basura. Asimismo, otra cuadrilla se encarga de cortar los yuyos y malezas.