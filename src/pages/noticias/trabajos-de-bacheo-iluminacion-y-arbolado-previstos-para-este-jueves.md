---
category: Agenda Ciudadana
date: 2022-06-23T07:36:53-03:00
thumbnail: https://assets.3dnoticias.com.ar/bacheo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: Trabajos de bacheo, iluminación y arbolado previstos para este jueves
title: Trabajos de bacheo, iluminación y arbolado previstos para este jueves
entradilla: La Municipalidad informa el avance del cronograma de tareas previstas
  en los planes de bacheo y Santa Fe se Ilumina. Además, se suman los trabajos programados
  en el arbolado público.

---
La Municipalidad avanza con el plan de bacheo diseñado para reparar más de 12.000 metros cuadrados de calles de la capital provincial. El programa se desarrolla por fases, priorizando las zonas de la ciudad que evidencian mayor circulación.

Actualmente se trabaja en:

* Ituzaingó, entre Alberdi y Mitre
* Ituzaingó, entre Güemes y Avellaneda
* Ituzaingó, entre Avellaneda y Dorrego
* Balcarce, entre Lavalle y Güemes
* Tucumán, entre Francia y Urquiza
* Saavedra, entre Primera Junta y Mendoza
* Callejón Aguirre, entre Europa y Aguado
* Callejón Funes, entre Aguado y Peñaloza
* Ecuador y La Paz

**Iluminación**

En el marco del plan Santa Fe se Ilumina, el municipio concreta tareas tendientes a recuperar el alumbrado existente en toda la ciudad. Cuadrillas municipales trabajan en la colocación de nuevas columnas de iluminación y artefactos led en distintos barrios y avenidas, como así también, en el mantenimiento de la red actual de la ciudad.

En ese sentido, se realizará el mantenimiento del sistema eléctrico, entre otros puntos, en:

* Plaza El Vergel
* Plazoleta 3 de Junio
* Plaza Rebeck
* Zona del ISEF
* Costanera Este
* Fuentes de la ciudad
* Barrio El Pozo
* Distrito de la Costa

**Trabajos en el arbolado público**

La Municipalidad recuerda que los trabajos de poda del arbolado público sólo se concretan a los efectos de despejar cámaras de seguridad y facilitar la visualización, o ante la colocación de nuevas columnas de alumbrado. Del mismo modo, esta tarea recibe la denominación de poda sanitaria cuando los ejemplares poseen ramas secas y/o colgantes que representan un peligro para los peatones o el tránsito.

Mañana se realizarán labores de este tipo en los barrios 7 Jefes y Guadalupe.