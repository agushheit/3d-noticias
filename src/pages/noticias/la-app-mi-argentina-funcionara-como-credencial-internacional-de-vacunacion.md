---
category: Agenda Ciudadana
date: 2021-09-07T06:15:28-03:00
thumbnail: https://assets.3dnoticias.com.ar/CERTIFICADO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: La app Mi Argentina funcionará como credencial internacional de vacunación
title: La app Mi Argentina funcionará como credencial internacional de vacunación
entradilla: Será a través de un carnet digital de la aplicación, funcionando como
  documento oficial que acredita internacionalmente la vacunación desde este lunes.
  Estará disponible en inglés y en español.

---
La credencial digital de vacunación Mi Argentina funcionará desde hoy como documento oficial de acreditación internacional de la aplicación de la vacuna contra el coronavirus, medida que está siendo comunicada a las cancillerías de los otros países, direcciones de migraciones y autoridades de frontera, reportó el Ministerio de Salud.

El carnet digital está disponible tanto en español como en inglés con el objetivo de ser presentado en el exterior por aquellas personas que deban desplazarse entre países. Además, las autoridades nacionales solicitaron que los países envíen sus modelos de credencial para verificar la documentación de las personas que deseen ingresar al país.

La aplicación en Mi Argentina está disponible para todas aquellas personas que se hayan vacunado, dado que se genera a partir de los datos cargados en el Registro Federal de Vacunación Nominalizado (Nomivac).

Para ello se debe descargar la aplicación desde el sitio web [https://www.argentina.gob.ar/aplicaciones/mi-argentina,](https://www.argentina.gob.ar/aplicaciones/mi-argentina, "https://www.argentina.gob.ar/aplicaciones/mi-argentina,") crear la cuenta en [https://id.argentina.gob.ar/registro/](https://id.argentina.gob.ar/registro/ "https://id.argentina.gob.ar/registro/") y validar la identidad en [https://www.argentina.gob.ar/miargentina/validar-identidad.](https://www.argentina.gob.ar/miargentina/validar-identidad. "https://www.argentina.gob.ar/miargentina/validar-identidad.")

La credencial contiene nombre, apellido, número de DNI, datos de la vacuna, nombre, lote y cantidad de dosis, el lugar y fecha donde te fue aplicada.

También se informó que cada jurisdicción es la responsable de la carga de información de cada persona que se vacuna y que luego aparece en la credencial de Mi Argentina. En caso de errores en la credencial, en los datos o de la vacuna, la persona puede reportarlo al sistema nacional de salud para que lo analice y -junto a la jurisdicción correspondiente- realicen la rectificación que sea necesaria.

En simultáneo, se requirió que los demás países informen cuáles son sus modelos de credencial de vacunación en pos de avanzar en la verificación de la documentación de las y los viajeros que deseen ingresar a la Argentina.

Cancillería aclaró que, ante la variabilidad de exigencias de los países para viajes internacionales, y considerando que la Organización Mundial de la Salud (OMS) aún no se pronunció sobre una certificación de alcance global, se recomienda a las personas que viajen al exterior consultar las medidas sanitarias vigentes.

También, las personas deberán informarse sobre la documentación de viaje requerida por el país de destino al momento de realizar el viaje, y conservar la documentación física entregada por las autoridades de Salud al momento de recibir las dosis de vacunas "para poder complementar y/o acreditar su vacunación contra el SARS-CoV-2 si les fuera solicitado".