---
category: Estado Real
date: 2021-04-02T08:03:12-03:00
thumbnail: https://assets.3dnoticias.com.ar/obras.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: La Provincia dispuso más de $23 millones para obras en el departamento La
  Capital
title: La Provincia dispuso más de $23 millones para obras en el departamento La Capital
entradilla: Las localidades de Arroyo Leyes, Sauce Viejo, Emilia, Llambi Campbell
  y la vecinal Pro Adelantos Barranquitas de Santa Fe, recibieron fondos en el marco
  del Plan Incluir.

---
La ministra de Infraestructura, Servicios Públicos y Hábitat, Silvina Frana, encabezó el acto de entrega de fondos a cinco localidades del departamento La Capital, en la que Arroyo Leyes podrá construir su cementerio; en Sauce Viejo se realizará una reparación integral del Balneario “Don Roque”; en Emilia se construirá una piscina semiolímpica; para Llambi Campbell los fondos significan una nueva área especial para vehículos de gran porte; y finalmente con los aportes para la vecinal Pro Adelantos Barranquitas de Santa Fe se podrá hacer realidad un playón polideportivo.

En este sentido, la ministra Silvina Frana señaló: “Desde el gobierno provincial, a través del Plan Incluir, estamos dándole a los vecinos de estas cinco localidades, la buena noticia de que en el corto plazo podrán verse concretadas obras que en algunos casos son viejos anhelos, sueños y en otros necesidades concretas de la comunidad”.

“Y esto lo hacemos porque el gobernador Omar Perotti, desde el primer día de gestión, nos pidió que nos aboquemos a hacer realidad obras como estas que son cercanas a los santafesinos, obras que tienden a incluir, a generar arraigo y mejoran la calidad de vida de la gente”, añadió la ministra.

Por su parte, el presidente comunal de Emilia, Esteban Panigo manifestó: “Estamos realmente contentos por recibir estos fondos del gobierno provincial, para poder concretar una pileta semiolímpica que se inscribe dentro del Plan Incluir y que es una respuesta concreta, que se suma a tantos otros los pedidos de obras que venimos realizando desde hace tiempo, pero que en esta gestión se están haciendo realidad”.

A su turno, el presidente comunal de Llambi Campbell, Adrián Tagliari indicó: “Para nosotros el parque automotor de gran porte que proyectamos es sumamente necesario, dado que en nuestra localidad la principal actividad es la agrícola y es permanente el tráfico de camiones. Hace muchos años que teníamos este proyecto presentado y no encontrábamos la manera que el gobierno anterior nos financie estas obras tan imprescindibles para lograr este ordenamiento. Así que estamos muy contentos y agradecidos al gobernador Omar Perotti y a la ministra Silvina Frana”.

En el mismo sentido, el secretario de gobierno de Sauce Viejo, Gustavo López Torres, afirmó que “las reparaciones que vamos a realizar con estos fondos en el Balneario Don Roque, son de suma importancia porque mejorará la infraestructura, generará mayor concurrencia de turistas e incentivará al comercio local. Hacía mucho tiempo que no recibíamos aportes de la provincia, y éste es uno de los primeros que nos entregan luego de seis años”.

Finalmente, el presidente de la vecinal, Pro Adelanto Barranquitas, Alfredo Penissi, explicó que estas obras del playón polideportivo se encuentran dentro del marco del Plan Incluir. “Esto no solo va a redundar en beneficio de nuestra institución, sino de todos los barrios aledaños a nuestra jurisdicción barrial. Y esto se debe gracias a las gestiones realizadas por la ministra Frana y la decisión política del gobernador Omar Perotti, de realizar este aporte para poder hacer realidad un anhelo de años que venimos soñando los vecinos, para acercar y contener a los niños y jóvenes de la zona”.