---
category: Estado Real
date: 2021-05-18T09:34:30-03:00
thumbnail: https://assets.3dnoticias.com.ar/controles.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Diario Uno
resumen: Provincia analiza suspender las clases presenciales y cerrar bares y comercios
  ante la ocupación de camas críticas
title: Provincia analiza suspender las clases presenciales y cerrar bares y comercios
  ante la ocupación de camas críticas
entradilla: Los anuncios se harían este martes o miércoles por la tensión en el sistema
  de salud. También se podría bajar aún más el horario de circulación vehicular

---
La provincia anunciará nuevas restricciones antes del viernes para intentar bajar los contagios de Covid y darle un respiro al sistema de salud que está al borde del colapso. Este fin de semana prácticamente hubo ocupación plena de las camas críticas en las ciudades de Rosario, Santa Fe y Rafaela. En el caso de la capital provincial muchas se debieron a siniestros viales o hechos policiales que se podrían haber evitado si había restricciones y controles más duros.

Ahora, el gobernador Omar Perotti se reunió con los intendentes de las principales ciudades de la provincia para consensuar las nuevas medidas que serían anunciadas entre este martes y miércoles. Perotti tuvo varias reuniones durante la jornada y varios expertos le pidieron profundizar las acciones para bajar la circulación.

Según La Capital, no se descarta suspender las clases al menos por diez días y llevarlas a la modalidad virtual en todos los niveles, cerrar la gastronomía a la noche, suspender las actividades recreativas y deportivas y seguir con la prohibición de la circulación vehicular desde las 20, e incluso bajar ese horario límite.

“Tenemos que centrarnos en disminuir los encuentros sociales, que es donde se están produciendo la mayoría de los contagios”, indicó el intendente, Pablo Javkin, al término del encuentro que continuará este martes.

Es que las medidas restrictivas están siendo definidas en las más altas esferas de gobierno y lo que más preocupa es que el sistema sanitario no da más. En ese sentido, los directores de los hospitales de las ciudades más importantes de la provincia fueron taxativos: le dijeron a la ministra de Salud, Sonia Martorano, que el sistema ya “no tiene capacidad de atención”.

Paralelamente, se espera que el gobierno nacional también tome una definición al respecto y el presidente, Alberto Fernández, se reunió con sus colaboradores para empezar a delinear un nuevo Decreto de Necesidad y Urgencia (DNU) que fije restricciones más duras al menos durante los próximos diez días en las zonas del país donde la pandemia está golpeando más fuerte. En tal sentido, Rosario es considerada una de las zonas con alto riesgo epidemiológico.

Este lunes fue un día agitado. Durante la mañana las primeras líneas del Ministerio de Salud provincial se animaron por primera vez en más de un año de pandemia a pronunciar la palabra "colapso". Hasta ahora el solo pensarla generaba prurito.

Los números fueron lapidarios: 100 por ciento de ocupación de camas críticas en Rosario, tanto en el sector público como en el privado y un gran movimiento de gente en las calles que se torna propicio para que el virus se siga expandiendo.

Además, durante esta segunda ola quienes están necesitando internación son pacientes con un promedio de edad de 50 años, que demandan una ocupación de camas estimada en unos 21 días.

Uno de los asesores del gobernador Perotti en términos sanitarios, el ex ministro de Salud, Miguel Angel Capiello, no anduvo con vueltas. Directamente pidió volver a Fase 1 durante diez días.

Eso es lo que analizaron este lunes durante un encuentro virtual el gobernador y los intendentes de las principales ciudades de la provincia.

Durante el encuentro se analizaron diferentes variables pero a todos los unió la convicción de que hay que cerrar actividades. La lupa está puesta en los encuentros sociales y en la circulación.

No fueron pocas las voces que pidieron que el toque de queda sea antes de las 20, como hasta ahora, y que se profundice los fines de semana, que es cuando más gente se reúne en casas particulares.

Las medidas seguirán bajo análisis este martes, en un nuevo encuentro que el gobernador mantenga con los intendentes.