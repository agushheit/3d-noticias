---
category: Agenda Ciudadana
date: 2021-01-23T09:54:09Z
thumbnail: https://assets.3dnoticias.com.ar/ife.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Con información de Infobae
resumen: El Gobierno confirmó que no habrá más ni ATP ni IFE
title: El Gobierno confirmó que no habrá más ni ATP ni IFE
entradilla: El ministro de Economía aseguró que se verifican cuatro meses de reactivación
  “heterogénea”, que los programas de ayuda no continuarán y que “evolucionaron” hacia
  otros como el Repro

---
El ministro de Economía Martín Guzmán disertó en Chaco y destacó que el Presupuesto tiene como objetivo la recuperación inmediata y aumentar la capacidad productiva en un plazo mediano para no chocar con restricciones.

El ministro de Economía, Martín Guzmán, aseguró hoy que “tenemos una economía que lleva cuatro meses en recuperación” a la que consideró “heterogénea”, y afirmó que el Presupuesto nacional “debe ser el corazón de la estrategia económica para la recuperación”.

Lo hizo en su arribo hoy a Resistencia, la capital chaqueña, para participar junto al gobernador Jorge Capitanich de reuniones con empresarios y de la asunción del nuevo ministro de Planificación, Infraestructura y Economía de la provincia, Santiago Pérez Pons.

Pero más allá de los datos de la macroeconomía, Guzmán buscó dar por finalizada una discusión que desde hace días cruza al gabinete económico y señaló que los programas de ayuda Ingreso Familiar de Emergencia (IFE) y el de Asistencia de Emergencia al Trabajo y la Producción (ATP) no continuarán.

> “Hay ciertas medidas que se adoptaron en el contexto de las restricciones más fuertes para la circulación. Esas restricciones hoy no están presentes, han cambiado. De modo que las medidas deben ser otras” dijo el ministro.

En cuanto al ATP, Guzmán señaló que “se implementó durante 2020 para proteger al trabajo y la producción, y se ha evolucionado a lo que se conoce como el sistema REPRO. Ahora se continuará con este programa, que se ha ampliado para atender las necesidades de los sectores críticos, que son distintos del que constituía el conjunto de sectores críticos al principio de la pandemia”.

Respecto del IFE, el titular del Palacio de Hacienda dijo: “Lo mismo ocurre con las medidas para proteger a los sectores más vulnerables. El IFE fue una medida para las restricciones más duras en un contexto de circulación que hoy no están. Hoy los programas son otros”.

> **El IFE fue una medida para las restricciones más duras en un contexto de circulación que hoy no están**

A partir de eso, Guzmán apuntó hacia el presupuesto en donde dijo que la ley para el 2021 “debe ser el corazón de la estrategia económica para la recuperación, debemos construir una cultura más profunda de planear”, dijo el ministro Guzmán hoy al visitar la provincia del Chaco.

En ese sentido, destacó que “el Presupuesto tiene como objetivo la recuperación inmediata y aumentar la capacidad productiva en un plazo mediano para no chocar con restricciones” y que en la norma se “incluyó un aumento de la inversión en inclusión social activa, lo que nos permite contar con programas para el desarrollo social, para proteger los sectores más vulnerables. Pero nuevamente, hoy los programas son otros”.

En otro pasaje de la conferencia se refirió al déficit con motivo de los números publicados en estos días y dijo que el Gobierno estableció “un sendero en el cual el Presupuesto 2021 es el primer paso en lo que buscamos cumplir dos cuestiones al mismo tiempo: primero, que el Estado tenga un rol activo en la recuperación, impulsando la demanda agregada y, al mismo tiempo, propiciando las condiciones para que haya una mayor capacidad productiva en la economía y que esto sea un multiplicador de la economía en el sector privado. Esto es algo que siempre en una situación de recesión debe ocurrir, el Estado tiene un rol en la recuperación, más generalmente en adoptar políticas en pos de más dinamismo productivo”.

“Al mismo tiempo hay que ir poniendo las cuentas fiscales en orden. Esto significa que la economía o el sendero fiscal tiene que transitar por un corredor relativamente estrecho en el cual se impulsa a la demanda agregada mientras el aumento de la recaudación, que ocurre cuando la actividad económica aumenta, resulta en una reducción del déficit”, agregó.

En otro orden, repitió la necesidad de ir “reduciendo gradualmente” las necesidades de financiamiento por parte del Banco Central al Tesoro. “Todo el programa de estabilización macroeconómica plurianual va a tener esos principios en cuenta”.

Con respecto a la política cambiaria, dijo que el Gobierno definió un objetivo. “El peso se va a ir depreciando contra el dólar a una tasa consistente, que nos permita ir acumulando reservas. El tipo de cambio real tiene que ser más o menos el mismo en diciembre 2021 que a fines de 2020″, aseguró.

Por último, afirmó que una economía tranquila es una economía que definió su tendencia, asociado esto a la generación de empleo y estabilidad de ingreso, lo que requiere bajo niveles de inflación.

“Un tema central de la política económica es la reducción de la inflación. En 2019 fue arriba del 53%, bajó casi 18 puntos porcentuales en 2020, y cerró en 36,1%. En los últimos meses del año se experimentó una dinámica que no debe tomarse como significativa sobre qué esperar en 2021″, planteó.

Y agregó: “La inflación la atacamos de forma integral, se usan múltiples instrumentos de política económica. Buscamos que se vaya reduciendo 5 puntos porcentuales año a año”.

El jefe del Palacio de Hacienda mantendrá un encuentro con directivos de cámaras empresariales chaqueñas y ofrecerá una charla en la Universidad Nacional del Nordeste (UNNE). Guzmán también visitará una planta textil en Resistencia, junto con el mandatario provincial y el flamante ministro de Economía chaqueño.

La visita a Chaco forma parte del nuevo rol que tomó el ministro tras el acuerdo con los acreedores privados y que se profundizará durante este año: salir al territorio.

Días atrás Guzmán estuvo en Entre Ríos en donde habló de las deudas provinciales y después hizo lo propio en Mar del Plata, provincia de Buenos Aires.

Sin embargo, también está realizando visitas menos promocionadas en pymes productivas de la zona del Conurbano y hasta jugó al fútbol con trabajadores de una textil. Algunos sectores del Gobierno señalan que lo están midiendo como candidato para las elecciones de medio término para que Guzmán siga un camino similar al del ahora gobernador Axel Kicillof, que fue ministro y diputado antes de acceder al cargo de gobernador.