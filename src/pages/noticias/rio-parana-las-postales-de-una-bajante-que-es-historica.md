---
category: El Campo
date: 2021-07-07T08:43:25-03:00
thumbnail: https://assets.3dnoticias.com.ar/01_Sutubal_Baja_Prensa.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ELONCE / VIDEO LA POSTA DEL VOLADOR
resumen: 'Río Paraná: las postales de una bajante que es histórica'
title: 'Río Paraná: las postales de una bajante que es histórica'
entradilla: En diferentes puntos del país se puede ver un un paisaje inusual por la
  histórica bajante.

---
“En años y años no se ha visto una bajante tan profunda y tan acentuada como esta. La playa ha mostrado toda su expansión y todo el potencial que podría brindar para los turistas y amantes del río”, dijo Manuel Tennen, intendente de Villa Urquiza y agregó que el sector del camping “se encuentra en una situación anómala y no se puede disfrutar”.

Consultado sobre si la bajante está afectando la provisión de agua potable, Tennen explicó que, en la actualidad, “el agua es potabilizada y producida de los pozos perforantes que están desde años y la toma de agua, que está a la vera del río, no está en funcionamiento porque la bomba que chupa el agua está anclada en la arena debido a la bajante”; además dio cuenta que cuando la Provincia hizo el traspaso de la obra a la comuna “le faltaban un par de etapas” para su puesta en marcha.

Por otra parte, el Intendente de Villa Urquiza habló sobre las expectativas que tienen ante el inicio del receso invernal y señaló que se plegarán a las medidas que establezca el gobierno provincial y nacional. “El Gobernador anunció que habrá una apertura turística. Si se plasma en un decreto y nos dan las certezas que no habrá inconvenientes, no tenemos problemas en adherir a lo que saque la Provincia porque somos una localidad turística por excelencia, una de las más visitadas y estaremos gustosos de recibir al turismo”. En este sentido, hizo especial hincapié en el comportamiento de la gente y en la importancia de seguir manteniendo los protocoles en relación a la pandemia. “La responsabilidad empieza por cada uno”, dijo finalmente Tennen.