---
layout: Noticia con imagen
author: "Fuente: Diario UNO"
resumen: Vacunación Covid 19
category: Agenda Ciudadana
title: Ginés confirmó que la vacunación comenzará en diciembre
entradilla: De esta forma, Ginés anunció que a fines de diciembre comenzará la
  vacunación contra el coronavirus en el país. Personal de salud y mayores de 64
  años con enfermedades serán los primeros en ser vacunados.
date: 2020-11-19T12:35:26.583Z
thumbnail: https://assets.3dnoticias.com.ar/gines.jpg
---
El ministro de Salud de la Nación, Ginés González García, adelantó que la vacunación contra el coronavirus "comenzaría fines de diciembre o principios de enero para llegar a una escala muy grande en marzo y será la campaña de vacunación más grande de la historia argentina".

En palabras de Ginés "será la campaña de vacunación más grande que tuvo la historia de la Argentina ya que movilizará a todos los ministerios, provincias y muchas instituciones y jurisdicciones con retribución equitativa y para todos", analizó en diálogo con Canal 9 de Mendoza desde Casa de Gobierno.

Según cálculo el ministro, "**se comenzará a vacunar fin de diciembre o principios de enero y ya en marzo a una escala muy grande para vacunar a mucha gente**".

Primero, dijo que "serán vacunados los más susceptibles, de más de 64 años con otras enfermedades, y los trabajadores de la salud, tan en riesgo y necesarios para todos".

Finalmente, el funcionario nacional dijo este mediodía que **habrá "vacunas de cinco proveedores y la idea es vacunar a muchos con la temporalidad más rápida posible en cuanto sean aprobadas y el compromiso de pruebas cumplidas y los límites de seguridad y efectividad cumplidas**".