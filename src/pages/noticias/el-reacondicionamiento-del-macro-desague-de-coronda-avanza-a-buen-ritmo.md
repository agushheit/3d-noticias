---
category: Estado Real
date: 2021-01-17T10:20:47Z
thumbnail: https://assets.3dnoticias.com.ar/desague-coronda.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: El reacondicionamiento del Macro Desagüe de Coronda avanza a buen ritmo
title: El reacondicionamiento del Macro Desagüe de Coronda avanza a buen ritmo
entradilla: 'Se trata de una obra esperada y requerida durante años por vecinos, concejales
  y autoridades de distintas gestiones municipales. '

---
El secretario de Recursos Hídricos, Roberto Gioria y el subsecretario de Desarrollo Hídrico y Comités de Cuenca, Mariano Diez, recorrieron los trabajos de limpieza y reacondicionamiento del Macro Desagüe en la ciudad de Coronda, junto al intendente Ricardo Ramírez y los concejales, Claudio Minetto y Patricia Nuñez.

En relación a esto, Gioria destacó la inversión provincial y la voluntad política de finalizar la obra: “Hablamos de una obra muy importante, no sólo para disminuir el riesgo hídrico de los vecinos de Coronda sino también por el volumen de los trabajos que requiere. Se trata de una obra que iniciamos hace un mes y demandará unos tres a cuatro meses más, empleando varias máquinas, entre ellas retroexcavadoras de brazo largo, debido a que el canal tiene un ancho de base de unos 20 metros. A su vez, la limpieza y el reacondicionamiento que hay que realizar es importante, tanto por la vegetación que ha crecido como la gran cantidad de sedimento depositado. Asimismo, la erosión de los caminos y de toda la cuenca es un problema de envergadura, por lo que estamos estudiando alternativas para disminuirla”, explicó.

“Tanto esta obra, como la del desagüe Moreno, son prioridades para el gobernador Omar Perotti y la ministra Silvina Frana, que, desde los primeros meses de gestión, nos pidieron trabajar de lleno para concretarlas. Estimamos comenzar con las obras del desagüe Moreno en marzo”, concluyó Gioria.

Por su parte, el intendente Ramírez agradeció a las autoridades del gobierno provincial por ejecutar la obra del Macro Desagüe y valoró el trabajo conjunto: “La limpieza del desagüe es algo que venimos esperando desde hace muchos años. Y la verdad, que contar con este mantenimiento es importantísimo para disminuir el riesgo hídrico de la ciudad. Donde trabajamos en colaboración mutua porque la provincia pone las máquinas, el personal y el combustible. Mientras que el municipio se hace cargo de las horas extras”, finalizó.

**LA OBRA**

El Macro Drenaje se constituye en la defensa oeste de la localidad y actúa como un canal interceptor del agua superficial que se genera en la zona rural, al noroeste de Coronda. Este canal es parte del sistema del arroyo Colastiné que descarga finalmente en el río Coronda.

Tiene una longitud aproximada de 8 kilómetros y es una obra que fue construida en el 2012 a fin de mitigar los problemas de anegamientos de la ciudad, pensado con el objetivo de proteger el casco urbano.

Sin embargo, desde el 2012 no se hicieron las tareas necesarias de mantenimiento y, por ende, la provincia en conjunto con la municipalidad realizaron relevamientos topográficos con el fin de detectar y cuantificar la sedimentación en el canal, que está obstruyendo el normal funcionamiento del canal, y así dimensionar los trabajos a realizar, permitiendo continuar con su limpieza y su reacondicionamiento general.