---
category: El Campo
date: 2021-03-20T06:00:52-03:00
thumbnail: https://assets.3dnoticias.com.ar/granos.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Recibidores de granos exigen que la jornada laboral se reduzca a 6 horas
title: Recibidores de granos exigen que la jornada laboral se reduzca a 6 horas
entradilla: '"No vamos a aceptar salarios empobrecidos y menor cantidad de puestos
  de trabajo, exigimos lo que nos merecemos", sostuvo el secretario General del gremio,
  Pablo Palacio.'

---
La Unión de Recibidores de Granos y Anexos (URGARA) exigió nuevamente la reducción de la jornada laboral a 6 horas durante su 64° Congreso Anual Ordinario llevado a cabo de manera virtual.

El encuentro contó con la participación de los 32 congresales e integrantes de la Comisión Directiva Nacional, directivos de Seccional y Delegados Regionales, junto con los veedores del Ministerio de Trabajo, Empleo y Seguridad Social de la Nación.

"No vamos a aceptar salarios empobrecidos y menor cantidad de puestos de trabajo, exigimos lo que nos merecemos", sostuvo el secretario General del gremio, Pablo Palacio.

En ese sentido, Palacio remarcó: "Es nuestro objetivo buscar la reducción de la jornada de trabajo a 6 horas, tal como lo fue históricamente. Queremos reivindicar ese derecho, lo que permitirá mejorar la calidad de vida de nuestros trabajadores, en aspectos tan importantes como la salud, el descanso, el esparcimiento y generar más puestos de trabajo".

"El sector agroexportador, que ha sido beneficiado por la suba del dólar, la suba del precio de la soja, la baja de las retenciones, con cosechas que cada vez son más importantes en toneladas, debe reconocer el esfuerzo de sus trabajadores", agregó el sindicalista.

Por último, Palacio señaló: "Reafirmamos nuestro compromiso para mejorar el salario de nuestros trabajadores, manteniendo el nivel de discusión en paritarias y tratando de que se reconozca la función e importancia de nuestros trabajadores en la cadena de valor; buscamos que la inflación no dañe nuestros salarios".