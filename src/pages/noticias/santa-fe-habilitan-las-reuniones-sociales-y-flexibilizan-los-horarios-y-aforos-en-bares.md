---
category: La Ciudad
date: 2021-07-24T06:00:56-03:00
thumbnail: https://assets.3dnoticias.com.ar/BARES.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'Santa Fe: habilitan las reuniones sociales y flexibilizan los horarios y
  aforos en bares'
title: 'Santa Fe: habilitan las reuniones sociales y flexibilizan los horarios y aforos
  en bares'
entradilla: 'Los encuentros afectivos sólo podrán realizarse con hasta 10 personas.
  Cada jurisdicción determinará si la ocupación en locales gastronómicos se puede
  ampliar a un 50%. '

---
Mediante el decreto Nº 220 del gobierno provincial, se determinó la nueva modalidad de convivencia en pandemia en todo el territorio provincial, desde la hora cero de este sábado 24 de julio y hasta el 6 de agosto inclusive. La novedad es que ahora se habilitan las reuniones sociales de hasta 10 personas en todo el territorio provincial en domicilios particulares y espacios públicos, dice el acto administrativo que lleva la firma del gobernador Omar Perotti.

Asimismo, los locales gastronómicos -bares, restaurantes, heladerías y otros autorizados a funcionar como tales, con concurrencia de comensales-, podrán trabajar los viernes, sábados y vísperas de feriados, entre las seis horas y las 24 horas. El resto de los días de la semana, entre las seis y las 22 horas. Fuera de estos horarios, “sólo podrán realizar actividad en las modalidades de reparto a domicilio y de retiro, siempre que esta última se realice en locales de cercanía”.

Respecto del aforo permitido, “las autoridades municipales y comunales deberán especificar en cada caso la cantidad de mesas que corresponda, a los fines de cumplir con el coeficiente máximo del 50% de ocupación. Esta determinación deberá informarse al público en lugar visible en el ingreso”. La actividad del comercio mayorista y minorista prácticamente no se modifica.

Otro de los puntos salientes es que seguirá habilitada la actividad de los casinos, “cumplimentando las reglas generales de conducta y de prevención y los protocolos oportunamente aprobados para la actividad; en el horario de diez a 24 horas los viernes, sábados y víspera de feriados y de diez a 22 horas el resto de los días de la semana”.

“Se restringe el uso de la superficie libre de circulación a un máximo del cincuenta 50% de la misma, tomando como referencia complementaria la de una persona cada dos metros cuadrados de espacio circulable; sin realización de eventos de ningún tipo, ni habilitación de las salas de bingos y los juegos de paño de ruleta, carteados y de dados”, completa.

También se autoriza la actividad de las ferias francas de comercialización de productos alimenticios y artesanales, “sujeto a la autorización y determinaciones que dispongan las autoridades locales de manera complementaria a las reglas generales de conducta y de prevención sanitaria y los protocolos específicos oportunamente aprobados”.

**Deportes**

El decreto autoriza la práctica recreativa de deportes grupales en modo entrenamiento en espacios privados, por turnos y en grupos, y los gimnasios, natatorios y establecimientos afines, también por turnos y en grupos. Todo esto entre la hora 7 y las 22, con hasta 50% de ocupación de la superficie disponible permitido.

También, la pesca deportiva y recreativa en la modalidad desde costa y embarcados, ocupando en este último caso un máximo del 50% de la capacidad de transporte de personas habilitada para la embarcación por las autoridades competentes, debiendo ser menor en caso que con dicho porcentaje no pueda garantizarse el debido distanciamiento social.

Seguirán autorizadas actividades de guarderías náuticas, a los fines del retiro y depósito de las embarcaciones para el desarrollo de las actividades habilitadas por el presente; la navegación recreativa o deportiva, en cualquier tipo de embarcación: la navegación para el transporte de pasajeros en ningún caso podrá ser con el fin de realizar actividades recreativas, deportivas o reuniones sociales, pudiendo solo efectuarse para la concurrencia de personas a realizar actividades esenciales.

**Salones de eventos**

En el decreto N° 220 aparece otra novedad: la autorización de la actividad de salones de eventos y de fiestas, pero sólo por una semana, del 31 de julio hasta el 6 de agosto. Estos locales quedan así autorizados para la realización de eventos sociales, con un factor de ocupación de la superficie disponible (aforo) para el público de hasta el 50%, “y que asegure el distanciamiento entre personas y un espacio de una persona cada dos metros cuadrados de espacio circulable; “sin actividades que signifiquen baile u otras que impliquen circulación de los asistentes entre las mesas o ubicaciones dispuestas”.

También, sólo por una semana (del 31 de julio y hasta el día 6 de agosto), se habilitan las competencias deportivas provinciales, zonales o locales de carácter profesional o amateur, incluidas las automovilísticas y motociclísticas, que se desarrollen en espacios abiertos al aire libre o cubiertos con suficiente ventilación, sin concurrencia de espectadores, en ningún caso.

**Prórrogas**

Se prorroga hasta el 6 de agosto de 2021 inclusive la realización de actividades religiosas en iglesias, templos y lugares de culto con hasta 50% de aforo, tomando como referencia complementaria la de una persona cada dos metros cuadrados de espacio circulable. También se prorroga la actividad en cines, teatros y salas de espectáculos de centros culturales a los efectos del desarrollo de artes escénicas.

**Restricción a la circulación**

En todo el territorio provincial, continúa restringida la circulación vehicular en la vía pública en el horario de 22 y hasta las seis horas del día siguiente. Los exceptuados de esta prohibición de circular son el personal esencial en la emergencia, comprensivas de las situaciones de fuerza mayor, y los desplazamientos desde y hacia los lugares de trabajo de los que desarrollan actividades habilitadas, incluidos los de los propietarios de los locales establecimientos.

“En los horarios establecidos de restricción de la circulación vehicular en la vía pública, quienes circulen en ocasión de concurrir a realizar o de haber realizado una actividad habilitada, deberán portar la documentación o constancia (reserva, ticket o factura) que acredita esa circunstancia”, dice el decreto.

**Qué se suspende**

Curiosamente, el decreto anterior había habilitado el funcionamiento de locales de juegos infantiles y otros establecimientos afines, en espacios cerrados o al aire libre, comúnmente denominados miniclubs o peloteros: ahora pasan a ser actividades suspendidas. Siguen vedadas discotecas, funcionamiento de bares, restaurantes, patios de comidas y espacios de juegos infantiles ubicados en centros comerciales, paseos comerciales o shoppings con asistencia de clientes a los locales.

También, la realización de eventos culturales y recreativos relacionados con la actividad teatral y música en vivo que impliquen concurrencia de personas en espacios abiertos de acceso libre para el público; la actividad artística en plazas, parques y paseos; asambleas y actos eleccionarios de personas jurídicas públicas y privadas, en forma presencial.