---
category: Deportes
date: 2022-01-04T06:15:23-03:00
thumbnail: https://assets.3dnoticias.com.ar/falcioni.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Julio Falcioni es el nuevo entrenador de Colón
title: Julio Falcioni es el nuevo entrenador de Colón
entradilla: 'En las últimas horas, la dirigencia de Colón llegó a un acuerdo con Julio
  Falcioni para que sea el nuevo entrenador, en reemplazo de Eduardo Domínguez

'

---
El fin de semana explotó el rumor del posible interés de Colón en Julio Falcioni como alternativa a entrenador. La elección del reemplazante de Eduardo Domínguez no era nada fácil. En primera instancia, se quería apuntar a un perfil similar, de alguien joven y con ideas de la nueva escuela. Sin embargo, tras la designación de Mario Sciacqua como director deportivo, se dio un golpe de efecto y se optó mejor por alguien de experiencia para afrontar un calendario cargado, con la Copa Libertadores como gran objetivo.

Fue así como el presidente José Vignatti llamó al Emperador, tras alejarse de Independiente, que decidió no renovarle el contrato. Las charlas preliminares dejaron buenas sensaciones y este lunes, se insistió con la idea para tratar de alcanzar un acuerdo. Sobre todo, porque el miércoles arrancará la pretemporada en el predio Ciudad Fútbol.

Julio Falcioni es el nuevo DT de Colón en lugar de Eduardo Domínguez, junto al director deportivo, Mario Sciacqua.

Había buenas expectativas y fue así como este lunes se terminaron de pulir algunas cuestiones y quedó todo listo para que Julio Falcioni sea el flamante conductor sabalero. Un impulsor de las ideas que mamó justamente Eduardo Domínguez, que dejó una huella imborrable en Santa Fe y elevó la vara de exigencia nunca antes vista.

Por eso, para esta oportunidad se modificó la meta inicial y se fue a la carga por una persona de experiencia. Julio Falcioni, de 65 años, es el mayor recorrido en esta Liga Profesional y tomará la posta para un segundo ciclo, después de lo realizado durante siete meses entre 2006/2007. Cosechó seis victorias, nueve empates y 10 derrotas.

De esta manera, bajo su conducción, Colón cosechó 27 puntos sobre 75 en juego, alcanzando una efectividad de apenas el 36%. En ese lapso de encuentros, el equipo convirtió 30 goles y le marcaron 35, logrando una diferencia de -5. Renunció luego de un empate como local ante Arsenal 1-1.

Ahora con un sinnúmero de batallas en el lomo, se quedará con el puesto en el sonaron un montón de nombres, pero la opción estaba muy cerca. Así se pone en marcha la era Falcioni en el Sabalero.