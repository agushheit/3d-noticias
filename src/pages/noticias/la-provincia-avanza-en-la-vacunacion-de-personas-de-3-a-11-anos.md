---
category: Estado Real
date: 2021-10-27T06:15:32-03:00
thumbnail: https://assets.3dnoticias.com.ar/vacunacion.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La provincia avanza en la vacunación de personas de 3 a 11 años
title: La provincia avanza en la vacunación de personas de 3 a 11 años
entradilla: Desde el Ministerio de Salud confirmaron que se ampliará la capacidad
  de inoculación en los vacunatorios. Brindaron detalles de la tercera dosis de refuerzo
  anunciado a nivel nacional.

---
La provincia de Santa Fe continúa llevando adelante el operativo de vacunación contre el Covid 19 en todo el territorio provincial. Así, se anunció la ampliación de la capacidad de los vacunatorios para inocular a personas de 3 a 11 años. “En Rosario, a partir de mañana en el predio de la ex rural se empezará a inocular a esta población”, confirmó la ministra de Salud, Sonia Martorano.

“Hasta hoy en la ciudad se estaban vacunando 1.000 personas de esta población por día, por lo que a partir de mañana solo aquí -ex rural- se podrán vacunar a 3.000 niños y niñas” detalló la funcionaria, quien agregó: “Hoy tenemos 105 mil niños y niñas de 3 a 11 años ya vacunados, sobre un total de 200 mil inscriptos y un universo de 400 mil”.

En el mismo marco, Martorano resaltó: “Estamos avanzando también en personas de 12 a 17 años. En toda la provincia nos quedan 30 mil turnables que van a terminar de vacunarse la semana próxima en el interior ya que, por la cadena de frío se distribuyeron más tarde; en tanto que en las ciudades más grandes finaliza la vacunación está semana. Por lo que podríamos comenzar con las segundas dosis”.

Por su parte, el secretario de Salud, Jorge Prieto, agregó que se han vacunado a la fecha “156 mil niños y niñas de entre 12 y 17 años. Esto implica un alto porcentaje de lo que hace a los 700 mil niños, niñas y adolescentes que tenemos entre 3 y 17 años”.

En el mismo sentido, el funcionario aseguró: “Invitamos a que se inscriban aquellas personas que aún no lo han hecho, y a los más jóvenes que acudan a su segunda dosis”. Y agregó que actualmente “para ingresar a algún evento, por ejemplo, se requiere una sola dosis, pero ya vamos a comenzar a solicitar las dos dosis. Este es un tema de solidaridad, de completar el esquema por lo que les solicitamos que se presenten a su turno”, anticipó.

**62% DE LA POBLACIÓN VACUNA Y AVANCE DE LA VACUNACIÓN**

  
Más adelante, Martorano destacó que “estamos en casi 2 millones 200 mil santafesinos y santafesinas vacunados; esto es un 62% de la población anotada en el registro voluntario. Es un muy buen número ya que estamos sobre la media nacional”.

“Estamos vacunando fuertemente segundas dosis por lo que queremos llevar tranquilidad a la población que hay buen stock de todas las vacunas en el territorio provincial. Y en cuanto a las primeras dosis tenemos el sistema de vacunación libre por lo que se pueden acercar en toda la provincia a los vacunatorios”, sumó la ministra.

Por último, sobre el anuncio de la Nación sobre la aplicación de una tercera dosis, Martorano afirmó: “Se va a comenzar con esta tercera dosis para personas mayores de 50 años que hayan recibido esquema completo de Sinopharm. Asimismo, hemos definido en el Comité Federal de Salud que a los inmunocomprometidos también se les aplique esta dosis adicional. Estamos esperando los últimos lineamientos, ya que el refuerzo es diferente y comenzaría a partir de diciembre”.

En tanto que Prieto agregó que el llamado “refuerzo” es una tercera dosis destinada a personas que son población priorizadas, personal esencial y de salud, como sucede con la vacuna antigripal.