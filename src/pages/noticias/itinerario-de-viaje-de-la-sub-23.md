---
category: Deportes
date: 2021-06-05T06:00:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/SUB.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: AFA
resumen: Itinerario de viaje de la Sub 23
title: Itinerario de viaje de la Sub 23
entradilla: El equipo conducido por Fernando Batista embarcó este viernes por la mañana
  rumbo a España, donde disputará dos amistosos.

---
El preseleccionado Sub 23 que dirige Fernando Batista afrontará dos compromisos en Marbella. Para ello, concentró durante la noche del jueves y ya se realizó los hisopados pertinentes para poder viajar. En tanto, este viernes la delegación partió desde el Aeropuerto Internacional Ministro Pistarini, de Ezeiza, a las 11.10 con destino Ámsterdam, donde hará escala para luego embarcar en vuelo rumbo a Málaga, arribando a la ciudad española alrededor de las 16 horas del sábado. Luego, un micro trasladará al plantel hacia el hotel H10 Andalucía Plaza, donde se hospedará Argentina para comenzar con sus entrenamientos en tierra marbellí desde el domingo 6 de junio.

Los amistosos se disputarán el martes 8 de junio, ante Dinamarca, y el viernes 11/6, frente a Arabia Saudita. El regreso de la delegación a nuestro país está pautado para el domingo 13 de junio por la mañana.  
**Jugadores convocados para la gira en Marbella**

Jeremías Ledesma (Cádiz - España) - _13/02/1993_

Nehuén Pérez (Granada CF - España) - _05/11/2000_

Claudio Bravo (Portland Timbers - EEUU) - _13/03/1997_

Leonel Mosevich (FC Vizela - Portugal) - _04/02/1997_

Hernán De La Fuente (Vélez - Argentina) - _07/01/1997_

Pedro De La Vega (Lanús - Argentina) - _07/02/2001_

Fernando Valenzuela (Famalicao - Portugal) - _22/04/1997_

Santiago Colombatto (Club León - México) - _17/01/1997_

Adolfo Gaich (Benevento Calcio - Italia) - _26/02/1999_

Esequiel Barco (Atlanta United - EEUU) - _29/03/1999_

Joaquín Blázquez (Talleres - Argentina) - _28/01/2001_

Marcelo Herrera (San Lorenzo - Argentina) - _03/11/1998_

Facundo Medina (Lens - Francia) - _28/05/1999_

Fausto Vera (Argentinos Juniors - Argentina) - _26/03/2000_

Tomás Belmonte (Lanús - Argentina) - _27/05/1998_

Matías Vargas (RCD Espanyol) - _08/05/1997_

Ezequiel Ponce (Spartak Moscow - Rusia) - _29/03/1997_

Agustín Urzi (Banfield - Argentina) - _04/05/2000_

Thiago Almada (Vélez - Argentina) - _26/04/2001_

Martín Payero (Banfield - Argentina) - _11/09/1998_

Francisco Ortega (Vélez - Argentina) - _19/03/1999_

Lautaro Morales (Lanús - Argentina) - _16/12/1999_

**Cuerpo técnico y auxiliares**  
Director Técnico: Fernando Batista  
Ayudante Técnico: Damián Ayude  
Ayudante Técnico: Esteban Solari  
Preparador Físico: Cristian Palandella  
Entrenador de arqueros: Damián Albil  
Videoanalista: Nebio Merola  
Auxiliar utilería: Damián Simonelli  
Auxiliar utilería: Maximiliano Ledesma  
Director Médico: Pablo Flores Cárdenas  
Médico: Fernando Rudi  
Kinesiólogo: Claudio Farina  
Kinesiólogo: Juan Cruz Goycochea  
Auxiliar kinesiología: Gonzalo Guzmán  
Presidente de la delegación: Javier Méndez Cartier  
Empleado administrativo: Nahuel Suárez  
Empleado administrativo: Mariano Cabellón  
Jefe de prensa: Lucas Gaioli  
Representante World Eleven: Rodrigo Bauso