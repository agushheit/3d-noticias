---
category: Agenda Ciudadana
date: 2021-07-04T06:00:22-03:00
thumbnail: https://assets.3dnoticias.com.ar/BRIGADISTAS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Es oficial, la provincia tiene su brigada de reacción inmediata para combatir
  el fuego
title: Es oficial, la provincia tiene su brigada de reacción inmediata para combatir
  el fuego
entradilla: 'El secretario de Protección Civil de la provincia, Roberto Rioja, lo
  confirmó este fin de semana. '

---
inmediata para combatir, fundamentalmente, los incendios en las islas pero que, además, estará capacitada para afrontar eventos de magnitud tales como derrumbes, tormentas o derrame de líquidos peligrosos.

"Antes, un operativo para combatir los incendios nos llevaba entre 48 y 72 horas. Ahora, con esta brigada, estaremos en condiciones de dar respuestas en 4 o 5 horas", explicó el secretario de Protección Civil de la provincia, Roberto Rioja, en diálogo con diario La Capital desde uno de los cursos de capacitación que se están brindado en forma conjunta con los ministerios de Medio Ambiente de la provincia y de Nación.

El funcionario aclaró que la brigada -que consta de 15 integrantes, de los cuales tres son mujeres- "ya está operativa para al ruedo ante una emergencia. La etapa que estamos transitando es la de una profunda e intensa capacitación, como por ejemplo en RCP, manejo de incendios, derrumbes, tormentas. Con este trabajo pretendemos lograr brigadistas completos".

Rioja explicó que la selección de los brigadistas se hizo a lo largo y ancho de toda la provincia, buscando perfiles de actores experimentados -varios de los seleccionados son bomberos voluntarios y al menos dos formaron parte del Ejército Argentino como brigadistas-, cuyas edades oscilan entre los 22 y 24 años de base y 38 y 40. "Apuntamos a que, además de la experiencia, debían tener la vocación porque ser brigadista no es una tarea sencilla ya que debe estar preparado para actuar en situaciones casi siempre de emergencia". explicó Rioja.

La brigada, como ya publicó este diario, tendrá su sede operativa en un a la refaccionada de lo que fue el Liceo Militar General Manuel Belgrano, ubicado en la localidad de Recreo, a escasos 15 kilómetros de la ciudad de Santa Fe. Pero, además, se contará con una guardia permanente en La Ribera, un predio ubicado en la cabecera del puente Rosario-Victoria, que cedió el Ministerio de Medio Ambiente de la provincia.

El secretario de Protección Civil destacó el trabajo conjunto con "los ministerios de Medio Ambiente -encargado de dictar los cursos de capacitación-, Salud y Seguridad y el apoyo de la Guardia Rural Los Pumas, que realizan controles y monitoreos diarios, como también el trabajo cotidiano que realiza la gente de Prefectura Naval".

Acerca del apoyo con que contará la brigada y los medios que empelarán, Rioja indicó que la provincia firmó un convenio con la Federación Argentina de Aerofumigadores para contar con aviones hidrantes en caso de ser necesario. "Por otra parte -acotó-, la provincia cuenta con un helicóptero V2 que en 15 días estará disponible y que sirve para carga, que cuenta con helibalde y que puede transportar una media docena de brigadistas". Por otra parte, aclaró que, de acuerdo a la magnitud del evento producido, se recurrirá a solicitar el apoyo de Nación "o de quien corresponda".

"Este es el objetivo que nos habíamos planteado. Hoy es el puntapié inicial de este gran proyecto que teníamos de tener una respuesta distinta para todos los pobladores de la provincia", redondeó Rioja.