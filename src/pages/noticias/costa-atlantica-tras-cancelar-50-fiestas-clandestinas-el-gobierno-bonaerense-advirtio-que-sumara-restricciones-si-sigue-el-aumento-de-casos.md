---
category: Agenda Ciudadana
date: 2021-01-11T10:19:53Z
thumbnail: https://assets.3dnoticias.com.ar/110121-Axel-Kicillof.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: '3DNoticias - Fuente: Infobae'
resumen: 'Costa Atlántica: el Gobierno bonaerense que sumará restricciones si sigue
  el aumento de casos'
title: 'Costa Atlántica: tras cancelar 50 fiestas clandestinas, el Gobierno bonaerense
  advirtió que sumará restricciones si sigue el aumento de casos'
entradilla: El jefe de Gabinete bonaerense, Carlos Bianco, aseguró que «hay que reforzar
  los cuidados y el cumplimiento de los protocolos» para evitar que la segunda ola
  de contagios los obligue a profundizar las medidas sanitarias.

---
El aumento de casos de coronavirus preocupa cada vez más al gobierno de Axel Kicillof, que este sábado decidió continuar con su esquema de fases vigente para la habilitación de actividades y servicios de acuerdo con su situación epidemiológica y sanitaria. En los municipios que se encuentran en fases 3 y 4 se suspenderá, a partir de este lunes, entre la 1 de la madrugada y las 6 de la mañana, todas actividades comerciales, artísticas, deportivas, culturales, sociales y recreativas.

Al mismo tiempo, el gobierno bonaerense evitó restringir la circulación durante la noche y solo optó por cerrar las actividades en 118 ciudades, que son las que tienen más casos de COVID-19, y la aplicación de multas de hasta tres millones de pesos para los organizadores y asistentes a fiestas clandestinas.

En ese contexto, el jefe de Gabinete bonaerense, Carlos Bianco, advirtió este domingo que los indicadores oficiales muestran que, si no se cumplen las medidas restrictivas, los casos de coronavirus «van a seguir en aumento», por lo que pidió «reforzar los cuidados y el cumplimiento de los protocolos».

Bianco señaló que, si bien la instrucción de Alberto Fernández y de Kicillof es «cuidar la vida sin dejar de lado el trabajo y la producción», la postura es que no se puede «ir de frente hacia un desastre sanitario». El funcionario destacó que es importante que la gente cumpla los protocolos para que el rebrote no aumente su volumen en el corto plazo.

«Siguen aumentando los casos y todos nuestros indicadores predictivos muestran que, si no tomamos medidas, van a seguir aumentando», planteó en declaraciones a radio Rivadavia. Sin embargo, aclaró que **hay una buena noticia, que está vinculada al tiempo que pasa la gente en terapia intensiva respecto de la primera ola, ya que es bastante menor.**

Si bien sostuvo que las autoridades sanitarias y políticas no quieren que el alza de casos se transforme en una segunda ola, el funcionario reiteró que, si persisten los descuidos sociales, va a seguir en aumento la cantidad de contagios. En ese sentido, aclaró: «si siguen los casos en aumento, implicará que tendremos que tomar medidas más restrictivas».

Bianco explicó que se pondrá en marcha un control muy duro con los municipios «para establecer un sistema de multas, principalmente para aquellos que organicen fiestas clandestinas», que son hoy una de las principales causas de contagio. «Si se puede establecer una aplicación de esta medida, podremos provocar una baja en la curva de contagios», sostuvo.

También aclaró que **los números de contagios son muy altos, ya que casi todos los testeos que se realizan a quienes tienen síntomas, dan positivo.** Por último, Bianco adelantó: «vamos a estar arriba de los 4.000 casos promedio durante toda la semana». El funcionario bonaerense además expresó que «es bastante preocupante lo que está sucediendo, ya que estamos en un aumento de más de mil casos diarios respecto de la semana anterior».

**El problema más complejo que hoy afecta el gobierno bonaerense es la desarticulación de fiestas clandestinas.** Las autoridades sanitarias advierten que esos encuentros son los principales focos de contagio. En las ciudades de la costa los intendentes profundizaron los controles nocturnos luego del encuentro con Kicillof la semana pasada, en el que llegaron a la conclusión de que para no aplicar una restricción más estricta, es necesario multiplicar los controles y prevenir los encuentros entre grandes grupos de personas.

**El sábado por la noche, luego de 75 denuncias anónimas,se desarticularon 19 fiestas clandestinas en Mar del Plata**. Lo mismo sucedió en Pinamar donde, según indicó este mediodía el intendente local Martín Yeza, se desarticularon 30 fiestas que se estaban realizando en casas y bares. «Desactivamos una fiesta en la frontera donde se concentraron cerca de 700 personas», explicó en una entrevista con la radio La Once Diez.

Bianco no fue el único funcionario que hizo un pedido público para que la gente cumpla con los protocolos. El ministro de Turismo y Deportes, Matías Lammens, aseguró en declaraciones radiales que «la temporada de verano va a continuar hasta marzo», pero apeló a la responsabilidad individual para que se extremen los cuidados sanitarios y se evite la suspensión del actual periodo estival vacacional. «Lo importante es que cumplamos con las reglas del cuidado sanitario y no nos relajemos», sostuvo el ministro.