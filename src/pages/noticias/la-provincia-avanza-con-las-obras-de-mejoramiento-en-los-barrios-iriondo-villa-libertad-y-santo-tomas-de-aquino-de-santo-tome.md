---
category: Estado Real
date: 2021-05-06T08:20:48-03:00
thumbnail: https://assets.3dnoticias.com.ar/obras.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: La provincia avanza con las obras de mejoramiento en los barrios Iriondo,
  Villa Libertad y Santo Tomás de Aquino de Santo Tomé
title: La provincia avanza con las obras de mejoramiento en los barrios Iriondo, Villa
  Libertad y Santo Tomás de Aquino de Santo Tomé
entradilla: Las obras comprenden la construcción de desagües pluviales y cloacales,
  tendido eléctrico, calles y cordones cunetas y un edificio comunitario. La inversión
  provincial supera los 300 millones de pesos.

---
Los ministros de Infraestructura, Servicios Públicos y Hábitat, Silvina Frana, y de Trabajo, Empleo y Seguridad Social, Juan Manuel Pusineri; y el secretario de Hábitat, Amado Zorzón, recorrieron las obras de mejoramiento e infraestructura en los barrios Iriondo, Villa Libertad y Santo Tomás de Aquino, de la ciudad de Santo Tomé, que comprenden la construcción de desagües pluviales y cloacales, tendido eléctrico, estabilizado de calles, cordones cunetas y un nuevo edificio comunitario, beneficiando a 37 manzanas y directamente a unos 720 lotes.

Durante la recorrida, la ministra Frana manifestó que “para nosotros era una preocupación permanente poder resolver los inconvenientes que tenía la gestión de esta obra y poder iniciar los trabajos de mejoramiento inmediatamente, porque entendíamos la importancia de esta obra para los vecinos de los barrios Santo Tomás de Aquino, Iriondo y Villa Libertad. Esto tiene que ver con un Estado presente, con generar condiciones de vida digna, beneficiando a cientos de familias santotomesinas y mejorando sustancialmente su calidad de vida”, destacó.

“Con hechos concretos y reales damos respuesta a los santafesinos, estas son las políticas que nos insta el gobernador Omar Perotti, tratando de superar este gran obstáculo que es la pandemia, que nos limita y a veces retrasa los tiempos, pero que de ninguna manera impide que lleguemos con soluciones a los vecinos y vecinas de la provincia”, subrayó Frana.

En este sentido, el ministro Pusineri remarcó el impulso de la obra pública como factor determinante en la generación de empleo: “Hacia principios de este año registramos en construcción niveles de actividad superior a la prepandemia, es decir, no solamente se trata de la gente que trabaja directamente en la construcción sino que indirectamente se genera una cadena virtuosa en la generación de empleo. El empleo es la mejor forma de salir de la pobreza y en eso el Estado provincial está presente con un conjunto de acciones y en particular con la obra pública”, destacó el funcionario.

Finalmente, el secretario de Hábitat, Amado Zorzón, detalló que “la obra actualmente está en un 50%, por lo que resta ejecutar la construcción de una bomba de elevación y otra de impulsión para terminar con la red cloacal, y por otra parte, se necesita avanzar con el desagüe pluvial que requiere la ejecución de bocas de tormenta y la canalización correspondiente. El monto de inversión del gobierno provincial en estas obras supera los 300 millones de pesos”, finalizó.