---
category: Agenda Ciudadana
date: 2022-06-22T07:41:37-03:00
thumbnail: https://assets.3dnoticias.com.ar/bacheo8.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: Se puso en marcha el Plan Integral de Bacheo 2022
title: Se puso en marcha el Plan Integral de Bacheo 2022
entradilla: Las primeras intervenciones comenzaron por Candioti Sur y seguirán por
  el micro y macrocentro. En paralelo se trabajará en distintos barrios y en avenidas
  y calles de penetración.

---
Se repararán casi 53 mil m2 con hormigón y asfalto, y se invertirán $524 millones. Emilio Jatón supervisó los arreglos.

En Avellaneda y Gobernador Candioti y con el desembarco de las máquinas, comenzó el Plan de Bacheo 2022 que se desarrollará en los próximos seis meses y por casi $524 millones. Serán unos 52.819 m2, entre trabajos de hormigón y de asfalto, que se van a reparar en distintos puntos de la ciudad. El intendente Emilio Jatón recorrió el inicio de las tareas.

“Estaba esperando este momento, como lo estaban esperando los vecinos, pero ellos por años. Tuvimos que licitar, son varias empresas, distintos frentes de trabajo, abarca el 60% de la ciudad y en un plazo de seis meses. Esperamos que esto rinda los frutos que nosotros anhelamos porque hace mucho tiempo que no se hace un plan de ésta magnitud y va a cambiar la realidad”, dijo el mandatario local.

Siguiendo esta línea, Jatón destacó el trabajo que se llevó a cabo con las instituciones, las vecinales y los vecinos de los distintos barrios. “Trabajamos con ellos por los baches históricos, pero la Municipalidad tiene un relevamiento de bache por bache, barrio por barrio, y una gran parte de ellos se van a ver solucionados en el transcurso de estos seis meses”, detalló.

**Cronograma**  
El Plan de Bacheo 2022 abordará 27.069 m2 de hormigón y 25.750 m2 de asfalto. Con el primer material se intervendrá en sectores de Barranquitas, Gral. Alvear, Los Hornos, Unión y Trabajo, Sargento Cabral, y Jardín Mayoraz. También en algunas calles de los barrios San Roque, Fomento 9 de Julio y Alberdi.

Con asfalto, las tareas comenzaron en barrio Candioti Sur y también va a comprender el micro y macrocentro. “La intención fue comenzar por este barrio y en las vacaciones de invierno y con el receso escolar, desembarcar en el micro y macrocentro donde va a ser más complicado el tránsito”, detalló el director de Gestión Urbana, Matías Pons Estel.

En esta línea contó que equipos municipales están trabajando en Candioti Norte, también en el sur de la ciudad y en el norte, más precisamente en barrio El Vergel. En unas semanas está previsto empezar a intervenir las avenidas troncales como Aristóbulo del Valle, Facundo Zuviría, Gral. Paz, López y Planes. Alte. Brown; y las calles Pedro Vittori, Diagonal Goyena, Vélez Sarsfield, Riobamba. También mejorarán la transitabilidad las calles que están dentro de la jurisdicción de las vecinales Altos del Valle, La Esmeralda y Las Delicias.

Las calles de penetración y gran tansitabilidad que tienen un sentido de circulación de este-oeste también serán reparadas. Además por donde circula el transporte público como Javier de la Rosa, Padre Genesio, Salvador del Carril, Demetrio Gómez, Dr. E. Maradona, Luis J. de Asua, A. Greca, L.F. Leloir, Juan de Garay, G. Estévez Boero, Av. Tte. Loza, Av. 12 de Octubre, Hugo Wats, Caferata, Av. Gorriti, Millán Medina, Estrada, E. Zeballos, Castelli, Mendoza, Lamadrid, R. Sáenz Peña, Estrada, San José, Moreno, Zavalía, Balcarce, Güemes e Ituzaingó.

El director de Gestión Urbana les pidió paciencia a los vecinos por estas intervenciones que “son necesarias”. “Hace un mes estuvimos con todas las instituciones y vecinales de todos los barrios de Santa Fe para organizar los cortes, cómo van a estar trabajando las máquinas y los sectores a intervenir. En el micro y macro centro y Candioti va a ser más rápido porque es asfalto, pero les pedimos que eviten la zona porque los cortes serán por tramos”, agregó Pons Estel.