---
category: Agenda Ciudadana
date: 2021-04-02T08:07:31-03:00
thumbnail: https://assets.3dnoticias.com.ar/billetera.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de LT10
resumen: 'Billetera Santa Fe: el programa sigue e incorpora a los usuarios de Iphone'
title: 'Billetera Santa Fe: el programa sigue e incorpora a los usuarios de Iphone'
entradilla: Entre los nuevos anuncios se destacan la llegada del programa a los teléfonos
  Iphone y la extensión de los descuentos en gastronomía y turismo a todos los días
  de la semana.

---
A partir de este 1º de abril comienza a regir la segunda etapa del programa Billetera Santa Fe, una herramienta del Gobierno provincial que busca activar el consumo e incrementar las ventas en comercios locales a partir de beneficios y reintegros exclusivos del 20% en algunos rubros y del 30% en otros.

“El programa vino para quedarse y va a ir ampliando rubros y beneficios y utilidades para los santafesinos” afirmó Juan Marco Aviano, secretario de Comercio Interior y Servicios por LT10.

Sobre las distintas etapas, el funcionario explicó que cada tres meses se va a trabajar en la renovación de rubros, condiciones, productos que se puedan incorporar o sacar si no funcionan, ajustes, y completar con bienes y productos para en un futuro incorporar rubros de servicios.

“A partir de hoy,  comienza un trimestre donde tenemos alimentos y medicamentos con un reintegro del 30% todos los días” a los que se le suma gastronomía y turismo, que en la primera etapa esta solo los lunes, martes y miércoles.

Por otro lado, afirman que evaluarán la posibilidad de incorporar combustibles, aunque no en esta próxima etapa.

Entre otro de los anuncios, la semana que viene ya estará disponible la aplicación para los usuarios de Iphone, y “será posible descargar en tecnología iOS desde App Store”.

Los nuevos rubros que se incorporan a partir de hoy son colchonerías, marroquinerías, bazares, heladerías, regalerías y locales de artículos de limpieza. Estos se suman a supermercados, farmacias, bares y restoranes.

 

**Los detalles de la Billetera Santa Fe** 

Billetera Santa Fe otorga un 30 % de descuento en alimentos, indumentaria, calzados, juguetería, librería, bares, restaurantes, farmacias y turismo, y un 20 % en electrodomésticos. El reintegro es solventado por el gobierno de Santa Fe en un 85 por ciento, y el 15 por ciento restante está a cargo del comercio. El dinero se acredita a las 24 horas hábiles de realizada la compra en el "Saldo Virtual de la Billetera", con un tope de reintegro 5.000 pesos por mes.

El consumidor que deberá acceder deberá tener domicilio en la provincia de Santa Fe y ser mayor de 18 años. Además debe tener un dispositivo móvil, descargarse la aplicación billetera Plus Pagos, y abonar los bienes y servicios con saldo virtual de la billetera o con cualquier tarjeta de débito que esté adherida en la billetera Plus Pagos.

En tanto, los comercios deben tributar en la provincia de Santa Fe, tienen que estar inscriptos en AFIP y API, y tener una cuenta o realizar la apertura de una cuenta en el Banco Santa Fe que los habilitará para operar con Plus Cobros QR. El comercio deberá registrarse en la web del Banco Santa Fe, obteniendo una cuenta o vinculando la existente en simples pasos.