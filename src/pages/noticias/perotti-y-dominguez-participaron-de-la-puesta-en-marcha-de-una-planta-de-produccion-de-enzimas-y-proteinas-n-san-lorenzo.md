---
category: Estado Real
date: 2021-11-05T06:00:40-03:00
thumbnail: https://assets.3dnoticias.com.ar/sanlorenzo.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Perotti y Dominguez participaron de la puesta en marcha de una planta de
  produccion de enzimas y proteínas n San Lorenzo
title: Perotti y Dominguez participaron de la puesta en marcha de una planta de produccion
  de enzimas y proteínas n San Lorenzo
entradilla: "“Santa Fe es el corazón productivo y de la innovación de la Argentina”,
  aseguró el gobernador. Junto al ministro de Agricultura, Ganadería y Pesca de la
  Nación, visitaron las nuevas instalaciones de la empresa Keclon."

---
El gobernador Omar Perotti, y el ministro de Agricultura, Ganadería y Pesca de la Nación, Julián Domínguez, participaron este jueves, en la ciudad de San Lorenzo, de la inauguración de la planta de la empresa biotecnológica Keclon, fundada por investigadores del Conicet y la Universidad Nacional de Rosario (UNR), y se dedica a la producción de enzimas que se utilizan para reducir desechos e incrementar la eficiencia en la industria aceitera y de la alimentación.

Para la construcción de la planta, desarrollada bajo los más altos estándares tecnológicos internacionales, y de un laboratorio modelo de investigación y desarrollo en Rosario, la empresa completó más de 14 millones de dólares de inversión privada, sumado al acompañamiento del Conicet, la UNR, la Agencia Nacional de Promoción Científica y Tecnológica, y la Agencia Santafesina de Ciencia, Tecnología e Innovación.

En total se generarán más de 80 nuevos puestos de trabajo, creando empleo de calidad para numerosos científicos argentinos. La planta, que cuenta con una capacidad de fermentación de 65.000 litros (ampliable a 200.000 litros), permitirá potenciar la producción y comercialización de productos de excelencia a nivel global, posicionando a la Argentina como referente en soluciones biotecnológicas.

“Es una hermosa oportunidad de poder vivenciar y trasladar a toda la comunidad de qué se tratan las empresas de base tecnológica, la biotecnología, y la economía del conocimiento”, resumió el gobernador Omar Perotti, quien agregó que “el conocimiento brinda hoy la enorme posibilidad de que nuestros sectores tradicionales puedan tener un salto de competitividad y valor agregado”.

El mandatario provincial agregó que “Argentina, con este conocimiento, es un jugador fuerte de la economía, que puede combinar el cuidado del ambiente con la innovación. Se trata de un proyecto de alto impacto en lo que hace a la generación de expectativas positivas del país”, sostuvo.

“Esto es un reconocimiento a nuestros talentos –prosiguió Perotti-, a la capacidad organizativa que nos da la alternativa de que científicos nuestros puedan tener aquí la posibilidad de desarrollar y de crecer, y de motivar y entusiasmar a otros”.

**PRODUCCIÓN E INNOVACIÓN**

El gobernador indicó que “la pandemia le dio al sector científico una oportunidad de colocarse en el centro, y el sector la está tomando claramente, y nosotros queremos estar a la par, acompañando a que se afirme esa señal. Queremos estar a la par de una Argentina que consolide su sector científico-tecnológico, de una provincia que tiene todos los elementos para ser uno de los centros de la economía del conocimiento”, expresó.

“El desafío de Santa Fe es no perder un solo minuto en articular, sumar, y estar a la par de cada uno de nuestros científicos, universidades, y centros tecnológicos, triplicando los presupuestos”, aseguró Perotti, para luego recordar la puesta en marcha del programa SF500.

Finalmente, el gobernador aseguró que “es un orgullo estar aquí, ser gobernador de esta provincia donde se está gestando esta acción coordinada, que sin dudas le va a dar a toda nuestra provincia, y a la Argentina una posibilidad de inversión internacional, de una nueva matriz de desarrollo y crecimiento, y de volver a generar riqueza en la Argentina”, dijo, y cerró: “Santa Fe es el corazón productivo y de la innovación de la Argentina”.

**PRESENTE Y FUTURO**

Por su parte, el ministro de Agricultura, Ganadería y Pesca de la Nación, Julián Domínguez, destacó la importancia de la inversión y describió al Conicet como “un faro, que anticipa las señales del futuro; ustedes pusieron en marcha el futuro, y lo llenan de presente”.

“Argentina puede liderar esta rápida adaptación a la demanda del cambio climático y convertirla en una oportunidad”, pronosticó el funcionario nacional, quien añadió: “Al futuro no le tenemos que tener miedo, tenemos muchas posibilidades de acertar, crecer y generar riquezas con un altísimo valor para nuestro país”.

“Esta experiencia de hoy es una señal del futuro; hoy me dieron vestigios de las políticas que queremos diseñar, Santa Fe está liderando en materia de desarrollo en la industria del conocimiento”, cerró Domínguez.

En tanto, Agustín Otero Monsegur, presidente de la empresa, agradeció la visita de las autoridades y afirmó: “Para cualquier persona del mundo de los negocios, no hay mayor satisfacción que completar un proyecto innovador, desafiante y complejo como el de la planta que estamos inaugurando, por el impacto positivo que genera en nuestra comunidad, en la propia empresa y, a nivel agregado, en nuestro país”.

De la inauguración también participaron, los ministros, de Producción, Ciencia y Tecnología, Daniel Costamagna; de Ambiente y Cambio Climático, Erika Gonnet; la subsecretaria de Economía del Conocimiento, María Apólito; el vicepresidente de Asuntos Tecnológicos del Conicet, Roberto Daniel Rivarola; el rector de la Universidad Nacional de Rosario, Franco Bartolacci, y el intendente de San Lorenzo, Leonardo Raimundo.

**LA FIRMA**

Keclon es una empresa dedicada al desarrollo de soluciones biotecnológicas innovadoras basadas en ingeniería de proteínas y microorganismos y en procesos de fermentación de alto rendimiento que mejoran la eficiencia productiva y la sustentabilidad ambiental.

La compañía desarrolló una plataforma tecnológica propietaria utilizando herramientas de ingeniería genética, biología sintética y técnicas de evolución dirigida. Keclon brinda soluciones para las industrias de la alimentación, oleoquímica y farmacéutica.