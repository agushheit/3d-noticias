---
category: Agenda Ciudadana
date: 2021-03-29T06:00:10-03:00
thumbnail: https://assets.3dnoticias.com.ar/billetera.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Billetera Santa Fe se amplía a toda la semana, para gastronomía y turismo
title: Billetera Santa Fe se amplía a toda la semana, para gastronomía y turismo
entradilla: Así lo informaron los secretarios Juan Marcos Aviano y Alejandro Grandinetti.
  La extensión comenzará a regir el 1° de abril.

---
El programa Billetera Santa Fe se extiende a toda la semana en los rubros de turismo y gastronomía a partir del 1° de Abril.

Así lo informaron los secretarios de Comercio Interior y Servicios, Juan Marcos Aviano, y de Turismo, Alejandro Grandinetti, quienes brindaron precisiones sobre la ampliación durante un desayuno de trabajo que tuvo lugar en el bar El Cairo de Rosario este viernes.

Durante el encuentro, Aviano indicó que “en el sector gastronómico y de turismo, vamos a extender el beneficio del 30% de los lunes, martes y miércoles a todos los días, a partir del primero de abril. Con esta acción buscamos que se sumen más comercios del rubro gastronómico, así como también hoteles, cabañas, y todo el complejo de alojamiento y servicios turísticos”.

Sobre el programa, que cuenta con más de 170 mil usuarios registrados en toda la provincia, y 4.200 firmas comerciales adheridas, detalló que “está funcionando muy bien, y aunque aún tenemos desafíos, estamos abarcando todos los rubros. En abril se sumarán colchonerías, marroquinerías y bazares, entre otros”.

Finalmente, el funcionario aclaró que “cualquier comercio puede acceder al beneficio, realizando el trámite en la página web de la provincia, y allí acordando las condiciones para poder funcionar con el treinta por ciento de reintegro”.

Por su parte, Grandinetti destacó: “Del 30% de descuento, el 85% lo cubre la provincia de Santa Fe, es decir, estamos inyectando el producto del esfuerzo fiscal provincial a un incentivo del mercado interno”.

Refiriéndose a la actividad turística, el funcionario expresó que “es generadora de actividad económica”, y señaló que “la provincia ha sido ratificada entre los diez destinos más elegidos de la República Argentina, con un nivel de reservas del 80 % para el fin de semana largo de Semana Santa”.

Billetera Santa Fe otorga un 30 por ciento de descuento en alimentos, indumentaria, calzados, juguetería, librería, bares, restaurantes, farmacias y turismo, y un 20 por ciento en electrodomésticos.

El reintegro es solventado por el gobierno de Santa Fe en un 85 por ciento, y el 15 por ciento restante está a cargo del comercio. El dinero se acredita a las 24 horas hábiles de realizada la compra en el "Saldo Virtual de la Billetera", con un tope de reintegro 5.000 pesos por mes.

El consumidor que deberá acceder deberá tener domicilio en la provincia de Santa Fe y ser mayor de 18 años. Además, debe tener un dispositivo móvil, descargarse la aplicación billetera Plus Pagos, y abonar los bienes y servicios con saldo virtual de la billetera o con cualquier tarjeta de débito que esté adherida en la billetera Plus Pagos.

En tanto, los comercios deben tributar en la provincia de Santa Fe, tienen que estar inscriptos en AFIP y API, y tener una cuenta o realizar la apertura de una cuenta en el Banco Santa Fe que los habilitará para operar con Plus Cobros QR. El comercio deberá registrarse en la web del Banco Santa Fe, obteniendo una cuenta o vinculando la existente en simples pasos.