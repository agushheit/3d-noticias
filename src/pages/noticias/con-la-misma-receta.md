---
category: Deportes
date: 2021-03-10T06:28:37-03:00
thumbnail: https://assets.3dnoticias.com.ar/colon-buscara-obtener-su-cuarta-victoria-consecutiva-aldosivi-el-cementerio-los-elefantes.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Marisa Lemos
resumen: Con la misma receta
title: Con la misma receta
entradilla: Colon de Santa fe venció a Aldosivi por 2 a 1 en un agónico pero histórico
  partido y lidera el Grupo A

---
El Sabalero avanza a pasos agigantados. Sin tropiezos. Sin vacilaciones, ganó los cuatro partidos que disputó. Y en esos encuentros apenas recibió un gol. Puede que se trate de un equipo que no brilla, pero tiene el mérito y eso es  incuestionable. 

En los minutos finales, Colón iba por el triunfo, pero también sufría ante cada avance de un Aldosivi que juega mucho mejor con la pelota que sin ella. Fue en ese momento cuando el delantero de 36 años comenzó a guiar los ataques del Sabalero. 

Rodríguez pidió la pelota y se mostró como socio permanente de Alexis Castro, otra figura del encuentro que genero peligro durante el encuentro, aguantando la pelota hasta más no poder, el Pulga, descargaba  o encaraba cada vez que se le presentaba la posibilidad. Así generó el penal que le cometió Iritier. Y luego se encargó de convertirlo.

Colón es un equipo pragmático y la  fórmula viene funcionando para el conjunto que suele defender con línea de tres y que en general muestra pocas fisuras ya que los carrileros, Meza y Escobar, tienen mucho compromiso para ubicarse por detrás de la línea de la pelota ante la pérdida. El Sabalero exhibe mucho compromiso, con jugadores versátiles que cumplen la función de marcar y soltarse para jugar.

**El próximo encuentro será ante Estudiantes de La Plata el día sábado 13 a las 21:30hs.**