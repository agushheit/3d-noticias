---
category: La Ciudad
date: 2021-11-02T06:00:57-03:00
thumbnail: https://assets.3dnoticias.com.ar/YAPEYU.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Yapeyú disfrutó de otro Barrio Abierto y del próximo inicio de importantes
  obras
title: Yapeyú disfrutó de otro Barrio Abierto y del próximo inicio de importantes
  obras
entradilla: 'Ya está el obrador instalado para dar inicio a los trabajos del Integrar
  Yapeyú, que consisten en la provisión de agua, desagües, asfalto, cordón cuneta,
  iluminación y otras obras que beneficiarán a cuatro barrios. '

---
Los vecinos y vecinas de Yapeyú disfrutaron el sábado pasado de otro Barrio Abierto, con una feria de emprendedores de la economía social y popular y espectáculos musicales. También fue la oportunidad de anunciar que ya se instaló el obrador para pronto dar inicio a las obras del Integrar Yapeyú, que recuperarán el bulevar 12 de Octubre y llevarán agua, desagües, asfalto, cordón cuneta, iluminación y otras mejoras a cuatro barrios.

Así lo anunció la titular de la Agencia Santa Fe Hábitat de la Municipalidad, Paola Pallero, ante el numeroso público que disfrutó de Barrio Abierto en Avenida 12 de octubre, entre Ex combatientes de Malvinas y Neuquén. “Finalmente está instalado el obrador y en pocos días más comenzarán los trabajos de estas obras por 350 millones de pesos, que beneficiarán a cuatro barrios: Yapeyú, San Agustín, La Ranita y Loyola Norte”, dijo la funcionaria.

Pallero agregó que justamente el bulevar 12 de Octubre donde se realizó el evento, será renovado de forma integral con la colocación de nuevo mobiliario y de luces led bajas, la reconstrucción de las veredas y otros trabajos, que le darán relevancia al bulevar y al comercio instalado sobre el mismo.

Además, resaltó la ejecución del Acueducto Santa Marta, proyectado para proveer a los barrios ubicados en el extremo noroeste de la ciudad de Santa Fe, junto con la realización de 750 conexiones domiciliarias que dotarán de agua potable a familias de La Ranita y Loyola Norte. “También tenemos una obra muy importante de desagües pluviales que hará que los excedentes hídricos escurran hacia el reservorio Nº 6, minimizando el anegamiento de algunos barrios en días de lluvias intensas, como por ejemplo, La Ranita”, explicó Pallero.

Cabe destacar que el conjunto de obras del Integrar Yapeyú es financiado por el Banco Internacional de Reconstrucción y Fomento (BIRF), a través del Ministerio de Desarrollo Territorial y Hábitat de la Nación, y cuenta con el aval del Gobierno provincial. En tanto, el proyecto ejecutivo, la búsqueda del financiamiento y la licitación fueron realizados por la Municipalidad de Santa Fe, a través de la Agencia Santa Fe Hábitat.

**Un día de propuestas**

La segunda edición de Barrio Abierto Yapeyú contó con 35 stands con las producciones de los artesanos y emprendedores del barrio: postres, sahumerios, herrería, crochet, manualidades de distinto tipo. A esto se sumaron comidas y bebidas para asegurar una tarde-noche a toda celebración. En lo que respecta a la música, participaron distintos artistas del barrio, convocados por la Red de Instituciones.

La concejala Laura Mondino destacó el evento que lleva adelante el municipio y los vecinos y vecinas, a través de la Red de Instituciones que nuclea a más de 30 organizaciones del barrio: “Estamos muy contentos con esta actividad porque es el fruto del trabajo con la Red de Instituciones de Yapeyú, que tiene una gran vida en materia de organizaciones. Eso hace que el barrio planifique y arme propuestas para sus habitantes”.

La concejala señaló que “queremos que los vecinos del barrio ocupen la calle, como hoy sucede con este bulevar 12 de Octubre, que será renovado, y puedan disfrutar de espacios públicos de calidad”.