---
category: La Ciudad
date: 2021-08-28T06:15:08-03:00
thumbnail: https://assets.3dnoticias.com.ar/BARBIJOFREE.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Por ahora, "nada de barbijo free" en las calles de Santa Fe
title: Por ahora, "nada de barbijo free" en las calles de Santa Fe
entradilla: 'La posibilidad de dejar de utilizar el barbijo Santa Fe fue descartada
  de plano por la Ministra de Salud, Sonia Martorano. '

---
De la mano de un buen panorama que se presenta en la provincia de Santa Fe en materia epidemiológica, y teniendo en cuenta el avance del plan de vacunación, la ministra de Salud, Sonia Martorano, fue consultada sobre la posibilidad de dejar de usar el barbijo en los meses que se aproximan. Su respuesta fue contundente.

"De ninguna manera; de ninguna manera se analiza la posibilidad por ahora de la no utilización de barbijo", respondió Martorano ante la consulta. "Además, es una situación que aún vemos en todo el mundo, en lugares que nos llevan varios meses de experiencia, no solo en el tema vacunación, sino también en cuanto a la evolución de la pandemia de coronavirus", continuó agregando la titular de la cartera sanitaria.

A muchos especialistas epidemiólogos, como también a la referente de Salud de la provincia de Santa Fe, le sorprende el bajo acatamiento en la utilización que tiene el santafesino en comparación con otros ciudadanos del territorio provincial. "En la ciudad de Rosario, la concientización sobre el uso del barbijo caló más profundamente que en la ciudad de Santa Fe", manifestó un especialista.

"Hoy en día, el plan de acción tiene que ver con vacunar y con la continuidad de cuidados por parte del ciudadano. La vacunación sola no sirve si no se mantienen las medidas de cuidado que siempre recalcamos. El objetivo es vacunar con dos dosis al mayor porcentaje de la población posible y luego, analizando el mapa epidemiológico, estudiaremos como sigue la situación; pero hoy el barbijo continúa como prioritario", sentenció Sonia Martorano.

En relación al ritmo de vacunación que se viene imprimiendo en la provincia de Santa Fe, la ministra de Salud provincial indicó que "la regularidad es altísima". "Estamos vacunando por día a más de 50 mil personas en todo el territorio provincial. Todo es posible gracias a las personas que participan de los operativos de vacunación, los que hacen posible que en Santa Fe se vacune de lunes a lunes", subrayó.

"El único límite que tenemos en este momento es la llegada de vacunas. Hay días que inoculamos menos personas, pero es por la cantidad de vacunas que tenemos disponibles para esa jornada. Acá no tenemos stock; vacuna que llega a la provincia, vacuna que se coloca en el brazo de un santafesino o santafesina", manifestó Martorano.

Consultada por la posibilidad de una tercera dosis, Martorano dijo que "no se descarta hablar de un refuerzo anual", aunque "en la actualidad lo prioritario es terminar esquemas completos de vacunación en toda la población".