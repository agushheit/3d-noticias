---
category: Agenda Ciudadana
date: 2021-02-16T07:51:30-03:00
thumbnail: https://assets.3dnoticias.com.ar/paso.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Clarín
resumen: Sin consenso aún con la oposición, el Gobierno igual pone en marcha el operativo
  para modificar las PASO
title: Sin consenso aún con la oposición, el Gobierno igual pone en marcha el operativo
  para modificar las PASO
entradilla: 'Siguen vigentes las tres opciones que se planteó el gobierno para modificar
  el calendario electoral: suspender o aplazar las primarias o votarlas con las generales
  en un día.'

---
El Frente de Todos avanza con el operativo de retocar las PASO. Las Primarias Abiertas, Simultáneas y Obligatorias, que están previstas para que el próximo mes de agosto los diferentes partidos definan sus candidatos legislativos en todo el país, están en etapa de revisión con tres escenarios posibles.

Los diferentes actores del Gobierno, como ya contó Clarín, analizan las siguientes posibilidades: una es la de suspender las PASO este año, con motivo de la pandemia y de destinar los casi 20.000 millones de pesos de su costo a vacunas y otros gastos relativos al coronavirus. En este lote, militan activamente los gobernadores peronistas Juan Manzur (Tucumán), Sergio Uñac (San Juan) y Jorge Milton Capitanich (Chaco). A ellos se puede sumar a los radicales Gerardo Morales (Jujuy) y Gustavo Valdés (Corrientes), quienes se oponen también a que en 2021 se disputen internas, pero que no encuentran demasiado eco en Juntos por el Cambio para esta decisión.

La segunda opción es la del corrimiento de fechas: esto es, que las PASO se corran de agosto a la segunda semana de septiembre y que las elecciones generales pasen de octubre a noviembre. Aquí el Gobierno pretende ganar tiempo, con datos de la economía y una pretendida recuperación y también, conseguir que la vacunación haya alcanzado a una enorme mayoría de los argentinos cuando esté entrada la primavera.

En esto, tiene importancia un proyecto presentado por los diputados cercanos a Martín Lousteau (Carla Carrizo y Emiliano Yacobitti) que propugnan la fecha de septiembre para las primarias. De este modo, podrían decir que desde el seno de Juntos por el Cambio se impulsa un cambio de fechas.

Por último, está el "sistema de doble voto", que uniría las PASO y las generales en una misma fecha, en una especie de revival de la ley de lemas. Según voceros del Frente de Todos, esto "ordena la interna y distribuye espacios". Y especulan con que la UCR (partido centenario y con presencia en cada pueblo y ciudad del país) podría suscribir esta idea, pues le resolvería la múltiple presentación de candidatos a lo largo de toda la geografía argentina, y donde podría sacar partido sobre el PRO, que no alcanza a tener inserción en todo el país.

¿Cómo siguen los planes del Gobierno de Alberto Ángel Fernández para el operativo de retoque de las primarias? Sin haber alcanzado aún algún tipo de acuerdo con la oposición, la semana próxima se empezará a debatir el tema en la Cámara de Diputados, que maneja con riguroso celo su titular Sergio Tomás Massa.

Esta discusión comenzará en dos comisiones, la de Asuntos Constitucionales y la de Legislación General: en ambas tiene mayoría el FdT. Fuentes parlamentarias dijeron a Clarín que "intentaremos lograr el mayor nivel de consenso posible". Esto significa que no hay consenso con la mayor fuerza opositora, que es Juntos por el Cambio, donde tampoco hay una posición uniforme sobre el tema.

La línea de conversación allí debería explorarse en el diálogo de Massa con Cristian Ritondo -jefe del bloque PRO- quien mantiene una relación privilegiada con Horacio Rodríguez Larreta y María Eugenia Vidal. Con el radical Mario Raúl Negri -jefe radical parlamentario- hoy no hay diálogo abierto.

La otra definición para avanzar con el tema PASO es el llamado al Consejo Federal de Salud, esto es, la cita de todos los ministros de Salud del país.

Máximo Kirchner defendía esta posición, para emparentar cualquier decisión con "una decisión sanitaria y no política". Como dejó trascender el titular del bloque de diputados nacionales del peronismo, "las autoridades nacionales y los partidos políticos deben establecer un criterio epidemiológico para luego decidir qué medidas adoptar en torno a las elecciones. Cualquier decisión debe estar basada en ese criterio epidemiológico. El COFESA (que reúne a los ministros de salud de todo el país) debería realizar una evaluación según el nivel de contagios y a partir de ese criterio nacional establecer las modificaciones en el calendario si resulta necesario", dijo con intención de que se sepa, el hijo de Cristina Kirchner.

Máximo Kirchner no auspicia la suspensión de las PASO, como sostienen algunos funcionarios nacionales y los gobernadores antes enumerados. El jefe de La Cámpora dice que "el argumento de suspender por el costo de la elección no es válido. La elección de autoridades y la posibilidad de elegir de la gente no tiene precio". en una clara definición de que quiere internas y de que su agrupación está activa para competir en algunas categorías, fundamentalmente en la provincia de Buenos Aires, su lugar de mayor despliegue territorial.

En la cumbre oficialista que se realizó la semana anterior (y que develó Clarín), y de la que participaron Alberto Fernández, Santiago Cafiero, Sergio Massa, Máximo Kirchner y Wado De Pedro, se analizó que "correr la fecha ya es un triunfo del Gobierno". Más allá de no conseguir una fecha única para las dos elecciones, que se logre aplazar la votación a septiembre y noviembre, ya sería un logro oficial.

En estos días, las conversaciones sobre el tema, continuaron entre las cabezas del Frente de Todos. "Arde el chat", lo graficó una de ellas. Y en este fin de semana largo (por los feriados de carnaval), el ministro del Interior, Wado De Pedro, hace una nueva ronda de consultas por todo el país para medir, tanto en dirigentes oficialistas como opositores, cómo está actualmente la temperatura para las tres variables que el Gobierno maneja para las PASO.