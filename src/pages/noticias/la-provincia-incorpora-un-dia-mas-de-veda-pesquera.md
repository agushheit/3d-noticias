---
category: Estado Real
date: 2021-07-07T08:05:27-03:00
thumbnail: https://assets.3dnoticias.com.ar/pesca-deportiva.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: La provincia incorpora un día más de veda pesquera
title: La provincia incorpora un día más de veda pesquera
entradilla: A partir del 6 de julio sólo podrá realizarse la actividad los días martes,
  miércoles y jueves. La medida fue tomada teniendo en cuenta la situación actual
  del río Paraná.

---
El Gobierno de Santa Fe, por medio de la Resolución Nº 152/2021 del Ministerio de Ambiente y Cambio Climático, dispuso la prohibición de la pesca comercial los días viernes. Entre los considerandos de la Resolución que entra en vigencia, se destaca la evaluación de la situación actual del río Paraná en general y de la sometida a la jurisdicción de la Provincia de Santa Fe en particular. 

De esta forma, y por lo mencionado anteriormente, a partir del 6 de julio de 2021, queda prohibida la pesca comercial los días lunes y viernes, sumándose a los días sábados, domingos y feriados de veda establecidos por la Ley Nº 12.703. Asimismo, la pesca deportiva podrá practicarse todos los días exclusivamente con devolución obligatoria puesto que bajo tal modalidad, la afectación de la fauna ictícola es mínima. Esta nueva resolución regirá hasta tanto se dicte una norma que la sustituya. 

Lograr una pesca sostenible en el tiempo requiere tener en cuenta el manejo de los peces, asegurando la protección biológica del recurso, y el componente social y económico. 

Un punto a resaltar es que las medidas establecidas fueron consensuadas con la Dirección de Recursos Naturales de la provincia de Entre Ríos, quienes definieron similares previsiones en lo referente al número de días de veda comercial y restricciones a la pesca deportiva, cumpliendo así con una visión holística del sistema pesquero en la cuenca del río Paraná. Además, posibilitará ejercer fiscalizaciones y controles más eficaces.

Las nuevas disposiciones son resultado de un trabajo interministerial entre los ministerios de Ambiente y Cambio Climático, de Producción, Ciencia y Tecnología; y de Desarrollo Social, junto a la Jefatura de asesores de la provincia. Durante el periodo que duren las nuevas medidas, el Ministerio de Producción, Ciencia y Tecnología, con la colaboración del Ministerio de Ambiente y Cambio Climático, fiscalizarán el estricto cumplimiento con el objetivo de proteger el recurso.

En este marco, y a efectos de mitigar los efectos económicos y sociales que la bajante extraordinaria del río Paraná ocasiona al sector pesquero, la provincia de Santa Fe, a través del Ministerio de Desarrollo Social, brindará contención económica para los pescadores comerciales registrados y para aquellos que utilizan la pesca como medio de subsistencia.

Antecedentes

La decisión de ampliar los días de veda pesquera surge en base a diversos indicadores. Desde el Instituto Nacional del Agua (INA) se advirtió que “los niveles en el río Paraná en territorio argentino, incluyendo al Delta, quedarán oscilando por debajo del límite de aguas bajas (...) por lo que la situación ambiental del Delta sigue siendo desfavorable”. 

Asimismo, el 28 de junio se celebró una reunión con la Comisión de Pesca Continental del Consejo Federal Agropecuario (CPC-CFA) en la que se consideraron las recomendaciones presentadas por el Subcomité Técnico a partir de los resultados de la Campaña N° 53 del “Proyecto de Evaluación Biológica y Pesquera de Especies de Interés Deportivo y Comercial en el Río Paraná” (EBIPES). En dicha campaña se propuso mantener la restricción sobre los cupos de exportación y no innovar sobre las asignaciones de cupos mensuales que se han venido otorgando desde principios del año 2021, ascendiendo hasta el 30 de junio de 2021 a 3.186 (tres mil ciento ochenta y seis) toneladas de sábalo, lo que representa aproximadamente un tercio de lo que se venía asignando en años anteriores.

Esta medida coincide con la postura sustentada por el Gobierno de Santa Fe ante la Comisión de Pesca Continental. 

Durante fines de mayo, además, se había realizado una reunión del Consejo Provincial Pesquero, encabezada por la ministra de Ambiente y Cambio Climático, Erika Gonnet, en donde los presentes, por amplia mayoría, coincidieron en la necesidad de seguir restringiendo la actividad pesquera hasta tanto mejoraran las condiciones hidrológicas.

En la misma, participaron todos los actores vinculados a la temática: representantes de pescadores comerciales o artesanales, de cooperativas de pescadores, de clubes de pescadores deportivos, de frigoríficos y acopiadores de pescado y de organizaciones de la sociedad civil tales como El Paraná No Se Toca y el Taller Ecologista. También hubo representantes del sector turístico, de universidades e institutos técnicos y científicos, de Municipios y Comunas, de la Administración de Parques Nacionales, del Poder Legislativo provincial, y del Ministerio de la Producción, Ciencia y Tecnología de la provincia. En dicha oportunidad también asistieron representantes de la Procuración de la Corte Suprema de Justicia de la provincia y de los amparistas que promovieron la veda que dispuso la Justicia a fines de 2020.

Cabe recordar que a fines de 2020 la Justicia había dispuesto una veda total desde el 29 de diciembre de 2020 hasta el 31 de marzo pasado. En dicho lapso, el gobierno de Santa Fe realizó diversas presentaciones logrando así una veda parcial y el consiguiente proceso de resoluciones que establecía nuevas regulaciones en base a un análisis ambiental, económico y social.