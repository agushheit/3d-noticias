---
category: Agenda Ciudadana
date: 2021-12-28T06:00:35-03:00
thumbnail: https://assets.3dnoticias.com.ar/larreta.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: 'Larreta: "Ninguno de los gobernadores de Juntos por el Cambio vamos a aumentar
  impuestos"'
title: 'Larreta: "Ninguno de los gobernadores de Juntos por el Cambio vamos a aumentar
  impuestos"'
entradilla: 'El mandatario porteño ratificó que la Ciudad no adhiere al Consenso Fiscal
  y aseguró que hay "coordinación" con sus pares de Jujuy, Mendoza y Corrientes que
  sí lo hicieron.

'

---
El jefe de Gobierno porteño, Horacio Rodríguez Larreta, afirmó hoy que hay "coordinación" entre los cuatro gobernadores de Juntos por el Cambio y que "ninguno" aumentará impuestos, al tiempo que ratificó el rechazo de la Ciudad de Buenos Aires al nuevo Consenso Fiscal.

Dado que los gobernadores radicales Gerardo Morales (Jujuy), Rodolfo Suárez (Mendoza) y Gustavo Valdés (Corrientes) firmaron el pacto fiscal que promovió el Gobierno nacional, Rodríguez Larreta explicó la postura de su administración al tiempo que remarcó que todos ellos comparten "la misma convicción".

"Hay coordinación entre los cuatro gobernadores de Juntos por el Cambio", aseguró el mandatario porteño y agregó: "La convicción es la misma: ninguno de los cuatro vamos a aumentar impuestos. En el caso de ellos, por la realidad financiera de las provincias, tienen que firmar el pacto fiscal, pero en nuestro caso tenemos un reclamo hecho ante la Corte".

De esta manera, recordó que firmar el Consenso Fiscal 202 implica la suspensión de los juicios que cada distrito tenga contra el Estado nacional, "lo cual afectaría directamente el reclamo" que tiene en el máximo tribunal contra el Gobierno "por la quita arbitraria e inconsulta de los fondos de coparticipación", según expresó.

"Por eso, tuvimos que tomar medidas de contingencia. Vuelvo a reafirmar mi compromiso: si la Corte Suprema falla a favor de la Ciudad, la devolución de esos fondos va a ser destinada a bajar impuestos", precisó Larreta.

Al cuestionar la posibilidad de crear y aumentar impuestos que el Consenso Fiscal le otorga a los gobernadores, Larreta sostuvo que "este no es el camino", porque "la Argentina necesita impulsar un desarrollo federal" y subrayó que charló al respecto "con los gobernadores radicales".

"Este Consenso que propusieron habilita la creación de nuevos impuestos, como el impuesto a la herencia, y el aumento de otros, como el de las alícuotas máximas de Ingresos Brutos a los sectores productivos", subrayó el jefe de Gobierno de la Ciudad.

Y agregó: "Es grave no solo porque habilita más impuestos sino porque, además, anula el camino de baja de impuestos que veníamos transitando con el Consenso Fiscal de 2017, que creemos que debe retomarse cuando estén dadas las condiciones económicas para hacerlo".

"Desde la Ciudad no vamos a acompañar el Consenso Fiscal impulsado por el Gobierno nacional. Asumimos un compromiso central con los argentinos y lo vamos a cumplir: no apoyar el aumento ni la creación de nuevos impuestos al trabajo y a la producción", afirmó el mandatario.