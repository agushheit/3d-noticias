---
layout: Noticia con imagen
author: Nota de la redacción de 3DNoticias
resumen: Cupo laboral trans
category: Estado Real
title: La Municipalidad suma otras cinco trabajadoras a través del cupo laboral trans
entradilla: El intendente Emilio Jatón encabezó el acto que se realizó este
  viernes. Las flamantes trabajadoras municipales se desempeñarán en distintas
  dependencias.
date: 2020-11-21T12:16:01.330Z
thumbnail: https://assets.3dnoticias.com.ar/jat%C3%B3n-trans.jpg
---
La lucha emprendida hace años comienza a dar frutos. Esta mañana, cinco mujeres trans que trabajan en la Municipalidad les dieron la bienvenida a otras cinco, que se incorporan en el cumplimiento del cupo trans. En ese marco, el intendente Emilio Jatón les dio la bienvenida a “las chicas que se suman al equipo municipal” y destacó que “están acá gracias a una larga lucha colectiva”.

En la Estación Belgrano, el intendente expresó: “Hoy dijeron que esto les puede cambiar la vida, y eso va a depender de ustedes. Si lo toman como un trabajo más, o como el gran desafío de su vida y, me parece que este será uno de los grandes desafíos”. Por otra parte, el mandatario indicó que “ojalá que la Municipalidad sea una vidriera para que gracias al buen trabajo que seguramente ustedes harán, haya empresarios y empresarias que empiecen a abrir sus cabezas y a incorporar personas trans. Esa es la lucha y yo me sumo con ustedes”.

**Las cinco nuevas personas se incorporarán, a partir de diciembre, a las dependencias del 0800, la Dirección de Salud, y a las Estaciones, mientras que las cinco que actualmente se encuentran desempeñando tareas -en Cultura y en Integración- proseguirán sus tareas en el municipio.**

![](https://assets.3dnoticias.com.ar/jaton-trans1.jpg)

## **Contrarrestar desigualdades**

Durante el acto que se realizó en la Estación Belgrano, Soledad Artigas, directora de Mujeres y Disidencias de la Municipalidad, indicó: “Ustedes y todas las compañeras y compañeros que están ahí escuchando, siempre le han puesto el cuerpo a esta revolución que venimos viviendo desde hace algunos años, este cambio de paradigma que ustedes impulsaron. Estas son las verdaderas políticas que defienden la vida de las personas, que garantizan los derechos de todes. Estamos orgullosas de acompañarlas. Esta lucha no solo es de ustedes, es una lucha por las que ya no están y es una lucha para el futuro”.

En tanto, Mariano Granato, secretario de Integración y Economía Social, consignó que esta mañana “estamos siendo testigo de una transformación muy profunda que está viviendo la sociedad y que la gestión del intendente Emilio Jatón acompaña e impulsa”. Y adujo que esto tiene que ver con garantizar derechos, con igualar oportunidades y con cumplir una ordenanza, pero consignó que “queremos ir mucho más allá que ese formalismo y asumir el compromiso político de intervenir ante las desigualdades de la sociedad”.

## **Dignidad laboral**

Mariana Magalí Reynoso (40 años) es una de las ingresantes de este año. Recuerda que cuando la llamaron por teléfono para avisarle “fue una emoción tremenda porque lo esperaba hace mucho. Esto es una lucha de hace muchísimos años que venimos llevando muchas compañeras y ojalá esto se siga replicando en los próximos años”.

Mariana cuenta con una gran experiencia laboral: “Yo hace 10 años que trabajo como asistente de personas con discapacidad, soy prestadora independiente de discapacidad; mi trabajo es acompañarlos, hacer de terapeuta”.

Asimismo, sobre su vida, relata que ejerció la prostitución años atrás. Por tal motivo, destaca la oportunidad de poder modificar su realidad: “Está fantástica esta política de inclusión, esto da pie a que en los próximos años las chicas puedan acceder a un trabajo digno y encontrar su lugar. Porque el trabajo dignifica y el mismo trabajo te da diferentes posibilidades, por ejemplo, poder tener el día de mañana una vivienda, que es a lo que nosotros más aspiramos, a poder tener una mejor calidad de vida. Nuestra expectativa de vida es de 35 a 40, creo que yo ya la superé”.

Junto a Mariana, ingresan también a la Municipalidad este año Renata Suniergrilli, Carla González, Selene Mendoza y Jackeline Quinteros.



![](https://assets.3dnoticias.com.ar/jaton-trans-2.jpg)

## **Un cambio de vida**

Por su parte, Luz Natalia Vega, conoce como pocas lo que significa que el Estado te brinde una oportunidad, ni más, ni menos: “Soy una de las chicas trans que ingresó el año pasado por el cupo laboral, estoy trabajando en Mujeres y Disidencias de la Municipalidad, a cargo de las compañeras”.

“Yo antes de ingresar a la Municipalidad me dedicaba a la prostitución, la verdad que fue un cambio terrible en mi vida porque estoy más tranquila, todo cambió un montón”, cuenta Luz y repasa que ahora “estoy haciendo el secundario y muchas cosas que antes no pude hacer. Yo ya venía haciendo cursos, porque mi idea era empezar a salir de la calle. El año pasado empecé a hacer capacitaciones y eso me llevó al cupo laboral, fui, me anoté y ahí quedé y empecé a trabajar en el 2019”.

Con respecto a cómo modificó su vida tener un empleo estable, Luz repasa: “Tuve muchos cambios desde que arranqué a trabajar, es una gran oportunidad. La verdad que también es un orgullo para mí estar ayudando a otras compañeras donde se les puede brindar una ayuda económica, sanitaria, alimentos”.