---
category: La Ciudad
date: 2021-02-01T07:08:56-03:00
thumbnail: https://assets.3dnoticias.com.ar/ciudad-capital.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad de Santa Fe
resumen: Santa Fe vivirá una semana con actividades para toda la familia
title: Santa Fe vivirá una semana con actividades para toda la familia
entradilla: |2-

  En el marco del ciclo Verano Capital, la Municipalidad ofrece una semana repleta de actividades recreativas, deportivas y culturales para todas las edades y en distintos puntos de la ciudad.

---
Con los protocolos y distanciamiento necesarios, la Municipalidad de Santa Fe propuso una intensa agenda de actividades deportivas y culturales para disfrutar de la temporada estival en los solariums y parques de Santa Fe.

Con acceso libre y gratuito se podrá hacer deportes, recorrer el Jardín Botánico, disfrutar del Cine Itinerante por los Derechos, o realizar los recorridos de Mi Ciudad como Turista acompañados por audioguías disponibles en distintas plataformas virtuales.

**Cine Itinerante por los Derechos**

Este martes 2 de febrero, continúa el ciclo “Cine itinerante por los derechos”. A partir de las 18 horas, con entrada libre y gratuita, en el Espigón II de la Costanera Oeste (Boneo y Av. Alte. Brown), se proyectará “La Familia Belier”.

El ciclo transitará a lo largo del verano por distintos espacios públicos de Santa Fe Capital e invita a disfrutar de variada filmografía, que abordará ejes como discapacidad, infancias, adultos mayores, pueblos originarios, consumidores y usuarios, siempre desde la perspectiva de derechos humanos. La propuesta es coordinada desde la Dirección de Derechos y Vinculación Ciudadana con las secretarías de Educación y Cultura, y Políticas de Cuidado y Acción Social, y el acompañamiento de Cine Club Santa Fe.

Los interesados en asistir a la actividad deberán llevar reposeras, sillón o lona; utilizar barbijo y mantener el distanciamiento social.

**Paseos por el Botánico**

Los santafesinos y visitantes podrán recorrer el Jardín Botánico "Ing. Lorenzo Parodi", ubicado en avenida Gorriti al 3900. Allí, con turnos previos, se pueden hacer dos circuitos educativos.

Uno de ellos es un recorrido para realizar un avistaje de aves, reconocimiento de la flora, registro de especies y ciencia ciudadana. Esta opción se puede hacer los miércoles y viernes, de 8 a 12, y los sábados, de 8 a 12 y de 18 a 20 horas.

La segunda alternativa se denomina "Vivero Abierto" y consiste en conocer el trabajo de los viveros municipales. Los trabajadores explicarán la tarea que desempeñan y cada uno de los procesos de la producción. Como cierre podrán hacer un plantín y se llevarán uno como regalo. Además, si el visitante lo desea, podrá también recorrer el lugar y disfrutar de la flora y la fauna que lo cobija. Esta opción se podrá hacer los miércoles y viernes, de 8 a 11.

Ambas alternativas son con turno previo que se deberá sacar enviando un mail a: jardinbotanicosantafe@gmail.com

 

**Vereda para jugar**

Las intervenciones lúdicas que la Municipalidad realizó en las veredas de la Estación y el Jardín de Loyola Sur, a partir de un proyecto conjunto con la Red de Instituciones del barrio, se activan durante esta temporada para recibir a las familias.

Los jueves y viernes de verano, de 15.30 a 18.30 hay actividades para todas las edades. La esquina de José Pedroni y Furlong se llena de propuestas “para crecer jugando, liberando la imaginación de los pies a la cabeza”, como dice la invitación destinada a todas las niñas y niños del barrio.

La rayuela, un tablero de ajedrez y otro de ta-te-tí, son algunos de los juegos -con sus respectivas piezas artesanales- que fueron diseñados en baldosas de cerámica por la Escuela de Diseño y Artes Visuales del Liceo Municipal. En cada encuentro habrá a disposición otras propuestas lúdicas: títeres para construir y jugar, botánica y arte para experimentar, y arcilla y barro para crear.

La lectura y la escritura también tendrán su espacio en esta propuesta, y se pondrá a disposición de quienes asistan un cuaderno para escribir y trabajar juntos la historia del barrio.

**Centro Experimental del Color**

En el espacio cultural se encuentra en exposición “Kiwi: el barro y las palabras”, que propone actividades lúdicas de escritura y modelado en arcilla para todas las edades. Los encuentros se realizan de jueves a domingo, de 18 a 19 horas. Los cupos son limitados a ocho personas, en función de los protocolos sanitarios que rigen para estas actividades, por lo que se deberá solicitar turno previamente vía WhatsApp al número 342-5118392, de lunes a viernes, de 9 a 15 horas.

**Jazz bajo las estrellas**

El jueves 4 de febrero, desde las 21, continúa la música en vivo en el ciclo Jazz Bajo las Estrellas que lleva adelante La Birra es Bella & Paladar Negro (Balcarce 1681), con apoyo de la Municipalidad. La entrada es libre y gratuita y se deben realizar reservas al 342-4051510.

**Expresiones culturales**

Durante el verano, la Municipalidad brinda actividades vinculadas a la danza, la música, la lectura y oficios en distintos puntos de Santa Fe capital para todas las edades.

Las propuestas se dictan en las Estaciones de La Guardia, Barranquitas, Coronel Dorrego y CIC de Facundo Zuviría, además del Mercado Progreso, la explanada de la Estación Belgrano y Candioti Park. De esta manera, la Municipalidad descentraliza el acceso a actividades recreativas al aire libre con estricto protocolo sanitario, que incluye inscripción previa en cada actividad y el uso de burbujas para garantizar el distanciamiento social. Las inscripciones se realizan en los espacios donde se dictan los talleres.

En el Mercado Progreso (Balcarce 1635) se dictan clases de danza y movimiento para jóvenes y adultos, los martes y viernes, de 9.30 a 11 horas. También, folclore para adultos, los martes y jueves, de 19 a 21.

Por su parte, en la explanada de la Estación Belgrano (Bulevar Gálvez 1150) se dan clases de folclore para adultos al aire libre los domingos, a las 19, y tango para adultos los martes y jueves, de 18 a 19.30.

En Candioti Park (Rosalía de Castro 1882) se dicta un taller de skate destinado a las infancias y juventudes, los martes y jueves, de 17 a 20.

En la Estación La Boca, los martes y jueves se desarrolla la propuesta “Lecturas a la sombra” en el horario de 10 a 12 para las infancias.

Marroquinería en Eco Reciclado se brinda martes y jueves, de 10 a 12, en la Estación Dorrego (Larrea 1735) para jóvenes y adultos.

Además, en la Dirección de Deportes (Almirante Brown 5294) ofrecen tango para adultos al aire libre, los martes y jueves, a las 21.

En la Estación Mediateca (Tucumán y Pasaje Mitre) se realiza un taller integral de Hip Hop para jóvenes, los martes y jueves, a las 15. En el mismo lugar, se invita a participar del taller de producción de objetos decorativos los lunes, miércoles y viernes, de 17.30 a 19, destinado a las infancias y juventudes.

En la Estación Barranquitas (Iturraspe y Estrada) se dicta el taller de Circo Integral los martes y jueves, de 16 a 18, para jóvenes.

En la Estación CIC de Facundo Zuviría (Facundo Zuviría 8002) se ofrece un taller de guitarra y percusión los martes, de 16 a 18, y los miércoles, de 10 a 12, tanto para jóvenes como para adultos.

**Deportes al aire libre**

Durante la temporada de verano el deporte también se vive a pleno en la ciudad. Con acceso libre y gratuito, habrá propuestas de actividades deportivas al aire libre en las sedes y horarios que se detallan a continuación.

En el Espigón II, se podrá disfrutar de beach vóley de lunes a viernes de 19 a 20; beach fútbol los lunes, miércoles y viernes de 17 a 18 y los martes y jueves de 18 a 19 horas; funcional los lunes, miércoles y viernes de 18 a 19; y juegos recreativos de lunes a viernes de 17 a 18 horas.

En el Parque del Sur habrá yoga los martes y jueves de 10 a 11; funcional los martes y jueves de 18 a 19; beach vóley de lunes a viernes de 17 a 20; gimnasia y caminata los lunes, miércoles y jueves de 17 a 20; iniciación deportiva de lunes a viernes de 17 a 20; boxeo recreativo los martes y jueves de 17 a 18; y ritmos los jueves de 18 a 19 horas.

Por su parte, en el Parador Este se realizará beach handball los miércoles y viernes de 17 a 18; beach vóley de lunes a viernes de 17 a 20; beach fútbol de lunes a viernes de 17 a 20; funcional de martes a viernes de 17 a 18; ritmos de lunes a viernes de 18 a 19; y caminatas recreativas de martes a viernes de 18 a 19 horas.

En la sede de Deportes, ubicada en Avenida Almirante Brown 5294, habrá funcional los lunes, miércoles y viernes de 18 a 19; beach handball los lunes y miércoles de 17 a 18.30; beach vóley de lunes a viernes de 17 a 20; beach tenis los martes, jueves y viernes de 17 a 20; beach fútbol de lunes a viernes de 17 a 20; ritmos, martes y jueves de 19 a 20; juegos recreativos, lunes a viernes de 18 a 20; sóftbol, miércoles y viernes de 17 a 20; vóley, lunes a viernes de 18 a 20; handball, lunes de 17 a 18.30; y tenis, martes, jueves y viernes de 17 a 20 horas.

Y, en la costa, en la sede de Colastiné Norte, habrá ritmos y gimnasia para adultos, los martes y jueves de 8 a 11 y de 18 a 19; y en la sede La Guardia los lunes, miércoles y viernes de 9 a 11 horas.

En la sede del Centro Gallego, ubicado en Avenida Galicia 1357, habrá yoga los lunes, miércoles y viernes de 8 a 9 y de 9 a 10; funcional los lunes, miércoles y viernes de 10 a 11 y los martes y jueves de 17 a 18; pilates los martes y jueves de 8 a 9;  GAP los martes y jueves de 9 a 10; ritmos infantil los martes y jueves de 10 a 11. Por la tarde, además de funcional, se podrá realizar newcom los lunes, miércoles y viernes de 19 a 20; minihockey los martes y jueves de 18 a 20; y minivóley los lunes, miércoles y viernes de 17 a 19 horas.

En el Polideportivo La Tablada, se desarrollará la actividad de funcional de lunes a viernes de 8 a 9 y los lunes, miércoles y viernes de 15 a 16; zumba los martes y jueves de 9 a 10 y los lunes, miércoles y viernes de 16 a 17; ritmos los martes y jueves de 15 a 16; patín babys los martes y jueves de 16 a 17; Patín Infantil los martes y jueves de 17 a 18; Patín Juvenil los martes y jueves de 18 a 19; Patín Mayores los martes y jueves de 19 a 20; tae-kwondo los lunes, miércoles y viernes de 17 a 18; Boxeo los lunes, miércoles y viernes de 18 a 20; ritmos los lunes, miércoles y viernes de 20 a 21 horas.

En Alto Verde, en el Polideportivo ubicado en Manzana 6, habrá básquetbol de lunes a viernes; fútsal de 9 a 10; vóley playa de 10 a 11; natación menores nivel 3 de 11 a 12; natación menores nivel 2 de 10 a 11; natación menores nivel 1 de 11 a 12; natación para niños (N 1) de 9 a 10; hockey de 9 a 10; natación adultos mayores los lunes, miércoles y viernes de 10.30 a 11.30; y las actividades para adultos mayores los lunes, miércoles y viernes de 9.30 a 10.30 horas. Por la tarde las propuestas son las siguientes: ritmos y aerolocal martes y jueves de 14 a 15; Sipalki- Do los lunes, miércoles y viernes de 15.30 a 16.30; fútbol femenino y masculino todos los días de la semana de 18 a 19; actividades primera infancia de 16 a 17; natación menores nivel 3 de 17 a 18; natación menores nivel 2 de 16 a 17; natación menores nivel 1 de 18 a 19; natación menores (N 1) de 17 a 18; y aquagym de 18 a 19 horas.

Por otra parte, en la Estación La Boca, ubicada en la Manzana 0 de Alto Verde, los lunes y miércoles de 17 a 18.30 se realiza Sipalki- Do; mientras que los martes y jueves hay propuestas para los adultos mayores a las 9.30 horas.

En la sede de Deportes, de lunes a viernes se puede realizar aerolocal en el horario de 8 a 9; los lunes, miércoles y viernes de 9 a 10 hay ritmos y de 10 a 11 funcional. Los martes y jueves de 8 a 9 se desarrolla la clase de yoga; los miércoles y viernes de 9.30 a 9.30 la clase de Taichi; mientras que los martes y jueves de 10 a 11 hay funcional; y de 9.30 a 10.30 pilates. Por la tarde, los lunes y miércoles se puede asistir a la propuesta de rollers infantil iniciación a partir de las 17; a rollers patín carrera a las 18; hockey sobre ruedas de 19 a 20; ritmos infantil a las 18; funcional a las 19; y pilates de 18 a 19 horas. Asimismo, los martes y jueves de 18 a 19 hay yoga; y de 19 a 20 ritmos en la playa. De lunes a viernes de 19 a 20 hay ritmos; los lunes, miércoles y viernes de 20 a 21 también hay ritmos. Los martes y jueves de 21 a 22 hay tango. Y, por último, el círculo de corredores funciona los lunes y miércoles de 15 a 16.30 y los martes y jueves de 21.30 a 23 horas.

Para finalizar, también hay actividades en la Estación Belgrano. De lunes a viernes se puede realizar funcional de 8 a 9 y ritmos de 19.30 a 20.30; mientras que los martes y jueves hay ritmos de 9 a 10; yoga y GAP de 10 a 11; y tango de 20.30 a 21.30.

**Recorridos de Mi Ciudad como Turista**

Mi ciudad como turista es una propuesta que ofrece, por un lado, visitas guiadas gratuitas a santafesinos y visitantes con modalidad freetour y; por el otro, recorridos autoguiados que se pueden llevar a cabo sin la necesidad de asistir a un horario específico.

En cualquiera de sus dos modalidades se podrá conocer puntos emblemáticos de la ciudad a través del relato vivencial de quienes los transcurren diariamente, con distancias cortas (entre los 800 a 1500 metros).

Para escuchar y/o descargar las audioguías se puede ingresar a la página de la ciudad: [https://santafeciudad.gov.ar/secretaria-de-produccion-y-desarrollo-economico/turismo/mi-ciudad-como-turista/,](https://santafeciudad.gov.ar/secretaria-de-produccion-y-desarrollo-economico/turismo/mi-ciudad-como-turista/, "https://santafeciudad.gov.ar/secretaria-de-produccion-y-desarrollo-economico/turismo/mi-ciudad-como-turista/,") o también se pueden escuchar desde el perfil de Santa Fe Capital en Spotify: [https://open.spotify.com/show/0TghWaTuOn4LAnbja2YuMC](https://open.spotify.com/show/0TghWaTuOn4LAnbja2YuMC "https://open.spotify.com/show/0TghWaTuOn4LAnbja2YuMC") . Vale destacar que en ambas plataformas se encuentran disponibles los recorridos de Paseo del Puerto, Paseo Costanera, Paseo Peatonal San Martín, Manzana Jesuítica, Paseo Boulevard, Casco Histórico, entre otros.

Las audioguías son un desarrollo de la Municipalidad orientado a generar herramientas

de autogestión útiles para la ciudadanía y los visitantes que permitan reforzar el compromiso como municipio sustentable, generando políticas amigables con el ambiente.