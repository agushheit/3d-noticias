---
category: La Ciudad
date: 2021-10-11T22:55:15-03:00
thumbnail: https://assets.3dnoticias.com.ar/BOLICHES.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Volvieron a abrir los boliches en la noche santafesina
title: Volvieron a abrir los boliches en la noche santafesina
entradilla: Este sábado los boliches bailables apostados en la Ruta 168 volvieron
  a abrir sus puertas luego de casi dos años, por la pandemia del coronavirus.

---
El miércoles pasado y luego del pedido tanto de dueños de boliches como de salones de eventos, la provincia autorizó desde este sábado 9 de octubre, la posibilidad de bailar en lugares cerrados, dándole así la posibilidad a este sector de poder volver a funcionar casi con normalidad, como lo hacía casi dos años atrás, antes del comienzo de la pandemia por coronavirus.

Como normativa para funcionar, el decreto establece que los mismo podrán funcionar viernes, sábado y víspera de feriados. El aforo varía entre los espacios abiertos y cerrados. En los primeros se puede usar el 70% de la superficie mientras en los segundos solo el 50. Podrán permanecer abiertos de 20 a 3 de la mañana y solo podrán ingresar quienes cuenten con al menos una dosis de la vacuna contra el coronavirus aplicada al menos 14 días antes.

**¿Qué pasó en la ciudad de Santa Fe?**

El sábado solo tres de los locales decidieron abrir sus puertas: Altavista, House y Bonito Pueblo y según se informó desde el área de Control de la Municipalidad, los protocolos se cumplieron, así como también los controles por el organismo en el lugar. Según se informó en la primera jornada unos 1.800 santafesinos decidieron asistir a la reapertura de los boliches.

Algunos empresarios decidieron no abrir sus puertas, algunos de ellos muy críticos. “Poner nuestro negocio con lo deteriorado que está en casi dos años sin uso conlleva inversiones muy grandes y el contexto comercial no es muy saludable, por todos los condicionamientos”, sintetizó Jorge Reynoso dueño del complejo La Pirámide.

Por otro lado también dijo que "el área de boliches vino a raíz de una ordenanza de relocalización (y anterior de emergencia nocturna) que antes de la pandemia no se respetó; la gestión anterior no respetó la misma ordenanza que cerró lugares en el centro, que hizo invertir a tres sociedades, y no terminó las obra del sector”, recordó. “Teníamos muchos problemas para funcionar, fue muy restrictiva la actividad y la gente nos dio la espalda; empezaron a proliferar los pubs en barrio Candioti, cosa que también está fuera de la normativa, y hoy esta nueva gestión tiene un desafío muy grande a ver qué va a hacer con la nocturnidad”.

Según Reynoso, solo si se respeta la ordenanza y si el municipio cumple con mejorar la zona los boliches podrían arriesgarse a desarrollar la actividad comercial.