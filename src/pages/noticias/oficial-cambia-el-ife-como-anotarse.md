---
category: Agenda Ciudadana
date: 2021-01-26T09:29:12Z
thumbnail: https://assets.3dnoticias.com.ar/anses.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Con información de El Litoral
resumen: 'Oficial: Cambia el IFE, ¿Cómo anotarse?'
title: 'Oficial: Cambia el IFE, ¿Cómo anotarse?'
entradilla: 'Sin la cuarta entrega del bono de $ 10000, el Ministerio de Desarrollo
  Social buscará crear 300.000 nuevos empleos '

---
El ministro de Desarrollo Social, Daniel Arroyo, informó este domingo cómo mutará la asistencia que el Estado activó por medio del bono de $ 10000 del Ingreso Familiar de Emergencia ( IFE) de Anses por la crisis sanitaria del coronavirus, y que impactó en 8,9 millones de personas, entre ellas titulares de la Asignación Universal por Hijo ( AUH), monotributistas y desempleados. 

"La primera forma que tenemos que hacer para que la gente pueda cambiar de la asistencia por un ingreso sostenido en el tiempo se va a dar por medio del Potenciar Trabajo", enfatizó el funcionario en diálogo con un medio radial. 

Arroyo explicó que quienes ya forman parte de la iniciativa cobran la mitad del salario mínimo, vital y móvil, esto es $ 10.300, tienen que trabajar 4 horas aunque puede extender la jornada con montos adicionales y de cumplir con los requisitos, acceder a créditos para máquinas y herramientas.

"Realmente la salida de la pobreza es con el trabajo y por eso vamos a crear 300.000 nuevos puestos", proyectó el ministro. 

 

**Plan Potenciar Trabajo: qué es**

Se trata de la unificación de los programas Hacemos Futuro y Proyectos Productivos Comunitarios se unificaron en Potenciar Trabajo. Esta iniciativa busca mejorar la empleabilidad y la generación de nuevas propuestas productivas a través de la terminalidad educativa, la formación laboral y la certificación de competencias con el fin de promover la inclusión social plena de aquellas personas que se encuentran en situación de vulnerabilidad social y económica.

 

**Requisitos**

Para permanecer en el programa tenés que cumplir con los siguientes requisitos:

1) Si estás participando en Proyectos Socio-Productivos, Socio-Comunitarios y/o Socio-Laborales:

\- Integrar un grupo de trabajo en una Unidad de Gestión o una Unidad de Gestión Asociada en el que realices alguna de las siguientes actividades: tareas de cuidados, reciclado y servicios ambientales, construcción, infraestructura social y mejoramiento barrial y habitacional, agricultura familiar y producción de alimentos, producción de indumentaria y otras manufacturas, y comercio popular.

\- Realizar las actividades durante un promedio de 80 horas mensuales.

\- Acreditar el cumplimiento de las actividades.

 

2) Si estás bajo la modalidad Terminalidad Educativa (finalización de los estudios primarios y secundarios):

\- Acreditar que estás estudiando con la presentación en ANSES del Formulario de Terminalidad Educativa (FOTE), certificado por la autoridad educativa correspondiente.  

 

**Potenciar Trabajo: ¿Cómo inscribirse?**

A la hora de inscrbirse en el Plan, mediante el Registro Nacional de Trabajadores y Trabajadoras de la Economía Popular (Renatep), Anses solicita la Certificación Negativa que en la que se deja constancia si la persona cobra algún plan social, si tiene aportes como trabajador en relación de dependencia, monotributista o empleada doméstica, entre otros.

 

**Qué es la negatividad de Anses y cómo tramitarla**

Es un comprobante que emite ANSES en el que se deja constancia de que no (o sí) registrás:

\- Aportes como trabajador bajo relación de dependencia.

\- Declaraciones Juradas de provincias no adheridas al SIPA (tanto para trabajadores activos como pasivos).

\- Transferencias como Autónomo y/o Monotributista y/o Trabajadores de Casas Particulares (empleada doméstica).

\- Asignación por Maternidad para Trabajadora de Casas Particulares.

\- Cobro de la Prestación por Desempleo.

\- Cobro de programas sociales.

\- Asignación Universal por Hijo (AUH).

\- Asignación por Embarazo (AUE).

\- Progresar.

\- Cobro de prestaciones previsionales vigentes a la fecha de la solicitud.

\- Obra social.

\- Si estás o no registrado como Monotributista Social.

 

Para tramitar la negatividad de Anses, que tiene una validez de 30 días, es necesario seguir estos pasos:

\- Ingresar a [https://www.anses.gob.ar/consulta/certificacion-negativa](https://www.anses.gob.ar/consulta/certificacion-negativa "https://www.anses.gob.ar/consulta/certificacion-negativa")

\- Seleccionar "Ingresar a la consulta"

\- Completar el los datos solicitados: CUIL y fecha de la consulta

\- Seleccionar "Consultar"

 

**Cómo anotarse al Plan Potenciar Trabajo**

Para solicitar ser parte del programa, es necesario registrarse en el Registro Nacional de Trabajadores y Trabajadoras de la Economía Popular (Renatep). Ser parte le permitirá al inscripto acceder a programas como el Potenciar Trabajo u otros de Anses, poder participar de redes de comercialización e incluso tener acceso a herramientas crediticias.

Pueden inscribirse todos aquellos que realicen actividades en el marco de la economía popular. Por ejemplo, está destinado a vendedores ambulantes, artesanos, cartoneros, recicladores, albañiles, trabajadores de la construcción, pequeños agricultores, etc.

Para la inscripción, es necesario seguir los siguientes pasos: 

\- Ingresar al siguiente link: [https://www.argentina.gob.ar/desarrollosocial/renatep](https://www.argentina.gob.ar/desarrollosocial/renatep "https://www.argentina.gob.ar/desarrollosocial/renatep")

\- Registro Nacional de Trabajadores y Trabajadoras de la Economía

En caso de cumplir con los requisitos allí descriptos, hacer clik en el formulario de inscripción:

 - Cargar tipo y número de Documento

\- Completar datos personales

\- Guardar los datos cargados

\- Una vez que se genere el cartel "Se generó su preinscripción con éxito"

\- Clickear en descargar comprobante para guardarlo