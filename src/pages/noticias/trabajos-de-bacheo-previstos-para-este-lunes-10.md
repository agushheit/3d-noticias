---
category: La Ciudad
date: 2021-08-23T06:00:24-03:00
thumbnail: https://assets.3dnoticias.com.ar/BACHEO22.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Trabajos de bacheo previstos para este lunes
title: Trabajos de bacheo previstos para este lunes
entradilla: 'La Municipalidad informa las calles por las que avanza el cronograma
  de tareas previsto en el Plan de Bacheo 2021. '

---
En enero pasado, la Municipalidad puso en marcha un plan de bacheo para reparar más de 12.000 metros cuadrados de calles de la capital provincial. El programa se desarrolla por fases, priorizando las zonas de la ciudad que evidencian mayor circulación.

**Actualmente se trabaja en:**

Salvador del Carril y General Paz

Marcial Candioti al 2700

Marcial Candioti y Fray Justo Santa María de Oro

Marcial Candioti y Gobernador Candioti

Pedro Díaz Colodrero y Alvear

Necochea y Sargento Cabral

Necochea y Rosalía de Castro

Ituzaingó y Necochea

El municipio informa que todas las tareas se realizan siempre en sentido del tránsito. Puede haber cortes de circulación y desvíos de colectivos, a medida que avanzan las obras y se realizan intervenciones mayores. Además, se detalla que los trabajos están sujetos a las condiciones climáticas.

**Avenida Freyre**

Por tareas relacionadas con el funcionamiento del hospital reubicable, permanece interrumpida la circulación vehicular en ambas manos de avenida Freyre al 2100, tanto para peatones como para el tránsito vehicular.

**Esto implica el desvío de las siguientes líneas de colectivos:**

Línea 9: de recorrido habitual por Mendoza a Saavedra, Juan de Garay, 4 de Enero y Mendoza, a recorrido habitual

Línea 14: de recorrido habitual por avenida Freyre a Primera Junta, Zavalla, Mendoza, Saavedra y Monseñor Zaspe, a recorrido habitual

Línea 16: de recorrido habitual por avenida Freyre a Moreno, Francia, Primera Junta y San Lorenzo, a recorrido habitual

**Por otro lado, las paradas serán:**

Línea 9: avenida Freyre y Primera Junta, avenida Freyre y Mendoza, Mendoza y Saavedra, Saavedra y Lisandro de la Torre, y Juan de Garay y Francia

Línea 14: Mendoza y avenida Freyre, Mendoza y Saavedra, Saavedra y Lisandro de la Torre , Saavedra y Juan de Garay, Saavedra y Moreno, y Monseñor Zaspe y San Lorenzo

Línea 16: avenida Freyre y Moreno, Saavedra y Moreno, Francia y Corrientes, Francia y Lisandro de la Torre, Francia y Mendoza, y Primera Junta y San Lorenzo

**Por el operativo de vacunación**

La Municipalidad comunica que, en el marco del operativo de vacunación que se realiza en la ciudad, habrá controles en el tránsito vehicular en las inmediaciones de:

La Redonda, Arte y Vida Cotidiana, en Salvador del Carril y Belgrano, de 7 a 15 horas

Del mismo modo, se recuerda que continúan los operativos en el ex Hospital Iturraspe (avenida Perón y bulevar Pellegrini), el Cemafe (Mendoza al 2400) y el Hospital de Niños “Dr. Orlando Alassia” (Mendoza al 4100).