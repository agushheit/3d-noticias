---
category: Estado Real
date: 2021-03-06T07:00:37-03:00
thumbnail: https://assets.3dnoticias.com.ar/ambiente.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Provincia participó de la apertura de Asambleas Ordinarias del Consejo
  Federal de Medio Ambiente
title: La Provincia participó de la apertura de Asambleas Ordinarias del Consejo Federal
  de Medio Ambiente
entradilla: Durante la misma, Santa Fe fue elegida para presidir la Comisión de Cuencas
  Hídricas. Además, será parte de la Comisión de Cambio Climático.

---
La ministra de Ambiente y Cambio Climático, Érika Gonnet, participó este jueves de la apertura del período anual de asambleas ordinarias Nº 97 del Consejo Federal de Medio Ambiente (COFEMA). La actividad, llevada a cabo en el Museo del Bicentenario en la Ciudad Autónoma de Buenos Aires, fue encabezada por el presidente electo, Santiago Azulay, secretario de Ambiente de La Rioja, y acompañada en esta oportunidad por el ministro de Ambiente y Desarrollo Sostenible de la Nación, Juan Cabandié. Asimismo, Dina Migani, secretaria de Ambiente de Río Negro, fue electa vicepresidenta del Consejo.

Al respecto, Gonnet detalló que “durante el encuentro se realizó un repaso de las acciones llevadas a cabo durante el 2020, los temas que fueron tratados en las diferentes comisiones y los ejes de trabajo para el presente año. En la elección de autoridades de las diferentes comisiones, la provincia de Santa Fe fue elegida para presidir la Comisión de Aguas y Cuencas Hídricas. Además, también vamos a ser parte de la Comisión de Cambio Climático, teniendo en cuenta que nuestra provincia tiene Ley de Acción Climática desde diciembre del 2020”.

Durante la Asamblea, el Consejo reconoció el valor del Programa “Club Ambiental, Solar y Educativo” (CASE), y sus efectos como acción concreta de mitigación contra el cambio climático, así como también su valor como instrumento de educación ambiental no formal, de promoción de tecnologías amigables con el ambiente y de impulso al mercado de los empleos verdes y las renovables, y con la posibilidad concreta de ser replicado a lo largo de todo el territorio nacional deciden declarar de interés como acción de mitigación contra el cambio climático al Programa “Club Ambiental, Solar y Educativo”.

Cabe señalar que a través de la implementación del Programa CASE se han beneficiado más de 40 localidades de la Provincia y unos 20.000 jóvenes deportistas santafesinos que vieron a sus clubes transformarse en espacios deportivos más sostenibles.

Por último, Gonnet detalló que “estuvimos dialogando con el ministro Juan Cabandié sobre el trabajo que venimos llevando adelante sobre el inventario de humedales que presentamos el pasado 2 de febrero y a raíz de esta iniciativa, van a iniciar sus inventarios las provincias de Río Negro y Tierra del Fuego”.

Acompañaron a la ministra, Saida Caula, directora provincial de Economía Circular y Marcelo Gallini, subsecretario de Cambio Climático.

**Cofema**

El Consejo Federal de Medio Ambiente (COFEMA) tiene su origen en un Acuerdo Federal, instrumentado en su respectiva Acta Constitutiva y reconoce como antecedente directo el Pacto Federal Ambiental de 1993.

De acuerdo a lo establecido en el Art. N° 39 del Acta Constitutiva de 1996, es un sujeto de derecho público, constituido por las provincias signatarias; las que adhieran en el futuro; el Estado Nacional, y la actual Ciudad Autónoma de Buenos Aires.

Dicho organismo, emanado de la voluntad estatal local y central, tiene como principio rector el federalismo y reconoce como objetivo principal, el de coadyuvar a la generación de una política ambiental de integración entre las provincias y el gobierno federal.

Este Consejo Federal es un ámbito de concertación de políticas ambientales, en el cual los representantes de las diversas jurisdicciones, se expiden a través de acuerdos y normas como Resoluciones y Recomendaciones, consensuadas, acordadas y suscritas en el marco de las Asambleas Ordinarias y Extraordinarias.