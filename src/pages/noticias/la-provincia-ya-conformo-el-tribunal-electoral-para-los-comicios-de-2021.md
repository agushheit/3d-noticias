---
category: Agenda Ciudadana
date: 2020-12-11T16:26:02Z
thumbnail: https://assets.3dnoticias.com.ar/falistocco.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Diario UNO'
resumen: La provincia ya conformó el Tribunal Electoral para los comicios de 2021
title: La provincia ya conformó el Tribunal Electoral para los comicios de 2021
entradilla: Será presidido por Héctor Falistocco, quién será el presidente de la Corte
  el año que viene. Los vocales titulares serán los jueces Daniel Fernando Acosta
  y Javier Miguel Mirande.

---
La Corte Suprema de Justicia de la provincia anunció este jueves, conforme la previsión contenida en el artículo 29 de la Constitución de la provincia, en consonancia con leyes y reglamentaciones respectivas, la integración del **Tribunal Electoral de la provincia de Santa Fe para el año 2021**.

El mismo será presidido por el presidente de la Corte Suprema de Santa Fe, electo para dicho período, Roberto Héctor Falistocco y como vocales por los magistrados que resultaron sorteados. Los vocales titulares son Daniel Fernando Acosta, juez del Colegio de Cámara de Apelación en lo Penal de Rosario; y Javier Miguel Mirande, juez de la Cámara de Apelación de Circuito de Santa Fe

Mientras que los vocales suplentes son Eduardo Jorge Antonio Pagnacco, juez de la Cámara de Apelación de Circuito de Rosario; y Oscar José Burtnik juez del Colegio de Cámara de Apelación en lo Penal de Santa Fe. Mientras que actuará como procurador Fiscal Jorge Alberto Barraguirre, procurador General de la Corte.

**La última vez que Falistocco presidió el Tribunal Electoral de la provincia fue en 2015 y no le tocó una tarea sencilla**. En aquella ocasión, a diferencia de las elecciones de medio término del año que viene, se dirimían elecciones locales, senadores y diputados provinciales y la gobernación. En las primarias, que se disputaron el 19 de abril no hubo mayores inconvenientes. 

En el Frente Progresista, la lista Adelante, que llevaba a Miguel Lifschitz como candidato a gobernador, se impuso con el 70,65% de los votos a Mario Barletta, que era el postulante por la lista Firmeza para avanzar. Sin embargo, los 533.087 sufragios que sumaban los dos candidatos del Frente –los 376.627 de Lifschitz y los 156.460 de Barletta– no alcanzaban al candidato más votado. Miguel del Sel había conseguido 536.480 votos por la Unión PRO Federal. En tercer lugar había quedado Omar Perotti, del Frente Justicialista para la Victoria, con 365.239.

![](https://assets.3dnoticias.com.ar/tribunal1.jpg)

Lejos de polarizarse la elección general entre los dos candidatos más votados, los comicios que se desarrollaron el 14 de junio de ese año mantuvieron un reparto de tercios con mucha paridad. 

En esos días se vivieron momentos de mucha tensión en el local de bulevar Pellegrini al 2900, donde funcionaba entonces la Secretaría Electoral provincial. 

El escrutinio demandó nueve días donde los fiscales partidarios cruzaron acusaciones y los del PRO pedían abrir muchas urnas. Eso hizo que los resultados se oficializaran el 24 de junio. Lifschitz había conseguido 584.017 votos; Del Sel 582.521; y Perotti 558.57. El Frente Progresista se había impuesto por solo 1.496 sufragios, una de las diferencias más exiguas en la historia de las elecciones a gobernador en la provincia. En principio el candidato del PRO no quería reconocer la derrota y amenazó con judicializar el conteo. Sin embargo, un día después reconoció el triunfo de Lifschitz. 

**Quien debió conducir y hacerse cargo de la transparencia de ese escrutinio fue Falistocco, que ahora volverá a presidir el Tribunal Electoral.**