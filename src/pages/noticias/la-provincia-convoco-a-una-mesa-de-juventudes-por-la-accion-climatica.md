---
category: Estado Real
date: 2021-01-29T10:10:01Z
thumbnail: https://assets.3dnoticias.com.ar/gonnet.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: La provincia convocó a una Mesa de Juventudes por la Acción Climática
title: La provincia convocó a una Mesa de Juventudes por la Acción Climática
entradilla: La ministra Erika Gonnet mantuvo un encuentro con juventudes de distintas
  localidades en el marco de las actividades por el Día de la Educación Ambiental.

---
La ministra de Ambiente y Cambio Climático, Erika Gonnet, encabezó una nueva reunión de la Mesa de Juventudes por la acción climática, espacio que surge conjuntamente con la sanción de la Ley de Acción Climática Provincial, impulsada por un proyecto del gobernador Omar Perotti. El encuentro sucedió en la sede de Gobierno de Rosario, con la participación virtual de jóvenes, profesionales, representantes de organizaciones de la sociedad civil y estudiantes de distintas localidades.

La jornada contó con la participación de representantes de diversas organizaciones entre las que se encontraban el Grupo de Reflexiones Ambientales desde Latinoamérica, la ONG Sentido Ambiental, el Frente universitario por el ambiente, el Consultorio jurídico de la UNR, Agenda Ambiental Santa Fe, jóvenes de Rosario, Rafaela, Santo Tomé, Santa Fe y Reconquista, entre otros.

“El tema que nos convocó es la educación ambiental que es transversal a todos los actores, donde tratamos de incluir que sea un derecho, que podamos avanzar en esa responsabilidad social de todos los sectores; y lo tenemos que hacer con quienes llevan esta bandera que son las juventudes de la provincia”, afirmó Gonnet.

“Fue un espacio de debate, porque se plantearon varios temas, no solo educación ambiental sino también pudimos hablar de otras temáticas sobre las cuales ya veníamos trabajando en nuestro ministerio, incorporando además a muchos y muchas jóvenes de nuestro ministerio” resaltó Gonnet.

Y agregó que se habló “de la cuestión legislativa, de las cosas legisladas y las que faltan legislar, las que hay que corregir y también aprovechamos la oportunidad para contar que el gobierno de la provincia de Santa Fe, en conjunto con el gobierno nacional, está formalizando el proyecto de ampliación del Parque Nacional Islas de Santa Fe”.

"En este marco que hemos vivido a lo largo de todo el año, atravesado por una pandemia y con los incendios, la provincia avanzó con diferentes acciones como la ampliación de las áreas naturales protegidas de Santa Fe, y hoy anunciamos, junto con parques nacionales, la ampliación del Parque Nacional Islas de Santa Fe”, desarrolló la propuesta la Ministra, luego de la actividad de la que participó también Eugenio Magliocca, integrante de la Administración de Parques Nacionales.

“El objetivo es que la Mesa de Juventudes sea un espacio de diálogo y construcción, que pueda ampliarse y potenciarse, son quienes llevan la bandera ambiental al frente y quienes deben tener un rol principal”, valoró Gonnet. “La Ley de Acción Climática es un claro ejemplo de ello, los jóvenes tuvieron un rol fundamental en el desarrollo”, ejemplificó.

Por su parte, el Ing. Franco Blatter, subsecretario de Tecnologías para la Sostenibilidad, contó que “en el marco de la Ley de Acción Climática que tiene la provincia de Santa Fe, convocamos a la Mesa de Juventudes que es uno de los instrumentos que establece la norma para que podamos implementar las políticas para toda la provincia impulsada por los jóvenes”.

“Esta es una situación muy importante porque nos da la voz de los jóvenes y los involucramos en las cuestiones del día a día del Ministerio, a su vez participaron un montón de organizaciones de varios lugares de la provincia”, amplió.

Por último, Blatter indicó que “quedaron en agenda de trabajo 3 temas muy segmentados y específicos: la construcción de normativa; la construcción de espacios de reflexión y conocimiento; y por otro lado, la cuestión de promoción ambiental y voluntariado ambiental en territorio. Porque la problemática ambiental refleja una cuestión de justicia social y justicia socio ambiental”.

Durante el encuentro se atravesaron temas de educación ambiental, ley de acción climática, humedales y áreas protegidas, la propuesta de ampliación del Parque Nacional Islas de Santa Fe, y el trabajo a futuro de proyectos vinculados, entre otros temas, a la biodiversidad. Motivo de este último punto, luego del encuentro, los integrantes de la Mesa de las Juventudes recorrieron junto a la Ministra y el equipo de trabajo, las instalaciones del Acuario del Río Paraná.

De la actividad participó además el subsecretario de Cambio Climático, Ing. Marcelo Gallini, e integrantes del equipo de trabajo del Ministerio de Ambiente y Cambio Climático.