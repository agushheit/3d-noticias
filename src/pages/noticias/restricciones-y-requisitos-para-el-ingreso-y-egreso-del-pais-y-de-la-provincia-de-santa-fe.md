---
category: Agenda Ciudadana
date: 2021-01-19T09:00:43Z
thumbnail: https://assets.3dnoticias.com.ar/terminal.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: " Restricciones y requisitos para el ingreso y egreso del país y de la provincia
  de Santa Fe"
title: Restricciones y requisitos para el ingreso y egreso del país y de la provincia
  de Santa Fe
entradilla: En el marco de la pandemia de Covid 19, la Defensoría realizó un compendio
  de la normativa vigente para el tránsito.

---
La Defensoría del Pueblo de la provincia de Santa Fe recuerda las restricciones para el ingreso y egreso del país dispuestas por el Ejecutivo nacional y los requisitos para el tránsito dentro del territorio argentino, en el marco de la pandemia de Covid 19.

 

Están autorizados a ingresar al país:

\-Transportistas o tripulantes de cualquier nacionalidad en ejercicio de su actividad.

\- Nacionales y residentes en la República Argentina, cualquiera sea su lugar de procedencia.

\- Extranjeros autorizados expresamente para desarrollar actividad laboral o comercial, cumplir con una misión oficial diplomática, para participar de eventos deportivos que cumplan los protocolos específicos y para reunificación familiar con argentinos residentes que requieren visado.

\- Extranjeros autorizados que sean parientes directos de ciudadanos argentinos, y que ingresen transitoriamente a nuestro país por razones de necesidad, y no requieran visado en virtud de acuerdos bilaterales o medidas unilaterales que eximan de dicho requisito

\- Extranjeros declarados en tránsito hacia otros países con una permanencia en el aeropuerto internacional menor a 24 horas.

 

**Requisitos para viajar hacia Argentina:**

\- Declaración jurada electrónica para el ingreso al territorio nacional.

\- Todos los viajeros deberán cumplir con la prueba de diagnóstico 72 horas previas al viaje internacional con resultado negativo para Covid 19 y acompañar a la declaración jurada electrónica.

\- Cumplir cuarentena de 10 días desde que les fuera efectuada la prueba PCR negativa.

\- Acompañar la declaración jurada electrónica con un seguro de viajero especial para ciudadanos extranjeros no residentes.

Actualmente están suspendidos todos los vuelos directos de pasajeros que tengan como origen o destino el Reino Unido de Gran Bretaña e Irlanda del Norte, ante el nuevo linaje en la secuenciación de muestras locales, respecto al ingreso de personas.

 Se recomienda fuertemente diferir viajes al exterior cuando no fueran para actividades esenciales a:

\- los nacionales o extranjeros residentes en el país,

\- en particular, a los mayores de sesenta años de edad o a personas pertenecientes a los grupos de riesgo definidos por la autoridad sanitaria,

 

**Qué se exige al llegar**

\-Control sanitario

\-Inspección visual

\- Reporte de la tripulación de terceros

\- Autoreporte

\- Evaluación del riesgo sanitario

\- Aleatorio

 

**Uso de aplicaciones**

Se recomienda registrarse en la APP CUIDAR (los nacionales) dentro de las 48 horas del arribo o en la APP específica para los extranjeros una vez que esté disponible, o a través de la web, con el fin de:

\- Autofavorecer el conocimiento de la situación de riesgo.

\- Autodiagnóstico.

\- Sensibilización sobre recomendaciones sanitarias de prevención y autocuidado.

 

**REQUISITOS ADICIONALES SOLICITADOS POR LA JURISDICCIÓN DE DESTINO EN EL PAÍS**

\- Cumplir con los protocolos específicos, en especial los corredores seguros

\- Respetar los requisitos de la autoridad sanitaria local y contar con los respectivos permisos de circulación.

 

**Qué se exige al salir**

\-Declaración jurada electrónica

\- Completarla 48 horas antes de iniciar el viaje por ser requisito migratorio

\- Exhibirla al embarque al operador del medio de trasporte internacional o si viaja por un medio de trasporte propio completarla antes de iniciar el viaje

\- Someterse al control de salud en el punto de entrada cuando se considere necesario

 

Quienes deseen más información sobre la normativa nacional y los requisitos pueden ingresar a [https://www.argentina.gob.ar/salud/coronavirus/ingresar-egresar-argentina](https://www.argentina.gob.ar/salud/coronavirus/ingresar-egresar-argentina "https://www.argentina.gob.ar/salud/coronavirus/ingresar-egresar-argentina")

 

**Ingreso a la provincia de Santa Fe con fines turísticos**

Desde el 21 de diciembre pasado, pueden ingresar a la provincia de Santa Fe, por motivos turísticos:

\- Residentes en la provincia de Santa Fe

\- Propietarios de inmuebles o complejos turísticos que no sean residentes en la provincia

\- Personas residentes de otras provincias y/o extranjeros en todo el territorio provincial.

 

**Los requisitos a cumplir son:**

\- Registrarse en la aplicación covid-19 de la Provincia de Santa Fe y completar con carácter de Declaración Jurada el formulario que se encuentra en la misma: [https://www.santafe.gob.ar/ms/covid19/aplicacion-movil-covid-19-provincia-de-santa-fe](https://www.santafe.gob.ar/ms/covid19/aplicacion-movil-covid-19-provincia-de-santa-fe "https://www.santafe.gob.ar/ms/covid19/aplicacion-movil-covid-19-provincia-de-santa-fe")

\- Certificado de circulación Nacional: www.argentina.gob.ar/verano

\- Contar con reserva de un servicio o alojamiento turístico.

\- Menor de edad: El adulto responsable referenciará en la aplicación los datos de ambos.

 

**Ingreso a otras provincias con fines turísticos**

Para conocer los requisitos y restricciones de ingreso a cada una de las provincias de la República Argentina pueden hacerlo ingresando a [https://www.argentina.gob.ar/transporte/covid-19/requisitos-ingreso-por-provincia](https://www.argentina.gob.ar/transporte/covid-19/requisitos-ingreso-por-provincia "https://www.argentina.gob.ar/transporte/covid-19/requisitos-ingreso-por-provincia")