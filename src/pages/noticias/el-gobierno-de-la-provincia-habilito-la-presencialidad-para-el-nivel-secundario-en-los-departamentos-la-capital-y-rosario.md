---
category: Agenda Ciudadana
date: 2021-07-01T08:51:50-03:00
thumbnail: https://assets.3dnoticias.com.ar/escuela1.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Gobierno de Santa Fe
resumen: El gobierno de la provincia habilitó la presencialidad para el nivel secundario
  en los departamentos La Capital y Rosario
title: El gobierno de la provincia habilitó la presencialidad para el nivel secundario
  en los departamentos La Capital y Rosario
entradilla: La medida es a partir del jueves 1° de julio y abarca a todos los años
  de la educación secundaria.

---
Conforme a los indicadores sanitarios de los departamentos La Capital y Rosario que muestran una mejora de la situación sanitaria, el ministerio de Educación habilitó la presencialidad en los establecimientos educativos del nivel secundario de toda la provincia.

Esta medida tiene como objetivo que  las y los estudiantes de este nivel puedan tener contacto presencial con las y los docentes antes del receso escolar de invierno. Los encuentros se darán  dentro de los protocolos de la presencialidad cuidada y con el sistema de alternancia bimodal.

Bajo las pautas de este formato de alternancia, cada establecimiento desplegará las estrategias organizacionales pertinentes a fin de garantizar la presencialidad de todas las burbujas o sub-grupos antes del inicio del receso escolar.