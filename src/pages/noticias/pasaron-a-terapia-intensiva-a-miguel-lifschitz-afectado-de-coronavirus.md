---
category: Agenda Ciudadana
date: 2021-04-22T07:53:27-03:00
thumbnail: https://assets.3dnoticias.com.ar/miguel-lifschitz-diputadospng.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Diario Uno
resumen: Pasaron a terapia intensiva a Miguel Lifschitz, afectado de coronavirus
title: Pasaron a terapia intensiva a Miguel Lifschitz, afectado de coronavirus
entradilla: Fue derivado a la sala de cuidados intensivos para mayor control e incremento
  de soportes terapéuticos

---
El diputado provincial y ex gobernador de la provincia Miguel Lifschitz fue trasladado a una unidad de cuidados intensivos en el sanatorio del centro rosarino donde se encuentra internado desde el lunes debido a complicaciones en su cuadro de coronavirus.

"En el día de la fecha el equipo médico evaluó que, para tener un mayor control de su evolución y para incrementar los soportes terapéuticos, el Ing. Miguel Lifschitz continuará su recuperación en la unidad de cuidados intensivos", se detalla en el breve comunicado. "El paciente se encuentra estable y en buen estado general", se aclara.

El socialista había manifestado algunos síntomas el 10 de abril pasado y, un día más tarde, comentó a través de las redes sociales que se había realizado el hisopado, con resultado positivo de coronavirus.

Lifschitz fue internado este lunes en un sanatorio de Rosario, en el marco de un aumento de casos de Covid-19 en el país y, especialmente, en territorio santafesino, lo que provoca alarma en el sistema sanitario, desde donde reclaman mayores restricciones a la movilidad y a las actividades comerciales.