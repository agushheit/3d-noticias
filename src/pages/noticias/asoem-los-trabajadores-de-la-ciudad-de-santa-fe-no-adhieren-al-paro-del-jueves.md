---
category: Agenda Ciudadana
date: 2021-06-17T07:12:23-03:00
thumbnail: https://assets.3dnoticias.com.ar/municipalidad-santafe.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: 'Asoem: los trabajadores de la ciudad de Santa Fe no adhieren al paro del
  jueves'
title: 'Asoem: los trabajadores de la ciudad de Santa Fe no adhieren al paro del jueves'
entradilla: Asoem, el gremio de los municipales de Santa Fe, Rincón, Recreo, Arroyo
  Leyes y Monte Vera no adhiere a las medidas de Festram. Habrá jornada normal de
  trabajo

---
El Cuerpo de delegados de la Asoem definió –en forma virtual y por amplia mayoría– solidarizarse con todos los trabajadores que están afrontando una crítica situación sanitaria y realizar las tareas propias de los agentes municipales y comunales en toda la jurisdicción. De esta manera, tanto la Comisión Directiva como el Cuerpo de Delegados, expresaron su desacuerdo con la medida de acción directa planteada por la Festram para el próximo jueves 17 de junio, “en defensa de la seguridad, la salud y la vida”.

Entre las consideraciones expresadas para adoptar la resolución en la jurisdicción de la Asoem (Santa Fe, Rincón, Recreo, Arroyo Leyes y Monte Vera), se destacan:

• La gestión de la pandemia de la Asoem.

• Las estadísticas de seguimiento de los ocho Comités Paritarios de Salud y Seguridad en el Trabajo según Ley Provincial Nº 12.913.

• La situación, que vive la ciudadanía del ámbito metropolitano que representa el sindicato de Asoem.

• La madurez y responsabilidad que tiene la entidad sindical, como así también, sus representados: los trabajadores.

• La atemporalidad de la medida, cuando el desafío hoy es estar todos juntos en la pelea contra el virus.

**Argumentos**

“El día 17 de junio la jurisdicción de Asoem no va a hacer ninguna medida de acción directa, va a trabajar de manera normal y va a seguir apoyando. De hecho, en la ciudad de Santa Fe y en la zona metropolitana tenemos muchísimos centros de vacunación que necesitan de los trabajadores municipales y comunales. Hoy no podemos estar en contra de la sociedad”, afirmó el secretario general Juan Medina.

“La medida tiene sustento en la madurez y la responsabilidad que debemos tener los dirigentes. No podemos, hoy por hoy, plantear una medida negativa”, sostuvo y añadió que: “La no realización de la medida de acción directa tiene que ver con toda la estructura de gestión de pandemia que ha puesto el sindicato de Asoem desde marzo de 2020 hasta el momento”.

Además, señaló que la importancia del trabajo conjunto entre los gobiernos nacional, provincial, municipal y comunal con las organizaciones sindicales y sociales para “codo a codo, pelear contra este virus que está socavando a la sociedad”.

Medina también aclaró que, “el sindicato de Asoem, administrativamente, sigue estando dentro de la Federación, pues se envía todos los meses las cuotas societarias de los trabajadores, pero políticamente hace un año y medio que Asoem tiene su propia impronta dentro de la región". "De hecho, nuestras paritarias locales son mucho más importantes que las paritarias provinciales en un montón de cuestiones”, concluyó el sindicalista.