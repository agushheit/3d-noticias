---
category: Estado Real
date: 2020-12-23T11:30:23Z
thumbnail: https://assets.3dnoticias.com.ar/2312navidad.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: " Navidad y Año Nuevo: Recomendaciones para personas que se alojan en residencias"
title: " Navidad y Año Nuevo: Recomendaciones para personas que se alojan en residencias"
entradilla: El Ministerio de Salud detalló medidas para el egreso de los adultos mayores,
  la duración del encuentro y la vuelta al lugar mencionado, con un aislamiento preventivo.

---
El Ministerio de Salud detalló medidas para el egreso de los adultos mayores, la duración del encuentro y la vuelta al lugar mencionado, con un aislamiento preventivo.

El Gobierno de la provincia, a través del Ministerio de Salud, reiteró la existencia de «trasmisión comunitaria de Covid en la mayoría de las jurisdicciones y que el 83% de las muertes se producen en mayores de 60 años, mientras el 85% de los casos confirmados ocurren en menores de 60 años».

Ante este escenario y con la proximidad de las celebraciones de Navidad y Año Nuevo, recuerdan que «es fundamental la aplicación de medidas de prevención para reducir el riesgo de contagio, particularmente en aquellas personas que pertenecen a la población con mayor vulnerabilidad, ya que no contamos con un tratamiento efectivo».

## **Residencias de larga duración**

Puntualmente, desde la cartera sanitaria provincial se evaluó, tal como se ha hecho a lo largo del año, la situación de las personas que se alojan en residencias para adultos mayores. Para aquellos que van a salir de estos lugares a pasar las fiestas con sus familias, se deberá firmar una declaración jurada -por parte de la persona responsable de retirar al/la residente-, pero además se establecerá un aislamiento preventivo cuando reingrese a dicho lugar, pasado el encuentro familiar.

## **Para ello se emitieron las siguientes recomendaciones:**

◾ La persona responsable de buscar al mayor en el establecimiento deberá completar y firmar previo al mismo la declaración jurada que se ofrece a continuación:

<br/>

[<span style="font-family: arial, helvetica; font-size: 16px; font-weight: 700; color: #ffffff; background: #EB0202; margin: 35px; overflow: hidden; padding: 10px;">  **DECLARACIÓN JURADA** </span>](https://assets.3dnoticias.com.ar/ddjj-retiro-ancianos-de-residencia.pdf "Descargar DDJJ")

<br/>

◾ Si alguna persona tiene síntomas o diagnóstico de COVID-19, o es contacto de un caso confirmado, debe permanecer en aislamiento y por lo tanto no participar de reuniones sociales ni salir de su casa, excepto para buscar atención médica.

<br/>

***

## **Luego del encuentro:**

En caso de que alguna de las personas que participaron en el encuentro festivo con una persona mayor comenzara inmediatamente o posterior al mismo con síntomas o diagnóstico de COVID-19, además de dar aviso a sus contactos estrechos, deberá comunicar esta situación a la residencia para que se tomen todas las medidas de prevención correspondientes.

<br/>

## **Aislamiento**

◾ Al regreso a la residencia, el adulto mayor que participó de encuentros festivos por fuera de la misma, deberá permanecer en aislamiento por 14 días. El aislamiento podrá realizarse por cohorte en caso de que quienes fueron retirados del establecimiento hayan sido más de uno. En este caso, se tomarán todas las medidas que implican el aislamiento (uso de EPP en el personal, etc.).

◾ Si durante el aislamiento el adulto mayor comienza con uno o más síntomas de COVID-19, deberá activarse el protocolo de caso sospechoso.

<br/>

***

## <span style="color: #EB0202;">**Las recomendaciones generales deberán ser respetadas de la manera más estricta posible**</span>

◾ Se recomienda que las reuniones se realicen al aire libre, en patios, terrazas, veredas y siempre garantizando que se cumpla con el distanciamiento físico pertinente para minimizar el contacto entre los asistentes.

◾ De no ser posible al aire libre, abrir puertas y ventanas para asegurar buena ventilación.

◾ Dentro de lo posible, mantener la distancia mínima requerida entre personas, máxime con las personas con factores de riesgo o mayores de 60 años.

<br/>

## **Burbujas sociales**

◾ Limitar el número de personas en cada reunión.

◾ En la distribución de las personas en la mesa, cena o brindis, se recomienda que aquellos convivientes, o contactos habituales, se ubiquen juntos y sin mezclar con otros grupos.

◾ NO compartir vasos, cubiertos ni utensilios. No tomar de la misma botella o lata.

◾ En todo momento cumplir con las medidas de distanciamiento, uso de barbijo e higiene de manos.

***