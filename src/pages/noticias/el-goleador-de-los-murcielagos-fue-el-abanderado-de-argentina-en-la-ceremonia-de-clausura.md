---
category: Deportes
date: 2021-09-06T06:00:39-03:00
thumbnail: https://assets.3dnoticias.com.ar/PARALIMPICOS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: El goleador de Los Murciélagos fue el abanderado de Argentina en la ceremonia
  de clausura
title: El goleador de Los Murciélagos fue el abanderado de Argentina en la ceremonia
  de clausura
entradilla: En la mejor actuación de nuestro país en más de veinte años -con 9 medallas
  y 32 diplomas-, el goleador de fútbol para no videntes Maximiliano Espinillo portó
  la bandera argentina en la fiesta de cierre.

---
Maximiliano Espinillo, goleador de Los Murciélagos, ganadores de la medalla de plata, fue el abanderado de la delegación argentina en la ceremonia de clausura de los Juegos Paralímpicos de Tokio 2020.

Con nueve medallas y 32 diplomas, Argentina cerró la 16ta edición de los Juegos Paralímpicos con su mejor actuación del siglo XXI.

Espinillo marcó siete goles en los cinco partidos que disputó el equipo de fútbol de no videntes que perdió la final por un ajustado 1-0 ante Brasil.

En Tokio 2020, la delegación argentina casi duplica las cinco (1 de oro, 1 de plata y 3 de bronce) de Rio 2016.

Lo más destacado de la delegación nacional pasó por la natación, el atletismo, con Hernán Urra y Antonella Ruiz, y Los Murciélagos que estuvieron cerca de la tan ansiada medalla dorada.

En total fueron 59 atletas (19 mujeres y 40 hombres) los que representaron a Argentina en Tokio 2020.

**Los medallistas argentinos**

Brian Impellizzeri (Atletismo - Plateada en salto en largo T37), Fernando Carlomagno (Natación - Plateada en 100m espalda S7), Hernán Urra (Atletismo - Plateada en lanzamiento de bala F35), Matías De Andrade (Natación - Plateada en los 100 metros espalda S6), Los Murciélagos (Plateada en fútbol para ciegos), Antonella Ruiz Díaz (Atletismo - Bronce en lanzamiento de bala F41), Yanina Martínez (Atletismo - Bronce en los 200 metros T36), Juan Samorano (Taekwondo - Bronce en K44 hasta 75 kilos) y Alexis Chávez (Atletismo - Plateada en los 100 metros T36) ganaron las nueve medallas de Argentina que igualaron el registro de 1996