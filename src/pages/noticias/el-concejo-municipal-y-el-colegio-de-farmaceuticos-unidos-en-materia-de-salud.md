---
category: La Ciudad
date: 2021-03-15T06:10:13-03:00
thumbnail: https://assets.3dnoticias.com.ar/concejocolegio.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: El concejo Municipal y el colegio de farmacéuticos; unidos en materia de
  salud
title: El concejo Municipal y el colegio de farmacéuticos; unidos en materia de salud
entradilla: 'El presidente del Concejo Municipal Leandro González firmó un convenio
  marco de colaboración mutua con representantes del Colegio de Farmacéuticos 1° Circunscripción. '

---
En la sala Zapata Gollán del Concejo Municipal, autoridades del Colegio de Farmacéuticos 1° Circunscripción participaron de una firma de convenio con el presidente del Concejo Municipal, Leandro González, con el fin de llevar adelante actividades en conjunto que ayuden a concientizar a la sociedad sobre diferentes temáticas vinculadas a los medicamentos y sus consumos.

González explicó que para el Concejo es “muy importante avanzar en trabajos colaborativos con organizaciones. En este caso coincide que tenemos objetivos desde el Estado también planteados por ellos, con la idea de trabajar contenido social y capacitación a la comunidad”.

González comentó que una de las ideas es retomar algo que desde el Concejo junto al Colegio ya ha sido trabajado años anteriores: “Queremos retomar una campaña articulada sobre la peligrosidad del consumo de medicamentos a la hora de manejar, algo a lo que no le prestamos atención y que sucede cotidianamente y de forma normalizada en la sociedad. También trabajaremos sobre un planteo del Colegio trabajado por la concejala Laura Spina vinculado a la disposición final de los medicamentos vencidos, que muchas veces tenemos en nuestros hogares y no sabemos si tirarlos a la basura o al inodoro, cuando en realidad lo que necesitamos es un sistema articulado de disposición final, donde la gente sepa dónde y de qué manera dejar sus medicamentos vencidos o inutilizados”.

La presidenta del Colegio Miriam del Pilar Monasterolo comentó que están “muy conformes y muy contentos por la firma de esta acta acuerdo dentro de un convenio marco de colaboración recíproca entre nuestro colegio y el Honorable Concejo Municipal. Es un tema que habíamos comenzado a conversar en el mes de enero y hoy pudimos concretarlo con un proyecto de trabajo muy importante, con ejes para la población santafesina, que va a repercutir y mejorar la calidad de vida con campañas de difusión como dijo Leandro, con un eje en la disposición final de medicamentos peligrosos, con la peligrosidad de la conducción bajo el consumo de ciertos medicamentos, y también con la preocupación que nos genera la venta de medicamentos fuera de las farmacias. Muchas temáticas interesantes que iremos concretando a lo largo del año con una agenda establecida”.

En la firma del convenio estuvo presente junto a las máximas autoridades del Colegio de Farmacéuticos 1 Circunscripción el concejal Carlos Suarez.