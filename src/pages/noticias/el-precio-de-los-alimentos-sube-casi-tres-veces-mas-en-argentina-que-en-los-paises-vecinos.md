---
category: Agenda Ciudadana
date: 2022-04-19T08:52:06-03:00
thumbnail: https://assets.3dnoticias.com.ar/noodles-2150272_1920.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa SRA
resumen: El precio de los alimentos sube casi tres veces más en Argentina que en los
  países vecinos.
title: El precio de los alimentos sube casi tres veces más en Argentina que en los
  países vecinos.
entradilla: Según la entidad es así, porque no se combate adecuadamente la inflación

---
La inflación en Argentina no cede. El INDEC midió para marzo una suba de los precios minoristas de 6,7% mensual y de 55,1% anual. Los precios crecieron liderados por los rubros educación (23,6% mensual), prendas de vestir y calzado (10,9%) y vivienda, electricidad y gas (7,7%). Los alimentos, a pesar de las expectativas, no lideraron las subas, sino que lo hicieron 7,2% mensual. Dentro de alimentos y bebidas no alcohólicas (área AMBA), lo que más influyó fue la suba de pan y cereales (11,6% mensual), seguido de lácteos (9,3%) y café, té y yerba (8,2%).

Los precios de los alimentos, como de tantos otros productos, quedan inmersos en la dinámica de inercia inflacionaria, que acumula 55,1% anual, y que ya tiene una estimación anual de las consultoras del REM del Banco Central de un promedio de 59,5% anual para 2022. Los shocks de precios internacionales por la guerra de Ucrania-Rusia tienen impactos puntuales sobre los precios: los problemas de inflación se deben a las erróneas políticas anti-inflacionarias, en una economía argentina que ya acumula cuatro años de alta inflación.

Las comparaciones con lo acontecido en países vecinos son relevantes. En Brasil, los precios de los alimentos crecieron en marzo 2,42% mensual y 11,62% anual. La tasa de inflación minorista general fue de 1,62% mensual y 11,3 % anual, el incremento más alto en ese mes desde 2015 (IBGE). Dentro de alimentos y bebidas, el maíz creció 23,3% anual, y las harinas 13,5% anual. La harina de trigo subió 18,02%, y papas y legumbres crecieron 55,9% anual. En una economía que también tiene rubros con mayor inflación que los alimentos, los combustibles crecieron 28,8%, y los transportes 17,7% anual. Como se verifica, si bien hay algunos impactos de precios internacionales, la inflación general del país se mantiene dentro de la tendencia que traía, y no hay controles de precios, de acuerdo a un análisis realizado por el economista jefe de CRA, Ernesto O'Connor.

En Uruguay, en tanto, los precios de los alimentos subieron en marzo 2,43% mensual, impulsados por legumbres y hortalizas. El Índice de Precios de Consumo (IPC) creció 1,11% mensual y 9,38% anual en marzo (INE), siguiendo fuera de la meta del gobierno, de entre 3% y 7% anual. Dentro de alimentos y bebidas no alcohólicas, pan y cereales aumentó 1,82%, carne 2,32%, leche, huevos y quesos 4,24%, frutas 1,14%, y legumbres y hortalizas 5,9%. Se destacan además subas en nafta (2%) y gasoil (1,98%), entre otros rubros.

_De la comparación con Brasil y Uruguay, también productores de granos y alimentos, se pueden extraer lecciones de cómo enfrentar la inflación, sin medidas intervencionistas que desalientan la producción y no resuelven el problema inflacionario, sino que lo agravan._

![](http://www.cra.org.ar/files/image/6/6617/625735327acdb.jpg)