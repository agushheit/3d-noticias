---
category: La Ciudad
date: 2021-09-25T06:00:57-03:00
thumbnail: https://assets.3dnoticias.com.ar/AGUACONCEJO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: La contaminación del agua en barrio Candioti Sur llegó al Concejo
title: La contaminación del agua en barrio Candioti Sur llegó al Concejo
entradilla: 'Este viernes se presentaron dos pedidos de informe para que la empresa
  Assa explique qué fue lo que ocurrió para que se distribuya en la red domiciliaria
  de ese barrio agua no apta para el consumo humano

'

---
Lueg del informe del Enress que afirmaba que se detectaron al menos tres muestras de agua no apta para consumo humano en la red domiciliaria de bario Canditi Sur, el tema llegó al Concejo Municipal. La concejala Laura Mondino presentó dos pedidos de informes dirigidos a la empresa Assa relacionados con el tema y marcó la gravedad del asunto en cuestión. Además hizo referencia al desperdicio del recurso durante las tareas de purga de las cañerías y tanques.

Por su parte, la concejala del Frente Progresista sostuvo: “Nos preocupa el cuidado del agua en Santa Fe y por eso desde el Concejo Municipal pedimos que la Municipalidad eleve un pedido de informes a Aguas Santafesinas. Queremos saber qué pasa con la prestación del servicio, qué cañerías fueron purgadas y cómo la empresa resguarda la calidad del agua”

Los proyectos de Mondino también solicitan información en relación a cuántos litros de agua estiman que se pueden haber perdido, dónde y de qué manera se descartó el agua no apta para consumo humano y qué impacto ambiental puede llegar a generar esta disposición.

“Estamos frente a una situación de gravedad que tiene que ver con la salud comunitaria, pero también con el cuidado del ambiente. Por eso también estamos indagando al Enress respecto de cuáles van a ser las medidas sancionatorias frente a esta situación” completó, y remarcó “el carácter de servicio público y la importancia que reviste la provisión adecuada de agua corriente para la población”.

El químico que llegó a la red de agua de Assa provocó altas concentraciones de aluminio en el líquido que se distribuyó a principios de la semana pasada y que provocó que la misma no cumpla con los parámetros de calidad y no se pueda consumir.