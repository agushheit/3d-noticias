---
category: Estado Real
date: 2021-12-19T06:15:14-03:00
thumbnail: https://assets.3dnoticias.com.ar/OMICRON.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: El Ministerio de Salud de la provincia informó un nuevo caso de la cepa Omicrón
title: El Ministerio de Salud de la provincia informó un nuevo caso de la cepa Omicrón
entradilla: Se trata de una persona de la ciudad de Roldán contacto estrecho del primer
  caso detectado.

---
El Ministerio de Salud de la provincia de Santa Fe informa que se ha detectado un nuevo caso de la cepa Ómicron de Covid 19. Se trata de una persona de la ciudad de Roldán contacto estrecho del primer caso detectado. Esta persona se encontraba aislada de acuerdo al protocolo vigente.

Vale agregar, que la paciente es una mujer de 44 años que cuenta con dos dosis de la vacuna para el Coronavirus y se encuentra cursando la enfermedad en su domicilio. Con este caso, la provincia de Santa Fe registra dos personas contagiadas con la cepa Ómicron de Covid-19.

En este marco, el Ministerio de Salud recuerda que ante cualquier síntoma compatible con Coronavirus - fiebre de 37,5°C, tos, dolor de garganta dificultad respiratoria, rinitis/congestión nasal, dolor muscular, cefalea, diarrea y/o vómitos - deben acercarse al puesto de testeo más próximo.

Asimismo, es fundamental sostener los cuidados personales, lavado de manos, distanciamiento social y el uso de barbijo.