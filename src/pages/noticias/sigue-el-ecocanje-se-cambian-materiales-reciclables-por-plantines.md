---
category: La Ciudad
date: 2021-07-07T08:02:19-03:00
thumbnail: https://assets.3dnoticias.com.ar/reciclado.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Municipalidad Santa Fe
resumen: 'Sigue el Ecocanje: se cambian materiales reciclables por plantines'
title: 'Sigue el Ecocanje: se cambian materiales reciclables por plantines'
entradilla: Una vez al mes, en cada uno de los puntos, se recibe material reciclable,
  limpio y seco. A cambio se otorgan semillas y plantines.

---
Con el objetivo de promover el reciclado, la Municipalidad de Santa Fe continúa con las jornadas de Ecocanje. En diferentes puntos, durante la mañana, una vez al mes, se instalan puestos en los que, a cambio de semillas y plantines para huertas familiares, se reciben materiales reciclables o reutilizables limpios y secos, como papel, cartón, latas y plástico. Lo recolectado es enviado a la planta de clasificación de materiales reciclables y reutilizables del Complejo Ambiental, para ser procesado y comercializado.

En julio, las jornadas de Ecocanje se realizan en distintos barrios, en su mayoría en el horario de 10 a 12. Comenzarán el miércoles 7 en la Feria Agroecológica Candioti, que se realiza en Marcial Candioti y Rosalia Castro; y el jueves 8, la propuesta se traslada a la Estación Barranquitas, ubicada en Iturraspe y Juan Manuel Estrada.

En tanto, el sábado 10 la jornada continúa de mañana en el Mercado Progreso, en Balcarce 1635, y el miércoles 14 es el turno del Jardín Botánico, en avenida José Gorriti 3902.

La jornada de ecocanje llegará al Distrito La Costa el viernes 16, en Ruta 1 y Bv Los Eucaliptos, y el sábado 17 a Vecinal Ceferino Namuncurá, en 12 de Octubre 9501. En ambos casos será de 10 a 12 horas.

En el Distrito Noreste, la actividad que promueve la práctica del reciclado será el miércoles 21 de julio, de 14 a 16, en avenida Aristóbulo del Valle 9446. El sábado 24 la propuesta sigue en el Norte en la sede de la Sociedad de Quinteros de Santa Fe, ubicada en Alfonsina Storni 2500, en el horario de 10 a 12.

El 27 de julio, las jornadas de ecocanje cierran en la sede de la Asociación de Comerciantes de Aristóbulo del Valle (ACAV), en Aristóbulo del Valle 6726, también de 10 a 12.