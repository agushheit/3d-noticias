---
layout: Noticia con imagen
author: "Fuente: Diario Uno"
resumen: "Vuelta a las aulas"
category: Agenda Ciudadana
title: Caravana para reclamar el regreso a clases presenciales en la ciudad
entradilla: "Será este sábado. La caravana, organizada por padres de alumnos de
  los últimos años de secundaria, es en pedido de la finalización presencial del
  ciclo 2020."
date: 2020-10-22T18:09:18.379Z
thumbnail: https://assets.3dnoticias.com.ar/caravana.jpg
---
El próximo sábado 24 de octubre, desde las 17, un grupo de padres autoconvocados de alumnos de los últimos años de secundaria, marcharán en caravana de vehículos desde el Puente Colgante hasta la Casa de Gobierno, con el objetivo de reclamar por la finalización presencial del ciclo 2020 para los 5tos y 6tos años y por un plan de regreso a las aulas para el ciclo 2021.

El recorrido de la "Caravana por la vuelta a clases", comenzará en el Puente Colgante, continuará por Bulevar y seguirá por calle Urquiza; de ahí continuará por General López hasta llegar a la Plaza de Mayo, frente a la Casa de Gobierno, donde se realizará una sentada con distanciamiento social.

"Por la pandemia de Covid-19, se implantó una cuarentena que suspendió las clases en todos los niveles, aún cuando no había circulación de virus. La extensión de esta medida, razonable cuando sí hay circulación como en estos momentos, generó muchos problemas que afectan a miles de estudiantes, no sólo en lo académico, sino también, y fundamentalmente, en lo social y lo anímico", reza el comunicado que lanzó la agrupación Padres Organizados Santa Fe.

"Somos padres que nos fuimos sumando en un grupo y uniendo a raíz del sentimiento de ver a nuestros hijos transitando su último año de escuela muy tristes y angustiados", sostuvo en diálogo con UNO Santa Fe, Adelina Pizzarulli, integrante de Padres Organizados SF.

"No somos ni padres anti cuarentena, ni tampoco perseguimos intereses políticos; buscamos que las cosas se hagan de la mejor manera y creemos que es posible diagramar estrategias para volver a las aulas", continuó agregando Pizzrulli.

Por otro lado, desde la organización de padres, en el comunicado esbozado, indicaron que "el reclamo es por la vuelta a clases de manera segura y sostenida". "Entendemos que no se puede hacer masivamente en las actuales condiciones de crecimiento de contagios, pero sí pedimos que comiencen los preparativos que garanticen las actividades para el ciclo 2021, y además entendemos que es posible y necesario, evaluar y hacer el esfuerzo para posibilitar que los estudiantes de los 5tos y 6tos años de las escuelas secundarias puedan finalizar este ciclo de manera presencial, con la modalidad adecuada", sigue el mensaje.

Uno de los pedidos de los padres en el comunicado, es "declarar a la Educación como actividad esencial y establecer la Emergencia Educativa, a fin de trabajar las readecuaciones 2020/21 como un plan integral que permita recuperar los aprendizajes y contenidos no cumplidos en esta etapa de virtualidad".