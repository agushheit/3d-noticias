---
layout: Noticia con imagen
author: .
resumen: Shopping del Puerto
category: La Ciudad
title: Desde este miércoles reabre el patio de comidas del shopping del Puerto
  de Santa Fe
entradilla: A través de las redes sociales el shopping celebró la noticia, dio a
  conocer el protocolo y adelantó que se podrá disfrutar de un nuevo sector al
  aire libre.
date: 2020-11-19T12:50:44.376Z
thumbnail: https://assets.3dnoticias.com.ar/patio-comidas.jpg
---
El de los shoppings fue uno de los sectores más golpeados, producto de las restricciones a las actividades dispuestas por la cuarentena. Estuvieron muchos meses sin poder abrir y, cuando lo pudieron hacer, fue con ciertas disposiciones, como por ejemplo que los espacios de juegos y los patios de comidas no podían funcionar.

Hasta ayer martes 17 de noviembre los locales comerciales tenían sus puertas abiertas y los comedores o puestos de comidas podían solo funcionar con delivery. Desde este miércoles 18 esto cambia en la ciudad de Santa Fe y así lo anunció el Shopping La Ribera a través de sus redes sociales: se habilita el patio de comidas, con ciertos protocolos y modalidades de atención y con horarios y días predeterminados.

Según se informó estarán abiertos para que la gente pueda ir a disfrutar de domingo a jueves de 11 a 23, mientras que el viernes y el sábado será de 11 a 24. Será con una capacidad del 30% y además se le agregará una zona de galería al aire libre.

Para poder ingresar solo se podrá hacer a través de la puerta que da a la galería en el exterior, se hará control de temperatura, así como será obligatorio el uso de tapabocas y el distanciamiento social.