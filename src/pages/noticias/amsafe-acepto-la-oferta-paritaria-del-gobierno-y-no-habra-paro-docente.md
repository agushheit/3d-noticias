---
category: Agenda Ciudadana
date: 2021-11-04T06:15:58-03:00
thumbnail: https://assets.3dnoticias.com.ar/amsafe.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Amsafé aceptó la oferta paritaria del gobierno y no habrá paro docente
title: Amsafé aceptó la oferta paritaria del gobierno y no habrá paro docente
entradilla: Los docentes públicos aceptaron el incremento salarial del 17% en tres
  tramos que había ratificado el ejecutivo luego de los días de paro. Se definió por
  17.452 votos positivos contra 14.442 negativos.

---
Luego de la ratificación de la oferta salarial que deslizó el gobierno provincial a la Asociación del Magisterio de Santa Fe (Amsafé), los docentes públicos aceptaron la propuesta y no habrá mas días de paro. La determinación se conoció luego de la asamblea del gremio tras conocer el resultado de los votos de los representados, donde se impuso la moción para adherir al acuerdo por el aumento en tres tramos en los porcentajes ofrecidos por sobre rechazar la propuesta y volver a las calles.

El incremento de salario se aprobó por casi 3.000 votos en la asamblea en la que se presentaron las mociones de los docentes que emitieron su voto por el sí o por el no, con 17.452 votos positivos frente a 14.442 negativos, con un total de votos de 32.127. Desde Amsafé La Capital se acentuó la postura de aceptar la propuesta de aumento ofrecida, con 4.449 votos para adherir frente a 1.792 negativas, por lo que la seccional Rosario fue la que modificó mayoritariamente su postura para adherir a la oferta.

La propuesta salarial que se hará efectiva en cuestión es de un aumento del 10% en octubre (cobrado por docentes de Sadop y que cobrarán los docentes públicos por planilla complementaria), otro 5% en diciembre y un 2% restante a cobrar en enero. Con estos porcentajes, los maestros santafesinos percibirán un aumento anual del 52%.

Luego de la reunión del pasado lunes 1 de noviembre, lo que inclinó la balanza a favor de la aceptación del ofrecimiento fue la mejora de algunos ítems no salariales. Sobre esta base, propuso que la asamblea de Amsafé reconsidere todo y prometió a cambio, que si se aprobaba lo propuesto, el Gobierno se comprometía a liquidar el aumento a los docentes públicos –algo que en principio no había hecho– y a no descontar los días de paro.

Entre las mejoras no salariales que ofreció el gobierno a Amsafé estuvieron:

>> Compromiso del gobierno a llamar a una reunión en el Ministerio de Educación para acordar la recuperación de contenidos de los tres días de paros, suspendiendo los descuentos respectivos.

>> Sustanciar la convocatoria a concurso de nivel secundario el 1° de diciembre.

>> Iniciar el proceso con vistas a modificar el reglamento orgánico de los Institutos del Nivel Superior.

>> Aportar 2500 horas Cátedra en el período Noviembre/Febrero para el acompañamiento de trayectorias escolares de la Secundaria Básica, las cuales serán distribuidas en escuelas urbanas y cubiertas por escalafón de suplencias de preceptores.

>> Dar respuesta a los casos puntuales que se trajeron a la mesa del día viernes 29 de octubre referidos al Boleto Educativo Gratuito. Se incluye a partir de noviembre la extensión del beneficio a quienes se trasladen para los Sábados Activos, a los equipos de ESI y a los equipos Socioeducativos.

>> Aportar a la mejora de las prestaciones del IAPOS con los nuevos convenios para odontología en los departamentos del Sur que antes dependían de Rosario y que ahora contarán con acuerdos con prestadores locales. También dejar constancia que el incremento de los honorarios médicos reconocidos para el período Octubre/Marzo eliminará dificultades constatadas en los cupos.