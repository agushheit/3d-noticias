---
category: Agenda Ciudadana
date: 2021-04-26T06:00:38-03:00
thumbnail: https://assets.3dnoticias.com.ar/anses.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Bono Anses de $15.000: este lunes comienzan a pagar el beneficio para la
  AUH, AUE y monotributistas de las categorías más bajas'
title: 'Bono Anses de $15.000: este lunes comienzan a pagar el beneficio para la AUH,
  AUE y monotributistas de las categorías más bajas'
entradilla: El Gobierno aclaró que se trata de “un subsidio extraordinario” que se
  abonará “por única vez” en el marco de las nuevas restricciones ante la segunda
  ola de Covid-19.

---
La Administración Nacional de la Seguridad Social (Anses) comenzará a pagar este lunes el refuerzo de la Asignación Universal por Hijo (AUH) de 15.000 que para los beneficiarios cuyo DNI finalice en 0, de acuerdo al calendario de pago del organismo.

El presidente Alberto Fernández había anunció que dada la decisión de restringir la circulación en el Área Metropolitana de Buenos Aires (AMBA) y otras zonas del país ante la llegada de la segunda ola de coronavirus, se compensaría a beneficiarios de asignaciones sociales y a monotributistas de las categorías más bajas con un nuevo bono de $15.000 por los 15 días de cierre dispuestos hasta el 30 de abril.

El anuncio presidencial incluyó la interrupción de las clases presenciales por 15 días más la prohibición de circulación entre las 20 y las 6 de la mañana se limita al Área Metropolitana de Buenos Aires (AMBA).

La suma de 15.000 pesos se acreditará de forma automática en la cuenta en la que perciben la asignación sin realizar ningún trámite adicional y según terminación de documento, por lo que este lunes 26 de abril comienzan los finalizados en 0

En ese marco, los que recibirán el nuevo bono de $15.000 serán los titulares de AUH, los padres o tutores de los beneficiarios. Hay algo más de 2 millones de titulares de la asignación en todo el país, de los cuales alrededor de la mitad están en el AMBA.

El beneficio alcanza también a mujeres embarazadas de 12 semanas o más que estén desocupadas, sean trabajadoras informales con ingresos iguales o inferiores al Salario Mínimo, Vital y Móvil, monotributistas sociales, trabajadoras de servicio doméstico registradas, personas inscriptas en alguno de los programas Hacemos Futuro (Argentina Trabaja y Ellas Hacen), Manos a la Obra o programas del Ministerio de Trabajo.

Asimismo, y siempre limitado a las zonas geográficas afectadas por las nuevas restricciones a la circulación, el pago del bono de emergencia también será destinado a monotributistas de las categorías A y B exclusivamente.

Las personas registradas ante la AFIP como monotributistas que facturan al año hasta $208.739,25 (algo menos de $17.394 mensuales) como principal actividad están comprendidas en la categoría A del régimen simplificado. Mientras que las que facturan hasta $313.108,8 al año (casi $21.000 mensuales) son las que están en la categoría B del monotributo.

Esas dos categorías, que son las mismas que recibieron los tres desembolsos del Ingreso Familiar de Emergencia (IFE) del año pasado, son las únicas que van a recibir el apoyo monetario oficial.

Durante 2020, el subsidio de $10.000 se aplicó en tres oportunidades para evitar que muchas personas cayeran en la pobreza y la indigencia. El pago de ese bono se dio en un contexto de mayores restricciones que las actuales, sobre todo teniendo en cuenta que la economía estaba prácticamente cerrada y regía desde el 20 de marzo el aislamiento social preventivo y obligatorio (ASPO).

Las medidas de restricción se fueron flexibilizando durante 2020 y el IFE desapareció, mientras que el Programa de Asistencia de Emergencia al Trabajo y la Producción (ATP) diseñado para ayudar a las empresas y comercios a pagar el 50% de los salarios fue reemplazado por uno similar denominado de Recuperación Productiva (REPRO), que ya va por su segunda edición y tiene también como objetivo ayudar a las empresas a pagar el salario de sus trabajadores.

En ese marco, la determinación del Poder Ejecutivo de otorgar el bonode $15.000 a partir de este mes fue oficializada a través del Decreto 261/2021, que fue publicado el miércoles en el Boletín Oficial.

En el mismo, que lleva la firma del Presidente, del jefe de Gabinete, Santiago Cafiero, y del ministro de Trabajo, Claudio Moroni, se aclaró que se trata de “un subsidio extraordinario” que se abonará “por única vez” y no será susceptible de descuento alguno ni computable para ningún otro concepto.

“Esta medida tiene que ver con empezar por los más vulnerables”, indicó la titular de Anses, Fernanda Raverta.

Y agregó que la suma de 15.000 pesos se acreditará de forma automática en la cuenta en la que perciben la asignación sin realizar ningún trámite adicional y según terminación de documento, por lo que este lunes 26 de abril comienzan los finalizados en 0.

“Esta medida tiene que ver con empezar por los más vulnerables”, destacó Raverta y remarcó que “son casi un millón de personas”.

Según se informó oficialmente, en la Ciudad de Buenos Aires recibirán el refuerzo 97.000 familias, de las cuales 79.000 perciben AUH, 1.200 reciben la Asignación por Embarazo y 17.000 son monotributistas categorías A y B que cobran Asignaciones Familiares.

La inversión total por parte del organismo será de 1.460 millones de pesos.