---
category: Agenda Ciudadana
date: 2020-12-26T11:03:33Z
thumbnail: https://assets.3dnoticias.com.ar/2412-cierre-fronteras.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Infobae'
resumen: El Gobierno cerró el ingreso de turistas de países limítrofes
title: El Gobierno suspendió más destinos internacionales y cerró el ingreso de turistas
  de países limítrofes
entradilla: Rige desde el 25 de diciembre y se extenderá hasta el 8 de enero. El plan
  de vacunación comenzaría el próximo lunes

---
<span style="color: #EB0202;"> **Coronavirus** </span>**|** El presidente Alberto Fernández encabeza, en la Casa Rosada, una nueva reunión del Comité de Vacunación, encargado de diseñar la logística para distribuir y aplicar las vacunas contra el covid-19 a partir del próximo lunes. El encuentro también servirá para resolver el endurecimiento de los ingresos al país por vía terrestre y aérea, para evitar que una nueva cepa de coronavirus, descubierta en Gran Bretaña, ingrese al país. Previo al cónclave, **Casa Rosada informó que las restricciones de vuelo se extendieron a Italia, Dinamarca, Países Bajos y Australia**.

Del encuentro participan los ministros que integran el Comité de Vacunación: el jefe de Gabinete, Santiago Cafiero; los ministros de Salud, Ginés González García; de Defensa, Agustín Rossi; de Interior, Eduardo de Pedro; y la ministra de Seguridad, Sabina Frederic. Son los principales funcionarios que estarán a cargo de la coordinación del operativo de vacunación.

También están presentes los infectólogos Eduardo López y Pedro Cahn, que forman parte del comité de expertos que asesoran al Presidente por el covid-19. El Presidente está preocupado por el crecimiento de casos de coronavirus y el comportamiento social. Nota un desborde en los centros comerciales y en los bares. A eso se le suma la aparición de una nueva cepa, que en el Gobierno esperan controlar y evitar que ingrese.

Antes de la reunión, el Gobierno informó que pondrá en marcha a partir de las 0 horas del 25 de diciembre nuevos requisitos para el ingreso y egreso de argentinos y residentes, y de extranjeros, previamente autorizados por Migraciones, a través del aeropuerto internacional de Ezeiza o de San Fernando, establecidos como únicos ingresos a la Argentina.

**Los requisitos incluirán la presentación de un test de PCR negativo y la realización de una cuarentena obligatoria de 7 días, según una decisión administrativa de la Jefatura de Gabinete, que se extenderá hasta el 8 de enero**.

La decisión, basada en los informes técnicos de Migraciones, informará los pasos terrestres que se encontraban habilitados hasta el momento y permitirán sólo el ingreso de ciudadanos argentinos y residentes, también con el requisito de PCR negativo y realizar cuarentena obligatoria de 7 días.

De esta manera, **queda en suspenso la prueba piloto que autorizó el ingreso de turistas extranjeros provenientes de países limítrofes, tanto por en las terminales aéreas de Ezeiza y San Fernando, como por la portuaria de Buquebus**. En esta terminal del Puerto de Buenos Aires sólo se autorizará el ingreso de argentinos y residentes.

Migraciones también dispuso que los pasos fronterizos de San Sebastián e Integración Austral, ambos situados en Tierra del Fuego, Antártida e Islas del Atlántico Sur, queden exceptuados de la restricción de egresos e ingresos, para garantizar el tránsito con la República de Chile y la conexión con el resto del territorio argentino.

A la vez, a partir de las recomendaciones del Ministerio de Salud, **se dispuso suspender la llegada y salida de vuelos desde y hacia Italia, Dinamarca, Países Bajos y Australia, como así también mantener la restricción con Gran Bretaña**, a raíz de la situación epidemiológica que registran estos países tras la aparición de una nueva cepa de coronavirus.

**Los exceptuados de esta nueva medida son los diplomáticos, funcionarios y el personal esencial proveniente de los destinos habilitados, que deberán contar con autorización Migraciones y presentar test de PCR negativo**. También estará exceptuado el personal de transporte internacional y las tripulaciones, y los deportistas enmarcados en protocolos específicos para eventos deportivos, previa autorización de Migraciones.

Las restricciones al ingreso desde el Reino Unido se impusieron el domingo último a la noche, cuando se dejó aterrizar el último vuelo de British Airways al día siguiente. Ahora, se agregará un listado de países donde se haya detectado la variedad más contagiosa del virus, entre ellos Alemania, Francia, Holanda, Bélgica, Italia y Australia. Por ahora, España no será incluido, porque el análisis se realiza «país por país».

Luego de recibir el informe del grupo de epidemiólogos, que ayer volvió a reunirse por segunda vez en 48 horas, el Presidente tomó la decisión de ampliar la prohibición de vuelos provenientes del Reino Unido a otros países de la Unión Europea que confirmaron la aparición de una variante del COVID-19 que no lo hace más letal, pero sí más contagioso.