---
category: El Campo
date: 2021-06-23T09:16:01-03:00
thumbnail: https://assets.3dnoticias.com.ar/carne.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: LT10
resumen: 'Carne: una por una, las medidas anunciadas por el Gobierno'
title: 'Carne: una por una, las medidas anunciadas por el Gobierno'
entradilla: Además de la reapertura gradual de exportaciones, se anunció la puesta
  en marcha de un Plan Ganadero, la extensión de la oferta de cortes a precios accesibles
  y la prohibición de vender al exterior algunos cortes.

---
El Gobierno anunció este martes una serie de medidas vinculadas al sector cárnico, que incluyen la prohibición de vender al exterior algunos cortes, la reapertura gradual de las exportaciones para ciertos productos y la implementación de un Plan Ganadero.    

**Exportaciones**

\- Reapertura gradual: se podrá exportar hasta el 50% del volumen mensual promedio exportado en 2020. La medida rige hasta el 31 de agosto próximo.

\- Hasta el 31 de diciembre próximo no se podrá vender al exterior media res, cuartos con huesos y 7 cortes: asado, falda, matambre, tapa de asado, cuadrada, paleta y vacío. 

\- Se crea una Mesa de Coordinación Sectorial, encabezada por el Ministerio de Desarrollo Productivo. 

\-Se profundizarán los controles sobre el sector exportador.

\- Se aplicarán nuevas medidas para evitar posible subfacturación y desalentar las maniobras ilegales.

**Plan Ganadero**

En los próximos 30 días se pondrá en marcha el Plan Ganadero. Su elaboración está a cargo de los ministerios de Desarrollo Productivo y Agricultura, Ganadería y Pesca.

Los principales puntos de este programa serán calibrados con los sectores productivos de la cadena cárnica que participaron de la reunión de este martes en Casa Rosada, que estuvo encabezada por el presidente Alberto Fernández. 

El objetivo de mediano plazo es aumentar la producción de carnes, y pasar de las 3,2 millones de toneladas actuales a 5 millones de toneladas por año.

**Cortes a precios bajos**

Se anunció que las ofertas de cortes a precios accesibles estarán vigentes todos los días de la semana, y que se fortalecerá el Mercado Federal Ambulante. Ambas medidas rigen fundamentalmente en AMBA, pero son difíciles de observar en el interior del país. 

Por la primera de ellas, los valores de venta son hasta 45% más bajos que los actuales: 

\- Tira de asado: $359 x kg.

\- Vacío: $499 x kg.

\-  Matambre: $549 x kg.

\- Cuadrada / Bola de lomo: $515 x kg.

\- Tapa de asado - $429 x kg.

\- Carnaza - $379 x kg.

\- Falda - $229 x kg.

\- Roastbeef - $409 x kg.

\- Espinazo - $110 x kg (vigente en Precios Cuidados).

\- Carne picada - $265 x kg.

\- Paleta - $485 x kg.

En cuanto al Mercado Federal Ambulante, desde el Gobierno aseguraron que se ampliará su alcance, con más puntos de venta en el AMBA "y en distintas aglomeraciones urbanas del país".

Las carnicerías móviles del Mercado Federal Ambulante comercializan los siguientes cortes envasados al vacío:

\- Tira de asado - $349 x kg.

\- Carnaza - $369 x kg.

\- Cuadrada - $499 x kg.

\- Roastbeef - $399 x kg.