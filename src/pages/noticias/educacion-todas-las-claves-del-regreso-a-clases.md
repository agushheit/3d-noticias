---
category: Agenda Ciudadana
date: 2021-07-23T06:15:50-03:00
thumbnail: https://assets.3dnoticias.com.ar/XLAES.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: 'Educación: todas las claves del regreso a clases'
title: 'Educación: todas las claves del regreso a clases'
entradilla: El objetivo es recuperar el vínculo presencial, dentro del sistema bimodalidad,
  y la jornada escolar con horario completo.

---
La ministra de Educación, Adriana Cantero, dio precisiones sobre la vuelta a clases tras el receso invernal, junto al secretario de Educación, Víctor Debloc; la subsecretaría de Educación Inicial, Rosana Cencha; la subsecretaría de Educación Primaria, Nanci Alario; y el subsecretario de Educación Secundaria, Gregorio Vietto.

En la oportunidad, la titular de la cartera educativa indicó que "hemos transitado un tiempo de varias interrupciones dada la curva epidemiológica. Pero esos indicadores están mejorando; y nos permite planificar la vuelta de la presencialidad en todos los niveles y modalidades”, con el cumplimiento estricto de los protocolos y los cuidados, como el distanciamiento social, el uso de barbijos y la ventilación permanente de las aulas.

En primera instancia, la primaria tendrá tres bloques bloques horarios de 70 minutos y dos espacios de recreación, para ventilar aulas, de 15 minutos cada uno. En tanto, para secundaria serán tres bloques horarios de 70 minutos y uno de 35 minutos, alternados con recreos para descanso y ventilación.

En la circular, firmada por la ministra de Educación de Santa Fe, Adriana Cantero, se indica que seguirán las burbujas rotativas con alternancia semanal y que esta modalidad empezará a regir en agosto, dándole una posibilidad a los establecimientos educativos para prepararse durante la semana próxima y adecuarse a los nuevos horarios sugeridos desde el Ministerio.

"Los indicadores están mejorando al día de la fecha. Si bien seguimos con un nivel de riesgo alto en la mayoría de los departamentos de la provincia, estamos saliendo de la alarma epidemiológica en Santa Fe y en Rosario, lo que nos permite planificar la vuelta a la presencialidad en todos los niveles y modalidades", afirmó Cantero.

A ello sumó: "Nos proponemos más tiempo en la escuela para poder fortalecer los recorridos escolares y estamos analizando, en ese formato, la posibilidad de recuperar la jornada simple completa tal cual la conocimos antes de la pandemia. Se recupera, como mínimo, una hora cátedra más y en el nivel secundario dos horas cátedra".

Y explicó que "aquellas escuelas que tienen posibilidad, con recursos propios, de jornadas extendidas, van a extender una hora más en el cursado de los niños".

Respecto del nivel secundario, la funcionaria dijo que para 1º y 2º año harán hincapié en "fortalecer las tutorías donde los chicos han tenido menos posibilidades de constituirse en estudiantes secundarios". Por su parte, para 3º y 4º año del secundario piensan en jornadas denominadas "Sábados activos" para "fortalecer y enriquecer el proceso educativo de esos cursos".

"Para los terciarios, que vuelven a clases el 8 de agosto, también planeamos una presencialidad alternada, con cuidados", concluyó.

**"Sábados activos"**

Por otro lado, Cantero anunció que “estamos pensando para las secundarias orientadas los ‘Sábados Activos’, que como el programa ‘Verano Activo’, buscará enriquecer y fortalecer las experiencias educativas de los adolescentes de tercer y cuarto año”. Asimismo, en el último año de escuela secundaria, se implementará el Plan Egresar, para que las y los adolescentes terminen la trayectoria escolar obligatoria sin adeudar materias.

En este sentido, la ministra aclaró que "todo lo que esté por fuera de la jornada reglamentaria de los docentes, se realizará con recursos extras, excepcionales, con fondos nacionales y provinciales".

Cantero indicó que, "en el mes de agosto, y conforme al acuerdo federal que hemos firmado, se van a relevar aquellos estudiantes que han tenido una trayectoria escolar intermitente para poder direccionar las políticas de asistencia educativa. Estamos trabajando con una mirada fuerte puesta en esas trayectorias escolares volviendo a la presencialidad cuidada con alternancia y en aquellos lugares donde se pueda con presencialidad plena, siempre con un monitoreo semanal".