---
category: Agenda Ciudadana
date: 2021-09-19T06:15:13-03:00
thumbnail: https://assets.3dnoticias.com.ar/QUEMAS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Santa Fe, entre las cuatro provincias que registran focos activos de incendios
  forestales
title: Santa Fe, entre las cuatro provincias que registran focos activos de incendios
  forestales
entradilla: Así lo informó el reporte del Servicio Nacional de Manejo del Fuego realizado
  este sábado. En el mismo escenario se encuentran Entre Ríos, San Luis y Córdoba.

---
Las provincias de San Luis, Santa Fe, Córdoba y Entre Ríos registraban este sábado focos activos de incendios forestales, según el reporte diario realizado por el Servicio Nacional de Manejo del Fuego (SNMF), que además aseguró que las llamas que afectaban a Mendoza se encuentran controladas.

Tras el monitoreo satelital que se realiza todos los días, el organismo en su reporte diario informó que se detectó un nuevo foco activo en la localidad de Pringles, en San Luis.

Este foco activo se suma a los existentes en la capital de Santa Fe; en la localidad cordobesa de Colón; y los de Victoria, provincia de Entre Ríos.

Asimismo, se informó que, en Catamarca, los incendios que había en la localidad de Tinogasta fueron "extinguidos", mientras que en Capayán están "controlados".

Con respecto a la provincia de Mendoza, el SNMF confirmó que hay un foco "controlado" en la localidad de Malargüe.

Finalmente, el organismo precisó que fueron extinguidas las llamas que afectan las localidades de San Alberto y Calamuchita, en Córdoba.

El SNMF otorga la categoría de "foco activo" cuando "el fuego se propaga libremente y puede crecer y los medios trabajan para extinguirlo", en tanto las llamas que afectaban a la localidad de Tinogasta ya se encuentran controladas.

Un incendio controlado es cuando "la línea de control ha quedado establecida definitivamente, anclada y asegurada" y "se considera que no hay posibilidad de rebrotes. Esta situación tendría que ser irreversible", se explicó.

El SNMF recordó nuevamente que el 95% de los incendios forestales "son producidos por intervenciones humanas".

"Entre las primeras causas se encuentra el uso del fuego para la preparación de áreas de pastoreo. Otras causas que prevalecen son el abandono de tierras, las fogatas y las colillas de cigarrillos mal apagadas", añadió el organismo.