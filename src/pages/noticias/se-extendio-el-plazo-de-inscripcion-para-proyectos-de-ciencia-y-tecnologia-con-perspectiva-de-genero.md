---
category: Estado Real
date: 2020-12-23T09:48:48Z
thumbnail: https://assets.3dnoticias.com.ar/2312genero.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Se extendió el plazo de inscripción para proyectos de Ciencia y Tecnología
  con perspectiva de género
title: Se extendió el plazo de inscripción para proyectos de Ciencia y Tecnología
  con perspectiva de género
entradilla: Habrá tiempo hasta el 29 de diciembre para la presentación de los proyectos
  en santafe.gob.ar

---
El Gobierno de Santa Fe, a través de la Secretaría de Estado de Igualdad y Género junto al Ministerio de Producción, Ciencia y Tecnología, informó que se extendió el plazo de presentación de proyectos hasta el 29 de diciembre. El programa «Ciencia Tecnología e Innovación (CTI) con perspectiva de género», está destinado a grupos de investigación e instituciones públicas o privadas sin fines de lucro del sistema ciencia, tecnología e innovación, radicadas en la provincia de Santa Fe.

Dicho programa se desarrolla con el objetivo de promover la equidad de género en las instituciones, organismos y empresas ligadas a la ciencia, la tecnología y la innovación, en pos de aumentar la participación de las mujeres y diversidades en la dirección de proyectos vinculados a la Ciencia y Tecnología.

«Este programa que impulsamos en conjunto con el Ministerio de Producción, Ciencia y Tecnología, específicamente con la Secretaría de Ciencia, Tecnología e Innovación, tiene que ver con la convicción que sostenemos, desde el Gobierno de Santa Fe y por la decisión política del gobernador Omar Perotti, de que la ciencia y la tecnología deben tener perspectiva de género, que las mujeres y las diversidades sexuales deben ocupar espacios de creación, investigación y decisión en estos ámbitos», detalló la secretaria de Estado de Igualdad y Género, Celia Arena.

«El espíritu de este programa entonces tiene que ver con ello, con brindar las herramientas necesarias para que mujeres y personas de la diversidad sexual encabecen proyectos de ciencia y tecnología», agregó.

En tanto, la secretaria de Ciencia, Tecnología e Innovación, Marina Baima, indicó: «Ampliamos el tiempo de la convocatoria porque queremos que más equipos liderados por mujeres y disidencias lleguen a presentar sus proyectos. Asumimos este desafío porque sabemos que existen políticas de género, pero todavía queda por construir».

Además, explicó: «Queremos conocer cómo potenciamos proyectos que ya existen o están emergiendo y desde el Estado creemos que hay que crear las condiciones para que llegue la ciencia y la tecnología al territorio, con perspectiva, y para ello tenemos que conocer la realidad».

En este sentido, se busca estrechar la vinculación entre la innovación y los asuntos de género a través del diseño de nuevas investigaciones y emprendimientos; así como mejorar la representación de las mujeres y diversidades en la conformación de los equipos de trabajo, en pos de la transformación de los sitios de la CTI en espacios más habitables para las identidades femeninas.

## **Convocatoria para la presentación de proyectos**

Hasta el 29 de diciembre se encuentra abierta la inscripción a la convocatoria para la presentación de proyectos de Ciencia y Tecnología con perspectiva de género dentro de seis ejes:

1- Producción, trabajo y desigualdades;

2- Ambiente, cambio climático, sustentabilidad, energías renovables;

3- Educación, conectividad y acceso a tecnologías;

4- Salud y políticas públicas;

5- Desarrollo de infraestructura local;

6- Subjetividades emergentes, representaciones sociales, visibilización.

**Se destinarán $ 3.000.000 a instituciones santafesinas, hasta $ 150.000 por proyecto ganador.**

Los equipos de trabajo deberán respetar la paridad de género, o sea, estar compuestos por al menos un 50 % de mujeres. Se valorará especialmente aquellos que incluyan población travesti trans entre sus integrantes, y los que incorporen jóvenes investigadores/as. El proyecto de investigación deberá ser integrado por no menos de ocho (8) personas, una co-dirección y una dirección.

La presentación deberá realizarse a través de la carga online de los proyectos.

### **Requerimientos, bases y condiciones en:**

[https://www.santafe.gob.ar](https://www.santafe.gob.ar/index.php/web/content/view/full/236886/(subtema)/236715)

**+ Info y contacto:** [cienciaygenero@santafe.gov.ar](mailto:cienciaygenero@santafe.gov.ar)

***