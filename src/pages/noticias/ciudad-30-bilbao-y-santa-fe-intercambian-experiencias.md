---
category: La Ciudad
date: 2021-01-29T09:55:40Z
thumbnail: https://assets.3dnoticias.com.ar/bilbao.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad Santa Fe
resumen: 'Ciudad 30: Bilbao y Santa Fe intercambian experiencias'
title: 'Ciudad 30: Bilbao y Santa Fe intercambian experiencias'
entradilla: Funcionarios de la Municipalidad mantuvieron un encuentro virtual con
  integrantes del ayuntamiento de la ciudad española, referente a nivel mundial en
  movilidad sustentable.

---
En el marco de la implementación Ciudad 30, esta mañana se realizó una videoconferencia entre autoridades de la Municipalidad y Alfonso Gil, concejal de la Movilidad de Bilbao, localidad española de referencia en la Movilidad Sustentable a nivel mundial. Vale recordar que Ciudad 30 es una iniciativa impulsada por el Ejecutivo municipal y aprobada por el Concejo, que busca generar una mejor convivencia en el tránsito del macro y micro centro de la ciudad. La iniciativa se replica en ciudades de todo el mundo.  

Bilbao es Ciudad 30 desde el 2018; ese período de tiempo le permitió constatar los beneficios que trae aparejado para los ciudadanos reducir la utilización de los vehículos particulares y apelar a una movilidad activa. “El coche es poderoso, no quiere perder terreno. El coche conquistó nuestras ciudades. Lo que hay que entender es que las medidas de Ciudad 30 no son contra los coches, lo que tenemos que hacer es contextualizar su uso”, indicó Alfonso Gil, y advirtió: “Prepárense porque el coche reactiva a mucha gente, pero fíjense, lo que fue un escándalo en un principio, ahora es un conjunto de ciudadanos y ciudadanas convencidas de que hoy en esta ciudad se vive mejor”.

En consonancia con las declaraciones de Gil, la ONG Greenpeace elabora todos los años el Ranking de la Movilidad Urbana. En el informe del 2019 sostiene que “la capital vizcaína es líder en movilidad sostenible gracias a un modelo que coloca la movilidad peatonal en el centro de la vida urbana. Tras una profunda transformación urbana, en las últimas dos décadas, centrada en la consolidación y la proximidad, actualmente un 64% de los desplazamientos en la Villa de Bilbao se realizan a pie, mientras que el uso del vehículo privado es solo del 11% y baja año tras año”.

**Seguir con el ejemplo**

Los funcionarios de la Municipalidad de Santa Fe que participaron de la videoconferencia fueron Javier Mendiondo, secretario de Desarrollo Urbano, y Virginia Coudannes, secretaria de Control y Convivencia Ciudadana. “Desde hace un tiempo tenemos comunicaciones con el área de Movilidad del Ayuntamiento de Bilbao; específicamente hoy tuvimos una reunión virtual con el referente del área, Alfonso Gil, para generar un intercambio acerca de la implementación de Ciudad 30 en la capital provincial”, destacó Coudannes.

En ese sentido, la funcionaria destacó que “Bilbao es la primera ciudad en el mundo con más de 300.000 habitantes que implementó el límite de velocidad a 30 kilómetros por hora a toda la ciudad, por eso fue importante conversar acerca de los desafío, las diferentes partes de implementación que ellos utilizaron y de ahí obtener aprendizajes al respecto”.

Por su parte, Mendiondo recordó que “no hay que perder de vista el objetivo de esta iniciativa, que es ir hacia ciudades más saludables, más sustentables, y más cuidadoras de sus habitantes. Es importante entender que Ciudad 30 no pasa solamente por reducir la velocidad, sino que detrás de estas medidas, estamos en favor de una ciudad para las personas, entender que la calle es parte de un sistema de espacios públicos para todas y todos”.

En tanto, Coudannes indicó que además de avanzar en la demarcación del perímetro de Ciudad 30, “se avanza en lo pedagógico, el control desde el cuidado y la prevención. Además seguimos trabajando con los diferentes actores como los comerciantes e integrantes de asociaciones de Seguridad Vial”.

**Alcance de la iniciativa**

Vale recordar, que en agosto del 2020 el Concejo aprobó la planificación y gestión a largo plazo de escenarios urbanos sostenibles, inclusivos y seguros. Específicamente, se determinó la máxima velocidad de 30 kilómetros por hora entre las calles Urquiza al oeste, Suipacha al norte, General López al sur y Rivadavia (desde Suipacha hasta Mendoza) y 27 de Febrero (desde Mendoza hasta General López) al este, con exclusión de las calles que delimitan la zona, y con excepción de las calles General López al Norte, San Jerónimo al este, 3 de Febrero al sur, en los tramos que rodean la plaza 25 de Mayo, y calle San Martín en el tramo comprendido entre las calles 3 de Febrero y Juan de Garay.

Además propone que los edificios públicos, dentro del área comprendida en “Ciudad 30” deberán garantizar la disponibilidad de bicicleteros públicos en espacios cubiertos o semicubiertos, sea en los propios inmuebles o en espacios aledaños.

Por otra parte, las playas de estacionamiento deberán admitir el ingreso de motovehículos, ciclomotores y bicicletas, reservando para ello como mínimo un 15% del total de la superficie destinada al estacionamiento de automóviles, considerando un 5% para motovehículos y ciclomotores; y un 10% para bicicletas medido en superficie del espacio de estacionamiento. Para esto, se cobrará una tarifa cuyo monto no puede ser superior al 33% para motos y ciclomotores; y al 10% para bicicletas, de la tarifa vigente sobre los vehículos automotores.