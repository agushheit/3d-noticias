---
category: Agenda Ciudadana
date: 2022-11-16T07:37:10-03:00
thumbnail: https://assets.3dnoticias.com.ar/jaton-y-salmeras.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: Santa Fe Capital festejó sus 449 años con una fiesta multitudinaria
title: Santa Fe Capital festejó sus 449 años con una fiesta multitudinaria
entradilla: En la Costanera Oeste y ante más de 120 mil personas, Los Palmeras y artistas
  invitados brindaron un recital de nivel internacional. Quejas en redes sociales
  por el sonido.

---
Este martes por la tarde, la Costanera Oeste se vistió de fiesta. Entre el Faro y el Puente Colgante, la Municipalidad desplegó múltiples propuestas para que vecinos y vecinas de la capital provincial celebren el aniversario 449 de la ciudad, con actividades para toda la familia, dispuestas sobre avenida 7 Jefes. Los festejos contaron con la presencia del gobernador, Omar Perotti.

El comienzo de este año de celebraciones fue a pura música, con un recital de Los Palmeras y amigos, enmarcado también en los 50 años que cumple el tradicional grupo de cumbia santafesina. Así, también pasaron por el escenario Soledad, Jorge Rojas, Karina La Princesita, Axel y La Delio Valdez, entre otros. Antes, los santafesinos Josela y La Delfino Flow & la Puro Flow Band tuvieron a su cargo los primeros acordes.

Según se informó, más de 120 mil personas participaron del evento sumándose a las actividades educativas, deportivas y recreativas propuestas por la Municipalidad. Las mismas se extendieron a lo largo de toda la zona y estuvieron acompañadas por un área gastronómica y paseos de ferias.

En redes sociales,  circularon videos y comentarios acerca del deficiente sistema de sonido del evento. 

![](https://assets.3dnoticias.com.ar/Captura de pantalla 2022-11-16 074233.png)

![](https://assets.3dnoticias.com.ar/Captura de pantalla 2022-11-16 074352.png)