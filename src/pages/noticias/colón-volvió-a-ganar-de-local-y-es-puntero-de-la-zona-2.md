---
layout: Noticia con imagen
author: "  Por Clara Blanco"
resumen: "Colón: puntero de la zona 2"
category: Deportes
title: Colón volvió a ganar de local y es puntero de la zona 2
entradilla: El Sabalero venció por 2 a 0 a Central Córdoba con goles de
  Christian Bernardi y Pulga Rodríguez. No ganaba en su cancha desde el 25 de
  noviembre de 2019, cuando le ganó a Estudiantes por 3 a 2.
date: 2020-11-17T17:22:22.480Z
thumbnail: https://assets.3dnoticias.com.ar/col%C3%B3n-central-cordoba.jpg
---
Colón cerró la tercera fecha de la Copa Liga Profesional con un triunfo sobre Central Córdoba de Santiago del Estero. El Sabalero se impuso por 2 a 0 en su cancha con goles de Christian Bernardi y Luis Miguel Rodríguez. Hacía 262, es decir casi un año, que el equipo de Eduardo Domínguez no jugaba oficialmente de local.

Habían pasado apenas 6 minutos del inicio del partido cuando Colón concretó el primer tanto. Un cabezazo de Bernardi abría el marcador y ponía en ventaja al conjunto local, quien había tenido más intervención en el juego ante un desarmado Ferroviario. Mientras que a los 36 del complemento el que definió el partido fue el Pulga Rodríguez con un exquisito remate que dejó sin chances a los santiagueños.

De esta manera, Colón sumó 7 puntos y es el puntero de la zona 2 seguido por Independiente que tiene 5 unidades. El próximo rival del Sabalero será Defensa y Justicia, con el cual se enfrentará el sábado a las 17:10 nuevamente en el Brigadier Estanislao López