---
category: Agenda Ciudadana
date: 2021-02-13T07:14:59-03:00
thumbnail: https://assets.3dnoticias.com.ar/vueltaaclases.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: El Consejo Federal de Educación ratificó la vuelta a clases presenciales
  "cuidadas y seguras"
title: El Consejo Federal de Educación ratificó la vuelta a clases presenciales "cuidadas
  y seguras"
entradilla: 'Los ministros del área confirmaron la vuelta al aula en todo el país,
  según la situación epidemiológica de cada región y bajo condiciones de seguridad
  sanitaria. '

---
El Consejo Federal de Educación ratificó el retorno a las clases presenciales en todo el país de acuerdo a la situación epidemiológica de cada región y con condiciones de seguridad sanitaria que garanticen el cuidado de la salud, bajo la premisa de que "sin salud no hay presente y sin educación no hay futuro", según afirmó el presidente Alberto Fernández al cerrar el encuentro.

La 102° Asamblea del Consejo Federal de Educación definió trabajar en "el proceso de retorno a las clases presenciales en todo el país según la situación epidemiológica de cada región y bajo condiciones de seguridad sanitaria que garanticen el cuidado de la salud de la comunidad educativa y la implementación de los protocolos aprobados", se informó oficialmente.

_"Volver a clase esta vez no tiene las características de un día de marzo cualquiera: es volver llenos de cuidados, preservando la distancia y la cercanía de docentes y alumnos, y priorizando a la salud de los docentes, a quienes debemos ayudar en su vacunación lo antes posible"_, afirmó el Presidente, acompañado en el encuentro por el ministro de Educación, Nicolás Trotta.

En una conferencia de prensa tras el encuentro, Trotta dijo que el Consejo Federal de Educación reafirmó el "compromiso con la presencialidad cuidada y segura" en las escuelas ante el inicio del ciclo lectivo 2021, a partir de la semana próxima en distintas jurisdicciones de todo el país, y subrayó que "va a ser un proceso en construcción la vuelta a la presencialidad cuidada".

Asimismo, anunció la creación de un observatorio para el seguimiento y evaluación del regreso a clases presenciales, y dijo que "se reafirmó un esquema de presencialidad, no presencialidad y una combinación de ambos para poder cumplir los 180 días de clases".

Según Trotta, "están dadas las condiciones, con la particularidad de cada región, para tener presencialidad en las escuelas", aseguró el funcionario tras la reunión integrada por los ministros de Educación de todas las jurisdicciones, dirigentes gremiales y legisladores que se desarrolló en la residencia de Olivos.

En el encuentro, el CFE ratificó que las clases iniciarán en todos los niveles obligatorios y modalidades en las fechas fijadas en el calendario escolar de cada jurisdicción "bajo cualquiera de las formas de escolarización: presencial, no presencial o combinada".

Sin embargo "se planteó la necesidad de priorizar la actividad escolar presencial por sobre otras formas en todos los niveles y modalidades de la educación obligatoria cuando un distrito cuente con la situación sanitaria de distanciamiento social, preventivo y obligatorio", según se informó oficialmente.

En esa misma línea, se establecieron prioridades entre los grupos de estudiantes para el regreso presencial a las aulas, como, por ejemplo, la sala de 5 años del nivel inicial, y el primer y el último año del nivel primario y del nivel secundario en todas sus modalidades y orientaciones.

Participaron en la conferencia de prensa la ministra de Educación porteña, Soledad Acuña; la directora general de Cultura y Educación bonaerense, María Agustina Vila; el ministro de Educación de Córdoba, Walter Grahovac; el director general de Escuelas de Mendoza, José Manuel Thomas, y la ministra de Educación de Santiago del Estero, Mariela Nassif.

En la reunión del Consejo, el Presidente también sostuvo que "la riqueza de las sociedades hoy no se mide por las riquezas naturales sino por la inteligencia que se logra desarrollar, y una sociedad que retrasa su calidad educativa en estos tiempos se va quedando al margen del mundo".

"_Una sociedad que desarrolla la inteligencia a través de la educación es una sociedad que tiene una posibilidad de avanzar a pasos agigantados, y la educación de nuestros chicos es el futuro de nuestra patria_", completó el mandatario.

También estarán primeros en la lista las poblaciones escolares que no hayan podido mantener su continuidad pedagógica en 2020 y alumnos con discapacidad que no forman parte de los grupos de riesgo.

En cuanto al personal docente y no docente, serán dispensados de asistir a los establecimientos de manera presencial quienes formen parte del grupo de riesgo hasta tanto no estén vacunados.

Además de los responsables educativos de las 24 jurisdicciones, participaron en la reunión la titular del Consejo Federal de Educación, Marisa Díaz; Sonia Alesso y Roberto Baradel (CTERA), Fabian Felman (Confederación de Educadores Argentinos), Sergio Romero (UDA), Jorge Kalinger (SADOP), Hugo Yasky (Comisión de Educación de Diputados), Rodolfo Tecchi (Consejo Interuniversitario Nacional), Luisa Brumana (UNICEF Argentina) y Roberto Valent (UNESCO Argentina).