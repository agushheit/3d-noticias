---
category: Agenda Ciudadana
date: 2021-09-24T06:15:10-03:00
thumbnail: https://assets.3dnoticias.com.ar/ELORTONDO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Femicidio en Elortondo: mujeres policías le reclaman al Senado provincial
  que actúe'
title: 'Femicidio en Elortondo: mujeres policías le reclaman al Senado provincial
  que actúe'
entradilla: " Exigen que los legisladores traten el proyecto para crear un espacio
  de denuncias a policías por fuera del Ministerio de Seguridad."

---
Claudia González fue asesinada este sábado por un policía de Seguridad Vial, con la complicidad de dos oficiales que lo acompañaron.

A través de un comunicado, mujeres policías de la provincia expresan su repudio al femicidio cometido por tres policías de Seguridad Vial este sábado y solicitan la creación de un espacio para denunciar la violencia machista dentro de la fuerza policial por fuera del Ministerio de Seguridad.

En el comunicado, la red de mujeres policías expresa: "Una vez más nos encontramos alzando nuestras voces, una vez más la violencia extrema nos azota, nos quita una congénere, una futura colega, una mujer que decidió, y "el macho" violento le dijo que no tenía derecho a ser. Un violento que se viste de uniforme cotidianamente, porque sabemos que no sólo están con uniforme, sino que tienen miles de caras e investiduras. Las mujeres trabajadoras de la seguridad pública, somos antes que nada mujeres, y elegimos siempre el diálogo, el consenso, usamos las vías legítimas para el reclamo de nuestros derechos, siempre buscando canales, expresando muestras vivencias, tratando de colaborar y contenernos unas a otras, en una soledad que asusta, con realidades que nos atraviesan y nos recuerdan todo el tiempo que nada alcanza, que nos siguen matando todos los días".

"Las estadísticas nos dicen que, de cada 5 femicidios, un macho femicida es un integrante de las fuerzas de seguridad. Es una cifra alarmante, que nos emiten alertas todo el tiempo. Las mujeres de la red de policías les queremos decir que estos machos femicidas son nuestros "colegas", nuestros "compañeros" de trabajo, con quienes debemos compartir nuestros días. Nos preguntamos hasta dónde llega el machismo, hasta donde llega la "dueñidad" con una mujer. "La maté porque era mía" dice Galeano en sus escritos, haciendo referencia a la violencia machista, y ayer una vez más se nos estrujó el corazón, nos invadió la angustia, y la bronca e impotencia fueron protagonistas. Nos matan a todas, algunas de un disparo con toda la impunidad de la misoginia uniformada y legitimada, pero a otras nos matan lentamente, hasta llegar al punto de no creernos, de tratarnos de locas, de justificar a los violentos de siempre, ningunear el testimonio de las víctimas, porque ellos son los señores, ellos son la investidura que exhiben y que aplauden, y nosotras las histéricas, las mentirosas, las que queremos "joderles la carrera"", sostienen.

Cabe recordar que esta Red de mujeres policías presentó proyecto de ley del Centro integral con perspectiva de género (CIPGEN) en la Legislatura santafesina. La idea fue tomada por los Ministerios de Seguridad y de Igualdad y Género y se formó un espacio, pero desde la Red sostienen que es necesario que la cartera de Seguridad no tenga intervención. "Son parte los que ejercen la violencia que se denuncia", señalan. Al proyecto le queda transitar por una sola comisión del Senado.

"La violencia machista está naturalizada, es corporativa, se esconde en cofradías, se disfraza de buena gente, y se sostiene desde el momento que no se cuestiona el sistema ni la estructura desigual histórica. Nos preguntamos porque uno de cada cinco femicidas forman parte de las fuerzas de seguridad, que pasa con esas instituciones donde éstos machos se sienten tan a gusto, dónde se desarrollan tan fácilmente, y suman cargos y jerarquías que acompañan a la impunidad de ejercer ese poderío desigual. Desde que nos encontramos como red contamos lo que nos pasa como mujeres, compartimos los problemas, y propusimos posibles soluciones", explican.

Y concluyen: "En cada reunión que participamos hemos transmitido situaciones muy duras que han generado silencios eternos, hemos terminado todas llorando, pero juntas, y eso lo valoramos, también hemos tenido pequeños logros que en un contexto como el de hoy no nos alcanzan, necesitamos otras intervenciones. Todos los días son de lucha e intentamos seguir adelante. Sabemos muy bien que se hace lo que se puede con lo que hay, que antes no existían muchos espacios que hoy sí, pero insistimos, no alcanza. Desde la red de mujeres policías solo podemos tratar de proponer para que se gestione y que otros están obligados a concretar. Nuestra lucha por erradicar todo tipo de violencia y discriminación continúa. Necesitamos la aprobación del CIPGEN. Necesitamos que se nos tenga en cuenta por qué solo nosotras sabemos lo que pasamos todos los días, y acá estamos para ser parte de una solución".