---
category: La Ciudad
date: 2021-10-17T06:15:12-03:00
thumbnail: https://assets.3dnoticias.com.ar/STAFE.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: '"Santa Fe va a tener una inversión en obras públicas de las más importantes
  de los últimos años"'
title: '"Santa Fe va a tener una inversión en obras públicas de las más importantes
  de los últimos años"'
entradilla: 'El ministro de Obras Públicas de la Nación informó que este año será
  un 200 por ciento superior a la de 2019. '

---
El ministro de Obras Públicas de la Nación, Gabriel Katopodis, destacó el volumen de obras públicas que el gobierno nacional está ejecutando o en proceso de licitación en Santa Fe, un total de 328 en toda la provincia, fue cauto a la hora de hablar sobre el pedido de un nuevo puente que una las ciudades de Santa Fe y Santo Tomé, que dependerá de su inclusión en el presupuesto nacional, y se comprometió a atender las muchas demandas por más obras que hay en la provincia en la medida de las posibilidades económicas.

 "Hay muchas demandas de la provincia. Ampliar carriles en la ruta 9, convertir en autovía la 11. Cuando asumí en la provincia estaban todas las rutas paradas, todas las empresas concursadas y el Ministerio de Obras Públicas debía millones de pesos. Pudimos pagar las deudas y reactivar las obras, por lo cual tenemos mucha confianza en que vamos a poder seguir haciendo muchas más", dijo en una entrevista con El Litoral.

 Katopodis estuvo este sábado en Santa Fe donde anunció obras por 10 mil millones de pesos que financiará su ministerio para mejorar la red de accesos viales a la ciudad de Santa Fe entre la fuente de la Cordialidad en la ruta 168, avenida Alem y la ruta 11 desde la salida a Santo Tomé hasta la localidad de Candioti; e inauguró un nuevo tramo de la autovía de la ruta 34 entre Susana y Rafaela.

 El ministro de Obras Públicas, en su paso por la provincia, enumeró alguna de las obras que desde su cartera están haciéndose o en proceso de licitación como la ruta 33 en el sur santafesino, la licitación de 19 centros de desarrollo infantil, desagües cloacales en Villa Constitución, un acueducto en San Lorenzo muy cerca de terminarse y obras de pavimento, cordón cuneta y equipamiento en casi todos los municipios, un total de 168 proyectos por 2800 millones de pesos.

 La ampliación de la planta potabilizadora de Santa Fe, a lo que se había comprometido en mayo con el intendente Emilio Jatón, es el otro anunció importante que realizó. Se licitará el martes, insumirá una inversión de 4.420 millones de pesos y beneficiará a 49.000 personas ya que aumentará la producción de agua potable para atender la demanda insatisfecha en barrios y el crecimiento de la población a 25 años. Para tener una magnitud de la obra que se realizará, en la actualidad la planta brinda a la red de distribución 8.250 m3/hora y la ampliación sumará 6.250 m3/hora, un 75% más.

 En paralelo, el ministerio de Obras Públicas de la Nación viene financiando pequeños proyectos destinados a reforzar las estaciones de bombeo de agua afectadas por la bajante de los ríos en la región, que trajeron dificultades de abastecimiento a la actual planta potabilizadora.

 -Desde hace mucho tiempo, desde la provincia se viene pidiendo la construcción de un nuevo puente que una las ciudades de Santa Fe y Santo Tomé. ¿Escuchó este reclamo?

 -Voy a ser claro. Lo que hay es un proyecto ejecutivo terminado y seguramente en la negociación del presupuesto y en la previsión de como vengan las partidas presupuestarias podremos ir dándole más cuerpo a eso. Preferimos ser muy cautos porque es un proyecto presupuestario muy importante para lo cual tenemos que ver como lo podemos calzar con alguna fuente de financiación porque de lo contrario va a ser difícil.

 -El año pasado lo entrevisté y me dijo que en 2021 iban a duplicar los montos de inversión en obra pública en la provincia. ¿Tiene alguna previsión para el año que viene?

 -En la medida que podamos mantener estos niveles de inversión, que son absolutamente extraordinarios de la provincia, vamos a estar satisfechos. La gente tiene que saber que la inversión en 2021 será aproximadamente un 200 por ciento más que la que hizo el gobierno nacional en 2019. Quiere decir que estamos superando por mucho lo que el gobierno anterior tenía para esta provincia. Todavía no tenemos el presupuesto del año que viene por lo cual tenemos que ser un poco cautos con las previsiones pero no hay duda que Santa Fe con todas estas obras que estoy mencionando, como la de la circunvalación que anunciamos este sábado, va a tener una inversión de las más importantes de los últimos años.

 - ¿Va a tener continuidad el programa Argentina Hace?

 -La va a tener. Lo que hacemos en este programa es, a medida que los municipios van presentando los proyectos estos se aprueban, firmamos los convenios, los municipios licitan y le vamos transfiriendo los recursos. Esto viene funcionando con una serie de obras desde agua a pavimento y equipamiento. Ese programa está llegando a 1800 municipios que incluye a casi el 90% de la población.

 -En el acto, los funcionarios de la provincia y el intendente de Santa Fe destacaron la buena relación que tienen que usted y la predisposición de los funcionarios del ministerio. ¿Cuál es su opinión sobre ello?

 -Es una característica del gobierno nacional y de nuestro ministerio trabajar con todos los gobernadores e intendentes sin diferenciar por color político. El gobernador en ese sentido me manifestó desde el primer día la necesidad de trabajar con todos los intendentes, con el de Santa Fe, con el de Rosario. Así que fue una indicación del gobernador trabajar con todos, es una indicación del gobernador fijar estas prioridades. Las de hoy (por ayer) son obras que ya no explican el momento de la pandemia porque quedó atrás. Nos tocó construir salud pública, ahora de lo que se trata es construir obras para el desarrollo. El gobernador tiene claro cual es el desarrollo que necesita Santa Fe y en función de ello la necesidad de estas obras.

 -¿Va a ser usted el ministro que va a inaugurar la obra total de la ruta 34?

 -Seguramente en abril vamos a terminar con la variante de Rafaela, vamos a incorporar los 14 kilómetros que de alguna manera fueron sacados del proyecto y colocados en el esquema de PPP y quedaron claramente inactivos. Vamos a reactivar estos 14 kilómetros para que toda la traza de Sunchales a Rafaela quede completa y podamos garantizar a todos los productores de la zona una de las autovías más importantes del país.

 **Obras en Santa Fe por más de 76 mil millones de pesos**

 El ministerio de Obras Públicas de la Nación tiene compromisos de inversión en la provincia de Santa Fe por un total de 76.040 millones de pesos distribuidos de la siguiente manera: $ 1.301,9 millones en obras finalizadas, $ 35.048 millones en obras en ejecución y $ 39.689 millones en proyectos y obras a licitar.

 En total en la provincia de Santa Fe hay 107 obras en ejecución, 202 proyectos a licitar y en licitación, y 19 obras finalizadas.

 El monto total pagado por obras públicas en la provincia es de $ 7.282 millones en 2020 y el presupuesto aprobado para este año es de $ 12.678.

 Entre las obras más importantes que se hicieron de nuestra región están el Sistema de Provisión de Agua Potable en Recreo ($ 52,4 millones), Reacondicionamiento de Canales Interceptores Norte. Sur y Cierre y Construcción de Defensa de la localidad de Colonia Margarita, Dpto Castellanos ($ 26,4 millones), Reacondicionamiento del Canal Villa Cululú y Cañada Sunchales, Municipio de Colonia Aldao, Dto. Castellanos ($ 671 millones) 

 Entre las que se están ejecutando, en algunos casos con muy tiempo por delante para ser terminadas, están las obras incluidas en el Programa Federal de Saneamiento (PROFESA) para el financiamiento de 14 proyectos en las localidades de Casilda, Esperanza, Firmat, Funes, Rafaela, Reconquista, Santo Tomé, Santa Fe y Rosario ($ 1.076 millones), el colector principal de calle Espora ($ 484 millones), mantenimiento de las rutas nacionales 11 y 1V11 tramo: Emp. RN A007 - Emp. RP 39 ($ 613 millones), Duplicación de calzada de la ruta nacional 34 tramo: RP 13 - Sunchales ($ 3.660 millones), Repavimentación y mantenimiento ruta nacional 98 Tramo: Emp. RN 11 (Vera) - Tostado (Malla 209A) ($ 1.279 millones), Centro Federal Penitenciario Litoral Argentino Coronda ($ 1.424 millones), Edificio de Aulas de la Universidad nacional del Litoral - Av. Costanera Este ($ 267 millones) y desagües cloacales en la comuna de Emilia ($ 83 millones).

 "Vamos a hacer una mejor elección"

 Respecto al resultado de las elecciones de setiembre, para el ministro Katopodis "la gente tuvo en la votación la posibilidad de descargar mucho cansancio, mucho agotamiento, que fue el reflejo de más de un año y medio muy difícil y complicado para todos. Todos los oficialismo, que se tuvieron que hacer cargo de decisiones necesarias y responsables pero ingratas, tuvieron que responder electoralemente y los resultados no fueron buenos en casi ningún lugar del mundo. Tenemos por delante la tarea de acercarnos mucho más a la gente para explicarles cómo ya estamos dejando atrás la pandemia y cómo vamos a poner todo el esfuerzo en esta nueva etapa que es básicamente poner en marcha la economía y generar empleo. Seguramente vamos a hacer una mejor elección de la que hicimos.

 -En recuperar empleos y apuntalar la inversión, su ministerio es clave.

 -Ya venimos con 14 meses consecutivos de crecimiento, empleo y mano de obra. No hay un insumo de la construcción que no este batiendo récord en cuanto al despacho y la producción como el cemento, el hormigón, ladrillos, asfalto. Así que no hay dudas que la construcción y la obra pública son uno de los pilares de la recuperación, pero también entendemos que la recuperación no es homogénea, llega ni a todos los sectores ni a todos los argentinos y por eso la decisión del presidente es acelerar todo lo posible las respuestas para que llegue a todos.