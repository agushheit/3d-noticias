---
category: La Ciudad
date: 2021-04-21T07:20:08-03:00
thumbnail: https://assets.3dnoticias.com.ar/bache.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad de Santa Fe
resumen: Trabajos de bacheo previstos para este miércoles
title: Trabajos de bacheo previstos para este miércoles
entradilla: La Municipalidad informa las calles por las que avanza el cronograma de
  tareas previsto en el Plan de Bacheo 2021. Puede haber cortes de circulación y desvíos
  de colectivos a medida que se desarrolla la obra.

---
En enero pasado, la Municipalidad puso en marcha un plan de bacheo para reparar más de 12.000 metros cuadrados de calles de la capital provincial. El programa se desarrolla por fases, priorizando las zonas de la ciudad que evidencian mayor circulación.

En la zona del micro y macrocentro, está previsto que los trabajos se concreten en:

* 1° de Mayo, entre Uruguay y Jujuy
* Urquiza, entre General López y 3 de Febrero
* Francia, entre General López y Monseñor Zazpe
* San Lorenzo, entre General López y Monseñor Zazpe
* San Lorenzo, entre Catamarca y La Rioja
* San Lorenzo, entre Catamarca y Vera
* San Lorenzo, entre Hipólitor Yrigoyen y Crespo
* Salvador Caputto, entre San Lorenzo y Saavedra

En tanto, en avenidas troncales, la obra continuará por:

* Aristóbulo del Valle y French, mano norte-sur
* Aristóbulo del Valle, entre avenida Gorriti y Los Paraísos, mano sur-norte

**Por el operativo de vacunación**

La Municipalidad comunica que, en el marco del operativo de vacunación que se realiza en la ciudad, habrá controles en el tránsito vehicular, de 8 a 20 horas, en las inmediaciones de:

La Esquina Encendida, en la intersección de Facundo Zuviría y Estanislao Zeballos

Centro de Educación Física (CEF) N° 29, ubicado en avenida Galicia al 2002