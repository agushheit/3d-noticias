---
category: Agenda Ciudadana
date: 2021-08-30T06:15:32-03:00
thumbnail: https://assets.3dnoticias.com.ar/Humedales.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Ambientalistas cortaron el puente Rosario-Victoria por los nuevos incendios
  en los humedales
title: Ambientalistas cortaron el puente Rosario-Victoria por los nuevos incendios
  en los humedales
entradilla: Los manifestantes anticiparon que adoptarán medidas similares "todas las
  veces que sean necesarias para que pongan fin a las quemas que están devastando
  territorios e islas del Delta y por una nueva Ley de Humedales ya".

---
La Multisectorial Humedales, ambientalistas y vecinos rosarinos, cortaron este domingo el tránsito en el puente Rosario-Victoria, para exigir que se ponga fin "a los nuevos focos de incendio" que por estos días están "devastando" islas del Delta del Paraná.

Los manifestantes se convocaron a partir de las 15 horas, en la explanada que sube hacia el puente Rosario-Victoria, desde Boulevard Rondeau, donde interrumpieron el tránsito vehicular, en la mano que lleva desde esta ciudad hacia la vecina localidad entrerriana.

En ese lugar, donde los ambientalistas desplegaron una gran bandera con la leyenda "Somos Humedal", permanecieron un par de horas y luego marcharon hasta la parte media del Puente.

El corte, que se extendió hasta las 19 horas, generó demoras a decenas de autos, camiones y camionetas que intentaban circulan por ese estratégico lugar que comunica a Santa Fe con Entre Ríos.

Los ecologistas dijeron a Télam que decidieron cortar el tránsito "ante la inmensa cantidad de nuevos focos de incendio en los humedales", de los últimos días.

Y anticiparon que adoptarán medidas similares "todas las veces que sean necesarias para que pongan fin a las quemas que están devastando territorios e islas del Delta y por una nueva Ley de Humedales ya".

La protesta se decidió con "carácter de urgente", debido a que ayer, en Villa Constitución, "el humedal transformado en ceniza caía sobre los patios de las casas. Las columnas de humo allí son inmensas”, dijo a Télam Julieta Bernabé, de la Multisectorial Humedales.

Tras señalar que este domingo "hay nuevas quemas", detalló que los focos de incendio de ayer "se veían a simple vista y continuaban hacia San Nicolás y Baradero", donde, según vecinos, "los incendios se ubican en tierras donde se desarrollan actividades agroganaderas".

La ambientalista explicó que los humedales "están amenazados cuando no devastados por incontables actividades productivas que el mismo Estado está habilitando".

En ese marco, Bernabé señaló que pese a la travesía náutica y terrestre del último 18 de agosto, y la manifestación realizada por ecologistas frente al Congreso de la Nación, para pedir la urgente sanción de la Ley de Humedales "la respuesta fue un no rotundo".