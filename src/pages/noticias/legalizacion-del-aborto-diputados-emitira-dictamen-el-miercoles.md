---
category: Agenda Ciudadana
date: 2020-12-06T12:03:26Z
thumbnail: https://assets.3dnoticias.com.ar/aborto.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Telam'
resumen: 'Legalización del aborto: Diputados emitirá dictamen el miércoles'
title: 'Legalización del aborto: Diputados emitirá dictamen el miércoles'
entradilla: Este viernes concluyeron las reuniones informativas. La iniciativa será
  tratada de forma presencial. En la misma sesión se analizará el Programa de los
  1.000 días.

---
Un plenario de comisiones de la Cámara baja emitirá el próximo miércoles dictamen sobre el proyecto de Interrupción Voluntaria del Embarazo (IVE), luego del debate de este viernes, última jornada de las reuniones informativas en las que expusieron 70 oradores, a favor y en contra de la iniciativa enviada al Congreso por el Poder Ejecutivo.

Así, tras la reunión por teleconferencia que mantuvo el plenario de comisiones de diputados, la del miércoles se realizará en forma presencial, así como también las previstas para el jueves y viernes, que se prevé se extenderán al menos por 20 horas.

Junto con el proyecto de legalización del aborto se tratará el Programa de los 1.000 días, también iniciativa del Poder Ejecutivo, que busca proteger a las mujeres embarazadas y a los niños hasta los primeros años de la infancia.

## **FIN DE LA PRIMERA ETAPA**

Con la reunión de comisiones realizada, culminó la primera etapa de la discusión del proyecto de Interrupción Voluntaria del Embarazo hasta la semana 14, que comenzó el último martes con las exposiciones de los ministros de Salud Ginés González Garcia, de Mujeres, Géneros, y Diversidad, Elizabeth Gómez Alcorta, y la secretaria Legal y Técnica Vilma Ibarra.

A lo largo de las cuatro jornadas, el plenario estuvo conducido por los presidentes de las comisiones de Legislación General Cecilia Moreau, de Legislación Penal Carolina Gaillard, de Mujer y Diversidades Mónica Macha, y de Acción Social y Salud Pablo Yedlin.

Al cerrar el debate, la diputada Moreau señaló el "orgullo" de haber participado de este debate. "Nosotros, como dirigentes políticos, tengamos la posición que tengamos, debemos estar orgullosos de llevar adelante este debate, porque estamos haciéndonos cargo de nuestra responsabilidad como legisladores", dijo y contó su experiencia.

"Esta mujer que está presidiendo este debate, a los 16 años le pasó, me falló el método anticonceptivo. Yo tomé la decisión de interrumpir ese embarazo y lo tomé totalmente consciente", asumió.

Señaló que "era una nena que soñaba con ser médica, con militar, con vivir un montón de cosas, y tuve la suerte de hablarlo con mi mamá que me acompañó en esa decisión. Tenía los medios para llegar a un aborto más seguro. En mi colegio, hubo compañeras que no tuvieron la posibilidad que tuve yo de acceder a un aborto seguro".

La diputada entrerriana Gaillard marcó que “la mujer que decide interrumpir su embarazo lo va a hacer esté penalizado o no" y, en ese sentido, afirmó que "es el Estado el que debe estar presente para acompañar, para proteger y para cuidar en ese momento a esa mujer que toma la decisión".

La diputada del PRO, Silvia Lospenatto reivindicó la decisión de Mauricio Macri de haber habilitado el debate en 2018 porque, dijo, "aunque no apoyaba el aborto lo hizo con vocación republicana" y consideró que si bien no se logró su sanción "será recordado como uno de los grandes debates" de los últimos años.

“No logramos sancionar la ley, pero sin dudas que no perdimos. Las mujeres no perdimos con el debate del 2018. Las mujeres ganamos, porque el aborto, socialmente, está despenalizado", completó.

En cambio, la diputada macrista Victoria Morales Gorleri defendió la "innegable existencia de la persona por nacer como sujeto de derecho en el ordenamiento jurídico" de Argentina.

"La vida de la persona por nacer cuenta con protección jurídica, siendo esta tutelada y protegida a través del delito del aborto", sostuvo.

La secretaria parlamentaria del bloque del Frente de Todos, Cristina Álvarez Rodríguez, puso el eje en que la IVE es "un tema de salud pública" y que el hecho de que "el aborto esté penalizado es una barbaridad que habla de otro tiempo", en el que "las mujeres no tenían derecho ni a tener identidad ni a ser ciudadanas".

"A las mujeres argentinas y del mundo, todo nos cuesta el doble. Nos costó poder votar, nos costó acceder a la Ley de Paridad. Esto también nos cuesta. Somos nosotras, en nuestros cuerpos, las que llevamos adelante nuestras historias, las que cargamos con nuestras elecciones, las que queremos ser libres, respetadas, ciudadanas, las que no queremos que nadie decida por nosotras", planteó.

También desde el Frente de Todos, la diputada Vanessa Massetani hizo hincapié en que "hoy ya nadie piensa que se le puede reprochar penalmente a la mujer la decisión de abortar" y agregó: "Incluso quienes nos llamamos celestes, hemos logrado visualizar esta propuesta alternativa y superadora de los 1.000 días".

La diputada radical Brenda Austin llamó la atención respecto de que "muchos de los invitados de los sectores que se oponen a la ley fueron pastores y referentes de distintas religiones" y dijo: "No me parece que esté mal que piensen así, pero el Congreso es el lugar en el que se sancionan las leyes para toda la ciudadanía".

En tanto, la diputada de Consenso Federal Graciela Camaño criticó que "se pretenda, desde una suerte de redefinición del feminismo de este siglo 21, instalar la idea que solo se es feminista si se defiende el derecho al aborto".