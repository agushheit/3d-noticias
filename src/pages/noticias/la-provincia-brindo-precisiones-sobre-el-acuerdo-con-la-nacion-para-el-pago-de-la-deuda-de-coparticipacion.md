---
category: Agenda Ciudadana
date: 2022-06-30T07:48:18-03:00
thumbnail: https://assets.3dnoticias.com.ar/2022-06-29NID_275138O_1.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: LA PROVINCIA BRINDÓ PRECISIONES SOBRE EL ACUERDO CON LA NACIÓN PARA EL PAGO
  DE LA DEUDA DE COPARTICIPACIÓN
title: LA PROVINCIA BRINDÓ PRECISIONES SOBRE EL ACUERDO CON LA NACIÓN PARA EL PAGO
  DE LA DEUDA DE COPARTICIPACIÓN
entradilla: La actividad fue encabezada por el secretario de Justicia y el fiscal
  de Estado y contó con la participación de legisladores provinciales.

---
En el Salón Blanco de Casa de Gobierno, el secretario de Justicia, Gabriel Somaglia; y el fiscal de Estado, Rubén Weder; brindaron precisiones a legisladores provinciales sobre el acuerdo de la provincia con la Nación para el pago de la deuda de coparticipación.

El secretario de Justicia, Gabriel Somaglia; y el fiscal de Estado, Rubén Weder; encabezaron este miércoles un encuentro con legisladores provinciales para explicar los detalles del acuerdo de la provincia con la Nación para el pago de la deuda de coparticipación.

En la oportunidad, Somaglia sostuvo que "el objeto del encuentro con los legisladores que estuvieron presentes fue hacerles saber el estado de la negociación actual y, sobre todo, algunos interrogantes planteados sobre facultades del Poder Ejecutivo para poder cerrar el convenio con Nación, tal como lo ordenó la Corte Suprema de Justicia de la Nación”.

“Lo que estamos en condiciones de afirmarles es la facultad total y única del Poder Ejecutivo de la provincia en llevar adelante el acuerdo con la Nación . Acá no hay incumplimiento por parte de la provincia, la Corte dio un plazo y estamos dentro de los mismos que se vencen en el mes de julio. Si se vencen tampoco hay consecuencias negativas para el erario púbico, por cuanto esto queda abierto hasta que la Corte lo pueda validar", manifestó el secretario de Justicia.

A su turno, el fiscal de Estado explicó que “estamos en el ámbito de la ejecución de una sentencia que fue dictada en un proceso judicial en el año 2015 y que la Corte decidió reintegrarle a la provincia lo retenido a la provincia, en la forma y en los plazos que las partes acuerden. No hay una sentencia a dar sumas de dinero, sino a acordar entre las partes”.

“Después de analizar este tema en el marco de una ejecución de sentencia y de que lo acordado tiene que pasar por un control de legalidad y de congruencia por parte de la Corte Suprema de Justicia Nacional, quien homologará lo que se acuerde, a partir de ahí, el interés público está garantizado por la decisión de la Corte”, sostuvo Weder.

“El antecedente inmediato que tiene la misma naturaleza -explicó Weder-, es el caso de la deuda de San Luis, donde estaban en juego las mismas normas y la sentencia fue dictada el mismo día que la de Santa Fe, que acordó un pago en bonos”.

Somaglia sostuvo que “no se trata de un contrato o un acuerdo donde alguien cede o alguien concede, ni hay quita de ninguna naturaleza ni resignación de ninguna posición jurídica o económica por parte del Estado de la provincia. Acá lo que se esta haciendo es cumplir una sentencia judicial, en la que la Corte ha marcado pautas y ha dejado en libertad a la Nación y a la Provincia, para poder acordar”.

“Una vez que eso se acuerde, será supervisado por la Corte en cuanto a la legitimidad de los firmantes. El Poder Ejecutivo, como representante de la provincia, es quien está facultado para llevar adelante la negociación, así lo marca la Constitución y son las atribuciones que cada uno de los poderes tienen y, en el caso de que se cerrara, aún por la Ley de la Fiscalía de Estado, el fiscal tiene facultades para firmar un acuerdo de estas características”, concluyó el secretario de Justicia.

**PRESENTES**

De la actividad participaron la diputada Agustina Donnet, los diputados Oscar Martínez y Ricardo Olivera, y los senadores Armando Traferri y Alcides Calvo.