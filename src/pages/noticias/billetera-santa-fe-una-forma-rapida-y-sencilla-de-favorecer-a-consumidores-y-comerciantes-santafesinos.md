---
category: Estado Real
date: 2021-01-17T10:23:22Z
thumbnail: https://assets.3dnoticias.com.ar/billetera-santa-fe.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: "“Billetera Santa Fe”, una forma rápida y sencilla de favorecer a consumidores
  y comerciantes santafesinos  "
title: "“Billetera Santa Fe”, una forma rápida y sencilla de favorecer a consumidores
  y comerciantes santafesinos "
entradilla: A través de simples pasos, podrán acceder a beneficios y reintegros que
  tienen por objetivo sumar poder de compra y favorecer la demanda de bienes y servicios,
  fomentando el comercio local.

---
El gobernador de la provincia Omar Perotti presentó este martes “Billetera Santa Fe”, el programa de beneficios y descuentos que busca incentivar la demanda de bienes y servicios mediante el otorgamiento de reintegros de dinero a todas las personas que realicen compras en comercios radicados en la provincia.

Esta herramienta permitirá sumar poder de compra otorgando reintegros de un 30 por ciento en la adquisición de alimentos, indumentaria, calzados, juguetería, librería, bares, restaurantes, farmacias, gastronomía y turismo, y un 20 por ciento en electrodomésticos.

El mismo será solventado por el Gobierno de Santa Fe en un 85 por ciento, y el 15 por ciento restante estará a cargo del comercio. El dinero se acreditará entre las 24 y 48 horas hábiles de realizada la compra en el “Saldo Virtual de la Billetera”, con un tope de reintegro de 5.000 pesos por mes. Las promociones estarán vigentes todos los días en el caso de los alimentos y farmacias; en tanto que los lunes, martes y miércoles será para los demás rubros.

**CONDICIONES PARA ACCEDER AL PROGRAMA**

Requisitos si sos consumidor: 

* Tener domicilio en la Provincia de Santa Fe (validación contra Renaper).
* Ser una persona física, mayor a 18 años.
* Tener un dispositivo móvil con capacidad para descargar apps.
* Descargarse del PlayStore la billetera PlusPagos.
* Cargar Saldo en la Billetera en un Santa Fe Servicios o transfiriendo desde una Cuenta Bancaria u otra Billetera Virtual o Cargando los datos de la Tarjetas de Débito de cualquier banco.
* Pagar en un comercio adherido al programa con el QR del Comercio, ver los locales adheridos en el Mapa de PlusPagos o en la Web de la Provincia
* A las 24hs se reintegra el % en el Saldo Virtual, se podrá ver el movimiento como “Beneficio Billetera Santa Fe”

Requisitos para adherir como comercio: 

* Persona Física o Jurídica cuyo comercio que tribute en la provincia de Santa Fe.
* Inscripto en AFIP y API.
* (*) se le aplicarán las retenciones impositivas que corresponda, tanto nacionales como provinciales.
* Abrir una cuenta en Banco Santa Fe para operar con QR, sin costo.
* La Apertura de la cuenta en el BSF le dará acceso vía un mail a la solución web o App para poder cobrar con QR.
* Se podrá utilizar utilizar el QR de PlusPagos, ya sea desde la app , desde la solución web o simplemente imprimiendo el QR
* La Web/App le permitirá poder acceder a imprimir o mostrar un QR o integrarlos con su sistema mediante la lectura del Código de Barra que surge en cada compra
* Funcionalidades de Pluscobros: 

      – Cada caja tendrá un  QR exclusivo

      – Crear  sucursales y cajas

      – Control de movimientos por caja

      – Cobrar las ventas en una cuenta Bancaria

      – Generar links de pago individuales o masivos

      – Acceder a un botón de pagos

Para mayor información, ingresar en www.santafe.gov.ar/billeterasantafe, allí se hace la adhesión a este programa de beneficios y se enviará el usuario y clave de adhesión para comenzar