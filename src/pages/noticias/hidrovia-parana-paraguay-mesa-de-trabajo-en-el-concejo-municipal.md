---
category: Agenda Ciudadana
date: 2021-04-01T07:59:25-03:00
thumbnail: https://assets.3dnoticias.com.ar/concejo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Concejo Santa Fe
resumen: 'Hidrovía Paraná – Paraguay: mesa de trabajo en el Concejo Municipal'
title: 'Hidrovía Paraná – Paraguay: mesa de trabajo en el Concejo Municipal'
entradilla: Concejales y concejalas se reunieron con diferentes disertantes en una
  jornada de trabajo con el objetivo de poner en debate el rol de la ciudad de Santa
  Fe en la hidrovía Paraná – Paraguay.

---
En el recinto de sesiones, se llevó adelante la mesa de trabajo “Santa Fe en la hidrovía. Una oportunidad histórica para el desarrollo de la región”. En la jornada se realizó una lectura global sobre el desarrollo regional y local para comprender el valor estratégico del emplazamiento geográfico donde se encuentra nuestra ciudad capital y conocer las potencialidades a partir de una mejor explotación de la hidrovía.

Al respecto, el presidente del Concejo, Leandro González, afirmó que “como Concejo Municipal estamos convencidos de que los distintos debates que tengan que ver con la ciudad y la región tienen que tener como epicentro al Concejo. En ese sentido, en el marco del Plan 2021, Concejo Abierto, Participativo y Transparente, recibimos la propuesta del concejal Paco Garibaldi para discutir de manera colectiva sobre la hidrovía con distintas organizaciones de la ciudad y diversos dirigentes políticos y sociales. Entendemos que la hidrovía es un debate sobre nuestro río y diversas cuestiones vinculadas a la producción, la economía, el empleo y que también, roza hasta cuestiones de seguridad. En ese marco, la discusión sobre la concesión de la hidrovía es fundamental para la ciudad de Santa Fe”.

Por otra parte, el concejal Paco Garibaldi detalló que “en un mes vence la concesión actual de la hidrovía para lo que es el dragado, balizamiento y señalización de la misma y se está discutiendo un nuevo proceso licitatorio. En esa licitación, la ciudad y la provincia de Santa Fe no pueden estar al margen por lo que aporta en términos económicos, productivos, en la generación de empleo y desarrollo territorial, y fundamentalmente, en la posibilidad de zanjar esa grieta histórica entre campo e industria. Pero también, es la posibilidad de vincularnos culturalmente, de generar nuevas centralidades en la provincia, y de discutir el federalismo como corresponde”.

![](https://assets.3dnoticias.com.ar/concejo1.jpg)

**Presentes**

En la actividad, se pronunciaron disertantes inmiscuidos en la temática, desde puntos de vistas diversos, con roles y funciones distintos. Estuvieron presentes en el recinto o de manera virtual: Miguel Lifschitz, presidente de la Cámara de Diputados de Santa Fe; Matías Schmüth, secretario de Producción de la ciudad de Santa Fe; Ignacio Mantaras, presidente de la Sociedad Rural; Eugenio Martín de Palma, rector de la Universidad Católica de Santa Fe; Raúl Pedraza, decano de la Facultad de Ingeniería y Ciencias Hídricas de la UNL; Néstor Dona, secretario de Gestión Estratégica de la Universidad Católica de Santa Fe; Fernando Imaz y Sergio Ludueña, representantes de la Universidad Tecnológica Nacional; Alejandra Chena, del Ente de Coordinación Metropolitana; Sergio Winkelmann, presidente de la Cámara Argentina de la Construcción Santa Fe; Marcelo Perassi, presidente de la Cámara de Comercio Exterior; Roberto Pilatti.

Asimismo, se sumaron Mario Ramello, coordinador de la Cámara del Transporte de Cargas de Santa Fe; Paula Rodeles, representante del Centro Comercial Santa Fe; Martín Garay y Diego Truco, en representación de la Asociación Autotransporte de Cargas Santa Fe; Mario Huber, representante del Parque Logístico InterPuertos; Diego Colono, representante del DEM en el Parque Industrial Polígonos I; y Federico Culzoni.

También se sumaron de manera virtual las diputadas Clara García y Érica Hynes y la ex diputada nacional y ex ministra de Producción, Alicia Siciliani.

En tanto, estuvieron presentes junto al presidente del Concejo, Leandro González, los concejales y concejalas: Paco Garibaldi, Guillermo Jerez, Laura Spina, Valeria López Delzar, Luciana Ceresola, Laura Mondino, Federico Fulini, Paco Garibaldi, Mercedes Benedetti, Inés Larriera, Juan José Saleme, Lucas Simoniello y Sebastián Mastropaolo.

![](https://assets.3dnoticias.com.ar/concejo2.jpg)