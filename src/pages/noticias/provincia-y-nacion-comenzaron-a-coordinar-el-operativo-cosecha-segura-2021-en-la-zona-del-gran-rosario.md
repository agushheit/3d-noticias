---
category: El Campo
date: 2021-02-10T06:44:38-03:00
thumbnail: https://assets.3dnoticias.com.ar/campo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: Provincia y nación comenzaron a coordinar el operativo “Cosecha Segura 2021”
  en la zona del Gran Rosario
title: Provincia y nación comenzaron a coordinar el operativo “Cosecha Segura 2021”
  en la zona del Gran Rosario
entradilla: El objetivo es el ordenamiento del tránsito pesado, que este año prevé
  la circulación de unos 14 mil camiones por día. Asimismo, busca evitar casos de
  inseguridad y optimizar la logística en las rutas de la región.

---
El ministro de Gobierno, Justicia, Derechos Humanos y Diversidad de la provincia, Roberto Sukerman, encabezó este lunes una reunión con intendentes y presidentes comunales del Gran Rosario, en el marco del operativo Cosecha Segura 2021 que apunta a disminuir el nivel de conflictividad y evitar posibles de casos de inseguridad en la logística de la cosecha gruesa.

“Es la primera de una serie de reuniones con intendentes, presidentes comunales, y autoridades nacionales del área de agricultura, transporte y seguridad para planificar lo que va a ser en las próximas semanas la cosecha 2021. También, participaron localidades que si bien no tienen puerto se ven muy afectadas por el tránsito de camiones”, describió el ministro.

Asimismo, Sukerman adelantó que también habrá encuentros con autoridades sanitarias “porque todavía estamos en pandemia y tiene que haber controles en ese sentido, como los hubo el año pasado”. Confirmó que a los mismos se sumarán referentes de Vialidad nacional y provincial y de la Administración Federal de Ingresos Públicos (Afip) “para llevar adelante los controles tributarios correspondientes”.

De la reunión, llevada a cabo en la sede de Gobierno de Rosario, participaron, además, el subsecretario de Seguridad de la Nación, Luis Morales; el subsecretario de Transporte de la Nación, Marcos Farina; el subsecretario de Agricultura de la Nación, Delfo Buchaillot; y subdirector ejecutivo de la Comisión Nacional de Regulación del Transporte (CNRT), Diego Giuliano.

**TRABAJO CONJUNTO**

Más temprano, referentes del Ministerio de Seguridad de Santa Fe se reunieron con autoridades nacionales para la implementación conjunta del operativo que comenzará en el mes de marzo en la provincia de Santa Fe.

“Al tener en suelo santafesino los puertos privados más importantes del país y teniendo en cuenta que por aquí se va el 80% de la cosecha de granos, ello representa un desafío porque por nuestras rutas van a converger más de 14 mil camiones. Esto amerita que haya todo un equipo de trabajo de la provincia junto a fuerzas federales y referentes de Nación, porque desde Santa Fe podemos idear y programar, pero necesitamos el aporte de todos los estamentos del Estado”, aseguró el subsecretario de la Agencia Provincial de Seguridad Vial (Apsv), Osvaldo Aymo.

Por su parte, el subsecretario de Prevención y Control del Ministerio de Seguridad santafesino, Alberto Mongia, resaltó que “para este operativo tan complejo y prolongdo es imposible lograrlo sin el trabajo en conjunto de todas las fuerzas locales, provinciales y municipales; tenemos un equipo, no es unipersonal, y con la experiencia que han tenido en otros años vamos a lograr que este año la cosecha sea segura como esperamos”.

A su turno, el subsecretario de Intervención Federal, Luis Morales, adelantó que “vamos a planificar las tareas de la misma manera que lo hicimos el año pasado, pero con ajustes y refuerzos. Por eso esta es una reunión de trabajo que nos va a permitir una planificación óptima, teniendo en cuenta el tiempo que lleva la cosecha y la cantidad de camiones que sabemos van a pasar, principalmente, por la zona del cordón industrial”.

“Queremos llevar tranquilidad a los transportistas y desde nuestro lugar como representante de fuerzas federales en la provincia quiero decir que están garantizados el trabajo y la vocación de servicio”, completó Morales.

Durante el encuentro estuvieron presentes también el Jefe de Gabinete de Ministerio de Transporte de la Nación, Abel Chavo; el director general de Transporte de Carga, Juan Manuel Escudero; el secretario de Transporte de Santa Fe, Osvaldo Miatello; la jefa de Policía de Santa Fe, Emilce Chimenti; y los jefes regionales de las unidades II, XV y XVII

**ORDENAMIENTO Y CONTROL**

Entre las tareas que se llevarán a cabo para afrontar el Operativo Cosecha Segura 2021 están la regulación de la circulación del tránsito pesado por determinadas rutas y/o caminos; el ordenamiento y desvío de camiones hacia una playa de espera o dársena segura determinada; y la detención temporal de transporte de cargas por el período de tiempo necesario a los fines de evitar el colapso de las vías de circulación en las zonas de terminales portuarias y corte o interrupción total o parcial del tránsito.