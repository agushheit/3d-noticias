---
category: La Ciudad
date: 2021-09-27T06:00:11-03:00
thumbnail: https://assets.3dnoticias.com.ar/EPE.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La EPE cambia el acceso de las oficinas de Bv. Pellegrini
title: La EPE cambia el acceso de las oficinas de Bv. Pellegrini
entradilla: A partir del lunes, se accederá por Pasaje Martínez 2627.

---
La Sucursal Santa Fe Sur de la Empresa Provincial de la Energía informó que, por tareas de remodelación edilicias, a partir del lunes 27 de setiembre, el acceso a las oficinas comerciales del edificio de Bv. Pellegrini, se hará por Pasaje Martínez 2627.

 Por otro lado informó que el horario de atención es de 7.30 a 13.

 Además, recordó que la mayoría de las gestiones relaciones con el servicio eléctrico se pueden realizar a través de la oficina virtual, ingresando en www.epe.santafe.gov.ar

 