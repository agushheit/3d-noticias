---
category: Agenda Ciudadana
date: 2022-01-16T06:15:20-03:00
thumbnail: https://assets.3dnoticias.com.ar/lima.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Alerta amarillo por tormentas para este domingo en la provincia de Santa
  Fe
title: Alerta amarillo por tormentas para este domingo en la provincia de Santa Fe
entradilla: El aviso fue emitido por el Servicio Meteorológico Nacional para los departamentos
  de General López, Constitución, Caseros, San Lorenzo, Rosario e Iriondo.

---
Este sábado por la noche, el Servicio Meteorológico Nacional (SMN) emitió un alerta amarillo por tormentas para las provincias de Buenos Aires, Córdoba, Santa Fe, Entre Ríos, La Rioja, Catamarca, Salta y Jujuy.

 En territorio santafesino, dicho aviso afecta a los departamentos de General López, Constitución, Caseros, San Lorenzo, Rosario e Iriondo. La Capital no se encuentra incluido.

 Sin embargo, según el SMN, la ciudad de Santa Fe posee entre un 40% y 70% de probabilidades de lluvia para este sábado por la noche, la cual podría continuar durante la jornada del domingo por la tarde/noche.

 El organismo nacional indicó además que Tierra del Fuego posee alerta amarillo por lluvias. Chubut y Santa Cruz por viento.