---
layout: Noticia con imagen
author: "Fuente: Telefe Santa Fe"
resumen: "Santo Tomé: Coronavirus"
category: Agenda Ciudadana
title: "Santo Tomé: Cierran preventivamente el Concejo Municipal por un caso de Covid"
entradilla: El Concejo Municipal de la ciudad de Santo Tomé suspende sus
  actividades como medida precautoria tras la aparición de un caso positivo de
  Covid.
date: 2020-11-04T13:57:54.942Z
thumbnail: https://assets.3dnoticias.com.ar/santo-tome.jpg
---
Ayer, desde el Concejo Municipal de la ciudad de Santo Tomé confirmaron la determinación de dar por suspendidas sus actividades a partir de hoy y hasta el próximo lunes 9 de noviembre inclusive, como medida precautoria luego de la aparición de un caso positivo de Covid en una de las personas que habitualmente prestan servicio en el órgano legislativo local.

Tal y como se indicó desde el Concejo, como consecuencia de esta noticia y con el objetivo de preservar la salud tanto de los ediles como de sus asesores y todos aquellos que allí se desempeñan, se decidió que al menos por este lapso no concurran a trabajar ni el personal político ni tampoco los empleados del Concejo.

Con este panorama, y esperando que no surjan novedades respecto a otros contagios, la próxima sesión del cuerpo legislativo santotomesino tendría lugar el próximo miércoles 18 de noviembre, siempre con la posibilidad de que esta fecha se modifique en caso de ser necesario.