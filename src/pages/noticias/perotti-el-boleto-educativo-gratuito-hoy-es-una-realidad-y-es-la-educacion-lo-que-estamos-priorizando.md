---
category: Estado Real
date: 2021-02-20T06:00:23-03:00
thumbnail: https://assets.3dnoticias.com.ar/BOLETOEDUCATTIVO.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Perotti: “El Boleto Educativo Gratuito hoy es una realidad y es la educación
  lo que estamos priorizando”'
title: 'Perotti: “El Boleto Educativo Gratuito hoy es una realidad y es la educación
  lo que estamos priorizando”'
entradilla: El gobernador presentó la iniciativa por la cual la provincia invertirá
  4.500 millones de pesos para garantizar la asistencia de estudiantes, docentes y
  auxiliares a los establecimientos educativos de toda la provincia.

---
El gobernador de Santa Fe, Omar Perotti, presentó en Rosario la puesta en marcha del Boleto Educativo Gratuito, una iniciativa que tiene por objetivo asegurar la asistencia de estudiantes, docentes y asistentes escolares a todos los establecimientos educativos de la provincia, eliminando así una potencial barrera de acceso a la educación. La inversión del Estado será de 4.500 millones de pesos por año y con potencialidad de alcanzar a 500.000 beneficiarios.

Durante el lanzamiento en el Parque Nacional a la Bandera, Perotti afirmó: “Cuando propuse el Boleto Educativo Gratuito lo hice con la plena convicción de que la educación es el camino a la libertad, es la mejor herramienta para igualar oportunidades. Por eso nada mejor que todos nuestros alumnos y alumnas estén en la escuela aprendiendo, formándose y capacitándose”.

“Era indispensable poner en marcha este programa luego del año pasado, que ha sido duro para muchas familias. El Boleto Educativo Gratuito hoy es una realidad y la educación es lo que priorizamos”, agregó el mandatario provincial.

“Sentimos que el Boleto Educativo es una inversión y que es aquí donde hoy se pone en marcha. A partir de este momento comienza la inscripción en toda la provincia”, aseveró el gobernador.

“Este esfuerzo tiene que ver con lo que sentimos que es el futuro de la provincia, que se trata de generar oportunidades para sus habitantes. De eso se trata: sin oportunidades no hay posibilidades de construir una sociedad más justa y más segura, y la educación es ese camino”, concluyó Perotti.

**Acceso a la Educación**

Por su parte, el director provincial de Boleto Educativo y Franquicias, Juan Rober Benegui, sostuvo que esta es “una promesa de campaña que se hace realidad. Estamos acá para celebrar y agradecer que impulsen políticas públicas que tienen como objetivo garantizar la llegada de estudiantes, docentes y auxiliares a los establecimientos educativos. Gracias gobernador por hacernos la generación que vea el boleto educativo una hecho realidad”.

En tanto, la docente María Alejandra destacó que el “boleto gratuito es un hecho revolucionario que va a modificar la vida de muchas familias. Los docentes pensamos en la educación pública, inclusiva y de calidad. El boleto educativo no solo va a lograr que los niños y niñas asistan a la escuela, sino también que permanezcan en ella. Celebro esta iniciativa y espero que sigamos la lucha por otros derechos de niños, niñas, jóvenes y docentes”.

Por su parte, los referentes de la Federación de Estudiantes Secundarios, Irene y Cristian, afirmaron que “es un sueño hecho realidad ver esta promesa cumpliéndose. Hoy estamos acá para celebrar la realidad de una lucha histórica que representa al movimiento estudiantil y que nos precedió en el tiempo. Queremos celebrar la decisión política del gobierno provincial de lanzar este programa porque entendemos que es un gran avance en las luchas estudiantiles. Es importante que la educación sea accesible para todos”.

Por último, el estudiante de abogacía de la UNR, Simón, aseguró: “Hoy la historia empieza a cambiar porque la implementación del boleto gratuito va a generar igualdad de oportunidades, que volvamos con mayor frecuencia y que apostemos al desarrollo de nuestras comunidades. Todo eso es arraigo, el boleto educativo gratuito genera arraigo”.

**Cómo es el sistema**

El gobierno provincial abonará el costo del boleto a estudiantes de nivel inicial, primario, secundario, terciario y universitario; así como también a docentes y auxiliares de establecimientos educativos reconocidos por el Ministerio de Educación que utilicen el servicio público de transporte automotor de pasajeros interurbanos, suburbanos y urbanos.

En el caso de las líneas urbanas, el gobierno provincial cubrirá el costo de dos boletos diarios -al igual que en las interurbanas- si la distancia entre el domicilio y el establecimiento educativo es menor a 60 kilómetros. En el caso de que el recorrido sea mayor, se cubrirán dos boletos al mes.

El Boleto Educativo Gratuito entrará en vigencia a partir de la reanudación del presente ciclo lectivo, el 15 de marzo. Sólo podrá utilizarse en los servicios de las empresas provinciales y municipales que cubran trayectos dentro de la provincia de Santa Fe. La inscripción puede realizarse ingresando en [https://www.santafe.gob.ar/boletoeducativo/](https://www.santafe.gob.ar/boletoeducativo/ "https://www.santafe.gob.ar/boletoeducativo/")

**Presentes**

Durante la actividad, también estuvieron presentes la vicegobernadora de la provincia, Alejandra Rodenas; el intendente de Rosario, Pablo Javkin; ministros y ministras del gabinete provincial; legisladores y legisladoras nacionales y provinciales; intendentes y presidentes comunales; concejales; representantes de centros de estudiantes y gremiales, entre otras autoridades.