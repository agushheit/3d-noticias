---
category: La Ciudad
date: 2021-11-22T06:00:51-03:00
thumbnail: https://assets.3dnoticias.com.ar/construccion.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Cuánto cuesta en Santa Fe construir una casa de 70 metros cuadrados
title: Cuánto cuesta en Santa Fe construir una casa de 70 metros cuadrados
entradilla: 'Se conocieron los costos de construcción de una casa para octubre. Subió
  con respecto al mes anterior 4,4%. Mano de obra y materiales, lo que más aumentó

'

---
El sueño de la casa propia cada año que pasa se vuelve inalcanzable para millones de argentinos. La construcción de una casa de 70 metros cuadrados, que en octubre de 2020 costaba en Santa Fe 2.740.002,3 de pesos, en el mismo mes de este año salió $4.513.016,06.

Es decir, que una casa sale 1.773.013,76 pesos más de un año al otro. Una variación porcentual respecto al mismo mes del año anterior del 64,7 por ciento. En junio del 2021 el metro cuadrado de construcción costó 39.142,89 pesos

Según los datos del costo de la construcción dados a conocer por el Instituto Provincial de Estadística y Censos (Ipec), en la ciudad de Santa Fe correspondiente a octubre de este año, el costo de la construcción se registra un aumento de 4,4% en relación al mes anterior. Este resultado surge como consecuencia del alza de 2,6% en el capítulo “Materiales”, de 5,5% en el capítulo “Mano de obra” y de 15% en el capítulo “Gastos generales.”

En el capítulo “Materiales”, los grupos con los aumentos más relevantes son: Maderas, 6,0%; Revoques finos, 5,2%; Ferreterías, 4,9%; Cañerías polipropileno agua fría y caliente, 4,9%; Granitos, 4,7%; Amoblamientos, 3,7%; Cañerías epoxi gas, 3,7%; 3,5%; Chapas; Cañería PVC, cloaca/pluvial 3,5%.

El capítulo ‘’Mano de Obra’’ incorpora los nuevos valores establecidos por la Resolución Nº 2021-430-APNSECT-MPYT para las categorías laborales previstas en el Convenio Colectivo de Trabajo Nº 76/75, estableciendo un aumento del 34% que rige a partir del 1 de octubre de 2021, aplicable sobre los valores vigentes al 31 de marzo de 2021.

**Costo de la Construcción del Ipec**

Cabe destacar que para el cálculo del costo para una casa se considera que la adquisición de los materiales, insumos, la contratación de la mano de obra y demás, se realiza dentro del ejido de la misma ciudad de Santa Fe. No se incluye el valor del terreno, ya que el mismo se encuentra por fuera del proceso constructivo. Se incluye el IVA, ya que la vivienda es construida por un particular, es decir, que absorbe todo el impuesto como consumidor final.

Sobre el relevamiento de materiales desde el Ipec indican que se realiza mensualmente, a partir de un directorio de locales dedicados a la venta al por menor de materiales para la construcción. Para la mano de obra, se toman como referencia las remuneraciones de los trabajadores de las empresas constructoras. Se consulta a la Uocra, colegios profesionales y demás instituciones relacionadas, obteniendo así, el valor hora y adicionales correspondientes.