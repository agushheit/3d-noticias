---
category: Deportes
date: 2021-04-13T06:06:02-03:00
thumbnail: https://assets.3dnoticias.com.ar/union-basket.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Marisa Lemos
resumen: 'Liga Argentina: Unión derrotó a Central Olímpico y se consolida como líder
  de la Conferencia Norte'
title: 'Liga Argentina: Unión derrotó a Central Olímpico y se consolida como líder
  de la Conferencia Norte'
entradilla: 'Este domingo, Unión dio por terminados sus partidos en la mini sede en
  Paraná con una victoria ante Central Olímpico 96 a 83. '

---
Este domingo, Unión dio por terminados sus partidos en la mini sede en Paraná con una victoria ante Central Olímpico 96 a 83. De esta manera, el Tatengue se consolida como líder de la Conferencia Norte de la Liga Argentina de Básquet.

Maximiliano Martín fue la figura del conjunto dirigido por Juan Francisco Ponce y del partido con 27 puntos, 8 rebotes y 2 asistencias.

Unión dio por terminada su presencia en la capital entrerriana con tres triunfos y una derrota y el próximo 20 de abril se medirá en el “Ángel Malvicino” ante Echagüe.

 

**Síntesis**

Unión: Salaberry 4, Borsatti 4, Bandeo 16, Bertona 8, Godoy 9, Jaime 7, Isola 5, Martin 27 y Uranga 16. DT: J.F. Ponce.

Central:  Correnti 0, Tabbia 15, Vittar 11,Crotti 17, Karabitian 3, Manassero 0, Barrales 18 y Zalio 19. DT: E. Lancellotti.

**Parciales:** 16-19, 46-40 y 70-62.