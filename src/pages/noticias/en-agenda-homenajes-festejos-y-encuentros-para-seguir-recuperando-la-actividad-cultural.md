---
category: La Ciudad
date: 2021-11-05T06:00:30-03:00
thumbnail: https://assets.3dnoticias.com.ar/CULTURA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'En agenda: homenajes, festejos y encuentros para seguir recuperando la actividad
  cultural'
title: 'En agenda: homenajes, festejos y encuentros para seguir recuperando la actividad
  cultural'
entradilla: La programación de esta semana propone el homenaje “Piazzolla 100 años”,
  la celebración del Día de la Cumbia Santafesina y la 55º Feria Nacional de Artesanos,
  entre otras actividades.

---
Los espacios culturales gestionados por la Municipalidad invitan a disfrutar de programación pública y gratuita, y producciones generadas desde el ámbito independiente y el privado. Esta semana la agenda convoca desde este jueves 4 de noviembre con “Piazzolla 100 años”, el homenaje que organiza el gobierno local en el Teatro Municipal, con la participación de organismos y grupos locales. Ese día a las 20.30 horas se presentarán la Orquesta de Cámara del Instituto Superior de Música de la Universidad Nacional del Litoral, que dirige el maestro Juan Rodríguez; y el dúo de piano que integran Daniela Salinas y Lilia Salsano. El sábado 6, a la misma hora, se presentarán el sexteto de tango Que lo Tangueó y Danilo Cernotto Quinteto. Las entradas son gratuitas y se deberán retirar previamente en la boletería de San Martín 2020: de lunes a sábado de 9 a 13 y de 17 a 21 horas.

Otro encuentro destacado de esta semana será la celebración del Día de la Cumbia Santafesina, también organizado por el municipio. Será el domingo 7 desde las 19 horas con la participación de Sin Identidad, El Rosario, Valen Alegre y Los del Bohío. Las entradas también son gratuitas y se deben retirar en la boletería del Teatro Municipal, en los días y horarios mencionados anteriormente.

**Artesanías de todo el país**

Los artesanos y las artesanas nucleados en la Feria del Sol y la Luna invitan a la 55º Feria Nacional de Artesanos, que organizan con el apoyo de la Municipalidad. Será desde este jueves y hasta el domingo 7 de noviembre, de 16 a 23 horas, en la Plaza Pueyrredón (Bv. Gálvez 1600). Para participar llegarán a la ciudad 200 artesanos y artesanas de la del interior de la provincia de Santa Fe, la Ciudad Autónoma de Buenos Aires, y las provincias de Buenos Aires, Córdoba, Salta, Jujuy, La Pampa, Río Negro, Chaco, San Luis y Catamarca, entre otras.

El sábado y el domingo, desde las 18 horas, habrá espectáculos para toda la familia. La grilla de la primera tarde comenzará con el show de ilusionismo y humor del Mago Fernán. Desde Buenos Aires, llega después La Chifloneta Títeres, con su unipersonal “Super Tónica”, en la que el malvado Transitorno que había sido congelado en el Polo Sur, se libera por el calentamiento global y regresa para una nueva batalla épica con la heroína de esta historia. La función será en el marco del 6º Festival Títereré. A las 19 horas será el turno del acordeonista, compositor e intérprete de chamamé, Daniel Franich, nacido en Villa Minetti, acompañado por la pareja de danzas que integran Omar Sansone y Daniela Corti. El cierre de la jornada será a puro reggae con el grupo Tamo como Queremo!

El elenco de Teatro de Títeres Municipal se presentará el domingo a las 18 horas, con “El gato y los ratones”, que pone en escena las aventuras de “un gato al que le gustan mucho el queso y los pescados, y dos ratones con mucha hambre que buscan aquí y allá qué comer”. La Guardia Urbana Folk recorrerá en su show un repertorio de chacareras, gatos, escondidos, zambas, cuecas y chamamé entre otros. Estarán acompañados por el grupo de del Taller Cultural de Folclore de la Secretaría de Educación y Cultura de la Municipalidad. A las 19.30 está prevista la tradicional entrega de premios a la Mejor Pieza, Mejor Stand y Trayectoria de la feria nacional. A las 20 se presentará la Murga La Desbocada dirigida por Facundo Céspedes, con 17 artistas en escena y un repertorio que incluye canciones propias de “El murgatorio”, la obra que estrenaron recientemente, y otras clásicas donde se referencia la agrupación, con una puesta que terminará de orquestar un brillante show en el tablado.

**Clásico y moderno**

El ciclo de música Sala 5/360 presentará a Polaca Latosinski Grupo, este viernes 5 de noviembre a las 20 horas,en la planta alta de la Estación Belgrano.Con un repertorio que recorre distintas épocas y estilos del tango, la agrupación interpretará arreglos “que mezclan lo clásico con lo moderno, elaborados específicamente para el carácter de cada integrante y la personalidad sonora de cada instrumento musical”. A la voz de Alejandra “Polaca” Latosinski, se suman Alejandro Fissore en piano, Ariel Micol en bandoneón, IvánWolkovicz en contrabajo y Raúl Vallejos en violín. El proyecto se sostiene en un trabajo de investigación sobre autores, compositores, intérpretes y estilos del tango, donde plasman su compromiso con “la transmisión de nuestra música ciudadana, el contenido conceptual, histórico y la idiosincrasia del pueblo”.

La entrada es gratuita. Los pases se entregarán en puerta (Bv. Gálvez 1150) a partir de las 17 horas, hasta completar la capacidad del espacio.El ciclo continúa el viernes 12 de noviembre, con Pata en Tierra, el trío conformado por Sergio Checho Rosa, Pablo José Ayala y José Carlos Ayala.

**Muestras y exposiciones**

En la Estación Barranquitas (Terraplén Irigoyen y Juan del Campillo) se puede recorrer la muestra de Barrilete Museo de los Niños (Córdoba), que invita a conocer la obra de María Elena Walsh a través de sus personajes, historias y canciones. Para participar de esta propuesta se debe sacar turno previamente. Los medios para solicitarlo son: por Whatsapp al número 3425064024 o por correo electrónico a cultura@santafeciudad.gov.ar

Los días y horarios previstos son: de martes a viernes a las 10, a las 12, a las 14 y a las 16; y los sábados, a las 14 y a las 16 horas. Cada turno se extiende durante una hora y media, que es el tiempo necesario para recorrer y participar de las distintas actividades. Los cupos son limitados por protocolo. La organización reúne a la Municipalidad, a Barrilete y a la Asociación Espacios Educativos.

Durante la semana también se puede visitar la Sala Ariel Ramírez, en la planta alta de la Estación Belgrano (Bv. Gálvez 1150), que entre otros contenidos propone una muestra patrimonial “100 años, 100 canciones”, basada en la colección de objetos y documentos personales del artista, y la muestra fotográfica “Puntos que conectan”, del Archivo de la Imagen Documental Santafesina. La sala abre de martes a viernes, de 9.30 a 12.30 horas.

Hasta el 14 de noviembre se podrá recorrer “Paraje Vecino. Prácticas relacionales en La Boca”, en el Centro Experimental del Color (ala oeste de la Estación Belgrano), de martes a viernes, de 9.30 a 12.30, y los sábados y domingos, de 17 a 20 horas. La muestra, que contó con el acompañamiento curatorial de Florencia Qualina y Leandro Martínez Depietri de Bienalsur, reúne obras y procesos de Ángeles Rivero, Florencia Horn, Luciano Giardino, Valeria Barbero y Laura Hotham, a partir de la residencia artística organizada por la Municipalidad en el Paraje La Boca (Alto Verde), que se desarrolló junto a vecinas y vecinos de la comunidad. Sus producciones dialogan en el montaje con objetos y obras provenientes de los acervos del Museo Municipal de Artes Visuales Sor Josefa Díaz Clucellas, del Museo César Fernández Navarro, del Museo César López Claro y del Museo de Liro, desarrollado por una vecina de La Boca.

**Producciones independientes**

Los sábados 6 y 13 de noviembre a las 21 horas, Terátriba presenta su versión de “Terapia (comedia en tres sesiones y un diagnóstico)”, la obra de Martín Giner que eligieron para su debut como grupo de teatro independiente. “Una comedia donde la demencia y las situaciones delirantes juegan el papel protagonista. Una irreverente alucinación que apuesta por la risa espontánea”, como la definen Amorina Nora de Larrechea en la dirección, Hernán Cogorno y Luis Martín Rinaldi en las interpretaciones.

El grupo surgió en 2020, después de compartir más de diez años en las producciones de Cruz del Sur Constelación Creativa, el elenco teatral de la UTN Facultad Regional Santa Fe, a cargo de Fernando José Díaz. Los integrantes del grupo participaron también en otros proyectos realizados por diferentes elencos de la ciudad de Santa Fe y Entre Ríos, y como estudiantes de la Escuela Provincial de Teatro N° 3200, en diversas muestras y obras teatrales. Las entradas generales están a la venta a un valor de $500.

King Producciones presenta el domingo 7 a las 21 horas, en la Sala Mayor, Break 5how Dance “Viaje en el tiempo”, de Maximiliano Leffe, también director de “Resiliente”, “Futuresex Loveshow” y “Fondo Monetario Familiar”. Este espectáculo de danzas urbanas con más de siete años de trayectoria, contará con música en vivo a través de un set acústico, recuperando otras puestas de la productora, en diálogo también con el tiempo que se abre con la pandemia. Las coreografías son de Leffe y Ailén Giordano, que también integran el staff junto a Valentina Cabrera, Paula Gorostidi, Lucia Savogin, Ema Nagel, Mariano Olivera, Samira Leguizamón, Julia Redondo, Thomas Ortiz, Ramiro Sajama, Milton Robles, Lara Coronel, Bárbara Muller y Gianfranco Barrios. Las entradas generales están a la venta a un valor de $500. El 12 de noviembre darán una nueva función en la Sala Marechal.

**Queen Sinfónico Coral**

El Coro Polifónico y la Orquesta de la Parroquia Nuestra Señora La Merced brindarán dos funciones de su espectáculo tributo a Queen, compuesto por canciones de la banda británica, arregladas por Rodrigo Naffa, director artístico del espectáculo. La presentación será en dos funciones el sábado 6 de noviembre a las 19 y 21.30 horas, en el escenario del Anfiteatro “Juan de Garay”, con la participación del solista rosarino Andrés Novero que interpreta los solos de Freddie Mercury; y la banda Nada Más y Nada Menos, integrada por Matías Allende y Pablo Menéndez en guitarras, Irene Marchi en bajo y Emilio Marchi en batería.

El concierto será a beneficio de la Sociedad de Beneficencia del Hospital Cullen, de la Fundación Regazo, de la Casa Encuentro del Padre Axel y de la Parroquia Nuestra Señora de La Merced, siguiendo la impronta solidaria que tiene el coro. Las anticipadas están a la venta a un valor de $500 y se consiguen a través de los integrantes del coro y en las sedes de las instituciones que serán beneficiadas. El día del concierto, se podrán adquirir en puerta, a $600.

**Santa Fe Vive Bien**

La séptima Expo Holística Santa Fe Vive Bien se realizará este viernes 5 de noviembre, de 17 a 21 horas; y el sábado 6 y domingo 7, de 9 a 13 y de 17 a 21 en el Mercado Progreso (Balcarce 1635). Como en cada edición, el evento reunirá a profesionales y emprendedores que pondrán al alcance del público, herramientas y productos para mejorar la calidad de vida.

En el patio del Mercado se podrán recorrer los stands y participar de las actividades grupales. En la Sala Bicentenario, ubicada en la planta alta, se ofrecerán charlas y talleres en los que los profesionales compartirán sus saberes y técnicas. Entre los expositores podrán encontrarse productos y servicios. Habrá terapeutas técnicas que se podrán vivenciar en el momento: aplicaciones de energía, tarot, masajes, entre otras. Dentro de los productos se podrá encontrar aromaterapia, cremas naturales, artesanías, adornos, mandalas tejidos, indumentaria, hierbas, tanto orgánicos como naturales e industriales.

La entrada será gratuita. Para más información consultar la web y las redes sociales de Santa Fe Vive Bien.