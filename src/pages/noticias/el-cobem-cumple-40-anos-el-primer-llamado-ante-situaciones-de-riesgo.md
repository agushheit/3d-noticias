---
category: La Ciudad
date: 2021-07-14T08:30:20-03:00
thumbnail: https://assets.3dnoticias.com.ar/cobem3.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad de Santa Fe
resumen: 'El Cobem cumple 40 años: el primer llamado ante situaciones de riesgo'
title: 'El Cobem cumple 40 años: el primer llamado ante situaciones de riesgo'
entradilla: La Central Operativa Brigada de Emergencias Municipal, más conocida como
  Cobem, cumple cuatro décadas brindando la primera respuesta ante desastres y emergencias.
  Historias de trabajadores con vocación de servicio.

---
Derrumbes, rescate de animales, evacuaciones, caída de mampostería, accidentes de tránsito, intervenciones en la vía pública, situaciones surgidas por la pandemia. Son innumerables las situaciones de emergencia que atraviesa la comunidad en Santa Fe capital, ante las cuales el Cobem está preparado para salir su auxilio, además de tener un rol de prevención y capacitación.

La Central Operativa Brigada de Emergencias Municipal, conocida popularmente como Cobem, fue creada en 1981 como organismo operativo de la Junta Municipal de Defensa Civil y se le asignó el número 103 como sistema único de atención a emergencias de la ciudad. Este miércoles 14 de julio se cumplen 40 años de su conformación. El objetivo que motivó su origen es bien definido: dar la primera respuesta ante situaciones de peligro, emergencia o desastre en la ciudad, con la finalidad de organizar y coordinar el operativo de intervención.

Cintia Gauna, directora de Gestión de Riesgos del municipio, expresó que “es una gran alegría llegar a este 40 aniversario de una institución tan querida por la sociedad santafesina como es el Cobem, que fue creado en el año ‘81 y fue un organismo que por distintas situaciones viene siendo postergado en años anteriores”.

En ese contexto, la funcionaria recordó que antes de comenzar la actual gestión presentó ante el intendente Emilio Jatón un Programa de Fortalecimiento del Cobem que persigue entre sus objetivos instruir al personal y profesionalizar su formación, asignándole un perfil orientado a la reducción de riesgo de desastres, con un rol protagónico en las medidas de prevención y mitigación; aumentar la capacidad operativa mediante una refuncionalización del organismo, incrementando progresivamente la cantidad de agentes y adecuando el perfil. También mejorar las condiciones de los espacios físicos asignados para su funcionamiento, adecuándolos a las necesidades del servicio; y optimizar el recurso logístico en general, priorizando elementos de protección personal, unidades móviles y herramientas, entre otros.

![](https://assets.3dnoticias.com.ar/cobem1.jpg)

![](https://assets.3dnoticias.com.ar/cobem.jpg)

**Fortalecer el equipo en pandemia**

Este plan, que sigue en marcha, se vio atravesado por la pandemia por Covid-19. Como parte de un primer abordaje en la emergencia sanitaria mundial, en el plano local el Cobem tuvo a cargo la realización de testeos en la Terminal de Ómnibus Manuel Belgrano, a pasos de su sede central, a viajeros que llegaban desde otras localidades.

Desde el año pasado a la fecha, hizo 28 mil test de anomia en toda la ciudad, colaboró en traslados sociales y en actividades municipales como parte de las acciones en las que participó la entidad para contrarrestar el impacto de la pandemia.

“En la pandemia todo se complejizó; el tener menos recursos humanos en las distintas áreas fue realmente un desafío y pudimos lograrlo, pudimos generar una red para potenciar a las personas que estuvieron trabajando, una de las aristas del programa de fortalecimiento con el recurso humano. En la pandemia, quien estuvo y está dando respuesta a las emergencias de la ciudad es el Cobem. Entonces, para mí es un logro enorme llegar a esta instancia, a este festejo. El Cobem habla por sí solo de todo lo que ha sido y lo que representa para la ciudad y para nosotros es un orgullo festejar estos 40 años”, destacó Gauna.

Como parte de las actividades por el 40°, el intendente Emilio Jatón hará entrega de una nueva unidad con equipamiento para el organismo en un acto que se realizará el próximo viernes.

![](https://assets.3dnoticias.com.ar/cobem2.jpg)

**Sentido de pertenencia**

Claudia Antunez trabaja desde 2008 en Cobem como recepcionista de llamadas que provienen de la línea 103: “Acá se aprende guardia por guardia. A la experiencia la vas juntando día a día, cada vez que se atiende el teléfono son experiencias diferentes”, cuenta y ejemplifica que “llaman personas que están solas y necesitan hablar. Me encanta escuchar a las personas”.

Una hija desesperada porque su padre no reacciona y llama para pedir ayuda. Un recuerdo que Claudia no se olvida más: “Cuando empecé a trabajar y me llamaban por una emergencia o algo malo que pasaba yo me ponía mal, quería largarme a llorar y no podía evitarlo, terminaba con la voz cortada hablando porque quería solucionarlo, me dolía lo que la persona me transmitía, me hacía sentir mal. Yo cortaba la llamada y pensaba que me tenía que concentrar para estar fuerte y darle seguridad a esa persona de que su problema iba a ser solucionado. Eso me llevó un tiempo poder manejarlo para que la persona se sienta segura y sepa que estaba llamando al lugar correcto”.  

También Claudia resalta que muchas veces se encuentran con escenarios impensados: “Mandamos la ambulancia confiando en lo que la persona te dijo y cuando van allá es otra cosa, pero trabajando en equipo todo se soluciona, le buscamos una solución. Me siento orgullosa de estar acá y pertenecer a este trabajo, de los compañeros que tengo porque al entrar acá no tenía experiencia y ellos colaboraron, me fueron enseñando. Lo que sé y lo que voy aprendiendo es gracias a ellos que me brindaron de corazón”.

Marcos Raúl Quintana hace 39 años que ingresó como operario, luego fue supervisor y a lo largo de los años pasó por distintas tareas, supo manejar tractores, camionetas de rescate, ambulancias, unidades de prevención, camiones cisternas como parte de su trabajo y hoy está a cargo de la dirección del Cobem luego del fallecimiento de Ramón Gamarra el año pasado. Él creció en su trabajo a la par del Cobem y repasa que “tuvimos servicios muy complejos, como cuando se pierde una vida, más cuando son criaturas, y a uno le afecta. Por eso, después de finalizar lo tratamos de charlar como para poder desahogarnos”. También, rescata que “lo más lindo es la gratitud de la gente”.

Una anécdota que Marcos no se olvida es la primera vez que participó de un parto en una exposición en el Parque Garay: “Eran cordobeses, estaban en una casita rodante y estábamos todos apretaditos. Teníamos todo teórico y cuando llegamos a los hechos fue algo que nunca nos había pasado. Ver un bebé recién nacido es algo que los parteros nomás lo saben. Cómo agarrar al bebé, se nos resbalaba, uno tiene miedo por ser tan frágil”, recuerda con una sonrisa. “El Cobem para mí es mi segunda familia. Si no estoy en casa, estoy en el Cobem”, dice y se autocorrige: ”Es parte de mi familia”.

![](https://assets.3dnoticias.com.ar/cobem5.jpg)

**Con vocación de servicio**

Daniel Ramos hace 26 años que trabaja en la brigada. Antes que compartir su historia personal como trabajador municipal pide permiso para contar que el servicio está siempre a disposición de la comunidad: “El 103 es gratuito y funciona las 24 horas del día. Nos ponemos a disposición ante emergencias y los pedidos de auxilio que nos llegan por radio. Automáticamente el servicio se pone en marcha a partir del llamado de un vecino, se puede activar el servicio de ambulancia y de rescate”, precisa sobre la dinámica y el funcionamiento de la brigada.

El Cobem está integrado por 74 trabajadores. Cada uno con su rol específico teje una red bien firme que permite que hace 40 años “estemos a disposición de la comunidad todos los días y donde se necesite”, define Ramos, que tiene a su cargo la coordinación de las unidades y la conducción de ambulancias. Él es un apasionado de lo que hace: “El Cobem es para mí no solo una fuente de trabajo sino una vocación de servicio, me gusta. Estoy en un lugar que me gusta. La ciudad nos enseña y es muy importante. Estamos capacitándonos. Estamos muchas veces de licencia y llamamos o queremos venir porque no nos podemos desenchufar, es adrenalina y es apasionante. Es muy lindo, muy buen trabajo. Sentir la sirena, sentir que alguien necesita, ya eso amerita a que estemos atentos a cualquier situación”.

![](https://assets.3dnoticias.com.ar/cobem4.jpg)