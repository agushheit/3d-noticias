---
category: Estado Real
date: 2021-03-31T07:10:47-03:00
thumbnail: https://assets.3dnoticias.com.ar/capacita.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: Se presentó el programa de formación laboral “Santa Fe Capacita”
title: Se presentó el programa de formación laboral “Santa Fe Capacita”
entradilla: Su objetivo es mejorar la empleabilidad de los santafesinos y santafesinas
  a través de propuestas presenciales y virtuales.

---
El programa de formación laboral “Santa Fe Capacita” fue presentado por el Ministerio de Trabajo, Empleo y Seguridad Social a través de una videoconferencia que reunió a representantes de municipalidades, comunas, sindicatos y organizaciones sociales de toda la provincia.

Se trata de una oferta de más de 30 cursos diseñados junto a instituciones intermedias que brinda herramientas para los santafesinos que buscan un nuevo empleo o ya se encuentran trabajando. La edición 2021 incluye trayectos formativos en modalidad presencial y virtual.

Durante la actividad, el ministro de Trabajo, Empleo y Seguridad Social, Juan Manuel Pusineri, indicó que estas acciones “tienden a generar empleo de calidad, trabajo digno, que es el que habilita mayor estabilidad y crecimiento de la economía y desarrollo de los trabajadores y de la comunidad.”

Mientras, la directora provincial de Capacitación y Formación Laboral, Valeria March, destacó el trabajo coordinado que se está llevando adelante entre distintos niveles del estado y otros actores sociales: “Hoy estamos presentando el resultado de varios meses de trabajo mancomunado con los sindicatos, las instituciones intermedias y Municipios y Comunas, para dar respuestas concretas a las necesidades de la población”.

La funcionaria explicó que este año el programa “se enfoca en líneas de acción como economía del cuidado, empleos verdes, Industria 4.0, trabajo rural y también oficios tradicionales. Todas estas líneas las vamos a trabajar de manera transversal, articulando con los ministerios de Producción, Desarrollo Social, Educación y Medio Ambiente.”

Finalmente, el secretario de Coordinación y Gestión, Juan González Utges, remarcó que estas herramientas apuntan a mejorar la inserción real de las personas en el mundo del trabajo. Para ello se trabajó en base a un diagnóstico que permitió identificar áreas de vacancia y de demanda de posibles empleos.

**PARTICIPANTES**

En el encuentro virtual comentaron sus experiencias como participantes institucionales Norberto Heyaca (secretario general de la Asociación Gremial de Docentes de la Universidad Tecnológica Nacional -FAGDUT-), Gonzalo Goyechea (presidente comunal de María Teresa) y Susana Olive (de la Cooperativa Mujeres Solidarias).

La oferta de cursos, así como los links de inscripción pueden consultarse en www.santafe.gob.ar/trabajo.

[Oferta de cursos](https://www.santafe.gov.ar/noticias/recursos/documentos/2021/03/2021-03-30NID_270479O_1.pdf)