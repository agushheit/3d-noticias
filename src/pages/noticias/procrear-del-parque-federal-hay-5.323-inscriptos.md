---
category: La Ciudad
date: 2021-10-13T06:15:34-03:00
thumbnail: https://assets.3dnoticias.com.ar/PROCREFIN.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Procrear del Parque Federal: hay 5.323 inscriptos'
title: 'Procrear del Parque Federal: hay 5.323 inscriptos'
entradilla: Ahora comienza el análisis de cumplimiento de requisitos, proceso que
  definirá la lista final de participantes. El sorteo está previsto que se desarrolle
  el lunes 18.

---
Este martes a las 10 finalizó el plazo de inscripción para el Procrear del Parque Federal. Fuentes del Ministerio de Desarrollo Territorial y Hábitat de la Nación confirmaron a UNO que fueron un total de 5.323 los inscriptos en esta ocasión.

Desde la cartera nacional advirtieron que se trata de un listado que es provisorio. Es que ahora comienza el proceso de entrecruzamientos de datos y cumplimiento de requisitos que definirá la lista final de participantes que podrán pasar a la instancia del sorteo que en principio se desarrollará el lunes 18.

Se trata de santafesinos, que viven y tienen domicilio en esta capital, y que sueñan con ser adjudicatarios de una de las 199 viviendas que se están construyendo en dicho espacio verde, centro geográfico de la ciudad. Hay 15 viviendas que corresponden a la primera torre y 184 que se encuentran en la segunda.

Según informaron desde la Secretaría de Hábitat, Urbanismo y Vivienda del gobierno de la provincia a UNO, en el sorteo no se van a adjudicar el total de las viviendas. Si bien se aguardan definiciones de Nación, en Santa Fe prevén que en primera instancia se realice el sorteo de las 15 unidades habitacionales de la torre I (que ya estarían en condiciones de ser entregadas) y 22 de la torre II que se encuentra en construcción.

La idea del gobierno es que a medida que estén habitables se vayan adjudicando y entregando los departamentos. En ese sentido, la fuente consultada por este medio describió que en la torre donde se construyen los 184 departamentos, "en la medida se puedan habilitar y no obstaculice al resto se van a ir entregando".

Añadió que "la obra comenzó el mes pasado y que el objetivo es que en 18 meses este concluida".

Cabe recordar que están dirigidas exclusivamente para vivienda única familiar y de ocupación permanente. En total, en el Parque Federal, está revisto que se construyan 368 viviendas. Sus dimensiones son variables y van de 76 a 102 metros cuadrados. El complejo posee departamentos de 1, 2 y 3 dormitorios.

**Repaso de requisitos**

• No haber resultado beneficiado/a con planes de vivienda en los últimos diez (10) años.

• No tener, tanto el/la titular, como el/la cotitular, bienes inmuebles registrados a su nombre, ni como propietarios ni como co-propietarios, al momento de iniciar el proceso de inscripción. Con excepción de los casos detallados en las Bases y Condiciones.

• Contar con el Documento Nacional de Identidad vigente (DNI o Libreta de Enrolamiento o Libreta Cívica) de todos los integrantes de la solicitud al momento de iniciar el trámite.

• Ser argentinos/as o extranjeros/as con residencia permanente en el país.

• Tener entre 18 y 64 años al momento de completar la inscripción.

• Acreditar el estado civil declarado en el formulario de inscripción, excepto el estado civil soltero.

• Presentar Certificado de Discapacidad en caso de corresponder.

• Demostrar ingresos netos mensuales del grupo familiar conviviente –tanto el/la solicitante como su cónyuge o pareja conviviente– entre dos y ocho salarios mínimos vital y móvil (SMVyM). Los ingresos mínimos necesarios para acceder a una determinada vivienda podrán variar según el predio y la tipología seleccionada. De acuerdo a la información declarada en el formulario, se indicará si existen o no tipologías disponibles.

• Demostrar, como mínimo, doce (12) meses de continuidad laboral registrada.

• No registrar antecedentes negativos en el Sistema Financiero durante los últimos doce (12) meses por falta de pago de obligaciones de todo tipo; no encontrarse inhabilitados por el BCRA o por Orden Judicial; no registrar juicios según informe de antecedentes comerciales en los últimos cinco (5) años; no registrar embargos; y no registrar peticiones o declaraciones de concurso o quiebra.

• Los y las participantes, al momento de completar el formulario de inscripción, podrán incluir solo un/a cotitular. Para el caso de los y las participantes de estado civil casado/a, el/la cónyuge, será considerado cotitular automáticamente.

• El/la titular y el/la cotitular deberán encontrarse unidos por alguno de los vínculos que se detallan a continuación, los cuales deberán encontrarse registrados.

• Matrimonio.

• Unión convivencial.

• Unión de hecho, siempre que coincidan los domicilios declarados por el/la titular y cotitular.

• Todas las notificaciones en el marco de las presentes bases y condiciones serán realizadas a la dirección de correo electrónico declarada por el/la participante en el formulario de inscripción.