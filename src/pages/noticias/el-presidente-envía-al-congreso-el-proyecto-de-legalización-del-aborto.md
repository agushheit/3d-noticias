---
layout: Noticia con imagen
author: "Fuente: NA"
resumen: Aborto Legal
category: Agenda Ciudadana
title: El Presidente envía al Congreso el proyecto de legalización del aborto
entradilla: Así lo confirmó Alberto Fernández, tras resaltar que "el Estado no
  debe desentenderse de ninguna de estas realidades".
date: 2020-11-18T14:03:17.924Z
thumbnail: https://assets.3dnoticias.com.ar/alberto-fernandez.jpeg
---
El presidente Alberto Fernández anunció este martes que enviará al Congreso dos proyectos de ley que incluyen la legalización de la Interrupción Voluntaria del Embarazo y el Plan de los 1.000 días.

A través de un mensaje difundido a través de las redes sociales, el mandatario aseguró que de esa manera cumple con el compromiso asumido durante la campaña electoral y al momento de asumir la primera magistratura.

Fernández aseguró al respecto que "**el Estado no debe desentenderse**" y argumentó: "**La legalización del aborto salva vidas de mujeres y preserva su capacidad productiva**".

En un mensaje grabado, el jefe de Estado llamó al "diálogo franco" y al "respeto" en su tratamiento, sobre todo ante los que piensan diferente y concluyó diciendo: "**Que sea ley**".

![](https://assets.3dnoticias.com.ar/tweet.jpg)

Ver en [Twitter](https://twitter.com/alferdez/status/1328775329398329344?s=20)

Fernández envió al Congreso de la Nación para su tratamiento, **dos proyectos de ley para que "todas las mujeres accedan al derecho a la salud integral**".

**El primero de ellos legaliza la interrupción voluntaria del embarazo y garantiza que el sistema de salud permita su realización en condiciones sanitarias que aseguren su salud y su vida.**

**El segundo, instituye el Programa de los Mil Días, con el objeto de fortalecer la atención integral de la salud de la mujer durante el embarazo y de sus hijos e hijas en los primeros años de vida.**

"Mi convicción, que siempre he expresado públicamente, es que el Estado acompañe a todas las personas gestantes en sus proyectos de maternidad. Pero también estoy convencido que es responsabilidad del Estado cuidar la vida y la salud de quienes deciden interrumpir su embarazo durante los primeros momentos de su desarrollo".

"La criminalización del aborto de nada ha servido. Solo ha permitido que los abortos ocurran clandestinamente en cifras preocupantes. Cada año se hospitalizan alrededor de 38.000 mujeres por abortos y desde la recuperación de la democracia murieron más de 3.000 mujeres por esa causa".

"**La legalización del aborto salva vidas de mujeres y preserva sus capacidades reproductivas, muchas veces afectadas por abortos inseguros. No aumenta la cantidad de abortos ni los promueve. Solo resuelve un problema que afecta a la salud pública**".