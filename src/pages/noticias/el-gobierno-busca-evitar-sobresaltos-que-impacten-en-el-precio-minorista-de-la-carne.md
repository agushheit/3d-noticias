---
category: Agenda Ciudadana
date: 2021-11-21T06:00:49-03:00
thumbnail: https://assets.3dnoticias.com.ar/CARNE.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: El Gobierno busca evitar sobresaltos que impacten en el precio minorista
  de la carne
title: El Gobierno busca evitar sobresaltos que impacten en el precio minorista de
  la carne
entradilla: A través de soluciones consensuadas con los principales actores del sector,
  el equipo económico busca herramientas que permitan moderar el impacto de la suba
  en los precios de la carne a nivel minorista.

---
El equipo económico salió a dejar en claro la decisión del Gobierno nacional de buscar herramientas, que permitan moderar el impacto de la suba de los valores de la hacienda vacuna en pie en los precios de la carne a nivel minorista, a través de soluciones consensuadas con los principales actores del sector.  
  
Así lo afirmaron fuentes oficiales al comunicar el acuerdo alcanzado el viernes para congelar los precios de la carne en las grandes cadenas, nucleadas en la Asociación de Supermercados Unidos (ASU) a lo largo del feriado largo.  
  
"La próxima semana continuarán las conversaciones con el objetivo de arribar a una solución consensuada que garantice la estabilidad del precio de las carnes, evitando así sobresaltos que afecten a los consumidores en el último tramo del año", indicaron las fuentes.  
El mensaje al sector privado se puso en marcha el jueves por la noche a través de una reunión que mantuvieron en el Palacio de Hacienda el ministro de Economía, Martín Guzmán, con sus pares de Desarrollo Productivo, Matías Kulfas; de Agricultura, Ganadería y Pesca, Julián Domínguez; y el secretario de Comercio Interior, Roberto Feletti.  
  
Desde el Ministerio de Economía dijeron que el objetivo del encuentro fue "asegurar la estabilidad de los precios de las carnes".  
  
**Salto en los precios de hacienda**

La luz de alerta se prendió a mediados de semana tras el salto registrado en los precios de la hacienda en pie en el Mercado de Liniers, entre el viernes de la semana pasada y el martes.  
  
Entre ambas jornadas, los precios de las principales categorías de consumo tuvieron un salto de entre 10% y 15%.  
  
Por ejemplo, el precio promedio del novillo pasó de $ 208,43 el kilo a $ 221,57, mientras que el novillito creció de $ 214,79 a $ 247,10 y la vaquillona de $ 208,46 a $ 237,13.  
  
Este incremento a nivel mayorista puede llegar a traducirse en un aumento de entre 20% y 25% en las carnicerías.  
  
Las explicaciones esgrimidas por los diferentes eslabones de la cadena cárnica respecto de los aumentos en el precio de la hacienda, consideran una multiplicidad de factores que impulsaron los aumentos.  
  
Uno de ellos es la merma en la oferta de hacienda "gorda" para faena por parte de los feedlots, que hace ya un tiempo, debido a los problemas para reponer animales en los corrales por cuestiones de rentabilidad y encarecimiento de la hacienda, llevaron a que hoy la ocupación de los mismos se ubique por debajo de lo normal (menos del 65%).  
  
A esto se suma una baja de oferta estacional de animales frente a un aumento de la demanda por fin de año, y cuestiones básicamente referidas a la inflación que produjeron un retraso en los valores de la hacienda.  
  
Medidas políticas para controlar el precio de la carne

Es por eso que desde el Gobierno mostraron preocupación respecto del potencial impacto y comenzaron a dialogar con algunos sectores, como por ejemplo el supermercadismo, para lograr un congelamiento que dure hasta el lunes.  
  
El Gobierno nacional desplegó a lo largo del último año y medio una serie de herramientas para intentar contener el precio de la carne, con diversos resultados.  
  
En primer lugar, alcanzó un acuerdo con el sector exportador para ofrecer en 1.000 bocas de expendio de cadenas de supermercados y cadenas de carnicerías, 6.000 toneladas mensuales de cortes populares a precios accesibles.  
  
Este acuerdo, suscripto en su mayoría con el Consorcio de Exportadores de Carne (ABC), seguirá vigente hasta fin de año.  
  
Sin embargo, al tomar en cuenta el acotado volumen mensual y que el precio del alimento no cedía en su incremento, el Gobierno nacional decidió en mayo cerrar las exportaciones de carne, para después establecer un esquema en el cual sólo se permitía exportar el 50% del volumen mensual respecto del año anterior, y prohibió despachar siete cortes populares hasta fin de año.  
  
Si bien la medida fue repudiada por el conjunto de la cadena, logró que durante cuatro meses consecutivos (de julio a octubre) se produzca una baja acumulada del 3%, y frene las subas que se venían dando de forma constante desde julio de 2020.