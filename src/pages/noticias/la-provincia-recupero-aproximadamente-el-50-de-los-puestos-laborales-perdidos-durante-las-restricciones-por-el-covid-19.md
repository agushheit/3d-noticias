---
category: Estado Real
date: 2021-01-26T09:58:51Z
thumbnail: https://assets.3dnoticias.com.ar/trabajo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno Santa Fe
resumen: La provincia recuperó aproximadamente el 50% de los puestos laborales perdidos
  durante las restricciones por el Covid-19
title: La provincia recuperó aproximadamente el 50% de los puestos laborales perdidos
  durante las restricciones por el Covid-19
entradilla: Así lo aseguró el ministro de Trabajo, Juan Manuel Pusineri, de acuerdo
  a los datos elaborados por el Indec para el tercer trimestre de 2020.

---
El Ministro de Trabajo de la provincia, Juan Manuel Pusineri, destacó este lunes que en el último semestre de 2020 se recuperaron, aproximadamente, el 50 por ciento de los empleos perdidos durante “el peor momento de las restricciones” por la pandemia de Covid-19.

El titular de la cartera laboral mencionó que en ese período “se perdieron estimativamente 40.000 puestos de trabajo”, entre los que se incluyen unos 15.000 puestos de empleo registrado, “pese a la prohibición de despidos y a la doble indemnización”, además de “trabajadores informales y monotributistas”, en tanto que hubo unos “200.000 trabajadores con reducciones salariales”. 

Pusineri expresó que durante 2020, respecto del empleo, “el gobierno tuvo una actitud defensiva, porque se abocó a armar los protocolos sanitarios para permitir el regreso de la actividad en cada establecimiento, e intervino en numerosos conflictos por despidos y suspensiones, tratando de morigerar, con las herramientas que tenía, el impacto de la crisis económica derivada de la pandemia”.

En ese sentido, recordó que “Santa Fe fue una de las primeras provincias, sino la primera, que elaboró un protocolo para industrias y lo puso en funcionamiento”.

**RECUPERACIÓN**

El ministro de Trabajo informó que, en base a informes del Instituto Provincial de Estadísticas y Censos (Indec), en el tercer trimestre de 2020 “se empieza a notar una recuperación”. Esos datos “indican que el empleo a partir de septiembre comienza a subir todos los meses”; y a la espera de los datos del Indec del cuarto trimestre, el funcionario prevé que esa tendencia se mantendrá, con números que “van a seguir mostrando una mejora en lo que hace al empleo y a la actividad”. 

“De hecho, en algunos sectores como la industria y la construcción -que en diciembre de 2020 presentaron mejores números que en diciembre de 2019-, ya se registra un crecimiento que supera los índices prepandemia”, agregó Pusineri, quien no obstante remarcó que hay que aguardar la publicación del Indec, ya que “hay un segmento de trabajadores informales y monotributistas que son los primeros afectados, ya que en el sector formal rige la prohibición de despidos”.

**CREACIÓN DE EMPLEO**

Finalmente, el ministro aseguró que “para seguir recuperando empleo todavía está faltando que la pandemia, las estrategias del gobierno y los cuidados de la sociedad permitan una mayor normalidad en las actividades económicas”.

También aseguró que “la meta del gobierno provincial en materia de trabajo, que durante 2020 quedó pospuesta, es trabajar y facilitar los mecanismos que ayuden a la creación de empleo. El desafío es que en este proceso, que ojala la pandemia no lo interrumpa o no tengamos episodios excepcionales, es que el Estado acompañe con herramientas concretas que estimulen la creación de puestos de trabajo”, concluyó Pusineri.