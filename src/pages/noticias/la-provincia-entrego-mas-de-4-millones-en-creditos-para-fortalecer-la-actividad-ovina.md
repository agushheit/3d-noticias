---
category: Estado Real
date: 2021-09-14T06:00:10-03:00
thumbnail: https://assets.3dnoticias.com.ar/ACTOVINA.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La provincia entregó más de $4 millones en créditos para fortalecer la actividad
  ovina
title: La provincia entregó más de $4 millones en créditos para fortalecer la actividad
  ovina
entradilla: El financiamiento se realiza a través de la ley Ovina nacional que prevé
  la asistencia a pequeños y medianos productores, con el objetivo de promover el
  desarrollo de la actividad.

---
En el marco de la ley Ovina nacional, el Ministerio de Producción, Ciencia y Tecnología de la provincia, a través de la secretaría de Agroalimentos, entregó 10 créditos, por un total de 4,1 millones de pesos, a productores ovinos con el fin de fortalecer la actividad en el territorio santafesino.

El proceso comenzó este viernes con la entrega de estos aportes reintegrables (AR) y culminará próximamente con la de otros dos, provenientes del Fondo Fiduciario para la Recuperación de la Actividad Ovina (Frao,) ley Ovina 25.422. Los recursos serán utilizados para adquirir insumos; infraestructura necesaria para desarrollar la actividad; y animales, tanto vientres como machos.

En esta oportunidad, los beneficiarios son pequeños productores de las localidades de Alcorta, Luis Palacios, Avellaneda, Las Bandurrias, Colonia Bigand, Hugentobler, Tortugas, Toba, Villa Minetti y El Arazá.

La ley Ovina nacional previó para nuestra provincia un fondo total de 5,2 millones de pesos en 2021, con una tasa del 0% a valor nominal, que pueden ser devueltos mediante una cuota anual durante cinco años y con un periodo de dos años de gracia (plazo máximo de devolución: 7 años). El monto máximo a financiar por proyecto productivo será de hasta 500 mil pesos.