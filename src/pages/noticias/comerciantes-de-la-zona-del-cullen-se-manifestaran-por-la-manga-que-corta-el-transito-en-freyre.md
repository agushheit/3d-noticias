---
category: La Ciudad
date: 2021-08-30T06:15:40-03:00
thumbnail: https://assets.3dnoticias.com.ar/MANGA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Comerciantes de la zona del Cullen se manifestarán por la manga que corta
  el tránsito en Freyre
title: Comerciantes de la zona del Cullen se manifestarán por la manga que corta el
  tránsito en Freyre
entradilla: Expresan que las ventas en los comercios bajaron hasta un 50 por ciento
  por "la escasa circulación". Comerciantes se manifestarán este lunes detrás del
  Cullen.

---
Este lunes a las 14, comerciantes de avenida Freyre, de la zona del hospital Cullen y vecinos del lugar, se manifestarán detrás del nosocomio para pedir el levantamiento de la manga que une el Cullen con el hospital de campaña, ubicado en el Liceo Militar, y de ese modo liberar el tránsito en la zona.

"Nos vamos a manifestar para mostrar la situación y luego presentar una nota a las autoridades del hospital Cullen. Pedimos que retiren la manga que corta el tránsito sobre avenida Freyre porque nos está afectando en lo comercial a todos los que vivimos en la zona del comercio", sostuvo, Fernanda, referente de los comerciantes de la zona.

"Cortaron el tránsito a principios de mayo y ya llevamos 4 meses que no circulan autos por la zona ni transeúntes, tanto al sur como al norte de la manga que corta el tránsito en avenida Freyre entre Salta y Lisandro de la Torre", subrayó la comerciante.

El hospital de campaña se instaló a fines de abril en el Liceo Militar General Belgrano con una "manga" conectora entre el Hospital Cullen que cortó el tránsito de la Avenida Freyre.

"Tenemos firmas de muchísimos comercios de Freyre, ya que es una avenida que la gente no toma porque sabe que está cortada", sostuvo Fernanda y continuó agregando: "Somas más de 50 los comerciantes de avenida Freyre y calle Mendoza que vimos afectadas nuestras ventas en una merma de casi 50 por ciento".

"Los alquileres en el área son elevados y ya muchos comerciantes cerraron las puertas de sus locales porque el movimiento de personas es mínimo desde hace 4 meses", finalizó Fernanda, comerciante de la zona.