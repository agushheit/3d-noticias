---
category: La Ciudad
date: 2021-02-25T18:00:36-03:00
thumbnail: https://assets.3dnoticias.com.ar/boleto.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Aclaran que la estructura puesta fuera de El Molino Fabrica Cultural estará
  solo 60 días
title: Aclaran que la estructura puesta fuera de El Molino Fabrica Cultural estará
  solo 60 días
entradilla: El gobierno provincial dio a conocer un comunicado donde explica las razones
  del conteiner para promocionar el Boleto Estudiantil Gratuito que ubicaron en Boulevard
  Gálvez 2350.

---
Este martes la Facultad de Arquitectura, Diseño y Urbanismo (FADU) de la Universidad Nacional del Litoral expresó en sus redes sociales oficiales un _“contundente rechazo a las obras que el Gobierno Provincial ha realizado frente a El Molino Fábrica Cultural"_, ubicado en Boulevard Gálvez 2350. Argumentaron que con la colocación de un conteiner azul que tiene como fin promocionar el Boleto Estudiantil Gratuito (BEG) "agreden a un proyecto arquitectónico" y llamaron a "proteger el Patrimonio" de la ciudad.

Este miércoles, el gobierno provincial emitió un comunicado oficial en el que se explica que se trata de una estructura desmontable "que va a permanecer allí por 60 días, y se construyó sin realizar ninguna intervención en el edificio histórico".

“_Desde el área que trabaja en la implementación del Boleto Educativo Gratuito informamos que la instalación de una estructura en El Molino Franchino de la ciudad de Santa Fe se fundamenta en la necesidad de ejecutar tareas extraordinarias de promoción y difusión del programa Boleto Educativo Gratuito_", expresaron desde prensa de Gobierno.