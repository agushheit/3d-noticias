---
category: Agenda Ciudadana
date: 2021-03-22T06:15:27-03:00
thumbnail: https://assets.3dnoticias.com.ar/prestamos.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Créditos para la vivienda: detalle de los préstamos para la construcción
  que el Gobierno anunciará en las próximas semanas'
title: 'Créditos para la vivienda: detalle de los préstamos para la construcción que
  el Gobierno anunciará en las próximas semanas'
entradilla: Según explicó el ministro de Desarrollo Territorial y Hábitat, Jorge Ferraresi,
  se está elaborando un nuevo plan de préstamos hipotecarios, “no para comprar, sino
  para construir”.

---
El Gobierno prepara el anuncio en las próximas semanas de un nuevo sistema de créditos hipotecarios que abandonará el esquema de actualización UVA indexado a la inflación y que comenzará a seguir la variación de los salarios. Los nuevos préstamos estarían disponibles para la construcción de viviendas, en lugar de para la compra directa de una propiedad.

Así lo aseguró en declaraciones radiales el ministro de Desarrollo Territorial y Hábitat, Jorge Ferraresi, que “en las próximas semanas” el presidente Alberto Fernández será el encargado de presentar el nuevo sistema. “Queremos que todos tengan acceso al derecho de la casa propia”, expresó.

“Nosotros ya estamos trabajando con el Banco Hipotecario en el plan Procrear, que lleva adelante las acciones financieras y de administración. Corresponde al Presidente anunciarlo, pero los nuevos créditos van a ser no para comprar (propiedades) sino para construir”, dijo el ex intendente de Avellaneda y que ese sistema nuevo tendrá “una fórmula atada al salario”.

El ministro de Desarrollo Territorial y Hábitat, Jorge Ferraresi adelantó que en las próximas semanas se anunciará un nuevo sistema de créditos hipotecarios que abandonará el esquema de actualización UVA

En octubre, el Poder Ejecutivo ya había enviado al Congreso un paquete de proyectos de ley para impulsar la actividad de la construcción compuesto por beneficios tributarios, un blanqueo de capitales para desarrollos inmobiliarios y la creación de una Sociedad Hipotecaria que fomente nuevos préstamos para la vivienda, que no pasó por el parlamento.

En ese momento, se explicó que los nuevos créditos -presentados por el Ministerio de Economía- iban a estar disponibles, se preveía, para la construcción de primera o segunda vivienda sobre terreno propio o terreno a adquirir. El monto para construcción sería de hasta el 100% del presupuesto de obra. Mientras que el monto para compra de terreno será hasta el 80% de la tasación o precio de venta del terreno, únicamente para la construcción de primera vivienda.

También estarían incluidos los cambios de primera o segunda vivienda por una a estrenar (primera escritura). En ese caso, el monto para la compra contemplaría hasta el 80% de la tasación o precio de venta de la vivienda. En tanto, el monto para cambio sería de hasta el 100% de la diferencia entre el valor de la vivienda que se adquiere y el valor del inmueble que se reemplaza.

Por otra parte, se esperaba que también contemplen la refacción, ampliación o terminación de primera o segunda vivienda. El monto del préstamo en este caso contemplaría hasta el 100% del presupuesto de obra. Por último, estaba previsto que también fuera incluida la adquisición o cambio de vivienda única de ocupación permanente. Monto para adquisición: hasta el 80% de la tasación o precio de venta de la vivienda.

De acuerdo a las palabras del ministro Ferraresi, el anuncio que hará Alberto Fernández en las próximas semanas estaría centrado en aquellas personas que busquen construir en un terreno que ya sea de su propiedad, en lugar de la adquisición directa de un inmueble.

En octubre, cuando fueron presentados por el Ministerio de Economía, los nuevos créditos iban a estar disponibles, se preveía, para la construcción de primera o segunda vivienda sobre terreno propio o terreno a adquirir.

El cambio central de los créditos que presentará el Gobierno con respecto a los UVA está relacionado al sistema de actualización de las cuotas. Tras la experiencia de la indexación por inflación, la iniciativa propondría ahora un ajuste que siga al nivel de variación de los salarios registrados.

Para cubrir ese desfasaje entre la actualización UVA (que es la preferencia de los bancos acreedores) y del nuevo sistema por salarios es que el Gobierno además había anunciado la creación de una Sociedad Hipotecaria que administre un Fondo Fiduciario de Cobertura y Promoción (FFCP).

De acuerdo al anuncio de ese momento, el Fondo Fiduciario sería solventado por una porción de la cuota del crédito hipotecario, aportes de las entidades, la misma rentabilidad que obtenga el fondo y un aporte inicial del Tesoro Nacional.

El objetivo del fondo sería intercambiar flujos ajustados en base al coeficiente de variación salarial (CVS), que se ajustan más a las preferencias de deudores, con otros ajustados en base a los índices de precios y construcción (CER/ICC), preferidos por bancos e inversores.

La integración del fondo se haría a través de un aporte activo de los bancos que participen del mercado de crédito hipotecario, con entre 1% y 5% del valor residual de cada crédito hipotecario cubierto; un aporte trimestral de los bancos que participen de hasta un máximo del 1,25% de la rentabilidad de los pasivos remunerados del BCRA del trimestre; y una prima contenida en la cuota que deberá pagar el deudor hipotecario.

El cambio central de los créditos que presentará el Gobierno con respecto a los UVA está relacionado al sistema de actualización de las cuotas. Tras la experiencia de la indexación por inflación, la iniciativa propondría ahora un ajuste que siga al nivel de variación de los salarios registrados.

Ferraresi denunció que durante el Gobierno de Mauricio Macri se dejaron de construir viviendas y se paralizaron las que estaban en construcción. “Es perverso que el macrismo haya dejado sin construir 55 mil viviendas”, dijo el funcionario. Según cálculos oficiales, en la actualidad hay un déficit habitacional que ronda los 2 millones de viviendas.

Agregó que “en la reconstrucción de la Argentina” el derecho a la vivienda debe ser una política de Estado. “Trabajamos en beneficio de la igualdad y el ascenso social para que cada argentino tenga su propia vivienda”, remarcó Ferraresi y dijo que el Gobierno nacional espera construir 260.000 casas en los próximos tres años.