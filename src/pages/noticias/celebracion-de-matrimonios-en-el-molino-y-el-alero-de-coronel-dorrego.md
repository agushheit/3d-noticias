---
category: La Ciudad
date: 2020-12-18T11:50:54Z
thumbnail: https://assets.3dnoticias.com.ar/1812molino.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Celebración de matrimonios en El Molino y El Alero de Coronel Dorrego
title: Celebración de matrimonios en El Molino y El Alero de Coronel Dorrego
entradilla: Los espacios culturales de la capital provincial comenzarán a recibir
  ceremonias civiles todos los viernes. Bajo los protocolos sanitarios, se realizarán
  hasta 60 casamientos por mes.

---
A partir de enero, las parejas que deseen contraer matrimonio civil en la ciudad de Santa Fe podrán hacerlo en dos de los espacios que el Ministerio de Cultura de la provincia gestiona en la capital: **El Molino, Fábrica Cultural** (Bv. Gálvez y República de Siria) y **El Alero de Coronel Dorrego** (Bv. French y Sarmiento).

Así lo anunciaron este jueves en una conferencia de prensa el ministro de Cultura, Jorge Llonch, el subsecretario de Registros, Francisco Dallo, la directora del Registro Civil, Luisina Giovanini, y la directora de Programas Socioculturales, Mariana Escobar.

El titular de Cultura expresó que, «así como **esta propuesta ya comenzó a implementarse en Rosario en el Galpón 13 y el Petit salón de la Plataforma Lavarden**, y ahora en la capital, la idea es llevar las celebraciones a espacios de toda la provincia».

Llonch, asimismo, agregó: «Queremos abrir estos lugares hermosos para las y los ciudadanos de Santa Fe; las celebraciones van a tener música también, así que vamos a poder dar trabajo a músicos locales; creo que es una actividad fantástica».

Por su parte, Dallo, en el encuentro que tuvo lugar en El Café Literario del Molino, indicó: «Seguimos generando nuevos espacios para celebrar matrimonios, trabajando codo a codo con el Ministerio de Cultura. La idea desde la Secretaría de Justicia es afrontar esta situación complicada que vivimos actualmente con nuevas propuestas, con desafíos como digitalizar, conectar, y utilizar espacios tan lindos como El Molino y otros que tiene Cultura para celebrar estas uniones».

La directora del Registro Civil, en tanto, explicó: «El turno se puede solicitar a través de la [web de gobierno](https://www.santafe.gov.ar/ "web")  y, una vez a la semana, los viernes, estaremos dando la autorización para que puedan contraer matrimonio en **El Molino y en El Alero de Coronel Dorrego**».

Respecto de la propuesta, Giovanini señaló: «Buscamos descentralizar la actividad y que todos tengan acceso a poder celebrar un evento familiar con más asistencia de público, mejor cuidados, en espacios lindos, siempre siguiendo la decisión del gobernador de abrirnos a la comunidad en este contexto tan difícil, dando respuestas y con un fuerte apoyo del ministro Llonch y de Mariana».

Para finalizar, la directora de Programas Socioculturales, Mariana Escobar, señaló: «Durante este tiempo en los espacios culturales, además de acompañar la pandemia, estamos trabajando para que, con la seguridad que la situación sanitaria nos exige, podamos abrir al público y volver a encontrarnos en la presencialidad prontamente».

***

[![Celebración de Matrimonios](https://assets.3dnoticias.com.ar/1812molino1.webp)](https://assets.3dnoticias.com.ar/matrimonios-en-espacios-culturales.mp4 "Reproducir")

