---
category: Agenda Ciudadana
date: 2022-01-03T06:00:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/cortesupremadepollo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Jueces y abogados pidieron una nueva ley para integrar el Consejo de la Magistratura
title: Jueces y abogados pidieron una nueva ley para integrar el Consejo de la Magistratura
entradilla: "El reclamo surgió tras la reunión que mantuvo el Consejo con las autoridades
  de la Federación Argentina de Colegios de Abogados, el Colegio Público de Abogados
  de la Capital Federal y la Asociación de Magistrados.\n\n"

---
Los estamentos de jueces y abogados advirtieron que el plazo de 120 días que la Corte Suprema fijó para que el Consejo de la Magistratura se amplíe a 20 miembros es "imposible de cumplir", por lo que solicitaron al Ejecutivo y al Congreso que impulsen una nueva ley para cumplir con ese requerimiento, mientras que la Casa Rosada ya decidió que incluirá un proyecto referido al Consejo en las sesiones extraordinarias que convocará a mediados de este mes.  
  
Los pedidos de una nueva ley surgieron tras la reunión que, seis días después del fallo, mantuvo el Consejo con las autoridades de la Federación Argentina de Colegios de Abogados (FACA), el Colegio Público de Abogados de la Capital Federal (CPACF) y la Asociación de Magistrados y Funcionarios de la Justicia Nacional (AMFJN), entidades que agrupan a magistrados y a profesionales del Derecho que ejercen en CABA.  
  
El vicepresidente del Consejo, el juez Alberto Lugones, se sumó a las advertencias sobre la imposibilidad de cumplir con el lapso fijado por la Corte, al plantear en declaraciones radiales que se trata de "un plazo que no tiene mucho sentido" y cuestionar que se haya dejado pasar mucho tiempo cuando "ahora hay que apagar el incendio en 120 días".  
  
La decisión del máximo tribunal implica que el Consejo debe retornar a su composición anterior, por lo que deberá ampliarse de sus 13 miembros actuales a 20 integrantes, que se distribuirán entre cinco jueces (uno de ellos, el titular de la Corte, este domingo el santafesino Horacio Rosatti), cuatro diputados, cuatro senadores, cuatro abogados, dos académicos y un representante del Ejecutivo.  
  
También se pronunció sobre lo imposible de cumplir con el plazo de 120 días el titular del Colegio Público de Abogados (CPACF) Eduardo Awad, quien advirtió sobre los inconvenientes que implicaría para la entidad convocar a elecciones para que sus 60.000 afiliados elijan a sus representantes para el Consejo, por lo cual debería hacerlo en medio de la pandemia y con los padrones desactualizados.  
  
Las críticas se concentran en el plazo, ya que los representantes de los abogados y los jueces que se pronunciaron sobre el tema coincidieron con el criterio que fijó la Corte, de aumentar el número de miembros para evitar el predominio del estamento político por sobre jueces, abogados y académicos.  
  
El propio Consejo de la Magistratura, que encabeza el abogado, docente universitario y rector de la Universidad de Lomas de Zamora Diego Molea, le pidió al presidente Alberto Fernández y a los titulares del Senado y la Cámara de Diputados, Cristina Fernández de Kirchner y Sergio Massa, la "pronta sanción" de una ley de integración y organización del organismo que cumpla con el fallo dictado por la Corte.  
  
En sendos oficios emitidos al Poder Ejecutivo y a las dos cámaras del Poder Legislativo con fecha del 29 de diciembre, Molea fundamentó su pedido en el "exiguo lapso que resta hasta la expiración del plazo de 120 días dispuesto por la referida resolución judicial para procurar la integración completa de este cuerpo en los términos de la ley 24.937, y la posibilidad que vencido ese término sin lograrse la composición plena, pueda generarse una paralización de las actividades".  
  
Molea señaló también que resulta "necesaria la convocatoria a sesiones extraordinarias", por lo que le solicitó al Presidente que "tenga a bien contemplar la posibilidad de ejercer la facultad que le acuerda el artículo 99, inciso 9°, de la Constitución Nacional".  
  
La actual composición del órgano que selecciona a los jueces y analiza su conducta, vigente desde 2006, reúne seis legisladores (tres por cada Cámara, dos de la mayoría y uno de la minoría), tres jueces, dos abogados, un académico y un representante del Poder Ejecutivo Nacional, por lo que suma 13 miembros.  
  
El fallo que la Corte dictó el 16 de diciembre, con los votos del presidente Horacio Rosatti, el vice Carlos Rosenkrantz y el ministro Juan Carlos Maqueda más la disidencia parcial de Ricardo Lorenzetti, declaró inconstitucional la ley 26080, sancionada en febrero de 2006, que redujo los miembros del Consejo de la Magistratura de 20 a 13, encargó al Congreso una nueva ley que lo regule y concedió un plazo de 120 días corridos para que se elijan a los nuevos miembros.  
  
Por unanimidad, el máximo tribunal definió, 15 años después de la sanción de la ley, que la composición no respetaba el "equilibrio" de los sectores que seleccionan, sancionan y remueven magistrados, y dispuso que el Congreso dicte una normativa que lo regule y el Consejo vuelva a integrarse con un total de 20 miembros si la la ley no se sanciona antes de los 120 días corridos.  
  
Hace quince días, al conocerse el fallo de la Corte, el ministro de Justicia, Martín Soria, advirtió que su contenido ponía en riesgo "el funcionamiento del Poder Judicial, porque puede implicar la parálisis de las designaciones de jueces, de la administración del Poder Judicial y de las sanciones a jueces" y calificó la decisión del tribunal como "un golpe a las instituciones democráticas sin precedentes".  
  
A principios de diciembre, días antes de que el máximo tribunal resolviera la inconstitucionalidad de la ley 26080 que amplió a 20 el número de consejeros, el Gobierno anunció que enviaría al Congreso un proyecto de reforma del organismo que selecciona y analiza la conducta de los jueces: esa reforma preveía el aumento de 13 a 17 integrantes.  
  
La encargada de anunciarlo fue la portavoz del Ejecutivo, Gabriela Cerruti, quien entonces señaló que la iniciativa permitiría "una representación más equilibrada dentro del Consejo" y contribuiría a "recuperar la confianza de la sociedad en la Justicia, garantizar la independencia del Poder Judicial y el trabajo armónico" en el organismo.