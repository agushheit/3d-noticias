---
category: La Ciudad
date: 2021-07-16T06:00:40-03:00
thumbnail: https://assets.3dnoticias.com.ar/RODRIGUEZ.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Ocho alianzas electorales en Santa Fe
title: Ocho alianzas electorales en Santa Fe
entradilla: Fueron inscriptas ante la justicia electoral federal con asiento en esta
  capital. El cierre de listas vencerá el 24 del corriente

---
Ocho fueron los frentes o alianzas electorales inscriptas ante la secretaría electoral nacional de Santa Fe y ahora el juez federal con competencia electoral, Reinaldo Rodríguez deberá convalidarlas. El magistrado dio un plazo de 24 a tres de esas ententes fueron intimadas a modificar el nombre para evitar confusiones en el electorado. Es que tres de ellas llevan el nombre Santa Fe en el frente.

El proceso de alianzas que venció ayer antecede a la inscripción de precandidatos a senadores y diputados que se producirá el 24 del corriente.

**Las inscripciones concretadas fueron las siguientes**

**Frente Amplio Progresista**: conformado por Partido Socialista, Gen, Partido Demócrata Progresista, Pares, Libres del Sur e Igualdad y Participación.

**Frente de Izquierda y de Trabajadores - Unidad-:** lo integran el Partido Obrero, el Partido Socialista de los Trabajadores y la Izquierda de los Trabajadores.

**Juntos por el Cambio**: Pro, UCR, Coalición Cívica, Unir y Ucede.

**Primero Santa Fe:** alianza compuesta por PAIS, MId y el Partido Demócrata Cristiano.

**Hagamos Santa Fe**: Compromiso Federal, Partido Conservador Popular.

**Santa Fe nos Une**: integrado por el Partido Autonomista y Unión Libertad.

**Soberanía Popular**: patrocinado por Partido del Trabajo y del Pueblo y Liga de los Pueblos Libres.

**Frente de Todos:** encabezado por el Partido Justicialista más Partido Comunista, Partido Solidario, Partido Instrumento Electoral por la Unidad Popular; Fe, Partido Frente Renovador y Santafesino Cien por Ciento.