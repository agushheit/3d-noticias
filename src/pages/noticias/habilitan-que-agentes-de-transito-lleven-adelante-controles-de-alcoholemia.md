---
category: Agenda Ciudadana
date: 2022-06-28T08:08:06-03:00
thumbnail: https://assets.3dnoticias.com.ar/concejo2.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: Habilitan que agentes de tránsito lleven adelante controles de alcoholemia
title: Habilitan que agentes de tránsito lleven adelante controles de alcoholemia
entradilla: El Concejo aprobó  la  incorporación  de los agentes municipales de tránsito
  para realizar la comprobación de los niveles de alcoholemia y narcolemia.

---
Concejales y concejalas votaron a favor de la modificación de la Ordenanza N° 10.017 para que se incorpore a los agentes municipales de tránsito al trabajo que ya llevan adelante personal sanitario para la detección de niveles de alcoholemia y narcolemia, mediante la utilización de los dispositivos de detección.

Por su parte, el presidente del Concejo, Leandro González, puntualizó que “el Ejecutivo Municipal envío un mensaje para dar algunas herramientas más a la hora de llevar a cabo los controles de alcoholemia. Ahora habilitamos que agentes de tránsito, previas capacitaciones certificadas, puedan llevar adelante ese rol. Esto bajo la mirada de que una de las principales causas de siniestralidad tiene que ver con los accidentes de tránsito por alcoholemia, por lo que nos pareció oportuno avanzar por unanimidad con esta iniciativa”.

**Estudios a los fines de incorporar alómetros a los controles de alcoholemia**

A su vez, se aprobó una resolución que solicita al Ejecutivo Municipal proceda a realizar los estudios de factibilidad técnica y económica para incorporar alómetros a los controles de alcoholemia que se realizan en nuestra ciudad.

Al respecto,el autor de la iniciativa, concejal Sebastián Mastropaolo, destacó que “Los alómetros son dispositivos que permiten verificar en cuestión de segundos, si una persona tomó alcohol o no. Estos aparatos no miden la graduación de alcohol en sangre, pero arrojan un resultado positivo o negativo en cuanto a la ingesta de alcohol. Este resultado se alcanza tan solo en el lapso de un segundo y a través de la exhalación de aire. Solamente a aquellos conductores que den positivo en esta primera prueba, se les realiza el segundo test con alcoholímetro para detectar dosaje”.

Además comentó que “se trata de un aparato nuevo en donde no hay que apoyar la boca en la boquilla, sino que es suficiente con acercarlo a la persona para que detecte si hay presencia de alcohol, y obtener un resultado positivo o negativo”.