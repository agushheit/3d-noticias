---
category: Estado Real
date: 2021-03-11T06:19:32-03:00
thumbnail: https://assets.3dnoticias.com.ar/aportes.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: La provincia firmó convenios del Plan Incluir con comunas del departamento
  La Capital por más de 60 millones de pesos
title: " La provincia firmó convenios del Plan Incluir con comunas del departamento
  La Capital por más de 60 millones de pesos"
entradilla: Los montos para obras serán destinados a las localidades de Monte Vera,
  Llambi Campbell, Arroyo Aguiar, Campo Andino, Arroyo Leyes, Nelson y Emilia.

---
El ministro de Gestión Pública, Marcos Corach, encabezó este miércoles la rubrica de convenios en el marco del Plan Incluir con localidades del departamento La Capital.

El encuentro se llevó adelante en la Casa de Gobierno de Santa Fe y del mismo participaron los presidentes comunales de Monte Vera, Llambi Campbell, Arroyo Aguiar, Campo Andino, Arroyo Leyes, Nelson y Emilia; el asesor del Poder Ejecutivo, Rubén Michlig; el subsecretario de Comunas, Carlos Kaufmann; y el senador por el departamento La Capital, Marcos Castelló.

Luego del encuentro, Michlig afirmó que “en el proceso de transición e inicio de nuestra gestión, incluso fue uno de los motivos que generó la llamada ley de Necesidad Pública, señalábamos el problema de los municipios y comunas en cuanto a las dificultades de financiamiento, a los déficit que tenían y la imposibilidad de avanzar en políticas públicas”.

En dicho marco, el asesor del Ejecutivo observó que “a partir del año pasado y luego de un gran esfuerzo económico, se pudo poner al día la deuda que había con los gobiernos del interior por diferentes conceptos: coparticipación, obras menores y subsidios. Hoy, ya estamos en la puesta en marcha del programa Incluir que abarca a los 365 municipios y comunas con aportes que llegan a la gente y que posibilita a la población tener agua potable, acceso a los barrios, a la vivienda, y a nuevos desagües”.

En el mismo sentido, Kaufmann añadió: “Estos fondos son otorgados a pequeñas localidades del departamento La Capital de todos los signos políticos” al tiempo que explicó que “el plan Incluir que antes era el Abre y que abarcaba solo a un puñado de ciudades, fundamentalmente las grandes, por decisión del gobernador Omar Perotti y con los recursos disponibles, los reciben todos los pueblos de la provincia de Santa Fe”.

Por su parte, el presidente Comunal de Nelsón, Héctor Leiva, afirmó que “nos permite mejorar las condiciones sociales de los habitantes de la localidad. Nos llevamos la satisfacción de firmar este convenio y pronto recibir los aportes para hacer las obras que habíamos comprometido. Vamos a llevar adelante una senda iluminada que irá desde el Pueblo al Cementerio, brindará seguridad y estará parquizada”.

Además, Luis Alberto Pallero, presidente de la Comuna de Monte Vera, sostuvo que “es una satisfacción muy importante obtener este dinero para obras que siempre son necesarias. Agradecemos al gobernador que nos incluye porque Monte Vera dentro de poco será ciudad y necesitamos el apoyo del Estado”.

Por último, respecto a la obra que la localidad podrá solventar por medio del Plan Incluir, será un “desagüe que es necesario para varias manzanas de la Comuna que se inundan, poder sacar el agua hacía un desagüe primario hacia la Setúbal, por lo que esta obra dará a toda esa gente tranquilidad”.

**DETALLES DE LOS APORTES**

>> _Comuna de Monte Vera_: $ 3.912.360 para trabajos en el desagüe de calle Entre Ríos.

>> _Comuna de Llambi Campbell:_ $ 14.996.979 para cordón cuneta y pavimento articulado.

>> _Comuna de Arroyo Aguiar:_ $ 8.631.395 para la construcción de un nuevo Centro de Salud; y $6.544.646 para obras de pavimento.

>> _Comuna de Campo Andino:_ $ 2.298.026 para trabajos de construcción de cordón cuneta.

>> _Comuna de Arroyo Leyes_: $ 14.878.200 para levar adelante trabajos de cordón cuneta sobre calle 5 y 120.

>> _Comuna de Nelson:_ $ 3.762.610 para obras de senda peatonal.

>> _Comuna de Emilia_: $ 5.029.000 para trabajos en desagües pluviales y alumbrado público.