---
category: La Ciudad
date: 2020-12-28T11:32:36Z
thumbnail: https://assets.3dnoticias.com.ar/2812puente-colgante.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Diario UNO'
resumen: 'Puente Colgante peatonal y ciclista: piden que la propuesta se debata en
  el Concejo'
title: 'Puente Colgante peatonal y ciclista: piden que la propuesta se debata en el
  Concejo'
entradilla: El proyecto está presentado en el Concejo municipal. Desde la Mesa de
  Ciclismo Urbano levantan la voz para que se debata la propuesta hecha hace ya 5
  años.

---
Desde la Mesa de Ciclismo Urbano (MCU) se insiste en que la propuesta para hacer al Puente Colgante peatonal y ciclista se ponga en agenda en el Concejo municipal. La iniciativa sostiene puntualmente que se destine el Puente Colgante para tránsito exclusivo de peatones y ciclistas, cerrado para vehículos motorizados de forma permanente.

El proyecto ya fue presentado en el Concejo por el concejal Juan José Saleme, aunque desde organizaciones como Santa Fe en Bici manifiestan que luego de 5 años existe «mucha bronca» por la falta de atención que recibió la iniciativa. Además, sostienen que se está incurriendo desde el gobierno municipal a una «falta a promesas de campaña».

Las distintas organizaciones ciclistas de la ciudad manifestaron a UNO que la iniciativa «facilitaría la conexión con la actual ciclovía de Costanera Oeste y acceso desde Bv. Gálvez, como también con la bici senda que se implementó por debajo del Puente Oroño, que hoy es una zona muy insegura».

Además, desde la MCU sostienen que «es la única vía de paso para peatones y ciclistas que asisten a la Ciudad Universitaria, predio UNL, Costanera Este, Barrio el pozo, Alto Verde, Vuelta del Paraguayo, etc.»

***

![](https://assets.3dnoticias.com.ar/2812-bici-puente-colgante.webp)

***

**Santa Fe en Bici**, semanas atrás, pintó bicicletas en la calzada del Puente Colgante, con el objetivo de alertar a los automovilistas sobre el paso de ciclistas. Yamila Riego, referente de la organización, manifestó que «ante la ausencia del Estado es lo que estuvo a nuestro alcance y demandó un gran esfuerzo de trabajo e insumos».

«Hace 5 años en el Concejo Municipal Henrik Lundorff, un politólogo Danés experto en movilidad activa, pedía de parte de Santa Fe en Bici que el Puente Colgante sea destinado a ciclistas y peatones, tal como en Copenhague existe el puente serpiente. El mismo dejó a la vista que no necesitábamos de dicha estructura, porque ya teníamos un puente. Solo bastaba la voluntad política de llevarlo adelante», recalcó Riego.

Otras justificaciones en las que se basa la propuesta planteada por las organizaciones ciclistas santafesinas son: el uso del puente para hacer deportes corriendo o en bicicleta, imposibilidad de convivencia entre autos y bicicletas, uso del puente Oroño exclusivo para automóviles y potenciar el atractivo turístico del Puente Colgante mediante un uso más democrático.