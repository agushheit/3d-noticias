---
category: Agenda Ciudadana
date: 2021-04-03T06:00:09-03:00
thumbnail: https://assets.3dnoticias.com.ar/hidro.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Hidrovía: consideran que el Puerto Santa Fe "no debe estar ausente en la
  planificación"'
title: 'Hidrovía: consideran que el Puerto Santa Fe "no debe estar ausente en la planificación"'
entradilla: 'El municipio y la Mesa de Entidades Productivas presentaron un documento
  sobre su posicionamiento ante el Consejo Federal de la Hidrovía. '

---
El intendente de la ciudad de Santa Fe, Emilio Jatón, junto a los representantes del gobierno provincial, el representante del Ente Administrador del Puerto de Santa Fe (EPSF) y las organizaciones que forman parte de la Mesa de Entidades Productivas, presentaron un documento sobre su posicionamiento ante el Consejo Federal de la Hidrovía.

En la misiva sostienen varios puntos, entre ellos destacan que "el Puerto de la ciudad de Santa Fe tiene una importancia estratégica para el desarrollo del país y la región desde una mirada verdaderamente federal. Por su ubicación privilegiada, es facilitador del desarrollo de las ciudades del área metropolitana del gran Santa Fe, del centro-norte de la provincia de Santa Fe y de su área de influencia que abarca el centro-norte de las provincias de Entre Ríos, Córdoba, San Juan y La Rioja, y a provincias del Noreste y Noroeste argentino (NEA y NOA)", expresaron.

Además, piden que se le dé participación al municipio de Santa Fe como miembro activo del Consejo Federal de la Hidrovía y de sus comisiones: "Esto es s fundamental para fijar la posición de la ciudad en relación con la defensa del crecimiento y desarrollo del puerto local y la ciudad, tanto ante la histórica situación que reviste la futura licitación para el mantenimiento de las condiciones de navegabilidad de la Hidrovía Paraná-Paraguay, así como en relación a futuras discusiones que puedan darse en dicho ámbito".

Otro de los pedidos expuestos es que "las definiciones, debates y acuerdos que se generan en el marco de dicho Consejo Federal sientan las bases de la matriz logística nacional para los próximos 20/30 años, y las posibilidades de desarrollo del interior del país. Santa Fe, por su incidencia e historia no puede quedar al margen de dichas intervenciones".

**Segunda provincia más productiva**

En otro tramo de los argumentos, señalan en el documento que "Santa Fe es la segunda provincia más productiva del país, y el gobierno nacional viene sosteniendo una deuda histórica con el Puerto de la capital provincial, la cual se acrecienta ante la no invitación a participar del Consejo Federal. La voz de los intereses económicos y geopolíticos ante este ámbito es la municipalidad de Santa Fe en conjunto con el Ente Administrador del Puerto de Santa Fe y las fuerzas vivas locales, por lo que la participación activa en este marco y la posibilidad de introducir mejoras al puerto de Santa Fe en las condiciones de la nueva concesión permitirá, en el mediano plazo, generar variables sociales y económicas favorables para el desarrollo de nuestras sociedades y de nuestras economías regionales".

En este sentido, también exigen que "es absolutamente necesario equilibrar la provincia en materia portuaria, actualmente desbalanceada hacia el sur, poniendo énfasis en el desarrollo de los recursos e infraestructuras del centro-norte provincial, priorizando una mirada integral y complementaria de todos los puertos que la integran", y agregan que por razones históricas y geográficas, el puerto de la ciudad de Santa Fe, "por ser el último puerto de ultramar y punto de inflexión estratégico para el país por su vinculación con el Corredor Bioceánico Central Porto Alegre-Coquimbo, tiene que ser tenido en cuenta en las definiciones del nuevo contrato de concesión de la vía navegable más importante del país".

"La ciudad de Santa Fe -prosigue el texto- tiene las condiciones para constituirse en un nodo logístico estratégico, y debe bregarse por mejorar las condiciones de operación de su Puerto y su necesaria reconversión y/o ampliación sobre el cauce principal del río Paraná para que vuelva a ser un puerto activo, pujante y generador de oportunidades de desarrollo para toda la región".

**Solicitud**

El posicionamiento concluye con el pedido que hacen desde el gobierno local y la Mesa de Entidades Productivas, y que dividen en los siguientes puntos:

\-Que la Municipalidad de Santa Fe sea incorporada en el debate por el futuro de la hidrovía e invitada a formar parte activa del Consejo Federal de la Hidrovía y de las comisiones que se constituyeron en su marco.

\-Que se reconstruya el nodo logístico Santa Fe a través de la ampliación de las instalaciones portuarias sobre la margen del río Paraná y la vuelta del ferrocarril a la terminal portuar-Que en futuro contrato de concesión se actualice la jurisdicción acuática del Puerto de Santa Fe, debiendo establecer en la parte pertinente de la delimitación de la zona: "comprendida entre el Km. 586 del Río Paraná, tramo exterior de acceso al Puerto de Santa Fe y la zona de aguas profundas naturales en el Río de la Plata exterior hasta la altura del Km 239,1 del Canal Punta Indio utilizando la ruta por el Canal Ingenio Emilio Mitre".

\-Reclamar que las profundidades del tramo Océano-Timbúes tengan como diferencial un máximo de 4 pies respecto al tramo Timbúes-Santa Fe, recordando que en el 2º Encuentro Argentino de Transporte Fluvial realizado en la Ciudad de Rosario el 1º de agosto de 2005 los gobernadores de las provincias de Santa Fe y de Entre Ríos, reclamaron que no se supere el diferencial de cuatro pies que planteamos de profundidades entre el tramo Océano-San Martín y el tramo San Martín- Santa Fe (en aquel entonces San Martín, hoy extendido hasta Timbúes).