---
category: La Ciudad
date: 2021-09-22T06:00:17-03:00
thumbnail: https://assets.3dnoticias.com.ar/SEGURIDADVIAL.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Jatón se reunió con Lagna por el trabajo conjunto en materia de seguridad
  vial
title: Jatón se reunió con Lagna por el trabajo conjunto en materia de seguridad vial
entradilla: Ests martes se rubricó un acta compromiso con la Agencia Nacional de Seguridad
  Vial para la entrega de cascos y chalecos de visibilidad a motociclistas.

---
El intendente de Santa Fe destacó “el trabajo conjunto para cambiar las conductas viales y bajar el nivel de siniestro de los conductores de motos”. La entrega de cascos se realizará de manera gratuita luego de concretar cursos de capacitación.

El intendente Emilio Jatón participó esta mañana de un acto que tuvo como objetivo primordial trabajar en conjunto por la seguridad de los motociclistas. En la ocasión, se rubricó un acta compromiso con la Agencia Nacional de Seguridad Vial para la entrega de cascos y chalecos de visibilidad a motociclistas. El acto se realizó en el Ministerio de Seguridad y contó con la presencia del ministro Jorge Lagna.

Luego de la reunión, Jatón hizo hincapié en la importancia de políticas públicas de prevención y destacó que se debe trabajar en forma conjunta: “Es imposible cambiar las conductas viales si no lo hacemos entre todos: nación, provincia y los municipios”. En consonancia, el mandatario añadió que a ese esfuerzo colectivo “hay que sumarle la conducta de cada uno de nosotros; este es un buen paso, es un punto de encuentro entre todos. La prioridad es cambiar las conductas y bajar el nivel de siniestro de los conductores de motos, ese es el objetivo central”.

Cabe recordar que el municipio santafesino ya viene implementando la Oficina Itinerante de Motovehículos que tiene como objetivo asesorar, generar un registro y promover el uso responsable de motos. Asimismo, se entregan cascos a quienes cumplan ciertos requisitos y tramiten la licencia por primera vez.

Por otra parte, ante una consulta de los medios, el intendente remarcó que “Santa Fe tiene un problema grave de inseguridad y le venimos diciendo al gobernador y al ministro de Seguridad que es necesario reforzar la tarea de la policía en la ciudad. Esperamos que ese pedido también se exprese en las reuniones nacionales”.

También asistieron al acto de hoy, el subsecretario y la directora de la Agencia Provincial de Seguridad Vial (APSV), Osvaldo Aymo y Antonela Ceruti, respectivamente; y el director ejecutivo de la Agencia Nacional de Seguridad Vial (ANSV), Pablo Martínez Carignan. Además, participaron los intendentes de Venado Tuerto, Leonel Chiarella; de Reconquista, Amadeo Vallejos; de Rafaela, Luis Castellano; y de Tostado, Enrique Mualem.

**Implementación**

El acta compromiso se enmarca en el Plan Federal que lleva adelante la Agencia Nacional de Seguridad Nacional y tiene dos instancias. Por un lado, la capacitación a conductores de motovehículos y, por otro lado, con la entrega de mil cascos y chalecos de visibilidad. En ese sentido, los municipios mencionados recibirán 100 ó 200 cascos cada uno; para Santa Fe son 200 unidades.

Asimismo, cada Municipalidad será la encargada de entregar los cascos previa capacitación. “Luego de esa parte teórica se realizará una instancia práctica y, a los destinatarios de esa capacitación, se les entregarán los cascos”, indicó Ceruti.

**Santa Fe como ejemplo**

“La inseguridad vial es la primera causa de muerte en menores de 35 años, y de ellos un porcentaje muy alto, son motociclistas” detalló, por su parte, Martínez Carignano y añadió que “frente a esta realidad recorremos el país y vemos que hay autoridades que son indiferentes; en Santa Fe pasa lo contrario. Estoy muy agradecido a la provincia y a los intendentes porque tienen ganas de trabajar, de que esto cambie y este plan de cascos apunta a eso: a capacitar a los motociclistas y coronarlas con la entrega de protección como un primer paso para continuar trabajando”.

Por otra parte, el funcionario de Nación se refirió a las secuelas que puede dejar en caso de siniestros la falta de cascos “Otro de los temas que hablamos en esta reunión es la de difundir el 149, se trata de una red de asistencia a la víctima en la que la ANSV aporta fondos desde para pagar una operación, hasta un subsidio económico en el caso del que quede incapacitado para trabajar es el sostén de familia. Así que estamos muy satisfechos de este camino que iniciamos con Santa Fe y estoy seguro de que va a beneficiar a mucha gente”, dijo.