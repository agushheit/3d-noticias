---
category: Agenda Ciudadana
date: 2021-12-27T06:15:38-03:00
thumbnail: https://assets.3dnoticias.com.ar/moralesloustou.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: La UCR mira el 2022 con el foco en la reunificación de sus bloques en el
  Congreso aunque las tensiones no aflojan
title: La UCR mira el 2022 con el foco en la reunificación de sus bloques en el Congreso
  aunque las tensiones no aflojan
entradilla: 'La UCR quiere dejar atrás las luchas internas porque tiene el objetivo
  de empoderarse dentro de la coalición de Juntos por el Cambio para generar una alternativa
  presidencial propia que sea competitiva.

'

---
La lista de unidad que alcanzaron el gobernador de Jujuy, Gerardo Morales, y el senador nacional Martín Lousteau, que consagró al primero como presidente del Comité Nacional luego de semanas de confrontación directa y broncas personales, alcanzó para contener las llamas pero no para apagar totalmente las tensiones entre los bandos.

La escisión del bloque radical en la Cámara de Diputados fue el punto más álgido en la saga de desencuentros entre el establishment de la UCR, que representan los gobernadores y figuras resilientes de la estructura radical como Mario Negri y Alfredo Cornejo, y el sector de la "Renovación" que quiere refrescar el partido centenario y darle una impronta generacional distinta de la mano de Lousteau.

La división de la tropa legislativa coincidió con un episodio que poco tuvo de anecdótico: la hostilidad recíproca por el liderazgo radical llegó a tal punto que apenas dos semanas atrás, en la sede del partido, el jujeño le revoleó un vaso de vidrio al economista de rulos, en medio de una reunión catastrófica en la que no pudieron ponerse de acuerdo en nada.

El asunto es que detrás de estas luchas internas por el poder partidario exhibidas prácticamente a cielo abierto, la UCR tiene el objetivo de empoderarse dentro de la coalición de Juntos por el Cambio para generar una alternativa presidencial propia que sea competitiva.

Pero poder dar ese salto de competitividad, que lo sustraiga del lugar de furgón de cola del PRO que jugó durante los años del macrismo en el poder, a la UCR no le queda más remedio que superar las fricciones internas que en las semanas previas pusieron a prueba la unidad. No le queda mucho tiempo con el 2022 a la vuelta de la esquina y un 2023 cada vez más cerca en el horizonte.

A todas luces, el primer paso en ese camino será lograr una rápida reunificación de los dos bloques de la Cámara de Diputados. Negri, una figura que paga caro el costo de la derrota en su propio terruño a manos de Luis Juez, fue bancado por Cornejo, Morales y Carrió, y obtuvo el apoyo mayoritario para seguir al frente de la bancada radical, reducida a 33 miembros.

El "bilardismo" político es implacable con los que cargan con una derrota reciente en sus espaldas, aún cuando Negri pueda exhibir como nadie en la oposición una trayectoria probada a base de un sinnúmero de batallas legislativas, que lo forjaron como un profesional de la conducción parlamentaria, tanto en la oratoria como en el arte de la negociación permanente.

La derrota devaluó su peso interno y lo relegó en la carrera por presidir el interbloque de JxC. Por las dudas, ya avisó que no le interesa conservar el cargo que ejerció ininterrumpidamente desde 2013.

El sector de Lousteau, que en Diputados preside el también cordobés Rodrigo de Loredo, rompió con el bloque "oficial" de la UCR porque considera que la sociedad dio un mensaje democrático a través de las urnas en el sentido de que existen liderazgos ya caducos que quedaron vaciados de representatividad social.

En su flamante mandato, Morales busca reparar la unidad de la UCR y surgió con fuerza el nombre de Rodrigo de Loredo para presidir el interbloque de Juntos por el Cambio. Sería la forma de salir por arriba del laberinto, con Negri en el bloque y el bando de Lousteau premiado con el interbloque. Esta arquitectura soñada un grupo tiene enormes obstáculos para ser una realidad: en Juntos por el Cambio nadie admite la posibilidad de que la presidencia del interbloque pueda ser una prenda para resolver la interna de uno de los partidos.

Además, la bancada más numerosa es la del PRO con 50 integrantes, y como tal el presidente del partido amarillo, Cristian Ritondo, tendría derecho a arrogarse la titularidad del interbloque si quisiera, sin demasiado debate, y especialmente si la competencia a plantearse fuera De Loredo, que preside un bloque de tan sólo 11. Con todo, hasta el momento el PRO no viene explicitando, al menos públicamente, un interés rotundo e irreductible para hacerse  
con ese lugar.

Las fichas se terminarán de ordenar a fines de febrero, en la antesala a la Asamblea Legislativa, pero la UCR tiene el desafío de llegar a esa instancia con su fuerza unificada en la Cámara de Diputados, para negociar en una posición de mayor fortaleza con sus socios de JxC.

En principio, ya se consiguió que Morales y Lousteau firmaran una tregua y rubricaran un acuerdo sobre las nuevas autoridades partidarias. Algo inimaginable apenas dos semanas atrás cuando las partes afilaban las armas para ir a la guerra. Componedor, Cornejo habría sido clave para el armisticio.

Sin embargo, la tregua entre los bandos es frágil, y como botón de muestra de que no será fácil remendar la unidad cabe mencionar el escándalo que suscitó el faltazo de la diputada cordobesa Brouwer de Koning, quien estaba en Disney con su familia en momentos en que Juntos por el Cambio perdía por penales (127 a 126) la votación por Bienes Personales en el recinto el martes pasado.

Debido a esa actitud, de viajar al exterior en lugar de cumplir con su trabajo a escasos días de haber asumido la banca, por poco no la quieren crucificar. No es para menos: con su voto y con el del otro ausente en la sesión, el larretista Álvaro González (PRO) hubieran sido suficientes para dar vuelta el resultado a favor de Juntos por el Cambio. Una batalla ganable que se perdió.

"Se la quieren comer cruda", graficaron fuentes parlamentarias de la UCR a NA. Y por si quedaban dudas desde este sector le piden al bando de Lousteau y De Loredo que "se hagan cargo" de la derrota, según pudo reconstruir este medio.

El experimentado diputado Luis Pastori no anduvo con vueltas e hizo pública la ira contra el eje "renovador" a través de un tuit que sonó a música bélica.

"Todo empieza a quedar claro. Los diputados de la Renovación acompañan al kirchnerismo para aprobar nuevos aumentos de impuestos. Como lo han hecho a lo largo de estos dos años. Después, cuando vienen las elecciones, buscan diferenciarse. Tarde. El pueblo ha aprendido a distinguir", escribió el misionero, en un mensaje que fue retuiteado por Negri.

Por su parte, en declaraciones radiales Karina Banfi agitó aún más el avispero: "En el bloque que conduce Mario Negri, jamás hubiésemos permitido que un diputado viaje teniendo en cuenta la paridad que hay".

La interna radical sigue muy picante y en medio de ese clima se juega la unidad parlamentaria y como fuerza política de la UCR.