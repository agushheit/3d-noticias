---
category: Agenda Ciudadana
date: 2021-02-21T07:00:54-03:00
thumbnail: https://assets.3dnoticias.com.ar/VENTADIRECTA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: En la provincia de Santa Fe ya rige la reglamentación para la "venta directa"
title: En la provincia de Santa Fe ya rige la reglamentación para la "venta directa"
entradilla: El secretario de Comercio Interior, Juan Marcos Aviano, habló sobre la
  instrumentación de esta nueva normativa en todo el territorio santafesino

---
El Gobierno de Santa Fe, a través del Ministerio de Producción, Ciencia y Tecnología, reglamentó la Ley N.º 13.905 denominada de “Venta Directa”, que promueve la registración de las empresas que operan con este canal directo de comercialización, y tienen sede o revendedores independientes en el ámbito de la provincia de Santa Fe.

La norma data del año 2019, y luego de un trabajo articulado entre la Secretaría de Comercio Interior y Servicios, la Cámara Argentina de Venta Directa, y empresas líderes del sector, como la santafesina Essen.

Esta reglamentación pondrá en marcha dos aspectos centrales: por un lado, la obligación de registrar las empresas que tienen el canal de venta directa con sus revendedores, y también la exigencia de contar con medios de contacto virtuales y oficinas en el ámbito provincial, para que los revendedores y consumidores puedan realizar sus reclamos y comunicaciones con la empresa proveedora.

En ese sentido, el secretario de Comercio Interior, Juan Marcos Aviano, expresó que “luego de un año de trabajo, diálogos, reuniones y encuentros con empresas, legisladores, la cámara nacional y nuestro equipo de trabajo, hemos reglamentado esta ley que entendemos transparenta el mercado de la venta directa, un canal muy difundido en todo el país, y donde la provincia de Santa Fe es líder en ventas, tanto por oferentes como por consumidores”.