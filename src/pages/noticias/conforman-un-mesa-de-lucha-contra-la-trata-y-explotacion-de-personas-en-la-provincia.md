---
category: Agenda Ciudadana
date: 2021-07-26T06:00:57-03:00
thumbnail: https://assets.3dnoticias.com.ar/1081052160_0_236_1920_1316_1920x0_80_0_0_5d7eb75d8b4168d25e0abc73d5085844.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Conforman un Mesa de lucha contra la trata y explotación de personas en la
  provincia
title: Conforman un Mesa de lucha contra la trata y explotación de personas en la
  provincia
entradilla: Santa Fe forma parte de los 16 distritos que ya constituyeron esa instancia,
  cumpliendo el objetivo de articulación federal.

---
Convocada de forma conjunta entre la Dirección Operativa del Comité de Lucha contra la Trata y explotación de Personas, Protección y asistencia de sus víctimas de la Jefatura de Gabinete de Ministros, a cargo de Gustavo Vera, y la Secretaría de Derechos Humanos del Ministerio de Gobierno, Justicia y DDDHH, a cargo de Lucila Puyol, quedó conformada la Mesa Interinstitucional de lucha contra la Trata y Explotación de Personas de la provincia de Santa Fe.

De este modo la provincia, forma parte de las 16 provincias que ya constituyeron dicha instancia, cumpliendo el objetivo de articulación federal.

Durante la reunión, Vera se refirió a los ejes centrales del Plan Nacional contra la Trata y Explotación de Personas 2020-2022. “Desde la secretaría anunciamos la creación y construcción de refugios en distintas zonas de la provincia”.

En este contexto, el gobierno de la provincia, avanza notablemente en el fortalecimiento en la asistencia a víctimas, mediante la planificación con gobierno locales para incorporación de inmuebles y el diseño de la infraestructura a los fines de la puesta en funcionamiento de espacios para el refugio, tras el rescate de víctimas de trata y explotación”, indicó.

Participaron del encuentro encuentro virtual, funcionarios nacionales y provinciales abocados a este tema tan sensible y miembros de fiscalía.