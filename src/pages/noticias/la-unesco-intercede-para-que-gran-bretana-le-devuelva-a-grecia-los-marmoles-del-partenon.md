---
category: Estado Real
date: 2022-05-24T09:10:37-03:00
thumbnail: https://assets.3dnoticias.com.ar/628beb6bb9a46_900.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Télam
resumen: La Unesco intercede para que Gran Bretaña le devuelva a Grecia los mármoles
  del Partenón
title: La Unesco intercede para que Gran Bretaña le devuelva a Grecia los mármoles
  del Partenón
entradilla: 'Se trata de frisos y esculturas  del emblemático templo de Atenea que
  fueron extraídos con sierras a pedido del embajador británico en el Imperio Otomano,
  y actualmente forman parte de la colección del Museo Británico. '

---
En un contexto europeo donde ya se han restituido a sus territorios de pertenencia algunas de las obras expoliadas por los imperios coloniales, **los mármoles del Partenón que reclama Grecia a Gran Bretaña** vuelven a estar en la mira, con la mediación de la Unesco y el acuerdo de ambos gobiernos, aunque ya se vislumbra la **negativa de las autoridades del Museo Británico** para definir un acuerdo que reúna todos los fragmentos disponibles en la ciudad de Atenas.  
  
La disputa sobre las esculturas del Partenón se agravó en los últimos días después de que Grecia rechazara la afirmación del Museo Británico de que **gran parte de las estatuas, retiradas por Lord Elgin hace más de 200 años, fueron recuperadas "entre los escombros" alrededor del monumento**, consigna el medio británico The Guardian.  
  
Las afirmaciones de que "gran parte del friso fue extraído de los escombros del Partenón" y **"estos objetos no fueron todos cortados del edificio como se ha sugerido"** realizadas por el subdirector del museo, Jonathan Williams, en la reunión anual del comité intergubernamental de la Unesco que promueve la devolución de bienes culturales, fueron **rechazadas por la ministra de cultura de Grecia, Lina Mendoni**, quién **acusó a Elgin de cometer robo en serie**, según el medio.  
  
"A lo largo de los años, las autoridades griegas y la comunidad científica internacional han demostrado con argumentos incontestables los verdaderos hechos que rodean la sustracción de las esculturas del Partenón", cita The Guardian a Mendoni.  
  
A su vez, la ministra afirma al medio que **"Lord Elgin utilizó medios ilícitos e injustos para apoderarse de las esculturas del Partenón y exportarlas, sin un verdadero permiso legal para hacerlo, en un flagrante acto de robo en serie".**  
  
En cambio, las opiniones vertidas por parte del Museo en la reunión de la semana pasada dan un nuevo giro a la larga disputa cultural, a pocos días de la decisión histórica por parte del gobierno del Reino Unido de estar dispuesto a conversar sobre la demanda de Grecia, para que los mármoles atenienses vuelvan a estar reunidos, refiere el medio británico.  
  
**Las esculturas fueron quitadas del templo con ayuda de sierras de mármol** con conocimiento de Elgin, tal como se rastrea en la correspondencia entre el diplomático escocés y el artista italiano Giovanni Battista Lusieri, encomendado para la supervisión y traslado de las antigüedades en 1801, informa.  
  
Para Mendoni, en un contexto internacional que repatría los tesoros a sus países de origen, la campaña continúa, porque Grecia "está dispuesta a entablar un diálogo honesto y sincero con el Reino Unido, de buena fe, dentro del marco legal y el contexto ético establecidos por las recomendaciones y decisiones de la Unesco", cita The Guardian.

El Museo Británico fundado en 1753, fue el primer museo público nacional del mundo que abrió sus puertas en 1759 abarcando todas las áreas del conocimiento humano, y se considera como un recurso único destinado a un público global: **"las esculturas del Partenón son un elemento vital de esta colección mundial interconectada"** y forman parte del patrimonio mundial compartido y trascienden las fronteras políticas", dice el museo que no es un organismo gubernamental y cuyas colecciones no pertenecen al gobierno británico.  
  
**Las 15 metopas, 17 esculturas y 75 de los 160 metros de largo del friso original, en mármol, con 2500 años de antigüedad en disputa, decoraban el templo dedicado a la diosa Atenea,** el Partenón, en la Acrópolis de Atenas construida entre el 450–430 antes de nuestra era. Las esculturas ornamentales fueron retiradas del templo por el entonces embajador británico ante el Imperio Otomano entre 1799 y 1803, Lord Eglin (Thomas Bruce, 1766-1841) cuando Grecia estaba bajo el dominio de ese imperio, y se encuentran exhibidas en la sala 18 del museo londinense que las expone desde 1817.  
  
Según el Parlamento británico, Elgin adquirió las piezas entre 1799 y 1810, conocidas comúnmente como los mármoles de Elgin o los del del Partenón o bien "esculturas del Partenón", término preferido por el museo, que las adquirió en 1816 tras la votación del parlamento para los fondos necesarios. Y en la actualidad se conservan en esta institución en virtud de la Ley del Museo Británico de 1963. Sin embargo, en un informe del parlamento británico de 2017 se afirma que desde la adquisición de los mármoles, estuvo la sugerencia de su devolución.  
  
El largo recorrido diplomático para la recuperación del patrimonio griego comenzó formalmente en octubre de 1983 con el pedido de Grecia al Gobierno británico, tras el apoyo de la Unesco de 1982, lo cual fue rechazado por el Reino Unido en abril de 1984.  
  
Posteriormente, **el pedido griego abogó por la devolución de las esculturas para reunir la colección para ser expuestas en el nuevo Museo de la Acrópolis (2009), presentado en 2000.**  
  
En 2013 la Unesco propuso mediar en la disputa, lo cual fue rechazado por el gobierno británico y el museo en 2015.  
  
La posición del Gobierno británico se asienta en que "las cuestiones relativas a la propiedad y la gestión de las esculturas del Partenón son competencia de los administradores del Museo Británico", según detalla el informe del Parlamento y es algo que se replica en las respuestas actuales.