---
category: Agenda Ciudadana
date: 2021-06-11T08:55:57-03:00
thumbnail: https://assets.3dnoticias.com.ar/joaquin-presento-la-dimision-su-cargo-la-epe-la-que-fue-aceptada-el-gobierno-provincial.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Diario Uno
resumen: Alberto Joaquín renunció al directorio de la EPE tras el escándalo del "vacunatorio
  vip"
title: Alberto Joaquín renunció al directorio de la EPE tras el escándalo del "vacunatorio
  vip"
entradilla: El ingeniero ocupaba el cargo de vicepresidente de la empresa. Recibió
  las dos dosis entre febrero y marzo.

---
Tras la polémica desatada por el "vacunatorio VIP" contra el coronavirus en el hospital Eva Perón de Granadero Baigorria, en el sur provincial, este jueves renunció a su cargo el vicepresidente de la Empresa Provincial de la Energía (EPE), Alberto Joaquín. La dimisión ya fue aceptada por el gobierno provincial.

El ingeniero, de 78 años, figura como vacunado con la primera dosis de Sputnik el 2 de febrero y con la segunda el 11 de marzo.

A través de un comunicado, Joaquín indicó este jueves que fue vacunado contra el coronavirus en esas fechas porque en enero había tenido una "recaída en mi condición de hipertenso", por lo que solicitó ser inoculado "para poder continuar cumpliendo con las exigencias que el cargo demanda".

En dicho descargo, fundamentó su decisión a vacunarse, pese a que era un momento en que no se estaba vacunando masivamente ni tampoco a personas con comorbilidades.

**El descargo**

"A los 78 años asumí la vicepresidencia de la EPE, ad honorem. Pese a todas las dificultades que la pandemia generaba fui cumpliendo con las múltiples obligaciones que el cargo exigía. A mediados de enero de este año tuve una recaída en mi condición de hipertenso. Como esto potenciaba los riesgos que ya estaba corriendo, y perteneciendo la empresa a un sector esencial, solicité que se me vacunara para poder continuar cumpliendo con las exigencias que el cargo demanda", indicó en su descargo.

Y continuó: "Esto se realizó en el Hospital Eva Perón de Granadero Baigorria los días 2 de Febrero y 11 de Marzo. Facilité mi documento y pedí que mi acto fuera agendado como corresponde y se me entregara el carnet correspondiente".

Por último, dijo que estaba "muy lejos" de sus intenciones "la supuesta participación en un «vacunatorio VIP» que muy livianamente me han adjudicado".