---
category: La Ciudad
date: 2021-04-04T08:00:23-03:00
thumbnail: https://assets.3dnoticias.com.ar/terapia.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Segunda ola, ¿cómo está la ocupación de camas de terapia intensiva en Santa
  Fe?
title: Segunda ola, ¿cómo está la ocupación de camas de terapia intensiva en Santa
  Fe?
entradilla: La provincia tiene un promedio del 62 por ciento de ocupación de las UTI
  y la ciudad de Santa Fe una media del 63 por ciento, entre covid y no covid.

---
La ministra de Salud de la Nación, Carla Vizzotti, aseguró que el país está ingresando en la segunda ola de la pandemia del coronavirus y la provincia de Santa Fe empezó a experimentar en los últimos días un aumento de casos que está por debajo de las cifras nacionales. Sin embargo, los números empiezan a preocupar a las autoridades santafesinas.

La buena noticia es que el sistema de salud de la provincia no está en niveles de estrés (más del 70 por ciento de ocupación de camas de terapia intensiva). Según datos del Ministerio de Salud de la provincia, a nivel provincial la ocupación de camas covid y no covid en las Unidades de Terapia Intensiva (UTI) está en un promedio del 62 por ciento. Mientras que en la ciudad de Santa Fe esa cifra es del 63 por ciento.

Si bien son números altos, aún muestran que hay un resto para afrontar la llegada de la segunda ola de contagios. En esas cifras es que el gobierno se apoya para no tomar medidas restrictivas por el momento.

Por su parte, Perotti, no descarta recurrir a medidas drásticas en el caso de que la situación epidemiológica así lo requiera. Por el momento, el promedio de ocupación de camas críticas está lejos de significar un problema, aunque si se produce un exponencial aumento de casos eso derivará en un rápido crecimiento de la ocupación de las terapias intensivas.

Por ese motivo, una de las principales herramientas para ralentizar ese proceso tiene que ver con reforzar las buenas conductas sociales. Justamente ese tema ya fue analizado por los equipos de Salud de provincia y de las principales ciudades. El objetivo es encarar una campaña de difusión para que la población tome conciencia de la importancia de los cuidados personales, lo que repercutirá en mejores resultados a nivel colectivo.

Además, el gobierno provincial estudia la posibilidad de suspender las cirugías programadas para evitar una ocupación de camas críticas que se puede postergar sin generar riesgos en los pacientes afectados.

En caso de no obtener los resultados esperados, el siguiente nivel de medidas ya ingresaría en el terreno de las restricciones.

_Con información de UNO Santa Fe_