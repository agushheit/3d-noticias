---
category: Agenda Ciudadana
date: 2022-11-16T18:45:14-03:00
thumbnail: https://assets.3dnoticias.com.ar/344795_oliver.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Redaccion 3Dnoticias
resumen: 'Fabian Palo Oliver : "Se consolida la hipocresía y los privilegios"'
title: 'Fabian Palo Oliver : "Se consolida la hipocresía y los privilegios"'
entradilla: EL diputado Fabian Palo Oliver, en la red social Twitter dio cuenta de
  la reunion que mantuvo con la Ministra Celia Arena y el Secretario de Justicia Gabriel
  Somaglia.

---
El diputado Fabian Palo Oliver dio  su opinión en la red social Twitter,  luego de finalizada la reunión que mantuvieron diputados con los responsables de los concursos para jueces comunitarios. 

"Reunión con la Ministra Celia Arena y el Secretario de Justicia Gabriel Somaglia. Ratificamos que el concurso está viciado. Confirmamos que el único criterio tenido en cuenta es el acomodo y el abuso en el uso del poder. Se consolida la hipocresía y los privilegios "

En el mismo posteo, y ante la pregunta de algunas personas, acerca de los fundamentos expuestos por los funcionarios, Palo Oliver fue lapidario.

"Ningún fundente (sic) merece atención. Son unos sinvergüenza, plantean que no están obligados a publicar los puntajes y que la calificación global es el orden de mérito. Da asco tanta hipocresía, tanta mentira para violentar el derecho de los concursantes."

Foto extraída de la red social Twitter.