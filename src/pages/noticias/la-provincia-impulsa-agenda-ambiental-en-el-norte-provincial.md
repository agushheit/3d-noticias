---
category: Estado Real
date: 2021-08-28T06:00:58-03:00
thumbnail: https://assets.3dnoticias.com.ar/CRESPO.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La provincia impulsa agenda ambiental en el norte provincial
title: La provincia impulsa agenda ambiental en el norte provincial
entradilla: La ministra de Ambiente y Cambio Climático, Erika Gonnet, avanzó con presidentes
  comunales e intendentes en diferentes proyectos.

---
La provincia de Santa Fe, a través del Ministerio de Ambiente y Cambio Climático, continúa desarrollando y fortaleciendo proyectos ambientales en distintas localidades del territorio santafesino. Esta semana, la Ministra Erika Gonnet, visitó Calchaquí, Villa Guillermina, Florencia, Reconquista y Gobernador Crespo.

La primera ciudad visitada fue Calchaquí, donde Gonnet participó de una reunión junto al Intendente Rubén Cuello y su equipo, con el objetivo de conversar acerca de las problemáticas ambientales del lugar. Luego del encuentro, la Ministra junto al Intendente visitaron el Micro Bosque Urbano, un espacio nuevo de Calchaquí que busca poner en valor los árboles nativos de la localidad con acciones de inclusión social, y también el vivero municipal. Por su parte, desde el Ministerio de Ambiente y Cambio Climático, se realizó la entrega de 80 árboles para continuar avanzando en el trabajo de forestación que se realiza en la localidad.

Asimismo, en relación a la Gestión Integral de Residuos, se realizó un aporte no reintegrable para que Calchaquí obtenga 35 contenedores con el objetivo de continuar fortaleciendo los procesos que vienen llevando adelante en materia de residuos. Además, la Ministra también visitó La Abonera del Pueblo, un espacio en donde se realiza abono para la tierra con residuos de poda de todos los vecinos; y formó parte de una forestación en el marco del Día del Árbol.

La ministra resaltó el trabajo que vienen llevando adelante los jefes territoriales en materia ambiental y particularmente en relación a la gestión integral de residuos comentó que “es una problemática mundial que requiere una planificación a largo plazo pero sobre todo una decisión política de querer abordar el tema”, y agregó: “Es importante que los municipios y comunas de nuestra provincia cuenten con el apoyo del gobierno de Santa Fe para avanzar en ese sentido”.

La recorrida continuó en la comuna de Villa Guillermina, donde la Ministra se reunió junto a la Presidenta Comunal Nancy Ávalos e integrantes de su equipo. En el Complejo Histórico Cultural, Educativo y Ambiental las funcionarias conversaron acerca de las proyecciones de una agenda ambiental articulada para la localidad. En un trabajo conjunto entre la comuna y el Ministerio de Ambiente y Cambio Climático, se avanza en la localidad en un laboratorio de semillas nativas e iniciativas de forestación, entre otras acciones.

En la localidad de Florencia, la ministra Erika Gonnet hizo entrega de un aporte no reintegrable para la compra de una cinta transportadora, una prensa y una refacción del tinglado para continuar fortaleciendo sus procesos de Gestión de Residuos. El intendente Rubén Quain junto a su equipo de trabajo agradecieron el acompañamiento del Gobierno de Santa Fe y valoraron el aporte que mejorará el servicio en la localidad.

Durante la recorrida, junto a la Ministra participaron el secretario de Políticas Ambientales, Arq. Oreste Blangini, la Directora Provincial de Economía Circular, Ing. Saida Caula, el Director Provincial, Lic. Esteban Koziol, y la Directora Provincial de Desarrollo Territorial, Arq. Josefina Obeid.

En tanto, este viernes la recorrida comenzó por Reconquista, ciudad en donde Gonnet mantuvo una reunión con el intendente Enri Vallejos en la sede municipal. Además, se realizó una visita a un predio de la localidad en donde antes funcionaba un basural a cielo abierto y ahora se trabaja desde la Municipalidad en planes de forestación para convertirlo en un nuevo espacio público. En dicho predio la Ministra compartió la plantación de especies nativas en una jornada que forma parte de la celebración del Día del Árbol. Cabe resaltar que el gobierno de Santa Fe, a través del Ministerio de Ambiente y Cambio Climático, otorgó recientemente un aporte para la adquisición de 35 ecopuntos y 140 recipientes para residuos de 240 litros.