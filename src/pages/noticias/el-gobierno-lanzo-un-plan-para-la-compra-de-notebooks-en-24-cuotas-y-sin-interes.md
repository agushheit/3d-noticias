---
category: Agenda Ciudadana
date: 2021-01-30T03:08:22Z
thumbnail: https://assets.3dnoticias.com.ar/pc.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Diario Uno
resumen: El gobierno lanzó un plan para la compra de notebooks en 24 cuotas y sin
  interés
title: El gobierno lanzó un plan para la compra de notebooks en 24 cuotas y sin interés
entradilla: La campaña se llama "Vuelta al Cole". ¿Cómo acceder al beneficio?

---
El Banco Nación lanzó hoy la campaña “Vuelta al Cole” para la adquisición de notebooks y tablets en 24 cuotas sin interés con las tarjetas de crédito Nativa Mastercard y Nativa Visa.

Según informaron desde la entidad bancaria, el objetivo de la campaña es ampliar el alcance de las herramientas tecnológicas para los estudiantes de todos los niveles educativos.

Asimismo, indicaron que la propuesta regirá del 5 al 7 de febrero y se podrá acceder a través de la TiendaBNA, una plataforma desarrollada entre el BNA y Nación Servicios, una empresa del grupo Banco Nación.

Además, la propuesta incluye artículos de librería e indumentaria, con un beneficio del 30% de descuento para compras en un pago con un tope de devolución de $1.800 y un 15% para adquisiciones de 2 a 6 cuotas sin tope y sin interés. Esta opción funcionará del 8 al 13 de febrero.

La iniciativa para la compra de notebooks y tablets en cuotas sin interés complementa la reciente campaña de Navidad y Reyes, en la cual miles de clientes accedieron a TiendaBNA para adquirir bienes y servicios con un plazo máximo de 18 cuotas, también, sin interés. En este sentido, el ticket promedio de los clientes que accedieron a esa propuesta alcanzó los $19.600.

En tanto, informaron que sigue vigente hasta el próximo 28 de febrero el Plan de Línea Blanca con 36 cuotas fijas y con hasta 3 cuotas sin interés para la compra de todos los productos de Tecno, Hogar y Electro.

Por otra parte, destacaron que los artículos que generaron más demanda en la TiendaBNA fueron las computadoras y los teléfonos celulares, con ventas por más de $ 35 millones desde el comienzo de la promoción, aunque también se registró un considerable movimiento en relación con productos de audio y video, pequeños electrodomésticos y, especialmente, ventiladores.