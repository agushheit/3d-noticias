---
category: Agenda Ciudadana
date: 2021-10-17T06:15:55-03:00
thumbnail: https://assets.3dnoticias.com.ar/PRECIOS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: El gobierno provincial ve con buenos ojos un acuerdo nacional para congelar
  precios
title: El gobierno provincial ve con buenos ojos un acuerdo nacional para congelar
  precios
entradilla: El secretario de Comercio Interior Juan Marcos Aviano indicó que se puede
  "apoyar o complementar" el acuerdo, asegurando que "Santa Fe es la provincia que
  más inspecciona precios en la Argentina".

---
A la espera de definiciones que adopte Nación sobre el programa de congelamiento de precios que busca concretar con empresarios el secretario de Comercio Interior, Roberto Feletti, la provincia se posicionó a favor del acuerdo para restringir los incrementos de al menos 1.245 productos de la canasta básica. Esto fue plasmado por el par provincial de Feletti, Juan Marcos Aviano, quien manifestó que en Santa Fe se puede "apoyar o complementar" el acuerdo a nivel nacional.

Según lo que trascendió sobre la decisión del gobierno nacional, el listado completo de productos que integrará el acuerdo abarca todo el espectro del consumo masivo: hay productos de almacén, de perfumería, de limpieza, alimentos frescos y congelados, entre otros. Para esto, el gobierno pidió que las empresas informen el precio que los mismos tenían al 1 de octubre pasado.

En diálogo con UNO Santa Fe, Aviano se manifestó sobre el programa impulsado por el gobierno nacional de cara a las elecciones de noviembre: "Todo lo que se haga con voluntad de las partes es viable. Aún no hubo una notificación oficial ni una resolución de la Secretaría que establezca el listado de productos y de precios. Todo lo que se ha dado a conocer hasta ahora puede ser oficial o no, pero no está formalmente en vigencia, si bien hay conversaciones".

"Vamos a esperar y, por supuesto, poner a disposición a todo el sector empresario y al gobierno de la provincia en lo que nuestro territorio corresponda para poder acompañar en su momento para llevarlo adelante no solamente en las grandes cadenas de supermercados sino también que pueda tener una influencia en los comercios pequeños o autoservicios", continuó el funcionario del ministerio de Producción, Ciencia y Tecnología.

De no mediar acuerdo a nivel nacional el secretario de Comercio Interior, Roberto Feletti, anticipó que se aplicará la ley de Abastecimiento y la Secretaría tendrá que avanzar con "políticas de precios máximos no consensuadas". Acerca de la reacción empresarial a la medida, el funcionario manifestó que el intento de orden se sella un acuerdo el gobierno implementará "precios máximos no consensuados"

"Nosotros podemos apoyar o complementar a un acuerdo nacional de precios, abastecimiento y consumo. Estamos esperando una convocatoria para los próximos días, hasta ahora solo hubo comunicaciones informales", afirmó al respecto Aviano.

Según el Instituto Provincial de Estadística y Censos (Ipec), la inflación en la provincia de Santa Fe se mantiene en sus índices casi a la par que la medida nacional, con un 33,8% de incremento acumulado registrado hasta agosto 2021, restando las mediciones del organismo correspondientes al mes de septiembre. El rubro de alimentos y bebidas mantuvo una suba del 35,7% durante este año, con un incremento de precios constante.

Frente a la consulta sobre cómo podría fiscalizarse un cumplimiento de la medida en supermercados y comercios, Aviano aseguró que se hará "como venimos haciendo con Precios Cuidados, con Precios Santafesinos, con Súper Cerca". Y afirmó: "Somos la provincia que más inspecciona en la Argentina, tanto en los programas provinciales como nacionales. No queremos avanzar en alguna aseveración hasta tanto no haya un acuerdo firmado y una lista de productos y precios".

Al respecto de posibles incumplimientos de parte de supermercadistas o comerciantes en los precios estipulados, el secretario provincial de Comercio Interior subrayó que "el acuerdo que se firma no va a eso. No tienen como objetivo la sanción y que las inspecciones sean fiscalizaciones con un fin punitivo. Si alguien lo rompe se lo notifica y ante alguna reincidencia si puede haber alguna cuestión relacionada a un sumario".

Al ser consultado por si la provincia tenía voz para lograr un acuerdo con privados o si se daba de manera unilateral de parte del Estado nacional, el funcionario respondió: "La palabra no es unilateral, este es un acuerdo de precios del Estado nacional con las principales firmas que hacen al consumo masivo de la Argentina. Sí hay particularidades propias de nuestro territorio que deben ser tenidas en cuenta. Somos una provincia productora de alimentos con una presencia importante de empresas a lo largo y a lo ancho del territorio".

"Seguimos en un contexto sanitario y económico difícil y hay que llevar tranquilidad a la góndola y a la mesa de los santafesinos. Yo creo que es posible con mucho más diálogo que se sigue manteniendo, hubo frentes que se han ido resolviendo y Santa Fe es parte de eso", concluyó sobre esto el funcionario.ar una política de precios "genera malestares", tras lo cual añadió: "si hay malestar en los empresarios, más lo hay en el pueblo".

**Billetera Santa Fe y presupuesto 2022**

Billetera Santa Fe sigue multiplicando sus ventas, cuestión que debió ser tenida en cuenta a la hora de la proyección de cálculo para asignación de fondos en el presupuesto 2022. Si bien es un número variable, desde el gobierno provincial confirmaron a UNO que rondan el 50% las operaciones con Billetera Santa Fe en supermercados, "algunos más".

Frente a la consulta sobre cual fue el monto definido para sostener en funcionamiento el programa económico de pago virtual, Aviano indicó: "No he leído al detalle la proyección del ministerio de Economía, pero sí la propia cartera económica confirmó que se iban a tomar previsiones para las erogaciones que se den con Billetera Santa Fe. Como todo presupuesto son proyecciones, lo mismo ha ocurrido este año y el programa se garantizó potenciando el consumo".