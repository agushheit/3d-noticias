---
category: Agenda Ciudadana
date: 2021-03-28T06:15:03-03:00
thumbnail: https://assets.3dnoticias.com.ar/ALIMENTOS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: INFOBAE
resumen: El Gobierno repartió unos 10 mil millones de pesos en comida a través de
  organizaciones sociales vinculadas al kirchnerismo
title: El Gobierno repartió unos 10 mil millones de pesos en comida a través de organizaciones
  sociales vinculadas al kirchnerismo
entradilla: 'Otros 113 mil millones de pesos fueron girados por el ministerio de Desarrollo
  Social a través de la Tarjeta Alimentar por la cual los beneficiarios pueden comprar
  solo alimentos. '

---
Desde enero 2020 y hasta el mes de febrero de este año, el gobierno nacional destinó $131.826.220.932 al Plan Nacional Argentina Contra el Hambre (PNACH). De ese total, $113.057.865.000 se vehiculizó a través de la Tarjeta Alimentar, el principal programa asistencial del ministerio de Desarrollo Social, a cargo de Daniel Arroyo. Los restantes $18.768.355.932 corresponden a otros programas.

Del informe al que accedió Infobae de manera exclusiva, surge que, de ese último monto, casi 10 mil millones de pesos destinados a repartir comida, se realizó a través de organizaciones sociales. Algunos de esos programas son los que están destinados a llevar alimentos secos a comedores escolares, merenderos y desarrollos comunitarios como Sembrar Soberanía Alimentaria o Pro-Huerta.

Esos movimientos sociales son los que tienen trabajo territorial en los barrios y representación en el propio Ministerio de Desarrollo Social como Emilio Pérsico, el Secretario de Economía Social y secretario general del Movimiento Evita y, entre otros, el líder de Barrios de Pie, Daniel Menéndez, a cargo de la Subsecretaría de Promoción de la Economía Social y Desarrollo local.

Ambos integran la Unión de Trabajadores de la Economía Popular (UTEP), que lidera Esteban “Gringo” Castro, quien surgió del Movimiento Evita.

Ese espacio también está referenciado en Fernando “Chino” Navarro, un dirigente que tiene despacho en la Casa Rosada, es secretario de Relaciones Políticas y Parlamentarias, un área del organigrama de la jefatura de Gabinete, a cargo de Santiago Cafiero.

El propio informe del Plan Nacional Argentina Contra el Hambre, refiere a la creación del “Registro Nacional de Comedores y Merenderos (RENACOM)” para “contar con información precisa acerca de cada uno de los comedores y merenderos que distintas organizaciones de la sociedad civil llevan adelante en los barrios de todo el país, brindando asistencia alimentaria de forma gratuita a personas en situación de vulnerabilidad social”.

Según se destaca: “Hasta el momento, se han inscripto 13.865 lugares que dependen de organizaciones sociales a lo largo del país, la mayoría de ellas pertenecen a la provincia de Buenos Aires donde se registraron 3.684 comedores y 1.919 merenderos”.

Desde el ministerio de Desarrollo Social, además de confirmar las abultadas cifras destinadas a paliar el hambre en la Argentina, destacaron que el RENACOM “se creó para que todo sea transparente”.

Es a través de estos movimientos y de esos locales que se canaliza buena parte de los alimentos que llegan a los más humildes. Los comedores están ubicados, además, en las zonas más desprotegidas del país donde los padres van a alimentarse con sus hijos, o a recibir una vianda o un bolsón de alimentos secos.

Quizás un dato resuma las necesidades de los más humildes, de los que menos tienen y están inmerso en la pobreza y la indigencia. Entre enero de 2020 y febrero de este año y según lo revela la Dirección Nacional de Asistencia Critica, se distribuyeron 61.199.010 kilos alimentos. La provincia de Buenos Aires con 34.381.770 de kilos de comida repartida, está a la cabeza de la asistencia social. Le sigue la Ciudad Autónoma de Buenos Aires con 3.332.479 kilos de comida distribuida entre los más necesitados.

Lejos de poder achicar la ayuda del Estado a los sectores más vulnerables, esta se fue incrementando mes a mes, y continuará así por la demora en la reactivación económica, la generación de empleo y la inflación que golpea el bolsillo de los asalariados y trabajadores informales.

El mismo documento al que accedió infobae describe la situación de manera descarnada.

“Durante los últimos años, la Argentina atravesó un proceso de deterioro económico y social manifestado en un considerable aumento de la pobreza, la indigencia y el desempleo. Asimismo, desde los primeros meses del año 2020, la pandemia del COVID-19 trajo aparejada una profundización de la crisis social y económica preexistente que afectó particularmente a las poblaciones más vulnerables de nuestro país”.

La fuertísima inversión en alimentos, la gran mayoría secos y que se hicieron llegar a través de bolsones y que se adquirían a través de la Tarjeta Alimentar, tiene su correlato en la descripción que se realiza desde la cartera de Arroyo y que está planteada en el documento: “La situación alimentaria nutricional actual registra una grave vulneración de derechos básicos que afecta a la población en general y a niñas, niños y adolescentes en particular. La consecuencia de dicha vulneración es la alta prevalencia de malnutrición (sobrepeso y obesidad en todas las edades), retraso de la talla (8% de los niños y niñas menores de 6 años), anemia (30% en todas las edades en mujeres, niños, niñas y adolescentes)”.

De hecho, como ya informó este medio, el gobierno de Alberto Fernández admitió que la pobreza aumentó casi 20 puntos en medio de la pandemia y la cuarentena.

Así lo reveló el informe de pobreza multidimensional publicado por el Sistema de Información, Evaluación y Monitoreo de Programas Sociales (Siempro), que depende de la Presidencia de la Nación. De esta manera se llegaba en el primer semestre de 2020 a un 47% de la población en situación de pobreza multidimensional, lo que representa 21 millones de personas.

La ayuda alimentaria para los sectores más desprotegidos fue una prioridad para el ministro Arroyo desde que llegó al cargo. Y en la Tarjera Alimentar, según su opinión, encontró la mejor manera de canalizarlo.

El trabajo de la cartera de Arroyo, titulado “Estado de situación PLAN NACIONAL ARGENTINA CONTRA EL HAMBRE”, sostiene que: “Desde enero 2020 -y hasta febrero pasado-, se acreditaron un total de 1.567.051 titulares de Tarjetas Alimentar, que alcanzan un total de 2.040.277 destinatarios (1.945.047 hijos, 45.710 embarazadas y 49.520 hijos e hijas con discapacidad). La inversión en todo el período fue de $113.057.865.000″.

En mayo y en diciembre, con la fuerte cuarentena y aislamiento social como telón de fondo, se otorgaron refuerzos extraordinarios para titulares de la Tarjeta Alimentar. Y a partir de febrero de este año, se incrementó en un 50% el valor de la acreditación mensual.

Para aquellas familias con un hijo o hija, de $4.000 pasaron a cobrar, solo para la compra de alimentos, $6.000; y para quienes tuvieran dos o más hijos, de $6.000 a $9.000.

La Tarjeta Alimentar, a diferencia de otros planes, no tiene intermediarios. El dinero está destinado a cada uno de los beneficiarios y estos pueden comprar solo alimentos. De hecho el informe, bajo el título “Estudios sobre consumos y gastos de titulares de la Tarjeta Alimentar”, analizó los “patrones de consumo y de gasto”, especialmente en alimentos, de las familias titulares de ese beneficio.

Cinco de cada 10 kilos de alimentos comprados corresponden a alimentos saludables, poco menos de 2 kilos, a alimentos no recomendados y algo más de 3 kilos a otro tipo de alimentos. Entre los alimentos recomendados el mayor peso lo tienen los lácteos y luego las proteínas animales, entre los no recomendados las gaseosas y las harinas y cereales con azúcar; y entre los otros tipos de alimentos las harinas sin azúcar y el azúcar”.

El consumo de verduras y de frutas representa en conjunto el 5% del total (3% y el 2% respectivamente). El precio promedio pagado por kilo de alimentos saludables es de $151, el de otro tipo de alimentos es de $113 y el de los alimentos no recomendados de $231. El promedio total es de $150.

Además de la Tarjeta Alimentar, el ministerio de Desarrollo Social reforzó la asistencia alimenticia a través de varios programas que nucleados en Plan Argentina Contra el Hambre.

Para la asistencia de comedores escolares, se destinó $7.301.361.155. Según pudo establecer Infobae desde el Ministerio de Desarrollo Social y de los movimientos sociales, estos últimos se encargaron de colaborar con los establecimientos educativos, sobre todo en los barrios más vulnerables del Conurbano Bonaerense.

Para los comedores comunitarios y merenderos, donde los movimientos sociales tienen preponderancia, se derivaron $6.623.302.129; otros $400.000.000 fueron trasferidos a las provincias para reforzar la situación alimentaria generada por la crisis del COVID-19.

Otros $3.320.421.100, llegaron a los sectores más vulnerables derivados del denominado “Módulo Alimentario COVID-19″, estos alimentos secos, y algunos cocidos a través de viandas, llegaron a municipios y gobernaciones para evitar el descontento social en medio de la pandemia.

En alcaldías como La Matanza o Quilmes, se solicitó la colaboración de las Fuerzas Armadas para cocinarlos y repartirlos en los barrios más vulnerables. Los secos, la propia intendencia los distribuía con medios propios o se los hacía llegar a los comedores vinculados a los movimientos sociales.

Al proyecto relacionado con la comercialización de alimentos, vinculado a los trabajadores de la economía popular se destinaron $80.000.000.

Así como buena parte de los comedores y merenderos del país, están bajo la la orbita de los movimientos sociales, como el Evita y Barrios de Pie, entre otros y los bolsones de alimentos son distribuidos por ellos, por un acuerdo que se realizó desde el propio ministerio de Desarrollo Social, donde sus dirigentes ocupan cargos de importancia.

Otros más de 200 millones de pesos fueron derivados a emprendimientos sociales como “sembrar soberanía” o “Pro-Huerta”, entre otros programas.