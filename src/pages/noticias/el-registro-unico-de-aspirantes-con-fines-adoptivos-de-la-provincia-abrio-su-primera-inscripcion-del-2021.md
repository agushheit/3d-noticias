---
category: Estado Real
date: 2021-03-03T06:46:50-03:00
thumbnail: https://assets.3dnoticias.com.ar/adopcion.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: El Registro Único de Aspirantes con Fines Adoptivos de la Provincia abrió
  su primera inscripción del 2021
title: El Registro Único de Aspirantes con Fines Adoptivos de la Provincia abrió su
  primera inscripción del 2021
entradilla: Las solicitudes se recibirán durante los primeros diez días de marzo.
  Luego se hará lo propio durante junio, septiembre y diciembre.

---
El Registro Único de Aspirantes con Fines Adoptivos de la Provincia de Santa Fe abrió su primera inscripción del año 2021 y permanecerá recibiendo solicitudes durante los primeros 10 días de marzo.

En ese sentido, la directora del área, Magdalena Galli Fiant, informó que “cada tres meses habrá una apertura de inscripciones para facilitar el acceso y brindar información para que los aspirantes puedan formalizar su solicitud. Además, en el transcurso de este año, habrá cuatro instancias para que quienes desean adoptar puedan enviar sus formularios iniciales en los meses de junio, septiembre y diciembre”.

**INSCRIPCIONES ONLINE**

Las personas que deseen inscribirse deberán enviar al correo electrónico del RUAGA, el formulario F1 completo, con datos personales, y sugerencias sobre temas de interés, para los encuentros informativos que se darán con posterioridad a esta primera inscripción. Los formularios F1 se descargan de la página de la Provincia (www.santafe.gov.ar) y se envían adjuntos al mail correspondiente a Santa Fe (registros@santafe.gob.ar) o Rosario (registrosrosario@santafe.gob.ar).

“El año pasado solo hubo dos instancias de inscripción en las que se registraron 240 inscriptos en la primera etapa y 170 en la segunda, respectivamente”, explicó Galli Fiant. Asimismo, agregó que “esta experiencia nos confirma que establecer este sistema de inscripción realmente era una necesidad y estamos satisfechos con los resultados”.

**NUEVO SISTEMA DE BÚSQUEDA**

Durante el primer año de gestión, el RUAGA implementó un nuevo sistema de búsqueda, para aquellos aspirantes que se encuentran inscriptos, y han sido evaluados y admitidos por el Registro. Los mismos pueden acceder a las Búsquedas Abiertas Internas, en la que se intenta vincular niños, adolescentes y grupos de hermanos, cuando no se ha logrado la adopción en la Provincia o a nivel Nacional.

Al respecto, la directora sostuvo que “es una invitación a pensar la posibilidad de adopción de niños y/o adolescentes que tienen una situación diferente a la que se declaró inicialmente al definir su disponibilidad adoptiva. En este momento estamos cursado la quinta BAI y está dirigida a todos los inscriptos cuando encuadran en su modalidad adoptiva”.

Por último, Galli Fiant señaló además que “desde la Red Federal que conforman los Registros de Adoptantes de todo el país y que está coordinada por la Dirección Nacional de Registro de Adoptantes, está trabajando mucho para desarrollar buenos contenidos informativos para todos aquellos que tienen interés en la adopción y para los medios de comunicación, con el fin de acercar conceptos claros e información fidedigna”.