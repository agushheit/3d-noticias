---
category: La Ciudad
date: 2021-03-22T06:15:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/comercios.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Los comercios advierten que sería "imposible volver a cerrar"
title: Los comercios advierten que sería "imposible volver a cerrar
entradilla: Argumentaron que "ningún comerciante soportaría cerrar sus puertas nuevamente".

---
A raíz de los "indicios" que advirtió el gobierno sobre la llegada de la segunda ola de contagios a territorio santafesino, desde el Centro Comercial Santa Fe avisaron que decretar un nuevo cierre en la actividad "sería el golpe de gracia para los comercios". A un año del comienzo de la cuarentena sostienen que hay "expectativa negativa" por la precariedad del sector.

Martín Salemi, presidente del Centro Comercial Santa Fe, sostuvo: "Nosotros pregonamos la institucionalidad, respetando las normas y las leyes, pero lamentablemente la sociedad está cansada y endeudada. Cerrar de nuevo sería un problema muy grave".

En 2020, hasta la pandemia había un leve crecimiento en el consumo de los comercios, aunque la situación cambió drásticamente cuando se decretó la cuarentena. Sin embargo, el sector comercial llevaba casi 24 meses de caída en la actividad prepandemia. A propósito de esto, Salemi indicó que "el cierre total fue muy prolongado, dejó a muchos comerciantes en el camino y se incrementó mucho la cantidad de locales que cerraron en la ciudad.

"Aquellos que pudieron sobrevivir lo hicieron en base a mucho esfuerzo, de quemar reservas, incluso vendiendo parte de su capital. En esta situación, los comercios están en una situación de precariedad ante lo que viene. Todavía no hay un indicio que indique que la situación comience a cambiar", continuó el presidente del Centro Comercial.

**Expectativa negativa e incertidumbre en el sector**

A octubre del año pasado, un relevamiento que hecho por el Centro Comercial arrojó que hubo unos 150 locales que cerraron en la ciudad de Santa Fe. Con este dato que se relevó el año pasado, Salemi advirtió que "esto marcará la tendencia que se espera para el relevamiento de marzo".

El último informe del Centro Comercial arrojó que en febrero de 2021 que el 65% de los comercios vendió menos que en febrero de 2020. Sobre esto, el referente comercial indicó que "lamentablemente las clases demoraron en arrancar, el crecimiento de las ventas de artículos escolares se dio en marzo, aunque esperamos verlo reflejado en el nuevo relevamiento".

Según Salemi, en el sector "la realidad es que hay expectativa negativa por una segunda ola de contagios y que se puedan llegar a cerrar actividades hacen que se genere un consumo negativo en los comercios. La gente tiende a ahorrar y resguardarse porque no sabe qué va a pasar".

Desde el Centro Comercial posicionaron al rubro de la indumentaria y calzado como uno de los más afectados. Para Salemi, el hecho de que los eventos en la ciudad de Santa Fe no estén habilitados en su gran mayoría "afecta mucho a la compra “de fin de semana”. El rubro construcción crece porque la gente quiere resguardar sus ahorros, pero más allá de eso no hay otros rubros que estén en crecimiento".