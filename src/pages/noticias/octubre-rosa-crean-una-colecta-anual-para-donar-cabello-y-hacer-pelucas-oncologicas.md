---
category: La Ciudad
date: 2021-10-15T06:15:26-03:00
thumbnail: https://assets.3dnoticias.com.ar/CABELLO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'Octubre Rosa: crean una colecta anual para donar cabello y hacer pelucas
  oncológicas'
title: 'Octubre Rosa: crean una colecta anual para donar cabello y hacer pelucas oncológicas'
entradilla: 'El Concejo local instituyó una campaña anual de donación de cabello natural
  para la confección de pelucas para pacientes oncológicos en Santa Fe. También, un
  banco solidario. '

---
Durante octubre se conmemora el "mes de concientización sobre Cáncer de Mama", más conocido como "Mes Rosa", que es una instancia para insistir sobre la importancia de los controles regulares y diagnósticos precoces (como métodos de prevención) frente a una de las enfermedades más frecuentes en las mujeres. El Concejo se hizo eco de problemática, y sancionó una ordenanza creando una campaña anual de donación de cabello para la confección de pelucas para pacientes oncológicos.

 Esta campaña se realizará el 4 de febrero de cada año, y la finalidad es crear un banco solidario de facilitadores de pelucas (peluqueros, estilistas y otros profesionales del rubro) en toda la ciudad de Santa Fe. La norma busca "promover acciones para "facilitar el acceso a pelucas para pacientes oncológicos quienes, producto del tratamiento quimioterapéutico sufren alopecia, a través del trabajo colaborativo entre el sector público y privado con afinidad en esta temática".

 En este sentido, la ordenanza instituye una mesa de trabajo para coordinar las acciones vinculadas a las tareas que hagan a la recepción de donaciones, clasificación, disposición del cabello y confección de pelucas. Esta mesa estará compuesta por la Aspa Santa Fe (Asociación Santafesina de Peluqueros y Anexos) y la Cámara de Estilistas y Afines Santafesinos, entre otras entidades del rubro, "quienes coordinarán a aquellos profesionales que voluntariamente deseen participar de las tareas propias de su oficio", sostiene el texto sancionado.

 El proyecto -votado por unanimidad-, fue impulsado por Lucas Simoniello (FPCyS), y por su par de interbloque, Mercedes Benedetti. El edil aclaró que en la confección del texto definitivo colaboró Chicas Pink (organización que trabaja en la temática). 

 Cabe recordar que el cáncer de mama es la primera causa de muerte por tumores en mujeres en la Argentina. En el país ocurren 5.600 muertes por esta enfermedad, que afecta mayoritariamente a mujeres de entre 45 y 70 años: más del 75% de ellas no tienen ningún antecedente familiar.

 "La detección temprana es fundamental, ya que los tumores de un centímetro tiene hasta un 90% de probabilidades de curación. Las mamografías y el auto examen son herramientas clave. Y como se sabe, las células cancerosas atacan a las del cabello, lo que trae aparejado la pérdida de éste", manifestó Simoniello.

 Para conformar una peluca se necesitan no menos de 25 metros de cabello, y en el mercado el costo de éstas va de entre 30 mil y 60 mil pesos, a lo que se suma unos 1.500 pesos de materiales y unas 20 horas de trabajo. Ese encarecimiento dificulta mucho el acceso a las pelucas de pacientes con cáncer de mama, sobre todo para aquellas personas de escasos recursos.

  **La mirada del otro**

 Mercedes Benedetti contó que la ideal original "nació de un problema personal" "Cuando uno transita esto empieza a conocer a un montón de personas a las que les pasa lo mismo. Y una de las mayores preocupaciones de una mujer es que al perder el cabello, la enfermedad no se puede ocultar más", relató.

 Y en esto, la mirada del otro normalmente "es muy amorosa, pero el tema es cómo uno toma esa mirada: no lo que ese otro nos quiere significar, sino lo que representa para una persona que está en esa instancia (de enfermedad)", contó la edila.

 "Sé lo que es lo difícil que es conseguir y pagar una peluca, lo cual se vuelve prohibitivo para los sectores sociales de menores recursos", agregó Benedetti, quien agradeció a Simoniello y a todos los concejales por "haber sabido ponerse en el lugar del otro" y así darle impulso a la ordenanza. 

 Para conformar una peluca se necesitan no menos de 25 metros de cabello, y en el mercado el costo de éstas va de entre 30 mil y 60 mil pesos, a lo que se suma unos 1.500 pesos de materiales y unas 20 horas de trabajo. 

 