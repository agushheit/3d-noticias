---
category: La Ciudad
date: 2021-09-26T06:00:41-03:00
thumbnail: https://assets.3dnoticias.com.ar/GUADALUPE.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Santa Fe se Ilumina: arrancaron los trabajos en Guadalupe'
title: 'Santa Fe se Ilumina: arrancaron los trabajos en Guadalupe'
entradilla: " El monto oficial es de más de 50 millones de pesos y se colocarán 405
  columnas y artefactos. La obra se complementará con el recambio de luminarias que
  se llevará adelante en la avenida Siete Jefes."

---
Este viernes, el intendente Emilio Jatón supervisó las tareas de colocación de columnas de iluminación que comenzaron en Tacuarí y Larrea. Por un monto superior a los 50 millones de pesos, las obras se habían licitado en junio para un gran sector de la ciudad comprendido por los barrios Guadalupe Noreste y Central Guadalupe. Esta semana, las vecinas y vecinos de esos barrios pudieron observar que los trabajos ya se iniciaron.

Con las columnas recién emplazadas, Jatón, junto a la concejala Laura Mondino y el presidente de la vecinal Guadalupe Noreste, Alberto Mellit, dialogaron con las vecinas y vecinos sobre las mejoras, entre ellas en materia de seguridad, que significará el nuevo sistema lumínico al barrio.

“Esta obra forma parte de Santa Fe se Ilumina; ya comenzamos con estos trabajos que son tan necesarios, porque este barrio, al igual que tantos otros, está sufriendo graves problemas de inseguridad y este es el aporte que estamos haciendo desde el municipio: iluminar el barrio”, indicó el intendente. Y detalló que ese sistema lumínico “es el más obsoleto que tiene la ciudad de Santa Fe, no tiene el cableado pertinente, ni los tableros. Es una obra que abarca más de 405 columnas en Guadalupe y más de 300 en Siete Jefes”.

“Una vez que terminen las obras, este sector quedará muy bien iluminado”, indicó el intendente y remarcó que en toda la ciudad se colocarán más de 3.500 luminarias led, con varios frentes de trabajo al mismo tiempo.

El plazo de ejecución es de 6 meses y las columnas que se colocaron esta mañana fueron diseñadas por personal municipal. Se trata de un diseño mucho más amigable desde lo estético, que no necesitan tanto porte y no resultan tan invasivas desde la trama urbana. Por otro lado, resultan mucho más cómodas para trabajar y contarán con los mismos artefactos led que se colocaron en bulevar, en barrio Scarafía y en Liceo Norte. Se trata de equipos muy chicos, de última tecnología que tienen una potencia ideal para iluminar los barrios.

**Mejores luces, más seguridad**

Con el eje puesto en aportar mejoras, entre ellas en materia de seguridad, el intendente remarcó que en un barrio tan importante como Guadalupe, “hasta ahora tienen dos artefactos lumínicos por cuadra, con luces amarillas, rotas; estamos haciendo un trabajo integral en este barrio porque era muy necesario. Y ese trabajo se replica en toda la ciudad: la idea es llegar al 40 % de iluminación led a fin de año”.

Por su parte, Mondino coincidió con el intendente en cuanto al “problema de inseguridad que viven los vecinos y vecinas” y consignó que tanto desde la Municipalidad como del Concejo “podemos aportar en ese tema y la iluminación es fundamental, tener luz blanca a la noche, que nos permita transitar con mayor seguridad es lo que se pretende y sin dudas esta obra cambiará radicalmente la vida de los vecinos”.

**“Una realidad”**

Por su parte, Mellit expresó que los trabajos que comenzaron en el barrio “son un viejo anhelo que tiene la vecinal, lógicamente, para mejorar la seguridad”. En consonancia, el presidente de la vecinal aclaró: “No es que ahora no habrá más inseguridad, pero esto ayuda un montón y este sueño que teníamos hoy comienza a concretarse, ya se están poniendo las columnas. La obra va avanzando, es una realidad, se ve”.

Las 405 columnas nuevas se colocarán en Guadalupe estarán en el sector comprendido por Javier de la Rosa al Sur, General Paz al Oeste y French al Norte; también abarca a un sector de barrio Judiciales.

Cabe recordar que el plan que lleva adelante el municipio incluye la nueva iluminación de los bulevares y avenidas como Pellegrini, Gálvez, Rivadavia, y los barrios Siete Jefes, Scarafía, Santa Marta, Las Flores, Liceo Norte, Yapeyú y Ceferino Namuncurá, Roma, Fomento 9 de Julio, entre otros. Se trata de una iniciativa que busca garantizar un servicio eficiente de iluminación para todos los habitantes de la capital provincial.