---
category: Deportes
date: 2021-11-13T06:15:05-03:00
thumbnail: https://assets.3dnoticias.com.ar/FIDEO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Perfil
resumen: Argentina derrotó a Uruguay con un golazo de Di María y se acerca a Qatar
  2022
title: Argentina derrotó a Uruguay con un golazo de Di María y se acerca a Qatar 2022
entradilla: "El equipo de Lionel Scaloni continúa por la buena senda en las Eliminatorias
  y se impuso con lo justo en Montevideo. Video.\n\n"

---
La Selección Argentina venció 1-0 a Uruguay en Montevideo por la fecha 13 de las Eliminatorias Sudamericanas rumbo al Mundial de Qatar 2022  
  
Ángel Di María convirtió el único tanto del partido y fue un verdadero golazo. Recibió una asistencia por parte de Paulo Dybala, y sin ninguna marca pegada a él, el futbolista del PSG, mandó la pelota cerca del ángulo superior derecho de Fernando Muslera, que intentó desviarla pero fue imposible.

No fue uno de los mejores partidos, en cuanto a rendimiento de varios jugadores de la era de Lionel Scaloni. Logró llevarse los tres puntos ante un rival que lo incomodó más de una vez. Emiliano Martínez fue determinante en la primera acción de peligro para La Celeste.   
  
Los fanáticos que llegaron al estadio Campeón del Siglo, corearon el nombre de Lionel Messi para ver acción del 10. Ingresó a los 75 minutos por Giovani Lo Celso. La Albiceleste no generó otras chances para aumentar el marcador, mientras que los dirigidos por Óscar Tabárez fueron superiores pero no incomodaron al Dibu.   
  
La Scaloneta llega a 26 partidos invicta. Se ubica segunda con 28 puntos, a 7 unidades de Brasil. Argentina buscará acortar distancia con el único clasificado al Mundial por Sudamérica y encaminar su pasaje directo a Qatar, en la próxima jornada cuando se juegue, en San Juan, el clásico ante la Canarinha.