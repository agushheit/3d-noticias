---
category: Agenda Ciudadana
date: 2020-12-03T10:17:32Z
thumbnail: https://assets.3dnoticias.com.ar/assa.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: 'Plan Incluir: Aguas Santafesinas continúa mejorando el sistema cloacal de
  la ciudad de Santa Fe'
title: 'Plan Incluir: Aguas Santafesinas continúa mejorando el sistema cloacal de
  la ciudad de Santa Fe'
entradilla: 'La empresa inició obras en una importante estación elevadora de la red
  de desagües cloacales. '

---
Los trabajos demandan una inversión provincial superior a los cinco millones de pesos y tienen un plazo de ejecución de 180 días.

Aguas Santafesinas está iniciando obras en una importante estación elevadora de la red de desagües cloacales de la ciudad de Santa Fe como parte de su programa de mantenimiento de instalaciones y en el marco las acciones del Plan Incluir.

En esta oportunidad las obras se desarrollarán en la estación elevadora cloacal Río Salado, ubicada en Gobernador Menchaca y Estado de Israel, que integra las 30 necesarias para la operación del servicio en la ciudad debido a sus peculiaridades topográficas. Dichos trabajos demandan una inversión provincial superior a los cinco millones de pesos y tienen un plazo de ejecución de 180 días.

Los trabajos previstos en la obra comprenden reparación de los portones de ingreso, el reemplazo de las aberturas metálicas, mejoras en los dos caminos de acceso al predio y en veredas perimetrales, el reemplazo de parte del cerco olímpico perimetral, ampliación y reparación de la cubierta y cielorrasos, y renovación de las instalaciones eléctricas del edificio. Además, se incluye el reemplazo de tapas de las cámaras de acceso, arreglo de revoques, la pintura completa de la edificación y la limpieza general del predio.

## **BUEN USO DE LAS CLOACAS**

Desde la empresa, recuerdan que es conveniente tener en cuenta la importancia de hacer un buen uso del servicio cloacal para evitar inconvenientes tanto en la red como en estas instalaciones donde solo deben descargarse los líquidos cloacales de los domicilios provenientes de baños, cocinas y lavaderos, evitando el ingreso de elementos sólidos y tóxicos que afecten las instalaciones, a los usuarios y el ambiente.

## **PLAN INCLUIR**

El Plan Incluir tiene como eje de gestión la intervención sobre los barrios y la población más vulnerable, los factores de riesgo asociados a la delincuencia, principalmente en los lugares donde hay mayores desventajas sociales, educativas, culturales, comunitarias, físicas y ambientales, con la incorporación de mesas participativas y organizaciones de la sociedad civil, y con obras de infraestructura.

Del programa participan los ministerios de Desarrollo Social; Seguridad; Salud; Educación; Infraestructura; Ambiente y Cambio Climático; Cultura; Gestión Pública; la Secretaría de Estado de Igualdad y Género; la Empresa Provincial de la Energía (EPE); Aguas Santafesinas S.A. (Assa); municipios, comunas y organizaciones sociales.