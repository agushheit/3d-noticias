---
category: Agenda Ciudadana
date: 2021-04-21T07:32:52-03:00
thumbnail: https://assets.3dnoticias.com.ar/construccion.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: 'Líneas de construcción: lo que se necesita para acceder a los créditos'
title: 'Líneas de construcción: lo que se necesita para acceder a los créditos'
entradilla: Se entregarán 65 mil créditos personales para la compra de materiales
  y trabajos de mano de obra, y otros 22 mil créditos hipotecarios para la construcción
  de vivienda nueva en lote propio.

---
El gobierno nacional lanzó 87.000 créditos a tasa cero de interés para la refacción y construcción de viviendas en todo el país, en el marco del programa federal Casa Propia, líneas que se ajustarán por la fórmula Hog.Ar de actualización de capital basada en la evolución de los salarios.

A través de dos líneas, se entregarán 65 mil créditos personales para la compra de materiales y trabajos de mano de obra, y otros 22 mil créditos hipotecarios para la construcción de vivienda nueva de hasta 60 metros cuadrados de superficie en lote propio.

La inscripción ya se encuentra abierta en www.argentina.gob.ar/casapropia donde se debe completar el formulario de inscripción en caso de cumplir con todos los requisitos previstos en las bases y condiciones, allí detallados.

Ambas líneas estarán ajustadas por la nueva fórmula Hog.Ar de actualización de capital basada en la evolución de los salarios, con el objetivo de dar previsibilidad a los tomadores de estas líneas crediticias, informó el Ministerio de de Desarrollo Territorial y Hábitat.

"A través de estas acciones estamos impulsando un nuevo modelo de crédito en la Argentina en el que se vincula el trabajo con el derecho a la vivienda y donde el ordenador es el salario de las y los argentinas y argentinos, aplicando la nueva fórmula Hog.Ar de actualización de capital basada en la evolución del Coeficiente de Variación Salarial (CVS)", detalló.

Durante el lanzamiento, el ministro Ferraresi señaló que trabajan "para que la vivienda no sea más una cuestión de especulación financiera, sino que sea un derecho y que ese derecho tenga que ver con generar condiciones para poder soñar con una Argentina inclusiva y de igualdad de oportunidades para todos y todas".

"Para ello, diseñamos este sistema solidario donde cada uno paga en función del salario que tiene y así cada uno genera la posibilidad de construir este nuevo paradigma; volvimos para ser mejores y empezamos a generar esas condiciones a partir del hábitat", afirmó.

**Los detalles**

Los créditos personales para la compra de materiales y trabajos de mano de obra, que se destinarán a mejorar las condiciones habitacionales en viviendas existentes de todo el país, serán por un monto de hasta $ 240.000 con un plazo de hasta 36 meses y tres meses de gracia.

El porcentaje máximo de la cuota sobre el salario será de 25%, con una cuota máxima de $ 6.917,37, y se otorgará con una acreditación de ingresos de entre $ 25.000 y $ 175.000.

En tanto, los 22 mil créditos hipotecarios serán para la construcción de vivienda nueva de hasta 60 metros cuadrados de superficie en lote propio, por un monto máximo de hasta $ 4 millones, con financiación 100%, sin necesidad de ahorro previo.

El porcentaje máximo de la cuota sobre el salario será de 25%; el plazo de devolución será de hasta 30 años, a tasa cero con fórmula HogAr; y se podrá realizar la construcción en terreno de familiar directo para quienes no cuenten con uno propio.

A modo de ejemplo, con un salario de $ 53.000 se podrá tomar un crédito con un plazo de 360 cuotas de $ 13.375; mientras que con un salario de $ 175.000 serán 110 cuotas de $ 43.740.

De acuerdo con el detalle brindado por el Ministerio, para la línea de construcción de viviendas el crédito acompañará la evolución del presupuesto a medida que se ejecute la obra.

Asimismo, se ofrecerán a los beneficiarios distintos modelos de vivienda con el objetivo de acortar los plazos de ejecución.

Casa Propia es una política federal de viviendas que tiene por objetivo brindar 264.000 soluciones habitacionales y garantizará el derecho a la vivienda promoviendo su acceso igualitario en todo el país.

"Esta nueva iniciativa del Gobierno nacional con alcance federal busca seguir reduciendo el déficit habitacional y nace de comprender a la vivienda como un derecho y a su construcción como parte indispensable del proceso productivo", indicó la cartera que conduce Ferraresi.