---
category: Agenda Ciudadana
date: 2021-10-16T06:15:37-03:00
thumbnail: https://assets.3dnoticias.com.ar/femicidios.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: En 2020, la mitad de los femicidios en la provincia se produjeron en la casa
  de la víctima
title: En 2020, la mitad de los femicidios en la provincia se produjeron en la casa
  de la víctima
entradilla: 'Según un informe provincial, hubo 26 víctimas categorizadas como femicidios
  durante el año pasado. Del total de asesinatos, 15 se cometieron en la vivienda
  que vivía la mujer agredida.

'

---
Se encuentra disponible la primera presentación del informe realizado a partir de la base de datos conformada por el Ministerio de Igualdad, Género y Diversidad, el Ministerio de Seguridad y el Ministerio Público de la Acusación.

El informe revela que en 2020 se produjeron un total de 45 muertes violentas e intencionales de mujeres en toda la provincia. Asimismo, detalla que en 26 casos –de los 45– se detectaron elementos de violencia de género, razón por la cual, se clasificaron como femicidios.

Sobre el lugar en el que se produjeron las muertes, el informe precisa que 10 fueron en el departamento Rosario (38,5%); 3 en La Capital (11,5%) y 3 en General Obligado (11,5%).

En cuanto al contexto en el que se produjeron los femicidios, el informe detalla que 17 fueron femicidios íntimos (65,4%), es decir, aquellos en los que había una relación de pareja o de expareja entre la víctima y el agresor. Por su parte, 3 fueron femicidios sexuales (11,5%) y otros 3 se produjeron en un contexto de criminalidad organizada (11,5%).

El informe también precisa el lugar en el que se produjeron los 26 femicidios. Del total, 12 fueron cometidos en la vivienda que compartían la víctima y el agresor (46,1%); 7 en espacios públicos (26,9%) y 3 en la vivienda de la víctimas (11,5%). Acerca al vínculo entre la víctima y el agresor, en 17 casos eran pareja o expareja (65,4%) y 4 se conocían (15,4%).

Sobre el medio empleado, el documento concluye que 7 de los 26 femicidios fueron cometidos con armas de fuego (26,9%). Por su parte, 5 fueron cometidos con golpes (19,2%); 4 mediante el uso de fuego (15,4%) y otras 4 por asfixia o ahorcamiento (15,4%).

El informe también detalla la edad de las víctimas: 12 tenían entre 30 y 39 años (46,1%); 5 entre 40 y 49 años (19,2%); y 3 tenían 70 años o más (11,5%).

**Testimonios oficiales**

“Este informe preliminar constituye un paso más del trabajo articulado que venimos realizando con las distintas jurisdicciones intervinientes. El objetivo de esta y de otras acciones que llevamos adelante es que el Estado unifique sus indicadores y su conocimiento para el abordaje integral, en este caso, de las violencias más extremas por motivos de género”, sostuvo la ministra de Igualdad, Género y Diversidad, Celia Arena.

“Es una política de fondo para el gobierno provincial – continuó. Recordemos, también, que hay una decisión de nuestro presidente Alberto Fernández, que creó el Consejo Federal para el abordaje integral de los femicidios, travesticidios y transfemicidios, que en la provincia de Santa Fe tiene su correlato, enmarcado en la firme voluntad de Omar Perotti de avanzar en esta integración y coordinación con el resto de los poderes del Estado”, agregó.