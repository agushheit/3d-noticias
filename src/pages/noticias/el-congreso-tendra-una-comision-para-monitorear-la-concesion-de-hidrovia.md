---
category: Agenda Ciudadana
date: 2021-04-18T06:00:50-03:00
thumbnail: https://assets.3dnoticias.com.ar/HIDROVIA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: El Congreso tendrá una comisión para monitorear la concesión de Hidrovía
title: El Congreso tendrá una comisión para monitorear la concesión de Hidrovía
entradilla: Dos comisiones del Senado dictaminaron el proyecto de la santafesina.
  La ciudad de Santa Fe invitada a exponer en la próxima reunión del Consejo Federal.

---
El plenario de comisiones de Asuntos Constitucionales y de Presupuesto y Hacienda del Senado, presidido por María de los Angeles Sacnun (FdeT - Santa Fe) y Carlos Caserio (FdeT - Córdoba) dio dictamen favorable por unanimidad al proyecto de ley presentado por la santafesina para la creación una Comisión Bicameral en el ámbito del Congreso para el seguimiento, monitoreo, auditoría y contralor del proceso de concesión y funcionamiento de la Hidrovía Paraná-Paraguay, del sistema de navegación troncal.

La iniciativa propone que dicha comisión esté integrada por seis senadores y seis diputados, designados de acuerdo al reglamento de cada Cámara y que pueda solicitar todo tipo de documentación e información relacionada con la Hidrovía y el sistema de navegación troncal, tanto del sector privado como del sector público, y de organismos locales e internacionales.

Tendrá la facultad de citar a funcionarios nacionales, provinciales, municipales y comunales a prestar informes, como así también a representantes de los organismos que manejan, controlan, ejecutan y desarrollan la licitación de la Hidrovía. También podrá requerir información vinculante a las universidades, entidades empresariales, industriales, del comercio, de la producción agropecuaria y de los trabajadores.

El propósito, señaló Sacnun, "será elaborar informes con propuestas concretas y asesorando en el avance de las acciones a seguir, haciendo especial énfasis en el desarrollo armónico de la nación, el respeto al federalismo, el desarrollo de las economías regionales y el cuidado del medio ambiente, auditando las inversiones, costos y gastos que se desprendan de la licitación de la Hidrovía".

Para la senadora santafesina "es muy importante que el Parlamento no delegue facultades y ejerza efectivamente sus funciones de contralor. Porque acá, no solamente estamos debatiendo el dragado o el balizamiento, sino que estamos discutiendo un proyecto de Nación, un proyecto de país, con el consiguiente desarrollo de las economías regionales. Constituir una Comisión Bicameral es materializar el rol constitucional del control en cabeza del Parlamento. El contralor del Estado en el sistema de navegación troncal es fundamental para evitar los delitos federales que se cometen en el Paraná, y que tienen que ver con la criminalidad económica, el contrabando, la evasión fiscal, la trata de personas y el narcotráfico".

La senadora destacó que "si accedemos a los estudios que se han realizado en Naciones Unidas en el año 2017, se observa que la Argentina pierde por año el 4 por ciento de su PBI. Estamos hablando de 21 mil millones de dólares, mientras nosotros discutimos como hacemos para que los recursos de la Argentina alcancen para hacer efectivos todos los derechos que consagra la Constitución Nacional, que no es más ni menos que ese gran contrato social que hemos celebrado entre todos los argentinos y las argentinas".

"Si no ponemos la mirada de contralor sobre el flujo de mercadería y sobre que las empresas transnacionales no evadan los recursos que la Argentina requiere para despegar y salir adelante, la verdad es que estaríamos equivocándonos. La Argentina figura entre los cinco países del mundo más afectados por la pérdida de ingresos fiscales a manos de las empresas transnacionales. Imagínense si no es importante que el Congreso de la Nación Argentina lleve adelante el contralor" aseguró.

"Hemos sancionado la ley de la Marina Mercante y cómo impacta en la creación de puestos de trabajo y fundamentalmente en los fletes. Sabemos que en la balanza de Argentina hay aproximadamente 4500 millones de dólares de fletes con lo cual, ahí tenemos que tener una mirada muy concreta a la hora del desarrollo del empleo y de la marina mercante en nuestro país. Y por otra parte hasta qué punto esto tiene que ver con el modelo de Nación, que está por encima de los posicionamientos políticos partidarios. lo cual se relaciona con el impacto de la recaudación de Argentina" finalizó Sacnun.