---
category: Agenda Ciudadana
date: 2021-09-26T21:40:28-03:00
thumbnail: https://assets.3dnoticias.com.ar/ALBERTOX.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: De qué se trata el programa Registradas que se presentará este lunes
title: De qué se trata el programa Registradas que se presentará este lunes
entradilla: El mandatario estará acompañado por la ministra de Mujeres, Géneros y
  Diversidad, Elizabeth Gómez Alcorta; el ministro de Trabajo, Claudio Moroni; el
  presidente del Banco Nación.

---
El presidente Alberto Fernández encabezará este lunes a las 11.30 en la Casa Rosada el anuncio del Programa "Registradas (Recuperación Económica, Generación de Empleo e Inclusión Social)", que promueve la reducción de la informalidad en el sector de las trabajadoras de casas particulares y garantiza su acceso y permanencia a un empleo registrado.  
  
Por la implementación de este programa, el Estado nacional paga una parte del sueldo de la trabajadora durante 6 meses, mientras la parte empleadora debe registrarla y pagar sus aportes, contribuciones, ART y el porcentaje del sueldo restante.  
  
Además, se abre una cuenta sueldo gratuita a nombre de la trabajadora en el Banco Nación y allí se transfiere el sueldo, detalló Presidencia.  
  
Por otro lado, a las 8.30, el jefe de Gabinete, Juan Manzur, mantendrá una reunión de trabajo con el ministro de Transporte, Alexis Guerrera.  
  
En qué consiste el programa

•Es un programa para reducir la informalidad en el sector de las trabajadoras de casas particulares y garantizar su acceso y permanencia a un empleo registrado.  
  
•El Estado nacional paga una parte del sueldo de la trabajadora durante 6 meses. La parte empleadora debe registrar a la trabajadora y pagar sus aportes, contribuciones, art y el porcentaje del sueldo restante.  
  
•Se abre una cuenta sueldo gratuita a nombre de la trabajadora en el banco nación y allí se transfiere el sueldo.