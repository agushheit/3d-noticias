---
category: La Ciudad
date: 2021-02-12T18:00:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/violenciagenero.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Municipalidad acompaña a mujeres que sufren violencia de género
title: La Municipalidad acompaña a mujeres que sufren violencia de género
entradilla: A través de la Dirección de Mujeres y Disidencias se realiza un abordaje
  interdisciplinario de cada caso.

---
Desde diciembre de 2019, funciona en el ámbito de la Municipalidad de Santa Fe, la Dirección de Mujeres y Disidencias. El área, a través de un abordaje interdisciplinario, acompaña a quienes atraviesan situaciones de violencia de género y solicitan asistencia.

La directora de Mujeres y Disidencias, Soledad Artigas, explicó que el trabajo en la dependencia que conduce, consiste en “acompañar y asesorar a las mujeres que atraviesan alguna situación de violencia de género”. Según detalló, “el municipio no es un organismo de recepción de denuncias, por lo que el hecho de que las mujeres se acerquen a charlar con nosotras no implica necesariamente que deban hacer una denuncia. No obstante, nosotros recepcionamos las dudas, escuchamos las situaciones que nos cuentan y acompañamos en ese proceso”, relató.

En cuanto al equipo que presta servicio, indicó que se compone de psicólogas, abogadas, trabajadoras sociales y psicólogas sociales, quienes realizan una escucha activa de lo que las mujeres cuentan. En base a esa entrevista se evalúa cada situación particular y su contexto, se le sugieren las acciones posteriores y se buscan las mejores soluciones “porque no hay una respuesta única para estos temas”, describió Artigas.

Posteriormente se elabora una estrategia y un plan de protección de esa mujer y se acompaña esa situación, articulando con otros organismos estatales, ya sean provinciales o nacionales. La directora amplía que “se generan todas las articulaciones para poder dar una respuesta y contener a la persona en la situación que está viviendo”.

**Vías de comunicación**

Quienes deban comunicarse con la Dirección de Mujeres y Disidencias pueden acercarse a la sede de la dependencia municipal, ubicada en 25 de mayo 2884, de lunes a viernes, de 8 a 18 horas, o comunicarse telefónicamente al 4571666 o 4571525. También pueden llamar a la Línea de Atención Ciudadana del municipio, al 0800 777 5000, durante las 24 horas, los siete días de la semana.