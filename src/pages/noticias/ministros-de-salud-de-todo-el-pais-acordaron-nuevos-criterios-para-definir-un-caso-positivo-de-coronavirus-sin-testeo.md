---
category: Agenda Ciudadana
date: 2022-01-23T06:15:41-03:00
thumbnail: https://assets.3dnoticias.com.ar/VIZZOTTIDELTA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: Ministros de Salud de todo el país acordaron nuevos criterios para definir
  un caso positivo de coronavirus sin testeo
title: Ministros de Salud de todo el país acordaron nuevos criterios para definir
  un caso positivo de coronavirus sin testeo
entradilla: ' COFESA acordó que "se clasificará como caso confirmado toda persona
  que reúna al menos uno de los criterios epidemiológicos y uno de los criterios clínicos
  establecidos".

  '

---
La ministra de Salud de la Nación, Carla Vizzotti, junto a sus pares de las 24 jurisdicciones del país consensuaron una nueva definición para la confirmación de casos de coronavirus por criterio clínico y epidemiológico.

Tras un nuevo encuentro virtual del Consejo Federal de Salud (COFESA), que duró poco más de una hora, se acordó que "se clasificará como caso confirmado toda persona que reúna al menos uno de los criterios epidemiológicos y uno de los criterios clínicos establecidos".

En tanto, los criterios epidemiológicos a tener en cuenta serán: ser contacto estrecho con un caso confirmado en los últimos 10 días; haber participado de un evento o espacio social/laboral en el cual se hayan producido al menos tres casos confirmados, lo que se considera un brote; o que resida en una zona con muy alta incidencia de infectados: superior a 500 casos cada 100 mil habitantes en los últimos 14 días.

Los criterios clínicos implican que "presente dos o más síntomas (fiebre, tos, dolor de garganta, dificultad para respirar, vómitos/diarrea/dolor de cabeza/dolor muscular) o pérdida repentina del gusto/olfato".

"Es clave esta reunión para poder revisar la situación epidemiológica y hacer el análisis conjunto de esta tercera ola con predominancia de la variante Ómicron", resaltó Vizzotti en un comunicado.

En ese marco, la titular de la cartera sanitaria nacional destacó la importancia de establecer una nueva definición para la confirmación de casos de Covid-19 por criterio clínico y epidemiológico teniendo en cuenta que "esta ola tiene una dinámica muy diferente".

Con respeto a las estrategias de cara al inicio del ciclo lectivo, Vizzotti informó que está en funcionamiento una mesa de trabajo específica con el ministerio de Educación en la que se definió como prioridad "trabajar para estimular la vacunación pediátrica de COVID-19 y de Calendario, tanto en niños niñas y adolescentes, como así también en docentes, no docentes y en toda la comunidad educativa".

Por último, la funcionaria nacional recomendó a las jurisdicciones "estimular el contacto con los ministerios de educación y de salud de las provincias para culminar con un consejo conjunto federal de salud y educación donde se pueda avanzar en la actualización de protocolo para establecimientos educativos".