---
category: Estado Real
date: 2021-01-04T10:30:23Z
thumbnail: https://assets.3dnoticias.com.ar/010421-Lifschitz.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: El Litoral'
resumen: 'Miguel Lifschitz: «No hubo relación de la Cámara de Diputados con el Poder
  Ejecutivo»'
title: 'Miguel Lifschitz: «No hubo relación de la Cámara de Diputados con el Poder
  Ejecutivo»'
entradilla: 'El socialista advierte sobre las relaciones difíciles con la Casa Gris
  y señala la retención de partidas de la provincia para los gobiernos locales. La
  disyuntiva frente a ser candidato, o no, este año. '

---
No comparte hacer un frente «anti» y valida la necesidad de tener propuestas para la sociedad. Muestra preocupación por la vinculación de la política con el delito y valora la tarea de una justicia autónoma.

**<span style="font-family: Helvetica, sans-serif; font-size: 1.1em; font-weight: 700"> Fragmentos del diálogo con Miguel Lifschitz:</span>**

—**Terminó un año atípico no solo en lo sanitario y económico, sino también en el ámbito parlamentario. La pandemia hizo que la Cámara cambiara de sede (Paraninfo), que hubiera sesiones virtuales y semivirtuales...**

—Fue un año sorpresivo, nadie esperó algo parecido. En los primeros meses un _shock_, pero daba la sensación de que, tanto los gobiernos como los especialistas en salud e infectólogos sabían todo y tenían todo controlado. Con el correr del tiempo se les fueron quemando los papeles a todos –no solo en la Argentina– y hoy nadie puede pronosticar cuándo se termina la pandemia, cuándo se termina de vacunar, cómo va a evolucionar la situación. Existe más incertidumbre que otra cosa. Fue un año complejo, pero muy bueno para la Legislatura, en particular, si me abstraigo del contexto. Hubo muchísimo trabajo legislativo. Arrancamos en enero con las famosas leyes de emergencia y nos paramos hasta el 30 de diciembre. Sesionamos todo el año, hubo permanentes reuniones de comisiones, plenarios, presencial o virtual, nunca paramos el funcionamiento. Como dato objetivo, se han presentado y discutido más de 3 mil proyectos, el doble de cualquiera de los diez años últimos años. Además, hubo un buen clima en la Legislatura; en Diputados ha habido discusiones fuertes, polémicas, pero dentro de un clima de respeto, de debate y eso se debe rescatar.

—**De todas maneras, ¿hubo una relación difícil entre la Cámara y el Poder Ejecutivo?**

—Con el Poder Ejecutivo diría que no hubo relación; no hubo relación de la Cámara de Diputados con el Ejecutivo. Las pocas reuniones que ha habido a lo largo del año con ministros, intentando buscar alguna concertación, acuerdo, consenso, no han fructificado. No ha habido voluntad ni vocación del gobierno por buscar diálogo en la Cámara. Hasta ahora, funcionaba de alguna manera el Senado como especie de articulador, en algunos proyectos, de algunas leyes, pero no sé qué ocurrirá de aquí en adelante.

—**El Frente tiene mayoría en Diputados y ha intentado convertir en leyes temas que fueron agenda de su gobierno, y el gobierno actual los acusó de querer gobernar. ¿Se encuentran en una situación complicada al no compartir todo lo que hace el Ejecutivo?**

—Desde luego que no, pero no hemos visto vocación de diálogo y de busca de acuerdos con la oposición en Diputados donde el Frente Progresista es mayoría. Es su estrategia, una forma de gobernar, obviamente no ha sido nunca la nuestra, pero es el estilo, la forma de este gobierno. Durante algún tiempo tuvimos un mayor diálogo con Esteban Borgonovo, pero rápidamente nos dimos cuenta de que Borgonovo no era la figura que el gobernador había empoderado para las decisiones políticas. Con la partida de Esteban, nos hemos quedado prácticamente sin interlocutores en el gobierno.

—**¿Las pocas reuniones que ha tenido con el gobernador Omar Perotti no han servido para hablar de la agenda provincial?**

—Fueron dos reuniones: una en Santa Fe y la otra en Rosario, pocos días antes de la presentación del Presupuesto, donde hablamos sobre presupuesto y ley tributaria. Obviamente, le manifesté la intención de darles tratamiento rápido, y de hecho lo hicimos en ambos casos, pero nunca hubo una vocación de buscar acuerdos. Ambas leyes las hemos votado porque consideramos que son herramientas necesarias para el gobierno, pero no porque hubiéramos llegado a algún acuerdo con el Ejecutivo. En el caso del presupuesto, los acuerdos se lograron en el Senado y es una manera de resolver la situación, pero no es lo habitual y lo común.

—**En la semana última el ministro (Marcelo) Sain lo acusó a usted de frenar las leyes de seguridad y dijo que las hará efectivas por decreto.**

—Si lo podía hacer por decreto sería bueno que lo hubiese hecho al inicio de la gestión y no hubiéramos perdido un año. Los proyectos de leyes fueron presentados en octubre, tenemos un equipo muy grande de gente trabajando en ese tema, hemos convocado a asesores. Son proyectos complejos. Al gobierno le llevó más de diez meses elaborarlos y es razonable que las cámaras se tomen su tiempo para poder estudiarlo. En el medio aparecieron otros temas como Presupuesto, ley Tributaria que fueron prioridad. Incluso en las charlas con el gobernador él me priorizó estos temas que tienen que ver con la gobernabilidad y la economía. En febrero y marzo tendremos debates sobre el tema seguridad. Los vamos a trabajar con mucha seriedad, responsabilidad porque son leyes que no tienen que ver con el presente ni el corto plazo, tienen que ver con el futuro. Van a tener impacto sobre los próximos gobiernos y como nosotros tenemos expectativas de volver al gobierno en una fecha no muy lejana, nos interesa que sean leyes que tengan fuerte respaldo, consenso legislativo. Vamos a trabajar para esto.

—**El gobernador habló de cambiar la forma de hacer política y cuestionó a la Legislatura.**

—Hasta ahora hemos dado tratamiento a todos los proyectos de ley. No sé a qué se refiere.

—**Volvió a hablar del presupuesto 2020, que lo armaron durante los últimos meses de su gestión...**

—Eso ya terminó. Pero, además, es una discusión absurda porque ese presupuesto fue aprobado por unanimidad en las dos cámaras. Fue un acuerdo legislativo de todos los bloques, incluido el peronismo en todas sus expresiones y la redacción final se acordó con Walter Agosto y Rubén Michlig. Hay testigos. Yo asumí con un presupuesto que elaboró la gestión de Antonio Bonfatti y Hermes Binner lo hizo con uno elaborado por Jorge Obeid. Obviamente después se acuerda en la Legislatura y fue lo que ocurrió en esa oportunidad.

—**¿Usted sostiene que este año no hubo obstrucción legislativa?**

—Si me lo dicen podemos ver a qué aspecto se están refiriendo, pero hasta ahora hemos tratado todos los temas a pesar de que nunca hubo diálogo con el Poder Ejecutivo. He sido gobernador cuatro años, senador, y normalmente cuando hay un proyecto de ley que le interesa al Ejecutivo vienen ministros, se proponen cambios, se aceptan sugerencias de los legisladores, se buscan textos. Fíjese lo que pasa a nivel nacional donde hay discusiones, acuerdos entre bloques. Esto no ha pasado en la Cámara de Diputados. Lo que hemos aprobado es porque entendíamos que, responsablemente, teníamos que aprobarlo, pero no existe ese diálogo, ese ida y vuelta, esa búsqueda de acuerdos legislativos. Hasta ahora el Senado ha jugado ese rol pero no sé que pasará en el futuro. Pretendemos tener una interlocución más directa con el gobierno, sería mucho más razonable.

—**Ha vuelto a recorrer la provincia, ¿cuando habla con intendentes y presidentes comunales también dicen tener problemas por falta de diálogo con el gobierno?**

—Sí, está generalizado, incluso hay intendentes del peronismo que también lo dicen por lo bajo. Hay una falta de ida y vuelta entre gobierno y gobiernos locales en general –puede haber excepciones–, falta de respaldo, de proximidad del gobierno provincial en relación con los gobiernos locales. Es evidente, notable. No solo en términos de vinculación, de diálogo, sino también en términos económicos. Los municipios, por lo menos los del Frente Progresista, se han tenido que arreglar frente a la pandemia –en gran parte– con recursos propios.

—**¿Pero hubo recursos votados por la Legislatura para la pandemia?**

—Les votamos muchísimos recursos. Votamos la Ley Covid, que eran 15 mil millones de los cuales el compromiso era que 3 mil fueran para municipios y comunas, y a la fecha no han llegado a los 800 millones de ejecución de esa partida. Ha pasado casi un año.

***

**<span style="font-family: Helvetica, sans-serif; font-size: 1em; font-weight: 700; color: #F0060D;">#DATO: en el año 2020 se aprobaron 2220 proyectos en la Cámara de Diputados.</span>**

***

**<span style="font-family: Helvetica, sans-serif; font-size: 1em; font-weight: 600;">Sobre política y delito</span>**

—**¿Cómo observa lo ocurrido en el Senado con las denuncias de fiscales, el pedido de desafuero, etc.?**

—Por un lado, me entristece y me preocupa que después de casi 25 - 30 años (creo que lo último fue el caso Vanrell), volvamos a tener en Santa Fe –provincia que se caracterizaba por una calidad institucional distinta y superior a la mayoría de las provincias– la sospecha de vínculos entre la política y el delito o la corrupción. Que ocurra es preocupante. Como ciudadano y como dirigente político me preocupa. Por otro lado, para no ver solo la parte vacía del vaso, valoro positivamente que exista una justicia en Santa Fe autónoma, independiente, que haya fiscales que se han animado primero hay ir contra sus propios colegas y compañeros de trabajo -cosa muy poco habitual en la justicia y en general en todos los ámbitos institucionales. Es un logro para destacar. Después se animen a avanzar sobre eventuales complicidades políticas como en este caso. Tengo una sensación agridulce: por un lado, la preocupación de ver a la política presuntamente involucrada, pero también destacar que hemos sabido construir en Santa Fe una justicia que es autónoma de los poderes políticos.

—**Hay mucha controversia sobre la figura del ministro de Seguridad, Marcelo Sain, que llegó desde la provincia de Buenos Aires. Sain llegó a la política santafesina de la mano del Frente.**

—Nosotros no lo trajimos para funciones políticas, sino por su perfil académico para asesoramiento en el ámbito del Ministerio de Seguridad en temas específicos. Esa fue su función durante buena parte de mi gestión. Después él se presentó a un concurso organizado por el Poder Judicial y a partir de allí dejó de estar en la órbita del Poder Ejecutivo. Más allá de eso, hoy es un funcionario político del gobernador y en todo caso lo importante, lo que va a juzgar la gente, no es a los ministros sino al gobernador, por lo bueno y lo malo.

***

**<span style="font-family: Helvetica, sans-serif; font-size: 1em; font-weight: 700; color: #F0060D;">#DATO: en el año 2020 se presentaron 4190 proyectos en la Cámara de Diputados.</span>**

***

**<span style="font-family: Helvetica, sans-serif; font-size: 1em; font-weight: 600;">Sobre una posible candidatura</span>**

—**Se viene un año electoral. Hay mucha especulación sobre su posible candidatura. ¿Va a formar parte del proceso electoral?**

—No tengo una definición tomada porque en esto valen las aspiraciones personales, pero también la decisión del conjunto. Tengo claro que tengo una responsabilidad muy importante dentro de mi partido y dentro del Frente Progresista, no es que voy a tomar en soledad esa decisión. Se mezclan dos prioridades: una con el lugar importante que para mi siempre ha sido la senaduría nacional. Primero porque el Senado –lo vimos varias veces en los últimos meses– siempre termina siendo la caja de resonancia de los grandes debates nacionales. Es una cámara más reducida y suele ser un ámbito de mucho peso a nivel nacional. En segundo lugar, porque los senadores juegan un rol muy importante en la defensa de los intereses de su provincia. No lo he visto demasiado en los senadores que representaron a Santa Fe en los 30 y pico de años de democracia (salvo alguna excepción), pero he visto senadores de otras provincias que pelean como leones los recursos, las obras, el presupuesto. Quizás esto tenga que ver con una constante histórica de Santa Fe que ha sido perjudicada en la distribución por todos los gobiernos nacionales y sigue ocurriendo con el actual. Este es un lugar de representación política muy importante, destacado y un desafío muy grande para el que ocupa el lugar. También creo que las elecciones legislativas de medio término son una gran oportunidad para posicionar nuevas figuras, yo ya soy una persona demasiado conocida. Tenemos una camada de dirigentes del Frente de 40 y 40 y pico de años que no estaría nada mal que algunos de ellos tuvieran la oportunidad de encabezar listas el año próximo. Sería una buena forma de renovar la oferta electoral del frente. Me debate entre esas dos opciones y no lo tengo definido.

—**Hizo mucho ruido político esta semana un video de dos dirigentes radicales muy frentistas como Felipe Michlig y Maximiliano Pullaro llamando a construir un frente más amplio en Santa Fe. ¿Cómo lo toma usted?**

—Yo también comparto la idea de tener un frente más amplio. El Frente Progresista nació con cuatro partidos y terminó con ocho, fue ampliándose. En algún momento lo integró un sector del radicalismo, después la totalidad del radicalismo y, más tarde, un sector de ese partido eligió otros caminos electorales. Volver a reconstituir el espacio original y poder ampliarlo está en el objetivo de todos. Obviamente hay matices, hay formas, hay tiempos y eso depende mucho de lo que hagamos nosotros y de lo que haga el contexto político general, como esté posicionado el gobierno nacional, la provincia, como sea la oferta y el escenario electoral. Tenemos una identidad política, una trayectoria, una historia –llevo más de 30 años en cargos– vamos a ser leales y fieles a esa historia, a esa trayectoria. No vamos a hacer malabarismos en política. Una cosa es ampliar y otra cosa es un rejunte electoral. Podrá haber matices, discusiones dentro del Frente donde hemos tenido posiciones diferentes en muchos temas y vamos a tratar de lograr acuerdo y consenso que al final del camino nos permita seguir siendo una fuerza competitiva en la provincia de Santa Fe.

—**En el video plantean frenar al kirchernismo.**

—El peronismo es un universo muy variopinto. El kirchnerismo tiene mucha fuerza a nivel nacional, con Cristina y su sector político, no tanto acá en la provincia de Santa Fe donde vamos a enfrentar a un peronismo más heterogéneo como es el que representa Omar Perotti y que incluye sectores del kirchnerismo. Nosotros no debemos definirnos políticamente por el adversario sino por lo que somos. Apuesto a eso y apuesto a que podamos conformar un espacio de ideas progresistas, democrático, amplio, diverso, pero con valores éticos, valores republicanos y metas programáticas que podamos compartir y nos den una identidad política.