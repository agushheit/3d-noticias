---
category: Agenda Ciudadana
date: 2021-10-07T06:00:32-03:00
thumbnail: https://assets.3dnoticias.com.ar/MASSA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Ingresa a Diputados un proyecto para convertir planes sociales "en trabajo
  genuino"
title: Ingresa a Diputados un proyecto para convertir planes sociales "en trabajo
  genuino"
entradilla: El Gobierno nacional avanza en su objetivo de reconvertir los planes en
  puestos de trabajo, una idea que el presidente Alberto Fernández viene expresando
  desde el comienzo de su mandato.

---
El presidente de la Cámara de Diputados, Sergio Massa, anunció este miércoles que este jueves ingresará a ese cuerpo legislativo un proyecto de ley para convertir los planes sociales "en trabajo genuino", con impacto en diversas actividades de la economía nacional que se están reactivando.  
  
"Mañana ingresaremos la ley que convierte los planes sociales en trabajo genuino. Con metas, capacitaciones y herramientas de promoción para salir de la emergencia del plan y construir un puente hacia el trabajo para casi un millón de argentinos y argentinas", publicó Massa en su cuenta en la red social Twitter, aunque no incluyó los detalles del proyecto.  
  
El presidente Alberto Fernández destacó desde el comienzo de su gestión la decisión gubernamental de transformar los planes sociales en trabajo, lo cual se concretó en anuncios en diversos rubros.  
  
También Massa había destacado esa idea durante recorridas diversas, por ejemplo en una pyme de impresiones gráficas en el municipio de Pilar.  
  
"Ese trabajo es el que tenemos que recuperar y potenciar, transformando los planes sociales en planes de empleo. El plan social es la emergencia, lo permanente y digno es el trabajo. Tenemos que hacerlo por Ley, para obligar al Estado a recorrer ese camino", expresó en esa ocasión el exintendente de Tigre.  
  
En ese sentido, agregó: "Necesitamos un nuevo modelo de asistencia social, con planes que incluyan prestaciones laborales y terminalidad educativa obligatoria. Condiciones fundamentales para construir el futuro del país. Tenemos la capacidad y recursos humanos para hacerlo posible".  
  
El Gobierno tiene previsto sumar a esta propuesta de reconversión laboral a la industria textil, de extenso desarrollo en el país.

En esa línea, el ministro de Desarrollo Social de la Nación, Juan Zabaleta, también destacó este miércoles que el Gobierno busca "reconvertir los planes sociales en empleo genuino" con diversas iniciativas para la reconstrucción del país .  
  
"Toda herramienta, instrumento o decisión que vaya en el camino al trabajo genuino, registrado y la reconstrucción del empleo, el Gobierno del presidente Fernández no va a tener dudas en discutirlo; así lo hacemos con empresarios y empresarias, con dirigentes de organizaciones sociales y sindicatos", indicó.  
  
El ministro afirmó a la prensa que el objetivo en su área es "transformar el Plan Potenciar Trabajo en trabajo genuino" e insistió en la necesidad de avanzar hacia "reconvertir planes sociales en empleo genuino en otros sectores de la producción y de la economía", donde, dijo, se está "viendo alguna recuperación".  
  
Mencionó "experiencias vinculadas al sector de la cosecha, productores de naranja, arándano, pera, manzana, algodón y yerba que pueden contratar beneficiarios del Plan Potenciar Trabajo", y un convenio firmado esta semana con la Unión Obrera de la Construcción (Uocra) y con la Cámara Argentina de ese sector para capacitar beneficiarios.  
  
Zabaleta resaltó que "todo lo que propongan hombres y mujeres de sectores empresariales y representantes de los movimientos sociales va a estar en discusión, siempre y cuando vayan direccionados a la obtención de trabajo genuino".  
  
El ministro también anunció la permanencia de la Tarjeta Alimentar, los planes Potenciar Trabajo y de urbanización de barrios al confirmar que "todo eso continúa, tiene que ver con la decisión del Presidente de gobernar la Argentina, de transformarla y reconstruirla, y no con una fecha de elecciones".  
  
Fuentes oficiales habían adelantado que el Gobierno nacional avanza en su objetivo de reconvertir los planes sociales en puestos de trabajo genuino, una idea que Alberto Fernández viene expresando desde el comienzo de su mandato, y ya incorporó a trabajadores rurales y de la construcción al plan que forma parte de la reactivación económica y de la etapa actual de la pospandemia.  
  
Si bien la decisión había sido tomada antes de la irrupción del coronavirus, la emergencia sanitaria obligó al Poder Ejecutivo a reordenar sus prioridades, por lo que recién iniciado 2021 el Estado retomó ese punto de la agenda enfocado en la coexistencia entre la asistencia social y el mercado laboral.  
  
Así, tanto el Presidente como los dos ministros de Desarrollo Social de su gestión (Daniel Arroyo, de 2019 a 2021, y su sucesor Zabaleta) pusieron el foco en la necesidad de transformar los distintos planes en puestos de trabajo, en un acuerdo tripartito con las cámaras empresarias y las organizaciones de trabajadores.  
  
El primer paso fue dado en agosto pasado con los rurales, que en un total de 250 mil pasaron a desempeñar sus funciones en el terreno mientras mantienen la asistencia del Estado, sin perder las asignaciones ni ninguna de sus atribuciones.

A ese inicio le siguió el programa "Construir Trabajo e Igualdad", anunciado el lunes último por el Gobierno y que implica la transformación de planes en trabajo en el sector de la construcción.  
  
El Gobierno tiene previsto sumar a esta propuesta de reconversión laboral a la industria textil, de extenso desarrollo en el país, y la gastronomía, entre otros sectores de la economía que, además de impactar en la multiplicación de puestos de trabajo, favorecen la producción y el consumo.