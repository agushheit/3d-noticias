---
category: Estado Real
date: 2021-02-18T06:08:53-03:00
thumbnail: https://assets.3dnoticias.com.ar/paritaria.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Gobierno de Santa Fe
resumen: La provincia retomó las reuniones paritarias con los gremios docentes y de
  la administración central
title: La provincia retomó las reuniones paritarias con los gremios docentes y de
  la administración central
entradilla: Se llevaron adelante encuentros con representantes de Amsafe, Sadop, Uda
  y Amet; y de UPCN y ATE.

---
El gobierno de la provincia, a través del ministro de Trabajo, Empleo y Seguridad Social, Juan Manuel Pusineri retomó este miércoles el diálogo paritario con los representantes de los gremios docentes y de la administración central.

En primer término, de la reunión paritaria docente participaron funcionarios del Ministerio de Educación y representantes de la Asociación del Magisterio de Santa Fe (Amsafe), del Sindicato Argentino de Docentes Privados (Sadop), de la Unión de Docentes Argentinos (UDA) y de la Asociación del Magisterio de Enseñanza Técnica (Amet).

Al respecto, Pusineri destacó que se realizaron “avances muy importantes en lo que tiene que ver con el regreso a la escuela”; y anunció que los maestros y maestras podrán acceder al Boleto Educativo Gratuito “con independencia del ingreso que tengan”.

Asimismo, teniendo en cuenta que este jueves 18 se realizará la paritaria nacional docente, se acordó esperar los avances de dicha reunión para programar la cuestión salarial de la provincia de Santa Fe. Al respecto, el titular de la cartera laboral indicó que ”seguirán funcionando las comisiones de condiciones laborales para recoger las inquietudes de los gremios. Con independencia de que no tengamos el marco referencial nacional en cuanto a aumentos porcentuales, hacia el interior de los escalafones, siempre hay ajustes que hacer”.

Por último, Pusineri manifestó, con respecto a la reducción de frecuencias o cambios de horarios del transporte público, por la pandemia, que “el Ministerio de Educación habilitará un mecanismo para que puedan notificar esa situación y no se vean perjudicados por una inasistencia cuando no tuvieron la posibilidad de llegar al establecimiento escolar”.

Por su parte, el secretario de Educación, Víctor Debloc, manifestó que “se abordaron diferentes temas, entre los que se acordó ratificar la vigencia de los códigos Nº 500 (licencia Covid) y la necesidad de comunicar a las escuelas que están vigentes para un adecuado uso de los mismos”. En este caso, las licencias son por edad, por factores de riesgo o por tener hijos en edad escolar a cargo.

Por otra parte, ante la inminente habilitación de la inscripción del boleto educativo gratuito, el funcionario provincial aclaró que “en el encuentro se resolvió la eliminación de los topes de ingreso para asegurar el beneficio del boleto educativo gratuito a todos los asistentes escolares, docentes y estudiantes”.

Por último, Debloc indicó que “se acordó garantizar que los docentes que voluntariamente quieran hacerse el hisopado podrán hacerlo y para ellos se establecerá la logística y los lugares tratando que se pueda distribuir en diferentes centros de la provincia, a la vez de avanzar en la organización de los centros de vacunación”.

Asimismo, se pautaron las próximas reuniones con las comisiones de Condiciones de Trabajo y de Pautas Salariales; y se convino un nuevo encuentro para el 25 de febrero venidero, también bajo la misma modalidad.

Del encuentro también participaron los secretarias de Gestión Territorial Educativa, Rosario Cristiani; y de Administración de la cartera educativa, Cristian Kuverling; y el director provincial de Enseñanza Privada, Rodolfo Fabucci.

**PARITARIA ESTATALES**

Con respecto a la reunión paritaria con representantes de la Unión del Personal Civil de la Nación (UPCN) y de la Asociación de Trabajadores del Estado (ATE), se acordaron “los instructivos para que se gestionen las titularizaciones de las subrogancias de las categorías 3 a 7, en base a la planta permanente de cada Ministerio. Asimismo se va a poner en marcha una comisión que analice el blanqueo de las sumas que se acordaron en las paritarias del año pasado”, indicó Pusineri; y detalló que “el relevamiento llevado adelante conjuntamente con los sindicatos indica que hay alrededor de mil trabajadores en las jurisdicciones ministeriales en condiciones de modificar su situación laboral”.

Por último, el ministro de Trabajo adelantó que “mañana se reúne la comisión técnica salarial con representantes de Hacienda, Función Pública y de los gremios, la cual tendrá como marco macroeconómico la resolución de la paritaria nacional y el acuerdo de precios y salarios. Estos instrumentos que se generan entre gobiernos, empresarios y empleadores en una instancia nacional son de referencia ineludible para las provincias”, concluyó.