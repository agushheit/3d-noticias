---
category: Deportes
date: 2021-08-09T06:00:04-03:00
thumbnail: https://assets.3dnoticias.com.ar/argentinajjoo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: 'Balance argentino: diez momentos que deja Tokio 2020'
title: 'Balance argentino: diez momentos que deja Tokio 2020'
entradilla: La delegación argentina no pudo conseguir ningún oro tras cuatro Juegos
  consecutivos (Atenas 2004, Beijing 2008, Londres 2012 y Río de Janeiro 2016) con
  medallas doradas, pero tuvo grandes actuaciones.

---
La vigencia de Las Leonas, la gloria del vóleibol y el rugby; las despedidas de Paula Pareto, Luis Scola y Pedro Ibarra; la revelación de Lucas Guzmán y las frustraciones del seleccionado de fútbol, Santiago Lange, Cecilia Carranza o Delfina Pignatiello constituyen, entre otros, los hechos más destacados de los Juegos Olímpicos Tokio 2020 que se clausuraron este domingo en la capital japonesa.

El hockey sobre césped argentino volvió a subirse a un podio olímpico como sucede ininterrumpidamente desde Sydney 2000, en los tiempos de la construcción de la mística de Las Leonas. Esta vez fueron nuevamente las chicas las que consiguieron una medalla de plata, la tercera en todo el ciclo, después de quedar relegadas en Río 2016, donde se consagraron los varones con un sorpresivo oro.

El DT Carlos "Chapa" Retegui formó un equipo con 13 debutantes olímpicas que honró el historial competitivo del seleccionado femenino y estuvo a la altura de los mejores del mundo, incluso del campeón invicto Países Bajos.

Las Leonas sumaron la quinta medalla de los últimos 21 años (3 platas y 2 bronces) y confirmaron que los rasgos históricos de su identidad permanecen claramente reconocibles en su fisonomía colectiva.

Sin demasiado crédito en el exterior pero fundamentado en su propia confianza interna, el seleccionado argentino protagonizó el hecho deportivo más relevante del vóleibol nacional desde Seúl '88: la conquista del bronce olímpico.

El equipo de Marcelo Méndez, con el liderazgo de Facundo Conte, repitió la gesta llevada a cabo por aquella formación "albiceleste" en la que destacaban su padre Hugo, Daniel Castellani, Waldo Kantor y Jon Uriarte.

A poco tiempo de cumplir una discreta actuación en la Liga de Naciones 2021, Argentina maduró como un conjunto confiable y determinado para darle disputa a las potencias de la disciplina. En su camino al bronce se impuso sobre Estados Unidos, Francia, Italia y Brasil, último campeón olímpico.

El binomio Santiago Lange-Cecilia Carraza Saroli llegó a Tokio 2020 como el equipo de vela con mayores posibilidades de obtener una medalla, basado en su condición de ganador del oro de la clase Nacra 17 en Río 2016.

Los resultados sobre las aguas de la Bahía de Sagami no fueron los esperados, especialmente en las regatas regulares. Así, los abanderados argentinos llegaron a la medal race sin oportunidades de pelear por el podio y se despidieron con una digna victoria en esa prueba, lo que les brindó un diploma olímpico.

Una actuación meritoria pero por debajo de las propias expectativas del equipo, que días después de la competencia anunció su separación. Fin de ciclo.

En estos Juegos Olímpicos, el rugby -en su versión seven- se transformó en el 19no. deporte en aportar una medalla para Argentina en la historia. Sucedió en su segunda participación después del debut de la disciplina en Río 2016 y tres años del oro logrado en los Juegos de la Juventud en Buenos Aires.

Con una base de ambos equipos, el entrenador Santiago Gómez Cora conformó gran un equipo que sólo perdió con Nueva Zelanda y Fiji, campeón hace cinco años en Brasil.

El bronce quedó asegurado con la victoria sobre Gran Bretaña, que significó una revancha por la eliminación en los cuartos de final de la anterior edición olímpica.

Los Juegos de Tokio 2020 quedaron en la memoria como los últimos de tres grandes campeones olímpicos: la judoca Paula Pareto, el basquetbolista Luis Scola y el defensor Pedro Ibarra, histórico miembro de Los Leones. También como el de la despedida para Gonzalo Carou y Sebastián Simonet, dos referentes de Los Gladiadores del handball.

La "Peque" Pareto, ya consagrada como leyenda olímpica, mostró otra vez su coraje para competir y obtuvo un diploma luego de quedar eliminada en un repechaje por la medalla de bronce, al que llegó condicionada por una lesión en el codo.

La campeona en Río 2016, también oro en el Mundial Astaná 2015 y en los Panamericanos Guadalajara 2011, cerró su etapa olímpica con dos preseas ya que en Beijing 2008 se había colgado el bronce.

A los 41 años, Luis Scola clausuró su historia como jugador del seleccionado argentino de básquetbol, iluminado por el brillo de los resultados con la Generación Dorada pero especialmente reconocido por su inobjetable condición de líder.

Con "Luifa", oro en Atenas 2004 y bronce cuatro años más tarde en Beijing, se marcha una figura admirada a nivel interdisciplinario y un espejo de compromiso y dedicación hacia la camiseta celeste y blanca.

Con similar perfil, Pedro Ibarra (35) cerró su carrera en Los Leones, que atesora el oro olímpico en Río como instante único de una trayectoria de 17 años y más de 300 partidos internacionales.

Así como el correntino Sebastián Crismanich sorprendió al mundo con su medalla de oro en Londres 2012, el bonaerense Lucas Guzmán estuvo cerca de dar la nota nuevamente en taekwondo, lo que se constituyó en una de las buenas noticias para el deporte argentino en Tokio 2020.

El atleta de 27 años, oriundo de la localidad de Merlo, llegó hasta las semifinales de la categoría -58 kilos y luego quedó al borde del podio olímpico tras caer en un repechaje por la medalla de bronce.

El diploma obtenido se suma a la vitrina en la que ya guardaba la presea de bronce de los Juegos Panamericanos Toronto 2015, la de oro de Lima 2019 y el bronce del Mundial Manchester 2019.

El seleccionado sub 23, campeón del torneo Preolímpico el año pasado, llegó a Tokio con un plantel armado en base a la disponibilidad y no al deseo del entrenador, que fue víctima de una situación recurrente en la historia: el retaceo de futbolistas.

El "Bocha" Fernando Batista no pudo contar con los tres jugadores mayores autorizados por el reglamento ni tampoco con los menores que disputaron la Copa América con el seleccionado mayor (Lautaro Martínez, Gonzalo Montiel, Cristian Romero,

Lisandro Martínez, Nahuel Molina, Exequiel Palacios, Nicolás González, Nicolás Domínguez y Julián Álvarez).

El calendario de la Selección principal; la negativa de los clubes argentinos con competencias internacionales y las trabas puestas por clubes del exterior limitaron su elección.

De todos modos, acudió con un plantel de jerarquía y el equipo no dio la talla: perdió en el debut con Australia, el ganó a Egipto, empató con España y marchó en primera ronda como hace cinco años en Brasil.

La nadadora Delfina Pignatiello es una de las mayores promesas del deporte argentino pero en Tokio 2020 no tuvo los resultados esperados después de las dos medallas de plata en los Juegos de la Juventud Buenos Aires 2018 y las tres de oro en los Panamericanos Lima 2019.

Con 21 años, en su primer Juego de mayores, quedó última en las series de las dos pruebas que disputó: los 800 y 1.500 metros libres.

Pero lo peor de su experiencia en Tokio 2020 acaso fue los ataques recibidos en las redes sociales que la llevaron a una decisión drástica: eliminar sus cuentas en Twitter, Youtube y Twitch y borrar contenido de otras. "La gente es muy cruel y por más que ignore quiero cuidar mi salud mental por sobre todas las cosas", justificó.

El judoca Emmanuel Lucenti protagonizó la mayor decepción argentina en los Juegos: duró apenas 28 segundos. El búlgaro Ivailo Ivanov lo sorprendió con una llave y le ganó por ippón en la prueba eliminatoria de 81 kilogramos, por los 16vos. de final.

El tucumano salió del tatami consternado y necesitó más tiempo que lo que duró el combate para procesar lo sucedido: "No entiendo nada; prefería haberme fundido antes que perder así. Nunca pensé que Ivanov me iba a atacar tan rápido. Esto me deja muy bajoneado porque nunca había perdido tan temprano. No sé qué habrá analizado él de mí para atacarme enseguida, pero éste es un golpe a mi orgullo", confesó.

Luego vinculó su mal desempeño a una preparación que consideró deficitaria por las restricciones derivadas de la pandemia: "Pero también quiero decir que es muy difícil venir a un Juego Olímpico entrenando solamente en Tucumán y Santiago del Estero.

No pude ir a Croacia hace un mes por un tema familiar, pero no sé qué hubiera pasado si hubiese tenido más apoyo, porque a mí no me aprovecharon ni me respetaron".

Lucenti, de 36 años, cerró su cuarta experiencia olímpica luego de clasificarse 21ro. en Beijing 2008, 7mo. en Londres 2012 y 9no. en Río 2016.

La frustración por malos rendimientos es parte del deporte, pero la que se experimenta por no poder competir luego de cinco años de preparación es un golpe durísimo de asimilar. Eso lo ocurrió al santafesino Germán Chiaraviglio, que dio positivo de Covid-19 en Japón a dos días de presentarse en la prueba salto con garrocha.

El múltiple medallista panamericano, de 34 años, se disponía a vivir sus segundos Juegos consecutivos.