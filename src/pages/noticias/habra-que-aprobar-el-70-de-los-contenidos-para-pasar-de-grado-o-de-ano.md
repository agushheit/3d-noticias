---
category: Agenda Ciudadana
date: 2021-07-18T06:15:10-03:00
thumbnail: https://assets.3dnoticias.com.ar/70PORCIENTO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Habrá que aprobar el 70% de los contenidos para pasar de grado o de año
title: Habrá que aprobar el 70% de los contenidos para pasar de grado o de año
entradilla: La cartera educativa nacional realizará esa propuesta el próximo martes,
  cuando se reúna el Consejo Federal de Educación.

---
El ministerio de Educación de la Nación propondrá el próximo martes a los ministros de todo el país "la aprobación del 70% de los contenidos priorizados como mínimo" para promocionar de grado o año, además de "acordar los criterios de evaluación".

En la próxima Asamblea del Consejo Federal de Educación, que se realizará el próximo martes, el Ministerio promoverá "un acuerdo de trabajo pedagógico para el segundo semestre del año tendiente a incrementar el tiempo de enseñanza, fortalecer la presencialidad y los aprendizajes en todo el territorio".

"El proyecto promueve que las ministras y ministros alcancen un acuerdo que considere como criterio para la promoción de un grado/año a otro tener aprobados al menos el 70% de los contenidos que fueron priorizados para este año escolar, en cada una de las jurisdicciones", señaló la cartera educativa en un comunicado.

En ese marco, precisó que se buscará fijar "metas que permitan evaluar si las y los estudiantes están en condiciones de avanzar en sus trayectorias educativas".

El documento que presentará el Ministerio de Educación se centra en el incremento de los tiempos de enseñanza para fortalecer la calidad de los aprendizajes desde agosto hasta diciembre y en febrero del 2022 para aquellas y aquellos estudiantes que lo necesiten.

Además, el Ministerio hizo hincapié en el fortalecimiento de la enseñanza en el primer ciclo de la primaria (de primero a tercer grado) y en que la promoción de 3ro. a 4to. grado de la primaria se dará cuando las y los chicos hayan logrado su alfabetización inicial, es decir, si han adquirido las competencias de lectura y escritura básicas.

También, contempla que "los estudiantes que hayan tenido trayectorias intermitentes o de baja intensidad" y necesiten "más tiempo y acompañamiento para apropiarse de los aprendizajes requeridos", puedan contar con "instancias específicas de intensificación de la enseñanza".

"Se trata de un período especial de dictado de clases y de trabajo pedagógico en febrero 2022 con las y los estudiantes que terminan salita de 5 en el nivel inicial; 6to. o 7mo. grado de la primaria; o 5to. 6to. ó 7mo. año de la secundaria", detalló el Ministerio.