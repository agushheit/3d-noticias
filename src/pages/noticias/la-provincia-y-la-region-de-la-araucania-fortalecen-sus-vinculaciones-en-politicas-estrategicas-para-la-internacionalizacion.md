---
category: Estado Real
date: 2021-08-02T06:00:06-03:00
thumbnail: https://assets.3dnoticias.com.ar/ARAUCANIA.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La provincia y la región de la Araucanía fortalecen sus vinculaciones en
  políticas estratégicas para la internacionalización
title: La provincia y la región de la Araucanía fortalecen sus vinculaciones en políticas
  estratégicas para la internacionalización
entradilla: Durante el mes de julio se organizaron cinco reuniones sectoriales con
  el objetivo de conocer los enfoques de gestión de diversos temas, identificar desafíos
  comunes y oportunidades de cooperación y negocios.

---
La Secretaría de Cooperación Internacional e Integración Regional de la Provincia organizó una serie de reuniones sectoriales entre funcionarios santafesinos con sus pares de La Araucanía (Chile), con el objetivo de informar las principales políticas públicas de la provincia y detectar áreas de interés para incrementar los intercambios provinciales en comercio e inversiones, turismo, ciencia tecnología e inversión. Se presentaron también en mesas especiales las políticas públicas de ambiente, cambio climático y género.

En los encuentros participaron la ministra de Ambiente y Cambio Climático, Erika Gonnet; el secretario de Turismo, Alejandro Grandinetti; la secretaria de Mujeres, Géneros y Diversidad, Florencia Marinaro; el secretario de Comercio Exterior, Germán Burcher; y la secretaria de Ciencia, Tecnología e Innovación, Marina Baima, acompañados con sus equipos de trabajo.

La Araucanía convocó a diferentes áreas del Gobierno Regional y delegados regionales de organismos nacionales, entre ellos al Director regional del Servicio Nacional de Turismo, ProChile Araucanía,  la Directora regional del Servicio Nacional de la Mujer y Equidad de Género (SERNAMEG), profesionales de la Secretaría Regional Ministerial (SEREMI) de Medio Ambiente, el encargado del Ciencia, Tecnología e Innovación y la encargada de Gestión Ambiental del Gobierno Regional de La Araucanía, e InvestAraucanía.

En el cierre del ciclo, la Secretaría de Cooperación Internacional e Integración Regional, Julieta de San Félix, destacó que “la realización de estas reuniones en forma virtual demostró que Santa Fe avanza en su agenda de internacionalización de manera creativa en un mundo disruptivo, que nos demanda abordajes novedosos para incrementar la vinculación de la provincia, de nuestras industrias y de nuestros ciudadanos con el mundo”. Esto, en consideración de que los tradicionales viajes y misiones institucionales no son posibles en el actual contexto sanitario. Esta modalidad virtual permitió un abordaje en profundidad de temas de interés mutuo y se contó con el compromiso de importantes funcionarios de ambos gobiernos.

La construcción de una agenda bilateral para el desarrollo de los territorios

Durante las reuniones los socios fueron identificando temáticas en las que se continuará cooperando por medio del intercambio de experiencias, actividades organizadas en conjunto y el fortalecimiento de capacidades locales para el desarrollo de los territorios.

En materia turística Santa Fe y La Araucanía comparten el interés en la construcción de productos conjuntos innovadores como el fomento de la pesca deportiva contra temporada y el desarrollo de un corredor turístico para motorhomes. Se acordó la convocatoria a PyMES a un ciclo de ronda de negocios sectoriales virtuales para potenciar sus exportaciones y, con el objetivo de fortalecer los ecosistemas locales de innovación, se organizará una maratón de ideas que permita identificar desafíos comunes de las startups santafesinos y de La Araucanía.

Dado que Santa Fe se encuentra iniciando la segunda etapa de implementación del Presupuesto con Perspectiva de Género, valora el aprendizaje de la experiencia del Ministerio de la Mujer y Equidad de Género de Chile en adquisiciones y compras públicas con enfoque de género. Asimismo, el Ministerio de Ambiente y Cambio Climático de Santa Fe brindará apoyo al Gobierno Regional de la Araucanía en la formulación de su Plan regional de Medio Ambiente por medio de la transferencia santafesina en la elaboración de un Atlas de Riesgos.  

**El proyecto**

La iniciativa propuesta es parte de un proyecto de Cooperación Descentralizada Sur Sur (CDSS) entre el Gobierno Regional de La Araucanía y la provincia de Santa Fe, que busca el fortalecimiento de la URAI de La Araucanía, a través de la transferencia de conocimientos y experiencias de la contraparte santafesina.

El proyecto fue seleccionado por el Mecanismo Estructurado para el Intercambio de Experiencias de Cooperación Sur-Sur (MECSS) del PIFCSS, cuyo primer objetivo estratégico consiste en fortalecer las capacidades institucionales de los organismos rectores y actores claves de la Cooperación Sur-Sur y la Cooperación Triangular a través del financiamiento de proyectos para intercambiar conocimientos y experiencias entre los países miembros, de acuerdo a las capacidades y fortalezas que cada uno de los países ha desarrollado a nivel de su institucionalidad o políticas públicas.