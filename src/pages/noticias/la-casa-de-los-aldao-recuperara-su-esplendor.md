---
category: Agenda Ciudadana
date: 2020-12-19T11:57:04Z
thumbnail: https://assets.3dnoticias.com.ar/1912-casa-aldao.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: La Casa de los Aldao recuperará su esplendor
title: La Casa de los Aldao recuperará su esplendor
entradilla: Ubicada en el centro fundacional de la ciudad de Santa Fe, es Monumento
  Histórico Nacional. Los trabajos comprenden la recuperación y puesta en valor de
  la estética edilicia, tal como fue concebida originalmente.

---
El Ministerio de Infraestructura, Servicios Públicos y Hábitat, anunció que está próximo a realizar el **llamado a licitación para la recuperación y puesta en valor del edificio Casa de los Aldao**, ubicado en el centro histórico de la ciudad de Santa Fe, calle Monseñor Zaspe 2861.

***

**Es uno de los exponentes de la arquitectura doméstica del período hispánico que se conserva en el país y, actualmente, es sede de la Junta Provincial de Estudios Históricos.**

***

Consultada sobre esta intervención patrimonial, la ministra Silvina Frana destacó: «Recuperar estos edificios emblemáticos para la sociedad santafesina significa preservar la identidad histórica, arquitectónica y cultural de una casa que logró un protagonismo histórico en la conformación de la República. Actualmente, esta casa constituye una de las pocas muestras de la arquitectura hispánica que se conservan en el país».

«Mediante esta rehabilitación, buscamos recuperar la habitabilidad en el edificio, conservando sus espacios y estructuras, para que pueda seguir desarrollando sus actividades la Junta Provincial de Estudios Históricos, y devolverle a los santafesinos un espacio que recupera la memoria y tiene un inconmensurable valor arquitectónico-patrimonial», comentó Frana.

Por su parte, la secretaria de Arquitectura y Obras Públicas, Leticia Battaglia, brindó detalles del pliego: «Se va a realizar una intervención integral de la casona histórica y la biblioteca. Hemos detectado problemas de filtraciones, fundamentalmente en los techos de teja y en la losa de la biblioteca. Al tratarse de un edificio patrimonial, el objetivo es devolver y preservar el estado original de la casa en cuanto a forma, color y diseño, por esto estamos trabajando en conjunto con la Comisión Nacional de Monumentos».

«Se trata de un claro ejemplo de arquitectura de tierra, por lo que el tratamiento de las paredes (fisuras, revoques y pintura)  se llevará adelante con el uso de técnicas y materiales similares a los originales, es decir, el uso de morteros de barro para el interior y de cal para el exterior, garantizando que la capa resultante sea permeable al vapor del agua, evitando se conserve la humedad en el interior de la tapia y se deshaga por dentro», explicó la secretaria Battaglia.

***

## **LA INTERVENCIÓN**

Las zonas a intervenir corresponden al cuerpo histórico de la casona: dos habitaciones en planta baja y una en planta alta, incluyendo los pasadizos entre los dos patios (frente y fondo), y el bloque lateral donde hoy funciona el área administrativa y biblioteca de la Junta Provincial de Estudios Históricos.

**Los trabajos a realizar comprenden:**

* Impermeabilizaciones de las cubiertas a los fines de asegurar estanqueidad en los bloques a intervenir;
* Reconstrucción de los revoques interiores y exteriores, previa reparación de las fisuras y grietas de los paramentos;
* Pintura general (interior y exterior);
* Demolición del depósito ubicado en el sector suroeste del conjunto, donde se evidencian importantes hundimientos y socavones, lo que llevó a la aparición de grandes grietas en muros comprometiendo la estabilidad estructural de dicho sector.
* Se prevé, además, un tratamiento de la estructura de madera de la cubierta de tejas coloniales de la casona histórica ante el accionar de xilófagos y una reparación integral de todas las piezas de madera de aberturas, rejas, alero exterior y balcón.

***

## **EL ACTO**

**La apertura de sobres se realizará el próximo martes 22 de diciembre, a las 10 horas**, en la sala de reuniones del Ministerio de Infraestructura, Servicios Públicos y Hábitat, sito en calle 3 de Febrero 2649 (planta baja) de la ciudad de Santa Fe.