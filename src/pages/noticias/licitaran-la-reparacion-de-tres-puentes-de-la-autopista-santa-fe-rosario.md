---
category: Estado Real
date: 2021-06-07T06:15:05-03:00
thumbnail: https://assets.3dnoticias.com.ar/AUTOPISTA.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Licitarán la reparación de tres puentes de la autopista Santa Fe-Rosario
title: Licitarán la reparación de tres puentes de la autopista Santa Fe-Rosario
entradilla: 'Las estructuras se ubican sobre el río Carcarañá y en cercanías al acceso
  a Coronda. '

---
El Ministerio de Infraestructura, Servicios Públicos y Hábitat, a través de la Dirección Provincial de Vialidad (DPV), realizará la apertura de ofertas para ejecutar los trabajos de conservación y reparación de tres puentes localizados en cercanías a Villa La Ribera y Coronda, atento a los informes efectuados sobre el estado de las construcciones.

Al respecto, el administrador general de la DPV, Oscar Ceschi indicó que "desde la División Puentes, dependiente de la Dirección General de Conservación, señalaron la importancia de intervenir rápidamente, por eso decidimos encarar estas tareas".

Además, agregó que "en la autopista Rosario - Santa Fe tenemos activos trabajos de repavimentación en distintos tramos y la construcción del intercambiador en el acceso norte a Santo Tomé. Y, además, estamos por comenzar la construcción del nuevo sistema de iluminación para el acceso a Santa Fe", repasó Ceschi.

“Desde el gobierno hay una firme decisión de seguir dotando de infraestructura a todo el territorio, en este caso son dos nuevas licitaciones, y con otras seis que en el trascurso de este mes estaremos celebrando".

**Las obras**

Los puentes sobre el río Carcarañá se encuentran en el límite entre los departamentos Iriondo y San Lorenzo, sobre el kilómetro 29, en cercanías al acceso a Villa La Ribera y el área de servicios. El objeto de la obra se basa en el mantenimiento de los cuatro estribos, que presentan socavaciones, tantos en las losas de acceso como en el cantero central de desagüe en la cabecera sur. En el proyecto, se incluye el desbosque, destronque y limpieza de los extremos; la protección flexible de hormigón en las áreas contiguas a las losas y el repintado de barandas metálicas. La licitación será el jueves 17 de junio, contará con el presupuesto oficial de $31.109.778,61 y el plazo de ejecución de obras se fijó en cuatro meses.

El puente sobre el kilómetro 106, está localizado entre los accesos a las localidades de Arocena y Coronda, en el departamento San Jerónimo. La superestructura existente tiene una luz total de 54,6 metros, está compuesta por un tablero principal, reposado sobre ocho vigas de acero, a su vez apoyadas sobre pilas de hormigón armado.

La reparación de la misma se debe al impacto accidental de un camión, que provocó la rotura de una de las vigas principales y el desplazamiento del tablero. La solución propuesta comprende cortar el alma de las vigas afectadas; construir y colocar nuevas piezas; y por último elevar el tablero para retomar la posición original.

Se destaca que entre las tareas complementarias se deberá efectuar un desvío provisorio, la reparación de barandas y una nueva carpeta asfáltica. La licitación tendrá lugar el viernes 18 de junio con un presupuesto oficial de $13.957.201,22 y un plazo de para la terminación fijado en seis meses.