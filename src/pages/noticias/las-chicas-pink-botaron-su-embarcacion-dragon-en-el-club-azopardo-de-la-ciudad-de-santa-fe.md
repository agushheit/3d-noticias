---
category: La Ciudad
date: 2021-10-25T06:15:09-03:00
thumbnail: https://assets.3dnoticias.com.ar/CHICASPINK.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Las Chicas Pink botaron su embarcación "Dragón" en el Club Azopardo de la
  ciudad de Santa Fe
title: Las Chicas Pink botaron su embarcación "Dragón" en el Club Azopardo de la ciudad
  de Santa Fe
entradilla: "La asociación de mujeres recuperadas de cáncer de mama inauguró su góndola
  china en aguas del río Santa Fe. Participaron de la botadura el gobernador Omar
  Perotti y el intendente Emilio Jatón.\n\n"

---
Días atrás, un medio de la cuidad, daba a conocer la historia de las 25 mujeres que conforman la asociación civil Chicas Pink de Santa Fe. Muchas de ellas lucharon y vencieron al cáncer de mama y ahora, a bordo de su propio bote Dragón, remarán sobre la laguna Setúbal para concientizar sobre esta enfermedad, previéndola mediante de su detección precoz con estudios médicos (controles y mamografías).

 Este viernes, quedará grabado a fuego en la memoria de estas remeras, ya que en el Club Náutico Azopardo, realizaron la ceremonia inaugural para botar su embarcación, de origen chino (de 320 kilos) que fue adquirida en marzo y llegó a la ciudad en abril. Fue traída desde el Delta del Tigre y ahora tiene guardería en Azopardo.

 La presidenta de la Asociación Chicas Pink, Viviana Cámara, describió durante la botadura que "esta asociación está constituida por mujeres sobrevivientes de cáncer de mama y entusiastas, quienes practicamos el remo en Bote Dragón como forma de rehabilitación linfática y muscular. Actualmente somos 25, todas con un empuje impresionante, llenas de energía, mujeres auténticas, vivas y con alegría".

 Del acto participaron el gobernador Omar Perotti y el intendente Emilio Jatón, entre otros funcionarios provinciales y municipales, además de las autoridades de la entidad naútica. En este sentido, Perotti destacó la actitud de las Chicas Pink "son una demostración clara de la prevención y de las posibilidades de recuperación, haciéndolas visibles".

 El gobierno provincial fue partícipe en la adquisición de la embarcación, al otorgar el 8 de marzo, en conmemoración del Día de la Mujer, un subsidio de $ 450.000. El resto del dinero, lo consiguieron a través de beneficios, colectas y donaciones. Además, durante la actividad se anunció que el gobierno de la Provincia otorgará otro aporte para construir el muelle, lo que permitirá que las mujeres puedan remar todo el año.

 A su turno, el intendente Jatón afirmó que "Santa Fe conoce la historia de las Chicas Pink, que se paraban frente al Puente Colgante para contar su lucha y asumieron un desafío enorme", y aseguró que "en la vida, los desafíos aparecen sin que uno los provoque, como les pasó a ustedes. La historia es cómo tomamos esos desafíos y qué hacemos con ellos: los guardamos o los convertimos en fortaleza. Y las Chicas Pink no sólo hicieron del desafío una fortaleza sino que además, lo hicieron colectivo".

 Mientras que el presidente del Club Azopardo, Fabio Cremón, dio la bienvenida a las autoridades presentes. "Es un honor que nos acompañen y puedan visualizar la función social que tienen los clubes. Hoy es un momento muy especial para las Chicas Pink, es un orgullo enorme que formen parte de nuestra casa, que tienen una labor fundamental. El Bote Dragón es el resultado del esfuerzo", manifestó.

 **Concientización**

Por su parte, la ministra de Igualdad, Género y Diversidad, Celia Arena, indicó que "es sumamente importante pensar en el después, en el acompañamiento, cuando te atraviesa algo que te cambia la perspectiva de la vida. Ustedes transmiten eso y tienen la posibilidad de sumar a más personas y ser un sostén para el resto de las mujeres y para todo el grupo familiar.

 A su vez, Arena remarcó que "el cáncer de mama se lleva en la provincia de Santa Fe más de 500 mujeres, lo que lo convierte en la causa de mayor mortalidad en las mujeres mayores de 50 años. Es importante que tomemos conciencia de que la detención precoz salva vidas y que por eso hay que retomar los controles. En la pandemia bajó un 70% la cantidad de estudios y mamografías que se hicieron, pero hoy la acción de todas ustedes hace que las mujeres tomemos conciencia de la importancia trascendental que tiene retomar esos controles y seguir avanzando en estas cuestiones".

 El director de Salud del municipio, César Pauloni, recordó que actualmente, la Municipalidad y las Chicas Pink despliegan una estrategia de prevención del cáncer de mama en barrio Nuevo Horizonte. A través de una acción territorial conjunta, la semana pasada se procedió a la búsqueda de mujeres de entre 40 y 70 años que residan en esa zona de la ciudad para convocarlas a realizarse un chequeo ginecológico completo. El director de Salud informó que la tarea continuará la semana próxima, cuando los agentes municipales y las chicas de la asociación comiencen a visitar cada casa del barrio, para encontrar a las mujeres que aún no hayan sido contactadas.

 **Las Chicas Pink y su "Dragón"**

El grupo nace en la ciudad de Santa Fe el 17 de diciembre de 2016. La idea más allá de práctica el remo, es concientizar socialmente, ya que es muy importante la detección temprana para un mejor abordaje de la situación; transmitir el mensaje de que no están solas y de que hay vida después del tratamiento.

 La embarcación de origen chino fue adquirida en marzo, es un bote a remo de fibra de vidrio tipo góndola, de 18,40 metros de eslora y 1,14 de manga, con un llamativo rostro del mitológico ser en la proa y con la cola del mismo en su popa. En su "cuerpo" viajan a bordo 10 palistas simples o remeras ubicadas sobre asientos de madera en cinco filas -dos por banda-, más una timonel y una encargada del hacer sonar un tamboril, que marca el pulso de cada palada para impulsar la embarcación.

 Cámara, recordó que "esta historia comenzó un día en que a pesar del momento en que estaba viviendo, cayendo al vacío, no tenía otra opción que luchar y seguir adelante por mí y mis seres queridos. Nació así un alma guerrera que cambiaría mi vida para siempre y la de muchas mujeres también".

 Según describió, la pregunta inicial "por qué a mí" se transformó en "para qué" y ese fue el disparador de lo que hoy es Chicas Pink, una asociación constituida por mujeres sobrevivientes de cáncer de mama pero también entusiastas, que practicamos el remo como forma de rehabilitación linfática y muscular".

 "Tenemos empuje, estamos llenas de energía, estamos vivas -dijo emocionada- y luchamos fuerte, con alegría, y lo seguiremos haciendo. Hoy celebramos el despertar del Bote Dragón cuyo significado constituye la esperanza viva de mujeres y hombres que reciben el diagnóstico de cáncer de mama", finalizó.