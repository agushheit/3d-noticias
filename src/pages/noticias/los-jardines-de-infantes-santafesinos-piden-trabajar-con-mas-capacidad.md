---
category: La Ciudad
date: 2021-01-29T10:34:13Z
thumbnail: https://assets.3dnoticias.com.ar/jardines.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: Los jardines de infantes santafesinos piden trabajar con más capacidad
title: Los jardines de infantes santafesinos piden trabajar con más capacidad
entradilla: Desde la agrupación de jardines maternales se están organizando para pedir
  la ampliación de cupos para poder costear el funcionamiento

---
Más de 400 jardines de infantes de la provincia de Santa Fe se están agrupando para pedir de forma conjunta la ampliación de cupos, ahora fijada en 50% de capacidad por el protocolo sanitario. Luego de dos meses de trabajo desde la apertura de los jardines "nunca se detectó un chico con Covid-19 dentro de la institución", según el profesor Fernando Collados, vicedirector del jardín Tweety.

En comunicación con UNO explicó que a nivel provincial están preparando un informe para presentar frente a mesa de trabajo para que nos permitan ampliar los cupos debido a que también los padres lo necesitan. "Estamos con listas de espera porque no podemos tomar más chicos y los padres están desesperados tratando de ubicar a los chicos en alguna institución".

Fernando contó: "Estamos recibiendo informes que indican que las personas menores de 13 años no presentan altos índices de contagio ni situación complicada con la enfermedad", y subrayó: "Llevamos dos meses de trabajo donde, si hubo un comunicado de inasistencia de algún alumno es de parte de la familia, que por algún pariente o por prevención deciden no enviar al chico al jardín. Pero nunca se detectó un chico con Covid-19 dentro de la institución".

Por otro lado también dio cuenta de una situación complicada que se da con los pequeños de tres años, que tendrían que pasar a la escuela, pero ante la incertidumbre de la apertura de las aulas los padres les piden que los sigan recibiendo.

"Nos están pidiendo por favor que continúen hasta que la escuela empiece; lo que pasa es que tenemos inscriptos que ya cubren esa sala. No podemos tenerlos más a los que pasaban a la escuela", dijo el dueño del jardín, reflejando la situación del resto.

La Agrupación de Jardines Maternales de la Ciudad de Santa Fe, la Asociación de Jardines de Rosario y el resto de la provincia presentarán en breve este pedido a las autoridades de Salud y Educación, cuando les asignen una mesa de trabajo, junto a toda la información respectiva.

**Conectar con los pares**

"Nosotros hicimos todo el esfuerzo y le buscamos todas las formas para poder llegar a los chicos pero el contacto, la presencia, la mirada en los ojos, la lectura del grupo, la posibilidad de estimular de otra manera o contener de otra forma es imposible de hacer virtualmente", explicó el profe del jardín de infantes, Fernando Collados. "En lo real uno está ahí y va haciendo lectura permanentemente de la actividad que se plantea".

Consultado sobre el regreso a las salas este diciembre, Fernando calificó como "muy positivo" el reencontrarse con espacios que están adaptados para ellos. "A nivel general notamos la necesidad de retomar, de volver a encontrarse. Hay muchos que se volvieron a reconocer acá" y aclaró que hubo algunos casos puntuales de chicos "bastante pegados con el familiar" porque "prácticamente no pudieron tener vida normal y transitaron un año muy encerrados, sin posibilidad de contacto con muchos chicos".

"Ha costado ese desprenderse y asimilar a la docente. Pero con paciencia y reducciones de horario se puede lograr la adaptación". Si siempre fue complicado para los pequeños el comienzo del jardín, ahora con el protocolo se volvió algo más brusco, según el profesor. "El padre no puede entrar al jardín, solamente se permite unos minutos en caso de que se complique y no pueda desprenderse el nene. Ante esto lo que hicimos fue ocupar el patio, el lugar donde pueda acceder el papa, probar si puede hacer upa con la seño".

"Yo creo que se puede seguir avanzando respetando el protocolo tranquilamente. Yo apuesto a que la educación sea lo primero que se tenga que abrir y lo último que se tenga que cerrar. Sobre todo en la primera infancia, porque lo que vemos es que tranquilamente pueden seguir con sus actividades y no habría problemas de salud", opinó Fernando.

![La socialización con pares es parte del rol educativo de los jardines.](https://media.unosantafe.com.ar/p/2d793d376f400e2a639d3d4224ee4669/adjuntos/204/imagenes/029/457/0029457098/jardin-infantes-maestra-barbijojpg.jpg?0000-00-00-00-00-00)

**Los cuidados con los más chiquitos**

"El protocolo lo trabajamos bien. Las seños se cambian la ropa cuando ingresan y la dejan en una bolsa aparte, el calzado queda acá fijo, el barbijo es uno por día y se usa uno distinto para el exterior que para el interior. Todo eso lo tenemos incorporado. Algunas hacen cuatro o cinco horas y están todo el tiempo con el barbijo, pero es cuestión de acostumbrarse", dijo Fernando.

Explicó que se les está dando mayor prioridad a los patios, rociando con sanitizante al término de cada grupo y que en las salas se trabaja con ventanas y puertas abiertas para que circule el aire. También explicó que prenden ventiladores y aires acondicionados, y que a estos últimos, para no romperlos "los dejamos funcionando 20 minutos, descansamos 10 o 15 y después los volvemos a prender. Dentro de todo hemos descubierto que se sigue manteniendo una temperatura con la que se puede trabajar, si no las temperaturas altas afectarían la salud también".

Sobre las inspecciones aclaró que por protocolo no se le permitiría la entrada a otra persona extraña que puede hacer recorrido dentro del jardín y que eso "frena el trabajo de poder llevar a cabo una inspección". Informó que "las inspecciones que pudieron llevarse hasta el momento fueron más que nada en la zona del ingreso, donde se pidió la cantidad de cada grupo".

Otro inconveniente que marcó Fernando fue la utilización de juguetes que no pueden ser compartidos, porque tienen que ser desinfectados, así que tuvo que darse una reorganización interna en la que "las seños programan con qué materiales van a trabajar durante la semana porque saben que el lunes pueden usar un elemento y la otra sala recién puede usarlo el martes".

También aclaró que las mesas se comparten entre dos o tres chicos y se ponen separadas, entonces si quieren cambiar e ir a jugar a la otra mesa, o para el momento de la merienda, pueden hacerlo. Por último detalló que armaron "islas" con los pisos de goma eva para cada nene y que antes un canasto, que se ponía en un solo espacio, ahora se divide en cuatro, lo que permite la separación entre los chicos.

"Cuando están jugando, por ahí uno se levanta y quiere ir a compartir con otro, así que invitamos a otro a jugar con otros juguetes", y agregó: "Es imposible que estén distanciados todo el tiempo pero intentamos ocupar lo que más se pueda todo el espacio físico de la sala".

"Estamos tratando de evitar buscarle lo negativo de poder estar con otro, porque justamente nuestro mayor trabajo es el de socializarlos. Los buscamos hacer rotar y dentro de todo se maneja lo más posible la distancia", concluyó.

![Durante la pandemia los jardines de todo el país tuvieron que reclamar por asistencia económica ante la imposibilidad de seguir prestando sus servicios.](https://media.unosantafe.com.ar/p/2d9371d06d740088c12ffb6d13d9d954/adjuntos/204/imagenes/028/838/0028838900/jardines-maternales-jpg.jpg?0000-00-00-00-00-00)

**El costo de la educación**

Sobre la experiencia durante la pandemia, Fernando Collados explicó que cerca de agosto o septiembre, muchos jardines tuvieron problemas para seguir cobrando la cuota; incluso, de los 420 registrados "perdimos un par en el camino". En su mayoría al no poder recibir el servicio completo muchos entendieron que no correspondía seguir pagando la cuota, situación que en las escuelas no se dio, remarcó el dueño del jardín.

Según Fernando, durante el aislamiento se intentó continuar con el proyecto educativo mediante videos ya armados, o reuniones por Zoom pautadas por horario, sin mucho éxito. Sí fue más productivo el envío de actividades por medio de videos por WhatsApp o Facebook, o un combinado de entrega de materiales con video tutorial para desarrollar la actividad.

"Algunos padres han sostenido y otros no, y nosotros los entendemos a los que sí y a los que no, y no juzgamos ni encasillamos a nadie porque la pandemia es algo que nos superó".

Consultado sobre un posible aumento en el monto de la cuota, el vicedirector del jardín de infantes dijo que va a depender de si les permiten o no tener más capacidad de cupos, ya que hay que actualizar sueldos y prever el aumento de alquileres. "Yo creo que después de marzo va a haber una pequeña reestructuración. Si bien acomodamos algo los números en diciembre, porque a los montos los mantuvimos todo el año, la mayoría de los jardines ahora en enero modificó para poder sostenerse, porque sino una sala con 10 chicos y una docente no rinde lo mismo, en algunos casos salen derecho."

"Lo que menos queremos es que el nivel inicial sea elitista, tiene que llegar a todos, pero tenemos que sostener lo que tenemos", finalizó el docente.