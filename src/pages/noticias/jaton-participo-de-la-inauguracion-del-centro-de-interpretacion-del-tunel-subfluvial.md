---
category: La Ciudad
date: 2021-12-15T06:00:22-03:00
thumbnail: https://assets.3dnoticias.com.ar/TUNEL.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Jatón participó de la inauguración del Centro de Interpretación del Túnel
  Subfluvial
title: Jatón participó de la inauguración del Centro de Interpretación del Túnel Subfluvial
entradilla: ". El acto se realizó en el marco del 52º aniversario de esta emblemática
  obra de ingeniería que une las provincias de Santa Fe y Entre Ríos.\n\n"

---
El Túnel Subfluvial “Raúl Uranga – Carlos Sylvestre Begnis” celebró su 52° aniversario y, en ese marco, se inauguraron las obras de puesta en valor y ampliación del Complejo Social y del nuevo Centro de Interpretación. El acto estuvo encabezado por los gobernadores de Santa Fe, Omar Perotti; y de Entre Ríos, Gustavo Bordet; y también participó el intendente de Santa Fe, Emilio Jatón, y otros funcionarios de ambas provincias.

El director de Turismo de la Municipalidad de Santa Fe, Franco Arone consideró que se “trata de un hecho trascendental porque el intendente Emilio Jatón tomó la decisión de que el turismo social sea una política de Estado en la ciudad de Santa Fe; y este espacio puesto en valor también es importante pensando en esta sinergia con la provincia de Entre Ríos”.

Arone agregó: “Este lugar tiene la posibilidad de alojamiento, un espacio previsto también para hacer deportes y actividades vinculadas a la recreación y apunta a la tercera edad, y a segmentos de los niveles primarios y secundarios, con lo cual es una alternativa que tiene una obra de ingeniería única como es el Túnel que se inauguró hace 52 años y hoy habilitó también su centro de interpretación, entonces esta idea del intendente de potenciar el turismo, sobre todo el social nos obliga a tomarlo en la agenda de trabajo como un atractivo a fortalecer e incorporar dentro de la oferta turística de la ciudad”.

Por último, hizo referencia a la sinergia que se deberán tener entre ambas ciudades. “Es un espacio que compartimos y tenemos que potenciar juntas porque una de ellas decidió tomar al turismo como política de estado y sobre todo garantizar el derecho al turismo social. Por eso el compromiso está asumido y seguiremos trabajando en ese sentido”, finalizó el director de Turismo de la ciudad de Santa Fe.

**De qué se trata**

El Complejo Social del Túnel Subfluvial, ubicado sobre el extremo Paraná del ente interprovincial y frente al río contará, en esta primera etapa, con una capacidad de alojamiento de 60 plazas, edificios complementarios para la administración, servicios médicos y generales; un salón comedor y áreas recreativas.

El Centro de Interpretación se encuentra en los límites del predio. Se trata de un gran esqueleto de hormigón que continúa la obra de Mario Roberto Álvarez –prestigioso arquitecto argentino que diseñó los edificios gemelos que se encuentran en ambas cabeceras del viaducto-, donde se podrán visualizar las imágenes que fueron tomadas hace más de 50 años, durante la construcción del enlace, además de las maquetas representativas que dan cuenta de la importancia que reviste el Túnel Subfluvial en tanto obra de ingeniería.

Con la finalización de estas obras, el Túnel Subfluvial “Raúl Uranga – Carlos Sylvestre Begnis” vuelve a albergar a delegaciones, contingentes y grupos en un lugar totalmente renovado y ambientado, tanto en lo tecnológico como así también en su funcionamiento.

**Fortalecer lazos**

Por su parte, el gobernador de Santa Fe, Omar Perotti destacó la apertura de estas obras y resaltó la “decisión de alto contenido federal que tuvieron dos gobernadores como Silvestre Begnis y Uranga”. Luego agregó: “A las dos provincias nos llena de orgullo y nos tiene que fortalecer esa mirada federal, en esa integración que a partir de allí las dos provincias y las dos ciudades vecinas fueron creciendo en todos estos años”.

Por su parte, el gobernador de Entre Ríos, Gustavo Bordet destacó: “Hoy es un día muy importante: hace 52 años se inauguraba el Túnel subfluvial, una obra que desde el lado de Entre Ríos rompió el carácter insular de la provincia; fue la primera obra que nos conectó de manera rápida y directa con la vecina provincia de Santa Fe”.

Antes de finalizar, el mandatario entrerriano destacó también que “este complejo va a servir para fomentar el turismo social” y agregó: “Sintetiza esa unión que nos caracteriza, a partir de la construcción del Túnel Subfluvial, a santafesinos y entrerrianos. Este es el desafío que tenemos por delante, y que nos convoca a seguir generando acciones para el desarrollo de nuestras provincias”.