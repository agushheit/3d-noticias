---
layout: Noticia con imagen
author: .
resumen: Adiós a Pino Solanas
category: Agenda Ciudadana
title: Falleció Pino Solanas en París, tras contagiarse de coronavirus
entradilla: A mediados de octubre, el embajador argentino ante la Unesco informó
  que había dado positivo en coronavirus y que estaba internado en un hospital
  de París.
date: 2020-11-07T15:20:51.825Z
thumbnail: https://assets.3dnoticias.com.ar/pino.jpg
---
El embajador argentino ante la Unesco, Fernando "Pino" Solanas, falleció esta madrugada en París, Francia, donde recibía un tratamiento por coronavirus, informó esta madrugada la Cancillería.

"Enorme dolor por Pino Solanas. Murió en cumplimiento de sus funciones como embajador de Argentina ante la Unesco", indicó el Palacio San Martín en su cuenta oficial de Twitter.

"Será recordado por su arte, por su compromiso político y por su ética puesta siempre al servicio de un país mejor. Un abrazo a su familia y sus amigos", agrega la publicación.

A mediados de octubre, el exsenador nacional informó -a través de su cuenta de Twitter- que había dado positivo en coronavirus y que estaba internado en un hospital de París, donde está ubicada la sede de este organismo de la ONU para la educación, la ciencia y la cultura.

El exlegislador posteó junto con ese último mensaje una foto en la que se mostraba en reposo en una cama de terapia intensiva, mientras permanecía aislado y recibía tratamiento.

Cineasta, ex diputado nacional, ex senador y principal referente de la agrupación Proyecto Sur que integra el Frente de Todos, Solanas tenía 84 años y se encuentra en el grupo de riesgo etario más expuesto ante el coronavirus.

**Una vida de militancia a través de su acción política y su obra artística**

Fernando "Pino" Solanas deja un legado caracterizado por su vida de militancia, que se desplegó a través de su apasionada acción política y su obra artística con contenido social.

Nacido el 16 de febrero de 1936 en Buenos Aires, sus ideas políticas marcaron su actuación pública, tanto como cineasta, donde destacó con una decena de exitosas películas con fuerte contenido social, hasta su participación en política.

“Pino” fue un actor clave en la creación de varios espacios de centro-izquierda que lo llevaron a ocupar varios cargos, como diputado, senador nacional o embajador ante la Unesco, su último rol como servidor público.

![](https://assets.3dnoticias.com.ar/pino-solanas.jpg "Pino Solanas")

Formado intelectual y políticamente bajo el influjo de nombres como Raúl Scalabrini Ortiz, Arturo Jauretche y Juan José Hernández Arregui, Solanas comenzó a plasmar sus ideas políticas a través de su obra cinematográfica a comienzos de la década del 60´.

Así surge en 1962 su primer cortometraje de ficción, Seguir Andando. En 1968 llegaría el primer largometraje, La Hora de los Hornos, una trilogía donde aborda algunos de los temas que lo apasionaron a lo largo de su carrera: el neocolonialismo y la violencia en el país y en América Latina.

Un año después, en 1969, funda el grupo Cine Liberación, una corriente de realizadores que sirve como base de resistencia a la dictadura, promoviendo un circuito alternativo de difusión de sus producciones.

Es este mismo grupo de Cine Liberación el que es convocado por Juan Domingo Perón, en el exilio en Madrid, para realizar sus dos testimonios fílmicos: La Revolución Justicialista y Actualización Doctrinaria para la toma del poder.

En 1975 presenta Los Hijos de Fierro, y ante las amenazas de muerte y un intento de secuestro parte al exilio, radicándose en Francia, donde realiza en 1980 el documental La Mirada de los Otros.

Desde el exterior, estuvo activamente involucrado en la defensa de los derechos humanos y denunció a la dictadura militar a nivel internacional. Con el triunfo de Raúl Alfonsín en 1983 regresa al país y filma Tango, El exilio de Gardel, y luego en 1988 Sur, ambas premiadas en los festivales de cine más prestigiosos del mundo.

Los comienzos de los 90´ marcan su irrupción de lleno en el mundo de la política, y su voz se alza para criticar al gobierno de Carlos Saúl Menem (1989-1999).

Entonces, promovió la modificación de la ley de Radiodifusión que regía durante la dictadura militar y se mostraba particularmente crítico con las privatizaciones y la ley de reforma del Estado promovidas por la administración menemista.

En 1992, de la mano del dirigente Luis Brunati gestan el Frente del Sur, presentándose ese mismo año como candidato a senador nacional por la Ciudad de Buenos Aires.

Luego, en 1993, se funda el Frente Grande, con la inclusión del sector que lideraba Carlos “Chacho” Alvarez, y “Pino” Solanas es electo diputado nacional por la provincia de Buenos Aires.

Solanas juega un rol importante en la elección a convencionales constituyentes para la reforma constitucional de 1994, obteniendo casi 18% de los votos en la provincia de Buenos Aires.

Su actuación política estuvo dominada por algunos temas que lo obsesionaban, como la defensa del medio ambiente y la deuda externa argentina

Desencantado con lo que consideraba un giro “a la derecha” del Frente Grande, a partir del “Pacto del Molino” entre “Chacho” Álvarez, Graciela Fernández Meijide, y José “Pilo” Bordón, Solanas termina su mandato en 1997 y decide replegarse en su rol como cineasta.

Su actuación política estuvo dominada por algunos temas que lo obsesionaban, como la defensa del medio ambiente y la deuda externa argentina, cuyo origen cuestionaba en duros términos.

En 1998 termina su película La Nube, y es premiado en los festivales de Venecia y La Habana, en este último caso por su trayectoria.

Luego vendrán sus películas Memoria del Saqueo y La Dignidad de los Nadies, mientras en 2007 estrena Argentina Latente, su documental sobre el potencial científico de Argentina.

Su carrera política se retoma desde entonces con un gran ritmo. En 2007 encara la candidatura a Presidente de la Nación por Proyecto Sur, proponiendo la nacionalización de los recursos estratégicos.

Es electo diputado nacional por el período 2009-2013 y senador nacional entre 2013 y 2019.

![](https://assets.3dnoticias.com.ar/equipo-solanas.jpg "Pino Solanas")

A mediados del año pasado, resuelve respaldar la fórmula presidencial Alberto Fernández-Cristina Fernández de Kirchner y es electo diputado nacional por la Ciudad de Buenos Aires para el período 2019-2023.

El presidente Alberto Fernández le solicita que renuncie a su banca para desempeñarse como embajador argentino ante la Unesco, la oficina de Naciones Unidas para la Educación, la Ciencia y la Cultura, cargo que desempeñó en París hasta su fallecimiento.

Al resumir sus ideas políticas, Solanas explicaba que fundó Proyecto Sur para “profundizar una propuesta política, económica, social y cultural para el país. Ubicamos el respeto a la condición humana sobre cualquier otra consideración, proclamando como principios básicos la defensa del ambiente y la propiedad pública de nuestros recursos naturales, como condición para alcanzar una auténtica justicia social y garantizar la soberanía nacional”.

“Se trata de cambiar esta Argentina saqueada y esta pseudo-democracia para reemplazarla por una Argentina justa y latinoamericanista con pleno protagonismo popular”, afirmaba “Pino” por ese entonces.

A principios de octubre visitó al Papa Francisco en el Vaticano, y días después, el 21, comunicaba por redes sociales que estaba internado por coronavirus en terapia intensiva y que seguía “resistiendo”.