---
category: Agenda Ciudadana
date: 2020-12-16T12:41:19Z
thumbnail: https://assets.3dnoticias.com.ar/diputado-neuquen.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Diario UNO'
resumen: Un diputado propone la pena de muerte para asesinos y violadores de niños
title: Un diputado propone la pena de muerte para asesinos y violadores de niños
entradilla: Se trata del legislador de Neuquén Francisco Sánchez, quien aseguró que
  pedirá que se evalúe la pena capital para esos casos.

---
**El diputado por Neuquén, Francisco Sánchez, reaccionó ante la noticia de la muerte de un pequeño que fue violado por su padrastro y aseguró que presentará un proyecto para que “asesinos y violadores de menores de edad” reciban la pena de muerte.**

“Voy a presentar un proyecto para aplicar pena de muerte a monstruos como estos. Seguro que ahora que ya no consideramos más el Pacto de San José de Costa Rica en Argentina, voy a conseguir la adhesión de quienes lo rechazaron cuando votaron el aborto”.

Cabe recordar que la pena de muerte está derogada desde el año 1984, cuando el entonces presidente Raúl Alfonsín suscribió a la Argentina al Pacto de San José de Costa Rica. De este modo, el único modo que existe de instaurarla nuevamente sería renunciando a dicho pacto. La última vez que se aplicó la pena de muerte en el país por un delito común fue en julio de 1916.

Teniendo en cuenta que el diputado neuquino representa a quienes se oponen a la sanción de la ley que busca la interrupción voluntaria del embarazo, el legislador hizo mención a dicho pacto por considerar que el aborto sería algo similar a una pena de muerte.