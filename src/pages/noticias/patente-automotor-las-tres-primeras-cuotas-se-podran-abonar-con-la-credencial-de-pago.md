---
category: Agenda Ciudadana
date: 2021-03-06T07:30:01-03:00
thumbnail: https://assets.3dnoticias.com.ar/API.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Patente automotor: las tres primeras cuotas se podrán abonar con la credencial
  de pago'
title: 'Patente automotor: las tres primeras cuotas se podrán abonar con la credencial
  de pago'
entradilla: La Administración Provincial de Impuestos (API) informó que los contribuyentes
  de la patente única sobre vehículos abonarán ese tributo con un incremento que no
  superará el 40 por ciento del impuesto determinado en 2020.

---
El titular de la API, Martín Avalos, indicó que "los contribuyentes que desean abonar en forma presencial van a emplear para el pago la misma boleta que ya le fue distribuida, utilizando la credencial de pago". En ese sentido remarcó que la "credencial de pago se encuentra en el vértice inferior derecho y que también cuenta un código de barra". Cabe destacar que los códigos de barra de las cuotas 1, 2 y 3 fueron anulados.

La API recuerda que continúan vigentes todos los medios electrónicos de pago, y las modificaciones de los valores se efectúan de manera automática por sistema.

**Los que ya pagaron**

El titular de la API sostuvo que "todos aquellos que hayan abonado alguna de las cuotas antes de la entrada en vigencia de la nueva normativa, en la medida que la misma sea superior al 40 por ciento, el pago en exceso será computado como pago a cuenta de las cuotas 3, 4, 5 y 6, en ese orden de prelación si subsiste el saldo a favor".

En cuanto a los que hayan realizado el pago total anual, "el reintegro se hará de manera automática a través de una transferencia en la CBU del contribuyente", informó el funcionario provincial.

Los contribuyentes que tengan un pago en exceso al 40% deberán ingresar a la página www.santafe.gob.ar/api y allí se encontrará un aplicativo web para cargar los datos y denunciar la CBU para la transferencia, o se puede hacer una presentación por escrito ante cualquiera de las dependencias de API.

**Nuevos Vencimientos**

Se reprogramaron los vencimientos de las cuotas 1, 2 y 3, los que operarán en las siguientes fechas:

Cuota 1: entre el 5 y 9 de abril de 2021.

Cuota 2: entre el 16 y 23 de junio de 2021.

Cuota 3: entre el 16 y 20 de agosto de 2021.