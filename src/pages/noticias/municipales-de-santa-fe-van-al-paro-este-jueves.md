---
category: La Ciudad
date: 2021-06-15T08:13:17-03:00
thumbnail: https://assets.3dnoticias.com.ar/muni.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Festram con información de Diario El Litoral
resumen: Municipales de Santa Fe van al paro este jueves
title: Municipales de Santa Fe van al paro este jueves
entradilla: Festram dispuso un paro de 24 horas para este jueves 17 de junio en reclamo
  de vacunación para este sector laboral que fuera declarado esencial desde el primer
  minuto de la Emergencia por Covid-19.

---
De acuerdo a lo resuelto por el Plenario de Secretarios Generales de los Sindicatos adheridos a Festram, se dispuso un paro de 24 horas para este jueves 17 de junio en reclamo de vacunación para este sector laboral que fuera declarado esencial desde el primer minuto de la Emergencia por Covid-19.

Según se informó, cada sindicato asegurará la medida de fuerza garantizando las guardias mínimas y los sectores afectados directamente a la atención de la Pandemia.

Tal como se reclamó al Ministerio de Trabajo, al de Salud y al propio Gobernador, los casos de Covid se incrementaron en un 350%, 3 de cada 10 casos fueron contraídos en el ámbito laboral. 

"Consideramos justo que se hayan vacunados a otros sectores, pero se excluyeron –entre otros- a docentes municipales. Se vacunó a todo el personal de seguridad, pero en los operativos participa mayoritariamente el personal municipal y comunal. Se vacunó a empleados privados de servicios fúnebres, pero la manipulación de féretros y cadáveres lo hacen quienes trabajan en los cementerios. Hoy las áreas de Control, Recolección de Residuos, Desarrollo Social, trabajadores afectados a Defensa Civil, Tránsito, Policía Municipal, Inspectores y Guardias Urbanas, áreas de servicios como alumbrado, obras sanitarias, aguas y tantas otras actividades están igualmente expuestos que otros sectores ya vacunados", expusieron en un comunicado de prensa.  

En ese sentido, agregan: "Funcionarios de la Provincia convocan específicamente a personal municipal y comunal, a través de sus autoridades, a colaborar en los operativos de control y asistencia sanitaria pero ignoran la necesidad de su vacunación y el índice de contagios y fallecimientos de nuestros compañeros y compañeras. Incluso hemos tomado conocimiento de gestiones realizadas por Intendentes y Presidentes Comunales, para exigir la vacunación, pero tampoco han sido atendidas, es decir, no hay vacunas para un servicio declarado Esencial en Pandemia como lo es el Municipal".  

Al mismo tiempo, se reclama "por la integridad física de los trabajadores municipales y comunales que son enviados a la realización de operativos para el cumplimiento de Protocolos, y son víctimas de graves agresiones por parte de los infractores. Las respuestas violentas y hasta el uso de la fuerza, son una constante en cada operativo". 