---
category: Agenda Ciudadana
date: 2021-03-10T06:51:58-03:00
thumbnail: https://assets.3dnoticias.com.ar/festram.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Festram
resumen: 'Paritaria municipal: sin acuerdo habrá paro la semana próxima'
title: 'Paritaria municipal: sin acuerdo habrá paro la semana próxima'
entradilla: Será el miércoles 17 y jueves 18 sin asistencia a los lugares de trabajo.

---
Ante la falta de acuerdo en la paritaria municipal, Festram dispuso un paro de 48 horas para la semana próxima. 

Será los días miércoles 17 y jueves 18, sin asistencia en los lugares de trabajo, facultando a los Sindicatos a adecuar la modalidad para garantizar la medida.

Según el comunicado enviado desde Festram "después de varias jornadas de negociaciones y a pesar de coincidir en los parámetros referenciales de ofertas salariales nacionales y provinciales que superen los índices inflacionarios, los representantes de los Intendentes no lograron definir una propuesta de aumento concreta". 

Por tanto, aguardan que los Intendentes hagan una oferta salarial "acorde a las expectativas y necesidades de los trabajadores municipales que permitan llegar a un acuerdo antes de la medida de fuerza".