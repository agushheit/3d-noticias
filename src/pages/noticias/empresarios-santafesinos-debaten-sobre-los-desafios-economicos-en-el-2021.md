---
category: Agenda Ciudadana
date: 2021-05-18T09:32:37-03:00
thumbnail: https://assets.3dnoticias.com.ar/distefano.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Carlos Suarez
resumen: Empresarios santafesinos debaten sobre los desafíos económicos en el 2021
title: Empresarios santafesinos debaten sobre los desafíos económicos en el 2021
entradilla: Será en el marco de un desayuno donde estarán presentes referentes y representantes
  de industriales, empresarios y comerciantes de la ciudad.

---
El encuentro es organizado por el concejal de la UCR-Juntos por el Cambio Carlos Suárez y contará con la disertación del economista Salvador Di Stefano.

Este martes a las 9 en los Salones Ríos de Gula (Dique 2 del Puerto)  se llevará a cabo un desayuno para debatir acerca de los desafíos económicos en el 2021. El encuentro, que se realizará respetando todo los protocolos establecidos, es organizado por el concejal de UCR-Juntos por el Cambio, Carlos Suárez, y contará con la disertación del economista Salvador Di Stefano. 

Entre quienes participarán de este desayuno se encuentran el presidente de la Sociedad Rural, Ignacio Mántaras; el presidente y el gerente del Centro Comercial, Martín Salemi y Fabián Zanutigh; el director ejecutivo del Mercado de Productores, Carlos Otrino; el presidente de la Cámara de la Construcción, Sergio Winkelmann, la presidenta de la Cámara de Mujeres Emprendedoras, Marita Moyano; el presidente de la Cámara de Empresas y Corredores Inmobiliarios, Walter Govone; además de representantes de la Bolsa de Comercio.

**Salvador Di Stefano**

Es un reconocido analista económico y desempeña actividades de investigador, periodista, conferencista y consultor desde el año 1991 a la fecha. Asesora a 50 empresas en el país: bancos, mutuales, aseguradoras, frigoríficos, empresas agropecuarias (agricultura, ganadería y cerdos), corredores, acopios, venta y producción de insumos, agentes de bolsa, constructoras, metalúrgicas, comercios mayoristas y minoristas entre otras. Actualmente es Director de Agroeducación, una empresa destinada a capacitación en Agro, en Argentina y América Latina. 

En los medios gráficos, es columnista del suplemento económico del Diario La Capital de Rosario y de la Revista Fortuna. También en los medios de comunicación trabajó en la conducción del Programa A Fondo por LT8 de Rosario y Canal 5 de Televisión. Además, fue director editorial de la página WEB ON24.