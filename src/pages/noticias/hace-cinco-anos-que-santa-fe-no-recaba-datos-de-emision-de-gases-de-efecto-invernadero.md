---
category: La Ciudad
date: 2021-11-08T06:15:10-03:00
thumbnail: https://assets.3dnoticias.com.ar/foto.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Hace cinco años que Santa Fe no recaba datos de emisión de gases de efecto
  invernadero
title: Hace cinco años que Santa Fe no recaba datos de emisión de gases de efecto
  invernadero
entradilla: El especialista Rubén Piacentini indicó que faltan estructuras que permitan
  recabar datos precisos. Según el último informe, la ciudad emite 4,7 millones de
  toneladas de gases contaminantes por año.

---
Luego del inventario de emisión de gases de efecto invernadero en Santa Fe realizado en 2016 por el Laboratorio de Eficiencia Energética, Sustentabilidad y Cambio Climático de la Facultad de Ciencias Exactas de la Universidad Nacional de Rosario (UNR), los profesionales a cargo del proyecto no han vuelto a ser convocados para actualizar los datos. Las últimas mediciones indicaban que un santafesino emite 7 toneladas de gases de efecto invernadero por año.

El gobierno provincial dio a conocer los resultados del estudio durante 2019, donde se arrojó que en la bota santafesina se emiten 23,4 millones de toneladas de gases de efecto invernadero por año, de los cuales 4,7 corresponden a la ciudad de Santa Fe. En cuanto a la medición per cápita, se pudo plasmar que cada santafesino emite 7 toneladas de gases contaminantes a la atmósfera cada año, 1,6 toneladas por debajo de la media nacional.

UNO Santa Fe dialogó con el Dr. Rubén Piacentini, quien es científico investigador del Conicet, director del área de Física de la Atmósfera, Radiación Solar y Astropartículas del Instituto de Física de Rosario (Ifir) y director del Laboratorio a cargo de la investigación previamente mencionada, entre otros títulos.

Luego del inventario provincial de emisión de gases de efecto invernadero con datos recabados durante 2016, Piacentini (quien estuvo a cargo del Contaminación en Santa Fe y el paísproyecto) manifestó que "a partir de allí no fuimos consultados para continuar el inventario. En su momento costó muchísimo trabajo y esfuerzo conseguir datos para ese año porque no está estructurada la legislación de la provincia como para que las distintas áreas recaben datos que son de interés para el cálculo de las emisiones".

El Doctor en Física rosarino Rubén Piacentini, director del Laboratorio de Eficiencia Energética, Sustentabilidad y Cambio Climático de la Facultad de Ciencias Exactas de la UNR.

**Contaminación en Santa Fe y el país**

Respecto de la incidencia de Santa Fe y Rosario en el total de emisiones por año, el especialista indicó que "por ser ciudades grandes y no tener contribución agrícola las emisiones son bastante menores por habitante, aunque colaboran mucho proporcionalmente porque tienen la mayor concentración de habitantes, Santa Fe tiene una proporción significativa de esto". En este marco, Rosario representa el 50% de las emisiones, seguida por la ciudad capital de la provincia con un 20%.

Hay que señalar que si bien es cierto que Argentina contribuye de forma general en la emisión del 1% de gases en el mundo, el Doctor en Física rosarino postula que "cuando dividimos las emisiones totales por habitante, éstas terminan siendo importantes. El punto es que superamos las emisiones de muchos países de Europa. Teníamos emisiones por habitante en el orden de las 8 o 9 toneladas por año (unas 100 veces más el peso de una persona), en tanto que Francia o España teniendo más habitantes tenían menos emisiones por habitante".

**Bajante del Paraná y calentamiento global**

Ante los riesgos de seguir con estos índices de emisión de gases de efecto invernadero, el investigador resaltó que esto implica "no solo que se continúe dando el impacto actual del calentamiento global sino que se amplifique".

Y a esto lo ejemplificó con la bajante histórica en toda la cuenca del Paraná: "Es cierto que ocurrió una sequía similar con una bajante extrema en 1944, lo que ocurre es que estos eventos estaban previstos para suceder cada 100 años. En nuestra región tenemos eventos de este tipo repitiéndose cada década y en algunos casos cada año. Eso quiere decir que se está incentivando el fenómeno".

![](https://media.unosantafe.com.ar/p/0135b4f2ea4e3b6785b41db232960817/adjuntos/204/imagenes/030/327/0030327557/bajante-08jpg.jpg?0000-00-00-00-00-00 =642x417)

"Los informes de los científicos dicen que las causas de la sequía en la cuenca del Paraná, siendo el mayor proveedor de agua del sur de Brasil, Paraguay y el norte de Argentina, son acciones como la deforestación y principalmente el cambio climático", resaltó sobre esto Piacentini.

**Cambiar a energías renovables**

"Los ciudadanos tenemos que hacer esfuerzos", sostuvo el investigador del Conicet, postulando que "hay posibilidades de reducir las emisiones esencialmente en el consumo de energía de la llamada energía estacionaria, que es la que consumen motores, aires acondicionados, electrodomésticos, vehículos, etc. Hay automóviles que circulan con un tercio del consumo energético de un automóvil convencional, con motores híbridos".

"Yo considero que tenemos que aumentar muchísimo la eficiencia en el uso de energía, somos muy ineficientes en el uso de energía. Tenemos que cambiar la matriz energética, pasar de fuentes de energía contaminantes (petróleo, gas, carbón) a fuentes de energía renovables (solar, eólica, biomasa, biocombustibles, geotermia, del mar, del agua, etc.)", puntualizó Piacentini.

Con respecto a la aislación de las viviendas y los edificios, el director del Ifir indicó que "por más que haya una legislación para la eficiencia energética en los nuevos edificios hay que actuar sobre los edificios existentes, no solo mejorando la calidad de vida sino también la salud. Un edificio aislado tiene la grandísima ventaja de no experimentar ni mucho frío en invierno ni mucho calor en verano, lo que contirbuye a la salud humana".