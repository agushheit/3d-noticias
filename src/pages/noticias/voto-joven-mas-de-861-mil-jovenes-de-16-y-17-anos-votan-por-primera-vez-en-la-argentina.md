---
category: Agenda Ciudadana
date: 2021-09-11T06:00:17-03:00
thumbnail: https://assets.3dnoticias.com.ar/VOTOJOVEN.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: 'Voto joven: más de 861 mil jóvenes de 16 y 17 años votan por primera vez
  en la Argentina'
title: 'Voto joven: más de 861 mil jóvenes de 16 y 17 años votan por primera vez en
  la Argentina'
entradilla: La mayoría se localizan en la provincia de Buenos Aires (333.342), Córdoba
  (72.459), Santa Fe (63.022), y en Ciudad Autónoma de Buenos Aires (51.268). La provincia
  con menos electores jóvenes es Tierra del Fuego (4.495).

---
Un total de 861.149 jóvenes de 16 y 17 años, nacidos en 2004 y 2005, se incorporaron al padrón electoral nacional y votarán por primera vez este domingo en las PASO, informó este viernes la Cámara Nacional Electoral (CNE).

La mayoría de ellos se localizan en la provincia de Buenos Aires, donde votarán 333.342 jóvenes. En segundo lugar, se ubica la provincia de Córdoba con 72.459 nuevos electores, en tercer lugar, Santa Fe con 63.022, y en cuarto lugar Ciudad Autónoma de Buenos Aires con 51.268. En tanto, la provincia con menos electores jóvenes es Tierra del Fuego, con 4.495.

Lucía Moreno (17) votará por primera vez este domingo. Desde el barrio porteño de Nuñez, la joven -que además participa activamente del centro de estudiantes de su colegio, el Liceo 9- dijo a Télam que votar "es una responsabilidad" y que lo hace por "por convicción propia".

"Pibes de mi edad desaparecieron por luchar por ese derecho y es necesario como ciudadanos elegir a nuestros representantes, que son los que conducen nuestra vida", agregó.

Como estudiante, le interesa votar candidatos que "prioricen la educación y que les den espacios a los jóvenes que realmente estamos interesados en cambiar algo".

**Ley de Voto Joven**

El derecho a participar en las elecciones y elegir a sus representantes de jóvenes de 16 y 17 años está establecido en la Ley de Ciudadanía Argentina N° 26.774, promulgada en 2012 y conocida como "Ley de Voto Joven".

Según un informe sobre "Voto joven" publicado por del Ministerio del Interior de la Nación, desde la primera implementación de la ley, las juventudes "han aumentado progresivamente su participación en los comicios electorales, llegando a ser, en el año 2019, 869.667 electores y electoras de 16 y 17 años", lo que representó el 2,61% promedio del padrón electoral.

En este sentido, el informe agregó que, en esta franja etaria, la participación efectiva de 2019 fue de 63%, es decir, alrededor de 20 puntos porcentuales menor que la participación promedio a nivel nacional, que en Argentina es, desde 1983, del 80%.

También, voceros del Ministerio del Interior aseguraron a Télam que, "a pesar de la pandemia, logramos mejorar la situación de jóvenes que tramitan su DNI a los 16 años, respecto de las PASO 2019".