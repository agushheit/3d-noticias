---
category: Agenda Ciudadana
date: 2021-08-24T06:15:57-03:00
thumbnail: https://assets.3dnoticias.com.ar/CARCELES.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Mario Caffaro. El Litoral
resumen: Pretenden prohibir por ley el uso de telefonía móvil en las cárceles
title: Pretenden prohibir por ley el uso de telefonía móvil en las cárceles
entradilla: También se propone la instalación de inhibidores de señal, así como habilitar
  la comunicación de telefonía fija y videollamada a través de internet por cable.

---
El diputado Nicolás Mayoraz (Somos Vida y Familia) ingresó un proyecto de ley para prohibir el uso de telefonía móvil e internet inalámbrica en todos los establecimientos penitenciarios de la provincia y a la vez crear el programa 'Cárceles Seguras, Santa Fe Segura' consistente en la instalación de inhibidores de señal de telefonía móvil e internet inalámbrica en el perímetro de esos establecimientos.

La iniciativa que también firman sus compañeros de bloque Natalia Armas Belavi y Juan Argañaraz, había sido anunciada por el propio Mayoraz durante la última sesión que realizó la Cámara y donde se aprobaron dos pedidos de informes sobre la situación en el servicio penitenciario. Uno de los proyectos lo ingresó el propio legislador rosarino para conocer las requisas efectuadas en los últimos tres años, discriminando por cada una de ellas la cantidad de celulares incautados y el destino de los mismos. Entonces señaló ante el pleno de la Cámara en destacar que los fiscales del Ministerio Público de la Acusación afirman que la mayoría de los delitos organizados hoy se organizan desde las cárceles con órdenes por teléfono o por mensajes de whatsapp. "Todos los días hay hechos de violencias originados en las cárceles y debemos actuar ante esto", dijo entonces Mayoraz quien acotó que "hoy no se respeta ningún derecho, ni el de las personas privadas de libertad, pero menos de vecinos que están viviendo la situación de inseguridad constante, permanente donde nadie puede decir estar a salvo de ser alcanzado por una bala".

El proyecto de ley determina que el único y exclusivo medio de comunicación telefónica permitido para uso del personal del servicio penitenciario es a través del servicio de telefonía fija e internet por cable. También dispone que cada pabellón debe contar con un espacio para la comunicación de los internos por vía telefónica y videollamada a través de internet por cable. Propone que las unidades penitenciarias cuenten con un registro de llamadas en el que se asiente nombre de interno, horario de inicio y finalización de los teléfonos públicos. Aclara que el registro no podrá limitar el derecho de comunicación de los internos y tendrá carácter confidencial.

En el último artículo se determina que falta de cumplimiento de la eventual norma en el Servicio Penitenciario será considerada falta grave e incumplimiento de los deberes de funcionario público.

Mayoraz advierte que "la sociedad se volvió rehén de un sistema de seguridad que resulta ineficiente en extremo, con una clara incapacidad a la hora de implementar políticas de prevención de delitos. Ello resulta agravado por una concepción abolicionista disfrazada de garantismo, que apela a la falacia de las garantías que deben respetarse a toda persona que delinque, garantías en cuyo respeto coincidimos. Pero falaz desde el momento en que termina transformando el cumplimiento de las penas en un verdadero espejismo". El legislador señala a El Litoral que "la continuidad de las empresas delictivas dentro de los establecimientos penitenciarios constituye una problemática que tiene a la sociedad subyugada". Refiere al ingreso ilegal de equipos de telefonía celular a las cárceles que en muchos casos los internos lo utilizan para prácticas delictivas como secuestros y estafas telefónicas. "Los celulares también resultaron ser una herramienta útil para las asociaciones ilícitas y de esa manera se facilitó la tarea de narcos y sicarios para poder organizar robos, asesinatos y hasta organización de motines", afirmó.

En materia de legislación comparada, Mayoraz informa que El Salvador, Ecuador y Brasil dictaron leyes para instalar inhibidores de señal. Señala que ninguna norma internacional prohíbe ese tipo de medidas. En la provincia, la ley 11.661 adhirió a la ley 27.375 que reformó la ley de ejecución de la pena privativa de la libertad y en uno de los artículos determina que "quedan prohibidas las comunicaciones telefónicas a través de equipos o terminales móviles. A tal fin se deberá proceder a instalar inhibidores en los pabellones o módulos de cada penal" (art 160).

Mayoraz afirma ante El Litoral que "la sociedad necesita medidas para vivir más tranquila, y como legisladores debemos tener la determinación suficiente para brindar las herramientas legislativas que sirvan para implementar políticas públicas que velen por el bienestar de los santafesinos".

**Aparato**

Un teléfono fijo con conexión fue secuestrado ayer de la celda de Ariel Máximo "Guille" en su celda de la cárcel federal de Marcos Paz, pcia de Buenos Aires. Ese fue el saldo de una requisa que se realizó en el penal en el marco de la investigación que se lleva adelante por la balacera al Centro Penal de Justicia de Rosario de la semana pasada, en la previa al inicio del juicio que tenía, precisamente, a Guille como principal acusado.

**Las fugas en los penales**

Tras las fugas ocurridas en los penales de Piñeiro y de Coronda, el diputado Ariel Bermúdez (Creo) ingresó un extenso pedido de informes sobre esos hechos, pero también sobre cambios en los protocolos para evitar la reiteración de casos.

El pedido apunta a la fuga de dos internos de Piñero el 17 de mayo; la otra del 27 de junio y la del penitenciario de Coronda del 17 de agosto. Pide se informe si se ha podido detectar algún patrón reiterativo de falencias en los sistemas y medidas de seguridad predispuestas y si se han adoptado acciones correctivas. "Resulta evidente que las medidas de seguridad con las que cuentan los penales han sido desbordadas en distintas oportunidades lo cual exige la implementación de acciones que se encuentren orientadas a reforzar en forma urgente la seguridad" explicó el legislador a El Litoral.

**Amenazas**

En el marco de la segunda audiencia del juicio contra los integrantes de la banda criminal rosarina Los Monos, por los ataques contra blancos judiciales se ventilaron una serie de audios con mensajes intimidatorios, en uno de los cuales exigían la liberación de los miembros del grupo encarcelados bajo amenaza de "matar a todos" los jueces.

"Somos acá la banda de los Cantero. A Los Monos suéltenlos porque lo vamos a agarrar al juez, le vamos a reventar toda la casa y lo matamos", expresa el macabro audio enviado a través de un llamado al centro de emergencias.

"Suelten a todos Los Monos porque los matamos a todos. Estoy diciendo en serio. No jodan porque los matamos a todos ustedes", dice la misma voz al profundizar el mensaje.