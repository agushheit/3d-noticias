---
category: Agenda Ciudadana
date: 2020-12-04T10:29:13Z
thumbnail: https://assets.3dnoticias.com.ar/AUTISMO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: El gobierno provincial trabaja por una educación para la autonomía de todas
  las personas con discapacidad
title: El gobierno provincial trabaja por una educación para la autonomía de las personas
  con discapacidad
entradilla: El subsecretario de Inclusión para Personas con Discapacidad, Patricio
  Huerga, rechazó el proyecto de ley que busca identificar a toda persona con TEA
  (Trastornos del Espectro Autista).

---
El gobierno de la provincia de Santa Fe rechazó el proyecto de ley, con media sanción en el Senado provincial, que propone el uso de una pulsera azul con la leyenda “Soy Autista llamar al 911”, para las personas con TEA (Trastornos del Especto Autista).

El subsecretario de Inclusión para Personas con Discapacidad, Patricio Huerga, subrayó que “desde el gobierno provincial bregamos por una **educación para la autonomía**, intentamos construir herramientas que acompañen a las personas con discapacidad, pero hay que evitar cualquier forma de estigmatización”.

“Este jueves 3 de diciembre, es el Día Internacional por los Derechos de las Personas con Discapacidad que Naciones Unidas instauró en 1992 con el objetivo de poner en agenda el tema. **La discapacidad hoy es una cuestión de derechos humanos**, no es una cuestión médica ni de lástima”, detalló Huerga.

Respecto al proyecto que tiene media sanción en el Senado, para reconocer a toda persona del espectro autista con una pulsera azul, el subsecretario de Inclusión para Personas con Discapacidad afirmó que “es una lástima que no nos hayan consultado. Porque les hubiéramos dado nuestra opinión como gobierno, y también los hubiésemos contactado con instituciones que hace muchísimos años vienen trabajando en la temática”.

En ese marco, Huerga afirmó que **el proyecto es “un retroceso a viejos paradigmas**. Las personas con discapacidad, en este caso del espectro autista, deben ser tratadas de acuerdo a sus particularidades, como cualquier otra. No desconocemos la problemática, pero estamos convencidos de que cada familia puede implementar los medios necesarios y no obligar a todo el mundo”.

También explicó que “el artículo 22 de la Convención Internacional de los Derechos de las Personas con Discapacidad habla de que los Estados partes protegerán la privacidad de la información personal y relativa a la salud. Entendemos la preocupación que pueden llegar a tener las familias, pero el hecho de identificarlos con una pulsera no se puede obligar por ley a toda persona con autismo”.

En ese marco, las autoridades del Poder Ejecutivo han recibido innumerables mensajes de familias con personas con TEA manifestando el rechazo al proyecto de ley, así como también de las siguientes instituciones: la Comisión de Derechos Humanos de la Facultad de Psicología de la Universidad Nacional de Rosario; la Asociación Argentina de Padres de Autismo; la Federación Argentina de Autismo; el Colegio de Psicólogos de la provincia de Santa Fe; Asociación Civil Déjame Entrar y la Unión de Entidades de y para personas con Discapacidad de la Provincia de Santa Fe; quienes apoyan la postura del gobierno provincial.