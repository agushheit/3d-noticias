---
category: Agenda Ciudadana
date: 2021-02-18T07:21:09-03:00
thumbnail: https://assets.3dnoticias.com.ar/boleto-gratuito.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Diario Uno
resumen: 'El boleto educativo gratuito se presenta el jueves: cuáles son los requisitos'
title: 'El boleto educativo gratuito se presenta el jueves: cuáles son los requisitos'
entradilla: Se aplicará desde el 15 de marzo pero antes se iniciarán las inscripciones.
  Quiénes y cómo podrán acceder

---
Este jueves el gobernador Omar Perotti presentará el boleto educativo gratuito, una promesa de campaña que comenzará a aplicarse desde el 15 de marzo con el regreso de la actividad escolar presencial del nuevo ciclo lectivo

La inscripción comenzará en los próximos días desde el sitio web del gobierno provincial y habrá alcances y requisitos para poder obtener el beneficio. En tanto, la gestión santafesina informó que continuará funcionando el medio boleto para aquellos que no cumplan los requisitos.

El boleto gratuito será para estudiantes de diferentes niveles y modalidades incluidos alumnos de establecimientos privados y universidades.

También podrán acceder docentes y no docentes del sistema educativo provincial.

Habrá dos viajes por día entre las 6 y las 23.30 en todos los servicios urbanos

Para los tramos interurbanos (de menos de 60 kilómetros) serán dos viajes gratuitos por día hábil y en los tramos de más de 60 kilómetros, habrá dos viajes gratis por mes.

Entre los requisitos a cumplir, según anticipó Radio 2 de Rosario, los estudiantes deberán acreditar que el grupo familiar tiene “ingresos no mayores a dos canastas básicas totales”, actualmente en 108 mil pesos.

Para cumplir el trámite se pedirá una declaración jurada de ingresos y la ID Ciudadana que se hace en el sitio web de la provincia especialmente dedicado al boleto gratuito.