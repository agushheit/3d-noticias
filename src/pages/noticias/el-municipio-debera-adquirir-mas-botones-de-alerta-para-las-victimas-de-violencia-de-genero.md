---
category: La Ciudad
date: 2021-04-09T07:20:12-03:00
thumbnail: https://assets.3dnoticias.com.ar/larriera.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Ines Larriera
resumen: El Municipio deberá adquirir más botones de alerta para las víctimas de violencia
  de género
title: El Municipio deberá adquirir más botones de alerta para las víctimas de violencia
  de género
entradilla: 'Asi lo resolvió el Concejo Municipal luego de aprobar el proyecto de
  los concejales Inés Larriera y Carlos Pereira (UCR-Juntos por el Cambio), que insta
  al Ejecutivo a adquirir estos dispositivos. '

---
El Concejo Municipal aprobó este jueves un proyecto de resolución de los concejales Inés Larriera y Carlos Pereira (UCR-Juntos por el Cambio) para que el Ejecutivo proceda a la compra de los dispositivos conocidos como “Botones de Alerta”, que son destinados para mujeres víctimas de violencia de género. 

Al respecto, los ediles Larriera y Pereira manifestaron: "En febrero de este año presentamos un proyecto solicitando información sobre cómo se estaba trabajando en el Área de Mujeres y Disidencias de la Municipalidad.  Sabemos que son muchas las mujeres que están en la lista de espera para recibir esta medida de seguridad que se implementó hace 8 años". 

El sistema de botones de alerta destinado a las mujeres víctimas de violencia de género,  comenzó a funcionar en agosto de 2013. La ciudad de Santa Fe fue pionera en la implementación de este tipo de políticas,  con el objetivo de que el Municipio se involucre en la prevención. 

Larriera y Pereira sostuvieron que "hasta 2019 se entregaron 3327 botones a mujeres que lo requirieron. A fines de 2019 eran 967 las  mujeres que lo utilizaban. Es necesario que esta política siga vigente y que el Ejecutivo proceda a la compra de nuevos dispositivos, teniendo en cuenta la demanda existente". 

"Día a dia la realidad nos muestra que este es un problema que sigue teniendo fuerte presencia en la sociedad. Hace pocas horas nos enteramos de un nuevo femicidio ocurrido en el área metropolitana de Santa Fe. Cada 23 horas hay un nuevo femicidio en el país. Por eso es fundamental reforzar todas las políticas relacionadas a esta problemática, que puedan contribuir a proteger a las mujeres que se encuentran en riesgo y a reducir esta triste estadística", cerraron.