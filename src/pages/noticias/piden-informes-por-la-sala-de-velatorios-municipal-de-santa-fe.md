---
category: La Ciudad
date: 2021-10-22T06:00:16-03:00
thumbnail: https://assets.3dnoticias.com.ar/mudallel.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Piden informes por la sala de velatorios municipal de Santa Fe
title: Piden informes por la sala de velatorios municipal de Santa Fe
entradilla: "La concejala Jorgelina Mudallel elevó un pedido de informes en relación
  al funcionamiento de la Sala de Velatorios que posee la necrópolis local. \n\n"

---
Jorgelina Mudallel y Marcela Aeberhard requirieron a las autoridades municipales información en relación al funcionamiento de la Sala de Velatorios que funciona en el Cementerio Municipal.

 Asimismo, requieren conocer si actualmente se encuentra en funcionamiento dicha sala, sobre el estado de instalaciones de la misma, la cantidad de asistencias que se han brindado a lo largo del corriente año y si existe algún tipo de protocolo que se esté implementando.

 Además Mudallel y Aeberhard preguntan sobre cuál es el aforo de asistentes que actualmente se permite, y que se detallen en el informe si existen reclamos vinculados a su funcionamiento.

 **Servicio municipal**

 Desde mediados del año 2018 el Cementerio de Santa Fe cuenta con su sala de velatorios municipal producto de una ordenanza iniciativa de la ex concejala Marcela Aeberhard. “Lo que hicimos fue visibilizar esta necesidad para muchas familias santafesinas que pasando por un momento doloroso, se encuentran con la imposibilidad de tener una despedida digna”, sostuvo Aeberhard.

 “El sepelio es un servicio social oneroso para muchas familias que no pueden afrontarlo; por eso la sala de velatorios municipal gratuita es de mucha utilidad. Es sabido que, en este ámbito, la presencia del Estado es indispensable, a efectos de tutelar los derechos fundamentales de los ciudadanos y de brindar las herramientas necesarias para generar la mayor igualdad posible en la sociedad toda”, señalaron.

 El pedido de informes fue aprobado por unanimidad en la última sesión del Concejo Municipal.

 