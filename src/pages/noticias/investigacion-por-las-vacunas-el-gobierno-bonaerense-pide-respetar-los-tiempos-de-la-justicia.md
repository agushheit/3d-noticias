---
category: Agenda Ciudadana
date: 2021-01-07T08:49:20Z
thumbnail: https://assets.3dnoticias.com.ar/070121-carlos-blanco.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Télam'
resumen: 'Investigación por las vacunas: el gobierno bonaerense pide respetar «los
  tiempos de la Justicia»'
title: 'Investigación por las vacunas: el gobierno bonaerense pide respetar «los tiempos
  de la Justicia»'
entradilla: 'El jefe de Gabinete de la provincia de Buenos Aires, Carlos Bianco, se
  refirió a las 400 dosis que quedaron inutilizadas tras perder la cadena de frío. '

---
El jefe de Gabinete bonaerense, Carlos Bianco, dijo que «hay que ser respetuosos de los tiempos de la Justicia» en la investigación las 400 dosis de vacuna Sputnik V que quedaron inutilizadas tras perder la cadena de frío en el Hospital Oncológico de Olavarría, y afirmó que «las vacunas estaban en un freezer en la temperatura que correspondía».

En diálogo con radio Provincia, Bianco manifestó estar «muy preocupado» por la situación y dijo que esperan que no «vuelva a pasar», ya que «perder un lote de vacunas no es buena noticia para nadie».

«Llamó la atención que no funcionen las cámaras de seguridad, pero hay que dejar que la Justicia averigüe qué pasó», indicó el funcionario al referirse a la investigación a cargo del fiscal general de Azul, Marcelo Sobrino.

El martes, el ministro de Salud bonaerense, Daniel Gollan, denunció el hecho ante la Justicia, mientras que el fiscal Sobrino conformó que se investiga si el hecho se trató de una «falla eléctrica» o «un atentado».

El intendente de Olavarría esa localidad, Ezequiel Galli, advirtió también que si fue un sabotaje «habrá que ir hasta las últimas consecuencias» y explicó que al hospital «llegaron 450 dosis, de las cuales se habían aplicado ya 50» y dijo que «las otras 400 vacunas perdieron la cadena de frío por estar a temperatura ambiente».

**El jefe comunal afirmó que el municipio «no tuvo injerencia» y fue la Región Sanitaria IX quien tuvo participación en el control de las vacunas.**

El viceministro de Salud bonaerense, Nicolás Kreplak, dijo que desde el gobierno provincial esperan que no haya sido intencional la pérdida de la cadena de frío de 400 dosis de la vacuna Sputnik V.

«Tenemos un sistema bastante obsesivo y probado. Lo que observamos en Olavarría es que hubo un desvío de la cadena de frío», afirmó el funcionario en declaraciones a radio _El Destape_.

Kreplak sostuvo que, cuando las autoridades sanitarias fueron a ver qué pasó, advirtieron algunas cuestiones que les llamaron la atención, «por ejemplo, que la perilla del _freezer_ que tenía que estar al máximo, estaba al mínimo».

«También la cámara de seguridad dejó de grabar. Esperemos que no sea algo deliberado», dijo y precisó que «poner en la grieta una campaña de vacunación es bastante inhumano».