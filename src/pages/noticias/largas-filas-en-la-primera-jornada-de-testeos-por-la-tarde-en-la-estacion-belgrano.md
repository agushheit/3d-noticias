---
category: La Ciudad
date: 2021-12-30T06:00:59-03:00
thumbnail: https://assets.3dnoticias.com.ar/hisopados.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Largas filas en la primera jornada de testeos por la tarde en la Estación
  Belgrano
title: Largas filas en la primera jornada de testeos por la tarde en la Estación Belgrano
entradilla: 'Masiva concurrencia de santafesinos que fueron a hisoparse. Comenzaron
  a las 17 y en una hora ya habían testado a 400 personas.

'

---
Este miércoles comenzaron a funcionar en la ciudad de Santa Fe dos centros de hisopados por la tarde. La medida fue tomada en el contexto de una escalada de casos de coronavirus y ante el colapso que presentaban algunos centros de testeos.

En la Estación Belgrano, el operativo comenzó a las 17 y en apenas una hora, los profesionales que atienden en el lugar ya habían realizado 400 test.

Según adelantaron referentes de la Región Salud Santa Fe, el nivel de positividad se elevó al 30 por ciento.

Cerca de las 18, en el lugar había alrededor de 200 personas aguardando su turno. Entre los concurrentes, se daban discusiones vinculadas a las demoras y a los motivos por los cuales las personas iban a hisoparse.

Al haber un límite de personas a testear, varios santafesinos que llegaron a la estación debieron regresar sin ser hisopados y con la recomendación de volver al día siguiente

**Horarios y lugares**

De lunes a viernes, de 17 a 20, se realizarán hisopados en la Estación Belgrano y en el viejo Hospital Iturraspe. Desde la cartera sanitaria, informaron que el viernes 31 no se realizarán hisopado por la tarde (sí por la mañana)