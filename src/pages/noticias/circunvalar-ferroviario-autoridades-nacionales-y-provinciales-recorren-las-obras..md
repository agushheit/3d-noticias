---
category: Estado Real
date: 2022-11-17T17:34:15-03:00
thumbnail: https://assets.3dnoticias.com.ar/2022-11-17NID_276616O_2.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: 'Circunvalar Ferroviario: autoridades nacionales y provinciales recorrieron
  las obras. '
title: 'Circunvalar Ferroviario: autoridades nacionales y provinciales recorrieron
  las obras. '
entradilla: Los trabajos permitirán a las formaciones ferroviarias del Belgrano Cargas
  evitar transitar por la ciudad capital, brindará mayor seguridad, y reducirá el
  tiempo de llegada de la producción a los puertos.

---
El gobernador de la provincia, Omar Perotti, junto al titular de Trenes Argentinos, Martín Marinucci, inspeccionó este jueves por alrededor de tres horas las obras ferroviarias en marcha del Circunvalar Santa Fe, que por primera vez evitará el paso del tren de carga por el ejido urbano, con una inversión que supera los $ 7.000 millones y beneficiará a más de medio millar de personas del Gran Santa Fe.

Los trabajos, que Nación y provincia comenzaron en Empalme San Carlos en 2021, constan de 15,5 km de construcción de nuevas trazas, así como la recuperación de otros 45 kilómetros de vías nuevas sobre trazas existentes, pero actualmente no operativas, y que incluye una serie de obras complementarias, todo enmarcado en el Plan de Modernización del Transporte Ferroviario impulsado por el Ministerio de Transporte de la Nación en conjunto con el gobierno provincial.

La obra implicará mejorar el acceso de la producción agraria del norte argentino a los puertos de exportación, y facilitará la circulación ferroviaria del Belgrano Cargas al evitar el paso del tren por el entramado urbano del Gran Santa Fe.

La ingeniera del Belgrano Cargas, Marianela Bernardi, explicó que actualmente **_“son 17 kilómetros de vías las que hay que renovar -del total de 45km- y nos restan renovar solo tres. A su vez, vamos a hacer 10 pasos a nivel, a instalar barreras y señalización activa, y también se va a hacer la renovación de alcantarillas”._**

**_“Tenemos cinco alcantarillas que son in situ, son puentes de 12 metros aproximadamente, y alcantarillas prefabricadas. También se va a mejorar la estación de bombeo que se encuentra en Santo Tomé, triplicando su capacidad. Esta es una obra muy importante porque forma parte del plan integral de renovación del Belgrano Cargas_**”, remarcó la ingeniera.

**DETALLES DE LA OBRA**

Además de la construcción de 15,5 kilómetros de vías en nueva traza y la recuperación de unos 45 kilómetros existentes, el proyecto implica un nuevo puente ferroviario de 900 metros de largo sobre el río Salado, tres nuevos puentes vehiculares sobre las rutas provinciales N°70, N°11 y N°4, para cruces a distinto nivel, 53 nuevas alcantarillas en 60 kilómetros de vías, el cerramiento en la zona rural y urbana y la relocalización de la estación de bombeo de Santo Tomé. Asimismo, se mantendrán las cuatro bombas actuales y se agregarán otras cuatro.

Es la primera vez en la historia argentina que se realiza una circunvalación ferroviaria de esta envergadura para evitar el paso del tren de carga por una ciudad, lo que beneficiará a más de 525.000 habitantes del Gran Santa Fe.

**MENOR IMPACTO AMBIENTAL Y MAYOR SEGURIDAD VIAL**

El Circunvalar Santa Fe significará un menor impacto ambiental del tren en el entramado urbano, la integración de la ciudad al disminuir las barreras urbanas, una mejora de las comunicaciones y los servicios, un impacto positivo para la flora, paisaje y forestación debido a la ejecución de obras complementarias, la limpieza y revegetación de predios, una mayor seguridad vial por la disminución del riesgo de accidentes, y el respeto y mantenimiento de las actuales condiciones hidráulicas de la zona.

A su vez, implica la contratación de mano de obra local y redunda en una mejora de la actividad económica de la zona, tanto de bienes como de servicios.

En cuanto a la reducción del tiempo, se pasará de tardar 10 horas entre Santa Fe y los puertos de Timbúes, a 2.30 horas, se registrará un aumento de velocidad del tren: de 20 km/h a 65 km/h, así como de su frecuencia, ya que pasará de dos trenes con 45 vagones por día a ocho trenes de hasta 100 vagones, y una reducción de costos logísticos en un 30%.