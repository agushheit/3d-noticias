---
category: La Ciudad
date: 2021-11-13T06:15:47-03:00
thumbnail: https://assets.3dnoticias.com.ar/REGISTRO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: El Registro Civil de Santa Fe abrirá sábado y domingo para retiro de DNI
title: El Registro Civil de Santa Fe abrirá sábado y domingo para retiro de DNI
entradilla: Aproximadamente son 6 mil los DNI que se encuentran para retirar en toda
  la provincia.

---
La Secretaria de Justicia a través del Registro Civil de la Provincia de Santa Fe, informó que las oficinas estarán abiertas para el retiro de DNI durante las jornadas del sábado y domingo, con motivo del acto eleccionario.

 Al respecto, detallaron que el horario de atención será el siguiente:

 **Sábado 13: de 9 a 12 hs.**

**Domingo 14: de 9 a 16 hs.**

 Los mismos se pueden retirar en las oficinas del Registro Civil ubicadas en San Luis 2950, en la ciudad de Santa Fe y en el resto de las delegaciones provinciales.

 El director del Registro Civil, Mariano Galvez, informó que “se abren las sedes del Registro en cada localidad de la provincia, con la intención de que los días sábado y domingo, aquellas personas que por razones laborales o por situaciones personales, no pueden acercarse en el horario habitual en las oficinas, a recibir el DNI, lo puedan hacer el fin de semana, en un horario más flexible, para recibirlo y emitir el sufragio el próximo domingo”.

 “Es importante destacar que el RENAPER implementó un sistema de seguimiento ON LINE del DNI, a través de un link se puede conocer en tiempo real donde se encuentra el documento, y es útil para aquellos que no tienen la confirmación de que el documento está en el Registro Civil, para hacer la consulta antes de concurrir a las dependencias ”, informó el funcionario.

 Cabe mencionar que aproximadamente se encuentran para retirar 6000 documentos, 2100 de Rosario, 2900 en Santa Fe, y el resto en las delegaciones de la provincia