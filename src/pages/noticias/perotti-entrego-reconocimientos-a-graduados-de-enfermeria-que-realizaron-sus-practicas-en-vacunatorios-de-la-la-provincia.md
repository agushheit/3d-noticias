---
category: Estado Real
date: 2021-11-10T21:38:26-03:00
thumbnail: https://assets.3dnoticias.com.ar/enfermeras.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Perotti entregó reconocimientos a graduados de enfermería que realizaron
  sus practicas en vacunatorios de la la provincia
title: Perotti entregó reconocimientos a graduados de enfermería que realizaron sus
  practicas en vacunatorios de la la provincia
entradilla: Son 725 graduados oriundos de Santa Fe, Rosario, Casilda, Esperanza, San
  Cristóbal, Reconquista, Firmat y Rufino.

---
El gobernador de la provincia, Omar Perotti, y las ministras de Educación, Adriana Cantero, y de Salud, Sonia Martorano, entregaron este martes 725 reconocimientos a los graduados de la carrera “Técnico Superior en Enfermería” que realizaron sus prácticas profesionalizantes en los vacunatorios habilitados para la Campaña de Vacunación Covid 19. Dichas prácticas se realizan el último tramo de la carrera.

A fines de 2020, a través de una resolución conjunta entre los ministerios de Educación y de Salud se determinó que los estudiantes de la carrera lleven adelante su Práctica Profesionalizante en vacunatorios, participando con su formación en el Operativo de Vacunación más importante de la historia de la provincia.

De los 725 graduados, 315 corresponden a Santa Fe, 250 a Rosario, 28 a Casilda, 15 a Esperanza, 18 a San Cristóbal, 37 a Reconquista, 44 a Firmat y 18 a Rufino.

En la oportunidad, el gobernador felicitó a todos los graduados: “Para cada uno de ustedes es un día inolvidable, termina una etapa y se abren otras posibilidades. Disfrútenlo plenamente. Estamos muy agradecidos por lo que ustedes han hecho, por las prácticas, por la elección que han tomado como carrera pero, fundamentalmente, elección de vida. Lo mejor para ustedes en esta decisión”.

En la misma línea, Martorano puntualizó: “Tengo el corazón que hierve de felicidad, porque es un día de alegría y felicidad, todo esto pasó en un año muy especial, ustedes fueron enfermeros y enfermeras en la primera línea durante la pandemia, van a contar que fueron parte de la historia de la salud, en el mega operativo provincial de vacunación”.

Del mismo modo, Cantero destacó: “Cumplieron una etapa muy importante en la vida profesional, la provincia tiene muchos más estudiantes de enfermería con diferentes instituciones para que haya más jóvenes que puedan desarrollar la profesión con arraigo, eso es el resultado de una gestión y un Estado presente”.

**LA CARRERA**

La carrera Técnico Superior en Enfermería es una carrera de nivel Superior. Supone tres años de estudio. Actualmente se desarrolla en institutos superiores del Ministerio de Educación de Gestión Estatal y Privada y en escuelas de Enfermería del Ministerio de Salud. El Plan de Estudios está encuadrado en las resoluciones 2487 Ministerio de Educación y 0093 Ministerio de Salud del año 2017.

El técnico superior en Enfermería se forma para la promoción, prevención, recuperación y rehabilitación de la persona, la familia, grupo y la comunidad. Hasta el nivel de cuidados intermedios, en los ámbitos comunitario y hospitalario, gestionando su ámbito de trabajo y participando en estudios de investigación-acción.

La carrera se puede estudiar en Rufino, Firmat, María Teresa, Venado Tuerto, Rosario, Rafaela, Santa Fe, Esperanza, Casilda. San José del Rincón, San Justo, San Cristóbal, Reconquista, Villa Ocampo, Vera, Tostado, Oliveros, San Jorge, Cañada de Gómez, Calchaquí, Helvecia.

**PRESENTES**

Participaron también de la actividad los secretarios de Salud, Jorge Prieto; de Educación, Víctor Debloc; de Administración del Ministerio de Salud, Pamela Suárez; su par de Educación, Cristian Kuverling; de Coordinación de Salud del Área Metropolitana de Rosario, Miguel Rabbia; los subsecretarios de Educación Superior, Patricia Moscato; de Planificación y Fortalecimiento, Leandro Constantini; los directores de Enfermería, Emilse Belletti; y de Investigación y Evaluación Educativa, Francisco Corgnalli, entre otros.