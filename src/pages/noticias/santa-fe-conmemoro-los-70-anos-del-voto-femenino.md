---
category: Estado Real
date: 2021-11-11T06:00:11-03:00
thumbnail: https://assets.3dnoticias.com.ar/votofemenino.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Santa Fe conmemoró los 70 años del voto femenino
title: Santa Fe conmemoró los 70 años del voto femenino
entradilla: 'Con actividades en la explanada de El Molino, Fábrica Cultural, junto
  al DOMO MICAELA, la provincia se sumó a la Acción Federal #UnVotoPorLaParidad del
  Ministerio de Mujeres, Géneros y Diversidad de la Nación.'

---
Al cumplirse 70 años de la primera vez que las mujeres pudieron votar y ser votadas, el gobierno provincial, a través del Ministerio de Igualdad, Género y Diversidad, conmemoró este miércoles el voto femenino y se sumó a la Acción Federal #UnVotoPorLaParidad del Ministerio de Mujeres, Géneros y Diversidad de la Nación.

La actividad se desarrolló en la explanada de El Molino, Fábrica Cultural, junto al DOMO MICAELA, donde decenas de mujeres se sumaron a participar de la jornada que recordó esta importante fecha para los derechos políticos de las mujeres.

Al respecto, la ministra de Igualdad, Género y Diversidad, Celia Arena, destacó que “este evento, en clave federal, es una punta más de todo el trabajo que hacemos en conjunto, un distintivo de la gestión del Ministerio de Mujeres, Género y Diversidad; y en este día rememorando 70 años, un suspiro en la historia”.

“Pensar que hace 70 años, la mujeres ni siquiera teníamos el derecho político de elegir, ni de ser electas, nos pone en la dimensión de todo lo que avanzamos y nos hace reflexionar, también”, resaltó la ministra santafesina; y señaló que “es una celebración por nuestros derechos, los derechos políticos y la militancia que hubo, de Eva Perón, que pudo recoger toda la militancia de distintas mujeres a lo largo de la historia de la Argentina, y construir ese voto para lograr ese avance y la representación de tantas mujeres”.

En tanto, la subsecretaria de Políticas de Igualdad del Ministerio de Mujeres, Géneros y Diversidad de la Nación, Pilar Escalante, destacó la “federalidad de la acción, que está siendo muy bien recibida. Se están acercando mujeres, lesbianas, travestis y trans, y esto nos habla de una historia que es la que nos ha traído hasta acá, que nos da la fuerza para seguir avanzando y tener efectivamente una participación igualitaria en la política, en la economía, en la cultura”.

Por último, la secretaria de Mujeres, Géneros y Diversidad provincial, Florencia Marinaro, precisó que “nos sumamos a una actividad del Ministerio de Mujeres, Géneros y Diversidad de la Nación, que invitaron en plazas de todo el país a conmemorar el voto femenino, este derechos que las mujeres conquistamos hace más de 70 años. Estamos aquí celebrando un derecho, y también la paridad, que es un derecho que hace pocos años también conquistamos”.

**Acción Federal**  
Esta acción tuvo su correlato en las principales plazas de nuestro país, donde convocadas por esta fecha que remite al aniversario de los 70 años desde la primera vez que las mujeres pudieron votar, tras la sanción de la Ley de los Derechos Políticos, y ser votadas, participaron de distintas actividades conmemorativas.

En Santa Fe hubo proyecciones y una obra de teatro: “Mujeres Inolvidablemente Luchadoras – Julieta Lanteri & Eva Perón”.