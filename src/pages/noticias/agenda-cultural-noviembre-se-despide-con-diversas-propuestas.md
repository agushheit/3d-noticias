---
category: Cultura
date: 2021-11-27T06:00:50-03:00
layout: Noticia con imagen
author: 3D Noticias
resumen: 'Agenda cultural: noviembre se despide con diversas propuestas'
title: 'Agenda cultural: noviembre se despide con diversas propuestas'
entradilla: 'Habrá música en vivo con el homenaje a Ariel Ramírez en el que participan
  coros y orquestas de las escuelas de la ciudad, los encuentros Santa Fe tiene Blues
  y Santa Fe Jazz Campus, con la presencia de Ernesto Jodos. '
thumbnail: https://assets.3dnoticias.com.ar/ARIELR.jpg
link_del_video: ''

---
El último fin de semana del mes invita a disfrutar de una extensa programación cultural, en los espacios gestionados por la Municipalidad. Las propuestas incluyen grandes eventos musicales y la realización de la primera edición de +Feria, organizada por la Municipalidad y galerías de arte de Santa Fe, Rosario, Unquillo y Paraná.

Entre los eventos destacados, el sábado 27 a las 20.30, en el Anfiteatro del Parque del Sur, se llevará a cabo el concierto “Homenaje a Ariel Ramírez”, que protagonizan 270 niñas, niños y jóvenes, de coros y escuelas de música de la ciudad, acompañados por músicos profesionales de Sonamos Latinoamérica y el Coro Municipal de Santa Fe. La dirección artística está a cargo del Mtro. Manuel Marina, también responsable de los arreglos para “Mujeres Argentinas”. El proyecto es parte de las acciones que organiza el municipio para celebrar el centenario del nacimiento del pianista y compositor. La entrada será gratuita, con retiro previo en la boletería del Teatro Municipal (San Martín 2020), hasta dos por persona: lunes a sábados de 9 a 13 y de 17 a 21, domingos de 10 a 12 y de 17 a 21 horas.

También el sábado desde las 20.30, pero en el Patio Avellaneda habrá una Noche de Rap y Hip Hop, con Leo Cuatro Dos Cero, Gogoclap, Delfino & Puro Flow Band y Bruno Tredici DJ. Además actuarán por primera vez en vivo los integrantes del Taller Integral de Hip Hop Mediateca Crew, que se desarrolló en Santa Rosa de Lima durante este año, como parte de los espacios formativos de la Secretaría de Educación y Cultura de la Municipalidad. La entrada será gratuita, y se contará con la presencia de emprendedores y puestos gastronómicos en el ingreso a la Estación Belgrano por Avellaneda y Castellanos.

El sábado 27 y el domingo 28 de 17 a 22.30 horas, se desarrollará +Feria en la planta alta de la Estación Belgrano. En las dos jornadas se podrán conocer las propuestas de 13 galerías de Santa Fe, Rosario, Paraná y Unquillo, en paralelo con mesas editoriales especializadas, presentaciones y conversatorios, y una subasta de obras.

**Gugliotta-Cabal en la Sala Ramírez**

El ciclo de música en la sala Ariel Ramírez presenta el jueves 25 a las 20, al dúo que integran la soprano María Florencia Gugliotta y la pianista Amparo Cabal, consolidadas en esta formación mientras estudiaban en el Instituto Superior de Música de la UNL. El concierto será en el espacio ubicado en la planta alta de la Estación Belgrano, donde se podrá disfrutar de esta propuesta de música de cámara para canto y piano de compositores de los diversos períodos históricos entre el clasicismo y la actualidad. La entrada es gratuita. La próxima fecha será el 2 de diciembre, con la participación de Luciana Tourné Trío.

La sala también se podrá visitar durante la semana en Bv. Gálvez 1150, hasta el viernes, de 9.30 a 12.30 horas. Entre otros contenidos incluye la muestra patrimonial “100 años, 100 canciones”, basada en la colección de objetos y documentos personales del artista, y la muestra fotográfica “Puntos que conectan”, del Archivo de la Imagen Documental Santafesina.

**Cómo se hizo “Espejo de agua”**

En la Sala Cero del Museo de la Constitución se realizará el desmontaje de la obra de teatro de sombras contemporáneo “Espejo de agua” de la Colectiva Rodante. El encuentro será hoy a las 21 horas, en el espacio ubicado en 1º de Mayo y Avda Circunvalación, en el marco de la intervención artística “Disposiciones transitorias” que realiza el colectivo Proyecto Deatres, con la coordinación de Victoria Ferreyra y Raquel Minetti.

Durante el encuentro, las realizadoras compartirán detalles del proceso creativo de la obra que indaga en el pasado y el presente de la laguna Setúbal. Parte de la premisa de buscar en las penumbras de la historiografía para recuperar los nombres de la laguna como una acción anticolonialista de nuestra historia, nuestros parámetros de belleza, relatos y relación con la naturaleza. En el relato enfatizan especialmente sobre la presencia de los quiloazas, pueblo originario de nuestra laguna, del que poco se sabe. Los créditos de la puesta cuentan con Paula Yódice en la narración y lectura, Sofía Esper como bailarina/intérprete, Laura Martínez en las visuales, Joselina Martínez en la voz en off, Julia Torres en sonido, Esteban Coutaz en la música original, Sofía Gerboni en la asesoría de vestuario y Aldana Mestre en el registro fotográfico.

“Disposiciones transitorias” continúa durante estos meses y se incorpora a la narrativa de las visitas guiadas. En noviembre, el Museo se puede visitar de jueves a domingos de 9.30 a 12.30 y de 16 a 19 horas, con entrada libre y gratuita. A partir de diciembre, abrirá los mismos días, pero de 17 a 20 horas.

**Salón Litoral en el MMAV**

Desde el jueves pasado se puede recorrer el “Salón Litoral 2021”, en el Museo Municipal de Artes Visuales “Sor Josefa Díaz y Clucellas” (San Martín 2068). La muestra se compone de obras de 30 artistas seleccionados entre 227 que se postularon de todo el país. El montaje incluye la obra de María Paula Massarutti que recibió el Premio Adquisición, las de Virginia Abrigo, Soledad Rolleri, Jerónimo Veroa, Luciana Paoletti y Daniela Arnaudo, que recibieron premios estímulo; y tres que recibieron menciones honoríficas de Gisela Ajzensztat, Rodrigo Illescas y María Eugenia Pérez. Los días y horarios para visitar el salón son de miércoles a sábado, de 9.30 a 12.30 y de 17 a 20; domingos y feriados, de 17 a 20 horas.

**Santa Fe Jazz Campus con Ernesto Jodos**

Durante este fin de semana se desarrollarán las últimas actividades de este mes dentro de Santa Fe Jazz Campus, que desde 2019 promueve distintas actividades orientadas al aprendizaje y difusión de la música de jazz en nuestra ciudad a partir de una iniciativa de Sebastián López, Pepi Dallo y Chano Casas. Este año el curso comenzó en septiembre y durante tres meses ofreció en modalidad virtual y presencial, conciertos, masterclasses, talleres y ensambles con la participación de destacados referentes.

En el marco de esta tercera edición organizada junto a la Municipalidad, el sábado 27 de noviembre desde las 20 horas, el pianista Ernesto Jodos, una de las figuras destacadas del jazz en Sudamérica, dará un concierto en el Mercado Progreso (Balcarce 1635) junto a Pablo Aristein en saxo, Sebastián López en guitarra, Cristian Bórtoli en contrabajo y Chano Casas en batería.

Ese mismo día, pero por la tarde, Jodos dará la masterclass Conceptos de Improvisación para Ensamble de Jazz, en la sede del Liceo Municipal (Molino Marconetti). El domingo 28 desde las 19 horas, también en el espacio educativo ubicado en el Dique II Puerto de Santa Fe, habrá un concierto con ensambles instrumentales, ensambles de voces, en una jam session abierta a todo público.

**Santa Fe tiene Blues**

The Polacos, Fede Teiler y Asociación Litoraleña de Blues, de Paraná, se presentan el viernes 26 de noviembre a las 18 horas, en el Mercado Progreso. Con entrada libre y gratuita se podrá disfrutar de este reencuentro con el blues, que un grupo de músicos santafesinos impulsa desde 2015 a partir del trabajo con instituciones locales. Esta iniciativa que en sus primeras ediciones se llamó Festival de Blues en Santa Fe busca recuperar la escena de ese género musical que en la década de 1990 había puesto a la capital en el circuito nacional. En esa tradición están los recitales que compartían bandas como Memphis La Blusera, Pappo´s Blues, La Mississippi, La Bolsa, Las Blacanblues, Miguel Botafogo Trío, con bandas locales como La Polaca Blues, La Jefferson Blues Band, La Fulana Blues Band, La Olegario y tantas otras reconocidas por su gran calidad.

**En la Sala Mayor**

La programación del Teatro Municipal “1º de Mayo” tendrá hoy una noche de festejos. Desde las 20.30 la novena edición del espectáculo de folclore latinoamericano “América Baila”, invita a celebrar los 30 años de trayectoria de la Compañía Folklórica Yapeyú. La entrada general tiene un valor de $500.

El sábado 27 se presenta “Alicia y el vuelo” en dos funciones, a las 18 y a las 20 horas. Esta producción con 100 artistas en escena integra el teatro, la danza aérea y contemporánea, las acrobacias artísticas, el cine y la poesía, con la dirección de la coreógrafa, bailarina, danzaterapeuta y directora del Espacio Cultural Del Creador, Mónica Romero Sineiro. La entrada general se vende a $500; con descuento para socios/as de UPCN, $400. Además, comprando entradas para las dos funciones, se pueden adquirir a $450 cada una para el sector de plateas bajas y altas.

La “Gala de Ópera en Concierto” de la Compañía Coral de Santa Fe se reedita el domingo 28 a las 20 horas. El repertorio incluye coros y arias de Mozart, Verdi, Puccini, Bellini, Donizetti, entre otros compositores, con la participación de la soprano María Belén Rivarola, la mezzosoprano Guadalupe Barrientos, el tenor Gastón Oliveira Weckesser y el pianista Mario Spinosi, bajo la dirección de Pablo Villaverde Urrutia. La entrada es gratuita. Los pases se deben retirar previamente en la boletería del Teatro.

**Domingo en Sala Marechal**

El 28 de noviembre a las 21 horas se presenta “La Invocación. Versión absurda de la historia de un invento que cambió nuestras vidas”. Será la última función de esta puesta que interpretan Eduardo Fessia, Gabriela Feroglio, Miguel Pascual y Ruy Gatti, con dramaturgia de Sebastián Roulet, también a cargo de la dirección con María Flavia del Rosso. La soledad, las supersticiones, el amor, las divisiones políticas, y un invento revolucionario, son los ejes que atraviesan esta obra seleccionada en la categoría Producción Emergente por el jurado de Escena Santafesina 2019. Las entradas anticipadas y con descuento para estudiantes y jubilados, se venden a $400; mientras que el día de la función costarán $500.