---
category: Agenda Ciudadana
date: 2021-08-03T06:15:37-03:00
thumbnail: https://assets.3dnoticias.com.ar/BILLETERA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Perotti dijo que se evalúa sumar más rubros al programa "Billetera Santa
  Fe"
title: Perotti dijo que se evalúa sumar más rubros al programa "Billetera Santa Fe"
entradilla: El mandatario confirmó que el programa va a continuar "más allá de la
  pandemia". Se trata de la app que brinda reintegros del 30 por ciento por compra
  realizada.

---
El gobernador de la Provincia, Omar Perotti, dijo este lunes que el gobierno evaluará a finales de año sumar más rubros a la aplicación Billetera Santa Fe. Se trata de la app que permite, a través del pago virtual mediante un smartphone, obtener un reintegro del 30 por ciento por sobre la compra realizada.

El mandatario provincial aseveró que se va a sostener "más allá de la pandemia", ya que los comercios seguirán requiriendo asistencia del Estado. Añadió que sobre finales de año analizarán qué otro rubro incorporará.

Por otro lado, habló sobre la interna del Partido Justicialista que se dirimirá en las Primarias, Abiertas, Simultáneas y Obligatorias (Paso) al señalar que no responderá "críticas o agravios. Se apuesta a un cambio importante en la provincia. Vinimos a cambiar las cosas, a decir que las cosas no van a seguir iguales y que hay que dar posibilidades a nuevos actores".

Perotti participó de un acto junto al ministro de Ciencia y Tecnología de la Nación Roberto Salvarezza en el predio del Conicet Rosario. En ese marco, se disculpó con las personas que fueron citadas a la ex Rural este domingo y no pudieron recibir la segunda dosis de Sputnik V.

"Cuando se comete un error se reconoce. Claramente hubo un error humano. Se reconoce y se pide disculpas. El gobierno está tratando de subsanar la faltante con la llegada de más dosis y con la producción de ese componente de la segunda dosis en Argentina, más estudios que se hacen para ver la posibilidad de combinar vacunas", expresó en conferencia de prensa.

El gobernador se refirió así a la situación que ocurrió este domingo en la ex Rural, cuando personas que fueron citadas para la segunda dosis de la Sputnik V no pudieron recibir su vacuna por faltante del componente 2.