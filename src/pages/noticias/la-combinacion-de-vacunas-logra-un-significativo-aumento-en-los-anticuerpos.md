---
category: Agenda Ciudadana
date: 2021-08-19T06:00:02-03:00
thumbnail: https://assets.3dnoticias.com.ar/VACONVINACION.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: La combinación de vacunas logra un "significativo" aumento en los anticuerpos
title: La combinación de vacunas logra un "significativo" aumento en los anticuerpos
entradilla: Fue revelado en los resultados preliminares de los estudios heterólogos
  de inmunización contra la Covid llevados a cabo gracias a más de dos mil voluntarios.

---
Resultados preliminares de los estudios heterólogos de inmunización contra el coronavirus que se realizan en el país demostraron que las combinaciones entre las vacunas analizadas no causan efectos adversos graves y producen un "significativo" aumento en los anticuerpos.

Los primeros datos de esta investigación fueron presentados esta tarde a través de una teleconferencia en la que la ministra de Salud, Carla Vizzotti, participó desde Moscú junto al titular del Fondo Ruso de Inversión Directa, Anatoly Braverman, además de autoridades sanitarias de las jurisdicciones involucradas.

Más de dos mil voluntarios de las provincias de Buenos Aires, Córdoba, La Rioja, San Luis y la Ciudad Autónoma de Buenos Aires participan del estudio de combinación de vacunas contra el Covid-19 que comenzó en el mes de julio y que presentará sus resultados finales durante la primera quincena de septiembre.

El Estudio Colaborativo para la evaluación de Esquemas Heterólogos de Vacunación contra Covid-19 en la República Argentina (ECEHeVac) registra 560 casos de combinación de vacunas por cada jurisdicción, 280 mayores de 60 años de edad y 280 de entre 18 y 59.

El estudio se propone evaluar la inmunogenecidad y los efectos adversos en los esquemas de vacunación heterólogos conformados por la combinación de vacunas disponibles hasta el momento en el país: Sputnik V, Astrazeneca, Sinopharm, Moderna y Cansino.

Este relevamiento cuenta con 8.310 voluntarios inscriptos, de los cuales 2.361 ya fueron enrolados y 1.165 ya fueron vacunados contra Covid-19 con distintos esquemas combinados de vacunas.

Este proceso comenzó en la Ciudad de Buenos Aires el pasado 12 de julio, en la provincia de Buenos Aires el 19 de julio, en San Luis el 7 de agosto, en Córdoba el 9 de agosto y en La Rioja el 11 de agosto.

La Ciudad de Buenos Aires y la provincia de Buenos Aires pudieron presentar en la conferencia de esta tarde los datos de seguridad e inmunidad a 14 y 28 días de la vacunación, mientras que en las provincias de Córdoba, La Rioja y San Luis se espera que los resultados finales de este estudio sean presentados entre 5 y el 11 de septiembre.

La ministra de Salud, Carla Vizzotti, afirmó que "este estudio es fruto de la cooperación no solo entre jurisdicciones argentinas sino con el Fondo Ruso de Inversión Directa, porque este estudio no solo produce evidencia para Argentina sino para los programas de inmunización de la región y a nivel mundial".

"La idea es sumar evidencia para que los efectores de salud y tomadores de decisión puedan alentar la combinación de vacunas sobre bases sólidas; este es un estudio con una fortaleza científica muy importante que nos llena de orgullo poder presentarlo y contar el camino que queremos recorrer para dar respuestas basada en la evidencia y con consenso federal", subrayó.

El titular del Fondo Ruso de Inversión Directa, Anatoly Braverman, destacó que "Argentina fue uno de los primeros países que habilitaron el uso de Sputnik V y apreciamos mucho la colaboración con Argentina, por eso este Fondo y el ministerio de Salud argentino son auspiciantes del estudio de combinación de vacunas".

"Nos alegra mucho anunciar los resultados de estos estudios porque los datos muestran que Sputnik Light, que es el primer componente de Sputnik V, brinda excelentes resultados en combinación con otras vacunas; Sputnik Light es una vacuna independiente con una eficacia muy elevada y sirve de 'booster' para aumentar la efectividad de otras vacunas", apuntó.

Braverman recordó que "los diseñadores de Sputnik V fueron los primeros en utilizar combinación de vacunas gracias al uso de dos diferentes vectores de adenovirus en sus dos componentes, ahora muchos diseñadores utilizan el enfoque de Sputnik porque asegura una respuesta inmune más fuerte y más prolongada".

"Desde el principio nosotros estuvimos a favor de la colaboración entre productores de vacunas y de avanzar en investigaciones conjuntas, como hacemos desde finales de 2020 en un acuerdo con Astrazéneca", añadió.

A su turno, el ministro de Salud bonaerense, Nicolás Kreplak, explicó que no se reportaron eventos adversos supuestamente atribuibles a la vacunación e inmunización (Esavi) graves en el estudio realizado con la combinación de vacunas de Astrazeneca, Sputnik, Sinopharm y Moderna, excepto una hospitalización no asociada a la vacunación.

Otra conclusión es que "no existe asociación entre el esquema aplicado (heterólogo u homólogo) y la presencia de sintomatología a 24, 48 y 72 horas" de la inoculación.

Si "se observa una asociación estadísticamente significativa entre poseer una comorbilidad previa y no presentar síntomas a las 48 y 72 hs. de aplicada la segunda dosis de la vacuna, tanto en esquemas homólogos como heterólogos", se indicó en estos resultados parciales.

En cuanto a inmunidad (título de anticuerpos), el estudio bonaerense observó "un aumento significativo en el nivel de anticuerpos IgG anti S luego de 14 días de aplicada la segunda dosis en las combinaciones evaluadas".

También encontraron "un aumento significativo en el nivel de anticuerpos neutralizantes SARS-CoV-2 luego de 14 días de aplicada la segunda dosis en las combinaciones evaluadas".

Entre otras autoridades, estuvieron en la presentación Pablo Nuñez, subsecretario de Coordinación Institucional del ministerio de Ciencia y Tecnología y Jorge Geffner, investigador en el Instituto de Investigaciones Biomédicas en Retrovirus y SIDA (Inbirs, Conicet-UBA), quien señaló que "en Argentina tuvimos una gran asignatura pendiente, que es construir un puente entre la salud pública y la ciencia" y afirmó que ahora "empezamos a reconvertir esa falencia".

En el mismo sentido, Kreplak destacó el "enorme sacrificio con el que muchos investigadores construyeron" este estudio "para poner los resultados a disposición de los que tenemos la responsabilidad de las decisiones".