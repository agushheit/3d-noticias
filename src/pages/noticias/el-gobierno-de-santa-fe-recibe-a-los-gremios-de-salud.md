---
category: Agenda Ciudadana
date: 2021-09-27T06:15:34-03:00
thumbnail: https://assets.3dnoticias.com.ar/SALUDGREMIOS.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: El Gobierno de Santa Fe recibe a los gremios de Salud
title: El Gobierno de Santa Fe recibe a los gremios de Salud
entradilla: |2-

  El jueves pasado había comenzado la serie de reuniones con los gremios que agrupan a los trabajadores docentes y de la administración central.

---
El Gobierno de Santa Fe continuará este lunes la ronda de reuniones paritarias con los gremios que representan a los trabajadores de la Salud, y que se inició la semana pasada con los sindicatos docentes y estatales.

El encuentro con los dirigentes de la Asociación de Médicos de la República Argentina (AMRA) y del Sindicato de Profesionales Universitarios de la Sanidad (Siprus) se realizará desde las 11 en la modalidad virtual.

Se trata de la etapa de revisión del acuerdo salarial acordado en marzo pasado, que incrementó los haberes de los agentes de las distintas reparticiones del Estado en un 36% en promedio.

El jueves pasado comenzó la serie de reuniones con los gremios que agrupan a los trabajadores docentes y de la administración central.

Tras esos encuentros, el ministro de Trabajo, Juan Manuel Pusineri, reconoció que "hoy tenemos un nivel inflacionario que supera a los números de aquel presupuesto", en referencia al aumento otorgado en el tercer mes del año.

"Queremos llevar la tranquilidad de que nuestra voluntad y nuestro compromiso es que el salario no pierda poder adquisitivo este año. Por eso, el gobierno va a formular una propuesta en el transcurso de la semana que viene, que cada gremio deberá evaluar. Vamos a trabajar para que la propuesta sea aceptada", añadió el funcionario.

El secretario general de la Asociación Trabajadores del Estado (ATE), Jorge Hoffmann, opinó que “resulta necesario y urgente la recuperación del salario de los trabajadores y especialmente de los sectores medios que vienen perdiendo en los últimos años, porque además el acuerdo paritario así lo refleja”.

Fuentes gremiales coincidieron en señalar que la revisión deberá oscilar en un mínimo del 10% de suba, con lo cual la oferta que presentará el Gobierno el viernes 1 de octubre estaría en línea con esas aspiraciones, ya que Pusineri adelantó en declaraciones a la prensa efectuadas ayer que estima que "el piso será del 10%".

El Índice de Precios al Consumidor (IPC) que mide el Instituto Provincial de Estadística y Censos (IPEC), y que es tomado como parámetro en las paritarias, aumentó un 2,8% en agosto, con lo cual en los primeros ocho meses del año tuvo un alza del 33,8%, en tanto en los últimos doce meses llegó al 53,2%.