---
category: Agenda Ciudadana
date: 2022-06-25T00:57:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/carpool-g1d05b2d31_1280.png
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3d noticias
resumen: 'Compartir el auto para ahorrar costos: una tendencia que crece.'
title: 'Compartir el auto para ahorrar costos: una tendencia que crece.'
entradilla: 'Carpooling es el nombre con el que se conoce esta práctica, y Carpolear,
  una aplicación santafesina, es la más utilizada.  '

---
El carpooling o auto compartido significa viajar varias personas en un mismo vehículo para llegar a un destino común. Su objetivo es claro y preciso: optimizar el uso del auto al maximizar la cantidad de asientos utilizados.

El contacto se produce a través de sitios gratuitos de Internet donde conductores y pasajeros publican los trayectos que necesitan recorrer, con fechas y horarios. Cuando hay una coincidencia, el sistema les permite ponerse en contacto, acordar la división de costos y empezar a viajar juntos. La aplicación mas conocida en Argentina, es Carpoole.ar,  su sitio web es carpoolear.com.ar.

En su sitio web, se puede leer las ventajas del sistema:

En primer lugar, uno de los mayores impactos positivos de Carpoolear es reforzar los lazos sociales. Al compartir un viaje, personas que muchas veces no se conocían entre sí, generan un vínculo a través de las charlas del viaje. Surgiendo posibilidades de trabajo y nuevos contactos, intercambiando puntos de vista sobre distintos temas, generando confianza entre la gente, formando hasta un sentimiento de “comunidad carpoolera”. Esto va logrando un cambio cultural frente a las personas desconocidas, además de viajes mucho más entretenidos y divertidos.

En segundo lugar, la práctica del carpooling genera un impacto ambiental positivo cuando dos conductores de automóviles que habitualmente viajan en autos separados deciden unirse y viajar en un mismo vehículo dejando el otro guardado en el garage. Si esto se hace habitual en las personas podríamos reducir la cantidad de autos circulantes y así disminuir también el tiempo de viaje, las emisiones de gases de efecto invernadero, el consumo de combustibles fósiles (recursos no renovables), el smog y la contaminación sonora que provocan los vehículos, entre otros beneficios.

La mayoría de los viajes que se cargan actualmente en la plataforma no generan un impacto ambiental directo. Los mismos son viajes interurbanos que se realizan ocasionalmente los fines de semana. Sin embargo, sirven para que las personas conozcan la práctica, se animen a viajar con desconocidos y encuentren una nueva forma de viaje. En un futuro, cuando se logre que la práctica se vuelva masiva, esperamos que las personas consideren el compartir el auto como algo viable para aplicar tanto en sus viajes ocasionales de fin de semana como a los de rutina, ir al trabajo, escuela, etc. También esperamos que más conductores se dejen llevar en auto ajeno y dejen el suyo guardado.

En tercer lugar, compartir el auto implica un beneficio económico para los viajeros. Cuando se finaliza un viaje se calcula el costo de combustible y peajes y se divide por la cantidad de ocupantes del vehículo. Entonces, cuando aumenta el número de ocupantes, se disminuye el costo del viaje y el mismo se vuelve más accesible.