---
category: Agenda Ciudadana
date: 2021-08-14T06:15:06-03:00
thumbnail: https://assets.3dnoticias.com.ar/PADRON.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Ya se pueden consultar los padrones para las PASO
title: Ya se pueden consultar los padrones para las PASO
entradilla: Las primarias abiertas, simultáneas y obligatorias se realizarán el próximo
  12 de septiembre. Consultá dónde votás.

---
La Cámara Nacional Electoral (CNE) informó este viernes que ya están disponibles los padrones definitivos para ser consultados por la ciudadanía, de cara a las primarias abiertas, simultáneas y obligatorias (PASO) que se realizarán el próximo 12 de septiembre.

"¡Ingresá en [http://padron.gob.ar](http://padron.gob.ar "http://padron.gob.ar") para saber tu lugar y mesa de votación!"

• Desde la Cámara se informó que "los electores argentinos residentes en el exterior solo se encuentran habilitados para votar en las elecciones generales" previstas para el 14 de noviembre próximo.

• En los comicios se habilitarán no más de ocho mesas de votación por establecimiento, con el fin de evitar la aglomeración de personas.

• Se implementará una franja horaria para quienes integren los grupos de riesgo, y se utilizarán elementos de protección personal e higiene, en el marco de la pandemia de coronavirus.

• Se incluirá la figura del "facilitador sanitario", quien deberá hacer cumplir el protocolo y las medidas de prevención contra el coronavirus.

**Reunión del ministro del Interior y la CNE**

El jueves, el ministro del Interior, Eduardo de Pedro, y el presidente de la CNE, Santiago Corcuera, dialogaron en torno a la organización de las PASO y las elecciones generales de este año en el marco de la pandemia.

Durante el encuentro, en el Salón de los Escudos de la Casa Rosada, de Pedro y Corcuera continuaron trabajando en una agenda centrada en perfeccionar los recaudos sanitarios acordados en el protocolo sanitario para los comicios.

Ambos funcionarios se comprometieron a seguir coordinando las acciones necesarias para asegurar el nivel de participación habitual, tanto en las PASO del 12 de septiembre próximo como en la general del 14 de noviembre, informaron fuentes oficiales.

**Transmisión y recuento de resultados electorales**

La Dirección Nacional Electoral (DINE) entregó ante la CNE los códigos fuente del sistema de transmisión, recuento y difusión de resultados provisorios para las PASO.

“Entregamos a la Justicia Nacional Electoral una copia de los dos softwares; tanto de transmisión como de totalización y difusión de los datos que serán utilizados en la noche de la elección”, informó la titular de la DINE, Diana Quiodo, según se informó en un comunicado del Ministerio del Interior.

La entrega se realizó el jueves y, además de Quiodo, participó la secretaria de Asuntos Políticos, Patricia García Blanco.

Tal como establece el artículo 108 del Código Nacional Electoral, la Junta Electoral mantendrá bajo resguardo y permitirá a los partidos las comprobaciones que requieran con la antelación necesaria.

"La importancia de la entrega es cumplir con la manda legal y ponerlo disposición de los partidos para que realicen las comprobaciones del sistema. Más adelante irán entregando distintas actualizaciones a medida que se avanza en el trabajo con los partidos políticos en el Consejo de Seguimiento de las elecciones 2021 de cara al simulacro del 21 de agosto", indicó la titular de la DINE.

De esta manera, se precisó en el comunicado oficial, la cartera que preside Eduardo De Pedro, "continúa avanzando en las acciones previstas en el cronograma electoral de cara a las elecciones primarias del 12 de septiembre".

El último sábado, la DINE realizó una prueba de respuesta del sistema de recuento provisorio de datos en el que se digitaron más de 6 mil telegramas con configuraciones de candidaturas y cargos diferentes de Chaco, La Pampa, San Luis, Santa Cruz y Tierra del Fuego.

Además, se evaluó el trabajo de las unidades operativas de Correo Argentino ubicadas en el barrio porteño de Barracas y en la localidad bonaerense de Monte Grande.