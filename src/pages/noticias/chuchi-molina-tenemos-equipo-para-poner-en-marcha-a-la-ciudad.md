---
category: La Ciudad
date: 2021-07-16T06:15:03-03:00
thumbnail: https://assets.3dnoticias.com.ar/CHUCHI2.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Chuchi Molina: “Tenemos equipo para poner en marcha a la ciudad” '
title: 'Chuchi Molina: “Tenemos equipo para poner en marcha a la ciudad” '
entradilla: La precandidata a concejal de Santa Fe presentó la lista que encabeza
  para las PASO, que incluye entre sus miembros a dirigentes UCR, Coalición Cívica
  y PRO, y de distintos barrios de la ciudad.

---
.La nómina esta liderada por el sector, que a nivel provincial, tiene como referente a José Corral, quien destacó a Chuchi Molina por su “capacidad de comprender a los otros, algo tan importante en este momento de angustias de dificultades, y su capacidad de diálogo y consenso, también tan necesario”.

Este jueves a la mañana, Adriana “Chuchi” Molina presentó la lista de precandidatos a concejales de Santa Fe que encabeza por el sector de Juntos por el Cambio que a nivel provincial lidera el exintendente José Corral, quien acompañó la actividad.

“Hemos tomado la decisión de conformar este equipo por la necesidad de que la ciudad se ponga en marcha. Y cuando decimos que la ciudad necesita ponerse en marcha lo que decimos es que Santa Fe requiere planificación, equipos que efectivamente lleven a cabo todas las decisiones que hay que tomar en estos momentos tan difíciles y tan complicados”, afirmó Chuchi Molina en diálogo con la prensa en la Plaza Pueyrredón, donde se llevó a cabo la presentación.

“Vamos a aportar desde el Concejo la experiencia, el compromiso, el diálogo, que nos parece tan importante en un tiempo tan difícil como el que estamos atravesando”, agregó la precandidata, quien puso como premisa empezar a proyectar la ciudad pospandemia, además de atender las necesidades inmediatas. “Los vecinos y vecinas nos hablan de la necesidad de liderazgo, decisiones firmes y claras, y atender la prestación de servicios esenciales, como limpieza, iluminación, seguridad o transporte. Venimos de cinco días sin colectivos, la sociedad necesita saber cómo seguirá presentándose ese servicio cuando vuelvan las clases, por ejemplo”, advirtió.

A su turno, el ex intendente José Corral valoró: “Chuchi es una lider natural, una mujer con mucha experiencia fue secretaria de Gobierno de nuestra gestión, concejal, es profesora universitaria, a lo que se suma su activa militancia por los derechos de la mujer”.

“Veo en ella esa capacidad de comprender a los otros que es tan importante en este momento de angustias, de dificultades. El momento requiere del acompañamiento de alguien que sea capaz de escuchar, de ponerse en el lugar del otro, de empatía. Además, está acompañada de un grupo de hombres y mujeres de todos los partidos de Juntos por el Cambio, de la Coalición Cívica y el PRO, y de diferentes barrios de la ciudad”, subrayó José Corral.

**Pluralidad y diversidad**

La propuesta que encabeza Adriana Molina para las PASO se llama Vamos Juntos. Al respecto, detalló: “Somos una lista plural y diversa, en edades y en lo territorial, pero con un objetivo común, que es poner a la ciudad en marcha. Plural porque estamos todos los sectores políticos que conforman Juntos por el Cambio: está Carlos Pereira un dirigente, como dice José, que es de los que más conoce la ciudad; Ángeles Álvarez, una joven abogada que tiene toda esa energía para abordar los temas nuevos que tienen que estar en el Concejo, pensando en los jóvenes, en el primer empleo, en los emprendedores; Mauricio Amer, que es el titular de la Coalición Cívica ARI, en la provincia; Ivana Alvarenga, una militante desde 2011 del PRO en la ciudad de Santa Fe”. Mencionó además a “Ramón Narváez, un histórico vecinalista de Barranquitas. Clara Ruda, una abogada, pero como le gusta decir a ella, la mamá de un nene, un gran referente en el tema educación. Y Luis Pancera, otro referente barrial”.

![](https://assets.3dnoticias.com.ar/CHUCHI1.jpeg)