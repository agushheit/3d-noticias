---
layout: Noticia con imagen
author: Por
resumen: Unión perdió ante Bahía
category: Deportes
title: "Sudamericana: Unión perdió ante Bahía por un error de Moyano"
entradilla: El Tatengue perdió por 1 a 0 contra su rival brasileño en el estadio
  Arena Fonte Nova de Salvador Bahía. La revancha se jugará el martes 1 de
  diciembre a las 19:15 en el 15 de Abril.
date: 2020-11-25T12:49:08.979Z
thumbnail: https://assets.3dnoticias.com.ar/union.jpg
---
Unión no tuvo un buen comienzo en los octavos de final de la Copa Sudamericana. El Tatengue perdió por 1 a 0 contra Bahía en el estadio Arena Fonte Nova tras un penal convertido por Gilberto Oliveira Souza a los 34 minutos del segundo tiempo. Ahora deberá revertir el resultado el martes 1 de diciembre a las 19:15 en el 15 de Abril.

En los primeros minutos del partido ambos equipos jugaron de igual a igual y tuvieron chances claras de convertir. Pero Sebastian Moyano ahogó el grito de gol de Bahía y Juan Manuel García estrelló un cabezazo en el travesaño, aunque exigió al arquero Douglas. Por eso los equipos se fueron al descanso con el marcador igualado en cero.

En el complemento fue más Unión, mostró mucho más juego, manejó los tiempos y acechó un largo rato a Bahía. Pero cuando el partido estaba llegando a su fin, Moyano barrió a un jugador del conjunto local y el árbitro no dudó en cobrar penal. El encargado de patearlo y cambiarlo por gol fue Gilberto Oliveira Souza.

Pese al resultado, Unión definirá la historia en su cancha y deberá marcar por lo menos un gol para forzar los penales. La llave aún está abierta y por el nivel futbolístico que mostró Bahía, en el Tatengue son más que optimistas.