---
category: Agenda Ciudadana
date: 2021-10-20T06:15:49-03:00
thumbnail: https://assets.3dnoticias.com.ar/AMSAFE.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe.
resumen: Este miércoles los docentes inician el paro de 48 horas y se movilizan al
  Ministerio de Educación
title: Este miércoles los docentes inician el paro de 48 horas y se movilizan al Ministerio
  de Educación
entradilla: La concentración será a las 10.30. Desde el gobierno sostienen que no
  habrá otra oferta salarial. Amsafé insiste en que "la solución al conflicto no es
  con amenazas"

---
Con la ratificación del paro de 48 horas para esta semana, los docentes públicos nucleados por Amsafé se movilizarán al ministerio de Educación durante el próximo miércoles. El conflicto sigue sumando días sin resolución, teniendo en cuenta que desde el gobierno provincial el mismo gobernador Omar Perotti aseguró que no habrá otra oferta salarial a la ya efectuada a los gremios, la cual Amsafé rechazó.

El sindicato, que viene de realizar un paro de 24 horas la semana pasada, exige que el gobierno mejora la propuesta de incremento salarial del 17% a abonar en tres tramos: una del 10% a partir de diciembre 2021, un 5% en diciembre y a partir de enero del 2022 una última suba del 2%.

Rodrigo Alonso, secretario General de Amsafé La Capital en diálogo con UNO ya había recalcado que el gobierno "se está equivocando", y pidió un nuevo llamado a paritarias. "Hay que buscar una solución a este conflicto; que el conflicto tiene solución y es la convocatoria a paritarias para discutir la mejora de la propuesta en términos integrales; no solamente en términos salariales, sino también con respecto a las condiciones de trabajo".

El gobierno de Santa Fe instó a los docentes públicos a que dejen en suspenso el paro de 48 horas que realizarán esta semana para que la administración provincial pueda liquidar sus salarios con el 17 por ciento de aumento, un porcentaje que los maestros rechazan. Lo hizo a través de la ministra de Educación, Adriana Cantero, quien ratificó que la gestión de Omar Perotti no hará otra oferta salarial a los docentes y que si estos no aceptan ese 17 por ciento, los maestros no lo van a cobrar.

La ministra de Educación provincial Adriana Cantero ya declaró su apoyo a la decisión de Perotti: "No habrá otra oferta laboral. Ya lo explicó el gobernador. Esta era la mejor propuesta y además fue trabajada con todos los trabajadores públicos", dijo enfática la titular de la cartera educativa.

La funcionaria se refirió así al aumento propuesto y aceptado por los trabajadores estatales de ATE y UPC, y a los docentes privados nucleados en Sadop. Los únicos sectores que no aceptaron la oferta salarial del gobierno fueron los docentes públicos y los profesionales de la salud

Frente a la inminencia de un nuevo paro, Cantero también insistió en hacer un llamado a los docentes. "Pedimos que muestren voluntad de diálogo, que por ahora no parece que haya. Y que sea antes del miércoles", disparó la ministra. Y añadió: "Sería bueno que los docentes tuvieran algún gesto". Busca, así, evitar la repetición de la medida de fuerza que dejó sin clases a los alumnos de las escuelas públicas.

Entre las condiciones que desde Amsafé presentan disconformidad luego de la oferta paritaria que deslizó el gobierno se encuentran: aumento de salario, condiciones dignas para enseñar y aprender, solución de problemas con Iapos y el blanqueo de todas las