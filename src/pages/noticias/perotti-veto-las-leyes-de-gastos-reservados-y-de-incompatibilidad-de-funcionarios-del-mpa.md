---
category: Agenda Ciudadana
date: 2020-12-12T12:39:53Z
thumbnail: https://assets.3dnoticias.com.ar/perotti20.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Perotti vetó las leyes de gastos reservados y de incompatibilidad de funcionarios
  del MPA
title: Perotti vetó las leyes de gastos reservados y de incompatibilidad de funcionarios
  del MPA
entradilla: Se trata las leyes de rendición de cuentas de gastos reservados y la modificación
  del régimen de incompatibilidades de los funcionarios del MPA

---
A través del decreto 1.786, el titular del Ejecutivo provincial vetó totalmente la ley 14.013, que regulaba la rendición de cuentas de gastos reservados o especiales, destinados a solventar gastos necesarios para la realización de investigaciones criminales complejas. 

Asimismo, a través del decreto 1.787, hizo lo propio con la ley 14.016, que modificaba el régimen de incompatibilidades de los funcionarios del Ministerio Público de la Acusación (MPA), del Servicio Público Provincial de la Defensa Penal y del Organismo de Investigaciones.

En cuanto a la rendición de cuentas de gastos reservados, el primer decreto considera que ya cuentan “con procedimientos administrativos y legislativos a esos fines”, y advierte que “los plazos establecidos no están relacionados con los plazos procesales que rigen las investigaciones penales”.

También, señala que “el Poder Ejecutivo considera que no es oportuno ni conveniente, en el marco de la situación pública existente que ha derivado en investigaciones en trámite en sede judicial, consagrar modificaciones legislativas cuando las mismas permitirían revelar públicamente -transcurrido apenas un año-, el destino de fondos e incluso la identidad de personas que hubiera, con su intervención o testimonio, favorecido el esclarecimiento de causas judiciales”.

En tanto, sobre la incompatibilidad, el decreto 1.787 indica que “se establece una cesantía automática por el mero transcurso del tiempo, omitiendo de esa manera considerar que la garantía en la estabilidad de los funcionarios públicos cesa por razones disciplinarias o cuando se pierde su idoneidad física o intelectual; no cuando, por ejemplo, por razones de salud, tiene derecho a una licencia mayor al plazo establecido” y que lo propuesto “podría producir afectaciones a derechos adquiridos o perjudicar situaciones jurídicas consolidadas bajo la vigencia de las leyes cuya modificación se propone, con la consecuente inconstitucionalidad de la norma”.