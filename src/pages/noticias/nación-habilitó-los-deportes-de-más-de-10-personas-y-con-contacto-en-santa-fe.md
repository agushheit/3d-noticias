---
layout: Noticia con imagen
author: .
resumen: Habilitan deportes en Santa Fe
category: Agenda Ciudadana
title: Nación habilitó los deportes de más de 10 personas y con contacto en Santa Fe
entradilla: Se deberá garantizar la organización de turnos, si correspondiere, y
  los modos de trabajo que garanticen las medidas de distanciamiento e higiene
  necesarias para disminuir el riesgo de contagio de Covid-19.
date: 2020-11-13T17:15:49.103Z
thumbnail: https://assets.3dnoticias.com.ar/habilitan-deporte.jpg
---
A través de la Decisión Administrativa 2053/2020, publicada este viernes en el Boletín Oficial, el gobierno Nacional habilitó el desarrollo de deportes grupales de más de diez (10) personas o donde los participantes interactúen y tengan contacto físico entre sí.

Al respecto, se especifica que el "gobierno provincial debe dictar las reglamentaciones necesarias para el desarrollo de las actividades referidas, pudiendo los Gobernadores implementarlas gradualmente, suspenderlas o reanudarlas, en el marco de sus competencias territoriales, en virtud de las recomendaciones de la autoridad sanitaria local, y conforme a la situación epidemiológica y sanitaria".

Luego se agrega que "en todos los casos se deberá garantizar la organización de turnos, si correspondiere, y los modos de trabajo que garanticen las medidas de distanciamiento e higiene necesarias para disminuir el riesgo de contagio de COVID-19".

**Resolución:** [BOLETÍN OFICIAL ](https://www.boletinoficial.gob.ar/detalleAviso/primera/237262/20201113)

**Protocolo presentado por Santa Fe:** [Protocolo](https://assets.3dnoticias.com.ar/Protocolo.pdf)