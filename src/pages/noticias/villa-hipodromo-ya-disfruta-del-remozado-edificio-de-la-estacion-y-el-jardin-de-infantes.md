---
category: La Ciudad
date: 2021-10-29T06:00:27-03:00
thumbnail: https://assets.3dnoticias.com.ar/VILLAHIPODROMO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Villa Hipódromo ya disfruta del remozado edificio de la Estación y el Jardín
  de Infantes
title: Villa Hipódromo ya disfruta del remozado edificio de la Estación y el Jardín
  de Infantes
entradilla: 'Se inauguró este jueves la Vereda para Jugar y se dejaron habilitadas
  las tareas de reacondicionamiento e intervenciones estéticas tanto en el edificio
  del Jardín de Infantes como en la Estación Villa Hipódromo. '

---
El intendente Emilio Jatón recorrió este jueves los remozados Jardín Municipal y Estación de Villa Hipódromo, que fueron puestos a nuevo a partir de los trabajos de mantenimiento generales, intervenciones estéticas, un sector que se montó para realizar los talleres de peluquería, y la nueva Vereda para Jugar, que es un proyecto educativo y cultural de la Municipalidad, que busca inscribir el juego en el territorio.

En la ocasión, el intendente se dirigió a los presentes y destacó los trabajos realizados y la calidad del servicio educativo que se ofrece en el barrio: “Quedó en condiciones como para que los padres traigan a niñas y niños para que convivan en el Jardín, tengan sus espacios lúdicos, y puedan desarrollar esa necesidad de estar con el otro y crecer intelectualmente”.

Además expresó que “este logro forma parte de lo que venimos diciendo y construyendo con ustedes porque lo que hacemos es escucharlos: ustedes son los que vienen aquí, dan clases, traen a sus hijos. Esto es de ustedes. Esto es público”.

En la actividad estuvieron presentes las niñas y niños que asisten al Jardín Municipal y a la Estación, sus familias, vecinas y vecinos, docentes, directivos, integrantes de los equipos de las Secretarías de Educación y Cultura, y de Integración Social y Economía Social. Además de dejar inauguradas la vereda y las obras realizadas, se entregaron reconocimientos a quienes intervinieron en la elaboración de las baldosas de la Vereda para Jugar, así como a representantes de diversas instituciones que integran la Red del barrio.

La subsecretaria de Gestión Cultural y Educativa, Huaira Basaber, expresó su alegría respecto de que “estos sean espacios de participación de calidad”. Destacó el trabajo realizado “con el corazón de todo el equipo”, y sostuvo que “esto habla de que queremos una ciudad integrada, elegida y para todos por igual”.

Por su parte, Ayelén Dutruel, secretaria de Integración y Economía Social sostuvo: “Emilio nos viene impulsando en el trabajo mancomunado entre la Estación y el Jardín, entre todos estos espacios que son públicos, que son de nosotros”, y agregó que: “Esta frontera, que es la puerta que separa al edificio de la Estación del espacio del Jardín de Infantes, se va abriendo y vamos pasando y disfrutando juntos”.

A Zaida Torres, directora zonal de Jardines Municipales, la une una historia especial con Villa Hipódromo, barrio en el que nació y fue docente en el Jardín. Durante la inauguración expresó que venían viendo la necesidad de que las niñas y los niños pudieran contar con un espacio en la vereda donde pudieran jugar: “Ellos tomaban como lugar de juego a la vereda y de ahí salió esta idea”, sostuvo. Sobre las obras realizadas dentro del Jardín afirmó que “las salas quedaron habitables, en un espacio donde se puede aprender, donde las seños van a poder trabajar con nuevas materialidades”. Finalmente propuso: “Ahora, a volar la imaginación”.

La recorrida fue propicia para cantarle el cumpleaños feliz a Santiago, uno de los niños que asisten a las actividades de la Estación Villa Hipódromo, donde se dictan talleres de panadería, de cocina para niños y niñas y próximamente de peluquería; también hay una juegoteca, un taller tiktokero, y se hacen caminatas saludables, fútbol mixto y funcional.

En la actividad también estaban presentes el secretario de Educación y Cultura, Paulo Ricci; y la secretaria de Políticas de Cuidado y Acción Social, María Victoria Rey.

**Veredas para jugar**

La iniciativa, que surge en el marco del Plan Integrar, comenzó con la primera Vereda para Jugar, en el Barrio Loyola Sur, con el objetivo de hacer uso del espacio público en un contexto de pandemia, donde no estaba permitido permanecer en ámbitos cerrados, pero sí se habilitaban los espacios recreativos. Se trata de una política educativa, cultural y de integración social de los espacios, las infancias y los barrios.

El diseño de las veredas tienen un desarrollo visual y estético en la composición de sus formas, colores y poética espacial. Cada propuesta varía en las materialidades: papel, madera, cartón, barro, democratizando procesos de construcción y diseño. Dentro de las propuestas, las rayuelas lingüísticas y visuales promueven la imaginación y la participación.

Con financiamiento de FAE, las Secretarías de Integración Social y de Educación y Cultura de la Municipalidad de Santa Fe, en diálogo con la Delegación Regional Educación IV, desarrollaron un taller de esmaltado de cerámica en el Jardín Municipal de San Lorenzo. Las baldosas producidas en el marco de esa actividad forman parte de la vereda que se inauguró hoy. De este modo, se unen dos puntos distintos de la ciudad y eso, sostiene Basaber, “nos hace más colaborativos, solidarios, nos permite pensarnos como parte de una red de participación”.