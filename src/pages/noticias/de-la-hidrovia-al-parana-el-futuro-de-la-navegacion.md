---
category: El Campo
date: 2021-06-22T07:47:05-03:00
thumbnail: https://assets.3dnoticias.com.ar/hidrovia-parana-paraguay.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Agroclave
resumen: 'De la hidrovía al Paraná: el futuro de la navegación'
title: 'De la hidrovía al Paraná: el futuro de la navegación'
entradilla: En un panel organizado por la Facultad de Agronomía de la UBA (Fauba),
  expertos debatieron sobre este tema estratégico para el país

---
La navegación fluvial es y siempre ha sido una cuestión de soberanía nacional. En los últimos meses, el debate en torno a la concesión de uno de los tramos críticos del río Paraná, que forma parte de la denominada Hidrovía Paraguay-Paraná, tomó relevancia ya que su vencimiento abre una oportunidad única para que la Argentina recupere el control de la navegación de una de sus principales salidas al océano Atlántico. En este contexto, el debate gira en torno a una gran pregunta: ¿Se debe volver a privatizar el tramo en cuestión o se lo debe recuperar para su gestión y control? El Estado argentino todavía no dio una respuesta concreta, por lo cual, el destino de la gestión del río Paraná es una moneda que está en el aire.

En este contexto, el Grupo de Estudio y Trabajo en Políticas Agropecuarias (GET-PA) de la Facultad de Agronomía de la UBA (Fauba) organizó un encuentro virtual el 28 de abril para debatir sobre el futuro de la navegación del Paraná, que a su vez es central para la integración de todo el territorio argentino. En el debate participaron Mempo Giardinelli, periodista y escritor, Jorge Taiana, actual senador nacional y exministro de Relaciones Exteriores en el período 2005 a 2010, y Gustavo Idígoras, Presidente de la Cámara Argentina de la Industria Aceitera y Centro de Exportadores de Cereales (Ciara-CEC). El encuentro tuvo la participación de Patricia Lombardo, docente de la Fauba y coordinadora del GET-PA.

Según Patricia Lombardo, en el marco de las actividades de discusión y reflexión que organiza el GET-PA, se convocó a este debate con el propósito de “instalar en el ámbito de nuestra facultad la discusión sobre el futuro del control y la administración de un punto estratégico en el ejercicio de la soberanía nacional”.

En 1993 se licitó un tramo del río Paraná, que va desde el puerto de Santa Fe hasta el canal Punta Indio, en el Río de la Plata. Este tramo concesionado es la principal vía de entrada y salida del comercio exterior de nuestro país: por ella se transportan alrededor del 80% de las exportaciones de granos de la Argentina y su gestión representa un negocio de u$s 300 millones anuales. La concesión se otorgó al consorcio Hidrovía SA, conformado por la empresa belga Jan de Nul y el holding argentino Emepa SA, e involucra su mantenimiento, dragado y señalización para garantizar la navegabilidad. La empresa comenzó a operar el 1 de mayo de 1995, para lo cual recibió un subsidio estatal de u$s 40 millones anuales durante 8 años. El 30 de abril de 2021 finalizó la concesión, tras 25 años de estar bajo el control de Hidrovía S.A.

El 26 de noviembre de 2020, ante la proximidad del vencimiento, el Poder Ejecutivo delegó en el Ministerio de Transporte la facultad para realizar un nuevo llamado a licitación del tramo y para su adjudicación, mediante el decreto 949. Este decreto encendió las alarmas Actualmente, el gobierno nacional decidió prorrogar el vencimiento por 90 días y, con esto, la incertidumbre continúa.

El debate comenzó con la intervención de Mempo Giardinelli, que hizo énfasis en el origen de la palabra ‘hidrovía’ y la forma en que el lenguaje contribuye a perpetuar una lógica neoliberal que reduce al río a un único rol de vía de transporte de mercaderías. “Acá el protagonista es el río. Para los once millones de argentinos y argentinas que vivimos a la orilla del Paraná, la palabra ‘hidrovía’ no significa nada. El Paraná no es algo folclórico, sin valor: varias generaciones nos criamos con el orgullo de que fuera nuestro río”.

Para Giardinelli, detrás de esta resignificación, hay un trasfondo concreto: “Nosotros no tenemos una misión romántica, esto es también una cuestión práctica. A lo largo de estos 25 años, la Afip no cobró dinero. No se pagaron impuestos y ninguna provincia recibió un centavo”. Añadió que a esto se suma el hecho de que no existen los estudios de impacto ambiental que determinen las profundidades de dragado que deben respetarse en cada tramo del río. El escritor también enfatizó el problema de la falta de controles sobre las cargas que se transportan. Actualmente, los buques que transitan el tramo concesionado sólo deben presentar el inventario de sus cargas a través de una declaración jurada. Para Giardinelli, la mejor opción es no volver a privatizar y crear un organismo estatal integrado por el gobierno nacional y las provincias ribereñas.

Gustavo Idígoras, por su parte, trajo una posición contraria a la estatización: “El Paraná hoy necesita más obras y mayor inversión. Eso implica, por ejemplo, mayor profundidad de dragado, llegar a los 40 pies para que puedan entrar barcos de mayor calado. Esto no beneficia sólo a las empresas exportadoras, beneficia a toda la cadena de valor. Hoy un productor que está en Salta no puede levantar su cosecha porque el Paraná tiene poco calado. Si logramos un mayor calado, ese productor en Salta podrá invertir, hacerse económicamente viable y generar un efecto social en toda la comunidad donde vive, porque empezaría a generar más tecnología, más empleo y más industrialización”.

Dijo que el sector privado está en condiciones de hacer las inversiones que requiere el río. Mencionó el hecho de que, en estos 25 años de concesión, el Paraná pasó de transportar 20 millones de toneladas de granos a transportar 180 millones. “Además, hoy vendemos alrededor de 100 millones de toneladas de todo el complejo industrial —maíz, trigo, soja, girasol, aceite de soja y de girasol, harina, biodiesel, glicerina, entre otros— a 100 mercados del mundo”. Añadió también que el sector privado invirtió 15.000 millones de dólares en infraestructura portuaria en las últimas dos décadas, lo que permitió a nuestro país competir en el mercado global.

> Idígoras considera que el sector privado debe hacerse cargo de la administración del río. “Promovemos una nueva concesión”, concluyó.

El 20 de noviembre de 1845, cuando la Argentina era todavía una Confederación, ocurrió una de las batallas más memorables de nuestra historia. En un contexto de conflicto interno y de surgimiento del Estado nacional, Inglaterra y Francia presionaban a Juan Manuel de Rosas, gobernador de Buenos Aires, para que permitiera la libre navegación de los ríos, que les permitiría a su vez comerciar directamente con las provincias. En una relación de fuerzas completamente desigual, las tropas argentinas combatieron a los invasores en el río Paraná y si bien no lograron contener su avance, evitaron su desembarco en el territorio argentino. El Senador Jorge Taiana comenzó su intervención recordando este episodio que no casualmente se conmemora como el Día de la Soberanía Nacional.

> "El sector privado debe hacerse cargo de la administración del río, dijo Idígoras (Ciara-CEC)

Con este marco histórico, Taiana enfatizó la presencia estratégica que el río Paraná siempre tuvo en la Argentina. En este sentido, la discusión que tiene lugar en este momento es decisiva: “El fin de la concesión es una oportunidad que nos da el argumento y el timing para reflexionar qué queremos hacer con el río, con la integración de la Argentina fluvial y marítima y con las comunicaciones en los próximos 25 o 30 años. Y la oportunidad es ahora”.

También habló de un tema medular: la situación del canal de Magdalena. Actualmente, la única vía de acceso al Paraná a través del Río de la Plata es el canal de Punta Indio, al sur de Montevideo. Esto significa que las embarcaciones que vienen de los puertos argentinos del sur tienen que pasar obligatoriamente por el canal Punta Indio, que es uruguayo, para acceder a la cuenca del Plata.

“Este embudo tiende a fortalecer el puerto de Montevideo y el de Nueva Palmira”, añade Taiana También significa una división entre la Argentina fluvial y la Argentina marítima. Por esto, el senador consideró que el Estado nacional no debe tomar decisiones sólo en función de los números de la economía, sino pensar en el desarrollo de una estrategia integral de cara a los próximos 30 años diseñada en base a conexiones que generen un sistema bioceánico.

"Es la oportunidad para reflexionar sobre qué hacer con el río y con la integración de la Argentina fluvial y marítima

“Creo que es hay que dar por finalizado el contrato con Hidrovía SA”, concluyó Taiana. “Esta es una decisión estratégica respecto del futuro del país que abre otros debates. No estamos hablando solo del río Paraná: estamos hablando de las perspectivas de todos los puertos, los privados, los consorcios, los puertos provinciales, y las estrategias que vamos a diseñar para ellos”.

El debate que abre el fin de esta concesión sigue abierto y las defensoras y los defensores de la estatización del Paraná todavía creen que es posible concretar ese sueño. Para Patricia Lombardo, “la proximidad del vencimiento del contrato es una oportunidad única para discutir sobre el desarrollo de esta vía de comercio regional e internacional, en el marco de un modelo productivo nacional. Esta discusión debe incluir la participación de las provincias y de los diversos actores sociales involucrados”.

Lombardo añadió que “el futuro del río Paraná precisa de un proyecto atravesado por una mirada estratégica”. La oportunidad empieza por devolverle el nombre al río Paraná.