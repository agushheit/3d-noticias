---
category: Agenda Ciudadana
date: 2021-09-18T06:15:07-03:00
thumbnail: https://assets.3dnoticias.com.ar/GABINETE.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Alberto Fernández relanza su gestión con un nuevo Gabinete
title: Alberto Fernández relanza su gestión con un nuevo Gabinete
entradilla: Luego del resultado de las PASO el Presidente relanza su gobierno con
  cambios en el Gabinete y una convocatoria a los gobernadores.

---
El presidente Alberto Fernández oficializó la noche del viernes una serie de cambios en el Gabinete de Gobierno como cierre de una jornada signada por múltiples reuniones y por la renuncia "indeclinable" a su cargo presentada por el secretario de Comunicación, Juan Pablo Biondi, la primera dimisión en medio de las tensiones que en el Frente de Todos (FdT), luego del resultado de las PASO del último domingo.

Los anuncios fueron realizados por la noche, instantes después de que el mandatario se retirara de la Casa Rosada rumbo a la Residencia de Olivos, para mañana viajar rumbo a la provincia de La Rioja, donde tiene previsto reunirse con gobernadores peronistas.

Alberto Fernández nombre como nuevojefe de Gabinete, el actual gobernador tucumano Juan Manzur. como nuevo ministro de Seguridad, Aníbal Fernández; el de Ganadería, Agricultura y Pesca, Julián Domínguez; el de Relaciones Exteriores y Culto y saliente jefe de Gabinete, Santiago Cafiero; el nuevo ministro de Educación, Jaime Perczyck; el de Ciencia y Tecnología, Daniel Filmus: y el secretario de Comunicación y Prensa, Juan Ross. Todos ellos jurarán en sus cargos el próximo lunes a las 16.

Más temprano, la primera dimisión indeclinable del equipo de Gobierno fue la de Juan Pablo Biondi: "Motiva esta decisión la crisis desatada en las últimas horas y espero que mi alejamiento del cargo contribuya a pacificar, en parte, estos momentos difíciles que nos toca vivir", indicó en su carta de renuncia.

En tanto, el Presidente encabezará mañana en La Rioja de un "almuerzo de trabajo" con gobernadores peronistas en el que se analizará el relanzamiento del Gabinete, informaron fuentes oficiales.

Del encuentro formarán parte al menos ocho mandatarios provinciales, entre ellos el anfitrión Ricardo Quintela, quien visitó hoy al jefe de Estado en la Casa Rosada.

El jueves, en una misiva pública, la vicepresidenta Cristina Fernández había cuestionado la figura del funcionario, quien en su renuncia rechazó esas acusaciones.

"Me ofenden y lamento las malas interpretaciones que hiciera sobre mí la señora vicepresidenta, al considerarla una líder indiscutible del espacio político que representa ella junto con usted", expresó Biondi en el texto de su renuncia "indeclinable" al cargo que ejerciera desde el 10 de diciembre de 2019.

En su publicación del jueves, Fernández de Kichner había criticado las "operaciones" de prensa "permanentes" que "sólo terminan desgastando al Gobierno".

También reseñó la Vicepresidenta en esa carta que, tras las PASO, había solicitado cambios en el gabinete e incluso indicó que había sugerido al gobernador de Tucumán, Juan Manzur, como jefe de Gabinete.

El ministro del Interior, Eduardo "Wado" De Pero, fue quien el miércoles inició la serie de ofrecimientos de salida del gabinete por parte de distintos colaboradores, lo que motivó las intervenciones públicas del Presidente y la Vicepresidenta.

A De Pedro se habían sumado luego los ministros Martín Soria (Justicia) Carlos Salvarezza (Ciencia), Tristán Bauer (Cultura) y Juan Cabandie (Ambiente), además de las titulares del PAMI y Anses, Luana Volnovich y Fernanda Raverta, respectivamente; la secretaria de Comercio Interior, Paula Español; el presidente del ACUMAR, Martín Sabbatella, y la directora del INADI, Victoria Donda.

"La gestión de Gobierno seguirá desarrollándose del modo que yo estime conveniente", había expresado el jueves el mandatario a través de un hilo de Twitter, que fue respondido horas después por la carta de Fernández de Kirchner.

alejamiento de Biondi se conoció después de un nuevo día de reuniones que el Presidente mantuvo, primero, en la Residencia de Olivos y, luego, en la Casa Rosada, como parte de las discusiones en la coalición oficialista tras los resultados de las PASO.

Fernández arribó a Casa Rosada pasado el mediodía, acompañado por los secretarios de la Presidencia, Julio Vitobello; y de Asuntos Estratégicos, Gustavo Beliz, y luego de haber participado desde Olivos, en forma virtual, del Foro de las Principales Economías sobre Energía y Clima.

Por su parte, la vicepresidenta mantuvo por la mañana reuniones en su despacho del Senado con el diputado Eduardo Valdés y, posteriormente, con De Pedro.

"Le agradezco la confianza que me brindara al asignarme funciones tan distinguidas en su equipo de Gobierno y las ideas que impulsaron este proyecto político que triunfó en 2019", expresó Biondi en la carta de su renuncia.

El funcionario saliente manifestó sus deseos de que "esta nueva etapa que comienza sea superadora de la precedente" y consideró un "honor" haber acompañado al mandatario en la gestión de la pandemia.

Por su parte, el presidente de la Cámara de Diputados, Sergio Massa, estuvo en el Congreso para la resolución de las citaciones por el Presupuesto 2022 que fue presentado el miércoles pasado por el ministro de Economía, Martín Guzmán; y luego se dirigió hacia el palacio de Hacienda, según indicaron fuentes de su entorno.

Previo a su llegada a la Casa Rosada, Fernández había participado junto a líderes globales del Foro de las Principales Economías sobre Energía y Clima (MEF, por sus siglas en inglés), una reunión convocada por el presidente de Estados Unidos, Joe Biden, para impulsar acciones ante la crisis del cambio climático.

La discusión en el FdT se produce luego de que en las PASO la alianza opositora Juntos por el Cambio (JxC) lograra avanzar posiciones y relegara al oficialismo en la mayoría de los distritos, con epicentro en la provincia de Buenos Aires, que concentra casi el 40 por ciento del padrón electoral.