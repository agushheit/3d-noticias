---
category: Estado Real
date: 2021-04-11T06:00:18-03:00
thumbnail: https://assets.3dnoticias.com.ar/AGUAS.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Aguas Santafesinas seguirá capacitando a su personal en el abordaje constructivo
  de conflictos
title: Aguas Santafesinas seguirá capacitando a su personal en el abordaje constructivo
  de conflictos
entradilla: La empresa rubricó un convenio de cooperación con la Secretaría de Justicia
  de la provincia para la continuidad del Curso de Abordaje Constructivo de Conflictos.

---
Aguas Santafesinas S.A rubricó un convenio de cooperación con la Secretaría de Justicia de la provincia para la continuidad del Curso de Abordaje Constructivo de Conflictos.

La firma del convenio de renovación estuvo a cargo del presidente de Aguas, Hugo Morzan; el secretario provincial de Justicia, Gabriel Somaglia; el subsecretario de Acceso a la Justicia, Franco Stampone, y el director del Centro de Asistencia Judicial, Claudio Ainbinder.

Del acto también participaron, la vicepresidenta de Aguas, Marisa Gallina, y Juan Manuel Costantini, integrante del directorio de la empresa. Además, estuvo presente personal gerencial de diferentes áreas y los asistentes al primer ciclo iniciado el año pasado. Al finalizar el mismo se entregaron los certificados a los participantes.

El acuerdo dispone que la Subsecretaría de Acceso a la Justicia brinde a los empleados de la empresa capacitación en resolución de conflictos interpersonales.

La fructífera experiencia iniciada el año pasado con esta propuesta estuvo destinada, en esa primera instancia en modalidad virtual, a los asesores comerciales de las diferentes localidades donde Aguas presta servicios.

Este año la nueva edición del curso estará destinada a los mandos medios de la empresa.

Durante el acto de firma, Hugo Morzan recordó que “cuando hablábamos de realizar esta capacitación no pensábamos siquiera que estaríamos en esta condición que transitamos durante este tiempo. La situación de pandemia le dio características particulares a este curso, pero también, de alguna manera, nos puso frente a un nuevo desafía en el abordaje de conflictos”.

Por lo mencionado sostuvo que “fue más oportuna que nunca esta posibilidad. Un agradecimiento a todos los que tomaron este desafío de capacitación dentro de la empresa, a los capacitadores que estuvieron siempre disponibles en esta modalidad virtual y presencial durante poco tiempo, pero que naturalmente le brindaron al personal de la empresa una capacitación muy importante”.

Morzan expresó su reconocimiento “muchísimas gracias a todos por la disposición, la colaboración, me parece que esta es una forma importante de mostrar cómo desde la gestión provincial podemos articular cosas desde el Estado en beneficio de los ciudadanos, en este caso los usuarios de Aguas Santafesinas”.

Por su parte, Somaglia expresó que “es una satisfacción muy grande para nosotros acercarnos a transmitir algunas de las funciones que la Secretaría de Justicia tiene a su cargo que son las diferentes alternativas de resolver cuestiones interpersonales para que las situaciones no se vuelvan conflictivas”.

En ese sentido indicó que “resolver cuestiones interpersonales de modo pacífico es un modo de repensar la sociedad; los vínculos con quienes trabajamos en idénticos ámbitos laborales y las funciones que nosotros tenemos que brindar hacia los terceros. La Secretaría de Justicia tiene como función, más allá de vincularnos con el Poder Judicial, un gran sector más amplio que son los servicios a terceros a través del Registro Civil, el Registro de la Propiedad, la Inspección de Personas Jurídicas, el Centro de Acceso a la Justicia y la Subsecretaría de Acceso a la Justicia”.

Somaglia concluyó que “es un poco pensar que puede facilitar el Estado para que la cultura de nuestras conductas individuales no sea solamente el conflicto y demandar Justicia a través del Poder Judicial. Creemos que estas son formas de poder transmitirles y de poder capacitar sobre lo que tiene que ver mucho con el público en general y con la ciudadanía. Por lo tanto, para nosotros es una enorme satisfacción que podamos hacer esto que tiene un fuerte impacto desde el Estado. El Estado debe estar presente y debe generar hechos de esta naturaleza”.