---
category: Estado Real
date: 2022-01-06T06:00:50-03:00
thumbnail: https://assets.3dnoticias.com.ar/perottifmi.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Perotti: "El acuerdo con el FMI debe tener como primer objetivo proteger
  a la producción y al trabajo argentino"'
title: 'Perotti: "El acuerdo con el FMI debe tener como primer objetivo proteger a
  la producción y al trabajo argentino"'
entradilla: "El gobernador de Santa Fe participa de la reunión convocada por el presidente
  de la Nación, Alberto Fernández y el ministro de Economía, Martín Guzmán.\n\n"

---
El gobernador de Santa Fe, Omar Perotti, participó este miércoles de la reunión convocada por el presidente de la Nación, Alberto Fernández, y el ministro de Economía, Martín Guzmán, “a fin de informarnos como van las negociaciones con el Fondo Monetario Internacional por el pago de la deuda”, explicó el mandatario provincial.

Al respecto, precisó que la convocatoria fue extensiva a “todos los gobernadores” y destacó: “Todos los actores tenemos la responsabilidad de asistir y aportar a la solución que nos permita cumplir, cuidando del presente y el futuro de nuestro país, de su gente, de su producción y de su empleo”.

“Ante el endeudamiento y la crisis que llegó después, la gente espera que haya una construcción de la política a su favor que genere condiciones de crecimiento y no de incertidumbre”, manifestó Perotti, al tiempo que resaltó que “las diferencias son bienvenidas pero no la ventaja política, eso no suma nada. Un país no puede estar sin presupuesto y sin un acuerdo que de previsibilidad y lleve tranquilidad a trabajadores y empresarios, por temas partidarios a dos años de la próxima elección”.

“El acuerdo con el FMI tiene como primer objetivo proteger a la producción y al trabajo argentino- sumó el gobernador. Esta es la condición para que sigamos creciendo y se pueda pagar la deuda. La fórmula es simple: generar riquezas para poder pagar, dejando atrás ajustes que empobrecen y atacan al sector productivo y tecnológico”.

En ese sentido, Perotti manifestó que “proteger y defender al que produce y trabaja es el pilar sobre el cual debe buscarse la forma que nos permita pagar la deuda”.

“Los argentinos venimos atravesando la crisis provocada por el Covid-19, con consecuencias muy duras sobre la economía. Ahora que arrancamos no podemos detenernos, no podemos parar todo y pagar a costa de detener el crecimiento, dejar de crear empleos y nuevamente darle un golpe a las PyMES y a la producción nacional”, se explayó el titular de la Casa Gris.

Al respecto, el mandatario santafesino ejemplificó: “En Rosario se generaron 65 mil puestos de trabajo en el último trimestre; estamos con una tasa de desempleo menor al 2019, cuando nos tocó asumir. Esto es fruto del esfuerzo de todos y debemos entender que la negociación con el FMI debe garantizar que no se haga un ajuste feroz que paguen trabajadores y emprendedores; y que se protege la producción del campo, de la industria y la innovación de nuestro país”.

Además, Perotti señaló que “la inseguridad se combate con más policías, y también con más inversiones que generen oportunidades de empleo, de emprender un negocio o de ampliar el que ya se tiene”.

“El acuerdo con el Fondo debe ser sobre la base de proteger al que produce e invierte, porque justamente ellos dinamizan la economía, generando crecimiento, lo que nos permitirá continuar cumpliendo con los compromisos asumidos”, destacó el goberandor.

Por último, Perotti manifestó: “Mi compromiso con los santafesinos es que Santa Fe sea el corazón productivo y de la innovación del país, ese es mi norte y voy a apoyar todas las decisiones que estén de acuerdo con este objetivo”, concluyó.