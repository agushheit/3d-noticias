---
category: Estado Real
date: 2020-12-22T11:04:24Z
thumbnail: https://assets.3dnoticias.com.ar/2212somaglia.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Asistencia financiera de la Nación para talleres socioeducativos en penales
  juveniles
title: Asistencia financiera de la Nación para talleres socioeducativos en penales
  juveniles
entradilla: Se trata de prácticas institucionales destinadas a promover modelos para
  el vínculo de convivencia desde la perspectiva centrada en la intervención socioeducativa.

---
El gobierno de la provincia, a través de su secretario de Justicia, Gabriel Somaglia, celebró un **convenio con la Secretaria Nacional de Niñez, Adolescencia y Familia**, por el cual el organismo nacional **subsidiará con recursos económicos los «Talleres Socioeducativos» del área Penal Juvenil de Santa Fe**. 

Dichos talleres tienen por objeto fortalecer las prácticas institucionales destinadas a promover modelos para el vínculo de convivencia, desde la perspectiva centrada en la intervención socioeducativa.

«Es sumamente importante construir puentes con el gobierno Nacional a través de convenios que fortalecen la relación y demuestran la confianza en la gestión de recursos y las políticas que se implementarán a través del mismo», resaltó Somaglia.

El convenio asiste de manera financiera al área de Justicia Penal Juvenil para la realización de los talleres que permitirán la continuidad de la política de inclusión a los jóvenes en la sociedad.

Se brindará formación en el área educativa, creación de bibliotecas, cine, recreación, deportes y computación, entre otros, y permitirá adquirir equipamiento para implementarlos.

***