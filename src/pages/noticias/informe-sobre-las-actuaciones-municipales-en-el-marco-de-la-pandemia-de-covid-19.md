---
layout: Noticia con imagen
author: .
resumen: Prevención del Coronavirus
category: La Ciudad
title: "Informe sobre las actuaciones municipales en el marco de la pandemia de
  COVID-19 "
entradilla: La Municipalidad de Santa Fe informa las acciones que se llevan
  adelante para la prevención de la circulación del coronavirus en la ciudad y
  los servicios que garantiza a la población.
date: 2020-11-16T13:42:11.435Z
thumbnail: https://assets.3dnoticias.com.ar/garrafas.jpg
---
La Municipalidad informa las acciones que lleva adelante, en el marco de la pandemia de coronavirus, y los servicios que se sostienen por ser considerados esenciales y no encuadrarse en el aislamiento preventivo dispuesto por el Gobierno nacional.

## **Atención de personas en situación de calle**

Desde el inicio del aislamiento social, la Municipalidad articula acciones con distintas organizaciones sociales que trabajan con esa población para proveerle elementos de higiene y alimentos. Además, están identificadas todas las personas en situación de calle, se les está haciendo un seguimiento y se les brinda asistencia.

## **Controles en la Terminal de Ómnibus**

La Municipalidad, a través del Cobem y otras áreas municipales, comenzó el 13 de marzo el relevamiento de todas las personas que ingresan a la ciudad a través de la terminal de colectivos. Allí se verifica si tienen síntomas asociados al coronavirus, se los registra y se les indican las medidas de prevención que deben tener.

Desde que comenzaron las medidas preventivas de aislamiento, se registraron 956 santafesinos que regresaron del exterior, 635 de ellos oriundos de la capital provincial. Durante la última semana no se registraron ingresos de santafesinos provenientes del exterior.



## **Control y convivencia**

La Municipalidad realizó un amplio operativo para verificar el efectivo cumplimiento de las normas y generar instancias de concientización entre la población, de acuerdo a las disposiciones provinciales y nacionales vigentes. Durante toda la semana, más de 60 agentes municipales acompañaron a quienes se desplazaron por parques, plazas y paseos, insistiendo con las medidas de prevención que deben mantenerse y desalentando reuniones en esos espacios. Estas actuaciones se completaron en conjunto con agentes de la dirección municipal de Deportes y de la Policía de la provincia.

También se efectuaron **controles en los complejos donde se realizan actividades físicas y deportivas**, en función de las normativas gubernamentales, tal es el caso de gimnasios, fútbol 5 y paddle, entre otros.

En las ferias de Las 4 Vías, La Verdecita, la ex Estación Mitre, la Costanera Oeste y el CIC de Facundo Zuviría se constituyeron puntos fijos de información y se controló el cumplimiento de las disposiciones para el funcionamiento. De estos operativos participaron agentes de GSI, inspectores de Control y funcionarios de la Secretaría de Control. Del mismo modo, en la peatonal San Martín y en las avenidas comerciales General Paz, Facundo Zuviría y Aristóbulo del Valle, se controló el cumplimiento de horarios de apertura y cierre del comercio minorista y mayorista, de acuerdo a lo establecido por Decreto.

**La Guardia de Seguridad Institucional (GSI) cuenta con 230 agentes municipales que atienden puntos fijos, concretan sus recorridos de rutina y completan tareas de difusión entre los vecinos y las vecinas de la ciudad.** Los mismos recorren el ejido urbano verificando y promoviendo el cumplimiento del distanciamiento social. En ese sentido, y ante la violación de las normas establecidas, el rol de los agentes es el de disuadir de esta actitud de manera preventiva o dar intervención a la policía y al gobierno provincial, que son las autoridades a las que les compete el control y la sanción.

Tal como se viene haciendo desde marzo, la línea de Atención Ciudadana del municipio recibe denuncias de los habitantes de la capital en cuanto a ruidos molestos y reuniones sociales que no están permitidas. Ante situaciones de este tipo, se recepcionan los reclamos y se cursa la atención correspondiente. Durante la última semana se registraron 29 actuaciones de este tipo a través del Centro de Monitoreo.

Además, **los inspectores de tránsito llevaron adelante en diferentes puntos de la ciudad, operativos sanitarios y de circulación, conjuntamente con Policía de la provincia, personal de la Agencia Provincial de Seguridad Vial y agentes sanitarios del Cobem, de los que también participa la GSI.** Esta semana se concretaron en doble turno frente a Cilsa, en la Ruta N° 168 a la altura de la Fuente de la Cordialidad, en Iturraspe y avenida Perón, en la avenida Aristóbulo del Valle y en el kilómetro 0,5 de la Ruta 1. Otros puntos de control estuvieron situados en la rotonda de Almirante Brown y Javier de la Rosa, a la altura del Faro en Costanera Oeste, en avenida Salvador del Carril y en la plaza Pueyrredón, todos ellos en tres turnos.

[](<>)En los mismos se verifica la procedencia y el destino de los vehículos, y la portación tanto de la autorización para circular como de la documentación pertinente. También se realiza un examen de temperatura corporal y de olfato. Por último, se controla la utilización del barbijo y se brindan las recomendaciones de higiene y seguridad. Durante la última semana se retuvieron 34 motovehículos y 15 automóviles. También se verifican las unidades del transporte público de pasajeros.

**En bares y restaurantes, la Municipalidad concretó recorridas para verificar el cumplimiento de los protocolos y las limitaciones horarias.** En esos operativos participaron inspectores de Control, de controles especiales (Brigada de Sonoridad y Brigada de Expendio), de Producción, agentes de COBEM y la GSI, adicionales de policía y funcionarios de la Secretaría de Control y Convivencia Ciudadana y de la Secretaría de Producción. Se inspeccionó el respeto por la capacidad fijada para el interior de cada local, que no puede ser superior al 30%; la cantidad de comensales por mesa, que no puede superar las cuatro personas; el registro de cada comensal en una planilla, consignando datos personales; la presencia del veedor o encargado del local, que monitorea el cumplimiento de los protocolos internos; y el distanciamiento entre las mesas colocadas al aire libre. También se aborda a empleados y clientes a quienes, luego de pedir su consentimiento, se les realiza test de anosmia y medición de temperatura.

Se inspeccionaron distintos establecimientos y se atendieron denuncias recepcionadas por sistema. **En total se labraron 14 actas de infracción y 3 cédulas de notificación.** En cuanto a controles voluntarios de temperatura corporal y test de olfato entre clientes y personal de bares y restoranes, se concretaron más de 50, en conjunto con el área de Juventudes del municipio.



## **Transporte público de pasajeros**

Las líneas de colectivos siguen funcionando con la frecuencia reducida y cumplen periódicamente con las tareas de desinfección indicadas por la Municipalidad.

En cuanto a taxis y remises, continúa la indicación de no llevar a más de 2 pasajeros, en el asiento de atrás y con las ventanillas bajas.

En todos los casos se recuerda a la población que sólo está permitida la circulación para aquellas personas que cumplen tareas esenciales.



## **Recolección de residuos**

La Municipalidad sigue garantizando el servicio de recolección de residuos en días y horarios habituales. De todas maneras, ese servicio se limita específicamente a los residuos domiciliarios. No deben sacarse residuos de poda, escombros ni otros voluminosos.

Además, se solicita a la comunidad que los residuos domiciliarios se saquen sólo en bolsa cerrada, se rocíen con alcohol al 70% o lavandina diluida y no se coloquen en el piso. Las personas que estén cumpliendo la cuarentena o presenten síntomas compatibles con coronavirus, deben utilizar doble bolsa.

Se recuerda que, como medida sanitaria y para preservar a los trabajadores de la planta de separación de residuos, todos los desechos serán enterrados en el relleno sanitario ya que no se puede verificar que no estén contaminados.

## **Garrafas a precio diferencial**

La venta de garrafas de gas a precio diferencial que realiza la Municipalidad junto a las empresas en distintos puntos de la ciudad, continúa con el siguiente cronograma:

* **Martes de 11:30 a 12:30 horas:**

– Frente a “El Alero” (bulevar French, entre Necochea y Sarmiento)

– Plaza de la Paz (Alto Verde)

– Ruta 1, kilómetro 2.7 intersección de Calle Eucaliptus (frente a UPCN)

– Plazoleta Isabel La Católica (Blas Parera y Fray Cayetano Rodríguez)

– Jardín Botánico (avenida José Gorriti al 3900)

* **Jueves de 11:30 a 12:30 horas:**

– Vecinal las Delicias (Alfonsina Storni 3100)

– Solar Municipal de Varadero Sarsotti

– Escuela N° 869 Julio Argentino Roca, Ruta 1 kilómetro 4

– Distrito Norte: Vecinal Juana Azurduy (Matheu 6250)

– Centro Integrador Comunitario de Facundo Zuviría al 8000

**Se recuerda que el valor único de referencia es $ 420 y para adquirirlas, se debe llevar el envase vacío de 10 kilos.** Por cuestiones de seguridad, los envases deben ser nacionales y habilitados.

## **Violencia de género**

La Municipalidad de Santa Fe sigue garantizando la guardia telefónica las 24 horas, todos los días, a través del 0800-777-5000 para la denuncia y el asesoramiento ante situaciones de violencia de género.

## **Guardias mínimas**

Se recuerda a la población que, con el objetivo de preservar la salud del personal municipal y evitar la circulación de personas, se trabaja con guardias mínimas en tareas que no son esenciales en este contexto. Por lo tanto, se completan tareas de bacheo, alumbrado público y desmalezado con equipos reducidos.

## **Cementerio Municipal**

**Las visitas están autorizadas de lunes a sábados, de 14 a 17 horas, y los domingos de 9 a 12 horas.** En tanto, la Municipalidad de Santa Fe elaboró un nuevo protocolo para reabrir salas de velatorio, de modo que se puedan concretar los cortejos fúnebres. Los horarios establecidos son de 7 a 18, los participantes se deberán agrupar en dos turnos y no podrán ser de más de 10 personas por cada servicio fúnebre. Los velatorios tendrán una duración máxima de 3 horas.

## **Atención ciudadana**

Se recuerda a la población que, ante síntomas como fiebre y dolor de garganta o dificultad para respirar, hay que llamar al número dispuesto por el gobierno provincial 0800-555-6549.

Para denunciar violación de la cuarentena y dar intervención a las fuerzas de seguridad provincial, el número habilitado es 0800-555-6768.

En tanto, para comunicarse con la Municipalidad, las vías de contacto son el 0800-777-5000, la aplicación Santa Fe Ciudad Móvil, el correo electrónico informes@santafeciudad.gov.ar y el chat web de la página www.santafeciudad.gov.ar.