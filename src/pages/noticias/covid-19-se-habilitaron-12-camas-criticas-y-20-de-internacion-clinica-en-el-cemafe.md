---
category: Agenda Ciudadana
date: 2021-04-23T07:40:53-03:00
thumbnail: https://assets.3dnoticias.com.ar/covid.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: 'Covid 19: Se habilitaron 12 camas críticas y 20 de internación clínica en
  el Cemafe'
title: 'Covid 19: Se habilitaron 12 camas críticas y 20 de internación clínica en
  el Cemafe'
entradilla: El Centro de Especialidades Médicas Ambulatorias de Santa Fe se convirtió
  hoy en “un pulmón en la red de prestaciones sanitarias”, graficó el secretario de
  Salud, Jorge Prieto.

---
Este jueves el Secretario de Salud, Jorge Prieto, junto a Fabián Mendoza y Sebastián Calvet, autoridades del CEMAFE, presentaron la puesta en funcionamiento del sistema que permite descomprimir la alta demanda de camas que se presenta en los Hospitales JM. Cullen y JB. Iturraspe.

La estrategia fue planificada el año pasado pero sin necesidad de desarrollarla hasta este momento, en el cual se habilitan 8 camas de terapia intensiva, 4 más terapia intermedia y 20 camas de internación clínica para pacientes no Covid, mientras se continúa brindando la atención habitual.

Cabe destacar que en la mayoría de los hospitales la capacidad de atención se incrementó considerablemente frente a la pandemia, llegando a un 700% en Rafaela y 400% en Venado Tuerto y otro tanto en el Viejo Iturraspe de la ciudad capital.

“Hoy se amplía el sistema sanitario, esta es una estrategia que planificamos ya el año pasado pero, por el número de casos, no requirió la apertura”, explicó el secretario de Salud.

Y agregó: “La provincia de Santa Fe no escapa a la realidad del país, ni el mundo, está ola impacta mucho en lo que hace la transmisibilidad, es decir, cuando hay más circulación hay más transmisibilidad y al aumentar aumenta también la internación por casos severos”.

Además, Prieto puntualizó: “Ante este escenario se realizó la puesta en funcionamiento un sector destinado a camas críticas y de internación generales en el CEMAFE. Este lugar es un pulmón en la red de contención de prestaciones sanitarias de los grandes efectores de tercer nivel, hospitales Cullen y Iturraspe. Ambos reciben también a ciudadanos y ciudadanas que son derivadas de otras localidades hacia la ciudad de Santa Fe”.

Esfuerzo e ingeniería para no interrumpir la atención

El director del CEMAFE, Fabián Mendoza destacó y agradeció “al staff del efector que tomó este desafío y se pudo concretar con la articulación realizada junto al Ministerio de Salud, y la coordinación de los hospitales, jefes de servicio, de terapia”.

Por su parte, Sebastián Calvet explicó que continúa la atención: “lo que hicimos es una reingenieria de acuerdo a la demanda de las especialidades más requeridas y gracias, a la colaboración del equipo de salud, podemos mantener la atención que venimos desarrollando e iremos modificando o reestructurándola semana a semana para evaluar la situación”.

**Estrategia e incorporación de camas**

Para el secretario de Salud “en esta segunda ola hay un impacto en personas menores de 65 años y una rapidez en la evolución clínica que nos ocupa y preocupa”, por lo cual también agradeció al personal de salud. “Sabemos que esta ola va a tener un gran impacto y por eso también agradecemos a todo el personal porque están redoblando esfuerzos”, finalizó.

**Ampliación de la red de atención**

Durante los últimos meses se han incorporado camas a la red de los hospitales que se suman a las habilitadas en el Cemafe y se distribuyen de la siguiente manera:

* Hospital Iturraspe nuevo en un 100%
* Hospital Cullen en un 100%
* Hospital Iturraspe Viejo, 400% y 4 camas críticas
* Hospital de Reconquista 100%
* Hospital de Rafaela:700%
* Hospital Centenario: 120% y 6 camas críticas
* Hospital Provincial: 100%
* Hospital Gamen: 100%
* Hospital de Villa Constitución 100%
* Hospital de Firmat 100%
* Hospital en Casilda 100%
* Hospital de Cañada de Gómez 100%
* Hospital en Armstrong 100%
* Hospital de Venado Tuerto 400%
* Hospital Eva Perón 100% y una incorporación de 7 camas críticas y 20 generales.