---
category: Estado Real
date: 2021-06-29T09:26:47-03:00
thumbnail: https://assets.3dnoticias.com.ar/arena.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: 'Ley Micaela: El gobierno de Santa Fe firmó un convenio con la Procuración
  General de la Corte para su implementación'
title: 'Ley Micaela: El gobierno de Santa Fe firmó un convenio con la Procuración
  General de la Corte para su implementación'
entradilla: La firma se realizó entre el Ministerio de Igualdad, Género y Diversidad
  y la Procuración la Corte Suprema de Justicia de Santa Fe.

---
El Gobierno de Santa Fe, a través del Ministerio de Igualdad, Género y Diversidad, suscribió un acuerdo de colaboración recíproca con la Procuración General de la Corte Suprema de Justicia de Santa Fe para la implementación de la Ley Micaela, referida a la capacitación obligatoria en perspectiva de género, con enfoque de derechos humanos. Los cursos darán comienzo después de la feria judicial y culminarán antes de fin de año.

“Es una alegría para este flamante Ministerio firmar el primer convenio como Ministerio por Ley Micaela con el Procurador de la Corte, Jorge Barraguirre, entendemos desde este Ministerio, desde el Poder Ejecutivo, que es fundamental trabajar transversalmente con los otros poderes, también estamos dando una respuesta institucional y política a una demanda histórica que tiene que ver con la capacitación en perspectiva de género”, sostuvo la subsecretaria de Mujeres, Género y Diversidad, Florencia Marinaro.

“La firma de este convenio significa que todos los equipos técnicos del Ministerio van a formular y hacer el curso para los trabajadores y trabajadoras de la Procuración, funcionarios/as, fiscales de Cámara, defensores, y toda la estructura administrativa que tiene la procuración de la Corte, inaugurando así una etapa de trabajo con clases y evaluaciones. En este año y medio de gestión se fue implementando la Ley Micaela por etapas, se comenzó con el Poder Ejecutivo, Municipios y Comunas, Organizaciones, y hoy se da con el Poder Judicial, es un trabajo conjunto entre los dos poderes”, agregó Marinaro.

Por parte del Poder Judicial, el procurador General de la Corte Suprema, Jorge Barraguirre, expresó: “El poder judicial viene trabajando desde hace mucho tiempo a través de su Centro de Capacitación Judicial, frente al nuevo panorama legislativo entendemos que también tenemos que buscar algunas especificidades o algunos entrenamientos mucho más específicos por parte de quienes la propia ley indica que son los organismos a cargo de estas capacitaciones, por eso nuestra satisfacción en este día y dejar la capacitación en manos del Ministerio”.

**SOBRE EL CONVENIO**

El Acuerdo de Colaboración, firmado el 28 de junio, tiene por objetivo “establecer lazos de mutua colaboración para llevar adelante las actividades tendientes a la implementación de la ley de Capacitación Obligatoria en Género para todas las personas que Integran los Tres Poderes del Estado, conocida como Ley Micaela en el ámbito de la Procuración General de la Corte Suprema de Justicia de Santa Fe”. En este sentido, y para tal fin diseñarán de manera conjunta un plan de implementación que, teniendo como base los contenidos diseñados por el Ministerio de Igualdad, Género y Diversidad en su carácter de autoridad de aplicación de la Ley Provincial Nº 13.891.

El programa de capacitación en perspectiva de género, con enfoque de derechos humanos incluirá a todo su personal, tanto administrativo como a los fiscales de las Cámaras de Apelación; los y las defensores y defensoras generales de las Cámaras de Apelación; los y las fiscales; los y las defensores generales; los asesores y fiscales de menores.

El Ministerio de Igualdad, Género y Diversidad es el organismo de aplicación a nivel local. El espíritu de la normativa contribuye a promover una sociedad más justa e igualitaria, libre de violencias y discriminaciones.