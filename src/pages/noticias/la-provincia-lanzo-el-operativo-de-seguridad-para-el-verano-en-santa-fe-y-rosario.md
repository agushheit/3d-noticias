---
category: Estado Real
date: 2021-12-18T18:00:25-03:00
thumbnail: https://assets.3dnoticias.com.ar/operativobikini.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La provincia lanzó el operativo de seguridad para el verano en Santa Fe y
  Rosario
title: La provincia lanzó el operativo de seguridad para el verano en Santa Fe y Rosario
entradilla: Se sumarán más agentes a patrullar los sectores de playas, parques y paseos.

---
El ministro de Seguridad, Jorge Lagna, encabezó el lanzamiento del Operativo de Seguridad para el Verano en el balneario La Florida de la ciudad de Rosario.

En ese marco, el funcionario provincial destacó estos operativos que año a año se realizan junto al municipio de Rosario, e indicó que “este año tendrá otra particularidad, ya que con la ‘pospandemia’ hay una explosión del turismo local, y Rosario no va a ser ajena al turismo de los rosarinos, de la gente que vive en las adyacencias y también de otros lugares. Esperamos una temporada plena, con muchísima afluencia de público, por eso vamos a reforzar aún más lo que veníamos haciendo año tras año”, remarcó Lagna.

“Vamos a disponer de más agentes, con patrulleros, con motos, en un trabajo combinado con la municipalidad, con todas sus áreas de control, y con el 107. Queremos que la gente se divierta, disfrute, porque hay una gran necesidad de ocupar los espacios públicos, los lugares de recreación, así que ojalá podamos llevar adelante esta hermosa temporada que se avecina con mucho disfrute y pocas incidencias. Por eso vamos a poner mucha atención en estos controles, para que las familias disfruten de la belleza de Rosario. En eso estaremos, con el compromiso asumido del gobernador Perotti y el intendente Javkin para que Rosario siga creciendo y quienes vengan la sigan disfrutando”, finalizó el ministro.

Estuvieron presentes, junto al titular de la cartera de Seguridad, el subjefe de la Unidad Regional II de la Policía de Santa Fe, Marcelo Mendoza; y la secretaria de Control y Convivencia de la Municipalidad de Rosario, Carolina Labayru.

**OPERATIVO DE VERANO EN SANTA FE**

En la costanera santafesina, este viernes también se lanzó el Operativo de Seguridad para el Verano, con la presencia del coordinador provincial de Seguridad Social, Facundo Bustos, y el Jefe de la Unidad Regional I de Policía, Martín García.

“El Operativo Verano consiste en prestar la seguridad preventiva en todos los balnearios de la Unidad Regional I. En total son ocho balnearios que están habilitados, junto a las zonas aledañas a la ciudad de Santa Fe como Rincón, Sauce Viejo y Santo Tomé”, describió Bustos, y agregó: “Contamos con la apoyatura de las direcciones de la Policía de la provincia de Santa Fe, más los recursos de la Unidad Regional I, y tenemos binomios de caminantes, bicicletas y caballería, y los demás móviles, con motos y patrulleros, que están haciendo el precinto de seguridad ciudadana que tenemos dentro del éjido de la URI”, finalizó.