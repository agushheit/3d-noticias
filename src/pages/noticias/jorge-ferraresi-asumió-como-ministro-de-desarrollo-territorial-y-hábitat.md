---
layout: Noticia con imagen
author: "Fuente: Telam"
resumen: Asumió Jorge Ferraresi
category: Agenda Ciudadana
title: Jorge Ferraresi asumió como ministro de Desarrollo Territorial y Hábitat
entradilla: El Presidente le tomó juramento al nuevo funcionario este jueves
  mediante una ceremonia en el Museo de la Casa Rosada.
date: 2020-11-20T11:32:32.718Z
thumbnail: https://assets.3dnoticias.com.ar/ferraresi.jpeg
---
El presidente Alberto Fernández tomó juramento a Jorge Ferraresi, como nuevo ministro de Desarrollo Territorial y Hábitat, en reemplazo de María Eugenia Bielsa quien renunció la semana pasada.

La jura del nuevo funcionario se realizó este jueves mediante una ceremonia en el Museo de la Casa Rosada.

Ferraresi, de 59 años, es un ingeniero recibido en la Universidad Tecnológica Nacional y desde 2009 hasta ahora se desempeñó como intendente del municipio bonaerense de Avellaneda, ocupando con anterioridad otros cargos a nivel nacional y provincial.

![](https://assets.3dnoticias.com.ar/tweet.jpg)

Ver en Twitter: *[https://twitter.com/CasaRosada](https://twitter.com/CasaRosada/status/1329543600653430785?s=20)*