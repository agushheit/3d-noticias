---
category: Agenda Ciudadana
date: 2021-07-17T06:00:01-03:00
thumbnail: https://assets.3dnoticias.com.ar/PENES.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: La Nación destinó más de $ 13 millones para comprar 10.000 penes de madera
  pulida
title: La Nación destinó más de $ 13 millones para comprar 10.000 penes de madera
  pulida
entradilla: La licitación, que se realizó a través del Ministerio de Salud, también
  incluye dispensers de preservativos.

---
El 30 de junio, la cartera que conduce Carla Vizzotti publicó en el portar Comprar la requisitoria de elementos para promoción sobre cuidados de salud sexual; también incluye dispensers de preservativos; la explicación del Ministerio

El 24 de junio, Sandra Marcela Tirado, Secretaria de Acceso a la Salud de la Nación y que reemplazó en ese cargo a la actual ministra de Salud, Carla Vizzotti, dio luz verde a un pedido de la Dirección de respuesta al VIH, ITS, Hepatitis virales y Tuberculosis para la compra de materiales de promoción de salud sexual que incluyen la compra de 10.000 penes de madera pulida. El monto asignado por la cartera sanitaria a los materiales, que incluyen también dispensers de preservativos y maletines de color turquesa, asciende a $ 13.371.100.

A través de la resolución 35 de 2021, Tirado indicó en los considerandos que la adquisición de “los insumos solicitados permitirán asegurar una amplia disponibilidad de materiales de promoción cuya finalidad sea concientizar y evitar la propagación de enfermedades de transmisión sexual tales como el VIH y otras ITS en el marco de la Ley N° 23.798″.

Y luego resolvió autorizar “la convocatoria de la Licitación Pública N° 80-0023-LPU21, para la adquisición de materiales de promoción, compuestos por dispensers de preservativos, penes de madera y maletines, solicitada por la Dirección de respuesta al VIH, ITS, Hepatitis virales y Tuberculosis, destinados a la población general y a profesionales de la salud, a distribuirse en los Centros de Atención Primaria de la Salud (CAPS), Regiones Sanitarias, Programas Provinciales, Municipales y otros establecimientos de todo el país”.

Siete días después, el 1° de julio, se publicó la licitación en la plataforma Comprar, donde se conoció el “pliego de bases y condiciones” que debía cumplir aquel proveedor que quisiera participar de la compulsa pública, que finalizó el martes pasado y de la que participaron cuatro empresas.

En el documento, de 22 páginas, se detallan las características de los “tres renglones” (ítems) que conforman el pedido:

**RENGLÓN N° 1**

Dispenser de Preservativos. 10.000 unidades, con las siguientes características:

· Materiales: poliestireno alto impacto blanco de 1,5 mm de espesor, termo formado.

· Medidas: 330 mm de alto x 165 mm de ancho x 90 mm de profundidad.

· Diseño: en el frente se deberá incluir impreso a 4 colores un mensaje institucional.

· Terminación: de apoyo y con perforaciones para colgado.

· Embalaje: caja conteniendo 4 tolvas de dispensado de preservativos.

RENGLÓN N° 2

Penes de Madera. 10.000 unidades, con las siguientes características:

· Material: madera semidura.

· Medidas: alto 170 mm, diámetro del cilindro 40 mm, diámetro base 50 mm.

· Terminación: pulido.

· Embalaje: caja conteniendo 100 penes de madera

RENGLÓN N° 3

Maletines. 10.000 unidades, con las siguientes características:

· Material: polipropileno natural.

· Medidas: 40 cm x 30 cm x 10 cm.

· Diseño: tela color turquesa con ribetes; cinta y manija de mano en color turquesa, de polipropileno de 50 mm de ancho y largo regulable. Estampa impresa a 4 colores en el frente. Cierre color turquesa oscuro símil tela del maletín o turquesa símil ribetes y cintas.

· Embalaje: caja conteniendo 50 maletines.

La inusual convocatoria, en medio de la pandemia del coronavirus Covid-19, rápidamente se volvió viral en redes sociales, en donde se generó un debate en torno a si el contexto era el adecuado para avanzar con la iniciativa de salud sexual.

Algunas de las ofertas ofrecidas estuvieron por abajo y otras por arriba de los $ 13.371.100. De los cuatro oferentes, solo uno cotizó por los tres renglones por un monto total de $ 14.870.000. Mientras que los otros tres lo hicieron por uno o dos de los tres ítems.

**Tres ofertas para los penes: dos de origen extranjero y uno nacional**

Al analizar el detalle de las tres ofertas para la venta de los 10.000 penes de madera, dos de los oferentes indicaron que son importados y uno que es de producción nacional. En los tres casos, respetan el detalle de las medidas: alto 170 mm, diámetro del cilindro 40 mm, diámetro base 50 mm.

Solo uno de los oferentes detalla la madera en la que se confeccionarían las 5000 unidades: paraíso o grevillea.

En tanto, las ofertas van desde $ 498 por unidad, hasta los $ 1430 y en el medio se sitúa la de “producción nacional”, con $ 882,37.

**La explicación del Ministerio de salud**

Tras la repercusión del llamado, desde el Ministerio de Salud respondieron a través de un comunicado que dice: “Atendiendo a estudios preliminares que dan cuenta de un deterioro de los indicadores relacionados con las enfermedades de transmisión sexual durante la pandemia, el Ministerio de Salud de la Nación llama a una licitación pública (0023/2021) para la adquisición, en el marco de la implementación de la Educación Sexual Integral (ESI) y de las estrategias sanitarias para garantizar su cumplimiento, de kits educativos con materiales para la realización de capacitaciones”.

“Los kits educativos para la promoción y prevención, contienen preservativos, dispenser para la accesibilidad de los preservativos y maletines donde se transportan los elementos para las capacitaciones, entre los que se cuentan penes de madera, gel lubricante y materiales de comunicación”, detallaron, y agregaron: “Los materiales están destinados a los equipos que trabajan en terreno, ya sea en instituciones de salud, educación o en el ámbito comunitario”.

Según el parte enviado a este medio: “Ninguna provincia compra este tipo de insumo. La distribución mensual planificada es de 800 kits. El material se distribuye a través de los programas provinciales. Entre los oferentes para la adquisición de los kits, figura una oferta máxima por 14 millones y otra oferta menor por 4 millones. En el caso de adquirirlos, el Ministerio de Salud optará por la oferta menor”.

“La cantidad a adquirir -que se consolidó a partir de las distribuciones realizadas en años anteriores- se debe a que se distribuyen en alrededor de 5200 efectores de salud y a otros equipos gubernamentales y organizaciones que trabajan la ESI”, sigue la respuesta, y agrega: “La cantidad a comprar además tiene relación con la posibilidad de llevar la política preventiva a la mayor cantidad de lugares posibles colocando estos recursos cerca de las personas, incluyendo organizaciones de la sociedad civil y otros organismos gubernamentales en la respuesta preventiva al VIH y otras ITS”.

Según la cartera sanitaria: “En estudios preliminares se observa un descenso en el uso del preservativo durante la pandemia, por tal motivo, es necesario planificar políticas de intervención con medidas educativas, de promoción y asistencia post-pandemia. Además, en nuestro país más del 98% de las infecciones se producen a partir de relaciones sexuales sin protección”.

“La situación actual de nuestro país en relación con la transmisión de enfermedades de transmisión sexual, como por ejemplo sífilis, manifiesta un aumento de los diagnósticos, especialmente en los casos en adolescentes y jóvenes. Todo esto determina la necesidad de políticas de prevención en Salud Sexual, sostenida y de impacto nacional”, continúa el texto, y cierra: “En relación al uso de preservativos, para que el uso sea adecuado, se realizan capacitaciones y educación sanitaria a jóvenes y adolescentes. Es por esto que se necesita material para que, mediante la Educación Sexual Integral, se transmita el uso adecuado”.