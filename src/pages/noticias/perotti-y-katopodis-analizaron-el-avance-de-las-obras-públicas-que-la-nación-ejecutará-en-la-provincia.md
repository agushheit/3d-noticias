---
layout: Noticia con imagen
author: .
resumen: Obras públicas en la provincia
category: Estado Real
title: Perotti y Katopodis analizaron el avance de las obras públicas que la
  Nación ejecutará en la Provincia
entradilla: El gobernador y el ministro de Obras Públicas de Nación supervisaron
  el plan de tareas que comenzará a implementarse en 20 días.
date: 2020-11-10T14:18:32.922Z
thumbnail: https://assets.3dnoticias.com.ar/obras-publicas.jpg
---
El mandatario provincial mantuvo un encuentro este lunes,  en Buenos Aires, con el titular del área nacional de Obras Públicas. Tras el mismo, Perotti afirmó que junto al ministro Katopodis, “avanzamos en el plan de ejecución de las obras públicas de la Nación en la provincia de Santa Fe, en el marco del convenio que firmamos a principio de año y revisamos todas las presentaciones del plan Argentina Hace”.

En esa línea, el titular de la Casa Gris aseguró que “en los próximos 20 a 30 días, en la mayoría de los municipios donde los proyectos están aprobados, se van a hacer los desembolsos, es decir, vamos a tener las obras en ejecución con la importancia de lo que eso genera, en la infraestructura de cada uno de nuestros pueblos y ciudades, y también en la ocupación que la construcción genera y que tanto necesitamos”, indicó Perotti.

En el plan de obras que el gobierno nacional impulsa para Santa Fe se trasluce un dato concreto: la Nación duplicará lo que viene invirtiendo en territorio santafesino en el próximo presupuesto. El ministro Katopodis enfatizó esta apreciación al señalar que “la inversión federal en obras públicas en la provincia de Santa Fe, durante 2021, será más del doble que en el actual período, y pasará de 5.057 millones de pesos a 12.142 millones”, concluyó.