---
layout: Noticia con imagen
author: Nota de la Redacción de 3DNoticias
resumen: "RUAGA: nueva modalidad"
category: Agenda Ciudadana
title: El Ruaga presentó una nueva modalidad para detección de familias adoptivas
entradilla: Se trata de una nueva estrategia de búsqueda de aspirantes para los
  niños, niñas y adolescentes que siguen a la espera de una familia.
date: 2020-11-22T14:04:06.621Z
thumbnail: https://assets.3dnoticias.com.ar/ruaga.jpeg
---
**El Registro Único de Aspirantes a Guarda con fines Adoptivos de la Provincia de Santa Fe (Ruaga) lanzó este jueves una nueva modalidad para detección de familias.** Es una nueva estrategia de búsqueda de aspirantes para los niños, niñas y adolescentes que siguen a la espera de una familia adoptiva, luego de agotadas las búsquedas dentro del Sistema Provincial y en la Red Federal de Registros.

**La búsqueda está dirigida a todos los aspirantes inscriptos y admitidos en el Ruaga**, cualquier sea su disponibilidad adoptiva. Se los invita a considerar una convocatoria particular sin que implique inicialmente una modificación de su proyecto adoptivo registrado.

El Ruaga enviará un correo electrónico desde la casilla  [ruagaconvoca@santafe.gov.ar](ruagaconvoca@santafe.gov.ar), con una breve descripción de la situación de la niña, niño o adolescente. Los aspirantes podrán voluntariamente solicitar ser considerados y evaluados para el caso, respondiendo por la misma vía dentro de los diez días hábiles.

**En este momento son 14 casos en espera de una familia**, entre ellos grupos de hermanos, algunos en edad adolescente. “Están deseosos de formar una familia”, afirmó la directora del RUAGA, Magdalena Galli Fiant.

Por otro lado, agregó que hay casos de dos y cuatro hermanos, como así también de chicas y chicos que tienen alguna discapacidad o enfermedad de tratamiento. **“Todos ellos esperan una familia”**, finalizó Galli Fiant.