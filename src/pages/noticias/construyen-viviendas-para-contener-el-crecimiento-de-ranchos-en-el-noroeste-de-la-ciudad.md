---
category: La Ciudad
date: 2021-07-04T06:00:44-03:00
thumbnail: https://assets.3dnoticias.com.ar/NOMASRANCHOS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Construyen viviendas para contener el crecimiento de ranchos en el noroeste
  de la ciudad
title: Construyen viviendas para contener el crecimiento de ranchos en el noroeste
  de la ciudad
entradilla: En la segunda etapa del programa No Más Ranchos en Santa Fe se están construyendo
  63 casas en barrio San Agustín II. Aseguran que en Santa Fe se instalan unos 350
  ranchos por año.

---
Esta semana se inició la construcción de 63 viviendas en barrio San Agustín II, en el noroeste de la ciudad, para erradicar los ranchos que estaban asentados allí. Se da en el marco de la segunda etapa del programa No Más Ranchos en Santa Fe, llevado a cabo por el movimiento Los Sin Techo en conjunto con el gobierno provincial.

En esta etapa se firmará un segundo convenio para construir otras 200 viviendas más en barrio San Agustín II, con el primer tramo ya iniciado con la construcción de 63 casas que serán el hogar de las familias asentadas en ranchos del lugar. El pasado martes se comenzaron a construir 30 viviendas, detrás de la escuela Santa Mónica y en uno de los lugares con mayor pobreza de la ciudad, con un plazo de obra estimado en un mes.

José Luis Zalazar, responsable de Hábitat y Desarrollo Comunitario del movimiento Los Sin Techo, manifestó: "Si bien esto ya forma parte del segundo convenio que deberíamos firmar, ya comenzamos con todo. Un día frío, pero con un calor, una esperanza y un horizonte distinto para 30 familias que van a vivir en una casa digna dentro de un mes si todo acompaña".

La primera etapa del programa significó la construcción de 200 viviendas, con 120 casas construidas en barrio La Loma y 80 en barrio Jesuitas. Esta etapa está "prácticamente concluida" según el responsable del movimiento. Frente a esto, Zalazar destacó: "Estamos terminando en barrio La Loma, estimamos que en julio ya estarán terminadas las casas".

**Por año se instalan unos 350 ranchos en Santa Fe**

Con respecto a la multiplicación de ranchos en los barrios humildes de la ciudad, el responsable de Hábitat de los Sin Techo subrayó: "Hay que tener en cuenta que el crecimiento poblacional se censó a la fecha del 15 de septiembre del 2020. Nosotros con seguridad tenemos censados 63 ranchos en San Agustín II, pero notamos que hay un 15% más de ranchos que se han sumado".

"Estamos reduciendo la cantidad, pero sobre todo estamos conteniendo el crecimiento. No hay que olvidarse que en Santa Fe se instalan 350 ranchos anuales, un promedio de casi un rancho por día. Ojalá podamos terminar con la erradicación de los ranchos, ese es el objetivo a corto plazo y por eso estamos trabajando con las familias y con el gobierno provincial en lo que hace a los materiales para las casas", afirmó Zalazar.

Los últimos indicadores arrojan que en la actualidad hay 10.000 familias que son indigentes en la ciudad, mientras que en el Gran Santa Fe son 12.100 en total. Estas personas son las que sienten en carne propia las carencias de no tener forma alguna de calefacción convencional, indicando Zalazar frente a esto que "se hicieron campañas juntando frazadas, conseguimos donaciones, pero realmente no sabemos cómo se calefaccionan. Solamente unos mártires pueden vivir en cuatro chapas de cartón".

Para concluir, el referente de Los Sin Techo remarcó: "Lo más importante es que se podrá transformar en realidad el sueño del padre Atilio Rosso, que fue que la gente viva dignamente y que no haya más ranchos en Santa Fe. Ese es el legado y la herencia de amor al prójimo que recibimos, lo que es una cuestión espiritual y realista".