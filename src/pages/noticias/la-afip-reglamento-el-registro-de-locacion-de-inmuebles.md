---
category: Agenda Ciudadana
date: 2021-02-18T06:32:02-03:00
thumbnail: https://assets.3dnoticias.com.ar/afip1.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: La AFIP reglamentó el registro de locación de inmuebles
title: La AFIP reglamentó el registro de locación de inmuebles
entradilla: Los contribuyentes que asuman el carácter de locadores en los contratos
  quedan obligados a la registración de la operación, de lo contrario serán sancionados.

---
La Administración Federal de Ingresos Públicos (AFIP) reglamentó hoy el sistema de registración de contratos de locación de inmuebles, que permitirá "obtener información relevante para verificar el cumplimiento de las obligaciones fiscales".

El organismo indicó que la Resolución General 4933 se publicará este jueves en el Boletín Oficial, con lo que "aquellos contribuyentes que asuman el carácter de locadores o arrendadores en los contratos quedan obligados a la registración de la operación".

La resolución establece sanciones para quienes incumplan y no presenten la información requerida dentro del régimen de registración de contratos de locación de inmuebles.

Todos los contratos alcanzados deberán ser informados en forma digital, al ingresar con clave fiscal en afip.gob.ar al servicio "Registro de Locaciones de Inmuebles-Reli- Contribuyente". 

Así, se deberán proporcionar datos vinculados con su rol de locador, la identificación del inmueble y el resto de los intervinientes, al tiempo que se tiene que adjuntar el contrato. 

"Si bien no son sujetos obligados, la normativa contempla la posibilidad de que los intermediarios (corredores, inmobiliarias y escribanos) registren un contrato en representación de sus clientes", señaló.

En un comunicado, la AFIP puntualizó que el régimen también prevé que los sujetos que asuman el carácter de locatarios o arrendatarios podrán informar en forma voluntaria un contrato y subrayó que el Régimen de Registración de contratos de locación de inmuebles entra en vigencia a partir del primero de marzo.

"Los contratos celebrados a partir del 1° de julio de 2020 y que continúen vigentes, así como aquellos concretados durante marzo de 2021, gozarán de un plazo excepcional para su registración hasta el 15 de abril de 2021, inclusive", remarcó.

Según la normativa, los contratos alcanzados son: locaciones de bienes inmuebles urbanos, arrendamientos sobre bienes inmuebles rurales, locaciones temporarias de inmuebles –urbanos o rurales- con fines turísticos, locaciones de espacios o superficies fijas o móviles delimitados dentro de bienes inmuebles como locales comerciales, hipermercados y shoppings.

El plazo para informar estas operaciones, tanto para cuando se suscriba el contrato de locación o arrendamiento, o para cuando se modifique uno ya existente, será dentro de los 15 días corridos posterior al acto.

La ley N° 27.551 dispuso que, cuando se inicien acciones judiciales a causa de la ejecución de un contrato de locación, previo a correr traslado de la demanda, el juez debe informar a la AFIP sobre la existencia del contrato.

El sistema para las comunicaciones judiciales se encontrará disponible a partir del 15 de abril de 2021.