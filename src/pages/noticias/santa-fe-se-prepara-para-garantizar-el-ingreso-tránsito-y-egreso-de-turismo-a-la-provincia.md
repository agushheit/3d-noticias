---
layout: Noticia con imagen
author: 3DNoticias
resumen: "Turismo: Santa Fe se prepara "
category: Agenda Ciudadana
title: Santa Fe se prepara para garantizar el ingreso, tránsito y egreso de
  turismo a la provincia
entradilla: Se lleva adelante un trabajo integral interministerial que involucra
  tanto las condiciones de manejo y trazabilidad de las rutas, como los
  protocolos de salud correspondientes.
date: 2020-11-26T13:29:58.796Z
thumbnail: https://assets.3dnoticias.com.ar/protocolos-provincia.jpeg
---
El secretario de Turismo de la provincia, Alejandro Grandinetti, brindó detalles del encuentro llevado a cabo este miércoles para organizar el ingreso, tránsito y egreso de turistas a la provincia de Santa Fe en materia de protocolos COVID-19. Para ello, participan autoridades de los ministerios de Seguridad, Salud, Infraestructura, y Gestión Pública y las secretarias de Turismo y Municipios y Comunas.

Al respecto, Grandinetti aseguró que “el eje está puesto en la recepción de turistas del resto del país y también aquellos que lo hagan siendo santafesinos. Para ello, vamos a ofrecerles un entorno seguro desde el punto de vista de las condiciones de manejo y trazabilidad de las rutas, a través de Seguridad Vial; un operativo que cuide a los integrantes de las burbujas afectivas o familiares, a través de la policía; conjuntamente con el Ministerio de Salud para recordar protocolos y controlar las condiciones para hacer turismo en época de COVID-19”.

“Además -agregó el secretario de Turismo-, vamos a generar información para cada uno de los automovilistas que se desplacen por la provincia acerca de las atracciones turísticas y sobre protocolos para alojamientos, playa, espacios abiertos, turismo aventura, turismo al aire libre y los lugares en los cuales pueden disfrutar cercanía de algunas atracciones”.

“Buscamos que Santa Fe, que fue distinguida con el sello internacional como destino seguro para hacer turismo en época de pandemia, sea segura en el tránsito por las rutas de la provincia”, afirmó Grandinetti.

Por último, precisó que “se están estudiando las condiciones y requisitos a solicitar para ingresar a la provincia. Lo tenemos que seguir evaluando con el Ministerio de Salud”.