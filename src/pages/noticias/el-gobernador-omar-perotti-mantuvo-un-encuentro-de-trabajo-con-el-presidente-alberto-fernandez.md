---
category: Estado Real
date: 2021-12-02T06:15:37-03:00
thumbnail: https://assets.3dnoticias.com.ar/omaryalberto.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: El gobernador Omar Perotti mantuvo un encuentro de trabajo con el presidente
  Alberto Fernández
title: El gobernador Omar Perotti mantuvo un encuentro de trabajo con el presidente
  Alberto Fernández
entradilla: En la ciudad de Buenos Aires, el mandatario provincial también se reunió
  con el jefe de Gabinete; y los ministros nacionales de Seguridad y Ambiente.

---
El gobernador Omar Perotti mantuvo este martes una reunión con el presidente de la Nación, Alberto Fernández, para "repasar la realidad de la provincia, en su conjunto; y la problemática de seguridad, en particular", según precisó tras el encuentro el mandatario santafesino.

Luego de la reunión, Perotti explicó que fue "una reunión de rutina, de seguimiento, después del acuerdo para que las Fuerzas Federales estén presentes en la provincia de Santa Fe, particularmente en Rosario”, dijo. Cabe recordar que al territorio santafesino arribaron 575 agentes federales. En ese sentido, el gobernador detalló que "se repasó su desempeño y la necesidad de seguir con una fuerte presencia", manifestó.

Más adelante, Perotti indicó que el Presidente "ha ratificado su compromiso -porque la Argentina no puede permitir que esto pase en su territorio-, de seguir de cerca, ajustar y si es necesario reforzar la presencia y equipar a la provincia de Santa Fe, particularmente en Rosario", remarcó.

Previamente, el gobernador se reunió con el jefe de Gabinete, Juan Manzur; y con los ministros de Seguridad, Aníbal Fernández; y de Ambiente y Desarrollo Sostenible, Juan Cabandié.

Al respecto, Perotti señaló que tanto con el "jefe de Gabinete como con el ministro (Aníbal) Fernández, repasamos este compromiso" del Gobierno Nacional del envío de Fuerzas Federales a la provincia de Santa Fe.

Durante el encuentro, el ministro Manzur le manifestó al gobernador santafesino “su decisión de estar en la provincia en los próximos días, monitoreando el seguimiento de los temas de seguridad”; pero también “de las obras que se vienen llevando adelante y aquellas que están planteadas en el Presupuesto 2022 para la provincia de Santa Fe", precisó el el gobernador.

**ISLAS DE SANTA FE**  
Por último, el gobernador recordó que la reunión con el ministro de Ambiente y Desarrollo Sostenible “fue por un proyecto ambicioso: el de ampliar el Parque Nacional Islas de Santa Fe. Una noticia muy importante para la provincia de Santa Fe". En ese sentido, Perotti destacó que el acuerdo alcanzado tiene “una fuerte connotación de cuidado de nuestras áreas naturales y, particularmente, el resguardo de los humedales en la zona de islas del río Paraná. Este proyecto prioriza la cuestión ambiental y también la promoción turística de la provincia de Santa Fe", concluyó el gobernador de la provincia.