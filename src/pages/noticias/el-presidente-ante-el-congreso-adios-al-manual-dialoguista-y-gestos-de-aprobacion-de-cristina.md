---
category: Agenda Ciudadana
date: 2021-03-02T06:44:56-03:00
thumbnail: https://assets.3dnoticias.com.ar/alberto.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Clarín
resumen: 'El Presidente ante el Congreso: adiós al manual dialoguista y gestos de
  aprobación de Cristina'
title: 'El Presidente ante el Congreso: adiós al manual dialoguista y gestos de aprobación
  de Cristina'
entradilla: Los polémicos proyectos que anunció, como el de crear un tribunal constitucional,
  no están aun redactados. Impronta electoral y búsqueda de polarización.

---
De la moderación a la polarización. Alberto Fernández cajoneó el manual dialoguista en la apertura de las sesiones ordinarias del Congreso y abrió una etapa de confrontación con la Justicia, la oposición y los medios de comunicación, que parece estar enfocada en reforzar su vínculo con los sectores más radicalizados del kirchnerismo.

El Presidente acompañó su discurso con el adelanto de que enviará dos proyectos que apuntan a "circunscribir" el poder de la Corte Suprema de Justicia, entre otras iniciativas sobre las que trabaja para reformar el Poder Judicial, y anunció que impulsará una "querella criminal" contra el gobierno de Mauricio Macri por el manejo de la deuda con el Fondo Monetario Internacional.

Fue un discurso en el que evitó una autocrítica en torno al escándalo por el vacunatorio VIP que funcionaba en el Ministerio de Salud, con ribetes de campaña, sensiblemente diferente al que había pronunciado en la edición 2020, cuando convocó a todos los sectores y fuerzas políticas a trabajar en conjunto, pero que contó con la aprobación en varios pasajes de Cristina Kirchner y le valió algunos cruces con diputados de Juntos por el Cambio.

Alberto F. dejó para el tramo final del discurso más largo -una hora y 47 minutos- que ofreció como jefe de Estado, su abordaje sobre el funcionamiento del Poder Judicial. 

“El Poder Judicial está en crisis, parece vivir en los márgenes del sistema republicano”, arrancó el mandatario, al tiempo que recordó, por ejemplo, que los jueces no pagan Ganancias. Si bien, como había anticipado Clarín, en los pasillos de Casa Rosada descontaban que la Justicia iba a ser uno de los principales ejes del discurso, con la insistencia a los diputados para que aprueben la reforma del fuero federal, no estaba zanjado el debate respecto hasta dónde apuntaría Fernández.

Rápidamente el jefe de Estado dejó en claro su desafío a los integrantes del Alto Tribunal. “En el caso de los miembros de la Corte Suprema, acceder a la declaración jurada de sus bienes es virtualmente imposible”, cuestionó, ante los cinco ministros que siguieron la ceremonia vía Zoom.

Según el Presidente, “los grandes medios de comunicación ocultan” estos "hechos llamativos" de la Justicia. Entre ellos, el jefe de Estado aludió la situación del fiscal federal Carlos Stornelli, investigado en la causa de espionaje. 

Ofuscado por algunas decisiones del Alto Tribunal, Alberto F. dijo que busca “la creación de un Tribunal Federal de Garantías que tendrá competencia exclusivamente en cuestiones de arbitrariedad y al que se podrá llegar por vía del recurso extraordinario”.

En enero, el Presidente había hablado de su intención de copiar un tribunal que existe en España que “no” está por debajo de la Corte sino "a la par". Aunque según pudo saber Clarín el proyecto no está "ni siquiera redactado", a priori no parece ir en ese sentido.

Tampoco está escrito el texto que prometió enviar para reformar “y despolitizar” el Consejo de la Magistratura.

La batería de iniciativas para cambiar el funcionamiento del Poder Judicial, una reedición más austera de la "democratización de la justicia" auspiciada por Cristina en 2013, se completó con el pedido de que Diputados apruebe la reforma del Ministerio Público Fiscal (no hubo alusiones al pliego de Daniel Rafecas), y la advertencia de que el Congreso debe cumplir un rol más activo en el control de jueces.

Las duras palabras de Alberto F. por momentos generaron aplausos de su Gabinete y del grueso de legisladores del Frente de Todos. También, más importante, gestos de aprobación de Cristina.

Un efecto similar produjo su repaso por la herencia del gobierno de Macri y el anuncio de que pedirá que se investigue la deuda de 44 mil millones de dólares con el Fondo Monetario Internacional que tomó la gestión anterior.

“He instruido a las autoridades pertinentes para que formalmente inicien querella criminal tendiente a determinar quienes han sido los autores y partícipes de la mayor administración fraudulenta y de la mayor malversación de caudales que nuestra memoria registra”, dijo.

En un escenario menos hostil (con sólo 90 legisladores) del que anticipaban en el oficialismo, el escándalo por el vacunagate tuvo una breve aparición. "Las reglas se deben cumplir. Si se cometen errores, la voluntad de este Presidente es reconocerlos y corregirlos. Aún cuando en lo personal me causaran mucho dolor, tomé las decisiones que correspondían", se defendió.