---
category: Agenda Ciudadana
date: 2021-06-09T08:36:22-03:00
thumbnail: https://assets.3dnoticias.com.ar/colectivo1.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Diario Uno
resumen: Para el municipio, si no hay ayuda de nación, la crisis en el transporte
  es terminal
title: Para el municipio, si no hay ayuda de nación, la crisis en el transporte es
  terminal
entradilla: El secretario de Transporte de la ciudad, Lucas Crivelli, insistió con
  una distribución equitativa de subsidios. Aseguró que no hay margen para un aumento
  de boleto.

---
Seis ciudades del interior del país, entre ellas Santa Fe, le solicitaron al gobierno nacional una distribución más equitativa de subsidios al transporte.

Lucas Crivelli, secretario de Transporte de esta capital, advirtió que se trata de un servicio municipal que "en el interior del país está verdaderamente en una crisis de carácter terminal, si es que no hay una apoyatura del gobierno nacional para acompañar esta prestación".

Señaló con relación a la reunión entre los intendentes de la ciudades involucradas en el reclamo (Rosario, Bariloche, Córdoba, Mar del Plata y Bahía Blanca) que las mismas apuntan al pedido para que haya equidad, y añadió: "Porque resulta muy difícil entender que un ciudadano del interior del país tenga que pagar 40, 45, 50 pesos un boleto de colectivos y en Capital Federal pueda abonar $18,90 sin mayores complicaciones.

Subrayó en declaraciones a la emisora LT 10 que "las empresas en el Amba reciben subsidios por colectivos que rondan los 700 mil pesos, mientras que en el interior del país alcanzan, a duras penas los 200 mil pesos".

Para el funcionario municipal, "la discrecionalidad en la asignación de los recursos hace que haya servicios que estén próximos a tener una crisis terminal y otros servicios que siguen funcionando, aun sin pasajeros, sin mayores complicaciones".

Y profundizó: "Particularmente, en la ciudad de Santa Fe, la realidad indica que el 45 por ciento del costo del servicio se abona con subsidios y el resto es con boleto. Pero no hay margen para que la gente pueda abonar más. Por otro lado, en caso de subir el boleto habría que subirlo arriba de los 130 pesos. Son tarifas inaccesibles.

Hoy la tarifa no es una variable de ajuste, porque además hay muy pocos pasajeros que están usando el servicio y se necesita una mayor oferta. La tarifa no sería la herramienta de negociación; sino que se necesita de la restructuración de subsidios.