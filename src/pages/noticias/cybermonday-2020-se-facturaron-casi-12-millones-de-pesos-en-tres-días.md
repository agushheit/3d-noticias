---
layout: Noticia con imagen
author: "  Por Clara Blanco"
resumen: CyberMonday 2020
category: Agenda Ciudadana
title: "CyberMonday 2020: Se facturaron casi 12 millones de pesos en tres días"
entradilla: Según datos aportados desde la Cámara Argentina de Comercio
  Electrónico los artículos más vendidos fueron alimentos, bebidas con y sin
  alcohol, indumentaria y accesorios infantiles, entre otros.
date: 2020-11-05T18:00:29.569Z
thumbnail: https://assets.3dnoticias.com.ar/cybermonday.jpg
---
Del 2 al 4 de noviembre se realizó la edición 2020 del CyberMonday que registró el ingreso de 3 millones de usuarios al sitio oficial. Además se supo que el 72% de esos consumidores optaron por analizar las ofertas desde dispositivos móviles. Ambos datos fueron aportados por la Cámara Argentina de Comercio Electrónico (CACE), organizadora del evento.

La pandemia por coronavirus marcó un gran cambio en los hábitos de consumo y el comportamiento durante la experiencia de compra de las personas. Las ventas online se dispararon debido al aislamiento y las empresas debieron ofrecer nuevas alternativas a sus clientes. Con respecto a la ciudad de Santa Fe, varios locales y marcas comenzaron a vender sus productos desde un sitio web.

Según información oficial brindada por CACE, en este CyberMonday que fue 2, 3 y 4 de noviembre, se facturaron 11.811 millones de pesos entre las 555 empresas participantes. Los productos más vendidos fueron alimentos, bebidas con y sin alcohol, indumentaria y accesorios infantiles, decoración y calzado no deportivo.

"En esta edición, la cercanía al verano y los hábitos generados en la pandemia se reflejan en las compras de los usuarios que se focalizan en aparatos de refrigeración del hogar, elementos para acondicionar los exteriores y como cada año, en los productos de tecnología y deporte", sostuvo Gustavo Sambucetti, director institucional de la Cace.

Cabe aclarar que el sitio oficial de [CyberMonday 2020](https://www.cybermonday.com.ar/) sigue activo dado que ahora se promociona la CyberWeek. La misma va desde este jueves 5 de noviembre hasta el domingo 8.