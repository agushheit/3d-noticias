---
category: La Ciudad
date: 2021-02-24T05:01:48-03:00
thumbnail: https://assets.3dnoticias.com.ar/alisos.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad de Santa Fe
resumen: La Municipalidad comenzó a censar a las familias que viven en Los Alisos
title: La Municipalidad comenzó a censar a las familias que viven en Los Alisos
entradilla: 'El relevamiento se hace en forma conjunta con el gobierno provincial.
  El asentamiento ubicado en el sector norte del barrio El Pozo se ha visto afectado
  por las crecidas de la laguna Setúbal en reiteradas ocasiones.

'

---
Equipos municipales de la Agencia Santa Fe Hábitat y la Secretaría de Cuidados, en forma conjunta con personal de la Secretaría de Hábitat de la provincia, comenzaron a censar hoy a las familias que viven de manera permanente en el sector norte de la ribera del barrio El Pozo, específicamente sobre la playa Los Alisos.

El objetivo es constatar la situación real del asentamiento (cantidad de familias asentadas, conformación de los grupos familiares, tiempo de ocupación), a fin de establecer la línea de base para un futuro plan de reasentamiento o relocalización. A la vez, se recabará información referida a educación, salud, movilidad, seguridad y emprendimientos de economía social.

El asentamiento en cuestión tiene un crecimiento sostenido desde 2009 y al estar emplazado en una zona de seguridad hídrica ha sido alcanzado por las crecidas de la laguna Setúbal en reiteradas ocasiones. Además de las altas condiciones de vulnerabilidad física y habitacional, el área carece de equipamientos y espacios comunes que sí están presentes en la parte consolidada de El Pozo.

La actualización del relevamiento de la fecha arrojó que hay un total de 57 viviendas en Los Alisos -en las cuales habrá que ver cuántos grupos familiares conviven allí-, y de 11 familias en el sector conocido como El Cañaveral.

“Con el Plan Integrar, desde el municipio estamos trabajando de manera coordinada y consensuada con los diferentes niveles del Estado, organizaciones sociales y entidades intermedias en el desarrollo de distintos proyectos de integración socio urbana. En algunos casos, como el de la playa Los Alisos, prevén el reasentamiento de las familias”, describió Paola Pallero, a cargo de la Agencia Santa Fe Hábitat.

“Las relocalizaciones buscan darle una solución sostenible a las familias que viven en situación de riesgo hídrico. Después de este relevamiento, se iniciarán las consultas con las familias a relocalizar a fin de elaborar la propuesta final de reasentamiento y las alternativas de compensación tanto para las viviendas como para las actividades económicas y sociocomunitarias a desplazar”, añadió.

Como antecedentes, se contaba con el relevamiento realizado en 2017 por el Registro Nacional de Barrios Populares (Renabap) en el cual fueron encuestadas 34 familias (estimándose en ese momento un total de 48 familias). En tanto, según un relevamiento realizado en junio de 2019 por la Secretaría de Estado del Hábitat había 51 familias viviendo efectivamente.

**Beneficiarios**

Las familias residentes a relocalizar deben cumplir con las siguientes condiciones:

 ·         Residir y/o desarrollar una actividad económica y/o comunitaria en el sector cuya liberación se requiera por riesgo ambiental,

·         Estar registradas en el Censo Barrial de Reasentamiento que se efectúe como parte de los planes o que acrediten vivir en la zona afectada al momento del censo,

·         No tener otros inmuebles bajo titularidad del/la adjudicatario/a de vivienda o de los/as miembros de su hogar.