---
category: La Ciudad
date: 2022-01-08T06:15:04-03:00
thumbnail: https://assets.3dnoticias.com.ar/COLONIASMUNICIPALES.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Arrancan las colonias municipales de verano
title: Arrancan las colonias municipales de verano
entradilla: "Con 3.000 inscriptos y estrictos protocolos, este martes comenzará la
  propuesta destinada a las infancias, las juventudes y las personas adultas mayores.
  Este año habrá ocho sedes. \n\n"

---
Este martes 11 de enero comenzará la edición 2022 de las colonias de verano. Son unos 3 mil inscriptos los que se sumaron a esta propuesta gratuita de la Municipalidad de Santa Fe que se desarrollará de lunes a viernes, de 8 a 12 horas, en ocho sedes y con estricto protocolo ante el aumento de casos de Covid-19.

La secretaria de Integración y Economía Social del municipio, Ayelen Dutruel, afirmó que “la convocatoria superó las expectativas ampliamente, teniendo en cuenta que el año pasado no pudimos hacer colonias en el esquema tradicional que esperan las vecinas y vecinos de la ciudad. Este año volvemos con el esquema de colonias en predios, con piletas y un armado de calidad para esperar a infancias, a jóvenes y a personas adultas mayores”.

La edición 2022 busca impulsar la intergeneracionalidad: “Habrá propuestas en que los niños convivirán con adultos mayores y jóvenes, con una fuerte impronta recreativa y deportiva. Sabemos que es importante pensar en el derecho al uso del tiempo libre. Las colonias son un espacio para disfrutar, para recrearse con otros y de manera colectiva”, definió la funcionaria.

Según se informó, los participantes concurrirán a las colonias hasta el 18 de febrero desde las estaciones municipales, en transportes gratuitos.

**Inscripciones abiertas**

Dutruel mencionó que la convocatoria continuará abierta “para aquellos que aún no han podido acercarse por diferentes cuestiones a las estaciones municipales”.

Son 17 los puntos habilitados para la inscripción: las estaciones municipales de Colastiné, La Guardia, Alto Verde, La Boca, San Lorenzo (Entre Ríos 4080), Barranquitas (Iturraspe y Estrada), Mediateca (pasaje Mitre y Tucumán), CIC Facundo (Facundo Zuviría 8040), Loyola (Furlong 8400, esquina Pedroni), San Agustín (Fray Francisco de Aroca 9549), Abasto (Gobernador Menchaca y Pedro de Espinoza), Villa Teresa (La Pampa 6200/6300), Dorrego (Larrea 1735), Varadero Sarsotti (Tacuaritas S/N), Las Lomas (Viñas 6800 o Padre Viera, entre Estanislao Zeballos y Espora), Liceo Norte (Callejón Aguirre 5197, esquina pasaje Larguía) y Villa Hipódromo (Roque Sáenz Peña 6148).

Al momento de la inscripción se deberá completar una ficha y presentar certificado médico, fotocopia de DNI, buco dental y último electrocardiograma, en el caso de las personas adultas mayores.

**Protocolo vigente**

Ante la escalada de casos positivos de Covid-19, desde el municipio se ultiman detalles para actualizar protocolos.

En ese sentido, la secretaria precisó que “habrá burbujas y se definirán prioridades, en relación a las edades”. También detalló que “en cada unidad de transporte y predio habrá protocolos a cumplir y para eso trabajamos con los equipos de las colonias”.

**Grupos y sedes**

Las sedes para la temporada de verano 2022, son Amsafe y Amsap, que recibirán tanto a grupos de personas adultas mayores,como a infancias y juventudes; Stia, que recibirá únicamente a personas adultas mayores, al igual que Pierdas Blancas; y Alto Verde, destinada a infancias y juventudes, del mismo modo que Fraternidad deportiva, Estacionero y Canillitas.

El grupo de infancias está destinado a personas de entre 6 y 13 años, que concurrirán a las colonias de martes a jueves; el grupo de juventudes (14 a 35 años) participará de la propuesta los viernes; y las personas adultas mayores podrán asistir de lunes a viernes. Para el caso de las personas con discapacidad que deseen participar, deberán inscribirse en los grupos de acuerdo a su edad, siendo el rango etario en el caso de juventudes, de 13 a 40 años.