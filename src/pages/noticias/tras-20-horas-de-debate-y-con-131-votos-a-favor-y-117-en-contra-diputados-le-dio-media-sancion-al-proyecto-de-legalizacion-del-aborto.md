---
category: Agenda Ciudadana
date: 2020-12-11T16:52:55Z
thumbnail: https://assets.3dnoticias.com.ar/aborto.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuentes: Infobae / Diario Uno'
resumen: Tras 20 horas de debate y con 131 votos a favor y 117 en contra, Diputados
  le dio media sanción al proyecto de legalización del aborto
title: Tras 20 horas de debate y con 131 votos a favor y 117 en contra, Diputados
  le dio media sanción al proyecto de legalización del aborto
entradilla: La Cámara Baja aprobó el texto que pasará al Senado, donde se esperan
  números más ajustados. Afuera del Congreso, militantes “verdes” y “celestes” realizaron
  una vigilia para esperar el resultado.

---
En una maratónica sesión que ya acumula 20 horas pero continúa con el debate de la ley de los 1.000 días, la Cámara de Diputados le dio media sanción al proyecto de Interrupción Voluntaria del Embarazo (IVE). La iniciativa se aprobó con 131 votos a favor, 117 en contra y 6 abstenciones y ahora deberá tratarse en el Senado, donde se esperan números más ajustados, con una definición voto a voto.

**A diferencia de 2018**, cuando la Cámara Baja le dio luz verde al proyecto con 129 votos positivos, 125 negativos y una sola abstención, **esta vez se extendió la brecha**. En tanto, fuera del Congreso militantes “verdes” y “celestes”, que habían comenzado a arribar a partir del mediodía, llevaron a cabo una vigilia durante toda la noche, a la espera de la resolución. Finalmente, quienes deseaban “que sea ley” terminaron celebrando.

Por su parte, el Presidente de la Nación estuvo atento a lo que pasaba en la Cámara de Diputados durante toda la jornada. Lo mantuvieron informado la secretaria de Legal y Técnica, Vilma Ibarra, y el ministro del Interior, Eduardo ‘Wado’ de Pedro, quien desde Casa Rosada intercambió mensajes con la ministra de Mujeres, Géneros y Diversidad, Elizabeth Gómez Alcorta. Las dos se quedaron todo el día y toda la noche en el Congreso, con la lista de diputados y diputadas marcados en sus celulares con distintos colores según el sentido de su voto.

Ya varios días atrás, el número necesario para la aprobación estaba garantizado, aunque ayer el plan fue consolidarlo e intentar ampliar la brecha como señal hacia la sociedad y hacia el Senado, donde hay una virtual paridad.

El rumor sobre la posibilidad de que Alberto Fernández pasara por el Congreso se instaló fuerte por la tarde, después de que varios funcionarios y ministros dejaran en claro con su presencia el fuerte respaldo del oficialismo, el primer gobierno en presentar un proyecto de interrupción voluntaria del embarazo. Precisamente trabajaron en el texto Ibarra y Gómez Alcorta, a pedido del Presidente.

Sin embargo, hubo coincidencias en Casa Rosada y en Diputados respecto a dejar el protagonismo a los legisladores de todas las fuerzas políticas. “Si uno se quiere apropiar, lo que se logra es que no se vote la ley”, señaló Gómez Alcorta a un grupo de periodistas, entre ellos a Infobae, antes de la medianoche. “Es de la calle, es de las pibas, es de La Campaña”, se la oyó decir.

***

![](https://assets.3dnoticias.com.ar/aborto1.jpg)

***

La ministra advirtió que el voto fue transversal, por lo que el resultado fue atribuido de la misma manera a diputados de Juntos por el Cambio, como de fuerzas provinciales, de izquierda y del Frente de Todos. La funcionaria reivindicó además el antecedente del debate de 2018, por haber evidenciado un tema hasta entonces tabú para la política. Ese año, fue Mauricio Macri el primer presidente en habilitar el tema, aún cuando no lo apoyaba, y el Senado frenó la ley.

Para conmemorar el antecedente pasaron por el Congreso varios ex diputados de aquel grupo autodenominado “las sororas”. Hubo foto en Pasos Perdidos con el ahora diputado provincial Daniel Lipovetsky (PRO); la intendenta quilmeña Mayra Mendoza; la ahora diputada provincial santafesina Lucila de Ponti; la correntina Araceli Ferreyra; la presidenta de AySA, Malena Galmarini; y quienes siguen ocupando una banca como Leonardo Grosso; Mónica Macha y Cecilia Moreau.

En la escena faltó Silvia Lospennato quien, junto a Moreau y Macha, volvió a ser protagonista en el nuevo debate, mientras que Galmarini, vestida de traje verde, no dejó de buscar más votos. Hubo charlas con gobernadores, como el de San Juan, el de Misiones, el de Salta y el de Tucumán, tanto para sumar votos como para evitar que otros se cayeran.

Más allá de los 165 discursos divididos entre los que votaron a favor y en contra, no hubo discursos “beligerantes”, salvo la denuncia de la diputada del PRO Dina Rezinovsky. “Con la billetera están comprando diputados oficialistas. Me frustra saber que están dando vuelta diputados”, resaltó la legisladora macrista como hace dos años, cuando Elisa Carrió, entonces diputada, se retiró a descansar y al regresar por la mañana expresó sus sospechas sobre el cambio de votos ocurrido durante la madrugada.

El voto en contra estaba arriba en el inicio de la sesión, pero finalmente, por el cambio de opinión de legisladores pampeanos, se votó la media sanción por 129 contra 125. Solo hubo una abstención.

***

![](https://assets.3dnoticias.com.ar/aborto2.jpeg)

***

Este año. el postergado debate que anunció el Presidente el 1° de marzo se trató en comisiones sólo una semana y se trató en una sesión que arrancó ayer minutos después de las 11 de la mañana y se extendió durante toda la madrugada. La estrategia fue apuntar a la necesidad de contemplar la IVE como política de Salud y, en paralelo, votar la ley de los Mil Días -fue aprobada con 196 votos a favor y 5 abstenciones- para acompañar a las mujeres gestantes y a los niños hasta los tres años.

Varias veces el presidente del cuerpo, Sergio Massa, pidió respetar los tiempos acordados: 5 minutos por cada diputado y diputada. Los excesos fueron extendiendo el debate y postergando la hora de votación. De todos modos, se lo notó muy conforme por el tono, de un respeto poco habitual.

No hubo gritos, ni chicanas, ni discursos virulentos. Hasta alguien tan efusivo como el cordobés Luis Juez lo destacó: “Presidente no sé como ser original a la una de la mañana. Y cómo hablar sin romper este clima armonioso, pensamientos distintos pero un clima armonioso”.

Según su punto de vista, el debate no resolvería el tema que como abogado anticipó que “lo va a terminar de resolver la Corte”. Defendió “el derecho de la persona por nacer, que el único derecho que tiene es a ver la luz”.

Lo siguió en la palabra el cordobés Pablo Carro, del Frente de Todos. “Córdoba es una provincia de contrastes”, arrancó una risa propia y carcajadas entre otros legisladores. Y en su caso, justificó el voto a favor.

Pablo Yendlin, Vilma Ibarra, Sabina Fréderic, Sergio Massa, Ginés González García, Elizabeth Gómez Alcorta y Fernanda Raverta en el Salón de Honor de Diputados (Presidencia)

Pablo Yendlin, Vilma Ibarra, Sabina Fréderic, Sergio Massa, Ginés González García, Elizabeth Gómez Alcorta y Fernanda Raverta en el Salón de Honor de Diputados (Presidencia)

“Vamos a votar de manera transversal porque no podemos ser indiferentes para un problema de salud pública”, señaló Maximiliano Ferraro en su última sesión como presidente del bloque de la Coalición Cívica. Como en esa fuerza política, todos tuvieron divisiones internas. Él mismo opina distinto que Carrió, aún cuando preside la Coalición Cívica. Cuatro de los 14 diputados del bloque ‘lilito’ habían anticipado su voto a favor, el resto fueron voceros de la campaña ‘por las dos vidas’ e incluso María Lucía Lehmann solicitó postergar el debate hasta después de Reyes. Mariana Zuvic mantuvo en reserva su posición.

Otro que en el punteo de “verdes” fue contado a favor fue Sebastián García de Luca (PRO), muy cercano a Emilio Monzó, el ex presidente de la cámara de Diputados. En su caso no pidió la palabra y prometió revelar su postura recién en el momento de la votación. No pidió la palabra como sí lo hizo Flavia Morales, del Frente de la Concordia de Misiones. Dos años atrás Morales votó en contra, pero esta vez sorprendió. “Desde la votación de 2018 he reflexionado, he analizado. La penalización no logra evitar que muchas mujeres realicen esta práctica y más aún en la clandestinidad. En efecto, la intervención del Estado, en este momento, es fundamental”, apuntó. Y aseguró: “No se trata de lo que yo piense, sino de lo que está pasando allá afuera y estas prácticas clandestinas están ocurriendo aún en pandemia. He entendido que tenemos que optar entre la existencia de una regulación u optar por que las cosas sigan de la misma manera”. Nadie se hizo cargo de haberla convencido.

A la inversa, la chaqueña Aída Ayala (UCR), presente en el recinto, anunció hace varias semanas que esta vez cambiaría su voto y rechazaría la IVE.

***

![](https://assets.3dnoticias.com.ar/aborto3.jpg)

***

A la 1:55, Nancy Sand, del Frente de Todos de Corrientes, anunció su postura. En el oficialismo la tenían en la lista “celeste”, aunque ella había hecho declaraciones periodísticas a favor de la ley. Así lo destacó: “A pesar de que pensé que podía acompañar lo contrario, hoy debo decir que por formación, por convicción y por conciencia que la interrupción de una vida en desarrollo no es la solución a los problemas”.

En contrapartida, la santafesina Alejandra Obeid (Frente de Todos), católica, reveló que le costó mucho tomar la decisión a favor y agradeció a su bloque y a sus amigas por ayudarla: “Pude entender que lo que pensamos sobre estas leyes no pueden nublar nuestro compromiso de legislar en la ampliación de derechos”.

Otros que terminaron con la incertidumbre durante los discursos fueron el sanjuanino Francisco Guevara, que un día antes visitó a la dirigente de La Cámpora Mayra Mendoza. Guevara es el único diputado “verde” de su provincia.

También la ex gobernadora de Catamarca Lucía Corpacci reveló su postura al aparecer en el recinto con un pañuelo de la Campaña por el Aborto. A las 2.39, más de 12 horas después de iniciada la sesión, el jujeño Jorge Rizzotti, de la UCR, develó la incógnita: “Voto a favor”.

***

![](https://assets.3dnoticias.com.ar/aborto4.jpg)

***

La diputada santafesina Vanesa Massetani, del Frente de Todos, advirtió en pleno debate: “Ningún funcionario va a decirme lo que tengo que hacer hoy aquí en esta banca. Fueron elegidos a dedo. A mí me eligió el pueblo”.

En los palcos había funcionarios nacionales y ella, aunque no lo mencionó por su nombre, hizo referencia al ministro de Salud, Ginés González García, por llamar “fenómeno” al embrión: “Resulta por demás revelador lo que puede realizar un ‘fenómeno’: se mueve, da patadas, agita los brazos, la cabeza, se chupa el dedo, la mano, hace gestos con su caca, bebe líquido amniótico y hace pis”. Como ella, varios “celestes” fundamentaron su voto en contra al considerar como el inicio de la vida al momento de la concepción y en la saturación que implicaría para el sistema sanitario, a pesar que del otro lado insistían en que en la actualidad los embarazos se pueden interrumpir de manera ambulatoria con la ingesta de pastillas.

También en el PJ, a pesar del involucramiento del Presidente y su gabinete, hubo opositores a la ley, como la mayoría de los sanjuaninos, entre los que estuvo José Luis Gioja, presidente del Consejo Nacional del partido.

“Algunos han dicho que no es el momento. Si algo sabemos las mujeres argentinas es que siempre hay una excusa cuando queremos ampliar derechos, nunca es el mes apropiado, nunca es el día apropiado”, respondió a muchos diputados Cristina Álvarez Rodríguez, sobrina nieta de Evita e integrante de la mesa partidaria. Enumeró derechos otorgados por ley como el voto femenino o los más recientes como el matrimonio igualitario y la identidad de género. La diputada, muy cercana a la vicepresidenta Cristina Fernández de Kirchner, contó que el jefe del bloque, Máximo Kirchner, cedió su tiempo para que se expresaran las diputadas del Frente de Todos “porque cree que las mujeres tenemos que seguir alzando la voz y dando la pelea juntas”.

***

![](https://assets.3dnoticias.com.ar/aborto5.jpeg)

***

Kirchner no estuvo en el recinto: sigue aislado después de haber compartido un acto y un traslado en auto con el gobernador Axel Kicillof y un colaborador bonaerense que fue diagnosticado COVID-19 positivo. Es más, como él y una cincuentena de diputados, la entrerriana Blanca Osuna (Frente de Todos) participó en forma remota y hasta dio un discurso desde un sanatorio, internada con coronavirus. Hubo aplausos para ella.

En las bancadas mayoritarias hubo voto transversal. “Hoy me tiene sin cuidado si alguien lo hizo con criterio oportunista”, desestimó Albor Cantard de la UCR de Santa Fe, que reivindicó a su partido como una fuerza laica y justificó su apoyo. En cambio Hernán Berisso, matancero del PRO, pidió más prevención y trató de explicar que “no estar a favor del aborto legal no significa que estoy a favor del aborto clandestino”. Jorge Lacoste, de la UCR de Entre Ríos, explicó que está en contra del aborto aunque votaría a favor porque la ley establece un derecho pero no una obligación, ya que quien no lo desee no interrumpirá su embarazo.

El debutante Omar de Marchi, flamante vicepresidente primero, varias veces pidió acotar los discursos. Diez minutos antes de las 2, avisó que la suma de cada minuto extra que estaban utilizando los diputados pasaría la votación para las 7. Massa ya había pedido colaboración con la amenaza de cortar algún micrófono, lo que nunca hizo.

***

![](https://assets.3dnoticias.com.ar/aborto6.jpg)

***

Pasadas las cinco de la mañana, el diputado Daniel Ferreyra anunció que se iba a abstener de votar tras denunciar que su hija recibió amenazas: “Una periodista dijo que vendí mi voto. Por esa razón no estoy actuando libremente y a la hora de votar me voy a abstener”.

# **Cómo votaron los diputados de Santa Fe**

### El Frente de Todos votó a favor del proyecto de aborto legal, el macrismo tuvo cuatro respaldos y seis rechazos; el partido socialista, dividido.

<br/>

***

<span style="font-family: arial,helvetica; font-weight: bold; font-size: 1.2em">🟢 **A favor de la ley de interrupción voluntaria del embarazo (IVE) votaron:**</span>

<br/>

**Del Frente de Todos (PJ)**: Marcos Cleri, Josefina González, Germán Martínez, Alejandra Obeid, Esteban Bogdanich y Patricia Mounier.

**De la coalición Juntos por el Cambio**: los legisladores de extracción radical Albor Cantard, Gonzalo del Cerro, Ximena García y Juan Martín.

**Del Partido Socialista**: Enrique Estévez.

<br/>

***

<span style="font-family: arial,helvetica; font-size: 1.2em; font-weight: bold;">🔵 **En contra de la ley de interrupción voluntaria del embarazo (IVE) votaron:**</span>

<br/>

**De Juntos por el Cambio**: Gisela Scaglia, Juan Núñez, Luciano Laspina, Federico Angelini, Carolina Castets, Lucila Lehmann (de la Coalición Cívica).

La **massista** Vanesa Massetani y el **socialista** Luis Contigiani.

<br/>

***

El proyecto obtuvo media sanción la mañana del viernes con 131 votos afirmativos y 117 negativos, luego de una sesión de más de 20 horas y finalizó llegando a las 9 de la mañana, ya que se continuó con el proyecto de Ley de los 1.000 días, ahora ley.

#### <span style="color: #25282C; font-family: arial,helvetica; font-weight: bold;">Votación Ley Aborto Legal Diputados 2020 **|**</span><span style="color: #25282C; font-family: arial,helvetica;"> [Conteo de votos](https://es.scribd.com/document/487758416/Votacion-Ley-Aborto-Legal-Diputados-2020) </span>