---
category: Agenda Ciudadana
date: 2021-03-01T07:30:05-03:00
thumbnail: https://assets.3dnoticias.com.ar/CONGRESO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: El Presidente dará su mensaje ante la Asamblea Legislativa
title: El Presidente dará su mensaje ante la Asamblea Legislativa
entradilla: 'Será este lunes a partir de las 12 desde un recinto de la Cámara de Diputados
  atípico: no habrá invitados especiales y buena parte de los legisladores seguirá
  las instancias del discurso de manera virtual.'

---
El presidente Alberto Fernández dejará inaugurado este lunes el 139 período de sesiones ordinarias del Congreso con un mensaje ante la Asamblea Legislativa, en un escenario atípico y sin invitados a causa de las normas sanitarias de prevención por la pandemia de coronavirus.  
Con eje en las metas previstas por su administración para dar impulso a la reactivación de la economía, en un año que volverá a estar marcado por la pandemia, el Presidente dará su tercer mensaje ante la Asamblea Legislativa a partir de las 12 desde un recinto de la Cámara de Diputados en el que no habrá invitados especiales ya que buena parte de los legisladores seguirá las instancias del discurso de manera virtual.

El mandatario se dirigió por primera vez ante la Asamblea durante su asunción del 10 de diciembre de 2019 y pronunció su segundo discurso durante la primera apertura de sesiones ordinarias de su mandato, el 1 de marzo de 2020, 20 días antes de decretar el aislamiento social y obligatorio con motivo de la propagación de la Covid-19.

La vicepresidenta Cristina Fernández de Kirchner arribará al Palacio Legislativo en primer término y será la encargada de abrir, a las 11, la Asamblea Legislativa.

**Los ejes del discurso**

Si bien se mantuvo hermetismo, voceros presidenciales informaron que las políticas sanitarias para paliar los efectos de la pandemia de coronavirus y las metas fijadas para ahondar la reactivación económica serán los pilares del mensaje.  
Por primera vez en la historia legislativa, los gobernadores no estarán en el recinto de sesiones y deberán seguir las instancias de la Asamblea a través de las cámaras que se dispondrán en el hemiciclo.

De acuerdo a las medidas sanitarias para cumplir con el distanciamiento social, será un recinto con acceso limitado y el jefe del Estado hablará a la Asamblea, escoltado por las autoridades de ambas Cámaras legislativas y los miembros de su Gabinete pero con la mayoría de los legisladores conectados en forma remota.

En tanto, los cinco integrantes de la Corte Suprema, Elena Highton de Nolasco, Ricardo Lorenzetti, Juan Carlos Maqueda, Horacio Rosatti y Carlos Rosenkrantz, también participarán de manera virtual.

La vicepresidenta Fernández de Kirchner y el presidente de la Cámara de Diputados, Sergio Massa, remarcaron esta semana, al anunciar las medidas de acceso limitado al recinto, la importancia de "dar estricto cumplimiento a todas las medidas sanitarias que garanticen el cuidado de la salud de todas las personas que participen".

Por esa razón, la sesión de Asamblea se llevará a cabo bajo la modalidad mixta presencial y remota, con una cantidad limitada de integrantes del Poder Legislativo, entre los que se priorizará a los jefes de las bancadas de cada una de las Cámaras.

El titular de la Cámara baja, Massa, pidió a las autoridades de los bloques parlamentarios un listado acotado de legisladores que podrán presenciar el discurso desde el recinto.  
El resto participará a través de la plataforma Webex, de la misma manera en que se realizaron las sesiones remotas a lo largo del año 2020.  
Desde las 9, los legisladores recibirán un link de acceso al "recinto virtual" y a partir de las 10 quedará habilitado el acceso al mismo, precisaron fuentes del Senado de la Nación.

La prensa estará presente, pero se restringirá su acceso de acuerdo con las instrucciones dadas a conocer esta semana. La cobertura audiovisual y fotográfica estará a cargo de los equipos de comunicación del Congreso de la Nación en forma exclusiva.Los periodistas sólo podrán permanecer en el salón de Pasos Perdidos, el salón Delia Parodi y el palco de prensa del recinto de la Cámara de Diputados.

La apertura de las sesiones del año pasado se realizó en momentos en que la pandemia de coronavirus ya afectaba a las principales capitales de Europa y Asia, pero aún no había llegado a la Argentina.  
El Gobierno nacional decidió 18 días después del discurso de Fernández, el 20 de marzo de 2020, disponer la cuarentena que, con mayor o menor intensidad, se mantuvo durante buena parte del año pasado.  
En el Congreso, en tanto, mediante el acuerdo entre el oficialismo y la oposición, se decidió avanzar con un protocolo de sesiones remotas que permitió realizar 31 de ellas a lo largo de 2020, cuando en 2019, y sin pandemia, sólo se llevó a cabo una decena.  
Para ello, hizo falta no sólo la virtualidad del trabajo legislativo, sino un oficialismo con mayoría propia, algo que conspiró contra la realización de sesiones en Diputados, donde sesionaron en 18 oportunidades.