---
layout: Noticia con imagen
author: "Fuente: Diario UNO"
resumen: Robo a Escuela Luis Borruat
category: La Ciudad
title: Otra vez la escuela Luis Borruat sufrió robo de elementos y destrozos
entradilla: La institución educativa de barrio Santa Rosa de Lima sufrió el
  segundo hecho en la última semana. Se llevaron mercadería del comedor escolar,
  todos los ventiladores y rompieron cosas.
date: 2020-11-16T13:38:36.514Z
thumbnail: https://assets.3dnoticias.com.ar/borruat.jpg
---
Este sábado antes de la medianoche, la escuela Nº 1.111 Luis Borruat, sufrió el sexto robo en loque va del año y el segundo en la última semana. La institución ubicada en la esquina de Suipacha y Aguado, en barrio Santa Rosa de Lima, además del robo de la mercadería del comedor escolar y todos los ventiladores, sufrió destrozos en el mobiliario los maleantes defecaron en las instalaciones.

Los vecinos fueron quienes dieron aviso al 911 tras escuchar ruidos provenientes del establecimiento escolar. Al lugar llegaron efectivos de la Subcomisaría 2ª y del Comando Radioeléctrico que se encontraron con las autoridades de la escuela, recorrieron el interior y observaron daños en las instalaciones y aberturas como el robo de alimentos.



![](https://assets.3dnoticias.com.ar/borruat1.jpg)



**Peritajes criminalísticos**

Informaron la novedad a la Jefatura de la Unidad Regional I La Capital de la Policía de Santa Fe, y estos hicieron lo propio con el fiscal en turno del Ministerio Público de la Acusación, que ordenó la investigación y el esclarecimiento del caso, y la realización de los peritajes criminalísticos de rigor a los agentes del Área Científica de la Agencia de Investigación Criminal.