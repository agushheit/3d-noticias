---
category: Agenda Ciudadana
date: 2022-03-02T11:52:01-03:00
thumbnail: https://assets.3dnoticias.com.ar/epe.png
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Eco365
resumen: Cómo quedará el nuevo esquema de tarifas con el recorte de subsidios que
  aplicará el Gobierno
title: Cómo quedará el nuevo esquema de tarifas con el recorte de subsidios que aplicará
  el Gobierno
entradilla: 'Se trabaja en un proyecto para que los usuarios de mayor poder adquisitivo
  paguen los valores plenos, mientras que en otros casos habrá topes a las actualizaciones '

---
![](https://www.rosario3.com/__export/1586461856736/sites/rosario3/img/2020/04/09/tarifas.jpg_1572130063.jpg)El Gobierno trabaja en un nuevo esquema tarifario con topes de incremento de acuerdo al coeficiente de variación salarial y quita de subsidios al 10% de los usuarios de mayor poder adquisitivo.

En línea con las negociaciones con el Fondo Monetario Internacional (FMI) y la necesidad de reducir el déficit fiscal, el Gobierno reflotó un proyecto de Sergio Massa al respecto, que había sido aprobado y vetado en 2018, y al que ahora busca actualizar.

La idea es que las subas tengan como tope un 80% del coeficiente de variación salarial, para de este modo quedar por debajo de la inflación, pero no tan rezagadas. De esta manera, al 20% de ajuste previsto para este año se le podría aplicar otro 22%.

A su vez, se apunta a reducir gradualmente los subsidios a usuarios comerciales y mejorar la progresividad de los mismos a los hogares a través de un sistema de segmentación, con el objetivo de bajar un 0,6% del PBI en subsidios.

Los recortes del Ente Nacional de Regulación de la Energía (ENRE) se llevarán adelante no sólo sobre el Área Metropolitana de Buenos Aires, sino también sobre otras nueve áreas urbanas (entre las que se incluye Rosario), en principio siempre sobre los usuarios de mayores recursos. Traducido: habría un incremento para todos del 40%, mientras que en el caso de los que se queden sin subsidios alcanzaría el 100%.