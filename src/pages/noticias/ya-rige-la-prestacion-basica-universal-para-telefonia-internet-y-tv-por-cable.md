---
category: Agenda Ciudadana
date: 2021-01-02T10:21:19Z
thumbnail: https://assets.3dnoticias.com.ar/01012021-Enacom.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: El Litoral'
resumen: Ya rige la Prestación Básica Universal para telefonía, internet y TV por
  cable
title: Ya rige la Prestación Básica Universal para telefonía, internet y TV por cable
entradilla: El Ente Nacional de Comunicaciones (Enacom) informó que ya está en vigencia
  la Prestación Básica Universal y Obligatoria (PBU) para servicios de telefonía móvil,
  fija, Internet y TV por cable.

---
Esta prestación se determinó a través del Decreto de Necesidad y Urgencia (DNU) 690/20, que fijó un servicio mínimo de 150 pesos para telefonía móvil.

El decreto tuvo como objetivo garantizar el derecho humano de acceso a las Tecnologías de la Información y las Comunicaciones (TIC) por cualquiera de sus plataformas, lo cual requirió de la fijación de reglas por parte del Estado para asegurar un uso equitativo, justo y a precios razonables.

Podrán optar por adherirse a la PBU las personas beneficiarias de la Asignación Universal por Hijo (AUH) y la Asignación por Embarazo, así como también sus hijos e hijas de entre 16 y 18 años, y miembros de su grupo familiar; beneficiarios y beneficiarias de Pensiones No Contributivas que perciban ingresos mensuales brutos no superiores a dos (2) salarios mínimos vitales y móviles.

El beneficio se extiende a clubes de barrio, asociaciones de bomberos voluntarios y entidades de bien público.

La PBU «persigue el horizonte de la expansión de derechos establecidos por el Gobierno nacional, como lineamiento general para asegurar que todos los argentinos y argentinas gocen de las mismas oportunidades», indicó el Ente.

«Desde el comienzo de su gestión, **Enacom estableció que la comunicación es un derecho humano básico**, y el acceso a las TIC, un servicio esencial para la construcción de ciudadanía», concluyó el comunicado.