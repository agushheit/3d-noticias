---
category: La Ciudad
date: 2021-06-12T06:00:11-03:00
thumbnail: https://assets.3dnoticias.com.ar/FISH.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Municipalidad facilita la venta de pescado de mar a precios populares
title: La Municipalidad facilita la venta de pescado de mar a precios populares
entradilla: En el marco de Mercado Santafesino, este sábado se podrán adquirir productos
  de calidad en la plaza Pueyrredón. El miércoles, en tanto, será en la feria de Las
  4 Vías.

---
La Municipalidad informa que tras un acuerdo alcanzado con la Dirección Nacional de Políticas Integradoras del Ministerio de Desarrollo Social de la Nación, los vecinos y vecinas de la capital provincial tendrán la posibilidad de adquirir pescado de mar en ferias populares de la ciudad. Esta iniciativa, enmarcada en el Mercado Santafesino, estará disponible este sábado 12 de junio y el próximo miércoles 16 del mismo mes.

La intención es acercar productos de calidad a precios accesibles. De esta forma, se complementan los productos de la economía social y popular que se ofrecen en las ferias de la ciudad.

La secretaria de Integración y Economía Social del municipio, Ayelén Dutruel, explicó que esta iniciativa “se enmarca en varias líneas que venimos construyendo en conjunto, porque creemos importante, tal como nos lo marca el intendente Emilio Jatón, generar políticas transversales e intersectoriales que profundicen la articulación entre los distintos niveles del Estado. Esto genera mejores condiciones para los trabajadores y las trabajadoras de la economía social y popular de la ciudad”.

El primer punto será la Feria de la Plaza Pueyrredón (Balcarce y Alberdi) que se desarrolla los sábados de 8 a 13 horas. Posteriormente, el miércoles 16 a la misma hora, será el turno de la feria de Las 4 Vías (Mariano Comas y Facundo Zuviría). Cabe destacar que la comercialización de pescado será hasta agotar el stock disponible para cada día.

Entre los principales productos, se ofrecerá merluza a $ 430 el kilo, lomo de atún a $ 650 el kilo, pollo de mar a $ 600 el kilo, pejerrey a $ 650 el kilo y corvina a $ 300 el kilo, entre otros. Y un dato relevante es que podrá abonar con todos los medios de pago, incluida la Tarjeta Alimentar.

El asistente ejecutivo de Mercados y Ferias, Emilio Scotta, destacó el arribo de esta propuesta a la ciudad, “a partir de la articulación con el Ministerio de Desarrollo Social de la Nación y el Plan Federal de Ferias. Además, indicó que se trata de “pescado de mar traído directamente desde Mar del Plata, por lo que “se trata de productos de calidad que fortalecen la economía regional”.

Del mismo modo, mencionó que al tratarse de un puesto móvil, puede instalarse a cierta distancia del espacio habitual de feria, para evitar la aglomeración de gente. Y acotó que la premisa es mantener las medidas de prevención y cuidado que estamos transmitiendo desde la Municipalidad en esos espacios de feria, de modo que puedan seguir funcionando.

“Lo importante es seguir complementando la oferta de alimentos que brindamos en nuestras ferias y no generando competencias desleales sino, por el contrario, aspirando a la integración del comercio social, solidario y cooperativo que hay en la ciudad y el país también”, concluyó Scotta.

**Mercado Santafesino**

Es una propuesta que se fortaleció a partir de la pandemia por el Covid-19, ante las dificultades de vender y comercializar productos elaborados por trabajadores de la economía social. En ese marco, la Municipalidad diseñó alternativas de acompañamiento para que la producción y venta de diferentes insumos continúe y esas familias puedan seguir contando con un sustento.

Dutruel recordó que “el Mercado Santafesino es una política con una mirada integral que vincula distintas perspectivas y un diálogo permanente con los trabajadores y las trabajadoras de la economía social y popular de la ciudad, generando un mercado local y mejores condiciones para estos sectores en lo que tiene que ver con todo el proceso de su economía, desde la producción hasta la comercialización”.

Además, subrayó que la propuesta “encuentra a los distintos actores: ferias de la economía popular y cooperativas de la ciudad. Nosotros, como Estado municipal, promovemos el encuentro de esos trabajadores, la generación de mejores condiciones y la multiplicación de ofertas en el marco de las ferias y los mercados de la economía social en la ciudad”, concluyó la secretaria.