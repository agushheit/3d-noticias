---
category: La Ciudad
date: 2021-09-13T06:15:30-03:00
thumbnail: https://assets.3dnoticias.com.ar/MUJERESCONCEJO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Rumbo al Concejo: tres mujeres entre las más votadas'
title: 'Rumbo al Concejo: tres mujeres entre las más votadas'
entradilla: Con casi el 80 por ciento de las mesas escrutadas, Mondino, única lista
  en su interna, alcanzaba los 14.035 votos, mientras Juntos por el Cambio rozaba
  los 40 mil.

---
Cerca de la medianoche y con el 80 por ciento de las mesas escrutadas, la candidata oficialista **María Mondino** por el Frente Amplio Progresista, llegaba a los 14.353 votos y se afianzaba como la candidadta más votada de las primarias.

Sin embargo, los frentes de Juntos por el Cambio y Frente de Todos, quienes tuvieron cinco y seis listas en sus internas respectivamente, alcanzaban a duplicarla en votos.

**Adriana Molina**, lideró la interna de Juntos por el Cambio con 13.774 votos; seguido por Marcucci con 11.114 sufragios. Más atrás: Ceresola (6.370) Carlos Suárez (5.514) Nicolas Rabosto (2.694) En total, el frente sumaba 39.466 sufragios.

En tanto que en el Frente de Todos, **Jorgelina Mudallel**, con 9.702 votos se imponía en la interna. Seguido por Rossi con 6.600. Seguido por: Aeberhard (5.030) Violeta Quiroz (4.377) Lucas Maguid (3.233) Ana Ingaramo (2.797) y Mariana Bergallo (889). En total, el frente llegaba a los 32.628 sufragios.

![](https://assets.3dnoticias.com.ar/PORCETANJE.jpg)