---
category: La Ciudad
date: 2021-02-10T06:23:49-03:00
thumbnail: https://assets.3dnoticias.com.ar/alumbrado.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad de Santa Fe
resumen: La Municipalidad licitó la compra de materiales eléctricos por 17 millones
  de pesos
title: La Municipalidad licitó la compra de materiales eléctricos por 17 millones
  de pesos
entradilla: Este lunes se abrieron los sobres con las ofertas para la adquisición
  de luminarias LED, mientras que la semana pasada se concretó un acto similar para
  materiales eléctricos varios.

---
Durante todo el primer semestre de 2021, la Municipalidad desarrollará el Plan de Iluminación previsto por el proyecto de Presupuesto para el año en curso, con la premisa de recuperar el alumbrado existente, incluyendo el mantenimiento de la red, y la nueva iluminación para los barrios que actualmente no poseen un servicio eficiente.

En ese marco, este lunes se abrieron los sobres con las ofertas presentadas para la licitación correspondiente a la adquisición de artefactos LED, con un presupuesto oficial de $ 6.182.500. En la oportunidad se presentaron seis propuestas: STRAND S.A., que cotizó dos ofertas, por $ 3.321.250 y $ 2.942.750; TRIA-VIAL TECH S.A., por $ 4.501.200; IGNIS LIGHTING S.R.L., por $ 3.865.000; LUG ARGENTINA S.A., por $ 5.663.102; ELECTROMECÁNICA TACUAR S.R.L., por $ 3.722.500; y RADIO LUX S.A., la cual quedó desestimada por no adjuntar garantía de oferta.

Este acto se suma a uno similar, desarrollado la semana pasada, para la adquisición de materiales eléctricos varios, con un presupuesto oficial de $ 11.262.800. En la ocasión se presentó una sola oferta, correspondiente a ELECTROMECANICA TACUAR S.R.L., por un monto de $ 8.830.790.

La secretaria de Hacienda del municipio, Carolina Piedrabuena, indicó que “en el marco de las políticas públicas que definió el intendente Emilio Jatón para este ejercicio 2021, se inscribe la revalorización y la recuperación del espacio público”. En ese sentido, mencionó la importancia de las dos licitaciones de materiales eléctricos, teniendo en cuenta la ejecución del Plan de Iluminación que se lleva adelante y se extenderá durante los primeros meses del año.

Respecto de la licitación de este lunes, señaló que todas las ofertas válidas “están por debajo del presupuesto oficial, por lo que en menos de un mes vamos a comenzar con la ejecución de las obras previstas”. Del mismo modo, acotó que “a la compra de materiales para la reposición de luminarias LED -en este caso para el bulevar, la avenida Alem y otras importantes arterias de la ciudad- se suma la adquisición de materiales eléctricos para la instalación de sistemas de iluminación completa en barrios que actualmente poseen un servicio deficiente”.

**Plan de iluminación**

Prevé dos tipos de intervenciones: la actualización tecnológica, mediante el recambio de luces de sodio por LED, y el nuevo sistema de alumbrado con columnas y equipos LED.

Respecto del primero de los puntos, ya está en marcha la licitación para la compra de 250 artefactos LED que comenzarán a instalarse en el bulevar Gálvez para recambiar las existentes y solucionar el problema actual que hace que varias luminarias no enciendan. Posteriormente se continuará por calle Rivadavia y avenida Alem, donde la situación es similar. Esto permitirá reunir aparatos suficientes para ir sumando otros corredores, tal es el caso del Parque Garay y avenida Gorriti, entre otros.

Sobre las intervenciones de nueva iluminación, la primera etapa contempla cinco sectores que serán beneficiados, al menos en un principio: Scarafía, donde se instalarán 100 columnas con luminarias LED; Liceo Norte, donde se instalarán 61 columnas; Santa Marta, con 84 columnas; Siete Jefes, cuyo proyecto consiste en colocar 310 columnas; y Guadalupe, al noreste, donde se instalarán 405 columnas con sus equipos LED, cableado y tableros. A esto se suman las inversiones en columnas con artefactos LED en barrio Roma, Fomento 9 de Julio, y en el entorno de los hospitales de la ciudad.

En paralelo, se trabaja en reparar todo lo que actualmente no funciona tanto en materia de equipos, como sobre sectores completos. Se trata de acciones básicas para el mantenimiento, como es el caso de la reparación del artefacto, el cableado, los tableros o las células fotoeléctricas.