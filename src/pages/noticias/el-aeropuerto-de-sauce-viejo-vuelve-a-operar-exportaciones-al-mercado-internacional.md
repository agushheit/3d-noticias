---
category: La Ciudad
date: 2021-09-10T06:00:12-03:00
thumbnail: https://assets.3dnoticias.com.ar/AEROPUERTO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: El aeropuerto de Sauce Viejo vuelve a operar exportaciones al mercado internacional
title: El aeropuerto de Sauce Viejo vuelve a operar exportaciones al mercado internacional
entradilla: Tras más de 15 años sin operar tránsito internacional. Si bien existen
  algunas limitaciones por el momento para las cargas, indicaron que "se trata de
  un verdadero hito para la provincia y la región".

---
El Ministerio de Infraestructura, Servicios Públicos y Hábitat, a través de la Secretaría de Empresas y Servicios Públicos, informó que el Aeropuerto Sauce Viejo (ASV) retoma el servicio de exportaciones con vuelos directos para realizar operaciones de comercio exterior en el mercado internacional.

Al respecto, la ministra de Infraestructura, Servicios Públicos y Hábitat, Silvina Frana, destacó que "esta es otra muestra concreta de la decisión política del gobernador Omar Perotti para reactivar los entes aerocomerciales y generar mayor competitividad y empleo para los santafesinos. Esta será una herramienta de exportaciones que sumará competitividad para las pymes de la región, no sólo de la capital provincial, sino también de las zonas aledañas a las rutas 19, 34, 6 y 70, por mencionar algunas".

El trabajo se realizó en conjunto con la Cámara de Comercio Exterior de Santa Fe, Servicios Portuarios (Depósito Fiscal del Puerto de Santa Fe), Grupo SP (ROS AIR CARGO S.A), y representantes de Aerolíneas Argentinas en Rosario, Santa Fe y Paraná.

En este sentido, la directora del ASV, Silvana Serniotti, resaltó que “el trabajo conjunto entre el sector público y el privado ha permitido generar las condiciones para llevar adelante la primera exportación aérea desde el Aeropuerto Sauce Viejo, después de más de 15 años sin operar tránsito internacional”.

“Se trata de un verdadero hito para la provincia y la región, ya que logra tender puentes directos con los mercados más competitivos del mundo, agilizando la operatoria aduanera porque permite a los clientes hacer aduana de origen o destino en el depósito fiscal Servicios Portuarios, del Puerto de Santa Fe”, destacó Serniotti.

**Ruta para las exportaciones e importaciones**

Para la exportación, se puede hacer Aduana de origen en el depósito Fiscal de Servicios Portuarios y se realiza un tránsito aduanero hacia el aeropuerto de Sauce Viejo, donde la mercadería se embarca a Aeroparque, haciendo tránsitos a Santiago de Chile, Lima, San Pablo, Montevideo y otros países limítrofes. También se puede conectar por vía terrestre Aeroparque y Ezeiza con un Tránsito Aduanero (TLAT) y salir a todos los destinos disponibles del resto del mundo, desde allí con vuelos directos de Aerolíneas Argentinas y conexiones interlineales.

Para la importación, también se puede llegar a Ezeiza y Aeroparque, y desde allí conectar con Sauce Viejo, haciendo un tránsito hacia Servicios Portuarios, donde se nacionaliza la mercadería. La limitación por el momento es carga no palletizada, bultos sueltos de 60x40 centímetros (medida que soporta el scanner del Aeropuerto) y peso menor a 80 kilogramos por bulto. Con un máximo de aproximadamente entre 1500 y 3000 kilogramos por vuelo, ya que los aviones que llegan a Sauce Viejo son Boeing 737 y Embraer 190.