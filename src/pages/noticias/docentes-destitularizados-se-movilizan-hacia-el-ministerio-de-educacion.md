---
category: La Ciudad
date: 2021-02-04T03:53:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/docentes.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Docentes destitularizados se movilizan hacia el Ministerio de Educación
title: Docentes destitularizados se movilizan hacia el Ministerio de Educación
entradilla: El reclamo será este jueves 4 de febrero a las 10:30 frente a la sede
  del Ministerio de Educación en la ciudad de Santa Fe y luego marcharán hacia Casa
  de Gobierno.

---
A días de su asunción, el gobernador Omar Perotti emitió un decreto donde se autoimpuso un plazo de 60 días para revisar los procesos de titularización (entre otros procesos administrativos) que se sustanciaron en el último año de la gestión anterior. Ese tiempo de revisión de los procesos venció en febrero de 2020. A casi un año de ese decreto y con todos los tiempos vencidos y en contexto de pandemia, el Ministerio de Educación de Santa Fe revocó tres expedientes de titularización docente y llamó a nuevo concurso “conforme a la normativa vigente” dejando a más de 500 docentes en situación de precariedad.

Uno de esos expedientes era el de los docentes del EEMPA 1330. Este establecimiento educativo surgió en 2015 con el antiguo programa “Vuelvo a Estudiar” y en el año 2018 fue transformado y declarado escuela EEMPA provincial según Decreto N° 3446/18. Fue la única institución educativa que no tuvo que modificar su cronograma de clases durante la pandemia y pudo funcionar normalmente según el calendario escolar. Su modalidad de trabajo es de 89% virtual y 11% presencial, con un encuentro presencial mensual (generalmente día sábado) y una clase de consulta presencial semanal.

Actualmente cuenta con 3000 alumnos activos y más de 2000 egresados desde sus inicios en toda la provincia de Santa Fe.

Los docentes que comenzaron su recorrido en 2015, lograron concursar los cargos en los cual se desempeñaban con más de 2 años de antigüedad y lograr la tan ansiada estabilidad laboral durante el 2019. Titularización que se realizó en el ámbito de las paritarias docentes, con reconocimiento del Poder Ejecutivo y aval del gremio docente.

Estos docentes hoy retrocedieron en sus carreras. Con grandes incertidumbres y en algunos casos con patologías propias de la situación de disgusto y depresión. En el mes de noviembre tomaron conocimiento de la “revisión de expedientes” a través de los medios de comunicación.

“El gremio rápidamente nos ofreció todo su apoyo, nos puso a disposición sus abogados y presentó un recurso de amparo porque la titularización revestía carácter de ley al ser realizada en el marco del acuerdo paritario” relató a 3D Noticias una docente damnificada.

“Durante el mes de enero de 2021- en el medio de la licencia anual obligatoria- llegaron los telegramas informando la pérdida de la condición de titular y el cambio de situación de revista a interinos algo que corroboramos al ingresar a nuestros legajos online” comentó y aclaró que la situación interina es similar a la de un reemplazante, “la patronal puede prescindir de los servicios en cualquier momento”.

Por último, la docente sostuvo: “entendemos que el gobierno entrante está en todo su derecho de revisar los concursos anteriores a su gestión, pero en este caso en particular está vulnerando nuestros derechos como trabajadores. No sólo irrumpieron un acuerdo paritario, sino que están atentando contra la salud psíquica y emocional de compañeras y compañeros que somos sostén de familia. Nos precarizaron y, lejos de dar tranquilidad en semejante contexto pandémico, nos hunden en una absoluta incertidumbre sobre nuestra continuidad laboral.”