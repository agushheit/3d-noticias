---
category: Estado Real
date: 2021-05-17T06:00:08-03:00
thumbnail: https://assets.3dnoticias.com.ar/SEGURIDAD.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Provincia invirtió 7.400 millones de pesos para reforzar la seguridad
  en todo el territorio Santafesino
title: La Provincia invirtió 7.400 millones de pesos para reforzar la seguridad en
  todo el territorio Santafesino
entradilla: Desde comienzos de 2020 a mayo de 2021 se incorporaron vehículos, armamento,
  tecnología y armas para modernizar a la fuerza policial.

---
El gobierno provincial, a través del Ministerio de Seguridad a cargo de Jorge Lagna, invirtió cerca de 7.400 millones de pesos en equipamiento, armamento y modernización edilicia para reforzar la seguridad en todo el territorio provincial. Este plan se lleva adelante desde inicios del año 2020 y continuará a lo largo del 2021 con el objetivo de reestructurar a la fuerza provincial y dotarla de insumos de última generación, que perfeccionen el trabajo policial en las calles de la provincia. Asimismo, hubo un incremento en la cantidad de efectivos policiales en calle, el egreso de nuevos efectivos policiales y el ingreso de nuevos cadetes al ISEP, que se traducen en un refuerzo significativo para los patrullajes y operativos.

Al respecto, el ministro de Seguridad, Jorge Lagna indicó que “el Plan Integral de Seguridad es solo una parte de lo que estamos llevando adelante en materia de seguridad. Creemos que la seguridad se debe trabajar de una manera integral, porque una política de seguridad no es solamente más equipamiento, más armamento, más integrantes de la fuerza; es también una visión donde el Estado esté presente en todos sus estamentos, para generar más inclusión y oportunidades para todos”.

“Desde los inicios de la gestión nos comprometimos a modernizar nuestras fuerzas de seguridad y lo estamos cumpliendo gracias al apoyo del gobernador Omar Perotti ha diseñado. Tenemos licitaciones en marcha y una inversión cercana a los 7400 millones de pesos para que la seguridad de Santa Fe tenga el nivel tecnológico para estar a la altura de las mejores del país”, destacó Lagna.

**Plan integral de inversión policial en la provincia**

En el marco del plan de inversión que lleva adelante la provincia, se incorporaron un total de 220 camionetas, de las cuales 200 fueron 4x2; en tanto que los 20 restantes fueron 4x4. Este incremento en la flota vehicular policial significó una inversión de $477.584.400.260. Asimismo, se licitaron un total de 360 nuevas camionetas: 120 4x2; 60 4x4; y 180 compactas, por un monto total de $1.395.180.000 que pronto serán distribuidas en la provincia. Además, se realizará una nueva licitación por 50 camionetas por un monto de $250.000.000.

Por otro lado, se adquirieron 260 motos de distintas marcas y cilindradas, para perfeccionar el trabajo policial. Se trata de 50 unidades de Kawasaki Versys 650; 105 unidades de Kawasaki Versys 300; y 105 unidades de Corven Touring 250, con una inversión total de más de 4 millones de pesos.

Durante este tiempo el Programa Integral de Seguridad Policial también adquirió 15.000 chalecos antibalas de Fabricaciones Militares, que están siendo entregados; tuvo lugar la incorporación de un sistema moderno de armas; 26 minibuses de traslado de personal; canes y personal táctico; seis vehículos de carga; un millón de municiones de varios calibres; y 100 cámaras para patrulleros, además de equipamiento informático moderno, indispensable para investigación criminal.

**Centros de operaciones policiales**

Dentro de la Estrategia de Seguridad Preventiva para Grandes Centros Urbanos se establecieron siete núcleos de carácter analítico-operacional denominados Centros de Análisis y Operaciones Policiales (COP), ubicados en las ciudades de Santa Fe, Rosario, Rafaela, Reconquista, San Lorenzo, Venado Tuerto y Rosario; y un Centro Provincial de Análisis y Operaciones Policiales (COP Central) con sede también en Rosario.

Dichos COP estarán conformados funcionalmente por tres componentes: Oficina de Análisis Criminal, Oficina de Operaciones Policiales y Oficina de Evaluación Operacional Policial.

La Oficina de Análisis Criminal es la encargada de reunir, sistematizar, analizar información y producir análisis criminal, el cual incluye una serie de estudios destinados al conocimiento del fenómeno criminal en función de la prevención de las conductas delictivas.

Cabe destacar que en los COP se invertirán gran parte de los 3 mil millones de pesos enviados por el gobierno nacional. Cada COP comprende una integración de sistemas tecnológicos y de comunicaciones, tales como aquellos aplicativos que recaban información criminal en la provincia (las denuncias al 911, comisarías y Centros Territoriales de Denuncias); el Sistema de Cámaras de Videovigilancia de provincia, municipios y comunas; además de los sistemas de GPS, información generada por drones y cámaras portátiles personales y en vehículos.

De tal modo, el COP permitirá planificar, dirigir y evaluar la operación policial en todos sus aspectos. Estos centros tendrán una inversión total de $2.837.948.600 millones de pesos.

En este sentido, el ministro Jorge Lagna remarcó que “una obra de estas dimensiones va a cambiar la vida de toda la provincia y va a mejorar la calidad de los vecinos y vecinos y modernizar los recursos humanos y tecnología para de la Policía provincial. Esta obra va a ser testigo del salto tecnológico importante, que va a dar la seguridad en Santa Fe”.

**Más efectivos en tareas operativas**

Además, el Plan Integral de Seguridad implica una reorganización de la estructura policial para sacar los policías de funciones administrativas y llevarlos a la calle para reforzar los patrullajes. Más de 1700 agentes se distribuyeron en las zonas con mayores incidencias. También implica la incorporación de nuevos efectivos, egresados del Instituto de Seguridad Pública y el ingreso de mujeres a las filas de la fuerza.

El Instituto de Seguridad Pública (ISeP) es una herramienta esencial para seleccionar y formar a los aspirantes a ingresar a la Policía de la provincia, y para capacitar y actualizar al personal policial que integra la fuerza. Actualmente el ISEP cuenta con tres delegaciones, con sede en Santa Fe, Rosario y Murphy. Pero en el futuro próximo contará con dos sedes más en Reconquista y Rafaela.

El funcionamiento de estas nuevas instituciones en el sur provincial permitirá generar recursos humanos con vínculo de arraigo en la región, mejorando las condiciones laborales de los efectivos policiales que hoy en día son provenientes, en su mayoría, del norte santafesino. Lagna indicó: “Desde el año pasado venimos trabajando en dos aspectos importantes: la capacitación constante en la Policía y las mejoras en la infraestructura de las sedes. Duplicamos el ingreso de aspirantes al Instituto y, en el marco de la igualdad de género que pregonamos en la provincia, debemos destacar la incorporación de mil mujeres a las filas de nuestras fuerzas provinciales. Las mujeres policías tienen un rol muy importante, con mayor protagonismo en la toma de decisiones y en los cargos jerárquicos".