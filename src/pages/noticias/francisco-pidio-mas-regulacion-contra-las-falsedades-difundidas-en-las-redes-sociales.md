---
category: Agenda Ciudadana
date: 2021-08-28T06:00:31-03:00
thumbnail: https://assets.3dnoticias.com.ar/PANCHO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Francisco pidió más regulación contra "las falsedades difundidas en las redes
  sociales"
title: Francisco pidió más regulación contra "las falsedades difundidas en las redes
  sociales"
entradilla: El Papa recibió este viernes a una delegación de la Red Internacional
  de Legisladores Católicos, donde pidió que "asuman la tarea de una labor seria y
  una completa reflexión moral...".

---
El papa Francisco pidió este viernes a un grupo de legisladores católicos de todo el mundo que haya más regulación contra "las falsedades difundidas a través de las redes sociales", al tiempo que destacó las "herramientas de la política" para "proteger la dignidad humana cuando se ve amenazada".

"Las herramientas de la política y la regulación permiten a los parlamentarios proteger la dignidad humana cuando se ve amenazada", planteó el pontífice este viernes al recibir en el Vaticano a una delegación de la Red Internacional de Legisladores Católicos.

En ese marco, el Papa ejemplificó con "el flagelo de la pornografía infantil, la explotación de datos personales, la ataques a infraestructuras críticas como hospitales, a falsedades difundidas a través de las redes sociales".

"Una legislación cuidadosa puede y debe guiar la evolución y aplicación de la tecnología para el bien común", agregó luego Jorge Bergoglio.

"Por tanto, los animo encarecidamente a que asuman la tarea de una labor seria y una completa reflexión moral sobre los riesgos y oportunidades inherentes al progreso científico y tecnológico, para que la legislación y las normas internacionales que las regulan pueden centrarse en promover el desarrollo humano integral y la paz, más que el progreso como un fin en sí mismo", añadió Francisco.

"Las innovaciones pueden atentar contra la dignidad del ser humano"

Para el Papa, de todos modos, "no se trata de frenar el progreso tecnológico".

En ese marco, el obispo de Roma lamentó que a veces "las innovaciones pueden atentar contra la dignidad del ser humano" cuando son guiadas "por sí mismas y por las fuerzas del mercado sin las oportunas directrices impresas por las asambleas legislativas y otras autoridades públicas guiadas por el sentido de responsabilidad social".

"En una era de disturbios políticos y polarización, los parlamentarios y los políticos en general no siempre son tenidos en gran estima", enmarcó el pontífice.

Al tiempo que se preguntó y los animó; "Esto no es nuevo para ustedes. Sin embargo, ¿qué llamada existe más alta que la de servir al bien común y dar prioridad al bienestar de todos, antes que al beneficio personal?".

"Su objetivo debe ser siempre este, porque una buena política es indispensable para la fraternidad universal y la paz social", sentenció luego ante el grupo con base en Viena y coordinado por el cardenal austriaco Christoph Schönborn.