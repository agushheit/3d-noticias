---
category: Agenda Ciudadana
date: 2021-12-13T01:13:25-03:00
thumbnail: https://assets.3dnoticias.com.ar/casa-de-gobierno.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Diario Uno
resumen: Con la agenda de la seguridad y la justicia federal, Perotti recibe a los
  legisladores nacionales por Santa Fe
title: Con la agenda de la seguridad y la justicia federal, Perotti recibe a los legisladores
  nacionales por Santa Fe
entradilla: El gobernador convocó a los tres senadores y los 19 diputados nacionales
  para este lunes. La seguridad y mejorar la estructura de la justicia federal en
  la provincia serán los ejes del encuentro

---
Este lunes el gobernador Omar Perotti recibirá a los diputados y senadores nacionales por Santa Fe. La reunión está prevista a media mañana en Casa de Gobierno y el eje del encuentro será coordinar una agenda de temas donde la seguridad y la necesidad de ampliar la cantidad de juzgados federales en la provincia, temas prioritarios en la coyuntura de la provincia.

Luego de las últimas elecciones, el gobernador concretó un encuentro con los presidentes de bloque de la Cámara de Diputados de la provincia. Mientras que ahora convocó a los tres senadores nacionales y a los 19 diputados nacionales.

El crecimiento de la violencia ligada al narcotráfico y el lavado de dinero en Rosario y Santa Fe y los hechos de inseguridad en las principales ciudades de la provincia obligan a la política a establecer bases de diálogo. La provincia de Santa Fe viene reclamando desde hace tiempo la creación de juzgados y cargos de fiscales y jueces federales para afrontar el crecimiento del narcotráfico en territorio santafesino. Para lograr que eso se transforme en una prioridad, los dirigentes políticos de Santa Fe tienen que dar señales claras de unidad en el reclamo.

Además, hay varios legisladores nacionales que quieren poner sobre la mesa el tema del fallo de la Corte Suprema de Justicia que ordena a Nación pagar la deuda histórica con Santa Fe. Concretamente quieren saber cómo será la negociación con el Gobierno nacional para cobrar y cuál será el destino que se le dará a los 86.000 millones de pesos.

Al encuentro están invitados los senadores nacionales Carolina Losada, Dionisio Scarpín (Juntos por el Cambio) y Marcelo Lewandowski. Asimismo los 19 diputados nacionales por Santa Fe. Federico Angelini, Mario Barletta, Laura Castets, Gabriel Chumpitaz, Germana Figueroa Casas, Ximena García, Luciano Laspina, Juan Martín, José Carlos Núñez, María Victoria Tejeda, por Juntos por el Cambio; Enrique Estévez y Mónica Fein, del Frente Amplio Progresista; Marcos Cleri, Germán Martínez, Vanesa Massetani, Magalí Mastaler, Roberto Mirabella, Alejandra Obeid, Eduardo Toniolli, del Frente de Todos.

**Antes con Cafiero**

El gobernador Omar Perotti estará este lunes, a las 9.30 en un acto junto al canciller, Santiago Cafiero, y su par de Entre Ríos, Gustavo Bordet. En ese encuentro Cafiero presentará la iniciativa Cancillería Federal, un nuevo eje de la política exterior comercial de la Argentina para promover las exportaciones y los nuevos mercados para Pymes provinciales.

El acto se hará en el marco del encuentro regional del Litoral del programa Desafío Exportador. La sede será el Centro de Convenciones del Hotel Los Silos de la ciudad de Santa Fe.