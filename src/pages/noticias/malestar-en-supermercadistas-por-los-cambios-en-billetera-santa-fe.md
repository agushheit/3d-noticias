---
category: La Ciudad
date: 2022-01-08T06:15:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/billeteracambios.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Malestar en supermercadistas por los cambios en Billetera Santa Fe
title: Malestar en supermercadistas por los cambios en Billetera Santa Fe
entradilla: 'Desde el lunes 10 de enero Billetera Santa Fe funcionará en los súper
  solo lunes, martes y miércoles. Los privados dijeron que les notificaron por WhatsApp

'

---
A partir del lunes 10 de enero se avizoran cambios en la app de pago virtual de la provincia "Billetera Santa Fe". Las modificaciones resueltas por el Ejecutivo plantean como principal cambio el recorte de los días habilitados para usar la Billetera en supermercados, almacenes y farmacias, pudiendo pagar por este medio solo los días lunes, martes y miércoles.

La medida no cayó para nada bien en los supermercadistas locales, puesto que no hubo comunicación formal con los empresarios previo al anuncio oficial que aún se hace esperar. En diálogo con UNO Santa Fe el referente de la Cámara de Supermercados y Autoservicios de Santa Fe (Camsafé) Gabriel Silva afirmó que la medida les fue comunicada "por WhatsApp, ni siquiera se trató de una comunicación formal".

Consultado por cómo cayó la resolución en el sector, Silva manifestó: "Esta es una decisión unilateral de la provincia que dispone todos los cambios en el sistema de Billetera Santa Fe y nosotros no tenemos absolutamente nada que ver. No es algo deseado por los supermercadistas y autoservicios".

"Lamentablemente vamos a tener que acatarlo con un tiempo absolutamente corto, sin comunicación. La provincia no se ha movido bien en relación a este tema en absoluto", continuó el empresario referente de supermercadistas.

Cabe destacar que ante las consultas de UNO hacia fuentes de las carteras provinciales de Economía y Producción, desde ambos ministerios declararon desconocer la medida, lo que luego fue confirmado a este medio por los privados. La idea de los supermercadistas es buscar al menos que la resolución no inicie desde el lunes 10, aunque destacaron que hubo negativas de parte de la provincia para concertar un encuentro previo a la fecha.

Sobre esto, el representante de Camsafé afirmó que el gobierno "nunca adelantó la medida". Y agregó: "Hemos pedido una reunión y nos plantearon que esa reunión se haría la semana que viene, cuando la reunión debería ser previa a la medida". Hasta el momento, en los encuentros que mantuvieron entre representantes de la Secretaría de Comercio del Ministerio de la Producción y los actores privados no se había deslizado la posibilidad de estas modificaciones.

**Las modificaciones a partir del 10 de enero**

De acuerdo con la resolución firmada el 31 de diciembre por el ministro de Economía, Walter Agosto, la devolución del 30% se mantendrá en alimentos y bebidas, farmacias, bares y restaurantes, indumentaria, calzado y marroquinería, juguetería y librería, turismo, mueblería y colchonería y electrónica, según afirma La Capital. Mientras tanto, bazar y artículos de limpieza tendrán un reintegro del 20% y en las compras en los vinotecas y licorerías el beneficio será del 15%.

Bares y restaurantes seguirán contando con el reintegro todos los días, la modificación será que en supermercados, autoservicios y farmacias tendrán la devolución solamente en compras realizadas entre lunes y miércoles, mientras que en el resto de los rubros será solamente lunes y miércoles.

La resolución 752 remarca que "la recepción por parte de ciudadanos y comercios ha superado las expectativas generando un círculo virtuoso en la economía provincial, paliando los efectos económicos perjudiciales de la pandemia".