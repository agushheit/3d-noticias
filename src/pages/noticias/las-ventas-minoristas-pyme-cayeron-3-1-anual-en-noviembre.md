---
category: Agenda Ciudadana
date: 2022-12-05T07:47:10-03:00
thumbnail: https://assets.3dnoticias.com.ar/came.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: Las ventas minoristas pyme cayeron 3,1% anual en noviembre
title: Las ventas minoristas pyme cayeron 3,1% anual en noviembre
entradilla: Se cumplen cinco meses consecutivos con las ventas en baja.

---
Las ventas minoristas pymes descendieron 3,1% en noviembre frente al mismo mes del año pasado, medidas a precios constantes. Se cumplen cinco meses consecutivos con las ventas en baja, aunque en los once meses del año todavía acumulan un crecimiento (1,4% frente a igual periodo 2021).

Sólo **Farmacia y Perfumería** creció en la comparación anual, el resto se retrajo. En la comparación mensual, las ventas se mantuvieron sin cambios.

En el balance general, noviembre no fue un mes bueno. Los comercios continuaron sintiendo el menor poder adquisitivo del consumidor generando cierta preocupación. La cercanía de fin de año trae una moderada expectativa de crecimiento del consumo, aunque divergente entre los distintos rubros. Algunos empresarios consultados comentaron que tienen dudas sobre cómo manejar los stocks para las fiestas: comprar para evitar faltantes o ser prudentes para evitar sobrantes.

Así surge del Índice de Ventas Minoristas Pymes de la Confederación Argentina de la Mediana Empresa (CAME), elaborado en base a su relevamiento mensual entre 1.157 comercios minoristas de todo el país, realizado entre el 1 y 3 de noviembre.

**1) Alimentos y Bebidas**. Las ventas cayeron 1,3% anual en noviembre, se mantuvieron sin cambios en la comparación mensual y acumulan un alza de 2,8% en los once meses del año (frente a igual periodo del 2021).

A pesar que los aumentos de precios se desaceleraron en el mes, los comercios continuaron sintiendo el menor poder adquisitivo de la gente. El Mundial de fútbol contribuyó, en parte, a mantener las ventas relativamente activas. Desde septiembre 2022, se registran valores interanuales negativos.

“Las personas están comprando principalmente productos básicos y relegando otros como fiambres, quesos o bebidas alcohólicas que tienen mayor margen de utilidad para el comercio”, expresó la dueña de un maxi kiosco de la ciudad de Córdoba.

“Se notó muy poca plata en la calle”, fue el comentario del propietario de un almacén en Godoy Cruz, Mendoza.

**2) Bazar, decoración, textiles para el hogar y muebles.** Las ventas en noviembre retrocedieron 2,7% anual y bajaron 1,9% en la comparación mensual, a precios constantes. Los comerciantes coincidieron que después del Día de la Madre en octubre, la venta minorista para el sector en este mes se hizo cuesta arriba.

“Noviembre fue malo en ventas en la comparación mensual y anual. Tanto que tuvimos que reducir la jornada laboral de algunos empleados”, resumió la dueña de un local dedicado a la venta de productos decorativos de corcho y mimbre de la Ciudad de Buenos Aires.

“Las ventas no mejoran para nuestro rubro porque las familias priorizan otras necesidades y se cuidan en los gastos”, dijeron desde un local de la ciudad de Pergamino, en Provincia de Buenos Aires.

**3) Calzado y marroquinería.** Las ventas en noviembre declinaron 5,3% anual y se incrementaron 1,7% mensual, siempre medidas a precios constantes. Los negocios trabajaron en renovar vidrieras y lanzar promociones. Igual se vendió poco, pero hay buenas expectativas para diciembre. Los comerciantes creen que irán mejorando con las fiestas y el cambio de temporada, siempre que no haya problemas de abastecimiento de mercadería.

“Noviembre fue difícil, hubo faltantes de productos, de insumos y los precios estuvieron disparadísimos, con ajustes semanales”, dijo un empresario de la ciudad de San Luis.

“Pusimos nuevas promociones, muy tentadoras que ayudaron a incrementar las ventas”, manifestó la encargada de un comercio en la ciudad de Rosario, en Santa Fe.

**4) Farmacia y perfumería**. Las ventas en noviembre crecieron 8,9% anual y 0,9% mensual, a precios reales. A pesar de los faltantes de medicamentos por las demoras de algunas droguerías, las farmacias continuaron trabajando bien, aunque con algunos problemas de cobro en aquellas que operan con obras sociales. También fue un mes bastante dinámico para las perfumerías, que se movieron en función de eventos (tanto para uso personal como para regalo), con buena demanda tanto de productos nacionales como importados.

“Se vendió poco, pero vamos a repuntar en diciembre. Tenemos listos sorteos, packs de regalos, y ofertas para Navidad”, señaló la responsable de una perfumería de la capital de Salta.

“Durante noviembre tuvimos un incremento importante en las ventas de antigripales porque se dieron muchos casos de influenza”, comentaron desde una farmacia en la ciudad de Santiago del Estero.

**5) Ferretería, materiales eléctricos y materiales de la construcción.** Las ventas bajaron 0,7% anual en noviembre (a precios constantes) y 1,2% en la comparación mensual. Comercios consultados remarcaron que este mes faltaron clavos, alambres, y materiales eléctricos.

“La inflación es desconcertante, no tiene tope y genera mucha incertidumbre”, dijo el dueño de una ferretería de San Salvador de Jujuy.

“Entra gente al negocio, pero se hacen compras más pequeñas, se está cuidando mucho el ingreso”, expresó un empresario de la ciudad de Paraná, en Entre Ríos.

**6) Textil e indumentaria.** Las ventas retrocedieron 18,2% anual en noviembre y subieron 1,2% en la comparación mensual. El ramo está muy resentido porque los precios vienen aumentado fuerte. Las compras del público se orientaron a ofertas o promociones específicas. Las empresas están convencidas que en diciembre se venderá bien, y están preparando sus stocks para la fecha. Se trata de un rubro vinculado al desempeño del ingreso familiar.

“La indumentaria es uno de los rubros donde los precios más subieron a lo largo del año. La situación es difícil, noviembre fue muy claro”, dijo la dueña de una lencería de Resistencia, Chaco.

“El mes fue terrible, no había gente en la calle, no cubrimos los costos”, se lamentaron desde un comercio en la ciudad de Mendoza.