---
category: Agenda Ciudadana
date: 2021-11-18T06:00:24-03:00
thumbnail: https://assets.3dnoticias.com.ar/pichetto.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: 'Pichetto: "El Gobierno ha tratado de instalar una agenda que no es la de
  la derrota"'
title: 'Pichetto: "El Gobierno ha tratado de instalar una agenda que no es la de la
  derrota"'
entradilla: 'El ex candidato a vicepresidente cuestionó el acto convocado por el Frente
  de Todos a Plaza de Mayo: "Lo importante sería que el Gobierno hubiera procesado
  con madurez el resultado electoral".'

---
El auditor general y dirigente de Juntos por el Cambio Miguel Ángel Pichetto sostuvo hoy que "el Gobierno perdió de manera contundente" las elecciones legislativas y cuestionó que, sin embargo, "ha puesto elementos distractivos y ha tratado de instalar una agenda que no es la de la derrota".

"Los hechos son los que mandan en política, tenés que asumir todo con una cuota de realismo", afirmó el ex compañero de fórmula de Mauricio Macri en diálogo con el programa "Pan y circo" que conduce Jonatan Viale por Radio Rivadavia y en este sentido, afirmó que "el Gobierno perdió de manera contundente esta elección, en todos los planos".

Pichetto cuestionó la movilización convocada por el Frente de Todos para festejar el "Día de la militancia", al señalar que luego del resultado de los comicios, en el oficialismo "han puesto elementos distractivos y han tratado de instalar una agenda que no es la de la derrota".

"Lo importante sería que el Gobierno hubiera procesado con madurez el resultado electoral y empezar a plantearle a la sociedad y la oposición cuál es el proyecto económico para salir adelante", evaluó el ex senador nacional del PJ.

Según dijo, "no hay que comprarse este caramelo de madera, en realidad el gobierno instaló la propuesta de un programa para dialogar, después hizo un discurso de que habían ganado”.

“Los hechos son los que mandan en política, tenés que asumir todo con una cuota de realismo”, consideró el rionegrino, que reiteró: "El Gobierno perdió de manera contundente esta elección, en todos los planos”.

A la vez, al hablar de Juntos por el Cambio, Pichetto evaluó: “Yo creo que la construcción política de Juntos por el Cambio tiene que ir del centro a la derecha, en ese espacio hubiésemos tenido una gran potencia”.