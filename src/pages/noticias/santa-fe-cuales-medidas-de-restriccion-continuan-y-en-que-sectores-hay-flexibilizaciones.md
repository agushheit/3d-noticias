---
category: La Ciudad
date: 2021-06-14T06:15:28-03:00
thumbnail: https://assets.3dnoticias.com.ar/MASRESTRICCIONES.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Santa Fe: cuáles medidas de restricción continúan y en qué sectores hay
  flexibilizaciones'
title: 'Santa Fe: cuáles medidas de restricción continúan y en qué sectores hay flexibilizaciones'
entradilla: Regirán desde el lunes hasta el próximo 25 de junio. ¿Qué pasa con las
  clases, comercios, bares y encuentros sociales?

---
El gobierno de la provincia de Santa Fe dispuso una serie de restricciones con vigencia a partir del sábado 12 de junio en contra de la propagación del coronavirus. En general, se optó por mantener la limitación de la circulación, el horario y la modalidad de atención de bares y locales comerciales durante este fin de semana. Pero, a partir del lunes y hasta el próximo 25 de junio, se establece la apertura de locales en shoppings y la actividad deportiva que no sea de contacto, entre otras actividades.

Respecto a las clases presenciales, la ministra de Salud, Sonia Martorano, dijo que toda la provincia está por encima de los 500 casos cada 100.000 habitantes y eso no permite volver a la presencialidad, por lo menos, en los departamentos La Capital, Rosario y San Lorenzo. "Sí vamos a tener presencialidad en aquellas localidades que hayan tenido menos de 10 casos en los últimos 14 días", aclaró Martorano.

**Medidas vigentes entre el lunes 14 y el 25 de junio**

\- Se amplía el horario de la actividad del comercio mayorista y comercio minorista, con atención al público en los locales, todos los días de la semana hasta las 19.

\- El factor de ocupación de la superficie cerrada de los locales, destinada a la atención del público, no podrá exceder del treinta por ciento (30%).

\- Queda habilitada la actividad de los locales comerciales ubicados en centros y paseos, en modalidad galería; cumplimentando las reglas generales de conducta, prevención y los protocolos establecidos, hasta las 19.

\- Los locales gastronómicos podrán funcionar entre las 06 y las 19.

\- La restricción vehicular será de esta forma: en todo el territorio provincial queda autorizada la circulación vehicular en la vía pública, desde las 06 hasta las 20.

\- A partir de las 20 y hasta las 06 del día siguiente sólo se permitirá la circulación de quienes realicen actividades esenciales.

**Quedan exceptuados de la restricción:**

\-Quienes cumplan con su actividad laboral

\-Quienes estén debidamente justificadas por una situación de fuerza mayor (incluyendo asistencia médica, farmacéutica y cuidado de personas)

\-Para circular deberán llevar la documentación respaldatoria y la app provincial COVID19 o app CUIDAR.

\-Se habilitan las actividades religiosas al aire libre sin superar las 10 personas, manteniendo la distancia mínima de 2 metros y el uso de barbijo en todo momento.

\-Sólo bajo modalidad entrenamiento, al aire libre y sin superar las 10 personas, se habilita la actividad de clubes, gimnasios, natatorios y otros establecimientos afines.

\-Continúa suspendida la práctica de deportes grupales de contacto (al aire libre y en espacios cerrados), y las competencias deportivas provinciales, zonales o locales de carácter profesional o amateur, no habilitadas expresamente por las autoridades nacionales.

\-Continúan suspendidas las actividades náuticas, pesca deportiva y recreativa en modalidad costa y embarcados, actividades de los clubes deportivos vinculados a las mismas y de guarderías náuticas.

\-Continúan prohibidos los encuentros sociales y familiares en lugares cerrados.

**Teletrabajo**

Se invita a los Poderes Legislativo y Judicial y a los Municipios y Comunas de la Provincia, a priorizar en relación al personal de su dependencia la modalidad de prestación de servicios en forma remota o teletrabajo, en las áreas donde ello fuera posible.