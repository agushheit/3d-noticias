---
category: Agenda Ciudadana
date: 2021-09-10T06:15:01-03:00
thumbnail: https://assets.3dnoticias.com.ar/CANNABIS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Las respuestas positivas del cannabis medicinal a la epilepsia refractaria
  superaron el 80%
title: Las respuestas positivas del cannabis medicinal a la epilepsia refractaria
  superaron el 80%
entradilla: El centro pediátrico destacó que el uso tuvo un alto impacto" y que los
  estudios "confirmaron al CBD como "una herramienta terapéutica sumamente valiosa".

---
Las respuestas positivas del cannabis medicinal (CBD) en los pacientes con epilepsia refractaria superaron el 80% de los casos estudiados y una reducción del número de crisis del 60%, lo que indica que su uso "contribuye también a mejorar aspectos cognitivos, conductuales y motores", indicó el Hospital Garrahan en el marco de la Semana de la Epilepsia.

El centro pediátrico destacó que el uso de cannabis medicinal "tuvo un alto impacto" y que los estudios "confirmaron al CBD como "una herramienta terapéutica sumamente valiosa como adyuvante al tratamiento en niños y niñas con encefalopatías epilépticas refractarias".

Los resultados indican que su uso "contribuye también a mejorar aspectos cognitivos, conductuales y motores, repercutiendo en una mejora en la calidad de vida, tanto de las y los pacientes como de sus familias", destacó el Garrahan.

La epilepsia afecta a 1 de cada 100 niñas y niños, y si bien el 80% de las epilepsias pueden ser controladas con fármacos, el 20% requiere diagnóstico, tratamiento y controles más específicos.

Se trata de un trastorno neurológico severo que se produce por una actividad eléctrica excesiva de un grupo de neuronas hiperexcitables del sistema nervioso central. Se manifiesta en crisis espontáneas recurrentes con consecuencias médicas, psicológicas y sociales.

El diagnóstico de la enfermedad es clínico en la mayoría de los casos, basado en una entrevista y un examen físico. Los signos de alerta pueden ser confusión temporal, episodios de ausencias, movimientos espasmódicos incontrolables de brazos y piernas o pérdidas de conocimiento o conciencia.

Alrededor del 80% de las personas con epilepsia pueden ser tratadas con fármacos anticonvulsivos básicos y un 10% con dieta específica o cirugía. Aproximadamente el 20% de las y los pacientes son resistentes a las diferentes opciones terapéuticas.

Gabriela Reyes, miembro del servicio de Neurología del Garrahan, explicó que muchas y muchos pacientes con epilepsia no están en tratamiento o reciben terapias inapropiadas: “Una de las razones es la falta de atención adecuada y de seguimiento”.

Por esta razón, desde la ILAE -Liga Internacional contra la Epilepsia en sus siglas en inglés- se promueve la Semana de la Epilepsia como una oportunidad para informar sobre esta afección y además quitar estigmas sobre quienes la padecen.

Reyes resaltó que “resulta crucial educar tanto a los médicos, trabajadores y personas clave de la comunidad como al público en general, y seguir investigando para poder mejorar la calidad de vida de todos los pacientes pediátricos con esta patología”.

El Garrahan indicó que la atención de pacientes con esa patología se originó con la creación de la Clínica de Epilepsia en 1990 y que se desarrollaron equipos de trabajo multidisciplinario compuesto por los servicios de Neurología, Neurocirugía, Neuroimágenes, Clínicas Interdisciplinarias, Nutrición, Biología Molecular y Farmacocinética.

En este maco "se logró avanzar en diferentes campos para el tratamiento de las epilepsias fármaco resistentes, como el abordaje de pacientes candidatos a Cirugía de Epilepsia, a la Estimulación del Nervio Vago, a la Terapia o Dieta Cetogénica o al uso de Cannabis Medicinal.