---
category: Agenda Ciudadana
date: 2020-12-03T11:04:20Z
thumbnail: https://assets.3dnoticias.com.ar/pesca-deportiva.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: 'Coronavirus: La provincia dispuso modificaciones horarias para natatorios
  y pesca deportiva'
title: 'Coronavirus: La provincia dispuso modificaciones horarias para natatorios
  y pesca deportiva'
entradilla: Los natatorios podrán permanecer abiertos hasta las 22 horas, mientras
  que las actividades relacionadas a la pesa deportiva y recreativa podrán realizarse
  hasta las 21 horas.

---
El gobierno provincial, a través del [Decreto Nº 1628](https://assets.3dnoticias.com.ar/decreto-1628.pdf "Decreto 1628"), estableció a partir del 1 de diciembre nuevos horarios de cierre para algunas actividades y servicios.

De esta manera, los natatorios a los fines de la práctica de las actividades gimnasia acuática, gimnasia terapéutica, entrenamiento y enseñanza de natación podrán estar abiertos hasta las 22 horas.

En tanto, la pesca deportiva y recreativa en la modalidad desde costa y embarcados, navegación recreativa, actividades de los clubes deportivos vinculados a las mismas y de las guarderías náuticas, a los fines del retiro y depósito de las embarcaciones hasta las 21 horas.