---
category: Agenda Ciudadana
date: 2021-12-16T06:00:17-03:00
thumbnail: https://assets.3dnoticias.com.ar/CONSEJOCORTE.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: Alberto Fernández pidió abrir una discusión honesta sobre el mejor diseño
  de la Corte Suprema de Justicia
title: Alberto Fernández pidió abrir una discusión honesta sobre el mejor diseño de
  la Corte Suprema de Justicia
entradilla: 'El jefe de Estado llamó a "revisar sus mecanismos de trabajo, número
  de integrantes y división de tareas en el máximo tribunal".

'

---
El presidente Alberto Fernández pidió esta tarde abrir "una discusión honesta sobre el mejor diseño de la Corte Suprema de Justicia", con el objetivo de "revisar sus mecanismos de trabajo, número de integrantes y división de tareas en el máximo tribunal".

"Resulta necesario establecer una discusión honesta sobre el mejor diseño de la Corte Suprema de Justicia de la Nación. Las demoras en el servicio de justicia, junto a elementos que no hacen a su aceitado funcionamiento, imponen revisar sus mecanismos de trabajo, número de integrantes y división de tareas en el máximo tribunal", resaltó Fernández.

Al participar del encuentro de cierre de año del Consejo Económico y Social (CES), en el Teatro Nacional Cervantes, Fernández precisó: "Se trata de una tarea que tenemos que impulsar con el máximo nivel de patriotismo, amplio espíritu republicano y apego a la verdad".  
En ese marco, el Presidente advirtió que el futuro acuerdo con el Fondo Monetario Internacional (FMI) requiere de la "responsabilidad y sensibilidad de toda la dirigencia" política.

"La reconstrucción del crédito público y el valor de la moneda tiene que estar acompañada de un proceso que no detenga nuestra recuperación productiva", subrayó.

En su discurso, Fernández planteó diez desafíos estructurales de su gobierno para el próximo año, entre los que incluyó la reconstrucción del federalismo, un nuevo modelo educativo de la escuela secundaria, y un Estado con una administración eficaz y soberana.

Además, sumó como pilares básicos la constitución de una Agencia Nacional de Prevención de la Violencia, la reconstrucción de una adecuada prestación del servicio básico de justicia, promover un debate sobre el mejor modo de elegir a los jueces a través del Consejo de la Magistratura de la Nación, e implementar los juicios por jurados a nivel federal.

Al hacer mención a la idea de "reconstruir el federalismo", Fernández volvió a plantear la idea de trasladar la Capital Federal al interior del país y consideró necesario abrir el debate.

"Ronda en mi cabeza la idea de trasladar nuestra Capital Federal al interior del país. Si lo hiciéramos, no solo generaríamos otro polo de desarrollo donde Argentina lo necesita, sino que nos obligaría a repensar el Área Metropolitana de Buenos Aires para superar esta concentración macro cefálica", subrayó.

En otro tramo de su intervención, afirmó que "no hay políticas de Estado sin Estado", y amplió: "Necesitamos una administración eficaz y soberana y a la altura de los grandes cambios tecnológicos y sociales".

"Vamos a poner en marcha en breve plazo –a través de la Jefatura de Gabinete de Ministros- una convocatoria para seleccionar a los 8.000 cargos directivos esenciales del Estado central, que tengan un proceso de formación que recoja las mejores experiencias mundiales", anticipó.

El Presidente también convocó a "decirle nunca más a la violencia", y comunicó que "en los próximos 100 días" convocarán "a todas las instancias políticas, sociales y de conocimiento del país al diseño de un Plan Maestro de Acción, que guíe la constitución de una Agencia Nacional de Prevención de la Violencia".

Respecto de la Justicia, el mandatario consideró que "los problemas de impunidad se solucionan con más y mejor justicia", por lo que convocó a "promover un debate social sobre el mejor modo de seleccionar y elegir a los jueces".

En el tramo final de su oratoria, Fernández ponderó: "Nuestra Patria tiene todo para encaminarse a un horizonte de despegue, si somos capaces de unir energías, emociones y visiones con sentido generoso y fraterno. Para eso debemos dejar de lado la avaricia y el egoísmo".

"El milagro argentino no depende de ninguna mano mágica ni de ningún regalo externo caído del cielo. El milagro argentino depende de una Argentina unida y querida, que deje atrás el maltrato de las fracturas permanentes y el egoísmo de excluir a quien piensa distinto", concluyó.