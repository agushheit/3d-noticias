---
category: Agenda Ciudadana
date: 2021-10-08T06:15:47-03:00
thumbnail: https://assets.3dnoticias.com.ar/PUSINIERI.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: El gobierno anunció que descontará los días de paro a los docentes y ratificó
  la oferta salarial
title: El gobierno anunció que descontará los días de paro a los docentes y ratificó
  la oferta salarial
entradilla: El gremio docente rechazó por mayoría la propuesta del 17% que realizó
  el gobierno provincial y determinó un paro para la semana próxima y otro en 15 días

---
El ministro de Trabajo, Empleo y Seguridad Juan Manuel Pusineri ratificó la propuesta del 52% de aumento salarial a los docentes y anunció que se descontarán los días a los trabajadores que hagan paro.

"La oferta salarial que hace el gobierno de un 52%, medida desde febrero de 2021 a enero de 2022, es una propuesta que supera cualquier otra oferta salarial que se haya hecho en el orden nacional y provincial en la república Argentina", explicó.

Hasta el momento UPCN, ATE y Sadop son los gremios que aceptaron la propuesta salarial efectuada por el Gobierno, y el gobierno anunció que va a proceder a liquidar el aumento correspondiente del mes, del 10%. Con respecto a Amsafé, que la rechazó, Pusineri dijo que mientras no esté la aceptación de los docentes públicos no se les va a liquidar ningún tipo de aumento.

"Está claro que en el caso de Amsafe estamos atravesados por una situación política interna en la cual determinado sector de la dirigencia de Amsafe habitualmente no acepta ninguna propuesta, con lo cual sea del 50, del 60, del 70, del 80 o del 90 por ciento, la respuesta va a ser la misma", disparó el titular de la cartera de trabajo.

Con respecto al paro anunciado para la próxima semana, Pusineri opinó que “es una medida desproporcionada”. “Venimos de un año complicado, de una situación de pandemia que obligó muchas veces a la virtualidad; me parece que hay una expectativa muy fuerte de los alumnos y de los padres de que haya clases, de manera tal que frente a una propuesta que es la mejor de la Argentina y un salario docente en la provincia de Santa Fe que es, si no el mejor, uno de los mejores salarios de la Argentina, que la respuesta sea un paro a nosotros nos parece desproporcionado”, explicó.

Y agregó: “Deberá ser Amsafe (o la dirigencia que propone rechazar la propuesta) quien le debe explicar a los padres y a los alumnos por qué actúa de esta manera”.

Visto de enero a febrero, el aumento con respecto al año anterior sería de un 52%, pero concretamente para este último tramo el número es un 17%, dividido en tres tramos: 10% en octubre, 5% en diciembre y 2% en enero. Según el funcionario el gobierno se mantendrá firme en la propuesta y aseguró que se liquidarán los aumentos a los gremios que sí aceptaron.

Finalmente, Pusineri argumentó que "el derecho de huelga es un derecho garantizado constitucionalmente e internacionalmente a punto tal de que es considerado un derecho humano" pero que “no quiere decir que el día no trabajado se deba a liquidar”, confirmando que se le descontará el día a los docentes que se adhieran al paro.

“El día que no se trabaja o el día de clase que se pierde es un día que no se liquida y eso no significa en lo más mínimo ninguna afectación al derecho de huelga. Por supuesto que el día que no se trabaja no se paga", sentenció.