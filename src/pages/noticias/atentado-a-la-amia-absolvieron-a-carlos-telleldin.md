---
category: Agenda Ciudadana
date: 2020-12-26T10:24:58Z
thumbnail: https://assets.3dnoticias.com.ar/2412telledin.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Infobae'
resumen: 'Atentado a la AMIA: absolvieron a Carlos Telleldín'
title: 'Atentado a la AMIA: absolvieron a Carlos Telleldín'
entradilla: Lo resolvió el Tribunal Oral Federal 3. La Fiscalía y las querellas habían
  pedido que sea condenado por haber entregado la camioneta que se utilizó como coche
  bomba.

---
Carlos Telleldín fue absuelto hoy en el juicio oral en su contra por haber entregado la camioneta _traffic_ blanca que fue utilizada como coche bomba en el atentado a la AMIA. La decisión fue tomada por jueces del Tribunal Oral Federal 3, Andrés Basso, Javier Ríos y Fernando Canero. La Fiscalía y las querellas habían pedido que sea condenado a prisión perpetua y a 20 años de cárcel.

Telleldín escuchó su absolución a través del sistema de videoconferencia que se utiliza para las audiencias por la pandemia del coronavirus. «Reitero, como en 2004, mi inocencia total y absoluta en este terrible atentado», había dicho  a la mañana quien, entre 1994 y 2004, estuvo preso en la cárcel de Devoto por el caso.

Los jueces darán, el próximo 26 de marzo a las 17 horas, los fundamentos de su sentencia. Desde ese momento, la Fiscalía y las querellas tendrán 10 días para apelar el veredicto y que sea revisado por la Cámara Federal de Casación. Fuentes allegadas a las acusaciones descuentan que lo harán después de conocer los fundamentos del tribunal.

El tribunal, en su veredicto, ratificó que el atentado a la AMIA fue un hecho de lesa humanidad. También rechazaron todos los planteos de nulidad que la defensa de Telleldín había hecho y finalmente lo absolvió.

**Este es el tercer juicio que afrontó Telleldín**. El primero fue por el mismo hecho y fue absuelto cuando se decretó la nulidad del caso y se ordenó que se vuelva a investigar. Y el segundo fue por las irregularidades en la investigación del atentado en el que fue condenado a tres años y medio de prisión.

**El atentado a la AMIA ocurrió hace 26 años y medio**. La mañana del 18 de julio de 1994, una camioneta _traffic_ con entre 300 y 400 kilos de explosivos detonó en la AMIA, mató a 85 personas y 151 resultaron heridas. Era el segundo atentado en el país después del ataque en 1992 a la embajada de Israel.

En 2001 empezó el primer juicio oral. Terminó en 2004 cuando el TOF 3, con otra integración de jueces, declaró todo nulo por las irregularidades en la investigación judicial que había realizado el entonces juez Juan José Galeano. Entre ellas, que Telleldín cobró 400 mil dólares para acusar falsamente a oficiales de la Policía Bonaerense de que habían tenido la _traffic_ en su poder. Así, Telleldín, los policías y el resto de los acusados fueron absueltos.

En 2009, la Corte Suprema de Justicia de la Nación confirmó la nulidad del caso, excepto la parte de la entrega de la camioneta, por lo que Telleldín volvió a ser investigado y llegó a este segundo juicio que se inició en mayo del año pasado, se suspendió en marzo por la pandemia y se retomó en junio por videoconferencia.

Los fiscales de la Unidad AMIA, Roberto Salum, Santiago Eyherabide y Santiago Roldán, la querella que representa a los familiares de las víctimas, a cargo de Horacio Etcheverry y Analía Fangano, y la de AMIA-DAIA, encabezada por Miguel Bronfman, pidieron que Telleldín sea condenado por haber entregado la camioneta que se usó en el atentado. Solo difirieron en el delito y el monto de la pena. La Fiscalía y los familiares pidieron perpetua y AMIA-DAIA 20 años y la detención tras la condena.

***

![](https://assets.3dnoticias.com.ar/2412amia.webp)

***

Los acusadores dijeron en sus alegatos que Telleldín fue la última persona comprobada que tuvo en su poder el motor que se encontró en los escombros de la AMIA y sus esquirlas en los cuerpos de las víctimas. 

También que hizo la venta con papeles falsos -estaba a nombre de Ramón Martínez, una persona con la que estaba enemistada, fraguó la firma y puso un número de DNI que empezaba con 41 millones que para 1994 no existía-; que sembró testigos para hacer un montaje de que no estaba vinculado al hecho y que cuando ocurrió el atentado se fugó a Misiones, donde fue detenido. Y señalaron que en el juicio se recogieron testimonios que indicaron que Telleldín estaba muy nervioso cuando se conoció el ataque a la AMIA.

«Dio el elemento esencial para el hecho. Telleldín se representó que ese vehículo podía ser usado como un coche bomba y a pesar de eso siguió adelante con su accionar. Actuó con indiferencia y egoísmo», dijo el fiscal Salum en su alegato.

Antes del veredicto, Telleldín, de 59 años, doblador de autos y abogado, habló de la venta: «La camioneta se la vendí a una persona que vino por el aviso del diario, se vendió de manera normal. Tenía acento centroamericano. Por eso la presunción de que podía estar vinculado al narcotráfico. Pero es una presunción. Una locura tremenda decir que me tenía que representar el atentado. Pasaron 27 años y siguen diciendo las mismas mentiras».

Su defensora oficial, Verónica Carzolio, había pedido su absolución. Sostuvo que se debía aplicar el criterio de plazo razonable de juzgamiento, ya que después de 26 años el acusado no puede ser responsable de las demoras judiciales. También que **la causa está prescripta, ya que no es un delito de lesa humanidad porque, para eso, debe ser cometido por un estado o por una organización que le responda y que no está probado que Irán haya participado como país del atentado**. 

Agregó que Telleldín no pudo defenderse en la investigación del caso por la falta de imparcialidad del destituido juez Galeano, y que ya fue juzgado en un proceso en el que fue absuelto, por lo que, tal como marca la ley, no puede ser acusado dos veces del mismo hecho. Todos los planteos fueron rechazados.

El año que viene intervendrá la Cámara Federal de Casación cuando la Fiscalía y las querellas apelen la absolución. Casación también tiene que resolver las condenas y absoluciones en el juicio por las irregularidades en la investigación del atentado. Telleldín tiene una pena de tres años y medio de prisión, el ex juez Galeano de seis años, los ex fiscales Eamon Mullen y José Barbaccia de dos años y el expresidente Carlos Menem fue absuelto, entre otros.