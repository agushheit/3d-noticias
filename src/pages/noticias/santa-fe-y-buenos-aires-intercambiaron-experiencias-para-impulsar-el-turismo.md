---
category: La Ciudad
date: 2020-12-29T10:55:40Z
thumbnail: https://assets.3dnoticias.com.ar/2912-turismo.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Santa Fe y Buenos Aires intercambiaron experiencias para impulsar el turismo
title: Santa Fe y Buenos Aires intercambiaron experiencias para impulsar el turismo
entradilla: Representantes de la Municipalidad de Santa Fe y del Gobierno de la Ciudad
  de Buenos Aires se reunieron para compartir políticas innovadoras y dar a conocer
  el proyecto «Mi ciudad como turista».

---
Funcionarios de la Municipalidad de Santa Fe participaron de una reunión virtual con el Ente de Turismo de la Ciudad de Buenos Aires con el fin de conocer, intercambiar y ampliar la información sobre el proyecto «Mi ciudad como turista» que está actualmente en aplicación en el municipio local. La invitación surgió desde el Gobierno de CABA en el marco del programa de transferencia de conocimientos y buenas prácticas de la Dirección General de Promoción Turística.

Durante el encuentro se reunieron Matías Schmüth, secretario de Producción y Desarrollo Económico; Franco Arone, director Ejecutivo de Turismo; Victoria Rodríguez, subdirectora de Generación de Contenidos de Santa Fe Capital, y Manuel Viola, coordinador de Comunicación Territorial. Mientras que por el Gobierno de la Ciudad de Buenos Aires participaron Diego Gutiérrez, director general de Promoción Turística; Ignacio Coronel, jefe de gabinete; Roberto Klopsch, gerente operativo de Promoción Turística; Mario Katzenell, gerente de Relacionamiento; Leandro González asesor de proyectos estratégicos de promoción; y Tomás de Vedia, coordinador de la Subsecretaría de Cooperación Urbana Federal.

Arone, junto al equipo de creación del proyecto, presentaron las nueve audioguías que se encuentran a disposición de los vecinos de Santa Fe y los turistas, que permiten conocer los circuitos caminables que resaltan el patrimonio histórico, cultural y los atractivos naturales, rescatando los imperdibles de Santa Fe. Además, en la ocasión se trabajaron temas de accesibilidad turística, el plan de comunicación y el aspecto sustentable del proyecto.

![](https://assets.3dnoticias.com.ar/2912-turismo1.webp)

«Fue una reunión de trabajo para poder mostrarles y asesorarlos sobre el trabajo que venimos haciendo con audioguías, que es una experiencia que está funcionando bien en la ciudad. Pudimos contar el proceso y el objetivo de esta iniciativa, que se enmarca dentro de las políticas públicas que impulsa el intendente Emilio Jatón para generar nuevas herramientas de disfrute del espacio público para los santafesinos y para los turistas», explicó por su parte Schmüth, y agregó: «Quedamos en poder seguir trabajando en conjunto para poder mantener el vínculo y convenir acciones de turismo».

En tanto, Gutiérrez, director de Promoción Turística de CABA, indicó: «Este encuentro tiene que ser el puntapié para seguir trabajando en conjunto, aprovechando cada oportunidad para que, en equipo, fortalezcamos el turismo nacional. Estos espacios nos permiten intercambiar experiencias y generar mejoras en cada uno de nuestros proyectos, sumando ideas innovadoras y creativas».

<br/>

<span style="font-family: Helvetica, sans-serif; font-size: 1.2em; font-weight: 700">**Audioguías**</span>

La Municipalidad de Santa Fe incorporó audioguías a sus paseos turísticos, en un trabajo conjunto entre la Dirección de Turismo y la Subsecretaría de Comunicación del municipio, que se pueden escuchar en línea o descargar en cualquier dispositivo móvil, de manera gratuita, desde la web oficial de la Municipalidad y desde el canal de Spotify.

Ya hay 9 paseos con estas guías en audio, cada uno de ellos con varias pistas, y la idea es seguir agregando espacios y edificios.

<br/>

<span style="font-family: Helvetica, sans-serif; font-size: 1.2em; font-weight: 700">**Transferencia de conocimiento**</span>

El programa de transferencia de conocimiento y buenas prácticas de CABA nace por la necesidad de generar espacios de intercambio colectivo en un sector que reviste de gran importancia en la economía nacional e internacional, y debe buscar nuevas formas de operar a través de los avances tecnológicos en un nuevo paradigma. Por otro lado, permite fortalecer los lazos con los representantes de los gobiernos provinciales y municipales, cómo también con el sector privado, buscando temas de interés que generen una mejor gestión en la oferta turística.

Ambos destinos -Santa Fe y Buenos Aires- continuarán trabajando juntos de cara al futuro y se encontrarán elaborando un convenio de colaboración para firmar el año próximo.