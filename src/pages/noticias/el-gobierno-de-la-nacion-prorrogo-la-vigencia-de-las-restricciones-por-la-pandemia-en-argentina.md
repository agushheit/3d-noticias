---
category: Agenda Ciudadana
date: 2021-07-10T06:15:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/LAROSADA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: El Gobierno de la Nación prorrogó la vigencia de las restricciones por la
  pandemia en Argentina
title: El Gobierno de la Nación prorrogó la vigencia de las restricciones por la pandemia
  en Argentina
entradilla: El DNU, publicado en el Boletín Oficial, lleva la firma de todo el gabinete
  de ministros y extiende las medidas hasta el 6 de agosto inclusive.

---
El gobierno nacional prorrogó oficialmente las restricciones por la pandemia a través de un nuevo Decreto de Necesidad y Urgencia, publicado este viernes en el Boletín Oficial.

Entre los considerandos, el documento explica que "a nivel mundial, al 7 de julio de 2021 se confirmaron más de 184 millones de casos y casi 4 millones de personas fallecidas, con más de DOSCIENTOS (200) países, áreas o territorios, afectados por COVID-19. En tanto, la región de las Américas representa el CUARENTA POR CIENTO (40 %) del total de nuevos casos a nivel mundial en la última semana y que, en relación con los casos acumulados, la región de las Américas comprende el CUARENTA POR CIENTO (40 %) de los casos y el CUARENTA Y OCHO POR CIENTO (48 %) de las muertes totales".

"Aunque la situación en la región continúa siendo dispar -prosigue el documento-, siendo EE.UU. y Brasil los países que lideran el total acumulado de casos de la región, ya se han detectado variantes del virus SARS-CoV-2, consideradas de preocupación (Alpha, Beta, Gamma y Delta), en diversos países afectando varios continentes, con aumento importante de circulación de la variante Delta, por lo que se desarrollaron estrategias para disminuir la posibilidad de transmisión de esta variante a nuestro país. En relación con la circulación de nuevas variantes, ha surgido la variante Delta, originalmente detectada en India como variante de preocupación, que ha sido reportada a la fecha en OCHENTA Y SEIS (86) países; acorde a diversos estudios, la variante Delta tiene mayor contagiosidad y transmisibilidad que las otras variantes detectadas con anterioridad -se estima que es entre un CINCUENTA POR CIENTO (50 %) y un SETENTA POR CIENTO (70 %) más contagiosa que la variante Alpha-".

"En las últimas semanas, el aumento de casos en muchos de los países de la región continúa incrementándose, principalmente en América del Sur, con saturación de los sistemas de salud en algunos países. Mientras, la tasa de incidencia acumulada para ARGENTINA es de 9489 casos cada 100.000 habitantes, la tasa de letalidad se mantiene estable en DOS COMA UNO POR CIENTO (2,1 %) y la tasa de mortalidad es de MIL NOVECIENTOS NOVENTA Y SEIS (1996) fallecimientos por millón de habitantes", añade el Decreto.

"El descenso observado en las últimas semanas a nivel nacional se continúa observando en algunos grandes centros urbanos, donde al 8 de julio, se registran OCHO (8) de DIECISIETE (17) aglomerados con una razón menor a CERO COMA OCHO (0,8), lo que marca una tendencia en descenso, pero otros OCHO (8) aglomerados dejaron de descender y presentan una razón de entre CERO COMA OCHO (0,8) y UNO COMA DOS (1,2). Algunas jurisdicciones presentan más de un OCHENTA POR CIENTO (80 %) de ocupación de camas de terapia intensiva y el número de personas internadas en UTI superó el pico registrado en 2020, pero en las últimas semanas se comenzó a registrar un descenso en el número de personas internadas. La incidencia en algunos aglomerados urbanos continúa elevada, con alta tensión en el sistema de salud, y OCHO (8) de las VEINTICUATRO (24) jurisdicciones registran más del OCHENTA POR CIENTO (80 %) de ocupación de camas de terapia intensiva, lo que genera riesgo de saturación y aumento de la mortalidad. Que VEINTIDÓS (22) de las VEINTICUATRO (24) jurisdicciones presentan alta incidencia en promedio -DOSCIENTOS CINCUENTA (250) casos cada 100 mil habitantes en los últimos CATORCE (14) días-, y OCHO (8) de ellas superan los QUINIENTOS (500) casos cada 100 mil habitantes en los últimos CATORCE (14) días", prosigue el texto.

Además, destaca que "las jurisdicciones con mayor ocupación promedio de camas de terapia intensiva son las Provincias del NEUQUÉN, de SANTA FE, de CORRIENTES, de RÍO NEGRO, de SALTA, de SAN JUAN, de ENTRE RÍOS y de CATAMARCA".

"Con el fin de disminuir el impacto de la segunda ola en nuestro país, se deben adoptar en forma concomitante con el proceso de vacunación medidas sanitarias y de prevención destinadas a mitigar la transmisión del virus. Por lo tanto, en el presente decreto se dispone la prórroga del Decreto N° 287/21 y el plazo establecido en su artículo 30, desde el día 10 de julio y hasta el 6 de agosto de 2021, inclusive", comunica el texto.