---
layout: Noticia con imagen
author: "Fuente: Gobierno Provincia de Santa Fe"
resumen: Plan 50 Destinos
category: Estado Real
title: Perotti y Lammens firmaron convenios  por obras turísticas en Santa Fe,
  en el marco del Plan “50 Destinos”
entradilla: “Cada crisis genera una oportunidad, y con esto vamos a poner a
  Santa Fe en el mapa del turismo receptivo”, aseguró el gobernador.
date: 2020-11-06T21:43:00.000Z
thumbnail: https://assets.3dnoticias.com.ar/06-11.jpg
---
El gobernador Omar Perotti y el ministro de Turismo y Deportes de la Nación, Matías Lammens, presentaron este jueves, en Rosario, la firma de los convenios para la realización de tres obras turísticas en distintas ciudades de la provincia. Entre las acciones a desarrollar se destaca un Centro Artístico Metropolitano en Rafaela, el Complejo Turístico ‘Gente del Agua’ en Reconquista y el Museo Ferroviario en Pérez.

“Cada crisis genera una oportunidad, y la oportunidad es poder mostrarnos hacia todos los santafesinos y argentinos como un destino con capacidad de recepción”, señaló Perotti durante el acto.

En ese marco, recordó que, en diciembre de 2019, en la primera reunión con Lammens, se coincidió en el compromiso de “poner a Santa Fe en el mapa del turismo receptivo, y que los números del turismo santafesino eran muy pequeños para la potencialidad que tiene esta provincia”.

“Seguimos trabajando en la instalación de este concepto de que Santa Fe tiene con qué irrumpir, y fuerte, en el turismo”, aseguró Perotti, y anticipó: “Vamos a tener un verano diferente; vamos a tener muchísimos santafesinos que van a descubrir el río, y otros lugares de la provincia”.

Y subrayó: “Santa Fe tiene capacidad (hoteles, aeropuertos), y con protocolos, seriedad y entusiasmo, podremos tener un buen fin de año y un buen verano, pero eso dependerá de cómo cada uno vaya acompañando estas medidas que hemos definido en la nueva modalidad de convivencia”.

**MOTOR DE DESARROLLO**

A su turno, Lammens indicó que se trabaja para que “el turismo sea uno de los grandes protagonistas del desarrollo económico de la Argentina, porque es un gran generador de divisas”. Y, en números, explicó que el movimiento del turismo interno representa el 9 por ciento del Producto Bruto Interno, y genera más de un millón de puestos de trabajo.

Por otro lado, señaló que Santa Fe, pese a “no ser un destino emblemático, es uno de los destinos más vendidos, y eso tiene que ver con el trabajo que se ha hecho a través del programa «Santa Fe Plus»”. Y sumó: “Tenemos que dotar de infraestructura a los destinos que tiene mucho para ofrecer, y en esto la provincia de Santa Fe tiene una gran oportunidad”.

Por su parte, el secretario de Turismo de Santa Fe, Alejandro Grandinetti, dijo que los convenios “ratifican el convencimiento del Ministerio de Turismo, que pensaba que la salida a la crisis era inyectar recursos para recuperar la vitalidad a partir de la demanda”. Y agregó: “El gobernador nos pidió que Santa Fe agregue un plus para convencer a muchos argentinos que vengan a la provincia, y así nació el programa «Santa Fe Plus»”.

En tanto, la subsecretaria de Desarrollo Estratégico del Ministerio de Turismo y Deportes de la Nación, Eugenia Benedetti, indicó que las acciones se ejecutan en el marco del ‘Plan 50 Destinos’, que abarca obras de infraestructura turística en todo el territorio argentino. Se están destinando 1.200 millones pesos con el objetivo de dinamizar la actividad, con mejoras que fortalezcan la oferta turística y fomenten la competitividad sectorial.

**TRES OBRAS TURÍSTICAS**

Una de las obras para Santa Fe es la concreción del Centro Artístico Metropolitano de Rafaela, que demandará una inversión de 28.604.000 pesos, y tiene como objetivo posicionar a ese distrito como ciudad de eventos y convenciones, y que se destaque a nivel regional y nacional.

Se destaca también el Complejo Turístico ‘Gente del Agua’, en la ciudad de Reconquista, que demandará una inversión de 23.294.100 pesos, y tendrá como finalidad desarrollar el ecoturismo y turismo activo como un nuevo producto de la ciudad.

Finalmente, con el objetivo de poner en valor el patrimonio ferroviario de la ciudad de Pérez, se ejecutará el ‘Museo Ferroviario’, que contará con una inversión de 7.959.220 pesos.