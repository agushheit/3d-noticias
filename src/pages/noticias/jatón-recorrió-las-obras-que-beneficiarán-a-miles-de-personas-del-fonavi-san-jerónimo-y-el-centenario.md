---
layout: Noticia con imagen
author: .
resumen: Obras municipales
category: Estado Real
title: Jatón recorrió las obras que beneficiarán a miles de personas del Fonavi
  San Jerónimo y el Centenario
entradilla: "Los trabajos beneficiarán directamente a cerca de 10 mil vecinas y vecinos. "
date: 2020-11-19T12:47:23.000Z
thumbnail: https://assets.3dnoticias.com.ar/jat%C3%B3n.jpg
---
Las obras buscan la disminución del riesgo hídrico; mejora del tránsito peatonal y vehicular; simplificación de la conexión con los barrios Chalet y San Lorenzo, y con las avenidas Mar Argentino y J. J. Paso; y la consolidación del proceso de urbanización.

Esta mañana, el intendente Emilio Jatón recorrió los avances en obras de mejoramiento vial y desagües pluviales que se llevan adelante en el Fonavi San Jerónimo y el Centenario. Los beneficiarios directos de estos trabajos serán unos 9.600 vecinos y vecinas; y de manera indirecta, cerca de 49.000, dentro del Distrito Suroeste. También estuvo la concejala Laura Mondino.

La obra significará una mejora en las condiciones de vida de los vecinos del barrio Centenario y su entorno, incluido el Fonavi San Jerónimo. Entre los beneficios se pueden mencionar la contribución al ordenamiento urbano del sector y su zona de influencia: disminución del riesgo hídrico, mejora del tránsito peatonal y vehicular, simplificación de la conexión con los barrios Chalet y San Lorenzo, y con las avenidas Mar Argentino y J. J. Paso, y consolidación del proceso de urbanización.

Vale recordar, que las obras comenzaron en el mes de julio y estarían previstas culminar en mayo del 2021. El monto de contrato es de 70 millones de pesos, financiados por el gobierno provincial, mientras que la inspección de la obra se realiza en conjunto con la Municipalidad. La empresa Mundo Construcciones es la encargada de concretar las tareas.

## **Esenciales**

Luego de recorrer junto al intendente Jatón los diferentes frentes de trabajos que se llevan adelante, Paula Nardin, directora de coordinación técnica de la Secretaría de Obras y Espacio Público, destacó: “Esta obra era muy esperada por todos los vecinos del barrio Centenario. Los objetivos principales son disminuir el riesgo hídrico y mejorar la accesibilidad de un amplio sector”.

En ese sentido, la funcionaria recordó que los trabajos contemplan el “cordón cuneta de todo el Fonavi San Jerónimo, con ripio y pavimento de hormigón frente a la Escuela Simón Bolívar; lo cual es fundamental para conectar la zona”.

Patricia Ledesma es vecina de la manzana 2 del Fonavi San Jerónimo y, en estos momentos, su cuadra está en plena obra. “Estamos re contentos porque esto hacía muchísima falta. Estamos recontra agradecidos de los trabajos que están haciendo”, comentó luego de conversar con el intendente Jatón.

Asimismo, Juan Carlos Bertusi, con más de 30 años en el Fonavi, destacó: “Va a quedar hermosa esta obra, la verdad es que se dedicaron bien al barrio este año. Y avanza rápido. Le comentaba recién al intendente que trabajan muy rápido”.

“Nos comentaba Jatón que van a parquizar y van a hacer estacionamientos, además del mejorado y los desagües. Va a quedar muy bien todo esto. Sin dudas son muy importantes estas obras; hay sectores en los que no se puede salir los días de lluvia. Esto beneficia a cualquier cantidad de gente”, indicó Juan Carlos.

![](https://assets.3dnoticias.com.ar/obra.jpg)

## **En detalle**

La zona sobre la cual se realiza esta intervención se encuentra al suroeste de la ciudad de Santa Fe. La obra tiene una extensión de más de 2000 metros, está comprendida entre las calles Libertad, entre 10 de Junio y Diego Núñez; Islas Malvinas, entre 10 de Junio y Diego Núñez; Diego Núñez, entre Libertad y Raúl Tacca; Raúl Tacca, entre Diego Núñez hasta el empalme con el pavimento existente frente al CARD “Pedro Candioti”; y Cristóbal Colón, entre Raúl Tacca y Gobernador Nicasio Oroño.

En las calles Libertad, Diego Núñez y la mayor extensión de Raúl Tacca con sus transversales hasta calle Reconquista, se colocará mejorado (16.500 m2) con cordón cuneta de hormigón. Sobre la calle Raúl Tacca, entre Rodríguez Peña y José María Pérez, y en calle Cristóbal Colón, entre Raúl Tacca y Gobernador Nicasio Oroño, se ejecutará un nuevo pavimento asfáltico de 8 metros de ancho con el correspondiente cordón cuneta.

Y finalmente, en calle Islas Malvinas entre Diego Núñez y 10 de Junio se ejecutará un pavimento de hormigón. De esta manera, las calzadas permitirán un mayor y más seguro flujo de tránsito, con una estructura de pavimento adecuada para el volumen presente y futuro.

En cuanto a las obras de desagües, en el sector del Fonavi San Jerónimo, sobre calle Diego Núñez, se ejecutará un conducto principal que inicia en Crucero General Manuel Belgrano y desemboca en el canal Tacca. A ese conducto se vincula un desagüe que se desarrolla por calle Malvinas Argentinas, a partir de la bocacalle de 10 de Junio.

Los líquidos pluviales de calle Raúl Tacca, serán colectados mediante bocas de tormentas emplazadas en las intersecciones de Vera Mujica, José María Pérez, Salustiano Zavalía, Gobernador Nicasio Oroño, Reconquista y Cristóbal Colón. Cada una de estas captaciones se vinculan a desagües desarrollados transversalmente a la calle Raúl Tacca y desaguan en el canal aledaño.

Los desagües transversales a Raúl Tacca estarán sobre Vera Mujica, donde está previsto un conducto que capta los líquidos pluviales desde calle Cristóbal Colón y otro por José María Pérez, que se desarrolla desde calle Reconquista y transporta el agua que llega a la intersección de ambas calles.

Por Tarragona, se encuentra proyectado un conducto desde Reconquista hasta el canal Tacca; y por último, en las intersecciones de Raúl Tacca con Gobernador Nicasio Oroño, Salustiano Zavalía y Reconquista, se ejecutará en cada caso un conducto para reunir el líquido que confluye a cada una de las intersecciones mencionadas.