---
category: Estado Real
date: 2020-12-24T10:30:43Z
thumbnail: https://assets.3dnoticias.com.ar/paradero.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Se solicita información sobre el paradero de Marcelo Francisco Barreyra
title: Se solicita información sobre el paradero de Marcelo Francisco Barreyra
entradilla: El hombre de 42 años, está ausente de su hogar desde el pasado 28 de octubre.

---
La Secretaría de Derechos Humanos y Diversidad de Santa Fe solicita cualquier información sobre el paradero de Marcelo Francisco Barreyra, de 42 años, quien está ausente de su hogar de la ciudad de Santa Fe, provincia de Santa Fe, desde el 28 de octubre de 2020.

Según consta en la denuncia, Marcelo Francisco Barreyra tiene tez blanca, una estatura de 1,65 metros, cabello negro y corto, ojos marrón claro, y contextura robusta.

Vestía jeans claros, campera color gris marca Nike y zapatillas, también de esta marca, color azul.

Entre sus señas particulares, la denuncia señala tatuajes de San La Muerte y el Gauchito Gil en la pierna derecha, y de una calavera en la espalda.

En su búsqueda intervienen la comisaría sub-primera de la Unidad Regional I y el Ministerio Público de la Acusación.

Se solicita la más amplia colaboración de toda la ciudadanía a los fines de recabar información que aporte a la búsqueda. Ante cualquier dato por favor contactarse con la Secretaría de Derechos Humanos y Diversidad de Santa Fe (Mendoza 3443, de la ciudad de Santa Fe o Balcarce 1145 de la ciudad de Rosario), llamar a la línea gratuita 0800-555-3348, o bien al celular 342 155357756.