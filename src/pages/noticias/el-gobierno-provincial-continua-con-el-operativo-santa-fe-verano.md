---
category: Agenda Ciudadana
date: 2021-01-18T09:00:31Z
thumbnail: https://assets.3dnoticias.com.ar/santa_fe_verano.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: El gobierno provincial continúa con el operativo “Santa Fe Verano”
title: El gobierno provincial continúa con el operativo “Santa Fe Verano”
entradilla: El dispositivo recorre localidades costeras con el objetivo de mostrar
  los atractivos turísticos de la provincia y promover cuidados de salud y seguridad
  vial durante estas vacaciones.

---
El gobierno provincial continúa desarrollando el operativo “Santa Fe Verano”, una iniciativa de prevención y concientización frente a la pandemia del COVID-19. Se trata de un operativo integral de salud y educación coordinado entre distintas áreas del gobierno provincial, que tiene como finalidad transmitir hábitos saludables y seguros durante el verano. Se despliega en espacios públicos con alta concurrencia de gente, tales como parques, plazas, playas y zonas comerciales.

El dispositivo ya se desarrolló en la ciudad de Rosario, Santa Fe y en la Ruta Nacional Nº11, a la altura de Sauce Viejo, y continúa recorriendo localidades costeras de norte a sur de la provincia.

“El gobernador Omar Perotti fue el impulsor de este operativo por el cual queremos demostrar que la provincia de Santa Fe no sólo tiene atractivos turísticos, sino también acciones de concientización, control y seguridad en las rutas para que todos puedan disfrutar de una muy buena temporada”, aseguró Alejandro Grandinetti, secretario de turismo de la provincia.

![](https://assets.3dnoticias.com.ar/sf_verano.jpeg)

**ACCIONES DE PREVENCIÓN**

En cada localidad donde se desarrolla el operativo “Santa FE Verano”, el área de salud entrega barbijos y alcohol en gel, a la vez que informa y promueve la importancia del distanciamiento social y la higiene de manos como la mejor forma de cuidarnos entre todos y no propiciar el contagio del virus. También, en algunos casos, los equipos de promotores sanitarios brindan información sobre Dengue, entregando repelente contra insectos desarrollados por el Laboratorio Industrial Farmacéutico (LIF).

Jorge Prieto, secretario de Salud de Santa Fe, sostuvo que “estamos acompañando a la población en estas vacaciones tan esperadas, luego de un año complejo. Aprendimos a socializar y comunicarnos de una forma diferente y ahora tenemos que aprender a habitar y pasear de una forma diferente. Por todo esto, este verano tiene que ser el más cuidado, con el apoyo de un Estado presente para que cada uno que salga de vacaciones pueda disfrutar de los hermosos lugares que tiene nuestra provincia”.

Por su parte, Seguridad Vial insta a tomar recaudos a la hora de conducir y hacerlo en forma segura para disfrutar de las vacaciones en familia.

Mientras que la Secretaría de Turismo promueve formas seguras de viajar y disfrutar en familia, conociendo nuestra provincia, valorando los ambientes naturales, todo con pautas de estricto cuidado frente al virus.

![](https://assets.3dnoticias.com.ar/sf_verano1.jpeg)

**SANTA FE VERANO**

Santa Fe Verano es un programa multiagencial en el que participan los Ministerios de Salud, Seguridad y Cultura, junto con la Secretaría de Turismo y la Agencia de Desarrollo (ADER), en la ciudad de Santa Fe.

El objetivo es cuidar la temporada estival y realizar diferentes actividades, entre las que se destacan los Test de Anosmia y Detectar, como así también el control de las nuevas medidas de restricción a la circulación nocturna.

![](https://assets.3dnoticias.com.ar/sf_verano2.jpeg)