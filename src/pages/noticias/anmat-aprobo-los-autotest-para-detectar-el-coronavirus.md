---
category: Agenda Ciudadana
date: 2022-01-06T06:00:17-03:00
thumbnail: https://assets.3dnoticias.com.ar/AUTOTEST.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: ANMAT aprobó los autotest para detectar el coronavirus
title: ANMAT aprobó los autotest para detectar el coronavirus
entradilla: 'Luego de varias idas y vueltas la Administración dio visto bueno a su
  uso, por el aumento de casos y el colapso en los centros de testeo.

'

---
Hace instantes la Administración Nacional de Medicamentos, Alimentos y Tecnología Médica, ANMAT, aprobó el uso y venta de cuatro autotest para detectar coronavirus: Abbott, Roche, Vyam Group y Wiene. Luego de varias idas y vueltas la decisión se llevó a cabo por la escalada de casos por las variantes Delta y Ómicron y el colapso en los principales centros de testeo.

El paso siguiente es que el Ministerio de Salud reglamente su uso e implementación de los test rápidos que serán vendidos en farmacias de todo el país y adquiridos de modo individual a través de instituciones públicas o privadas, o jurisdicciones.

Ayer el ministro de salud porteño, Fernán Quirós, había solicitado la aprobación inmediata para que los centros de la ciudad no colapsen.

El resultado de la prueba debe ser reportado dentro de las 24 horas de realizada a través del código de barra de cada empaque y se dispondrá de un plazo mayor cuando no fuese utilizado, ya sea por el usuario individual o por un responsable de reporte en caso de gran volumen de test.