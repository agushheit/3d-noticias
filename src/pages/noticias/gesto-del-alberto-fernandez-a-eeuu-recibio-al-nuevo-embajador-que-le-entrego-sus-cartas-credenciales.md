---
category: Agenda Ciudadana
date: 2022-01-25T06:00:40-03:00
thumbnail: https://assets.3dnoticias.com.ar/STANLEY.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: 'Gesto del Alberto Fernández a EEUU: recibió al nuevo embajador que le entregó
  sus cartas credenciales'
title: 'Gesto del Alberto Fernández a EEUU: recibió al nuevo embajador que le entregó
  sus cartas credenciales'
entradilla: 'Se trata de Marc Stanley, un dirigente cercano a Joe Biden que asumió
  formalmente el cargo tras presentarse ante el Presidente en Casa Rosada.

  '

---
El presidente Alberto Fernández recibió este lunes en la Casa Rosada al nuevo embajador de Estados Unidos en Buenos Aires, Marc Stanley, quien le entregó al mandatario sus cartas credenciales.

El encuentro formó parte del protocolo para el inicio de las funciones de un representante diplomático, que en este caso es además un hombre muy cercano al presidente de Estados Unidos, Joe Biden.

En ese contexto, se destacó que Fernández recibiera personalmente las cartas credenciales de Stanley, dado que otros embajadores que se encuentran en el país no han tenido aún encuentros con el Presidente o hicieron su presentación en grupo.

Al finalizar la reunión, Fernández destacó el fortalecimiento de la relación bilateral entre la Argentina y Estados Unidos, mientras que el flamante embajador calificó el encuentro como "excelente", al tiempo que ponderó los vínculos que existen entre los gobiernos de ambos países.

"Es un honor para mí haber sido elegido para cumplir las tareas de embajador ante un gran país como Argentina", señaló Stanley tras el encuentro del que también participaron el jefe de Gabinete, Juan Manzur; el canciller, Santiago Cafiero, y la ministra Consejera de la Embajada de Estados Unidos, MaryKay Carlson.

"Hoy presenté mis cartas credenciales al presidente Alberto Fernández, a quien agradezco su cálida recepción. Es un honor servir como Embajador de Estados Unidos en Argentina y trabajar para fortalecer la amistad y colaboración entre nuestros países", expresó Stanley a través de Twitter.

La nominación de Stanley a la Embajada estadounidense en Argentina fue anunciada por Biden el 6 de agosto de 2021 y confirmada el 17 de diciembre, por lo que el último paso que faltaba para el inicio formal de su misión diplomática era la presentación de las cartas credenciales.

Stanley es descripto como un dirigente muy cercano a Biden, para cuya campaña electoral trabajó activamente, y su asunción formal como embajador se da en medio del acercamiento que el Gobierno intenta con Estados Unidos por la negociación con el Fondo Monetario Internacional (FMI).

El nuevo representante diplomático de Estados Unidos llegó a la Argentina hace cuatro días y fue recibido en el Aeropuerto Internacional de Ezeiza por MaryKay Carlson, quien hasta el momento estuvo al frente de la Embajada.

Tras la asunción formal de Stanley, la diplomática realizó una despedida a través de la cuenta oficial de Twitter de su oficina, al anunciar que a partir de ahora será el flamante embajador quien se hará cargo de las comunicaciones oficiales por ese medio.

"Ha sido un placer y un orgullo conversar con ustedes y aprender más sobre Argentina. A partir de hoy, el embajador Marc Stanley hará uso de esta cuenta. Yo continúo en la Embajada como Ministra Consejera. Gracias por este año de Twitter juntos, los dejo en las mejores manos", señaló Carlson.