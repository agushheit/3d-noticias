---
layout: Noticia con imagen
author: "Fuente: El Litoral"
resumen: Convocatoria para donar plasma
category: La Ciudad
title: Nueva convocatoria a recuperados de Covid-19 para que donen plasma
entradilla: Quienes cursaron la enfermedad y tienen el alta poseen anticuerpos
  que pueden beneficiar a pacientes en curso. Ya hubo 50 donaciones y se
  practicaron 31 transfusiones.
date: 2020-10-22T16:13:01.879Z
thumbnail: https://assets.3dnoticias.com.ar/plasma.jpg
---
En el Hospital Iturraspe se realiza la recepción del pre-donante para el análisis y posterior donación de plasma. Las personas recuperadas de Covid-19 poseen en el plasma de su sangre anticuerpos que pueden beneficiar a quienes están cursando la enfermedad. Por ese motivo, son convocadas en medio de la pandemia para ayudar a llevar adelante el tratamiento de los enfermos por el virus.

Desde la puesta en marcha del operativo, hubo varias consultas y ofrecimientos de donación de plasma, de las cuales se pudieron hacer efectivas hasta el momento 50 donaciones. Cabe mencionar que por cada donación de generan 3 unidades, es decir que el plasma de las 50 donaciones sirve para realizar 150 transfusiones. Y hasta el día de hoy se realizaron 31 transfusiones en la ciudad, siempre a través del método de aféresis.

La modalidad es la siguiente: a cada persona con intenciones de donar plasma se le asigna un turno en el Hospital Iturraspe. Lo primero que se realiza es una entrevista y examen clínico. Si la persona presenta las condiciones óptimas para continuar con el proceso de donación de plasma, se le toman muestras de sangre para enviar al Centro Regional de Hemoterapia en Rosario para su análisis. En caso de que la sangre sea apta para la donación, se continúa con el proceso; en caso negativo se desestima.



**PROCEDIMIENTO**

Las personas con resultados aptos para la donación de plasma, son comunicadas por la institución y se coordina el día para que se acerquen al hospital a realizar la donación. Es importante aclarar que, en la institución se aceptan pre-donantes mujeres y hombres (ver Requisitos). "Tenemos un mínimo de dos donantes diarios, hay días en los esa cifra se duplica", dijo Dra. Rosanna Valentini, integrante del Cudaio, quien además agregó que en un momento debieron "parar, por un desperfecto en las máquinas, pero ya se solucionó".

**REQUISITOS**

Para poder donar plasma se deben considerar los siguientes requisitos:

* Cumplir con todos los requisitos exigidos para un donante habitual de sangre.
* Contactarse 28 días después del alta médico de Covid-19.



**No todos pueden donar**

"Hay que recordar que hubo unos 70 días sin donantes en la ciudad, porque los casos no aparecían y no había enfermos", explicó luego la doctora. "A partir de que comenzaron los contagios, ahora hay una lista muy larga de donantes voluntarios a los que hemos convocado".

No todos pudieron donar porque algunos no cumplieron los requisitos para hacerlo", dijo Valentini, quien estimó que el "25% de los que se comunicó no está en condiciones, no cumple las características o no contaban con anticuerpos".

"Con el compromiso ciudadano, vamos a superar juntos esta pandemia. Sé donante de plasma", expresa un comunicado dado a conocer por el Iturraspe.



**PARA DONAR**

Si la personas que es potencial donante, cumple con los requisitos y quiere donar plasma, debe comunicarse al teléfono: 342-4777777 (Doctora Rosanna Valentini) o completar un formulario del CUDAIO en: cudaio.gob.ar/donacion-de-plasma. Por dudas o consultas, escribir a donaplasma@santafe.gob.ar