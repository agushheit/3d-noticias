---
category: Estado Real
date: 2021-04-14T07:38:05-03:00
thumbnail: https://assets.3dnoticias.com.ar/credito.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: La provincia presentó la línea de créditos para la Economía Circular
title: La provincia presentó la línea de créditos para la Economía Circular
entradilla: 'Estará destinada a pymes, organizaciones sociales y emprendedores para
  proyectos que regeneren recursos y contribuyan a la mitigación del cambio climático. '

---
El gobierno de Santa Fe, a través del Ministerio de Ambiente y Cambio Climático, avanza en el desarrollo de un Plan de Economía Circular. En ese marco, presentó este martes una línea de créditos para pymes, organizaciones sociales y emprendedores para proyectos que regeneren recursos y contribuyan a la mitigación del cambio climático.

El acto que se desarrolló de manera virtual, estuvo encabezado por la ministra de Ambiente y Cambio Climático, Erika Gonnet,  acompañada por su par de Producción, Ciencia y Tecnología, Daniel Costamagna; el senador nacional, Roberto Mirabella; el secretario general de CFI, Ignacio Lamothe; y la secretaria de Articulación con la Comunidad de UNRaf, María Cecilia Gutiérrez. Además, participaron el ministro de Economía, Walter Agosto, y el director del Laboratorio de Economía Circular de UNRafTec, Alejandro Jurado.

Estas acciones forman parte de un trabajo conjunto entre el gobierno provincial, el Consejo Federal de Inversiones y la Universidad Nacional de Rafaela. El anuncio se realizó durante el lanzamiento del ciclo de charlas “Desafío Circular Santa Fe”, con referentes destacados del sector productivo, académico y social, que vienen aplicando los principios de la economía circular y la bioeconomía.

En la apertura del encuentro, Gonnet afirmó: “En 2020 comenzamos un estudio con un equipo técnico que analizó veinte cadenas de valor con potencial de incorporar conceptos de economía circular”, valoró la ministra. Además, contó que “el fortalecimiento y las líneas de crédito son herramientas para avanzar hacia la economía circular pero también para acompañar la reactivación económica provincial; una iniciativa del gobierno de Santa en la que intervienen distintos actores, tenemos una articulación permanente con los ministros de Producción y Economía”.

En ese sentido, se priorizaron 10 sectores estratégicos: reciclaje; producción forestal; agroecología; ganadería sustentable y regenerativa; bioenergía; turismo de naturaleza; valorización de subproductos; biomateriales; construcción sostenible; y economía colaborativa.

Este estudio comenzó con un diagnóstico multidimensional orientado a comprender los elementos claves del territorio. Para ello, el equipo técnico analizó 20 cadenas de valor existentes con potencial de transicionar o incorporar los principios de la economía circular y la bioeconomía. La siguiente etapa, que acaba de iniciar, es de planificación, a través de la formulación de los aspectos principales del futuro plan provincial, y la propuesta de marco regulatorio para acelerar la transición desde una economía lineal, con externalidades ambientales y sociales negativas hacia la economía circular, regenerativa e inclusiva.

Por su parte, Costamagna dio detalles de la línea de créditos: “En esta unidad de enlace entre los ministerios y con el financiamiento de CFI ponemos créditos a disposición de micro emprendedores, de micro empresas y pymes, adelantándonos a las cuestiones ambientales y productivas en un mercado internacional cada vez más exigente y con futuros problemas a los cuales creemos que hay que anticiparse”.

Además, expresó: “Vamos a apoyar a las empresas con créditos a sola firma, orientándonos a emprendedores que quieran iniciar proyectos de estas características que nos interesa estimular. Y también vamos a apoyar a las pymes con otra línea de créditos desde 500 mil hasta un millón de pesos”.

“Queremos resaltar que son créditos para acompañar todo el esfuerzo que se hace desde el Ministerio de Ambiente. Hoy plantando esta semilla buscamos un futuro con un desarrollo importante para la provincia, no sólo en lo productivo y medio ambiental, sino también en la diferenciación de nuestros productos”, concluyó el ministro.

A su turno, Lamothe celebró la iniciativa de la provincia de Santa Fe e indicó: “Creo que vamos a la economía circular por la fuerza; por el nivel de consumo y porque el planeta no tiene los recursos para abastecer a los seres humanos”.

Asimismo, aprovechó la oportunidad para recordar ante los presentes el lanzamiento de CFI de la red federal de espacios de fabricación digital. “Estamos asistiendo a una megatendencia que se conoce como diseño distribuido o dato distribuidos. El gobierno de Santa Fe está apostando a la multiplicación de espacios de fabricación digital. Con CFI lanzamos la red federal de espacios de fabricación digital, que están proliferando a gran velocidad. Son espacios para captar información de cómo se produce, diseño, y reproducción a escala social de estos procesos”, detalló.

Por su parte, María Cecilia Gutiérrez, secretaria de Articulación con la Comunidad de UNRaf, resaltó que “el rol de la universidad cambió en estos años. Una universidad sin territorio ni actores de la sociedad se queda sin valores. La UNRaf tiene seis años, es muy joven, sin embargo se caracteriza por proyectos que tienen mucho de información, creatividad y protección del medio ambiente. La economía circular es uno de ellos”, agregó.

Al cierre, el senador Mirabella afirmó que "es clave que profundicemos desde Santa Fe la discusión de la producción en argentina y que el país sea clave en la producción de biocombustibles", y remarcó que "el mundo está yendo hacia ahí".

En ese sentido indicó que "en nuestra provincia el biodiesel es líder. El 80 por ciento de capacidad de producción está en Santa Fe. Esto es por la gran cantidad de biomasa", y concluyó: "Nosotros planteamos un proyecto para poner en debate este tema, y que forme parte del desarrollo de la economía circular".

**DESAFÍO CIRCULAR SANTA FE**

El ciclo de webinars comenzó este martes con la exposición “Ganadería regenerativa”. La misma estuvo a cargo de Pablo Prellasco, de FVSA ganadería sustentable de pastizal; Leticia Mesa, de INALI UNL CONICET; Jorge Torelli, secretario de Agroalimentos de la provincia; Pablo Borelli, de OVIS ganadería regenerativa; e Ignacio Corominas, de Establecimiento Don Luis; y tuvo como moderador al Director del Laboratorio de Economía Circular de UNRafTec, Alejandro Jurado.

El segundo encuentro está previsto para el miércoles 21 de abril bajo el título “Acción Climática”, mientras que “Regeneración Ecosistémica” será el tercer webinar el día 28. En mayo se desarrollarán “Producción de árboles nativos”, “Biomateriales – Bioplásticos”, Acuicultura ecológica” y “Construcción sostenible”; en junio “Apicultura”, “Agroecología”, Recuperación, reutilización y reparación”, “Reciclaje inclusivo” y “Valorización de subproductos y pérdidas”; en julio, el ciclo cerrará con los encuentros “Bionergía”, “Turismo regenerativo”, “Ecoturismo y humedales” y “Alimentación circular”.

Durante los encuentros participarán funcionarios provinciales quienes expondrán, en cada una de las temáticas, las diversas acciones que el gobierno viene implementando y proyectando. Es por eso que, durante el encuentro de hoy participaron funcionarios de los ministerios de Ambiente y Cambio Climático, de Producción, Ciencia y Tecnología, y de Economía.

El mismo está destinado a municipios y comunas, empresas, cooperativas, asociaciones y productores. Las y los interesados podrán inscribirse en [https://bit.ly/39XIPdr.](https://bit.ly/39XIPdr. "https://bit.ly/39XIPdr.")