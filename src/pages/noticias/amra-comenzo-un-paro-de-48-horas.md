---
category: La Ciudad
date: 2020-12-30T11:48:40Z
thumbnail: https://assets.3dnoticias.com.ar/3012-amra.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: AMRA comenzó un paro de 48 horas
title: AMRA comenzó un paro de 48 horas
entradilla: Los médicos no están de acuerdo con la oferta que hizo el Gobierno que
  contempla un 6% de aumento en blanco y otras cifras en negro. Como es habitual,
  se cumplen las guardias mínimas.

---
El sindicato de Médicos de Santa Fe, AMRA, comenzó un paro de 48 hs. ante el malestar que generó la oferta salarial que les hizo el Gobierno.

Recordemos que el Gobierno les ofreció un 6% de aumento en diciembre, en blanco y un 4% en negro. Oferta similar, a la que ya aceptaron los docentes.

Por este motivo, solo se cumple con las guardias mínimas en hospitales y efectores de salud.

En diálogo con LT10, Néstor Rossi, dirigente de AMRA, sostuvo que «no es una postura dura, como el Gobierno cree. Ellos mantienen las cifras en negro y nosotros las rechazamos por una simple razón: si las pueden dar en blanco, ¿por qué nos las van a dar en negro? También están los pases a plantas: el incumplimiento con lo que se firmó en septiembre. Con eso sobra como para tomar una y diez medidas de fuerza».