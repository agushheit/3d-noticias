---
category: La Ciudad
date: 2021-05-16T06:00:32-03:00
thumbnail: https://assets.3dnoticias.com.ar/gsolano.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'Santo Tomé: la concejala Gabriela Solano pide por dos espacios verdes'
title: 'Santo Tomé: la concejala Gabriela Solano pide por dos espacios verdes'
entradilla: '"Desde hace tiempo venimos trabajando, gestionando y haciendo énfasis
  en la necesidad alcanzar una ciudad más limpia, sostenible y amigable con el medio
  ambiente", dijo.'

---
La concejala por el PS, Gabriela Solano, ingresó dos propuestas al Concejo que persiguen la puesta en valor de dos espacios verdes de la ciudad de Santo Tomé, la manzana N° 1727, entre las calles Buenos Aires y Entre Ríos; y en la manzana N° 3031, entre las calles Santiago del Estero y Aristóbulo del Valle; según consta en la página web oficial de la Municipalidad de Santo Tomé.

Al respecto, Solano declaró: "La puesta en valor y recuperación de los espacios verdes son aristas claves para nuestra banca. Desde hace tiempo venimos trabajando, gestionando y haciendo énfasis en la necesidad alcanzar una ciudad más limpia, sostenible y amigable con el medio ambiente, porque consideramos que es fundamental para mejorar la calidad de vida de la comunidad y alcanzar un bienestar sostenido en el tiempo".

El proyecto solicita al Departamento Ejecutivo Municipal la posibilidad de proceder a realizar estudios de factibilidad técnica y económica para la puesta en valor de las dos áreas mencionadas, teniendo en cuenta los siguientes ítems: forestación, iluminación, veredas y señalética.

"La puesta en valor de estos espacios verdes ha sido solicitada por vecinas y vecinos", explicó la edil y contó: ''Hace poco, hicimos una visita a los barrios, allí la gente nos transmitió esa inquietud. Con mi equipo asumimos el compromiso y decidimos materializar los proyectos e ingresarlos al Concejo Municipal".

Por su parte, se espera que el cuerpo legislativo acompañe la iniciativa de los dos proyectos de resolución.

"Se trata de intervenir estos lugares que hoy son baldíos o microbasurales y transformarlos en espacios verdes que sean funcionales a la integración social, la urbanización, la recreación, la educación. Para mi banca son puntos que devienen indispensables en la vida social de las personas", finalizó la presidenta del Concejo Municipal.