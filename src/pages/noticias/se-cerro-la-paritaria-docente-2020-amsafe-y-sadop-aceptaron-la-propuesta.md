---
category: La Ciudad
date: 2020-12-30T10:45:32Z
thumbnail: https://assets.3dnoticias.com.ar/3012-Paritarias-Docentes.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: LT10'
resumen: 'Se cerró la paritaria docente 2020: Amsafe y Sadop aceptaron la propuesta'
title: 'Se cerró la paritaria docente 2020: Amsafe y Sadop aceptaron la propuesta'
entradilla: El ofrecimiento del gobierno comprende una suma remunerativa y bonificable
  y una no remunerativa. Los gremios docentes y los funcionarios provinciales volverán
  a reunirse en febrero.

---
En el transcurso de este martes, Amsafe y Sadop informaron que aceptaron la propuesta de incremento de sueldos realizada por el gobierno provincial. De esta forma, se cerró por este 2020 la discusión salarial del sector docente.

En el caso del gremio de maestros públicos, fue luego de un plenario de delegados regionales, modalidad que desde el inicio de la pandemia reemplaza a las históricas asambleas provinciales.

En cuanto al Sindicato de Docentes Particulares, la decisión de la aceptación fue tomada por el congreso provincial (seccionales Rosario y Santa Fe).

Según se informó desde Sadop, se estableció «la continuidad de la lucha por la incorporación de las sumas no remunerativas y bonificables, el monitoreo constante de la situación sanitaria para establecer las condiciones de regreso al trabajo a los edificios escolares, el reconocimiento total y absoluto a las y los docentes santafesinos que durante el año y a pesar de las malas condiciones laborales fueron garantes del derecho a la educación de niñas, niños y adolescentes y el compromiso de continuar luchando en el 2021 por mejores condiciones laborares y salariales».

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.1em; font-weight: 700">EL OFRECIMIENTO </span>**

A grandes rasgos, el aumento se compone de dos partes: una remunerativa y bonificable que se liquidará en la misma forma y condiciones que los aumentos porcentuales otorgados durante enero y febrero 2020. Esto se complementa con un incremento del 37% sobre las sumas adicionales, no remunerativas y no bonificables que se cobraron a partir de noviembre.

En estas nuevas escalas, la suba -en promedio- representa un porcentaje que va del 10% al 13% según el cargo y la antigüedad. También se fijan con una actualización del 200% las asignaciones familiares y que todas las sumas acordadas (tanto remunerativas como no remunerativas) se trasladan a jubilados y pensionados de manera proporcional.

**La paritaria docente se retomará el 5 de febrero de 2021**.