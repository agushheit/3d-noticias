---
category: La Ciudad
date: 2021-07-22T06:15:18-03:00
thumbnail: https://assets.3dnoticias.com.ar/REPROGRAMACION.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Se reprogramaron turnos de vacunación en La Redonda
title: Se reprogramaron turnos de vacunación en La Redonda
entradilla: Corresponden a personas que debían vacunarse el próximo viernes en el
  ex taller ferroviario, reprogramando sus turnos para este sábado para no sobrecargar
  el vacunatorio, afirman desde Salud.

---
En la continuidad del operativo de vacunación contra el Covid en la ciudad de Santa Fe, el ministerio de Salud provincial reprogramó turnos correspondientes al día viernes 23 de julio en La Redonda. Los turnos de inoculación que fueron reprogramados se pasaron para el día sábado 24 de julio en el mismo centro vacunatorio.

Desde el ministerio de Salud provincial informaron a UNO que la reprogramación de turnos se llevó a cabo para no saturar la capacidad del nuevo vacunatorio ubicado en el predio del Parque Federal, dividiendo en dos días la vacunación planificada originalmente para el día viernes.

Se les notificó mediante SMS y mail a aquellas personas que sufrieron un cambio en su día de vacunación. Por esto, todos aquellos que debían inocularse durante el próximo viernes deben revisar su casilla de mensajes y correo electrónico, revisando si su turno fue modificado.

El vacunatorio en el predio La Redonda fue habilitado desde el pasado viernes 16 de julio, donde se está inoculando a un ritmo máximo de 800 turnos por día. El horario de vacunación es de 8 a 16, mientras que se desplegaron 10 puestos para inocular a los santafesinos. Además, se instalaron carpas para que el que lo desee pueda vacunarse sin bajarse de los autos en caso de que haya buen tiempo.