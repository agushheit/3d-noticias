---
layout: Noticia con imagen
author: 'Fuente: La Capital'
resumen: Híbridos para siembras tardías
category: El Campo
title: Más híbridos de alto potencial para siembras tardías
entradilla: NK presentó alternativas para esta modalidad de cultivo con tres híbridos
  de gran desempeño en diversas zonas productivas del país.
date: 2020-11-14T17:39:23.009+00:00
thumbnail: https://assets.3dnoticias.com.ar/nk1.jpg
link_del_video: ''

---
La adopción de materiales tardíos crece año tras año entre los productores de maíz. En línea con esta tendencia, NK recomienda tres nuevos híbridos de alta performance y mayor estabilidad que apuntan a esta modalidad de cultivo.

“Para quienes estén planificando sus lotes a mediados de noviembre y diciembre, NK tiene varias alternativas de materiales que ofrecen buena agronomía, sanidad y seguridad de cosecha, afianzando el plan de negocios ideado para estas fechas. Y todo esto, además, está acompañado por la tecnología Agrisure Viptera3, que ofrece el mejor control de lepidópteros del mercado”, asegura Francisco Pérez Brea, Gerente de Marketing de NK.

En este escenario, el porfolio de genética para maíz tardío se consolida con el SYN126 Viptera3 para la región norte del país y el SYN840 Viptera3 para la zona núcleo. “Además del rendimiento y la estabilidad, el perfil agronómico de estos materiales se destaca por la excelente resistencia al quebrado del tallo, lo que asegura el rinde y la integridad del cultivo desde la siembra hasta el momento de la cosecha”, aseguró Andrés Caggiano, gerente de Desarrollo de Producto Maíz y Girasol de NK.

La semillera recomienda el SYN126 Viptera3 para ambientes del NOA y el NEA. “Los insectos son más agresivos y dañinos en el norte que en la zona núcleo, por lo que la tecnología Viptera3 contra lepidópteros es un diferencial dentro de la región. Es un híbrido con excelente competitividad por su perfil sanitario y agronómico en Chaco, Santiago Del Estero, Tucumán, Salta y el norte de Santa Fe”, agregó Caggiano.

"Teniendo en cuenta que va a ser un año Niña, toma importancia elegir materiales con buena agronomía y sanidad”

Asimismo, el SYN505 VIP3 es otra opción de alto potencial de rendimiento para NOA y NEA. Tiene excelente perfil sanitario y muy buen perfil agronómico por contar con una eficiente protección contra lepidópteros. Su ciclo completo acompaña al rendimiento de los principales maíces que se siembran en la zona, ofreciendo versatilidad para diversificar al período más crítico.

El SYN840 Viptera3 también ofrece una gran performance tanto en la provincia de Córdoba, como en el oeste y sur de la provincia de Buenos Aires. Es que por su gran adaptabilidad es recomendado para siembras tempranas y tardías en ambientes restrictivos de toda la región maicera templada.

De esta manera, la semillera suma tres nuevos híbridos con tecnología Agrisure Viptera3, que llevan tranquilidad a los productores maiceros que quieren asegurar la mayor estabilidad y eficiencia productiva posible en las siembras tardías.

“Teniendo en cuenta que las proyecciones climáticas indican que va a ser un año Niña, lo que implica menos lluvias que el promedio histórico, y focalizándonos en que estamos próximos a la ventana de siembra tardía, toma una mayor importancia que el productor pueda disponer de materiales que ofrecen buena agronomía, sanidad y seguridad de cosecha. Planificar conjuntamente con los productores y acercarles soluciones acordes para cada realidad es clave para nosotros. El éxito de cada lote es nuestro éxito también”, concluye Pérez Brea.

![](https://assets.3dnoticias.com.ar/nk.jpg)

## **La empresa**

NK está de nuevo en el mercado argentino con una fuerte impronta innovadora y todo el respaldo de Syngenta. Durante 2020 demostrará todo su potencial genético y biotecnológico. En NK creemos en la innovación y por eso la impulsamos.

Syngenta es una de las principales empresas agrícolas del mundo. Su objetivo es mejorar la sustentabilidad, la calidad y la seguridad de la agricultura con ciencia de clase mundial y soluciones innovadoras para cultivos. Sus tecnologías permiten a millones de agricultores de todo el mundo hacer un mejor uso de los recursos agrícolas limitados. Con 28,000 personas en más de 90 países, trabaja para transformar cómo se producen los cultivos. A través de asociaciones, la colaboración y el programa TheGoodGrowth Plan, la compañía comprometidos con mejorar la productividad de los campos, rescatar las tierras de la degradación, mejorar la biodiversidad y revitalizar las comunidades rurales.