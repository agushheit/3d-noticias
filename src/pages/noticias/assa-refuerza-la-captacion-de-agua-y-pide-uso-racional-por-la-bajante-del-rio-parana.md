---
category: La Ciudad
date: 2021-06-19T06:00:38-03:00
thumbnail: https://assets.3dnoticias.com.ar/ASSA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Assa refuerza la captación de agua y pide uso racional por la bajante del
  Río Paraná
title: Assa refuerza la captación de agua y pide uso racional por la bajante del Río
  Paraná
entradilla: 'Este viernes el Río Paraná marcó 58 cm en el puerto de la ciudad de Santa
  Fe y las proyecciones indican que continuaría en descenso. '

---
En este sentido, informaron desde la empresa que concretaron y tienen en curso diversas acciones en plantas potabilizadoras que se abastecen del sistema río Paraná.

**Santa Fe:** Incremento de la capacidad de captación en las tomas sobre los ríos Colastiné y Santa Fe mediante la colocación de tres nuevas bombas.

**Reconquista:** La puesta en marcha de la toma del nuevo Acueducto Reconquista permite asegurar la captación de agua. En la toma de la antigua planta se extendieron las cañerías de las bombas existentes en el muelle de captación.

**Acueducto de la Costa (San José del Rincón):** Se instalaron dos nuevas bombas sumergibles para asegurar captación, que se suman a las fijas existentes en el muelle.

**Acueducto Centro Oeste (Planta Monje):** Se puso en funcionamiento una nueva bomba en la toma sobre el río Coronda. Se reubicaron las bombas existentes para mantener su capacidad de captación.

**Rosario:** Se reemplazó una de las grandes bombas que llevaba 60 años de funcionamiento por otra de última generación tecnológica. Se están instalando tres nuevas bombas suplementarias en una toma móvil adicional. En total la capacidad de captación se incrementa un 15 %.

**Cuidar el agua**

Además de las tareas realizadas y de las que se están haciendo, el presidente de Aguas Santafesinas, Hugo Morzan, señaló: “A todo lo que hagamos desde Aguas es imprescindible sumar el aporte de los usuarios, realizando un uso racional y solidario del agua potable".

En ese punto, el titular de Assa expresó: "Es necesario reducir el derroche de agua en el lavado de veredas o automóviles; acotar el riego de jardines y espacios verdes; evitar las pérdidas en cañerías y sanitarios; etc.".

"Nosotros seguimos realizando una prédica constante con las escuelas mediante nuestro programa ‘Aguas Educa’ y esperamos que esos mensajes que llegan a niños y niñas también se reflejen en conductas positivas de cuidado en cada hogar”, finalizó.