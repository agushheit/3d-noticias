---
category: Estado Real
date: 2021-10-09T06:15:49-03:00
thumbnail: https://assets.3dnoticias.com.ar/circunvalar.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Perotti y Guerrera dieron inicio a la obra del primer circunvalar ferroviario
  del país, que evitará que el tren atraviese la cuidad
title: Perotti y Guerrera dieron inicio a la obra del primer circunvalar ferroviario
  del país, que evitará que el tren atraviese la cuidad
entradilla: "“Hay una apuesta fuerte al ferrocarril como una herramienta clave para
  la producción, el trabajo, la generación de riqueza y la posibilidad, a través de
  esas vías, de llegar a los puertos”, dijo el gobernador."

---
El gobernador de la provincia de Santa Fe, Omar Perotti; y el ministro de Transporte de la Nación, Alexis Guerrera, encabezaron este jueves el acto de inicio de los trabajos que modificarán la traza del Belgrano Cargas, gracias a la construcción de 15,5 kilómetros de vías y un nuevo puente ferroviario en la capital provincial.

La Circunvalar Santa Fe del Belgrano Cargas comprende 60,5 kilómetros de vías con un recorrido desde Santo Tomé hasta Laguna Paiva. El proyecto, sin antecedentes en la historia ferroviaria argentina, permite que el tren rodee la ciudad de Santa Fe en lugar de atravesarla, y evita 61 pasos a nivel en medio de la ciudad, lo que mejora la seguridad vial, reduce el viaje en ese tramo a un cuarto del tiempo y también los costos logísticos en un 30%. Cabe recordar que el pasado 29 de septiembre se llevó a cabo en Casa Rosada la firma del acta de inicio de obra, que contó con la presencia del presidente Alberto Fernández.  
  
Por otra parte, previamente se avanzó en la firma de convenios para impulsar el desarrollo de la actividad portuaria de la región, ya que tienen como fin el desarrollo logístico y económico del transporte portuario y ferroviario, tanto de pasajeros como de cargas, como parte del Plan de Modernización del Transporte.  
  
En la oportunidad, el gobernador Omar Perotti afirmó que “fue el presidente Alberto Fernández el que tomó la decisión de destrabar y continuar con el financiamiento para que el ferrocarril Belgrano recupere su traza y para las adendas que nos permitían las otras obras complementarias. Esa decisión política es la que nos tiene hoy aquí a los santafesinos como protagonistas. Las vías más modernas de la Argentina hoy están en nuestra provincia; son las del ferrocarril Belgrano que han unido Santa Fe–San Justo, San Justo-San Cristóbal y desde allí al norte”, dijo.

“Esos ramales se han ido complementando con obras que han permitido que localidades entrañables con el vínculo con el ferrocarril, como San Cristóbal y Laguna Paiva, vuelvan a retomar vida. Aquí hay una apuesta fuerte al ferrocarril como una herramienta clave para la producción, el trabajo, la generación de riqueza y para la posibilidad, a través de esas vías, de llegar a los puertos y, de allí, al mundo a traer divisas”, agregó el gobernador.

“Son seis mil millones de pesos que hoy ha traído el ministro de Transporte de la Nación Argentina a la provincia de Santa Fe; más de cuatro mil en la obra del circunvalar. En esta obra van a trabajar 900 trabajadores y trabajadoras, es la importancia de la obra pública vinculada a la recuperación y todas las inversiones que se van generando por donde el ferrocarril va a impactando en el día a día. Esta es una señal clara de mostrarle a todos los argentinos que hay un país en marcha, que hay un interior con fuerte vocación de crecimiento y de recuperación”, concluyó Perotti.

A su turno, el ministro Guerrera afirmó: “Estamos acá porque creemos en la Argentina productiva y que se desarrolla, en la Argentina de los capitales nacionales, pero también de la participación de las fuertes inversiones internacionales para desarrollar esa logística que tanto necesita un país productor de alimentos. Estamos reunidos en el inicio de una obra, no estamos firmando ya un acta compromiso, ni estamos adjudicándola como sucedió hace un tiempo atrás”, afirmó el funcionario.

“Esta obra también es el disparador de muchas otras necesidades que tiene no solamente la provincia de Santa Fe, sino también la Argentina, para desarrollar un sistema de transporte moderno, no solamente para la carga, sino también para los pasajeros. Argentina necesita de un gran desarrollo logístico y es justamente lo que se plantea”, agregó el ministro nacional.  
  
En tanto, la ministra de Infraestructura, Servicios Públicos y Hábitat, Silvina Frana, recordó “las reuniones que tuvimos por orden del gobernador Omar Perotti, para ver la forma de que este sueño se hiciera realidad. Cuando llegamos a la gestión había una obra para adjudicar, y se fueron cumpliendo un conjunto de etapas a partir de un trabajo mancomunado”.

En este sentido, destacó la “decisión del gobierno nacional y provincial de poner a la Argentina y a Santa Fe de pie, a través de la obra pública y de la infraestructura que tiene que ver con el desarrollo”; y resaltó que iniciar esta obra “significa mucho más que volver al tren, significa producción, crecimiento, desarrollo, trabajo; y nos permite soñar con un futuro diferente, con una inclusión diferente”.

Por su parte, el presidente de Trenes Argentinos Operaciones e Infraestructuras, Martín Marinucci, manifestó su “enorme alegría por ver trabajadores y máquinas empezando a llevar a cabo un proyecto de mucho tiempo, que tenía que ver con pensar en un país que lleve su producción a través de los trenes sin generar riesgo a la ciudadanía, en este caso de Santa Fe. Esta obra no solamente tiene que ver con generar mayor comodidad al tránsito urbano, al transporte ferroviario, sino con darle mayor seguridad a cada paso del tren en esos 62 pasos a niveles que tenemos en la ciudad”, expresó.

Asimismo, recordó que es “un proyecto que el gobernador nos planteó como una necesidad imperiosa, para llevar adelante esta obra que se empezó a licitar hace mucho tiempo, y que era necesaria la decisión del presidente Alberto Fernández para llevarla adelante”.  
  
**DETALLES DE LA OBRA CIRCUNVALAR SANTA FE**  
La obra ferroviaria Circunvalación de la ciudad de Santa Fe representa un proyecto estratégico y contempla la construcción de 15,5 kilómetros nuevos de vías y la modernización de otros 45 kilómetros. A su vez, se creará un nuevo puente ferroviario de 900 metros de largo sobre el río Salado y tres nuevos puentes vehiculares sobre las Ruta Provincial 70, 11 y 4, para cruces a distinto nivel. También, se construirán 53 alcantarillas nuevas; se realizará el cerramiento para zona rural y urbana y la relocalización de la estación de bombeo de Santo Tomé, manteniéndose las cuatro bombas actuales y con el agregado de cuatro nuevas.  
  
Se trata de la primera vez en la historia argentina que se realiza una circunvalación ferroviaria de esta envergadura para evitar el paso del tren de carga por una ciudad, lo que beneficiará a más de 525.000 habitantes del Gran Santa Fe. Los objetivos planificados por el Ministerio de Transporte de la Nación que conduce Guerrera, son mejorar el acceso de la producción agraria del norte argentino a los puertos de exportación y facilitar la circulación ferroviaria del Belgrano Cargas al evitar el recorrido del tren por el entramado urbano.  
Además, la obra generará más de 900 nuevos puestos de trabajo, reducirá los costos logísticos un 30%, permitiendo incrementar la cantidad y longitud de trenes que atraviesan el Gran Santa Fe a diario y reduciendo 2 horas el tiempo de circulación. También mejorará la seguridad vial y la conectividad de los santafesinos y santafesinas que transitan por la zona, evitando cruces ferroviarios y facilitando la movilidad de vehículos y peatones.  
  
**DETALLES DE LOS CONVENIOS FIRMADOS**  
En primera instancia, las autoridades llevaron a cabo la firma de un Convenio Marco, que tiene por objeto sentar las bases para desarrollar actividades de cooperación y colaboración para el mejoramiento de los niveles de eficacia y eficiencia de las operaciones portuarias para los puertos santafesinos. El Convenio Marco fue firmado por el gobernador Omar Perotti y el ministro Guerrera.  
Además, se firmó un convenio para el mejoramiento de la infraestructura y servicios portuarios del Puerto de Santa Fe, que demandará una inversión por parte del Ministerio de Transporte de $200.000.000, y contempla obras en la Terminal de Contenedores y Cargas Generales; la Terminal Multipropósito; la Terminal de Agrograneles y la adquisición de nuevos equipos. El mismo fue rubricado por Guerrera; y el presidente del Ente Administrador del Puerto de Santa Fe, Carlos Arese.  
  
El tercer convenio fue firmado junto al Ente Administrador Puerto Reconquista (EAPRe) y tiene como objeto la reactivación del Puerto Reconquista y contempla una inversión de $200.000.000. Se llevará a cabo la reparación y modernización de los frentes de atraque; construcción de silos de almacenamiento, galpones, sistemas de carga incluida la fijación de la cinta transportadora al muelle; reparación del pavimento portuario; dragado del canal de acceso y adquisición de equipamiento para la señalización y la operatividad del puerto, entre otras tareas. Firmaron el convenio el ministro nacional y el interventor del Puerto de Reconquista, Martín Deltin.

El cuarto convenio de cooperación fue firmado entre el Ministerio de Transporte de la Nación, la provincia de Santa Fe, las municipalidades de las ciudades de Santa Fe y Santo Tomé y la Universidad Nacional del Litoral (UNL) para la elaboración del estudio sobre la viabilidad técnica, económica y operativa respecto del servicio ferroviario de pasajeros en el tramo ciudad Santa Fe–Santo Tomé. Dicho acuerdo fue firmado por Perotti, Guerrera, el intendente de Santa Fe, Emilio Jatón; la intendenta de Santo Tomé, Daniela Qüesta; y el rector de la UNL, Enrique Mammarella.

Por último, Perotti y Guerrera firmaron un convenio con el intendente Jatón por medio del cuál, el Ministerio de Transporte financiará la obra de refacción de la terminal de colectivos de la ciudad de Santa Fe, con un aporte de $104.696.874.

Allí, el representante de la Bolsa de Comercio de Santa Fe, Pedro Cohan, brindó detalles sobre “un compendio de obras que todas las instituciones de la región consideran indispensables para la actividad productiva, y la dotación de infraestructura que ya tiene la ciudad y su área metropolitana. Este trabajo ha sido consensuado con todas las entidades de la región y tiene incluida información que nos han dado los organismos públicos, corroborando el estado de las acciones”.

**TIMBÚES**  
La jornada culminó con una visita del gobernador Perotti y del ministro Guerrera a Puertos Timbúes, donde Transporte desarrolla la construcción de 11 kilómetros nuevos de vías que permitirán el acceso de las formaciones ferroviarias al complejo agroindustrial, disminuyendo los tiempos operativos de descarga y los costos logísticos.

**PRESENTES**  
Participaron de las actividades el secretario de Gestión de Transporte, Diego Giuliano; el vicepresidente de Trenes Argentinos Cargas, Martín Gainza; los ministros de Producción, Ciencia y Tecnología, Daniel Costamagna, y de Gestión Pública, Marcos Corach; el secretario de Empresas y Servicios Públicos, Carlos Maina; la secretaria de Gestión Federal, Candelaria González del Pino; el secretario de Transporte, Osvaldo Miatello; la secretaria Técnica, Juliana Armendáriz; y el secretario de Comercio Exterior, Germán Bürcher.

Además, estuvieron presentes los intendentes de Santa Fe, Emilio Jatón; de Santo Tomé, Daniela Questa; de Esperanza, Ana María Meinners; de Rafaela, Luis Castellano, y los presidentes comunales de Sauce Viejo, Pedro Uliambre, y de Empalme San Carlos, Hugo Heinzen. Asistieron también, el presidente y el vicepresidente de la Bolsa de Comercio de Santa Fe, Martín Vigo Lamas y Ulises Mendoza, respectivamente; el rector de la Universidad Nacional del Litoral, Enrique Mammarella; el presidente y el vicepresidente de Unión Industrial de Santa Fe, Alejandro Taborda, y Guido Montes de Oca, respectivamente; el vicepresidente 1° y el gerente de la Cámara de Comercio Exterior de Santa Fe, Julio de Biasi; Carlos Rafaelli, respectivamente; el presidente de la Sociedad Rural de Santa Fe, Ignacio Mántaras; el presidente del Ente Administrador Puerto Santa Fe, Carlos Arese, y el interventor Puerto de Reconquista, Martín Deltín.