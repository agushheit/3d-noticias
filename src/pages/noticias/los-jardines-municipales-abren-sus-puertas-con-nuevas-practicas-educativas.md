---
category: La Ciudad
date: 2020-12-04T10:52:31Z
thumbnail: https://assets.3dnoticias.com.ar/JARDINES.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Los jardines municipales abren sus puertas con nuevas prácticas educativas
title: Los jardines municipales abren sus puertas con nuevas prácticas educativas
entradilla: 'La vuelta a clases en los Jardines Municipales se garantiza según los
  protocolos vigentes, cumpliendo con la normativa federal. El año académico comenzará
  a principios de febrero de 2021. '

---
Se dará prioridad a aquellos niños que ya forman parte de la comunidad educativa de los jardines municipales.

**La vuelta a clases en los jardines municipales será escalonada y progresiva** tal como indica la normativa nacional, provincial y municipal. 

El protocolo que va a regir el funcionamiento de estos espacios educativos fue elaborado en forma conjunta con los equipos de directoras, psicopedagogas y trabajadoras sociales que forman parte de la comunidad educativa del nivel inicial de nuestra ciudad. 

Este proceso fue supervisado por la Dirección de Salud de Santa Fe y presentado al Comité Mixto en el mes de octubre.

**A principios del mes de noviembre se comenzó el proceso de reinscripción** para garantizar el retorno de los niños y niñas que ya son parte del sistema de educación inicial del municipio. 

Esta etapa de inscripción finaliza el 11 de diciembre y se reanudará el 1° de febrero para poder organizar los grupos y así garantizar las mejores condiciones sanitarias a todos los que forman parte de la comunidad educativa. 

**Y a principios de febrero comenzarán las actividades académicas**, como siempre ocurrió con el sistema de jardines municipales.

Las preincripciones al ciclo 2021 están sujetas a las normativas sanitarias y habilitaciones provinciales y de acuerdo con la curva epidemiológica para el mes de febrero entendiendo que las unidades de medida se vieron modificadas por lo tanto también cambió la capacidad edilicia.

El inicio del ciclo será escalonado y progresivo, acorde a los protocolos vigentes. La idea es comenzar con las salas de 3 años de manera continua pues los niños y niñas de esta edad pueden movilizarse con mayor autonomía y pueden establecerse pautas de circulación garantizando distanciamiento y comportamientos seguros.

 Las salas de los más pequeños tendrán alternancias en horarios y días.

## **Vínculo con las familias**

En este año no se ha perdido el vínculo con las familias de los jardines municipales. 

Desde que se dictó la cuarentena en el país se sostienen encuentros semanales con las familias para la entrega de alimentos como así también se han identificado casos de mayor necesidad a los que se ha podido garantizar el abastecimientos de pañales, alimentos secos y productos de limpieza.

## **Fin de un ciclo**

El 11 de diciembre culmina el ciclo lectivo según calendario académico. 

Desde el municipio se están organizando encuentros de despedida presenciales con los docentes de las salas de 3 años que finalizan su recorrido por los jardines municipales. 

En estos encuentros haremos entrega del certificado de fin de ciclo y un obsequio elaborado por las docentes.

## **Capacitaciones**

El personal de jardines participará de un encuentro para compartir las actualizaciones de los protocolos específicos de jardines maternales presentados por el gobierno provincial. 

Cabe destacar que el personal de 98 asistentes (contando con aquellas que no se vieron afectadas por las consecuencias de la pandemia) y directoras realizaron las capacitaciones generales realizadas por salud laboral.

También mencionar que se realizaron las gestiones para que los jardines cuenten con los elementos de seguridad e higiene y reforzar las actualizaciones con las sucesivas reformas de los protocolos en función del Covid 19. 

Por último, funcionarios del área vienen llevando adelante encuentros progresivos con las 117 docentes de cada jardín en grupos y en forma virtual trabajando sobre el proyecto de retorno.