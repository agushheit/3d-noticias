---
category: El Campo
date: 2021-01-24T10:15:38Z
thumbnail: https://assets.3dnoticias.com.ar/M.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Con información de Infobae
resumen: El maíz y los puertos rosarinos explicaron dos tercios de las exportaciones
  de granos y oleaginosas del 2020, el segundo volumen más alto de la historia
title: El maíz y los puertos rosarinos explicaron dos tercios de las exportaciones
  de granos y oleaginosas del 2020, el segundo volumen más alto de la historia
entradilla: La Fundación Mediterránea, estima que en 2021 la exportación de granos
  y sus principales derivados industriales aportará entre USD 2.600 y USD 6.500 millones
  más que el año pasado

---
Los despachos de granos desde puertos argentinos durante 2020, con 56,5 millones de toneladas y el maíz como principal producto, fueron “el segundo mayor volumen de la historia, 23% pr encima del tonelaje promedio exportado los últimos 5 años”, precisa un reciente informe de la Bolsa de Comercio de Rosario (BCR).

El volumen de embarques quedó apenas 6% abajo del récord de 2012, dice el informe, que atribuye esa pequeña retracción de la producción al clima seco y caluroso que se registró en los meses de febrero y marzo del 2020.

A su vez, “el 68% de los despachos se realizaron en las terminales portuarias ubicadas al sur y el norte de Rosario, y poco menos de la mitad de la producción nacional de granos exportados -44%- se embarcó en las terminales de siete de las diez cerealeras líderes de la agroindustria”, precisa el informativo semanal de la bolsa rosarino, firmado por la analista Emilse Terré, que destaca que pese a la pandemia de coronavirus, el sector agroexportador mantuvo su desempeño. A su vez, entre los granos, el maíz fue el producto estrella, al punto de representar cerca de 64 % del total. El informe también subraya el aumento de nada menos que el 43% en los despachos de sorgo, un cultivo estrechamente ligado al maíz, al punto que los productores de ambos cultivos están institucionalmente nucleados en Maizar, la “Asociación del maíz y el sorgo argentino”.

En contraste con el notable desempeño productivo y exportador del maíz y el sorgo, los envíos de soja al exterior cayeron 30%, los de cebada 17% y los de trigo 8 por ciento. De hecho, una de las recientes reacciones a la prohibición temporaria a las exportaciones de maíz que impuso el gobierno en su intento de contener su precio y abaratar los costos de producción de las carnes de pollo, vacuna y porcina, fue que con medidas así el país podría volver rápidamente al proceso de “sojización” que había empezado a desandar en los últimos años. A diferencia de los demás granos y aceites, la soja no es muy consumida en la Argentina.

![La evolución de las ventas semanales de soja y maíz, incluyendo el mercado interno](https://www.infobae.com/new-resizer/i_n7tTZSHx0WxhfdrTuIPQlwFNI=/420x236/filters:format(jpg):quality(85)//cloudfront-us-east-1.images.arcpublishing.com/infobae/IT2W5D5I5ZCR5CFB2LG666L2GY.jpg)

"En un año signado por el golpe de la pandemia de covid-19 sobre el consumo y las variables claves del mercado nacional e internacional, el desempeño del agro argentino probó ser una de las pocas ramas cuyo desempeño exportador continuó mostrando la pujanza que lo caracteriza”, dice el trabajo de la bolsa rosarina, que resalta que el desempeño productivo y exportador se logró pese a “las dificultades que impuso la bajante histórica del Río Paraná, que alcanzó su nivel más bajo en 50 años, y la incertidumbre macro general” y del clima caluroso y seco del fin del verano 2020.

Aunque el volumen total de granos fue el segundo más alto de la historia, en el caso del maíz, con 36 millones de toneladas embarcadas (como ya se dijo, 64% del despacho total) sí alcanzó un récord histórico, superando en 3% el récord del ciclo promedio y en 50% el promedio de los últimos cinco años.

**Granos de oro**

El volumen embarcado en 2020 “no sólo representa el más alto en la historia para el grano amarillo, un 3% por encima del año anterior, sino que supera en más del 50% el promedio despachado en el último lustro”, señala el informe, quien también recuerda que 68%, poco más de dos tercios, de las exportaciones granarias “partieron de las terminales radicadas en el Gran Rosario, en línea con los máximos históricos”.

Además, los puertos del sur de Santa Fe significaron en 2020:

\- El 79% del maíz de origen argentino (y el 81% del maíz paraguayo), por un total de más de 28,3 millones de toneladas.

\- El 67% del trigo, por un volumen total de 6,9 millones de toneladas.

\- El 37% de los embarques de poroto de soja por un total de 2,4 millones de toneladas.

\- El 60% del sorgo, con despachos cercanos a las 375.000 toneladas.

![Evolución reciente y proyección de la exportación de granos y sus principales derivados industriales](https://www.infobae.com/new-resizer/_jWJ94PpxdIMDeHL-EDkXUoqXqU=/420x187/filters:format(jpg):quality(85)//cloudfront-us-east-1.images.arcpublishing.com/infobae/KQOFGJOW7RFGFCRBN3H7KUHABY.jpg)

Otro estudio, del investigador Juan Manuel Garzón, del Ieral de la Fundación Mediterránea, ausculta a su vez los stocks remanentes y proyecta las ventas del ciclo 2020-21. Al respecto, dice que “por el momento, que el área sembrada con los dos principales cultivos de verano (maíz y soja) se mantendría con pocos cambios en la campaña 2020/21 (respecto de la previa)”. Eso sí, las labores de siembra van un poco más lentas, por la poca humedad de los suelos “por lo que resulta clave que las lluvias hagan su aporte en las próximas semanas para completar la superficie total prevista”.

En cuanto a los precios, tras las fuertes subas de septiembre a noviembre, la relativa estabilidad de diciembre y las bajas de los últimos días, el estudio señala que los mercados de futuros sugieren que las cotizaciones se mantendrán en valores altos en 2021, lo más altos desde 2014.

A partir de ciertas variaciones en esas condiciones, Garzón trazó distintos escenarios para la producción y exportación de granos y principales derivados industriales en 2021 y concluyó que “en un “escenario base de precios y cantidades (cotizaciones de mercados de futuros y volúmenes similares a los de este año), el flujo de divisas 2021 se estima en USD 31,1 mil millones, USD 4,6 mil millones más que en 2020”.

Los otros escenarios les dan aumentos de entre USD 2.600 millones y USD 6.500 millones respecto del año pasado, lo que significa que el campo seguirá siendo pródigo en el aporte de la mercadería más escasa de la economía argentina: dólares.