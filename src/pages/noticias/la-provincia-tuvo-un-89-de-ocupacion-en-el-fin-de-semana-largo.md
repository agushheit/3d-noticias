---
category: La Ciudad
date: 2022-06-20T12:25:41-03:00
thumbnail: https://assets.3dnoticias.com.ar/argentina-gad695bacd_1920.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: La provincia tuvo un 89% de ocupación en el fin de semana largo.
title: La provincia tuvo un 89% de ocupación en el fin de semana largo.
entradilla: El fin de semana extra largo quedo cerca de igualar las cifras de Semana
  Santa, con un total de 4.4 millones de turistas y excursionistas que recorrieron
  todo el país.

---

Según la CAME (Cámara Argentina de la Mediana Empresa) “unos 2.3 millones de turistas y 2.1 millones de excursionistas recorrieron el país durante este fin de semana largo”. El nivel de ocupación promedió el 90% y hubo un nutrido calendario de eventos y el buen clima ayudaron a los turistas. El gasto diario salto a $6.500, pero aun así la gente viajo y festejo el Día del Padre.

Hubo una nutrida agenda de eventos, destacándose el Foro Nacional de Turismo, con 1.500 participantes del país en Termas de Rio Hondo o el Desfile de Gauchos en Salta que convoco a 7.000 gauchos de todos los rincones de la argentina.

Según el informe publicado hoy por la CAME, la provincia de Santa Fe tuvo un buen movimiento con el 89% de ocupación. Se observaron muchos santafesinos que eligieron hacer sus mini vacaciones dentro de la misma provincia.

Rosario fue lo más destacado, con la presencia de autoridades nacionales, vecinos, turistas y excursionistas de todo el país. Otros destinos populosos fueron la ciudad de Santa Fe, y circuitos ligados a la naturaleza como Melincué y Reconquista y el corredor de la ruta provincial n°1. Entre las actividades distinguidas, tenemos la 31° edición de la Fiesta Provincial del Citrus en la localidad de Malabrigo, con la presencia de grandes artistas nacionales y locales como Alejandro Lerner, la Banda XXI, Los Alonsitos, La Contra y Los de Imaguaré. En la ciudad de Cayastá, la 46° Fiesta Provincial de la Yerra para conmemorar la primera marcación de animales del Virreinato del Rio de la Plata, en 1576, con la presentación de músicos reconocidos.