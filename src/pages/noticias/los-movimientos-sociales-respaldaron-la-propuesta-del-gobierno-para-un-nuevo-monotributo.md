---
category: Agenda Ciudadana
date: 2021-10-25T06:15:30-03:00
thumbnail: https://assets.3dnoticias.com.ar/MONOTRIBUTO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Los movimientos sociales respaldaron la propuesta del Gobierno para un nuevo
  monotributo
title: Los movimientos sociales respaldaron la propuesta del Gobierno para un nuevo
  monotributo
entradilla: El proyecto busca "formalizar" a los trabajadores y las trabajadoras de
  la economía social, a través de la creación de un nuevo monotributo destinado a
  ese sector.

---
Dirigentes de organizaciones sociales y políticas respaldaron la iniciativa del Gobierno nacional para "formalizar" a los trabajadores y las trabajadoras de la economía social, a través de la creación de un nuevo monotributo destinado a ese sector, que les permitirá tener las mismas prestaciones que un trabajador formal, en materia previsional y de salud, entre otros beneficios.  
  
"Unos 4.5 millones de habitantes de nuestro país están en la economía popular y trabajan mucho, es hora que ese trabajo se reconozca como tal", celebró Gildo Onorato, secretario gremial de la Unión de Trabajadores de la Economía Popular (UTEP) y dirigente del Movimiento Evita.  
  
En diálogo con Télam, Onorato destacó que el presidente Alberto Fernández haya escuchado sus necesidades y lo haya volcado en un proyecto de ley sobre el que hoy trabajan varios Ministerios y la AFIP, y dejó claro que esa iniciativa va en el sentido de lo que sostiene su agrupación de que Argentina requiere que el crecimiento económico sea "de abajo hacia arriba".  
  
Esta semana, el Gobierno nacional anunció que trabaja en un proyecto para llevar al "circuito formal" a los trabajadores de la economía social, a través de su registro como monotributistas productivos, lo que implicará facilidades en el pago de las cargas durante los primeros años y acceso a jubilación y cobertura de salud.  
  
Según Onorato, el "monotributo inclusivo absorbe medidas que los movimientos populares venían demandando para la reconstrucción de la relación fiscal y financiera del cooperativismo y el mutualismo" con las instituciones y también de la "compra estatal a las cooperativas", que termina mejorando además la actividad comercial de esas pequeñas unidades de negocio.  
  
Rafael Klejzer, director nacional de Políticas Integradoras del Ministerio de Desarrollo Social y referente del Movimiento Popular La Dignidad, destacó también la iniciativa al considerar que es un "reconocimiento estatal de la condición de laburantes de aquellas personas que, descartadas del mercado laboral, se inventan un trabajo para resolver la materialidad de su vida".  
  
Según dijo a Télam, la iniciativa " viene a restaurar derechos negados por la economía de mercados, que no puede ni quiere resolver el problema del trabajo".  
  
En tanto, para Luis D´Elía, secretario general de la Federación de Tierra, Vivienda y Hábitat (FTV) y presidente del partido MILES, el "monotributo social" implica "subsidiariedad al trabajo comunitario y cooperativo" y marca un "camino al salario básico y universal".  
  
Eduardo Montes, presidente de la Federación de Trabajadores de la Economía Social (FETRAES), también se expresó y dijo que "los trabajadores de la economía social y popular empiezan a dejar la informalidad con este proyecto" y agregó: "Creemos que esta política apunta a reconocernos como lo que somos, trabajadores".  
  
"Es hora de que empecemos a entender que la economía social; las cooperativas de trabajo y fábricas recuperadas, los emprendedores y emprendedoras, los compañeros de la agricultura familiar, los que iniciaron una olla popular en pleno macrismo y hoy trabajan en transformar esa organización en emprendimientos productivos para sus vecinos; todos estos sectores representan una alternativa real para reactivar la economía de nuestro país", subrayó.  
  
El Gobierno nacional anunció que trabaja en un proyecto para llevar al "circuito formal" a los trabajadores de la economía social, a través de su registro como monotributistas productivos

  
Desde la Corriente Clasista y Combativa (CCC), el diputado Juan Carlos Alderete (FTV-CCC) dijo a Télam que la medida también "ayuda para que pymes y micropymes puedan incorporar a trabajadores de los planes sociales a la formalidad", al tiempo que ratificó que "siempre es positivo transformar el trabajo informal o precarizado en empleo registrado".  
  
Nahuel Beibe, secretario general del Movimiento Martín Fierro, también manifestó su apoyo a la iniciativa impulsada por el Gobierno nacional "para fortalecer al sector", pero dijo que necesitan "más impacto para multiplicar el trabajo, incorporando a esos trabajadores masivamente en la obra pública, la construcción de viviendas populares y la producción de alimentos en tierras publicas".  
  
Ángel Lito Borello, coordinador nacional del Movimiento Popular Los Pibes y secretario de DDHH de la UTEP, valoró este instrumento que "mejora los derechos de lxs trabajadores que son empujados a la informalidad y al descarte" y dijo que siguen "reclamando ser parte del diseño de las políticas públicas que fortalezcan la economía popular y a sus organizaciones, para una mayor integración popular en el proyecto de país" al que aspiran.  
  
Desde Barrios de Pie-Libres del Sur, la dirigente Silvia Saravia, aportó su mirada y dijo que "regularizar al sector es importante",en tanto que Gastón Harispe, diputado del Parlasur (FTV-PJ) y responsable del Movimiento Octubres, consideró que el proyecto del Gobierno "es imprescindible para avanzar en formalizar a trabajadoras y trabajadores para poder facturar y vender productos, tener derecho a jubilarse, créditos, beneficios sociales como jubilaciones y obra social".  
  
"Es avanzar para volver a la experiencia peronista de trabajo digno que nuestro pueblo necesita. En un mundo donde la exclusión avanza y los trabajadores son descartados por mayor productividad y ganancias con poca mano de obra, el trabajo es el eje de la disputa entre países", completó.  
  
Finalmente, desde la Unión de Trabajadores de la Tierra (UTT), Agustín Suárez dijo a Télam que "la informalidad en el sector rural es muy amplia e histórica" y que por esa razón desde su agrupación junto a la Mesa Agroalimentaria Argentina vienen "reclamando hace tiempo que vuelva el Monotributo Social Agropecuario a costo cero".  
  
"Monotributo que antes del Gobierno de Mauricio Macri estaba y servía, no es ni va a ser algo que solucione el problema de la informalidad en los trabajadores pero es un primer paso para dar acceso a la salud, a obra social, para facturar, aportar a la jubilación a trabajadores informales", advirtió.