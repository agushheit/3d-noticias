---
category: Cultura
date: 2021-07-16T06:00:40-03:00
thumbnail: https://assets.3dnoticias.com.ar/VACACIONESINVIERNO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Cultura para disfrutar las vacaciones de invierno en Santa Fe capital
title: Cultura para disfrutar las vacaciones de invierno en Santa Fe capital
entradilla: Durante las dos semanas del receso invernal, la Municipalidad ofrecerá
  una extensa agenda de espectáculos para toda la familia, con entrada libre y gratuita.
  Los encuentros serán con aforo limitado por la pandemia.

---
Los espacios culturales gestionados por el municipio reabren esta semana con numerosas propuestas para disfrutar en vacaciones de invierno. La medida se adoptó a partir de la habilitación de cines, teatros y salas de espectáculos, dispuesta por el gobierno provincial desde el 9 de julio.

El Teatro Municipal “1° de Mayo”, el Mercado Progreso y el Anfiteatro “Juan de Garay” del Parque del Sur, son algunos de los escenarios donde se desarrollará la programación, a los que se suma el ciclo Veredas para Jugar en Loyola Sur, entre otras propuestas del gobierno local para compartir junto a los vecinos y las vecinas en toda la ciudad.

Los encuentros serán con aforo limitado por protocolo. Habrá puestos de sanitización en el ingreso a los espacios, se exigirá el uso permanente de barbijos o tapabocas y la distancia física para cuidar al público, a los artistas, el personal técnico y de sala.

**En el Mercado, de viernes a domingo**

Las tardes invernales se podrán disfrutar al aire libre, en el patio del Mercado Progreso (Balcarce 1635), siempre de 15 a 18 horas. Del 16 al 18 de julio habrá espectáculos de teatro, títeres y circo.

El viernes se presentará “El show de Lari y Marian”. En esta puesta, que interpretan Larisa Sánchez y Marian Lediv, los personajes comparten un feliz día de playa hasta que empiezan a competir por ver quién baila, canta y actúa mejor. Después llegará la propuesta del Circo Litoral, con malabares, magia, acrobacias y mucho humor. Un varieté para toda la familia, con Sheryl Castelvi, Augusto Fassi, Maia Korón, Silvana Thompson, Esteban “Kito” Lejbowicz, Francisco Saz y Esteban Sosa.

El encuentro del sábado comenzará con “Los demonios”, que llegarán de la mano del Elenco de Títeres del Teatro Municipal para desplegar sobre el retablo, “las historias de unos diablos muy especiales, a quienes las maldades que intentan hacer siempre les salen mal”. El grupo de teatro independiente Amalgama presentará a continuación su musical “Drácula una mordida de alegría”. En esta puesta, donde la música y los enredos cuentan la historia de un Drácula distinto a todos los demás, participan Santiago Vega, Eduardo Córdoba, Luis Rinaldi, Paz Wehren/Melina Chia y Julieta Colombara, dirigidos por Mariano Franco.

En la tarde del domingo, el grupo Magia Payasa -Cebolla, Carlota, Brasstolito y Sultana- recibirá al público con música y humor, para dar paso al despliegue de títeres, muñecos, actores y escenografía que realiza La Gorda Azul en “Transforma Bestias”. Con las interpretaciones de Victoria Menéndez, Elías Alberto y Facundo Girolimeto, el grupo propone un divertido viaje al castillo del conde más famoso de las películas de terror. La dirección es de Ulises Bechis.

**Veredas para jugar**

En Loyola Sur, vuelven las Veredas para Jugar, a partir de la intervención que realizaron la Municipalidad y la Red de Instituciones Barriales para mejorar con propuestas lúdicas el espacio que conecta al Jardín Municipal con la Estación de ese barrio. En la esquina de Pedroni y Furlong, habrá distintas actividades para toda la familia: hoy -jueves 15-, de 10 a 12 horas; los viernes 16 y 23 de julio, de 14 a 16.30.

![](https://assets.3dnoticias.com.ar/VACAS2.jpg)