---
category: Estado Real
date: 2021-10-24T06:00:38-03:00
thumbnail: https://assets.3dnoticias.com.ar/arijon.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Acueducto Desvío Arijón- Rafaela: Perotti visitará la feria de ciencias
  escolar y el bosque educativo de Rafaela'
title: 'Acueducto Desvío Arijón- Rafaela: Perotti visitará la feria de ciencias escolar
  y el bosque educativo de Rafaela'
entradilla: Será durante la jornada del próximo lunes 25 de octubre. La megaobra cuenta
  con financiamiento internacional y en su primera etapa se invierten más de $14 mil
  millones.

---
El gobernador Omar Perotti participará este lunes 25 de octubre de una jornada pedagógica sobre la importancia del agua, que se desarrollará en el Complejo Educativo Barrio San José de la ciudad de Rafaela. Por la tarde, observará el ingreso de cañerías del Acueducto y recorrerá la planta potabilizadora de aguas de dicha ciudad.

Cabe señalar que el acueducto Desvío Arijón – Rafaela entró en su etapa final y su relevancia es motivo de tareas escolares en diferentes instituciones educativas de Rafaela y es consecuencia del abordaje curricular trabajado durante el año, referido al cuidado y a la importancia del agua.

Al respecto, el secretario de Empresas y Servicios Públicos, Carlos Maina, explicó la complejidad y la magnitud de la obra: “Hablamos de una mega obra, con financiamiento internacional, que en esta primera etapa cuenta con una nueva planta potabilizadora, cinco estaciones de bombeo, unos 130 kilómetros de cañería, más de 14 mil millones de pesos invertidos y que beneficiará a las localidades de Matilde, San Carlos Norte, San Carlos Centro, San Carlos Sud, Santa Clara de Buena Vista, San Mariano, Sa Pereira, Angélica, Susana, Rafaela”.

Y agregó: “El agua que se toma desde el río Coronda en Desvió Arijón, se dirige hasta San Carlos Centro (tramo II) y desde allí va hacia San Mariano (tramo III), continúa hasta Angélica (tramo IV) y finalmente ingresa a Rafaela (tramo V)”.

A su vez, Maina recordó: “Nos hicimos cargo de la obra en diciembre de 2019, con muy poco movimiento de trabajo porque no se les pagaba a las empresas. Pero cuando el gobernador Omar Perotti comenzó su gestión, tomó la decisión de avanzar y que la obra se ponga en marcha, la supervisó personalmente y en la actualidad, el acueducto llegó a Rafaela. La ejecución llevará por lo menos unos meses más y poner en régimen esa infraestructura, otros 6 u 8 meses aproximadamente, de acuerdo a la experiencia de ASSA en otros acueductos. Lo importante es que el acueducto hoy ya está en Rafaela”.

**JORNADA PEDAGÓGICA**

Este lunes 25 desde las 9 horas, alumnos de los niveles inicial, primario y secundario de unas 17 escuelas y del anexo del CEF 19, participarán de una feria de ciencias denominada "Propuesta pedagógica institucional y comunitaria NOS APROPIAMOS DEL ACUEDUCTO”. La misma contará con producciones pedagógicas, artísticas y científicas realizadas por las niñas, niños y adolescentes, quienes aplicaron los saberes trabajados en el aula, a través del uso de los cuadernos pedagógicos utilizados desde principio de año, reflejando la importancia de la emblemática obra y el cuidado del agua.

Esta actividad escolar es consecuencia del abordaje curricular referido al cuidado y la importancia del agua, y que plasma en trabajos de diferentes formatos, la llegada del Acueducto Desvío Arijón – Rafaela, que tiene que ver con el ingreso del servicio de agua potable a sus hogares.

La Feria de Ciencias se desarrollará en el Complejo Educativo Barrio San José, sito en Muniagurria 58, de la ciudad de Rafaela.

**ACUEDUCTO PARA RAFAELA**

Mientras que desde las 18 horas, el gobernador visitará el Bosque Educativo “Norberto Besaccia”, donde realizará anuncios relacionados al Acueducto Desvío Arijón – Rafaela.

La alimentación a la ciudad de Rafaela desde la planta de Desvío Arijón implicará un importante cambio en las características del agua potable de la ciudad, reduciendo niveles de salinidad y haciendo posible la incorporación al servicio en el futuro de otros sectores del ejido urbano.

En la ciudad de Rafaela el agua proveniente del acueducto se almacenará en una nueva reserva de 5 millones de litros que se suma a la existente de 10 millones de litros.

Actualmente Rafaela se abastece de agua potable a través de un sistema combinado: un acueducto de 60 kilómetros proveniente de Esperanza; y agua de perforaciones locales que es potabilizada mediante el proceso de desalinización denominado ósmosis inversa.

La llegada del Acueducto Desvío Arijón permite sacar de servicio la principal planta de ósmosis inversa de la ciudad, manteniendo con volúmenes reducidos el aporte del Acueducto Esperanza-Rafaela.

**CÓMO FUNCIONA EL ACUEDUCTO**

>> Toma de agua

El Acueducto Desvío Arijón capta el agua necesaria para potabilizar en un muelle de toma sobre el río Coronda que impulsa el agua cruda hasta la planta a través de un acueducto de casi 2,5 kilómetros de extensión.

>> Planta potabilizadora

La planta potabilizadora está ubicada en inmediaciones de Desvío Arijón, entre la autopista Santa Fe-Rosario y la ruta nacional N° 11.

Cuenta con dos módulos de tratamiento con una capacidad media diaria de producción de 100 millones de litros, capacidad duplicada gracias a la obra ejecutada por el Gobierno provincial.

El volumen total de producción equivale a la carga diaria de 15.000 camiones cisterna.

Cada módulo de tratamiento consta de:

>> Precloración: proceso previo para asegurar la eliminación de elementos orgánicos

>> Coagulación: Agregados de productos precipitantes para la clarificación, que eliminará la particularidad turbiedad del agua de río.

>> Floculación: Formación de flocs para el paso siguiente.

>> Sedimentación: de “alta carga”, que precipita la turbiedad, tornando el agua transparente.

>> Filtros rápidos: Aseguran la eliminación de cualquier partícula que hubiera pasado la sedimentación.

>> Desinfección con cloro: Elimina cualquier elemento orgánico perjudicial para la salud.

La planta posee una cisterna enterrada de reserva dividida en dos partes –para poder hacer limpieza o mantenimiento sobre la misma-, que suma 7 millones de litros de capacidad de almacenamiento.

También existe una estación de tratamiento de lodos para los barros que son extraídos durante el proceso, destinada a la recuperación de los mismos y cuidado del ambiente. Esta planta dispone de una laguna de estabilización de los líquidos remanentes del proceso.

Además de la estación de bombeo, se dispone de instalaciones para talleres, almacenamiento y dosificación de insumos químicos, sala de cloración -equipada con dispositivo de neutralización de eventuales fugas- y laboratorio de control de calidad.