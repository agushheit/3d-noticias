---
category: El Campo
date: 2021-01-27T09:56:20Z
thumbnail: https://assets.3dnoticias.com.ar/miel.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno Santa Fe
resumen: Santa Fe lidera la exportación de miel junto a Buenos Aires y Entre Ríos
title: Santa Fe lidera la exportación de miel junto a Buenos Aires y Entre Ríos
entradilla: Son más de 1.400 los apicultores registrados a lo largo de la provincia.
  Desde el Ministerio de Producción, Ciencia y Tecnología destacaron al 2020 como
  un año positivo para el sector.

---
Con un crecimiento en las exportaciones del 45% entre el año 2019 y el 2020, la actividad apícola santafesina mantiene un marcado liderazgo, que la posiciona en tercer lugar a nivel nacional, luego de Buenos Aires y Entre Ríos.

En números, este crecimiento se reflejó en 10.122 toneladas exportadas de enero a noviembre de 2020, significando un importe de más de 24 millones de dólares (las cifras del año 2019 arrojaban 6.977 toneladas por un monto de 16 millones de dólares).

En ese marco, desde las secretarías de Agroalimentos y de Comercio Exterior del Ministerio de Producción, Ciencia y Tecnología santafesino, se articulan acciones para consolidar y ampliar la oferta de productos de la colmena en el mercado internacional, a través de la promoción y asesoramiento en modelos asociativos y en fuentes de financiamiento.

Es importante resaltar que la provincia cuenta con 16 cooperativas de origen apícola y nueve asociaciones de productores, orientadas al asesoramiento, capacitación, promoción y financiamiento de la apicultura santafesina.

Al respecto, el director provincial de Producción Lechera y Apícola, Abel Zenklusen, mencionó que “el incremento de las exportaciones ponen de manifiesto el potencial de los productores, las condiciones fitogeográficas de la provincia y un mercado internacional demandante de la calidad de nuestras mieles tanto convencional como orgánica”.

Haciendo un balance del año 2020, el funcionario resaltó que “ha sido altamente positivo para el sector, ya que se logró consolidar un marco institucional a través de la Mesa de Diálogo Apícola en la cual se generan los consensos necesarios para avanzar en una estrategia de crecimiento de la cadena”.

**MOTOR PRODUCTIVO**

La provincia de Santa Fe cuenta con 1.419 apicultores registrados según el Registro Nacional de Productores Apícolas (Renapa), con unas 393095 colmenas totales, de las cuales 6.700 se encuentran certificadas en producción orgánica.

Cuenta con 105 salas de Extracción de Miel habilitadas y tres establecimientos exportadores, desde donde se comercializa más de la mitad de la producción regional. Con tres establecimientos fraccionadores de miel habilitados para la exportación y 56 habilitados por el RNE (Registro Nacional de Establecimiento), Santa Fe está en el segundo lugar a nivel país en la cantidad de establecimientos fraccionadores.

Además, en la provincia se distribuyen siete establecimientos estampadores de cera, cuatro cabañas apícolas certificadas y 20 multiplicadores de reinas.

Este motor productivo de la economía regional tomó impulso junto con el apoyo de políticas concretas que llevó adelante el gobierno provincial. En tal sentido, Zenklusen enumeró “la descentralización de registros e información (Rupp y Renapa); convenios para sala de Extracción entre Senasa y el ministerio; la formación de la Delegación Santa Fe de la Cámara Argentina de Fraccionadores de Miel; el acompañamiento en proyectos de valor agregado, fondos rotatorios, e innovación tecnológica en ámbitos provinciales y nacionales; y el fortalecimiento del movimiento asociativo cooperativista y acompañamiento con herramientas de financiamiento”.

Finalmente, el funcionario manifestó que en 2021 “buscamos consolidar los logros del año pasado” y adelantó que “las acciones van a estar orientadas a la mejora de productividad, promoción del consumo de miel vinculados al mercado interno y externo, disponibilidad de herramientas financieras a medida de la cadena, fortalecimiento de la institucionalidad y puesta en valor de la abeja en la sociedad santafesina”.