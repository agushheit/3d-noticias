---
category: La Ciudad
date: 2021-06-19T06:15:26-03:00
thumbnail: https://assets.3dnoticias.com.ar/SERVICIOS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Servicios municipales con motivo del fin de semana largo
title: Servicios municipales con motivo del fin de semana largo
entradilla: La Municipalidad informa cómo será el funcionamiento de los servicios
  de recolección de basura, transporte público y estacionamiento medido, con motivo
  del feriado nacional del 17 de junio, que se trasladó al lunes 21.

---
La Municipalidad comunica los horarios y servicios municipales que se prestarán con motivo del feriado nacional del 17 de junio, en que se conmemora el Día del Paso a la Inmortalidad del General Martín Miguel de Güemes que se trasladó al lunes 21 de junio.

En ese sentido, se informa que la recolección de basura y barrido de calles será prestada de manera habitual por las empresas Cliba y Urbafe.

**Transporte y estacionamiento**

Por otra parte, el transporte urbano de colectivos tendrá el lunes frecuencias equivalentes a los días domingo. En cuanto al sistema de estacionamiento medido (SEOM), no estará operativo.

**Cementerio**

El lunes las puertas permanecerán abiertas de 8 a 12 horas, con motivo del día feriado, para las visitas del público. Vale recordar que, con motivo del Día del Padre, que será el próximo domingo, el cementerio municipal estará abierto de 8 a 16; mientras que el sábado el horario será de 9 a 16 horas.

**Mercado Norte**

El Mercado Norte, ubicado en la intersección de Santiago del Estero y Urquiza, abrirá el sábado en su horario habitual de 8 a 19, mientras que el domingo y lunes feriado, lo hará en el horario de 9 a 13 horas.