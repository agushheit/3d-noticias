---
category: La Ciudad
date: 2020-12-12T12:05:55Z
thumbnail: https://assets.3dnoticias.com.ar/distrito.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: El Distrito Suroeste abrió sus puertas en el edificio de la rotonda Boca
  del Tigre
title: El Distrito Suroeste abrió sus puertas en el edificio de la rotonda Boca del
  Tigre
entradilla: 'La Municipalidad acondicionó el inmueble ubicado en Dr. Zavalla y J.
  J. Paso, frente a la cancha de Colón. También se hicieron intervenciones en el espacio
  urbano. '

---
Luego de varios meses de trabajo, la Municipalidad habilitó el edificio ubicado en el acceso sur a la ciudad, sobre la rotonda conocida como Boca del Tigre, frente al Club Atlético Colón.

**El inmueble fue puesto en valor y se transformó en la nueva sede del Distrito Suroeste, que anteriormente funcionaba en la Estación Mitre**. Se trata de un espacio que se encontraba vacío y, gracias a una serie de intervenciones a cargo de la Secretaría de Obras y Espacio Público, pudo volver a ocuparse.

**Esta sede descentralizada de la Municipalidad, atiende de 7.30 a 17; y allí se pueden hacer trámites** relacionados con inmuebles, automotores, actividades económicas y profesionales, servicios, pedir asesoramiento para realizar gestiones como boleto educativo, sacar el carné para ex combatientes de Malvinas, permiso de libre estacionamiento, credencial frentista, y otros trámites municipales.

En este sentido, el secretario de Gobierno, Miguel González, entregó la llave a los empleados que cumplirán allí las distintas tareas. “La readecuación de este espacio tiene que ver con la mirada que el intendente Emilio Jatón tiene sobre los barrios y la posibilidad de tener edificios de calidad en cada uno de ellos. 

<br/>

![](https://assets.3dnoticias.com.ar/distrito1.jpg)

<br/>

Queda a las claras el interés que tiene la Municipalidad por relacionarse con los vecinos en el territorio. Esto es lo que Emilio rescata en estos tiempos y por eso la importancia de poner en valor este edificio tan importante y que en la ciudad cumplió distintos roles”, manifestó.

Luego, antes de terminar, expresó: “La restauración y recuperación del patrimonio histórico es claramente una marca del municipio y lo va a seguir siendo. Por la importancia del Puente Carretero y la relación con Santo Tomé, era necesario que también tuviéramos esta cara renovada para nuestros vecinos y además para las personas que llegan de otros lados”.

<br/>

# **Distintas obras**

Por su parte, la secretaria de Obras y Espacios Públicos, Griselda Bertoni, dio más detalles sobre la recuperación del inmueble y contó: 

“Se trata de un edificio patrimonial y emblemático de la ciudad de Santa Fe. Todos los pueden reconocer porque está en el ingreso a la ciudad. Además de poner en valor, se hicieron las reformas necesarias para que el centro de distrito funcione. Es un edificio de los años 40, tiene una historia muy grande, se hizo como obra complementaria al Puente Carretero y había que recuperarlo”.

Este año, y con recursos y trabajadores municipales, en el edificio conocido como Boca del Tigre se concretaron refacciones, como la limpieza general de la terraza, el sellado de grietas y fisuras, la impermeabilización de la losa y la restitución de revoques. También se construyeron nuevos baños, se reemplazó toda la instalación eléctrica, se colocó nueva iluminación y, por último, el equipamiento necesario para el funcionamiento del distrito.

“Del lado norte se construyó una nueva rampa para personas con discapacidad y la concretaron empleados de los talleres municipales; los bancos que se colocaron se hicieron con la madera reciclada de los árboles caídos por tormentas; y los trabajos de naturación lo realizaron trabajadores del área verde. Esta es una primera etapa de una intervención urbana que va a comprender toda la rotonda”, detalló más adelante Bertoni.

<br/>

![](https://assets.3dnoticias.com.ar/distrito2.jpg)

<br/>

# **Priorizar la accesibilidad**

También se realizaron obras relacionadas con la peatonalización de buena parte del área conocida como Boca del Tigre, para facilitar el acceso al inmueble central. El objetivo era que las personas lleguen a la sede del distrito de manera fácil y segura, teniendo en cuenta la alta afluencia de vehículos que transitan por la zona, principalmente desde y hacia Santo Tomé.

“Una parte de la rotonda se unió a la del edificio y hay un giro menos. La próxima etapa es modificar el tránsito vehicular y que sea toda un área de transporte público, peatonal y bicisenda para que los vecinos puedan acceder de manera cómoda y rápida”, manifestó luego la funcionaria municipal.

La idea central fue hacer más amigable el entorno para el peatón y para los medios reducidos de movilidad, mientras se generaba un espacio de permanencia disfrutable, con bancos, plantas y cestos de residuos, alrededor del edificio donde se ubicará el Distrito Suroeste. 

En este sentido y antes de terminar, dijo: “Ésta es una característica propia de todas las intervenciones que se hacen desde el municipio, relacionado con la accesibilidad universal y la prioridad para el peatón y el transporte público”.

![](https://assets.3dnoticias.com.ar/distrito3.jpg)