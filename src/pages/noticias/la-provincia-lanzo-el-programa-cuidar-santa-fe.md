---
category: Estado Real
date: 2021-10-28T06:00:11-03:00
thumbnail: https://assets.3dnoticias.com.ar/ARENA.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La provincia lanzó el programa "Cuidar Santa Fe"
title: La provincia lanzó el programa "Cuidar Santa Fe"
entradilla: "“La mirada está puesta en redistribuir y trabajar en la corresponsabilidad
  en las tareas de cuidado”, explicó Arena. "

---
El gobierno provincial, a través del Ministerio de Igualdad, Género y Diversidad, llevó adelante este miércoles el lanzamiento del programa Cuidar Santa Fe, en el marco de las políticas públicas de promoción de la corresponsabilidad de las tareas de cuidado a efectos de garantizar una organización social del cuidado más justa y con igualdad de género.

La actividad -con modalidad híbrida- tuvo lugar en el Salón Rodolfo Walsh de la sede del gobierno provincial en la ciudad de Rosario, donde también se presentó el Registro Público de Espacios, Instituciones y Servicios donde se brindan cuidados comunitarios, privados y estatales en Santa Fe; y se realizó el Seminario “Sistemas y Políticas de Cuidado. Perspectivas y Prospectivas” a cargo de Silvia Lilian Ferro.

“Es un programa que venimos trabajando desde hace tiempo entre todos los ministerios, para tener un registro de todos los espacios de cuidado, para tener un buen diagnóstico, porque entendemos que los cuidados del hogar recaen mayoritariamente en las mujeres, y tener una política sobre esto ayuda a cerrar estas brechas de desigualdad”, sostuvo la ministra de Igualdad, Género y Diversidad, Celia Arena.

“La mirada está puesta en redistribuir y trabajar en la corresponsabilidad en las tareas de cuidado, porque esto impacta de manera positiva en el desarrollo de las mujeres”, dijo Arena, y convocó a la organizaciones y entidades que tienen espacios de cuidados a sumarse al registro.

La actividad estuvo orientada especialmente a organizaciones sociales que proveen cuidados, cooperativas de cuidados, espacios comunitarios de cuidados, organizaciones sindicales de trabajadoras domésticas, espacios de cuidados y lactarios, sindicatos y mutuales. Participaron también la secretaria de Mujeres, Género y Diversidad, Florencia Marinaro, y la coordinadora del programa Cuidar Santa Fe, Majo Gerez.

Dentro de las etapas de implementación del Programa, un primer paso es la elaboración de un diagnóstico común sobre las formas en las que se organiza socialmente el cuidado en nuestra provincia, a través de la Mesa Interministerial de Políticas de Cuidado; el Registro Público de Espacios, Instituciones y Servicios donde se brindan Cuidados Comunitarios, Privados y Estatales en Santa Fe; y el Mapeo de servicios, programas y proyectos provinciales y locales existentes enfocados en los cuidados.

**REGISTRO DE ESPACIOS DE CUIDADO**

Las y los sujetos beneficiarios de esta política pública serán, tanto quienes dependen del cuidado de otras personas para su desarrollo vital -niños y niñas, personas con discapacidad y personas mayores dependientes-; como también, aquellas personas que prestan los servicios de cuidados, dentro del territorio de la provincia de Santa Fe.

La finalidad es crear un Sistema Integral de Cuidados de la provincia de Santa Fe, que garantice el derecho de las personas a recibir cuidado, cuidarse y a cuidar en condiciones de calidad e igualdad en todo el territorio provincial, promoviendo el desarrollo de la autonomía, la atención y asistencia a las personas en situación de dependencia y el desarrollo infantil y juvenil, en el marco de un modelo de corresponsabilidad entre familias, estado, mercado y comunidad, así como entre feminidades y masculinidades.

Para inscribirse al Registro Público de Espacios, Instituciones y Servicios donde se brindan Cuidados Comunitarios, Privados y Estatales en Santa Fe, es necesario completar el formulario en [www.santafe.gob.ar/cuidarSF](http://www.santafe.gob.ar/cuidarSF)

Para más información y consultas, escribir a: cuidar@santafe.gov.ar