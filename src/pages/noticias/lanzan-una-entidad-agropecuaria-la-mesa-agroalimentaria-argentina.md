---
category: Agenda Ciudadana
date: 2021-06-09T08:39:21-03:00
thumbnail: https://assets.3dnoticias.com.ar/movilizacion-pequenos-productores-el-otro-campo-se-une-una-mesa-agroalimentaria.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Agroclave
resumen: 'Lanzan una entidad agropecuaria: la Mesa Agroalimentaria Argentina'
title: 'Lanzan una entidad agropecuaria: la Mesa Agroalimentaria Argentina'
entradilla: El nucleamiento integrado por la UTT, el Movimiento Nacional Campesino
  y Fecofe se presentará este miércoles

---
Un grupo de organizaciones, integrantes del denominado “otro campo” y representantes de pequeños y medianos productores de alimentos, lanzarán el próximo miércoles la Mesa Agroalimentaria Argentina, un espacio que buscará articular políticas que den respuestas a la “crisis alimentaria en nuestro país”.

La iniciativa es impulsada por la Unión de Trabajadores y Trabajadoras de la Tierra (UTT), el Movimiento Nacional Campesino Indígena Somos Tierra (MNCI-ST) y la Federación de Cooperativas Federadas (Fecofe), y la mesa se presentará desde una finca recuperada de la provincia de Mendoza, en un acto que se transmitirá en vivo vía streaming. Será este miércoles a las 14.

El nuevo agrupamiento se propone, de acuerdo a la comunicación difundida por sus organizadores, “garantizar el abastecimiento de alimentos de calidad y a precios justos, con el horizonte de construir un modelo agrario que garantice la soberanía alimentaria”.

Los promotores de la iniciativa destacaron que esas organizaciones “se unen para constituir la mesa del campo que faltaba”. Nahuel Levaggi, coordinador nacional de la UTT y presidente del Mercado Central de Buenos Aires, consideró que “actualmente, se encuentra en tensión la visión sobre qué modelo agroalimentario debemos construir”.

Por su parte, el presidente de Fecofe, Juan Manuel Rossi, sostuvo que los integrantes de la nueva mesa creen “en un campo poblado, con acceso a la tierra, con arraigo, con producción y comercialización local, fortaleciendo un esquema tecnológico sustentable y agroecológico, con cooperativas y pymes fortalecidas, y generando trabajo digno que active las economías regionales”.

“Entendemos el alimento como un derecho del pueblo, y al Estado con un deber profundo en promover un modelo productivo que garantice ese derecho”, destacó Rossi.

Diego Montón, referente nacional del MNCI-ST, se preguntó: “Si el precio internacional de la soja está en valores históricos, ¿cómo es que al pueblo le cuesta cada vez más poner comida en el plato?”.

El lanzamiento de la Mesa Agroalimentaria Argentina se transmitirá a través de las redes sociales @trabajadoresdelatierra, @MNCI.SomosTierra y @fedcoopfedltda, con los hashtag #ElCampoQueAlimenta #LaMesaQueFaltaba. El lema del lanzamiento será: “El campo que alimenta y construye Soberanía Alimentaria”.