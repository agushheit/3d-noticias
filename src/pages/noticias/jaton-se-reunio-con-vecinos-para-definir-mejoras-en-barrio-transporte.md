---
category: La Ciudad
date: 2020-11-27T10:44:37Z
thumbnail: https://assets.3dnoticias.com.ar/./jaton-transporte.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Barrio Transporte
title: Jatón se reunió con vecinos para definir mejoras en barrio Transporte
entradilla: El encuentro se llevó a cabo en Club Pucará con la participación de integrantes
  del gabinete municipal. Reforzar la iluminación, y realizar mejoras viales e hídricas
  fueron algunos de los ejes abordados.

---
El intendente Emilio Jatón se reunió este miércoles con vecinas y vecinos de Transporte para definir de manera colectiva mejoras para el barrio. “En este encuentro, que surge a pedido de ustedes, buscamos que barrio Transporte entre en un plan de trabajo programado, con tiempos y plazos con cumplimientos”, definió el intendente.

En la reunión, que se desarrolló en las instalaciones exteriores de Club Pucará, Jatón compartió que desde el municipio se trabaja para recuperar y poner en funcionamiento la Vecinal Facundo Quiroga como así también un plan de obras para revertir la realidad "de muchos barrios abandonados hace tiempo”, expresó.

Reforzar la iluminación, reparar y realizar obras de desagües y cordón cuneta, y profundizar la higiene del barrio fueron algunos de los puntos que sobresalieron en el encuentro. “Lo que nosotros venimos a proponer es todo lo que tenemos a nuestro alcance hoy, con nuestro equipo, para empezar a solucionar los problemas que son urgentes”, afirmó Jatón.

También estuvieron la secretaria de Obras y Espacio Público, Griselda Bertoni; el secretario de Ambiente, Edgardo Seguro; y el director de Gestión Operativa de la Secretaría de Asuntos Hídricos y Gestión de Riesgo, Alberto Mitri.

#### **Obras planificadas**

Tras el encuentro, Bertoni planteó que vinimos a “escuchar para empezar a resolver problemas de larga data. Es un barrio céntrico que se encuentra entre avenidas, que tiene problemas múltiples. Vinimos a escuchar, y a ponernos a disposición a demandas que son muy importantes como el tema higiene, desagües hídricos e iluminación, con muchos sectores completos apagados”. En ese sentido, la funcionaria detalló que la tarea que sigue es “hacer un estudio profundo para dar respuesta y solución integral a esta pequeña porción de ciudad que estuvo olvidada los últimos tiempos”.

Transporte era uno de los barrios incluidos con una serie de obras que el Programa Municipal de Inversiones (Promudi) había licitado en la gestión provincial anterior y que no se van a llevar adelante. Por este motivo, la funcionaria indicó que “hay una demanda alta, de algo que es muy lógico, que es tener cordón cuneta en vez de zanja, porque el sistema de desagües a cielo abierto es de muy difícil mantenimiento”.

Sobre esta obra, explicó que “nos comprometimos a hacer un proyecto, a hacer números. El intendente se comprometió a buscar financiamiento externo porque son obras carísimas, pero no le vamos a esquivar a lo que hay que hacer de inmediato, y vamos a hacer una programación para darle resolución definitiva”.

####   
**Espacio enriquecedor**

Cristian Papili, vecino del barrio que participó del encuentro, compartió que la reunión surgió a partir de iniciativa de los vecinos “y el municipio recoge el guante y se acerca. Como le dijimos al intendente, nos parece fundamental e importante porque habla de una proximidad de las políticas públicas, de esta cuestión del acercamiento al vecino, poder recoger las inquietudes y poder, desde allí, elaborar políticas públicas”.

Entre las propuestas presentadas se destacan iluminación, ejecución de cordón cuneta y mejorado de calles que, para Cristián, son “cuestiones fundamentales para la cotidianeidad de todos los vecinos y las vecinas”. Sobre el encuentro, destacó también que “fue un espacio enriquecedor porque se toma la palabra y se llevan las propuestas, además porque hubo personal y funcionarios municipales de las distintas áreas de gestión”.

![](https://assets.3dnoticias.com.ar/./jaton-transporte1.jpg)