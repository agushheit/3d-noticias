---
category: Estado Real
date: 2021-08-15T06:00:54-03:00
thumbnail: https://assets.3dnoticias.com.ar/INCENDIOS.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Delta del Paraná: se retoma el operativo conjunto para el combate de incendios
  en territorio entrerriano'
title: 'Delta del Paraná: se retoma el operativo conjunto para el combate de incendios
  en territorio entrerriano'
entradilla: Se registraron nuevos focos frente a Villa Constitución. La provincia
  trabaja de manera articulada con el gobierno de Entre Ríos.

---
La provincia de Santa Fe puso en marcha un equipo de operaciones para combatir los focos de incendios frente a Villa Constitución, los cuales pertenecen a territorio entrerriano.

Durante toda la jornada, se realizaron acciones desde las localidades de Alvear y Villa V

Constitución en las que trabajaron de forma coordinada más de 30 personas.

Debido a las condiciones climáticas de niebla, recién a partir del mediodía de ayer viernes se realizó el traslado de 22 brigadistas al terreno afectado, en un helicóptero con helibalde enviado por el Ministerio de Ambiente y Desarrollo Sostenible de la Nación. Por su parte, desde el Aeródromo de Alvear, se realizó el vuelo de un avión hidrante de forma continua, gracias a un convenio firmado entre el Gobierno de Santa Fe con la Federación Argentina de Cámaras Agroaéreas (FEARCA). Además de estas acciones, en Villa Constitución los agentes involucrados armaron una sala de operaciones para monitorear de cerca los focos de incendios.

Del Operativo Interministerial participaron integrantes del Ministerio de Ambiente y Cambio Climático, del Ministerio de Gobierno, a través de la Secretaría de Protección Civil, de la Brigada de Atención y Prevención de Emergencias de Santa Fe y de la Brigada de Entre Ríos.

También se encontraron en servicio integrantes del Ministerio de Seguridad, a través de la guardia rural Los Pumas, y Bomberos Voluntarios.

Cabe destacar que en el día de ayer la Guardia Rural Los Pumas, en una embarcación

perteneciente al Ministerio de Ambiente y Cambio Climático de la Provincia de Santa Fe,

mientras realizaban sus tareas de prevención y patrullaje en el río Paraná, detectaron una columna de humo en territorio entrerriano, la cual fue confirmada con el vuelo vigía que se llevó adelante unos minutos más tardes. Por tal motivo, es que desde esta mañana se dio inicio nuevamente al operativo interministerial e interprovincial.