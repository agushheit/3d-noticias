---
category: Agenda Ciudadana
date: 2020-12-05T11:58:24Z
thumbnail: https://assets.3dnoticias.com.ar/PESCA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Operativo de relevamiento de pescadores comerciales y de subsistencia de
  la Laguna La Picasa
title: Operativo de relevamiento de pescadores comerciales y de subsistencia de la
  Laguna La Picasa
entradilla: Se prevé la inscripción de licencias y permisos de pesca comercial y de
  subsistencia, otorgamiento de obleas identificatorias de embarcaciones, precintado
  de redes y asesoramiento a acopiadores y pescadería.

---
El Ministerio de Producción, Ciencia y Tecnología, a través de su Secretaría de Agroalimentos, llevará a cabo un operativo de relevamiento de pescadores comerciales y de subsistencia de la Laguna La Picasa, con el fin de cumplir con las normativas vigentes en nuestra provincia en materia pesquera.

Dicha actividad tendrá lugar el próximo jueves 10 de diciembre desde las 10:30 horas en la localidad de Diego de Alvear. Los puntos de registro serán acordados con los municipios y comunas de la región y serán informados a través de los mismos.

El operativo prevé la inscripción de licencias de pesca comercial; la inscripción de permisos de pesca de subsistencia; el otorgamiento de obleas identificatorias de embarcaciones; precintado de redes y asesoramiento a acopiadores y pescaderías

En tanto que los requisitos que deben cumplir los pescadores que se presenten son los siguientes:

* DNI del titular (fotocopia 1º y 2º hoja) -DNI del cónyuge (original).


* CUIL del solicitante.


* Si es jubilado o posee otros ingresos, recibo de haberes.


* Antecedentes anteriores de pesca (si tuviera).


* Exhibir artes de pesca (mallas).


* Estar incluidos en la nómina solicitada por la comuna.