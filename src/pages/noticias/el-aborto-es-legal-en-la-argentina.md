---
category: Agenda Ciudadana
date: 2020-12-30T11:42:16Z
thumbnail: https://assets.3dnoticias.com.ar/3012-aprobación-aborto.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Infobae'
resumen: El aborto es legal en la Argentina
title: El aborto es legal en la Argentina
entradilla: 'El Senado sancionó la ley con una votación menos ajustada a lo esperado.
  Tras doce horas de debate, la Cámara Alta aprobó la normativa con 38 votos a favor,
  29 en contra y 1 abstención. '

---
**El apoyo de los cinco legisladores que no se habían expresado públicamente terminó inclinando la balanza.**

Luego de doce horas de debate, en una sesión histórica, el Senado nacional convirtió en ley el proyecto que legaliza el aborto hasta la semana 14 de gestación. **La sanción tuvo más votos a favor de los esperados durante los últimos días: 38 senadores, entre peronistas, radicales y del PRO**. Del otro lado, tuvo 29 en contra, también con un corte igual de transversal, además de una abstención. De todos modos, fue el Frente de Todos el que más votos aportó, aunque uno de sus máximos adversarios fue el jefe del bloque, José Mayans. 

Hubo dos ausencias que también restaron votos “celestes”: el ex gobernador de San Luis, Adolfo Rodríguez Saá, y la senadora riojana Blanca Vega. La abstención de Guillermo Snopek, peronista de Jujuy, también se descontó a ese sector.

Por primera vez desde que Cristina Fernández es Vicepresidenta, su hijo Máximo Kirchner, jefe del bloque de diputados del Frente de Todos, atravesó el Palacio Legislativo y siguió el tramo final de una sesión, en este caso los discursos de cierre y la votación de la interrupción voluntaria del embarazo junto con Sergio Massa, presidente de la cámara de Diputados. 

La titular del cuerpo se había retirado temprano y le dejó su lugar a Claudia Ledesma Abdala, a Martín Lousteau y a Laura Rodríguez Machado. Volvió para los últimos discursos de cierre, uno celeste y uno verde por cada bloque y para la votación que determinó la sanción de la ley de interrupción voluntaria del embarazo por un margen mucho mayor al que se preveía.

![](https://assets.3dnoticias.com.ar/3012-aborto1.webp)

La sesión se desarrolló sin sobresaltos, a pesar del enojo de Silvia Elías de Pérez, que viajó desde Tucumán, se vistió completamente de celeste y convocó a judicializar la ley. Hubo respeto en los discursos, que no tuvieron exabruptos ni duros epítetos. Desde un palco también siguieron los discursos la ministra de Mujeres, Géneros y Diversidad, Elizabeth Gómez Alcorta; la viceministra de Salud, Carla Vizotti; la asesora presidencial Dora Barrancos; la secretaria de Legal y Técnica, Vilma Ibarra, que fue la redactora del proyecto del Gobierno consensuado con senadores y diputados, y Malena Galmarini, presidenta de AySA. Hubo también varias diputadas impulsoras de la ley, todas con algo verde o vestidas de ese color.

En su mayoría fueron discursos emotivos, como el de la peronista Silvina García Larraburu, que en 2018 votó en contra y esta vez a favor. O el de la bonaerense Gladys González (PRO), que se describió como profundamente católica y recordó que perdió un embarazo dos días después de haber votado a favor del aborto. «Por un instante pensé que Dios me había castigado», dijo conmovida para luego aclarar que «el Dios en el que creo no es un Dios que castiga es un Dios que ama y es compasivo». Así, reclamó al Estado y a la Iglesia hacer algo para cambiar la situación.

En los cierres, el formoseño Luis Naidenoff, presente en el recinto, alzó fuerte la voz para reclamar la sanción del proyecto remitido por Diputados. Ya había votado a favor en 2018 y, nuevamente, fue el único senador de su provincia en apoyar la ley. «El negocio de la clandestinidad tiene la marca del dinero y el sello de la desigualdad» afirmó y pidió «terminar con esta injusticia». «La ley protege la desigualdad», remarcó y apuntó a los otros dos senadores formoseños que se opusieron. Habitualmente enfrentado con el oficialismo, celebró una votación compartida por distintos partidos políticos. «Ojalá, con este debate transversal que se ha dado, se pueda tener la capacidad de encontrar puntos de coincidencias en otros temas de la Argentina», subrayó. Aunque, a diferencia del planteo de algunos dirigentes de Juntos por el Cambio, consideró que «la prioridad es un trato igualitario para las mujeres».

![](https://assets.3dnoticias.com.ar/3012-aborto2.jpg)

Lo siguió Anabel Fernández Sagasti, vicejefa del bloque del Frente de Todos, que se describió emocionada y nerviosa. Asintió y sonrió al escucharla con suma atención Cristina Fernández. La mendocina destacó «ponerse de acuerdo sin mezquindades» como hicieron referentes de distintas fuerzas. «El Gobierno ha tomado una decisión difícil, con mucho coraje, porque lo más fácil es seguir mirando para otro lado», agradeció el envío del proyecto por parte del Ejecutivo.

Paradójicamente el cierre de la discusión, antes de la votación, quedó para Mayans que lanzó un reproche a Alberto Fernández: «El Presidente está obligado a respetar la Constitución», aseveró y además calificó como «una mala ley con muy mala redacción». Incluso criticó la posible reglamentación prometida a cambio de un par de votos. «No se puede alterar la ley», alertó mientras la Vicepresidenta miraba con gesto de fastidio. Usó argumentos que todo el año usó la oposición y también avisó que se judicializará. «No se respeta el orden institucional, si lo quieren hacer háganlo bien», insistió. En su caso se pasó del tiempo estipulado para los cierres que era de 25 minutos. «El Código Civil dice una cosa y el Código Penal dice otra cosa, no se puede acompañar este desastre legislativo que presentaron», fundamentó el formoseño.

El debate en el recinto había comenzado a las 16. El primer tema fue votar la ampliación de la licencia de José Alperovich, investigado en una causa por abuso sexual. Nadie puso reparos, aunque de haber estado hubiera elegido el lado celeste.

La definición se fue anticipando discurso tras discurso. Durante varias semanas, Oscar Castillo (Frente Cívico de Catamarca) guardó silencio. Fue breve, el más breve de todos. Anticipó el último voto que faltaba definir a favor de la interrupción voluntaria del embarazo. Simplemente dijo que volvería a votar como lo hizo dos años atrás y pidió insertar el mismo discurso. A la inversa la senadora García Larraburu (Frente de Todos) dijo que se deconstruyó y que en los últimos dos años escuchó y cambió de opinión: votó a favor de la ley, cuando en 2018 había votado con el grupo celeste que frenó la sanción. El resultado finalmente fue mucho más amplio de lo esperado o al menos de lo que se había hecho público.

En los últimos días de uno y otro lado contaban la misma cantidad de senadores a favor y en contra. Incluso se hablaba de una definición “por penales” y del intento kirchnerista por evitar un escenario en el que Cristina Fernández fuera al desempate.

Sin embargo el trabajo voto a voto de oficialistas y opositores y hasta de la Casa Rosada, logró un resultado más amplio. «Esta ampliación de derechos es imparable», afirmó Martín Lousteau al fundamentar su voto afirmativo y pedir a los que dudaban del voto que meditaran. «Se lo digo especialmente a los miembros de mi partido», agregó. Y le respondió a su correligionaria Silvia Elías de Pérez que pidió ir a la Justicia contra la ley: «Hemos escuchado algunas amenazas acerca de la inconstitucionalidad por el tema de los tratados internacionales, (…) lo expuso Gil Lavedra y quiero leer algo que para mí y para mi partido es muy importante. Alfonsín se opuso muy férreamente a que en la Constitución del 94 quedara plasmada alguna posición, por eso quiero leer un fragmento de sus propias palabras en la Convención Constituyente: la cláusula que estamos considerando ha sido el resultado de extensas conversaciones e intercambio de ideas este proyecto, que no le dice a la Legislatura que penalice el aborto o que libere cualquier posibilidad de aborto, sino que se trata de una iniciativa que podría estar perfectamente vinculada a la legislación de un país que acepta el aborto, como es Suecia, y también podría estarlo a la de un país como Irlanda, que lo prohíbe».

<br/>

<span style="font-family: Helvetica, sans-serif; font-size: 1.2em; font-weight: 800">**Una jornada histórica**</span>

«Este debate ha sido de profundo respeto», señaló Julio Cobos desde Mendoza, al reiterar que votaría en forma negativa. Su compañera de bancada Pamela Verasay votó a favor. El mendocino hizo un pedido a la senadora Norma Durango, miembro informante del Frente de Todos como presidenta de la comisión de la Banca de la Mujer que había anunciado en la apertura de la sesión el compromiso para modificar, con un veto parcial, un artículo de la ley cuestionado por algunos senadores. «La reglamentación con veto no puede ir contra el espíritu de la ley ni en más ni en menos», advirtió sobre una corrección pedida por Alberto Weretilneck como condición para votar a favor. El senador de Juntos Somos Río Negro había advertido que dejar como motivación para un aborto después de la semana 14 la “salud integral” de la persona gestante era un término demasiado abierto a interpretaciones. 

Durango avisó que el Presidente, al promulgar la ley, quitará la palabra “integral” de manera de evitar la vuelta de la norma a Diputados. Fue una de las negociaciones para lograr la aprobación y evitar además la vuelta a la cámara de origen y apurar la sanción.