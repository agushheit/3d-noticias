---
category: Estado Real
date: 2021-05-22T06:00:10-03:00
thumbnail: https://assets.3dnoticias.com.ar/AMBULANCIAS.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Provincia adquirió 60 nuevas ambulancias de alta complejidad
title: La Provincia adquirió 60 nuevas ambulancias de alta complejidad
entradilla: El gobernador Perotti, al entregar las primeras 12 unidades afirmó que
  “reformar el sistema de salud y la asistencia de la atención en la emergencia, es
  una muy buena noticia para todos los santafesinos y santafesinas”.

---
El gobierno provincial entregó este viernes 12 nuevas ambulancias de alta complejidad, de un total de 60 adquiridas para fortalecer el sistema de salud en el marco de la pandemia por Covid-19.

Estas primeras unidades, fueron destinadas a las localidades de Reconquista, Vera, Ceres, Florencia, Rosario, Santa Fe, Tostado, San Javier, Venado Tuerto, San justo, Rafaela y Rufino.

Al respecto, el gobernador Omar Perotti afirmó que “es más que oportuno poder sumar las primeras 12 ambulancias equipadas para alta complejidad, de las 60 que han sido adquiridas, cada una se va a su localidad, de punta a punta de la provincia, desde Florencia a Rufino. En los próximos días vamos a tener nuevas entregas, completando este lote”.

Además, Perotti destacó que “a estas 60 ambulancias las vamos a acompañar con otras que tendrán tracción 4 x 4 para ser destinadas al norte, por la cantidad de extensión de caminos rurales que tenemos que cubrir. Además, vamos a seguir con un plan que nos permita mejorar el parque, porque lo tomamos muy deteriorado”.

“Lamentablemente, son muchos los traslados en plena segunda ola, tener buenas unidades equipadas con respiradores dentro de la misma ambulancia es un elemento central. Reforzar el sistema de salud y la asistencia de la atención en la emergencia, es una muy buena noticia para todos los santafesinos y santafesinas”, agregó el mandatario.

Por último, el gobernador sostuvo que “el sistema está poniendo lo mejor, como cada uno de los enfermeros, de los médicos, de todo el personal que está al frente de las ambulancias atendiendo en la emergencia. Por lo cual, es también un aliciente muy fuerte para ellos que llegue el equipamiento. De la misma manera que equipamos enormemente con respiradores todo el sistema”.

Por su parte, la ministra de Salud, Sonia Martorano, expresó que “las ambulancias van a cubrir las necesidades de la provincia en la región norte, centro y sur. Es algo muy importante porque estamos en un momento muy difícil a nivel nacional y provincial, cursando una pandemia histórica que nos sorprendió e interpeló. Este instrumento va a ser fundamental para que las localidades realicen los traslados de los pacientes”.

Asimismo, Martorano reconoció que “se está trabajando fuertemente en el fortalecimiento de todo el sistema de salud, no sólo en lo que tiene que ver con las camas críticas, que se triplicaron, sino trabajar también en el recurso humano con la preparación a través de simulacros”, y agregó que la entrega de estas unidades “también es un aporte, porque son ambulancias con alta tecnología, que van a servir de soporte en las localidades y esto también tiene que ver con ese fortalecimiento del sistema de salud”.

**Atención de calidad**

Por último, el secretario de Emergencias Sanitarias de la provincia, Eduardo Wagner, destacó que “este es un esfuerzo más del gobierno, ya que es muy importante el equipamiento que se va a distribuir en toda la provincia. Se trata de ambulancias de alta complejidad que cuentan con respiradores, cardiodesfibriladores, que van a permitir que un paciente crítico pueda ser trasladado en una terapia intensiva móvil a un lugar de mayor complejidad, brindando una atención de calidad al paciente”.

“En esta primera etapa, estas 12 ambulancias están distribuidas en las cinco regiones de la provincia, muy equitativamente, para que puedan responder a las necesidades que hoy tienen, sobre todo en el norte, donde no hay empresas privadas y el único soporte que tienen es el Estado. Entonces es muy importante, también, reforzar ese lugar”, concluyó Wagner.