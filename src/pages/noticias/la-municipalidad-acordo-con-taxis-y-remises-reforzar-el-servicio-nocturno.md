---
category: Agenda Ciudadana
date: 2021-11-09T06:00:58-03:00
thumbnail: https://assets.3dnoticias.com.ar/TAXISTAS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Municipalidad acordó con taxis y remises reforzar el servicio nocturno
title: La Municipalidad acordó con taxis y remises reforzar el servicio nocturno
entradilla: Se llevó a cabo una reunión y se definió tener guardias con horarios rotativos
  para cumplir con la prestación durante la noche. También se decidió otorgar más
  licencias.

---
En la Estación Belgrano se desarrolló una reunión entre funcionarios municipales y representantes del subsistema de taxis y remises que se presta en la ciudad. Participaron empresarios e integrantes de los sindicatos en representación de los choferes. Entre otros temas, se acordó reforzar el servicio durante la noche y se anunció el otorgamiento de nuevas licencias.

Este tipo de encuentros se viene desarrollando de manera semanal con el fin de lograr la prestación de un mejor servicio a los santafesinos. En esta reunión estuvo más presente el tema de inseguridad a partir de los hechos que se incrementaron en los últimos días y desde el municipio destacaron la importancia de coordinar acciones con el Ministerio de Seguridad de la provincia.

Tras la reunión, Lucas Crivelli, subsecretario de Movilidad y Transporte del municipio, dio detalles: “Una de las cuestiones que habíamos acordado son las guardias nocturnas porque los usuarios necesitan de la prestación del servicio. No se trata de una guardia excesiva sino que son 4 horas semanales por vehículo y todas las partes estamos de acuerdo en que se preste y desde el municipio las estamos exigiendo y en este encuentro lo que se acordó es que estas guardias sean rotativas”.

Por su parte, el presidente de la Sociedad de Taximetristas Unidos de Santa Fe, Damián Cóceres, si bien hizo mención a la preocupación por la inseguridad que atraviesa el sector, destacó el acuerdo al que llegaron con el municipio con el fin de prestar estas guardias nocturnas. “En la mesa nos pusimos de acuerdo y se van a rotar los horarios para que no trabajen siempre los mismos choferes y vamos a seguir intentando garantizarlo como sucede hasta ahora”, dijo.

Hoy el servicio nocturno se presta de 22 a 6 y el propósito es reforzar ese horario y la intención es justamente que sean rotativas estas guardias para que no siempre recaiga en los mismos choferes.

**Más licencias**

Otro de los temas que también se analizó en esta mesa de trabajo tiene que ver con las licencias. En este sentido, Crivelli detalló: “Hay cerca de 150 licencias caídas de remises y unas 25 de taxis que estamos reactivando y poniendo otra vez en el sistema. Ya iniciamos también las debidas intimaciones porque no actualizaron en el último tiempo y son más de 50; y en el transcurso del mes hay 100 licencias que deberían retornar al municipio para ser reasignadas”

En este sentido, el funcionario consideró que esto último es fundamental para una mejor prestación. De todas maneras, aclaró que “esto lleva su tiempo porque hay una serie de requisitos y trámites que se deben hacer, y completar la documentación necesaria”. Además se hará un listado con los interesados en adquirir una licencia, como marcan las normativas vigentes.