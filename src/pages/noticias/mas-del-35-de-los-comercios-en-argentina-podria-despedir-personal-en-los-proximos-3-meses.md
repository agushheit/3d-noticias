---
category: Agenda Ciudadana
date: 2020-12-23T11:20:48Z
thumbnail: https://assets.3dnoticias.com.ar/2312cac.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: El Litoral'
resumen: Comercios en Argentina podrían despedir personal en los próximos 3 meses
title: Más del 35% de los comercios en Argentina podría despedir personal en los próximos
  3 meses
entradilla: El dato surge de una encuesta realizada por la Cámara Argentina de Comercio
  y Servicios (CAC), que se difundió este martes.

---
**Más del 35% de los comercios del país tiene previsto despedir personal en los próximos tres meses**, a raíz de las restricciones que aún siguen vigentes por la pandemia, de acuerdo con una encuesta difundida este martes.

Según un relevamiento efectuado recientemente por la Cámara Argentina de Comercio y Servicios (CAC), el 29,9% de las empresas de la actividad están sin operaciones desde el inicio de la cuarentena -el 20 de marzo último-, mientras que el resto se encuentra total o parcialmente operativa.

Además, el 35,6% despedirá empleados en los próximos tres meses y el 73,5% estima que la situación económica del país dentro de seis meses estará «peor o mucho peor» que ahora.

En tanto, el 46% vaticina que su situación estará igual dentro de seis meses y, de continuar las restricciones actuales por los próximos 30 días, el 54% de las empresas podrán continuar operativas; el 19,5% sufrirá importantes pérdidas; el 17,2% deberá reducir su tamaño y el 11,5% cerrará sus puertas, precisó la encuesta de la CAC, que se realizó entre el 15 y el 20 de este mes.

El relevamiento también arrojó que el 24,1% las ventas mercantiles cayeron más del 50% respecto a la situación previa a la pandemia, y que el 73,6% no realizaba ventas on line antes de la pandemia: ahora ese porcentaje cayó al 52,9%.

Asimismo, el 74,7% asegura estar sin atraso en el pago de salarios; 57,5% sin atraso en el pago de impuestos y 69% en el pago del pago de servicios. Apenas el 50,6% dice que podrá pagar el aguinaldo en las «condiciones habituales».

Por último, el 25,4% de las compañías continúa con teletrabajo y el 74,7% no realizará inversiones en el corto plazo.

Participaron del relevamiento empresas de diversos tamaños: hasta 9 empleados (58,6%); entre 10 y 49 empleados (25,3%); entre 50 y 200 empleados (11,5%); y empresas con más de 200 empleados (4,6%), de rubros tales como comercio minorista, comercio mayorista y gastronomía, entre otros.

Por otra parte, el 54% de las empresas encuestadas manifestó realizar actividades esenciales.