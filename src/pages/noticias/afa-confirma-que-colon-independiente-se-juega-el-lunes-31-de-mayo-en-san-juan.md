---
category: Deportes
date: 2021-05-25T08:47:19-03:00
thumbnail: https://assets.3dnoticias.com.ar/colon.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: AFA confirma que Colón-Independiente se juega el lunes 31 de mayo en San
  Juan
title: AFA confirma que Colón-Independiente se juega el lunes 31 de mayo en San Juan
entradilla: La otra semifinal entre Boca y Racing irá el mismo día. En tanto, la final
  se disputará el miércoles 2 de junio. Todo se jugará en el estadio del Bicentenario. 

---
La semifinal de la Copa de la Liga Profesional entre Colón e Independiente se disputará el lunes 31 de mayo en San Juan, según confirmó este lunes por la noche la AFA y TNT Sports, la cadena que transmite la competición. 

El otro duelo Boca - Racing también fue programado para ese día. En tanto, la final fue anunciada para el miércoles 2 de junio. Todo se jugará en el estadio del Bicentenario. 

Aún resta definir el horario. Más temprano la señal de TV había comunicado que los tres encuentros se disputaban a las 16, pero luego aclaró que aún no se definió la hora de inicio. 

Cabe recordar que tras el anuncio de Alberto Fernández sobre un nuevo confinamiento estricto, el pasado viernes la Asociación del Fútbol Argentino se acopló a la medida y suspendió los partidos previstos para el fin de semana de la Copa de la Liga Profesional. 

El sábado estaba programada la semifinal entre Colón e Independiente a jugarse en San Juan desde las 19. Mientras que la otra llave se iba a disputar desde las 15.30 del domingo entre Boca Juniors y Racing. 

![](https://assets.3dnoticias.com.ar/afa.jpg)