---
category: La Ciudad
date: 2021-05-05T08:02:31-03:00
thumbnail: https://assets.3dnoticias.com.ar/cullen.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad de Santa Fe
resumen: Cortes de calle para montar una rampa frente al hospital de campaña
title: Cortes de calle para montar una rampa frente al hospital de campaña
entradilla: Será desde las 8.30. La rampa que vinculará al hospital Cullen con el
  centro asistencial ubicado en el predio del Liceo Militar. También habrá desvío
  de colectivos urbanos.

---
A partir de este miércoles, habrá desvíos de tránsito sobre avenida Freyre. El operativo de tránsito responde al comienzo de tareas de montaje de una rampa que vinculará al hospital Cullen con el centro asistencial reubicable, instalado por el Gobierno Nacional en el predio del Liceo Militar General Manuel Belgrano.

Por eso, se interrumpirá la circulación vehicular en:

* Avenida Freyre y Juan de Garay, sentido sur-norte. El desvío será hacia Juan de Garay
* Avenida Freyre y Lisandro de la Torre, sentido sur-norte. El desvío será hacia avenida Freyre, con sentido norte-sur

En lo que refiere al transporte público, la municipalidad informó que la Línea 16 modificará su recorrido de la siguiente manera: avenida Freyre, Moreno, Francia, Salta y San Lorenzo, hacia recorrido habitual. Las paradas se ubicarán, entonces, en avenida Freyre y Moreno, Saavedra y Moreno, Francia y Corrientes, Francia y Lisandro de la Torre, y Salta y San Lorenzo, hacia recorrido habitual.