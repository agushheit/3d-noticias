---
category: La Ciudad
date: 2021-12-27T06:00:48-03:00
thumbnail: https://assets.3dnoticias.com.ar/fumigacion.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Trabajos de fumigación, iluminación y bacheo previstos para este lunes
title: Trabajos de fumigación, iluminación y bacheo previstos para este lunes
entradilla: "La Municipalidad informa el avance del cronograma de tareas previstas
  en los planes de fumigación, de bacheo y Santa Fe se Ilumina.\n\n"

---
La Municipalidad avanza con el calendario semanal de fumigaciones en distintos sectores de la capital provincial. Como es habitual, se utilizan insecticidas de baja toxicidad. Según se informó, se aplican larvicidas en las zonas donde hay zanjas y zanjones, y adulticida en parques y paseos.

En ese sentido, está previsto la realización de trabajos en:

Con pulverizador:

* Costanera Este
* Paseo de la Laguna
* Zona de boliches frente a Ciudad Universitaria y espacios verdes linderos
* Barrio El Pozo
* Parque Garay

Con motomochilas:

* Parque del Sur
* Estación Mitre y espacios verdes linderos
* Convento de San Francisco
* Barrios del distrito sur y suroeste de la ciudad

Vale recordar que en caso de lluvia, se suspende el servicio.

**Iluminación**

En el marco del plan Santa Fe se Ilumina, el municipio concreta tareas tendientes a recuperar el alumbrado existente en toda la ciudad, para llegar a fin de año al 40% de la ciudad con luces led. Cuadrillas municipales trabajan en la colocación de nuevas columnas de iluminación y artefactos led en distintos barrios y avenidas, como así también en el mantenimiento de la red actual de la ciudad.

En ese sentido, se trabajará en la colocación de nuevas luminarias en:

* Los barrios: 7 Jefes, Fomento 9 de Julio, Roma y Guadalupe

También se realizará el mantenimiento del sistema eléctrico, entre otros puntos, en:

* Distrito la costa, donde se instalarán 1500 metros de nuevas líneas

**Bacheo**

Por otra parte, el municipio avanza con el plan de bacheo diseñado para reparar más de 12.000 metros cuadrados de calles de la capital provincial. El programa se desarrolla por fases, priorizando las zonas de la ciudad que evidencian mayor circulación.

Actualmente se trabaja en:

* Lavaisse y Gobernador Freyre
* Europa y Boneo
* Calles internas de barrio Candioti Norte

Según se informa, todas las tareas se realizan siempre en sentido del tránsito. Puede haber cortes de circulación y desvíos de colectivos a medida que avanzan las obras. Además, se detalla que los trabajos están sujetos a las condiciones climáticas.