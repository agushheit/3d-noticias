---
category: Agenda Ciudadana
date: 2021-06-08T08:00:17-03:00
thumbnail: https://assets.3dnoticias.com.ar/vacunación.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Diario Uno
resumen: Este sábado comienzan a vacunar a personas sin patologías previas
title: Este sábado comienzan a vacunar a personas sin patologías previas
entradilla: Así lo aseguró la ministra de Salud, Sonia Martorano. Además, confirmó
  que para este viernes "va a estar vacunada la población objetivo que comprende personas
  de 18 a 59 años con comorbilidades".

---
La ministra de Salud, Sonia Martorano, recorrió este lunes el Centro de Vacunación del Predio ex Rural de la ciudad de Rosario en el marco de la continuidad de la vacunación contra el Covid-19 que despliega el gobierno provincial en todo el territorio santafesino. En ese contexto, explicó que “estamos ya en un 70% de vacunación de la población objetivo y para este viernes van a estar colocadas todas las vacunas en la población que comprende personas de 18 a 59 años con comorbilidades; ya para este sábado están turnadas personas sin patologías previas”.

Martorano informó que “las diferentes ampliaciones en los vacunatorios provinciales nos permitirán aplicar aproximadamente 40.000 dosis diarias; y a partir de la llegada de las vacunas se programaron turnos para sábados y domingos, no sólo para colocar primeras dosis sino también segundas y así completar los esquemas de vacunación; con una logística muy bien preparada y llevada adelante de gran manera por los equipos de salud”.

Al ser consultada por la cantidad de personas con comorbilidades a vacunar, la ministra de Salud señaló que “lo tenemos mensurados más o menos en 300.000 personas, 150.000 ya las tenemos turnadas para esta semana y el resto hay muchas que fueron vacunadas por ser profesionales de la salud, docentes o en seguridad”. Y agregó: “Recuerden aquellos no se han anotado, háganlo en el registro de vacunación web; y quienes tengan comorbilidades deben traer certificado médico y hacer una declaración jurada”.

**Situación epidemiológica**

En cuanto a las reuniones que se mantienen a diario con funcionarios nacionales, ministros y gobernadores de las diferentes provincias del país, Martonano explicó que “en cada una de los encuentros se trata el esquema de vacunación. Además se comparte la situación epidemiológica de cada provincia, y hay un diálogo muy fluido con las autoridades nacionales".

Asimismo, la ministra de Salud explicó que “si podemos sostener la baja de contagios diarios podemos ir mejorando a futuro el pronóstico. Recordemos que se trata de un virus mucho más agresivo, con predilección por la gente joven, gran contagiosidad y hoy una cama mínimo está ocupada 21 días”.

"Hay que sostener las medidas de precaución, de autocuidados personales, así podríamos mejorar los números de los efectores de salud. En este contexto de emergencia sanitaria internacional que estamos viviendo es de suma necesidad que mantengamos los cuidados, el distanciamiento social, usemos correctamente el barbijo y lavado de manos. Con esto y la vacunación, que cada día es más ágil, podemos tener un panorama mas favorable”, concluyó Martorano.