---
category: Agenda Ciudadana
date: 2021-11-15T06:00:07-03:00
thumbnail: https://assets.3dnoticias.com.ar/PARTICIPACION.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Aumentó la participación de ciudadanos en las urnas en un 5%
title: Aumentó la participación de ciudadanos en las urnas en un 5%
entradilla: "El incremento era un escenario esperado, debido a la mejora de la situación
  epidemiológica por el avance de la vacunación contra el coronavirus y la flexibilización
  de los protocolos sanitarios.\n\n"

---
La participación de votantes en las elecciones legislativas de medio término que se realizaron este domingo en todo el país se ubicaron ente el 71 y 72 por ciento, según las primeras proyecciones, lo que mostró un aumento respecto a las primarias, abiertas, simultáneas y obligatorias (PASO) del 12 de septiembre, que marcaron un 66,21 por ciento.  
  
El avance en el plan estratégico de vacunación, con 63.972.842 de dosis aplicadas -35.732.443 con esquema iniciado y 27.427.249 con esquema completo-, además de los refuerzos que ya se aplican con terceras dosis a adultos mayores, hizo posible que este domingo se pueda concurrir a votar en con un protocolo menos estricto que en las PASO de septiembre.  
  
Así, cuando marcaban las 18 y se cerraban los comicios, el ministro de Interior Eduardo 'Wado' de Pedro precisó en conferencia de prensa desde el Centro de Cómputos, en Barracas, que "el porcentaje de votación proyectado" era de "alrededor del 71 ó 72 por ciento".  
  
Esta convocatoria a las urnas mostró un aumento en la participación de la ciudadanía respecto a las PASO del 5 por ciento, un escenario que se esperaba debido a la mejora de la situación epidemiológica y la flexibilización de los protocolos sanitarios en el marco de la pandemia provocada por el coronavirus.  
  
Sin embargo, el porcentaje total de participación es menor que el de las legislativas de 2017, cuando se presentó el 77.61% de la ciudadanía a votar; y en 2019, en la elección presidencial que ganó el Frente de Todos con la fórmula Alberto Fernández-Cristina Fernández de Kirchner, en la que emitió su voto el 81,08% del electorado.  
  
En todos los casos, las elecciones generales de cada año, en retrospectiva lograron mayor adhesión de votantes que sus respectivas PASO.  
  
Así, en 2011 votó el 79,39% del padrón, en la elección que reeligió en la presidencia a Cristina Fernández de Kirchner; mientras que en las legislativas de 2013 lo hizo el 77,64%.  
  
En 2015, lo hizo el 81,07% y en la segunda vuelta que consagró como presidente a Mauricio Macri votó el 80,77%.  
  
En tanto, el número más alto de asistencia a las urnas que se registró en 1983, en el contexto de la vuelta de la democracia, cuando en el marco de la victoria de Raúl Alfonsín, se votó el 85,61% del padrón electoral.  
  
Como contracara, en las PASO del 12 de septiembre, en el marco de la pandemia provocada por el coronavirus y los estrictos protocolos para la votación, la participación de la ciudadanía marcó "el índice más bajo desde que se implementaron las primarias obligatorias".  
  
En esa ocasión, sobre un padrón de 34.385.460 electores, concurrieron a votar 22.765.590 personas, lo que constituyó el 66,21% del padrón, según informó el Ministerio del Interior.  
  
En retrosperspectiva, en 2011 acudió a las urnas un 78,66% de los inscriptos, en una elección que incluía cargos ejecutivos además de los legislativos, que se renuevan cada dos años.  
  
En 2013, votó el 76.83 %, sólo para cargos legislativos; mientras que en las PASO del 2015 el porcentaje de ciudadanos que emitió su sufragio fue del 74,91%, en una elección con renovación ejecutiva y parlamentaria.  
  
En las elecciones de medio término de 2017 el porcentaje de asistencia a las elecciones primarias para la contienda legislativa había sido del 72,37%; y en las últimas PASO, en 2019, se registró un 76,4%, también de cargos ejecutivos y legislativos.  
  
El récord de ausentismo en las legislativas a nivel nacional se registró en 2001, cuando el porcentaje fue del 26%, en el marco del llamado "voto bronca", que sumó además casi un 20 por ciento de votos en blanco y nulos, tras la crisis política y económica de ese año.