---
layout: Noticia con video
author: .
resumen: "Proyecto Interpuertos "
category: La Ciudad
title: "Interpuertos: AAUCAR pide que avance el proyecto"
entradilla: El Parque Interpuertos es una iniciativa que permitirá transformar
  los terrenos del noroeste de la ciudad, lindante con Los Polígonos, en un
  moderno centro de transferencias de carga.
date: 2020-11-03T23:29:00.000Z
url: https://youtu.be/18zNaUXpLB4
thumbnail: https://assets.3dnoticias.com.ar/2020-10-07.jpg
---
Desde la Asociación de Autotransporte de Cargas de Santa Fe (AAUCAR) y la Asociación Argentina de Transportadores de Hacienda (AATHA) piden retomar el diálogo con las autoridades para avanzar con el proyecto Interpuertos.

El Parque Interpuertos es una iniciativa que permitirá transformar los terrenos del noroeste de la ciudad, lindante con Los Polígonos, en un moderno centro de transferencias de carga. Se busca aprovechar la ubicación y los accesos a la avenida Circunvalación, para generar un espacio que nuclee las actividades del transporte de cargas en general y, también, la de los transportistas de Hacienda.