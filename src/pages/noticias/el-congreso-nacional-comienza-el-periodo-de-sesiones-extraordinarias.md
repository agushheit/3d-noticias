---
category: Agenda Ciudadana
date: 2021-01-02T10:24:39Z
thumbnail: https://assets.3dnoticias.com.ar/01012021-Congreso.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: El Congreso Nacional comienza el período de sesiones extraordinarias
title: El Congreso Nacional comienza el período de sesiones extraordinarias
entradilla: El Congreso Nacional comenzará este lunes el período de sesiones extraordinarias,
  que se extenderá hasta el 28 de febrero y comprenderá un amplio temario.

---
El temario a tratar contempla **la reforma judicial, la modificación del sistema de elección del Procurador y la deuda pública**.

En las últimas sesiones ordinarias, que fueron ampliadas hasta el 3 de enero, el Senado convirtió en ley, en la madrugada del miércoles, el **Proyecto de Interrupción Voluntaria del Embarazo**  (IVE) hasta la semana 14 de gestación, en un debate histórico de ese cuerpo legislativo.

La ley girada al Poder Ejecutivo Nacional deberá ser promulgada por el Gobierno Nacional, para lo cual tendrá diez días hábiles que vencerán el 14 de enero. Como es de rigor, tras esa instancia llegará el paso de la reglamentación, también en manos del Ejecutivo.

En la misma jornada  se aprobó, además, el denominado **Plan de los 1000 Días** que establece una nueva asignación por Cuidado de Salud Integral y consiste en el pago de una Asignación Universal por Hijo, a concretarse una vez por año, para contribuir al cuidado de cada niño o niña menor de tres años.

Por otra parte, la Cámara de Diputados dio sanción definitiva al proyecto que establece una **nueva fórmula de ajuste de las jubilaciones, pensiones, y asignaciones sociales**, que se calculará sobre la base del crecimiento de la recaudación y los salarios.