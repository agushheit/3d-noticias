---
category: Agenda Ciudadana
date: 2020-12-04T10:34:26Z
thumbnail: https://assets.3dnoticias.com.ar/TURISMO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: La provincia de Santa Fe autorizó la vuelta progresiva del turismo interno
title: La provincia de Santa Fe autorizó la vuelta progresiva del turismo interno
entradilla: Regirá desde el 5 de diciembre para residentes y propietarios de inmuebles
  en la provincia; y desde el 21 de diciembre para visitantes de otras zonas del país
  en todo el territorio santafesino.

---
El gobierno de la provincia de Santa Fe dispuso, a través del decreto Nº 1640, un **cronograma progresivo de la vuelta a la actividad turística en el territorio santafesino**. La norma establece que a partir del sábado 5 de diciembre de 2020 se habilita para:

1- El turismo interno de residentes de la provincia de Santa Fe.

2 - Los propietarios de inmuebles o complejos turísticos, aunque no sean residentes en la provincia, para concurrir y alojarse en los que son de su propiedad.

3 - Para el turismo receptivo de personas residentes en otras provincias, cuyo destino turístico se encuentre en los Departamentos Caseros, Constitución, General López, Rosario y San Lorenzo.

Además, el decreto N°1640 establece que **a partir del lunes 21 de diciembre de 2020 se habilitará el turismo receptivo de personas residentes en otras provincias**, en el resto del territorio provincial.

## **PROTOCOLOS**

La autorización está sujeta al cumplimiento de los protocolos de las distintas actividades: alojamientos, cabañas, campings, playas y balnearios, guías de pesca y embarcados y turismo aventura; ocupación de hasta un 50% de las plazas disponibles; y seguimiento de las recomendaciones e instrucciones de la autoridad sanitaria.

En tanto, que las personas que quieran participar de las actividades turísticas en la provincia deberán **registrarse en la aplicación móvil COVID-19 de la provincia de Santa Fe** y completar con carácter de declaración jurada los datos que se requieren en la misma. 

Además, tendrán que **obtener el certificado de circulación** disponible en[ www.argentina.gob.ar/verano](https://www.argentina.gob.ar/verano); y acreditar la reserva de un servicio o alojamiento turístico autorizado.

**En el caso de viajar con menores de edad, el adulto responsable deberá consignar sus datos en la aplicación santafesina, en el campo asignado al efecto**.

Las autoridades provinciales y locales coordinarán los procedimientos de fiscalización para garantizar el cumplimiento de las medidas de distanciamiento social, preventivo y obligatorio y los protocolos vigentes.

Si las autoridades locales detectaran un signo de alerta epidemiológico o sanitario, deberán comunicarlo de inmediato a la autoridad sanitaria provincial, que podrá recomendar al Poder Ejecutivo la suspensión total o parcial con carácter preventivo cuando la evolución de la situación lo aconseje.