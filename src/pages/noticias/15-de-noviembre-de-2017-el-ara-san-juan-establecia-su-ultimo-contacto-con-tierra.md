---
category: Agenda Ciudadana
date: 2021-11-16T06:15:56-03:00
thumbnail: https://assets.3dnoticias.com.ar/ARA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: 15 de noviembre de 2017, el ARA San Juan establecía su último contacto con
  tierra
title: 15 de noviembre de 2017, el ARA San Juan establecía su último contacto con
  tierra
entradilla: Este lunes se cumplen cuatro años de la última comunicación con la embarcación
  que patrullaba aguas argentinas, con 44 tripulantes a bordo.

---
El 15 de noviembre de 2017, exactamente cuatro años atrás, el submarino ARA San Juan estableció lo que sería su último contacto con tierra mientras navegaba por aguas argentinas con 44 tripulantes a bordo, a lo que siguió una intensa búsqueda que culminó con el hallazgo de restos de la nave, un año más tarde.  
  
El ARA San Juan, un submarino clase TR-1700, de 66 metros de largo y 3,6 de ancho, perdió contacto con la Armada a las 7.19 del miércoles 15 de noviembre de 2017, ocho horas después de que el jefe de operaciones del submarino informara sobre un principio de incendio en el tanque de baterías número 3, provocado presuntamente por el ingreso de agua por el sistema de ventilación, y que terminó provocando su hundimiento.  
  
Al momento de su desaparición, el buque navegaba a la altura del Golfo San Jorge, en medio de un fuerte temporal, mientras patrullaba la zona para tratar de identificar buques que pescaran ilegalmente dentro del espacio marítimo argentino.  
  
Los restos del submarino ARA San Juan, desaparecido junto a sus 44 tripulantes desde el 15 de noviembre de 2017, fueron localizados el 17 de noviembre de 2018 sobre el lecho marino a unos 800 metros de profundidad en el Atlántico Sur.  
  
Ese último contacto de la Armada con la nave ocurrió ese 15 de noviembre de 2017 a las 7:30 de la mañana y luego se perdió la posición del navío.  
  
Este lunes el actual ministro de Defensa, Jorge Taiana, hizo un llamado a "mantener vivo el recuerdo y el honor de cada tripulante" del submarino ARA San Juan, a cuatro años de su última comunicación.  
  
"Reafirmo mi compromiso personal, como ministro de Defensa, de continuar acompañando a sus familiares en la búsqueda de verdad y justicia", afirmó Taiana desde su cuenta personal de Twitter.  
  
La última posición conocida del ARA San Juan fue en el área de operaciones del Golfo San Jorge, a 240 millas náuticas (432 kilómetros de la costa), al sudoeste de la Península Valdés, y recién un año después, el 17 de noviembre de 2018, la empresa privada Ocean Infinity informó el hallazgo del submarino, muy cerca del punto de último contacto y a 907 metros de profundidad.  
  
Dos años más tarde se conoció que el gobierno del expresidente Mauricio Macri ocultó a los familiares y a la opinión pública que habían ubicado al submarino el 5 de diciembre de 2017, gracias a las declaraciones de uno de los oficiales de la Armada procesado en la causa, Enrique López Mazzeo, que lo reveló en la causa que tramitaba en la justicia federal de Caleta Olivia.  
  
Según el testimonio, desde esa fecha, veinte días después de la desaparición de la nave, se mantuvo la reserva hasta que el día 17 de noviembre de 2018, fue informado a la opinión pública.  
  
Pero, además, en septiembre del año pasado, se dieron a conocer detalles de actos de espionaje ilegal realizados a los familiares de los tripulantes del ARA San Juan, en momentos en el que el submarino aún permanecía desaparecido, entre enero y junio del 2018.  
  
La interventora de la Agencia Federal de Inteligencia (AFI), Cristina Caamaño, presentó ante la Justicia Federal de Mar del Plata la denuncia por presunto espionaje ilegal realizado durante el Gobierno de Macri.  
El expresidente Macri se presentó el 28 de octubre de este año ante la justicia federal de Dolores para dar declaración indagatoria, aunque fue suspendida porque no estaba relevado de su deber de confidencialidad, y fue reprogramada para el 3 de noviembre en el mismo lugar, pero el exmandatario se negó a declarar y presentó un escrito en que el aseguró que "jamás" espió ni ordenó espiar a nadie.  
  
Asimismo, la defensa de Macri recusó al juez de Dolores, Martín Bava, a cargo de la tramitación de la causa, y aún se espera la resolución de la Justicia respecto a si el magistrado continuará o no a cargo del expediente.  
  
Una semana más tarde, el ex ministro de Defensa, Oscar Aguad, se presentó ante el juzgado de Dolores para declarar como testigo pedido por la defensa de Macri en la causa de espionaje.  
  
Pero, además, a comienzos de noviembre del 2020, pocos días después del tercer aniversario, la querella mayoritaria de familiares de tripulantes del submarino denunciaron al exmandatario Mauricio Macri y al ex ministro de Defensa Oscar Aguad por el presunto encubrimiento del hundimiento del navío.  
  
La querella, patrocinada por los abogados Valeria Carreras, Lorena Arias y Fernando Burlando, presentó la denuncia penal ante los tribunales federales de Comodoro Py por encubrimiento agravado.  
  
El 20 de noviembre comenzó la investigación por el hundimiento, en el marco del Consejo de Guerra, para determinar las responsabilidades del hecho que se cobró la vida de 44 marinos.