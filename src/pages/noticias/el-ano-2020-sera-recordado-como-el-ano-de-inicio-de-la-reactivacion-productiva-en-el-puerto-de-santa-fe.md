---
category: El Campo
date: 2020-12-31T11:24:15Z
thumbnail: https://assets.3dnoticias.com.ar/puerto.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: El año 2020 será recordado como el año de inicio de la reactivación productiva
  en el Puerto de Santa Fe
title: El año 2020 será recordado como el año de inicio de la reactivación productiva
  en el Puerto de Santa Fe
entradilla: Tras décadas de estar inactivo, el Puerto de Santa Fe termina el año con
  23 embarques y más de 60.000 toneladas exportadas.

---
En el marco del plan de reactivación, en la Terminal de Agrograneles se intensificaron las operaciones portuarias con destinos a Uruguay, Taiwán y el centro-norte del país.

En tal sentido, el presidente del Ente Administrador del Puerto Santa Fe, Carlos Arese, destacó: «Nada de esto sería posible sin la previsión y las inversiones realizadas por el Puerto Santa Fe tanto en materia de dragado del canal de acceso y zona de maniobras, así como otras tantas inversiones en infraestructura portuaria».

Y añadió: «también es muy importante el trabajo que estamos realizando en la Terminal Agrograneles. Se está recuperando la “gimnasia portuaria”, ya que este tipo de operaciones requiere de mucha coordinación y trabajo en equipo».

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.15em; font-weight: 800">PUERTO PRODUCTIVO</span>**

«Ahora tenemos una visión de un “puerto productivo”. Allá por abril, cuando comenzaban a llegar los primeros camiones con granos, de quince o veinte camiones diarios pasamos a treinta, cuarenta y hoy estamos en más de cincuenta, actividad que en ese momento era solamente de ingreso y egreso, tomando los silos como simple almacenaje», relató Arese.

Los silos, con el paso de los días se fueron colmando de granos, a niveles históricos de almacenaje superando el 60% de la capacidad total. La lógica consecuencia de ello fue buscar una salida por agua. Y así fue como en el mes de junio amarró el primer buque después de muchos años de inactividad en el puerto.

En relación con esto, Arese señaló: «de esta forma llegamos a fin de año con 23 embarques en seis meses y más de 60.000 toneladas exportadas por agua desde la Terminal Agrograneles. Con los sucesivos embarques fuimos recuperando la gimnasia portuaria, el músculo productivo del Puerto Santa Fe, mejoramos los procesos y los rendimientos y lo hicimos invirtiendo en el reacondicionamiento de una infraestructura prácticamente “oxidada” por el paso del tiempo».

Estas acciones permitieron que las pymes de la región encuentren en el Puerto Santa Fe la posibilidad de exportar sus productos directamente, sin intermediarios, abaratando sus costos logísticos.

«Nuestro mayor éxito estuvo en dimensionar adecuadamente cuál era nuestro espacio, nuestra escala para trabajar. Nuestros clientes que están trayendo granos al puerto, son las empresas pequeñas y medianas de la zona, que son pymes agropecuarias que por su escala o dimensión se les dificulta la llegada a los grandes puertos del sur de la provincia. Con los buques sucede algo similar, son buques medianos adecuados a la medida de nuestro puerto, con aproximadamente cien metros de eslora, que cargan unas 2500/2.600 toneladas de granos en cada viaje».

Con relación al trabajo que se viene desarrollando en el Puerto de Santa Fe, la ministra de Infraestructura, Servicios Públicos y Hábitat, Silvina Frana, comentó: «Estas son las acciones concretas que durante años todos queríamos ver: un puerto activo y productivo. Esta es la Santa Fe de pie, potenciando su perfil productivo y de exportación».

«Cuando vemos afluencia de barcos, vemos el trabajo de nuestra gente y el impulso a la actividad agropecuaria e industrial. Esto muestra crecimiento y arraigo, con un objetivo claro de integración y de salida de productos de Santa Fe al mundo», concluyó Frana.

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.15em; font-weight: 800">VALOR AGREGADO</span>**

Una serie de acciones que complementan este cambio de modelo, es la asistencia técnica para cada cliente. En la propia planta se realiza la actividad de partición de los granos, para lo cual se tuvo que reparar la máquina partidora de granos. Por la alta demanda y una necesidad de cumplir con los tiempos de los clientes se incorporaron otras dos máquinas partidoras y se realizaron un sinnúmero de reparaciones e inversiones exclusivamente portuarias.

Además, se firmó un convenio con la Bolsa de Comercio de Santa Fe para el análisis de granos en el Laboratorio de la Cámara Arbitral de Cereales de la BCSF, a fin de realizar el procedimiento de almacenaje de acuerdo a la calidad del grano ingresado con las condiciones apropiadas.

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.15em; font-weight: 800">EL TURNO DEL TRIGO</span>**

Finalmente, desde el Ente Portuario mencionaron la llegada del primer camión con trigo de la cosecha 20/21. Hoy los silos del puerto almacenan unas 20.000 toneladas de trigo.

«Que estemos recibiendo trigo de la campaña 20/21 durante los embarques de maíz, es muy significativo y nos llena de entusiasmo, en este despertar productivo del Puerto de Santa Fe», subrayó Arese.

Los silos del puerto hoy almacenan soja, sorgo, maíz y el recién llegado, trigo. Con estas actividades se va consolidando la reactivación del Puerto Santa Fe, logrando la continuidad y regularidad en los embarques en la Terminal Agrograneles, potenciando el perfil productivo de Santa Fe.