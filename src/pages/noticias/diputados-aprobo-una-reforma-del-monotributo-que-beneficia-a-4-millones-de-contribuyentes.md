---
category: Agenda Ciudadana
date: 2021-07-03T06:00:03-03:00
thumbnail: https://assets.3dnoticias.com.ar/MONOTRIBUTO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Diputados aprobó una reforma del Monotributo que beneficia a 4 millones de
  contribuyentes
title: Diputados aprobó una reforma del Monotributo que beneficia a 4 millones de
  contribuyentes
entradilla: Con 233 votos a favor, el proyecto impulsado por el presidente de la Cámara,
  Sergio Massa, llevará alivio a 1.592.466 personas que trabajan en el Área de Buenos
  Aires y a 2.487.685 que lo hacen en el resto del país.

---
La Cámara de Diputados aprobó por una amplia mayoría y giró al Senado el proyecto de ley que crea un Programa de Fortalecimiento y Alivio Fiscal para Pequeños Contribuyentes, que permitirá que los monotributistas no deban afrontar ninguna deuda acumulada por la diferencia resultante entre lo que pagaron entre enero y junio y los nuevos valores establecidos por la ley 27.618.

El plenario legislativo aprobó por 233 votos que fueron aportados por el Frente de Todos, Juntos por el Cambio, los interbloques Federal y Unidad Federal para el Desarrollo, Acción Federal y Movimiento Popular Neuquino, mientras que los dos legisladores de la izquierda se abstuvieron.

La reforma de la ley del Monotributo impulsada por el presidente de la Cámara de Diputados, Sergio Massa, elimina el retroactivo con lo cual los monotributistas no deberán afrontar ninguna deuda acumulada por la diferencia resultante entre lo que pagaron entre enero y junio y los nuevos valores establecidos por la ley 27.618.

Con este proyecto se busca dar mayor alivio fiscal y previsibilidad a la actividad económica de los monotributistas que suman un total de 4.080.151 contribuyentes; de los cuales 1.592.466 (39%) se encuentran en el Área Metropolitana de Buenos Aires (AMBA), y 2.487.685 (61%) en el resto del país.

Los contribuyentes debían pagar el monotributo con nuevos valores, pero la AFIP prorrogó el vencimiento del monotributo del 25 de junio al 5 de agosto próximo.

Al abrir el debate, el presidente de la comisión de Presupuesto y Hacienda, Carlos Heller, señaló que ambos proyectos tienen beneficios para la ciudadanía, y al respecto expresó: "Todo lo que hemos hecho en esta modificación de la llamada Ley de Monotributo, está basado en ese criterio".

"Hemos dispuesto el sostenimiento de los valores de las cuotas que se pagan hasta junio, a los que había hasta diciembre del 20", agregó.

Destacó que "es un proyecto que busca aliviar la situación de 4 millones de monotributistas que tenían preocupación y angustia, para los que tenían deudas".

Por su parte, la legisladora del Frente de Todos, Alicia Aparicio señaló que "este proyecto no solo busca asistir a los sectores golpeados, también puede inscribirse en un programa mayor que tiene como objetivo lograr el crecimiento mejorando la equidad del sistema".

En tanto, el sindicalista y diputado oficialista Hugo Yasky señaló que este proyecto "va a beneficiar a cuatro millones de monotributistas, forma parte de la lógica de volver a poner el salario y el consumo interno como el movimiento que lleve a la recuperación económica".

Desde la oposición, el radical Luis Pastori dijo que esta ley viene "a corregir la absurda situación provocada por la mala administración de la AFIP. sobre el retroactivo" y señaló que "nos parece bien el aumento de los parámetros de facturación bruta anual, que estaban muy desactualizados".

"También proponemos que se aumente otro parámetro, que es el del monto de los alquileres devengados, que hoy están en un promedio de 8.800 pesos mensual para las categorías A y B. Acompañamos también la moratoria que se otorga a los monotributistas".

Por su parte, el diputado del Pro Luciano Laspina aseguró que " esta ley que hoy vamos a votar no debe ser vista como un acto de reivindicación. Tiene que ser reconocida como un error administrativo injustificable del Ministerio de Hacienda, que no implica un alivio tributario para contribuyentes".

Uno de los aspectos que mayor debate generó fue el artículo 11 que se refería a las condiciones para poder acceder a los beneficios y allí el oficialismo impuso su criterio por 132 contra 102 votos de Juntos por el Cambio.

A lo largo del proyecto del monotributo se fijan los incrementos que tendrán escalas en las cuales las más bajas podrán duplicar los montos de facturación.

Otro punto central es que aquellos que se excedieron de la facturación prevista en su categoría podrán permanecer en el régimen siempre que no superen la facturación de $5,55 millones anuales y un patrimonio de hasta $6,5 millones.

Asimismo, se contempla un plan de pagos para regularizar la deuda de los monotributistas, con un máximo de 60 cuotas, un interés de financiación no superior a 1,5% mensual, para deudas que se venían pagando en planes vigentes y caducos, y para contribuyentes que no ingresen al Alivio Fiscal Monotributo, entre otras cuestiones.

**Ganancias y Bienes Personales**

El plenario legislativo aprobó y giró al Senado por 227 contra 3 votos un proyecto de reforma de los Impuestos a las Ganancias y de Bienes Personales con el fin de fomentar el ahorro en pesos destinado a ampliar las exenciones en los mencionados Impuestos para los activos de inversiones financieras en moneda nacional.

Uno de los puntos aspectos de este proyecto es que se eliminó el artículo tercero que permitía gravar por el tributo por bienes personales los plazos fijos que no se depositen por un plazo menor a 275 días como había anunciado el titular del cuerpo, Sergio Massa.