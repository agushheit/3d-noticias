---
category: La Ciudad
date: 2022-05-11T10:41:03-03:00
thumbnail: https://assets.3dnoticias.com.ar/TAXI.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3d noticias
resumen: Impugnan el Listado de Aspirantes a Licencias de Taxi.
title: Impugnan el Listado de Aspirantes a Licencias de Taxi.
entradilla: El sindicato unico de choferes de taxis de la ciudad  ipugnó el registro
  de aspirtante por "graves irregularidades".

---
A través de un parte de prensa, Pablo Ferrero, secretario general del Gremio choferes de taxis de Santa Fe, afirmo que "presentamos una impugnación al Registro de Aspirantes ante una serie de irregularidades en su confección".

Para poder obtener una licencia de taxi, hay que cumplir una serie de requisitos. Uno de ellos, es haber trabajado “ininterrumpidamente, los últimos 24 meses anteriores al momento de la convocatoria a la inscripción a dicho registro”.

Este requisito NO SE ESTA CUMPLIENDO EN ESTA CONVOCATORIA.

En la Resolución N° 67/2022, donde se lista el padrón, el municipio argumenta que, por razones derivadas de la pandemia, el cumplimiento del artículo expresado anteriormente, es injusta para aquellos que, en razón de fuerza mayor, como la cuarentena, no pudieron trabajar normalmente.

**Desde este sindicato recordamos a toda la ciudadanía, que, durante la pandemia, trabajamos adoptando todos los protocolos sanitarios respectivos, y fuimos la opción de transporte más usada para aquellos trabajadores esenciales. Y que se cumplieron con todos los requisitos legales, manteniendo las fuentes de trabajo. Los choferes que estuvimos trabajando los últimos 24 meses ininterrumpidamente, cumpliendo con todas las exigencias de las autoridades locales, estamos en DESVENTAJA al eliminar ese requisito.**

**Exigimos que la PRIORIDAD para encabezar los listados de aspirantes, sea para quienes cumplan TODOS LOS REQUISITOS EXIGIDOS.**

**Nos reservamos el derecho de presentar una medida de no innovar a la justicia, si no se nos convoca a trabajar en pos de un registro de aspirantes que contemple a los verdaderos trabajadores del sistema.![](https://assets.3dnoticias.com.ar/WhatsApp Image 2022-05-11 at 10.38.28 AM.jpeg)**