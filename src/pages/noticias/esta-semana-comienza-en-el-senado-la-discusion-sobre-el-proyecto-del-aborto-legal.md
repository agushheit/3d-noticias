---
category: Agenda Ciudadana
date: 2020-12-14T12:09:44Z
thumbnail: https://assets.3dnoticias.com.ar/cristina1.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Infobae'
resumen: Está semana comienza en el Senado la discusión sobre el proyecto del aborto
  legal
title: Está semana comienza en el Senado la discusión sobre el proyecto del aborto
  legal
entradilla: En los próximos días habrá en tres comisiones varios expositores a favor
  y en contra de la iniciativa. Se espera que se pueda debatir el 29 de diciembre.

---
En un año en el que la actividad en el Congreso Nacional fue más intensa que en los anteriores, los últimos días del 2020, casi en el cierre de las sesiones ordinarias que se prorrogaron hasta el 3 de enero, prometen un debate arduo y complejo en el Senado de la Nación donde el oficialismo espera que el proyecto de ley sobre Interrupción Voluntaria del Embarazo (IVE) y el llamado “Plan de los 1.000 días” obtengan la aprobación de ese cuerpo legislativo para que se convierta en realidad una de las promesas electorales del presidente Alberto Fernández.

**Este lunes a partir de las 14 empezará a sesionar en forma virtual** el plenario de tres comisiones que el jueves emitiría un dictamen para que se pueda debatir el 29 de diciembre. Presidirá la de la Banca de la Mujer, que lidera la pampeana Norma Durango (Frente de Todos) y también intervendrán la de Justicia y Asuntos Penales, con el neuquino Oscar Parrilli a la cabeza (otro legislador del oficialismo) y la de Salud, cuyo titular es el radical jujeño, Mario Fein, quien ya expresó su rechazo a esta iniciativa que propicia la legalización y la despenalización del aborto.

Se espera para este primer día la defensa del proyecto que hará el Poder Ejecutivo luego de que se produjera la media sanción en la Cámara de Diputados en la madrugada del viernes pasado con 131 votos a favor, 117 en contra y 6 abstenciones. Por eso expondrán ante los senadores el ministro de Salud, Ginés González García, la ministra de Mujeres, Géneros y Diversidad, Elisabeth Gómez Alcorta y la secretaria de Legal y Técnica de la Presidencia, Vilma Ibarra. Estas dos últimas participaron activamente en la confección y la redacción de la iniciativa que el presidente Fernández elevó el lunes 17 de noviembre.

Para debatir el otro proyecto, el de los 1.000 días, habrá plenario de dos comisiones: la de Salud, que lo presidirá, y la de Presupuesto y Hacienda, que encabeza el cordobés Carlos Caserío.

***

![](https://assets.3dnoticias.com.ar/festejo.jpg)

***

**De acuerdo a los primeros sondeos las posturas a favor y en contra de la IVE se encuentran en un empate técnico.** Hasta último momento habrá negociaciones con la intención de volcar la balanza hacia un lado u otro. La pelea será voto a voto. Es una batalla que no se quiere perder como hace dos años. Aquella vez, la derrota por 38 a 31 y una abstención golpeó fuerte a quienes defienden la legalización y la despenalización del aborto en nuestro país. En los diferentes bloques, como ocurriera con los diputados, la posición no es uniforme. Todavía hay entre 5 y 6 senadores que no se pronunciaron sobre el tema.

Como ya sucedió en la Cámara de Diputados, irán a exponer en el Senado expertos en el tema entre este martes y miércoles con el objetivo de convencer a los indecisos o indefinidos. Quienes están a favor del proyecto y los que están en contra se dividirán en partes iguales a esos expositores, que serían, en principio, 30.

**Otra situación que se repite es el rol protagónico de la Iglesia que sobrevolará el Senado mientras se discuta este proyecto.** 

Sucedió otras veces, como cuando se trató el Divorcio vincular o el matrimonio igualitario en el Palacio Legislativo y las presiones fueron más que evidentes. Ya en la elección de la fecha para debatir, el 29 de diciembre, se advierte que esa fecha no es antojadiza. Desde el oficialismo trataron de no abrir más heridas entre los sectores católicos y evangélicos: evitar que fuera antes de Navidad, una fecha muy especial para todos los creyentes de esas religiones, que se oponen fuertemente a la iniciativa gubernamental. En tanto, los más de 100 obispos nucleados en la Conferencia Episcopal Argentina tienen previsto emitir este martes un documento con fuertes críticas hacia la iniciativa gubernamental.

**Cuando se voten ambos proyectos estarán habilitados para participar 71 senadores**. La Cámara la integran 72, pero el tucumano José Alperovich pidió la prórroga de su licencia hasta el 31 de diciembre, ya que es investigado en una causa por abuso sexual tras la denuncia de una de sus sobrinas, quien también había trabajado como su asesora. Hay otro senador, el catamarqueño Oscar Castillo, que declaró que se abstendrá. En 2018 había votado afirmativamente, pero en este momento acusa al Gobierno de oportunismo político. Más allá de estos dos casos, los que relevan números en la Cámara Alta, ven por ahora un 32 a 32 en las preferencias en la crucial votación. **En caso de haber igualdad, desempatará Cristina.**

Aunque solo falten unos pocos días para que los senadores decidan, son varios los que remarcan que el camino “será larguísimo”. Asimismo, destacan la infinidad de presiones que podrían recibir los legisladores. “No te olvides que lejos de acá, más que nada en las provincias, los senadores son tan conocidos como los gobernadores”, le contó en “off” un integrante de la Cámara a Infobae. Por eso todos se mueven con extrema cautela y tratando de no dar ningún paso en falso.