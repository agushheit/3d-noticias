---
category: La Ciudad
date: 2021-11-11T06:00:03-03:00
thumbnail: https://assets.3dnoticias.com.ar/titulos.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Profesores santafesinos no quieren "regalar el título" y se niegan a la promoción
  directa en el secundario
title: Profesores santafesinos no quieren "regalar el título" y se niegan a la promoción
  directa en el secundario
entradilla: "La organización Docentes por la Educación cuestiona que se \"regale el
  título\" a los alumnos y consideran que la medida podría fomentar la ociosidad.\n\n"

---
La circular 8/21 emitida desde la Subsecretaría de Nivel Secundario del Ministerio de Educación de la provincia de Santa Fe establece que los alumnos que en este último trimestre tengan una participación que supere el 70% podrán aprobar los espacios curriculares de 2020 y 2021. 

 Según indica el documento, "aquellas trayectorias que la escuela considera/valora como sostenidas al cierre del tercer trimestre, e independientemente de lo que haya sucedido en trimestres anteriores, tendrán como nota final una calificación en cada espacio curricular que oscilará entre el 6 (seis) y el 10 (diez) acordes a las escalas de calificaciones establecidas en el decreto 181/09, tanto en los espacios curriculares cursados durante 2020 como en los cursados en 2021, sean de igual denominación o con diferente denominación, pero del mismo trayecto. A modo de ejemplo: Matemática de 3er año 2020 y 4º año 2021, con trayectoria sostenida se registra asignándole una calificación de 6 o más en ambos espacios curriculares".

 Ante esta situación, El Litoral tomó contacto con Virginia Valenzisi, representante de los docentes, quien argumentó que “es totalmente injusto para los que sí se esforzaron en adquirir los conocimientos y también para el que no pudo alcanzar, porque si te cuesta aprender los contenidos básicos de una materia y te regalamos la aprobación, no lo volvés a ver en el resto de la secundaria”. “Y si tuviste cuarto y quinto año en pandemia, esos contenidos que se vieron de manera alterna entre la virtualidad, la modalidad de burbujas y la presencialidad, esos contenidos no los recuperás y si querés empezar una carrera terciaria o universitaria o insertarte en el mundo laboral, llegás con ese bache”, añadió la profesora.

 "Uno entiende - añadió Valenzisi -, porque estuvo trabajando y costó la revinculación, pero no podemos decirles “porque vos apruebes el último trimestre de tal materia, entonces aprobás también la materia correlativa anterior". "Además, hemos recibido mensajes de alumnos y papás organizados de diferentes lugares que coinciden en que no es justo y están contentos de que hayamos alzado la voz". 

 En tanto, la docente admitió que en algunas escuelas de la ciudad de Rosario ya están trabajando con los Supervisores “para que la acreditación de las unidades pedagógicas sea diferente o se pueda trabajar desde este planteo del periodo de apoyo con alumnos que también adeudan materias del año pasado y que no sea una promoción directa”, pero advirtió que “lamentablemente, consideramos que esto lo van a hacer solo las escuelas particulares o privadas y las escuelas de gestión pública generalmente no toman estas decisiones y esta medida ministerial para intentar subsanar todo lo que no se hizo durante estos dos años amplía las desigualdades entre los chicos”.

 