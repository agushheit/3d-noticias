---
category: Agenda Ciudadana
date: 2021-02-28T06:30:50-03:00
thumbnail: https://assets.3dnoticias.com.ar/BOLETO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Boleto Educativo: En sólo una semana, se inscribieron 82.000 beneficiarios'
title: 'Boleto Educativo: En sólo una semana, se inscribieron 82.000 beneficiarios'
entradilla: El Gobierno Provincial recorrerá los barrios de Santa Fe ciudad difundiendo
  la iniciativa.

---
Pasada una semana del lanzamiento del Boleto Educativo Gratuito, ya se inscribieron 82 mil beneficiarios, entre estudiantes, docentes y asistentes escolares.

"Al momento ya llegaron a ser 80 mil, y a partir de la semana entrante estaremos inscribiendo en otros barrios y en otras ciudades para seguir sumando beneficiarios, y el potencial es de 500 mil personas en toda la provincia", estimó el director del Boleto Educativo Rober Benegui.

Además del punto instalado en la Fábrica Cultural El Molino, Benegui confirmó una activa difusión de la herramienta de inclusión: "Saldremos con operativos móviles por toda la provincia, y por los distintos barrios de Santa Fe y Rosario."

El Boleto Educativo Gratuito incluye estudiantes de todos los niveles, docentes escolares de los establecimientos provinciales y estudiantes de universidades públicas y privadas y asistentes escolares. La inscripción es a través del portal web de la provincia ó a través de la app Boleto Educativo.