---
category: Estado Real
date: 2021-12-20T06:00:46-03:00
thumbnail: https://assets.3dnoticias.com.ar/losbajos.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: El gobierno de Santa Fe impulsa la creación de una reserva hídrica en los
  bajos submeridionales
title: El gobierno de Santa Fe impulsa la creación de una reserva hídrica en los bajos
  submeridionales
entradilla: La propuesta implica la protección de más de 300 mil hectáreas.

---

La Ministra de Ambiente y Cambio Climático, Erika Gonnet, y la Ministra de Infraestructura, Servicios Públicos y Hábitat, Silvina Frana, mantuvieron un encuentro en la Estancia Las Gamas junto al Equipo Técnico Interdisciplinario de los Bajos Submeridionales con el objetivo de continuar avanzando en diferentes iniciativas para la región. En ese sentido, desde el Ministerio de Ambiente y Cambio Climático, realizaron la presentación de un proyecto para la creación de una reserva hídrica natural en los Bajos Submeridionales. La actividad contó con la participación de integrantes del Ministerio de Producción, Ciencia y Tecnología y de la Administración Provincial de Vialidad.

“Junto al gobernador Omar Perotti impulsamos acciones climáticas concretas protegiendo áreas que brindan importantes servicios ecosistémicos y promoviendo modelos de desarrollo sostenible” comentó la Ministra Erika Gonnet y agregó que “la iniciativa de esta nueva reserva hídrica natural se suma al proyecto de ampliación del parque nacional Islas de Santa Fe que implica un aumento desde la superficie actual de 4096 hectáreas actuales a un total de más de 10.000 hectáreas”.

“Siempre estuvimos convencidos de la participación, pero no solamente ciudadana, sino que también tenga que ver con las instituciones y con la región. Para nosotros venir al territorio a contar algo que proyectamos es fundamental porque interactuamos con quienes viven y producen acá”, comentó la funcionaria y continuó: “Valoramos que las cuestiones ambientales se discutan en estos ámbitos porque siempre se dieron en contextos de enfrentamiento”.

La nueva reserva natural hídrica propuesta contempla una superficie de 311.367 hectáreas, con superficie de polígono, que se encuentra delimitado en el norte por la Ruta Provincial 31, hacia el Oeste por la Ruta Provincial 13, hacia el Sur por la Ruta provincial 32, un camino comunal y la ruta provincial 40, y al Este por el Arroyo Golondrina. La categoría elegida se encuentra establecida en el Sistema Provincial de Áreas Naturales Protegidas, a través de la Ley Provincial Nº12.175/03.

“Ante la explicación de los técnicos de Ambiente sobre la propuesta de esta reserva, se hicieron todos los aportes de entidades productivas y técnicas, sobre cómo mejorar la propuesta”, resaltó el coordinador del Equipo Técnico Interdisciplinario, Ing. Mario Basán, y agregó que “todos los actores estratégicos para nuestro territorio de la región de los bajos, están presentes”.

Luego de la actividad, el Director Regional de INTA en Santa Fe, Ing. Alejandro Longo comentó: “El balance es muy positivo porque se generó un ámbito de interacción entre lo público y lo privado muy importante”, y aseguró “Estamos totalmente alineados con la propuesta que escuchamos”.

Cabe resaltar que la iniciativa se encuentra enmarcada dentro del Programa Regenera Santa Fe, el cual propone un esquema estratégico de ampliación e incorporación de áreas naturales protegidas del territorio provincial para su protección y conservación. Regenera Santa Fe trabaja en base a tres ejes principales: la incorporación de nuevas áreas naturales protegidas, el Inventario Provincial de humedales, y la ampliación Parque Nacional Islas de Santa Fe.

**Trabajo interministerial**

Desde el Ministerio de Infraestructura, Servicios Públicos y Hábitat, se llevó adelante, además, la presentación del Plan Director de los Bajos Submeridionales, desarrollado de manera interjurisdiccional con las provincias de Santiago del Estero y Chaco. En este marco, la ministra Silvina Frana, comentó: "Desde enero del año 2020 hubo una clara definición del gobierno provincial y nacional de realizar una fuerte intervención en la zona de los Bajos Submeridionales. Esto tiene que ver con una política sustentable y de arraigar a nuestra gente en el norte, a fin de generar las condiciones necesarias para que la gente pueda vivir y desarrollarse en el lugar donde nació".

Por su parte, el secretario de Desarrollo Territorial y Arraigo, Fabricio Medina, expuso sobre las diferentes herramientas y programas productivos para la región de los Bajos Submeridionales con los que cuenta el gobierno de Santa Fe y señaló que “la Comisión enriquecerá este trabajo y permitirá tener protocolos y certificaciones. El mundo nos está demandando alimentos sanos, la mesa de cada uno de los habitantes nos interpela y, sin dudas, aquí tenemos un atributo natural y podemos contribuir a la alimentación de cientos de miles de argentinos y también al cuidado del medio ambiente, protegiendo a nuestra flora y fauna”.

**Presentes**

Durante la actividad estuvieron presentes más de 40 integrantes del Equipo Técnico Interdisciplinario de los Bajos Submeridionales, integrado por la Confederación de Asociaciones Rurales de la Provincia de Santa Fe (CARSFE); la Federación Agraria Argentina de las jurisdicciones de Tostado y Vera; la Unión de Organizaciones de Pequeños Productores de la Cuña Boscosa y los Bajos Submeridionales de Santa Fe (UOCB); el Instituto Nacional de Tecnología Agropecuaria (INTA); INTA Reconquista; Fundación para el Desarrollo en Justicia y Paz (Fundapaz); Instituto Nacional del Agua (INA); la Secretaría de Agricultura Familiar, Campesina e Indígena de la Nación (Safci), y la Universidad Nacional del Litoral, entre otros.

Además, estuvieron presentes el Biólogo Alejandro Giraudo, del Instituto Nacional de Limnología (INALI), el Ing. José Pensiero de la Facultad de Ciencias Agrarias de la Universidad Nacional del Litoral, y el Ing. Luis Horacio Luisoni del Instituto Nacional de Tecnología Agropecuaria, especialistas en la temática, quienes fueron invitados para sumar sus experiencias a la iniciativa.

Desde el gobierno provincial expusieron por el Ministerio de Ambiente y Cambio Climático la Directora Provincial de Desarrollo Ecosistémico, Arq. Josefina Obeid y el asesor técnico, Ing. Alejandro Montenegro. Además, estuvieron presentes el subsecretario de Desarrollo Ecosistémico, Roque Chávez, y el asesor técnico, Biólogo Blas Fandiño. Desde el Ministerio de Infraestructura, Servicios Públicos y Hábitat, participó el Director Provincial de Gestión Territorial, Ing. Nicolás Fernández, y por la Administración Provincial de Vialidad, Oscar Seschi e Ing. Daniel Riccotti.