---
category: La Ciudad
date: 2020-12-02T17:47:52Z
thumbnail: https://assets.3dnoticias.com.ar/PREDIO-ATE.png
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Prensa UNL'
resumen: 'Predio UNL-ATE: abren las inscripciones a la colonia de vacaciones'
title: 'Predio UNL-ATE: abren las inscripciones a la colonia de vacaciones'
entradilla: El plazo para anotarse va del 3 al 8 de diciembre. Dirigida a niños y
  niñas de 4 a 13 años, la colonia comenzará el 14 de este mes. Se extremarán medidas
  sanitarias basadas en el Protocolo de Bioseguridad de la UNL.

---
Respetando rigurosamente las medidas sanitarias establecidas en el Protocolo de Bioseguridad de la Universidad Nacional del Litoral, el lunes 14 de diciembre comenzará a desarrollarse la nueva temporada de la colonia de vacaciones en el Predio UNL-ATE. Dirigida a niños y niñas de entre 4 y 13 años de edad, la iniciativa se extenderá hasta el 19 de febrero en las instalaciones del espacio recreativo y deportivo ubicado en Alicia Moreau de Justo S/N, Costanera Este de Santa Fe.

Las personas interesadas en que sus hijos e hijas participen de la colonia deben completar el formulario que está alojado en la página web del Predio UNL-ATE -www.prediounl-ate.org.ar-. La instancia de preinscripción se extiende del jueves 3 al martes 8 de diciembre. Una vez enviada la solicitud, desde la institución se pondrán en contacto para coordinar la instancia de inscripción final en la que es necesario presentar la documentación indicada en el sitio web mencionado (solicitud de inscripción; ficha médica; fotocopia DNI del niño o niña).

## **Colonia de vacaciones**

La colonia de vacaciones tendrá una duración de 10 semanas que comenzarán a partir del lunes 14 de diciembre y culminarán el viernes 19 de febrero. El servicio funcionará de lunes a viernes (excepto feriados) de 8 a 12 -en la primera media hora se realizará un ingreso ordenado y la última media hora el egreso-. Sus beneficiarios directos serán los socios e hijos de asociados al Predio UNL-ATE que cuenten con una edad comprendida entre los 4 a 13 años.

### **+ Info:**

Para ampliar esta información es necesario comunicarse con el Predio UNL-ATE ingresando a la web www.prediounl-ate.org.ar; llamando al teléfono (0342) 4571149; o escribiendo un correo electrónico a informes@prediounl-ate.org.ar