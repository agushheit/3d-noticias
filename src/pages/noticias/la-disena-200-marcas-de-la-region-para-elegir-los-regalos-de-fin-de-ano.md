---
category: La Ciudad
date: 2021-12-16T06:15:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/DISEÑA21.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'La Diseña: 200 marcas de la región para elegir los regalos de fin de año'
title: 'La Diseña: 200 marcas de la región para elegir los regalos de fin de año'
entradilla: 'Este miércoles comenzó la edición navideña de la feria en la Estación
  Belgrano (Bv. Gálvez 1150). Se extenderá hasta el 22 de diciembre, siempre de 17
  a 23 horas, con entrada libre y gratuita. '

---
Con la llegada de las fiestas de fin de año, la capital santafesina multiplica sus opciones de compra en los paseos comerciales y ferias. En una conferencia de prensa que tuvo lugar esta mañana, mientras se ultimaban los detalles para la apertura de La Diseña, el secretario de Educación y Cultura de la Municipalidad, Paulo Ricci, brindó detalles de esta edición navideña que comienza hoy a las 17 horas en la Estación Belgrano, y que continuará en ese mismo espacio hasta el 22 de diciembre.

Teniendo en cuenta que el año pasado el encuentro se pudo desarrollar con un protocolo que implicaba la reducción de la cantidad y la circulación del público, entre otros cuidados que se tuvieron por la pandemia, el secretario valoró que esta edición se realiza sin límite en cuanto al aforo a partir de las últimas flexibilizaciones que se dieron en las actividades económicas, comerciales y recreativas considerando el bajo riesgo sanitario que presenta actualmente la región. Explicó además que el montaje de la feria tampoco está condicionado como ocurrió el año pasado, por lo que se libera el tiempo de permanencia y la circulación de las personas, y se recupera un formato similar y muy cercano a lo que fue históricamente La Diseña.

“La única modificación que quedó desde el año pasado es que el patio gastronómico se arma nuevamente al aire libre, en el estacionamiento sobre calle Avellaneda, para tener más espacios de circulación y distanciamiento en los andenes de la Estación Belgrano”, dijo en diálogo con los medios. Y detalló como otra buena noticia que “de las 200 marcas que participan hay un 40% de diseñadores y diseñadoras que se suman por primera vez, que fueron parte de la convocatoria 2021-2022 que se inició en mayo de este año, reuniendo primero a la Mesa del Diseño como indica la ordenanza que tenemos desde 2019”.

**Crecimiento y renovación de las propuestas**

La oferta de esta edición abarca los rubros accesorios y complementos, productos de almacén natural, comunicación en múltiples soportes, mobiliarios y objetos, indumentaria y textil. Como es habitual, se suma el Taller de La Guardia con un stand de venta y muestra de sus producciones realizadas en cerámica artesanal. En este sentido, Ricci señaló que en esta convocatoria hubo un crecimiento de marcas de objetos y mobiliarios, especialmente de juegos para las infancias, y de los productos de almacén natural. En el sector gastronómico también se observa una diversidad mayor, con más de 20 emprendimientos de comidas, cervezas y dulces.

“La Diseña es un espacio para recorrer, para pasarla bien, comer algo y volver a recorrer para buscar un regalo que falta. También es una oportunidad de trabajo muy importante para emprendedoras y emprendedores de la ciudad de Santa Fe. Como dijimos apenas asumimos en 2019, es un evento al que le vamos a dar continuidad y sumar novedades. Sostuvimos durante todo 2020 y 2021 otros espacios para el diseño, como el Mercado Progreso que se está consolidando los domingos como un espacio para las ferias, pero este es el gran evento de la ciudad de Santa Fe al que no podemos dejar de venir para buscar obsequios y para apoyar a nuestras diseñadoras y diseñadores. La Diseña se sigue sosteniendo, sigue creciendo, con una oferta muy interesante y renovada, y con muchas novedades que seguramente los van a sorprender”, concluyó Paulo Ricci.

**Proceso de selección y fortalecimiento**

La convocatoria 2021-2022 se dio en el marco de la Ordenanza N° 12.656/19. Como se informó oportunamente se presentaron 398 marcas de las cuales 240 participaron de las instancias obligatorias de clínicas formativas para la mejora de las producciones. De ellas, 40 no cumplieron con los requisitos de admisibilidad.

Las clínicas contaron con profesionales y especialistas del sector, que estuvieron a cargo de esas instancias entre septiembre y octubre, en modalidad virtual. Los contenidos teórico-conceptuales se estructuraron en torno a ejes que profundizaron sobre la innovación y el diseño, el empleo de materiales locales, la sustentabilidad, la proyección en el territorio, la evaluación sobre el público al que está destinado cada producto; y la relación con nuestra historia, cultura y sus vínculos en el presente. El registro audiovisual de esas clínicas es de acceso público, por lo que se encuentran disponibles en Capital Cultural, la plataforma de contenidos de la Municipalidad de Santa Fe.

Después de ese proceso, el Jurado -que integraron Jéssica Roude, Judith Savino y Alejandro Sarmiento- eligió las 200 marcas que participarán en las ferias que se desarrollen hasta noviembre de 2022.

Para dar continuidad al acompañamiento que tuvieron las marcas durante el proceso de selección y clínicas, se contará durante la feria con la presencia del Jurado y con la Mesa de Diseño Santafesino, que se conformó en el mes de octubre con la participación de integrantes de las secretarías de Educación y Cultura y de Producción y Desarrollo Económico de la Municipalidad; representantes de los distintos rubros del sector; de la Facultad de Arquitectura, Diseño y Urbanismo de la Universidad Nacional del Litoral y concejales. En ese ámbito y con representación de la Cámara de Diseño, el municipio articuló las estrategias necesarias para realizar esta nueva edición de La Diseña.