---
category: Agenda Ciudadana
date: 2022-01-12T06:15:14-03:00
thumbnail: https://assets.3dnoticias.com.ar/NOAISLARSE.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: No deberán aislarse los contactos estrechos que estén vacunados con dosis
  de refuerzo y no tengan síntomas
title: No deberán aislarse los contactos estrechos que estén vacunados con dosis de
  refuerzo y no tengan síntomas
entradilla: El Gobierno nacional modificó el aislamiento obligatorio para contactos
  estrechos de casos positivos, por lo que ahora ya no se deberán aislar los que tengan
  el esquema completo de vacunación.

---
El Gobierno nacional modificó el aislamiento obligatorio para contactos estrechos de casos positivos, por lo que ahora ya no se deberán aislar las personas que tengan el esquema completo de vacunación con la dosis de refuerzo y no presenten síntomas.  
  
Así lo comunicó la ministra de Salud de la Nación, Carla Vizzotti, tras una reunión del Consejo Federal de Salud (Cofesa), en la cual se acordaron las nuevas pautas para aquellas personas que sean contacto estrecho de casos positivos de Covid.   
  
A pocos días de que el Gobierno redujera el tiempo de aislamiento para aquellas personas que fueron contacto estrecho de un caso positivo, ya que pasó de 10 a 5, ahora se determinó que quienes cuenten con el esquema completo de vacunación y hayan recibido el refuerzo cuatro meses atrás o menos, no estarán obligados a quedarse aislados.  
  
Sin embargo, la ministra remarcó que los gobiernos provinciales decidirán lo que hacer al respecto, ya que desde Nación hacen la recomendación.  
En declaraciones radiales, la ministra sostuvo: "En términos de aislamiento, estamos viendo que hay un impacto importantísimo en el sistema de salud y en servicios esenciales. Lo que acordamos en relación a eso son tres escenarios diferentes".  
  
Además, la funcionaria advirtió que la posibilidad de evitar el aislamiento implica un alto grado de responsabilidad social: "La recomendación es que todas las personas que son contactos estrechos asintomáticos minimicen las actividades sociales, que es donde la evidencia demuestra que se producen la mayor cantidad de los contagios".