---
category: La Ciudad
date: 2021-04-28T08:09:52-03:00
thumbnail: https://assets.3dnoticias.com.ar/concejo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Concejo Municipal Santa Fe
resumen: La agenda del Concejo Municipal para recordar el 18º aniversario de la inundación
title: La agenda del Concejo Municipal para recordar el 18º aniversario de la inundación
entradilla: Con una muestra fotográfica, un conversatorio virtual y un reconocimiento
  a personas destacadas por su acción solidaria durante la tragedia, el Concejo Municipal
  recuerda los 18 años de la inundación de 2003.

---
En un nuevo aniversario de la inundación, el Concejo Municipal organiza una serie de actividades conmemorativas al cumplirse 18 años de la tragedia hídrica que afectó de manera directa a más de 130 mil vecinos y vecinas del oeste de la ciudad en 2003.

La agenda del Concejo prevé la exhibición de una muestra fotográfica de tres fotoperiodistas santafesinos que registraron el ingreso del Río Salado y la magnitud de las consecuencias materiales en los barrios del cordón oeste; un conversatorio virtual con los fotógrafos responsables de la muestra que también estará disponible de manera online; y un reconocimiento a vecinos y vecinas que tuvieron un rol solidario destacado en las acciones de rescate, evacuación y contención de damnificados.

El presidente de la institución, Leandro González, explicó que el objetivo es reflejar el fuerte compromiso y recuerdo que desde hace años la ciudad de Santa Fe tiene cada 29 de abril. "Por eso lo hacemos cada aniversario desde hace años, por lo que significó y aún significa para los santafesinos, por el dolor para las víctimas directas e indirectas de la trágica inundación, y por sus familias".

"Desde el lugar que nos toca -continuó- tenemos la obligación ética y moral de generar este tipo de actividades para recordar la solidaridad de cientos de vecinos y vecinas que tendieron una mano, pusieron un bote o una lancha a disposición, movieron una bolsa de arena, ayudaron a una persona a nadar o directamente la cargaron en andas. Pero todo esto sin olvidarnos que la única forma genuina de intentar cicatrizar las heridas de miles de personas y familias sólo puede lograrse a través de la memoria y la justicia. Siempre".

**Muestra fotográfica y conversatorio virtual**

Los fotógrafos santafesinos José Almeida, Hugo Pascucci y Eduardo Seval, fueron algunos de los periodistas que retrataron y registraron escenas de las primeras horas en las que el agua del Río Salado comenzaba a ingresar en distintos barrios del cordón oeste, durante el lunes 28 y el martes 29 de abril de 2003.

Bajo el título Rescatar(nos). Muestra fotográfica de un instinto colectivo, los tres fotoperiodistas, junto al Concejo Municipal, exhibirán 36 fotografías que reflejan el dramatismo, la desesperación y el dolor, pero también la solidaridad con la que los vecinos y vecinas de la ciudad emprendieron las tareas de asistencia, evacuación y contención en el medio de la tragedia hídrica.

Como describe la presentación de la muestra, el trabajo periodístico de gran valor histórico y cada una de las piezas fotográficas que componen la exposición, no sólo conmemoran uno de los acontecimientos más importantes de la historia reciente de nuestra ciudad sino que, además, “buscan salvar del agua un instinto colectivo: el de la solidaridad”.

Si bien Rescatar(nos) estará exhibida en el hall principal del Concejo Municipal desde el martes 27 de abril, por razones sanitarias la institución dispuso una galería virtual en el sitio web oficial del organismo para conocer cada una de las fotografías que componen esta muestra testimonial.

Además, el jueves por la tarde, desde las 18 hs., se realizará un conversatorio online denominado Fotoperiodismo y memoria colectiva. A 18 años de la inundación del 29 de abril, en el que los tres fotógrafos expositores de la muestra compartirán su experiencia profesional y brindarán un testimonio como protagonistas directos de la catástrofe hídrica.

Esta actividad virtual estará moderada por Leandro González y será abierta a toda la comunidad.

**Vecino solidario**

En 2018 se sancionó una ordenanza mediante la cual se establece al 29 de abril como el día del “Vecino solidario santafesino”, para reconocer a quienes ayudaron de manera desinteresada y desde distintos ámbitos a las personas afectadas por la inundación.

Es por eso que el día jueves 29 por la mañana se realizarán reconocimientos a vecinos y vecinas que tendieron una mano y trabajaron por los demás en aquellos días.

**Fotógrafos**

José Almeida comenzó su carrera periodística colaborando con la sección deportes del diario La Provincia en 1996. Desde 2001 y durante la tragedia, trabajó para el Diario Clarín, Olé, La Razón, y las revistas Viva, Pymes, Elle, entre otros. Colabora con medios nacionales e internacionales.

Hugo Pascucci es reportero gráfico y docente, trabajó para importantes medios locales y nacionales como La Capital de Rosario, Hoy en la Noticia de La Plata, Rosario 12 y Página 12 y fue corresponsal para los diarios Clarín y Olé.

Eduardo Seval es reportero gráfico, realizó colaboraciones para diarios nacionales e internacionales y agencias como EFE, Reuters, Infosic y Photo Sport Argentina. Fue Jefe de Fotografía de Diario UNO Entre Ríos y de La Razón (sucursal Santa Fe), y desde 1998 es corresponsal del Diario Popular.