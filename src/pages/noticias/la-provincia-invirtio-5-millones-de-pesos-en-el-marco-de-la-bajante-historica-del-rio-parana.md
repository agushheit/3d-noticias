---
category: Agenda Ciudadana
date: 2020-12-06T11:46:59Z
thumbnail: https://assets.3dnoticias.com.ar/buceo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: La provincia invirtió 5 millones de pesos en el marco de la bajante histórica
  del río Paraná
title: La provincia invirtió 5 millones de pesos en el marco de la bajante histórica
  del río Paraná
entradilla: Aguas Santafesinas incorporó un equipo de buceo de última generación para
  el mantenimiento de sus tomas de agua.

---
Aguas Santafesinas adquirió un equipo de buceo de última generación, destinado al mantenimiento de sus tomas de agua, en el marco de la bajante histórica del río Paraná que demanda tareas permanentes para sostener la operatividad de dichos sistemas.

Si bien hoy la demanda de trabajo está focalizada en las tomas de las plantas, el equipamiento se usará en todas las tareas de mantenimiento subacuático que demanda ASSA, incluyendo cisternas, decantadores, tanques e intervenciones en la vía pública.

Los dos buzos habilitados realizan tareas de mantenimiento y reparación bajo agua en las quince ciudades y en los seis sistemas de acueductos que forman parte del área de servicio de ASSA.

Ahora están abocados al mantenimiento de las tomas de agua de las plantas potabilizadoras ubicadas sobre el curso del río Paraná o sus afluentes, afectadas por la histórica bajante que provoca obstrucciones en las rejas por donde ingresa el agua, por la presencia de vegetales, principalmente camalotes.

Diariamente los buzos retiran troncos, camalotes y otros residuos que navegan cerca de las tomas y pueden obstruirlas.

También realizan tareas de limpieza directa sobre las rejas de retención sumergidas de las tomas, para lo que es necesario detener el bombeo de captación de agua.

Gracias a una inversión de 5 millones de pesos del gobierno provincial, ahora Aguas incorporó el más moderno equipo existente en el mercado internacional, a los fines de optimizar y tornar más seguro el trabajo de sus buzos.

Se trata de un casco Kirby Morgan -empresa californiana con más de 50 años de trayectoria- modelo KM97 con su correspondiente panel de control y todas las piezas accesorias de inmersión. El casco de acero inoxidable es el más moderno del mercado y se ajusta a las máximas normas internacionales de seguridad.

La adquisición incluye, además del casco, tubos de oxígeno auxiliares, consola para la intercomunicación entre buzos con monitoreo de consumo de aire y profundidad, botas especiales y conducto umbilical normalizado.