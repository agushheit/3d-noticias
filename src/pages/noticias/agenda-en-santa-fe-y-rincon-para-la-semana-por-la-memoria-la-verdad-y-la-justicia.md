---
category: Agenda Ciudadana
date: 2021-03-20T13:00:52-03:00
thumbnail: https://assets.3dnoticias.com.ar/24_marzo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: " Agenda en Santa Fe y Rincón para la Semana por la Memoria, la Verdad y
  la Justicia"
title: Agenda en Santa Fe y Rincón para la Semana por la Memoria, la Verdad y la Justicia
entradilla: Para rescatar la memoria a través de la cultura y promover a la libertad
  de expresión como derecho humano indispensable para la construcción cultural de
  una sociedad.

---
El Ministerio de Cultura de la Provincia de Santa Fe organizó las siguientes actividades para agendar:

* **Domingo 21 a las 20:00 Hs.**

**La Casa de los Gobernadores (Bv. Gálvez 1202).**

Canciones prohibidas por la dictadura. Cintia Bertolino, Candela Fernández e Irene Marchi.

Gratis. Ingreso por orden de llegada.

* **Lunes 22 de 16:00 a 18:00 Hs.**

**El Alero Coronel Dorrego (Av. French 1758).**

 Intervención teatral: “Juegos de Niños”, de Modus Vivendi.

 Instalaciones: “Yo soy” y “Bordar memoria”.

Proyección de “Historias que abrazan”, basadas en el libro “Ovillo de trazos”.

Pizarrón abierto.

* **Jueves 25 a  las 17.30 Hs.**

**El Alero Acería (Cafferata y Roca).**

Instalación de libros prohibidos por la dictadura

“La caja encantada”. Narra cuentos Joselina Martínez

Proyección de biografías de las y los autores.

Inauguración del espacio de lectura “Manuelita” para niños y niñas.

* **Viernes 26 a las 19:00 Hs.**

**El Molino, Fábrica Cultural (Bulevar y Pedro Vittori, Santa Fe).**

Lectura de cuentos prohibidos

* **Sábado 27 a las 16:00 Hs.**

**Espacio cultural de Yapeyú (Misiones 6658).**

Festival del teatro y la memoria.

Presenta y coordina: Juan Pablo Quinteros

* **Sábado 27 a  las 19:30 Hs.**

**La Redonda, Arte y Vida Cotidiana (Salvador del Carril y Belgrano).**

Música y teatro.

Gratis, cupo limitado. Reservas: [www.eventbrite.com.ar/e/147120420137](http://www.eventbrite.com.ar/e/147120420137?fbclid=IwAR2kCy7pQXz8gcubHq4ruFVSZJPt6M-Z7a_8Ye2SNV_QHDzfnFLBC0RxZhU)

* **Domingo 28 de 16:00 a 19:30 Hs.**

**El Alero Las Flores (Azopardo y Millán Medina).**

Canciones prohibidas interpretadas por Dante Langhi, Agus Bar y Nacho Zablocki.

Artesanos de la Feria Florecer,

Relatos orales sobre el 24 de Marzo.

Proyecciones.

* **Domingo 28 a las 16:00 Hs.**

**Sapukay - La Casa de Fernando Birri (Ubajay 1512, San José del Rincón).**

Lectura de cuentos infantiles prohibidos

Instalación “Memoria, Verdad y Justicia” realizada junto a residentes.

**Más información sobre programación, entradas y protocolos, en las RRSS de cada espacio.**