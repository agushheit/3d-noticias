---
category: El Campo
date: 2022-01-25T06:15:09-03:00
thumbnail: https://assets.3dnoticias.com.ar/exportaciones.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Las exportaciones de Santa Fe crecieron más de un 60%: ¿qué produce y a
  quién le vende la provincia?'
title: 'Las exportaciones de Santa Fe crecieron más de un 60%: ¿qué produce y a quién
  le vende la provincia?'
entradilla: 'La suba registrada fue entre enero y noviembre del 2021, comparado con
  igual período del 2020, condicionado por la pandemia. Cinco países reciben el 40
  por ciento del total de bienes producidos por Santa Fe.

  '

---
En el período enero - noviembre de 2021 las exportaciones con origen en la provincia de Santa Fe alcanzaron un valor de U$S 16.597,5 millones, lo que representa un aumento del 63,2% respecto del mismo período del 2020, año en el cual la economía mundial se vio fuertemente condicionada por la pandemia. Así lo revela un nuevo informe del Instituto Provincial de Estadísticas y Censos de Santa Fe (Ipec).

En materia de volúmenes exportados, medidos en toneladas, presentaron una suba de 20,8%; mientras que los precios, expresados en dólares por tonelada, subieron 35,1% en relación a enero – noviembre de 2020.

El monto de las exportaciones de Productos Primarios en el período analizado fue de U$S 2.183,8 millones, 37,9% más que en el mismo período de 2020. Las cantidades medidas en toneladas aumentaron 2,5%, acompañado por un aumento en los precios de 34,6%.

En el acumulado de 2021, las ventas externas de Manufacturas de Origen Agropecuario totalizaron U$S 12.095,7 millones, y mostraron un aumento de 62% en relación al mismo período del año anterior. En términos de toneladas, las exportaciones aumentaron 26,8%, y los precios medios aumentaron 27,8%.

Se registraron exportaciones de Manufacturas de Origen Industrial por un monto de U$S 2.232,3 millones, 108,2% más que en igual período del año 2020. Los volúmenes exportados registraron un aumento de 68,9%, acompañado por una variación interanual de precios positiva de 23,2%.

El valor exportado de Combustibles y Energía fue de U$S 85,7 millones, reflejando una variación interanual positiva de 72,7%. Las cantidades exportadas aumentaron 12,9% y los precios aumentaron 53%.

Principales compradores:

India fue el principal destino de exportación con una participación sobre el total de las ventas externas de 10,7%. Los valores exportados aumentaron 70,9% respecto del mismo período de 2020. Los envíos se componen principalmente por aceite de soja en bruto, incluso desgomado (Grasas y aceites); tortas, harinas y “pellets” de la extracción del aceite de soja (Residuos y desperdicios de la industria alimenticia); y cueros y pieles curtidos de bovino, depilados, secos.

China se situó en el segundo lugar, concentrando el 8% de los envíos. Las exportaciones a este destino aumentaron 34% en términos interanuales. Las principales exportaciones a China fueron carne bovina, deshuesada, congelada (Carnes); porotos de soja, excluido para siembra (Semillas y frutos oleaginosos); y aceite de soja en bruto, incluso desgomado (Grasas y aceites).

En tercer lugar, se ubicó Países Bajos, con una participación del 7,3% del total. Las ventas a este país aumentaron 167% respecto del mismo período de 2020. Las principales exportaciones a Países Bajos fueron biodiesel y sus mezclas, sin aceites de petróleo o de mineral bituminoso o con un contenido inferior al 70% en peso de estos aceites (Productos químicos y conexos); seguido por carne bovina, deshuesada, fresca o refrigerada (Carnes); y harina y “pellets” de la extracción del aceite de soja (Residuos y desperdicios de la industria alimenticia).

Los primeros cinco destinos de exportación se completan con Vietnam, que registró un aumento de 33,9%, y Brasil, cuya variación positiva fue de 43,4% en términos interanuales.

Las exportaciones a Vietnam estuvieron compuestas principalmente por harina y “pellets” de la extracción del aceite de soja (Residuos y desperdicios de la industria alimenticia); maíz en grano (Cereales); y lecitinas y otros fosfoaminolípidos (Productos químicos y conexos).

Los principales productos enviados a Brasil estuvieron compuestos principalmente por trigo y morcajo, excepto para siembra, y maíz en grano (Cereales); seguido por reductores, multiplicadores y variadores de velocidad (Máquinas y aparatos, material eléctrico); y amortiguadores de suspensión de vehículos automóviles (Material de transporte terrestre).

Del sexto al décimo puesto lo componen Indonesia, España, Chile, Malasia y Perú.