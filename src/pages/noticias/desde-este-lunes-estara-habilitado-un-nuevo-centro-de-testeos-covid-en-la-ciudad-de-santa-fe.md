---
category: Estado Real
date: 2021-04-26T06:15:20-03:00
thumbnail: https://assets.3dnoticias.com.ar/TESTEOS.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Desde este lunes estará habilitado un nuevo centro de testeos covid en la
  ciudad de Santa Fe
title: Desde este lunes estará habilitado un nuevo centro de testeos covid en la ciudad
  de Santa Fe
entradilla: Funcionará en el viejo Hospital Iturraspe y permitirá realizar 200 análisis
  diarios. Se deberá asistir con turno previo otorgado por el 0800 555 6549.

---
La provincia de Santa Fe, a través del Ministerio de Salud, dispondrá desde este lunes 26 de abril de un nuevo centro de testeos en el viejo Hospital Iturraspe en la ciudad de Santa Fe, con el objetivo de incrementar los controles en el marco de la segunda ola de contagios por Covid 19.

Al nuevo dispositivo, que permitirá realizar los test de antígenos y, en caso que sea indicado el PCR, se podrá acceder mediante un turno otorgado por el 0800 555 6549.

En ese sentido, el director de la región Santa Fe de Salud, Rodolfo Rosselli detalló: “Este nuevo centro de testeo tendrá una modalidad similar a la que funciona en otros espacios: las personas ingresan con su vehículo mediante el portón que da a la calle Presidente Perón donde está ubicado el dispositivo, que tendrá dos personas a cargo de hisopar”. Con el fin de evitar inconvenientes se prevé que el test se realice dentro del vehículo y, luego se podrá salir por calle Obispo Gelabert.

Asimismo, Roselli estimó que “se realizarán más de 200 testeos en un horario estipulado entre las 8 y las 12 hs. Sin embargo, se tendrá en cuenta el contexto sanitario y se irá ampliando la franja de atención junto a la cantidad de análisis”.

Cabe señalar que estas medidas se dan ante el aumento de consultas recibidas por el 0800 de la provincia y por el incremento de casos. Es por ello que se podrían llegar a duplicar la cantidad de testeos a realizar diariamente. “En estos momentos se realizan cerca de 250 testeos diarios en CEMAFE; en Sayago y en el Mira y López el promedio es de 50 testeos por día”, concluyó el funcionario provincial.

El nuevo dispositivo se suma a los que actualmente funcionan ubicados en el hospital Sayago, en el Hospital Mira y López y en el CEMAFE.