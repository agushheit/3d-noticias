---
category: Agenda Ciudadana
date: 2021-06-22T08:35:45-03:00
thumbnail: https://assets.3dnoticias.com.ar/comercios.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Centro Comercial/Diario Uno
resumen: 'Día del Padre: los comerciantes esperaban más ventas'
title: 'Día del Padre: los comerciantes esperaban más ventas'
entradilla: Además, la encuesta del Centro Comercial indica que el 40 por ciento de
  los comerciantes vendió menos que el año pasado

---
La celebración del Día del Padre permitió a la mayoría de los comercios tener un leve repunte en las ventas, pero estuvo lejos de lo esperado en un momento marcado por la crisis sanitaria y económica. Según la encuesta que realizó el Centro Comercial de Santa Fe, el 40 por ciento de los comerciantes vendió menos que el año pasado para la misma celebración. Mientras que el 30 por ciento dijo vender lo mismo que en 2020 y el otro 30 por ciento aseguró que se incrementó respecto a un año atrás.

![](https://media.unosantafe.com.ar/p/fd8c19d7d7728d7a54a3654251d95821/adjuntos/204/imagenes/030/166/0030166301/whatsapp-image-2021-06-21-at-104011jpeg.jpeg?0000-00-00-00-00-00)

Cuando la entidad que nuclea a gran parte del comercio de la ciudad preguntó por las expectativas de venta, la mitad de los comerciantes dijo que fueron peor o mucho peor de lo esperado, mientras que el 30 por ciento dijo que fue igual y el 20 por ciento dijo que fue mejor de lo que se esperaba.

![](https://media.unosantafe.com.ar/p/146d8f5d53f8f45c69133215d63501c7/adjuntos/204/imagenes/030/166/0030166269/whatsapp-image-2021-06-21-at-104010-1jpeg.jpeg?0000-00-00-00-00-00)

A pesar de que la celebración del Día del Padre no cumplió con las expectativas de la mayoría, el 60 por ciento de los comerciantes aseguró que estos festejos ayudan a reactivar las ventas del mes, algo que se fomenta con promociones especiales.

![](https://media.unosantafe.com.ar/p/949a3861d714be5dc0891ecf83c4cda9/adjuntos/204/imagenes/030/166/0030166285/whatsapp-image-2021-06-21-at-104010-2jpeg.jpeg?0000-00-00-00-00-00)

Uno de los datos que se relevó en la encuesta fue la utilización de la Billetera Santa Fe. el 60 por ciento de los comercios encuestados adhirieron a ese sistema de pago y el 40 por ciento de los locales que lo tienen aseguraron que la mitad de sus ventas se hizo con la herramienta virtual de pago.

![](https://media.unosantafe.com.ar/p/9b02b42636f302155920c633afb1c232/adjuntos/204/imagenes/030/166/0030166253/whatsapp-image-2021-06-21-at-104010jpeg.jpeg?0000-00-00-00-00-00)

Por último, el Centro Comercial preguntó por el tique promedio de venta que este año fue de 2.900 pesos, lo que significa un crecimiento del 26,1 por ciento respecto de los 2.300 pesos del año pasado, muy por debajo de la inflación. Pero, además, también estuvo muy lejos del crecimiento que venía registrando año tras año. En 2019 había tenido un crecimiento del 43,7 por ciento respecto al año anterior y, en 2020 había crecido un 100 por ciento respecto a 2019.