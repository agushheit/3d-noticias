---
layout: Noticia con imagen
author: .
resumen: Capacitaciones gratuitas
category: Estado Real
title: Con una amplia convocatoria continúan las capacitaciones laborales
  gratuitas de la Municipalidad
entradilla: Son más de 900 los santafesinos y santafesinas que actualmente
  participan de los cursos de formación laboral que brinda el municipio
  santafesino. Son gratuitos y abiertos a toda la comunidad.
date: 2020-11-19T12:40:01.282Z
thumbnail: https://assets.3dnoticias.com.ar/capacitaciones-muni.jpg
---
Los cursos de formación laboral que brinda el municipio santafesino, en conjunto con diferentes instituciones, son gratuitos, abiertos a toda la comunidad y para distintos mercados laborales.

Los cursos de empleo forman parte de la política en capacitación laboral que impulsa la Municipalidad de Santa Fe, con el objetivo de adquirir y actualizar los conocimientos y habilidades de las personas para mejorar sus perfiles laborales y generar más oportunidades en la búsqueda de empleo. **Hasta la fecha se desarrollan 25 cursos y participan más de 900 santafesinos.**

Las propuestas se iniciaron en septiembre y la respuesta por parte de los interesados fue inmediata. En primer lugar, el municipio junto a la Red de Escuelas de Oficios Digitales Potrero Digital, desarrolló los **cursos de programación web y comercio electrónico**, con una adhesión de 138 personas. Potrero Digital pertenece a la Fundación Compromiso y fue fundada por la productora “MundoLoco” del cineasta Juan José Campanella en 2018. Su misión es identificar y potenciar oportunidades de innovación social en tres ejes centrales: Desarrollo Social y Económico, Medio Ambiente y Cultura. Potrero Digital cuenta con el acompañamiento de importantes empresas del sector TICs como MercadoLibre, Digital House, Cisco y Google.

Por su parte, **otras 240 personas se encuentran participando de los cursos de Entrenador deportivo**, Herramientas para el teletrabajo, “Community Manager” (administrador de redes sociales digitales), y “Nail Art” (manicuría), que comenzaron en octubre para santafesinos mayores de 16 años. Los dos últimos fueron financiados por el Ministerio de Trabajo, Empleo y Seguridad Social de Santa Fe, por lo que cuentan con doble certificación.

El director de Empleo y Trabajo Decente, Guillermo Cherner, afirmó que “estamos ante un nuevo escenario en materia de empleo, que se aceleró ante las necesidades impuestas por la situación sanitaria, pero que venía profundizándose en los últimos años”. Este nuevo contexto “requiere que los gobiernos locales estemos cada vez más atentos a la dinámica del mercado de trabajo y a las oportunidades de inclusión laboral, generando herramientas que permitan prepararse mejor a quienes están en búsqueda de empleo, para las transformaciones en el mundo del trabajo y las oportunidades existentes en la ciudad de Santa Fe”.

Asimismo, se desarrolló una propuesta formativa para el trabajo denominada “**ProgramON**” junto a la Fundación Chicos.Net y Coca Cola Argentina, de la cual participaron 83 jóvenes de entre 17 y 24 años. La misma consistió en un **curso de alfabetización digital** de seis semanas para fortalecer sus competencias y habilidades para encontrar un empleo.

En la misma línea, se realizó un **taller de búsqueda de empleo**, que contó con una asistencia de 110 participantes, y otro de fortalecimiento del perfil laboral en Linkedin con 80 personas. Mientras que otros 35 santafesinos asistieron a un **curso de orientación e inclusión laboral**.

## **Empleo verde**

Además, se realizaron dos capacitaciones en empleos verdes, que buscaron promover y difundir la creación de puestos de trabajo decente, que contribuyan a preservar el medioambiente. Las mismas fueron realizadas en conjunto con la Organización Internacional del Trabajo (OIT), en temas de Bioenergía y Economía Circular, y contaron con una participación de 180 personas.

Por otro lado, en conjunto con el Municipio de Rosario, se realizaron **cursos de Teletrabajo, Oratoria y Resolución de Conflictos Laborales**, en los que se formaron 30 santafesinos.

Al ser Santa Fe miembro de de la Red Federal de Servicios de Empleo de Argentina, unas 43 personas se encuentran capacitándose en cursos promovidos por el Ministerio de Trabajo, Empleo y Seguridad Social de la Nación. Estos se realizan en coordinación con entidades gremiales, que tienen por objetivo brindar formación profesional a trabajadores desocupados, desarrollando nuevas competencias profesionales o recalificando las existentes.

En este momento se llevan adelante **cursos de “cajero de tienda”** y **“cajero de supermercados”**, que dicta la Federación Argentina de Empleados de Comercio. También **capacitaciones en “Estrategias de comunicación cultural”, “Producción de eventos culturales” y “Vestuario escénico”**, promovidas por la Escuela de Oficios Artísticos de Santa Fe.

Junto a la Federación Argentina de Trabajadores de Edificios de Renta y Horizontal se proponen los **cursos de “Competencias digitales en el uso de redes sociales”, “Educación Financiera”, “Introducción a la impresión 3D” y “Diseño paramétrico 3D”**. Y la Federación de Asociaciones de Trabajadores de la Sanidad Argentina brinda el de **“Asistente en Cuidados Gerontológicos”**.

## **Gratuitos**

Los cursos son **gratuitos y virtuales**, por lo que es requisito tener acceso a dispositivos digitales y conexión a internet. Aquellos interesados en conocer las propuestas de capacitación para el verano, pueden enviar un correo a *[empleo@santafeciudad.gov.ar](mailto:empleo@santafeciudad.gov.ar)*.