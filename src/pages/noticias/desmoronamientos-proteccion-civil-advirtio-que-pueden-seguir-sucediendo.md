---
category: La Ciudad
date: 2021-09-11T06:15:28-03:00
thumbnail: https://assets.3dnoticias.com.ar/SECAE.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'Desmoronamientos en Sauce Viejo: Protección Civil advirtió que "pueden seguir
  sucediendo"'
title: 'Desmoronamientos en Sauce Viejo: Protección Civil advirtió que "pueden seguir
  sucediendo"'
entradilla: Según indicó el Jorge Nicolier, “el monitoreo es clave, como también lo
  son las recomendaciones de precaución a los vecinos".

---
El Jefe de Protección Civil Zona Norte, Jorge Nicollier, declaró a CyD Noticias que se está en permanente comunicación con el presidente comunal Pedro Uliambre, por los recientes desmoronamientos en la costa de Sauce Viejo. "Ya hicimos un relevamiento de esa zona, en el marco del escenario preocupante que da la bajante. Se está monitoreando; se han presentado algunos proyectos (de obras de contención), pero éstos tienen un costo económico muy importante", admitió.

En el mientras tanto, "el monitoreo es clave. Como también lo son las recomendaciones de precaución a los vecinos, puesto que estas situaciones van a seguir sucediendo por la gran bajante del río que estamos viviendo", dijo Nicollier.

Respecto del reglamento de medidas precautorias, "éste comprende todas las medidas pertinentes para que la gente no se acerque a la zona y así no corra riesgo la vida de ningún vecino. Si bien no fueron damnificadas viviendas particulares, sí fueron afectados los predios de las mismas", declaró.

"Hay que tener la mayor cautela posible, e informar inmediatamente (ante un eventual desmoronamiento) con la comuna de Sauce Viejo, que es la primera actuante en la ocasión. Desde el gobierno comunal se comunican con nosotros para coordinar qué medidas tomar. Pero claramente ante esta situación, la precaución, sobre todo: es lo más importante", subrayó el funcionario.

Tanto la comuna sauceña como el gobierno provincial están al frente de un proyecto de contención de toda esa zona, aseguró Nicolier. "Los desagües pluviales están menos firmes y esto también complica la situación -añadió-. Hay un proyecto, pero que lleva tiempo y trabajo. Son obras que no se han podido realizar antes porque el agua contenía esas barrancas. Ahora estarían (esas obras) en condiciones de realizarse, pero habrá que esperar. Provincia está avanzando con las localidades costeras en este aspecto", cerró el jefe de Protección Civil.