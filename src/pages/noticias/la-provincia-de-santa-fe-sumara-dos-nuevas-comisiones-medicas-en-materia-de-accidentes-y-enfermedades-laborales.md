---
category: Estado Real
date: 2021-07-11T06:00:12-03:00
thumbnail: https://assets.3dnoticias.com.ar/ART.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La provincia de Santa Fe sumará dos nuevas comisiones médicas en materia
  de accidentes y enfermedades laborales
title: La provincia de Santa Fe sumará dos nuevas comisiones médicas en materia de
  accidentes y enfermedades laborales
entradilla: Las aperturas se producirán durante este mes de julio en el marco de la
  adhesión a la Ley Nacional de Riesgos de Trabajo (ART).

---
La provincia, conjuntamente con el Ministerio de Trabajo y la Superintendencia de Riesgos del Trabajo de la Nación, inaugurará estas dos Comisiones Médicas Jurisdiccionales: una emplazada en la ciudad de Rafaela y la otra en Rosario. Las mismas son el resultado del proceso de adhesión de Santa Fe a la Ley Nacional de Riesgos de Trabajo, el cual se inició en 2020 impulsado por el gobernador Omar Perotti. Seguidamente en abril de este año se firmó un convenio entre los gobiernos provincial y nacional por el cual se acordó ejecutar acciones de coordinación, colaboración y puesta en marcha del sistema.

El ministro de Trabajo, Empleo y Seguridad Social de la provincia, Juan Manuel Pusineri informó que “tal como se comunicó en oportunidad de la firma del convenio hace dos meses atrás, estas comisiones se suman a las cinco ya existentes en todo el territorio provincial.” Asimismo, remarcó que “el cumplimiento de lo comprometido en ese momento es también muestra de la voluntad de Nación y Provincia de seguir avanzando para que el sistema funcione y que el trabajador cuente con una instancia ágil que facilite el acceso a las prestaciones económicas y médicas, sin renunciar a ninguno de sus derechos.