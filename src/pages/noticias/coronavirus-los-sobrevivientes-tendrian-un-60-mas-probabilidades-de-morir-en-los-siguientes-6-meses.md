---
category: Agenda Ciudadana
date: 2021-04-28T08:50:54-03:00
thumbnail: https://assets.3dnoticias.com.ar/covid-amba.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ScienceDaily
resumen: 'Coronavirus: los sobrevivientes tendrían un 60% más probabilidades de morir
  en los siguientes 6 meses'
title: 'Coronavirus: los sobrevivientes tendrían un 60% más probabilidades de morir
  en los siguientes 6 meses'
entradilla: Incluye a los asintomáticos y a los que no tuvieron complicaciones. Es
  de acuerdo al que se cree es el estudio integral más grande de Covid-19 hasta la
  fecha.

---
Superar el coronavirus es motivo de alegría y hasta de festejo. El maldito virus nos atemoriza y somete desde hace más de un año y, claro, cuando alguien logra vencerlo lo festejamos como un gol en la final del Mundial. Pero puede que, siguiendo con la analogía futbolera, el partido todavía no haya terminado.

Es que, de acuerdo a un nuevo e inmenso estudio, el que se cree que es el más grande realizado hasta ahora, muestra unos datos realmente desalentadores. Porque parece que los que logran sobrevivir al Covid-19 tendrían un 60 por ciento más probabilidades de morir en los siguientes 6 meses de complicaciones derivadas del paso del virus por el cuerpo.

Se sabe ya, casi con certeza, que muchos sobrevivientes, incluso aquellos que tuvieron casos leves, continúan padeciendo una variedad de problemas de salud mucho después de que la infección inicial debería haberse resuelto.

El nuevo estudio, cuyos resultados fueron publicados esta semana en la revista Nature, asegura que los sobrevivientes de Covid-19, que incluye a todos, los hospitalizados y hasta los asintomáticos, tienen un aumento riesgo de muerte en los seis meses siguientes al diagnóstico con el virus.

En la investigación, realizada por la Facultad de Medicina de la Universidad de Washington en St. Louis, EE.UU. los científicos catalogaron las numerosas enfermedades asociadas al paso del coronavirus, proporcionando una descripción general de las complicaciones a largo plazo y revelando la enorme carga que esta enfermedad probablemente supondrá para la población mundial en los próximos años.

El estudio es inmenso, de hecho se cree que es el más grande que se realizó hasta ahora: involucró a más de 87 mil pacientes con Covid-19 y casi 5 millones de pacientes de control de una base de datos federal.

"Nuestro estudio demuestra que hasta seis meses después del diagnóstico, el riesgo de muerte incluso después de un caso leve no es trivial y aumenta con la gravedad de la enfermedad", aseguró el autor principal, el Dr. Ziyad Al-Aly.

"No es una exageración decir que las consecuencias para la salud a largo plazo, es la próxima gran crisis de salud de Estados Unidos. Dado que más de 30 millones de estadounidenses han sido infectados con este virus, y que la carga del Covid-19 prolongado es sustancial, los efectos persistentes de esta enfermedad repercutirán durante muchos años e incluso décadas", agregó.

"Los médicos deben estar atentos al evaluar a las personas que han tenido Covid-19. Estos pacientes necesitarán atención integrada y multidisciplinaria".

**Efectos secundarios a largo plazo**

En el nuevo trabajo, los investigadores pudieron calcular la escala potencial de los problemas vislumbrados por primera vez a partir de informes médicos y estudios más pequeños que insinuaban los efectos secundarios de gran alcance de sobrevivir al coronavirus desde problemas respiratorios y ritmos cardíacos irregulares hasta salud mental. problemas y caída del cabello.

"Este estudio difiere de otros que han analizado el Covid-19 durante mucho tiempo porque, en lugar de centrarse solo en las complicaciones neurológicas o cardiovasculares, por ejemplo, adoptamos una visión amplia y utilizamos las vastas bases de datos de la Administración de Salud de los Veteranos (VHA) para catalogar todas las enfermedades que pueden serle atribuibles", dijo Al-Aly, también director del Centro de Epidemiología Clínica y jefe del Servicio de Investigación y Educación del Sistema de Atención Médica de Asuntos de Veteranos de St. Louis.

Los investigadores demostraron que, después de sobrevivir a la infección inicial, los sobrevivientes tenían un riesgo de muerte casi un 60% mayor durante los siguientes seis meses en comparación con la población general.

En la marca de los seis meses, el exceso de muertes entre todos los sobrevivientes se estimó en ocho personas por cada 1.000 pacientes. Entre los pacientes que estaban lo suficientemente enfermos como para ser hospitalizados y que sobrevivieron más allá de los primeros 30 días de la enfermedad, hubo 29 muertes por cada 1.000 pacientes durante los siguientes seis meses.

"Estas últimas muertes debidas a complicaciones a largo plazo de la infección no se registran necesariamente como muertes por Covid-19", añadió Al-Aly, y continuó: "En cuanto al número total de muertes por la pandemia, estos números sugieren que las muertes que estamos contando debido a la infección viral inmediata son solo la punta del iceberg".

Para ayudar a comprender los efectos a largo plazo más graves, los investigadores realizaron un análisis separado de 13,654 pacientes hospitalizados con Covid-19 en comparación con 13,997 pacientes hospitalizados con gripe estacional. Todos los pacientes sobrevivieron al menos 30 días después del ingreso hospitalario y el análisis incluyó seis meses de datos de seguimiento.

Así pudieron confirmar que, a pesar de ser inicialmente un virus respiratorio, el Covid-19 prolongado puede afectar a casi todos los sistemas de órganos del cuerpo.

Los investigadores identificaron problemas de salud importantes recién diagnosticados que persistieron en los pacientes con Covid-19 durante al menos seis meses y que afectaron a casi todos los órganos y sistema regulador en el organismo, que incluye:

* Sistema respiratorio: tos persistente, dificultad para respirar y niveles bajos de oxígeno en la sangre.
* Sistema nervioso: ictus, dolores de cabeza, problemas de memoria y problemas con los sentidos del gusto y el olfato.
* Salud mental: ansiedad, depresión, problemas de sueño y abuso de sustancias.
* Metabolismo: nueva aparición de diabetes, obesidad y colesterol alto.
* Sistema cardiovascular: enfermedad coronaria aguda, insuficiencia cardíaca, palpitaciones y ritmos cardíacos irregulares.
* Sistema gastrointestinal: estreñimiento, diarrea y reflujo ácido.
* Riñón: lesión renal aguda y enfermedad renal crónica que, en casos graves, puede requerir diálisis.
* Regulación de la coagulación: coágulos de sangre en piernas y pulmones.
* Piel: erupción y caída del cabello.
* Sistema musculoesquelético: dolor articular y debilidad muscular.
* Salud general: malestar, fatiga y anemia.

Si bien ningún sobreviviente sufrió todos estos problemas, muchos desarrollaron un conjunto de varios problemas que tienen un impacto significativo en la salud y la calidad de vida.

"En comparación con la gripe, Covid-19 mostró una carga de enfermedad notablemente más alta, tanto en la magnitud del riesgo como en la amplitud de la participación del sistema de órganos", sostuvo Al-Aly. "'Long Covid-19' es más que un síndrome posviral típico. El tamaño del riesgo de enfermedad y muerte y el grado de afectación del sistema de órganos es mucho mayor que lo que vemos con otros virus respiratorios, como la influenza".

Además, los investigadores encontraron que los riesgos para la salud de sobrevivir al Covid-19 aumentaban con la gravedad de la enfermedad, y los pacientes hospitalizados que requerían cuidados intensivos tenían el mayor riesgo de complicaciones prolongadas y muerte.

"Algunos de estos problemas pueden mejorar con el tiempo, por ejemplo, la dificultad para respirar y la tos pueden mejorar, y algunos problemas pueden empeorar", agregó Al-Aly. "Continuaremos siguiendo a estos pacientes para ayudarnos a comprender los impactos continuos del virus más allá de los primeros seis meses después de la infección. Estamos a poco más de un año de esta pandemia, por lo que puede haber consecuencias aún no visible".