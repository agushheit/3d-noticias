---
category: La Ciudad
date: 2021-08-05T06:00:37-03:00
thumbnail: https://assets.3dnoticias.com.ar/AGUASROJAS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Tomás Rico para El Litoral
resumen: Analizan si los efluentes que tiñeron de rojo el agua provinieron de una
  empresa avícola
title: Analizan si los efluentes que tiñeron de rojo el agua provinieron de una empresa
  avícola
entradilla: 'Los resultados técnicos preliminares apuntan a que el líquido desemboca
  en el Dique II a través de un desagüe pluvial, y no cloacal como debería. Se estudia
  si hubo una falla entre los dos sistemas. '

---
La Provincia analizará las muestras de agua y enviará en las próximas horas notas de pedido de informe con un plazo de 48 horas a la empresa Sánchez y Sánchez.

Las aguas se tiñeron de rojo en el Dique II del Puerto Santa Fe y el olor en el ambiente es nauseabundo. Este llamativo cambio de color no se debe a floraciones de microorganismos que habitan este ecosistema (como por ejemplo cuando las cianobacterias afloran y queda el agua verde), sino que se debe a causas antrópicas por efluentes volcados a través de un desagüe pluvial y que desemboca en una de las cabeceras de la zona portuaria.

Los resultados preliminares del informe técnico que son evaluados entre Aguas Santafesinas (Assa), el Ministerio de Ambiente de la Provincia y la Municipalidad de Santa Fe, indican que los efluentes tienen como origen la empresa avícola "San Andrés", que tiene su fábrica a metros del puerto (Marcial Candioti y Av. Alem).

Edgardo Seguro, secretario de Ambiente de la Municipalidad de Santa Fe, indicó a El Litoral que "los informes aún no son concluyentes, hay algunas hipótesis con las que apuntamos a que el líquido proviene del frigorífico San Andrés por las características, que serán analizadas para ver si concuerdan con las del frigorífico, lo que es muy probable porque se veía material graso".

Durante la mañana de este miércoles, el Ministerio de Ambiente y Cambio Climático, en conjunto con el Ente Administrador de Puerto de Santa Fe, realizó la toma de dos muestras de agua en Dique II de la ciudad capital. Además, desde el Ministerio enviarán en las próximas horas notas de pedido de informe con un plazo de 48 horas a la empresa Sánchez y Sánchez, al Enress para que notifique a Assa, y a la Municipalidad de Santa Fe.

**Características y análisis**

Respecto de las características del efluente que se observó en el Dique II, el secretario de Ambiente comentó que durante la mañana de este miércoles se tomaron muestras del agua para analizarlas en laboratorio. "Antes de asegurar el origen del líquido se harán los análisis, porque podría ser otra empresa del sector que esté usando un líquido de color similar", sostuvo.

Las muestras que serán analizadas por Aguas Santafesinas y el Ministerio de Ambiente de la Provincia. buscarán determinar si hay alguna carga orgánica en el agua; se medirá el pH; la demanda química y bioquímica. Estos resultados podrían estar en unas 48 horas y los más específicos en unos 7 días.

Este medio supo que el laboratorio de Ecotoxicología de la Facultad de Bioquímica y Ciencias Biológicas de la UNL se acercó hasta el Dique II para tomar imágenes de la situación, pero al descartarse que se tratara de una floración de algas no registraron muestras.

**¿Fallas en el sistema de desagüe?**

Seguro destacó que lo llamativo es que la empresa avícola vuelque sus líquidos a la conexión pluvial, ya que tiene permiso de Aguas Santafesinas (Assa) para descargar en las cloacas. "Hace muchos años pasó lo mismo por la rotura de un caño cloacal dentro de un pluvial grande, eso después se rectificó. Por eso ahora estamos analizando si existe la posibilidad de que la cloaca se haya roto dentro de un pluvial; o que esté obstruida la cloaca y haya levantado el nivel y esté llevando el líquido hacia otro lado", remarcó el secretario de Ambiente.

Para corroborar si hubo alguna falla de este tipo, Seguro señaló que, según lo acordado con Aguas Santafesinas, este jueves se desobstruirán los caños cloacales para buscar si hay alguna rotura. "Si esto no nos da resultado veremos si no hay alguna conexión hacia los pluviales que desconozcamos", mencionó el funcionario.

Desde Assa comunicaron a la prensa que se trata de un desagüe pluvial que no corresponde a la empresa. "Estamos verificando de que no haya inconvenientes dentro de las instalaciones a cargo de Assa, redes de agua potable y cloacas, por fuera de eso no tenemos ninguna injerencia", remarcaron y agregaron que este líquido descargado en el puerto no llega hasta la toma de agua Hernández, ya que las bombas de succión están ubicadas aguas arriba.

**Poca dilución y mayor concentración**

La poca dilución del agua hacia el Río Santa Fe, a causa de la bajante extraordinaria, agravaría los efectos ambientales, ya que esos efluentes volcados pueden quedar retenidos en el sedimento y así afectar a las comunidades biológicas que estén cercanas, donde se incluyen peces y demás organismos.

Esta perturbación ambiental, que habría sido generada por la empresa avícola, para el secretario de Ambiente no es intencional: "A veces sucede porque se interconectaron sistemas o porque está tapado. Hoy (miércoles) el frigorífico tuvo la mejor predisposición y revisamos dentro y fuera, y no encontramos nada en los planos que esté mal. Por eso no descartamos que por alguna reparación se hayan conectado los sistemas".

**Otro caso similar**

Días atrás, una situación similar generó polémica en la ciudad de Rawson (Chubut), donde el volcado de efluentes de pesqueras de la capital chubutense provocó la aparición de una "laguna rosada" en la zona del parque industrial de la ciudad vecina de Trelew. El fenómeno derivó en un cruce entre ambos municipios y el gobierno provincial y provocó reclamos sobre el cuidado del medio ambiente. Además del color llamativo del agua, la contaminación trajo olores nauseabundos, proliferación de insectos y otros vectores.