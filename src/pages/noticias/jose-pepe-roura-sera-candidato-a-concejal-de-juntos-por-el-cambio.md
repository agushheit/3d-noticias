---
category: Agenda Ciudadana
date: 2021-07-07T07:55:23-03:00
thumbnail: https://assets.3dnoticias.com.ar/pepe-flor.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: José “Pepe” Roura será candidato a concejal de Juntos por el Cambio
title: José “Pepe” Roura será candidato a concejal de Juntos por el Cambio
entradilla: “Junto a Flor Gonzalez comenzamos a transitar una propuesta de trabajo
  basada en el futuro y crecimiento de Santo Tomé y queremos continuar profundizando
  cada iniciativa."

---
Como parte del equipo de trabajo de la concejal de la UCR Florencia González, el experto en transporte y planeamiento urbano buscará un lugar en el Concejo. “Junto a Flor comenzamos a transitar una propuesta de trabajo basada en el futuro y crecimiento de Santo Tomé y queremos continuar profundizando cada iniciativa. Nos sobran las ganas, el entusiasmo y el compromiso porque amamos nuestra ciudad y creemos en todo su potencial”, afirmó.

De la mano de la edil de la UCR Florencia González, el experto en transporte y planeamiento urbano José “Pepe” Roura será candidato a concejal de Juntos por el Cambio en la ciudad de Santa Tomé. “Vamos a poner en el centro de la escena a las instituciones y a los vecinos, que son los verdaderos protagonistas y quienes mejor saben de las necesidades de su barrio y de la ciudad. Venimos trabajando de esta línea junto a Flor y el equipo, con el objetivo de pensar y avanzar en una Santo Tomé moderna, ordenada, con reglas de convivencia claras y pujante”, agregó.

Profesional de Ciencias Económicas de la Universidad Nacional del Litoral y ex funcionario municipal de la ciudad de Santa Fe, José “Pepe” Roura es vecino de Santo Tomé y forma parte del equipo de trabajo de la Concejal Florencia González. “Somos un equipo de personas que no quieren quedarse de brazos cruzados . Estamos convencidos que la experiencia en gestión, la vinculación con nuestras instituciones, universidades, el trabajo dinámico y profesional son las piezas centrales para la tarea que nos proponemos: construir una Santo Tomé con proyección a futuro, pensada para los próximos 40 o 50 años”, sostuvo.

Por su parte, la concejala Florencia González sostuvo: “Pepe se sumó a nuestro equipo de trabajo para aportarnos toda su experiencia en materia de transporte y planificación urbana. Debemos replicar las buenas experiencias de la ciudad de Santa Fe, iniciativas que han sido transformadoras para los vecinos y vecinas, y que muchas veces no requieren de grandes inversiones, sino de gestiones, acuerdos y soluciones inteligentes. Y en este sentido nos parece fundamental poder sumar en el Concejo un vecino con la experiencia, la formación, el compromiso social de Pepe”.

![](https://assets.3dnoticias.com.ar/pepe.jpeg)