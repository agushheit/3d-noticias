---
category: Agenda Ciudadana
date: 2021-01-27T10:05:26Z
thumbnail: https://assets.3dnoticias.com.ar/fmi.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Con información de Infobae
resumen: Mientras el Gobierno cancela el IFE y el ATP, el FMI aconsejó a los países
  no retirar el apoyo fiscal en medio de la pandemia
title: Mientras el Gobierno cancela el IFE y el ATP, el FMI aconsejó a los países
  no retirar el apoyo fiscal en medio de la pandemia
entradilla: La economista jefe del organismo, Gita Gopitah, afirmó que la crisis económica
  está lejos de despejarse mientras hay una carrera entre el virus mutante y la incipiente
  vacunación global

---
Mientras el Gobierno decidió retirar los principales programas de apoyo que implementó en 2020, el Fondo Monetario Internacional (FMI) aseguró que los países no deben dejar de apoyar a los sectores de la población mientras continúe la pandemia.

Sin hacer una mención específica a la Argentina, la economista jefe del organismo multilateral, Gita Gopitah, afirmó que las perspectivas para el mundo son mejores que hace unos meses atrás, pero advirtió que hay una carrera entre “el virus mutante y la campaña de vacunación” cuyo desenlace todavía resulta difícil de prever.

Al presentar la actualización de las perspectivas económica mundiales, Gopitah ratificó que la crisis del año pasado fue la peor desde la Gran Depresión de 1929 y aclaró que la recuperación en los países se está dando en “dos velocidades”, con sectores que se recuperan rápido y otros más atrasados.

En este sentido, subrayó que es clave la expansión fiscal que se registró el año pasado para minimizar los efectos de la pandemia no se retire, dado que el problema de salud está lejos de terminar.

En este sentido, aunque cuando se le hizo una pregunta sobre la Argentina la economista la evitó amablemente, cabe recordar que el ministro de Economía, Martín Guzmán ratificó que el Gobierno ya no aplicará más ni el IFE ni el ATP, sino que mantendrá algunos programas específicos y focalizados, luego de que la Argentina registrara en 2020 un déficit del 8,5% del PBI, el más alto en 44 años.

![El ministro de Economía, Martín Guzmán, confirmó que no seguirá ni el IFE ni el ATP
](https://www.infobae.com/new-resizer/gFn0gDhG3AObfrHNDGAiQJAJq-o=/420x280/filters:format(jpg):quality(85)//cloudfront-us-east-1.images.arcpublishing.com/infobae/FOPBCRFIK5DFHIFMUSYUPTB5ZE.jpg)

Además, pese a que en agosto cerró en forma exitosa la renegociación de la deuda con los acreedores del sector privado, el país no tiene margen para volver al mercado de capitales internacional y, por otro lado, la alta inflación le impide mantener el ritmo de emisión monetaria del año pasado.

Gopinath se refirió a la situación regional de América latina, al indicar que la región “fue muy golpeada por la pandemia”, aunque, como otras zonas, logró recuperarse parcialmente en la última parte del año, “Se observan diferentes velocidades de recuperación y contracción. En Brasil y México vemos mucho apoyo fiscal que permiten una mejor recuperación”. De inmediato, aclaró: “Hasta que la pandemia termine el riesgo es alto”. El organismo mejoró su previsión para la región al 4,1 por ciento.

Además, descartó que haya riesgo de una “inflación galopante” a nivel global, pese a que la Argentina ocupa el segundo lugar a nivel regional y uno de los 10 primeros a nivel global.

“Estamos viviendo tiempos muy inciertos. Mucho depende de la carrera entre el virus mutante y la vacuna. Lo que sí vemos es una divergencia entre países emergentes que tienen una peor perspectiva que los avanzados, lo cual en buena medida depende de la velocidad en la distribución de las vacunas. Y también en el acceso al financiamiento”, afirmó.

Cabe recordar que el gobierno y el FMI están negociando un nuevo programa de refinanciación de la deuda que tomó la Argentina durante la presidencia de Mauricio Macri por USD 45.000 millones.

El ministro expresó que buscan un acuerdo de facilidades extendidas, que le podría permitir al gobierno postergar hasta 10 años el pago del capital de la mencionada deuda, que, a priori, recae entre 2021 y 2023.

El FMI ha dejado en claro que no se podrá avanzar en la negociación hasta que el Gobierno no deje en claro su programa económico -pese al compromiso de reducir el déficit fiscal este año al 4,5% del PBI- y, aunque Guzmán afirmó que no se puede pensar en no acordar con el organismo, hay sectores en el oficialismo que confían en poder aplazarlo durante este año electoral, para evitar cualquier ajuste que complique el panorama de cara a los comicios legislativos de octubre próximo.