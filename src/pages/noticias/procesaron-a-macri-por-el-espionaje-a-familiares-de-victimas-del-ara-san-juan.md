---
category: Agenda Ciudadana
date: 2021-12-02T06:15:33-03:00
thumbnail: https://assets.3dnoticias.com.ar/MAUMACRI.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: Procesaron a Macri por el espionaje a familiares de víctimas del ARA San
  Juan
title: Procesaron a Macri por el espionaje a familiares de víctimas del ARA San Juan
entradilla: 'El ex Presidente fue procesado por el juez federal de Dolores, Martín
  Bava.

'

---
El juez federal subrogante de Dolores Martín Bava procesó sin prisión preventiva pero con un embargo de 100 millones de pesos al ex presidente Mauricio Macri por el espionaje ilegal a familiares del submarino ARA San Juan, hundido en 2017.

Macri fue procesado por “el delito de realización de acciones de inteligencia prohibidas en carácter de autor, en virtud de haber posibilitado la producción de tareas de inteligencia ilegal, generado las condiciones para que se pudieran llevar a cabo, almacenado y utilizado datos sobre personas, por el solo hecho de sus acciones privadas, u opinión política, o de adhesión o pertenencia a organizaciones partidarias, sociales, sindicales o comunitarias, y por la actividad lícita que desarrollaban”.

Además, el juez le reprochó a Macri “haber pretendido influir en la situación institucional y política del país, en la vida interna de los partidos políticos, en asociaciones y agrupaciones legales de cualquier tipo y en las personas”.

Bava también procesó a Macri por “abuso de autoridad de funcionario público en carácter de autor”.

Como medida adicional, el juez volvió a prohibir la salida del país del ex presidente, “sin perjuicio que el imputado se encuentra fuera del país en este momento”.

“La medida comenzará a regir a partir de su reingreso a la República Argentina”, aclaró Bava.

El juez también le impuso como “reglas de conducta” que “no podrá ausentarse de su lugar habitual de residencia por un término mayor a los diez días sin previo aviso al tribunal”.

También “deberá dar aviso por sí o por intermedio de su abogado defensor de cualquier cambio de domicilio, como así también deberá dar aviso y requerir previa autorización para efectuar cualquier viaje al exterior del país”.

Las víctimas, señaló el juez en su resolución, también hicieron mención a tener problemas en sus teléfonos celulares e incluso con sus correos elelctrónicos, con lo cual el juzgado no descarta una intervención ilegal de dichos dispositivos.

“Algunos de los testigos han relatado la pérdida de fotografías de sus familiares desaparecidos, mensajes intercambiados con los mismos, que estos “salieron” de distintos grupos de la aplicación WhatsApp, que se sobre calentaban sus aparatos telefónicos, escuchaban voces ajenas a la conversación que mantenían, ruidos de fondo y demás inconvenientes que les hacían pensar que sus comunicaciones podían estar siendo intervenidas”, justificó el magistrado en su fallo de 171 páginas.

Sobre Macri, sostuvo “desde su cargo de Presidente de la República Argentina, no pudo ser ajeno a los hechos que se le imputan”.

“Lejos de ello, la propia de inteligencia pone al imputado en rol de establecer los lineamientos generales y la conducción política de todo el sistema de inteligencia, mediante el cual se desarrollaron las acciones ilegales investigadas en autos, en particular, el espionaje de los familiares de los tripulantes del submarino ARA San Juan”. Incluso, el juez señala que muchos de los informes producidos por los agentes de inteligencia rezaban “pedidos y reclamos al Sr presidente de la Nación Mauricio macri por familiares de tripulantes del submarino ARA San Juan”.

Como prueba, el juez tambien hizo un lineamiento temporal de reuniones que Macri mantuvo con el entonces director de la AFI Gustavo Arribas en fechas pegadas a cuando se produjeron los informes de inteligencia hoy cuestionados.

“A lo largo de esta investigación se comprobó que las tareas ilegales implicaron el registro de manifestaciones, acciones de visibilización, reclamos, misas y otra serie de eventos, así como la compilación de datos personales de las víctimas”, concluyó el magistrado en su fallo.

Sobre le objetivo de los informes de inteligencia, el juez sostiene que “las acciones de relevamiento, compilación y análisis expresamente prohibidas fueron producidas para el conocimiento del imputado. Las mismas tenían por objeto informar los movimientos que llevaron adelante las víctimas en el marco de los reclamos que realizaban contra su gestión de gobierno como consecuencia del hundimiento del submarino".

Ahora, la defensa de Macri a cargo de Pablo Lanusse cuenta con diez días para apelar ante la Cámara Federal de Mar del Plata. Por lo pronto, en la causa con Macri suman doce los procesados.