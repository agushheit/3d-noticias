---
category: Agenda Ciudadana
date: 2022-06-22T07:36:30-03:00
thumbnail: https://assets.3dnoticias.com.ar/team-g234c8676e_1920.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: Moratoria para la regularización de cooperativas y mutuales
title: Moratoria para la regularización de cooperativas y mutuales
entradilla: Se encuentra disponible el Programa Moratoria para aquellas entidades
  que adeudan documentación.

---
Luego de la transformación histórica que impulsamos con la Resolución Renovar, el INAES pone a disposición una nueva moratoria para que puedas regularizar tu entidad.

A través de esta moratoria, las cooperativas y mutuales que adeuden documentación podrán ponerse al día presentando únicamente los últimos 3 balances, sin importar cuantos años de documentación adeuden. Así, las entidades tendrán la oportunidad de regularizar autoridades, acceder a subsidios y créditos estatales, volver a brindar productos y servicios, entre muchos otros beneficios.

Para acceder a la moratoria, las entidades deben inscribirse en el curso virtual “Pre admisión Programa Moratoria” disponible en la plataforma Trámites a Distancia de 3 módulos que las acompañará durante todo el proceso, sea o no pre-admitida la cooperativa o mutual.

**¿CONSULTAS?  
Enviá un correo electrónico a:** [**moratoria@inaes.gob.ar**](https://www.argentina.gob.ar/noticias/moratoria@inaes.gob.ar)**  
Comunicarse por teléfono: 011-4124-9300 interno 3403**