---
category: Agenda Ciudadana
date: 2021-11-10T06:00:32-03:00
thumbnail: https://assets.3dnoticias.com.ar/Salon.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Salones de eventos: incrementan el aforo permitido y amplían horarios de
  funcionamiento'
title: 'Salones de eventos: incrementan el aforo permitido y amplían horarios de funcionamiento'
entradilla: Desde provincia dictaron flexibilizaciones para los rubros de salones
  de eventos, casinos, bingos y la actividad deportiva. El foco estuvo puesto en la
  ampliación horaria

---
En el marco de las medidas de convivencia en pandemia por Covid-19, el gobierno provincial dictó la resolución Nº 542, con flexibilizaciones ampliando los días y horarios para la realización de actividades deportivas, culturales y recreativas. La resolución rige a partir de este martes 9 de noviembre.

Al respecto, el ministro de Trabajo, Empleo y Seguridad Social, Juan Manuel Pusineri, detalló que la medida se adopta “luego de las reuniones mantenidas con distintos sectores económicos y sociales, que hacen al quehacer de la provincia de Santa Fe; y teniendo en cuenta la evaluación del cuadro sanitario, a partir de la importancia, intensidad y extensión de la campaña de vacunación”.

**Las flexibilizaciones**

>> Actividad deportiva en modalidad entrenamiento y recreativa de deportes individuales o grupales; y gimnasios, natatorios y establecimientos afines: todos los días de la semana, entre las 6 y las 22 horas.

>> Actividad de casinos y bingos, cumplimentando las reglas generales de conducta y de prevención y los protocolos oportunamente aprobados, los días jueves, viernes, sábados y vísperas de feriados, entre las 10 y las 3 horas del día siguiente, y el resto de los días de la semana entre las 10 y las 2 horas del día siguiente.

>> Actividad de los salones de eventos, fiestas y similares, para la realización de eventos sociales: los días jueves, viernes, sábados y vísperas de feriados, entre las 8 y las 3 horas del día siguiente, y el resto de los días de la semana, entre las 8 y las 2 horas del día siguiente. Sin exceder la ocupación del setenta por ciento (70%) de la superficie disponible.

En este caso, las autoridades municipales y comunales podrán autorizar una ocupación superior de la superficie, en la medida que se verifiquen las condiciones de la infraestructura, para asegurar la adecuada ventilación de los espacios.

Por este motivo, “se ha decidido avanzar en algunas flexibilizaciones de horarios y de aforos para distintos sectores: los salones de eventos, las actividades deportivas, recreativas, de entrenamientos, y de gimnasios; y de bingos y casinos”, detalló el ministro.

Por último, Pusineri destacó que “este es un nuevo esquema de trabajo que tiene que ver con ir transitando hacia una etapa de normalidad para empezar a dejar atrás la pandemia, siempre preservando todos los cuidados”.