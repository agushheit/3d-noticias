---
category: Agenda Ciudadana
date: 2021-04-06T06:19:10-03:00
thumbnail: https://assets.3dnoticias.com.ar/ximenagarcia.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Diputada García
resumen: 'Ximena García: “La ciudadanía santafesina necesita una respuesta urgente
  en materia de Seguridad”'
title: 'Ximena García: “La ciudadanía santafesina necesita una respuesta urgente en
  materia de Seguridad”'
entradilla: El Interbloque Juntos por el Cambio acompañó un pedido de informes de
  Ximena García para conocer los avances en políticas de seguridad que el gobierno
  nacional lleva adelante en la provincia de Santa Fe.

---
Observando la información del Servicio Nacional de Investigación Criminal (SNIC) puede observarse que en el año 2019 la tasa de homicidios dolosos de Argentina se ubicó en 5,1, un 4,5 por debajo de la registrada para la provincia de Santa Fe, que en el mismo año registró 9,6. Comparativamente, en el desglose por provincias ofrecido por el SNIC, durante el 2019 la provincia de Santa Fe lideró la tasa de homicidios dolosos a nivel país seguida por Tucumán y Chubut.

En 2020 de 9,6, la tasa pasó a 10 homicidios cada 100 mil habitantes y, en lo que va de 2021, ya se contabilizan 93 homicidios vinculados en su mayoría a conflictos interpersonales y a la economía del delito.

“En la provincia de Santa Fe la problemática en torno a la inseguridad es alarmante. Hoy, los centros urbanos del Gran Rosario y el Gran Santa Fe son los más afectados y la inseguridad es la principal preocupación de los santafesinos”, explica García.

No obstante, los homicidios no son la única problemática ya que la provincia de Santa Fe también registra una elevada cantidad de “hechos delictivos con armas de fuego”, solo en 2020 se registraron 1.554 hechos de este tipo, con un promedio de 129,5 por mes. Por otro lado, y en torno a los robos, entre 2019 y 2020 el promedio osciló entre 44.509 y 49.062 (según datos de la policía provincial y estadísticas del MPA).

Frente a este doloroso panorama, el Ministerio de Seguridad de la Nación creó la Unidad Rosario del Ministerio de Seguridad (UMR) con la misión de “prevenir y reducir el delito, así como brindar mayor seguridad a la población santafesina”. Siendo sus funciones, la coordinación de acciones investigativas y operativas de las Fuerzas Policiales y Federales en articulación con la Justicia Federal y el Ministerio de Seguridad de Santa Fe; receptando las inquietudes y problemáticas de los actores locales, vinculados a la seguridad, y gestionando una respuesta institucional; y monitoreando el desarrollo de las causas judiciales complejas o vinculadas a las políticas de seguridad.

**Tiempo de mostrar resultados**

Sobre esta UMR, es que el proyecto presentado por la Diputada Nacional Ximena García busca conocer, qué acciones se han desarrollado para mejorar la seguridad de los santafesinos, qué acciones investigativas y operativas ha motorizado el gobierno nacional, cómo viene trabajando con los gobiernos locales y la cantidad de efectivos de Fuerzas de Seguridad Federales desplegados en la provincia.

Por otro lado, el proyecto también interpela a la gestión del gobierno nacional, acerca del destino y el grado de avance de las iniciativas financiadas desde la JGM en torno al “Convenio de Cooperación Institucional y Asistencia Financiera”, firmado el 16/10/2020 entre ambas jurisdicciones por un total $3.000 millones destinados a modernización tecnológica y a la reforma integral del sistema de seguridad pública.

“Consideramos que es menester conocer cómo se están articulando las jurisdicciones y cómo se está ejecutando el financiamiento en una materia tan sensible como la seguridad pública, más aún cuando se trata de una provincia con profundos problemas vinculados a la violencia y el narcotráfico como es Santa Fe”, sentenció la diputada.

El proyecto también lleva la firma de los Diputados Nacionales: María Lucila Lehmann, Federico Angelini, Carolina Castets y Gisela Scaglia.