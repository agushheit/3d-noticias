---
category: La Ciudad
date: 2021-09-01T06:15:01-03:00
thumbnail: https://assets.3dnoticias.com.ar/CLOACAS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Según un relevamiento nacional, hay 51 barrios de Santa Fe sin conexión a
  cloacas
title: Según un relevamiento nacional, hay 51 barrios de Santa Fe sin conexión a cloacas
entradilla: La cifra se desprende de un informe del Registro Nacional de Barrios Populares,
  donde se registró que aproximadamente 13.600 familias no tienen acceso a cloacas

---
Según un informe del Registro Nacional de Barrios Populares (Renabap) hecho a través de encuestas a los vecinos, ninguno de los 51 barrios populares de la ciudad tiene acceso a la red cloacal. Estos en su mayoría son barrios apostados en el cordón oeste de Santa Fe, identificados por el organismo nacional, que abarca unas 13.600 familias en total.

En el relevamiento hecho por el Renabap, se identificó que 40 de los 51 barrios santafesinos sin conexión regular a cloacas cuentan con desagüe a pozo negro/ciego u hoyo. Además, en otros 9 barrios existe la conexión mediante desagüe a cámara séptica.

**Conexión a agua de red**

En algunos casos en donde se desprenden los datos, el informe abarca sectores de barrios, sin abarcar a toda la zona. Otro dato preocupante que se desprende del informe es el referido al acceso al agua de red, donde solo dos de los 51 barrios identificados tienen garantizada una conexión formal.

En este ítem, se detectó que un 80% de los barrios abarcados mantiene una conexión irregular a la red pública con agua corriente. Un 14% de los barrios cuenta con bombas de agua domiciliarias conectadas a pozo, mientras que el 6% restante cuenta con camión cisterna y conexión regular.

**Conexión eléctrica**

En cuanto a la situación del acceso a la conexión eléctrica, en el informe se detectó que en 39 barrios de Santa Fe existen conexiones irregulares a la red (un 76% del total de barrios populares). A su vez, otros diez sectores de la ciudad mantienen una conexión formal con medidor domiciliario y uno con medidor comunitario. En el barrio Los Bañados se registró acceso formal pero con consumo limitado.