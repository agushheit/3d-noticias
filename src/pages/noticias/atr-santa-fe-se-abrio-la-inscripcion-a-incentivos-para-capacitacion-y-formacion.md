---
category: Estado Real
date: 2020-12-26T11:53:41Z
thumbnail: https://assets.3dnoticias.com.ar/1912-atr.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: 'ATR Santa Fe: Se encuentra abierta la inscripción a incentivos para capacitación
  y formación'
title: 'ATR Santa Fe: Se encuentra abierta la inscripción a incentivos para capacitación
  y formación'
entradilla: Los y las interesadas tienen tiempo hasta el 28 de diciembre para realizar
  la inscripción en www.santafe.gob.ar

---
El Gobierno de Santa Fe, a través de la Secretaría de Estado de Igualdad y Género, abrió **la inscripción a Activá Futuro**, un **programa de incentivo y apoyo a jóvenes de entre 17 y 35 años, con domicilio en la provincia de Santa Fe, que planean continuar su formación**.

Al respecto, la secretaria de Estado de Igualdad y Género, Celia Arena, precisó que «este es el segundo proyecto de ATR Santa Fe, el área de juventudes del Gobierno de Santa Fe que depende de la Secretaría de Estado de Igualdad y Género, y esperamos que la convocatoria sea tanto o más exitosa que con ATR Proyectos Participativos, donde se encuentran en evaluación unos mil proyectos grupales de cinco mil jóvenes de todo el territorio de la provincia».

«La formación y la capacitación es una de las garantías para lograr la autonomía, y es por ello que estamos invitando a los y las jóvenes a participar de esta convocatoria. La pandemia por COVID-19 nos modificó la vida a todos y a todas, y es por ello que hoy, más que nunca, las juventudes de la provincia, que tienen ideas, son innovadoras, emprendedoras, comunicadoras de los cambios y adaptables a los nuevos contextos, deben sentirse acompañadas a la hora de emprender sus proyectos de vida. Este es el compromiso que asumimos los y las integrantes de la gestión de Omar Perotti y es por ello que convocamos a las juventudes a formarse y a capacitarse en las temáticas propuestas», concluyó Arena.

**<span style="font-family: helvetica; font-size: 1.25em; font-weight: 600;"> La convocatoria </span>** 

ATR Activá Futuro es un programa de incentivo y apoyo a jóvenes que planifican iniciar o dar continuidad a su formación y capacitación priorizando las siguientes temáticas: conectividad y nuevas tecnologías, ambiente, y aquellas terminalidades que aborden acciones que den respuestas a problemáticas económicas, de salud, sociales, educativas, culturales y de promoción de derechos surgidas en el contexto de la pandemia por Covid-19.

Además, se priorizarán también otras formaciones y capacitaciones no universitarias con impacto inmediato en la empleabilidad.

Los interesados pueden obtener más información en [www.santafe.gob.ar](http://www.santafe.gob.ar/ "Gobierno de la provincia"); o contactarse con [atrsantafe@santafe.gob.ar](mailto:atrsantafe@santafe.gob.ar)