---
category: La Ciudad
date: 2021-12-01T06:15:32-03:00
thumbnail: https://assets.3dnoticias.com.ar/taCHOS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Aumenta la tarifa de taxis en la ciudad de Santa Fe
title: Aumenta la tarifa de taxis en la ciudad de Santa Fe
entradilla: "La bajada de bandera de la tarifa diurna será de $ 110, y la nocturna
  de $ 121.\n\n"

---

La Municipalidad de la Ciudad de Santa Fe informó que a partir de este miércoles 1 de diciembre se incrementa la tarifa del servicios de taxis.

 El aumento regirá a partir de la hora cero, tanto para las tarifas diurna como nocturna.

 En ese sentido, la tarifa diurna (que rige de 6 a 22 horas) será de $ 110 la bajada de bandera. La ficha cada 130 metros costará $ 9, y el minuto de espera (y hasta los 5 minutos) será de $ 9; en tanto que la espera desde los 5 minutos en adelante será a convenir entre el chofer y el pasajero.

 Mientras, la tarifa nocturna (de 22 a 6) tendrá una bajada de bandera de $ 121, la ficha cada 130 metros y el minuto de espera (y hasta los 5 minutos) serán de $ 10; y la espera superior a los 5 minutos también será a convenir.

 Dichos aumentos, requeridos por los trabajadores del sector debido a los incrementos de los diferentes insumos, se estipulan de acuerdo al valor del precio del litro de nafta premium.