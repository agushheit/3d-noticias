---
layout: Noticia con imagen
author: .
resumen: "COVID 19: Protocolos"
category: Estado Real
title: "COVID19:  Requisitos y restricciones de ingreso por provincia"
entradilla: Se acerca el verano y con ello la obligación de pensar en el turismo
  en medio de la pandemia que ha desatado el Covid-19.
date: 2020-11-05T12:47:56.653Z
thumbnail: https://assets.3dnoticias.com.ar/covid-viajar.jpg
---
Así, los gobiernos tanto nacional como provinciales, van pensando en programas y protocolos que permitan hacer que la gente pueda recorrer los puntos turísticos con promociones y de forma accesible en medio de una crisis económica y sin olvidar que el virus sigue circulando.

A continuación, enumeramos los requisitos y restricciones de ingreso por provincia:

**Chaco**

* Solicita que los vuelos hacia esa provincia sean a partir del 25 de octubre.
* Se requiere que quien quiera hacer uso de tal opción obtenga el permiso provincial propuesto en el programa denominado “Volver a Casa” ingresando a la página https://gobiernodigital.chaco.gob.ar/login.
* Limitaciones propias del art. 11 del DNU N° 792/20. Además, el horario de llegada a la provincia deberá respetar la alarma sanitaria establecida, la cual es entre las 7 y 21 hs.

**Chubut**

Presta conformidad y no informa requisitos.

**Córdoba**

* Presta conformidad a que los servicios de transporte, tanto aéreo como terrestre, ingresen al territorio de la provincia de Córdoba.
* Requisitos para abordar un vuelo con destino la provincia de Córdoba:
* Los pasajeros que pretendan usar estos servicios deberán portar el “CERTIFICADO ÚNICO HABILITANTE PARA CIRCULACIÓN – EMERGENCIA COVID-19” y/o el instrumento sustitutivo o complementario que la normativa vigente requiera.
* Descargar la APP Cuidar y proceder al correcto llenado y actualización de los datos requeridos por la misma, incluyendo la declaración Jurada por COVID 19.

**Corrientes**

* Los pasajeros deberán solicitar el ingreso a la provincia de Corrientes el permiso, trámite online a través del sitio web https://permisos.corrientes.gob.ar/permisos
* Requisitos: motivo de la solicitud del permiso, DNI, número de trámite de DNI, nombre y apellido, fecha de nacimiento, mail, teléfono, resultado negativo por PCR menor a 48 hs. En caso de no contar con PCR menor a 48 hs. El pago vía online de un testeo por los medios electrónicos que el sistema pone a disposición del requirente, control éste que se realizará en el lugar de ingreso.

**Jujuy**

* Los pasajeros deben contar con Certificado Único Habilitante para Circulación-Emergencia COVID19 y descargar la APP CIUDAR.
* Los pasajeros sin residencia en JUJUY deben poseer obra social que tenga prestación en dicha prov. o en caso contrario contratar un seguro de viajero

**La Rioja**

* Presta conformidad para reanudación de servicios aéreos a partir del 19 de octubre.
* Informa que se realizará control médico obligatorio en el Aeropuerto de LA RIOJA a los pasajeros al momento de su llegada.
* Además, se solicita que los pasajeros cuenten con certificado de PCR o Test rápido de antígenos con resultado negativo menor a 72 hs.

**Mendoza**

Presta conformidad con la mayor cantidad de frecuencias que la logística dispuesta en conjunto con las empresas permita, y con las personas que necesiten o pretendan viajar a la provincia por motivo alguno.

**Misiones**

* Solicita con prontitud, autorizar la apertura progresiva del Aeropuerto de Iguazú, pensando a futuro en el turismo de cabotaje dentro del país.
* Se requiere certificado de COVID-19 negativo menor a 48 hs. Y en caso de no contar con el mismo se deberá realizar el Test rápido de COVID-19 o PCR bajo su exclusivo gasto.
* Toda persona que ingrese al territorio provincial deberá utilizar los mecanismos electrónicos de geolocalización y auto test establecidos.

**Neuquén**

* Vuelos restringidos únicamente para pasajeros de actividades esenciales; personas en tratamiento médico, o repatriados con domicilio en la Provincia del Neuquén que tengan como destino final alguna de las 7 (siete) localidades que registran transmisión comunitaria de COVID-19 en la Provincia.
* Requisitos para pasajeros: Deberán portar Certificado de Circulación “CUIDAR” para actividades esenciales, y/o constancia de turno médico, someterse a control de síntomas (temperatura) antes de la partida y al arribo del vuelo. Las aerolíneas deberán presentar listado nominalizado de pasajeros y tripulación que incluya datos de contacto; y ubicación de los mismos dentro del vuelo. Los repatriados deberán acreditar domicilio en Neuquén.
* Se deberá buscar un esquema de programación de los vuelos que evite la superposición de las frecuencias en la llegada y en la partida; a fin de evitar aglomeración de personas en el Aeropuerto de la Ciudad de Neuquén.
* Requisitos para pasajeros: Deberán portar Certificado de Circulación “CUIDAR” para actividades esenciales, y/o constancia de turno médico; someterse a control de síntomas (temperatura) antes de la partida y al arribo del vuelo. Las aerolíneas deberán presentar listado nominalizado de pasajeros y tripulación, que incluya datos de contacto; y ubicación de los mismos dentro del vuelo. Los repatriados deberán acreditar domicilio en la Provincia del Neuquén.

**Rio Negro**

* Informa que los pasajeros deberán presentar Certificado COVID-19 Negativo realizado con un máximo de 48hs. de antelación a los traslados, como así también deberá ser controlada la temperatura de los mismos previo al ascenso a las aeronaves.
* Asimismo, los pasajeros deberán completar una planilla con carácter de declaración jurada, la que deberá contener sus datos personales y la información relativa al itinerario previsto (Nombre y apellido, DNI, teléfono de contacto, lugar de residencia, domicilio de alojamiento en Río Negro, motivo del viaje, fecha de regreso, etc.). A los fines de su recepción, esta Provincia dispondrá de personal en los Aeropuertos. En el mismo sentido, y al momento de ingreso a la Provincia, las personas serán sometidas al examen de los signos vitales por parte del personal sanitario afectado a los operativos de control dispuestos en distintos puntos de nuestra geografía.

**Salta**

Para ingresar a la provincia es necesario presentar:

* * DNI.
  * Certificado de circulación – coronavirus Covid-19.
  * Permiso de ingreso de particulares con residencia en la provincia de Salta (RUIS). En el caso de ingresos humanitarios y sanitarios (asistencia a persona mayor, estar en situación de calle adonde se encuentren quienes necesiten volver, ser trabajador golondrina, repatriados del extranjero, retorno luego de culminar tratamiento médico en otra provincia, por fallecimiento de familiar directo, estado grave de un familiar directo, asistencia a menores en riesgo) se puede ingresar con un permiso especial en lugar del RUIS.
  * Una declaración jurada sobre las condiciones sanitarias de la persona en la cual certifique estar libres de síntomas compatibles con COVID-19 y no haber tenido contacto estrecho con un paciente positivo en los últimos 14 días previos al ingreso a la Provincia.
* Además, es necesario descargar la app SaltaCovid o cargar sus datos en la web
* Están autorizados a ingresar y deben cumplir con 14 días de aislamiento en domicilio las personas que realicen transporte de cargas e ingresen para descansar (el aislamiento puede interrumpirse a pedido de la empresa en el día 7), los residentes de zonas 1 y 2 y los ingresos sanitarios y humanitarios (salvo dificultad o urgencia).
* No requieren aislamiento en domicilio las personas consideradas esenciales, los que ingresan para realizar alguna actividad, los que realizan transportes de cargas (ingreso laboral ni los que ingresen a descansar en las zonas 3 y 4).
* Toda persona que cuente con IgG positivo está exenta de realizar aislamiento de ingreso a la provincia con vigencia por noventa días.

**San Juan**

* Se requiere para el ingreso a la provincia el certificado de PCR negativo menor a 72 hs., exceptuando los menores de 6 años que no están obligados a presentar dicho certificado.
* Además, se exige exhibir CERTIFICADO UNICO HABILITANTE PARA CIRCULACIÓN-EMERGENCIA COVID19 o poseer la APP CIUDAR.
* Cumplir estrictamente con lo previsto en el PROTOCOLO PROVINCIAL COVID19 según el tipo de transporte utilizado.
* Además, se solicita la reanudación de los servicios entre SAN JUAN y BUENOS AIRES que prestaba AEROLINEAS ARGENTINAS SA conforme la programación que la misma empresa realiza.

**San Luis**

* Se presta conformidad para personal esencial y razones de salud.
* Para los pasajeros que ingresen/egresen de la provincia será requisito indispensable, portar con la autorización de ingreso/egreso gestionada a través de www.sanluis.gov.ar/coronavirus/ la que deberá ser acreditada y visada previo a embarque por personal autorizado.
* Es obligación inexcusable de la empresa prestataria del servicio de transporte verificar el estricto cumplimiento del presente protocolo, antes del embarque de cada pasajero, en caso de incumplimiento, será responsable de los daños y perjuicios que pudiere generar, debiendo además regresar al pasajero al lugar de origen y asumiendo todos los gastos y costos que ello implique.
* Se solicita autorización para que en la zona de pre embarque de la línea área, se faculte la presencia de una persona de nuestra jurisdicción (recurso humano aportado por la Provincia) a fin de fiscalizar los requisitos establecidos en el protocolo, previo al embarque del pasajero, siendo indispensable para efectuar el correspondiente control.

**Santa Cruz**

* La ANAC autorizó para noviembre el ingreso de dos frecuencias semanales a Río Gallegos y El Calafate, según lo recomendado por el Ministerio de Salud de la provincia de Santa Cruz.
* Estas frecuencias están sujetas a la autorización mediante aplicación provincial y la capacidad de garantizar el control y aislamiento de los pasajeros por 14 días en un hotel o dispositivo de aislamiento certificado por el Comité Operativo de Emergencia (COE) de cada localidad.
* Previo al traslado a la provincia los pasajeros deben tener:
* * Certificado Único de Circulación Nacional COVID-19.
  * Permiso provincial emitido en circulacion.santacruz.gob.ar – Transporte Público Nacional Aéreo y Terrestre.
  * Declaración jurada de circulación y permanencia del Ministerio de Salud
* La provincia no requiere un PCR negativo previo al traslado.

**Santa Fe**

* En base a criterios epidemiológicos, los factores de riesgo potencial de contagio y conforme al artículo 2º de la Resolución 221 del Ministerio de Transporte de la Nación-, se determinaron las siguientes condiciones y requisitos a cumplimentar por cada pasajero:
* * Se autoriza a viajar a la Provincia de Santa Fe a Personal esencial y personas que deban realizar tratamientos médicos.
  * Presentar Certificado Único Habilitante Para Circulación - Emergencia COVID-19.
  * Exhibir DD JJ sobre estado de salud (DJES) de la App de la Provincia de Santa Fe e información del viaje interprovincial.
  * El Personal esencial o personas que deban realizar tratamientos médicos, podrán ingresar al territorio de la provincia por 72Hs. Estarán eximidos de realizar cuarentena, pero no podrán sociabilizar. Se deja expresamente aclarado que en el caso de que la estadía se prolongue por más de 72hs, y previo al reinicio de su actividad, deberán cumplimentar una cuarentena de 14 días.
  * Se autoriza a viajar a toda persona residente, no exceptuada, no esencial, que se encuentre en otra provincia u otro país, y desee regresar a su lugar de residencia. La misma deberá realizar cuarentena obligatoria.
* El Poder Ejecutivo de la Provincia da a conocer que mantendrá la evaluación de la actividad en base a los criterios epidemiológicos que determinen las autoridades sanitarias y los factores de riesgo potencial de contagio que la misma implica.

**Santiago del Estero**

* Se presta conformidad para aquellas personas que se encuentren habilitadas por el art. 11 del DNU N° 792/20.
* Se establecerá un Protocolo especifico que aplicarán los organismos encargados de coordinar los traslados, donde se les informará toda la parte operativa y los requerimientos específicos para cada empresa que quiera realizar dichos traslados, como así también toda la documentación necesaria y los requisitos con los que deberán contar los usuarios del servicio.
* Los pasajeros deberán presentarse de manera obligatoria para solicitar su pasaje con DNI e hisopado negativo de hasta 72 hs. antes avalado por el SIISA (Sistema Integrado de Información Sanitaria Argentina).
* Al ingreso a la provincia se realizará un control a los pasajeros de temperatura y olfato, test de diagnóstico rápido, DDJJ que deberá firmar y tener descargada en su teléfono móvil la aplicación provincial CUIDAR SE para el seguimiento de los pasajeros que se hayan trasladado.

**Tierra del Fuego**

A partir del 1º de noviembre la provincia requiere una constancia de PCR negativo con antelación no mayor a 72 horas o IgG positivo hasta 30 días previos al vuelo.

**Tucumán**

Los pasajeros que arriben al aeropuerto Benjamín Matienzo deberán:

* Presentar el Certificado nacional de circulación (CUIDAR)
* Declaración jurada a los efectos de contar con la trazabilidad del movimiento de los pasajeros arribados.
* Al arribar, se realizarán estudios clínicos sobre parámetros vinculados a los síntomas compatibles con COVID-19.

Para mayor información, el gobierno nacional habilitó la página <https://www.argentina.gob.ar/transporte/covid-19/requisitos-ingreso-por-provincia>