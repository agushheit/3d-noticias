---
category: Agenda Ciudadana
date: 2021-04-30T10:19:56Z
thumbnail: https://assets.3dnoticias.com.ar/casa.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Más de 100 mil inscriptos al programa Casa Propia
title: Más de 100 mil inscriptos al programa Casa Propia
entradilla: "Los créditos anunciados por el Gobierno la semana pasada están destinados
  a la refacción y construcción de viviendas. Cómo inscribirse.\n\n"

---
El ministro de Desarrollo Territorial y Hábitat, Jorge Ferraresi, confirmó que ya se inscribieron más de 100 mil personas para acceder a la flamante línea Créditos Casa Propia, lanzada la última semana junto al presidente Alberto Fernández, para la refacción y construcción de nuevas viviendas.

Como parte del programa federal Casa Propia-Construir Futuro, estas nuevas líneas otorgarán 87 mil créditos individuales a tasa cero para seguir reduciendo el déficit habitacional.

Los préstamos hipotecarios ajustarán por la fórmula Hog.Ar de actualización de capital basada en la evolución de los salarios, lo que permitirá una mayor previsibilidad a los tomadoras de estos créditos.

"Estos nuevos créditos son parte del compromiso que tenemos como Estado Nacional para reducir el déficit habitacional", señaló Ferraresi.

En un comunicado, el ministro sostuvo que "al mismo tiempo actuarán como generadores de trabajo en cada uno de los municipios porque la construcción o refacción de viviendas no sólo les cambia la vida a las propias familias, sino que resultan fundamentales para potenciar el proceso productivo que será, sin lugar a dudas, el motor económico en la reconstrucción de la Argentina”.

A través de esta nueva iniciativa, el Ministerio de Desarrollo Territorial y Hábitat entregará 65 mil créditos personales tanto de $100 mil como de $250 mil pesos para refacciones, terminaciones, reemplazos o solución a problemas de la vivienda, y otros 22 mil créditos hipotecarios para la construcción de viviendas nuevas de hasta 60 metros cuadrados de superficie en lote propio.

Según detalló el ministro, entre los más de cien mil inscriptos e inscriptas hay aspirantes de las 23 provincias y de la Ciudad Autónoma de Buenos Aires, tanto para los dos segmentos de refacción, como para la línea de construcción.

Entre ellas, las provincias que más inscriptos aportan son Buenos Aires, Córdoba, Santa Fe y Mendoza, seguidas por Entre Ríos, Tucumán y CABA.

Quienes estén interesados y cumplan con los requisitos detallados en www.argentina.gob.ar/casapropia podrán completar allí mismo su formulario de inscripción.