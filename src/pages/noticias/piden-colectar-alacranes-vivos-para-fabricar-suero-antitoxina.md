---
category: La Ciudad
date: 2021-12-16T06:00:48-03:00
thumbnail: https://assets.3dnoticias.com.ar/unnamed.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Piden colectar alacranes vivos para fabricar suero antitoxina
title: Piden colectar alacranes vivos para fabricar suero antitoxina
entradilla: '"El Malbrán hizo un pedido a las provincias a las que hace llegar el
  suero para que faciliten, a su vez, la llegada de alacranes vivos"'

---

"Campaña masiva para la recolección de escorpiones vivos en los domicilios. Por el momento se recibirán en el ex Hospital de Niños, Bulevar Gálvez al 1563, 1ª piso". La novedad fue compartida en las últimas horas a través de las redes sociales y llamó inmediatamente la atención. ¿Será real el pedido? Lo es. Se trata de una solicitud del Instituto Malbrán que es el que elabora antitoxinas que permiten tratar a personas, sobre todo niñas y niños, que fueron picadas por un alacrán y desarrollan cuadros moderados a severos.

 Víctor Tramontin es Veterinario e integra el equipo provincial de Control de Vectores y de la provincia. En diálogo con El Litoral explicó en qué consiste esta campaña y cuáles son los recaudos que se deben tomar a la hora de "capturar" un ejemplar vivo de alacrán.

 "El objetivo es recibir alacranes vivos que luego enviamos al Malbrán (en Buenos Aires) donde le sacan la toxina a partir de la cual se hace el suero antiescorpiónico", explicó el profesional.

 "El Malbrán hizo un pedido a las provincias a las que hace llegar el suero para que faciliten, a su vez, la llegada de alacranes vivos", aportó.

 **Todas las precauciones**

 En este punto es fundamental saber cómo se debe realizar la captura de un escorpión en el domicilio. "La tarea debe hacerse con todas las precauciones", aclaró Tramontin.

 "Como primera medida no hay que agarrarlo directamente con las manos". El resto de los consejos están publicados en la página oficial de zoonosis:

 - Tener protegidas las manos con guantes para evitar ser picados.

\- Manipular el alacrán con algún objeto que ponga distancia con el cuerpo (palita, escoba, palo, varilla, etc.)

\- Introducir el alacrán en un frasco de vidrio o plástico. Se coloca el frasco sobre el ejemplar y por debajo se pasa una hoja de papel de manera que al dar vuelta el recipiente el animal quede adentro.

\- Humedecer un pedacito de algodón con agua y dejarlo en el fondo del frasco: son ejemplares que necesitan líquido para sobrevivir.

\- Cerrar el frasco herméticamente con una tapa a rosca. Si se duda de la efectividad del cierre encintar la unión de la tapa y el frasco.

\- Realizar perforaciones pequeñas en la tapa.

\- Enviar al Programa de Zoonosis y Vectores de la Provincia de Santa Fe (Bv. Gálvez 1563 1° piso) con los datos de procedencia y algún contacto telefónico.

\- Tener en cuenta que los alacranes que han sido fumigados o rociados con algún producto químico o biológico o aquellos que murieron no podrán ser enviados.

 **Prevención**

 Todas estas acciones son necesarias cuando se encuentra un alacrán en el domicilio. Pero también hay maneras de evitar que ingresen, sobre todo en esta época (de mayor temperatura) en que se encuentran con mayor actividad.

 Para ello se aconseja no dejar ropa tirada en el piso, sacudir el calzado antes de colocárselo sobre todo si se trata de niñas y niños, colocar un burlete o algún otro mecanismo para sellar las puertas de la casa que dan al exterior y tener especial cuidado con las rejillas y desagües de la cocina y el baño.

 Para ello se recomienda colocar una tela metálica o un enrejado en rejillas. Si ninguna de estas opciones es posible, se pueden forrar las rejillas con una media de mujer (medias de nylon) que va a permitir que pase el agua pero no que ingresen los alacranes a la vivienda.

 Si aún así una persona es picada, se recomienda concurrir al Hospital Cullen (si es adulta) o al Hospital Orlando Alassia si se trata de menores de 14 años, para evaluar el tratamiento a seguir.