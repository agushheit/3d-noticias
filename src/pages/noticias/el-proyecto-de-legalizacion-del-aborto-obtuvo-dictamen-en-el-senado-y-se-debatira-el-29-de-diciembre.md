---
category: Agenda Ciudadana
date: 2020-12-18T11:13:31Z
thumbnail: https://assets.3dnoticias.com.ar/1812senado.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: El Litoral'
resumen: El proyecto de legalización del aborto obtuvo dictamen en el Senado y se
  debatirá el 29 de diciembre
title: El proyecto de legalización del aborto obtuvo dictamen en el Senado y se debatirá
  el 29 de diciembre
entradilla: 'El proyecto de ley sobre Interrupción Voluntaria del Embarazo (IVE),
  tal como esperaba el oficialismo, se discutirá en el Senado de la Nación el 29 de
  diciembre. '

---
El jueves 17 de diciembre por la tarde, un plenario de tres comisiones: Banca de la Mujer, Justicia y Asuntos Penales y Salud, emitió el **dictamen favorable** para que sea tratada en el recinto.

**En la Cámara de Diputados el proyecto fue aprobado el viernes pasado por 131 votos a favor, 117 en contra y 6 abstenciones tras un debate que insumió más de 20 horas**. Ahora, en la cámara alta se espera una votación mucho más pareja. En 2018, la primera vez que se debatió la legalización y despenalización del aborto, la ley resultó rechazada por 38 votos contra 31.

«Pasamos a la firma», anunció Norma Durango, la presidenta de la Banca de la Mujer, comisión cabecera del plenario. Si bien procesarlas de forma virtual lleva tiempo, distintas fuentes del oficialismo y autoridades de las comisiones confirmaron que están garantizadas los 28 avales necesarios.

La sorpresa es que, entre ellos, se encuentra la firma «a favor, pero en disidencia parcial» del senador oficialista Edgardo Kueider, uno de los legisladores que figuraba indefinido. Desde su entorno confirmaron que, al igual que Weretilneck y Martínez, el entrerriano plantea que se hagan modificaciones en el recinto.