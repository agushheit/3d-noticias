---
category: La Ciudad
date: 2021-04-10T06:00:51-03:00
thumbnail: https://assets.3dnoticias.com.ar/LLUVIAS.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Informe de lluvias en la capital
title: Informe de lluvias en la capital
entradilla: La Municipalidad brinda una actualización de datos de lluvia y actuaciones
  con motivo de las precipitaciones de este viernes.

---
La Municipalidad informa los datos de lluvia caída en la capital provincial este viernes 9 de abril, cuando la red de estaciones meteorológicas propias registró los siguientes datos acumulados hasta las 18 horas de hoy:

DOAE (COBEM): 62,25 mm

Centro: 104,00 mm

Alto Verde: 70,25 mm

CIC Facundo Zuviría: 63,75 mm

La intensidad máxima de lluvia del día de la fecha fue de 42,00 mm/h, a las 11:10 horas, en el DOAE (COBEM). En tanto la ráfaga máxima de viento fue de 34,8 km/h, de dirección Este Sureste (ESE) a las 12:45 horas, en Alto Verde.

**Reclamos recibidos**

Durante toda la jornada, se registraron a través de la línea de Atención Ciudadana, 49 reclamos en total relacionados con las lluvias: alumbrado público (4), arbolado público (9), recursos hídricos (20), higiene ambiental (12), acción social (1) y tránsito (3).

Se recuerda que quienes tengan inquietudes o reclamos, pueden comunicarse al 0800-777-5000.