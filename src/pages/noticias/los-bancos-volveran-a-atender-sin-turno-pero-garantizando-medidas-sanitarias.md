---
category: Agenda Ciudadana
date: 2021-11-12T06:15:23-03:00
thumbnail: https://assets.3dnoticias.com.ar/BANCO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Los bancos volverán a atender sin turno, pero garantizando medidas sanitarias
title: Los bancos volverán a atender sin turno, pero garantizando medidas sanitarias
entradilla: Lo dispuso el Banco Central a través de un comunicado donde se agregó
  que "si la capacidad de atención lo permite, se podrá atender con ambas modalidades,
  dando prioridad a la atención de los clientes que tienen turno".

---
El Banco Central (BCRA) resolvió este jueves que las entidades financieras volverán a atender al público sin turno a partir del lunes próximo, debiendo garantizar las medidas preventivas de salud.  
  
En un comunicado, la autoridad monetaria dispuso que "las entidades financieras y las empresas que prestan servicios vinculados, podrán a partir del lunes realizar la atención presencial del público en general sin turno".  
  
La medida dispone que "en caso de optar por dar prioridad a la atención con turno, las entidades deberán exhibir esa condición en forma clara en sus páginas de Internet, el turno no podrá demorar más de tres jornadas hábiles y se deberá tramitar en forma sencilla a través de esas páginas o por otro medio electrónico y atenderse con celeridad".  
  
El BCRA aclaró que "si la capacidad de atención lo permite, se podrá atender con ambas modalidades, dando prioridad a la atención de los clientes que tienen turno y a personas que se presenten con su Certificado Único de Discapacidad vigente o con movilidad reducida".  
  
La norma alcanza a las entidades financieras, empresas no financieras emisoras de tarjetas de crédito y/o compra, los otros proveedores no financieros de crédito inscriptos, las casas de cambio y las empresas de cobranzas extrabancarias.  
  
El BCRA detalló que "el pago de haberes previsionales o prestaciones de la seguridad social se deberá realizar conforme al cronograma establecido por el respectivo organismo. El día en que los beneficiarios deban presentarse para cobrar su jubilación, pensión y/o prestación, podrán realizar cualquier otro trámite".  
  
Más adelante, la normativa enfatiza que "las entidades alcanzadas por esta resolución están sujetas al estricto cumplimiento de las normas sanitarias y de las recomendaciones dispuestas por las autoridades sanitarias nacional y/o jurisdiccional, garantizando la provisión de los elementos sanitarios y de limpieza y el cumplimiento de las distancias interpersonales de seguridad estipuladas".  
  
En ese sentido, se recuerda que deberán cumplir la totalidad de las medidas preventivas generales previstas por el Decreto de Necesidad y Urgencia N° 678/21 y modificatorios, destacándose las siguientes reglas de conducta, "las personas deberán mantener, entre ellas, una distancia mínima de 2 metros, las personas deberán utilizar tapabocas en espacios compartidos, se deberán ventilar los ambientes en forma adecuada y constante y las personas deberán higienizarse asiduamente las manos", concluye el BCRA.