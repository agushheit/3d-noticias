---
category: Estado Real
date: 2021-01-26T10:03:53Z
thumbnail: https://assets.3dnoticias.com.ar/martorano.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno Santa Fe
resumen: 'Martorano: “La vacunación es un acto solidario, no implica sólo protegerse
  a uno sino también a los que están cerca”'
title: 'Martorano: “La vacunación es un acto solidario, no implica sólo protegerse
  a uno sino también a los que están cerca”'
entradilla: La ministra de Salud se aplicó la primera dosis de la vacuna Sputnik V
  y destacó el trabajo coordinado en el histórico operativo.

---
A pocos días de cumplirse un mes desde la llegada de las primeras vacunas contra el Covid-19, la ministra de Salud, Sonia Martorano, recibió este lunes en el Galpón 17 de la ciudad de Rosario, la primera dosis de la misma.

“Esta vacuna con la cual estuvimos trabajando junto a los equipos de salud ya se incorporó para los mayores de 60 años. Recordemos que en esta primera etapa la prioridad la tienen los agentes de salud y estamos a la espera de más dosis para completar la inoculación de todos los trabajadores”, indicó la funcionaria.

Además, “luego de haber vacunado al recurso humano que desempeña sus tareas en áreas críticas e hipercríticas, laboratorio de biología molecular, hisopadores, servicios de emergencias y traslados, en este momento se comenzaron a vacunar personal de guardia, es decir aquellos que se encuentran con mayor exposición con esta pandemia” detalló Martorano; y agregó que “estamos esperando que Rusia termine la producción y que las Sputnik V lleguen a Buenos Aires para luego ser distribuidas en todo el país”.

Asimismo, Martorano expresó que “la vacuna está teniendo una aceptación cada vez mayor, la vacunación es un acto solidario, porque no implica únicamente protegerse uno sino también a los que están cerca”.

Junto con la ministra se vacunaron las directoras del Hospital de Niños Zona Norte, Mónica Jurado; del Hospital Provincial Rosario, Teresita Ghio; del Hospital Centenario, Claudia Perouch; y la directora Provincial de Enfermería, Emilse Belletti.

En este sentido, Jurado declaró: “Estamos felices de poder transmitir esta oportunidad a nuestros colegas, poder construir salud entre todos y mejorar nuestra capacidad de trabajo en función de la expectativa que nos da poder vacunarnos”.

Además, destacaron que la respuesta de los trabajadores de salud es muy buena, que hay mucha adherencia y expectativa de poder protegerse.

**CRONOGRAMA DE VACUNACIÓN**

Al ser consultada sobre las personas que continúan en la lista de vacunación, la ministra de Salud expresó “el cronograma comienza con el personal de salud (70 mil trabajadores), luego las fuerzas de seguridad (30 mil), y luego los docentes (90 mil)”.

“Santa Fe ya se encuentra preparada para vacunar, hoy contamos con 140 localidades con freezers y todas las condiciones para vacunar. Además tenemos 21 lugares que tienen que ver con efectores de tercer nivel con terapias intensivas pero se va a extender a estos 140 puntos para poder realizar la vacunación”, prosiguió.

A su vez, Martorano detallo que “terminada esas etapa se seguirá con grupos priorizados, los adultos mayores de 60 años, aclarando que para este segmento etario se siguen todas las normativas nacionales. Posteriormente, las personas de entre 18 y 59 años con comorbilidades”.

“Todo esto suma 1.200.000 personas que son la población objetivo. Una vez concluidas todas estas etapas, se comienza con la vacunación para la población en general”, explicó Martorano.

**DISTRIBUCIÓN DE LAS VACUNAS**

Una vez que ingresan al país, el 8% está destinada a Santa Fe. De ese total, se distribuyen a las 5 regiones de la provincia y luego cada región distribuye por todo su departamento.

En este sentido Martorano informó que “inicialmente se comienza con la Sputnik V, que requiere dos dosis, y también se sumaría la vacuna Astra Zeneca, de Oxford, de la cual ya hay una compra muy grande realizada, y con estas dos está asegurada una cantidad muy grande para el país. Asimismo, a nivel nacional, se sigue haciendo gestiones con otras marcas y otros laboratorios”.

“Hay que entender que todo el planeta está necesitando las vacunas y por eso quienes la producen se encuentra contra reloj para poder hacer las entregas”, concluyó la ministra.

**REGISTRO DE VACUNACIÓN**

El Estado provincial está trabajando de manera coordinada en la implementación de una aplicación donde podrán inscribirse aquellos santafesinos y santafesinas, que encuadren en la población objetivo, su intención de vacunación.

“Cuando ya estemos próximos a la vacunación para las personas que no comprenden los grupos priorizados o de riesgo, vamos a comenzar con el registro el cual tiene dos maneras, uno que tiene como objetivo saber cuántas personas se desean vacunar y, una vez registrado, el ciudadano pueda elegir el lugar donde vacunarse y se le asigna un turno”, concluyó la ministra de Salud.