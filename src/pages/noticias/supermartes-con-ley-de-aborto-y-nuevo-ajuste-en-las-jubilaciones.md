---
category: Agenda Ciudadana
date: 2020-12-28T11:02:11Z
thumbnail: https://assets.3dnoticias.com.ar/congreso.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: El Litoral'
resumen: Supermartes con ley de aborto y nuevo ajuste en las jubilaciones
title: Supermartes con ley de aborto y nuevo ajuste en las jubilaciones
entradilla: 'La discusión del proyecto de Ley de Interrupción Voluntaria del Embarazo
  (IVE) se iniciará este martes y podría extenderse hasta el día siguiente. '

---
El proyecto para legalizar el aborto hasta la semana 14 de gestación, cuenta casi con tantos apoyos como rechazos en el Senado.

En el marco de fuerte polarización, no participan dos legisladores peronistas: el tucumano José Alpérovich, suspendido mientras enfrenta una causa por abuso sexual, y el expresidente Carlos Menem, internado y en delicado estado de salud. Las opositoras Lucila Crexell y Stella Maris Olalla no han adelantado sus posiciones, que serán decisivas a la hora del conteo.

La discusión volverá a ser a través de videoconferencia y solo se permitirá a poco más de una docena de legisladores en el recinto: cuatro por cada uno de los dos bloques mayoritarios (el Frente de Todos y Juntos por el Cambio) y seis por las demás bancadas.

Durante los últimos días, la sanción de la iniciativa, tal como llegó de Diputados, fue puesta en duda por planteos de reformas; el oficialismo promete correcciones en la reglamentación para convencer voluntades y evitar que el proyecto vuelva a Diputados en segunda revisión.

Los sectores que apoyan el proyecto y aquellos que lo rechazan convocaron a realizar una concentración y vigilia en las inmediaciones del Congreso y en diferentes puntos del país

La Conferencia Episcopal Argentina (CEA) reiteró su «dolor» ante el proyecto. Durante una misa en Luján, el presidente del Episcopado, monseñor Oscar Ojea, pidió «establecer acuerdos fundamentales desde la dirigencia política, económica y social que nos permitan mirar hacia adelante priorizando tierra, techo y trabajo para todos».

Junto con la iniciativa se tratará el proyecto conocido como el Plan de los 1.000 Días, para la atención y el cuidado de la salud durante el embarazo y la primera infancia. Establece una nueva asignación por Cuidado de Salud Integral, que consiste en el pago de una Asignación Universal por Hijo (AUH) a concretarse una vez por año para ayudar al cuidado de cada niño o niña menor de tres años.

Al mismo tiempo, extiende la Asignación Universal por Embarazo (AUE), que pasará de tener seis mensualidades a nueve, para abarcar la totalidad de la gestación.

<br/>

**<span style="font-family: helvetica; font-size: 1.3em;">Movilidad jubilatoria</span>**

El oficialismo de la Cámara de Diputados aspira a convertir en ley el martes, con el respaldo de bloques provinciales, el proyecto de movilidad jubilatoria, por el cual las jubilaciones se ajustarán en forma trimestral con una fórmula que combina en un 50% la recaudación de la Anses y en otro 50% la variación salarial.

El proyecto, que ya tiene sanción del Senado, será debatido el martes -a partir de las 11 horas- en una sesión especial, la última del año, que se realizará bajo la modalidad presencial con las excepciones de conexión remota para aquellos diputados que integran grupos de riesgo, ya sea por cuestiones de edad o de salud.

La iniciativa recibió dictamen el miércoles pasado en el marco de un plenario de las comisiones de Previsión y Seguridad Social, que preside Marcelo Casaretto; y de Presupuesto, a cargo de Carlos Heller; ambos del Frente de Todos.

El plenario de comisiones avaló el texto por el cual las jubilaciones se ajustarán con una fórmula que combina en un 50% la recaudación de la Anses y en otro 50% la variación salarial, surgida de la que resulte más alta entre las medidas por el Indec y por el Ministerio de Trabajo, con base en el índice de Remuneración Imponible Promedio de los Trabajadores Estables (Ripte).

La fórmula, que surgió de una propuesta elaborada sobre la base del trabajo de una comisión mixta que el Poder Ejecutivo envió al Senado, comenzará a utilizarse desde 2021, cuando venza el actual período de emergencia en la materia.

El nuevo índice está basado en la fórmula aplicada desde 2009 por el gobierno de Cristina Fernández de Kirchner, que luego el expresidente Mauricio Macri dejó sin efecto y estableció su propio esquema jubilatorio que, según los análisis, habría perjudicado los haberes del sector pasivo.

  
 