---
category: Deportes
date: 2021-07-27T06:00:55-03:00
thumbnail: https://assets.3dnoticias.com.ar/JJOOARG.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Los resultados argentinos en la tercera jornada de Tokio 2020
title: Los resultados argentinos en la tercera jornada de Tokio 2020
entradilla: Uno por uno, el detalle de los resultados de las competencias de la tercera
  jornada de los Juegos Olímpicos Tokio 2020.

---
Los siguientes son los resultados cosechados por atletas argentinos en la tercera jornada de competencia de los Juegos Olímpicos Tokio 2020.

\* Esgrima: Belén Pérez Maurice perdió con la húngara Anna Marton 15-12 en primera ronda de sable individual.

\* Tiro: Melisa Gil, 18va. en la clasificación general de skeet femenino.

\* Tiro: Federico Gil, 17mo. en skeet masculino.

\* Rugby 7: Argentina 29 - Australia 19, Grupo A, partido #1

\* Rugby 7: Argentina 14 - Nueva Zelanda 35, Grupo A, partido #2.

\* Handball: Argentina 25 - Alemania 33

\* Tenis: Nadia Podoroska le ganó a Ekaterina Aleksandrova (ROC) por 6-1 y 6-3 por la segunda ronda del single femenino.

\* Vela: Laser femenino / Lucía Falasca: 27ma. (regata #3) y 21ra (#4)

\* Vela: RS:X - femenino / Celia Tejerina: - 18va. (#4) - 15ta (#5) y 21ra (#6)

\* Vela: RS:X - masculino / Saubidet; 21ro. (#4) , no terminó (#5) y 19no. (#6)

\* Vela: Laser masculino / Francisco Guaragna: 32do. (#2) y 22do. (#3).

\* Boxeo: Francisco Verón le ganó a Adam Chartoi (Suecia) por 5-0 en los 16vos. de final de peso medio.

\* Básquetbol: Argentina 100 - Eslovenia 118

\* Boxeo: Ramón Quiroga perdió con Gabriel Escobar (España) 5-0 (unánime) en los 16vos de final de peso mosca.

\*Hockey: Argentina 3 - España 0 (Leonas), Grupo B, partido #2.

\* Natación: Delfina Pignatiello, octava en la Eliminatorias 1500m. / Serie 5, con un tiempo de 16m. 33s. 69/1000.

\* Vóleibol: Argentina 2 - Brasil 3, parciales 19-25, 21-25, 25-16, 25-21 y 16-14 (masculino).