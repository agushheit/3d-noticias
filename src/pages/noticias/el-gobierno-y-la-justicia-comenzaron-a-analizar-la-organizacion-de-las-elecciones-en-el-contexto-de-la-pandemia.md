---
category: Agenda Ciudadana
date: 2021-02-05T07:05:50-03:00
thumbnail: https://assets.3dnoticias.com.ar/elecciones.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Infobae
resumen: El Gobierno y la Justicia analizan la organización de las elecciones en el
  contexto de la pandemia
title: El Gobierno y la Justicia analizan la organización de las elecciones en el
  contexto de la pandemia
entradilla: Fue la primera reunión entre los jueces de la Cámara Electoral y las autoridades
  de la Dirección Nacional Electoral. Se trabajó pensando en los comicios internos
  y las generales

---
El gobierno nacional y la Justicia tuvieron ayer la primera reunión del año para comenzar a delinear las elecciones legislativas que se realizarán en un contexto que nunca tuvieron: una pandemia. El encuentro fue entre la Cámara Nacional Electoral y la Dirección Nacional Electoral, que depende el Ministerio del Interior, en la sede del tribunal, a dos cuadras de la Casa Rosada.

En la reunión estuvieron los jueces de la Cámara, Santiago Corcuera y Daniel Bejas -el tercer integrante, Alberto Dalla Vía, está de licencia- la titular de la DINE, Diana Quiodo, la secretaria de Asuntos Políticos del Ministerio del Interior, Patricia García Blanco, y funcionarios del tribunal. “Fue un encuentro preparatorio. Sin definiciones pero productivo para empezar a organizar las elecciones”, coincidieron ante Infobae dos fuentes consultadas.

Los temas que se trataron fueron varios. Pero de desarrolló bajo una premisa: las elecciones de este año serán dos, las internas el 8 de agosto y las generales el 24 de octubre, más allá de la discusión política planteada sobre la primera sobre si suspender o no las PASO. Fue un planteo que los gobernadores le hicieron al presidente Alberto Fernández y sobre el que se espera una definición del gobierno. En la reunión de ayer no se analizó el tema -es una decisión política que requiere una modificación de la ley electoral por parte del Congreso Nacional- y se habló de la organización de las dos elecciones. “Si hay una suspensión de las PASO no debería demorarse porque el calendario electoral comienza en abril”, explican en la Cámara Electoral.

Uno de los más importantes fue la organización y la logística de las elecciones. Con la pandemia del coronavirus los comicios deberán tener medidas especiales y eso implica un formato de elección distinto a anteriores. Tanto los jueces como las funcionarias coincidieron en que se deberá contar con elementos de higiene en cada lugar de votación -alcohol en gel, barbijos o mascaras-, medidas para el distanciamiento -es conocida la aglomeración que se da en los colegios- y hasta personal especial que controle los protocolos. También de contemplar la convocatoria de más empleados y de autoridades de mesa en virtud de quiénes sean personas de riesgo ante el virus, o que se contagien o sean contactos estrechos. El armado de burbujas entre los afectados a la organización es una de las alternativas.

Hubo coincidencias en la necesidad de contar con la opinión del Ministerio de Salud y de la elaboración de protocolos específicos para los comicios. Tanto la justicia como el gobierno trabajan en este tema desde el año. Los dos partes tuvieron reuniones con los apoderados de los partidos políticos y los jueces electorales de cada provincia. En esos encuentros se comenzaron a escuchar ideas. Los jueces de la Cámara plantearon que esa organización requerirá una asignación presupuestaria mayor a la prevista en cualquier elección.

Sobre ese punto los jueces de la Cámara plantearon la necesidad que las autoridades de mesa reciban un cobro “motivador” para asistir en este contexto sanitario y que no haya demoras en los pagos. En los comicios presidenciales de 2019 el monto fue de 1.900 pesos. En todas las elecciones la presencia de las autoridades de mesa es una preocupación.

Otro tema, y en el que el Correo Argentino ya comenzó a trabajar, es que se necesitarán más lugares de votación que en otras elecciones para que en cada uno haya menos votantes. El Correo está haciendo un mapeo en todo el país para buscar otros espacios, además de los colegios, que cumplan con las condiciones como lugar de votación.

También se habló de la vacunación de todas las personas que trabajarán en la elección. Hace dos semanas la Cámara Electoral le pidió al ministro de Salud, Ginés González García, que sea vacunado todo el personal que esté afectado a las elecciones. La respuesta de las funcionarias del Ministerio del Interior fue que en el plan de vacunación contra el Covid-19 están entre el personal prioritario los integrantes de las fuerzas de seguridad de todo el país que en las elecciones trabajan en la custodia de los colegios y las urnas, entre otras funciones. Falta contemplar a quiénes serán autoridades de mesa y los fiscales de los partidos, entre otras personas. Sobre eso todavía no hay una definición de las autoridades sanitarias que en gran medida estará marcada por el acceso que el país tenga a vacunas.

“Es necesario empezar a definir medidas concretas”, planteó uno de los integrantes de la reunión. Todo estará sujeto al escenario sanitario que se presente en el futuro. Si habrá más restricciones cuando se vote o no. Los contactos entre la Cámara y el gobierno continuarán. La semana que viene los integrantes del tribunal tendrán otra reunión con los jueces electorales de todas las provincias.