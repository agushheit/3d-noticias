---
category: Estado Real
date: 2022-01-28T17:02:18-03:00
thumbnail: https://assets.3dnoticias.com.ar/fmi.png
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3d noticias
resumen: 'Comunicado de prensa del FMI: “El personal técnico del FMI y las autoridades
  argentinas han llegado a entendimientos sobre políticas clave como parte de sus
  discusiones en curso sobre un programa respaldado por el FMI.'
title: Comunicado de prensa del FMI
entradilla: 'Un equipo del  FMI, encabezado por Julie Kozack, directora adjunta del
  Departamento del Hemisferio Occidental, y Luis Cubeddu, jefe de misión para Argentina,
  emitió hoy la siguiente declaración: '

---
“El personal técnico del FMI y las autoridades argentinas han llegado a entendimientos sobre políticas clave como parte de sus discusiones en curso sobre un programa respaldado por el FMI. Las principales áreas del acuerdo incluyen lo siguiente:

“El personal técnico del FMI y las autoridades argentinas acordaron el sendero de consolidación fiscal que formará un ancla de política clave del programa. El sendero fiscal acordado mejoraría de manera gradual y sostenible las finanzas públicas y reduciría el financiamiento monetario. Es importante destacar que también permitiría aumentar el gasto en infraestructura y ciencia y tecnología y protegería programas sociales focalizados. Acordamos que una estrategia para reducir los subsidios a la energía de manera progresiva será fundamental para mejorar la composición del gasto público.

“Hemos llegado a un entendimiento sobre el marco para la implementación de la política monetaria como parte de un enfoque múltiple para enfrentar la alta y persistente inflación. Este marco tiene como objetivo asegurar tasas de interés reales positivas para respaldar el financiamiento interno y fortalecer la estabilidad.

“También hemos acordado que el apoyo financiero adicional de los socios Internacionales de Argentina ayudaría a reforzar la resiliencia externa del país y sus esfuerzos para asegurar un crecimiento más inclusivo y sostenible.

“El personal técnico del FMI y las autoridades argentinas continuaran su trabajo en las próximas semanas para llegar a un acuerdo a nivel del personal técnico. Como siempre es el caso, el acuerdo final sobre un acuerdo de programa estaría sujeto a la aprobación del Directorio Ejecutivo del FMI.”