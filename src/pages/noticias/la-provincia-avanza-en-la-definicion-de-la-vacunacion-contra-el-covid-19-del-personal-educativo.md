---
category: Estado Real
date: 2021-01-22T03:49:51Z
thumbnail: https://assets.3dnoticias.com.ar/cantero.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno Provincia de Santa Fe
resumen: La provincia avanza en la definición de la vacunación contra el Covid 19
  del personal educativo
title: La provincia avanza en la definición de la vacunación contra el Covid 19 del
  personal educativo
entradilla: La ministra Cantero participará este viernes de la reunión del Consejo
  Federal de Educación que encabezará el ministro nacional Nicolás Trotta. Mientras
  tanto avanza en la planificación de las acciones en Santa Fe.

---
La ministra de Educación, Adriana Cantero, junto a sus pares de todo el país, participarán este viernes de la primera reunión del Consejo Federal de Educación de 2021 con el titular de la cartera educativa nacional, Nicolás Trotta.

Uno de los temas sustantivos será sin duda, la vacunación del personal educativo contra el Covid 19. No obstante, las carteras de Educación y de Salud de nuestra provincia avanzan en la planificación del operativo que se dará apenas arriben las dosis para el sector.

En ese marco, Cantero aseguró que en Santa Fe, “hace un mes venimos trabajando con el Ministerio de Salud identificando los centros de vacunación que deberíamos habilitar en todo el territorio provincial, haciendo el mapeo, el trabajo geográfico, calculando los movimientos de la población para tener la planificación lista de modo que ni bien lleguen las vacunas destinadas al sector educativo, podamos empezar con inmediatez la campaña masiva”.

Asimismo, la ministra sostuvo que según las previsiones “el 15 de marzo estaríamos iniciando actividades escolares con todos los protocolos y al mismo tiempo vacunando a los adultos que están a cargo de las escuelas” teniendo en cuenta que en la provincia “son más de 85 mil agentes” entre docentes y asistentes escolares, por lo cual, aseguró que “la campaña va a llevar unos días también para cumplimentarse”. Y remarcó que “este año en nuestra línea de Escuelas Seguras agregamos el trabajo en esta vacunación masiva a las inversiones destinadas a la adecuación sanitaria de los edificios escolares, la preparación de alternativas de cursado con protocolos, la provisión de insumos para esos cuidados y la insistencia en que todos los niños y las niñas tengan su plan de vacunación completos”.

**REUNIÓN DEL CONSEJO FEDERAL**

Cantero adelantó que en el encuentro de este viernes se abordará “entre todos los criterios federales para las alternativas de presencialidad en las escuelas, la campaña de vacunación y la semana de formación federal. Participar de ese espacio de trabajo colectivo, con debates y consensos que articulan el sistema educativo, es de suma importancia”. Al mismo tiempo observó que “Santa Fe es grande, es heterogénea, tiene un territorio muy diverso y exige una planificación situada”.

Más adelante Cantero se refirió a la advertencia de médicos en torno a que las clases presenciales podrían favorecer los contagios. “La provincia está trabajando un formato posible con aplicación de protocolos” y siempre hemos sido respetuosos de las indicaciones de nuestras autoridades sanitarias y lo seguiremos haciendo con esa orientación. No obstante en nuestra experiencia del año pasado, las escuelas con alto cumplimiento de los protocolos, con jornadas escolares adaptadas, han demostrado ser espacios más seguros que otros”.

En ese plano, Cantero expresó que de acuerdo a la experiencia en los más de 150 establecimientos educativos del centro norte que a fin de 2020 reiniciaron clases presenciales se comprobó que “el formato es posible y que los chicos habitaron la escuela con estos protocolos sin dificultad”.

**MÁS DE 300 MILLONES DE PESOS DE INVERSIÓN**

En último orden, la ministra se refirió al ámbito paritario docente, a partir de lo cual sostuvo que “el diálogo ha sido nuestra herramienta de trabajo” con los gremios docentes y adelantó que “estamos esperando el 5 febrero para el primer encuentro paritario de este año”.

Respecto de la infraestructura escolar destacó que “se viene haciendo un trabajo que hacía muchísimos años no se hacía: relevar todas las necesidades, planificar la inversión de los recursos nacionales, provinciales y municipales”.

La titular de la cartera educativa Santafesina mencionó que el año pasado se invirtió “siempre desde el punto de vista de recuperar los establecimientos educativos con una mirada sanitaria” y destacó que “en estos días se está a punto de distribuir nuevamente una inversión de más de 300 millones de pesos para seguir en ese camino y seremos muy exigentes con la aplicación de la Ley de Financiamiento Educativo que llega a todos los municipios y comunas porque esos fondos deben invertirse en las escuelas para cumplir las metas que esa normativa nacional propone en la búsqueda de las metas educativas que incluye el compromiso asegurar escolaridad plena en los tramos obligatorios”.

Por último, la ministra sostuvo que la provincia “viene trabajando insistentemente por el derecho de la educación de los chicos” y abogó por un “trabajo conjunto para que todos los alumnos y los maestros recuperen un espacio en la escuela para poder trabajar” en el marco de la pandemia.