---
category: Estado Real
date: 2022-02-02T10:35:14-03:00
thumbnail: https://assets.3dnoticias.com.ar/pantanal-gbf51e584b_1920.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: '3d noticias '
resumen: 'Dia mundial de los humedales: ¿en que estado se encuentra la ley?'
title: 'Dia mundial de los humedales: ¿en que estado se encuentra la ley?'
entradilla: 'El proyecto de Ley quedó fuera del listado de temas para tratar en las
  sesiones extraordinarias del Congreso. De esta manera perdió estado parlamentario
  y volvió a foja cero. '

---
La discusión por los humedales comenzó a tomar más fuerza tras los incendios registrados en el Delta del Paraná durante 2020. El fuego expansivo hizo que se instale en la agenda la necesidad de sancionar una Ley de Humedales. El 24 de noviembre la Comisión de Recursos Naturales y Conservación del Ambiente Humano firmó el dictamen de mayoría y dejó el texto listo para ser tratado en la Cámara Baja. Tanto desde el oficialismo como desde la oposición expresaron por esos días estar dispuestos a votar a favor.

Según el reglamento, los proyectos de ley que ingresan a la Cámara de Diputados deben ser tratados en el término de dos años, considerando cada año como el período ordinario de marzo al 30 de diciembre. Puede haber una prórroga por indicación del Presidente durante diciembre que fue lo que ocurrió en 2021.

Así, el 1 de marzo de 2021 se cumplió el primer año legislativo y el mes que viene se cumplirá el segundo. Al no ser incluido en el temario para las extraordinarias, la Ley de Humedales volvió a cero. Los proyectos ambientales que sí serán tratados son la creación del Parque y Reserva Nacional Ansenuza y Bañados del Río Dulce, en Córdoba; el proyecto para el Parque y Reserva Nacional Islote Lobos, en Río Negro; y la creación del Área Marina Protegida Bentónica Agujero Azul.

¿Por qué el 2 de febrero se conmemora el Día Internacional de los Humedales?

El **día mundial de los humedales se** celebra el **2 de febrero** de cada año para **conmemorar** la fecha de adopción de la Convención Ramsar sobre los **Humedales** en la ciudad iraní Ramsar en 1971, y crear conciencia en todo el mundo acerca del valor de los **humedales** para la humanidad y el planeta.

Para mas información,  [https://elauditor.info/actualidad/ley-de-humedales--una-materia-que-sigue-pendiente_a61f818bf7290536a900b5b64](https://elauditor.info/actualidad/ley-de-humedales--una-materia-que-sigue-pendiente_a61f818bf7290536a900b5b64 "https://elauditor.info/actualidad/ley-de-humedales--una-materia-que-sigue-pendiente_a61f818bf7290536a900b5b64")