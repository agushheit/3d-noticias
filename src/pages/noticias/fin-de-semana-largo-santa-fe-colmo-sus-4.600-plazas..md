---
category: Agenda Ciudadana
date: 2022-12-12T07:30:28-03:00
thumbnail: https://assets.3dnoticias.com.ar/turismo2.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: 'Fin de semana largo: Santa fe colmó sus 4.600 plazas. '
title: 'Fin de semana largo: Santa fe colmó sus 4.600 plazas. '
entradilla: El hospedaje, en la provincia promedió el 80%, con lugares que llegaron
  a la ocupación plena, como fue la capital.

---
“Desde el jueves 8 de diciembre hasta hoy domingo 11, miles de viajeros recorrieron el país generando un movimiento gastronómico, hotelero y comercial. El gasto diario por persona fue de $8.772, un 8,3% superior al último fin de semana largo, en noviembre”, informó la Cámara Argentina de la mediana empresa (CAME)

En Rosario, con más disponibilidad de alojamiento, se ocupó el 80% de sus 8.000 camas disponibles, con una estadía promedio de dos noches. Otras ciudades con mucha convocatoria fueron Melincué, Puerto Gaboto, Esperanza, Villa Constitución, Santa Rosa de Calchines y Saladero Cabal. Hubo alta demanda de propuestas culturales, pero lo que más convocó fueron los circuitos turísticos de la costa santafesina, con las playas como centro de atracción. El turista que llegó a la provincia fue de ciudades cercanas, como Buenos Aires, Córdoba, Entre Ríos y la misma provincia.

Cerca de un 1.200.000 de turistas recorrieron la Argentina el fin de semana extra largo, gastando $37.304 millones. Fue la última escapada del año y las familias la aprovecharon.

Mar del Plata, el Partido de La Costa, Puerto Iguazú, la Quebrada de Humahuaca, Villa Carlos Paz, la ciudad de Mendoza, San Carlos de Bariloche y El Calafate, fueron algunas de las ciudades más concurridas.

En los siete fines de semana largos que van del año, ya viajaron 12.479.534 turistas y dejaron un impacto económico directo de $255.124 millones.

Los números fueron elaborados por CAME en base a información brindada por entidades de provincias, municipios y Nación.