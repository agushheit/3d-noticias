---
category: Agenda Ciudadana
date: 2021-08-16T06:15:34-03:00
thumbnail: https://assets.3dnoticias.com.ar/enacom.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: El Enacom rechazará los nuevos aumentos dispuestos por las empresas de comunicaciones
title: El Enacom rechazará los nuevos aumentos dispuestos por las empresas de comunicaciones
entradilla: '"Nosotros vamos a rechazar los aumentos" en los servicios de telefonía
  fija y móvil, internet y televisión paga para agosto y septiembre que las empresas
  informaron a los usuarios.'

---
El Ente Nacional de Comunicaciones (Enacom) rechazará los aumentos en los servicios de telefonía fija y móvil, internet y televisión paga para agosto y septiembre que las empresas informaron a los usuarios, aseguró a Télam el vicepresidente de la dependencia oficial, Gustavo López.

"Nosotros vamos a rechazar los aumentos", dijo López, quien advirtió que "lo que pretenden las empresas es un proyectado del 65 al 70% de aumento en un año", incremento al que calificó de "disparate".

"Con aumentos de ese tipo, amparados por la justicia, mucha gente se va quedar sin servicio", enfatizó el funcionario.

En enero de este año se descongelaron los precios de los servicios de comunicaciones, En esa oportunidad, el Enacom autorizó hasta el 1 de julio aumentos del 20%, mientras que las empresas aumentaron alrededor de 32%, amparadas en medidas cautelares que frenaron la aplicación del DNU 690 y sus reglamentaciones que categorizaron a la tv paga, internet y telefonía como servicios públicos esenciales y en competencia.

"Nuestro objetivo era que no hubiera aumentos hasta diciembre, pero las empresas ya anunciaron incrementos para agosto y septiembre. Esta semana nosotros lo vamos a rechazar", indicó López en diálogo con Télam.

El vicepresidente del Enacom recordó que existen medidas cautelares iniciadas por las empresas que impiden la aplicación del DNU 690 que declara a internet, telefonía fija y móvil y tv paga como servicio público esencial y en competencia, así como la posibilidad de regular los precios.

"En este punto no hay dialogo porque cada uno tiene una postura diametralmente opuesta, no hay posibilidad de zanjarlo: O es servicio público o no es servicio público" insistió López.

El vicepresidente del Enacom dijo que a pesar de las objeciones realizadas al DNU 690, "las empresas están invirtiendo más que en el 2019", cuando los precios estaban liberados.

López concluyó que tras analizar los montos invertidos en los últimos seis años, "la inversión no depende de la regulación sino de la macroeconomía".

Consultado por la posición de quienes promueven que el Estado subsidie la demanda y deje los precios liberados a la competencia indicó que: "subsidiar la demanda sería: cobren lo que quieran que total el Estado lo paga. Nosotros decimos: cobren lo justo, lo razonable, que nosotros también ayudamos".

Subrayó, además, que el Fondo de Servicio Universal tiene por objeto financiar la expansión de redes y que los ingresos que existen por ese concepto no alcanzarían para subsidiar la demanda.

Ante la posición del sector privado que asegura verse condicionado por la declaración de servicio público, y especialmente por la regulación de precios, López señaló que "nosotros no nos metemos en sus ganancias, pero estamos cuidando al consumidor".

Agregó que la decisión de declarar a internet como servicio público se tomó un año atrás cuando en plena pandemia, con el aislamiento social vigente y un congelamiento en las tarifas de gas, luz y agua, las empresas del sector "quisieron volver a aumentar de un modo brutal" los precios de los servicios.

La semana pasada el presidente Alberto Fernández reavivó el debate público al sostener que las comunicaciones son un servicio público "le pese a quien le pese".

Desde el ámbito legislativo, el senador oficialista Alfredo Luenzo presentó un proyecto de ley que toma el mismo principio del DNU 690 y el presidente de la comisión de Comunicaciones de la Cámara de Diputados, Pablo Carro, sostuvo que "en la Argentina que viene no podemos depender en servicios esenciales como la conectividad, solo con el criterio de cómo quieran cobrarlos las grandes empresas".

Para el vicepresidente del Enacom queda claro que "vale la pena el debate público sobre si internet o las telecomunicaciones tienen que tener un acceso universal o tienen que estar restringido por la ley de oferta y demanda".

López rechazó que la declaración de servicio público sea una medida que vaya en contra de un grupo empresario determinado, dado que Telecom fue la primera empresa en imponer aumentos superiores a los aprobados por el Enacom.

"Nunca lo pensamos así. Las circunstancias nos llevan a determinadas declaraciones porque quien encabezo la lucha en contra fue un grupo especifico", indicó.

Recordó que "todo esto vino a partir de la pandemia" y que "el diálogo por la suba de precios se rompió cuando quisieron aumentar mucho más allá de lo que la lógica indicaba".

Señaló que la declaración de internet como servicio esencial es una recomendación de las Nacionales Unidas y puso como ejemplo de esto que en "Chile ya tiene media sanción el servicio público de internet y Colombia lo aprobó la semana pasada".

A lo que se suma que tienen la misma calificación para internet en México, Canadá y "algunos países de Europa", agregó el funcionario.

"Nosotros lo hicimos, lo creemos y lo sostenemos porque nos parece que como la luz, el gas y el agua, es un servicio esencial para la vida cotidiana. La declaración de servicio publico nos permite pensar y discutir hacia adelante normas que garanticen el acceso. Todo lo demás estamos dispuestos a conversarlo siempre", finalizó López.