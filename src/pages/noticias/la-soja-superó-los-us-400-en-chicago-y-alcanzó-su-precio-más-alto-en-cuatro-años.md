---
layout: Noticia con imagen
author: "Fuente: Telam"
resumen: Subió el precio de la soja
category: El Campo
title: La soja superó los US$400 en Chicago y alcanzó su precio más alto en
  cuatro años
entradilla: "La tendencia positiva internacional se replicó en el mercado local:
  el precio de la soja subió  hasta los $26.450 la tonelada, mientras que la
  oferta en dólares tuvo una escalada de US$8 para cerrar a US$335."
date: 2020-11-06T11:49:04.099Z
thumbnail: https://assets.3dnoticias.com.ar/soja.jpg
---
La soja continúa con su tendencia alcista en el mercado de Chicago y -tras tres ruedas consecutivas en alza- hoy pudo superar la marca de los US$400 por tonelada y alcanzar la mejor marca de los últimos cuatro años, impulsada por la fuerte demanda china y por la falta de lluvias en zonas productoras de Sudamérica.

La soja saltó hoy US$8,1 en la plaza bursátil estadounidense y la posición más cercana alcanzó los US$404,5 la tonelada, su valor más alto desde julio de 2016.

De esta manera, en lo que va de la semana, la oleaginosa acumula un aumento de casi US$17 en el contrato de noviembre.

Sus subproductos también acompañaron la suba del poroto con un salto del 3,11% (US$23,6) en el aceite hasta los US$781,7 la tonelada, mientras que la harina ganó 0,45% (US$1,9) y se ubicó en US$427 la tonelada.

En el marcado local la tendencia positiva de Chicago también se replicó: el precio de la soja disponible subió $450 hasta los $26.450 la tonelada, mientras que la oferta en dólares tuvo una escalada de US$8 para cerrar a US$335.

Los fundamentos de este marcado incremento en los precios de la soja y derivados se sustentan en la fuerte demanda china de poroto y a las condiciones de sequía que afecta a las zonas productoras de Sudamérica, en especial en Brasil.

Según la corredora argentina de granos Grassi, "en Sudamérica, el clima vuelve a ganar atención, principalmente en Brasil. Según pronósticos, en los próximos días predominarían condiciones de sequía y se especula con posible retraso en el desarrollo productivo".

Al respecto, el analista de la Bolsa de Comercio de Rosario (BCR), Federico Di Yenno, dijo a Télam que "tenemos un escenario climático de la Niña (clima seco) para este año que puede impactar fuertemente en los rendimientos de América del Sur".

"Hay mucho monitoreo en el mercado respecto a las condiciones climáticas que pueden existir en la región. Sabemos que los años Niña están inversamente correlacionados con el rendimiento de la soja en Argentina. A nosotros nos pega mucho y también a la zona sur de Brasil", agregó el especialista.

Si bien en los últimos días se registraron lluvias en zonas productoras de Brasil que impulsaron las siembras, que ya venían retrasadas a causa de la sequía, la cantidad de agua caída fue considerada "insuficiente", lo que también explicó en parte la suba de precios de principios de semana.

En la Argentina, las últimas precipitaciones permitieron que se realicen los primeros trabajos de implantación de la oleaginosa, pero los pronósticos de los próximos meses dan cuenta de la presencia de un clima seco, lo que también impacta en el precio de la oleaginosa, sostuvo Di Yenno.

Otro punto central que no solo explica esta suba, sino la que se viene dando desde hace ya meses, es la fuerte demanda de China, principalmente a Estados Unidos.

Esta situación, para la corredora de granos Grassi, fue el principal motivo de la suba de los meses previos, por "el dinamismo en la demanda externa" ya que "China estaría comprando soja americana y se destacan negocios con Brasil".

Para el especialista de la BCR, "China va a ser para el 2021 el principal motor de la recuperación mundial y en esto hay dos cuestiones: por un lado el incremento de la demanda, la decisión de China de comprar y acumular stocks y, por otro lado, los dólares que están dando vueltas en el mundo, dada la super emisión que hemos tenido por la pandemia, tarde o temprano van a terminar yendo a los commodities".

"O sea, China en vez de ahorrar en dólares o en bonos, termina comprando commodities y eso es lo que se está viendo en el mercado", sintetizó Di Yenno.

Esta fuerte demanda china de soja estadounidense impactará, según los especialistas, también en los stocks norteamericanos y en sus proyecciones de exportación y la cuestión podría verse reflejada en el próximo informe de oferta y demanda mundial de granos del Departamento de Agricultura de Estados Unidos (USDA) que se publicará el 10 de noviembre.

A modo de cierre, desde la corredora Grassi señalaron que los operadores "comienzan a descontar un panorama de menores stocks finales" de Estados Unidos, junto con un incremento en las exportaciones y un recorte en la estimación de producción para el actual ciclo en el país del Norte.