---
category: Deportes
date: 2021-07-25T06:00:02-03:00
thumbnail: https://assets.3dnoticias.com.ar/JJOO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Cómo les fue a los atletas argentinos durante la primera jornada de competencia
title: Cómo les fue a los atletas argentinos durante la primera jornada de competencia
entradilla: El siguiente es un resumen de resultados en distintas disciplinas durante
  la primera jornada de competencia en los Juegos Olímpicos Tokio 2020

---
**Tiro:** Fernanda Russo, en carabina 10m., quedó 40ma. en la clasificación inicial.

**Beach Volley:** Azaad-Capogrosso 0 - Alison-Alvaro (Brasil) 2

**Handball:** Argentina 27 - Francia 33 (masculino)

**Judo:** Paula Pareto fue eliminada en repechaje -48kg. Recibió un diploma olímpico.

**Beach Volley:** Gallay-Pereyra 0 - Agatha-Duda (Brasil) 2

**Remo:** Milka Kraljev-Evelyn Silvestro en scull doble ligero quedaron sextas y últimas en su serie. Disputarán un repechaje mañana.

**Ciclismo de ruta:** Eduardo Sepúlveda no clasificó.

**Tenis**: Diego Schwartzman-Facundo Bagnis perdieron ante Kevin Krawietz-Tim Puetz (Alemania) por 6-2 y 6-1 en la primera ronda del doble masculinoHockey: Argentina 1 - España 1 (masculino).

**Tenis**: Andrés Molteni-Horacio Zeballos perdieron ante Jamie Murray-Neal Skupski (Gran Bretaña) por 7-6 (3), 4-6 y 13-11 en la primera ronda del dobles masculino.

**Vóleibol:** Argentina sucumbió con Rusia por 3-1 (25-21, 23-25, 17-25 y 21-25) en el debut del grupo B. El lunes, a las 9.45, chocarán con Brasil.

**Taekwondo:** Lucas Guzmán perdió en la pelea por el bronce con el Mijaíl Artamónov por 15-10, fue eliminado en -58kg y recibió diploma olímpico.

**Boxeo:** Mirco Cuello venció a Hamsat Shadalov (Alemania) por puntos en la categoría 57kg. peso pluma y pasó a octavos de final.

**Tenis de mesa:** Horacio Cifuentes derrotó a Yoshua Van Shing (Vanuatu) por 4-0 (11-2, 11-5, 12-10 y 11-6), clasificó a la segunda ronda y enfrentará a Chin Yuan Chuang (Taiwán).

**Boxeo:** Brian Arregui no pudo con Delante Johnson (EEUU) y sucumbió por puntos en la preliminar 63-69 kg. Quedó eliminado.

**Tenis de mesa:** Gastón Alto perdió ante Álvaro Robles (España) por 4-1 (8-11, 11-14, 14-12, 11-2 y 11-7) y fue eliminado en primera ronda.

**Natación:** Virginia Bardach terminó tercera en su carrera y 17ma. en la tabla general en las eliminatorias 400 combinados con un tiempo de 5:01.98. Quedó eliminada.