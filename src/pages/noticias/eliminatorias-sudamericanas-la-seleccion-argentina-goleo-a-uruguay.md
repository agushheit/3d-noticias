---
category: Deportes
date: 2021-10-11T06:00:38-03:00
thumbnail: https://assets.3dnoticias.com.ar/argentina.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: TyC Sports
resumen: 'Eliminatorias Sudamericanas: la Selección Argentina goleó a Uruguay'
title: 'Eliminatorias Sudamericanas: la Selección Argentina goleó a Uruguay'
entradilla: 'La Albiceleste volvió a jugar en Buenos Aires y con público: le ganó
  3-0 a la Celeste con goles de Lionel Messi, Rodrigo De Paul y Lautaro Martínez.'

---
Con goles de Lionel Messi, Rodrigo De Paul y Lautaro Martínez, la Selección Argentina goleó 3-0 a Uruguay, que tuvo desde el arranque a Nicolás De la Cruz, y vivió una noche inolvidable en el Estadio Monumental. Se jugó nuevamente con público, que disfrutó de un vibrante partido de punta a punta.

El equipo de Lionel Scaloni destrabó un partido que comenzó muy chivo en el Monumental por culpa de Luis Suárez, quien exigió a Emiliano Dibu Martínez en tres oportunidades. Primero fue un bombazo a quemarropa, luego una pirueta dentro del área chica y por último un disparo desde un ángulo cerrado que se estrelló en el palo.

Sin embargo, fue un grito de Messi desde bien adentro que abrió un marcador que costó y mucho, a los 38 minutos del primer tiempo. Y fue así porque Uruguay propuso un trámite bravo con un trabajo defensivo casi sin fisuras y con contras que estuvieron muy cerca del gol, todas de Suá

La Pulga se la tiró de cachetada a Nicolás González, uno de los que picó toda la noche, pero esta vez no alcanzó a conectarla y Fernando Muslera se comió el amague. La suerte jugó a favor para el 1-0 y para el gol número 80 (18 desde afuera del área) del capitán con la Selección Argentina. De ahí a los vestuarios fue todo para una Albiceleste que buscó incansablemente el arco rival.

Dos chances claras de Giovani Lo Celso, una que se estrelló en el travesaño y luego picó en la línea (imposible para Federico Valverde y compañía), un zurdazo de Lautaro Martínez que tapó abajo Muslera y finalmente el gol de Rodrigo De Paul (el segundo con esta camiseta) tras una distracción en la salida del elenco charrúa.

En el segundo tiempo, el entrenador Óscar Washington Tabárez mandó a la cancha a Edinson Cavani y Darwin Núñez para atacar con todo lo que tenía a disposición. Argentina asumió el riesgo -por momentos quedó mano a mano-, aunque sin consecuencias negativas porque en el arco tuvo a un Dibu Martínez muy encendido. La exhibición continuó y el tercero estaba a caer, solo era cuestión de minutos. Así lo hizo Lautaro Martínez.

Messi, el director de esta orquesta, abrió la cancha para De Paul y todo terminó en un pase gol para Lautaro al ras del piso. Fue una Selección Argentina armónica, con brillo individual, jugadas colectivas y variantes para llegar al ataque, mientras que Uruguay se mostró siempre impotente. Nadie pudo salvarlo del nocaut, ni siquiera sus jugadores de gran trayectoria ni los de gran actualidad.

Scaloni mandó a la cancha a Ángel Di María y el Monumental se vino abajo.