---
category: Agenda Ciudadana
date: 2021-06-27T06:15:36-03:00
thumbnail: https://assets.3dnoticias.com.ar/DESPIDOS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: El Gobierno prorrogó hasta el 31 de diciembre la prohibición de despidos
  y suspensiones
title: El Gobierno prorrogó hasta el 31 de diciembre la prohibición de despidos y
  suspensiones
entradilla: " El Poder Ejecutivo Nacional prorrogó por medio de un Decreto de Necesidad
  y Urgencia (DNU) la prohibición de los despidos y suspensiones hasta el próximo
  31 de diciembre, en el marco de la emergencia pública "

---
A través del DNU 413/2021, el Gobierno señaló además que "los despidos y las suspensiones que se dispongan en violación de lo dispuesto no producirán efecto alguno y se mantendrán vigentes las relaciones laborales existentes y sus condiciones actuales".

Además, en la normativa se remarca que "las prohibiciones previstas en el presente decreto no serán aplicables a las contrataciones celebradas con posterioridad a la entrada en vigencia del Decreto 34/19 -que declaró la emergencia laboral antes de la pandemia- ni respecto del personal que preste servicios en el ámbito del Sector Público Nacional, con independencia del régimen jurídico al que se encuentre sujeto y de la naturaleza jurídica de la entidad empleadora".

Quedan asimismo exceptuados y/o exceptuadas de tales prohibiciones, quienes se encuentren comprendidos y/o comprendidas en el régimen legal de trabajo para el personal de la industria de la construcción regulado por la Ley 22.250.

**Los fundamentos de la medida**

En sus fundamentos de la norma se reseña que la Organización Internacional del Trabajo (OIT) alude a "la necesidad" de que, en el contexto de la pandemia de coronavirus, "los gobiernos implementen medidas dirigidas a paliar los efectos nocivos en el mundo del trabajo, en particular en lo referido a la conservación de los puestos de labor".

Y en tal sentido recuerda la importancia de tener presente la Recomendación 166 de la OIT, que subraya “que todas las partes interesadas deberían tratar de evitar o limitar en todo lo posible la terminación de la relación de trabajo por motivos económicos, tecnológicos, estructurales o análogos, sin perjuicio para el funcionamiento eficaz de la empresa, establecimiento o servicio, y esforzarse por atenuar las consecuencias adversas de toda terminación de la relación de trabajo por estos motivos, para el trabajador o los trabajadores interesados”.

En el DNU, se insiste en que el organismo "ha llevado a cabo un análisis pormenorizado sobre las disposiciones fundamentales de las normas internacionales del trabajo pertinentes en el contexto del brote de la Covid-19, publicado el 27 de marzo de 2020, sosteniendo que las patologías contraídas por exposición en el trabajo a dicho agente patógeno podrían considerarse como enfermedades profesionales".

Y que, en ese marco, "diversos países han declarado que la afección por la Covid-19 producida por la exposición de los trabajadores y las trabajadoras al virus SARS-CoV-2 durante la realización de sus tareas laborales, reviste carácter de enfermedad profesional".

Se sostiene además que "las medidas que se establecen en el presente decreto son razonables y proporcionadas con relación a la amenaza y al riesgo sanitario que enfrenta nuestro país y se adoptan en forma temporaria, toda vez que resultan perentorias y necesarias para proteger la salud de determinados sectores de la población trabajadora particularmente vulnerable".