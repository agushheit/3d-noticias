---
category: La Ciudad
date: 2021-11-11T06:15:13-03:00
thumbnail: https://assets.3dnoticias.com.ar/candidatos.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Santa Fe: quién se va y quién se queda en el Concejo'
title: 'Santa Fe: quién se va y quién se queda en el Concejo'
entradilla: 'Son ocho las bancas que se renuevan en el Concejo de Santa Fe entre siete
  frentes que están en competencia este 14 de noviembre, en las elecciones generales

'

---
Otra vez comenzó la cuenta regresiva para ir a las urnas. Este domingo 14 de noviembre se llevan adelante las elecciones generales en las cuales se elegirán diputados y senadores nacionales, así como también, en el caso de la ciudad de Santa Fe, se deben renovar ocho bancas del Concejo Municipal.

Tras las Paso del 12 de septiembre pasado fueron siete listas, de las 17 que compitieron, las que pasaron a las generales de este 14 de noviembre. Para lograrlo, los frentes y partidos tuvieron que superar un umbral de votos para poder participar de la elección general. Eso está normado en la Ley 12.367, que establece para la categoría de concejales el umbral mínimo del 1,5 por ciento del padrón. Lo mismo para lograr entrar en el reparto por sistema D\`hondt de la interna cada lista tuvo que conseguir el 1,5 por ciento del total de los votos emitidos. De esa manera se conformaron las listas que los frentes presentarán en los comicios generales.

Otro de los puntos a tener en cuenta para la conformación final de las listas es que el ordenamiento de los candidatos se realiza contemplando la Ley 14.002 de paridad de género que se aplicará por primera vez en la provincia. Por lo tanto, cada dos lugares siempre tiene que haber una persona de cada género.

**Las listas para noviembre**

**Frente de Todos** quedó conformada de la siguiente manera: Jorgelina Mudallel, Alejandro Rossi, Marcela Aeberhard, Fernando Molina, Violeta Quiroz, Edelmiro Maidana, Natalia Ocampo y Lucas Maguid.

La lista de **Juntos por el Cambio** quedó integrada por Adriana Molina, Hugo Marcucci, Carlos Pereira, Luciana Ceresola, Celina Traverso, Carlos Suárez, Ángeles Álvarez y Ulrich Lehmann.

El **Frente Progresista** había presentado una única lista y así competirá en las generales: los ocho candidatos titulares serán Laura Mondino, Leandro González, Virginia Coudannes, Pablo Mainer, Lorena Martínez, Marcelo Visuara, María Belén Pereira y Mario Fernández.

La lista de **Unión Federal** está conformada por Juan José Piedrabuena, María Gimena Carmelé, Ricardo Arredondo, Alejandra Castro, Martín Rico, Sofía Calvo, Facundo Chaves y Lorena Fernández.

**Barrio 88**: Eliana Ramos, Juan Cruz Valdez, Sofía Amherdt, Pablo Almirón, Rosario Lencina, Tomás Galluccio, Gladis Jara y Facundo Lovato.

**"La lista Mejor”** de Saúl Perman, quien estará acompañado por María de los Milagros Bergagna, Pascual De Stefano, María Adela Recalde, Facundo Olmos, Anabela Femia, Lisandro Lafferriere y María Virginia Romero.

**Frente Federal Vida y Familia** solo “clasificó” la lista VOS (Valores, Objetivos, Soluciones). Quedará integrada por: Maximiliano Puigpinos, Alicia Velázquez, Eduardo Armas Belavi, Matilde Ponce, Martín Ballesta, Adelina Pizzarulli, Waldo Zeballos y Consuelo Vigo.

![Así es la boleta para elección de Concejales en la ciudad de Santa Fe que estará en el cuarto oscuro](https://media.unosantafe.com.ar/p/e6a0b44ef8264986a5b54afcbed31b2a/adjuntos/204/imagenes/030/714/0030714978/boleta-concejales-generales-2021jpg.jpg?2021-11-08-13-00-39 =453x642)

Así es la boleta para elección de Concejales en la ciudad de Santa Fe que estará en el cuarto oscuro

Tribunal Electoral Santa Fe

**Quiénes se van y quiénes se quedan**

**Terminan su mandato**

• Leandro González: UCR - Frente Progresista

• Luciana Ceresola: PRO - Juntos por el Cambio

• Jorgelina Mudallel: Partido Justicialista

• Laura Mondino: Santa Fe Puede Más - Frente Progresista

• Carlos Pereira: UCR - Juntos por el Cambio

• Sebastián Pignata: Frente Renovador - Juntos

• María Laura Spina : UCR - Frente Progresista

• Carlos Suárez: UCR

**Continúan con su banca**

• Mercedes Benedetti: Santa Fe Puede Más - Frente Progresista hasta 2023

• Federico Fulini: Partido Justicialista - Unidad Ciudadana hasta 2023

• Julio Garibaldi: Santa Fe Puede Más - Frente Progresista hasta 2023

• Guillermo Jerez: Barrio 88 hasta 2023

• Inés Larriera: UCR - Juntos por el Cambio hasta 2023

• Valeria López Delzar: CREO - Frente Progresista hasta 2023

• Sebastián Mastropaolo: PRO - Juntos por el Cambio hasta 2023

• Juan José Saleme: Partido Justicialista hasta 2023

• Lucas Simoniello: UCR - Frente Progresista hasta 2023