---
category: Agenda Ciudadana
date: 2021-09-19T06:00:40-03:00
thumbnail: https://assets.3dnoticias.com.ar/PRIMAVERA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Definirán controles para evitar un "desmadre" en los festejos de Primavera
title: Definirán controles para evitar un "desmadre" en los festejos de Primavera
entradilla: Funcionarios del área de Control del municipio y autoridades de la URI
  coordinarán un operativo de seguridad. Habrá inspecciones en la Costanera, en la
  zona de la Costa y en lugares de alta concurrencia pública.

---
Este martes 21 de septiembre comienza la primavera y se celebra el Día del Estudiante. Con la liberalización de muchas actividades y de horarios de circulación, hoy la ciudad volvió a algo parecido a su rutina “pre pandémica”: a diario puede verse el alto movimiento de personas en los principales puntos de concentración pública.

Si bien los indicadores que marcan el ritmo de contagios de coronavirus están bajos y estables, la posibilidad de un “desmadre” de festejos con aglomeraciones de personas (que podría llevar a nuevas infecciones) está presente en este contexto, sumado al consumo excesivo de alcohol, que derivaría en problemas dentro de la vía pública.

Ante esto, el próximo lunes por la mañana funcionarios del área de Control de la Municipalidad local se reunirán con autoridades de la Unidad Regional I (URI) para afinar un operativo conjunto de control y de seguridad.

De momento, fuentes del Gobierno local se limitaron a adelantar que se priorizará el control con agentes municipales en los lugares de mayor circulación vehicular y de concentración pública desde este lunes por la tarde y durante todo el martes. “Se pondrá el foco en la Costanera, la zona de la Costa y de quintas, y puntos urbanos que tradicionalmente son de concurrencia por parte de los jóvenes”, dijeron.

En la reunión, además del municipio y de la URI, podrían participar autoridades políticas de localidades de la zona, como Rincón y Sauce Viejo. No obstante, los detalles finos del operativo de control que se desplegará se conocerán el lunes**.**