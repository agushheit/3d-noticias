---
category: Agenda Ciudadana
date: 2021-02-12T09:27:24-03:00
thumbnail: https://assets.3dnoticias.com.ar/economia.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Clarin
resumen: La curiosa explicación del Ministerio de Economía sobre la carrera precios-salarios
  de 2020
title: La curiosa explicación del Ministerio de Economía sobre la carrera precios-salarios
  de 2020
entradilla: Hacienda señaló los ingresos de los trabajadores aumentaron 37% promedio
  anual y que es mayor a la inflación anual de diciembre pasado de 36,1%.

---
Voceros del Ministerio de Economía afirman que en 2020 los salarios aumentaron 37% promedio anual, mientras que a diciembre la inflación interanual fue 36,1%. En ambos casos son cifras que se desprenden del INDEC.

Dicho así, pareciera que el año pasado los salarios le aventajaron a la inflación. Pero no es así.

Para que la comparación salarios/inflación sea homogénea, ambas variables deben corresponder al mismo período. “Peras con peras y manzanas con manzanas”. Y es lo que no sucede con la comparación de la suba de 37% de los salarios versus 36,1% de la inflación.

La inflación interanual a diciembre – punta a punta 36,1%—difiere de la inflación promedio anual (42%) porque abarca períodos diferentes.

La inflación interanual refleja la variación de los precios, en este caso, de diciembre 2020 con los que el INDEC relevó en diciembre de 2019. Compara el índice de diciembre de un año contra el índice de diciembre del año anterior.

En cambio la inflación anual permite conocer la variación promedio de los precios durante los 12 meses de 2020 respecto de los 12 meses de 2019. Esto significa que hay que sumar los 12 índices del año, obtener el promedio y compararlo con el promedio de los 12 índices del año anterior.

Así, el INDEC informó que a diciembre los salarios tuvieron un alza interanual del 33% frente a una inflación interanual del 36,1%.

¿Y el 37% de incremento salarial que afirman las fuentes de Economía?

Ese 37% corresponde al índice salarial de 2020 versus el promedio de 2019. “Si uno toma los índices publicados por INDEC, el índice promedio del año 2020 correspondiente al total índice de salarios es de 282,82 mientras que ese promedio para el año 2019 es de 206,37. La variación porcentual entre ambos valores arroja un aumento de 37,0%”, calcularon en Economía.

Pero a renglón seguido el comunicado de Economía no dice a cuánto ascendió la inflación promedio del año pasado, lo que lleva a la confusión sobre el resultado final.

De los índices publicados por INDEC, surge que el índice de inflación promedio de 2019 es de 232,8 y el de 2020 de 330,5. Eso representa un incremento del 42%.

En consecuencia, mientras en la medición interanual los salarios (33%) perdieron 3,1 puntos frente a la inflación (36,1%), en la medición promedio anual la pérdida es de 5 puntos: 42% frente al 37%.

Con distinta intensidad, ambas mediciones arrojan que en 2020 los salarios perdieron la carrera frente a la inflación