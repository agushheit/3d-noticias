---
category: La Ciudad
date: 2021-01-20T09:21:35Z
thumbnail: https://assets.3dnoticias.com.ar/clubes.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad de Santa Fe
resumen: La Municipalidad visitó más de 100 clubes durante el 2020
title: La Municipalidad visitó más de 100 clubes durante el 2020
entradilla: En 55 casos se entregaron aportes económicos para paliar la pandemia y
  se colaboró en el diseño de proyectos junto a las instituciones. Además se realizó
  el seguimiento de los protocolos ante el Covid-19.

---
En un año difícil, atravesado por la pandemia de coronavirus, la Municipalidad se abocó al acompañamiento y la interacción con los clubes de la capital provincial. Es que en la nueva normalidad, el deporte y sus instituciones son considerados vitales para el crecimiento y el desarrollo de los vecinos y vecinas de la ciudad.

Luego de la conformación del Consejo Municipal del Deporte (Comude) y tras el cierre temporario de las instituciones deportivas por efectos de la pandemia, la Dirección de Deportes llevó a cabo varios encuentros virtuales para analizar la compleja situación. En el marco de la incertidumbre imperante, esos contactos permitieron mantener una comunicación estrecha con referentes y dirigentes de los clubes.

Con ese objetivo se realizaron más de 100 visitas a las sedes de las entidades, durante todo el 2020, lo cual se sumó a los aportes económicos entregados a 55 instituciones deportivas, en el marco del paquete de medidas que implementó el intendente Emilio Jatón para apoyar a las actividades económicas que padecieron los efectos del aislamiento preventivo.

El director ejecutivo de Deportes del municipio, Adrián Alurralde, recordó que entonces la situación era preocupante, ya que todas las instituciones, en mayor o menor medida, habían perdido parte de su masa societaria. Sin embargo, destacó que mantuvieron “intacta su fortaleza para salir adelante, más allá de las adversidades” y subrayó “el esfuerzo y la labor denodada de sus dirigentes” para alcanzar esa meta.

Alurralde destacó que “la generosidad fue notable de parte de algunos clubes para brindar sus instalaciones desde el comienzo de la pandemia para alojar personas y colaborar con el sistema sanitario”. Mientras tanto, el municipio colaboraba en la implementación de diversas estrategias para promover actividades virtuales.

Una vez que se iniciaron las reaperturas de manera progresiva, las entidades mostraron gran responsabilidad en el cumplimiento de los protocolos establecidos. “La idea fue que para retomar las actividades deportivas y rescatar a los deportistas se debía trabajar en conjunto y tuvimos una gran receptividad”, apuntó el director.

El gran desafío para el año que comienza es profundizar el trabajo mancomunado entre el municipio y las instituciones para recuperar sus economías y a sus deportistas. Para esto, el compromiso es aunar esfuerzos y respaldar a cada entidad, ya sea desde la logística o el recurso material. Con ese objetivo continuarán las visitas a las asociaciones deportivas, como modalidad de trabajo.

Alurralde aseveró que “los clubes son un saludable ámbito de educación y contención para el crecimiento integral y colectivo de nuestra sociedad, las infancias y juventudes; son el lugar donde nuestros mayores disfrutan de actividades para una vida plena; y son el espacio donde el juego, el deporte y la recreación se convierten en medios para la convivencia y la construcción de ciudadanía”.

**Municipalidad presente**

La tarea de la Dirección de Deportes, que depende de la Secretaría de Integración y Economía Social, se basó en el acompañamiento a las instituciones en diversas instancias.

Por decisión del Ejecutivo, se efectuaron aportes económicos de $ 50.000 para paliar, en parte, la difícil situación de los clubes barriales. En ese sentido, 55 instituciones recibieron fondos genuinos de parte del municipio. Además, se otorgó ayuda alimentaria y de logística a aquellos clubes inmersos en contextos menos favorables.

Como proyecto a mediano plazo, la Municipalidad presentó la iniciativa que crea el Observatorio de clubes y actividades físicas de la capital provincial. La oficialización se completó durante la última reunión del Comude.

Se trata de un plan inédito en la región que propone la fundación de un observatorio encargado de concretar estudios y trabajos multidisciplinarios para mejorar la gestión deportiva. La intención es proyectar esas actividades en un plano de equidad e igualdad de oportunidades y derechos con visión estratégica. De este modo se facilitará la gestión del conocimiento relacionado con el deporte y la actividad física, a partir del uso de las nuevas tecnologías.