---
category: La Ciudad
date: 2021-05-28T08:23:56-03:00
thumbnail: https://assets.3dnoticias.com.ar/virtualidad.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad de Santa Fe
resumen: " Personas mayores: capacitaciones para que la virtualidad no sea una barrera"
title: " Personas mayores: capacitaciones para que la virtualidad no sea una barrera"
entradilla: La Municipalidad invita a participar de diversos talleres destinados a
  acompañar durante la pandemia, y enseñar diferentes actividades.

---
En el contexto de pandemia, la Municipalidad lleva adelante diferentes acciones con el propósito de fortalecer las relaciones familiares, comunitarias y sociales de las personas mayores y las distintas organizaciones e instituciones que se vinculan con la temática, como por ejemplo, los centros de jubilados, entre otras.

En este marco, continúan las propuestas que este año incluyen, por un lado, un Ciclo de Talleres Integrales denominado “Encuentros Mayores” y, por otro lado, la segunda edición de las “Micropropuestas Educativas y Culturales” destinadas a personas mayores y llevadas a cabo por equipos docentes y referentes territoriales mediante la celebración de un convenio con la Universidad Nacional de Entre Ríos (Uner).

Cabe destacar que en el 2020 participaron 716 personas de las siete ediciones de diferentes cursos y talleres orientados a adultos mayores. En tanto, hasta el momento, participan unas 143 personas de los primeros talleres que comenzaron a dictarse este año. En tal sentido, hay 42 inscriptos a la micropropuesta de manejo de celulares, redes y plataformas y 101 en las actividades virtuales de “Encuentros Mayores”.

Diego Piedrabuena, subdirector de Promoción de Derechos y Autonomía de Personas Mayores de la Municipalidad de Santa Fe, consignó: “Durante la primera ola vimos que las personas más afectadas por el aislamiento eran los adultos mayores. Por tal motivo, lo que buscamos generar con estas acciones es reforzar los vínculos, el acompañamiento”.

Desde los talleres se hace especial hincapié en la enseñanza de aplicaciones que permitan comunicarse de manera virtual. “Muchas de las propuestas tienen que ver con el manejo de la tecnología, porque notamos que uno de los impedimentos que tienen las personas mayores es el uso o el manejo de los teléfonos o las computadoras”, señaló el funcionario.

En esa línea, Piedrabuena indicó que el objetivo es superar una simple llamada telefónica “se busca que estos vínculos se mantengan y refuercen. Y que las personas que están en etapa de envejecimiento puedan hacerlo de manera activa y productiva. Las micropropuestas son instancias de participación, donde se los escucha, se les pide su parecer, su opinión”.

**Fundamentación**

Frente a la continuidad del distanciamiento social obligatorio por la propagación del Covid-19, desde la Municipalidad se considera prioritario repensar el acompañamiento de las personas mayores ante la necesidad de que permanezcan en sus hogares como medidas de prevención y cuidado.

Lo que antes se realizaba de manera presencial, en esta nueva realidad, es necesario experimentarlo a través de la virtualidad desde el uso de diferentes tecnologías. Por tal motivo, el municipio sostiene que el “quedarse en casa” no implica pasividad, sino por el contrario, supone incorporar prácticas de cuidado para alimentarse en forma saludable,  fortalecer la condición física y capacidad funcional, fomentando el aprendizaje a través de ejercicios cognitivos, lectura, música para contribuir con entornos de cercanía, afectividad y contención.

En este contexto, estas propuestas tienen como objetivo facilitar el fortalecimiento de vínculos afectivos o sociales y brindar servicios de proximidad mediante actividades de estimulación cognitiva, deportivas, recreativas, educativas y culturales, por considerarse fundamentales e imprescindibles como factores protectores para las personas mayores durante el desarrollo de las tareas cotidianas y  para mantener una calidad de vida saludable.

**Encuentros Mayores**

El ciclo de talleres integrales denominado “Encuentro Mayores” es un conjunto de actividades semanales que se proponen para la participación de personas de 60 años en adelante, aunque también se incluyen a personas que se acerquen a esa edad. En estos talleres se ofrecen diferentes prácticas y materiales que facilitan y acompañan el proceso y desarrollo de las propuestas para integrarlas en la dinámica cotidiana de sus hogares.

El objetivo central de este ciclo es ofrecer un servicio de acompañamiento y asesoramiento virtual con cercanía y proximidad que supere a la instancia telefónica habitual teniendo en cuenta las diversas formas de envejecer y la necesidad de ampliar las oportunidades para la sociabilidad y participación de las personas mayores, fortaleciendo la solidaridad y la reciprocidad entre generaciones como prácticas fundamentales para el desarrollo del bienestar.

Los destinatarios son personas adultas mayores y familiares, organizaciones vinculadas a la temática y ciudadanía en general. En esta edición, cuentan con las siguientes variantes: Talleres de construcción de elementos de apoyo (adaptación de elementos para la motricidad fina); Nutrición y alimentación saludable; Cuidados integrales: salud mental y emocional en contexto de pandemia y Yoga para el bienestar; Sexualidad y vejez  en perspectiva de género; Consumos Problemáticos; Envejecimiento activo y productivo; Actividades de expresión artística y literaria.

Los talleres son semanales y se pueden inscribir de manera telefónica -342-6109633- o a través del correo electrónico: areaadultosantafeciudad@gmail.com.

**Micropropuestas**

Por otra parte, se llevará adelante la segunda edición de micropropuestas educativas y culturales, que organiza el Departamento de la Mediana y Tercera edad de la Facultad de Ciencias de la Educación de la UNER, en forma conjunta con la Municipalidad de Santa Fe y la Dirección de Adultos Mayores del Ministerio de Desarrollo Social de Entre Ríos.

Las mismas no pretenden ser cursos o talleres virtuales, sino que  son una invitación a recorrer una temática a través de diversas actividades propuestas por cada docente, utilizando diferentes recursos: escritos, sonoros, audiovisuales y/o gráficos.

Las micropropuestas tendrán una duración de un mes con dos encuentros virtuales con el profesor. La finalidad de estas instancias será realizar una puesta en común e intercambio sobre lo trabajado.

Este año, las propuestas son: Manejo de redes y celular; Salud de entrecasa; Alimentación; Estimulación Cognitiva; Actividad Física; Taller de Escritura; y Taller de Teatro.

**Informes e inscripción**

Dirigirse a la Dirección de Adultos Mayores de la Municipalidad de Santa Fe, de 8 a 18 horas:

–       Teléfono fijo: 342-4571844

–       Teléfono celular: 342-6109633

–       Correo electrónico: areaadultosantafeciudad@gmail.com