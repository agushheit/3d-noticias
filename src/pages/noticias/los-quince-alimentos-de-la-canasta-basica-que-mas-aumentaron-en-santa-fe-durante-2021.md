---
category: Agenda Ciudadana
date: 2022-01-19T06:15:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/INFLACION.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Los quince alimentos de la canasta básica que más aumentaron en Santa Fe
  durante 2021
title: Los quince alimentos de la canasta básica que más aumentaron en Santa Fe durante
  2021
entradilla: 'Los incrementos llegan hasta el 82% interanual en algunos alimentos,
  con un 53,5% de aumento en este rubro. Esto se ubica por encima de la inflación
  general, que arrojó un 50,7% según el Ipec.

  '

---
Comunicados los números de la inflación anual de Santa Fe vuelve a salir a la luz lo caro que es acceder a los alimentos de la canasta básica. Este rubro incrementó sus precios en un 53,5% promedio con respecto a diciembre de 2020, con aumentos que llegan hasta el 82% de manera interanual en algunos productos alimenticios.

Comparando los índices del Instituto Provincial de Estadística y Censos (Ipec) respecto a la inflación de diciembre 2020 y 2021, se divisan fuertes aumentos en la mayoría de los productos alimenticios, algunos de manera sumamente pronunciada.

Difiriendo entre los quince productos con mayor aumento interanual en Santa Fe se distinguen el queso cremoso, cerveza, aceite de girasol, carne picada, leche entera, yerba, asado, pollo entero, pan francés, arroz blanco, harina de trigo, azúcar, fideos secos, agua sin gas y huevos.

En un contexto en donde la inflación interanual acumulada durante 2021 fue del 50,7% según el Ipec, varios productos alimenticios registraron aumentos por encima de este índice. El queso cremoso fue el alimento con la mayor suba porcentual en su precio, aumentando un 82% de diciembre de 2020 a diciembre de 2021. Segunda quedo la cerveza de litro (o porrón) en donde su precio aumentó un 70% en un año según el organismo provincial.

A estos dos productos les siguen el aceite de girasol y la carne picada (ambos con un 65% de aumento interanual) completando de esta manera el podio. Luego prosiguen en la lista el litro de leche entera, la yerba y el asado, todos estos insumos superando el 50,7% de aumento a nivel general en la inflación.

**Números de diciembre**

Las categorías donde se registró una mayor variación con respecto a noviembre son Otros bienes y servicios con un aumento del 5% (donde se observan cigarrillos y accesorios, artículos y servicios para el cuidado personal y servicios diversos); e Indumentaria, con un incremento del 4,8%.

La menor variación se vio en Atención médica y gastos para la salud con el 1%, que incluye productos medicinales, accesorios terapéuticos y servicios médicos.

Por otro lado, se destaca la variación de precios en Transporte y comunicaciones, con un aumento del 2,4%, donde se incluyen:

* Transporte 4,5%
* Transporte público de pasajeros 10,8%
* Adquisición de vehículos 5,1%
* Funcionamiento y mantenimiento de los vehículos 2,6%
* Combustibles y lubricantes 0,2%