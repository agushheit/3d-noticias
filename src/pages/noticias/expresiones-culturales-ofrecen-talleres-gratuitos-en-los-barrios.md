---
category: La Ciudad
date: 2021-01-21T09:04:02Z
thumbnail: https://assets.3dnoticias.com.ar/cultura.png
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad de Santa Fe
resumen: 'Expresiones culturales: ofrecen talleres gratuitos en los barrios'
title: 'Expresiones culturales: ofrecen talleres gratuitos en los barrios'
entradilla: Durante el verano, la Municipalidad brinda actividades vinculadas a la
  danza, la música, la lectura y oficios en distintos puntos de Santa Fe capital para
  todas las edades.

---
La Municipalidad, a través de la Secretaría de Educación y Cultura, brinda talleres gratuitos en distintos barrios de Santa Fe Capital. En las Estaciones y lugares emblemáticos, se realizan actividades vinculadas a la música, la danza, la lectura y el aprendizaje de oficios.

Las propuestas se dictan en las Estaciones de La Guardia, Barranquitas, Coronel Dorrego y CIC de Facundo Zuviría, además del Mercado Progreso, la explanada de la Estación Belgrano y Candioti Park.

De esta manera, la Municipalidad descentraliza el acceso a actividades recreativas al aire libre con estricto protocolo sanitario, que incluye inscripción previa en cada actividad y el uso de burbujas para garantizar el distanciamiento social. Las inscripciones se realizan en los espacios donde se dictan los talleres.

**En el Centro y Este**

En el Mercado Progreso (Balcarce 1635) se dictan clases de danza y movimiento para jóvenes y adultos, los martes y viernes, de 9.30 a 11 horas. También, folclore para adultos, los martes y jueves, de 19 a 21.

Por su parte, en la explanada de la Estación Belgrano (Bulevar Gálvez 1150) se dan clases de folclore para adultos al aire libre los domingos, a las 19, y tango para adultos los martes y jueves, de 18 a 19.30.

En Candioti Park (Rosalía de Castro 1882) se dicta un taller de skate destinado a las infancias y juventudes, los martes y jueves, de 17 a 20.

En la Estación La Boca, los martes y jueves se desarrolla la propuesta “Lecturas a la sombra” en el horario de 10 a 12 para las infancias.

Marroquinería en Eco Reciclado se brinda martes y jueves, de 10 a 12, en la Estación Dorrego (Larrea 1735) para jóvenes y adultos.

Además, en la Dirección de Deportes (Almirante Brown 5294) ofrecen tango para adultos al aire libre, los martes y jueves, a las 21.

**En el Oeste y el Norte**

En la Estación Mediateca (Tucumán y Pasaje Mitre) se realiza un taller integral de Hip Hop para jóvenes, los martes y jueves, a las 15. En el mismo lugar, se invita a participar del taller de producción de objetos decorativos los lunes, miércoles y viernes, de 17.30 a 19, destinado a las infancias y juventudes.

En la Estación Barranquitas (Iturraspe y Estrada) se dicta el taller de Circo Integral los martes y jueves, de 16 a 18, para jóvenes.

En la Estación CIC de Facundo Zuviría (Facundo Zuviría 8002) se ofrece un taller de guitarra y percusión los martes, de 16 a 18, y los miércoles, de 10 a 12, tanto para jóvenes como para adultos.