---
category: Estado Real
date: 2021-01-10T10:34:48Z
thumbnail: https://assets.3dnoticias.com.ar/100121-Perotti-intendentes.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: 'Restricciones Nocturnas: intendentes piden que el MPA y la Justicia acompañen
  los controles'
title: Intendentes y presidentes comunales piden que la Justicia y el MPA acompañen
  los controles
entradilla: El pedido fue elevado durante el encuentro virtual que Perotti mantuvo
  este viernes, en el marco de las nuevas medidas que la provincia dispondrá por la
  pandemia de Covid 19.

---
El gobernador Omar Perotti, junto con la ministra de Salud, Sonia Martorano encabezaron este viernes dos importantes encuentros. 

En primer lugar, mantuvieron una **reunión virtual con representantes del Comité de Expertos en Salud que asesora a las autoridades provinciales** en el marco de la pandemia de Covid 19; y en una segunda videoconferencia, el diálogo fue con intendentes y presidentes comunales **para analizar la adecuación de la provincia a las medidas dispuestas por la Nación frente a la pandemia**.

En la oportunidad, el gobernador afirmó: «hemos hecho esfuerzos importantes que queremos cuidar. Fortalecimos el sistema de salud en todas las localidades, aumentando considerablemente el número de camas».

Seguidamente el mandatario provincial reconoció que «hay una percepción de relajamiento con los cuidados básicos. Nosotros vamos en otro sentido. Tenemos que reforzar las conductas de prevención, optimizando mensajes y acciones. Por eso, buscamos consensuar horarios y vamos a generar todas las acciones de concientización necesarias. **El objetivo es mantener el mayor nivel de actividad posible restringiendo circulación nocturna**. Tenemos que seguir apuntalando la responsabilidad del cuidado».

En ese marco, el titular de la Casa Gris explicó que «lo que hoy **se evaluó y está sujeto a revisión hasta tanto se defina el contenido del decreto, es la posibilidad que de domingo a jueves las actividades se extiendan hasta las 00:30 horas y los viernes y sábados hasta la 01:30 horas en toda la provincia**. Además, se habilitaría a los municipios y comunas a tener la facultad de determinar su propio horario de culminación de actividades. También se avanzó en que, en el caso de que requieran hacer restricciones según la situación epidemiológica de su localidad, puedan hacerlo».

Perotti añadió que «el Ministerio de Salud también podría hacer observaciones sobre la base de criterios epidemiológicos sobre la situación de tal o cual lugar y en virtud de ello pueda solicitar medidas restrictivas para esos lugares».

Respecto a la restricción de circulación durante la noche, el mandatario provincial resaltó que **«hubo un pedido unánime de los intendentes y presidentes comunales para que el Ministerio Público de la Acusación (MPA) y la Justicia acompañen los controles que realicen las fuerzas de seguridad de la provincia».**

Por último, Martorano hizo hincapié en el operativo de vacunación. «Estamos preparados para ponerlo en marcha de nuevo en toda la provincia de Santa Fe con la colaboración de los municipios y comunas, a partir de un invalorable esfuerzo en conjunto. Nos interesa continuar observando cómo sigue la curva de casos y evaluar como continuar en ese contexto», concluyó la ministra de Salud.

De las videoconferencias participaron, además, el secretario de Salud de la provincia, Jorge Prieto y el subsecretario de Comunas, Carlos Kaufmann.