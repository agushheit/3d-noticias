---
category: Deportes
date: 2021-05-16T06:00:57-03:00
thumbnail: https://assets.3dnoticias.com.ar/colon.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Colón le ganó a Talleres por penales y es semifinalista
title: Colón le ganó a Talleres por penales y es semifinalista
entradilla: De arriba Talleres fue el que “asustó” ni bien comenzado el partido. Pero
  Colón no tardó en reaccionar, minutos después, con un tiro libre del “Pulga” Rodríguez
  que, con lo justo, Marcos Díaz mandó al corner.

---
Antes de los 4 minutos, el primero de las manos en favor de Burián, negándole el gol a Fragapane, quien inmediatamente después tuvo otra acción destacada tras un corner y un centro que no llegó a destino.

Todo esto, y solo iban 7 minutos. Hasta que, a los 13 minutos, centro de Rodríguez desde la izquierda, y Alexis Castro que saltó más alto que todos puso el 1 a 0.

A los 20 segundos mano a mano con el que se queda Burián. Un minuto después, buen remate de Auzqui que controló en dos tiempos el arquero de Colón.

Pero de tanto insistir y exigir al arquero sabalero, el ex Unión, Franco Fragapane, esta vez se quedó con el duelo ante el arquero sabalero y puso el empate.

Hasta el final de la primera etapa, solo se repitieron algunas llegadas más de la visita. Un centro de Tenaglia donde la pelota se desvió y Burián encontró. Y luego, otra vez el 1 sabalero que apareció para controlar un tiro libre de Mac Allister.

**Segundo tiempo**

El complemento tuvo acción rápido. Y fue negativa para Colón. A los 5 minutos, segunda tarjeta amarilla para Eric Meza que tuvo que dejar el campo de juego y a su equipo con 10.

Recién a los 22 minutos hubo otro momento desafortunado en el partido. En esta oportunidad, en contra de Talleres. Salida con la mano de Marcos Díaz que queda corta, su compañero Mauri intentó llegar, pero la pierna se le fue muy larga e impacto contra un rival. Roja directa. 10 contra 10, y el encuentro que seguía en empate, ahora también en cantidad de jugadores en cancha.

Luego no hubo jugadas de gran peligro, por lo que todo indicaba que el partido se iba a definir desde los 12 pasos. Y fue así…

**Los penales**

Para Colón convirtieron: Castro, Aliendro, Piovi, Lértora y Rodríguez

Por Talleres convirtieron: Fragapane, Retegui y Soñora. Burián le atajó a Rafael Pérez.