---
layout: Noticia con imagen
author: .
resumen: Prevención de Inundaciones
category: Estado Real
title: La Provincia transfirió fondos al municipio santafesino  para el
  mantenimiento y reparación de las estaciones de bombeo
entradilla: Se trata de un aporte no reintegrable que el gobierno provincial
  otorgó a favor del municipio de la capital santafesina para la protección
  contra inundaciones por lluvias.
date: 2020-11-10T15:11:43.437Z
thumbnail: https://assets.3dnoticias.com.ar/bombeo-estaciones.jpg
---
El pasado 13 de octubre, se firmó el Decreto N° 1049/20 que lleva la firma del gobernador Omar Perotti y los ministros de Economía, Walter Agosto y de Infraestructura, Servicios Públicos y Hábitat, Silvina Frana, en el cual el gobierno provincial dispuso un aporte no reintegrable a la municipalidad de Santa Fe por un monto de $4.700.625, para tareas de mantenimiento y reparación del sistema de bombeo que posee la ciudad –que involucra las 6 motobombas- para protección contra inundaciones por lluvias.

En este sentido, la ministra Silvina Frana habló acerca de la importancia de este aporte provincial: “Hoy estamos transfiriendo el aporte a la municipalidad de Santa Fe. Y como santafesina tengo en claro lo que significa tener operativas y en buenas condiciones las estaciones de bombeo de la ciudad, porque de esta manera la vamos a estar mejor preparados para atender contingencias y disminuir el riesgo hídrico”.

Y agregó: "Hay una problemática en la ciudad de Santa Fe que debe ser atendida con recursos concretos. Y esta es una de las tantas muestras que da el gobernador Omar Perotti, de poner por delante las necesidades de los santafesinos, dejando de lado la identidad partidaria”.

"Hablamos de obras fundamentales para minimizar el riesgo de inundación a todo un cordón oeste de la ciudad que es absolutamente vulnerable. Estas son las inversiones que el gobierno de la provincia ejecuta mirando las prioridades de los municipios y comunas. Y nuestra agenda de trabajo está enfocada allí, en atender las problemáticas de las santafesinas y santafesinos".

**MANTENIMIENTO Y REPARACIÓN**

Consultado sobre los detalles de los trabajos que se realizarán con este aporte, el secretario de Recursos Hídricos, Roberto Gioria, explicó: “El aporte está destinado al servicio técnico de las 6 motobombas de la ciudad que hoy están funcionando al 25% de su capacidad y necesitan mantenimiento”.

Más adelante, Gioria se refirió a la otra parte del sistema que lo constituyen las electrobombas: “Van a requerir reparación y cambios en el cableado las electrobombas Nº 1 y 2 de la Estación Oeste, y la contratación de una grúa para la electrobomba Nº5 que requiere un cambio total del equipo”.

Finalmente, el funcionario agregó que se proveerá de elementos y tableros eléctricos para todo el sistema de bombeo, tanto en las estaciones del cordón Oeste y en las estaciones de la Costa, como en aquellas a las que se equipa con bombas durante eventos de emergencia, para la correcta protección ante cambios de tensión eléctrica.