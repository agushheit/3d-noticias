---
category: La Ciudad
date: 2021-01-13T07:50:29Z
thumbnail: https://assets.3dnoticias.com.ar/13121-controles.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: '3DNoticias - Fuente: UNO Santa Fe'
resumen: 'Restricciones de circulación: aplicarán controles semanales y operativos
  de prevención los fines de semana'
title: 'Restricciones de circulación: aplicarán controles semanales y operativos de
  prevención los fines de semana'
entradilla: 'Este martes se realizó un encuentro para analizar los resultados del
  primer día del decreto que restringe la circulación nocturna. '

---
Este martes en horas del mediodía se llevó adelante un nuevo encuentro de la Mesa de Coordinación Institucional en Seguridad, la cual está compuesta por autoridades del Ministerio de Seguridad, referentes de la Municipalidad de Santa Fe, del Ministerio Público de la Acusación (MPA), la Policía de la provincia y Fuerzas Federales. 

El objetivo fue analizar los resultados de los controles luego del primer día del decreto provincial que restringe la circulación nocturna.

Durante la reunión se analizaron los puntos de mayor concentración de personas que se registran en la ciudad de Santa Fe y se agregaron nuevos controles en diversos puntos del ámbito urbano de la capital provincial. 

Del encuentro, participaron autoridades del MPA, y se les planteó la necesidad de incluir sanciones y procedimientos penales para aquellas personas que no cumplan las restricciones.

En este sentido, el coordinador del Ministerio de Seguridad, Facundo Bustos, expresó: «en el marco de la Mesa de Coordinación Institucional de Seguridad nos reunimos para analizar y planificar nuevas medidas a tomar en caso de encontrar en flagrancia a personas que no respeten el decreto provincial».

«Queremos que la población entienda que esto lo hacemos para cuidarnos entre todos, hemos armonizado los intereses económicos del sector comercial y gastronómico, pero debemos evitar la circulación para garantizar que el virus no avance y puedan disminuir los contagios en todo el territorio provincial», continuó agregando Bustos.

Durante el cónclave se analizaron los puntos de mayor concentración de gente y que más requieren del refuerzo de tareas de prevención y control. 

«Para ello, se utilizan los mapeos de la Central de Emergencias 911 y la Central de Información Criminal dependiente del ministerio de Seguridad, a través de un monitoreo constante que permita desarticular eventos masivos y aglomeraciones», manifestó el coordinador del Ministerio de Seguridad.

«Con el objetivo de fomentar el trabajo conjunto y el objetivo de cooperación entre los tres niveles del Estado, se organizó el cronograma semanal de operativos y controles que se llevarán a cabo en la ciudad de Santa Fe, haciendo fuerte hincapié en los operativos de prevención durante el fin de semana», finalizó Facundo Bustos, coordinador del Ministerio de Seguridad.