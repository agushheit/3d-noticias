---
layout: Noticia con imagen
author: 3DNoticias
resumen: "Santo Tomé: pavimento"
category: La Ciudad
title: "Pavimentación en Santo Tomé: qué sectores contempla el plan municipal"
entradilla: Contempla 76 cuadras que son ejecutadas por personal del Municipio,
  bajo la modalidad de contribución por mejoras. Qué sectores ya quedaron
  concluidos y cuáles están en ejecución.
date: 2020-11-25T12:22:32.288Z
thumbnail: https://assets.3dnoticias.com.ar/bacheo-santoto.jpeg
---
La intendenta de Santo Tomé, Daniela Qüesta supervisó los trabajos de hormigonado realizados por el Ejecutivo local en calle Chacabuco, entre Moreno y Castelli, uno de los sectores comprendidos en el Plan de Pavimentación de 76 Cuadras.

En su recorrida, Qüesta valoró la continuidad de las obras que se llevan adelante por administración “a pesar del contexto tan adverso que nos impone la pandemia y las condiciones económicas del país”.

Respecto al sector intervenido, recordó que está compuesto por un total de 14 cuadras diseminadas en 5 vecinales. “Se trata de pequeños circuitos con calle de tierra que quedaron aislados en zonas pavimentadas, lo que dificulta su mantenimiento”, señaló.

### **EL PLAN, SECTOR POR SECTOR**

El Plan de Pavimentación de 76 Cuadras es ejecutado por personal del Municipio, bajo la modalidad de contribución por mejoras, y contempla la construcción de calzadas de hormigón simple, con un espesor de 15 centímetros, además de obras complementarias de saneamiento hídrico en todas las arterias intervenidas.

Luego del sorteo público correspondiente, los trabajos que se llevan adelante actualmente corresponden al grupo de cuadras Nº 1 previsto en el plan de obra.

El mismo comprende la pavimentación de los siguientes circuitos:

\- Gral. López e/ Mitre y río Salado – Ejecutado.

\- La Rioja e/ Av. Mitre y río Salado – Ejecutado.

\- Pasaje público e/ Candioti y Av. Mitre – Ejecutado.

\- Saavedra e/ Necochea y Sargento Cabral – Ejecutado.

\- Sargento Cabral e/ Saavedra y Moreno – Ejecutado.

\- Pasaje Reconquista e/ Chacabuco y vías del ferrocarril – Ejecutado.

\- Chacabuco e/ Moreno y Castelli – Ejecutado.

\- Moreno e/ Sargento Cabral y Chacabuco – A ejecutar.

\- Mateo Booz e/ Moreno y Castelli – A ejecutar.

\- Pasaje Payró e/ Avellaneda y Obispo Gelabert – A ejecutar.

\- Pasaje Bedetti e/ 20 de junio y Batalla de Ituzaingó – A ejecutar.

\- 20 de Junio e/ Pasaje Bedetti y Alvear – A ejecutar.

\- 20 de Junio e/ Alvear y 7 de Marzo – A ejecutar.

\- Derqui e/ San Juan y río Salado – A ejecutar.

\- San Juan e/ Derqui y Gaboto – A ejecutar.

### **MÁS DECLARACIONES**

“Para nosotros, estar aquí trabajando significa haber superado una cantidad de inconvenientes como la restricción de personal por los protocolos de prevención del Covid, el aumento permanente de los insumos y del costo operativo”, enfatizó Qüesta.

En este sentido, destacó el compromiso y la capacidad de adaptación de los empleados municipales, como así también el significado de “cumplir con la palabra y sostener este plan que excede a nuestra gestión de gobierno”.

“Con la continuidad de este programa, sumado a la obra de pavimento flexible en los barrios La Paz y UPCN, seguimos sosteniendo con mucho esfuerzo la política de mejoramiento de la red vial que nos propusimos como una meta central de nuestro gobierno”, completó la mandataria.