---
layout: Noticia con imagen
author: "Fuente: Diario UNO"
resumen: "ISEF: tramitación de títulos "
category: La Ciudad
title: Casi 80 egresados del Isef alertan que no pueden trabajar porque no
  recibieron su título
entradilla: El Instituto Superior de Educación Física de la ciudad de Santa Fe
  no asiste a las guardias mínimas y no se adaptó virtualmente para cumplir con
  la tramitación de los títulos de los alumnos recibidos.
date: 2020-11-18T13:22:13.271Z
thumbnail: https://assets.3dnoticias.com.ar/isef.jpg
---
Un grupo de egresados del profesorado de Educación Física de Santa Fe comenzaron a hacer visible a través de las redes sociales un problema que se les presentó producto de la pandemia para poder desarrollar su actividad: no pueden obtener sus títulos o certificados de título en trámite desde hace más de un año, proceso que se agravó aún más en los últimos meses ya que no se está trabajando de manera virtual.

En diálogo con el programa "Buen Santa Fe" un egresado del Isef, Marcos Jortack, denunció que la institución no entrega ni los títulos ni certificado de título en trámite del Profesorado de Educación Física desde hace aproximadamente un año ya que en el Instituto Superior de Educación Física de la ciudad de Santa Fe no se está trabajando de manera presencial y, según él, tampoco de manera virtual. "Desde el Ministerio les piden que haya una guardia mínima y eso no se está cumpliendo." Jortack señaló que nunca tuvieron documentación digitalizada y que "tienen miedo" de que no les pasen las materias aprobadas, ya que no tienen forma de revisarlo.

El vocero de casi 80 alumnos que se encuentran en la misma situación dijo que la única respuesta que recibieron fue que no hay personal. "Al no tener nada digitalizado se necesita que haya personal para generar esos certificados, pero como no está yendo gente al instituto es una cadena que se repite y no nos pueden dar ninguna respuesta", señaló.

Además, puntualizó que cuando se acercan al instituto y sí encuentran gente les "cierran la puerta en la cara".

## **La necesidad de trabajar**

Jortack señala desde noviembre de 2019 está recibido y que todavía no recibió ni una constancia. "Imaginate que yo presento mi currículum, me llaman y en el momento en el que necesito presentar una certificación de que soy docente no tengo forma de corroborarlo", expresó.

"Necesitamos inscribirnos a los escalafones y este certificado (de título en trámite) tiene la validez de un año no más y a muchos chicos que se los entregaron se les está por vencer. Y al no tener respuesta van a perder la inscripción que hicieron".

Según el egresado, intentaron comunicarse también por mail o WhatsApp y no recibieron respuesta ni del regente ni del vicerregente. Aclaró: "El supervisor regional del Ministerio de Educación se comunicó y dijo que iba a hacer lo posible, pero tampoco hemos obtenido respuesta".

En un intento desesperado por apurar los trámites los alumnos se juntaron y acudieron a un abogado, que presentará este martes una intimación firmada por todos. "No vemos ninguna otra opción porque no recibimos respuesta", finalizó.