---
category: Agenda Ciudadana
date: 2021-11-22T06:15:55-03:00
thumbnail: https://assets.3dnoticias.com.ar/LAMMNES.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: 'Fin de semana largo: 3,5 millones de turistas se movilizaron por el país'
title: 'Fin de semana largo: 3,5 millones de turistas se movilizaron por el país'
entradilla: 'Con el impulso de PreViaje hay un gran nivel de ocupación en los principales
  destinos, un 20% más que en la misma fecha prepandemia

'

---
Unos 3.550.000 turistas se están movilizando por el país durante este fin de semana largo, según datos del sector privado. Es entre un 20 y un 25% más que el mismo fin de semana largo de 2019 y 2018, prepandemia.

“Estamos viviendo otro gran fin de semana largo en todo el país, muy superior a las cifras previas a la pandemia”, expresó el ministro de Turismo y Deportes, Matías Lammens quien agregó que “tuvimos el mejor octubre de la década y vamos a seguir creciendo en estos últimos meses del año, con el impulso de PreViaje y el gran impacto del turismo receptivo. El turismo está en marcha y la reactivación ya se siente en todas las regiones del país”.

Reportes oficiales indican que se alcanzaron cifras de ocupación plena en las provincias de Misiones, Mendoza, Salta y San Luis. Destinos como Puerto Iguazú, Mendoza, Salta y Merlo llegaron al 100%; Gualeguaychú, Rosario, Pinamar, Villa Carlos Paz, San Rafael, la Quebrada de Humahuaca, San Juan y Villa La Angostura, 95%; el Partido de la Costa, Puerto Madryn, Federación y Santa Rosa de Calamuchita, 90%; El Calafate, 87%; San Carlos de Bariloche, 85%; Mar del Plata, Villa Gesell y Ushuaia, 80%; San Martín de los Andes, 75%.

De acuerdo a información oficial, las localidades más elegidas fueron el Partido de la Costa, Mar del Plata, San Carlos de Bariloche, Puerto Iguazú, Villa Carlos Paz, Villa Gesell, Mendoza, Salta, Pinamar, Ciudad de Buenos Aires, Federación, Ushuaia, El Calafate, San Martín de los Andes, Merlo, San Rafael, Puerto Madryn, Villa La Angostura, Gualeguaychú, la Quebrada de Humahuaca, Santa Rosa de Calamuchita y El Bolsón.

Entre los factores clave que explican el exitoso fin de semana largo está el impulso de PreViaje. Gracias a este programa, más de 300.000 argentinas y argentinos compraron sus escapadas para noviembre de 2021 mediante el programa que devuelve el 50% de los gastos, movilizando más de $8.500.000 en consumo solo durante este fin de semana. Se estima que, para fin de año, cerca de 1.000.000 de personas viajen por el país con esta iniciativa.

De acuerdo a información oficial, más de 2.500.000 turistas ya utilizaron PreViaje, lo que lleva generados ingresos por $49.000.000 para el sector turístico.