---
category: Agenda Ciudadana
date: 2021-06-19T06:00:36-03:00
thumbnail: https://assets.3dnoticias.com.ar/SOCIALISMO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Turismo: el socialismo cuestiona al gobierno provincial por no asistir al
  sector con fondos disponibles'
title: 'Turismo: el socialismo cuestiona al gobierno provincial por no asistir al
  sector con fondos disponibles'
entradilla: Desde el socialismo en el Cámara de Diputados de la provincia precisaron
  que en 2020 más del 40% de los fondos que la Ley de Explotación de Casinos y Bingos
  destina al rubro turismo no fueron ejecutados por la provincia.

---
Desde el bloque de diputadas y diputados del socialismo solicitaron al gobierno de Omar Perotti que informe por qué el Ministerio de Producción, Ciencia y Tecnología no utilizó durante 2020 más del 40% de los fondos que, por la Ley N°11.998 de Explotación de Casinos y Bingos, destinados a financiar políticas activas de promoción del turismo en todo el territorio provincial. Se trata de 4,5 millones de pesos cuyas partidas recién fueron incorporadas al ejercicio de este año recién en el mes de junio.

“El saldo no invertido por el gobierno de Omar Perotti representa el 40% del crédito presupuestado en 2020, lo que demuestra una vez más la dificultad de esta gestión para administrar los recursos de las santafesinas y santafesinos”, afirmó el diputado José Garibay, autor de la iniciativa. “En el contexto de crisis y pandemia que estamos atravesando se convierte en una cuestión grave”, agregó el legislador por el socialismo.

**Secretaría de Turismo de Santa Fe**

En la presentación, los legisladores y legisladoras señalan que el sector económico vinculado al turismo y actividades afines “es una de las actividades más afectadas” por las sucesivas medidas de confinamiento y aislamiento social dispuestas desde marzo del año pasado. Y remarcan que la imposibilidad de ejercer estas actividades en el marco de la pandemia puso al sector “en una gravísima situación financiera durante todo el 2020 e incluso lo que llevamos del corriente año”.

En ese marco, para Garibay “es imposible pensar que no hubiera emprendimientos que necesitaran esos recursos”, y se preguntó: “Si este no era el momento para asistirlos, cuándo sería”.

“Es mucha la ineficiencia que ha mostrado el gobierno de Omar Perotti desde el inicio de la pandemia para ayudar a los distintos sectores que lo necesitan en tiempo y forma”, afirmó el legislador, al tiempo que advirtió que “las empresas que cierran no vuelven a abrir automáticamente; es capital y empleo que se destruye y no se recupera. Además de los recursos que no sabemos por qué esta administración no utiliza y mantiene inmovilizados, tiene otras herramientas que tampoco se interesa en implementar”.

En tal sentido, el diputado del socialismo recordó que en octubre del año pasado la Legislatura sancionó la Ley Nº14.009 de Emergencia provincial del sector turístico, que contempla beneficios impositivos y crediticios y aún no fue reglamentada por el Ejecutivo provincial.

Además de pedir informes al Ejecutivo por la no utilización de los fondos que dispone la ley Nº 11.998, la presentación realizada por el bloque socialista indaga también por qué recién el 4 de junio de este año, mediante el decreto Nº 758 del gobernador Omar Perotti, las partidas sin ejecutar fueron incorporadas al ejercicio 2021.