---
category: Agenda Ciudadana
date: 2021-09-20T06:15:06-03:00
thumbnail: https://assets.3dnoticias.com.ar/vacunatecareta.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: La vacunación en menores deberá esperar porque no llegaron las dosis de Pfizer
title: La vacunación en menores deberá esperar porque no llegaron las dosis de Pfizer
entradilla: Las vacunas destinadas a los menores de entre 12 y 17 años se esperaban
  que lleguen el fin de semana, pero recién estarán en la provincia lunes o martes.

---
Aún no llegaron a la provincia las casi 8.000 dosis de vacunas de Pfizer que le corresponden de las llegadas hace más de diez días y que este fin de semana ya iban a estar las dosis en Santa Fe y Rosario. Esto demora el inicio de la vacunación en la franja de personas de 12 a 17 años sin comorbilidades, que estaba prevista inmediatamente después del arribo de las dosis. Desde el Ministerio de Salud provincial estiman que los componentes enviados por el laboratorio estadounidense llegarán entre lunes y martes, y con ello se comenzará a vacunar a la población objetivo mencionada.

A principios de esta semana, la ministra de Salud de la provincia, Sonia Martorano, confirmó que sobre el fin de semana llegaría a Santa Fe y a Rosario la primera partida de vacunas de Pfizer y que se destinaría al grupo etario entre 12 a 17 años sin factores de riesgo. Pero esto no sucedió aún, y desde el mismo Ministerio de Salud estiman que pueden llegar entre lunes y martes.

A raíz de la eventual llegada de las dosis de Pfizer durante el fin de semana (algo que, finalmente, no ocurrió), se habían dictado capacitaciones para tener una mejor manipulación sobre estas vacunas, ya que las dosis demandan un operativo de distribución y conservación especial, a 70º bajo cero. Además, al tratarse de una vacuna liofilizada (es decir, que consiste en un polvo que debe ser diluido) se deben aplicar diluyentes y jeringas especiales. “Es una vacuna que tiene una complejidad diferente y queremos ser muy cuidadosos”, explicó la ministra de Salud.

Mientras la provincia espera las primeras dosis de Pfizer, ya hay casi 200.000 menores de entre 12 y 17 años anotados para recibir la vacuna contra el coronavirus. La cantidad total de menores anotados para vacunarse es de 231.276. De ellos, 198.778 no presentan factores de riesgo. Por su parte, los menores de entre 12 y 17 años con comorbilidades que se anotaron para recibir la vacuna fueron 32.498, de los cuales 30.001 fueron inoculados con una dosis y 15.633 ya tienen el esquema completo. Es por eso que en caso de no estar anotados, los interesados en recibir la vacuna pueden hacerlo en este enlace.

De acuerdo al cronograma establecido por Pfizer, se espera que en el último trimestre de este año se pueda completar la entrega de las 20 millones de dosis anunciadas y acordadas entre el gobierno nacional y el laboratorio estadounidense. Desde la cartera sanitaria nacional entienden que llegarían 6 millones en octubre, otros 6 millones en noviembre y el resto en diciembre.