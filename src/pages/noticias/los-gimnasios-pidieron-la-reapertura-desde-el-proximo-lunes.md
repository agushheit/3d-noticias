---
category: Agenda Ciudadana
date: 2021-06-10T07:01:01-03:00
thumbnail: https://assets.3dnoticias.com.ar/los-gimnasios-la-ciudad-santa-fe-pidieron-la-urgente-reapertura.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: Los gimnasios pidieron la reapertura desde el próximo lunes
title: Los gimnasios pidieron la reapertura desde el próximo lunes
entradilla: En un fuerte comunicado solicitaron a Provincia y Municipalidad la reapertura.
  "No consideramos una buena decisión que nos nieguen el derecho a trabajar después
  de lo ocurrido el fin de semana en la ciudad"

---
En las últimas horas, desde la Cámara de Gimnasios, Natatorios y Afines de la ciudad de Santa Fe, solicitaron al gobierno provincial y al ejecutivo municipal la reapertura de la actividad bajo estrictos cumplimientos de los protocolos sanitarios. "Nos cerraron las puertas de todos los establecimientos a consecuencia de las últimas medidas provinciales y ya pasaron 35 días sin que podamos trabajar", manifestaron desde el sector.

Un duro comunicado emitido por la Cámara de Gimnasios de la ciudad, da cuenta de la "delicada" y "crítica" situación que vive el sector desde lo económico y social. "En estos momentos no consideramos una buena decisión negarnos el derecho a trabajar, sobre todo, después de los ocurrido el fin de semana con los festejos en las calles de Santa Fe", comenzaron argumentando del sector. "Decidieron cerrar nuestra posibilidad de trabajar a pesar de los estrictos protocolos bajo los que nuestra actividad venía funcionando; pero adherimos a la directiva provincial aunque nunca, en nuestros 150 días de cierre desde el inicio del confinamiento allá por el mes de marzo del 2020, buscamos salirnos de la norma".

"Necesitamos que entiendan lo desesperante, angustiante y crítica que es nuestra situación. Ya no podemos, al igual que la gran mayoría de los establecimientos que nuclea a la actividad física, resistir un solo día más. Los subsidios que el Gobierno de la provincia prometió están llegando en cuenta gotas. Queremos en forma conjunta establecer un punto de partida y reapertura en donde se reconozca, además, la importancia y el rol esencial que cumple la actividad física realizada dentro de espacios seguros y controlados", subrayaron los referentes de los gimnasios en la ciudad de Santa Fe.

"Ya no existe más margen para estar cerrados. Se resalta que los gimnasios brindamos salud a la población a través de la actividad física; no somos actividades recreativas, ni deportivas, clubes, ni competencias, por lo que pedimos no estar catalogados dentro de ese combo a la hora de definir restricciones. Si la provincia sigue la línea de priorizar la actividad económica, no pueden dejarnos afuera de la posibilidad de trabajar”, continuaron esgrimiendo entre los fundamentos del comunicado para volver a trabajar.

En este contexto, desde la Cámara de Gimnasios solicitaron de forma urgente la reapertura a ambos gobiernos, tanto provincial como municipal. El pedido es concreto: reabrir a partir del lunes 14 de junio con el protocolo adecuado a las recomendaciones nacionales de prevención de transmisión por aerosoles en el marco de la campaña "ventilar", garantizando en los locales la correcta ventilación de los espacios y la calidad óptima del aire, mediante el uso de la ventilación cruzada, el uso de medidores de co2 (dióxido de carbono), con un sistema de trabajo con aforo a un 30 por ciento y turnos rotativos de hasta 10 personas en espacios cerrados, manteniendo las medidas de seguridad e higiene.

Dentro del pedido de reapertura, se incorpora también el sistema de uso de balcones, terrazas y espacios al aire libre, como así también la asistencia de todas aquellas personas que fueron vacunadas y que necesitan la actividad física por prescripción médica como parte del tratamiento de enfermedades crónicas no trasmisibles y en los horarios permitidos en función de las restricciones de circulación.

**Campaña Ventilar para la reapertura**

Se trata de un programa nacional elaborado por científicos del Conicet que alienta la medición de dióxido de carbono en los ambientes, para poder garantizar a los usuarios que el aire que respiran dentro de cada espacio del gimnasio está en los niveles aceptables.

La medición permite, según el tipo de actividad, la cantidad de gente y el tiempo de duración, ver cuán limpio es el aire para evitar respirar lo que ya fue respirado. En los tres días que abrieron antes del último cierre pudieron hacer pruebas, y la idea es trabajar en los protocolos de cada espacio para que sea seguro entrenar en un ambiente cerrado con ventilación, y así equipararlo a un espacio al aire libre.