---
category: La Ciudad
date: 2022-01-14T06:15:15-03:00
thumbnail: https://assets.3dnoticias.com.ar/RIOCERO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Las dos temperaturas máximas históricas en Santa Fe fueron con el río en
  cero
title: Las dos temperaturas máximas históricas en Santa Fe fueron con el río en cero
entradilla: 'Según los registros del Centro de Informaciones Meteorológicas de la
  UNL, el 2 de enero de 1963 la ciudad soportó 45,4ºC. Este jueves alcanzó los 46,5
  ºC. El punto en común: la bajante del río Paraná.

  '

---
El Centro de Informaciones Meteorológicas (CIM) perteneciente a la Facultad de Ingeniería y Ciencias Hídricas de la Universidad Nacional del Litoral (FICH-UNL) registró este jueves a las 14.46 la [temperatura](https://www.unosantafe.com.ar/temperatura-a34557.html) más alta registrada en la historia de la ciudad de Santa Fe: 46,5°. De esa manera se rompió el récord registrado por el mismo organismo el 2 de enero de 1963, que fue de 45,4°, y el de este 12 de enero de 2022 con 46,1º. Una condición común entre esta brecha de 59 años es la pronunciada bajante del río Paraná en el Puerto Santa Fe.

Según registros de la UNL y de Prefectura Naval, en 1963 la altura mínima fue de 0,31 metros, y la media anual fue de 2,57 metros. En lo que va del 2022 la altura osciló entre los 0,36 metros y los -0,22. Y en 2021 la altura más alta del Paraná fue de 3,41 metros y la más baja -0,18. El especialista en hidrometeorología, Luis Dopazo, señala que las bajantes pueden tener una relación directa con las temperaturas altas: "El regulador de la temperatura en nuestra ciudad es la humedad y el que genera la humedad es el río, si nosotros hoy tuviéramos un río normal no llegamos a los 40 grados. Por eso estamos viviendo estas temperaturas extremas".

En esta línea traza una diferencia entre la medición de la temperatura y de la sensación térmica. "Con el bloqueo térmico que tenemos ahora en el centro del país, sería más insoportable (el calor) porque tenemos temperatura alta y humedad. Como pasó en el 2016 que tuvimos 53 grados de sensación térmica, porque tuvimos una crecida y una temperatura muy alta. Pero ahí llegamos a los 40 grados".

"El río evapora mucha agua, entonces, ahora en la atmósfera no tenés nada más, así que el sol calienta directamente al suelo. Si tuviéramos un río alto en la atmósfera, tendríamos aire y agua, aire humedad. Entonces la energía del sol se dispersaría en calentar el suelo y en calentar la humedad de la atmósfera. Se reparte la radiación solar. Como ahora prácticamente no tenemos humedad, la radiación del sol va directamente a calentar el suelo, a subir la temperatura. **Este miércoles cuando se dio 46,1 grados teníamos 17% de humedad. Cuando nuestra humedad promedio es el 52%**", detalló.

Al ser consultado por este medio sobre la comparación y diferencias que se pueden hacer entre ambas bajantes, Dopazo explicó: "La bajante de 1963 fue una bajante ordinaria, con niveles normales para una bajante. Esta bajante que tenemos ahora es extraordinaria y ya la podemos catalogar de bajante extrema. Porque para comparar con los valores de ahora tenemos que ir a la década del 40. Más precisamente a 1944 donde el río tenía en enero una mínima de cero pero en invierno llegó a tener 68 centímetros por debajo del hidrómetro".

Por su parte, Ignacio Cristina, docente investigador del CIM, dijo: "Si analizamos los datos de lluvia anuales de 1962 y 1963. Vemos que el primero tuvo una precipitación acumulada de 661 milímetros. El 63 tuvo una precipitación acumulada de 883 milímetros, siempre redondeando. En función de esos datos, y que río estaba bajo, se podría llegar a decir que en esa época se estuvo en una condición que puede haber llegado a ser similar a la que se está hoy. Sobre todo analizando los montos acumulado de lluvias que se ven por debajo de lo que son las precipitaciones medias para la región". Además aportó que en 1962 la altura mínima del Paraná fue de 0,39, con una media anual de 2,33 y que en noviembre de ese año se registró 45,4 grados de temperatura. Misma medida que se repitió en enero de 1963 con valores del río similares. Sin embargo no se aventuró a hacer comparaciones en profundidad por la falta de más información que contextualice los escenarios de 1963. Por ejemplo refirió que no hay información sobre si existió el fenómeno de La Niña en ese momento.

Por su parte Dopazo apuntó a la planificación urbana como posible factor de aumentos en las variaciones de temperatura: "En los 60 hay que pensar que la calle Ángel Casanello no estaba asfaltada. Para el norte había muy pocas calles asfaltadas, las avenidas solamente. El resto era de tierra. Ahora tenés asfalto hasta el limite con Monte Vera. Todo lo que eso hace es acumular calor, queda como calor latente. Hace 40, 50 años lo que pasaba era que bajaba el sol entonces refrescaba más rápido porque había menos hormigón. Ahora se mantiene el calor durante toda la noche. No se alcanza a enfriar". El especialista informó que El Niño y La Niña son fenómenos que se comenzaron a estudiar desde los 80.

## Datos y registros oficiales

En relación al registro histórico de temperaturas máximas con el que cuenta la FICH de la UNL, tanto Dopazo como Cristina explicaron a este medio que son medidas que se tomaron desde 1920 hasta hoy, en diferentes estaciones meteorológicas de la ciudad de Santa Fe, que mediante criterios científicos acordados por académicos especialistas fueron adoptadas y archivadas en la Universidad Nacional del Litoral. Y hacen la diferencia de las medidas que toma el Servicio Meteorológico Nacional (SMN) en la estación de Sauce Viejo, a 25 kilómetros del CIM. Por su parte, Dopazo explica que allí siempre se registran entre cuatro y cinco grados menos que en la capital provincial por sus características geográficas particulares. "La estación meteorológica de la FICH es la única habilitada en la ciudad con características aprobadas por el SMN", sostiene Dopazo.

En esta línea, los registros históricos del SMN indican que la temperatura máxima registrada en la estación de Sauce Viejo en 1963 fue 44 grados. De acuerdo a datos de la FICH UNL que brindó Cristina, el 2 de enero de 1963 la temperatura fue de **45,4**°, pero aclara: "son los registros que tenemos desde 1920, antes pudo haber temperaturas altas pero son registros que pueden llegar a tener los jesuitas que fueron los primeros que tomaron estas mediciones en la plaza 25 de Mayo de nuestra ciudad".

## Como sigue el río

En relación al río, Dopazo indicó que "va a seguir en condiciones extremas de bajante, si o si, por lo menos hasta el 31 de marzo que es la información que tenemos". Podemos calcular a tres meses: enero febrero y marzo. Más allá de esa fecha no tenemos datos científicos para comprobarlos".

"Para que el río Paraná crezca en nuestra zona, tiene que darse grandes lluvias en la cuenca alta del río de la Plata, es decir en Brasil. En esa zona, las lluvias comienzan en el primavera con lo cual nos hace suponer que vamos a tener que esperar hasta la próxima primavera a ver si llegan las lluvias. Recién a partir del próximo verano veríamos un río medianamente normal. La última vez que pasó esto fueron cuatro años: de 1968, 69, 70 y 71. Nosotros recién estamos transitando el tercer año. Siempre en hidrología y en meteorología nos movemos con datos y estadísticas.", agrega Dopazo.

## Miradas diferentes sobre el futuro

En relación al análisis que se puede hacer sobre esta temperatura, Dopazo opinó: "A modo personal, y esta es una cuestión mía, preparémonos porque este verano va a ser el más fresco de los próximos veranos que vamos a tener la humanidad. Porque si seguimos con esta tendencia de negar el cambio climático y no hacer nada, este verano va a ser el más fresco de los próximos años. Por ahí puede ser extremista pero si no nos ponemos las pilas, si no entramos en conciencia que tenemos que hacer todos, no solo el Estado, ni el gobierno provincial, la municipalidad, sino todos, no nos ponemos a hacer algo en conjunto esto va a seguir en aumento. Después los inviernos van a ser mucho mas fríos. Porque ocurre lo mismo, con el río muy bajo no hay humedad entonces la temperaturas bajas son mas intensas. Estamos complicados. ¿Cómo se soluciona esto? Plantando árboles. Simple y sencillo".

Cristina, en cambio, tiene una postura más moderada: "Esta situación que se esta viviendo, que es sumamente preocupante, se ha dado en otras épocas en la historia de estudio de las que tenemos datos, y nada dice que no se pueda volver a dar. No se puede decir que esta situación que se esta dando ahora es lo que se va a dar definitivamente de acá en adelante. No por el hecho que llevamos casi tres años de bajante no quiere decir que a fines del año que viene por ejemplo, no tengamos una crecida".

"Coincido en que la deforestación juega un rol muy importante, pero de ahí a decir que de ahora en más vamos a tener estas temperaturas, yo no lo digo. Hoy se está en una situación extrema muy grave por una combinación de factores pero no es que de ahora en mas se van a registrar estas temperaturas. Es una situación típica como ya se ha visto en esta época del año como ya hemos visto y nadie dice que no se pueden volver a dar, pero eso no quita que en un futuro no tengamos justamente la situación opuesta con ríos muy altos, niveles de lluvias superior a los promedios de la región y temperaturas por debajo de la media para esta zona", agregó.

Y concluyó el especialista en meteorología de la FICH: "Después de esta bajante tuvimos un montón de crecidas más, y un montón de años húmedos. Lo que si se puede decir que esto es un fenómeno que mas allá del cambio climático es cíclico. Se han dado bajantes y sequías muy extraordinarias como se han dado crecidas extraordinarias. De hecho en la década de los 90 tuvimos crecidas muy importantes. Es más, en toda la década del 90 tuvimos solo dos valores en enero que superaron los 40 grados, después el resto no supero los 38 grados de máxima apenas".