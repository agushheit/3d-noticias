---
layout: Noticia con imagen
author: .
resumen: Renuncia de Vicentín
category: El Campo
title: La Bolsa de Comercio de Rosario aceptó la renuncia de Vicentin
entradilla: El Consejo Directivo de la Bolsa de Comercio de Rosario (BCR) aceptó
  la renuncia presentada por la firma Vicentin y la emplazó a presentar una nota
  suscripta por el presidente del directorio para hacerla efectiva.
date: 2020-11-11T22:34:23.080Z
thumbnail: https://assets.3dnoticias.com.ar/vicentin.png
---
El Consejo Directivo de la  BCR decidió en forma unánime aceptar la renuncia presentada por la firma Vicentin a esa entidad y le requirió que en el plazo de 10 días acompañe una nota de ratificación suscripta por el presidente del directorio para hacerla efectiva.

En un comunicado, la entidad bursátil rosarina dijo que "el Consejo Directivo resolvió igualmente aprobar y ratificar en forma unánime lo actuado por la Mesa Ejecutiva mediante Resolución N° 9/2020, que decidió el inicio del Sumario, por las mismas razones vertidas en sus considerandos, y rechaza terminantemente las alusiones contenidas en la renuncia respecto a que el mismo fuera decidido por distintas razones a las expuestas o respondiendo a otros intereses".

La BCR ratificó que "la instrucción del sumario, que quedaría obstruido reglamentariamente por la presentación de la renuncia como asociado, persigue como único objetivo dilucidar el cumplimiento de normas estatutarias y reglamentarias que conllevan mantener vigentes los valores que han guiado a nuestra Bolsa de Comercio por más de 130 años, como lo es la transparencia y el desarrollo de mercados, donde prime la buena fe y el valor de la palabra empeñada".

Por último, la BCR rechazó también, "en forma terminante, que el inicio del referido sumario contradiga el principio constitucional de presunción de inocencia, y mucho menos que refleje una condena anticipada que pueda causar ningún gravamen, sino la apertura de una investigación para determinar la eventual infracción a disposiciones legales, estatutarias o reglamentarias de nuestra Institución, que todos los asociados declaran conocer y acatar al momento de solicitar su admisión como socios (artículo 2°, inciso h, apartado 1, del Reglamento General)".