---
category: La Ciudad
date: 2021-08-01T06:00:48-03:00
thumbnail: https://assets.3dnoticias.com.ar/Manzana.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Santa Fe capital integra el Corredor Turístico más importante de América
  del Sur
title: Santa Fe capital integra el Corredor Turístico más importante de América del
  Sur
entradilla: Se trata del Camino internacional de los Jesuitas, una ruta que une cinco
  países latinoamericanos. Desde hoy se puede visitar la nueva web donde se presentan
  los diferentes destinos que integran este producto turístico

---
La Municipalidad de Santa Fe fue notificada recientemente por el Ministerio de Turismo y Deportes de la Nación sobre la habilitación del portal del Camino Internacional de los Jesuitas: www.caminodelosjesuitas.com. Desde hoy, las personas interesadas en conocer las propuestas de este Camino -del cual la ciudad de Santa Fe forma parte desde fines de 2020-, ya pueden acceder y navegar por el portal. El Camino Internacional de los Jesuitas es considerado el principal corredor turístico de América del Sur dado que une ciudades de países como Argentina, Uruguay, Paraguay, Brasil y Bolivia. En tal sentido Santa Fe capital es una de las pocas a nivel nacional que lo integran.

Al respecto, el intendente Emilio Jatón destacó que “los acontecimientos históricos y culturales que tuvieron lugar en nuestra Manzana Jesuítica fueron fundamentales para conseguir que la ciudad de Santa Fe sea incorporada al Camino Internacional de los Jesuitas”.

Además, señaló que “estamos en comunicación permanente con las autoridades del Colegio y también con las entidades que conforman el Safetur para trazar estrategias conjuntas que nos permitan hacer de Santa Fe capital un destino cada vez más interesante para los turistas. Y los resultados están a la vista”.

Por otra parte, Emmanuel Sicre, rector del Colegio Inmaculada Concepción, aseguró que “la incorporación de nuestra Manzana al Camino Internacional abre la posibilidad de hacer más visible nuestro proyecto humanizador cristiano y, al decir del P. Guillermo Furlong en su Historia del Colegio de la Inmaculada de la ciudad de Santa Fe, también sus irradiaciones culturales, espirituales y sociales”.

**Turismo religioso**

Este segmento del turismo, que se caracteriza por la realización de viajes o incluso estadías en lugares emblemáticos para los diferentes credos, moviliza alrededor de 350 millones de personas en el mundo. Es por ello que la incorporación de la ciudad de Santa Fe al Camino Internacional de los Jesuitas es una oportunidad para atraer mayor número de visitantes a la capital provincial.

La Manzana Jesuítica de Santa Fe capital, ubicada en el corazón del casco histórico, es un complejo integrado por el Colegio Inmaculada Concepción y el Santuario Nuestra Señora de los Milagros. Entre los hitos que la convierten en un sitio emblemático para el Turismo Religioso se destacan, por un lado, haber sido el primer colegio de la orden en el país (año 1610), pero también haber sido el escenario del milagro de la Virgen Inmaculada documentado tanto con actas eclesiásticas como civiles en 1636. Ya en momentos de la conformación nacional el Colegio alojó a los Constituyentes de la Convención de 1853. Y finalmente en el siglo XX fue la institución que alojó a Jorge Bergoglio, Papa Francisco, en su etapa de formación como maestrillo de la Academia de Literatura, entre otros sucesos.

Estos son algunos de los argumentos con los que se aplicó para la incorporación al Camino Internacional de los Jesuitas, un producto compuesto por diversos pueblos, sitios arqueológicos, estancias y otros solares jesuitas.

**Un imperdible de la ciudad**

La Manzana Jesuítica de Santa Fe es un lugar emblemático de la capital provincial. En su interior no sólo se resguarda el patrimonio religioso sino también cultural, científico e histórico. Por eso mismo, desde la Municipalidad de Santa Fe se llevan a cabo visitas guiadas gratuitas durante todo el año. Incluso, durante este último período marcado por la situación pandémica, se han desarrollado protocolos para poder seguir brindando este servicio y resguardar la salud de quienes lo visitan.

También se han desarrollado materiales para que el circuito sea recorrido de forma autoguiada. Para ello se instaló señalética y cartelería adecuada en las instalaciones de la Manzana y paralelamente se ha desarrollado una audioguía bilingüe español-inglés para diversificar la oferta al respecto. La misma está disponible para ser descargada desde la web del municipio (www.santafeciudad.gov.ar). También se puede escuchar desde la cuenta de Spotify de Santa Fe capital.