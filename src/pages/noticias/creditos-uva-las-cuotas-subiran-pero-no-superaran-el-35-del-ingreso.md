---
category: Agenda Ciudadana
date: 2021-01-25T09:41:42Z
thumbnail: https://assets.3dnoticias.com.ar/uva.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: La Nación
resumen: 'Créditos UVA: las cuotas subirán, pero no superarán el 35% del ingreso'
title: 'Créditos UVA: las cuotas subirán, pero no superarán el 35% del ingreso'
entradilla: El ministro Jorge Ferraresi confirmó que las cuotas se descongelarán en
  febrero con alzas de 6% y 9%, según el monto; habrá un nuevo DNU para garantizar
  el tope sobre el salario

---
El ministro de Desarrollo Territorial y Hábitat, Jorge Ferraresi, informó que las cuotas de los créditos hipotecarios UVA se descongelarán a partir de febrero y los tomadores empezarán a pagar entre un 6 y un 9% más, de acuerdo con el esquema de convergencia de ajustes contemplado en el decreto 767, publicado en septiembre pasado. Sin embargo, en los próximos días saldrá un nuevo decreto para garantizar que la cuota no supere el 35% del ingreso del tomador.

"Se dispara el artículo de convergencia y la semana próxima nos reuniremos con la mayoría de los bancos que tienen créditos UVA para crear una herramienta para que ningún crédito supere el 35% del ingreso del grupo familiar", afirmó el sábado Ferraresi en declaraciones a Radio 10.

De esta forma se mantendría el esquema incluido en el artículo 3° y el anexo del decreto 767 para retomar el sendero del descongelamiento, previsto inicialmente para febrero de 2020 y paralizado desde marzo por el inicio de la pandemia del Covid-19, pero se modificará el artículo 4°, que dejaba librado a los bancos habilitar una instancia para considerar la situación de aquellos clientes que acrediten que el importe de la cuota a pagar supera el 35% de sus ingresos.

Para los créditos para vivienda única hasta 120.000 UVA (representan aproximadamente el 80% de las financiaciones hipotecarias UVA), que tuvieron dos congelamientos (desde agosto de 2019 hasta enero de 2020 y luego entre marzo de 2020 y enero de 2021), el incremento en la cuota en febrerosería del 9%, tomando en cuenta el esquema de convergencia y los datos del Relevamiento de Expectativas del Mercado (REM). Una cuota promedio de $13.000 congelada en septiembre de 2019 hasta enero de 2020 se incrementó a $14.200 en marzo de 2020 y en febrero de 2021 pasaría a $15.500, con un aumento de $1300.

En tanto, los hipotecarios para vivienda única de más de 120.000 UVA y los prendarios solo se beneficiaron del segundo congelamiento, por lo que el incremento en la cuota sería menor, del orden del 7%. Una cuota promedio de $13.000 en septiembre de 2019 se incrementó a $16.600 hasta marzo de 2020 y fue congelada en dicha cifra hasta este mes. En febrero de 2021 pasaría a $17.800, con un aumento de $1200.

Usando la misma fórmula y las proyecciones de inflación promedio del REM, el economista Federico González Rouco estima que en 18 meses la cuota aumentaría un 153% respecto del valor congelado a enero de este año.

A similar número llega el colectivo Hipotecados UVA. "De no mediar un nuevo DNU, las cuotas subirán un 65% durante 2021, si la inflación se mantiene en el 3% mensual, un escenario poco probable, y a lo largo de los 18 meses que durará la convergencia subirían un 160%, un porcentaje imposible de solventar para cualquier asalariado o trabajador independiente", opinó Marcelo Mercere, abogado e integrante de ese colectivo.

**Cortes de luz: fuertes reclamos de usuarios de varias zonas del GBA a Edenor y Edesur**	

Según estimaciones de los bancos, como resultado de los sucesivos congelamientos las cuotas se ubican en la actualidad no solo por debajo de los valores contractuales, sino también por debajo de los valores teóricos que hubieran asumido en caso de que el ajuste se realizara por la evolución de los salarios y no por la inflación (los tomadores de créditos hasta 120.000 UVA afrontaron en los últimos 17 meses apenas dos aumentos de cuotas, que las ubican un 19% por encima de los registros de agosto de 2019).

El último registro de mora publicado por el Banco Central (BCRA) para créditos hipotecarios (agosto de 2020) es de 0,8% y, a pesar de la pandemia, solo se incrementó 0,2 puntos porcentuales interanual. Asimismo, un estudio realizado por el Banco Nación (principal otorgante de este tipo de créditos) sostiene que el 65% de los deudores pertenecen al segmento ABC1 y que el 85% de los deudores con cuenta sueldo en el banco poseen una relación cuota/ingreso individual inferior al 35% (sin contar otros deudores y codeudores).

Por su parte, el jueves pasado, el expresidente del BCRA y creador de la UVA, Federico Strurzenegger, afirmó en su cuenta de Twitter que "en todos los créditos dados entre 2016 y 2019 ninguna cuota que haya arrancado en 25% de los ingresos superó el 30% de la relación cuota/ingreso", lo que explicaría la baja mora.

    <blockquote class="twitter-tweet"><p lang="es" dir="ltr">Una vez más se plantea del debate sobre los préstamos hipotecarios UVA. Abro hilo con algunas consideraciones. (0/11)</p>&mdash; Fede Sturzenegger (@fedesturze) <a href="https://twitter.com/fedesturze/status/1352429526018650113?ref_src=twsrc%5Etfw">January 22, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

"Para evaluar los sucesivos congelamientos que ya ocurrieron me pregunto si alguien realmente vio qué era lo que estaba pasando con las cuotas en términos de ingresos o si se evaluó la mora", agregó, criticando también al gobierno de Mauricio Macri, y dijo que "en la Argentina el Estado siempre está a tiro para regalar plata".

"Esto hace que sea negocio protestar y apostar a que se haga cargo de tus deudas. Pero hacerlo daña los incentivos para que el crédito se siga dando. Acá el capital no se licúa, pero eso no es motivo para condonarlo. Futuros propietarios, inmobiliarias, Uocra, constructores, bancos son perdedores del lobby para condonar los créditos UVA. Una sociedad que no logra preservar un solo contrato, aun los de privados, es una sociedad que no puede pensar en el futuro y, por ende, que no tiene futuro", concluyó.