---
category: Agenda Ciudadana
date: 2021-07-09T09:36:23-03:00
thumbnail: https://assets.3dnoticias.com.ar/vacunación.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: Vacunación en Santo Tomé este viernes feriado
title: Vacunación en Santo Tomé este viernes feriado
entradilla: 'Se colocarán 900 dosis, desde las 08:0 a las 15 horas inclusive. '

---
Este viernes, continua el operativo de vacunación en la ciudad de Santo Tomé. Las dosis se aplicarán a partir de las 8 y hasta las 15 para lo que se otorgaron 900 turnos.

Mañana se colocarán solo primeras dosis a personas con y sin riesgos, agentes policiales, docentes, auxiliares docentes y personal de Salud.

En la vecina ciudad, ya se está vacunando a ciudadanos de 33 años, sin comorbilidades.

Además, se vacunará a pacientes reprogramados de primera y segunda dosis, que  perdieron su turno.