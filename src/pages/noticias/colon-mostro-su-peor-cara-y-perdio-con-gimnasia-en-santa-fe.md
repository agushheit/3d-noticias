---
category: Deportes
date: 2020-12-13T14:25:36Z
thumbnail: https://assets.3dnoticias.com.ar/futbol.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'FUENTE: DIEZ EN DEPORTES – TÉLAM'
resumen: Colón mostró su peor cara y perdió con Gimnasia en Santa Fe
title: Colón mostró su peor cara y perdió con Gimnasia en Santa Fe
entradilla: Con goles de Weigandt y Contín, el Lobo superó 2-0 al Sabalero en el Brigadier
  López, por la 1ª fecha de la zona Campeonato de la Copa Diego Maradona. El Rojinegro
  jugó el peor partido de la temporada.

---
Colón fue un cúmulo de ideas y este sábado, en el peor partido de la temporada, perdió como local ante Gimnasia (LP) 2-0, por la 1ª fecha de la zona Campeonato de la Copa Diego Maradona. Marcelo Weigandt y Nicolás Contín (G) marcaron los goles del encuentro que se disputó en el Brigadier López y fue transmitido por LT10 AM 1020 y FM "X" 103.5.

**El Sabalero salió decidido a plantarse e imponer condiciones como en cada partido como local, sin embargo, no estaba lúcido como siempre**. Esto lo notó Gimnasia que, en un trabajo de hormiga, poco a poco fue llevándolo a su terreno. El elenco de Eduardo Domínguez iba al frente, pero sin ideas y escaso de solidez, algo que distaba mucho de lo que se venía viendo.

Fue así como Gimnasia inclinó la cancha y a los 19' le dio el primer aviso con un tiro de Nicolás Contín, que le dio mal de zurda ante la salida de Leonardo Burián. Un atisbo de que algo no estaba bien. Así y todo, llegó la respuesta inmediata de Tomás Chancalay con un tiro lejano que contuvo Jorge Brown.

Colón estaba contrariado con la pelota y la cantidad de amonestados (Bianchi, Olivera, Bernardi y Aliendro) era el fiel reflejo de que las cosas no estaban saliendo. Justamente a los 29' se dio el sacudón en la tarde santafesina: Victor Ayala ejecutó un tiro libre centrado que exigió a Burián, que en lugar de sacarle quizo contenerla y cuando se le escapaba, estaba el pibe Marcelo Weigandt para empujarla y poner el 1-0.

Algo que se veía venir, producto del flojo funcionamiento. Colón apretó en el final y casi lo empata con un tiro de Aliendro, pero se fue apenas desviado. Así se extinguieron los 45' iniciales, en la producción más deslucida de Colón en lo que iba del certamen.

<br/>

## **Segundo tiempo**

Lejos de encontrar soluciones en el inicio del complemento, Colón terminó de complicarse al recibir el segundo de Gimnasia al 1' a través de Nicolás Conti, que sacó provechó de un quedo de Bianchi y Burián. Un baldazo de agua fría para agudizar la confusión en Colón, que no daba pie con bola. El Lobo no hizo el segundo minutos después de no ser por la falta de puntería de Eric Ramírez, cuando tenía el arco a su merced tras, para variar en la tarde, otro rebote de Burián.

Producto de todo esto, Eduardo Domínguez no dudó a cambiar y mandó a la cancha a Brian Farioli y Tomás Sandoval en un escenario muy desigual. Pero era a todo o nada y por eso se apostó a ir y resignar un poco en defensa. Como estaba la cosa, Gimnasia quedaba a la expectativa de liquidar la cosa de contra.

Chancalay era el que más empujaba en un Colón desilachado y desconocido. Si bien es cierto que no podía acertar al arco, probaba e intentaba entre lo muy poco para destacar. El Lobo era prolijo y solo se dedicaba a cuidar la victoria, a la espera de una contra letal. Pero dio la sensación que el tiempo restante fue simplemente para las estadísticas, ya que la cosa no cambió y Colón terminó perdiendo el segundo partido como local, en la peor actuación en la temporada.

<br/>

## **Formaciones**

<br/>

🔴⚫ **Colón**: Leonardo Burián; Emmanuel Olivera, Bruno Bianchi y Rafael Delgado o Gonzalo Escobar; Alex Vigo, Federico Lértora, Rodrigo Aliendro, Gonzalo Piovi y Christian Bernardi; Tomás Chancalay y Wilson Morelo. DT: Eduardo Domínguez.

<br/>

🔵⚪ **Gimnasia**: Jorge Broun; Marcelo Weigandt, Paolo Goltz, Maximiliano Coronel y Matías Melluso; Víctor Ayala, Harrinson Mancilla y Eric Ramírez; Brahian Alemán y Matías García; Nicolás Contín DT: Leandro Martini-Mariano Messera.

<br/>

**Goles**: PT 29' Marcelo Weigandt (G) y ST 1 Nicolás Contín (G).

**Árbitro:** Germán Delfino.

**Estadio:** Brigadier López.