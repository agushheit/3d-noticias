---
category: Agenda Ciudadana
date: 2020-12-19T11:46:36Z
thumbnail: https://assets.3dnoticias.com.ar/1912-vacunacion.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Prensa Festram'
resumen: 'Festram: municipales deben ser prioritarios para la vacunación contra el
  Covid'
title: Festram pide que trabajadores de municipios y comunas sean considerados prioritarios
  para la vacunación contra el Covid
entradilla: 'La Federación de Sindicatos de Trabajadores Municipales de Santa Fe (Festram)
  elevó la solicitud al Gobernador Omar Perotti, a la ministra de Salud Sonia Martorano  y
  al ministro de Trabajo, Roberto Sukerman, '

---
La Federación de Sindicatos de Trabajadores Municipales de Santa Fe (Festram) envió notas al Gobernador Omar Perotti, a la ministra de Salud Sonia Martorano, y al ministro de Trabajo, Roberto Sukerman, a través de las cuales solicita «que las autoridades abocadas a determinar el personal y los grupos con prioridades para recibir la vacuna contra el COVID 19, incluyan a las trabajadoras y trabajadores municipales como grupo prioritario».

Festram recordó que fueron incorporados por la normativa nacional como sector exceptuado del «aislamiento social, preventivo y obligatorio», fundamentado en la calidad de servicio esencial que revisten las  tareas a cargo de los municipios en un contexto de pandemia.

«En la Provincia de Santa Fe el Poder Ejecutivo dictó innumerables medidas de coordinación y delegación de tareas con Municipios y Comunas. Entre ellas se incorporaron acciones de control para evitar la propagación del virus, revisión en los ingresos a las localidades, asistencia en salud con incorporación de centros de aislamiento locales y refuerzo en materia social, entre otras medidas de emergencia atribuidas; tareas estas que se instrumentaron a través de los trabajadores municipales y comunales», agregó Festram en un comunicado.

«Las consecuencias en nuestro sector -continúa-, y conforme a relevamientos desarrollados por esta Federación, arrojan que al menos 250 trabajadores municipales y comunales se contagiaron de COVID-19 en el ámbito laboral, y de esos contagios laborales se ha tenido que lamentar la irreparable pérdida de varios de ellos», asimismo, «un gran porcentaje aún padecen las consecuencias del virus, con pérdidas de su capacidad física -principalmente respiratoria- sin saber si será temporal o definitiva».

Festram menciona también que «es necesario resaltar que el trabajo de prevención de las ART y su respuesta frente a la enfermedad, fue calificada como "muy mala" por el 80% de los sindicatos consultados, principalmente los de tamaños medios y chicos en cuanto a la cantidad de afiliados».

Es por todas estas razones que Festram «plantea firmemente la necesidad de incorporar a los trabajadores municipales como sector prioritario para recibir la vacuna de COVID 19, ya que no solo están efectivamente expuestos al riesgo de contagio y muerte, sino que, además, están sin cobertura ni respuesta frente a las empresas privadas que deben prevenir el riesgo y responder frente a la ocurrencia de la enfermedad y sus consecuencias».