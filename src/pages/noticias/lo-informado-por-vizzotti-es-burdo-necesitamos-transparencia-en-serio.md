---
category: Agenda Ciudadana
date: 2021-02-23T06:06:18-03:00
thumbnail: https://assets.3dnoticias.com.ar/juanchi.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Juan Martín
resumen: "“Lo informado por Vizzotti es burdo, necesitamos transparencia en serio”"
title: "“Lo informado por Vizzotti es burdo, necesitamos transparencia en serio”"
entradilla: 'El diputado de Juntos por el Cambio, demanda que la ministra de Salud
  no se limite a brindar datos parciales, sino que de cuenta de todas las personas
  que han recibido la Sputnik V de manera discrecional. '

---
El diputado nacional Juan Martín elevó un pedido de Acceso a Información Pública, dirigido al poder ejecutivo nacional, en particular el ministerio de Salud que conduce Carla Vizzotti, para que la funcionaria brinde en detalle todo lo vinculado al escándalo de los vacunados VIP contra el coronavirus, sobre todo, de aquellas personas políticamente expuestas que recibieron dosis de manera discrecional. El legislador radical por Santa Fe marcó que la información difundida por el gobierno nacional resulta parcial, ya que por ejemplo en lo relacionado al vacunatorio del Hospital Posadas, solo se informó de quienes se inmunizaron en ese lugar el día 18 de febrero. “Lo informado por Vizzotti es insuficiente, necesitamos transparencia en serio”, afirmó el diputado.

En ese marco, el diputado radical solicita que se dé a conocer “la nómina de personas que recibieron la vacunación Covid-19 y que figuren encuadradas como “Personal Estratégico” a nivel nacional y/o en las 24 jurisdicciones y que no correspondan a las Fuerzas de Seguridad y Armadas, Docentes y personal no docentes de todos los niveles educativos y/o Personal del Servicio Penitenciario”.

“Exigimos sobre todo que se identifique a aquellas personas ‘políticamente expuestas’ que hayan recibido la vacuna respectiva, su encuadre en función de las etapas y públicos objetivos que avalan tal aplicación”, indicó el legislador en su pedido.

“Queremos saber los motivos por los cuales se autorizaron esas aplicaciones, así como la o las personas responsables de las decisiones en cada caso”, agregó Juan Martín en su solicitud. “Consideramos -concluyó- que es información que tiene que estar registrada en el Ministerio de Salud que ahora encabeza Vizzotti y debe hacerse pública porque la sociedad demanda transparencia y un plan de vacunación confiable y eficiente”.