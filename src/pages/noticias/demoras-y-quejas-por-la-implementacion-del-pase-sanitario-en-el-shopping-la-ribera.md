---
category: La Ciudad
date: 2021-12-22T06:15:28-03:00
thumbnail: https://assets.3dnoticias.com.ar/shallnotpass.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Demoras y quejas por la implementación del pase sanitario en el shopping
  La Ribera
title: Demoras y quejas por la implementación del pase sanitario en el shopping La
  Ribera
entradilla: 'Ante la queja de "inconstitucional" muchas personas se marcharon del
  lugar en el primer día de vigencia del pase sanitario por no acreditarlo. El ministro
  Pusineri defendió el accionar del shopping.

'

---
El primer día de vigencia del pase sanitario en Santa Fe ya planteó sus primeros interrogantes acerca de su aplicación. Esto se puso de manifiesto en el shopping La Ribera, lugar en donde se comenzó a pedir el requisito en el ingreso al lugar. Se registraron situaciones de malestar entre varias personas que por no tener el pasaporte no pudieron ingresar al lugar.

Al grito de "inconstitucional", grupos de personas se marcharon del lugar por no tener acreditada su vacunación en su celular o en papel. El pase sanitario que entró en vigencia este martes 21 de diciembre en la provincia y se puede acreditar a través de la app Cuidar, Mi Argentina o con el carnet físico de vacunación.

Distintos usuarios se quejaron de que la medida impuesta para fomentar la vacunación, argumentando que la misma "no respeta derechos y libertades de las personas". Se registraron demoras para ingresar al lugar a causa de estos reclamos, a pesar de que el personal de seguridad se ofreció a colaborar para que puedan obtener el pase sanitario en sus celulares, lo cual algunas personas rechazaron.

**"El shopping obró bien"**

Al respecto, el ministro de Trabajo de la provincia Juan Manuel Pusineri indicó que "las sanciones por incumplimiento son las mismas que cuando existían las restricciones". Y sobre esto agregó: "Si no se cumple con las normas sanitarias, se da intervención a la justicia. Si el shopping no dejó entrar a quien no tenía el pase, obró bien, de acuerdo a la norma vigente".

La incertidumbre esta puesta en cómo se controla su implementación y cómo se procede ante un incumplimiento de algún local, centro comercial o club en donde se dejen pasar personas sin haber acreditado su vacunación.

Incertidumbre generalizada

El mismo municipio fue quien reconoció a este medio que aún no hay certezas sobre la modalidad de implementación, fiscalización y control que rige con la vigencia del pase sanitario desde este martes 21 de diciembre. "Estamos aguardando una notificación formal del gobierno de la provincia para saber de qué manera coordinar la implementación del pase sanitario y su control", indicaron desde el Ejecutivo municipal.

La normativa debe aclarar en que medida se autoriza a la policía de la provincia y a los municipios a través de sus respectivas direcciones, para intervenir ante la realización de los controles necesarios y en su caso, disponer de las medidas que fueren procedentes ante un incumplimiento de alguno de los actores.

El decreto que lleva la firma del gobernador Omar Perotti, respecto a la implementación del pase sanitario a partir del 21 de diciembre, establece que "las autoridades municipales y comunales, en concurrencia con las autoridades provinciales competentes, coordinarán los procedimientos de fiscalización necesarios para garantizar el cumplimiento de las medidas dispuestas".