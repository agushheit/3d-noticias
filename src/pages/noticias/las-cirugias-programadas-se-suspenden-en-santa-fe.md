---
category: Estado Real
date: 2021-04-27T07:48:15-03:00
thumbnail: https://assets.3dnoticias.com.ar/hospital-Cullen.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: Las cirugías programadas se suspenden en Santa Fe
title: Las cirugías programadas se suspenden en Santa Fe
entradilla: La resolución rige desde este lunes y establece que sólo se realizarán
  procedimientos urgentes y oncológicos. “Comprendamos la gravedad porque debemos
  bajar los casos”, afirmó Martorano.

---
El Ministerio de Salud de la provincia dispuso este lunes la suspensión de cirugías programadas en el sector privado, en el marco de la pandemia de Covid 19 y ante la gran demanda de camas de internación en las últimas semanas. Esta medida, oficializada a través de una resolución de dicho organismo, se suma a la dispuesta desde el pasado 1° de abril para efectores del sector público.

En este sentido, la titular de la cartera sanitaria, Sonia Martorano, sostuvo: “El porcentaje de ocupación de camas es de un 90%, el recurso humano es finito y estamos al límite en la utilización del oxígeno. Por este motivo -continuó- firmamos la resolución por la cual se suspenden las cirugías en todo el ámbito de la provincia para el sistema privado, ya que en el sistema público la medida se adoptó el 1 de abril, lo que nos permitió poder continuar con la atención".

Cabe señalar que a partir de la fecha quedan suspendidas las cirugías programadas, exceptuando las urgencias o las oncológicas que no puedan reprogramarse. “Quedan todas las instituciones afectadas a la atención del Covid -sin desatender lo que sea no Covid- y al servicio de la población para atender esta pandemia”, agregó Martorano.

**COMPRENDER LA GRAVEDAD DE LA SITUACIÓN**

La ministra también ejemplificó algunas de las acciones adoptadas desde la cartera que dirige, en pos de sumar camas para la atención de pacientes Covid ante el incremento de contagios. “El hospital Centenario pasó de 14 a 40 camas; el Centro de especialidades médicas ambulatorias, Cemafe, ubicado en la ciudad capital, sumó 8 camas con respirador y 20 camas de internación general, es decir, de ser un centro de medicina ambulatoria pasó a ser un lugar de terapia intensiva y de internación general. Lo propio ocurrió con el hospital provincial en Rosario; en Rafaela contamos con 35 camas con respirador y están todas ocupadas”.

Asimismo señaló que en “el hospital modular que se inauguró ayer tenemos 20 camas con flujo de oxígeno que ya se están ocupando”. Y en ese marco afirmó: “Comprendamos la gravedad porque debemos bajar los casos”.

Por último, Martorano volvió a recalcar que el Covid “es un virus muy contagioso y muy agresivo. Las radiografías y tomografías que observamos denotan cómo se genera un daño pulmonar severo y la variación -en relación con la primera ola- es que las personas afectadas son jóvenes”.

“Pido que todos tomen conciencia porque hay un límite en el aumento de camas”, concluyó.

[Resolución N° 656](https://www.santafe.gob.ar/noticias/recursos/documentos/2021/04/2021-04-26NID_270779O_1.pdf)