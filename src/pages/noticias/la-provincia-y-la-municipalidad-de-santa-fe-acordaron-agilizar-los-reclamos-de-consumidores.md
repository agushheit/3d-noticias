---
category: Agenda Ciudadana
date: 2021-04-02T07:01:09-03:00
thumbnail: https://assets.3dnoticias.com.ar/CONSUMIDOR.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: La provincia y la Municipalidad de Santa Fe acordaron agilizar los reclamos
  de consumidores
title: La provincia y la Municipalidad de Santa Fe acordaron agilizar los reclamos
  de consumidores
entradilla: El convenio prevé que la Oficina Municipal del Consumidor podrá realizar
  las audiencias, que tendrán la validez formal para que los proveedores se vean obligados
  a acudir a la conciliación de partes.

---
El Ministerio de Producción, Ciencia y Tecnología de la provincia, a través de la Secretaría de Comercio Interior y Servicios, suscribió un convenio con la municipalidad de Santa Fe por el cual los reclamos de consumidores de la ciudad se resolverán en la misma instancia local. 

El acuerdo, suscripto por el secretario de Comercio Interior, Juan Marcos Aviano, y el intendente Emilio Jatón, prevé que la Oficina Municipal del Consumidor podrá realizar las audiencias entre las partes, que tendrán la validez formal para que los proveedores se vean obligados a acudir en la conciliación de partes, y así resolver en forma rápida y sencilla estos problemas.

Aviano expresó: "Hoy estamos cerrando el Mes de las y los Consumidores con otro hecho importante para hacerle la vida más fácil a la ciudadanía. Con este convenio ya no existe más el doble reclamo, ese largo camino que a veces se tenía que transitar, porque si al hacer el reclamo en la Municipalidad la empresa no acudía a resolverlo, luego se debía iniciar nuevamente en la oficina provincial”. 

Además, destacó que "no es correcto que el Estado te duplique la queja. Hoy estamos resolviendo en la ciudad de Santa Fe algo que ya hemos hecho en Rosario el año pasado y lo venimos haciendo a lo largo y ancho de la provincia en más de 25 oficinas locales".

Por su parte, la directora provincial de Promoción de la Competencia y Defensa del Consumidor, Betania Albrecht, resaltó que "apostamos a una política de descentralización de funciones, entendiendo que es el Estado local quien conoce en profundidad las realidades de sus ciudadanos y el primer espacio al que una persona se dirige ante la aparición de un inconveniente o conflicto. Estamos convencidos de que es necesario otorgar todas las herramientas posibles para buscar la solución de esos inconvenientes”.

De la actividad participó también el director de Derechos y Vinculación Ciudadana, Franco Ponce de León.