---
category: La Ciudad
date: 2020-12-02T17:48:31Z
thumbnail: https://assets.3dnoticias.com.ar/DESAGUES.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Informe de lluvias en la capital
title: Informe de lluvias en la capital
entradilla: La Municipalidad brinda una actualización de datos de lluvia y actuaciones
  con motivo de las precipitaciones de este miércoles.

---
La Municipalidad informa los datos de lluvia caída en la capital provincial este miércoles 2 de diciembre, cuando la red de estaciones meteorológicas propias registraron los siguientes datos acumulados:

**DOAE (Cobem):** 29,75 mm

**Centro:** 39,75 mm

**Alto Verde:** 29,75 mm

**CIC de Facundo Zuviría:** 47,50 mm

La intensidad máxima de lluvia fue de 87 mm/h, a las 8.50 hs., en el CIC de Facundo Zuviría. En tanto la ráfaga máxima de viento fue de 37,4 km/h, y se registró a las 10.10 horas, con dirección Sudoeste (SO).

## **Reclamos recibidos**

Se registraron a través del Sistema de Atención Ciudadana 28 reclamos relacionados con las lluvias hasta las 11.15 de la mañana del miércoles: por arbolado público (4), alumbrado urbano (1), recursos hídricos (7), higiene ambiental (14) y acción social (2).

Entre los reclamos, se registraron anegamientos, postes caídos, residuos o grandes podas en la vía pública y limpieza de bocas de tormenta y zanjas, entre otros.

Se recuerda que quienes tengan inquietudes o reclamos, pueden comunicarse con la **Línea de Atención Ciudadana** (0800-777-5000).