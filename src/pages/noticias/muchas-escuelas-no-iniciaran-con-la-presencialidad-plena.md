---
category: La Ciudad
date: 2021-08-30T06:00:45-03:00
thumbnail: https://assets.3dnoticias.com.ar/PRESENCIALIDADLUNES.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Muchas escuelas no iniciarán con la presencialidad plena
title: Muchas escuelas no iniciarán con la presencialidad plena
entradilla: Según Amsafé la Capital un gran número de instituciones educativas no
  volverán este lunes a la presencialidad plena "por falta de tiempo para su organización".

---
A partir de este lunes 30 de agosto, según lo anunció la ministra de Educación, Adriana Cantero, se retomará la presencialidad plena en todos los niveles educativos de las escuelas de la provincia. Si bien en los últimos días se brindaron detalles de la Circular Nº 25, a través de la cual se hace efectivo el anuncio con todos los detalles a toda la comunidad educativa, desde Amsafé La Capital confirmaron que muchas escuelas no podrán recibir a todos los alumnos de los distintos niveles por "falta de tiempo para la organización".

"Otras escuelas, directamente no podrán aplicar el nuevo protocolo por las distancias y no habrá presencialidad plena", indicaron a UNO Santa Fe desde Amsafé La Capital. Vale recordar que, en las últimas horas, la Asociación del Magisterio de Santa Fe (Amsafé), señaló que “el retorno” a la presencialidad escolar “debe ser gradual” porque “la forma intempestiva de retorno pleno genera malestar”.

A través de un comunicado, la entidad sindical respondió los dichos de la ministra de Educación santafesina, Adriana Cantero, quien había instado a los docentes a ser creativos y flexibles ante el regreso de la presencialidad sin alternancia, que comenzará este lunes en toda la provincia.

"A partir del lunes las escuelas podrán diseñar sus espacios para que la mayor cantidad de alumnos de la trayectoria obligatoria, acceda a la presencialidad plena ", comenzó diciendo este último viernes Cantero. "Puede ser que exista, sobre todo en las grandes ciudades, un número pequeño de escuelas que dada la gran cantidad de matrícula con la que cuentan y las dimensiones de sus espacios escolares requiere una intervención del ministerio para tener alternativas de excepcionalidad".

**El protocolo para la vuelta a la presencialidad**

Desde el área de Educación, recuerdan que el regreso a la presencialidad depende de tres condiciones: los departamentos se deben encontrar en contexto de bajo y medio riesgo epidemiológico y baja proporción de uso de camas de terapia intensiva; las escuelas deben estar en condiciones óptimas de infraestructura y servicios para su apertura a la comunidad educativa; y el chequeo permanente por parte de cada jurisdicción de las condiciones sanitarias, de manera de establecer las medidas a continuar o modificar.

**Para la apertura de las escuelas es necesario respetar:**

• Distanciamiento físico (actualmente establecido en 1.5 m entre los/as alumnos/as, que se extiende a 2 m en los espacios comunes de la escuela). El distanciamiento hacia y entre docentes también se establece en 2 m.

• Ventilación: debe ser permanente, manteniendo puertas y/o ventanas abiertas en todo momento, aún en épocas de baja temperatura. Asimismo, debe ser cruzada para permitir corrientes de aire que limiten el eventual contagio por aerosoles. Aquellos espacios que no cuenten con ventilación adecuada no deberán ser utilizados para las clases

• Uso permanente de barbijo/mascarilla, a partir de los 6 años de edad

• Higiene frecuente de manos, de manera de disminuir la posibilidad de contagio por contacto que, aunque menos frecuente, debe ser minimizada

• Aislamiento de casos sospechoso y de contactos estrechos