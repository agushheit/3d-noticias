---
category: La Ciudad
date: 2021-05-05T09:07:28-03:00
thumbnail: https://assets.3dnoticias.com.ar/terapia.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Diario Uno
resumen: Por falta de camas en el hospital público, se reclama a sanatorios para que
  reciban pacientes Covid
title: Por falta de camas en el hospital público, se reclama a sanatorios para que
  reciban pacientes Covid
entradilla: Desde el hospital público piden que se activen lugares para atenciones
  a pacientes con coronavirus en áreas críticas en el sector privado

---
Las camas críticas son un recurso esencial en esta etapa de la segunda ola en la ciudad de Santa Fe y un ejemplo fue el trabajo coordinado que se llevó a cabo este lunes entre las autoridades del Hospital Cullen y del viejo Hospital Iturraspe para lograr, en unas tres horas, la posibilidad de aumentar el número de camas en terapia para la atención de pacientes con coronavirus.

Juan Pablo Poletti en conferencia de prensa habló de ello y además llamó a que clínicas y sanatorios se esfuercen en cumplir con lo acordado el año pasado, de trabajar en conjunto para poder colaborar con el sistema de salud público.

"Este lunes 3 de mayo, por primera vez y durante algunas horas, el hospital se quedó sin camas críticas, pero con el trabajo en equipo con los demás nosocomios pudimos hacer las derivaciones correspondientes. El viejo Hospital Iturraspe pudo reubicar pacientes y de esta manera habilitar una nueva sala para terapia Covid, agregando seis nuevas camas. Todo se pudo hacer en tan solo tres horas, trabajando en equipo", remarcó.

Por otro lado aseveró: "La necesidad hoy en día es la de tener camas críticas, que no van a estar en el hospital militar, por lo cual de manera paralela en el Cullen estamos por comenzar una obra en el interior de la nueva unidad coronaria inaugurada recientemente, para abrir una boca de oxígeno en la red troncal que permitirá poder respirar cuatro camas más. Trabajos que comenzaremos en las próximas horas y en unos cinco días más ya lo podríamos tener resuelto".

También hizo referencia al inconveniente que están teniendo con el sector privado en relación a disponibilidad de camas para poder descomprimir la salud pública: "Cada vez que debemos de trasladar a un paciente con Covid tenemos inconvenientes porque los sanatorios refieren no tener camas y ante esta saturación sabemos que el paciente con o sin obra social va a llegar al público, por eso estamos intentando que en este tipo de situaciones, los privados puedan desarrollar la estrategia para la habilitación de camas".

"Los privados que hoy solamente reciben pacientes Covid son los que atienden Pami, que deben tener espacio disponible para ello, por eso la ministra de Salud ya avisó a las demás clínicas y sanatorios activar lugares para atenciones Covid en áreas críticas, ya que el hospital público tiene un límite. Era un acuerdo que se había firmado el año pasado y se debe llevar a cabo".

En tanto, el director de la Regional de Salud Santa Fe Rodolfo Rosselli sostuvo que en el departamento La Capital "en el sector privado, si bien la ocupación de camas es alta, estamos en un 75 por ciento. En cambio en el público estamos en un 90 por ciento y más también, dependiendo de las fluctuaciones diarias".