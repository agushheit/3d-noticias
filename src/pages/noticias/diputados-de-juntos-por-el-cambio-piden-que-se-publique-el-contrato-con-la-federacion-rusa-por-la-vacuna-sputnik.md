---
category: Agenda Ciudadana
date: 2020-12-29T11:04:20Z
thumbnail: https://assets.3dnoticias.com.ar/2912-gines-gonzalez-garcia.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: NCN'
resumen: Diputados de Juntos por el Cambio piden que se publique el contrato con la
  Federación Rusa por la vacuna Sputnik
title: Diputados de Juntos por el Cambio piden que se publique el contrato con la
  Federación Rusa por la vacuna Sputnik
entradilla: Solicitan la inmediata remisión del convenio firmado con la empresa de
  la Federación Rusa «Human Vaccine» Limited Liability Company, productora de la vacuna
  Gam-COVID-Vac, denominada Spunitk V.

---
A través de una carta enviada al titular de la cartera de Salud por los legisladores nacionales que integran la Comisión de Acción Social y Salud Pública de la Cámara baja, advirtieron además que «la llegada de un lote de vacunas Sputnik V a nuestro país implica que se ha suscripto un convenio con la empresa productora de la misma» y agregaron que «ese contrato no ha sido aún remitido y en caso de mantenerse el incumplimiento de la manda legal, nos veremos obligados a recurrir a la Justicia».

«El artículo 11 de la Ley 27.573 establece que los contratos celebrados en virtud de la presente ley deberán ser remitidos a las autoridades de la Comisión de Acción Social y Salud Pública de la Honorable Cámara de Diputados de la Nación y de la Comisión de Salud del Honorable Senado de la Nación, lo que hasta el momento no ha sucedido», indicaron en la misiva.

Asimismo, los diputados lamentaron que «desde ese Ministerio no se esté brindando la información que la ciudadanía necesita ni se acepte el camino del diálogo propuesto por quienes suscribimos la presente».

En ese sentido, añadieron que «resulta impropio de la función ministerial negarse a dar explicaciones afirmando que la vacuna es segura, pero sin indicar cómo lo saben, es decir, sin siquiera describir cuáles son los elementos de juicio que se han tenido en cuenta para aprobarla, considerando que fuimos el primer país que lo hizo fuera de la ex URSS, y que no hay publicaciones científicas que la avalen».

Los diputados que firmaron la carta son: Carmen Polledo, Rubén Manzi, Claudia Najul, Graciela Ocaña, José Cano, Silvia Lospennato, Hernán Berisso, Dina Rezinovsky, Gonzalo del Cerro, Lidia Ascárate, Soledad Carrizo, Sebastián Salvador, Gisela Scaglia, Leonor Matínez Villada, Camila Crescimbeni y Soher El Sukaria.

<br/>

[![Carta completa](https://assets.3dnoticias.com.ar/2912-preview-carta.webp)](https://assets.3dnoticias.com.ar/2912-carta-GGG.pdf "PDF")


[<span style="background-color: #212F3D; color: #ffffff; font-family: Helvetica, Arial, sans-serif; font-size: 0.9em; font-weight: bold;"> Leer la carta completa </span>](https://assets.3dnoticias.com.ar/2912-carta-GGG.pdf "Ver más")

<br/>