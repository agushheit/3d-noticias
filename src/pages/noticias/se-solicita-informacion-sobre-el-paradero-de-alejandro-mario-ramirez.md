---
category: La Ciudad
date: 2020-12-31T09:48:47Z
thumbnail: https://assets.3dnoticias.com.ar/3112-marioramirez.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: " Se solicita información sobre el paradero de Alejandro Mario Ramírez"
title: " Se solicita información sobre el paradero de Alejandro Mario Ramírez"
entradilla: A través de la Secretaría de Derechos Humanos y Diversidad de Santa Fe.

---
La Secretaría de Derechos Humanos y Diversidad de Santa Fe solicita cualquier información sobre el paradero de **Alejandro Mario Ramírez**, de 66 años, que está ausente de su hogar de la localidad Ángel Gallardo, provincia de Santa Fe, desde el 29 de diciembre de 2020.

Según consta en la denuncia, Alejandro Mario Ramírez tiene tez trigueña, es calvo, tiene ojos negros y contextura robusta.

En su búsqueda intervienen la comisaría 20º de la Unidad Regional I de la Policía y el Ministerio Público de la Acusación a cargo del doctor Fontana.

Alejandro Mario Ramírez padece problemas de salud mental, señala la denuncia.

**Se solicita la más amplia colaboración de toda la ciudadanía a los fines de recabar información que aporte a la búsqueda**. 

Ante cualquier dato por favor contactarse con la Secretaría de Derechos Humanos y Diversidad de Santa Fe (Mendoza 3443, de la ciudad de Santa Fe o Balcarce 1145 de la ciudad de Rosario), llamar a la **línea gratuita 0800-555-3348**, o bien al **celular 342 155357756**.