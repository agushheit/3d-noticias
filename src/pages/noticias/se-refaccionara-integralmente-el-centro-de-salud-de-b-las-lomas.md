---
category: La Ciudad
date: 2021-03-01T07:15:16-03:00
thumbnail: https://assets.3dnoticias.com.ar/CENTRO-DE-SALUD.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Se refaccionará integralmente el Centro de Salud de B° Las Lomas
title: Se refaccionará integralmente el Centro de Salud de B° Las Lomas
entradilla: 'En treinta días estarían comenzando las tareas de reparación. El presupuesto
  ronda los 10 millones de pesos.  '

---
En el marco del Plan Incluir, el Ministerio de Infraestructura, Servicios Públicos y Hábitat, efectuó un concurso de precios para refaccionar y poner en valor el Centro de Salud de barrio Las Lomas en la ciudad de Santa Fe.

El acto fue presidido por la ministra de Infraestructura, Servicios Públicos y Hábitat, Silvina Frana; quien estuvo acompañada por la secretaria de Arquitectura y Obras Públicas, Leticia Battaglia; el subsecretario de Planificación, Omar Romero; y el secretario ministerial, Andrés Dentesano.

"Esta obra es una intervención muy concreta en uno de los barrios en los que prácticamente no se hicieron obras durante años. Hay una decisión firme de que el Estado esté presente con obras en éste y en los barrios más vulnerables", explicó la ministra.

Frana afirmó que "realizamos un concurso de precios para que rápidamente podamos refaccionar el Centro de Salud de barrio Las Lomas, un efector que desde su inauguración en 2007 se fue deteriorando y no recibió a lo largo de estos años ningún tipo de intervención.

Por último, la ministra adelantó que "en los próximos treinta días, deberían estar comenzando las obras e indicó que en una segunda etapa se proyecta la construcción de un salón de usos múltiples (SUM) con distintos objetivos: talleres de contención, asistencia psicopedagógica y apoyo escolar entre las principales propuestas".

"Pondremos en valor el Centro de Salud, haciendo refacciones de revoques, cielo rasos, reparación de filtraciones, aberturas, sanitarios. Además, la obra incluye la construcción de las veredas, el pilar de la luz y el cerco perimetral. Lo vamos a poner en condiciones, para beneficiar no sólo a los vecinos, sino al personal de salud que actualmente desarrolla sus tareas en malas condiciones", detalló la secretaria de Arquitectura y Obras Públicas, Leticia Battaglia.

**Las ofertas**

Se recibieron dos ofertas, la primera correspondiente a Campana Juan Manuel, que cotizó por la suma de $ 9.360.456,92; y la segunda a BR Construcciones SRL, que presentó una propuesta económica de $ 9.913.225,21.