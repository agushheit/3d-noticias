---
category: Agenda Ciudadana
date: 2020-12-01T11:35:00.000+00:00
thumbnail: https://assets.3dnoticias.com.ar/cristina.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Infobae'
resumen: 'Duro revés para Cristina Kirchner: Casación validó la declaración de los
  arrepentidos'
title: 'Duro revés para Cristina Kirchner: la Cámara de Casación validó la declaración
  de los arrepentidos en el caso de los Cuadernos'
entradilla: Por dos votos a uno, el máximo tribunal penal de justicia rechazó los
  planteos en donde se pedía la inconstitucionalidad de la ley.

---
La causa de los cuadernos podrá seguir avanzando hacia juicio oral. La Cámara Federal de Casación Penal rechazó este lunes un planteo para que se declare la inconstitucionalidad de la ley de los arrepentidos. Se trata del caso de corrupción que tiene a la vicepresidenta Cristina Kirchner como acusada de ser la jefa de una asociación ilícita que recaudaba coimas de empresarios que hacían negocios con el Estado.

Los arrepentidos constituyen el corazón de la causa de los cuadernos. Oscar Centeno, el autor de los cuadernos, fue el primero de los arrepentidos en agosto de 2018 que desató la ola de pactos que se hicieron en la fiscalía de Carlos Stornelli. En total hubo 31 arrepentidos entre empresarios y ex funcionarios que buscaron aliviar su situación para salir de prisión o evitar una detención. Por eso había mucho temor en un sector de Comodoro Py y en la oposición de que el caso quedara en la nada.

El fallo fue rubricado por los jueces Ana María Figueroa, Daniel Petrone y Diego Barroeteveña, por dos votos a uno.

El primero en pronunciarse fue hace diez días el juez Barroetaveña, que rechazó los planteos. Luego, una operación de urgencia por una apendicitis lo obligó a internarse y estuvo de licencia hasta el viernes pasado. Mientras tanto, el expediente giró por el despacho de Figueroa, que se pronunció por hacer lugar a los recursos. El voto decisivo fue el del juez Petrone.

En una resolución de unas 200 páginas, por mayoría, el tribunal resolvió rechazar por mayoría los recursos de la defensa de Julio de Vido, Hernán Gómez y Juan Lascurain contra la ley del arrepentido.

“En el caso concreto de la Ley 27.304, los recurrentes deberían demostrar, de manera concreta y específica, de qué modo la norma cuya inconstitucionalidad alegan viola garantías y derechos reconocidos por la Carta Magna y, en ese ámbito de discusión, quedarán al margen del examen las motivaciones y finalidades que inspiraron a los legisladores para sancionar la norma en trato (por cierto, es útil memorar que la ley fue aprobada por ambas cámaras con una amplísima mayoría”, sostuvo el voto mayoritario en el fallo al que accedió Infobae.

Y resaltó: “No resulta ocioso recordar los objetivos perseguidos por la ley, en cuanto a que se sancionó con la finalidad de dotar al sistema de enjuiciamiento penal de mayores herramientas en busca de alcanzar resultados óptimos en la investigación, instrucción y juzgamiento de los delitos de corrupción –entre otros delitos incorporados– que en muchas ocasiones resultan sumamente complejos y de dificultosa determinación”.

En otra resolución de 6 páginas, los jueces también rechazaron los pedidos de la defensa de Cristina Kirchner, Julio de Vido y Roberto Baratta en donde se pedía la nulidad de la declaración de Oscar Centeno.

Se descuenta que las defensas buscarán que la Corte Suprema de Justicia analice y revoque esta decisión. Pero el fallo termina convalidando el avance de la causa rumbo a juicio oral. El expediente fue elevado en septiembre de 2019 al Tribunal Oral Federal 7.

Los planteos prometen reeditarse en esa instancia cuando el caso finalmente empiece en un debate oral, cuando los arrepentidos tengan que repetir lo que dijeron ante la fiscalía.

## **La ley del arrepentido**

![](https://assets.3dnoticias.com.ar/DEVIDO.jpg)

La Ley 27.304, promulgada en diciembre de 2016, habilita a un acusado a aportar ante la fiscalía datos relevantes y comprobables para el avance de una investigación a cambio de obtener una pena menor en el juicio oral.

Hace unos días, en medio de la expectativa por este fallo, el presidente Alberto Fernández había dicho que era una ley “definitivamente mala” porque se trataba de “un sistema de compra-venta de testimonios”. Y advirtió que se aplicó “casi simultáneamente en Ecuador, Brasil y Argentina: en los tres casos para perseguir dirigentes políticos opositores”. Lawfare.

Los planteos de las defensas habían sido revelados a fines de septiembre por Infobae, único medio presente en la audiencia de Casación en donde los representantes del ex ministro Julio de Vido, la abogada del ex funcionario Hernán Gómez y los representantes legales de los empresarios Gerardo Ferreyra y Juan Lascurain explicaron por qué, a su criterio, debían caer las declaraciones de los arrepentidos.

En aquella audiencia hubo tres críticas claves: la inconstitucionalidad de la ley, el uso de esta ley en la causa cuadernos (porque se votó en 2016 y se analizaban hechos del gobierno de los Kirchner) y la falta de registros fílmicos o audiovisuales de los dichos que los 31 acusados plantearon ante el fiscal Carlos Stornelli para conseguir convertirse en imputados-colaboradores y así garantizarse una pena segura, pero mínima, al momento del veredicto.

![](https://assets.3dnoticias.com.ar/STORNELLI.jpg)

La falta de grabaciones no fue una novedad en el mundo judicial: no existían. Pero fue la primera vez que quedó oficialmente certificado tras un pedido de la defensa de De Vido. Los fiscales Stornelli y Fabiana León –que será la acusadora si se concreta el juicio oral– habían justificado esa ausencia al señalar que la ley solo hablaba del registro con medios técnicos idóneos.

Un cuarto planteo fue sostenido por la defensa de Lascurain, y fue una reedición que ya había sostenido ante el Tribunal Oral Federal 7, que aún no había contestado a ese cuestionamiento: pidió el sobreseimiento de su cliente al sostener que el autor de los cuadernos, Oscar Centeno, estaba acusado por un delito más grave que su cliente, y por lo tanto se incumplía con la ley del arrepentido que obliga al confesar disparar hacia arriba. Ese argumento serviría para la mayoría de los empresarios acusados que quedaron afuera de la asociación ilícita, cuando Centeno quedó como un participante de esa banda que, según el fiscal Stornelli y el fallecido juez Claudio Bonadio, encabezó Cristina Kirchner.

En la audiencia, las defensas hablaron de la teoría del fruto del “árbol envenado”, la doctrina que establece que si una prueba se obtuvo de manera ilícita no puede ser usada en un proceso judicial. Esa doctrina fue, precisamente, lo que hizo caer la investigación de la causa AMIA, cuando el caso llegó a juicio y los que terminaron acusados fueron los investigadores.

## **La causa**

El caso de los cuadernos de la corrupción marcó un punto de inflexión en Comodoro Py cuando estalló el 31 de julio de 2018 y derivó en una seguidilla de ex funcionarios y empresarios presos. Fue entonces cuando una veintena de empresarios y ex funcionarios, acusados de una asociación ilícita encabezada por la ex presidenta Cristina Kirchner, reconocieron haber pagado o cobrado coimas en el marco de los negocios, licitaciones o convenios que surgían desde el Ministerio de Planificación. Algunos explicaron que lo hacían porque les pedían plata para la campaña, otros porque si no ponían dinero no obtenían licitaciones o no conseguían que se liberaran los pagos por los negocios que ya tenían con el Estado.

Lo cierto es que cada uno de los declararon como imputados-colaboradores ante el fiscal Stornelli lo hizo sabiendo que debía aportar datos ciertos y valiosos para la investigación, apuntando “hacia arriba”. A cambio, los que estaban presos consiguieron la libertad y los que podían quedar detenidos consiguieron seguir el proceso sin ir a cárcel.

Con su arrepentimiento, firmaban el reaseguro de que, a la hora de la sentencia, tendrían una reducción de la pena. Ahí se anotaron los ex funcionarios José López y Claudio Uberti, el financista Ernesto Clarens y empresarios que van desde Carlos Wagner hasta Angelo Calcaterra, pasando por Aldo Roggio y Enrique Pescarmona.

Las defensas cuestionaron desde el primer momento la aplicación de la ley en esta causa. Pero la Sala I de la Cámara Federal, con la firma de los jueces Leopoldo Bruglia y Pablo Bertuzzi –hoy en el centro de la polémica por sus traslados y desplazados del segundo piso de Comodoro Py– ratificaron los procesamientos dictados, validaron las declaraciones de los arrepentidos y elevaron el caso a juicio oral. El tema ya está en manos del Tribunal Oral Federal 7, a cargo de Fernando Canero, Enrique Mendez Signori, y, hasta hace unos días, Germán Castelli, otro de los magistrados desplazados por el Senado porque llegó a ese lugar por un traslado.

Allí Cristina Kirchner está acusada de ser la jefa de una asociación ilícita –junto al fallecido ex presidente Néstor Kirchner– de haber comandado una asociación ilícita que funcionó entre principios de 2003 y noviembre de 2015, “cuya finalidad fue organizar un sistema de recaudación de fondos para recibir dinero ilegal con el fin de enriquecerse ilegalmente y de utilizar parte de esos fondos en la comisión de otros delitos, todo ello aprovechando su posición como funcionarios del Poder Ejecutivo Nacional”.

Por debajo de la hoy vicepresidenta, estaban De Vido y Baratta, Clarens y Wagner, de la compañía Esuco, que presidió la Cámara Argentina de la Construcción; y por abajo los “integrantes”, entre los que aparecen los arrepentidos José López, Claudio Uberti y Centeno, que se convirtió en testigo protegido. Los empresarios, que en un primer momento habían sido incluidos por Bonadio en la asociación ilícita, quedaron acusados por la Cámara Federal solo por haber entregado coimas.

[Fallo causa 9608](https://assets.3dnoticias.com.ar/FALLO-Causa9608 -FERNANDEZ,-Cristina-Elisabet-INC100.pdf)