---
category: El Campo
date: 2021-03-24T07:42:36-03:00
thumbnail: https://assets.3dnoticias.com.ar/corral.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa José Corral
resumen: 'José Corral: “Defender la industria de los biocombustibles es cuidar empleo
  santafesino”'
title: 'José Corral: “Defender la industria de los biocombustibles es cuidar empleo
  santafesino”'
entradilla: Lo afirmó el referente de Juntos por el Cambio, tras una visita a la planta
  de la firma BioEnergy en Roldán, junto al diputado nacional Juan Martín y el dirigente
  rosarino Eugenio Malaponte.

---
Lo afirmó el referente de Juntos por el Cambio, tras una visita a la planta de la firma BioEnergy en Roldán, junto al diputado nacional Juan Martín y el dirigente rosarino Eugenio Malaponte. Allí se reunieron con representantes de la empresa y del sector, a quienes les manifestaron apoyo a la prórroga de la ley de biocombustibles, que vence en mayo. “Es una actividad que agrega valor a lo que produce el suelo santafesino”, destacó José Corral.    

“Nos parece imperioso que se prorrogue la Ley de Biocombustibles y que se discuta mientras tanto una nueva regulación que tome en cuenta todo lo que creció el sector en este tiempo. Defender la industria de los biocombustibles es cuidar el empleo santafesino”. De este modo el dirigente radical y referente de Juntos por el Cambio, José Corral, sintetizó su respaldo a que se prorrogue la ley nacional que establece el corte de la nafta y el gasoil con una porcentaje de biocombustibles, extensión que será tratada en Diputados este jueves por impulso de la oposición y sin apoyo del kirchnerismo. 

José Corral hizo estas declaraciones tras una visita a la planta de la firma BioEnergy, en el área industrial de Roldán, en el sur santafesino, acompañado por el diputado nacional Juan Martín y el dirigente radical rosarino, Eugenio Malaponte. Allí mantuvieron un encuentro con el responsable de la firma, Sebastián Pucciarello; el titular de la Cámara de Biocombustibles, Juan Facciano, y demás representantes del sector. 

Tras la reunión, José Corral dijo: “Transmitimos nuestro respaldo a la prórroga de la Ley de Biocombustibles que vence el 12 de mayo, y que obliga a incluir un porcentaje de biocombustibles en la nafta y el diesel. Eso ha permitido el desarrollo de una industria en todo el país, pero particularmente potente en la provincia de Santa Fe por la producción agrícola existente en la provincia, que se estima que emplea en el orden de los 5.000 trabajadores entre puestos directos e indirectos. El biodiesel es una manera de agregarle valor a lo que producimos en el suelo santafesino”. 

Para José Corral, la producción de biocombustibles “da vida y actividad a pueblos y ciudades intermedias. Es la nueva ruralidad de la que hablamos. Siempre insistimos con el desarrollo de la bioeconomía, que es justamente estas cadenas de valor que conectan o enlazan la producción agrícola con la producción de energía, combustibles en este caso, incorporando tecnología nacional”.  “Los directivos de Bioenergy nos contaron que han participado grupos de la Facultad de Ingeniería Química de la Universidad Nacional del Litoral en el armado de la planta y hay muchas pymes que proveen o que compran sus productos de esta industria que es parte de una cadena de valor y de trabajo”, contó el dirigente radical. “Por lo tanto -reiteró- nos parece imperioso que prorrogue la obligación del corte de naftas y gasoil y que se discuta mientras tanto una nueva regulación que tome en cuenta todo lo que creció el sector en este tiempo”. 

**Contradicciones del kirchnerismo**

José Corral recordó que “justamente un gobierno del mismo signo político del actual es el que puso en marcha esta ley que vence en mayo. Por lo tanto la única explicación de que no se resuelva esta situación son los intereses de la industria petrolera, que nos parece que también hay que desarrollar en el país. Pero eso no puede ser en desmedro de una actividad tan importante y vigorosa como la producción de combustibles sustentables”. 

Para el referente de JxC “la opción no es combustibles fósiles o energías renovables, sino combustibles fósiles y energías renovables. O sea, Vaca Muerta y agroindustria”. “El país necesita diversificar su matriz energética y esta es una manera virtuosa de hacerlo”, concluyó.