---
category: Agenda Ciudadana
date: 2021-06-15T07:49:14-03:00
thumbnail: https://assets.3dnoticias.com.ar/ecomerce.png
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: La provincia lanza un Programa de capacitación y aceleración para promover
  que profesionales y pymes exporten a través del comercio electrónico
title: La provincia lanza un Programa de capacitación y aceleración para promover
  que profesionales y pymes exporten a través del comercio electrónico
entradilla: Está destinado a diseñar e implementar un plan integral para promover
  las exportaciones mediante sus propias tiendas en línea y/o los marketplaces con
  foco en el mercado local, de Latam, regional y global.

---
El gobierno de la provincia realizará este miércoles desde las 17:30hs la presentación del Programa de capacitación y aceleración Ecommerce Crossborder de Servicios Profesionales y Bienes, herramienta destinada a diseñar e implementar un plan integral para promover las exportaciones a través del Comercio Electrónico Transfronterizo de las MiPyMES y los profesionales de la de la provincia, mediante sus propias tiendas en línea y/o los marketplaces con foco en el mercado local, de Latam, regional y global.

El programa que es impulsado por la Secretaría de Comercio Exterior y Agencia Santa Fe Global de la Provincia de Santa Fe, en conjunto con el Consejo Federal de Inversiones (CFI) y el eCommerce Institute, está destinado a profesionales y técnicos de la provincia que estén interesados en vender sus servicios a través de plataformas digitales: ingenieros, arquitectos, diseñadores, abogados, traductores, desarrolladores y afines. También integrantes de empresas, emprendedores, pymes y usuarios en general que desarrollen su actividad en el sector productivo de la provincia.

De la presentación participará el ministro de Producción, Ciencia y Tecnología, Daniel Costamagna; el secretario de Comercio Exterior, Germán Burcher; junto al secretario General del CFI, Ignacio Lamothe; entre otras autoridades.

**ACTIVIDADES VIRTUALES**

Todas las actividades del programa se realizarán de manera 100% virtual y se brindará mayor información sobre el mismo en el Webinar que habrá a continuación del acto de apertura. Esta instancia estará a cargo del presidente del eCommerce Institute, Marcos Pueyrredon y del director del Programa, Sebastián Herrera.

En dicho primer encuentro se brindará una introducción al programa y servirá para abordar la importancia de los servicios y datos en el marketing internacional online y la competitividad global y la estrategia comercial para generar ventas. En tanto, que el segundo encuentro tendrá lugar el próximo 23 de junio.

Los interesados en participar del lanzamiento y el Webinar deben inscribirse ingresando al siguiente enlace: [https://ecapacitacion.org/webinar-programa-crossborder-ecommerce/](https://ecapacitacion.org/webinar-programa-crossborder-ecommerce/ "https://ecapacitacion.org/webinar-programa-crossborder-ecommerce/")