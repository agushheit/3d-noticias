---
category: La Ciudad
date: 2021-11-10T06:00:45-03:00
thumbnail: https://assets.3dnoticias.com.ar/EMOTIVO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'Cardiopatías congénitas: el Hospital Alassia hará intervenciones de alta
  complejidad desde 2022'
title: 'Cardiopatías congénitas: el Hospital Alassia hará intervenciones de alta complejidad
  desde 2022'
entradilla: "Equipos de salud se capacitan con personal del Garrahan y este martes
  estuvo en la ciudad el jefe del servicio de ese hospital, referente en todo el país.\n\n"

---
Un emotivo acto se desarrolló este martes por la mañana en el Hospital de Niños Orlando Alassia. Hubo recuerdos y reconocimientos para conmemorar diez años de la Fundación de Cardiopatías Congénitas y un año desde la recuperación de intervenciones quirúrgicas en ese centro de salud.

 Allí estuvieron integrantes de la Fundación, que cumplió su aniversario en 2020 (es decir, en plena pandemia), junto con el gobernador Omar Perotti, la ministra de Salud Sonia Martorano, el secretario del área Jorge Prieto, la ministra de Obras Públicas e Infraestructura Silvina Frana, el director del Hospital de Niños Osvaldo González Carrillo y numerosas autoridades locales, provinciales y de otras ciudades santafesinas. Junto con todos ellos, llegó a la capital provincial el jefe de Cirugía Cardiovascular en el Hospital Garrahan, Dr. Pablo García Delucis, cuyo equipo trabaja de manera coordinada con profesionales santafesinos en la capacitación necesaria y permanente de manera que las intervenciones de alta complejidad se realicen también en Santa Fe.

 Aquí la palabra clave es descentralizar, "que significa no solamente que un chico o una chica con una cardiopatía congénita sea tratado en su lugar de origen y cerca de sus afectos; tiene que ver también con la formación de un recurso humano especializado a nivel local", sostuvo Delucis en diálogo con los medios. El objetivo es que pacientes con estas patologías "puedan ser tratados en su lugar, en un ambiente familiar sin tener que trasladarse a otra ciudad". Todo ello, garantizó el profesional, tiene un impacto positivo en la recuperación.

 En Santa Fe ya se realizaron cirugías de mediana y baja complejidad pero "el hospital está capacitado para hacer cirugías de alta complejidad donde tienen que atenderse desde las cuestiones más simples a las más complejas, y para eso necesita de todos dentro del hospital, no solamente el equipo del quirófano".

La ministra de Salud Sonia Martorano evaluó como "histórico" el avance que fue anunciado hoy,, reconoció la decisión política del gobernador Omar Perotti para llevarlo a cabo y señaló que desde aquel momento en que se volvieron a realizar cirugías cardiovasculares en el hospital se completaron numerosos procedimientos de manera exitosa.

 En esta nueva etapa, "el centro-norte de la provincia no tendrá que derivar a estos niños. Viene un equipo que es de lo mejor del país, a hacer cirugías pero también a trabajar en la formación del equipo local. Estamos avanzados en la formación del talento humanos en la provincia y para eso se está formando in situ con los mejores del país", señaló la ministra.

 A su turno, Silvina Frana, de Obras Públicas e Infraestructura, confió en que el sector de cardiopatías comenzará a funcionar en el corto plazo: "En diciembre se va a licitar una primera etapa de la intervención general que se va a hacer sobre el hospital, de planta baja y primer piso incluida la ampliación de la Guardia. En el primer piso habrá instrumental de diagnóstico por imágenes". Para ello serán destinados unos 1000 millones de pesos y nueve meses de ejecución de obra.

 La segunda etapa, cuya licitación está prevista para enero de 2022, estará destinada a oncología infantil. En este caso, en conjunto con la Fundación Mateo Esquivo.

 **"Un antes y un después"**

 Ramiro Puyol fue el encargado de recordar la génesis de la fundación y de homenajear con nombre y apellido a quienes colaboraron para que su trabajo siguiera creciendo.

 "Una no logra dimensionar bien lo que significa esto", señaló más tarde Mónica Patiño, referente de la Fundación. "La resolución de las cardiopatías congénitas de manera quirúrgica tiene que ver con el cirujano, el equipo y la institución". Muy emocionada, consideró que la posibilidad de contar con estos espacios en el Hospital Alassia significa "un antes y un después para las familias", y para los pacientes "menos muerte, menos secuela, menos recuperación".

 Antes de todo eso, de los diálogos con la prensa y de los anuncios y cuando aún faltaban varios detalles emotivos en las palabras y los gestos que acompañaron el acto, una frase recibió desde una gran pantalla a quienes se acercaron este martes al patio del Hospital Alassia: "No puede haber una revelación más intensa del alma de una sociedad que la forma en la que trata a sus niños...".

 