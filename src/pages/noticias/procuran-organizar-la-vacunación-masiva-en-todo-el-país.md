---
layout: Noticia con imagen
author: "Fuente: NA"
resumen: Vacunación masiva Covid-19
category: Agenda Ciudadana
title: Procuran organizar la vacunación masiva en todo el país
entradilla: El Comité de Vacunación de reunirá este lunes para definir logística
  y distribución. Primero serán vacunados los grupos de riesgo.
date: 2020-11-22T13:56:39.139Z
thumbnail: https://assets.3dnoticias.com.ar/vacunaci%C3%B3n.jpeg
---
El presidente Alberto Fernández encabezará el próximo lunes la reunión del Comité de Vacunación para poner en marcha la logística que demandará la distribución y aplicación de las vacunas con las que el Gobierno espera contar en los próximos meses.

El mandatario convocó al comité que integran los ministerios de Salud, de Defensa y de Seguridad para comenzar a diagramar el operativo y "empezar cuanto antes, entre fines de diciembre y los primeros días de enero" con la aplicación de la vacuna, según indicó el mandatario días atrás.

La idea del Poder Ejecutivo es tener en marcha la logística para cuando lleguen las primeras vacunas, en el marco de los acuerdos que está cerrando con distintos países y laboratorios para garantizarse ese insumo para la lucha contra el Covid-19.

En ese contexto, el Gobierno ya avanzó con la compra de 20 millones de jeringas descartables para utilizar en la inmunización de la población, según se publicó en el Boletín Oficial el pasado viernes.

A través de la Resolución 32/2020, el Ministerio de Salud aprobó una contratación de emergencia para comprar la impresionante cantidad de jeringas descartables con el objetivo de "**implementar la estrategia de vacunación COVID 19 que se desarrollará en todo el país durante el primer semestre del año 2021**".

Para adquirir los insumos el Gobierno destinará la suma de 58.400.000 pesos -es decir, 2,92 pesos cada jeringa- que saldrán de las partidas presupuestarias de la cartera conducida por Ginés González García para los ejercicios 2020 y 2021.

La empresa beneficiaria de la licitación fue Propato Hnos S.A.I.C., una firma dedicada a la fabricación y venta de insumos médicos como apósitos, máscaras, gasas, catéteres y desfibriladores, entre otros.

**Se prevé que las vacunas Sputnik V, AstraZeneca y Pfizer lleguen a la Argentina entre fines de diciembre y marzo, mientras que hay gestiones para conseguir también una de las producidas en China**.

La que está más cerca de llegar a la Argentina es la vacuna rusa Sputnik V producida por el Centro Nacional de Epidemiología y Microbiología de Gamaleya, de la cual el país podría tener 20 millones de dosis.

El jefe de Gabinete, Santiago Cafiero, afirmó este sábado que el Gobierno espera poder firmar el precontrato de compra de esa vacuna la semana que viene.

La expectativa del Gobierno es que, si resulta aprobada por la ANNMAT, la vacuna se aplique a unas 10 millones de personas a partir de finales de diciembre, según lo que adelantó Fernández en los días pasados.

En tanto, el 7 de noviembre pasado el Gobierno confirmó la firma del acuerdo con el laboratorio AstraZeneca para la compra de 22 millones de dosis de su vacuna, elaborada junto con la Universidad de Oxford, y espera que lleguen en el primer semestre de 2021.

El Gobierno también espera conseguir unas 700 mil dosis de la vacuna de Pfizer, la farmacéutica norteamericana que confía en lanzar rápidamente su producto una vez que tenga la aprobación de la Administración de Drogas y Alimentos de los Estados Unidos (FDA).