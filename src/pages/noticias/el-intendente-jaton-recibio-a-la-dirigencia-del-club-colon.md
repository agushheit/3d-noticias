---
category: Deportes
date: 2021-09-30T06:00:20-03:00
thumbnail: https://assets.3dnoticias.com.ar/JATONCOLON.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: El intendente Jatón recibió a la dirigencia del Club Colón
title: El intendente Jatón recibió a la dirigencia del Club Colón
entradilla: En el encuentro, las partes conversaron sobre la vuelta del público a
  la cancha, hecho que se producirá este lunes cuando Colón reciba en su estadio a
  Banfield.

---
Este miércoles, el intendente Emilio Jatón recibió al presidente del Club Atlético Colón, José Vignatti. En la oportunidad, se dialogó sobre el retorno de los hinchas a las canchas de fútbol que, en la capital de la provincia se producirá el próximo lunes por la noche, cuando Colón reciba a Banfield.

Las partes conversaron sobre la capacidad del estadio “Brigadier López”, el aforo determinado por la autoridad sanitaria y los preparativos para el evento. En ese sentido, el municipio confirmó que acompañará el acontecimiento con los servicios que presta habitualmente, es decir, el ordenamiento del tránsito y el control del expendio de bebidas en las inmediaciones de la cancha, entre otros.

Por su parte, la dirigencia valoró la convocatoria y la consideró muy importante, teniendo en cuenta que no son pocos los que anhelan volver a presenciar los partidos. Del mismo modo, agradeció que desde el municipio, en la figura de su intendente Emilio Jatón, se aborde esta situación.

En esa instancia, Vignatti ratificó que la idea es la entrada exclusiva de socios y confirmó que actualmente se venden abonos para ingresar, por lo que se puede llevar un control minucioso de la cantidad de asistentes, para garantizar el 50% de aforo.

Concluido el encuentro, el secretario de Producción y Desarrollo Económico municipal, Matías Schmüth, aseguró que nos “parecía muy importante conversar sobre cómo la institución organiza el evento”. En ese sentido, afirmó que “la vuelta del público es una gran noticia para una ciudad como la nuestra, donde el fútbol es una fiesta”.

Del mismo modo, aseguró que “nos pareció propicia la reunión para conocer cómo se prepara el retorno de los hinchas, teniendo en cuenta que es una buena posibilidad que la autoridad sanitaria permita la vuelta del fútbol con público”.

Finalmente, Schmüth anticipó que las conversaciones continuarán con los directivos del Club Atlético Unión, cuyos hinchas volverán al estadio el viernes 9 de octubre.

**Participantes**

La reunión, convocada por el municipio, también contó con la presencia del secretario general del municipio, Mariano Granato; y el secretario de Control y Convivencia Ciudadana, Fernando Peverengo. En tanto, representando a la entidad rojinegra, estuvieron Espartaco Sandaza y Gustavo Ingaramo.