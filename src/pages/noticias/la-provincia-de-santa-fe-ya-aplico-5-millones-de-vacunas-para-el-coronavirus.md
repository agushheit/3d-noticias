---
category: Estado Real
date: 2021-10-31T18:00:48-03:00
thumbnail: https://assets.3dnoticias.com.ar/coronaqueen.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La provincia de Santa fe ya aplicó 5 millones de vacunas para el coronavirus
title: La provincia de Santa fe ya aplicó 5 millones de vacunas para el coronavirus
entradilla: En este contexto, la ministra Martorano aseguró que todos los santafesinos
  mayores de 3 años llegarán a fin de año con el esquema completo de vacunación.

---
La provincia de Santa Fe continúa con el histórico operativo de vacunación para el Coronavirus en todo el territorio, así es como ya se aplicaron 5 millones de dosis. Ante esto, la ministra de Salud, Sonia Martorano agregó: “Lo importante es que esto representa que el 64% de los santafesinos ya tiene esquema completo de inmunización”.

Del mismo modo, la funcionaria de la cartera sanitaria destacó: “Estamos avanzando fuertemente con la población de 3 a 11 años y ya vacunamos a más de 165 mil personas, mientras que de 12 a 17 ya aplicamos 210 mil dosis y 40 mil chicos ya completaron el esquema”.

A su vez, Martorano hizo hincapié en las terceras dosis que empezaron a aplicarse en la semana y puntualizó: “Estamos aplicando vacunas Astrazeneca a quienes hayan recibido las dos dosis de Sinopharm y sean mayores de 50 años, al igual que a los inmunodeprimidos (independientemente de las vacunas aplicadas)”.

Vale aclarar que aquellos que no recibirán terceras dosis, no deben volver a inscribirse en el Registro Provincial de Vacunación, sino que serán citados directamente por mensaje de texto o correo electrónico, ante cualquier consulta pueden comunicarse al 0800 555 6549.