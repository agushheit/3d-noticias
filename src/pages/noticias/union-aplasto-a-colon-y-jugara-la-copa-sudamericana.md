---
category: Deportes
date: 2021-12-12T06:15:46-03:00
thumbnail: https://assets.3dnoticias.com.ar/UNION.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Unión aplastó a Colón y jugará la Copa Sudamericana
title: Unión aplastó a Colón y jugará la Copa Sudamericana
entradilla: 'En un desbordante 15 de Abril, Unión goleó 3-0 a Colón, con los gritos
  de Machuca, García y González, y se metió en la edición 2022 de la Copa Sudamericana

'

---
Unión se quedó con una nueva edición del Clásico Santafesino, al golear en el 15 de Abril a Colón por 3 a 0, resultado que lo metió por tercera vez en su historia en la Copa Sudamericana. Imanol Machuca (PT 8') y Juan Manuel García (PT 33'), ambos en el primer tiempo; y Gastón González (ST 48') marcaron los tantos.

Unión salió a jugar el partido con actitud y personalidad, mientras que Colón pareció hacerlo condicionado por la final con River. La intensidad con la que jugó uno y otro fue muy diferente, y ya desde el comienzo, fue el Tate quien marcó el ritmo del partido. Presionando arriba y recuperando la pelota y ganando por las bandas.

Colón se mostraba lento y llegaba tarde la mayoría de las veces. Avisó Unión con un remate de Corvalán que se fue desviado. Pero a los 8 minutos llegaría la apertura del marcador.

Tiro libre frontal en favor de Unión y una acción de distracción en una pelota quieta preparada para que Imanol Machuca con un remate rasante colocara el balón al lado del caño derecho del arco defendido por Burián, quien no atino a reaccionar.

Rápidamente pudo aumentar Unión, con un remate de Mauro Luna Diale debajo del arco que logró interceptar Paolo Goltz. Colón inquieto con dos pelotas paradas ante sendos tiros de esquina y los cabezazos desviados de Goltz y Lértora.

Pero el Tate lucía mejor y tuvo una chance inmejorable cuando Picotón González asistió a Juan Manuel García, quien quedó mano a mano con Burián pero falló en la definición. A los 34 minutos Juanchón tendría revancha, con un cabezazo ante la mala salida de Burián para decretar el 2 a 0 parcial y darle tranquilidad al equipo.

El Tate jugó con intensidad y concentración y por eso se fue a los vestuarios con un resultado justo de dos tantos contra cero ante su tradicional adversario.

En el segundo tiempo se esperaba una reacción de Colón, que nunca llegó, avanzó pero jamás atacó y nunca complicó al Tate. Incluso en el inicio del segundo tiempo, Gastón González estuvo cerca de anotar el tercero pero su remate se fue por encima del travesaño.

Unión esperaba muy atrás recostado en el resultado ante un equipo sabalero que jamás estuvo en partido y que en grandes pasajes del partido pareció estar en una práctica o en un amistoso de pretemporada. La cuestión mental fue clave Unión lo quiso jugar y ganar y Colón se quiso sacar el compromiso, sabiendo que en una semana juega una final con River.

Está claro que ese factor resultó determinante, sino no se explica la manera en que Colón jugó este Clásico. Y fue siempre Unión el que estuvo cerca de aumentar el marcador. Lo tuvo Cuqui Márquez y no definió bien, rematando a las manos de Burián. Pero cuando se jugaba tiempo de descuento, González ganó en velocidad por izquierda, hizo una pared con Nardoni y se fue de cara al arco para definir cruzado y decretar el 3 a 0 y desatar la locura en el 15 de Abril porque no solo goleaba sino que se metía en la Copa Sudamericana.

Fue sin dudas una tarde soñada para Unión, ya que goleó a su rival de toda la vida amplió la diferencia en el historial y además le puso la frutilla al postre con su tercera participación en un certamen internacional. Un cierre inmejorable de año para el Tate y a Colón sin dudas que le queda el objetivo más importante y eso se notó a lo largo de los 90 minutos. Unión lo jugó como un Clásico y Colón como un partido más eso explica el resultado final.

**SÍNTESIS DEL CLÁSICO**

**Unión 3:** Sebastián Moyano; Federico Vera, Franco Calderón, Emanuel Britez, Claudio Corvalán; Imanol Machuca, Enzo Roldán, Juan Carlos Portillo, Gastón González; Mauro Luna Diale y Juan Manuel García. DT: Gustavo Munúa.

**Colón 0:** Leonardo Burián; Facundo Garcés, Paolo Goltz y Rafael Delgado; Facundo Mura, Rodrigo Aliendro, Federico Lértora, Christian Bernardi y Gonzalo Piovi; Cristian Ferreira y Facundo Farías. DT: Eduardo Domínguez.

**Goles:** PT 8' Imanol Machuca (U) y 33' Juan Manuel García (U); ST 48' Gastón González (U).

**Cambios:** ST 0' Eric Meza y Lucas Beltrán x Facundo Mura y Facundo Garcés (C); 12' Kevin Zenón x Imanol Machuca (U); 21' Fernando Márquez y Juan Nardoni x Juan García y Enzo Roldán (U); 28' Santiago Pierotti y Nicolás Leguizamón x Cristian Ferreira y Facundo Farías (C)

**Amonestados:** Vera, Britez, Calderón (U); Piovi, Beltrán, Lértora, Goltz (C)

**Árbitro:** Fernando Rapallini.

**Estadio:** 15 de Abril.