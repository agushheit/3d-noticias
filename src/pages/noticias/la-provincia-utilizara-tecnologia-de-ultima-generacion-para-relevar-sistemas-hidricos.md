---
category: Estado Real
date: 2021-02-17T06:59:37-03:00
thumbnail: https://assets.3dnoticias.com.ar/hidricas.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: La provincia utilizará tecnología de última generación para relevar sistemas
  hídricos
title: La provincia utilizará tecnología de última generación para relevar sistemas
  hídricos
entradilla: Permitirá identificar las obras prioritarias de los Comités de Cuenca,
  además de diagnosticar, diseñar y planificar obras hídricas en el territorio santafesino.

---
El Ministerio de Infraestructura, Servicios Públicos y Hábitat, licitará la contratación de un servicio de auditoría para la ejecución de un Relevamiento Topográfico aéreo con tecnología Lidar de sistemas hidrológicos. El objetivo es mejorar la gestión del recurso hídrico en la provincia, obtener un conocimiento acabado del funcionamiento de las cuencas y la identificación de áreas con riesgo de afectaciones por anegamientos, y de esta manera planificar obras de canalizaciones de excedentes pluviales o las que permitan la contención del avance de las aguas naturales frente a crecidas o precipitaciones excesivas. Cuenta con un presupuesto oficial de 70 millones de pesos.

Al respecto, el secretario de Recursos Hídricos, Roberto Gioria, manifestó: "Este es un sistema de alta tecnología con un sensor láser activo que se maneja desde una aeronave tripulada. Esta tecnología Lidar permitirá relevar y producir información topoaltimétrica de alta resolución, optimizando los trabajos para realizar los mapas cartográficos y modelos hidrológicos que permitan evaluar el funcionamiento de los sistemas hídricos en las cuencas de la Provincia”.

**APERTURA DE SOBRES**

El acto licitatorio se desarrollará el viernes 5 de marzo, a las 10 horas, en el Ministerio de Infraestructura, Servicios Públicos y Hábitat - Sede Costanera- sito en calle Avda. Alte. Brown 4751 de la ciudad de Santa Fe.

**TECNOLOGÍA LIDAR**

LiDAR (acrónimo del inglés LIDAR, Light Detection and Ranging o Laser Imaging Detection and Ranging) es un dispositivo que permite determinar la distancia desde un emisor láser a un objeto o superficie utilizando un haz láser pulsado. Allí se interpretan los datos de una nube de puntos generada a partir de los diferentes disparos realizados por el LiDAR para conocer con exactitud la distancia a un plano u objeto. En general, la tecnología LiDAR tiene aplicaciones en geología, sismología y física de la atmósfera.