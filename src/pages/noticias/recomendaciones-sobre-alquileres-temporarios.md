---
category: La Ciudad
date: 2021-01-09T12:00:18Z
thumbnail: https://assets.3dnoticias.com.ar/090121-alquileres.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Recomendaciones sobre alquileres temporarios
title: Recomendaciones sobre alquileres temporarios
entradilla: Ante la cantidad de consultas recibidas en la Dirección de Derechos y
  Vinculación Ciudadana, el municipio brinda algunos consejos sobre los alquileres
  temporarios en tiempos de pandemia.

---
Con el inicio del período estival y el contexto de pandemia, la Dirección de Derechos y Vinculación Ciudadana de la Municipalidad de Santa Fe registró un incremento en las consultas sobre los alquileres de quintas, casas y/o departamentos. Desde el área se brindó información al respecto y consejos para que tengan en cuenta los posibles inquilinos.

Con el comienzo de la temporada de verano, el alquiler de una quinta, casa o departamento es una opción utilizada por muchos ciudadanos. Frente a esta tendencia, hay algunas pautas a tener en cuenta a la hora de suscribir contratos de locación de este tipo.

Franco Ponce De León, director de Derechos y Vinculación Ciudadana municipal, mencionó que «las consultas recibidas son tanto de personas que buscan propiedades, como de dueños de que desean alquilar sus inmuebles en este período. Por eso decidimos brindar información pertinente para, en este contexto de pandemia, saber cuáles son los alcances de estos contratos temporales, de modo de disfrutar el tiempo de descanso sin inconvenientes».

En ese sentido recordó que, dado el presente contexto de Covid-19 y los constantes cambios en las disposiciones sanitarias, es importante tener en cuenta las siguientes pautas y sugerencias:

– El instrumento contractual que se celebra entre las partes debe ajustarse en un todo a las regulaciones y protocolos sanitarios nacionales, provinciales y municipales donde se ubica la propiedad

– En caso de prohibición expresa de las autoridades para viajar hacia el destino y habiéndose tomado antes una reserva, la misma podrá ser tomada a cuenta, a opción del locatario, reprogramando la fecha de su estadía. De no reprogramar, la seña deberá ser reintegrada íntegramente al locatario.

– En caso de que la autoridad competente, por motivos de salud relacionados pura y exclusivamente a casos de Covid-19, llegase a prohibir al locatario el ingreso –en forma personal no general– a la localidad, se sugiere una comunicación entre ambos contratantes a fin de reprogramar las fechas de la estadía. Este punto refiere a los casos en que no existe prohibición general para el libre ingreso, pero la autoridad de control identifica síntomas compatibles con coronavirus en una persona particular, prohibiéndole la entrada.

– Previo al inicio del vínculo contractual, el locador deberá entregar toda la documentación requerida por la autoridad para el libre acceso al municipio, asumiendo cualquier gasto a su cargo.

***

Ante cualquier duda, inconveniente o reclamo, los vecinos y vecinas de la ciudad pueden contactarse con la **Dirección de Derechos y Vinculación Ciudadana** a través de las siguientes vías: 

**WhatsApp:** +54 9 342 531-5450 

**Teléfono:**  0800 444-0442

**Correo electrónico:** [derechosyvinculacionciudadana@santafeciudad.gov.ar](mailto: derechosyvinculacionciudadana@santafeciudad.gov.ar)

**Personalmente:** Salta 2840, de lunes a viernes, de 7.15 a 13 horas.

***

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.15em; font-weight: 700">Qué es un alquiler temporario</span>**

Un contrato de alquiler temporario con fines turísticos es un acuerdo para brindar alojamiento en viviendas amuebladas por un período no mayor a los tres meses, según el Código Civil y Comercial de la Nación. La duración del mismo dependerá de la voluntad de las partes contratantes, siempre y cuando no se exceda del máximo establecido legalmente.

Para estos casos, aún pese a que el período de tiempo es acotado, se recomienda elaborar por escrito y firmar el correspondiente contrato, teniendo en cuenta que se debe incluir la mayor determinación de derechos y obligaciones de las partes y se deben evitar las cuestiones pautadas verbalmente sin consignarlas en el contrato. 

A su vez, se debe incluir el inventario del inmueble en dos ejemplares, a fin de evitar reclamaciones futuras entre las partes por el deterioro, no funcionamiento o faltante de objetos.

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.15em; font-weight: 700">Derechos y obligaciones </span>**

Las principales obligaciones del locatario son: **pagar el precio convenido y mantener la integridad del inmueble**, conservando la propiedad en las mismas condiciones en que se recibió.

Por otro lado, el principal derecho del locador es **cobrar el precio pautado por el uso del inmueble** y su principal obligación es **garantizar el pleno uso del mismo de manera pacífica y sin alteración del destino para el que se contrató**. Debe recibirlo de manos del locatario en el día y la hora previamente establecida.

Es importante tener en cuenta la lectura previa del instrumento contractual, de modo que lo firmado responda a todo lo que se acordó previamente en la negociación.