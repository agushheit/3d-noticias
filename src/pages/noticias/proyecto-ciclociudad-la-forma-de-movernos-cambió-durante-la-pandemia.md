---
layout: Noticia con video
author: Canal de youtube 3DNoticias
resumen: Proyecto Ciclociudad
category: La Ciudad
title: "Proyecto Ciclociudad: la forma de movernos cambió durante la pandemia"
entradilla: Distintas ciudades planean aumentar las bicisendas y ciclovías para
  alentar su utilización y tornar más seguro este medio de transporte que reduce
  la contaminación y permite el distanciamiento social.
date: 2020-11-11T13:40:04.908Z
url: https://youtu.be/HDRb2GJnWgM
thumbnail: https://assets.3dnoticias.com.ar/2020-10-07.jpg
---
La pandemia que azota al mundo desde comienzos de este año no solo vino a modificar los hábitos de trabajo y reestructurar la planificación de las tareas hogareñas, sino que también ha modificado los medios y formas de moverse en las ciudades, sobre todo en las grandes urbes.

En la ciudad de Santa Fe, el proyecto Ciclociudad lleva varios años trabajando en pos de fomentar el uso de este medio de transporte y convertir a la ciudad en un espacio pensado para las personas y no para los autos. La ciudad cuenta con unos 30 kilómetros de circuitos para bicicletas entre senderos, bicisendas y ciclovías, repartidos por diferentes zonas y en algunos casos compartidos con peatones.

“Hicimos un sondeo con comerciantes y talleres de reparación de bicicletas y todos nos han manifestado el crecimiento exponencial en el rubro entre un 50 y un 70 por ciento. En los casos que menos vendieron tuvieron un 40% de crecimiento de ventas entre abril y agosto. Algunos comercios del rubro nos comentaban que tuvieron hasta el 120% de aumentos de ventas con respecto al mismo período del año anterior” comentó Joaquín Azcurain, representante del proyecto Ciclociudad.

Durante la cuarentena, la cantidad de vehículos circulando fue mucho menor y la bicicleta se transformó en un medio económico de transporte que, además, permitía mantener el distanciamiento social. Azcurain sostiene que durante este período de aislamiento social preventivo y obligatorio “se instaló una nueva cultura de movilidad. En algunos aspectos, la pandemia vino a acentuar algunas tendencias que ya venían marcándose en nuestra ciudad y en el mundo respecto al cambio de hábitos en el uso de la bicicleta.”

“Hay un cambio generacional muy grande. Hoy los chicos de 18 para arriba ya no tienen la idea del auto como símbolo de estatus o de ahorro” comenta Joaquín quien, junto al equipo de Ciclociudad, sostiene que costo de mantenimiento de un vehículo particular ronda entre los 10.000 y 12.000 pesos mensuales entre nafta, estacionamiento, patente, etc.

[](<>)“La bicicleta, además de ser más económica y un medio de transporte saludable y sustentable, ha cobrado gran relevancia porque nos da la pauta de que nos movemos más rápido, pese a la falta de infraestructura. Debemos preparar a las ciudades para esta oleada grande de usuarios de la bicicleta que crece y va a seguir creciendo. La educación, la convivencia vial y la infraestructura deben ir de la mano.” concluyó.

Mirá la segunda parte de la entrevista en nuestro [Canal de Youtube](https://youtu.be/rHuB56xM0Mc)