---
category: La Ciudad
date: 2021-10-06T06:00:18-03:00
thumbnail: https://assets.3dnoticias.com.ar/SALUDLABORAL.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Municipalidad puso en marcha una campaña de salud laboral
title: La Municipalidad puso en marcha una campaña de salud laboral
entradilla: " Hasta el jueves, un dispositivo realizará distintos controles frente
  al Palacio Municipal, de 8.30 a 12. Luego se recorrerá las distintas delegaciones
  y dependencias para cubrir a todos los empleados."

---
La Municipalidad de Santa Fe y Asoem, el gremio que nuclea a los trabajadores municipales, pusieron en marcha una campaña de prevención de enfermedades crónicas no transmisibles y promoción de hábitos saludables. Hasta el jueves 7 de octubre, de 8.30 a 12, en la explanada del palacio municipal se realizarán los chequeos de salud al personal municipal.

Durante las semanas siguientes la campaña recorrerá otras dependencias municipales con el fin de que todos los trabajadores se puedan hacer estos controles. Se trata de una acción que impulsan varias secretarías de la Municipalidad como la de Gobierno, la de Políticas de Cuidados y Acción Social, entre otras, y que se debaten en la mesa del Comité Mixto, en la que también participa el gremio Asoem.

En este sentido, el director de Salud municipal, César Pauloni, contó: “Esta campaña se va a transformar en un programa. Es una acción veníamos pensando desde hace un tiempo, pero la pandemia hizo retrasar el inicio. Puntualmente lo que se hacen son controles metabólicos de los trabajadores como de presión arterial, de glucemia, de oximetría; y además algunas mediciones como peso, talla y junto a esto se comparan algunos parámetros que pueden dar un panorama un poco más claro en cuanto a la salud del trabajador”.

Es importante aclarar que los trabajadores que concurran a hacerse estos chequeos deben hacerlo con dos horas de ayuno; y para hacérselo deben completar un formulario. Todo el control se hace por etapas hasta llegar a la consulta con un profesional médico que es quien analiza los datos recabados y le da una recomendación, sugerencia o consejo sobre cómo debe continuar y qué hábitos debe modificar o incorporar, sobre todo en el ámbito laboral.

Esta campaña se hará durante el mes de octubre, pero la intención es continuarla en el tiempo y lograr un vínculo de ida y vuelta con los trabajadores. Por eso esta semana será en el Palacio Municipal y luego recorrerá varias dependencias municipales para evitar que los empleados tengan que trasladarse para hacerse dichos controles.

“Los datos recabados en estas planillas luego serán digitalizados y eso nos va a permitir tener una serie de datos estadísticos sobre la salud de los trabajadores”, dijo Pauloni. En la ocasión contó también que habrá un apartado para el Covid-19 para conocer quiénes tuvieron y quiénes no la enfermedad y cuáles son las secuelas que se perciben.

**Llegar a todos**

Por su parte, José Macaine, integrante del Comité Mixto de Higiene y Seguridad por parte de Asoem, manifestó: “Es muy importante esta acción porque un trabajo preventivo de estas características hasta el momento no se había hecho y la idea es sostener esto en el tiempo y con todos esos datos hacer, no solo un trabajo de concientización sino también de prevención y así poder mejorarle la salud a todos los empleados municipales”.

Por eso aprovechó la ocasión para invitar a todos los municipales a que se acerquen pero les aconsejó a los que no prestan funciones en el Palacio Municipal que no asistan esta semana a calle Salta al 2900 ya que el dispositivo visitará las distintas dependencias y delegaciones para abarcar a esa población. “La idea es llegar al 100 por ciento de los trabajadores y por eso vamos a recorrer todos los lugares”, finalizó el representante del gremio.