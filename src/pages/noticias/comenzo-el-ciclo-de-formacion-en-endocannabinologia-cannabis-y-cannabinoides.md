---
category: Agenda Ciudadana
date: 2021-06-22T08:41:55-03:00
thumbnail: https://assets.3dnoticias.com.ar/canabis.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: " Comenzó el ciclo de formación en Endocannabinología, Cannabis y Cannabinoides"
title: " Comenzó el ciclo de formación en Endocannabinología, Cannabis y Cannabinoides"
entradilla: Se llevó adelante el primero de los cuatro encuentros de la actividad
  de extensión dentro de la diplomatura sobre la materia de la Universidad Nacional
  de Rosario (UNR).

---
Comenzó el ciclo de charlas de formación en Endocannabinología, Cannabis y Cannabinoides brindado por profesionales de todo el mundo expertos en materia de Cannabis. Se trata de una actividad de extensión que cuenta con más de 200 inscriptos y está coordinada por la Doctora Raquel Peyraube.

La misma se dictará los próximos tres jueves a las 10:30 vía Zoom y fue organizada en conjunto por la Subsecretaría de Proyectos Científicos y Tecnológicos del Ministerio de Producción, Ciencia y Tecnología de la Provincia de Santa Fe, la Fundación de Estudios Internacionales de Rosario ( FUNPEI) y  el Centro de Estudios Interdisciplinarios de la Universidad Nacional de Rosario (UNR).

En el primer encuentro, el licenciado en Biología, magíster en Farmacología, doctorado en Ciencias de la Salud y la Vida, e investigador español, el Doctor Xavier Nadal, disertó sobre “Métodos de Extracción”. Del mismo participaron más de 70 personas que felicitaron la propuesta académica y el nivel de excelencia del material expuesto. También participaron el director del Centro de Estudios Interdisciplinarios de la UNR, Dario Maiorana; el presidente FUNPEI, José Romero, y el subsecretario de formación policial del Ministerio de Seguridad de la Provincia de Santa Fe, Andrés Rolandelli. Además, el ingeniero Esteban Robaina y la licenciada Élida Formete, miembros del directorio del Laboratorio Industrial Farmacéutico de la Provincia de Santa Fe (LIF)

La subsecretaría de Proyectos Científicos y Tecnológicos, Eva Rueda, manifestó durante su disertación que “en noviembre, se reglamentó la ley 27.350, que autoriza el uso medicinal, terapéutico y paliativo del Cannabis. En este sentido, entendemos que, desde la Secretaría de Ciencia, Tecnología e Innovación, debemos propiciar instancias de formación, con expertos internacionales en las temáticas seleccionadas, de modo tal que todos los actores que quieran trabajar en el tema tengan acceso al conocimiento científico de calidad”. 

“Es importante tener conocimiento científico y pericia,  para desarrollar con responsabilidad políticas públicas en este campo nuevo de acción que está estrechamente relacionado con la salud de los ciudadanos y ciudadanas”, agregó.

La coordinadora del encuentro, directora Académica del “Diploma de Estudios Avanzados en Endocannabinología, Cannabis y Canabinoides” y partícipe del Comité Ejecutivo de la International Association for Cannabinoid Medicines, Raquel Peyraube, celebró el encuentro y sostuvo que “las actividades de extensión de cualquier formación universitaria y fundamentalmente de las Universidades Públicas son un mandato que debemos tener los docentes y quienes integramos la academia pública. Esto es así porque debemos llevar el conocimiento a la comunidad de formación científica para optimizar, mejorar y motivar hacia la producción, en este caso de este tipo de medicamentos en base a cannabis y cannabinoides".

Asimismo, afirmó que "es fundamental poder contar con actividades de extensión que lleven a distintos grupos según los intereses y objetivos, que la universidad salga de las aulas para ir al encuentro de la gente. Es grato participar de este tipo de acciones porque creo que se satisface una de mis misiones que es la divulgación y democratización de la información". 

**PRÓXIMOS ENCUENTROS**

>>24 de junio 10:30 am - Raquel Peyraube: Cannabis de uso medicinal: Definición y conceptos.

>>01 de julio 10:30 am - Dra. Marta Vázquez

Farmacocinética e interacciones de los Cannabinoides.

>>08 de julio 10:30am - Dr. Guillermo Moreno-Sanz

Productos de Uso Medicinal.

Para inscribirte de manera gratuita podés hacer click acá [https://bit.ly/3gDPRYv](https://bit.ly/3gDPRYv "https://bit.ly/3gDPRYv") y recibirás un email con el link de la plataforma Zoom para participar.