---
category: Agenda Ciudadana
date: 2021-12-17T06:00:36-03:00
thumbnail: https://assets.3dnoticias.com.ar/MonicaPeralta.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: 'Peralta tras la reunión con el gobernador Perotti: “Necesitamos un plan
  integral de seguridad pública”'
title: 'Peralta tras la reunión con el gobernador Perotti: “Necesitamos un plan integral
  de seguridad pública”'
entradilla: 'La diputada rosarina por el partido GEN sostuvo que es necesario “trabajar
  lo urgente y otra instancia de planificación para el largo plazo". '

---
Las declaraciones sucedieron luego del encuentro entre representantes de los bloques de la Cámara Baja y los intendentes de Santa Fe y Rosario con el Jefe de Estado provincial.

El gobernador Omar Perotti se reunió este miércoles con representantes de los bloques de la Cámara de Diputados y los intendentes de las ciudades de Santa Fe, Emilio Jatón, y de Rosario, Pablo Javkin, para analizar medidas de seguridad.

Durante el encuentro se le propuso al Gobernador regionalizar la capacitación de la policía para implementar una política de cercanía y que el Servicio penitenciario vuelva a estar bajo el Ministerio de Seguridad. También le solicitaron fortalecer el Ministerio Publico de la Acusación (MPA) en el Presupuesto 2022 y generar llamados a concurso para aumentar el volumen de la entidad en sus distintas jurisdicciones.

En sentido, la diputada rosarina por el partido Generación para un Encuentro Nacional (GEN), Mónica Peralta, destacó que se requiere un “plan estratégico para la urgencia actual y repensar la planificación para el largo plazo".

“Necesitamos un plan integral de seguridad pública que no son tres leyes para reformular la policía ni leyes aisladas que reformen el Código Procesal Penal o reuniones esporádicas con funcionarios nacionales para que nos envíen fuerzas federales”, señaló la Legisladora.

“Necesitamos acordar núcleos estratégicos en políticas preventivas del delito, fortalecer los operativos de las fuerzas de seguridad, realizar una fuerte inversión en desarrollo social así como en lo educativo y en lo cultural”, afirmó la Diputada para agregar luego que se requiere un “trabajo continuo con el poder judicial”.

"También es fundamental la delegación de facultades y dotar de mayor recursos a los Municipios para el control y prevención del delito", manifestó.

**Inseguridad en Rosario**

Por su parte, el intendente Pablo Javkin solicitó una intervención sostenida con fondos de obras menores que necesita la ciudad sureña. “El gobierno provincial debe tomar dimensión de las realidades de todo el departamento Rosario", subrayó.

En este sentido, la diputada Peralta elevó una carta en enero de 2020 donde solicitó convocar a una mesa de diálogo con todos los sectores políticos y que la Seguridad se constituya como una firme política de Estado para garantizar la calidad de vida de las y los vecinos de la provincia.

"El desafío es repensar las interacciones de las distintas áreas de gobierno y efectivizar un abordaje integral para este grave problema social. Si bien los recursos económicos y la inversión son decisiones urgentes, lo más imperioso es rever lo que estamos haciendo con lo que tenemos. La falta de dinero o presupuesto no es excusa para pensar de forma ineficiente. Es nuestra obligación como mandatarios y mandatarias devolverle la confianza a la ciudadanía demostrando madurez y solvencia para resolver lo que pasa. Por eso necesitamos hacer partícipes a todos los sectores", remarcó Peralta.

"No es momento de contar los muertos de una gestión u otra. Necesitamos un plan integral que trascienda a los gobiernos de turno. No hay seguridad pública sin gestión y no hay gestión sin acuerdos a largo plazo", reflexionó la legisladora.

Sobre el creciente delito complejo que transita en la ciudad, la Diputada sostuvo que es "imprescindible" una mesa de coordinación con organismos nacionales como el Registro de Precursores Químicos (RemPre), el Registro Nacional de Armas (Renar), la Unidad de Información Financiera (IUF) y con las fuerzas federales. “Es vital para tener éxito en las políticas. El lugar que ocupa hoy el delito, tanto organizado como diversificado, no se desarma con un slogan de campaña", concluyó Peralta.

Durante su labor legislativa, la Diputada presentó en la Cámara Baja diversos proyectos como pedidos de informe al Ejecutivo y al Ministerio de Seguridad en relación a distintas problemáticas que se dan dentro de las fuerzas policiales de la provincia, como la violencia de género, discriminación, casos de gatillo fácil o detenciones arbitrarias.