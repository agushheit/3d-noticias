---
category: La Ciudad
date: 2021-07-10T06:00:48-03:00
thumbnail: https://assets.3dnoticias.com.ar/COBEM.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: El Cobem capacita a los santafesinos en técnicas de RCP
title: El Cobem capacita a los santafesinos en técnicas de RCP
entradilla: 'Desde el 9 hasta el 15 de julio, el personal de la Central Operativa
  Brigada de Emergencias Municipal recorrerá la ciudad dictando clases de Reanimación
  Cardiopulmonar (RCP). '

---
La Central Operativa Brigada de Emergencias Municipal, conocida por todas y todos como Cobem, recorrerá la ciudad brindando charlas teóricas y prácticas abiertas para todo el público sobre Reanimación Cardiopulmonar (RCP).

Cabe recordar, que el 14 de julio se cumplirán los 40 años de la creación de este servicio a la comunidad. En ese marco, se decidió brindar capacitaciones bajo la consigna de informar y capacitar sobre las metodologías preventivas, en qué consiste la técnica de RCP y cómo aplicarla correctamente. Por tal motivo, se invita a los vecinos y vecinas a participar de estas actividades que son abiertas y tienen un objetivo primordial: salvar vidas.

Los participantes se pueden inscribir previamente vía correo electrónico: capacitación.riesgo@santafeciudad.gov.area o también pueden concurrir sin turno e inscribirse en el momento en alguno de los lugares donde se realizarán las capacitaciones.

**En toda la ciudad**

El recorrido de las capacitaciones sobre RCP comenzará este viernes 9 de julio en un puesto que se emplazará en Aristóbulo del Valle y Hernandarias. Cabe mencionar que los cursos se realizarán cada dos horas. En ese sentido, el primero se iniciará a las 10.30 y el último a las 17.30.

El sábado 10 y el domingo 11, las capacitaciones se realizarán simultáneamente en tres direcciones: en el playón ubicado en la Dirección de Deportes (Almirante Brown 5294), de 14.30 a 17.30. En El Alero del barrio Coronel Dorrego (French 1701), de 10 a 11.30; y en el Jardín Botánico (Gorriti 3900), de 14.30 a 17.30.

La agenda continúa el lunes 12 en el Parado Español (Hernandarias y General Paz), de 10 a 11.30; y en la Estación Belgrano, de 14.30 a 17.30. En tanto, el martes 13 se recorrerán los distritos Noroeste y Este. Las clases serán en el Polideportivo La Tablada (Teniente Loza 6700), de 10.30 a 11.30 y en el Centro Gallego (Av. Galicia 1357), de 14.30 a 17.30.

El miércoles 14 se realizarán los actos oficiales en el Palacio Municipal. Y el jueves 15 finalizarán las capacitaciones, que tendrán como escenario el distrito de la Costa a la mañana, y por la tarde en Alto Verde. En ese sentido, se hará de 10 a 11.30 en la Ruta 1, Km 2.7 y a desde las 14.30 hasta las 17.30, en la Vecinal de Alto Verde (por calle Demetrio Gómez).

**Salvar vidas**

Los problemas cardíacos y cerebrovasculares son una de las principales causas de muerte en el mundo. La Reanimación Cardiopulmonar es una técnica muy simple que consiste en aplicar compresiones rítmicas sobre el pecho de una persona que está sufriendo un paro cardíaco. Esta maniobra permite garantizar que fluya sangre oxigenada por sus órganos vitales, hasta que llegue la atención médica.