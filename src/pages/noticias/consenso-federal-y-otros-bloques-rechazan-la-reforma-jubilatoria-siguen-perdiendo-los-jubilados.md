---
category: Agenda Ciudadana
date: 2020-12-17T10:41:15Z
thumbnail: https://assets.3dnoticias.com.ar/1712-diputados.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: NCN'
resumen: 'Consenso Federal y otros bloques rechazan la reforma jubilatoria: «siguen
  perdiendo los jubilados»'
title: 'Consenso Federal y otros bloques rechazan la reforma jubilatoria: «siguen
  perdiendo los jubilados»'
entradilla: Los diputados nacionales de Consenso Federal, a los que se suman los santafesinos
  Enrique Estévez y Luis Contigiani, anunciaron que no apoyarán la nueva fórmula de
  movilidad jubilatoria.

---
“No vamos a acompañar la fórmula de movilidad jubilatoria que aprobó el Senado de la Nación porque sigue perjudicando a los jubilados y pensionados”, informaron Graciela Camaño, Jorge Sarghini y Alejandro “Topo” Rodríguez en un comunicado, al que también adhirieron Estévez, del Partido Socialista, y Contigiani, del Frente Progresista Cívico y Social.

Los legisladores advirtieron que los jubilados “no pueden seguir siendo la variable de ajuste de los errores en que incurren los sucesivos gobiernos”.

“Mauricio Macri y Cambiemos los perjudicaron seriamente al cambiarles el sistema, con una fórmula de movilidad jubilatoria solo defendible en una economía que fracasa: se estanca o decrece con alta inflación. También fueron perjudicados por la dolarización de las tarifas y su duro impacto en el costo de vida. Ahora, la propuesta del Frente de Todos y de Alberto Fernández no resuelve el problema del achatamiento y de la pérdida de poder adquisitivo”, argumentaron.

Según los diputados que integran el Interbloque Federal, “una vez más, sigue soslayándose el problema de fondo, que no se soluciona con una fórmula de actualización: el sistema previsional argentino está quebrado y es indispensable aumentar la cantidad de aportantes, lo que se puede lograr incorporando al 50% de los trabajadores privados que hoy están en la informalidad y creando trabajo. Ese es el gran mandato social que debe cumplir el gobierno”.

Además, señalaron que defenderán su propia propuesta, “una fórmula más justa, con garantía de que no se perderán ingresos reales sobre la base de considerar parámetros como el costo de vida, la recaudación, los precios y los salarios”.

<span style="color: #cf0000;">“Pero no alcanza con discutir proyectos aislados. Argentina requiere, principalmente en este tema, una salida sostenida en consensos sociales amplios, profundos y permanentes”</span>, concluyeron.

El proyecto de movilidad jubilatoria fue aprobado en el Senado el 10 de diciembre con 41 votos a favor y 26 en contra, y en la Cámara de Diputados todavía no tiene fecha oficializada de tratamiento, pero se especula con que sea el próximo 29 de diciembre.

La nueva fórmula de movilidad es similar a la aplicada durante los años 2008 a 2017, al combinar en un 50% la recaudación de la ANSeS y en otro 50% la variación salarial, surgida esta última de la más alta entre las medidas por el INDEC y por el Ministerio de Trabajo (índice RIPTE).

***

![](https://assets.3dnoticias.com.ar/1712-diputados1.jpg)

***

## **Juntos por el Cambio ratificó su rechazo a la nueva fórmula de actualización de las jubilaciones**

En una videoconferencia de la que participaron representantes del interbloque de diputados, advirtieron que “la caída de las jubilaciones en 2021 no tendrá piso”.

Diputados del interbloque Juntos por el Cambio que integran las comisiones de Previsión y Seguridad Social y de Presupuesto y Hacienda analizaron este martes la fórmula de movilidad jubilatoria aprobada por el Senado y volvieron a rechazar el proyecto, que se encuentra pendiente de tratamiento en la Cámara baja.

Fue en una videoconferencia de la que participaron, entre otros, los radicales Alfredo Cornejo, Alejandro Cacace, Miguel Bazze, Luis Pastori, Carla Carrizo, Atilio Benedetti, Albor Cantard y Víctor Hugo Romero; Graciela Ocaña, Luciano Laspina y Carmen Polledo por el Pro; y Alicia Terada por la Coalición Cívica.

Según expresaron los diputados en un comunicado posterior a la reunión virtual, “la eliminación de la variable de inflación (IPC) de la fórmula quita la garantía a los jubilados de preservar el poder adquisitivo de sus haberes”.

“Esta variable es la más utilizada en el mundo para preservar el valor de los haberes, especialmente en los momentos donde la economía está en recesión, y en un país como el nuestro con altas tasas de inflación”, consideraron los diputados.

En efecto, la nueva fórmula propuesta por el Poder Ejecutivo e impulsada por el oficialismo se compone en un 50% de la recaudación de la ANSeS y en otro 50% de la variación salarial más alta entre las medidas por el INDEC y por el Ministerio de Trabajo (índice RIPTE), excluyendo el componente inflacionario.

Los representantes de Juntos por el Cambio analizaron que “ya el año 2020 ha resultado en un profundo ajuste a los jubilados, que perdieron entre 1.000 y 18.000 pesos por los aumentos otorgados por decreto y frente a la suspensión de la ley de movilidad, que hubiera otorgado un aumento de 42% para todos los jubilados”.

Más aún, alertaron que “hay jubilados que han llegado a dejar de percibir hasta 140.000 pesos en el año como resultado de las medidas del presidente Alberto Fernández”, y agregaron que “100.000 millones de pesos ha sido el ahorro para el Gobierno, motivo único por el que tomó la decisión”.

“Esta pérdida continuará hacia 2021 con la fórmula propuesta. Con el presupuesto ya aprobado para el año próximo, y bajo la inflación estimada por el propio Gobierno, la Oficina de Presupuesto del Congreso confirmó que habrá una caída en términos reales de las jubilaciones. Si la inflación fuera más alta, del orden del 50% como marca el relevamiento de expectativas de mercado del Banco Central, la caída de las jubilaciones podría llegar a 20 puntos en un solo año”, proyectaron.

Los diputados, finalmente, lamentaron: <span style="color: #cf0000;">“Los jubilados siguen siendo la variable de ajuste de este gobierno, han perdido poder adquisitivo sus haberes en el peor momento, durante la pandemia y con una fuerte recesión, y no vamos a acompañar ni consentir que sigan perdiendo hacia adelante”.</span>

Paralelamente, Graciela Camaño, Jorge Sarghini y Alejandro “Topo” Rodríguez, del bloque Consenso Federal, sumados a Enrique Estévez, del Partido Socialista, y Luis Contigiani, del Frente Progresista Cívico y Social, emitieron otro comunicado donde también rechazaron el proyecto con media sanción del Senado.