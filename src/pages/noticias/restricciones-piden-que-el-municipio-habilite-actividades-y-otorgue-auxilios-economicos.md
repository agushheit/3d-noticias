---
category: La Ciudad
date: 2021-05-12T07:12:37-03:00
thumbnail: https://assets.3dnoticias.com.ar/ines-carlitos.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Inés Larriera
resumen: 'Restricciones: piden que el Municipio habilite actividades y otorgue auxilios
  económicos'
title: 'Restricciones: piden que el Municipio habilite actividades y otorgue auxilios
  económicos'
entradilla: Los concejales Inés Larriera y Carlos Pereira (UCR-Juntos por el Cambio)
  piden que el intendente “asuma el liderazgo y las facultades que el orden jurídico
  le reconoce”.

---
Los concejales Inés Larriera y Carlos Pereira (UCR-Juntos por el Cambio) piden que el intendente “asuma el liderazgo y las facultades que el orden jurídico le reconoce”, y en una nota le solicitan que disponga la apertura de clubes y gimnasios; se otorguen subsidios y se exceptúe de tributos a bares, restaurantes, hoteles y salones de eventos; se permita la apertura de todas las ferias al aire libre, y se extienda el horario de atención de los kioscos. 

Ante las restricciones a diversas actividades económicas decretadas por el Gobierno provincial en el marco de la pandemia de Covid, los concejales de la UCR en Juntos por el Cambio, Carlos Pereira e Inés Larriera, le solicitaron al intendente Emilio Jatón que disponga una serie de medidas “en uso de facultades propias y la plena vigencia de la Autonomía Municipal que la Constitución Argentina reconoce”. En ese sentido, los ediles enfatizaron que ante las nuevas restricciones “el Municipio no solo no puede permanecer ausente, sino que debe asumir un rol decididamente activo en la conducción de la crisis sanitaria y económica”. 

Carlos Pereira e Inés Larriera recordaron que “los Municipios gozan de Autonomía reconocida constitucionalmente”, y tienen “la potestad exclusiva a la hora de habilitar o no actividades. Por lo tanto, el Intendente no puede permanecer indiferente a las medidas que dicta la Provincia, y puede y debe marcar los criterios de aperturas y cierres en la Ciudad. Y reclamaron que Jatón “debe asumir el liderazgo y las facultades que el orden jurídico le reconoce”.

**Aperturas y auxilios**

En una nota que envían este miércoles al intendente Emilio Jatón, los concejales radicales en Juntos por el Cambio sostuvieron que clubes deportivos y gimnasios deben abrir con los protocolos y restricciones adecuados a cada momento de la pandemia. “Como todos sabemos el deporte es salud, y como ya lo hemos comprobado durante el 2020, restringir las actividades físicas causa afecciones en la salud tan preocupantes como el Covid”, advirtieron, y resaltaron que las actividades deportivas al aire libre, en cuanto no impliquen grandes aglomeraciones, tienen un muy bajo potencial de contagios. Mientras tanto, “las Actividades Deportivas en lugares cerrados pueden desarrollarse con reducción de los aforos, cumplimiento de protocolos y control del Estado”.

También remarcaron que “la gastronomía, la hotelería y los salones de eventos “deben ser auxiliados económicamente por el Estado. Las actividades económicas que sufren grandes restricciones deben recibir subsidios y exenciones tributarias”. Recordaron que “el Concejo Municipal aprobó la iniciativa que presentamos desde nuestro Bloque para que Provincia otorgue subsidios y el Municipio exceptúe de tributos a bares, restaurantes, hoteles y salones de eventos”.

Por otra parte, entendieron que todas las ferias al aire libre deben abrir, con estrictos protocolos controlados por el Municipio. “No se puede condenar al cierre a actividades económicas de subsistencia sin ningún tipo de auxilio desde el Estado. Otros Municipios de la Provincia, como Rosario y Reconquista, en ejercicio de la Autonomía han habilitado el funcionamiento de las mismas. Y en la ciudad, el intendente ha puesto en marcha un criterio discriminatorio permitiendo, por ejemplo la apertura de La Baulera en el Mitre o el CIC de Facundo Zuviría y no permitiendo la Feria de la Costanera o el Paseo de los Artesanos en la Pueyrredón”, apuntaron.

Finalmente, Carlos Pereira e Inés Larriera pidieron que los kioscos sean autorizados a extender el horario de atención nocturna. “Se trata de comercios de proximidad, ubicados la mayoría en el interior de los barrios, a los cuales los compradores llegan caminando y sin generar problemas para la circulación. Al igual que los bares pueden recibir público hasta las 22 o 23, brindando un servicio necesario a las familias ante el cierre del resto de la actividad comercial”, argumentaron.