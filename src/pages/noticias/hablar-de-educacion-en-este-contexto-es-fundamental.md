---
category: La Ciudad
date: 2021-04-21T07:14:47-03:00
thumbnail: https://assets.3dnoticias.com.ar/concejo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Concejo Municipal Santa Fe
resumen: "“Hablar de educación en este contexto es fundamental”"
title: "“Hablar de educación en este contexto es fundamental”"
entradilla: |2-

  “Es una situación muy difícil la que atravesamos y creemos que estas instancias son elementales para hablarle a las familias, a los docentes y a los estudiantes”.

---
Este lunes se llevó adelante el primer encuentro del ciclo de Educación Socio-emocional, a cargo de la Lic. Cecilia Marino, denominado “Otro Mundo. 365 días de aprendizaje socioemocional”.

La conferencia fue de manera online y el presidente del Concejo, Leandro González, remarcó “el entusiasmo porque proyectamos el Plan Abierto, Participativo y Transparente para el 2021 y se está concretando con el comienzo del primer ciclo de Vínculos y Ciudadanía. Tomamos a la educación del punto de vista socio-emocional para llegar a estudiantes, docentes, familias. Se anotaron más de 500 personas en esta primera charla que a lo largo de las semanas y el desarrollo de las próximos encuentros profundizará sobre este tema. Estamos en un punto alto de estrés por parte de la comunidad educativa y también de la familia, creemos que este es un espacio importante para dialogar y sumar herramientas”.

Por otra parte, la disertante Cecilia Marino destacó la actividad y el estar “muy feliz de este primer encuentro, para mi es un gusto poder colaborar con un granito de arena, en el bienestar emocional de los educadores que es tan importante. Lo dije en la capacitación, los alumnos no aprenden solamente lo que el docente les enseña, sino que aprenden de lo que el docente es. Es muy importante que los educadores trabajemos para nuestro bienestar emocional, porque de eso va a depender la calidad del vínculo que establezcamos con los alumnos, la calidad de los aprendizajes y el disfrute de la tarea. Aunque parezca raro hablar de disfrute en los tiempos que vivimos pero creo que nunca tenemos que renunciar a disfrutar del trabajo que hacemos y poder hacerlo con alegría y centrados, no estresados o fuera de nuestro eje. Es fundamental el modelo que seamos para nuestros alumnos en cuanto a la gestión de nuestras emociones”.

El ciclo continuará el lunes 26 con “Familia-escuela y emociones. Superando los opuestos”, concluyendo el lunes 3 de mayo con “Atrapados en emociones que nos complican. Ansiedad y Miedo”.

Las conferencias serán de manera online y quiénes quieran participar tendrán que inscribirse al link: [https://forms.gle/x3CWFKNgbRwSSpkk6](https://forms.gle/x3CWFKNgbRwSSpkk6 "https://forms.gle/x3CWFKNgbRwSSpkk6")

**Disertante**

María Cecilia Marino es Lic. en psicopedagogía por la Universidad del Salvador y tiene un Posgrado en Gestión Curricular por la Facultad Latinoamericana de Ciencias Sociales. Es asimismo Profesora para la Enseñanza Primaria. Directora de la Editorial Proyecto Cepa, desde donde promueve la edición de libros y materiales didácticos y actividades de formación para profesionales de la educación desde hace más de diez años.

Es capacitadora en temas relacionados con educación emocional e innovación en educación y dicta cursos y conferencias en forma permanente en Argentina y distintos países de habla hispana.