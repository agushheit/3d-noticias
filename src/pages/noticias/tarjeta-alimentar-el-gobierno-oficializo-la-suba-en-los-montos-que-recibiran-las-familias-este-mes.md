---
category: Agenda Ciudadana
date: 2021-02-04T03:48:30-03:00
thumbnail: https://assets.3dnoticias.com.ar/alimentar.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Tarjeta Alimentar: el Gobierno oficializó la suba en los montos'
title: 'Tarjeta Alimentar: el Gobierno oficializó la suba en los montos que recibirán
  las familias este mes'
entradilla: El refuerzo será de 2.000 y 3.000 pesos y se abonará el próximo 19 de
  febrero.

---
El **Ministerio de Desarrollo Social** oficializó este jueves la suba en los montos mensuales a acreditar en la **Tarjeta Alimentar.** El incremento en los saldos es del 50%, por lo que las familias **percibirán 6 mil o 9 mil pesos, según corresponda.**

La decisión fue estudiada por el Gobierno Nacional durante las últimas semanas y ayer confirmada por el ministro **Daniel Arroyo.** Frente al aumento de la inflación registrado en los últimos meses, ante el incremento de los alimentos y la compleja situación económica profundizada por la pandemia de coronavirus, el titular de la cartera de Desarrollo Social anunció que “en este mes de febrero se va a dar un 50 por ciento de aumento en los montos de la Tarjeta Alimentar”, teniendo además como propósito garantizar “una buena nutrición” de los sectores populares más vulnerables.

En efecto, la medida fue oficializada a través de la **Resolución 63/2021** que fue publicada hoy en el **Boletín Oficial**. “En atención a la situación por la que atraviesa nuestro país, alcanzando extremos niveles de pobreza, agravada por la situación de emergencia sanitaria durante la pandemia por COVID-19, resulta imperioso instrumentar las medidas necesarias y adecuadas para paliar tal situación y fortalecer los instrumentos que se vienen implementando a fin de coadyuvar la búsqueda de condiciones más equitativas”, fundamentó el ministerio conducido por Arroyo.

Por ello, “se estima necesario incrementar en un 50% los montos establecidos para cada categoría” de la Tarjeta Alimentar. De esta manera, **las madres con un solo niño menor de 6 años que percibían $4.000 por mes pasaran a recibir $6.000, mientras que las que tienen dos o más hijos menores de 6 años pasarán a cobrar $9.000 pesos en lugar de los $6.000 que recibían hasta ahora durante el mismo período.**

***

![](https://assets.3dnoticias.com.ar/tarjeta-alimentar.webp)

***

“La intención es sostener el poder de compra de alimentos por parte de las familias y avanzar en un esquema de una buena nutrición. Un chico que come bien tiene oportunidades mientras que otro que se alimenta mal, tendrá complicaciones en la vida. **Es evidente que hay un problema serio en los costos.** Por eso lo mejor es comprar en mercados populares y ferias directamente al pequeño productor y, así, evitar la intermediación”, analizó Daniel Arroyo.

En este contexto, el funcionario señaló que “en el mes de julio del año pasado, el consumo de leche, carne, frutas y verduras bajó a cerca del 40 por ciento, pero que con los refuerzos percibidos en diciembre se logró subir con la Tarjeta Alimentar el consumo de alimentos frescos en 55 por ciento”.

El refuerzo de la tarjeta Alimentar se dispuso como una de las políticas de asistencia para las familias que sostendrá el Poder Ejecutivo tras confirmar que no continuará con el Ingreso Familiar de Emergencia (IFE) y el programa de Asistencia al Trabajo y la Producción (ATP).

“La tarjeta está dirigida a madres o padres con hijos e hijas de hasta 6 años de edad que reciben la AUH. También a embarazadas a partir de los 3 meses que cobran la asignación por embarazo y personas con discapacidad que reciben la AUH, por lo que **llega a más de un millón y medio de familias”**, indicó el Ministerio de Desarrollo Social a través de un comunicado.

Según el cronograma de pagos que difundió la ANSES para febrero, al igual que en meses anteriores, las personas que reciben el pago de la Tarjeta Alimentar y la tengan en formato físico cobrarán el tercer viernes de cada mes, por lo que les corresponderá el próximo **viernes 19**, mientras que al resto se le acreditará el saldo en la cuenta durante la última semana del mes, aunque las fechas aún no fueron precisadas.