---
category: La Ciudad
date: 2022-01-15T06:15:51-03:00
thumbnail: https://assets.3dnoticias.com.ar/lluvias.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Cómo se prepara la ciudad ante la llegada de tormentas y fuertes lluvias,
  desde el domingo
title: Cómo se prepara la ciudad ante la llegada de tormentas y fuertes lluvias, desde
  el domingo
entradilla: Desde el municipio afirmaron que está dispuesto el operativo diagramado
  por el área de Recursos Hídricos

---
Todos los pronósticos del tiempo coinciden en que este domingo llegaría el alivio para la ciudad de Santa Fe luego de una semana con récords históricos de temperatura. A su vez, se esperan fuertes tormentas que, si bien pondrán fin a las altas temperaturas, también podrían traer complicaciones.

El Servicio Meteorológico Nacional prevé una caída de 160 mm de lluvia acumulada para la próxima semana. Ante este escenario, se consultó al municipio sobre cómo se prepara la ciudad ante la llegada de las precipitaciones después de largos períodos de sequía.

El área de Asuntos Hídricos local tiene diagramado un operativo para este tipo de eventos que varía según la intensidad de las lluvias o las tormentas que afectan las más de 63.000 cuadras que componen toda la cuadrícula de Santa Fe, que comprende la limpieza profunda de zanjones y canales para evitar obstrucciones, así como el monitero especial de aquellos barrios en dónde generalmente se producen anegaciones con poco milimetraje.

También desde el comienzo de la nueva gestión se incorporó un sistema especial de monitoreo de las estaciones de bombeos para controlar su correcto funcionamiento.

Por otro lado además del Área Hídrica, Acción Social, Protección Civil, Salud y Comunicación tiene un papel fundamental para la coordinación de acciones una vez comenzado el operativo para poder dar respuesta a la demanda de los santafesinos.

Si bien no hay un número preciso de integrantes de cuadrillas, si está determinado que todos lo que debían de cumplir una tarea ese día y debieron ser suspendidas por el clima, inmediatamente están a disposición para trabajar en el nuevo operativo.

**Qué recomendaciones se le hacen al vecino**

Ante el alerta, se recomienda a los vecinos no sacar a la vía pública objetos o residuos que puedan obstaculizar el normal escurrimiento del agua de lluvia. Además, se recuerda que, después de lluvia, realicen acciones de descacharrado de patios, vaciando o descartando recipientes donde se pudo acumular agua.

Se recuerda que las consultas o reclamos se pueden realizar al Sistema de Atención Ciudadana, llamando al 0800-777-5000.