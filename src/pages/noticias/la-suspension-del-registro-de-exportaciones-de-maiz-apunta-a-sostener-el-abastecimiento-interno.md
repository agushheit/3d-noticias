---
category: El Campo
date: 2021-01-06T10:23:19Z
thumbnail: https://assets.3dnoticias.com.ar/060121-maiz.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: El Litoral'
resumen: La suspensión del registro de exportaciones de maíz apunta a sostener el
  abastecimiento interno
title: La suspensión del registro de exportaciones de maíz apunta a sostener el abastecimiento
  interno
entradilla: Es el principal argumento esgrimido por las autoridades nacionales y dirigentes
  del sector privado que avalan la medida tomada la semana pasada por el Ministerio
  de Agricultura.

---
El objetivo es priorizar la provisión del mercado interno ante las complicaciones que desde determinados sectores productivos -en especial los relacionados con la producción de carne aviar, porcina y vacuna- para obtener materia prima para alimentar a sus animales a pesar de la cosecha récord alcanzada la campaña pasada.

**Argentina es un gran exportador mundial del cereal, pero también posee un alto consumo interno, que en su gran mayoría es dedicado a la alimentación animal.**

Según un informe la Bolsa de Comercio de Rosario (BCR), la producción de maíz fue de 51,5 millones de toneladas en la campaña 2019/20. De ese total, más de 38 millones de toneladas fueron consideradas como saldo exportable, mientras que el remanente tuvo como principal destino a los corrales de engorde o _feedlots_, tambos de producción lechera, granjas de cerdos y criaderos avícolas.

Sin embargo, **desde hace varios meses hay sectores que salieron a advertir que, a pesar del volumen récord de producción, enfrentaban complicaciones para poder adquirir el maíz destinado a alimentar sus animales.**

La semana pasada, horas después de que el Gobierno anunciara la suspensión temporal del registro de exportaciones, el presidente de la Cámara de Empresas Procesadoras Avícolas (CEPA), Roberto Domenech, sostuvo que «no es mucho el maíz que queda y son muchos los sectores que venimos con muchas dificultades para conseguirlo, ya desde hace 90 días».