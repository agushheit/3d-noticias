---
category: La Ciudad
date: 2021-05-07T08:42:16-03:00
thumbnail: https://assets.3dnoticias.com.ar/katopodia1.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad de Santa Fe
resumen: Jatón se reunió con Katopodis
title: Jatón se reunió con Katopodis
entradilla: El intendente viajó a Buenos Aires para avanzar en la ampliación de la
  planta potabilizadora con el ministro de Obras de la Nación. También evaluó con
  Enrique Cresto acciones en conjunto para extender la red cloacal.

---
El intendente Emilio Jatón se reunió el miércoles con el ministro de Obras Públicas de la Nación, Gabriel Katopodis, para avanzar sobre la ampliación de la planta potabilizadora de agua. Se trata de una obra que permitirá proveer de ese servicio básico a una gran cantidad de barrios del noroeste de la capital y mejorarlo en toda la ciudad. Luego, también en Buenos Aires, mantuvo un encuentro con Enrique Cresto, titular del Ente Nacional de Obras Hídricas de Saneamiento (Enohsa).

“Es una obra que venimos pidiendo hace años, así que estamos muy contentos del compromiso que asumió el presidente Alberto Fernández y que hoy reafirma el ministro Katopodis. Es un primer paso para poder llegar con el agua a barrios que siempre estuvieron postergados y para mejorar la calidad del servicios para todos los usuarios”, señaló Jatón al finalizar el encuentro.

Y agregó: “Este tipo de acciones requieren de un trabajo articulado entre todos los niveles del Estado, una tarea que priorice a las y los vecinos. Por eso vamos a trabajar con el gobierno provincial y el nacional para elaborar el proyecto ejecutivo”.

Durante la reunión, el intendente y el ministro nacional también abordaron otras obras que se están realizando en la ciudad con financiamiento del programa Argentina Hace y sobre más iniciativas que están proyectándose en Santa Fe.

![](https://assets.3dnoticias.com.ar/katopodis.jpg)**Más servicios esenciales**

Más tarde, Jatón se reunió con Enrique Cresto, titular del Ente Nacional de Obras Hídricas de Saneamiento, para analizar la situación de muchos barrios respecto al acceso al sistema de cloacas. “Gran parte de la capital provincial todavía no tiene ese servicio, necesitamos revertir eso. Por eso hoy trabajamos sobre la posibilidad de unir esfuerzos para acortar esa brecha que tiene que ver con la salud y las condiciones de vida”, indicó el intendente.

En el encuentro abordaron los proyectos que se pondrán en marcha en el corto plazo a partir de la gestión que se lleva adelante con Aguas Santafesinas pero también la necesidad de sumar a la capital provincial otras líneas de acción que permitan continuar con la ampliación de la red cloacal.

“Hoy, en Santa Fe, cerca del 40 por ciento de los hogares no tiene cloacas. Poder concretar ese tipo de obras nos permite integrar los barrios, tener una ciudad con menos desequilibrios y mejorar la vida de muchas familias”, cerró el intendente Jatón.

![](https://assets.3dnoticias.com.ar/cresto.jpg)