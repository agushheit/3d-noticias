---
category: La Ciudad
date: 2021-11-24T06:15:01-03:00
thumbnail: https://assets.3dnoticias.com.ar/palometasattack.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Municipalidad recuerda que las playas no están habilitadas como balnearios
title: La Municipalidad recuerda que las playas no están habilitadas como balnearios
entradilla: "Debido a la bajante de la laguna Setúbal y ante la aparición de palometas,
  está prohibido ingresar al agua. Estos espacios sólo pueden utilizarse como solariums.
  .\n\n"

---
La Municipalidad recuerda que a raíz de la bajante excepcional del río, los balnearios de la Laguna Setúbal sólo pueden utilizarse como solariums; mientras que los únicos lugares habilitados para el baño recreativo son los piletones de los parques del Sur y Garay.

Sobre la imposibilidad de utilizar los balnearios para el baño recreativo, la directora de Gestión de Riesgos de la Municipalidad, Cintia Gauna, sugiere “no ingresar al agua por la peligrosidad que representa la propia Laguna, que tiene profundidades muy marcadas y el suelo inestable, sobre todo, en la zona más próxima a la costa”.

Además, la funcionaria mencionó otras situaciones que se suman, como la mayor concentración de palometas y otras especies, por lo que se pretende evitar cualquier problema en la salud de los santafesinos y de los turistas que llegan a Santa Fe.

Es importante señalar que el bajo nivel de las aguas, sumado a las altas temperaturas que se suelen registrar en esta época, provocan la presencia de palometas y rayas, como así también la posible aparición de ofidios.

Por todo esto, se solicita a la población que no ingrese al agua, sobre todo teniendo en cuenta que este período de bajante va a continuar en los próximos meses.