---
category: Agenda Ciudadana
date: 2021-05-26T08:08:08-03:00
thumbnail: https://assets.3dnoticias.com.ar/medicos-vida.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Diario La Capital
resumen: Tensión y más de 30 detenidos en la marcha de médicos anticuarentena
title: Tensión y más de 30 detenidos en la marcha de médicos anticuarentena
entradilla: La manifestación se realizó en las inmediaciones del Monumento a la Bandera
  en la ciudad de Rosario. Los participantes no usaban barbijos ni respetaron las
  restricciones vigentes

---
A las 13 de ayer, unas ochenta personas se reunieron a los pies del Monumento a la Bandera y comenzaron a meditar y a invocar deidades para que “la energía se disperse y se abra un portal que ilumine el mundo”, según contó una mujer que se encontraba entre los asistentes. En paralelo, la agrupación de “Médicos por la Verdad” se había convocado en el mismo lugar, a las 15, para realizar un acto de “esclarecimiento” sobre la situación pandémica y las restricciones sociales, desde el uso de barbijo hasta la nueva modalidad de circulación. Así, fueron dos los grupos que dieron cita, uno llamado “Aliados de la Gloria” y otro conformado por adeptos de “Médicos”.

“Aliados” es una agrupación creada por el médico Mariano Arriaga, quien también es miembro fundador de”Médicos”. En la manifestación, de la que participaron alrededor de 400 personas, se contaban muchas oriundas de las provincias de Córdoba, Santiago del Estero y Buenos Aires, a las que se sumaron manifestantes de Rosario. Promediando la tarde y luego de la intervención de la policía provincial y agentes de la Guardia Urbana Municipal (GUM), los detenidos eran más de 30 manifestantes.

A las 13.30, y enterados de la situación por la prensa, llegaron al lugar unos 100 efectivos policiales, encabezados por la jefa provincial Emilce Chimenti, quien intentó disuadirlos de distintas maneras para lograr que los manifestantes se dispersaran o bien se pusieran el barbijo. Lo mismo intentaron hacer agentes de la GUM.

Integrantes del grupo que meditaba explicaron que la idea inicial era lograr “una canalización de energía que se hace todos los meses. No tiene nada que ver con la pandemia. Igual, no creemos en el uso del barbijo, pero sí en la energía. Es necesario anclar la energía”, contó Carolina.

Ludmila, quien llegó al lugar para meditar, explicó a La Capital que fue al Monumento convocada por redes sociales. “Yo tuve un problema de salud y con esto salí adelante. No podrían entenderlo quienes no se contactan, quien no se reconcilie consigo mismo. No hay una organización que comande esto, es una cuestión de libertad y de creer en otras cosas. Por ejemplo, yo me contacto con alguien que me cuenta de esto y de ahí nos vamos comunicando y, así, por medio de las redes llegué acá. Somos gente libre”. Ludmila ronda los 30 años y estaba descalza mientras iba de un lado a otro por el parque.

Durante toda la manifestación, realizada como un claro hecho político de visualización, integrantes de “Médicos” fueron organizando distintos focos de discusión y al grito de “libertad, libertad” cuestionaban a los policías y a los agentes de la GUM sobre la forma en que intentaban disuadirlos.

Otra mujer, que fue increpada por efectivos de la GUM, sostuvo: “Todos deben leer las indicaciones de la Organización Mundial de la Salud, el barbijo trae cáncer y es lo que produce el contagio, allí se depositan las bacterias. Es necesario que todos se despierten”. Como apóstoles de una verdad revelada o a revelar buscaban que “todos sepan, que no se dejen engañar. ¿Acaso Bill Gates no inventó los virus de las computadoras y luego los antivirus? Bien, así se maneja esto ahora, igual que con las computadoras, por eso Gates está detrás. No se hacen las autopsias de los cadáveres para que no nos enteremos”.

A las 14.30, los efectivos policiales, entre ellos efectivos del Grupo de Infantería de Respuesta Inmediata (Giri) pertrechados con cascos, escudos antimotines y pistolas lanza gases decidieron dispersar a los manifestantes y arremetieron contra varios grupos sin llegar a usar armas y sin golpear a nadie, pero manejando firmeza en las acciones. Esto se repitió a lo largo de toda la tarde, hubo forcejeos y así fueron arrestadas más de 30 personas, entre ellas Arriaga.

La tensión fue en aumento, los grupos iban de un lado al otro mientras Sebastián, jefe a cargo del grupo de la GUM, megáfono en mano invitaba a los manifestantes a “desplazarse y colocarse barbijos”. En tanto, en los diferentes focos los policías forcejeaban con manifestantes que los increpaban, y así algunos terminaron arrestados.

Toda la zona del Monumento fue un ir y venir de grupos, manifestantes que entonaban el Himno Nacional; gritos de “libertad”; insultos a los policías y frases tiradas al aire: “¿Cómo no entienden?; “Son ciegos”; “Queremos ser libres y salir del engaño del Estado”; “¡No sean esclavos del tirano!”.

Entre los gritos y la dispersión de los grupos fue detenido el médico terapista mendocino José Luis Gettor. Minutos después, un hombre alto y calvo le gritó a Virginia Benedetto, fotógrafa de este diario (ver página 7) y tras el apriete arengó a los manifestantes al grito de “seamos libres”.

La gran duda a ese momento era quién concentraba las decisiones, ya que los grupos se movían hacia distintos lugares pero sin retirarse. Los manifestantes explicaban que se movían por su propia voluntad “libres y sin nadie que organice nada. Somos libres y queremos que no nos envenenen”. Los grupos se mantenían a la defensiva ante la prensa y continuaban con su discurso de autoconvocados por las redes.

Sin embargo, la que organizaba era una mujer. Lucía, cordobesa y de unos cuarenta años, dirigía a distancia a ciertos manifestantes, que con la voz cantante se iban acercando a los grupos y los incentivaban a continuar en la plaza. Lucía, llevó al lugar la versión de que “hay una orden de arresto para los médicos, ustedes sigan, yo me voy”, les dijo a quienes la rodeaban para desaparecer del lugar.

### “Libres”

Minutos después, una multitud de unas 400 personas se reunió en la explanada de Monumento y un hombre, bajo una pancarta de “Libres”, dijo a los gritos que “no quieren que el mundo se entere de lo que pasa en Argentina”, frase a la que una mujer sumó que “hay una orden” de arresto para los médicos. “Los Aliados y los Médicos queremos un debate abierto, público y científico, queremos romper esta dictadura sanitaria. Han sido detenidos los médicos que estaban con nosotros y tenemos que protegerlos. No todos los que están acá son amigos nuestros, cuidémonos”.

Poco después, una de las organizadoras, quien formaba parte del grupo de Lucía y por su tono de voz tampoco era rosarina, planteó con su celular en mano: “Nos va a hablar la doctora Beatriz”, y por un celular se escuchó la voz de una mujer: “Fuerza , no van a poder ocultar la verdad”, dijo desde un lugar lejano a la manifestación.

Durante la tarde, los manifestantes siguieron con sus planteos, y se dispersaron apenas pasadas las 17.30. Mientras, los detenidos fueron trasladados a Jefatura, a disposición de la Justicia.