---
category: La Ciudad
date: 2021-02-20T06:00:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/CRUZROJA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'González: "La Cruz Roja tiene mucho para seguir aportando en la ciudad"'
title: 'González: "La Cruz Roja tiene mucho para seguir aportando en la ciudad"'
entradilla: El presidente del Concejo Municipal, Leandro González, fue recibido en
  la sede local de la Cruz Roja Filial Santa Fe por el coordinador general de la institución,
  Osvaldo Ferrero, e integrantes de la comisión directiva.

---
El presidente del Concejo Municipal, Leandro González, fue recibido en la sede local de la Cruz Roja Filial Santa Fe por el coordinador general de la institución, Osvaldo Ferrero, e integrantes de la comisión directiva.

Al respecto, el concejal declaró que “es la primera de muchas reuniones que vamos a realizar para iniciar un camino de trabajo conjunto para el 2021. Creemos que hay una necesidad permanente de capacitar y formar a los vecinos en cuestiones que son básicas y elementales, como RCP o primeros auxilios”.

A su vez, González destacó a la Cruz Roja por ser “una institución con reconocimiento local e internacional que históricamente ha estado presente en los momentos difíciles de la ciudad, como en las inundaciones de 2003 y 2007. El más reciente, cuando formaron parte de la articulación del voluntariado en el marco de la Pandemia de Covid-19, en conjunto con Emilio Jatón y la gestión municipal”.

El coordinador general de la Filial Santa Fe, Osvaldo Ferrero afirmó que “la Cruz Roja cumple un rol de auxiliar a los servicios públicos, está para apoyar todas las acciones que el Estado quiera desarrollar en la comunidad”. Y agregó que “aprovechando que de a poco podemos retomar la presencialidad, vamos a delinear un plan de acción para volver a realizar capacitaciones en primeros auxilios en escuelas, clubes, vecinales y demás organizaciones de la sociedad civil”.

González y Ferrero coincidieron en la necesidad y la voluntad de trabajar en conjunto: “ni el gobierno ni las instituciones pueden solas, la idea tanto de Cruz Roja como del Concejo Municipal es trabajar estas cuestiones en equipo”.