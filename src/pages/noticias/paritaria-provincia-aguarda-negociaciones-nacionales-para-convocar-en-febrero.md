---
category: Agenda Ciudadana
date: 2022-01-26T06:15:50-03:00
thumbnail: https://assets.3dnoticias.com.ar/PUSINIERI.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Paritaria: provincia aguarda negociaciones nacionales para convocar en febrero'
title: 'Paritaria: provincia aguarda negociaciones nacionales para convocar en febrero'
entradilla: 'La provincia mira con atención la paritaria nacional docente que se reunirá
  la próxima semana. Anticipó que se podría repetir el esquema del año pasado

  '

---
El ministro de Trabajo, Juan Manuel Pusineri, confirmó que la convocatoria a la paritaria central será en febrero, algo que se había comprometido "con cada uno de los gremios de la administración el año pasado".

El funcionario precisó que "se está esperando de alguna forma la evolución de algunos datos nacionales", y destacó: "Al no haber presupuesto nacional, ni provincial, hay una pauta inflacionaria que se establece en esos instrumentos que hoy no tenemos"

Como generalmente ocurre, Pusineri indicó: "La paritaria nacional docente, que empieza la semana que viene, es un dato que vamos a seguir con atención para llevar adelante el proceso en la provincia de Santa Fe. Vamos a esperar un poco la evolución de este elemento (por la paritaria nacional) porque hoy prácticamente no contamos con pautas oficiales, como fue el año pasado como para empezar la discusión. Después, lógicamente, el intercambio que vayamos teniendo con cada uno de los gremios".

Consultado sobre si el gobierno buscará un acuerdo anual, con la posibilidad de que el mismo sea revisado, apuntó: "Lógicamente depende de lo que se logre acordar. No es una cuestión que se resuelva unilateralmente. A mi modo de ver, el año pasado este esquema funcionó. Un aumento anual con una revisión pasada la mitad del año, septiembre u octubre".

En declaraciones a la emisora LT10, comentó que dicha estrategia "funcionó porque el salario no perdió con la inflación el año pasado". "Me parece que es un esquema que los gremios aceptaron, previo plebiscito con sus afiliados. Me parece que es un esquema va a funcionar en la provincia y que en la Argentina en general va a terminar primando para la discusión paritaria", resaltó el funcionario.

Señaló que no existen diferencias en las negociaciones entre los distintos gremios de la administración, y comentó: "Si hablamos de aumentos porcentuales, pensamos en todos los sectores de la administración; docentes, administración central, salud, Vialidad, inclusive las empresas del Estado también toman esa pauta en el marco de negociación. Tampoco vimos que eso haya tenido alguna resistencia con algunos gremios".