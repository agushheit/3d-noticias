---
category: Agenda Ciudadana
date: 2021-06-06T06:15:39-03:00
thumbnail: https://assets.3dnoticias.com.ar/REACTIVO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Este domingo llega el principio activo para comenzar la producción argentina
  de la Sputnik V
title: Este domingo llega el principio activo para comenzar la producción argentina
  de la Sputnik V
entradilla: 'Los dos mandatarios anunciaron la producción en Argentina de la vacuna
  rusa a través de una videoconferencia realizada este viernes. '

---
El presidente Alberto Fernández y su par de la Federación Rusa, Vladimir Putin, anunciaron oficialmente el inicio de la producción de la vacuna Sputnik V en la Argentina por parte del laboratorio Richmond, que tiene como objetivo desarrollar unas 500.000 dosis por semana.

Los mandatarios realizaron el anuncio este viernes a través de una videoconferencia, en la que el presidente Alberto Fernández le expresó a su colega ruso su "agradecimiento" por el "compromiso que ha tomado con mi Patria", y que posibilitó que un "número muy importante de los argentinos hayan logrado la inmunidad gracias a la vacuna Sputnik V".

En ese contexto, Alberto Fernández destacó que Rusia ayudó a la Argentina a "conseguir las vacunas" contra el coronavirus cuando "el mundo no" las "estaba dando" y anunció que el domingo llegará al país "el principio activo" para comenzar a elaborar la Sputnik V en el país "de inmediato" y que también llegarán más dosis para continuar con la campaña de vacunación.

Asimismo, manifestó que "Argentina y Rusia tienen una historia común de 136 años" y afirmó que "la pandemia dio una gran oportunidad para que ese encuentro se profundice y afiance mejor que nunca",

"Celebro y agradezco sinceramente", expresó el presidente y reiteró que Argentina está "inmensamente agradecida".

"En Argentina decimos que los amigos se conocen en los momentos difíciles y cuando pasamos un momento difícil, el Gobierno de Rusia estuvo al lado de los argentinos ayudándonos a conseguir las vacunas que el mundo nos negaba", dijo el mandatario argentino.

"Podemos hacer esto posible a través del diálogo y la cooperación a nivel global", dijo el presidente de la Federación Rusa en la videoconferencia en la que también participaron el director ejectivo del Fondo Ruso de Inversión Directa, Kirill Dmitriev, y la responsable del desarrollo en Bielorrusia.

En la videoconferencia, el jefe de Estado dijo también que se traerán vacunas para Paraguay, mientras se termina de "acercar a México y Rusia" para que la vacuna llegue también al pueblo mexicano.

Además, reseñó que Argentina fue "el primer país en América Latina en aprobar la vacuna Sputnik V y el segundo en el mundo" y afirmó que está muy conforme con "los logros alcanzados" y que "siempre" confió en "la capacidad de Rusia para "el desarrollo de una vacuna tan importante".

"Millones de argentinos preservaron la vida gracias al desarrollo científico de Rusia, en el que siempre confiamos", expresó el jefe de Estado argentino.

**Convenios**

Rusia firmó convenios con 66 países de los cinco continentes para proveerlos de vacunas Sputnik V, pero también con productores de 20 países para su desarrollo local a través de asociaciones.

Durante la videoconferencia, en la que estuvieron representantes de otros países que desarrollan la vacuna, como Serbia, se vio un video en el que se mostraron los beneficios de la Sputnik V, con un alto nivel de eficacia en la inmunización contra el coronavirus.

Allí se informó que 800 millones de personas pueden ser vacunadas anualmente en todo el mundo gracias al volumen de producción alcanzado a través de estos acuerdos con laboratorios.

Del anuncio participó también el director ejecutivo del Fondo Ruso de Inversión Directa, Kirill Dmitriev.

En la videoconferencia también participó el presidente de Laboratorios Richmond, Marcelo Figueiras, quien autorizó el lanzamiento de la fabricación en la Argentina.

En ese sentido, resaltó que "Rusia es un país que ha sabido desarrollar la investigación, la ciencia y la tecnología" y dijo: "Siempre tuvimos confianza en esa capacidad para el desarrollo de una vacuna tan importante y que tanto esperaba el mundo".

Antes de Alberto Fernández, habló Putin, quien remarcó que la vacuna Sputnik V es "una de las más eficientes del mundo" y que "está registrada en 66 países".

"Cuando hablamos de la salud de las personas hay que dejar las especulaciones políticas", sostuvo el mandatario ruso en relación a los prejuicios en varios países que habían sido refractarios a la llegada de la Sputnik V.

Al respecto, aseguró que "las pruebas demuestran que nuestra vacuna es la mejor" entre las producidas en el mundo y aseveró que el fármaco del Instituto Gamaleya servirá para la "cooperación de mutuo beneficio y será muy útil para el desarrollo de la ciencia mundial y de la industria farmacéutica".