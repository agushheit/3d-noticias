---
category: Estado Real
date: 2021-12-15T06:00:23-03:00
thumbnail: https://assets.3dnoticias.com.ar/acueducto.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Perotti recibió a una delegación del fondo Kuwaití que financiará la primera
  etapa del acueducto Santa Fe / Córdoba
title: Perotti recibió a una delegación del fondo Kuwaití que financiará la primera
  etapa del acueducto Santa Fe / Córdoba
entradilla: Durante 15 días, representantes de los gobiernos de Santa Fe, Córdoba
  y de la Nación, junto a miembros de la delegación kuwaití, mantendrán reuniones
  técnicas y visitarán los lugares donde se realizará la obra.

---
El gobernador Omar Perotti recibió este martes a representantes de la misión técnica del Fondo Kuwaití, en el marco del financiamiento del acueducto interprovincial Santa Fe-Córdoba quienes, junto a representantes de los gobiernos de ambas provincias y de la Nación, dieron inicio a una serie de reuniones de trabajo.

El encuentro se desarrolló en la Casa de Gobierno de la ciudad de Santa Fe y el objetivo fue presentar al Fondo, el proyecto completo del acueducto (en dos etapas) y discutir dos documentos de trabajo (borradores de acuerdos de préstamo y de garantía) en relación al financiamiento del Bloque A de la Fase I de la Etapa I, consistente en la estación de bombeo y la planta potabilizadora, instancia que, junto con la Etapa II, tendrá un impacto directo sobre 410.000 habitantes de ambas provincias.

Del encuentro participaron entre otros, el secretario de Empresas y Servicios Públicos, Carlos Maina; la secretaria de Gestión Federal, Candelaria González del Pino; el subsecretario de Proyecto de Inversión, Claudio Vissio; el secretario de Recursos Hídricos de la provincia de Córdoba, Edgar Castello; el jefe de la delegación kuwaití, Tareq Al Menayes; y el presidente de Aguas Santafesinas, Hugo Morzán, además de representantes de los gobiernos Nacional, de la provincia de Córdoba y la delegación kuwaití.

Tras la reunión, el gobernador Perotti explicó que “el objetivo del encuentro, para Santa Fe y Córdoba, es que podamos llevar adelante los trabajos técnicos que nos permitan cerrar la primera etapa del financiamiento del acueducto biprovincial y que tengamos, en base a eso, la posibilidad de sumar una obra importante para poblaciones del centro-oeste de nuestra provincia y, en esta primera etapa, de la ciudad de San Francisco de Córdoba”.

“Cerrar esta primera etapa es fundamental -prosiguió Perotti-, cubriría una primera etapa con un desembolso de 50 millones de dólares, con lo que se iniciarían las obras desde Santa Fe. Es la etapa más importante, que es la instancia de captación para después trasladarla en el recorrido por nuestra provincia y en la generación de abastecimiento a las localidades de nuestro territorio hasta la ciudad de San Francisco”, detalló el mandatario santafesino.

**UN VÍNCULO CONSOLIDADO**  
Luego, el gobernador recordó que “desde 1994, durante la gobernación de Carlos Reutemann, Santa Fe tiene una relación con el Fondo de Kuwait que comenzó con una asistencia financiera para rutas provinciales y después fue derivándose a distintas obras de saneamiento y de acueductos”, y valoró que “éste vínculo no ha tenido alteraciones, independientemente de los distintos momentos financieros de la Argentina”.

En ese sentido, Perotti brindó un especial “agradecimiento a la actitud que siempre ha tenido el Estado de Kuwait y las autoridades del Fondo, que en los distintos momentos que vivió nuestro país siempre ha estado presente. Nunca dejaron de asistirnos y es así como queremos que ustedes se sientan en nuestra provincia”.

“Por esas obras iniciales –continuó el gobernador-, por las que continuaron en el transcurso de los años, mi deseo hoy es que ustedes sientan que para nosotros es la conformación de un vínculo sólido, y queremos que se lleven en esta visita una reafirmación de ese compromiso”. Y agregó: “No son un banco más, no son un Fondo más, nosotros consideramos al Estado de Kuwait y al Fondo como verdaderos amigos que han consolidado una relación a lo largo de todos estos años”.

**APUESTA AL DESARROLLO**  
A su turno, el jefe de la delegación kuwaití, Tareq Al Menayes, afirmó que “es un honor y un privilegio estar aquí trabajando en un proyecto tan importante”, y recordó que el Fondo fue establecido en 1961 por el gobierno del Estado Kuwaití “para promover el desarrollo en determinados países. Sus operaciones abarcan a más de 100 países, ha financiado más de 1.000 proyectos de desarrollo y ha concedido préstamos especiales y técnicos”.

Al Menayes mencionó que “esta amistad que comenzó en 1994, condujo al otorgamiento de cinco préstamos destinados a proyectos hídricos y viales por un total de 231 millones de dólares”, y reconoció que “estamos particularmente orgullosos de dos de estos proyectos que se llevaron a cabo en la provincia de Santa Fe: puntualmente uno de 1994 en una traza vial, y otro de 2014 referido a un proyecto hídrico”.

Finalmente, expresó su satisfacción por el trabajo conjunto realizado hasta aquí y aseguró que “esta colaboración biprovincial demuestra la intención del Fondo de continuar apostando al desarrollo de ambas provincias”.

**PRIMERA ETAPA**  
La secretaria de Gestión Federal de la provincia, Candelaria González del Pino, recordó que “en octubre del año pasado, en presencia del presidente de la Nación y del embajador de Kuwait, junto a los gobernadores de Santa Fe y Córdoba, se presentó el proyecto que hoy nos convoca: el acueducto interprovincial Santa Fe-Córdoba. Hemos continuado con este trabajo desde aquel momento, y hoy estamos comenzando con 15 días presenciales de encuentros, con reuniones técnicas y visitas al lugar donde va a comenzar esta gran obra”.

El acueducto interprovincial pasará por las localidades de Barrancas, San Fabián, Arocena, Coronda, Larrechea, San Eugenio, Gálvez, Gessler, Loma Alta, López, Campo Piaggio, Colonia Belgrano, San Martín de las Escobas, Traill, Cañada Rosquín, Casas y Las Bandurrias.

En ese sentido, el secretario de Empresas y Servicios Públicos de la provincia, Carlos Maina, explicó que se conformaron dos equipos de trabajo para los próximos 15 días, “uno estrictamente técnico y otro para lo financiero y lo concerniente a la unidad ejecutora biprovincial”.

El funcionario indicó que “la obra en esta primera etapa, está prevista en ocho bloques de financiamiento y estamos tratando con la delegación del Fondo de Kuwait en el primero, el denominado Bloque A, que es el más importante, porque es el corazón de la obra y corresponde a la toma sobre el río y la planta potabilizadora, que da inicio a los trabajos para después colocar cañerías hasta la ciudad de San Francisco”.

Por su parte, el secretario de Recursos Hídricos de Córdoba, Edgar Castello, resaltó “la importancia del trabajo conjunto que venimos desarrollando y que podamos llegar a buen puerto en esta decisión”, y añadió que “la importancia de llegar con el agua potable a estas regiones se debe a que son de las más productivas de Argentina”.

**PRESENTES**  
Del encuentro en el Salón Blanco participaron también, en representación del gobierno nacional, el secretario de Asuntos Estratégicos, Guido Edul; el subsecretario de Relaciones Financieras Internacionales para el Desarrollo, Martín Oliva; y el coordinador de Proyectos con Financiamiento Externo Bilateral, David Tauss.

Por la delegación kuwuaití estuvieron Hassan El Massri, Mohammed Abdulazz, Naser Al Mutari, Mis Manar Mansour, Munzer Hamdeh, Talai Alnukhialan y Lucas Reiris.

Y por la delegación de la provincia de Córdoba lo hicieron, el director general de Asuntos Legales, José Ignacio Torino; y Santiago Picasso y Pablo Weirzbick Pedrotti, por la Administacion Provincial de Recursos Hídricos.