---
category: Estado Real
date: 2021-08-30T06:00:37-03:00
thumbnail: https://assets.3dnoticias.com.ar/TAMBEROS.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La provincia entregó fondos por 1,5 millones de pesos a pequeños tamberos
  del norte santafesino
title: La provincia entregó fondos por 1,5 millones de pesos a pequeños tamberos del
  norte santafesino
entradilla: El objetivo es continuar con el apoyo económico para poder cubrir las
  necesidades de un sector con un alto componente de arraigo territorial.

---
El Ministerio de Producción, Ciencia y Tecnología, a través de la Dirección Provincial de Producción Lechera y Apícola de la Secretaría de Agroalimentos, entregó en Reconquista aportes no reintegrables a 10 pequeños productores tamberos de las localidades de Avellaneda, Arroyo Ceibal, Nicanor Molinas y Malabrigo, por un monto total de más de 1.500.000 pesos.

Los fondos son canalizados a través de la Asociación para el Desarrollo Regional con recursos aportados por el Ministerio. Luego de las entregas, el Director Provincial de Producción Lechera y Apícola, Abel Zenklusen, valoró: “Desde el gobierno provincial tomamos la iniciativa de apoyar a este sector de productores de baja escala. En esta oportunidad entregamos aportes no reintegrables por un monto total de 1,5 millones de pesos”.

“La idea es, por pedido del ministro Daniel Costmagna, continuar brindando apoyo y acompañando con recursos. Pero al mismo tiempo llevar adelante asesoramiento técnico a quien lo necesite. El mismo trabajo se realizará en otros departamentos de la provincia, como 9 de Julio, San Martín y La Capital, donde también hay grupos de productores de este perfil”, agregó el funcionario.

Además, brindando detalles de su visita al norte provincial, Zenklusen detalló: “También pusimos en conocimiento todos los financiamientos vigentes a través de Banco Nación y en la reunión con productores, pudimos detectar un buen número de productores en Reconquista y sus alrededores que necesitan el apoyo provincial”.

“Desde el Ministerio revelaremos las necesidades puntuales de cada productor y accionamos para llevar a cabo medidas concretas. La decisión política es estar presentes, ya que se trata de muchos puestos de trabajos y familias que dependen de esta producción, con un gran componente de arraigo territorial, sentido de permanencia y generación de empleo”, concluyó Zenklusen.