---
category: Agenda Ciudadana
date: 2021-06-28T06:00:06-03:00
thumbnail: https://assets.3dnoticias.com.ar/homenaje.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: El presidente y los gobernadores homenajearon a los fallecidos con Covid
title: El presidente y los gobernadores homenajearon a los fallecidos con Covid
entradilla: Perotti participó del acto de este domingo al mediodía en el Centro Cultural
  Kirchner. Fernández pidió construir "un país con diversidad y sin divisiones irreconciliables"

---
El presidente Alberto Fernández remarcó este domingo la necesidad de que "tanto pesar" por el coronavirus "se vuelva fuerza e impulso para construir el futuro de nuestro país con diversidad y sin divisiones irreconciliables", al encabezar un homenaje a los fallecidos por la pandemia de Covid junto a gobernadores, entre los que estuvo Omar Perotti, y al jefe de Gobierno porteño.

En un emotivo acto desarrollado este mediodía en el Centro Cultural Kirchner (CCK) y transmitido por cadena nacional, Fernández pidió cuidar a la Argentina para que "entre todos y todas, juntos y juntas, derrotaremos a la pandemia"

"Con diversidad, cuidémonos ente todos y juntos derrotaremos a la pandemia", pidió el Presidente, y abogó por la idea de estar "unidos para mancomunar, recuperar, reconstruir, y así podremos mirar el futuro con memoria y confianza".

Fernández afirmó que con esta pandemia "estamos presenciando un cataclismo que azota a la humanidad, con millones personas fallecidas en el mundo entero".

Y recordó que "cada una de las personas que fueron víctimas de la pandemia tenían un nombre, una vida, cada una tenía hija, hijo, hermanos, madres, padres, familiares y amigos"; y subrayó que "a quienes se han ido en estos momentos dolorosos no los olvidaremos nunca".

Entre los gobernadores presentes en homenaje por las víctimas del Covid estuvieron Raúl Jalil (Catamarca), Jorge Capitanich (Chaco), Gustavo Valdés (Corrientes), Gustavo Bordet (Entre Ríos), Sergio Ziliotto (La Pampa) y Rodolfo Suárez (Mendoza), Axel Kicillof (Buenos Aires), Mariano Arcioni (Chubut), Gildo Insfrán (Formosa), Gerardo Morales (Jujuy), Ricardo Quintela (La Rioja), Oscar Herrera Ahuad (Misiones), Omar Gutiérrez (Neuquén), Sergio Uñac (San Juan), Alberto Rodríguez Saá (San Luis), Omar Perotti (Santa Fe), Gerardo Zamora (Santiago del Estero), Gustavo Melella (Tierra del Fuego) y Juan Manzur (Tucumán); el jefe de Gobierno porteño, Horacio Rodríguez Larreta; y los vicegobernadores Manuel Calvo (Córdoba) y Eugenio Quiroga (Santa Cruz).