---
category: Agenda Ciudadana
date: 2021-11-20T06:00:54-03:00
thumbnail: https://assets.3dnoticias.com.ar/carneprecio.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Congelarán el precio de la carne para el fin de semana largo, pero aumentará
  un 20% a fin de año
title: Congelarán el precio de la carne para el fin de semana largo, pero aumentará
  un 20% a fin de año
entradilla: 'El gobierno y los supermercados acordaron frenar los aumentos de precios
  de la carne en estos días. Sin embargo, se espera que antes de fin de año suba un
  20 por ciento

'

---
El Gobierno nacional y la Asociación Supermercados Unidos (ASU) alcanzaron un acuerdo por el cual se congelará el precio de la carne vacuna durante el fin de semana largo, tras los aumentos en el precio de la hacienda de esta semana. Sin embargo, se espera que antes de fin de año se produzca un aumento que rondará el 20 por ciento.

Así lo confirmaron fuentes oficiales y de la cámara empresaria, cuyo entendimiento para no incrementar los valores en góndola estará vigente hasta el lunes inclusive.

Las cadenas de supermercados que acompañarán la medida serán Coto, Changomás, Cencosud (Jumbo, Disco y Vea) Carrefour, La Anónima y Día%, firmas que también participan en la oferta de cortes populares a precios rebajados.

Asimismo, fuentes oficiales indicaron que "la próxima semana continuarán las conversaciones con el objetivo de arribar a una solución consensuada que garantice la estabilidad del precio de las carnes, evitando así sobresaltos que afecten a los consumidores en el último tramo del año".

El compromiso asumido por los supermercados con el Gobierno nacional surge tras los fuertes aumentos que se registraron en los últimos días en el mercado de Liniers en los precios de la hacienda de consumo, que en promedio, concretó un salto del 20% entre el viernes de la semana pasada y este martes.

De hecho, en ese período, el precio del novillo pasó de $ 208,43 el kilo a $ 221,57, mientras que el novillito creció de $ 214,79 a $ 247,10 y la vaquillona de $ 208,46 a $ 237,13.

Al cierre de hoy, los precios mostraron un leve descenso, como marcó el ministro de Economía, Martín Guzmán, en una entrevista por Radio Con Vos, al tiempo que indicó que el Gobierno nacional "está llevando adelante todas las gestiones necesarias para estabilizar el precio de los productos clave en la canasta".

Así, el novillo se ubicó en $ 220,21, el novillito en $ 234,26 y la vaquillona en $ 224,80.

Es por eso que ayer Guzmán, mantuvo un encuentro con sus pares de Desarrollo Productivo, Matías Kulfas; de Agricultura, Ganadería y Pesca, Julián Domínguez; y con el secretario de Comercio Interior, Roberto Feletti, en el que se analizó y se trabajó sobre la dinámica de los precios de las carnes para "garantizar su estabilidad".

La carne vacuna es uno de los alimentos básicos que más aumentó en la comparación interanual, incluso, por encima de la inflación y que mantuvo un crecimiento desde finales del año pasado.

El Gobierno nacional decidió a mediados de mayo cerrar las exportaciones para después establecer un esquema en el cual solo se permitía exportar el 50% del volumen mensual respecto al año anterior y prohibió despachar siete cortes populares hasta fin de año.

A partir de su implementación, en el período julio-octubre el precio de la carne registró una estabilización y acumuló una baja del 3%, según mediciones el Instituto de Promoción de la Carne Vacuna (Ipcva), a pesar de que en la comparación interanual el precio promedio se posicionó un 70,3%.

Por otro lado, para atenuar estas subas, el Gobierno renovó a principios del mes pasado un acuerdo con el sector exportador para ofertar 6.000 toneladas mensuales de cortes a precios accesibles en más de 1.000 bocas de expendio.

El acuerdo -rubricado por los Ministerios de Desarrollo Productivo y de Agricultura, Ganadería y Pesca- mantiene los precios vigentes hasta el momento: asado de tira a $359 el kilo; vacío a $499; matambre a $549; falda a $229; tapa de asado a $429; cuadrada/bola de lomo a $515; roast beef/aguja a $409; carnaza/cogote a $379; y paleta a $485 el kilo.

**Aumentos antes de las Fiestas**

En Santa Fe los carniceros ya vaticinan que antes de las fiestas la suba de la carne puede alcanzar como máximo un 20%. Se habla de la falta de abastecimiento de animales en el mercado de Liniers, la baja en el consumo por los altos precios en los que se reemplaza por pollo o cerdo, entre los factores que influyen en la conformación de precios pero también "en el rubro se le echa mucho la culpa a los intermediarios, en este caso a los matarifes, ni los frigoríficos, ni los productores, son quienes aprovechando la época de las fiestas, pone en practica la «viveza gaucha» y entonces aumentan los precios", dijo  Marcos, quien tiene su carnicería hace 11 años.

"Las subas comienzan en noviembre alcanzando entre un 2 y 3 por ciento y a principio de diciembre ya trepa al 10%. Además a esto debemos sumarle casi siempre un 5 adicional antes de las fiestas, lo que suma un total de 20". El carnicero dijo que esto "no ayuda para nada al rubro que de por sí está golpeado porque las ventas son muy bajas" y agregó que "esta semana entre el lunes y este jueves la carne subió un 5%".