---
category: La Ciudad
date: 2021-10-20T06:15:49-03:00
thumbnail: https://assets.3dnoticias.com.ar/CANCERMAMA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Municipalidad despliega una estrategia de prevención del cáncer de mama
title: La Municipalidad despliega una estrategia de prevención del cáncer de mama
entradilla: Agentes municipales junto al grupo de acción Chicas Pink salen en la búsqueda
  de mujeres que no se hayan realizado el control ginecológico anual. La actividad
  se enmarca en el mes de lucha contra el cáncer de mama.

---
Este martes 19 de octubre- se conmemoró el Día Mundial de Lucha contra el Cáncer de Mama. En ese marco, el municipio desplegó una estrategia que se inició ayer en barrio Nuevo Horizonte y se trasladará luego a las Estaciones. La actividad consiste en salir al encuentro de mujeres que se hayan salteado el control ginecológico anual, para convocarlas al centro de salud barrial y realizarles el examen médico.

La tarea está a cargo de agentes sanitarios municipales en coordinación con Chicas Pink, un grupo de mujeres que desarrolla tareas en relación a la temática desde hace tiempo en la ciudad. En ese sentido, entre lunes y mates, se visitaron las instituciones educativas del barrio, en el horario de 10 a 14, cuando se concentra la mayor cantidad de mujeres. Está orientada a aquellas de entre 40 y 70 años que por algún motivo no hayan completado los controles.

El director de Salud municipal, César Pauloni, explicó que la campaña consta de dos etapas: las visitas a la escuela y el jardín del barrio, con el objetivo de otorgar turnos médicos para los controles integrales de la salud. Posteriormente, se realizará la búsqueda activa de mujeres, puerta a puerta, en los domicilios particulares.

Según detalló el funcionario, “evidenciamos que durante la pandemia hubo mucha deserción de los controles médicos de todo tipo, especialmente los ginecológicos. Por eso queremos buscar activamente a las mujeres para que se acerquen a completar los chequeos”, afirmó.

En ese sentido, Pauloni recordó que el de mama es uno de los tipos de cánceres más frecuentes y calificó como “sumamente importante la visibilización de esta problemática en esta fecha en particular”. Por ello, mencionó que la estrategia se extenderá luego a las estaciones municipales de diferentes barrios, de modo de “continuar con acciones extendidas en el tiempo y generar en la comunidad, una conciencia de la importancia de los controles”.