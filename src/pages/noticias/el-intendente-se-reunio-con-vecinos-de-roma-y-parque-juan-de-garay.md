---
category: La Ciudad
date: 2021-01-18T10:29:08Z
thumbnail: https://assets.3dnoticias.com.ar/jaton.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: El intendente se reunió con vecinos de Roma y Parque Juan de Garay
title: El intendente se reunió con vecinos de Roma y Parque Juan de Garay
entradilla: 'Emilio Jatón escuchó sus necesidades y les presentó los proyectos previstos
  para ambos barrios. Continuarán las mejoras en el gran pulmón verde de la ciudad
  y ripiarán Roque Saenz Peña entre Mendoza y Lisandro de Torre. '

---
En una de las canchas del club Newels Old Boys, vecinos de barrio Roma -cuya jurisdicción comprende las vecinales del mismo nombre y Parque Juan de Garay- recibieron al intendente Emilio Jatón, quien llegó con algunos integrantes de su gabinete para dialogar sobre los proyectos que están previsto para esta zona de la ciudad. También escucharon cada uno de los reclamos para darles soluciones y lograr un trabajo coordinado y en conjunto.

En la ocasión, Jatón les confirmó que en pocos días se colocará ripio en Roque Saenz Peña entre Mendoza y Lisandro de Torre. Se trata de un viejo pedido de los vecinos. También se harán obras de mantenimiento en Santiago de Chile y Juan Díaz de Solis, ambas pavimentadas pero con un cierto deterioro.

El tema seguridad también se puso sobre la mesa. Los vecinos le mostraron al intendente un estudio que tiene sobre cuáles son los lugares donde más suceden los robos y arrebatos, y las zonas más peligrosas. La intención es colocar alarmas comunitarias para disuadir la inseguridad. Desde el municipio se coordinó una reunión con el área de Control para avanzar en esta idea.

“Nos juntamos con integrantes de ambas vecinales y también con vecinos del lugar y nos plantean cuestiones que tienen que ver con el barrio”, empezó a contar Jatón y luego agregó: “Les anunciamos que vamos a ripiar Sáenz Peña , entre Mendoza y Lisandro, una calle clave. Además se están reponiendo luminarias y se van a hacer otras mejoras e intervenciones necesarias para mejorar el barrio”.

Luego se refirió a los encuentros con los vecinos que está teniendo en distintos barrios de la ciudad, y al respecto aseguró: “Lo importante de estas reuniones es que cada vecino se acerca y plantea su necesidad de alumbrado público hasta su cuestión personal, cómo es la convivencia entre los vecinos; y ahí está la parte positiva porque estamos haciendo la ciudad con ellos mismos”.

“Nosotros podemos tener un programa y que ahora se ve cambiado por la situación que estamos viviendo pero hay cuestiones que para los vecinos son primordiales y vamos a ir detrás de ellas. Nos plantean cosas que casi son obligaciones del municipio y por eso las vamos a hacer”, manifestó el intendente.

![](https://assets.3dnoticias.com.ar/jaton1.jpg)

**Temas tratados**

La lista de temas que se trataron es larga. El tratamiento de la basura y la recolección fue uno de ellos. El compromiso de los vecinos de trabajar para evitar la conformación de minibasurales. También surgieron otros como tareas de bacheo, de poda, reposición de luminarias y de bocas de registro, entre otros. Además, se habló de la creación de una plaza.

El presidente de la vecinal Roma, Eduardo Rodríguez destacó “la buena noticia” de que se va a ripiar Roque Saenz Peña: “Es muy necesario para el desahogo del tránsito del propio barrio hacia el sur”, pero además, al estar en condiciones, se evita la formación de minibasurales. “Es de tierra y con cada lluvia se deteriora un poco más, es una calle abandonada. Por eso le agradecemos a Jatón la decisión porque para nosotros es clave y va a solucionar un montón de problemas”, destacó el vecinalista.

También destacó el gran avance que tiene el proyecto de instalación de alarmas comunitarias. “Tenemos en la vecinal una comisión de seguridad que está trabajando mucho en este tema. La intención es ayudar a disuadir la inseguridad. Como le dijimos al intendente, no solo pedimos cosas sino también queremos colaborar y trabajar en conjunto en pos de mejorar el barrio”, destacó Rodríguez.

**Cambios en el parque**

La tesorera de la vecinal Parque Juan de Garay, Graciela Pieroni aprovechó para agradecerle al intendente Emilio Jatón por “los cambios que se produjeron en los últimos tiempos en el Parque que venía de un abandono total”. Al mismo tiempo, agregó: “Sabemos que lleva tiempo y dinero pero de a poco vamos solucionando los problemas que tenemos y principalmente el saneamiento de los lagos que requiere una intervención ambiental más importante”.

Los vecinos y la institución están muy comprometidos con el mantenimiento de este gran espacio verde de la ciudad, pero en ciertas cosas se necesita la ayuda del Estado. “Se hicieron muchas cosas, pero aún faltan. La iluminación es clave y hoy es obsoleta porque aún siguen los focos amarillos y pedimos que de apoco se vaya cambiando a led, eso le daría otra vida e impediría delitos”, explicó más adelante.

“Tenemos confianza en que el intendente va a ir de a poco solucionando estos problemas. Entendemos que la pandemia y la situación en la que se encontró la Municipalidad, dificultan un poco las cosas pero estamos esperanzados que vamos a salir adelante y sobre todo si trabajamos en conjunto”, finalizó Pieroni.