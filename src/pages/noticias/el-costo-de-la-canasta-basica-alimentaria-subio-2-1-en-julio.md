---
category: Agenda Ciudadana
date: 2021-08-20T06:00:32-03:00
thumbnail: https://assets.3dnoticias.com.ar/CANASTA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: El costo de la canasta básica alimentaria subió 2,1% en julio
title: El costo de la canasta básica alimentaria subió 2,1% en julio
entradilla: Una pareja tipo con dos hijos menores necesitó percibir ingresos por $29.002,86
  para no caer en situación de indigencia, informó el Instituto Nacional de Estadística
  y Censos.

---
El costo de la canasta básica alimentaria (CBA) registró en julio un incremento del 2,1%, por lo que una pareja con dos hijos menores necesitó percibir ingresos por $ 29.002,86 para no caer en situación de indigencia, informó el Instituto Nacional de Estadística y Censos (Indec).

En tanto, el costo de la canasta básica total (CBT), que marca la línea de pobreza, fue en junio de $ 67.576,66 con un aumento del 1,6% mensual.

La variación registrada en los precios de los productos que integran la canasta alimentaria y de la canasta total -que comprende también a tarifas de servicio públicos y vestimenta- se ubicaron por debajo de la inflación general de julio, de 3%, según los números informados por el Indec.

De esta forma. en los primeros siete meses del año, el valor de la CBA se incrementó el 27,87%, mientras que el costo de la CBT avanzó 24,66%.

En tanto, a lo largo del último año la canasta alimentaria acumuló una suba de 58,3% y la canasta básica total, de 56,4%.