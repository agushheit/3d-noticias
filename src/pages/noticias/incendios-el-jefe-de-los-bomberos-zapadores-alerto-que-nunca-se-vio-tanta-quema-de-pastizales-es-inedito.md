---
category: Agenda Ciudadana
date: 2021-08-30T06:15:59-03:00
thumbnail: https://assets.3dnoticias.com.ar/QUEMAS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Incendios: el jefe de los Bomberos Zapadores alertó que "nunca se vio tanta
  quema de pastizales, es inédito"'
title: 'Incendios: el jefe de los Bomberos Zapadores alertó que "nunca se vio tanta
  quema de pastizales, es inédito"'
entradilla: Lo afirmó el jefe de los Bomberos Zapadores de la provincia Andrés Lastorta.
  "No damos abasto con la cantidad de quemas en varias zonas de la provincia y en
  las islas", reconoció.

---
Los incendios de magnitud que combaten los brigadistas de Santa Fe y Entre Ríos en un sector de islas de ambas provincias puso de manifiesto la gran cantidad de hectáreas que ya se consumieron en lo que va del año: 50 mil. A esta altura, durante el año pasado ya se habían quemado 300 mil hectáreas, pero desde el Observatorio de la Universidad Nacional de Rosario (UNR) estiman que esta temporada podrían ser 400 mil, dentro de un pronóstico de pocas precipitaciones y un río con tendencia a alcanzar la máxima altura negativa ostentada desde 1944.

En diálogo con el programa Todavía no es tarde (Radio 2), el jefe de los Bomberos Zapadores de la provincia de Santa Fe, Andrés Lastorta, dijo que “lo que se está viviendo desde el año pasado es inédito, nunca se vio tanta quema de pastizales. Esto se debe a la sequía y a la gente que prende fuego sin tomar dimensión del daño que está generando al ambiente y a las personas".

“No damos abasto con la cantidad de quemas en varias zonas de la provincia y en las islas”, reconoció.

**En la ciudad**

En cuanto a los focos en la zona metropolitana Santa Fe, se montó un operativo en el puerto de la ciudad el 21 de agosto en donde trabajan más de 50 personas, entre integrantes del Ministerio de Ambiente y Cambio Climático, de la secretaría de Protección Civil, de la Brigada de Atención y Prevención de Emergencias Provincial, de Bomberos Voluntarios, del Centro de Monitoreo Meteorológico y Climático, de Parques Nacionales y del Ministerio de Ambiente y Desarrollo Sostenible de Nación. Además, integrantes del Ministerio de Seguridad y Prefectura Naval Argentina se encuentran a disposición en la zona, trabajando mediante patrullaje para la prevención de posibles nuevos focos.

**El impacto de las quemas**

En diálogo con el programa Ahí Vamos Juan Antonio Rivera, doctor en Ciencias de la Atmósfera y los Océanos e investigador del Conicet, reflexionó sobre el cambio climático y el impacto de las quemas: “Siempre pensamos que (el cambio climático) va a pasar en el futuro y nunca pensamos que ese futuro ya lo estamos viviendo desde hace varios años”.

“Las emisiones de gases efecto invernadero son las responsables del incremento en la temperatura global y el incremento en la frecuencia y severidad de los eventos climáticos extremos”, recordó, mencionando los fenómenos que están ocurriendo en el hemisferio norte asociados a olas de calor y tormentas severas. “Es todo un combo que justamente merece ser tenido en cuenta por los gobiernos”, advirtió.

Según el experto, quien se desempeña en el Consejo Nacional de Investigaciones Científicas y Técnicas, el dióxido de carbono es el responsable principal del incremento en la temperatura global, seguido por otros gases efecto invernadero como el metano y el óxido nitroso. “En el caso del metano, su residencia en la atmósfera es de una o dos décadas aproximadamente mientras que el dióxido de carbono tiene un tiempo de residencia inmenso, puede llegar a los siglos”, explicó.