---
layout: Noticia con imagen
author: "  Fuente: Diario Uno"
resumen: Clubes y jardines de infantes
category: La Ciudad
title: 'Colonias, clubes y jardines de infantes: Perotti habló de "definir
  incorporaciones de manera gradual y ordenada"'
entradilla: El gobernador se refirió a sectores que esperan ser habilitados.
  Analizan la vuelta de actividades deportivas al aire libre.
date: 2020-11-08T12:53:00.000Z
thumbnail: https://assets.3dnoticias.com.ar/piscina.jpg
---
El gobernador de la provincia, Omar Perotti, hizo referencia a algunos sectores que todavía esperan la habilitación y los permisos del gobierno provincial para comenzar a funcionar bajo protocolos

Luego de los anuncios del presidente de la Nación, vinculados a una "nueva etapa etapa de convivencia con el Covid" orientada fundamentalmente a Buenos Aires y Amba, el mandatario provincial adelantó que las próximas incorporaciones estarán vinculadas con actividades al aire libre.

"Continuamos trabajando para acentuar la baja de contagios y preparando los primeros protocolos de aire libre con los cuales se harán las próximas incorporaciones", escribió en la red social Twitter.

Con relación a las colonias de verano, clubes y jardines de infantes, apuntó: "Estamos elaborando protocolos. Evaluamos la situación de cada sector social, educativo, deportivo, comercial o productivo para definir posibles incorporaciones de manera gradual y ordenada".

Y definió: "La lucha contra la pandemia es larga, por eso en la medida que cumplamos los protocolos y los cuidados, podremos ir incorporando actividades. Como siempre decimos, la prioridad es cuidar la salud de todos y todas reactivando el movimiento de la economía".

![](https://assets.3dnoticias.com.ar/status-perotti.jpg)

Ver en [Twitter](https://twitter.com/omarperotti/status/1325088232292356096?s=20)

Este viernes, luego de los anuncios del presidente Fernández, Perotti se expresó a través de la misma red social. Manifestó que la provincia de Santa Fe se mantendrá por los 14 próximos días de la misma manera en que está hasta el momento: es decir continuará en fase de aislamiento y además no se habilitarán nuevas actividades.

"Luego de los anuncios del presidente, decidimos mantener el mismo esquema en la modalidad de convivencia con el virus en nuestra provincia, por lo cual no se habilitarán nuevas actividades y continuaremos con los mismos protocolos vigentes hasta el momento" dijo en referencia a las medidas fijadas los últimos 14 días en el territorio provincial, es decir en Aislamiento Social y Preventivo Obligatorio.

"Evaluamos permanentemente la situación epidemiológica, y analizamos ampliar la cantidad de trabajadores en obras de la actividad privada. Lo mismo con permitir un mayor número de feligreses en las ceremonias religiosas", apuntó.

Y adelanto: "Teniendo en cuenta los protocolos presentados, la práctica de algunas actividades de naturaleza deportiva está siendo considerada para su habilitación"