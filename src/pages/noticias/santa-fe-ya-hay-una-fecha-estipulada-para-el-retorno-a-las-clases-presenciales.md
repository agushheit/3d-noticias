---
category: Agenda Ciudadana
date: 2021-06-19T06:15:01-03:00
thumbnail: https://assets.3dnoticias.com.ar/CLASES.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Santa Fe: ya hay una fecha estipulada para el retorno a las clases presenciales'
title: 'Santa Fe: ya hay una fecha estipulada para el retorno a las clases presenciales'
entradilla: El gobierno provincial anunció este viernes otras localidades que se sumarán
  a la modalidad presencial, donde no están las grandes ciudades como Santa Fe y Rosario.

---
Como anticipó la ministra de Salud de Santa Fe, Sonia Martorano, la vuelta a la presencialidad en la totalidad de las escuelas por ahora no es posible. La funcionaria explicó que “hay dos factores que lo impiden: la incidencia de casos (cantidad de casos cada 100 mil habitantes en los últimos 14 días), que no debe superar los 500; y la ocupación de camas, que no debe superar el 80%, pero que está por encima de ese número, tanto en el sector público como en el privado”.

“Queremos volver a la presencialidad, la infraestructura está preparada y los niños se cuidan más que los adultos. No me preocupa lo que pasa adentro sino lo que tiene que ver con lo que pasa afuera”, agregó. Martorano afirmó que Santa Fe “está en el punto justo donde pareciera que la curva (de contagios) se amesetó y tendría una tendencia a bajar. Pero esto va de la mano con que sostengamos las medidas y que nos cuidemos. Es un momento clave para que todos lleguen a vacunarse”, remarcó.

Esta semana, 253 poblaciones de la provincia de Santa Fe volvieron a las clases presenciales alternadas, con un esquema gradual, con una apertura en aquellos lugares donde la ola de contagios por coronavirus mermó a partir de las medidas restrictivas dispuestas por el gobierno nacional. En las próximas horas, el gobierno anunciará otras localidades que se sumarán a la modalidad presencial.

Ahora, el objetivo del gobierno de Santa Fe es que el próximo lunes 28 de junio (a dos semanas del inicio de las vacaciones de invierno) retornen las clases presenciales en las grandes ciudades, como Santa Fe y Rosario, así como en el resto de la provincia.

El gremio de los docentes de escuelas públicas de la provincia Amsafé ya había deslizado esta posibilidad. “Si en la próxima semana mejoran los índices de contagios, es posible volver a las escuelas. El índice de replicación, el RT, está bajando, es decir que los contagios se duplican cada más día. Si logramos sostener eso, creo que se podría volver a clases antes de las vacaciones”, señaló la secretaria general del gremio, Sonia Alesso.