---
layout: Noticia con imagen
author: "Fuente: Diario UNO"
resumen: Amenazas a un Concejal
category: Agenda Ciudadana
title: Le dejaron un muñeco con un tiro en la cabeza en la puerta de la casa a
  un concejal de Rincón
entradilla: El episodio sucedió en la madrugada del sábado en la puerta de
  ingreso a la vivienda de la casa del concejal radical del bloque Vamos Juntos
  de Rincón, Andrés Sopérez. Repudio de varias instituciones.
date: 2020-11-16T13:29:33.573Z
thumbnail: https://assets.3dnoticias.com.ar/soperes.jpg
---
Este sábado por la madrugada, pasadas las 3 y cuando el concejal Andrés Sopérez regresaba a su casa en la ciudad de Rincón, se encontró colgado en el portón de ingreso un muñeco de trapo, con un balazo en la cabeza y algunos mensajes.

Él mismo decidió fotografiarlo y publicarlo en sus redes sociales y allí se puede ver: un muñeco con un balazo en la frente, alfileres, manchas de sangre y amenazas en papel como , “Corrupto esta te va…”; “Intendente ja ja?”; “Cuidate!!!”.

El concejal de Vamos Juntos radicó la denuncia y dijo no tener inconvenientes con nadie y que desde hace mucho tiempo vive en la ciudad. Además nunca dijo querer ser candidato a intendente, y que "esto parece parte de una campaña sucia en la previa a las elecciones". reconoció tener un poco de miedo por su familia: mujer e hijos, por lo que decidió hacer la denuncia.

El Comité La Capital de la UCR manifestó su repudio por lo ocurrido.

Fuente: [Diario UNO](https://www.unosantafe.com.ar/santa-fe/le-dejaron-un-muneco-un-tiro-la-cabeza-la-puerta-la-casa-un-concejal-rincon-n2623702.html)