---
category: Agenda Ciudadana
date: 2020-12-05T12:45:05Z
thumbnail: https://assets.3dnoticias.com.ar/VACUNACION.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Primera reunión del Comité de Vacunación para la campaña masiva contra el
  Covid 19
title: Primera reunión del Comité de Vacunación para la campaña masiva contra el Covid
  19
entradilla: Participaron referentes ministeriales de la provincia de Santa Fe y abordaron
  la logística para el operativo de inoculación.

---
El Ministerio de Salud viene realizando diferentes encuentros para avanzar en la logística de aplicación de vacunas contra el Coronavirus.

Ante esto, se llevó adelante la reunión del Comité de Vacunación que permitirá confeccionar el plan que se implementará en toda la provincia.

Participaron del encuentro representantes de los ministerios de Educación, Seguridad, Salud, Desarrollo Social, Gobierno, Producción y del Ministerio de Defensa de la Nación.

En ese marco, la titular de la cartera sanitaria provincial, Sonia Martorano, presentó las líneas de trabajo y afirmó: “Estamos ante un mega operativo de vacunación que tiene una logística muy compleja”.

Además, agregó: “Hay que trabajar sobre la cadena de frío, compra de insumos, mucha interacción y sinergia para generar logística de centros de vacunación, porque es en un contexto de pandemia, donde no podemos tener conglomerado de personas”.

## **Población objetivo**

La población objetivo de la primera fase es de 1.200.00 personas, para aplicarse en enero, febrero y marzo. Vale aclarar que aquel paciente que reciba su primer dosis, debe recibir segunda dosis a los 21 o 28 días acorde a la vacuna que reciba y, luego de un plazo mínimo de 14 días de la segunda dosis podrá desarrollar anticuerpos.

Martorano, recordó el trabajo realizado durante este año y afirmó “que para el inicio de la pandemia se llevó adelante una logística muy importante, las compras eran un tema muy complejo, pero participaron diferentes áreas y pudimos resolverlo bien.

Más allá de las vacunas, este encuentro tiene que ver con la logística de cuidar en situación de pandemia a nuestra gente”, concluyó la ministra.

## **Registro de vacunadores voluntarios**

Desde la página web de la provincia, se puede acceder al formulario para inscribirse y participar del operativo: 

[https://servicios.santafe.gov.ar/voluntarios_vacunacion/.](https://servicios.santafe.gov.ar/voluntarios_vacunacion/. "https://servicios.santafe.gov.ar/voluntarios_vacunacion/.")