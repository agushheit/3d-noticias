---
category: Agenda Ciudadana
date: 2022-05-24T08:57:03-03:00
thumbnail: https://assets.3dnoticias.com.ar/congreso.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Noticias Argentinas
resumen: Diputados recalienta su agenda con el debate de Boleta Única, Compre Argentino
  y Consejo de la Magistratura
title: Diputados recalienta su agenda con el debate de Boleta Única, Compre Argentino
  y Consejo de la Magistratura
entradilla: La oposición insiste con la reforma electoral, a la cual el oficialismo
  se opone. Por la tarde, el FdT pasará a la ofensiva con la discusión de la reforma
  del Consejo de la Magistratura y la ley de Compre Argentino.

---
La **Cámara de Diputados retomará hoy su agenda** de trabajo cuando discuta, en primer turno, el proyecto opositor para implementar **la Boleta Única** de Papel, en tanto que por la tarde el oficialismo pasará a la ofensiva con la discusión de la reforma del **Consejo de la Magistratura y la ley de Compre Argentino.**

Las **comisiones de Asuntos Constitucionales, Justicia, Presupuesto y Hacienda,** que la semana pasada recibieron el aporte de una veintena de especialistas invitados de diferentes ámbitos (académico, ONG's, dirigentes políticos) volverán a reunirse en un plenario desde las 10.

La implementación de **la Boleta Única es un tema en el que Juntos por el Cambio viene insistiendo desde hace tiempo** y que logró instalar en la última sesión al aprobarse una moción para el emplazamiento de las comisiones y un cronograma de trabajo que culminaría el 31 de mayo con la firma de los dictámenes.

Con 132 votos, la moción para habilitar el tratamiento recibió el apoyo del interbloque Federal, de los liberales del bloque provincial de rionegrinos y hasta de la diputada del Partido Obrero Romina del Plá.

La legisladora del Frente de Izquierda, sin embargo, aclaró ayer que esa fuerza política no participa de las negociaciones en pos de un dictamen de todo el arco opositor.

El sistema de boleta única implica que toda la oferta electoral esté condensada en una única planilla suministrada por el Estado, la cual el elector deberá llevar al cuarto oscuro, donde marcará los casilleros con las opciones de su preferencia para cada una de las categorías.

De acuerdo al proyecto, el instrumento solamente se aplicaría para los cargos nacionales (esto es, diputados y senadores nacionales, Presidente y vicepresidente), y entraría en vigencia por primera vez en las elecciones del año que viene.

**Las críticas del Frente de Todos a la boleta única radican en que el diseño gráfico de la misma solo permite mostrar los primeros nombres de las listas de diputados nacionales,** por lo que el instrumento sería -según argumentan- funcional al ocultamiento de candidatos que están cuestionados ya sea por la Justicia o por la opinión pública.

La boleta única se usa en la mayoría de los países del mundo y hay experiencias también en nuestro país. El instrumento ya se utiliza en Santa Fe, Córdoba, Salta y la Ciudad Autónoma de Buenos Aires (CABA), en tanto que Mendoza lo acaba de aprobar.

**Entre las ventajas que enumera la oposición sobresale el hecho de que quedará atrás la reposición de boletas,** por la simple razón de que no habrá boletas en el cuarto oscuro. Con ello, se elimina el problema de la falta de papeletas y del fenómeno de las "boletas truchas".

Al quedar la impresión de las boletas a cargo del Estado, se achica la discrecionalidad en la asignación de fondos a los partidos para la campaña, y también tiene un impacto directo en la huella ecológica por los miles de litros de tinta y kilos de papel que se ahorrarán.

Por último, **se agilizaría el recuento de votos,** con la posibilidad de que se incorporen lectores que puedan escanear rápidamente las boletas únicas.

Desde las 14, las mismas comisiones (excluyendo la de Presupuesto) se abocarán al tratamiento de la **reforma del Consejo de la Magistratura** que empuja el oficialismo y que la oposición resiste.

**El proyecto oficialista , que ya tiene media sanción del Senado, propone una composición del cuerpo con 17 integrantes**, con paridad de género y excluye a miembros de la Corte Suprema.

Se prevé además la creación de cuatro regiones federales (AMBA, Norte, Centro y Sur), y cada una de ellas tendrá un representante abogado y otro representante juez en el Consejo de la Magistratura.

El resto de los miembros provendrían del Congreso (6 integrantes), del ámbito académico/universitario (dos representantes) y uno designado por el Poder Ejecutivo Nacional.

Desde la principal coalición opositora no acompañarán el proyecto oficialista, ya que excluye a la Corte Suprema de la presidencia del Consejo.

También a las 14, las comisiones de Industria y Presupuesto se reunirán en un plenario con el objetivo de apurar el dictamen de Compre Argentino, que eleva los márgenes de preferencia para las empresas nacionales en licitaciones públicas.

Esto significa que hasta cierto tope, las empresas de origen nacional podrán ser adjudicatarias de las licitaciones a pesar de haber ofertado precios por encima de los ofrecidos por compañías extranjeras.

El proyecto oficial contempla elevar del 15% al 20% los márgenes de preferencia para mipymes, del 8 al 15% para el resto de las empresas argentinos, y también se incluye un 12% para empresas extranjeras que tengan incorporados capitales nacionales.

Por otra parte, la iniciativa del Ejecutivo establece un margen adicional de 3% para empresas con composición mayoritaria de mujeres u otras identidades no binarias (en capital social y puestos jerárquicos y en el plantel general de la misma).

En la semana pasada, el oficialismo cedió al reclamo opositor para excluir al PAMI del régimen propuesto, pero de todos modos las posturas seguían alejadas y se continuaba negociando con Juntos por el Cambio para ver la posibilidad de acordar un dictamen unificado.

Durante la jornada se constituirán formalmente seis comisiones, con la designación de sus autoridades: Asuntos Municipales; Defensa del Consumidor, del usuario y de la competencia); Libertad de Expresión; Peticiones, Poderes y Reglamento; y Juicio Político.

**Escrito por**[**Sebastián Hadida**](https://noticiasargentinas.com/noticias/autor/462-sebastianhadida)

**Buenos Aires, NA**