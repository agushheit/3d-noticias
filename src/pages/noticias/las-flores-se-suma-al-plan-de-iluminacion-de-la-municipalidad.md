---
category: La Ciudad
date: 2021-07-14T08:18:45-03:00
thumbnail: https://assets.3dnoticias.com.ar/lasflores.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad de Santa Fe
resumen: Las Flores se suma al Plan de Iluminación de la Municipalidad
title: Las Flores se suma al Plan de Iluminación de la Municipalidad
entradilla: En total se colocarán 50 nuevos artefactos led. Además de reflectores
  para mejorar la iluminación en la zona de los monoblocks. Los trabajos cuentan con
  una inversión de unos 2,5 millones de pesos.

---
Ya comenzó la colocación de luces led que abarca el barrio Las Flores. El intendente Emilio Jatón recorrió este martes los trabajos que se extenderán, aproximadamente, por dos semanas. En total se colocarán 50 nuevos artefactos de iluminación led, además de reflectores para mejorar la iluminación en la zona de los monoblocks. Los trabajos cuentan con una inversión de unos $ 2,5 millones de pesos.

Luego de la recorrida, Matías Pons Estel, secretario de Obras y Espacio Público, indicó que actualmente se realizan cuatro frentes de trabajo de iluminación en diferentes sectores de la ciudad. En ese sentido, el funcionario detalló que el trabajo que se lleva adelante en Las Flores I es producto de la articulación entre vecinos y la Municipalidad. Era un reclamo que realizaban desde hace tiempo, se lo plantearon al intendente Jatón e inmediatamente se pusieron a trabajar los equipos técnicos”.

![](https://assets.3dnoticias.com.ar/alumbrado.jpg)

**Mejorar el barrio**

Alicia Castillo, presidenta de la vecinal de Las Flores conversó con el intendente sobre las obras que comenzaron en el barrio. Luego de visualizar los planos y los proyectos de obras, indicó: “Estuvimos hablando con el intendente de los cambios de luces comunes a led y del recambio de cables. Es muy interesante para el barrio. La iluminación nueva es muy buena. Si bien faltan cosas, por algo se empieza y festejamos cuando se hace una obra nueva”.

Por otra parte, Castillo destacó: “El hecho de que pongan nuevas led va a ayudar en el tema de la seguridad. Algunas de las calles por donde se van a trabajar, incluye: Arenales,  Azopardo, Jesús Misericordioso, Florencio Sánchez, Millán Medina, entre otras. Es una obra muy importante, estamos muy agradecidos”.

![](https://assets.3dnoticias.com.ar/alumbrado1.jpg)

**Frentes de obra**

En consonancia, Pons Estel indicó: “Las obras ya arrancaron. Primero se trabajará en todas las calles internas del barrio. Tenemos para dos semanas de trabajo, ya que luego se empieza a trabajar tanto del lado Este como del Oeste de los Monoblocks en la recuperación de la iluminación existente, sobre todo en los espacios públicos, y en cada una de las zonas internas de los edificios. Además del nuevo cableado de alimentación”.

Las tareas incluyen, además de la colocación de nuevos reflectores, trabajos de poda y despeje sobre la vegetación existente para que haya mejor apertura lumínica en cada bloque de viviendas.

Cabe recordar que el Plan de Iluminación que lleva adelante la Municipalidad se realiza paralelamente en diferentes sectores de la capital provincial. “En este momento se hacen obras muy importantes de nueva iluminación y nueva tecnología en Scarafía, Liceo Norte, Yapeyú Oeste, Ceferino Namuncurá y Santa Marta. Son obras que están iniciadas y con diferentes porcentajes de avance”.

Por otro lado, está todo dado para que inicien las tareas que se licitaron recientemente y que incluyen: 430 nuevas luces en Guadalupe Este, 360 luces y columnas en 7 Jefes y la iluminación de Fomento 9 de Julio y barrio Roma. “Se trata de obras muy importantes para el municipio, que estaban en agenda y están a punto de arrancar”, aseguró el funcionario.