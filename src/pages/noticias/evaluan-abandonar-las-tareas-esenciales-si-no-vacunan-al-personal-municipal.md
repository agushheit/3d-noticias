---
category: Agenda Ciudadana
date: 2021-05-13T09:05:10-03:00
thumbnail: https://assets.3dnoticias.com.ar/municipales.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Festram
resumen: Evalúan abandonar las tareas esenciales si no vacunan al personal municipal
title: Evalúan abandonar las tareas esenciales si no vacunan al personal municipal
entradilla: Así lo indicaron desde Festram.  "Si no se garantiza la vacunación vamos
  a analizar la paralización inmediata de las tareas", aseguraron.

---
Desde la Federación de Trabajadores Municipales de la Provincia de Santa Fe (Festram), reiteraron al gobierno provincial y a la propia obra social Iapos, instrumentar los mecanismos que garanticen la vacunación contra el Covid-19 de los trabajadores municipales y comunales que fueron declarados esenciales desde el inicio de la pandemia y que aún continúan prestando servicios en sectores de alto riesgo de contagio.

"El proceso frente al Covid-19 tiene diversas características debido a la multiplicidad de tareas que los compañeros y compañeras ejercen -además de la salud municipal- como recolectores de residuos, los inspectores y guardias en general, la atención al público y quienes en gran número realizan tareas de calle. Esto viene produciendo un contagio verificado por goteo, y debido a la vastedad del territorio santafesino y sus 365 municipios y comunas, adquiere una dimensión enorme, en donde ya hay muchas dependencias cerradas o reducidas al máximo sus posibilidades de trabajo y esto lo saben de forma directa los intendentes y presidentes comunales de toda la provincia", sostuvieron en un comunicado desde Festram.

"La desesperante información sobre la muerte de una familia completa por Covid-19, que se reprodujera en todos los medios de comunicación del país, oculta las falencias en la política de salud preventiva y los criterios de vacunación. La muerte de nuestro compañero, Carlos Schweizer, inspector de la Municipalidad de Santo Tomé y sus padres -también uno de ellos periodista y empleado del gobierno provincial- pudieron haberse evitado", continuaron informando.

La base de datos y la propia estructura del Iapos -que contiene a todos los trabajadores esenciales del sector público provincial y municipal- no fue utilizada para asegurar la vacunación de los sectores de riesgo, ni a los trabajadores y trabajadoras públicos con comorbilidades debidamente empadronados por la obra social. Hay que destacar que las ART brillan por su ausencia en la prevención y en la atención de las enfermedades laborales en esta emergencia sanitaria, embolsando fortunas en esta pandemia", indica el comunicado de Festram.

**Números de contagios de Covid-19 en municipios y comunas**

Se tomó una muestra testigo que incluyó a 20 Sindicatos de los 43 adheridos a Festram, con un universo de 27 mil trabajadores, de un total de 45.000 en toda la provincia. Los empleados municipales y comunales contagiados se incrementaron en un 350% en el período de que va desde octubre del 2020 a mayo de 2021. "Con esta encuesta se estima en más de 1.000 los trabajadores municipales que contrajeron Covid-19 en sus lugares de trabajo desde el inicio de la pandemia. Las muertes por coronavirus como consecuencia de la actividad laboral se acrecentaron en un 100% y la incidencia de contagio en el ámbito laboral es del 30%. Es decir, que de cada 10 empleados, 3 trabajadores municipales se contagian en la realización de sus tareas como personal esencial", subrayaron desde Festram.

"Ante estos hechos y hasta tanto el estado provincial y sus municipios no garanticen las vacunación a los sectores laborales en riesgo de contraer Covid-19, desde Festram se instará a la aplicación de Ley Provincial 12.913 de Comité de Salud y Seguridad para que estos evalúen los riesgo y en tal caso ordenen la paralización inmediata de las tareas", finalizaron.