---
category: Estado Real
date: 2021-09-29T06:00:41-03:00
thumbnail: https://assets.3dnoticias.com.ar/AEROPUERTO.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La provincia trabaja para consolidar el proyecto del parque comercial y logístico
  de Santa Fe
title: La provincia trabaja para consolidar el proyecto del parque comercial y logístico
  de Santa Fe
entradilla: Es un proyecto de gran relevancia ya que abarca más de 90 hectáreas aledañas
  al Aeropuerto Metropolitano de Santa Fe; se destinarán a la radicación de empresas
  comerciales proveedoras de mercaderías.

---
El secretario de Comercio Interior y Servicios, Juan Marcos Aviano, encabezó la tercera reunión del Consejo Directivo Ente Parque Comercial y Logístico de Santa Fe. Del encuentro, realizado en el Aeropuerto Metropolitano de Santa Fe, participó su presidenta, Silvana Serniotti, junto al administrador provincial del Servicio de Catastro e Información Territorial, Héctor Rodriguez, y a miembros de municipios, entidades comerciales y Cámara Mayorista de Santa Fe.

El Ente del Parque Comercial y Logístico de Santa Fe es un proyecto de gran relevancia para el Gran Santa Fe ya que abarca más de 90 hectáreas aledañas al Aeropuerto Metropolitano de Santa Fe, éstas se destinarán a la radicación de empresas comerciales proveedoras de mercaderías a nivel mayorista y con necesidades de servicios de logística.

Luego de la reunión Aviano valoró los encuentros y afirmó: “Hoy mantuvimos una nueva reunión que nos permite seguir consolidando el equipo de trabajo para lo que va a ser el futuro Parque. Articulamos con el Aeropuerto, futuros vecinos, en el loteo que comprenderá a las empresas logísticas y mayoristas del Gran Santa Fe”.

“Seguimos avanzando en todos los trámites que corresponden a este gran proyecto que es tener este Parque Comercial y Logístico en el gran Santa Fe”, agregó el funcionario provincial.

Para concluir, el secretario de Comercio Interior y Servicios detalló: “Actualmente se hacen presentes los tres municipios, el Aeropuerto Metropolitano de Santa Fe, las entidades comerciales, la sinergia público-privada. Esta articulación es la que nos encomienda permanentemente el gobernador Omar Perotti y el ministro de Producción, Ciencia y Tecnología, Daniel Costamagna y que actualmente se ve reflejada en el Parque Comercial y Logístico del Gran Santa Fe”.