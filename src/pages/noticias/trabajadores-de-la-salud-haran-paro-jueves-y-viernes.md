---
category: Agenda Ciudadana
date: 2021-06-29T09:42:19-03:00
thumbnail: https://assets.3dnoticias.com.ar/terapiajpg.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: Trabajadores de la salud harán paro jueves y viernes
title: Trabajadores de la salud harán paro jueves y viernes
entradilla: El Sindicato de trabajadores de la Sanidad convocó a un paro nacional
  en reclamo de actualización de salarios

---
El Consejo Directivo de la Federación de Asociaciones de Trabajadores de la Sanidad Argentina convocó a un paro nacional de actividades los días jueves 1 y viernes 2 de julio. La modalidad del paro sería de cuatro horas por turno, manteniendo las guardias mínimas.

Los trabajadores de Clínicas, Sanatorios, Hospitales de Comunidad, Servicios de Emergencias, Centros de Diagnóstico, Laboratorios de Análisis Clínicios, Institutos Geriátricos y Psiquiátricos agrupados en ATSA dejarán de trabajar “ante el fracaso de las negociaciones paritarias”. Desde hoy y hasta el miércoles se realizarán asambleas informativas en todos los sectores y turnos.

    https://www.facebook.com/382637465199210/photos/a.384630728333217/3861831380613117/?type=3

“No vamos a tolerar más excusas ni explicaciones”, dice el comunicado de ATSA Santa Fe publicado en Facebook, convocando al paro. “Ninguna razón puede eximir a los empresarios de la obligación de actualizar los salarios en momentos de alta inflación como los que estamos viviendo. No vamos a resignar el poder de compra de los salarios de los trabajadores”.

Y agrega: “Los empresarios se unieron e irresponsablemente se niegan a negociar. La única herramienta para vencer esa resistencia es la fuerza de la acción para poder actualizar el valor de nuestros salarios”.

    https://www.facebook.com/permalink.php?story_fbid=3861826493946939&id=382637465199210