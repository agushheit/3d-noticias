---
category: Agenda Ciudadana
date: 2021-07-09T09:44:20-03:00
thumbnail: https://assets.3dnoticias.com.ar/elecciones.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: La semana que viene se notificará a las autoridades de mesa
title: La semana que viene se notificará a las autoridades de mesa
entradilla: Son 24.576 personas para cubrir las 8.192 mesas. Por la capacitación y
  el trabajo durante el acto electoral cada uno cobrará 4.000 pesos

---
Desde la Secretaría Electoral Nacional se anunció que la semana que viene se comenzarán a enviar las notificaciones a las personas que fueron designadas autoridades de mesa para las próximas elecciones Primarias, Abiertas, Simultáneas y Obligatorias (Paso). El anuncio se realizó en una conferencia de prensa que presidió el juez federal con competencia electoral, Reinaldo Rodríguez.

Además, se brindaron algunos detalles sobre el próximo acto electoral que estará atravesado por la pandemia. La cantidad de electores habilitados es de 2.768.525 personas, de las cuales 51,44 por ciento (1.424.199) son mujeres y el 48,46 por ciento (1.344.326) son varones. Además, hay 62.898 personas que tienen entre 16 y 17 años al 14 de noviembre que estarán habilitados para votar en los comicios nacionales. El crecimiento del padrón respecto a 2019 es de 7.574 personas.

"Nadie se va a quedar sin votar, al contrario, alentamos que la gente vote", dijo Rodríguez al aclarar que las personas que estén en la puerta de los locales de votación van a ser anotados para que puedan votar. Al respecto dijo que espera que en las generales el promedio de personas que hayan concurrido a sufragar se ubique entre el 72 y el 75 por ciento, que son los niveles históricos.

Por otra parte, el juez aseguró que se está trabajando para que las personas voten en los establecimientos más cercanos a su domicilio y no se tengan que trasladar a 20 cuadras.

También participaron la conferencia de prensa la secretaria electoral nacional, María Magdalena Rodríguez; la prosecretaria electoral nacional, Mariana Langhi; el jefe del centro de informática del Poder Judicial de la Nación, Daniel Bonazzola; la coordinadora electoral del Correo Argentino, Sandra Cejas; y el jefe del comando electoral de Santa Fe, el coronel Juan Carlos Borri.

**Capacitación y designación**

Las autoridades electorales destacaron que para las autoridades de mesa existe un registro donde los interesados se pueden anotar en el sitio web www.elecoral.gob.ar. Luego de las postulaciones es la Secretaría Electoral la que evalúa si los postulantes cumplen con todos los requisitos para una posterior convocatoria, tal como se lo hizo para esta convocatoria.

En ese portal, las personas que sean designadas como presidente de mesa o vocal deberán confirmar su participación en el acto electoral o podrán excusarse si la causal está comprendida dentro de la ley.

Para estas elecciones está previsto que haya 24.576 autoridades de mesa, tres por cada una de las 8.192 mesas habilitadas. Para los presidentes de mesas y los vocales hay previsto un pago de 1.500 pesos por la capacitación y 2.500 pesos por la jornada de los comicios para cada autoridad de mesa. Además, la capacitación será virtual, a través de un sitio creado específicamente para esa cuestión. Allí habrá contenidos referidos al acto electoral y sobre la pandemia.

"Estamos generando las notificaciones para las autoridades de mesa y seguramente la semana que viene el correo comenzará a distribuirlas", dijo Bonazzola quien, además, brindó detalles sobre el sitio donde está el Registro Público de Postulantes a Autoridades de Mesa.

Ante la consulta de si las autoridades de mesa tendrán las dos dosis de las vacunas contra el Covid antes del 12 de septiembre, que es la fecha en la que se realizarán las Paso, Gutiérrez dijo: "Eso forma parte del protocolo. Nosotros convocamos ahora a las personas y no sabemos si están vacunadas. Con las personas que no estén vacunadas vamos a armar una lista que coordinarán la Cámara Electoral Nacional y el Ministerio de Salud para facilitar a la gente la vacunación y que todos tengan las dos dosis".