---
category: Agenda Ciudadana
date: 2021-02-20T06:00:34-03:00
thumbnail: https://assets.3dnoticias.com.ar/CONSEJOECONOMICO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Alberto Fernández lanzó el Consejo Económico y Social
title: Alberto Fernández lanzó el Consejo Económico y Social
entradilla: 'El presidente presentó el nuevo órgano que buscará abordar estrategias
  y políticas vinculadas con la educación, el desarrollo regional, la ciencia y la
  tecnología, el empleo y las jubilaciones. '

---
El presidente presentó el nuevo órgano que buscará abordar estrategias y políticas vinculadas con la educación, el desarrollo regional, la ciencia y la tecnología, el empleo y las jubilaciones. Estará conformado por 30 miembros surgidos del diálogo con los sectores empresariales, gremiales, académicos y de la sociedad civil.

El presidente Alberto Fernández encabezó este viernes el acto de presentación del Consejo Económico y Social con un llamado a "repensar el capitalismo" a la luz de la experiencia de la pandemia, sostuvo que “no es posible pensar una economía sin ética" y afirmó que "creer que el secreto es estar el uno contra el otro" ha dejado al país "en medio de una sociedad injusta y desigual".

En el encuentro realizado en el Centro Cultural Kirchner (CCK), el primer mandatario estuvo acompañado por el jefe de Gabinete, Santiago Cafiero; por el secretario de Asuntos Estratégicos y presidente del flamante Consejo, Gustavo Beliz, y por los integrantes de ese foro que debatirá con un plazo de 1000 días para encontrar consensos sobre cinco "misiones" prioritarias para el desarrollo nacional.

Apelando al diálogo como bandera, señaló que al país no "le fue bien” “creyendo que el secreto es dominar al otro" y reiteró la necesidad de una política que tenga raíces morales y éticas.

El Presidente afirmó también, que "estar tanto tiempo el uno contra el otro" ha dejado al país "con una sociedad profundamente injusta y desigual".

"Una sociedad que condena al 40 por ciento a vivir en la pobreza. Una sociedad con violencia de género y femicidios ¿Cuánto tiempo más debemos ver la inacción de la justicia y la policía?", se preguntó en consonancia con los asesinatos de mujeres conocidos en las últimas horas.

También hizo eje en la cuestión económica y señaló que "como dice el papa Francisco", la "cultura del descarte" ha llevado "al peor de los mundos". "Si bien es cierto que el capitalismo existe, también es cierto que debe ser repensado, a la luz de la experiencia que la pandemia nos ha marcado", completó.

En cuanto a los designados integrantes del Consejo Económico y Social, el mandatario dijo que fueron "convocados para que, de una vez por todas", se pueda construir un "país con otra lógica, la del diálogo y el encuentro", y enfatizó que "no es una mesa para ver qué interés predomina".

Antes del acto realizado en la sala principal del CCK, Fernández, Beliz y los consejeros mantuvieron una reunión privada que duró aproximadamente una hora y que, según comentó a Télam uno de los presentes, sirvió de presentación institucional.

Los integrantes del Foro se reunirán por primera vez durante la primera semana de marzo y serán asesorados por más de 100 investigadores del Conicet y por un grupo de expertos internacionales, entre ellos, Ricardo Lagos, Dilma Rousseff y José Mujica, expresidentes de Chile, Brasil y Uruguay, respectivamente; y los economistas Jeffrey Sachs, Luigino Bruni y Rebeca Grynspan.