---
category: Agenda Ciudadana
date: 2021-03-02T06:46:32-03:00
thumbnail: https://assets.3dnoticias.com.ar/vacunas.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias con información de DD
resumen: 'Otro vacunatorio VIP: detectan una lista de familiares “acomodados” e intervienen
  el hospital de Reconquista'
title: 'Otro vacunatorio VIP: detectan una lista de familiares “acomodados” e intervienen
  el hospital de Reconquista'
entradilla: El director renunció luego de que se lo exigiera el gobernador Omar Perotti,
  pero se desligó de las irregularidades. La coordinadora regional de Salud se hará
  cargo de la investigación.

---
Otro caso de vacunación VIP contra el coronavirus​, detectado en la localidad santafesina de Reconquista, derivó en la intervención del hospital regional donde se inoculó a familiares de miembros del Consejo Administrativo que no formaban parte del personal de salud.

La situación se conoció la semana pasada y el propio gobernador, Omar Perotti, reclamó la renuncia del director del centro de salud.

La coordinadora de la Regional de Salud de Reconquista, Leira Mansur, confirmó este lunes que se dispuso por decreto ponerla a cargo del hospital para trabajar con la subdirectora y miembros del Consejo de Administración en un informe que permita dilucidar el alcance de las irregularidades cometidas con la vacunación y la cantidad de personas que fueron inoculadas fuera de la población a la que se apuntaba en una primera etapa.

Además de las acciones impulsadas por el ministerio de Salud provincial, que apunta a disponer sanciones administrativas, los responsables podrían ser alcanzados por una acción penal en la Justicia.

El viernes pasado presentó su renuncia el director del hospital de Reconquista Olga Stucky, Fabián Nuzzarello. El profesional, sin embargo, dijo no ser responsable de lo sucedido porque cuando se aplicaron las vacunas VIP estaba de vacaciones y desconocía la existencia de listado paralelos.

“Cuando hay un escándalo de esta magnitud el dueño del circo termina haciéndose cargo de los payasos”, afirmó en su defensa ante la consulta del portal Reconquista Hoy. “Tuvimos miles de presiones desde el primer día de gente que se quería vacunar y nunca accedimos”, planteó Nuzzarello.

Entre los responsables del cronograma que permitió vacunar a familiares y a personal ajeno al hospital de Reconquista se apunta a tres integrantes del Consejo de Administración del nosocomio.

La investigación apunta al representante de los profesionales, el farmacéutico Gerardo Rodgers; de los no profesionales, Mabel Godoy, y de la Cooperadora, Susana Visciglio, quien además admitió ser responsable de la confección de las cuestionadas listas.

Entre los vacunados VIP se encuentran familiares de algunos miembros del Consejo de Administración.

Rodgers dijo haber recibido un llamado de Visciglio para ser anotado en la lista de los que se iban a aplicar la vacuna y que en ese contacto pidió que se incluyera a su esposa (también farmacéutica) y a dos cuñados, uno bioquímico y otro farmacéutico.

El representante del Estado en el Consejo de Administración del hospital, Andrés Ramseyer, también puso este lunes su renuncia a su disposición tras admitir que había incluido en la lista de vacunación a su esposa.

En una carta acepta que para su conducta “no hay justificativo válido, no hay excusa medianamente aceptable, por lo menos moralmente”.