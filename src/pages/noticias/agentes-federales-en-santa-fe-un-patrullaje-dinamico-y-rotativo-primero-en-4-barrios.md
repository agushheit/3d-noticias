---
category: La Ciudad
date: 2021-10-27T06:15:01-03:00
thumbnail: https://assets.3dnoticias.com.ar/GENDARMES.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'Agentes federales en Santa Fe: un patrullaje "dinámico y rotativo", primero
  en 4 barrios'
title: 'Agentes federales en Santa Fe: un patrullaje "dinámico y rotativo", primero
  en 4 barrios'
entradilla: Se planificó que los 102 efectivos que llegaron para atender la seguridad
  urbana hagan mucho trabajo “de calle”. Se entregarán 18 patrulleros nuevos y mini
  buses para el traslado del personal. 

---
Arribaron a la ciudad 102 agentes de las fuerzas federales (de Gendarmería, Prefectura y Policía Federal), y ya hay una planificación preliminar de cómo será el operativo para combatir la inseguridad.

 En principio, la idea será establecer un trabajo coordinado con efectivos de la Policía de la provincia en cuatro barrios “calientes”  y en horarios puntuales. El área noroeste de esta capital (particularmente barrio Yapeyú) será la “primera prueba”, y los despliegues se irán redirigiendo hacia otros puntos de ejido urbano.

 De acuerdo a lo que pudo averiguar El Litoral de altas fuentes de Seguridad provincial, ese trabajo coordinado (entre agentes de Nación y santafesinas) será clave. “Habrá mucho patrullaje, lo cual nos daría a nosotros un ‘aire’ para poder cubrir otras zonas de esta capital”, aseguraron.

 Con los 102 efectivos que arribaron, se llega a completar la suma de 509 agentes federales operativos en esta capital: todos para atender la seguridad ciudadana.

 “Se arrancó en cuatro barrios de la capital provincial y vamos a ir rotando, de forma dinámica”, añadieron las mismas fuentes interrogadas. Insistieron en que la intención es que “(los efectivos federales) ya mismo salgan a la calle”. Con todo, esa presencia disuasiva y preventiva en el territorio es una de las claves de operativo que buscará contrarrestar el delito.

 Pero además, este miércoles -en principio, a la hora 9-, se entregarán 18 patrulleros nuevos y mini buses para el traslado de los agentes. “Será una primera entrega, habrá más”, adelantaron las mismas fuentes de la cartera de seguridad.

 **Reunión**

Hace 15 días, el Ministro de Seguridad, Jorge Lagna, había adelantado que con la llegada de los efectivos se trabajará en “determinados barrios y avenidas”, de acuerdo a la información provista por el municipio local y por el Ministerio Público de la Acusación (MPA), respecto de las áreas de mayor conflictividad. “Se generará un comando conjunto que funcionará todas las semanas”, dijo A las declaraciones las dio tras de la reunión que mantuvo con el intendente de la ciudad, Emilio Jatón. 

 Otra de las novedades que había adelantado el funcionario provincial es que ya se terminaron los pliegos de licitación para la reapertura de la Comisaría 7ma. que está en la zona de barrio Yapeyú, también una zona azotada por la violencia. Con ello, en poco tiempo volverá a estar operativa. Y se habilitarán otros dos Centros Territoriales de Denuncias en la ciudad: la Municipalidad iba a acondicionar los espacios que se pondrán a disposición.