---
category: La Ciudad
date: 2021-03-17T07:05:24-03:00
thumbnail: https://assets.3dnoticias.com.ar/bacheo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad de Santa Fe
resumen: Trabajos de bacheo previstos para este miércoles
title: Trabajos de bacheo previstos para este miércoles
entradilla: La Municipalidad informa las calles por las que avanza el cronograma de
  tareas previsto en el Plan de Bacheo 2021. Puede haber cortes de circulación y desvíos
  de colectivos a medida que se desarrolla la obra.

---
En enero pasado, la Municipalidad puso en marcha un plan de bacheo para reparar más de 12.000 metros cuadrados de calles de la capital provincial. El programa se desarrolla por fases, priorizando las zonas de la ciudad que evidencian mayor circulación.

En la zona del micro y macrocentro, está previsto que los trabajos se desarrollen en:

* Avenida Freyre, entre bulevar Pellegrini y Obispo Gelabert, mano oeste
* San Lorenzo, entre Catamarca y bulevar Pellegrini
* En tanto, en avenidas troncales, la obra continuará por:
* Aristóbulo del Valle, entre las calles Espora y avenida Gorriti, en ambas manos

Por otra parte, en el marco de los trabajos ejecutados en la obra Conducto Pluvial Mariano Comas, operará un corte total del tránsito vehicular en:

Mariano Comas, entre San Lorenzo y avenida López y Planes. Por este motivo, se organiza el desvío de calle Mariano Comas, por San Lorenzo. El tránsito pesado, en tanto, se desviará en Urquiza hacía bulevar Pellegrini

El municipio informa que todas las tareas se realizan siempre en sentido del tránsito. Del mismo modo, indica que puede haber cortes de circulación y desvíos de colectivos que se efectúan a medida que avanza la obra y se realizan intervenciones mayores.

Por otra parte, se detalló que la realización de los trabajos está sujeta a las condiciones climáticas.