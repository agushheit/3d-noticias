---
category: La Ciudad
date: 2021-08-13T06:00:16-03:00
thumbnail: https://assets.3dnoticias.com.ar/administradores.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Se abre el Registro Municipal de Administradores de Consorcios
title: Se abre el Registro Municipal de Administradores de Consorcios
entradilla: La Municipalidad pone en marcha la Ordenanza Nº 12.603 para aquellas personas
  físicas o jurídicas que desarrollan la actividad de la administración de los consorcios
  habitacionales de propiedad horizontal.

---
La Ordenanza Nº 12.603 dispone la creación del Registro Municipal de Administradores de Consorcios de Propiedad Horizontal, estableciendo requisitos específicos por las particularidades de la actividad. El mismo pone en foco la gran incidencia de los edificios en la ciudad, para lo cual la Oficina Municipal del Inquilino desarrolló actuaciones virtuales y presenciales con el fin de dar respuesta a las inquietudes de propietarios y vecinos.

El trámite de registro debe hacerse por Oficina Virtual, donde se indica -además de toda la documentación requerida-, la base legal. Una vez realizada la inscripción, será posible la descarga de Constancia de Registro correspondiente para su exhibición.

El director de Derechos y Vinculación Ciudadana, Franco Ponce de León, manifestó: “Este registro no solo será en beneficio de aquellos que ejercen la actividad (administradores), sino también para los vecinos de la ciudad que requieren del servicio de administración (propietarios y/o locatarios), jerarquizando así la función de los administradores de consorcios y dando seguridad a la hora de abonar los montos correspondientes a expensas ordinarias como extraordinarias”.

Para mayor información, se recuerdan los canales de comunicación: presencialmente de lunes a viernes, de 7.15 a 13 horas, en Salta 2840, en la sede de Derechos y Vinculación Ciudadana; o por whatsapp al 342-5315450; o al teléfono 4574119, o por correo electrónico a derechosyvinculacionciudadana@santafeciudad.gov.ar.