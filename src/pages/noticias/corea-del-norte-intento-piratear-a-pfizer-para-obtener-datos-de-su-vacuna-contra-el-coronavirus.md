---
category: Agenda Ciudadana
date: 2021-02-17T07:12:08-03:00
thumbnail: https://assets.3dnoticias.com.ar/kim-jon.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: SL
resumen: Corea del Norte intentó piratear a Pfizer para obtener datos de su vacuna
  contra el coronavirus
title: Corea del Norte intentó piratear a Pfizer para obtener datos de su vacuna contra
  el coronavirus
entradilla: Lo denunció Seúl. Hablan de una “ciberguerra” para tener ventaja sobre
  el desarrollo del fármaco.

---
El Servicio de Inteligencia de Seúl (Corea del Sur) denunció que ciberdelincuentes norcoreanos intentaron entrar en los sistemas del gigante farmacéutico estadounidense Pfizer en busca de información sobre la vacuna contra el coronavirus.

“El Servicio Nacional de Inteligencia de Seúl nos informó que Corea del Norte trató de obtener tecnología relacionada con la vacuna y el tratamiento contra la Covid-19 utilizando la ciberguerra para hackear a Pfizer", dijo el diputado Ha Tae-keung, citado por la agencia de noticias AFP.

El país que gobierna Kim Jong-un permanece aislado del mundo desde enero del año pasado cuando cerró sus fronteras para intentar protegerse del virus que fue detectado por primera vez en China en diciembre de 2019.

El líder insistió que el país no tiene casos de coronavirus, a pesar de que los expertos extranjeros dudan de esa afirmación.

Corea del Norte solicitó vacunas al mecanismo solidario Covax y le van a mandar cerca de 2 millones de dosis, en lo que es la primera confirmación de un pedido de ayuda internacional ante la pandemia.

Covax, un mecanismo multilateral impulsado por la Organización Mundial de la Salud (OMS) para buscar una distribución equitativa de las vacunas en el mundo, entregará 1.99 millones de dosis al país comunista, según el informe de distribución provisional publicado a principios de este mes.

Ha Tae Keung explicó que Pyongyang habría tratado de "obtener tecnología relacionada con el tratamiento y la vacuna contra la COVID-19 haciendo uso de herramientas propias de la guerra cibernética para hackear Pfizer".

No es, sin embargo, la primera denuncia por ataques informáticos que recibe corea del Norte: la ONU apuntó la semana pasada que, presuntamente "usan ciberataques para actualizar su arsenal".

![](https://assets.3dnoticias.com.ar/pfiser.webp)

## **La vacuna, blanco de ataques**

La vacuna está en el centro de la polémica por ciberdelincuentes. En diciembre del año pasado, un ciberataque dirigido contra la Agencia Europea de Medicamentos (EMA) culminó de la peor manera: filtraron documentos confidenciales sobre medicamentos para la Covid-19 y la vacuna de Pfizer - BioNTech en la llamada "internet oscura" (dark web), según reveló una compañía de ciberseguridad italiana.

La Agencia había quedado inmersa en una investigación para esclarecer la brecha de seguridad que sufrió a finales del pasado año, y por el que se detectó un acceso ilegal a documentos vinculados con el Covid-19, relativos a medicamentos y vacunas de terceros.

Si bien no afectó ni al funcionamiento del organismo europeo ni a los plazos sobre la evaluación y la aprobación de vacunas y medicamentos, una fuente de la Comisión Europea reconoció que la información "fueron manipulados antes de su publicación online, para minar la confianza" en los inmunizantes.

Los archivos de la Agencia se publicaron en una entrada de blog por uno de los atacantes, que compartió mensajes de correo electrónico y nombres de funcionarios, así como capturas y documentos pertenecientes a un portal de comunicación segura reservada a personal autorizado. Estiman que al menos un centenar de cuentas accedieron a esa información.

También investigadores de seguridad de IBM dijeron que detectaron intentos de ciberespionaje dirigido para tratar de recopilar información vital sobre la iniciativa de la Organización Mundial de la Salud (OMS) para distribuir la vacuna contra el Covid-19 en países en desarrollo.

Para engañar a sus víctimas, los ciberpiratas usaron principalmente el método de spear phishing, que consiste en hacerse pasar por alguien conocido para obtener datos confidenciales y sensibles.

Especialistas estiman que estas jugadas podrían seguir ocurriendo.