---
category: La Ciudad
date: 2020-12-12T11:54:12Z
thumbnail: https://assets.3dnoticias.com.ar/circo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: 'VERANO CAPITAL: propuestas variadas para disfrutar a cielo abierto'
title: 'VERANO CAPITAL: propuestas variadas para disfrutar a cielo abierto'
entradilla: Entre hoy y el domingo, se realizarán actividades culturales y recreativas
  en la Plaza Constituyentes, el Parque Garay, el Parador Laguna Beach y calle recreativa
  en la Costanera Oeste, entre otras iniciativas.

---
**Los espectáculos son libres y gratuitos. Paralelamente, continúa la oferta de prácticas deportivas en playas y parques.**

Este fin de semana continúan las propuestas culturales y deportivas para disfrutar en espacios abiertos. Cada una de las actividades se realizará con demarcaciones que permiten un distanciamiento adecuado para poder disfrutar con los protocolos de prevención necesarios.

En tal sentido, las actividades culturales se realizarán este fin de semana en las zonas: Centro, Norte y Oeste.

<br/>

***

<span style="background-color: #CF0000; color: #ffffff; font-family: arial,helvetica; font-size: 19px; font-weight: bold;">   ZONA CENTRO  </span> **|** <span style="background-color: **#34495E**; font-family: arial,helvetica; font-size: 18px; font-weight: thin;">El viernes 11, a las 19 horas, el “**Mercado Norte sale a la Plaza**”. La propuesta se realizará en la Plaza Constituyentes y contará con la música en vivo de Melina Bruera y grupo, que continúa promocionando su nuevo simple y videoclip "Cómo eran las cosas", cover de Babasónicos.</span>

***

<span style="background-color: #CF0000; color: #ffffff; font-family: arial,helvetica; font-size: 19px; font-weight: bold;">   ZONA ESTE  </span> **|** <span style="background-color: **#34495E**; font-family: arial,helvetica; font-size: 18px; font-weight: thin;">El sábado 12, a las 19, en el **Parador Laguna Beach** (Almirante Brown 5294) se podrá disfrutar de la música en vivo de Cortamambo.</span>

***

<span style="background-color: #CF0000; color: #ffffff; font-family: arial,helvetica; font-size: 19px; font-weight: bold;">   ZONA OESTE  </span> **|** <span style="background-color: **#34495E**; font-family: arial,helvetica; font-size: 18px; font-weight: thin;">El domingo 13, a las 17.30 en el **Parque Garay**, artistas del pequeño circo ambulante "Circo del Litoral", junto a artistas del Circo Unión, recorrerán el parque con números circenses para entretener a toda la familia.</span>

***

<br/>

# **Más opciones**

Paralelamente, se realizan otras actividades con el apoyo de la Municipalidad. En ese sentido, la Feria de Artesanos del Sol y la Luna funcionará el sábado y domingo, a partir de las 15, en la Plaza Pueyrredón. Es obligatorio concurrir con uso de tapabocas y mantener el distanciamiento social.

Asimismo, en el ciclo "Atardeceres en Casa tomada" que se realiza con el apoyo de la Municipalidad, se presentará el dúo integrado por Seba Barrionuevo & Plinky Baima. Juntos recorrerán desde el son cubano a otras músicas latinas. Se presentan el domingo, en la misma noche a las 20.30 y a las 22, en los dos turnos que el **restó de Villa California** (Calle del Sol 2635, San José del Rincón) organiza para garantizar las medidas de distanciamiento y cuidado del público.

<br/>

# **Calle recreativa**

Este sábado, la Costanera Oeste se transforma en calle recreativa. La propuesta organizada por la Municipalidad se realizará entre las 8 y las 12 horas, desde el Monumento al Brigadier López hasta el Faro. Fines de semana posteriores se peatonalizarán otras calles.

**Calle Recreativa** es una iniciativa que, a través del trabajo conjunto entre el municipio y distintas organizaciones, pretende facilitar la circulación y promover la utilización masiva de la bicicleta como medio de movilidad amigable con el medio ambiente, incentivando la participación popular, la relación entre vecinos y, además, la apropiación del espacio público por parte de los ciudadanos.

<br/>

# **Deportes al sol**

Las actividades deportivas continúan durante este fin de semana en la capital santafesina en seis espacios a cielo abierto.

**Se trata de las sedes ubicadas en las playas de la Costanera Este, la del Centro de Deportes de la Costanera Oeste, las del Espigón I y II y los Parques Sur y Garay.**

El fin de semana, de 10 a 12 y de 17 a 20 horas, se podrán realizar las siguientes actividades: juegos recreativos, beach vóley, beach rugby, beach handball, beach fútbol, beach tenis, funcional y ritmos.

Cabe señalar que, además de los 90 guardavidas que supervisarán las playas, **la Municipalidad dispuso la presencia de más de 100 profesores de Educación Física que estarán a cargo de las actividades deportivas y recreativas**.

<br/>