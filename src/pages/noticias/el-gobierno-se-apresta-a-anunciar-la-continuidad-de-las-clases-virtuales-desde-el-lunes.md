---
category: Agenda Ciudadana
date: 2021-05-29T06:00:20-03:00
thumbnail: https://assets.3dnoticias.com.ar/clases.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: El gobierno se apresta a anunciar la continuidad de las clases virtuales
  desde el lunes
title: El gobierno se apresta a anunciar la continuidad de las clases virtuales desde
  el lunes
entradilla: 'La permanencia en "alarma sanitaria" de los departamentos La Capital,
  Rosario y San Lorenzo sería determinante para que las clases presenciales no vuelvan. '

---
El gobierno de la provincia se apresta a anunciar cómo será el esquema de restricciones y actividades habilitadas a partir del lunes y en la capital santafesina se da por hecho que entre las decisiones se destaca la referida a la continuidad de las clases virtuales.

Se aguarda que la Casa Gris comunique esta resolución.

Según publica La Capital de Rosario, el gobierno a cargo de Omar Perotti se dispone a retroceder a la vigencia del decreto que regía en la provincia hasta que el presidente de la Nación, Alberto Fernández, anunció un confinamiento más estricto para tratar de contener la segunda ola de contagios y muertes por coronavirus y atenuar el tremendo impacto que eso provoca sobre el sistema sanitario, que está colapsado en toda la provincia.

En ese sentido, lo que vaya a decidir y comunicar la administración provincial depende en gran medida de lo que decida el gobierno nacional en torno a la continuidad o no de la vigencia del confinamiento.

Sobre ese punto también se esperan definiciones para la tardecita-noche de este viernes, aunque hasta el momento no hubo adelanto sobre una eventual aparición pública de Fernández para comunicar cómo será el próximo esquema en el marco de la pandemia del Covid-19

Desde la Casa Gris anticiparon que no hay que esperar un discurso de Perotti para anunciar cuáles serán las actividades habilitadas y cuáles no a partir del lunes.

Al parecer, el esquema volvería a comunicarse a través de placas que el gobierno provincial distribuirá a través de su página web, sus redes sociales y de la prensa. La última vez, cuando anunció medidas un par de días antes de que el gobierno nacional dispusiera un confinamiento más estricto, esa comunicación resultó un tanto confusa y requirió de aclaraciones por parte de funcionarios para que los santafesinos la entendieran.

Respecto a la continuidad o no de las clases, los voceros consultados por La Capital dijeron que todavía no hay una definición de Perotti aunque anticiparon que muy probablemente sigan dictándose de manera virtual, como ocurrió esta semana.

Pese a las dudas, lo que ya parece un hecho es que en los departamentos Rosario, San Lorenzo y La Capital la posibilidad de que regresen las clases presenciales son nulas.