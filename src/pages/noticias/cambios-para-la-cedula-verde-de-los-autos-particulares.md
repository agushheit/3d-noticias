---
category: Agenda Ciudadana
date: 2021-03-21T08:15:52-03:00
thumbnail: https://assets.3dnoticias.com.ar/cedula.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Cambios para la "cédula verde" de los autos particulares
title: Cambios para la "cédula verde" de los autos particulares
entradilla: La Dirección Nacional de los Registros de la Propiedad Automotor y Créditos
  Prendarios (DNRPA) decidió derogar una disposición dictada por Cambiemos que estipulaba
  en tres años la vigencia del permiso.

---
Desde el 5 de abril próximo, las Cédulas de Identificación de los Automotores y Motos, conocidas como “Cédulas Verdes”, tendrán una vigencia de un año corrido a partir de su expedición y no de tres como sucede actualmente, dispuso el Gobierno.

Esta medida no regirá cuando esa cédula se encuentre en poder del titular registral del automotor o cuando se trate de automotores destinados y debidamente habilitados por la autoridad competente al uso taxi o remís, al servicio de alquiler sin conductor, al transporte de carga o de pasajeros o registrados a nombre del Estado nacional, provincial o municipal, en cuyo caso no tendrán plazo de vencimiento. Una vez vencido el plazo de validez, o dentro de los 90 días corridos anteriores al vencimiento, el titular registral podrá solicitar la expedición de una nueva cédula

La decisión no involucra a las Cédulas Azules -de autorizados a conducir- que no tienen vencimiento. Así lo estableció la Dirección Nacional de los Registros de la Propiedad Automotor y Créditos Prendarios (DNRPA) que decidió derogar una Disposición de mayo de 2019 (dictada por el Gobierno anterior) que estipulaba en tres años la vigencia de estas Cédulas, obligatorias para manejar un vehículo.

El organismo -que depende del Ministerio de Justicia- aclaró que salvo en los casos en que expresamente se determine un plazo menor (como automotores importados temporalmente), las cédulas de identificación vencerán a un año corrido desde su expedición. Una vez vencido el plazo de validez, o dentro de los 90 días corridos anteriores al vencimiento, el titular registral podrá solicitar la expedición de una nueva cédula.

La DNRPA consideró que el plazo de vigencia de tres años para este instrumento (vigente desde el 2 de mayo de 2019) deviene excesivo a la luz de la seguridad jurídica y del ordenamiento del tránsito vehicular La petición se formulará mediante el uso de Solicitud Tipo 02 (formulario manual) o TP (formulario digital) y se entregará, en el acto de retirarse la nueva Cédula, la anterior en uso para su retención por parte del Registro.

En tanto, los duplicados se extenderán con una vigencia de un año, excepto cuando normas vigentes fijen un plazo menor. La cédula en cuestión no tiene vencimiento en poder del titular registral: quien es propietario o copropietario del bien no debe realizar ningún trámite adicional a los fines de conducir su propio automotor.