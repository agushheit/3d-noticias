---
category: Estado Real
date: 2021-10-03T06:00:17-03:00
thumbnail: https://assets.3dnoticias.com.ar/PUERTOSTAFE.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: El puerto de Santa Fe ya cuenta con la habilitación del depósito fiscal general
  tipo "plazoleta de contenedores y cargas generales"
title: El puerto de Santa Fe ya cuenta con la habilitación del depósito fiscal general
  tipo "plazoleta de contenedores y cargas generales"
entradilla: 'Es en el marco del nuevo servicio de barcazas que conectará el puerto
  local con el de La Plata. '

---
El gobernador Omar Perotti participó este sábado de una reunión con la administradora general de Aduanas de AFIP, Silvia Traverso, durante la cual se brindaron precisiones sobre la habilitación del depósito Fiscal General tipo "Plazoleta de Contenedores y Cargas Generales" del Ente Administrador del Puerto de Santa Fe, trámite necesario para la puesta en funcionamiento del nuevo servicio fluvial de barcazas feeder para contenedores, que conectará la Terminal de Contenedores del Puerto Santa Fe, situada estratégicamente en el corazón de la Hidrovía (kilómetro 584 del Río Paraná), con la Terminal TecPlata S.A., ubicada en el puerto de La Plata.

El servicio mejorará la logística y la conectividad del interior del país con el Puerto de La Plata y de allí hacia el exterior; y estará destinado a empresas locales para reducir los costos logísticos de exportadores e importadores de las regiones del Centro, NEA y NOA del país.

El Puerto de Santa Fe contaba con esta habilitación por parte de AFIP, pero se perdió en 2015.

Al respecto, Omar Perotti destacó que “el puerto va tomando forma y movimiento: ya se superaron las 100 mil toneladas en el movimiento de granos. El otro objetivo del Ente era trabajar en la habilitación de la plazoleta de contenedores y para eso se requería una autorización de aduana”, recordó el gobernador de la provincia.

En este sentido, el mandatario precisó que “esto sin dudas genera expectativas de mejorar el nexo de las cargas internacionales. Será una operatoria que esperemos se consolide paso a paso, y que garantice la presencia de una barcaza, mínimo dos veces al mes, para tener la posibilidad de vincularse con el puerto de La Plata y desde allí con navieras a Brasil o a los países asiáticos”, explicó Perotti.

Y destacó que “el objetivo es dar confianza a los operadores de la región, ya que tenemos exportaciones de productos lácteos, molinería, que hoy tienen que mover en camión al puerto de Buenos Aires, con altos costos logísticos; y trabajar para que nuestras empresas tengan mejores costos, y más posibilidades de llegar al mundo con sus productos, lo que significa más trabajo para nuestra gente”, concluyó el gobernador.

Por su parte, Silvia Traverso precisó que “lo que hicimos desde Aduana Argentina es habilitar esta plazoleta para que empiecen a operar con los contenedores. Este es un trámite que nos llevó un tiempo porque es engorroso, lo logramos habilitar temporariamente, tienen 360 días para completar todo lo que falta, para que Santa Fe tenga definitivamente este depósito fiscal”.

Del encuentro también participaron los secretarios de Servicios Públicos, Carlos Maina; y de Comercio Exterior, Germán Bürcher; el presidente del Ente Administrador del Puerto de Santa Fe, Carlos Arese; el vicepresidente de la Bolsa de Comercio Santa Fe, Melchor Amor Arranz; los presidentes de UISF, Alejandro Taborda; y de Cacesfe, Marcelo Perassi; y los integrantes del directorio del Puerto Daniel Piedrabuena (representante empleados), Julio de Biasi, Ignacio Mántaras (presidente de la Sociedad Rural Santa Fe) y Javier Gutiérrez (Usuarios Puerto).

**Servicio**

El nuevo servicio consiste en una barcaza con capacidad de carga de 500 TEUS, aproximadamente, que arribará al Puerto de Santa Fe dos veces por mes, para traer y llevar contenedores de importación y exportación a y desde el Puerto La Plata; y desde allí con salida a Brasil y Asia.

El arranque de la operatoria se prevé para fines de octubre o principios de noviembre, con el envío de entre 60 y 80 contenedores vacíos al puerto de Santa Fe.