---
category: Agenda Ciudadana
date: 2021-02-24T06:50:25-03:00
thumbnail: https://assets.3dnoticias.com.ar/zanini.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de La Nación
resumen: Carlos Zannini fue vacunado contra el coronavirus y registrado como "personal
  de salud"
title: Carlos Zannini fue vacunado contra el coronavirus y registrado como "personal
  de salud"
entradilla: 'Carlos Zannini, de 66 años, recibió la primera dosis de la Sputnik V
  y fue registrado como "personal de salud" '

---
Carlos Zannini, procurador del Tesoro de la Nación y uno de los dirigentes más cercanos a Cristina Kirchner, recibió la primera dosis de la vacuna Sputnik V, según quedó asentado en los registros el Sistema Integrado de Información Sanitaria Argentino, SISA.

También fue vacunada Patricia Alsúa, la esposa de Zannini, cercana a Cristina Kirchner. Cuando ella era presidenta, Alsúa y su marido eran visitantes asiduos de la Quinta de Olivos. Participaron de cenas frecuentes y compartieron cumpleaños con el círculo íntimo de los Kirchner. La relación se mantuvo pese al paso del tiempo. El año pasado, cuando la vicepresidenta cumplió 67 años, Alsúa fue una de las invitadas a la comida que le organizó el diputado nacional Eduardo Valdés -otro de los dirigentes que fue vacunado- en el café Las Palabras, en Almagro.

Actual jefe de los abogados del Estado, Zannini fue secretario de Legal y Técnica de los gobiernos de Cristina y Néstor Kirchner. Recibió la vacuna el 22 de enero, el mismo día que el presidente Alberto Fernández. También en esa fecha se vacunaron el vocero presidencial, Juan Pablo Biondi, y el secretario general de la Presidencia, Julio Vitobello. La Casa Rosada afirmó que esos dos funcionarios fueron vacunados por recomendación de la Unidad Médica Presidente, por ser considerados parte de "la burbuja" que acompaña al jefe de Estado.

De los registros oficiales no surge dónde recibió la vacuna el procurador del Tesoro, si lo hizo en un hospital público o en alguna otra dependencia.

El nombre de Zannini trasciende en medio del escándalo que estalló el viernes pasado, cuando se conoció que en el Ministerio de Salud funcionó un vacunatorio para amigos del poder. El Presidente le pidió entonces la renuncia a Ginés González García. En los últimos días salieron a la luz lo nombres de distintos funcionarios que recibieron la Sputnik V sin estar en el orden prioritario establecido por el plan de vacunación.