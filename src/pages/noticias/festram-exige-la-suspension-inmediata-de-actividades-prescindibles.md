---
category: Agenda Ciudadana
date: 2021-05-19T09:27:19-03:00
thumbnail: https://assets.3dnoticias.com.ar/municipalidad-santafe.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Festram
resumen: Festram exige la suspensión inmediata de actividades prescindibles
title: Festram exige la suspensión inmediata de actividades prescindibles
entradilla: Sin vacunación y con tareas no indispensables, los gobiernos locales aportan
  al inminente colapso sanitario

---
La Federación de Sindicatos de Trabajadores Municipales (Festram) –que viene reclamando la vacunación para los trabajadores expuestos a riesgo de contagio por Covid– hoy exige también la suspensión de actividades que no resulten imprescindibles en el marco del colapso sanitario que se vive en gran parte de los departamentos de la Provincia.

El gremio advierte la existencia de dependencias municipales y comunales cerradas por contacto directo con Covid y destaca que las “burbujas” armadas para rotar la atención de servicios públicos están peligrosamente superadas.

Festram afirma que la situación sanitaria en las distintas localidades de la Provincia es extremadamente crítica, ya que la alta incidencia de casos en todos los Departamentos, muestran un comportamiento sanitario verdaderamente alarmante. Por otra parte, la aparición de nuevas cepas u otros factores marcan un aumento en la agresividad del Covid, ampliando su ataque a todos los sectores etarios.

Del colapso sanitario se pasó a un nivel de catástrofe en la atención de la salud. El 80% de la Salud Pública en el aglomerado del Gran Rosario es Municipal y ese sector está en absoluta indefensión. Tal como advirtió, oportunamente, Festram en el reclamo al Ministerio de Salud, el colapso ya no es una advertencia, sino una realidad puesta de manifiesto por todas y todos los trabajadores de Salud que en Asambleas piden desesperadamente la incorporación de mayor cantidad de personal para reemplazar a quienes están quebrados en la capacidad operativa. Por eso exigimos que se dé respuesta inmediata al reclamo del Sindicato de Trabajadores Municipales de Rosario.

Está claro que hasta la completa inmunización comunitaria mediante la vacuna, es imprescindible la urgente disminución de la circulación de las personas para frenar los contagios, e instar al cuidado colectivo e individual de la población.

Por lo expuesto reclamamos a los Intendentes y Presidentes Comunales el estricto cumplimiento de las disposiciones sanitarias dispuestas por el Gobierno Nacional ajustando estas políticas hacia dentro de los Municipios, reduciendo la actividad laboral no imprescindible en el ámbito de las Administraciones Locales, hasta tanto se produzca la vacunación del Personal Municipal y Comunal, generando una inmediata paralización de tareas no prioritarias como aporte a la reducción de la circulación y actividades no vinculadas a la lucha contra el Covid.

Asimismo, Festram informa que la próxima semana, se llevará a cabo por plataforma virtual un Plenario de Secretarios Generales para evaluar la situación y resolver las medidas a seguir.