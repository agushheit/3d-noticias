---
category: La Ciudad
date: 2021-05-25T08:32:30-03:00
thumbnail: https://assets.3dnoticias.com.ar/mitre.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad de Santa Fe
resumen: 'Estación Mitre: cómo es el trabajo de restauración del emblemático edificio'
title: 'Estación Mitre: cómo es el trabajo de restauración del emblemático edificio'
entradilla: "La Municipalidad avanza en la reparación del espacio histórico. Los trabajos
  cuentan con un avance del 20 por ciento. \n"

---
Las capas sobre capas de pintura que se amontonan sobre las paredes de la ex Estación Mitre son una muestra de la acumulación de los trabajos mal hechos a lo largo del tiempo. Desde hace un mes, la Municipalidad de Santa Fe inició la puesta en valor de un edificio que en sus años de esplendor fue la puerta de acceso a la ciudad.

Javier Mendiondo, secretario de Desarrollo Urbano de la Municipalidad, se refirió a las obras que ya cuentan con un avance del 20 por ciento y que se realizan en el espacio emplazado al ingreso del barrio San Lorenzo. “Estamos avanzando en un proceso muy esperado por toda la zona suroeste de la ciudad que es la recuperación de uno de los edificios emblemáticos patrimoniales que tiene Santa Fe y que estaba a la espera de su reacondicionamiento”, dijo el funcionario.

Mendiondo agregó que “este proceso se inició en el marco del Plan Integrar, por el cual estamos trabajando para equilibrar la ciudad; y encontramos que en todo el sector Oeste hay distintos edificios que requieren atención y mantenimiento, como este del Mitre. El 6 de diciembre, en el marco de la red de instituciones del barrio, el intendente Emilio Jatón presentó el proyecto integral que tenía en su primera etapa, la puesta en valor de este edificio”.

“Este frente urbano del Oeste que arranca en el Parque de la Constitución y termina en Camino Viejo encuentra en el edificio de la estación Mitre uno de los puntos estratégicos porque sabemos que es la puerta entrada a barrios importantes como San Lorenzo, Chalet, El Arenal y también la vinculación con Santa Rosa de Lima”, explicó Mendiondo, quien destacó que -por su relevancia patrimonial- el trabajo lo llevan adelante empresas y especialistas con la capacidad necesaria para realizar las tareas de restauración.

![](https://assets.3dnoticias.com.ar/mitre1.jpg)

**De oficio, restaurador**

Luciano Hernández, nacido y criado en Santa Fe, restauró 1.013 edificios a lo largo de todo el país. La Estación Mitre es otro de los edificios con historia que pondrá en valor: “Esta restauración ha sido largamente esperada por la comunidad y por el barrio donde se implanta”, indicó, y luego de observar el estado del inmueble añadió: “A través de varias gestiones se han querido hacer distintos intentos y ahora, con la actual administración de la Municipalidad, se está logrando poner en valor la fachada de un edificio emblemático, histórico para la ciudad”.

El trabajo que realizan sobre la fachada del edificio las personas que están a su cargo es minucioso. Los detalles son esenciales. “Lo primero que se hace es retirar las capas de pintura existentes, observamos pinturas a la cal de varios colores que se han ido superponiendo durante varios años. Luego se corrigen las molduras, las imperfecciones y se completa el sustrato”, repasa Hernández y agrega que, posteriormente, “se procede a un velado completo de la superficie para restituirle, con la misma textura y materialidad, lo que fue originalmente en su época de esplendor”.

**Un edificio con historia**

En el inventario del Patrimonio Arquitectónico de Santa Fe, la obra de la Mitre figura con su nombre original: Estación del Ferrocarril Central Argentino. Se proyectó en 1889 y se construyó dos años después, en 1891.

Mendiondo indicó que Santa Fe es una ciudad con una historia de casi 450 años y “eso significa que tiene diferentes capas de sus momentos históricos plasmadas en sus edificios, en su arquitectura. En este caso, se trata de un edificio de finales del siglo XIX con una impronta del ferrocarril, y del rol que tenía la ciudad en ese momento, dentro de un sistema económico y poblacional que la transformaba en un punto estratégico del interior del país”.

El funcionario explicó que se convocó a empresas con especialistas y artesanos que tienen la experiencia adecuada para llevar adelante esta tarea por haber trabajado en todo el país en edificios de características similares. “Primero, hicieron un diagnóstico y luego evaluaron los distintos materiales que se usaron originalmente. Ahora están volviendo a darle con técnicas actuales una puesta en valor de aquellas tecnologías que se usaron a finales del siglo XIX”, resaltó.

Por su parte, Hernández dijo que “a través de un trabajo que se llama veladura lo que se logra es volver la fachada a su estado original. No nos olvidemos de que esta es una construcción industrial ferroviaria que tenía como premisa ser una estación cabecera y tiene la particularidad, que no tienen otras estaciones, que el andén está perpendicular al eje de simetría del edificio”.

Cabe señalar, que desde hace 44 años, Hernández restauró obras emblemáticas a nivel nacional. En ese sentido, entre sus trabajos figura la puesta en valor del Congreso de la Nación, la Basílica de Luján, un edificio que es un símbolo arquitectónico de Buenos Aires: el Kavanagh, declarado Patrimonio Mundial por la UNESCO, en 1999, el Ministerio de Desarrollo Social de la Nación, entre otras obras patrimoniales.

En tanto, en su ciudad natal puso en valor la Casa de la Cultura, el Mercado Progreso, el Rectorado de la Universidad Nacional del Litoral, por mencionar algunos trabajos.

**Revitalizar la zona**

Con respecto a lo que implica poner en valor un edificio que está en malas condiciones, Hernández señala que “es sumamente positivo reavivar cualquier edificio muerto, que no tenga uso. Y lo convierte en un espacio de permanente actividad. Eso le hace bien no solo al edificio, sino también a su entorno y al nuevo uso que se va a implementar”.

“Yo creo que la decisión que tomó el intendente Emilio Jatón es importante porque no solo pone en valor la obra, sino que también genera nuevas actividades en un edificio recuperado”, destaca Hernández. Cabe recordar, que la intervención incluye todo el entorno de la Estación.

Las obras iniciaron a comienzos de marzo y se extenderán, en esta primera etapa, por ocho meses. Se trata de uno de los edificios más emblemáticos de la ciudad y que, durante muchos años, estuvo abandonado. A ese deterioro se suman los daños que sufrió durante un importante incendio a mediados de 2019.

**Obra integral**

Mendiondo indicó, asimismo, que “esta primera etapa consiste en la puesta en valor del edificio empleando técnicas actuales sobre cómo intervenir el patrimonio sin dejar de lado que estamos en presencia de un edificio centenario, que tiene que tener una nueva vida”. Y mencionó que “esta intervención sólo puede ser realidad gracias al diálogo con la asociación El Birri -que es un espacio cultural que viene ofreciendo numerosas actividades hace tiempo-, así como con las vecinales de la zona, con las instituciones y los propios vecinos del suroeste de la ciudad”.

El proyecto de intervención integral de recuperación del Predio Mitre -que insumirá aproximadamente $ 120 millones en total- contempla el inmueble, el parque, la feria y todo el entorno del edificio ferroviario.

En esta primera etapa, se prevé la reparación y restauración de la fachada principal, ejecución de aberturas y rejas nuevas y reemplazo de parte de la cubierta metálica en el hall del edificio y en el andén contra el edificio. También se realizará la adecuación de desagües pluviales y cloacales; nuevas instalaciones eléctricas; nuevos sanitarios públicos y reparación de los baños existentes; y recuperación de los andenes para uso público.

Con respecto al financiamiento de los trabajos, Mendiondo señaló que se realizará con el dinero que ingresó por el pago de una multa por la demolición de un edificio patrimonial. “Durante 2020 trabajamos con la Comisión de Patrimonio de la ciudad, que está integrada por representantes de las universidades, de los colegios profesionales y del Concejo Municipal, para definir que los fondos de la sanción se destinen a la recuperación de la Estación Mitre”, dijo el funcionario, y aclaró que el dinero no podía destinarse a reconstruir el inmueble demolido porque implicaría “un falso histórico”, ya que no se cuentan con las técnicas ni los materiales de construcción de la época.

![](https://assets.3dnoticias.com.ar/mitre2.jpg)