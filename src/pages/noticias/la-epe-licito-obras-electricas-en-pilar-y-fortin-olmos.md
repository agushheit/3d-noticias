---
category: Estado Real
date: 2021-01-12T09:22:48Z
thumbnail: https://assets.3dnoticias.com.ar/12121-licitacion-EPE.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: La EPE licitó obras eléctricas en Pilar y Fortín Olmos
title: La EPE licitó obras eléctricas en Pilar y Fortín Olmos
entradilla: El Directorio  abrió este lunes, en su sede central de la ciudad de Santa
  Fe, en formato virtual, los sobres correspondientes a las licitaciones públicas
  3509 y 3510, para la construcción de nuevas obras eléctricas.

---
La inversión supera los 32 millones de pesos y beneficiará a usuarios de las localidades de Pilar, en el departamento Las Colonias; y Fortín Olmos, en Vera.

Para la zona urbana de Pilar, la EPE gestiona con un presupuesto oficial de $ 14.675.992,30, la remodelación de aproximadamente 14,6 kilómetros de tendido convencional de baja tensión, reemplazado los conductores existentes por nuevo tendido preensamblado, con un plazo de ejecución de las tareas de 360 días.

En la oportunidad, presentaron ofertas las firmas SEI Servicios SA $ 18.265.952,79, Bauza Ingeniería SRL $ 20.337.313,38, MEM Ingeniería SA $ 22.393.101,33 y Base Ingeniería SRL $ 23.027.885,62, cotizaciones que serán evaluadas por una comisión inter gerencial, que sugerirá la empresa adjudicataria de los trabajos.

En otro acto se conocieron las propuestas técnicas y económicas en el marco de la licitación pública 3510, con un presupuesto oficial de $ 17.838.257,97 para la construcción de nuevos tendidos de media tensión y puestos de transformación para productores rurales del departamento Vera.

La obra tiene por finalidad realizar suministros a productores rurales en el Paraje rural Km 70, ubicado al sur de la Localidad Fortín Olmos, y para ello resulta necesario remodelar aproximadamente 7.900 metros de líneas de media tensión. Además, se repondrá 2800 metros de conductor neutro en una línea existente y el reemplazo de estructuras en mal estado, por soportes nuevos o cualquier otro material defectuoso a fin de brindar un suministro de calidad.

Los trabajos comprenden el izado de 12.000 metros de líneas de media tensión y se contempla la construcción de 16 puestos de transformación de 10 kVA de potencia para brindar suministro a los productores rurales.

En la oportunidad, la empresa MEM Ingeniería SRL presentó  una oferta de $ 18.620.375,84.

La ministra de Infraestructura, Servicios Públicos y Hábitat, Silvina Frana, afirmó: «cuando hablamos de igualdad de oportunidades nos referimos a llegar a cada rincón de la provincia con este tipo de obras eléctricas que verdaderamente necesita la gente y, así, contrarrestar los desequilibrios territoriales. Estas obras no solo beneficiarán a muchos vecinos, sino que además fortalecerán la Santa Fe que queremos, con esquemas de desarrollo productivo y generación de empleo en tiempos muy difíciles».

Por su parte, el titular de la EPE, Mauricio Caussi, señaló que «el objetivo de estas obras es mejorar el abastecimiento de electricidad, incrementando la confiabilidad del sistema de energía en estas regiones de los departamentos Las Colonias y Vera. Estas gestiones, acotó Caussi, solucionarán algunos de los inconvenientes planteados por las autoridades comunales de Fortín Olmos en la reunión realizada en diciembre pasado».

Participaron de la licitación 3510, además, el presidente comunal de Fortín Olmos, Rodrigo Acevedo; la titular de la Comuna de Garabato, Belkis Beatriz Villalba; y de Tartagal, Ramón Ledesma.

<br/>

## **Fondo de electrificación rural**

La Ley Nº 13.414 creó el Fondo de Electrificación Rural (FER) con el objeto de dotar del servicio eléctrico al sector rural, en particular de las regiones y pobladores más aislados del territorio santafesino, «que sean declaradas de electrificación rural obligatoria y en las que exista un estado de necesidad de infraestructura eléctrica», sean pobladores rurales con o sin explotaciones agropecuarias, plantas industriales radicadas en la zona rural y centros urbanos con menos de 3000 habitantes.