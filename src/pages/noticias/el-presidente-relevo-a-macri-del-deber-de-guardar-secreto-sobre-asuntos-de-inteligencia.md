---
category: Agenda Ciudadana
date: 2021-10-29T06:00:05-03:00
thumbnail: https://assets.3dnoticias.com.ar/ALBERTAAAA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: El Presidente relevó a Macri del deber de guardar secreto sobre asuntos de
  inteligencia
title: El Presidente relevó a Macri del deber de guardar secreto sobre asuntos de
  inteligencia
entradilla: La medida es un paso necesario para que el exjefe de Estado brinde declaración
  indagatoria en la causa por presunto espionaje ilegal a familiares de víctimas del
  ARA San Juan, confirmaron fuentes oficiales.

---
El presidente Alberto Fernández firmó este jueves por la noche el decreto para relevar al exmandatario Mauricio Macri del deber de guardar secreto sobre asuntos de inteligencia, un paso necesario para que el exjefe de Estado brinde declaración indagatoria en la causa por presunto espionaje ilegal a familiares de víctimas del ARA San Juan, informaron fuentes oficiales.  
  
A través del decreto 750, al que tuvo acceso Télam, el mandatario relevó a Macri de la "obligación de guardar secreto y confidencialidad, en el marco de la causa No 8559/20 caratulada 'Iuspa Benítez, Nicolás y otros s/Averiguación de delito querellante: Mereles, Andrea Alejandra y otros'".  
  
El jefe de Estado tomó esa determinación "visto el requerimiento formulado por el Juez a cargo del Juzgado Federal de Primera Instancia de la Ciudad de Dolores, Provincia de Buenos Aires", en referencia al magistrado Martín Bava, "en el marco del legajo reservado FMP No 8559/2020/3 correspondiente a la causa No 8559/20 y lo dispuesto en el artículo 16 de la Ley N° 25.520, su modificatoria Ley N° 27.126".  
  
En los considerandos, Fernández consignó que relevó al expresidente por su condición de "imputado de un delito de acción pública, y con el fin de posibilitar el pleno ejercicio del derecho de defensa del imputado, así como la adecuada y efectiva actuación de los órganos jurisdiccionales".  
  
"Corresponde relevar al imputado del deber de secreto y confidencialidad que establece la normativa vigente", completó Fernández.  
  
La indagatoria a Macri por supuesto espionaje ilegal a los familiares de los tripulantes del submarino Ara San Juan fue suspendida este jueves por la mañana a pedido de su abogado, quien consideró necesario que se le levantara a su cliente el deber de guardar secreto sobre asuntos de inteligencia.  
  
La decisión de suspender la audiencia fue tomada por el juez Bava, luego de recibir el pedido verbal del abogado Pablo Lanusse y tras solicitar la opinión del fiscal Juan Pablo Curi, que fue favorable al planteo del exmandatario.