---
category: Estado Real
date: 2021-02-02T06:45:13-03:00
thumbnail: https://assets.3dnoticias.com.ar/api-santa-fe-www.ignacioonline.com_.ar_.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Régimen Simplificado. Novedades para inscriptos
title: Régimen Simplificado con beneficios para inscriptos
entradilla: 'El Pago Total Anual 2021 llega con el beneficio del descuento de dos
  cuotas '

---
Los contribuyentes del Impuesto sobre los Ingresos Brutos adheridos al Régimen Simplificado hasta el 31 de Marzo de 2021 solo contarán con la opción de generar el Pago Total Anual 2021 con el beneficio del descuento de dos cuotas mensuales (ART. 14 BIS - LEY IMPOSITIVA (t.o. 1997 y modificatorias)

A su vencimiento, se habilitarán la generación de las cuotas mensuales correspondiente al período Abril / Diciembre 2021.

Asimismo, aquellos contribuyentes que cuenten con Saldo a Favor -por retenciones y/o percepciones o pago de los períodos eximidos 09 a 12/2020-  podrán aplicar el mismo al Pago Total Anual, para lo cual tendrán que ingresar al trámite: Impuesto sobre los Ingresos Brutos: Régimen Simplificado - Gestión de Saldos a Favor  disponible en el sitio [www.santafe.gov.ar/api]() .