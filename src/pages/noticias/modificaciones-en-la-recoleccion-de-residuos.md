---
category: La Ciudad
date: 2021-05-08T06:15:28-03:00
thumbnail: https://assets.3dnoticias.com.ar/reciclado.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Modificaciones en la recolección de residuos
title: " Modificaciones en la recolección de residuos"
entradilla: 'Los días para el sacado de basura domiciliaria húmeda e inorgánica se
  mantienen, pero este esquema coexiste ahora con otro: en 12 barrios, un camión pasa
  a contraturno para el retiro exclusivo de materiales secos. '

---
En la ciudad de Santa Fe, el cronograma regular de días para el retiro de residuos domiciliarios húmedos (domingo, martes, miércoles y viernes) y secos (lunes y jueves) se mantiene igual. Pero ahora este criterio coexiste con un sistema mixto, que se empezó a implementar tras la declaración del Aspo, cuando se creía que el contacto con superficies contaminadas del nuevo coronavirus era una alta fuente de contagio, y se podía poner en riesgo a recolectores y recicladores.

En el recorrido extra de un camión recolector que sale en contraturno de los recorridos ya habituales por 12 barrios de la ciudad, y que retira exclusivamente residuos secos (nada de húmedos, ni ramas, ni montículos de tierra), y que los lleva directo a la Planta de Tratamiento del Complejo Ambiental.

También se encuentran a disposición las "islas de separación", que están en una quincena de puntos de la ciudad y en las que pueden depositarse, también, sólo secos reciclables (ver Eco Estaciones de reciclado).

"No hay un cambio en los días que ya existían para la diferenciación", deja en claro el secretario de Ambiente municipal, Edgardo Seguro.

"En el sistema anterior (de lunes y jueves), las personas separaban residuos en origen, pero las empresas recolectoras tienen prohibido por ordenanza clasificar en la vereda: no pueden los recolectores separar aquellos residuos húmedos orgánicos e inorgánicos. Se llevan todo. No había ni hay una recolección diferenciada, sí 'diferente' -digamos-, porque sólo un grupo de vecinos, en esos días sacaba residuos secos", añadió el funcionario.

En este sistema mixto, el camión de contraturno sólo puede retirar residuos secos reciclables. Levanta las bolsas que los vecinos hayan dejado con material seco. "Esto hace que el sistema sea realmente diferenciado, ahora sí", subrayó Seguro.

**Los barrios**

El cronograma de recolección a contraturno sería de la siguiente manera: lunes, Fomento 9 de Julio y Jardín Mayoraz en turno matutino, y Colastiné Norte (a partir de las 14); martes, María Selva I, Alvear y Sargento Cabral (turno matutino); miércoles, María Selva II, Sargento Cabral y San Roque en turno matutino; Candioti Sur (turno vespertino); jueves, en Colastiné Norte en Sector Este (vespertino), y viernes, Candioti Norte (turno vespertino).

**Eco Estaciones de reciclado**

En otros sectores de la ciudad donde no se implementa este sistema mixto, se establecieron "islas de separación". Son las llamadas "Eco estaciones de reciclado". Allí, los vecinos pueden ir con sus bolsas de residuos secos y dejarlas. Todo ese material va directamente a la Asociación Dignidad y Vida Sana.

Estas Eco Estaciones están encuentran en el Palacio Municipal; en el Instituto Municipal de Salud Animal (Jardín Botánico); Instituto Municipal de Salud Animal (Parque Garay); Distrito Este; Distrito Noreste ; Distrito La Costa; Estación Las Paltas (Colastiné); Estación Belgrano; Costanera Este; Costanera Oeste; Shopping Puerto Plaza; Mercado Norte, y ACAV (Aristóbulo del Valle 6726).

_Con información de El Litoral_