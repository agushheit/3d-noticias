---
category: Agenda Ciudadana
date: 2021-02-13T07:16:42-03:00
thumbnail: https://assets.3dnoticias.com.ar/controlesvialesjpeg.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Provincia Incrementa Los Operativos De Control Para Prevenir Siniestros
  Viales Durante El Feriado Largo De Carnaval
title: La Provincia Incrementa Los Operativos De Control Para Prevenir Siniestros
  Viales Durante El Feriado Largo De Carnaval
entradilla: La APSV implementó a partir de este viernes más de 80 puestos de control
  en rutas de la provincia por el aumento de los desplazamientos vehiculares.

---
La Agencia Provincial de Seguridad Vial (PSV), que depende del Ministerio de Seguridad, reforzó este viernes la prevención en rutas de la provincia con mayor cantidad de puntos de control y personal con el objetivo de prevenir situaciones de riesgo y proteger la circulación de las familias que se moverán durante estos cuatro días de feriado. También, se difundirán recomendaciones para un viaje seguro en las distintas redes sociales de la Agencia y de Gobierno.

**Más control**

Desde la tarde del viernes hasta la noche del martes se dispuso la instalación de 87 puestos de control en todas las unidades regionales de la provincia, fundamentalmente en zonas de quintas y casas de fin de semana, tanto de región norte como sur, y en los accesos a rutas que llevan a sitios turísticos.

“Vamos a estar presentes para monitorear que la circulación sea ordenada, pero sobre todo para detectar a aquellos conductores que ponen en riesgo a otros”, expresó el Subsecretario de APSV, Osvaldo Aymo. “Se espera un importante caudal de vehículos en las rutas por eso la función del control es prevenir. El consumo de alcohol está entre las conductas que más se van a fiscalizar durante estos días porque sigue habiendo muchas personas que salen a la ruta habiendo consumido alcohol y de la única manera que podemos detener esta cadena de irresponsabilidad es a través del control y la sanción”, puntualizó el funcionario.

En este sentido, el organismo provincial dispuso 87 puntos de control en los que se desempeñarán agentes de la Policía de Seguridad Vial e inspectores locales de cada jurisdicción, así como personal de la Agencia Nacional de Seguridad Vial. Dadas las características del turismo interno se priorizará también la presencia de fuerzas de seguridad en Ruta Provincial 1 y Ruta Nacional 11, que atraviesan localidades de la zona costera en la región norte de la provincia y en Ruta 9 que conecta la ciudad de Rosario con localidades aledañas, que cuentan con gran cantidad de casas de fin de semana.

**Conductas de riesgo**

Los controles en ruta permiten detectar conductas de riesgo que pueden derivar en siniestros viales. A través de ellos la APSV fiscalizará el consumo de alcohol en todos los puestos de controles las 24 horas, el uso de dispositivos de seguridad, la portación de la documentación exigible (licencia de conducir, póliza de seguro, comprobante de la RTO), la distribución y exceso de pasajeros en vehículos particulares y remises y conductores vulnerables en condiciones de riesgo como los motociclistas que circulen sin casco o más de un acompañante.

Por otro lado, la APSV reforzó la difusión de las principales recomendaciones que permitan tener un viaje seguro. El objetivo es brindar información sobre distintas situaciones que pueden presentarse durante el viaje, y cómo actuar ante cada una de ellas para evitar riesgos y aumentar la seguridad vial. Entre las recomendaciones, la APSV destaca revisar el estado mecánico general del vehículo y especialmente comprobar los frenos; programar el viaje teniendo en cuenta las personas que viajan, si hay niños o personas mayores, etc., a fin de establecer paradas y tiempos de descanso y no ingerir alcohol.

Es indispensable que todos los pasajeros lleven abrochado el cinturón de seguridad y que los niños viajen siempre atrás en los SRI (Sistemas e Retención Infantil) correspondientes a cada edad. Se deben llevar encendidas las luces bajas las 24 hs; tomar las precauciones de distancia y visibilidad antes de realizar un adelantamiento; respetar los límites de velocidad reglamentarios y no consumir alcohol antes ni durante la conducción.

La documentación reglamentaria que se debe portar es la siguiente:

\-Licencia de conducir vigente

\-Cédula única de identificación del automotor

\-Póliza de seguro en vigencia

\-Ultima patente del año en curso paga

\-DNI

\-RTO

\-Si el vehículo funciona con GNC, se debe contar con autorización para utilizar este tipo de combustible.

El estado del tránsito en las rutas se puede consultar a través de la información que difunde la APSV en sus cuentas de Facebook /seguridadvialsantafe y Twitter @redsegvial.

**Circuito vial itinerante**

La APSV continúa recorriendo localidades de zonas costeras de la provincia en el marco del Operativo Santa Fe Verano para promover pautas de movilidad segura y responsable. El espacio vial es un dispositivo inflable que visita parques, plazas y playas de distintas regiones con una propuesta lúdico educativa que permite que los niños y niñas participen de un recorrido en karting, bicicleta o a pie tomando contacto con las señales de tránsito y las principales nociones para utilizar el espacio público de una forma segura, solidaria e inclusiva.

Durante este fin de semana el espacio se instalará el sábado de 16 a 20 hs. en el Acuario de la ciudad de Rosario y en Calle Recreativa el domingo de 9 a 12. El sábado estará presente en la Plaza de los Artesanos en la localidad de Luis Palacios mientras que en Santa Fe el domingo estará presente en el Playón Deportivo de la Costanera Oeste, de 9 a 12.