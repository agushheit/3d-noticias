---
category: Agenda Ciudadana
date: 2021-11-15T06:15:37-03:00
thumbnail: https://assets.3dnoticias.com.ar/PRO.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: 'Victoria de Juntos por el Cambio a nivel nacional: el Gobierno achicó la
  diferencia pero no le alcanzó'
title: 'Victoria de Juntos por el Cambio a nivel nacional: el Gobierno achicó la diferencia
  pero no le alcanzó'
entradilla: 'El frente opositor, que ahora ganará terreno en Diputados y el Senado,
  reeditó las victorias en Buenos Aires, CABA, Córdoba, Santa Fe y Mendoza, los distritos
  de mayor peso electoral. '

---
Juntos por el Cambio se imponía este domingo en los principales distritos del país y en algunos obtenía un mejor resultado que en las PASO, elección que el Gobierno buscó revertir sin éxito, lo que le valdrá un escenario de extrema polarización en el Congreso a partir del 10 de diciembre.

El frente opositor, que ahora ganará terreno en Diputados y el Senado, reeditó las victorias en Buenos Aires, CABA, Córdoba, Santa Fe y Mendoza, los distritos de mayor peso electoral.

Por su parte, el Frente de Todos no logró su objetivo de dar vuelta resultados para lograr mantener la mayoría en la Cámara Alta y una posición privilegiada en Diputados, aunque sí pudo recortar la diferencia en Buenos Aires.

Según los resultados oficiales, también consiguió revertir las derrotas de las PASO en Chaco y Tierra del Fuego, aunque por las pocas bancas que pone en juego, no impactará en el reparto de bancas nacional.

Los búnkers de Juntos por el Cambio se mantenían en un clima de festejo, con triunfos también en Jujuy, La Pampa, Chubut, Corrientes, Entre Ríos, Misiones, San Luis y Santa Cruz.

En el comando de campaña del oficialismo, ubicado en el barrio porteño de Chacarita, se aguardaba la llegada del presidente Alberto Fernández y la vicepresidenta Cristina Kirchner ya anticipó que no acudirá.