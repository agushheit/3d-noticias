---
category: Estado Real
date: 2021-05-11T08:44:32-03:00
thumbnail: https://assets.3dnoticias.com.ar/cemafe.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: La unidad de oncología del Cemafe cumplió dos años al servicio de la ciudadanía
title: La unidad de oncología del Cemafe cumplió dos años al servicio de la ciudadanía
entradilla: "“Es un servicio que no tiene techo, crece día a día aún en épocas de
  Covid. Siempre teniendo como lupa la necesidad del paciente”, indicó el director
  de Gestión Institucional, Sebastián Calvet."

---
La Unidad de Oncología del Centro de Especialidades Médicas Ambulatorias de Santa Fe (CEMAFE) cumplió, el pasado 2 de mayo, su segundo aniversario en el efector de la capital provincial.

Al respecto, el director de Gestión Institucional del Cemafe, Sebastián Calvet afirmó: “Este equipo tuvo la posibilidad de aggiornarse a un nuevo hospital como el Cemafe cuando se inauguró, y no fue solamente el traslado sino que todo el equipo de salud de la Unidad de Oncología redobló la apuesta porque se aumentaron significativamente la cantidad de tratamientos quimioterapéuticos”.

A lo que agregó: “Es un servicio que no tiene techo, crece día a día aún en épocas de covid. Siempre teniendo como lupa la necesidad del paciente y lo mejor que hay para referenciar cómo trabaja este equipo es preguntarles a los pacientes como fueron atendidos en todo su proceso de la enfermedad”.

Por su parte, el director Ejecutivo del efector, Fabián Mendoza, expresó: “Nos sentimos orgullosos por el servicio que se brinda en el Cemafe; es una unidad que nunca ha parado, siendo responsable de que cada paciente pueda acceder al tratamiento de una patología que genera tanto malestar, no solo en la persona sino también en la familia. Es un servicio esencial y tratamos de garantizar la continuidad todo el tiempo, aún en pandemia.”

 

**UNIDAD DE ONCOLOGÍA**

Al ser consultada por el desempeño del servicio de oncología durante los dos años en Cemafe, la responsable de la Unidad de Oncología, Rosana Viroglio, mencionó: “Este es un centro que recibe la derivación de pacientes de todo el centro-norte de la provincia. El año pasado y a pesar de la pandemia, se han realizado casi 10.000 consultas en el servicio”.

“El servicio además cuenta con una línea telefónica exclusiva, 0342-4844403, donde el paciente puede pedir un turno al igual que los distintos profesionales de los distintos efectores pueden comunicarse para realizar las consultas correspondientes”, agregó.

**LA “LUPA” PUESTA EN EL PACIENTE**

La unidad de oncología cuenta con una estrategia de atención al paciente tanto en el efector como fuera del mismo. Al respecto, la coordinadora de Enfermería de la unidad funcional de oncologia, Carina Stechina, explicó que “el paciente cuando ingresa a la Unidad es atendido por un médico quién lo evalúa por primera vez a través de una biopsia, decide el tratamiento de quimioterapia y a partir de ahí, desde enfermería se le realiza una entrevista previa donde se consultan los antecedentes de la persona, es decir si sufre de diabetes, hipertensión, se le aclara alguna duda que haya quedado y muchas veces se requiere de la ayuda de una psico-oncologa ya que es una enfermedad que impacta muchísimo en la persona, y de esta forma el paciente se retira mas aliviado y entendiendo muchas cosas mas”.

“Una vez que la medicación está lista, se lo llama al paciente, lo ve el médico, pasa a la sala, donde se le hacen todos los controles siguiendo los protocolos internacionales, lo cual nos requiere una capacitación constante ya que se actualiza permanentemente, y se coloca la vía e inicia el tratamiento”, agregó.

Asimismo Stechina indicó: “Lo bueno que tiene el sistema implementado en el Cemafe es que la personas vienen, se hacen el tratamiento y se retiran. De esta forma el paciente no tiene contacto con otro tipo de patologías como infecciones o pacientes covid, sigue en contexto familiar lo cual les sirve de gran apoyo”.

Finalmente, al ser consultada sobre el trabajo durante la pandemia, la coordinadora indicó que: “Una de las estrategias durante la pandemia fue dividir las áreas. El sector dónde se realizan los hisopados estaban por un lado y el servicio de oncología por otro, lo que nos permitió no mermar el trabajo (como sí pasó en otras localidades del país). Y otra fue la metodología burbuja donde los equipos se turnaban una vez por semana para realizar la atención y así en caso de haber un compañero contagiado, la otra burbuja puede continuar con la atención normal del paciente”.

“La felicidad y la gratitud del paciente es lo que mas nos enorgullece. Aquí en el servicio implementamos una campana donde el paciente la hace sonar una vez que finaliza con el tratamiento. Es un momento hermoso que te llena de tanta alegría y nos motiva a seguir adelante día a día”, concluyó Stechina.

**EL ÁREA DE RECONSTITUCIÓN DE SITOSTÁTICOS**

El área de reconstitución de sitostáticos es el área habilitada bajo las normas legales que cumplen los requisitos de calidad necesarios para elaborar tratamientos quimioterápicos pero también se preparan otros medicamentos que son de alto riesgo y biopeligrosos, es decir que su manipulación pueden traer daños a los operadores técnicos, al personal de enfermería que administra el tratamiento, como también perjudicar al medio ambiente. Esto se realiza bajo normativas de los ministerios de Salud y el de Medio Ambiente.

Al respecto, el Farmacéutico y director técnico del sector, Marcos Araya, informó que “en principio cuando arrancamos fue todo un desafío en que el tratamiento se haga un 100% en forma ambulatoria, ya que antes era en un hospital con internación. Se incorporó toda la parte de tumores sólidos y luego con la migración del hospital Iturraspe se incorporaron los tratamientos de oncohematología, es decir tumores líquidos, cuya preparación se realiza acá, algunos se administran en Cemafe y otros en el nuevo hospital Iturraspe, coordinando previamente el traslado”.

“Esta metodología de tratamiento ambulatorio, el paciente lo recibió de buen agrado. Se lleva todo un seguimiento de la parte médica y de enfermería. Articulamos las prestaciones con la consulta oncológica dada en Cemafe con tratamientos quimioterápicos no endovenosos ya sea por vía oras o de administración intramuscular o subcutánea se puedan realizar en las localidades del interior donde reside el paciente. Es decir se articula a través de la red de farmacia de la provincia con la red de prestación médica y de enfermería, para que el paciente pueda hacer el tratamiento en un lugar más cómodo para él o ella siempre y cuando se cumplan los requisitos de calidad y seguridad en el tratamiento”, finalizó el profesional.

**ESTADÍSTICAS**

Desde el comienzo de la unidad, se recibieron 600 pacientes del centro-norte provincial. Además, en el mismo período se realizaron 5500 administraciones (tratamientos) pero de las mismas, a veces se requiere administrar al paciente dos o más drogas, entonces se contabilizan las preparaciones las cuales en estos dos años, llegaron a 10000.