---
category: La Ciudad
date: 2022-01-13T06:15:58-03:00
thumbnail: https://assets.3dnoticias.com.ar/epecablee.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Fallas en un cable subterráneo de la EPE dejaron al menos 7 barrios sin luz
title: Fallas en un cable subterráneo de la EPE dejaron al menos 7 barrios sin luz
entradilla: 'Los arreglos insumirán varias horas de trabajo. "No es posible por el
  momento dar un horario cierto de finalización de las tareas", informaron desde la
  EPE.

'

---
Este miércoles por la tarde, cerca de las 15 horas, el centro de distribución Mayoraz, que la Empresa Provincial de la Energía (EPE) posee en el norte de la ciudad, salió de servicio. Producto de la situación, desde distintos barrios de la capital comenzaron a reportar cortes de luz y bajones de tensión.

Los barrios que quedaron sin servicio producto de la falla son: Fomento 9 de Julio, Escalante, María Selva, Los Hornos, Don Bosco, Pro Adelanto Barranquitas y Belgrano. En otros barrios como Mariano Comas y barrio Candioti Norte, se registran bajones de tensión.

Según indicaron desde la EPE, "hay que detectar y reparar un cable subterráneo alimentador fallado, que une el centro de distribución Mayoraz con la Estación Transformadora Santa Fe Centro".

"Los trabajos insumirán algunas horas de trabajo y el servicio no se puede realimentar por otros vínculos, en virtud de la demanda que tienen las instalaciones", continuaron agregando desde la Empresa Provincial de la Energía.

Según manifestaron en relación a la solución del problema registrado, "durante la noche, en forma progresiva, se irá normalizando el abastecimiento de energía". "No es posible por el momento dar un horario cierto de finalización de las tareas", finalizaron.