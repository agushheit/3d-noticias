---
category: Agenda Ciudadana
date: 2021-07-03T06:00:34-03:00
thumbnail: https://assets.3dnoticias.com.ar/EXTRANJEROS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Perotti dispuso que cumplan aislamiento quienes regresan del exterior
title: Perotti dispuso que cumplan aislamiento quienes regresan del exterior
entradilla: A través de un decreto el gobernador de la provincia, Omar Perotti, dispuso
  medidas preventivas para evitar contagios de la temible variante Delta.

---
Omar Perotti dispuso por decreto que las personas que ingresen al territorio santafesino provenientes del extranjero deberán someterse a exámenes de coronavirus, tal cual lo dispuso el gobierno nacional en el marco del avance de variantes del coronavirus en distintas partes del mundo. Se obliga a los que viajan o regresan a Santa Fe a realizarse un nuevo control de Covid-19 a los siete días de efectuado el test de ingreso al país.

Este jueves a la noche se dio a conocer el Decreto Provincial Nº 1.051 que determina “con carácter precautorio, que las personas que a partir de la fecha del presente decreto –1 de julio de 2021– ingresen al territorio de la Provincia de Santa Fe por cualquier punto de la misma provenientes del extranjero, y con independencia del medio de transporte que empleen a tal fin, deberán observar en sus domicilios o lugar de residencia que denuncien, por el término de siete (7) días corridos desde su ingreso las medidas de aislamiento obligatorio a que hace referencia el artículo 7º del Decreto de Necesidad y Urgencia (DNU) 260/20, prorrogado hasta el 31 de diciembre de 2021 por su similar Nº 167/21, al que la Provincia adhiriera por Decreto Nº 0173/21)”.

De acuerdo a la normativa las personas que ingresen al territorio provenientes del extranjero deberán, a los siete días corridos de haber ingresado y haberse realizado un test diagnóstico para SARS-Cov-2, realizarse otro, previo a dar por concluida la medida de aislamiento correspondiente; lo que en ningún caso podrá verificarse sin contar en forma fehaciente con un test de resultado negativo.

En tanto, “de resultar positivo el nuevo test diagnóstico para SARS-Cov-2, el laboratorio que lo realice deberá arbitrar los recaudos para que la autoridad sanitaria nacional competente secuencie genómicamente la muestra de laboratorio y la autoridad sanitaria provincial realice el inmediato rastreo de los contactos estrechos de ese viajero, sobre la base de los mecanismos previstos para la trazabilidad de su ingreso y de traslado al lugar de aislamiento”.

El decreto provincial establece que la persona diagnosticada positiva deberá continuar en aislamiento hasta obtener el alta correspondiente.

“Con el concurso de los ministerios de Salud y Seguridad se constituirán equipos de constatación domiciliaria del cumplimiento de las restricciones dispuestas por las autoridades sanitarias con motivo de la pandemia, pudiendo requerir a tales fines la colaboración del Ministerio Público de la Acusación y de los Juzgados Comunitarios de Pequeñas Causas, allí donde estos existan; formulando las denuncias pertinentes en caso de constatar incumplimientos”, indica el tercer artículo y agrega: “Sin perjuicio de lo anterior se podrá requerir la colaboración e intervención de los organismos federales competentes, en las constataciones que se efectúen; debiendo en todos los casos notificar y dar intervención de las medidas a las municipalidades y comunas donde la persona que haya ingresado a la provincia proveniente del extranjero denuncie que va a cumplir el aislamiento social”.

Por último, señala a la Secretaría de Gestión Federal de la Gobernación como la autoridad de aplicación del régimen establecido en el decreto, estando a su cargo la coordinación de la actividad de los organismos provinciales, con las municipalidades y comunas, y con los del Gobierno Nacional competentes.