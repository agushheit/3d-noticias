---
category: Agenda Ciudadana
date: 2021-12-31T06:00:25-03:00
thumbnail: https://assets.3dnoticias.com.ar/deponti.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Un año con más derechos sexuales y reproductivos
title: Un año con más derechos sexuales y reproductivos
entradilla: 'Primer aniversario de la Ley de Interrupción Voluntaria del Embarazo,
  la opinión de Lucila De Ponti, diputada provincial del Movimiento Evita.

'

---
Estamos a un año de la sanción de la [ley](https://www.unosantafe.com.ar/ley-a33470.html) 27610. El 30 de diciembre de 2020 se legalizaba el [aborto](https://www.unosantafe.com.ar/aborto-a10984.html) en nuestro país y comenzaba una nueva etapa como corolario de las décadas de lucha del movimiento de [mujeres](https://www.unosantafe.com.ar/mujeres-a24544.html) y disidencias. Comenzaba una nueva etapa y un nuevo desafío: garantizar ese derecho que ya estaba escrito en la Ley.

A lo largo y ancho de la Argentina el cumplimiento de la Ley ha sido disímil. Muchos factores inciden en esto, la historia y la cultura de cada territorio, las capacidades institucionales, el funcionamiento de los sistemas de salud, la presencia de organizaciones feministas, la calidad educativa. Según el Ministerio de Salud durante el primer año de vigencia de la Ley se realizaron 32.758 interrupciones de embarazos en condiciones seguras a través del sistema de salud público. A su vez, la cifra de hospitales y centros médicos que realizan los procedimientos aumentaron en un 30%, son 1243 instituciones que garantizan el derecho a la información y al aborto en Argentina.

Han existido también múltiples resistencias a la aplicación de la Ley. Diferentes grupos que no están de acuerdo con el derecho presentaron 37 causas judiciales para impedir que se lleven adelante los procesos de IVE. Sumado a esto se interpuso ante la Corte Suprema un planteo de inconstitucionalidad de la norma, que por supuesto fue rechazado.

Las desigualdades entre las distintas provincias son notorias en cuanto a las políticas sanitarias de cada región. Eso significa que por un lado debemos seguir trabajando para que el gobierno nacional implemente políticas que contribuyan a la igualdad de derechos entre todas las mujeres argentinas, al mismo tiempo que se fortalece la formación de profesionales y los procedimientos de atención en cada sistema de salud provincial. El derecho a elegir y decidir debe ser una realidad por igual en cada rincón del país.

Otro aspecto fundamental es la consolidación del aborto medicamentoso a través de la producción y distribución de misoprostol, así como el acceso a la información correspondiente para un uso acompañado y responsable. En ese sentido Santa Fe ha sido pionera con la producción de misoprostol en el laboratorio público que hoy abastece a los efectores provinciales.

Un año atrás conquistamos un derecho que significó muchísimo para el movimiento feminista latinoamericano. Las mujeres argentinas abrimos un camino de derechos por el cual ingresaron otros países de la región. El desafío de acompañar la concreción de esta política nos deja un panorama desparejo pero que sin dudas muestra avances. Es momento de seguir adelante y trabajar en una agenda de políticas que garanticen mejorar la calidad de vida de cientos de personas. Vamos por las tareas de cuidado, el parto respetado y la dignidad de todas, todos y todes.