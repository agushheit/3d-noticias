---
category: La Ciudad
date: 2021-01-05T10:54:21Z
thumbnail: https://assets.3dnoticias.com.ar/municipalidad-santafe.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: El Litoral'
resumen: La Municipalidad prorroga la moratoria para tres tributos
title: La Municipalidad prorroga la moratoria para tres tributos
entradilla: Se trata del DReI, del Derecho de Concesión y Transferencia de Licencias
  de Taxis y Remises y del Registro de Habilitaciones Especiales. Los contribuyentes
  pueden acogerse a la moratoria hasta fines de febrero.

---
La Municipalidad extiende por dos meses más, la vigencia de la **moratoria para tres tributos**, dispuesta por la Ordenanza N° 12.730 (Capítulo II), la cual finalizaba en diciembre, pero que daba la posibilidad de prorrogarse por otros 60 días.

De esta manera, mediante la resolución interna 239, la Secretaría de Hacienda prorroga la posibilidad de acogerse al **plan especial de regularización de pagos, a los contribuyentes que adeuden únicamente los períodos de abril, mayo, junio, julio y agosto de 2020, de los siguientes tributos**:

a) Derecho de Registro e Inspección (DReI)

b) Derecho de Concesión y Transferencia de Licencias de Taxis y Remises

c) Registro Municipal de Habilitaciones Especiales

**Los beneficios serán del cero por ciento para los intereses resarcitorios**, tanto para el pago contado como también para la formalización de convenios de pago que pueden hacerse en hasta 12 cuotas mensuales y consecutivas sin interés de financiación.

Cabe recordar que la Ordenanza 12.730 fue un mensaje del Ejecutivo Municipal con una serie de exenciones y condonaciones de distintos tributos municipales en el marco del paquete de medidas anunciado en octubre por el intendente Emilio Jatón para morigerar el impacto económico de la pandemia en distintos sectores.