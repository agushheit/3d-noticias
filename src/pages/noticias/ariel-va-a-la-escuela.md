---
category: Agenda Ciudadana
date: 2022-05-06T14:43:21-03:00
thumbnail: https://assets.3dnoticias.com.ar/Ariel-va-a-la-escuela-1-1200x1200.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: Ariel va a la escuela
title: Ariel va a la escuela
entradilla: U n puente entre la obra del maestro Ariel Ramírez y las infancias

---
En el cierre del programa que organizó la Municipalidad para celebrar el centenario del nacimiento de Ariel Ramírez, se entregó el material didáctico “Ariel va a la escuela” y se brindó una capacitación para estudiantes del Liceo Municipal y docentes de música de nivel primario, que vuelven a sus aulas con nuevas estrategias para abordar la obra del compositor y pianista.

La Sala Ariel Ramírez recibió ayer por la mañana cerca de 100 docentes de música de la ciudad y estudiantes del profesorado de la Escuela de Música del Liceo Municipal “Antonio Fuentes del Arco”, que participaron de la presentación del programa “Ariel va a la escuela”. El encuentro comenzó con Alejandra Bontempi tocando el piano vertical E. Pitzer 2751 Leipzig que perteneció a Ariel Ramírez, mientras los docentes ingresaban a la sala y recibían los ejemplares impresos del material, que también está disponible para descarga gratuita, en la plataforma Capital Cultural, junto a otros contenidos desarrollados para acompañar el trabajo en el aula.

La actividad contó con la participaron del secretario de Educación y Cultura de la Municipalidad, Paulo Ricci; de la delegada de la Región IV del Ministerio de Educación de la provincia, Mónica Henny; y de Patricia Hein, coordinadora de Programas y Organismos Musicales de la Municipalidad y autora del material junto a Marisa Anselmo, Juan Sebastián Barbero, Leonel Franzoi y Soledad Gauna; con la colaboración de Fabiana Sinchi. 

Después del acto, se realizó una capacitación en grupos para vivenciar algunas de las actividades que se proponen, en cuatro grandes áreas: Escucha, Canto, Movimiento y Percusión, tanto corporal como instrumental. El cierre de la jornada fue con todos los docentes interpretando canciones de Ramírez, guiados por Maximiliano Maglianese y Rocío Elizalde, de la escuela de percusión corporal Latido Americano. 

Homenaje cultural y educativo

Paulo Ricci señaló que “Ariel va a la escuela” es el resultado de un año entero de trabajo, como parte del programa de las acciones organizadas por  la Municipalidad, que celebraron los 100 años del nacimiento del compositor. “Este era uno de los objetivos más importantes del proyecto y ahora lo concretamos con la entrega de los cuadernillos para que en todas las  escuelas de la ciudad de Santa Fe y en todas las escuelas públicas del Departamento La Capital, docentes de música puedan trabajar los contenidos y colaborar en el reconocimiento y la difusión de la obra de Ariel Ramírez, como uno de los más grandes compositores, músicos y educadores de la música”, dijo. 

En esa línea recordó también que la propuesta tiene como antecedente un programa educativo que el propio Ariel Ramírez creó en la década de 1980, en Buenos Aires. “Recuperamos el espíritu de aquel programa La música va a la Escuela y parte de su nombre, para reinventarlo con autores y autoras locales, docentes de música de Santa Fe, trabajando sobre una selección de un cancionero tan rico como es el de Ramírez”.

Por su parte, la delegada de la Región IV del Ministerio de Educación agradeció “la invitación que nos hizo la Municipalidad para poder sumarnos a este Programa, que pudimos articular a través de los supervisores de música para acompañar la propuesta”. Además remarcó el trabajo constante con el municipio, en el Fondo de Asistencia Educativa, que fue ampliando su rol habitual del mantenimiento de edificios a otras iniciativas como FAE Lectura.

10 canciones y una mirada interdisciplinaria

Patricia Hein contó que el proyecto surgió en el Espacio Pedagógico Musical Ariel Ramírez, que conformaron instituciones educativas y organismos de la ciudad de Santa Fe en diciembre de 2020, para la organización del gran concierto de “Mujeres argentinas”, que se realizó en noviembre del año pasado, en el Anfiteatro “Juan de Garay”. 

En los primeros encuentros se planteó la posibilidad de ampliar los alcances de esa mesa de trabajo y un grupo de docentes coincidieron en la necesidad de elaborar un material que permita difundir la obra de Ramírez en el nivel primario con recursos innovadores, entendiendo que de esa manera se abre una puerta al vasto y diverso panorama de la música folclórica argentina. 

Durante la presentación también brindó detalles del contenido y el proceso de investigación y elaboración que les permitió lograr un material flexible para el trabajo en el aula, con ejercicios abiertos a las adaptaciones que considere cada docente. Partiendo de una selección de 10 canciones de Ariel Ramírez, las actividades que se proponen son variadas, utilizan el canto, el movimiento, la improvisación, la percusión, la realización de paisajes sonoros, quodlibet y canto a dos voces, la escucha de diferentes géneros musicales y de diversas agrupaciones corales y orquestales. “Pensamos este material sobre una obra de nuestro folclore, de un gran compositor, con recursos actualizados para la enseñanza. En el interludio de la pista que grabamos de “El Paraná en una zamba”, hay una propuesta para hacer una improvisación rappeada; y los timbres que usamos en las pistas instrumentales son de una sonoridad con la que las niñas y niños están más familiarizados”, precisó la coordinadora de Programas y Organismos Musicales.

Además, contó que para cada canción se plantean actividades para trabajar de manera interdisciplinaria, en articulación con áreas como las ciencias sociales, naturales, lengua y literatura, plástica, entre otras.

El material se complementa con un anexo que contiene las partituras en cifrado americano y las letras completas de cada canción. También se encuentran disponibles en Capital Cultural las pistas instrumentales de las canciones, grabadas por Alfonso Bekes, en una tonalidad adecuada al rango de extensión de las voces infantiles; y las 10 canciones sugeridas para escuchar en versiones de grandes intérpretes. 

Para profundizar en aspectos de la vida y la obra de Ramírez, estudiantes de la cátedra Práctica de la Educación Musical III del Instituto Superior de Música de la Universidad Nacional del Litoral desarrollaron una página web complementaria con podcasts, música, entrevistas, pautas y sugerencias para las clases. La docente Lía Zilli coordinó el trabajo al que se puede acceder mediante un QR impreso en “Ariel va a la escuela”, o directamente en Capital Cultural.

Ariel y las infancias

Desde el prólogo del material, el pianista Facundo Ramírez, agradeció la familiaridad que desde el título del proyecto señala el propósito de acercar esa gran figura artística que fue su padre, a niñas y niños. Recuerda también que con la recuperación de la democracia, a comienzos de la década de 1980, Ariel Ramírez dirigió el Centro de Divulgación Musical de la Ciudad de Buenos Aires. Desde ese espacio dio a conocer nuevos artistas, programó festivales al aire libre con orquestas y grupos de gran trayectoria, pero su tarea más importante, afirma Facundo, se llamó La Música va a la Escuela, con la que llevó el folclore argentino a las escuelas primarias. “Cuarenta años después el cuaderno didáctico Ariel va a la Escuela, tiende un puente entre aquel pasado y este presente, para que las infancias de Santa Fe abracen la inmensa obra musical de un santafesino de veras”, concluyó.