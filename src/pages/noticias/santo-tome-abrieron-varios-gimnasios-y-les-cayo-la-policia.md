---
category: Agenda Ciudadana
date: 2021-05-11T08:52:24-03:00
thumbnail: https://assets.3dnoticias.com.ar/MIER-5-GIMANASIOS-EN-SANTO-TOME-1024x546.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Lt10 y Santo Tomé al día
resumen: 'Santo Tomé: abrieron varios gimnasios y les cayó la policía'
title: 'Santo Tomé: abrieron varios gimnasios y les cayó la policía'
entradilla: Los dueños de los establecimientos insisten en que los dejen trabajar
  y este lunes se manifestaron frente al Puente Carretero.

---
Pese al decreto provincial que estableció el cierre de los gimnasios hasta el 21 de mayo, muchos establecimientos decidieron abrir sus puertas a pesar de las restricciones. Tal es así que en Santo Tomé, en varios locales les cayó la policía ante el incumplimiento de las medidas vigentes.

No obstante, los dueños insisten en que los dejen trabajar y continúan el reclamo para que sean considerados una actividad esencial.

Es por eso que este lunes los dueños de gimnasios se concentraron en Las Cinco Esquinas (25 de mayo y Avenida 7 de Marzo) para manifestarse contra el cierre y pedir que los dejen trabajar. Posteriormente, el reclamo se trasladó al Puente Carretero

Si bien, no hubo cortes de tránsito, la gran cantidad de personas que se manifestaron ocasionaron algunos inconvenientes. Esperan que las autoridades los escuchen y los habiliten para trabajar.