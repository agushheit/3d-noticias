---
category: Estado Real
date: 2020-12-29T10:43:40Z
thumbnail: https://assets.3dnoticias.com.ar/2912-assa.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Aguas Santafesinas recuerda la necesidad del uso responsable y solidario
  del agua potable
title: Aguas Santafesinas recuerda la necesidad del uso responsable y solidario del
  agua potable
entradilla: Se debe a la intensa ola de calor que alcanza a toda la región, ya que
  la alta demanda puede llevar a un consumo por encima de su capacidad de producción.

---
Ante la intensa ola de calor con altas temperaturas que alcanza a toda la región, Aguas Santafesinas recuerda la necesidad de realizar un uso responsable y solidario del agua potable, ya que la alta demanda puede llevar a un consumo por encima de su capacidad de producción.

La empresa está trabajando con toda la disponibilidad de sus sistemas de captación, potabilización y distribución de agua potable.

Por eso, para sostener la prestación de este servicio esencial es necesario el aporte de los usuarios mediante acciones concretas que no modifican la calidad de vida, pero evitan derroches innecesarios:

◾ Piletas de lona: se debe evitar la renovación constante del agua de las piletas de esparcimiento en forma innecesaria. Una pileta de lona llena de 3.000 litros equivale al consumo diario de agua potable de 25 personas. Con la dosificación diaria de hipoclorito de sodio (lavandina) o con una pastilla de cloro sólido se puede conservarla en adecuado estado por varios días. También se debe lavar los pies antes de ingresar a la pileta, retirar la basura de la superficie y cubrirla con una lona o plástico mientras no se use para evitar el ingreso de polvo u hojas que afectan el estado de conservación del agua.

◾ No utilizar el agua potable en actividades que pueden postergarse, en particular las que demandan importante cantidad de agua: lavado de autos y veredas, regado de jardines, lavarropas, llenado o renovación del agua de piletas de esparcimiento.

◾ Una manguera con salida continua de agua gasta 500 litros por hora: evitar su uso. Solo lavar las veredas los días y horarios autorizados por la Municipalidad. Utilizar baldes o mangueras provistas de sistemas de corte (gatillo o interruptores o pulsadores) para evitar el derroche. Tampoco lavar vehículos ni arrojar aguas servidas a la vía pública.

◾ No están permitidas las bombas “chupadoras” conectadas en forma directa a la red, que causan riesgos y perjuicios directamente a las cañerías de sus instalaciones internas y de sus vecinos.

◾ No se debe dejar que el agua corra innecesariamente al lavar los platos, al lavarse los dientes o al bañarse, una ducha de 10 minutos consume 80 litros de agua.

◾ Controlar las pérdidas en canillas, tanques de agua y otras instalaciones, un depósito de inodoro con deficiencias desperdicia 4.500 litros por día.

En caso de localizar una fuga de agua potable en la vía pública, los usuarios pueden notificarla a través del Centro de Atención 24 horas por WhatsApp: +54 341 695-0008