---
category: Agenda Ciudadana
date: 2021-10-18T06:00:44-03:00
thumbnail: https://assets.3dnoticias.com.ar/lealtad.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Militancia y organizaciones sociales llenaron la Plaza de Mayo por el Día
  de la Lealtad
title: Militancia y organizaciones sociales llenaron la Plaza de Mayo por el Día de
  la Lealtad
entradilla: También desde temprano se instalaron inmensos globos de los municipios
  de La Matanza, Quilmes y Florencio Varela, entre otros, y de sindicatos y movimientos
  sociales.

---
Militantes y dirigentes de organizaciones políticas, sindicales y sociales colmaron esta tarde la Plaza de Mayo para participar del acto por el 76° aniversario del Día de la Lealtad Peronista.  
  
En un clima festivo, se observaron además numerosas familias con equipos de mate, militantes con bombos, banderas y redoblantes y los tradicionales vendedores de choripanes, empanadas y bondiolas, entre otras comidas, que conforman el tradicional paisaje de los actos populares, en este caso convocado, por diversas organizaciones, como las Madres de Plaza de Mayo.  
También desde temprano se instalaron inmensos globos de los municipios de La Matanza, Quilmes y Florencio Varela, entre otros, y de sindicatos y movimientos sociales.  
  
Entre los presentes estuvieron el gobernador bonaerense, Axel Kicillof; el primer candidato a diputado nacional porteño del Frente de Todos Leandro Santoro, el ministro de Ciencia, Tecnología e Innovación, Daniel Filmus, y el exministro de Desarrollo Social Daniel Arroyo, entre otros.  
  
Numeroso grupo de personas se concentra en Plaza de Mayo. (Foto: Leo Vaca)

Asimismo, pegada a las rejas que rodean la Casa Rosada, sobre la calle Balcarce, hubo una inmensa bandera de varias decenas de metros con la leyenda "Imposible apagar tanto fuego-Néstor Vive" e imágenes pintadas del expresidente Néstor Kirchner.  
  
Al llegar a la plaza, por la Avenida de Mayo, se instaló un cartel proclamando "Encuentro peronista", con los rostros de José de San Martín, Juan Perón, Eva Perón, Manuel Belgrano, el Indio Solari, el padre Carlos Mugica y la Abuela de Plaza de Mayo Estela de Carlotto.  
  
También hubo concentraciones en los municipios de La Matanza, Quilmes y Florencio Varela. (Foto: Leo Vaca)

Se destacaron las columnas de La Cámpora, de H.I.JO.S de desaparecidos -que llegó en medio del aplauso de quienes ya estaban en el paseo-, de la CTA y hasta un grupo de la Colectividad Paraguaya Argentina, con trompetas y platillos, bailando con paraguas abiertos con las caras de Perón, Evita y Cristina Kirchner.  
  
Asimismo, hubo militantes de La Tupac Amaru, la CTD Aníbal Verón, el Frente Social Peronista (con un cartel con el escudo del PJ y la cara de Néstor Kirchner), la agrupación de Luis D'Elía, el Peronismo Militante (con una bandera negra con letras negras que la atraviesa de lado a lado, en la que se Lee "No nos han vencido") y la agrupación "Megafón".

Este 17 de octubre se conmemora el 76° aniversario de la movilización popular que se desarrolló para reclamar la libertad del entonces coronel Juan Domingo Perón, exsecretario de Trabajo y Previsión detenido en la Isla Martín García, quien al año siguiente se convertiría en Presidente de la Nación