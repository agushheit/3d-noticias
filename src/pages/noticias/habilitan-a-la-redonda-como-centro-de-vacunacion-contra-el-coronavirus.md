---
category: La Ciudad
date: 2021-07-16T06:15:03-03:00
thumbnail: https://assets.3dnoticias.com.ar/LAREDONDA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'Habilitan a La Redonda como centro de vacunación contra el coronavirus '
title: Habilitan a La Redonda como centro de vacunación contra el coronavirus
entradilla: 'El predio ubicado en Salvador del Carril y Belgrano se suma a La Esquina
  Encendida (Av. Facundo Zuviría y E. Zeballos), el CEF n°29 (Av. Galicia y Las Heras)
  y al Cemafé (Mendoza 2419). '

---
El predio de La Redonda quedará habilitado desde este viernes como centro de vacunación contra el coronavirus en la ciudad de Santa Fe.

El edificio ubicado en Salvador del Carril y Belgrano se suma a La Esquina Encendida (Av. Facundo Zuviría y E. Zeballos), el CEF n°29 (Av. Galicia y Las Heras) y al Cemafé (Mendoza 2419), los otros tres espacios de la capital provincial en los que se aplican dosis.

Para brindar más detalles, el Ministerio de Salud convocó este viernes a una conferencia de prensa a cargo de la subsecretaria de 1er y 2do nivel de atención, Marcela Fowler, el director de Salud de la Región Santa Fe, Rodolfo Rosselli, y Nerea Tacari, del Ministerio de Igualdad, Género y Diversidad.

Cabe recordar que La Redonda fue una de las sedes del operativo "Detectar Federal", la campaña nacional en la que se realizan test de antígenos para tener un resultado rápido y proceder a los cuidados y al aislamiento de las personas.