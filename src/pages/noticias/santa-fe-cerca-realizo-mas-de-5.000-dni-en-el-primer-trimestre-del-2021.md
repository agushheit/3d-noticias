---
category: Estado Real
date: 2021-05-08T06:00:02-03:00
thumbnail: https://assets.3dnoticias.com.ar/SANTAfe.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Santa fe + cerca realizó más de 5.000 dni en el primer trimestre del 2021
title: Santa fe + cerca realizó más de 5.000 dni en el primer trimestre del 2021
entradilla: El dispositivo del gobierno provincial se inició el 1 de febrero, y desde
  entonces recorrió once departamentos llevando trámites, asesoramiento y capacitaciones
  de la Secretaría de Justicia.

---
El programa provincial “Santa Fe + Cerca”, dependiente de la Secretaría de Justicia, realizó desde su inicio en el mes de febrero, un total de 5.109 trámites de DNI en su recorrida por los departamentos La Capital, Belgrano, San Justo, San Lorenzo, General López, General Obligado, Rosario, San Cristóbal, Las Colonias, Constitución y Vera.

Al respecto, el secretario de Justicia, Gabriel Somaglia, señaló que “el dispositivo es una idea que se generó desde la Secretaría de Justicia, y desde noviembre del año pasado, permitió entregar casi 10 mil DNI, con presencia del Estado en los sectores más necesitados de los servicios, con un acompañamiento integral, donde se le indica a la persona cómo y dónde gestionar distintos trámites, como por ejemplo una pensión o gestionar algún trámite de obra social”.

Además, destacó que “nos acompaña también el Colegio de Abogados de Santa Fe, que permite a los jóvenes profesionales hacer sus primeras experiencias y brindar asesoramiento jurídico, en una tarea de proximidad”.

Asimismo, el secretario resaltó que “hay un gran número de asociaciones civiles en los barrios de la provincia, como vecinales, bibliotecas, cooperadoras que también deben pasar por la Secretaria de Justicia a través de la Inspección de Personas Jurídicas y se armaron talleres para darle a conocer cómo son los trámites para mantener actualizados sus registros para poder acceder a subsidios que el Estado otorga”.

El funcionario mencionó también el programa “Protejamos Nuestra Vivienda”, del Registro de la Propiedad, que proteje el inmueble, de modo que no pueda ser ejecutado por situaciones de contingencia económica, y es un servicio gratuito.

**Números**

En el mes de febrero, se realizaron 1.319 DNI; en marzo, 1.783; y en abril, 1.734, lo que totaliza en tres meses 4.836 trámites concretados. En tanto, la primera semana de mayo se realizaron 273 DNI, alcanzando un total de 5.109 de Documentos de Identidad en la provincia de Santa Fe.

De esta manera y luego de tres meses de recorrida por la provincia de Santa Fe, se cumplió con el objetivo de garantizar el acceso de la ciudadanía a los servicios judiciales y facilitar los trámites que se realizan en el Registro Civil. Además, se incorporó «Viernes Joven» a través de una iniciativa nacional del Renaper, que otorga prioridad a los mayores de 14 años para la renovación de su DNI, siendo un trámite de carácter obligatorio.

En el año 2020, a pesar de los inconvenientes generados por la pandemia se logró realizar un total de 7.231 DNI.

A su turno el senador por La Capital, Marcos Castelló manifestó que “Santa Fe + Cerca, hace realidad el Estado en el barrio, acá está la virtud de esta decisión política que tomó el gobernador Omar Perotti, de descentralizar los trámites, porque el vecino que viene a buscar una solución, se va con lo que vino a buscar”.

Por último, Jorgelina Mudallel, concejala de Santa Fe, indicó que “la gente se va contenta porque se va con una solución y eso es lo importante, darle respuesta a la gente, llevar la herramienta a los barrios es necesario en el marco de pandemia que estamos atravesando”.

**Otras acciones**

Además, el dispositivo a través del Centro de Asistencia Judicial (CAJ), otorga apoyo, contención y acompañamiento a familiares y víctimas de delitos, que solicitaron asistencia para situaciones de conflictos personales.

El Dispositivo Territorial y Multiagencial sumó la participación de la Secretaria de Estado de Igualdad y Género con talleres, que abordaron la problemática de la violencia de género, creando un espacio de diálogo, y también se sumó el Boleto Educativo Gratuito compartiendo espacios en común.

**Presentes**

También participaron el subsecretario de Acceso a la Justicia, Franco Stampone; y los directores del Centro de Asistencia Judicial, Claudio Ainbinder; y de Boleto Educativo Gratuito, Juan Rober Benegui.