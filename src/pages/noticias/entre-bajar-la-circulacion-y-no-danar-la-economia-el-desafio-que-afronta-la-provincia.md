---
category: Agenda Ciudadana
date: 2021-04-22T07:50:28-03:00
thumbnail: https://assets.3dnoticias.com.ar/desempleojpg.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Diario Uno
resumen: Entre bajar la circulación y no dañar la economía, el desafío que afronta
  la Provincia
title: Entre bajar la circulación y no dañar la economía, el desafío que afronta la
  Provincia
entradilla: El ministro de Trabajo, juan Pusineri, dijo que la provincia se encamina
  a "un marco de restricciones porque la cuestión sanitaria así lo está indicando"

---
**El ministro de Trabajo, Juan Manuel Pusineri, afirmó que la** [**provincia**](https://www.unosantafe.com.ar/provincia-a22351) **se encamina a "un marco de** [**restricciones**](https://www.unosantafe.com.ar/restricciones-a30200) **porque la cuestión sanitaria así lo está indicando".** Advirtió que "toda la provincia, salvo dos departamentos, estén pintados de rojo (en situación de riesgo epidemiológico) y la ocupación de camas preocupa".

Hasta el mediodía de este miércoles, las [medidas](https://www.unosantafe.com.ar/medidas-a24338) no estaban definidas y continuaban las reuniones con diferentes sectores de la actividad económica. "La restricción a la circulación genera consenso desde varios aspectos. El horario no está definido pero es una de las medidas que mayor consenso genera. Posibilita que no haya accidentes de tránsito que tiene un piso alto de ocupación del sistema sanitario, ayudaría en ese sentido".

Sobre la afectación que podrían tener algunos sectores, como por ejemplo la gastronomía, destacó: "En principio vamos a tratar de no afectar la actividad económica en todo lo que podamos, la gastronomía sería de los sectores que podría seguir funcionando". Señaló "que desalentar la movilidad es uno de los factores que reunió mayor consenso".

Sobre las actividades de fin de semana, el titular de la cartera laboral definió: "Se trata de actividades que aglomeran un conjunto de personas alrededor de un hecho, sea deportivo, cultural, recreativo. Tanto en la reunión de expertos como de presidentes comunales se intendentes fue uno de los aspectos que se trató. Es probable que avancemos en restricciones en ese sentido".

Si bien señaló que la intención del gobierno "no es afectar la actividad económica", subrayó que en "el contexto actual tenemos que tomar medidas y esas medidas están surgiendo de las conversaciones que estamos teniendo".

Sobre encuentros sociales en espacios públicos, deslizó: "Va camino a ser fuertemente desalentado. Las reuniones, así sean en espacios públicos. Se van a intensificar los controles".

Sobre la presencialidad de clases: "Por el momento, de acuerdo a las particularidades de la provincia, lo que implica un análisis sanitario y de la forma de desplazamiento, el comité de expertos ha sugerido sostener la presencialidad escolar y ene se sentido nos encaminamos"

"Estamos solicitando a la legislatura provincial que se apruebe la legislación correspondiente que incremente las multas para fiestas clandestinas. Será una forma para que quien se proponga realizar este tipo de eventos, lo piense dos veces si tiene que obrar sumas significativas como sanción por este tipo de cuestiones", finalizó.