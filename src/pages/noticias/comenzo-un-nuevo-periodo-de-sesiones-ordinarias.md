---
category: La Ciudad
date: 2021-03-05T06:54:06-03:00
thumbnail: https://assets.3dnoticias.com.ar/concejo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Concejo Deliberante
resumen: Comenzó un nuevo período de Sesiones Ordinarias
title: Comenzó un nuevo período de Sesiones Ordinarias
entradilla: Este jueves, en el recinto del cuerpo legislativo local se dio inicio
  al Primer Período de Sesiones Ordinarias del Concejo Municipal.

---
De acuerdo a lo que indica el protocolo, los concejales de los distintos bloques se reunieron para dar ingreso a los proyectos que serán tratados en las sesiones posteriores, para dar paso luego al discurso del Intendente Emilio Jatón.

A su turno, Emilio Jatón evaluó el año que pasó en el marco de la pandemia por el Covid-19 y desarrolló las políticas que se implementarán durante esta nueva etapa. En tanto, insistió en el Plan Integrar como un pilar de su gestión, un plan que actúa en tres planos: la provisión de servicios básicos, la gestión ambiental y la participación ciudadana a través de las redes institucionales.

“Iniciamos el camino que nos va a llevar a celebrar los 450 años de la fundación de nuestra ciudad. Ese hecho que nos enorgullece, debe ser un hito que nos movilice, nos entusiasme, desde el ejemplo del pasado pero con la mirada en el futuro. Estoy convencido que participar es fundamental para dejar atrás las diferencias. Encontrarnos en una saludable convivencia política y social para poner a Santa Fe y a sus ciudadanos en el centro de nuestras acciones”, destacó en su discurso el intendente santafesino.

“Nadie tiene el derecho de monopolizar la lucha por el interés y el bienestar de cada santafesino y santafesina. Debe ser un compromiso de todos. El camino es claro, sabemos por dónde ir. Necesitamos recuperar la convivencia creando ciudad, apostando a la gente”, finalizó Jatón.

![](https://assets.3dnoticias.com.ar/concejo3.jpg)

Por su parte, el presidente del Concejo Municipal, Leandro González, dio su opinión sobre el discurso del intendente y resaltó que “fue un repaso muy puntilloso de las políticas de gobierno llevadas adelante durante el 2020 en el cual la pandemia sabemos que generó cambios de planes. Creo que unos cuantos planteos pro positivos que tienen que ver con obras en los diferentes barrios del cordón oeste de la ciudad”.

“Obras e intervenciones en distintos barrios del cordón oeste, mejoras y recuperación de espacios públicos, acompañamiento al sector productivo, comercio, industria y emprendedores luego de un año difícil. La importancia del Plan Integrar, obras estructurales en barrios como Yapeyú, San Agustín, La Ranita, Loyola Norte y Villa Teresa, sumado a la puesta en valor del cantero central del Bulevar 12 de octubre. El aumento del 47% del presupuesto en políticas con perspectiva de género, la convocatoria al Consejo de Seguridad Urbana, entre varios anuncios más. Un trabajo de perfil bajo pero de cara a los vecinos y vecinas”, concluyó.

![](https://assets.3dnoticias.com.ar/concejo1.jpg)

**Las voces de los concejales**

Luciana Ceresola (PRO – Juntos por el Cambio): “Fue un discurso donde se manifestó el cambio de paradigma que vivimos en el 2020, esto de convivir con nuevas realidades. Me pareció interesante las obras en los barrios 12 de Octubre, en Yapeyú y en Berutti. Y las charlas que se están teniendo con el Banco Iberoamericano para traer sistemas de seguridad que permitan minimizar el riesgo hídrico”.

Jorgelina Mudallel (Partido Justicialista): “Fue un discurso correcto. Fue honesto reconocer que la mayoría de las obras que se van a llevar a cabo en la ciudad de Santa Fe son con presupuesto Nacional y Provincial, eso demuestra la responsabilidad institucional para ayudar y trabajar mancomunadamente pensando en el futuro de los santafesinos y santafesinas”.

Carlos Suárez (UCR – Juntos por el Cambio): “Celebramos la convocatoria del Consejo de Seguridad que venimos reclamando hace mucho tiempo pero es la única dimensión en esta materia, no se está tratando cómo se abordarán los problemas. Falta más puntualización sobre la obra pública y cómo se va a acompañar a los sectores productivos que fueron los más afectados por la pandemia”.

Inés Larriera (UCR – Juntos por el Cambio): “Me hubiera gustado que el intendente nos comente como va a revertir la situación de atraso y abandono en la que se encuentra la ciudad en este momento. Es un reclamo permanente de los vecinos respecto a la falta de mantenimiento de espacios públicos, de los desagües, de bacheo. Estamos preocupados de la cotidianeidad, hoy nos encontramos con invasión de mosquitos, yuyos altos, 7000 luminarias apagadas en la ciudad”.

![](https://assets.3dnoticias.com.ar/concejo2.jpg)