---
category: Deportes
date: 2021-02-05T06:44:54-03:00
thumbnail: https://assets.3dnoticias.com.ar/basket.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La ciudad de Santa Fe será sede del torneo de Básquetbol Nacional
title: La ciudad de Santa Fe será sede del torneo de Básquetbol Nacional
entradilla: Los partidos comenzarán el 18 y se extenderán en cinco fechas hasta el
  26 del mes en curso.

---
La capital santafesina fue elegida sede de la primera etapa del torneo de Liga de Básquetbol Nacional. La competencia comenzará el próximo 18 de este mes sin público, en el estadio cubierto “Angel Pedro Malvicino” y con televisación por el canal de la provincia.

“Que Santa Fe retome el lugar que tenía en el básquetbol provincial es un orgullo. Creo que este es el paso más importante para, a partir de acá, empezar a crecer. El gobernador Perotti lo dijo apenas asumimos: sostener a los clubes, a los deportistas, para que, en los momentos difíciles, como el que estamos atravesando con esta pandemia, seguir logrando que estas instituciones sean los referentes en la contención y la inclusión. En este caso, trabajando juntos para desarrollar el turismo y el deporte”, remarcó Danilo Capitani, Ministro de Desarrollo Social.

El acto contó también con la presencia del secretario de Turismo de la provincia, Alejandro Grandinetti, la secretaria de Deportes de Santa Fe, Claudia Giaccone y el presidente de la Asociación Santafesina de Básquetbol, Roberto Monti, así como representantes de los clubes atléticos santafesinos de Colón y de Unión, entre otros.

**TELEVISACIÓN**

El secretario de Turismo de la provincial puso de relieve que “el deporte es la base fundamental del desarrollo entre los jóvenes y a partir de la enseñanza que tuvimos durante 2020, que nos cambió la vida a todos, es que no hay posibilidades de grieta: la pandemia nos mostró que puede haber políticas de Estado, que no hay diferencias entre las diferentes administraciones circunstanciales de diferentes colores políticos”.

Grandinetti destacó: “El pedido del gobernador fue que tenemos que estar al lado, en la trinchera de batalla, para que efectivamente, acompañar en este tránsito de la pandemia hasta la nueva realidad, a aquellos que generan trabajo y oportunidades” al tiempo que anunció la decisión de “la televisación para todos los santafesinos y santafesinas que quieran disfrutar de este evento”.

**DEPORTE ACTIVO PESE A LA PANDEMIA**

La secretaria de Deportes de la provincia valoró el trabajo “codo a codo para aún en estas circunstancias tan difíciles para el deporte como lo fue el año pasado, y con todos los cuidados que tenemos que seguir teniendo, se mantenga este año una agenda de prioridad absoluta para tener el deporte activo y sostener a todas nuestras instituciones deportivas, a nuestros deportistas, a lo largo y ancho de toda la provincia”.

Al dirigirse a los presentes, el titular de la Asociación Santafesina de Básquetbol agradeció a los gobiernos provincial y municipal “por apoyar este emprendimiento”, y recordó que el básquetbol santafesino “le dio prestigio a este deporte, a nivel nacional”, y que esta iniciativa “redunda en beneficio para toda la región, porque va a permitir que surjan nuevos jugadores que serán el futuro de este deporte a nivel local, provincial y nacional”.

También, estuvieron los representantes del Club Atlético Unión, Hernán Tettamanti, y del Club Atlético Colón, Carlos Fertonani; y el secretario de Producción y Desarrollo Económico de la Municipalidad de Santa Fe, Matías Schmüth entre otros.

Tettamanti, expresó que “fue un año muy difícil para el deporte y arrancar el 2021 de esta manera, demostrando que la ciudad de Santa Fe está capacitada para albergar y ser el puntapié inicial para la vuelta del básquet nos llena de orgullo”. Del mismo modo, Fertonani agradeció el apoyo “del gobierno provincial y municipal” y destacó que “en el ADN de los santafesinos está el ser sede de grandes eventos”.

**FIXTURE**

El partido inaugural será el 18 de febrero con el partido de Unión vs Colón. La fecha 1, será el sábado 20 de febrero: Echagüe vs Independiente, Unión vs villa San Martín, Colón vs Central de Ceres y Sportivo América vs Tiro Federal.

La fecha 2 será el domingo 21 de febrero entre Villa San Martín vs Echagüe, Central de Ceres vs Unión, Tiro Federal vs Colón e Independiente vs Soprtivo América.

La fecha 3 está prevista para el martes 23 de febrero con Echagüe vs Central de Ceres, Unión vs Tiro Federal, Colón vs Independiente y Sportivo América vs Villa San Martín.

La fecha 4 será el miércoles 24 de febrero y se disputarán Tiro Federal vs Echagüe, Independiente vs Unión, Villa San Martín vs Colón y Central de Ceres vs Sportivo América.

Por último, la 5 fecha se jugará el viernes 26 de febrero entre Independiente vs Tiro Federal, Villa San Martín vs Central de Ceres y Echagüe vs Sportivo América.