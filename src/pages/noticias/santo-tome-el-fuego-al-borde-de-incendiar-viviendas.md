---
category: La Ciudad
date: 2022-01-13T06:15:51-03:00
thumbnail: https://assets.3dnoticias.com.ar/fuegosantoto.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Santo Tomé: el fuego al borde de incendiar viviendas'
title: 'Santo Tomé: el fuego al borde de incendiar viviendas'
entradilla: 'Desde el martes por la noche el fuego merodea la zona de barrios cerrados
  y countries de Santo Tomé. Este miércoles, un voraz incendio estuvo a metros de
  ingresar a distintos complejos habitacionales

'

---
Horas de extremo dramatismo se vivieron este miércoles desde la mañana cuando empleados del Complejo Residencial La Tatenguita, junto a Bomberos de distintas localidades, lograron sofocar las llamas de un descontrolado incendio que amenazaba con llegar al interior del barrio cerrado en la zona del acceso norte de Santo Tomé.

Lo cierto es que desde el martes por la noche, desde la zona del río Salado y hacia el sur, por campos internos y complejos deportivos, el fuego avanzaba producto de la acción del viento arrasando con todo a su paso. La tarea de bomberos de Santa Fe y San Carlos lograron contenerlo sobre el final del día. Pero este miércoles, las llamas y el fuego se volvieron a avivar y el incendio llegó hasta el acceso norte santotomesino.

Justamente es dicha zona, en el acceso norte que une la zona de barrios cerrados y countries con la ciudad de Santo Tomé, el fuego comenzó a descontrolarse, cruzó el camino y llegó a tomar algunos árboles internos del complejo residencial La Tatenguita, lindero a El Paso y frente al country Dos Lagunas.

Gracias al rápido y desesperado trabajo de los empleados del complejo residencial La Tatenguita, con equipamiento interno, lograron contener el ingreso de las llamas del feroz incendio hasta la llegada de personal de Bomberos.

Tras varias horas de intenso trabajo, el fuego se sofocó en toda la zona de descampados donde, según narraron los vecinos, "mucha gente realiza la quema de basura".

Desde Bomberos Zapadores de la provincia de Santa Fe, indicaron que "tras el arduo trabajo de los últimos días, ahora solo resta esperar las lluvias para que ayuden a controlar la situación definitivamente".