---
category: Deportes
date: 2021-04-20T07:54:08-03:00
thumbnail: https://assets.3dnoticias.com.ar/colon-godoy-cruz.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Marisa Lemos
resumen: Colón empató con Godoy Cruz en el Cementerio de los Elefantes
title: Colón empató con Godoy Cruz en el Cementerio de los Elefantes
entradilla: El Sabalero igualó 2 a 2 como local al Tomba, con tantos de Facundo Farías
  (31m PT) y Luis Rodríguez (11m ST). Mientras que Martín Ojeda (7m ST y 25m ST) hizo
  los dos goles del visitante.

---
Colón sigue siendo el cómodo puntero de la Zona 1 de la Copa de la Liga Profesional de Fútbol a tres fechas del final de la fase de grupos pero este domingo le tocó sufrir en Santa Fe ante un Godoy Cruz en franca recuperación, que pudo llevarse la victoria sobre el final de un emotivo partido que concluyó igualado 2 a 2 en el estadio Brigadier General Estanislao López.

La mejoría de Godoy Cruz en el certamen surgió en la segunda mitad de esta decena de fechas disputadas, y a partir de allí no sorprendieron sus buenas presentaciones como, por ejemplo, con goleada incluida en Avellaneda a Racing Club por 4 a 2.

Es que el entrenador Sebastián Méndez supo insuflarle algo vital como la confianza a sus dirigidos y hacerlos salir a jugar en pos de los tres puntos en cualquier escenario, algo que se tradujo en un giro positivo para el conjunto mendocino.

Y esto le sirvió, por ejemplo, para levantar dos veces un marcador adverso ante el puntero de la Zona 1 con 21 unidades contra los 12 del equipo del "Gallego", exayudante de campo de Diego Maradona en Gimnasia y Esgrima La Plata.

También cabe resaltar  la habilidad del promisorio juvenil de 18 años Facundo Farías mantuvo siempre viva la ilusión de los de Eduardo Domínguez hasta el final, y en la última jugada del encuentro los dueños de casa tuvieron la gran chance de dejar en Santa Fe las tres unidades en disputa.

"Nos quedó la sensación amarga de no habernos quedado con los tres puntos. Pero yo sigo jugando como en el potrero", reflexionó Farías, que ya está en el radar de algunos importantes clubes europeos y también brasileños.