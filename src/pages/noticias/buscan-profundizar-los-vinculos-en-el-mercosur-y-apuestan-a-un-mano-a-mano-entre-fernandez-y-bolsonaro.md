---
category: Agenda Ciudadana
date: 2021-12-06T06:00:03-03:00
thumbnail: https://assets.3dnoticias.com.ar/albertobolsonaro.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: Buscan profundizar los vínculos en el Mercosur y apuestan a un mano a mano
  entre Fernández y Bolsonaro
title: Buscan profundizar los vínculos en el Mercosur y apuestan a un mano a mano
  entre Fernández y Bolsonaro
entradilla: 'Según confiaron a NA fuentes diplomáticas, los vínculos con el gobierno
  de Bolsonaro comenzaron a aceitarse en los últimos meses, más allá de los cortocircuitos
  iniciales entre ambas gestiones.

'

---
Con el escenario político nacional lejos de lo que fue la agitada campaña electoral, el Gobierno nacional busca profundizar el vínculo con sus socios del  
Mercosur y apuesta a un mano a mano entre el presidente Alberto Fernández y su par de Brasil, Jair Bolsonaro.

Fuentes diplomáticas indicaron a Noticias Argentinas que se está trabajando para concretar un encuentro entre Fernández y Bolsonaro en el marco de la Cumbre de los líderes del Mercosur, que se realizará en Brasilia el próximo 16 y 17 de diciembre, pero "aún no está cerrado".

Luego de varios cortocircuitos, la relación bilateral entre ambos países pareciera comenzar a transitar senderos menos turbulentos, producto de la gestiones que realizó el embajador argentino en Brasil, Daniel Scioli.

Según confiaron a NA fuentes cercanas al diplomático, los vínculos con el gobierno de Bolsonaro comenzaron a aceitarse luego de varios encuentros que gestó el propio Scioli con integrantes del Ejecutivo brasileño.

Pese a que en el inicio de la gestión de Alberto Fernández se desarrollaron varios cruces con la administración de Brasil, tanto en Brasil como en la Argentina son conscientes de la importancia que tiene el vínculo entre ambos países, motivo por el cual las diferencias ideológicas parecieran haber quedado de lado.

"Trabajamos todos los días para que esta unión se acreciente en beneficio mutuo", enfatizó Fernández respecto de la relación con el país vecino en un mensaje grabado que se transmitió días el pasado miércoles para celebrar el 36 aniversario del Día de la Amistad argentino brasileña.

En ese marco, Scioli agradeció a Bolsonaro por abrirle "las puertas de su Gobierno" y poner "a todo su equipo a disposición", con el objetivo de "encauzar" la relación entre ambas naciones.

"Estoy convencido que el próximo 17 de diciembre, en ocasión de la nueva cumbre de Presidentes del Mercosur, que también marca la primera visita oficial del Presidente Alberto Fernández a Brasil, será una gran oportunidad para continuar profundizando nuestra alianza estratégica", ponderó.

A principios de noviembre, el canciller Santiago Cafiero destacó: "Uno de los desafíos que me puso el Presidente apenas asumí fue el de revincularnos con los tres países del bloque, naturalmente con Brasil".

Durante una entrevista con los medios argentinos enviados a la Cumbre de líderes del G20 que se realizó en la ciudad italiana de Roma, entre ellos NA, el funcionario nacional reconoció que el país buscó "bajar las tensiones obvias y visibles" con Brasil.

En ese foro internacional, que se desarrolló el pasado 30 y 31 de octubre, Bolsonaro y Fernández no compartieron una reunión bilateral, pero sí se produjo el primer saludo presencial entre ambos jefes de Estado.

"Tuvieron su primer apretón de manos y fue gentil ese intercambio", describió Cafiero luego del encuentro de ambos presidentes.

Además, el jefe del Palacio San Martín puntualizó: "Argentina tiene que cuidar el Mercosur, Brasil tiene que cuidar el Mercosur. Si a la Argentina la va bien es porque a Brasil también le está yendo bien, independientemente de las dirigencias políticas esto fue así siempre".

"Eso lo vamos a cuidar. Brasil también es consciente. Lo mismo con Uruguay y con Paraguay", reflexionó el ministro de Relaciones Exteriores.

Según indicaron fuentes de la Casa Rosada a Noticias Argentinas, los preparativos que implica la Cumbre de los líderes del Mercosur están en manos de la Cancillería argentina, que tendrá la misión de cuidar los detalles de lo que será la primera visita del Presidente a Brasil.