---
category: Agenda Ciudadana
date: 2021-01-30T14:18:29.000+00:00
thumbnail: https://assets.3dnoticias.com.ar/formosa.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Infobae
resumen: 'Formosa: Amnistía Internacional solicitó que se condenen los atropellos'
title: 'Formosa: Amnistía Internacional solicitó a la Secretaría de DDHH que condene
  los atropellos'
entradilla: |2-


  Desde la ONG señalaron que el gobierno nacional no debe convalidar las políticas de Gildo Insfrán, que “exponen a las personas a situaciones de encierro compulsivo y arbitrario”. La respuesta oficial.

---
**Amnistía Internacional** solicitó hoy a la Secretaría de Derechos Humanos de la Nación que condene las violaciones a los derechos humanos cometidas por el gobierno de Formosa y **que agote todos los medios a su alcance para revertir la grave situación** a partir de las medidas desproporcionadas para combatir la pandemia provocada por el COVID-19 en la provincia.

A través de una carta dirigida al secretario de Derechos Humanos, **Horacio Pietragalla Corti**, la organización reiteró su preocupación por la gravedad de las denuncias de las personas afectadas, luego de [**la visita de la misión nacional, en la que se recorrieron centros de aislamiento**](https://www.infobae.com/politica/2021/01/28/el-gobierno-defendio-a-gildo-insfran-no-hay-centros-de-aislamiento-clandestinos-en-formosa-es-casi-un-chiste-hablar-de-eso/). En ese sentido, **enfatizaron que los hechos denunciados demuestran “una grave afectación” del derecho a la salud, a la integridad física y mental, a la vida, a no sufrir tratos crueles, inhumanos y degradantes, a la libertad personal, a la privacidad e intimidad, y a recibir un trato digno.**

“El Gobierno nacional no debe convalidar una política que expone a las personas a situaciones de encierro compulsivo y arbitrario, y a otras violaciones a sus derechos. El mandato de la Secretaría de Derechos Humanos consiste en la promoción y protección de los derechos humanos; por eso, **debe ser absolutamente respetuosa de las denuncias recibidas de los propios afectados y actuar en consecuencia, sin excusas**”, sostuvo la directora ejecutiva de Amnistía Internacional Argentina, Mariela Belski.

**Los hechos denunciados**

![](https://assets.3dnoticias.com.ar/pietragalla-if.jpg)

El gobierno de la provincia de Formosa dispuso la **cuarentena involuntaria** y **obligatoria** en Centros de Atención Sanitaria (CAS) como política sanitaria. Por su naturaleza, los mecanismos a través de los cuales se implementó la cuarentena compulsiva constituyeron una **privación de la libertad de facto en condiciones insalubres por tiempo indeterminado y de manera arbitraria.**

Según detalla Amnistía Internacional, “entre las denuncias, surgen testimonios de **personas que sufrieron y sufren el aislamiento de manera compulsiva en establecimientos que, por sus condiciones, las expusieron al contagio**. Además, los plazos del aislamiento no fueron debidamente determinados, durando incluso más de 30 días, y las personas registraron una falta de acceso a los resultados de sus testeos de COVID-19 e información precisa sobre su situación de salud”.

Asimismo, desde la ONG señalaron que los testimonios revelaron que personal policial realizaba custodia las 24 horas, siendo la presencia de personal de salud muy poco significativa. Por otra parte -según los testimonios- **en los centros se registró hacinamiento, falta de higiene, falta de ventilación, provisión escasa de alimentación**, utilización de rejas y cierre de aberturas como puertas y ventanas, inexistencia de espacios separados para hombres, mujeres, adultos mayores y niños/as para proteger mejor su salud y resguardar su intimidad, entre otras graves denuncias. **Incluso, se han dado traslados y liberaciones repentinas ante el anuncio de la visita de la Secretaría de Derechos Humanos.**

“**Una política sanitaria nunca es exitosa si implica el avasallamiento de los derechos humanos.** La vulneración de derechos no puede ser una alternativa en un país respetuoso de sus compromisos internacionales. **El cumplimiento de los derechos humanos no es un obstáculo para combatir al COVID-19 sino, por el contrario, es el marco que asigna legitimidad, legalidad y eficacia a las acciones de gobierno**”, plantearon en un comunicado oficial.

Por todo ello, Amnistía Internacional ratificó que la protección y defensa de los derechos de las personas deben ser una prioridad para la implementación de políticas para contener la pandemia y solicitó que la actuación de la Secretaría de Derechos Humanos dé cuenta del máximo respeto a los derechos humanos y a las obligaciones internacionales que el Estado argentino ha asumido.

**La respuesta oficial**

A través de una nota formal, la Secretaría de Derechos Humanos detalló que tras su visita a Formosa, se encuentra “procesando la información recopilada para formular las observaciones y recomendaciones pertinentes”.

En esa línea, agregaron que los testimonios y la información aportada por Anmistía Internacional “será incluida en el análisis”. El Secretario Horacio Pietragalla también se puso a disposición del organismo “para coordinar un encuentro”.