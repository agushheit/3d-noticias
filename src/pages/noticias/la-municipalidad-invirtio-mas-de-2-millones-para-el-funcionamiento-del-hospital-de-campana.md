---
category: La Ciudad
date: 2021-05-02T06:15:30-03:00
thumbnail: https://assets.3dnoticias.com.ar/Hospital.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Municipalidad invirtió más de $ 2 millones para el funcionamiento del
  hospital de campaña
title: La Municipalidad invirtió más de $ 2 millones para el funcionamiento del hospital
  de campaña
entradilla: Se diseñó el operativo de tránsito para garantizar el correcto funcionamiento
  del efector reubicable, instalado por la Nación en la capital de la provincia.

---
Este viernes 30 de abril, autoridades municipales inspeccionaron los trabajos que se llevan adelante en el predio del Liceo Militar “General Manuel Belgrano”, ubicado en avenida Freyre al 2100, en el marco de la instalación del hospital modular aportado por el Gobierno Nacional para la capital de la provincia.

En primera instancia, el municipio concretó la limpieza del terreno. Posteriormente, procedió a la poda de los ejemplares, tanto del interior como del exterior, para facilitar las tareas de ingreso y despliegue de las carpas que conforman el efector móvil.

Para continuar, y en un trabajo conjunto con la Empresa Provincial de la Energía (EPE), la Municipalidad colocó iluminación en el interior del Liceo y se montaron los tableros de distribución eléctrica para proveer a las carpas sanitarias. Cabe recordar que además de la luminaria, será necesario conectar equipamiento médico por lo que se requiere de un sistema que soporte la demanda energética.

Desde el municipio se confirmó que en las tareas intervinieron más de 30 empleados municipales y la inversión total que rondó los $ 2.100.000.

El gerente ejecutivo de Gestión Urbana del municipio, Matías Pons Estel, señaló que con la instalación de los baños químicos que se efectuará el fin de semana, quedarán concluidos los trabajos en el lugar. Según mencionó, la intención es “cumplir con la palabra del intendente Emilio Jatón, que se comprometió a poner a disposición todas las áreas pertinentes del municipio para la realización y la ejecución del hospital de campaña”.

Por último, y debido a que el efector móvil se ubica frente al hospital “José María Cullen”, agregó que una vez puesto en funcionamiento el centro asistencial, se montará un dispositivo especial de control de tránsito para facilitar las tareas del personal.