---
category: Agenda Ciudadana
date: 2021-04-24T06:15:45-03:00
thumbnail: https://assets.3dnoticias.com.ar/372022_meoni.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Murió Mario Meoni, el ministro de Transporte
title: Murió Mario Meoni, el ministro de Transporte
entradilla: El ministro de Transporte de la Nación, Mario Meoni, murió tras sufrir
  un accidente de tránsito cuando viajaba por la ruta 7 hacia Junín, provincia de
  Buenos Aires.

---

El parte policial indica que el ministro iba al volante ya que viajaba solo, y que perdió el control de su Ford Mondeo cerca del kilómetro 112, en un paraje conocido como San Alberto, donde el auto volcó por razones todavía bajo investigación.

Enterado del accidente, el presidente Alberto Fernandez aseguró que "con mucha tristeza recibí la ingrata noticia del fallecimiento de @mariomeoni, Ministro de Transporte de nuestro Gobierno. Con él perdemos a un político cabal, incansable y honesto. Un funcionario ejemplar. Con sincero pesar acompaño a quienes como yo lo han querido y respetado".

En el mismo sentido se expresó el jefe de Gabinete Santiago Cafiero: "Inmensa tristeza por la muerte de Mario Meoni. Un tipazo. Gran compañero, comprometido enormemente con su tarea al frente del Ministerio de Transporte. Profundamente conmovidos por esta dolorosa noticia acompañamos a su familia y a su querido pueblo de Junín".

El canciller Felipe Solá también se expresó: "Conmovido y muy triste por el fallecimiento de Mario Meoni. Se fue un dirigente y militante de inmenso valor, pero sobre todo una gran persona. Lo vamos a extrañar. Un sincero abrazo a sus familiares y amigos en este doloroso momento". A su turno, el ministro de Defensa, Agustín Rossi dijo: "Consternado por el fallecimiento de Meoni. Enorme compañero de trabajo. Gran tipo. Enorme tristeza. Abrazo a sus familiares".

El jefe del interbloque de Juntos por el Cambio, el radical Mario Negri, dijo a través de Twitter que sentía una "enorme tristeza por el fallecimiento de Mario Meoni, buena persona y funcionario honesto. Mis condolencias a sus familiares y amigos".

El ministro era un dirigente de extracción radical que militó junto a Sergio Massa en el Frente Renovador y había participado del acto del presidente Alberto Fernández en Rosario, en el marco del programa Capitales Alternas. "Hoy acompañamos al presidente Alberto Fernández en la tercera reunión del Gabinete Federal en Rosario, Santa Fe. Allí, junto al gobernador @omarperotti firmamos convenios para realizar 6 obras necesarias en la provincia con una inversión nacional de $918.639.458", había publicado en su cuenta de Twitter poco después del acto en Rosario.

Meloni nació en la localidad de Ascensión, partido de General Arenales y desde 1991 estaba casado con Laura Oliva, con quien tuvo hijos mellizos, Felipe y Robertino. Cada viernes hacía la misma ruta para pasar los fines de semana en la ciudad que fue intendente entre 2003 y 2015.

Allegados al ministro señalan que cuando era intendente, comentaba lo peligrosa que era la ruta donde terminó perdiendo su vida, y quiso convertirla en autovía. Meoni se había convertido en intendente de Junín en 2003 y fue reelecto en 2007. En ese tiempo, Meoni había integrado lo que se conoció como Concertación Plural que había promovido el entonces presidente Néstor Kirchner.