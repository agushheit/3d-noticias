---
category: Agenda Ciudadana
date: 2021-01-31T07:39:38-03:00
thumbnail: https://assets.3dnoticias.com.ar/colectivos.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de El Litoral
resumen: 'Nación quitó los subsidios al transporte de larga distancia: preocupación
  en el sector'
title: 'Nación quitó los subsidios al transporte de larga distancia: preocupación
  en el sector'
entradilla: El Ministerio de Transporte reglamentó el Fondo Compensador. Incluyó líneas
  urbanas y suburbanas hasta 60 kilómetros, pero excluyó a las de larga distancia.
  “Podría haber quiebres de empresas”, advierten desde Atap.

---
Mediante la Resolución Nº 29 emitida el pasado jueves 28 de enero, el Ministerio de Transporte de la Nación reglamentó el Fondo de Compensación al transporte público de pasajeros por automotor urbano y suburbano del interior del país. Se trata del nuevo esquema de subsidios al sector para este año, con una redistribución dentro de la cual se incluyó al transporte urbano y suburbano de hasta 60 km., pero quedó excluido el transporte interurbano de larga distancia.

En el artículo 2, la resolución establece que serán beneficiarios del Fondo los servicios públicos de transporte por automotor de pasajeros urbanos, municipales o provinciales; y los servicios públicos de transporte por automotor de pasajeros suburbanos provinciales (...) cuyos recorridos entre cabeceras sean de hasta 60 km.". Pero en el inciso b (punto 4) dice: "Las acreencias del Fondo Compensador no tendrán como destinatarios a los servicios urbanos diferenciales, expresos, exclusivos y los servicios interurbanos provinciales".

Para la provincia de Santa Fe corresponderían -para el transporte urbano y suburbano- el siguiente esquema de subsidio, según el anexo que acompaña el referido acto administrativo: para este mes de enero, 434.400.000,00 pesos; para febrero, 217.200.000,00 pesos; y para marzo, 217.200.000,00 pesos. El total de fondos que llegarían a la provincia para sostener esas dos modalidades de transporte es de 2.896.000.000,00 pesos.

Pero en esta "torta" de acreencias no figura, como se dijo, el transporte de larga distancia de más de 60 kilómetros de recorrido. "Esto es una canallada, es la crisis terminal de sector", se quejó en diálogo con El Litoral Leandro Solito, referente de Asociación de Transporte Automotor de Pasajeros de Santa Fe (Atap), nucleada en la Federación Argentina de Transportadores por Automotor de Pasajeros (Fatap).

**¿Quiebres?**

"Esto traerá aparejado el quiebre de muchas empresas de larga distancia, que ya con el subsidio actual tenían dificultades para sostenerse (más aún en el contexto de pandemia; cabe recordar que el sector pudo volver a trabajar en diciembre pasado). Las empresas vienen con una mochila muy pesada: muchos meses sin brindar sus servicios, desapareció la recaudación por venta de boletos como ingreso fundamental de ingresos y ahora se les quita los subsidios nacionales", advirtió.

En el anterior esquema del Fondo Compensador estaban todas las modalidades de transporte incluidas (urbanos, suburbanos e interurbano provinciales de larga distancia). "Ahora se vuelve al sistema de distribución de antes de 2011: con esto, se beneficia al Amba en detrimento de todo el interior del país", volvió a fustigar.

Solito adelantó que el lunes mantendrán reuniones con legisladores nacionales por Santa Fe por este tema, y se presentará una nota a la Fatap: "Las empresas de larga de distancia estaban intentando ahora generar un poco más de fluidez en los ingresos por la posibilidad del inicio del ciclo lectivo; se iba a poner en vigencia el Boleto Educativo Gratuito… Pero con esta resolución, no sé qué va a pasar con la sostenibilidad del sector. Hay mucha preocupación".

La Provincia podría fijar el miércoles una actualización de tarifas para los servicios de larga distancia. "Esto sería un paliativo, o más o menos... Viendo la situación en contexto, el sistema se tiene que redimensionar, porque la gente viaja menos, los patrones sociales de circulación cambiaron: la gente en general se desacostumbró al colectivo, como era a marzo de 2020 (antes de la declaración de la Aspo); es decir, modificó su hábitos de circular. Pero sin subsidios nacionales, todo cae en saco roto; no entramos en los ATP y sacan los subsidios al sector: es una canallada", criticó. Al sector sólo le queda el "colchón" de los subsidios provinciales, "que se incrementó en 500 millones de pesos respecto del año pasado", añadió el referente de Atap.

**"Cautela"**

El secretario de Transporte provincial, Osvaldo Miatello, anticipó a este diario que el próximo lunes cerca de las 13 mantendrá una reunión vía Zoom -junto a los secretarios de transporte del resto de las provincias- con autoridades nacionales. "Desde el Ministerio de Transporte nacional nos convocaron para darnos detalles de la resolución. Yo sería cauto hasta tanto saber con exactitud qué implicancias concretas tiene esta decisión administrativa", dijo.

Además, "aún no nos remitieron los convenios que debe firmar cada provincia, en los cuales están los anexos con los detalles de qué montos hay que pagar a cada empresa. Como tampoco eso llegó, no quisiera adelantar nada para no generar incertidumbre, hasta tanto conocer con precisión qué alcances concretos tiene esta norma", declaró. Habrá que esperar hasta el lunes por la tarde para conocer cómo sigue el tema.

**Puntos clave de la resolución**

\-La resolución emitida por el Ministerio de Transporte nacional reglamenta el Fondo de Compensación al Transporte Público de pasajeros por automotor urbano y suburbano del interior del país creado por el artículo 125 de la Ley N° 27.467 de Presupuesto General de la Administración Nacional para el Ejercicio 2019, el cual ha sido prorrogado por el artículo 72 de la Ley N° 27.591 de Presupuesto General de la Administración Nacional para el Ejercicio 2021, a fin de asistir a las provincias y por su intermedio a las empresas prestadoras de servicios públicos de transporte automotor urbano y suburbano de carácter provincial y municipal de cada jurisdicción, con un monto de afectación de hasta $ 20.000.000.000.

\-Será condición para percibir acreencias en el marco del Fondo de Compensación la suscripción de convenios entre las provincias y el Ministerio de Transporte.

\-Los destinatarios del Fondo Compensador serán los servicios públicos de transporte por automotor de pasajeros del Interior de la República Argentina, prestados por sujetos públicos o privados, que sean efectuados únicamente bajo la modalidad denominada 'servicio común' que, a los fines de la obtención de las acreencias, se dividen en servicios públicos de transporte por automotor de pasajeros urbanos, municipales o provinciales servicios públicos de transporte por automotor de pasajeros suburbanos provinciales definidos por las autoridades provinciales (...) cuyos recorridos entre cabeceras sean de hasta 60 km.

\-Las acreencias del Fondo Compensador no tendrán como destinatarios a los servicios urbanos diferenciales, expresos, exclusivos y los servicios interurbanos provinciales.