---
category: La Ciudad
date: 2021-01-26T09:56:09Z
thumbnail: https://assets.3dnoticias.com.ar/playa.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad Santa Fe
resumen: La Municipalidad intensifica el mantenimiento de las playas
title: La Municipalidad intensifica el mantenimiento de las playas
entradilla: Las tareas se profundizaron en la Costanera Este y continuarán por la
  Oeste. A la limpieza de las zonas de arena se suma el perfilado de los bosques nativos
  y la recolección manual de residuos.

---
Desde que inició la temporada de verano, la Municipalidad desarrolla un cronograma de limpieza en las playas de la capital provincial, ubicadas en las costaneras Este y Oeste, las cuales se utilizan exclusivamente como solariums. Las tareas, a cargo de la Secretaría de Obras y Espacio Público, se desarrollan a los fines de mantener la limpieza de los espacios que utilizan tanto vecinos y vecinas como visitantes, garantizando el disfrute.

Si bien el trabajo se realiza durante todo el año, se intensificó a partir del 1° de diciembre, especialmente en las áreas delimitadas como bosques nativos. Se trata de los seis sectores ubicados en la Costanera, compuestos por especies leñosas e identificados con cartelería específica. El objetivo es mejorar la cantidad y la calidad de la arena, mientras se maneja la vegetación en la playa.

En gran parte de la extensión de las playas de la ciudad hay sectores donde, por un proceso de naturación espontánea, se desarrollan pastos bajos y se superponen a la carpeta de arena y obstaculizan con ello la posibilidad de su disfrute. Para recuperar su aspecto, el municipio realiza un paso de disco de arado y luego de una rastra que retira la toda la vegetación superficial removida y devuelve a la arena ese aspecto de limpieza tan anhelado por los usuarios.

Asimismo, desde diciembre, se dispuso un refuerzo en los trabajos de limpieza con cuadrillas que recorren las playas cuatro veces por semana realizando recolección de diseminados que luego son embolsados y subidos para su retiro por el servicio de recolección urbana. Otro grupo de trabajo corta el césped con motoguadañas en la parte alta de ambas costaneras.

Limpieza y despeje

El director de Infraestructura Verde de la Secretaría de Obras y Espacio Público, Paulo Chiarella, explicó que una de las tareas más importante es el perfilado de esos bosques nativos, a los fines de mantenerlos en la silueta original y despejarlos de toda la vegetación herbácea que va colonizando la arena. Los profesionales que tienen a cargo esa labor buscan delimitar la vegetación y la arena y, cuando la oportunidad lo permite, aplican un delineamiento paisajístico.

“No es una simple acción librada a su suerte sino un trabajo de intervención en el paisaje”, detalló. Además, dejó en claro que “no es un desmalezamiento sin criterio sino una estrategia de intervención en el paisaje a los fines de mejorar el lugar”. El trabajo se compone de tres fases: el perfilado y desmalezado; el paso de disco para remover la vegetación de borde; y el paso con rastra para el retiro de esa vegetación. Estas tareas permiten recomponer la zona de arena, limpiándola en los lugares con presencia de herbáceas.

Chiarella recordó que para esta temporada se decidió marcar la presencia de los bosques nativos que actualmente se expresan de un modo muy particular, por efecto de la bajante evidenciada en los cursos de agua. Aunque mencionó que por tercer año consecutivo la vegetación forestal viene creciendo, recordó que “históricamente se peleó contra la vegetación allí ubicada, que es la que nos trae el valle aluvial del río”.

Teniendo en cuenta que ese bosque pugnará siempre por expresarse, en este caso a la vera de la laguna Setúbal, “este año decidimos conservar aquellas zonas donde se desarrollaron las especies arbóreas, por los servicios ambientales que brindan: sombra, como lo más evidente; sostenimiento de la biodiversidad urbana, al tiempo que se evita el daño que hacen las ciudades al corredor fluvial del Paraná; y ahorro del costo de pelear contra una vegetación que se expresará espontáneamente siempre”.

![](https://assets.3dnoticias.com.ar/playa1.jpg)

![](https://assets.3dnoticias.com.ar/playa2.jpg)