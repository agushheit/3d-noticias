---
category: Agenda Ciudadana
date: 2021-03-10T06:59:42-03:00
thumbnail: https://assets.3dnoticias.com.ar/construccion.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: La construcción es el sector con mejores expectativas de contratación de
  personal
title: La construcción es el sector con mejores expectativas de contratación de personal
entradilla: Un estudio privado revela que el 78% de los empleadores encuestados "no
  espera realizar cambios en sus dotaciones de personal", el 7% planea disminuirlas,
  el 11% aumentarlas y el 4% restante no sabe si realizará cambios.

---
El sector de la construcción lidera las expectativas de contratación para el segundo trimestre, mientras que el 78% de los empleadores argentinos espera mantener sus dotaciones de personal, indicó hoy un informe privado. 

"En seis de las nueve actividades económicas relevadas los empleadores esperan aumentar sus nóminas durante el segundo trimestre del 2021", señaló la encuesta realizada por la consultora Manpower Group.

El análisis sostuvo que el 78% de los empleadores encuestados "no espera realizar cambios en sus dotaciones de personal", al tiempo que el 7% planea disminuirlas, el 11% aumentarlas y el 4% restante no sabe si realizará cambios.

 A su vez, al ser consultados sobre las expectativas de recuperación en virtud del impacto del COVID-19 sobre los negocios, el 29% afirmó que "espera que sus empresas vuelvan a los niveles de contratación pre-pandémicos dentro de los próximos doce meses".

Durante el trimestre anterior el 35% planteaba este horizonte de recuperación.

Por otra parte, el 11% de los encuestados piensa que le llevará más de doce meses recuperar los niveles de contratación, mientras que en el período anterior el 32% manifestaba estos plazos. 

El estudio subrayó que "el 3% cree que los niveles de contratación nunca regresarán a los valores previos a la pandemia, cuando en el relevamiento anterior el 7% había expuesto esta intención".

"El mayor porcentaje (45%) indicó que no presenta cambios en los niveles de contratación desde que comenzó la pandemia del COVID-19", aclaró.

Remarcó que "en seis de las nueve actividades económicas relevadas, los empleadores esperan aumentar sus nóminas durante el segundo trimestre del 2021". 

"Estas expectativas positivas son lideradas por la construcción", destacó. 

Sostuvo que luego se ubicaron "los sectores Manufacturas, Minería y Extracción, Comercio Mayorista y Minorista y Administración Pública y Educación".

"Por el contrario, Transportes y Servicios Públicos reporta las proyecciones de contratación más débiles", puntualizó. 

Respecto de las regiones, evaluó que en tres de seis los empleadores proyectan incrementar sus nóminas de personal durante el segundo trimestre del 2021.

"La región Pampeana lidera estas expectativas", remarcó y consideró que, por el contrario, el NEA revela las intenciones de contratación más débiles.

Por su parte, Cuyo y el Área Metropolitana de Buenos Aires (AMBA) reportaron expectativas nulas.

Al referirse a la situación en el mundo, analizó que "en América los empleadores esperan aumentos en sus expectativas de contratación durante el segundo trimestre de 2021 en ocho de los diez países relevados". mientras que en dos de ellos preven disminuir sus nóminas. 

"En comparación con el trimestre anterior, las intenciones se fortalecen en cuatro países, pero se debilitan en seis", apuntó. 

Resaltó que Estados Unidos, Brasil y Canadá muestran las expectativas más positivas en cuanto a la contratación de personal, mientras que Panamá y Perú registran las más débiles.