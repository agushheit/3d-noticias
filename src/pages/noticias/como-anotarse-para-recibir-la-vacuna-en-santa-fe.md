---
category: Agenda Ciudadana
date: 2021-02-23T07:17:54-03:00
thumbnail: https://assets.3dnoticias.com.ar/vacunación.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: "¿Cómo anotarse para recibir la vacuna en Santa Fe?"
title: "¿Cómo anotarse para recibir la vacuna en Santa Fe?"
entradilla: Este lunes el gobierno santafesino habilitó el sitio web para que los
  ciudadanos se anoten para recibir la dosis contra el coronavirus. Cuáles serán las
  prioridades.

---
Ya está habilitado el registro para que los santafesinos puedan anotarse para recibir la vacuna contra el coronavirus. Puede hacerlo toda la ciudadanía porque el formulario no limita edades ni condiciones de morbilidad, pero Salud hará un orden de prioridades y la aplicación comenzará con los mayores de 70 años.

Si bien la página de gobierno estuvo colapsada durante varias horas ante la demanda para ingresar, con paciencia aquellos que quieran inscribirse pueden hacerlo. Para registrarse se debe ingresar al Plan Provincia Santa Fe Vacuna haciendo click [AQUÍ](https://www.santafe.gob.ar/santafevacunacovid/inicio)

La llegada a la provincia de la vacuna producida en India comenzó en Santa Fe la vacunación de personas que se encuentran en geriátricos, donde tendrán prioridad los residentes mayores de 70 años.

El secretario de Salud Jorge Prieto recomendó que se inscriban por el momento los mayores de 80 años y posteriormente los de 70 quienes tienen prioridad, dejando para este grupo la posibilidad de acceder al sistema.

**Cómo seguirá la vacunación**

Posteriormente, los adultos entre 60 y 69 años; las fuerzas de seguridad; las personas entre 18 y 59 años con factores de riesgo; los docentes y no docentes del nivel inicial, primaria y secundaria.