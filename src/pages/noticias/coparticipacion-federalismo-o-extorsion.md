---
category: Agenda Ciudadana
date: 2020-12-02T10:41:44.000+00:00
thumbnail: https://assets.3dnoticias.com.ar/ximenagarcia.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Por Ximena García
resumen: 'COPARTICIPACIÓN: ¿Federalismo o extorsión?'
title: 'COPARTICIPACIÓN: ¿Federalismo o extorsión?'
entradilla: La recurrente discusión sobre coparticipación no es casual. En estos últimos
  meses hemos sido testigos de un importante y progresivo degradamiento de nuestra
  calidad institucional.

---

###### [**XIMENA GARCÍA**](https://twitter.com/XimenaGarciaBl "Twitter") **| Diputada Nacional por Juntos por el Cambio**

El presidente Alberto Fernández nos ha acostumbrado a que el gobierno se ejerce a través del uso y abuso de herramientas constitucionales de excepción, como los DNUs, y siempre en pos de sus propios intereses político partidarios, y en detrimento de nuestro Estado de Derecho, avanzando sobre la letra de nuestra Constitución Nacional fundamentalmente respecto de los principios Republicanos y Federalistas.

Dicho esto, no nos sorprende la desprolija convocatoria a la última sesión del período ordinario, no sólo por su escasa antelación, sino por su intrínseca nulidad al transcurrir aún estando vencidos los protocolos de sesiones virtuales, a través de la cual el oficialismo buscó hacernos parte de la sanción de esta norma flagrantemente inconstitucional.

Como sostuvimos desde el bloque de Juntos por el Cambio en ocasión de dictarse el Decreto 735/2020, que redujo el coeficiente de coparticipación de CABA del 3,50% al 2,32%, transfiriendo aprox. $40.000 millones a la provincia de Buenos Aires a fin de atender a los reclamos salariales de las fuerzas de seguridad, hoy nos vemos en la obligación de volver a denunciar la sanción de una nueva detracción arbitraria sobre los fondos que conforman la coparticipación de CABA, consagrándose un precedente gravísimo en la materia.

El texto, acompañado de fundamentos endebles que no logran justificar la reducción del coeficiente de coparticipación del 2,32% al 1,40%, consagrada un desfinanciamiento del 70%, a partir de la fijación del gasto anual en $ 24.500.000.000 como compensación por la transferencia de los servicios de seguridad no federales debido a que, curiosamente, el costo presupuestario para asumir salarios y gastos de funcionamiento de la policía de la ciudad alcanza los $ 75.800 millones.

Esta modificación vulnera dos principios constitucionales que merecen nuestra atención. Por un lado aquel plasmado en el art. 75 inc. 2 que indique que “no habrá transferencia de competencias, servicios o funciones sin la respectiva reasignación de recursos”, y por otro, atropella las reglas del federalismo de concertación que determina que el Estado Nacional no puede de modo unilateral modificar dichas asignaciones sin acuerdos previos entre las partes, sobretodo cuando estas acciones conllevan un menoscabo a los intereses de alguno de los entes de la federación. En este sentido la CSJN tiene dicho que “por tratarse de derecho intrafederal”, la inexistencia del correspondiente acuerdo de las provincias, implicaría una falta inaceptable de diálogo institucional.

![](https://assets.3dnoticias.com.ar/coparticipacion.jpg)

El oficialismo nos quiere convencer de la legalidad de sus pretensiones por el solo hecho de darles forma de ley, cuando conocen a la perfección su carácter unilateral que, lejos de consagran nuevos acuerdos, pretende echar por tierra la legislación y los principios existentes en la materia.

En 2017, la ley 26.288 determinó que CABA asumiría las funciones y facultades de seguridad en todas las materias no federales, a partir de la celebración de convenios específicos. Aún así, no fue hasta enero de 2016 que se convino la transferencia de la PFA a CABA. Ello implicó que el 45% de los agentes de policía, el 100% del servicio de seguridad metropolitana que incluye 54 comisarías, el 75% de bomberos, el 45% del resto de los programas y el 85% de las áreas centralizadas pasarán a manos de la ciudad, por lo que a través del Decreto 194/16 se incrementó el coeficiente de coparticipación de CABA del 1,40% al 3,75%.

El consenso fiscal de 2017, comprometió al Estado Nacional a reducir dicho coeficiente con el objeto de preservar la igualdad con las provincias. Y en cumplimiento de ello, el presidente Maurico Macri, lo convalidó a través del Decreto 257/18, reduciéndolo al 3,50%.

Es cada vez más evidente que los problemas de las provincias “amigas”, como la comandada por Axel Kicilof, relativizan las necesidades del resto de las jurisdicciones, dañando gravemente a nuestro maltrecho sistema de coparticipación federal vigente desde 1988.

Si existiera interés legítimo por discutir este tema, seríamos convocados, junto a los representantes de todas las provincias a fin de intentar sancionar una nueva Ley convenio de Coparticipación Federal siguiendo los lineamientos contenidos en la Carta Magna, aportando reglas claras, equidad y automaticidad en la distribución.

La solución no radica en parches autoritarios, que con su disfraz de democráticos alientan la mezquindad y la separación. Debemos terminar con el manoseo institucional que genera el sometimiento de los gobiernos provinciales a cambio de los fondos necesarios para solventar las necesidades básicas de sus provincias.

Basta de atropellos institucionales que consolidan la extorsión. Queremos vivir en un Estado con más y mejor federalismo.

🌐 [https://ximenagarcia.com.ar/](https://ximenagarcia.com.ar/ "https://ximenagarcia.com.ar/")