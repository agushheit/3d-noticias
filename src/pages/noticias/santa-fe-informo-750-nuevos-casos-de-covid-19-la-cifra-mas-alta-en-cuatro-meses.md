---
category: La Ciudad
date: 2021-12-28T06:15:13-03:00
thumbnail: https://assets.3dnoticias.com.ar/COVID.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Santa Fe informó 750 nuevos casos de Covid-19, la cifra más alta en cuatro
  meses
title: Santa Fe informó 750 nuevos casos de Covid-19, la cifra más alta en cuatro
  meses
entradilla: 'Tras el fin de semana, el Ministerio de Salud comunicó 354 infectados
  más que los informados el pasado jueves. De ese total, 78 corresponden a esta capital.

'

---
El Ministerio de Salud de Santa informó este lunes 750 nuevos casos de Covid-19 en toda la provincia de Santa Fe. Representa el número de infectados más alto en más de cuatro meses, ya que es superado por los 766 registrados el 18 de agosto.

De ese total, 78 corresponden a la ciudad de Santa Fe y 327 a Rosario.

El número total de confirmados desde el inicio de la pandemia se eleva a 476.822. En toda la provincia ya se recuperaron 464.778 pacientes y 3.398 transitan la enfermedad.

La cartera sanitaria informó este lunes un fallecido. El total de muertes es 8.646.

**A nivel nacional**

Otras 31 personas murieron y 20.263 fueron reportadas con coronavirus en las últimas 24 horas en la Argentina, con lo que suman 117.066 los fallecidos registrados oficialmente a nivel nacional y 5.480.305 los contagiados desde el inicio de la pandemia, informó hoy el Ministerio de Salud.

La cartera sanitaria indicó que son 880 los internados con coronavirus en unidades de terapia intensiva, con un porcentaje de ocupación de camas de adultos de 34,4% en el país y del 36,2% en la Área Metropolitana Buenos Aires. El parte precisó que murieron 13 hombres y 17 mujeres y 1 personas sin identificación de género.

Respecto a los 13 hombres, 5 fallecieron en la Provincia de Buenos Aires, 1 en CABA, 1 en Entre Ríos, 2 en Río Negro, 2 en Salta, 1 en Santa Fe y 1 en Tucumán.

El detalle de las 17 mujeres fallecidas es el siguiente: 9 en la Provincia de Buenos Aires, 2 en Buenos Aires, 1 en Córdoba, 1 en Entre Ríos, 2 en Mendoza, 1 en Río Negro y 1 en Tucumán.

El Ministerio indicó además que se realizaron en las últimas 24 horas 70.532 testeos y desde el inicio del brote ascienden a 27.537.879 las pruebas diagnósticas para esta enfermedad.

Según la última actualización del Monitor Público de Vacunación, las dosis aplicadas hasta ahora en el país son 74.785.702; 37.870.906 personas están vacunadas con el esquema inicial y 32.321.657 tienen el esquema completo, mientras que 2.341.630 recibieron la dosis adicional y 2.251.509 la de refuerzo.

En tanto, las dosis distribuidas en todo el país son 93.727.645.

Respecto a los casos de infectados diarios reportados, en Buenos Aires se produjeron 6.199, 4.005 en CABA, 193 en Catamarca, 216 en Chaco, 59 en Chubut, 171 en Corrientes, 5.583 en Córdoba, 115 en Entre Ríos, 31 en Formosa, 86 en Jujuy, 133 en La Pampa, 17 en La Rioja, 242 en Mendoza, 65 en Misiones, 243 en Neuquén, 297 en Río Negro, 268 en Salta, 78 en San Juan, 128 en San Luis, 112 en Santa Cruz, 110 en Santiago del Estero, 35 en Tierra del Fuego y 1.107 en Tucumán.

El total de acumulados por distrito indica que la provincia de Buenos Aires suma 2.135.784 casos; la Ciudad de Buenos Aires, 554.095; Catamarca, 52.339; Chaco, 101.891; Chubut, 84.497; Corrientes, 96.928; Córdoba, 553.051; Entre Ríos, 138.536; Formosa, 62.543; Jujuy, 49.504; La Pampa, 69.549; La Rioja, 34.072; Mendoza, 168.063; Misiones, 37.809; Neuquén, 118.051; Río Negro, 105.444; Salta, 90.430; San Juan, 70.514; San Luis, 81.381; Santa Cruz, 60.043; Santiago del Estero, 82.461; Tierra del Fuego, 32,658 y Tucumán, 2023.716.