---
category: Agenda Ciudadana
date: 2021-05-10T06:15:43-03:00
thumbnail: https://assets.3dnoticias.com.ar/aviones.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: Este lunes llegan al país medio millón de dosis de Sputnik V
title: Este lunes llegan al país medio millón de dosis de Sputnik V
entradilla: Desde el 24 de diciembre de 2020 al 30 de abril de este año, la Argentina
  recibió 11.698.250 vacunas Sputnik V, Sinopharm y AstraZeneca.

---
Un cargamento de medio millón de dosis del componente 1 de la vacuna Sputnik V llegará a la Argentina a primera hora del lunes proveniente desde Moscú.

En un vuelo de Aerolíneas Argentina, a la 1:00 del lunes llegarán 500 mil nuevas dosis, con las cuales la cantidad de vacunas recibidas aumentará a casi 12,2 millones.

Desde el 24 de diciembre de 2020 al 30 de abril de este año, la Argentina recibió 11.698.250 vacunas Sputnik V, Sinopharm y AstraZeneca, y hasta el momento se distribuyeron 11.337.571 y se aplicaron 9.082.597, de acuerdo a los datos del Monitor Público de Vacunación, que permite conocer en tiempo real el avance en todo el país.

En tanto, de ese total, 6.035.850 dosis corresponden a la vacuna Sputnik V (4.975.690 del primer componente y 1.060.160 del segundo componente); 4.000.000 a Sinopharm; 580.000 a Covishield/ AstraZeneca; y 1.082.400 de AstraZeneca que se recibieron a través del mecanismo COVAX de la Organización Mundial de la Salud.

Entre diciembre y enero llegaron al país 820.150 dosis de Sputnik (410.150 del primer componente y 410.000 del segundo).

Asimismo, en febrero arribaron un total de 2.497.890: 917.890 corresponden a la vacuna Sputnik V (729.090 al componente 1 y 188.800 al componente 2); 580.000 a Covishield y 1.000.000 a Sinopharm.

En marzo se recibieron 2.452.870 dosis de vacunas contra la Covid-19: de ese total, 2.234.470 son Sputnik (1.773.110 del componente 1 y 461.360 del componente 2) y 218.400 de AstraZeneca del mecanismo COVAX.

Mientras que, en abril, arribaron al país un total de 5.927.340 vacunas, de las cuales 2.063.340 son del componente 1 de Sputnik; 864.000 de AstraZeneca del mecanismo COVAX y 3.000.000 a Sinopharm.

El 19 de mayo se liberarán también 861.600 dosis de AstraZeneca por medio de COVAX para completar los esquemas de inmunización de quienes ya recibieron la primera dosis de Covishield/AstraZeneca.