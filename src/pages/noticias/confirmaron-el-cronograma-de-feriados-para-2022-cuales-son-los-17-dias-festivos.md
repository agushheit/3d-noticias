---
category: Agenda Ciudadana
date: 2021-11-14T06:15:53-03:00
thumbnail: https://assets.3dnoticias.com.ar/MARDEL.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: 'Confirmaron el cronograma de feriados para 2022: cuáles son los 17 días
  festivos'
title: 'Confirmaron el cronograma de feriados para 2022: cuáles son los 17 días festivos'
entradilla: "Se prevén 12 feriados inamovibles, dos trasladables y tres con fines
  turísticos. El decreto será publicado el lunes próximo en el Boletín Oficial.\n\n"

---
Los ministerios del Interior y de Turismo y Deportes informaron el cronograma de feriados para el 2022, que prevé 12 feriados inamovibles, dos trasladables y tres con fines turísticos.  
  
Los feriados fueron dispuestos por el decreto 789, firmado por el presidente Alberto Fernández, que será publicado el lunes próximo en el Boletín Oficial.  
  
**Inamovibles**

De ese modo, los feriados inamovibles son: el 1° de enero (Año Nuevo); 28 de febrero y 1° de marzo (Feriados de Carnaval); el 24 de marzo (Día Nacional de la Memoria por la Verdad y la Justicia); el 2 de abril (Día del Veterano y de los Caídos en la Guerra de Malvinas) y el 15 de abril (Viernes Santo).  
  
También serán feriados inamovibles: el 1° de mayo (Día del Trabajador); el 25 de mayo (Día de la Revolución de Mayo); el 20 de junio (Paso a la Inmortalidad del General Manuel Belgrano); el 9 de julio (Día de la Independencia); 8 de diciembre (Día de la Inmaculada Concepción de María) y el 25 de diciembre (Navidad).  
  
**Trasladables**

En tanto, uno de los feriados trasladables será el del 17 de junio (Paso a la Inmortalidad del General Don Martín Miguel de Güemes), que se celebrará el mismo viernes porque no se puede trasladar al lunes siguiente porque coincide con el 20 de junio.  
  
El otro feriado trasladable de 2022 será el 17 de agosto (Paso a la Inmortalidad del General. José de San Martín) se celebrará el 15 de agosto y el 12 de octubre (Día del Respeto a la Diversidad Cultural) se celebrará el 10 de octubre.  
  
**Con fines turísticos**

En cuanto a los feriados con fines turísticos, el decreto estableció como fechas el 7 de octubre, 21 de noviembre y 9 de diciembre.  
  
Así, el próximo año contará con cuatro fines de semana largos, de cuatro días cada uno: desde el sábado 26 de febrero al martes 1° de marzo; desde el viernes 17 al lunes 20 de junio; desde el viernes 7 al lunes 10 de octubre; y desde el jueves 8 al domingo 11 de diciembre.  
  
Los ministerios señalaron en el comunicado que los feriados contribuirán a impulsar escapadas turísticas y, con ellas, reactivación de la actividad turística.