---
category: La Ciudad
date: 2021-10-06T06:15:21-03:00
thumbnail: https://assets.3dnoticias.com.ar/bolichesjpg.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Pese a la habilitación de Salud, los boliches podrían no abrir en Santa Fe
title: Pese a la habilitación de Salud, los boliches podrían no abrir en Santa Fe
entradilla: Un referente de los boliches ubicados en la Ruta 168 dijo que "están evaluando"
  la reapertura porque no les conviene comercialmente, pese a la habilitación

---
Los boliches ya están habilitados para abrir con un 70 por ciento de aforo, al aire libre, desde las 20 hasta las 3 del día siguiente. Pueden trabajar viernes, sábados y vísperas de feriado, por lo que en la víspera del feriado del viernes 8 ya podría volver el baile oficialmente a la ciudad de Santa Fe. Pero, ¿se podrá?

En diálogo con el programa La Mañana de UNO (de 7 a 9 por FM 106.3 La Radio de UNO) el dueño del complejo La Pirámide, Jorge Reynoso, habló por todo el sector de las discotecas cuando dijo: “Estamos evaluando si comercialmente vale la pena abrir; creemos que no”.

“Poner nuestro negocio con lo deteriorado que está en casi dos años sin uso conlleva inversiones muy grandes y el contexto comercial no es muy saludable, por todos los condicionamientos”, sintetizó.

La pandemia está lejos de ser el problema principal de los boliches. Con la relocalización de los boliches a una ruta olvidada, y la creciente oferta nocturna en el centro de la ciudad, los dueños tienen las manos atadas. “Se reduce mucho la gente que uno puede llegar a ofrecerle algo interesante, algo convocante, fundamentalmente porque no podemos competir con la plaza gastronómica que se ha desarrollado en la ciudad”, explicó Reynoso.

“En lo que respecta a La Pirámide no hay posibilidad de abrirlo en lo inmediato, si se decide abrir. No te aseguraría los otros dos construidos, que son Alta Vista y Águilas”, dijo, y agregó que también existen serias posibilidades de que Island cierre.

Según Reynoso los lugares que posiblemente abran son las concesiones al aire libre como Club del Lago y Excursionistas pero advirtió que “el rubro está muy golpeado”.

“Las inversiones para poder abrir son muy grandes, los riesgos son muy grandes. Además, uno hace una gran inauguración y te llueve; teniendo los lugares para poder bailar adentro no se pueden usar, ¿qué hacemos?”, se preguntó. “Para nosotros en este contexto es muy difícil trabajar”.

**Boliches en la 168**

El referente contó que está prevista una reunión con el municipio “para ver si se van a hacer respetar las ordenanzas y los lugares únicos para bailar que son los que dictan la zonificación de la ciudad”.

“El área de boliches vino a raíz de una ordenanza de relocalización (y anterior de emergencia nocturna) que antes de la pandemia no se respetó; la gestión anterior no respetó la misma ordenanza que cerró lugares en el centro, que hizo invertir a tres sociedades en el sector, y no terminó las obra del sector”, recordó. “Teníamos muchos problemas para funcionar, fue muy restrictiva la actividad y la gente nos dio la espalda; empezaron a proliferar los los pubs en barrio Candioti, cosa que también está fuera de la normativa, y hoy esta nueva gestión tiene un desafío muy grande a ver qué va a hacer con la nocturnidad”.

Según Reynoso, solo si se respeta la ordenanza y si el municipio cumple con mejorar la zona los boliches podrían arriesgarse a desarrollar la actividad comercial.

“Amén de que la posibilidad de modificar una ordenanza siempre existe, el municipio tendría que poner en condiciones el lugar: terminar las obras, poner iluminación, mantener el sector, hacer un sendero seguro. Y fundamentalmente hacer respetar la ordenanza vigente, que no permite que abran ni Villadora, ni República, que no haya fiestas en el puerto, en la Belgrano que no haya fiesta en ningún lado”, apuntó.

Y subrayó: “La ordenanza es muy clara. El único lugar donde se pueden desarrollar actividades bailables es en la 168 y unos que otros corredores por la costa y Gorriti en el norte de la ciudad, nada más. Todo lo otro que sucede está por fuera de la ordenanza”.

“La propuesta del 168 le dio la espalda al ciudadano y quedó para un grupo de jóvenes bastante reducido; a nosotros cuando se nos achica el segmento con el cual podemos ejercer nuestra actividad, el negocio deja de ser negocio”, concluyó.

Sobre la experiencia de los asistentes, recordó que las noches eran “muy ingratas” porque el municipio mandaba inspectores que “destrataban” a los clientes y por ende no regresaban. Además, precisó que “no solo la oferta se ha ido desmereciendo por no poder cobrar un valor aceptable de entrada, sino que las condiciones son muy malas para trabajar”.

“Hay bastante competencia porque hay cinco o seis lugares que abren y la gente dejó de ir a lugares donde les resultó incómodo, donde se los maltrató y encontró más saludable la ciudad, un bar, un pub, una fiesta puntual. Esperan en el año irse a la fiesta de disfraces en Entre Ríos, para la cual pagan lo que no pagan nunca en la ciudad, y con toda razón porque es algo nuevo y puntual”, finalizó.