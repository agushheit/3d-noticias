---
category: Agenda Ciudadana
date: 2021-11-10T06:15:30-03:00
thumbnail: https://assets.3dnoticias.com.ar/sancor1.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'SanCor: escepticismo ante los rumores de una "reactivación definitiva"'
title: 'SanCor: escepticismo ante los rumores de una "reactivación definitiva"'
entradilla: 'Crecen los rumores de un nuevo financiamiento del gobierno nacional.
  Los tamberos señalan que el principal desafío sigue siendo recuperar la confianza.

'

---
Mientras crecen los rumores en torno a la creación de un fideicomiso para la reactivación definitiva y sustentable de SanCor, entre los tamberos reina el escepticismo y la falta de información.

Fue el gremio de Atilra quien sorprendió este lunes al hacer pública una reunión, a la cual definió como "de alta significación para el futuro de la empresa y sus trabajadores". A través de un comunicado de prensa, el sindicato de los trabajadores lácteos hizo referencia a un encuentro (del cual participó) entre ministros del gobierno nacional, el presidente del Banco Nación, el presidente de SanCor (Pablo Gastaldi) y un grupo empresario-inversor.

Según Atilra, el objetivo del cónclave fue uno: "Llevar adelante el compromiso de una «reactivación definitiva y sustentable» de la Cooperativa SanCor Cul".

A pesar del comunicado gremial, ninguna autoridad gubernamental (provincial-nacional) empresaria y cooperativista se anima a realizar declaraciones públicas sobre el futuro de SanCor en el mercado de la leche.

En ese marco, los tamberos están expectantes por un nuevo espaldarazo hacia la cooperativa pero también se muestran escépticos.

En diálogo con UNO Santa Fe, el secretario de la Mesa de Productores de Leche de Santa Fe (Meprolsafe), Marcelo Aimaro, dijo que por el momento solo "hay rumores" y no hay nada oficial.

Además, agregó que observa con "buenos ojos" que se intente una recuperación de SanCor a través de la inyección de dinero (ayuda crediticia). Igualmente, advirtió: "Volver a recuperar la cooperativa sería muy bueno pero va a llevar tiempo, confianza y seriedad".

"Las expectativas están, pero hoy hay mucho escepticismo en el productor", afirmó Aimaro

Entre las seis plantas industriales de la Cooperativa, se procesan diariamente 650 mil litros de leche y la dotación de personal es de 1.700 empleados. SanCor prevé alcanzar un volumen diario promedio de producción de 1,2 millones de litros de leche, con proyección a 1,5 millones. Con este nivel productivo, la cooperativa asegura "el normal funcionamiento futuro y el pago de todas sus obligaciones".

El referente de los productores tamberos reconoció que con la última reestructuración de SanCor (2017) "se cortó la caída libre de SanCor” y señaló que la cooperativa en el último tiempo "estaba cumpliendo" con los pagos a los tamberos que le venden leche.

"Para que el productor le vuelva a vender leche a SanCor llevará un tiempo porque se tienen que demostrar dos cosas: confianza y seguridad. Lo que tiene que generar la cooperativa nuevamente es confianza, es lo primero que deberá revertir", definió el titular de Meprolsafe.

Según Atilra, en la reunión celebrada el lunes, el gobierno se mostró dispuesto a otorgar y poner a disposición "todas las herramientas de financiamiento que tenga a su alcance para logra esa reactivación sustentable. «Vamos a apoyar ese plan de negocios ambicioso para recuperar el liderazgo que tuvo esta cooperativa» sentenció el Ministro de Producción" Matías Kulfas. En el mismo documento, el gremio destaca que "el presidente del Banco Nación subrayó que «tenemos la decisión política de colaborar para llegar a buen término, vamos a encontrar un camino entre todos y comprometo el apoyo del banco para esto».

Las opciones que maneja SanCor

A través de un comunicado, publicado el 25 de octubre pasado y titulado "Todos hablan de SanCor, pero NO con SanCor", la cooperativa señaló que "para lograr mayor volumen de operatoria, es imprescindible acceder al financiamiento del capital de trabajo destinado a la mayor adquisición de materia prima e insumos".

En esa línea, señala que dicho financiamiento podría obtenerse a través de:

\- "Aportes financieros o de inversión de organismos y/o instituciones públicas y/o privadas".

\-"Apoyo gubernamental para el cobro de la deuda contraída por la República Bolivariana de Venezuela, por la provisión de leche en polvo. En este sentido, se solicitó la gestión pertinente a las autoridades nacionales, a través de la Cancillería".

\- "La constitución de un Fondo de Garantías destinado exclusivamente a la compra de materia prima e insumos".

En ese mismo documento, la cooperativa destaca que "el Plan al que se alude fue revisado y validado por consultores designados por organismos oficiales, y fue presentado a las autoridades sindicales y de los gobiernos nacional y de las provincias de Santa Fe y Córdoba".

Y añade: "Si bien la Cooperativa mantiene sus operaciones corrientes, es indispensable aumentar en el corto plazo el volumen de procesamiento de leche para evitar sobresaltos y dudas sobre sus posibilidades de éxito. Desde el inicio del proceso de reestructuración, SanCor continúa siendo una cooperativa de productores lecheros, se mantiene abierta al diálogo y a escuchar ideas y propuestas dirigidas al logro de los objetivos establecidos. En este sentido, SanCor recibirá, analizará y definirá toda iniciativa que le sea presentada y que resulte útil y beneficiosa para sus asociados y partícipes del sistema pero que, a la fecha, no hemos recibido.