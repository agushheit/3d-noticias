---
category: La Ciudad
date: 2021-10-24T06:15:25-03:00
thumbnail: https://assets.3dnoticias.com.ar/bleynat.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Habrá 134 barrios con wifi libre en Santa Fe y Rosario
title: Habrá 134 barrios con wifi libre en Santa Fe y Rosario
entradilla: La iniciativa del gobernador de la provincia, aprobada por la Legislatura,
  alcanzará además a todas las comunas y ciudades del territorio santafesino, beneficiando
  a tres millones de personas.

---
La flamante Ley de Conectividad, impulsada por el gobernador Omar Perotti como uno de los principales ejes de su gestión y sancionada por la Legislatura santafesina, posibilitará que haya 134 barrios con wifi libre en Rosario y Santa Fe; al tiempo que permitirá licitar 25 trazas de 4.400 kilómetros de fibra óptica para llegar con radioenlaces a los 365 municipios y comunas del territorio santafesino.

 Actualmente, la provincia ya licitó ese servicio en 20 barrios populares (8 de Santa Fe y 12 de Rosario), que están en proceso de adjudicación. A partir de la sanción de la ley se continuará con esos procesos licitatorios y se licitará el total de la obra.

 El monto de inversión que se destinará al ambicioso programa de conectividad asciende a 100 millones de dólares, financiado por un crédito internacional muy ventajoso. A su vez, la provincia aportará otros 24 millones de dólares.

 El plazo de ejecución estimado es de cuatro años y la intención del gobierno provincial es llamar a licitación lo antes posible, para bajar los tiempos que insumió el debate de la ley en las dos cámaras legislativas.

 El secretario de Tecnologías para la Gestión de la provincia, Sergio Bleynat, destacó que se trata de un proyecto que abarca múltiples aspectos y que permitirá, sólo en el área educativa, "la construcción de jardines, escuelas y la resignificación de la plataforma educativa y la formación docente".

 "A nivel mundial se está hablando de una unidad ciberfísica: la tecnología es parte integrante y fundamental en la vida de las personas, es una forma de conectarse", expresó el funcionario. Y agregó: "Si no se le puede brindar a las personas una forma de conectarse, quedan fuera de una realidad que se está dando a nivel mundial y que es cada vez más presente en nuestra sociedad", destacó Bleynat.

 **Igualdad de oportunidades**

 Sobre la flamante ley, el funcionario provincial destacó que "esto es igualar oportunidades en los territorios, que nadie tenga que venir a vivir al centro de una ciudad para tener un buen servicio de internet. A partir de esta nueva infraestructura que la provincia va a poner a disposición, cambiará la forma de trabajar, de poder pensar una familia, una sociedad y una comunidad", vaticinó.

 Además, el titular de Tecnologías para la Gestión del gobierno provincial recordó que "antes de asumir, veníamos trabajando con el gobernador Omar Perotti y su preocupación era el atraso tecnológico de conectividad que había en la provincia, algo que quedó demostrado en la pandemia". 

 También, Bleynat explicó que "es importante entender cuál es la concepción de la conectividad. Hoy la conectividad y la infraestructura que vamos a abordar con Santa Fe + Conectada es la construcción de muy buenos cimientos para la casa que queremos levantar para todo Santa Fe", dijo.

 "Hoy es indispensable tener video salud, hacer teletrabajo, estudiar a distancia; eso es impensable sin una buena conectividad. Y Santa Fe en ese aspecto venía muy retrasada", resaltó el funcionario. 

 Y abundó: "Santa Fe + Conectada viene a saldar esa deuda, viene a poner al Estado como un actor importante en esta materia, construyendo esa infraestructura y trabajando con las instituciones y con todos los privados que están en este rubro para mejorar el servicio en todas las localidades de la provincia".

 