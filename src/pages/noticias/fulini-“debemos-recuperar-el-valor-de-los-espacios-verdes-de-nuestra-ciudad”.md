---
layout: Noticia con imagen
author: "Fuente: Notife"
resumen: Espacios verdes
category: La Ciudad
title: "Fulini: “Debemos recuperar el valor de los espacios verdes de nuestra ciudad”"
entradilla: El concejal de Unidad Ciudadana – PJ presentó un proyecto de
  resolución para solicitarle al ejecutivo municipal que se lleven adelante
  tareas de limpieza y poda en el Parque del Sur General Belgrano.
date: 2020-11-12T13:50:39.163Z
thumbnail: https://assets.3dnoticias.com.ar/parque-sur.jpg
---
Se trata de una solicitud para que la Municipalidad de la Ciudad de Santa Fe realice un estudio de factibilidad técnica y económica, con el objetivo de encontrar soluciones profundas y duraderas frente a los grandes problemas de salubridad e higiene que han invadido el Parque Sur.

“Decidimos presentar este proyecto porque escuchamos los reclamos de la gente y porque queremos recuperar el valor de los espacios verdes de nuestra ciudad, que vuelvan a mostrar el brillo que alguna vez tuvieron. Tanto el Parque Garay como el Parque del Sur, por poner sólo dos ejemplos, son sitios emblemáticos de Santa Fe a los que acude una gran cantidad de personas, casi todos los días del año”, manifestó Fulini.

La iniciativa del edil propone que se realicen tareas de desmalezado, poda y una limpieza profunda de toda la superficie que abarca el parque que, en esta época del año con la llegada del calor, comienza a ser visitado por muchos santafesinos y santafesinas para realizar deportes, pasear a los niños, a sus mascotas o salir a distenderse.

“Creemos que es imperiosa la necesidad de llevar adelante trabajos de higiene en este lugar, porque la acumulación de malezas y residuos generan un medio propicio para atraer alimañas portadoras de enfermedades infectocontagiosas. Si hay alguna lección importante que debe dejarnos la pandemia de Covid-19 es la de tener mayores cuidados y mucha más responsabilidad con el medio ambiente, con la higiene y con nuestra propia salud”, concluyó Fulini.