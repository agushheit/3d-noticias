---
category: Agenda Ciudadana
date: 2021-03-13T06:15:09-03:00
thumbnail: https://assets.3dnoticias.com.ar/utiles.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Billetera Santa Fe: La Provincia extendió el beneficio en librerías hasta
  el 31 de marzo'
title: 'Billetera Santa Fe: La Provincia extendió el beneficio en librerías hasta
  el 31 de marzo'
entradilla: Durante todos los días, en comercios adheridos, se reintegrará el 30%.
  Además, se continúa ofreciendo una canasta escolar a precio sugerido de 20 artículos
  a 2490 pesos.

---
El Ministerio de Producción, Ciencia y Tecnología, a través de la Secretaría de Comercio Interior y Servicios, extendió hasta el 31 de marzo de los beneficios del Programa Billetera Santa Fe en comercios destinados a la venta de artículos escolares de cara al reinicio de clases en todo el territorio provincial.

Los beneficios del programa abarcan a comercios del rubro todos los días de la semana, lo que implica el reintegro del 30% y, al mismo tiempo, se continúa ofreciendo una canasta escolar a precio sugerido de 20 artículos a 2490 pesos en librerías adheridas.

La canasta a disposición incluye: Lápices de color por 12 unidades, bolígrafo, lápices de grafito, Voligoma, mapas Rivadavia Nº 3, bolígrafo Bic surtido, crayones, kit escolar 3 pinceles, témpera por 12 unidades, cuaderno tapa flexible por 42, cuaderno tapa dura araña por 42, repuesto rayado - cuadriculado por 400 hojas, repuesto hojas Nº 3 rayado - cuadriculado por 288, rapel glacé lustre x 10 hojas, papel glacé metalizado por 10 hojas, goma tinta – lápiz, tijera, marcadores escolares, carpeta escolar, lápiz corrector punta metálica, sacapuntas, mochila lisa escolar, cartuchera y cuadernillo.

**Billetera Santa Fe**

Esta herramienta permite sumar poder de compra otorgando reintegros de un 30 por ciento en la adquisición de alimentos, indumentaria, calzados, juguetería, librería, bares, restaurantes, farmacias, gastronomía y turismo, y un 20 por ciento en electrodomésticos.

Los interesados deben registrarse en www.santafe.gov.ar/billeterasantafe, para obtener un usuario y contraseña.