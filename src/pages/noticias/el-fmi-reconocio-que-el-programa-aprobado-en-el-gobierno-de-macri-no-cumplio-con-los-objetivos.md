---
category: Agenda Ciudadana
date: 2021-12-23T06:00:33-03:00
thumbnail: https://assets.3dnoticias.com.ar/FMI.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: El FMI reconoció que el programa aprobado en el Gobierno de Macri "no cumplió
  con los objetivos"
title: El FMI reconoció que el programa aprobado en el Gobierno de Macri "no cumplió
  con los objetivos"
entradilla: 'Sostuvo que "el programa salió de su curso en agosto de 2019 y la Junta
  Ejecutiva solo completó cuatro de las doce revisiones previstas"

'

---
El Fondo Monetario Internacional (FMI) reconoció hoy que el programa económico que implementó el gobierno de Mauricio Macri y por el cual recibió un crédito de 45.000 millones de dólares"no cumplió con los objetivos de restaurar la confianza en la viabilidad fiscal y externa".

"El programa salió de su curso en agosto de 2019 y la Junta Ejecutiva solo completó cuatro de las doce revisiones previstas. Las autoridades decidieron cancelar el acuerdo el 24 de julio de 2020", señaló esta noche en un comunicado tras la evaluación que realizó el Directorio del organismo.

La revisión es de carácter normativo cuando se otorgan créditos por encima de los cupos que le corresponden a cada país miembro.

"Se requiere una evaluación ex post en todos los casos de préstamos del FMI por encima de los límites normales de endeudamiento para revisar el desempeño con respecto a los objetivos originales del programa, discutir si el diseño del programa fue apropiado y evaluar si el programa fue consistente con las políticas del Fondo", dijo el organismo.