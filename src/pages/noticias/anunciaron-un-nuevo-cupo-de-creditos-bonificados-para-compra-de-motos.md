---
category: Agenda Ciudadana
date: 2021-03-18T07:41:07-03:00
thumbnail: https://assets.3dnoticias.com.ar/moto1.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Diario UNO
resumen: Anunciaron un nuevo cupo de créditos bonificados para compra de motos
title: Anunciaron un nuevo cupo de créditos bonificados para compra de motos
entradilla: El Banco Nación informó que el Programa Mi Moto ingresó en la cuarta etapa
  de financiamiento con un nuevo cupo y créditos para la adquisición de motos de fabricación
  nacional

---
El Programa Mi Moto ingresó en la cuarta etapa de financiamiento con un nuevo cupo y créditos para la adquisición de motovehículos de fabricación nacional, respaldados con tasas bonificadas, informó hoy el Banco de la Nación Argentina (BNA).

"Comenzará hoy la cuarta etapa del Programa Mi Moto, que incorpora un nuevo cupo para solicitar el crédito para la adquisición de motos de fabricación nacional, en 48 cuotas y a tasa bonificada", señaló el BNA en un comunicado de prensa.

En esta nueva etapa regirá la misma operatoria que se estableció en las ediciones anteriores: una vez preaprobado el crédito, el cliente dispondrá de 7 días corridos para reservar la moto en la Tienda BNA.

La línea de financiamiento para motos es un proyecto conjunto entre el banco y el Ministerio de Desarrollo Productivo, que aportó una bonificación de 10 puntos porcentuales anuales de la tasa de interés vigente.

El monto máximo a financiar por usuario es de $200.000, a un plazo de 48 meses, sistema de amortización francés, y alcanza a todos los usuarios, clientes o no clientes de la entidad.

Respecto a la tasa de interés, para quienes cobren sus haberes a través del BNA, será de 28,5% en tanto que para el resto de los usuarios será de 37,5%.

Por la compra de una moto de $150.000 se pagaría una cuota inicial de $ 6.019,30 en el primer caso y de $7.059,21 para el resto de los usuarios.

Las marcas y modelos de motocicletas disponibles para el programa se pueden consultar en el sitio [https://tiendabna.com.ar/mi-moto](https://tiendabna.com.ar/mi-moto "https://tiendabna.com.ar/mi-moto") y los créditos, según se aclaró, no incluyen los costos y gastos adicionales accesorios a la operatoria.

Las marcas que participan del plan son: Beta, TVS, Corven, Bajaj, Brava, Gilera, Honda, Mondial, Kymco, Keller, Motomel, Keeway, Benelli, Guerrero, Okinoi y Zanella.