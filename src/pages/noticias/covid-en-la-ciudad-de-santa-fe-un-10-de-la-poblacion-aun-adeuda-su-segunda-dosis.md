---
category: La Ciudad
date: 2022-01-25T06:00:29-03:00
thumbnail: https://assets.3dnoticias.com.ar/VACUNACION35.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'Covid: en la ciudad de Santa Fe, un 10% de la población aún "adeuda" su
  segunda dosis'
title: 'Covid: en la ciudad de Santa Fe, un 10% de la población aún "adeuda" su segunda
  dosis'
entradilla: "Del total de habitantes de esta capital, 94% recibió una vacuna y 84,5%
  ya tiene la segunda. Hay un \"desfasaje\" del 10% de esquemas incompletos. \n"

---
De acuerdo al último informe de Sala de Situación del municipio local -tercera semana epidemiológica de enero-, en toda la ciudad de Santa Fe hay un 94% de la población con una dosis de la vacuna anti Covid, y 84,5% con las dos inoculaciones. Esto quiere decir que un 10% de la población que se colocó la primera vacuna aún adeuda la segunda, es decir, el esquema completo.

 Es un porcentaje preocupante, teniendo en cuenta que casi el 38% de los muertos por esta enfermedad en la capital tenían apenas una sola aplicación, y con dos dosis se reportan desde mayo del año pasado un 9% de fallecidos. Está claro, más allá de las comorbilidades de los pacientes fallecidos -las cuales pueden agravar un cuadro clínico al contraer coronavirus-, que contar con el esquema completo genera una mayor protección contra el SARS-CoV-2.

 Los porcentajes de vacunación completa "pueden resultar buenos, pero instamos a todas esas personas con residencia en la ciudad que aún no completaron su esquema a que lo hagan, dirigiéndose a los vacunatorios disponibles o verificando la llegada del turno (a través de la página Santa Fe vacuna)", pidió en declaraciones públicas César Pauloni, director de Salud municipal. Para completar, con dosis de refuerzo hay un 25,6% de habitantes de esta capital.

 Cabe recordar que estos números se estiman sobre la población de esta capital: el gobierno local proyecta el total demográfico de Santa Fe en 426.145 personas.

  **Bajaron los casos, pero...**

 El informe advierte que en toda Semana Epidemiológica N° 3 -con corte hasta este sábado 22 de enero-, se contabilizaron 9.123 nuevos casos confirmados en la ciudad. Hubo 1.311 casos menos de Covid-19 reportados en comparación con la semana N° 2, cuando se informaron 10.434 nuevos infectados, hasta ahora el récord de esta tercera ola. Es decir, un 12,5% de disminución de las infecciones en 15 días.

 Pero estos guarismos son apenas orientativos, no absolutos: "El número de casos (reales) puede ser mayor, en virtud de las personas que no informan que están infectadas a través de la App Cuidar o del 0800-555-6549 de la provincia; y más aún considerando la modificación en los criterios sobre las personas que deben hisoparse y las que no", advirtió Pauloni.

 Asimismo, la variante predominante Ómicron "es mucho menos sintomática (afecta las vías aéreas superiores), e incluso es oligosintomática. Por lo cual, hay personas en las que puede pasar desapercibido su contagio, justamente porque no se manifiestan tanto los síntomas", dijo el director del Salud.

 Otro dato llamativo es la cantidad de personas reinfectadas (que se enfermaron al menos dos veces de Covid). En la tercera semana fueron 5.302 habitantes: un 9,5% de reinfecciones. Ese cálculo porcentual en la segunda semana era 6,9% (3.873 personas): casi un 3% más de gente se volvió a contagiar en sólo 15 días.

 **Tasa de letalidad**

 Se incrementó asimismo la cantidad de muertos por Covid en la última semana respecto de la anterior: fueron 13 las personas fallecidas con residencia en la ciudad capital. "A esto lo vimos en la curva de la pandemia allá por agosto de 2021, cuando se llegó a un número similar (en lo peor de la segunda ola de contagios). Pero más allá de ello y comparando este número de decesos con la gran cantidad de casos nuevos confirmados, la letalidad sigue bajando", agregó Pauloni.

 Hoy esa tasa es del 1% cuando durante casi toda segunda ola de contagios (con la variante Gamma con predominio) era del 1,5% o mayor. "Esa disminución en la letalidad está directamente relacionada con el nivel de vacunación en la ciudad de Santa Fe", afirmó el funcionario. Desde que comenzó la pandemia, el coronavirus se cobró la vida de 874 personas.

 **Fallecidos y vacunación**

 El informe de Sala de Situación indica otro interesante elemento a considerar: la relación entre personas muertas como consecuencia del Covid-19 y la cobertura de vacunación que éstas tenían. Así, desde mayo de 2021 (que fue el mes desde el cual se pudo ajustar el análisis de ambos indicadores) a la fecha, el 53% de los muertos no tenía ninguna dosis aplicada; el 37,3% sólo tenía una dosis (es más del 90% de los decesos desde ese mes); un 8,9% tenía las dos dosis, y un 0,6% (tres personas) contaba con la dosis de refuerzo.