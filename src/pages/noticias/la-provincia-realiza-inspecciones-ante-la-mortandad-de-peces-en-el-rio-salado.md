---
category: Agenda Ciudadana
date: 2020-12-09T11:14:01Z
thumbnail: https://assets.3dnoticias.com.ar/PECESMUERTOS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: La provincia realiza inspecciones ante la mortandad de peces en el río Salado
title: La provincia realiza inspecciones ante la mortandad de peces en el río Salado
entradilla: La bajante histórica que castiga la zona es considerada el factor determinante.
  Así lo comunicó el Ministerio de Ambiente y Cambio Climático que, junto con otros
  organismos, evalúan la situación.

---
El gobierno de Santa Fe, a través de la Subsecretaría de Recursos Naturales del Ministerio de Ambiente y Cambio Climático, comunicó el resultado de los primeros informes respecto de la mortandad de peces en el cauce del río Salado. 

En ellos se indica que la histórica bajante es una de las principales causas que genera “déficit de oxígeno”.

Al respecto, el subsecretario de Recursos Naturales, Gaspar Borra, señaló que “este fenómeno de mortandad en la cuenca del Salado inferior, cuando entra a la provincia de Santa Fe, se verifica en distintos puntos del curso”, por lo cual “la principal hipótesis que manejamos con los biólogos, es que se trata de un fenómeno natural por la bajante pronunciada histórica y extraordinaria, sumado a la poca lluvia y el calor”.

Y aclaró que, de confirmarse, es un fenómeno natural. No obstante “desde el Ministerio estamos haciendo numerosos estudios de peces y del agua para confirmar o descartar factores antrópicos, es decir, ocasionados por el hombre”.

Así, si bien subrayó que “en este caso particular no descartamos ninguna hipótesis”, también detalló que “por los estudios de los biólogos la más fuerte es el fenómeno natural” y que “estos estudios los hacen la Facultad de Ingeniería Química a través de su laboratorio.

También el Instituto Nacional de Limnología (INALI) trabaja en esto, y contamos con la colaboración de la Municipalidad de Esperanza”, mencionó.

A la espera de resultados de muestreos para determinar la causa concreta, Borra advirtió: “tomaremos todas las medidas que tenemos que tomar” en el caso de que no se trate de un fenómeno natural.