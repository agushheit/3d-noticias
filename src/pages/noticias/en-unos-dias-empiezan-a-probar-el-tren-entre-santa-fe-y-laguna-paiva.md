---
category: La Ciudad
date: 2021-07-03T06:15:06-03:00
thumbnail: https://assets.3dnoticias.com.ar/TRENES.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: En unos días empiezan a probar el tren entre Santa Fe y Laguna Paiva
title: En unos días empiezan a probar el tren entre Santa Fe y Laguna Paiva
entradilla: Ya se están estudiando cuáles serán las paradas que habrá en el trayecto
  de 39 kilómetros. La formación será de una locomotora y dos coches para 140 personas.

---
El tren de cercanías entre la ciudad de Santa Fe y la de Laguna Paiva está cada vez más cerca de concretarse y en unas semanas, aún no está definida la fecha, se realizará la primera prueba de circulación. Además, los santafesinos podrán utilizarlo para transportarse dentro de la ciudad. Por eso gran parte del trabajo que se está haciendo en la actualidad tiene que ver con el estudio de la ubicación de las paradas.

El lunes pasado hubo una reunión en la Casa de Gobierno de Santa Fe de la que participó el gobernador Omar Perotti; el presidente de Trenes Argentinos Cargas (TAC), Daniel Vispo; Martín Gainza, vicepresidente de TAC; y Sergio Sasia, secretario General de la Unión Ferroviaria a nivel nacional. En ese encuentro se hizo un repaso de todas las obras ferroviarias que se están haciendo en la provincia de Santa Fe y, en particular, de lo que refiere al tren de cercanías entre Santa Fe y Laguna Paiva, que es un trabajo en conjunto entre el Ministerio de Transporte de la Nación y la provincia.

El trayecto comprende 39 kilómetros de vías. Actualmente TAC está trabajando en el mejoramiento y puesta en condiciones de ese trayecto. Además, hay un equipo mixto entre ambas partes para definir todo lo que tiene que ver con lo operativo y la circulación que va a hacer ese servicio. "Entre otros aspectos se están estudiando las frecuencias, las paradas que va a tener el tren en esos 39 kilómetros y todas las cuestiones urbanísticas que se tienen que adaptar para el proyecto", le dijo a UNO Santa Fe Martín Gainza.

El material rodante ya está definido y constará de una locomotora y dos coches, que son los que se van a utilizar en las cuatro frecuencias diarias –dos de ida y dos de vuelta– que se tienen previstas. Eso permitiría un movimiento de entre 600 y 800 personas por día. Aún se está trabajando en definir cuántas serán las paradas (se estiman entre ocho y 10 en todo el trayecto) y dónde estarán ubicadas.

"Hay aspectos técnicos que se tienen que analizar. En las próximas semanas ya vamos a estar en condiciones de hacer una prueba de circulación, pero aún no tenemos fecha definida", aclaró Gainza quien aseguró que el proyecto ya está muy avanzado. Según las primeras estimaciones la capacidad máxima de pasajeros por viaje será de 144 personas.

Gainza explicó que el trayecto en la ciudad de Santa Fe se hará por las vías que actualmente está utilizando el transporte ferroviario y que, paralelamente, se va a hacer el circunvalar Santa Fe para que el transporte de cargas no pase por la ciudad. La parada inicial en la capital provincial estará en la esquina de Bulevar Gálvez y Vélez Sarsfield, aunque el vicepresidente de TAC aclaró que no será en la Estación Belgrano, sino en cercanías de ese predio.

El servicio lo va a prestar por Sofse Trenes Argentinos Operaciones y aún no está definido el costo del pasaje. En principio se estima que el viaje tendrá una duración de entre 1.0 horas y 1.20 horas. En cuanto a la inversión total del proyecto, Gainza especificó que son 400 millones de pesos, de los cuales el Estado Nacional aporta, a través del Ministerio de Transporte 250 millones de pesos y la provincia los 150 millones de pesos restantes.

El proyecto del circunvalar está en la etapa administrativa final para la adjudicación de las obras y eso está a cargo de Trenes Argentinos Infraestructura, la empresa nacional que tiene como objetivo administrar y ejecutar las obras ferroviarias.