---
category: Estado Real
date: 2021-01-23T08:46:59Z
thumbnail: https://assets.3dnoticias.com.ar/epe.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de la Provincia de Santa Fe
resumen: La EPE invertirá más de $250 millones en equipamiento para la prestación
  del servicio
title: La EPE invertirá más de $250 millones en equipamiento para la prestación del
  servicio
entradilla: Se conocieron las ofertas presentadas en 3 licitaciones públicas para
  adquirir transformadores y celdas de distribución secundaria.

---
La Empresa Provincial de la Energía (EPE) abrió los sobres con las ofertas de 3 licitaciones públicas, para comprar 216 transformadores de distribución de distinta potencia, 12 transformadores de subtransmisión y 46 celdas de distribución secundaria, en el marco del plan anual de equipamiento. El presupuesto global es de más de $250.000.000.

Al respecto, el titular de la EPE, Mauricio Caussi, afirmó que “durante este mes estamos abriendo un importante número de licitaciones para reforzar la prestación del servicio eléctrico en las distintas regiones de la provincia, a través de gestiones para construir nuevas obras e incorporar equipamiento en los distintos niveles de tensión”.

**Ofertas**

Para la compra de 216 transformadores, presentaron ofertas 7 empresas: Tubo Trans Electric SA, por $ 144.155.472,54; Anif SA, por $ 189.297.663,62; Fohama Electromecánica SA, por $ 168.468.311,14; Inelpa Transformadores SA, por $ 153.597.609,51; Mayo Transformadores SRL, por $ 222.100.425,09; Argeltra SA, por $ 218.329.677,80; y Tade Czerweny SA, por $ 233.973.531,13. El presupuesto oficial es de $ 146.300.257,19.

En tanto, para la adquisición de 12 transformadores de subtransmisión, presentaron ofertas las firmas Cotizaron Tubos Trans Electric SA, por $ 54.546.992,87; Artrans SA, por $ 77.146.795,70; Fohama Electromecanica SRL, por $ 89.881.538,83; Mayo Transformadores SRL, por $ 83.183.693,52; Argeltra SA, por $ 101.435.047,47; Tadeo Czerweny SA, por $ 82.968.048,94; y Vasile y Cía SA, por $ 80.032.166,18. El presupuesto oficial es de $ 60.206.227,17.

Por último, para la compra de celdas de distribución secundaria, cotizaron algunos o todos los ítems, las firmas ABB, por $ 23.916.677,96; Tipem SA, por $ 24.406.009,19; y Schneider Electric Argentina SA, por $ 39.618.155,04. El presupuesto oficial es de $ 44.056.898,20.

Equipos interdisciplinarios analizarán las propuestas técnicas y económicas de cada licitación y elevarán los informes correspondientes, para la adjudicación de los trabajos.