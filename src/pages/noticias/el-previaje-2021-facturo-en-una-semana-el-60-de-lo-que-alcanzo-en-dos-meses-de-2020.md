---
category: Agenda Ciudadana
date: 2021-09-21T06:00:57-03:00
thumbnail: https://assets.3dnoticias.com.ar/previaje.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: El Previaje 2021 facturó en una semana el 60% de lo que alcanzó en dos meses
  de 2020
title: El Previaje 2021 facturó en una semana el 60% de lo que alcanzó en dos meses
  de 2020
entradilla: Según precisó el subsecretario de Promoción Turística, Andrés Krimer,
  la carga de facturas de servicios enmarcados en el programa superó en su primera
  semana los 6.300 millones de pesos.

---
La carga de facturas de servicios enmarcados en el programa Previaje II superó en su primera semana los 6.300 millones de pesos, lo que representa el 60% del total de la edición 2020, que se extendió durante dos meses, afirmó este lunes el subsecretario de Promoción Turística, Andrés Krimer.

El funcionario dijo a Télam Radio que tras los informes del sector privado, con varias empresas que quintuplicaron su facturación, al comenzar la carga de tickets la semana anterior, "el lunes y martes tuvimos un colapso bastante importante en la página de cargas, por la cantidad de visitas por minuto que tenía".

"Al día de hoy, tenemos más de 6.300 millones de pesos cargados", precisó Krimer y señaló que "Previaje uno duró desde octubre hasta diciembre, y nosotros hicimos el 60% en una semana de carga y tenemos también 122.000 usuarios ya inscriptos que han cargado comprobantes, con un fuerte movimiento para el mes de noviembre, que era uno de los objetivos".

El subsecretario explicó que "noviembre es un mes de baja en general, no es un mes con mucho movimiento y nos pone muy contentos poder haber movido la temporada, adelantarla, y que noviembre ya sea un mes con mucho movimiento, porque por supuesto el sector lo necesitaba muchísimo".

Sobre las provincias más elegidas, señaló que son Río Negro, Buenos Aires, Salta y Mendoza, y destacó que "hay una gran distribución federal, igual que pasó en Previaje uno, la lógica del programa permite a los argentinos llegar a todos los rincones del país", con lo que se distribuye este aporte del Estado nacional entre todas las provincias.

También señaló que según datos del sector privado hay también un importante movimiento en alojamiento debido al programa Previaje, con "destinos que ven su capacidad saturada ya", aunque aclaró que "a medida que avanzamos en el tiempo hay más disponibilidad"

"Pero después de la realidad que ha pasado el turismo en la historia a nivel mundial, no solamente en la Argentina, nos pone muy contento ver que esté pasando esto: una gran demanda de pasajes domésticos y también una gran cantidad de reservas en destinos de todo el país", añadió.

Tras mencionar que "gracias al avance de la vacunación en el país estamos en una baja histórica de casos", sostuvo que se mantienen todas las medidas sanitarias, aunque "está comprobado por la temporada de verano que tuvimos que el turismo se puede realizar sin grandes riesgos, y por los fines de semanas largos, que superan el promedio de 2019".

También adelantó que luego del éxito observado en la primera semana del Previaje II, "va a haber definitivamente una ampliación, porque además es una herramienta para diversificar economías regionales que dependen del turismo".

Sobre el turismo extranjero, que no aplica para este programa, comentó que trabajan "sobre opciones para estimular el turismo receptivo, que también es importante para el país por el ingreso de divisas y por el movimiento que genera el sector en todo el país, pero es una iniciativa que estamos trabajando por separado de Previaje".

Krimer puntualizó que los compradores "tienen hasta el 26 de septiembre para cargar las facturas correspondientes a los viajes de noviembre, y ya pueden comprar y cargar facturas desde diciembre en adelante y pueden utilizar el programa para viajar en cualquier momento del año próximo".