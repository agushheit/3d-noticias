---
category: Agenda Ciudadana
date: 2021-10-16T06:15:14-03:00
thumbnail: https://assets.3dnoticias.com.ar/CANNABIS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: El 75,5% de los usuarios de cannabis medicinal redujeron el consumo de fármacos
title: El 75,5% de los usuarios de cannabis medicinal redujeron el consumo de fármacos
entradilla: " En el 98% aseguró haber mejorado su calidad de vida, aunque sólo dos
  de cada diez tuvo un médico que lo acompañara en este proceso."

---
La Primera Encuesta Nacional de Personas que Usan Cannabis, que se presentó este viernes, reveló que el 75,5% de los personas que tratan algún problema de salud con cannabis medicinal logró reducir el consumo de fármacos y casi la totalidad (el 98%) aseguró haber mejorado de este modo su calidad de vida, aunque sólo dos de cada diez tuvo un médico que lo acompañara en este proceso.  
  
El relevamiento, realizado por la revista especializada THC y el Centro de Estudios de la Cultura Cannábica Argentina (Cecca) con la Licenciatura de Historia de la Universidad Nacional de Quilmes (UNQ), mostró que un cuarto de los usuarios de cannabis acceden a este producto a partir del autocultivo y en casi el 90% de los casos lo hicieron sintiendo miedo a ser perseguidos judicialmente al menos alguna vez.  
  
![](https://www.telam.com.ar/advf/imagenes/2021/10/616a021567806_1004x1657.jpg)"La gran mayoría de las personas que hacen un uso medicinal reporta que les mejoró bastante la calidad de vida y que como consecuencia de esto bajó el uso de otros fármacos", dijo a Télam el sociólogo Emiliano Flores, director del estudio e investigador de Cecca.  
  
Al comparar este dato con los propósitos del uso recreativo, el investigador destacó que ambos grupos tienen en común "la búsqueda de bienestar, que es lo que mejor resume por qué la gente se acerca al cannabis" en general.  
  
Es que casi 8 de cada 10 usuarios recreativos consumen marihuana para compartir un momento con amigos y por placer pero también lo hacen "para relajar", "para mejorar el ánimo" y "para relajar el cuerpo", entre otros motivos que reveló el estudio.  
  
Respecto al escaso acompañamiento de profesionales médicos que tienen quienes hacen un uso medicinal, Flores aseguró que eso ocurre porque "hay pocos profesionales de la salud que están trabajando cannabis y porque se llega a ser usuario partir del ingreso a redes de usuarios" y no por consejo médico, a los que se acude en un segundo momento.  
  
"Todo esto puede estar cambiando a partir de la creación del registro de autocultivadores y porque cada vez más se suman más profesionales y hoy existen cursos de perfeccionamiento y posgrado para personas del campo de la salud", dijo.  
  
Presentado en el marco de la Expo Cannabis que comenzó este viernes en La Rural y se extenderá hasta el domingo, el informe está basado en una encuesta online respondida por 64.646 personas de entre 16 y 92 años de todas las provincias, que usaron cannabis al menos una vez en el último año.  
Entre otros datos, el estudio mostró que a pesar de que el 75% de los usuarios accede al cannabis de formas diferentes al autocultivo (48,2% lo compra, 17,2% por regalo y 8% por cultivo colectivo), a 9 de cada 10 le gustaría tener sus propias plantas.  
  
En cuanto a los motivos que los lleva a desistir de esta idea, el 72,9% apuntó el temor a conflictos legales, con vecinos o con familiares.  
  
En orden de importancia, los otros factores que desincentivan esta práctica son falta de espacio (9,8%), falta de semillas (9,7%) y falta de conocimientos (7,6%).  
  
En esa línea, casi un tercio de quienes usan cannabis reportó haber tenido algún conflicto con las instituciones en virtud de este hábito y dentro de este subgrupo, el 80% fue demorado sin condena y uno de cada 10 fueron detenidos o detenidas también sin que nunca se les dictara una sentencia condenatoria.  
  
**Edades de los usuarios cannábicos**

Por otro lado, cuanto más jóvenes son las personas que consumen cannabis en cualquiera de sus formas, mayor es la incidencia de los problemas legales: los centennials (16 a 24 años) son quienes más reportan conflictos que afectan al 34,55% de este grupo, seguidos por los millennials (25 a 39 años) con un 28,2%, la generación X (40 a 54 años) con un 19,4%, los boomers (55 a 70 años) con 10,3% y por último los adultos mayores con 4,5%.  
**La forma de acceso, por otro lado, varía con la edad, género y propósito de uso.**  
"Los que hacen uso medicinal son los que más autocultivan pero a su vez el autocultivo está más masculinizado y es más frecuente entre la generación X, mientras que la compra en el mercado gris es más común entre los centennials y adultos mayores", detalló.  
  
Como podía preverse, el 82,4% de las personas que usan cannabis lo hacen principalmente de manera recreativa, mientras que el 17,6% lo consume con fines medicinales; ya sea para tratamiento de afecciones propias, de un tercero o de una mascota.  
  
"Una de las cosas que queda en evidencia es que se trata de un fenómeno trasversal a la sociedad, porque encontramos todos los usos en todas las provincias y público de diferentes edades usando el cannabis con diferentes fines, con presencia de todos los géneros y niveles de ingreso", afirmó.  
**Entrecruzamiento de variables y datos**

Por otro lado, el entrecruzamiento de datos sobre tipo de usos secundarios del cannabis en un gráfico demuestra que "hay una proporción importante que lo usa con dos o tres fines" e incluso hubo "entre 700 y 800 que respondieron que hacen los cuatro usos posibles" al mismo tiempo.  
  
En cuanto a la frecuencia de uso, aumenta con la edad y entre los usuarios medicinales: ocho de cada 10 adultos mayores que utilizan cannabis lo hacen todos los días, pero el 20% de los centennials lo hace una vez por mes o una vez por año. En tanto, mientras 5 de cada 10 usuarios medicinales consumen cannabis diariamente, entre los recreativos ese porcentaje disminuye al 38,3%.  
  
"Este es un dato interesante porque la frecuencia de uso se suele tomar como indicador de consumo problemático sin relacionarlo con otras variables, pero cuando uno ve quiénes son los que más usan, por edad resulta que son los adultos mayores y por uso, resulta que es el medicinal", dijo.  
  
El sociólogo explicó que la encuesta realizada entre el 11 de noviembre y 11 de diciembre de 2020 se planteó el objetivo de producir un estudio que empiece a subsanar en algo "la falta de datos empíricos".  
  
"Cuando queríamos abordar de forma comprensiva el fenómeno del cannabis en la Argentina, nos encontrábamos con que los datos son muy pocos, tienen un enfoque epidemiológico que hace hincapié en el daño o el riesgo de tomar contacto con la sustancia y no están actualizados", señaló.  
  
En este punto, el estudio mostró que sólo 3,3% de los encuestados considera que tiene una relación problemática con el cannabis, ya sea porque le trae problemas familiares, legales, con el estudio o efectos no deseados.  
  
Pero 98,9 % está en desacuerdo con su prohibición "con diferentes matices", porque hay quienes están de acuerdo con la legalización de todas las drogas ilegales (31%), quienes solo están de acuerdo con regular el cannabis (38,9%) y quienes creen que la prohibición está mal pero primero hay que avanzar con educación y salud (28,9%).