---
category: La Ciudad
date: 2021-04-16T08:29:10-03:00
thumbnail: https://assets.3dnoticias.com.ar/berutti.png
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad de Santa Fe
resumen: La Municipalidad licitó obras históricas para calle Beruti
title: La Municipalidad licitó obras históricas para calle Beruti
entradilla: Se trata de un trabajo en dos etapas simultáneas que se extenderán por
  más de 20 cuadras. El monto total de inversión es de $ 234.119.571,13. El plazo
  de ejecución es de 8 meses.

---
La Municipalidad de Santa Fe llevó a cabo este jueves por la mañana el acto de apertura de sobres donde se conocieron las ofertas económicas para iniciar una obra fundamental para el noroeste de la ciudad. Se trata de los trabajos de pavimento y desagües de calle Beruti en una extensión superior a las 20 cuadras, una obra largamente esperada por los vecinos y vecinas de ese sector urbano y que el intendente Emilio Jatón tomó entre las prioridades de su gestión.

“Poder avanzar con esta obra es un acto de justicia para todos los barrios de esta parte de nuestra ciudad y también es cumplir con la palabra empeñada por el intendente, con quien trabajamos para integrar Santa Fe”, dijo la secretaria de Obras y Espacio Público de la Municipalidad, Griselda Bertoni, quien encabezó la licitación que contó con tres oferentes.

El monto total de inversión es de $ 234.119.571,13 con financiamiento provincial (decreto N° 2360 de diciembre pasado). En tanto, la Municipalidad asume a su cargo el diseño, la contratación, la inspección y el mantenimiento de la obra.

En esa línea, Bertoni repasó: “Todos los que circulan por el noroeste saben que el primer tramo de Beruti es de una densidad de tránsito y de personas impresionante, con una infraestructura que no está a tono con esas necesidades. Por eso se licitaron estos trabajos que se realizarán en dos tramos (que se concretarán en forma simultánea) e incluyen una importante obra de desagües hídricos, cordón cuneta, pavimento e iluminación. Beruti es una arteria que tiene una acometida que va casi hasta el reservorio del Oeste, es una avenida muy importante. Así que esta licitación es muy positiva”.

Cabe destacar que los plazos de ejecución para ambos tramos son de entre seis y ochos meses. “La evaluación de las propuestas económicas que conocimos hoy se concretará antes de los 30 días. Son tres firmas reconocidas de la ciudad, con propuestas económicas muy cercanas al presupuesto oficial, así que estimamos que a fines de mayo o principios de junio, se podrán iniciar las obras”, anticipó Bertoni.

Los trabajos se enmarcan en el Plan Integrar del municipio, que es el eje del plan de gobierno para la ciudad de Santa Fe. En el caso de calle Beruti, se trata de una obra que hace décadas esperaban los vecinos y ahora está pronto a convertirse en realidad, proporcionando una mayor calidad de infraestructura urbana a un sector postergado, donde es necesario profundizar la prestación de servicios públicos de calidad.

**Ofertas económicas**

La licitación pública de pavimento y desagües de calle Beruti se divide en dos lotes: Lote 1,  etapa I (con un presupuesto oficial de $ 92.445.180,69) , entre Arzeno y Camino Viejo. En tanto, el lote 2: etapa II (con un monto de $ 141.674.390,44), entre Camino Viejo y Furlong.

En tanto, las ofertas económicas que se presentaron pertenecen a las firmas: Coemyc S.A. que, para el lote 1, ofertó $ 103.762.509,36; y para el lote 2: $ 162.922.103,96. Cabe remarcar que la firma Coemyc S.A. presentó una oferta alternativa en caso de resultar adjudicataria de ambos lotes según el siguiente detalle: Lote 1: $ 97.536.758,80. Lote 2: $ 153.146.777,72.

La segunda propuesta económica pertene a la firma Mundo Construcciones. Lote 1: $ 110.772.276,33; Lote 2: $ 153.489.993,66. En este caso, también hubo una oferta alternativa en caso de que se le adjudiquen ambos lotes con un descuento del 12,11 por ciento, según el siguiente detalle: Lote 1: $ 97.357.753,67; Lote 2: $ 134.902.355,43.

La tercera oferta pertenece a Wilkelmann S.R.L.: Lote 1: $ 98.195.523,47; Lote 2: $ 149.951.172,17. En este caso, la empresa no ofreció propuestas alternativas.

**Detalles de las etapas**

Cabe destacar que la obra incluye dos etapas que se llevarán a cabo de la siguiente manera:

–          Etapa I, entre Arzeno y Camino Viejo: en el tramo de Beruti entre Arzeno y Cafferata se completará el bacheo. Entre Cafferata y Camino Viejo, en tanto, se realizarán trabajos de pavimento de hormigón de 10 metros de ancho junto con los desagües pluviales.

–          Etapa II, entre Camino Viejo y Furlong: en el tramo de Beruti entre Camino Viejo y pasaje Geneyro se realizará un pavimento de hormigón a dos calzadas, de 7 metros, con un cantero central de 4 metros y desagües pluviales. Por otra parte, en el tramo que se extiende entre pasaje Geneyro y Furlong, se demolerá la carpeta asfáltica existente y se colocará un nuevo pavimento de hormigón, completando con los correspondientes desagües pluviales.

Las obras resultan de suma trascendencia para los vecinos y vecinas de la zona, ya que implicarán mejoras sustanciales para su calidad de vida. Calle Beruti constituye una conexión directa entre la avenida Blas Parera y varios barrios del noroeste, por lo que es fundamental optimizar la circulación.

Asimismo debe tenerse en cuenta que actualmente, cuando se registran precipitaciones medianamente importantes, se observan anegamientos en las zonas más bajas. Por lo tanto se hace primordial ejecutar desagües que se conviertan en una solución a los inconvenientes causados por el deficiente escurrimiento superficial.

Las tareas mejorarán también la movilidad urbana y garantizarán la circulación del transporte público de pasajeros en su recorrido habitual, aún en días con condiciones meteorológicas adversas. En ese marco, se pavimentarán tramos que actualmente carecen de infraestructura vial, mientras que se repararán los sectores que así lo requieran. La intención es que la calzada permita un flujo de tránsito mayor y más seguro, con una estructura de pavimento adecuada para el volumen de tránsito que circula actualmente.

**Desagües pluviales**

En lo que refiere a desagües pluviales, la iniciativa se enmarca en el Plan Director que lleva adelante la Secretaría de Asuntos Hídricos y Gestión de Riesgo de la Municipalidad.

Para el caso de la Etapa I, se beneficiarán los habitantes de las vecinales Juana Azurduy, Del Tránsito y Estanislao López, donde residen más de 10.000 personas, según datos proporcionados por el Indec en el año 2010. En lo que respecta a la Etapa II, los trabajos se localizan en Los Troncos, que cuenta con 5.428 habitantes.

En ambos casos, se trata de un área de la capital provincial que cuenta con establecimientos educativos varios, entre ellos, un jardín municipal. Además, hay comercios de proximidad de diversos rubros, un centro de atención primaria de la salud y un club social y deportivo.