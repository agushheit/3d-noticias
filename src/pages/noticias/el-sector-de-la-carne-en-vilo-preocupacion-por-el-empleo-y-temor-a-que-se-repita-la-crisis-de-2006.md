---
category: Agenda Ciudadana
date: 2021-05-22T06:00:03-03:00
thumbnail: https://assets.3dnoticias.com.ar/CARNE.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: INFOBAE
resumen: 'El sector de la carne, en vilo: preocupación por el empleo y temor a que
  se repita la crisis de 2006'
title: 'El sector de la carne, en vilo: preocupación por el empleo y temor a que se
  repita la crisis de 2006'
entradilla: La Federación de Sindicatos de Trabajadores de la Carne y Afines de la
  República Argentina reclamó  herramientas necesarias para sostener la totalidad
  de los puestos de trabajo en los frigoríficos exportadores”.

---
Los gremios de la carne expresaron su preocupación por el alcance que tendrá la decisión anunciada por el presidente Alberto Fernández de frenar la exportación de carne vacuna por 30 días para atender la suba en el precio en góndola. Para los sindicatos, la medida no tendrá efecto pleno sobre los valores de mercado doméstico, y por ello solicitaron a la Casa Rosada se contemple el parate que le impondrá a la actividad, que mueve a más de 70 mil trabajadores en los frigoríficos, para quienes –entienden- debe haber un plan de contingencia.

Gabriel Vallejos, secretario general de la Federación de Sindicatos de Trabajadores de la Carne y Afines de la República Argentina (FESITCARA), pidió a la administración nacional que “se arbitren las herramientas necesarias para sostener la totalidad de los puestos de trabajo en los frigoríficos exportadores que verán afectada en su nivel de actividad durante el alcance de la medida”.

Desde el gremio, que cuenta con 15 mil afiliados que desarrollan su actividad en frigoríficos exportadores de la Ciudad Autónoma de Buenos Aires y en las provincias de Buenos Aires y Santa Fe, se plantea el interés por conocer qué alcance tendrá la medida, motivo por el cual desde el gremio se pidió sendas reuniones con funcionarios del Ministerio de Trabajo de la Nación, y también con la Secretaría de Comercio de la cartera de Desarrollo Productivo.

“En las empresas afectadas y en los frigoríficos que exportan se habla de lo mismo: hacer faena hasta el lunes o martes próximo, y luego ver el alcance de la medida. Esto nos deja muy preocupados. Si bien la medida contempla las necesidades de los consumidores y de la mesa de los argentinos, desde el sindicato esperamos que también se contemple las necesidades de continuar con los puestos de trabajo”, comentó Vallejos.

Respecto al impacto que podría tener la medida oficial sobre el precio final de la carne que se paga en las góndolas y carnicerías locales, el referente del gremio cárnico aseguró que “creemos que el precio de la carne se puede amesetar, pero no creemos que sea la solución definitiva. Por otro lado, no solo la carne está subiendo, también lo hacen otros alimentos. No creemos que sea la solución definitiva cerrar la exportación”.

Para el gremio, la primera señal de alarma se registró en un frigorífico de Casilda que decidió paralizar la faena y su operatoria hasta el 30 de mayo próximo a la espera de lo que se disponga desde el gobierno nacional. “En las empresas se habla de lo mismo: estamos muy preocupados”, reiteró Vallejos. También se sumó la crisis por la que atraviesa el Frigorífico Logros, principal exportador de carne de Córdoba, que evalúa suspensiones y recorte de horas extras.

**Alerta**

Consultado sobre la visión que se tiene desde el ámbito gremial sobre medidas similares adoptadas durante la gestión del kirchnerismo, el titular de la Federación comentó: “No dejamos de alarmarnos. Entre los años 2006 al 2008, no quedó una buena referencia de lo que se aportó como receta. Ya que allí se perdieron unos 15 mil puestos de trabajo. Por ello iremos viendo el alcance general de las medidas. Estamos preocupados y queremos llevar tranquilidad a nuestros afiliados. Hoy hay incertidumbre en la industria y no queremos perder puestos de trabajo”.

Según Vallejos, desde hace dos años se logró una reactivación en la industria frigorífica local en la actividad de faena de la carne vacuna a partir del incremento en la exportación: hecho que se tradujo en un mayor nivel de empleo en frigoríficos de entre el 20 al 30% y la concreción de ventas cárnicas por casi 2.800 millones de dólares, solamente durante el 2020. Esto también posibilitó que hoy estén contratados en la industria unos 70 mil trabajadores.

“Esperamos por ello que la medida adoptada no pase de los 30 días”, comentó Vallejos al referirse al impacto que tendría una medida como el freno a la exportación para el sector cárnico local.