---
category: La Ciudad
date: 2021-04-24T06:00:01-03:00
thumbnail: https://assets.3dnoticias.com.ar/FER´PEROT.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Nación y Santa Fe acordaron obras para la ciudad
title: La Nación y Santa Fe acordaron obras para la ciudad
entradilla: 'En las próximas semanas se van a reunir con el ministro de Obras de Nación,
  Gabriel Katopodis, quien estará a cargo de acelerar los trabajos. '

---
Este viernes, el intendente Emilio Jatón participó en Rosario de un acto encabezado por el presidente de la Nación, Alberto Fernández, en el cual se anunciaron inversiones para toda la provincia. En ese contexto, llegó el compromiso para mejorar la planta potabilizadora de agua, una obra que forma parte de las prioridades de la ciudad porque mejorará la calidad del servicio de miles de vecinos y vecinas.

Del encuentro en la Sede de la Gobernación en Rosario también participaron el gobernador Omar Perotti; el intendente rosarino, Pablo Javkin; e integrantes del Gabinete nacional, entre otras autoridades. Los anuncios se produjeron en el marco del programa Capitales Alternas, que establece la realización de sesiones entre el presidente y sus funcionarios en cada una de las provincias argentinas.

Concluido el encuentro, Jatón aseguró que “siempre es bueno participar de una reunión del Gabinete nacional, sobre todo cuando uno tiene todos los ministros y puede trasladarles las necesidades de la ciudad”. En ese sentido, aseveró que “veníamos trabajando en muchos aspectos y tuvimos algunas respuestas”. Por caso, valoró que el Presidente le haya confirmado una obra para mejorar el servicio de agua potable en Santa Fe.

“Esto es el fruto del trabajo de muchos meses y tener respuestas positivas para nosotros es muy importante. Trabajar en forma coordinada con la Nación es muy importante”, afirmó el intendente. A su vez, dijo que en los próximos días se anunciará oficialmente la llegada de ministros nacionales a la ciudad “para cerrar convenios y eso es lo positivo que logramos en el día de hoy”.

Jatón remarcó que los y las vecinas de Santa Fe están muy interesadas en que los gobiernos resuelvan problemas concretos más allá de cualquier diferencia política y se comprometió a seguir defendiendo los intereses de la ciudad. “Veníamos pidiendo hace meses la posibilidad de empezar a ampliar la planta potabilizadora de agua de la ciudad y tuvimos una respuesta en ese sentido” agradeció el intendente.

Fernández reconoció que había una “deuda” con la ciudad de Santa Fe, en relación a “mejorar la planta potabilizadora” y aseguró que “nuestro ministro de Obras Públicas, Gabriel Katopodis, se pondrá en contacto para llevar adelante esas obras lo antes posible porque sabemos que Santa Fe lo necesita. Es parte de esas obras que muchas veces la gente no ve y son tan importantes para el desarrollo de una comunidad”, mencionó el Presidente.

**Otros anuncios importantes**

El Presidente de la Nación también informó que, en el marco de la lucha contra el COVID-19, el hospital de campaña del Ejército Argentino se instalará en el Liceo Militar, frente al Hospital José María Cullen, con el objetivo de “ayudar a los tratamientos médicos, la vacunación, el testeo y todo lo que haga falta para que en Santa Fe aliviemos también el problema”.

Sobre ese tema, Jatón valoró la decisión porque “era un pedido de los médicos, una necesidad que le reiteramos durante la semana al presidente y hoy nos respondió públicamente”. Se trata de una medida que ayudará a descomprimir la saturación del servicio de salud en la región, ya que Santa Fe recibe pacientes de distintas localidades de su zona de influencia.

Por otra parte, entre las novedades que beneficiarán a la ciudad capital, el Presidente -además- anunció la firma de un convenio para la prestación de servicios ferroviarios de pasajeros entre las ciudades de Santa Fe y Laguna Paiva, con una inversión de 400 millones de pesos. Los próximos días se analizará el proyecto fino de la propuesta, que incluye otras obras específicas sobre la traza del tren.

Del mismo modo, Fernández confirmó trabajos en el Puerto de Santa Fe con una inversión de 200 millones de pesos. Se trata de tareas que permitirán incrementar la capacidad operativa del sitio elevador de granos, a través de la adquisición del equipamiento necesario, con lo cual se dio el visto bueno a algunos planteos que habían formulado sectores de la producción.

Finalmente, el gobernador Perotti se mostró complacido por la presencia del presidente y sus ministros: “Esta forma de estar en cada provincia permite cumplir compromisos de campaña y darle contenido efectivo a la palabra federalismo”. Además, agradeció “los aportes a la provincia de Santa Fe” y afirmó que “lo que Santa Fe recibe lo potencia y lo devuelve multiplicado”.