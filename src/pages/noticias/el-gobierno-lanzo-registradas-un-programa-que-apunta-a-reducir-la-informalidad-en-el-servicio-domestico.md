---
category: Agenda Ciudadana
date: 2021-09-28T06:15:45-03:00
thumbnail: https://assets.3dnoticias.com.ar/REGISTRADAS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: El Gobierno lanzó "Registradas", un programa que apunta a reducir la informalidad
  en el servicio doméstico
title: El Gobierno lanzó "Registradas", un programa que apunta a reducir la informalidad
  en el servicio doméstico
entradilla: El Estado nacional pagará una parte del sueldo de la trabajadora durante
  seis meses, mientras la parte empleadora deberá registrarla y pagar sus aportes,
  contribuciones, ART y el porcentaje del sueldo restante.

---
El gobierno nacional puso formalmente en marcha el Programa "Registradas", que busca incentivar la registración de empleadas de casas particulares a través de distintos beneficios, entre los que se destaca el pago de hasta el 50 por ciento del salario de las nuevas trabajadoras durante un plazo de seis meses.

El anuncio se realizó este lunes al mediodía luego de que la ministra de las Mujeres, Elizabeth Gómez Alcorta; los ministros de Economía, Martín Guzmán y de Trabajo, Claudio Moroni; el presidente del Banco Nación, Eduardo Hecker, y la titular de AFIP, Mercedes Marcó del Pont, mantuvieran una reunión con el presidente Alberto Fernández.

Según se explicó oficialmente, la inscripción al programa permanecerá abierta durante octubre, noviembre y diciembre a través del sitio web de la AFIP y se aclaró que, para el trabajador, el beneficio no es contradictorio con la Asignación Universal por Hijo, la Asignación por Embarazo, el Progresar, el Potenciar Trabajo o la Tarjeta Alimentar.

**Lanzamiento del Programa Registradas**

La medida implica la transferencia por parte del Estado nacional de una suma por mes equivalente al 50 o 30 por ciento de la remuneración neta mensual declarada por el empleador al momento de la inscripción al programa.

En diálogo con la prensa acreditada en Casa Rosada, Gómez Alcorta recordó que la medida apunta a un sector "en el que una de cada cinco mujeres del país trabaja y donde cayó la contratación porque hubo un impacto muy fuerte durante la pandemia" de coronavirus.

La funcionaria señaló que el Estado nacional pagará una parte del sueldo de la trabajadora (por un monto de hasta 15.000 pesos) durante seis meses, mientras la parte empleadora deberá registrarla, pagar sus aportes, contribuciones, ART y el porcentaje del sueldo restante, siempre comprometiéndose "a mantener la ocupación durante cuatro meses más luego de concluir" el plazo del Programa, explicó.

Tampoco se podrá despedir a una trabajadora registrada para volverla a contratar y el salario deberá depositarse obligatoriamente en una cuenta bancaria.

El aporte del Estado será de un 50 por ciento de la remuneración neta mensual mínima cuando "la parte empleadora tengan un ingreso bruto mensual inferior al 70 por ciento del mínimo no imponible del Impuesto a las Ganancias que actualmente es de 117.374 pesos".En cambio, será de un 30 por ciento, cuando la parte empleadora cuente con un ingreso bruto mensual que esté comprendido entre el 70 y el 100 por ciento del mínimo no imponible.

Señaló Gómez Alcorta que, hasta ahora, los empleadores "sólo tenían como incentivo para la registración la deducción de los costos de Ganancias" y que, por ello, esta es "una medida progresiva que apunta a la clase media" que no es alcanzada por ese impuesto y "donde muchos, por el impacto de la pandemia, han tenido que restringir esas contrataciones".

"Por eso nosotros queremos apoyar y dar un alivio a esas familias de clase median que en su organización al reactivarse la vida y de las actividades laborales y escolares, necesitan esos apoyos de la economía del cuidado", subrayó Gómez Alcorta.

**Cómo acceder al programa**

Para poder acceder a la política, la parte empleadora deberá percibir ingresos mensuales inferiores al mínimo no imponible del Impuesto a las Ganancias, y será la responsable de afrontar el porcentaje restante del sueldo, los aportes, las contribuciones y de la cuota de la ART de la trabajadora inscripta o del trabajador inscripto, según se informó oficialmente.

En el caso de la trabajadora, deberá trabajar como mínimo 12 horas semanales y podrá estar inscripta únicamente bajo la nómina de un empleador o empleadora. La medida se aplicará a aquellas relaciones laborales nuevas con personal de casas particulares, y el empleador o empleadora deberá mantener el puesto de trabajo los cuatro meses posteriores a la finalización del beneficio.

Cuando el empleador o empleadora dé el alta a la nueva trabajadora en la AFIP, el Estado creará una cuenta sueldo en el Banco de la Nación Argentina para ella y transferirá allí, de manera mensual, un porcentaje del salario neto declarado (30 o 50 por ciento según corresponda), por un monto de hasta 15.000 mensuales.

El porcentaje del sueldo que paga el Estado dependerá del ingreso mensual de las y los empleadores.

Además, para poder solicitarlo, durante los 12 meses anteriores a la entrada en vigencia del decreto, el empleador o empleadora debe haber percibido en promedio mensual ingresos brutos de cualquier naturaleza iguales o inferiores al mínimo no imponible del Impuesto a las Ganancias, es decir 175 mil pesos.

Por su parte, Martín Guzmán resaltó la necesidad de la medida, al explicar que "la pandemia ha sido desigualadora en todo el mundo, y ha afectado especialmente a la situación económica de las mujeres".

Frente a ese escenario, el ministro reivindicó "el rol activo del Estado" que "es fundamental para atacar estas desigualdades y el esquema de políticas públicas que venimos adoptando tiene como una de sus bases esa premisa".

A su turno, Marcó del Pont dijo que se trata al mismo tiempo de "una política de ingresos, de formalización y de bancarización e inclusión financiera", ya que "automáticamente una vez que se tenga un mes de recibo de sueldo el Banco Nación le va dar una tarjeta de crédito que va a tener un límite que se va a similar a un Salario Mínimo Vital y Móvil de hasta 32 mil pesos".

Fuentes oficiales comentaron que el Gobierno nacional que sea un mínimo de 90.000 trabajadoras del servicio doméstico las que sean alcanzadas por el plan, y que la aspiración "de máxima" es recuperar los 300.000 empleos que se perdieron en la pandemia.

![](https://assets.3dnoticias.com.ar/INFOGRAFIA.jpg)