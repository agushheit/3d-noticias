---
category: La Ciudad
date: 2021-12-18T06:15:08-03:00
thumbnail: https://assets.3dnoticias.com.ar/nope.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Presupuesto 2022 rechazado: cómo votaron los diputados de Santa Fe'
title: 'Presupuesto 2022 rechazado: cómo votaron los diputados de Santa Fe'
entradilla: 'Entre los 19 legisladores santafesinos hubo siete votos afirmativos y
  doce negativos. El presupuesto 2022 fue rechazado en la cámara baja y el presidente
  prorrogará el presupuesto 2021.

'

---
La derrota del oficialismo en la Cámara de Diputados en el marco del rechazo al presupuesto 2022 presentado por el ejecutivo estuvo marcada por la polarización del voto y esto se apreció en las posturas de los 19 diputados de la provincia de Santa Fe.

Sin ninguna sorpresa en cuanto a la decisión de cada uno, cada representante santafesino votó afín a la posición general de su partido, primando por 12 a 7 el "No" a la ley de leyes entre los votos de los legisladores por Santa Fe.

Desglosando entre la postura emitida entre los diputados santafesinos de cada espacio político encontramos que todos los legisladores del Frente de Todos por Santa Fe dieron el visto bueno a la aprobación del presupuesto, acompañando al oficialismo nacional. Entre ellos se encuentran Marcos Cleri, Germán Martínez, Vanesa Massetani, Magalí Mastaler, Roberto Mirabella, Alejandra Obeid y Eduardo Toniolli.

Por su parte, entre los legisladores de los interbloques que conforman la oposición hubo un claro y uniforme rechazo al presupuesto que presentó el ejecutivo al Congreso a través del ministro de Economía Martín Guzmán. En cuanto a los diputados de Santa Fe abarcados en el frente de Juntos Por el Cambio, en el espacio del PRO, votaron de forma negativa Federico Angelini, Gabriel Chumpitaz, German Figueroa Casas, Luciano Laspina y José Carlos Núñez.

La misma postura tuvieron los representantes por Santa Fe de la Unión Cívica Radical (UCR), negando la aprobación del presupuesto los diputados Mario Barletta, Ximena García y Juan Martín. De la misma forma votó la legisladora radical santafesina María Victoria Tejeda, quien pertenece al nuevo bloque Evolución Radical, dentro de la fragmentada UCR.

También votaron en contra del presupuesto promovido por el presidente Fernández y el ministro Guzmán los representantes del socialismo santafesino, formado por Enrique Estévez y la recientemente electa Mónica Fein, ambos legisladores por el Frente Amplio Progresista.