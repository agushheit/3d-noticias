---
category: La Ciudad
date: 2021-04-25T06:15:21-03:00
thumbnail: https://assets.3dnoticias.com.ar/BONDI.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: El lunes el servicio de colectivos será normal en la ciudad de Santa Fe
title: El lunes el servicio de colectivos será normal en la ciudad de Santa Fe
entradilla: Se dictó la conciliación obligatoria por 10 días y de esta manera se levanta
  el paro de colectivos urbanos e interurbanos de pasajeros que está previsto por
  UTA para este lunes 26 de abril.

---
Finalmente quedó sin efecto el paro de trasporte público de pasajeros que había anunciado UTA en todo el país para el lunes 26 de abril y que iba a afectar a los colectivos urbanos e interurbanos de la ciudad de Santa Fe. El servicio, por lo tanto, se prestará con normalidad.

Según se informó, el Ministerio de Trabajo de la Nación dictó la conciliación obligatoria en el conflicto de la Unión Tranviarios Automotor (UTA). La decisión de la cartera laboral rige por 10 días, prorrogables en caso de que empresarios y sindicalistas no lleguen a un acuerdo.

El reclamo de los trabajadores se debe a el pedido de una recomposición salarial y recibir la vacuna contra el coronavirus, en vistas de que prestan un servicio esencial.