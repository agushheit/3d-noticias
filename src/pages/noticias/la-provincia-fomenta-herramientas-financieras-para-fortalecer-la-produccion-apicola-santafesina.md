---
category: El Campo
date: 2021-01-19T09:00:24Z
thumbnail: https://assets.3dnoticias.com.ar/apicola.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de la Provincia de Santa Fe
resumen: La Provincia fomenta herramientas financieras para fortalecer la producción
  apícola santafesina
title: La Provincia fomenta herramientas financieras para fortalecer la producción
  apícola santafesina
entradilla: Productores del sector pudieron beneficiarse a través de distintas líneas
  crediticias para continuar con su trabajo durante la pandemia de Covid-19.

---
El Gobierno de la provincia, a través del Ministerio de Producción, Ciencia y Tecnología, dispuso en el inicio de la pandemia diferentes herramientas financieras para que la producción apícola mantuviera su desarrollo frente a las dificultades que todos los sistemas productivos debieron afrontar durante 2020. El sostenimiento y el crecimiento de este sector permitió la elaboración de productos saludables y naturales para los santafesinos y el mundo, generando una oportunidad para la promoción del arraigo local, el crecimiento económico y el sostenimiento de la biodiversidad en todo el territorio santafesino.

El director de Producción Lechera y Apícola, Abel Zenklusen, destacó la importancia que ha tenido para el eslabón de producción primaria de la cadena apícola la posibilidad de acceso a los Créditos para la Reactivación Productiva 2020 del Consejo Federal de Inversiones (CFI) y sostuvo que “esta herramienta orientada a proyectos de crecimiento de colmenas para productores de mediana dimensión tuvo plena difusión y acompañamiento desde la gestión de la cartera productiva para facilitar la accesibilidad de los productores a esta valiosa fuente crediticia”.

Además, resaltó que “la línea fue consultada por más de 90 productores del segmento por un monto de $ 42 millones, lo que puso de manifiesto el interés de éstos por las condiciones favorables para afianzar el crecimiento de sus apiarios. Los valores superaron la expectativa inicial que tenía el sector de recursos, referenciada al uso de esta herramienta en años anteriores, lo que nos permitió trabajar en la ampliación de la misma y de esta forma poder asistir la requisitoria y poner a disposición para el 2021 esta línea ampliando los montos y los segmentos de uso a adecuaciones de infraestructura; indumentaria apícola, material inerte y envases. Todos estos servicios imprescindibles para la actividad, manteniendo las mismas condiciones de tasas”.

Zencklusen expresó “que para este año se prevé realizar en el territorio diferentes capacitaciones con técnicos de organizaciones apícolas sobre formulación de proyectos para ampliar la capacidad de acceso de los productores a las diferentes fuentes de financiamiento disponibles”.

El coordinador de Apicultura, Roberto Giudicatti, manifestó “que el trabajo conjunto con las organizaciones y productores nos permitió que más del 50 por ciento de los proyectos presentados fueran aprobados, llegando a un desembolso de $ 13.8 millones en el año 2020, situación que superó ampliamente las estimaciones de uso en relación a otros períodos. Consideramos que la búsqueda de herramientas útiles y a la medida del sector, así como el valor que da el Gobierno de Santa Fe al acceso al financiamiento con estas líneas crediticias y nuevas propuestas en el Programa Santa Fe de Pie acompañan una producción regional liderada por Santa Fe que contribuye al ingreso de divisas al país por más de 200 millones de dólares anuales”.