---
category: Agenda Ciudadana
date: 2021-05-29T18:15:51-03:00
thumbnail: https://assets.3dnoticias.com.ar/avion.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Más de dos millones de vacunas AstraZeneca llegan el lunes al país
title: Más de dos millones de vacunas AstraZeneca llegan el lunes al país
entradilla: En total son 2.148.600 pertenecientes al acuerdo con México, de las cuales
  el principio activo fue elaborado en la provincia de Buenos Aires y terminado de
  envasar en el país azteca y Estados Unidos.

---
La Argentina superará el lunes los 17,5 millones de vacunas recibidas desde el inicio de la pandemia, cuando arriben más de dos millones de dosis de AstraZeneca, en la mayor partida que llegue en un solo vuelo, y que reforzará el plan de vacunación dispuesto por el gobierno nacional en el marco de la segunda ola de la enfermedad.

Esta partida se sumará a los acuerdos alcanzados -pero que aún faltan terminar de formalizar- con la empresa china Sinopharm para la provisión de 6 millones de vacunas entre junio y julio, y con el laboratorio de origen chino Cansino Bio para contar con más dosis para inmunizar a la población.

**AstraZeneca**

Según informó la ministra de Salud, Carla Vizzotti, más de dos millones de dosis de la vacuna AstraZeneca contra el coronavirus llegarán el próximo lunes al país, en lo que constituirá la mayor partida que arribe en un solo vuelo y, de esta manera, la Argentina superará los 17,5 millones de dosis recibidas.

"Seguimos sumando. Hoy nos confirmaron que este lunes llegan al país otras 2.148.600 dosis del acuerdo bilateral de Argentina con AstraZeneca", publicó el jueves la ministra en su cuenta de Twitter.

El vuelo AC 7326/30 de Air Canadá tiene previsto aterrizar en el aeropuerto de Ezeiza el lunes a las 7.55 de la mañana, con un cargamento de 2.148.600 dosis, cuyo principio activo se produjo en la Argentina.

Fuentes oficiales informaron a Télam que el cargamento de estas vacunas se recogerá de Ohio, en Estados Unidos, y de allí se trasladará en camión a Toronto, Canadá, desde donde luego partirá el avión rumbo a Buenos Aires.

El componente activo de estas vacunas se elaboró en el laboratorio de Garín, en la provincia de Buenos Aires, se envasó en México y se terminó de acondicionar en los Estados Unidos.

**Números**

De esta manera, con la llegada el lunes de esta nueva partida, la Argentina "superará así los 17 millones de vacunas, "que son 17 millones de esperanzas", destacó Vizzotti en su publicación.

Hasta la mañana del viernes, de acuerdo con los datos del Monitor Público de Vacunación, el registro online que muestra en tiempo real el operativo de inmunización en todo el territorio argentino, fueron distribuidas 14.264.154 vacunas, de las cuales 11.676.733 ya fueron aplicadas: 9.084.600 personas recibieron la primera dosis y 2.592.133 ambas.

"Esta semana recibimos 2.785.200 dosis para acelerar el plan estratégico de vacunación, ya está en marcha su distribución en las 24 jurisdicciones de la Argentina. Seguimos contando. Y también contamos con vos. Sigamos cuidándonos", concluyó el hilo de Twitter de la funcionaria.

De las 2.785.200 dosis recibidas esta semana se encuentran Sputnik V y las desarrolladas en conjunto por la Universidad de Oxford y AstraZeneca, que fueron adquiridas tanto por el mecanismo Covax de la OMS como en un acuerdo bilateral con la empresa farmacéutica británica.

Ya fueron aplicadas 11.676.733 de vacunas: 9.084.600 personas recibieron la primera dosis y 2.592.133 ambas.

Ya fueron aplicadas 11.676.733 de vacunas: 9.084.600 personas recibieron la primera dosis y 2.592.133 ambas.

**Sinopharm**

En tanto, Vizzotti confirmó la posibilidad de alcanzar un nuevo acuerdo con la empresa china Sinopharm para la provisión de 6 millones de vacunas contra el coronavirus, luego de que esta semana el jefe de Gabinete, Santiago Cafiero, anunciara que el Gobierno alcanzó un acuerdo con el laboratorio de origen chino Cansino Bio para la provisión de más vacunas.

Según fuentes oficiales, en ambos casos los acuerdos estás cerrados y solo restaría la firma para comenzar a concretarlos.

En declaraciones formuladas esta mañana a Radio 10 desde Cuba, donde se encuentra junto con la asesora presidencial Cecilia Nicolini, Vizzotti se refirió a la posibilidad de "avanzar con la firma de un nuevo contrato con Sinopharm para que lleguen 2 millones (de dosis) en junio y 4 millones en julio".

"Hay que seguir vacunando, profundizar y acelerar la llegada y la distribución de dosis", dijo la ministra y señaló que la prioridad es "avanzar con la vacunación de la población priorizada y seguir completando los esquemas".

Por otro lado, informó que a principios de junio próximo debería llegar la "información oficial" sobre la producción local de la Sputnik VIDA en el laboratorio Richmond, cuyo control de calidad se está evaluando en Rusia.

Vizzotti y la asesora presidencial Cecilia Nicolini se encuentran en Cuba para conocer de parte de las autoridades sanitarias e investigadores de ese país los avances del desarrollo de las vacunas Abdala y Soberana.

Por su parte, Nicolini sostuvo que el gobierno nacional espera dar un "salto cualitativo y cuantitativo" en el plan de vacunación contra el coronavirus, con las dosis que están llegando de distintos laboratorios y destinos, y reafirmó el objetivo de inmunizar a la totalidad de la población estratégica "antes de la llegada del invierno".

"Esperamos que haya un salto cualitativo y cuantitativo en la cantidad de dosis que vamos a estar aplicando en los próximos días y en las próximas semanas", sostuvo la funcionaria en declaraciones formuladas a Radio La Red desde Cuba.

"Queremos conocer de primera mano los resultados de la fase 3 de las vacunas que se desarrollan aquí, los que ya están bastante avanzados", dijo Nicolini y precisó que en el día de hoy harán "una visita a la entidad regulatoria en conjunto con la Anmat, para comenzar a intercambiar toda la información".

En ese marco, la asesora presidencial dijo que "el objetivo es también analizar la posibilidad que la Argentina pueda apoyar en el proceso productivo".