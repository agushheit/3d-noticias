---
category: Agenda Ciudadana
date: 2021-05-04T07:55:23-03:00
thumbnail: https://assets.3dnoticias.com.ar/comercios.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias / Prensa CAME
resumen: 'Ventas minoristas: cayeron 8,3% respecto al mes de marzo'
title: 'Ventas minoristas: cayeron 8,3% respecto al mes de marzo'
entradilla: 'Abril fue un mes atípico: las ventas crecieron 40,8% anual, según el
  índice elaborado por CAME, pero todavía se mantienen 26,8% por debajo de abril de
  2019. '

---
Abril fue un mes atípico: las ventas crecieron 40,8% anual, según el índice elaborado por CAME, pero todavía se mantienen 26,8% por debajo de abril de 2019. Después de un marzo donde parecía que la recuperación podía comenzar a pisar fuerte, las nuevas restricciones para circular, el temor al covid, los ingresos familiares que no terminaron de recuperarse y la falta de mercadería, desaceleraron todo.

* El Índice de ventas minoristas pyme que elabora CAME subió 40,8% anual en abril, según la medición a precios constantes. Ese aumento es muy importante, pero todavía insuficiente porque la actividad se mantiene 26,8% debajo de abril de 2019. Es que para volver al nivel de operaciones de dos años atrás, el crecimiento anual debería haber sido de 92%, un porcentaje lejano a las posibilidades actuales del mercado de consumo.
* Para el primer cuatrimestre del año las ventas se incrementaron 6,8% frente a los mismos meses de 2020. En la comparación mensual (abril 2021 vs. marzo 2021), cayeron 8,3% aunque incide en parte la estacionalidad de la época (marzo suele ser mejor que abril). También, cabe destacar que la medición refleja el promedio nacional, y la situación en AMBA es aún más complicada, en virtud de las nuevas restricciones que afectaron significativamente a diversos sectores. 
* Además de la retracción del consumo por precaución frente a la incertidumbre sobre cómo evolucionará el Covid, como ya sucedió en marzo, pesaron las demoras en las entregas de mercadería. El 58% de las empresas relevadas tuvo problemas de abastecimiento, con un plazo promedio de demoras de 28 días, siendo el sector textil indumentaria uno de los más afectado, con un promedio de 40 días de atrasos.
* Todos los rubros subieron en la comparación anual. Algunas de las tasas más elevadas ocurrieron en: Calzado y marroquinería (+69,9%), que tras el desplome de casi 80% en abril 2020, rebotó impulsado principalmente por la demanda de calzado escolar. De todos modos, casi ningún comercio de ese sector se animó a definir abril como un mes bueno. “Nos hubiese gustado vender más, pero no hay plata en la calle, y como la gente sale menos, renueva menos su calzado”, dijo un comerciante de Río Gallegos, Santa Cruz. Mientras que desde Castelar, Gran Buenos Aires, un empresario contó que tienen demoras de 25 días en recibir la mercadería porque sus proveedores están con las fábricas cerradas por empleados con covid y eso los está perjudicando.
* Otro rubro con fuerte crecimiento fue Mueblerías, decoración y artículos para el hogar (+70,4%), pero todavía se mantiene 63,5% por debajo de los niveles de ventas de abril 2019. “Teníamos proyectado mayores ventas a las obtenidas, pero se notó el menor poder adquisitivo de la gente”, dijo el propietario de una mueblería de Ushuaia, Tierra del Fuego.

![](https://assets.3dnoticias.com.ar/ventas-minoristas.png)

![](https://assets.3dnoticias.com.ar/venta-minorista1.png)

* Alimentos y bebidas: las ventas en los comercios pymes de abril aumentaron 4,7% anual y se ubicaron solo 4,3% debajo del mismo mes de 2019. En aquel entonces, habían bajado 8,6%, siempre midiendo a precios constantes, por eso la tasa de recuperación es bastante inferior al resto. Incidió mucho la baja en la venta de carnes y el desplazamiento de la demanda hacia los canales mayoristas y online de grandes supermercados. “Cerrar más temprano nos perjudica porque los horarios coinciden con los del trabajo de la mayoría de la gente, que ahora tiene menos tiempo para ir de compras. Consumen online, pero en los supermercados más grandes”, coincidieron dos empresarios de San Fernando del Valle de Catamarca.
* Electrodomésticos, artículos electrónicos, computación, celulares y accesorios: la salida en los comercios pymes se incrementaron 58,7% comparadas con abril 2020, a precios constantes, pero resultaron 52,7% menores al mismo mes de 2019. “Marzo había empezado bien, pero desde mitad de ese mes se dejó de vender y en abril ya nos fue muy mal, se vendió poco y productos de bajo valor”, observaron desde una casa de electrodomésticos de Paraná, Entre Ríos. “No tuvimos stock de muchos productos y eso nos perjudicó, porque la gente no cambia por sustitutos fácilmente, espera o busca en otros comercios”, expresaron desde La Rioja.
* Textil indumentaria: las ventas minoristas subieron 65,6% anual pero aún están 64,1% debajo de abril 2019. “Si bien las ventas de abril 2021 fueron mejores a las de 2020, tengo que decir que no son buenas”, manifestó el propietario de un negocio de ropa de la Ciudad de Santa Fe. “Las primeras dos semanas estuvieron bien, pero después se cayó la venta”, agregó un empresario textil de Santiago del Estero. “Fue un mes con muchos altibajos, las ventas fueron mayores que en abril de 2020 pero notablemente inferiores a marzo 2021”, contó un empresario de Córdoba. Con el mismo panorama coincidió un colega de San Salvador de Jujuy: “luego de los rumores de la nueva cepa de covid y del cierre de algunas ciudades, las ventas se frenaron”.
* Con demoras en las entregas que pueden llegar a los 6 meses, el rubro Ferretería, materiales eléctricos y materiales para la construcción tuvo un ascenso anual de 27,7% en abril, pero registra aún una caída de 22,7% si se la compara con el mismo mes de 2019. La recuperación de ese sector podría ser más rápida sino se viera limitada por la falta de productos. “No tenemos ni fechas ciertas para pautar entregas, ni precios para dar referencias por esos productos, entonces no los vendemos”, declaró un comerciante de Santa Rosa, La Pampa.

![](https://assets.3dnoticias.com.ar/venta-minorista2.png)

![](https://assets.3dnoticias.com.ar/venta-minorista3.png)