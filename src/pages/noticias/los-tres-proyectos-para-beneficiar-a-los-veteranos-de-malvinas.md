---
category: Agenda Ciudadana
date: 2021-04-02T06:15:36-03:00
thumbnail: https://assets.3dnoticias.com.ar/PROYECTO VETERANOS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Los tres proyectos para beneficiar a los veteranos de Malvinas
title: Los tres proyectos para beneficiar a los veteranos de Malvinas
entradilla: El jefe de Estado anunció el envío de iniciativas al Parlamento, que buscan
  dar reconocimiento a los veteranos de guerra con medidas concretas de parte del
  gobierno nacional.

---
El presidente Alberto Fernández enviará al Congreso tres proyectos de ley con medidas para beneficiar a los excombatientes de la guerra de Malvinas, en el marco del 39º aniversario de la contienda bélica en las islas del Atlántico Sur.

Acompañando los recordatorios del aniversario, esta semana el jefe de Estado anunció el envío de estas tres iniciativas al Congreso, que buscan dar reconocimiento a los veteranos de guerra con medidas concretas de parte del Gobierno nacional.

**Régimen Previsional de Excepción, Especial y Optativo para el Otorgamiento de Beneficios Jubilatorios**

El primer proyecto de ley que enviará Fernández al Parlamento establece el “Régimen Previsional de Excepción, Especial y Optativo para el Otorgamiento de Beneficios Jubilatorios”, destinado a los soldados conscriptos excombatientes de las Fuerzas Armadas que participaron en las acciones siendo civiles.

Esta iniciativa ya fue votada en el Congreso por unanimidad el 16 de noviembre de 2016, pero vetada por el entonces presidente Mauricio Macri, el 12 de diciembre del mismo año.

**Programa Nacional de Atención al Veterano de Guerra**

La segunda iniciativa apunta a revalorizar e institucionalizar el Programa Nacional de Atención al Veterano de Guerra, otorgándole carácter de ley nacional.

Este programa fue creado ante la necesidad de garantizar el derecho constitucional a la salud y contempla las patologías que atraviesan quienes han estado en combate, además de brindar atención médica y odontológica al grupo familiar.

**Eximición del pago de peajes**

El tercer proyecto establece un beneficio de eximición del pago de peajes en rutas y autopistas nacionales, destinado a los exsoldados conscriptos y civiles que hubieren participado en el Teatro de Operaciones Malvinas (TOM) en efectivas acciones de combate.

El pasado 31 de marzo, al anunciar estos proyectos que enviaría al Congreso, el Presidente señaló que “la sociedad argentina todavía tiene una deuda muy importante con cada uno de los hombres que participaron en el conflicto”.

Lo hizo al encabezar una reunión, mediante videoconferencia desde la residencia de Olivos, del Consejo Nacional de Asuntos Relativos a las Islas Malvinas, Georgias del Sur, Sandwich del Sur y los Espacios Marítimos e Insulares.

"Debemos avanzar rápidamente en poner a esos héroes en el lugar que les corresponde y no basta con llamarlos héroes, hay que hacerles más fácil la vida después de lo que les tocó vivir y es esa la obligación que tenemos", completó el mandatario.