---
category: Agenda Ciudadana
date: 2021-01-15T04:21:33Z
thumbnail: https://assets.3dnoticias.com.ar/bomba-bajante-historica-Parana.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: '3DNoticias - Fuente: Télam'
resumen: Por la bajante histórica, Santa Fe instalará una nueva bomba de captación
  de agua
title: Por la bajante histórica, Santa Fe instalará una nueva bomba de captación de
  agua
entradilla: La flamante bomba que demandó una inversión cercana al millón de dólares,
  será instalada este fin de semana por técnicos de la empresa provincial Aguas Santafesinas.

---
El gobierno de Santa Fe instalará en Rosario una bomba de extracción de agua en el río Paraná para reforzar la capacidad de captación en la planta potabilizadora, ante la bajante extraordinaria del río y sostener, además, la prestación del servicio esencial en pandemia, informaron este miércoles desde Aguas Santafesinas.

A raíz de los trabajos de instalación se interrumpirá el suministro de agua potable, entre las 23 de este sábado y las 6 del domingo en Centro, Oeste, Sudoeste y Sur de Rosario, y en las vecinas localidades de Villa Gobernador Gálvez y Funes.

«Se realizará el recambio de una de las bombas más antiguas que tiene el sistema de captación en la planta potabilizadora, por un equipo más moderno y de mayor capacidad», dijo a Télam el gerente de Comunicación de Aguas Santafesinas, Guillermo Lanfranco.

Detalló que la actual bomba tiene una capacidad de captación «de 3 millones de litros/hora, y la nueva alcanza los 5 millones de litros en la hora».

«Esto hace más confiable el sistema y ofrece la posibilidad de captar más agua, frente al fenómeno de la bajante extraordinaria del río», señaló el funcionario.

Dijo que la inversión directa del Gobierno provincial «es de aproximadamente un millón de dólares» y que el reemplazo de la bomba es, además, «para sostener la prestación de este servicio esencial ante esta circunstancia de pandemia» por la Covid-19.

La bajante histórica del río Paraná es preocupante, y según pronósticos del Instituto Nacional del Agua (INA) «en lo que resta de este verano y posiblemente entrado el otoño, vamos a seguir con el mismo nivel», señaló Lanfranco.

«Este nivel ya lo venimos teniendo desde hace un año y meses, con bajantes de alrededor de un metro y picos abajo muy inferiores. Hoy estamos en los 40 centímetros, cuando el río históricamente debería estar en los 2,50 metros», subrayó.

Y, por último, anticipó: «si el río sigue bajando, podemos llegar a tener algún problema en la distribución de agua potable en Rosario y ciudades vecinas».