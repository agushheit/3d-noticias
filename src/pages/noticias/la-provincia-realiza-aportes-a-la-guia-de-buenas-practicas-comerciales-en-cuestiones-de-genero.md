---
category: Estado Real
date: 2021-08-08T06:00:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/GENERO.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La provincia realiza aportes a la “guía de buenas prácticas comerciales en
  cuestiones de género”
title: La provincia realiza aportes a la “guía de buenas prácticas comerciales en
  cuestiones de género”
entradilla: Se trata de una Guía de prácticas comerciales impulsada por la Dirección
  Nacional de Defensa del Consumidor y Arbitraje de Consumo.

---
El gobierno de la provincia de Santa Fe, a través de la Dirección de Promoción de la Competencia y Defensa del Consumidor, realiza aportes para la redacción definitiva de la “Guía de buenas prácticas comerciales en cuestiones de géneros y diversidades”, propuesta por la Dirección Nacional de Defensa del Consumidor y Arbitraje de Consumo.

En esta misma línea, se formularon diversas recomendaciones con el objetivo de brindar los aportes necesarios para promover prácticas comerciales y de consumo que sean respetuosas de la dignidad e igualdad de las y los consumidores. Para la redacción de la misma, se trabaja de manera articulada con el Ministerio de Igualdad, Género y Diversidad de la provincia.

La directora provincial de Promoción de la Competencia y Defensa del Consumidor, María Betania Albrecht, manifestó: "Estamos orgullosos del trabajo que realizamos a fin de sumar aportes a esta guía. Nuestra provincia, constantemente se compromete e involucra en la promoción de prácticas comerciales y de consumo con perspectiva de género”.

“Trabajamos de manera articulada con funcionarias del Ministerio de Igualdad, Género y Diversidad, lo que demuestra que se trata de temáticas transversales, las cuales, para rendir sus frutos, requieren del trabajo articulado y colaborativo de ambas áreas, para lograr el objetivo buscado: la generación de relaciones de consumo saludables, igualitarias y respetuosas”, concluyó la funcionaria.