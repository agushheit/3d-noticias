---
category: Deportes
date: 2021-02-09T06:39:12-03:00
thumbnail: https://assets.3dnoticias.com.ar/Tapia-Coria-web.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: AFA pidió el regreso de público a los partidos
title: AFA pidió el regreso de público a los partidos
entradilla: 'El Presidente de la AFA se reunió con el Jefe de Gabinete de Ministros,
  Gustavo Coria, para presentarle una solicitud vinculada al regreso de un 33% de
  aforo a los estadios para la Copa de la Liga Profesional. '

---
Claudio Tapia, Presidente de la Asociación del Fútbol Argentino solicitó a los gobiernos de las provincias que tienen equipos de fútbol en Primera División, el regreso del 33% de público a los estadios una vez que el proceso de vacunación planificado para los próximos meses continúe avanzando. 

De acuerdo con la AFA, el pedido "tiene su fundamento en el éxito alcanzado en las distintas Fases que hemos completado desde que nos fuera autorizado el reinicio de la actividad futbolística".

En la nota se enumeran los pasos que se dieron desde el "entrenamiento, primero con grupos reducidos de 6 jugadores, luego con el plantel completo, siguiendo con la realización de partidos de entrenamiento y, finalmente con la disputa de los torneos oficiales".

Finalmente, AFA destaca que el campeonato oficial se respetará el "Protocolo que nos fuera aprobado para los partidos amistosos de entrenamiento, combinado con aquél que se aplicaba para los encuentros por la Conmebol Libertadores 2020".

![](https://assets.3dnoticias.com.ar/carta.jpg)