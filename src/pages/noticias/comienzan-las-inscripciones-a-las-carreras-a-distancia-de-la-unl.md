---
layout: Noticia con imagen
author: "Fuente: Prensa UNL"
resumen: " UNL: Carreras a Distancia"
category: La Ciudad
title: Comienzan las inscripciones a las carreras a distancia de la UNL
entradilla: El plazo va del 15 de noviembre al 28 de febrero. La oferta
  académica a distancia incluye más de 40 carreras y cursos. Los títulos que se
  otorgan son de nivel universitario, con validez en todo el país.
date: 2020-11-17T14:20:24.022Z
thumbnail: https://assets.3dnoticias.com.ar/universidad.jpg
---
La Universidad Nacional del Litoral abre las inscripciones para estudiar carreras a distancia. La oferta académica en esta dimensión de la UNL es de más de 40 carreras y cursos entre ciclos iniciales de carreras de grado, tecnicaturas, ciclos de licenciatura, cursos de formación profesional y cursos de capacitación.

La duración de las propuestas, según el tipo, ronda entre los 2 y 3 años. El cursado es íntegramente virtual y asincrónico, y las gestiones académicas y administrativas se desarrollan online por medio del Campus Virtual UNL

De este modo, la UNL tiene habilitadas las inscripciones a todas sus carreras ya que, recordemos, el lunes 2 de noviembre comenzó el proceso correspondiente a las carreras presenciales. El plazo en este sentido se extiende hasta el viernes 11 de diciembre y para ampliar esta información es necesario ingresar en [www.unl.edu.ar/ingreso](https://www.unl.edu.ar/ingresounl/)



## **Carreras a distancia**

Para estudiar a distancia, las inscripciones se habilitarán desde el 15 de noviembre hasta el 28 de febrero de 2021. Para inscribirse es necesario ingresar al portal[ www.unlvirtual.edu.ar](http://www.unlvirtual.edu.ar/), elegir la carrera o curso de interés y seguir los pasos para realizar la inscripción online.

Tanto la oferta académica de carreras y cursos de la modalidad a distancia, como los requisitos para anotarse y la documentación que es necesario presentar, también se informan en esta plataforma.

Asimismo, se puede acceder a la oferta completa de carreras y cursos, consultar filtrando la búsqueda por unidades académicas; y acceder al Manual del Estudiante, una herramienta que mejora la experiencia de navegación de estudiantes por los distintos ambientes de UNL Virtual.

Cabe señalar que los y las ingresantes a todas las propuestas cursan un curso introductorio a la modalidad virtual titulado Estudios Universitarios y Tecnologías.

Quienes son ingresantes al Ciclo Inicial de Abogacía deben realizar también Cursos de Articulación Disciplinar y General. En todos los casos el dictado es virtual y se cursarán al inicio del año académico. Son obligatorios, pero no eliminatorios.

Para estudiar en UNL Virtual las carreras se abonan por cada materia que se curse. Los aranceles y toda información sobre la propuesta se encuentran en el detalle de cada carrera.



## **Carreras presenciales**

En cuanto a las carreras presenciales, del 2 de noviembre al 15 de diciembre se debe completar la ficha de datos personales en [www.unl.edu.ar/ingreso](https://www.unl.edu.ar/ingresounl/) y solicitar un turno para inscripción a la carrera elegida. Quienes ingresen los datos recibirán un correo electrónico de confirmación.

**Luego se deberá cargar la siguiente documentación:**

a) Documento Nacional de Identidad actualizado, constancia de extravío o constancia de trámite.

b) Constancia de CUIL -[www.anses.gob.ar](https://www.anses.gob.ar/)-.

c) Título de estudios secundarios, que luego deberá estar legalizado por la UNL. En su defecto, es posible cargar el título de estudios secundarios sin legalizar o constancia de título en trámite; constancia de alumno regular; o constancia de finalización de estudios secundarios en la que se indique si se adeudan o no materias, y cuáles.

**Hay tiempo de digitalizar la documentación hasta un día antes del turno asignado.**

Las inscripciones se llevarán a cabo desde el 16 de noviembre al 11 de diciembre. Se solicita a las y los interesados que el día del turno seleccionado estén atentos al teléfono y al mail ya que, si hay algún problema con la documentación, desde la UNL se comunicarán a través de alguno de esos medios. Una vez que se haya concretado la inscripción llegará al interesado/a un e-mail confirmando la inscripción.

Para el ingreso 2021 en la modalidad presencial, se realizaron modificaciones en la ficha de inscripción en el marco de las políticas de género y diversidad sexual que promueve la Universidad. Cada ingresante, en su Sistema de Gestión del Estudiante, podrá inscribirse con el nombre que se corresponde con el de su identidad de género, como lo establece el Art. 12, segundo párrafo, Ley N° 26.743 de Identidad de Género. Además, en la ficha de inscripción, las personas que tengan una identidad de género distinta a la que figura en su DNI podrán reflejarla si así lo desean.



## **Toda la propuesta académica**

Los stands virtuales de las facultades, centros universitarios, institutos y escuelas de la UNL, toda la propuesta académica con más de 140 carreras presenciales y a distancia, los sistemas de becas, investigación, cultura, emprendedurismo, deportes y toda la vida universitaria continúan a disposición en [www.unl.edu.ar/expocarreras](https://www.unl.edu.ar/expocarreras/). Lo mismo sucede con las 22 charlas que, producidas en el estudio de Litus TV en formato de programa televisivo y con resolución Full HD, pueden volver a verse entrando en la plataforma. Lo mismo sucede con las conversaciones que se desarrollaron en Instagram @expocarrerasunl con referentes de las unidades académicas de la UNL.

En esta web, además, hay diferentes maneras de acceder a la propuesta académica: cuenta con buscadores generales en los que se filtra la información en base a opciones -modalidades, unidad académica, nivel, categoría, palabras claves-; hay un buscador por unidades académicas, otro por ciudades en las que tiene presencia la Universidad y otro por área disciplinar. Además, hay un stand virtual de cada una de las facultades, institutos, escuelas y centros universitarios con información, fotos, videos y enlaces. Está alojada toda la información sobre el Ingreso 2021, becas, inscripciones y sobre todas las propuestas que tiene la UNL para sus estudiantes: residencias, centro de idiomas, experiencias internacionales, extensión, cultura, deporte, predio recreativo, emprendedurismo, investigación y salud.