---
category: La Ciudad
date: 2022-01-04T06:00:09-03:00
thumbnail: https://assets.3dnoticias.com.ar/educvial.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: El Centro de Educación Vial reprograma turnos otorgados para esta semana
title: El Centro de Educación Vial reprograma turnos otorgados para esta semana
entradilla: "El espacio permanece cerrado en horas de la tarde por reducción de personal,
  debido a un caso de Covid-19 positivo. Quienes tenían turnos para la tarde, serán
  contactados para reprogramar. \n\n"

---
La Municipalidad informa que durante esta semana, el Centro de Educación Vial atenderá sólo en horario matutino, de lunes a sábados, de 7 a 13 horas. De este modo, quienes tenían turnos otorgados para la tarde, serán contactados para reprogramar.

Según se indicó, la medida se tomó por una reducción de personal, signada por un caso de Covid-19 positivo. Ante esto, quienes habían obtenido un turno para los horarios vespertinos, serán contactados por el medio que registraron al momento de pedir el turno y se les informará sobre la reprogramación.

**Nueva prórroga para las licencias de conducir**

La Secretaría de Control y Convivencia Ciudadana del municipio indicó que se estableció una nueva prórroga para las licencias de conducir vencidas en 2021 y en los tres primeros meses de 2022. Las mismas tendrán vigencia hasta junio próximo.

Según se confirmó, la Agencia Provincial de Seguridad Vial hizo lugar a un pedido del municipio, por la demanda existente para la renovación de los carné.

Con el objetivo de evitar la aglomeración de personas en los centros de emisión de licencias, se estableció que la prórroga alcanza a aquellas con vencimiento en 2021 y en los meses de enero, febrero y marzo del año en curso.