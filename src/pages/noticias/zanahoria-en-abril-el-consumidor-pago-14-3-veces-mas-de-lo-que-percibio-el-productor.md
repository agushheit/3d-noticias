---
category: El Campo
date: 2021-05-18T08:19:10-03:00
thumbnail: https://assets.3dnoticias.com.ar/zanahoria.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa CAME
resumen: 'Zanahoria: En abril, el consumidor pagó 14,3 veces más de lo que percibió
  el productor'
title: 'Zanahoria: En abril, el consumidor pagó 14,3 veces más de lo que percibió
  el productor'
entradilla: 'La brecha de precios para el promedio de los 24 principales alimentos
  agropecuarios que participan de la mesa familiar aumentó 8,7% en abril, al subir
  de 4,29 veces en marzo a 4,67 veces. '

---
La brecha de precios para el promedio de los 24 principales alimentos agropecuarios que participan de la mesa familiar aumentó 8,7% en abril, al subir de 4,29 veces en marzo a 4,67 veces. La desmejora se explica por una caída de 3,7% en los precios al productor y un incremento de 1,63% en el costo que pagó el consumidor en comercios de cercanía. Zanahoria y calabaza fueron los productos con más brecha y acelga, huevo y pollo, los de menos.

\- La diferencia de precios entre lo que pagó el consumidor y lo que recibió el productor en el campo por sus alimentos agropecuarios subió 8,7% en abril.

\- La variación fue impulsada por una caída de 3,74% en los desembolsos al productor, mientras que los costos al consumidor tuvieron una suba de 1,63% en los comercios pymes de cercanía y una baja de 0,44% en los hipermercados.

\- En abril, para los 24 agroalimentos de consumo familiar relevados, los consumidores pagaron en promedio 4,67 veces más de lo que cobró el productor en la tranquera de sus chacras. En marzo esa desigualdad fue de 4,29 veces y en febrero de 4,42 veces.

\-Los productos con mayor suba mensual en sus brechas fueron: **naranja,** con un alza de 86,5%, **limón** (+83,3%), **repollo** (+23,3%) y **zanahoria** (+22,5%). Los de mayores bajas fueron **tomate** y **mandarina** (ambos descendieron 13,4%), y **calabaza** (-11,5%).

\- Los datos surgen del **Índice de Precios en Origen y Destino (IPOD)** que elabora el **sector de Economías Regionales de CAME** en base a los precios de origen de las principales zonas de producción y a más de 700 precios de cada producto en destino, relevados no solo en verdulerías y mercados por un equipo de 30 encuestadores, sino también mediante un monitoreo de los importes online de los principales hipermercados del país, durante la segunda quincena del mes.

**Otros datos de abril**

* **IPOD frutihortícola:** subió 10,0% en el mes y la brecha para esos productos promedió en 4,96 veces.
* **IPOD ganadero:** bajó 5,5%, con una disparidad promedio de 3,23 veces. El IPOD ganadero resultó 34,9% más bajo que el IPOD agrícola, ampliando esa diferencia frente a marzo.
* **Participación del productor en el precio final:** cayó 6,1%, de 29,9% en marzo a 28,1% en abril.

![](https://www.redcame.org.ar/advf/imagenes/2021/05/6097dff258fef_706x300.png)

**Mayores y menores brechas**

\- En abril las mayores desigualdades se dieron en: **zanahoria**, con una diferencia de 14,3 veces, **calabaza** con 9,4 veces, **zapallito** (8,5 veces) y **limón** (7,8 veces).

\- Los productos con menores disparidades, en tanto, fueron: **acelga** (1,6 veces), **huevos** (2,2 veces), **pollo** (2,2 veces) y **repollo** (2,3).

![](https://www.redcame.org.ar/advf/imagenes/2021/05/6097dff25cda5_706x281.png)

![](https://www.redcame.org.ar/advf/imagenes/2021/05/6097dff25f5e1_706x282.png)

**Donde más subió y más bajó la brecha**

En abril hubo 14 productos con subas en sus brechas y 10 con bajas, que determinaron un incremento promedio de 8,7% en el IPOD del mes.

\- IPOD **naranja:** trepó 86,5%, por una disminución de 46,2% en los precios de origen y un aumento de 0,3% en los de destino. La variación del monto en origen, según comentaron los productores, responde a que aumentó la oferta por el inicio de la cosecha local, sumado al descarte de la elaboración de Rusia. Interanualmente, se observa una variación del 142,85% en el importe de origen.

\- IPOD **limón:** escaló 83,3%, con descensos de 56,2% en los precios de origen y de 19,7% en los costos de destino. En origen, la variación, de acuerdo con lo informado por los productores, se debe a un aumento en la oferta. Interanualmente, los montos de origen subieron 121,81%.

\- IPOD **tomate:** bajó 13,4%. Esta reducción responde a un alza de 7,4% en los importes de origen y una disminución del 7% en los de destino. Interanualmente, se observa una caída de 18,65% en el costo de origen.

\- IPOD **mandarina:** descendió 13,4%, tras un aumento de 0,7% en los precios de origen y un descenso de 12,8% en los importes al consumidor. Interanualmente, se observa una variación de 178,8% en el monto de origen.

**Participación del productor**

\- La participación promedio del productor en el precio final de los 24 productos agropecuarios bajó 6,1%, ubicándose en 28,1% en abril.

\- La mejor situación la volvió a tener la **acelga**, donde el productor recibió en promedio el 61% del precio de venta minorista de esa verdura, aunque algo menos del 65% que había recibido en marzo. También la **zanahoria** volvió a ser el producto en peor situación: el productor apenas se llevó el 7% de lo que pagó el consumidor (había sido el 8,6% en marzo).

![](https://www.redcame.org.ar/advf/imagenes/2021/05/6097dff265905_706x71.png)

[![Flourish logo](https://public.flourish.studio/resources/bosh.svg)A Flourish chart](https://public.flourish.studio/visualisation/5946801/?utm_source=showcase&utm_campaign=visualisation/5946801)

El **IPOD** es un indicador elaborado por el **sector de Economías Regionales de CAME** para medir las distorsiones que suelen multiplicar por varias veces los precios de los productos agropecuarios, desde que salen del campo hasta que llegan al consumidor. Estas desigualdades son muy dispares según producto, región, forma de comercialización y época del año.

En general, las diferencias se deben a un conjunto de comportamientos. Por un lado, los especulativos, adoptados por diversos actores de la cadena de valor que abusan de su posición dominante en el mercado –básicamente, los hipermercados, los galpones de empaque y cámaras de frío–. Por el otro, factores tales como la estacionalidad, que afecta a determinados productos en algunas épocas del año, las adversidades agroclimáticas, y los costos de almacenamiento/acopio y transporte, entre otros.

![](https://www.redcame.org.ar/advf/imagenes/2021/05/6097dff26a343_706x504.png)

**Metodología del IPOD**

El **Índice de Precios en Origen y Destino (IPOD)** comenzó a elaborarse en agosto de 2015, a fin de visibilizar la falta de transparencia en las cadenas de valor.

A través del I**POD** se busca conocer la brecha de importes entre dos momentos de la etapa de comercialización de un producto agropecuario:

**- Precio de origen:** monto que se le paga al productor.

**- Precio de destino:** costo que abona el consumidor en góndola.

La diferencia origen-destino indica la cantidad de veces que aumenta el precio del producto desde su salida del campo hasta su comercialización en góndola.

El relevamiento para elaborar el IPOD de abril se realizó durante la segunda quincena del mes, con una cobertura geográfica nacional.

Para obtener los valores de destino se relevaron más de 700 precios de cada producto, no solo a través de un monitoreo diario de los montos online de los principales hipermercados del país, sino también mediante un equipo de 30 encuestadores en mercados y verdulerías de Catamarca, Chaco, Chubut, Córdoba, Entre Ríos, Formosa, Jujuy, La Pampa, La Rioja, Mendoza, Misiones, Neuquén, Río Negro, Salta, San Luis, Santa Cruz, Santa Fe, Santiago del Estero, Tierra del Fuego, Tucumán, Ciudad Autónoma de Buenos Aires y Provincia de Buenos Aires.

Para obtener los precios de origen se consultó telefónicamente a 10 productores de cada alimento, localizados en sus principales zonas productoras.