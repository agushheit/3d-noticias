---
category: Deportes
date: 2021-08-06T06:00:06-03:00
thumbnail: https://assets.3dnoticias.com.ar/MARTINEZ.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: La abanderada paralímpica se prepara para levantar el pabellón
title: La abanderada paralímpica se prepara para levantar el pabellón
entradilla: 'La rosarina Yanina Martínez será la abanderada argentina en los Juegos
  Paralímpicos de Tokio. '

---
Después de los Juegos Olímpicos de Tokio vienen los Juegos Paralímpicos de Tokio. En Río 2016, Argentina consiguió una sola medalla de oro. Fue la que consiguió Yanina Martínez, que triunfó en la prueba de 100 metros en la categoría T36. La atleta que nació y vive en Rosario acudirá a Japón y esta vez con un plus. Será la abandera de la delegación junto al también comprovinciano Fabián Ramírez. Su entrenador, Martín Arroyo, habló con la prensa sobre la distinción como abanderada para Martínez: “Está muy contenta ella, su familia, todos los que la rodeamos a Yani. No es un mérito por lo de Río, sino que es un mérito de toda la carrera que ella tuvo. Creo que es muy merecido que Yani lleve la bandera”.

Martínez y equipo se preparan para defender el título paralímpico. “Es un juego olímpico más con lo que fue su primera participación en Londres -comentó Arroyo-. Luego con la gran experiencia de Río. Y ahora se suma esta. Las tres son totalmente distintas. Esta tiene el condimento especial que Yani va a portar la bandera. La veo muy motivada. La veo muy bien y muy entusiasmada”.

Obtener una medalla en un juego paralímpico nunca es fácil. Martínez intentará hacerlo por segunda vez. “Lo que es podio siempre es difícil -consignó Arroyo-. Yanina está con marcas como para poder subir y eso nos ilusiona mucho. Esta pandemia ha generado que no sabemos cómo están las demás, cómo está Yanina para las demás tampoco. Nos vamos muy ilusionados de que a podio se puede subir, si todo sale bien”.

**Entrenar con discapacidad física**

Preparar a una atleta con desventajas como Yanina no es fácil. Sobre esto, Martín comentó: “Sabemos que hay una parálisis, que hay ciertos factores que están comprometidos como la coordinación, el equilibrio, la plasticidad muscular. Tratamos de generar trabajos que mejoren la marcha, la postura. Desde ahí se basan muchos trabajos que mejoran el rendimiento para poder luego mejorar la parte técnica de lo que es el atletismo”.

La práctica deportiva intensiva supuso un factor de bienestar para Martínez. ¿Cómo se observa esto? “De Yani está muy marcado lo que fue el proceso y ha cambiado mucho su forma de vida. De pensar, de actuar, de relacionarse con nosotros. De tener más amigos. No a nivel virtual, sino en la presencialidad. Ha creado un carácter que está dispuesto a poder llegar a pequeños y grandes compromisos. Pasó de ser una nena a una mujer. De una chica que realizaba un juego a hacer un deporte de alto rendimiento y eso se fue formando a través de su entrenamiento, de sus relaciones”, respondió Martín Arroyo.

El entrenador aseguró que no admira a ninguna atleta en particular, pero que sí observa ciclismo: “Cuando termina el atletismo pone en su celular ciertas partidas en el momento decisivo en donde salen. Ella sola disfruta estar viendo eso”. Por otra parte, no está definido si la medallista paralímpica seguirá compitiendo tras la competencia en tierras japonesas. “Veremos si Yanina quiere seguir, yo creo que sí. Eso va a depender de mucha gente para mantener su equipo de trabajo. No es solamente las ganas”, comentó Arroyo.

Afortunadamente para Martín y Yanina la pandemia no les impidió poder prepararse. “Los primeros 45 días fueron muy duros para todo el mundo a nivel pandemia. Algunos atletas han sufrido con algunos aspectos. Nosotros como atletas después de esos 45 días que estuvimos todos adentro fuimos uno de los primeros deportes que salió. Primero que el fútbol. Nuestro compromiso de Tokio estaba vigente. Estamos metidos en este juego olímpico y veremos qué tanto nos afectó en esta cuestión”, afirmó el preparador.