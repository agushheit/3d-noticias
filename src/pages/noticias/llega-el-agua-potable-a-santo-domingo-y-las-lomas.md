---
category: La Ciudad
date: 2021-09-30T06:00:14-03:00
thumbnail: https://assets.3dnoticias.com.ar/LASLOMAS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Llega el agua potable a Santo Domingo y Las Lomas
title: Llega el agua potable a Santo Domingo y Las Lomas
entradilla: " “Estamos saldando una deuda histórica con montos que no superan los
  30 millones de pesos y se deberían haber hecho años atrás”, indicó Jatón."

---
Esta mañana se licitaron dos obras que serán fundamentales para las vecinas y vecinos de los barrios Santo Domingo y Las Lomas. Se trata de la ampliación y mejora del servicio de agua potable para ambos barrios. La apertura de sobres fue encabezada por el intendente Emilio Jatón, la ministra de Infraestructura, Servicios Públicos y Hábitat, Silvina Frana, y el presidente de Aguas Santafesinas S.A. (ASSA), Hugo Morzán. Ambas obras cuentan con un presupuesto oficial -en el marco del Plan Incluir- de 30 millones de pesos más IVA y plazos de ejecución de 4 meses (Las Lomas) y 5 meses (Santo Domingo).

“Estamos saldando una deuda histórica con montos que no superan los 30 millones de pesos y con plazos de ejecución de 5 meses”, indicó el intendente haciendo referencia a lo importantes de tomar decisiones políticas conjuntas. “La deuda es histórica porque los barrios y las personas están viviendo hace años así y sabemos que no están pidiendo ni más ni menos que agua; es una fuerte decisión política que no se hizo antes y se toma hoy”, dijo.

**Políticas que cambian vidas**

Con respecto a la falta de decisión de gestiones anteriores para concretar estos trabajos esenciales, Jatón añadió: “por eso es muy importante que hoy municipio y provincia lo estamos concretando. Porque habla a las claras de decisiones políticas que antes no se tomaban y, en el medio, los vecinos de Santo Domingo y Las Lomas, solicitando un derecho primario: tener agua en sus barrios”.

En ese sentido, el intendente repasó: “Cuando se hizo la planta potabilizadora en Beruti y Camino Viejo, iba a dar agua a Santo Domingo, esa conexión la tenía que hacer el municipio anterior y nunca la hizo, por eso lo de hoy es muy importante, estamos llevando un derecho a los ciudadanos. Éstas son políticas que cambian la vida de la gente. Felicitaciones a la lucha de Santo Domingo y Las Lomas”

Finalmente, el intendente Jatón resaltó: “Quiero destacar el momento histórico que está viviendo la ciudad de Santa Fe, tanto por su planta potabilizadora como por la llegada del agua potable a los barrios donde muchos pensaban que nunca iba a llegar”, destacó el mandatario en referencia a obras emblemáticas como la de Colastiné Sur.

**Las ofertas**

Para los trabajos de Santa Domingo se presentaron tres ofertas: Winkelmann SRL $ 27.922.259,17 + IVA; Novelo Cristian $ 26.922.753,60 + IVA; y Tiguan SA $ 25.666.186,62 +IVA.

En tanto, para llevar adelante los trabajos en Las Lomas hubo dos oferentes: Novelo Cristian $ 7.626.550 + IVA; y Tiguan SA $ 8.310.478,27 + IVA.

**Detalles de obra**

Los trabajos que se concretarán en Santo Domingo requieren de más inversión porque se tiene que realizar un tendido de más de 1.500 metros de cañerías, explicó el presidente de ASSA. “Los trabajos tienen un monto de alrededor de 20 millones de pesos. En tanto, en el barrio Las Lomas se llevará adelante la mejora de la red existente y un tendido de algo más de 400 metros de cañerías. De esa manera, se podrá llegar a 1000 nuevas conexiones y darle servicio a 4000 vecinos de la ciudad de Santa Fe”, dijo Morzán.

Por otra parte, Frana destacó el trabajo conjunto con ASSA y la Municipalidad para resolver una urgencia y de solucionar una falencia con una obra estructural.