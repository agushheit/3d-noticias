---
category: Estado Real
date: 2021-05-05T08:53:29-03:00
thumbnail: https://assets.3dnoticias.com.ar/fisfe.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: La provincia firmó un convenio con FISFE en el marco de la transición energética
  santafesina
title: La provincia firmó un convenio con FISFE en el marco de la transición energética
  santafesina
entradilla: 'La ministra de Ambiente y Cambio Climático, Erika Gonnet, destacó: "Esta
  es una acción más del plan de transición energética santafesina que impulsamos desde
  el gobierno de Santa Fe".'

---
El gobierno provincial, a través del ministerio de Ambiente y Cambio Climático, firmó un convenio con la Federación Industrial de Santa Fe (FISFE) para avanzar en políticas que permitan el fortalecimiento del sector vinculado a la sostenibilidad, la eficiencia energética y las energías renovables.

El acuerdo fue celebrado durante un encuentro virtual, encabezado por la ministra de la cartera, Erika Gonnet, y tiene el objetivo de desarrollar una serie de acciones enmarcadas en el proceso de transición energética santafesina.

“La ejecución de este plan posibilitará el desarrollo de una plataforma web, de una red de aprendizaje, y el desarrollo de lineamientos para el establecimiento de un sistema de gestión energética industrial", detalló Gonnet.

Y en ese sentido resaltó que "es una acción más del plan de transición energética santafesina que impulsamos desde el gobierno de Santa Fe", e indicó que “entre las partes coincidimos en elevar la propuesta ante el Consejo Federal de Inversiones (CFI) para su financiamiento”.

“Tenemos una agenda de trabajo junto a FISFE que tiene que ver con el fomento de la eficiencia energética, las energías renovables y la sostenibilidad en el sector industrial; en ese sentido seguimos generando herramientas vinculadas a la transición energética”, concluyó la ministra.

Por su parte, el presidente de FISFE, Víctor Sarmiento, celebró la suscripción del convenio y expresó: “Este acuerdo se firma por un convencimiento que tiene la Federación y que comparte con el Ministerio, de poder llegar a los lugares más lejanos de la provincia de Santa Fe, donde realmente se va a necesitar estar a la altura de las circunstancias y para que esto concretamente llegue a las PYMES, que quizás están más alejadas de nuestro centro provincial”.

Además, puso el foco en la importancia de la conectividad y el impacto positivo en las PYMES más distantes del territorio. “Si hablamos de inclusión, la conectividad será uno de los puntos centrales que nos va a permitir tener una provincia más equilibrada, distributiva y justa. Hay que conectar a la provincia y ser justos, derramar en aquellos que no tengan las mismas condiciones, porque geográficamente están a una distancia que no se los permite”, indicó.

A su turno, Mariano Ferrazini, tesorero de FISFE, afirmó: “Este acuerdo se enmarca en un trabajo público/privado que venimos llevando adelante como Federación Industrial que nos permitirá trabajar en eficiencia en el sector productivo, además de lograr una producción más sustentable con el medioambiente”.

Y agregó: “Esto es necesario por varios factores, pero uno de los más importantes es lograr beneficios para el sector industrial a través de la economía circular”.

De la reunión participaron la ministra de Ambiente y Cambio Climático, Erika Gonnet; el secretario de Desarrollo Ecosistémico y Cambio Climático, Jorge Caminos, el subsecretario de Tecnologías para la Sostenibilidad, Franco Blatter, y el asesor, Maximiliano Aimaretti. En tanto, FISFE estuvo representada por su presidente, Víctor Sarmiento, junto a Walter Andreozzi, Mariano Ferrazini y Alberto Rosandi.