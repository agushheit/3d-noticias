---
category: El Campo
date: 2021-02-08T07:11:20-03:00
thumbnail: https://assets.3dnoticias.com.ar/rural.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Clarín
resumen: La réplica de las entidades agrarias a Alberto Fernández por la amenaza de
  subir retenciones
title: La réplica de las entidades agrarias a Alberto Fernández por la amenaza de
  subir retenciones
entradilla: Dirigentes de la Sociedad Rural, la Federación Agraria y la CRA le contestaron
  duramente al Presidente. Creen que el conflicto es “ideológico” y prevén protestas.

---
Las entidades agrarias que componen la Mesa de Enlace encendieron las alertas y comenzaron a responderle a Alberto Fernández y a la idea en danza de una probable suba de retenciones al campo con la que el Presidente advirtió al sector en una entrevista publicada este domingo.

Para Carlos Vila Moret, vicepresidente de la Sociedad Rural, esas declaraciones "son preocupantes porque desconocen la dinámica de la formación de precios y parten de una mirada prejuiciosa en la que el campo y los productores somos siempre los culpables".

"No se sabe hasta qué punto es desconocimiento o un relato ideológico que les sirve para la interna", criticó el dirigente agrario, al advertir que "los productores no somos formadores de precio ni en la carne, ni en el pan ni en ninguno de los productos de la 'mesa de los argentinos', y la incidencia de la materia prima en el precio final es mínima".

Según su criterio, "este tipo de medidas tendría un efecto opuesto a lo deseado" porque una suba en las retenciones traería como consecuencia "una menor superficie sembrada, lo que equivale a una menor oferta y una mayor presión sobre los precios". "La única manera de garantizar el abastecimiento en el mercado doméstico a precios accesibles es apuntalar la producción. Lo que hay que hacer es producir más. En la medida en que la agricultura y la ganadería sean negocios rentables, más productores se volcarán a ellos, lo que engrosará la oferta y hará que los precios sean razonables", agregó Vila Moret.

El Presidente habló de una suba de retenciones o del establecimiento de cupos a la exportación como métodos posibles para controlar los aumentos de precio en los alimentos. En el mismo sentido se había pronunciado anteriormente Cecilia Todesca, vicejefa de Gabinete.

> El Gobierno proyectó en su presupuesto anual una inflación del 29%, pero los analistas privados del mercado ya manejan una proyección del 50%.

"Creer que con medidas como un aumento de retenciones o la implementación de cupos de exportación se va a lograr una mejora para el consumidor es desconocer cómo se forman los precios", se quejó el vicepresidente de la SRA. "El uso de estas herramientas ya tiene un resultado conocido en el pasado cercano: caída de la producción y de las exportaciones y, por lo tanto, una pérdida de mercados externos y un menor ingreso de divisas", sostuvo.

> Las exportaciones de soja están gravadas en 33% y para subirlas se requiere el voto del Congreso. El trigo y el maíz pagan 12% y podrían subirse hasta 15% por decreto.

Como ejemplo, el dirigente agrario recordó los resultados de una medida similar tomada durante el mandato de Mauricio Macri: "Tan solo basta ver cómo evolucionó el precio del pan cuando en 2018 el anterior gobierno volvió a poner las retenciones de 4 pesos por dólar exportado: el precio del pan, lejos de bajar, siguió el curso de la inflación".

Por su parte, para Carlos Achetoni, presidente de Federación Agraria Argentina (FAA), un avance del Gobierno en ese sentido decantaría en un seguro desencuentro con el campo. "No se entiende cómo debemos comprender esta situación. Sabiendo cómo está el ánimo del productor, si avanzan en eso es muy probable que vaya a generar un conflicto", advitió en diálogo con CNN Radio. "Por un lado necesita el ingreso de divisas, que cerrando las exportaciones no van a existir. Pero por otro, lo asocian a los precios que tienen en góndola cuando no somos formadores de precios", se quejó.

Las protestas también se escucharon en voz de Jorge Chemes, presidente de Confederaciones Rurales Argentinas (CRA). "No deja de sorprenderme la falta de capacidad de entendimiento por parte de los funcionarios cuando le tratamos de transmitir que el sector primario, además de no ser formadores de precios, es el que menos participación tiene en la cadena de cualquier producto", dijo el producto a TN y se quejó porque "quien tiene la mayor proporción es el Estado porque lo que más pesa es el paquete impositivo".

"El análisis del Presidente es equivocado- continuó Chemes-. Creo que el Gobierno debería fijarse en el costo de los impuestos de cualquier alimento que compra uno en la góndola, donde un 40 o un 50% es un factor que inside en forma importante. Evidentemente parece que el Gobierno no lo quiere entender".

Para el dirigente de la CRA "esto es ideológico, con lo cual, se deja de lado la realidad y se trata de generar una imagen negativa del campo hacia la sociedad". "El campo no es un problema para el país; el campo es la solución. En lugar de gastar energía en estas discusiones habría que hablar de como logramos mejores estrategias para una mayor producción", concluyó.

Mañana habrá una reunión de la Mesa de Enlace. Se prevé una dura respuesta en conjunto ante las declaraciones de Alberto Fernández.