---
category: Estado Real
date: 2022-01-24T21:49:59-03:00
thumbnail: https://assets.3dnoticias.com.ar/licipuerto.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Se licitaron obras para el puerto de Santa Fe por mas de 220 millones de
  pesos
title: Se licitaron obras para el puerto de Santa Fe por mas de 220 millones de pesos
entradilla: "“Hace aproximadamente 15 años que no se venían realizando inversiones
  con un perfil productivo en el Puerto de Santa Fe”, aseguró Maina."

---
El Ministerio de Infraestructura, Servicios Públicos y Hábitat, a través de la Secretaría de Empresas y Servicios Públicos, llevó adelante seis licitaciones enmarcadas en el convenio -firmado el pasado 7 de octubre- entre el Ente Administrador del Puerto de Santa Fe (EAPSF) y el Ministerio de Transporte de la Nación cuyo objeto es sentar las bases para desarrollar actividades de cooperación y colaboración para optimizar los niveles de eficacia y eficiencia de las operaciones portuarias para los puertos santafesinos.

En ese sentido, para el mejoramiento de la infraestructura y servicios portuarios del Puerto de Santa Fe, el Ministerio de Transporte de de la Nación dispuso $200.000.000 para obras de infraestructura portuaria y equipamiento.

Al respecto, el secretario de Empresas y Servicios Públicos, Carlos Maina señaló: “Esto está alineado con el perfil productivo que le viene dando el gobernador Omar Perotti a los puertos públicos de la provincia. En particular, este es un convenio marco con dos inversiones muy importantes: una en el puerto de Reconquista -que dentro de poco vamos a estar haciendo la misma actividad- y por supuesto, las licitaciones que desarrollamos hoy en el Puerto de Santa Fe, alentando el impulso que se le viene dando junto con el Consejo Directivo y la presidencia de Carlos Arese frente al Puerto de Santa Fe. Con muchas inversiones en el 2020, con el transporte de granos y a partir de noviembre de 2021, con el transporte de contenedores desde el puerto santafesino a Asia”.  
  
“La inversión en estas licitaciones, en esta etapa, están por encima de los 200 millones de pesos. Pero tenemos las puertas abiertas de la Subsecretaría de Puertos y Vía Navegables, como para ir actualizando los valores proyectados el año pasado, así que pronto vamos a tener novedades sobre las inversiones finales”, aseguró el funcionario.

Asimismo, Maina destacó que “hace aproximadamente 15 años que no se venían realizando inversiones con un perfil productivo en el Puerto de Santa Fe y eso repercutió en la inactividad del portuaria”. Y agregó: “Ponerlo en valor, ponerlo en funcionamiento, requiere gestionar y llevar adelante inversiones. Y así, el Puerto vuelva a ser un engranaje fundamental para la logística de la región”, concluyó.

Por su parte, el presidente del EAPSF, Carlos Arese, valoró la apertura de ofertas y la participación de las empresas: “Hicimos la apertura de sobres de seis licitaciones que oportunamente habían sido convocadas en el mes de diciembre, todas para obras destinadas a la actividad portuaria, fundamentalmente, dirigidas casi todas a la Terminal de Contenedores”.

Y recordó: “La Terminal de Contenedores comenzó a operar en diciembre y ya llevamos dos embarques, y un tercero que sumaremos en los próximos días. De esta manera, vamos incorporando más carga, más contenedores, pero también más empresas, que es lo más importante”.

“Estamos recibiendo muchas consultas y se van sumando nuevas empresas, porque van viendo que el sistema tiene regularidad, tiene confiabilidad y cumple con las dos frecuencias mensuales que proponemos desde el Puerto de Santa Fe, de cara al mundo”, subrayó Arese.

**Las obras**

LP N°02/2021 - Reconstrucción del talud inclinado - Terminal de contenedores y cargas generales.

Entre las principales tareas se destacan demoliciones varias, sellado de tablestacado,relleno y estabilizado de talud, incluyendo geotextil, reconstrucción de viga de coronamiento superior, vereda y desagüe pluvial, reparación de tensores y la provisión y colocación de colchonetas gaviones.

LP N°03/2021 - Puesta en valor de la Terminal de contenedores y cargas generales.

Las obras contemplan un relleno de densidad controlada bajo losas hundidas, reconstrucción de losas de pavimentos de hormigón, reparación de juntas y fisuras, construcción de caniles y adecuación de galpón existente.

LP N°04/2021 - Puesta en valor del sistema de aspiración de la Terminal Agrograneles.

Las tareas conllevan el relevamiento del sistema de aspiración de polvo y propuesta de mejoras, anteproyecto de sistema de aspiración de polvos en el sector de descarga de camiones y trenes y provisión e instalación de ciclones secos.

LP N°05/2021 - Puesta en valor de la Terminal Multipropósito.

Entre los principales trabajos se destaca el relevamiento del sistema de aspiración de polvo y propuesta de mejoras, el anteproyecto de sistema de aspiración de polvos en el sector de descarga de camiones y trenes y la provisión e instalación de ciclones secos..

LP N°06/2021 - Intervención del muelle de la Terminal Multipropósito.

Comprende el sellado de juntas del muelle y el señalamiento y protección del área verificada.

LP N°07/2021 - Pintura de la Terminal Agrograneles.

Implica la pintura exterior de los silos, sector de manipuleo, galería de embarque y los edificios de carga y descarga de mercadería, de la Terminal Agrograneles.