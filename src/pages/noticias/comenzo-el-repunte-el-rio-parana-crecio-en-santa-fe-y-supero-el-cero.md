---
category: La Ciudad
date: 2021-09-04T06:00:17-03:00
thumbnail: https://assets.3dnoticias.com.ar/BAJANTE.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'Comenzó el repunte: el Río Paraná creció en Santa Fe y superó el cero'
title: 'Comenzó el repunte: el Río Paraná creció en Santa Fe y superó el cero'
entradilla: 12 cm fue el registro del mediodía de este viernes en el puerto local.
  Se espera que en los próximos días supere el metro en la capital provincial. Sin
  embargo, esta mejoría sólo será temporaria.

---
Tal y como se había anticipado, las lluvias en el noreste del país y el sur de Brasil posibilitarán un repunte del Río Paraná en el marco de proceso de bajante histórica que se vive por estos meses.

Ese crecimiento ya se nota en el hidrómetro del puerto de la ciudad de Santa Fe. En las últimas horas el nivel del río creció 18 cm y superó el piso del cero, para colocarse en 12 cm. La anterior medición de Prefectura Naval Argentina en la capital de la provincia había arrojado -0,06 mts.

Con este incremento de la altura espera también que la Laguna Setúbal recomponga un poco su imagen y pueda evitar, por unos días, mostrarse con extensas orillas, saltos y el suelo lagunar a la vista de todos.

Según el último reporte del Instituto Nacional del Agua, el nivel oscilará entre los 0,05 metros y los 0,45. Pero luego continuará en ascenso hasta alcanzar el 14 de septiembre (hasta allí llega el pronóstico) los entre 1,40 metros y los 1,70 metros de altura en la ciudad de Garay.

La “mala noticia” es que tras este “repunte acotado de corto plazo”, como lo nombró el organismo nacional, el Río Paraná volverá a descender el nivel de forma abrupta. Y el escenario actual de bajante continuaría al menos hasta fin de año.