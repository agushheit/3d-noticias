---
category: La Ciudad
date: 2021-08-01T06:00:14-03:00
thumbnail: https://assets.3dnoticias.com.ar/licencias.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Vuelven a prorrogar el vencimiento de las licencias de conducir en Santa
  Fe
title: Vuelven a prorrogar el vencimiento de las licencias de conducir en Santa Fe
entradilla: La secretaría de Control del municipio emitió una resolución que contempla
  los casos caducados entre el 15 de febrero de 2020 y el 31 de diciembre de 2021.

---
La Municipalidad de Santa Fe estableció una nueva prórroga de las licencias de conducir. Se trata de una resolución complementaria a la emitida en mayo pasado, que en este caso contempla los carnés caducados entre el 15 de febrero de 2020 y el 31 de diciembre de 2021.

Cabe recordar que el objetivo es evitar la aglomeración de personas en los centros de emisión de licencias, dándoles a los vecinos y vecinas, la posibilidad de tramitar el plástico paulatinamente. Con esa intención, la secretaría de Control y Convivencia Ciudadana del municipio ya dictó otras normas de similares características, en consonancia con las estipuladas por la Agencia Provincial de Seguridad Vial.

EN ESTA OPORTUNIDAD, LA RESOLUCIÓN INTERNA N° 69 FIJA LA PRÓRROGA DE LA SIGUIENTE MANERA:

· Las licencias de conducir caducadas entre el 15 de febrero de 2020 y el 31 de julio de 2020 tendrán una prórroga de 18 meses, a partir de la fecha de vencimiento

· Las licencias de conducir caducadas entre el 1 de agosto de 2020 y el 31 de diciembre de 2020, tendrán una prórroga de 12 meses, a partir de la fecha de vencimiento

· Las licencias de conducir que caducan entre el 1 de enero de 2021 y el 31 de julio de 2021, tendrán una prórroga de 9 meses, a partir de la fecha de vencimiento

· Las licencias de conducir que caducan entre el 1 de agosto de 2021 y el 31 de diciembre de 2021, tendrán una prórroga de 9 meses, a partir de la fecha de vencimiento