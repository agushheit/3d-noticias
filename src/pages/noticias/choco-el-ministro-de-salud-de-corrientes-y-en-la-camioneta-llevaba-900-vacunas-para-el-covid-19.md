---
category: Agenda Ciudadana
date: 2021-03-09T06:51:56-03:00
thumbnail: https://assets.3dnoticias.com.ar/sputnik-corrientes.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Clarín
resumen: Chocó el ministro de Salud de Corrientes y en la camioneta llevaba 900 vacunas
  para el Covid-19
title: Chocó el ministro de Salud de Corrientes y en la camioneta llevaba 900 vacunas
  para el Covid-19
entradilla: Ricardo Cardozo se descompensó y sufrió un accidente cuando viajaba a
  Goya. La explicación de la Gobernación sobre las dosis de Sputnik V que transportaba.

---
El ministro de Salud de Corrientes, Ricardo Cardozo, sufrió este lunes una descompensación cardíaca y chocó la camioneta que conducía hacia la ciudad de Goya. Dentro del vehículo encontraron dos conservadoras con un lote de 900 vacunas Sputnik V contra el coronavirus, confirmaron fuentes oficiales.

En un comunicado oficial, el Gobierno de Corrientes -a cargo del radical Gustavo Valdés- explicó que "Cardozo trasladaba personalmente en la ocasión las vacunas destinadas a Goya en el marco del Plan de Vacunación contra el Covid-19 que la Provincia lleva adelante en el interior provincial".

Y agregaron que viajaba a Goya "para fortalecer el Plan de Vacunación". Siempre según la información difundida por la Gobernación, Cardozo sufrió una descompensación cardíaca que generó que tuviera un accidente de tránsito "sin consecuencias físicas".

El funcionario circulaba por la Avenida Maipú de la capital provincial hacia la ruta, para dirigirse a Goya, ciudad en la que horas antes había llegado en avión un equipo de Epidemiología para colaborar con el Comité de Crisis, en un intento de controlar un brote local de Covid-19.

El director del Hospital Regional de Goya, Raúl Martínez, confirmó: “El ministro Cardozo me llamó para avisarme que traía las vacunas personalmente. Más tarde, desde Inmunizaciones del Ministerio me avisaron del accidente y el lote llegó a la tarde a través de la empresa encargada de la logística”. 

“Personalmente me encargo de controlar la temperatura de cada lote de vacunas que llegan y las Sputnik estaban con el frío adecuado”, agregó Martínez.

Por su parte, el secretario general de la Gobernación, Carlos Vignolo, indicó que “las vacunas salen con un remito. Después de lo ocurrido volvieron al depósito”.

> La Provincia de Corrientes tiene contratada a Droguería del Sud para la conservación y distribución de las dosis.

¿Por qué esta vez las vacunas eran llevadas por un funcionario en un vehículo conducido por su cuenta, sin un chofer asignado? Según la Gobernación, el ministro Cardozo "decidió trasladar personalmente las dosis destinadas a Goya" después de retirarlas de la droguería "con el remito correspondiente y los elementos necesarios para transportarlas". 

Además, en el comunicado oficial se detalló que llevaba un total de 900 vacunas Sputnik V del componente 1, lote 486010121. Y que las dos conservadoras utilizadas para el transporte estaban "cerradas y selladas", y contenían "placas de PCM y hielo seco".

"Estas son las condiciones habilitadas para el transporte de las mismas, sin ser necesario un vehículo refrigerado para dichas conservadoras", afirmaron.

"Existe un Protocolo de Calificación de Performance para la distribución de vacunas congeladas (igual o menor a -18°C), el cual valida por 96 horas cada conservadora en estas condiciones", agregaron.

Un funcionario de segunda línea del Ministerio de Salud Pública de Corrientes contó que Cardozo suele manejar la camioneta oficial en sus viajes al interior de la provincia. “Incluso el gobernador Valdés suele manejar la camioneta en sus giras”, remarcó.

El episodio protagonizado por el ministro correntino se produjo alrededor de las 9, en la Avenida Maipú esquina Guayquiraró de la capital correntina, donde la camioneta que conducía el funcionario impactó contra una Toyota Hilux.

Aunque no se registraron heridos, el ministro fue trasladado por su descompensación al Instituto de Cardiología de Corrientes, donde lo sometieron a un cateterismo cardíaco que constató lesiones coronarias agudas responsables del evento.

"Fue sometido en forma inmediata a una angioplastia con tres stents coronarios, los que fueron implantados en forma exitosa y sin complicaciones", informaron fuentes de ese centro de salud.