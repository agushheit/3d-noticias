---
category: La Ciudad
date: 2021-05-23T06:15:36-03:00
thumbnail: https://assets.3dnoticias.com.ar/FERIADO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Servicios municipales con motivo del feriado por el 24 y el 25 de Mayo
title: Servicios municipales con motivo del feriado por el 24 y el 25 de Mayo
entradilla: El municipio informa cómo será el funcionamiento de los servicios de recolección
  de basura, transporte público y estacionamiento medido, con motivo del feriado del
  lunes 24 y el martes 25 de mayo.

---
La Municipalidad comunica los horarios y servicios municipales que se prestarán con motivo del feriado nacional de los días lunes 24 (feriado puente) y martes 25, día en el que se conmemora el 211º Aniversario de la Revolución de Mayo.

En ese sentido, se informa que la recolección de basura y barrido de calles serán prestados con normalidad por las empresas Cliba y Urbafe.

**Transporte y estacionamiento**

Por otra parte, el transporte urbano de colectivos tendrá, este lunes y martes, frecuencias equivalentes a los días domingo. En cuanto al sistema de estacionamiento medido (SEOM), no estará operativo.

**Cementerio**

En tanto, de acuerdo a las nuevas disposiciones nacionales y provinciales el Cementerio Municipal permanecerá cerrado a las visitas desde este viernes 21, hasta el 31 de mayo.

En ese marco, durante los días sábados, domingos y feriados se atenderá bajo la modalidad habitual de guardia, mientras que durante los días hábiles se atenderán normalmente todos los trámites.

En tanto, en el marco del feriado del 24 de mayo, los turnos de cremación otorgados para ese día serán adelantados al sábado 22.