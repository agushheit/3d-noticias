---
category: La Ciudad
date: 2021-01-27T10:03:30Z
thumbnail: https://assets.3dnoticias.com.ar/agua.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Diario Uno
resumen: Con pandemia y ola de calor 700 familias de barrio Santo Domingo no tienen
  agua potable
title: Con pandemia y ola de calor 700 familias de barrio Santo Domingo no tienen
  agua potable
entradilla: Analizan realizar acciones de protesta ante la falta de respuesta de las
  autoridades. De los tanques comunitarios que hay en el barrio, el líquido sale turbio
  o directamente no sale

---
En un contexto de emergencia sanitaria, donde mantener la higiene puede salvar vidas, 700 familias del barrio Santo Domingo de Santa Fe no tienen el servicio de agua potable. "La integración urbana de los barrios populares es indispensable para reducir los brutales niveles de desigualdad social y pobreza estructural", señalan desde la Unión de Trabajadores de la Economía Popular (Utep). Y durante la advertencia violeta por la ola de calor que azotó al país, y en particular a la ciudad con una sensación térmica de 44 grados, los vecinos de Santo Domingo no pudieron llenar ni una botella de agua.

"Hace como tres días que nuevamente 700 familias están sin agua potable. Ya son cinco años de reclamos a todos los estamentos del Estado: Nación, provincia y municipio. Los vecinos de Santo Domingo no merecen este tipo de maltrato. Estamos peleando por la obra del agua potable y por la organización e integración urbana a un barrio que le falta todo. Pero en este momento, se suma que esta situación de no tener agua está trayendo problemas de salud a la gente", explicó a UNO Santa Fe Carlos Abad, del Movimiento Popular La Dignidad y de la Utep.

En un comunicado, expresaron desde la organización: "Sin agua potable. Sin tendido eléctrico. Sin cloacas. Sin pavimento ni ripiado. Sin cordón cuneta. Sin recolección de residuos. Sin servicio de transporte. Sin conectividad. Sin espacio público ocioso. Sin plaza. Sin instituciones".

"Queremos que hagan la obra prometida. Ya se cumplió un mes de la presentación de notas. Prometieron una obra para el verano y todavía no arrancaron, iban a poner casa por casa la conexión. Venimos sufriendo esto de que no hay agua o si sale no alcanzan a llenarse los tanques. Con la canilla no llegás ni a llenar una botella, estás dos horas", reclamaron los vecinos esta mañana en un diálogo con LT10.

Los vecinos del barrio cuentan que no hay mantenimiento si no lo realizan los vecinos. "No hay servicio de nada en Santo Domingo. No hay ni un dispensario. Cuando se nos enferma un chico tenemos que ir al centro de salud de Las Lomas porque acá no hay nada", expresaron otros vecinos a UNO.