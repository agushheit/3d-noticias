---
category: La Ciudad
date: 2022-01-08T06:00:57-03:00
thumbnail: https://assets.3dnoticias.com.ar/lacalor.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Ante una nueva ola de calor, la EPE recomienda uso eficiente del servicio
title: Ante una nueva ola de calor, la EPE recomienda uso eficiente del servicio
entradilla: Se pronostica una semana con temperaturas extremas, que exigirá todo el
  parque eléctrico en esta parte del país.

---
Ante los anuncios del Servicio Meteorológico Nacional de varios días con temperaturas muy elevadas para esta región del país, desde la Empresa Provincial de la Energía se reiteran consejos útiles para una utilización más eficiente del recurso energético:  
  
>>La más significativa es usar los equipos de aire acondicionado a no menos de 24°. Cada grado por debajo de esa marca, produce un consumo mayor de electricidad, de entre 5 y 10 %.  
  
>>Limpiar periódicamente los filtros de los equipos acondicionadores de aire y vigilar el mantenimiento del mismo.  
  
>>Mantener cerradas las aberturas de los ambientes con aire acondicionado y evitar fugas que derivan en un derroche de energía y, por lo tanto, de consumo.  
  
>>Utilizar lámparas led.  
  
>>Reducir consumos ociosos de electricidad (luces y electrodomésticos encendidos a solas, sin que ningún miembro del hogar lo esté utilizando, vidrieras durante el día o fuera del horario comercial, burletes de heladeras defectuosos, etc.)  
  
>>Tener en cuenta el etiquetado de eficiencia energética de aires acondicionados, heladeras y lavarropas al momento de adquirirlos. Las más eficientes son de clase A. A mejor clase, menor consumo.  
  
>>El uso eficiente de la energía redunda en solidaridad y mejor aprovechamiento del servicio para todos.  
  
>>Ante inconvenientes en el servicio, los canales para comunicarse en las áreas técnicas Rosario, Santa Fe y Rafaela son: 0800-777-4444, mensaje de texto, la palabra luz, espacio, número de usuario al 22215, a través de la oficina virtual [www.epe.santafe.gov.a](http://www.epe.santafe.gov.ar/)r, Facebook /epeoficial y whatsapp al número 342 5101000.  
  
**Aislados - Covid**  
  
La EPE advierte que, como consecuencia del desarrollo de rebrote de la pandemia Covid19, 374 agentes de distintos sectores de la organización se encuentran aislados o con confirmación de la enfermedad.  
  
Un porcentaje importante de estos empleados, cumplen funciones operativas específicas de atención del servicio en las calles. Esta situación, puede originar eventuales demoras en la solución de algunos reclamos.