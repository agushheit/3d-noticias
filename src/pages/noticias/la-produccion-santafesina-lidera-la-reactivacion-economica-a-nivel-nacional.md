---
category: El Campo
date: 2020-12-06T12:07:36Z
thumbnail: https://assets.3dnoticias.com.ar/produccion.png
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: " La producción santafesina lidera la reactivación económica a nivel nacional"
title: " La producción santafesina lidera la reactivación económica a nivel nacional"
entradilla: 'El Ministerio de Producción, Ciencia y Tecnología relevó inversiones
  privadas por más de 142 mil millones de pesos durante 2020.  '

---
El Ministerio de Producción, Ciencia y Tecnología realizó un relevamiento de los proyectos de inversión del sector privado durante 2020. 

Dicho informe, que previó la recolección de información por sector y un trabajo conjunto con cámaras y entidades gremiales de prácticamente todo el arco productivo, determinó que durante el año 2020 se realizaron inversiones en la provincia por más de 142 mil millones de pesos; vitales indicadores que atienden la generación de demanda y determinan el nivel de actividad económica y empleo.

Al respecto, el ministro de Producción, Ciencia y Tecnología, Daniel Costamagna, valoró que “este desarrollo de nuevos emprendimientos productivos verificado durante 2020 se fundamentó en el esfuerzo realizado, tanto por el sector privado y público, con el propósito de sostener las actividades económicas en Santa Fe a pesar del complejo contexto impuesto por la pandemia de Covid-19. 

El esfuerzo resultó posible gracias a la valiosa tarea y la participación proactiva de las entidades gremiales-empresariales, referentes de las actividades agropecuarias, industriales y del comercio de la provincia de Santa Fe y el fuerte acompañamiento que realizamos desde el Gobierno Provincial”.

Asimismo, el secretario de Industria, Claudio Mossuz, destacó que “Santa Fe está liderando la reactivación de la industria a nivel nacional. Hoy estamos viendo que ninguna otra provincia tiene los indicadores en los niveles que los tenemos aquí, aun a pesar de un año muy complejo por el contexto de pandemia”.

En relación con las causas de estos números, Mossuz indicó que “existe responsabilidad directa de nuestros industriales, pero más allá de sus esfuerzos, nos han hecho saber que fue fundamental el acompañamiento del Estado en todo este tiempo”.

“Otro punto a tener en cuenta es que en Santa Fe la pandemia impactó más tarde que en la provincia de Buenos Aires, hecho que determinó que en el mes de abril nuestras industrias ya estuvieran trabajando al 80% de su capacidad. 

En este sentido también se debe destacar que se llevó adelante una importante tarea conjunto entre sector público y privado para contar rápidamente con protocolos que permitieron volver al trabajo y cuidar la salud”, cerró.

## **INVERSIONES EN SECTORES PRODUCTIVOS SANTAFESINOS**

Los actuales buenos precios de los productos agrícolas y las mejores expectativas para la próxima campaña, junto a la esperada recuperación de numerosas economías nacionales para 2021, entre otros aspectos, impulsan actividades agropecuarias en la provincia de Santa Fe. 

Al respecto, el estimador mensual de actividad económica correspondiente a agricultura y ganadería presentó en septiembre de este año una primera mejora interanual, dejando atrás largos meses de retrocesos.

La importante recuperación de la actividad manifestada en la industria de la maquinaria agrícola refleja la creciente incorporación de equipamientos y, por lo tanto, la concreción de inversiones en el sector agropecuario. 

En septiembre de 2020 la actividad de esta tradicional industria santafesina creció un 62,7% con relación al mismo período de 2019 y el nivel de nuevos patentamientos de maquinaria agrícola, vial e industrial se incrementó desde mayo de 2020, alcanzando en septiembre una mejora del 11% en relación con igual mes del año anterior.

Otro sector de incidencia en el proceso de formación bruta de capital es la construcción, que desde mayo de 2020 manifestó mejoras mensuales en su nivel de actividad aportando nuevas señales acerca de la recuperación de las inversiones. 

Los despachos de cemento crecieron, incluso en su cotejo interanual, mostrando en octubre un incremento del 27,5% en relación con el mismo mes del año pasado, siendo, además, el mayor registro desde el 2017.

La industria frigorífica aglutina en Santa Fe varios proyectos de expansión a partir de la importante demanda externa y la recomposición del consumo interno. 

La lechería, muebles y colchones, biotecnología y la industria química mostraron destacadas inversiones en 2020.

La producción de electrodomésticos amplió la capacidad de producción en algunas de sus plantas, mientras que en la agroindustria se inauguró una importante terminal portuaria.

Las principales inversiones realizadas en la Provincia de Santa Fe durante 2020 según actividades económicas originantes son las siguientes: sector agropecuario 56 mil millones de pesos; industria manufacturera, 51 mil millones de pesos; y otras actividades económicas 34 mil millones de pesos. En tanto que el total de inversiones relevadas en Santa Fe durante 2020 superó los 142 mil millones de pesos.

Otro aspecto a destacar es el claro perfil exportador de varios proyectos de expansión identificados, especialmente en materia de manufacturas de origen agropecuario e industrial. Considerando un tipo de cambio nominal en torno a 75 pesos por dólar promedio durante el año, se estima que las inversiones identificadas por el Ministerio de Producción, Ciencia y Tecnología totalizan 1.900 millones de dólares.