---
category: La Ciudad
date: 2021-07-05T06:15:26-03:00
thumbnail: https://assets.3dnoticias.com.ar/VACUNACION35.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: En Santa Fe ya vacunan a menores de 35 años
title: En Santa Fe ya vacunan a menores de 35 años
entradilla: En la capital de la provincia de Santa Fe se enviaron para la jornada
  dominical 1.200 turnos, en su mayoría para personas que tienen menos de 35 años.

---
El operativo de inmunización contra el coronavirus de la provincia de Santa Fe siguió hoy con turnos para personas menores de 35 años que no poseen comorbilidades, mientras se sigue rastreando a personas mayores de 60 y al personal de seguridad y de otras actividades que por algún motivo no fueron vacunados.

El Ministerio de Salud local informó que, con las 23.887 dosis aplicadas en la jornada de ayer, la provincia ya utilizó 1.818.489 de las 1.902.700 que recibió desde la Nación, lo que representa el 95,57%.

En la capital de la provincia se enviaron para la jornada dominical 1.200 turnos, en su mayoría para personas que tienen menos de 35 años sin comorbilidades, que estaban siendo inoculados en el Centro de Educación Física número 29, situado en avenida Galicia al 2000.

El Gobierno central envió hasta el momento 726.900 dosis de la vacuna Sputnik V, 740.800 de la AstraZeneca y 435.000 de la Sinopharm, contando las del primer y segundo componente.

**Récord**

La provincia de Santa Fe ya inoculó al 72% de las personas inscriptas en el Registro Provincial de Vacunación contra el coronavirus y esta semana marcó un récord de 214.476 turnos para aplicar inmunizantes.

La ministra de Salud, Sonia Martorano, dijo que “los números demuestran que estamos avanzando a pasos agigantados en la campaña de inmunización, algo muy gratificante que nos llena de esperanzas”.

La funcionaria precisó que se enviaron “784.946 turnos en el mes de junio y a eso debemos agregarle que ya se inoculó al 72% de los inscriptos en el Registro Provincial de Vacunación” por lo que, agregó, “actualmente somos el quinto distrito en porcentaje de dosis aplicadas en relación a las recibidas”.

Además, la titular de la cartera sanitaria destacó que se otorgaron 214.476 tunos en el período comprendido entre el lunes 28 de junio y el domingo 4 de julio, lo que constituye “una cifra récord hasta el momento” en Santa Fe.

Martorano celebró esas cifras e insistió, nuevamente, en la necesidad de mantener los cuidados sanitarios para reducir los contagios de coronavirus.

“Si bien tenemos un leve descenso o amesetamiento de los casos, como asimismo de las internaciones, todavía la gravedad de los cuadros que llegan a cuidados intensivos nos preocupa, como también el alto grado de fallecimientos diarios”, dijo la funcionaria según un comunicado del Gobierno.

“De modo tal que no es momento para relajar los cuidados, más bien todo lo contrario”, recomendó, y reiteró que “la vacuna es la segunda herramienta después de la prevención, del cuidado individual y colectivo para evitar los contagios”.