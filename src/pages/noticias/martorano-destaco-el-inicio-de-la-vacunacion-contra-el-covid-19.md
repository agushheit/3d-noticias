---
category: Estado Real
date: 2020-12-31T11:02:20Z
thumbnail: https://assets.3dnoticias.com.ar/3112-martorano.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Martorano destacó el inicio de la vacunación contra el COVID-19
title: Martorano destacó el inicio de la vacunación contra el COVID-19
entradilla: La ministra de Salud realizó un balance sobre la primera jornada de vacunación
  «histórica» contra el COVID-19 en Santa Fe.

---
En el marco de la pandemia, esta semana llegaron a la provincia de Santa Fe las vacunas Sputnik V y el martes comenzó la inoculación a trabajadores de la salud. Al respecto, la ministra de Salud, Sonia Martorano, brindó detalles del operativo y remarcó los resultados obtenidos.

«Ya contamos con un número importantísimo de vacunados en la provincia, algo verdaderamente destacable porque llevamos solo dos días y el nivel de adhesión es muy alto», agregó Martorano.

«Recordemos –continuó– que inicialmente está dirigida a un grupo muy reducido de trabajadores: terapias, laboratorios de biología molecular y al servicio de emergencias y traslados. Y estamos muy contentos porque hasta el momento todo transcurrió sin ningún inconveniente».

En otro orden, destacó la importancia del seguimiento, la trazabilidad y la farmacovigilancia que se hace de la Vacuna Sputnik V y de las personas que la recibieron. «Los datos nominalizados se cargan en el sistema SICAP, por eso podemos ir viendo en tiempo real quién se vacunó, en qué efector, en qué horario y si se reportó algún efecto secundario».

Y agregó Martorano en relación con ello: «Hasta el momento, los efectos fueron los esperados; dentro de las 48 horas de aplicada el paciente puede presentar fiebre o febrícula, dolores musculares o un cuadro seudogripal. Tenemos registrados casos leves en un 0,8 por ciento de los vacunados, un porcentaje mínimo y habitual para cualquier vacuna. Además, todos ellos se encuentran bien».

<br/>

<span style="font-family: Helvetica, sans-serif; font-size: 1.15em; font-weight: 800">**SE ENTREGARON 54 NUEVOS FREEZERS**</span>

Sumados a los ya existentes, la ministra de Salud anunció que se están repartiendo 54 freezers más para que, en cada lugar y estratégicamente, haya cómo garantizar la cadena de frío.

Asimismo, destacó que hoy se sumó un punto nuevo de vacunación. «Hasta el momento son 21 puntos públicos entre provinciales, municipales y comunales. Además de 14 efectores privados, uno más que ayer», cerró.