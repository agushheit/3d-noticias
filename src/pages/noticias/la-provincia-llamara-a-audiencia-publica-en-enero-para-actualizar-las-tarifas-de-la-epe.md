---
category: Estado Real
date: 2021-12-23T06:00:28-03:00
thumbnail: https://assets.3dnoticias.com.ar/EPE.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La provincia llamará a audiencia publica en enero para actualizar las tarifas
  de la EPE
title: La provincia llamará a audiencia publica en enero para actualizar las tarifas
  de la EPE
entradilla: La audiencia se realizará el 7 de enero, a las 9 horas, de manera virtual
  a través de videoconferencia por medio de la plataforma Google Meet.

---
La Empresa Provincial de la Energía, que actúa bajo la órbita del ministerio de Infraestructura, Servicios Públicos y Hábitat, a través de la Resolución N° 1278/21, llevará adelante una audiencia pública con el objeto de escuchar y recabar las opiniones de los santafesinos y santafesinas que deseen expresarse sobre la propuesta de adecuación del cuadro tarifario efectuado por la Empresa Provincial.

En este sentido, desde la empresa de Energía se recordó que durante 2020, la provincia en línea con el gobierno nacional tomó la decisión de congelar las tarifas debido al fuerte impacto en lo sanitario, económico y social de la pandemia. Y que en 2021, se dispuso porcentajes de incrementos máximos escalonados, que se aplicaron en el caso de usuarios residenciales, comerciales, industriales e institucionales, en un 14% para mayo, 8% en julio y 9% en noviembre.

A tal efecto, la audiencia pública constituye el ámbito para escuchar y recabar las opiniones de los santafesinos y santafesinas, y además, rendir cuentas de las acciones, obras y avances realizados por la EPE.

Al respecto, el presidente de la EPE, Mauricio Caussi, aseguró que “desde el gobierno provincial estamos haciendo un esfuerzo muy grande por cuidar el bolsillo de los santafesinos, por cuidar la competitividad de nuestras pequeñas y medianas empresas. En ese marco, la EPE solicita una actualización del cuadro tarifario que no sólo cuide esos elementos, sino que permita la continuidad del flujo de inversiones para sostener la calidad del servicio”, señaló Caussi.

**ORGANIZACIÓN**  
La audiencia pública será presidida por el secretario de Empresas y Servicios Públicos, Carlos Maina, quien tendrá la facultad de conducir el acto, designar un coordinador, impartir instrucciones, efectuar declaraciones y dar las órdenes que considere necesarias para su normal desarrollo. Y como conductora alterna, la coordinadora de Administración, Marcela Basano.

Cabe destacar que en primer término, el representante de la Empresa Provincial de la Energía expondrá los fundamentos para modificar las tarifas y luego se continuará con los oradores inscriptos en el registro.

**PARTICIPANTES Y ORADORES**  
Los interesados en participar de la audiencia pública deberán inscribirse en forma obligatoria en el registro habilitado en: ([https://servicios.santafe.gov.ar/audiencia_sector_ee/](https://servicios.santafe.gov.ar/audiencia_sector_ee/ "https://servicios.santafe.gov.ar/audiencia_sector_ee/")) que estará activo desde el miércoles 29 de diciembre hasta el miércoles 5 de enero, día que cerrará la lista de oradores. Las personas interesadas deberán indicar si su inscripción es en calidad de orador o participante y consignar sus datos personales.

Para todos los participantes que decidan inscribirse en calidad de oradores de la Audiencia, será obligatorio adjuntar el archivo que contenga el escrito correspondiente que manifieste los fundamentos de la exposición, los cuales no podrán exceder de tres carillas, en formato A4, letra Arial 12, interlineado simple. Cualquier inscripción que no cumpla los requisitos determinados en la presente resolución, será pasible de ser rechazada.

El acto se desarrollará de manera virtual a través de videoconferencia por medio de la plataforma Google Meet. Un día antes de su exposición recibirán por el mail informado, la dirección para acceder a la sala meet.

El orden y el tiempo máximo de exposición de los oradores será de 5 minutos para cada uno.

**TRANSMISIÓN**  
La audiencia pública será transmitida en vivo por las redes sociales del Ministerio de Infraestructura, Servicios Públicos y Hábitat para que los santafesinos puedan observarla desde donde lo deseen.