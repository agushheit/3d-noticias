---
category: Agenda Ciudadana
date: 2021-04-01T08:17:38-03:00
thumbnail: https://assets.3dnoticias.com.ar/pobreza.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias/Indec
resumen: En el Gran Santa Fe la pobreza llegó al 39,8%, según el Indec
title: En el Gran Santa Fe la pobreza llegó al 39,8%, según el Indec
entradilla: Así lo indicó el organismo este miércoles. Los datos corresponden al segundo
  semestre de 2020.

---
La pobreza creció al 42% hacia fines de 2020, una cifra que equivale a alrededor de 12 millones de personas, según informó esta tarde el Instituto Nacional de Estadística y Censos (INDEC).

Los datos corresponden al segundo semestre de 2020, lo que significa un incremento de 1,1% respecto del primer semestre de ese año.

![](https://assets.3dnoticias.com.ar/indec.jpg)

De acuerdo a los datos del organismo, en el Gran Santa Fe la cifra llegó al 39,8%. 

El organismo indicó que la indigencia afecta al 10,5% de la población, porcentaje similar al registrado en junio del año pasado.

El porcentaje de hogares por debajo de la línea de pobreza alcanzó el 31,6%; en estos residen el 42,0% de las personas.

De acuerdo con las mediciones oficiales, en los 31 aglomerados urbanos, se encuentran por debajo de la línea de pobreza 2.926.890 hogares que incluyen a 12.000.998 personas, aunque si esta cifra es proyectada a todo el país se eleva a alrededor de 19 millones.

El Indec señaló que dentro de ese conjunto, 720.678 hogares se encuentran por debajo de la línea de indigencia, e incluyen a 3.007.177 personas indigentes, esto es el 10,5% de la población, el mismo nivel de junio, equivalente a 4.700.000 personas.

A fines de 2019, la pobreza afectaba al 35,5% de la población -con lo que aumentó 6,5% en un año- y el 8% de ese total era indigente.

Según las cifras oficiales, a fines de 2020, el ingreso total familiar promedio de los hogares pobres fue de $29.567, mientras la Canasta Básica Total promedio del mismo grupo de hogares alcanzó los $50.854, por lo que la brecha se ubicó en 41,9%, el valor más alto de la serie por quinto semestre consecutivo.

"De esta manera, no solo hubo un aumento en la incidencia de pobreza respecto del primer semestre de 2020, sino que la situación de las personas bajo la línea de pobreza empeoró por la mayor distancia entre sus ingresos y la Canasta Básica Total", explicó el organismo.

[Descargar Informe Completo](https://www.indec.gob.ar/uploads/informesdeprensa/eph_pobreza_02_2082FA92E916.pdf)