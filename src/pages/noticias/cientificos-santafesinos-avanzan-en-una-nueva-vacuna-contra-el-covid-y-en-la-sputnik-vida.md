---
category: Agenda Ciudadana
date: 2021-05-28T08:41:27-03:00
thumbnail: https://assets.3dnoticias.com.ar/vacuna-sputnik-v.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: Científicos santafesinos avanzan en una nueva vacuna contra el Covid, y en
  la Sputnik VIDA
title: Científicos santafesinos avanzan en una nueva vacuna contra el Covid, y en
  la Sputnik VIDA
entradilla: Desarrollan una novedosa vacuna de una sola dosis, que obtuvo resultados
  eficientes "muy potentes" durante la primera etapa. Además, la UNL participa en
  las pruebas de validación de la vacuna Sputnik VIDA.

---
Un equipo de investigadores del Conicet trabaja en el desarrollo de una vacuna contra el SARS-CoV-2. En estudios preclínicos generaron una respuesta inmune muy potente contra el coronavirus, y ahora el diseño de la vacuna es candidata a segunda generación. Es decir, que el proyecto pasa a una segunda etapa más sofisticada. En este marco, el Conicet, la Fundación Instituto Leloir (FIL) y la compañía biotecnológica Vaxinz firmaron un acuerdo para desarrollar la vacuna y llevarla a ensayos clínicos.

Sobre los resultados de los estudios preclínicos obtenidos hasta el momento, "confirman que la vacuna genera una respuesta inmune potente contra el virus SARS-CoV-2 en el 100 por ciento de los animales vacunados y que se mantiene durante al menos cinco meses sin decaer”, afirma Osvaldo Podhajcer, coordinador del proyecto, jefe del Laboratorio de Terapia Molecular y Celular del IIBBA e investigador del Conicet. El grupo de Podhajcer tiene décadas de experiencia en el uso de plataformas de adenovirus para terapia en cáncer y trayectoria en el desarrollo de vacunas experimentales para el virus del papiloma humano (VPH).

![Verónica López , Felipe Nuñez, Eduardo Cafferata y Sabrina Vinzón trabajan en el laboratorio. Foto: gentileza FIL.](https://media.unosantafe.com.ar/p/98152debfcd657017f6a64bed0225e95/adjuntos/204/imagenes/030/041/0030041827/cientificosjpg.jpg?0000-00-00-00-00-00)

Esta vacuna argentina está diseñada en una sola dosis para lograr una inmunidad duradera contra SARS-CoV-2, según señalaron los investigadores. Verónica López, del Conicet en el IIBBA y otra de las líderes del proyecto, afirmó: “Hemos logrado desarrollar esta vacuna de segunda generación con un diseño innovador basada en vectores adenovirales híbridos diferentes a los utilizados en las vacunas actuales basadas en el mismo tipo de vectores y ya aprobadas con carácter de emergencia”.

“Los resultados preclínicos en roedores mostraron que las vacunas desarrolladas inducen anticuerpos que neutralizan al virus como una respuesta inmune celular contra el mismo, que es lo que se busca para lograr protección a largo plazo”, destaca Sabrina Vinzón, colíder del proyecto e investigadora del Conicet en el IIBBA.

**UNL participa en las pruebas de validación de la vacuna Sputnik VIDA**

Por otro lado, la Universidad Nacional del Litoral participa en las pruebas de validación de la Vacuna Sputnik VIDA. Las tareas se llevan a cabo en el Centro de Medicina Comparada (CMC) de la Facultad de Ciencias Veterinarias de la UNL que forma parte del ICiVet-Litoral, instituto de doble dependencia UNL-Conicet. Y participa en el desarrollo de la vacuna contra el Covid-19 en el marco de la investigación impulsada por la Fundación Instituto Leloir (FIL) y el Conicet.

El CMC es una plataforma tecnológica única en su tipo en el sistema científico-académico argentino. Convocado por el Laboratorio Richmond, este Centro de FCV-UNL está realizando pruebas regulatorias para la vacuna Sputnik V contra el Covid-19 que se produce en el país con la fórmula del Centro Gamaleya. “Es un orgullo tanto para el sistema universitario nacional como para la UNL contar con este centro tan importante para la salud pública argentina e internacional. El CMC es una apuesta de nuestra universidad en alianza estratégica con el Conicet que, por sus capacidades, características y certificaciones, es único en su tipo y permite realizar avances claves como el de las pruebas en la vacuna Sputnik V de producción nacional”, aseguró el rector Enrique Mammarella.

“Desde el CMC estamos trabajando en el diseño y ejecución de las estrategias de ensayos preclínicos regulatorios para armonizar no solo los requerimientos de Anmat, sino también de otras agencias regulatorias”, explicó el director del CMC y vicedecano de FCV-UNL, Hugo Ortega.

Luego, Ortega subrayó que “el Centro de Medicina Comparada ha brindado apoyo tecnológico para lograr grandes avances del sector farmacéutico argentino, apoyando a muchos grupos académicos y empresas que tienen interacción con el sector científico, dando soluciones tecnológicas no disponibles previamente en el país. Es una plataforma tecnológica única en su tipo en todo el país ya que cuenta con todas las habilitaciones regulatorias y acreditaciones necesarias para la realización de ensayos preclínicos de alta complejidad”.