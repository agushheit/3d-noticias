---
category: Agenda Ciudadana
date: 2021-07-10T06:00:25-03:00
thumbnail: https://assets.3dnoticias.com.ar/HABILITAMEs.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Santa Fe habilita desde este viernes cines, teatros y casinos
title: Santa Fe habilita desde este viernes cines, teatros y casinos
entradilla: El Gobierno de Santa Fe habilita desde este 9 de julio el funcionamiento
  de cines, teatros, espectáculos artísticos y casinos

---
El gobernador Omar Perotti firmó anoche el Decreto 1.111 que dispone la modalidad de convivencia en la pandemia entre las cero horas de este viernes y el 23 de julio próximo. La norma también habilita la actividad hípica en todo el territorio provincial y extiende los horarios de funcionamiento del sector gastronómico.

Así, bares y restaurantes podrán atender al público hasta las 23 los días viernes, sábados y vísperas de feriados, mientras que lo harán hasta las 21 las jornadas restantes.

El decreto también habilita, en los mismos días y horarios, el funcionamiento de las salas de cines con aforo y una serie de medidas de cuidados establecidas en el Protocolo para Complejos Cinematográficos que rige para todo el país.

Además, regresará la actividad en teatros y centros culturales para actividades musicales –con y sin público– en los mismos días y horarios que la gastronomía y los cines.

También queda habilitada desde hoy la actividad de los casinos en el horario de 10 a 23 los viernes, sábados y víspera de feriados, y hasta las 21 los restantes.

El decreto estipula restricciones a la circulación vehicular desde las 21 a las 6 de la jornada siguiente, con excepción del personal que desarrolla las diferentes actividades consideradas esenciales. Y establece que para concurrir a las actividades habilitadas debe hacerse a pie y a comercios o entidades de cercanía.

La norma mantiene la apertura de actividades deportivas en gimnasios y clubes hasta 10 personas, la pesca deportiva y la navegación recreativa en embarcaciones, así como las actividades religiosas en iglesias y templos.

Finalmente, el horario de apertura de comercios mayoristas y minoristas se mantiene hasta las 19, y continúan cerrados los patios de comida de los shoppings hasta el 23 de julio próximo.