---
layout: Noticia con imagen
author: Fuente. La Capital
resumen: "Soja: siembra récord"
category: El Campo
title: Con agua y precio, la siembra de soja tuvo un arranque récord
entradilla: Tras la recuperación hídrica, se implantó en una semana la mitad de
  la superficie prevista en la región. La cosecha de trigo, con magros rindes.
date: 2020-11-14T17:49:06.558Z
thumbnail: https://assets.3dnoticias.com.ar/foto3.jpg
---
Agua y precio, una combinación de lujo para el inicio de la siembra de soja. Mientras la cotización de la oleaginosa superó a mitad de semana los u$s 420 por tonelada, en la región se aceleran las tareas de implantación.

La Guía Estratégica para el Agro (GEA) señaló que, tras la recuperación hídrica, se sembró 1,7 millón de hectáreas de soja en siete días, todo un récord. Es la mitad de toda el área de primera de la región núcleo.

“Las lluvias de fines de octubre dieron una posibilidad de oro: una ventana de siembra oportuna para toda el área”, señalaron los especialistas del sistema de seguimiento de cultivos de la Bolsa de Comercio de Rosario. El doctor en Ciencias Atmosféricas José Luis Aiello estimó que es posible un regreso de las lluvias en estos días, aunque en forma irregular. Sí señaló que “los modelos son un poco más favorables en las proyecciones para la región pampeana a partir de la segunda década del mes”.

Los trabajos de siembra están a pleno. En siete días se sembraron 1,7 millón de hectáreas, la mitad de la soja de primera. Desde la GEA destacaron que la capacidad de siembra “vuelve a sorprender”, ya que el año pasado se había alcanzado a sembrar 1,1 millón de hectáreas en siete días, algo que parecía insuperable.

Las lluvias de fines de octubre fueron claves para que suceda esto: destrabaron una situación muy difícil y alcanzaron a toda el área, que venía con un déficit hídrico de 120 a 180 mm. En total se avanzó sobre el 52% del área intencionada. La humedad para la siembra es buena, aunque en algunas zonas comienza a escasear.

**Maíz golpeado**

Al maíz, en cambio, le jugaron en contra las heladas tardías y en muchos lotes se ven menos plantas y desparejos. Y esto no es algo que haya pasado en forma puntual, porque de esto hablan los ingenieros de Carlos Pellegrini en Santa Fe, La Violeta en Buenos Aires y Marcos Juárez en Córdoba. Y allí, los técnicos advierten que no se logró el stand de plantas deseado por temperaturas frescas, escasez hídrica y deficiencia de vigor.

“Hay un 20% menos de stand de plantas y desuniformidad temporal. Esto le pone una limitante al techo de rinde. Incluso en ambientes con napa, que se apunta a 140 qq/ha, ahora el potencial sería 125 a 130 qq/ha”, explican los técnicos de la Bolsa de Comercio.

![](https://assets.3dnoticias.com.ar/foto4.jpg)

**El trigo busca el descuento**

En pocos días comienza la cosecha de trigo en la región y el cereal se juega su última carta. A poco de finalizar el ciclo, los días frescos son ideales para el buen llenado de granos y esta puede ser la oportunidad del cultivo para recuperar algunos quintales. En Cañada Rosquín se habla de un aumento de un 40 a 50% del rinde, aunque no dejan de ser rindes bajos: “donde se esperaban 10 qq/ha van a terminar con 15qq/ha y donde se estimaba cosechar 15 qq/ha ahora van a estar en 22 o 24 qq/ha”, dicen desde la GEA.

**Cosecha del centro norte**

En el centro norte de Santa Fe, el trigo ya cosechado muestra una fuerte baja en los rindes respecto de la campaña anterior debido a la incidencia de la sequía en todo el ciclo, por lo que se estima un promedio de no más de 20 ó 21 quintales por hectárea. Así lo reportaron el Ministerio de la Producción de la provincia y la Bolsa de Comercio de Santa Fe en el informe semanal del Servicio de Estimaciones Agropecuarias (SEA).

La cosecha de ese cereal viene “revelando en sus resultados el déficit hídrico sufrido”, cuando se lleva recolectado el 20% de la superficie sembrada, es decir, unas 69.700 hectáreas. Los productores estiman que los rendimientos promedio fluctuarán entre los 20 y los 21 quintales, lo que representaría una baja de entre 13 y 14 quintales de un año para otro, ya que en la campaña 2019 se llegó a 34,25 quintales por hectárea. Esa merma en el rendimiento tendría como resultado la cosecha de alrededor de 720.000 toneladas.

**El precio de la soja vuela**

Por el lado del mercado, el informe mensual de oferta y demanda mundial de granos del Departamento de Agricultura de Estados Unidos (Usda) tuvo impacto netamente alcista en la Bolsa de Chicago, donde el precio de la oleaginosa se disparó al valor más alto de los últimos cuatro años y medio. El miércoles superó los u$s 420 por tonelada.

El Usda calculó la cosecha del país del Norte en 113,5 millones de toneladas de soja, una baja de 2,5 millones de toneladas respecto al informe de octubre. Las cifras quedaron notablemente por debajo de las previsiones formuladas por el mercado. A pesar de eso, la estimación de producción continúa siendo récord para EEUU. Por su parte, las existencias finales pasaron de 7,9 millones de toneladas en octubre a 5,2 millones de toneladas en noviembre.

En lo que respecta a la Argentina, el Usda recortó la estimación de producción en 2,5 millones de toneladas, al pasar de una proyección de 53,5 millones de toneladas en octubre a 51 millones en el nuevo informe de noviembre.

El alza de los precios responde a tres factores: recorte de la oferta en EEUU, algunos pronósticos sobre posibles complicaciones climáticas en el Hemisferio Sur y la demanda china, que ya compró 12 millones de toneladas de soja norteamericana.

“Tras una toma de ganancias importante, el mercado de granos volvió a la suba, impulsado por los viejos fundamentos: una demanda muy activa, stocks declinantes en Estados Unidos y dudas sobre la producción de Sudamérica”, señaló Dante Romano, analista de mercados agrícolas de la Universidad Austral.

Distintos analistas estiman que, aunque el mercado tiene fundamentos alcistas, podría verse en el corto plazo alguna corrección técnica a la baja de las cotizaciones.