---
layout: Noticia con imagen
author: "Fuente: Diario UNO"
resumen: Estafa loteo de Recreo
category: Estado Real
title: Los imputados por la estafa del loteo de Recreo podrán evitar la prisión
  preventiva
entradilla: El juez Héctor Gabriel Candioti aceptó medidas alternativas a la
  prisión preventiva. Con una fianza los cinco acusados podrán transitar el
  proceso en libertad.
date: 2020-11-22T14:10:54.128Z
thumbnail: https://assets.3dnoticias.com.ar/tribunales.jpg
---
Se impusieron medidas alternativas a la prisión preventiva a las cinco personas imputadas por integrar una asociación ilícita que realizaba estafas relacionadas al loteo de un inmueble de 614 hectáreas ubicado en jurisdicción de la ciudad de Recreo.

Así lo dispuso el juez Héctor Gabriel Candioti en una audiencia que comenzó el pasado martes y que, luego de diversos cuartos intermedios, finalizó esta tarde. Se ordenó que cada imputado constituya una caución real que, en total, suman 54 millones de pesos. También deberán cesar en las operaciones del loteo y participar de una mesa de trabajo para solucionar el conflicto a los vecinos damnificados.

“Entre las medidas alternativas, cada imputado deberá constituir una caución real”, señalaron las fiscales del MPA a cargo de la investigación, Mariela Jiménez, María Laura Urquiza y Bárbara Ilera. “De acuerdo a lo informado en la audiencia por los abogados de cada imputado, los inmuebles tienen un valor de entre 5 y 20 millones de pesos”, precisaron las fiscales y aclararon que “entre los cinco bienes, la caución real suma un total de 54 millones de pesos”.

En relación a las otras normas de conducta impuestas, las funcionarias del MPA detallaron que “el juez resolvió que deberán participar de una mesa de trabajo para solucionar el conflicto a los damnificados. Esa mesa deberá estar integrada por escribanos y contadores y se invitará a un representante de la Defensoría del Pueblo de Santa Fe”. También indicaron que “en las reuniones deberán intervenir las personas que vivan en los inmuebles del loteo y también aquellos que compraron lotes y no viven en el lugar”.

“Por otra parte, se dispuso que los imputados deberán abstenerse de cualquier tipo de comunicación entre sí y con las personas perjudicadas por las maniobras delictivas”, valoraron las fiscales. “Asimismo, **no podrán continuar con las operaciones del loteo**”, concluyeron.

“Los imputados aceptaron las medidas alternativas y el juez tuvo por acreditado –en grado de probabilidad– todos los hechos ilícitos que atribuimos desde la Fiscalía a las cinco personas investigadas”, remarcaron las fiscales Jiménez, Urquiza e Ilera.

También señalaron que “**el proceso penal continuará y buscaremos avanzar para lograr condenas para los cinco imputados**”.

Las personas investigadas son un abogado, un escribano y un contador oriundos de la ciudad de Santa Fe. El abogado tiene 46 años y sus iniciales son ARA; el escribano tiene 47 años y sus iniciales son EAL, mientras que el contador tiene 48 años y sus iniciales son OML. Los otros dos imputados son un comerciante oriundo de Recreo de 51 años cuyas iniciales son LOG y un odontólogo de 59 años que vive en Miramar (provincia de Buenos Aires) y cuyas iniciales son OAP.

“**Al abogado, al escribano, al contador y al odontólogo se les atribuyó la coautoría del delito de asociación ilícita, en calidad de organizadores**”, recordaron las fiscales y agregaron que “también se les endilgó la coautoría de los delitos de estafas reiteradas y falsedad ideológica”.

**En relación al comerciante de Recreo, las fiscales del MPA precisaron que “se les imputó la coautoría de los delitos de asociación ilícita –en calidad de miembro–, estafas reiteras y falsedad ideológica.**

“Si bien los hechos ilícitos comenzaron a cometerse en julio del año 2000, la actividad de la asociación ilícita que investigamos se inició en septiembre de 2015”, precisaron las fiscales Jiménez e Ilera. “Las nueve personas que tenemos identificadas –cuatro de las cuales fueron imputadas hoy– formaban parte de esta organización de carácter permanente, estable y organizada que tenía como objetivo darle apariencia legal a la estructura jurídica del fideicomiso creado bajo el nombre de “Barrio Las Mercedes de Recreo”, agregaron.

Las funcionarias del MPA aclararon que “a través de ese fideicomiso disponían de los lotes que conforman el inmueble titularidad de ‘Las Mercedes de Recreo SA’”

“Las maniobras fueron numerosas y de distinto tipo. Incluyen la simulación de una cesión de acciones, la venta de 100 lotes a terceros de buena fe y la autorización para dar en pago 600 lotes más, sin tener el derecho para hacerlo”, subrayaron las fiscales.