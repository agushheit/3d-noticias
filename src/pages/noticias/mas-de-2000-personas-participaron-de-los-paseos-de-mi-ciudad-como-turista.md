---
category: La Ciudad
date: 2021-01-17T10:07:51Z
thumbnail: https://assets.3dnoticias.com.ar/paseos-municipalidad-sta-fe.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: Más de 2000 personas participaron de los paseos de "Mi ciudad como turista"
title: Más de 2000 personas participaron de los paseos de "Mi ciudad como turista"
entradilla: La propuesta se puso en marcha en enero de 2020. Con los protocolos necesarios,
  continúo durante el año. En forma presencial se pueden hacer los sábados; para los
  otros días, se promueve el uso de las audioguías.

---
“Mi ciudad como turista” nació hace un año como una propuesta de verano con cinco grandes itinerarios guiados para recorrer de a pie los espacios emblemáticos de la capital. Esta iniciativa le hizo frente a la pandemia por Covid-19 y, protocolos mediante, continúo durante todo el 2020. Hoy son nueve los recorridos que se pueden hacer y durante este tiempo, 2.057 personas eligieron esta alternativa turística.

La iniciativa sigue vigente de manera presencial durante los sábados enero y febrero con el fin de generar una oferta turística de verano consolidada. El Paseo Boulevard es el que corresponde este fin de semana. La cita es a las 19 horas y el punto de encuentro es la Plaza Pueyrredón, la referencia son los paraguas verde de Santa Fe Capital. Para los otros días, se promueve el uso de las audioguías.

Es importante destacar que el programa fue impulsado por el intendente Emilio Jatón con el objetivo de generar las mejores condiciones para que las santafesinas y los santafesinos desarrollen y/o incrementen la conciencia anfitriona y vocación embajadora. Los nueve paseos invitan a redescubrir la ciudad con ojos de turista.

La iniciativa permite diversificar la oferta turística local, generando una usina de paseos en la ciudad de Santa Fe. Es una herramienta que fomenta el desarrollo del turismo social, porque contribuye a eliminar las barreras económicas, sociales y culturales que impiden a las santafesinas y santafesinos el disfrute de su ciudad.

El programa se ha potenciado a través de las audioguías, que permiten disfrutar de los paseos en cualquier momento del día, eliminando las barreras físicas y temporales. Actualmente todos los establecimientos hoteleros de la ciudad cuentan con acrílicos con el código QR y Spotify para que los turistas puedan escanearlos y acceder a los paseos turísticos.

**Datos**

De las 2.057 personas que participaron de los paseos de "Mí ciudad como turista" desde el 12 de enero de 2020, 289 son turistas-excursionistas cuyas procedencias son 14 provincias argentinas, pero también de 11 países; mientras que 1.768 santafesinas y santafesinos eligieron esta propuesta y llegaron de 28 barrios de la ciudad de Santa Fe.

En este marco y para brindar un mejor servicio, se capacitaron a 57 profesionales Guías y Técnicos en Turismo, quienes hicieron las prácticas en los distintos paseos  turísticos: Costanera; Peatonal; Boulevard; Casco Histórico; Museos y Lugares Históricos; Manzana Jesuítica; Camino de la Constitución; Cementerio; y Paseo del Puerto.

Sobre esta propuesta, el director Ejecutivo de Turismo municipal, Franco Arone, consideró: “Mi ciudad como turista es el programa de sensibilización turística local más importante. Es una herramienta que genera un doble impacto, por un lado incrementa la conciencia anfitriona y vocación embajadora en los santafesinos y santafesinas, que a partir de los nueve paseos guiados y gratuitos pueden redescubrir la ciudad con ojos de turistas. En tanto que estimula la diversificación de la oferta turística, asegurando alternativas de disfrute para los turistas que llegan a la ciudad”.

**Recorridos autoguiados**

Los circuitos que integran el programa “Mi ciudad como turista” tienen la particularidad de poder realizarse de manera autoguiada. Quienes deseen recorrerlos de forma particular pueden acceder a la información necesaria sobre cada atractivo descargando las audioguías desde el sitio de la Municipalidad o escucharlas a través de la plataforma Spotify.

Una audioguía es un relato pregrabado que se ofrece a las personas para completar un tour de manera individual. De este modo, el sistema electrónico permite recorrer cualquier lugar siguiendo la guía que indica información y detalles sobre la historia de lo que se ve, sea una calle, un inmueble o un objeto.

Arone también se refirió a las audioguías y aseguró que “representan una innovación en la manera de recorrer, descubrir y disfrutar la ciudad de Santa Fe” y luego agregó: “A nivel nacional, y recientemente en el Mercosur, se las reconoce como un desarrollo estratégico e innovador que mejora el acceso a la oferta servicios turísticos, e incrementa la competitividad como destino. Los dos desarrollos fueron impulsados por el intendente Emilio Jatón, quien ha definido que el turismo sea una verdadera política de Estado”.