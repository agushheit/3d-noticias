---
category: La Ciudad
date: 2021-11-05T06:00:33-03:00
thumbnail: https://assets.3dnoticias.com.ar/BASURA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Plantean que el relleno sanitario de la ciudad de Santa Fe se convierta en
  metropolitano
title: Plantean que el relleno sanitario de la ciudad de Santa Fe se convierta en
  metropolitano
entradilla: En la reunión realizada este martes, se debatió la disposición final de
  la basura, un problema que padecen las 21 localidades del área metropolitana.

---
Este martes, en la Estación Belgrano, se llevó adelante una nueva reunión del Ente de Coordinación Metropolitana (ECAM). En esta oportunidad, el tema excluyente fue el tratamiento de los residuos sólidos que realizan las 21 localidades integrantes del cuerpo.

 El encuentro fue encabezado por el intendente Emilio Jatón y contó con la participación del secretario Ejecutivo del ECAM, Miguel González; y la directora de Vinculación Institucional del ECAM, María Alejandra Chena. También estuvieron los intendentes de Esperanza, Ana Meiners; y Recreo, Omar Colombo; los presidentes comunales de Pujato Norte, Claudia Pacitti; y Monte Vera, Alberto Pallero; junto con representantes de San José del Rincón, Ignacio Milessimo; Recreo, Fernando Faz; Santo Tomé, Ricardo Méndez; y San Jerónimo Norte, María Laura Vogt Oggier.

Concluida la reunión, González recordó que el Ente ya había abordado la posibilidad de crear un Consorcio para la Gestión Integral de los Residuos Sólidos Urbanos (GIRSU) común para el área metropolitana del Gran Santa Fe, tal lo establecen la ley provincial N° 13.055 de Basura Cero y el estatuto de ECAM. En ese sentido, insistió en que el de los residuos sólidos urbanos “no es un problema de una localidad sino que todas tienen dificultades al respecto. La idea fue que cada una contara los problemas que tiene y las posibles soluciones”, dijo.

 González mencionó que la capital de la provincia “tiene un relleno sanitario que data de hace muchos años pero todavía está al servicio. La idea que el intendente Jatón impulsa es que todos los municipios y comunas del área metropolitana puedan pensar una estrategia conjunta para un futuro relleno”.

 Para abordar este tema, estuvo presente el secretario de Ambiente del municipio, Edgardo Seguro, quien brindó un detallado informe sobre la situación en que se encuentra el relleno sanitario de la capital provincial, cuya ampliación se evalúa actualmente. “Tenemos que contemplar a todas las localidades que llegan hasta el relleno y aquellas que podrían llegar, en virtud de que hay muchas que no llegan a Santa Fe pero podrían hacerlo, porque tampoco tienen relleno y sus residuos representan un grave problema”, explicó.

 En ese sentido, el funcionario indicó que una de las ideas es “denominar al relleno de la ciudad como relleno metropolitano, de modo que se pueda conseguir financiamiento suficiente para su ampliación, en virtud de que los aportes que puede hacer la Nación o la provincia”. Según contó, “el relleno de Santa Fe recibe 15 mil toneladas mensuales (600 al día), de lo cual el 10% pertenece a residuos de otras localidades y otro 10% a empresas que son del área metropolitana”.

Por lo expuesto, Seguro afirmó que “Santa Fe es la instalación sanitaria más grande del noreste del país y una de las más grandes del norte del país. Eso hace que haya que cuidar esa instalación y poner en la agenda que su mantenimiento, su correcta operación y su costo hacen a la vida de los habitantes del área metropolitana”.

 Frente a ello, mencionó que “los intendentes y presidentes comunales se mostraron abiertos y receptivos ante esta posibilidad, sabiendo que es un problema de escala, que nadie puede abordar solo, excepto la ciudad de Santa Fe. Entonces, si todos pudiéramos ir a un mismo objetivo, podría ser una solución para las 21 localidades del área metropolitana”.

 