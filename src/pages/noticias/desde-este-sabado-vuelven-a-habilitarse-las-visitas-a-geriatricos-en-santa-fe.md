---
category: La Ciudad
date: 2021-08-07T06:15:13-03:00
thumbnail: https://assets.3dnoticias.com.ar/geriatricos.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Desde este sábado vuelven a habilitarse las visitas a geriátricos en Santa
  Fe
title: Desde este sábado vuelven a habilitarse las visitas a geriátricos en Santa
  Fe
entradilla: Los residentes deberán tener el esquema completo de vacunación y los visitantes
  deberán estar inoculados con al menos una dosis. ¿Cómo es el protocolo de visita?

---
Con el nuevo decreto provincial que contempla las nuevas reglas de convivencia frente a la pandemia, se habilitaron nuevamente las visitas en los geriátricos de Santa Fe desde el próximo sábado. Se podrá visitar los establecimientos con un estricto protocolo definido por el gobierno provincial punto por punto.

Solo se podrá visitar a residentes que tuvieren el esquema de vacunación Covid completo con al menos diez (10) días corridos de antelación; las personas que realicen las visitas deberán contar con al menos una dosis de las vacunas inoculadas con al menos diez (10) días corridos de antelación, lo que se deberá acreditar al concurrir.

**¿Cuáles son las pautas del protocolo para visitas?**

\- El establecimiento deberá verificar catorce (14) días corridos sin casos positivos de Covid, tanto de personas mayores residentes como del personal. Si se detectasen contagios o casos sospechosos la habilitación de visitas se suspenderá inmediatamente.

\- La persona residente deberá expresar su voluntad de recibir la visita o de realizar una salida, salvo que esté impedido de manifestarse.

\- Ninguna persona debe ingresar a los geriátricos como visita de residentes en el caso que presente o haya presentado en los últimos diez (10) días corridos cualquiera de los siguientes síntomas: temperatura mayor o igual a 37,5°C, tos, odinofagia, disnea, malestar general, pérdida del gusto o del olfato, rinitis o congestión nasal, diarrea o vómitos; sea contacto estrecho de un caso sospechoso o confirmado de Covid en los últimos catorce (14) días corridos; cuente con antecedente de viaje al exterior del país en los últimos catorce (14) días corridos.

\- Quien se presente a realizar la visita o salida deberá suscribir una declaración jurada negativa de los extremos que no se pueden verificar al momento del ingreso para concretar la actividad.

\- Las visitas en los geriátricos se realizarán en espacios abiertos o cerrados con suficiente ventilación natural cruzada; el mobiliario y demás elementos existentes en el mismo deberán ser de plástico o material análogo que permita su fácil y efectiva desinfección.

\- El circuito de ingreso de las visitas debe prever la existencia de zonas de sanitización en sitios cercanos a la entrada del establecimiento, conforme al protocolo.

\- Deberán respetarse estrictamente las reglas generales de conducta y prevención sanitaria en los geriátricos; en particular la disposición y utilización de sustancias sanitizantes de manos y calzado, el uso de tapabocas y el distanciamiento de dos (2) metros entre las personas durante toda la visita. A las visitas previo al ingreso se les realizará un control de temperatura personal; en caso de registrarse una temperatura de 37,5° o más, no se permitirá el ingreso y se contactará al servicio de salud oficial.

\- Las visitas y salidas deberán ser programadas previamente con turnos. El protocolo prevé que en el caso de las visitas sólo podrán ser dos (2) visitantes a la vez por residente, siempre que el espacio destinado a ese fin permita el distanciamiento de dos (2) metros entre las personas; sin poder extenderse por más de treinta (30) minutos y estableciendo que entre visita y visita medie al menos un intervalo de treinta (30) minutos para la desinfección del espacio utilizado y los elementos que contenga.