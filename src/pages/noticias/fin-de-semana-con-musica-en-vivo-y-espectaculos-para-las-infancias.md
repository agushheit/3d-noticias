---
category: Cultura
date: 2021-08-07T06:00:51-03:00
layout: Noticia con imagen
author: 3D Noticias
resumen: Fin de semana con música en vivo y espectáculos para las infancias
title: Fin de semana con música en vivo y espectáculos para las infancias
entradilla: 'De viernes a domingo habrá numerosas propuestas artísticas en los espacios
  culturales de la Municipalidad. '
thumbnail: https://assets.3dnoticias.com.ar/cultura.jpg
link_del_video: ''

---
La cartelera de los espacios culturales se renueva este fin de semana con diversas propuestas, manteniendo los cuidados para poder disfrutar de los encuentros. A los ciclos semanales que se desarrollan en la Estación Belgrano y en el Mercado Progreso se suman esta semana las funciones del Festival de Títeres Trotamundos, que organiza la Municipalidad en el Mes de las Infancias. En la sala Marechal del Teatro Municipal “1° de Mayo” habrá funciones de viernes a domingo, a las 18, con entrada gratuita. Los pases deben retirarse previamente en la boletería, hasta dos por persona.

Continúa Sala 5/360, el ciclo de música en vivo con sonido cuadrafónico que se desarrolla en la sala 5, ubicada en la planta alta de la Estación Belgrano. Oscar Frodo Peralta y Gaspar Macor protagonizan esta nueva fecha con Mbojeré dúo, el proyecto en el que abordan un repertorio de música instrumental latinoamericana integrado principalmente por composiciones originales, algunas de ellas inéditas, recorriendo ritmos como el chamamé, rasguido doble, valsa, choro, joropo y polca. El dúo, que en 2018 editó su primer disco “Cimbroneando”, busca en sus composiciones y arreglos incorporar al folklore latinoamericano diferentes recursos que explotan al máximo las posibilidades tímbricas de la guitarra, sin perder las características esenciales de cada género.

La próxima fecha de Sala 5/360 será el viernes 13 de agosto, con Palo Ruiz; el 20, con Nilda Godoy & Cacho Hussein y el 30, con Seba Barrionuevo. Las entradas son gratuitas. Se retiran hasta dos por persona el mismo día del concierto desde las 17 horas, en el ingreso por Boulevard Gálvez 1150.

**Noche de jazz**

El espacio ubicado en Balcarce 1625 invita también este viernes, desde las 18, a una Noche de Jazz. El ingreso del público se permitirá por orden de llegada, hasta completar el 30% de la capacidad del espacio, habilitada por protocolo.

Con un repertorio de standards del género se presentarán Hugo García en batería, Cristian Bórtoli en contrabajo, Seba López en guitarra y Pepi Dallo en trompeta, reunidos en su proyecto Cool Jazz Quartet.

Fer Lagger Ska Jazz también será parte de esta noche, con clásicos del jazz y del ska. Su propuesta en el escenario reúne a 13 intérpretes, en una fusión que definen como “muy divertida, alegre y bailable”: Fernanda Lagger en dirección, arreglos y saxo tenor; Matías Amerise y Torcuato Arzeno en trombones; Emiliano Dean en saxo alto, Ivana Papini en clarinete, Fernando Lara en saxo tenor, Rodolfo Mena en batería, Alexander Russelwhite en bajo eléctrico, Gonzalo Bonaveri en guitarra, Luciano Ortiz en percusión y Agustín Leyendecker en teclado.

**Noche de rock**

Super Sopa y TEN protagonizan el sábado la Noche de rock, también desde las 18. Flaco Ferrero, Gabo Barukel, Fer Gómez, Gugui Morana y José Domínguez integran Super Sopa, una banda santafesina de rock que tiene como premisa la convergencia, principalmente desde lo musical pero en un sentido más amplio. La conformación y consolidación del grupo se dio este año, con la grabación y proyección de su primer material discográfico Vol. 1, estrenado en junio de 2021 y disponible en todas las plataformas digitales de música. Con un sonido moderno, crudo y sanguíneo, las canciones son el producto del trabajo compositivo desde lo grupal, lo colectivo, y hablan de diferentes temas y posicionamientos que tratan de dar una visión común, fresca, lúdica, comprometida e inclusiva, invitando a quien escucha tanto a que se divierta, baile y/o reflexione.

La propuesta de TEN tiene como epicentro la canción: composiciones propias, personales y profundas que escapan de las recetas de un género específico. En general aglutinado con una base de rock potente, las melodías y acordes, timbres y ritmos se enriquecen con elementos del jazz, el pop, el reggae, músicas populares latinoamericanas y cualquier ingrediente expresivo que aporte a la canción. El grupo está integrado desde 2016 con Bernardo Aguirre en guitarra y voz, Valentín Gatti en batería, Lisandro Schurjin en sintetizadores, a los que se sumó en 2021, Fernando Silva en bajo. El compromiso con la calidad de la música, y la incorporación en 2019 de Santiago Torres como ingeniero de sonido, condujo a que las presentaciones en vivo suenen poderosas y equilibradas. Cuentan además con dos trabajos producidos de forma independiente: el LP “Viejas Tecnologías”, de 2017; y el EP “Flores y Antenas”, de 2020; mientras trabajan en su próximo disco.

**Tardecitas en el Mercado**

El domingo desde las 15 horas, la Maga Sultana (Jimena Medina) y los magos Fernán (Fernando Conti), Lucas (Lucas Soto Payva) y Mayers (Marcelo Reynaudo) invitan a divertirse en familia y participar de su show “Magia Re magia”. También se podrá disfrutar de “¡Auch!”, un espectáculo de humor, magia, circo y clown con el payaso, equilibrista y malabarista Cebolla, la payasa Ortensia en acrobacias de piso y aéreas, la Payasa Margarita con juegos y globología, haciendo de las suyas junto a la payasa Carlota.