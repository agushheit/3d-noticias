---
category: Deportes
date: 2021-12-19T09:26:25-03:00
thumbnail: https://assets.3dnoticias.com.ar/RIVER.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: River fue demasiado para Colón y lo dejó sin su segundo título
title: River fue demasiado para Colón y lo dejó sin su segundo título
entradilla: 'En la final del Trofeo de Campeones, River goleó a Colón por 4-0 y se
  quedó con el título. El Sabalero cometió errores en defensa que pagó muy caro.

'

---
River terminó siendo demasiado para la ilusión de Colón y el equipo sabalero fue goleado por 4-0 en la final del Trofeo de Campeones. Hasta el primer gol del Millonario, el Sabalero venía jugando un partido aceptable, en cuanto al orden táctico y el rigor para marcar. De mitad de cancha hacia atrás era correcto. Pero nunca tuvo argumentos como para lastimar al merecido campeón.

El gol de Julián Álvarez inclinó la balanza para el lado de River y mucho más cuando llegó el segundo para comenzar a sentenciar la historia. Colón a diferencia del Clásico mostró actitud, pero la diferencia entre un equipo y otro es muy notoria y a partir de allí se explica el resultado final.

En un primer tiempo parejo y en donde Colón neutralizaba los intentos de River, apareció Julián Álvarez a los 41' para establecer el 1-0. Hasta allí poco había pasado frente a los arcos y el partido se jugaba como quería el Sabalero, incomodando al elenco millonario. La pelota era patrimonio de River pero no encontraba los espacios, como consecuencia de la disciplina táctica con la que jugaba el Rojinegro

Pero el equipo que dirige Marcelo Gallardo tiene a un delantero de enorme jerarquía como Álvarez quien dispuso de una chance y no perdonó. Incluso el gol tuvo una dosis de fortuna, ya que la jugada se inició con un rebote que dio en el cuerpo de Rodrigo Aliendro, Santiago Simón aprovechó para meter el centro y la aparición del delantero que con un toque de zurda anticipó a Bruno Bianchi y dejó sin reacción a Leonardo Burián.

Colón atacaba por derecha con las proyecciones de Eric Meza, quien subía con mucho criterio pero no resolvía bien en el último pase. Facundo Farías se movía con inteligencia, pero a veces quedaba lejos del arco. El Sabalero no inquietaba a Franco Armani, pero no sufría en defensa. Apretaba bien en la zona media para no dejar jugar a los volantes internos y luego ganaba en los duelos mano a mano.

Por momentos el encuentro parecía una partida de ajedrez, mucha contracción a la marca, al repliegue, pero poco espacio para la creatividad. Ambos cumplían con eficacia una parte del juego, pero no tenían claridad cuando debían proponer en ataque. En el inicio del juego había avisado Álvarez pero con un remate que no parecía peligroso aunque Burián terminó dando un rebote largo.

El primer tiempo no se jugó bien y es lógico teniendo en cuenta que se trataba de una final. Con dientes apretados, ninguno de los dos equipos escatimó sacrificio y también existieron infracciones fuertes. Al punto tal que en esos primeros 45' hubo cuatro amonestados, dos por cada equipo. Tácticamente el planteo inicial de Eduardo Domínguez fue acertado, pero le faltó determinación en ataque.

Y en el segundo tiempo Colón intentó reaccionar, pero no tuvo manera de hacerlo. Rápidamente River estuvo cerca del segundo con un remate de Simón que se fue por encima del horizontal. El Sabalero tuvo un poco más la pelota en el comienzo, pero le faltaba claridad y el Millonario estaba agazapado para la contra.

Y a los 12' llegó el segundo gol, una mala salida de Rafael Delgado propició que nuevamente Julián Álvarez apareciera dentro del área para definir. Aunque a diferencia del primero en este debió probar en dos oportunidades. El delantero anticipó a Paolo Goltz tapó Burián y en el rebote la toco de derecha para el 2-0.

A partir de allí todo se le hizo cuesta abajo, Domínguez movió el banco metiendo tres cambios y buscando la reacción. Pero esos dos goles eran demasiada ventaja. Colón no supo como inquietar a Armani. Apenas un centro de Facundo Mura que Aliendro no llegó a conectar. Y un remate de Farías que el arquero de River controló sin problemas.

La sensación siempre era de que River estaba más cerca del tercero que Colón el descuento. Y a los 39' Benjamín Rollheiser decretó el 3-0. El delantero que había ingresado hacía pocos minutos aprovechó un rebote en el palo izquierdo luego de un disparo de Álvarez y con la pierna derecha empujó el balón pese a la estirada de Burián.

Pero faltaba un gol más para consumar la goleada, Colón ya era un desconcierto total en defensa y el que aprovechó fue Jorge Carrascal a los 45' quien ingresando por izquierda metió un remate rasante y bien esquinado para el definitivo 4-0. River aprovechó todas las ventajas otorgadas por Colón y fue un justo campeón del Trofeo de Campeones.

Síntesis

Colón: 1-Leonardo Burián; 21-Eric Meza, 2-Bruno Bianchi, 6-Paolo Goltz, 40-Rafael Delgado; 23-Christian Bernardi, 14-Federico Lértora, 29-Rodrigo Aliendro, 11-Alexis Castro; 16-Cristian Ferreira y 10-Facundo Farías. DT: Eduardo Domínguez.

River: 1-Franco Armani; 2-Robert Rojas, 17-Paulo Díaz, 6-Héctor Martínez, 20-Milton Casco; 5-Bruno Zuculuni, 8-Agustín Palavecino; 31-Santiago Simón, 13-Enzo Fernández, 26-José Paradela y 9-Julián Álvarez. DT: Marcelo Gallardo.

Goles: PT 41' Julián Álvarez (R), ST 12' Julián Álvarez (R), 39' Benjamín Rollheiser (R), 45' Jorge Carrascal (R).

Cambios: ST 17' Jorge Carrascal x Palavecino (R), 18' Santiago Pierotti x Ferreira (C), Lucas Beltrán x Bernardi (C), Facundo Mura x Meza (C), 31' Leonardo Ponzio x Fernández (R), Tomás Galván x Simón (R), Benjamín Rollheiser x Paradela (R), 36' Nicolás Leguizamón x Farías (C).

Amonestados: Martínez y Zuculini (R), Castro, Lértora y Goltz (C).

Expulsados: Delgado (C).

Árbitro: Patricio Loustau.

Estadio: Madre de Ciudades.