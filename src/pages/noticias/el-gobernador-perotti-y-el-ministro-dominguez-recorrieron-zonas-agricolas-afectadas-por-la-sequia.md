---
category: Estado Real
date: 2022-01-16T06:00:58-03:00
thumbnail: https://assets.3dnoticias.com.ar/squia.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: El gobernador Perotti y el ministro Domínguez recorrieron zonas agrícolas
  afectadas por la sequía
title: El gobernador Perotti y el ministro Domínguez recorrieron zonas agrícolas afectadas
  por la sequía
entradilla: 'El mandatario santafesino y el titular de la cartera agropecuaria de
  la Nación, visitaron campos afectados por la ausencia de lluvias. '

---
El gobernador de la provincia, Omar Perotti; y el ministro de Agricultura, Ganadería y Pesca de la Nación, Julián Domínguez, realizaron durante este sábado una recorrida por establecimientos rurales ubicados en las zonas más afectadas por la sequía.

En la oportunidad, remarcaron que desde el Gobierno Nacional se está trabajando junto a Santa Fe y otras jurisdicciones del país para “brindar soluciones concretas a los productores”, al tiempo que anunciaron la actualización del fondo de emergencia agropecuaria, que este año se incrementará por decisión del presidente Alberto Fernández “entre 10 y 12 mil millones de pesos”.

En uno de los establecimientos recorridos, el gobernador Perotti declaró que “durante la semana el tema de la sequía fue tratado en reuniones con el ministro (Domínguez) en Buenos Aires y era importante hacer esta visita para ver la situación real, particularmente con el maíz de primera. Ver prácticamente un cultivo sin granos genera preocupación en la cadena productiva”, dijo.

El mandatario santafesino agregó que la crítica situación por la ausencia de lluvias “deja sin capital de trabajo” y “compromete a la explotación lechera por la falta de un insumo clave”; y destacó que junto a Nación “se instrumentará la distribución de los fondos de la emergencia para que lleguen lo más ágil y rápido posible”.

Perotti remarcó, además, que “el productor debe saber que estamos trabajando sobre esta necesidad. Siempre que se dio este tipo de instancias, de emergencias, las Asociaciones para el Desarrollo junto al Ministerio de la Producción ponen a disposición las herramientas para llegar con la asistencia de la manera más directa”.

**“LA DECISIÓN ES ACOMPAÑAR”**

Por su parte, el ministro Domínguez, detalló que "lo que nos ha indicado el presidente Alberto Fernández es la actualización del fondo de emergencia, una necesidad y pedido histórico de los productores. Los gobernadores hablaron con el Presidente y él me pidió que esté aquí, junto a nuestros productores. La decisión es acompañar en este momento de incertidumbre, consecuencia de la sequía", señaló el titular de la cartera agropecuaria nacional.

Y expresó: "Es un tema que monitoreamos permanentemente. El objetivo es la actualización del fondo de emergencia para que sea nuevamente un instrumento de respuesta rápida ante las pérdidas. Esperemos que en las próximas horas el tiempo alivie las condiciones, pero lo que se perdió se perdió y hay que trabajar sobre la recuperación de capital de trabajo”.

“El gobernador Perotti es un referente de consulta permanente del Presidente. Santa Fe para nosotros es una prrovincia testigo de cómo se debe trabajar en estos casos. El cambio climático y las consecuencias de las sequías vinieron para quedarse. Ante esta situación, se precisa que tengamos también instrumentos de riego para los pequeños y medianos productores, con un financiamiento diferenciado, de adquisición directa, de la industria nacional y con tecnología nacional", finalizó el ministro.