---
category: Agenda Ciudadana
date: 2021-10-26T06:15:17-03:00
thumbnail: https://assets.3dnoticias.com.ar/gerogieva.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Alberto Fernández se reunirá con Kristalina Georgieva por la deuda argentina
  con el FMI
title: Alberto Fernández se reunirá con Kristalina Georgieva por la deuda argentina
  con el FMI
entradilla: El Presidente y la directora del Fondo Monetario Internacional se encontrarán
  en Roma el próximo fin de semana para tratar, entre otros temas, el planteo de un
  mayor plazo para los vencimientos próximos.

---
El presidente Alberto Fernández y la directora gerente del Fondo Monetario Internacional (FMI) Kristalina Georgieva se reunirán el fin de semana próximo en Roma, donde el mandatario argentino participará de la Cumbre de jefes de Estado del G20, informaron hoy fuentes gubernamentales.  
  
Fernández tomará parte de la reunión del G20 en la capital italiana el sábado y domingo venideros, antes de trasladarse a la ciudad escocesa de Glasgow para intervenir en la Conferencia de las Naciones Unidas sobre el Cambio Climático (COP26).  
  
De este modo, el jefe de Estado argentino y Georgieva se verán por segunda vez en menos de seis meses, luego del encuentro que mantuvieron también en Roma el 14 de mayo último.  
  
En aquella oportunidad compartieron el primer encuentro presencial, una reunión "constructiva y franca" de una hora y media en el hotel Sofitel, en la que acordaron llegar a un acuerdo "lo más rápido posible" sobre la renegociación de la deuda, sin que se le "exija mayores esfuerzos al pueblo argentino", según le planteó el mandatario en el encuentro.  
  
Fernández estará acompañado en Roma por el canciller Santiago Cafiero y el ministro de Economía Martín Guzmán.  
  
El mandatario argentino planteará un mayor plazo para el pago de la deuda con el organismo internacional de crédito, con el cual tiene vencimientos por US$ 18.000 millones en 2022, US$ 19.000 millones en 2023 y US$ 5.000 millones en 2024.  
  
Ya el 22 del mes pasado la Argentina canceló con el FMI una deuda por US$ 1.885 millones que se cubrieron con los Derechos Especiales de Giro (DEG), los que llegaron el 23 de agosto último como parte de la asistencia del Fondo por la pandemia a sus países miembros, equivalente a US$ 4.334 millones.  
  
Para lo que resta del año, el Gobierno debe pagar US$ 400 millones en intereses el mes próximo y otros US$1.880 millones el 22 de diciembre.  
  
Además, Fernández propondrá la cancelación de la sobretasa del 2% que se le cobra a los países que recibieron un monto superior a la cuota que aporta a ese organismo multilateral de crédito, luego de que el país recibiera US$ 44.000 millones en la administración del expresidente Mauricio Macri.  
  
Además, el mandatario insistió en la cumbre latinoamericana sobre cambio climático denominada Diálogo de alto nivel sobre acción climática en las Américas, en la que participó por videoconferencia el 8 del mes pasado, en que "los canjes de deuda por acción climática, los mecanismos de pago por servicios ecosistémicos y el concepto de deuda ambiental que ostentan los países menos desarrollados son otras claves para la salida de la crisis" del medioambiente.