---
category: Estado Real
date: 2021-10-19T06:00:16-03:00
thumbnail: https://assets.3dnoticias.com.ar/ferrocarril.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Perotti: "Las inversiones en el ferrocarril nos ayudan a generar expectativas
  de inversión y generación de empleo"'
title: 'Perotti: "Las inversiones en el ferrocarril nos ayudan a generar expectativas
  de inversión y generación de empleo"'
entradilla: El gobernador Omar Perotti y el ministro de Transporte de la Nación, Alexis
  Guerrera, participaron en el primer viaje del tren de pasajeros que volvió a cruzar
  la Laguna La Picasa.

---
El gobernador de la provincia de Santa Fe, Omar Perotti, y el ministro de Transporte de la Nación, Alexis Guerrera, participaron este lunes en el primer viaje del tren de pasajeros que volvió a cruzar la Laguna La Picasa.

Luego de la paralización del servicio de cargas y pasajeros que une las provincias de Santa Fe y Buenos Aires, producto de una inundación en la región en 2017, hoy se realizó el primer viaje del restablecido servicio entre Junín y Rufino, que reduce 24 horas el transporte de cargas.

Las obras de reparación de 13 kilómetros del pedraplén sobre la laguna comenzaron en diciembre de 2019 y se mantuvieron durante todo el 2020 y 2021, pese a la pandemia, y demandaron una inversión de 200 millones de pesos.

En la oportunidad, Omar Perotti afirmó que "hay una pertenencia al ferrocarril. Cuando necesitamos que el país se ponga en marcha y crecer en la generación de divisas, aparecen las posibilidades del potencial de nuestro sector agropecuario, agroindustrial y agroalimentario. Allí tenemos la capacidad, el conocimiento y la fortaleza de nuestros productores para hacerlo. Para que ese esfuerzo rinda y vuelva de la mejor manera, aparece la logística y los costos. La combinación de camión, ferrocarril e hidrovía es la que nos permite menores costos de logística", sostuvo el gobernador.

Y agregó: "Nuestra provincia encabeza los niveles de inversión y actividad productiva en todos sus sectores y en recuperación del empleo registrado. Sin dudas, las inversiones en el ferrocarril nos ayudan a empujar y a generar expectativas. El ferrocarril vuelve a dar esa mirada, a demostrar que no hay que irse, que las posibilidades del día a día están. Por eso, el agradecimiento por esta vinculación que nos ayuda enormemente en la provincia de Santa Fe. Rosario-Cañada, Santa Fe-Laguna Paiva, Rosario-Empalme, son algunos de los desafíos que le van a permitir a nuestra gente movilizarse de otra manera, hacerlo mucho más sustentable y, fundamentalmente, acortando las distancias y generando ese vínculo que habíamos perdido", puntualizó Perotti.

Por su parte, el ministro Guerrera sostuvo que "hoy estamos reivindicando el sistema ferroviario y la resistencia de los trabajadores del sistema ferroviario que nunca aflojaron. También a vecinos y ciudadanos que vieron dejar pasar el ferrocarril. En cada lugar que vamos está esa emoción, añoranza y el agradecimiento de todo un pueblo que reconoce la importancia, en este caso, del tren de pasajeros".

El titular de la cartera de Transporte agregó que “estamos en una provincia estratégica para la producción nacional, para las exportaciones, para mejorar la competitividad y para salir al mundo. Santa Fe tiene la mayor cantidad de los puertos que le permiten a la Argentina exportar su riqueza. Con Trenes Argentina Cargas estamos mejorando esas vías y ese material rodante en todo el norte argentino para llegar de mejor manera a los puertos. El gobernador gestiona permanentemente, con Trenes Argentinos, los mejores accesos a esos puertos", finalizó Guerrera.

**DETALLES DE LA OBRA**

En el primer viaje, que contó con una locomotora 0km fabricada en Argentina, estuvieron a bordo también, los presidentes de Trenes Argentinos Cargas (TAC), Daniel Vispo; y de Trenes Argentinos Operaciones (TAO) y Trenes Argentinos Infraestructura (TAI), Martín Marinucci.

La obra, ejecutada por TAC, mejorará notablemente los tiempos de viaje del tren de cargas, que debía recorrer 333 kilómetros más para evitar la laguna, lo que equivalía a 24 horas más de viaje. Además, permite unir nuevamente la ciudad de Junín con Rufino para el transporte de pasajeros, restableciendo el servicio operado por TAO.

Al respecto, Vispo destacó que “pudimos conectar nuevamente Junín, con Rufino, por la traza original de nuestra línea San Martín, y nos llevó dos años poner en valor esta vía que nos genera tantos beneficios”; y precisó que esta obra consistió en “la reconstrucción de 13 kilómetros del pedraplén de la laguna La Picasa, con más de 30 mil toneladas de ripio y el armado de las vías”, recordó.

Por último, Marinucci señaló que “cuando asumimos la gestión empezamos a ver cómo hacíamos para poner al sistema ferroviario en el lugar que supo estar, y parte de este desafío era conectar a cada argentino y argentina a través de las vías”.

En este sentido, destacó “la obra de La Picasa, para poder hoy haber llegado con este primer tren, y contarles que a partir de noviembre van a tener la posibilidad de subirse al tren en Rufino y llegar a Retiro, en la ciudad de Buenos Aires. Una obra que sólo se puede hacer articulando Estado con privados, con el compromiso que cada trabajador y trabajadora ferroviaria”, concluyó Marinucci.

**PRESENTES**

También estuvieron presentes en el acto, el secretario de Transporte de la Nación, Diego Giuliano; la ministra de Infraestructura, Servicios Públicos y Hábitat, Silvina Frana; la senadora nacional por la provincia de Santa Fe, María de los Ángeles Sacnun; el intendente de Rufino, Natalio Lattanzi; el intendente de General Pinto, Alfredo Zavatarelli; y demás legisladores, autoridades y representantes gremiales.