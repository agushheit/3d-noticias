---
category: La Ciudad
date: 2021-04-23T07:36:10-03:00
thumbnail: https://assets.3dnoticias.com.ar/Lucas-Simoniello.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Lucas Simoniello
resumen: Se podrán subdividir terrenos para construir más viviendas
title: Se podrán subdividir terrenos para construir más viviendas
entradilla: El Concejo Municipal aprobó un proyecto de Lucas Simoniello que tiene
  como objetivo facilitar la división de lotes, buscando así incentivar la construcción
  de viviendas.

---
l Concejo Municipal aprobó un proyecto de Lucas Simoniello que tiene como objetivo facilitar la división de lotes, buscando así incentivar la construcción de viviendas. “Sabemos lo difícil que es adquirir un terreno en la ciudad, sobre todo en estos momentos, por eso aportamos una herramienta para que más santafesinos puedan acercarse al sueño de la casa propia” afirmó el concejal.

Simoniello hizo referencia al “gran porcentaje de vecinos que deben dedicar la mayor parte de sus ingresos al alquiler y a quienes, a pesar de tener la suerte de tener un trabajo estable y le sobran algunos pesos a fin de mes, su limitada capacidad de ahorro no les permite proyectar a futuro. Desde nuestro espacio y por la responsabilidad que nos dan los santafesinos de trabajar por la ciudad, proponemos nuevas herramientas que nos permitan abordar estas situaciones, sumando aportes de profesionales y vecinos”.

“Tenemos que plantearnos como objetivo que los santafesinos puedan volver a soñar con la casa propia; que el que se esfuerce y ahorre pueda edificar y construir un hogar para su familia” aseguró Simoniello y agregó que: “muchos tenemos algún amigo o familiar que en los últimos años se mudó a localidades del área metropolitana como Recreo, Santo Tomé o Arroyo Leyes. Mientras que también hay muchas personas que tienen lugar en sus terrenos como para subdividirlos y que ahí sus hijos, familiares o amigos puedan aprovechar para construir sus viviendas”.

“Algo para destacar también es que la construcción puede ser uno de los grandes dinamizadores de la economía local” -afirmó el concejal- Sin dudas contribuye a reactivar las industrias relacionadas a la construcción, genera mano de obra y compra de materiales, entre otras cosas”.

**Sobre la ordenanza**

Esta ordenanza facilita la división de lotes, para que, donde según la normativa hasta hoy vigente, se puede construir solo una vivienda, se puedan realizar dos o más casas individuales. También se incluye la posibilidad de poder construir viviendas arriba o atrás de otras ya existentes, siempre y cuando el espacio lo permita. “Este tipo de construcciones también son aptas para el uso de créditos como el Procrear o el recientemente anunciado Programa Casa Propia” destacó Simoniello.

El concejal resaltó que “la ordenanza fue sancionada por amplio consenso” y remarcó que “es fruto de un trabajo en equipo y se nutrió tanto del aporte de especialistas y colegios profesionales con los que hemos trabajado en este último tiempo, como de las charlas con muchos vecinos y vecinas de la ciudad que esperan la posibilidad de construir su vivienda”.

“Con este trabajo participativo llegamos a esta herramienta simple e innovadora para multiplicar la tierra disponible para poder construir en la ciudad” afirmó Simoniello. “Con esta ordenanza no pretendemos sólo aumentar la cantidad de propietarios, sino multiplicar las posibilidades de alcanzar el tan buscado sueño de la casa propia. Se trata de ampliar oportunidades para el acceso a la ciudad, que significa sentirnos incluidos en ella, viendo una posibilidad de construcción de viviendas” finalizó.