---
category: Estado Real
date: 2021-01-09T11:04:31Z
thumbnail: https://assets.3dnoticias.com.ar/090121-verano-activo.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Verano Activo ya está en marcha en la capital provincial
title: Verano Activo ya está en marcha en la capital provincial
entradilla: 'El primer encuentro de Verano Activo, impulsado por el Ministerio de
  Educación de la provincia, se llevó a cabo en el espacio cultural El Alero, ubicado
  en el barrio Coronel Dorrego de la ciudad de Santa Fe. '

---
El secretario de Educación, Víctor Debloc, y la secretaria de Gestión Territorial Educativa, Rosario Cristiani, visitaron el espacio cultural El Alero donde se realizó el primer encuentro de la línea de trabajo **Verano Activo.** 

Allí se encontraron con el primer grupo de chicos y chicas que participaron de las actividades lúdicas y deportivas, coordinadas por los equipos recreativos integrados por profesores de educación física  y trabajadores culturales y sociales residentes.

Participaron también de la visita: la directora provincial de Programas Socioculturales del Ministerio de Cultura de la Provincia de Santa Fe, Mariana Escobar; el director provincial de Educación Física, Alfredo Giansily; y la coordinadora del Área de Educación Física de la cartera educativa, Alejandra Pochón.

En dicho encuentro, Debloc señaló: «esta es una experiencia muy valiosa donde hemos visto a los chicos y chicas muy entretenidos y conectados. Seguramente traerán a otros compañeritos y compañeritas de su barrio en el próximo encuentro. Es una gran alegría que en toda la provincia esté iniciando esta experiencia, ya tenemos 100 localidades en marcha y lo más importante es que las actividades recreativas, formativas, culturales y lúdicas volverán a conectar a los chicos y chicas con los saberes escolarizados».

Respecto de los objetivos de **Verano Activo**, el secretario de Educación explicó: «la idea es que en el mes de febrero se dé el vínculo educativo más preciso, que estará cimentado en el esfuerzo de estas primeras convocatorias del mes de enero».

En este sentido, Cristiani agregó: «hoy vinimos a disfrutar de estas actividades lúdicas y recreativas que convocan a los chicos y chicas  permitiéndoles el reencuentro entre pares,  y  a nosotros nos posibilita trabajar en el vínculo pedagógico futuro. Estos chicos y chicas saldrán fortalecidos y fortalecidas y nosotros integraremos muy pronto otras actividades culturales y científico-tecnológicas en toda la provincia».

Por su parte, Mariana Escobar indicó que «los espacios culturales van a ser parte de este **Verano Activo** y hoy tenemos esta primera experiencia hermosa donde el ministerio de Cultura estará trabajando para compartir eventos culturales y espectáculos para niños, niñas y adolescentes, brindando encuentros que acompañen la vinculación desde el afecto, en este proyecto tan esperanzador y comprometido».

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.15em; font-weight: 700">Acompañar: puentes de igualdad</span>**

La línea de trabajo de **Verano Activo** está integrada en el **Programa Nacional Acompañar: puentes de igualdad** que brinda acciones de acompañamiento socio-comunitario con un equipo de referentes territoriales abocados a **acompañar a cada estudiante para revincularlo con la escuela** y ofrecerle «puentes» durante el verano, durante el tiempo extraescolar, y a distancia en los territorios en los que aún no pueda retomarse la presencia física.

El **Programa Acompañar** consta de diversas acciones, dentro de las cuales se destacan:

\- El co-diseño de las políticas con cada jurisdicción educativa. Está dirigido a complementar iniciativas existentes y generar diferentes solidaridades territoriales concretas atendiendo a las necesidades y realidades de cada territorio para concretar el derecho a la educación de todos y todas

\- Identificación y geolocalización de las y los estudiantes que han interrumpido el contacto con la escuela. Fortalecer o crear redes existentes de acompañamiento de niñas, niños y adolescentes que han interrumpido su escolaridad.

\- Constitución de Mesas de articulación local: impulsar la co-participación activa de todos los representantes locales, distritales y provinciales con el objetivo de aunar esfuerzos e ir a buscar a las y los estudiantes que las escuelas hayan identificado como aquellos que han visto interrumpida su escolaridad por motivos multicausales.

\- Acompañar, a través de distintos recursos, el diseño y puesta en marcha en la escuela de proyectos institucionales y diversificación de propuestas pedagógicas para la revinculación de los y las estudiantes.

\- Programa de acompañamiento Socio-comunitario: a cargo de un equipo de referentes territoriales abocado a acompañar a cada estudiante.