---
category: La Ciudad
date: 2021-01-06T11:11:16Z
thumbnail: https://assets.3dnoticias.com.ar/060121-procrear-parque-federal.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: El Litoral'
resumen: 'Procrear: este miércoles se sortean 22 viviendas del Parque Federal'
title: 'Procrear: este miércoles se sortean 22 viviendas del Parque Federal'
entradilla: Es para quienes se anotaron en la última inscripción de diciembre pasado,
  y completaron satisfactoriamente la documentación requerida. El sorteo será televisado
  a las 18 horas por la TV pública.

---
Este miércoles a las 18 horas y por la TV pública, se sortearán 1.200 viviendas Procrear en seis provincias de país, entre las que se incluye Santa Fe. **Y en la ciudad serán 22 los departamentos a sortearse y adjudicarse, los cuales pertenecen al desarrollo urbanístico Procrear del Parque Federal**. 

No habrá sorteos para otros puntos de la bota santafesina en esta ocasión: solo en la capital. **Deberán estar atentos quienes se anotaron en la última inscripción del programa, el 7 de diciembre pasado.**

En esta oportunidad, el sorteo será solo para adquisición de viviendas en desarrollos urbanísticos mediante créditos hipotecarios, con plazos que van de entre 10 hasta los 30 años, siempre de acuerdo a los ingresos de cada familia aspirante.

Esto se aclara porque el nuevo Procrear incluye, además de las líneas hipotecarias (compras de viviendas, de lotes con servicios y construcción de viviendas), líneas crediticias personales para adquisición de materiales, reparaciones, refacciones y ampliaciones de viviendas, etcétera. Son nueve líneas en total.

Respecto de este último punto, «hace 15 días se sortearon lotes con servicios, pero también microcréditos para refacciones y ampliación en distintos puntos del país», le dijeron a El Litoral fuentes de Ministerio de Desarrollo Territorial y Hábitat nacional.

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.1em; font-weight: 700"> Cambios en el Programa</span>**

Cabe recordar que el Plan Procrear se reestructuró luego del cambio de gestión nacional en diciembre pasado, lo cual llevó a algunas modificaciones en el diseño, las definiciones de tipologías, los segmentos sociales de acceso a los créditos y la fórmula de coeficiente de acceso.

Por ejemplo, ahora se aplicará el coeficiente HogAr -que reemplazó al del sistema UVA, el cual evolucionaba según el índice inflacionario-. El coeficiente HogAr «se calcula de acuerdo con la evolución del Coeficiente de Variación Salarial (CVS) entre el último día del quinto mes anterior y el último día del cuarto mes anterior al cual corresponda el ajuste», explicaron las mismas fuentes.

«Este ajuste no podrá exceder en más del 2% al ajuste de capital que hubiese resultado de aplicar el Coeficiente de Estabilización de Referencia (CER) desde el último día del mes que corresponde al pago de la primera cuota siguiendo el mismo procedimiento aplicado para el ajuste por CVS», agregaron.

Además, ese tope del 2% del CER «significa que si en algún mes, por paritarias o decisiones de los empleadores o si el salario se recupera y le puede “ganar” a la inflación, en esos meses solo ajustará hasta ese porcentaje». También, el formato HogAr se divide en cuatro segmentos de acuerdo con el nivel de ingreso, «por lo cual, a menor ingreso, menor es la tasa de interés que los beneficiarios pagan».

Por último, hay segmentos de líneas crediticias específicas para personas con discapacidad, mujeres jefas de hogar con hijos a cargo, excombatientes de Malvinas, personal de salud y personal docente.

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.1em; font-weight: 700"> Proyecciones</span>**

Según el sitio web oficial de Procrear, el Ministerio de Desarrollo Territorial y Hábitat se puso como meta otorgar 300 mil créditos -personales e hipotecarios-, que van desde el mejoramiento de la vivienda existente hasta viviendas nuevas en desarrollos urbanísticos, dentro de las nueve líneas del Programa.

Con relación a los créditos hipotecarios Procrear para acceder a la adquisición de vivienda en un desarrollo urbanístico, **se estima que hay 11 mil viviendas por adjudicar mediante sorteo en todo el país**. Y de acuerdo a lo que adelantaron medios porteños sobre el nuevo Procrear, el Gobierno nacional proyecta construir 20.000 viviendas y generar 24.000 lotes con servicios.

En el nuevo Procrear hay segmentos de líneas crediticias específicas para personas con discapacidad, mujeres jefas de hogar con hijos a cargo, excombatientes de Malvinas, personal de salud y personal docente.

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.1em; font-weight: 700"> 78 EN TOTAL</span>**

Serán 78 los sorteos para líneas de créditos hipotecarios en distintos desarrollos urbanísticos Procrear que están en marcha para las provincias de Buenos Aires, Córdoba, Santa Fe, Mendoza, San Luis y Misiones. 

Cada sorteo será mediante sistema electrónico y se transmitirán por la señal de la TV Pública a la hora 18: se realizarán de acuerdo a los segmentos y a las tipologías disponibles, y estarán supervisados y certificados por escribana pública. 

**Los resultados se publicarán en la página web del Ministerio de Hábitat.**