---
category: La Ciudad
date: 2021-08-26T06:00:02-03:00
thumbnail: https://assets.3dnoticias.com.ar/LUCESALEM.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Se repararon más de 270 luminarias desde Alem hasta el rulo de Cilsa
title: Se repararon más de 270 luminarias desde Alem hasta el rulo de Cilsa
entradilla: Los trabajos se enmarcan en el plan de iluminación de la Municipalidad.
  Las tareas incluyen la ex Mar Argentino, Cruz Roja, 27 de Febrero, Alem y colectoras.

---
La Municipalidad culminó las reparaciones de 271 luminarias en un sector estratégico de la ciudad comprendido por: avenida de la Constitución Nacional Argentina (ex Mar Argentino), Cruz Roja y colectora, 27 de Febrero y avenida Alem. Las obras comenzaron a fines de julio y finalizaron este martes.

Con respecto al alcance de los trabajos, Matías Pons Estel, secretario de Obras y Espacio Público del municipio, consignó que las tareas se enmarcan “en el Programa Santa Fe se Ilumina. En este caso, se trabaja en la recuperación íntegra de las luminarias emplazadas en Mar Argentino, 27 de Febrero y Alem”. Y amplió que, en un breve plazo, también arrancará el recambio de unas 80 luces led ubicadas por Alem desde calle Alberdi hasta el Puente Oroño.

En tal sentido, las tareas se realizan con personal de la Dirección de Alumbrado Público y Electromecánica, con materiales previstos por la Municipalidad y con los nuevos vehículos especiales que se incorporaron el mes pasado a la flota.

“La inversión se enmarca en los 330 millones de pesos que el municipio dispuso para el 2021 en materia de iluminación en distintos barrios de la ciudad. Y para este caso particular de Alem, 27 de Febrero y Mar Argentino, la inversión es de 600 mil pesos en materiales como lámparas, ignitores y balastros”, añadió el funcionario, quien recordó que la semana pasada el intendente Emilio Jatón firmó dos contratos de licitación para avanzar con la nueva iluminación en Guadalupe y Siete Jefes.

Cabe destacar que la Municipalidad viene llevando adelante el Plan Santa Fe se Ilumina, que prevé la colocación de alrededor de 3.500 luminarias led, que se suman a las existentes. El objetivo es que para fin de año se alcance al 40% de la iluminación de la ciudad compuesta por luces led. Las obras están llegando a distintos barrios y avenidas de la ciudad y contemplan cuatro tipos de intervenciones: colocación de columnas, reemplazo del sistema de cableado, nuevos artefactos led e instalación de tableros, además de avanzar la reparación del sistema de iluminación existente.

**Detalle**

En el sector de Mar Argentino se repararon 128 luminarias. Los trabajos incluyeron 103 lámparas de sodio de 400w, 34 balastos de sodio de 400w y 54 ignitores.

En tanto, en el sector de Cruz Roja y colectora se repararon 55 luminarias: 20 lámparas de sodio de 400w, 10 balastos de sodio de 400w, 24 ignitores. Además, 8 lámparas de mercurio halogenado de 400w, 14 de 250w y 8 balastos de sodio de 250w.

En tanto, en Alem y colectora, se repararon 88 luminarias: 12 lámparas de sodio de 400w, 21 balastos de sodio de 400w, 40 ignitores. Más 41 lámparas de mercurio halogenado de 400w, 19 de 250w y 12 balastos de sodio de 250w.