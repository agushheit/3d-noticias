---
category: Agenda Ciudadana
date: 2021-04-17T06:15:15-03:00
thumbnail: https://assets.3dnoticias.com.ar/GANANCIAS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Aporte solidario: más de 90 contribuyentes presentaron amparos'
title: 'Aporte solidario: más de 90 contribuyentes presentaron amparos'
entradilla: La nómina se desprende de la consulta pública disponible en el sistema
  web del Poder Judicial de la Nación bajo jurisdicción de la Cámara Nacional de Apelaciones
  en lo Contencioso Administrativo Federal.

---
Los empresarios Héctor Magnetto, José Aranda y Lucio Pagliaro, vinculados al Grupo Clarín, y más de 80 contribuyentes encuadrados en el pago del Aporte Solidario y Extraordinario interpusieron un recurso de amparo para evitar el pago de ese tributo.

Así se desprende de la consulta pública disponible en el sistema web del Poder Judicial de la Nación bajo juridisdicción de la Cámara Nacional de Apelaciones en lo Contencioso Administrativo Federal.

El listado incluye también el futbolista Carlos Tévez, las contribuyentes Matilde Noble Mitre y María Candelaria Caputo y los empresarios Alejandro Saguier, Constancio Vigil, Artín Kalpakian y Eduardo Kalpakian, entre otros.

Este viernes también realizaron presentaciones Critiano Rattazi, Pilar Pérez Companc, Luciana Benotti, Renatro Cirelli, Jorge Luis Oubiña, Alicia Humuzia, Enrique Peláez, Carlos Sánches Alzaga y Viviana Novelle.

**Aporte Solidario**

La Ley de Aporte Solidario y Extraordinario de las grandes fortunas se aprobó a principios de diciembre del año pasado con el objetivo de moderar los efectos de la pandemia de coronavirus y está destinado a los patrimonios superiores a los 200 millones de pesos.

El proyecto de ley fue impulsado por el presidente del bloque oficialista en la Cámara baja, Máximo Kirchner, bajo la premisa de recaudar alrededor de 300 mil millones de pesos para volcar a planes productivos y sanitarios

La ley contempla el cobro de 2% a los patrimonios de las personas físicas que hayan declarado hasta la fecha de la promulgación de la ley más de 200 millones de pesos.

Esa tasa se eleva al 2,25% en casos bienes por entre 300 y 400 millones de pesos, al 2,50% entre 400 y 600 millones y al 2,75% cuando sea de 600 a 800 millones.

La tasa sube al 3% cuando los patrimonios oscilen entre 800 millones y 1.500 millones; al 3,25% para el rango de 1.500 a 3.000 millones de pesos, y a partir de esa cifra es del 3,5%.

Se puede acceder a la lista completa en [https://www.telam.com.ar/notas/202104/551034-aporte-solidario-amparos.html](https://www.telam.com.ar/notas/202104/551034-aporte-solidario-amparos.html "https://www.telam.com.ar/notas/202104/551034-aporte-solidario-amparos.html")