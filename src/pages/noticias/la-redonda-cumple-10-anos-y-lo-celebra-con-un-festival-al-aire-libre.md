---
category: Estado Real
date: 2020-12-26T12:58:05Z
thumbnail: https://assets.3dnoticias.com.ar/laredondasf.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: La Redonda cumple 10 años y lo celebra con un festival al aire libre
title: La Redonda cumple 10 años y lo celebra con un festival al aire libre
entradilla: 'El domingo 27 de diciembre, el espacio cultural retoma las actividades
  presenciales para festejar su primera década. La celebración será con cupo limitado. '

---
En su décimo aniversario La Redonda, Arte y Vida Cotidiana, el espacio que depende del Ministerio de Cultura de la provincia, ubicado en Salvador María del Carril y Belgrano, de la capital santafesina, llevará adelante la celebración, entre las 19 y las 23 horas de este domingo.

El festejo se realizará en el hemiciclo exterior, de cara al Parque Federal, y será gratuito, aunque con cupo limitado, para respetar los protocolos sanitarios. Las entradas se pueden obtener de manera online ingresando a [www.eventbrite.com.ar](https://www.eventbrite.com.ar/e/la-redonda-10-anos-somos-parte-de-algo-grande-tickets-133479419537) o de manera presencial en La Redonda, de lunes a viernes de 9 a 13 horas.

![](https://assets.3dnoticias.com.ar/redonda.webp)

<br/>

<span style="font-family: helvetica; font-size: 1.2em; font-weight: 800;">**ACERCA DEL ESPECTÁCULO**</span>

Durante la jornada habrá espectáculos musicales de Amandicia (jazz, funk, soul); Candela Fernández (folclore latinoamericano intervenido con recursos electrónicos); Maloca Trío (sonidos desde Brasil); Groove Trío (jazz) y el cierre con The Golden Days (rockabilly). Entre cada presentación el DJ Nicolás Negrete se hará cargo de la musicalización y la conducción estará a cargo del actor y performer Naza Pig.

Además, habrá intervenciones en las ventanas de La Redonda con Julieta Taborda y Daniel Payero Zaragoza (danza), y Matías Domínguez y Julián Bruna (clown).

El patio gastronómico tendrá propuestas para todos los gustos (incluso comida vegetariana, vegana y apta para celíacos/as) y se invita a las y los concurrentes a asistir con sus sillones, reposeras y mantas, para aprovechar el espacio verde en donde tendrá lugar la actividad.

De acuerdo con los protocolos sanitarios, en el marco de la pandemia, **no se permitirá el ingreso sin tapabocas**, habrá puestos de expendio de alcohol en gel y se mantendrá la distancia social.

<br/>

<span style="font-family: helvetica; font-size: 1.2em; font-weight: 800;">**UNA DÉCADA REDONDA**</span>

El festejo por los 10 años será la vuelta a las actividades presenciales en La Redonda. «Arrancamos el 2020 con un colorido, ruidoso y masivo carnaval, que esperábamos sea el puntapié inicial de un año lleno de juegos, encuentros y nuevas propuestas; pero los planes cambiaron, para todos y todas», señalaron desde la coordinación del espacio.

Respecto del funcionamiento de La Redonda durante estos meses, remarcaron: «Cuidarnos y cuidar a quienes siempre transitan este espacio fue la prioridad, por eso pusimos el lugar a disposición del Ministerio de Salud y sus trabajadores, quienes a lo largo de todos estos meses se pusieron al frente de la batalla para proteger la vida de las y los santafesinos».

Asimismo, indicaron: «Hoy, manteniendo los cuidados, dejamos un rato de lado la virtualidad para volver a encontrarnos en este lugar donde todo empezó hace 10 años. La Redonda de la nueva década se abre a las expresiones urbanas que llenan las calles de la ciudad; a las diversas identidades que pintan color arcoíris las luchas y las conquistas; a los creadores y creadoras que necesitan espacios para crecer; a las infancias libres que forjan los mundos en los que queremos vivir. Los invitamos a seguir construyendo juntos y juntas esta historia, porque somos parte de algo grande».