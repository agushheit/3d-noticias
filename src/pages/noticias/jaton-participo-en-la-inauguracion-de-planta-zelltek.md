---
category: La Ciudad
date: 2021-10-22T06:00:21-03:00
thumbnail: https://assets.3dnoticias.com.ar/ZELLTEKJATON.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Jatón participó en la inauguración de planta Zelltek
title: Jatón participó en la inauguración de planta Zelltek
entradilla: 'El intendente estuvo en el acto realizado en el Parque Tecnológico Litoral
  Centro de la capital provincial. '

---
Este jueves, en el Parque Tecnológico Litoral Centro de la ciudad de Santa Fe, el intendente Emilio Jatón participó del acto en el que se inauguró la remodelada planta de Zelltek. Según informaron desde la empresa, la ampliación de las instalaciones implicó una inversión cercana a los 12 millones de dólares.

En la oportunidad, Jatón afirmó que “cuando uno está en lugares como este Parque Tecnológico siente el orgullo de vivir en esta ciudad. Aquí se trabaja en silencio y muchas veces, desde la sociedad, se desconoce qué pasa acá adentro”, aseguró. Según dijo, “el hecho de que estemos todos juntos acá significa que este es el camino: la única manera es que la articulación de las universidades, lo público y lo privado hagamos de este lugar, así como de la Argentina, una verdadera economía del conocimiento”.

En referencia a la capital de la provincia, el intendente aseveró que “Santa Fe es un lugar de contrastes: se construye un rancho por día pero es la ciudad que mayor cantidad de científicos tiene en relación a sus habitantes. Esos dos contrastes conviven y este es un claro ejemplo: a metros de donde se construyen los ranchos, se está haciendo economía del conocimiento, conocimiento que nace en esta misma ciudad”.

Por último, mencionó que “el mundo le está abriendo las puertas hoy a la Argentina y sabe del potencial que tenemos”. Por eso llamó a convertir a Santa Fe en “la ciudad de la economía del conocimiento”. Y concluyó en que “Zelltek nos está dando el ejemplo de que es posible. No perdamos este impulso, no perdamos este piso. El techo está muy alto y depende de nosotros”.

**Economía del conocimiento**

De la actividad también participaron el ministro de Desarrollo Productivo de la Nación, Matías Kulfas; el gobernador de la provincia, Omar Perotti; el embajador de Alemania en la Argentina, Ulrich Sante; el gerente general de Amega Biotech, Juan Ceriani; el presidente del directorio Parque Tecnológico Litoral Centro, Norberto Nigro; y el rector de la Universidad Nacional del Litoral (UNL), Enrique Mammarella, entre otras autoridades nacionales, provinciales y locales.

El ministro Kulfas se mostró “muy contento de estar en esta inauguración con una inversión tan importante para Santa Fe y para la Argentina porque la inversión en biotecnología, en ciencia y en conocimiento argentino, integrando la calidad de nuestros investigadores y nuestros inversores marca a las claras el camino a recorrer para que la Argentina se ponga de pie”.

En ese sentido, evaluó que en la nueva planta se puede ver “una maravillosa articulación: el desarrollo empieza en el territorio, en lo local. Se pueden tener muchos instrumentos pero se necesita de interlocutores adecuados. Y en Santa Fe tenemos una materia prima maravillosa para la innovación y el conocimiento. Esto es algo que se ha puesto en valor hace tiempo y verlo dar sus frutos es muy importante, con todo el trabajo que significa el acompañamiento del municipio y la provincia”, afirmó.

Por su parte, el gobernador Perotti señaló que “este es un momento muy importante para la vida de la empresa, pero también una realidad concreta para la vida de la ciudad y la provincia, y un símbolo de este momento de la Argentina”. Por ello agradeció “a todos los que han sido parte del proceso de esta empresa, como la UNL que marcó un camino y fue pionera en estos procesos”.

Posteriormente, el embajador Sante recordó que “Argentina y Alemania son socios confiables desde hace muchas décadas”. Al respecto, amplió que “entre las más de 300 empresas alemanas, muchas de ellas son fieles a la Argentina desde hace 100 años en las buenas y en las malas” y que “Alemania y los Estados miembros de la Unión Europea son los mayores inversores directos en Argentina y esta no es una cuestión obvia porque los cambios económicos globales exigen a nuestras economías modernizarse”.

En ese contexto, mencionó a Zelltek como un ejemplo de las “empresas abastecidas de capital financiero proveniente de Alemania que se desarrolló en forma continua y llega a esta inauguración demostrando el grado de éxito que pueden alcanzar estas asociaciones basadas en la investigación”.

Por último, el gerente general de Amega Biotech, Juan Ceriani, rememoró que en 2022, “Zelltek cumplirá 30 años desde que nació como un emprendimiento en la Universidad Nacional del Litoral”. Además, recordó que “fue el primer convenio que involucró la incubación de una empresa biotecnológica en una universidad argentina. Hoy está muy en boga hablar de ecosistemas biotecnológicos, pero Zelltek fue innovadora en esa primera experiencia”.

**Zelltek**

Es una firma surgida en 1992 que desarrolla, produce y comercializa biofarmacéuticos. La ampliación de la planta, en la que se invirtieron cerca de 12 millones de dólares, permitirá a la empresa de base tecnológica elaborar sus productos a partir de sus propios principios activos, sustituyendo la importación de medicamentos. De este modo, Zelltek podrá multiplicar su capacidad productiva, consolidando su posición a nivel internacional.