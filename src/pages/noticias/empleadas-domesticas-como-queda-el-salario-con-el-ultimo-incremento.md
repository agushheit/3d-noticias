---
category: Agenda Ciudadana
date: 2021-10-15T06:15:49-03:00
thumbnail: https://assets.3dnoticias.com.ar/EMPLEADAS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Empleadas domésticas: cómo queda el salario con el último incremento'
title: 'Empleadas domésticas: cómo queda el salario con el último incremento'
entradilla: Se acordó un incremento de 6% en noviembre, mientras se adiciona un 2%
  al 5% de diciembre para las empleadas domésticas. Cómo queda su salario

---
El Consejo del Salario para los Trabajadores de Casas Particulares pautó este miércoles un nuevo aumento para las empleadas domésticas, con lo que el acumulado anual llegará al 50%.

En el marco de una nueva reunión, se acordó un incremento de 6% en noviembre, mientras se adiciona un 2% al 5% de diciembre. De ese modo, el acumulado anual paritario, al tomar en cuenta junio de este año y mayo de 2022, llega al 50%. En el encuentro también se estipuló una nueva revisión para marzo del año próximo.

El incremento salarial impactará en 1,5 millones de trabajadoras de casas particulares. El acuerdo previo incluía que en junio se iban a otorgar incrementos no acumulativos del 13%; 12% a partir de septiembre 2021; 5% a partir de diciembre 2021, y 12% desde marzo 2022, con cláusula de revisión ese mes.

La Comisión es un ente tripartito que reúne a representantes del Estado, empleadores y los gremios del sector. En el país existen 1,5 millones de empleadas registradas del sector.

A mitad de año, la Comisión Nacional de Trabajo en Casas Particulares había acordado una suba salarial para el personal doméstico del 42% a abonar en cuatro tramos.

El acta firmada establecía una suba desdoblada en un 13% en junio, un 12% en septiembre y otro 5% en diciembre, con un 12% en marzo del año próximo. Los ajustes al alza también alcanzan a personal de cuidado y caseros. Además, por primera vez, desde septiembre se les reconoció a las trabajadoras del sector antigüedad por el 1% y el ítem por zona desfavorable sumará un 2%.

También se estableció –por primera vez– un adicional salarial por “Antigüedad”, equivalente a un 1% por cada año de antigüedad de la trabajadora en su relación laboral, sobre los salarios mensuales. Este adicional se abona mensualmente desde el 1 de septiembre de 2021. El tiempo de servicio a los fines de este adicional por antigüedad comenzó a computarse a partir del 1 de septiembre del año 2020, sin efecto retroactivo.

Anteriormente, había acordado una actualización que rigió entre diciembre y abril, del 28%, abonado en tres cuotas.

**Cómo queda el salario con el último incremento**

Categoría 1: Supervisor: quien realiza la coordinación y control de las tareas que realizan personas que están a su cargo.

\- Hora con retiro: $273,50.

\- Hora sin retiro: $306.

\- Mensual con retiro: $34.910.

\- Mensual sin retiro: $38.886.

Categoría 2: Empleada doméstica y personal para tareas específicas: aquellas personas que fueron contratadas para realizar tareas puntuales, por las cuales se debe tener el conocimiento adecuado para llevarlas a cabo.

\- Hora con retiro: $264,50.

\- Hora sin retiro: $290.

\- Mensual con retiro: $32.433,50.

\- Mensual sin retiro: $36.104

Categoría 3: Empleada doméstica y caseros: personal que presta tareas inherentes al cuidado general y preservación de una vivienda en donde habita con motivo del contrato de trabajo.

\- Hora: $249,5.

\- Mensual: $31.644.

Categoría 4: Asistencia y cuidado de personas. Comprende la asistencia y cuidado no terapéutico de personas, tales como: personas enfermas, con discapacidad, niños, adolescentes, adultos mayores.

\- Hora con retiro: $249,5.

\- Hora sin retiro: $279,50.

\- Mensual con retiro: $31.644

\- Mensual sin retiro: $35.264.

Categoría 5: Personal para tareas generales. Prestación de tareas de limpieza, lavado, planchado, mantenimiento, elaboración y cocción de comidas y, en general, toda otra tarea típica del hogar.

\- Hora con retiro: $231,50.

\- Hora sin retiro: $249,5.

\- Mensual con retiro: $28.457.

\- Mensual sin retiro: $31.644.

La revisión de paritarias de trabajadoras de casas particulares se dio en un contexto en el que otros gremios también lograron mejores acuerdos y, de hecho, en las últimas horas el sindicato metalúrgico de la UOM también alcanzó un entendimiento.

En ese caso, logró una actualización salarial del 50,2%, que incluye una cláusula de revisión para diciembre.