---
category: Agenda Ciudadana
date: 2020-12-21T13:41:16Z
thumbnail: https://assets.3dnoticias.com.ar/2112fotoenacom.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Infobae'
resumen: Nuevos planes de internet, telefonía y cable lanzados por el Gobierno
title: 'Nuevos planes de internet, telefonía y cable lanzados por el Gobierno: cómo
  será el mecanismo para poder acceder a ellos'
entradilla: El ENACOM publicó en el Boletín Oficial la resolución mediante la cual
  se aprobó la  «Prestación Básica Universal Obligatoria»

---
Con el objetivo de «favorecer la universalización del acceso por parte de los sectores con menores ingresos y la población vulnerable», el Gobierno anunció el viernes pasado el lanzamiento de un nuevo «Precios Cuidados» para los servicios de internet, telefonía y televisión por cable, con planes subsidiados para más de 10 millones de argentinos.

Si bien la medida está destinada a jubilados, desocupados, titulares de AUH entre otros planes, tras el anuncio oficial desde el sector alertaron sobre la inviabilidad para su implementación y plantearon que es casi imposible controlar quiénes podrían ser reales beneficiarios y quiénes no, por lo que un universo mucho más amplio de personas estaría en condiciones de solicitar los servicios subsidiados lo que afectaría a las empresas prestadoras.

Este lunes, a través de la Resolución 1467/2020 publicada en el Boletín Oficial, el Ente Nacional de Comunicaciones (ENACOM) aprobó la «Prestación Básica Universal Obligatoria», para el servicio básico telefónico, comunicaciones móviles, internet y televisión por cable, mediante la cual las compañías deberán brindar los planes subsidiados a partir del 1 de enero de 2021.

De esta forma la prestación básica para telefonía móvil tendrá un costo mensual de $150, mientras que existirán dos opciones para paquete de datos, una por $200 por mes y la otra por $18 por día. Para el caso de la telefonía fija, el precio final será de 380 pesos. Mientras que para la televisión paga, la prestación básica implica un descuento sobre el precio del plan de menor valor ofertado por el operador al 31 de julio pasado y/o aumentos posteriores que se autorizaren. Si el operador tiene más de 500.000 abonados, el descuento será del 30%; si tiene entre 100.000 y 500.000, del 25%; entre 30.000 y 100.000, del 20%.

El ENACOM detalló quiénes podrán adherirse como beneficiarios del paquete que anunció el Gobierno. En efecto la medida abarca a:

►Jubilados, Jubiladas, Pensionados y Pensionadas, con remuneración inferior o igual a dos salarios mínimos, así como sus hijos de entre 16 y 18 años.

►Trabajadores y trabajadoras en relación de dependencia con remuneración inferior o igual a dos salarios mínimos, así como sus hijos de entre 16 y 18 años.

►Beneficiarios de la Asignación Universal por Hijo, la Asignación Universal por Embarazo, seguro de desempleo y programas sociales, así como sus hijos de entre 16 y 18 años.

►Pensiones no contributivas con remuneración inferior o igual a dos salarios mínimos, así como sus hijos de entre 16 y 18 años.

►Monotributo Social así como sus hijos de entre 16 y 18 años.

►Trabajadores y trabajadoras monotributistas inscriptos e inscriptas en una categoría cuyo ingreso anual mensualizado no supere DOS (2) Salarios Mínimos Vitales y Móviles así como sus hijos-as/tenencia de entre 16 y 18 años.

►Beneficiarios del Régimen Especial de Seguridad Social para Empleados de Casas Particulares (Ley N° 26.844) así como sus hijos de entre 16 y 18 años.

►Usuarios y usuarias que perciban una beca del Programa Progresar.

►Personas que se encuentren desocupadas o se desempeñen en la economía informal, así como sus hijos de entre 16 y 18 años.

►Beneficiarias y beneficiarios de programas sociales, así como sus hijos de entre 16 y 18 años.

►Clubes de Barrio y de Pueblo que se encuentren registrados conforme lo dispuesto por la Ley 27.098.

►Asociaciones de Bomberos Voluntarios definidas por la Ley 25.054 como entes de primer grado y que se encuentren registradas en los términos de dicha ley.

►Entidades de Bien Público definidas por la Ley 27.218 como: asociaciones civiles, simples asociaciones y fundaciones que no persiguen fines de lucro en forma directa o indirecta y las organizaciones comunitarias sin fines de lucro con reconocimiento municipal que llevan adelante programas de promoción y protección de derechos o desarrollan actividades de ayuda social directa sin cobrar a los destinatarios por los servicios que prestan, debiendo estar inscriptas ante el Centro Nacional de Organizaciones de la Comunidad (CENOC).

***

![](https://assets.3dnoticias.com.ar/2112enacom1.webp)

***

En lo que respecta al mecanismo para poner en marcha esta medida, el ENACOM precisó que «los prestadores obligados» deberán implementar «un sistema ágil y sencillo a través de todos sus canales de atención para que los usuarios puedan optar por obtener las PBU». Las empresas deberán realizar la instalación de los servicios incluso si la misma entendiera que la persona no se encuentra alcanzada por la Prestación Básica Universal Obligatoria.

En efecto, si el prestador entendiera que el usuario no se encuentra dentro del universo de beneficiarios, podrá informar al ENACOM «expresando los motivos de la negativa», quien resolverá la cuestión en un plazo que no podrá exceder de 30 días corridos.

«Transcurrido ese plazo sin que este ENACOM emita una Resolución en tal sentido se considerará que el usuario o usuaria se encuentra alcanzado por la “Prestación Básica Universal Obligatoria” y el licenciatario o licenciataria deberá continuar con la prestación del servicio», se explicó en el artículo 15. Ahora bien, si el ente estatal hiciera lugar al reclamo, el licenciatario «podrá dar de baja inmediatamente al servicio y emitir el correspondiente estado de deuda a cargo del solicitante» que en efecto no se encontraba alcanzado por la PBU.

Por otra parte las empresas podrán efectuar descuentos, promociones y/o mejores condiciones por debajo de los precios estipulados en la presente resolución y «los usuarios no tendrán obligación de permanencia, de modo de poder acceder a lo que sea más beneficioso».

No obstante, los prestadores podrán solicitar la actualización de los precios de dichas prestaciones «de manera cuatrimestral, tomando como referencia las variables de ajuste aplicadas a AUH». La actualización solicitada será evaluada por el ENACOM.

Por otra parte, el ente dirigido por Claudio Ambrosini dispuso este lunes el aumento del 5% para los servicios de internet, telefonía y televisión por cable a partir de enero de 2021. Las empresas deberán tomar como precio de referencia los valores vigentes al 31 de julio de 2020. En el caso de los licenciatarios que posean menos de 100 mil accesos y que no hubieran aumentado sus precios a lo largo de este año, la suba permitida podrá ser de hasta un 8%.

***