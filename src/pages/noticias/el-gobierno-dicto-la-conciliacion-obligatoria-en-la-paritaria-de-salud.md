---
category: Agenda Ciudadana
date: 2021-03-21T08:15:49-03:00
thumbnail: https://assets.3dnoticias.com.ar/ciprus.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: El gobierno dictó la conciliación obligatoria en la paritaria de salud
title: El gobierno dictó la conciliación obligatoria en la paritaria de salud
entradilla: SIPRUS había anunciado un paro para lunes y jueves de la semana que viene.
  AMRA, quien también rechazó la oferta, exigió que en 48 horas el gobierno los "vuelva
  a convocar y formalice una oferta salarial superadora".

---
El Ministerio de Trabajo, Empleo y Seguridad Social de la provincia, a pedido de la cartera de Salud, dictó la conciliación obligatoria por el término de 15 días hábiles para los profesionales del sector representados por el Sindicato Único de Profesionales Universitarios de la Salud (SIPRUS)

El gremio, además de rechazar la propuesta paritaria había anunciado un paro, sin asistencia a los lugares de trabajo, para lunes y jueves de la semana que viene.

Además, plantearon la realización de una jornada provincial de provincial de protesta con retención de tareas y movilización para los días 22 y 25 de marzo del presente, con mantenimiento de guardias mínimas y atención de pacientes covid.

AMRA, quien también rechazó la propuesta, les pidió a las autoridades del Gobierno Provincial "que en las próximas 48 horas nos vuelva a convocar y formalice una oferta salarial superadora a los fines que se pueda volver a ser analizada y sometida a votación. En caso de que no haya en los próximos días una nueva reunión paritaria se llevarán adelante las medidas gremiales necesarias para defender el poder adquisitivo de nuestros compañeros".

Mediante la Resolución 0002/2021, el gobierno pretende que el sindicato deje sin efecto las medidas anunciadas, debiendo el personal representado por esta entidad gremial prestar servicios normalmente y retomar la mesa de diálogo paritario.

Por otro lado, se dictamina que el Ministerio de Salud debe formular propuestas que permitan la resolución pacífica de esta situación.

El Ministerio de Trabajo fundamenta la decisión tomada en el carácter de servicio esencial que revisten las actividades sanitarias y hospitalarias (art. 24, Ley 25.877), especialmente en este momento en que se prevé un recrudecimiento o “segunda ola” del fenómeno epidemiológico de Covid19 a corto plazo.

La audiencia para el encuentro de las partes involucradas se fijó para el martes 23 de marzo y se realizará bajo modalidad virtual.