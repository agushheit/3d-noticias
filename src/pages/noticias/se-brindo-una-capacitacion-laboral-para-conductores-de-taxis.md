---
category: La Ciudad
date: 2021-07-23T06:00:53-03:00
thumbnail: https://assets.3dnoticias.com.ar/TAXISTAS.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Se brindó una capacitación laboral para conductores de taxis
title: Se brindó una capacitación laboral para conductores de taxis
entradilla: 'Estuvo destinada a santafesinos que buscan insertarse en el mundo laboral
  o actualizarse en la práctica profesional de conducción de taxis. '

---
La Municipalidad ofreció dos jornadas de capacitación en iniciación a la actividad de chofer de taxi, conducción preventiva y responsabilidad vial que tuvieron lugar en la sede del Liceo Municipal (ex Molino Marconetti). La mismas, fueron totalmente gratuitas y estuvieron destinadas a quienes buscan tramitar y obtener la licencia de conducir profesional, que habilita a trabajar como taxista en la ciudad de Santa Fe. La formación se organizó en acuerdo con la Agencia Nacional de Seguridad Vial, el Sindicato de Peones de Taxis de Santa Fe y la Sociedad Taximetrista Unidos de Santa Fe.

El secretario de Producción y Desarrollo Económico municipal, Matías Schmüth, sostuvo que “fue una propuesta que nos acercó la Sociedad Taximetrista Unidos de Santa Fe mostrándonos la necesidad que tienen de mano de obra para peones de taxis. A raíz de esto, nos pareció importante generar una propuesta de capacitación junto a la Agencia Nacional de Seguridad Vial, en la que ofrecemos a quienes hoy están haciendo el curso la posibilidad de contar con una herramienta que es el carnet profesional”.

Además, continuó el funcionario, “teniendo en cuenta que quienes realizan el curso son personas que se encuentran en la búsqueda de trabajo, hemos logrado, a través de un acuerdo con la Agencia para el Desarrollo (ADER), que se financie con un crédito a tasa 0 y con un plazo de gracia, el costo que tiene tramitar la matrícula. De esta manera, generamos un vínculo entre la oferta y la demanda de empleo, y construimos junto al sector privado, oportunidades de trabajo para los santafesinos que se encuentran atravesando un mal momento”.

El curso tiene como objetivo reconocer las situaciones de conflicto que se pueden presentar al transitar y analizar el riesgo vial existente involucrando al trabajador como agente activo y responsable de dicha situación. Y, por otra parte, plantear la posibilidad de cambio desde el lugar que ocupan los taxistas como partícipes de una misma sociedad imponiendo como premisa el respeto por el prójimo y la sana convivencia.

**En primera persona**

Natalia es una de las alumnas que se sumaron a esta iniciativa en busca de un trabajo que le permita tener su propio ingreso económico y, a su vez, conducir un vehículo porque es algo que le gusta hacer. “Me inscribí al curso porque no tengo trabajo y esto me permite obtener el carnet. Vivo con mis padres que son grandes y están jubilados y necesito tener un aporte para mí y me gusta manejar”. En cuanto a lo aprendido en el curso sostuvo que “es muy gratificante, porque te responden las dudas que tenés y te brindan tips para el manejo. Lo recomendaría sin dudarlo, tener la posibilidad de hacer un curso gratis y contar con este material es muy bueno para nosotros”.

**Temas abordados**

Al respecto, José Torres, capacitador de la Agencia Nacional de Seguridad Vial expresó: “En este curso de conducción segura tratamos temas como el espacio público, la cultura vial, la seguridad automotriz y las condiciones psicofísicas. Buscamos darles herramientas a los conductores para que ellos puedan tomar las mejores decisiones en cada acción que se puedan encontrar en el tránsito”.

En esa misma línea sostuvo que “entendemos que ellos como conductores de taxis tienen que ser un ejemplo para la sociedad, por lo que es importante que transmitan estos conceptos y tengan la seguridad incorporada a todas las acciones diarias, por ejemplo, insistiendo en el uso del cinturón de seguridad a los pasajeros y buscando lugares seguros para el ascenso y descenso de los mismos.”

La capacitación se dictó de manera presencial durante dos jornadas consecutivas de cuatro horas y la aprobación fue de carácter participativo, es decir, se valoró la actitud activa del alumno, que obtuvo una certificación avalada por la ANSV, la Municipalidad de Santa Fe, el Sindicato de Taxis y Cooperativa de Radiotaxis. Asimismo, su aprobación posibilita también la adquisición de un crédito para poder abonar la tasa de obtención de la Licencia Nacional de Conductor Profesional, con dos meses de gracia para ser devuelto una vez que ya estén realizando la actividad para la cual se capacitaron. Sumado a esto, la Sociedad Taximetrista Unidos de Santa Fe promoverá la inserción laboral de aquellos participantes que obtengan la licencia y el sindicato los acompañará en el proceso de inicio de la actividad.

Damián Cóceres, presidente de la Sociedad Taximetrista Unidos de Santa Fe sostuvo que este curso “es algo que venimos gestionando hace rato y que podemos llevar a cabo gracias a una reunión que mantuvimos con funcionarios municipales. Hoy estamos concretando el primer curso para habilitación a nuevos postulantes a choferes de taxi, a través del cual buscamos profesionalizar el trabajo del taxista, y seguir trabajando para los santafesinos”.