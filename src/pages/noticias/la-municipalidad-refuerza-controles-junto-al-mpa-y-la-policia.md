---
category: La Ciudad
date: 2021-09-01T06:00:19-03:00
thumbnail: https://assets.3dnoticias.com.ar/CONTROLES.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Municipalidad refuerza controles junto al MPA y la Policía
title: La Municipalidad refuerza controles junto al MPA y la Policía
entradilla: 'El 60% de las motos que se retuvieron no tiene chapa patente o está adulterada,
  según se desprende de los resultados de los operativos de control vehicular. '

---
En los comercios de venta de motovehículos también se incrementarán los controles. A su vez, se verifican entre 5 y 7 motos mensuales retenidas que fueron reportadas como robadas o con pedidos de captura.

La Municipalidad de Santa Fe viene reforzando los controles en la vía pública sobre los motovehículos tanto en los puestos fijos como móviles, y detectó que del total de motos retenidas, el 60% carece de chapas patente obligatorias o las mismas están adulteradas. Asimismo, mensualmente se verifica que entre 5 y 7 motos retenidas fueron reportadas como robadas o tienen pedido de captura por parte de la policía provincial, por haber estado involucrada en algún ilícito.

El secretario de Control y Convivencia Ciudadana de la Municipalidad, Fernando Peverengo, indicó que “en cuanto a los controles de tránsito fuimos optimizando resultados, con el objetivo de focalizarnos para que en 2023 Santa Fe sea una ciudad donde no circulen motos sin chapas patente o con chapas patente adulteradas, que hoy es una constante. Esta es una de las grandes problemáticas que no sólo la tiene la ciudad de Santa Fe sino también la región y la provincia”.

Además del ambicioso plan de iluminación para renovar y mejorar el alumbrado público y las tareas de desmalezado -que son servicios municipales que impactan también en la seguridad-, Peverengo indicó que, en lo correspondiente a seguridad pública urbana, la Municipalidad aborda tanto el control de tránsito como los operativos especiales en comercios vinculados como armerías, desarmaderos, etc., lo que se hace en conjunto con el Ministerio Público de la Acusación (MAP).

Sobre las inspecciones a comercios de venta de motos, Peverengo dijo: “Vamos a poner el control ahí porque hoy una persona va al local con un documento de identidad, le dan créditos accesibles y saca su moto sin estar patentada, con la promesa de hacerlo. Ese ciclomotor sale a la calle sin cumplir las reglas y esa irregularidad sale del propio comercio, por lo que tenemos previsto empezar a trabajar para controlar una situación que es preocupante en Santa Fe”, explicó.

En cuanto a cómo se realizarán las actuaciones, el secretario de Control y Convivencia Ciudadana de la Municipalidad indicó que primero se va a notificar a esos comercios sobre la legislación vigente y sobre lo que deben cumplir como vendedores, para que los motovehículos salgan patentados. “Si no cumplen pueden ser pasibles de sanción y hasta de una clausura temporal del local”, advirtió.

**Motovehículos**

Peverengo remarcó que “el operativo de motovehículos es la mejor colaboración que podemos hacer en materia de Control municipal contra la inseguridad” y fundamentó: “Esto no quiere decir que toda persona que circule en ciclomotor sin chapa patente sea un delincuente, pero la estadística dice que en la mayoría de los hechos delictivos que se dan en la ciudad de Santa Fe participan motohevículos y el 100% de ellos es una moto sin chapa patente o con la chapa patente adulterada. Entonces es un gran aporte que estamos haciendo desde el municipio para combatir la inseguridad dentro de las facultades que tenemos como gobierno local”.

El secretario reforzó los números surgidos de los operativos de calle que se realizan en las avenidas como Blas Parera, Aristóbulo del Valle, Gorriti, Peñaloza, avenida Costanera y en distintos puntos del microcentro. “Desde el jueves pasado, que pusimos en funcionamiento un operativo móvil, hemos aumentado el control en calle”, indicó. Sólo el último fin de semana, hubo 116 motos retenidas y 18 autos.

En ese sentido, el funcionario agregó: “El 60% de los motovehículos no tienen chapas patentes o bien circulan con patentes adulteradas, que es un porcentaje que preocupa. Esa es la mayor infracción que encontramos”.

Con respecto a las motos que son retenidas, el funcionario explicó que son trasladadas al Depósito de Vehículos Retenidos (DVR) y luego son verificadas por personal policial, “porque también tenemos que ver si tienen pedidos de capturas”. De ser así, se ponen a disposición de la Justicia. “La información que tenemos es que de 5 a 7 motos por mes de las que retenemos, tienen pedido de secuestro. Muchas veces son motos robadas o bien que han tenido algún pedido de captura policial y que están a disposición de la Justicia”, consideró Peverengo.

En un caso común, el motovehículo queda en la DVR hasta que aparezca su dueño a retirarla con la documentación vigente en mano. “Si falta la patente, esa moto quedará retenida hasta que la persona pueda patentarla y presentarse con la chapa a la DVR, se la colocan y se va. La otra opción es contra presentación del documento de inicio del trámite, en cuyo caso puede retirar la moto con una unidad de traslado”, señaló Peverengo.

**Otros controles**

Por otro lado, el municipio continúa con controles, en conjunto con el Ministerio Público de la Acusación (MPA), sobre otros comercios vinculados. “En cuanto a las armerías, inspeccionamos 10 de las 11 que hay, y la que no pudimos ver es porque tiene un régimen especial de inscripción. En las otras se labraron actas y encontramos un arma que no se condecía entre la vendida y la facturada; no eran los mismos datos. En ese caso interviene el MPA”, destacó el funcionario.

También la Municipalidad realizó operativos en desarmaderos en conjunto con el MPA, que en su momento fue noticia porque se encontró una gran cantidad de cables del tendido eléctrico. “Asimismo, hacemos reuniones periódicas con el MPA; en su momento con los fiscales Martí y Giavedoni, en encuentros más focalizados en las picadas y ruidos molestos. Pero también se habló sobre volver a hacer controles en desarmaderos o chacharitas, que es algo que debe hacer la Policía, porque como municipio, la única injerencia que tenemos es corroborar si el local está habilitado o no”.