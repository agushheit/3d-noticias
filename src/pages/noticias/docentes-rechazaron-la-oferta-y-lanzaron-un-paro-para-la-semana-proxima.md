---
category: Agenda Ciudadana
date: 2021-03-11T07:35:38-03:00
thumbnail: https://assets.3dnoticias.com.ar/escuela1.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Rosario3
resumen: Docentes rechazaron la oferta y lanzaron un paro para la semana próxima
title: Docentes rechazaron la oferta y lanzaron un paro para la semana próxima
entradilla: Los maestros de escuelas públicas votaron en contra de la propuesta de
  la provincia. Piden que el aumento se adelante y se otorgue en menos tramos. Se
  viene la primera medida de fuerza del año

---
Habrá paro docente la semana próxima. Los maestros de escuelas públicas nucleados en el gremio Amsafé votaron este miércoles en contra de la propuesta salarial de la provincia y ya anunciaron la primera medida de fuerza del año, para lunes y martes.

Los trabajadores escolares analizaron la oferta presentada en la reunión paritaria del 8 de marzo y en el plenario provincial la consideraron insuficiente, con 12.657 votos por el rechazo y 11.576 por la aceptación, detalló Amsafé.

El gremio señaló que la propuesta salarial “contenga menos tramos y se adelante el pago” del aumento ofrecido por el Ejecutivo, que había sido de un 35 por ciento de incremento a completarse en tres etapas.

“El plenario provincial, realizado en el día de la fecha, con la participación de las y los delegados seccionales de los 19 departamentos, y la votación de más de 24 mil trabajadoras y trabajadores de la educación, resuelve paro de 48 horas los días 15 y el 16 de marzo”, detalla la resolución de Amsafé.

Y agrega que “en caso de no haber una nueva propuesta”, se lanzará otra medida de fuerza de 48 para los días 23 y 25 de marzo.

El lunes, la secretaría General de Amsafe y Ctera, Sonia Alesso, había calificado como "buena" la oferta y valoró el hecho de que en octubre se pueda revisar el acuerdo según la inflación, pero las bases en la votación, por escaso margen, decidieron el rechazo.