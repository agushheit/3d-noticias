---
category: La Ciudad
date: 2021-11-14T06:00:07-03:00
thumbnail: https://assets.3dnoticias.com.ar/VOLUNTARIOS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Municipalidad reconoció a voluntarios del programa Santa Fe Capital Unida
title: La Municipalidad reconoció a voluntarios del programa Santa Fe Capital Unida
entradilla: "Se trata de santafesinos y santafesinas que, desde el inicio de la pandemia,
  asisten a vecinos y vecinas que lo requieren. Se les brindó un reconocimiento por
  su compromiso ciudadano.\n\n"

---
Este viernes, en la sede del Liceo Municipal “Antonio Fuentes del Arco”, la Municipalidad reconoció a quienes participan del programa de voluntariado “Santa Fe Capital Unida”. Se trata de aquellos santafesinos y santafesinas que desde el inicio de la cuarentena, en marzo de 2020, brindan acompañamiento a vecinos y vecinas de la ciudad que lo requieren.

Desde que comenzó, el programa busca constituir un espacio colaborativo donde se potencien los esfuerzos y se promueva la participación, la cercanía y la inclusión de actores genuinos que generen acciones para con la sociedad. Cabe destacar que en el inicio de la cuarentena (marzo 2020), el voluntariado puso el foco en la población más vulnerable ante el coronavirus: adultos mayores y grupos de riesgo.

Actualmente, el plan ayuda a más de 190 personas y registra más de 280 voluntarios y voluntarias de distintas instituciones. El director de Derechos y Vinculación Ciudadana del municipio, Franco Ponce de León, ranco Ponce de León, aseguró que la iniciativa pretende “generar un reconocimiento a cada uno de los voluntarios y darles las gracias, pero también decirles que este programa continúa, está vigente, y se irá adaptando a las distintas necesidades que hay en la ciudad”.

“Lo interesante de todo esto, como dice el intendente Emilio Jatón, es que los santafesinos siempre aseguramos que somos solidarios. En estos casos puntuales se ve la solidaridad de los santafesinos y las santafesinas”, afirmó.

A modo de ejemplo, Ponce de León mencionó que algunas instituciones, “como la Cruz Roja Argentina, fueron una parte muy importante al principio del voluntariado, brindado las capacitaciones”. Luego, mencionó otras acciones puntuales realizadas por las diferentes entidades que aún forman parte del programa. Y recordó que aún pueden sumarse, tanto instituciones como personas que necesitan asistencia.

**Necesidades y ganas de ayudar**

El programa se propone articular las necesidades con las ganas de ayudar, en pos de construir una Santa Fe más unida. Con ese fin, y a partir de las demandas propias del COVID-19, pero también de situaciones puntuales como la vulnerabilidad de las personas en situación de calle en pleno invierno, la Municipalidad impulsó este iniciativa que contó con el acompañamiento de organizaciones como la Parroquia San Agustín, el Honorable Concejo Municipal, la Universidad Católica de Santa Fe, el Centro de Ex Combatientes de Malvinas, la Mesa de Diálogo Interreligioso y Cultura de Paz Santa Fe, el Centro Cristiano Koinonya, la parroquia Don Bosco, El Arca del Alba, la Asociación Vecinal Barrio Dr. René Favaloro, la Asociación 2 de Abril, la Iglesia de Jesucristo de los Santos de los Últimos Días, y la Cruz Roja Argentina.

La ayuda comunitaria contiene líneas de acción tales como: escucha telefónica (contención, escucha activa) y virtual (compartir información precisa sobre la pandemia, prevención, ejercicios físicos y recreativos) y asistencia presencial (provisión de insumos, paseo de mascotas). Durante el año, se implementaron modalidades como ayuda para inscribirse en el registro de vacunación y acompañamiento telefónico para quienes fueron vacunados.

Además de las acciones nombradas anteriormente, durante el invierno se trabajó articuladamente desde la dirección de Derechos y Vinculación Ciudadana, junto a la secretaría de Políticas de Cuidado y Acción Social, en el acompañamiento y la protección de personas en situación de calle brindando alimentos, infusiones calientes, ropa de abrigo, calzado y frazadas. Para esta acción, los voluntarios fueron capacitados por la dirección de Acción Social de la Municipalidad, cuyos integrantes brindaron las herramientas y la información para realizar los recorridos.

Aquellos santafesinos que quieran solicitar algún tipo de las asistencias mencionadas, como así también, las instituciones que quieran sumarse al programa, pueden realizar la solicitud a través de los canales del Santa Fe Te Escucha, al teléfono 0800 7775000 o al correo electrónico [informes@santafeciudad.gov.ar](mailto:informes@santafeciudad.gov.ar).

**Una amistad que perdura**

Nilda Perotti, más conocida como Bety, fue una de las personas asistidas en el programa de Voluntariado Santa Fe Unida. “Me enteré del voluntariado por una nota del diario. Yo, a pesar de los años que tengo, que son unos cuántos, siempre he tenido distintas actividades”, contó la mujer, que es docente jubilada.

Así fue como comenzó su vínculo con esta iniciativa municipal: en plena pandemia, ella necesitaba unos cartuchos para su impresora pero no podía salir. Entonces, llamó a la línea gratuita de la ciudad para solicitar ayuda y la contactaron con Facundo Garrido, voluntario que se sumó desde la Universidad Católica de Santa Fe.

“Le pedí que si venía desde el centro me comprara los cartuchos y que cuando llegara a casa, se los iba a pagar”. Y así sucedió. “¿Cómo querés que no lo quiera tanto, si sin conocerme, desembolsó dinero para que yo tuviera la impresora en condiciones?”, contó agradecida Bety.

Facundo suma su testimonio a esta historia que generó un nuevo vínculo en la pandemia. “Yo me enteré de un voluntariado por un e-mail que me mandó la facultad y me pareció una buena idea para aportar algo a la sociedad”. Se anotó y siempre estuvo atento al llamado del municipio para pasar a la acción y poder ayudar a quienes lo necesitaban.

“Me llamaron desde el municipio avisándome que Bety se iba a contactar conmigo y después me llegó el mensaje de WhatsApp pidiéndome los cartuchos”. Los retiró, se los llevó a la casa y fue el comienzo de una amistad que perdura hasta hoy.

Con las nuevas habilitaciones, Bety ya puede salir un poco más y no lo llama para pedirle ayuda pero siguen en contacto. “En una oportunidad, nos prometimos que nos íbamos a encontrar a tomar un café, aunque yo soy bastante matera y no se puede, pero le dije que lo podíamos cambiar por un liso”, cuenta Bety entre risas. Todavía esa cita no se concretó, pero al reencontrarse en este acto del municipio, en el que la mujer pudo conocer también a la familia de Facundo, la promesa sigue intacta.

**Presentes**

También participaron del encuentro, la secretaria de Políticas de Cuidado y Acción Social, Victoria Rey; la directora de Vinculación Ciudadana, Ileana Robles; el director de Acción Social, Gabriel Maurer; la directora de Gestión de Riesgo, Cintia Gauna; y la coordinadora del programa de voluntariado, Florencia Costantini.