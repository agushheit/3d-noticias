---
category: La Ciudad
date: 2022-01-03T06:15:07-03:00
thumbnail: https://assets.3dnoticias.com.ar/tunelamuneto.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Túnel Subfluvial: aumenta otro 30% el costo del peaje'
title: 'Túnel Subfluvial: aumenta otro 30% el costo del peaje'
entradilla: 'La medida rige desde el sábado. El costo para pasar de Santa Fe a Paraná
  en auto por el Túnel Subfluvial pasa de 100 a 130 pesos. En 11 meses la tarifa subió
  100%.

'

---
El viernes 31 a la noche, desde el ente biprovincial que administra Túnel Subfluvial que une Santa Fe con Paraná se informó que, a partir de las 00 de este sábado 1 de enero del 2022, se actualizaba el cuadro tarifario, con aumentos.

**El aumento es de un 30% para autos y motos, ya que pasó de 100 a 130 pesos el costo del peaje.**

"El Ente Interprovincial no recibe subsidios ni subvenciones – como sí ocurre con otros corredores viales- por lo que, la adecuación tarifaria, es imprescindible para la continuidad de las obras y la ejecución de proyectos necesarios para el siguiente ejercicio. Esta medida resulta indispensable para garantizar el debido mantenimiento y correcto funcionamiento del enlace vial", explicaron desde el viaducto.

![](https://media.unosantafe.com.ar/p/6de9a4b839a064a27709260266bf36ae/adjuntos/204/imagenes/030/829/0030829739/tunel_precios-enero2022jpg.jpg?2022-01-01-11-21-23 =510x499)

"Asimismo, se informa que se mantienen vigentes la Resoluciones que otorgan el 50% de descuento para el uso frecuente de automóvil y el 20% al transporte público de pasajeros y de carga, de empresas con domicilio y radicadas en las provincias de Santa Fe y Entre Ríos", remarcaron.

Vale recordar que el último aumento había sido en julio pasado, cuando se llevó la tarifa de 90 a 100 pesos. Y el anterior había sido en febrero de 2021. Por lo tanto, en 11 meses, la tarifa para autos y motos tuvo un incremento de 100%.