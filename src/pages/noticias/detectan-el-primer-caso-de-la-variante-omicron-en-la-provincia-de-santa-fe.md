---
category: Agenda Ciudadana
date: 2021-12-16T06:15:39-03:00
thumbnail: https://assets.3dnoticias.com.ar/OMICRON.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: Detectan el primer caso de la variante Ómicron en la provincia de Santa Fe
title: Detectan el primer caso de la variante Ómicron en la provincia de Santa Fe
entradilla: 'El caso fue notificado por el Ministerio de Salud de la provincia de
  Santa Fe. La persona afectada es una mujer de la ciudad de Rosario con antecedente
  de viaje a Estados Unidos.

'

---
El Ministerio de Salud de Santa Fe informó que se detectó el primer caso de la variante Ómicron de Covid 19 en esa provincia.

Según el parte oficial, se trata de una mujer de la ciudad de Rosario con antecedente de viaje a Estados Unidos. A su vez, por ese mismo caso hay 22 personas que se encuentran aisladas, siendo monitoreadas por los equipos de salud.

La paciente, de acuerdo al mismo documento, es una mujer de 40 años que cuenta con tres dosis de la vacuna para el Coronavirus (la tercera con menos de 14 días desde su aplicación) y se encuentra cursando la enfermedad en su domicilio.

“Comenzó con síntomas (tos y fiebre) el 8 de diciembre pasado, dos días después de haber ingresado al país. Tras el resultado positivo de la PCR se realizó la secuenciación genómica y se confirmó la variante Ómicron”, según el reporte del gobierno de Santa Fe.

La cartera de Salud santafesino aprovechó para recordar que, ante cualquier síntoma compatible con Coronavirus – fiebre de 37,5°C, tos, dolor de garganta dificultad respiratoria, rinitis/congestión nasal, dolor muscular, cefalea, diarrea y/o vómitos-, los afectados “deben acercarse al puesto de testeo más próximo”.