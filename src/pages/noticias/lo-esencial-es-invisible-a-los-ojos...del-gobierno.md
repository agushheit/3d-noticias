---
category: Agenda Ciudadana
date: 2021-02-15T03:46:11-03:00
thumbnail: https://assets.3dnoticias.com.ar/barletta.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Mario Barletta
resumen: Lo esencial es invisible a los ojos...del gobierno
title: Lo esencial es invisible a los ojos...del gobierno
entradilla: 'Abrir o no abrir las escuelas no debería ser la cuestión, sino asegurar
  las condiciones necesarias tanto para volver a la presencialidad como para que algo
  de lo virtual pueda ser posible. '

---
La situación que estamos viviendo en materia educativa no tiene precedentes. Si no abrimos las escuelas, se estima que casi el 50% de los niños van a continuar su proceso de escolarización con graves problemas de aprendizaje, lo que implicaría una verdadera catástrofe educativa.

Abrir o no abrir las escuelas no debería ser la cuestión, sino asegurar las condiciones necesarias tanto para volver a la presencialidad como para que algo de lo virtual pueda ser posible. Debemos asegurarnos una vuelta a clases responsable.

Pero creo que es momento de hacer foco en los chicos. Porque está demostrado que las escuelas son lugares seguros, de importante contención socio-emocional y apoyo, donde se genera actividad social e interacción humana, y donde se detectan la mayoría de los casos de violencia y maltrato infantil. Hay evidencia científica que indica que hay que volver a las aulas.

Este martes 9 de febrero nos movilizamos en todo el país por la vuelta a las escuelas y el comienzo de las clases de manera presencial. Porque a pesar de las reiteradas afirmaciones de los últimos días del ministro Trotta sobre su compromiso con el comienzo presencial del ciclo lectivo, tenemos motivos para desconfiar. La conducta general del gobierno de anunciar cosas que luego no se cumplen; las idas y vueltas en el mensaje del Jefe de Gabinete, Presidente y Ministro; el rechazo a tratar el proyecto de Ley de Emergencia Educativa a nivel nacional y en las provincias; la bajada de línea a la militancia del Frente de todos dentro de los sindicatos satélites y de algunos gobernadores contra la presencialidad (bajo la fórmula "no están dadas las condiciones"); la inoperancia del Ministerio de Educación Nacional y de los gobiernos provinciales que, a pesar de tener un año de previsión, aún no prepararon las escuelas en materia edilicia y no las dotaron con los recursos necesarios para retomar el ciclo lectivo 2021.

Junto a mi equipo trabajamos durante todo el 2020 en resaltar la importancia de la educación y de brindar los recursos necesarios para no perder la vinculación de nuestros hijos con la escuela. Solicitamos declarar la Emergencia Educativa, impulsamos la ley de Conectividad Escolar, realizamos una clase abierta demostrando que se puede retomar la presencialidad e incluso le presentamos un Protocolo con normas de sanidad al Ministerio de Educación de la Provincia de Santa Fe, adaptable a cualquier institución educativa. Porque entendemos que ante una crisis como la que nos presentó el Covid todos debemos empujar para el mismo lado.

Nuestra provincia cuenta con casi 4.800 establecimientos educativos que hoy no tienen la certeza de cuándo volverán a abrir sus puertas.

En la actualidad, más de 900.000 niños, niñas y jóvenes están en sus casas con la incertidumbre de qué les depara el ciclo lectivo 2021. De todos ellos, casi el 50% no está en condiciones de continuar con su proceso de escolarización.

Sabemos que el 2020 fue un año muy duro para todos los argentinos, para todo el mundo. Que, en materia educativa, tanto docentes como alumnos hicieron enormes esfuerzos y padecieron muchísimas limitaciones. Pensemos en los miles de chicos que no pudieron conectarse ni un sólo día para poder seguir aprendiendo; y en los maestros y maestras que están haciendo un esfuerzo enorme por seguir trabajando en condiciones de desigualdad.

Son ellos los que merecen más que nadie una respuesta concreta.

Estamos convencidos que, respetando el protocolo sanitario, podemos abrir las escuelas de manera segura y ordenada. Revinculando a los alumnos con los docentes, que además de enseñar contenido académico, colaboran en la difusión de medidas de cuidado que permiten detectar casos y colaborar con la salud pública.

En este año, la virtualidad debe ser un complemento a la presencialidad, no la principal acción educativa. Parecería -parafraseando a El Principito- que la educación no tiene esencialidad (como la salud y la seguridad) en la mirada política de las autoridades. Reclamemos junto a padres, docentes y alumnos VOLVER A LA ESCUELA.