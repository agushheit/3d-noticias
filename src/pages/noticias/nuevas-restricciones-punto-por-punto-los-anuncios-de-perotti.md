---
category: Agenda Ciudadana
date: 2021-04-22T07:37:37-03:00
thumbnail: https://assets.3dnoticias.com.ar/restricciones.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Nuevas restricciones: punto por punto los anuncios de Perotti'
title: 'Nuevas restricciones: punto por punto los anuncios de Perotti'
entradilla: El gobernador de la provincia dio precisiones en relación a las medidas
  que implementará en Santa Fe ante los crecientes contagios por coronavirus. Este
  miércoles los positivos fueron 1.949

---
Ell gobernador Omar Perotti brindó un mensaje en el que detalló las nuevas restricciones definiciones definidas para la provincia de Santa Fe como consecuencia del aumento de casos de coronavirus en los principales departamentos santafesinos.

La segunda ola del virus, según el mandatario, hizo imperiosa la necesidad de limitar la circulación de la población como una de las medidas para evitar la propagación de la enfermedad y el colapso sanitario, que ya se está observando en ciudades como Rafaela y Rosario.

En el anuncio, estuvo acompañado por los intendentes de Santa Fe y Rosario, Emilio Jatón y Pablo Javkin respectivamente, y la ministra de Salud provincial Sonia Martorano.

**Nuevas restricciones:**

* Suspensión de la circulación vehicular a partir de las 21 y hasta las 6, salvo para los esenciales.
* Competencias deportivas provinciales, zonales o locales de carácter profesional o amateur no habilitadas expresamente por las autoridades nacionales, mediante DNU del Poder Ejecutivo Nacional o Decisiones Administrativas de a Jefatura de Gabinete de Ministros del Poder Ejecutivo Nacional; incluidas las competencias automovilísticas y motociclísticas provinciales, zonales o locales.
* La actividad física en el espacio público; salvo para la realización de manera individual o acompañado por convivientes, de salidas breves de esparcimiento, en beneficio de la salud y el bienestar psicofísico, sean caminatas, correr o circular en bicicleta, sin detenerse ni concretar reuniones a aglomeración de personas.
* Pesca deportiva y recreativa en la modalidad desde costa y embarcados, navegación recreativa: y las actividades de los clubes deportivos y de las guarderías náuticas, en relación a las actividades vinculadas que se suspenden.
* Actividad hípica en hipódromos.
* Actividad de salas y complejos cinematográficos.
* Actividad del comercio minorista en locales ubicados dentro de centros comerciales, paseos comerciales o shoppings y demás establecimientos comprendidos en el articulo 2 inciso e) apartado 4. de la Ley N° 12.069 con asistencia de clientes a los locales.
* No queda alcanzada por suspensión dispuesta en el presente inciso la venta telefónica o por medios electrónicos, con la modalidad particular de entrega y pago previamente convenidos (también llamada "pick up"), a realizar en un espacio exterior lindante (playas de estacionamiento o similares).
* En cuanto a los locales de venta minorista, el horario de actividad para los no esenciales será de 10 a 19. En tanto, los esenciales como de venta de alimentos, podrán tener sus puertas abiertas hasta las 20.
* Funcionamiento de cines, bares, restaurantes, patios de comidas y espacios de juegos infantiles ubicados en centros comerciales, paseos comerciales o shoppings y demás establecimientos comprendidos en el articulo 2 inciso e) apartado 4 de la Ley N° 12069 con asistencia de clientes a los locales; salvo para los bares, restaurantes y demás locales gastronómicos que tuvieran ingresos y egresos exteriores independientes, a los que puede accederse, sin transitar por el espacio de circulación para concurrir a los locales comerciales de ventas de mercaderías.
* Se suspenderán los espectáculos teatrales y musicales, incluso los que son al aire libre.
* Siguen suspendidas las reuniones sociales en domicilios particulares.

> _Regirán desde las 0 horas del viernes 23 de abril hasta las 24 del domingo 2 de mayo._

En tanto, aclaró que los municipios y comunas tienen facultades para actuar con medidas de mayor impacto teniendo en cuenta los mismos objetivos: menor circulación y menor nivel de contagio.

"Necesitamos la colaboración de todos y es lo que el momento amerita", dijo Perotti, "para cuidar a los compañeros de trabajo, familiares y amigos", agregó.

"Entendemos que Santa Fe tiene compromisos fuertes para resguardar la salud, el trabajo y la educación", argumentó en el cierre del mensaje.

**Ampliación de las nuevas restricciones y actividades que se sostienen**

Por su parte, la ministra Sonia Martorano profundizó dando detalles de lo anunciado previamente por el mandatario provincial. Rubro por rubro y actividades deportivas y recreativas. También puntualizó cómo serán hasta el 2 de mayo las restricciones en las reuniones sociales.