---
category: Agenda Ciudadana
date: 2021-02-16T08:36:49-03:00
thumbnail: https://assets.3dnoticias.com.ar/aulas.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias con información de LT10
resumen: Cuenta regresiva para el retorno a la presencialidad
title: Cuenta regresiva para el retorno a la presencialidad
entradilla: Se trata de estudiantes que debían graduarse en 2020. Volverán a partir
  del miércoles, todos los días y por tres semanas. Se trata de una prueba para el
  inicio de clases en marzo.

---
El gobierno de Santa Fe Santa Fe confirmó el retorno a la presencialidad de clases "segura y cuidada" a partir de este miércoles en todas las escuelas de la provincia.

La vuelta será "solamente para los que terminan el nivel", explicó Víctor Debloc, secretario de Educación provincial. "Los séptimos de primaria, quintos de la escuela secundaria, con cualquier orientación, y las escuelas técnicas y agrotécnicas que terminan el sexto año; es decir, los cursos que terminan nivel están convocados a su apertura y al desarrollo de un proceso de intensificación de la enseñanza durante tres semanas, desde el 17 de febrero hasta el 12 de marzo, para culminar el proceso 2020", profundizó el funcionario.

La concurrencia será de lunes a viernes pero con una jornada reducida. "Se prevé el inicio el inicio a las 8, con la distancia social de dos metros entre uno y otro; controlando la temperatura, realizando el lavado de manos y dirigiéndose a las aulas correspondientes que preparó la escuela", explicó.

Luego, "8.30, comienza el desarrollo curricular; aproximadamente una hora y media de clases para desarrollar distintas materias que la escuela se puso de acuerdo curricularmente sobre cómo trabajar cada día; se va planeando, situadamente en relación a la matrícula y la comunidad educativa. Tenemos varios espacios, aulas, para distribuir en grupos de diez, de ocho, de 12; depende de la densidad de ocupación que vaya resultando en cada escuela. Tendrá múltiples variaciones. El Ministerio lo que hace es dar orientaciones generales para el retorno a la presencialidad segura".

Subrayó que se trabajará "con equipos docentes. Es decir, van a trabajar no solamente los maestros de séptimo; sino los de sexto, los de quinto, cuarto, tercero; se organizan, forman equipos; van circulando, van pasando de un aula a otra. Pero todo depende de la lógica organizativa de cada escuela porque una cosa es una escuela que tiene 200 alumnos, otra que tiene 300 y otra que tiene 500".

Explicó que el objetivo es "que estos grupos puedan afianzar aprendizajes, desarrollar aquellas habilidades que son fundamentales; como lingüística, matemática".

Comentó además que se "deben elaborar los informes evaluativos", sobre los cuales destacó: "Ya empezaron a elaborarse el año pasado; noviembre, diciembre. Y ahora, con estos avances se van a ir terminando los informes evaluativos para cada alumno, cada familia debe entrar en contacto con ese informe. Luego, trasladar esos informes evaluativos a calificación numérica para la confección de los títulos de secundaria y para la confección de los séptimo grado de primaria. De tal manera que hay que acreditar la finalización de nivel primario y del nivel secundario".

Apuntó además que se les brindará la posibilidad a las escuelas de "realizar los actos de colación o los actos de graduación por el sistema más conveniente: online, en pequeñas presencias, en varios días. Son protocolos cuidados siempre; tanto para la convocatoria de actividades presenciales en la escuela o cuando se hagan los actos de colación correspondiente".

Diferenció este regreso selectivo de alumnos que terminan nivel (primario o secundaria) con el inicio del ciclo lectivo que será el 12 de marzo y que tendrá otras características y será bimodal, con presencialidad alternada.

El funcionario pidió "no relajarse con los cuidados; con el lavado de manos, los dos metros de distancia social y con el barbijo". En declaraciones al programa "De10", que se emite por LT10", insistió: "El virus puede estar en cualquier lado. Yo me cuide 11 meses, desde fines de mazo –como tantos otros– y apareció el virus; me tomó, me complicó un poco. Acá estoy saliendo. No relajarse con los cuidados, porque es un problema presente, no ha pasado. Estamos en pandemia, no hay ninguna «pospandemia». No nos podemos relajar".