---
category: La Ciudad
date: 2021-11-10T06:00:38-03:00
thumbnail: https://assets.3dnoticias.com.ar/Verano.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Deporte, cultura, paseos y espacios óptimos para vivir el verano en Santa
  Fe
title: Deporte, cultura, paseos y espacios óptimos para vivir el verano en Santa Fe
entradilla: La Municipalidad puso en marcha Verano Capital. Para esta temporada, en
  las playas, parques y paseos habrá una extensa agenda con propuestas para todas
  las edades.

---
Con los piletones a pleno y los juegos de agua con todo su esplendor en el Parque del Sur, el intendente Emilio Jatón habilitó la temporada de Verano Capital 2021-2022. Las altas temperaturas se anticiparon y ese fue el motivo para adelantar la apertura que habitualmente se hace cada 15 de noviembre, en consonancia con el aniversario de la ciudad. En la ocasión, el mandatario local e integrantes de su equipo de trabajo dieron detalles de las propuestas deportivas, culturales y turísticas que tendrá Santa Fe para vivir este verano en los diferentes espacios públicos, playas, parques y paseos.

“Se adelantó el calor y se anticipó la temporada”, fueron las primeras palabras del intendente, que luego manifestó: “Siempre la habilitábamos para el aniversario de Santa Fe, pero lo adelantamos una semana porque las altas temperaturas lo requerían. De hecho, los santafesinos ya están usando los distintos espacios, están los guardavidas y está todo listo”.

En referencia concretamente al Parque del Sur, Jatón contó: “Este es uno de los espacios más elegidos. Cada fin de semana, llegamos a tener unas 4.000 personas y debe estar en condiciones. Por eso renovamos el bar, arreglamos los baños, las piletas ya están en funcionamiento y los guardavidas están trabajando”.

En esta línea, agregó: “Los lugares públicos son espacios donde la gente tiene que estar bien, por eso el Parque Sur está de esta manera, y este año le vamos sumar una intensa agenda de actividades deportivas, culturales y de todo tipo para que grandes y chicos puedan disfrutar. Van a ser un eje central para hacer ciudadanía”.

Es importante aclarar que los Espigones I y II, playa Grande, Costanera Este, el Paseo Néstor Kirchner y Los Alisos, en barrio El Pozo, están habilitados únicamente como solariums. En tanto, se habilitaron como balnearios los piletones de los parques del Sur y Garay, junto con el polideportivo de Alto Verde.

**Para disfrutar**

Es importante destacar que los juegos de agua del Parque del Sur se ponen en funcionamiento a las 10 de la mañana, cuando comienza la actividad de los guardavidas, y dejan de funcionar a las 20 horas. Luego se hace una limpieza de las piletas, el filtrado de los sistemas y la purificación del agua de modo de dejarlo listo para el día siguiente.

El secretario de Obras y Espacio Público, Matías Pons Estel, también destacó que se mejoró el sistema de alumbrado de todo el Parque del Sur con el fin de “extender el horario de uso”, sobre todo teniendo en cuenta que se habilitó un parador gastronómico. “Está todo en marcha para que los vecinos puedan disfrutar de este hermoso lugar que el municipio mantiene en óptimas condiciones”, dijo el funcionario.

**Deporte y recreación**

Deporte, cultura y turismo convivirán este verano en una extensa programación. La secretaria de Integración y Economía Social, Ayelén Dutruel detalló: “Hoy se inauguran todos los deportes de playa pero no sólo en las costaneras Este y Oeste sino también en el Parque del Sur, en particular. Se pensó en nuestros parques, en la actividad deportiva pero también en la recreación para todas las edades”.

En este sentido, la municipalidad despliega el deporte y las prácticas lúdicas, recreativas y de actividades físicas desde una mirada integral, abarcando todos los sectores sociales, las instituciones, agrupaciones, géneros y franjas etáreas, a fin de llegar a cada rincón de la ciudad con propuestas concretas, singulares, significativas y acordes a las necesidades de cada grupo en particular.

De este modo, continúan las propuestas anuales tales como funcional, ritmos, patín, yoga, tai chi, iniciación deportiva, atletismo, aerolocal, entre otros, en los espacios deportivos como el Centro Gallego, el Polideportivo La Tablada, la Dirección de Deportes y la Estación Belgrano; y se suman los parque del Sur y Garay, y el Polideportivo Alto Verde. En todos los lugares se extenderán los horarios hasta las 21, adaptándose a la temporada de verano.

**Cultura y turismo**

El secretario de Educación y Cultura, Paulo Ricci, hizo referencia a las actividades diseñadas para la temporada estival: “Como sucedió el año pasado, vamos a sostener una agenda cultural en los paradores de las playas con danza y música en vivo todos los fines de semana. También estaremos en los parques tradicionales y sumamos a la programación el Parque de la Estación Mitre, que fue recientemente inaugurado, donde los sábados y domingos habrá espectáculos infantiles. En paralelo, sostendremos la programación musical, gratuita y libre en los espacios municipales como el Mercado Progreso y el anfiteatro del Parque del Sur”, describió.

Por último, el director de Turismo, Franco Arone, hizo referencia a las acciones previstas para la temporada y promoción de Santa Fe Capital. “Hoy tenemos nueve paseos guiados y gratuitos que se están consolidando. Recientemente inauguramos el Paseo por el Cementerio Municipal y estos espacios públicos como el Parque del Sur se suman a la propuesta turística. Se instaló una carpa con información que va a ser itinerante para mostrar al santafesino y al visitante todas las alternativas disponibles”, dijo Arone.

Antes de finalizar, mencionó las promociones previstas en la región para buscar al turista de cercanía. “Hoy la ciudad está teniendo muy buenos niveles de ocupación y esperamos tener una muy buena temporada de verano”, finalizó el director de Turismo.