---
category: Estado Real
date: 2021-01-29T09:57:40Z
thumbnail: https://assets.3dnoticias.com.ar/vivienda.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: 'Perotti: “La vivienda es uno de los multiplicadores más importantes del
  empleo”'
title: 'Perotti: “La vivienda es uno de los multiplicadores más importantes del empleo”'
entradilla: 'El gobernador encabezó este jueves la licitación de la primera etapa
  de la construcción de 220 viviendas en la ciudad de Santa Fe, que cuenta con una
  inversión de $750 millones.

'

---
El gobernador Omar Perotti encabezó este jueves la apertura de sobres con ofertas para la construcción de la primera etapa de 220 viviendas en el loteo Esmeralda Este II de la ciudad de Santa Fe, con una inversión de más de $750 millones.

Perotti expresó que “esta licitación de 220 viviendas y su infraestructura nos da la posibilidad de tener un nuevo plan en marcha. Hace pocos días firmamos un convenio con Nación para poder construir viviendas con planes de financiamiento nacional en el territorio provincial, en cada uno de los municipios de la provincia, es decir, esto que estamos haciendo aquí es algo que se viene repitiendo”.

“La vivienda va a ser un motor importante, no solamente por la necesidad, ya que hay más de 100.000 santafesinos inscriptos en el registro esperando tener su casa, sino porque es uno de los multiplicadores más importantes del empleo”, agregó el gobernador.

Por último, Perotti manifestó que “entre todos los planes vamos a llegar a 6.000 viviendas en la provincia de Santa Fe” y luego, homenajeó al exgobernador Jorge Obeid, en un nuevo aniversario de su muerte, recordando que “le puso una impronta muy fuerte a su gestión, principalmente en vivienda, que nos permitió llegar a casi 20 mil unidades habitacionales en todo el territorio provincial. Es una muy buena manera de recordarlo en el día de hoy, poniendo en marcha 220 viviendas”, concluyó.

Por su parte, la ministra de Infraestructura, Servicios Públicos y Hábitat, Silvina Frana, expresó que “la construcción de las viviendas demanda una inversión del gobierno provincial, que quiero destacar porque no forma parte de las cuatro mil que se van a trabajar con Nación, esto ya estaba previsto en el presupuesto provincial y va a dar respuesta a muchas familias, además evidencia que empezamos el año con la firme decisión del gobernador de tener una fuerte política de viviendas”.

**LAS OFERTAS**

En primer lugar, se conocieron los oferentes para la ejecución de 80 viviendas de 2 dormitorios, que cuentan con un presupuesto oficial de $273.168.147.

Para esta licitación se presentaron 14 oferentes: UT conformada por Mundo Construcción S.A y Capitel S.A presentó una oferta de $ 260.825.382,70; Pirámide S.A ofertó $ 293.007.323,08; Pecam S.A. presentó la oferta de $ 250.666.718, 94; Cocyar S.A. ofertó la suma de $ 302.650.189,92; Edeca S.A. presentó la oferta de $ 286.815.359,99; Sanimat SRL ofertó $ 275.535.431,40; Pilatti S.A. presentó la oferta de $ 255.948.924,82; Coirini S.A. ofertó $ 299.771.892,94; Coemyc S.A. presentó la oferta de $ 264.326.105,80; VFM S.A ofertó $ 284.000.001,20; Epreco S.R.L. presentó la oferta de $ 267.504.572,74; UT conformada por Constructora Comercial Arq. Juan Manuel Campana y Arq. Mario E. Iglesias Construcciones ofertó $ 275.138.331,74; Tecsa S.A. ofertó $ 269.330.472,62; Dinale S.A presentó la oferta de $ 335.279.985,71;

A su turno, se abrieron los sobres para licitar la construcción de 68 viviendas de 2 dormitorios, con un presupuesto oficial de $231.865.347.

En la oportunidad se presentaron las siguientes empresas oferentes: UT Mundo Construcciones S.A. y Capitel Constructora ofertó $ 229.148.442,12; Pirámide S.A. ofertó $ 251.041.352,80; Pecam S.A. ofertó $ 264.232.581,48; Cocyar S.A. ofertó $ 266.344.728,88; Edeca S.A. ofertó $ 248.095.900,27; Sanimat S.R.L ofertó $ 234.346.975,96; Dinale S.A. ofertó $ 282.339.138,37; Pilatti S.A. ofertó $ 231.639.694,83 ; Corini S.A. ofertó $194.940.104,22 ; Coemyc S.A. ofertó $ 226.919.794,20; VFM S.A. ofertó $ 247.000.000,60; Epreco S.R.L ofertó $ 229.346.572,16; UT Constructora Comercial Arq. Juan Manuel Campana y Arq. Mario E. Iglesias Construcciones ofertó $ 235.351.711,85; Tecsa S.A. ofertó $ 234.287.777,24.

Por otro lado, este viernes se llevará a cabo la licitación de las 72 viviendas restantes.