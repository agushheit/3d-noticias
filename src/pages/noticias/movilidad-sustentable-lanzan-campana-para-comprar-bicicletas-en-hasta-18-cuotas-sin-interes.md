---
category: Agenda Ciudadana
date: 2022-01-27T06:00:11-03:00
thumbnail: https://assets.3dnoticias.com.ar/biciasbna.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: 'Movilidad sustentable: lanzan campaña para comprar bicicletas en hasta 18
  cuotas sin interés'
title: 'Movilidad sustentable: lanzan campaña para comprar bicicletas en hasta 18
  cuotas sin interés'
entradilla: "Desde este jueves de habilitará \"la adquisición de bicicletas, de distintos
  rodados, modelos y marcas, en hasta 18 cuotas sin interés, con tarjetas de crédito
  Visa y Mastercard emitidas por el BNA\", informó el Banco.\n\n"

---
El Banco Nación (BNA) lanzará una campaña este jueves para la compra de bicicletas en hasta 18 cuotas sin interés, con el objetivo de promover la movilidad sustentable.  
  
"El Banco de la Nación Argentina lanzará mañana una campaña para la adquisición de bicicletas, de distintos rodados, modelos y marcas, en hasta 18 cuotas sin interés, con las tarjetas de crédito Visa y Mastercard emitidas por el BNA", informó este miércoles la entidad a través de un comunicado  
  
La propuesta estará vigente dos días en la TiendaBNA, una plataforma de comercio electrónico desarrollada entre el BNA y Nación Servicios.  
  
La oferta incluye bicicletas de diferentes marcas, como Aire Libre, Avelino Bike, Daewoo, Faydi, Fierce, Futura, Kuwara, Philco, Randers, Shifter, Siambretta, SLP y UniBike, y su disponibilidad se puede consultar en el sitio web [**https://tiendabna.com.ar**](https://tiendabna.com.ar "https://tiendabna.com.ar")  
  
La entidad aclaró que la compra de cualquiera de los productos en hasta 18 cuotas sin interés no implica ningún recargo financiero para el comercio o los clientes, en tanto la bonificación estará a cargo del Banco Nación y los clientes podrán elegir el financiamiento más conveniente, de 3 a 18 cuotas sin interés.