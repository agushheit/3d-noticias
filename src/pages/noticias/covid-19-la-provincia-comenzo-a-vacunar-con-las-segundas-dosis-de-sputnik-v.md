---
category: Estado Real
date: 2021-01-21T09:00:34Z
thumbnail: https://assets.3dnoticias.com.ar/sputnik1.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno del Provincia de Santa Fe
resumen: 'Covid-19: La provincia comenzó a vacunar con las segundas dosis de Sputnik
  V'
title: 'Covid-19: La provincia comenzó a vacunar con las segundas dosis de Sputnik
  V'
entradilla: Tras la llegada de los tratamientos se realiza la inoculación a los trabajadores
  de salud que se aplicaron la primera dosis.

---
El Ministerio de Salud informó que, en el marco de la campaña nacional histórica de vacunación contra el Covid-19, comenzó este miércoles la inoculación con la segunda dosis de Sputnik V a los trabajadores de la salud y de esta manera completar el tratamiento de inmunización.

“Este va a ser un día recordado por muchos agentes que trabajan en la salud. La llegada a tiempo de esta segundo componente de la vacuna nos da una garantía de lo que se había pactado con el gobierno nacional en cuanto al cronograma de aplicación de la vacuna”, indicó desde el Cemafe el secretario de Salud, Jorge Prieto.

“Recordemos que en el inicio de esta campaña, se comenzó con los servicios críticos e hipercríticos. Asimismo, la provincia de Santa Fe está preparada con todo el recurso humano y los elementos necesarios para el momento en el que ingresen mas vacunas y de esta manera completar los 50 mil trabajadores de la salud que están faltando, en una segunda instancia se continuará con personal de seguridad y el resto de las fuerzas, se continuará con docentes y luego se personas de 18 a 59 años con comorbilidades y ni bien se habilite, continuaríamos con una población muy sensible que los los adultos mayores de 60 años”, destacó.

Por su parte, y al ser consultada por el proceso de vacunación en el efector, la referente en el área del Cemafe, Vanina Pairola, explicó que “tenemos los grupos seleccionados de a 5 personas, está todo organizado para que cuando ellos lleguen podamos tener turnos consecutivos y así no desperdiciar ninguna dosis”.

**REGISTRO DE VACUNACIÓN**

Al ser consultado sobre el registro de vacunación, el secretario de Salud explicó que “hay mucha adhesión por parte de la población a inscribirse para solicitar ser vacunados de forma voluntaria y para ello, se está trabajando de manera coordinada en la implementación de una aplicación donde podrán inscribirse aquellos santafesinos y santafesinas, que encuadren en la población objetivo, su intención de vacunación”.

“De ahí en más, una vez que lleguen las vacunas y se establezca la gradualidad del programa de vacunación, cada una de las personas a través de su DNI va a poder referenciarse al centro de salud mas cercano a su domicilio donde podría recibir la vacuna”, concluyó Prieto.

**OPERATIVOS TERRITORIALES**

Por su parte, el director regional de Salud de Santa Fe, Rodolfo Rosselli, detalló que “estamos abocados intensamente a la vacunación del personal de salud, pero también continuamos con los operativos sanitarios en los distintos barrios de la región”.

Además agregó: “Se intensificaron las acciones territoriales y nos abocamos fundamentalmente en acciones de información, promoción, toma de temperatura y test de olfatos en distintos puntos estratégicos en la ciudad de Santa Fe”.

“A través del programa Verano Seguro, que se lleva adelante en la plaza Pueyrredón, la costanera Oeste y la terminal de omnibus, se le brinda información a los ciudadanos y ciudadanas sobre dengue, calendario de vacunación y covid”, finalizó Roselli.

**HOSPITAL DE NIÑOS ZONA NORTE DE ROSARIO**

Asimismo, la inoculación con la segunda dosis de Sputnik V comenzó en la ciudad de Rosario. Al respecto, la directora del hospital de Niños Zona Norte de la ciudad, Mónica Jurado dijo que “en el marco de esta gran campaña de vacunación en la provincia de Santa fe y con mucha adhesión por parte de los trabajadores del hospital, empezamos a vacunar hoy con el segundo componente de la vacuna que completa el tratamiento de inmunización”.

“La vacuna tiene un componente en la primera dosis y otro componente distinto en la segunda, es por ello que se trata de un tratamiento”, continuó.

Asimismo, la directora del efector recordó que “por cada envase vienen 5 dosis y es por ello que armamos sistema programado y se esta vacunando por servicio de riesgo a menor riesgo y vamos tamizando los equipos para que todos reciban su dosis. Los turnos son en grupos de a 5 personas para no desperdiciar ninguna vacuna”.

“Se trabaja más tranquilos con el personal inmunizado, pero igualmente seguimos con los cuidados que hemos aprendido durante este tiempo de pandemia. Es fundamental seguir cuidándonos, utilizando el tapa boca, respetando el distanciamiento social, el lavado de manos y entender que la pandemia no se fue, queda un largo camino y que el deseo de todas y todos es vacunarnos, sobre todo aquellas personas de riesgo porque necesitamos tener el 70% de la población vacunada”, concluyó Jurado.