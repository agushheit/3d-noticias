---
category: La Ciudad
date: 2021-11-07T06:15:47-03:00
thumbnail: https://assets.3dnoticias.com.ar/alquiler.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Inquilinos en alerta por la suba de alquileres: 50% en noviembre'
title: 'Inquilinos en alerta por la suba de alquileres: 50% en noviembre'
entradilla: Quienes cumplieron un año de contrato en el mes de noviembre tuvieron
  subas del 50 por ciento, en base al coeficiente que elabora el banco central

---
El inicio de un nuevo mes, para quienes que renovar contratos de alquiler o alquilar una nueva propiedad trae consigo la suba de los montos a abonar. Desde la Asociación de Inquilinos de Santa Fe mostraron nuevamente su preocupación, principalmente para aquellos que cumplieron con su primer año de contrato con la nueva ley de alquiles, la cual dispone que las subas se realizan bajo el coeficiente elaborado por el Banco Central, en dónde se estable un promedio de la suba entre el índice de inflación y los salarios. Esto arrojó para el mes de noviembre, un incremento que ronda el 50 por ciento.

"Entendemos que se debe a la crisis económica que atraviesa el país dónde la inflación es altísima y los salarios de los trabajadores, que son los que mayormente alquilan, han quedado muy por el piso, entonces de ese promedio es donde surge el 50% que va a tener que pagar el inquilino durante el plazo de un año. Esta situación ya era preocupante en plena pandemia y aun seguimos en esta situación aunque se puede apreciar que estamos saliendo", dijeron desde la asociación.

Por otro lado también aseguraron que para hacerle frente a los aumento "los inquilinos resignan cuestiones básicas, achicando gastos dónde se pueda para pode pagar el techo dónde habitan, siendo un derecho básico, pero se va a comenzará a ver la dificultad para poder pagar, habrá algunos caso en dónde buscarán mudarse a la periferia o volver con los familiares".

En cuanto a la crisis, "se profundizó de un año al otro, ya que en el 2020 se contaba con un decreto de necesidad y urgencia que congelaba alquileres y prohibir desalojos, algo que ya dejo de estar vigente . Ahora los precios de los alquileres no solo se descongelaron, sino que se han disparado producto de la especulación inmobiliaria y financiera y si le sumamos la suba, no hay bolsillo que resista"