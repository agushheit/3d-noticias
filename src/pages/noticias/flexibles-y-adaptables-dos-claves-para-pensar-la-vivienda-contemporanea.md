---
category: La Ciudad
date: 2021-09-05T06:00:02-03:00
thumbnail: https://assets.3dnoticias.com.ar/CUIDAD.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'Flexibles y adaptables: dos claves para pensar la vivienda contemporánea'
title: 'Flexibles y adaptables: dos claves para pensar la vivienda contemporánea'
entradilla: 'Espacios neutros y desregulados, sostenibilidad ambiental y revalorización
  de las áreas comunes para favorecer el encuentro son algunos de los conceptos que
  desarrolla la Arq. Margarita Trlin. '

---
El acceso a la vivienda sigue siendo, más que una preocupación, un sueño imposible para buena parte de la comunidad. Pero por fuera de los planes, los créditos, los alquileres y requisitos, "debe ser considerada un derecho de todas las personas". Así lo entiende Margarita Trlin, Arquitecta, profesora de Proyecto Arquitectónico II y secretaria de Investigación y Relaciones internacionales de FADU-UNL.

En diálogo con los medios, aporta una perspectiva profesional y académica sobre la problemática habitacional, los cambios sociales que se traducen en nuevas necesidades y los nuevos usos que redescubrió la pandemia por Covid-19 y el consecuente aislamiento generalizado. Todo atravesado por un componente vital: el factor ambiental.

\-¿Qué aportes puede hacer la universidad a la problemática del déficit de viviendas?

.El tema en cuestión constituye uno de los aspectos que preocupa a la UNL, el cual atraviesa diferentes campos del conocimiento, en particular aquellos ligados al proyecto del hábitat. La universidad en tanto lugar de producción de conocimiento, de discusión y reflexión, permite el abordaje del tema en términos reflexivos y crítico-propositivos en sus más variados aspectos. Una consideración que comprende el tema de la vivienda desde la problemática de su inserción urbana, su relación con los espacios públicos, hasta las nuevas conceptualizaciones del hábitat doméstico atento a otros modos de vivir y a las diferentes configuraciones de las unidades de convivencia.

Pensar la vivienda desde la complejidad de la vida contemporánea resulta perentorio; los cambios señalados son poco o nada considerados en los proyectos de vivienda colectiva o social. Por lo tanto, es preciso encarar el tema con otras perspectivas que consideren los nuevos modos de vida y atiendan la emergencia de nuevos actores, y redefinir varios aspectos del habitar. La vivienda colectiva y la vivienda social insertas de manera articulada a la trama urbana, en complejos de mediana densidad o bien en las periferias en programa híbridos que integren hábitat y producción, configuran salidas posibles para enfrentar el déficit de vivienda bajo parámetros ambientalmente sostenibles.

\-¿Cómo podrían adaptarse las construcciones a las nuevas realidades sociales y familiares? Hogares uniparentales, familias ensambladas, jóvenes, personas que viven solas...

\-La familia tradicional basada en el matrimonio con hijos viviendo bajo un mismo techo está en crisis. Nuevas relaciones y nuevos modos de vida requieren de una revisión crítica de los proyectos de vivienda ya se trate de vivienda individual o colectiva. La vivienda contemporánea debe responder a la diversidad de situaciones de vida actuales tanto en lo relativo a la conformación familiar o de unidades de convivencia familias monoparentales, familias ensambladas, parejas, personas que viven solas, personas que viven juntas de manera transitoria, incorporación del trabajo en el hogar, entre otras condiciones. En este sentido, los criterios de flexibilidad y adaptabilidad cobran relevancia, los espacios de uso indeterminado, no apriorístico configuran un capital ya que habilitan apropiaciones variadas a lo largo del tiempo.

La posibilidad de mayor grado de libertad en la determinación de usos y destinos, permite a los habitantes desregular el espacio. En este sentido el concepto de ductilidad, versatilidad e indeterminación permite adecuar los diferentes ámbitos y la vivienda en general a posibles y cambiantes requerimientos. En este punto algunas soluciones constructivas, y algunos elementos diseñados por y para la movilidad, colaboran en la creación de espacios neutros capaces de responder a estos nuevos escenarios.

En la mayoría de las unidades de vivienda las condiciones dimensionales dan muy pocas opciones para promover otro tipo de apropiaciones ya que han sido proyectadas desde una supuesta y estructurada función previa de los espacios.

\-¿La pandemia dejó en evidencia nuevas necesidades en materia habitacional?

\-El habitar doméstico está sujeto a permanentes variaciones, algunas motivadas por cambios en los modos de vida, por el trabajo a distancia dentro del hogar y por la emergencia de acontecimientos inesperados como la pandemia. Estos nuevos requerimientos necesitan de una respuesta espacial capaz de absorber en el tiempo este entramado de intereses.

La vivienda debe ser considerada como un derecho de todas las personas, y por tanto alejado de todo tipo de especulaciones, principalmente las referidas a la especulación inmobiliaria. Se hace imperioso replantear las demandas programáticas y por ende las superficies asignadas, replanteando su proyecto desde esa concepción de derecho y con la amplitud y generosidad que merece el tema. La incorporación de espacios intermedios en viviendas en altura a partir de terrazas, balcones y espacios comunes de sociabilización se hace imprescindible en cualquier tipo de vivienda; el rigor, la cuarentena forzosa del año pasado puso de relieve esta necesidad. A esto se suma la consideración de ámbitos muy bien iluminados con luz natural y ventilados adecuadamente con buena circulación de aire.

Si se piensa en una ciudad de cercanía, con densidades medias, que permita a sus habitantes contar con los equipamientos públicos, comercio, lugares de esparcimiento y trabajo en un espacio de 15 minutos, se hace necesario revisar la forma en que la vivienda colectiva aporta a esa idea. Entornos de media densidad parecieran aportar en ese sentido, mientras que los grandes conjuntos de alta densidad poco colaboran con la pretensión de una ciudad, de un barrio ambientalmente sostenible. Hay excelentes ejemplos que han formalizado estos valores a través de la historia, dando cuenta de la importancia y puesta en valor de estos aspectos, enalteciendo el sentido del vivir en el ámbito privado y el comunitario, en una fluida relación de intercambio.

En esta revisión merece un especial apartado la consideración en los nuevos proyectos de las condiciones climáticas del sito de emplazamiento, el aporte de soluciones creativos que permitan el ahorro energético y promuevan el uso racional de los recursos, un conjunto de aspectos que procuran garantizar el grado de confort adecuado a los habitantes, bregando por la construcción de entornos sostenibles.

La consideración de los espacios públicos y comunitarios otorgan valor y sentido a la vivienda, como espacios complementarios al habitar doméstico, brindando el ámbito adecuado para los necesarios vínculos que en este contexto de pandemia es necesario rescatar y poner en valor. En esta sintonía las vías públicas para peatones en todas sus variantes y los servicios públicos de transporte accesibles deben tener prioridad, desalentando el uso irracional del automotor, habida cuenta de los beneficios que trae a las ciudades la ausencia de tráfico y por lo tanto la disminución de factores contaminantes.

"La vivienda colectiva y la vivienda social insertas a la trama urbana, en complejos de mediana densidad o en las periferias en programa híbridos que integren hábitat y producción, configuran salidas posibles para enfrentar el déficit de vivienda bajo parámetros ambientalmente sostenibles".

**PERFIL**

Margarita Trlin es Arquitecta y Magíster; profesora de Proyecto Arquitectónico II y Secretaria de Investigación y Relaciones internacionales de FADU-UNL.

**DESPOBLAMIENTO**

"Los procesos de despoblamiento que vienen sufriendo las áreas centrales de las ciudades, agravados por la pandemia, traen consigo problemas sociales y económicos que afectan notablemente la dinámica urbana", explicó Trlin. "Rehabitar estas áreas a partir de políticas y programas de vivienda específicos que procuren evitar y mitigar estos procesos, como sucede en otras ciudades como Montevideo -por ejemplo-, contribuiría a restablecer la calidad de vida en toda su riqueza y complejidad", cerró.

**AIRE, LUZ, AGUA Y VERDE**

\-¿Cómo incorporar el tema ambiental en el diseño de viviendas? ¿Es una estrategia económicamente accesible?

\-No podríamos hablar de la calidad de vida de la vivienda sin considerar estos aspectos. Es de destacar la importancia de las cuestiones ambientales, la consideración de indicadores como el metro cuadrado por habitante de espacios abiertos destinados a plazas, parques, espacios de circulación, un conjunto de variables que es necesario verificar e impulsar programas y políticas que aporten a alcanzar los valores óptimos de dichas relaciones.

El cambio climático, la salud, la polución, la contaminación, etcétera, constituyen preocupaciones que deben ser parte de la agenda pública, repercutiendo indefectiblemente en la vivienda en todas sus manifestaciones. Hay experiencias interesantes en muchas ciudades del mundo que a través de su historia han promovido estos aspectos. La Organización Mundial de la Salud establece como criterio aceptable 15 metros cuadrados por habitante de estos lugares. Ciudades como Curitiba en Brasil con unos 50 metros cuadrados por habitante de espacios verdes es un ejemplo en nuestro continente, donde por iniciativa pública a través de gestiones ejemplares se ha convertido en un modelo de ciudad planificada, jerarquizando la condición urbana de la ciudad.

En nuestro país, Rosario es otra ciudad que, sin tener los índices deseados, tiene uno de los más altos: 12 metros cuadrados por habitante; definiendo a través de su historia una preocupación por estos aspectos. La asimilación de diversas tecnologías como las energías renovables, la utilización de materiales adecuados y la preocupación por el buen diseño de las viviendas individuales y agrupadas conforman activos que repercuten en el logro de una buena calidad de vida contribuyendo también al cuidado del medio ambiente. En este sentido, la utilización de colectores solares para el calentamiento de agua, calefacción o generación de electricidad, son un aporte interesante.

El aprovechamiento del agua de lluvia recolectada en los edificios y o viviendas para su reutilización como aguas grises también es una manera de potenciar los recursos que tenemos. En este sentido, esta última práctica ayuda también a mitigar los efectos de inundación de ciertas áreas de las ciudades por ausencia de suelos absorbentes, producto del avance irracional de la ciudad. En síntesis, la ciudad es un organismo vivo, y dentro de ella los distintos actores que la conforman a través de manifestaciones de la esfera pública como privada requiere de una mirada integral y sistémica, donde nada vale por lo individual. Es necesario llevar a cabo una planificación que pueda absorber todos estos requerimientos.