---
category: Agenda Ciudadana
date: 2021-07-21T06:00:16-03:00
thumbnail: https://assets.3dnoticias.com.ar/motos.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La provincia refuerza la seguridad del norte de la bota
title: La provincia refuerza la seguridad del norte de la bota
entradilla: El ministro Jorge Lagna entregó motovehículos para San Javier y el departamento
  General Obligado. Además, firmó un convenio para crear una nueva Escuela de Policía
  en Reconquista y refaccionar la comisaria de Florencia.

---
El gobierno de la provincia, a través del Ministerio de Seguridad avanza en el Plan Integral de Seguridad en el norte provincial. En esta oportunidad, el ministro de Seguridad, Jorge Lagna, entregó 12 motos en Reconquista para equipar a la Policía del departamento General Obligado. Las nuevas motos se destinarán a las localidades de Villa Ana, Lanteri, Reconquista y Avellaneda.

Además, Lagna firmó un convenio para inaugurar en 2022 una sede de la Escuela de Policía en Reconquista.

Al respecto, el ministro de Seguridad, expresó: “Como siempre lo digo, es muy difícil gestionar la seguridad en un contexto de pandemia y desigualdad social como el que vivimos actualmente, pero tenemos el firme compromiso de avanzar en el Plan Integral de Seguridad para todo el territorio provincial, que iniciamos a principio de gestión con las Mesas de Seguridad Local y que vamos diseñando y adaptando de acuerdo a las necesidades de cada pueblo y ciudad que recorremos".

"Nos comprometimos a tener más policías en la calle, a equipar y modernizar a las fuerzas de seguridad para mejorar el trabajo preventivo y reforzar el patrullaje", indicó Lagna y agregó que "por eso hoy entregamos estas motos para incrementar la flota, que se suman a las camionetas entregadas y a los vehículos que están en proceso de licitación".

"Además, firmamos un convenio para crear una sede del Instituto de Seguridad Pública en Reconquista con el objetivo de regionalizar la educación de nuestros futuros agentes y para que tengan mayores vínculos de arraigo con la región en la que trabajan", señaló Lagna.

"Trabajamos en conjunto con Municipios y Comunas para reforzar la seguridad desde un enfoque integral con el fin de prevenir el delito predatorio e investigar en profundidad las grandes bandas criminales", culminó el ministro.

**Presentes**

En el acto estuvieron presentes el secretario de Gestión Institucional y Social de la Seguridad, Bruno Rossini; el subsecretario de Formación y Capacitación, Andrés Rolandelli; el subjefe de Policía de la provincia Ariel Zancochia; el coordinador del Ministerio de Seguridad, Nicolas Nardoni, el Director del Isep, Gabriel Leegstra, el Intendente de Reconquista, Enrique Vallejos; los presidentes Comunales de Lanteri y Villa Ana y otras autoridades provinciales y policiales.

**Recorrida por San Javier y Florencia**

El ministro Jorge Lagna también recorrió la localidad de San Javier donde llevó adelante la entrega de 4 motos 0 Km para la Policía del departamento. Y visitó Florencia, donde entregó otras 2 motos 0 km y firmó un convenio para la refacción de la comisaría local.

La entrega se realizó en el marco de la inversión integral en tecnología, motos y vehículos que viene realizando la provincia a través del Ministerio de Seguridad.