---
category: La Ciudad
date: 2021-07-10T06:15:26-03:00
thumbnail: https://assets.3dnoticias.com.ar/paso.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'Elecciones en Santa Fe, carrera al Concejo: nombres de peso entre las precandidaturas'
title: 'Elecciones en Santa Fe, carrera al Concejo: nombres de peso entre las precandidaturas'
entradilla: A la medianoche de este viernes venció el plazo de presentación de listas
  de precandidaturas ante las autoridades partidarias de quienes competirán en las
  elecciones Paso , el próximo 12 de septiembre.

---
Laura Mondino y Leandro González (actual presidente del Legislativo local) buscarán renovar bancas dentro del FPCyS. Adriana "Chuchi" Molina, Carlos Suárez, Hugo Marcucci y Luciana Ceresola son cabeza de listas dentro de Juntos por el Cambio. En el PJ, Jorgelina Mudallel, Alejandro Rossi y Marcela Aeberhard también competirán. El 12 de septiembre, las Paso.

A la medianoche de este viernes venció el plazo de presentación de listas de precandidaturas ante las autoridades partidarias de quienes competirán en las elecciones Primarias, Abiertas, Simultáneas y Obligatorias (Paso), el próximo 12 de septiembre, por una banca en el Concejo santafesino. El lunes deberán hacerlo ante el Tribunal Electoral de la provincia. Y entre las tres alianzas políticas más importantes, las cabezas de listas llevan nombres de peso.

Todo indicaba que en la alianza del Frente Progresista, Cívico y Social (FPCyS) iba a haber una lista de unidad, pues la intención era "no obturar si hay otras expresiones minoritarias que quieran participar", dijeron fuentes cercanas al intendente Emilio Jatón. La concejala Laura Mondino (persona de gran confianza del titular del Ejecutivo local) iba a encabezar la lista oficialista, y el actual presidente del Legislativo local, Leandro González (Radicales Libres) ocupaba el segundo lugar.

En la alianza de Juntos por el Cambio eran cuatro las listas para la elección interna. La que impulsa el último intendente de Santa Fe, José Corral, que liderará Adriana "Chuchi" Molina; y otras tres que encabezarán Hugo Marcucci, del sector de Mario Barletta; Carlos Suárez (UCR) y Luciana Ceresola (Pro), respectivamente.

En el Frente de Todos (PJ) ya definieron que competirán en la interna partidaria Alejandro Rossi, Jorgelina Mudallel, Marcela Aeberhard y Violeta Quiróz. También sonaba unas listas encabezadas por Mariana Bergallo, dentro de la alianza del Frente de Todos.

**Algunos nombres confirmados**

Dentro de Juntos por el Cambio, en la lista de precandidatos que lleva como "primera" a Adriana "Chuchi" Molina, aparecía en segundo lugar Carlos Pereira (actual concejal y ex secretario General de la gestión de Corral, quien buscará retener su banca). La tercera es Ángeles Álvarez, militante de la Juventud Radical. Siguen Mauricio Amer, de la Coalición Cívica, trabaja temas vinculados a la transparencia; Ileana Alvarenga, del Pro, participante activa en grupos de seguridad y proteccionista independiente; Ramón Narváez, referente de barrio Barranquitas; Clara Ruda, trabaja temas vinculados a la educación y la vuelta a clases; Luis Pancera, referente de Barrio San Martín.

Otra lista -siempre dentro de este espacio- es la que liderará Carlos Suárez (actual concejal, también busca renovar su banca), seguido por Florencia Maresca, Sergio Acosta, Carla Korol, Soledad Santillán, Carlos Ferrero y Nancy Jorge.

También la actual edila Luciana Ceresola intentará retener su atril en el recinto de Salta 2943. Estaba confirmado que la referente local del Pro iba a ser cabeza de lista, y que será secundada por Ariel Rodaz (pastor evangélico, del partido UNO, lista Santa Fe nos Une).

Y una cuarta lista (del sector del ex intendente de la ciudad, Mario Barletta) será la que encabeza Hugo Marcucci (ex senador por La Capital). Estará acompañado por Celina "Pupi" Traverso (reconocida jugadora de hockey local que tuvo proyección internacional) y Ulrich Lehmann, que fuera dos veces precandidato al Concejo en las elecciones de 2017 y 2019.

**Frente de Todos**

Hasta el final, la incógnita pasaba por quiénes integrarán la lista Hacemos Santa Fe, que es el espacio del gobernador Omar Perotti y que encabezará Jorgelina Mudallel, persona de confianza del primer mandatario santafesino. Según supo este medio, por el segundo lugar podrían ir Fernando Molina (ex secretario administrativo del Concejo) o Claudio Aibinder (la decisión no estaba definida, al cierre de esta edición), y Natalia Ocampo en tercer lugar.

Alejandro Rossi (Unidad Ciudadana) será otro de los que intentará llegar al Concejo. Estará acompañado de Camila Kletzky, Edermilo "Milo" Maidana, Andrea Rodríguez, Néstor Raúl Guerrero, Ximena Frois, Alejandro Gitrón, Marisa Almada y José Luis Martínez.

Otra de las listas confirmadas dentro de la alianza del Frente de Todos es la encabezada por Violeta Quiróz (militante social, referente de la Agrupación Mesas de Trabajo, conducida por Gerardo Crespi), quien estará secundada por Daniel Godoy (militante político que acompañó la carrera política del ex gobernador Jorge Obeid) y la Dra. María Laura Segado (defensora de los derechos de las infancias y adolescencias).

El sector de la ex edila santafesina Marcela Aeberhard también competirá en las internas. Cabe recordar que la ex concejala lanzó semanas atrás la Agrupación "23 de Septiembre", un espacio dentro del PJ "de característica plural, inclusivo y diverso".

**DEBUT**

En la provincia, con el debut de la Ley de Paridad para conformar las listas, se renuevan 14 intendencias, todas las comisiones Comunales y, parcialmente, todos los Concejos Municipales. Cabe recordar que quienes renuevan sus bancas son Leandro González, Laura Mondino y María Laura Spina (FPCyS); Carlos Pereira, Carlos Suárez y Luciana Ceresola (UCR-Pro-Juntos por el Cambio); dentro del PJ, Jorgelina Mudallel y Sebastián Pignata (del Frente Renovador-Juntos, fuerza que integra la alianza Frente de Todos).