---
category: Agenda Ciudadana
date: 2021-12-22T06:00:39-03:00
thumbnail: https://assets.3dnoticias.com.ar/variante.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Se reportaron dos nuevos caso de Ómicron en la provincia de Santa Fe
title: Se reportaron dos nuevos caso de Ómicron en la provincia de Santa Fe
entradilla: 'Con estos ya suman cuatro los casos confirmados con la nueva cepa de
  Ómicron. Se trata de dos personas de la localidad de Funes

'

---
El Ministerio de Salud de la provincia confirmó este martes que fueron detectados otros dos nuevos casos de la variante Ómicron de coronavirus en territorio santafesino.

En el parte sanitario se detalló que se trata de habitantes de la localidad de Funes cuyo nexo epidemiológico aún está en investigación.

Los pacientes son varones de 48 y 42 años, contactos estrechos entre sí. Cuentan con las dos dosis de la vacuna para el coronavirus y se encuentran cursando la enfermedad en su domicilio.

Con estos casos, la provincia de Santa Fe registra cuatro personas contagiadas con la cepa Ómicron de covid-19 si se suman los dos ya confirmados en Rosario y Roldán.

En este marco, el Ministerio de Salud recomendó que ante cualquier síntoma compatible con coronavirus –fiebre de 37,5°C, tos, dolor de garganta dificultad respiratoria, rinitis/congestión nasal, dolor muscular, cefalea, diarrea y/o vómitos– deben acercarse al puesto de testeo más próximo.

Asimismo, se aconsejó sostener los cuidados personales, lavado de manos, distanciamiento social y el uso de barbijo.