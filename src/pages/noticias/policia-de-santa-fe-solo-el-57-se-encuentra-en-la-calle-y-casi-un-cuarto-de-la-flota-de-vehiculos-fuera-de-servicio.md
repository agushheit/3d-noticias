---
category: Agenda Ciudadana
date: 2021-02-19T07:44:09-03:00
thumbnail: https://assets.3dnoticias.com.ar/sain.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: José Villagrán
resumen: 'Policía de Santa Fe: solo el 57% se encuentra “en la calle” y casi un cuarto
  de la flota de vehículos fuera de servicio'
title: 'Policía de Santa Fe: solo el 57% se encuentra “en la calle” y casi un cuarto
  de la flota de vehículos fuera de servicio'
entradilla: 'Así reza en el informe que el ministro de seguridad Marcelo Saín iba
  a presentar en la comisión de Diputados y salió a la luz en las últimas horas. Crítica
  situación de la Unidad Regional I del departamento La Capital. '

---
El viernes pasado, Marcelo Saín debía presentarse en la comisión de Seguridad de la Cámara de Diputados de la provincia, en su condición como ministro en el área, para brindar explicaciones sobre el plan que tiene el gobierno enfrentar la escalada de violencia en las ciudades de Santa Fe y Rosario y el estado actual de la fuerza policial. 

Sin embargo, el ministro decidió no ir tras la negativa de la comisión de satisfacer los pedidos que él mismo había pedido: que la reunión sea declarada de “carácter público”, pueda asistir todo su equipo de trabajo y la prensa pueda asistir para presenciar el “cara a cara” entre el funcionario y los diputados. 

Así pasó el fin de semana largo de carnaval y en las últimas horas “salió” a la luz el informe sobre seguridad que el responsable máximo tenía pensado brindar. 

**El estado actual de la fuerza policial**

Hoy la policía de la provincia de Santa Fe cuenta con 22.092 efectivos, de los cuales el 57.6% se dedica a tareas “operacionales”; esto es,  servicios de prevención y seguridad, personal “de calle”. Hay, además, un  24,19% del personal policial que se encuentra abocado a funciones “no operables”, dedicándose al soporte y sostenimiento burocrático-administrativo funcional de las dependencias policiales. Y, finalmente, el 10.6% del total de la fuerza están en situación de disponibilidad, con licencias COVID, médicas o a la espera de retiro. 

Esto quiere decir que en total hay casi unos 12.600 efectivo de la fuerza custodiando las calles de las ciudades y comunas de la provincia de Santa Fe. Si tenemos en cuenta el censo poblacional del año 2010 que arroja que la provincia había hasta ese entonces 3.200.000 de personas, estamos hablando que en Santa Fe hay un policía cada 254 habitantes. Teniendo en cuenta que, producto de la pandemia, el año pasado no se pudo realizar el nuevo censo (que arrojaría una cantidad de población aún mayor), el número se elevaría un tanto más.

El informe reconoce que el relevamiento realizado durante el año 2020 había arrojado un alto nivel de policías “en los escritorios” y que la presencia en los barrios, por ejemplo era poca. 

![](https://assets.3dnoticias.com.ar/sain1.PNG)

Del total de policías con los que cuenta la provincia, el 63.8% se encuentra en las 19 unidades regionales que hay en toda la bota santafesina, el 22,3% se ubica en las diferentes direcciones de la estructura de seguridad (como la Dirección General Policía de Acción Táctica, Dirección General Policía Comunitaria y la Dirección General Policía de Seguridad Vial, por citar algunas). El resto del total se distribuye entre la Plana Mayor de Jefatura de Policía de la Provincia de Santa Fe, la Dirección General de Bomberos, Agencia de Control Policial, la Agencia de Investigación Criminal, las Tropas de Operaciones Especiales, las dos unidades de protección de testigos, la Oficina de Gestión Judicial y la Dirección Central Ojo.  

![](https://assets.3dnoticias.com.ar/sain2.PNG)

En general, las Unidades Regionales de la policía dedican el 59% de su personal a tareas operacionales, aunque existen grandes variaciones entre ellas. Por ejemplo, las UR II Rosario, UR VI Constitución, UR VIII General López, UR IX General Obligado, UR X Iriondo, UR XVIII San Martín son las que mayor cantidad de personal dedican a tareas de seguridad y prevención (entre el 64 y el 68%). El personal con tareas no operacionales en esas dependencias se mantiene en un promedio de 25% .

También es importante destacar la situación del personal de la Agencia de Investigación Criminal y las Tropas de Operaciones Especiales. El 8.75% de los efectivos policiales de la Provincia de Santa Fe prestan funciones en AIC, del total el 59,67% se encuentra abocado a tareas operativas de investigación, mientras que 26,37% realiza tareas no operacionales. Por otro lado, el 90 % del personal de las Tropas de Operaciones realiza tareas operativas. 

**La URI La Capital, en estado crítico**

Decíamos que en el informe elaborado por el ministerio de Seguridad, uno de los datos que surgían hacían referencia a la cantidad de oficiales que se encontraban destinados a realizar actividades “no operativas”. Un caso ejemplar de esto se da en el departamento La Capital. 

En enero del 2020, en toda la jurisdicción había más oficiales “no operativos” que oficiales dedicados al patrullaje de calles. Pero esto no era propio solamente de la Unidad Regional I, sino que además sucedía en la UR IV, UR V, UR IX, UR XV, UR XVI, UR XVIII y UR XIX. 

![](https://assets.3dnoticias.com.ar/sain3.PNG)

Hoy, según los datos que iba a presentar el ministro Saín frente a los diputados, esta situación se corrigió en todas las unidades regionales de la provincia de anta Fe. 

Pro no es el único dato que brinda el informe que preocupa en la unidad que se tiene que encargar de cuidar a la ciudad capital y alrededores. Del total de oficiales, la URI La Capital es la unidad que menor proporción de policías dedican a tareas operacionales (46%), siendo un porcentaje muy menor al promedio que ya vimos.

Pero además la situación poblacional de detenidos en las dependencias policiales es caótica. A inicios de febrero de 2021, se indica que existen 884 personas alojadas en comisarías de las distintas localidades, aclarándose que la cantidad máxima permitida es de 674, es decir hay sobrepoblación

En La Capital y alrededores, durante todo el año pasado, se han vivido jornadas de reclamos en comisarías como la 9na de la ciudad de Santa Fe o “la 12” de Santo Tomé, donde se denunciaba la sobrepoblación o subocupación de estos lugares, agudizados por la llegada de la pandemia.

En todo el departamento La Capital hay una capacidad total de 125 internos. Esto quiere decir que juntando todas las comisarías de la jurisdicción se llega a ese total. Sin embargo, la realidad indica que en la actualidad hay 288 presos, un 230% más de lo que la estructura puede alojar. 

  
![](https://assets.3dnoticias.com.ar/sain4.PNG)

**La situación de la flota vehicular de la policía**

En la exposición que debía realizarse el día viernes de la semana pasada, el ministro Saín iba a decir que cuando asumió frente al ministerio de Seguridad se encontró con un casi 30% de la flota de vehículos (camionetas, autos y motos) fuera de servicio. En números concretos, del total de 3.373, solo se encontraban en funcionamiento 2.400.

Es en este punto, donde probablemente, el discurso del funcionario provincial se iba a acentuar más en la “herencia” recibida de la gestión del Frente Progresista. Y como, bajo el comando de su dirección, esos números comenzaron a mejorar, llegando a tener en este febrero del 2021 el 24% de la misma fuera de servicio, siendo este un número todavía alto. 

![](https://assets.3dnoticias.com.ar/sain5.PNG)

En ese sentido, se explica que “teniendo en cuenta el promedio actual de vehículos en servicio de todas las dependencias (75,66%) hubo un incremento de un 4,51% respecto al año anterior”.

En el 2020, el ministerio de Seguridad adquirió 220 camionetas (200 0km tipo pick up 4x2 y 20 camionetas 0km tipo pick up 4x4, ambos modelos equipados para patrullaje). El 70% de estas se destinaron a las 19 Unidades Regionales, el resto entre las siguientes dependencias: Policía de Acción Táctica, Seguridad Vial, Policía Comunitaria, Tropa de Operaciones Especiales, Seguridad Rural y Bomberos. Asimismo, en el año 202y según dicho trabajo, 1 se prevé sumar sumar 460 móviles a la flota de la policía de la Provincia de Santa Fe, entre motovehículos, automóviles y camionetas. Por otra parte se proyecta para el 2022 adicionar un 50% de movilidad y para el 2023 sumar otro 50% más de vehículos para la prevención del delito. 

**Situación del personal según género** 

El 34,64% del personal policial de la Policía de la Provincia de Santa Fe es femenino. De estas 7.653 mujeres, el 44,3% (3.249) realiza tareas operacionales y el 32% tareas no operacionales. Comparativamente, en el caso de los hombres el 63% realiza tareas operacionales, mientras que el 20% tareas no operacionales. 

![](https://assets.3dnoticias.com.ar/sain6.PNG)

El Instituto de Seguridad Pública de la Provincia de Santa Fe duplicó el número de ingresantes, llegando a 1.660 postulantes, de los cuales 920 son mujeres y 740 hombres. Cabe destacar que en el año 2018 no ingresaron mujeres, en 2019 ingresaron 235 y, en 2020, 330. Para los años venideros se proyectan un 25% de aumento en la cantidad de ingresantes. 

![](https://assets.3dnoticias.com.ar/sain7.PNG)