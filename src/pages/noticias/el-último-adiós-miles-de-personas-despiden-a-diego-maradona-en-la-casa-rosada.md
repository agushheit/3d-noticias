---
layout: Noticia con imagen
author: "Fuente: Infobae"
resumen: Maradona en Casa Rosada
category: Agenda Ciudadana
title: "El último adiós: miles de personas despiden a Diego Maradona en la Casa
  Rosada"
entradilla: "Pasadas las 6 de la mañana se abrieron las puertas de la Casa de
  Gobierno. Entre empujones con la Policía, ingresaron pequeños grupos de
  personas; en principio, el acceso se permitirá hasta las 16 horas. "
date: 2020-11-26T13:06:28.109Z
thumbnail: https://assets.3dnoticias.com.ar/chau-diego.jpg
---
Pasadas las seis de la mañana se abrieron las puertas de la Casa Rosada para el público en general. Luego de algunos incidentes y empujones con la Policía, comenzaron a ingresar de a grupos. Una vez dentro, las personas pasan frente al féretro y luego salen a través de la puerta de Balcarce 24.

Lágrimas, llantos descontrolados, cantitos de cancha, algunos hasta envueltos en banderas de Argentina. La cantidad de gente es inmensa para despedir al jugador más grande de la historia del fútbol.

Mayores, niños. Todos quieren despedirlo. Cada tanto se escuchan aplausos, que terminan con un “Olé Olé Olé, Diego Diego!”. Algunos incluso pasan y le arrojan camisetas (de todos los clubes) como ofrenda. Pasadas las 7:30, se amontonaban junto al féretro decenas de “casacas” de la Selección, Boca, Argentinos Juniors, y muchas otras. En tanto, afuera, en la explanada, de a ratos se vuelven a ver empujones y algunas corridas.

La zona de la Plaza de Mayo quedó desbordada desde la madrugada. Anoche a última hora comenzaron a acercarse aquellos fieles para darle el último adiós a Diego Armando Maradona. El Gobierno Nacional se encargó del operativo de seguridad para vallar la Casa Rosada y acondicionar el salón donde desde muy temprano se acercó la familia del astro futbolístico para una ceremonia íntima.

En las primeras horas de la madrugada ya una multitud hacía fila para ingresar al salón donde descansa el ídolo mundial. Poco antes de las 7 de la mañana, la cantidad de gente era incontable. La fila llegaba hasta la Avenida 9 de Julio.

Quienes quieran despedirse en forma presencial de Diego podrán trasladarse a la Casa Rosada caminando desde la avenida 9 de Julio por la Avenida de Mayo, cruzar la Plaza e ingresar a Gobierno por la puerta de Balcarce 50. Por indicación de la familia, el velorio es a cajón cerrado y se extenderá hasta hoy las 16 horas.

![](https://assets.3dnoticias.com.ar/diego1.jpg)

Pasada la medianoche Claudia Villafañe, Dalma y Giannina Maradona llegaron a Casa Rosada para aguardar la llegada del cuerpo del ídolo mundial. Se sumó su otra hija, Jana Maradona, Verónica Ojeda y su hijo Dieguito Fernando para la despedida íntima del entorno del 10. Participaron miembros del equipo campeón de México 86: Oscar Ruggeri, Sergio Batista, Julio “Vasco” Olarticoechea, Jorge Burruchaga, Ricardo Giusti, Carlos Tapia y Oscar Garré; ex jugadores y actuales de Boca Juniors: Carlos Tévez, Rolando Schiavi, Martín Palermo y Ramón Ávila.

De los jugadores a los que dirigió en la Selección Argentina, además de Tévez y Palermo, pasadas las cuatro de la mañana llegó el ex capitán Javier Mascherano.

![](https://assets.3dnoticias.com.ar/diego2.jpg)

Del velorio íntimo también fueron parte Guillermo Coppola y el presidente de la Asociación del Fútbol Argentino, Claudio “Chiqui” Tapia. No obstante, se lo vio a ingresar a Casa Rosada al líder de La 12, Rafael Di Zeo.

A las cuatro de la mañana una escena que tuvo como protagonista a Rocío Oliva revivió una vez más la disputa familiar del entorno del astro. A la última pareja de Maradona se le negó el ingreso a Casa Rosada. “Me dijeron que venga a las siete de la mañana cuando entre toda la gente”, comentó en diálogo con la prensa y apuntó a la familia de Diego: “Toda la maldad que hacen se paga, así les va a ir a todos”.

![](https://assets.3dnoticias.com.ar/diego3.jpg)

Mientras tanto, en las afueras de la casa de Gobierno, una multitud aguardó varias horas. Hinchas de diversos equipos del fútbol argentino, personas con camisetas del Seleccionado Nacional e incluso la barra brava de Boca -”La 12”- fueron parte de la vigilia.

La dificultad que tiene esta ceremonia fúnebre es la exigencia del distanciamiento social, considerando que esperan la llegada de al menos un millón de personas. La gente está realmente muy amontonada.

![](https://assets.3dnoticias.com.ar/diego4.jpg)

Según trascendió, personal de la Casa Rosada está dedicado a pedir la distancia de protocolo en las filas una vez adentro. Afuera, el personal de seguridad está dedicado a esa tarea, aunque con menos exigencia, ya que están al aire libre.

El operativo entre la Policía de la Ciudad y la Federal incluye 1500 efectivos y un vallado desde la 9 de Julio hasta la Casa de Gobierno. Además, se acreditaron 840 periodistas para la cobertura.

![](https://assets.3dnoticias.com.ar/diego5.jpg)

Para los que quieran despedirse por televisión, la transmisión está asegurada en directo y en forma ininterrumpida mientras dure el velorio. La productora La Corte, con su experiencia de más de 30 años en producción y distribución de imágenes, está encargada de la tarea. Son entre 10 y 12 cámaras colocadas para que los televidentes no pierdan detalles de lo que son las exequias de mayor trascendencia desde la Argentina, en el mundo.

![](https://assets.3dnoticias.com.ar/diego6.jpg)

Durante toda la tarde de ayer, se los vio recorriendo la zona donde finalmente se colocó la capilla ardiente y los exteriores de la Rosada, buscando los mejores ángulos para la transmisión. Ya a la una de la mañana de hoy estaban trabajando instalando sus equipos, y preparando el personal para enviar el material en forma gratuita a todas las agencias, canales y medios nacionales e internacionales que lo soliciten.

![](https://assets.3dnoticias.com.ar/diego7.jpg)

Alberto Fernández no se imaginó tener que gobernar en medio de una pandemia global, que lo enfrentó a los más complejos desafíos que puede tener un jefe de estado en cualquier parte del mundo. Pero tampoco pensó que le tocaría despedir a Diego Armando Maradona, el más grande ídolo popular de los argentinos y una figura de fenomenal trascendencia fuera de las fronteras.

![](https://assets.3dnoticias.com.ar/diego8.jpg)

Fuentes oficiales informaron que cerca de las 8 de la mañana estará llegando el jefe de Gabinete, Santiago Cafiero, quien mantendrá un encuentro con la familia de Maradona. Posiblemente el Presidente también pase a darle el pésame.

En medio del shock, apenas unos minutos de haberse enterado de la luctuosa noticia, dispuso que se decretara duelo nacional por tres días y, todavía movilizado por la emoción, le pidió al subsecretario General de la Presidencia, Miguel Ángel Cuberos, que se comunicara con Claudia Villafañe para ofrecerle el respaldo del Gobierno para lo que necesitara.

![](https://assets.3dnoticias.com.ar/diego9.gif)

Cuberos fue quien se encargó de recibirlo el 26 de diciembre de 2019, cuando el Diego volvió a Casa Rosada después de muchos años. En la explanada de avenida Rivadavia lo esperó ese día a las 12.30, preocupado porque llegara tarde o desaliñado para una reunión que sería formal. Pero Maradona estuvo puntual, con bermudas pero con camisa y saco. Aunque sin corbata. Allí se confundieron en un abrazo y Cuberos lo sostuvo de un brazo para ayudarlo a cruzar el Salón de los Bustos y subirse al ascensor que los llevaría al primer piso, donde está el despacho del Presidente. Ya caminaba con gran dificultad.

![](https://assets.3dnoticias.com.ar/diego10.jpg)

Cuando se encontró con Fernández, ambos se emocionaron hasta el llanto. Quedaron un rato abrazados y les costó recuperarse. Los testigos aún recuerdan la escena igualmente conmovidos. Muchos funcionarios tuvieron la chance de compartir el encuentro y sacarse fotos con él, desde el jefe de Gabinete, Santiago Cafiero, hasta el ministro de Economía, Martín Guzmán, quien tuvo tiempo de mandar a comprar una camiseta con el número 10 y llevarla a Casa Rosada para que su ídolo la autografíe.

Después, el Presidente le preguntó si quería salir a saludar a la gente al balcón de Perón, porque porque ya había ido trascendido que estaba en Gobierno y querían verlo. “Obvio”, contestó rápido. Como gran pompa, fue acompañado por personal de seguridad desde el despacho presidencial hasta el Salón Eva Perón y el Salón de los Científicos, que son las dos salas que dan a los balcones que usó Juan Domingo Perón, y salió a saludar y cantar “para Macri que lo mira por TV”, mientras hacía la “V” de la victoria.

Eran días de felicidad. Después vino la pandemia, el notable desmejoramiento de Maradona y, ayer al mediodía, la muerte.

![](https://assets.3dnoticias.com.ar/diego11.jpg)

[](<>)Con dolor, funcionarios de casi todos los ministerios se pusieron a disposición de un operativo que nadie hubiera querido. El Ministerio de Defensa y el de Seguridad, organizando los accesos.

El Ministerio de Salud, los dispositivos sanitarios. El Ministerio del Interior, proveyendo la información necesaria de los invitados especiales. La Dirección de Ceremonial, preparando con la familia los ingresos. La Casa Militar, reforzando la seguridad de los espacios presidenciales la Casa Rosada, muchos de los cuales directamente fueron refuncionalizados, como el caso del despacho de la vicejefa Cecilia Todesca, que quedó para uso de la familia Maradona durante el tiempo que dure el velorio.

Si bien Presidencia informó que el velorio durará hasta las 16 de este jueves, anoche se especuló con la posibilidad de que dure más días. Al personal de prensa le dijeron que “vengan mañana a las 7 (por hoy) y prepárense, que tal vez no puedan volver a sus casas por tres días consecutivos”.

Algunos tienen la experiencia de lo que fue el velorio de Néstor Kirchner, en octubre del 2010, aunque no es el caso del Presidente y su equipo, que ya no formaban parte de la gestión de Cristina Fernández de Kirchner.