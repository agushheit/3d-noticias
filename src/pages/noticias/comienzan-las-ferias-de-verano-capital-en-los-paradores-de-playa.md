---
category: Agenda Ciudadana
date: 2021-01-22T03:52:49Z
thumbnail: https://assets.3dnoticias.com.ar/feria.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad de Santa Fe
resumen: Comienzan las ferias de Verano Capital en los paradores de playa
title: Comienzan las ferias de Verano Capital en los paradores de playa
entradilla: 'Este viernes se dará inicio a un ciclo de ferias de emprendimientos locales
  en los distintos paradores de playa ubicados sobre las costaneras Este y Oeste. '

---
El viernes, desde las 17.30 horas, se llevará a cabo una feria a cielo abierto en el Parador Santa Fe de la Costanera Este. La actividad, que es impulsada por Capital Activa de la Municipalidad, congrega a emprendimientos locales que promocionarán sus productos de temporada entre el público presente. Además, se dispondrá de un punto de ecocanje para dejar residuos reciclables a cambio de semillas y plantines para armar una huerta en casa. El acceso será libre y gratuito.

Al respecto, el secretario de Producción y Desarrollo Económico, Matías Schmüth, destacó que “este viernes comenzamos con un ciclo de ferias que se desarrollarán en los distintos paradores de playa a lo largo de la temporada de verano. Es una propuesta que se suma a la agenda de Verano Capital para que la puedan disfrutar los santafesinos y turistas que visiten la ciudad y para que, al mismo tiempo, sea una oportunidad de venta para los emprendedores locales que participan de cada fecha”.

La propuesta invita a conocer el espíritu emprendedor existente en la ciudad y a apoyar iniciativas locales que sean innovadoras y sustentables y que desarrollen procesos productivos con características de “triple impacto”, es decir, que pretendan generar algún tipo de impacto social, económico o ambiental en la ciudad de Santa Fe.

En este sentido, Schmüth dijo que en Santa Fe los emprendedores tienen mucho para mostrar y, en esta oportunidad, desde la Municipalidad se busca orientar las ferias a propuestas vinculadas al verano y al desarrollo de los paradores de playa. Es por esto que se van a ir realizando en los distintos paradores ubicados sobre ambas costaneras, de forma tal que los emprendedores locales cuenten con un espacio para mostrar sus productos, y potenciar la capacidad que tenemos localmente.

El emprendedurismo “es un sector muy fuerte y eso es importante mostrarlo, darle un lugar y sumarlo al operativo Verano Capital en el que también proponemos actividades turísticas, deportivas y culturales para vivir la temporada en la ciudad”, sostuvo.

**Acompañamiento**

La Municipalidad acompaña al ecosistema emprendedor con distintas estrategias y herramientas laborales desde el comienzo de la gestión del intendente Emilio Jatón. En el 2020 se brindaron talleres y capacitaciones virtuales para emprendedores que tuvieron una adhesión de más de 700 asistentes.

Consultado sobre el trabajo con el sector, el funcionario indicó que “el municipio tiene como objetivo potenciarlo porque es una verdadera pata de la economía local. En este sentido y, teniendo en cuenta la situación difícil que nos tocó atravesar a todos los santafesinos por la pandemia, desde el comienzo de gestión tratamos de acompañarlos y de ofrecer herramientas, capacitaciones, programas de fortalecimiento y las ferias de Capital Activa que se desarrollaron durante todo este tiempo”.

  
**Próximas fechas**

La programación continuará el viernes 29 de enero en el Parador Laguna Beach de la Costanera Oeste, el 5 de febrero en la zona del Puente Colgante sobre Costanera Oeste, el 19 de febrero en Parador Sunset Coast de la Costanera Este y el 5 de Marzo en el Parador Santa Kite del Espigón II de la Costanera Oeste.

Es importante aclarar que en caso de lluvia las fechas serán reprogramadas y que, debido a la situación epidemiológica actual, se respetará el protocolo sanitario correspondiente para este tipo de actividad y se tomarán los recaudos para el cuidado de todos los asistentes.