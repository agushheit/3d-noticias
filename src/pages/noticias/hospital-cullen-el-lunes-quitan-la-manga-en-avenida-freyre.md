---
category: La Ciudad
date: 2021-09-25T06:15:01-03:00
thumbnail: https://assets.3dnoticias.com.ar/MANGACULLEN.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Hospital Cullen: el lunes quitan la manga en Avenida Freyre'
title: 'Hospital Cullen: el lunes quitan la manga en Avenida Freyre'
entradilla: Lo confirmó el director del Hospital Cullen, Juan Pablo Poletti. Estiman
  que si no hay problemas con el operativo, a la noche se podría habilitar el tránsito

---
Finalmente, el director del Hospital Cullen le puso día al operativo para sacar la manga instalada desde abril pasado sobre Avenida Freyre y que se usaba para conectar el edificio del nosocomio con el hospital militar instalado enfrente, en la sede del Liceo Militar General Manuel Belgrano.

"Tras las reuniones que estamos manteniendo con el Ministerio de Salud, el municipio, en conjunto con los integrantes de la comisión del hospital, articulamos la organización para ir trasladando de manera paulatina a los pacientes de la carpa militar al Cullen y en lo posible terminar esto el lunes a primera hora y de esta manera al mediodía se podría comenzar con el desarme de la manga para que a la noche, y de acuerdo al operativo que realizará la Municipalidad, a la tardecita noche, poder habilitar la circulación en esa arteria", confirmó Poletti.

Cabe recordar que la manga se instaló a finales del mes de abril para conectar el nosocomio con el Hospital Militar que se había levantado en las instalaciones del Liceo Militar, que prestó el gobierno nacional, ante la necesidad de poder ampliar las camas de internación para dar respuesta a la demanda que se había producido por la segunda ola del coronavirus. Pasado un tiempo y ante la baja de casos, los comerciantes de la zona comenzaron a solicitar levantar la estructura que impide la circulación por la mencionada avenida, ya que sus ventas seguían en baja.

Luego de reuniones y de establecer un operativo necesario para llevar esto a cabo, el lunes se habilitaría nuevamente el tránsito en el sector, retirando la manga, pero la estructura del hospital militar seguiría en pie, en prevención de una eventual suba de casos o demanda de atención en las próximos semanas.