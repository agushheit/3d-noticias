---
category: La Ciudad
date: 2021-08-15T06:00:33-03:00
thumbnail: https://assets.3dnoticias.com.ar/finde.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Fin de semana largo con actividades para disfrutar en la capital santafesina
title: Fin de semana largo con actividades para disfrutar en la capital santafesina
entradilla: Los espacios culturales de la ciudad ofrecerán una programación de música
  en vivo y espectáculos para compartir en familia como parte del Mes de las Infancias.
  La entrada es gratuita, con aforos limitados por protocolo.

---
La Municipalidad preparó una intensa agenda cultural para este fin de semana largo, con distintas propuestas en la Estación Belgrano, en el Mercado Progreso y en el Anfiteatro “Juan de Garay”; y también en la Peatonal San Martín y la avenida Aristóbulo del Valle.

Con entrada gratuita se podrá disfrutar de conciertos y espectáculos para toda la familia, en paralelo con las funciones del Festival de Títeres Trotamundos que se desarrollan en el Teatro Municipal y en salas independientes, todo como parte del Mes de las Infancias.

**La canción como territorio**

El -viernes 13 de agosto-, desde las 19 horas, se presentará el cantante, bajista y guitarrista Franco “Palo” Ruiz, en Sala 5/360, el ciclo de música en vivo con sonido cuadrafónico que se desarrolla en la sala ubicada en la planta alta de la Estación Belgrano. La música de este artista oriundo de Frías, Santiago del Estero, y radicado en la ciudad de Santa Fe desde el año 2015 construye rutas, cruces y aperturas entre la raíz folklórica y la urbana, haciendo de la canción un territorio propio, personal.

En esta ocasión, interpretará “El camino”, un recorrido autobiográfico por paisajes en movimiento, un mapa poético y musical que invita al espectador al encuentro consigo mismo. Lo acompañarán Lisandro Herbel en piano y sintetizador, Alejandro Ferrero en percusión y Matías Aguiar en bandoneón.

La próxima fecha de Sala 5/360 será el viernes 20, con Nilda Godoy & Cacho Hussein y el 27, con Seba Barrionuevo. Las entradas son gratuitas. Se retiran hasta dos por persona el mismo día del concierto, desde las 17 horas, en el ingreso por Boulevard Gálvez 1150.

**Música y títeres en el Mercado**

El Mercado Progreso (Balcarce 1625) propone tres encuentros en torno a la música y propuestas para toda la familia el domingo a la tarde, en el marco del Festival de Títeres Trotamundos. La entrada es gratuita y el ingreso del público se permitirá por orden de llegada, hasta completar el 30% de la capacidad del espacio, habilitada por protocolo.

El viernes 13, desde las 18 horas, se presentan el músico independiente Nico Gómez De Prana y la banda de hip hop-rock Maca Revolt and The Sound Sisters.

La discografía de Nico Gómez De Prana (nombre artístico de Miguel Ángel Olivera) incluye “Hogar, dulce sonido” (2010), “Música en mi cuero” (2013), “Conga Santificada” (2015) y “Vengo” (2020). Con una trayectoria de más de 10 años como solista, ha llevado su sonido negro litoraleño por distintos escenarios locales y a distintos puntos del país.

DJ Shuka (Josefina Schmuck), Marti Muchiutti en flauta, Aldana y Fiorella Azetti de Destellos Dance Studio en el baile integran la formación de Maca Revolt and The Sound Sisters. A través de instrumentales de Hip- Hop Rock, creadas por Backing Band y Wolf esta crew de mujeres y disidencias realizan un show cargado de rimas y baile.

La propuesta para el sábado 14, también desde las 18, reunirá al dúo instrumental ZZ99 de Pablo Jaimet y Luli Molinas; y a Badú Pop, con “Gustavo Cerati Tributo a la Carta”. Loli Godoy y Ega Lostia realizarán un homenaje hecho canción, interpretando en dos guitarras, dos voces, loops y secuencias recorriendo la trayectoria musical de quien fuera líder de Soda Stereo.

El domingo 15, a partir de las 16 horas, se podrá disfrutar de una función del Festival de Títeres Trotamundos. El grupo Hasta las Manos interpretará “Pantomimas”, un espectáculo de títeres de guantes que reúne dos piezas breves de teatro de títeres mímico, escritas por el maestro Gabriel “Guaira” Castilla.

**Folklore y una Tarde para las Infancias**

El sábado 14 de agosto, desde las 16 horas, el Anfiteatro del Parque Sur (Av. Illia y San Jerónimo) abre sus puertas a una Noche de folklore, con baile y música en vivo. La grilla comienza con el Ensamble de Bombos que integran músicas y bailarinas que participan del Colectivo Folklore Feminista Santa Fe. Mediante la percusión, el canto y la danza, tocando y bailando coplas, chacareras y huaynos, reivindican la raíz ancestral, poniendo en evidencia la fuerza de la grupalidad y haciendo visible su militancia desde el arte.

El Rejunte Chango también será parte de este encuentro, después de editar su primer álbum “Las cosas de la vida”, en 2020, y con el objetivo que marca el rumbo a este proyecto: encontrarle un sonido nuevo a las danzas folklóricas tradicionales argentinas. La grilla de la noche finalizará con el conjunto vocal Grupo Identidad.

En el Mes de las Infancias, se podrá disfrutar el domingo a partir de las 15, de Pan y Circo y el Dúo Karma de los músicos cubanos Xóchitl Galán y Fito Hernández, con su concierto para todas las edades “Abre la ventana”. Una guitarra, dos voces, kalimba, caxixis y ritmos corporales son