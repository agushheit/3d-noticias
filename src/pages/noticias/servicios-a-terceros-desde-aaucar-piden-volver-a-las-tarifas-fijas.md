---
layout: Noticia con video
author: .
resumen: AAUCAR reclama tarifas planas
category: La Ciudad
title: "Servicios a terceros: desde AAUCAR piden volver a las tarifas fijas"
entradilla: La Asociación Autotransporte de Carga de Santa Fe expresó su
  preocupación ante el cambio de modalidad de cálculo de las horas a emplear por
  los servicios a terceros que brinda la Municipalidad de Santa Fe.
date: 2020-11-11T13:32:45.126Z
thumbnail: https://assets.3dnoticias.com.ar/aaucar.jpg
url: https://youtu.be/uvF0WNWuQCE
---
Se trata del servicio de supervisión y ordenamiento de tránsito que brindan los inspectores municipales ante los trabajos que debe realizar cualquier vecino que implique la utilización de camiones y/o grúas.

"Anteriormente teníamos una tarifa fija según el vehículo, y por hora, lo que nos resultaba más fácil trasladar el costo del servicio a nuestros clientes. Hoy la situación es completamente diferente. Desde el inicio de la gestión modificaron el modo de cálculo" sostuvo Esteban Poccia, presidente de AAUCAR. "Hoy la hora de trabajo depende de la antigüedad del inspector asignado al brindar el servicio, por lo que no tenemos claridad sobre los costos en forma anticipada y se nos complica trasladar el monto final a nuestros clientes".

El tarifario que regía con anterioridad estipulaba un monto fijo, acorde con el tipo de vehículo que se utilizaba y la cantidad de horas trabajadas. "Queremos volver a contar con una tarifa plana, que nos permita conocer de antemano los costos de servicio y poder trasladar la tarifa final a nuestros clientes" culminó Poccia.