---
category: Estado Real
date: 2021-11-18T06:00:52-03:00
thumbnail: https://assets.3dnoticias.com.ar/polonia.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Perotti recibió a la embajadora de Polonia en Argentina, Alejandra Maria
  Piatrowka
title: Perotti recibió a la embajadora de Polonia en Argentina, Alejandra Maria Piatrowka
entradilla: '"Nos interesa generar un vínculo con uno de los países que tiene una
  potencialidad importante con nuestra provincia y una vocación de acercarse", sostuvo
  el gobernador de la provincia, luego del encuentro.'

---
El gobernador Omar Perotti recibió este miércoles a la embajadora de Polonia en Argentina, Aleksandra Maria Piatkowska, en el Salón Protocolar de Casa de Gobierno, en la ciudad de Santa Fe.

En la oportunidad, Perotti afirmó que "es una verdadera satisfacción y un orgullo tener a la embajadora de Polonia dedicándonos dos días importantes de su agenda. Una provincia como la nuestra, con un perfil exportador que tiene más del 20% de las exportaciones argentinas, es una provincia abierta al intercambio y es deseosa de radicaciones de industrias en el territorio, pero también de establecer vínculos con las distintas partes del mundo", expuso el gobernador.

"Tenemos en nuestra historia inmigrantes polacos que han nutrido la provincia de Santa Fe, particularmente en la ciudad capital y Rosario, caracterizados por su fuerte vocación de trabajo", agregó Perotti y puntualizó que "nos interesa generar un vínculo con uno de los países que tiene una potencialidad importante con nuestra provincia y una vocación de acercarse. Vemos un potencial creciente en esa relación".

"Fruto de la jornada del trabajo de hoy, con el área de Cooperación Internacional y de Comercio Exterior vamos a organizar seminarios de conocimiento de Polonia y de Santa Fe, para que los actores interesados y quienes participan de la vida del comercio internacional puedan saber más de esas opciones", concluyó el gobernador.

Por su parte, Piatkowska agradeció al gobernador y sostuvo que “es un gran honor estar acá. Tenemos una agenda muy apretada, empezando con el gobernador, después con secretarios de Cultura y Turismo y mañana reuniones con industriales y cooperación económica. Todos los puntos de trabajo que podemos imaginarnos de trabajo en el área económica, cultural y turismo, están incluidos en esta visita”.

Luego del encuentro con el gobernador, la embajadora sostuvo que “nos presentamos de forma muy detallada viendo las cosas que se pueden hacer juntos, dónde hay puntos comunes, dónde hay necesidades de provincia y de Polonia y cómo hacer negocios. En la provincia de Santa Fe se está haciendo un trabajo muy importante para tener un intercambio, no solamente político o diplomático, sino de tener proyectos comunes”.

Participaron también de la actividad, el cónsul Honorario de Polonia en Rosario, Bartolome Estanislao Moszoro; la asistente del Cónsul Honorario de Polonia en Rosario, María Irene Moszoro; y la secretaria de Cooperación Internacional de la provincia, Julieta de San Félix.

**AGENDA DE TRABAJO**

Durante la visita de la embajadora Piatkowska a la provincia, se reunió con el secretario de Turismo de la Provincia de Santa Fe, Alejandro Grandinetti, y el secretario de Industrias Culturales, Felix Fiore.

En la ciudad de Santa Fe visitará el Parque Tecnológico Litoral Centro (PTLC), Zoovet, la Aceleradora del Litoral, el Parque Industrial de Sauce Viejo y PB Leiner Argentina. En tanto, en Rosario visitará la empresa Bioceres.