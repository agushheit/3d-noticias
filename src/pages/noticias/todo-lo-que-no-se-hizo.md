---
layout: Noticia con imagen
author: Por Sofía Gioja
resumen: "Opinión: Sofía Gioja"
category: Agenda Ciudadana
title: Todo lo que no se hizo. Santa Fe entre las áreas con mayor cantidad de
  femicidios en lo que va del año 2020
entradilla: Si sos victima de violencia de género llamá a la línea gratuita 0800
  777 5000. Atienden todos los días del año a cualquier hora. También podés
  solicitar asesoramiento o ayuda a la línea nacional 144.
date: 2020-11-07T15:10:00.000Z
thumbnail: https://assets.3dnoticias.com.ar/femicidios1.jpg
---
El, ya vetusto*, Modelo de Protocolo Latinoamericano de investigación de las muertes violentas de mujeres por razones de género* (femicidio o feminicidio), detalla que, en un estudio mundial sobre homicidios del 2011, la Oficina de las Naciones Unidas contra la droga y el Delito (UNODC), señaló que las muertes violentas de mujeres, eran principalmente causadas por sus parejas íntimas o en el marco de sus relaciones familiares, y que las mujeres tenían más probabilidades de morir dentro de sus hogares que fuera de este.

Ya en ese entonces se sabía, a grandes rasgos, cómo prevenir un femicidio, o en todo caso, hacia donde apuntar para esclarecer el asesinato de una mujer. Se sabía que el agresor pertenecía al círculo íntimo o era conocido. Había denuncias (a veces muchas). Había signos, había evidencia, había pedidos de ayuda. Lo que no había: perimetrales, tobilleras, atención integral en violencias de género, empatía y, mucho menos, sororidad.

[Artículo Completo >](https://3dnoticias.com.ar/opinion)