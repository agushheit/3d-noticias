---
category: Deportes
date: 2021-03-31T07:31:54-03:00
thumbnail: https://assets.3dnoticias.com.ar/liga-barrial.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad de Santa Fe
resumen: La Municipalidad aportará fondos a la Liga Infantil de los Barrios
title: La Municipalidad aportará fondos a la Liga Infantil de los Barrios
entradilla: Se trata de 24 clubes que reúnen de 4.000 niños en torno al fútbol, talleres
  y actividades culturales.

---
Este martes, en la cancha de Los Crack de Guadalupe, el intendente Emilio Jatón firmó un convenio con el presidente de la Asociación Civil Deportiva y Cultural de los Barrios, Guiliano Carnaghi. El acuerdo establece que el municipio otorgará a la Liga Infantil de los Barrios, un aporte no reintegrable total de $ 750.000.

El monto se utilizará para afrontar los gastos de movilidad de equipos de fútbol infantil que participan en el torneo barrial. Se trata de un certamen que cada fin de semana disputan 24 clubes de la capital provincial, Santo Tomé y Sauce Viejo, y que reúnen a más de 4.000 niños y niñas.

Una vez rubricado el acuerdo, Jatón afirmó que “para nosotros es una inmensa alegría porque sabemos lo que significa la Liga Infantil de los Barrios; y lo que hace el convenio es darles un sustento económico para todo el año”.

El intendente recordó que la entrega del subsidio fue aprobada por el Concejo Municipal pero hasta el momento no se había materializado. “Ahora queremos que sea política de Estado y que ellos, todos los años, tengan los fondos suficientes para el traslado de los chicos a los lugares de juego”, dijo.

“Para nosotros, la Liga infantil de los Barrios es ciudadanía, es llegar a las familias. Es muy importante el trabajo social que hacen y por eso significa un reconocimiento que les permitirá solventar, mes a mes, los traslados de los chicos para que se lleven a cabo los partidos en los distintos lugares”, insistió.

En la oportunidad, estuvieron presentes representantes de los 24 clubes que integran la Liga Infantil de los Barrios, lo que para Jatón hizo al acto “muy emotivo, ya que es difícil dimensionar el trabajo social inmenso que hace cada institución. Sólo basta ir a un partido para darse cuenta de lo que significa: los chicos y las chicas jugando, las familias alrededor de la cancha y la creación de ciudadanía que simboliza”, concluyó.

También rubricaron el acuerdo, los secretarios de Gobierno, Nicolás Aimar; y de Integración y Economía Social, Mariano Granato, por el municipio. Mientras que por la Asociación, lo hicieron la secretaria Gloria Aquino, y la tesorera Graciela Miranda. Además, estuvieron presentes el presidente del Concejo Municipal, Leandro González; y la concejala Laura Mondino.

![](https://assets.3dnoticias.com.ar/club.jpg)

**Un club en cada barrio**

Guiliano Carnaghi afirmó que para la Liga “es muy importante este apoyo, porque es una lucha de muchos años. No pudimos lograrlo con la gestión municipal anterior, porque hubo muchos impedimentos para que se reconozca el marco normativo de la Liga Infantil de los Barrios, pero ahora estamos avanzando”.

Según explicó, la agrupación que preside reúne a “24 clubes que están en barrios populares y periféricos de la ciudad, los cuales no cuentan con recursos y se autogestionan”. En ese sentido recordó que el movimiento surgió “en el 2013 como una respuesta a que hay muchos barrios en la ciudad que no cuentan con un club, entonces la Liga viene a tratar de resolver esa deficiencia”.

“Nuestro lema es que no quede ningún barrio sin club, por eso es tan importante el trabajo que realizan para niños y niñas en condiciones de vulnerabilidad”.

Además, acotó que “la agrupación no es sólo de fútbol y deporte sino que también hay comedores comunitarios, copa de leche, talleres de oficios y actividades culturales. Es una red de organizaciones comunitarias que tratan de hacer de su un barrio un lugar más justo”.

![](https://assets.3dnoticias.com.ar/club1.jpg)

![](https://assets.3dnoticias.com.ar/club2.jpg)