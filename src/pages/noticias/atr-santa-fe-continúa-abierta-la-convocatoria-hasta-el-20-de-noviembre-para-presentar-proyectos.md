---
layout: Noticia con imagen
author: .
resumen: ATR Santa Fe
category: Estado Real
title: "ATR Santa Fe: Continúa abierta la convocatoria hasta el 20 de noviembre
  para presentar proyectos"
entradilla: El Gobierno de Santa Fe convoca a los y las jóvenes de la provincia
  a participar de ATR Proyectos Participativos. Se financiarán proyectos
  destinados a impactar positivamente en las comunidades con hasta 50 mil pesos.
date: 2020-11-15T14:43:32.342Z
thumbnail: https://assets.3dnoticias.com.ar/atr.jpg
---
El Gobierno de Santa Fe, a través de la Secretaría de Estado de Igualdad y Género, convoca a participar del programa ATR Proyectos participativos (Por más Autonomía, Territorialidad y Redes Jóvenes), iniciativa busca impulsar las ideas de los y las jóvenes de toda la provincia de Santa Fe a través de la financiación de hasta $ 50.000 de proyectos colectivos que persiguen como finalidad el impacto positivo en sus comunidades.

Hasta el próximo 20 de noviembre, quienes tengan entre 15 y 29 años podrán presentar sus proyectos en grupos no menores a cinco integrantes, y debe haber al menos una persona mayor de edad en el grupo.

Serán priorizadas las propuestas relacionadas con conectividad y nuevas tecnologías, ambiente, acciones que den respuestas a problemas económicos, sociales, educativas, culturales y de promoción de derechos surgidas en el contexto de la pandemia por Covid-19. En todos los casos se deberá prever que el proyecto se encuadre dentro de las actividades contempladas como “permitidas”.

El objetivo general del programa es potenciar a las juventudes como protagonistas activas de sus comunidades, para así poder garantizar la transversalidad y la coordinación territorial de las políticas específicas, como herramienta para incrementar la aproximación a las necesidades y los retos de la población joven de cada área geográfica de Santa Fe. A la vez, se busca potenciar su participación en la transformación social y en la conducción de los asuntos públicos, promover sus capacidades creadoras y emprendedoras, tanto en el medio urbano como rural, para fomentar el arraigo y el desarrollo humano.

Para sumar tu proyecto podés ingresa acá: [www.santafe.gov.ar](https://www.santafe.gov.ar/formularios/index.php/599634)

**Bases y condiciones:** [www.santafe.gob.ar](https://www.santafe.gob.ar/index.php/web/content/download/259934/1367779/)

**Contacto:** atrsantafe@santafe.gob.ar

**Instagram:** [ATRJuventudesSF](https://www.instagram.com/ATRJuventudesSF) 

**¿Qué es ATR?**

A través de ATR Juventudes Santa Fe, la Secretaría de Estado de Igualdad y Género se encarga de generar políticas públicas transversales orientadas a los y las jóvenes de la provincia de Santa Fe, haciendo foco en la articulación interministerial, con la importancia de priorizar que las juventudes encuentren, con el acompañamiento del Estado, las herramientas para desarrollar sus proyectos de vida, expresar sus ideas y construir sus sueños.

En este sentido, las políticas públicas están basadas en cuatro líneas fundamentales: Autonomía, Transformación, Desarrollo y Territorialidad, buscando fortalecer la participación de los y las jóvenes en todo el proceso.

Desde un enfoque transversal e integral, se trabaja en pos de darle perspectiva joven a las políticas que piensen las distintas áreas de gobierno.