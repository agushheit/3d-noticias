---
category: Agenda Ciudadana
date: 2021-07-17T06:00:37-03:00
thumbnail: https://assets.3dnoticias.com.ar/anses.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: El 2 de agosto comienza el pago del bono de $5.000 para jubilados y pensionados
title: El 2 de agosto comienza el pago del bono de $5.000 para jubilados y pensionados
entradilla: El beneficio anunciado esta semana por el presidente Alberto Fernández,
  alcanzará a más de 6 millones de personas, un 83% de los beneficiarios previsionales.

---
El bono de $ 5.000 para jubilados y pensionados comenzará a pagarse el lunes 2 de agosto según terminación de DNI y con los cronogramas de pago habituales de la Administración Nacional de la Seguridad Social (Anses), informó el organismo previsional.

El bono anunciado esta semana por el presidente Alberto Fernández, y la directora ejecutiva de la Anses, Fernanda Raverta, alcanzará a más de 6 millones de personas, es decir, al 83% de los beneficiarios previsionales.

Están incluidos en el beneficio jubilados, jubiladas, pensionados y pensionadas con hasta dos haberes mínimos, titulares de la Pensión Universal para el Adulto Mayor (PUAM), Pensiones No Contributivas (PNC) por Vejez e Invalidez y Madres de 7 hijos.

La Anses, mediante un comunicado, destacó que "este bono, financiado con recursos del Tesoro Nacional, no sufrirá ningún descuento ni será computable para ningún otro concepto".

Para aquellas personas que tengan ingresos hasta dos haberes mínimos ($46.129,40 ), la suma será de $5000; para las que perciben entre $46.129,41 y $51.129,39, el monto será el equivalente hasta alcanzar este último monto.

"Es un bono que les permitirá tener la certeza de una jubilación digna", afirmó la titular de la Anses tras el anuncio de esta semana, quien destacó el trabajo y el compromiso del Gobierno de "recuperar el valor de compra de las jubilaciones que en dos años del gobierno anterior perdieron 20 puntos".

Este bono de $5.000 será el tercero que se otorga en 2021 a las jubilaciones mínimas -los anteriores fueron de $1.500 en abril y mayo- aunque el primero que incluirá también a los haberes que duplican el haber más bajo.