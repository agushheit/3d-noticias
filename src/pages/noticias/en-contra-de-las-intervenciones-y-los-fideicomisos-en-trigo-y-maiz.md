---
category: El Campo
date: 2022-02-18T11:29:17-03:00
thumbnail: https://assets.3dnoticias.com.ar/campo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa SRA
resumen: En contra de las intervenciones y los fideicomisos en trigo y maíz
title: En contra de las intervenciones y los fideicomisos en trigo y maíz
entradilla: La Mesa de Enlace emitió hoy un comunicado de prensa antes las versiones
  que circulan sobre la puesta en marcha de fideicomisos para trigo y maíz.

---
Ante las versiones que circulan desde hace varios días sobre la puesta en marcha de fideicomisos para el trigo y maíz, desde la Comisión de Enlace de Entidades Agropecuarias (CEEA) queremos dejar bien en claro que estos instrumentos no serían necesarios si no hubieran permanentes intervenciones y fijaciones de precio por parte del Gobierno, si se pudiera trabajar libremente en la oferta y demanda, si se accediera a los precios que existen en la realidad. En el trigo, ya los productores agropecuarios argentinos recibimos los valores más bajos del mundo.

Además, vale aclarar que no hubo instancias de explicación sobre cómo funcionaría ninguno de los dos fideicomisos ni qué monto es el que se busca recaudar, o cuántas toneladas se pretende subsidiar para los molinos –en el caso del trigo– y para los productores de carne bovina, porcina y de pollo –en cuanto al maíz.

Por otra parte, un dato a remarcar sería que la incidencia de esta medida en el precio al mostrador por parte del trigo y el maíz, sería mínima.

Entonces, este intento de política no resolvería el problema, ya que lo que incide en los aumentos de precios, es la suba de los valores de los demás componentes del mismo como la logística, la distribución, el procesamiento, y la comercialización, por efecto de la inflación misma. Y también, y muy importante, es el componente impositivo.

Esto sumado al recientemente renovado fideicomiso aceitero y a las versiones acerca de que se podría cerrar o regular las exportaciones de leche, no hacen más que generar incertidumbre, complicar el escenario sobre el que trabajamos a diario, y no dar ninguna respuesta a ninguno de los problemas que dicen querer resolver.

Creemos que esta propuesta de fideicomiso sería una retención encubierta, una alternativa que quieren imponer desde el Gobierno al aumento de los derechos de exportación que quedó descartado tras la no aprobación del Presupuesto 2022.

Además, entendemos que sobre el precio del cereal se van a descontar los costos de comercialización, el flete, la retención y lo necesario para alimentar el fideicomiso. Al respecto, sabemos por experiencias fallidas pasadas, que (aunque digan que lo va a nutrir la exportación) esto lo terminará solventando, como siempre, el productor.

Consideramos que, de prosperar esta medida, se trataría de otro mordiscón fiscal al productor, otro impuestazo para intentar sujetar el andamiaje que se cae a pedazos de intervenciones y fijaciones de precios unilaterales.

El campo ya no acepta más nada de esto, por lo que le pedimos a los funcionarios que recapaciten sobre sus propuestas y posibles acciones