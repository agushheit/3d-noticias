---
category: Agenda Ciudadana
date: 2021-01-21T08:51:17Z
thumbnail: https://assets.3dnoticias.com.ar/comercio.png
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Lt10
resumen: Las provincias del Litoral lideraron la remontada en la actividad comercial
title: Las provincias del Litoral lideraron la remontada en la actividad comercial
entradilla: Al igual que en septiembre, Formosa, Corrientes, Chaco, Misiones, Santa
  Fe y Entre Ríos volvieron a ser las regiones de mejor desempeño, con crecimiento
  interanual.

---
Las provincias del Litoral argentino lideraron la remontada de la actividad comercial sobre el tramo final de 2020, tras el piso tocado en abril con una cuarentena estricta debido al impacto de la pandemia de Covid-19.

Así se desprende del Informe de Panorama Productivo correspondiente a diciembre, realizado por el Ministerio de Desarrollo Productivo y que muestra la evolución de los principales indicadores de la actividad.

En la actividad comercial se observa una "marcada heterogeneidad" provincial y municipal, dice el informe e indica que al menos hasta octubre se observó una performance similar a la de septiembre, con la mediana del comercio cayendo en torno al 5% real en el promedio del país.

Al igual que en septiembre, Formosa, Corrientes, Chaco, Misiones, Santa Fe y Entre Ríos volvieron a ser las regiones de mejor desempeño, con crecimiento interanual.

"Formosa y Misiones nuevamente lideraron las subas con alzas del 10% interanual en ambos casos", indicó el análisis elaborado por la cartera que conduce Matías Kulfas.

"Las razones de este llamativo dinamismo tienen que ver con el elevado perfil agroindustrial, la baja incidencia de casos de Covid-19 y que estas provincias tienen los principales pasos fronterizos con Paraguay, país al cual en la prepandemia eran frecuentes los tours de compras por parte de la población argentina limítrofe", explicó Desarrollo Productivo.

No obstante, hay casos en los que el Estado provincial jugó un rol clave con el financiamiento: el Banco de Formosa, por ejemplo, financió a pymes, monotributistas y autónomos por más de 1.479 millones de pesos entre abril y diciembre de 2020.

Según datos del Gobierno formoseño, hubo financiamiento a tasa cero o tasa subsidiada a 2.640 pymes, monotributistas y autónomos por más de 491 millones de pesos desde abril y en julio se sumaron líneas promocionales para 244 empresas por 989 millones de pesos.

En la performance del comercio a nivel nacional, en el extremo de más lenta recuperación están Neuquén, que en octubre había registrado una caída del 17,8%; San Luis (-14,1%), Tierra del Fuego (-13,6%), Tucumán (-13,5%), Santa Cruz (-12,6%) y la Ciudad de Buenos Aires (-12,4%), según Desarrollo Productivo.

Salvo en el distrito porteño, todas estas provincias tuvieron contagios crecientes de Covid-19 en octubre.

A la inversa, provincias como Jujuy, Salta y La Rioja están entre las que más mejoraron su situación; en los tres casos, los picos de contagios estuvieron en agosto (Jujuy) y septiembre (Salta y La Rioja).