---
category: Agenda Ciudadana
date: 2020-12-03T10:53:55Z
thumbnail: https://assets.3dnoticias.com.ar/serviciodoméstico.png
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: LT10'
resumen: 'Servicio doméstico: tendrá un 28% de aumento en tres cuotas'
title: 'Servicio doméstico: tendrá un 28% de aumento en tres cuotas'
entradilla: Recibirán un aumento salarial del 28% en tres cuotas. La negociación se
  extendió desde la semana pasada y en la que hubo pedidos de los sindicatos que fueron
  desde el 40% hasta el 100% de aumento.

---
**El personal doméstico recibirá un aumento salarial del 28% en tres cuotas (no acumulativo)**. El salario de las trabajadoras domésticas hoy no supera los $24.303,5 mensuales en su categoría más alta.

La Unión Personal Auxiliar de Casas Particulares (Upacp), con representación a nivel nacional, solicitó una recomposición del 42%, un mayor porcentaje de incremento para las zonas desfavorables en el sur del país y un bono no remunerativo a cargo del Estado. La mayoría de los gremios hizo pedidos en torno a ese número, pero finalmente se acordó una suba del 28%.

**El aumento se pagará en tres cuotas:** un 10% con el sueldo de diciembre (que se cobra en enero), un 8% en febrero de 2021 y un 10% en abril. Además, se acordaron tres puntos de incremento por zona desfavorable (pasa de 25% a 28%).

El personal para tareas generales, que hoy tiene un salario mensual de $17.785,50 para la modalidad con retiro, pasará a cobrar $19.564,05 con el sueldo de diciembre. Con el salario de febrero, recibirá $20.986,89. Y con el de abril, $22.765,44.