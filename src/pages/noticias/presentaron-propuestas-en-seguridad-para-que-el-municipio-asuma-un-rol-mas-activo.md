---
category: La Ciudad
date: 2021-03-17T08:41:54-03:00
thumbnail: https://assets.3dnoticias.com.ar/ines.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Presentaron propuestas en seguridad para que el Municipio asuma un rol más
  activo
title: Presentaron propuestas en seguridad para que el Municipio asuma un rol más
  activo
entradilla: Ante el incremento de la inseguridad en la ciudad, los concejales Inés
  Larriera y Carlos Pereira (UCR-Juntos por el Cambio) solicitaron la aplicación de
  una serie de medidas que involucren más al municipio en la materia.

---
Ante el incremento de la inseguridad en la ciudad, los concejales Inés Larriera y Carlos Pereira (UCR-Juntos por el Cambio) solicitaron a las autoridades locales la aplicación de una serie de medidas -que ya fueron presentadas en sendos proyectos de ordenanza- entre las que figuran la incorporación de más cámaras en espacios públicos; botones de alerta para comercios; la entrega de más botones de pánico a mujeres víctimas de violencia de género, y retomar las tareas de control en la vía pública, entre otras. “Son medidas concretas que puede realizar un Estado municipal que quiera comprometerse en un tema tan importante, y de hecho muchas de ellas fueron puestas en marcha por la anterior gestión, aunque lamentablemente en el último año hubo un corrimiento del Municipio en la materia”, señalaron los ediles. 

Preocupados por el incremento de la inseguridad en la ciudad de Santa Fe, los concejales de la UCR en Juntos por el Cambio, Inés nés Larriera y Carlos Pereira recordaron una serie de propuestas que se presentaron en el cuerpo legislativo para que el Municipio haga su aporte “en el tema que más preocupa a los vecinos y que necesita de la colaboración de todos. Por eso creemos que las autoridades locales tienen herramientas para poder colaborar en mejorar la seguridad en la ciudad”.

La lista de propuestas incluye desde la ampliación e incorporación de más tecnología en el sistema de vigilancia en el espacio público, hasta retomar las rutinas de control y vigilancia en la vía pública.

Los concejales señalaron que “se trata de medidas concretas que puede realizar un Estado municipal que quiera comprometerse en un tema tan importante, y de hecho muchas de ellas fueron puestas en marcha por la anterior gestión, aunque lamentablemente en el último año hubo un corrimiento del Municipio en la materia. Y todo indica que hay un desentendimiento por parte del Intendente en este tema: en el discurso de apertura de sesiones, donde supuestamente traza el rumbo de su gestión, Emilio Jatón no anunció una sola medida en materia de seguridad”, alertaron.

**Tecnología**

Inés Larriera y Carlos Pereira enfatizaron la necesidad de incorporar más cámaras en espacios públicos, calles comerciales y lugares de mayor concentración de peatones. “Es necesario avanzar con el anillo de fibra óptica para mejorar la cobertura”, aseguraron, y recordaron que la anterior gestión instaló cámaras en la zona de la Peatonal, por Blas Parera hacia el norte y todo el sector Suroeste de la ciudad. “Ahora hay que realizar el tendido de fibra óptica por Bulevar Pellegrini y Gálvez, y de esta manera mejorar la cobertura hacia los barrios que están en adyacencias, como 7 Jefes, Candioti Norte y Sur, Mariano Comas y Fomento 9 de Julio. Y en una segunda etapa avanzar hacia las Centros Comerciales de las avenidas Aristóbulo del Valle, General Paz, López y Planes y Facundo Zuviría y la Costanera”, apuntaron.

También recordaron su propuesta de entregar botones de alerta a comercios que trabajan en horario nocturno. “El Centro de Monitoreo tiene capacidad suficiente y un software adecuado para atender la conexión de algunos cientos de botones de alerta más (como ya lo hecho con situaciones de violencia de género y en colectivos). Se debe avanzar hacia un sistema donde los comercios adquieran el dispositivo de alerta y el Municipio los conecte al Centro de Monitoreo, para su seguimiento durante las 24 horas”, enfatizaron.

Además, con el objetivo de brindar una mejor respuesta ante un evento que ingresa a través del Sistema de Monitoreo de Alarmas, propusieron sumar la videoverificación, que es la transmisión de imágenes a través de wifi que activa su visualización en el Centro de Monitoreo Municipal al momento de la activación del botón de alerta. “Es un complemento importante, que permite visualizar qué está sucediendo en el lugar. Es tecnología que pueden adquirirla los propios beneficiarios de las alarmas y el Municipio necesita una mínima inversión para adaptar el software del Centro de Monitoreo”, sostuvieron Inés Larriera y Carlos Pereira.

**Asistencia a víctimas**

Los concejales de la UCR en Juntos por el Cambio también solicitaron volver a asistir desde el Municipio a las víctimas de familiares de víctimas de hechos delictivos. “El Gabinete de Asistencia Psicológica a víctimas de delitos y familiares que funcionó durante años en el ámbito del Municipio fue desmantelado por la actual gestión y el personal de psicólogos y otros profesionales fue despedido. Se trata de un servicio importante a la hora de brindar asistencia, ayuda y contención a quienes han padecido un hecho delictivo y, en particular, en situaciones donde se ha perdido la vida un familiar cercano”, apuntaron. Y entendieron que “esta es una forma efectiva para que el Municipio pueda estar cerca de quienes más lo necesitan y colaborar no solo con la asistencia psicológica tan necesaria sino también con otros tipos de asesoramiento, por ejemplo, en lo jurídico”.

También recordaron que “Santa Fe fue una ciudad pionera en el país en el otorgamiento de botones de pánico a mujeres que sufren situaciones de violencia habiendo, incluso, dado un importante apoyatura a las medidas dispuestas por el Poder Judicial en miles de situaciones concretas. Hoy existe en el Municipio una lista de espera de cientos de mujeres a las que no se les ha provisto todavía del dispositivo. Es necesario que el Municipio adquiera en forma urgente los botones y atienda esta demanda insatisfecha que pone en riesgo a muchas mujeres”, enfatizaron.

**Mejorar el espacio público**

Otro punto marcado por Inés Larriera y Carlos Pereira fue la necesidad de mejorar la iluminación en calles y espacios públicos. “Demás está marcar la importancia de una buena iluminación en calles de la Ciudad. Las zonas oscuras, con muchas luces apagadas, facilitan e incentivan el accionar delictivo. La ciudad llegó a tener durante el 2020 un total de 9000 luminarias apagadas, según lo reconoció el propio Intendente. Es necesario que el Municipio realice urgente una fuerte inversión en luminarias ya que cuenta con el personal y el equipamiento necesario para realizar estas tareas”, marcaron.

Por otra parte, recordaron que durante 2020 el Municipio retiró a la Guardia de Seguridad Institucional de muchas tareas de patrullaje y monitoreo, y paralelamente creció exponencialmente el número de cuidacoches y limpiavidrios. También se registraron vandalizaciones de plazas y parques, con rotura y pérdida de mobiliario urbano, e incluso se registraron robos a dependencias municipales (Jardines, Distritos, Escuelas de Trabajo) como nunca antes en la historia del Municipio. “Es importante que la GSI recupere su función y patrulle mejor los espacios públicos”, reclamaron.

**Eje social**

Finalmente, Inés Larriera y Carlos Pereira pidieron “retomar con fuerza el Programa Escuelas de Trabajo”, que dejó de funcionar como tal desde la asunción de la actual gestión municipal. Para los ediles, es de fundamental importancia que el Estado brinde oportunidades para mejorar la vida de las personas y, con ello, evitar situaciones de marginalidad social que conduzcan a conductas delictivas. “El desafío principal es trabajar con las personas más jóvenes, ayudando para que terminen sus estudios y capacitando para el trabajo. Las Escuelas de Trabajo eran un espacio que ayudaban a los jóvenes a terminar la escolaridad secundaria y a acompañarlos si querían continuar con estudios terciarios y universitarios. También brindaban capacitaban para el trabajo, a través de la enseñanza de oficios y la búsqueda de empleo en el sector privado, al punto que muchos jóvenes pudieron acceder a su primer trabajo en empresas privadas”, señalaron, y recordaron que “además, las Escuelas de Trabajo eran un ámbito de acompañamiento, de contención ante diversas situaciones familiares, de resolución de conflictos y de acompañamiento para el acceso al sistema de salud y otros servicios sociales”.