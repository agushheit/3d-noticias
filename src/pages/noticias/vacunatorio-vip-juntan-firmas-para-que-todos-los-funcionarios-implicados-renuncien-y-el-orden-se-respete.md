---
category: Agenda Ciudadana
date: 2021-02-23T07:31:32-03:00
thumbnail: https://assets.3dnoticias.com.ar/change.png
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Con información de Clarín
resumen: 'Vacunatorio vip: juntan firmas para que todos los funcionarios implicados
  renuncien y el orden se respete'
title: 'Vacunatorio vip: juntan firmas para que todos los funcionarios implicados
  renuncien y el orden se respete'
entradilla: Luego de que estalló el escándalo por la vacunación de políticos, periodistas
  y familiares, proponen que la gente adhiera a una petición en change.org.

---
El escándalo por el vacunatorio vip en el ministerio de Salud -punta de un inmenso iceberg de centros y listas blue de quienes accedieron a la vacuna Sputnik V antes de los que debían hacerlo- sigue esparciendo esquirlas en el gobierno, la oposición, los medios y el golpeado operativo de vacunación contra el coronavirus. Además de las reacciones y denuncias judiciales contra los funcionarios implicados, que comenzó a investigar el fiscal Eduardo Taiano, también se impulsa una campaña de firmas para que esos funcionarios sean despedidos y se respete el orden de vacunación establecido por las autoridades.

Los abogados Carlos Negri y Alejandro Drucaroff iniciaron una petición en la plataforma Change.org, dirigida al presidente y a todos los gobernadores, para que "la vacunación se haga estrictamente en el orden de prioridad de los grupos de riesgo, como lo establecen las normas vigentes y empezando por el personal del sistema de salud del cual queda un gran número sin vacunar".

La petición -cuya adhesión puede hacerse en [http://change.org/VacunacionVip-](https://www.change.org/p/basta-de-vacunaci%C3%B3n-vip-que-se-vacune-siguiendo-estrictamente-el-orden-establecido-alferdez-carlavizzotti-santicafiero?") también exige "que los responsables de la 'Vacunación VIP' renuncien y que sus conductas, como las de los vacunados en forma ilícita, sean investigadas."

"Planteamos la petición al margen de la grieta y dejando en claro que no importa si son oficialistas u opositores los que permiten o logran estas ventajas ilícitas y repugnantes".

En los argumentos de la campaña, sus impulsores explican que, "como no contamos con el número de vacunas necesario para poder vacunar rápidamente a toda la población, el Gobierno Nacional y los Gobiernos provinciales han establecido un orden lógico en el cronograma de vacunación, priorizando a los trabajadores de la salud y a los sectores de la población que mayor riesgo corren ante el virus."

"Sin embargo, en las últimas semanas, salieron a la luz diversos casos de personas que, aprovechando su cercanía con el poder, se han vacunado en forma privilegiada, a pesar de que no les correspondía según el orden fijado. En esa lista se incluyen funcionarios públicos nacionales y provinciales, legisladores y allegados a todos ellos que contaron con la obvia complicidad de los encargados de administrar las vacunas en los lugares o los responsables de las reparticiones donde se vacunaron".

"Esos actos vergonzosos no sólo vulneran la ética y son contrarios a toda idea de solidaridad", continúa la presentación, "sino que constituyen violaciones de normas vigentes que deben ser sancionadas con todo el peso de la Ley. Por eso, le queremos pedir al Presidente de la Nación que los responsables de lo que llaman 'Vacunación VIP' renuncien y que sus conductas sean investigadas."

Por último, la petición reclama que "de ahora en adelante haya un sistema transparente, donde se publique una lista que muestre cuantas dosis hay, y a que ciudadanos con nombre y apellido van a ser proporcionadas esas dosis. Lamentablemente, si no existe un sistema así, nuevamente puede haber privilegiados que se aprovechen de su posición."