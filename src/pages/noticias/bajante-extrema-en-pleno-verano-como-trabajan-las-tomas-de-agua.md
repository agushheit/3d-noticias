---
category: La Ciudad
date: 2022-01-06T06:15:37-03:00
thumbnail: https://assets.3dnoticias.com.ar/assa.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'Bajante extrema en pleno verano: ¿Cómo trabajan las tomas de agua?'
title: 'Bajante extrema en pleno verano: ¿Cómo trabajan las tomas de agua?'
entradilla: " Si bien la provisión hasta el momento puede brindarse, remarcan el uso
  responsable y solidario del recurso.\n\n"

---
El río Paraná, en las áreas de influencias a la ciudad de Santa Fe, continúa con niveles hidrométricos muy bajos. Este miércoles, la altura era de 15 cm por debajo del cero en el Puerto Santa Fe y la situación no parece que se vaya a modificar en las próximas semanas, según lo que indican las proyecciones del Instituto Nacional del Agua (INA). 

 Ante este contexto, y en plena temporada estival donde el consumo de agua por parte de la población es cada vez mayor, El Litoral dialogó con Germán Nessier, vocero de la empresa Aguas Santafesinas (Assa) para conocer cómo es actualmente la operatividad de las tomas de agua. 

 "En el muelle de la Toma Hernández están funcionando seis bombas y en la toma de Colastiné hay cuatro bombas operativas", remarcó Nessier y destacó que "la cuarta bomba que se instaló en Colastiné permite operar al 100 % la capacidad de transporte que tiene el acueducto que trae agua". 

 **Panorama conocido**

 Una de las ventajas que presenta este escenario de bajante del río Paraná y sus afluentes, es que ya van a cubrirse dos años desde que los niveles están muy por debajo de lo normal. Es por ello que Assa tuvo el tiempo necesario para acondicionar sus acueductos y alistarse de la mejor manera para atender la alta demanda de agua potable.  

 "El escenario actual ya lo tuvimos, no es muy distinto al que sucedió a mitad del 2021, a diferencia que la ventaja que ahora tenemos es que no hay afectación en el sabor del agua", señaló el vocero de Assa y agregó: "Continuamos operando con los equipos de refuerzo que se habían instalado en su momento en la toma de Colastiné, donde se incorporaron bombas sumergibles de refuerzo de gran potencia". 

 Respecto a si se contempla la posibilidad de que la bajante se profundice aún más, Nessier señaló que "estamos atentos a los pronósticos que vienen haciendo las autoridades del INA y así seguir con una vigilancia activa para poder reforzarnos. Tenemos equipos para reforzar las tomas, en función de los escenarios que se vayan presentando". Estos refuerzos a los que hizo mención el vocero de Assa, se refieren a las bombas sumergibles que pueden instalarse más alejadas de la costa, donde sí está asegurado un caudal permanente de agua. 

 "Cuando se ponen a prueba los sistemas, y se incorporan los usos que son estacionales y son recreativos (como el llenado de piletas), hay que tener mucha cautela en un escenario que, por un lado está la coyuntura de la bajante, y por el otro la capacidad instalada que siempre es finita, no es infinita", aseveró Nessier y concluyó: "Apelamos al uso responsable y solidario del agua". 

 **USO RESPONSABLE Y SOLIDARIO**

 Haciendo hincapié en el mensaje del cuidado y el uso responsable y solidario del recurso, algunas de las recomendaciones para no derrochar el agua: 

 -No llenar piletas o piscinas. Mantener limpia el agua para evitar llenarlas seguido.

\-Reparar las instalaciones defectuosas que originan pérdidas o fugas de agua.

\-Cerrar bien las canillas después de usarlas y no dejarlas abiertas inútilmente.

\-Abrir la ducha en el momento de entrar.

\-Mantener la canilla cerrada mientras se hace el cepillado de los dientes.

\-No dejar perder agua mientras se hace otra actividad.

\-No utilizar el lavarropas sino es con la carga completa de ropa.

\-No dejar las mangueras abiertas en el jardín o regar en horarios de alto consumo.

\-No utilizar el servicio de agua potable para el lavado de vehículos.