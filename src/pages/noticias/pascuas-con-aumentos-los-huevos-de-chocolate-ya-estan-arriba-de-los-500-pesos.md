---
category: La Ciudad
date: 2021-03-20T06:00:55-03:00
thumbnail: https://assets.3dnoticias.com.ar/HUEVOS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Pascuas con aumentos: los huevos de chocolate ya están arriba de los 500
  pesos'
title: 'Pascuas con aumentos: los huevos de chocolate ya están arriba de los 500 pesos'
entradilla: Los chocolates subieron entre 84 y 199% en relación al año pasado, según
  un relevamiento. Las fiestas de Pascua se vienen con aumentos en los alimentos.

---
Se acercan las fiestas de Pascuas y los productos de la llamada canasta básica de alimentos para esas celebraciones de Semana Santa presentan notables aumentos.

Este año que superan el índice de inflación, especialmente en los elaborados en base a cacao que tuvieron remarcaciones de entre un 84 y casi 199 por ciento con relación a la misma época del año pasado. En la ciudad de Santa Fe, dependiendo la marca y el tipo de chocolate, se puede encontrar un rango de precios de entre 220 a 500 pesos los 100 gramos. Por lo que un huevo de 227 gramos, mediano, de chocolate negro con forma de conejo de marca estándar cuesta 500 pesos, uno ovalado blanco con decoración de 200 gramos ronda los 800 pesos y uno del mismo peso, pero negro está 650 pesos en marcas de rango medio.

Después, los huevitos más chicos menores a los 100 gramos van de 170 a 500 pesos, dependiendo la marca. Los huevos de Pascua que apuntan a la niñez, con juguetes o dulces en su interior, están arriba de 650 pesos los que tienen 100 gramos de chocolate en marcas tradicionales. El típico huevo con forma de conejo de 60 gramos de marca estándar cuesta alrededor de 200 pesos (Bonafide, Felfort, Cabsha) y de marcas argentinas de ventas masivas arriba de los 400 pesos (Bon o Bon). Luego, marcas de alta gama tiene los 100 gramos de chocolate arriba de los 650 pesos la más barata (Águila, Milka, Ferrero Rocher, etc.). Lo más variado es lo artesanal, que van los 100 gramos entre los 100 a los 1.000 pesos. Los más caros son los artesanales de Bariloche que se pueden comprar por internet, que en general tienen más concentración de cacao.

Miguel Calvete, titular del Instituto de Estudios de Consumos Masivos, entidad que hizo un relevamiento sobre el movimiento de precios que registraron los productos que se consumen tradicionalmente durante la Semana Santa afirmó: “El seguimiento se realizó sobre los derivados del chocolate y cacao, los panificados y los llamados productos de mesa de vigilia, que son fundamentalmente pescados y enlatados”, explicó Calvete, quien subrayó de inmediato: “Encontramos distintos impactos de la inflación interanual en lo que respecta a los derivados del chocolate, con aumentos de entre un 84 por ciento y 119 por ciento”.

En declaraciones al programa "El primero de la mañana" de LT8, Calvete precisó que con respecto a los productos de panificación el promedio de incremento interanual fue del 73 por ciento. Estamos hablando de productos de primeras y segundas marcas. En el caso de las primeras, el aumento es aún mayor”.

“En el caso de la merluza, que es lo que más se consume en la mesa de vigilia, encontramos un aumento que va del 54 y 63 por ciento, dependiendo de los puntos de ventas, ya sean pescaderías, supermercados pequeños o grandes. Estamos hablando siempre de valores promedios”, aclaró Calvete sobre lo que se viene para estas Fiestas de Pascuas.

Según la entidad que monitorea la evolución de precios, en el caso de los alimentos enlatados como atún, caballa y merluza el promedio de incremento interanual “está entre el 45 y 52 por ciento. Eso es lo que menos aumentos tuvo, pero en todos los casos los incrementos superaron la tasa de inflación”.

“Por lo general, la venta de los productos estacionales se incrementa un 60 por ciento. Eso es lo usual año tras años. Lo que se estima para este año es que el crecimiento será de un 40 por ciento y 45 por ciento. Eso también hace que los comerciantes especulen por la sobre demanda. También esperamos un consumo segmentado dado que los sectores medios o medios altos son aquellos que en esta fecha impulsan el consumo", dijo sobre los aumentos en las Pascuas.

Y concluyó: "Las personas que están en la base de la pirámide de consumo, sin contar los que están en la extrema pobreza, no compran estos productos. Eso es lo que hemos relevado. Desde hace una década el consumo de estos productos vinculados a Pascuas, es algo más reservado a sectores asalariados más acomodados”.