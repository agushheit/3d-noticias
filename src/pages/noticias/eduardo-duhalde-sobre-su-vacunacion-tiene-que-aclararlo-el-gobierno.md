---
category: Agenda Ciudadana
date: 2021-02-24T06:48:03-03:00
thumbnail: https://assets.3dnoticias.com.ar/duhalde.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de La Nación
resumen: 'Eduardo Duhalde, sobre su vacunación: "Tiene que aclararlo el Gobierno"'
title: 'Eduardo Duhalde, sobre su vacunación: "Tiene que aclararlo el Gobierno"'
entradilla: El expresidente Eduardo Duhalde afirmó que es el Gobierno quien debe aclarar
  la situación de su vacunación. El listado fue divulgado ayer por la Casa Rosada.

---
El expresidente Eduardo Duhalde afirmó que es el Gobierno el que debe aclarar la situación de su vacunación, que trascendió en una lista de 70 beneficiados con dosis por fuera del protocolo previsto. El listado fue divulgado ayer por la Casa Rosada.

"Tiene que aclararlo el Gobierno, tienen que aclararlo ellos", fue la lacónica frase que pronunció el exmandatario vía telefónica. Tras esa sentencia, no realizó más apreciaciones y finalizó la comunicación.

El expresidente no se vacunó en soledad. También accedieron al beneficio de una inoculación anticipada su esposa, Hilda "Chiche" Duhalde; sus hijas, María Eva y Juliana, y su secretario, Carlos Mao.

La escueta respuesta de Duhalde es la misma que expresó ante otras requisitorias periodísticas, como la del diario Clarín.

El nombre de Duhalde apareció ayer en la lista de 70 vacunados que difundió la nueva ministra de Salud, Carla Vizzotti, tras el escándalo de la vacunación vip que le costó el puesto a su antecesor, Ginés González García.

Además de Duhalde varios peronistas históricos figuran en la nómina que blanqueó el Gobierno ayer. Entre otros, el exintendente de Tres de Febrero, Hugo Curto, y el dirigente Lorenzo Pepe. Daniel Scioli y Carlos Zannini fueron otros nombres que aparecieron en el listado de vacunados por fuera de los grupos prioritarios.