---
category: Agenda Ciudadana
date: 2021-09-24T06:15:14-03:00
thumbnail: https://assets.3dnoticias.com.ar/HOFFMANN.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'Se retomaron las paritarias: tras la primera reunión, estatales y docentes
  aguardan una oferta'
title: 'Se retomaron las paritarias: tras la primera reunión, estatales y docentes
  aguardan una oferta'
entradilla: Este jueves volvieron a reunirse los gremios con la provincia. Entre otros
  temas, se planteó la necesidad de un aumento salarial. El viernes de la semana próxima
  habría una propuesta concreta.

---
Este jueves se retomaron las negociaciones paritarias entre el gobierno provincial con los gremios de los empleados estatales y con los docentes.

Como suele ocurrir, en este primer encuentro no hubo ofrecimientos, pero las partes acordaron volver a reunirse el viernes de la semana próxima donde los representantes de los trabajadores esperan recibir una propuesta.

Tras el encuentro con los gremios de la administración central, el titular de UPCN, Jorge Molina, comentó que se realizaron “reclamos salariales y de agenda gremial, como son las subrogancias y el pase a planta, fundamentalmente”.

“Nunca hay acuerdos en la primera reunión, pero sí se presentaron los criterios, y nuestro criterio es que el salario supere a la inflación, se lo dijimos al gobierno, obviamente hay que hacer proyecciones, pero queremos que el sueldo de los trabajadores en diciembre esté por encima del Índice de Precios al Consumidor”.

Mientras, el secretario general de ATE, Jorge Hoffmann, agregó que “la próxima reunión va a ser el viernes de la semana próxima y ese día esperamos la propuesta, que debe superar a la inflación, esa es una política pública nacional y tenemos expectativa que la propuesta sea el viernes y responda a este planteo”.

**Docentes**

En el mismo rumbo se desarrolló la paritaria entre el Ejecutivo santafesino y los gremios docentes, el encuentro “se centró en la discusión salarial. Planteamos la necesidad de que el gobierno presente una propuesta que implique recuperar el poder adquisitivo, un aumento salarial para activos y jubilados”, explicó Rodrigo Alonso, de Amsafe.

Al respecto, agregó que ese ofrecimiento también “debe contemplar además una propuesta de pase al salario básico de las sumas no remunerativas y no bonificables otorgadas en 2020, para mejorar el salario, la jerarquía, y la antigüedad. El gobierno tomó nota de esto y planteó que el viernes 1 de octubre iba a presentar una propuesta”

Respecto del monto del incremento salarial Alonso indicó que “de acuerdo a nuestros informes evaluamos que la inflación para 2021 podría llegar a entre 46,5 y 50 por ciento, por lo tanto, desde esos parámetros estamos empezando a discutir. La paritaria nacional plantea el 45,5 por ciento, se firmó en agosto y planteaba una revisión en noviembre. Nosotros entendemos que esa paritaria quedo corta y creemos que la provincia está en condiciones de otorgar una recomposición salarial que implique no solamente empatarle a la inflación, para no perder, sino que también tiene que garle a la inflación”.