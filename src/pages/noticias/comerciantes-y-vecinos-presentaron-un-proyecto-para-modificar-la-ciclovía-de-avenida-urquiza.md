---
layout: Noticia con imagen
author: ACAPAU
resumen: Senda Inclusiva
category: La Ciudad
title: Comerciantes y vecinos presentaron un proyecto para modificar la ciclovía
  de avenida Urquiza
entradilla: 'Elaborado por comerciantes de la zona que se oponen a la que está
  instalada, el proyecto "Senda Inclusiva" no sería solo para uso de ciclistas,
  sino también para runners, skaters y personas en silla de ruedas. '
date: 2020-11-01T12:00:00.000Z
thumbnail: https://assets.3dnoticias.com.ar/urquiza1.jpg
---
**La Asociación de Comerciantes, Amigos y Profesionales de Avenida Urquiza (ACAPAU)** presentó esta semana un proyecto alternativo a la actual ciclovía de avenida Urquiza instalada por la Municipalidad de Santa Fe en el mes de mayo. El proyecto originar contempla un carril para bicicletas que se extiende desde Bulevar y hasta J. J. Paso al sur por dicha arteria.

[](<>)"Nuestro malestar con la ciclovía improvisada de calle Urquiza nació desde el día cero. Al ver los trabajos llevados adelante por la municipalidad, rápidamente comenzamos a hablar con concejales para que tomen cartas en el asunto. Lo que actualmente están haciendo, genera un peligro inminente para el tránsito y para la vida de los usuarios de esa ciclovía, totalmente inapropiada, puesta en marcha de forma intempestiva sin dar ningún fundamento, ni consultar a los vecinos", sostuvo Rubén, integrante de ACAPAU.

"A raíz de la falta de respuestas, conformamos la Asociación de Comerciantes, Amigos y Profesionales de Avenida Urquiza (ACAPAU) y comenzamos a movilizarnos entre vecinos y comerciantes de la zona", subrayó Luis, otro de los integrantes de la Asociación. "Estamos hablando de una de las calles principales de Santa Fe, donde están desarrollando un proyecto totalmente improvisado y precario. Por eso, es que la propuesta que elevamos y presentamos entre comerciantes y vecinos, pensamos que es superadora desde todo punto de vista. Queremos un ciudad con proyectos de calidad, inclusiva, agradable, segura y lista para disfrutar. Acá pintaron una línea amarilla y listo", continuó Luis.

**Los puntos y fundamentos del nuevo proyecto para la ciclovía de Urquiza**

1. El lado derecho es más seguro para la circulación de los ciclistas, ya que la propuesta actual va adyacente al carril más rápido de la avenida siendo de sobrepaso vehicular.

2. El carril para los ciclistas se encuentra protegido por las dársenas de estacionamiento, lo cual impide que las bicicletas queden expuestas a accidentes y lo suficientemente aisladas de los carriles de circulación vehicular, ampliada la seguridad por al espacio de apertura de cortesía de los autos.

3. Ofrece la posibilidad de construir una bicisenda de doble mano. No nos parece razonable que se ejecute simplemente una senda de una mano; está claro que deberá ejecutarse a futuro la otra senda de circulación de sur a norte, pero ello obligaría a intervenir otra arteria céntrica, con los inconvenientes que ello traería aparejado. Aquí estamos ante una intervención que sólo afecta una calle y soluciona la circulación hacia ambas direcciones.

4. Incluimos como se ve en las láminas de Proyecto hay una segunda senda de menor porte en idea original para uso Runners.

5. El Proyecto requiere de un muy bajo costo de ejecución, ya que toda la intervención se resuelve con pintura sobre el asfalto, con muy pocos conos de seguridad, al solo efecto de separar la circulación de la bicisenda cordones similares a los de vereda con arreglos Naturales.

"Estamos convencidos que la propuesta presentada es superadora a la aprobada por el Concejo Municipal, fundamentalmente en lo relacionado a la seguridad vial. Además, mantuvimos conversaciones con organizaciones que trabajan las problemáticas de la bicicleta en la ciudad y otras organizaciones nacionales como Luchemos por la Vida e instituciones académicas, las cuales manifestaron su acuerdo con el criterio sustentado", fundamentaron en un comunicado desde la Asociación de Comerciantes, Amigos y Profesionales de Avenida Urquiza (ACAPAU).

![](https://assets.3dnoticias.com.ar/urquiza.jpg)