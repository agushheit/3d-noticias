---
layout: Noticia con imagen
author: "Fuente: Diario UNO"
resumen: Apartaron a un sacerdote
category: Agenda Ciudadana
title: Apartaron a un sacerdote por enviar videos sexuales a sus alumnos
entradilla: La decisión la tomó el Arzobispado de Santa Fe ante una denuncia de
  carácter sexual. El religioso daba clases en una escuela en Gobernador Crespo
  y prestaba sus servicios en otras tres localidades del norte provincial.
date: 2020-11-26T13:49:38.056Z
thumbnail: https://assets.3dnoticias.com.ar/la-criolla.jpg
---
Este martes se conoció la noticia del apartamiento de un sacerdote que prestaba sus servicios religiosos en al menos tres localidades del norte santafesino por orden del Arzobispado de Santa Fe ante una denuncia de carácter sexual. Según trascendió, el cura habría enviado videos con contenido pornográfico a alumnos de una escuela de la localidad de Gobernador Crespo, en donde es titular de la cátedra Programa Educación para el Amor, más conocida como PEPA.

Si bien se desconoce aún si el envío de los videos fue un error o hubo intencionalidad, el religioso fue trasladado mientras se realiza la investigación. Además de dar clases en Gobernador Crespo, donde residía, también oficiaba misa en La Criolla, Vera y Pintado y La Camila.

Desde el Ministerio de Educación de la provincia de Santa Fe se activaron los protocolos pertinentes para este tipo de casos y se informó a los padres de los alumnos.