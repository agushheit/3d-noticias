---
layout: Noticia con imagen
author: "Fuente: La Política Online"
resumen: Cambios en Gobierno
category: Estado Real
title: "Exclusivo: Perotti decidió nombrar a Sukerman en Gobierno, pero espera
  hasta fin de año para hacer los cambios"
entradilla: El ministro de Gestión Pública, Rubén Michlig, también le avisó que
  se va en diciembre y se esperan más reemplazos en el gabinete.
date: 2020-11-17T14:34:43.790Z
thumbnail: https://assets.3dnoticias.com.ar/perotti.jpg
---
Con la intempestiva salida de Esteban Borgonovo del Ministerio de Gobierno, Omar Perotti debió anticipar el armado de un nuevo gabinete que lo acompañe a partir de diciembre.

Si bien como reveló LPO en exclusiva, el gobernador ya tenía en mente hacer cambios en el equipo, la renuncia de Borgonovo aceleró la definición de los cambios. Una decisión que generó fastidio en el gobernador que tuvo que aguantar que Borgonovo dejara trascender críticas y acusara que no tenía espacio para gestionar.

Fuentes al tanto de los cambios que se vienen afirmaron a LPO que el actual ministro de Trabajo, Roberto Sukerman, asumirá la cartera de Gobierno, mientras que Juan Manuel Pusineri quien está al frente de la Secretaría de Trabajo, se hará cargo de la conducción de la cartera laboral.

Sukerman cuenta con dos condiciones importantes: es rosarino y milita en el espacio de Agustín Rossi. De esta manera, la agrupación La Corriente se haría cargo del ministerio político, en un enroque que protege el futuro de Sukerman al sacarlo del área de Trabajo, una de las más conflictivos en épocas de vacas flacas.

![](https://assets.3dnoticias.com.ar/sukerman.jpg)

Pero no es el único cambio que se viene. Rubén Michlig le anunció a Perotti que a fin de año dejará la nueva cartera de Gestión Pública y será reemplazado por otro rafaelino, Marcos Corach, de íntima confianza del gobernador como lo era Michlig.

Según pudo saber LPO, la relación entre Michlig y el ministro de Economía, Walter Agosto, se fue desgastando con el paso de los meses. Agosto es un economista clásico y un ferviente defensor del superávit fiscal y de la necesidad de contar con fondos de reserva.

La relación entre Michlig y el ministro de Economía, Walter Agosto, se fue desgastando con el paso de los meses. Agosto es un economista clásico y un ferviente defensor del superávit fiscal. Su ortodoxia chocó de frente con la idea del ministro de Gestión Pública de repartir fondos en sus recorridas por la provincia.

La ortodoxia chocó de frente con la necesidad que tiene cualquier encargado de un área como Gestión que debe recorrer la provincia llevando plata para solucionar problemas, sobre todo en época de pandemia.

No obstante, Michlig no quedará totalmente afuera del gobierno y se ocupará del armado político del perottismo en el centro y norte de la provincia.

![](https://assets.3dnoticias.com.ar/borgnovo.jpg)

En este clima es lógico que los rumores de otros cambios en el gabinete se amontonen. La bielsista Silvina Frana, al frente del súper Ministerio de Infraestructura, fue una protagonista de los trascendidos de renuncia en más de una oportunidad, pero siempre fueron negados desde su entorno.

Semanas atrás, cuando los contagios de coronavirus estaban al tope, con más de dos mil casos diarios, circuló insistentemente la versión de la salida de la ministra de Salud, Sonia Martorano. A ellas, se sumaron el de Cultura, Jorge Llonch y de Educación, Adriana Cantero. Habrá que esperar para diciembre para ver cuáles se confirman.