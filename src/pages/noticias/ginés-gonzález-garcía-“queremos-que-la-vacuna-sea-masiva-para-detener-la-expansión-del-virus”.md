---
layout: Noticia con imagen
author: "Fuente: Radio Mitre"
resumen: "Coronavirus: vacuna masiva"
category: Agenda Ciudadana
title: "Ginés González García: “Queremos que la vacuna sea masiva para detener
  la expansión del virus”"
entradilla: El ministro de Salud de la Nación reiteró que no será obligatorio
  vacunarse, aunque apostó al convencimiento por parte de la sociedad. “Cuanta
  más gente se vacuna, menor será la propagación del virus”.
date: 2020-11-07T14:51:43.036Z
thumbnail: https://assets.3dnoticias.com.ar/ggg2.jpg
---
El ministro de Salud de la Nación, Ginés González García, confirmó este sábado que la vacuna contra el coronavirus no será obligatoria, aunque destacó que el objetivo del Gobierno es que su aplicación “sea masiva para detener la expansión del virus”.

En ese sentido, el funcionario explicó que “mientras más gente vacunada haya, menos circulación habrá” del coronavirus y remarcó que primero comenzarán con los grupos de riesgo.

No obstante, González García sostuvo que la vacuna no solucionará la pandemia e instó a la sociedad a seguir con los cuidados para evitar un nuevo pico de contagios. “Deberemos seguir con los cuidados, con el distanciamiento y el uso de tapabocas”, explicó al respecto.

Además, en declaraciones a Radio Mitre, el ministro se refirió a la decisión del Gobierno de adquirir millones de dosis de vacunas contra el coronavirus a Rusia y aseguró que el fármaco deberá superar todas las etapas de prueba necesarias para inmunizar a la población.

“Hace meses que iniciamos las negociaciones y están muy avanzadas. Tenemos distintas alternativas y distintos proveedores, pero todos estamos subordinados a que termine la fase 3“, detalló.

En ese sentido, se mostró “muy conforme” con la gestión del Gobierno y aseguró que su “miedo” era que “la “hegemonía política tuviera un rezago de la disponibilidad de vacunas” para con la Argentina. “Eso no va a pasar”, destacó.

“Argentina va a tener en tiempo y forma la cantidad de vacunas para combatir el coronavirus”, agregó González García al tiempo que se refirió al proceso de logística para aplicarla en todo el país y aseguró que será un “desafío único”.