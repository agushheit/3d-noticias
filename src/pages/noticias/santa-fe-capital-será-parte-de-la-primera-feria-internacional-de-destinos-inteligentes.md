---
layout: Noticia con imagen
author: .
resumen: "Santa Fe: Destino Inteligente"
category: La Ciudad
title: Santa Fe Capital será parte de la primera Feria Internacional de Destinos
  Inteligentes
entradilla: La ciudad mostrará su oferta turística del 20 al 29 de noviembre en
  un stand virtual. La feria es una forma innovadora de afrontar la crisis que
  afecta al sector.
date: 2020-11-17T16:30:17.643Z
thumbnail: https://assets.3dnoticias.com.ar/playa.jpg
---
**Durante 10 días, Santa Fe Capital mostrará sus servicios, atractivos y propuestas innovadoras en materia turística en la primera Feria Internacional de Destinos Inteligentes (FI-DI).** 

La ciudad contará con un stand virtual que podrá ser visitado a través del sitio [www.fi-di.com](https://www.fi-di.com/) junto a cientos de destinos turísticos, proveedores y prestadores de servicios del mundo y del país.

“**Hace poco Santa Fe fue declarada Destino Seguro por el Consejo Mundial de Viajes y Turismo**. Esto habla del trabajo que venimos haciendo desde el primer día de la emergencia sanitaria para estar preparados para cada etapa de la pandemia”, recordó el intendente Emilio Jatón y agregó: “Nuestra ciudad tiene mucho para ofrecer como destino para turistas argentinos y extranjeros y, por eso, estamos trabajando fuertemente en ampliar las propuestas y poner en valor nuestra historia, nuestra cultura y nuestros paisajes”.

Durante los diez días de feria, el stand virtual podrá ser visitado por los usuarios, donde se mostrarán los atractivos turísticos, habrá personal de la Dirección de Turismo de la Municipalidad que brindará información, y además, se ofrecerá una serie de contenidos multimedia que los usuarios podrán disfrutar como videos y fotografías. También, como en todas las instancias de este tipo, habrá espacios para la formación y el intercambio comercial.

## **Desafío FI-DI**

**FI-DI es la primera experiencia virtual de una feria de turismo en el país.** 

Es organizada por la Red de Destinos Inteligentes y ofrece una oportunidad de transformar de una vez por todas las lógicas para el sector. En esta primera edición nuestra ciudad tendrá un stand que permitirá mostrar de la mejor manera posible los atributos diferenciales de la capital provincial.

Además de las acciones de promoción, a través de materiales multimedia, la ciudad participará con dos charlas: una sobre el desarrollo de las audioguías en el marco del P**rograma “Mi ciudad como Turista”**, y otra sobre el desarrollo del sistema de calidad interna.

En este sentido, la primera de las charlas estará a cargo del director ejecutivo de Turismo municipal, Franco Arone, quien mostrará el desarrollo de las audioguías, que convierten a la ciudad de Santa Fe en la primera del país en ofrecer con este tipo de materiales. En tanto, la segunda charla estará a cargo del personal de la Dirección de Turismo y será sobre el desarrollo del Sistema de Calidad Interna de la Dirección de Turismo (SICAT) que optimiza recursos del Estado municipal.

Además, en el marco de FI-DI se desarrollarán otras actividades como rondas de negocios, áreas de networking y seminarios de capacitación.

## **Acerca de FI-DI**

En números, la primera Feria Internacional de Destinos Inteligentes (FI-DI), reunirá más de 100 destinos turísticos del país y del mundo, más de 200 oradores nacionales e internacionales y se estiman más de 20 mil participantes.

Es la primera experiencia virtual para el formato Ferias en el país y será una experiencia revolucionaria para el rubro.

## **Audioguías**

La Municipalidad invita a redescubrir Santa Fe, a mirar con otros ojos esos lugares por los que el vecino pasa a diario, y a dejarse sorprender por la historia y la cultura de la capital provincia. 

Las audioguías se pueden escuchar en: el sitio oficial de[ Santa Fe ciudad](https://santafeciudad.gov.ar/secretaria-de-produccion-y-desarrollo-economico/turismo/mi-ciudad-como-turista/)