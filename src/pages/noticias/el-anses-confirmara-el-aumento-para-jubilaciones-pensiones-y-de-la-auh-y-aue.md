---
category: Agenda Ciudadana
date: 2021-02-18T07:23:07-03:00
thumbnail: https://assets.3dnoticias.com.ar/anses.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de ANSES y Diario UNO
resumen: El Ansés confirmará el aumento para jubilaciones, pensiones, y de la AUH
  y AUE
title: El Ansés confirmará el aumento para jubilaciones, pensiones, y de la AUH y
  AUE
entradilla: Sin IFE, el organismo previsional confirmará la primera actualización
  de los haberes para marzo. Se suman los montos de ayuda escolar anual y continúa
  la recarga doble de la Tarjeta Alimentar.

---
La Administración Nacional de Seguridad Social (Ansés), anunciará el porcentaje de actualización que aplicará para llevar adelante la primera suba del año para jubilados, pensionados, titulares de la Asignación Universal por Hijo

Con la cancelación del bono de $10.000 del Ingreso Familiar de Emergencia (IFE), el conjunto de beneficiarios de planes asistenciales podrán sumar también el monto correspondiente a la Tarjeta Alimentar y la Ayuda Escolar Anual.

*** Aumento para jubilados y pensionados**

Ansés ya cuenta con un indicador definido que le permitirá conformar el número final del primer anuncio de suba para jubilados y pensionados: se trata de la Remuneración Imponible de Trabajadores Estables (Ripte) de diciembre de 2020 que se ubicó en 2%. Sumado al 1,3% de noviembre y 4,6% de octubre, el cuarto trimestre del año pasado arrojó una mejora del 7,9%, sin embargo aún resta por adicionar otros de los parámetros de acuerdo a lo estipulado por la Ley 27.609, es decir el otro 50% determinado por la recaudación tributaria del organismo previsional.

De esta manera la suba de marzo 2021 rondará entre el 8 y el 9,5%.

Si se toma como referencia el promedio de 9%, la jubilación mínima pasará de $19.035,29 a $20.748,47 y la máxima de $128.089,54 a $139.617,6.

*** Aumento para AUH y AUE de Ansés**

Además de los haberes jubilatorios, el aumento de marzo a confirmarse en los próximos días también impactará en los beneficiarios de la Asignación Universal por Hijo (AUH) y Asignación por Embarazo (AUE).

La Ley 27.609 aprobada por el Congreso de la Nación contempla la actualización de los haberes previsionales cada tres meses, lo que deja un total de cuatro aumentos previstos para este 2021 y que se aplicarán en marzo, junio, septiembre y diciembre.

Con el cálculo del 9%, el valor de AUH y AUE serán los siguientes:

Asignación Universal por Hijo pasará de $3.717 a $4.051,53

Asignación Universal por Embarazo pasará de $3.717 a $4.051,53

*** Aumento de la Tarjeta Alimentar y pago de Ayuda Escolar Anual 2021**

Esta se anunció el pago de la Ayuda Escolar Anual 2021 para los beneficiarios de la AUH y Asignación Familiares por Hijo (AFH).

El trámite se puede realizar de forma presencial en una oficina del organismo previsional con solo sacar un turno online o bien por la web oficial.

Además en marzo también se continuará con la recarga de saldo de la Tarjeta Alimentar con la reciente suba del 50%, que elevó los montos a $6.000 y $9.000, según la cantidad de beneficiarios.

El beneficio se acredita el tercer viernes de cada mes, por lo que el próximo 19 de febrero será el día de cobro para aquellas personas que cuenten con la tarjeta física, y el lunes 22 de febrero, quienes lo hagan por medio de cajero automático.