---
category: La Ciudad
date: 2021-01-25T09:15:36Z
thumbnail: https://assets.3dnoticias.com.ar/colectivos.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Carlos Suarez
resumen: Carlos Suárez solicitó que se declare la emergencia en el sistema de transporte
  público por colectivos
title: Carlos Suárez solicitó que se declare la emergencia en el sistema de transporte
  público por colectivos
entradilla: 'Será por 180 días y en ese lapso se deberá convocar a todos los actores
  involucrados en el tema, para abordar los aspectos económicos, estructurales y normativos
  del sistema. '

---
“El sistema de transporte público en Santa Fe agoniza, por lo que es el momento de tomar medidas de fondo”, afirmó el concejal de UCR-Juntos por el Cambio.

Ante la confirmación por parte de funcionarios de la Municipalidad de Santa Fe de un  nuevo aumento en el precio del boleto, y teniendo en cuenta la discriminación que sufre la ciudad en la distribución de subsidios para el transporte por colectivos, el concejal de UCR-Juntos por el Cambio, Carlos Suárez, solicitó la declaración emergencia de este sistema por 180 días. 

“Planteamos la declaración de emergencia a los fines de darnos, como ciudad, un plazo prudencial para definir el sistema de transporte que podemos tener”, afirmó el edil. En ese lapso de tiempo, el proyecto de ordenanza prevé la convocatoria a una mesa de discusión a integrantes del Ministerio de Transporte de la Nación, Secretaría de Transporte de la Provincia, representantes de los distintos bloques del Concejo, universidades, centro comercial, sindicatos, asociaciones civiles que aborden la temática, firmantes del pacto de movilidad y vecinales. 

Además, durante el plazo que dure la emergencia, el proyecto de ordenanza permite al municipio adoptar medidas para readecuar el diseño estratégico, técnico y operativo de la prestación del servicio, con el fin de garantizar su funcionamiento. En este sentido, se deberá privilegiar la proximidad, la promoción de transportes alternativos sustentables y la posibilidad de combinación de medios de movilidad.

“Vemos que el sistema de transporte público de Santa Fe agoniza, por lo que es el momento de tener una discusión de fondo y tomar medidas de fondo. Se debe evaluar, desde todas las miradas, el sistema que hoy tenemos, sus carencias y sus virtudes. A partir de allí debemos avanzar en un rediseño integral del mismo, que nos permita en una ciudad que claramente crece, poder pensar en un sistema que también vaya creciendo y que ello no necesariamente signifique un golpe en el bolsillo de los vecinos”, indicó Carlos Suárez.