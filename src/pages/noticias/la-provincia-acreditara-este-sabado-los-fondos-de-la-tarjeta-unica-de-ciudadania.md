---
category: Agenda Ciudadana
date: 2021-02-20T06:30:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/TARJETAUNICAjpg.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Provincia acreditará este sábado los fondos de la Tarjeta Única De Ciudadanía
title: La Provincia acreditará este sábado los fondos de la Tarjeta Única De Ciudadanía
entradilla: El Ministerio de Desarrollo Social, a través de la Dirección Provincial
  Programa Alimentario, informa que a partir del sábado 20 de febrero estarán acreditados
  los fondos de la Tarjeta Única de Ciudadanía.

---
El Ministerio de Desarrollo Social, a través de la Dirección Provincial Programa Alimentario, informa que a partir del sábado 20 de febrero estarán acreditados los fondos de la Tarjeta Única de Ciudadanía, correspondientes al mes de febrero, en todas las localidades de la provincia de Santa Fe.

Además, se acreditará un refuerzo de $700 para los beneficiarios que convivan en su grupo familiar con niños/as de hasta 12 años de edad, y/o personas mayores a partir de los 65 años de edad; llegando a $1.000.

**Componente Celíacos**

Asimismo, el 20 de febrero estarán acreditados los fondos de la Tarjeta Única de Ciudadanía, Componente Celíacos, correspondientes al mes de febrero, en todas las localidades de la provincia de Santa Fe.

Todos los beneficiarios con celiaquía, que no cuentan con cobertura de obra social, percibirán una acreditación de $3.000.

Asimismo, recordamos los derechos y obligaciones a las y los beneficiarios de la tarjeta:

>> Presentar DNI

>> No existe la obligación de gastar todo el monto en una sola compra y un solo comercio

>> Nadie puede retener la tarjeta

>> Debe exigirse el comprobarte de la compra con el saldo de la tarjeta

>> Puede realizarse la compra en cualquier comercio adherido a la red

>>El comerciante no puede recargar el precio por el uso de la tarjeta

Ante cualquier irregularidad, debe realizarse la denuncia en la comuna o municipio correspondiente y, también, en el Ministerio de Desarrollo Social de Santa Fe, comunicándose al teléfono (0342) 4579269 o a través de www.santafe.gov.ar.