---
category: Estado Real
date: 2021-12-09T06:15:46-03:00
thumbnail: https://assets.3dnoticias.com.ar/videovigilancia.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La provincia invierte 20 millones de pesos para la incorporación de nuevas
  cámaras de videovigilancia en la cuidad de Santa Fe
title: La provincia invierte 20 millones de pesos para la incorporación de nuevas
  cámaras de videovigilancia en la cuidad de Santa Fe
entradilla: 'Será a través de un convenio firmado entre el Ministerio de Seguridad
  y la Municipalidad, en el marco del Plan Incluir.

'

---
El ministro de Seguridad de la provincia, Jorge Lagna, junto al Intendente de Santa Fe, Emilio Jatón, rubricaron este martes un convenio en el marco del Plan Incluir, por el que la provincia otorgará 20 millones de pesos a la capital provincial para incorporar alrededor de 68 cámaras de videovigilancia, entre nuevas a adquirir y otras a reparar.

En ese marco, Lagna sostuvo: “Nosotros, en esta nueva etapa en el Ministerio, apuntamos a tener mayor visibilidad en las calles, y este aporte para videovigilancia tiene que ver con eso. Vamos a invertir en más tecnología y agentes en la calle. En breve se recibirán casi 900 policías, que la mayoría estarán en Santa Fe y Rosario”, indicó el ministro, y agregó: “La manera de abordar el delito es con todo el Estado presente y en conjunto, por eso también estamos en la tarea de profundizar la relación con el Municipio con la Mesa de Seguridad Local”.

Por su parte, el nuevo secretario de Seguridad Pública de la provincia, Jorge Bortolozzi, valoró la firma institucional del Convenio y dijo: “Entendemos que con esto estamos dando una respuesta concreta. Las cámaras que se van a adquirir son fundamentales para el esclarecimiento de hechos, para la prevención, para la investigación, y la inversión en tecnología es muy importante para poder llevar más tranquilidad a la gente”.

Finalmente, el intendente de Santa Fe, Emilio Jatón, remarcó que “el objetivo es reforzar la videovigilancia en lugares claves. Le presentamos al ministro un proyecto para eso, porque vamos a apuntar directamente a los lugares que nos marca el mapa del delito. Esto se va a operativizar con una licitación que vamos a realizar en los próximos días, para poder poner en funcionamiento las cámaras que no están en condiciones y aquellas que le vamos a sumar a la ciudad”.