---
layout: Noticia con imagen
author: "Fuente: LT10"
resumen: "Transporte: más habilitaciones"
category: Agenda Ciudadana
title: Habilitaron el transporte de media y larga distancia en la provincia
entradilla: A través del decreto Nº 1440, el gobernador Omar Perotti dispuso que
  los servicios de micros que aún permanecían inactivos deberán adecuarse a los
  protocolos dictados por la Nación.
date: 2020-11-19T12:11:45.870Z
thumbnail: https://assets.3dnoticias.com.ar/colectivos.jpeg
---
El gobernador Omar Perotti firmó el decreto Nº 1440, por el que la Provincia adhiere a los protocolos elaborados por la Nación para el servicio de transporte ferroviario y automotor.

En la práctica, esto implica que se habilitaron los servicios de micros de media y larga distancia en el territorio santafesino.

## **CONDICIONES PARA LAS EMPRESAS**

El texto indica que las empresas de transporte deben cumplir ciertas condiciones: ajustarse al cronograma de frecuencias, los horarios y modalidades de prestación de los servicios que defina la Secretaría de Transporte, en base a las medidas de prevención dispuestas por el Ministerio de Salud; y limitar el traslado a las personas que se encuentren habilitadas para circular.

## **REQUISITOS PARA LOS PASAJEROS**

En tanto, los pasajeros, sean residentes o no residentes, al ingresar a la provincia de Santa Fe, deben cumplir con los siguientes requisitos:

**1.** Portar el **certificado único habilitante para circulación** y toda documentación requerida.

**2.** Realizar la **declaración jurada de salud en la app Covid-19 de la Provincia de Santa Fe** y cumplimentar los formularios relacionados.

**Además, las personas no residentes en la provincia de Santa Fe, deben cumplimentar los siguientes requisitos:**

**3.** Informar el viaje interprovincial en la app COVID-19 de la Provincia de Santa Fe o en el sitio: http://www.santafe.gob.ar.coronavirusweb/formulario_viaje_interprovincial.

**4. Permanecer durante los primeros catorce (14) días, o todo el período si el plazo de permanencia fuere menor, en sus domicilios** o lugares de residencia observando aislamiento social, preventivo y obligatorio y la prohibición de circular, limitando los desplazamientos a los estrictamente necesarios relacionados con la actividad que motivó su ingreso a la Provincia, o por razones alimentarias, de salud o de fuerza mayor o a las actividades habilitadas; siendo obligatorio en tales casos el uso de elementos de protección de nariz, boca y mentón correctamente colocados.

**5.** Tener cumplimentado o cumplimentar el procedimiento que el Ministerio de Salud determine a los fines de acreditar que no tienen síntomas de padecer Covid-19, lo que deberán acreditar o cumplimentar a simple requerimiento de las autoridades competentes.

## **CONTROL A CARGO DE LA EMPRESA**

El decreto aclara además que el control del cumplimiento de los requisitos mencionados será responsabilidad de la empresa que brinda el servicio de transporte.

Aquellas personas que no cumplan con los requisitos solicitados por la Provincia de Santa Fe no podrán iniciar el viaje desde la localidad de origen.

Al ingreso a la provincia, el viajante deberá adjuntar toda la documentación que respalde el motivo de su solicitud cumpliendo con los requisitos desarrollados en el presente y los que en el futuro se establezcan.

## **CUÁLES SON LOS RECORRIDOS QUE SE HABILITAN**

Se trata de los **trayectos de más de 60 kilómetros desde la ciudad capital**. En este sentido, Leandro Solito presidente de ATAP, detalló al móvil de LT10 los servicios que vuelven a funcionar:

* **Santa Fe –  Rosario**
* **Santa Fe – Reconquista.** Tanto los que pasan por la costa (San Javier, Romang, Alejandra), como hacia el oeste (San Cristóbal, Ceres, Villa Minetti).
* **Santa Fe – Rafaela**

Además, **se habilitan los trayectos desde Santa Fe por ruta 19 y luego la 34, que unen las localidades de los departamentos Castellanos y General San Martín**.

También aclaró que, si estos servicios atraviesan ciudades y comunes, deben contar con el permiso de las autoridades locales para transitar por esas localidades.

Por otro lado, si bien consideró que la habilitación es una “buena noticia”, el escenario es completamente distinto al que existía en marzo, previo a las restricciones por la pandemia de Covid-19. “El escenario económico y financiero en el que las empresas deben volver a trabajar es muy crítico”, indicó.

Por lo que, a pesar de la resolución gubernamental, habrá que esperar para ver de qué manera las compañías podrán cumplir con todas las medidas necesarias para volver a circular.

![](https://assets.3dnoticias.com.ar/micros.jpeg)

## **QUÉ DEBES TENER EN CUENTA PARA VIAJAR EN MICRO**

* ### CAPACIDAD Y DISTANCIAMIENTO

La ocupación máxima de los micros será de 37 pasajeros para los coches semicama, 26 para “Cama ejecutivo” y 18 para “Cama suite”.

Además, se recomienda que las unidades utilicen un elemento de separación física que divida a los pasajeros de los choferes y que sea de un material transparente para no afectar la visibilidad no la seguridad a la hora de brindar el servicio.

* ### HIGIENE Y CONTROL

Se tendrá que proveer de tapabocas a conductores y empleados. A su vez, se deberá tomar la temperatura de los trabajadores, tripulantes y pasajeros, y cumplir con estrictas normas de ventilación, desinfección y limpieza de espacios comunes y sanitarios.

* ### USO DEL AIRE ACONDICIONADO

Tendrá que funcionar en modo recirculación y cumplir con las 20 renovaciones de aire por hora en toda la unidad. Además, se deberán mantener las ventanillas abiertas durante todo el viaje, aunque el sistema esté encendido.

## CONDICIONES Y REQUISITOS PARA CIRCULAR

Tanto las empresas como los pasajeros deberán cumplir con ciertos requisitos para poder viajar.

* **Empresas:** Las empresas deben: ajustarse al cronograma de frecuencias, los horarios y modalidades de prestación de los servicios que defina la Secretaría de Transporte, en base a las medidas de prevención dispuestas por el Ministerio de Salud; y limitar el traslado a las personas que se encuentren habilitadas para circular.
* **Pasajeros**: Los pasajeros, sean residentes o no residentes, al ingresar a la provincia de Santa Fe, deben cumplir con los siguientes requisitos:

  1. Portar el certificado único habilitante para circulación y toda documentación requerida. 
  2. Realizar la declaración jurada de salud en la app Covid-19 de la Provincia de Santa Fe y cumplimentar los formularios relacionados.

     **Además, las personas no residentes en la provincia de Santa Fe, deben cumplimentar los siguientes requisitos:**
  3. Informar el viaje interprovincial en la app COVID-19 de la Provincia de Santa Fe o en el sitio: http://www.santafe.gob.ar.coronavirusweb/formulario_viaje_interprovincial.
  4. Permanecer durante los primeros catorce (14) días, o todo el período si el plazo de permanencia fuere menor, en sus domicilios o lugares de residencia observando aislamiento social, preventivo y obligatorio y la prohibición de circular, limitando los desplazamientos a los estrictamente necesarios relacionados con la actividad que motivó su ingreso a la Provincia, o por razones alimentarias, de salud o de fuerza mayor o a las actividades habilitadas; siendo obligatorio en tales casos el uso de elementos de protección de nariz, boca y mentón correctamente colocados.
  5. Tener cumplimentado o cumplimentar el procedimiento que el Ministerio de Salud determine a los fines de acreditar que no tienen síntomas de padecer Covid-19, lo que deberán acreditar o cumplimentar a simple requerimiento de las autoridades competentes.

El cumplimiento de los requisitos y medidas será responsabilidad de cada empresa que brinda el servicio de transporte.

Asimismo, las personas que no cumplan con los requisitos solicitados por la Provincia de Santa Fe no podrán iniciar el viaje desde la localidad de origen.