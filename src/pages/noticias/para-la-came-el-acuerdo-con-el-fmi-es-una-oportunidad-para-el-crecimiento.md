---
category: Estado Real
date: 2022-01-28T16:57:58-03:00
thumbnail: https://assets.3dnoticias.com.ar/35porciento.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3d noticias
resumen: Para la CAME el acuerdo con el FMI es una "oportunidad para el crecimiento"
title: Para la CAME el acuerdo con el FMI es una "oportunidad para el crecimiento"
entradilla: Para la Mediana Empresa, el acuerdo constituye "una oportunidad para el
  crecimiento del sector productivo del país"

---
La Confederación Argentina de la Mediana Empresa (CAME) consideró hoy positivo el principio de acuerdo con el Fondo Monetario Internacional anunciado por el Presidente y remarcó que constituye "una oportunidad para el crecimiento del sector productivo del país".  
 El titular de la entidad, **Alfredo González**, sostuvo que "el anuncio que realizó el jefe de Estado nos permite pensar en un entendimiento que no postergue las posibilidades reales de las pequeñas y medianas empresas".  
 "La Argentina necesitaba alcanzar un acuerdo con el organismo de crédito para lograr un horizonte de certidumbre en el marco de una profunda crisis económica que atraviesa el país", agregó **González** poco después del discurso presidencial.  
 Al mismo tiempo, la entidad gremial empresaria manifestó que "el país se encuentra una vez más ante la posibilidad de unir esfuerzo para salir de la crisis, con un camino de diálogo, entendimiento y compromiso de todos los sectores y el acuerdo con el Fondo constituía un paso imprescindible en ese sentido".