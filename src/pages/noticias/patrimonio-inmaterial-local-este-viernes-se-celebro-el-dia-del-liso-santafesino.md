---
category: La Ciudad
date: 2021-12-11T06:00:35-03:00
thumbnail: https://assets.3dnoticias.com.ar/LISOLISTO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Patrimonio inmaterial local: este viernes se celebró el “Día del liso santafesino”'
title: 'Patrimonio inmaterial local: este viernes se celebró el “Día del liso santafesino”'
entradilla: La Municipalidad de Santa Fe acompañó las celebraciones en un bar ubicado
  frente a Plaza 25 de Mayo con un puesto turístico móvil para difundir este modo
  tradicional de beber cerveza.

---
En un bar frente a la Plaza 25 de Mayo, este viernes Santa Fe Capital le rindió culto al liso, ese modo singular de beber cerveza que es patrimonio inmaterial de la ciudad desde 2014 por una Ordenanza del Concejo Municipal. La Municipalidad de Santa Fe acompañó la celebración con la instalación de un puesto turístico móvil en el que se brindó a quienes se acercaban material informativo y un vaso de liso que lleva impreso Puente Colgante, uno de los íconos máximos de la capital.

El secretario de Educación y Cultura, Paulo Ricci, participó del evento junto con su par de Producción y Desarrollo Económico, Matías Schmüth, y el director de Turismo, Franco Arone. El funcionario destacó que el liso “es un patrimonio no solamente gastronómico sino también cultural de la ciudad de Santa Fe, que es conocida en la región y en el país por esta forma tan singular de tomar la cerveza, tan tradicional y tan instalada en nuestra cultura”.

Ricci precisó además que desde el municipio “queremos instaurar el Día del Liso Santafesino como una fecha obligada en el calendario cultural, turístico y gastronómico de la ciudad de Santa Fe el segundo fin de semana de diciembre, para que se acerquen los santafesinos y santafesinas, y los visitantes de la región y del país a probar esto tan singular y tan rico que es el liso bien servido, bien fresco y bien servido”.

**Con impronta turística**

Franco Arone recordó la reciente participación de Santa Fe Capital en la Feria Internacional de Turismo, en el que el liso tuvo un rol preponderante: “Uno de los distintivos que llevamos fue el vaso de liso y hoy estamos con un punto de promoción turística entregando merchandising, porque es importante que los santafesinos recuperemos estas tradiciones, nuestros sabores típicos, y los mostremos al resto del país”.

Gabriel Andruszczyszyn fue uno de los organizadores del evento de este viernes, en el que se rindió culto al liso. En la ocasión, expresó que “nosotros somos guardianes de la cultura cervecera tratando de mantenerla viva y sintiéndonos respaldados por las autoridades locales que entendieron que esto va más allá de cualquier bandería, es una forma que nos identifica y es un acervo cultural solamente santafesino”, aseguró.

El artista Oscar Pecorari, más conocido como “Peco”, fue el autor de una placa que hoy quedó descubierta para inmortalizar este día con una obra hecha con la técnica de fileteado porteño. Él se mostró muy feliz de compartir su trabajo: “Me siento muy honrado de que me hayan elegido para hacer la placa para que quede plasmado el día del liso santafesino. El liso, como el Puente Colgante son cosas muy nuestras”.