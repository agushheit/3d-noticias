---
category: La Ciudad
date: 2021-04-19T06:14:40-03:00
thumbnail: https://assets.3dnoticias.com.ar/cominitaria.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'Leandro González: "Nuestros barrios tienen que recuperar presencia de la
  policía comunitaria"'
title: 'Leandro González: "Nuestros barrios tienen que recuperar presencia de la policía
  comunitaria"'
entradilla: El presidente del Concejo Municipal planteó la necesidad de fortalecer
  estrategias policiales de proximidad y cercanas a los vecinos de los barrios de
  la ciudad.

---
La posibilidad de recuperar y fortalecer la intervención de la policía comunitaria en distintos barrios de Santa Fe, fue una de las propuestas que el presidente del Concejo Municipal, Leandro González, le planteó al ministro de Seguridad en el marco de una nueva reunión del Consejo de Seguridad Urbana.

“La policía comunitaria tiene que recuperar protagonismo operativo en los barrios de Santa Fe. Cuando se creó en 2013, esta estrategia de policiamiento comunitario llegó a funcionar en 9 barrios de la ciudad y había sido muy bien recibida por las instituciones, las organizaciones sociales, los comerciantes y los vecinos”, expresó Leandro González.

En este sentido, el presidente del Concejo Municipal recordó que la policía de seguridad comunitaria fue impulsada y desarrollada en el marco del Plan de Seguridad Democrática, con la idea de innovar y ampliar las capacidades operativas de la Policía de Santa Fe desde nuevos paradigmas que permitan optimizar los servicios policiales.

“La policía comunitaria tiene un enfoque preventivo a partir del diálogo con actores vitales de un barrio determinado, que le permite conocer mejor la vida social y las dinámicas de ese territorio. Es importante que los policías construyan un vínculo con el comerciante, la directora de la escuela, el coordinador del distrito municipal, el dirigente de un club, y los vecinos”, explicó el concejal del Frente Progresista.

“Este paradigma de policiamiento de proximidad permite mejorar los vínculos y los canales de diálogo entre la policía y la comunidad; genera mayores condiciones para la prevención de las conflictividades sociales y del delito, a partir del diseño de intervenciones focalizadas; y además aumenta la confianza de las personas en la policía”.

A su vez, González destacó que “el diálogo y el contacto permanente es el que a las fuerzas de seguridad le puede permitir anticiparse a los delitos, pero, sobre todo, a los conflictos sociales que pueden derivar en situaciones de violencia altamente lesiva”.

**Consejo de Seguridad Urbana**

Hace unos días, y convocada por el intendente Emilio Jatón, se desarrolló una nueva reunión del Consejo de Seguridad Urbana en la que participaron autoridades del Ministerio de Seguridad de la Provincia, el Ministerio Público de la Acusación, la Policía de la Provincia y representantes de fuerzas federales de seguridad, funcionarios de la Municipalidad de Santa Fe, concejales y concejalas, e integrantes de las vecinales de la ciudad.

En el marco de esta nueva reunión y de las medidas preventivas de seguridad pública discutidas con los vecinos y vecinas que participaron, Leandro González le propuso al ministro de Seguridad de la provincia, Jorge Lagna, la necesidad de recuperar estrategias de la vigilancia comunitaria. Iniciativa que, además, fue presentada como proyecto de comunicación.

“La reunión con el ministro fue muy buena, quedó clara la predisposición de la cartera de mejorar y trabajar en forma articulada con el municipio, el Concejo, el MPA y las vecinales. Está claro que la policía y la justicia tienen un rol importante, pero para abordar las complejidades de los delitos y de las violencias que están generando mucha preocupación creemos que es necesario desarrollar políticas públicas inteligénciales junto con la ciudadanía. Es en esta línea que proponemos recuperar la capacidad operativa de la policía comunitaria como una de las medidas para llevar tranquilidad a los barrios”, finalizó González.