---
category: Agenda Ciudadana
date: 2022-01-09T06:00:13-03:00
thumbnail: https://assets.3dnoticias.com.ar/precios.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: El Gobierno se reúne con empresas de alimentos para lanzar la nueva etapa
  de Precios Cuidados
title: El Gobierno se reúne con empresas de alimentos para lanzar la nueva etapa de
  Precios Cuidados
entradilla: 'La nueva canasta, que también incluirá artículos de consumo masivo, tendrá
  1.300 artículos que aumentarán un 2% mensual durante el primer trimestre.

'

---
El Gobierno se reunirá el lunes con empresas productoras de alimentos y artículos de consumo masivo para firmar un nuevo convenio para volver a poner en funcionamiento el programa Precios Cuidados, que en esta ocasión contará con unos 1.300 productos y aumentos promedio de 2% mensual.

De esta manera, la canasta de productos se duplicará respecto de la anterior versión del clásico programa con el que se busca reducir el impacto de la inflación. Además, el valor de los artículos incluidos en esta edición de Precios Cuidados se irá actualizando durante el primer trimestre un 2% mensual.

El programa venció el pasado viernes, cuando concluyó el congelamiento de 700 productos desde el 20 de octubre. La firma de este nuevo acuerdo fue producto de las negociaciones que encabezó el secretario de Comercio Interior, Roberto Feletti, con los empresarios.

"Hay predisposición al diálogo y el Gobierno plantea que busca garantizar una canasta amplia, diversa, con precios accesibles y con bienes de calidad, siempre representativa del consumo promedio de los argentinos", señalaron en su entorno.

Pese a que no se rubricó formalmente el acuerdo de precios, algunos de los nuevos productos incluidos en esta etapa pudieron verse en las góndolas. El acuerdo alcanzará tanto a supermercados como almacenes y habrá un precio diferencial por tamaño de comercio, debido a que durante la etapa del congelamiento hubo dificultades en los comercios de proximidad para garantizar el margen de rentabilidad.

El programa Precios Cuidados fue creado a finales de 2013 durante la gestión de Cristina Kirchner al frente de la Casa Rosada: el objetivo era -y es- contener la inflación en los productos de la canasta básica.