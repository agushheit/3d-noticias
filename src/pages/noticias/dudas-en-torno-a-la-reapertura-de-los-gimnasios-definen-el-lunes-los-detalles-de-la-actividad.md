---
category: La Ciudad
date: 2021-06-13T06:15:33-03:00
thumbnail: https://assets.3dnoticias.com.ar/GYM.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Dudas en torno a la reapertura de los gimnasios: definen el lunes los detalles
  de la actividad'
title: 'Dudas en torno a la reapertura de los gimnasios: definen el lunes los detalles
  de la actividad'
entradilla: Pese al nuevo decreto provincial con las actividades permitidas, existe
  un alto grado de incertidumbre sobre el desarrollo de la actividad física en espacios
  cerrados.

---
Tanto jueves como viernes, referentes de distintas actividades deportivas hicieron sentir su disconformidad con protestas en los ingresos a la ciudad de Santa Fe por la imposibilidad de abrir centros de entrenamientos, gimnasios y distintos espacios para la actividad física. En ese contexto, fueron recibidos por referentes del gobierno santafesino y les propusieron aguardar el decreto provincial con las nuevas medidas de convivencia en el marco de la pandemia por Covid-19, que finalmente salió este viernes por la noche.

El decreto provincial Nº 848 con fecha del 11 de junio, en el marco de las restricciones y medidas sanitarias por la pandemia de coronavirus, establece en su artículo 5, inciso C, en relación a las actividades que quedan permitidas en todo el territorio provincial desde la cero (0) hora del día 14 y hasta el 25 de junio de 2021, lo siguiente: gimnasios, natatorios y establecimientos afines en espacios al aire libre, por turnos y en grupos de hasta diez (10) participantes.

Pese a esta situación, referentes de gimnasios esperan que el lunes desde la municipalidad los habiliten a trabajar con un aforo reducido de personas en espacios cerrados. Fuentes municipales indicaron a UNO Santa Fe que este lunes comenzarán a dialogar con el gobierno provincial para analizar la situación puntual de la actividad en los gimnasios de la ciudad, que por lo establecido en el decreto, solamente podrán dar clases con turnos de 10 personas únicamente en espacios al aire libre.

A mediados de semana, un duro comunicado emitido por la Cámara de Gimnasios de la ciudad, dio cuenta de la "delicada" y "crítica" situación que vive el sector desde lo económico y social. "En estos momentos no consideramos una buena decisión negarnos el derecho a trabajar, sobre todo, después de los ocurrido el fin de semana con los festejos en las calles de Santa Fe", comenzaron argumentando del sector. "Decidieron cerrar nuestra posibilidad de trabajar a pesar de los estrictos protocolos bajo los que nuestras actividades venían funcionando; pero adherimos a la directiva provincial, aunque nunca, en nuestros 150 días de cierre desde el inicio del confinamiento allá por el mes de marzo del 2020, buscamos salirnos de la norma".

En este contexto, desde la Cámara de Gimnasios solicitaron de forma urgente la reapertura a ambos gobiernos, tanto provincial como municipal. El pedido es concreto: reabrir a partir del lunes 14 de junio con el protocolo adecuado a las recomendaciones nacionales de prevención de transmisión por aerosoles en el marco de la campaña "ventilar", garantizando en los locales la correcta ventilación de los espacios y la calidad óptima del aire, mediante el uso de la ventilación cruzada, el uso de medidores de co2 (dióxido de carbono), con un sistema de trabajo con aforo a un 30 por ciento y turnos rotativos de hasta 10 personas en espacios cerrados, manteniendo las medidas de seguridad e higiene.

Habrá que esperar hasta el lunes para entender, independientemente de la claridad del decreto provincial, que pasará con las actividades en los gimnasios de la ciudad, si abrirán sus puertas pese a la habilitación para realizar sus funciones en espacios al aire libre. Por otro lado, los referentes autoconvocados del sector, amenazan con nuevos cortes y manifestaciones en las calles de la ciudad de Santa Fe.