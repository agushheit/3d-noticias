---
layout: Noticia con imagen
author: Nota de la redacción de 3DNoticias
resumen: Obras en terraplén Garello
category: Estado Real
title: La provincia licitó la obra de ensanchamiento del terraplén Garello
entradilla: Se presentaron 10 oferentes para realizar los trabajos de
  engrosamiento del terraplén y mejorar las condiciones de seguridad de la
  defensa ante eventuales crecidas. Las tareas demandarán unos tres meses de
  ejecución.
date: 2020-11-21T12:09:13.746Z
thumbnail: https://assets.3dnoticias.com.ar/apertura-licitaci%C3%B3n.jpg
---
El Ministerio de Infraestructura, Servicios Públicos y Hábitat, a través de la Secretaría de Recursos Hídricos, realizó la **apertura de sobres para ejecutar la obra de ensanchamiento del terraplén de la Defensa Garello**, en el sector de la toma de agua y del tramo colapsado de la protección.

Con esta acción se logrará una sección del terraplén más robusta, con un coronamiento más amplio, lo que contribuirá a mejorar la seguridad de la defensa ante crecidas, hasta que se concrete la obra de estabilización de margen, de la cual, el ensanchamiento se constituye en la primera etapa.

En este sentido, el secretario de Recursos Hídricos, Roberto Gioria, brindó detalles de esta primera etapa de la obra definitiva de la protección del terraplén: “Hasta que se concrete la obra principal del terraplén, **lo que hacemos en esta primera etapa es actuar dando una respuesta en el corto plazo, aumentando el cuerpo del terraplén**. Esto consiste en un ensanchamiento en la zona colapsada, aumentando la seguridad del terraplén ante alguna crecida del río Paraná que pudiese venir en el mientras tanto. Al coronamiento se lo va a ensanchar alrededor de 12 a 15 metros según el lugar”.

Más adelante el secretario Gioria informó sobre los estudios hidrológicos que se están realizando para la elaboración del proyecto ejecutivo de la protección completa: “Paralelamente a esta obra, se están realizando estudios hidrológicos, topográficos, batimétricos, y mediciones de caudales y velocidades, que permitirán la elaboración del proyecto ejecutivo para la obra completa. De esta forma se planteará una nueva obra en el talud húmedo del terraplén donde se encuentra un muelle perteneciente a la estructura de la toma de agua cruda, que es una de las dos tomas que tiene la empresa Aguas Santafesinas SA (ASSA), con el objetivo de lograr la estabilización del terraplén, cuyo proyecto se está desarrollando con el financiamiento del Consejo Federal de Inversiones (CFI).”

Por su parte, el senador provincial Marcos Castelló comentó el impacto de esta obra para los vecinos de la costa: “La verdad que es un placer poder comunicar este tipo de noticias que tienen que ver con pagar una vieja deuda que siempre los vecinos lo hemos charlado en diversas reuniones. El Garello claramente ha cobrado una importancia tremenda y creo que esta licitación lo que busca, en esta primera etapa, es el ensanchamiento y lo que sería la protección de toda la zona. Así que **consideramos que tiene una importancia altísima, sobre todo para los vecinos y para quienes conocemos de la problemática en la zona, y que hoy se empieza a saldar esta vieja demanda**”.

![](https://assets.3dnoticias.com.ar/apertura-licitación1.jpeg)

## **LAS OFERTAS**

Con un presupuesto oficial de 36.693.366,48 millones de pesos, se presentaron 10 ofertas: MUNDO CONSTRUCCIONES S.A. presentó una propuesta económica por la suma de $ 30.362.047,85; FERRER S.A. cotizó $ 33.720.560,96; PONCE CONSTRUCCIONES S.R.L. cotizó $ 31.108.739,99; ING. CIVIL PEDRO A. MARTÍNEZ CONSTRUCCIONES cotizó $ 29.636.335,38; COCYAR S.A. cotizó $ 34.355.255,12; ALANCO S.A. cotizó $ 27.861.520,00; RÍO SALADO S.R.L. cotizó $ 25.686.177,93; EMPRESA CONSTRUCTORA PILATTI cotizó $ 29.352.412,89; COEMYC S.A. cotizó $ 44.620.805,21 y presentó una oferta alternativa por $ 39.629.588,41; y una última oferta correspondiente a la empresa EFE CONSTRUCCIONES DE CARLOS ALBERTO FIERRO que cotizó la suma de $ 32.400.000,00.



## **LOS TRABAJOS**

Las tareas que deberán realizarse corresponden a limpiar la zona de obra extrayendo la vegetación del paramento seco y removiendo el suelo cemento del canal de guarda en caso de que exista, luego se efectuará una ampliación del cuerpo del terraplén sobre el paramento seco y coronamiento de la obra de defensa contra inundaciones con compactación. Posteriormente se recubrirá con suelo humífero, y se ejecutarán tareas para reconstruir el canal de guarda en un tramo y recubrirlo con suelo cemento con drenes, y reposición y/o construir el alambrado en caso de ser necesario.

## **PRESENTES**

Acompañaron el acto licitatorio, el secretario Andrés Dentesano, el concejal por la ciudad de San José del Rincón, Facundo Amati, y un representante de cada empresa oferente.