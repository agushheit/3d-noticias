---
category: Agenda Ciudadana
date: 2020-11-27T11:11:43Z
thumbnail: https://assets.3dnoticias.com.ar/aborto.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: NA'
resumen: IVE en Diputados
title: Diputados definió el cronograma para el tratamiento del proyecto de interrupción
  voluntaria del embarazo
entradilla: La Cámara de Diputados llevará a sesión especial el proyecto de interrupción
  voluntaria del embarazo (IVE) el próximo jueves 10 de diciembre.

---
En una reunión por videoconferencia que encabezó el presidente de la Cámara de Diputados, Sergio Massa, con los presidentes y vicepresidentes de las cuatro comisiones que intervendrán en el tratamiento de la iniciativa de interrupción voluntaria del embarazo (IVE), se acordó una agenda de trabajo intensa y comprimida para la semana que viene, con jornadas que empezarán a las 10:00 y culminarán a las 19:00, al tiempo que el viernes se prevé la firma de dictamen.

"Es velocidad, pero con intensidad", aclararon a NA las fuentes legislativas consultadas.

Si se mantiene este cronograma y se lograra la media sanción dentro de dos semanas, el Senado podría tratarla la semana siguiente: el objetivo del sector a favor de la legalización del aborto es convertir en ley la iniciativa antes de Navidad.

En martes a las 10:00 será la primera reunión informativa del plenario conjunto de las comisiones de Legislación General, Salud, Mujeres y Diversidad y Legislación Penal, con la presencia por la mañana de los ministros Ginés González García (Salud) y Daniel Arroyo (Desarrollo Social), además de la secretaria Legal y Técnica y autora del proyecto del Poder Ejecutivo, Vilma Ibarra.

En paralelo, se tratará el proyecto de los "1.000 días" en un plenario de las comisiones de Salud y Presupuesto, que tendrá su primera reunión informativa el lunes que viene a las 10:00, mientras que se espera la firma del dictamen para el jueves 3 de diciembre.

En la reunión de este jueves se consensuó que para el debate de la legalización del aborto en el plenario de comisiones habrá un total de 50 expositores repartidos equitativamente entre el sector "verde" a favor del aborto legal y el "celeste" autodenominado "pro vida".

"Lo bueno de esta etapa, en la que habrá menos expositores que hace dos años, es que va a haber un parámetro de calidad porque se van a elegir a los más destacados", indicó a NA una diputada nacional.

Se replicará el modelo que se utilizó en el debate del 2018 cuando cada disertante tuvo siete minutos para desarrollar sus argumentos, al tiempo que también se dispondrán unos minutos adicionales para que los diputados puedan hacerles preguntas.

Los expositores estarán divididos en cuatro grupos por especialidad (científicos, sanitaristas médicos, ético religiosos y juristas) y se irán turnando.

En el encuentro de esta tarde participaron, además de Massa, la presidenta de la comisión de Legislación General (que será cabecera del debate), Cecilia Moreau, de Salud, Pablo Yedlin, de Mujeres y Diversidad, Mónica Macha, y de Legislación Penal (Carolina Gaillard).

También concurrieron las vicepresidentas de las mismas comisiones, todas ellas en representación de Juntos por el Cambio: Carla Carrizo (Legislación General), Carmen Polledo (Salud), Silvia Lospennato (Mujeres y Diversidad) y Marcela Campagnoli (Legislación General).

Dos diputados que participaron del cónclave resaltaron a NA que al igual que en la reunión del martes pasado hubo "un clima ameno" que permitió que se fijaran "reglas claras" y un "muy buen ambiente de trabajo" incluyendo a Polledo y Campagnoli, las únicas dos detractoras de la ley de este grupo operativo que conducirá el debate.

Como ya se había adelantado, en esta segunda reunión se ratificó el compromiso de denunciar penalmente en la Justicia todo "escrache" o expresión de amenaza contra legisladores que pudiera afectar el "derecho a opinión" de los diputados.

"Será una sesión muy larga, que podría ser de 20 horas. Queda por discutir la modalidad de sesión", señalaron a NA altas fuentes legislativas de la oposición.

Juntos por el Cambio viene pujando, y de hecho así lo transmitió a través de una carta formal a Massa el miércoles pasado, para que el oficialismo acceda a retomar la presencialidad de las sesiones, por lo que las próximas horas de negociación serán decisivas.

De todos modos, los ánimos no parecieran orientarse hacia una conflictividad severa ya que hay una voluntad general de no demorar el tratamiento, y lo más probable es que pueda acordarse sin dificultad un esquema de mayor presencialidad con la posibilidad de que cada bloque arme una lista de la cantidad de diputados que por factores de riesgo unas otras razones participarán de forma remota.

Sobre la mesa de negociación existe una propuesta para que se adopte un sistema de rotación, como el que se utiliza en otros países como Estados Unidos, por el cual la mayoría de los diputados asista presencialmente al Congreso el día de la sesión, pero participe desde sus despachos a excepción del momento de dar el discurso, cuando tendrán que ocupar sus lugares en las bancas.

"Hay que encontrar equilibrio entre el derecho a la presencialidad y no descuidar la salud, ya que el recinto es un espacio cerrado donde no circula mucho el aire", señalaron.