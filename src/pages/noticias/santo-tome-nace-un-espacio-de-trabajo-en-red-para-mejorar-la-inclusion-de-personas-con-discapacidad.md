---
category: La Ciudad
date: 2021-09-23T06:00:44-03:00
thumbnail: https://assets.3dnoticias.com.ar/gon1.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gonzalez
resumen: 'Santo Tomé: Nace un espacio de trabajo en red para mejorar la inclusión
  de personas con discapacidad'
title: 'Santo Tomé: Nace un espacio de trabajo en red para mejorar la inclusión de
  personas con discapacidad'
entradilla: " La reunión fue convocada y organizada por la concejala local Florencia
  González, quien señaló que “reflexionar juntos y trabajar en red es fundamental,
  sobre todo en este contexto de pandemia.”"

---
El encuentro tuvo lugar en las instalaciones de Casa Club, y fue coordinado por la edil santotomesina Florencia González, y por las Licenciadas en Terapia Ocupacional Soledad Santa Cruz y Macarena Santa Cruz, que forman parte de su equipo. La idea central: pensar la inclusión desde el trabajo en red y generar un espacio de reflexión sobre el impacto de la pandemia en personas con discapacidad y trastorno.

“Trabajar por la inclusión significa primero saber escuchar, por eso convocamos a familiares e instituciones que son quienes de mejor manera nos pueden indicar la situación que atraviesan y sus necesidades” expresó González, y agregó: “sabemos que aún queda muchísimo por hacer. Ojalá éste sea el primer encuentro de muchos porque queremos ayudarlos y aportar nuestro granito de arena.”

En la reunión estuvieron presentes -entre otros-, representantes de ALPI, TDA, TDAH, CEA - padres unidos, del Centro de Día y el Taller protegido ADALPE, y Cromosoma 21 Inclusivo. Según indicó la concejala: “Queríamos cada uno conozca las iniciativas y el trabajo del otro, y fundamentalmente sus experiencias en el marco de pandemia y sus próximos desafíos. Abordamos realidades y preocupaciones de cómo está pensado el sistema educativo y laboral, y cómo podemos desde ese ámbito incluir, acompañar y brindar oportunidades a las personas con discapacidad y trastornos.”

Finalmente, Florencia González señaló que van a seguir trabajando desde su espacio para garantizar estos espacios de intercambio y de contención.![](https://assets.3dnoticias.com.ar/gon2.jpg)