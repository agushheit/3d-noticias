---
category: Agenda Ciudadana
date: 2020-12-21T14:13:20Z
thumbnail: https://assets.3dnoticias.com.ar/2112turismosantafe.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Diario UNO'
resumen: Santa Fe abre sus puertas para la llegada de los turistas
title: Santa Fe abre sus puertas para la llegada de los turistas a la provincia
entradilla: Tal como se anunció a través del Decreto Nº 1.640, este lunes 21 cualquier
  argentino que quiera hacer turismo en nuestra provincia, ya está habilitado. Cuáles
  son los requisitos.

---
El gobierno de la provincia de Santa Fe dispuso, a través del Decreto Nº 1.640, un cronograma progresivo de la vuelta a la actividad turística en el territorio santafesino. 

La norma estableció que el 5 de diciembre pasado se habilitara el turismo para residentes y propietarios de inmuebles en la provincia, quienes podrán visitar otras regiones del distrito. 

Esto coincidió con el último fin de semana extralargo en Argentina, lo que dejó un balance positivo en la región, sobre todo para los cabañeros, quienes en su momento manifestaron que la ocupación era casi del 100 por ciento, algo que los alentaba y presagiaba un buen futuro.

Ahora, y según el mismo calendario que se dio a conocer, es el turno, a partir de este lunes 21, del arribo a la provincia de turistas que provengan de toda la Argentina. En su momento, el secretario de Turismo de la provincia, Alejandro Grandinetti manifestó: «Desde hace un mes que estamos promocionando el destino en los principales medios de la República Argentina». E indicó: «Santa Fe tienen que competir, como lo hace en otros rubros. Somos una provincia grande, importante, y lo demostramos en la generación de divisas para la Argentina; nos faltaba poner este motor en marcha».

Destacó además: «Fuimos la segunda provincia de Argentina que obtuvo una distinción por nuestros protocolos de seguridad para desarrollar un turismo seguro para todos aquellos que nos vengan a visitar»; y agradeció «a todos los trabajadores y empresarios que tuvieron la templanza para llevar estos meses de Covid-19, y volver a apostar a un proyecto de turismo; incluso con los hoteles cerrados, se estuvieron realizando inversiones, porque ven que hay una decisión estratégica de tener turismo todo el año».

Para poder hacer turismo en la provincia se debe registrar en la aplicación móvil «Covid-19» de la provincia de Santa Fe y completar con carácter de declaración jurada los datos que se requieren en la misma. Además, tendrán que obtener el certificado de circulación disponible en [www.argentina.gob.ar/verano](https://www.argentina.gob.ar/verano); y acreditar la reserva de un servicio o alojamiento turístico autorizado.

# **Cómo gestionar el Certificado Verano**

►En la web [https://www.argentina.gob.ar/verano](https://www.argentina.gob.ar/verano "https://www.argentina.gob.ar/verano") ingresar en  «¿Todavía no lo tenés? Sacalo ahora».

►Hay que seleccionar el destino y señalar si el pasajero tiene DNI argentino o no.

►Se deben completar los datos personales: nombre y apellido, DNI, domicilio, teléfono celular, correo electrónico y un contacto de emergencia.

►Para añadir los datos del grupo familiar están las opciones  «Mayor de 13 años que usa la app Cuidar» y  «Menor de 13 años o mayor que no utiliza la app Cuidar». Luego hay que indicar el vínculo o parentesco con cada persona y sus nombres y apellidos, fechas de nacimiento, celulares y DNI.

►Se debe consignar el tipo de transporte a utilizar en el traslado, ya sea ómnibus, vehículo propio o cualquier otro medio. También pide datos del hospedaje y las fechas de estadía.

►Una vez enviada la solicitud, la respuesta se recibirá dentro de las siguientes 48 horas.

►Concluido el trámite, puede descargarse desde la página de inicio con el número de DNI, aunque también se puede presentar desde el correo electrónico (tiene la misma validez que el certificado impreso) y en la app Cuidar.