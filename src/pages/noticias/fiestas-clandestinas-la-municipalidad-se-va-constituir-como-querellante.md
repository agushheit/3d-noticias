---
category: La Ciudad
date: 2020-12-31T10:46:00Z
thumbnail: https://assets.3dnoticias.com.ar/3112-jaton.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: 'Fiestas clandestinas: la Municipalidad se va constituir como querellante'
title: 'Fiestas clandestinas: la Municipalidad se va constituir como querellante'
entradilla: Se incrementarán los controles de alcoholemia para la celebración del
  Año Nuevo. Las multas pueden superar los $ 80.000.

---
El intendente Emilio Jatón se refirió a las reuniones no autorizadas que se realizan en la ciudad. En este sentido, el mandatario anticipó que además de intensificar los controles e incrementar las multas, **el municipio se constituirá como querellante y será partícipe principal en el proceso de denuncias ante el Ministerio Público de la Acusación** (MPA).

«Preocupa el comportamiento de algún sector de la población que a veces parece no entender el problema que tenemos. Por eso vamos a incrementar al doble las multas por las fiestas clandestinas, nos vamos a incorporar al proceso judicial y denunciaremos a las personas que las hagan y a los dueños de los domicilios, pero además vamos a sumar más controles de alcoholemia», dijo el intendente.

Vale recordar que las multas que corresponden por desobediencia del Art. 16 bis (Ordenanza Nº 7882 y sus modificatorias) más por ruidos molestos y bailes o espectáculos no habilitados, pueden superar los 80.000 pesos. El valor de la sanción establecido en dicho artículo referido al incumplimiento de las medidas de prevención del Covid-19 es de 100 a 1000 UF (Unidad Fija).

«Entendemos que estos momentos son claves para poder frenar esa segunda ola de la pandemia y vamos a trabajar respecto a eso», resumió Jatón.