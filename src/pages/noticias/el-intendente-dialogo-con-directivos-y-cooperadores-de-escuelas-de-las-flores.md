---
category: La Ciudad
date: 2021-10-09T06:00:38-03:00
thumbnail: https://assets.3dnoticias.com.ar/LASFLORES.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: El intendente dialogó con directivos y cooperadores de escuelas de Las Flores
title: El intendente dialogó con directivos y cooperadores de escuelas de Las Flores
entradilla: 'Emilio Jatón fue recibido en la escuela Juan de Garay y aprovechó para
  anunciarles que hará la limpieza de un terreno ubicado en las inmediaciones, que
  lleva años de abandono. '

---

El intendente Emilio Jatón se reunió este jueves con directivos y cooperadores escolares de distintos establecimientos educativos ubicados en un sector del barrio Las Flores. Aprovechó para contarles que comenzó en pocos días la limpieza de terrenos baldíos ubicados en las inmediaciones de estas escuelas y que hoy están llenos de basura, escombros y malezas.

Esta intervención formará parte de una serie de trabajos que se harán en la zona como instalación de columnas con iluminación led que colaborarán con la seguridad, mejorado en las calles y otras tareas que permitirán una mejor accesibilidad entre los vecinos y la conexión de los barrios con avenidas troncales como Blas Parera y Peñaloza.

El secretario de Ambiente municipal, Edgardo Seguro, brindó más detalles sobre las buenas noticias que les llevaron a los vecinos de este sector de la ciudad: “Es un complejo educativo muy grande y tiene un problema de vieja data en una serie de terrenos linderos relacionado con la higiene urbana. Por una serie de obras que se hicieron en un momento se acumuló escombro y tierra que hizo que creciera la maleza y se llenara de basura”.

En esta línea, el intendente les anunció a los directivos, docentes y cooperadores escolares que en unos pocos días ingresarán las máquinas para limpiar ese sector. “Emilio tomó la decisión de sanear estos predios y eso va a permitir, junto a otras obras complementarias como apertura de calles, ripiado e iluminación, que el camión recolector ingrese, se mejore la higiene del lugar y así quedará un espacio para que se pueda concretar algún proyecto y disfruten las escuelas y los vecinos”, detalló Seguro.

Este espacio está ubicado hacia al este de Blas Parera, entre Millán Medina, Larrea y Regimiento 12 de Infantería. “En todo este sector no hay buena transitabilidad porque quedó aislada del resto del barrio por el zanjón norte y en estos terrenos se acumuló la basura. Ahora, con todo esto despejado, Obras Públicas podrá colocar la iluminación y después se podrá cortar el pasto, y mantener limpio para el uso de todo el barrio. En definitiva será una intervención integral”, agregó el secretario de Ambiente.

El funcionario municipal resaltó el hecho de que estos proyectos surgen de las reuniones con los vecinos y en este caso con las instituciones. “La limpieza y el trabajo con la comunidad es clave y muy importante. Por eso decidieron armar un cronograma de acciones y así empezar a usar este espacio entre todos, y que se mantenga”, señaló.

**Trabajo conjunto**

Por su parte, Mónica Godoy, directora de la escuela Nº 19 Juan de Garay, en representación de sus pares, destacó las buenas noticias para las instituciones y los vecinos de este barrio. “Hace mucho tiempo que las instituciones que formamos el complejo, estamos solicitando el alumbrado público y la limpieza de un predio ubicado detrás de la escuela que era un basural y hoy recibimos la buena noticia de que lo van a mejorar”, contó.

Siguiendo esta línea, agregó: “No solo van hacer la limpieza sino que van a mejorar la iluminación y eso traería transitabilidad, que es lo que también estamos necesitando. Pero además nos invitaron a seguir trabajando juntos y nos escucharon. Nos dimos cuenta que todas las firmas que juntamos con los cooperadores para poder hacer esto hoy tiene sus frutos, esto significa que todos los vecinos fueron escuchados y esperamos ahora que la primera quincena de noviembre ya podamos estar disfrutando de este lugar recuperado”.

Además, participaron de la reunión, la concejala Laura Mondino, representantes de las escuelas Juan de Garay, de la Primaria para Adultos Nº 2545 José Hernández, la de Enseñanza Media para Adultos Nº 1157 Julieta Lanteri; y la de Educación Técnica Nº 633 Centenario de Bolivia; el Centro de Educación Física (CEF) Nº 55 “Nehuen”; el Jardín de Infantes Nº 52 Juan Ramón Jimenez; y el Taller de Educación Manual Nº 158 Almafuerte.