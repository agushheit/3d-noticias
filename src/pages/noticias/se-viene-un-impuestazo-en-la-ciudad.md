---
layout: Noticia con imagen
author: Nota de la Redacción de 3DNoticias
resumen: Impuestazo en la ciudad
category: La Ciudad
title: Se viene un impuestazo en la ciudad
entradilla: Se trata de aumentos en la Tasa General de Inmuebles que van desde
  el 80 al 160% para el 2021.
date: 2020-11-24T11:47:34.805Z
thumbnail: https://assets.3dnoticias.com.ar/boelta-tgi.jpg
---
Semanas atrás, el intendente de la ciudad de Santa Fe envió al Honorable Concejo Municipal el proyecto de presupuesto para el próximo año donde, además de explicar cómo y en que se van a utilizar los recursos, deja planteada la política tributaria del municipio para el 2021.

Desde el bloque de Juntos por el Cambio sostienen que es un aumento excesivo y que el esfuerzo que se le pide a los vecinos es muy grande. Al respecto, Carlos Pereira declaró: “El intendente Jatón dejó en claro que va a mantener el esquema de aumentos de la TGI tal y como se realizó durante el 2020. El de este año fue un esquema claramente excesivo y si hacemos el cálculo con los porcentajes que se aplicaron, para enero de 2021 hay propiedades que van a pagar casi el doble de lo que pagaron en 2020”.

**Los aumentos oscilan entre el 75% y el 160% de acuerdo a la zona de la ciudad donde se ubiquen**. “Estas cifras claramente están entre 2 y 3 veces por encima de la inflación. Y esto se va a mantener durante todo el año. Pensemos que, en menos de 2 años de gestión, la tasa municipal aumentó más del doble y en algunos casos se triplicó y esto nos preocupa. En un año muy complicado, con una dura crisis económica producto de la pandemia, no nos parece justo que se les exija a los santafesinos un esfuerzo de tal magnitud y mucho menos que sean los contribuyentes quienes paguen la crisis de las finanzas públicas“, culminó Pereira.

![](https://assets.3dnoticias.com.ar/aumento-tgi.jpg)

![](https://assets.3dnoticias.com.ar/aumento-tgi1.jpg)

![](https://assets.3dnoticias.com.ar/aumento-tgi2.jpg)