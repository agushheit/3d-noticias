---
category: La Ciudad
date: 2021-01-20T09:10:58Z
thumbnail: https://assets.3dnoticias.com.ar/municipalidad.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad Santa Fe
resumen: 'Telefonía fija: la Municipalidad tomará reclamos en las vecinales'
title: 'Telefonía fija: la Municipalidad tomará reclamos en las vecinales'
entradilla: Profesionales del municipio llegarán este miércoles a la vecinal Fomento
  9 de Julio, el jueves a Libertad de Barranquitas y el viernes a San José, los barrios
  más afectados por la falta del servicio.

---
En los últimos días, la Municipalidad recibió una gran cantidad de reclamos de vecinos y vecinas de esta capital, por la falta del servicio de telefonía fija. Del mismo modo, se multiplicaron los pedidos de las vecinales, debido a que importantes sectores de algunos barrios no cuentan con la prestación.

Por el aumento de los pedidos y teniendo en cuenta que algunos usuarios están imposibilitados de trasladarse, la Municipalidad decidió iniciar una recorrida por los barrios que se vieron más afectados. De este modo, profesionales de la dirección tomarán los reclamos correspondientes en las vecinales.

En todos los casos, se atenderá de 10 a 12 horas, según el siguiente cronograma: el miércoles 20, en la vecinal Fomento 9 de Julio, el jueves 21 en Libertad de Barranquitas y el viernes 22 en San José. A los vecinos y vecinas que se acerquen, se les solicita que cuenten con el DNI y fotocopia, una factura del servicio y fotocopia, y los números de reclamo que posean.

El director de Derechos y Vinculación Ciudadana de la Municipalidad, Franco Ponce de León, informó que desde el área que encabeza, “hemos enviado notas tanto a la empresa de telefonía fija como también al Ente Nacional de Comunicaciones (Enacom) que es el organismo nacional encargado de controlar todo lo referente a telefonía”. Según detalló, “no sólo pedimos que se solucionen los inconvenientes rápidamente, sino que se establezca un resarcimiento para los clientes afectados, teniendo en cuenta que la falta de telefonía también origina inconvenientes en los servicios de Internet y el sistema de alarmas”.

Ponce de León mencionó que entre los perjudicados, hay casos en los cuales el teléfono no funciona desde hace semanas e incluso meses. Al respecto, recordó que “el reglamento de clientes de telefonía habla de un máximo de tres días hábiles para responder el reclamo, en caso de que haya algún tipo de interrupción por parte de la empresa”.

En los contactos establecidos por la Dirección de Derechos y Vinculación Ciudadana, la firma prestataria adujo inconvenientes relacionados con el robo de cables. Incluso confirmó la radicación de la denuncia pertinente en el Ministerio Público de la Acusación (MPA). Ante esto, el municipio solicitó por nota que se redoblen los esfuerzos tendientes a reponer las líneas, atendiendo a que se trata de un servicio público.

**Contacto**

Quienes padezcan inconvenientes en el servicio de telefonía fija, pueden contactarse con la Dirección de Derechos y Vinculación Ciudadana a través de las siguientes vías de comunicación: por teléfono, al 0800 444 0442; a través de un mensaje de texto o audio, en el WhatsApp 3425315450; por e-mail a derechosyvinculacionciudadana@santafeciudad.gov.ar, o en forma presencial, en Salta 2840. Se recuerda que todos los trámites iniciados en la dependencia municipal son completamente gratuitos.