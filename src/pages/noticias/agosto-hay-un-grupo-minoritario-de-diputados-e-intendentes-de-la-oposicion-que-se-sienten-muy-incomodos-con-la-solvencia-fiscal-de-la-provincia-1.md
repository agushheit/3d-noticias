---
category: Estado Real
date: 2022-01-26T06:15:13-03:00
thumbnail: https://assets.3dnoticias.com.ar/agossto.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Agosto: "Hay un grupo minoritario de diputados e intendentes de la oposición
  que se sienten muy incómodos con la solvencia fiscal de la provincia"'
title: 'Agosto: "Hay un grupo minoritario de diputados e intendentes de la oposición
  que se sienten muy incómodos con la solvencia fiscal de la provincia"'
entradilla: Así lo manifestó el ministro de Economía de la provincia tras la falta
  de aprobación por parte de la Cámara de Diputados del Proyecto de Presupuesto 2022.

---
El ministro de Economía de la provincia, Walter Agosto, confirmó que el primer decreto del año reconduce el presupuesto del año pasado ante la falta de aprobación por parte de la Cámara de Diputados del Proyecto de Presupuesto 2022. Cabe señalar que dicho proyecto para 2022 fue remitido a la legislatura el 29 de septiembre pasado, y contó con el voto unánime de la Cámara de Senadores, pero no fue acompañado por la Cámara Baja.

En ese contexto, Agosto analizó junto a los máximos responsables de Hacienda y Presupuesto del Gobierno Nacional, algunos detalles de lo que finalmente sería el decreto de reconducción del presupuesto provincial.

Con relación al decreto de prórroga del presupuesto para el corriente año, Agosto destacó que la misma “se efectúa en el marco de lo dispuesto en el Artículo 55 de la Constitución Provincial y de la ley 12510/05 de Administración Financiera vigente en la provincia. Se reconduce el presupuesto vigente con excepción de los créditos y recursos previstos por única vez o cuya finalidad hubiera sido cumplimentada”.

“También deben contemplarse los créditos presupuestarios indispensables para asegurar la continuidad de los servicios que el estado provincial debe garantizar”, indicó Agosto, quien además expresó: “Veo que hay algunos diputados e intendentes de la oposición que creen que la prórroga del presupuesto solo afecta al Poder Ejecutivo. Hay que tener en cuenta que también alcanza al poder legislativo y a diferentes fondos con destino a municipios y comunas”.

Asimismo, el titular de Economía manifestó que el debate parlamentario “fue muy bueno, e incluso con muchas más instancias que en la discusión del año anterior”.

No obstante, el funcionario advirtió que “hay un grupo minoritario de diputados de la oposición que se sienten muy incómodos con la solvencia fiscal de la provincia y con el hecho de que el gobierno haya superado el descalabro fiscal que ellos mismos dejaron. Enfrascados en esa lógica no contribuyen a un debate constructivo”, concluyó.