---
layout: Noticia con imagen
author: "Fuente: Municipalidad de Santa Fe"
resumen: Licencia de Conducir
category: La Ciudad
title: Se habilitan 600 turnos para quienes sacan el carnet por primera vez
entradilla: A través del sitio web se puede acceder a los cupos especiales para
  quienes deben gestionar su licencia de conducir inicial.
date: 2020-11-06T12:09:00.000Z
thumbnail: https://assets.3dnoticias.com.ar/carnet.jpg
---
La Municipalidad de Santa Fe habilitó cupos especiales para quienes tramitan por primera vez su licencia de conducir, sea o no profesional. Las personas interesadas ya pueden ingresar a la web y solicitar un turno para diciembre.

En total habrá disponibles 600 turnos entre los que se habilitan para la realización de trámites y para las charlas obligatorias. En todos los casos, la atención de quienes tramitan su licencia inicial se hará en el Centro de Educación Vial Municipal ubicado en el Parque Juan de Garay.

“Con el intendente Emilio Jatón venimos reforzando diferentes medidas para poder garantizar una mayor cobertura del servicio de licencias en este momento de emergencia sanitaria en el cual se trabaja con todos los protocolos de distanciamiento y con restricciones de personal”, explicó la secretaria de Control y Convivencia, Virginia Coudannes, y agregó: “En ese marco, abrimos nuevos turnos para poder garantizar la viabilidad de este servicio”.

En ese sentido, desde el inicio de la emergencia sanitaria por COVID-19, la Municipalidad ha realizado las adecuaciones necesarias para poder seguir realizando todos los trámites vinculados a la licencia de conducir garantizando las medidas de higiene y distanciamiento que corresponden para preservar la salud de la ciudadanía y de las y los trabajadores.

Quienes deseen acceder al trámite, deberán ingresar a www.santafeciudad.gov.ar, buscar el botón “Turnos” y seleccionar “Licencias de conducir”. Luego, siguiendo las indicaciones del formulario podrán solicitar alguno de los cupos para “Licencia Inicial”, sólo en la sede del parque Garay.

Por otro lado, se recuerda que quienes tengan licencias de conducir que hayan vencido este año también pueden hacer el trámite de renovación pero las mismas, por disposición municipal y en consonancia con las normativas provinciales y nacionales, siguen siendo válidas por 365 días después de la fecha de vencimiento.