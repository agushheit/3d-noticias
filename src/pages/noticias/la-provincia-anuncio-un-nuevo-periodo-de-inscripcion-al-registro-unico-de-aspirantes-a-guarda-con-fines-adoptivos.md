---
category: Agenda Ciudadana
date: 2021-09-01T06:15:17-03:00
thumbnail: https://assets.3dnoticias.com.ar/adopcion.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: La provincia anunció un nuevo período de inscripción al Registro Único de
  Aspirantes a Guarda con fines Adoptivos
title: La provincia anunció un nuevo período de inscripción al Registro Único de Aspirantes
  a Guarda con fines Adoptivos
entradilla: Será del 1 al 10 de septiembre, gratuito y online. En octubre se implementará
  el programa "Ruaga Acompaña", orientado a las personas y parejas ya incorporadas
  al registro.

---
La provincia, a través de la directora del Registro Único de Aspirantes a Guarda con fines Adoptivos (Ruaga), Magdalena Galli Fiant, anunció un nuevo período de inscripción del 1 al 10 de septiembre para los aspirantes a guarda con fines adoptivos. El trámite es totalmente gratuito y online.

Los interesados deben ingresar a la web de la provincia: www.santafe.gob.ar, clickear el botón "Ruaga" y descargar el Formulario F1. Este formulario se completa solo con los datos personales y se envía adjunto a un mail a las casillas: registros@santafe.gob.ar o registrosrosario@santafe.gob.ar.

Galli Fiant confirmó, además, que a partir del próximo mes de octubre se implementará el programa “Ruaga Acompaña”, orientado a las personas y parejas ya incorporadas al registro que están disponibles para ser convocadas para la guarda de niños y adolescentes en situación de adoptabilidad.

El programa de la provincia incluye la realización de talleres con aspirantes que tengan disponibilidad adoptiva similar, encuentros con familias que han transitado por la adopción y otras propuestas audiovisuales y escritas que ayudarán a sostener una espera activa.

Galli Fiant destacó que el programa “Ruaga Acompaña" está dirigido a quienes tienen situaciones similares como los inscriptos para adoptar adolescentes, grupos de hermanos, aquellos que están esperando y no han sido convocados. "Agrupando de esta manera, podemos organizar estas instancias acompañando desde los profesionales del registro para que ellos reciban experiencia y testimonio de familias que ya transitaron la adopción" y agregó "se busca que haya un espacio dentro del registro, donde ese tiempo de espera sea más acompañado, de preparación y de dar frutos", expresó.

Asimismo, respecto a los tiempos de adopción, explicó que "la mayoría de las personas buscan niños muy pequeños y son los que prácticamente no están en situación de adoptabilidad, porque los que si están son mayores de la edad buscada generalmente".

"Mejoramos el sistema de inscripción, que haya más información y transmisión de esta situación para que los que se inscriban sepan que esperamos familias que reciban grupos de hermanos, más grandes o con alguna discapacidad", finalizó.

Cabe señalar que desde agosto del año 2020 está vigente el actual sistema de inscripción, que incluye encuentros informativos obligatorios de los que ya participaron 708 postulantes, entre los que hay personas solas, parejas, matrimonios y uniones convivenciales.

Estos encuentros permiten un primer acercamiento entre los nuevos aspirantes y los profesionales del Registro Único de Aspirantes a Guarda con fines Adoptivos, previo a que presenten su solicitud de registración.