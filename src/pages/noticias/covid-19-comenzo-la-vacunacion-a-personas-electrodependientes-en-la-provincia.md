---
category: Estado Real
date: 2021-04-19T06:00:56-03:00
thumbnail: https://assets.3dnoticias.com.ar/ELECTRODEPENDIENTES.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Covid-19: comenzó la vacunación a personas electrodependientes en la provincia'
title: 'Covid-19: comenzó la vacunación a personas electrodependientes en la provincia'
entradilla: El Ministerio de Salud planifica en todas las regiones la inoculación
  mediante un operativo domiciliario.

---
El Ministerio de Salud de la provincia comenzó a vacunar contra el Coronavirus a personas electrodependientes mediante un operativo domiciliario, en el marco de la campaña que lleva adelante para toda la población objetivo.

Cabe recordar que las personas con esta condición requieren de un suministro eléctrico constante y en niveles de tensión adecuados para alimentar el equipamiento que, por prescripción médica, les resulta imprescindible para vivir; y que, por lo general, su movilidad es reducida.

Es por ello que se dispuso un operativo especial para garantizar la inmunización en esta población sin que tengan que trasladarse a un centro de vacunación.

Al respecto la ministra de Salud Sonia Martorano destacó que “de modo similar a como hicimos con las personas alojadas en geriátricos, y para que no deban movilizarse, los equipos de Inmunizaciones comenzaron una estrategia domiciliaria con personas electrodependientes en toda la provincia, que se extenderá la semana próxima”.

Finalmente, dijo que “hasta el mediodía del viernes ya habían recibido la primera dosis un número importante de personas en Rosario y en Santa Fe; mientras que el lunes de la semana próxima el operativo comenzará en Venado Tuerto, en Reconquista y en Rafaela”.

De esta forma, la provincia continúa con el operativo de vacunación contra el Coronavirus en diferentes poblaciones, tal como sucedió con los geriátricos, las residencias de personas con discapacidad, los trabajadores de salud, los trabajadores de la educación y las fuerzas de seguridad. Asimismo, esta semana se inició la inoculación a personas trasplantadas.