---
category: La Ciudad
date: 2020-12-11T10:55:20Z
thumbnail: https://assets.3dnoticias.com.ar/CASABOMBA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: La provincia comienza a instalar la "Casabomba 0" en el barrio Centenario
  de Santa Fe
title: La provincia comienza a instalar la "Casabomba 0" en el barrio Centenario de
  Santa Fe
entradilla: 'Se trata de seis electrobombas que funcionarán en caso de crecidas del
  río para disminuir el riesgo hídrico. '

---
**La obra, que requirió una inversión cercana a los 40 millones de pesos, beneficiará a 90.000 vecinos de la zona sur.**

El gobierno provincial, a través del Ministerio de Infraestructura, Servicios Públicos y Hábitat comenzó el operativo de instalación de la “Casabomba Cero”, ubicada en el barrio Centenario, sobre la avenida Circunvalación de la ciudad capital, que demandó una inversión de 39.971.114 de pesos.

La ministra de Infraestructura, Servicios Públicos y Hábitat, Silvina Frana, destacó la relevancia de la obra y la decisión de finalizarla: “Hoy estamos felices porque luego de casi 10 años, estamos culminando con una obra que beneficiará a más de 90 mil vecinos”.

“Se trata de una obra anhelada por los vecinos de los barrios Centenario, Chalet, San Lorenzo y Fonavi San Jerónimo, cuyo principal objetivo es disminuir el riesgo hídrico, especialmente en los momentos en que el río Paraná esté alto y se deban cerrar las compuertas”, explicó la ministra.

“Estas son las obras prioritarias que, quizás, no tienen una hermosa foto, pero, como nos señala el gobernador Omar Perotti, lo importante es que la obra pública sea un pilar para brindarle respuesta a nuestra gente y para mejorarle la calidad de vida a los ciudadanos”, subrayó Frana.

Por su parte, el secretario de Recursos Hídricos, Roberto Gioria, explicó que "se van a construir una alcantarilla de acceso a la estación por el norte; una embocadura por el sur; una cámara de distribución de caudales; cuatro pozos de bombeo para electrobombas sumergibles; dos pozos de bombeo para electrobombas verticales; un pozo para la instalación de una motobomba de respaldo, ante la falta de energía eléctrica; un canal de descarga; una cámara de descarga; una cámara de compuertas; un conducto de descarga; una playa de maniobras; un acceso vial; y una sala de operación, transformadores y arrancadores", detalló.

Asimismo, aclaró que “se realizarán trabajos de herrería no contemplados en el contrato original, ante la necesidad de incorporar protección y cerramiento a la obra, con el objetivo de mejorar las condiciones de trabajo y seguridad del personal que opera la estación de bombeo".

![](https://assets.3dnoticias.com.ar/CASABOMBA1.jpeg)

# **LARGA ESPERA**

La obra de la “Casabomba Cero” comenzó a proyectarse en 2011 y se encuentra ubicada en la denominada Cuenca Centenario -sobre la avenida Circunvalación-, en el sur de la ciudad. Consiste en un reservorio y una estación de bombeo con un conducto de descarga de agua hacia el río Santa Fe, más el reacondicionamiento del canal Tacca y un terraplén de cierre provisorio.

El presupuesto oficial de la obra asciende a $ 39.971.114, con un plazo de ejecución de cinco meses.

El objetivo del proyecto consiste en la adquisición de cuatro electrobombas sumergibles de entre 500 y 600 lts/s de caudal y dos electrobombas sumergibles de flujo axial con hélice de 16° con motor eléctrico sumergible, de entre 3000 lts/s de caudal.

Estos trabajos beneficiarán directamente a los barrios Centenario, Chalet, San Lorenzo y Fonavi San Jerónimo e, indirectamente, a los barrios 12 de Octubre, Roque Sáenz Peña, Santa Rosa de Lima y Roma Sur, que corresponden a la cuenca de aporte al sistema del reservorio Estación de bombeo 2.

![](https://assets.3dnoticias.com.ar/CASABOMBA2.jpeg)