---
category: La Ciudad
date: 2020-12-02T10:57:15Z
thumbnail: https://assets.3dnoticias.com.ar/jaton.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: La Municipalidad renovó el permiso de uso libre de la Estación Belgrano
title: La Municipalidad renovó el permiso de uso libre de la Estación Belgrano
entradilla: El convenio firmado entre la ciudad y la Nación es sin límite de tiempo.
  “Estamos haciendo lo que hay que hacer”, dijo el intendente Jatón.

---
Este martes, mediante una reunión virtual, el intendente Emilio Jatón rubricó un acuerdo con la Agencia de Administración de Bienes del Estado (AABE) para que la Municipalidad siga haciendo uso libre de la Estación Belgrano. En este caso, y a diferencia del acuerdo anterior, el usufructo será sin límite de tiempo.

En el encuentro entre las autoridades, se firmó el convenio mediante el cual Nación cede el espacio de 3.2 hectáreas a la capital de la provincia. Según se indicó, se contempla el edificio de la ex-estación de trenes, así como también, el predio que se extiende desde el bulevar Gálvez hacia el norte, hasta calle Chacabuco, entre Avellaneda y Dorrego. El permiso se fundamenta en que el inmueble y el terreno lindante son utilizados como centro de exposiciones, convenciones y ferias de escala regional, además dependencias municipales.

En primera instancia, Jatón subrayó “el valor de la palabra porque algunos lo ponían en duda y hoy quiero reconocer que la AABE cumplió con su palabra”. El intendente recordó que el proceso se inició en el mes de enero, “cuando asumimos la gestión municipal sabiendo que a los pocos meses se vencía el comodato de diez años así que empezamos a trabajar: viajamos a Buenos Aires, tuvimos la primera reunión con la gente del AABE y presentamos los papeles correspondientes, pero la pandemia de COVID-19 alargó los tiempos”.

“Estamos haciendo lo que teníamos que hacer porque era el camino que había que recorrer. Este es un patrimonio de la ciudad y de los santafesinos. Ahora tenemos tiempo para seguir trabajando en la Estación y utilizándola”, aseveró Jatón. Acto seguido, indicó que el municipio propone ahora resolver la utilización “del resto de las 20 hectáreas de este predio. En esa discusión estarán las tres partes: Nación, provincia y municipio para ver cuál va a ser su destino”.

El intendente recordó que la Belgrano “tuvo tres momentos históricos: las fotos nos hablaban de un gran apogeo, con mucha gente dándole vida. Después tuvo un olvido, por parte del mismo Estado nacional. Y posteriormente, la misma política del municipio lo empezó a poner en su estado actual”, enumeró. Ahora, el objetivo de la actual gestión es “seguir ese proceso, avanzando en la reconstrucción, de hecho, actualmente estamos trabajando en poner en condiciones algunos salones que lo necesitaban y agrandando el espacio físico de uso”.

“Lo de hoy es un avance enorme, pero nos queda un gran desafío. Lo que tenemos que hacer es poner la mirada en lo que resta para que no vuelva a caer en el olvido. Debemos pensar qué hacemos con estos bienes del Estado. Para ello, nosotros nos ponemos a disposición y a nuestros equipos técnicos para pensar entre todos qué hacemos”, concluyó.

Vale recordar que a mediados de 2020 operó el vencimiento del convenio anterior, que tenía un plazo de 10 años. Es por ello que el intendente Jatón y su gabinete habían iniciado en enero pasado, una serie de gestiones ante el Gobierno nacional, con el objetivo de renovar ese permiso.

El próximo desafío, para la siguiente etapa, será trabajar en conjunto entre el municipio, la provincia y la Nación para definir el destino del resto del predio. Se trata de un espacio de 20 hectáreas que se ubican por detrás del cuadrante cedido y que no están incluidos dentro de este convenio ni el anterior.

![](https://assets.3dnoticias.com.ar/JATON1.jpg)

## **Participantes**

En representación de la AABE, su presidente, Martín Cosentino, afirmó que “después de mucho tiempo de afinar el lápiz con el municipio, la provincia y la gente de ferrocarriles, llegamos a firmar este permiso de uso para que la ciudad pueda seguir teniendo el usufructo de este espacio para la comunidad y para todos los santafesinos”. En ese sentido, aseguró que “desde que asumimos en la gestión, establecimos como fundamental hacer del espacio público algo para toda la comunidad y poner los bienes del Estado para que sean de uso público de todos”.

Finalmente, Cosentino señaló que “estamos todos muy contentos por haber llegado a esta decisión” y manifestó su deseo de visitar prontamente la Estación Belgrano, “para conocerla y recorrerla”.

De la reunión virtual también tomaron parte el secretario de Desarrollo Urbano municipal, Javier Mendiondo; la ministra de Infraestructura, Servicios Públicos y Hábitat, Silvina Frana, en representación del Gobierno provincial; concejales locales; y legisladores provinciales y nacionales.

![](https://assets.3dnoticias.com.ar/JATON2.jpg)