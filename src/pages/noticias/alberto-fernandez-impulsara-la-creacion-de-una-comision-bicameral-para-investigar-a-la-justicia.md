---
category: Agenda Ciudadana
date: 2021-03-03T07:12:38-03:00
thumbnail: https://assets.3dnoticias.com.ar/af-03032021.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de La Nación
resumen: Alberto Fernández impulsará la creación de una comisión bicameral para investigar
  a la Justicia
title: Alberto Fernández impulsará la creación de una comisión bicameral para investigar
  a la Justicia
entradilla: Después del durísimo discurso ante la Asamblea Legislativa, Fernández
  habló del tema con Massa; el mandatario busca que se investigue el trabajo del Poder
  Judicial

---
Tras el duro discurso en el que reclamó al Congreso “que asuma su rol de control cruzado sobre el Poder Judicial”, el presidente Alberto Fernández decidió impulsar la creación de una comisión bicameral, de diputados y senadores, “que investigue a la Justicia”.

Según confirmaron fuentes oficiales a LA NACION, el Presidente le adelantó ese objetivo ayer al titular de la Cámara de Diputados, Sergio Massa, tras hablar ante la Asamblea Legislativa. Incluso, el jefe del Estado y Massa se reunirán en los próximos días, posiblemente el fin de semana, para avanzar con los detalles.

**LA CORTE ELIGIÓ EL SILENCIO COMO RESPUESTA A LAS CRÍTICAS DE ALBERTO FERNÁNDEZ**

“Es un proyecto de Alberto”, anticipó uno de los principales asesores del Presidente. La decisión de Fernández sorprendió incluso a altos funcionarios del Gobierno que no estaban al tanto de los movimientos del mandatario y que desconocían que esos planes existieran.

En paralelo, dispuestos a no perder tiempo, el día después de que el mandatario cargó contra la Justicia, el kirchnerismo duro tomó la lanza y anticipó la jugada de crear una comisión bicameral para indagar sobre el trabajo de los jueces.

La idea, que planteó públicamente el senador Oscar Parrilli, de estrecha confianza de la vicepresidenta Cristina Kirchner, se sumará al resto de los cambios que ya propuso el Poder Ejecutivo, como la reforma judicial y del Ministerio Público Fiscal, que se encuentran en la Cámara de Diputados para su tratamiento.

Pese al impulso de la Casa Rosada, cerca del Presidente tienen claro que en medio de la disputa electoral es un escenario complicado. “Si no pasa la reforma judicial, esto es muy difícil”, reconoció un integrante del gabinete nacional.

Desde temprano, el kirchnerismo salió a respaldar las palabras del Presidente. Además de Parrilli, uno de los exegetas de la expresidenta, también cuestionaron a la Justicia dos de los principales líderes de La Cámpora: el ministro del Interior, Eduardo de Pedro, y el ministro de Desarrollo de la Comunidad de la provincia de Buenos Aires, Andrés Larroque.

“El discurso del Presidente me satisfizo plenamente. Abordó problemas que tiene la Argentina con toda realidad”, dijo Parrilli, en declaraciones a El Destape Radio. Y agregó: “Como consecuencia del macrismo, en la Justicia tenemos estas fallas estructurales que le complican la vida al ciudadano común”.

En ese contexto, el senador por Neuquén, dijo: “El Presidente pidió una comisión bicameral que investigue a la Justicia. Que la sociedad vea como está funcionando el Poder Judicial y cuáles son los cambios que hay que hacer”.

Hoy existe en el Congreso una comisión bicameral de seguimiento del Ministerio Público Fiscal, así como una Comisión de Justicia en ambas cámaras. Quienes desconocían los planes de creación de la nueva bicameral sostenían que Fernández, lo que pretendía, era que se usaran las herramientas que ya tiene el Congreso, como por ejemplo la posibilidad de abrir juicios políticos a los jueces de la Corte Suprema. Aunque, finalmente, por la noche ya no había dudas: el Presidente promoverá la constitución de una comisión bicameral.

Si bien allegados a Fernández reconocieron que las palabras que dio ayer fueron particularmente duras, rechazan hablar de una “cristinización”. Independientemente de estos debates internos, en el Senado y en el Instituto Patria había satisfacción por el tono que eligió el Presidente para poner en marcha el año legislativo.

Antes de firmar los libros de actas de las dos cámaras como indica el protocolo, el presidente creyó que la vicepresidenta también debía estampar su firma, pero recibió una concisa explicación.

![](https://assets.3dnoticias.com.ar/02032021-3dn.webp)

“Quiero pedirle al Congreso con muchísimo respeto que asuma su rol de control cruzado sobre el Poder Judicial. Así lo prevé nuestra Constitución Nacional”, fueron las palabras que eligió el Presidente y que encendieron de inmediato alarmas entre los jueces. Y agregó: “Institucionalmente, alguien debe ocuparse de ver lo que ha ocurrido y ver lo que está ocurriendo en la administración de justicia de nuestra república. No para interferir en sus decisiones, si no para que sirva cumpliendo el rol que el Estado de Derecho le ha asignado”.

Poco afecto al contacto con los medios, Eduardo “Wado” de Pedro aprovechó el anuncio sobre la construcción de los denominados Centros Territoriales Integrales de Políticas de Género y Diversidad para marcar su postura. “El Poder Judicial tiene que escuchar. O se transforman o se van de un poder que busca igualdad y justicia”, dijo el ministro del Interior.

Otro de los que ponderó al Presidente fue Larroque: “Uno de los pilares de la república es que exista entre los poderes un marco de control y seguimiento (...). Sin ánimos de revanchas, queremos que las instituciones funcionen y que sean creíbles”.

**CORTOCIRCUITO EN EL KIRCHNERISMO POR LA PROPUESTA DE PARRILLI**

Y volvió sobre una idea en la que se apoya el Gobierno para promover cambios en los tribunales: el descrédito del Poder Judicial. “Hay un nivel de credibilidad en la Justicia de los más bajos de la historia. Eso no es bueno para la democracia ni para nadie”.

Otro cristinista que defendió el embate ante los magistrados fue el secretario de Justicia, Juan Martín Mena, quien aseguró que “no puede tolerarse un Poder Judicial al margen de la ley y la Constitución”. El número dos de la cartera que encabeza Marcela Losardo explicó que se trata de “un Poder Judicial que en el siglo XXI mantiene los privilegios del siglo XIX y no rinde cuenta de sus actos”.