---
category: Agenda Ciudadana
date: 2021-05-16T06:15:17-03:00
thumbnail: https://assets.3dnoticias.com.ar/perottij.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Restricciones: Perotti dijo que se esperaba una "baja mayor" de casos y
  que no habrá cambios"'
title: 'Restricciones: Perotti dijo que se esperaba una "baja mayor" de casos y que
  no habrá cambios"'
entradilla: El lunes comienza como estamos", señaló el mandatario. Confirmó que las
  clases en el nivel secundario, en los departamentos Rosario y San Lorenzo, seguirán
  siendo virtuales.

---
"Todo indica que vamos a seguir como estamos, así que no creo que haya algún anuncio por hacer", dijo el gobernador de la provincia Omar Perotti con relación a las restricciones; e insistió: "Tenemos que seguir manteniendo los cuidados a pleno".

El mandatario volvió a hablar de un "un amesetamiento (en los casos) pero alto". Si bien destacó que "las medidas están dando buen resultado y bajó el pico de la curva", se esperaba "alguna baja mayor (de contagios). Son testigos en el seguimiento diario de que no se ha dado".

Por otro lado, confirmó que las clases en el nivel secundario, en los departamentos de Rosario y San Lorenzo, continuarán dictándose de manera virtual al menos hasta la semana venidera.

En declaraciones a la emisora "Radio 2", de Rosario, Perotti fundamentó esa decisión "en que el número de contagios de coronavirus en el territorio santafesino se mantiene alto pese al amesetamiento de la curva".

Recordó que el nivel de ocupación de camas críticas sigue siendo "muy alto" y que por eso algunas medidas se seguirán manteniendo, por ejemplo, lo que respecta a las clases presenciales: "Continuará vigente el mismo esquema con los alumnos de nivel inicial y primario en las aulas y los de secundario asistiendo de forma remota".