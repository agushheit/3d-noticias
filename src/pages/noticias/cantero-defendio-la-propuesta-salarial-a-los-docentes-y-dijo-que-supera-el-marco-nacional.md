---
category: Agenda Ciudadana
date: 2021-11-02T06:00:58-03:00
thumbnail: https://assets.3dnoticias.com.ar/cantero.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Cantero defendió la propuesta salarial a los docentes y dijo que "supera
  el marco nacional"
title: Cantero defendió la propuesta salarial a los docentes y dijo que "supera el
  marco nacional"
entradilla: La provincia ratificó este lunes la propuesta salarial para el sector.
  Por su parte, Amsafé dijo que solicitará la mediación nacional

---
Este lunes, en el marco de una nueva reunión paritaria, el gobierno provincial ratificó su propuesta salarial para el sector docente. Se trata de un incremento salarial del 17 por ciento en tres partes (10 por ciento, en octubre; 5 por ciento en diciembre y dos por ciento en enero). De esa manera, un maestro de grado que recién se inicia cobraría en octubre 59.759 pesos. Mientras que en diciembre pasaría a 61.688 pesos y, en enero, a 62.460 pesos.

"Esta propuesta va a ser puesta a consideración de las compañeras y compañeros", dijo el secretario General de Amsafé La Capital, Rodrigo Alonso. Y añadió: "Pero queremos dejar en claro desde Amsafé que para nosotros no está cerrada la discusión paritaria y solicitamos la intervención de la mediación nacional haciendo uso del artículo 16 de la paritaria provincial".

En este marco, la negociación parece haber ingresado en un callejón sin salida. La ministra de Educación, Adriana Cantero afirmó tras el encuentro: “Ratificamos la oferta salarial básicamente porque consideramos que la misma cumple con los compromisos establecidos, supera el marco nacional, es una buena propuesta y así lo han entendido todas las trabajadoras y los trabajadores nucleados en los gremios que los representan en relación con el estado provincial”.

Por su parte, el ministro de Trabajo, Empleo y Seguridad Social, Juan Manuel Pusineri, afirmó: “No existe ningún elemento objetivo que sustente una modificación de la propuesta que realizamos hace algunas semanas y que fue aceptada por los gremios de la administración central, profesionales de la Salud, Luz y Fuerza y los trabajadores de la empresa de aguas”, afirmó.

El titular de la cartera laboral aseguró que “en el caso de que se acepte la oferta, la provincia está en condiciones de liquidar la semana que viene, por planilla complementaria, el 10% de aumento correspondiente al mes de octubre a quienes aún no lo han percibido”, Asimismo, el ministro aclaró que “el 2% ofrecido para el mes de enero se computa como parte del salario de este año 2021, por lo que se incorpora a la base de negociación del esquema salarial para 2022”.

Entre los otros puntos tratados, la ministra de Educación Cantero enumeró:

>> Sustanciar la convocatoria a concurso de nivel secundario el 1° de diciembre.

>> Iniciar el proceso con vistas a modificar el reglamento orgánico de los Institutos del Nivel Superior.

>> Aportar 2500 horas Cátedra en el período Noviembre/Febrero para el acompañamiento de trayectorias escolares de la Secundaria Básica, las cuales serán distribuidas en escuelas urbanas y cubiertas por escalafón de suplencias de preceptores.

>> Dar respuesta a los casos puntuales que se trajeron a la mesa del día viernes 29 de octubre referidos al Boleto Educativo Gratuito. Se incluye a partir de noviembre la extensión del beneficio a quienes se trasladen para los Sábados Activos, a los equipos de ESI y a los equipos Socioeducativos.

>> Aportar a la mejora de las prestaciones del IAPOS con los nuevos convenios para odontología en los departamentos del Sur que antes dependían de Rosario y que ahora contarán con acuerdos con prestadores locales. También dejar constancia que el incremento de los honorarios médicos reconocidos para el período Octubre/Marzo eliminará dificultades constatadas en los cupos.

>> Por último, y en caso de aceptarse la presente propuesta, el gobierno se compromete a llamar a una reunión en el Ministerio de Educación para acordar la recuperación de contenidos de los tres días de paros, suspendiendo los descuentos respectivos.