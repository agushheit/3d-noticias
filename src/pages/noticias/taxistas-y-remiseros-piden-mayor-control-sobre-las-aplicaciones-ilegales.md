---
category: La Ciudad
date: 2021-12-12T06:00:52-03:00
thumbnail: https://assets.3dnoticias.com.ar/taCHOS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Taxistas y remiseros piden mayor control sobre las "aplicaciones ilegales"
title: Taxistas y remiseros piden mayor control sobre las "aplicaciones ilegales"
entradilla: 'Elevaron una nota a la Municipalidad. "Es irrisoria la captura de vehículos
  sin autorización", sostienen los taxistas y remiseros autoconvocados.

'

---
La situación del servicio de taxis y remises en la ciudad de Santa Fe preocupa a todos los involucrados en el subsistema de transporte. Los clientes históricos de los coches negros y verdes están virando hacia las apps que "conectan choferes con pasajeros" como Uber y Maxim. En este contexto, taxistas y remiseros autoconvocados presentaron una nota al Ejecutivo municipal con "inquietudes sobre diferentes aspectos que atañen al trabajo diario y al futuro de la actividad".

La nota presentada por taxistas y remiseros autoconvocados en la Municipalidad está dirigida al subsecretario de Movilidad y Transporte, Lucas Crivelli. El comunicado tiene cinco puntos de "preocupación" para los trabajadores del sector: seguridad, transporte y aplicaciones ilegales, precarización laboral y capacitación, licencias y reconocimiento.

"Luego de muchas charlas y debate, logramos consensuar entre la mayoría de los trabajadores del volante autoconvocados, la necesidad de manifestarnos. Primero presentamos formalmente y de forma pacífica la preocupación que tenemos sobre el tema, para luego, si no tenemos respuestas, movilizarnos", sostuvo uno de los taxistas.

![Taxistas y remiseros presentaron una lista de ](https://media.unosantafe.com.ar/p/951747d96c4daed5c1566543941d308b/adjuntos/204/imagenes/028/903/0028903265/taxis-y-remises-la-municipalidad-3jpg.jpg?0000-00-00-00-00-00 =642x417)

"Necesitamos respuestas urgentes para comenzar a solucionar los múltiples problemas de los taxistas y remiseros que hoy se presentan en las calles de la ciudad. El malestar que nos unió a los trabajadores del volante después de mucho tiempo, es la falta a la verdad de los funcionarios municipales respecto a la realidad del subsistema de transporte", expresó uno de los trabajadores.

"Estamos realizando un promedio de 300.000 viajes por mes entre los más de 200 móviles disponibles que existen, algunos choferes trabajando más de 14 horas por día y atentando contra la salud del trabajador y del pasajero", finalizó.

**Uno de los pedidos de taxistas y remiseros: transportes y aplicaciones ilegales**

En la carta entregada en mesa de entrada del palacio municipal, taxistas y remiseros dejan establecidos cinco puntos a tratar, pero uno de ellos viene levantando polvareda desde hace ya mucho tiempo en la ciudad: Maxim y Uber.

"Afianzar los controles sobre el transporte ilegal que circula las 24 horas del día por avenidas troncales de la ciudad libremente con carteles luminosos (de noche) y carteles escritos (de día), y nadie toma cartas en el asunto", indican los trabajadores.

Por otro lado, piden "arbitrar la forma más adecuada para tomar el control sobre las app ilegales que crecen día a día sin control alguno y la irrisoria captura de vehículos afectados a tales app".