---
category: Agenda Ciudadana
date: 2021-11-29T06:00:03-03:00
thumbnail: https://assets.3dnoticias.com.ar/albert.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: La Casa Rosada fija nuevo rumbo a la política con el acuerdo con el FMI como
  eje
title: La Casa Rosada fija nuevo rumbo a la política con el acuerdo con el FMI como
  eje
entradilla: 'En Balcarse 50 se está produciendo un "intercambio de ideas entre compañeros".
  Afirman que esas conversaciones son para ordenar el "rumbo" del gobierno y que el
  punto central es el acuerdo con el organismo multilateral

'

---
En Casa Rosada anticipan que "se viene un interesante proceso de discusión para definir la política del Frente de Todos", de cara a lo que será la "segunda etapa" del gobierno de Alberto Fernández.

"Por ahora se está produciendo un intercambio de ideas entre compañeros, para ir viendo el rumbo de los próximos años de gestión. Hace falta organizarse para poder ordenar el rumbo", resaltaron a NA fuentes con despacho en Balcarce 50.

En esa línea, definieron al peronismo como "un proceso vivo", por lo que se producen "mesas de conversación con idas y vueltas, donde se llega a puntos de acuerdo".

"Se viene un cambio de época. La esencia es tener un proyecto de país", enfatizaron a Noticias Argentinas, y consideraron que "parte de la solución de la crisis de la Argentina es cambiar la mirada externa y darle suma importancia a la política exterior".

Al respecto, afirmaron "llegar a un acuerdo con el Fondo Monetario Internacional es algo así como un certificado de normalidad", que es lo que el país "necesita para darle valor al sistema financiero y comercial".

"Para que se normalice el país, en el marco de los negocios, la Argentina necesita acuerdo con el Fondo. Somos un país que parece estar sumergido en sus problemas internos. Es necesario que dejemos de mirarnos el ombligo y empecemos a mirar al mundo", puntualizaron.

En ese marco, confiaron que "la previsibilidad en el mundo de los negocios es necesaria y vital", por lo que "para incrementar el números de puestos laborales se necesitan inversiones, por eso es tan importante normalizar la macroeconomía".

"Hay un conjunto de planes de trabajo y de asistencia, pero la aspiración es que esos argentinos se integren a la vida productiva", señalaron.

Asimismo, aseguraron que "con un proceso de inversión y crecimiento económico se va a generar empleo, lo que se va a traducir en la disminución de planes sociales".

"Aspiramos a que con un fuerte proceso de inversión se genere empleo y podamos empezar a disminuir drásticamente la pobreza", expresaron.

Las mismas fuentes opinaron que "Argentina tiene una enorme capacidad si encuentra el rumbo", y ampliaron: "Las empresas argentinas tienen que robustecerse, porque perdieron parte de su valor durante el gobierno de (Mauricio) Macri".

Por último, ratificaron que "Argentina tiene enorme capacidad de respuesta", y recordaron que el país atravesó dos momentos distintos de crecimiento.

"Un período de crecimiento con endeudamiento, en la época de (el ex ministro de Economía Juan Domingo) Cavallo, y otro desde la presidencia de Néstor Kirchner hasta 2008, que fue más virtuoso. Con un desarrollo productivo sin endeudamiento y fuerte inversión publica. El dólar había salido de la conversación", concluyeron.