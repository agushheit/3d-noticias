---
category: Agenda Ciudadana
date: 2021-09-03T06:00:28-03:00
thumbnail: https://assets.3dnoticias.com.ar/INDUSTRIAL.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Destacan la recuperación industrial del país y proyectan la continuidad de
  mejora
title: Destacan la recuperación industrial del país y proyectan la continuidad de
  mejora
entradilla: Funcionarios, empresarios y sindicalistas resaltaron la recuperación de
  la actividad, en el marco de la conmemoración del Día de la Industria.

---
Funcionarios, empresarios y sindicalistas resaltaron la recuperación de la industria en todo el país y proyectaron para los próximos meses una continuidad del sendero positivo, en el marco de la conmemoración del Día de la Industria.

En este sentido, el ministro de Desarrollo Productivo, Matías Kulfas, subrayó que "la industria ya está produciendo casi 5 puntos por encima de 2019 y lleva 12 meses consecutivos de creación de trabajo, sumando 23 mil puestos más que a finales de 2019".

"Estamos revirtiendo una situación que era desastrosa, ya que de los 48 meses de gobierno de (Mauricio) Macri, en 46 hubo destrucción de empleo industrial", agregó el funcionario en declaraciones a Radio Del Plata.

En consonancia, el jefe de Gabinete, Santiago Cafiero, postuló en su cuenta de Twitter que la "actividad está por encima del 2019, con 23 mil nuevos puestos de trabajo y 12 meses consecutivos de crecimiento del empleo. La industria es orgullo y soberanía. Y es el corazón de la reactivación del país. Fortalecerla es el compromiso del presidente, Alberto Fernández, y de la vicepresidenta, Cristina Fernández de Kirchner".

El ministro de Obras Públicas, Gabriel Katopodis, a su vez expresó, en la misma red social, que "si de algo estamos convencidos en el gobierno de @alferdez es que la industria nacional significa empleo de calidad y desarrollo económico".

En tanto, el ministro de Desarrollo Territorial y Hábitat, Jorge Ferraresi, apuntó también en Twitter que "este día nos vuelve a encontrar apostando a un país con empleo y producción que deje definitivamente atrás al modelo de la especulación financiera".

A su vez, el gobernador bonaerense Axel Kicillof aseguró que "de a poco el entramado productivo se empieza a recuperar", luego de "años difíciles" que afectaron al sector industrial, como fue "la desidia del gobierno anterior y la pandemia", y reiteró su compromiso de "acompañar e impulsar" el proceso de recuperación para "poner en marcha a la provincia".

**Los empresarios**

Desde el arco empresarial, el presidente de la Unión Industrial de Santa Fe (UISF), Alejandro Taborda, indicó que "están dadas todas las condiciones para un largo período de crecimiento de la industria" y resaltó que, en su provincia, de 23 actividades industriales "20 están en recuperación".

En sintonía, el presidente de la Unión Industrial de Entre Ríos (UIER), Leandro Garciandía, convocó a "consensuar condiciones entre sector público y privado", paso que calificó como "clave para que la salida pospandemia sea un camino de oportunidades reales".

Por su parte, el presidente de Industriales Pymes Argentinos (IPA), Daniel Rosato, ponderó las medidas que en 2020 contribuyeron a que más de 10.000 pymes industriales se mantengan abiertas y sostengan las fuentes de trabajo.

En esa línea, aseguró que "esto dio una mayor previsibilidad para que a partir de octubre de 2020 se pudiera comenzar una reactivación sustentable y sostenida en los diferentes sectores de la industria pyme".

La Asamblea de Pequeños y Medianos Empresarios (Apyme) subrayó "la recomposición del empleo industrial a raíz de una mayor utilización de la capacidad instalada y la restitución de un Estado presente y activo, canalizador de las demandas de los sectores productivos, tras cuatro años de políticas anti industriales que devastaron el entramado económico".

**Los sindicatos y el respaldo de los gobiernos provinciales**

Desde el sector sindical, el secretario general del sindicato de Obreros Curtidores, Walter Correa, señaló que "en la provincia de Buenos Aires en este proceso de pospandemia ya se generaron casi 100.000 puestos de trabajo en el sector industrial".

De igual forma, la ministra de la Producción de La Pampa, Fernanda González, ponderó el grado de innovación y la competitividad que alcanzó la industria pampeana, a la vez que destacó la inversión y el rol del Estado en tiempos de pandemia para sostener y contener a las pymes.

Asimismo, el secretario de Desarrollo Industrial y Comercial de Jujuy, José Rossetto, sostuvo que " la pandemia de coronavirus evidenció la notabilidad que tiene el sector produciendo bienes esenciales y brindando servicios a actividades esenciales".