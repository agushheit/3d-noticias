---
category: Agenda Ciudadana
date: 2021-02-19T07:05:37-03:00
thumbnail: https://assets.3dnoticias.com.ar/negri.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa José Corral
resumen: 'Corral: “Juntos por el Cambio tiene que reforzar su presencia en el Congreso”'
title: 'Corral: “Juntos por el Cambio tiene que reforzar su presencia en el Congreso”'
entradilla: Mantuvo un encuentro con Mario Negri. “Necesitamos que Juntos por el Cambio
  incremente su número en el Congreso este año para poder poner un freno al kirchnerismo”,
  coincidieron.

---
El dirigente radical y referente de Juntos por el Cambio en Santa Fe, José Corral, mantuvo una reunión de trabajo con Mario Negri, titular del bloque de Diputados radicales y presidente del interbloque de la coalición. Durante el encuentro conversaron sobre la situación social, económica y política, y repasaron los temas de la agenda legislativa.

“Necesitamos que Juntos por el Cambio incremente su número en el Congreso este año para poder poner un freno al kirchnerismo”, coincidieron.

En ese sentido, José Corral remarcó que “en momentos en los que el oficialismo insiste con un plan de impunidad, a través ahora de la propuesta de Zaffaroni de la ley de amnistía, en la misma dirección que la apelación eterna de las causas de Cristina Kirchner, y el pedido de indulto a Milagro Sala, Juntos por el Cambio más que nunca está en la obligación de cumplir con el mandato electoral de una Justicia independiente, del equilibrio de poderes y la defensa de la Ley y de la institucionalidad”, sentenció.

Y añadió que “conversamos con Mario Negri la necesidad, en este año electoral, de fortalecer la representación legislativa, parlamentaria de Juntos por el Cambio, y dentro de la coalición de la UCR en particular”, y se mostró convencido de que “en esta oportunidad Santa Fe también va a hacer su aporte en ese sentido”.

En esa línea, Juan Martín coincidió en que “mientras los argentinos se encuentran preocupados y angustiados por el avance de la pandemia y las dificultades en el plan de vacunación, y por el avance de la inflación y la caída del salario real, el Gobierno concentra todas sus energías en un plan concreto: avasallar el Poder Judicial y el Congreso, con un claro plan de impunidad para que Cristina Fernández y los funcionarios del kirchnerismo no rindan cuentas frente a la Justicia”.

Y resaltó que “Juntos por el Cambio tiene un mandato claro, que tiene que ver con la defensa de las instituciones y con hacer volver a la agenda pública los temas que son de la gente y que necesitan resolución inmediata”. El diputado santafesino sostuvo que “este año más que nunca vamos a necesitar del compromiso, movilización y acompañamiento de todos los argentinos para fortalecer la posición de Juntos por el Cambio”.

Otro de los temas abordados fue justamente “la defensa de las reglas de juego en el año electoral”, apuntó José Corral: “No se puede cambiar las normas en mitad del partido, y las PASO son una herramienta de expresión de la ciudadanía. En un tiempo de crisis, de dificultades y de malestar de la ciudadanía, más que nunca son necesarias estas herramientas de expresión popular como las Primarias para seleccionar los candidatos de las diferentes fuerzas políticas”, enfatizó el dirigente santafesino.

De la reunión participaron también los diputados Juan Martín y Niky Cantard, y Adriana Molina, integrante de la mesa de conducción de la UCR.