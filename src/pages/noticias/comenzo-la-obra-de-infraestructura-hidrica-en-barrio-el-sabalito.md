---
category: La Ciudad
date: 2021-04-16T08:22:40-03:00
thumbnail: https://assets.3dnoticias.com.ar/sabalito.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad de Santa Fe
resumen: Comenzó la obra de infraestructura hídrica en barrio El Sabalito
title: Comenzó la obra de infraestructura hídrica en barrio El Sabalito
entradilla: 'El sector donde se trabaja comprende las calles Genaro Silva, Malvinas
  Argentinas y Ex Combatientes de Malvinas de Blas Parera al este; y todas las transversales. '

---
El intendente Emilio Jatón observó las tareas que se realizan sobre calle Genaro Silva, en el marco del proyecto de desagües pluviales y mejoras para el barrio El Sabalito, obras necesarias dentro del Plan Director de Desagües Pluviales desarrollado por el INA para Santa Fe. El sector donde se trabaja está ubicado en la cuenca Loza, al noroeste de la ciudad capital.

Después de años de olvido y de no haber buscado una solución de fondo a los anegamientos en este barrio del norte de la ciudad, la Municipalidad llega con una propuesta de trabajo integral, que prioriza la generación de infraestructura hídrica que no existía pero también complementa con otras acciones necesarias.

La intervención actual busca minimizar o atenuar los principales problemas que presenta la zona ligado al anegamiento de calles y viviendas con lluvias de mediana intensidad, debido a que no existe una red de desagües de importancia, y en ocasiones se debe recurrir a equipos de bombeo móviles para desagotar esa parte del barrio.

La obra consiste en ejecutar cunetas a cielo abierto o limpiar las existentes conjuntamente con los cruces de calles correspondientes, para lograr un adecuado funcionamiento de los desagües de la zona, permitiendo la descarga de los excedentes pluviales hacia calle Genaro Silva hasta la estación de bombeo ubicada en Tte. Loza y Blas Parera.

Las tareas comenzaron por calle Ex Combatientes de Malvinas hacia el Este, desde Arzeno hasta Edmundo Rosas; también se trabajará por calle Malvinas Argentinas de Edmundo Rosas hacia el este. Los excedentes pluviales interceptados por Ex Combatientes y Malvinas Argentinas son direccionados a la cuneta que se ejecutará sobre calle Edmundo Rosas, desde Ex combatiente de Malvinas hasta Genaro Silva.

**Obras complementarias**

La secretaria de Asuntos Hídricos y Gestión de Riesgo dio detalles de esta intervención: “En barrio El Sabalito, sobre calle Genaro Silva se está haciendo una obra de drenaje. Este sector pertenece a la cuenta del canal Loza, toda el agua que cae en este barrio debería drenar hacía allí pero no estaba sucediendo entonces decidimos hacer esta tareas. Se trata de la limpieza de las cunetas y la desobstrucción de los cruces”.

Los vecinos dialogaron con el intendente y le manifestaron su alegría por esta intervención que va a traer alivio en los días de lluvia. “Están contentos porque en cada lluvia sufren las consecuencias por la falta de estas obras, y ahora tendrán una solución”, destacó la funcionaria municipal.

En este sentido, Serra aclaró que estas obras se concretan tras el relevamiento luego de cada evento climático y la detección de puntos críticos. Se ejecutan con personal y fondos municipales.

“Se trata de una zona muy plana y es necesario realizar obras para lograr encauzar el agua hacia su salida. Estas obras son de mayor envergadura a las que habitualmente se hacen de mantenimiento de cunetas y cruces. El año pasado se ejecutaron nueve y para este año, hasta el momento, se proyectan cinco más. Son lugares con anegamientos históricos y en el caso de Sabalito es una solución definitiva”, finalizó Serra.

**Alivio al barrio**

Norma Velázquez vive en barrio Sabalito desde hace más de 20 años y aprovechó la visita de Jatón para agradecerle por la intervención que hoy se hace frente a su casa y charlar sobre otras acciones necesarias para la zona producto del abandono de muchos años.. “Están limpiando las cunetas que es muy importante y además hablamos de la luz en la esquina de Genera Silva y Edmundo Rosas que hace años que falta”, contó.

Luego dijo: “El intendente se comprometió a hacerlo y además nos va a poner piedras en la calle. Por eso estamos muy contentos y conformes. Quedamos satisfechos por la respuesta que nos dio, sobre todo que se está trabajando en el cuneteo porque cada vez que llueve el agua queda estancada y no podemos salir”.