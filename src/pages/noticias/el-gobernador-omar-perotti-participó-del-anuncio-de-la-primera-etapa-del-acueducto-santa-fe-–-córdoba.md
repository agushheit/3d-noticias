---
layout: Noticia con imagen
author: "- "
resumen: "Acueducto Santa Fe - Córdoba"
category: Estado Real
title: El gobernador Omar Perotti anunció la primera etapa del
  ACUEDUCTO SANTA FE – CÓRDOBA
entradilla: “Es un hecho altamente gratificante” para “llevar agua a muchos
  habitantes” y “mejorar sus condiciones sanitarias”, destacó el mandatario.
date: 2020-10-22T20:51:03.109Z
thumbnail: https://assets.3dnoticias.com.ar/acueducto.jpg
---
El gobernador Omar Perotti participó este miércoles de la videoconferencia que encabezó el presidente de la Nación, Alberto Fernández, para la puesta en marcha de la Etapa 1 del Acueducto Santa Fe–Córdoba. Del acto también participaron el gobernador de la provincia de Córdoba, Juan Schiaretti; el embajador de Kuwait en Argentina, Abdullah Ali Alyahya; y el secretario de Asuntos Estratégicos de la Nación, Gustavo Béliz.

La primera etapa del acueducto, que conectará la ciudad de Coronda con San Francisco, en la provincia mediterránea, insumirá una inversión de 250 millones de dólares. La obra es continuidad de una política de Estado en la región centro, iniciada en la gestión del exgobernador Miguel Lifschitz, que continúa ahora para llevar agua potable al centro-oeste santafesino. Más adelante, los trabajos continuarán hacia las localidades del norte provincial.

“Para nosotros es un día muy importante”, remarcó Perotti, a la vez que resaltó que la obra conjunta entre Santa Fe, Córdoba, la Nación y Kuwait “es un hecho altamente gratificante para llevar agua a muchos habitantes del centro - oeste provincial y mejorar sus condiciones sanitarias, esto que queremos encarar entre dos provincias sumando a otras que tenemos en marcha con el financiamiento de Kuwait”.

El mandatario santafesino reconoció, además, que “desde los años 90 comenzó el primer vínculo de Kuwait con nuestra provincia; en aquel momento gobernaba Carlos Reutemann, cuando comenzó un primer préstamo. Y de allí fue sin interrupciones. No nos llama la atención que en estos momentos Kuwait pueda estar ratificando una nueva asistencia”.

Por último, Perotti subrayó: “Creemos que con su presencia, presidente, en esta relación estamos afianzando un vínculo que, seguramente, va a crecer por la importancia que usted le dio desde un primer momento”.



**DERECHO HUMANO**

Por su parte, el presidente Alberto Fernández dijo que “este anuncio resuelve un problema que es garantizar que llegue el agua a todos los rincones que los argentinos lo necesiten. En este caso el esfuerzo conjunto de Santa Fe y Córdoba para desarrollar un acueducto que permita que las aguas del Paraná, ayude a ese este cordobés que tiene dificultades”.

“El acceso al agua es a esta altura de los acontecimientos un derecho humano. En verdad es imposible pensar en el desarrollo de una comunidad o de una sociedad que no tenga acceso a agua potable, por lo tanto estamos dando una solución a un problema que lleva bastante tiempo discutiéndose y que ahora sí podemos poner en marcha”, remarcó Fernández.

Finalmente, el embajador de Kuwait en Argentina, Abdullah Ali Alyahya, expresó: “Atravesamos 50 años de relaciones bilaterales, y eso está en constante desarrollo. Espero que logremos más cosas juntos, y estoy confiado que en el futuro cercano vamos a conseguir cosas muy importantes a nivel del intercambio comercial y también con otros proyectos que estamos confiados que vamos a poder realizar con nuestros hermanos argentinos”.



**ETAPA 1**

En la Etapa 1 se invertirán unos 250 millones de dólares y el acueducto conectará las ciudades de Coronda con la de San Francisco en Córdoba. La obra incluye toma de agua en el río Paraná, el acueducto de agua cruda, la construcción de una planta de potabilización y el acueducto de agua tratada hasta la ciudad de San Francisco.

El proyecto cuenta con la financiación del Fondo Soberano de Kuwait y beneficia a la población de 65 localidades, que comprenden a medio millón de personas al momento de finalización de las dos etapas del proyecto.