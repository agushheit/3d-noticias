---
category: La Ciudad
date: 2021-07-21T06:15:44-03:00
thumbnail: https://assets.3dnoticias.com.ar/autopista.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Autopista: vuelven este miércoles las demoras en el tránsito por trabajos
  de repavimentación'
title: 'Autopista: vuelven este miércoles las demoras en el tránsito por trabajos
  de repavimentación'
entradilla: Se trabajará de 8 a 18 horas en zona de banquinas, manteniéndose interrumpidas
  las salidas hacia ambos sentidos de avenida Circunvalación.

---
En el marco de las obras de repavimentación que se desarrollan sobre la autopista Santa Fe-Rosario en el ingreso a la ciudad capital, este miércoles 21 de julio continuarán los trabajos de 8 a 18 horas en zona de banquinas, manteniéndose interrumpidas las salidas hacia ambos sentidos de avenida Circunvalación.

Los desvíos, nuevamente, serán por avenida Presidente Perón hasta la rotonda de Barrio Hipódromo. Durante la jornada de miércoles quedará habilitado el acceso a Santa Fe desde Circunvalación Sur.

Este martes comenzaron los trabajos de repavimentación sobre la autopista Santa Fe-Rosario entre los km 155-156, sentido a Santa Fe. La obra, generó demoras en uno de los accesos más importantes que tiene la ciudad capital durante casi toda la jornada, desde las 8 y hasta las 18.

Durante ese lapso, se interrumpieron los ingresos y salidas desde y hacia circunvalación en relación con la mencionada autopista.

Es decir, ningún vehículo -que circuló sentido a Santa Fe- pudo utilizar el intercambiador que conecta la autopista Santa Fe-Rosario con circunvalación oeste.

En ese sentido, todo el tránsito debió continuar por Iturraspe, llegar a Av. Pte. Perón y doblar a la izquierda. Luego tomar Av. Blas Parera hasta llegar a la rotonda de B° Hipódromo para retomar circunvalación.

En tanto, quienes provengan desde circunvalación oeste a Santa Fe, tuvieron que continuar hasta intercambiador de Av. Intendente Irigoyen (Centenario) y retomar circunvalación en sentido contrario.

Todos estos desvíos se mantendrán durante la jornada de miércoles.