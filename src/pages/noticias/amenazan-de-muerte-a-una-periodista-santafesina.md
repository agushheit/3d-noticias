---
category: La Ciudad
date: 2020-12-02T19:54:19Z
thumbnail: https://assets.3dnoticias.com.ar/4.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Amenazan de muerte a una periodista santafesina
title: Amenazan de muerte a una periodista santafesina
entradilla: Marisa Escobar es periodista de la ciudad de Santa Fe. Conduce el programa
  "Mujeres Emprendedoras" que se emite por la señal Somos Santa Fe y está a la cabeza
  de "Marisa Escobar se conecta" por la señal de Radio Gol.

---
A principios del mes de octubre, Marisa invitó a su programa radial a Andrea Patiño, víctima de violencia de Género a quien su ex marido le impedía tener contacto con su hija.

Andrea había pedido ayuda para dar a conocer su caso porque no obtenía respuestas de las autoridades y se sentía sin salida.

En días previos al día de la madre, la historia de violencia se hizo conocida de la mano de Marisa y, a raíz de la exposición y visibilización que tuvo el caso , tanto Andrea como Marisa son constantemente amenazadas de muerte razón por la cual ambas están con custodia policial.

![](https://assets.3dnoticias.com.ar/3.jpeg)

El pasado 24 de noviembre, previo al festejo del día internacional de la NO violencia contra la mujer, Andrea sufrió la forzadura y apertura de su auto particular con destrozos en su interior. Además, el victimario se tomó el trabajo de dejar una nota con amenazas escritas sobre los asientos: "Dejá de joder con tu hija. Ahora es el auto, la próxima sos vos je je je" rezaban los papeles. Similar situación vivió Marisa Escobar en su vehículo particular. En este caso sin notas intimidatorias pero si con situaciones de vandalismo. Todas las pruebas fueron aportadas a la policía donde se asentó la denuncia correspondiente.

![](https://assets.3dnoticias.com.ar/1.jpeg)

Pero la historia no termina ahí. Ayer martes, 1 de diciembre, realizaron un nuevo atentado contra la integridad física de Andrea Patiño. Alguién había mandado a publicar un obituario con su nombre en un diario local de gran tirada. Ante esta situación, se realizó la denuncia policial correspondiente y desde el programa "Mujeres Emprendedoras" convocan a una manifestación frente a los Tribunales de Santa Fe solicitando intervención de las autoridades locales.

![](https://assets.3dnoticias.com.ar/2.jpeg)

![](https://assets.3dnoticias.com.ar/5.jpeg)

Mirá la nota completa en nuestro [canal de Youtube ](https://youtu.be/snxO_w-BYts "3DNoticias")