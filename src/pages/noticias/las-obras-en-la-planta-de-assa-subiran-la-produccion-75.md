---
category: La Ciudad
date: 2022-01-23T06:15:57-03:00
thumbnail: https://assets.3dnoticias.com.ar/AGUACONCEJO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Las obras en la planta de Assa subirán la producción 75%
title: Las obras en la planta de Assa subirán la producción 75%
entradilla: 'Las obras iniciarán en marzo con una inversión que supera los $5.200
  millones. Aseguran que "se cubrirá la demanda de agua en la ciudad hasta el año
  2050".

  '

---
El Ministerio de Infraestructura, Servicios Públicos y Hábitat, a través de la Secretaría de Empresas y Servicios Públicos anunció para el mes de marzo, el inicio de las obras de ampliación de la Planta Potabilizadora de Assa de la capital provincial, tras recibir la autorización de Nación, quien dispuso la contratación de la Unión Transitoria Obring S. A. – Supercemento S. A. I. C. – Basaa S. A. por la suma de $5.214.588.717,81, con un plazo de ejecución de 24 meses.

Al respecto, el secretario de Empresas y Servicios públicos, Carlos Maina señaló: “Este es un hito para la obra pública en la ciudad, porque la planta potabilizadora fue inaugurada en 1907, luego en 1996 se puso en funcionamiento la sala de filtros y este año vamos a ampliar la planta, incrementando su producción en un 75 por ciento y proyectando la demanda cubierta por 30 años más. Con esta obra llegaremos con agua a los barrios que hoy no tienen”.

"Sin dudas, hay una firme decisión política del gobernador Omar Perotti de avanzar con infraestructura sanitaria en todo el territorio y mejorar las condiciones de vida para los santafesinos, porque cuando hablamos de redes de agua potable y de cloacas, también estamos hablando de salud pública”, concluyó Maina.

**Las obras**

Cabe recordar que la obra fue licitada el 19 de octubre pasado por Assa y contemplaba un presupuesto oficial de $4.420.100.154,46 (en base a mayo de 2021), con fondos provenientes del Estado nacional a través del Ente de Obras Hídricas de Saneamiento (Enhosa) y tendrá un plazo de ejecución de 730 días.

Los trabajos incluyen la construcción de una nueva toma de captación de agua cruda, nuevos módulos de potabilización de alta tasa, diferentes reformas y mejoras operativas en sectores de producción, sistema de tratamiento de lodos y descarga de efluentes, y un nuevo laboratorio de calidad.