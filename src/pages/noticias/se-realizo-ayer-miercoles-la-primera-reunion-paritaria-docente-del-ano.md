---
category: Agenda Ciudadana
date: 2022-02-17T10:17:22-03:00
thumbnail: https://assets.3dnoticias.com.ar/2022-02-17NID_273826O_1.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3d noticias
resumen: Se realizo ayer miércoles la primera reunión paritaria docente del año.
title: Se realizo ayer miércoles la primera reunión paritaria docente del año.
entradilla: Se acordó una agenda de temas para trabajar conjuntamente en el ámbito
  de las comisiones técnicas.

---
La ministra de Educación de la provincia, Adriana Cantero, y su par de Trabajo, Empleo y Seguridad Social, Juan Manuel Pusineri, participaron este miércoles del inicio de las reuniones paritarias con autoridades de los sindicatos del sector docente.

En el encuentro –que se realizó de manera virtual- estuvieron presentes representantes de los gremios Amet, Sadop, Amsafe y UDA, funcionarios de los ministerios de Economía, de Educación y de Trabajo, así como responsables del Boleto Educativo Gratuito.

Al término del encuentro Cantero comentó que “en esta primera reunión todas las representaciones sindicales plantearon temas para poder construir una agenda conjunta y acordamos empezar el trabajo en comisiones técnicas para abordar temas que tienen que ver con las condiciones de trabajo, concursos y también por supuesto una comisión técnica salarial. En estos días vamos a estar celebrando esos encuentros técnicos y seguiremos dándole continuidad al diálogo paritario”.

Asimismo, se planteó la ampliación del alcance del Boleto Educativo Gratuito para que este beneficio alcance a más docentes, especialmente los docentes rurales y los que desempeñan sus tareas en centros de formación laboral.

Respecto del tema salarial, Pusineri ratificó la voluntad del gobierno de la provincia de que el salario docente se ubique por encima de la inflación real: “Vamos a garantizar una oferta que pueda ser analizada por los gremios antes del inicio del ciclo lectivo. Nuestra aspiración es que las clases inicien el 2 de marzo y todos los esfuerzos están orientados hacia ello”, agregó el funcionario.

Finalmente, las partes acordaron que el próximo encuentro se convocará en función del avance de los acuerdos alcanzados por las comisiones técnicas.