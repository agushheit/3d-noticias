---
category: Estado Real
date: 2022-01-27T06:15:30-03:00
thumbnail: https://assets.3dnoticias.com.ar/perottiyalberto.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Perotti y Fernández, firmaron un prestamos por 100 millones de dolares para
  financiar el programada Santa Fe+ Conectada
title: Perotti y Fernández, firmaron un prestamos por 100 millones de dolares para
  financiar el programada Santa Fe+ Conectada
entradilla: La provincia de Santa Fe aportará 24,6 millones de dólares. “Vamos a llevar
  4.000 kilómetros de fibra óptica para unir las 365 ciudades y pueblos de nuestra
  provincia”, dijo el gobernador.

---
El gobernador de la provincia, Omar Perotti; y el presidente de la Nación, Alberto Fernández, firmaron este miércoles un préstamo con el CAF -Banco de Desarrollo de América Latina- para financiar el "Programa de Inclusión Digital y Transformación Educativa Santa Fe+Conectada".

En la oportunidad, Perotti remarcó que “dar un salto en la conectividad en nuestra provincia es clave, ya que venimos con un retraso importante que la pandemia dejó al desnudo”.

Del mismo modo, el mandatario recordó que “fue un largo trabajo que iniciamos en conversaciones con el CAF previo a asumir, que luego tuvieron un impasse en la Legislatura, ya que para aprobar la ley la Cámara de Diputados demoró el mismo tiempo que el que demandó realizar todas las otras gestiones hasta llegar a esta instancia final”, sostuvo el gobernador de la provincia.

Y agregó: “Lo que estamos haciendo hoy nos permite desarrollar un proyecto estratégico para la provincia de Santa Fe, con el compromiso de todos los sectores de recuperar este tiempo y, en el menor tiempo posible, poder llamar a licitación, la cual queremos que sea en los primeros días de febrero”, dijo Perotti.

En ese sentido, el gobernador santafesino detalló que “vamos a llevar 4.000 kilómetros de fibra óptica para unir las 365 ciudades y pueblos de nuestra provincia, para dar un salto en algo que puso en evidencia la pandemia, donde 500 mil alumnos santafesinos quedaron fuera de la posibilidad de estar conectados”, explicó.

Además, el gobernador destacó la importancia de contar con conectividad en toda la provincia también para la medicina, la descentralización operativa del gobierno provincial, la justicia y la seguridad. “Es un proyecto estratégico para nosotros, estar acompañados por el Presidente nos compromete. Es un proyecto que va a ser exitoso en uno de sus planteos: afianzar el federalismo y arraigar a nuestra gente en sus lugares de origen”, concluyó Perotti.

Por su parte, el presidente de la Nación, Alberto Fernández, insistió en que “debemos dar los pasos para el desarrollo inclusivo en la Argentina. Nosotros necesitamos un país que pueda encontrar los caminos del crecimiento y desarrollo seguros. La conectividad, hoy en día, es un elemento esencial. Para el desarrollo y el funcionamiento de los gobiernos y de las empresas, internet es el camino para acercar distancias. Por lo tanto, todo lo que hacemos en favor de la conectividad es definitivamente un servicio que garantiza más conocimiento y desarrollo”, manifestó Fernández.

Y añadió: “Hay una inversión muy importante, que agradezco al Banco de Desarrollo de América Latina que piense en inversiones. Por esta vía estamos llegando y dando internet a los sectores más postergados, a chicos que necesitan conectarse para adquirir conocimientos y saberes. Para que el conocimiento hoy en día pueda ser real, la conectividad debe ser esencial”, finalizó el presidente de la Nación.

El encuentro se realizó en la Casa Rosada y del mismo participaron también el secretario de Asuntos Estratégicos de la Presidencia de la Nación, Gustavo Béliz; el subsecretario de Relaciones Financieras Internacionales para el Desarrollo, Leandro Gorgal; el ministro de Economía de la provincia, Walter Agosto; la secretaria de Gestión Federal de la Provincia, Candelaria González del Pino; y el representante del Banco de Desarrollo de América Latina, Santiago Rojas Arroyo.

**Inclusión digital**

Por su parte, Gorgal sostuvo que “esto significa el acompañamiento a la provincia en una importante política de conectividad. Es un préstamo del CAF, que desde la Nación acompañamos siendo garantes de esta operación. Esto va en línea con uno de los ejes estratégicos que ha definido el secretario Gustavo Béliz, en lo que tiene que ver con financiamiento internacional, que es apoyar a la inversión en conectividad digital. Un paso clave para avanzar en el desarrollo. Hoy por hoy cualquier actividad económica, educativa, social requiere de la conectividad como un insumo básico. Así que esto es un importante proyecto de la provincia de Santa Fe que va a impactar positivamente en más de tres millones de habitantes”.

Por otro lado, Agosto afirmó que “desde el principio de la gestión el gobernador ha estado trabajando en esta línea y hoy estamos dando un paso trascendente. A lo largo de todo este tiempo la provincia ha ido avanzando en una serie de cuestiones muy vinculadas a este proyecto y se han avanzado en licitaciones para ir accediendo con wifi gratis a barrios populares de Santa Fe y Rosario”.

Por último, Rojas destacó que “este es uno de los proyectos más importantes de CAF en Argentina”; y resaltó el “trabajo con la provincia de Santa Fe, con el gobernador, con el equipo del gobierno nacional, para llegar a este importante proyecto, que va a beneficiar a más de 3 millones de santafesinos en temas de internet y conectividad, llegando a barrios populares, a aquellos ciudadanos que hoy no tienen o tienen mala calidad de internet”.

“Estamos hablando de inclusión digital, de trabajar con el sector educativo, de salud, de seguridad de manera integral, para brindar soluciones de conectividad a la provincia de Santa Fe”, concluyó Rojas.

**Santa Fe+Conectada**

El “Programa de Transformación Digital e Inclusión Educativa”, que permitirá ampliar y modernizar la infraestructura del sistema de conectividad, contará con un financiamiento de 124,6 millones de dólares: 100 millones financiados por el CAF -Banco de Desarrollo de América Latina- y 24,6 millones que aportará la provincia.

La ejecución del referido programa, se estructura en cinco componentes: extensión de la Red Provincial de Datos de Santa Fe; fortalecimiento y modernización de la infraestructura del sistema educativo provincial; desarrollo de habilidades y competencias digitales para la gestión educativa a distancia; gestión del programa y otros gastos.

Además, busca garantizar conectividad de calidad a las 365 localidades de la provincia y tiene por objetivo ampliar y modernizar la infraestructura del sistema de conectividad para promover la inclusión digital, el arraigo, la transformación educativa, la gestión empresarial y del Estado y la reducción de brechas tecnológicas, productivas y de género con impacto concreto para el desarrollo sostenible del territorio.

Con la expansión de 4.000 kilómetros de fibra óptica se beneficiará a más de 3.300.000 de habitantes de la provincia de Santa Fe. Por su impacto y efectos de derrame sobre el ecosistema social, económico y productivo del territorio se estima que el Programa beneficiará de manera directa, a través de las trazas troncales de Fibra Óptica de 10/40/100G de ancho de banda, a 200 localidades con 2.757.803 habitantes.

Incluye también la construcción de 30 Nodos de Acceso por Radio Enlace de alta capacidad para llegar así al resto de las localidades de la provincia con la red de datos, cubriendo de esta manera el 100% de las localidades santafesinas.

Cabe señalar que, a través de esta iniciativa, también se contempla que 134 barrios populares de las ciudades de Santa Fe y Rosario tengan acceso al wifi libre. Asimismo, el proyecto incluye la expansión de la infraestructura educativa con la construcción de más de 50 nuevas escuelas con acceso a wifi, en la totalidad de las nueve regiones educativas.