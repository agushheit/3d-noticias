---
layout: Noticia con imagen
author: 3DNoticias
resumen: "#25N"
category: Estado Real
title: "25N: Cómo es el abordaje del municipio ante situaciones de violencia de
  género"
entradilla: Desde que asumió la actual gestión municipal, la Dirección de
  Mujeres y Disidencias acompañó en 1.921 casos de violencia machista. Ya se
  instalaron  ocho Carteleras Violetas en distintos barrios  de Santa Fe
  Capital.
date: 2020-11-25T12:32:59.089Z
thumbnail: https://assets.3dnoticias.com.ar/violencia-genero.jpg
---
A través de un abordaje interdisciplinario, la Dirección de Mujeres y Disidencias de la Municipalidad de Santa Fe acompaña a quienes atraviesan situaciones de violencia de género. En el marco del **Día Internacional de la Eliminación de la Violencia contra las Mujeres que se conmemora cada 25 de noviembre**, desde el área que conduce Soledad Artigas se repasa cómo es el trabajo que se realiza.

Además, **la Municipalidad lanza hoy la campaña Carteleras Violetas**, que consiste en la instalación de señalética con información para actuar ante situaciones de violencia de género, una iniciativa de la concejala Laura Mondino que fue trabajada con las vecinas y los vecinos en las redes de instituciones dentro del Plan Integrar. Este miércoles, se habilitará la de Barranquitas (en Iturraspe y Perón), y se suma a los carteles ya colocados en los barrios San Agustín, Villa Hipódromo, Cabal y Coronel Dorrego, como también a los tres instalados en Parque del Sur, Parque Federal y Costanera Este.

### **Proximidad**

“Somos el primer nivel de atención, somos quienes estamos más cerca, en la ciudad de Santa Fe, de las situaciones de violencia. Nuestro trabajo consiste cotidianamente en acompañar a las mujeres, en contarles cuáles son sus posibilidades, cómo tienen que denunciar, plantear estrategias para su cuidado, proponerles salidas y acompañarlas en ese proceso”, sintetiza la funcionaria, quien sostiene que realizar denuncias y transitar juzgados para solicitar medidas de protección “es una tarea que si una la emprende sola a veces puede ser engorroso y tiende a revictimizar una situación emocional que ya es terrible. Por eso nuestro trabajo es estar alivianando todo ese proceso que es difícil”.

Desde su jerarquización, la Dirección de Mujeres y Disidencias atendió 1.921 casos de violencia de género. Del total, 149 corresponden a diciembre de 2019, 196 a enero, 132 a febrero, 117 a marzo, 105 a abril, 118 a mayo, 215 a junio, 204 a julio, 165 a agosto, 180 a septiembre, 169 a octubre y 171 a lo que va de noviembre. El total incluye tanto nuevas consultas como el seguimiento mensual de distintos casos.

### **Vínculo con el municipio**

Además de acercarse las mujeres de forma directa a la sede de la Dirección de Mujeres y Disidencias del municipio ubicada en 25 de Mayo 2884, de lunes a viernes, de 8 a 18, o comunicarse a la Línea de Atención Ciudadana 0800 777 5000 que funciona las 24 horas, son muchos los casos que llegan al área a través de la derivación de otros organismos estatales.

Tal es el caso de los servicios sociales y guardias de hospitales públicos, los Centros de Atención Primaria de la Salud, la Línea Nacional gratuita 144, la Secretaría de Estado de Género e Igualdad provincial, y dependencias policiales. Al respecto, Artigas explicó que "todos los organismos públicos que se ven en contacto directo con alguien que se acerca a preguntar o a contar una situación, en el marco de alguna otra tarea o de otros servicios, nos contactan, y ese es otro método de ingreso".

Cuando las mujeres se acercan desde la dirección se realizan entrevistas con un equipo interdisciplinario integrado por una psicóloga y una abogada, formadas en perspectiva de género "que hacen una primera escucha y una contención emocional, porque es muy difícil ver a las mujeres llegar a ese momento", expresa Artigas. Luego, se plantea la estrategia de dónde y cómo hacer las denuncias, y se las acompaña.

También, en este proceso de atención, se define la estrategia de protección. En ese sentido, Artigas se cuestiona: "Esta mujer que llega, ¿puede volver a su casa? Muchas veces no. ¿Tiene un lugar dónde ir, una amiga, un familiar, una compañera que la pueda alojar? Mientras generamos la denuncia y conseguimos las medidas de protección, si tiene un lugar se la acompaña en ese proceso y, si no tiene un espacio donde transcurrir ese tiempo, le damos alojamiento en la Casa de Protección Municipal, que está en funcionamiento los 365 días del año”, detalla y concluye que “cada situación amerita activar una serie de estrategias particulares”.

### **El seguimiento**

Tras el primer acercamiento entre las mujeres y el municipio, se realiza el seguimiento y monitoreo de cada caso, la mayoría de las veces vía telefónica. Sobre este proceso, Artigas planteó que “lamentablemente lo que vemos es que los varones incumplen las medidas judiciales, incumplen las medidas de distancia, se vuelven a acercar, vuelven a generar una situación de violencia y entonces muchas veces las mujeres se ven obligadas a recurrir nuevamente al municipio, a llamarnos por teléfono para avisarnos lo que les está pasando".

**La asistencia a mujeres desde el municipio es permanente**: las llamadas recibidas por fuera del horario de atención habitual (de lunes a viernes de 8 a 18) a la **Línea de Atención Ciudadana 0800 777 5000** son derivadas a la guardia integrada por un equipo interdisciplinario que interviene ante situaciones de violencia de género, de lunes a viernes, de 18 a 8 horas, como así también los fines de semana y feriados.

### **Abordaje en pandemia**

"Para quienes prestamos servicios esenciales fue muy complejo. Ni bien se dictó el Aislamiento Social, Preventivo y Obligatorio el 20 de marzo, fuimos convocados por el intendente Emilio Jatón, quien obviamente nos pidió que el servicio siguiera intacto, que tratáramos de sostenerlo, de utilizar todos los recursos que estuvieran al alcance del municipio para que el servicio no deje de prestarse, para no disminuir su calidad", cuenta la funcionaria sobre cómo fue reacomodarse, para una gestión de solo tres meses en funcionamiento, al contexto de pandemia.

En ese sentido, recuerda que crear una nueva dinámica laboral tuvo una cierta complejidad: "Fue muy costoso, y lo fuimos organizando como toda la Municipalidad, trabajando por grupos, tratando de tener muchísima precaución y cumplir con todas las medidas sanitarias que las autoridades nos indican".

Por la imposibilidad de realizar trámites presenciales, desde abril el Ministerio Público de la Acusación y después el Juzgado de Familia habilitaron la atención a través de correos electrónicos oficiales. Así, las mujeres pudieron realizar denuncias en línea y no tuvieron que asistir a comisarías o Centros Territoriales de Denuncias, y desde el municipio se pudieron gestionar medidas de protección.

Este nuevo escenario permitió que se redujera "bastante el tiempo entre que la mujer llega a la Dirección y logra tener las medidas de protección en su casa", indicó Artigas, y confío en que "se sostenga esta posibilidad de seguir haciendo cosas online, de agilizar y, sobre todo, generar menor carga a la mujer. La mujer que es víctima de violencia ya carga con una situación subjetiva muy compleja y a veces los organismos estatales les pedimos que sigan poniendo el cuerpo para ir a denunciar, para esperar que les den las medidas".