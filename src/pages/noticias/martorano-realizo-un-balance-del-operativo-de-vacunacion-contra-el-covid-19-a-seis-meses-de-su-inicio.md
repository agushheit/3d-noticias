---
category: Estado Real
date: 2021-06-30T08:32:49-03:00
thumbnail: https://assets.3dnoticias.com.ar/vacunación.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: Martorano realizó un balance del operativo de vacunación contra el Covid
  19 a seis meses de su inicio
title: Martorano realizó un balance del operativo de vacunación contra el Covid 19
  a seis meses de su inicio
entradilla: La ministra de Salud destacó que se otorgaron más de 40 mil turnos en
  un día y la campaña continúa a paso firme en toda la provincia.

---
La ministra de Salud, Sonia Martorano, destacó que junio representó un mes histórico en lo que respecta al avance de la campaña nacional de inmunización contra el coronavirus en la provincia de Santa Fe, el día en que justamente se cumplieron seis meses del inicio de su implementación (29 de diciembre de 2020), fecha en la que además coincidió con el otorgamiento de 41.396 turnos en un día, lo que constituye un verdadero récord hasta el momento.  

En este sentido, Martorano celebró este avance –entre otros de importancia– en el marco de una recorrida por el hospital regional de Venado Tuerto, en donde observó el acelerado ritmo de inoculación del efector, felicitó a los trabajadores de la salud por el modo en que están gestionando la atención de los casos de Covid-19 que requieren internación, entre otros aspectos.

“Es muy grato para mí estar recorriendo este hospital Gutiérrez en donde alguna vez trabajé, constatando además que estamos bien en Venado Tuerto y en la región en lo que a vacunación respecta, y fundamentalmente a nivel provincial porque junio fue récord: ya se aplicaron 800.000 dosis solo en este mes, estamos arriba de 1.3000.000 primeras dosis y llegando a las 340.000 segundas dosis aplicadas”, comenzó expresando la ministra de Salud de la provincia. 

“Además –celebró Martorano– coincidiendo con los seis meses de campaña, hoy fue un día récord porque por primera vez hemos entregado 41.396 turnos. Eso en el marco de otros logros: se sumaron 40.000 mil turnos a los 140.000 que dimos el viernes de la semana pasada; en junio ya se entregaron casi 800.000 turnos; estamos vacunando a menores de 40 años sin comorbilidades en toda la provincia; y ya se turnó el 80% de las personas gestantes”.

**Destacable trabajo sin descanso de los equipos de Salud**

Mientras recorría la instalaciones del hospital regional de Venado Tuerto –acompañada por el secretario de Municipios y Comunas José Luis Freyre, el responsable de esa región de Salud Pedro Bustos y por el director del efector Daniel Alzari–, Sonia Martorano expresó que se trata de un lugar “muy querido” por ella y “muy pujante”, al tiempo que agregó: “Queremos acompañar y agradecer aquí a los  vacunadores y las vacunadoras que trabajan sin descanso, sin sábado, ni domingo, ni feriado; porque estamos todos comprometidos y la idea es que, como dice nuestro gobernador Omar Perotti, cada dosis que llegue vaya rápidamente al brazo de cada santafesino,  que es el mejor lugar para estar”. 

Profundizando, Martorano precisó que hoy la provincia está en un  37% de población vacunada, un poco más de 1.300.000 de la primera dosis, y 330.000 tiene el esquema completo. 

Asimismo informó que de la mano de llegada de las vacunas se avanza de este modo: “En primera dosis y, con lo que llegue de AstraZeneca, estamos completando los esquemas de segundas dosis. Por eso decía que dando 41.000 turnos por día estamos yendo muy rápido y dependemos de la llegada de las vacunas”.  

Para cerrar, destacó: “Esto va de la mano de lo que queremos lograr: la inmunidad en nuestra gente, porque tenemos una amenaza enfrente. Si bien yo creo que estamos saliendo de la segunda ola, parece que en estos días estarían bajando los casos, pero el riesgo de la variante Delta”.