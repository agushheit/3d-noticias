---
category: La Ciudad
date: 2020-12-22T11:19:35Z
thumbnail: https://assets.3dnoticias.com.ar/2212ciudad.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: "«Santa Fe libre de pirotecnia»: sigue vigente la ordenanza"
title: "«Santa Fe libre de pirotecnia»: la Municipalidad recuerda que está vigente
  la ordenanza"
entradilla: Se apela a la conciencia ciudadana, cuidando la salud de las personas
  y de los animales. Se harán controles para las fiestas por Navidad y Año Nuevo tanto
  de productos como de locales no habilitados.

---
Desde fines de 2019 rige en la ciudad la ordenanza Nº 12.429 que establece a Santa Fe como territorio «libre de pirotecnia», es decir que queda «prohibido fabricar, vender, manipular, transportar y usar pirotecnia de todo tipo, salvo que se tramite un permiso especial en el municipio, que solo admite elementos lumínicos de bajo impacto sonoro».

Desde la Municipalidad se recuerda que la normativa está vigente y se apela a la conciencia de los ciudadanos para dar cumplimiento a la misma. Además de controlar que se cumpla con la normativa, se apunta también a informar y sensibilizar a vecinos y vecinas sobre los alcances de la ordenanza.

En este marco, funcionarios de la Secretaría de Control y Convivencia Ciudadana mantuvieron reuniones con agentes de Brigada Explosivos de la provincia para llevar a cabo en conjunto los operativos de control sobre la venta ilegal callejera y en locales comerciales. 

Esta semana también se enviaron notificaciones a los locales que comercializaban estos elementos para recordarles la vigencia de esta ordenanza.

## **Excepción**

Es necesario aclarar que dentro de la normativa hay una excepción que permite la realización de espectáculos con fuegos artificiales lumínicos de bajo impacto audible, pero para poder utilizarlos se necesita un permiso municipal que se otorga cuando se solicita hacer el evento. El pedido se puede gestionar a través de la ventanilla única de eventos virtual. Por tanto, es potestad del Ejecutivo otorgar la autorización.

Además, se recuerda que no se dan permisos provisorios o temporarios para la venta de estos productos a comercios.

La ordenanza tiene entre sus fundamentos la protección de aquellas personas y animales que se ven afectados por los riesgos que produce la pirotecnia prohibida de impacto audible.