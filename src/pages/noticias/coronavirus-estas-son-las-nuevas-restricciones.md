---
category: Agenda Ciudadana
date: 2021-04-08T07:01:23-03:00
thumbnail: https://d7kqr73uznmf6.cloudfront.net/2020-10-07.jpg
layout: Noticia con video
link_del_video: https://www.youtube.com/watch?v=ANPgy8i8Bfo
author: con información de LT10
resumen: 'Coronavirus: éstas son las nuevas restricciones'
title: 'Coronavirus: éstas son las nuevas restricciones'
entradilla: Las medidas regirán desde el viernes hasta el 30 de abril en todo el territorio
  nacional. Gobernadores solo podrán ampliar o reducir horarios.

---
El presidente Alberto Fernández advirtió que en las últimas semanas "el relajamiento social" creció y  anunció las nuevas restricciones que se impondrán a partir de las 00 de este viernes y hasta el 30 de abril,  con la publicación de un nuevo DNU.

El mandatario argentino aclaró que las medidas regirán en todo el país y los gobernadores solo podrán ampliar o reducir horarios. 

Las nuevas medidas son:

\-    Se suspenden para todo el país los viajes grupales de egresados , de estudio y de grupos turísticos.

\-    En las zonas del país consideradas de mediano riesgo epidemiológico y sanitario es facultad y responsabilidad de gobernadores y gobernadoras adoptar en forma temprana medidas que disminuyan la circulación para prevenir los contagios. Ello es así, pues es del resorte exclusivo de las provincias el monitorear y hacer cumplir cualquier medida restrictiva de la circulación.

\-    En las zonas del país donde hay mayor riesgo epidemiológico y sanitario, se tomarán además las siguientes medidas. 

      a.    Se suspenden actividades sociales en domicilios particulares.

      b.    Se suspenden reuniones sociales en espacios públicos al aire libre de más de 20 personas.

      c.    Se suspenden actividades de casinos, bingos, discotecas o cualquier salón de fiestas.

      d.    Se suspende la práctica recreativa de cualquier deporte en lugares cerrados donde participen más de 10 personas.

      e.    Se establece el cierre de los bares y restaurantes a partir de las 23.

      f.    Se prohíbe la circulación entre las 00.00 y las 06.00 de la mañana de cada día. Según las jurisdicciones, las autoridades podrán solo ampliar estos horarios en función de las especificidades de cada lugar.

\-    Específicamente, en el AMBA sólo podrán usar el transporte público de pasajeros trabajadores considerados esenciales, toda la comunidad educativa y aquellos que ya fueron expresamente autorizados.

\-    Estas medidas de cuidado estarán vigentes desde las 00 de este viernes 9 de abril hasta el 30 de abril. El Gobierno Nacional evaluará cuidadosamente la correcta implementación de las medidas.