---
category: Agenda Ciudadana
date: 2021-11-28T06:00:52-03:00
thumbnail: https://assets.3dnoticias.com.ar/CORNEJO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: 'La oposición emitió una dura respuesta a CFK por el FMI : "Hágase cargo"'
title: 'La oposición emitió una dura respuesta a CFK por el FMI : "Hágase cargo"'
entradilla: 'El primero de los dirigentes de la oposición que recogió el guante fue
  el diputado nacional y titular del radicalismo, Alfredo Cornejo.

'

---
Dirigentes de Juntos por el cambio emitieron hoy una dura respuesta a la carta de la vicepresidenta Cristina Kirchner sobre el acuerdo con el FMI, y le pidieron a la ex mandataria que se haga "cargo" de la situación.

"Hágase cargo, deje las cartas y póngase a escribir alguna propuesta que nos saque del desastre económico que hicieron", apuntó apuntó el diputado nacional y titular del radicalismo, Alfredo Cornejo, en su cuenta de la red social Twitter.

En ese marco, el dirigente radical cuestionó a la vicepresidenta por considerar que "la culpa es siempre del otro", y le pidió "alguna propuesta" para que la Argentina pueda salir del "desastre económico que hicieron" desde la coalición oficialista.

"Para Cristina Kirchner la culpa es siempre del otro. Pero que Alberto Fernández sea presidente es suya", subrayó Cornejo.

Por su parte, la titular del PRO, Patricia Bullrich, le recomendó la vicepresidenta que "no se preocupe" por la oposición, porque cumplir con su "responsabilidad", y pidió que desde el Ejecutivo nacional "definan un plan de gobierno de una vez".

"No se preocupe por nosotros, señora de Kirchner; cumpliremos con nuestra responsabilidad", posteó Bullrich en su cuenta de Twitter.

Además, la presidenta del PRO apuntó: "Ahora cumplan ustedes: dejen de postergar todo, definan un plan de gobierno de una vez y ordenen el desvarío económico, social y de inseguridad en el que vivimos".

"Finalmente el Gobierno reconoció nuestro triunfo de manera formal a través de su Vicepresidenta. Logramos un objetivo estratégico ineludible: por primera vez desde 1983 el peronismo no tendrá quórum propio en el Senado de la Nación", concluyó.

En esa línea, el diputado nacional electo por Juntos por el Cambio y ex ministro de Educación de la Nación Alejandro Finocchiaro afirmó que el "objetivo" de la vicepresidenta "siempre fue" el de alcanzar "la impunidad" en las causas judiciales.

"La Vicepresidente escribe una carta diciendo que ella no incide en las decisiones del Gobierno. Nunca le creí nada y menos esto. Igual, su objetivo siempre fue otro: la impunidad", publicó Finocchiaro en su cuenta de Twitter.

En tanto, el diputado electo José Luis Espert calificó como "una sarta infernal de estupideces" las declaraciones de la ex mandataria.

"En buen romance, separando la paja del trigo y dejando de lado una sarta infernal de estupideces que dice, Cristina quiere defaultear la deuda con el FMI. O sea, quiere prender fuego el país. Linda Vice, no?", publicó Espert.

En un segundo tuit, el flamante diputado sentenció: "Dice...'la definición que se adopte y se apruebe, puede llegar a constituir el más auténtico y verdadero cepo del que se tenga memoria para el desarrollo y el crecimiento CON INCLUSIÓN SOCIAL de nuestro país'. Hoy, un acuerdo con el FMI es ajuste por desastre que los K hicieron".