---
category: Agenda Ciudadana
date: 2021-05-06T08:36:20-03:00
thumbnail: https://assets.3dnoticias.com.ar/creditos.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Senado Nacional
resumen: SE ANALIZARON PROYECTOS DE RENEGOCIACIÓN DE CRÉDITOS HIPOTECARIOS UVA
title: SE ANALIZARON PROYECTOS DE RENEGOCIACIÓN DE CRÉDITOS HIPOTECARIOS UVA
entradilla: Durante la comisión se presentaron dos iniciativas y expusieron referentes
  hipotecados UVA

---
La Comisión de Economía Nacional e Inversión se reunió esta tarde para debatir dos proyectos de ley, el del senador por Mendoza, Julio Cobos (UCR) que establece la renegociación de contratos para créditos hipotecarios UVA, y otro del senador por Jujuy, Mario Fiad (UCR) que regula las operaciones en los sistemas de créditos con capital ajustable UVA y UVI. 

La presidenta de la comisión, la senadora por Tucumán, Silvia Beatriz Elías de Pérez (UCR) abrió el encuentro destacando que "en un país donde hubiésemos tenido un crecimiento económico y menos inflación hoy no estaríamos preocupados por los créditos UVA". Sostuvo que "se ha producido a un enorme salto en las cuotas".

El senador Cobos explicó que su iniciativa "prevé que las entidades financieras ofrezcan la posibilidad de renegociación de contratos de créditos para la adquisición, construcción o ampliación de vivienda única, con cláusula de ajuste en UVA".

Sostuvo que "propone que se suspendan las ejecuciones hipotecarias y los desalojos para los créditos hipotecarios en UVA para vivienda única familiar por el término de un año". 

El senador Fiad consideró que "el sistema falló a la hora de dar una respuesta habitacional. Existe una gran desproporción entre  la cuota y el ingreso dado que estaba atado a la inflación, sumado a la pandemia".  "El proyecto de mi autoría tiene como objeto mejorar la situación económica de muchas familias que accedieron a los créditos UVA", destacó.. 

Manifestó que su iniciativa "busca la regulación de las operaciones enmarcadas en los sistemas de créditos con capital ajustable UVA y UVI (Unidad de Vivienda) previstas en las leyes 25.827 y 27.271 y propone que las actualizaciones se realicen mediante la aplicación del índice Ripte (Remuneración Imponible Promedio de los Trabajadores Estables) suministrado por el Ministerio de Trabajo".

Sostuvo que  "la cuota mensual resultante no debe exceder el 35% del ingreso del grupo familiar del deudor y dispone la suspensión por seis meses de las ejecuciones hipotecarias de viviendas adquiridas bajo la modalidad contemplada en la presente ley".

Tras la presentación de los proyectos, 5 referentes hipotecados UVA transmitieron su preocupación ante la Comisión.

La Dra Fernanda Yapur, integrante del colectivo "Hipotecados UVA" expresó que "solicitamos la readecuación del contrato, queremos poder pagar nuestros créditos, desde su nacimiento el crédito UVA ha crecido un 536% y no tiene tope". "La decisión es política ya que no se trata de un contrato entre privados. Agradecemos este espacio, necesitamos un amparo legal porque somos la parte débil del contrato", agregó.

La Sra. Romina Medina manifestó su "preocupación ante una cuota cada vez más difícil de pagar". Expresó que "apostamos nuestros ahorros de toda la vida para llegar al sueño de la casa propia, que ahora es una pesadilla porque nuestro crédito al ser indexado por un índice inflacionario nos expone ante una situación de incertidumbre y a un aumento desmedido".

El Sr. Fernando Salice, representante del colectivo hipotecado Mendoza, sostuvo que "de las deducciones de impuesto a las ganancias, habla solo del 40% de la parte de interés, yo entiendo que debería ser sobre la cuota total, tal como es en gastos médicos o alquiler que uno deduce de ganancias mensualmente".

La Sra. Perla Figueroa consideró que "hemos recorrido todos los pasillos y hemos podido hablar con legisladores y coincidíamos en este diagnóstico de la usura que implican las UVA. Somos trabajadores que queremos honrar nuestras deudas".

Por último, el Sr. Marcelo Macaluso aseguró que "nuestro día a día es un calvario, es muy duro pensar que si no tenemos respuestas va a pasar lo de siempre, muchas familias van a perder sus viviendas. Apelamos a su colaboración para poder frenar las ejecuciones y ponerle un fin a esto".