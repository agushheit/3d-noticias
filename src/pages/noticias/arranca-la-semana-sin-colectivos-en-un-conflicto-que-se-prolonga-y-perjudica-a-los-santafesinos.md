---
category: La Ciudad
date: 2021-07-12T06:15:15-03:00
thumbnail: https://assets.3dnoticias.com.ar/URBANOS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Arranca la semana sin colectivos en un conflicto que se prolonga y perjudica
  a los santafesinos
title: Arranca la semana sin colectivos en un conflicto que se prolonga y perjudica
  a los santafesinos
entradilla: Los trabajadores nucleados en UTA iniciaron el paro el pasado jueves 8
  de julio. "Esto va para largo", dijo uno de los representantes del sector empresario.

---
Desde las 13 del pasado jueves 8 de julio, los choferes de colectivos nucleados en la Unión Tranviarios Automotor están de paro. La medida es en reclamo al pago de aguinaldo y para que se cumpla el acuerdo paritario firmado. En ese sentido, advirtieron que solo cobraron la mitad del sueldo y con la antigua escala. La huelga afecta al transporte urbano e interurbano de la ciudad.

"Estamos esperando un llamado", le dijo a UNO Santa Fe, un referente gremial. Al mismo tiempo, una fuente vinculada al sector empresario señaló a este medio: "Hasta que no vengan fondos de algún lado, no hay posibilidad de pago alguna".

Lejos de ser optimista con respecto a la posibilidad de que el conflicto se resuelva y que el paro se levante, el representante de una de las empresas que tiene a cargo gran parte de las líneas que circulan por esta capital, apuntó: "Va para largo".

**Subsidios**

Desde la municipalidad de Santa Fe resaltaron atrasos en la transferencia de subsidios nacionales y provinciales. Se trata de recursos que se convirtieron en una herramienta indispensable para garantizar el funcionamiento de un servicio que no encuentra salida a una crisis cíclica y que lleva años.

En este contexto, la salida al conflicto parece lejana y por lo tanto la medida de fuerza continúa en el arranque de la semana. Días atrás, el subsecretario de Movilidad y Transporte de la Municipalidad de Santa Fe, Lucas Crivelli, aseguró "sostener el sistema de transporte en la ciudad de Santa Fe cuesta 130 millones de pesos por mes".

Pero advirtió: "Están llegando, entre los subsidios nacionales y los provinciales, 50 millones de pesos por mes. El resto se cubre con tarifa. Por lo cual, al haber bajado un 75 por ciento la cantidad de pasajeros y tickets, hoy la tarifa llega a cubrir aproximadamente entre el 50 y el 60 por ciento de los costos del sistema".

El secretario de Prensa de UTA, Sebastián Alem apuntó: "La situación ya no da para más. Este miércoles 7 de julio cobramos el 50% de los haberes, pero con la escala vieja, es decir sin el aumento correspondiente a la paritaria firmada recientemente, el cual era retroactivo al mes de mayo, así que deberíamos de haberlo cobrado".

Sobre el tema subsidios, subrayó: "Tenemos entendido que el gobierno nacional destina 8.000 millones de pesos para el subsidio del transporte, siempre y cuando las provincias hagan aportes similares y con sinceridad no sabemos si la provincia firmó o no".

"Hay pocos recursos para mantener el sistema"

El subsecretario de Movilidad y Transporte de la Municipalidad de Santa Fe Lucas Crivelli, en diálogo con UNO Santa Fe, se refirió a la crisis que generó la pandemia en el sector y a los problemas estructurales que vienen desde hace años.

—¿Cuánto cuesta hoy sostener el sistema de transporte de colectivos en la ciudad de Santa Fe?

—Hoy sostener el sistema de transporte en la ciudad de Santa Fe cuesta 130 millones de pesos por mes.

—¿Y cuántos subsidios llegan?

—Están llegando, entre los subsidios nacionales y los provinciales, 50 millones de pesos por mes.

—¿El resto se cubre con tarifa?

—El resto se cubre con tarifa. Por lo cual, al haber bajado un 75 por ciento la cantidad de pasajeros y tiques, hoy la tarifa llega a cubrir aproximadamente entre el 50 y el 60 por ciento de los costos del sistema.

—¿Y el resto?

—El resto son cambios estructurales que se van haciendo. Está claro que hay una dilación en el cambio de unidades. Todo 2020, 2021 y seguramente 2022, no habrá cambios de unidades a no ser que haya algún programa especial para eso. También hay que ser propositivos con Nación y con provincia porque todo contribuye. Esto está asociado a una cantidad menor de kilómetros de recorrido que se hace cambiando la frecuencia. Representa un ahorro, aunque no es mayor porque se ahorra combustible y algo de mantenimiento de los coches, pero los trabajadores del sector son exactamente la misma cantidad. Por otro lado, el costo laboral representa el 60% de los costos del sistema. Todo esto es fijo y no se puede mover. Este es el gran desafío de ver que el sistema siga siendo viable. Claramente no se puede obviar tener un sistema de transporte público porque el escenario sería caótico; sería ir en contra el mundo y en contra de lo que durante los últimos 30 años o 50 años el mundo ha propiciado.

—Le hacía esa pregunta porque en los momentos de crisis es cuando tiene que estar el Estado.

—Está claro que el Estado tiene que aparecer, hay que ver las formas. Puede ser como un prestador directo, puede aparecer como administrador de un sistema propiciando que se fomente a través del sector privado como es aquí en Santa Fe. Pero vemos que en todos los casos la situación de crisis hoy se está dando por igual. Rosario tiene 70% de su gestión pública como prestador. Córdoba tiene un sistema similar al nuestro y nosotros tenemos un sistema en el cual los prestadores son privados. Independientemente de cómo hoy está hecha la forma prestacional el sistema tiene una crisis en la cual los recursos necesarios son tan grandes que ningún municipio puede aportar. Si no estuviesen los subsidios de Nación para el Amba ya se hubiesen quedado sin transporte 10 años atrás y eso que tienen sistemas alternativos como los trenes y subte que también son solventados por recursos nacionales. Está claro que la presencia del Estado es fundamental. Pero necesitaríamos estar hablando de 120.000 o 130.000 pasajeros por día para poder animarme a decir que está la posibilidad de repensarlo desde el Estado para optimizarlo. Hoy en este esquema no hay recurso del Estado que aguante para poder pagar el agujero negro que tiene el sistema.

—En ese marco, ¿qué pasa con los subsidios?

—El Gobierno Nacional ha dispuesto para este año para todo el interior 20.000 millones de pesos contra 105.000 para el Amba. Ahí hay un primer problema que es estructural de lo que es la distribución de subsidios. Hay que reconocer que el gobierno nacional, con el tiempo, fue incrementando los suicidios, entre 2019 y ahora, de manera importante. Se pasó de 6.000 millones en 2019 a los 20.000 millones de este año. Si a eso le sumamos la promesa de 8.000 millones más, que permitiría sostener el incremento salarial del sector de UTA, siempre cuando las provincias que son la jurisdicción que regla el sistema de transporte independientemente, urbano o interurbano, se comprometa a un aporte de características similares

—¿La provincia está en condiciones de duplicar un aporte de ese tipo?

—Desde el municipio yo considero que sí. La provincia había iniciado un proceso muy interesante que venía de la mano del Boleto Educativo Gratuito, que preveía un presupuesto de 4.500 millones de pesos para poder asistir a ese sector. Es un boleto que no se ha ejecutado al tener a la educación en burbujas cada 15 días. La ejecución presupuestaria para ese recurso fue muy baja respecto a lo esperado. La refuncionalización de este recurso permitiría hacer el acompañamiento que está pidiendo Nación. No es que estamos pidiendo un recurso adicional nuevo. Al menos para este año con eso se puede asistir a las ciudades del interior de la provincia que tenemos sistema de transporte urbano y que no son tantas. Están Rosario, Santa Fe, Reconquista, Puerto San Martín, Venado Tuerto y Rafaela.