---
category: La Ciudad
date: 2022-01-04T06:00:38-03:00
thumbnail: https://assets.3dnoticias.com.ar/cabañascosta.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Las cabañas de la costa santafesina registran un 90% de ocupación a inicio
  de temporada
title: Las cabañas de la costa santafesina registran un 90% de ocupación a inicio
  de temporada
entradilla: 'A pesar de la "muy buena" ocupación en la zona costera, referentes del
  sector advirtieron que solo tres de cada veinte interesados terminan reservando
  su cabaña en Santa Fe.

'

---
A inicios de la temporada 2022, en la costa santafesina las reservas por alojamiento en cabañas alcanzaron un promedio del 90% de ocupación a pocos días transcurridos de enero.

Si bien varía entre el 80 y el 100% dependiendo la zona, cabañeros de Santa Fe aseguraron que es "muy buena" la ocupación y la demanda, aunque aumentó la cantidad de interesados que optan por no alquilar en relación a los precios actuales.

UNO Santa Fe dialogó al respecto con Jorge Unamuno, presidente de la Cámara de Cabañeros y Servicios Turísticos de Santa Fe (Cabasetur), quien aseguró que "en líneas generales la ocupación es muy buena", respecto a la cantidad de turistas alojados en las cabañas de la costa.

Según los precios actuales del mercado, una cabaña estándar para cuatro personas por fin de semana ronda los $24.000, aunque las opciones disponibles permiten encontrar cabañas con precios inferiores o superiores a este costo.

Insisten en la poca efectividad de reserva en las cabañas de la costa de Santa Fe.

Insisten en la poca efectividad de reserva en las cabañas de la costa de Santa Fe.

"La provincia tiene un corredor turístico muy grande, de Santa Fe para el sur hay bastante más ocupación. Hay ejemplos de cabañas que tienen todo enero y febrero reservado. Los que dependen de la pesca mas al norte de la provincia en la zona de Reconquista quizás tengan un poco menos de ocupación", continuó.

**Poca efectividad de reserva**

Sin embargo, la otra cara de la moneda radica en la poca cantidad de reservas que existen en relación a la gran demanda existente. Esto se grafica en que según cabañeros, actualmente de 20 interesados solo tres concretan su reserva en las cabañas de la costa.

En relación a esto, Unamuno sostuvo que "se siente que el poder adquisitivo está golpeado y a los turistas que habitualmente recibimos les está costando. Cada veinte llamados tenemos un 15% de efectividad, con tres ocupaciones. Los presupuestos que pasamos a veces resultan onerosos y a nosotros nos parecen acotados porque la rentabilidad es muy poca".

"En cuanto a los presupuestos el año pasado había más efectividad. A diferencia de este año que estamos rondando el 15 o 20% de efectividad el año pasado teníamos un 60%", afirmó en relación a esto el presidente de Cabasetur. Aunque agregó que "así y todo la ocupación y la demanda turística viene siendo muy buena, tanto que en muchos casos logramos ocupar entre el 80 y el 100% del lugar".

"El turista debe consultar siempre, tiene que llamar, averiguar e informarse antes de tomar una decisión. Los protocolos somos cada uno de nosotros, tanto el encargado, el dueño, el recepcionista y la gente que viene", concluyó el referente del sector cabañero.