---
category: Agenda Ciudadana
date: 2021-08-09T06:15:24-03:00
thumbnail: https://assets.3dnoticias.com.ar/energia.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: La bajante de los ríos seguirá hasta octubre y continuará afectando a represas
  hidroeléctricas
title: La bajante de los ríos seguirá hasta octubre y continuará afectando a represas
  hidroeléctricas
entradilla: La crisis hidrológica más importante de los últimos 77 años que afecta
  a la Argentina y buena parte de la región, podría profundizarse en el próximo trimestre.

---
Fuentes de la Subsecretaría de Energía Eléctrica señalaron que "si bien hay una clara bajante hidráulica a niveles históricos en las distintas cuencas, no hay un estrés para el sistema eléctrico en general y permite cubrir cómodamente la demanda a partir de otras fuentes de generación".

De acuerdo a las previsiones meteorológicas en el trimestre agosto-octubre no hay indicios de que se puedan revertir los bajos niveles de lluvia en la cuenca de los ríos Paraná y Uruguay, en el noreste argentino, algo similar a lo que es espera en el sur del país, en particular en la cuenca del Comahue.

La baja hidraulicidad de los ríos fue uno de los problemas que debió enfrentar este invierno la Secretaría de Energía, sumado a la reducción de los envíos de Bolivia de gas natural ante el reciente proceso de recuperación de la producción gasífera en Vaca Muerta -demorado un mes por las protestas en Neuquén- que no alcanzó para cubrir los picos de demanda.

Ese panorama llevó al Gobierno a definir la llegada de un segundo barco regasificador al puerto de Bahía Blanca, y a incrementar la importación de combustibles líquidos para garantizar la generación eléctrica.

**Agua potable**

La histórica situación hidrológica complica el abastecimiento de agua potable para la población, además de reducir la navegabilidad de los ríos, las operaciones portuarias y a distintas actividades productivas.

En el caso de la energía eléctrica esto esta teniendo particular afectación en lo que va del año en las mayores represas hidroeléctricas, que vieron reducir al 50% la capacidad de generación para esta época del año, como sucede en la actualidad en Yacyretá, sobre el río Paraná; en Salto Grande, sobre el río Uruguay; y en Chocón, en la Patagonia, con el río Limay.

Fuentes de la Subsecretaría de Energía Eléctrica explicaron que "si bien hay una clara bajante hidráulica a niveles históricos en las distintas cuencas, no hay un estrés para el sistema eléctrico en general y permite cubrir cómodamente la demanda a partir de otras fuentes de generación".

En ese sentido, dijeron que la potencia instalada al 31 de marzo de 2021 fue de 42.300 Mw, de los cuales la generación hidráulica aportó unos 11.300 Mw; la nuclear 1.755 Mw; la térmica entre ciclos combinados y turbo vapor 25.000 Mw; tecnología eólica unos 3.000 Mw; solar 760Mw y el remanente restante a través de equipos de combustión interna.

"Así cuando baja la hidraulicidad por el escaso aporte de lluvias afecta la hidroeléctrica, sube la generación térmica que puede quemar gas natural o combustibles líquidos como gasoil o fueloil, y también ganan participación las fuentes eólica y solar, lo que permite abastecer satisfactoriamente la demanda", explicaron las mismas fuentes.

Más allá de la consolidación del sistema eléctrico por la diversificación de su parque generador, en el cual la hidroeléctrica redujo su participación desde mediados de la década del 90 desde 48% del total al 22% con que cerró 2020, la bajante de los ríos se viene haciendo sentir en los últimos meses y todo indica que al menos continuará durante el próximo trimestre.

**Caudales mínimos**

En agosto y septiembre la cuenca del Paraná y el Uruguay entran en los meses de estiaje (caudal mínimo), ya que los de crecida son en febrero y marzo por las lluvias en las cuencas altas que nacen en Brasil, cuyo sistema de presas evita que el problema sea aún más grave aguas abajo.

La represa hidroeléctrica de Yacyretá está operando con 12 de sus 20 turbinas por la bajante del Paraná, lo que significa una reducción en la generación de energía de un 50%, ya que está generando en la actualidad por debajo de los 1.100 Mw, apenas un tercio de su capacidad técnica instalada de 3.200 Mw

En la actualidad, el caudal del Paraná tiene registros mínimos diarios de hasta 5.500 m3/s por la persistente escasez de precipitaciones en a zona centro de Brasil, cuando el caudal medio histórico para junio y julio son de 14.200 m3/s.

Una situación similar se está registrando en la repesa de Salto Grande, en donde debió reducir casi un 50% su capacidad de generación sobre un total de potencia instalada de 1.890 Mw.

La escasez de agua se convirtió también en un problema para la operación de las centrales termoeléctricas y las centrales nucleares de Atucha I (360 Mw) y Atucha II (745 Mw), que se encuentran a la vera del río Paraná y que utilizan ese recurso para la refrigeración de sus procesos de generación.

En el caso de las centrales nucleares, la empresa operadora Nucleoeléctrica Argentina comenzó en julio a tomar medidas físicas para paliar el tema, aumentando la profundidad del canal de acceso, con el alerta que de profundizarse el problema se debería analizar una disminución de potencia por la bajante.

Algo similar ocurre en las centrales térmicas como las de Vuelta de Obligado (816 Mw), San Martín (854 Mw) y San Nicolás (675 Mw) todas sobre la vera del Paraná inferior, que enfrentan dificultades en las tomas de agua ubicadas en distintos puntos del río.

Adicionalmente, se sigue en detalle como la bajante puede llegar afectar el aprovisionamiento de Gas Natural Licuado (GNL) mediante buques tanques, al barco regasificador que la empresa Excelerate Energy tiene amarrado en el puerto de Escobar y que aportó en los picos de demanda por encima de los 20 MMm3 dia de gas al sistema.

**COMAHUE**

La falta de agua también se hace sentir en la Cuenca del Comahue, conformada por las sub-cuencas del río Limay, Neuquén y Negro, donde se registra una bajante importante en este caso por la falta de precipitaciones níveas en alta montaña, un problema de seca considerada histórica para la región, que ya lleva 13 años.

Allí, las represas afectadas son las de El Chocón-Arroyito, Alicurá, Piedra del Águila, Banderita y Pichi Picun Leufú, que en conjunto aportan al sistema eléctrico unos 4.500 Mw de generación que se vieron sustancialmente reducidos para cuidar el recurso, y que motivó la reciente declaración de emergencia hídrica por parte de la Autoridad Interjurisdiccional de Cuencas.