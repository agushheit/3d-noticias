---
category: Agenda Ciudadana
date: 2021-08-27T06:15:52-03:00
thumbnail: https://assets.3dnoticias.com.ar/BALBRAN.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Alberto Fernández donará la mitad de su sueldo al Instituto Malbrán
title: Alberto Fernández donará la mitad de su sueldo al Instituto Malbrán
entradilla: El presidente se apersonó ante la Justicia por la causa que investiga
  el festejo del cumpleaños de la primera dama Fabiola Yáñez en la Quinta de Olivos,
  el 14 de julio de 2020.

---
El presidente Alberto Fernández se presentó este jueves en la causa por la celebración en la quinta de Olivos del cumpleaños de la primera dama, Fabiola Yañez, el 14 de julio del 2020, y propuso donar la mitad de su sueldo durante cuatro meses al Instituto Malbrán para reparar eventuales perjuicios.

La presentación fue efectuada ante el juzgado federal 7, a cargo de Sebastián Casanello, quien corrió vista al fiscal Ramiro González.

Tras recibir las presentaciones de todos los invitados que concurrieron a la reunión, González los imputó formalmente por la posible comisión de un delito vinculado a la supuesta violación de las medidas dispuestas para evitar la propagación del coronavirus, dijeron las fuentes judiciales.

El mandatario se presentó "por derecho propio y sin abogado patrocinante", informaron voceros de la Casa Rosada y confirmaron fuentes judiciales con acceso al expediente. Además, reclamó que la denuncia sea "desestimada" porque la conducta investigada no generó ningún daño ni contagio.

"Vengo a interponer excepción de falta de acción por inexistencia manifiesta de tipicidad", sostuvo el Presidente en la presentación en la que, además, planteo la posibilidad de avanzar hacia "la reparación integral del perjuicio en función de los hechos atribuidos".

"No escapa a mi entender que existe una norma penal, como el art. 205, que tipifica la conducta de quien violare medidas adoptadas por las autoridades competentes para propagar una pandemia, y que existe una norma que complementa este tipo penal en blanco, el DNU 260/20, que yo mismo he firmado. Ahora bien, tampoco escapa a mi entender que estamos frente a un delito de peligro abstracto", sostuvo el mandatario, profesor en la Facultad de Derecho de la Universidad de Buenos Aires.

"Ante la inexistencia de un resultado lesivo es que pongo a consideración del S.S. la insignificancia penal (no social o moral) del comportamiento denunciado que no ha lesionado el bien jurídico tutelado por su atipicidad o falta de antijuricidad material, y por ello solicito que la presente denuncia sea desestimada", continuó.

En otra parte de la presentación de 36 páginas a la que accedió Télam, el Presidente recordó que asumió "la total responsabilidad de lo ocurrido en la residencia de Olivos, ante cada uno de los ciudadanos de este país".

Y aclaró que "sin perjuicio de que los hechos aquí investigados han tomado dimensión pública, los mismos tuvieron lugar dentro de la órbita de la intimidad familiar".

El Presidente recordó que asumió "la total responsabilidad de lo ocurrido en la residencia de Olivos, ante cada uno de los ciudadanos de este país"

"La reunión ocurrida oportunamente, si bien revistió carácter privado, fue realizada en la Quinta de Olivos que es la residencia obligatoria del Presidente de la Nación y su familia", explicó el Presidente para luego detallar que allí desarrolla "las actividades propias del cargo" lo que incluye "en forma continua los asuntos familiares y de gestión, en el mismo ámbito, máxime en el tiempo de pandemia donde la acción de gobierno se trasladó íntegramente al predio".

"Es necesario aclarar que de ninguna manera se relajaron, evitaron u omitieron las medidas de cuidado de rigor. Y que en ningún caso se concretó el contagio propio, de los presentes, o de terceros. De aquí proviene la estimación referida a que, si se hubiera infringido alguna medida sanitaria, no se ha creado ningún peligro concreto de propagación del virus SARSCOV- 2", manifestó.

En la misma presentación, Fernández propuso donar el equivalente a la mitad de su salario como Presidente al Instituto Nacional de Enfermedades Infecciosas ANLIS "Carlos G. Malbrán", durante cuatro meses en forma consecutiva, y reclamó que si eventualmente es homologado tal acuerdo se dicte su sobreseimiento.

"Se ofrece hacer efectiva la donación precedentemente aludida, y tomando en cuenta los fundamentos que motivan este pedido, entendemos que una vez cumplido el acuerdo homologado, debe decretarse mi sobreseimiento en los términos detallados en esta presentación", sostuvo el Presidente.

En el expediente están imputados, además de Fernández y Yañez, los asistentes a la celebración, entre los que están algunos colaboradores de la primera dama: Carolina Marafioti, Severina Sofía Elizabeth Pacchi, Florencia Fernández Peruilh, Santiago Basavilbaso, Emanuel Esteban López, Fernando Daniel Consagra, Rocío Fernández Peruilh, Federico Abraham y Stefanía Domínguez.