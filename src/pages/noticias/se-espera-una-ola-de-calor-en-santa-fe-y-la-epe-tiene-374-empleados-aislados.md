---
category: La Ciudad
date: 2022-01-10T06:15:29-03:00
thumbnail: https://assets.3dnoticias.com.ar/EPE.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Se espera una ola de calor en Santa Fe y la EPE tiene 374 empleados aislados
title: Se espera una ola de calor en Santa Fe y la EPE tiene 374 empleados aislados
entradilla: 'El SMN pronostica mínimas de 31 grados para Santa Fe. La EPE planea solicitar
  apoyo de empleados tercerizados como refuerzo por el personal aislado

'

---
Luego de un pequeño respiro que dio el tiempo en la ciudad y la región con temperaturas agradables en los últimos días se espera que la cosa cambie la próxima semana. Se anuncia una ola de calor para todo el país y para Santa Fe se esperan temperaturas mínimas de 31 y máximas que rondarán los 40 grados centígrados en la capital santafesina, según datos del Servicio Meteorológico Nacional (SMN).

El golpe en el sector energético, que ya sufrió varios reveses en lo que marcha del verano con reiterados cortes de luz en distintos puntos de la ciudad, puede estar agravado por el personal aislado por casos de Covid o contactos estrechos que actualmente tiene la Empresa Provincial de la Energía (EPE) en su nómina. Incluso se emitió un comunicado pidiendo "un uso eficiente del servicio".

Hasta este viernes 7 de enero –el último dato disponible– la EPE registró unos 374 empleados aislados en toda la provincia, según confirmaron fuentes de la empresa. Este número crece día a día, puesto que el lunes la compañía había reportado un total de 151 agentes aislados y este viernes la cifra creció cerca de 150%, no ajeno a lo que sucede en todo el país con la tercera ola.

**La ola de calor**

Tanto el SMN como el Centro de Informaciones Meteorológicas (CIM) de la UNL coinciden en su pronóstico extendido anticipando a partir del lunes un ascenso de la temperatura que tocaría su pico durante el miércoles y jueves. **Para estas jornadas se espera una mínima de 31 grados y máximas de 40.** En tanto, no se esperan precipitaciones para los próximos días en Santa Fe.

Desde el organismo nacional se precisó que tanto Santa Fe como Capital Federal, provincia de Buenos Aires, Córdoba y el norte de la Patagonia se encuentran en alerta amarilla por la ola de calor, según el Sistema de Temperaturas Extrema del SMN.

“Se espera una semana con un ascenso muy marcado en las temperaturas”, aseguraron desde el SMN sobre el calor extremo que sacudirá a la zona central de Argentina. Y al respecto, agregaron: “Se volvería a repetir esta situación de calor extremo de diciembre”.

**Apoyo de más operarios**

En función de los inconvenientes que se puedan generar en la prestación del servicio, la EPE no descarta solicitar apoyo para sumar operarios de la industria eléctrica que planteen una solución a una coyuntura eventual, aunque aclaran que el personal contratado y tercerizado para responder está.

Cabe destacar que dentro de este número que comprende al personal aislado de parte de la EPE se encuentran aquellos que podrían reincorporarse por vencimiento de su período de aislamiento, aunque el número podría incluso subir por la dinámica de los contagios. No hay a la vista operaciones especiales previstas por la compañía energética provincial mas allá de las que surjan de las complicaciones que puedan existir.

Sobre los aislados que son operarios de calle, la EPE destacó que "un porcentaje importante de estos empleados, cumplen funciones operativas específicas de atención del servicio en las calles. Esta situación puede originar eventuales demoras en la solución de algunos reclamos".