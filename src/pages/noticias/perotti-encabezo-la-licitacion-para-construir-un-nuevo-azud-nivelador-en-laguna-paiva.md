---
category: Estado Real
date: 2021-07-04T06:00:38-03:00
thumbnail: https://assets.3dnoticias.com.ar/PEROTTIPAIVA.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: Perotti encabezó la licitación para construir un nuevo azud nivelador en
  Laguna Paiva
title: Perotti encabezó la licitación para construir un nuevo azud nivelador en Laguna
  Paiva
entradilla: 'El gobernador señaló que “estamos con obras en toda la provincia y cada
  una de ellas tiene sus recursos asignados; no existe más la duda y la preocupación
  de que éstas se hagan”. '

---
El gobernador de la provincia, Omar Perotti, encabezó este viernes la licitación de la obra de construcción de un nuevo azud nivelador en Laguna Paiva, para reemplazar al existente que se encuentra colapsado. Los trabajos tienen un presupuesto oficial de $ 99.973.963 y un plazo de ejecución de 16 meses.

Durante el acto realizado en la Sociedad Italiana de esa ciudad, Perotti señaló: “Nosotros creemos profundamente en la producción y el trabajo. Esto es lo que nos tiene que sacar adelante, generando oportunidades para nuestra gente. Así, cuando se nos planteó esta alternativa de recuperar la laguna, pasó a estar dentro de las prioridades”.

Al respecto, recordó que durante su gestión “la primera reunión que tuvimos fue con el ministro de Deportes y Turismo de la Nación (Matías Lammens), porque entendíamos que Santa Fe tiene que jugar muy fuerte en el tema del turismo”.

En ese sentido, Perotti mencionó que “nosotros tenemos actividades económicas fuertes: tenemos campo e industria. Hay otras provincias (una no muy lejana, como la vecina Entre Ríos), que tiene en el turismo una de sus principales actividades y generadora de ingresos. El río y la posibilidad que tenemos son las mismas en un montón de lugares. Entonces, por qué no aprovechar nosotros ese potencial enorme con el turismo en la provincia, el aprovechamiento del río, de las lagunas, de nuestro paisaje, de nuestra naturaleza y del cuidado que el santafesino hace de la misma”, destacó el gobernador.

Más adelante, Perotti afirmó que “tomamos la decisión de ponernos a trabajar para esto y no solamente para que los rancheros recuperen tranquilidad y puedan generar así su arraigo, sino para que toda la comunidad empiece a tener en esa laguna también una alternativa de puesta en marcha que genere recursos a los santafesinos”.

Como referencia, el mandatario santafesino indicó que “el año pasado, aún en pandemia, la provincia de Santa Fe estuvo como uno de los 10 destinos más elegidos. Eso no había ocurrido nunca y a eso tenemos que consolidarlo. Estamos convencidos de que para lograrlo hay que sumar alternativas y ésta seguramente va a ser una de ellas”.

Cabe señalar que el nuevo azud se ubicará aguas abajo del vertedero existente y permitirá regular los niveles del espejo de agua de manera de favorecer las actividades recreativas y controlar los niveles máximos, en crecidas, para que no afecten las viviendas costeras.

**MÁS OBRAS Y EL REGRESO DEL TREN**

Más adelante, en su alocución a los presentes durante la licitación de la obra, Perotti remarcó: “Estamos en obras en toda la provincia de Santa Fe. Cada localidad hoy tiene una obra, de mayor o de menor tamaño, pero tenemos rutas, viviendas, arreglos y construcciones de escuelas y de jardines”, subrayó. Y agregó que “cada obra como la que estamos licitando hoy tiene sus recursos asignados, no tiene que estar más en esta provincia la duda y la preocupación de algún vecino diciendo ojalá esta vez se haga”.

Asimismo, el titular de la Casa Gris se refirió al avance en el proyecto ferroviario del ramal Santa Fe – Laguna Paiva: “Con el tren queremos recuperar ese vínculo de Laguna Paiva con Santa Fe y con cada una de las localidades que tendrán su apeadero. Muy pronto estaremos haciendo el recorrido de prueba”.

**ESTADO QUE DA RESPUESTAS**

Por su parte, la ministra de Infraestructura, Silvina Frana, señaló que “a pesar de esta pandemia nos imponemos una agenda que siga dándoles respuestas a la gente y que se generen condiciones. Esta obra va a traer muy buenas consecuencias para Laguna Paiva, que tiene que ver con lo recreativo, la posibilidad del turismo, un círculo virtuoso que va a generar cosas positivas”, dijo.

Además, el intendente de Laguna Paiva, Elvio Cotterli, expresó que “es histórico que un gobernador esté aquí presente abriendo la licitación para recuperar la laguna, una obra tan esperada por todos los paivenses que nos va a permitir recuperar el turismo”. Además, recordó que “hace tres meses el gobernador se comprometió a darnos una solución y hoy está aquí para hacerlo”.

También, Cotterli destacó “el Plan Incluir, que nos permitió concretar cuadras de asfalto”; y valoró “los aportes provinciales para realizar la remodelación de nuestro parque infantil emblemático que tenemos en la ciudad, que tiene 40 años y es la primera vez que un gobierno provincial invierte para mejorarlo”.

**DETALLES DE LA OBRA**

El secretario de Recursos Hídricos, Roberto Gioria, explicó detalles de la obra que “va a remplazar al azud nivelado que ha colapsado y que impide regular el nivel de la laguna. La laguna pasó a tener el nivel del canal aliviador que está aguas abajo. Entonces, a ese azud nivelador colapsado vamos a reemplazarlo por uno nuevo, que va a estar unos metros aguas abajo del actual”.

Gioria indicó que “el colapso del azud se debió a la erosión por debajo y al pié del mismo. Ahora vamos a hincar unas tablestacas de seis metros de longitud para disminuir la posibilidad de la erosión y el muro de hormigón, eso unido conforma una pared que es prácticamente impermeable”.

Asimismo, el funcionario agregó que “aguas abajo vamos a hacer un cuenco, que es un piletón donde cae el agua que pasa por el vertedero, donde se aquietan las aguas, se les quita energía”. También, destacó que se colocará “un tramo de 12 metros de manta flexible con dados de hormigón, que también servirá para controlar la erosión”.

**LAS OFERTAS**

En la oportunidad, tres empresas presentaron sus ofertas: Pilatti, cotizó los trabajos en 85.436.687,87 pesos; Bauza Ingeniería S.R.L. -Ferrer S.A. (UTE), en 164.169.908,50 pesos; y COCYAR S.A., en 97.744.573,42 pesos.

**FONDOS PARA LAGUNA PAIVA Y RECREO**

El gobierno de la provincia entregó a Laguna Paiva un aporte por $250.000 con destino a la emergencia climática. En tanto, Recreo, recibió fondos de obras menores por $ 1.752.714 para el mejorado de calles de la ciudad.

**RECORRIDA**

El gobernador Perotti, junto al intendente Cotterli, recorrieron la obra de modernización del Parque Infantil el Zorzalito, que tiene una inversión de $4.000.000 del FFE (Fondo de Financiamiento Educativo).

Además, visitaron la obra de asfalto del Barrio Villa Talleres, en el marco del Plan Incluir, en el que se invierten $ 22.505.648. Se trata de la instancia previa al hormigonado, que comprende apertura de caja, preparación de base de suelo cemento y colocación de impresión.

Por último, recorrió el espacio de primera infancia Chikitines de Laguna Paiva “Chikilines”, que está en obra con fondos de la Nación y que se desarrollan a través del Programa de Apoyo al Plan Nacional de Primera Infancia y a la política de Universalización de la Educación Inicial.

Los trabajos a realizarse consistirán en mejorar el estado general de los edificios, seguridad, climatización, agua y saneamiento, construcción y ampliación de salones, incorporación de sanitarios para personas con discapacidad, niños y adultos, refacción de cocinas, depósitos de alimentos, nuevas instalaciones eléctricas, salidas de emergencias, impermeabilización de techos y otras reformas de diversa índole.

**PRESENTES**

También, participaron el secretario de Integración y Fortalecimiento Institucional, José Luis Freyre; el intendente de Recreo, Omar Colombo; el presidente comunal de Campo Andino, Néstor Moser, y autoridades de la Sociedad Italiana de Laguna Paiva, entre otras.