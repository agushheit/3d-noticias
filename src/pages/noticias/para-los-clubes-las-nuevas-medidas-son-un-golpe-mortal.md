---
category: Agenda Ciudadana
date: 2021-05-11T09:49:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/regatas.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Lt10
resumen: Para los clubes las nuevas medidas son "un golpe mortal"
title: Para los clubes las nuevas medidas son "un golpe mortal"
entradilla: El presidente del Club Regatas explicó que las nuevas disposiciones han
  dejado a los clubes agonizando.

---
Los clubes volvieron a cerrar sus puertas ante las nuevas restricciones. Alejandro Larriera, presidente del Club Regatas contó cómo siguen funcionando al móvil de LT10: "las puertas del club no están abiertas para la práctica deportiva. Lo único que permanece abierto es el jardín educativo 'Regatitas' que sigue funcionando por ser un jardín maternal. El personal sigue asistiendo a trabajar y a realizar las tareas de mantenimiento del club".

El presidente de Regatas fue contundente: "Estamos en total disconformidad con las medidas impuestas, creemos que no tienen razón de ser pero estamos acatando la medida. No hay ninguna de las 12 actividades del club habilitadas. Las piletas y los gimnasios están cerrados, así que si ven abiertas las puertas del club es porque por la misma puerta se ingresa al jardín".

En ese sentido, Larriera indicó que "esta medida nos parte a la mitad, estamos agonizando. Justamente en un momento en que según lo comisión, los socios están pagando la cuota, al estar todo cerrado, los socios no vienen a pagar la cuota. Estamos justos con el pago de sueldos. Esto es terrible y no sólo para regatas, sino que para muchas otras instituciones que tienen que ver con la salud de la gente. Estamos en total desacuerdo con esta medida, es totalmente carente de razonabilidad, pero igual la estamos acatando".

Respecto a los protocolos, el presidente del club explicó que  en el ingreso del club se toma temperatura, se pone alcohol y se mantiene la distancia. Además detalló que desde que empezó la pandemia el club tuvo un solo empleado contagiado, "que se contagió en otro lado y se lo mantuvo aislado. No hay una sola burbuja del club que haya mostrado algún tipo de problema en este sentido”.

Por último el presidente de Regatas lamentó que a pesar de que venían recuperando socios, ahora ese número empezará a caer de vuelta. “Es un golpe casi mortal para nosotros” conlcuyó.