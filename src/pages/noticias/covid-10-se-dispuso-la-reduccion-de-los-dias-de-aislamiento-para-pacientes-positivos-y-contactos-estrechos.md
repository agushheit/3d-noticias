---
category: Estado Real
date: 2021-12-30T06:15:08-03:00
thumbnail: https://assets.3dnoticias.com.ar/aislamientonew.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Covid-19: Se dispuso la reducción de los días de aislamiento para pacientes
  positivos y contactos estrechos'
title: 'Covid-19: Se dispuso la reducción de los días de aislamiento para pacientes
  positivos y contactos estrechos'
entradilla: Así fue acordado este miércoles en un nuevo encuentro del Consejo Federal
  de Salud, que encabezó la ministra Carla Vizzotti, y reunió a los titulares de las
  carteras sanitarias de todas las provincias.

---

La ministra de Salud provincial, Sonia Martorano, participó de este miércoles de una nueva reunión del Consejo Federal De Salud (Cofesa), que fue presidida por la titular de la cartera sanitaria nacional, Carla Vizzotti, y a la que asistieron ministros y ministras de las 24 jurisdicciones del país.

Tras el encuentro, Martorano destacó que “pusimos en contexto los dos años de pandemia y hoy un año de vacunación, donde evaluamos las 75 millones de vacunas colocadas a nivel país, y la situación epidemiológica hoy, en lo que llamamos a una nueva ola, con coexistencia de Delta y un avance importante de Ómicron, que varía de la contagiosidad y agresividad e internación”, continuó.

En ese sentido, la ministra de Salud santafesina manifestó que se puso en contexto y en consenso las nuevas medidas de aislamiento, tanto para los contactos estrechos como para los casos positivos de Covid-19 donde el punto de inflexión tiene que ver con el nivel de vacunación elevado que tenemos a la fecha: “Es por ello que se han bajado los días de aislamiento tanto del contacto estrecho asintomático como del caso confirmado en aquellos pacientes con vacunación completa”, detalló.

**REDUCCIÓN DE DÍAS DE AISLAMIENTOS**  
>> Aquella persona que sea contacto estrecho, asintomático, con esquema de vacunación completo, debe aislarse durante 5 días y 5 días de cuidados posteriores.

>> Si el contacto estrecho asintomático no se encuentra vacunado o tiene un esquema incompleto, el aislamiento queda en 10 días.

>> En cuanto a los casos positivos confirmados, si su esquema de vacunación es completo, le corresponde 7 días de aislamiento siempre y cuando las últimas 48 horas sean asintomáticas.

>> Si se trata de un caso confirmado con vacunación incompleta o sin vacunación el aislamiento será de 10 días.

>> Si el caso confirmado como positivo refiere a un personal de salud esencial, y tiene un esquema de vacunación completo se realizará un aislamiento por 7 días, siendo las últimas 48 horas asintomático o puede realizar aislamiento por 5 días con un test diagnóstico negativo.

Vale aclarar que se considera contacto estrecho a quien estuvo con una persona diagnosticada Covid positivo 48 horas antes del inicio de síntomas, por 15 minutos o más tiempo, sin barbijo, sin circulación o sin distanciamiento.

**AUTOTESTS**  
“Seguidamente se realizó una evaluación de la situación en las distintas provincias y se puso en evaluación la experiencia cordobesa con autotest. Seguimos trabajando en ese punto en cuanto a lo que tiene que ver con toma de muestra y trazabilidad”, cerró Martorano.

**REUNIÓN CON EL EQUIPO DE EXPERTOS**  
Por otro lado, ayer por la tarde el gobernador santafesino, Omar Perotti, mantuvo una nueva reunión con el equipo de expertos sanitarios donde se informó la realidad epidemiológica por la que están atravesando las diferentes provincias argentinas.

Al hacer referencia a posibles restricciones ante el aumento de casos positivos de Covid-19, Perotti señaló que “en intercambios con algunos gobernadores analizando medidas lo que aparece como posible de restricción son los eventos masivos, porque sería lo no indispensable, o lo que no alteraría el esquema de vacaciones, gastronomía ni movimientos de grupos más chicos”.

Finalmente, el gobernador de la provincia de Santa Fe destacó que “estamos un poco más seguros todos, al haber transitado, ganado experiencia, encaminado el tema vacunas, y refrescar lo que fue muy útil, con la comunicación de ustedes ayudando, el distanciamiento, el agua y el jabón, el barbijo, en eso todos y el que pueda seguir sumándose al esquema de comunicación va a ser necesario porque siguen siendo herramientas muy útiles”.

Por su parte, Martorano detalló la situación por la que está atravesando el territorio santafesino: "Sabíamos que iban a aparecer los casos, que de la mano de las habilitaciones, los aforos, y una gran necesidad de la gente de besarse y abrazarse, íbamos a tener un aumento de casos, lo estamos teniendo, claramente, pero la gran preocupación no son los que tenemos hoy, sino por el aumento”.

“Estamos con un gran número de testeos, a libre demanda en toda la provincia, pero la positividad que hace dos semanas era del 2 por ciento subió a 3, al 4, al 7, y está al 13, pero los números de anteayer dieron 22 por ciento, 26 ayer, y al privado dio 40. Claramente al final de la semana vamos a estar por encima. Sabemos que cuando se sube del 10 la situación está en otro contexto", agregó.

Asimismo, la titular de la cartera sanitaria hizo referencia a la ocupación de camas críticas destacando que “con este número de casos, lo que no estamos notando es la incidencia en el uso de camas de terapia, en toda la provincia, en el sistema público y privado, solamente tenemos 13 personas en terapia, y 15 en sala general. Los casos en terapia se dividen entre vacunados y no vacunados, tenemos hiperobesidad, comorbilidades y otros motivos, que eran más con Covid que por Covid".

"La ocupación de camas es del 55 por ciento, 77 en la ciudad capital, 40 en Rosario. Son números que no son altos, y mayoritariamente no Covid. El mapa va cambiando, de estar todo verde estamos teniendo áreas amarillas, aun no rojas, pero el mapa está cambiando", informó.

En cuanto a los síntomas a tener en cuenta ante la aparición de la nueva variante Ómicron, Martorano explicó que "vemos moco alto, rinitis, dolor de garganta, tos, febrícula, y no mucho más. Lo que estamos notando que los que están con tres dosis generalmente lo están transcurriendo muy bien, por eso, desde hoy, tercera dosis a los 5 meses, pero 60 años en adelante va con 4 meses. Vacunas hay".

"El 80 por ciento de la población tiene dos dosis, es un buen numero, pero no es nuestro techo porque seguimos vacunando”, concluyó la ministra de Salud.