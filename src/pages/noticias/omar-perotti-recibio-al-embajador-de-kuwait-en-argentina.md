---
category: Estado Real
date: 2021-08-20T06:15:06-03:00
thumbnail: https://assets.3dnoticias.com.ar/KUWAIT.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Omar Perotti recibió al embajador de Kuwait en Argentina
title: Omar Perotti recibió al embajador de Kuwait en Argentina
entradilla: 'El gobernador y el diplomático analizaron las obras financiadas y las
  oportunidades comerciales en los rubros alimentos, empresas de base tecnológica
  y energías renovables. '

---
El gobernador Omar Perotti recibió este jueves, en el salón Blanco de Casa de Gobierno, al embajador de Kuwait en Argentina, Abdullah Ali Alyahya, para analizar las obras financiadas en la provincia por dicho país y las oportunidades comerciales y de cooperación entre ambos estados.

Además, el embajador kuwaití mantuvo una reunión institucional con las autoridades de los Ministerios de Economía e Infraestructura, Servicios Públicos y Hábitat.

Durante el encuentro, Perotti expresó “la gratitud del pueblo de Santa Fe, por todos estos años de vínculos importantes con Argentina y, particularmente, con nuestra provincia; y el profundo deseo que esta relación tenga proyección de futuro, porque creemos que hay muchas posibilidades de crecer en relaciones comerciales de interés mutuo para ambos países”, anticipó el gobernador.

A continuación, el mandatario destacó “el compromiso que Kuwait ha asumido con Argentina y, en particular, con obras a financiar en nuestra provincia e independientemente de cualquier momento y situación económica, el vínculo de asistencia se mantuvo inalterable. Como ningún otro Estado, han tenido confianza en las posibilidades de recuperación de Argentina y en el cumplimiento de nuestros compromisos”, dijo.

Por último, Perotti manifestó “la vocación y el deseo de crecer en el volumen de exportación, particularmente en el rubro de alimentos”; pero también de trabajar en conjunto en empresas de base tecnológica y en energías renovables. Somos muy optimistas en el futuro de la relación, basados en la solidez de lo hecho hasta aquí”, concluyó.

A su turno, Abdullah Ali Alyahya dijo que “la relación entre Kuwait, Argentina y especialmente con Santa Fe como socio continuará. Estoy complacido por lo bien que está avanzando este proceso de apoyo del gobierno federal y haber llegado al acuerdo de protección de inversiones”. Además, sostuvo que en el futuro “estoy cien por ciento seguro que la relación ira de buena a mejor. Trabajaré para colaborar con el sector público y privado”.

**Obras de infraestructura**

En tanto, la ministra de Infraestructura, Servicios Públicos y Hábitat, Silvina Frana, indicó que “desde el inicio de la gestión recibimos financiamiento y pudimos comprobar que independientemente de quien esté al frente del gobierno, el país sigue respaldando y financiando proyectos de envergadura, como uno de los acueductos que está próximo a terminarse”.

Asimismo, Frana destacó que “el gobernador de la provincia tiene la firme decisión de potenciar aquellas zonas más postergadas y tenemos un banco de proyectos muy importante para lograrlo, sobre todo en el centro-norte provincial. Será un gusto trabajar con ustedes en ese sentido”, dijo.

El ministro de Economía, Walter Agosto, señaló que “la provincia de Santa Fe está avanzando en un contexto de recuperación y saneamiento de sus finanzas públicas” y dijo que pese a la pandemia “está mejorando en algunos sectores como el agroindustrial”.

El titular de la cartera económica provincial evaluó que “la provincia está muy bien preparada para gestionar financiamiento internacional para obras de infraestructura, aspectos productivos y tecnológicos como anteriormente ya se ha hecho con Kuwait”.

Por su parte, la secretaria de Cooperación Internacional e Integración Regional de Santa Fe, Julieta de San Félix, señaló que “planteamos una agenda para que pueda conocer el entramado productivo, el ecosistema innovador de la provincia de Santa Fe, pero también la calidad de su gente y su potencial turístico”.

De San Félix expresó respecto de las diferentes líneas de trabajo implementadas con el embajador kuwaití, que “nosotros estructuramos cuatro ejes de trabajo: Deportes, en el que hicimos visitas a clubes y al Museo del Deporte; el río y conocimiento, que es el eje transversal de la política productiva, la riqueza de sus exportaciones con un objetivo que no solo es incrementar la vinculación con Kuwait sino también sostener una tendencia; el entorno científico-tecnológico, visita a Bioceres para conocer la iniciativa del gobierno de Santa Fe junto al sector privado, de crear el Fondo Santa Fe 500, para 500 startups en la provincia; y por último la infraestructura e inversión, se recorrió el Paraná y el complejo portuario, ya que desde Santa Fe sale el 80% de la producción de la Argentina”, concluyó la funcionaria provincial.

**Presentes**

De la actividad participaron también la secretaria de Gestión Federal, Candelaria González del Pino; el subsecretario de Proyectos de Inversión, Claudio Vissio; el asesor del Estado de Kuwait, Lucas Reiris; y el consejero, Zeiad M M Alanbaie.

**Relaciones comerciales**

Cabe destacar que Santa Fe aporta el 9,85% del total de las exportaciones argentinas al mercado kuwaití y es la tercera provincia exportadora al mencionado país de Oriente Medio.

Por otro lado, la provincia de Santa Fe lleva a cabo diferentes obras públicas con financiamiento del Fondo Kuwaití para el Desarrollo. Ellas son:

>> Acueducto Reconquista: la etapa 1 se encuentra en ejecución financiada con recursos provinciales y el crédito obtenido ante el Fondo Kuwaití para el Desarrollo Económico Árabe por USD 51 millones. En tanto, la etapa 2 es financiada con fondos provinciales y el Fondo de la OPEP para el Desarrollo Internacional (OFID-OPEP) por USD 50 millones.

>> Acueducto Desvío Arijón: la etapa 1 está en funcionamiento y cubre Santo Tomé, Sauce Viejo y Desvío Arijón. La etapa 2 consta de varios tramos: Desvío Arijón-Matilde/ Matilde - San Carlos Centro/ San Carlos Sur y San Carlos Norte/ San Carlos Norte-San Mariano/ San Mariano - Angélica / Angélica – Rafaela. El Financiamiento internacional proviene del Fondo de Abu Dhabi por USD 80 millones; y del cofinanciamiento del Fondo de la OPEP para el Desarrollo Internacional (OFID-OPEP) por USD 50 millones.

>> Proyecto Acueducto biprovincial entre Santa Fe y Córdoba.