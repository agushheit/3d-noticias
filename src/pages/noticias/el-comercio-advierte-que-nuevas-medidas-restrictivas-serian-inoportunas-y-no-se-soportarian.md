---
category: Agenda Ciudadana
date: 2021-04-07T07:58:11-03:00
thumbnail: https://assets.3dnoticias.com.ar/peatonal.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Diario Uno
resumen: El comercio advierte que nuevas medidas restrictivas serían "inoportunas
  y no se soportarían"
title: El comercio advierte que nuevas medidas restrictivas serían "inoportunas y
  no se soportarían"
entradilla: Ante el crecimiento de contagios, el Centro Comercial invita a profundizar
  los controles y cumplir con los protocolos

---
Mientras los contagios por coronavirus crecen a nivel nacional, provincial y local, las autoridades sanitarias evalúan nuevas medidas restrictivas para frenar la embestida de la segunda ola.

La entrevista que mantuvo el gobernador Omer Perotti junto a los intendentes y presidentes comunales (a los cuales le pidió ajustar los controles sobre el cumplimiento de protocolos) sumado a las nuevas medidas restrictivas que se podrían tomar en Buenos y Aires y Caba, comienzan a generar preocupación en algunos sectores de la actividad económica de Santa Fe.

Ante las versiones de medidas que podrían replicarse en esta capital, el Centro Comercial advirtió que la "actividad comercial no soportaría medidas restrictivas".

El sector se adelantó al resto de las actividades y marcó su posicionamiento a través de un comunicado. La nota, con fecha 6 de abril de 2021, sostiene que "en momentos de tanta incertidumbre sobre las decisiones gubernamentales a tomar en el corto plazo ante el crecimiento de contagios de coronavirus, ratificamos nuestra posición de manifestar que sería absolutamente inoportuna una medida que tienda a restringir la actividad".

Para la entidad, "la experiencia del pasado año, nos revelan que ambas, salud y economía, deben ser asumidas como prioritarias, es por ello que adoptar medidas restrictivas para el sector productivo no fue, ni es actualmente, la solución de fondo".

La misiva continúa: "Con la llegada de la denominada «segunda ola» debemos asumir nuestro compromiso como sector y mantener vigentes todos los cuidados con la finalidad de preservar la salud de nuestros clientes además de los empleados y titulares de los comercios de nuestra ciudad".

Recordó también que "durante el año 2020 luego de aprobar la apertura de los locales, el comercio santafesino adaptó su atención al público dando cumplimiento de los protocolos establecidos y los resultados fueron excelentes si tenemos en cuenta que la tasa de contagio dentro de nuestros locales fue nula o muy baja. Esa muestra de responsabilidad sectorial, fue nuestro más sólido argumento al momento de reclamar ante las autoridades, la apertura de la actividad".

En esa línea insta "a la totalidad de los comercios a sostener y de ser posible, mejorar los cuidados que nos permitan preservar la actividad como parte fundamental de la cadena productiva de la cual somos parte".

![](https://assets.3dnoticias.com.ar/camara-de-comercio.png)