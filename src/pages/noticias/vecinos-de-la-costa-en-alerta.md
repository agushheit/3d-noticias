---
category: La Ciudad
date: 2021-02-17T09:23:48-03:00
thumbnail: https://assets.3dnoticias.com.ar/obras2.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: Vecinos de la costa en alerta
title: Vecinos de la costa en alerta
entradilla: Organizaciones y vecinos de la costa se movilizan contra proyectos inmobiliarios
  y turísticos en las orillas del Riacho santa Fe

---
Organizaciones y vecinos de la costa emitieron un comunicado en contra de las construcciones privadas a la vera del Riacho Santa Fe y anuncian una movilización y corte para el próximo lunes 22 de febrero a las 17,30 horas en la Fuente de la Cordialidad.

**El comunicado**

"Organizaciones y vecinos de la costa nos movilizamos contra el atropello de aquellos sectores que pretenden imponer sus intereses a cambio del despojo y destrucción de nuestros territorios.

Nos organizamos contra la impunidad con la que pretenden ejecutar sus proyectos inmobiliarios y turísticos a costa de negar a la comunidad el derecho de acceso a las orillas y a la naturaleza.

No es un hecho aislado, pasa en la Vuelta del Paraguayo y Alto Verde, en Colastiné, en Rincón y en Arroyo Leyes también.

Decimos Basta a la Privatización de las orillas.

Queremos ser protagonistas para poder decidir cómo vivir en el lugar que habitamos.

No a la privatización de las orillas de nuestros ríos y lagunas.

Exigimos a los gobiernos Provincial, Municipal y Nacional que la empresa que socavó las orillas del Riacho Santa Fe restaure la zona a como estaba.

No queremos que se lleven puesto el territorio isleño.

_Firmas_:

* Asociación Contrapunto Cultural - Coordinadora de Instituciones no gubernamentales de la Costa
* Centro de Protección a la Naturaleza CEPRONAT Santa Fe - Centro Cultural Alto Verde
* Proyecto Revuelta Vuelta del Paraguayo – FM POPULAR radio comunitaria Santa Rosa de Lima
* Héctor Alberto López, Arquitecto, Vicepresidente del Colegio de Arquitectos de Santa Fe
* Servicio de Educación Popular Santa Rosa de Lima - Asociación Vecinal Colastiné Sur
* Asociación Civil Manos Vivas Arroyo Leyes - Asociación Cultural de la Costa
* Asociación Civil Inundación Nunca Más – INNUMA Santa Fe - Agrupación La Marechal Santa Fe
* Asociación Civil Biblioteca Popular Las Orillas Colastiné Sur - Claudio Monzón Club Central Alto Verde
* Hugo Cabrera Alto Verde Noticias - Defensas de la Costa Colastiné Norte
* Merendero Esperanza, José Galvan, La Boca Alto Verde
* Coro de Niños Alto Verde - Grupo Vocal “Horacio Guaraní” Alto Verde
* Construir Futuro, Leonel Alvarez Escobar - Biblioteca Popular Juglares sin Fronteras Colastiné Norte
* Club Arroyito Seco Alto Verde - Tramas - Derecho en Movimiento
* Asamblea de vecinxs de Rincón - Asociación Vecinal Colastiné Norte
* Fundación CAUCE: Cultura Ambiental, Causa Ecologista Paraná
* Foro contra la Impunidad y por la Justicia de Santa Fe (Conformado por Madres de Plaza de Mayo de Santa Fe
* Familiares de Detenidos Desaparecidos por Razones Políticas - Santa Fe HIJOS - Santa Fe Liga Argentina por los Derechos Humanos – Santa Fe Asoc. Ex Presos Políticos de Santa Fe CTA ATE AMSAFE SADOP ADUL FESTRAM Militancia para la Victoria La Cámpora Movimiento Evita Partido Comunista)
* Multisectorial Humedales Santa Fe y La Capital - Red Ecosocialista -MST en FIT Unidad

_Adhieren_

Maria Graciela Ferreira, Ignacio Rintoul, Mariel Tocci, Juan Carlos Gomez, María Angélica Marmet, Mario Regazzoni, Dámaris Aneley Paola Benitez, Fabio Gonzalez, Luisa Cachiolate de Colastine Norte, miembros de Defensas de la Costa. Jorge Silva, Abogado, Claudia Kaki Ferrari, docente de Plástica, Artista de La Boca. Marcela Sabio, Artista, Colastiné Norte, Francisco Zanotti, Mariano Figueroa Representante de la Comunidad Hospital Iturraspe