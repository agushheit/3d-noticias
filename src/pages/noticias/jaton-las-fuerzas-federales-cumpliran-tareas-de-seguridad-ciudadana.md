---
category: La Ciudad
date: 2021-10-21T06:15:20-03:00
thumbnail: https://assets.3dnoticias.com.ar/REUNIONSEGURIDAD.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Jatón: “Las Fuerzas Federales cumplirán tareas de seguridad ciudadana”'
title: 'Jatón: “Las Fuerzas Federales cumplirán tareas de seguridad ciudadana”'
entradilla: El intendente de Santa Fe y el ministro de Seguridad definieron cómo será
  el funcionamiento de las diferentes Fuerzas Federales en la capital provincial.

---
El intendente Emilio Jatón y el ministro de Seguridad de la provincia, Jorge Lagna, concretaron este mediodía una reunión de trabajo con el objetivo de diagramar cómo será el desembarco de efectivos de las Fuerzas Federales a la capital provincial. Al término del encuentro, que se realizó en la sede del Ministerio de Seguridad, el intendente destacó: “Lo importante de este encuentro es que las vecinas y vecinos sepan que las fuerzas federales que van a venir a la ciudad de Santa Fe cumplirán una tarea de seguridad ciudadana”.

El número de integrantes que se sumarán a los trabajos de seguridad, los dió el ministro de Seguridad: “Se van a sumar, por reasignación de funciones, 102 efectivos de fuerzas federales más para la ciudad, que van a ser un número total trabajando en seguridad ciudadana de 490 efectivos de las tres fuerzas: Gendarmería Nacional, Prefectura y Policía Federal”.

En ese sentido, Jatón indicó que a partir de esa premisa “va a haber un trabajo conjunto con la Policía provincial para determinar dónde se apostarán”. En consonancia, el intendente remarcó: “Ya no los vamos a ver en la Costanera haciendo los controles vehiculares, no los vamos a ver en la Peatonal, sino que van a reasignarse en un trabajo conjunto con la Municipalidad y la Policía”.

Asimismo, el mandatario anticipó: “Vamos a sumar operativos multiagenciales, en los próximos días firmaremos un convenio con el MPA”.

Los anuncios más destacados que se realizaron este mediodía, tras el encuentro, incluyen el funcionamiento de un destacamento en French y Riobamba y la reapertura de la comisaría séptima. Asimismo, la Municipalidad está terminando dos espacios que cederá para abrir dos centros territoriales de denuncia.

**Nueva operatividad**

Por su parte, Lagna indicó: “Fue una reunión muy productiva, se había generado una gran expectativa a partir de la nueva operatividad que tendrá la policía de la provincia con las Fuerzas Federales en Santa Fe. Estamos trabajando con el municipio codo a codo”.

En lo referido a la manera de trabajar que se diagramó durante el encuentro con el intendente Jatón, el ministro indicó: “Se van a coordinar modalidades operativas muy diferentes, con trabajos no sólo en lo que se venía haciendo usualmente, sino en los barrios, en las entradas de los barrios y en las avenidas de acuerdo a la información que tenemos de la Municipalidad, el Ministerio y el Ministerio Público de la Acusación en cuanto a las zonas de mayor conflictividad”.

“Se ha generado un comando conjunto que va a funcionar todas las semanas. Vamos a hacer evaluaciones con las fuerzas federales. Estamos esperanzados en esta nueva modalidad de trabajo. El miércoles se van a presentar los nuevos patrulleros, se van a asignar 40 camionetas para la ciudad, que es una inyección importante, a lo que agregamos la reparación de alrededor de 20 patrulleros. Me parece que vamos a paliar bastante el clamor de la gente en cuanto al patrullaje, que es una realidad”, aseguró Lagna.

También indicó que “el miércoles mismo va a comenzar a funcionar un destacamento en French y Riobamba que es una zona muy conflictiva, principalmente, por el crecimiento del delito en la zona de Guadalupe y es un reclamo que vienen haciendo los vecinos. Ya hemos terminado los pliegos de licitación para la reapertura de la comisaría 7°, en barrio Yapeyú, una zona azotada por violencias. La Municipalidad está terminando dos espacios que nos va a ceder para abrir dos centros territoriales de denuncia. De los 900 agentes que en diciembre egresan del Isep se va a asignar un gran número a la ciudad de Santa Fe”.

**Autoridades presentes**

Participaron del encuentro autoridades nacionales, provinciales y municipales: Sebastián Faga, coordinador del ministerio de Seguridad de la Nación; Eduardo Macuglia, jefe de Gendarmería Nacional; Rogelio Pellegrino, de Prefectura Nacional Argentina; Victor Chanelco, de la Policía Federal Argentina; Facundo Bustos, coordinador de Gestión Social e Institucional de la Seguridad; Fernando Peverengo, secretario de Control y convivencia Ciudadana del municipio; y Ezequiel Costamagna, subsecretario de Gobierno.