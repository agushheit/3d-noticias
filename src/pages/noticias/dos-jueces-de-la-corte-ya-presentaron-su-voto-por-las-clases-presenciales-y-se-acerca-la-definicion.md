---
category: Agenda Ciudadana
date: 2021-05-04T08:14:39-03:00
thumbnail: https://assets.3dnoticias.com.ar/corte-suprema.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: Dos jueces de la Corte ya presentaron su voto por las clases presenciales
  y se acerca la definición
title: Dos jueces de la Corte ya presentaron su voto por las clases presenciales y
  se acerca la definición
entradilla: La expectativa está centrada en que en el Acuerdo de este martes haya
  una decisión del máximo tribunal sobre el conflicto con la Ciudad de Buenos Aires
  a raíz del decreto presidencial.

---
Los jueces de la Corte Suprema Juan Carlos Maqueda y Horacio Rosatti presentaron hoy ante la Secretaría de Juicios Originarios del Tribunal un voto común sobre el reclamo de la Ciudad de Buenos Aires por las clases presenciales en el distrito metropolitano e iniciaron así el camino final hacia un fallo que podría conocerse en el Acuerdo semanas previsto para mañana, martes.

Si bien el contenido del voto permanece en reserva, fuentes del tribunal adelantaron que está "muy fundamentado" y con profusa cita de fallos de la propia Corte sobre el estatus jurídico de la Capital Federal y las facultades que se desprenden tanto de la Constitución Nacional cuanto de la del propio estado local.

La especulación apunta a que la Ciudad de Buenos Aires obtendrá una reivindicación de la autonomía -que, por otra parte, ya fue reconocida por la propia Corte desde 2019- y en ese escenario se arrogaría la facultad de decidir la modalidad de las clases en su distrito.

Del resto de los jueces que componen el tribunal, la vicepresidenta, Elena Highton de Nolasco, ya se había inclinado por rechazar la "competencia originaria" porque entiende que la Ciudad no tiene la misma situación que las provincias y, en consecuencia, no corresponde que la Corte sea la única instancia para entender en este u otros reclamos contra la Nación.

El presidente de la Corte, Carlos Rosenkrantz, suele votar en sintonía con los planteos que realizan los gobernantes y los dirigentes opositores, en tanto que Ricardo Lorenzetti fluctúa en sus decisiones según el caso del que se tratare.

La mecánica de la decisión no requiere más tramitación que la presentación de los votos de cada juez, es decir que la instancia de discusión y debate ya está agotada y sólo resta computar el sentido de cada voto.

En ese sentido, las especulaciones indican que habrá un "voto concurrente", es decir que los jueces se pronunciarán en un sentido similar pero cada uno fundamentando su decisión con sus propios argumentos.

Usualmente la Corte no suele fallar dándole la totalidad de la razón a una de las partes y nada a la otra.

En este caso, los argumentos enfrentados entre Nación y Ciudad giran sobre dos tópicos esenciales: la autonomía porteña y la gestión de la pandemia.

El procurador general de la Nación interino, Eduardo Casal, ya se pronunció por la "inconstitucionalidad" del decreto de necesidad y urgencia (DNU) que venció el pasado viernes y que suspendió las clases presenciales en el Área Metropolitana de la Ciudad de Buenos Aires.

Ese dictamen reivindicó la autonomía porteña pero además consideró que no está probado, con rigor científico, que las clases presenciales sean un vector que potencie los contagios masivos de coronavirus. 

La decisión de la Corte se conocerá durante la vigencia de un nuevo DNU y si bien se trata de situaciones ligeramente distintas, la discusión de fondo sigue siendo si por esa vía legal el Estado Nacional puede disponer sobre la modalidad del ciclo lectivo en la Ciudad de Buenos Aires.