---
category: Agenda Ciudadana
date: 2021-06-02T08:53:47-03:00
thumbnail: https://assets.3dnoticias.com.ar/colectivos.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Inminente paralización definitiva del transporte del interior y pérdida de
  30.000 puestos de trabajo
title: Inminente paralización definitiva del transporte del interior y pérdida de
  30.000 puestos de trabajo
entradilla: La Federación Argentina de Transportadores por Automotor de Pasajeros
  F.A.T.A.P. emitió un comunicado en su sitio web

---
La Federación Argentina de Transportadores por Automotor de Pasajeros F.A.T.A.P. emitió un comunicado en su sitio web que reza lo siguiente:

A las Autoridades y Usuarios del servicio de transporte automotor:

**La paralización de los servicios de transporte de pasajeros en el interior del país a causa de un paro dispuesto por la representación sindical de los trabajadores revela la impotencia terminal del sector para alcanzar acuerdos** que permitan superar la gravísima situación que desde 2019 afecta a la actividad, profundizada a partir de marzo de 2020.

1. **Imposibilidad de atender una suba salarial y riesgos en los empleos**

En efecto, **a la fecha resulta materialmente imposible pactar ningún tipo de ajuste salarial** – más allá de la legitimidad del reclamo- dado que el Transporte del Interior del País no está en condiciones siquiera de garantizar el cumplimiento de las escalas salariales vigentes y menos aún de afrontar los costos de operación del sistema, poniendo en riesgos los empleos en el sector.

Esta situación ha sido incansablemente advertida por FATAP en las innumerables audiencias y presentaciones que a todo nivel gubernamental viene produciendo desde hace más de un año, sin haber encontrado respuestas concretas y eficaces que permitan dotar a la actividad de un mínimo de previsibilidad y sustentabilidad.

2. **Abandono del servicio e incumplimiento de los poderes concedentes**

**Lamentablemente, los gobiernos, en sus diferentes jurisdicciones, han generado acciones manifiestamente insuficientes en algunos casos, han incumplido los acuerdos que los obligaban localmente a sostener el sistema y resolver sus déficits en otros**, a pesar de lo cual no han cesado en exigir a las prestadoras que cumplan con los servicios aun cuando ellos sean deficitarios y económicamente inviables.

**Esta combinación de inacción o indiferencia estatal, incrementos incesantes de los costos de operación y obligatoriedad en el cumplimiento de servicios declarados esenciales para exigirlos, pero secundarios para sostenerlos, no puede tener otro resultado que la virtual paralización de la actividad**, la que amenaza en muchos casos con tornarse definitiva en caso de que no se adopten medidas inmediatas, contundentes y eficaces para garantizar la prestación de los servicios en condiciones de sustentabilidad.

3. **Discriminación del Gobierno Nacional entre el transporte del interior del país y del Area Metropolitana**

Es preciso señalar que, mientras el transporte del Interior del País se acerca a su definitiva paralización, la misma actividad, regulada por idénticas normas, convenio colectivo de trabajo y estructura de costos, con la única diferencia de desarrollarse en el Área Metropolitana de Buenos Aires goza de excelente salud, es sostenida y garantizada su prestación mediante una ingente asignación de los fondos del **Tesoro Nacional** que demuestran su existencia y revelan una clara preferencia respecto de su aplicación. **Así, en la República Argentina existe un sistema de transporte que debe ser sostenido y asegurado y otro que ha sido abandonado a su suerte, junto con cientos de empresas argentinas y más de 30.000 trabajadores directos.**

4. **Desigualdad entre los usuarios argentinos**

El resultado de tamaña diferencia en la asignación de los recursos nacionales, se refleja claramente en el precio del pasaje. **Mientras el AMBA** cuenta con 18 mil unidades en servicio, una tarifa $ 18, y 47.000 trabajadores, percibe **$16.000.000.000 en subsidios nacionales mensuales, el Interior del País**, con 13 mil unidades, una tarifa promedio de $37 y 3o.000 trabajadores, percibe únicamente **$1.500.000.000 por mes**.

En otras palabras, el Interior del País percibirá durante el año 2021 **$20.000.000.000** de aportes nacionales mientras el AMBA percibirá $**175.000.000.000** para el mismo período.

**En este estado FATAP solicita que las autoridades nacionales, provinciales y municipales con competencia en la materia asuman su responsabilidad y expresen, de modo urgente y con acciones concretas, su voluntad política y de gestión de garantizar el funcionamiento del sistema al que los usuarios de las provincias tienen derecho, así como la supervivencia de las empresas de capital nacional y los trabajadores que conforman el Transporte del Interior del País.**