---
category: Estado Real
date: 2021-07-28T06:00:15-03:00
thumbnail: https://assets.3dnoticias.com.ar/POES.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Santa Fe destina 15 millones de pesos a proyectos orientados a la economía
  social y la inclusión
title: Santa Fe destina 15 millones de pesos a proyectos orientados a la economía
  social y la inclusión
entradilla: La Agencia Santafesina de Ciencia, Tecnología e Innovación lanzó una nueva
  convocatoria POES+I con el propósito de impulsar soluciones o mejoras a demandas
  productivas y sociales.

---
El Gobierno de la provincia de Santa Fe, a través de la Secretaría de Ciencia, Tecnología e Innovación del Ministerio de Producción, Ciencia y Tecnología, impulsa una nueva convocatoria para el financiamiento de proyectos dirigidos Orientados a la Economía Social y la Inclusión (POES+I).

Para esta propuesta, se destina un monto total de $15.000.000 provenientes del presupuesto del Gobierno de la provincia de Santa Fe.

A través de esta convocatoria se busca impulsar soluciones o mejoras a demandas productivas y sociales a través de la generación y aplicación de nuevos conocimientos y desarrollos tecnológicos, mediante la implementación de proyectos que cuenten con la participación de actores o instituciones del sistema científico-tecnológico de la provincia.

La directora Provincial de Trazabilidad Social y Transferencia, Tecnológica, Constanza Estepa, responsable de la convocatoria POES+I, señaló: “La economía como ciencia social no habla de números sueltos en el vacío sino de las personas, sus necesidades y relaciones sociales. El crecimiento económico con inclusión social es la mejor herramienta que podemos brindar al sector socioproductivo”.

En ese sentido, indicó que “la convocatoria que estamos lanzando es fundamental para crear mayor valor agregado y contribuir con el acceso al trabajo de las y los santafesinos”.

**Aportes y condiciones**

Se pretende que los proyectos surjan de la definición de un problema territorial, buscando brindar herramientas para el desarrollo socioeconómico, que impulse la inclusión social, la sustentabilidad y la participación de una pluralidad de actores de la provincia de Santa Fe. Asimismo, impulsar el agregado de valor y la diversificación productiva en zonas rurales o localidades con menor densidad poblacional.

Se podrá solicitar financiamiento como ANR (Aporte No Reembolsable) por un monto máximo por proyecto de hasta $1.250.000. El cierre de la convocatoria para la presentación en formato electrónico es el 9 de septiembre.

Para más información sobre las bases y condiciones, ingresar en [http://www.santafe.gob.ar/index.php/web/content/view/full/239689/](http://www.santafe.gob.ar/index.php/web/content/view/full/239689/ "http://www.santafe.gob.ar/index.php/web/content/view/full/239689/")(subtema)/236062