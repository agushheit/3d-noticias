---
layout: Noticia con imagen
author: "Fuente: Infobae"
resumen: Prórroga para despidos
category: Estado Real
title: El Gobierno prorrogará la prohibición de despidos por otros 60 días
entradilla: Era uno de los pedidos de la CGT al gobierno de Fernández. Moroni ya
  habría redactado el DNU, que se hará oficial el sábado. Además, analizan
  seguir el mismo camino con la doble indemnización.
date: 2020-11-14T18:11:14.898Z
thumbnail: https://assets.3dnoticias.com.ar/moroni.jpg
---
El Gobierno prorrogará por otros 60 días la prohibición de despidos, que vencía a fin de mes. También prevé extender por un período similar la doble indemnización, vigente por ahora hasta el 17 de diciembre. Lo oficializarán este sábado.

Ambas medidas fueron tomadas al inicio de la pandemia para intentar atenuar el impacto de la crisis entre los trabajadores. Sin embargo, no lograron evitar que de marzo a esta parte aumente el desempleo.

Según consignó Infobae, el Decreto de Necesidad y Urgencia sobre prohibición de despidos ya fue redactado por Moroni y debe ser firmado por el resto de los ministros entre el lunes y el martes próximos. Se trata del cuarto DNU sobre el tema desde que el 1° de abril pasado.

La extensión se dictará en el marco de la emergencia pública en materia económica, financiera, fiscal, administrativa, previsional, tarifaria, energética, sanitaria y social establecida por la cuarentena obligatoria para contrarrestar el coronavirus.

La norma también extenderá por dos meses la prohibición de efectuar suspensiones por los mismos motivos, aunque quedarán nuevamente exceptuadas las suspensiones que sean pactadas individual o colectivamente u homologadas por la autoridad de aplicación en el marco de los términos del artículo 223 bis de la Ley de Contrato de Trabajo.

En su reunión de este martes, el consejo directivo de la CGT advirtió al Gobierno su malestar por el recorte de la ayuda económica, como el IFE y el ATP, y pidió “garantizar que el cambio de fórmula de actualización jubilatoria no perjudique a los beneficiarios del sistema previsional”.

Además, en ese momento la central obrera reclamó que se extendiera la prohibición de despidos y suspensiones, así como la doble indemnización, medidas que el ministro Moroni había asegurado que iban a continuar en la medida en que se prolongaran las dificultades en la economía y el empleo.