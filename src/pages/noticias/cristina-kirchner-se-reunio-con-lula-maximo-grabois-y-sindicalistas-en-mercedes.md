---
category: Agenda Ciudadana
date: 2021-12-12T06:00:41-03:00
thumbnail: https://assets.3dnoticias.com.ar/CRIATINAYLULA.jfif
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: Cristina Kirchner se reunió con Lula, Máximo, Grabois y sindicalistas en
  Mercedes
title: Cristina Kirchner se reunió con Lula, Máximo, Grabois y sindicalistas en Mercedes
entradilla: 'La visita de Lula, que comenzó el pasado jueves, finalizó con el encuentro
  con la vicepresidenta. También incluyó el acto en Plaza de Mayo y un encuentro con
  los secretarios generales de la CGT.

'

---
La vicepresidenta Cristina Kirchner se reunió con el ex mandatario de Brasil Luiz Inácio Lula Da Silva, referentes de La Cámpora y sindicalistas en la localidad bonaerense de Mercedes, en el marco de su visita al país.

La presidenta de la Cámara alta se entrevistó con Lula un día después de haber compartido junto al mandatario Alberto Fernández y al ex presidente de Uruguay José "Pepe" Mujica un acto en Plaza de Mayo para conmemorar los 38 años de democracia ininterrumpida en la Argentina.

"Con Lula en Mercedes. Se lleva a Brasil esta hermosa Evita que le regalé, de la compañera y gran artista argentina Marina Olmi", aseguró la vicepresidenta.

En su cuenta de la red social Twitter, la titular del Senado publicó también dos fotografías junto a Lula y el cuadro de Eva Perón, en el que la artistas Olmi la pintó sobre un fondo verde y amarillo.

Cristina Kichner y Lula se reunieron con sindicalistas e integrantes de La Cámpora en la localidad de Mercedes para hablar sobre el acto y el futuro de la región, supo NA de fuentes del Frente de Todos.

Del encuentro también participaron el jefe del bloque de diputados nacionales del oficialismo, Máximo Kirchner; el diputado y titular de La Bancaria, Sergio Palazzo; el referente social Juan Grabois; la intendenta de Quilmes, Mayra Mendoza; el ministro bonaerense Andrés "Cuervo" Larroque; la titular del PAMI, Luana Volnovich; el secretario general de la CGT Pablo Moyano; la titular de Anses, Fernanda Raverta; Mariano Recalde; Vanesa Siley; Walter Correa; María Luz Alonso; Juani Uztarroz; Lucía Campora; el titular del SUTERH, Víctor Santa María; Celso Amorin y Pablo Gentili, entre otros.

La visita de Lula, que comenzó el pasado jueves, incluyó el acto en Plaza de Mayo y un encuentro con los secretarios generales de la CGT en la sede de la calle Azopardo.

En la celebración por el 38° aniversario de la democracia y por el Día de los Derechos Humanos, Lula señaló: "Yo tuve la felicidad de gobernar Brasil en el período en que (Néstor) Kirchner gobernó Argentina y en que Cristina (Kirchner) gobernó Argentina".

Entre los aplausos, mencionó de la misma forma al venezolano Hugo Chávez, al boliviano Evo Morales, a los uruguayos Tabaré Vázquez y Mujica, al ecuatoriano Rafael Correa, al paraguayo Fernando Lugo, y la chilena Michelle Bachelet, entre otros ex presidentes de aquella época.

"Estos compañeros progresistas, socialistas, humanistas formaron parte del mayor momento de democracia de nuestra patria grande, nuestra querida América Latina", afirmó Lula y agregó: "Nuestra querida América tuvo su mejor período de 2000 a 2012, cuando nosotros gobernamos democráticamente todos los países de América del Sur".