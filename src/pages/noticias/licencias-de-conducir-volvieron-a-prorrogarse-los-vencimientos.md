---
category: La Ciudad
date: 2021-05-21T08:24:33-03:00
thumbnail: https://assets.3dnoticias.com.ar/carnet.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad de Santa Fe
resumen: 'Licencias de conducir: volvieron a prorrogarse los vencimientos'
title: 'Licencias de conducir: volvieron a prorrogarse los vencimientos'
entradilla: Se trata de aquellas que vencen entre el 15 de febrero de 2020 y el 31
  de julio de 2021. Para la extensión de la vigencia se dispuso un esquema, según
  la caducidad de cada carné.

---
La Municipalidad estableció una nueva prórroga de las licencias de conducir. La medida, en consonancia con la Resolución N° 0018/2021 de la Agencia Provincial de Seguridad Vial, establece un esquema para extender la vigencia de aquellos carné que vencen entre el 15 de febrero de 2020 y el 31 de julio de 2021.

Cabe recordar que el objetivo es evitar la aglomeración de personas en los centros de emisión de licencias, dándoles a los vecinos y vecinas, la posibilidad de tramitar el plástico paulatinamente. Con esa intención, la secretaría de Control y Convivencia Ciudadana del municipio ya dictó 12 normas de similares características.

En esta oportunidad, la Resolución Interna N° 36 fija la prórroga de la siguiente manera:

* Las licencias de conducir caducadas entre el 15 de febrero de 2020 y el 31 de julio de 2020, tendrán una prórroga de 18 meses, a partir de la fecha de vencimiento
* Las licencias de conducir caducadas entre el 1 de agosto de 2020 y el 31 de diciembre de 2020, tendrán una prórroga de 12 meses, a partir de la fecha de vencimiento
* Las licencias de conducir que caducan entre el 1 de enero de 2021 y el 31 de julio de 2021, tendrán una prórroga de 6 meses, a partir de la fecha de vencimiento