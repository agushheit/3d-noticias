---
category: Agenda Ciudadana
date: 2021-10-10T06:00:57-03:00
thumbnail: https://assets.3dnoticias.com.ar/NOLASCO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: La Corte Suprema sigue atenta la selección de un nuevo integrante y busca
  bajar el perfil
title: La Corte Suprema sigue atenta la selección de un nuevo integrante y busca bajar
  el perfil
entradilla: Con la dimisión de Highton de Nolasco, se produjo una suerte de clamor
  para que los eventuales reemplazantes en la Corte sean mujeres.

---
Tras la renuncia de Elena Highton de Nolasco y las críticas de Ricardo Lorenzetti al funcionamiento interno, la Corte Suprema busca salir de la consideración pública mientras sigue atentamente el eventual proceso de selección de reemplazantes que deberá hacer, sin plazos, el Poder Ejecutivo.  
  
En ese sentido, la única certeza en la coalición de gobierno es que el nombre del candidato o candidata surgirá después de las elecciones del 14 de noviembre próximo y tras una ronda de consultas con la oposición en el Congreso para asegurarse el apoyo de los dos tercios de los senadores presentes al momento de otorgar el acuerdo.  
  
Con la dimisión de Highton de Nolasco, se produjo una suerte de clamor para que los eventuales reemplazantes en la Corte sean mujeres y se comience a equilibrar así una integración histórica del máximo tribunal monopolizada por hombres.  
  
“No hay ninguna duda de que esta vacante en la Corte tiene que ser cubierta por una mujer" planteó el constitucionalista Gil Domínguez.

Precisamente, Highton fue la primera mujer en llegar a la Corte Suprema en 2004 y un año después se sumó la penalista Carmen Argibay, que falleció en 2014, y ahora se suma el fin de ciclo de la civilista, que deja al tribunal formado nuevamente solo por hombres.  
  
“No hay ninguna duda de que esta vacante en la Corte tiene que ser cubierta por una mujer, de hecho, el decreto por el cual Néstor Kirchner se autolimitó en su momento en la designación de jueces de la Corte aludía justamente a la diversidad de género”, recordó el constitucionalista Andrés Gil Domínguez un día después de conocerse la renuncia en la Corte.  
  
"Va a ser una mujer sí o sí", aseguró una funcionaria cercana al presidente Alberto Fernández, quien recibió la noticia de la renuncia de la propia Highton de Nolasco, a través de un llamado telefónico esa mañana a la Quinta de Olivos, antes de hacerla pública.  
  
No obstante, desde el Gobierno no adelantaron nombres de posibles postulantes al máximo tribunal que quedó integrado por sus restantes cuatro miembros: Horacio Rosatti (nuevo presidente), Ricardo Lorenzetti, Carlos Rosenkrantz y Juan Carlos Maqueda.  
  
Cuando el Poder Ejecutivo tenga un candidato a ocupar la vacante que dejó la única mujer integrante del cuerpo, deberá comenzar un proceso de selección para su reemplazo siguiendo lo que marca la Constitución Nacional en su artículo 99 inciso 4, que indica que la facultad de designación de un nuevo juez en el tribunal supremo "corresponde al Presidente de la Nación con acuerdo del Senado por dos tercios de sus miembros presentes".  
El artículo 3 del decreto 222 estableció en 2003 que “al momento de la consideración de cada propuesta se tenga presente, en la medida de lo posible, la composición general de la Corte Suprema de Justicia de la Nación para posibilitar que la inclusión de nuevos miembros permita reflejar las diversidades de género, especialidad y procedencia regional en el marco del ideal de representación de un país federal”.  
  
Además, aquel decreto -firmado también como jefe de gabinete por el actual presidente, Alberto Fernández- fijó la publicación de los antecedentes de los candidatos, su obligación de presentar declaración jurada de bienes y concedió la posibilidad de oponerse para particulares y asociaciones.  
  
La única certeza en la coalición de gobierno es que el nombre del candidato o candidata surgirá después de las elecciones del 14 de noviembre próximo y tras una ronda de consultas con la oposición en el Congreso.

Bajo las nuevas condiciones fijadas por el decreto fueron designados ministros de la Corte Eugenio Zaffaroni, Elena Highton de Nolasco, Carmen Argibay y Ricardo Lorenzetti, no así los dos últimos presidentes del alto tribunal, Rosenkrantz y Rosatti, que fueron designados por decreto por Mauricio Macri, pretendiendo incluso sortear el acuerdo del Senado.  
El exjuez federal, Carlos Rosanski, fue incluso más lejos en su pronunciamiento feminista y opinó que “la persona que ocupe su lugar (de Highton) debería pertenecer a lo que significa la marea verde con apertura mental a los nuevos tiempos, que pueda discutir diferentes identidades sexuales".  
  
Si el Gobierno termina finalmente por inclinarse por una mujer para integrar la Corte, tiene un amplísimo abanico de juristas para escoger: las camaristas de Casación (máximo tribunal penal del país), Ana María Figueroa y Ángela Ledesma; la civilista mendocina Aída Kemelmajer; o Graciana Peñafort, redactora de la Ley de Servicios de Comunicación Audiovisual, peronista declarada y actual directora de Asuntos Jurídicos del Senado, solo por mencionar algunas.  
  
De todos modos, más allá de las preferencias o candidatas en el oficialismo, el Gobierno está obligado a buscar un nombre de consenso para lograr el acuerdo en el Senado ya que cuenta con 43 senadores, entre propios y aliados, y necesitara algunos más para llegar a los dos tercios de los presentes en la sesión especial para ungir a la nueva ministra (o ministro) de la Corte.  
  
En la oposición no quisieron llevar a su agenda el tema del quinto miembro de la Corte porque entienden que la iniciativa debe ser del oficialismo y que se abocarán a la discusión cuando haya nombres concretos que anticipen el perfil que pretende la coalición gobernante.  
  
Como siempre en estos procesos, la consigna es que sea alguien “intachable” y con los suficientes pergaminos jurídicos y académicos como para sumarse al máximo tribunal de justicia del país.