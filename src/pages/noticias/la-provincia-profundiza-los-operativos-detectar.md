---
category: Estado Real
date: 2021-05-27T08:38:02-03:00
thumbnail: https://assets.3dnoticias.com.ar/detectar.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: La provincia profundiza los operativos detectAr
title: La provincia profundiza los operativos detectAr
entradilla: Será en articulación con el Ministerio de Salud de la Nación para realizar
  una búsqueda activa de casos de Coronavirus.

---
El Ministerio de Salud de la provincia retomó hoy y continuará durante una semana, los operativos DetectAr Federal en el departamento General López, en localidades determinadas como fundamentales para profundizar las acciones de testeo y aislamiento precoz, dada la circulación que el Coronavirus adquiere por la cantidad de casos y la rapidez con la que se duplican los contagios.

En ese departamento, el Operativo DetectAr Federal se reinició hoy en la ciudad de Venado Tuerto y continuará el 27 de mayo en Rufino; el 28 en Santa Isabel; el 29 en Murphy; el 30 en Firmat; el 31 en Hughes y el 1 de junio en Melincué.

Asimismo, este jueves habrá una jornada intensa de testeos en Elisa, departamento Las Colonias, en la región Santa Fe de Salud. Los testeos comenzarán a las 8, en el Centro Cultural Pedro Bovó.

**Criterios para realizar los testeos**

Sebastián Torres, coordinador de Dispositivos Territoriales de la provincia explicó que, como se viene realizando hasta el momento, la concurrencia no requiere turno previo, sino que se testearán personas, por orden de llegada, sintomáticas u oligosintomáticos que el equipo de salud considere necesario poder diagnosticarlos, aislarlos (a ellos y a sus contactos estrechos) y tratarlos en forma temprana evitando una evolución tórpida de los cuadros de Covid-19”.

Asimismo Rodolfo Rosselli, coordinador de la Región Santa Fe, explicó que en Elisa y otras localidad del centro norte “se realizarán también test de antígenos (rápidos, mediante hisopado nasofaríngeo) sin exigir que la persona tenga 48 horas o más desde que comenzó con síntomas. Sólo con un síntoma compatible haremos test rápidos. No importa cuándo comenzó el cuadro”.

“En el caso de que el test rápido sea positivo, se indicará aislamiento obligatorio de 14 días a la persona y su entorno. Y en caso de que sea negativo, pero la persona tenga factores de riesgo o sea trabajador esencial, se realizará prueba de PCR en laboratorio; y se le exigirá, mientras tanto, el mismo tipo de aislamiento preventivo”, agregó Rosselli.

Finalmente, el responsable de la región Santa Fe de Salud, adelantó que el mismo operativo se repetirá el viernes 27 de mayo bajo la misma modalidad, en las localidades de Nelson y Laguna Paiva.