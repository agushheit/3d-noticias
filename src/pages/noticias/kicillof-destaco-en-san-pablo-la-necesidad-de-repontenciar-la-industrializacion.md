---
category: Agenda Ciudadana
date: 2021-11-04T06:00:35-03:00
thumbnail: https://assets.3dnoticias.com.ar/kicillof.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Kicillof destacó en San Pablo la necesidad de "repontenciar la industrialización"
title: Kicillof destacó en San Pablo la necesidad de "repontenciar la industrialización"
entradilla: El gobernador bonaerense sostuvo, en una reunión que mantuvo en Brasil
  con 74 líderes de empresas. "La relación entre socios y vecinos es fundamental para
  el crecimiento productivo".

---
El gobernador bonaerense, Axel Kicillof, defendió este miércoles en Brasil el proceso de reindustriazación de la Argentina y la articulación productiva con su principal socio comercial para la reactivación económica tras la pandemia, lo que incluye la integración de las cadenas de valor y la generación de instrumentos financieros comunes.  
  
Lo hizo al reunirse con directivos de 74 empresas brasileñas y argentinas, entre ellas el Banco Itaú, las aerolíneas Gol y Azul y Aerolíneas Argentinas, así como automotrices y compañías de energía, en un encuentro que se realizó en la sede de la Federación de Industrias del Estado de Sao Paulo (Fiesp).  
  
El gobernador bonaerense asistió al encuentro junto con el embajador Daniel Scioli y el cónsul general, Luis María Kreckler.  
  
"Pretendemos que la recuperación sea rápida y mejor, con integración con el Mercosur y en particular con Brasil, que es socio comercial de Argentina y de la provincia de Buenos Aires", subrayó el mandatario.  
  
"Hablamos de cadena de valor, de integración productiva, de empezar a generar instrumentos financieros comunes y que esa integración pase a un estadio superior que sea la integración productiva, que las empresas integradas cuenten con políticas conjuntas para poder desarrollarse", dijo Kicillof ante una pregunta de Télam en una conferencia de prensa brindada luego de la reunión.  
  
El gobernador recordó que el 60% de las exportaciones de Argentina hacia Brasil tiene vinculaciones con la provincia de Buenos Aires.  
  
Kicillof dio especial énfasis a construir el tejido industrial bonaerense al afirmar que la industria había retrocedido 20% cuando asumió el cargo y que su objetivo es la "reindustrialización".  
  
"Esta reunión fue un éxito mayor de lo previsto, hubo mucho interés del empresariado paulista y brasileño. La Fiesp es una potencia productiva sudamericana", remarcó el gobernador.  
  
En ese sentido, detalló que ocho empresas agendaron reuniones en La Plata para presentar proyectos de inversión o de ampliación de inversiones, y citó el caso de una posible sociedad en la industria naval a través de la estatal Astilleros Rio Santiago y el dragado de la hidrovía del canal Magdalena.  
  
En el encuentro realizado en el piso 16 de la sede de la Fiesp, un edificio triangular que es un símbolo de la Avenida Paulista, en pleno corazón de San Pablo, se destacó la presencia del secretario de Desarrollo de la Industria, Comercio, Servicios e Innovación del Ministerio de Economía de Brasil, Gustavo Enne, del gobierno del presidente Jair Bolsonaro, en el marco de la diplomacia ejercida por el embajador Scioli.  
  
Kicillof señaló que ante la crisis mundial del comercio, problemas con embarques y "guerras comerciales", la relación entre socios y vecinos "es fundamental para el crecimiento productivo".  
"Hay varios proyectos en curso y todo esto es una oportunidad de salida de la pandemia vinculada a la articulación regional, cuando hay tanta turbulencia a nivel global. El comienzo de una escalera comienza por el primer escalón", enfatizó.  
  
Para Kicillof, "la provincia y el país tiene un plan propicio para el desarrollo industrial, con mucho espacio para la radicación de proyectos industriales".  
  
En el encuentro, Kicillof y Scioli destacaron que Brasil recuperó el primer lugar de socio de la Argentina durante la pandemia.

Para Rafael Cervone, vicepresidente de Fiesp, la llegada del gobernador de Buenos Aires -principal estado de Argentina así como San Pablo es de Brasil- marcó el reinició de los grandes eventos permitidos en la flexibilización de la pandemia en la sede empresarial.  
En octubre de 2021 las exportaciones argentinas a Brasil fueron de US$ 1.218,2 millones, y las brasileñas de US$ 1.125,8 millones, con saldo superavitario argentino de US$ 92,4 millones.   
  
Las exportaciones argentinas crecieron un 50,5% con respecto al mismo mes del 2020, mientras que las brasileñas se incrementaron un 38%.   
  
"Fue el segundo mes consecutivo de superávit para Argentina", destacó Scioli, revelando los datos recientes del Ministerio de Economía brasileño.  
  
En octubre la corriente de comercio bilateral alcanzó US$ 2.344 millones, representando un aumento del 44% interanual.  
  
En el período enero-octubre del 2021, las exportaciones argentinas fueron US$ 9.360 millones con una corriente de comercio de US$ 19.229 millones, representando un aumento del 46 % con respecto al mismo período del 2020.