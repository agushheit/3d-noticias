---
category: La Ciudad
date: 2021-12-07T06:00:37-03:00
thumbnail: https://assets.3dnoticias.com.ar/RELLENOMUGRE.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Con críticas al oficialismo, el Concejo aprobó la ampliación del relleno
  sanitario '
title: 'Con críticas al oficialismo, el Concejo aprobó la ampliación del relleno sanitario '
entradilla: 'Se aprobó la prórroga de la concesión para operar el relleno sanitario
  por otros siete años con la misma empresa que venía operando. Hubo críticas al municipio
  por "falta de previsibilidad"

'

---
Este lunes, el Concejo Municipal sesionó y aprobó la extensión por un plazo de siete años de la concesión del relleno sanitario, junto con su ampliación. El proyecto de ordenanza había sido enviado al cuerpo por la Municipalidad, ordenado por la secretaría de Ambiente. El oficialismo lo calificó como "un orgullo" para los santafesinos, mientras que los bloques opositores coincidieron en que se trata de una aprobación por necesidad.

La concesión seguirá siendo a cargo de la empresa Milicic S.A., cuestión que fue objeto de críticas al proyecto de ordenanza por no poder concretarse un llamado a licitación dados los apremiantes plazos para concretar qué se hará con los residuos en los próximos años. El actual contrato vence el próximo 3 de diciembre de 2022.

**"Un orgullo"**

La concejala oficialisa María Laura Mondino se mostró orgullosa de la prórroga acordada para la operabilidad del relleno sanitario, manifestando: "Estamos generando las condiciones para prever que la basura no se convierta en un problema grave en los próximos años. El relleno es el mas grande del noroeste del país, uno de los mas grandes de Argentina. La gestión de Emilio Jatón es consciente de ese desafío, pensando la ciudad a mediano y largo plazo para cuidar al ambiente y dar seguridad ambiental a las futuras generaciones".

**Críticas de toda la oposición**

Sin embargo, las críticas de parte de los bloques opositores del Concejo por la celeridad para debatir y acordar una prórroga teniendo en cuenta que el actual contrato finaliza en diciembre de 2022 no se hicieron esperar. El concejal Carlos Suárez de Juntos Por el Cambio indicó: "No se entiende que ha pasado en el ultimo año para que este mensaje no haya llegado con mayor antelación. Estamos prorrogando por siete años la concesión de un servicio publico y surgen muchas preguntas".

Quien fue más categórico fue su compañero de espacio Carlos Pereira quien señaló a esta prórroga como "una señal de que no hay una política cierta efectiva en la disposición de residuos solidos urbanos en la ciudad, tratándose a los ponchazos. No sabemos si habrá planta de compostaje".

"Los bloques opositores no queremos poner palos en la rueda ni mucho menos queremos dejar sin relleno sanitario a la ciudad cuando el principal problema es la falta de previsibilidad del municipio. Seguimos empantanados en la nada porque no sabemos que sistema de clasificación de residuos domiciliaria vamos a tener. Ojalá tengamos esta planta de compostaje para tener un sistema de clasificación de residuos mas acorde a siglo XXI", cerró Pereira.

Otro de los puntos que se citó con mayor crítica a la gestión municipal fue el nombrado por el edil Guillermo Jerez, del espacio Barrio 88. El concejal mencionó que "el 3 de diciembre de 2020 el ejecutivo hizo uso de la prórroga y nosotros nos enteramos tardíamente de esta posibilidad de prorrogar otros siete años, acotando los márgenes de llamar a licitación a otras empresas con expertise en el rubro", sobre la falta de posibilidad de un llamado a licitación.

"Santa Fe hoy cuenta con 35 normas sobre el tema y parece que no le encontramos la vuelta. Es un error decir que es un orgullo tener uno de los rellenos mas importantes, porque estamos discutiendo por la disposición final de residuos mientras tenemos que discutir como generamos basura. Tenemos que darle incentivos fiscales a los vecinos que colaboran con el tratamiento de residuos, es insustentable ambiental y fiscalmente este modelo", argumentó Jerez.

Los concejales del Partido Justicialista se manifestaron a tono a las críticas a la gestión del municipio encabezada por el intendente Emilio Jatón. La edila Jorgelina Mudallel mencionó: "Nos queda un sinsabor porque es un problema de larga data pero esperábamos que esta administración pudiera concretarlo". En consonancia a esto se manifestó su compañero de banda Federico Fulini, quien destacó que la prórroga se trata de "emparchar". Y agregó: "No entiendo como nos llevaron a esta instancia, sin planificación y sin previsión".

**La ordenanza**

El relleno sanitario será ampliado en ocho hectáreas, según se explicitó en el proyecto de ordenanza aprobado por el Concejo. Se construirá otro módulo al sur del actual, en el que estiman que podrá estar operativo por los próximos siete años, aunque en diálogo con UNO, el secretario municipal de Ambiente Edgardo Seguro había manifestado que su vida útil podría estirarse hasta nueve años dependiendo su estado.

Desde el municipio reconocieron que esta prórroga no configura la opción ideal para proceder a corto plazo con el almacenamiento y tratamiento de residuos. Sobre esto, Seguro sostuvo que "la pandemia y el arreglo de la deuda que quedó con las tres empresas de residuos nos llevaron a estar esta situación de tener que avanzar con un pedido de prórroga".

El otro proyecto importante dentro de la ordenanza aprobada por el Concejo radica en una futura planta de compostado, siendo el objetivo de la Municipalidad tratar 100 toneladas diarias de basura. Este número significaría un 20% de los residuos de la ciudad, con lo que se apunta a generar unas 50 toneladas de material estabilizado por día que tiene como destino la tapada del relleno.

Además, están abarcadas en la ordenanza obras de mejora en planta de secos, que luego de años en funcionamiento desde que se colocó el relleno sanitario tiene un grado de deterioro muy importante. "Está planificada la mejora y la restitución de su equipamiento, en estos diez años ha sufrido muchísimo por falta de mantenimiento. Lo tenía a cargo la asociación que lo opera", manifestó Seguro.