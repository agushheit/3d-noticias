---
category: Estado Real
date: 2021-03-16T05:48:01-03:00
thumbnail: https://assets.3dnoticias.com.ar/vacuna-policia.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: 'COVID – 19: La provincia comenzó la vacunación al personal de Seguridad'
title: 'COVID – 19: La provincia comenzó la vacunación al personal de Seguridad'
entradilla: 'Se trata de aquellos agentes entre 18 y 59 años con comorbilidades. '

---
El gobierno provincial, a través del Ministerio de Salud, comenzó este lunes por la tarde el proceso de inoculación contra el COVID - 19 a personas entre 18 y 59 años con comorbilidades que pertenecen a la fuerza de Seguridad.

Al respecto, el secretario de Salud, Jorge Prieto, afirmó que “hoy por la tarde comenzamos con otra estratificación de lo que tiene que ver con el Plan de Vacunación, que son las personas que están licenciadas con comorbilidades de la fuerza de Seguridad. Entonces se comenzó con la vacunación de acuerdo al listado que ha sido remitido de cada una de las regionales y se va a continuar así hasta culminar con el 100 por ciento. El objetivo es que toda persona que reciba la inmunización pueda retomar sus tareas de trabajo, siendo que la vacuna le va a dar una protección para volver a sus tareas habituales”.

Sobre un nuevo arribo de vacunas al país y respectivamente a la provincia, el secretario de Salud detalló que “mañana arriban 30 mil dosis más de la primera dosis de Sputnik. Se va a continuar con los trabajadores de la salud, que se han inscripto de forma independiente. Además, se va continuar con nuestra población objetivo: en este momento estamos con más 80, seguiremos con más 70 y más 60 como población prioritaria, vulnerable y sensible frente a la pandemia”.

Por último, Prieto se refirió al cronograma de vacunación más 80 y sostuvo que “hoy vacunamos 1.876 personas. Esta mañana fue un éxito porque es una población que tiene una movilidad totalmente diferente al resto y poder lograr un objetivo de 170 turnos cada media hora fue impactante. La idea es alcanzar el mayor número de personas vacunadas en el menor tiempo, siempre y cuando se disponga de la vacuna”.

En tanto, el coordinador del Ministerio de Seguridad, Facundo Bustos, destacó que “somos una de las primeras Policías del país en vacunar a sus agentes. En esta primera etapa vamos a vacunar a todos aquellos que tienen licencia y una enfermedad preexistente. Recordemos que las fuerzas de Seguridad están en la primera línea de batalla contra este virus, así que es muy importante para nosotros iniciar la vacunación”, concluyó.

Por su parte, la jefa de la Unidad Regional 1, Marcela Muñoz, detalló que a través “del departamento de personal se notificó a quienes tenían la voluntad de vacunarse con enfermedades preexistentes y, por lo tanto, estaban con licencia por Covid”; y precisó que en esta condición hay “480 agentes en la provincia, y en la Unidad Regional 1 son 250 los licenciados por Covid, y alrededor de 60, 70 efectivos quienes se inscribieron previamente en el Registro Provincial de Vacunación”.

Por último, Muñoz destacó que esta iniciativa es “importantísima, porque somos personal esencial, estamos en el frente, trabajando desde el comienzo de la pandemia”; y detalló que en la fuerza hay “alrededor de 22 mil efectivos y más del 60% tiene voluntad de vacunarse”.

**ROSARIO**

Idéntico operativo de vacunación a policías que dio inicio en la capital provincial se desarrolló en la ciudad de Rosario. En ese contexto, el coordinador del vacunatorio de la ex Rural, Sebastián Torres, manifestó: “Nos pone muy contento, hoy pudimos dar inicio a otro grupo poblacional que representa a esa población objetivo, cuando hablamos inicialmente de ese 1.200.000 de personas”. Y continuó: “Representa al grupo de trabajadores esenciales, los cuales están en permanente exposición, en este centro que estamos ahora, cerca de 200 trabajadores de la fuerza policial han sido vacunados”.

Por su parte, la Jefa de la Policía, Emilse Chimenti, sostuvo: “Lo estábamos esperando y es una alegría poder empezar con la vacunación de nuestros efectivos, empezamos por las personas de riesgos, que hoy no están prestando servicios por esa condición, la vacunación es voluntaria”.

**RESPETAR LOS TURNOS**

Por último, el secretario de Prácticas Socio Comunitarias, Ignacio Martínez Kerz, precisó que “como a las 3 de la tarde ya no quedaban personas mayores por vacunar, se aceleró la decisión de empezar a abordar el personal de seguridad”; y recomendó que “los próximos días se respeten los horarios, para que no pase lo de hoy, que personas que tenían turno a las 14, 15 y 16 horas se vacunaron a las 10:30, 11”.

En este sentido, reiteró que “para mejor organización y tranquilidad de las personas mayores que se vienen a vacunar, pedimos que se respeten los horarios de vacunación. No van a tener ningún problema”.