---
category: La Ciudad
date: 2021-12-05T06:00:07-03:00
thumbnail: https://assets.3dnoticias.com.ar/NOMELACONTEINER.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: La Terminal de Contenedores comenzó a operar en el Puerto de Santa Fe
title: La Terminal de Contenedores comenzó a operar en el Puerto de Santa Fe
entradilla: "Se efectuarán un mínimo de dos escalas mensuales entre los puertos de
  Santa Fe y La Plata, y de ahí al exterior. \n"

---
Esta semana, arribó a la Terminal Contenedores del Puerto Santa Fe la primera barcaza porta contenedores desde la terminal TecPlata, dando inicio de esta manera a la operatoria del nuevo circuito feeder Puerto Santa Fe – Puerto La Plata entre ambas estaciones fluviales.

 Al respecto, la ministra de Infraestructura, Servicios Públicos y Hábitat Silvina Frana aseguró que “la reactivación que muestra el Puerto de Santa Fe tiene que ver con una de las propuestas del gobernador Perotti desde el primer día de gestión. Hoy vemos un puerto trabajando a pleno, superando este año las 100 toneladas de granos exportadas y afianzado en este crecimiento constante, ahora con la reactivación de la terminal de contenedores que no operaba desde 2016".

 "Acá hay un enorme trabajo y compromiso no solo de nuestro ministerio, de la Secretaría de Empresas y Servicios Públicos que encabeza el ingeniero Carlos Maina y del equipo del Puerto que conduce Carlos Arese, sino también del ministerio de Producción Ciencia y Tecnología con quienes trabajamos codo a codo para afrontar esta recuperación productiva", señaló Frana.

 Por su parte, el administrador de Puerto de Santa Fe, Carlos Arese explicó que "el circuito está diseñado para efectuar un mínimo de dos escalas mensuales entre los dos puertos de Santa Fe y La Plata, y esta operatoria apunta a ser una nueva alternativa de conexión fluvial para las regiones del centro, noreste, noroeste y Montevideo".

 Arese indicó que desde TecPlata se conecta con el servicio de LogIn a Brasil, que también conecta cargas en Santos, con destino a Asia, mientras que la naviera Evergreen ofrece el servicio de conexión con Asia.

 Asimismo, el presidente del Ente Portuario recordó que el Puerto Santa Fe viene llevando adelante un proceso de reactivación productiva, primero recuperando en 2020 la actividad en la Terminal Agrograneles alcanzando cifras récord de embarques en el último año, después de mucho tiempo de inactividad. Ahora con la llegada de la primera barcaza el puerto recupera también la actividad en la Terminal Contenedores.

"Esta recuperación de la actividad productiva y portuaria, ha requerido grandes esfuerzos con muchísimo trabajo articulado y, a su vez, grandes inversiones -tanto en obras de infraestructura como en equipamiento- para acondicionar las terminales a los requerimientos logísticos, aduaneros, de Seguridad e Higiene y operativos", dijo.

 Y concluyó: "Este nuevo servicio regular de barcazas porta contenedores viene a cubrir una histórica demanda del sector productivo de toda la región y zona de influencia del Puerto Santa Fe".

 Sobre el arribo de la primera barcaza también se refirió el secretario de Comercio Exterior, Germán Burcher, quien indicó: “Esta embarcación que arriba para ser cargada con mercadería con destino a Asía y a Brasil, es un hecho significativo no sólo para el puerto, sino también para todo el entramado productivo del centro norte de la provincia de Santa Fe”.

 “Entre los objetivos que nos planteamos en la gestión, junto con el gobernador Omar Perotti y el ministro Daniel Costamagna, está incrementar las exportaciones de alto valor agregado y aumentar las exportaciones de nuestras pymes. Esto se logra también ganando competitividad a través de la logística. En este sentido, la posibilidad de sacar mercadería desde el puerto de Santa Fe es mejorar la competitividad a partir de mejores costos de exportación. Usar la infraestructura con la que cuenta la provincia sobre el río Paraná, siempre será superadora a la opción de camiones yendo al puerto de Buenos Aires”, concluyó el funcionario provincial.