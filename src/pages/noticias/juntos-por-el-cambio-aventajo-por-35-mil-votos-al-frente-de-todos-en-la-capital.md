---
category: La Ciudad
date: 2021-09-19T06:15:53-03:00
thumbnail: https://assets.3dnoticias.com.ar/ESCRUTINIOS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Juntos por el Cambio aventajó por 35 mil votos al Frente de Todos en La Capital
title: Juntos por el Cambio aventajó por 35 mil votos al Frente de Todos en La Capital
entradilla: Losada, Lewandowski y García se impusieron en las respectivas internas
  en la categoría senador. En la alianza ganadora se advierte un corte de boletas
  para Corral y para Barletta.

---
Los datos del escrutinio definitivo de los votos emitidos en la PASO del pasado domingo en el departamento La Capital corroboran los triunfos que se extendieron a toda la provincia en las tras alianzas más votadas. Carolina Losada - Dionisio Scarpin de Cambiemos con Ganas en Juntos por el Cambio; de Marcelo Lewandowski - María de los Ángeles Sacnun de la Celeste y Blanca en el Frente de Todos y de Clara García - Francisco Garibaldi de Adelante en el Frente Amplio Progresista.

Juntos por el Cambio aventajó por 35.150 votos al Frente de Todos en la elección nacional y tercero quedó el FAP cuyos dirigentes están al frente de las cuatro ciudades del departamento: Santa Fe, Santo Tomé, San José del Rincón y Recreo. La quinta, Sauce Viejo, elegirá intendente este año.

Las cuatro listas de Juntos totalizaron 98.861 votos contra 63.711 de Todos y 34.896 del FAP. La cuarta más votada fue Unite donde Betina Florito logró 19.794 votos. Después están los votos nulos (15.942) y en blanco (10.923) y más abajo los restantes precandidatos.

Dentro de Juntos por el Cambio, Cambiemos con Ganas sumó 34.992 en la categoría senadores (36.203 en diputados donde encabezó el ex intendente Mario Barletta). Segundo fue José Corral (Vamos Juntos) con 25.201 votos (la lista de diputados tuvo 23.204); tercero fue Federico Angelini - Amalia Granata con 21.489 y cuarto Evolución con Maximiliano Pullaro - Carolina Piedrabuena 17.179 (los diputados con 16.196 votos).

En el Frente de Todos, la Celeste y Blanca logró 38.923 contra 24.788 de Agustín Rossi y Alejandra Rodenas.

En el FAP, Adelante sumó 23.546 voluntades contra 11.350 de Rubén Giustiniani y María Eugenia Schmuck.