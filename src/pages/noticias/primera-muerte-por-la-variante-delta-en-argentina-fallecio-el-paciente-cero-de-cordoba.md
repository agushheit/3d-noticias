---
category: Agenda Ciudadana
date: 2021-08-23T06:15:06-03:00
thumbnail: https://assets.3dnoticias.com.ar/DELTADED.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'Primera muerte por la variante Delta en Argentina: falleció el "paciente
  cero" de Córdoba'
title: 'Primera muerte por la variante Delta en Argentina: falleció el "paciente cero"
  de Córdoba'
entradilla: Era un hombre que había viajado a Perú. Estaba internado desde hace tres
  semanas por una neumonía bilateral grave y no se había vacunado.

---
El hombre de nacionalidad boliviana conocido como el 'caso cero' por introducir la variante Delta de coronavirus a Córdoba y generar un brote de contagios falleció este domingo por la mañana en el Hospital Rawson, donde se encontraba internado desde hace tres semanas.

El deceso se dio por su avanzada complicación en la salud, ya que sufría "insuficiencia respiratoria causada por neumonía bilateral grave", y no estaba vacunado contra el coronavirus.

El fallecido había ingresado al país a mediados de julio, y no respetó el aislamiento obligatorio, además de participar de reuniones sociales, y así generó un foco de contagios intrafamiliares, donde resultaron contagiadas 35 de las 60 personas que portan o portaron esa cepa India en la provincia.

El Ministerio de Salud de Córdoba notificó que en las últimas 24 horas se conoció un nuevo caso de contagio de coronavirus con la variante Delta, con lo que suman 60 los infectados.

El informe oficial indica que el contagio recientemente registrado está vinculado a un contacto estrecho de otra persona que volvió de viaje desde el exterior, y que se encuentra realizando el debido aislamiento.

El caso cero o caso índice en la provincia fue notificado el 29 de julio y se trata de un viajero de 62 años de origen boliviano, residente en la ciudad de Córdoba, que arribó proveniente de Lima (Perú).

De acuerdo con los datos de la causa judicial, promovida por el Gobierno local, el viajero habría violado el aislamiento domiciliario que se le había impuesto y generó un brote intrafamiliar.

El hombre habría concurrido a un restaurante con su familia y a centros de compra durante el período en que debía estar aislado.

La Unidad Fiscal de Emergencia (UFES), que investiga la causa , imputó al viajero y a otras cuatro personas que, a sabiendas de que el hombre portaba el virus, compartieron encuentros.

En todos los casos fueron acusados por violar los artículos 202 y 205 del Código Penal, que establece penas de entre 3 y 15 años de prisión por realizar acciones que propagan la enfermedad viral.

Tras conocerse los primeros contagios a finales del mes pasado, la autoridad sanitaria había impuesto el aislamiento preventivo de 800 personas por ser contactos estrechos, contactos de contactos, y de burbujas escolares vinculadas con el denominado 'caso cero' y su entorno.