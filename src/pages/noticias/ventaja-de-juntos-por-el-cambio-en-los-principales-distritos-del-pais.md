---
category: Agenda Ciudadana
date: 2021-09-13T06:15:27-03:00
thumbnail: https://assets.3dnoticias.com.ar/DEPEDRO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Ventaja de Juntos por el Cambio en los principales distritos del país
title: Ventaja de Juntos por el Cambio en los principales distritos del país
entradilla: '"Siendo las 21.30 horas tenemos un poco más del 60 por ciento de las
  mesas escrutadas", anunció el ministro De Pedro, al hablar en la sede central de
  Correo Argentino.'

---
Con más del 75% de las mesas escrutadas a nivel nacional, Juntos por el Cambio se imponía en los principales distritos del país, incluida la provincia de Buenos Aires, donde la ventaja se estiraba a unos cinco puntos.  
Escrutado el 67,9% por ciento, los primeros datos en la provincia de Buenos Aires en las primarias abiertas, simultáneas y obligatorias (PASO) desarrolladas eran los siguientes:  
  
\- Juntos por el Cambio: 38,30 por ciento.  
  
\- Frente de Todos 33,52 por ciento.  
  
\- Frente de Izquierda y de Trabajadores: 5,14 por ciento  
  
\- Avanza Libertad (José Luis Espert): 4,48  
  
\- Vamos con Vos (Florencio Randazzo): 3,70  
  
**Ciudad de Buenos Aires**

En la Ciudad de Buenos Aires, escrutadas el 82 por ciento de las mesas en la ciudad de Buenos Aires, Juntos conseguía el 48,36 por ciento de los votos con las tres listas sumadas que compitieron en la interna de las Paso, mientras que el Frente de Todos conseguía el 24,55 por ciento de los votos.  
  
María Eugenia Vidal está obteniendo 475.021 votos y el precandidato del Frente de Todos, Leandro Santoro, conseguía 353.205 votos La nómina de Ricardo López Murphy contaba con 165.494 votos.  
  
El precandidato a diputado nacional por Avanza la Libertad, Javier Milei, está consiguiendo 199.057 votos (13,6 por ciento ) y la postulante postulante del Frente de Izquierda Miryam Bregman 90.153 votos.  
  
**En Mendoza**

Con el 75,61 por ciento de los votos escrutados, Cambia Mendoza se imponía con más del 43%.Escrutado el 75,61 por ciento de los votos de las primarias, abiertas y simultáneas y obligatorias (PASO) en la provincia de Mendoza, los resultados provisorios para las principales fuerzas políticas son los siguientes:  
  
**Para la renovación de las tres bancas en la Cámara de Senadores:**  
  
Cambia Mendoza 43,88% (341.393 votos).  
  
Frente de Todos 24,71% (193.239).  
  
Partido Verde cosecha 5,60% (43.606).  
  
Frente de Izquierda, 5,00% (38.991).  
  
**Para la renovación de las cinco bancas en la Cámara de Diputados:**  
  
Cambia Mendoza 43,82% (335.072 votos)  
  
Frente de Todos 24,66% (188.614 votos)  
  
Partido Verde 5,61% (42.938 votos)  
  
Frente de Izquierda 5,02% (39.065 votos)  
  
**Santa Fe**

En Santa Fe Losada se imponía por una diferencia mínima en la PASO de JxC, mientras que Lewandowski venció a Rossi en el FdT.Escrutado el 70,12 por ciento de los votos de las primarias, abiertas y simultáneas y obligatorias (PASO) en la provincia de Santa Fe, los resultados provisorios para las principales fuerzas políticas son los siguientes.  
  
**Para la categoría senadores nacionales:**  
  
Juntos por el Cambio: 40,90%. "Cambiemos con ganas (Carolina Losada) 30,58%, "Evolución" (Maximiliano Pullaro) 30,10%,  
  
30,34%, "Santa Fe nos une" (Federico Angelini) 23,49%, "Vamos juntos" (José Corral) 15,87%.  
  
Frente de Todos: 29,85%. "Celeste y Blanca" (Marcelo Lewandowski) 67,13%, "La Santa Fe que queremos" (Agustín Rossi) 32,86%.  
  
Frente Amplio Progresista: 10,40%. "Adelante" (Clara García) 67,91%, "Preparados para Santa Fe" (Rubén Gustiniani), 32,08%.  
  
Unite por la libertad y la dignidad: 2,83%. (Betina Florito).  
  
**Para la categoría diputados nacionales:**  
  
Juntos por el Cambio: 41,26%. "Cambiemos con ganas" (Mario Barletta) 30,04%, Evolución" (Gabriel Chumpitaz) 29,20%,  
  
30,35%, "Santa Fe nos une" (Luciano Laspina) 22,87%, "Vamos juntos" (Roy López Molina) 15,77%.  
  
Frente de Todos: 29,16%. "Celeste y Blanca" (Roberto Mirabella) 67,01%, "La Santa Fe que queremos" (Eduardo Toniolli) 32,98%  
  
Frente Amplio Progresista: 10,09%. "Adelante" (Mónica Fein) 69,02%, "Preparados para Santa Fe" (Fabián Oliver), 30,97%.  
  
Unite por la libertad y la dignidad: 3,00%. (Alejandra Oliveras). 2,98%  
  
Soberanía Popular: 2,86% (Carlos del Frade)

Los primeros resultados de las Primarias, Abiertas, Simultáneas y Obligatorias (PASO) fueron publicados esta noche, poco después de las 21.30. Así lo informó el ministro del Interior, Eduardo 'Wado' de Pedro, en una conferencia de prensa en la sede central del Correo Argentino.  
"Dimos otro paso definitivo en mejoramiento permanente del sistema electoral argentino" ya que "a partir de hoy por primera vez todos los fiscales de todas las fuerzas tienen la trazabilidad de todos los telegramas", explicó.  
Las PASO 2021 para elegir candidatos a legisladores en todo el país cerraron a las 18, luego de una jornada que transcurrió con normalidad y en la que acudió un importante caudal de votantes a las urnas, en comparación con la media histórica de los últimos años y pese a la pandemia de coronavirus.  
Más temprano, luego del cierre de los centros de votación, De Pedro reflexionó que se trató de una "jornada electoral histórica" que "refuerza el espíritu democrático y de participación que hay en en la sociedad argentina" y señaló que los comicios transcurrieron "en paz, con tranquilidad y orden".  
"Cuando tuve que votar me emocioné porque también recordé que llevamos 38 años ininterrumpidos de democracia y eso es lo que refleja que los argentinos y las argentinas aprendimos del pasado y es lo que tenemos que seguir cuidando nosotros y las generaciones futuras: nuestro sistema democrático, que permite la discusión y el debate y que comencemos a alejar los discursos del odio", reflexionó De Pedro.  
También destacó el "consenso alcanzado en el Congreso nacional" para modificar la fecha original de la elección realizada hoy y sumar así "12 millones de personas" vacunadas contra el coronavirus en las últimas semanas.  
"De esta manera pudimos ir a votar con más tranquilidad y menos miedo", dijo De Pedro y agradeció, no sólo a los votantes, sino también a quienes trabajaron para la organización de los comicios: "Fue un desafío de casi un millón de personas, tuvimos que combinar y articular mucho trabajo", dijo.  
Ponderó además, "en nombre del Gobierno nacional" la labor desplegada por "los integrantes del Comando Nacional electoral", quienes -señaló- cumplieron "un rol esencial", al igual que "los jueces federales" y los "gobernadores provinciales", y la participación de "más de 100.000 presidentes de mesa y casi 500.000 fiscales de todas las fuerzas políticas que sumaron y aportaron militancia, compromiso y transparencia al proceso electoral".