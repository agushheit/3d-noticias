---
category: La Ciudad
date: 2021-05-08T06:00:07-03:00
thumbnail: https://assets.3dnoticias.com.ar/Castraciones.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Municipalidad continúa con la campaña de castración de animales
title: La Municipalidad continúa con la campaña de castración de animales
entradilla: Durante el mes de mayo, sigue el plan de castración móvil por turnos,
  que lleva adelante el municipio con el objetivo de acercar el servicio, de manera
  gratuita, a todos los barrios de la capital provincial.

---
La Municipalidad difundió el nuevo cronograma de castración móvil correspondiente al mes de mayo, destinada a animales de compañía. Se trata de un servicio gratuito que se presta a vecinos y vecinas con la intención de controlar la población felina y canina de manera ética y responsable. Es parte de las acciones que el intendente Emilio Jatón considera centrales al momento de trabajar sobre la salud animal y la construcción de una mejor convivencia en el espacio público.

La atención se brinda con turnos programados que pueden obtenerse en la web oficial www.santafeciudad.gov.ar/turnos. Allí se debe buscar el apartado denominado “Castraciones”, se elige la opción “Solicitar un nuevo turno” y se completa el formulario con los datos pedidos. Posteriormente se escoge el día, el horario y la sede a la cual concurrir.

Las acciones previstas en la campaña incluyen la vacunación antirrábica y la atención veterinaria, todo de manera gratuita. Los servicios se prestan en el Instituto Municipal de Salud Animal (IMUSA), de lunes a viernes en las sedes del Parque Garay, de 8 a 17 horas; y del Jardín Botánico, de 8 a 13 horas. En tanto, en diferentes barrios de la ciudad, continúan según el siguiente esquema:

**Del 7 al 14 de mayo:**

– Distrito Centro/Este: Vecinal Central Guadalupe, calle Javier de la Rosa 1065

**Del 7 al 21 de mayo:**

– Distrito de La Costa: Vecinal Alto Verde, calle Demetrio Gómez S/N

**Del 10 al 21 de mayo, de 8 a 12 horas:**

– Distrito Noroeste: Casa del Sur, calle Azcuénaga 6400

– Distrito Norte: Vecinal Cabaña Leiva, Edmundo Rosas y Pedro de Espinosa

– Distrito Oeste: Vecinal Pro-mejoras Barranquitas, calle Artigas 4100

– Distrito Suroeste: Vecinal Estrada, calle Estrada 2990