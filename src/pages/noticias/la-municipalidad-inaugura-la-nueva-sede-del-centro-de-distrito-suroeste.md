---
category: Estado Real
date: 2020-12-01T11:32:03Z
thumbnail: https://assets.3dnoticias.com.ar/DISTRITO-SUROESTE.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: La Municipalidad inaugura la nueva sede del Centro de Distrito Suroeste
title: La Municipalidad inaugura la nueva sede del Centro de Distrito Suroeste
entradilla: Será el jueves 10 de diciembre, en la rotonda de Boca del Tigre. A partir
  de esa fecha, se atenderá al público, en el horario de 7.30 a 17.

---
El jueves 10 de diciembre, la Municipalidad inaugurará la nueva sede del Centro de Distrito Suroeste, ubicada en la rotonda conocida como Boca del Tigre (J.J. Paso y Zavalla), frente al Club Atlético Colón. Se trata de un espacio que se encontraba vacío y, gracias a una serie de trabajos a cargo de la Secretaría de Obras y Espacio Público, pudo volver a ocuparse.

El Centro de Distrito ubicado en avenida General López 3681/85 completa actualmente las tareas propias del traslado. Desde el 10 de diciembre, el horario de atención de la nueva sede será de 7:30 a 17.

  
 

## **Las obras**

![](https://assets.3dnoticias.com.ar/SUROESTE 1.jpg)

Este año, y con recursos y trabajadores municipales, en el edificio conocido como Boca del Tigre se concretaron refacciones, como la limpieza general de la terraza, el sellado de grietas y fisuras, la impermeabilización de la losa y la restitución de revoques. También se construyeron nuevos baños, se reemplazó toda la instalación eléctrica, se colocó nueva iluminación y, por último, el equipamiento necesario para el funcionamiento del distrito.

También se realizaron trabajos relacionados con la peatonalización de buena parte del área conocida como Boca del Tigre, para facilitar el acceso al inmueble central. El objetivo era que las personas lleguen a la sede del distrito de manera fácil y segura, teniendo en cuenta la alta afluencia de vehículos que transitan por la zona, principalmente desde y hacia Santo Tomé.

La idea central fue hacer más amigable el entorno para el peatón y para los medios reducidos de movilidad, mientras se generaba un espacio de permanencia disfrutable, con bancos, plantas y cestos de residuos, alrededor del edificio donde se ubicará el Distrito Suroeste.

![](https://assets.3dnoticias.com.ar/SUROESTE2.jpg)