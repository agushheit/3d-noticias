---
category: Agenda Ciudadana
date: 2021-02-06T07:33:24-03:00
thumbnail: https://assets.3dnoticias.com.ar/t4eletrabajo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Infobae
resumen: El 1 de abril comienza a regir la ley de teletrabajo. El sector privado espera
  un aumento del costo laboral y más juicios
title: El 1 de abril comienza a regir la ley de teletrabajo. El sector privado espera
  un aumento del costo laboral y más juicios
entradilla: 'El Ministerio de Trabajo publicó en el Boletín Oficial la resolución
  que fija la entrada en vigencia de la norma. '

---
El régimen de **teletrabajo** entra en vigencia a partir del próximo 1° de abril. El Gobierno definió el inicio del marco regulatorio del modo de trabajo que marcó gran parte de 2020 en medio de la pandemia de COVID-19. Su reglamentación, días atrás, generó cuestionamientos por parte del sector privado que advirtió un impacto negativo en el mercado laboral y en la estructura de costos de las empresas.

Cuando el Congreso aprobó la ley se aclaró que entraría en vigor luego de 90 días contados a partir de que se determine la finalización del período de vigencia del aislamiento social, preventivo y obligatorio. En la actualidad, Argentina transita la pandemia con las flexibilizaciones que permite la etapa de distanciamiento social, y aguardaba que el **Ministerio de Trabajo** dictase la resolución fijando la fecha de inicio del cómputo de tres meses indicados para que comience a regir la ley.

En efecto, este viernes en el **Boletín Oficial**, la cartera que conduce **Claudio Moroni** publicó la Resolución 54/2021 que fija como fecha de comienzo de dicho cómputo el 21 de diciembre de 2020, por lo que “a fin de dar certeza frente a un régimen laboral que se proyecta sobre obligaciones mensuales, corresponde establecer que el Régimen Legal del Contrato de Teletrabajo entrará en vigencia el 1° de abril del 2021″.

De esta forma, frente a un 2021 que estará marcado por la “nueva normalidad” y la incertidumbre por la situación epidemiológica, el Poder Ejecutivo confirmó que dentro de dos meses se aplicará la ley de teletrabajo.

El primer artículo de la norma establece que “las disposiciones de la Ley Nº 27.555 **no serán aplicables cuando la prestación laboral se lleve a cabo en los establecimientos, dependencias o sucursales de las y los clientes a quienes el empleador o la empleadora preste servicios de manera continuada o regular”.** Tampoco “en los casos en los cuales la labor se realice en forma esporádica y ocasional en el domicilio de la persona que trabaja, ya sea a pedido de esta o por alguna circunstancia excepcional”.

Su implementación prevé un escenario negativo para el sector privado, advirtieron economistas. Los puntos más cuestionados de la norma son la **reversibilidad**, que implicaría que las compañías mantengan el lugar de empleo “físico” por si un trabajador que hace _homeoffice_ quisiera volver a su oficina, y la posibilidad de **“desconexión digital”**, que no se ajustaría a la realidad de distintos sectores económicos.

Reversibilidad: la ley otorga al empleado la posibilidad de solicitar al empleador **volver a desarrollar tareas en la empresa sin posibilidad de que el empleador rechace el pedido**. En la reglamentación publicada a mediados de enero, se determinó que este derecho deberá ajustarse a los artículos 9 y 10 del Código Civil y Comercial de la Nación y 62 y 63 de la Ley de Contrato de Trabajo. “Recibida la solicitud de la persona que trabaja, con la sola invocación de una motivación razonable y sobreviniente, el empleador o la empleadora deberá cumplir con su obligación en el menor plazo que permita la situación del o de los establecimientos al momento del pedido”, establece el texto oficial. **En ningún caso dicho plazo podrá ser superior a 30 días.**

Desconexión digital: el otro punto bajo la lupa de los empresarios. El derecho a la desconexión digital establece que cuando la actividad de la empresa se realice en diferentes husos horarios o en aquellos casos en que resulte indispensable **“por alguna razón objetiva, se admitirá la remisión de comunicaciones fuera de la jornada laboral”**. “En todos los supuestos, la persona que trabaja no estará obligada a responder hasta el inicio de su jornada”, se precisó, y aclaró que “no se podrán establecer incentivos condicionados al no ejercicio del derecho a la desconexión” y “los incrementos vinculados a la retribución de las horas suplementarias no serán considerados incentivos”.

Para los economistas consultados por **Infobae**, este tipo de medidas representan un **desincentivo a la contratación de empleados y criticaron la ley por “ser muy rígida y poner trabas” al funcionamiento de la relación laboral** entre el empleador y el trabajador.

<br/>

![](https://assets.3dnoticias.com.ar/060221-consultoria.webp)

<br/>

Para **María Castiglioni Cotter**, de C&T Consultores Económicos, la ley “tiene demasiados puntos que generan trabas o desincentivo”. “Por ejemplo, **los costos que implican para una pyme la reversibilidad, que requiere mantener el lugar para el empleado que quiera dejar de teletrabajar. Esto en realidad desincentiva al régimen**”, explicó la economista.

Respecto al derecho a “desconexión digital”, Castiglioni Cotter remarcó que “no todos los trabajos son iguales”. “Una cosa es atención al público, otras pueden ser más de análisis, donde se trabaja con objetivos y sin horarios tan marcados. **Este tipo de regulaciones tenés que manejarlas a nivel sectorial o nivel empresa. Necesita una flexibilidad que no tiene esta ley**”, criticó. “Podés tener una ley menos rígida, no tan específica. **Ese exceso de regulación termina elevando costos, abre la puerta a posibles juicios”**, dijo.

**Miguel Calello**, integrante de la comisión directiva y expresidente de la Cámara de la Industria Argentina de del Software (CESSI), señaló que **“la ley no es buena para el sector,** quizás para otros sí, pero no para el nuestro. Habíamos pedido que dejen afuera a las empresas de economía del conocimiento porque si no se aprobaba una ley que las beneficia y esto son piedras en el camino, pero no pudo ser”, agregó Calello.

<br/>

![](https://assets.3dnoticias.com.ar/claudio-moroni.webp)

###### Claudio Moroni, ministro de Trabajo

<br/>

### **Otros puntos de la ley:**

El trabajador que ejerza el derecho a **interrumpir la tarea por razones de cuidado**, deberá comunicar en forma virtual y con precisión el momento en que comienza la inactividad y cuando esta finaliza. En los casos en que las tareas de cuidado no permitan cumplir con la jornada legal o convencional vigente, se podrá acordar su reducción de acuerdo con las condiciones que se establezcan en la convención colectiva.

Por otra parte, **los empleados que hubiesen pactado la modalidad de teletrabajo desde el inicio de la relación laboral no pueden revocar su consentimiento ni ejercer el derecho a que se les otorguen tareas presenciales**, salvo lo dispuesto en los Convenios Colectivos del Trabajo o en los contratos individuales.

Con respecto a los elementos de trabajo, su provisión “no se considera remuneratoria, y en consecuencia, no integra la base retributiva para el cómputo de ningún rubro emergente del contrato de trabajo, ni contribuciones sindicales o de la seguridad social”. Y la compensación de gastos, “aun sin comprobantes”, tampoco se considera remuneratoria.

***

[![Resolución 54/2021](https://assets.3dnoticias.com.ar/resol-542021.webp)](https://assets.3dnoticias.com.ar/B-Oficial-240482.pdf  "PDF")

***