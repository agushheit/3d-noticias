---
category: La Ciudad
date: 2021-04-09T06:44:21-03:00
thumbnail: https://assets.3dnoticias.com.ar/ecocanje.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad de Santa Fe
resumen: La Municipalidad continúa con las jornadas de ecocanje en distintos puntos
  de la ciudad
title: La Municipalidad continúa con las jornadas de ecocanje en distintos puntos
  de la ciudad
entradilla: "\nEn diferentes espacios, una vez al mes se recibirá material reciclable,
  limpio y seco, como cartón, papel, plástico y latas. A cambio se otorgarán semillas
  y plantines. "

---
La Municipalidad continúa con la campaña de reciclado a partir de nuevas jornadas de ecocanjes. En diferentes puntos, durante la mañana, una vez al mes, se instalarán puestos en los que, a cambio de semillas y plantines para huertas familiares, se recibirán materiales reciclables o reutilizables limpios y secos, como papel, cartón, latas y plástico.

Lo recolectado es enviado a la planta de clasificación de materiales reciclables o reutilizables del Complejo Ambiental, para ser procesado y comercializado por la Asociación Civil Dignidad y Vida Sana, entidad que opera en el lugar.

El primer miércoles de cada mes, en la Feria agroecológica Candioti se instalará uno de estos puestos. Será en la Plazoleta Emilio Zola (Diagonal Caseros 1900), de 10 a 12 horas. El segundo miércoles de cada mes, sucederá lo mismo en el Jardín Botánico Ing. Lorenzo Parodi, en Av. Gorriti 3900, de 10 a 13; y el tercer  miércoles, se pondrá uno de estos puntos de intercambio en el distrito Noreste, en Aristóbulo del Valle 9446, de 14 a 16.

El cronograma sigue el primer viernes del mes, en la Estación de Barranquitas, ubicada en Iturraspe esquina Juan Manuel Estrada, de 10 a 12; y el tercer viernes de cada mes, se pondrá un puesto de canje en la zona del Distrito La Costa, en la Ruta 1 y Bv. Los Eucaliptos, de 10 a 13.

Por último, el segundo sábado del mes, el Mercado Progreso será sede de un punto de Ecocanje, en Balcarce 1635, de 10 a 12, donde se recepcionarán los elementos a cambio de semillas; y el tercer sábado del mes, el punto se mudará a la vecinal Ceferino Namuncurá, en  12 de Octubre 9501, también de 10 a 12.

**Puntos fijos**

Por otro lado es importante recordar que continúan los puntos fijos de las Eco Estaciones de Reciclado ubicadas en lugares como la Estación Belgrano; los Distritos Este, Noroeste y La Costa; en la Estación de Cuidados Las Paltas, en Colastiné; en ACAV, en la avenida Aristóbulo del Valle 6726; en el Mercado Norte; en el Shopping Puerto Plaza; en el Palacio Municipal; en el IMUSA del Parque Garay y en el del Jardín Botánico; en Parque Sur; y en las costaneras Este y Oeste.