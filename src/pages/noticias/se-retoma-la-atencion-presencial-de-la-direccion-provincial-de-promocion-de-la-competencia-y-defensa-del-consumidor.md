---
category: Estado Real
date: 2021-08-01T06:15:36-03:00
thumbnail: https://assets.3dnoticias.com.ar/DEFENSA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Se retoma la atención presencial de la Dirección Provincial de Promoción
  de la Competencia y Defensa del Consumidor
title: Se retoma la atención presencial de la Dirección Provincial de Promoción de
  la Competencia y Defensa del Consumidor
entradilla: El Ministerio de Producción, Ciencia y Tecnología,}, informa que desde
  el próximo lunes se volverá a brindar atención de manera presencial, en sus sedes
  ubicadas en Santa Fe y Rosario.

---
Desde el próximo lunes se volverá a brindar atención de manera presencial, en sus sedes ubicadas en Santa Fe y Rosario.

>> Sede Santa Fe: Bv. Pellegrini 3100.

>> Sede Rosario: Mitre y Ricardone.

La atención presencial se realizará previo otorgamiento de turno que puede ser solicitado en el siguiente link: [https://turnos.santafe.gov.ar/turnos/web/frontend.php](https://turnos.santafe.gov.ar/turnos/web/frontend.php "https://turnos.santafe.gov.ar/turnos/web/frontend.php")

Opción de trámite: “Denuncia Defensa del Consumidor”

Asimismo, continúa la atención a distancia, tanto para denuncias como reclamos por defensa del consumidor, a través de las diferentes vías que han sido medio de actuación durante el transcurso de todo el 2020:

>> 0800-555-6768 (Opción 3)

>> Ventanilla Única Federal de Defensa del Consumidor ingresando al siguiente link: [https://www.argentina.gob.ar/produccion/defensadelconsumidor/formulario](https://turnos.santafe.gov.ar/turnos/web/frontend.php "https://turnos.santafe.gov.ar/turnos/web/frontend.php")

El horario de atención, tanto para la virtualidad como para la presencialidad, será de lunes a viernes de 08 a 13 horas.

Aquellas personas que se acerquen a las sedes deben tener en cuenta que, por cuestiones sanitarias, siguen vigentes los protocolos obligatorios para la prevención del COVID-19 a fin de resguardar la salud de la ciudadanía y del personal de la institucionará a partir del lunes 2 de agosto en sus sedes ubicadas en Santa Fe y Rosario.