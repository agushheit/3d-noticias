---
category: Agenda Ciudadana
date: 2021-08-12T06:15:36-03:00
thumbnail: https://assets.3dnoticias.com.ar/INDUSTRIAS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: En un año y medio de pandemia, el ausentismo en industrias santafesinas llega
  al 15%
title: En un año y medio de pandemia, el ausentismo en industrias santafesinas llega
  al 15%
entradilla: Así lo afirmaron desde la Unión Industrial de Santa Fe. En Buenos Aires
  el ausentismo es del 30%, en medio de la polémica de los empleados ausentados por
  no querer vacunarse contra el Covid.

---
El actual contexto de pandemia, que puso en jaque las prácticas laborales en las empresas del país, continúa impactando en el nivel de ausentismo de los empleados. En este marco, desde la Unión Industrial de Santa Fe (Uisf) reportaron que el índice de ausentismo en industrias santafesinas llega al 15%, en medio de la polémica por los grupos de trabajadores que se ausentan por decidir no ser vacunados contra el Covid-19.

El dato fue informado por el presidente de la Uisf, Alejandro Taborda, quien se hizo eco de lo que manifestó su par de la Unión Industrial Argentina (UIA), Daniel Funes de Rioja, sobre la propuesta de no pagar salarios a los trabajadores que se ausentaran por decidir no vacunarse.

Funes de Rioja había enfatizado que "aquel que está con primera dosis ya puede ser convocado, pero se genera el problema con los que no quieren vacunarse. En ese caso nadie puede obligarlos, pero nadie puede obligar a insertarlos en un medio laboral con riesgo para otros trabajadores y sus familias. Por lo tanto, entendemos que en esos casos cesa la dispensa y cesa la remuneración".

Frente a esto, Taborda destacó que los trabajadores santafesinos que decidieron no vacunarse se tratan de "excepciones muy puntuales". Añadió que "en el relevamiento que se hizo en el centro norte de la provincia de Santa Fe junto con la Federación Industrial no vemos que sea una generalidad. No creemos que sea un problema grave".

Al mismo tiempo, el referente de las industrias santafesinas advirtió que "hoy por hoy no hay ningún reglamento legal que exija eso y por ahora no vemos la necesidad de que haya algo al respecto".

Los números de ausentismo frente al avance de la pandemia son más acentuados en las industrias de la provincia de Buenos Aires, donde hay un 30% de trabajadores ausentados de su puesto de trabajo, el doble que Santa Fe. Al ser consultado por si la UIA contempla la posibilidad de una ola de juicios por esta postura, Funes de Rioja admitió que "en la Argentina todo se puede judicializar, pero la UIA no afirma que se tiene que despedir al trabajador, sino que cada empresa sabrá qué hacer en la práctica y los riesgos que asume".

"Lamentablemente aún con la norma muy clara a nadie se le puede ocurrir que alguien pueda ganar un salario sin trabajar. Este es el marco. Lo deseable es que no se judicialice porque hay suficiente argumentación, pero la decisión la va a tomar cada empleador", reseñó el dirigente de la UIA.

En ese sentido, el referente de las industrias de Santa Fe subrayó: "Estamos aconsejando a todas las empresas al diálogo constructivo con los sindicatos y operarios, haciendo hincapié en la necesidad de la vacunación. Tenemos buenas referencias de que la mayoría de operarios se ha querido vacunar, muchos con dos dosis".

"Nosotros estamos en contra de la prohibición de despidos porque trae toda una interferencia. Estamos mostrando signos de recuperación industrial y del empleo, y creemos que todas estas regulaciones lo que hacen es perjudicar a quien no tiene trabajo. No es necesaria esta regulación porque las empresas no están despidiendo gente", concluyó Taborda.