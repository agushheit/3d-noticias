---
category: Agenda Ciudadana
date: 2021-08-14T06:15:28-03:00
thumbnail: https://assets.3dnoticias.com.ar/monotributo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Lo que hay que saber sobre los nuevos créditos a tasa cero para monotributistas
title: Lo que hay que saber sobre los nuevos créditos a tasa cero para monotributistas
entradilla: " La propuesta no aplica para quienes además perciban ingresos en relación
  de dependencia, o estén inscriptos en el SIPA como trabajadores autónomos."

---
El Gobierno formalizó la creación del Programa Crédito a Tasa Cero 2021, que ofrecerá financiamiento por hasta $ 150.000 para un millón y medio de monotributistas de todo el país, a través del decreto 512/2021 publicado este viernes en el Boletín Oficial.

El programa tiene el objetivo de asistir a trabajadores y trabajadoras adheridos y adheridas al Régimen Simplificado para Pequeños Contribuyentes (RS), con el fin de acompañar el proceso de recuperación productiva.

Crédito a Tasa Cero 2021 consistirá en la obtención, por parte de las beneficiarias y los beneficiarios, de un crédito con subsidio del 100% del costo financiero total.

Podrán revestir la condición de beneficiarias y beneficiarios del Programa los sujetos que se encuentren adheridos al Monotributo, y cumplan con las condiciones que establezca el Ministerio de Desarrollo Productivo.

No se encuentran comprendidos en el Programa los sujetos adheridos al Régimen Simplificado para Pequeños Contribuyentes (RS) que perciban ingresos en relación de dependencia, o estén inscriptos en el Sistema Integral Previsional Argentino (SIPA) como trabajadores autónomos.

El Crédito a Tasa Cero 2021 consistirá en una financiación cuyo importe, según cada categoría del Monotributo, no podrá exceder de $ 90.000 para la A; $ 120.000 para la B y $ 150.000 para las restantes

Además, Desarrollo Productivo podrá definir otros sujetos no comprendidos en el presente Programa.

El Crédito a Tasa Cero 2021 consistirá en una financiación cuyo importe, según cada categoría del Monotributo, no podrá exceder de $ 90.000 para la A; $ 120.000 para la B y $ 150.000 para las restantes.

La obtención del financiamiento será compatible con haber recibido un Crédito a Tasa Cero el año pasado.

En el caso de monotributistas que no se encuentren en mora en el cumplimiento de sus obligaciones ni en las condiciones de vigencia de los beneficios que les fueran acordados, así como tampoco se encuentren con ejecución en trámite, o se les hubiera ejecutado la garantía del Fondo de Garantías Argentino (FoGAr), podrán acceder al nuevo crédito en las mismas condiciones que quienes no hayan accedido en 2020.

En el caso de quienes hubieran obtenido un Crédito a Tasa Cero en el marco del Programa ATP y se encuentren en mora en el cumplimiento del crédito 2020, deberán destinar el monto del nuevo crédito a cancelar la totalidad del monto adeudado y podrán gozar libremente del saldo, si quedare remanente.

Por su parte, los monotributistas que hubieran obtenido un Crédito a Tasa Cero en 2020, respecto de las o los cuales, la entidad financiera otorgante del crédito hubiera iniciado la ejecución o ejecutado la garantía del Fondo de Garantías Argentino (FoGAr), estarán excluidas y excluidos del nuevo beneficio.

En este sentido, el FoGAr podrá establecer pautas especiales para la obtención de una refinanciación sobre el saldo de capital del crédito adeudado.

Asimismo, los beneficiarios y las beneficiarias del nuevo programa, hasta la cancelación total de la financiación, no podrán acceder al Mercado Único y Libre de Cambios para realizar operaciones correspondientes a la formación de activos externos.

Tampoco podrán concertar ventas en el país de títulos valores con liquidación en moneda extranjera, o canjes de títulos valores por otros activos externos o transferencias de los mismos a entidades depositarias del exterior.