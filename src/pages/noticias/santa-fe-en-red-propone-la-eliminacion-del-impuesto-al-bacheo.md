---
category: La Ciudad
date: 2021-08-19T06:00:42-03:00
thumbnail: https://assets.3dnoticias.com.ar/ANTIBACHEO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Santa Fe en Red
resumen: Santa Fe en Red propone la eliminación del impuesto al bacheo
title: Santa Fe en Red propone la eliminación del impuesto al bacheo
entradilla: En la mañana del martes, se propuso en el Concejo Municipal un proyecto
  para eliminar el tributo "Derecho de Mantenimiento por el uso de la red vial Municipal"
  (popularmente conocido como "Impuesto al Bacheo").

---
El proyecto prevee la inmediata interrupción del cobro de este tributo que, junto al Impuesto a la Patente, constituye un caso de doble imposición tributaria en Santa Fe, situación que debe ser evitada según los principios legales de nuestro ordenamiento jurídico.

Creemos que, en estos tiempos difíciles para la economía de los vecinos, el municipio debe limitar su intervención para posibilitar el desarrollo integral de los santafesinos, colaborando a que reconstruyan su capacidad de ahorro y a que inviertan en la ciudad.

Instagram: [https://www.instagram.com/p/CSsK8uKHw3z/?utm_medium=copy_link](https://www.instagram.com/p/CSsK8uKHw3z/?utm_medium=copy_link "https://www.instagram.com/p/CSsK8uKHw3z/?utm_medium=copy_link")

Facebook: [https://www.facebook.com/105737377764436/posts/364146045256900/](https://www.instagram.com/p/CSsK8uKHw3z/?utm_medium=copy_link "https://www.instagram.com/p/CSsK8uKHw3z/?utm_medium=copy_link")

Twitter: [https://twitter.com/SantaFeenRed_/status/1427737276373688322?s=19](https://www.instagram.com/p/CSsK8uKHw3z/?utm_medium=copy_link "https://www.instagram.com/p/CSsK8uKHw3z/?utm_medium=copy_link")