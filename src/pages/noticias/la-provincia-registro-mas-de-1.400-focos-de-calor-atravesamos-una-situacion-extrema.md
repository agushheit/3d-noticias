---
category: Agenda Ciudadana
date: 2022-01-15T06:00:18-03:00
thumbnail: https://assets.3dnoticias.com.ar/INCENDIOS.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'La provincia registró más de 1.400 focos de calor: "Atravesamos una situación
  extrema"'
title: 'La provincia registró más de 1.400 focos de calor: "Atravesamos una situación
  extrema"'
entradilla: 'En solo una semana los focos de calor activos en la provincia pasaron
  de 795 a 1.400. La ministra de Ambiente y Cambio Climático Erika Gonnet sostuvo
  que esto se debe "al cambio climático".

  '

---
En medio de la ola de calor extremo que azota a la provincia de Santa Fe, la ministra de Ambiente y Cambio Climático, Erika Gonnet, advirtió por el incremento de focos de calor que se registran en todo el territorio santafesino provocando incendios. Según advirtió, “atravesamos una situación extrema” e identificó 1.400 focos de calor activos.

“Lo que está sucediendo en la provincia no escapa a lo que pasa en el país, esto tiene que ver con el cambio climático y la crisis climática que nos toca atravesar”, remarcó la funcionaria

Con respecto a los puntos de calor, detalló que a fines de diciembre el mapeo satelital arrojó 415 en toda la provincia. A principios de esta semana, el registro era de 795 y este viernes el número llegó a 1400. “Se trata de puntos de riesgo para que sucedan incendios”, aclaró.

“Esto indica la gravedad y la situación extrema que estamos atravesando. Tenemos focos de incendios muy preocupantes en el área metropolitana en los que venimos trabajando desde la última semana de diciembre”, añadió la ministra.

Con respecto al trabajo que se viene desarrollando en Rosario y la región, principalmente en la zona de islas, Gonnet destacó que hay un operativo montado en Alvear en coordinación con el gobierno de Entre Ríos.

“Tenemos que hacer hincapié en el trabajo interministerial que está haciendo la provincia. Este último año incorporamos 30 nuevos brigadistas que articulan con bomberos, hace 7 meses sumamos un monitoreo por parte del Ministerio de Seguridad e incorporamos un helicóptero”, remarcó.

Además, la funcionaria provincial explicó que cuentan con un convenio para el uso de medios aéreos que le demandan a la provincia una inversión de 1.300.000 de pesos por cada jornada de 6 horas que se realizan.

“No contamos esto porque queramos poner en términos numéricos la inversión, pero para que todos magnifiquemos la intervención que hacemos no solamente con un programa interministerial, sino que con hechos concretos y trabajando articuladamente con las provincias y Nación”, concluyó.