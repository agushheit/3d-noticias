---
category: La Ciudad
date: 2021-03-11T06:16:51-03:00
thumbnail: https://assets.3dnoticias.com.ar/colectivos.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad
resumen: Desde este lunes habrá 40% más de colectivos en la calle
title: Desde este lunes habrá 40% más de colectivos en la calle
entradilla: Luego de las gestiones realizadas ante las empresas, desde el municipio
  se indicó que la intención es ajustar la cantidad de unidades a la demanda de los
  usuarios por el regreso a clases.

---
Desde la Municipalidad se indicó que a partir del próximo lunes, cuando se retome la actividad escolar de forma presencial, la cantidad de coches del transporte urbano de pasajeros por colectivos se verá incrementada en un 40%. Según se informó, más unidades comenzaron a sumarse al sistema el pasado 1° de marzo, cuando volvieron a las aulas los alumnos de 7° grado y 5° año de distintas escuelas y diferentes modalidades. No obstante desde el 15, el aumento será aún más significativo.

El incremento es el resultado de diversas reuniones mantenidas por las autoridades municipales con las empresas del rubro que operan en la capital de la provincia. De este modo, este 40% más de la flota en calle permitirá ofrecer una frecuencia estimada de entre 12 y 15 minutos en todos los casos, aunque dependerá también de la situación de cada línea.

Así lo confirmó el subsecretario de Movilidad y Transporte del municipio, Lucas Crivelli, quien aseguró que el primer día del mes de marzo “se empezaron a ajustar las frecuencias y la cantidad de colectivos en calle, en virtud del comienzo paulatino a clases”. Agregó que desde entonces se inició un proceso de normalización del sistema, “de modo tal de poder prestar un servicio mucho más ajustado a partir del 15 de marzo”.

El funcionario reconoció que “se espera una situación de normalidad que vaya ajustándose día a día pero también dependerá de la modalidad del ciclo lectivo, es decir, de si los chicos van a la escuela todos los días o no”. Por otra parte, destacó que a esta situación se sumará “cómo impacte sobre el servicio y la cantidad de pasajeros, el boleto educativo gratuito”.

Se trata de variantes que tienden a la normalización del sistema, ya que la regularización completa dependerá de la cantidad de pasajeros que se transporte y de la situación sanitaria que se registre en la ciudad. En ese marco, Crivelli aseveró que “que se viene trabajando a diario y monitoreando de manera constante para que se concreten las readecuaciones necesarias que permitan la correcta prestación”.

De todos modos, pidió tener en cuenta que se trata de un servicio que trabaja con corte de boleto cercano al 33%, si se lo compara con igual período del año 2019, es decir, en la pre-pandemia. Una vez que vuelvan las clases con regularidad, se estima que la cantidad de pasajeros se incremente entre un 12 y un 18%, en promedio.

**Medidas de higiene**

El subsecretario municipal de Movilidad y Transporte recordó que, en cuanto a la capacidad de las unidades, pueden ir todos los pasajeros sentados, más diez parados. “Sabemos las dificultades que implica, tanto para el chofer como para los pasajeros, esta situación particular; por eso pedimos a los usuarios que salgan con el debido tiempo de antelación para contemplar esa realidad”, dijo.

Vale recordar que las medidas a cumplir por quienes abordan los colectivos se reducen al uso del barbijo permanente y a mantener la distancia de 1,5 metros con cualquier otra persona que vaya a bordo.

Respecto de la limpieza de las unidades, se mantiene como hasta el momento, tal como fue acordado con las empresas prestatarias: la desinfección se realiza entre dos y tres veces por día, por lo general, en las puntas de cada ramal.

**Cuándo Pasa**

Sobre la aplicación que anticipa el paso de las unidades, Crivelli afirmó que su funcionamiento es el adecuado, pero alertó que pueden registrarse algunas alteraciones relacionadas con los cambios de recorrido como consecuencia del plan de bacheo municipal en marcha.

“Cuando el colectivo se sale de línea puede tener algún retraso en la comunicación o presentar información imperfecta, pero eso tiende a normalizarse en 24 o 48 horas, cuando el coche vuelve a tomar el recorrido original”, explicó. Por este motivo pidió a los usuarios que recurran a las redes sociales del municipio para verificar los cambios de recorridos, en caso de que observen cortes de tránsito.