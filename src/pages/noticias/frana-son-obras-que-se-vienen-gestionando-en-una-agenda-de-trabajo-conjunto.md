---
category: Agenda Ciudadana
date: 2021-04-24T06:00:48-03:00
thumbnail: https://assets.3dnoticias.com.ar/FRANA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'Frana: "Son obras que se vienen gestionando en una agenda de trabajo conjunto"'
title: 'Frana: "Son obras que se vienen gestionando en una agenda de trabajo conjunto"'
entradilla: '"Es un gran día para la provincia de Santa Fe" destacó en Casa de Gobierno
  la ministra de Infraestructura, Servicios Públicos y Hábitat, Silvina Frana.'

---
"Es un gran día para la provincia de Santa Fe" destacó en Casa de Gobierno la ministra de Infraestructura, Servicios Públicos y Hábitat, Silvina Frana al repasar los anuncios que el presidente de la nación, Alberto Fernández, había realizado en Rosario y que implicarán una inversión de 76 mil millones de pesos para la provincia.

Al igual que había señalado en Rosario el gobernador Perotti, la ministra señaló que se puede apreciar un esquema federal como realidad concreta. Validó el concepto del presidente de tener capitales alternas y sacar el gabinete al interior del país para tomar contacto con las diferentes realidades.

En cuanto a las obras comprometidas, Frana destacó que "no son nuevas, se vienen gestionando desde el primer día que llegó esta administración. No son anuncios, son agendas de trabajo" subrayó.

En lo que hace al centro norte santafesino destacó que está en marcha la transformación de la ruta nacional 34 entre el cruce de la autovía ruta 19 y la ciudad de Sunchales pero también marcó la reanudación de los trabajos del desagüe Espora en esta capital así como el compromiso entre provincial y municipalidad para readecuar el Camino Viejo a Esperanza o avenida Menchaca.