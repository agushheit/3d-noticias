---
category: Estado Real
date: 2021-05-24T06:15:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/Gimnasio.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Provincia abre la inscripción para la asistencia económica de emergencia
  a los rubros afectados por la pandemia en departamentos del norte
title: La Provincia abre la inscripción para la asistencia económica de emergencia
  a los rubros afectados por la pandemia en departamentos del norte
entradilla: Se trata de gimnasios, centros de pilates y yoga, complejos de fútbol
  5 y de paddle que se vieron afectados por la medida de cierre dispuesta por el Decreto
  N° 647/21.

---
El Ministerio de Producción, Ciencia y Tecnología a través de la Secretaría de Comercio Interior y Servicios, informa que desde este domingo y hasta el viernes 28 de mayo inclusive, se encuentra disponible en la web oficial del Gobierno de Santa Fe, la inscripción vía formulario online para solicitar la Asistencia Económica de Emergencia a los rubros de actividades recreativas y de entrenamiento privadas que fueron alcanzadas por el cierre que se dispuso en el Decreto N° 647/21, donde se ampliaron las restricciones a los cinco departamentos del norte provincial.

Se trata de los departamentos 9 de Julio, Vera, General Obligado, San Javier y Garay, cuyas localidades, desde el 20 de mayo, también debieron cerrar las puertas en este tipo de comercios y locales, y por ello el gobierno provincial dispuso el otorgamiento de este aporte en concepto de subsidio.

Esta línea de apoyo prevé un monto de dinero que se acredita en la cuenta bancaria o virtual del comercio, y tiene como finalidad asistir a los titulares de estos emprendimientos con un importe que va a oscilar entre los 50 y 100 mil pesos de acuerdo a la condición frente a la AFIP, si abona alquiler y si cuenta con empleados en relación de dependencia.

Para acceder al formulario, la información y conocer la documentación que se debe adjuntar al trámite, se dispuso en el botón "Coronavirus" del sitio de internet, dentro del enlace "Asistencia Económica de Emergencia", o haciendo click en el siguiente link: [https://servicios.santafe.gov.ar/asistencia_ampliacion/.](https://servicios.santafe.gov.ar/asistencia_ampliacion/. "https://servicios.santafe.gov.ar/asistencia_ampliacion/.")

Al respecto se refirió el secretario de Comercio Interior y Servicios, Juan Marcos Aviano, quien manifestó: "Abrimos la inscripción para los departamentos que no estaban alcanzados con la medida de cierre de estos comercios. El pasado viernes 21 cerramos e hicimos lo propio con los 14 departamentos que venían adaptándose a las nuevas medidas de convivencia en pandemia, y el esfuerzo que está haciendo el sector privado tiene en esta gestión, por instrucciones del gobernador Omar Perotti, una respuesta y acompañamiento. En este caso se trata de ayudas directas, pero sin soslayar las medidas impositivas y tarifarias que ya rigen también desde API, EPE y ASSA, y son de acceso y conocimiento de todos".