---
category: El Campo
date: 2021-04-24T06:00:22-03:00
thumbnail: https://assets.3dnoticias.com.ar/SOJA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La suba de la soja y el maíz podrían generar US$ 10.000 millones extra en
  exportaciones
title: La suba de la soja y el maíz podrían generar US$ 10.000 millones extra en exportaciones
entradilla: La oleaginosa encadenó su novena alza consecutiva y alcanzó los US$ 565,76
  la tonelada, mientras que el maíz cerró la jornada este viernes a US$ 258,06 la
  tonelada.

---
Los precios internacionales de la soja y el maíz cerraron la semana con grandes subas que colocaron a los granos en sus valores más altos desde julio de 2013, lo cual permitiría que el sector alcance este año exportaciones por US$ 33.600 millones, es decir unos US$ 10.000 millones más que en 2020, de acuerdo a las proyecciones del sector.

La oleaginosa encadenó su novena alza consecutiva y alcanzó los US$ 565,76 la tonelada, precio que se ubica US$ 82,5 por encima de la cotización de principio de año y US$ 257,38 más que lo registrado en la misma fecha de 2020.

Por su parte, el maíz cerró la jornada este viernes a US$ 258,06 la tonelada, lo que significó un avance de US$ 67,51 desde el primer día de cotización del año, mientras que en la comparación interanual más que duplicó su precio, al pasar de US$ 125,68 al valor actual.

En base a estos precios, la Bolsa de Comercio de Rosario (BCR) estimó que los principales complejos agroindustriales, con el sojero a la cabeza, podrían realizar exportaciones por US$ 33.613 millones, US$ 10.000 millones más que en 2020.

Esta proyección se encuentra sujeta a que se mantengan los precios actuales, como así también, a que se cumplan las proyecciones de cosecha, que ubican en 50 millones de toneladas a la de maíz y en 45 millones de toneladas a la de soja.

Asimismo, la recaudación estatal vía retenciones para la campaña 2020/21 alcanzaría un total de US$ 8.600 millones, unos US$ 2.600 millones más que en el ciclo anterior, y el mayor monto desde la 2011/12.

**Estimaciones**

Entre los principales factores que explican la suba de ambos cultivos, la entidad puntualizó en "la ajustada hoja de balance que ambos granos presentan a nivel global, pero particularmente en Estados Unidos".

De acuerdo con las estimaciones del Departamento de Agricultura de Estados Unidos, la ratio stock/uso de soja a finales de la corriente campaña en el país norteamericano será de apenas el 3%, muy por debajo del 13% de finales de la campaña anterior, y un mínimo desde la 2013/14.

"Para tomar una noción de lo ajustada de la situación, esto implica que las existencias al final de la campaña serían suficientes para hacer frente a la demanda (suponiendo que ésta se sostiene en el ciclo nuevo) por apenas una semana y media", remarcó la BCR.

En el mismo sentido, se precisó que "algo similar ocurre con el maíz, ya que para la actual campaña se espera que la ratio stock/uso termine en 9%, el más bajo también desde la 2013/14".

El segundo de los factores que impulsó los precios fue la cuestión climática en Estados Unidos y en Brasil.

En el país del hemisferio norte, "una ola de frío ha azotado a la región de las planicies y el cinturón maicero, retrasando marcadamente la siembra de ambos cultivos y la germinación de los lotes ya implantados", mientras que, en Brasil, las condiciones de sequía afectarían a la siembra del maíz de segunda, lo cual genera mayor preocupación acerca de la oferta global del cereal.

El analista de mercados agropecuarios, Carlos Etchepare, sostuvo que "en estos días lo que pasó tiene que ver con la demora en la siembra en Estados Unidos por cuestiones climáticas y la presión de China por seguir comprando, lo que generó una masiva intervención de los fondos especulativos, que generan una demanda artificial del producto".

"Es muy importante en este momento esa participación, porque todo el mundo está pensando que, si fracasa la siembra estadounidense, los precios sigan volando y los fondos están pensando en que puede seguir subiendo el precio", agregó.

Sin embargo, remarcó que "de todas maneas, en algún momento van a tener que salir, porque si no quedan comprados y van a recibir mercadería y eso no les interesa. Cuando salgan van a tener que vender y van a generar una oferta, haciendo bajar el precio y tomando ganancias".

Por su parte, el responsable del Departamento de Análisis de Mercado de la corredora Grassi, Ariel Tejera, previó que "en el corto plazo, es probable que transitemos con un mercado muy volátil, atado a la evolución de las condiciones climáticas de Estados Unidos".

"Hacia mediano plazo, aún si el clima acompaña en el país del norte, es posible pensar en un escenario relativamente favorable para los productos agrícolas, si se mantienen el dinamismo de la demanda de China y la recuperación de la actividad en principales economías", finalizó.