---
layout: Noticia con imagen
author: .
resumen: "Biocombustibles "
category: Estado Real
title: Enerfe y el INTI firmaron un convenio para promover el uso sustentable de
  los biocombustibles en la provincia
entradilla: Se trata de un acuerdo por dos años, destinado a fortalecer el
  tejido productivo de la provincia.
date: 2020-11-18T13:28:57.669Z
thumbnail: https://assets.3dnoticias.com.ar/enerfe-inti.jpg
---
En el marco de la reglamentación del “Programa Provincial de Uso Sustentable de Biocombustibles”, se celebró la firma de un convenio marco entre la empresa provincial de gas y energías renovables, ENERFE, y el Instituto Nacional de Tecnología Industrial (INTI), destinado a fortalecer el tejido productivo de la provincia y la promoción de trabajos de interés común, acceso a instalaciones y actividades conjuntas con mutuos beneficios.

En este sentido, la ministra de Infraestructura, Servicios Públicos y Hábitat, Silvina Frana, celebró el convenio y destacó el trabajo que se viene realizando desde Enerfe desde el inicio de la gestión: “Esto está enmarcado en una agenda de trabajo que se viene desarrollando interministerialmente para generar empleos verdes y que sean amigables con el medioambiente. 

Desde estas políticas públicas buscamos poder disminuir la huella de carbono, y generar herramientas que permitan agregar valor al sistema productivo de la provincia. Y Santa Fe es pionera en materia de biocombustibles, diversificando el uso de la matriz energética y fomentando el uso de las energías renovables”, concluyó Frana.

Por su parte, el senador nacional Roberto Mirabella indicó que **es clave e importante que la provincia de Santa Fe se vaya dando paso a paso una estrategia sobre la bioeconomía,** porque para Santa Fe es clave esa posición dado que hoy es el principal productor de biocombustibles en Argentina, con lo cual si lo pensáramos en términos petroleros, sería la cuarta provincia petrolera en Argentina después de Chubut Neuquén y Mendoza”.

“La capacidad que ha generado tanto en inversión como en generación de empleo y en divisas por las exportaciones que se han generado en todos estos años es clave, con lo cual es un paso más para fortalecer este y para fortalecer todos los aspectos de los sectores de la industria y de nuestras actividades que están ligadas a la bioeconomía, a la biotecnología e ingeniería genética, a los biocombustibles, la producción de alimentos más saludables y más sanos, para la producción de biomateriales”, concluyó Mirabella.

A su turno, el presidente de Enerfe, Juan D´Angelosante, aseguró que **como santafesinos tenemos el orgullo de ser la primera provincia del país en promover el uso de un combustible sustentable, producido por nosotros mismos**: “Estamos en tiempos donde el uso de una energía más limpia es inminente en nuestro país y en todo el mundo”. Y recordó la capacidad productiva que tiene la provincia: “el 80% del biocombustible que se produce en el país, surge de Santa Fe, y este convenio, es un paso adelante para consolidarnos y seguir creciendo en materia de energía renovables”.

Por último, el presidente del INTI, Rubén Geneyro, valoró la posibilidad de trabajar en el proceso de la verificación técnica de la calidad de los combustibles: “Trabajaremos con una provincia que tiene un potencial y una fortaleza enorme en este segmento y para nosotros es estratégico. Siempre hablamos con la ministra Silvina Frana sobre la necesidad imperiosa que tenemos de complementar capacidades y esto me parece que es un paso adicional”. 

Y continuó: “Creo que hay una agenda enorme de trabajo y en esta agenda federal que planteamos, tenemos todas las expectativas y pensamos que el INTI tiene que hacer sus mayores esfuerzos para poner en valor todas las capacidades que tenemos a través de nuestros 46 centros tecnológicos a lo largo y ancho del país, con más de 2000 profesionales y técnicos altamente capacitados. **Debemos profundizar la reactivación productiva, industrializar fuertemente todo el país.** Hacer que la generación de valor en origen sea un eje sustancial para generar empleo de calidad y fortalecer la exportación”.

### **Presentes**

Participaron del acto, el secretario de Desarrollo Económico y Cambio Climático, Jorge Caminos; el vicepresidente de Enerfe, Juan Cesoni; el director operativo del INTI, Marcelo Marzocchini, el gerente de Asistencia Regional, Claudio Verterrey, la sub gerenta de Química y Ambiente del INTI, Adriana Rosso y representantes de CARFE y CARBIO.