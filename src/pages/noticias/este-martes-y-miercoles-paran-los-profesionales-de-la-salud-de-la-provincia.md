---
category: Agenda Ciudadana
date: 2021-01-12T09:39:14Z
thumbnail: https://assets.3dnoticias.com.ar/12121-paro-SIPRUS-covid.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: '3DNoticias - Fuente: UNO Santa Fe'
resumen: Este martes y miércoles paran los profesionales de la salud de la provincia
title: Este martes y miércoles paran los profesionales de la salud de la provincia
entradilla: El 98% de los afiliados a Siprus definieron la medida de fuerza de 48
  horas. Además, harán un paro de 72 horas la semana que viene.

---
En una votación directa, el 98% de los profesionales de la salud afiliados a Siprus (Sindicato de Profesionales Universitarios de la Sanidad) rechazó la propuesta paritaria del gobierno provincial. **Definieron medidas de fuerza de 48 horas para esta semana y de 72 horas para la siguiente.** De esa manera, este martes y miércoles se realizará la medida de fuerza, mientras que el paro de la semana que viene tiene fecha a definir.

«Es un contundente rechazo a una oferta salarial que deja al 80% de los profesionales con aumentos salariales inferiores a la inflación durante el 2020 y con cifras en negro. Muy lejos del reconocimiento que esperábamos al enorme esfuerzo que realizamos durante la crisis sanitaria», plantearon desde el gremio mayoritario.

«Además, quedan afuera de esta última recomposición salarial los trabajadores monotributistas y no se avanza en resolver el resto de los temas como las deudas salariales y de adicionales, los pases a planta y el cambio de escalafón».

«El gobierno provincial sigue guardando la plata en un plazo fijo, mientras ajusta el salario de los trabajadores y no se invierte lo necesario en la salud pública de la población», finalizaron.