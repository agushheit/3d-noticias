---
category: Estado Real
date: 2021-01-08T10:07:42Z
thumbnail: https://assets.3dnoticias.com.ar/080121-Perotti-obras.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Perotti recorrió los trabajos de reacondicionamiento del canal Vila Cululú
  y Cañada Sunchales
title: Perotti recorrió los trabajos de reacondicionamiento del canal Vila Cululú
  y Cañada Sunchales
entradilla: El gobernador Omar Perotti afirmó este jueves que «el Estado tiene que
  proteger el esfuerzo y la riqueza que generan los productores». La inversión de
  la obra hídrica supera los 670 millones de pesos.

---
El gobernador Omar Perotti recorrió este miércoles los trabajos de reacondicionamiento del Canal Vila Cululú y Cañada Sunchales que la provincia lleva a cabo en esa zona del Departamento Castellanos. 

**La obra tiene un avance de ejecución del 90%, una inversión actualizada de $ 670.650.124 y el plazo previsto de finalización es marzo de 2021.**

En la oportunidad, Perotti afirmó que «esta obra ha sido postergada durante muchísimo tiempo, con muchas cosechas perdidas por los daños de cada una de las inundaciones. Si sumamos todas las pérdidas que ha habido durante esos años, tranquilamente se podían haber hecho 10 obras como esta. Por eso, esto es el reconocimiento a ese esfuerzo puesto en poder destrabar aquellos obstáculos».

«El Estado tiene que acompañar a quien produce y tiene que tratar de resguardar que no se pierda riqueza. Por eso, la decisión es terminar estas obras públicas en el menor tiempo posible. En los próximos tres años la provincia va a tener mucho ritmo en el tema hídrico porque hubo mucha demora en las obras y tenemos una necesidad de proteger el esfuerzo y la riqueza que generan los productores», sostuvo Perotti.

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.1em; font-weight: 700">DETALLES DE LA OBRA</span>**

El proyecto contempla el reacondicionamiento del Canal Cañada Sunchales, con una longitud de 49,2 Km, y del Canal Vila Cululú, con una longitud de 53 Km., además del reemplazo de las obras de arte existentes y que no verifican hidráulicamente.

A la fecha se llevan a cabo las tareas de demolición de la estructura existente del puente ferroviario del Ramal A km 142,500 de progresiva 62+608 del canal Vila Cululú.

«El objetivo de demoler este puente es porque se trata de una obstrucción al escurrimiento. El avance de la obra está cercano al 90%, o sea que nos queda por culminar ese 10%. Vamos a reemplazar el puente NCA en demolición y reemplazarlo por otro. Esa sería la otra obra importante que nos queda», detalló el secretario provincial de Recursos Hídricos, Roberto Gioria.

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.1em; font-weight: 700">SOBRE EL OPERATIVO DE VACUNACIÓN CONTRA EL COVID-19</span>**

Durante la recorrida el gobernador también hizo referencia al operativo de vacunación contra el COVID-19 y afirmó que «la provincia de Santa Fe está preparada para que cuando Nación reciba las distintas dosis, poder empezar a vacunar».

«**Llegaron 12 mil dosis más para terminar la primera tanda destinada a todo el personal de salud**. Esta etapa es distinta a las anteriores de la pandemia porque ya iniciamos la vacunación. Por eso tenemos que tener vacaciones con protocolos y cuidados para que todos lleguemos a la vacuna», concluyó Perotti.

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.1em; font-weight: 700">PRESENTES</span>**

De la actividad participaron también el senador por el departamento Castellanos, Alcides Calvo; los secretarios de Recursos Hídricos, Roberto Gioria; y de Empresas y Servicios Públicos, Carlos Maina; y los intendentes de Rafaela, Luis Castellano; y de Sunchales, Gonzalo Toselli; entre otras autoridades.