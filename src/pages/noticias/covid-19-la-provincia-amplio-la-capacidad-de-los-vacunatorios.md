---
category: Estado Real
date: 2021-06-10T07:51:47-03:00
thumbnail: https://assets.3dnoticias.com.ar/vacunacion.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: 'Covid-19: La provincia amplió la capacidad de los vacunatorios'
title: 'Covid-19: La provincia amplió la capacidad de los vacunatorios'
entradilla: "“La llegada de gran número de vacunas nos permitió poder expandirnos
  en un ciento por ciento”, indicó el secretario de Salud Jorge Prieto."

---
El Ministerio de Salud provincial informó este miércoles que fueron ampliados los vacunatorios de la ciudad de Santa Fe, permitiendo llegar a más santafesinos y santafesinas con la vacuna contra el Covid-19.

Luego de realizar una recorrida por el Centro de Especialidades Médicas Ambulatorias de Santa Fe (CEMAFE), de la ciudad capital, el secretario de Salud, Jorge Prieto, explicó que “el ingreso de gran número de dosis a la provincia nos permitió hacer un turnado prolongado de una semana y poder alcanzar el número de inscriptos que tenemos a la fecha con comorbilidades entre los 18 y 59 años, concluyendo con esta población objetivo el fin de semana y comenzar con la población general de menores de 59”.

“Esto nos permitió poder expandirnos en un ciento por ciento en lo que hace a la capacidad operativa de los vacunatorios”, continuó.

Del mismo modo, el funcionario remarcó que “en cuanto a la ciudad de Santa Fe, la Esquina Encendida amplió su capacidad para aplicar 4.500 dosis diarias; en cuanto al Cemafe, pasamos de 4 a 8 vacunatorios permitiendo colocar 600 a 700 vacunas en una jornada; y en el CEF Nº 29 llegamos a una capacidad operativa de 3.000 dosis por día”.

“Contar con una gran cantidad de vacunas a nivel provincial nos permite duplicar la vacunación diaria, es decir que de 20.000 pasamos a 40.000 inoculaciones, lo que nos va a permitir en el menor tiempo posible alcanzar los brazos de todos los santafesinos y santafesinas con esta vacuna tan esperada”, indicó más adelante Prieto.

**ALTA OCUPACIÓN DE CAMAS CRÍTICAS**

Al ser consultado por las restricciones que se están llevando adelante en Santa Fe, Prieto destacó que en “la provincia continúa el gran impacto sanitario, con un porcentaje de ocupación de las camas críticas. Si bien se está observando que el número de casos se ha estabilizado, y en algunas localidades ha descendido, algo importante para tener en cuenta es que el porcentaje de internación es altísimo”.

A su vez, el secretario de Salud, recordó que “el comportamiento biológico que tiene este virus es completamente distinto al del año pasado y con más razón tenemos que extremar los cuidados. Con responsabilidad y solidaridad, si realizamos un acto con el mayor cuidado, es decir ventilación, uso de barbijo, teniendo distanciamiento, no permitiendo los aglomeramientos vamos a poder tener ciertas flexibilizaciones”.

“Hoy contamos con una segunda herramienta que es la vacuna, y en este momento que llevamos el 70% de la población objetivo vacunada, es fundamental cuidarnos”, concluyó Prieto.