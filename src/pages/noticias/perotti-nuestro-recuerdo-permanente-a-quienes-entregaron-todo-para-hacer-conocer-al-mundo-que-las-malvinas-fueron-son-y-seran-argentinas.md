---
category: Estado Real
date: 2021-04-03T06:00:14-03:00
thumbnail: https://assets.3dnoticias.com.ar/MALVINAS.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Perotti: “nuestro recuerdo permanente a quienes entregaron todo para hacer
  conocer al mundo que las Malvinas fueron, son y serán Argentinas”'
title: 'Perotti: “nuestro recuerdo permanente a quienes entregaron todo para hacer
  conocer al mundo que las Malvinas fueron, son y serán Argentinas”'
entradilla: El gobernador encabezó este viernes en Reconquista, el acto en conmemoración
  del Día del Veterano y de los Caídos en la Guerra de Malvinas al cumplirse el 39º
  aniversario de la gesta.

---
El gobernador Omar Perotti, encabezó en la ciudad de Reconquista, el acto en conmemoración del Día del Veterano y de los Caídos en la Guerra de Malvinas al cumplirse el 39º aniversario de la gesta.

Durante la actividad, llevada a cabo en la Plaza San Martín, el mandatario provincial destacó que “lo importante hoy es nuestro reconocimiento a tanto valor y tanta entrega. Tenemos a diario la posibilidad de convivir con nuestros héroes. A San Martín, a Belgrano, a nuestro Brigadier López, los conocemos a través de los relatos, a través de los retratos. Nunca pudimos conocer sus verdaderos rostros, ni sus voces. A nuestros héroes los tenemos aquí, bien cerca, presentes, de cuerpo y alma”.

Además, el gobernador de la provincia reflexionó: “Por eso la importancia de centrar la atención en cada uno de ellos. No dejemos nunca de escucharlos con lo que tienen para decir, no dejemos nunca de honrarlos. Pensemos en lo que sintió cada uno. Puedo imaginar una parte. Me tocó ser soldado movilizado en el Teatro de Operaciones Sur, en 1978, en el Conflicto de Beagle. La incertidumbre, el temor, el miedo y los recuerdos, son pensamientos que permanentemente llegan a la cabeza. Por eso mi reconocimiento profundo a cada uno de ustedes porque, además, les tocó entrar en combate”.

“Cada guerra tiene en la historia su trasfondo, tiene sus salidas, algunas definieron territorios, otras nos hicieron libres y, esta que protagonizaron ustedes, nos permitió el retorno a la democracia. Y ese es otro de los enormes reconocimientos que el pueblo argentino tiene que darles a diario”, agregó Perotti.

“Por eso, no debemos olvidar que están vivos, aquí y en nuestros corazones, que es el mayor y mejor homenaje que el pueblo argentino debe tener. El recuerdo permanente a tanta entrega y tanto valor; compromiso en las gestiones, en los diálogos, y firmeza en cada planteo y persistencia en hacer conocer al mundo que las Malvinas fueron, son y serán argentinas”, finalizó el gobernador.

**“Símbolos de la patria”**

Por su parte, el intendente de Reconquista, Amadeo Vallejos, expresó que “hacer memoria es pensarse, ubicarse, e inscribirse en un caminar como pueblo, como un colectivo múltiple y diverso, que hace su propia historia, para reconocer en la paz un derecho inalienable”.

En su discurso, el intendente expresó a la provincia el agradecimiento por dar a Reconquista la oportunidad de ser la sede del encuentro, y dijo que la presencia de Perotti hace a un “acto verdadero y legítimo”.

Además, recordó que la gesta de Malvinas fue “uno de los hechos más trágicos de la historia argentina, algo que aún nos interpela”, y añadió: “Estamos convencidos que la sociedad argentina, día tras día, debe reconsiderar sus vínculos con los hechos en Malvinas, los excombatientes y sus familias, donde nuevas generaciones deberán asumir deudas con jóvenes del pasado”.

“Hoy elevamos nuestro sentimiento en agradecimiento, respeto, y permanente reconocimiento a quienes caminaron este trayecto de 39 años. Siempre será la memoria la que otorgue sentido al pasado y permita construir dignamente un presente. Hoy los veteranos y los soldados caídos son un símbolo de la patria”, concluyó Vallejos.

En tanto, el presidente del Centro Ex Combatientes de Malvinas del departamento, César Reniero, sostuvo que “pasaron 39 años, pero es como estar allá nuevamente y es ahí donde afloran las emociones en cada uno de nosotros. Si habrá historias que contar todavía”.

Para el veterano de la guerra en el Atlántico Sur, “sabemos que Malvinas es una causa justa, nos pertenecen por historia, por geografía y por derecho. Pero la guerra es un error y un horror. Como veteranos, a lo largo de los años fuimos organizándonos para salir adelante, luchando por el reconocimiento con el firme propósito que Malvinas y sus héroes no queden en el olvido” expresó Reniero.

Por último, el obispo Marín destacó “el espíritu solidario de quienes regresaron de Malvinas, que trabajaron y lograron mantener viva la memoria de lo acontecido”.

Luego del acto, junto al intendente y los ministros provinciales, recorrió el Archivo Histórico Municipal Paseo del Bicentenario donde se inauguró una muestra fotográfica del Archivo de las memorias de Malvinas.

Las fotos allí expuestas son de diversa procedencia, muchas de las cuales son fotos particulares de los protagonistas, así como también fotocopias de diarios y revistas atesoradas por estos.

**Presentes**

Participaron también los ministros de Gestión Pública, Marcos Corach; y de Gobierno, Justicia, Derechos Humanos y Diversidad, Roberto Sukerman; el secretario de Hábitat, Urbanismo y Vuvienda, Amado Zorzón; el secretario de Educación, Víctor Debloc; el subsecretario de Gestión del Riesgo y Protección Civil de la Nación, Gabriel Gasparutti; l jefe de la III Brigada Aérea, Comodoro Plinio Guillermo Poma; el senador provincial por el departamento General Obligado, Orfilio Marcón; y el obispo Diocesano de Reconquista, Monseñor Ángel Marín; entre otras autoridades.