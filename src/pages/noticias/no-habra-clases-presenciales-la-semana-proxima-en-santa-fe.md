---
category: Agenda Ciudadana
date: 2021-05-30T06:15:44-03:00
thumbnail: https://assets.3dnoticias.com.ar/clasesjpg.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'No habrá clases presenciales la semana próxima en Santa Fe '
title: 'No habrá clases presenciales la semana próxima en Santa Fe '
entradilla: Así lo confirmó este sábado, el gobierno de la provincia de Santa Fe.
  Las clases solo serán virtuales "siempre que sea posible", señala la circular.

---
“Ante el agravamiento de los indicadores sanitarios en el territorio provincial, el sistema educativo sostiene su contribución de restricción de circulación para las personas, extendiendo los alcances de la Circular N.º 0012/2021 al período comprendido entre el 31/05 y el 04/06; por tanto, durante esos días permanecerán suspendidas todas las actividades escolares presenciales en el ámbito escolar”. Así lo determina la circular dada a conocer esta noche por el Ministerio de Educación de la provincia de Santa Fe, dando por confirmado que no habrá clases presenciales la semana próxima.

“Las instituciones darán continuidad al vínculo pedagógico en el modo de enseñanza en la distancia implementando los recursos que conocen para ese formato: aulas virtuales (siempre que sea posible), utilización de la telefonía celular para el envío de actividades y propuestas, entrega de materiales impresos y guías de trabajo”, agrega el texto oficial.

Desde el Ministerio se solicita a las escuelas que la semana próxima “entreguen módulos alimentarios, organicen al personal en equipos para hacer propicia esa oportunidad para distribuir materiales pedagógicos”.