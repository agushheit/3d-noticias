---
category: Agenda Ciudadana
date: 2021-08-11T06:15:50-03:00
thumbnail: https://assets.3dnoticias.com.ar/JURAMINISTROS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: El Presidente tomó juramento a los nuevos ministros Taiana y Zabaleta
title: El Presidente tomó juramento a los nuevos ministros Taiana y Zabaleta
entradilla: Jorge Taiana asumió formalmente como ministro de Defensa y Juan Zabaleta
  como titular de Desarrollo Social, tras prestar juramento ante el presidente Alberto
  Fernández en Salón Blanco de la Casa Rosada.

---
Jorge Taiana asumió formalmente como ministro de Defensa y Juan Zabaleta como titular de Desarrollo Social, tras prestar juramento ante el presidente Alberto Fernández en Salón Blanco de la Casa Rosada.

Jorge Taiana y Juan Zabaleta asumieron como ministros de Defensa y de Desarrollo Social, en reemplazo de Agustín Rossi y Daniel Arroyo, respectivamente, tras prestar juramento ante el presidente Alberto Fernández en la Casa de Gobierno.

Rossi Arroyo y renunciaron para dedicarse a sus precandidaturas a senador nacional y a diputado nacional, respectivamente, con miras a las PASO del 12 de septiembre.

Fernández elogió las trayectorias y las personalidades de los dirigentes que dejan sus cargos en el Gobierno nacional.

De Arroyo dijo que "trabajó incansablemente en un momento muy difícil" del país por la pandemia de coronavirus.

En cuanto a Rossi, recordó cuando tempranamente se sumó al Frente para la Victoria.

También agradeció a Rossi su rol "muy importante" al frente de Defensa durante la pandemia de coronavirus; lo llamó "querido amigo" y destacó que "siempre será su amigo".

Hasta la actualidad Taiana -quien fue canciller entre 2005 y 2010 y legislador del Parlasur- venía desempeñándose como senador nacional y titular de la comisión de Relaciones Exteriores de la Cámara alta.

Por su parte, Zabaleta venía ejerciendo como intendente del partido bonaerense de Hurlingham, en el oeste del Gran Buenos Aires.