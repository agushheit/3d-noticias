---
category: Agenda Ciudadana
date: 2021-01-12T09:43:10Z
thumbnail: https://assets.3dnoticias.com.ar/12121-regreso-a-las-aulas.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: '3DNoticias - Fuente: UNO Santa Fe'
resumen: 'En Santa Fe las clases comenzarán con un sistema bimodal: una semana presencial
  y otra virtual'
title: 'En Santa Fe las clases comenzarán con un sistema bimodal: una semana presencial
  y otra virtual'
entradilla: Así lo manifestó la ministra de Educación de la provincia, Adriana Cantero.
  Además, dio como fecha de regreso a las aulas el 17 de febrero, para chicos de séptimo
  grado y quinto y sexto de secundario.

---
Este lunes por la mañana la ministra de Educación de la provincia, Adriana Cantero, brindó precisiones de cómo será el ciclo lectivo 2021 en Santa Fe.

Dijo que será a través de un sistema bimodal, es decir que una semana las clases serán de manera presencial y otra virtual y que esto ya fue presentado ante el Consejo Federal de Educación, que fue aprobado y cuando el ministro (de Educación de la Nación, Nicolás) Trotta visite Santa Fe, vamos a hablar sobre el formato posible y de todas alternativas que tenemos para arrancar el año recuperando presencialidad.

> Cantero: «Se trabajará además con grupos de ocho o nueve chicos, y hasta 15 alumnos por curso, según las dimensiones de las aulas en cada escuela».

La fecha del retorno a las aulas será el 17 de febrero para los grupos prioritarios de séptimo grado y quinto o sexto año del secundario, que deben completar su nivel, el resto comenzará el 15 de marzo.

<br/>

# **Las medidas preventivas por el Covid-19**

Cantero agregó que los estudiantes deberán permanecer con el barbijo colocado mientras permanezcan en los edificios. «Habrá modificaciones en los tiempos. Habrá un tiempo de clases más prolongado que los bloques anteriores, de 40 minutos. Un recreo largo de media hora en el medio para dar tiempo a sanitizar las aulas. Habrá una rutina de lavado de manos y, una vez que vuelvan a los salones, habrá un período de clases más largo», indicó.

Asimismo, señaló que **el horario de clases, en general, será más acotado, porque se quiere evitar que los chicos se trasladen en las horas pico de la utilización del transporte público**, especialmente en ciudades como Rosario y Santa Fe. «El ingreso a la escuela será un poco más tarde, entre las 8 y 8.30 horas. Y la salida podría adelantarse unos 15 o 20 minutos para que los chicos puedan trasladarse antes del horario pico del transporte», añadió.

Al ser consultada sobre el estado de los edificios escolares, algo que fue muy criticado por los gremios docentes, Cantero remarcó: «hemos recibido los edificios en un estado sanitario con muchas deficiencias. Trabajamos durante todo el 2020, y en el receso escolar, en la readecuación sanitaria de todos los edificios. Pusimos todos los recursos al servicio de la infraestructura».