---
category: Agenda Ciudadana
date: 2021-03-28T06:00:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/pensiones.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: El próximo martes se abonarán los haberes a los beneficiarios de las Pensiones
  Sociales Ley 5.110
title: El próximo martes se abonarán los haberes a los beneficiarios de las Pensiones
  Sociales Ley 5.110
entradilla: Así lo informó la provincia y corresponde a los montos del mes en curso.

---
El Ministerio de Economía de la provincia dio a conocer la fecha de pago de las pensiones sociales Ley 5110, expresos por razones políticas (Ley N° 13.298), madres de víctimas del terrorismo de Estado (Ley 13.330), y excombatientes de Malvinas, correspondientes al mes de marzo. Las mismas se abonarán el martes 30 de marzo de 2021.