---
category: Estado Real
date: 2021-10-21T06:00:42-03:00
thumbnail: https://assets.3dnoticias.com.ar/CANTERO.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La provincia llevará una propuesta de calendario escolar a la nueva asamblea
  del Consejo Federal de Educación
title: La provincia llevará una propuesta de calendario escolar a la nueva asamblea
  del Consejo Federal de Educación
entradilla: Será este jueves. El planteo prevé el inicio del ciclo lectivo 2022 para
  el próximo 2 de marzo.

---
Con la presencia de representantes de todas las provincias del país y de la Ciudad Autónoma de Buenos Aires, se concretará este jueves, una nueva Asamblea del Consejo Federal de Educación (CFE).

Del encuentro, que se desarrollará en forma presencial, participará la ministra de Educación de la provincia, Adriana Cantero, quién llevará una propuesta para el debate del consejo.

Al respecto, Cantero señaló que “Santa Fe lleva al encuentro federal de mañana jueves, la propuesta de un ciclo lectivo que inicie para el 2022 el 2 de marzo y finaliza el 20 de diciembre, con una fecha para el receso escolar de invierno del 11 al 24 de Julio”.

Asimismo, la ministra de Educación aseguró que “esperamos que del acuerdo de todos los ministros, se resuelva finalmente cuál será el calendario escolar 2022 para todas y cada una de las provincias”.