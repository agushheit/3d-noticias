---
category: La Ciudad
date: 2021-12-13T01:21:48-03:00
thumbnail: https://assets.3dnoticias.com.ar/leandro.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Diario El Litoral
resumen: 'González: "Se puede generar consenso, y a través del diálogo hay alternativas"'
title: 'González: "Se puede generar consenso, y a través del diálogo hay alternativas"'
entradilla: 'El radical fue reelecto al frente del deliberativo local, por segunda
  vez. '

---
Pasó el jueves 9 de diciembre, los nuevos concejales juraron y ya ocupan su banca de forma oficial en el Concejo Deliberante de la ciudad. La otra novedad que dejó la sesión del pasado jueves, fue que el radical Leandro González (espacio Encuentro) resultó elegido -por unanimidad- como presidente del deliberativo local, y de esta manera el 2022 lo tendrá al frente por tercer año, luego de asumir en 2019.

"Creo que es una ratificación al camino que iniciamos hace dos años cuando me eligieron presidente del Concejo. Tratamos de llevar adelante una impronta de un Concejo abierto, transparente y participativo, que trate de escuchar a las distintas voces que tiene la ciudad, al menos en el plano general", remarcó González en una entrevista con El Litoral.

**-¿Cuáles son tus aspiraciones?**

\-En lo particular busco un diálogo permanente con el Ejecutivo Municipal, para tratar de llevar una agenda del proyecto político, por un lado, y por el otro llegar a acuerdos en los temas legislativos. Considero que en eso hubo una posibilidad de dialogar con todos los bloques, de escuchar y aceptar cambios cuando había miradas propositivas. Ese fue un proceso que se dio en la mayoría de las votaciones que tuvimos durante los dos años y fundamentalmente la semana pasada, con la votación del Presupuesto, Ordenanza Tributaria y extensión del Relleno Sanitario. 

**-¿Cómo analizás la llegada de personas ajenas a la política (Saúl Perman y Juanjo Piedrabuena) a este nuevo recinto?**

\-Lo tomo como algo positivo que eligió la gente. Ante eso no hay que buscar más explicaciones de que la gente en un momento determinado elige candidatos y posa su mirada. Estará después en cada sector político porqué hay una búsqueda por fuera de las principales estructuras o esquemas políticos. Son expresiones que son parte de un momento histórico que se dieron en otros lugares de la provincia y el país.

**-En este nuevo esquema del deliberativo local ¿Habrá que buscar más consenso?**

\-Vamos a estar totalmente abiertos a dialogar con todos los concejales que ingresaron. Particularmente con Perman y Piedrabuena, que por los diálogos que hemos tenido, parecen que son personas con las que se puede dialogar y que pueden hacer un aporte con su mirada para la ciudad también.

**-Respecto a la puja política y el acercamiento con los diferentes bloques ¿Ya empezaron a dialogar?**

\-En mi caso hablé con cada uno de los concejales que asumió el jueves y particularmente con los de Juntos por el Cambio, para generar una relación de confianza que ya tenemos con el resto de los concejales. Tenemos que empezar a pensar en una agenda en común, que por ahí es el desafío político, desde nuestro humilde lugar en la ciudad de Santa Fe buscamos demostrar que se puede generar consenso y que a través del diálogo hay alternativas, no es que todo el mundo quiera pelearse todo el día.

**-¿Hay buenas señales para llegar a ese Concejo que deseas?**

\-Sí, hay señales. Entendemos entre la apertura del Ejecutivo con Emilio (Jatón), lo que nosotros podemos hacer en el Concejo, más el diálogo fluido con los diferentes actores políticos de la ciudad, se pueden obtener buenos resultados.

**-¿Cómo responde el oficialismo municipal al voto de los vecinos en las últimas elecciones?**

\-Para todos los oficialismos era una elección de un nivel de complejidad alto, la pandemia profundizó los conflictos, situaciones de desigualdad, que escapan por ahí a lo que pueda hacer el Estado local. Creo que es una situación que hay que tener en cuenta.

Nosotros apostamos de alguna manera a generar una propuesta desde la política. Tomamos la decisión de formar una lista de unidad, eso quizás en un primer momento nos llevó a una elección de 18 mil votos, pero pudimos crecer mucho para la general y entendemos que hicimos una elección aceptable.

**-¿Se hace un análisis interno de la gestión y las propuestas que están llevando al Concejo?**

\-Hay un foco para profundizar en algunas cuestiones de gestión. En el presupuesto aprobado hay mucho destinado para la gestión urbana, que tiene que ver con la luminaria, el mejoramiento de calles, espacios públicos. Se está poniendo el foco donde sabemos que, sobre todo en el primer año de pandemia, tuvimos más dificultad. Ahora en este semestre hay un Estado municipal de pie, después de arrancar un proceso muy complejo con una deuda importante que se recibió a fines de 2019.