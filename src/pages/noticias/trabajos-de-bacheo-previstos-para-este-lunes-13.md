---
category: La Ciudad
date: 2021-10-04T06:00:53-03:00
thumbnail: https://assets.3dnoticias.com.ar/BACHEOOCT.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Trabajos de bacheo previstos para este lunes
title: Trabajos de bacheo previstos para este lunes
entradilla: El programa se desarrolla por fases, priorizando las zonas de la ciudad
  que evidencian mayor circulación.

---
Actualmente se trabaja en:

* 25 de Mayo y Santiago del Estero
* General Lopez al 3200
* Necochea y Sargento Cabral
* Necochea e Ituzaingó
* Marcial Candioti y Gobernador Candioti

El municipio informa que todas las tareas se realizan siempre en sentido del tránsito. Puede haber cortes de circulación y desvíos de colectivos, a medida que avanzan las obras y se realizan intervenciones mayores. Además, se detalla que los trabajos están sujetos a las condiciones climáticas.

**Operativos de vacunación**

La Municipalidad comunica que, en el marco del operativo de vacunación que se realiza en la ciudad, habrá controles en el tránsito vehicular en las inmediaciones de:

* La Esquina Encendida, en Estanislao Zeballos y Facundo Zuviría, de 7 a 15 horas

Del mismo modo, se recuerda que continúan las tareas en torno al ex Hospital Iturraspe (avenida Perón y bulevar Pellegrini), el Cemafe (Mendoza al 2400) y el Hospital de Niños “Dr. Orlando Alassia” (Mendoza al 4100).