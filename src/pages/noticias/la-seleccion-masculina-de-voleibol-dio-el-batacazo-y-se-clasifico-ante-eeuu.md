---
category: Deportes
date: 2021-08-02T06:15:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/VOLEY.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: La selección masculina de vóleibol dio el batacazo y se clasificó ante EEUU
title: La selección masculina de vóleibol dio el batacazo y se clasificó ante EEUU
entradilla: El equipo dirigido por Marcelo Méndez venció por 3 a 0 a su par de Estados
  Unidos en el cierre del Grupo B de los Juegos Olímpicos y se metió en los cuartos
  de final donde enfrentará a Italia.

---
El seleccionado argentino masculino de vóleibol le ganó un histórico partido a Estados Unidos por 3-0 (25-21, 25-23 y 25-23) y se clasificó a los cuartos de final en los Juegos Olímpicos de Tokio 2020.

El equipo de Marcelo Méndez jugó en altísimo nivel, eliminó a una potencia como Estados Unidos, medalla de bronce en Río 2016 y cuarto del ranking mundial, y el martes jugará por un lugar en las semifinales ante Italia, rival que se definió por sorteo, a las 5.00 hora de Argentina.

Con su tercer triunfo en el Grupo B, Argentina terminó en la tercera posición y se ilusiona con superar la actuación en Río 2016 cuando cayó en cuartos de final ante el anfitrión y luego campeón, Brasil.

Facundo Conte, hijo de Hugo, uno de las medallistas en Seúl 1988, fue el máximo anotador del partido con 16 puntos.

Con una actuación inolvidable, Argentina le ganó una verdadera final a Estados Unidos sin ceder sets y con un nivel colectivo superlativo.

El camino del equipo nacional en estos Juegos empezó con una derrota ante Rusia (3-1) y una dolorosa caída en un partidazo 3-2 contra Brasil -actual campeón olímpico-.

El equipo de Marcelo Méndez se repuso a estos golpes y con autoridad le ganó un muy buen encuentro a Francia por 3-2 y luego sumó su segundo triunfo ante Túnez (3-2) luego de revertir dos sets abajo.

El último escollo de Argentina en el Grupo B, uno de los más difíciles, era Estados Unidos, medalla de bronce en Río 2016, número cuatro del Mundo y subcampeón de la Liga de las Naciones de 2019.

El último antecedente tampoco era favorable para nuestro país ya que en la última Liga de las Naciones, en mayo de este año, fue derrota por 3-1.

Sin embargo, el equipo "albiceleste" jugó un partidazo de punta a punta, dejó en el camino a una potencia y mantiene viva la ilusión de darle la segunda medalla al vóleibol luego de 33 años.

El partido se jugó en el estadio Ariake y empezó una hora después de lo programado, debido al retraso que se generó por el partido entre Japón, que le ganó a Irán por 3-2.

De arranque, Argentina empezó jugando muy bien, apretando con el saque, ordenado en bloqueo y rejuego, y con Luciano DeCecco habilitando a sus atacantes con la calidad de siempre.

El saque fue la primera arma de ataque para el equipo argentino que cerró el primer set con 5 aces.

Desde que tomó la ventaja en el 7-6, la mantuvo hasta el final del set y llegó a estar cinco arriba.

En el segundo set, Argentina pudo mantener ese nivel altísimo y no dejó reaccionar a Estados Unidos.

Una muestra fue que Matt Anderson, uno de los mejores opuestos del mundo, no pudo crecer en el juego y Argentina le forzó varios errores.

Sobre el final del segundo set y con Argentina para ponerse 20-16, el árbitro dominicano Denny Cespedes cobró un inexplicable challengue pedido por Estados Unidos por una jugada de Facundo Conte.

A pesar de la injusta decisión arbitral, Argentina no se cayó, recuperó la ventaja que parcialmente se revirtió a favor Estados Unidos (21-20) y cerró con autoridad el 2-0 con un punto del recién ingresado Martín Ramos.

Como le sucedió ante Brasil, el conjunto de Méndez llegó al tercer set con un ventaja de 2-0 pero a diferencia de lo que pasó contra el campeón olímpico pudo cerrar el partido a su favor.

Con el marcador 11-8 a favor de Argentina, Conte se pegó un golpazo contra los carteles que preocupó al equipo ya que debió atenderse el brazo derecho.

Finalmente, el "Heredero" pudo seguir en cancha y terminó siendo el máximo anotador del partido con 16 puntos, tres más que el sanjuanino Bruno Lima, uno de los mejores jugadores de Argentina en este Juego Olímpico junto con el líbero Santiago Danani.

En las estadísticas finales, Argentina también fue superior ya que se impuso en los puntos de ataque (40 a 38), de bloqueo (8 a 7) y en los servicios (7 a 1). Estados Unidos solo quedó arriba en los puntos por errores (21 a 20)

El seleccionado formó con Luciano De Cecco, Bruno Lima; Sebastián Solé, Agustín Loser; Facundo Conte y Ezequiel Palacios. El líbero fue Santiago Danani. Luego ingresaron Matías Sánchez, Martín Ramos y Cristian Poglajen.

Argentina, que se clasificó tercera en el Grupo B, enfrentará a Italia, segundo del grupo A con cuatro triunfos y una derrota, en cuartos de final.

"Me gustaría dejar afuera a Italia", reveló Conte en declaraciones a TyC Sports luego del partido sobre el equipo europeo que ganó el bronce en Londres y la plata en Rio de Janeiro.

Brasil, defensor del oro, enfrentará a Japón y los otros cruces definidos eran Polonia- Francia y Rusia- Canadá. Todos los partidos se jugarán el martes próximo.