---
category: La Ciudad
date: 2021-12-23T06:15:08-03:00
thumbnail: https://assets.3dnoticias.com.ar/carnes.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: La carne a precios populares se consigue en pocos lugares y en escasa cantidad
title: La carne a precios populares se consigue en pocos lugares y en escasa cantidad
entradilla: 'Los cinco cortes vacunos a precios promocionales provienen de carne de
  exportación y cada supermercado o carnicería de Santa Fe optará por comercializarlos.

'

---
Luego de que el gobierno haya anunciado el acuerdo con los frigoríficos para comercializar cinco cortes de carne vacuna a precios promocionales llegadas las fiestas, aún hay incertidumbre respecto a donde se podrán conseguir. Tal como sucedió anteriormente, los lugares en donde ya está definido que habrá disponibilidad de estos cortes serán los supermercados de cadena nacional.

Este miércoles entró en vigencia el acuerdo de precios del Gobierno con los frigoríficos para ofrecer durante las fiestas cortes de carne a precios populares. La medida se extenderá hasta el 24 de diciembre y la semana próxima del 29 al 31, en la previa de año nuevo.

Al igual que en ocasiones anteriores donde se realizaron este tipo de anuncios, los acuerdos para comercializar cortes cárnicos a bajo precio corresponde a un tipo de carne "de exportación", como puede ser un novillo de más de 400 kg. El bajo precio de estos cortes va en correspondencia con el tipo de carne que abarca el programa, lo cual no fue del todo aclarado al momento del lanzamiento.

Desde la Cámara de Frigoríficos de Santa Fe confirmaron a UNO que cada supermercado o carnicería optará por vender o no la carne vacuna a un precio promocional. Lo describieron como "una decisión comercial", puesto que no será una decisión que se centralice en las cámaras que nuclean tanto a supermercadistas como a carniceros.

Los frigoríficos santafesinos manifestaron que estos cortes ya están siendo ofrecidos a los empresarios, aunque desde los comerciantes minoristas la mayoría optan por no comercializar este tipo de carne a precios bajos debido a que "no rota" en góndola. Al tratarse de cortes vacunos de exportación, provenientes de animales grandes, el argentino y en este caso el santafesino no lo adquiere, puesto que no está acostumbrado a consumir este tipo de carne.

Este medio pudo averiguar que entre los principales supermercados locales solo uno comercializará los cortes de carne a precios populares. En tanto, otra cadena de supermercados santafesina reconoció que al ser poca la cantidad ofrecida para comercializar a estos precios se decidió no formar parte del acuerdo.

Esto pone en jaque a la cadena de comercialización del producto también en el eslabón que corresponde al frigorífico, puesto que todos estos kilos de carne ofrecidos al mercado interno permanecen almacenados en estos establecimientos, inmovilizados cuando podrían ser comercializados al mercado externo que acostumbra a consumir este tipo de producto.

**Cinco cortes a precio promocional**

Los cortes abarcados en el programa serán:

* Vacío a $ 599 por kilo
* Matambre a $ 599 por kilo
* Asado a $ 549 por kilo
* Tapa de asado a $ 499 por kilo
* Falda a $ 399 por kilo.

Cabe destacar que que el acuerdo cuenta con un cupo total de 20 mil toneladas que son los que se distribuyeron en todo el paísy, una vez que se agoten, dejará de estar en vigencia hasta la semana próxima.