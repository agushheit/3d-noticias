---
category: Deportes
date: 2021-08-06T06:00:09-03:00
thumbnail: https://assets.3dnoticias.com.ar/MESSI.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: 'Conmoción en el fútbol: Messi se va del Barcelona'
title: 'Conmoción en el fútbol: Messi se va del Barcelona'
entradilla: El club catalán difundió un comunicado en el que hace hincapié en que,
  "a pesar de haberse llegado a un acuerdo entre ambas partes de firmar un nuevo contrato,
  no se podrá formalizar".

---
Lionel Messi dejará el Barcelona después de 16 años, anunció el club en un comunicado, a raíz de "obstáculos económicos y estructurales" impuestos por la liga española de fútbol para la renovación de su contrato.

El crack tenía todo acordado para firmar un nuevo vínculo por cinco años con el club, pero los requisitos financieros que exige la Liga no pudieron ser resueltos por la dirigencia catalana.

El mejor jugador del mundo y reciente campeón de la Copa América con Argentina quedó libre el pasado 30 de junio y por primera vez en su carrera vestiría la camiseta de otro club.

A través de un comunicado oficial, Barcelona informó que "a pesar de haber llegado a un acuerdo" con Messi para la renovación del contrato, el mismo no se pudo formalizar "debido a obstáculos económicos y estructurales" impuesto por la normativa de LaLiga española.

"Ante esta situación, Lionel Messi no continuará ligado al FC Barcelona. Las dos partes lamentan profundamente que finalmente no se puedan cumplir los deseos tanto del jugador como del Club", agregó el club catalán.

"El Barça quiere agradecer de todo corazón la aportación del jugador al engrandecimiento de la institución y le desea lo mejor en su vida personal y profesional", cerró el breve texto de Barcelona para despedir al máximo ídolo del club.

A veinte días de cumplirse un año del famoso "burofax", la historia de Messi y Barcelona parece que finalmente llegó a su fin después de 16 años como profesional.

En el plano personal, el rosarino jugó 778 partidos, convirtió 672 goles y brindó 305 asistencias.

Además, durante su carrera en Barcelona ganó seis balones de Oro al mejor jugador del Mundo.

En lo colectivo, el "10" conquistó diez ligas de España, siete Copa del Rey, siete Supercopa de España, cuatro Liga de Campeones de Europa, tres Supercopa de Europa y tres Mundial de Clubes.

El rosarino jugó 778 partidos, convirtió 672 goles y brindó 305 asistencias

Cuando parecía que todo se encaminaba al anuncio de un nuevo vínculo, Barcelona confirmó que no podrá cumplir con el acuerdo y anunció la salida del futbolista más influyente de la historia del club.

El presidente Joan Laporta, quien asumió su segundo ciclo a mediados de marzo con la promesa de retener a Messi, no pudo resolver los problemas financieros que el club arrastra desde el inicio de la pandemia y, al menos en principio, dejará ir a la máxima estrella del fútbol internacional.

El futuro de Messi es una incógnita, pero no se descarta que pueda suceder lo mismo que el año pasado cuando finalmente siguió en el club que lo recibió con 13 años desde su Rosario natal.

Con 34 años de edad, Messi tiene la posibilidad de arreglar un vínculo con otra institución por primera vez en su carrera profesional iniciada el 16 de noviembre de 2003.

Las primeras opciones que asoman son las que se manejaron hace casi un año cuando admitió su intención de irse de Barcelona: Paris Saint Germain (Francia) y Manchester City (Inglaterra).

Ambos, por poderío económico, son los únicos que podrían costear el contrato del astro mundial en medio de la puja que se vive en Europa por el "fair play" financiero y el fallido proyecto de la Superliga.

En el equipo francés se reencontraría con su amigo, el brasileño Neymar, con quien se juntó hace unos días mientras estaba de vacaciones en Ibiza.

La foto de Messi y Neymar junto a Leandro Paredes, Ángel Di María y el italiano Marco Verrati, le suma más morbo a la reciente y explosiva noticia.

La otra posibilidad, aunque mínima, es el arribo en la MLS de Estados Unidos, tal como admitió en su momento.

El "10" pasó unos días de sus vacaciones con toda su familia en Miami e incluso se informó que invirtió en varias propiedades, pero no parecería el momento para jugar en una liga que está en crecimiento, pero no está a la altura de Europa.

Messi demostró estar en plenitud futbolística luego de haber coronado con una gran actuación la reciente Copa América conquistada por Argentina en Brasil.

El capitán del seleccionado argentino finalmente pudo celebrar su primer título con la mayor y completó una actuación inolvidable con números impresionantes para su edad.

El rosarino convirtió 4 goles y aportó 5 asistencias en los siete partidos disputados en Brasil.

Además, fue el jugador con más efectividad de pases (81%), más chances creadas (21), el más gambetador (casi el 60%) y el que más remató al arco (28).

Desde el 30 de junio, fecha en la que se venció el último contrato, Messi no se refirió públicamente a su futuro pero uno de los puntos que presagiaban su continuidad fue la contratación de su amigo, Sergio "Kun" Agüero.

Después de 16 años junto con la "albiceleste", la "Pulga" se ilusionaba con la posibilidad de compartir el día a día con el "Kun" pero por ahora esto no se podrá concretar.

El futuro de Messi está en el aire, y a un año del Mundial de Qatar 2022 aun no tiene club definido

El tono utilizado por Barcelona para anunciar la salida de su máxima figura y la responsabilidad centrada en LaLiga española dejan abierta la posibilidad de que pueda haber una marcha atrás.

Por detrás asoma una puja política entre Javier Tebas, el titular de LaLiga, con los tres clubes más importantes de España (Barcelona, Real Madrid y Atlético de Madrid) por el apoyo que le dieron al proyecto de la Superliga europea, que finalmente quedó trunco por las duras advertencias de FIFA y UEFA.

El organismo que controla la Primera y Segunda división del fútbol español anunció en las últimas horas un acuerdo con CVC, un fondo de inversión con experiencia en el ámbito deportivo, que se mete en el mundo del fútbol tras incursiones en el rugby o la Fórmula 1.

Esta operación le representaba al fútbol español una inyección de 2.700 millones de euros pero Real Madrid ya comunicó oficialmente que se opone al proyecto.

Mientras tanto, el futuro de Messi está en el aire y a un año del Mundial de Qatar 2022 no tiene club para seguir desplegando su fútbol.