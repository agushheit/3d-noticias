---
category: La Ciudad
date: 2021-01-14T10:34:04Z
thumbnail: https://assets.3dnoticias.com.ar/concejo-y-farmaceuticos.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: '3DNoticias - Fuente: El Litoral'
resumen: 'Medicamentos: el Concejo Municipal y el Colegio de Farmacéuticos elaboran
  una agenda conjunta'
title: 'Medicamentos: el Concejo Municipal y el Colegio de Farmacéuticos elaboran
  una agenda conjunta'
entradilla: El presidente del Concejo Municipal, Leandro González, mantuvo un primer
  encuentro con integrantes del Colegio de Farmacéuticos.

---
La manipulación de medicamentos y la disposición final de los mismos, más la necesidad de concientizar sobre los peligros de la conducción de vehículos bajo los efectos de medicinas, fueron los principales temas de la primera reunión que mantuvo el presidente del Concejo Municipal, Leandro González, con integrantes del Colegio de Farmacéuticos de Santa Fe.

Con el objetivo compartido de realizar acciones que garanticen el acceso, la calidad y la seguridad de los medicamentos para los vecinos y vecinas de la ciudad, González destacó la importancia de construir una agenda conjunta de trabajo durante 2021.

«Valoramos la predisposición del Colegio de Farmacéuticos para encarar un año de trabajo en conjunto y poder construir una agenda de temas. Es importante cuidar distintos aspectos que hacen a la salud de los santafesinos», expresó el presidente del cuerpo legislativo tras la reunión.

Durante el encuentro con la presidenta del Colegio de Farmacéuticos 1° Circunscripción, Miriam del Pilar Monasterolo, y el gerente de servicios y productos profesionales, Javier Ávila, el compromiso de trabajo entre ambos organismos puso el foco en el rol que tienen los medicamentos y los servicios farmacéuticos para mejorar la calidad de vida de las personas, como aspecto fundamental de la promoción del derecho a la salud, la prevención de enfermedades y el seguimiento de tratamientos.

En este sentido, González precisó que «seguramente vamos a seguir reuniéndonos para tratar algunos aspectos normativos, otros sobre la manipulación y uso de medicamentos, la disposición final de medicamentos vencidos. Sobre este último punto, sabemos que hay proyectos de concejalas y concejales, como Laura Spina y Carlos Suárez, que también tienen inquietudes al respecto. La idea es consolidar un esquema de trabajo entre el Concejo Municipal y el Colegio de Farmacéuticos, en diálogos con el municipio y el intendente Emilio Jatón. Las farmacias son ámbitos esenciales en la pandemia, y necesitan del trabajo articulado de todos los niveles del Estado».

Por su parte, Miriam del Pilar Monasterolo, presidenta de la 1º Circunscripción del Colegio, declaró que «nos parece muy importante haber mantenido este primer diálogo para poder trabajar en conjunto proyectos, en el marco de una agenda común, que son importantes para la población santafesina».

Al mismo tiempo, Monasterolo adelantó que «pensamos desarrollar campañas de concientización sobre medicamentos, farmacias, disposición final de residuos peligrosos o vencidos, sobre diabetes, hipertensión; son algunas de las cosas en las que queremos avanzar e iremos trabajando en conjunto».