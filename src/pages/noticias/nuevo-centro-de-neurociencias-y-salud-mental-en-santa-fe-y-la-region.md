---
category: La Ciudad
date: 2021-11-03T06:15:05-03:00
thumbnail: https://assets.3dnoticias.com.ar/neurosantafe.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Nuevo Centro de Neurociencias y Salud Mental en Santa Fe y la Región
title: Nuevo Centro de Neurociencias y Salud Mental en Santa Fe y la Región
entradilla: |-
  La capital provincial cuenta con un flamante Centro de Neurociencias y Salud Mental.
  El mismo, ubicado en calle Suipacha 2241, es una nueva apuesta del Grupo Santa Fe.

---
El 29 de octubre se inauguró en la ciudad de Santa Fe un nuevo Centro de Neurociencias y Salud Mental. La fecha no fue azarosa. Justo ese día se conmemoró el Día Mundial de Concientización del Accidente Cerebro Vascular, razón por la cual desde Grupo Sana Fe decidieron reafirmar su compromiso de brindar calidad en el cuidado de la salud a la comunidad de Santa Fe y la Región.

 “Las secuelas a nivel psicológico de la pandemia y del estrés tienen hoy una evidencia muy clara. El confinamiento durante meses, los cambios de hábitos y las vivencias de procesos de enfermedad que pueden sufrir las personas propiamente dichas o sus seres queridos han cobrado un valor en los ámbitos de la salud muy importante. En ese sentido, este nuevo Centro de Neurociencias y Salud de Sanatorio Santa Fe viene a dar respuesta a esta problemática tan importante para nosotros hoy”, manifiesta el jefe del Servicio de Neurología del flamante centro, el médico neurólogo Francisco Varela (MN. N° 139048).

**Tecnología de punta y trabajo en equipo, la combinación perfecta**

 En el año 2018 la incorporación tecnológica de un equipamiento de última generación en intervencionismo neurovascular y cardiovascular, único en la región centro–litoral del país -El Azurion 7 M12 de Philips- fue una de las decisiones primordiales del Grupo Santa Fe en post de la calidad y seguridad.

 Días atrás se llevó a cabo la inauguración del Nuevo Centro de Neurociencias y Salud Mental del Grupo Santa Fe.Foto: Manuel Fabatía

 Pero ahora, la novedad es la constitución de un equipo de trabajo en capacitación permanente abarcando diferentes sub especialidades. Ello refleja la filosofía institucional y da respuesta de tres paradigmas centrales para el cuidado de la salud. Ellos son:

 Priorizar la prevención: diagnóstico temprano y tratamiento oportuno.

Continuidad de la Atención en el cuidado de la Salud a través de Equipos de Trabajo organizado en Redes articuladas de complejidades crecientes.

Foco en las enfermedades crónicas no transmisibles. En este caso: enfermedades neurovasculares.

 “Para nosotros, este nuevo centro es un desafío en los tiempos que corren. La medicina ha sufrido un cambio de paradigma: por un lado, la pérdida del rol protagónico que tenía tiempos atrás el médico de familia y, por otro lado, el surgimiento de la atomización de las ciencias en cuanto a la creación de especialidades y subespecialidades. Esto hace que sea sumamente importante la interacción entre diferentes especialidades para poder atender al paciente de manera adecuada y no sólo en el ámbito ambulatorio sino también en el ecosistema de internación y la sala de emergencia”, remarca el neurólogo Francisco Varela. Y agrega: “Esta nueva clínica es un centro no sólo dotado de la tecnología que brinda el Sanatorio Santa Fe sino de especialistas altamente capacitados. Para nosotros el recurso humano es nuestro principal motor o valor que tiene este Grupo que quiere posicionarse como un centro de referencia para las enfermedades neurológicas no sólo en la ciudad sino en toda la región”.

  **Cinco servicios con variedad de especialistas**

El nuevo Centro de Neurociencias y Salud Mental del Grupo Santa Fe ofrece: 

* Servicio de Neurología.
* Servicio de Dolor y cuidados Paliativos.
* Servicio de Patología de Columna Vertebral.
* Servicio de Neurocirugía.
* Servicio de Salud Mental.
*  Dentro de ellos están comprendidas áreas de atención en neurología, neurología cognitiva, neurocirugía, psiquiatría, psicología, centro de columna y dolor lumbar, centro accidente cerebrovascular, emergencias neurológicas, tratamiento del dolor, epilepsia, clínica de toxina botulínica, trastornos de ansiedad, trastornos de pánico, depresión, trastorno bipolar, esquizofrenia y otros trastornos psicóticos. También trastorno obsesivo compulsivo, trastornos conductuales en pacientes con demencia, patologías funcionales (fibromialgia, colon irritable, fatiga crónica) y trastornos de Estrés Postraumático.