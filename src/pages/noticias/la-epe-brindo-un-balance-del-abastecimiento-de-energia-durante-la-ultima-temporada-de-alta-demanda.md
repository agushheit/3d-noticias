---
category: Agenda Ciudadana
date: 2021-04-04T08:10:32-03:00
thumbnail: https://assets.3dnoticias.com.ar/EPE.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La EPE brindó un balance del abastecimiento de energía durante la última
  temporada de alta demanda
title: La EPE brindó un balance del abastecimiento de energía durante la última temporada
  de alta demanda
entradilla: El Presidente del Directorio de la Empresa, Mauricio Caussi, lo calificó
  como positivo, atendiendo el desafío de un contexto extraordinario en función de
  la situación sanitaria.

---
Caussi informó que “se registraron, en este período, niveles récords de abastecimiento de energía, sustentados en la alta demanda por la vigencia de olas de calor especialmente durante el mes de enero”.

Por otra parte, el titular de la empresa dijo que “las instalaciones pudieron transformar y distribuir electricidad en los grandes centros urbanos, que también significaron marcas históricas el día 25 de enero, tanto en la ciudad de Rosario con 639 MW, como en Santa Fe, con 392 MW”.

En ese sentido, reconoció el aporte de los usuarios, que de a poco van incorporando conductas de eficiencia energética en la utilización de los recursos. Asimismo, ponderó la campaña “Cuidar para Tener” que realizó la empresa a través de distintos medios masivos de comunicación, alentando el cuidado de los recursos energéticos.

También destacó el compromiso del personal para resolver cuestiones asociadas al servicio, tanto desde el sector operativo, como el administrativo, y subrayó que como consecuencia de los efectos de la pandemia hubo 48 agentes con Coivd+ y 347 aislados en este período.

Además, subrayó los trabajos de actualización del Plan de Contingencias corporativo, señalando que es la hoja de ruta que tienen las distribuidoras de energía eléctrica para activar correctamente las secuencias operativas en una actividad base para el desarrollo de la vida de las comunidades.

Otra característica de esta temporada fue la periodicidad de tormentas, en algunos casos con fuertes vientos y abundante caída de agua en regiones puntuales de la provincia, que implicaron daños en instalaciones eléctricas, especialmente en las redes áreas de la jurisdicción.

En su evaluación, en base a datos extraídos del Centro de Control de Operaciones de la Gerencia de Explotación, el Presidente de la empresa apuntó que el promedio de cantidad de salidas de servicio y el tiempo de interrupción en el sistema de media tensión disminuyó en el segundo semestre del año pasado en torno al 22 %, respecto al trienio 2017-2019.