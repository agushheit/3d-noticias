---
category: Estado Real
date: 2021-06-24T08:29:40-03:00
thumbnail: https://assets.3dnoticias.com.ar/detectar.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: 'Covid-19: El Plan DetectAR Federal recorrerá siete barrios y localidades
  de la región Santa Fe'
title: 'Covid-19: El Plan DetectAR Federal recorrerá siete barrios y localidades de
  la región Santa Fe'
entradilla: El camión itinerante de Salud de la Nación y equipos provinciales refuerzan
  el testeo territorial, potenciando el de los cuatro puntos fijos de testeo sin turno
  previo.

---
El Ministerio de Salud de la provincia continúa con fuertes operativos de búsqueda activa en territorio de casos de Covid-19 mediante puntos fijos de testeo espontáneo en la ciudad de Santa Fe, al tiempo que vuelve a recorrer barrios y localidades del departamento La Capital, junto al equipo profesional del DetectAR Federal y su camión sanitario.

Para ello, se trasladan puestos de consulta, sin turno previo, a zonas de mayor necesidad y demanda; y, asimismo, el DetectAr Federal recorrerá nuevamente otras 7 áreas y localidades de la región Santa Fe de Salud, desde el 24 de junio hasta el miércoles 30.

Sobre los puntos fijos de detección descentralizados, a los que se puede asistir de modo espontáneo sin turno previo –presentando uno o más síntomas compatibles con Covid-19 o habiendo sido contacto de casos confirmados–, Rodolfo Rosselli, director de la Región de Salud de Santa Fe, detalló que “el ubicado en barrio Centenario se trasladó la semana pasada a la estación Mitre, a unas pocas cuadras de donde estaba antes; mientras que el de la Costanera, desde el viernes próximo, se trasladará al El Alero de barrio Acería, para quedar fijo ahí. No obstante, los ubicados en la ex Estación Belgrano y en El Alero de Coronel Dorrego permanecerán trabajando en esos puntos”.

**Itinerario y cronograma**

Por otro lado, Rosselli informó los lugares y fechas por los que circulará el dispositivo territorial del DetectAr Federal, desde mañana jueves 24 hasta el 30 de junio, de 8 a 14 hs.

>>El jueves 24, en el Aeropuerto de Sauce Viejo

>>El viernes 25, en barrio Villa Hipódromo en  Blas Parera al 5200.

>>El sábado 26, en Santo Tomé, en el club Alianza, avenida Lujan 3260.

>>El domingo 27 en San José del Rincón, en Santa Rosa y Juan de Garay.

>>El lunes 28 en Recreo, en el barrio Mocoví, Lincoln entre Rosario y España.

>>El martes 29 en Monte Vera, en el corsódromo, calle  E. López al 6000.

>>Y el miércoles 30 en Las Flores, en Regimiento 12 de Infantería y Europa.