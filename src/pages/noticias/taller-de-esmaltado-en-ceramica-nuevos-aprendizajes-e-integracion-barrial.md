---
category: La Ciudad
date: 2021-10-26T06:00:29-03:00
thumbnail: https://assets.3dnoticias.com.ar/ESMALTADO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Taller de esmaltado en cerámica: nuevos aprendizajes e integración barrial'
title: 'Taller de esmaltado en cerámica: nuevos aprendizajes e integración barrial'
entradilla: Se trata de una primera experiencia que busca el desarrollo de acciones
  colaborativas y solidarias.

---
Como parte del Plan Integrar, la Municipalidad de Santa Fe, en diálogo con la Regional IV de Educación, comenzó a desarrollar un taller de esmaltado de cerámica en el Jardín Municipal de San Lorenzo. En este espacio, los jóvenes que participan de actividades en la Estación de ese barrio, produjeron las baldosas que formarán parte de la “Vereda para jugar” que se inaugurará el jueves 28 de octubre, a las 11 horas, en Roque Sáenz Peña 6127 (Villa Hipódromo).

En el Jardín Municipal San Lorenzo se recuperó, para el trabajo en talleres, un espacio que estaba destinado a depósito. El proyecto comenzó con la idea de hacer propuestas educativas con ciertos asociativismos entre distintos actores y actrices de cada barrio. La iniciativa contó con la financiación del Fondo de Asistencia Educativa (FAE) y la colaboración de la Dirección de Obras y Asistencia Educativa (DOAE) que concretó las tareas de reacondicionamiento y puesta en valor de este lugar que pertenece al edificio original del Jardín Municipal.

Al respecto, la subsecretaria de Gestión Cultural y Educativa municipal, Huaira Basaber, sostuvo que la iniciativa generó el entusiasmo de los jóvenes de inmediato y explicó que “se trata de una primera experiencia a través de la cual se busca encontrar el interés, el estímulo, para planificar nuevas propuestas para llevar adelante”. También es la primera vez que los jardines empiezan a tener otra función social y comparten zonas y lugares, generando espacios intergeneracionales.

Respecto del financiamiento, Basaber destacó que “a través del FAE, vamos ampliando la capacidad de sostener económicamente, y también ideológica y conceptualmente, proyectos que tienen un aporte expresivo y artístico, y están vinculados al oficio”.

**Identidad costera**

Sobre la propuesta, la funcionaria explicó que “se trata de ofrecer un aprendizaje que está vinculado a la sensibilidad, otra materialidad que es muy particular de nuestra ciudad que tiene una vinculación con la cerámica y el barro”. Y agregó: “Nosotros hablamos del barro como máximo bien cultural que tenemos. Podemos ver que muchos sectores de nuestra sociedad tienen una relación muy singular con este material, ya sea desde el lugar de las artes, del oficio o de la producción en las zonas costeras”.

En este sentido, la iniciativa busca recuperar las matrices culturales de cada barrio, haciendo llegar propuestas de calidad, razón por la cual los jóvenes sostienen estos espacios sin necesidad de que haya que insistir para que participen. Se trabaja desde la idea de las múltiples inteligencias, y en la búsqueda de ser aprendices haciendo.

**Veredas para jugar**

En este caso se está trabajando sobre matrices que ya están diseñadas en el marco del proyecto “Veredas para jugar”. Son rayuelas lingüísticas y visuales que promueven la imaginación y la participación.

La iniciativa, llevada adelante en forma conjunta por las secretarías de Integración Social y de Educación y Cultura de la Municipalidad, tiene que ver con la inscripción del juego en el territorio, que es esencial, es el trabajo vital de todas las infancias. Cabe destacar que, en esta rayuela en particular, estará incluida una baldosa que retoma la idea a la que arribó el año pasado el Consejo de las Infancias sobre la necesidad de “que los adultos tengan la cabeza más en las nubes”.

Esta rayuela destinada a Villa Hipódromo se está construyendo en San Lorenzo. De este modo se unen dos puntos distintos de la ciudad y eso, sostiene Basaber, “nos hace más colaborativos, solidarios, nos permite pensarnos como parte de una red de participación”.

“Es una decisión del intendente que las secretarías trabajemos en conjunto en la construcción de una ciudad mejor, elegida y querida, que es parte del pensamiento del Plan Integrar”, finalizó.