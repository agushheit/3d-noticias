---
category: La Ciudad
date: 2021-01-14T10:28:54Z
thumbnail: https://assets.3dnoticias.com.ar/transporte-interurbano.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: '3DNoticias - Fuente: El Litoral'
resumen: El sector interurbano pide un 50% de aumento en las tarifas
title: El sector interurbano pide un 50% de aumento en las tarifas
entradilla: 'La Asociación de Transporte Automotor de Pasajeros de Santa Fe le solicitó
  a la secretaría de Transporte provincial una actualización en el valor de los boletos. '

---
**Desde que se volvieron a autorizar las líneas interurbanas, la caída de pasajeros es del 80% respecto de marzo de 2020.**

La Asociación de Transporte Automotor de Pasajeros de Santa Fe (ATAP), junto con la federación que aglutina a las empresas con jurisdicción en la ciudad de Rosario y el sur provincial, solicitaron formalmente ante la secretaría de Transporte provincial un aumento del 50% en las tarifas de las líneas de transporte interurbano. El pedido está siendo estudiado por estas horas por las autoridades santafesinas.

¿Los motivos del pedido? Varios: que la última actualización tarifaria se autorizó en julio de 2019; que **el servicio se está prestando un 30% menos de lo normal** (antes de la declaración de la Aspo) y «con una caída de pasajeros respecto de marzo de 2020 de alrededor un 80% en algunos corredores, en otros, un 65-70% de caída. La situación es crítica», le dijo a El Litoral Leandro Solito, presidente de la ATAP.

De autorizarse el pedido de actualización de tarifas, «sería una herramienta que tendería a sincerar los costos», explicó. Pero aquí el paliativo será, en el actual contexto, «que haya un poco más de subsidios, los cuales reflejen la realidad que va a tener ahora nuestra actividad, con muy pocos pasajeros y escasas frecuencias, hasta que el modelo se vaya redimensionando a la nueva normalidad, y para que no sea tan traumática esta transición», expresó el titular

En este sentido, se aguarda el subsidio provincial que estaría siendo remitido el día 19 ó 20 de este mes y, con respecto al de Nación, aún no hay novedades, pero los fondos estarían llegando para cerca del 25 de enero. «A nivel nacional, se está discutiendo si se pueden dar subsidios para todo el país por 3 mil millones de pesos más. Pero en el interior habrá que ver cuánto llega, porque siempre nos vimos desfavorecidos con respecto al Amba», advirtió Solito.

Hoy el sector está sobre una estructura de funcionamiento que, por la pandemia, ya no existe más: es otro modelo. «Entonces, ¿cómo hacemos para ir depurando todo esto? No tenemos recursos para afrontar este nuevo esquema con menos frecuencias de salidas y muy pocos pasajeros. Estuvimos 10 meses sin trabajar y recaudando casi nada», añadió.

<br/>

## **Todas las líneas**

Solito aseguró que **en la ciudad de Santa Fe se activaron todas las líneas interubanas**. Por resolución provincial, «**se permite un 30% de horarios frecuencias y un 80% de viajantes** (un pasajero por asiento)», recordó. Funcionan, con frecuencias reducidas, Tata Rápido, El Norte, Paraná Medio, Laguna Paiva, Nece, Monte Vera, Empresa Gálvez y Coop. Tal, entre otras.

Provincia autorizó el transporte de media y larga distancia dentro de la bota santafesina, y las líneas de larga distancia interjurisdiccionales desde principios de diciembre. «Ahora estamos a la espera de saber qué harán las universidades e institutos superiores respecto de cómo serán las clases presenciales, si es que vuelven, para ver si ponemos más horarios de frecuencias», cerro Solito.