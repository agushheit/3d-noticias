---
category: Agenda Ciudadana
date: 2020-12-18T10:51:51Z
thumbnail: https://assets.3dnoticias.com.ar/1812gobierno.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Diario UNO'
resumen: El gobierno provincial convocó a los gremios para retomar la paritaria
title: El gobierno provincial convocó a los gremios para retomar la paritaria
entradilla: El gobierno santafesino convocó a ATE, UPCN, gremios docentes y de salud
  para retomar las negociaciones tal como se había determinado en septiembre. Las
  reuniones serán este viernes 18.

---
El gobierno provincial a través de la Secretaría de Trabajo, Empleo y Seguridad Social convocó a los gremios de empleados públicos, docentes y de salud a retomar la discusión paritaria, tal cual se había acordado meses atrás.

Según informaron desde ATE y UPCN, los gremios que representan a los trabajadores públicos, la reunión entre las partes se llevará a cabo este viernes 18 de diciembre a las 12 en la Casa de Gobierno. Por su parte, representantes de los gremios docentes de Amsafé y Sadop junto a Amra y Siprus en representación de los trabajadores de la salud, lo harán a partir de las 17.

En lo que refiere a temas que se tratarán están, además de lo salarial, la posibilidad del pago de un suplemento al personal de salud por su trabajo en pandemia, y la presencialidad en las escuelas en el 2021.

Otro reclamo refiere a la liquidación del aguinaldo, buscando que, en la base de cálculo, se incluyan las sumas no remunerativas y no bonificables que habían formado parte de las negociaciones anteriores y por último la posibilidad de la entrega de un bono.