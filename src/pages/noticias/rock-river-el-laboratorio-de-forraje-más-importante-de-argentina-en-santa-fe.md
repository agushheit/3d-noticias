---
layout: Noticia con imagen
author: "Fuente: LT10  /  ROCK RIVER LABORATORY"
resumen: Laboratorio santafesino modelo
category: El Campo
title: Rock River, el laboratorio de forraje más importante de Argentina, en Santa Fe
entradilla: La empresa en fase de incubación está creciendo a un ritmo del 70%,
  aún en tiempos de pandemia. Su especialidad es el área de la nutrición animal.
date: 2020-11-22T14:07:09.570Z
thumbnail: https://assets.3dnoticias.com.ar/campo.jpeg
---
Rock River Laboratory, dedicado al área de nutrición animal, comenzó a gestarse en 2015 y en 2017 ingresó a la incubadora de empresas del Parque Tecnológico Litoral Centro.

"**Estamos en fase de incubación, pero ya establecidos como el laboratorio de forraje más importante de la Argentina. Y por suerte lo tenemos acá, en Santa Fe**", celebra uno de sus fundadores, Leandro Mohamad, por LT10.

El equipo emprendedor está integrado por profesionales relacionados a la salud animal, que específicamente se dedican al análisis nutricional de forrajes y materia fecal bovina utilizando la tecnología indirecta de evaluación “espectroscopia por infrarrojo cercano” (NIRS).

La información generada proporciona datos para lograr una precisa formulación de dietas en animales, lo que se traduce en una utilización de los alimentos más eficiente, obteniendo más carne o leche con menos alimento.

**Pese a la pandemia de coronavirus, Rock River no ha dejado de prosperar.** "Para febrero o marzo llevábamos un crecimiento de 70% con respecto al año anterior de número de muestras recibidas y por suerte hemos seguido creciendo al mismo ritmo. Estamos súper contentos", expresa Mohamad, y agrega que la expectativa "es seguir creciendo".