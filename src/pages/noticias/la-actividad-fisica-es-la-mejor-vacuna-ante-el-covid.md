---
category: Deportes
date: 2021-04-27T07:41:49-03:00
thumbnail: https://assets.3dnoticias.com.ar/Costanera-Oeste.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Mario Barletta
resumen: La actividad física es la mejor vacuna ante el Covid
title: La actividad física es la mejor vacuna ante el Covid
entradilla: El deporte y la actividad física resultaron ser los grandes aliados para
  reforzar el sistema inmunológico y proteger del COVID-19.

---
El deporte y la actividad física - según diversas evidencias científicas - resultaron ser los grandes aliados para reforzar el sistema inmunológico y proteger del COVID-19, así como también, los mayores responsables en la recuperación positiva de quienes padecieron esta enfermedad.

Desde el inicio de la pandemia, los gobiernos nacional y provincial adoptaron una serie de medidas que restringieron la circulación y la realización de actividades en todo el territorio. La actividad física y deportiva no fue la excepción y trajo a consecuencia efectos nocivos para la salud física y psicológica.

Según un informe de la Organización Mundial de la Salud, la realización de ejercicio en forma moderada o intensa, colabora positivamente en la salud y bienestar físico y sugiere que el sedentarismo y la quietud favorecen la propagación de la enfermedad lo que, además, genera un impacto mucho mayor en materia de salud pública ya que la inactividad física está declarada como cuarto factor de riesgo de la mortalidad mundial.

Desde hace un tiempo, se viene trabajando desde diversas instituciones deportivas de Santa Fe en la optimización de las dinámicas de trabajo, la implementación de los protocolos sanitarios y la mejora en los recursos para tener un retorno cuidado a la actividad física. Por ello, celebro que hoy los gimnasios y los clubes sigan abiertos; y me parece vital que se generen iniciativas como el Proyecto de Ley elaborado por los diputados provinciales Alejandro Boscarol y Pablo Farías para que el deporte, el entrenamiento y la actividad física sean considerados un SERVICIO ESENCIAL para la salud. La práctica sistemática y continuada en el tiempo es un claro beneficio para la salud, reduce la ansiedad, fortalece los buenos hábitos, genera calidad de vida y colabora con el desarrollo integral de niños, jóvenes y adultos.

El deporte previene al menos 35 patologías crónicas, puede ayudarnos a sobreponernos y recuperarnos de la pandemia, puede contribuir en el esfuerzo por reconstruir un mundo mejor, más resistente y equitativo. Y también colabora en la promoción de la justicia, el trabajo en equipo, la igualdad, la inclusión y la perseverancia. Es imperioso sostener con recursos diversas estrategias públicas que promocionen la práctica y continuidad en el tiempo del ejercicio deportivo y que reconozcan los beneficios que brindan a la salud.

![](https://assets.3dnoticias.com.ar/WhatsApp Image 2021-04-23 at 15.20.34.jpeg)

![](https://assets.3dnoticias.com.ar/WhatsApp Image 2021-04-23 at 15.29.59.jpeg)