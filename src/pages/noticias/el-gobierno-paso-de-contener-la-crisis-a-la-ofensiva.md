---
category: Agenda Ciudadana
date: 2021-02-24T06:42:26-03:00
thumbnail: https://d7kqr73uznmf6.cloudfront.net/2020-10-07.jpg
layout: Noticia con video
link_del_video: https://youtu.be/es6ZBsgGzDs
author: con información de La Nación
resumen: El Gobierno pasó de contener la crisis a la ofensiva
title: El Gobierno pasó de contener la crisis a la ofensiva
entradilla: 'Alberto Fernández lideró un ataque frontal a la oposición, a los medios,
  y, sobre todo, a la Justicia, furioso por una crisis que absorbió a la Casa Rosada
  y alteró el clima de su estratégico viaje a México '

---
De la noche a la mañana, el Gobierno pasó a la ofensiva. Después de cuatro días de intentar asfixiar el escándalo de las vacunas VIP, Alberto Fernández y su círculo más cercano de asesores dejaron las explicaciones de lado. El Presidente lideró desde México un ataque frontal -a la oposición, a los medios, y, sobre todo, a la Justicia, su principal blanco-, furioso por una crisis que absorbió a la Casa Rosada y alteró el clima de su estratégico viaje a México con el que buscaba reforzar su alianza regional con Andrés Manuel López Obrador.

La furiosa diatriba presidencial en la conferencia de prensa en el Palacio Nacional junto a López Obrador soltó la tapa de una olla a presión. En la comitiva presidencial ya miraban con cierta frustración y ofuscación el enorme despliegue de la crisis en la Argentina, a la cual esperaban -sin éxito- contener con la salida de Ginés González García y la publicación de la lista de vacunados. "Respondemos con acciones", era la línea que bajaba del entorno presidencial. Alberto Fernández se ocupó de responder también con palabras.

La incomodidad en la que quedó encerrado el oficialismo era visible en el lobby del hotel Presidente Intercontinental de México, donde se hospeda la comitiva. Las discusiones sobre el escándalo dejaban de lado las charlas sobre la gira. Algunas voces aún esgrimían la orden del médico presidencial de vacunarse a raíz de los viajes oficiales. Un motivo razonable, eclipsado por los huecos y la ausencia de transparencia en la implementación de los protocolos, y una estrategia de comunicación que resultó, cuando menos, inefectiva, un cóctel que desató una tempestad que la Casa Rosada lleva ya cuatro días intentando capear.

En cuatro días, el Gobierno ofreció un notable giro en su estrategia: pasó de echar a un ministro y de prometer más controles en los protocolos de vacunación a culpar a los medios y cargar contra la Justicia

Hubo contradicciones en el mensaje oficial. Alberto Fernández eludía hablar de más medidas en la conferencia de prensa en México -"Ya dimos respuestas", fue una de sus frases"-, mientras la Casa Rosada preparaba nuevos anuncios en Buenos Aires, y anunciaba el inicio de la inmunización a los docentes junto con un vuelo a China para traer 904.000 dosis de la vacuna Sinopharm. "Estas son las cosas que vamos a hacer", decían en la comitiva.

El Presidente dijo que la vacunación VIP era "reprochable", pero, a la vez, justificó las inmunizaciones a funcionarios. El entorno de Fernández trataba de reforzar el mensaje oficial. Pero fuentes oficiales reconocían que podían llegar a surgir más escándalos en las provincias, de los cuales, anticipaban, no se harían cargo, pese a que desde el primer momento la estrategia de combate a la pandemia del coronavirus se concentró en Balcarce 50. Y hubo críticas a la falta de precisiones en el protocolo de vacunación. "Si se hubiera hecho bien de entrada, esto no pasaba", se escuchó.

La ofensiva mediática de Alberto Fernández, en México, y de su jefe de Gabinete, Santiago Cafiero, en Buenos Aires, terminaban de tejer un notable giro en el mensaje y el tono de una administración que en cuatro días pasó de echar a un ministro y de prometer más controles en los protocolos de vacunación a culpar a los medios y cargar contra la Justicia por investigar las irregularidades. Desde la comitiva dejaban trascender que la ofensiva judicial, que amenazaba con estirar el escándalo, fue la gota que rebasó el vaso.