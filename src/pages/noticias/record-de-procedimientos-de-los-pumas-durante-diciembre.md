---
category: Estado Real
date: 2021-01-03T11:01:26Z
thumbnail: https://assets.3dnoticias.com.ar/03012021-los-pumas.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Récord de procedimientos de «Los Pumas» durante diciembre
title: Récord de procedimientos de «Los Pumas» durante diciembre
entradilla: La Dirección General de Seguridad Rural llevó adelante más de 350 intervenciones,
  en toda la provincia, relacionadas con el abigeato y el decomiso de especies autóctonas,
  entre otras actividades ilegales.

---
Durante el mes de diciembre, efectivos de la Guardia Rural «Los Pumas», dependiente de la Policía de la Provincia de Santa Fe, ejecutaron más de 350 procedimientos positivos, que representan un récord para esta sección.

Esto es el resultado de numerosas intervenciones, largos procesos de investigación, tareas de prevención y patrullaje que concluyeron con 54 procedimientos relacionados al abigeato y terminaron con el secuestro de 196 animales y 14 personas detenidas. Del total, 116 animales vacunos se recuperaron en Calchaquí el pasado 28 de diciembre, producto de varios robos.

Asimismo, se llevaron a cabo diferentes actuaciones ligadas a los delitos contra la propiedad en las que se destacan robos y usurpaciones, las mismas también acabaron con 14 personas detenidas e imputadas, quienes continúan el proceso por las vías judiciales.

En tanto, los recorridos sobre las zonas de islas, rutas y caminos de toda la extensión provincial, terminaron con el decomiso de diversas especies, enmarcadas en las disposiciones vigentes sobre la caza y la pesca en la Ley Provincial 4830; numerosas cantidades de especies ictícolas incautadas a través de la aplicación de la Ley Provincial de Pesca Nº 12212 y 12722; y otro tipo de animales que fueron capturados y devueltos a su hábitat natural. Además, también se suma el decomiso de leña en la aplicación a la ley Nacional 13.373.

Sobre estas legislaciones, «Los pumas» ejecutaron más de 100 procedimientos por la ley provincial de caza y pesca 4830, que alcanzaron un total de más de 2100 especies decomisadas. Sumado a ello, por la ley de decomiso de leña, se incautaron más de 70 mil kg de leña y 41000 kg equivalentes a vigas de dicho material, y secuestraron 9 especies, las mismas devueltas a su hábitat luego de recibir el tratamiento veterinario correspondiente, de acuerdo a la Ley Nacional 22.421 de conservación de la fauna, que terminó con 3 personas imputadas.

En relación con el secuestro de carne vacuna, concertaron el decomiso de más de 460 kg de carne vacuna y 3 medias reses, además de 145 kg de carne porcina, todo esto producto de 132 procedimientos.

Con respecto al secuestro de armas de fuego, a partir de 15 intervenciones, «Los Pumas» incautaron 15 armas largas, 11 personas imputadas por los hechos, y también 9 vehículos secuestrados.

  
 