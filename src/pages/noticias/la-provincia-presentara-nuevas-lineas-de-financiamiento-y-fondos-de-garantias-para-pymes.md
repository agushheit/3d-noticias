---
category: Estado Real
date: 2021-06-24T08:32:15-03:00
thumbnail: https://assets.3dnoticias.com.ar/pymes.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: La provincia presentará nuevas líneas de financiamiento y fondos de garantías
  para pymes
title: La provincia presentará nuevas líneas de financiamiento y fondos de garantías
  para pymes
entradilla: La actividad será encabezada por el gobernador Omar Perotti, este jueves
  desde las 10, y contará con la participación del Secretario de la Pequeña y Mediana
  Empresa y los Emprendedores de la Nación, Guillermo Merediz.

---
El gobernador Omar Perotti, junto con los ministros de Economía, Walter Agosto; y de Ciencia, Tecnología e Innovación, Daniel Costamagna, anunciará este jueves, nuevas líneas de financiamiento y fondos de garantía para pymes.

La actividad se llevará a cabo a las 10 y contará con la participación del secretario para la Pequeña y Mediana Empresa y Emprendedores de la Nación, Guillermo Merediz; el presidente y vocal de Garantizar SGR, Gabriel González y Guillermo Moretti; y representantes del Nuevo Banco de Santa Fe (NBSF).

En la oportunidad, se presentarán las siguientes líneas de financiamiento y fondos de garantía:

* Línea de Desarrollo Federal para Inversiones con NBSF y el Ministerio de Desarrollo Productivo por $400.000.000. Se otorgará un monto máximo por pyme de $20.000.000 con un plazo de crédito hasta 61 meses, incluido el período de gracia de 6 meses, sistema de amortización francés y una tasa de referencia del 30%. El destino de los fondos es exclusivamente inversión productiva.
* Firma de una adenda convenio con Garantizar SGR para línea Bares y Restaurant, para ampliar la cobertura de las garantías a otorgar por este fondo. A través de esta línea, las micro, pequeñas y medianas empresas, así como monotributistas y responsables inscriptos de bares y restaurantes, podrán acceder a créditos de inversión destinados al acondicionamiento, calefacción, ventilación y cerramientos del espacio exterior para que puedan adecuar sus servicios a los protocolos vigentes.

La asistencia, por medio de un fondo de $1.000 millones, permite el acceso a préstamos de hasta $3.000.000, con plazos de financiación de 36 meses con 12 de gracia, a una tasa bonificada por el FONDEP, lo que resulta en una tasa del 0% para el período de gracia y 10% para los restantes 24 meses. Se monetiza a través del Banco Nación.

* Ampliación del Fondo Específico en Garantizar por $1.000 millones, lo que permitirá ofrecer garantías por $4.000 millones destinados a sectores industriales, economías regionales y servicios ligados a la producción, con líneas que se moneticen tanto por bancos públicos como privados.


* Segunda etapa del Programa Caminos de la Ruralidad por $1.000 millones. Mediante dicho programa, la provincia otorga aportes no reintegrables a municipios y comunas para la compra de materiales para el ripiado de caminos rurales de calzada natural, donde éstos deberán aportar el equipamiento y la mano de obra para llevar adelante la ejecución.

La primera etapa fue de $100 millones, que llegaron a seis localidades de los departamentos Castellanos, Las Colonias y San Jerónimo por 62 kilómetros que alcanza a 200 productores y 8 escuelas rurales.

En ese marco, se firmó un convenio con el Ministerio de Agricultura, Ganadería y Pesca de la Nación por $60.000.000, que alcanzará a 18,2 kilómetros. Esta ampliación de $1.000 millones permitirá la intervención en más 400 kilómetros de caminos rurales de toda la provincia.