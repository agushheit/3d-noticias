---
category: La Ciudad
date: 2021-06-29T08:50:47-03:00
thumbnail: https://assets.3dnoticias.com.ar/barVACIO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: LT10
resumen: Gastronómicos piden que los habiliten trabajar hasta las 23hs
title: Gastronómicos piden que los habiliten trabajar hasta las 23hs
entradilla: 'El sector asegura que no puede soportar más las restricciones horarias
  que los obliga a cerrar a las 20hs. Sostienen que en Capital Federal, Buenos Aires
  y Entre Ríos, rige este horario.  '

---
La asociación que nuclea a los empresarios gastronómicos le están solicitando al gobierno de forma desesperada que los habiliten trabajar hasta las 23hs.  Aseguran que muchos negocios trabajan exclusivamente de noche y que si los siguen obligando a cerrar a las 20hs muchos negocios van a terminar bajando las persianas.

Además, argumentan que en provincias vecinas como Buenos Aires, en la Ciudad de Buenos Aires y en Entre Ríos los bares y restaurantes están habilitados hasta las 23hs.   

Rodolfo Verde, dirigente gastronómico y hotelero manifestó que “hay mucha gente que necesita recrearse y con todos los protocolos, los bares y restaurantes son seguros. Necesitamos trabajar para pagar todos los costos que tenemos y no quebrar”.

Además, el empresario sostuvo que este martes tendrán una reunión con el ministro de Trabajo Juan Manuel Pusineri, donde se le realizará el pedido formal para que se habilite esta extensión horaria lo más rápido posible.

![](https://assets.3dnoticias.com.ar/bares.jpeg)