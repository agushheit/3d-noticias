---
category: Agenda Ciudadana
date: 2022-01-15T06:00:54-03:00
thumbnail: https://assets.3dnoticias.com.ar/reunionfmi.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: El Gobierno y la oposición se reunirán el martes por el FMI, pero todavía
  discuten el lugar
title: El Gobierno y la oposición se reunirán el martes por el FMI, pero todavía discuten
  el lugar
entradilla: " En la Casa Rosada quieren que el encuentro se lleve a cabo en el Ministerio
  de Economía. No se prevé por el momento que participe el Presidente. \n"

---
El Gobierno y los jefes parlamentarios y gobernadores de Juntos por el Cambio empezaron a acercar posiciones para concretar la postergada reunión por la negociación con el FMI, con una fecha por el momento definida y dudas sobre si será en el Ministerio de Economía o en el Congreso.

Fuentes calificadas de la Casa Rosada señalaron que el encuentro se llevaría a cabo el martes 18 de enero y allegados a los gobernadores y a los jefes de bloque de Juntos por el Cambio confirmaron que tienen agendada la misma fecha "sin ninguna noticia oficial en contrario".

De esta manera, luego de varios días de idas, con muchas dudas y versiones encontradas, ambas partes llegaron a un punto de coincidencia, pero todavía resta que definan el lugar en el que se llevará a cabo la reunión.

"Al pedido de la oposición de reunirse con (el ministro de Economía, Martín) Guzmán, la respuesta es 'sí, por supuesto'. Vengan al salón Belgrano del Ministerio'", respondió una de las fuentes gubernamentales consultadas.

El Gobierno puso como condición que los gobernadores Gerardo Morales (Jujuy), Gustavo Valdés (Corrientes) y Rodolfo Suárez (Mendoza), junto a los jefes de bloques del Congreso Mario Negri, Alfredo Cornejo, Cristian Ritondo, Luis Naidenoff y Humberto Schiavoni -entre otros que podrían sumarse- se haga en el Palacio de Hacienda.

A la vez, descartan por el momento que participe el presidente Alberto Fernández, que sí estuvo en la primera reunión convocada, a la que asistieron una decena gobernadores y el resto estuvo representado por emisarios, mientras que el único ausente fue el jefe de Gobierno porteño, Horacio Rodríguez Larreta, que a diferencia de los radicales no envió representantes.

La pelota quedó del lado de la oposición, que esperaba en un principio que la reunión se realizara en el Congreso, dado que las gestiones después de la postergación habían quedado en manos del presidente de la Cámara de Diputados, Sergio Massa, con Morales como interlocutor de Juntos por el Cambio y Guzmán del lado del Ejecutivo.

No obstante, mientras en la Casa Rosada advierten que si el encuentro no se hace en el Ministerio de Economía probablemente no haya encuentro, fuentes parlamentarias de la oposición deslizaron que, en principio, el lugar les daba lo mismo, por lo que la puntada final estaría por darse.

De todo modos, en las últimas semanas el clima de la relación entre el oficialismo y la oposición, especialmente en temas económicos, estuvo bastante caliente y cualquier detalle podría regresar todo a foja cero.

En Juntos por el Cambio hay visiones distintas sobre cuál debería ser la postura ante la negociación que el presidente Alberto Fernández encara con el FMI, aunque ya coincidieron en advertir que le pedirán a Guzmán más precisiones que las que brindó en la reunión con el resto de los gobernadores.

La mesa nacional de la coalición opositora consideró insuficiente la información proporcionada por el ministro de Economía y reclamó la presentación de un plan concreto, además de "una carta de intención" con el Fondo Monetario que pase por el Congreso.

Los diputados nacionales del Frente de Todos, encabezados por Máximo Kirchner, había respondido con enojo a este reclamo y remarcaron que el préstamo de 45 mil millones de dólares que contrajo el gobierno de Mauricio Macri con el FMI no pasó por el Congreso.

En el medio, Morales expresó públicamente que la deuda con el Fondo Monetario la contrajo la gestión de Macri y que por ello correspondía que la oposición fuera al menos "a escuchar" a Guzmán, lo cual abrió otro debate interno en la coalición opositora.

A ello se le sumaron las críticas de referentes nacionales de Juntos por el Cambio a la política económica luego de conocer el último dato de inflación, que mostró un acumulado de 50,9% para 2021, lo que volvió a poner presión sobre el debate en torno a la negociación con el FMI y las condiciones para un acuerdo.

Por el momento, hay coincidencia sobre la fecha para la reunión informativa, que además coincidiría con la visita del canciller Santiago Cafiero a Estados Unidos para mantener un encuentro con el secretario del Departamento de Estado, Antony Blinken, en el que la deuda será el tema principal.