---
category: Agenda Ciudadana
date: 2021-10-16T06:00:57-03:00
thumbnail: https://assets.3dnoticias.com.ar/FMI.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: El Gobierno ratificó ante inversores la voluntad de lograr un acuerdo con
  el FMI
title: El Gobierno ratificó ante inversores la voluntad de lograr un acuerdo con el
  FMI
entradilla: Ante más de una veintena de inversores y empresarios en Estados Unidos,
  el jefe de Gabinete y el ministro de Economía transmitieron precisiones acerca del
  rumbo político y la hoja de ruta de la economía.

---
El Gobierno ratificó ante empresarios e inversores de compañías estadounidenses su voluntad de alcanzar un "acuerdo positivo" con el Fondo Monetario Internacional (FMI), en una reunión donde se presentaron los principales lineamientos y perspectivas de la política y la economía argentina y se dialogó sobre los avances en la negociación con el organismo, se informó oficialmente.  
  
En el encuentro realizado en la sede del Consulado argentino en la ciudad de Nueva York, el jefe de Gabinete, Juan Manzur, afirmó que "un acuerdo positivo con el FMI es una prioridad nacional que involucra a todos los sectores de la sociedad argentina”.  
  
Ante una veintena de inversores y empresarios, Manzur y el ministro de Economía, Martín Guzmán, transmitieron precisiones acerca del rumbo político y la hoja de ruta de la economía, sobre la cual destacaron que el presidente Alberto Fernández "encomendó la tarea de, desde el diálogo, generar los entendimientos que sean necesarios para que la Argentina se recupere de forma sostenible”.  
  
Durante la reunión, en la que también estuvo presente el embajador en los Estados Unidos, Jorge Argüello, Manzur ratificó la voluntad argentina de honrar la deuda y afirmó que "esto tiene consenso adentro del Frente de Todos" y dijo que la intención del Gobierno nacional es "llevarlo al Congreso y tener el apoyo de todos los sectores políticos para que sea aprobado allí".  
  
El jefe de Gabinete sostuvo también que se busca "cerrar el mejor acuerdo posible, que sea sustentable y permita al país continuar en la senda de recuperación, que no impida y por el contrario acompañe el desarrollo"; e invitó a los inversores a la Argentina para concretar reuniones con el sector privado nacional y con las máximas autoridades políticas de Gobierno.  
  
Por su parte, Guzmán destacó la importancia de "fortalecer la sostenibilidad de la deuda, tanto desde el punto de vista macroeconómico como desde el político" y sostuvo que es necesario que el Congreso tenga más presencia en las decisiones de endeudamiento en moneda extranjera "para que la estabilización macroeconómica sea política de Estado y eso fortalezca a la Argentina".  
  
Además, el ministro de Economía manifestó que se apunta a alcanzar un programa que "incluya condiciones que establezcan un camino consistente con la sostenibilidad de los vencimientos de deuda”.  
  
También resaltó el trabajo realizado en la reestructuración de la deuda con los acreedores privados y precisó que permitió bajar el pago de intereses "del 3,4% del producto a 1,5%, lo cual construye el espacio para que podamos avanzar en el esquema de políticas públicas para tranquilizar la economía".  
  
Tras sus exposiciones, los funcionarios del Gobierno nacional respondieron preguntas de los empresarios vinculadas a la situación de la economía argentina y a las previsiones para el año próximo.  
  
Posteriormente, al realizar un balance del encuentro, Manzur afirmó que se trató de "un encuentro extremadamente productivo en el cual se manifestó puntual y centralmente la voluntad y decisión política del gobierno argentino de acordar con el Fondo Monetario Internacional, obviamente en los términos que esto no implique obstruir el desarrollo de la Argentina".  
  
"Recibimos una serie de consultas, preguntas y comentarios, todos respondidos; y el ministro Guzmán explicó en detalle cuáles son los pasos que se están dando, los avances que se vienen produciendo en el marco de un acuerdo sostenible, viable y por sobre todas las cosas sustentable de mediano y largo plazo", completó.  
  
Por su parte, Guzmán también calificó a la reunión como "muy productiva" y recordó que "en 2016, bajo el gobierno anterior, buena parte de quienes hoy estaban presentes empezaron a invertir en la Argentina con una expectativa de un sendero que luego colapsó y eso generó problemas de credibilidad muy fuertes producto del modelo económico fallido anterior y hay heridas que ir sanando".  
  
"Nuestro gobierno está conduciendo un proceso de fortalecimiento de la República Argentina como Estado-Nación, sobre la base de diálogo entre todos los sectores políticos y económicos, está transitando ese camino que permita terminar en una Argentina más fuerte con más trabajo, estabilidad y dinamismo productivo", añadió.  
  
A su turno, Argüello señaló que con esta reunión culmina una "semana de trabajo intenso con interlocutores de toda naturaleza con el objetivo de generar las condiciones políticas y económicas propicias para lograr un acuerdo positivo con el Fondo Monetario Internacional".  
  
Argüello también consideró "muy importante" la presencia del jefe de Gabinete en esta reunión de trabajo donde "se han ventilado todas las cuestiones técnicas y políticas que muestran claramente la decisión de un gobierno completo de avanzar en un acuerdo sostenible y sustentable con el Fondo Monetario Internacional".  
  
Por el sector de los inversores participaron Matías Silvani (GoldenTree Asset Management); Alberto Ades (NWI Management LP); Soledad López (Morgan Stanley); Ruben Kliksberg (Redwood Capital Management); Claudia Castro (Invesco US); Luis Simon (Goldman Sachs Asset Management); Martin Marron (JPMorgan); Gerardo Bernaldez (CarVal Investors); Gustavo Ferraro (Gramercy Management Company); Darío Lizzano (PointState Argentum); Fernando Grisales (Schroeders); Pablo Golberg (BlackRock); Gustavo Palazzi (VR Capital Group); Diego Ferro (M2M Capital); y Andrés Lederman (Fintech).