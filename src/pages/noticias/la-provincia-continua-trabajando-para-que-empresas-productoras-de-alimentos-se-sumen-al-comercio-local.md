---
category: Estado Real
date: 2021-08-22T06:00:44-03:00
thumbnail: https://assets.3dnoticias.com.ar/LOCAL.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La provincia continúa trabajando para que empresas productoras de alimentos
  se sumen al comercio local
title: La provincia continúa trabajando para que empresas productoras de alimentos
  se sumen al comercio local
entradilla: Funcionarios del ministerio de Producción, Ciencia y Tecnología recorrieron
  comercios en Rosario para dialogar y conocer la realidad del sector y potenciar
  la presencia de producción santafesina en góndola.

---
El Ministerio de Producción, Ciencia y Tecnología a través de la articulación de las secretarías de Comercio Interior y Servicios y de Agroalimentos lleva adelante un trabajo conjunto para desarrollar acciones en la industria de alimentos, como los sectores lácteos, de carnes y harinas, entre otros rubros de gran importancia para la producción santafesina.

En el marco de estos trabajos, el secretario de Comercio Interior, Juan Marcos Aviano visitó almacenes y autoservicios en Rosario con el objetivo de conocer los productos cárnicos que ofrecen los mismos y dialogar sobre los alcances del programa Billetera Santa Fe.

Informando sobre la actividad, Aviano indicó: “En este caso hemos visitado almacenes de la ciudad de Rosario que ya tienen exhibidos con góndolas verticales cortes de carne envasados al vacío de una empresa que visitamos hace tiempo, como es Agroganadero Don Atilio de la ciudad de Carcarañá. Firma que está empezando a incursionar en la venta directa al público a través de exhibidoras. Esta iniciativa tiene una integración muy interesante, ya que son exhibidoras compradas a la firma Inelro, una industria santafesina de la línea blanca que está en Salto Grande”.

Asimismo, el funcionario destacó: “Hay 40 almacenes de la ciudad de Rosario que quieren contar con estos cortes envasados al vacío para ofrecerles a sus clientes. Desde la Secretaría tenemos una visión integral de trabajo, queremos que el consumidor tenga diversas opciones, que exista mayor competencia y más oferta. Entendemos que es lo que se viene, lo que tenemos que articular y potenciar desde el Gobierno Provincial para acompañar al comercio, a la industria frigorífica y a todo el sector de las carnes”.

“A esto debemos sumarle la integración de Billetera Santa Fe, ya que estos comercios tienen la aplicación. Nos hablaban del incremento de sus ventas, Todo esto da muestra del enorme potencial que tenemos en la provincia, que estamos recorriendo de punta a punta”, concluyó el funcionario.