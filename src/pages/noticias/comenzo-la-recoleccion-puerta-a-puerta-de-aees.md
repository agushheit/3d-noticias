---
category: La Ciudad
date: 2021-06-12T06:00:42-03:00
thumbnail: https://assets.3dnoticias.com.ar/AEES.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Comenzó la recolección puerta a puerta de AEES
title: Comenzó la recolección puerta a puerta de AEES
entradilla: 'Se trata de la tercera edición de la campaña de la Municipalidad para
  recuperar Aparatos Electrónicos y Eléctricos en Desuso (AEES) en el Distrito Centro. '

---
Debido a la gran demanda, el lunes y el miércoles continuará la búsqueda casa por casa. Se inscribieron, previamente, 320 vecinas y vecinos. Se estima que se recolectarán unos 3 mil kilos.

La Municipalidad realizó hoy la primera jornada de recolección puerta a puerta de Aparatos Electrónicos y Eléctricos en Desuso (AEES) en el Distrito Centro. Esta mañana, el intendente Emilio Jatón fue hasta la explanada de la Estación Belgrano, uno de los puntos de encuentro donde las camionetas del municipio cargaban los equipos recolectados en un camión que luego los llevará a la Planta de Aparatos Eléctricos y Electrónicos. Otro de los camiones está emplazado en la sede del Instituto Municipal de Salud Animal (Imusa) ubicado en Obispo Gelabert 3691. La recolección puerta a puerta continuará el lunes y el miércoles.

“Esta iniciativa tiene que ver con todo un sistema de economía circular, con los Objetivos de Desarrollos Sostenibles (ODS)”, indicó el intendente Jatón y repasó que años atrás “todos estos aparatos iban al relleno sanitario y generaban una gran contaminación, porque eran enterrados sin ningún tipo de tratamiento”.

En consonancia, el mandatario señaló que tampoco se tenían en cuenta los elementos que se podían reciclar o vender. “Ahora todo eso funciona en una planta con la que cuenta la ciudad de Santa Fe, donde hacemos todo ese proceso, cumplimos con los ODS y evitamos la contaminación”, destacó Jatón.

Cabe señalar que los elementos recibidos son llevados a la flamante Planta de Aparatos Eléctricos y Electrónicos que fue puesta en funcionamiento este año por el municipio en el Complejo Ambiental como parte de las acciones desarrolladas para avanzar en la gestión integral de los residuos sólidos urbanos.

Por su parte, Edgardo Seguro, secretario de Ambiente de la Municipalidad, brindó detalles con respecto a cómo continuará la tercera edición de la campaña, que debido a la pandemia se realiza puerta a puerta. “Previamente se inscribieron online 320 personas y a partir de hoy comenzamos a retirar los insumos por los domicilios. Y continuaremos el lunes y el miércoles ya que el volumen es mayor al esperado. Estimamos que durante estos tres días superaremos los 3 mil kilos”, indicó el funcionario.

**Reutilizar**

Con respecto al proceso de reutilización de los equipos, Seguro consignó: “En general los vecinos nos informan cuáles son los equipos que funcionan. De todas maneras, en la Planta se hace una revisión y aquellos que andan correctamente se ponen a disposición de organizaciones civiles que no tengan acceso a esos insumos”.

“Esa es una forma de reintroducir un equipo, de que sigan teniendo un uso. De eso se trata la economía circular: la primera etapa es reusarlo, la segunda es repararlo”, indicó Seguro y añadió que “estamos haciendo un banco de repuestos para arreglar y reutilizar los equipos que llegan y aquellos que ya no se puedan usar más se desarman y todos sus componentes se ponen a disposición de Dignidad y Vida Sana, la asociación que trabaja en el Relleno Sanitario, para su venta”.