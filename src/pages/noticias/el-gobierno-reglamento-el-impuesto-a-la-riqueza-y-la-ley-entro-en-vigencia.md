---
category: Agenda Ciudadana
date: 2021-01-29T10:14:34Z
thumbnail: https://assets.3dnoticias.com.ar/afip.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Con información de Infobae
resumen: El Gobierno reglamentó el impuesto a la riqueza y la ley entró en vigencia
title: El Gobierno reglamentó el impuesto a la riqueza y la ley entró en vigencia
entradilla: Esperan recaudar $300.000 millones. Prevé el cobro por única vez de una
  tasa de entre el 2 y el 3,5% a los patrimonios de las personas físicas que hayan
  declarado más de $200 millones

---
El Gobierno reglamentó hoy el impuesto a la riqueza a través del **Decreto 2021/42** publicado en el **Boletín Oficial.** **Se trata de la Ley 27.605, que establece la creación de un “Aporte Solidario y Extraordinario para Ayudar a Morigerar los Efectos de la Pandemia”**

La norma fue sancionada el 4 de diciembre pasado; y su entrada en vigencia [se oficializó el 18 de diciembre mediante la publicación del Decreto 1024/2020 en el Boletín Oficial](https://www.infobae.com/economia/2020/12/18/el-gobierno-promulgo-el-impuesto-a-la-riqueza/), firmado por el presidente **Alberto Fernández**; el jefe de Gabinete, **Santiago Cafiero**; y el ministro de Economía, **Martín Guzmán**.

Cabe recordar que la ley prevé el cobro por única vez de **una tasa de entre el 2 y el 3,5 por ciento a los patrimonios de las personas físicas que hayan declarado más de 200 millones de pesos, lo que abarcará a 12 mil contribuyentes.**

El impuesto, que finalmente ahora entrará en vigencia, generó críticas desde diversos sectores. **Empresarios, industriales, el campo y dirigentes opositores hicieron públicos sin cuestionamientos**. De hecho sólo tres de doce países europeos que aplicaron el impuesto a la riqueza en las últimas décadas lo mantienen y en solo uno de ellos, **Suiza, el tributo recauda una porción significativa (1,1%) del PBI**.

    https://drive.google.com/file/d/14-vrnG1-Tgqonlfv5_ep2AWcMrhgWrbr/view

**Qué dice el decreto**

El **artículo 1** del decreto afirma que a los fines de la valuación de las acciones o participaciones en el capital de las sociedades regidas por la **Ley General de Sociedades**, los sujetos mencionados en el artículo 2° de la **Ley de Aporte Solidario y Extraordinario** podrán optar por considerar: la diferencia entre el activo y el pasivo de la sociedad correspondiente al 18 de diciembre de 2020, conforme la información que surja de un balance especial confeccionado a esa fecha, o:

**a.** El patrimonio neto de la sociedad del último ejercicio comercial cerrado con anterioridad a la fecha indicada en el inciso precedente.

**b.** La opción prevista en el párrafo anterior no podrá ser ejercida si la incorporación de las acciones o participaciones valuadas de acuerdo con el **inciso b)** precedente no arrojare aporte a ingresar, debiendo, en ese supuesto, valuarse en los términos del **inciso a)**.

El o la accionista, socio o socia o partícipe que hubiera modificado el porcentaje de su participación entre la fecha de cierre del último ejercicio comercial cerrado con anterioridad al 18 de diciembre de 2020 y esta última fecha, no podrá ejercer la opción descripta en el inciso b) del primer párrafo.

En el supuesto que un mismo sujeto tuviera participaciones en distintos entes, una vez ejercida la opción de este artículo, esta será de aplicación para la totalidad de su tenencia accionaria o participación en el capital de las sociedades.

Las sociedades o entidades emisoras estarán obligadas a suministrar la información requerida para la valuación, de conformidad con el presente artículo. Las disposiciones del presente artículo, en lo pertinente, también resultarán de aplicación cuando se trate de los sujetos mencionados en el **inciso d) del artículo 53** de la **Ley de Impuesto a las Ganancias**, texto ordenado en 2019 y sus modificaciones, que confeccionen balances en forma comercial.

Asimismo, en el **artículo 2** señala que a los fines de lo dispuesto en el **artículo 3° de la Ley de Aporte Solidario y Extraordinario**, los sujetos deberán declarar como propios e incluir en la base de determinación del aporte, los bienes aportados a las estructuras allí previstas, por un porcentaje equivalente al de su participación en estas. A esos efectos deberán considerarse las participaciones indirectas a que alude la norma legal, hasta el tercer grado, inclusive.

En tanto, en el **artículo 3** indica que los sujetos mencionados en el segundo párrafo del **inciso a) o en el inciso b) del artículo 2° de la Ley de Aporte Solidario y Extraordinario**, deben designar un único o una única responsable sustituto o sustituta a los efectos de cumplir con las obligaciones pertinentes a la determinación e ingreso del aporte.

![Esperan recaudar $300.000 millones. Prevé el cobro por única vez de una tasa de entre el 2 y el 3,5% a los patrimonios de las personas físicas que hayan declarado más de 200 millones de pesos (REUTERS/Marcos Brindicci)](https://www.infobae.com/new-resizer/xrzAaoty5B3F25FwikMTDwGwr2U=/420x236/filters:format(jpg):quality(85)//cloudfront-us-east-1.images.arcpublishing.com/infobae/OFFAZHPV7NBFJJWHJKC6D6UTBM =640x360)

Esperan recaudar $300.000 millones. Prevé el cobro por única vez de una tasa de entre el 2 y el 3,5% a los patrimonios de las personas físicas que hayan declarado más de 200 millones de pesos (REUTERS/Marcos Brindicci)

En el **artículo 4** establece que los bienes a los que se refiere el **inciso g) del artículo 22 del Capítulo II del Título VI de la Ley de Impuesto sobre los Bienes Personales**, no se considerarán a los fines de determinar los bienes comprendidos en las disposiciones de la **Ley de Aporte Solidario y Extraordinario**.

Mientras que en el **artículo 5** fija que el plazo de repatriación al que hace referencia el **artículo 6** [de la Ley de Aporte Solidario y Extraordinario](https://www.infobae.com/economia/2020/12/18/el-gobierno-promulgo-el-impuesto-a-la-riqueza/#:\~:text=Dos%20semanas%20despu%C3%A9s%20de%20que,de%20la%20pandemia%20de%20coronavirus.) debe computarse en días hábiles administrativos.

De acuerdo a la reglamentación, el **artículo 6** indica que quedan exceptuados de las disposiciones del **artículo 5 de la Ley de Aporte Solidario y Extraordinario** los sujetos comprendidos en el inciso **a) del artículo 2°**, que hubieren repatriado fondos en el plazo señalado en el artículo anterior, que representen, por lo menos, un 30% del valor total de los activos financieros en el exterior.

“La excepción indicada se mantendrá en la medida en que esos fondos permanezcan depositados en una cuenta abierta a nombre de su titular (caja de ahorro, cuenta corriente, plazo fijo u otras), en entidades comprendidas en el **régimen de la Ley N° 21.526** y sus modificaciones, hasta el **31 de diciembre de 2021 inclusive**, o, una vez cumplida la repatriación y efectuado el mencionado depósito, esos fondos se afecten, en forma parcial o total, a cualquiera de los siguientes destinos: su venta en el mercado único y libre de cambios, a través de la entidad financiera que recibió la transferencia original desde el exterior.

O a la adquisición de obligaciones negociables emitidas en moneda nacional que cumplan con los requisitos del **artículo 36 de la Ley N° 23.576** y sus modificatorias; a la adquisición de instrumentos emitidos en moneda nacional destinados a fomentar la inversión productiva que establezca el Poder Ejecutivo, siempre que así lo disponga la norma que los regula;

Que se aporte a las sociedades regidas por la **Ley General de Sociedades** en las que el o la aportante tuviera participación a la fecha de entrada en vigencia de la Ley de Aporte Solidario y Extraordinario y siempre que la actividad principal de aquellas no fuera financiera.

Agrega que en el supuesto mencionado en el **inciso d) precedente**, los sujetos que hubieran recibido los mencionados aportes no deberán distribuir dividendos o utilidades a sus accionistas o socios o socias, en los términos de los **artículos 49 y 50 de la Ley del Impuesto a las Ganancias** desde la entrada en vigencia de la presente reglamentación y hasta el **31 de diciembre de 2021, inclusive**.

“Cuando los fondos que se hubieren depositado se destinaren, en forma parcial, a alguna de las operaciones mencionadas en los incisos precedentes, el remanente no afectado a estas últimas deberá continuar depositado en las cuentas y hasta el 31 de diciembre de 2021, inclusive”, afirma el texto.

![El Gobierno reglamentó el impuesto a la riqueza y la ley entró en vigencia (Foto Presidencia)](https://www.infobae.com/new-resizer/ly8C-k-AvQIpzBpnfvWAX0EZnnE=/420x236/filters:format(jpg):quality(85)//cloudfront-us-east-1.images.arcpublishing.com/infobae/E7LEAUAZ7NATVASQU4H4LTRZ7M.jpg =1920x1080)

El Gobierno reglamentó el impuesto a la riqueza y la ley entró en vigencia (Foto Presidencia)

Indica que en aquellos casos en que no corresponda el ingreso del aporte en los términos del **artículo 5 de la Ley N° 27.605, los sujetos del inciso a) del artículo 2** de esa ley deberán ingresarlo de conformidad con lo normado en el artículo 4° de la citada norma legal.

“Las disposiciones de este artículo resultarán procedentes cuando los fondos y los resultados, derivados de las inversiones mencionadas en el segundo párrafo ‒obtenidos antes del 31 de diciembre de 2021 inclusive‒ se afectaren a cualquiera de los destinos mencionados en este y en las condiciones allí establecidas, incluso, de manera indistinta y sucesiva a cualquiera de ellos”, detalla.

En tanto, el **artículo 7** dispone que en el caso de participaciones societarias y/o equivalentes (títulos valores privados, acciones, cuotas y demás participaciones) en todo tipo de entidades, sociedades o empresas, con o sin personería jurídica, constituidas, domiciliadas, radicadas o ubicadas en el exterior incluidas las empresas unipersonales, **se entenderá que no constituyen activos** financieros cuando las entidades, sociedades o empresas constituidas, domiciliadas, radicadas o ubicadas en el exterior, en forma directa o indirecta, realicen principalmente actividades operativas, entendiendo que dicho requisito se cumple cuando sus ingresos no provengan en un porcentaje superior al 50 % de rentas pasivas.

“Sin perjuicio de ello, se presumirá que se trata de un activo financiero cuando dicha participación no supere el 10 % del capital de la entidad, sociedad o empresa constituida, domiciliada, radicada o ubicada en el exterior”, afirma el decreto.

En ese marco, señala que en el caso de créditos y todo tipo de derecho del exterior susceptibles de valor económico, no se consideran incluidos aquellos créditos y/o derechos del exterior vinculados a operaciones de comercio exterior realizadas en el marco de actividades operativas.

“Adicionalmente, tampoco están comprendidos en la definición de activos financieros los créditos y garantías, derechos y/o instrumentos financieros derivados, afectados a operaciones de cobertura que presenten una estrecha vinculación con la actividad económico-productiva y/o se destinen a preservar el capital de trabajo de la empresa en la que los sujetos alcanzados por el aporte extraordinario tuvieren participación”, añade.

El **artículo 8** indica que tratándose de sucesiones indivisas iniciadas a partir del **1° de enero de 2020 inclusive**, “estas deberán regirse, a los fines de la determinación del aporte, por la residencia del o de la causante al **31 de diciembre de 2019**”.

Por último, el **artículo 9** establece que [la AFIP será la encargada de instrumentar regímenes de información](https://www.infobae.com/economia/2020/12/26/impuesto-a-la-riqueza-la-afip-recargo-la-tarea-de-contribuyentes-y-contadores-al-exigir-la-duplicacion-de-datos-en-13-dias/) a los fines de recabar los datos que estime pertinentes para la oportuna detección de “las operaciones que puedan configurar un ardid evasivo o estén destinadas a la elusión del pago del aporte, a las que se refiere el último párrafo del **artículo 9 de la Ley de Aporte Solidario y Extraordinario**”.