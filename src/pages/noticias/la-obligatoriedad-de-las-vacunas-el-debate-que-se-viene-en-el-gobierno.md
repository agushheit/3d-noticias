---
category: Agenda Ciudadana
date: 2021-12-24T06:00:12-03:00
thumbnail: https://assets.3dnoticias.com.ar/albetvaccine.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: La obligatoriedad de las vacunas, el debate que se viene en el Gobierno
title: La obligatoriedad de las vacunas, el debate que se viene en el Gobierno
entradilla: 'Altas fuentes oficiales aseguraron a NA que las decisiones se toman “día
  a día” y afirmaron que “todo puede pasar”.

'

---

La escalada de casos registrado los últimos días y la aparición de la nueva variante Ómicron preocupan al Gobierno. Si bien al momento no planean incorporar nuevas medidas restrictivas más allá del pasaporte sanitario, la portavoz presidencial Gabriela Cerruti dejó abierta la posibilidad de implementar la obligatoriedad de la vacuna contra el Coronavirus.

“Las vacunas hacen que los casos sean leves, siempre puede haber causas de enfermedades concomitantes que agraven, y el mundo en su conjunto está poniendo la carga en pedir que la sociedad se vacune”, afirmó.

Y agregó: “Tenemos una tradición de apego a las vacunas. Insistimos e instauramos el pase sanitario para que todos se vacunen, y cada vez más tenemos que empezar a dejar claro que es responsabilidad de los no vacunados que puedan contraer la enfermedad y se transforme en una enfermedad grave, por lo que pedimos que cumplan con el calendario, y vayan a vacunarse”. En la misma línea, fuentes oficiales señalaron a NA que las decisiones para combatir la pandemia "se toman día a día".

Por otro lado, en conferencia de prensa, Cerruti destacó el papel del pasaporte sanitario a implementarse a partir del primero de enero, que demanda la aplicación del esquema completo para la realización de distintas actividades.

“Tener que aislar a los que están vacunados porque algunos no se quieren vacunar no nos parece justo, sobre todo teniendo en cuenta la enorme cantidad de la población decidió vacunarse, cuidarse y cuidarnos entre todos”, afirmó la portavoz en la típica conferencia de prensa de los jueves.

Si bien el Ministerio de Salud no planea incorporar la vacuna contra el Covid – 19 al cronograma, la idea se instala cada vez más dentro del Gobierno. "La adhesión a las vacunas en argentina es altísima. Sigue habiendo demanda, y el pase sanitario la va a incrementar más todavía", señalaron fuentes cercanas a la titular de la cartera, Carla Vizzotti, y agregaron: "Hoy nuestra estrategia es esa. No es algo que se esté analizando en este momento, no está en agenda".

En la línea con lo deslizado por la portavoz, altas fuentes calificadas de Gobierno aseguraron a NA que las vacunas contra el Coronavirus “dejerán de ser de emergencia”, y pasarán a ser obligatorias en un futuro próximo.

Cabe destacar que el jefe de Gabinete Juan Manzur en diálogo con NA, aportó el miércoles por la mañana a la idea de responsabilizar a las personas que decidieron no vacunarse. El exgobernador de Tucumán, definió al movimiento antivacunas como “gente especial”, y señaló que, si bien configuran expresiones minoritarias, tienden a entorpecer la salida de la pandemia.