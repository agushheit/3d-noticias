---
category: Agenda Ciudadana
date: 2021-02-28T06:00:33-03:00
thumbnail: https://assets.3dnoticias.com.ar/RECONQUISTA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Reconquista: el gobernador le pidió la renuncia al Director del Hospital
  por irregularidades en la vacunación'
title: 'Reconquista: el gobernador le pidió la renuncia al Director del Hospital por
  irregularidades en la vacunación'
entradilla: El gobernador Perotti le solicitó la renuncia a Fabián Nuzzarello, luego
  de detectarse un incumplimiento en el orden de prioridades dentro del operativo
  de vacunación contra el coronavirus.

---
El gobernador Omar Perotti le solicitó la renuncia al director del Hospital de Reconquista, Fabián Nuzzarello, luego de detectarse un incumplimiento en el orden de prioridades dentro del operativo de vacunación contra el coronavirus.

De acuerdo a lo que le informaron al mandatario provincial, en el mencionado hospital no se respetó el escalonamiento de las prioridades por el trabajo que cumple el personal sanitario, ya que inocularon antes a personas vinculadas a miembros del Consejo Administrativo del efector, los cuales no forman parte de la población objetivo.

Esto provocó la decisión del gobernador de Santa Fe de pedirle inmediatamente la dimisión a Nuzzarello, y advirtió nuevamente que será implacable en este tipo de situaciones que alteren o privilegien a sectores que no están contemplados en las prioridades de vacunación contra el coronavirus.

El mandatario provincial adoptó la determinación de solicitarle la renuncia al director Nuzzarello ante la falta de transparencia en el proceso de inoculación a la población establecida como prioridad en el hospital, ratificando que actuará sin dilaciones y de manera terminante ante cualquier tipo de irregularidad que se cometa en el proceso de vacunación contra el Covid-19.

Del mismo modo, la provincia informó que se trabaja diariamente para garantizar la transparencia y la eficiencia en cada uno de los más de 150 puntos de vacunación que se encuentran en el territorio santafesino.