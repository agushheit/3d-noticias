---
category: Agenda Ciudadana
date: 2022-06-27T08:11:44-03:00
thumbnail: https://assets.3dnoticias.com.ar/battery-gfae25e75e_1920.png
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: futurosustentable.com.ar
resumen: Fabricarán en Santa Fe baterías de litio para autos, colectivos y bicicletas
title: Fabricarán en Santa Fe baterías de litio para autos, colectivos y bicicletas
entradilla: Una empresa argentina en asociación con un grupo inversor chino montará
  una planta de fabricación de baterías de litio en Venado Tuerto, Santa Fe, y desplegarán
  para eso una inversión inicial de 12 millones de dólares.

---
Una empresa argentina en asociación con un grupo inversor chino montará una planta de fabricación de baterías de litio en Venado Tuerto, Santa Fe, y desplegarán para eso una inversión inicial de 12 millones de dólares, según aseguraron autoridades del Grupo Iraola y de Gotion High-Tech en una reunión con el presidente Alberto Fernández, antes de su viaje a Alemania, y funcionarios del equipo económico.

El jefe de Estado destacó que “estamos promoviendo la energía eléctrica para la movilidad y hemos enviado una ley a la Cámara de Diputados, es una ley parecida a la que promueve a la industria automotriz y agroalimentaria, y la idea es dar certezas a los inversores”.

El mandatario, que estuvo acompañado por el ministro de Desarrollo Productivo, Daniel Scioli, el secretario de Industria, Ariel Schale, el asesor del ministerio, Marcelo Kloster, la secretaria de Relaciones Económicas Internacionales, Cecilia Todesca, y de Minería, María Fernanda Ávila, analizó con la delegación de la compañía china los proyectos que tiene la Argentina en desarrollo con la industria del litio, que lo convierten en un referente regional en la materia.

Gotion Inc es una empresa líder en la fabricación de baterías de litio para la industria automotriz en varios países, ocupa el quinto lugar a nivel global, además de producir carbonato de litio grado baterías y celdas.

El mandatario conversó con los ejecutivos de la empresa asiática sobre la ley de Movilidad Sutentable, que se encuentra con estado parlamentario y pronta a ser tratada en el Congreso, que promueve las inversiones en electromovilidad, a través de la creación de un régimen especial para los vehículos propulsados por fuentes de energía sustentables.

Tras el encuentro, Daniel Scioli señaló que “el presidente ha generado las condiciones para que una empresa líder china invierta para fabricación en la cadena de valor del litio. Y si bien hace años se extrae y se exporta, ahora se han generado las condiciones de industrialización del litio, lo que es un gran avance hacia el futuro de la electromovilidad que se viene”.

Por su parte, el CEO de Gotion High Tech agradeció el recibimiento, invitó a Alberto Fernández a China para que conozca las plantas de la compañía y remarcó que “este tipo de visitas son muy importantes para nosotros y queremos seguir impulsando la relación bilateral”.