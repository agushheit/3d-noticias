---
category: La Ciudad
date: 2021-02-12T18:00:17-03:00
thumbnail: https://assets.3dnoticias.com.ar/autocinejpg.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Autocine en La Redonda: llega la pantalla móvil más grande disponible en
  la Argentina'
title: 'Autocine en La Redonda: llega la pantalla móvil más grande disponible en la
  Argentina'
entradilla: Será este domingo y lunes, con entrada libre, gratuita y cupos limitados,
  en el estacionamiento de ese espacio cultural. ¿Cómo conseguir los tickets?

---
La Secretaría de Turismo del Ministerio de Producción, Ciencia y Tecnología y el Ministerio de Cultura, sobre una propuesta del Ministerio de Turismo y Deportes de la Nación junto con el Consejo Federal de Turismo, llevarán adelante Autocine Argentina Unida, proyecciones que se realizarán el domingo 14 y lunes 15 de febrero próximos, en el estacionamiento de La Redonda, Arte y Vida Cotidiana (Salvador del Carril y Belgrano), aquí en la ciudad de Santa Fe.

La iniciativa, de entrada libre y gratuita pero con cupos limitados, tiene como objetivo ofrecer una propuesta cultural alternativa a las tradicionales, acorde con las circunstancias actuales de la emergencia sanitaria como producto de la pandemia de Covid-19; y cada función implica el montaje de la pantalla móvil más grande disponible en la Argentina.

### Pelis y entradas

La actividad llega a Santa Fe luego de recorrer gran parte del país proyectando películas nacionales e internacionales para todos los públicos. En esta ocasión se realizarán dos jornadas de proyecciones en el estacionamiento del espacio dependiente del Ministerio de Cultura:

La primera película, que se podrá ver el domingo 14 a las 20:30, es “Gilda”, basada en la vida y carrera de la famosa cantante, quien falleció en un accidente automovilístico en 1996. La cinta, dirigida por Lorena Muñoz y escrita por Tamara Viñes, está protagonizada por Natalia Oreiro.

El lunes 15, en tanto, habrá dos proyecciones: desde las 21 se podrá ver “Amando a Maradona” (2005, Javier Vázquez), un documental que retrata y da cuenta de la pasión desenfrenada que la figura de Diego Maradona despertaba y aún despierta.

A las 22:30 será el turno de “Mi último traje”, una película argentina-española-polaca dramática de 2018, escrita y dirigida por Pablo Solarz y protagonizada por Miguel Ángel Solá, Ángela Molina y Martín Piroyansky, que narra una historia que explora el judaísmo, la vejez, las diferencias generacionales y las heridas que continúan abiertas de la Segunda Guerra Mundial.

La entrada es gratuita pero con cupo limitado. Se debe sacar una entrada por vehículo y la capacidad del espacio es para 40 automóviles:

> > Entradas para el domingo 14: [https://www.eventbrite.com.ar/e/141133795983](https://www.eventbrite.com.ar/e/141133795983 "https://www.eventbrite.com.ar/e/141133795983")

> > Entradas para el lunes 15: [https://www.eventbrite.com.ar/e/141144128889](https://www.eventbrite.com.ar/e/141133795983 "https://www.eventbrite.com.ar/e/141133795983")

La propuesta contará, además, con un patio gastronómico para todos los gustos (habrá opciones para vegetarianos, veganos y celíacos) de la mano de emprendedores y emprendedoras locales, que ofrecerán sus productos sin pagar ningún tipo de canon por el espacio.

De acuerdo a los protocolos sanitarios en el marco de la pandemia, no se permitirá el ingreso sin tapabocas, habrá puestos de expendio de alcohol en gel y se mantendrá la distancia social.