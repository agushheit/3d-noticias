---
category: Agenda Ciudadana
date: 2021-05-27T08:50:38-03:00
thumbnail: https://assets.3dnoticias.com.ar/dengue1.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNL
resumen: Crece la incidencia del dengue en la provincia de Santa Fe
title: Crece la incidencia del dengue en la provincia de Santa Fe
entradilla: El hallazgo se desprende de un estudio realizado por investigadoras de
  FICH-UNL y del CONICET. En la provincia de Santa Fe, General Obligado registra la
  mayor incidencia de casos.

---
Investigadoras del Centro de Estudios de Variabilidad y Cambio Climático (CEVARCAM) de la Facultad de Ingeniería y Ciencias Hídricas (FICH) de la Universidad Nacional del Litoral (UNL) y Conicet estudian de qué modo se relacionan determinadas enfermedades infecciosas con el clima de la región Litoral. Comenzaron a trabajar con leptospirosis y en los últimos años, dada la importancia de la problemática del dengue en la región y en particular el histórico brote producido a principios del año 2020 en gran parte del país, elaboraron un diagnóstico de la situación de la provincia en un período de más de una década, como puntapié inicial para próximas instancias de investigación.

En el estudio publicado se realiza un análisis espacial y temporal de los casos de dengue, a partir de datos provistos por el Ministerio de Salud de la Nación, concluyendo que en la provincia de Santa Fe se produjeron cuatro brotes en los años 2009, 2016, 2019 y 2020, siendo el de mayor importancia el de 2020, con una incidencia cuatro veces mayor que el último más importante del año 2016. Por consiguiente, la incidencia de la enfermedad va aumentando en la provincia a medida que pasa el tiempo y los brotes son cada vez más frecuentes.

Asimismo, se detecta que el departamento General Obligado fue el de mayor incidencia en el período de estudio. “Concluimos que esto puede deberse a la ubicación geográfica del mismo, ya que es la vía de acceso a los países limítrofes donde la enfermedad es endémica y a las provincias del norte de Argentina, en las cuales los casos son más frecuentes. Por lo tanto, este departamento tiene más probabilidades de ingreso de casos importados desde otras zonas”, destacó María Soledad López, investigadora de UNL (FICH y FBCB) y Conicet.

Otro resultado que muestra el trabajo son los serotipos de dengue que circularon en la provincia. El dengue tiene cuatro serotipos. Hasta 2019, el serotipo 1 fue el que circuló en mayores proporciones, pero a partir de 2020, tanto el serotipo 1 como el 4 tuvieron proporciones semejantes. “Es muy importante saber los serotipos que circulan en los momentos de brote porque las formas graves de la enfermedad suelen desarrollarse con mayor probabilidad en personas que se infectaron y vuelven a contraer el virus con un serotipo diferente al anterior”, remarcó Andrea Gómez, investigadora de FICH-UNL y Conicet.

Cabe destacar que estos resultados fueron publicados en Scientific Data Nature, una de las revistas científicas más prestigiosas a nivel mundial.

**Transferencia**

Además del alto valor científico de este trabajo, las investigadoras destacaron el aporte que el mismo puede realizar a la salud de la población si los datos logran traducirse en acciones concretas. “Luego de detectar que General Obligado era uno de los departamentos de mayor incidencia de la enfermedad, nos acercamos a Reconquista para ofrecer nuestra colaboración y así surgió un convenio entre Conicet y la Municipalidad de esa ciudad. Desde el año pasado venimos trabajando con este Municipio en diferentes aspectos de la problemática del dengue. Principalmente realizamos el monitoreo de oviposición (puesta de huevos del mosquito Aedes aegypti, transmisor del virus del dengue) para contar con información en tiempo real, que le sirve a los tomadores de decisiones para poder maximizar sus recursos y orientar sus estrategias preventivas. Del mismo modo, este año comenzaremos a trabajar con el Municipio de Santo Tomé para evaluar la situación del vector y mejorar las acciones preventivas. El trabajo conjunto y sostenido en el tiempo entre los ámbitos académicos y gubernamentales es de fundamental importancia para poder lograr resultados positivos en las problemáticas de salud”.

**Equipo Interdisciplinario**

La publicación consiste en un trabajo multidisciplinario en el cual participaron como autores María Soledad López (CEVARCAM, Centro de Investigaciones sobre Endemias Nacionales (CIEN)-Facultad de Bioquímica y Ciencias Biológicas (FBCB) UNL-Conicet), Daniela Jordan (Ministerio de Salud de Santa Fe), Evelyn Blatter (cientibecaria CEVARCAM UNL), Elisabet Walker (CEVARCAM UNL - CONICET), Andrea Gómez (CEVARCAM UNL-CONICET), Gabriela Müller (CEVARCAM UNL-Conicet), Diego Mendicino (CIEN-FBCB UNL-CONICET), Michael Robert (Virginia Commonwealth University, USA) y Elizabet Estallo (Instituto de Investigaciones Biológicas y Tecnológicas CONICET-Universidad Nacional de Córdoba).

Al respecto, López subrayó: “Somos un grupo en desarrollo, conformado por investigadores jóvenes, en el que también participan estudiantes de grado que se vienen formando en la materia. Estamos muy contentos de que el esfuerzo y el empeño de estos años se manifiesten en algo tan importante como la publicación en esta revista”.