---
category: La Ciudad
date: 2020-12-11T11:46:25Z
thumbnail: https://assets.3dnoticias.com.ar/liso.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Este viernes se celebra el "Día del liso santafesino"
title: Este viernes se celebra el "Día del liso santafesino"
entradilla: Santa Fe celebra al liso como patrimonio cultural inmaterial de la ciudad
  a las 11 en Don Marcos (Castellanos y Las Heras). Organiza Beer Fest Group.

---
Este viernes, Santa Fe celebra a uno de sus símbolos máximos, el liso. Será en un encuentro que se realizará a las 11 en Don Marcos, espacio tradicional de la ciudad ubicado Castellanos y Las Heras.

La propuesta es organizada por Beer Fest Group y busca rendir homenaje al modo tradicional y singular en que Santa Fe disfruta de la cerveza, popularizada por Otto Schneider.

**Desde 2014, por iniciativa de la exconcejala Adriana Molina, se conmemora cada 11 de diciembre como el Día del liso santafesino, al haber sido declarado como patrimonio cultural inmaterial de la ciudad.** De esta manera, fue la primera vez que el Concejo santafesino sancionó a una tradición, en consonancia con lo establecido por la Convención de la Unesco.

# **Historia**

A principios del siglo XX, la ciudad de Santa Fe se convirtió en un polo de producción cervecera por una conjunción de factores geográficos e históricos que hicieron que dos cervecerías se instalaran y desarrollaran en la ciudad, como Santa Fe y Schneider.

**Otto Schneider llegó a la ciudad en 1911** y luego de trabajar varios años como maestro cervecero en la Cervecería Santa Fe -anteriormente en Bieckert y San Carlos-, **en 1931 fundó su propia cervecería** para hacer realidad su sueño. 

**Es allí donde desplegó no solo su tradicional receta, sino su propio modelo de producción y forma de servir la cerveza, dando origen al liso.** 

Si bien se tomaba desde un principio en jarras, fue Otto Shneider el que popularizó hacerlo en vaso de vidrio liso, sin textura, para sentir su temperatura y apreciar su color y pureza, eligiendo un vaso sin talla de 250 cm³ y servido con dos dedos de espuma. La gente lo adoptó inmediatamente y lo popularizó, y muy pronto llegaron desde otras provincias para probar el liso santafesino.

![](https://assets.3dnoticias.com.ar/liso1.jpg)