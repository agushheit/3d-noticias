---
category: Agenda Ciudadana
date: 2021-01-28T10:05:37Z
thumbnail: https://assets.3dnoticias.com.ar/helado.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Con información de Infobae
resumen: 'El kilo de helado ya supera los $1.000: qué disparó el precio y por qué
  las heladerías siguen en crisis'
title: 'El kilo de helado ya supera los $1.000: qué disparó el precio y por qué las
  heladerías siguen en crisis'
entradilla: Es un 30% más que en el mismo mes de 2020, un poco por debajo de la inflación
  general para el año pasado

---
Luego de un año de crisis, con caída de ventas y aumento de los costos, las heladerías artesanales porteñas buscan sobrevivir. En enero, el precio del kilo de helado en la ciudad de Buenos Aires va desde los $800 hasta algo más de $1.000 en las principales cadenas. Es aproximadamente un 30% más en comparación con el mismo mes de 2020, un poco por debajo de la inflación general para el año pasado, que cerró en 36,1% según el Indec.

¿Cuáles son los precios en las cadenas de heladerías premium? En Freddo y Volta, el kilo de helado se vende a $1.050, de acuerdo a los datos de la plataforma de delivery Pedidos Ya. En Lucciano’s, a $930 y en Persicco a $920. En tanto, en la cadena Daniel se puede conseguir por 850 pesos.

> Hoy las heladerías artesanales están con muy baja rentabilidad por el aumento de los precios de la materia prima

“Hoy las heladerías artesanales están con muy baja rentabilidad por el aumento de los precios de la materia prima. Los lácteos tienen los precios más o menos controlados, pero se usan muchos insumos importados que tuvieron fuertes aumentos en 2020, como el chocolate. Hay determinados chocolates para coberturas que subieron hasta 90% en el último año”, destacó Gabriel Famá, presidente de la Asociación Fabricantes Artesanales de Helados y Afines (Afadhya).

Entre los aumentos más significativos están las frutas secas. Según detalló Famá, un año atrás el precio final de un kilo de helado equivalía a dos kilos de almendras, mientras que hoy solo equivale a un kilo. Mientras que el kilo de nueces es más caro que el kilo de helado. La chaucha de vainilla, en tanto, se comercializa a “precio dólar” y hoy el valor es de $120.000 el kilo.

![El gusto más pedido por los argentinos es el dulce de leche granizado](https://www.infobae.com/new-resizer/nX8phA-d5mSoZo1HPmCaNHRm0wc=/420x280/filters:format(jpg):quality(85)//cloudfront-us-east-1.images.arcpublishing.com/infobae/4AC5HFNCEJHVFKPWXITOOCOELU.jpg)

“El costo del kilo de helado puede ser alto, pero hay que considerar la calidad del producto y de la materia prima que se usa. El precio condice con eso”, aclaró el presidente de la asociación. Se estima que en la ciudad de Buenos Aires hay unas 200 heladerías artesanales.

En los meses de la cuarentena más estricta, las ventas de las heladerías artesanales eran de un 20% a un 30% en comparación con los niveles de venta de los mismos meses de años anteriores. Ahora, estiman que están entre un 60% y un 70% respecto de enero del año pasado.

> Gran parte de las ventas de las heladerías son impulsivas y dependen de la circulación de gente que haya en la vía pública

“Comenzamos a sufrir el impacto de la pandemia ya desde los primeros días de marzo del año pasado cuando había menos gente en la calle, incluso antes de la cuarentena. Gran parte de las ventas de las heladerías son impulsivas y dependen de la circulación de gente que haya en la vía pública. El punto de inflexión fue el 10 de marzo cuando solo se permitió vender por delivery y no todas las heladerías lo tenían. Muchas se tuvieron que adherir a plataformas que cobraban hasta 35% de comisión”, detalló Famá.

A pesar de que en diciembre pasado las empresas de delivery acordaron topes máximos de hasta 18% en sus comisiones, solo se incluyó a los restaurantes y locales gastronómicos que integran la Federación Empresaria Hotelera Gastronómica de la República Argentina (Fehgra), pero no a las heladerías artesanales. Por eso, desde la asociación se reunirán la próxima semana con Paula Español, la secretaria de Comercio Interior, para impulsar un acuerdo similar para el rubro de las heladerías.

![Durante 2020 se pidieron más de 12 millones de kilos de helado por una de las plataformas de delivery](https://www.infobae.com/new-resizer/1o7qmKtxSGwv2uuLhyyCgyRkMsM=/420x420/filters:format(jpg):quality(85)//cloudfront-us-east-1.images.arcpublishing.com/infobae/X2NQRCNYMVFKTBQ5JWOFOXGHRE.jpg)

Desde el sector, aseguran que las heladerías artesanales suelen hacer en los meses de verano un “colchón” que les permite atravesar la caída de ventas durante los meses de invierno. Algo que no sucedió en 2020, por lo tanto avizoran una situación más compleja para los próximos meses, a partir de abril. Especialmente para los locales ubicados en las zonas del micro y macrocentro porteño, donde la circulación de personas sigue siendo muy baja.

Sin embargo, desde la asociación detallaron que no hubo un cierre significativo de locales durante 2020 y que la mayoría de las heladerías lograron mantenerse a flote. “Las heladerías que están en los barrios pudieron estar un poco mejor con respecto a las que están en las zonas más céntricas, no funcionan los tribunales, las oficinas, los teatros, y eso hace caer mucho las ventas. Las heladerías redujeron mucho los costos y, por ejemplo, no tomaron personal temporario en el verano como solían hacer. Depende mucho de la zona”, agregó Famá.

Según la plataforma de delivery Pedidos Ya, durante 2020 se pidieron más de 12 millones de kilos de helado. El top cinco de sabores más pedidos fue: dulce de leche granizado, banana split, tramontana, chocolate con almendras y frutilla a la crema.