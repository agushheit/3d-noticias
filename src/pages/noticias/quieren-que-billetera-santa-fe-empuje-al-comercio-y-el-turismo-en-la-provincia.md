---
category: Agenda Ciudadana
date: 2021-10-18T06:00:18-03:00
thumbnail: https://assets.3dnoticias.com.ar/BILLERETA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Quieren que Billetera Santa Fe empuje al comercio y el turismo en la provincia
title: Quieren que Billetera Santa Fe empuje al comercio y el turismo en la provincia
entradilla: El secretario de Comercio Interior de la provincia evaluó el funcionamiento
  de Billetera Santa Fe y dio detalles sobre su posible conjunción con el previaje.

---
La herramienta implementada por el Gobierno Provincial es única en el país y ofrece un reintegro de entre 20% y 30% de la compra en comercios adheridos mediante la app Billetera Santa Fe, elaborada por PlusPagos. Fue creada en febrero de este año y ya se confirmó que continuará durante todo el 2022.

En diálogo con el programa La Mañana de UNO (de 7 a 9 por FM 106.3 La Radio de UNO) el secretario de Comercio Interior de la provincia de Santa Fe, Juan Marcos Aviano, contó que cuando el gobernador diseñó esta herramienta encargó el diseño comercial al Ministerio de Producción y el diseño financiero al Ministerio de Economía.

“Las proyecciones eran de 500.000 billeteras a fin de año y 10.000 comercios, y una proyección de inversión del orden de los 3.500 millones de pesos. Hoy no te puedo precisar cuál es la inversión hasta la fecha, pero claramente se superó esa estimación para el 2021, garantizándose obviamente todos los recursos porque la situación y el contexto lo amerita”, aseguró.

El funcionario remarcó que el comercio y el bolsillo familiar necesitaban esta medida para recomponer sus ingresos por lo que los resultados duplicaron sus estimaciones: “Estamos en 1.100.000 billeteras y 25.000 comercios”. El presupuesto de 2022 que envió el Ejecutivo a la Legislatura contempla la continuidad de la medida aunque, insistió Aviano, “son estimaciones”.

Y agregó: “El gobernador fue muy claro: hay que sumar al comercio a la reactivación económica que ya claramente la industria y el agro en la provincia de Santa Fe están marcando y liderando a nivel nacional, con exportaciones de varios sectores y también las inversiones en ciencia y tecnología”.

El programa beneficia a los consumidores con un 30% de reintegro en una amplia variedad de rubros (como alimentos, indumentaria, jugueterías/librerías, farmacias, perfumerías y turismo) y un un 20% en electro, informática y artículos del hogar. Por mes, el reintegro que hace la provincia es de 5.000 pesos por Billetera (es decir, por persona). Consultado por la posibilidad de aumentar ese tope, en el marco de un continuo aumento de precios, el secretario de Comercio Interior dijo que “no está en evaluación”.

“Hemos generado un mercado importante de 1.100.000 usuarios y 25.000 comercios, por eso significa una inversión del Estado muy importante. También el aporte del comercio: de ese 30%, el 25,5% lo subsidia el Estado Provincial, pero el 4,5% lo aporta el comercio. Por lo pronto no está en evaluaciones”, afirmó.

Según el vocero, el sector comercial manifiesta su aceptación y el impacto positivo que tuvo la medida. “Hablan francamente de un momento de incremento de las ventas en volúmenes y en algunos casos me hablan de mejores números que antes de la pandemia. Eso es recomponer la actividad. Obviamente el 2019 no fue un buen año económico y para el comercio menos aún, pero hay otras perspectivas y otras expectativas en el sector comercial”.

“Hoy por hoy estamos muy conformes porque la aceptación también tuvo que ver con que siempre los destinatarios quieren ver si funciona. Al principio sí había comercios dudosos. Con cuestiones que o tienen que ver con el gobierno o cuando viene de arriba uno duda. Otros dicen «a caballo regalado no se le miran los dientes», pero lo importante es que el programa es rápido”, dijo Aviano, destacando la agilidad de la app y el rápido sistema de reintegros.

**Santa Fe Turística**

Si bien el santafesino que vacacione en la provincia ya puede aprovechar el programa nacional Previaje junto a la Billetera Santa Fe, se está buscando la forma de incluir al resto de los turistas. “Aún resta resolver la cuestión técnica y la solución tecnológica de que argentinos que no tienen domicilio en las provincia de Santa Fe, al comprar a través del previaje un paquete turístico con destino a Santa Fe, se pueda generar allí un complemento y agregar demanda a través de billetera Santa Fe, por supuesto durante los días de estadía de ese no santafesino dentro de la provincia”, dijo Aviano.

Pero aclaró: “Todavía no están las precisiones de cómo se va a poder resolver; lo está trabajando la Secretaría de Turismo para el Ministerio de Turismo de la Nación para ver también allí el diseño financiero entre Nación Servicios, que es quien opera la tarjeta del Previaje, y Pluspagos, que es quien opera Billetera Santa Fe".

“La voluntad de hacer esto es para volver a ubicar a Santa Fe entre los 10 destinos turísticos más elegidos de la Argentina. Sabemos que lo del fin de semana largo con 400.000 turistas en la provincia es un preludio de lo que puede ser nuevamente una buena temporada para la provincia, pero hay que resolver cuestiones técnicas que obviamente a veces no son lo rápido o fácil de resolver como uno quisiera”, concluyó el secretario.