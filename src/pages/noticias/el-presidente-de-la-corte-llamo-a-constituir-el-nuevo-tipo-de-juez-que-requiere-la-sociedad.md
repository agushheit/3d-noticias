---
category: Agenda Ciudadana
date: 2021-03-10T06:39:21-03:00
thumbnail: https://assets.3dnoticias.com.ar/corte-suprema.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=AI3oKB-jZ-s
author: 3D Noticias
resumen: El presidente de la Corte llamó a constituir el nuevo tipo de Juez que requiere
  la sociedad
title: El presidente de la Corte llamó a constituir el nuevo tipo de Juez que requiere
  la sociedad
entradilla: El Dr. Falistocco dejó inaugurado el Año Judicial 2021.Previamente, con
  el gobernador Omar Perotti, formalizaron el programa Santa Fe + Justicia

---
El Presidente de la Corte Suprema de Justicia de Santa Fe, Dr. Roberto Falistocco, y el Gobernador de la provincia, CPN Omar Perotti, firmaron hoy en los Tribunales de la ciudad de Santa Fe un acuerdo interinstitucional para llevar adelante el programa “Santa Fe + Justicia – Desafíos para la Justicia Santafesina”. La ceremonia de rubrica del acuerdo fue parte del Acto de Inicio del Año Judicial, con el que las máximas autoridades del Poder Judicial dejaron formalmente inauguradas las tareas de los tribunales provinciales para el año en curso. El acto contó con la presencia de la Corte completa, integrada por los ministros Eduardo Spuler, Rafael Gutiérrez, Mario Netri, María Angélica Gastaldi, Daniel Erbetta y el Procurador General, Jorge Barraguirre.

Mediante el Programa Santa Fe + Justicia, las autoridades de los tres poderes del Estado se comprometen a llevar adelante acciones tendientes a modernizar y perfeccionar el servicio de Justicia, identificando cuatro ejes temáticos: conectividad e informatización, información ciudadana, nuevas competencias y jurisdicciones y desafíos procesales.

Posteriormente, el Dr. Falistocco se refirió a los desafíos que enfrenta el Poder Judicial en tiempos de pandemia, las actividades realizadas para hacerle frente sin descuidar el servicio de justicia y los lineamientos que caracterizarán a su gestión al frente del Poder Judicial santafesino en este 2021.

Luego de realizar una breve reseña histórica y doctrinaria sobre cómo debe entenderse en estos momentos el mandato constitucional de “afianzar la justicia”, el Dr. Falistocco aseguró que la sociedad aspira hoy a contar con un nuevo tipo de juez, que sepa conjugar el conocimiento amplio del derecho, la comprensión de la realidad que lo circunda, la capacitación permanente, el acercamiento con la comunidad y una actitud personal de humildad, respeto y valoración del diálogo y el consenso. Así, el Presidente de la Corte puso especial énfasis en dos capacidades insoslayables para los jueces de la nueva realidad: la cultura digital, para incorporar el manejo de las herramientas informáticas, y la cultura comunicacional que le permita una fluida relación con la comunidad y los medios de comunicación. Falistocco reseñó los importantes avances realizados durante la pandemia para avanzar en el expediente electrónico, destinado a superar el expediente papel; sin embargo, insistió con que es la oralidad en los procesos judiciales la meta a la que se pretende arribar.

La necesidad de que el Poder Judicial adopte definitivamente una perspectiva de género también ocupó un espacio importante en el mensaje del Dr. Falistocco, quien llamó a redoblar esfuerzo mancomunados con todos los poderes del Estado para poner fin a la violencia de género, familiar y los femicidios.

El énfasis puesto por el Presidente de la Corte en la capacitación permanente fue el marco para un dar a conocer una idea que Falistocco desea instalar, según señaló, aunque sea para que sea motivo de debate: el proyecto de crear una Escuela Judicial santafesina que propenda a la formación integral de los magistrados y funcionarios que la sociedad le reclama al Poder Judicial. Y destacó: en relación a la interrelación de los órganos del Estado, la importancia central que adquiere que toda la provincia de Santa Fe avance en un proyecto de conectividad integral que vincule y permita relacionar a nuestro Poder Judicial con el resto de los organismos estaduales.

Antes de concluir y dar por iniciado el Año Judicial 2021, el Presidente de la Corte comentó la calidad institucional que ostenta la provincia de Santa Fe, marcada por el diálogo respetuoso entre Poderes, y su deseo de que dicho valor se mantenga en el tiempo.

Para finalizar, sostuvo enfáticamente: “En este escenario cabe recordar que la acción realizada por Eróstrato (incendiar el templo de Artemisa de Éfeso, considerado una de las siete maravillas del mundo, con el único fin de lograr fama a cualquier precio) forjó el "complejo de Eróstrato", el trastorno según el cual el individuo busca sobresalir, distinguirse, ser el centro de atención”.

**Autoridades presentes y firmantes**

Participaron de la firma del acta institucional, la Vicegobernadora Alejandra Rodenas, el presidente de la Cámara de Diputados Miguel Lifschitz, el ministro de Gobierno Roberto Sukerman, el secretario de Justicia Héctor Somaglia, el Fiscal General, Jorge Baclini, la Defensora General, Jaquelina Balangione, el presidente del Colegio de Magistrados y Funcionarios, Roberto Dellamónica, los Decanos de las Facultades de Derecho de la provincia Javier Aga, Alfredo Botta, Enrique Müller, Luis M. Caterina, y, presidentes de los cinco Colegios de Abogados Abramovich, Ensick, Pagano, Eusebio, y, Turcato; y el secretario general del Sindicato de Trabajadores Judiciales, Jorge Perlo.  
 

**LINK A DISCURSO COMPLETO:**

[http://www.justiciasantafe.gov.ar/js/index.php?go=c&idc=61](http://www.justiciasantafe.gov.ar/js/index.php?go=c&idc=61 "http://www.justiciasantafe.gov.ar/js/index.php?go=c&idc=61")