---
layout: Noticia con imagen
author: Nota de la redacción de 3DNoticias
resumen: "Aguas Santafesinas: WhatsApp"
category: Estado Real
title: Los usuarios de Aguas Santafesinas pueden efectuar reclamos o gestiones a
  través de WhatsApp
entradilla: El usuario solamente debe agendar el número 3416950008, que
  aparecerá disponible en su listado de contactos de WhatsApp, mostrando como
  foto de perfil el isologo de Aguas Santafesinas.
date: 2020-11-21T12:22:57.029Z
thumbnail: https://assets.3dnoticias.com.ar/assa.jpeg
---
Aguas Santafesinas puso en marcha un sistema de atención de usuarios a través de WhatsApp. Desde la empresa señalan que es un nuevo canal de autoconsulta, de fácil acceso, sin costo alguno y que permite efectuar trámites, reclamos o consultas sin necesidad de llamar por teléfono o ingresar a la web.

El usuario solamente debe agendar el número 3416950008, que aparecerá disponible en su listado de contactos de WhatsApp, mostrando como foto de perfil el isologo de Aguas Santafesinas. El sistema también está disponible en la aplicación de mensajes Telegram.

Aguas Santafesinas es la primera empresa de agua potable y saneamiento de la Argentina en disponer de un sistema automatizado de atención de usuarios a través de esta aplicación de mensajes.

## **¿CÓMO FUNCIONA?**

En el inicio del diálogo a través de WhatsApp, aparecerá un mensaje de bienvenida al servicio de autoconsulta.

Se solicita por única vez un correo electrónico de contacto con el usuario y el “Punto de Suministro” de inmueble, que está indicado en el sector izquierdo de la factura de servicio de cada usuario.

Se despliega un MENÚ PRINCIPAL para que el usuario seleccione la acción que desea realizar.

Están disponibles la mayoría de las gestiones que también se pueden efectuar a través de la Oficina Virtual de www.aguassantafesinas.com.ar 

**1.** Mi última factura

**2.** Consulta de deuda

**3.** Pagar factura

**4.** Trámites comerciales

**5.** Inconvenientes en el servicio

**6.** Dejanos tu consulta

**7.** Seleccionar otro punto de suministro

**8.** Seleccionar otro usuario (email)

En cada una de ellas a su vez se despliegan otras opciones para que vaya generando su trámite, reclamo o consulta.

Por cada acción ejecutada, el usuario debe confirmar los datos ingresados, con el fin de disminuir errores.

Una vez que el usuario finalizó su gestión, el sistema vuelve al MENU PRINCIPAL para continuar navegando, si así lo desea.