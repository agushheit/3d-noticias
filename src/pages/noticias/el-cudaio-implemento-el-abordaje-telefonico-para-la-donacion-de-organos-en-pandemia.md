---
category: Agenda Ciudadana
date: 2020-12-06T11:55:10Z
thumbnail: https://assets.3dnoticias.com.ar/cudaio.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: "\tEl CUDAIO implementó el abordaje telefónico para la donación de órganos
  en pandemia"
title: "\tEl CUDAIO implementó el abordaje telefónico para la donación de órganos
  en pandemia"
entradilla: El trabajo cotidiano en los efectores donantes debió adaptarse a las nuevas
  circunstancias de reducción considerable de la circulación y visitas de personas
  en esos ámbitos.

---
Si bien la **legislación vigente considera que es donante toda persona que no haya manifestado lo contrario**, las entrevistas de los coordinadores de CUDAIO con los familiares directos del potencial donante forman parte del procedimiento legal y cotidiano. 

En ellas se informa detalladamente sobre los procesos de donación y trasplante y se busca el acompañamiento de la familia a la decisión del donante, además de la indispensable contención ante la pérdida inesperada.

El protocolo de abordaje telefónico implementado por CUDAIO forma parte de los cursos formativos regulares que realiza la Coordinación Operativa de la institución para todos sus profesionales.

**La donación de órganos y tejidos en Santa Fe tiene un balance positivo**, en un contexto mundial complicado para la actividad. 

**La provincia es la segunda en cantidad de donantes a nivel nacional**, y el Hospital de Emergencias Dr. Clemente Álvarez (HECA), de Rosario, es el efector con más procesos de donación en todo el país.