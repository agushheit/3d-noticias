---
category: Estado Real
date: 2021-04-16T08:26:26-03:00
thumbnail: https://assets.3dnoticias.com.ar/precios-cuidados.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: 'El gobierno de la provincia prorrogó hasta el 31 de mayo de 2021 el programa
  Precios Santafesinos   '
title: 'El gobierno de la provincia prorrogó hasta el 31 de mayo de 2021 el programa
  Precios Santafesinos   '
entradilla: La iniciativa -que incluye 115 productos- es impulsada con cámaras mayoristas,
  supermercados, almacenes y empresas productoras de alimentos.

---
El Ministerio de Producción, Ciencia y Tecnología de la provincia, a través de la Secretaría de Comercio Interior y Servicios, anunció una nueva etapa del programa Precios Santafesinos. La iniciativa -que prevé una prórroga hasta el 31 de mayo de 2021- es impulsada a partir de acuerdos con cámaras mayoristas, supermercadistas, almacenes, kioscos y empresas productoras de alimentos de la provincia, y busca brindar precios de referencia de 115 artículos de primera necesidad a santafesinos y santafesinas.

Durante la presentación, realizada en la ciudad de Rosario, el secretario de Comercio Interior y Servicios, Juan Marcos Aviano, destacó que “continuamos con 115 productos, de 8 rubros distintos, que son los que los supermercados, de acuerdo a sus posibilidades, condiciones y dimensiones, pueden optar. Son productos genéricos, es decir, no establece ni obliga a una marca determinada, sí el precio que debe ser único en toda la provincia”.

“Vamos a hacer una revisión de todos los productos para la siguiente etapa, sabiendo que estamos en un momento muy complejo de la economía y todo lo que es el sistema de precios de los bienes esenciales”, dijo, y remarcó “el esfuerzo que ha hecho el sector privado, ya que la variación del precio promedio de los productos es del 5,6% respecto de las anteriores etapas”.

En ese sentido, el funcionario provincial se mostró “muy conforme de poder continuar con este programa”, y explicó que se va a “relanzar la inscripción y la adhesión de comercios. Necesitamos que haya más productos de Precios Santafesinos en las grandes cadenas de supermercados, y en el nivel medio y el comercio de proximidad. Que podamos estar más presentes con el programa de lo que hoy estamos”.

En rueda de prensa, consultado sobre la inflación en productos esenciales, el secretario recordó que Billetera Santa Fe, junto con Precios Cuidados y Precios Santafesinos, son herramientas que pretenden paliar esa suba. “Desde el gobierno provincial estamos acompañando con inspecciones, con controles, con monitoreos, con el acuerdo mayorista con los tres mercados de frutas y verduras para ver el ritmo de esos productos, que son importantes en la dieta y en la alimentación diaria de las familias”, dijo.

“La Secretaría de Comercio Interior de la Nación está aplicando severas multas en los desequilibrios que se pueden registrar. Y nosotros en la provincia estamos igual: entre marzo y abril ya tenemos 34 imputaciones realizadas a mayoristas y minoristas que no respetan los precios”, concluyó.

Por su parte, el presidente de la Cámara de Supermercados y Autoservicios de Rosario (CASAR), Sergio López, destacó que “empezamos a participar de estos programas que fuimos pioneros en Santa Fe y se están replicando en algunas provincias con acuerdos de precios. Convocamos también a todos los que no están adheridos a este programa. Es hora de que nos comprometamos todos en este sentido porque estamos pasando una situación muy compleja de sostenimiento de las empresas y de los comercios”.

 

Finalmente, el secretario de la Cámara, Mariano Martin, destacó la importancia de la “inyección de dinero mediante Billetera Santa Fe y los programas de Precios Cuidados o Precios Máximos”, y explicó: “Son marcas blancas, y lo que hace el comercio local, junto a la Cámara y a la Secretaría, es establecer acuerdos con la industria regional, que es la que no determina el precio de mercado y es a quien le importa poder entrar en un mercado nuevo, como son los supermercados de Rosario y la región”.