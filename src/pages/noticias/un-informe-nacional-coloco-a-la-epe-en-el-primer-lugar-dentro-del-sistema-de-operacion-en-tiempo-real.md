---
category: Estado Real
date: 2021-07-26T06:15:25-03:00
thumbnail: https://assets.3dnoticias.com.ar/CAMMESA.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Un informe nacional colocó a la EPE en el primer lugar dentro del sistema
  de operación en tiempo real
title: Un informe nacional colocó a la EPE en el primer lugar dentro del sistema de
  operación en tiempo real
entradilla: Se trata del documento periódico que elabora CAMMESA, en el que se destaca
  el trabajo desempeñado por las distribuidoras eléctricas del país.

---
La Empresa Provincial de la Energía (EPE) fue informada como la distribuidora eléctrica del país más eficiente en la indisponibilidad del Sistema de Operación en Tiempo Real (SOTR), de acuerdo al documento elaborado por la Compañía Administradora Mayorista del Mercado Eléctrico Sociedad Anónima (CAMMESA).

La evaluación corresponde al período comprendido entre marzo de 2020 y marzo de 2021 y, en el caso de la EPE, se realizó a partir de las mediciones y los estados de elementos de maniobras de 69 Estaciones Transformadoras AT/MT reportados cada dos segundos.

El titular de la EPE, Mauricio Caussi, señaló que, como resultado del análisis la Indisponibilidad Anual Móvil (IAM) de la Empresa fue de 10 horas y 53 minutos, lo cual marca el esfuerzo sostenido de toda la organización en garantizar la disponibilidad de los medios necesarios para la correcta operación de la red.

Cabe señalar que CAMMESA exige una IAM menor a 43,8 horas, correspondiendo sanciones económicas importantes en caso de incumplimientos.

El SOTR está constituido por el conjunto de sistemas que permiten operar en tiempo real el Sistema Argentino de Interconexión (SADI).

Esto incluye los sistemas de adquisición de datos de los Agentes del Mercado Eléctrico Mayorista (MEM) que participan del SOTR, los medios de comunicación de datos y voz.

**CAMMESA**

Es una empresa privada argentina sin fines de lucro. Sus objetivos principales comprenden la coordinación de los despachos económico-técnicos desde el SADI; la supervisión de la calidad y seguridad de las operaciones del SADI; el monitoreo de las transacciones económicas en los mercados spot y futuro; y la gestión de las operaciones de facturación, cobro y finanzas de los fondos de mercado.

Está compuesta por el Estado nacional, generadoras, transportistas, distribuidoras y grandes usuarios de energía eléctrica.