---
layout: Noticia con imagen
author: .
resumen: Producción de alimentos
category: El Campo
title: El rol del sector cooperativo agropecuario en la producción de alimentos
entradilla: Gobernadores de tres provincias, entre las que se encuentra Santa
  Fe, resaltaron la importancia de las políticas públicas para el sector.
date: 2020-11-14T17:52:11.866Z
thumbnail: https://assets.3dnoticias.com.ar/foto5.jpg
---
Gobernadores de tres provincias destacaron el rol de las cooperativas agropecuarias en la producción de alimentos sustentables y anunciaron políticas públicas para el sector, como la “recuperación del Fondo Cooperativo” en la provincia de Buenos Aires.

“Venimos de cuatro años en los que se les había puesto la lápida a las cooperativas, con el pago de impuesto a las Ganancias; hoy hay medidas concretas, incentivos para el sector cooperativo”, remarcó el gobernador bonaerense, Axel Kicillof, en un panel del Congreso organizado por Coninagro (Confederación Intercooperativa Agropecuaria).

Su colega de Mendoza, Rodolfo Suárez, indicó en tanto que del producto bruto de la provincia el 7% corresponde al sector agropecuario cooperativo y que “si le sumamos la actividad manufacturera llega al 20%”.

Suárez remarcó que esa forma de asociativismo “aumenta su importancia cuando consideramos el empleo que genera y el arraigo”.

El gobernador de Santa Fe, Omar Perotti destacó a su vez la importancia de “llevar al 100% el potencial” de las cooperativas agropecuarias y señaló que en el Presupuesto provincial para 2021 se dejó sin efecto el impuesto inmobiliario rural para campos “menores a 50 hectáreas”.

Perotti aseguró que, además de la menor carga impositiva, el Estado apoya a las cooperativas con “obras hídricas, expansión ferroviaria, articulación con la hidrovía”, así como una mayor transparencia en las operaciones.

Kicillof precisó que, entre las medidas específicas para el sector, la provincia lanzó la Incubadora de Cooperativas Agropecuarias, en la que ya se inscribieron 60 grupos de productores y emprendedores, mientras el fondo Fuerza Solidaria ofrece créditos específicos para proyectos de valor agregado y conectividad de la actividad cooperativa, con una tasa de 25%.

Subrayó luego la “recuperación del Fondo Cooperativo, con $ 144 millones anuales que tenían que aplicarse por ley al sector y se usaron para otras cosas en los últimos años”, y anunció el lanzamiento de la “primera convocatoria para el fondo rotatorio para cooperativas apícolas y pesqueras”. El gobernador bonaerense destacó por último la puesta en marcha del “consultorio técnico”, para que las cooperativas tengan asesoramiento gratuito en trámites ante el Inaes.