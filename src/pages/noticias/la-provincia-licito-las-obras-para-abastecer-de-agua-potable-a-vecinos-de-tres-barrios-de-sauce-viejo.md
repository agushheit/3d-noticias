---
category: Estado Real
date: 2020-12-29T10:49:24Z
thumbnail: https://assets.3dnoticias.com.ar/2912-agua-sauce-viejo.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: La provincia licitó las obras para abastecer de agua potable a vecinos de
  tres barrios de Sauce Viejo
title: La provincia licitó las obras para abastecer de agua potable a vecinos de tres
  barrios de Sauce Viejo
entradilla: Los trabajos demandarán una inversión superior a los 51 millones de pesos
  y beneficiarán a los barrios Jorge Newbery, Altos del Sauce y Loteo Mántaras.

---
La ministra de Infraestructura, Servicios Públicos y Hábitat de la provincia, Silvina Frana, presidió este lunes el acto de apertura de sobres con ofertas económicas de la licitación de la primera etapa de la obra de ampliación del Acueducto Desvío Arijón, que abastecerá de agua a 13.000 vecinos de los barrios Jorge Newbery, Altos del Sauce y el Loteo Mántaras, en la ciudad de Sauce Viejo, trabajos que demandarán una inversión de 51 millones de pesos.

El ramal se abastecerá del acueducto principal que se desarrolla desde la toma ubicada sobre el Río Coronda, en la comuna de Desvío Arijón, hasta la ciudad de Santa Tomé.

Al respecto, Frana comentó que «para nosotros abrir estas licitaciones es muy satisfactorio, luego de un inicio con muchos obstáculos y deudas heredadas, con una bajante histórica que demandó fuertes inversiones», y destacó que la obra«se financia con fondos totalmente de la provincia, gracias a otros de los objetivos del gobernador que fue sanear las cuentas».

«Seguramente, estas son las obras que no se eligen, porque enterrar caños no son las obras que se ven o se publicitan, pero sí, luego, redundan en el bienestar de la gente», concluyó la ministra.

Por su parte, el secretario de Empresas y Servicios Públicos, Carlos Maina, destacó que «hace casi un año estábamos pagando deudas heredadas, y en un año difícil logramos saldarlas; ahora podemos terminar el año con una obra de vital importancia para los vecinos de Sauce Viejo. Ojalá que en el corto plazo, todo los santafesinos tengamos acceso a este servicio básico tan esencial».

Finalmente, el intendente de Sauce Viejo, Pedro Uliambre, manifestó: «Esto es gestión, trabajar en conjunto, una obra que anhelábamos desde hace tiempo. Estoy agradecido con llegar a esta licitación y en poco tiempo saber que se empezará con una obra que es un gran beneficio para los vecinos».

<br/>

<span style="font-family: Helvetica, sans-serif; font-size: 1.2em; font-weight: 700">**LAS OFERTAS**</span>

La obra cuenta con un presupuesto oficial de $ 51.018.349,73 (con base al mes de marzo) y un plazo de ejecución de 8 meses. El tramo proyectado descargará en una cisterna de hormigón armado a ejecutar de 1000 m³ de capacidad de reserva, desde la cual se impulsará mediante bombas centrífugas hacia un tanque elevado existente de 150 m³ de capacidad. Se incluyen instalaciones accesorias y cerco perimetral.

A la licitación se presentaron seis empresas oferentes: Empresa Constructora Pilatti cotizó la suma de $ 69.476.525,08; MEM Ingeniería S.A. cotizó $ 69.217.156,50; Brumont S.A. ofertó $ 67.993.397,22; Winkelmann S.R.L. por $ 65.386.837,60; Cocyar S.A. por $ 76.488.279,36; y EFE Construcciones de Carlos A. Fierro cotizó $ 68.800.000.