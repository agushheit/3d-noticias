---
category: Agenda Ciudadana
date: 2022-05-24T08:42:47-03:00
thumbnail: https://assets.3dnoticias.com.ar/municipalidad.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: Servicios municipales previstos para el feriado del 25 de Mayo
title: Servicios municipales previstos para el feriado del 25 de Mayo
entradilla: Durante este miércoles, por el Día de la Revolución de Mayo,  la recolección
  de residuos será de forma habitual. Asimismo, se detallan los horarios del Cementerio
  y la frecuencia del transporte público.

---
  
Este miércoles 25 de mayo es feriado nacional en conmemoración del 212° aniversario de la Revolución de Mayo de 1810. En ese sentido, la Municipalidad informa los horarios de los servicios de recolección de residuos, cementerio, transporte público y estacionamiento medido, entre otros.

En cuanto a la recolección de residuos, las empresas prestarán el servicio con normalidad en toda la ciudad. Vale aclarar que los Eco Puntos permanecerán abiertos en los horarios habituales.

**Colectivos y estacionamiento**

En cuanto al transporte público de pasajeros, funcionará con la frecuencia de domingo. Por otra parte, el Sistema de Estacionamiento Medido (SEOM) no estará operativo.

**Cementerio**

Con respecto a la necrópolis municipal, el horario de visita será de 8 a 15; mientras que para las inhumaciones, la atención será de 8 a 11.30.

**Mercado Norte**

El Mercado Norte, ubicado en la intersección de Santiago del Estero y Urquiza, abrirá este miércoles, en el horario de 9 a 13.

**Juzgado de Faltas**

En este caso, las oficinas ubicadas en avenida Presidente Perón y bulevar funcionarán de 8 a 13.