---
category: Estado Real
date: 2021-07-31T06:00:12-03:00
thumbnail: https://assets.3dnoticias.com.ar/PEROTTISANJAVIER.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: El Gobernador Perotti presidió este viernes la licitación para construir
  26 viviendas en San Javier
title: El Gobernador Perotti presidió este viernes la licitación para construir 26
  viviendas en San Javier
entradilla: La provincia invertirá más de $72 millones, y el plazo de obra es de ocho
  meses. “La tierra, el techo y el trabajo afirman y consolidan la unidad familiar
  y el arraigo”, resaltó en el acto el mandatario santafesino.

---
El gobernador Omar Perotti encabezó este viernes la licitación para la construcción de 26 viviendas de dos y tres dormitorios, en la ciudad de San Javier. Las nuevas unidades cuentan con un presupuesto oficial de $72.457.456 y un plazo de obra de ocho meses.

Durante el acto, que se realizó en la Escuela de Enseñanza Media N°242 General Manuel Belgrano, Perotti afirmó que “queremos acercar a más santafesinos y santafesinas a una vivienda. Por eso nos propusimos trabajar en dar respuestas a las realidades que nos encontramos. Obras paradas a nivel nacional, en algunos casos planes de 2014 o 2015. Y con la decisión del Presidente, Alberto Fernández, estamos terminando de resolver cada una de esas situaciones e instrumentando la posibilidad de concluir unas 600 viviendas que habían quedado casi terminadas y otras con un grado de avance muy dispar”, dijo el gobernador.

Además, remarcó que “cuando está la posibilidad de una vivienda hay que remover todos los obstáculos. Vamos a seguir con nuevas licitaciones de viviendas, de acá a fin de año, en todo el territorio provincial, tanto con fondos de la Dirección Provincial de Viviendas como con fondos nacionales, para acercar a más santafesinos a una vivienda, porque eso moviliza el empleo y el trabajo”.

Asimismo, el mandatario provincial destacó que “tierra, techo y trabajo afirman y consolidan la unidad familiar y el arraigo. Que la gente pueda tener posibilidades de desarrollar su vida en los lugares donde nació y donde desea progresar, es para nosotros un tema central”.

“Creemos profundamente en las posibilidades de desarrollo de esta región, en sus oportunidades productivas y turísticas. Por eso, vamos a mejorar su infraestructura, para consolidar la oferta al visitante, y así mucha gente la tendrá como destino”, concluyó Perotti.

Acompañaron al gobernador, la ministra de Infraestructura, Servicios Públicos y Hábitat, Silvina Frana; el intendente de la ciudad de San Javier, Mario Migno; y el senador por el departamento San Javier, José Baucero.

**Derecho al hábitat**

Seguidamente, la ministra Frana recordó que “es la tercera vez que venimos a San Javier para trabajar y dar respuestas a lo que se llama «derecho al hábitat», que es un derecho consagrado por las Naciones Unidas”.

La ministra agregó que el gobierno de la provincia, “en línea con el gobierno nacional, con el que trabaja codo a codo de manera permanente, puso como faro de la gestión una gran inversión en obra pública, en todos los aspectos, y haciendo especial hincapié en dar una respuesta al derecho al hábitat”.

Frana mencionó que en dicha ciudad “venimos concretando obras de otra gestión, porque entendemos que nunca debemos perder de vista que el objetivo es trabajar para nuestra gente”, y en ese sentido agregó que “estamos haciendo muchas inversiones en lugares en los que antes no se hacían, generando las condiciones para que la gente pueda desarrollarse en el lugar donde nació si así lo desea, dándoles respuestas con hechos concretos”.

Por su parte, el intendente Migno manifestó que “hoy es un día de alegría y agradecemos muchísimo haber sido escuchados y que se aprovechen los terrenos para que se puedan construir más viviendas, las cuales son tan necesarias para los sanjavierinos”.

**Las ofertas**

En la oportunidad se presentaron 5 oferentes: ICA SRL, que cotizó $69.535.255; Tecnología aplicada SRL, que ofertó $74.993.400; una UTE integrada por MT SRL y Orion Ingeniería SRL, con la suma de $75.590.000; EFE Construcciones, por $96.000.000; y finalmente Tecsa SA, que cotizó $104.263.356.

**Las viviendas**

De las 26 unidades habitacionales, 19 serán de dos dormitorios (de 55m2), cinco de tres dormitorios (66,5 m2) y las dos restantes serán destinadas al cupo de personas con discapacidad. Además, se realizarán obras de infraestructura básica como provisión de energía eléctrica, red de alumbrado público, agua potable, veredas peatonales y rampas, arbolado y preparación de espacios verdes.

Estarán ubicadas en la intersección de H. Yrigoyen, calle Pública, Hermana Carina y las calles Los Jesuitas, Quiloazas y General Lavalle.

**Visita al hospital**

Durante su visita a la ciudad de San Javier, el gobernador Omar Perotti también recorrió el hospital Dr. Guillermo Rawson y observó las obras de construcción de un módulo de terapia intensiva para casos de Covid-19, la ampliación de la nueva guardia y la refacción integral.

Al respecto, mandatario santafesino destacó el “importante grado de avance de la obra”, que cuenta con un presupuesto oficial de $207.255.905 y un plazo de ejecución de 150 días.

Asimismo, el gobernador agradeció “a todo el personal de salud el esfuerzo, la preocupación y la atención de todos y cada uno de los sanjavierinos y de la gente de la región que necesitó atención”.

El efector de salud atiende a una población de aproximadamente 32.000 habitantes y del mismo dependen 10 centros de atención primaria de la región. Ante la urgencia de poder contar con el área de internación Covid-19 en funcionamiento, está previsto que el nuevo Módulo de Terapia Intensiva se ejecute dentro de los primeros 45 días del plazo de ejecución de la obra, para su habilitación y puesta en servicio.

Además, Perotti participó de la Apertura de sobres para la compra de un mamógrafo convencional, por un monto de u$s 65.000, que se llevó a cabo en el mismo hospital y para la cual se presentaron seis empresas oferentes. Asimismo, con fondos provinciales se adquirió el digitalizador de Rayos X, y dicho mamógrafo se anexará a este equipo.

Por último, el gobernador de la provincia recorrió la obra de alteo y construcción de una alcantarilla, alumbrado frente y dársenas en el camino de ingreso a la Sociedad Rural de San Javier, en el kilómetro 150 de la Ruta Provincial Nº 1. Los trabajos fueron realizados por la Dirección Provincial de Vialidad.

**Presentes**

También participaron del acto el director provincial de Relaciones Laborales, Mateo Marelli; el secretario de Hábitat, Urbanismo y Vivienda, Amado Zorzón; el director provincial de Vivienda y Urbanismo, Jose Manuel Kerz; las directora y vicedirectora de la Escuela de Enseñanza Media Nº242 Manuel Belgrano, Griselda Zilli y Norma Siviero, respectivamente; y la directora del Hospital Guillermo Rawson de San Javier, Silvia Bisio, entre otros.