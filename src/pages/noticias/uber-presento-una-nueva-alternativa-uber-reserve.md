---
category: Agenda Ciudadana
date: 2022-11-09T08:51:42-03:00
thumbnail: https://assets.3dnoticias.com.ar/ride-6325906_1280.png
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: 'Uber presento una nueva alternativa: Uber reserve'
title: 'Uber presento una nueva alternativa: Uber reserve'
entradilla: Esta herramienta permitirá a los usuarios reservar viajes de UberX y Uber
  Comfort entre 30 días y hasta dos horas antes, ayudando a planificar los tiempos
  para moverse en la ciudad.

---
Uber anunció el lanzamiento de Uber Reserve en Argentina, Paraguay y Uruguay. 

Esta herramienta permitirá a los/as usuarios/as reservar viajes de UberX y Uber Comfort entre 30 días y hasta dos horas antes, ayudando a planificar los tiempos para moverse en la ciudad.  
  
Con Uber Reserve, tanto quienes viajan como quienes conducen las unidades, podrán  planificar con anticipación, personalizando su experiencia de acuerdo a sus preferencias y necesidades para moverse en la ciudad. 