---
category: Agenda Ciudadana
date: 2021-05-31T06:00:10-03:00
thumbnail: https://assets.3dnoticias.com.ar/SACNUN.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Retrasar las elecciones "protege la salud y la democracia", dijo la senadora
  Sacnun
title: Retrasar las elecciones "protege la salud y la democracia", dijo la senadora
  Sacnun
entradilla: El proyecto para postergar un mes las PASO obtuvo dictamen por unanimidad
  en la comisión que preside la legisladora santafesina.

---
La senadora nacional por el Frente de Todos de Santa Fe, María de los Ángeles Sacnun, destacó que el proyecto de ley que propone retrasar un mes las elecciones Primarias, Abiertas, Simultáneas y Obligatorias (PASO) y las legislativas generales "busca proteger la salud de los argentinos en medio de la pandemia por coronavirus y el pleno ejercicio de sus derechos políticos".

"Es importante el proyecto porque íbamos a atravesar el acto electoral, tanto de las PASO como de las generales, en medio de un clima complicado, por el frío, en el que aumentan normalmente las enfermedades respiratorias y podría generar también un aumento de casos de coronavirus en el contexto de pandemia", explicó la legisladora nacional en una entrevista con Télam.

Sacnun ponderó que el "despacho haya salido con las firmas" también de los integrantes del interbloque opositor de Juntos por el Cambio.

El proyecto para postergar las elecciones obtuvo dictamen por unanimidad en la comisión que preside la santafesina y está listo para ser puesto a consideración del pleno de la Cámara alta esta semana que se inicia.

"Es un proyecto que viene con un alto consenso desde la Cámara de Diputados", recordó Sacnun y mencionó que "incluso el ministro del Interior, (Eduardo) 'Wado' De pedro reconoció la tarea que se fue realizando con los apoderados de las distintas fuerzas políticas y con la Cámara Nacional Electoral".

En ese sentido, señaló que "el hecho de consensuar esta postergación de las PASO de agosto a septiembre como las generales de octubre a noviembre, permite que el acto electoral se lleve adelante en meses menos fríos y que esto permita que en las escuelas donde se lleve adelante la elección se pueda garantizar la circulación de aire y no exista amontonamiento".

Sacnun dijo que "había que compatibilizar el ejercicio de ambos derechos: por un lado la defensa del derecho a la salud y por otro el pleno ejercicio de los derechos políticos" y expresó que "ambos derechos están protegidos"

Por otro lado, la senadora santafesina cuestionó la propuesta de la oposición de incluir en el proyecto que está listo para ser debatido esta semana, la idea de reformar la Ley Electoral para introducir el uso de la Boleta Única Electrónica, tal como reclamaba la oposición, al argumentar que ese instrumento "permite votar individualidades en lugar de proyectos políticos".

"Cualquier cambio en el sistema electoral será un debate que habrá que dar en otro momento y no en un año electoral", enfatizó la legisladora en sintonía con lo que había pronunciado el ministro del Interior ante los senadores el miércoles pasado.

No obstante, indicó que "la boleta única tiene ciertas particularidades, como garantizar que todos los partidos políticos tengan acceso a que su voto esté garantizado" y a que "cada votante tenga la posibilidad de elegir a su candidato sin tener que depender del reparto de votos del aparato político que cada fuerza pueda tener en el territorio".

"Se garantiza que cada boleta esté en el lugar de votación. Y evita que alguno cometa la picardía de robarse las boletas", remarcó la legisladora nacional.

Sin embargo, consideró que la boleta única, que ya funciona en las provincias de Córdoba, Salta y Santa Fe, "rompe con lo que los militantes políticos consideramos como la pertenencia a un espacio político".

"La boleta única permite votar individualidades y no proyectos políticos. Creo que hay que reivindicar a los proyectos políticos por encima de las individualidades", sentenció la presidenta de la Comisión de Asuntos Constitucionales de la Cámara de Senadores.