---
category: Agenda Ciudadana
date: 2021-12-18T06:00:52-03:00
thumbnail: https://assets.3dnoticias.com.ar/moralesucr.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Gerardo Morales fue consagrado como nuevo presidente de la UCR
title: Gerardo Morales fue consagrado como nuevo presidente de la UCR
entradilla: "El gobernador de Jujuy comandará el Comité Nacional del radicalismo tras
  ser elegido por el plenario de delegados reunidos en la histórica sede partidaria
  de la calle Alsina.\n\n"

---
El gobernador jujeño Gerardo Morales fue consagrado por unanimidad como nuevo presidente del Comité Nacional de la UCR por los próximos dos años, en una reunión plenaria desarrollada en la histórica sede partidaria de la calle Alsina, en el que ratificó su voluntad de disputarle a sus socios del PRO la hegemonía de Juntos por el Cambio hacia el 2023.  
  
El resultado de la elección para que el mandatario jujeño reemplace a Alfredo Cornejo al frente del radicalismo fue fruto de una negociación encarada por el propio Morales y el senador nacional Martín Lousteau, quien representa al sector interno UCR-Evolución, minoritario en el partido y que aspiraba también a ocupar el máximo cargo partidario.  
  
Tras una votación unánime por parte de los 94 delegados reunidos en la sede porteña de la UCR, Morales se quedó con la titularidad de la UCR y también retendrá la vicepresidencia primera de la Mesa Ejecutiva del partido que estará en manos de la bonaerense María Luisa Storani, mientras que la vicepresidencia segunda será ocupada por el propio Martín Lousteau.  
  
**El discurso de la asunción**

  
"Había muchos que pensaban que este plenario iba a hacer de sillazos, hemos demostrado que con diálogo hemos podido lograr esto", dijo Morales, al inicio de su discurso una vez consagrado como el nuevo jefe de la UCR.  
  
Enseguida, el gobernador -quien, al igual que Lousteau y el mendocino Alfredo Cornejo, tiene aspiraciones presidenciales hacia el 2023- envió un mensaje a sus socios del PRO en Juntos por el Cambio (JxC) y ratificó la voluntad del radicalismo de liderar la coalición hacia el 2023.  
  
"No queremos ser dueños de la coalición, porque no corresponde pero sí vamos a ser parte de la toma de decisiones y de la construcción del proyecto que espera la República Argentina que lidere Juntos por el Cambio, pero con un radicalismo fuerte, vigoroso y territorial en todo el país"Gerardo Morales

  
Afirmó que luego del resultado electoral de los comicios legislativos de noviembre "cambiaron las condiciones dentro de JxC" y hasta aventuró que, si Facundo Manes hubiese sido en los pasados comicios cabeza de lista a la Cámara de Diputados de la Nación, en lugar del macrista Diego Santilli, "también ganábamos en la provincia de Buenos aires".  
  
A renglón seguido, evaluó: "Hasta acá veníamos en una coalición con un rol casi secundario. En los años que le tocó gobernar a Cambiemos casi que fue un gobierno del PRO", lo que despertó el aplauso cerrado de los delegados nacionales que participaron del plenario.  
"Han cambiado las cosas", advirtió Morales y se mostró convencido que "el resultado del radicalismo en la última elección permitió lograr una relación simétrica (con el PRO) en Juntos por el Cambio y dejar de ser furgón de cola".  
  
Enfático, Morales sostuvo: "Dejamos de ser un radicalismo irrelevante para pasar a ser un radicalismo que va a aportar. Contundentemente las cosas han cambiado y cambiarán de ahora en más".  
  
Y avisó: "No queremos ser dueños de la coalición, porque no corresponde pero sí vamos a ser parte de la toma de decisiones y de la construcción del proyecto que espera la República Argentina que lidere Juntos por el Cambio, pero con un radicalismo fuerte, vigoroso y territorial en todo el país".  
  
"Este plenario, con diálogo y consensos -continuó- le demuestra a la República Argentina que tenemos la capacidad de unir al pueblo argentino", enfatizó el flamante presidente de la UCR.  
  
En una afirmación que pareció la de un candidato presidencial, Morales se mostró convencido de la necesidad de "dar un debate federal, que se construya desde lo más profundo del país".  
  
"El día que resolvamos los problemas estructurales de la economía de la periferia hacia el centro resolveremos el problema de la inflación. Ninguna provincia es inviable, todas tienen capacidad productiva", señaló y remarcó desde la UCR "gestionamos tres gobernadores (Jujuy, Corrientes y Mendoza), 600 intendentes. Cuantas veces hablamos con un Gobierno nacional que no escucha".  
  
Por otra parte, Morales pidió a sus correligionarios y socios de JxC "hacer evaluación autocrítica sobre la capacidad de respuestas que le damos a la sociedad".  
  
Fue allí que les marcó la cancha a los dirigentes del PRO, como Patricia Bullrich y Mauricio Macri que empujan una alianza con los libertarios, Javier Milei y José Luis Espert hacia el 2023.  
  
"No es casual la irrupción de esas voces que dicen que se ´vayan todos' como la de los Milei, que salen por derecha o por izquierda, si no hacemos un análisis autocrítico para resolver la repuesta a la gente", enfatizó.  
  
**El Plenario**

Como lo establece el protocolo para este tipo de plenarios, Cornejo abrió la sesión para dar un balance de gestión sobre los dos últimos años que estuvo frente del Comité Nacional.  
  
En su discurso, destacó la "muy buena performance" que la UCR tuvo en las legislativas de noviembre, que -dijo- le otorgó al partido una postura de "mayor equilibrio" frente a sus aliados del PRO en JxC.  
  
"Tuvimos una performance exitosa en casi todas las provincias, donde mejoramos nuestra representación territorial y legislativa respecto al 2019", remarcó.  
  
Cornejo abundó que la pasada elección "revitalizó" a la UCR y evaluó que el partido logró así "contribuir a un equilibrio de poder" en el Congreso Nacional que, sostuvo, "se vio claramente reflejado en la votación de esta mañana en la Cámara de Diputados de la Nación", donde fue rechazado [**el proyecto de Presupuesto 2022, enviado por el Poder Ejecutivo**](https://www.telam.com.ar/notas/202112/578172-camara-de-diputados-sesion-proyecto-presupuesto-2022.html).  
  
En otro tramo de su discurso, Cornejo aprovechó -como después lo hizo Morales- para recordarle al PRO la poca gravitación que tuvo su partido durante los cuatros años de la administración de Mauricio Macri y dejó en claro que los boina blanca buscarán disputarle la hegemonía de JxC al PRO de cara al 2023.  
  
"En estos dos años nos tocó volver a la oposición y nos tocó en los anteriores ser oficialismo en un gobierno en el que no tuvimos una participación plena y mucho menos como partido", dijo y agregó: "En estos dos años que estuvimos en la oposición el radicalismo logró equilibrar la ecuación de poder dentro de Juntos por el Cambio".  
  
A su turno, el senador nacional Martín Lousteau, quien logró la vicepresidencia segunda del Comité Nacional, destacó los acuerdos alcanzados entre los distintos sectores del partido en pos de lograr una lista de unidad para la renovación de los cargos en la nueva conducción nacional de la UCR.  
  
La "fumata blanca" llegó en las últimas horas luego de arduas negociaciones entre Morales y Lousteau, en las que también tuvieron una importante participación dirigentes como Enrique "Coti" Nosiglia, Daniel Angelici, Ángel Rozas y Ernesto Sanz, según confiaron fuentes partidarias a Télam.

Esas tratativas incluyeron la posibilidad de una futura reunificación del bloque en la Cámara de Diputados, fracturado días atrás con la decisión de Lousteau de no compartir la bancada que preside el cordobés Mario Negri.  
  
A ello se refirió Cornejo en declaraciones a la prensa realizadas antes del plenario: "Estamos conversando para que en algunos meses estemos unificando el bloque de diputados".  
  
"Es un muy buen día hoy para la Unión Cívica Radical. Cada uno contribuyendo complementariamente desde sus capacidades", dijo Lousteau, quien había amenazado con judicializar la elección de hoy del titular del Comité Nacional, cuestionando la legitimidad de los mandatos de algunos de los delegados, sobre todo aquellos de las provincias intervenidas o los que consideraban que tenían el mandato vencido.  
  
Superadas al menos hasta el momento las discusiones internas, el senador cambió el tono confrontativo de los pasados días y afirmó: "Somos un partido fuerte con historia, con gran representación legislativa".  
  
Como Morales y Cornejo, el senador evaluó que "la tarea" que tiene por delante el partido es hacer un "radicalismo más fuerte adentro (de JxC) que se pueda proyectar hacia afuera", de cara al 2023, porque "hoy tenemos la posibilidad de recuperar una agenda reformista".  
  
"Bienvenida la discusión, hagamos un mejor radicalismo. Tenemos que hablar sobre el futuro, tenemos que saber que queremos ser, como comunicarlo para enamorar a la sociedad. Iniciar una conversación ya", enfatizó.