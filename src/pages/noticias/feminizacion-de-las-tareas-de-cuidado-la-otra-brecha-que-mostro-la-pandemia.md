---
category: La Ciudad
date: 2021-10-10T06:00:52-03:00
thumbnail: https://assets.3dnoticias.com.ar/mujeres.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'Feminización de las tareas de cuidado: la otra brecha que mostró la pandemia'
title: 'Feminización de las tareas de cuidado: la otra brecha que mostró la pandemia'
entradilla: Un curso dictado por la Municipalidd de Santa Fe se convierte en una buena
  ocasión para reflexionar sobre una problemática que se debate desde hace décadas
  y que la crisis sanitaria mostró en toda su dimensión. 

---
Es posible que antes de la pandemia por Covid-19, si se observaba el interior de cualquier transporte público, podía encontrarse a buena parte del pasaje femenino planificando la jornada en casa, si era de mañana, o la cena y las tareas escolares de hijos e hijas, si la postal era de la última hora de la tarde.

 Durante el peor momento de la crisis sanitaria, la población que utilizaba el transporte público fue sensiblemente menor debido al Aspo, primero, y la Dispo, después. Pero la planificación de las tareas domésticas no mermaron. Al contrario, dejaron de ser remotas para volverse bien cercanas, en el mismo hogar. Con infancias resguardadas, clases presenciales, personas mayores o de riesgo puertas adentro y restricciones en la circulación para evitar contagios, las actividades de cuidados se multiplicaron y recayeron mayoritariamente sobre las mujeres; y cuando a esa rutina se le sumó el teletrabajo la situación se volvió aún más compleja y la carga, más pesada.

 Este escenario puso de relieve un tema que vienen debatiendo desde hace décadas la academia y los movimientos sociales, y mucho después se incorporó a las políticas púbicas: la feminización de las tareas de cuidado.

 Marisa Fournier , junto con Carla Zibecchi -ambas sociólogas e investigadoras en temas de género- dieron apertura al curso de Formación introductoria e integral en cuidados que organizó la Municipalidad de Santa Fe a través de la Secretaria de Políticas de Cuidados y Acción Social, a cargo de Victoria Rey, y que tendrá cinco encuentros el 29 de septiembre, el 6, 13 y 20 de octubre y el 3 de noviembre. La mayoría de las 400 personas que participaron el miércoles último de esa jornada fueron, como era de esperar, mujeres.

 - El de los cuidados es un tema que se hizo más visible durante la pandemia, pero está en la agenda desde hace mucho más tiempo-, planteó este diario a Fournier a quien pertenece la reflexión que encabeza esta nota.

 - Es una cuestión que se viene anunciando en el feminismo desde la década del '60 como la falta de reconocimiento del trabajo que, en forma predominante, realizan las mujeres sobre todo al interior de los hogares. Pero la cuestión de los cuidados en el Cono Sur tiene que ver con un sesgo económico. Cuando hablamos de la crisis de los cuidados estamos aludiendo a esta situación actual de la imposibilidad de sostener la vida en las mejores condiciones posibles de manera intergeneracional y respetando los cuidados de todas y todos, incluidas las mujeres que son las que más cuidan.

 - ¿Qué dejó en evidencia la pandemia cuando los efectos de ese repliegue hacia el interior de los hogares recayó en forma mayoritaria en las mujeres?

 - A mi entender la pandemia puso en evidencia la crisis de los cuidados, la idea de poner sobre el tapete la idea de la interdependencia y la codependencia. Cuando decimos que de aquí salimos, todas y todos es así, pero la pandemia puso en evidencia que hay una interdependencia humana respecto de la naturaleza que es ineludible: o miramos eso o nos hundimos todos.

 Y en los sectores populares lo que pasó fue que se enrostró claramente la cuestión alimentaria porque cuando las personas no pueden salir a trabajar, por más que sean trabajos precarios, no hay ingresos. Y si no hay ingresos no hay dinero ni siquiera para comer.

 La otra cuestión son los hogares estallados porque la niñez estuvo dentro y a esto se le sumó el teletrabajo. Allí hay una sobrecarga enorme de trabajo, sobre todo para las mujeres madres que no tenían tiempo para hacer otra cosa.

 Otro tanto pasó con las personas adultas mayores: viviesen o no en los hogares de hijos o hijas, esto incrementó la carga de cuidado. A mi entender es muy importante empezar a reconocer el cuidado como un trabajo. Sea comunitario o dentro del hogar, a nivel mercantil o como fuere, son tareas de cuidado sin las cuales no podríamos sobrevivir.

 **PERFIL**

Marisa Fournier es Lic. en Sociología de la UBA, con estudios de posgrado en Economía Social y Género, investigadora y docente en el Instituto del Conurbano (Universidad Nacional General Sarmiento), especialista en el estudio de cuidados en ámbitos comunitarios.

 - La expectativa de vida aumenta pero no siempre se llega en buen estado de salud a esa edad adulta mayor, y no cualquier persona puede hacerse cargo de esa tarea. Eso abre una fuerza de trabajo importante.

 - Es que los cuidados son fuente de trabajo importantísima, ya sea porque se requiere en hogares de doble proveedor (dos personas que trabajan por dinero para comprarse lo que necesitan) o porque hay más hogares con monoparentalidad, por ejemplo, de mujeres a cargo de sus hijos e hijas. Se cree que siempre hay una mujer madre cuidando y eso no está sucediendo por cambios culturales, cambios económicos o lo que sea. Entonces, necesitamos realmente capacitar para cuidar y cuidar a los que cuidan con buena remuneración, con los derechos asociados a esa tarea.

La preparación de los alimentos, esa práctica vital que en muchos barrios constituye la puerta a la supervivencia, está en manos de mujeres.Foto: Mauricio Garín.

 **PARA SEGUIR LEYENDO**

\- "Cuidados y mujeres en tiempos de COVID-19: la experiencia en la Argentina"

\- "Cuidados en América Latina y el Caribe en tiempos de COVID-19. Hacia sistemas integrales para fortalecer la respuesta y la recuperación"

 **Mayoría mujeres**

 Las tareas de cuidado quedan mayoritariamente en manos femeninas; según la Organización Internacional del Trabajo (OIT), mujeres y niñas realizan más de las tres cuartas partes del trabajo de cuidados no remunerado en el mundo y constituyen dos tercios de la fuerza laboral remunerada del cuidado. Así también son ellas las que capacitan y se capacitan. Así quedó en evidencia en la convocatoria realizada por la Municipalidad de Santa Fe para el curso de formación que se inició la semana pasada en forma virtual.

 Victoria Rey es Secretaria de Políticas de Cuidados y Acción Social y fue anfitriona del encuentro del que participaron unas 400 personas, la mayoría mujeres, muchas integrantes de organizaciones, instituciones y espacios comunitarios vinculados a los cuidados en los distintos barrios de la ciudad, trabajadoras y trabajadores de las estaciones municipales y de casas particulares o en tareas de cuidado en domicilio.

 "En la gran mayoría de los casos lo que sucede es que las mujeres realizan distintas tareas de cuidado tanto en el ámbito doméstico como en el comunitario (copas de leche, merenderos, espacios educativos, salud). Claramente el alto componente de mujeres también nos habla de esta característica histórica que tiene la feminización de los cuidados", reflexiona.

 "Lo que hizo la pandemia, aporta Rey, fue poner este tema en agenda cuando todo quedó centralizado al interior de cada hogar con el aislamiento y la cuarentena. Entonces, lo que antes se podía resolver puertas afuera, 'desfamiliarizando' algunos de los cuidados a través de la educación, los jardines, instituciones para personas mayores o con discapacidad, volvió a recaer en las familias. Y en el caso particular de los cuidados la mayor carga fue para las mujeres".

​"La pandemia contribuyó a hacer visible un tema que tiene mucho tiempo de discusión y por eso la Municipalidad y el intendente Emilio Jatón tomaron la iniciativa de generar esta secretaría. Porque entendemos que es un tema que tiene que abordar el Estado con políticas públicas para disminuir esa brecha de desigualdad social y alivianar la carga de esas familias que muchas veces no tienen cómo resolverlo".

 