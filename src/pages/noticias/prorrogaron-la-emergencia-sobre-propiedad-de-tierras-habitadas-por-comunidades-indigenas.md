---
category: Agenda Ciudadana
date: 2021-11-19T06:00:38-03:00
thumbnail: https://assets.3dnoticias.com.ar/PUEBLOS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Prorrogaron la emergencia sobre propiedad de tierras habitadas por comunidades
  indígenas
title: Prorrogaron la emergencia sobre propiedad de tierras habitadas por comunidades
  indígenas
entradilla: 'A través de un DNU firmado por el presidente Alberto Fernández también
  se suspende por 48 meses la ejecución de sentencias sobre los litigios. '

---
El presidente Alberto Fernández prorrogó este jueves por cuatro años la emergencia en materia de posesión y propiedad de las tierras que tradicionalmente ocupan las comunidades indígenas originarias del país, por medio de un Decreto de Necesidad y Urgencia (DNU) a través del cual también se suspende, por el mismo plazo, la ejecución de sentencias sobre los respectivos litigios.  
  
Mediante el decreto 805, publicado en el Boletín Oficial, el Poder Ejecutivo revalidó los plazos que fueron establecidos en la ley 26.160, y que en su oportunidad fueron extendidos por las leyes 26.554, 26.894 y 27.400, sancionadas a tales efectos.  
  
Al respecto, la portavoz presidencial, Gabriela Cerruti, señaló que "la prórroga de esta ley se realizó porque dicha extensión tenía media sanción del Senado y despacho de comisión de Diputados, pero como vencía hubo que hacer la prórroga por DNU antes del 23 de noviembre".  
  
En ese sentido, el Lonko (jefe) de la Confederación Mapuche de Neuquén, Jorge Nawel, comentó a Télam que distintas organizaciones indígenas del país están en contacto para definir "una estrategia en común" respecto al DNU presentado por el Presidente de la Nación.

> "La decisión sería si nos quedamos parados con el DNU presidencial o presionamos para que en la sesión del 23 de noviembre, último día de vigencia de la ley, se logre ratificar la prorroga por ley"Jorge Nawel, Lonko Mapuche de Neuquén

  
La decisión en cuestión sería, si "nos quedamos parados con el DNU presidencial o presionamos para que en la sesión del 23 de noviembre, último día de vigencia de la ley, se logre ratificar la prorroga por ley", agregó Nawel.  
  
Por su parte, la representante legal de la Asociación de Derecho Indígena, Silvina Ramírez, expresó a esta agencia que la prórroga por decreto del Poder Ejecutivo "no era ni lo deseable ni lo esperable", sino que desde las comunidades y quienes acompañan la lucha por sus derechos territoriales estaban esperando que se diera por medio del debate parlamentario, "que fortalecería los derechos indígenas y discutir algo más que una ley de emergencia, sino una ley de propiedad comunitaria indígena".  
  
Sin embargo, la abogada marcó que dado el escenario tan complejo, en medio de debates alrededor de las recuperaciones territoriales por mapuches en la Patagonia y considerando que la ley vence en pocos días, "no podíamos dejar que las comunidades estuvieran expuestas en una situación tan frágil cuando la ley perdiera vigencia".  
  
"Desde esa perspectiva y siendo totalmente pragmáticos, un DNU era la única respuesta frente al no debate en Diputados", añadió.  
  
En esa línea, la exsenadora y actual presidenta del Instituto Nacional de Asuntos Indígenas, Magdalena Odarda, destacó la importancia de la ley 26.160, que el DNU presidencial prorroga, y afirmó: "Es una ley valiente porque, ante los grandes intereses que existen por la tierra, protege a las comunidades indígenas de desalojos de su territorio, mientras se lleva adelante el proceso del relevamiento".  
  
Al respecto, Cerruti aclaró: "Esta norma habla de un reordenamiento territorial que se está llevando a cabo y que no está concluido", y además consideró que "el reordenamiento territorial es un espacio de diálogo para la cuestión de los pueblos originarios que reclaman derechos sobre algunos territorios".  
  
"En muchas provincias se está avanzando mucho con este proceso. En algunas más, en otras menos, pero existen las herramientas institucionales para que los gobiernos provinciales se sienten con los representantes legítimos de esos pueblos", apuntó.  
  
Sobre los hechos de violencia ocurridos en regiones de la Patagonia, sostuvo: "Repudiamos todo hecho de acción violenta y ayudamos a los gobiernos provinciales a prevenirla y a contenerlas y perseguirlas cuando estas suceden".  
**  
Detalles del decreto 805**

La normativa sancionada por el Presidente, y firmada por todos los ministros del Gabinete, suspende por "el plazo de emergencia declarada la ejecución de sentencias, actos procesales o administrativos, cuyo objeto sea el desalojo o desocupación de las tierras", y establece que "la posesión debe ser actual, tradicional, pública y encontrarse fehacientemente acreditada".  
  
Asimismo, se fija que "durante la vigencia del plazo de la emergencia declarada, el Instituto Nacional de Asuntos Indígenas (INAI) deberá realizar el relevamiento técnico-jurídico-catastral de la situación dominial de las tierras ocupadas por las comunidades indígenas, dar intervención al Estado Provincial y los Estados Municipales implicados y, en caso de corresponder, a la Administración de Parque Nacionales".  
  
Las autoridades también deberán también "promover las acciones que fueren menester con el Consejo de Participación Indígena, los Institutos Indígenas Provinciales, Universidades Nacionales, Provinciales y Municipales, Organizaciones Indígenas y Organizaciones No Gubernamentales".  
  
También se dispone la asignación "para cada uno de los cuatro ejercicios presupuestarios que se aprueben con posterioridad al presente decreto de un crédito de $ 290.000.000 destinados a la atención del Fondo Especial" contemplado en la Ley 26.160.  
  
El INAI presentará, a partir del año 2022, y antes del 30 de noviembre de cada año, ante el Congreso "un informe anual que detalle el estado de avance del proceso de relevamiento técnico-jurídico-catastral de la situación dominial de las tierras ocupadas por las comunidades indígenas, conforme lo establece el artículo 3º de la Ley Nº 26.160, el cual deberá incluir información georreferenciada sobre las comunidades relevadas y el proceso de relevamiento e indicará el porcentaje de avance", se establece en el DNU.

La Ley 26.160 se sancionó en el marco de lo previsto en el artículo 75, inciso 17 de la Constitución Nacional, que reconoce, entre otros aspectos, "la preexistencia étnica y cultural de los pueblos indígenas argentinos, la personería jurídica de las comunidades y la posesión y propiedad comunitaria de las tierras que tradicionalmente ocupan".  
  
La norma implicó "el cumplimiento de compromisos asumidos" por el Estado Argentino "mediante la ratificación del Convenio 169 de la Organización Internacional del Trabajo (OIT) sobre Pueblos Indígenas y Tribales, en base a un criterio que prevé que "los gobiernos deberán tomar las medidas que sean necesarias para determinar las tierras que los pueblos interesados ocupan tradicionalmente", según se indica en los fundamentos del decreto 805.