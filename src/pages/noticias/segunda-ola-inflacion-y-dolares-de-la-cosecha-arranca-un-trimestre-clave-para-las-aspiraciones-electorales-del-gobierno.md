---
category: Agenda Ciudadana
date: 2021-04-05T06:00:06-03:00
thumbnail: https://assets.3dnoticias.com.ar/ECONOMIA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: INFOBAE
resumen: 'Segunda ola, inflación y dólares de la cosecha: arranca un trimestre clave
  para las aspiraciones electorales del Gobierno'
title: 'Segunda ola, inflación y dólares de la cosecha: arranca un trimestre clave
  para las aspiraciones electorales del Gobierno'
entradilla: 'Las cifras mostrarán un fuerte repunte de la actividad, pero comparada
  con el peor momento de la cuarentena en 2020. '

---
El contagio de COVID-19 que afecta a Alberto Fernández le pone todavía más suspenso a la decisión que adoptará el Gobierno sobre las medidas restrictivas para transitar la “segunda ola” de la enfermedad de la mejor manera posible. El objetivo es afectar lo menos posible la actividad económica y concentrarse en el amplio rubro considerado como “esparcimiento”. Ir a una cuarentena estricta sería absurdo -y a la vez, impracticable- teniendo en cuenta los magros resultados del año pasado.

El segundo trimestre que ya comenzó pero que en la práctica se inicia mañana viene cargado de temas de gran relevancia para la economía argentina y a su vez para la necesidad que tiene el Gobierno de revertir los efectos de la fuerte crisis que generó la pandemia el año pasado. El salto de la pobreza al 42%, con 19 millones de argentinos en esa condición, es indudablemente el dato que mejor refleja el daño que dejó el confinamiento el año pasado. No es sólo económico, también hubo fuertes consecuencias sociales y psicológicas.

El derrumbe de la imagen del Gobierno en la opinión pública es un dato que aparece en todas las encuestas. También retrocedieron varios casilleros el propio Alberto Fernández y Cristina Kirchner. Sin embargo, el “núcleo duro” de poco más del 30% sigue apoyando contra viento y marea. La pregunta es qué sucederá con ese enorme caudal que permitió pasar de ese piso al 48% en las elecciones presidenciales de 2019. Muchos se sienten desencantados por un Gobierno que pensaron tendría menos injerencia del kirchnerismo duro y otros están enojados por la caída de la actividad y la pérdida del salario real.

Marzo terminó con datos preocupantes para la economía. El riesgo país volvió a los 1.600 puntos, la inflación trepó nuevamente en niveles de 4%. Pero la apuesta del Gobierno es sostener ahora la envión que mostró la actividad en los últimos meses, a partir del ingreso extraordinario de dólares por la cosecha

El Gobierno tiene varios focos de atención simultánea para tratar de “sacarle el jugo” a este segundo trimestre. Y sobre todo prepararse para el período preelectoral que siempre es más turbulento. El propio titular del Central, Miguel Pesce, reconoció que se prepara para recibir presiones cambiarias, pero dejó en claro al mismo tiempo que no habrá variaciones respecto a lo que se viene haciendo en los últimos meses. En los últimos días incluso el Central redujo el ritmo de suba del dólar oficial, que ahora apenas se ubica por encima del 2% mensual, la mitad de la inflación que se espera para marzo.

La decisión de evitar los cierres de fábricas, comercios o de la construcción está relacionada con sostener el envión de la actividad económica de los últimos meses. Incluso el juego estadístico mostrará subas espectaculares del PBI en abril y mayo, claro que, comparando con los peores meses de la cuarentena, cuando regía el confinamiento estricto. Pero dejando de lado este aspecto puntual, la actividad en general está dando muestras de reactivación, aunque suaves.

Enero fue un muy buen mes, alentado por el turismo interno, compras que venían reprimidas y fábricas que operaron por encima de su promedio para el arranque del año. Pero ya en febrero y marzo las cifras preliminares muestran cierto enfriamiento. La sostenida suba de la inflación provocó un fuerte impacto en los salarios y resintió la demanda. El Ahora 12 “recargado”, que permitió diferir el pago de la primera cuota por 90 días, resultó fundamental que el consumo no se resintiera todavía más. El problema es que ahora son las tarjetas las que se van quedando sin espacio ante la acumulación de cuotas.

La inflación persistente impide que mejore el poder adquisitivo de los salarios. La baja de Ganancias para los sueldos medios y medios altos es una apuesta para que repunte el consumo. El problema es que se lo quiere compensar con suba de impuestos a las empresas y de Ingresos Brutos en la provincia

Justamente la pelea contra la inflación es clave para empezar a cambiar el malhumor social y mejorar las expectativas sobre el futuro. Desde octubre, es decir seis meses consecutivos, que la inflación se ubique por encima del 3,5% mensual, o sea valores altísimos. Marzo también arrojó un pico de 4%, impulsada sobre todo por alimentos y combustibles, pero a partir de abril debería empezar a ceder, para consolidar la baja en mayo y junio.

Hay varios factores que confluyen para que la inflación desacelere, aunque muy gradualmente. La emisión monetaria bajó a la mitad, aunque sigue en torno a 40% interanual. El tipo de cambio oficial es utilizado como “ancla” como en tantas otras oportunidades y las tarifas aumentarían a cuentagotas pese al reclamo de las empresas.

Pero lo que más entusiasma por estas horas al Gobierno son los dólares que ingresarán por la cosecha de soja, a partir de mediados de abril y especialmente en mayo. En el primer trimestre del 2021, la liquidación de las cerealeras llegó a USD 2.700 millones y marcó un récord. A partir de los altos precios de la oleaginosa, que se mantiene por encima de los USD 500 la tonelada, se multiplicará el ingreso de divisas. El Banco Central acumularía reservas de libre disponibilidad y así tendría más “colchón” para enfrentar los complicados meses preelectorales.

Sin un salto de las inversiones ni recuperación de la confianza, cualquier recuperación económica será de corto plazo. Posiblemente alcance para llegar un poco mejor a las elecciones, pero no mucho más allá

Lo que no está claro es si habrá más acceso a dólares para los importadores. El Central flexibilizó normas para el ingreso de bienes de capital, con el objetivo de fomentar la inversión. Pero no luce suficiente ni mucho menos. Las dificultades para conseguir los permisos hacen que tanto las industrias como los comercios remarquen más de la cuenta. Sucede que el costo de reposición es muy difícil de calcular, ahora no tanto por la cotización de la divisa, sino sobre todo por los problemas para conseguir permisos de importación. Y al reducirse la oferta de bienes es esperable que se produzcan saltos de precios por encima de lo que sucedería en una situación normal.

**Estadística a favor y cambios en Ganancias**

La estadística jugará a favor del oficialismo en los próximos meses. Si los últimos números llevaron a los principales referentes del área económica a hablar de reactivación o incluso de crecimiento, los próximos datos ayudarán a consolidar el “relato”. La actividad crecería alrededor del 15% en abril, en comparación contra el mismo mes del año pasado, que marcó la caída más grande del PBI en la historia argentina, ante el cierre de prácticamente toda actividad productiva, con excepción de supermercados, farmacias, ferreterías y sanatorios. En mayo sucederá algo similar.

El repunte, sin embargo, no alcanzará a esconder el deterioro de los salarios, los millones que cayeron en la pobreza, pero sobre todo la total falta de perspectiva a futuro.

Esta semana se aprobará la modificación del impuesto a las Ganancias, que beneficiará a 1,2 millones de asalariados y les pondrá más plata en el bolsillo ya que dejarán de tributar. Si bien se trata de un largo reclamo que favorece a la clase media, existen fuertes dudas sobre la conveniencia de tal medida, aprobada con amplísimas mayorías legislativas. La contracara de lo que dejará de recaudar el Gobierno (unos $ 40.000 millones anuales) se verán más que compensados con un aumento de la presión tributaria a empresas y una suba de Ingresos Brutos en muchas provincias. Más impuestos a la producción y a la generación de empleo.

El cortoplacismo en casi todas las decisiones se impone una vez más y lo único que importa es sacarle provecho a este trimestre. Razones estacionales y estadísticas darán un respiro luego de tanto agobio, pero que alcanzará para muy poco. Sin acceso al financiamiento internacional, con niveles de inversión en valores bajísimos y con expectativas muy negativas es poco lo que se puede esperar, más allá de rebotes puntuales y seguramente de poca duración.