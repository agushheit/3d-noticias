---
category: La Ciudad
date: 2021-03-23T07:17:48-03:00
thumbnail: https://assets.3dnoticias.com.ar/acería.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: La provincia asistió a los vecinos ante un desprendimiento de una baranda
  de una escalera del Monoblock 1 en barrio Acería
title: La provincia asistió a los vecinos ante un desprendimiento de una baranda de
  una escalera del Monoblock 1 en barrio Acería
entradilla: El hecho sucedió el viernes por la tarde en la ciudad de Santa Fe. Personal
  del Ministerio de Infraestructura, Servicios Públicos y Hábitat, y del municipio
  local, concurrieron al lugar para el correspondiente peritaje.

---
El Ministerio de Infraestructura, Servicios Públicos y Hábitat, a través de la Secretaría de Hábitat, Urbanismo y Vivienda, y Dirección Provincial de Vivienda y Urbanismo (DPVyU), coordinaron un operativo preventivo en el Monoblock 1 de barrio Acería de la ciudad capital.

El viernes por la tarde, vecinos del barrio informaron un desprendimiento de mampostería en un sector de una escalera del monoblock 1. Inmediatamente se hicieron presentes al lugar autoridades y personal técnico de la Secretaría de Hábitat, Urbanismo y Vivienda, y de la Dirección Provincial de Vivienda y Urbanismo del gobierno provincial, quienes realizaron una inspección visual y comprobaron la caída de una parte de la baranda de mampostería del último tramo de la escalera, ubicada en los palieres de uso común del monoblock 1 a la altura del tercer piso, sin compromiso estructural de la escalera.

También, se verificó la presencia de personal de Edificaciones Privadas, de Defensa Civil y del COBEM, dependientes de la Municipalidad de Santa Fe, atento a que el poder de policía sobre las edificaciones y la defensa civil de los habitantes de la ciudad son potestades de los gobiernos locales.

Del diálogo de las autoridades con los vecinos, el director Provincial de Localización y Adquisición de Tierras, Francisco Alda, manifestó: “Inmediatamente que los vecinos nos informaron de lo ocurrido, concurrimos al lugar y colocamos los elementos de seguridad correspondientes para resguardar la vida de sus moradores. Si bien el mantenimiento preventivo y correctivo de los espacios comunes de uso común de las propiedades sometidas al régimen de propiedad horizontal es responsabilidad del consorcio de los mismos, este gobierno provincial, y por indicación de la ministra Frana, no dudó en llegar al lugar y brindar colaboración y asistencia inmediata”.

Más adelante, el funcionario aseguró que además de las 102 viviendas que se están terminando de construir, ya se licitaron y adjudicaron 90 viviendas más. “Esta intervención habitacional y urbanística va a dejar un barrio totalmente nuevo y con espacios vacantes para construir más viviendas. Este es el camino que nos indicó el gobernador Omar Perotti, la ministra Silvina Frana y el secretario de Hábitat, Amado Zorzón, de avanzar en la construcción de viviendas con la mayor rapidez posible, con el objetivo de achicar la demanda habitacional en la provincia”.

Asimismo, personal de la secretaría y de la dirección provincial, informaron que se continuará con el cronograma acordado desde el comienzo de la gestión y reafirmado en la reunión que referentes de las viviendas agrupadas tuvieron el viernes 12 de marzo en las instalaciones de la DPVyU con el director provincial. Se sostuvo el criterio de orden de relocalización de los monoblocks priorizando el número 15 y 14 con el objetivo de despejar la manzana que permita continuar con las licitaciones para construir más viviendas y proseguir con el plan de relocalización.

Finalmente, las autoridades del gobierno provincial se comprometieron, a modo de colaboración, y aunque no sea su responsabilidad primaria, a explorar alternativas para asesorar a los consorcios en las actividades de mantenimiento que los mismos deban realizar hasta el momento en que a los habitantes de cada monoblock les toque la reubicación y la respectiva demolición del mismo.