---
category: La Ciudad
date: 2020-12-11T11:36:27Z
thumbnail: https://assets.3dnoticias.com.ar/cemafe.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: El Cemafe realizó la primera cirugía odontológica de mediana y alta complejidad
title: El Cemafe realizó la primera cirugía odontológica de mediana y alta complejidad
entradilla: Se trató de una resección de tumor mandibular y reconstrucción inmediata
  con placa de titanio. Intervenciones quirúrgicas multidisciplinarias y mayor  capacidad
  de respuesta del quirófano del efector.

---
El Ministerio de Salud de la provincia informó que la Unidad de Medicina Odontológica y Cirugía Maxilofacial del Centro de Especialidades Médicas Ambulatorias de Santa Fe (Cemafe), realizó la **primera intervención de mediana y alta complejidad** de la especialidad consistente en una resección de un tumor mandibular y la reconstrucción inmediata con placa de titanio.

Se trata del único Servicio de Odontología del centro-norte de la provincia que reúne operatoria, cirugía dentoalveolar; periodoncia; endodoncia; prótesis; ortodoncia; estomatología; cirugía maxilofacial; atención de pacientes especiales y médicamente comprometidos; diagnóstico radiológico; y que cuenta con las últimas tecnologías a nivel mundial para realizar las intervenciones.

Al respecto, el jefe de la unidad, Gonzalo Pucheta, indicó: “Este miércoles junto al Servicio de Otorrinolaringología, se llevó adelante en el efector la primera cirugía de mediana y alta complejidad: resección de tumor mandibular y reconstrucción inmediata con placa de titanio. Todo dentro de este contexto de pandemia que dificulta la situación, pero con todos los protocolos de seguridad y sin dejar de avanzar”.

“Hoy el paciente se encuentra estable –evaluó-, pasó la noche bien, solo presentó un poco de inflamación en la zona de la operación, lo cual es normal en este tipo de intervenciones, pero sin dolor”.

<br/>

## **ATENCIÓN PÚBLICA GARANTIZADA**

Pucheta detalló que “este tipo de cirugías se realizaban en otros efectores” y aclaró que en el Cemafe “comenzamos a llevar adelante estas intervenciones, brindando así una opción más a la población, garantizándoles la atención y el acceso al sistema de salud público provincial”.

“Contamos con un servicio de odontología de alta complejidad donde realizamos prácticas que en otros lugares no se practican, y esto es posible gracias a un abanico grande de profesionales quienes posibilitan dar respuesta al ciento por ciento de las patologías del área odontológica y maxilofacial”, concluyó.

<br/>

## **CIRUGÍAS MULTIDISCIPLINARIAS**

Por su parte, el Director de Gestión Institucional del Cemafe, Sebastián Calvet, expresó que “este tipo de intervenciones quirúrgicas multidisciplinarias, se da en el marco de aumentar la capacidad de respuesta del quirófano del efector”.

Por último, señaló que “el año pasado se podían realizar algunas cirugías menores, endoscopías; este año y a pesar del contexto de la pandemia, se incorporaron distintos tipos de cirugías como ginecológicas, plásticas, general, urológicas, vascular, traumatológicas con artroscopía y de miembro superior”.