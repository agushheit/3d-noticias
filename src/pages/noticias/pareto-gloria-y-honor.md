---
category: Deportes
date: 2021-07-25T06:15:38-03:00
thumbnail: https://assets.3dnoticias.com.ar/PARETO.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: 'Pareto: gloria y honor'
title: 'Pareto: gloria y honor'
entradilla: '"La Peque" cerró este sábado su historia olímpica con un digno diploma
  en los Juegos Tokio 2020, los cuartos en su brillante carrera.'

---
Mejor judoca nacida en estas tierras y una de las expresiones más luminosas del deporte argentino del siglo en curso, la bonaerense Paula Belén Pareto, "La Peque", cerró este sábado su historia olímpica con un digno diploma en los Juegos Tokio 2020, los cuartos en su brillante carrera.

En el Centro Nippon Budokan de la capital japonesa pisó por última vez el tatami, al menos en lo que refiere a la máxima cita del deporte mundial, quien fuera la primera mujer argentina en consagrarse campeona olímpica hace cinco años en Río de Janeiro, ciudad en la que se convirtió definitivamente en leyenda.

A esos excepcionales resultados, la "Peque" le sumó una colección de medallas en el circuito internacional de judo, lo que redondeó una producción de 21 oros, 12 platas y 17 bronces en toda su campaña.

Competidora incansable y dueña de una fortaleza mental asombrosa, Pareto se ganó el reconocimiento de todo su país y también de la comunidad deportiva extranjera, algo demostrado con su elección como una de las atletas portadoras de la bandera del Comité Olímpico Internacional (COI) en la ceremonia de apertura de Tokio 2020.

Construyó su impresionante carrera en paralelo a sus estudios de médica traumatóloga, profesión que comenzó a ejercer de manera efectiva cuando ya era una gloria del deporte nacional.

Por todo eso se ganó la admiración de una sociedad que reconoció en ella los valores del trabajo, la perseverancia, el coraje, la convicción y también una fuerza arrolladora.

La originaria de San Fernando, que en los tiempos de los Juegos de Londres 2012 se entrenaba en el gimnasio de Estudiantes de La Plata, pasó tres lustros en la elite del deporte internacional con una amplia cosecha que incluye también haber sido medallista en tres Juegos Panamericanos (bronce en Río de Janeiro 2007, oro en Guadalajara 2011 y plata en Toronto 2015).

La consagración como campeona del mundo en Kazajistán frente a la japonesa Haruna Asani redundó en el premio máximo reservado cada año para el deportista argentino de mayor relevancia: el Olimpia de Oro

Del mismo modo, fueron tres las preseas que recaudó en Campeonatos Mundiales: plata en Cheliabinsk (Rusia) 2014, oro en Astaná 2015 y bronce en Bakú (Azerbaiyán) 2018.

La consagración como campeona del mundo en Kazajistán frente a la japonesa Haruna Asani redundó en el premio máximo reservado cada año para el deportista argentino de mayor relevancia: el Olimpia de Oro.

Con la misma tenacidad mostrada en el tatami, luchó los últimos años contra una hernia cervical que la sacó de competencia en los Panamericanos Lima 2019 y que la condicionó en el posterior Mundial de Japón, donde clasificó séptima.

En octubre de ese mismo año pasó por el quirófano y desde entonces inició su preparación para estos Juegos, sin saber que una pandemia aplazaría un año su despedida olímpica.

Sus dos primeras victorias por ippon en Tokio 2020, ante la sudafricana Geronay Whitebooi y la eslovena Marusa Stangar ilusionaron con un camino hacia su segundo oro olímpico pero el sueño quedó trunco con la derrota ante la japonesa Funa Tonaki en los cuartos de final y la caída en el repechaje ante la portuguesa Catarina Costa.

En Tokio 2020 clausuró su carrera olímpica luego de ser tercera en Beijing 2008, quinta con diploma en Londres 2012 y campeona en Río 2016.

De no mediar sorpresas, la competencia de este sábado habrá marcado también el absoluto final de su historia como judoca, según admitió días atrás: "Es mi último Juego Olímpico y mi última competencia casi seguro".

Sea o no, esta guerrera criolla de menos de 48 kilos, camino a los 36 años, ya goza para siempre de la reputación de auténtica leyenda para el deporte argentino.