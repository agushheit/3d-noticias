---
category: Estado Real
date: 2021-04-30T10:24:17Z
thumbnail: https://assets.3dnoticias.com.ar/giavedoni.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Diario Uno
resumen: 'Restricciones: los montos por incumplimientos serán desde $150.000'
title: 'Restricciones: los montos por incumplimientos serán desde $150.000'
entradilla: 'El fiscal Estanislao Giavedoni alertó sobre las consecuencias judiciales
  que se imponen ante incumplimientos de las restricciones por la pandemia

'

---
El representante del Ministerio Público de la Acusación, Estanislao Giavedoni, dio precisiones este jueves ante incumplimientos de las restricciones por la pandemia. UNO Santa Fe consultó los montos a pagar, en caso de tener que atravesar un proceso judicial, después de las distintas fiestas clandestinas del último fin de semana en barrios privados de la ciudad. El fiscal dijo que podrían partir de la base que se aplicó el año pasado en casos similares durante los juicios a prueba: 150.000 pesos.

En esta línea aclaró que no se trata de multas, ya que no están establecidas en el Código Penal como monto fijo o unidad. "La suspensión de juicio a prueba es un instituto que está previsto como criterio oportunidad y que requiere de la persona que cometió el delito determinadas obligaciones y eventualmente la reparación de daño desde el punto de vista económico. Mensuramos en base a la experiencia de los casos que tuvimos y por ejemplo, por una fiesta el año pasado se terminó siendo conminando un aporte de 150.000 pesos, o sea ese tipo de situaciones puede partir de esa base", informó Giavedoni.

En el caso de la fiesta clandestina en la que participó un funcionario público del Ejecutivo y que se conoció a través de un video que se viralizó en redes sociales, Giavedoni precisó que identificaron al organizador del evento y el propietario del establecimiento donde ocurrió. Al mismo tiempo adelantó que investigan la lista de asistentes, y que los mismos podrían enfrentar los mismos procesos judiciales que los ya imputados.

"El Código Penal para estos casos establece penas de prisión de acuerdo al artículo 205. Son delitos netamente excarcelables, lo que permite la posibilidad de aplicar lo que se llama "criterio de oportunidad" o suspensión de juicio a prueba, donde se puede llegar a un resarcimiento económico a la sociedad por el daño. El artículo 205 es la violación de las medidas impuestas por la autoridad. Incumplir el artículo 202 ya es distinto y posiblemente no tengamos la posibilidad de aplicar una suspensión de juicio a prueba o criterio de oportunidad porque es un delito más grave, propagación de la pandemia", explicó el fiscal.

Y recordó: "En la etapa anterior, desde el año pasado con todas las actuaciones que se llevaron adelante se logró por ejemplo una recaudación de más de un millón de pesos que fueron destinadas a organizaciones sociales y en su mayoría al Hospital Cullen de la ciudad de Santa Fe. Lo que buscamos en todo caso con estos procesos penales es alentar a las personas a que no realicen la conducta que está prohibida".

**Restricciones de circulación**

Al ser consultado por UNO Santa Fe por incumplimientos en la circulación dijo que actualmente lo que se hace es el anoticiamiento por parte de la policía de las medidas a las personas que se trasladen de 21 a 6 y la formación de causa penal en estado de libertad. Y en el caso de los vehículos, el secuestro de los mismos. "Para recuperar los vehículos pueden solicitar la devolución en el MPA y se genera el proceso penal, una vez culminado el mismo se evalúa o no la devolución. Normalmente se están devolviendo los vehículos tras el proceso penal", advirtió.

Por último, explicó que quienes son "anoticiados" quedan registrados en los antecedentes penales. "Queda registrado y termina la suspensión del juicio a prueba. Se cumplen con él las obligaciones asumidas y se hace la reparación. Culminado el plazo mínimo de un año o dos puede solicitarse el sobreseimiento y eso no genera antecedentes penales condenatorias. Y la aplicación de un criterio de oportunidad es la posibilidad que tiene el MPA de no llevar adelante la acción penal si se cumplió con la reparación del daño causado y si está dentro de las posibilidades que establece el artículo 19 del Código Procesal Penal", concluyó.