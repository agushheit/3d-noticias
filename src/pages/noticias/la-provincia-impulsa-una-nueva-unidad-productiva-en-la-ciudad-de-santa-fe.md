---
category: Estado Real
date: 2021-04-21T07:09:06-03:00
thumbnail: https://assets.3dnoticias.com.ar/molino.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: La provincia impulsa una nueva unidad productiva en la ciudad de Santa Fe
title: La provincia impulsa una nueva unidad productiva en la ciudad de Santa Fe
entradilla: En el marco del programa Santa Fe Más, 15 jóvenes se capacitarán en un
  bar temático y literario que funcionará en instalaciones de El Molino, Fábrica Cultural.

---
En el marco del programa del gobierno provincial “Santa Fe Más, Aprender Haciendo”, 15 jóvenes se capacitaran en un bar temático y literario que funcionará en instalaciones de El Molino, Fábrica Cultural. Esta unidad productiva es llevada adelante por los ministerios de Desarrollo Social y Cultura.

Durante la presentación, el ministro de Desarrollo Social, Danilo Capitani, detalló que “a través del programa Santa Fe Más, estamos articulando con los distintos ministerios, en este caso con Cultura, llevando adelante una estrategia para la constitución de una unidad productiva e insertar a los sectores más vulnerables en el circuito productivo”.

“En esta oportunidad, de manera conjunta con CAMCO, una institución que tiene mucho recorrido en el trabajo social y comunitario en la ciudad de Santa Fe, ponemos, a disposición de los jóvenes, las herramientas del Estado provincial para acompañar a los participantes en la concreción de su inserción laboral, con el objetivo de emprender un trabajo autogestivo y cooperativo”, destacó Capitani.

Por último, el Ministro prescisó que “las becas estimulo, les permiten a los jóvenes venir a aprender para luego vincularlos rápidamente desde lo social hacia lo laboral, que es lo que nos planteó el gobernador Perotti cuando iniciamos con el programa Santa Fe Más”.

En tanto, la directora provincial de Programas Socioculturales, Mariana Escobar, afirmó: “Queremos que todos los ministerios estén comprometidos con este proyecto, que es ambicioso desde el punto de vista de dar oportunidades y herramientas laborales a las juventudes de la provincia de Santa Fe, desde la capacitación y el desarrollo socioproductivo”.

Por su parte, la directora provincial de Desarrollo Territorial, Julia Irigoitía, expresó que “este proyecto significa una experiencia ejemplificadora de lo que queremos sea el recorrido de los chicos y las chicas de la provincia por el Santa Fe Más”.

**Molino Café**

Es un dispositivo de egreso con el objetivo de conformar una unidad productiva para constituir un espacio de cafetería y confitería brindando servicio al público habitué de El Molino, Fábrica Cultural.

Participa un grupo de 15 jóvenes que vienen construyendo su trayectoria en talleres y espacios socio comunitarios en la organización CAMCO; y trabajando contenidos teóricos y técnicos con competencias de maestro y ayudante pastelero, vendedor de productos de confitería, y auxiliar de cafetería entre otros.

Teniendo en cuenta la situación de emergencia sanitaria por la pandemia de COVID 19, se lleva adelante el Protocolo de higiene y funcionamiento de bares y restaurantes, compilado por la Secretaría de Comercio Interior y Servicios, con el aporte de cámaras y entidades del sector, y en base a lo establecido en la resolución 41/2020 del Ministerio de Trabajo, Empleo y Seguridad Social de la provincia de Santa Fe.

Atendiendo a dichos protocolos, el grupo se divide en dos subgrupos de 7 participantes cada uno, que variarán los días de trabajo y asistencia al espacio, debiendo cumplir 30 horas mensuales.

Acompañan este proceso; 2 docentes con idoneidad en dichas áreas técnicas y 2 referentes que aportan a la dinámica de la grupalidad y procesos subjetivos que los/as jóvenes vayan transitando en el período de formación y trabajo.

Desde el programa “Santa Fe Más: aprender haciendo” se brindará los recursos económicos necesarios para el pago de insumos destinados a la producción, pago de referentes y capacitadores/as y becas a los/as jóvenes.

**Santa Fe Más**

Es una política de inclusión socio-productiva que propone la articulación entre sindicatos, empresarios y organizaciones de la sociedad civil con el Estado para generar instancias de formación dirigidas a los jóvenes de entre 16 y 30 años. Los espacios formativos estarán distribuidos por todo el territorio y servirán como herramienta de acercamiento para diseñar estrategias de abordaje integrales.