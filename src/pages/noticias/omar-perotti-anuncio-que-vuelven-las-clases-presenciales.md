---
category: Agenda Ciudadana
date: 2021-06-26T06:15:05-03:00
thumbnail: https://assets.3dnoticias.com.ar/PRESENCIALES.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Omar Perotti anunció que vuelven las clases presenciales
title: Omar Perotti anunció que vuelven las clases presenciales
entradilla: Lo afirmó el gobernador Omar Perotti en un acto oficial en la Casa Gris
  y dijo que las decisiones se harán en base a indicadores sanitarios y epidemiológicos

---
Este viernes 25 de junio vence el actual decreto de la provincia y Omar Perotti evalúa cuatro puntos en especial respecto de las actividades que se pueden abrir o ampliar a partir de este sábado y con proyección hacia la semana que viene. Todas las decisiones que se tomen se harán, como siempre, en base a los indicadores sanitarios y epidemiológicos que se analizarán esta misma jornada junto al comité de expertos e intendentes y presidentes comunales.

“La idea es volver en toda la provincia con la presencialidad plena desde el lunes en el nivel inicial y primario y hoy analizaremos con el cierre del día las condiciones para ver en qué lugares podemos volver en plena magnitud”, dijo Omar Perotti.

“Cualquier mejora, aunque sea mínima de descenso de casos y camas ocupadas, se vuelca a la presencialidad en la educación. Tenemos todos los docentes vacunados, incluidos los reemplazantes inscriptos. Tenemos dos semanas hasta las vacaciones para que los más pequeños aprovechen el vínculo con los docentes. Ojalá después de las vacaciones la situación siga mejorando”, dijo el mandatario.