---
category: Estado Real
date: 2021-11-04T06:00:01-03:00
thumbnail: https://assets.3dnoticias.com.ar/herramientas.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La provincia entregó herramientas de trabajo a emprendedores de la economía
  social de a cuidad de Santa Fe
title: La provincia entregó herramientas de trabajo a emprendedores de la economía
  social de a cuidad de Santa Fe
entradilla: Se entregaron hornos y utensilios de cocina para fortalecer el trabajo
  que realiza en los barrios y que se intensificó durante la pandemia.

---
El ministerio de Desarrollo Social, a través de la secretaría de Integración Social e Inclusión Socioproductiva, formalizó la entrega de herramientas y utensilios de cocina a emprendedores del rubro gastronomía de la economía social de diferentes barrios de la ciudad de Santa Fe.

En esta oportunidad, se entregaron 7 hornos industriales para que vecinos de los barrios Las Lomas, San Martín y Eva Perón, continúen y expandan la producción de alimentos que se intensificó durante la pandemia.

Además, se otorgaron diferentes utensilios de cocina, herramientas de gran utilidad, para el emprendimiento socioproductivo de repostería de Caritas en el Barrio Santa Rosa de Lima.

El ministro de la cartera provincial, Danilo Capitani manifestó: “Abogamos porque esta relación, este trabajo en conjunto, esta articulación, entre las instituciones intermedias, la gente y el Estado se pueda profundizar cada día mas para que las herramientas con las que contamos puedan llegar a cada uno de los ciudadanos que vienen trabajando hace mucho tiempo”.

Y agregó: “Es gratificante saber que cada uno de los recursos del Estado termina en la gente que mas lo necesita y posibilita el trabajo tanto de los emprendedores como de las instituciones intermedias”.

Por su parte, el secretario de Integración Social e Inclusión Socioproductiva, Gustavo Chara, expresó: “Estamos muy contentos de que a través de las herramientas, los programas, las políticas públicas que potenciamos desde nuestro ministerio, se generen mejoras en el entorno y la calidad de vida en los ámbitos donde se desarrollan y viven cada uno de nuestros emprendedores".

Y agregó: "Este es un primer paso que nos permitirá articular el trabajo en cada uno de sus territorios, para seguir potenciando todas las iniciativas que tengan que ver con el asociativismo y la economía social”.

**ACCESO A LAS HERRAMIENTAS**  
El ministerio de Desarrollo Social, a través de este tipo de iniciativas garantiza el acceso a herramientas de trabajo de baja intensidad para emprendedores familiares vinculados a una comercialización cercana generando mas trabajo, fortaleciendo a los actores que emprenden en micro circuitos locales para incorporarlos al mundo del trabajo vinculado a una economía popular que crece e incluye desde abajo.

Cabe destacar que en el contexto de la pandemia la actividad laboral se vio muy afectada por lo que se trabaja para impulsar, fomentar y fortalecer actividades de la economía social y popular, generando con este tipo de políticas públicas el desarrollo productivo autogestivo y comunitario.