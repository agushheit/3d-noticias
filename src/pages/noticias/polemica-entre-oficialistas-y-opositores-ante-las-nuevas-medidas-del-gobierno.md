---
category: Agenda Ciudadana
date: 2021-01-07T21:41:09Z
thumbnail: https://assets.3dnoticias.com.ar/080121-moreau.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Télam'
resumen: Polémica entre oficialistas y opositores ante las nuevas medidas del Gobierno
title: Polémica entre oficialistas y opositores ante las nuevas medidas del Gobierno
entradilla: La restricción de la circulación nocturna dispuesta por el Gobierno Nacional
  y que el viernes entrará en vigencia, generó adhesión en la dirigencia oficialista
  y rechazo en referentes de Juntos por el Cambio y la UCR

---
Legisladores y referentes del oficialismo y la oposición se manifestaron hoy sobre las nuevas medidas que analiza el Gobierno para mitigar la propagación del coronavirus, entre ellas la restricción de la circulación en horario nocturno, considerada por unos como «muy prudente» y por otros como un «fracaso de la estrategia» sanitaria.

El presidente de la Comisión de Salud de la Cámara de Diputados, Pablo Yedlin (Frente de Todos-Tucumán), indicó en declaraciones a Télam: «Me parece muy prudente que el Gobierno nacional empiece a tomar medidas en forma temprana y empiece a ver cómo poder empezar a restringir un poco la circulación».

En ese sentido, sostuvo que las medidas permitirán analizar «si este rebrote que tenemos es simplemente por haber dejado de tener distanciamiento social en el final de la primera ola o estamos frente al inicio de una segunda, como sucede actualmente en Europa. Todas las medidas que se puedan tomar, y que además intentan ser medidas un poco menos restrictivas para permitir algún nivel de actividad durante el día, son oportunas», advirtió el legislador.

Asimismo, consideró que «obviamente, restringen algún nivel de actividad durante la noche, pero intentan ser la mitad de duras de lo que sería una restricción total».

«Como primera medida y en una escala, y obviamente dependiendo de la jurisdicción, no es lo mismo Jujuy que Tierra del Fuego: cada uno dependerá de la circulación viral que tengan en su lugar», expresó Yedlin y manifestó que las decisiones son correctas.

El diputado nacional de la UCR, Alfredo Cornejo, escribió en su cuenta de la red social Twitter: «Las medidas que tomará el gobierno de @alferdez \[Alberto Fernández\] son la clara demostración del fracaso de la estrategia sanitaria. Son el tiro de gracia a la economía de los argentinos. Restringir libertades y responsabilizar al otro, kirchnerismo puro y duro.»

[![Twitter](https://assets.3dnoticias.com.ar/cornejo-070121.webp)](https://twitter.com/alfredocornejo/status/1347203107122532354?s=20 "Leer tweet")

La presidenta del PRO, Patricia Bullrich, por su parte, sostuvo en su cuenta de Twitter: «El toque de queda debe aprobarse en el Congreso; no es potestad de un monarca. ¿Quiénes pagan el fracaso de la política sanitaria del Gobierno? Millones de argentinos que no pueden trabajar. ¡Otro golpe a los trabajadores!»

[![Twitter](https://assets.3dnoticias.com.ar/bullrich-070121.webp)](https://twitter.com/PatoBullrich/status/1347208990988652544?s=20 "Leer tweet")

El diputado nacional Leopoldo Moreau (Frente de Todos-provincia de Buenos Aires) sostuvo en diálogo con esta agencia que «todos los países del mundo están aplicando, frente al rebrote –que en la Argentina todavía no ocurrió–, medidas restrictivas. En la Argentina no hacemos nada distinto; mientras tanto, el Gobierno nacional está luchando en un mercado internacional salvaje para garantizar que, hacia fines de marzo, toda la población de riesgo esté vacunada».

Asimismo, sostuvo que «Patricia Bullrich y Alfredo Cornejo no son opositores, simplemente son caretas irresponsables que juegan con la vida de los argentinos. El gobierno de Alberto Fernández manejó, como pocos en el mundo, con un enorme criterio, la crisis de la pandemia», destacó Moreau.

En ese sentido, dijo que «es cierto que tenemos el dolor de más de 40.000 compatriotas que perdieron la vida, pero ninguno de ellos murió porque le faltara una cama o un respirador, un terapista, o los medicamentos que necesitaba. Las declaraciones del presidente de la UCR y de la presidenta del PRO confirman que, si estuvieran gobernando ellos, o sea Juntos por el Cambio, estaríamos viviendo una verdadera tragedia sanitaria», opinó Moreau.

En ese contexto, consideró que «los trabajadores y el sector productivo hubieran quedado totalmente desprotegidos, porque no habría ni IFE, ni ATP, ni subsidios, ni congelamiento de tarifas, ni nada que protegiera a la economía de los argentinos».

Los miembros del interbloque de diputados nacionales de Juntos por el Cambio manifestaron en un comunicado su gran preocupación por la decisión de establecer nuevas medidas para mitigar el coronavirus, que incluyen la restricción de la circulación en horario nocturno, y afirmaron que «nuevamente el Gobierno elige restringir las libertades individuales como una única respuesta a la crisis sanitaria del coronavirus».

Asimismo, manifestaron que «ya nadie puede desconocer, además, que la falta de políticas sanitarias agrava los problemas económicos», en el texto firmado por el presidente del interbloque de JxC y de la UCR, Mario Negri y los jefes de bloque del PRO, Cristian Ritondo y de la Coalición Cívica, Juan Manuel López.

El diputado nacional Eduardo "Bali" Bucca (Justicialista-provincia de Buenos Aires), por su parte, dijo a Télam que «el estado de situación actual de la pandemia en nuestro país nos debe hacer reflexionar que nos pone en una situación de volver a empezar y tomar decisiones».

«Porque está claro que se incrementan los casos a diario, está claro que a nivel mundial la pandemia sigue activa y en muchos países vemos la segunda ola, o la segunda profundización de un brote. Eso incipientemente se podría decir (sic) que se empieza a ver en ciertas zonas de nuestro país, en particular en nuestra provincia», apuntó Bucca.

El diputado consideró como «muy importante que el Gobierno vuelva a llamar y a convocar al diálogo y construcción de consenso de todas las fuerzas políticas, para que las decisiones que se tomen permitan unificar un mismo mensaje y sean acatadas con más contundencia por la sociedad».

El legislador sostuvo además que la restricción a la circulación en horario nocturno ya se metió en el debate político y en la grieta, y agregó que «el Gobierno y el Presidente tiene que convocar (sic), como al comienzo de la pandemia, a todos los actores y sectores».