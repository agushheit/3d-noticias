---
category: La Ciudad
date: 2021-03-14T06:00:11-03:00
thumbnail: https://assets.3dnoticias.com.ar/DEFENSA.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Vecinos de la Costa reclamaron por la reparación de las defensas
title: Vecinos de la Costa reclamaron por la reparación de las defensas
entradilla: 'A través de una manifestación, los residentes de la zona costera reclamaron
  la reparación de la “Defensa Garello”, para evitar inundaciones y mantener el servicio
  de agua potable. '

---
Vecinos del Distrito de la Costa santafesina colmaron el ingreso de la Ruta Provincial Nº1 en horas de la mañana de este sábado. A través de una manifestación, los residentes de la zona costera reclamaron la reparación de la “Defensa Garello”, para evitar inundaciones y mantener el servicio de agua potable en el área afectada.

Con pancartas, bombos y platillos, los representantes de la Agrupación “Defensas de la Costa” expresaron el urgente inicio de las obras en el kilómetro 3 y medio, jurisdicción de Colastiné Norte.

Damaris, referente de la organización barrial, explicó: Planteamos la problemática del terraplén en la parte del Garello, donde se esta socavando la muralla de contención. Es un lugar muy importante porque ahí se ubica la toma de agua que provee a toda la ciudad”.

Además, detalló que “el estado es terrible” de la toma que suministra el servicio de agua potable en las áreas de Colastiné Norte, Colastiné Sur, San José del Rincón y la ciudad de Santa Fe. Se encuentra en un estado de deterioro terrible. Siempre se han puesto parches que no duraron. Y ahora estamos pidiendo su arreglo definitivo”, comentó con Gabriela Hassan.

Por último, mencionó que a pesar de la apertura de sobres para el comienzo de la licitación de las obras de restauración, no obtuvieron nuevas respuestas por parte del Estado Municipal ni Provincial. “Se abrieron los sobres, pero nunca tuvimos respuestas. Quedó en la nada la licitación. Jamás empezaron las obras. Esto nos genera incertidumbre. Se empezó a hacer la licitación del inicio de las obras. Es un problema muy importante, ya que perjudica a toda la zona de la costa y la ciudad de Santa Fe"