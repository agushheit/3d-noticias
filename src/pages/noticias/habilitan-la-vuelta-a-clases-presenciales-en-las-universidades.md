---
layout: Noticia con imagen
author: "Fuente: Notife"
resumen: Vuelta a clases
category: Agenda Ciudadana
title: Habilitan la vuelta a clases presenciales en las universidades
entradilla: El Gobierno nacional habilitó el retorno de las actividades
  presenciales en universidades e institutos, cumpliendo un protocolo que
  garantice las medidas de cuidado de los trabajadores y estudiantes.
date: 2020-11-06T13:10:00.000Z
thumbnail: https://assets.3dnoticias.com.ar/universidad.jpg
---
El Gobierno nacional habilitó este viernes el retorno de las actividades presenciales en universidades e institutos, cumpliendo un protocolo y lineamientos generales que garanticen las medidas de cuidado de los trabajadores y estudiantes ante la pandemia por coronavirus.

Así se estableció a través de la Decisión Administrativa 1995/2020 que lleva la firma del jefe de Gabinete, Santiago Cafiero, y del ministro de Salud, Gines González García, que fue publicada este viernes en el Boletín Oficial.

“Establécese que en el marco del protocolo marco y lineamientos generales para el retorno a las actividades académicas presenciales en las universidades e institutos universitarios, aprobado por la Resolución del Ministerio de Educación 1084 del 8 de agosto de 2020, este revisará y prestará conformidad a los planes jurisdiccionales para el retorno de las actividades”, indica el artículo 1 de la norma.

Asimismo, establece que “con la conformidad del ministerio de Educación, la efectiva reanudación de las actividades académicas presenciales en Universidades e Institutos Universitarios será decidida por las autoridades provinciales y de la ciudad de Buenos Aires, quienes podrán suspender las actividades y reiniciarlas conforme la evolución de la situación epidemiológica”.

Las actividades presenciales podrán reanudarse “conforme los protocolos que en cada caso establezcan las autoridades sanitarias locales” y, entre esas medidas de cuidado, se encuentran “la organización de turnos, si correspondiere, y los modos de trabajo que garanticen las medidas de distanciamiento e higiene necesarias para disminuir el riesgo de contagio de COVID-19”.

En tanto, los desplazamientos de las personas “deberán limitarse al estricto cumplimiento de las actividades exceptuadas por la presente”, indica la disposición.

Por su parte, los institutos y universidades “deberán garantizar las condiciones de higiene y seguridad establecidas por la jurisdicción para preservar la salud de sus trabajadoras, trabajadores y estudiantes”.

La medida indica además que los gobernadores y el Jefe de Gobierno de la Ciudad Autónoma de Buenos Aires “decidirán la efectiva reanudación, pudiendo suspender las actividades y reiniciarlas en el marco de sus competencias territoriales, en virtud de las recomendaciones de la autoridad sanitaria local, y conforme a la situación epidemiológica y sanitaria”.

En tanto, el personal directivo, docente y no docente y los estudiantes que asistan a las actividades académicas presenciales “quedan exceptuados de la prohibición del uso del servicio público de transporte de pasajeros urbano, interurbano e interjurisdiccional, según corresponda y a este solo efecto”.

No obstante, en los considerandos, se recomienda que el transporte público “solo sea empleado cuando no se cuente con medios alternativos”.

[Leer Decreto](https://assets.3dnoticias.com.ar/universidad.pdf)