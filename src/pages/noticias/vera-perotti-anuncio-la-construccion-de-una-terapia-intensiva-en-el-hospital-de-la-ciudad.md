---
category: Estado Real
date: 2021-01-02T10:45:40Z
thumbnail: https://assets.3dnoticias.com.ar/01012021-Perotti-en-vera.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: 'Vera: Perotti anunció la construcción de una terapia intensiva en el hospital
  de la ciudad'
title: 'Vera: Perotti anunció la construcción de una terapia intensiva en el hospital
  de la ciudad'
entradilla: Además entregó aportes a municipios, comunas y clubes del departamento.
  En Calchaquí, encabezó el acto de apertura de sobres con ofertas para la construcción
  de la nueva terminal de ómnibus.

---
El gobernador Omar Perotti estuvo presente, el miércoles último, en el departamento Vera, donde anunció la construcción de una terapia intensiva para el hospital de la ciudad homónima.

Además, entregó aportes a municipios, comunas y clubes. También estuvo en la ciudad de Calchaquí, donde encabezó el acto de apertura de sobres con ofertas económicas para la construcción de la estación terminal de ómnibus.

La primera actividad se desarrolló en el Club Social de Vera. Allí, en el inicio de su discurso, el gobernador agradeció «a todos y a cada uno de los intendentes de la región por el trabajo y el esfuerzo que, en forma conjunta, nos ha permitido sobrellevar un año muy duro. El cuidado en cada una de las localidades ayudó a salvar vidas. Esto es algo que quiero reconocerlo».

En ese sentido, Perotti anunció que se construirá una terapia intensiva en el Hospital de Vera. Recordó que «en algunas áreas en particular, el esquema de nodos generó retrasos en muchas de las prestaciones de servicio a la población, y una de ellas ha sido en salud».

Al respecto, expresó que eso «disminuye enormemente la potencialidad en cada comunidad», y se refirió a las dificultades de los traslados, «que se vuelven interminables cuando tenés las distancias que hay nuestro norte. No hay posibilidad de funcionar si no tenés instancias intermedias con buen equipamiento, y Vera fue quizás la ciudad que más sufrió este tema».

Por esto, «en el rol importante que la salud nos marca, vamos a realizar una fuerte inversión en el hospital de Vera, que lo estuvimos recorriendo antes de venir aquí. No solamente vamos a estar haciendo las tareas de refacción y de reacondicionamiento, sino que vamos a montar una terapia», afirmó Perotti.

«Sé que esto es un anhelo de muchos que han trabajado y gestionado a través de instituciones en los distintos ámbitos. Expresaron lo que era no solamente un deseo, sino una necesidad», reconoció el gobernador de Santa Fe. «La coordinación y cooperación entre los sectores público y privado será la forma no solo de salir de la pandemia, sino de sostener los mejores y mayores niveles de prestación de salud en cada una de nuestras regiones».

Del acto participaron también la ministra de Infraestructura, Servicios Públicos y Hábitat de la provincia, Silvina Frana; el subsecretario de Comunas, de la provincia, Carlos Kaufmann; y los intendentes de Vera, Paula Mitre; y de Calchaquí, Oscar Cuello, entre otros.

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.1em; font-weight: 700">APORTES</span>**

En ese acto, Perotti entregó aportes, fondos y subsidios a municipios, comunas y clubes del departamento Vera por un total de $23.365.569. En conceptos de Fondos Covid se otorgaron fondos a las comunas de Margarita ($500.000), Tartagal ($500.000), Los Amores ($450.000) y a la ciudad de Vera ($1.000.000).

El municipio de Calchaquí recibió $2.252.552 del Fondo de Obras Menores; en tanto que para gastos corrientes también recibieron fondos las localidades de Garabato ($1.483.000), Tartagal ($1.893.809), Calchaquí ($5.838.667), Los Tábanos ($844.902), Intiyaco ($1.779.943) y Fortín Olmos ($2.535.519).

Además, se entregó a la municipalidad de Vera un aporte no reintegrable de $3.457.173 pesos para la compra de materiales y montaje de la obra electromecánica a la Estación Elevadora N°6 de cloacas; y se otorgaron subsidios al Club Atlético y Cultural Estudiantes Unidos de Vera ($330.000 para obras de infraestructura) y al Club Atlético Recreativo Ferrocarril Santa Fe de Vera ($500.000 para culminar distintas obras).

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.1em; font-weight: 700">TRABAJAR PARA NUESTRA GENTE</span>**

«Que estemos en el departamento Vera un 30 de diciembre es una de las tantas formas que tenemos para decir que, a pesar de las adversidades, está la firme decisión de superarlas, de no decaer, de hacer frente a todas y cada una de ellas», dijo Frana, quien agregó que «hoy, al final del año, estamos en uno de los lugares de la provincia menos visitados por funcionarios, y acá está el gobernador con distintos tipos de respuesta a muchas de las demandas de municipios y comunas del departamento».

Luego, la ministra recordó que, tras hacer frente a las deudas heredadas y a priorizar el sistema de salud para afrontar la pandemia, «se retomaron las obras y se comenzaron muchísimas a lo largo de toda la provincia, no haciendo anuncios, sino abriendo sobres de licitaciones o inaugurando tramos, con un profundo acompañamiento de la Nación», y aseguró que “nuestro gran objetivo es trabajar para nuestra gente”.

Por su parte, el subsecretario de Comunas, Carlos Kauffman, expresó que «en un año difícil, en la provincia vamos a totalizar en el transcurso de 2020 a una transferencia de cerca de 5 mil millones de pesos a todos los municipios y comunas», montos correspondientes «a fondos Covid, financiamiento educativo y de obras menores, lo que significa un 300% más de lo que se destinó en 2019». En ese sentido, agregó que «en el departamento Vera, con la entrega de hoy, vamos a llegar a casi 100 millones de pesos. Deseamos que el año que viene lo podamos acrecentar».

Finalmente, la intendenta de Vera, Paula Mitre, dijo: «Estamos muy contentos después de un año tan difícil y duro. Poder cerrar este 2020 con la presencia del gobernador va a quedar en la historia» de esta región de la provincia.

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.1em; font-weight: 700">LICITACIÓN EN CALCHAQUÍ</span>**

Posteriormente, en el salón del Centro Municipal de la ciudad de Calchaquí, Omar Perotti encabezó el acto de apertura de sobres con ofertas económicas para la obra de la nueva estación terminal de ómnibus, trabajos que cuentan con un presupuesto oficial de $178.268.482.

«Hoy estamos licitando obras en toda la provincia, abriendo sobres, y cada una de estas obras que se está licitando tiene sus fondos detrás, tiene sus recursos asignados, creo que es la forma en la que hay que ganar previsibilidad y confianza», dijo el gobernador, quien afirmó que esa «es la forma en la que sentimos que se debe trabajar: primero decimos que vamos a hacer algo, y lo hacemos. No venimos a decir que vamos a hacer la terminal, estamos abriendo los sobres, y lo hacemos porque tenemos los recursos».

Al respecto, el gobernador mencionó que «sabemos que es necesario dinamizar el trabajo, generándolo en toda la provincia», y valoró la posibilidad «de estar abriendo una licitación, cumpliendo un deseo, una aspiración de una comunidad que crece, que es laboriosa, que se distingue en la región por su perfil emprendedor. Acompañar a esas comunidades en sus aspiraciones es lo que hay que estar haciendo».

A su turno, el intendente Cuello comentó que «todas las empresas que hoy se presentan a la licitación ya se comunicaron con trabajadores de nuestra comunidad, con las metalúrgicas, con los electricistas, los corralones, con albañiles, para pedirles presupuestos, alternativas de organización para que trabajen en esta obra», dijo, y destacó la importancia de «generar en Calchaquí expectativas de trabajo para nuestra gente, además de lo que en sí representa esta obra».

Las tareas para la nueva terminal cuentan con un plazo de ejecución de 8 meses y se realizarán en el predio delimitado por las calles Juan de Garay, Mitre, Estanislao López y Tucumán, sobre el margen oeste de la Ruta Nacional N°11.

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1em; font-weight: 700">En la oportunidad se presentaron siete oferentes</span>**

Coirini S.A., que ofertó $147.037.241; Constructora Pilatti S.A. cotizó $157.052.876; Dyscon S.A., por $187.535.881; Cocyar S.A., por $119.626.228; Werk Constructora S.A., por $148.680.000; Coemyc S.A., por $151.809.492; y la UTE Orion Ingeniería S.R.L.-MT S.R.L., por $157.900.450.