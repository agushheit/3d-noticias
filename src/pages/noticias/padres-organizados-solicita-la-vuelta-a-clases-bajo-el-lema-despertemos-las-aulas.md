---
category: Agenda Ciudadana
date: 2021-02-09T07:50:28-03:00
thumbnail: https://assets.3dnoticias.com.ar/clases-presenciales.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Padres Organizados solicita la vuelta a clases bajo el lema "Despertemos
  las aulas"
title: Padres Organizados solicita la vuelta a clases bajo el lema "Despertemos las
  aulas"
entradilla: 'Ante la proximidad del inicio del ciclo lectivo 2021 piden asegurar un
  comienzo con educación presencial en tiempo y forma. '

---
La agrupación "Padres Organizados" de la ciudad de Santa Fe emitió un comunicado solicitando certezas y acompañando a la movida nacional de "Aulas Abiertas" que se realiza esta tarde desde las 18 hs en distintos puntos del país. 

La demanda de los padres comenzó a mediados del año pasado, y por primera vez los integrantes de la agrupación santafesina fueron recibidos el lunes 1º de febrero por un miembro del gabinete del ministro de Nación Nicolás Trotta en el marco de su visita a Santa Fe.

El comunicado solicita que "ante la proximidad del inicio del ciclo lectivo 2021 y queremos asegurarnos un comienzo con educación presencial en tiempo y forma. Como padres estamos totalmente comprometidos por un derecho que consideramos esencial: las clases 100% presenciales de nuestros niños y adolescentes."

Aseguran  que buscan "que se trabaje dentro del aula. Ese lugar que se llena de magia cada vez que suena el timbre para ingresar. Ese lugar que devela los sueños, las ilusiones, los miedos y tristemente, la realidad dura de muchos niños que solo encuentran en el aula el lugar de contención, desahogo y, también, alimentos…"

Sostienen que la propuesta de las autoridades son "una serie de improvisaciones y claras intenciones del gobierno que propone un sistema bimodal, medida tomada totalmente inconsulta, sin fundamentos científicos y unilateral. Dejamos en claro en numerosas oportunidades que este sistema es contraproducente en la generación de hábitos, además de resultar imposible sostener así una organización familiar."

Además de asegurar que  tienen en cuenta "la salud de nuestros niños y por supuesto, de los maestros que están dando todo su esfuerzo por superar este “bache educativo” que aun no puede ser llenado, y especialmente, de los guerreros que arriesgan todos los días su vida en los hospitales, sanatorios y clínicas" piden a las autoridades terminar con promesas vacías de compromiso y considerar a la educación como un derecho esencial.

![](https://assets.3dnoticias.com.ar/hoja1.jpg)![](https://assets.3dnoticias.com.ar/hoja2.jpg)