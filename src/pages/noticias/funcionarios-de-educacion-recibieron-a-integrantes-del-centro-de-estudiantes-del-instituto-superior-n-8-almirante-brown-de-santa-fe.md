---
category: Estado Real
date: 2021-02-21T07:30:48-03:00
thumbnail: https://assets.3dnoticias.com.ar/ALMIRANTEB.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Funcionarios De Educación Recibieron A Integrantes Del Centro De Estudiantes
  Del Instituto Superior Nº 8 “Almirante Brown” De Santa Fe
title: Funcionarios De Educación Recibieron A Integrantes Del Centro De Estudiantes
  Del Instituto Superior Nº 8 “Almirante Brown” De Santa Fe
entradilla: Se planteó, entre otros temas, una agenda de trabajo participativa para
  avanzar en la resolución de las dificultades edilicias en el establecimiento educativo.

---
Autoridades del Ministerio de Educación de la provincia mantuvieron un encuentro con representantes del Centro de Estudiantes del Instituto Superior del Profesorado N° 8 " Almirante Brown" de la ciudad de Santa Fe, para analizar diferentes ítems relacionados a las condiciones de infraestructura de su edificio actual.

En la reunión, de la que participaron la subsecretaria de Educación Superior, Patricia Moscato; el referente de Acompañamiento a las Trayectorias, Francisco Corgnali; y el supervisor Jorge Solhaune, los funcionarios provinciales escucharon las demandas de los estudiantes en torno a la proyección de un edificio propio y exclusivo para el instituto superior.

En relación con la importancia del encuentro, la profesora Moscato señaló que “este tipo de diálogos abonan la construcción de estrategias concertadas que respondan a las necesidades reales de los estudiantes”.

En ese sentido, desde la cartera educativa se planteó la iniciativa de avanzar en una agenda de trabajo participativa para subsanar las dificultades de la infraestructura edilicia, el inicio del ciclo lectivo 2021 en el Nivel Superior y las condiciones de cursado en el primer semestre en los institutos superiores. Asimismo, se evaluó la experiencia de cursado en la distancia en el ciclo lectivo 2020.