---
category: Agenda Ciudadana
date: 2021-03-29T06:15:04-03:00
thumbnail: https://assets.3dnoticias.com.ar/nalisis.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Página 12
resumen: 'Coronavirus: en qué consisten las nuevas medidas del gobierno para enfrentar
  la segunda ola de contagios'
title: 'Coronavirus: en qué consisten las nuevas medidas del gobierno para enfrentar
  la segunda ola de contagios'
entradilla: Carla Vizzotti, alertó sobre la peligrosidad de la trasmisión comunitaria
  de las nuevas variantes del coronavirus.

---
Con el objetivo de amainar los contagios y en la previa del feriado de Semana Santa, el gobierno nacional estableció una serie de medidas para frenar la segunda ola de contagio. "El riesgo epidemiológico está creciendo en el país", estableció Santiago Cafiero, a la vez que la ministra de Salud, Carla Vizzotti, alertó sobre la peligrosidad de la trasmisión comunitaria de las nuevas variantes del coronavirus.

Las medidas son:

· Teletrabajo de la administración pública nacional: desde mañana hasta el miércoles inclusive el sector público nacional quedará eximido de asistir al lugar de trabajo y cumplirá sus tareas de modo remoto o por teletrabajo. El Gobierno nacional invitó a sumarse a esta medida a los gobiernos provinciales, municipales, poder legislativo, judicial y al sector privado.

· Turismo con cuidado: Ante la proximidad de Semana Santa, es fundamental redoblar los esfuerzos para fortalecer los cuidados y sostener el turismo con cuidado, evitando las actividades de alto riesgo, lo que será posible con el compromiso de las provincias, los municipios, el sector privado y la sociedad. Este verano se movilizaron 12.8 millones de argentinos y argentinas en el territorio nacional, lo que demuestra que es posible.

· Reunión con sectores del turismo: Mañana se realizarán reuniones entre los Ministerios de Salud, y de Turismo y Deportes con las cámaras de turismo, de transporte, y las aerolíneas, para fortalecer los protocolos de cuidado. Además, se va a convocar un Consejo Federal de Salud y de Turismo conjunto para trabajar de manera consensuada y federal.

· Educación: En relación con las actividades educativas, la presencialidad en las aulas es una prioridad y no debe confundirse con las actividades sociales que puedan derivar del encuentro de los niños, niñas y adolescentes.

· Continuar con las medidas de prevención: El Gobierno nacional destacó la importancia de continuar llevando adelante las medidas básicas de cuidado que son la distancia de dos metros, utilizar tapa boca, mantener la higiene de las manos, realizar las actividades al aire libre o en ambientes aislados. Además, destacó la importancia de la consulta precoz ante síntomas, de evitar el contacto con otras personas y de cumplir el aislamiento en caso de ser contacto estrecho de un caso.

**Nuevos indicadores de riesgo epidemiológico**

Por su parte, en la conferencia, Vizzotti y Cafiero también anunciaron el nuevo índice para definir el riesgo epidemiológico tanto de las grandes urbes como en los pueblos. Ahora los indicadores constarán de dos variantes (Razón de los casos e Incidencia) que serán la base para que el Gobierno nacional evalúe con los gobiernos provinciales y municipales nuevas medidas para evitar mayores contagios.

En ese sentido, con estos nuevos indicadores, los funcionarios establecieron que hay 45 departamentos de 13 provincias, que se encuentran en mayor riesgo, mientras que hay cuatro departamentos (CABA, Posadas, Corrientes y Córdoba), de cuatro provincias, que presentan una incidencia mayor y ya que se trata de grandes aglomerados urbanos, constituyen un riesgo elevado.

Los distritos donde aumentaron la razón (el cociente entre el número de casos confirmados acumulados en los últimos 14 días y el número de casos confirmados acumulados en los 14 días previo) y la incidencia (el número de casos confirmados acumulados en los últimos 14 días por 100.000 habitantes) son en las provincias de:

Buenos Aires: Avellaneda, Berazategui, Campana, Cañuelas, Chivilcoy, Ensenada, Esteban Echeverría, Florencio Varela, General Alvarado, General San Martín, Hurlingham, José C. Paz, La Plata, Lanús, Malvinas Argentinas, Morón, Necochea, Olavarría, Pilar, Quilmes, San Fernando, San Isidro, San Miguel, San Vicente, Tigre, Trenque Lauquen, y Vicente López.

Chaco: Mayor Luis J. Fontana

Córdoba: Tercero Arriba

Corrientes: Goya

Entre Ríos: Colón

Mendoza: Luján de Cuyo

Misiones: Montecarlo

San Juan: San Juan capital

Santa Cruz: Deseado

Santa Fe: Castellanos, Constitución, Rosario, San Lorenzo, San Martin,

Santiago del Estero: Santiago del Estero capital

Tucumán: San Miguel de Tucumán, Lules, Tafí Viejo, Yerba Buena