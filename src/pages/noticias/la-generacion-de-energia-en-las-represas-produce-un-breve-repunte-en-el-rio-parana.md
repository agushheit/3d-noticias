---
category: Agenda Ciudadana
date: 2021-09-09T06:00:34-03:00
thumbnail: https://assets.3dnoticias.com.ar/USINA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: La generación de energía en las represas produce un breve repunte en el río
  Paraná
title: La generación de energía en las represas produce un breve repunte en el río
  Paraná
entradilla: Con Brasil al borde del colapso hidroeléctrico, los embalses empezaron
  a utilizar reservas que hicieron liberar gran cantidad de agua-

---
El aumento de casi 30 centímetros del río Paraná en el Puerto Santa Fe hizo que el agua supere el nivel cero del hidrómetro local. En la última medición que registró Prefectura Naval Argentina (Seccional Santa Fe) la marca fue de 22 cm.

¿A qué se debe este incremento en la altura del río? y ¿Hasta cuándo seguirá con esta tendencia? Fueron las primeras preguntas que le hizo El Litoral a Gustavo Villa Uría, ingeniero en Recursos Hídricos y subsecretario de Obras Hidráulicas en el Ministerio de Obras Públicas de la Nación.

"Es un repunte precario y artificial que se dio por la bajante en los niveles de los embalses por una mayor generación de energía de Brasil, que atraviesa una situación crítica, lo que generó que se tenga que poner mayor volumen de energía disponible, ya que había varios millones de personas sin electricidad. Esa generación hizo liberar más agua, lo que sucede es que hace tiempo desde las represas vienen derivando sus reservas y hemos visto mucha más agua de la que naturalmente producía la cuenca natural", explicó Villa Uría.

Al indicar que esta agua que llega es "artificial", se refiere el ingeniero a que no se produjeron importantes lluvias en el sur de Brasil: "Llovió unos 20 mm cuando lo que se esperan son 300 mm", ejemplificó el especialista. Respecto a la liberación de agua por metros cúbicos por segundo (m³/seg) que se desprendió de las represas en las últimas semanas, Villa Uría detalló que este miércoles se registraba aguas abajo del río Iguazú (cuenca que alimenta al Paraná) unos 8.100 m³/seg , que salen desde la represa binacional de Itaipú.

"Hace unos días, se liberaron entre 10.000 y 11.000 m3/seg. Eso está bajando en función de la restricción de energía que está llevando adelante Brasil", indicó el subsecretario de Obras Hidráulicas en el Ministerio de Obras Públicas de la Nación. Esta gran cantidad liberada es lo que en estos días se evidencia en esta región y que por unas semanas más seguirá llegando esta agua, que trae un poco de alivio.

"Vemos con agrado esta subida del nivel porque nos ayuda a comenzar el período de mayor consumo de agua con más disponibilidad. Sabemos que esto no va a durar, con lo cual se seguirá analizando día a día", aseguró Villa Uría, en referencia a la provisión de agua potable para las poblaciones ribereñas que usan el recurso del Paraná y sus afluentes para satisfacer las necesidades básicas.

**Dos años más de niveles bajos**

Este alivio que recibe el Paraná en su prolongada crisis hídrica y que ayuda a reducir los problemas en la provisión de agua potable, no se extenderá por mucho tiempo, será cuestión de pocas semanas para que empiece un leve descenso, por enésima vez…

Este martes, el Instituto Nacional del Agua (INA) actualizó el informe con las proyecciones relacionadas al nivel del río en la ciudad de Santa Fe y la región. Se indica que para el 14 de septiembre el río llegaría a 1,27 mts; mientras que para el 21 de este mes, bajaría a 97 cm.

Desde el INA explicaron que "el caudal descargado desde la alta cuenca comienza a disminuir", y agregaron: "Prevalece una tendencia descendente en todas las secciones del río Paraná en territorio argentino. Continuará predominando en los próximos tres meses. Se controla semanalmente las tendencias hidrométricas en toda la cuenca para las periódicas actualizaciones de la evolución en los próximos meses".

Siendo un poco futuristas y lo que se prevé para el largo plazo, el ingeniero en Recursos Hídricos alertó que "las perspectivas son muy largas y estamos pensando que posiblemente tendremos dos años más de aguas bajas. No en niveles como los actuales, pero sí con baja altura, por lo que habrá que adaptarse a estas condiciones".

**Brasil y una profunda crisis energética**

En los últimos días, las noticias del vecino país generaron grandes preocupaciones sobre la situación energética. El presidente de Brasil, Jair Bolsonaro, dijo que su país, "está al límite de su capacidad energética", producto de la sequía histórica que golpea la capacidad de las centrales hidroeléctricas. En este sentido, anticipó que habrá aumentos de tarifas de luz y pidió a la población reducir el consumo en sus hogares.

"Les voy a pedir que apaguen la luz que no usan en sus casas, vamos a ahorrar entre todos energía", señaló en su discurso Bolsonaro. La crisis provocó que el órgano regulador recomendara prepararse para comprar energía a países vecinos antes de fin de año. "Algunas centrales hidroeléctricas pueden dejar de funcionar si esta crisis hidrológica sigue existiendo", remarcó el mandatario brasilero.

También el presidente sostuvo que la crisis hídrica, la peor en 91 años, es el principal motivo detrás del aumento del costo de la energía eléctrica. Es por esto que está en marcha el plan de racionamiento de energía que emprendió la administración pública federal hasta abril de 2022 para enfrentar los problemas del abastecimiento de los reservorios de las centrales hidroeléctricas.