---
category: La Ciudad
date: 2021-08-20T06:16:47-03:00
thumbnail: https://assets.3dnoticias.com.ar/POSTCOVID.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Santa Fe incorpora un consultorio "Post Covid"
title: Santa Fe incorpora un consultorio "Post Covid"
entradilla: El mismo atenderá a mayores de 16 años que continúen con síntomas transcurridos
  30 días posteriores al alta epidemiológica.

---
El Ministerio de Salud provincial incorporará, a partir del próximo lunes 23 de agosto, un consultorio “Post Covid” en el Centro de Especialidades Médicas Ambulatorias de Santa Fe (Cemafe) para pacientes que hayan cursado la enfermedad de Covid-19 de manera ambulatoria.

Esta área está destinada a personas mayores de 16 años que persisten con síntomas (falta de aire, dolor de pecho, cefalea, mareos, palpitaciones) transcurridos los 30 días posteriores al alta epidemiológica. El seguimiento inicial de los pacientes que hayan padecido Covid leve - moderado ambulatorio, tiene que ser realizado por el médico del centro de salud más cercano. Una vez realizada esta primera evaluación, el profesional del efector será quien solicite la consulta al equipo de especialistas del Cemafe.

El consultorio estará disponible los días lunes y martes de 12 a 18 horas, y miércoles y jueves de 12 a 16 horas, con turnos programados. El paciente deberá llevar a la consulta el alta epidemiológica y resultado del hisopado nasofaríngeo positivo (+). En cuanto a quienes fueron contactos estrechos deberán tener hisopado (+) de su contacto y/o conviviente y alta epidemiológica del paciente. Al respecto, el director del Cemafe, Fabián Mendoza, indicó que “el Covid es una enfermedad nueva y si bien aún no cuenta con recomendaciones estandarizadas, sabemos que puede afectar a más de un órgano”.

“Por lo cual es muy importante que quienes la hayan padecido, sobre todo en los casos moderados y graves, tengan un seguimiento y se realicen una serie de estudios para poder descartar secuelas que se pudieran presentar, especialmente cardíacas, respiratorias y neurológicas”, continuó.

Finalmente, Mendoza explicó que “en virtud de esto se realizó un trabajo conjunto entre el Servicio de Neumonología del hospital Cullen y el Cemafe donde proponemos un abordaje personalizado, interdisciplinario e integral del paciente, generando la posibilidad de consultar a un equipo, especialistas en clínica, alergista, neumonología, cardiología, psicología y enfermería con tecnología disponible de baja, mediana y alta complejidad en diagnóstico por imágenes según lo requiera el caso”.