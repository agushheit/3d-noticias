---
layout: Noticia con imagen
author: "Fuente: La Nación"
resumen: Jubilaciones y pensiones
category: Agenda Ciudadana
title: "Jubilaciones: las incongruencias del proyecto del Gobierno para ajustar
  los haberes"
entradilla: A partir de marzo de 2021 las jubilaciones y pensiones tendrán
  reajustes semestrales.
date: 2020-11-15T15:01:11.741Z
thumbnail: https://assets.3dnoticias.com.ar/jubilados-1.jpg
---
A partir de marzo de 2021 las jubilaciones y pensiones tendrán reajustes semestrales y los porcentajes dependerán de la variación que tengan los salarios formales y la recaudación de impuestos que se destina, parcial o totalmente, al pago de prestaciones a cargo de la Anses. Los aumentos estarán sujetos a un tope, vinculado con los recursos totales que recibe el organismo y, en cambio, no tendrán piso. Será así, claro, si se aprueba en el Congreso el proyecto de ley que envió el Poder Ejecutivo el jueves último, tras el anuncio del lunes por la noche.

El comunicado oficial sobre la fórmula, emitido tras una reunión que encabezó el ministro de Economía, Martín Guzmán, y de la que participaron funcionarios del Gobierno y legisladores del Frente de Todos, incluyó un doble mensaje. La iniciativa se presentó con la promesa de que los haberes subirán por arriba de la inflación (una variable que, sin embargo, no está incluida ni en el cálculo, ni como base para una garantía de suba mínima) y, al mismo tiempo, con la afirmación de que lo propuesto permitirá bajar el déficit previsional, que explica en gran medida el desequilibrio fiscal, tema central en las conversiones con el Fondo Monetario Internacional, que comenzaron solo horas después de la nocturna difusión de la propuesta.

La exclusión de la inflación como una de las variables a observar y la alta sensibilidad -por la inclusión de la recaudación- a los ciclos económicos y a otras cuestiones no vinculadas con el poder adquisitivo de las jubilaciones, están entre las principales críticas que economistas y especialistas en seguridad social le hacen a la fórmula propuesta. Además, el proyecto abre un espacio a la discrecionalidad, porque postula delegar a la reglamentación, que puede ser inestable, algunas definiciones referidas al cálculo, en lugar de procurar que quede todo definido en la ley.

Otro aspecto crítico es la falta de transparencia que podría haber con una fórmula que es compleja y que no sería del todo nueva: es muy similar, de hecho, a la que estuvo vigente entre 2009 y 2017. Y en ese período nunca se hizo pública la totalidad de los datos utilizados para hacer los cálculos: eso impide el monitoreo, deja lugar a decisiones más políticas que técnicas y resta previsibilidad.

Hay también advertencias respecto de que cómo se plantea el tope. "Tras la suba de marzo, en septiembre se va a otorgar la movilidad que resulte más baja, al compararse el resultado de dos cálculos. Y por un componente de uno de esos cálculos, resulta que la suma de los incrementos de marzo y de septiembre no podrá ser mayor al aumento que haya tenido en un año la recaudación de todos los recursos destinados a la Anses, incrementado en un 3%", explica la abogada previsionalista Elsa Rodríguez Romero. En la práctica, agrega, solo se darían subas según la recaudación, salvo que el incremento de los salarios sea menor (en ese caso, esa evolución sí intervendría en el índice).

Más allá de los resultados que en los próximos años arroje la nueva modalidad, hay ya un punto de partida marcado por un ajuste a los haberes. La suspensión de la fórmula que rigió en 2018 y 2019 le permitió al gobierno de Alberto Fernández decidir subas con discrecionalidad. El resultado, hasta ahora y cuando aún falta conocer cuánto se dará en diciembre, es que los ingresos jubilatorios son más bajos de lo que serían bajo la vigencia de la ley 27.426.

Los aumentos por decreto fueron de entre 18,4% y 28,9% (dependiendo del nivel de ingresos), mientras que con la fórmula suspendida se habrían incrementado, todos por igual, un 35,9%. En el trimestre comprendido entre septiembre y este mes, por ejemplo, se cobra cada mes entre $996 menos (en el caso del haber mínimo) y $18.119 menos (en el ingreso máximo) de lo que se percibiría bajo la vigencia de la ley de fines de 2017.

Antes, entre 2018 y 2019, el bienio en el que rigió esa fórmula, los jubilados habían perdido un 14,5% de poder adquisitivo, con una suba acumulada de 94%, frente a una inflación de 127%. La modalidad fue pensada desde la expectativa de una inflación a la baja, que no se cumplió. Para hacer el cálculo se observaba lo ocurrido con los precios y los salarios con un rezago de seis meses. Al acelerarse la inflación, las subas de precios tomadas como referencia resultan de niveles más bajos que las de los meses cercanos a los aumentos.

A esa modalidad le fue mal en esos años de economía en recesión, pero el deterioro habría sido mayor si se hubiera implementado la modalidad ahora propuesta: la suba acumulada habría sido en tal caso de 70%, según una estimación hecha por el Ieral, y de 83% según cálculos que se hicieron dentro del mismo Gobierno.

**Cómo es la fórmula y cómo se comportaría**

Uno de los componentes de la fórmula que ahora se propone es la recaudación de recursos tributarios con destino a la Anses. El organismo recibe, por ejemplo, la totalidad del impuesto al cheque y parte del IVA, de los tributos a los combustibles y del impuesto PAIS, que pesa sobre la compra de dólares. Lo que se propone medir es la variación, entre "semestres idénticos de años consecutivos" de lo recaudado por esos impuestos dividido por los beneficios del sistema. La fórmula suma el 50% de esa variación y el 50% de la evolución semestral de la Remuneración Imponible Promedio de los Trabajadores Estables (Ripte). Y los datos a considerar tendrían un rezago de tres meses.

![](https://assets.3dnoticias.com.ar/jubilados-2.jpg)

La recaudación de recursos sería la variable central del mecanismo, porque, como ya se explicó, además de estar en la fórmula actuaría como límite para los incrementos del año, que no podrían superar la variación (multiplicada por 1,03) de la recaudación de los recursos asignados a la Anses por beneficio.

"No tiene sentido usar la recaudación como parte de la fórmula", señala Oscar Cetrángolo, investigador en el Instituto Interdisciplinario de Economía Política de la UBA y el Conicet. ¿La razón? Es una variable que nada tiene que ver con la meta de mantener el valor de los haberes previsionales y, en todo caso, considera, se usó en su momento cuando no había índices de inflación creíbles, a causa de la intervención política que sufrió el Indec bajo la presidencia de Cristina Kirchner.

Según cómo le vaya a la actividad económica, cómo se comporten las variables de la macroeconomía y cuáles sean las políticas tributarias, las variaciones de la recaudación y de la inflación bien pueden diferenciarse en más, o en menos. Por eso, el componente es rechazado por quienes dicen entender que el objetivo de la movilidad es dar estabilidad al valor de los haberes, evitando que haya reducciones o saltos positivos en función de cómo les va coyunturalmente a los recursos del Estado.

Hubo un mensaje desde el Gobierno con el cual se admite que no se espera un buen funcionamiento de la fórmula sin la condición del crecimiento: "Si la Argentina no crece, no hay fórmula de movilidad, ni fórmula de la Coca Cola que pueda favorecer a nadie", dijo la directora ejecutiva de la Anses, Fernanda Raverta al exponer en una reunión de la comisión mixta (integrada por funcionarios y legisladores del oficialismo y de la oposición), a la cual la ley le había asignado la misión de elaborar la propuesta, algo que, finalmente y pese a varias reuniones con ponencias de expertos, hizo el Poder Ejecutivo.

¿Resulta aceptable que, en un país con recurrencia de cambios de ciclos económicos, el Estado no pueda garantizarles a los jubilados que se mantendrá el valor de sus haberes, aun cuando no haya crecimiento? "En las últimas décadas, la Argentina ha sufrido muchos y a veces largos períodos de recesión, de manera que la fórmula previsional debe ser una que funcione bien en esos contextos", sostiene el economista Marcelo Capello, presidente del Ieral, de Fundación Mediterránea.

Según Capello, la ley debería buscar proteger al más débil, que es el jubilado y, por tanto, no trasladarle incertidumbre sobre las prestaciones futuras. Pero eso es precisamente lo que ocurrirá, advierte, con la fórmula que se propone, de la cual también critica la aplicación semestral de los reajustes, en lugar de la frecuencia trimestral que rigió en los últimos años. "Con inflación alta y, especialmente, con inflación creciente, los haberes tenderán a deteriorarse más entre un ajuste y otro" al tener períodos de espera más largos, dice.

El economista, que estuvo entre los consultados por la comisión, considera que la mejor opción es el ajuste por el índice de precios, porque eso haría más previsible el valor de los ingresos tanto en expansiones como en recesiones económicas. Además, agrega, "tendría la ventaja de que, en los períodos de crecimiento, el desequilibrio del sistema previsional tendería a bajar, porque el ritmo de la recaudación superaría a la inflación, según lo que muestra la experiencia".

En el análisis de los efectos que tienen los vaivenes de la economía entra en juego una propuesta que hicieron varios de los expertos que hablaron ante los integrantes de la comisión: la de un mecanismo de reajustes que considere a la inflación y que sea acompañado por un fondo anticíclico, que se nutra de recursos cuando la recaudación supere ciertos parámetros y del que puedan tomarse fondos en épocas de vacas flacas. De esa forma, el Estado tendría una herramienta para defenderse del "factor incertidumbre" (en lugar de trasladarlo al jubilado).

Esa alternativa no fue debatida. Tampoco hubo discusión de otras propuestas, como la de fijar, además de un tope, un piso para los aumentos, expresada por Claudia Danani y Sergio Rottenschweiler, investigadores de la Universidad Nacional de General Sarmiento.

En la primera reunión de la comisión tras los encuentros en los que se escuchó a los expertos, se recibió el proyecto desde el Poder Ejecutivo y, sin que se planteara un debate para eventuales cambios al texto, el oficialismo aprobó el dictamen.

Algo que sí se incorporó y que no existía en la fórmula de 2008 (de la cual la propuesta se diferencia también en otros aspectos), es una cláusula de "legislación constante", según el nombre con el que la anunció el Gobierno. El texto de la iniciativa dice, en rigor, que la comparación de los valores de los salarios, del número de beneficios del sistema y de la recaudación deberá hacerse "en forma homogénea". No se explica cómo lograr eso, sino que se deja librado a la reglamentación de la ley el establecimiento de los mecanismos de ajustes. Eso aporta complejidad, abre un espacio de discrecionalidad y no deja que la ley sea exhaustiva.

La cláusula tiene que ver con una de las principales críticas que recibe la inclusión de la recaudación impositiva en la fórmula: el hecho de que deje a los ingresos jubilatorios expuestos a los cambios tributarios o a los efectos del movimiento de otras variables de la economía.

Según advierte Rafael Rofman, investigador principal del Cippec y especialista en seguridad social, aumentar las jubilaciones según la recaudación no se ajusta al objetivo de la movilidad, por tres razones: es una variable que tiene movimientos muy volátiles, la información es poco transparente (hay que tener en cuenta que no se trata de la recaudación total, sino de una parte), y está sujeta a cambios frecuentes de reglas.

**Legislación constante**

Frente a la última crítica, en el Gobierno se sostiene que la cláusula de "legislación constante" logrará que las reformas tributarias o los cambios en el conjunto de tributos que van a la Anses, no tengan impacto.

¿Es aplicable eso? Si lo es, habría que hacer simulaciones para que, por ejemplo, el índice se calcule como si algún impuesto estuviera entre los que se derivan a la Anses (sin que esté), o como si otro impuesto no estuviera (aunque esté). Se desvirtuaría el principio según el cual es importante seguir la evolución de los recursos que recibe el sistema.

Esa cláusula "es una buena intención, pero es difícil de implementar", considera Rofman. Y agrega que, más allá de eso, las dificultades para desligar las decisiones de la política económica de la movilidad no solo están referidas a cambios normativos. "Si el Banco Central decide llevar el valor del dólar al doble, eso va a implicar un aumento del impuesto PAIS en un 100% -ejemplifica-. No hay un cambio de legislación, pero se produce un efecto sobre un componente de la fórmula. ¿Cómo incluir eso en la cláusula de legislación constante? Creo que es impracticable".

"La movilidad va a representar no solo la capacidad recaudatoria del Estado, sino también su voluntad política y, así, se van a generar conflictos entre posibles medidas", señala Rodríguez Romero quien, cuando expuso en la comisión, enfatizó en el concepto de que no se le debe pedir sustentabilidad a la movilidad, sino al sistema previsional en sí mismo.

Más allá de que el régimen es deficitario, los analistas advierten desde hace tiempo que hay varias cuestiones que deberían ponerse bajo análisis, para evaluar la viabilidad del sistema a mediano y largo plazo (por ejemplo, la creciente participación, entre los afiliados, de monotributistas con aportes bajos, y el impacto de los cambios demográficos). Y se señala que hubo medidas, como las moratorias, para las cuales no hubo evaluación previa de costos.

Rodríguez Romero describe que la sustentabilidad se vincula con el diseño del régimen, con las fuentes de financiamiento (aportes y contribuciones e impuestos varios), con quiénes son las personas eventualmente beneficiarias (los requisitos para jubilarse), con las contingencias a cubrir, con el cálculo del haber inicial y con la garantía de un ingreso mínimo. Según afirma, si se busca dar sustentabilidad con la movilidad, se les traslada a los jubilados una responsabilidad que no les es propia.

En cuanto a los salarios, el índice elegido para la fórmula es la Remuneración Imponible Promedio de los Trabajadores Estables (Ripte), el mismo que estaba, combinado con la inflación, en la anterior movilidad.

"El Ripte mide solo la parte de los salarios por la que se hacen aportes", dice el abogado Adrián Tróccoli, para señalar que algunas mejoras en los ingresos de los asalariados no se reflejan en ese indicador, que tampoco contempla los ingresos de quienes no estén declarados en el sistema jubilatorio durante 13 meses continuos.

El abogado advierte que un tema ausente es el de dar una compensación a los jubilados por la pérdida de poder adquisitivo de los últimos años y por el hecho de que este año se dieron subas diferenciadas y, hasta ahora, por debajo de lo que hubiera correspondido por la ley suspendida.

El defensor de la Tercera Edad de la ciudad de Buenos Aires, Eugenio Semino -que cree que los haberes deberían ajustarse según salarios y con una cláusula de garantía atada a la inflación-, considera vago discutir una fórmula sin plantear a la vez un camino de recomposición de ingresos. Junto con otros abogados, Semino patrocinó una causa en la Justicia contra la suspensión de la fórmula y las subas por decreto. Hasta ahora, el reclamo no tuvo avances y la decisión, afirma, es llevarlo a la Corte Interamericana de Derechos Humanos.