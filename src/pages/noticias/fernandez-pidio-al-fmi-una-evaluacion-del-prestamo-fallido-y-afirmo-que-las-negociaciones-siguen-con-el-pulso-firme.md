---
category: Agenda Ciudadana
date: 2021-12-03T06:00:51-03:00
thumbnail: https://assets.3dnoticias.com.ar/UNIONINDUSTRIAL.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: Fernández pidió al FMI una evaluación del "préstamo fallido" y afirmó que
  las negociaciones siguen con el pulso firme
title: Fernández pidió al FMI una evaluación del "préstamo fallido" y afirmó que las
  negociaciones siguen con el pulso firme
entradilla: '"Queremos lograr un acuerdo que nos permita refinanciar los abultadísimos
  vencimientos de deuda para los próximos tres años", resaltó el jefe de Estado.

'

---
El presidente Alberto Fernández pidió hoy al Fondo Monetario Internacional (FMI) una "evaluación" del préstamo "fallido" que se otorgó al gobierno de Mauricio Macri, y afirmó que las negociaciones para llegar a un acuerdo por la deuda de la Argentina con la entidad de crédito internacional siguen "a paso seguro y con el pulso firme".

"Seguimos negociando con el FMI a paso seguro y con el pulso firme. Queremos lograr un acuerdo que nos permita refinanciar los abultadísimos vencimientos de deuda para los próximos tres años a los que se había comprometido el gobierno de Mauricio Macri", resaltó Fernández.

Al participar del cierre de la 27º Conferencia organizada por la Unión Industrial Argentina (UIA), precisó: "Le pedimos al FMI que antes de que cerremos un nuevo acuerdo, haga su evaluación de lo que fue el fallido programa Stand-By".

En ese punto, el jefe de Estado reiteró que el préstamo, que implicó un desembolso de 44 mil millones de dólares, se utilizó para "pagar deuda insostenible y en financiar salida de capitales".

"Eso nos va a ayudar a terminar de entendernos. Es un paso necesario en este camino. Para poder escribir una nueva página, es necesario cerrar el capítulo anterior. Hagámoslo rápido y así avancemos sobre un terreno más firme y despejado", enfatizó.

Fernández aseguró que su Gobierno afrontará "las deudas que otros generaron", pero advirtió: "Ese acuerdo no será a costa del desarrollo del país ni en base a ningún programa de ajuste".

"Cuando somos enérgicos con nuestros acreedores demostramos nuestra seriedad. Nada serio puede resolverse en cinco minutos. El problema de la deuda externa no es un problema del gobierno de Alberto Fernández, es de la Argentina", enfatizó el Presidente.

En ese sentido, el jefe de Estado completó: "No trabajamos para sacarnos el problema de encima. Mi responsabilidad es trabajar para que la deuda no postergue más el desarrollo argentino".

"Buscamos que esa deuda no nos impida llevar adelante el programa plurianual que queremos implementar para que la Argentina continúe en la senda de la recuperación con generación de empleo, y para que podamos reducir la inflación", argumentó.

En un discurso leído, el Presidente destacó: "Tenemos una visión muy clara de cuál es ese programa plurianual que nos permitiría seguir en la senda de la recuperación virtuosa que hoy estamos viviendo. Estamos progresando en construir entendimientos con el FMI para que sobre esa base podamos llegar a un acuerdo".

Al referirse al aumento del precio de los alimentos, advirtió: "Veo con preocupación el aumento irracional del precio de los alimentos, aún sabiendo que hay factores exógenos que inciden. Seré inflexible con quienes se abusan en este contexto".

"Sabemos que hay problemas globales con la inflación, pero sabemos que en la situación argentina eso nos afecta seriamente. No es posible de que algunos pícaros aprovechen el momento para obtener ganancias extraordinarias. Vamos a ser muy exigentes en el cuidado de los precios de los productos de consumo masivo", subrayó.

En otro tramo de su exposición, Fernández aseguró que impulsará "políticas productivas que permitan lograr un auténtico renacer de la Argentina", pero alertó: "El desarrollo debe llegar a todos. No debe concentrarse en algunos".

Además, el Presidente anunció que "en las próximas semanas" firmará "un decreto para crear el Plan Productivo 2030", con el propósito de "potenciar" a los sectores productivos.

"En esta segunda etapa de mi gobierno, pondremos toda nuestra atención en enfrentar el problema de la pobreza y la exclusión. Sigo creyendo, como dije el primer día que en esta hora primero están los últimos", enfatizó.

Tras recordar las consecuencias de la pandemia de coronavirus, puntualizó: "Atravesamos un tiempo muy duro, que no nos deja ver que somos sobrevivientes de una pandemia, aquellos a los que la muerte no logró atrapar".

"Ese privilegio nos crea una necesidad ética y moral: construir el renacimiento de la Argentina, con diversidad, sin divisiones irreconciliables, sin crisis recurrentes, con desarrollo sostenido", agregó.

En el tramo final, Fernández sentenció: "Solo podemos soñar con un mejor futuro si cuidamos los esfuerzos que hemos hecho para que nuestra industria se vuelva a poner de pie. No podemos recaer en visiones anti-industriales, en aperturas indiscriminadas de la economía que destruyen a la industria nacional".

"Creemos que en la historia y en el mundo actual hay distintos tipos de capitalismo. Ni siempre ni en todas partes el capitalismo está dominado por la especulación financiera. El capitalismo no puede convivir con una democracia estable si no genera beneficios para todos los ciudadanos y ciudadanas", concluyó.