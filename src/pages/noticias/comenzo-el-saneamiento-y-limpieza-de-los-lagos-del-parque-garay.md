---
category: La Ciudad
date: 2021-09-28T06:15:51-03:00
thumbnail: https://assets.3dnoticias.com.ar/GARAY2.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Comenzó el saneamiento y limpieza de los lagos del Parque Garay
title: Comenzó el saneamiento y limpieza de los lagos del Parque Garay
entradilla: ". La tarea forma parte de un plan de recuperación de la Municipalidad
  de todo el espacio verde para la próxima temporada estival."

---
En uno de los puentes del Parque Juan de Garay, la Municipalidad ubicó un camión desobstructor que comenzó con la limpieza de los lagos. Es un trabajo que va a llevar unas semanas, y se va a intervenir en tres puntos del pulmón verde. La intención es levantar la basura y remover los sedimentos acumulados para tener agua de mejor calidad.

En paralelo, desde distintas secretarías del municipio, se hacen otros trabajos, como arreglos en los piletones, tareas de bacheo en el circuito vial y mantenimiento en el tendido eléctrico, entre otros con la idea de dejar el espacio en condiciones para la próxima temporada estival.

“Una de las prioridades del intendente Emilio Jatón es poner en condiciones todos los espacios públicos de la ciudad de Santa Fe para el verano 2021-2022. Sabemos que después de mucho tiempo de encierro por la pandemia el espacio público va a ser el protagonista y por eso estamos trabajando sobre ellos”, dijo el secretario de Obras y Espacio Público, Matías Pons Estel.

En esta línea, el funcionario agregó: “Es un parque que tiene un valor simbólico muy importante para los santafesinos y hace años que no tiene un trabajo de limpieza profunda ni de mantenimiento. Arrancamos con la instalación de tres bombas que inyectan agua de manera constante para que oxigene los lagos, entre otras acciones; y ahora se suma el camión aspirar la basura y luego una retroexcavadora levantará los sedimentos”.

Luego dijo que “la idea es poner en las mejores condiciones posible todo el parque” y en esa línea, contó: “El año pasado hicimos un trabajo muy fuerte en toda la zona de los piletones, y en la instalación eléctrica y este año tuvimos que retroceder unos pasos porque sufrimos muchos hechos de vandalismo, pero volvemos a trabajar sobre ello para poner en mejores condiciones el parque para el inicio de la temporada de verano y que todos los santafesinos y santafesinas recuperen este lugar”.

Además del saneamiento de los lagos, también hay otras intervenciones que se van a ir haciendo por etapas. “Se trata de un trabajo entre las distintas secretarías y va a llevar un tiempo y por orden de prioridades. Lo primero ahora es la limpieza del lago que hace más de una década que no se toca; le sigue la instalación eléctrica y los piletones, pero también están relevados los baches del circuito vial y sobre ellos también trabajaremos, es decir que son varios frentes en paralelo”, detalló Pons Estel.

**Materia pendiente**

Por su parte, el secretario de Ambiente del Municipio, Edgardo Seguro, dio más detalles sobre esta intervención y dijo: “Estos lagos son muy bonitos y lo que queremos es hacer una limpieza profunda, sin remover todo el fondo porque el agua no está en malas condiciones, pero se hacen las muestras correspondientes para poder mantenerla. Hay una diversidad muy interesante, muchas aves, peces, tortugas, entre otros; y la idea no es remover todo el fondo ni secar los lagos, sino sacar toda la basura y los sedimentos que quedaron estancados debajo de los puentes”.

Seguro anticipó que “la intención es también recuperar lo que tenía el viejo Parque Garay en sus lagos”, pero para que eso suceda “tenemos que tener un ambiente más sano, por eso la idea es ahora recuperarlo y hacerle un mantenimiento permanente y no permitir que se nos acumule basura debajo de los puentes”.

En cuanto al detalle sobre lo que se hizo este lunes, el secretario de Ambiente, explicó: “Se está haciendo la limpieza debajo de uno de los puentes donde hay un tabique y eso hizo que queden allí muchos desechos y es difícil sacarlo con los operarios y las maquinarias por eso se está usando una desobstrucción que tiene mucha potencia y además se hace un muestreo del agua para ver su calidad, que a simple vista no se ve en malas condiciones pero es esencial limpiar la superficie”.

![](https://assets.3dnoticias.com.ar/GARAY.jpg)