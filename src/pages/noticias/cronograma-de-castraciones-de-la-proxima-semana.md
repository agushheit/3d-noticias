---
category: La Ciudad
date: 2021-07-03T06:00:38-03:00
thumbnail: https://assets.3dnoticias.com.ar/CASTRACIONES.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Cronograma de castraciones de la próxima semana
title: Cronograma de castraciones de la próxima semana
entradilla: Con el objetivo de acercar el servicio a todos los barrios, la Municipalidad
  informa los lugares donde se realizarán castraciones desde el lunes 5 al jueves
  8 de julio.

---
La Municipalidad estableció el cronograma de castración móvil de la próxima semana, destinada a animales de compañía. Se recuerda que el servicio es gratuito y se ofrece a todos los vecinos y vecinas de la ciudad, con la intención de controlar la población felina y canina.

La atención se brinda con turnos programados que pueden obtenerse en la web oficial www.santafeciudad.gov.ar/turnos. Allí se debe buscar el apartado denominado “Castraciones”, se elige la opción “Solicitar un nuevo turno” y se completa el formulario con los datos pedidos. Posteriormente se escoge el día, el horario y la sede a la cual concurrir. El servicio se presta de lunes a viernes, excepto feriados, en el horario de 8 a 12, en todos los casos.

El esquema previsto desde el lunes 5 al jueves 8 de julio es el siguiente:

· Sede del Instituto Municipal de Salud Animal Parque Garay (Obispo Gelabert 3691)

· Sede del Instituto Municipal de Salud Animal Jardín Botánico (San José 8400)

· Distrito La Costa, Vecinal Alto Verde (Manzana 5)

· Distrito Norte, Vecinal Favaloro (Lamadrid 10550)

· Distrito Noroeste, Vecinal Scarafía (Alberti 5501, esquina Ignacio Crespo)

· Distrito Suroeste, manzana 6 del Fonavi San Jerónimo