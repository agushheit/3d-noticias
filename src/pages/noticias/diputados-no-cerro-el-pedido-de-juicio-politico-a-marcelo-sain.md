---
category: Agenda Ciudadana
date: 2021-03-26T08:17:16-03:00
thumbnail: https://assets.3dnoticias.com.ar/sain.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de El Litoral
resumen: Diputados no cerró el pedido de juicio político a Marcelo Sain
title: Diputados no cerró el pedido de juicio político a Marcelo Sain
entradilla: La mayoría dispuso una serie de medidas y recomendó al fiscal general
  que no acepte la reincorporación del ex ministro de Seguridad al Organismo de Investigaciones.
  El justicialismo mocionó el archivo del tema.

---
La Comisión de Juicio Político de la Cámara de Diputados resolvió mantener abierto el expediente del ex ministro de Seguridad, Marcelo Sain, y si bien no declaró admisible la presentación de Lucila Lehmann y Sebastián Julierac, dirigentes de la Coalición Cívica, la mayoría tampoco admitió el planteo de los justicialistas de archivar el tema ante la dimisión del denunciado.

La justicialista Matilde Bruera propuso archivar el tema; Oscar Martínez (Frente Renovador) pidió solicitar una copia oficial de la renuncia de Sain antes de enviarlo al archivo. Los restantes siete integrantes resolvieron una serie de pasos, entre ellos, invitar a los denunciantes a que ratifiquen y si desean ampliar la denuncia; al Poder Ejecutivo a que remita toda la información que tiene sobre el funcionario así como las razones de su salida mientras que a la justicia se pedirá que remita las denuncias que pudiera haber contra el ex funcionario.

Pero además el dictamen de la mayoría constituida recomendó al fiscal general, Jorge Baclini, que no resuelva el pedido de reincorporación a la dirección del Organismo de Investigaciones (OI) hasta tanto se termine el trámite del expediente en la Cámara Baja. El dictamen de mayoría lleva las firmas del presidente, Joaquín Blanco (PS), así como de Pablo Farías (PS), Clara García (PS), Marlen Espíndola (UCR), Georgina Orciani (UCR), Nicolás Mayoraz (Somos Vida y Familia) y Julián Galdeano (UCR - Juntos por el Cambio).

Blanco, Farías y Martínez estuvieron reunidos en el anexo de la Cámara de Diputados y los restantes seis integrantes participaron en forma remota. El encuentro se prolongó poco más de una hora antes de la firma de los tres dictámenes. Farías comentó después del encuentro que analizan la posibilidad de avanzar en la inhabilitación de Sain para ocupar cargos públicos en vista sus recientes actuaciones.

"Si renuncia el funcionario, no hay más juicio político" dijo Martínez recordando los fundamentos constitucionales que tiene la figura. No firmó el mismo dictamen que Bruera a la espera de que Omar Perotti envíe una nota oficial dando cuenta de la renuncia de Sain.

Este viernes saldrán las notas oficiales de la Comisión de Juicio Político al Poder Ejecutivo pero también al Poder Judicial que remita todas las actuaciones que se siguen al ex funcionario. "Pedimos al fiscal general y a la auditora del MPA (María Vranicich) que eviten la reincorporación al Organismo mientras se tramite el expediente" señaló Blanco.

"Es muy grave la denuncia, se han sumado nuevos elementos y hay un desprecio manifiesto por parte de Marcelo Sain y del Poder Ejecutivo de no acercar ningún tipo de documentación a la comisión de Juicio Político hasta la fecha" resaltó el socialista quien hizo notar que todavía la renuncia no fue publicada en el Boletín Oficial de la provincia.

Blanco insistió en que la comisión que preside no es jurídica, sino política. "Tenemos una responsabilidad política: ante una denuncia de una diputada nacional tenemos que verificarla, y después verificar si de esa denuncia no hay delitos penales que puedan estar implicados. La denuncia está en nuestro poder y de ninguna manera corresponde el mero archivo". El legislador consideró contradictorio que quien fuera ministro de Seguridad ahora intenta manejar el Organismo de Investigaciones "donde debería tener que investigarse asimismo. Es una contradicción y una paradoja inadmisible".

"Cuando el funcionario renuncia, el juicio político debe archivarse independientemente de que la Cámara o que los legisladores puedan dirigirse al MPA o a la Auditoría para seguir esgrimiendo las razones por las que entienden que Sain no puede volver al Organismo" afirmó Martínez al retirarse del edificio legislativo. 

Por su parte, Farías recordó que el juicio político puede terminar en la destitución del funcionario y la inhabilitación. "Si efectivamente Sain ha renunciado, no hay caso sobre destitución, pero sí para analizar si corresponde inhabilitar para ejercer cargos públicos. Entendemos que la inhabilitación debe analizar como posibilidad independiente de la destitución y por ende amerita continuar el juicio político".