---
category: Estado Real
date: 2021-02-28T06:00:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/industria.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Provincia articula políticas laborales con el Ministerio de Trabajo de
  la Nación
title: La Provincia articula políticas laborales con el Ministerio de Trabajo de la
  Nación
entradilla: Funcionarios y empresarios se reunieron en el Parque Industrial Sauce
  Viejo.

---
El ministro de Trabajo, Empleo y Seguridad Social de Santa Fe, Juan Manuel Pusineri, recibió al subsecretario de Articulación Territorial de Nación, Gerardo Girón, en el Parque Industrial Sauce Viejo, donde visitaron distintas instalaciones y conversaron con referentes de organizaciones empresariales para interiorizarse acerca de distintos aspectos de la realidad productiva de Santa Fe.

Al respecto, Pusineri destacó que “estamos viendo una recuperación de la actividad y el empleo muy importante. Nuestra obligación es acompañar y facilitar ese proceso con los programas de capacitación, de inserción laboral para jóvenes y de género, trabajando en conjunto con la Nación, los municipios, los empresarios y las organizaciones sindicales”.

En el encuentro también participaron el secretario de Coordinación y Gestión del MTEySS, Juan Gonzalez Utges; el presidente del Centro Comercial, Industrial y Agropecuario de Sauce Viejo, Julio Cauzzo; y el presidente Comunal, Pedro Uliambre.