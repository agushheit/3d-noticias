---
category: Agenda Ciudadana
date: 2021-01-03T11:13:45Z
thumbnail: https://assets.3dnoticias.com.ar/03012021-ANSES.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Está abierta la inscripción al Plan Potenciar Joven
title: Está abierta la inscripción al Plan Potenciar Joven
entradilla: Busca impulsar los emprendimientos de jóvenes de entre 18 y 29 años que
  se encuentren en situación de vulnerabilidad

---
El Gobierno lanzó el programa Potenciar Joven, que busca elaborar mecanismos comunitarios para el acompañamiento a chicos y chicas de entre 18 y 29 años en situación de vulnerabilidad, propiciando su acceso a recursos que garanticen sus derechos.

Tras la cancelación del Ingreso Familiar de Emergencia (IFE), el Gobierno ofrece como alternativa del bono de 10000 pesos que entregaba Anses, inscribirse en uno de los planes que lanzó el Ministerio de Desarrollo Social para seguir asistiendo a personas en situación de vulnerabilidad.

Por un lado, se unificaron algunos programas existentes, como el Hacemos Futuro, Salario Complementario y Proyectos Productivos para formar el plan Potenciar Trabajo, que busca mejorar la empleabilidad y la generación de nuevas propuestas productivas a través de la terminalidad educativa y la formación laboral.

Por otra parte, este año 2021 comienza la inscripción al Plan Potenciar Inclusión Joven que busca impulsar la participación de jóvenes a través de actividades educativas, formativas, sociales, culturales y creativas.

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1em; font-weight: 700">Detalles del Plan Potenciar Inclusión Joven</span>**

Es un programa de creación y fortalecimiento de estrategias de abordaje para la promoción y la inclusión social en situación de vulnerabilidad social.

Los que se inscriban contarán con una asignación mensual de $8500 pesos por 12 meses para la inclusión joven de manera directa e individualizada. A través de la creación de espacios de inclusión, también se planificará el financiamiento de distintos proyectos de inclusión joven a partir de diagnósticos y estrategias correspondientes a la realidad local implementando actividades y acciones de promoción de derechos de les jóvenes.

Además, se busca impulsar la participación de los jóvenes en actividades educativas, formativas, sociales, culturales y recreativas en el desarrollo de sus comunidades como actores fundamentales.

Así como también el programa financiará proyectos socioproductivos, sociolaborales, sociocomunitarios y socioeducativos llevados adelante por jóvenes.

Potenciar joven está destinado a personas desempleadas de entre 18 a 29 años, con experiencia en los siguientes rubros:

\- Carpintería

\- Gastronomía

\- Gomería

\- Jardinería

\- Peluquería

\- Textil

\- Diseño en Comunicación Audiovisual

Podrán obtener maquinarias y herramientas para poner en marcha fortalecer sus emprendimientos.

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1em; font-weight: 700">¿Quiénes van a ser parte del Potenciar Joven?</span>**

Se seleccionará a 370 proyectos ganadores, por medio de un comité de evaluación externo y los ganadores recibirán un kit determinado de maquinarias y herramientas para su emprendimiento.

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1em; font-weight: 700">Inscripción: cómo comenzar a tramitarla</span>**

Como primer paso, los interesados deberán enviar un correo electrónico a inclusionjoven@desarrollosocial.gob.ar

Una vez que hayan sido seleccionados, deben dirigirse a algunas de las organizaciones para continuar la inscripción.