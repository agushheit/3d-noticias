---
category: La Ciudad
date: 2021-10-29T06:00:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/spina.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'Ciudad de Santa Fe: el Concejo Municipal creará un Observatorio de Plataformas
  Digitales'
title: 'Ciudad de Santa Fe: el Concejo Municipal creará un Observatorio de Plataformas
  Digitales'
entradilla: "Servirá como órgano de consulta en cuestiones relacionadas con los distintos
  tipos de plataformas digitales que operan en la ciudad.\n\n"

---
El Concejo Municipal sancionó hoy una ordenanza para crear un “Observatorio de Plataformas Digitales” -OPD- que funcionará como órgano de “consulta, colaboración y participación” y deberá “articular la observación de los distintos tipos de plataformas digitales que operan en la ciudad”.

 Las plataformas digitales o virtuales permiten la ejecución de diversas aplicaciones o programas en un mismo lugar para satisfacer distintas necesidades y tienen diversos usos, tanto educativo como el comercio electrónico o la programación de servicios.

 Al respecto, la concejala Laura Spina, autora de la iniciativa, señaló que “en muchas jurisdicciones se han establecido observatorios de este tipo, para monitorear sus efectos en la sociedad, el trabajo, los comercios, empresas y los derechos de los consumidores”.

 El OPD deberá recomendar a la administración municipal políticas, regulaciones y estrategias; analizar las estrategias territoriales desplegadas por las plataformas que operan en la ciudad; estudiar su incidencia en el mercado laboral; analizar su impacto en las empresas locales y prácticas comerciales; evaluar el cumplimiento de la reglamentación municipal y tributación; y la gestión de quejas de los consumidores ante las plataformas, entre otras funciones.

 El Ejecutivo Municipal será el encargado de designar un Coordinador para el observatorio del que participarán las Secretarías de Producción, Hacienda, Control y Convivencia Ciudadana, las áreas de Informática, Comunicaciones; y cuatro concejales. Se invitará a un representante del Ministerio de Trabajo, a la Defensoría del Pueblo, a las Universidades Nacionales. Además, según los temas analizados, se podrá dar participación al Centro Comercial, Cámaras empresariales, Clusters tecnológicos y gremios de las actividades que se encuentren involucradas.