---
category: Estado Real
date: 2022-02-24T16:47:26-03:00
thumbnail: https://assets.3dnoticias.com.ar/escuela.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: "“El ministerio de Educación fomenta la ociosidad, propiciando la cultura
  del regalo y el desprecio al esfuerzo”"
title: "“El ministerio de Educación fomenta la ociosidad, propiciando la cultura del
  regalo y el desprecio al esfuerzo”"
entradilla: La agrupación Docente por la Educación acusó al Ministerio de Educación
  de la Provincia de Santa Fe de “seguir regalando la escolaridad de nuestros adolescentes
  y jóvenes. Seguimos quitándoles el futuro”.

---
En el comunicado afirman que: “El ministerio de Educación fomenta la ociosidad, propiciando la cultura del regalo y el desprecio al esfuerzo, en nombre de lo que   la circular 1-22 de la Subsecretaría de Educación Secundaria llama justicia curricular”.

“Según la misma, se pretende logar aprendizajes de calidad, pero, en el marco de la circular 8-21, donde se planteaba la aprobación de materias correlativas de los dos años de pandemia por haber participado el 70% del tercer trimestre del 2021”

Con difusión de la circular 1/22 para la educación del ciclo secundario, aseguran que “la nueva circular estable que aquellos alumnos con trayectorias intermitentes o de baja intensidad ‘promoverán al año escolar inmediato superior si dieron cuenta de apropiaciones de contenidos priorizados, en al menos 2 trayectos curriculares’. Es decir, pueden pasar de año con 4 o más materias previas. Además, el documento sostiene que ‘podrán acreditar los trayectos curriculares adeudados durante 2022 mediante la implementación de dispositivos situados’; léase trabajos prácticos sin instancia de evaluación”.

Con respecto a los alumnos de primer año de la secundaria “que no tengan aprobados alguno de los trayectos curriculares, ‘teniendo en cuenta que su trayectoria escolar implico haber transitado en pandemia el cierre del nivel primario y el inicio del secundario, promoverán de manera acompañada’, paran todos los de primer año”

“Parece que para nuestras autoridades la pandemia tiene la culpa de todo y se solución obligando a los docentes a aprobar a los alumnos y acreditando competencias que no tienen”.

Finalmente, y cerrando el comunicado, aseguran que “no solo los alumnos no aprenden y se los está estafando, sino que, además, les estamos enseñando que su esfuerzo no vale o que, aunque no hagan nada igual pasa de año, que se les darán múltiples oportunidades, que puede ir a la escuela a jugar y no estudiar. Entonces nos preguntamos, ¿es esto justicia curricular?, ¿es esto calidad educativa?”.

Link al comunicado

[https://twitter.com/Docentesxlaeduc/status/1496546073271312388?s=20&t=MG7q5V3y0hPRsC9KiXC-uA](https://twitter.com/Docentesxlaeduc/status/1496546073271312388?s=20&t=MG7q5V3y0hPRsC9KiXC-uA "https://twitter.com/Docentesxlaeduc/status/1496546073271312388?s=20&t=MG7q5V3y0hPRsC9KiXC-uA")

![](https://assets.3dnoticias.com.ar/docentes por la educacion.jpg)