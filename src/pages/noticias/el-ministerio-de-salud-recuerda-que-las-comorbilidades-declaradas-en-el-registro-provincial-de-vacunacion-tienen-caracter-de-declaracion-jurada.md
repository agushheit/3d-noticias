---
category: Agenda Ciudadana
date: 2021-06-04T08:16:45-03:00
thumbnail: https://assets.3dnoticias.com.ar/vacunación.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: El Ministerio de Salud recuerda que las comorbilidades declaradas en el Registro
  Provincial de Vacunación tienen carácter de Declaración Jurada
title: El Ministerio de Salud recuerda que las comorbilidades declaradas en el Registro
  Provincial de Vacunación tienen carácter de Declaración Jurada
entradilla: Desde la provincia insistieron en las consecuencias jurídicas que puede
  provocar no completar el formulario con información fehaciente.

---
En el marco del Plan Estratégico de Vacunación impulsado por el Ministerio de Salud de la Nación mediante la resolución 2883/2020, aquellas personas que formen parte de la población de 18 a 59 años con comorbilidades y se inscriban en el Registro Provincial de Vacunación deberán llevar un documento que respalde la situación declarada. Ante esto, en caso de no presentarlo, podrán completar una declaración jurada antes de ser inoculados.

A partir de mañana, en todos los vacunatorios de la provincia de Santa Fe se instará a firmar las declaraciones juradas a aquellas personas que no presenten la documentación respaldatoria.

Vale recordar que de acuerdo al artículo 293 del Código Penal, aquellas personas que presenten una declaración que no sea real o esté falseada pueden tener consecuencias penales.