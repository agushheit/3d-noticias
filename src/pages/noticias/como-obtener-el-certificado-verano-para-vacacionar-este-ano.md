---
category: Agenda Ciudadana
date: 2020-12-14T13:16:12Z
thumbnail: https://assets.3dnoticias.com.ar/verano.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Fuente: Ministerio de innovación publica de la Nación'
resumen: Como obtener el Certificado Verano para vacacionar este año
title: Como obtener el Certificado Verano para vacacionar este año
entradilla: 'Si el lugar que elegiste para irte de vacaciones te pide el Certificado
  Verano, podés tramitarlo desde 20 días antes de viajar. '

---
**Hacelo con tiempo, la solicitud se aprueba en 48hs.**

Cada provincia determina si el Certificado Verano es obligatorio o no para ir allí de vacaciones, así como si pide otros requisitos adicionales.

Si querés saber si te lo van a pedir en tu lugar de destino, consultá el mapa en: [www.argentina.gob.ar](https://www.argentina.gob.ar/verano#mapa "mapa")

Es recomendable que completes tu solicitud por lo menos 5 días antes de tu viaje. Te recordamos que la información que te solicitan para el certificado es la siguiente:

• el destino elegido;

• tus datos personales;

• datos personales de las personas que te acompañen;

• tu alojamiento (propio, alquilado, hotel, etc.);

• tu modo de transporte (auto, ómnibus, etc.).

Tras enviar el formulario, anotá el número de gestión, vas a necesitarlo para descargar el certificado.

Una vez que enviás tu solicitud, tenés que esperar la aprobación de tu lugar de destino, que puede demorar hasta 48 horas.

En ese período vas a recibir una notificación en el correo electrónico que informaste en el formulario. Si pasadas 48 horas no la recibiste, revisá tu carpeta de correo no deseado (spam) antes de iniciar cualquier reclamo o rehacer el trámite.

También podés consultar en línea si fue aprobado, sea con tu número de DNI o con tu número de documento extranjero, según el documento que hayas informado, y el número de tu gestión.

Si tu lugar de destino rechaza el trámite, debe informarte los motivos del rechazo.

**Si el destino que elegiste para veranear no aparece en el** [**mapa** ](https://www.argentina.gob.ar/verano#mapa "mapa") **puede ser porque la provincia no reglamentó todavía el uso del certificado**. A medida que las jurisdicciones se adhieran, se irán sumando más destinos.

**Podés llevarlo en la aplicación Cuidar, en la aplicación Mi Argentina, impreso o descargado en tu celular en formato PDF.**