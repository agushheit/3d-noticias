---
category: La Ciudad
date: 2020-12-28T10:38:00Z
thumbnail: https://assets.3dnoticias.com.ar/2812-castillo.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: El Litoral'
resumen: Confirmaron prisión perpetua al matador de la docente Vanesa Castillo
title: Confirmaron prisión perpetua al matador de la docente Vanesa Castillo
entradilla: El tribunal de apelación mantuvo la misma pena que se había impuesto a
  Juan Ramón Cano en el juicio oral y público llevado a cabo en septiembre de este
  año.

---
Además, se rechazó por improcedente un planteo en relación con una presunta imputabilidad disminuida del condenado. \[Crédito: Archivo El Litoral\]

Un tribunal de segunda instancia de Santa Fe confirmó la condena a prisión perpetua impuesta a Juan Ramón Cano de 34 años por la autoría del femicidio de Vanesa Castillo cometido en 2018 en Alto Verde.

La resolución fue tomada por los camaristas Fernando Gentile Bersano, Sebastián Creus y Alejandro Tizón, quienes confirmaron la decisión del tribunal de primera instancia en relación con la autoría material del femicidio. Asimismo, rechazaron por improcedente un planteo en el que se refería a una presunta imputabilidad disminuida del condenado.

La decisión del tribunal de segunda instancia fue tomada a raíz de la apelación presentada solo por la Defensa de Cano, ya que tanto la Fiscalía como la Querella no interpusieron ningún recurso contra la decisión del juicio oral y público que finalizó el 21 de septiembre de este año.

<br/>

### <span style="font-family: Helvetica, sans-serif; font-size: 1.1em; font-weight: 700">Confirmación</span>

La condena de primera instancia a Cano fue resuelta por unanimidad por el tribunal de juicio integrado por Rosana Carrara (presidenta), Leandro Lazzarini y José Luis García Troiano. Los magistrados le habían impuesto la pena por la autoría penalmente responsable del delito de homicidio triplemente calificado por alevosía, por ensañamiento y por ser perpetrado por un hombre contra una mujer mediando violencia de género (femicidio).

El tribunal también lo había condenado por la autoría de lesiones leves dolosas en perjuicio de un policía, delito por el cual fue absuelto en segunda instancia.

Las fiscales del MPA en el debate de primera y de segunda instancia fueron Cristina Ferraro y Bárbara Ilera. Por su parte, la madre y la hermana de Castillo –en representación de la hija menor de edad de la víctima– se constituyeron como querellantes y fueron representadas por la abogada Carolina Walker Torres y Matías Pautasso.

<br/>

### <span style="font-family: Helvetica, sans-serif; font-size: 1.1em; font-weight: 700"> Femicidio no íntimo</span>

Las fiscales del MPA destacaron que «los camaristas explican con claridad y realizan un aporte jurisprudencial a la figura del femicidio no íntimo». En tal sentido, agregaron que «los jueces de segunda instancia citan normas nacionales y tratados internacionales, jurisprudencia provincial y nacional y doctrina», y concluyeron que «una interpretación armónica de la normativa legal y supralegal aplicable al caso permite arribar mediante una intelección “sistemática” y armónica de la totalidad de normas que conforman el plexo normativo a la misma conclusión a la alcanzada por el tribunal de primera instancia».

Ferraro e Ilera también refirieron a la pretensión de la Defensa acerca de una presunta disminución de la imputabilidad del condenado y remarcaron que los camaristas son muy claros al sostener que «si bien el hecho presenta una violencia inusitada, excesiva –desde el punto de vista del resultado perseguido– la conducta no revela ausencia de coordinación de actos, realización de movimientos incoherentes o falta de ubicación espacial».

Las fiscales manifestaron que «los magistrados de segunda instancia hacen hincapié en que el accionar de Cano da cuenta de la **planificación, conciencia de haber realizado un acto incorrecto y temor a las consecuencias legales del mismo, lo cual es sumamente revelador de la conciencia de antijuridicidad**».

<br/>

### <span style="font-family: Helvetica, sans-serif; font-size: 1.1em; font-weight: 700">Violencia de género</span>

Las funcionarias del MPA también indicaron: «el fallo afirma que la Fiscalía contaba con indicios de que la muerte de la mujer podría encontrarse vinculada con dicho contexto (de violencia de género), esta advertencia motivó que investigue dicha cuestión (…) este razonamiento aparece claro en la actividad fiscal al requerir la realización del informe psicosocial del acusado».

Asimismo, especificaron que los camaristas sostienen que «el informe psicosocial (solicitado por la Fiscalía en la Investigación Penal Preparatoria) es sumamente revelador de la internalización de los rasgos inherentes a la violencia de género habiendo detectado patologías de índole psicológica y psiquiátricas de un hombre que concibe a las mujeres como un ser inferior el que se encuentra sometido a su voluntad y del que, consecuentemente, puede servirse a su voluntad, debiendo destacar que ello se revela incluso en relación con su madre (única persona con la que ha revelado algún vínculo afectivo) y hermanas».

Ferraro e Ilera indicaron que el tribunal expresa: «en definitiva, el hecho se desencadenó debido a que Cano concibió que Vanesa Castillo merecía la muerte debido a que no se sometió a su voluntad, cualquiera haya sido esta –someterla sexualmente, que lo traslade a su domicilio, etc.–. Esta circunstancia, junto con la modalidad comisiva y las concepciones interiorizadas por Cano a través de su vida, reveladas en el informe psicosocial, permiten determinar que el hecho fue cometido en un contexto de violencia de género».

<br/>

### <span style="font-family: Helvetica, sans-serif; font-size: 1.1em; font-weight: 700"> Indefensión</span>

El femicidio de Castillo fue cometido en horas del mediodía del jueves 15 de febrero de 2018, en inmediaciones de la escuela Victoriano Montes, ubicada en la Manzana 7 de Alto Verde.

«Cano interceptó a la víctima cuando salía de la institución educativa en la que trabajaba como docente, con la excusa de venderle calzado», recordaron Ferraro e Ilera. «Castillo rechazó la oferta, se acercó a su motocicleta y Cano le pidió que lo llevara hasta una cancha de fútbol cercana», relataron las fiscales.

Ferraro e Ilera añadieron que «luego de que la víctima se negara y mientras se subía a su motocicleta para volver a su casa, el condenado se abalanzó sobre ella, subió a la parte trasera del vehículo y le asestó numerosas puñaladas con un arma blanca en distintas partes del cuerpo».

«La docente quedó en una situación de indefensión total porque Cano la sujetó con tanta fuerza y de tal forma que la mujer comenzó a conducir mientras pedía auxilio. Luego, chocó contra un montículo de tierra, cayó al suelo y Cano se dio a la fuga», finalizaron las representantes del MPA.