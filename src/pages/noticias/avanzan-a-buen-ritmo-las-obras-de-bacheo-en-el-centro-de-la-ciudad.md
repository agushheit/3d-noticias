---
category: La Ciudad
date: 2021-03-20T06:00:25-03:00
thumbnail: https://assets.3dnoticias.com.ar/BACHE0.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Avanzan a buen ritmo las obras de bacheo en el centro de la ciudad
title: Avanzan a buen ritmo las obras de bacheo en el centro de la ciudad
entradilla: 'Se trata de los trabajos contemplados en el Plan de Bacheo que la Municipalidad
  puso en marcha el pasado enero. En paralelo, se concretan obras similares en las
  grandes avenidas de la capital provincial. '

---
A comienzos de año, la Municipalidad licitó la primera etapa de obras correspondientes al Plan de Bacheo 2021 que propone recuperar más de 13.000 metros cuadrados de calles y avenidas de la capital provincial. Esos trabajos, que comenzaron a desarrollarse en enero, se iniciaron por el micro y el macrocentro de la ciudad, las zonas más transitadas por vecinos y vecinas. El intendente Emilio Jatón supervisó los trabajos este viernes en avenida Freyre y La Rioja, donde se encuentra trabajando la empresa contratista.

De este modo, la Fase 1 contempla 4.500 m2 en el sector enmarcado entre los bulevares Pellegrini y Gálvez (al norte), avenida Freyre y Dr. Zavalla (al oeste), Rivadavia (al este), y J.J. Paso (al sur). Esta etapa, que inició con reparaciones con material de hormigón en la intersección de San Jerónimo y Presidente Illia, avanza a muy buen ritmo, a punto tal que ya lleva ejecutado el 60%.

Actualmente, la obra avanza por la mano oeste de avenida Freyre, desde el bulevar Pellegrini hasta General López y posteriormente se tomará el mano este. Se estima que en dos semanas se concluirá con ese corredor para iniciar el trabajo en las calles transversales que restan para finalizar la obra.

En paralelo se inició la Fase 2 que consiste en el bacheo de hormigón en avenidas troncales. Son más de 3.000 metros cuadrados que contemplan importantes arterias como General Paz, Peñaloza, presidente Perón, Blas Parera, Facundo Zuviría, Almirante Brown y Aristóbulo del Valle, ésta última, en la cual se trabaja por estos días.

Tanto en la zona céntrica como en las grandes avenidas, las tareas se realizan siempre en sentido del tránsito. En todos los casos operan reducciones de calzadas o cortes totales de circulación, a medida que avanzan las obras. Por este motivo, la Municipalidad solicita a vecinos y vecinas estar atentas a las comunicaciones oficiales sobre desvíos.

Por otra parte, se indicó que ambos frentes de trabajo se realizan en paralelo. La intención es avanzar lo más rápido posible, tanto en el centro como en las avenidas, a los efectos de continuar extendiéndose al resto de la capital provincial. Vale recordar que este tipo de obras siempre están sujetas a las condiciones climáticas.