---
layout: Noticia con imagen
author: Por Clara Blanco
resumen: Unión cayó ante Tucumán
category: Deportes
title: Unión no lo definió y cayó en su visita a Tucumán
entradilla: El equipo de Juan Manuel Azconzábal perdió por 3 a 1 en el
  Monumental José Fierro. Los goles del local fueron de Leonardo Heredia, Javier
  Toledo y Matías Alustiza. Para el Tatengue descontó Gabriel Carabajal de
  penal.
date: 2020-11-14T17:58:48.428Z
thumbnail: https://assets.3dnoticias.com.ar/uni%C3%B3n-tucuman.jpg
---
Unión llegó al Jardín de la República con el objetivo de ganar para treparse a la punta de la Zona 1. Sin embargo, la cancha de Atlético Tucumán siempre es un terrero difícil para el Tatengue y terminó perdiendo por 3 a 1. Los goles del local los marcaron Leonardo Heredia (ex Colón), Javier Toledo y Matías Alustiza. Mientras que para Unión descontó Gabriel Carabajal desde el punto penal.

El partido comenzó con mucha intensidad, pero ambos equipos no la aguantaron y la fuerza se fue diluyendo con el correr de los minutos. En el primer tiempo, el Tate no tuvo mayores ocasiones de gol salvo a los 45 minutos cuando Franco Troyansky se iba solo al arco de Lucchetti, pero el juez de línea marcó fuera de juego. Luego en las repeticiones de la televisión se pudo analizar que el Pocho no estaba adelantado.

Atlético Tucumán tuvo una más clara: Brian Blasi comente penal y Nazareno Arasa no dudó en cobrarlo. El encargado de patearlo fue Heredia, pero Sebastian Moyano contuvo ese disparo y evitó la caída de su vaya. Así los equipos se fueron al descanso con el marcador igualado en 0.

Los goles llegaron todos juntos en el complemento cuando en menos de 1 minutos el Decano marcó dos goles: el primero de Pupa Heredia, tomando revancha después del penal errado, y el segundo de Javier Toledo tras un error de Moyano.

Unión era y fue más que Atlético y de tanto presionar logró un penal. Gabriel Carabajal fue el jugador que se paró frente a Lucchetti y convirtió el descuento para el Tatengue. Sin embargo, el ingresado Matías Alustiza puso el 3 a 1 en el mejor momento del equipo santafesino y sobre el final del partido.

Esta es la primera derrota de Unión en lo que va del torneo y lo deja en segundo lugar de la Zona 1 con 4 puntos. Ahora deberá enfrentar a Arsenal el viernes 20 a las 17:10 en Sarandi.