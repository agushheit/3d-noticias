---
category: Agenda Ciudadana
date: 2021-04-08T06:10:04-03:00
thumbnail: https://assets.3dnoticias.com.ar/escuelas.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Diario Uno
resumen: Cantero dijo que en la escuela "no se producen contagios" y apuntó a las
  reuniones sociales
title: Cantero dijo que en la escuela "no se producen contagios" y apuntó a las reuniones
  sociales
entradilla: La ministra participó de la reunión del Consejo Federal de Educación.
  Dijo que en la provincia continuará la bimodalidad y descartó la presencialidad
  total.

---
Este martes se reunió el Consejo Federal de Educación, encuentro del cual participaron los ministros de educación de todo el país. En un contexto de posibles nuevas restricciones, el cónclave tuvo como una de uno de los temas centrales la continuidad o no de la presencialidad en el dictado de clases.

La ministra de Educación de Santa Fe, Adriana Cantero, advirtió que "la escuela no es un lugar donde se producen los contagios. La escuela es un lugar muy cuidado".

Y afirmó: "A la escuela llegan los contagios que se producen fuera de la misma. Por eso, es un lugar que todos intentamos seguir cuidando".

La titular de la cartera educativo pidió "que toda la comunidad intensifique los cuidados en otros ámbitos, donde circula (el virus). Fundamentalmente en las reuniones sociales que está demostrado que son los lugares de mayor contagio. También una reflexión a los padres por los viajes de estudiantes".

La funcionaria pidió que "todos aprendamos a cuidarnos fuera de la escuela, en los lugares donde circulamos. Porque los contagios hasta ahora, todos los estudios que se vienen haciendo indican que no son mayoritariamente en las escuelas".

"Las instituciones educativas siguen siendo un lugar cuidado; con distanciamiento social, con barbijo, con lavado de manos frecuente, con la aireación del aula", añadió.

Consultada sobre si la provincia evalúa la presencia total (hoy es bimodal, alterna presencialidad con virtualidad), la ministra respondió en declaraciones a la emisora Radio 2, de Rosario: "Por ahora vamos a seguir como estamos, es una forma posible en este tiempo y que estamos cuidando. Creo que no es momento de pensar en la presencialidad plena, que requiere reducir el distanciamiento social".

**Reunión de ministros**

Durante una reunión por videoconferencia que presidió el ministro de Educación Nicolás Trotta, los ministros coincidieron en asegurar que la evidencia disponible, tanto a nivel nacional como internacional, "nos permite afirmar que en las escuelas no se potencian los contagios del personal docente o de estudiantes".

"Por el contrario, en muchos casos los controles aplicados hasta el presente han permitido la detección de los mismos", indicaron.

En este sentido aseguraron que "hasta tanto la situación epidemiológica lo permita, mantendremos una presencialidad cuidada, en base a todos los acuerdos federales vigentes, y mediante la aplicación y control riguroso de los protocolos".

Indicaron, además, que de presentarse la necesidad de efectuar cierres parciales o totales, "se tomarán las decisiones teniendo en cuenta la menor unidad geográfica".

En esos casos se van a prever los escenarios para que la escuela que eventualmente tenga que cerrarse pueda volver a abrir "en cuanto se presente una ventana de oportunidad, que resguarde el bienestar de la comunidad educativa y de la sociedad en su conjunto".