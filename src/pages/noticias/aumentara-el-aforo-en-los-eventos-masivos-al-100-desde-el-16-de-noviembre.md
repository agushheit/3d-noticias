---
category: Agenda Ciudadana
date: 2021-10-30T06:15:56-03:00
thumbnail: https://assets.3dnoticias.com.ar/aforo100.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Aumentará el aforo en los eventos masivos al 100% desde el 16 de noviembre
title: Aumentará el aforo en los eventos masivos al 100% desde el 16 de noviembre
entradilla: Carla Vizzotti destacó que la situación se mantiene estable, aunque hay
  que seguir monitoreando para ver cómo evoluciona, y que la variante Delta está aumentando
  la proporción del número de infectados.

---
La ministra de Salud, Carla Vizzotti, anunció hoy que a partir del 16 de noviembre próximo se habilitará un aforo del 100 por ciento para los eventos masivos, en coincidencia con la disputa en San Juan del partido entre las selecciones de fútbol de Argentina y Brasil por las eliminatorias sudamericanas para el Mundial de 2022, a la vez que remarcó que octubre fue el mes con menos casos de coronavirus del año y que la situación epidemiológica se encuentra "estable".  
  
En una conferencia de prensa ofrecida esta tarde en la sede ministerial, la funcionaria dio un panorama general sobre la situación ante la pandemia de coronavirus.  
  
Allí explicó que el ingreso a los eventos masivos tendrá el requisito de al menos una dosis de vacuna contra el coronavirus y el uso de tapabocas cuando se desarrolle en lugares cerrados.

Con respecto a la situación epidemiológica, Vizzotti destacó que "octubre es el mes de menos casos del año" y que "tenemos un promedio de menos de 780 casos por día, muy bajo si lo comparamos con los casi 27 mil casos diarios del mes de mayo".  
  
También aseveró que hubo "20 semanas de descenso (de casos) y que las últimas dos semanas se produjo una pequeña fluctuación en los grandes conglomerados, generalmente a expensas de la circulación de la variante Delta".  
  
En cuanto a esa variante, dijo que "está aumentando la proporción en relación a otras en el número de casos, sobre todo en personas sin antecedentes de viaje y sin relación con alguien que haya tenido viajes. Tenemos el 61% total de país siendo delta y el 80% en el AMBA", aclaró.  
  
Y especificó que "se observa transmisión comunitaria en las provincias de Salta, Neuquén, Santa Fe y Tucumán; era lo esperado, el éxito de retrasar el ingreso, el éxito de retrasar la circulación predominante, y haber llegado a coberturas de vacunación con esquema completo satisfactorio".  
  
No obstante, destacó que la situación epidemiológica es "estable" y que se seguirá monitoreando "para poder definir como sigue esta tendencia del número de casos".  
  
También dijo que hay un aumento de la proporción de casos en los menores de 18 años. "Por eso es tan importante avanzar con la vacunación en adolescentes, niños y niñas", añadió.  
  
Remarcó que hay un descenso de las internaciones de pacientes con coronavirus, con alrededor de 600 personas ingresadas en terapia intensiva, "el número más bajo de todo el año si nos remontamos a junio del año pasado".  
  
La ministra añadió que "la mortalidad sigue descendiendo en la semana consecutiva número 20, con una disminución del 25% respecto a la semana previa".  
  
Entre los anuncios destacados también se encuentra la eliminación del test de antígeno al ingreso al país desde el 1 de noviembre, cuando se emita la decisión administrativa, "gracias a que ya tenemos el 56% de la población global que ha completado su esquema de vacunación y atento a que se ha cumplido el objetivo de retrasar la circulación de la variante Delta".  
  
Los vacunados que ingresan al país ya no realizan ese test y se planea retirarlo también con los no vacunados menores de 18 años.  
  
También se eliminará la realización de un test PCR entre el quinto y séptimo día para las personas vacunadas , mientras que los menores de 18 años, vacunados o no vacunados, y argentinos o extranjeros, estarán eximidos de cumplir la cuarentena.  
  
Por último, informó que se autoriza el transporte terrestre con ingreso al país a través de colectivos con el requisito del PCR al menos 72 horas antes y el esquema de vacunación completo.  
  
Y lo mismo en relación al transporte en ferry, donde se aumenta el aforo al 100%.  
  
Sobre la incorporación de Argentina en la llamada lista blanca, Vizzotti dijo que "es una recomendación de la Unión Europea para los países que la integran y que se debe a la situación epidemiológica que tenemos en nuestro país, que es muy favorable gracias a la campaña de vacunación".  
  
En este sentido, aclaró que es "muy importante" seguir cuidándose, completar los esquemas de vacunación, sostener la percepción de riesgo, testearse si se detectan síntomas y hacer el aislamiento.  
  
Para sostener a Argentina en la lista blanca "tenemos que tener una incidencia baja con una cantidad de testeos sostenida y una positividad baja".  
  
El país "ha logrado todos esos indicadores y tenemos que trabajar mucho para sostenerlos", destacó.  
  
Por último, consideró que "Argentina es un país seguro para el turismo receptivo".