---
category: Estado Real
date: 2021-01-17T10:27:59Z
thumbnail: https://assets.3dnoticias.com.ar/árboles.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: Plantamos 5 mil árboles durante el primer año del Programa Plantar para el
  Futuro
title: Plantamos 5 mil árboles durante el primer año del Programa Plantar para el
  Futuro
entradilla: La iniciativa, impulsada como acción concreta contra los efectos del cambio
  climático, incluye forestación con prioridad en especies nativas, capacitación,
  formación en arbolado público y cuidado de las especies.

---
El programa Plantar Para el Futuro, lanzado por el gobierno provincial a través del Ministerio de Ambiente y Cambio Climático en septiembre del año pasado, mostró resultados importantes al cierre de su primer año de rodaje. Llegando a 5 mil ejemplares plantados entre más de 50 municipios, comunas y organizaciones de la sociedad civil. El proceso de capacitación alcanzó a más de un centenar de personas vinculadas al arbolado urbano y periurbano.

“Se priorizan especies nativas y, en el año entrante, el objetivo es ampliar la cantidad de árboles, sentar las bases para la producción y lograr autoabastecimiento regional de los distintos ejemplares”, explicó Fabricio Fragapane, subsecretario de Protección de la Naturaleza.

Entre los logros también se destacan capacitaciones sobre arbolado urbano destinado a municipios y comunas con 157 participantes; y la formación de promotores ambientales en Santa Fe y Rosario en el marco del Plan Incluir.

“Lo que viene de aquí en adelante es cosechar mediante acciones políticas concretas, con las bases y los objetivos ya establecidos”, agregó Fragapane, quien tildó al proceso de arbolado público como “una herramienta de mitigación de los efectos del cambio climático e integradora de toda la sociedad”.

Con la mirada puesta en este año que recién comienza, el funcionario señaló que “el programa sigue en pie; la idea es seguir trabajando con todos los inscriptos en capacitaciones, entrega y plantación de árboles”. Y adelantó: “Estamos trabajando en una guía de arbolado para unificar criterios de plantación y conservación de árboles junto a municipios y comunas, fomentar y potenciar viveros locales y la producción propia, a partir de la recolección y clasificación de semillas para generar plantines de especies arbóreas en cada zona”.

Cabe agregar que Plantar para el Futuro incorporó la participación de instituciones intermedias, como actores principales de llegada al territorio y protagonistas en el cuidado de cada ejemplar plantado. Por su parte, las capacitaciones estuvieron a cargo de la Asociación Nacional de Arboricultura.

“Llevamos a cabo un convenio con el Instituto Nacional de Semillas (INASE) para que las semillas cuenten con certificación nacional. Así, junto al Ministerio de Producción, que administra la red provincial de viveros, y el aporte y asesoramiento científico académico del ETFI (Equipo Forestal Interinstitucional), potenciar la red de viveros en toda la provincia, para que en el mediano plazo cada región pueda autoabastecerse con árboles de calidad certificada según los protocolos técnicos correspondientes”, agregó.

El programa mencionado tiene una mirada innovadora de forestación, pensando no solo en plantar, sino también en la selección de las especies, en su mantenimiento y en la capacitación necesaria para lograrlo. Esto generó que el mismo sea declarado de interés federal por el Consejo Federal de Medio Ambiente. En su momento, la ministra de Ambiente y Cambio Climático, Erika Gonnet, destacó: “Junto al gobernador Omar Perotti presentamos este programa en el marco de las actividades por el Día del Árbol, poniendo en escena el rol de municipios y comunas, y por primera vez, incluyendo la posibilidad de que organizaciones de la sociedad civil formen parte”.

**PLANES DE ARBOLADO**

En este sentido, y como una de las herramientas para que municipios y comunas puedan acceder al programa, deberán presentar ante el Ministerio de Ambiente y Cambio Climático tanto su “Plan de Gestión Integral de Arbolado Público” como también, el “Plan de Avance Anual de Arbolado Público".

Todo en el marco de la Ley del Árbol y su Decreto Reglamentario. El plazo para dichas presentaciones vence el 1 de marzo del corriente año.