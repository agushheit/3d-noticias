---
category: Agenda Ciudadana
date: 2021-04-16T08:28:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/suarez.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Carlos Suarez
resumen: Carlos Suárez le solicitó al intendente que compre vacunas contra el Covid-19
title: Carlos Suárez le solicitó al intendente que compre vacunas contra el Covid-19
entradilla: A través de un proyecto de resolución, el concejal de UCR-Juntos por el
  Cambio le pidió al mandatario que evalúe la posibilidad de gestionar la compra de
  dosis para reforzar la vacunación en la ciudad.

---
A través de un proyecto de resolución, el concejal de UCR-Juntos por el Cambio le pidió al mandatario que evalúe la posibilidad de gestionar la compra de dosis para reforzar la vacunación en la ciudad. “Creemos en esta etapa de la pandemia, todos deben hacer su mayor esfuerzo y en esta línea, la Municipalidad puede hacer el intento para adquirir vacunas que aceleren el proceso de inmunización de los santafesinos”, afirmó.

El concejal de la UCR-Juntos por el Cambio Carlos Suárez, le solicitó al intendente que evalúe la posibilidad de gestionar la compra de vacunas destinadas a generar inmunidad contra el Covid-19. Para ello, en el proyecto de resolución presentado, se autoriza al Ejecutivo a celebrar los convenios que sean necesarios, con organismos públicos-privados, nacionales o internacionales. 

“Desde los diferentes Estados se le pide a la gente que haga su mayor esfuerzo y colabore con las medidas y protocolos para protegerse del Covid-19. En este sentido, creemos que todos los estamentos también deben hacer su mayor esfuerzo, y  por eso le solicitamos al intendente que haga el intento de adquirir dosis de la vacuna contra el Covid-19, para acelerar el proceso de inmunización de los vecinos de Santa Fe”, afirmó el edil opositor. 

En la misma línea, el concejal de UCR-Juntos por el Cambio recordó que el jefe de Gabinete del Gobierno Nacional realizó declaraciones públicas y ratificó que las provincias y las ciudades pueden adquirir vacunas, si cumplen con el procedimiento establecido. “En la provincia tenemos el ejemplo de Sunchales, que inició gestiones para la compra de dosis con el laboratorio de Oxford, como así también con laboratorios de Rusia, China e India. Estas negociaciones, según su intendente, se encuentran avanzadas y es por eso que solicitamos a la Municipalidad de Santa Fe que también inicie este camino de las gestiones propias para adquirir vacunas”, señaló.