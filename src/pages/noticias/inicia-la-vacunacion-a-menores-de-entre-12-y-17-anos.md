---
category: Agenda Ciudadana
date: 2021-08-02T06:15:20-03:00
thumbnail: https://assets.3dnoticias.com.ar/12.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Inicia la vacunación a menores de entre 12 y 17 años
title: Inicia la vacunación a menores de entre 12 y 17 años
entradilla: A partir de este martes comenzará la vacunación en 44 localidades santafesinas
  con la vacuna Moderna.

---
El Ministerio de Salud provincial informó que este lunes arribarán 70.280 dosis de la vacuna Moderna y durante la semana comenzará la vacunación contra el Covid-19 a menores de entre 12 y 17 años con factores de riesgo en 44 localidades del territorio santafesino. Los mismos deberán estar inscriptos en el Registro Provincial de Vacunación.

Cabe señalar que esta vacuna (Moderna) está sugerida y aprobada para niños y niñas que tengan entre 12 y 17 años, por lo que será la nueva estrategia de vacunación a nivel nacional.

Al respecto, el secretario de Salud, Jorge Prieto, expresó que “con los inscriptos al último viernes se ha completado la inoculación en 123 localidades de la provincia, y avanzando en el mega plan de vacunación para menores de edad, niños, niñas y adolescentes de 12 a 17 años. Contando hoy con una vacuna aprobada para ese rango etario, a partir del martes próximo se comenzará a vacunar en 44 localidades santafesinas”.

“En lo que respecta a la ciudad de Santa Fe, será en el hospital de niños Orlando Alassia - Mendoza 4151 -, mientras que, en Rosario, se hará lo propio en el Hospital de Niños Zona Norte - Av. de los Trabajadores 1331- y el efector Víctor J. Vilela -Virasoro 1855 -”.

“Los menores de edad de entre 12 y 17 años manifestarán su voluntad a través de la página web con su inscripción para recibir la vacuna Moderna, que sin dudas marcará una trayectoria importante en el implemento de la barrera epidemiológica frente a esta pandemia”, continuó el funcionario provincial.

Finalmente, Prieto indicó que “es de gran importancia que cada uno de los santafesinos puedan completar su esquema de vacunación y de esta manera lograr la inmunidad colectiva que nos permita enfrentar esta pandemia de una forma diferente, sin olvidarnos de que la principal barrera hoy sigue siendo la prevención”.

“Por eso pedimos que se inscriban, tenemos un padrón estimativo y contamos con las vacunas suficientes para que toda esta población pueda presentar una situación inmunológica diferente frente a esta pandemia”, concluyó el secretario de Salud.

**Población objetivo para la vacunación**

En esta primera etapa de inoculación con la vacuna Moderna para menores de edad de entre 12 y 17 años, la estrategia apunta a la inoculación de la población objetivo con las siguientes comorbilidades:

>> Diabetes tipo 1 o 2

>> Obesidad grado 2 (IMC > 35; puntaje Z > 2) y grado 3 (IMC > 40; puntaje Z > 3)

>> Enfermedad cardiovascular crónica: insuficiencia cardíaca, valvulopatías, miocardiopatías, hipertensión pulmonar, cardiopatías congénitas con insuficiencia cardíaca y/o cianóticas no corregidas.

>> Enfermedad renal crónica (incluidos pacientes en diálisis crónica). Síndrome nefrótico.

>> Enfermedad respiratoria crónica: Fibrosis quística, enfermedad intersticial pulmonar, asma grave. Requerimiento de oxígeno terapia, enfermedad grave de la vía aérea, hospitalizaciones por asma. Enfermedades neuromusculares con compromiso respiratorio.

>> Enfermedad hepática: cirrosis.

>> Personas que viven con VIH independientemente del CD4 y CV.

>> Pacientes en lista de espera para trasplante de órganos sólidos y trasplantados de órganos sólidos. Pacientes trasplante de células hematopoyéticas.

>> Pacientes oncológicos y oncohematológicos con diagnóstico reciente o “activa”.

>> Personas con tuberculosis activa.

>> Personas con discapacidad intelectual y del desarrollo.

>> Síndrome de Down (otros síndromes genéticos).

>> Personas con enfermedades autoinmunes y/o tratamientos inmunosupresores, inmunomoduladores o biológicos.

>> Inmunodeficiencias primarias.

>> Adolescentes que viven en lugares de larga estancia.

>> Personas de 12 a 17 años con Carnet Único de Discapacidad (CUD) vigente.

>> Personas de 12 a 17 años con pensión no contributiva con Certificado Médico Obligatorio.

>> Personas gestantes de 12 a 17 años previa evaluación riesgo/beneficio individual.

>> Malformaciones congénitas graves.