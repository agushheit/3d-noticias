---
category: Agenda Ciudadana
date: 2021-06-18T09:00:34-03:00
thumbnail: https://assets.3dnoticias.com.ar/registro-vacunacionjpg.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: 'Ansiosos a la espera: ¿Cómo averiguar si tengo el turno asignado para vacunarme?'
title: 'Ansiosos a la espera: ¿Cómo averiguar si tengo el turno asignado para vacunarme?'
entradilla: Crecen las dudas entre los que aun no fueron vacunados y esperan la notificación
  para hacerlo en Santa Fe. Se otorgan 40 mil turnos diarios.

---
Se acelera la llegada de vacunas al país y la campaña de vacunación en Santa Fe toma un ritmo más intenso. En este contexto crece la expectativa de adultos-jóvenes que, a pesar de no tener comorbilidades, esperan con ansiedad el llamado para ser vacunados.

Cabe recordar que desde esta semana se comenzaron a dar turnos y a inmunizar a personas mayores de 50 años sin patologías previas. El impulso que tomó la campaña, con el otorgamiento de 40 mil turnos diarios, pone a una gran mayoría en alerta ante la posibilidad de ser notificados con fecha y hora para recibir la primera dosis

Las dudas entorno a la posibilidad de tener un turno asignado sin saberlo (porque no les llegó el mensaje de texto o correo electrónico) provoca que muchos vayan al registro de vacunación a chequear dos informaciones: por un lado saber si efectivamente la persona se encuentra "registrada" y por el otro si fue turnada o aun no.

Para acceder a la información la tarea es sencilla. Primero es necesario acceder a la página oficial del registro Santa Fe vacuna. Solo es necesario colocar el DNI de la persona registrada y el número de trámite del mismo.

Para acceder hacé [click ACÁ](https://www.santafe.gob.ar/santafevacunacovid/inicio)

Para más información sobre la campaña de vacunación, [clic ACÁ](https://www.santafe.gob.ar/ms/santafevacuna/)

La Ministra de Salud, Sonia Martorano, dijo este jueves que "ya se otorgaron 180 mil turnos" en lo que va de la semana. "Continuamos a la espera de más dosis para seguir aumentando la cantidad de personas inoculadas", añadió.

La titular de la cartera sanitaria destacó el operativo que se realiza para inocular con la segunda dosis de Sputnik V, ya que “con las bajas temperaturas se realiza la vacunación en los automóviles para las personas mayores con el fin de que estén resguardadas”.

Explicó que se colocaron más de 1,3 millones de dosis de las cuales 1,1 millones son del primer componente.

“Tenemos que plantearnos metas cortas en tiempos tan acuciantes porque estamos transcurriendo una pandemia histórica. Por eso, lo inicial es llegar al 40%, no es inmunidad rebaño y no estamos tan lejos a este ritmo de vacunación”, agregó.

En ese sentido, el gobierno nacional informó el martes que la vacunación en Santa Fe con primeras dosis, en personas mayores de 18 años, asciende al 38,90 por ciento y la ubica por debajo del promedio general del país, que es del 39,04 %.