---
category: Agenda Ciudadana
date: 2021-02-14T06:00:03-03:00
thumbnail: https://assets.3dnoticias.com.ar/PAMI.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: PAMI anunció un aumento para el cuarto pago del Programa Plan Alimentario
title: PAMI anunció un aumento para el cuarto pago del Programa Plan Alimentario
entradilla: El pago corresponde a los meses de enero, febrero y marzo y reemplaza
  a los bolsones de alimentos que habitualmente se distribuían a través de centros
  de jubilados y pensionados.

---
El pago corresponde a los meses de enero, febrero y marzo y reemplaza a los bolsones de alimentos que habitualmente se distribuían a través de centros de jubilados y pensionados que por la pandemia deben permanecer cerrados.

PAMI pagará por cuarta vez una suma fija y extraordinaria a 550.000 personas afiliadas destinatarias del Programa Alimentario con un 10% de aumento respecto a la liquidación anterior. 

Los montos parten de $ 1.761 pero la asistencia puede ser mayor ya que la suma se ajusta al tipo de bolsón y zona de residencia en la que vive la persona afiliada. La asistencia económica se concretará a partir del 18 de febrero en los haberes de las personas afiliadas titulares de la prestación a través de un convenio que el instituto firmó con la Administración Nacional de la Seguridad Social (ANSES).