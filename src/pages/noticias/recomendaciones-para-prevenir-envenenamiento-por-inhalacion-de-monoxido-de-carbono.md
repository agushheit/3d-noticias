---
category: Agenda Ciudadana
date: 2021-06-17T21:51:28-03:00
thumbnail: https://assets.3dnoticias.com.ar/gas.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: Recomendaciones para prevenir envenenamiento por inhalación de monóxido de
  carbono
title: Recomendaciones para prevenir envenenamiento por inhalación de monóxido de
  carbono
entradilla: Ante las bajas temperaturas que se registran en esta época del año, el
  Ministerio de Salud brindó algunas recomendaciones.

---
El Ministerio de Salud de la provincia brindó diferentes recomendaciones para prevenir el envenenamiento por inhalación de monóxido de carbono (CO), ante las bajas temperaturas que se registran en estos días.

En ese marco, desde la cartera sanitaria se recordó que el CO se produce cuando se queman materiales combustibles como gas, gasolina, kerosén, carbón, petróleo o madera, y advirtieron que no se puede ver ni oler, con el peligro de que cuando se lo respira en niveles elevados, llega a causar la muerte por envenenamiento.

Las chimeneas, las calderas, los calentadores de agua o calefones y los aparatos domésticos que queman combustible como las estufas, hornallas de la cocina o calentadores a kerosén, también pueden producir CO si no están funcionando correctamente, al igual que los autos detenidos con el motor encendido.

**LOS SÍNTOMAS**

En relación a los síntomas que produce la inhalación del CO, informaron que los mismos corresponden a dolor de cabeza, mareos, debilidad, nauseas, vómitos, dolor en el pecho y confusión. Es de suma importancia aprender a reconocerlos para tomar medidas inmediatas.

**RECOMENDACIONES**

Las recomendaciones brindadas por la cartera sanitaria provincial están orientadas a alertar respecto de qué se debe hacer ante la aparición de síntomas de  envenenamiento mediante CO y cómo prevenirlo.

Por tal motivo, en caso de que alguien presente los síntomas descriptos deberá:

>> Dirigirse inmediatamente a un lugar donde pueda respirar aire fresco. Abrir las puertas y ventanas, apagar los aparatos que utilizan  combustibles y salir de la casa.

>> Dirigirse a una sala de emergencia o centro de salud y comunicar al médico que sospecha de envenenamiento con CO. El envenenamiento puede diagnosticarse con una prueba de sangre, hecha inmediatamente después de haber estado expuesto a él.

>> Al comenzar cada invierno, hacer inspeccionar por un técnico especializado todos los aparatos domésticos que utilizan combustible: calderas a gas, calentadores de agua, hornos y estufas u hornallas de gas, secadoras a gas, calentadores a kerosén o gas y también las chimeneas y estufas a leña. Todos los conductos deben estar bien conectados y en buenas condiciones y no deben estar bloqueados.

>> Escoger aparatos domésticos que eliminan los gases de la combustión hacia el exterior de su casa. Los aparatos deben instalarse correctamente y mantenerse según las instrucciones del fabricante.

>> Si se usa un calentador a kerosén o a gas que no tiene sistema de ventilación es necesario seguir cuidadosamente las instrucciones que trae el aparato. Usar el combustible apropiado y dejar abiertas las puertas que dan al resto de la casa. Mantener una ventana algo abierta para que entre aire y el consumo de combustible sea adecuado. De ser necesario llamar al servicio de emergencias 107.

>> No encender el motor de su auto dentro del garaje, aunque tenga la puerta abierta. El humo puede concentrarse rápidamente en el garaje o en la casa.

>> No usar el horno de gas para calentar la casa, aún por poco tiempo.

>> No usar carbón dentro de la casa, aún en la chimenea.

>> No irse a dormir dejando un calentador a gas o a kerosén encendido si el aparato no tiene ventilación hacia el exterior de la casa.

>> No usar aparatos con motores a gasolina –cortadoras de hierba o de maleza, sierras eléctricas, motores o generadores pequeños– dentro de un cuarto cerrado.

>> No ignorar los síntomas de envenenamiento con CO, especialmente si más de una persona siente los mismos síntomas. Ignorar estos síntomas puede llevar a la pérdida del conocimiento.