---
category: Agenda Ciudadana
date: 2021-12-11T06:15:49-03:00
thumbnail: https://assets.3dnoticias.com.ar/HAYTABLA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Dejan sin efecto la "tablita" que definía las licencias docentes por enfermedad
title: Dejan sin efecto la "tablita" que definía las licencias docentes por enfermedad
entradilla: 'El Ministerio de Educación provincial lo comunicó a los gremios en una
  reunión técnica de la paritaria realizada este viernes. Lo calificaron como "un
  triunfo de los gremios que integran la paritaria"

'

---
Una disposición del Ministerio de Educación, en 2016, establecía que las licencias por enfermedades debían ajustarse a una tabla que indicaba qué cantidad de días se les otorgaba a los docentes por el padecimiento y no a lo que indicaba la evaluación profesional de un médico.

A partir de la próxima semana el Ministerio dejará sin efecto la aplicación de la famosa "tablita" docente que establecía la cantidad de días de licencia que cada docente podía tomarse para determinada patología, según se informó a los gremios docentes de manera virtual en la reunión paritaria técnica realizada este viernes.

De la reunión paritaria participaron los cuatro gremios (Amsafé, Sadop, Amet y Uda) que forman parte de la discusión, además de la ministra de Educación, Adriana Cantero, y los responsables del área de Salud Laboral de la cartera educativa.

En 2016, el Ministerio de Educación de la provincia estipuló el máximo de días que un docente podía ausentarse de su escuela por determinadas enfermedades de corta duración. En su momento, el subsecretario de Recursos Humanos de la cartera educativa, Pablo Fernández, la definió como una forma de "ordenar" el sistema de ausentismo docente y evitar el "aprovechamiento indebido".

En diálogo con La Capital, el titular del Sadop Rosario, Martín Lucero, explicó que los gremios "veníamos pregonando desde el 2016 que la aplicación de la tablita era ilegal. Aparte no respetaba ningún criterio médico, porque el diagnóstico de una patología no tiene el mismo tiempo de recuperación para toda la gente. Tabular es dejar de lado el criterio de la salud para imponer un criterio estándar que no aplica de la misma forma en todas las personas".

El titular del gremio de los docentes particulares explicó que el pedido de derogación de la tablita "es un triunfo de los cuatro gremios que integran la paritaria. Siempre fue una lucha de todos. Siempre decimos que las licencias tienen que tener un criterio médico. Porque a lo mejor una patología tenía diez días de recuperación y por la tablita te daban dos. Entonces tenías que ir cinco veces al médico. Por eso decimos que la tablita no tenía razón de ser porque además no tenía ninguna justificación administrativa que la respaldara".

"Por supuesto que el Ministerio también aclaró que esta medida se va a dar con responsabilidad, que se van a auditar la cantidad de días que dan los médicos, porque se quiere evitar cualquier tipo de abuso. Pero sabemos que los docentes se van a comportar con responsabilidad", comentó Lucero sobre la resolución.

Lucero comentó que otra medida que satisfizo a los docentes es la facilitación del trámite de las licencias médicas docentes. "Hay que esperar para ver cómo van a implementar los procedimientos. Pero de la manera que lo comunicaron hoy se simplifica mucho el procedimiento de las licencias médicas. Va a tener valor el diagnóstico del médico, se va a poder adjuntar el certificado médico, la LM no se va a tener que mandar por mail sino cargar a través de la intranet".