---
category: Agenda Ciudadana
date: 2021-10-03T06:00:04-03:00
thumbnail: https://assets.3dnoticias.com.ar/CANCHA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: '"Yo no los llevaría", aconseja Sonia Martorano sobre los chicos en las canchas'
title: '"Yo no los llevaría", aconseja Sonia Martorano sobre los chicos en las canchas'
entradilla: La ministra de Salud de Santa Fe, Sonia Martorano definió estos eventos
  como "supercontagiadores" pero que tienen a favor que son "al aire libre".

---
Este jueves, el gobierno nacional oficializó cómo será la vuelta de los hinchas a las canchas de fútbol. Confirmó un aforo del 50 por ciento en los estadios y que los mayores de 18 tengan colocada al menos una dosis de la vacuna contra el coronavirus. Además, será necesario tramitar un nuevo certificado de circulación para eventos masivos, culturales y deportivos. En ese marco, la ministra de Salud de Santa Fe Sonia Martorano brindó algunas consideraciones y recomendaciones acerca del regreso de uno de los espectáculos más masivos que posee la provincia y el país.

La titular de la cartera sanitaria aclaró que "las decisiones (vinculadas al futbol de AFA) son nacionales y como jurisdicción (Santa Fe) no tenemos potestad de cambiar algo".

Explicó que, en la última reunión del Consejo Federal de Salud, los ministros dieron a conocer su parecer al respecto, opinión que no es vinculante con las decisiones que se tomen luego: "Nos pareció que son eventos que pueden ser supercontagiadores, está claro; pero lo que tienen a favor es que son al aire libre".

Señaló que lo más importante es usar "el barbijo, manteniendo las distancias y las burbujas". Indico que la recomendación del Cofesa fue que quienes concurran a los estadios "tengan una dosis, pero ir pensando que en las próximas fechas sea con vacunación completa".

Reconoció que el inconveniente va a presentar en "cómo se controla" a los hinchas que concurran. En ese sentido, dijo que la inquietud que se planteó en la reunión de expertos de la provincia fue que "más allá de los ingresos, el problema de estos eventos son los egresos".

Consultada sobre los menores de 18 que podrían concurrir al estadio, sector de la población que recién inicia con la vacunación, opinó: "Yo no llevaría a los chicos, es mi consejo médico; claramente no".

En declaraciones a los medios, Martorano comentó que "la preocupación sobre los menores, por no tener vacunación, fue una inquietud que surgió en la reunión de expertos. Si van, tendrían que estar en su burbuja, con su mamá, papá".

Igualmente, señaló con relación a la vacunación a este segmento de la población, que la provincia de Santa Fe se encuentra en una "buena" situación. "Estamos avanzando con los de 17 años. Las vacunas que se recibieron están colocadas; son las Pfizer. Estamos esperando más. Vamos a ir descendiendo, de 17 a 12. Recordemos que los que tenían factores de riesgo ya están con sus dos dosis", destacó.

Sostuvo, con relación a los eventos masivos, que siempre fueron pensados a que se desarrollen con pasaporte sanitario y con vacunación completa (dos dosis). "Creo que se va a ir solicitando, incluso a nivel nacional. Lamentablemente estamos viviendo lo que es una catástrofe sanitaria mundial. Lo que podemos hacer con esto es convivir. ¿Cómo lo hacemos? con dos cosas, primero sin miedo y para no tener miedo hay que tener conocimiento: tenemos que seguir usando el barbijo, seguir cuidándonos y que tenemos que vacunarnos. Y de esa manera poder ir restableciendo muchas de estas vivencias que tanto se extrañan".