---
category: Estado Real
date: 2021-10-07T06:00:20-03:00
thumbnail: https://assets.3dnoticias.com.ar/ruralidad.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Caminos de la ruralidad: La provincia firmó convenios para mejorar trazas
  en San Justo, La Penca y Caraguatá y Calchaquí'
title: 'Caminos de la ruralidad: La provincia firmó convenios para mejorar trazas
  en San Justo, La Penca y Caraguatá y Calchaquí'
entradilla: “Queremos que Santa Fe se convierta en la provincia que más caminos rurales
  tenga, para producir, para educar, para resolver los problemas de salud, y que dignifique
  la calidad de vida".

---
En el marco del Programa Caminos de la Ruralidad, el ministro de Producción, Ciencia y Tecnología, Daniel Costamagna; acompañado por el administrador provincial de Vialidad, Oscar Ceschi; y el director provincial Coordinación y Articulación Territorial, Matías Giorgetti; participó este miércoles de la firma de convenios con las localidades de los departamentos San Justo y Vera.

Los acuerdos fueron rubricados con San Justo, La Penca y Caraguatá y Calchaquí implican recursos por más de 133 millones de pesos y tendrán impacto en la producción, la educación y el arraigo en la región, mejorando y manteniendo los caminos rurales del territorio provincial, mediante el diseño de trazas viales.

Al respecto, Costamagna declaró: “Queremos que Santa Fe se convierta en la provincia que más caminos rurales tenga, para producir, para educar, para resolver los problemas de salud, y que dignifique la calidad de vida de todos aquellos que viven en el campo”.

Sobre los recursos que la provincia viene destinando al Programa, el ministro indicó: “Empezamos con 10 millones y hoy son más de 1000 millones de pesos invertidos en casi 500 kilómetros de caminos ripiados, que benefician a la producción y la educación, dando posibilidades a los docentes y a los alumnos de enseñar y aprender dignamente, junto al Boleto Educativo Rural”.

“Es una gran alegría poder ser parte de esto, que es lo que siempre anhelamos, ya que lo que necesitamos en el campo es la conectividad, es poder transitar, salir, producir, y resolver los problemas que se presentan a diario”, concluyó Costamagna.

Por su parte, Ceschi expresó: “Si bien el tema de los caminos pertenece al ámbito de Vialidad, en esta ocasión lo llevamos adelante en forma conjunta porque tiene que ver con la producción, para tener un análisis certero de dónde ejecutar estas obras. Es muy importante destacar que los propietarios deben ser los custodios de estos caminos”.

Asimismo, el titular de Vialidad Provincial indicó: “Este análisis se realiza a través del relevamiento que llevan adelante las direcciones de Articulación Territorial y la de Lechería en 12 departamentos de la provincia, donde hay escuelas, y producción, tratando de ser equitativos en cada distrito”.

**IMPACTO PRODUCTIVO EN SAN JUSTO**  
En el caso de la localidad de San Justo se destinarán 82.155.000 pesos para realizar la traza que cubrirá un tramo de 23.183 metros, incluyendo una escuela rural, que cuenta con 17 alumnos y 3 maestros; también incluye 11 productores ganaderos y 35 agrícolas, con una producción de más de 2500 toneladas de girasol, 1600 toneladas de maíz y 5000 toneladas de soja. También incluye 6 productores apícolas y 1 productor porcino.

El mismo se articulará a través del Consorcio de Propietarios Rurales. Al respecto, Costamagna señaló que “éste es el acuerdo más grande que se ha firmado con el Programa, y que queremos replicar, con una organización en esquema de consorcio”.

Del acto en el que se firmó el convenio participaron el intendente local, Nicolás Cuesta; el subsecretario de Desarrollo Territorial del Ministerio de Desarrollo Social, Santiago Felipoff, el presidente de Consorcios de Propietarios Rurales, Victor Predazoli; el secretario de Hacienda municipal, Luciano Neme y la directora de la Escuela N° 6185 “República Argentina”, Natalia Monzó.

**LA PENCA Y CARAGUATÁ**  
Por su parte, la Traza La Penca-Caraguatá, se extiende por 6.856 metros y se financiará con un aporte de 24.297.000 de pesos, e incluye la Escuela Nº 439 “Miguel Cabe”, que en la actualidad cuenta con 17 alumnos y 5 docentes. Además contiene en su recorrido 4 productores lecheros, que producen 5.239.000 litros de leche por año; contando también con 5 productores ganaderos, 3 agrícolas, y 2 equinos.

La directora de la Escuela N° 439 “Miguel Cabe”, Mónica Flores, destacó la importancia de los caminos para la institución y afirmó: “Este día es un hito, un momento clave para nosotros como institución educativa ya que era algo que anhelamos mucho, la ruralidad trabaja de forma colaborativa, siempre juntos y apoyándonos. Quiero compartirles esta frase -Venir juntos es un comienzo, permanecer juntos es un progreso y trabajar juntos es un éxito-, ojalá que la firma del convenio sea el comienzo de muchos otros”.

De la firma del acuerdo participaron también el presidente comunal, Edgardo Rabé; el directivo de la empresa Tregar, Vicente García; la directora de la Escuela N° 439 “Miguel Cabe”, Mónica Flores; la supervisora de la Regional IV del Ministerio de Educación, Liliana Rosef y autoridades de instituciones educativas de la región.

**CALCHAQUÍ**  
Finalmente, al camino planificado para la localidad de Calchaquí se destinarán 26.835.000 de pesos, y se extenderá por 7573 metros, incluyendo en su traza la Escuela CER Nº 568, que cuenta con 8 alumnos y 4 personas docentes y no docentes. También beneficiará a un productor lechero, 5 ganaderos, 4 agrícolas (destacando la producción algodonera), 3 apícolas y uno porcino.

En esta ocasión participaron del acto, el secretario de Desarrollo Territorial y Arraigo, Fabricio Medina; el intendente municipal, Rubén Cuello; el presidente de la Sociedad Rural de la localidad, Enzo Dotti; el presidente de la Asociación Civil Caminos Rurales; Luciano Sigaudo, el miembro de la sociedad rural y productor tambero, Martín Perelló; la supervisora de la región II sección I del Ministerio de Educación, Zulma Reinolch; y la directora de la Escuela N° 1378 -Escuela intercultural Bilingüe-, Mónica Toschi.