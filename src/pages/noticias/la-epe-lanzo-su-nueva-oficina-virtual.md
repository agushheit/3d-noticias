---
category: Agenda Ciudadana
date: 2021-10-13T06:00:46-03:00
thumbnail: https://assets.3dnoticias.com.ar/EPEVIRTUAL.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La EPE lanzó su nueva oficina virtual
title: La EPE lanzó su nueva oficina virtual
entradilla: Permitirá efectuar adhesión a la factura digital, débito automático, liquidaciones
  no recibidas y consultas de planes de pago, entre otras.

---
La Empresa Provincial de la Energía lanzó su nueva oficina virtual, más simple, ágil y segura, que permite efectuar gestiones comerciales más eficientes, con reducción de presencialidad y automatización de procesos, en el marco del plan EPE Digital que desarrolla esta distribuidora.

Los trámites virtuales que los usuarios podrán realizar son: adhesión a la factura digital, débito automático, liquidaciones no recibidas y consultas de planes de pago, entre otras.

La oficina virtual permite pagar las facturas a través de todas las plataformas habilitadas, verificar los pagos históricas, consultar los consumos de electricidad, actualizar datos y administrar distintos suministros.

La nueva web también permite canalizar reclamos técnicos, comerciales, denuncias de irregularidades, solicitudes de retiro de línea y reclamos por daños de artefactos.

Cada usuario del servicio eléctrico en todo el territorio provincial, podrá utilizar esta herramienta. El objetivo es brindar la posibilidad de una autogestión rápida, segura y sencilla desde cualquier dispositivo informático, independizando el lugar geográfico y horario en donde se encuentre.

Para registrarse, el usuario deberá adherirse en la página web de la EPE – www.epe.santafe.gov.ar/oficinavirtual, completando los datos requeridos y así recibir la factura por consumo de energía, vía mail.

La EPE recuerda que las facturas vencidas del servicio eléctrico podrán ser abonadas con un método similar al del homebanking, evitando impresiones, filas, traslados e inclusive sin extracciones de dinero.