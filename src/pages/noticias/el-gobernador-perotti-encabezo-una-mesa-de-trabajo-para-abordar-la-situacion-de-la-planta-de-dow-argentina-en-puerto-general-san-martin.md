---
category: Estado Real
date: 2021-09-15T06:15:24-03:00
thumbnail: https://assets.3dnoticias.com.ar/DOW.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: El Gobernador Perotti encabezó una mesa de trabajo para abordar la situación
  de la planta de Dow Argentina, en Puerto General San Martín
title: El Gobernador Perotti encabezó una mesa de trabajo para abordar la situación
  de la planta de Dow Argentina, en Puerto General San Martín
entradilla: 'La provincia, junto a la Nación, trabajan para lograr la continuidad
  productiva de la firma y la preservación de los puestos laborales. '

---
El gobernador de la provincia, Omar Perotti, encabezó este martes un encuentro para abordar la actual situación de la firma Dow Argentina dada la importancia de mantener la continuidad de la actividad productiva de la empresa en el país. El establecimiento petroquímico tiene su planta en la localidad de Puerto General San Martín, departamento San Lorenzo.

De la reunión participaron los ministros de Trabajo, Empleo y Seguridad Social, Juan Manuel Pusineri; y de la Producción, Ciencia y Tecnología, Daniel Costamagna; junto con el secretario general del Sindicato de Obreros y Empleados Petroquímicos Unidos (Soepu), Mauricio Brizuela.

Tras el encuentro, Pusineri ratificó la voluntad del gobierno provincial de que haya “continuidad de la empresa y de las fuentes laborales, con los actuales titulares o por medio de transferencia si deciden no seguir explotándola”, y en ese sentido destacó que “hay interesados en adquirir la firma”.

Del mismo modo, el funcionario manifestó una marcada inquietud: “No vemos porqué una empresa que es rentable y tiene que ver con la producción local, no pueda seguir en funcionamiento. Ese es el objetivo que tiene la provincia, en un trabajo conjunto que estamos encarando con la Nación”.

Por su parte, el ministro Daniel Costamagna dijo que mantener en funcionamiento la planta “es una cuestión central, por lo que representa esta empresa y por la implicancia que tiene en los sectores vinculados con la actividad. Son numerosos y hay muchos puestos de trabajo involucrados, de manera directa e indirecta, por lo que es fundamental el mantenimiento de esta unidad productiva, que además genera actividad para otras empresas e industrias”, explicó el ministro.

“Estamos trabajando, apoyando y acompañando a los trabajadores y a todos aquellos que tengan que ver con la producción de esta planta”, aseguró Costamagna, quien reconoció que “es un gran desafío, pero vamos a trabajar fuerte para preservar las fuentes de trabajo y el funcionamiento de esta planta”.

A su turno, el secretario general de Soepu, Mauricio Brizuela, valoró la conformación de la mesa de trabajo. “Es clave para lograr el objetivo”, dijo el gremialista, y agregó que “nos llevamos el compromiso de todos los actores de la provincia de hacer los esfuerzos para mantener en forma productiva la planta de Dow de Puerto General San Martín. Podemos decir que Nación y provincia están alineados para mantenerla en funcionamiento”, sintetizó. “Acordamos continuar el diálogo con una agenda abierta”, concluyó Brizuela.