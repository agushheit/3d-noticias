---
layout: Noticia con imagen
author: Por José Villagrán
resumen: Caen las ventas minoristas
category: Agenda Ciudadana
title: "Ventas minoristas: una nueva caída preocupa a las pymes"
entradilla: "La caída es de casi un 15%, según un estudio realizado por CAME que
  compara las ventas en octubre de 2019 y 2020. Dólar blue e incertidumbre en la
  economía, las principales razones de esta situación. "
date: 2020-11-03T12:04:39.123Z
thumbnail: https://assets.3dnoticias.com.ar/came.jpg
---
Terminó octubre y con ello se empiezan a conocer algunos datos de la economía real del país. Según un estudio realizado a 1300 comercios de todo el país y difundido por la Confederación de la Mediana Empresa (CAME), las vetas minoristas cayeron un 14,9% de forma interanual durante el mes que acaba de terminar. Así, en lo que va del año acumulan un descenso anual del 26,2%.

Entre los rubros que experimentaron una baja en sus ventas con respecto a octubre de 2019 se encuentran **Joyería, relojería y bijouterie** con un desplome de 36,1%, superando así la marca que había alcanzado el sector el mes pasado (-25,9%).

En ese mismo sentido, se encuentra el sector de **Calzados y Marroquinería** que declinó 35,9% anual. “Prácticamente no hay negocios que informen variaciones positivas” afirma el análisis de la CAME, dejando entrever que la crisis seguirá pegando fuerte a esta parte de la economía. Completa el podio de las mayores caídas **Jugueterías y librerías,** con una baja del 24,9%.

![](https://assets.3dnoticias.com.ar/pymes.jpg "Datos de la Confederación de la Mediana Empresa (CAME)")

**Las causas de la caída en las ventas**

Octubre fue un mes de mucha turbulencia. El salto del dólar blue, los rumores de una posible devaluación y las pocas señales de previsibilidad son las principales explicaciones que encuentra el sector para poder entender semejante realidad. Esto, según el informe “alentó el refugio en dólares de muchas familias, reduciendo la masa dedicada a consumo”, sin dejar de reconocer que “los problemas de ingresos, empleo, y los niveles de endeudamiento de las familias” siguen estando presente en esta realidad profundizada por la pandemia.

También se comenzaron a observar problemas referidos a ámbitos puntuales. Por ejemplo, las joyerías empiezan a “competir” con la proliferación de vendedores informales de estos productos, sobre todo en las grandes ciudades. Marroquinería siente la menor circulación de gente en la cuarentena, siendo que además uno de los últimos sectores en recuperarse.

Cabe destacar que las ventas minoristas registraron saldo negativo durante todo el año, pero luego del derrumbe interanual de 57,6 por ciento registrado en abril por la cuarentena la caída había comenzado a aminorar. En mayo el retroceso había sido de 50,6 por ciento, en junio de 34,8 por ciento, en julio de 27,7 por ciento, en agosto de 17,8 por ciento, y en septiembre de 10,1 por ciento. Sin embargo, la tendencia se quebró en octubre.