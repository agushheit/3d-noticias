---
category: Deportes
date: 2021-05-28T07:44:57-03:00
thumbnail: https://assets.3dnoticias.com.ar/colon-cai.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: "¿Qué canal emitirá la semifinal de Colón con Independiente?"
title: "¿Qué canal emitirá la semifinal de Colón con Independiente?"
entradilla: Se acerca el momento esperado para Colón en su duelo contra Independiente
  por la semifinal de la Copa de la Liga Profesional. Ya hay pantalla para San Juan

---
La semifinal entre Colón e Independiente ahora sí parece estar a la vuelta de la esquina. También pensarán lo mismo Boca y Racing, los primeros en saltar a la cancha en el Bicentenario de San Juan. Y así cómo se preparan los equipos para jugar una final, también tiene todo listo el canal que emitirá las acciones.

En este caso la resolución fue unificar la jornada de 31 de mayo a través de la pantalla de FOX Sports Premium, quien se encargará desde las 15 en narrar y televisar el duelo entre la Academia y el Xeneize.

Y apenas un par de horas después, por la misma señal, los televidentes podrán seguir el cotejo entre el Sabalero y el Diablo de Avellaneda.

Asimismo, el equipo de transmisión será el mismo, con Sebastián Vignolo en los relatos y Diego Latorre encargado de los comentarios.

Recordemos que los tres grandes que llegan a esta definición lograron en las últimas horas los objetivos propuestos: Racing y Boca están en los octavos de final de la Copa Libertadores. Por su parte, Independiente ganó su grupo y avanzó a los octavos de final de la Copa Sudamericana.

![Imagen](https://pbs.twimg.com/media/E2Z_KkEXEAEt38C?format=jpg&name=900x900)