---
category: La Ciudad
date: 2021-09-23T06:00:55-03:00
thumbnail: https://assets.3dnoticias.com.ar/BOLICHES.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: La zona de boliches abandonada y a la espera de la decisión para abrir
title: La zona de boliches abandonada y a la espera de la decisión para abrir
entradilla: Durante la pandemia se robaron el cableado del alumbrado público y la
  falta de desmalezamiento está a la vista. La otra cara es la de los empresarios
  de un sector que hace más de un año y medio está cerrado.

---
Uno de los sectores más castigados de la pandemia de Covid-19 es el de la nocturnidad, sobre todo los boliches, que hace más de un año y medio que están con las puertas cerradas y haciendo "malabares" para pagar deudas. Ahora habría un destello de luz al final del túnel, tras los anuncios realizados por el gobierno nacional y los dueños de estos espacios están expectantes a que el decreto provincial confirme esta habilitación.

"El sentimiento es ambiguo, porque si bien uno espera volver a trabajar después de casi dos años, la situación es muy mala no solo de la parte económica de los actores que ofrecemos este servicio, sino que estamos en un entorno deteriorado", indicó el empresario Jorge Reynoso, quien es uno de los propietarios del local bailable "La Pirámide", ubicado a la vera de la Ruta Nacional 168, frente a Ciudad Universitaria.

El "entorno deteriorado" al que hace alusión el empresario se evidencia claramente y a simple vista se refleja la falta de desmalezamiento y el mal estado del área que confluye a los boliches de este sector, que de noche está a oscuras porque durante la pandemia sustrajeron el cableado del alumbrado público. Ante esta situación, El Litoral consultó al municipio de las tareas de mantenimiento que harán en la zona, pero hasta el momento no estaban programadas y están a la espera de la decisión del gobierno de la Provincia.

"No hay luz en el sector, se la robaron. La zona está descuidada, falta desmalezar", aseveró Reynoso y al mismo tiempo destacó que "el municipio se comprometió a solucionarlo".

¿Cuándo será la reapertura?

Si bien aún resta definirse la aplicación del protocolo que adoptará la Provincia si es que decide habilitar los boliches, desde Nación lo expresado por la ministra Carla Vizzotti es habilitar las discotecas con aforo del 50% a partir del 1 de octubre, y las personas que asistan deberán contar con el esquema completo de vacunación (14 días previos al evento).

El referente de "La Pirámide" aún no tiene fecha de reapertura. "Esto nos cayó como un balde de agua fría. Tenemos que refaccionar nuestros locales, organizar eventos, volver a armar la estructura de difusión. Cuanto antes lo podamos hacer mejor, pero no está claro cuántos lugares van a abrir, muchos no lo van a hacer más", indicó el empresario y también deslizó que al mismo tiempo deberán analizar si esto "es negocio o no. Por ejemplo, yo tengo que pintar y refaccionar el lugar, es decir invertir mucho dinero. Pero si en dos semanas se da un rebrote y tenemos que cerrar otra vez ¿Qué hacemos?".

De ahí surge una incertidumbre de la que es difícil alejarse, porque lo planteado por Reynoso es así, la pandemia no terminó, más allá del levantamiento de las restricciones. Por ende lo que suceda de acá a un mes es totalmente incierto. "Hay posibilidades de volver a trabajar, pero el contexto es incierto y hacer inversiones hoy es muy complicado", comentó el empresario.

Si realiza el acondicionamiento de la discoteca y puede ponerlo en condiciones para la reapertura, el empresario mencionó que La Pirámide está habilitada para una capacidad de 6.439 personas, por lo que con el aforo del 50% podría dejar entrar a unas 3.220.

**Ordenanza "despareja"**

El bolichero recordó que las condiciones previas a la pandemia ya eran críticas para el sector de discotecas de la capital provincial, sobre todo luego de la puesta en vigencia de la ordenanza municipal (11.622), que se aprobó en 2009 pero que endureció sus controles en 2018.

"La ordenanza de relocalización vigente no fue exitosa, todo lo contrario. Antes de la pandemia nosotros estábamos agonizantes", lamentó Reynoso y recordó que en tiempos pre pandémicos "había mala accesibilidad, no había seguridad, ni senderos. La gente tenía horarios límites de ingreso y egreso, y nos terminaron dando la espalda porque la normativa no acompañó".

Una de las cuestiones que plantearon los referentes de las discotecas santafesinas fue que de la única manera que podrán volver a trabajar es que "el municipio aporte con las obras de mantenimiento para dejar el lugar ´saludable\` y también que el contexto tiene que acompañar. O se cumplen las ordenanzas o se llevan al Concejo y se vuelven a tratar", remarcó Reynoso y analizó la normativa y las condiciones desleales que observa: "Hubo un surgimiento de un polo gastronómico, por fuera de la ordenanza vigente, porque hay una desnaturalización de la norma al haber bares que están registrados de esa manera, pero funcionan como pubs y esta figura no está más en el nomenclador de espectáculos públicos".