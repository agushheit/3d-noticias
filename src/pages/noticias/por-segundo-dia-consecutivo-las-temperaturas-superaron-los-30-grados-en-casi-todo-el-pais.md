---
category: Agenda Ciudadana
date: 2021-10-28T06:15:46-03:00
thumbnail: https://assets.3dnoticias.com.ar/HOT.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Por segundo día consecutivo las temperaturas superaron los 30 grados en casi
  todo el país
title: Por segundo día consecutivo las temperaturas superaron los 30 grados en casi
  todo el país
entradilla: 'La mayor temperatura registrada en el día fue General Pico con 38.1°C,
  seguido de Termas de Río Hondo donde el termómetro alcanzó los 37.8°C y en Santa
  Rosa 36.9°C. '

---
Las altas temperaturas que se registran en gran parte del país continúan por segundo día consecutivo con temperaturas que, cerca del mediodía, superaban los 30 grados en diecisiete provincias, informó este miércoles el Servicio Meteorológico Nacional que estimó que hacia el fin de semana comenzarán a descender las marcas térmicas.  
  
El organismo emitió un alerta de temperaturas extremas de color amarillo para las provincias de Buenos Aires, Tucumán, Córdoba y San Luis.  
  
Esta categoría advierte que el calor puede tener un “efecto leve a moderado en la salud” y que estas marcas térmicas “pueden ser peligrosas, sobre todo para los grupos de riesgo como bebés y niños pequeños, mayores de 65 años y personas con enfermedades crónicas”.  
  
Las zonas afectadas por el alerta son: el sudoeste, norte y algunas localidades del primer, segundo y tercer cordón bonaerense, la provincia de Tucumán en su totalidad, el oeste de Córdoba y el sudoeste de San Luis.  
  
**Ciudad de Buenos Aires**

  
Según la información brindada por el organismo, pasadas las 14, la ciudad de Buenos Aires registró 33,3 grados, en una jornada en la que se espera que el tope sea 36.  
  
El martes, el distrito registró su máxima histórica para el mes de octubre con 36,3; récord que también se superaron en las localidades bonaerenses de San Fernando con 36.3, superando los 35,3 del 27 de octubre de 2014; en El Palomar con 35.3 (anterior récord 32,8; en octubre 2002) y en San Miguel con 35.2, apenas tres décimas más que en octubre de 2014.  
  
**Recomendaciones para afrontar el calor**

  
Se espera que las marcas térmicas continúen altas, por lo menos hasta el sábado en la región central del país, y ante esta situación se aconseja seguir las recomendaciones brindadas por el Ministerio de Salud como beber abundante agua, evitar las bebidas alcohólicas y azucaradas, no exponerse al sol de forma prolongada entre las 11 y las 17, comer liviano, reducir la actividad física, usar ropa liviana y de colores claros, al igual que sombrero y permanecer en espacios ventilados o acondicionados.  
  
**Ranking de temperaturas**

  
El ranking de temperaturas actualizado a las 14 por el SMN mostró un leve descenso de temperaturas comparadas a las del martes, sin embargo siguen siendo elevadas y, desde las 12, superan ampliamente los 30 grados en casi todo el país.  
  
En este sentido, la localidad que mayor temperatura registraba a las 14 fue General Pico, en La Pampa, con 38.1 grados, en tanto en Termas de Río Hondo, provincia de Santiago del Estero el termómetro alcanzó los 37.8 y en Santa Rosa, La Pampa, 36.9.  
  
Asimismo, San Fernando del Valle de Catamarca, registró 36.2; la ciudad cordobesa de Laboulaye 36.1 y las localidades bonaerenses de Bahía Blanca y Junín 36 y 35.7, respectivamente.  
  
Solo las provincias de Río Negro, Chubut, Santa Cruz y Neuquén no alcanzaron los 30 grados, aunque en esta última a las 14 la temperatura alcanzó los 29.2.  
  
"Esta semana va a ser la primera de calor intenso de la temporada”, sostuvo en diálogo con Télam, Cindy Fernández, meteoróloga del organismo que advirtió que el aumento de temperaturas comenzó el lunes y “va a continuar gran parte de la semana. Las temperaturas altas del país comenzarán a disminuir a partir del viernes desde el sur hasta el norte de forma paulatina”.  
  
Según la proyección del SMN, las temperaturas continuarán elevadas en CABA hasta el sábado, cuando se esperan 29 grados de máxima, jornada en donde comenzará un leve descenso de marcas térmicas que en el primer día de la semana próxima oscilarán entre los 16 y los 21 grados.  
  
El sudoeste de la provincia de Buenos Aires y la zona de Río Cuarto en Córdoba están afectadas por una alerta amarilla por temperaturas extremas altas, lo que implica "un efecto de leve a moderado en la salud, principalmente para las poblaciones de riesgo como bebés, niños menores de cinco años y personas con enfermedades crónicas", explicó Fernández.  
  
En tanto, en el norte de Santa Cruz y el sur de Chubut, se mantiene un alerta amarillo por “vientos del sector oeste con velocidades entre 45 y 65 kilómetros por hora, con ráfagas que pueden superar los 90”.  
  
Si bien las altas temperaturas son constantes y se registran en casi todo el país, el SMN advirtió que no es correcto denominar el fenómeno como ola de calor, ya que esto se produce cuando se superan durante tres días consecutivos "ciertos valores umbrales de temperaturas a partir de los cuales aumentaba la morbilidad y la mortalidad de las personas".