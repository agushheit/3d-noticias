---
category: La Ciudad
date: 2021-10-30T06:15:42-03:00
thumbnail: https://assets.3dnoticias.com.ar/lakalormavel.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Municipalidad recuerda las recomendaciones para prevenir golpes de calor
title: La Municipalidad recuerda las recomendaciones para prevenir golpes de calor
entradilla: En el marco de las elevadas temperaturas que se registran desde hace días,
  la Municipalidad brinda consejos sobre cómo actuar ante golpes de calor y deshidratación
  para evitar descompensaciones.

---
Ante las temperaturas elevadas que se registran de manera sostenida en los últimos días, la Municipalidad de Santa Fe brinda recomendaciones sobre cómo prevenir los golpes de calor y la deshidratación. Desde la dirección de Salud del municipio se recordó que ambas son cuestiones clínicas que sobrevienen cuando el cuerpo no puede regular su temperatura, la cual se eleva rápidamente, los mecanismos para eliminar calor fallan y el cuerpo pierde la capacidad de enfriarse.

El 60% del peso de una persona adulta es agua, por eso es importante hidratarse tomando 2 litros de ese líquido elemento todos los días y cuidarse de la exposición solar. Estas acciones son clave para prevenir enfermedades y mantener un buen estado de salud. Las mujeres embarazadas, los bebés, los niños pequeños y las personas mayores de 65 años suelen ser los más afectados por los golpes de calor en el verano.

A modo preventivo, la dirección de Salud municipal recuerdan la importancia de no exponerse al sol directo durante las 10 y las 16 horas, y de manera prolongada. Además, es menester usar ropa clara y liviana, colocarse protector, permanecer en espacios ventilados o acondicionados y, sobre todo, evitar hacer ejercicio en la hora pico: es totalmente desaconsejable hacer actividad física con una temperatura superior a los 30 grados.

Del mismo modo, califican como fundamental ingerir líquidos con mayor frecuencia, sobre todo ante exposición al sol, prácticas de ejercicios o al realizar actividad física. En ese sentido, recuerdan que los ancianos y los niños son quienes más deben controlar sus niveles de hidratación durante la época estival porque ven alterado su mecanismo de sed: los menores no siempre piden bebidas cuando tienen sed, mientras que los adultos mayores tienen menos agua corporal.

**Síntomas**

Sensación de mareo, náuseas, vómitos, dolores de cabeza o de estómago, debilidad, confusión, convulsiones y hasta pérdida de conciencia son los síntomas que puede presentar una persona que padece un golpe de calor.

Ante esta situación, se la debe posicionar boca arriba, en un sitio fresco, a la sombra y bien ventilado, así como también, quitarle la ropa que no necesite y aflojarle la que le oprime. También puede ayudar colocar compresas de agua fría en la cabeza, la cara, el cuello, la nuca y el pecho, e ir cambiándolas a medida que se calienten.

En caso de que esté consciente, se le deben ofrecer líquidos como agua segura, de rehidratación oral o bebidas isotónicas. Si está en estado de inconsciencia, es necesario activar de inmediato el sistema de emergencias sanitarias.