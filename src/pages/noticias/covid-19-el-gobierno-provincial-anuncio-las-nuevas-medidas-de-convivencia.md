---
category: Estado Real
date: 2021-10-02T06:15:43-03:00
thumbnail: https://assets.3dnoticias.com.ar/COVIDOCT.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Covid–19: el gobierno provincial anunció las nuevas medidas de convivencia'
title: 'Covid–19: el gobierno provincial anunció las nuevas medidas de convivencia'
entradilla: 'El decreto regirá a partir de las 0 horas del 2 de octubre de 2021. '

---
La ministra de Salud, Sonia Martorano; y el ministro de Trabajo, Empleo y Seguridad Social, Juan Manuel Pusineri, anunciaron este viernes las nuevas medidas de convivencia frente a la pandemia del Covid-19, que regirán en todo el territorio provincial a partir de las 0 horas del 2 de octubre de 2021.

En la oportunidad, Martorano detalló que “hoy toda la provincia de Santa Fe está en bajo riesgo. El promedio de casos en los últimos 14 días es de 60 por día, es el promedio más bajo desde el inicio de la pandemia. Esto tiene que ver con el trabajo realizado entre todos, de cuidados y vacunación”, dijo.

Sobre la vacunación, explicó que “el total de dosis aplicadas es superior a las 4.397.825. De primera dosis fueron 2.435.984. Lo interesante es evaluar cuántas de estas personas tienen esquema completo, es decir, dos dosis: 1.961.825. Esta semana estamos llegando a los dos millones de dosis completas en la provincia. Esto es muy importante porque el porcentaje es del 56% de la población general. Estas son buenas noticias”, puntualizó la ministra de Salud.

Por su parte, el ministro Pusineri sostuvo que “en función de la mejora del cuadro sanitario y del avance de la campaña de vacunación, el gobierno puede seguir instrumentando flexibilizaciones en las actividades económicas, comercial y recreativas”.

“Algunas de las medidas más importantes que van a comenzar a regir son la extensión de los horarios para las actividades gastronómicas y comerciales en una hora. Asimismo, en todas las actividades que hasta el momento tenían un aforo del 50%, ese aforo va a ser elevando al 70%”, agregó.

“Otras de las actividades que permite la afluencia de público son los eventos deportivos. Respecto de la actividad con las discotecas y salones de fiesta vamos a ir en un esquema progresivo, es decir, que las habilitaciones que se disponen son el comienzo de un esquema que vamos a ir monitoreando”.

Entre los requisitos para asistir a las nuevas actividades habilitadas, el ministro de Trabajo especificó que “deberán desarrollarse al aire libre, el aforo va a ser del 70% y quienes participen deberán hacerlo acreditando al menos una dosis de la vacuna”, concluyó el ministro de Trabajo.

En el desarrollo de cada una de las actividades mencionadas estará permitida la utilización máxima del aforo disponible que permita cumplir estrictamente con el distanciamiento social de las personas, sin exceder la ocupación del 70% de la superficie disponible.

El uso de elementos de protección que cubran nariz, boca y mentón será obligatorio en espacios cerrados de ingreso público y al aire libre cuando se diera la concurrencia simultánea o la cercanía de personas ajenas al grupo conviviente. Quedan alcanzadas:

a) Reuniones sociales en domicilios particulares y en espacios públicos.

b) Actividad deportiva en modalidad entrenamiento y recreativa de deportes grupales; gimnasios, natatorios y establecimientos afines.

c) Ejercicio de profesiones liberales, incluidos mandatarios, corredores y martilleros debidamente matriculados e inscriptos.

d) Actividad inmobiliaria y aseguradora.

e) Actividades administrativas de sindicatos, entidades gremiales, empresarias, cajas y colegios profesionales, entidades civiles y deportivas y obras sociales.

f) Actividades administrativas de las empresas industriales, de la construcción, comerciales o de servicios.

g) Actividades religiosas en iglesias, templos y lugares de culto, correspondientes a la Iglesia Católica Apostólica Romana y entidades religiosas inscriptas en el Registro Nacional de Cultos.

h) Actividad de los locales comerciales mayoristas y minoristas.

i) Locales gastronómicos (comprensivo de bares, restaurantes, heladerías y otros autorizados a funcionar como tales, con concurrencia de comensales).

j) Cines y complejos cinematográficos.

k) Competencias automovilísticas y motociclísticas, conforme lo dispuesto en los Artículos 11, 12 y 13 del Decreto Nº 1640/21; salvo las que se desarrollan con habilitaciones otorgadas por normas emanadas de autoridades del Gobierno Nacional, que se regirán por lo dispuesto en las mismas.

l) Realización de eventos culturales y recreativos relacionados con la actividad teatral y música en vivo que impliquen concurrencia de personas, tanto al aire libre como en teatros, centros culturales y otros lugares cerrados.

m) Actividad artística en plazas, parques y paseos.

n) Funcionamiento de locales de juegos infantiles y otros establecimientos afines, en espacios cerrados o al aire libre, comúnmente denominados miniclubs o peloteros.

ñ) Actividad en hipódromos y agencias hípicas.

o) Pesca deportiva y recreativa en la modalidad desde costa y embarcados, navegación recreativa o deportiva, y las actividades de guarderías náuticas, a los fines del retiro y depósito de las embarcaciones.

p) Actividad de los casinos y bingos.

q) La actividad de las ferias de comercialización de productos alimenticios y artesanías.

r) Actividad turística receptiva denominada de reuniones, congresos, jornadas o similares.

s) Discotecas y locales bailables, salones de fiestas para bailes o similares, con las demás condiciones establecidas en el presente Decreto.

En todo el territorio provincial, en el desarrollo de las actividades habilitadas, deberán cumplimentarse las reglas generales de conducta, los protocolos y condiciones específicas con las que fueron oportunamente aprobadas y sus ajustes posteriores, conforme las determinaciones y recomendaciones de la autoridad sanitaria; sin excederse del factor máximo de ocupación de las superficies.

En todo el territorio provincial, a partir de las 0 horas del 2 de octubre de 2021, queda restringida la circulación vehicular en la vía pública como a continuación se indica:

a) Los días sábados, domingos y feriados de 3 a 6 horas.

b) El resto de los días de la semana de 1 a 6 horas.

Quedan exceptuados de la restricción a la circulación vehicular dispuesta en el presente artículo la estrictamente necesaria para realizar actividades definidas como esenciales en la emergencia, comprensivas de las situaciones de fuerza mayor, y los desplazamientos desde y hacia los lugares de trabajo de los que desarrollan actividades habilitadas, incluidos los de los propietarios de los locales o establecimientos.

En los horarios establecidos de restricción de la circulación vehicular en la vía pública, quienes circulen en ocasión de concurrir a realizar o de haber realizado una actividad habilitada, deberán portar la documentación o constancia (reserva, ticket, factura, acreditación) demostrativa de esa circunstancia.

**Competencias deportivas**

Las competencias deportivas provinciales, zonales o locales de carácter profesional o amateur, que se desarrollen en espacios abiertos al aire libre o cubiertos con suficiente ventilación que se encuentran habilitadas podrán realizarse con la concurrencia simultánea de hasta mil espectadores, sin contar en esa cifra a los protagonistas del espectáculo, árbitros y auxiliares intervinientes. Cuando se tratare de competencias por equipos, queda autorizada la asistencia de público visitante.

Las competencias deportivas de carácter profesional o amateur, que se desarrollan con habilitaciones otorgadas por Decretos del Poder Ejecutivo Nacional, Decisiones Administrativas de la Jefatura de Gabinete de Ministros de la Nación y, en general, por normas emanadas de autoridades del Gobierno Nacional, se regirán por lo dispuesto en las mismas.

La actividad en hipódromos, con concurrencia de público, deberá ajustarse al máximo de mil personas asistentes; sin contar en esa cifra a los protagonistas del espectáculo y auxiliares intervinientes.

**Comercio mayorista y minorista**

La actividad del comercio mayorista y minorista de venta de mercaderías, con atención al público en los locales, se desarrollarán sin excederse del factor máximo de ocupación de las superficies; pudiendo extenderse todos los días de la semana hasta las 21 horas, con excepción de los kioscos y similares que podrán permanecer abiertos hasta las 24 horas para la atención al público residente en su cercanía; lo anterior sin perjuicio de la posibilidad de las farmacias de realizar los turnos de guardia.

Fuera del horario indicado en el presente Artículo solo podrán realizar actividad comercial a través de plataformas de comercio electrónico, venta telefónica y otros mecanismos que no requieran contacto personal con clientes y únicamente mediante las modalidades de entrega a domicilio o retiro, las que deberán concretarse en el horario de 6 a 21 horas.

**Locales gastronómicos**

Los locales gastronómicos (comprensivo de bares, restaurantes, heladerías y otros autorizados a funcionar como tales, con concurrencia de comensales), desarrollarán su actividad sin excederse del factor máximo de ocupación de las superficies que permita el cumplimiento estricto de las reglas generales de conducta y ajustándose además a los siguientes horarios:

a) Los días viernes, sábados y vísperas de feriados, entre las 6 horas y las 2 horas del día siguiente.

b) El resto de los días de la semana, entre las 6 horas y las 1 horas del día siguiente.

**DIscotecas y locales bailables**

Se habilita el funcionamiento de discotecas y locales bailables, salones de fiestas para bailes o similares, debiendo ajustarse a las siguientes condiciones:

a) Contar con habilitación de las autoridades locales, las que determinarán los espacios al aire libre autorizados y el aforo permitido para los mismos; lo que deberá estar informado debidamente en el ingreso.

b) Cumplimentando la regla general de no excedencia de ocupación máxima del 70% del espacio habilitado al aire libre.

c) Solo los días viernes, sábados y vísperas de feriado.

d) En el horario de 20 a 3 horas del día siguiente.

e) Permitiendo el ingreso solo de personas que cuenten con al menos una dosis de vacuna, aplicada con una antelación mínima de 14 días corridos.

Los salones de eventos o similares, cuando realicen reuniones que involucren actividad bailable, deberán ajustarse a las previsiones del presente Artículo.

**Horarios de actividades**

a) Actividad deportiva en modalidad entrenamiento y recreativa de deportes individuales o grupales; gimnasios, natatorios y establecimientos afines: entre las 7 y las 22 horas.

b) Actividades religiosas en iglesias, templos y lugares de culto, correspondientes a la Iglesia Católica Apostólica Romana y entidades religiosas inscriptas en el Registro Nacional de Cultos: entre las 8 y las 21 horas.

c) Cines y Complejos Cinematográficos: entre las 10 y la 1 horas del día siguiente.

d) Pesca deportiva y recreativa en la modalidad desde costa y embarcados, navegación recreativa o deportiva, y las actividades de guarderías náuticas, a los fines del retiro y depósito de las embarcaciones: entre las 7 y las 20 horas.

e) Funcionamiento de locales de juegos infantiles y otros establecimientos afines, en espacios cerrados o al aire libre, comúnmente denominados miniclubs o peloteros: entre las 8 y las 24 horas.

f) Actividad en hipódromos y agencias hípicas, organizando turnos para desarrollar las tareas de cuidado y entrenamiento de los animales y de mantenimiento de las instalaciones: entre las 7 y las 20 horas.

g) Actividad de los casinos y bingos, cumplimentando las reglas generales de conducta y de prevención y los protocolos oportunamente aprobados para la actividad: entre las 10 y las 2 horas del día siguiente.

h) Actividad de los salones de eventos, fiestas y similares, para la realización de eventos sociales, en las condiciones establecidas en el Artículo 6° del Decreto N° 1220/21 y sus prórrogas, y las complementarias dispuestas en el presente Decreto: entre las 8 y las 3 horas del día siguiente.

i) Práctica de competencias automovilísticas y motociclísticas: entre las 8 y las 20 horas, sin perjuicio de lo dispuesto en los Artículos 5° inciso k) y 6° último párrafo del presente decreto.

**Adultos mayores**

Además, se prorroga a partir de las 0 horas del 2 de octubre de 2021, en todo el territorio provincial, la autorización de las actividades para personas mayores de 60 años con el esquema de vacunación completo, en los denominados centros de día de rehabilitación y recreativos.

También se prorroga la autorización para las visitas y salidas programadas de personas mayores que residan en establecimientos geriátricos, residencias, o similares; sujetas al cumplimiento de los protocolos específicos establecidos para la actividad y las condiciones precisadas en los artículos 11 y 12 del Decreto Nº 1374/21; sin perjuicio de las específicas que pueda disponer la autoridad sanitaria.

**Agencias**

Se dispone que, desde las 0 horas del 2 de octubre de 2021, la actividad de los permisionarios (agentes y subagentes) de la Caja de Asistencia Social Lotería de Santa Fe para la comercialización de los juegos oficiales, podrá desarrollarse hasta el horario de cierre de captura de apuestas, conforme lo disponga el citado Organismo.

Por último, se recuerda que las autoridades municipales y comunales, en concurrencia con las autoridades provinciales competentes, coordinarán los procedimientos de fiscalización necesarios para garantizar el cumplimiento de las medidas dispuestas en el presente Decreto, de los protocolos vigentes y de las normas dispuestas en virtud de la emergencia sanitaria.

Para finalizar, la ministra Martorano recalcó “la importancia de los cuidados. Para sostener el nivel de habilitaciones es importante que sigamos utilizando el barbijo, la distancia, el lavado de manos y la vacunación. Los buenos resultados en nuestra provincia tienen que ver con el alto nivel de vacunación. Así que invitamos a inscribirse a quienes aún no lo hicieron”, remarcó.