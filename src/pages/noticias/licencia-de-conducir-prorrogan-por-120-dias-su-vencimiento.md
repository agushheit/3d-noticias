---
category: Agenda Ciudadana
date: 2021-03-31T07:25:02-03:00
thumbnail: https://assets.3dnoticias.com.ar/licencia.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad Santa Fe
resumen: 'Licencia de conducir: prorrogan por 120 días su vencimiento'
title: 'Licencia de conducir: prorrogan por 120 días su vencimiento'
entradilla: 'La disposición alcanza a credenciales que caducan entre el 1° de enero
  y el 31 de mayo del corriente año. '

---
La Municipalidad de Santa Fe informa que rige una prórroga por 120 días para el vencimiento de la Licencia Nacional de Conducir cuya caducidad opere entre el 1° de enero y el 31 de mayo de 2021, inclusive.

La medida se inscribe en la Resolución Interna N° 22 de la Secretaría de Control y Convivencia Ciudadana de la capital provincial, con fecha del 30 de marzo, en adhesión a la reciente normativa de la Agencia Provincial de Seguridad Vial, en el marco de la emergencia sanitaria por la pandemia de Covid-19.

La dependencia municipal ya había adoptado medidas similares en diversas ocasiones por el contexto de pandemia, como es el caso de las resoluciones N° 23 del 17 de junio, N° 27 del 31 de julio, N° 35 del 27 de agosto, N° 45 del 17 de septiembre, Nº 50 del 27 de octubre del 2020, y N° 57 del 28 de diciembre de 2020, en adhesión a normativas de la Agencia Provincial de Seguridad Vial.