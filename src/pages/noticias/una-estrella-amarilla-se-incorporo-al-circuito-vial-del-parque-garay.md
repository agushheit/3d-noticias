---
category: La Ciudad
date: 2021-06-11T09:31:40-03:00
thumbnail: https://assets.3dnoticias.com.ar/estrella2.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad de Santa Fe
resumen: Una Estrella Amarilla se incorporó al circuito vial del Parque Garay
title: Una Estrella Amarilla se incorporó al circuito vial del Parque Garay
entradilla: En el marco del Día de la Seguridad Vial, esta tarde la Municipalidad
  -junto a la agrupación de familiares víctimas de accidentes viales-, incorporaron
  una estrella amarilla en la pista de examen del Parque Garay.

---
Desde hoy, las vecinas y vecinos que tramiten la licencia de conducir o, simplemente circulen por el Parque Garay, observarán una estrella amarilla de cinco puntas, que representa: memoria, prevención, ley, justicia y educación. Este símbolo, que por Resolución Nº 22236 del Concejo Municipal fue incorporado como contenido obligatorio de los cursos y exámenes teóricos y prácticos de seguridad vial, fue pintado por el grupo Estrellas Amarillas Santa Fe Capital.

El acto contó con funcionarios municipales e integrantes de la agrupación Estrellas Amarillas Santa Fe, compuesta por familiares de víctimas de siniestros viales.

De esta manera, la señal “Estrella Amarilla” quedó incorporada al circuito de examen práctico del Centro de Emisión de Licencias de Conducir, ubicado en Salvador Caputto 3955. Luego del acto, Virginia Coudannes, secretaria de Control y Convivencia Ciudadana, consignó: “En el marco del Día de la Seguridad Vial realizamos esta acción que es un símbolo de concientización, de educación, de visibilizar la importancia de la vida y de una conducción responsable”.

Por su parte, Karina Ingrasciotta, referente de la Asociación Estrellas Amarillas, indicó: “Este es un día muy especial para nosotros. Recordamos a todas nuestras pérdidas. La estrella es un símbolo para que nadie más tenga que pasar por esto. Es transformar el dolor en amor”.

En consonancia, Karina se refirió a la necesidad de  “priorizar el valor de la vida. Es necesario que todos sepan lo que significan estas estrellas, por eso es importante que se haya puesto en este lugar. Una estrella en el asfalto es una persona que perdió la vida y hoy podría estar entre nosotros. Agradezco el apoyo de la Municipalidad para difundir y acompañar esta iniciativa”.

![](https://assets.3dnoticias.com.ar/estrella1.jpg)

**Símbolos**

La Campaña Estrellas Amarillas es un proyecto de concientización vial para los jóvenes, que buscaba apoyar leyes de prevención de accidentes y de apoyo a familias de víctimas de siniestros viales. Con diferentes formas, tamaños, nombres y fechas, las Estrellas Amarillas ya son parte del paisaje argentino, señalando los lugares peligrosos de un camino, donde se produjeron siniestros con víctimas fatales.

Hasta ahora, su instalación y difusión corrió por cuenta de los familiares de víctimas de accidentes de tránsito, pero en el año 2020 la Agencia Nacional de Seguridad Vial (ANSV) reconoció estas estrellas de cinco puntas (cada una representa: memoria, prevención, ley, justicia y educación) como señales oficiales. Esta iniciativa busca que quienes realicen el curso adquieran el conocimiento acerca del símbolo, su significado y alcance.

![](https://assets.3dnoticias.com.ar/estrella.jpg)