---
category: El Campo
date: 2021-10-23T06:00:21-03:00
thumbnail: https://assets.3dnoticias.com.ar/indice.png
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Came (confederacion argentina de la mediana empresa)
resumen: 'Segun la CAME, la brecha de precios en productos agropecuarios: bajó 5%
  en septiembre, pero sigue siendo elevada'
title: 'Segun la CAME, la brecha de precios en productos agropecuarios: bajó 5% en
  septiembre, pero sigue siendo elevada'
entradilla: La brecha de precios para el promedio de los 24 principales productos
  agropecuarios que participan de la mesa familiar se redujo 5% en septiembre, impulsada
  por la mejora en los precios al productor.

---
La brecha de precios para el promedio de los 24 principales productos agropecuarios que participan de la mesa familiar se redujo 5% en septiembre, impulsada por la mejora en los precios al productor. El consumidor pagó, en promedio, 5,9 veces más de lo que cobró el productor por esos alimentos. La participación del productor en los precios de venta subió a 26,4%.

\- La diferencia de precios entre lo que pagó el consumidor y lo que recibió el productor en el campo por sus productos agropecuarios bajó 5% en septiembre. La mejora ocurrió tras cinco meses continuos de aumento en ese indicador.

\- Para los 24 agro alimentos relevados, los consumidores pagaron en promedio 5,91 veces más de lo que cobró el productor (5,87 en julio y 6,22 en agosto).

\- La caída mensual del IPOD, considerando las estacionalidades de los productos, fue impulsada por un aumento de 13,5% en los precios al productor, mientras que los precios al consumidor subieron 5,6% en los comercios pymes de cercanía y 3,6% en los hipermercados. Estos porcentajes representan, en promedio, las variaciones de precios mensuales de los 24 productos de la canasta en los diferentes estadios (precio en origen, precio en góndola en comercios de cercanía y en hipermercados).

\- Considerando los 24 productos relevados, en 20 de ellos se podían encontrar mejores precios en los comercios minoristas pymes que en lo hipermercados. El brócoli mantuvo una diferencia importante, ya que en verdulerías y almacenes el precio del kilogramo fue 40,3% menor a los hipermercados (casi la misma diferencia que en agosto). Le siguió la leche, con un precio promedio 24,7% menor en los comercios pymes que en los hiper.

\- Los productos con mayor caída mensual en sus brechas fueron: frutilla, con una baja de 30,5%, zanahoria (-26,9%), zapallito (-26%) y brócoli (-24,6%). Los de mayores subas fueron tomate redondo (+50,8%) y repollo (+13,8%).

\- Los datos surgen del Índice de Precios en Origen y Destino (IPOD) que elabora el sector de Economías Regionales de CAME en base a los precios de origen de las principales zonas de producción y a más de 700 precios de cada producto en destino, relevados no solo en verdulerías y mercados por un equipo de 30 encuestadores, sino también mediante un monitoreo de los precios online de los principales hipermercados del país, durante la segunda quincena del mes.  

**Resultados de septiembre**

IPOD frutihortícola: bajó 4,5% en el mes y la brecha promedio se ubicó en 6,7 veces.

IPOD ganadero: bajó 7,9%, a 3 veces. El IPOD ganadero resultó 55,4% más bajo que el IPOD frutihortícola, ampliando nuevamente su brecha frente al mes anterior.

Participación del productor en el precio final: subió 1,2 puntos porcentuales en septiembre, de 25,2% a 26,4%.

**Mayores y menores brechas**

\- En septiembre, las mayores brechas entre precios de origen y destino se dieron en: limón con una diferencia de 15,4 veces, cebolla (13,2 veces), tomate redondo (10,8 veces) y zanahoria (10,7 veces).

\- Los productos con menores brechas, en tanto, fueron: frutilla (1,2 veces), pollo (1,6 veces), huevos (2,2 veces) y acelga (2,6).