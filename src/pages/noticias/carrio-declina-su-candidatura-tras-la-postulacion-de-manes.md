---
category: Agenda Ciudadana
date: 2021-07-04T06:00:33-03:00
thumbnail: https://assets.3dnoticias.com.ar/FMANES.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Carrió declina su candidatura tras la postulación de Manes
title: Carrió declina su candidatura tras la postulación de Manes
entradilla: El neurocientífico anunció en sus redes sociales su participación en las
  PASO del 12 de setiembre.

---
El neurocientífico Facundo Manes anunció este sábado que aceptó el ofrecimiento de la Unión Cívica Radical (UCR) para presentarse como precandidato a diputado por la provincia de Buenos Aires, con vistas a las PASO del 12 de septiembre, postulación con la cual el radicalismo busca revitalizarse, presionar por más protagonismo hacia adentro de Juntos por el Cambio y reclamar espacios en las listas de cara a las elecciones legislativas del 14 de noviembre.

"Es tiempo de estar presentes. Por eso, decidí aceptar la invitación que me hizo la Unión Cívica Radical y participar en las próximas elecciones. Esta es la lucha de nuestras vidas. Y vamos a estar hasta el final", expresó Manes en la mañana del sábado a través de un video de casi dos minutos publicados en sus redes sociales.

Tras conocerse la decisión del neurólogo de postularse electoralmente, inmediatamente se multiplicaron los saludos y congratulaciones de parte de los referentes del radicalismo, quienes buscan mayor protagonismo hacia adentro de la coalición opositora, que integran junto al PRO y a la Coalición Cívica.

En el video difundido, Manes explicó: "Decidí aceptar la invitación que me hizo la UCR y participar en las próximas elecciones", y agregó: "Se viene un nuevo país, en un nuevo mundo y hay que pensarlo, armarlo, explicarlo y hacerlo sobre todo".

En esa línea, agregó que "hay dos cosas que nos movilizan a los seres humanos: el miedo y la esperanza. Queda en nosotros elegir el camino que queremos tomar. Sepan que a pesar del dolor y el sufrimiento que estamos viviendo, podemos salir fortalecidos de todo esto".

Sostuvo además que "tenemos que convertir la resignación y el desencanto en esperanza. No podemos permitir que el miedo nos paralice. Es tiempo de estar presentes" y apuntó: "En este contexto tan crítico, la pregunta no es quién se involucra sino cómo hacemos todos para involucrarnos. El futuro no está escrito, depende de nosotros".

"Tenemos por delante un desafío que nunca imaginamos. No podemos enfrentar una crisis de esta magnitud con las mismas prácticas de siempre. Somos más que esto. En Argentina tenemos un dolor común. Necesitamos un sueño común. Esta es la lucha de nuestras vidas y vamos a estar hasta el final", concluye el mensaje con el que formalizó su pase a la arena política.

**Palabras de Elisa Carrió**

Por su parte, la exdiputada nacional y fundadora de la Coalición Cívica ARI, Elisa Carrió, confirmó que no se presentará como candidata en el distrito bonaerense por Juntos por el Cambio, luego de conocerse la postulación de Manes.

"Habiéndose confirmado la candidatura de Facundo Manes, mi participación como candidata en la provincia de Buenos Aires carece de sentido histórico y mi sacrificio resultaría inútil. Sólo lo hacía por la Unidad de los argentinos y Juntos por el Cambio", señal Carrió en un mensaje publicado en su cuenta oficial de la red social Twitter.

Con la precandidatura de Manes, la UCR está dispuesta a salir a disputar de igual a igual con el PRO los lugares en las listas de cara a las próximas PASO y legislativas.

No obstante, no es el único nombre para la provincia de Buenos Aires dentro del radicalismo: en los últimos días, también el intendente de San Isidro, Gustavo Posse, manifestó su intención de competir en esos comicios con lista propia.

En tanto, en el PRO, hay una disputa entre el vicejefe de Gobierno porteño, Diego Santilli; el intendente de Vicente López, Jorge Macri, y otros nombres que buscan representar al territorio bonaerense en los comicios legislativos.

**Voces de apoyo del radicalismo**

"Con Facundo queremos contribuir a que la oposición tenga una oferta mucho más amplia, que pueda tener más aceptación en el electorado bonaerense, y también se busca ampliar la base en todo el país", explicó hoy el titular nacional del radicalismo, el diputado Alfredo Cornejo.

En diálogo con radio Mitre, el exgobernador de Mendoza resaltó que "una figura como él, que no tiene pasado en el gobierno anterior y que comulga con principios básicos del radicalismo, al kirchnerismo le incomoda, los descoloca en su mensaje y su estética".

De todos modos, opinó que "no sería tan categórico en decir que él solo puede ganarle al kirchnerismo, pero sin dudas se hará mucho más competitiva la elección con Facundo que con otras figuras", y aclaró que "la elección legislativa en la provincia de Buenos Aires es difícil y muy complicada".

A su turno, el diputado radical Mario Negri, titular del interbloque de Juntos por el Cambio en la Cámara baja, habló de la necesidad de "revitalizar al radicalismo, sumar gente joven" y, en ese sentido, agregó: "Creo que Manes es un soplo que acompaña al partido, suma a la coalición, da equilibrio".

Negri apuntó además que "cuando en una coalición se habla de un solo partido ya no es más una coalición", en obvia referencia al PRO y su interna, que por estos días no logra llegar a acuerdos para integrar listas electorales.

También el senador nacional de la UCR Martín Lousteau se pronunció sobre la precandidatura de Manes, a través de las redes sociales.

"¡Bienvenido Facundo! El desafío de transformar la Argentina requiere un mejor radicalismo para hacer un mejor Juntos por el Cambio. Ahí estaremos", escribió el porteño Lousteau en cuenta de Twitter, para postear luego la declaración del neurocientífico en la que anuncia su postulación.

Del mismo modo, la UCR nacional, en su cuenta de Twitter, expresó: "¡Bienvenido @ManesF! Que decidas involucrarte es una gran noticia. Tus ideas y tu vocación por transformar la realidad son un gran aporte para la provincia de Buenos Aires y para toda la Argentina. ¡Vamos juntos a construir el futuro que soñamos!".

Hace menos de una semana, Manes estuvo en Jujuy junto a un grupo de dirigentes radicales para acompañar al gobernador Gerardo Morales en la jornada electoral en esa provincia.

Allí Manes había adelantado: "Necesitamos líderes como él (por Morales) que nos unan, no que nos dividan; necesitamos líderes que nos lideren con esperanza, no con miedo, porque el miedo nos paraliza, el miedo nos pone a la defensiva".