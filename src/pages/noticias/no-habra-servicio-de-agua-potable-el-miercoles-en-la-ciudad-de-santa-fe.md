---
category: La Ciudad
date: 2021-11-03T06:15:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/assa.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: No habrá servicio de agua potable el miércoles en la ciudad de Santa Fe
title: No habrá servicio de agua potable el miércoles en la ciudad de Santa Fe
entradilla: Por mantenimiento preventivo en las instalaciones eléctricas de la planta
  potabilizadora de Assa, el próximo miércoles se interrumpirá el servicio entre la
  medianoche y las 4. Luego podría haber "turbiedad".

---
Luego de los reiterados inconvenientes en el servicio de agua potable en las últimas semanas, Aguas santafesinas (Assa) comunicó a los vecinos de la ciudad de Santa Fe que no contarán con provisión de agua durante cuatro horas del próximo miércoles. Desde la empresa aseguran que se tratará de un mantenimiento preventivo que afectará a todo el radio servido desde la planta potabilizadora, advirtiendo que luego que se rehabilite el servicio podría registrarse turbiedad

**Segundo corte en 10 días**

La última semana el servicio estuvo interrumpido no solo en barrio Candioti sino en gran parte de la ciudad, producto de un corte eléctrico en la planta potabilizadora que provocó que solo esté funcionando con generación propia. Esto se dio en el marco del primer día de calor intenso de la temporada, por lo que se generaron cortes de luz en distintos puntos de Santa Fe.

Esto provocó no solo baja presión de agua en toda la ciudad sino ausencia total del servicio, a causa de que la planta permanecía trabajando por debajo de su capacidad por el desperfecto eléctrico. Con el correr de las horas la EPE normalizó el suministro eléctrico y Assa pudo retomar el ritmo de bombeo normal progresivamente.

**Agua turbia**

Previamente, fueron varios los casos de agua turbia que reflejaron los vecinos de barrio Candioti, lo que tuvo como denominador común los efectos de la bajante histórica del Paraná en la costa santafesina. Desde Assa comunicaron que el agua seguía siendo apta para consumo, aunque cambie su composición.

En este caso, intervino el Ente Regulador de Servicios Sanitarios (Enress), para diagnosticar el procedimiento que obró de parte de Assa ante la garantía de normalidad del servicio con un estándar de calidad del agua por debajo del normal, bajo el cual se emitió una resolución que exponía las severas infracciones expuestas.