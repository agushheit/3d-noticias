---
category: Agenda Ciudadana
date: 2021-04-13T09:18:09-03:00
thumbnail: https://assets.3dnoticias.com.ar/micros-turismo-protesta-Terminal-01.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Diario Uno
resumen: Transportistas continuarán el bloqueo en terminal e intensificarán los cortes
  en la ciudad
title: Transportistas continuarán el bloqueo en terminal e intensificarán los cortes
  en la ciudad
entradilla: Así lo decidieron transportistas de turismo independientes tras no conseguir
  respuestas a sus reclamos. Bloquearán durante la noche la Terminal de Ómnibus y
  este martes endurecerán los cortes.

---
Luego de un lunes de protestas con el bloqueo de la Terminal de Ómnibus de la ciudad Manuel Belgrano y diversos cortes de la calle, transportistas de turismo independiente continuarán con las medidas durante la noche y la madrugada para incrementar la protesta este martes con más cortes de tránsito

Durante todo el lunes, diversos empresarios de transporte del sector turístico de la región realizaron una fuerte protesta en Santa Fe y Rosario, como también en distintos puntos del país. El motivo de la medida se lleva adelante en rechazo a las restricciones dispuestas por el gobierno nacional para el turismo grupal con el objetivo de frenar el avance de la ola de coronavirus.

En la capital provincial, las protestas comenzaron a las 6.30 de este lunes con el bloqueo de la Terminal de Ómnibus Manuel Belgrano. Con el correr de las horas, más de 25 colectivos de viajes más algunos minibuses extendieron las medidas con cortes de tránsito en la zona, como por ejemplo en avenida Alem y Belgrano, Alem y Marcial Candioti y también en la intersección de calle La Rioja, negando el accesos al microcentro.

Durante todo el lunes se extendieron las medidas de protesta y desde el sector de manifestantes decidieron continuar durante toda la noche y la madrugada el bloqueo de la Terminal de Ómnibus.

“Tenemos el mismo protocolo que las empresas de línea, emitido por el Ministerio de Transporte de la Nación, pero no sabemos por qué a nosotros no nos dejan viajar y a ellos sí. Entendemos que estamos en pandemia y que hay contagios. Acá trabajamos todos, o no trabaja nadie", indicaron los referentes de la protesta.

**Lo que piden los transportistas**

Desde el sector de Transporte de Pasajeros de Turismo Autoconvocados, nos reunimos con el objetivo de reclamar y proteger nuestro derecho de poder ejercer libremente nuestra actividad comercial con normalidad. Siempre hemos cumplido y colaborado con las medidas que impuso el gobierno nacional. En estos momentos no podemos seguir sin realizar nuestras tareas ya que no tenemos recursos económicos suficientes para poder afrontar nuevamente la situación que sufrimos en el año 2020 de tener 9 meses sin trabajar ni recibir ninguna ayuda económica ni del gobierno nacional ni provincial. Estos meses desde que autorizaron desarrollar nuestra actividad, muchas empresas volvieron a recurrir a préstamos, créditos para poder reactivar la fuente laboral.

Las empresas de turismo cumplieron en su totalidad con los protocolos establecidos como de transportar de 80% de los pasajeros asumiendo las pérdidas que esto implica.

Por lo expuesto anteriormente solicitamos de forma urgente.

1\. Solicitamos el libre desempeño de nuestra fuentes de trabajo, con la igualdad que poseen las empresa de líneas y transportes aéreos.

2\. Solicitamos la eximición del Decreto 235/2021,

3\. Declarar la emergencia económica para el sector.

4\. Habilitar terminales con testeos rápidos de los pasajeros como para nuestros choferes.

5\. Cancelación del pago de ingresos brutos y ganancias por el periodo que dure la pandemia.

6\. Implementar un subsidio por los meses de interrupción de nuestra actividad o lucro cesante de nuestras unidades.

7\. El pago de sueldos de nuestros empleados y cargas sociales.

8\. Ampliación del vencimiento de habilitación nacional de las unidades con antigüedad de 15 años, incluyendo vehículos modelos año 2007.

9\. Solicitamos gasoil subsidiado para unidades con habilitación nacional.

10\. Solicitamos con urgencia una reunión con autoridades provinciales o nacionales para dejar asentado en acta el libre desempeño de nuestra actividad siempre cumpliendo con los protocolos establecidos.