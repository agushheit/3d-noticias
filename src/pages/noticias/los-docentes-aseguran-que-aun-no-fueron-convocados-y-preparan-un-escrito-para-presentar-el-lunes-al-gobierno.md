---
category: Agenda Ciudadana
date: 2021-10-23T06:00:33-03:00
thumbnail: https://assets.3dnoticias.com.ar/amsafe.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Los docentes aseguran que aún no fueron convocados y preparan un escrito
  para presentar el lunes al gobierno
title: Los docentes aseguran que aún no fueron convocados y preparan un escrito para
  presentar el lunes al gobierno
entradilla: Desde Amsafé aseguraron que hasta la tarde de este viernes no recibieron
  la convocatoria formal del gobierno para continuar la discusión paritaria.

---
A pesar de los dichos del Ministro de Trabajo de la provincia, Juan Manuel Pusineri, sobre la citación a los maestros para el miércoles que viene, los docentes públicos nucleados en Amsafé aún esperan la convocatoria formal del gobierno para continuar la paritaria.

Según confirmaron fuentes gremiales a UNO Santa Fe hasta las 19 de este viernes no había llegado ninguna cédula a la sede de Amsafé con la convocatoria para continuar con la discusión salarial la semana que viene.

Hasta el momento, los docentes aguardan la comunicación del Ministerio de Trabajo. Sin embargo, advirtieron que si se llega al lunes y eso no ocurre se presentará un escrito para exigir la convocatoria.

En tanto, desde el gobierno de la provincia se envió un breve comunicado a los medios para confirmar la convocatoria para el miércoles 27 de octubre, a las 10. "Asimismo se hace saber que teniendo en cuenta la solicitudes efectuadas, el Gobierno de la Provincia procederá a liquidar el aumento salarial a jubilados y pensionados del sector docente", finaliza el texto.

Los docentes decidieron rechazar el 6 de octubre la oferta de un aumento del 17 por ciento en tres tramos que le hizo el gobierno e inició un plan de lucha que se concretó con un paro de 24 y otro de 48 horas que se concretaron los días 13, 20 y 21 de octubre.