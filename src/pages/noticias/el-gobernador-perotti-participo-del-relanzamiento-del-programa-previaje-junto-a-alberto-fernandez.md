---
category: Estado Real
date: 2021-08-15T06:00:50-03:00
thumbnail: https://assets.3dnoticias.com.ar/PREVIAJE.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: El Gobernador Perotti participó del relanzamiento del programa previaje junto
  a Alberto Fernández
title: El Gobernador Perotti participó del relanzamiento del programa previaje junto
  a Alberto Fernández
entradilla: "“Nuestra provincia se ubicó dentro de los diez principales destinos de
  la Argentina y ahora aspiramos a mantenernos allí”, sostuvo el mandatario santafesino.."

---
El gobernador de la provincia, Omar Perotti, enfatizó hoy que Santa Fe “tomó la decisión de acompañar al turismo”, actividad que según el mandatario “debe convertirse en una de las principales actividades”.

“Desde el primer día empezamos a trabajar en ello. La pandemia nos dio también una oportunidad de que más argentinos y más santafesinos conozcan profundamente nuestra provincia y esto es lo que nos entusiasma en esta nueva edición” del Programa Previaje, dijo Perotti.

En ese sentido, recordó que “Santa Fe se colocó dentro de los diez principales destinos de la Argentina y ahora aspiramos a mantenernos allí, para seguir consolidando la recepción de más turistas propios que, buscando la cercanía, la tranquilidad y el medioambiente fueron descubriendo nuestra provincia y van a estar volviendo en los próximos meses de verano en los que podamos ir retomando, poco a poco, las posibilidades de hacer turismo en familia y con los amigos”, afirmó.

El gobernador participó este mediodía, de manera remota, del acto encabezado por el presidente de la Nación, Alberto Fernández, por el cual se anunció la puesta en marcha de la segunda edición del Programa Previaje 2021-22. Fernández estuvo acompañado por el ministro de Turismo y Deportes, Matías Lammens; y el gobernador de Misiones, Oscar Herrera Ahuad.

El escenario elegido fue el imponente Parque Nacional Iguazú, que tiene a las Cataratas como una de las máximas maravillas naturales del mundo.

El programa nacional servirá para la compra anticipada de servicios turísticos nacionales mediante la devolución de créditos por la mitad del gasto. El objetivo es fomentar el consumo y la demanda internos y dinamizar la cadena turística en todo el país, un sector que sufrió un impacto altamente negativo durtante la pandemia.

**Inversiones en santa fe**

“El hotelería y la gastronomía fueron sectores muy golpeados y necesitan tener ayuda. Nuestro deseo es seguir acompañando a una de las actividades que va a ser una de las principales en la provincia de Santa Fe. Las inversiones que estamos generando hacia el sector así lo indican: en rutas, en infraestructura sanitaria, en comunicaciones, en capacitación”, recordó el gobernador Omar Perotti.

Y agregó: “Todo tiene que ver con las posibilidades de mejorar la oferta, la atención y la forma en la que lo podemos hacer; el financiamiento para el sector también porque, hay que mejorar equipamiento y las condiciones en las cuales promocionemos a nuestra provincia en todo el país”.

El mandatario santafesino señaló también que “estamos convencidos de que mucha más gente va a utilizar el Programa, por los beneficios que dio en el primer año y seguramente la provincia de Santa Fe podrá sumar algún elemento adicional para entusiasmar a que más turistas que nos visiten. También, para que todos los santafesinos conozcamos profundamente cada rincón de nuestra provincia, que tiene muchísimas cosas hermosas para mostrarnos”, concluyó Perotti.

**Características y beneficios**

Durante el acto de relanzamiento del Programa Previaje, el presidente Alberto Fernández sostuvo que la gastronomía y el hotelería fueron "las dos actividades más dañadas, las grandes víctimas por la pandemia" de coronavirus; y destacó la vuelta de un plan que fue "muy exitoso en el verano pasado”.

Asimismo, Ferández remarcó que la vuelta del turismo será posible “gracias al avance de la campaña de vacunación” del Gobierno Nacional, y expresó: "Trajimos vacunas de excelencia y seguimos avanzando”.

El programa permite la compra anticipada de servicios turísticos nacionales mediante la devolución de créditos por el 50 por ciento del gasto. Los mismo podrán utilizarse desde noviembre de este año hasta diciembre de 2022, e incluirán la devolución por la mitad de los gastos realizados en agencias de viaje, alojamientos, transporte y otros servicios turísticos como centros de ski, bodegas y espectáculos artísticos, además del alquiler de vehículos.

Por su parte, el ministro de Turismo y Deportes de la Nación, Matías Lammens, recordó que, en su primera edición, la iniciativa "inyectó 15.000 millones de pesos en el sector y benefició a 600.000 turistas, incluyendo la prestación de más de 13.000 centros turísticos y 100.000 comercios".