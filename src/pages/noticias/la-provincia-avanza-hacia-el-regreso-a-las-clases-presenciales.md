---
layout: Noticia con imagen
author: Nota de la Redacción de 3DNoticias
resumen: Clases presenciales
category: Agenda Ciudadana
title: La provincia avanza hacia el regreso a las clases presenciales
entradilla: El Ministerio de Educación planificará la vuelta a la
  presencialidad. En las escuelas habilitadas, los alumnos recibirán los
  cuadernillos de apoyo escolar durante la primera quincena de diciembre.
date: 2020-11-22T13:59:58.805Z
thumbnail: https://assets.3dnoticias.com.ar/clases-presenciales.jpg
---
El gobierno de la provincia instruyó al Ministerio de Educación a través del decreto 1446 a la **planificación de encuentros que propicien la vuelta a la presencialidad**, mediante la organización de grupos de estudiantes, a fin de que concurran a la escuela la primera quincena del mes de diciembre.

En la oportunidad recibirán los cuadernos de trabajo que forman parte del acompañamiento de trayectorias escolares, compartiendo consignas de seguimiento y al mismo tiempo, fortaleciendo la convocatoria a la continuidad prevista en el ciclo 2021.

Cabe señalar que las acciones que se habilitan, serán llevadas adelante en esta primera etapa en las escuelas habilitadas (la mayoría de ellas rurales o en zonas con baja incidencia del Covid) con la conformación de grupos pequeños, atenidos a un estricto cumplimiento de los protocolos aprobados para el sector y las recomendaciones establecidas por las autoridades sanitarias.

Por último, estas decisiones se enmarcan en los acuerdos federales para el regreso progresivo al dispositivo presencial en las escuelas habiendo sido aprobado el plan oportunamente presentado por la provincia de Santa Fe y debidamente comunicado a las instituciones educativas de la jurisdicción. Para ello, se han habilitado las refacciones edilicias y la provisión de insumos para estas experiencias protocolizadas.