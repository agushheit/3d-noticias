---
category: La Ciudad
date: 2021-03-12T06:08:20-03:00
thumbnail: https://assets.3dnoticias.com.ar/procrear.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de El Litoral
resumen: Procrear II licitó obras para desarrollos urbanísticos en la ciudad de Santa
  Fe
title: Procrear II licitó obras para desarrollos urbanísticos en la ciudad de Santa
  Fe
entradilla: Se desarrollarán en la ciudad de Santa Fe y también en El Palomar (Buenos
  Aires) y Monte Maíz (Córdoba). 

---
El día miércoles 10 de marzo se realizó un nuevo llamado licitatorio para impulsar la construcción de viviendas en Monte Maíz, provincia de Córdoba; Ciudad de Santa Fe y El Palomar, Morón, en la provincia de Buenos Aires.

Las licitaciones contemplan la construcción de viviendas nuevas para que cada vez más familias  puedan tener su casa.

El desarrollo urbanístico de la ciudad de Santa Fe planifica dar continuidad al predio existente de Procrear con la edificación de 198 nuevos hogares; mientras que el Monte Maíz, provincia de Córdoba, prevé la construcción de 41 viviendas nuevas. Por último en El Palomar, municipio de Morón, provincia de Buenos Aires, se llevará adelante la construcción de 529 nuevas viviendas. Las tres obras en su conjunto supondrán una inversión aproximada de $ 4.100.000.000. 

Quienes accedan a una vivienda a través de la línea Desarrollos Urbanísticos de Procrear II podrán hacerlo a través de la nueva fórmula Hog.Ar, una nueva modalidad de actualización crediticia que se basa en el Coeficiente de Variación Salarial publicado por el Instituto Nacional de Estadística y Censos (INDEC) y tiene en cuenta la evolución de los salarios.