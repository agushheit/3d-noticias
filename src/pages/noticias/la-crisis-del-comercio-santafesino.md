---
category: La Ciudad
date: 2020-11-28T13:34:41Z
thumbnail: https://assets.3dnoticias.com.ar/comercio.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuentes: LT10  / LITUS'
resumen: Crisis del comercio santafesino
title: La crisis del comercio santafesino
entradilla: Cíclope aborda la delicada situación del sector. Desde abril, la cantidad
  de lo-cales ocupados cayó un 25% en las principales arterias de la ciudad. En tanto,
  desde octubre, la baja alcanza casi el 50%.

---
En esta nueva entrega de CÍCLOPE, se analiza la situación de crisis que afecta, entre otros tantos sectores, al comercio. La pandemia, sumada a un arrastre en la caída de la actividad económica que viene afectando al país hace más de dos años, afectaron fuertemente al sector de ventas minoristas en nuestra ciudad, y en el país entero.

Según un informe elaborado por el Centro Comercial de Santa Fe, en octubre se registraron un 25% más de locales de desocupados que en abril de este año, y casi un 50% más que en octubre de 2019, totalizando 1371 inmuebles vacíos en las principales avenidas comerciales de la ciudad. 

Las ventas online y la mudanza a espacios más pequeños o alejados de las principales arterias, fueron algunas de las alternativas que siguieron los comerciantes que pudieron al menos continuar con la actividad.

Pese a que hay esperanzas de contar con la vacuna contra el coronavirus en los primeros meses de 2021, por el momento no hay certezas del desarrollo de la economía, y el desarrollo de la actividad sigue siendo incierto en este contexto.

La ayuda estatal, las proyecciones y las alternativas para sortear esta crisis son parte del desarrollo de este nuevo informe.