---
category: Agenda Ciudadana
date: 2021-01-21T08:47:56Z
thumbnail: https://assets.3dnoticias.com.ar/congelamiento_alquileres.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Diario Uno
resumen: 'Congelamiento de los alquileres: ¿se extiende dos meses más?'
title: 'Congelamiento de los alquileres: ¿se extiende dos meses más?'
entradilla: El DNU creado para disminuir el impacto de la suba de precios de alquileres
  durante la pandemia, vence el 31 de enero. Reclaman extenderlo hasta marzo.

---
Durante la pandemia, la enorme mayoría de las familias argentinas tuvieron recortes salariales. Casi la mitad de los hogares vieron reducidos sus ingresos, mientras que el 40% sufrió algún tipo de problema laboral, como despedidos o suspensiones, de acuerdo a un estudio del Indec. Previendo este panorama desolador que sufrirían las familias, el Gobierno Nacional decretó a fines de marzo de 2020 el congelamiento de los alquileres y la suspensión de los desalojos.

El DNU regía hasta el 30 de septiembre pasado, pero se prorrogó luego hasta el 31 de enero de 2021, y a poco menos de dos semanas de que llegue a su fin las autoridades nacionales evalúan extenderlo. "Está en análisis y se está trabajando en eso. Se prorrogaría 2 meses más", indicaron voceros del Ministerio de Hábitat de la Nación.

El presidente de la Federación de Inquilinos Nacional, Gervasio Muñoz, manifestó la necesidad de que se extienda más allá del 31 de enero próximo el decreto que congela los alquileres al valor de marzo 2020, a la vez que posibilita el no pago de la renta sin desalojo para aquel inquilino que perdió su empleo o vio reducido drásticamente su ingreso.

A finales del 2020, durante una reunión con el ministro de Desarrollo Territorial y Hábitat, Jorge Ferraresi, se acercaron seis propuestas que pretenden solucionar el problema de acceso a la vivienda en alquiler.

En primer lugar, la extensión al menos hasta el 31 de marzo próximo del decreto 766/2020, que ordena el congelamiento de alquileres al valor de marzo 2020 y la suspensión de desalojos. Asimismo, solicitaron la reglamentación de los contratos de alquiler ante AFIP; promover una campaña de difusión de la nueva ley de alquileres y el decreto; la creación de un organismo de control; la prohibición de oferta en dólares de los inmuebles; y la generación de subsidios para la cancelación de pagos de alquileres atrasados.

"En principio, la extensión del decreto y el cumplimiento de la ley de alquileres son los puntos más urgentes, pero también es fundamental empezar a trabajar en medidas de fondo. Confiamos en que aquí comienza una nueva etapa para los inquilinos", sostuvo Muñoz.

El referente de los inquilinos subrayó que "a más de 6 meses no se han reglamentado varios artículos de la ley de alquileres, no hay control de su cumplimiento ni una campaña de difusión de derechos. La respuesta será una convocatoria a Plaza de Mayo".

En el país, unos 9 millones de argentinos alquilan, de los cuales 1,1 millones son porteños. Según un informe de la Federación de Inquilinos, hasta diciembre último, el 58,5% de los inquilinos acumula deudas de dos meses o más, lo que significa que 786.240 hogares están en condiciones de ser desalojados cuando finalice el decreto que prohíbe los desalojos.