---
category: Agenda Ciudadana
date: 2021-06-01T07:37:11-03:00
thumbnail: https://assets.3dnoticias.com.ar/elecciones.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Diario Uno
resumen: Las siete alianzas que competirán en los comicios provinciales
title: Las siete alianzas que competirán en los comicios provinciales
entradilla: Entre las novedades, un partido que competía por Juntos por el Cambio
  ahora integra el Frente de Todos. También hay siete alianzas locales

---
En las últimas horas del domingo se presentaron siete alianzas provinciales y siete comunales para competir en los comicios locales de este año. La mayor sorpresa se produjo en la incorporación del Partido Fe al Frente de Todos luego de estar en Juntos por el Cambio en las últimas dos elecciones.

Laguna Paiva, Capitán Bermúdez, Pérez, Rosario, Venado Tuerto, Florencia y San Guillermo son las localidades donde se inscribieron frentes comunales.

Las alianzas provinciales son el Frente de Todos (FdT), Frente Progresista (FPCyS), Juntos por el Cambio (JxC), Más Santa Fe, Primero Santa Fe, Igualdad y Soberanía y Mejor. El FDT está conformado por el PJ, Partido Comunista, Partido Solidario, Partido FE, Partido del Progreso Social, Confluencia Santafesina, Producción, Trabajo y Desarrollo y Frente Renovador Santa Fe.

    https://www.scribd.com/document/510066731/Alianzas-inscriptas-para-los-comicios-2021

Mientras que el Frente Progresista, Cívico y Social está integrado por la Unión Cívica Radical, el Partido Demócrata Progresista, Movimiento Libres del Sur, Partido Socialista, GEN, SI, CREO y PARES. Por su parte, Juntos por el Cambio está conformado por la Unión Cívica Radical, Coalición Cívica - ARI, PRO, Unión del Centro Democrático, Unidos.

Otra de las alianzas que que tiene representación en algunos concejos municipales y en la Legislatura santafesina es la que tiene entre sus referentes a los diputados provinciales Rubén Giustiniani y Carlos del Frade. Bajo el nombre de Igualdad y Soberanía ahora buscará crecer en las distintas localidades de la provincia. Esa alianza está integrada por los partidos Del Trabajo y del Pueblo, la Liga de los Pueblos Libres, Igualdad y Participación y el Movimiento del Trabajo y el Pueblo Rosarino.

Respecto de uno de los cambios más importantes en la conformación de las alianzas, la diputada provincial Cesira Arcando, referente del Partido FE en la provincia, salió a explicar por redes sociales el por qué su partido pasó de integrar Juntos por el Cambio, en 2019, al Frente de Todos para los comicios de este año. Arcando aseguró que su partido tiene un "origen gremial y peronista" por lo que no debe sorprender su participación en la coalición que ahora gobierna tanto a nivel nacional como en Santa Fe.