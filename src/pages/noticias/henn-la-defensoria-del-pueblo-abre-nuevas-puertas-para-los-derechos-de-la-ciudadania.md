---
category: La Ciudad
date: 2021-12-20T06:00:34-03:00
thumbnail: https://assets.3dnoticias.com.ar/defensoria.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'Henn: "La Defensoría del Pueblo abre nuevas puertas para los derechos de
  la ciudadanía"'
title: 'Henn: "La Defensoría del Pueblo abre nuevas puertas para los derechos de la
  ciudadanía"'
entradilla: 'El Defensor del Pueblo de Santa Fe Zona Norte se mostró satisfecho con
  las nuevas dependencias que mejorarán la atención pública. '

---
La Defensoría del Pueblo de Santa Fe inauguró el jueves pasado las instalaciones del nuevo anexo de la sede Santa Fe, ubicada en San Jerónimo 2721, a la vuelta de su sede central de Eva Perón 2726, en el microcentro de la ciudad. Las mismas estarán destinadas a dar una mejor atención a los ciudadanos que diariamente concurren a dicha dependencia, y comenzarán a funcionar desde el mes de enero.

  Los nuevos espacios proponen una atención más personalizada, para una escucha atenta y un estudio de cada situación que permita el trabajo mancomunado. Tanto la Dirección de Atención Ciudadana como la Dirección de Salud, discapacidad y Medio Ambiente que funcionarán en poco tiempo en el local, reciben a través de distintos canales de atención (mail, teléfono o presencial) más de 100 consultas por semana que son canalizadas por personal especializado en distintas temáticas.

 Eva Perón 2726. Las nuevas oficinas están a la vuelta de la sede central de la Defensoría y los edificios se intercomunican. Foto: Flavio Raina.

 En el marco de la inauguración de este nuevo anexo, el Defensor del Pueblo de Santa Fe Zona Norte, Jorge Henn, mostró su satisfacción por el flamante espacio destinado a mejorar la atención de la ciudadanía, en un lugar accesible, con un salón de usos múltiples que cuenta con toda la tecnología necesaria, "el que estará a disposición de todas las organizaciones civiles e instituciones que lo precisen para realizar reuniones", destacó.

 "No se trata sólo de un nuevo edificio sino de las personas que lo integran", recalcó Henn en diálogo con El Litoral, "el compromiso de los hombres y mujeres que conforman la Defensoría para dar un servicio acorde a las circunstancias". Y sumó también la labor que realizan sus pares en el sur provincial, a donde está al frente de la Defensoría Gabriel Savino.

 **El Defensor**

El Defensor del Pueblo es un colaborador crítico del Estado que debe preservar las garantías individuales de toda la ciudadanía. Si bien no tiene poder de coacción, su fortaleza radica sus argumentaciones ante cada presunta injusticia cometida a un ciudadano, lo que da el prestigio a la institución. Y en ese sentido, hay que decir que la Defensoría de Santa Fe es reconocida en toda Latinoamérica.

 El Defensor tiene la potestad de solicitar una rendición de cuentas al Estado cuando la respuesta solicitada por un ciudadano no conforma. Así lo establece la Ley 10.396 que regula su funcionamiento. "No se trata de una cuestión legal", aclara Henn. "Puede ocurrir que un vecino haya recibido una atención inadecuada en un hospital u otro organismo y concurra a la Defensoría para recibir asistencia en sus derechos y elevar luego un reclamo o destrabar una situación que lo perjudica".

 "Es que a veces ocurre que hay situaciones en las que el Estado no cuida al ciudadano, que se pregunta '¿quién podrá defenderme?'. Ahí aparece la figura del Defensor para proceder en su defensa", explica Henn.

 **Mediaciones**

En algunos casos es necesaria una instancia de mediación comunitaria. El abanico de temas va desde lo institucional hasta lo ambiental. Las estadísticas de la Defensoría indican que el 70% de este tipo de conflictos alcanzan una resolución. "En estos espacios (que son confidenciales) han recaído los protagonistas de la mayoría de los conflictos que ha tenido la ciudad y el área metropolitana", detalló el Defensor.

 SUM. Este Salón de Usos Múltiples estará a disposición de todas las organizaciones e instituciones de la ciudad y la región que lo necesiten utilizar para realizar reuniones de trabajo.Foto: Flavio Raina.

Otro de los servicios que brinda la Defensoría es el Centro de Atención a la Víctima. Este espacio tiene 25 años de experiencia en abordajes multidisciplinarios. Y no se limita a lo jurídico sino también a los aspectos psicológicos y asistenciales. Hoy se aborda "con una fuerte perspectiva de género, sin descuidar a otros colectivos vulnerables como son los adultos mayores y las personas con discapacidad", enumeró Henn.

 **En toda la provincia**

Para llegar a la mayor parte de la ciudadanía, la Defensoría cuenta con dos oficinas móviles a la que pronto se le sumará una tercera, destinada a las localidades del norte provincial. "El objetivo se acercar cada vez más los servicios que se brindan", dijo Henn. Cabe agregar que además cuentan con 34 delegaciones en toda la provincia.

 **Acciones colectivas**

Otra de las particularidades de los reclamos que atiende la Defensoría son las acciones de incidencia colectiva. Si bien la regla son las acciones o reclamos de incidencia individual, como la presentación de un recurso de amparo, también rigen las acciones de incidencia colectiva que son presentadas por un ciudadano pero benefician luego a un colectivo en la misma situación. Esto tiene un mayor impacto social e implica un cambio rotundo en la forma de gestionar los conflictos.

 **En pandemia**

\-La pandemia trajo crisis, nuevos problemas y agudizó otros conflictos. ¿Qué reclamos recibieron en la Defensoría?

\-Así es. No solamente hubo un aumento de la violencia de género producto del aislamiento y la convivencia en el encierro, sino que aumentó la cantidad de mediaciones, se replicaron las estafas telefónicas y otros conflictos. Por lo que hubo que capacitar al personal de la Defensoría para dar respuestas. Ya no actuamos ante el derecho vulnerado sino que ahora también nos anticipamos pensando mecanismos de monitoreo de las normas, para actuar antes.

 **Ciberestafas**

Uno de los cambios más importantes que trajo la pandemia de Covid-19 fue el vuelco de las gestiones de las personas a los entornos digitales. Desde solicitar compras con delivery, hasta las transacciones bancarias, el trabajo remoto desde las casas o tomando conexiones de redes públicas, todo implicó una abrupta y masiva circulación de información y datos personales que en otro momento los usuarios hubieran hecho lo posible por proteger.

 Paralelamente, se registró un incremento de maniobras engañosas, bajo diversas operatorias de ingeniería social y tecnológica, provocando daño patrimonial a las personas y suplantación de identidad digital en otros.

 La comunicación digital tomo protagonismo a partir de la pandemia. En este contexto, se perfeccionan cada vez más rápido las modalidades de estafas y fraudes: perfiles falsos en redes sociales que envían mensajes directos, llamadas telefónicas, mensajes de texto o de WhatsApp y otras aplicaciones de mensajería, además de correos electrónicos engañosos para obtener datos personales y bancarios.

 - ¿En qué grado se incrementaron los casos de violencia de género y las estafas digitales?

\-De forma exponencial. Estamos trabajando con el Banco de Santa Fe para generar un curso de formación sobre conocimientos bancarios y lo digital, que es un terreno fértil para las estafas y ocupa el primer lugar en el ranking. Y también es cierto que el encierro generó la convivencia de víctimas y victimarios. La Corte dictó una resolución para generar acceso a los espacios de protección de las víctimas. Hoy un mensaje de WhatssApp a un funcionario tiene el valor de una denuncia. Y la Defensoría siempre estuvo al pie del cañón dando la respuesta que se necesitaba.

 -¿Qué otras cuestiones aparecieron con la pandemia?

\-Nos abocamos a asistir a muchos santafesinos que habían quedado varados en el exterior y no tenían ni para comer. Hubo que tramitar ayuda en los consulados para que los asistan. Y hubo que asistir a los cuentapropistas que se quedaron sin el sustento de vida, de un día para el otro. Por esos días, los equipos de trabajo ya no distinguían qué día de la semana era, trabajaron de sol a sol.

 "Con los pies en el barro"

\-En ese contexto, este nuevo anexo viene a dar respuesta a este incremento de la demanda.

\-Yo siempre pienso una Defensoría con los pies en el barro. Esta Defensoría tiene mucho prestigio a nivel internacional. Pero de nada sirve ello si toda esta formación de insumos no se traduce en una presencia en los sectores más vulnerables de la sociedad. Queremos seguir siendo una Defensoría de la gente. El compromiso es que cada familia que ingrese en busca de una respuesta sea escuchada y se puede resolver su problema.

 -¿Cómo se anticipan a los conflictos?

\-En el caso de las estafas, por ejemplo, hicimos campañas para que la ciudadanía entienda que nunca nadie los va a llamar del Ansés para ofrecerle un beneficio. También asesoramos sobre las nuevas normativas a inquilinos. En cuestiones de salud, hemos seguido de cerca las complicaciones que tuvieron algunos beneficiarios de la obra social Iapos para recibir algunas de las prestaciones que les corresponden.

 -¿Y qué otros conflictos nuevos están monitoreando ahora en la sociedad santafesina?

\-Creemos necesaria la creación de un órgano de revisión de salud mental, por ejemplo. Queremos que sea un tema que conforme la agenda de reuniones con el gobernador (Omar Perotti) y con el presidente de la Corte Superma de Justicia (Rafael Gutiérrez).

 **"Ciudadanía digital"**

En la actualidad se habla de la democracia digital o e-democracia. Esto es poner a Internet al servicio de la gente. Permite a los ciudadanos manifestarse, expresarse, movilizarse, además poder involucrarse en los asuntos públicos. "Este cambio es fundamental, estamos pasando a una era de la "ciudadanía digital" en donde hay un empoderamiento importante de parte de las personas y que será un elemento esencial para poder hablar en términos de calidad de la democracia", señaló Henn.

 Pero para hablar de ciudadanía digital es necesario tener el acceso masivo a las nuevas Tecnologías de la Información y la Comunicación (TIC), las cuales abarcan dispositivos tecnológicos como el celular, Internet, la computadora, la televisión y todos los servicios relacionados como redes sociales, blogs, foros, etc; así como elementos técnicos que permiten su funcionamiento. "La inserción de la ciudadanía a nuevas formas de conocer, de acceder a múltiples saberes y un nuevo mundo de prácticas culturales y de relaciones sociales cotidianas, implican la preparación de diferentes competencias digitales y el conocimiento de sus implicancias", finalizó Henn.

 **Servicios de la Defensoría**

\-Reclamos ante el Estado

\-Defensa de usuarios y consumidores

\-Obras sociales, salud y medio ambiente

\-Mediación

\-Asistencia a víctimas

\-Actuaciones en violencia de género

\-Asesoramiento en general

 **ATENCIÓN**

Para recibir atención ciudadana no es necesario solicitar un turno previo. En caso de necesitar un turno para otro trámite, se puede solicitar en el sitio web: defensoriasantafe.gob.ar ó bien a los teléfonos (0342) 4573904 – 4573374. El nuevo anexo de atención al público está en San Jerónimo 2721. El mismo abrirá sus puertas en enero. Mientras tanto, continúa abierta la sede central de Eva Perón 2726, con horario de atención: 08 a 13 hs y 13.30 a 18 hs.