---
category: La Ciudad
date: 2021-05-26T08:17:36-03:00
thumbnail: https://assets.3dnoticias.com.ar/jaton.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: Jatón admitió que ante el "comportamiento variable de la gente" habrá que
  ajustar los controles
title: Jatón admitió que ante el "comportamiento variable de la gente" habrá que ajustar
  los controles
entradilla: El intendente dijo que hay un alto acatamiento a las restricciones, pero
  que se reunirá con autoridades policiales para ajustar los controles

---
El intendente de la ciudad de Santa Fe, Emilio Jatón, participó del acto por el 211º aniversario de la Revolución de Mayo y luego se refirió a la situación actual por la pandemia de Covid. En ese sentido señaló que "en términos generales hay un buen comportamiento ciudadano", sin embargo, también admitió que eso "sigue siendo variable" por lo que deberá acordar con la policía provincial para ajustar más los controles.

"Es mucho el esfuerzo que se está haciendo en la ciudad de Santa Fe, hay muchos comercios cerrados, bares cerrados con un alto acatamiento. Creemos que el momento así lo indica. Ayer (por el feriado del lunes) hubo gente dando vueltas por algunos parques, es natural que eso suceda. A veces hay que tratar de quedarse en su casa, se los estamos pidiendo. Es el momento más crítico, la semana más crítica. En la ciudad de Santa Fe hemos superado ampliamente los 1.500 casos en una semana, las camas están al 100 por ciento. Hasta ahora hubo un alto acatamiento y esperamos que así continúe", señaló.

Sin embargo, inmediatamente agregó: "Habíamos acordado algo así como 17 controles, hay que seguir ajustando. El comportamiento de la gente sigue siendo variable, por lo que hay que ver cómo ajustamos los controles. Esto es día a día. Hoy seguramente vamos a hablar con las fuerzas policiales para hacer un balance de la ciudad de Santa Fe".

Sobre los controles a comercios, Jatón dijo que hay que tratar de no llegar a la clausura y que siempre hay una intimación previa. "Hay que tomar conciencia", pidió y dijo que "está la posibilidad del Take Away, no es la óptima. Sabemos que están haciendo un gran sacrificio. Pero les digo que el sacrificio va a valer la pena si salimos de esta".

Nosotros tenemos una sala de situación. Lo medimos cada siete días y la que tuvimos ayer nos dio muy alta porque tuvimos una tasa de contagiosidad arriba del 46 por ciento, más de 700 (contagiados) cada 100.000 habitantes; también el RT muy alto. Estamos en un momento crítico y esperemos que la semana que viene, después de estas restricciones, esos números sean diferentes.

Ante la pregunta de si el municipio va a tener alguna política de ayuda hacia los sectores productivos afectados por las restricciones, Jatón dijo que se está trabajando con cada uno de los sectores y que ya se tuvieron reuniones durante la semana. "Seguramente vamos a acordar con ellos lo mismo que hicimos al principio de la pandemia", señaló.

Al ser consultado por la situación del transporte público y el reclamo de los empresarios para funcionar con un esquema de emergencia, Jatón pidió poner en contexto la situación al asegurar que no es solo un problema de la ciudad de Santa Fe. "El transporte público en la ciudad está sustentado en un 60 por ciento con el pago del boleto y eso ha caído en un alto porcentaje lo que hace inviable en términos económicos el transporte. Que hay que rediscutir el transporte en la ciudad, sí; que hay que tener en el centro a los usuarios, sí. Eso es lo que vamos a hacer seguramente la semana que viene porque la reunión con los empresarios es a diario. Pero ahora vamos a sumar a más gente a esa mesa", concluyó.