---
category: Agenda Ciudadana
date: 2021-11-17T06:15:18-03:00
thumbnail: https://assets.3dnoticias.com.ar/albert.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: Alberto Fernández será el único orador en el acto de la militancia
title: Alberto Fernández será el único orador en el acto de la militancia
entradilla: Fuentes oficiales confiaron a Noticias Argentinas que se espera la participación
  de unas cien mil personas, lo que en clave peronista significa "mostrar capacidad
  de movilización"

---
En busca de relanzar la gestión del Gobierno y mostrar "unidad" en el Frente de Todos, el presidente Alberto Fernández será mañana el único orador en el acto por el Día de la Militancia en Plaza de Mayo.

Fuentes oficiales confiaron a Noticias Argentinas que se espera la participación de unas cien mil personas, lo que en clave peronista significa "mostrar capacidad de movilización", y precisaron que Fernández será "el único orador".

A metros de la Casa Rosada, el jefe de Estado tomará la centralidad de la actividad, dado que será el único dirigente que estará en el escenario principal.

Según supo NA, el resto de los convocantes e integrantes del Frente de Todos seguirán el discurso desde abajo de las tarimas, sentados en unas mil sillas que se distribuirán en las cercanías.

La movilización, cuya convocatoria fue realizada para las 15:00, tendrá como consigna principal "Todos unidos triunfaremos", y buscará transmitir el mensaje de que en la coalición oficialista está "de pie" tras la derrota electoral.

"Va a ser una señal de fuerza movilizada y también de unidad. Una foto que para nosotros tiene importancia hacia adentro y hacia afuera del país por las circunstancias que estamos atravesando", enfatizó en declaraciones a NA el secretario general de la CTA de los Trabajadores y diputado nacional, Hugo Yasky.

En diálogo con esta agencia, el sindicalista afirmó que "la expectativa es que sea una acto multitudinario, con una presencia de todos los sectores que componen el Frente de Todos".

"Se alinearon los planetas porque convocan todas las centrales sindicales, los movimientos sociales, los intendentes, las distintas agrupaciones internadas que componen la fuerza política", subrayó.

La actividad, que tendrá como corolario un documento oficial que se dará a conocer minutos antes de que emita su discurso el Presidente, reunirá a los principales dirigentes del sindicalismo y los movimientos sociales, además de los integrantes del Gabinete nacional.

Asimismo, están confirmadas las presencias de los gobernadores Axel Kicillof (Buenos Aires), Jorge "Coqui" Capitanich (Chaco) y Osvaldo Jaldo (Tucumán).

En referencia al ingreso de los asistentes a la Plaza de Mayo, detallaron que los gremios lo harán por Diagonal Sur, los movimientos sociales desde Avenida de Mayo, mientras que La Cámpora se sumará desde Diagonal norte.

En el marco de los preparativos finales para conmemorar el 49 aniversario del regreso de Juan Domingo Perón a la Argentina tras 18 años de exilio después de su derrocamiento en 1955, el jefe de Gabinete Juan Manzur se reunió hoy en la Casa de Gobierno con la nueva cúpula de la CGT e intendentes del Conurbano bonaerense, quienes cuentan con un gran poder de movilización.

El ministro coordinador se reunió con los jefes comunales Alejandro Granados (Ezeiza), Alberto Descalzo(Ituzaingó) y Juan José Mussi (Berazategui), además del diputado bonaerense y ex intendente de Florencio Varela, Julio Pereyra.

Al finalizar el encuentro, Héctor Daer afirmó que se trata de "la conmemoración de un día que es muy simbólico" para los peronistas.

"Escuchaba a algunos políticos de la oposición decir ¿Cómo van a hacer un acto? Hay quienes borran o no tienen historia y nosotros somos peronistas, tenemos historia", apuntó el sindicalista.

En tanto, Pablo Moyano destacó que luego de las elecciones "quedó demostrado que hay unidad", y amplió: "Estamos esperanzados de que estos dos años que quedan de gobierno, en la post pandemia, va a empezar a reactivarse la economía".

"Acá no hay transición como dijo el inimputable de Macri, sino que vamos a seguir trabajando para revertir la situación económica de los trabajadores y seguramente en el 2023 el peronismo va a seguir gobernando", concluyó.