---
category: La Ciudad
date: 2021-12-10T06:00:55-03:00
thumbnail: https://assets.3dnoticias.com.ar/LEGISLATURA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Presupuesto, la ley tributaria y emergencia en Seguridad llegarán entre el
  pan dulce y el brindis
title: Presupuesto, la ley tributaria y emergencia en Seguridad llegarán entre el
  pan dulce y el brindis
entradilla: La Legislatura no sesionó este jueves y a la agenda legislativa le quedan
  pocas fechas antes del receso de verano

---
El Senado no sesionó este jueves y el fin de año está a la vuelta de la esquina. En esa cámara aguardan su tratamiento tres leyes de suma importancia: el presupuesto 2022, la ley tributaria anual y la emergencia en seguridad. La semana que viene se formalizarán los encuentros entre los equipos técnicos de los senadores con los del Ejecutivo provincial para empezar a avanzar en los diferentes temas que componen los dos primeros proyectos de ley.

Además, el gobierno incluyó en el temario de extraordinarias la modificación del Código Procesal Penal (está en Diputados) y el pase a planta de los médicos contratados (tiene media sanción del Senado y ya está en Diputados), entre otros.

El diputado justicialista y presidente del PJ a nivel provincial, Ricardo Olivera, consideró que a pesar de lo apretada que está la agenda legislativa, "hay voluntad" para que la mayoría de los temas salgan antes de fin de año. "La reunión que tuvo Omar Perotti con los presidentes de bloque la semana pasada fue muy importante. El clima es diferente y creo que la emergencia en Seguridad, el presupuesto, la tributaria, el pase a planta de los médicos y los convenios (con Ansés y la EPE) deberían salir. Veo un escenario tranquilo y de acuerdo", sostuvo.

Por su parte, los senadores radicales advirtieron que hay cuestiones a revisar en la forma de financiamiento que se plantea en el mensaje del Ejecutivo, pero que lo que más los preocupa es el cumplimiento con los gobiernos locales. "Tenemos un problema serio que lo venimos marcando desde hace dos años, que son las transferencias a municipios y comunas. Ahí contamos lo adeudado en Obras Menores y, principalmente, las asignaciones por el Plan Incluir donde hay una discriminación flagrante a los gobiernos que no son del signo político oficial", dijo el senador radical, Felipe Michlig.

En ese tintero de temas a acordar con el Ejecutivo, Michlig también incluyó la distribución de la obra pública en los diferentes departamentos. Esos temas van a ser los que se empezarán a discutir formalmente la semana que viene en una mesa donde no se descarta que se sienten algunos diputados. De esa manera se buscarían acuerdos que permitan que luego no se produzcan modificaciones cuando el proyecto llegue a la Cámara baja.

**El impacto del fallo de la Corte en el Presupuesto**

El martes pasado se conoció el fallo de la Corte que ordena a Nación pagar a Santa Fe la deuda histórica (que tendría un piso de 86.000 millones de pesos). Eso cambia la situación del Presupuesto provincial para 2022.

Si bien Michlig reconoció que "hay cuestiones procedimentales que hay que definir y acordar" para que eso tenga un impacto real en la economía santafesina, ya se está pensando en cómo contemplarlo. "Tal vez se pueda implementar un artículo especial. A todas luces el monto de la deuda está desactualizado. La tasa pasiva que implementa el Banco Central está en casi un 25 o 30 por ciento de lo que deberíamos cobrar. El fallo dice $86.000 millones, Perotti dice que son más de $100.000 millones. Pero deberíamos cobrar entre 250 y $300.000 millones", dijo Michlig.

De más está decir que en el proyecto original que envió el Ejecutivo eso no está contemplado y hay varios legisladores que quieren analizar de qué manera eso se incluye en el Presupuesto 2022. Pero lo cierto, también, es que aún no hubo conversaciones formales entre Nación y Provincia para empezar a buscar un acuerdo para el pago. La negociación no será fácil.

A pesar de ese escenario de incertidumbre, los legisladores creen que el tema no puede quedar fuera de la ley de presupuesto y piensan en la posibilidad de incorporar alguna cláusula que se active en caso que Nación y Provincia lleguen a un acuerdo.

En ese sentido, una de las posibilidades concretas es que el pago se efectúe a través de bonos que sean canjeables o negociables en el mercado de capitales. "La cláusula lo que intentaría es acotar la discrecionalidad del Ejecutivo en cuanto a cómo canjear esos papeles y, también, sobre el destino que se les daría a esos fondos", explicó el diputado socialista, Joaquín Blanco.

"Ante la inminencia del fallo y el acuerdo del pago hay que buscar una redacción que explicite esos términos dentro de este presupuesto que vamos a tratar", indicó-

Además, en el Senado hay legisladores que quieren tener una idea cierta de hacia dónde irán esos fondos. Varios senadores y diputados se expresaron abiertamente sobre que el destino de esos recursos tiene que ser gastos de capital, tanto obras como equipamiento. Un antecedente no tan lejano fue cuando el entonces gobernador Miguel Lifschitz tomó deuda por 500 millones de dólares y los senadores hicieron un listado con las obras que pretendían para sus departamentos.

En tanto, Olivera consideró que como aún no se establecieron las condiciones del acuerdo eso no se puede establecer en este momento. "En el momento que se produzca el acuerdo se puede hacer una modificación presupuestaria y con eso se resuelve el tema", sostuvo.

Por otra parte, esos fondos que en algún momento llegarán desde Nación se coparticipan a municipios y comunas. Allí algunos analizan si está la posibilidad de que la Legislatura pueda imponerle a los intendentes y presidentes comunales que ese dinero también vaya a gastos de capital y no a erogaciones corrientes.

**La Tributaria**

El texto que llegó desde el Ejecutivo no presenta mayores inconvenientes. Sin embargo, hay intendentes que se están quejando porque el incremento de la patente tiene un tope del 30 por ciento, algo que está muy por debajo de la inflación que hubo este año y de la proyectada para 2022.

Ese impuesto tiene un gran impacto en los gobiernos locales porque se coparticipa en un 90 por ciento a los municipios. Por eso algunos jefes comunales están planteando la necesidad de una compensación por esa baja real que van a tener en la recaudación.

Ahora el Senado tiene convocadas sesiones extraordinarias para el miércoles 15 y jueves 16. Mientras que Diputados tiene previstas dos sesiones, para el 16 y el 23 de diciembre. Aunque el cuerpo tiene facultades como para adelantarla al 22 de diciembre. Esa podría ser la fecha donde se termine la actividad legislativa del año. Aunque, de hacer falta, también está la posibilidad de sesionar el 29 y 30 de diciembre.