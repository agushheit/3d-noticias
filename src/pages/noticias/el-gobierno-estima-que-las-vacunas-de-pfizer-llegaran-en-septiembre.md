---
category: Agenda Ciudadana
date: 2021-08-02T06:15:43-03:00
thumbnail: https://assets.3dnoticias.com.ar/PFIZER.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: El Gobierno estima que las vacunas de Pfizer llegarán en septiembre
title: El Gobierno estima que las vacunas de Pfizer llegarán en septiembre
entradilla: Así lo manifestó el jefe de Gabinete, Santiago Cafiero, quien reafirmó
  que "agosto va a ser el mes de la segunda dosis.

---
El jefe de Gabinete, Santiago Cafiero, dijo este domingo que "agosto va a ser el mes de la segunda dosis" en la vacunación contra el coronavirus, y añadió que "el Presidente peleó una a una todas las vacunas y las trajo a la Argentina", como la de Moderna, que tendrá un "uso pediátrico", y la de Pfizer, sobre la que contó que en el Ejecutivo tienen la expectativa de que "estén llegando para el mes de septiembre".

Sobre las próximas acciones del Plan Estratégico de Vacunación, Cafiero adelantó que la determinación del Ejecutivo es seguir "cerrando contratos con diferentes laboratorios sin ideologías, sin geopolítica".

En ese marco, habló de las vacunas desarrolladas por los laboratorios estadounidenses Moderna y Pfizer. "Tenemos expectativa de que las vacunas de Pfizer estén llegando para el mes de septiembre, después de cerrar el marco contractual que tenemos que terminar de firmar", detalló, y añadió que el abastecimiento con esos inmunizantes terminará de verse cuando se firme "el cronograma de entregas" con el Ministerio de Salud.

"Son vacunas que se van a seguir utilizando (de acá en adelante), así como Moderna para el uso pediátrico, para ampliar ese abanico de vacunas", explicó.

“En agosto vamos a empezar a completar los esquemas. Tenemos más del 50 por ciento de mayores de 60 años con las dos dosis”, aseguró el Jefe de Gabinete al ser entrevistado en Radio 10.

Sobre ese tema, al evaluar la marcha de la campaña vacunatoria, subrayó que la sociedad argentina ya tiene "una cobertura muy importante con primeras dosis", lo que está en línea con el plan inicial en la materia.

Entonces contrastó sus ideas con la oposición, a la que acusó de proponer un formato de vacunación en el que el acceso al inmunizante quedaba en manos de las posibilidades de cada individuo. "Jamás pensaríamos, como habían dicho, que el que pueda acceder (a la vacuna) se la dé y el que no pueda, no. Para nosotros no es así. Pensamos que tenemos que seguir avanzando en una vacunación equitativa que llegue a todo el territorio nacional y a todos los argentinos y argentinas", puntualizó.