---
layout: Noticia con imagen
author: Por José Villagrán
resumen: Presupuesto Municipal 2021
category: La Ciudad
title: "Presupuesto Municipal 2021: Lo que piensa hacer Emilio Jatón en un año
  electoral"
entradilla: "El intendente de la ciudad presentó el plan que prevé la
  municipalidad llevar adelante el próximo año. Habló de “presupuesto
  responsable y realizable”. ¿En qué se va a utilizar la plata de los/as
  santafesinos/as? "
date: 2020-11-18T13:03:37.578Z
thumbnail: https://assets.3dnoticias.com.ar/foto-presupuesto.jpg
---
**La Estación Belgrano fue el escenario que utilizó el gabinete municipal para presentarles a los concejales y concejalas de la ciudad el ejercicio 2021 en materia de gastos, recaudación y obras para Santa Fe**

“Es un presupuesto responsable, realizable, y que tiene dos elementos básicos: la obra pública y el impacto social”, destacó el mandatario.

### **Punto por punto, las áreas que pondera el Presupuesto Municipal**

El ejercicio 2021 que piensan Jatón y compañía está valuado en 13.486.347.790 pesos, un 27% más que este 2020. “Planificar el 2021 implica redefinir prioridades de cara a un nuevo contexto de post pandemia con eje en lo social y en la intervención del espacio público como espacio de cuidado”, reza el mensaje presentado a los ediles y ya elevado a la presidencia del Concejo Municipal.

La secretaria de Obras y Espacios Públicos es la que más fondos recibirá en caso de ser aprobado este presupuesto: $ 2.268.160.898 pesos, pero sin obras importantes que se destaquen. Tan solo, la obra de agua potable para Colastiné Sur (que es un reclamo histórico de los vecinos) y la revalorización de la calle Camino Viejo a Esperanza son trabajos de infraestructura que más sobresalen. Luego, el municipio intervendrá en obras de menor complejidad, como las de iluminación (en apenas 6 lugares de la ciudad y con un estipulado de $185.055.000 pesos) y la puesta en valor de 20 estaciones municipales.

En segundo orden, la secretaría de Ambiente es la que más dinero absorberá, con $ 2.066.971.967 pesos. Pero llama la atención que en el mensaje del ejecutivo municipal no se estipulo en qué ni cuáles serán los programas que se van a encarar desde el municipio.

Las áreas de Control y Convivencia Ciudadana – con un cálculo de $ 1.099.688.954 pesos – y Educación y Cultura con $ 1.070.681.164 pesos completan el top four de lo que pretende realizar Jatón en su segundo año de intendencia.

### **¿Cómo viene la recaudación impositiva en Santa Fe Capital?**

Antes de que se realice la presentación del presupuesto a los concejales, se rumoreaba que la municipalidad tenía previsto aumentar la Tasa General de Inmueble y crear nuevos impuestos a los sectores más acomodados de la sociedad santafesina. “No vamos a incrementar la TGI el próximo año” dijo el titular del Ejecutivo. “Y lo de la creación de nuevos impuestos, este presupuesto no lo contempla” remató en conferencia de prensa.

Así, como se puede apreciar en el siguiente cuadro, la secretaria de Hacienda piensa recaudar más por Derecho de Propiedad e Inspección que por TGI ($2.361.723.159 pesos contra $ 1.374.800.407 pesos).

Aunque este punto promete ser el que más discusión genere dentro del ámbito del legislativo de la ciudad. Es que, para el concejal de Juntos por Cambio, Carlos Pereira, al no modificarse el esquema de la TGI que seguirá como se lo aprobó el año pasado, “en enero del 2021 habrá partidas que se verán incrementadas entre un 80% y 100%, lo que registra un verdadero impuestazo”

En tanto que, para el secretario de Gobierno, Nicolas Aimar, “se mantiene el mismo esquema que planteó el Concejo este año en el presupuesto 2020 junto con la modificación de la Ordenanza Tributaria. Lo único que se conservan son algunos parámetros de actualización con los topes fijados oportunamente para evitar que se dispare la tasa”, explicó, y añadió: “en febrero se hablaba de la inequidad tributaria en algunos padrones de la TGI que habían quedado desfasados con relación a su valor real; no hay ningún tipo de modificación de la normativa, lo único que sigue vigente es el articulado”.

### PROGRAMAS SOCIALES

### **El proyecto incluye al Plan Integrar como un eje transversal en todas las secretarías.** 

En ese marco se incluyen importantes obras de mejorado de calles, desagües y recuperación de espacios públicos en los barrios priorizados del cordón sur, oeste y norte de la capital provincial. En este sentido, lo presupuestado para el año que viene llega a $1.645.744.238 pesos. “Esto refleja la definición política de ampliar el derecho a la ciudad de las y los santafesinos” lo defiende la municipalidad en el proyecto.

Más de la mitad del presupuesto (59%), según su finalidad, está previsto para proyectos de educación, salud y cultura; al mantenimiento del espacio público; y a tareas vinculadas con alumbrado y limpieza. Y el 20% se destina a obras y equipamiento para la comunidad, entre las que se priorizan el mantenimiento y mejoramiento de las calles, del sistema de desagües y de iluminación.

También para la Dirección de Mujeres y Disidencias se estableció un incremento del 47% respecto al año pasado, definiendo que llegue a 44.617.766 pesos para potenciar y sumar nuevas líneas al trabajo de atención y acompañamiento de víctimas de violencia de género y la promoción de derechos. Allí se sumarán nuevas estrategias de abordaje que apuntan a generar más oportunidades a esos grupos.

“Este presupuesto es hijo de un año de pandemia, no podemos dejar de pensar cómo convivir con la pandemia. Saben ustedes los altos índices de desocupación, la desintegración que está sufriendo la ciudad, y todo eso tiene que ver con este presupuesto que apunta a la cercanía y a la convivencia para restablecer los lazos con la ciudadanía”, destacó el intendente.