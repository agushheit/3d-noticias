---
category: Estado Real
date: 2021-02-09T08:27:12-03:00
thumbnail: https://assets.3dnoticias.com.ar/universidad.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: La provincia autorizó la apertura de actividades universitarias presenciales
title: La provincia autorizó la apertura de actividades universitarias presenciales
entradilla: A través de los Ministerios de Trabajo y de Salud, se firmaron acuerdos
  con autoridades de instituciones educativas públicas y privadas.

---
Con el objetivo de retornar paulatinamente a la modalidad de trabajo presencial, la provincia de Santa Fe autorizó la apertura de actividades en instituciones de educación superior públicas y privadas.

A tal efecto, el Ministro de Trabajo, Empleo y Seguridad Social, Juan Manuel Pusineri, y su par de Salud, Sonia Martorano, junto a los rectores Enrique Mammarella (UNL), Franco Bartolacci (UNR), Rubén Ascua (UNRAF), Rudy Omar Grether (FRSF-UTN), Eugenio Martín de Palma (UCSF) y Rodolfo De Vincenzi (CRUP), firmaron acuerdos tendientes a permitir el retorno de miembros de la comunidad académica a sus lugares de trabajo, respetando los protocolos de bioseguridad establecidos.

La autorización incluye el desarrollo de actividades presenciales de investigación, de acceso a talleres, laboratorios y/o trabajos de campo, actividades de transferencia, prácticas de laboratorio y prácticas supervisadas, actividades administrativas, actividades de mantenimiento y limpieza para cumplir con las condiciones de seguridad e higiene necesarias para llevar a cabo las mencionadas actividades. La autorización comprende las actividades mencionadas que se realicen tanto en los domicilios de las universidades, facultades y sus sedes como así también las que se desarrollan en otras dependencias.

Cabe aclarar que estas medidas podrán quedar sin efecto en caso que el Poder Ejecutivo provincial modifique la normativa vigente, conforme al desarrollo de la situación epidemiológica.