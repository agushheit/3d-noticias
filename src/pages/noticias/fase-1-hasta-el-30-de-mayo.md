---
category: Agenda Ciudadana
date: 2021-05-21T09:19:43-03:00
thumbnail: https://assets.3dnoticias.com.ar/alberto.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Fase 1 hasta el 30 de mayo
title: Fase 1 hasta el 30 de mayo
entradilla: El presidente Alberto Fernández informó cuáles son las restricciones que
  comienzan a regir el sábado, para contener la segunda ola de coronavirus.

---
Las restricciones anunciadas por el presidente Alberto Fernández vía cadena nacional para la contención de la segunda ola de coronavirus, son las siguientes

* Se restringe la circulación en todas las zonas del país que se encuadren en Alto riesgo o en Alarma epidemiológica.
* La medida regirá desde este sábado 22 de mayo a las 0 horas hasta el domingo 30 de mayo inclusive.
* Quedarán suspendidas las actividades sociales, económicas, educativas, religiosas y deportivas en forma presencial.
* Estarán habilitados los comercios esenciales, los comercios con envío a domicilio y para llevar.
* Sólo se podrá circular en las cercanías del domicilio, entre las 6 de la mañana y las 18hs, o por razones especialmente autorizadas.

La medida dura 9 días e involucra a 3 días hábiles.

Después sigue el siguiente cronograma:

* Terminados estos 9 días, desde el 31 de mayo hasta el 11 de junio inclusive se retomarán las actividades en el marco de las medidas vigentes hasta el día de hoy.
* Se implementarán las restricciones que correspondan a cada zona según los indicadores epidemiológicos y sanitarios.
* Firme decisión de hacerlas cumplir estrictamente.
* En ese lapso, tratando de favorecer bajar aún más los contagios, se dispondrá que el fin de semana correspondiente al 5 y 6 de Junio se vuelvan a restringir las actividades en las zonas más críticas.
* Es una medida de cuidado intensiva y temporaria.

**Medidas adicionales para familias y empresas**

El Gobierno Nacional anunció medidas adicionales para proteger a las familias y a las empresas.

**Empresas:**

* Ampliación del Programa de Recuperación Productiva (REPRO) para atender a los sectores que se verán afectados por las medidas: comercio y otros. La inversión en este programa estará en torno a los $52.000 millones.
* Incremento del monto del salario complementario para las y los trabajadores de los sectores críticos y la salud, que pasa de $18.000 a $22.000. La inversión prevista es de $6.000 millones.
* Incorporación del sector gastronómico al REPRO y reducción de las contribuciones patronales en los sectores críticos, por $8.500 millones.

**Familias**:

* Desde este viernes hasta fin de mes se inyectarán 18.000 millones de pesos en los bolsillos de las familias beneficiarias con la tarjeta Alimentar. Esto lo anunció el presidente el viernes 7 de mayo.
* Ampliación de la Tarjeta Alimentar que permitirá fortalecer los ingresos de las familias con menores de hasta 14 años. La medida alcanza a casi 4 millones de niños, niñas y adolescentes. La inversión mensual para financiar la Tarjeta Alimentar será de $18.100 millones y se comienza a pagar el viernes 21 de mayo.
* Ampliación del Programa Progresar con becas para la terminación de la primaria y la secundaria, la capacitación profesional, el cursado de carreras universitarias y, también, para formar enfermeros y enfermeras. El objetivo del programa es llegar a 1 millón de jóvenes en todo el país con becas mensuales de entre $3.600 y $9.600. Las becas se pagan durante todo el año (12 meses). La inversión durante este año supera los $28.000 millones.
* Ampliación de la AUH y asignaciones familiares. El objetivo es llegar a 700.000 y el estimado es de $29.000 millones.
* Se transformó el Programa Potenciar Trabajo para asociarlos con distintas actividades productivas de la economía popular. Los 920.000 trabajadoras y trabajadores de este programa cobrarán en mayo $12.204 y hacia fin de año $14.040. La inversión anual estimada para este programa es de $167.000 millones.

**Sectores**:

* Cultura y Turismo. Se han reforzado las partidas presupuestarias para asistir a estos sectores en más de $4.700 millones.
* Fortalecimiento del Sistema de Salud. Para la atención de la segunda ola del COVID, se estima una inversión de $144.000 millones. Incluye la inversión en vacunas de $72.000 millones, el bono por 3 meses de $6.550 para 700.000 trabajadores y trabajadoras de la salud y $36.000 millones de reducción de contribuciones patronales y del impuesto a los créditos y débitos bancarios para las empresas del sector, entre otras partidas. Con estas políticas estamos llegando a casi 6 millones de niños, niñas y jóvenes: a través de la Tarjeta Alimentar, la ampliación de AUH y el Programa Progresar.
* La reducción del impuesto a las ganancias para las y los trabajadores del sistema privado registrado, hará que cerca de 1.270.000 de trabajadores y jubilados dejen de pagar este impuesto con retroactividad a enero. La devolución de los importes retenidos se hará en 5 cuotas a partir del mes de julio. Esto equivale a un esfuerzo fiscal de más de $40.000 millones.
* El paquete de medidas que se han ido anunciando para morigerar el impacto de la segunda ola de COVID supone un gasto, por encima de lo establecido en el Presupuesto 2021, superior a los $480.000 millones (1.3% del PIB).
* Este incremento del gasto será financiado a través de los mayores ingresos explicado por el Aporte Extraordinario de las Grandes Fortunas y el aumento de la recaudación.