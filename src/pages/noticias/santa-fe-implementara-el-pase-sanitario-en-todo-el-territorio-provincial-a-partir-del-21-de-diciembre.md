---
category: Estado Real
date: 2021-12-17T06:15:25-03:00
thumbnail: https://assets.3dnoticias.com.ar/PUSIGOL.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Santa Fe implementará el Pase Sanitario en todo el territorio provincial
  a partir del 21 de diciembre
title: Santa Fe implementará el Pase Sanitario en todo el territorio provincial a
  partir del 21 de diciembre
entradilla: Además de lo dispuesto por Nación, Santa Fe también lo implementará para
  centros culturales, teatros, cines, gimnasios, salones de eventos, fiestas, casino
  y bingos.

---
El gobierno de la provincia de Santa Fe, a través del Decreto Provincial N.º 2915, adhirió a la Decisión Administrativa Nº 1198/21 de la Jefatura de Gabinete de Ministros de la Nación. Dicha disposición establece la necesidad de que toda persona que haya cumplido los 13 años de edad y que asista a las actividades de mayor riesgo epidemiológico y sanitario deberá acreditar que posee un esquema de vacunación completo contra la COVID-19, aplicado al menos 14 días corridos antes de la asistencia a la actividad o evento, exhibiéndolo ante el requerimiento de personal público o privado designado para su constatación, y al momento previo de acceder a la entrada del evento o actividad.

En conferencia de prensa, el ministro de Trabajo, Empleo y Seguridad Social, Juan Manuel Pusineri, indicó que “el gobierno Nacional dispuso el pase sanitario para todo el territorio de la Argentina, posibilitando que cada unas de las jurisdicciones obrara, a partir de esa norma general, en consecuencia”. Por este motivo, “en el caso de la provincia de Santa Fe, el pase sanitario va a regir a partir del día 21 de diciembre para las personas mayores de 13 años que concurran a las actividades que determina la norma nacional”. Además, en el caso de la “provincia de Santa Fe se dispuso una extensión a otras actividades”, detalló Pusineri.

A continuación, el ministro indicó que la página web www.santafe.gob.ar cuenta con “el botón “Pase Sanitario” donde se puede acceder a la aplicación Mi Argentina, donde se encuentran los elementos digitales para comprobar que una persona tiene el esquema completo de vacunación”.

Respecto de la reciprocidad entre provincias en el pedido del pase, el funcionario indicó que “en el ámbito de la provincia de Santa Fe, cualquiera sea la procedencia, de la persona que quiera desarrollar alguna de estas actividades, va a tener que contar con el esquema de vacunación completo”.

**ACTIVIDADES**

A la fecha, las actividades de mayor riesgo epidemiológico y sanitario enumeradas por la Jefatura de Gabinete de Ministros de la Nación, son: viajes grupales de egresados y egresadas, de estudiantes, jubilados y jubiladas, o similares; actividades en discotecas, locales bailables o similares que se realicen en espacios cerrados; actividades en salones de fiestas para bailes, bailes o similares que se realicen en espacios cerrados; y eventos masivos organizados de más de 1000 personas que se realicen en espacios abiertos, cerrados o al aire libre.

En el territorio santafesino, será exigible el esquema completo de vacunación para la concurrencia a centros culturales, teatros, cines y gimnasios; participación en reuniones o celebraciones sociales en salones de eventos, fiestas y similares; asistencia a casinos y bingos; y concurrencia a atracciones turísticas, en predios delimitados con controles para el ingreso de la concurrencia.

**IMPLEMENTACIÓN**

Estas medidas entrarán en vigencia a partir de la hora 0 del martes 21 de diciembre. La acreditación de contar con el esquema de vacunación completo será a través de la aplicación denominada “Cuidar” – Sistema de prevención y cuidado ciudadano contra la COVID-19 – apartado “Información de Salud” en su versión para dispositivos móviles.

Las personas que no pudieran acceder a la aplicación podrán acreditarlo con certificado de vacunación contra la COVID-19 en soporte papel y/o formato digital, acompañado con el Documento Nacional de Identidad, en el cual consten las dosis aplicadas y notificadas al Registro Federal de Vacunación Nominalizado (NOMIVAC).

Por último, las autoridades municipales y comunales podrán disponer en sus respectivos distritos la necesidad de contar con el esquema de vacunación completo, para participar en otras actividades adicionales a las definidas en el presente Decreto.

**EXTENSIÓN DE HORARIOS**

Por otro lado, a partir de la Resolución N.º 605 del Ministerio de Gestión Pública del gobierno provincial, se habilita la realización de las actividades que a continuación se indican, en los días y horarios que se precisan:

>> Actividad de los locales gastronómicos: los días jueves, viernes, sábados y víspera de feriados, hasta las 5 horas del día siguiente; el resto de los días de la semana, hasta las 4 horas del día siguiente.

>> Actividad de los salones de eventos, fiestas y similares, para la realización de eventos sociales: los días jueves, viernes, sábados y víspera de feriados, hasta las 5 horas del día siguiente; el resto de los días de la semana, hasta las 4 horas del día siguiente.

>> Actividad de los casinos y bingos, cumplimentando las reglas generales de conducta y de prevención y los protocolos oportunamente aprobados para la actividad: todos los días de la semana, entre las 10 horas y las 5 horas del día siguiente.