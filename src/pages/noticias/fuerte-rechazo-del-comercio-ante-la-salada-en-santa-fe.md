---
category: Agenda Ciudadana
date: 2021-07-01T08:58:10-03:00
thumbnail: https://assets.3dnoticias.com.ar/la-salada-se-quiere-instalar-santa-fe.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: Fuerte rechazo del comercio ante La Salada en Santa Fe
title: Fuerte rechazo del comercio ante La Salada en Santa Fe
entradilla: Desde Fececo advierten a las autoridades provinciales, municipales y comunales
  que la informalidad de La Salada absorberá a los comercios registrados

---
La Federación de Cámaras Empresarias del Comercio y Otras Actividades de la Provincia de Santa Fe publicó un comunicado en el que expresan su “preocupación” ante “la inminente instalación de La Salada” en territorio santafesino. En el texto se manifiestan “en estado de alerta permanente” ante las declaraciones de Jorge Castillo, coordinador general del complejo, quien manifestó públicamente sus deseos de instalarse en inmediaciones de la autopista Rosario-Córdoba.

En diálogo con el programa Ahí Vamos (de 9 a 12 por FM 106.3 La Radio de UNO) Eduardo Taborda, presidente de Fececo, manifestó la oposición del organismo: "Ya que todos conocemos que son formatos comerciales que normalmente no son legales, que no tienen mercadería de origen correcto, que tienen un montón de irregularidades”.

“Estamos alertando a los gobernantes y a los intendentes y presidentes de comunas el cuidado que deben tener para la instalación de estos formatos. Existen muchas leyes y muchas ordenanzas como para poder reglamentar o prohibir estos formatos que no consisten con las reglamentaciones vigentes para ejercer el comercio legal y formal. Estamos alertando para que esto no suceda”, insistió Taborda.

Jorge Castillo, el Rey de La Salada, asegura que va a abrir la feria en la autopista a Córdoba para el Día de la Madre.

Jorge Castillo, el Rey de La Salada, asegura que va a abrir la feria en la autopista a Córdoba para el Día de la Madre.

La Salada podría ubicarse en Santa Fe

En comunicación con el diario La Capital, Jorge Castillo, dueño de La Salada, contó que para el 15 de julio tiene que tener definido dónde va a instalarse. “En esa fecha, o antes, voy al municipio y presento el proyecto para ver qué me exigen y empiezo a rodar la pelota para inaugurar el paseo de compras el Día de la Madre”, detalló.

En cuanto al predio que necesita para montar su paseo de compras, recuerda que cuando arrancó con la idea y mandó a su gente de confianza a contactar propietarios de campos: “Tenía tres posibles y después de que hablé con el diario (La Capital) sumé once y este lunes aparecieron tres más, una de ellas sobre la ex-Ruta 9”.

“Ahora son más las ofertas de santafesinos que la de cordobeses, yo tomo propuestas de 20 hectáreas como mínimo, quiero estar en medio de las vacas, la soja y el maíz, no en la parte urbana porque genero caos vehicular. El proyecto que tengo armado es para una propiedad de hasta cien hectáreas y calculo que se podrán montar alrededor de 2.000 locales”, explica Castillo.

Y aclara: “Yo no voy a arreglar con nadie, voy por derecha como corresponde; solo necesito que me reglamenten y ordenen lo que hace falta para abrir”. Sobre la catarata de voces en contra, aclara: “Antes de hablar tienen que ver de qué se trata y como escuché, si ladran los perros, es señal de que cabalgamos”.

Más allá de tener más ofertas de tierras en territorio santafesino, el "rey de la Salada" afirma que le es indiferente la provincia donde se instale: “No me interesa si es en Córdoba o Santa Fe; la prioridad es estar en la autopista y tener una bajada lejos de todo, y el que quiera comprar que venga. No voy a un centro comercial a joder gente como muchos tarados dicen”. A modo de aclaración explica que no quiere estar cerca de Rosario sino lejos de la gente, “solo en medio del campo como en Mendoza que estoy a 80 km de la capital”. A este emprendimiento en la provincia cuyana lo considera un acierto.

Sobre la magnitud del futuro polo de la autopista, el comerciante hace una comparación con la feria de Mendoza: “La economía cuyana es chica al lado de la de Santa Fe y Córdoba. Teniendo en cuenta que en los últimos años hemos tenido dos pandemias, la de (Mauricio) Macri y la del bicho, y tomando solo la del bicho, en Cuyo donde teníamos 700 puestos, hoy hay 300. Si en esa zona más pobre hay esa cantidad, en este emprendimiento vamos a tener arriba de 2.000 puestos.

Eduardo Taborda, presidente de la federación que representa a los comercios santafesinos.

Eduardo Taborda, presidente de la federación que representa a los comercios santafesinos.

Las alarmas de los comerciantes

El titular de la federación que nuclea a los comerciantes santafesinos aseguró: “Estos formatos no nos ayudan a las economías, no nos sirven para un Estado provincial o nacional y menos para uno local. A veces se ven tentados porque les ofrecen mucho dinero pero eso hace que un montón de actividades y comercios legales terminen cerrando sus puertas porque los absorben”.

Taborda precisó que en el transcurso de las próximas semanas comenzarán a trabajar “un poco más enérgicamente” porque no hay una presentación de pedido formal todavía y que hasta el momento “todo lo que se sabe es a través de noticias periodísticas”.

Además, relató: “Hemos trabajado años anteriores en la Provincia de Santa Fe en gestiones anteriores, y se ha trabajado también a nivel nacional con las distintas cámaras que nos representan a nosotros, y sinceramente los gobernantes tienen mucha, mucha, mucha información y muchas leyes como para poder suspender o como para poder prohibir la instalación de estos formatos”.

La alarma de la Federación radica en informalidad dentro del formato comercial de La Salada. “No hay registro de empleados, el registro de la mercadería nunca se sabe de dónde es, así que no podemos avalar estas cosas”, dijo Taborda. “Son millones y millones de pesos que se evaden porque están diseminadas en distintos lugares de la República Argentina en mayor o menor escala”.

En este sentido, aclaró que “el consumidor no tiene la culpa de estas cosas porque trata de estirar su dinero lo más posible”. “Estos formatos existen porque hay compradores, pero también tenemos que tener esa responsabilidad social donde decimos «bueno, si esto no ayuda al entramado económico de un país no podemos sostenerlo»".

“Es muchísimo el dinero que se va de ahí porque no hay el registro de ese comercio; no se pagan ingresos brutos; no se paga IVA; es incalculable sinceramente, pero es mucho el dinero que el Estado pierde de recaudación”, concluyó.