---
category: Agenda Ciudadana
date: 2021-03-06T07:30:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/8m.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Día de la Mujer: el Gobierno provincial desobligará de sus tareas a las
  trabajadoras estatales'
title: 'Día de la Mujer: el Gobierno provincial desobligará de sus tareas a las trabajadoras
  estatales'
entradilla: Desde Ni Una Menos convocan a una caravana. Será a las 17, desde Canal
  13 hacia plaza 25 de mayo.

---
El gobierno de la provincia de Santa Fe decidió desobligar a las empleadas de la administración pública este 8 de marzo, en ocasión de celebrarse el Día de la Mujer.

En el decreto 0143, fechado este viernes 5 de marzo, el gobierno provincial adhiere "a la conmemoración del Día Internacional de la Mujer" y a "las reivindicaciones del movimiento de mujeres en todas sus manifestaciones".

En el artículo 2 del mencionado documento se especifica el beneficio para las empleadas de la administración pública.

El concreto, se destalla:

"Dispóngase que, en atención a las dimensiones y trascendencia pública que han tomado las manifestaciones y acontecimientos que llevará a cabo el referido movimiento de mujeres durante ese día, las agentes mujeres e identidades feminizadas de la Administración Pública Provincial quedarán desobligadas el próximo lunes 8 de Marzo de sus tareas habituales para facilitar su participación en las distintas actividades organizadas por el colectivo de mujeres, las organizaciones sociales y sindicales y la Secretaría de Estado de Igualdad y Género de la Provincia; con excepción de aquellas personas que se encontraren afectadas a garantizar los servicios esenciales y la seguridad pública".

**Ni una Menos organiza una caravana**

El lunes 8 de marzo, por la tarde, se convoca desde Ni Una Menos Santa Fe, a una caravana por el "Paro Internacional de Mujeres, Lesbianas, Travestis, Trans y No Binaries", donde se leerá una proclama, habrá ferias y actividades culturales. La invitación sugiere llevar barbijo y respecto del distanciamiento social porque "nos cuidamos entre todas".