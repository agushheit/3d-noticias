---
category: Agenda Ciudadana
date: 2021-04-07T08:03:16-03:00
thumbnail: https://assets.3dnoticias.com.ar/bares.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Diario Uno
resumen: 'Provincia de Santa Fe: qué restricciones evalúan ante el aumento de casos
  de Covid'
title: 'Provincia de Santa Fe: qué restricciones evalúan ante el aumento de casos
  de Covid'
entradilla: No hay medidas oficiales y por el momento las autoridades de la provincia
  esperan las decisiones del gobierno nacional ante el aumento de casos de coronavirus

---
Por estas horas las autoridades provinciales aguardan las decisiones que tomará el gobierno nacional ante el aumento de casos de coronavirus. Los primeros lineamientos de lo que podría suceder en la provincia si la curva de contagios sigue ascendiendo se analizaron este domingo en el marco de una reunión virtual que encabezó el gobernador, Omar Perotti, y su ministra de Salud, Sonia Martorano, junto a intendentes y presidentes comunales.

En ese cónclave, todos coincidieron en señalar que la situación sanitaria “es preocupante”.

Las fuentes consultadas coincidieron en remarcar que “no se tomará ninguna definición hasta que Nación no anuncie restricciones”.

**Lo que viene**

En tal sentido, trascendió que esas medidas las podría disponer el presidente Alberto Fernández el próximo viernes y se especula con que se restringirá la circulación nocturna, entre las 22 y las 6, pero no se prevé interrumpir el tránsito interprovincial.

Recién allí entonces podría haber novedades en Santa Fe. “Somos conscientes de que la situación sanitaria se viene agravando en la provincia, pero también sabemos que no es fácil volver atrás por las necesidades sociales y económicas de la población”, le dijo a La Capital un funcionario con estrecha llegada al despacho del gobernador.

En tanto, la lupa está puesta en las reuniones sociales y las fiestas clandestinas. “El mayor índice de contagios se da en ese tipo de encuentros, donde la gente se relaja, olvida los cuidados sanitarios y se reúne en ambientes cerrados donde el virus circula con rapidez”, admitió un funcionario de la cartera sanitaria.

Precisamente, las reuniones en ámbitos cerrados encienden un alerta roja en virtud de que el avance del otoño traerá aparejado un descenso de la temperatura y por consiguiente llevará a la población a realizar menos encuentros al aire libre.

De allí que la idea sea encolumnares detrás de Nación y restringir aquí la circulación nocturna y monitorear de cerca los encuentros afectivos, algo que se había dado en la primera ola de contagios, cuando había que tramitar un permiso para concurrir a ese tipo de encuentros.

Por lo pronto, todos coinciden en la necesidad de que las aulas continúen abiertas en el marco de esta bimodalidad que lleva a los alumnos a tener una semana con clases presenciales y otra virtual.

Y más allá de la restricción nocturna de la circulación, nadie se atreve a aventurar si habrá alguna otra, aunque es factible que los bares y restaurantes vuelvan a cerrar más temprano y no a las 2.30 como lo están haciendo hoy los fines de semana y vísperas de feriado.