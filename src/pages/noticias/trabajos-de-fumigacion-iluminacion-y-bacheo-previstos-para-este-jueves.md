---
category: La Ciudad
date: 2021-11-18T06:00:40-03:00
thumbnail: https://assets.3dnoticias.com.ar/fumilacion.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Trabajos de fumigación, iluminación y bacheo previstos para este jueves
title: Trabajos de fumigación, iluminación y bacheo previstos para este jueves
entradilla: "La Municipalidad informa el avance del cronograma de tareas previstas
  en los planes de fumigación, de bacheo y Santa Fe se Ilumina.\n\n"

---
La Municipalidad avanza con el calendario semanal de fumigaciones en distintos sectores de la capital provincial. Como es habitual, se utilizan insecticidas de baja toxicidad. Según se informó, se aplican larvicidas en las zonas donde hay zanjas y zanjones, y adulticida en parques y paseos.

En ese sentido, está previsto la realización de trabajos en:

Con pulverizador:

* Costanera Este completa
* Paseo de la Laguna
* Zona de Confiterías, frente a Ciudad Universitaria y espacios verdes linderos
* Barrio El Pozo completo
* Parque Garay completo
* Cantero central de bulevar Pellegrini
* Naciones Unidas, hasta el zanjón Suipacha

Con motomochilas:

* Predio Ferial
* Estación Terminal de Ómnibus
* Zona interior del Puerto, hasta el Club Regatas
* Molino Marconetti
* Barrios La Esmeralda, Altos del Valle y Las Delicias

Vale recordar que en caso de lluvia, se suspende el servicio.

**Iluminación**

En el marco del plan Santa Fe se Ilumina,el municipio concreta tareas tendientes a recuperar el alumbrado existente en toda la ciudad, para llegar a fin de año al 40% de la ciudad con luces led. Cuadrillas municipales trabajan en la colocación de nuevas columnas de iluminación y artefactos led en distintos barrios y avenidas, como así también en el mantenimiento de la red actual de la ciudad.

En ese sentido, se trabajará en la colocación de nuevas luminarias en:

* Los barrios: 7 Jefes, Guadalupe, Yapeyú y Ceferino Namuncurá
* “Avenidas de tu barrio”: recambio de luminarias vapor de sodio a tecnología led en calle Berutti entre avenida Blas Parera y el reservorio

También se realizará el mantenimiento del sistema eléctrico, entre otros puntos, en:

* El acceso a Alto Verde, por calle Demetrio Gomez
* Avenida Alem
* Espacios comunes en Las Flores II

**Bacheo**

Por otra parte, el municipio avanza con el plan de bacheo diseñado para reparar más de 12.000 metros cuadrados de calles de la capital provincial. El programa se desarrolla por fases, priorizando las zonas de la ciudad que evidencian mayor circulación.

Actualmente se trabaja en:

* Lavaisse y Gobernador Freyre
* 25 de Mayo y Santiago del Estero
* Aristóbulo del Valle, entre J.M. Zuviría y Milenio de Polonia
* Ecuador y La Paz
* Europa y Boneo

Según se informa, todas las tareas se realizan siempre en sentido del tránsito. Puede haber cortes de circulación y desvíos de colectivos a medida que avanzan las obras. Además, se detalla que los trabajos están sujetos a las condiciones climáticas.