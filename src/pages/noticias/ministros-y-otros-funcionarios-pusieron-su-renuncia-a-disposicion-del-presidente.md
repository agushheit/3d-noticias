---
category: Agenda Ciudadana
date: 2021-09-16T06:15:16-03:00
thumbnail: https://assets.3dnoticias.com.ar/WADO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Ministros y otros funcionarios pusieron su renuncia a disposición del Presidente
title: Ministros y otros funcionarios pusieron su renuncia a disposición del Presidente
entradilla: La decisión fue considerada por el titular de Desarrollo Territorial y
  Hábitat, Jorge Ferraresi, como "de forma" tras los resultados electorales del domingo.

---
El ministro del Interior, Eduardo "Wado" de Pedro, y otros funcionarios del Gobierno pusieron este miércoles a disposición del presidente Alberto Fernández su renuncia, en una decisión que el titular de Desarrollo Territorial y Hábitat, Jorge Ferraresi, consideró como "de forma" tras los resultados electorales del domingo.

"Si Wado eligió la manera escrita el día de hoy, nosotros elegimos presentar la renuncia oralmente el mismo domingo o el lunes. La diferencia es de forma", aclaró Ferraresi.

También pusieron sus renuncias a disposición los ministros de Ciencia, Tecnología e Innovación, Roberto Salvarezza; de Cultura, Tristán Bauer, y la titular de la Anses, Fernanda Raverta, entre otros.

Luego de conocerse la decisión de parte del Gabinete, varios funcionarios comenzaron a llegar a la Casa Rosada, entre ellos los ministros de Desarrollo Social, Juan Zabaleta; de Seguridad ,Sabina Frederic; de Producción, Matías Kulfas, de Economía, Martín Guzmán; de Trabajo, Claudio Moroni; el canciller Felipe Solá y el titular de Obras Públicas, Gabriel Katopodis.

También se encontraban en Casa de Gobierno el jefe de Gabinete, Santiago Cafiero; la vicejefa de Gabinete Cecilia Todesca; y la secretaria Legal y Técnica, Vilma Ibarra.

También se lo vio ingresar a la Casa Rosada el exjefe de Gabinete Aníbal Fernández, actualmente interventor de los Yacimientos Carboníferos Río Turbio.

Apenas pasado el mediodía, el primer texto que puso a disposición del presidente Alberto Fernández su renuncia fue el que presentó el ministro de Interior.

"Escuchando sus palabras del domingo por la noche, donde planteó la necesidad de interpretar el veredicto que ha expresado el pueblo argentino, he considerado que la mejor manera de colaborar con esa tarea es poniendo mi renuncia a su disposición", dijo De Pedro en una carta que remitió al Jefe de Estado.

De Pedro manifestó que "motiva la presente poner a su disposición mi renuncia al cargo de ministro del Interior de la Nación con el que he sido honrado desde el 10 de diciembre de 2019".

El ministro Salvarezza también confirmó a Télam que puso a disposición del Presidente su renuncia, del mismo modo que lo hizo el ministro de Cultura, Tristán Bauer, a través de un texto.