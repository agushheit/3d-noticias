---
category: La Ciudad
date: 2021-11-17T06:00:09-03:00
thumbnail: https://assets.3dnoticias.com.ar/botanico.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Seguridad, iluminación y transporte, reclamos que se repiten en barrios de
  Santa Fe
title: Seguridad, iluminación y transporte, reclamos que se repiten en barrios de
  Santa Fe
entradilla: "Los habitantes de Nueva Pompeya están pidiendo el traslado de la comisaría
  a un punto estratégico de la zona. Además piden más iluminación en los espacios
  públicos y mejor servicio de colectivo\n\n"

---
Los vecinos lindantes al Jardín Botánico "Ing. Lorenzo Parodi", reconocieron que después de la reunión que tuvieron con integrantes del Ministerio de Seguridad, comenzó a notarse “más patrullaje dentro del barrio y del Jardín Botánico, por otro lado la subcomisaría cuenta con una camioneta nueva”, contó un vecino.

 Ahora queda por definir el traslado de la subcomisaría 26, tanto la Provincia como la Municipalidad se mostraron a favor del cambio.

 “La idea – explicó el vecino- es trasladarla a San José y Gorriti para marcar más presencia de la entidad policial en una zona donde está el corredor escolar, comercial y el centro de salud. Es importante que se pueda controlar mejor el movimiento sobre la avenida”.

 Por otro lado, informaron los vecinos, que las autoridades escolares armaron una carpeta con todos los delitos cometidos en las distintas escuelas y lo presentaron a las autoridades. “Ahora estamos esperando una solución favorable para toda esta situación que nos llevó a la inseguridad que estamos vivimos todo el tiempo”.

 Para finalizar una vecina pidió que la línea 11 de colectivo, se detenga en Gorriti para que puedan subir los pasajeros, “muchas veces no frena, aunque tiene lugar para que la gente pueda viajar”