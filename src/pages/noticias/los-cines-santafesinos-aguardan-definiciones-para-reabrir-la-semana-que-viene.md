---
category: La Ciudad
date: 2021-02-21T07:30:31-03:00
thumbnail: https://assets.3dnoticias.com.ar/CINESs.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Los cines santafesinos aguardan definiciones para reabrir la semana que viene
title: Los cines santafesinos aguardan definiciones para reabrir la semana que viene
entradilla: Los 500 trabajadores del sector esperan una autorización del gobierno
  y planean movilizarse en caso de no tener respuestas. El Cine América se prepara
  para reabrir el 25 de febrero.

---
Los cines en Santa Fe aguardan una resolución de parte del gobierno provincial para poder reabrir sus puertas luego de 11 meses cerrados. Desde el Sindicato de Trabajadores de Espectáculos Públicos de la provincia (Sutep) adelantaron que funcionarios del Ministerio de Trabajo provincial les comunicaron que se pediría la autorización a la nación para habilitar la actividad.

Luego de lo que fueron las distintas manifestaciones de los empleados de cines (tanto en Santa Fe como en Rosario) se retomó el diálogo entre la cartera de Trabajo provincial y el gremio.

"Supuestamente el gobierno provincial iba a pedir la autorización a nación. Esto fue el sábado 13 de febrero. Seguramente si no nos dan una respuesta se tomarán medidas desde SUTEP en Santa Fe y en Rosario", manifestó a UNO el tesorero del sindicato.

De los 500 trabajadores de los distintos complejos de cines en la provincia, el 50% corresponde a las "Complejos Multipantalla Internacionales" (como Cinemark en Santa Fe), y el restante 50% de los trabajadores pertenecen a los "cines tradicionales", como lo es el Cine América de la capital provincial.

##### El Cine América piensa en reabrir

Por su parte el tradicional Cine América de la ciudad se prepara para una posible reapertura, en caso que se concreten las habilitaciones, el jueves 25 de febrero o el jueves 4 de marzo.

Desde el establecimiento afirmaron a UNO que se está poniendo en condiciones el sistema de aire acondicionado para tener todo listo para reabrir sus puertas.

Para Guillermo Arch, referente del Cine América, el regreso "depende de dos factores, donde uno tiene mas peso que el otro: uno es la habilitación, que yo creo que va a estar todo en orden a partir del lunes. El otro tema es que se llegue con la puesta a punto de los aires acondicionados".

"Estamos apurando las obras del aire acondicionado, para poder reabrir el 25 de febrero o el 4 de marzo. Estos deben tomar aire de afuera. Invertimos alrededor de los 180.000 o 200.000 pesos", destacó Arch.

El cine reabrirá sus puertas con la película "Nosotros nunca moriremos", de Eduardo Crespo. Arch se mostró entusiasmado anunciado que el director irá al lanzamiento de la película, "que fue seleccionada como mejor película en la competencia oficial del Festival de San Sebastián el año pasado".