---
category: Agenda Ciudadana
date: 2021-08-17T06:15:53-03:00
thumbnail: https://assets.3dnoticias.com.ar/sputnikfederal.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Las vacunas Sputnik V producidas en el país ya se distribuyeron a todas las
  provincias
title: Las vacunas Sputnik V producidas en el país ya se distribuyeron a todas las
  provincias
entradilla: Se trata de 994.500 dosis correspondientes al componente 1 y 152.500 del
  componente 2. Llegaron el fin de semana a las 24 jurisdicciones del país para avanzar
  en la aplicación de los esquemas.

---
Un total de 1.147.000 dosis de los componentes 1 y 2 de Sputnik V, producidas en la Argentina por Laboratorios Richmond, llegaron entre sábado y domingo a las 24 jurisdicciones del país para avanzar en la aplicación de los esquemas de vacunación contra el coronavirus.

Según se informó, este fin de semana la Argentina superó los 10 millones de personas con el esquema completo de inoculación, en el marco del Plan Estratégico que el Gobierno lleva adelante para combatir el coronavirus.

En tanto, del total de Sputnik V que se repartieron este fin de semana, 994.500 dosis corresponden al componente 1 y 152.500 son componente 2.

Según el criterio dispuesto por el Ministerio de Salud de la Nación, en base a la cantidad de población de cada distrito, a la provincia de Buenos Aires le correspondieron 382.500 dosis de componente 1 y 58.750 de componente 2; a la Ciudad Autónoma 67.500 y 10.125; a Catamarca 9.000 y 1.125; a Chaco 27.000 y 4.500; a Chubut 13.500 y 2.250; a Córdoba 83.250 y 12.625; a Corrientes 24.750 y 3.375.

En tanto, a Entre Ríos 29.250 y 4.500; a Formosa 13.500 y 2.250; a Jujuy 18.000 y 2.250; a La Pampa 6.750 y 1.125; a La Rioja 9.000 y 1.125; a Mendoza 42.750 y 6.750; a Misiones 27.000 y 4.500.

Asimismo, a Neuquén 13.500 y 2.250; a Río Negro 15.750 y 2.250; a Salta 31.500 y 4.625; a San Juan 18.000 y 2.250; a San Luis 11.250 y 2.250; a Santa Cruz 9.000 y 1.125; a Santa Fe 76.500 y 12.375; a Santiago del Estero 22.500 y 3.375; a Tierra del Fuego 4.500 y 1.125; y a Tucumán 38.250 y 5.625.