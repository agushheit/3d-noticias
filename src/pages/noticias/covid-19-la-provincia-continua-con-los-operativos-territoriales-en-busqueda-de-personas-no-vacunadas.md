---
category: Agenda Ciudadana
date: 2021-09-23T06:00:19-03:00
thumbnail: https://assets.3dnoticias.com.ar/OPERATVO.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Covid 19: la provincia continúa con los operativos territoriales en búsqueda
  de personas no vacunadas'
title: 'Covid 19: la provincia continúa con los operativos territoriales en búsqueda
  de personas no vacunadas'
entradilla: El objetivo es vacunar a aquellas personas que por distintos motivos no
  pudieron acceder a colocarse ninguna dosis contra el coronavirus.

---
El Ministerio de Salud provincial continúa llevando adelanto los operativos de territoriales en búsqueda de personas que por diferentes motivos no pudieron acceder a la vacunación contra el covid-19. El operativo se realiza con la vacuna Cansino, la cual es monodosis y permite completar esquema de vacunación con una sola aplicación.

Al respecto, el coordinador de Dispositivos Territoriales, Sebastián Torres, indicó: “Estuvimos en la zona noroeste de Rosario, en el Centro de Salud Nº 47 Comunidad Qom, como parte de la estrategia que el gobernador Omar Perotti y la ministra de Salud, Sonia Martorano, para poder alcanzar a aquellas personas que por algún motivo no han podido acceder a la vacuna”.

“El lunes estuvimos en la zona sur con el mismo propósito. Contamos con un equipo de más de 40 agentes sanitarios, el camión sanitario de la provincia y utilizando la vacuna Cansino, que es monodosis. Se conserva entre 2 y 8 grados de temperatura lo cual nos brinda una cierta facilidad y simpleza al momento de trasladarla al operativo que llevamos adelante”, continuó.

Agregando que “cuando uno encuentra a un vecino o vecina que no se colocó ninguna dosis, esta vacuna al ser monodosis, nos permite completar automáticamente el esquema de vacunación y de esta manera podemos avanzar en el objetivo principal que es tener más personas cubiertas, protegidas para que esta pandemia sea lo menos costosa desde la salud de la gente y podamos todos volver a una normalidad”.

Al ser consultado por la metodología del operativo, Torres destacó que “por un lado, los agentes sanitarios se encuentran recorriendo casa por casa del barrio pero también el vecino o la vecina se puede acercar de manera espontánea. Se los va a recibir, les informan de que se trata y si no tienen ninguna dosis colocada, se los vacuna”.

“Estas personas vacunadas quedan registradas. Cada una de las personas vacunadas se va con su carnet, con esquema completo y también queda cargada en el sistema informático y pueda figurar formal y legalmente como ya vacunada”, concluyó.