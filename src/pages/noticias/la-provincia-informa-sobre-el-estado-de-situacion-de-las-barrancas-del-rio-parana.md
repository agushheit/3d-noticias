---
category: Estado Real
date: 2021-07-25T06:15:29-03:00
thumbnail: https://assets.3dnoticias.com.ar/BARRANCAS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La provincia informa sobre el estado de situación de las barrancas del río
  Paraná
title: La provincia informa sobre el estado de situación de las barrancas del río
  Paraná
entradilla: Debido a la bajante extraordinaria los sitios con barrancas apreciables
  pueden estar en una situación de inestabilidad y producirse derrumbes de magnitud.

---
La Secretaría de Recursos Hídricos del Ministerio de Infraestructura, Servicios Públicos y Hábitat informa que debido a la bajante extraordinaria en que se encuentra el río Paraná, los sitios con barrancas apreciables pueden estar en una situación de inestabilidad por lo que se pueden producir derrumbes de magnitud, sin previo aviso.

También las estructuras que se hayan desarrollado sobre ellas, como muelles, playones, protecciones, entre otros, pueden encontrarse en una situación crítica e inestable, por lo que se debe verificar su estado, previo a la habilitación por parte de las autoridades locales, para su uso.

Por lo tanto, se deben tomar medidas de seguridad para que no se permita desarrollar actividades, de tránsito y/o de asentamientos, cercanos a las barrancas, tanto sobre ella como debajo, así como tampoco sobre estructuras que no hayan sido verificadas.