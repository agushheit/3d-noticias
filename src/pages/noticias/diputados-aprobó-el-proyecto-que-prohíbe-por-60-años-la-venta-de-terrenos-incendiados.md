---
layout: Noticia con imagen
author: "Fuente: Diario UNO"
resumen: Terrenos incendiados
category: Agenda Ciudadana
title: Diputados aprobó el proyecto que prohíbe por 60 años la venta de terrenos
  incendiados
entradilla: El proyecto que establece un plazo mínimo de 60 años para no
  utilizar tierras incendiadas, fue aprobado y girado al Senado.
date: 2020-11-19T12:31:14.038Z
thumbnail: https://assets.3dnoticias.com.ar/diputdos.jpeg
---
La Cámara de Diputados aprobó y giró en revisión al Senado el proyecto de ley que protege los ecosistemas de los incendios accidentales o intencionales y prohíbe la venta de terrenos incendiados en plazos que van de 30 a 60 años, para evitar prácticas especulativas y emprendimientos inmobiliarios.

La iniciativa fue aprobada con el respaldo de 132 votos aportados por el Frente de Todos, los interbloques Federal y de Unidad Federal para el Desarrollo, y la izquierda, mientras que Juntos por el Cambio, el Movimiento Popular Neuquino y el Partido Social reunieron 96 sufragios por la negativa, y hubo cuatro abstenciones.

La iniciativa –impulsada por el presidente del bloque del Frente de Todos, Máximo Kirchner– se aprobó en el marco de una extensa sesión –que comenzó ayer al mediodía y continuaba esta mañana– y contó con la participación en los palcos de invitados de militantes a favor de la protección del medio ambiente.

En el último tramo del debate hubo fuertes cruces con gritos incluidos entre el Frente de Todos y Juntos por el Cambio, cuando el diputado oficialista Leonardo Grosso aseguró que "el fuego va a dejar de ser un negocio" y cuestionó a funcionarios de la gestión anterior. Grosso consideró que "es incompatible ser medioambientalista y neoliberal: ser de Cambiemos y medioambientalista".

Esta situación obligó al presidente de la Cámara, Sergio Massa, a pedir que se mantenga el silencio en medio de los gritos y las interrupciones de integrantes del interbloque de Juntos por el Cambio al discurso de Grosso.

El proyecto reforma la Ley 26.815 del Manejo del Fuego y establece que "**no se podrá cambiar el uso de esas zonas para emprendimientos inmobiliarios o cualquier actividad agrícola** que sea distinta al empleo y destino que la superficie tuviera como habitual al momento del incendio".

El texto remarca que **ese uso no podrá ser modificado por el término de 60 años para que "los bosques nativos o implantados, áreas naturales protegidas y humedales" tengan garantizadas las "condiciones para la restauración de las superficies incendiadas".**

En esos plazos "no se podrán realizar en los bosques naturales o implantados la división, subdivisión, loteo, fraccionamiento o parcelamiento, total o parcial, o cualquier otro emprendimiento inmobiliario, distinto al arrendamiento y venta, de tierras particulares". En el caso de que se trate de una zona agrícola, se impone una limitación de 30 años. Tampoco se podrá "hacer cualquier actividad agrícola que sea distinta al uso y destino que la superficie tuviera al momento del incendio".