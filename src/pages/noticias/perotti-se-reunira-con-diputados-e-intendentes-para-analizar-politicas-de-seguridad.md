---
category: La Ciudad
date: 2021-11-27T06:14:34-03:00
thumbnail: https://assets.3dnoticias.com.ar/PEROSEGUD.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Perotti se reunirá con diputados e intendentes para analizar políticas de
  seguridad
title: Perotti se reunirá con diputados e intendentes para analizar políticas de seguridad
entradilla: Se confirmó el encuentro y el secretario de Gobierno, Oscar Urruty realizó
  las invitaciones correspondientes.  Los diferentes jefes de bloques de la Cámara
  Baja anticiparon su participación en la Casa Gris.

---
El gobernador Omar Perotti se reunirá el miércoles venidero, a las 16, con el presidente de la Cámara de Diputados y de los 17 bloques que componen el cuerpo para analizar la situación de la seguridad en la provincia. La decisión fue confirmada por el secretario de Gobierno, Oscar Urruty quien venía teniendo reclamos de legisladores para sentarse a una mesa a discutir la temática.

 Además de los jefes legislativos han sido invitados los intendentes de Rosario, Pablo Javkin, y de Santa Fe, Emilio Jatón, las ciudades más afectadas por la inseguridad.

 Del encuentro tomarán parte además la vicegobernadora Alejandra Rodenas; los presidentes de las comisiones de Seguridad de ambas cámaras, Rodrigo Borla (Senado) y Juan Cruz Cándido (Diputados). En tanto, el gobernador estará acompañado por el ministro de Seguridad, Jorge Lagna, y otros funcionarios.

 De la reunión surgirán algunos de los temas de seguridad que serán habilitados para el tratamiento en Extraordinarias de la Legislatura que el Poder Ejecutivo convocará el próximo martes.