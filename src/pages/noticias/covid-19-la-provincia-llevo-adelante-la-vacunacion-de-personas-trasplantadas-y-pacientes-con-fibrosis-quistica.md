---
category: Estado Real
date: 2021-04-16T08:33:25-03:00
thumbnail: https://assets.3dnoticias.com.ar/vacunacion.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: 'Covid-19: La provincia llevó adelante la vacunación de personas trasplantadas
  y pacientes con fibrosis quística'
title: 'Covid-19: La provincia llevó adelante la vacunación de personas trasplantadas
  y pacientes con fibrosis quística'
entradilla: Los operativos se realizaron en Rosario, Santa Fe y Rafaela. Mañana continuará
  en la localidad de Reconquista.

---
El Ministerio de Salud provincial inició esta mañana la vacunación contra el Covid-19 para personas trasplantadas y pacientes con fibrosis quística las ciudades de Rosario, Santa Fe y Rafaela.

En el Galpón 17 de la ciudad de Rosario se vacunaron 264 personas; en el Hospital Jaime Ferré de Rafaela se aplicaron 27 dosis;  y en la “Esquina Encendida” de Santa Fe inocularon a 111 ciudadanos y ciudadanas.

Al respecto, Martorano explicó que “comenzamos a vacunar trasplantados y fibroquísticos, en la ciudades de Rosario, Santa Fe, Rafaela y mañana continuaremos en Reconquista. Se vacunan desde el auto, sin descender del mismo, para que no se expongan”.

“Hay un cronograma nacional el cual estamos cumpliendo estrictamente (personal de salud, mayores de 60 años, 18 a 59 con comorbilidades, personal docente, fuerzas de seguridad) y con la llegada de vacunas, cada uno irá ingresando en el grupo correspondiente”, recordó.

Al ser consultada sobre la llegada de la segunda ola, la ministra de Salud detalló que “la situación de la provincia va en consonancia con la nacional, hay que extremar los cuidados, no socializar, evitar reunirse porque el nivel de contagios esta acelerado, hay una agresividad del virus que no la vimos en la primera ola, es más agresiva en gente joven y ello nos lleva al aumento de ocupación de camas. Iremos viendo día a día las distintas decisiones que hay que tomar”.

Por otra parte, la funcionaria se mostró entusiasmada con la llegada de nuevas vacunas, mientras continúa la vacunación a otros grupos objetivo como las fuerzas de seguridad. Ante esto, destacó que se inocularán cerca de 4 mil agentes en lo que resta de la semana.