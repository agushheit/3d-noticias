---
category: La Ciudad
date: 2021-08-24T06:00:40-03:00
thumbnail: https://assets.3dnoticias.com.ar/CASTRACIONES.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Cronograma de castración y vacunación para animales de compañía
title: Cronograma de castración y vacunación para animales de compañía
entradilla: La Municipalidad informa los puntos en que se llevarán adelante los operativos.
  Ambos servicios se prestan de manera gratuita.

---
La Municipalidad informa los lugares donde continúa la campaña de castración y vacunación móvil para animales de compañía. Se trata de servicios gratuitos que se ofrecen a todos los vecinos y vecinas de la ciudad, a los efectos de controlar la población felina y canina.

Cabe recordar que, en el caso de la castración, se realiza con turnos programados, por lo que se solicita a los interesados, ingresar a la web oficial www.santafeciudad.gov.ar/turnos. Allí se debe buscar el botón para “Solicitar un nuevo turno” y completar el formulario con los datos requeridos. Posteriormente, el sistema ofrece escoger la fecha, la hora y la sede a la cual concurrir. Vale señalar que el servicio se presta de lunes a viernes, excepto feriados, en el horario de 8 a 12.

Según se informó, desde este lunes 23 y hasta el próximo 28 de agosto, las castraciones se realizarán en el Distrito La Costa (Club Velocidad y Resistencia, sobre la Ruta 1).

Por otra parte, desde el lunes 23 y hasta el 3 de septiembre, serán en el Distrito Este (vecinal Alvear, en Belgrano 4432), el Distrito Suroeste (manzana 6 del Fonavi San Jerónimo), el Distrito Norte (vecinal San José, en Dr. Zavalla 7100), el Distrito Oeste (en Pedro Centeno 4100, Barranquita Oeste) y el Distrito Noroeste (vecinal Juventud del Norte, en Lehmann 7699).

**Vacunación**

En tanto, la vacunación gratuita es por orden de llegada y los lugares y horarios serán:

– Domingo 29 de agosto, de 14 a 17 horas: en la Ferial del Jardín Botánico, Gorriti al 3900

– Jueves 2 de septiembre, de 14 a 19 horas: en el Paseo Capital de Nicolás Rodríguez Peña e Independencia, barrio Centenario

– Sábado 4 de septiembre, de 14 a 17 horas: en la Feria Sueños del Norte, en la intersección de las calles Misiones y Alsina, barrio Santa Rita

– Sábado 11 de septiembre, 14 a 17 horas: en la Feria Dignidad Artesanal, que funciona en Peñaloza y Regimiento 12 de Infantería, barrio Las Flores

– Domingo 19 de septiembre, de 14 a 17 horas: en la Feria de Emprendedores y Artesanos de la Costanera Oeste, en Almirante Brown y Casanello.