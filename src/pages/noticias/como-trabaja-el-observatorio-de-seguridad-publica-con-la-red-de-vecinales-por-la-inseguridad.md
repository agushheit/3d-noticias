---
category: La Ciudad
date: 2021-08-09T06:15:34-03:00
thumbnail: https://assets.3dnoticias.com.ar/seguridad.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Cómo trabaja el Observatorio de Seguridad Pública con la Red de Vecinales
  por la inseguridad
title: Cómo trabaja el Observatorio de Seguridad Pública con la Red de Vecinales por
  la inseguridad
entradilla: Los índices de criminalidad son monitoreados por este organismo que funciona
  desde 2019. Planean realizar una encuesta de seguridad en conjunto con la UNL en
  distintos barrios de la ciudad.

---
Los distintos episodios de inseguridad sucedidos en distintos barrios de Santa Fe preocupan a la ciudadanía y mantienen ocupadas a las autoridades del Ministerio de Seguridad provincial. Con este presente se realiza un trabajo articulado constante entre la cartera de seguridad y la Red de Vecinales de la ciudad en la lucha contra la delincuencia, con colaboración del Observatorio de Seguridad Pública de la provincia.

El Observatorio de Seguridad Pública (OSP) es un cuerpo que funciona bajo la órbita del Ministerio de Seguridad provincial y en pleno contacto con el Ministerio Público de la Acusación. Inaugurado durante la presente gestión del gobierno provincial en 2019, se encarga de llevar un registro de los distintos indicadores de delincuencia en Santa Fe con datos estadísticos oficiales.

Un medio de Santa Fe entrevistó a la subsecretaria del Observatorio de Seguridad Pública, Luciana Ghiberto, quien sobre el cuerpo de investigación destacó: "El OSP tiene un trabajo de triangulación con la Dirección de Política Criminal del MPA, para generar información de muy buena calidad estadística respecto de lo que llamamos violencias altamente lesivas (homicidios y heridos de arma de fuego)".

Sobre el método de trabajo utilizado en el órgano de investigación, Ghiberto expuso: "Hay un trabajo cotidiano en lo que es la división de información entre la policía, el Observatorio y el MPA, tratando de pensar esas vivencias en contextos y clasificarlas. Estas clasificaciones pueden ser respecto a si se trata de criminalidad organizada, conflictos interpersonales, en situación de robo, trabajando en territorios particulares".

A su vez, Jorge Fernández, quien es secretario de Política y Gestión de la Información del Ministerio de Seguridad. A propósito, manifestó: "Nosotros estamos en contacto permanente con la red de vecinales. En ese diálogo buscamos acordar políticas específicas para ciertos barrios, puesto que no todos los barrios tienen los mismos problemas de inseguridad y las estrategias son distintas".

"Esto nos permite conocer un poco más que pasa en el barrio mas allá de las estadísticas. Es una impronta de la gestión, hay que generar redes de colaboración con la gente de los barrios, que son quienes saben lo que pasa, y buscamos generar esas redes independientemente del color político. Puede ser más o menos eficaz pero esta es la lógica", continuó sobre el funcionamiento del Observatorio de Seguridad Pública.

Entre los principales reclamos que realizan las vecinales con respecto a la inseguridad, se identifican "el de mayor presencia policial en la zona, mayor diálogo con las autoridades del Ministerio. Tratamos de atender todas las situaciones y escuchar a todas las organizaciones en sus diversas problemáticas", según lo graficó Fernández.

"En algunos casos hay similitudes, con problemas generales que afectan a todos los barrios que tienen que ver con hurtos o robos. Algunos barrios tienen ciertas particularidades como el accionar de bandas organizadas en determinado territorio y que hace que esa violencia sea mayor por la cantidad de heridos de armas de fuego, de usurpaciones, que hacen que se mayor la violencia altamente lesiva", continuó el funcionario de la cartera de Seguridad.

**"Encuesta de Victimización" en Santa Fe**

El OSP mantiene un proyecto que es la “Encuesta de Victimización”, al que todavía se aguarda que la pandemia permita hacerlo. Según la titular del Observatorio de Seguridad Pública, "esta encuesta medirá cuestiones como sensación de seguridad y conductas de autoevitamiento, que son conductas que complejizan el fenómeno del delito y nos permiten medir más que aquello que solo lo que las personas denuncian en la policía".

A su vez, Fernández profundizó: "Allí se podría establecer un comparativo entre una Encuesta de Victimización y otra mientras se repita en el futuro. Teniendo en cuenta los datos de criminalidad registrada, las denuncias que ingresan al sistema y las distintas encuestas de victimización se puede tener una mirada mucho más integral".

La encuesta estaba prevista para el año pasado, luego para el primer semestre de este año y luego se pospuso nuevamente. Sostuvieron que "deben estar las condiciones para poder entrevistar personalmente a un montón de gente o al universo al que se va a seleccionar. Los tiempos administrativos, burocráticos y los acuerdos políticos ya están".