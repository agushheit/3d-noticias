---
category: La Ciudad
date: 2021-06-20T06:00:12-03:00
thumbnail: https://assets.3dnoticias.com.ar/colombo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'Recreo: Colombo confirmó su candidatura a intendente en el plenario del
  Frente Progresista'
title: 'Recreo: Colombo confirmó su candidatura a intendente en el plenario del Frente
  Progresista'
entradilla: En el marco del encuentro virtual del Frente Progresista convocado por
  el concejal Daniel Medús  el actual mandatario hizo un anuncio resonante, buscará
  ser reelegido en los próximos comicios.

---
Este viernes se desarrolló en forma virtual un plenario del FPCyS de Recreo donde participaron alrededor de 50 personas. Entre ellas se encontraban presentes diversos referentes políticos y territoriales, como así también miembros de instituciones de la ciudad.

La convocatoria, coordinada por el edil socialista Daniel Medús, surgió por la iniciativa del grupo de concejales del Frente, quienes propusieron este primer encuentro del año de cara a las próximas elecciones en nuestra ciudad.

A través del encuentro, varios de los compañeros y correligionarios tomaron la palabra para realizar una evaluación de la marcha en las acciones de gobierno, efectuar recomendaciones en vistas del proceso electoral y reforzar la pertenencia al grupo que hace casi cuatro años ganó las elecciones en las categorías concejal e Intendente de Recreo.

A su tiempo, Medús indicó que “Con la firme convicción de construir una Recreo más inclusiva, accesible, participativa y productiva llevamos adelante nuestro trabajo, junto al deseo y sueño de cada recreíno y recreína, por esto podemos decir orgullosos que más del 80 % de lo que dijimos que íbamos a hacer en nuestro plan de gobierno está hecho y lo restante está en marcha”.

“Nos parecía propicio –prosiguió el concejal–, impulsar nosotros este primer encuentro y que Omar Colombo nos acompañe para que reciba el fuerte respaldo que su gestión al frente del Gobierno merece, y al mismo tiempo, pueda oír qué expectativas tiene el grupo en cuanto a su participación en los próximos comicios”.