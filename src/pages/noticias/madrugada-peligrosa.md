---
layout: Noticia con imagen
author: "Por: Lemos Marisa"
resumen: Madrugada peligrosa
category: Agenda Ciudadana
title: Madrugada Peligrosa
entradilla: Durante la madrugada del martes, una pareja fue víctima de otro
  hecho de violencia producto de la ola de inseguridad que azota a nuestra
  ciudad.
date: 2020-10-27T16:51:52.592Z
thumbnail: https://assets.3dnoticias.com.ar/madrugada.jpg
---
El hecho ocurrió en una vivienda de calle Avellaneda al 3000 en Barrio Candiotti Sur, donde un hombre y su esposa embarazada fueron agredidos por un delincuente al intentar ingresar a su vivienda luego de la jornada laboral.

Ante el momento de desesperación, y procurando defender a su familia, el dueño de casa empezó a forcejear con el malviviente y este con su arma lo golpeó en la cabeza dejándolo mal herido en el suelo. La mujer, que cursaba su sexto mes de embarazo e intentaba proteger a su hijo menor que se encontraba en la casa, también recibió fuertes golpes y sufrió la quebradura de uno de sus dedos al lograr activar la alarma.

Los vecinos, al escuchar los desgarradores pedidos de ayuda llamaron al 911 y salieron rápidamente para defender a la pareja. El delincuente intentó huir en un auto modelo Ford Focus pero al no encender lo dejó abandonado y escapó corriendo del lugar minutos antes de que arribaran las fuerzas policiales.

El dueño de casa fue trasladado y asistido al Hospital Cullen donde le hicieron treinta puntos y horas más tarde tanto él como su mujer fueron dados de alta.

En el domicilio trabajó Personal Policial y Personal de la Agencia de Investigación Criminal quienes secuestraron un arma de fuego calibre 22 utilizada en el atraco y un total de seis proyectiles de dicha arma dispersados en el interior del inmueble. El vehículo continúa retenido y sometido a pericias.