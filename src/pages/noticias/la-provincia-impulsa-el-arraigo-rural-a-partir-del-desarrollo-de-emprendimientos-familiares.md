---
category: El Campo
date: 2020-12-31T10:31:22Z
thumbnail: https://assets.3dnoticias.com.ar/3112-agricultura-familiar.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: La provincia impulsa el arraigo rural a partir del desarrollo de emprendimientos
  familiares
title: La provincia impulsa el arraigo rural a partir del desarrollo de emprendimientos
  familiares
entradilla: Se presentaron los Módulos Productivos para el Fortalecimiento de la Agricultura
  Familiar,  iniciativa que pone el eje en los pequeños productores y productoras.

---
El ministro de Producción, Ciencia y Tecnología, Daniel Costamagna, acompañado por el secretario de Desarrollo Territorial y Arraigo, Fabricio Medina, encabezó este miércoles la presentación del Programa de Módulos Productivos para el Fortalecimiento de la Agricultura Familiar iniciativa que busca generar un modelo económico-social con inclusión en las áreas rurales, poniendo eje en los pequeños productores y productoras, fundamentalmente de los sectores más marginados y aislados de la provincia y las comunidades de pueblos originarios y los trabajadores rurales.

Dicha iniciativa, impulsada por la cartera productiva en conjunto con el Ministerio de Educación, pretende fortalecer la formación de los jóvenes rurales que terminan su carrera en una institución de la provincia, permitiendo el acceso a un microcrédito que le permita comenzar con su propio emprendimiento, generando arraigo, dado que lo podrá ejecutar en su entorno familiar y con la posibilidad de continuar su capacitación a través del vínculo con su escuela origen y la asistencia técnica de los profesionales del territorio.

En tanto que está destinada a jóvenes mayores de edad, que cursen el último año de una institución educativa agraria secundaria (pública o privada) de la provincia de Santa Fe: Escuelas Agro técnicas, Escuelas de la Familia Agrícola o similares, con orientación a alguna de las actividades propuestas en el Programa Provincial. 

Cabe aclarar que los mismos deben tener su residencia familiar en zonas rurales de la Provincia, en lo posible con proximidad a las escuelas de las que egresan.

En la presentación, Costamagna indicó: «este es un gran programa que articula a la producción con la educación que incluye a los jóvenes y la familia en una fuerte distribución territorial. El gran desafío es acompañar el paso de la escuela secundaria a la actividad productiva. En los próximos meses de receso escolar debemos aprovechar para ir puliendo la herramienta para que al inicio del año próximo podamos tener identificados qué grupos pueden ser beneficiarios de esta iniciativa».

Por su parte, Medina destacó: «se dio inicio a una nueva etapa en la agricultura familiar. Se ponen en funcionamiento estos módulos productivos que apuntan, fundamentalmente, al arraigo y a aquellos jóvenes que quieren producir a su lugar de origen y, muchas veces, continuar con la producción familiar. Con esta herramienta estableceremos el vínculo entre el alumno y el emprendedurismo para que los jóvenes se queden en el territorio, fortaleciendo la agricultura familiar con inclusión y arraigo».

La iniciativa prevé la aplicación a los módulos productivos de la Comisión Nacional de Microcrédito (Conami) para contar con herramientas que permitan financiar acciones de capacitación, formación y seguimiento de emprendimientos de pequeña escala. 

El abordaje territorial comprenderá la incorporación de dichos módulos en las familias de pequeños productores con la finalidad de favorecer el desarrollo económico, la inclusión social y el arraigo. «Esta herramienta consta de módulos caprino, apícola, avícola (en huevos y pollos parrilleros) y hortícolas, también para aquellos que quieran iniciarse en la producción de alimentos de manera agroecológica», informó Medina.

«Lo que se inicia con esta presentación es el proceso de inscripción para que los directores de escuelas detecten la población pasible de recibir este tipo de fortalecimiento», concluyó el secretario de Desarrollo Territorial y Arraigo.

De la actividad participaron también el director Provincial de Educación Técnicas, Salvador Fernando Hadad y el referente territorial para el norte de Santa Fe de la Secretaria de Agricultura Familiar, Campesina e Indígena de la Nación, Ariel Menapacce.