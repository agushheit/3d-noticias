---
category: Estado Real
date: 2021-04-25T06:15:17-03:00
thumbnail: https://assets.3dnoticias.com.ar/ASAMBLEAS.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Provincia suspendió la realización de asambleas y actos eleccionarios
  presenciales de personas jurídicas privadas
title: La Provincia suspendió la realización de asambleas y actos eleccionarios presenciales
  de personas jurídicas privadas
entradilla: La medida rige desde este viernes y hasta el 2 de mayo próximo, en el
  marco de la emergencia sanitaria por la pandemia del Covid 19.

---
El gobierno provincial suspendió, mediante el decreto 386/2021, la realización de asambleas y actos eleccionarios presenciales de personas jurídicas privadas, desde el 23 de abril y hasta el 2 de mayo próximo, en el marco de la emergencia sanitaria por la pandemia del Covid 19.

Por tal motivo, aclara la norma, “las asociaciones civiles, sociedades, colegios y consejos profesionales deben tener en cuenta que la no realización de tales actos, no implica incumplimiento por parte de la persona jurídica ni de sus administradores”.

Asimismo, indica que “respecto al vencimiento de los mandatos de los integrantes órganos de administración y/o de fiscalización (directores de sociedades anónimas, administradores de sociedades por acciones simplificadas, integrantes de comisiones directivas y/o de consejos de administración, miembros de órganos de fiscalización, etc.), rige una norma de fondo que dispone que los integrantes de los órganos permanecen en su cargo hasta ser reemplazados”.

Desde la Inspección General de Personas Jurídicas de Santa Fe consignaron que “el vencimiento de los mandatos de integrantes de órganos de administración y fiscalización no produce la acefalía de la persona jurídica, debiendo quienes integren órganos sociales mantenerse en el ejercicio de sus respectivas funciones hasta tanto sean legal y efectivamente reemplazados y/o reelegidos, según el caso, sin que para ello sea necesario la declaración de una prórroga, en tanto ya está establecida por la ley”.

Además, que “pueden celebrarse reuniones a distancia por medios digitales, en los casos en que los estatutos lo prevean y en aquellos en los que la ley lo permite; y que las asambleas o actos eleccionarios solo podrán desarrollarse luego que las medidas restrictivas en protección de la salud publica cesen por disposición de autoridad competente. Recién allí las entidades podrán fijar libremente nueva fecha de reunión”.