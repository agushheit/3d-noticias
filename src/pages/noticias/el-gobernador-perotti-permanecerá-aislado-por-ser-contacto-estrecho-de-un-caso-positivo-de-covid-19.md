---
layout: Noticia con imagen
author: 3DNoticias
resumen: Perotti permanecerá aislado
category: Agenda Ciudadana
title: El gobernador Perotti permanecerá aislado por ser contacto estrecho de un
  caso positivo de Covid 19
entradilla: La decisión se tomó al confirmarse que el ministro Marcelo Sain
  diera positivo en su hisopado.
date: 2020-11-25T12:01:21.513Z
thumbnail: https://assets.3dnoticias.com.ar/perotti.jpg
---
El gobernador de la provincia, Omar Perotti, permanecerá aislado por ser contacto estrecho del ministro de seguridad de la provincia, Marcelo Sain, a quien en las últimas horas le dio positivo el hisopado para COVID 19.

**El gobernador, se encuentra aislado de acuerdo a los protocolos vigentes. No presenta síntomas y posee buen estado de salud.**

El gobernador y el ministro, compartieron el viaje a la provincia de Río Negro, que realizaron el pasado lunes con motivo de la firma de un convenio en materia de seguridad que suscribió la provincia de Santa Fe con el INVAP.