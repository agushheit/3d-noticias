---
category: Agenda Ciudadana
date: 2021-02-18T08:28:06-03:00
thumbnail: https://assets.3dnoticias.com.ar/inflasión.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de TELAM
resumen: Una familia tipo necesitó ingresos por $ 56.459 para no caer bajo la línea
  de la pobreza
title: Una familia tipo necesitó ingresos por $ 56.459 para no caer bajo la línea
  de la pobreza
entradilla: En tanto, para no caer en la indigencia, un hogar de cuatro integrantes
  necesitó al menos $ 23.722 en el primer mes de 2021.

---
La Canasta Básica Total (CBT) registró en enero un aumento del 4,2%, por lo que un grupo familiar compuesto por dos adultos y dos menores necesitó contar con ingresos por $ 56.459 para no caer debajo de la línea de la pobreza, informó este miércoles el Instituto Nacional de Estadística y Censos (Indec). 

A la vez, la Canasta Básica Alimentaria (CBA), que mide únicamente un consumo de alimentos, tuvo una suba mensual de 4,6%, de modo que el mismo grupo familiar precisó de $ 23.722 para no caer en situación de indigencia.

Para el caso de un hogar de tres integrantes -dos adultos y un adolescente- la CBA fue de $18.886 y la CBT de $44.948, mientras que para un hogar de cinco integrantes -dos adultos y tres menores de 5 años- los valores alcanzaron $24.951 y $59.382, respectivamente.

De esta forma, la CBT tuvo una variación de 39,8% respecto a igual mes del año anterior, mientras que la CBA registró un aumento de 44%, una diferencia que se explica porque las tarifas de servicios públicos y de transporte están prácticamente congeladas desde principios de 2020, mientras los precios del rubro de alimentos fueron de los que más subieron en los últimos meses.

La inflación minorista fue de 4% en enero y de 38,5% en los últimos 12 meses, en un contexto en el que se destacó la suba de 42,3% en los valores de alimentos y bebidas en el mismo lapso, incremento que no fue mayor debido a los acuerdo de precios y el establecimiento de "precios máximos" para algunos productos en medio de la pandemia de coronavirus. 

Al cotejar contra el primer mes de 2020, el incremento del costo de la canasta básica alimentaria marcó una desaceleración de 11,8 puntos porcentuales, mientras que la canasta básica total mostró una retracción de 12,9 puntos, debido a que el año pasado marcaron un acumulado de 55,8% y 52,7%, respectivamente. 

La Canasta Básica Alimentaria responde a los requerimientos normativos kilocalóricos y proteicos imprescindibles para un determinado grupo familiar, compuesto entre otros productos por pan, arroz, fideos, papa, azúcar, legumbres secas, carne, menudencias, fiambres, aceite, huevo y leche, entre otros. 

En tanto, para determinar la Canasta Básica Total se utiliza como base la canasta familiar y se le suma los precios de bienes y servicios no alimentarios.

A comienzos de este año, el Gobierno renovó el acuerdo de Precios Cuidados “un programa que incorpora productos que se van a mantener todo el año", indicó la secretaria de Comercio Interior, Paula Español. 

Los productos tienen una actualización trimestral enero-abril de 5,6% promedio; mientras que en alimentos y bebidas el ajuste acordado es de 6,35% promedio, en limpieza es de 4,4% y en higiene personal es de 5,1%.

Español destacó que los precios de los productos “tienen que ser referencia en su góndola, entonces no pueden estar entrando y saliendo del programa porque, si no, dejan de cumplir ese rol".

El último Índice de Precios al Consumidor (IPC) mostró un incremento del 4% para enero y de esta forma acumuló un incremento del 38,5% desde enero de 2020 lo que, de todas formas, marcó una desaceleración de 14,4 puntos porcentuales respecto del 52,9% que había mostrado contra 2019. 

Uno de los datos más destacados del primer mes del año fue que si bien el rubro alimentos y bebidas no alcohólicas no resultó el de mayor incidencia en la suba del nivel general para todas las regiones, sí fue uno de los primeros tres, con un incremento del 4,8%, por arriba del nivel general.

En ese segmento se destacaron, especialmente, las subas en alimentos frescos como carnes y derivados, con incrementos de entre 6,5% y 9,6% mensual, dependiendo de la región geográfica del país; y frutas, con alzas de entre el 7% y 13,1%.