---
category: La Ciudad
date: 2021-11-18T06:15:16-03:00
thumbnail: https://assets.3dnoticias.com.ar/concejopresos.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Elecciones: ¿cuál fue el concejal más votado por los presos de Santa Fe?'
title: 'Elecciones: ¿cuál fue el concejal más votado por los presos de Santa Fe?'
entradilla: Comenzó el escrutinio definitivo en la sede de la secretaría electoral,
  donde se contaron los votos de las cárceles santafesinas en el plano local. Votó
  solo el 11% del padrón carcelario.

---
Finalizadas las elecciones generales del domingo 14 de noviembre, durante el pasado martes comenzó el escrutinio definitivo de votos en la sede de la secretaría electoral y UNO Santa Fe presenció el conteo.

Los bolsones de votos fueron trasladados desde las cárceles al edificio de calle 1º de Mayo, donde se realizó el conteo en las categorías locales para todas las localidades de la provincia que renovaban bancas en sus concejos o la propia intendencia. Cada preso resguardado en alguno de los diez establecimientos penitenciarios de la provincia que tenga residencia en la ciudad de Santa Fe podía elegir entre una de las siete opciones de concejales.

Es sabido que en la capital santafesina se impuso con claridad la lista de Juntos Por el Cambio encabezada por Adriana Molina y en las cárceles el resultado también fue categórico, pero a favor de otro candidato y ahora edil electo: Juan José Piedrabuena.

El hecho es que el cantante de cumbia que lidera el espacio de Unión Federal se llevó el 41,3% de los votos emitidos entre los presos de Santa Fe según los datos del escrutinio definitivo-

Quien fue uno de los "outsiders" de las elecciones en el plano local se impuso con claridad en los establecimientos penitenciarios, dejando en segundo lugar a la candidata oficialista María Laura Mondino con el 19,5% de los votos y en tercer lugar a Jorgelina Mudallel, quien encabezó la lista del Frente de Todos y obtuvo un 11,9% de los sufragios en las cárceles.

Recién en cuarto lugar aparece Adriana Molina de Juntos por el Cambio, quien fue la candidata más votada en la ciudad pero que fue elegida solo por el 8,6% de los presos.

**Participó el 11% del padrón**

Otro dato revelador fue la cantidad de internos abarcados en el padrón electoral que efectivamente emitieron su voto. Si bien las fuentes del Tribunal Electoral indicaron que generalmente en cada elección la participación es poca, en torno al 15% del padrón, esta vez fue incluso menor. Votaron solo 92 de los 803 presos con residencia en Santa Fe albergados en los penales de la provincia, algo más del 11%. En la bota santafesina el padrón en las cárceles lo conforman aproximadamente 4.500 internos.

El resto de los votos de los presos se repartieron entre el espacio Barrio 88 y su candidata Eliana Ramos (6,52%), el otro "outsider" de la elección y activista Saúl Perman (5,43%) y el candidato Maximiliano Puigpinos del Frente Federal Vida y Familia (1%). Hubo dos votos en blanco y tres se declararon nulos.

**Hasta el sábado habrá escrutinio**

Fuentes del tribunal electoral de la provincia sostuvieron que el escrutinio definitivo se seguirá desarrollando hasta el próximo viernes o sábado, día en el cual esperan terminar con el conteo final tanto en categorías locales como nacionales. En la secretaría el recuento comenzó el pasado martes por la tarde, a las 18 horas.