---
category: La Ciudad
date: 2022-01-26T06:00:51-03:00
thumbnail: https://assets.3dnoticias.com.ar/TUNEL.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: En el Túnel Subfluvial habrá tránsito alternado hasta el jueves
title: En el Túnel Subfluvial habrá tránsito alternado hasta el jueves
entradilla: La medida fue dispuesta por la realización de obras de tendido de fibra
  óptica.

---
El Ente Interprovincial del “Túnel Subfluvial Raúl Uranga-Carlos Sylvestre Begnis” informó que durante esta semana -hasta el jueves 27 de enero inclusive- se dispuso la alternancia en la circulación de los vehículos entre las 23 y 5 horas.

 La medida obedece a que se realizan obras de tendido de fibra óptica, motivo por el cual se deben realizar tareas en el interior del viaducto.

 Ante esta situación, se solicita a los automovilistas circular con precaución, respetando las velocidades establecidas para una mayor seguridad.