---
category: Agenda Ciudadana
date: 2021-11-15T06:00:34-03:00
thumbnail: https://assets.3dnoticias.com.ar/ALBERTOPOST.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: 'Alberto Fernandez: "Nos estamos poniendo de pie"'
title: 'Alberto Fernandez: "Nos estamos poniendo de pie"'
entradilla: Anunció que convocará a "los representantes de la voluntad popular para
  acordar una agenda lo más compartida" y detalló que la deuda con el FMI significa
  el "escollo más grande".

---
El presidente Alberto Fernández dijo que "nos estamos poniendo de pie y se abre a partir de ahora una etapa nueva para nuestro país".  
  
En un mensaje grabado desde la residencia de Olivos, el mandatario destacó que la economía está creciendo un 9 por ciento este año y dijo que la ciudadanía necesita "más certidumbre cada día, cada mes".  
  
El presidente señaló que "con esta elección termina una etapa muy dura" para la Argentina y "comienza una segunda" a la que llamó a afrontar con esperanza.  
  
Además, Fernández anunció que convocará a "los representantes de la voluntad popular para acordar una agenda lo más compartida" posible.  
  
Según detalló el mandatario, la deuda con el Fondo Monetario Internacional (FMI) significa el "escollo más grande que enfrentemos para continuar en la senda de la recuperación económica".  
  
"Debemos enfrentar este desafío para reparar el enorme daño que el endeudamiento ha provocado y cuyas consecuencias pesarán por varias generaciones", dijo.  
  
En ese sentido, el mandatario anunció el envío de un proyecto de ley para los primeros días de diciembre que "explicite el programa plurianual para el desarrollo sustentable, con los entendimientos con el staff del Fondo Monetario Internacional en las negociaciones" que lleva adelante Argentina con el organismo "sin renunciar a los principios de crecimiento económico".  
  
"Es una decisión política y tiene el aval del Frente de Todos, la vicepresidenta, el Gabinete", dijo el jefe de Estado en un mensaje en el que reconoció que se cometieron "errores" en el marco de las elecciones legislativas que hoy se realizaron.