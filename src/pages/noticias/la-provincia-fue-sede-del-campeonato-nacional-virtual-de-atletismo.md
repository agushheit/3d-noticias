---
category: Deportes
date: 2021-03-03T05:53:16-03:00
thumbnail: https://assets.3dnoticias.com.ar/atletismo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: La provincia fue sede del Campeonato Nacional Virtual de atletismo
title: La provincia fue sede del Campeonato Nacional Virtual de atletismo
entradilla: El primer certamen del año tuvo sus competencias en el Estadio Municipal
  Jorge Newbery de Rosario.

---
El Campeonato Nacional Virtual de Atletismo para las categorías U16, U18 y U20 comenzó a desarrollarse de manera descentralizada en diversos escenarios de Argentina, debido la coyuntura sanitaria existente.

El primer certamen del año tuvo sus competencias el pasado sábado en el Estadio Municipal Jorge Newbery de Rosario.

En esta ocasión, participaron atletas pertenecientes a las Federaciones afiliadas a la Confederación Argentina de Atletismo (C.A.D.A), donde debieron realizar las inscripciones, y las clasificaciones finales se realizarán de manera unificada con las marcas registradas en otros puntos de Argentina

**Los datos destacados en Rosario**

El atleta U16, Santiago Dreuss con un registro de 45.11 mejoró el récord Rosarino en la prueba de 295 metros c/vallas. Otras marcas destacadas fueron las logradas en 100 metros llanos U23 por Alejo Pafundi quien logró un registro de 10.78 seguido de cerca por Pablo Zuliani con 10.94.

En la categoría mayor (esta fuera de campeonato) se vivió una intensa carrera en los 10000 metros llanos entre Julián Molina y Martin Mendez, sobre el final el ganador fue Julian Molina con 29.29.42.

Por su parte, Brian Impellizzieri con 6.45 metros logró mejorar su marca personal en Salto en Largo, y Yanina Martinez, campeona paralímpica en los 100 metros T36 de Río 2016, hizo un tiempo de 14.77 segundos y quedó a solo 31 décimas de su marca en los últimos Juegos.

Cabe señalar que en el Centro de Alto Rendimiento Deportivo (C.A.R.D.) en la ciudad de Santa Fe, las competencias estaban previstas para los días viernes y sábado, pero debido al fallecimiento del Profesor Gastón del Pino, fueron reprogramadas por la Federación Santafesina de Atletismo para el próximo fin de semana.