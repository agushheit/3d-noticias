---
category: La Ciudad
date: 2021-03-27T06:00:58-03:00
thumbnail: https://assets.3dnoticias.com.ar/unl.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Docentes de la UNL piden apartar a un abogado del programa de derechos humanos
  de la universidad
title: Docentes de la UNL piden apartar a un abogado del programa de derechos humanos
  de la universidad
entradilla: Docentes de la UNL nucleados en Adul pidieron que se aparte a un abogado
  del área de derechos humanos de la universidad, que habla de manera despectiva sobre
  los organismos de derechos humanos.

---
Docentes de la UNL nucleados en Adul pidieron que se aparte a un abogado del área de derechos humanos de la universidad debido a un audio que se viralizó, en el que la persona en cuestión habla de manera despectiva sobre los organismos de derechos humanos, las iniciativas locales de estas organizaciones y las referentes Hebe de Bonafini y Estela de Carlotto.

"Las presentaciones, apoyadas por todas las organizaciones que componemos el Foro Contra la Impunidad y por la Justicia de Santa Fe, se realizaron en función de que consideramos que el abogado Roberto Vicente ha dejado en claro la subestimación y rechazo que tiene por la labor de los organismos de derechos humanos locales y a nivel nacional, así como un pensamiento estigmatizante y denigrante respecto de quienes tienen un enfoque crítico de aquel que él suscribe partidariamente", dice el comunicado emitido por Adul.

Según narraron, los dichos de Vicente se produjeron luego de una reunión institucional de la Comisión del Espacio de Memoria Excomisaría Cuarta, que conforman distintos organismos de derechos humanos, organizaciones sindicales, UNL y áreas de estado provincial.

"El abog. Vicente, aparentemente, tenía intenciones de enviar su informe sólo a la otra representante de la UNL en la Comisión, la abog. Rocío Giménez. En lugar de ello lo envío al grupo de comunicación de toda la Comisión. Como inmediatamente le comentamos, más allá que luego intentara una disculpa, era triste conocer la realidad de su pensamiento tan retrógrado, prejuicioso, sectario, estigmatizante y de desprecio al trabajo de la Comisión y a la tarea de muy importantes organismos de derechos humanos, dejándonos en claro que era infructuoso e inaceptable compartir tareas significativas con él", señalaron desde el gremio de docentes universitarios santafesinos.

A continuación, el comunicado completo:

El martes 23/3 ingresamos formalmente dos notas, una dirigida al Rector de la UNL y otra al Consejo Directivo de la Facultad de Ciencias Jurídicas y Sociales. Junto al Foro Contra la Impunidad y por la Justicia, solicitamos que se aparte al Abog. Roberto Vicente de sus funciones en el área de los derechos humanos como representante de la UNL.

Las presentaciones, apoyadas por todas las organizaciones que componemos el Foro Contra la Impunidad y por la Justicia de Santa Fe, se realizaron en función de que consideramos que el abog. Roberto Vicente ha dejado en claro la subestimación y rechazo que tiene por la labor de los organismos de derechos humanos locales y a nivel nacional, así como un pensamiento estigmatizante y denigrante respecto de quienes tienen un enfoque crítico de aquel que él suscribe partidariamente. Por lo tanto, solicitamos a las autoridades de la Universidad que renueven las representaciones políticas de la UNL en sus líneas de acción sobre derechos humanos.

Expresamos de forma resumida las razones que nos llevaron a presentar este pedido:

1\. El día 4 de marzo de este año, se realizó una reunión programada e institucional de la Comisión del Espacio de Memoria ExComisaría 4ta. (en adelante: “la Comisión”). La misma se llevó adelante con normalidad y de un modo que merecía una evaluación positiva de todes les integrantes de la Comisión que representan a distintos organismos de derechos humanos, organizaciones sindicales, UNL y áreas de estado provincial. Un tema acordado, fue que, dada la necesidad de distanciamiento por pandemia, igualmente se hagan presentes las actividades de memoria en los espacios públicos de la ciudad, con la colocación de pañuelos blancos. En ese sentido, se comprometió, a propuesta del representante de ADUL, que esa y otras actividades que involucren a la UNL se iban a tratar en la Comisión de memoria para la UNL, que cuenta con representación de los distintos estamentos y del Foro contra la Impunidad y por la Justicia de Santa Fe.

2\. Al día siguiente, el abog. Roberto Vicente, quien participó como representante de la UNL, produjo su informe sobre lo tratado en la reunión como lo hacen, en general, cada une de les integrantes para los espacios a quienes representamos. El abog. Vicente, aparentemente, tenía intenciones de enviar su informe sólo a la otra representante de la UNL en la Comisión, la abog. Rocío Giménez. En lugar de ello lo envío al grupo de comunicación de toda la Comisión. Como inmediatamente le comentamos, más allá que luego intentara una disculpa, era triste conocer la realidad de su pensamiento tan retrógrado, prejuicioso, sectario, estigmatizante y de desprecio al trabajo de la Comisión y a la tarea de muy importantes organismos de derechos humanos, dejándonos en claro que era infructuoso e inaceptable compartir tareas significativas con él.

3\. En los mencionados mensajes de audio, el abog. Vicente, con permanente tono despectivo y de desdén, expresa que:

a) la Comisión pretende ser pluralista, pero “... son todos funcionarios del gobierno, claro, no me jodan”, el trabajo de la Comisión se reduce a gente “… que se hablan entre ellos …” y “… meter fotos, ese tipo de cosas …” para “… recuperar quienes estuvieron presos”;

b) el representante de sindicatos en la Comisión, secretario de derechos humanos de ADUL, es alguien “... que no tiene idea, ni siquiera tiene idea de lo que defiende, este es uno de los grandes problemas que tiene ese pibe …”, se coloca en el “… lugar lo más izquierdoso posible pero después avala todas las posturas de derecha, inclusive Milani”;

c) según lo propuesto para otros espacios públicos, como actividad previa al 24-3-2021, “… quieren meter pañuelos \[blancos\] … en la Universidad” pero nos tiene que frenar planteando que hay que tratarlo con las autoridades de la UNL “… porque la Universidad es un espacio pluralista”. A lo mejor, puede ser tengamos algunos pañuelos en el hall del rectorado, “como para decir una cosa así”;

d) hay que cuidarse porque “… mañana nos van a meter … después van a querer el aula también”, así como “… el lunes inauguramos el aula Sufragistas, el año que vienen van a querer que inauguremos el aula de las Abuelas o de las Madres”;

e) también hay que cuidarse porque “… porque en una de esas nos quieren traer a la Bonafini o a la Carlotto”.

4\. Expuesta brutalmente por el abog. Vicente su sentida visión de rechazo a la actividad de los organismos de derechos humanos, venimos a solicitar que se adopten medidas para que el mismo no entorpezca las actividades de la UNL sobre este trascendente tema. En tal sentido, pedimos que el abog. Vicente se abstenga de toda intervención, desde el Programa de Derechos Humanos y desde el Consejo Directivo de la FCJS, sobre el proyecto que hemos promovido de nombrar “Madres de Plaza de Mayo” a un aula de esa facultad. También solicitamos que la UNL no le renueve la confianza para el desempeño de funciones políticas universitarias sobre derechos humanos, como lo es la Dirección del Programa de Derechos Humanos de la UNL ni la representación institucional en ámbitos como la Comisión participativa de Memoria en la UNL.

5\. Tal como lo presenta la UNL, los programas son instancias políticas y de gestión. El cargo político de Dirección del Programa de Derechos Humanos tiene que ser ejercido por alguien que no ponga en duda el real y pleno compromiso con las tareas de memoria, verdad y justicia que han protagonizado los organismos de derechos humanos en la historia reciente de nuestro país. Justamente el Programa de Derechos Humanos de la UNL, tiene como uno de sus objetivos fundamentales: “Coordinar acciones y desarrollar proyectos conjuntos con organizaciones sociales de Derechos Humanos a nivel local, nacional e internacional.” Esto es, la UNL merece ser representada en esta trascendente tarea por quien de manera confiable se guíe por la valoración de la tarea de los organismos de derechos humanos resultando esto claro para la comunidad, en general, y, en especial, para los organismos de derechos humanos y organizaciones sindicales y sociales que comparten tareas con los mismos. La misma confiabilidad debe merecer quien represente a la UNL en otras instancias de gestión participativa con presencia y protagonismos de los organismos de derechos humanos.

Por las razones expuestas, solicitamos que:

a) El abog. Roberto Vicente se abstenga de toda intervención respecto del proyecto de nombrar “Madres de Plaza de Mayo” a un aula de la FCJS.

b) Haya una renovación en la Dirección del Programa de Derechos Humanos de la UNL y de las representaciones institucionales de la UNL para esta materia. La interferencia del abog. Roberto Vicente obstaculiza el importante objetivo de coordinar acciones con los organismos de derechos humanos, así como representa visiones de sectarismos y discriminación que son contrarias a un real y consecuente pluralismo universitario que fortalezca la memoria contra las discriminaciones políticas.