---
category: Estado Real
date: 2021-07-23T06:00:36-03:00
thumbnail: https://assets.3dnoticias.com.ar/UNL.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La provincia brindó detalles sobre la obra de ampliación edilicia de la UNL
title: La provincia brindó detalles sobre la obra de ampliación edilicia de la UNL
entradilla: El proyecto fue evaluado por la Dirección General de Desarrollo Sustentable
  del ex Ministerio de Medio Ambiente de la provincia y fue categorizado como de bajo
  impacto ambiental.

---
La Universidad Nacional del Litoral en el año 2017 presentó un proyecto de ampliación edilicia para mejorar su oferta educativa. El Edificio de Aulas Costanera Este generará nuevos espacios para el desarrollo académico y científico, donde funcionará un establecimiento de educación secundaria. Cabe destacar que la obra se emplaza en un sector lindero a la Reserva Ecológica, área natural, co-administrada por la UNL con Fundación Hábitat y Desarrollo.

El proyecto fue autorizado por la municipalidad de la ciudad de Santa Fe ya que se ajustaba a su reglamento de ordenamiento urbano y al reglamento de edificación vigente.

El proyecto fue evaluado por la Dirección General de Desarrollo Sustentable del ex Ministerio de Medio Ambiente de la provincia y fue categorizado como de bajo impacto ambiental (Disposición DGDS N° 37/2017).

La Universidad, a partir del año 2017, con las autorizaciones y aprobaciones de la Municipalidad y la Provincia de Santa Fe, realizó un Concurso Nacional a través del Colegio de Arquitectos de la provincia de Santa Fe, integrante de la Federación Argentina de Entidades de Arquitectos, elaboró los pliegos de condiciones generales y particulares, y llevó adelante una licitación internacional que dio como resultado la adjudicación de la obra a la empresa Coemyc S.A.

El proyecto cuenta con financiamiento internacional de la CAF - Banco de Desarrollo de América Latina.

Con la obra ya en marcha, cuatro años después del inicio del proceso descripto de amplia difusión pública, agrupaciones ambientalistas ingresaron pedidos de intervención a la Universidad, reclamos ante la Defensoría del Pueblo y ante el Ministerio de Ambiente y Cambio Climático de la provincia de Santa Fe.

El Ministerio de Ambiente y Cambio Climático, dentro del ejercicio de sus competencias, el día 2 de julio del corriente año, realizó una inspección en el predio sobre el inicio de la obra.

Las actuales autoridades provinciales mantienen conversaciones permanentes con todos los actores involucrados convencidos de la importancia de la participación ciudadana ambiental, respetando el marco legal e institucional.