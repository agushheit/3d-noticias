---
category: La Ciudad
date: 2021-11-20T06:00:06-03:00
thumbnail: https://assets.3dnoticias.com.ar/DENUNCIAS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: En oficinas municipales funcionarán dos Centros Territoriales de Denuncias
title: En oficinas municipales funcionarán dos Centros Territoriales de Denuncias
entradilla: "El intendente y el ministro de Seguridad firmaron un convenio para poner
  en marcha dos CTD más en la ciudad. Uno funcionará en la zona de la costa y el otro
  en el Cementerio. \n\n"

---
En la sede del Ministerio de Seguridad, el intendente Emilio Jatón y el ministro Jorge Lagna firmaron un convenio a partir del cual funcionarán dos nuevos Centros Territoriales de Denuncias en espacios municipales. Uno funcionará en la sede del distrito La Costa, en la ruta provincial Nº 1, kilómetro 2,7; y el otro en la sede del distrito Oeste, ubicado en el Cementerio Municipal, en avenida Blas Parera 5401.

Es importante destacar que estos dos espacios se suman a los tres CTD que funcionan en el Predio Ferial, detrás de la terminal de ómnibus, en Las Heras 2883; en la Av. Aristóbulo del Valle 7404; y en Teniente Loza y Mansilla, en Barrio Yapeyú.

Los Centros Territoriales de Denuncia funcionan como un espacio alternativo a las comisarías y brindan atención, orientación a la comunidad y recepción de denuncias relativas a hechos delictivos o contravenciones.

Atienden de lunes a viernes de 8 a 20 y garantiza a los vecinos el acceso directo a la justicia ya que todas las denuncias o trámites que se realizan en los centros son enviados automáticamente, vía online, a las fiscalías correspondientes. Son atendidos por jóvenes abogados recientemente recibidos y trabajan en forma coordinada con los Centros de Orientación para Víctimas de Delitos y otras organizaciones relacionadas, según corresponda.

**Descentralización**

En este sentido, el secretario General de la Municipalidad, Mariano Granato, hizo referencia a este convenio y contó que “en el marco del proceso de descentralización de la Municipalidad y del trabajo en conjunto con el Ministerio de Seguridad de la provincia, se llega a esta instancia que tiene que ver con tener más Centros Territoriales de Denuncias en Santa Fe”.

“Es una demanda de la ciudadanía porque el dispositivo de los Centros Territoriales es algo muy eficiente a la hora de tomar denuncias y de trabajar la calidad de la denuncia y sobre todo el cuidado del ciudadano a la hora de hacerla, así que es muy importante estos dos nuevos centros que van a empezar a funcionar”, agregó.

El funcionario municipal destacó que los lugares fueron elegidos a partir de la distribución geográfica de los otros Centros Territoriales de Denuncias que ya funcionan en la ciudad y de la demanda de la ciudadanía. “En esta primera etapa uno de los Centros Territoriales estará ubicado en la costa y otro en el Cementerio, distritos La Costa y Oeste, con la proyección de hacer otros dos más también”, anticipó.

Antes de finalizar, Granato remarcó: “Lo importante de destacar es la articulación entre el gobierno de la provincia y el Estado municipal en un tema tan sensible para todos y todas y poner a disposición los espacios municipales para trabajar este tema, así que estamos muy conformes con este trabajo, con la articulación y con mucha expectativa de lo que viene para adelante”.

**Trabajo coordinado**

Por su parte, Carla Pecorini directora provincial de Centros Territoriales de Denuncia, detalló: “Los CTD vienen a suplir la tarea administrativa policial y tienen la impronta de ser atendidos por jóvenes profesionales del derecho, y a su vez por profesionales de la salud, como hay en el Centro Territorial de calle Las Heras, donde se constatan denuncias que contengan lesiones. Por eso celebro esta posibilidad de poder llevar a cabo la firma de un convenio que fue producto de un compromiso de Jorge Lagna y en conjunto con el intendente Emilio Jatón”.

La funcionaria provincial destacó también la rapidez con la que se coordinaron las tareas con la Municipalidad y “nuestra idea es pronto inaugurar estos dos nuevos espacios en Santa Fe, que suman a tres que ya están funcionando”, dijo.