---
category: Estado Real
date: 2021-07-27T06:00:02-03:00
thumbnail: https://assets.3dnoticias.com.ar/CEMTROSTESTEO.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: El gobierno provincial sumará dos centros de testeo espontáneo en la ciudad
  de Santa Fe
title: El gobierno provincial sumará dos centros de testeo espontáneo en la ciudad
  de Santa Fe
entradilla: Se trata del Centro de Especialidades Médicas Ambulatorias de Santa Fe
  (Cemafe) y el hospital de Campaña Iturraspe. De esta manera serán 6 las unidades
  de testeo territorial.

---
El Ministerio de Salud provincial sumará, esta semana, cómo centros de testeos espontáneos al Centro de Especialidades Médicas Ambulatorias de Santa Fe (Cemafe), ubicado en Mendoza 2419, y el hospital de Campaña Iturraspe, que se encuentra en Boulevar Pellegrini 3551, ambos de la ciudad de Santa Fe.

De esta manera, el Cemafe y el Iturraspe, se suman a las unidades de testeos territorial ubicadas en la Estación Belgrano (bulevar Gálvez y Dorrego), El Alero Dorrego (avenida French 1701), la Estación Mitre (avenida Gral. López 3698) y el Playón Municipal (Blas Parera 6250).

Para acceder al análisis, los ciudadanos y ciudadanas pueden acceder sin turno, presentando algún síntoma o, en el caso de no poseer síntomas, ser contacto estrecho de algún paciente que transcurre la enfermedad de Covid-19.

Al respecto, el director de la Región de Salud Santa Fe, Rodolfo Rosselli, indicó que “a los 4 puestos fijos que tenemos en la ciudad capital, incorporamos al Cemafe, que trabajará de lunes a viernes de 8:30 a 12:30 horas y a partir del día miércoles se incorpora también el hospital de campaña Iturraspe, que hará lo propio de lunes a viernes de 9 a 13 horas”.

“De esta manera contaremos con 6 unidades de testeos territorial de manera espontánea aumentando así el acceso de las personas a los hisopados. Recordemos además que esto se agrega como complemento de los llamados de ciudadanos y ciudadanos al 0800covid, quienes realizan las derivaciones correspondientes para los análisis”, agregó.

“Hemos visto un impacto sanitario importante debido al aumento de manera significativa de testeos en la ciudad de Santa Fe, desde el 12 de junio a la fecha llevamos realizados alrededor de 18 mil hisopados. Esta sin dudas en una de las herramientas por el cual ha comenzado a descender el número de casos”, concluyó Rosselli.