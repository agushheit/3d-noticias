---
category: El Campo
date: 2021-04-08T05:25:33-03:00
thumbnail: https://assets.3dnoticias.com.ar/in1617815525576.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: LT10
resumen: 'Riacho Santa Fe: realizan la reforestación del terreno'
title: 'Riacho Santa Fe: realizan la reforestación del terreno'
entradilla: Una empresa contratada por los responsables de los movimientos de suelo
  indebidos está plantando alrededor de 100 árboles en el lugar. Los trabajos culminarían
  este viernes.

---
A comienzos de febrero, la Municipalidad clausuró obras ilegales que llevaba adelante una constructora a la vera del riacho Santa Fe, al costado de la ruta nacional 168.

Allí, sin ningún tipo de habilitación, habían levantado un terraplén de dos metros y efectuado grandes excavaciones de tierra y movimientos de suelo, además de extraer piso por debajo del puente que conecta con Alto Verde.

Con la intervención de la Justicia, se estableció que los responsables de esas acciones ilícitas fueron Daniel Questa (dueño del terreno), Ignacio Benuzzi (apoderado) y la firma Coivalsa S. A. Todos ellos fueron intimados a restituir el predio a su estado original.

Las tareas con ese fin se iniciaron en marzo, y este viernes tendrán su culminación con la reforestación.

El móvil de LT10 pudo constatar que desde hace más de una semana, tres hombres trabajan para plantar 30 palmeras, 30 ceibos, 30 sauces y 10 palos borrachos.