---
category: Agenda Ciudadana
date: 2021-08-04T06:15:50-03:00
thumbnail: https://assets.3dnoticias.com.ar/MARTORANO.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: " Hagar Blau Makaroff para El Litoral"
resumen: Para Martorano, en Santa Fe la variante Delta será comunitaria a fines de
  agosto
title: Para Martorano, en Santa Fe la variante Delta será comunitaria a fines de agosto
entradilla: Ya se evalúa firmemente sumar una tercera dosis de refuerzo o repetir
  el año próximo un calendario nuevo.

---
La titular de la cartera sanitaria provincial pronosticó que la cepa que más preocupa a escala global será de transmisión comunitaria a finales de este mes en la bota santafesina.

La ministra de Salud provincial, Sonia Martorano, destacó esta nueva etapa de vacunas a los de 12 a 17 años con factores de riesgo que es una población estimada de 80.000 chicos. Fue en Rosario junto a Omar Tabacco, presidente de la Sociedad Argentina de Pediatría (SAP), el secretario de Salud Jorge Prieto y Mónica Jurado, directora del hospital de niños Zona Norte, donde fue lanzado este primer día para esa población.

Entre otros aspectos que maneja su cartera, dio a conocer su previsión de que a fines de agosto, a pesar de todos los esfuerzos por aislar a cada caso, la variante Delta será comunitaria en esta región, ya que se detectaron casos en Buenos Aires sin encontrar conexión con viajeros, y aseguró que “sobre posibles terceras dosis en el calendario de vacunación, se está hablando de hacer eso o de comenzar un esquema nuevo de vacunaciones el año que viene”.

En Rosario se establecieron los centros de vacunación para esta población objetiva en los dos hospitales de niños, el Zona Norte y el Víctor J. Vilela, y en la ciudad de Santa Fe, en el Orlando Alassia, para los adolescentes con enfermedades respiratorias crónicas, asma, cardíacos de base congénitas de grupo, diabéticos, obesidad, enfermos renales crónicos, HIV, trastorno en el desarrollo intelectual o con certificado único de discapacidad y los que se encuentran en lista de espera de trasplante. El orden de las aplicaciones en este caso no será descendiente por edades, ya que Martorano destacó que “se está comenzando con los más priorizados por mayor riesgo”.

Resaltó para llevar tranquilidad que para esta instancia “se trabajó fuertemente con el Cofesa, con los ministros y las sociedades científicas, así como la Sociedad de Pediatría, para generar un marco de confianza y seguridad, y para delinear el grupo objetivo de esta vacunación”, y agregó que “un grupo de fármaco vigilancia los irá contactando posterior a la aplicación de la vacuna, para seguir su evolución”.

Para este grupo ingresaron 70.280 vacunas, el estimado desde la Nación, pero la ministra especuló que pueden ser 80.000 adolescentes y niños. Sobre ellos, recordó: “Tenemos inscriptos el 25 por ciento, y por eso queremos pedirles que se inscriban, como pasó con los jóvenes que por suerte se fueron sumando cuando lo hemos pedido. Recomendamos que, para ello, hagan consulta con su pediatra si es pertinente su vacunación o no”.

Este martes se están vacunando 7.000 chicos en toda la provincia, por lo que la ministra explicó que “en la semana ya estarán todos vacunados”, y a su vez destacó: “Iremos con un operativo como hicimos con mayores, en los centros de día donde haya adolescentes”. Aseveró que están “aseguradas las segundas dosis de Moderna” para este grupo, y que “a finales de septiembre probablemente lleguen las vacunas Pfizer para utilizar probablemente en grupos de adolescentes sin comorbilidades”.

Consultada la ministra sobre la cepa que aqueja al mundo y ya se encuentra en el país, precisó: “En este momento tenemos un número bajo de ocupación de camas, pero es inminente la circulación de la cepa Delta”. Y adelantó: “La circulación comunitaria es algo que no se va a poder frenar. Ya tenemos siete casos en la provincia, seis son de acá, de Rosario. Han sido bloqueados, están en aislamiento sus contactos, todos tiene relación con viajes al exterior a Estados Unidos y España. En CABA hay más casos y dos no pudieron generar el nexo, así que es obvio que va a llegar a ser comunitaria, esperamos para fines de agosto”.

Por eso recordó que “hoy está en manos de cada uno de nosotros cuidarnos y testearnos, tenemos centros de testeos en todos lados, acercarse es gratuito, y no tener la duda, ante cualquier síntoma que hoy puede ser covid. Es la manera de frenar esto para evitar las internaciones. El objetivo es vacunación, más los cuidados desde el principio, porque te podés contagiar y contagiar a otros”.

**Terceras dosis y combinaciones inminentes**

La ministra Martorano aseguró sobre el calendario vigente de vacunaciones que “la prioridad en agosto es completar esquemas” y recordó que con Sinopharm es de 21 a 28 días, con AstraZeneca es a 56 días, y precisó que se está “en fecha de muchas de estas vacunas para completar”.

Fue consultada ante el retraso de las segundas dosis de las vacunas Sputnik, sobre cuánto tiempo puede faltar para que se apliquen combinaciones, y precisó: “Creemos que es inminente que en agosto se diga cuál es la mejor combinación. Se están haciendo estudios de combinaciones en provincia de Buenos Aires, en CABA, en Gamaleya, con Astra, con Sinopharm, y también con Moderna”.

Y sobre los estudios que se están haciendo para aplicar terceras dosis, como se avanza en otros países, aseguró “se está hablando de eso o de comenzar un esquema nuevo”.

**Los adolescentes y sus vacunas**

El presidente de la SAP, Omar Tabacco, fue consultado sobre la vacuna en adolescentes sin comorbilidades, y aseguró que “en el mundo está aprobada la Pfizer, y están los estudios en chicos más pequeños todavía. Esperamos sobre todo la seguridad además de la eficacia. Sobre Sputnik cuando los ensayos clínicos estén terminados se sumarán. Probablemente después se siga vacunando a los adolescentes sanos, en solidaridad con sus familiares mayores”.

La directora del Zona Norte, Mónica Jurado por su parte destacó que los adolescentes, desde los 13 años pueden asistir solos a vacunarse, no necesariamente con un cuidador, y no requieren ningún permiso para que sean vacunados.

Y explicó por qué no es en orden descendiente el llamado: “El grupo etario que se vacuna es de condiciones priorizadas por lo que no son en orden de mayores a menores. En la web de la Sociedad Argentina de Pediatría están todas las listas de comorbilidades que se incluyen, para que puedan inscribirlos”.