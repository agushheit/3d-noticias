---
category: La Ciudad
date: 2021-05-05T08:59:22-03:00
thumbnail: https://assets.3dnoticias.com.ar/educacion-emocional.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Concejo Santa Fe
resumen: Más de 700 personas se capacitaron sobre educación socio-emocional
title: Más de 700 personas se capacitaron sobre educación socio-emocional
entradilla: |2-

  Este lunes se concretó el último de los tres encuentros del ciclo Vínculos y Ciudadanía, a cargo de la Lic. Cecilia Marino.

---
Este lunes se concretó el último de los tres encuentros del ciclo Vínculos y Ciudadanía, a cargo de la Lic. Cecilia Marino. “Lo hicimos para brindar estrategias en relación al contexto social que vivimos, y estamos muy conforme con los resultados”, afirmó Leandro González.

Este lunes se llevó adelante el tercer y último encuentro del ciclo de Educación Socio-emocional, a cargo de la Lic. Cecilia Marino. Los encuentros se dividieron en tres ejes: Mundo 365 días de aprendizaje socioemocional; Familia-Escuela y Emociones. Superando los opuestos; y Atrapados en emociones que nos complican. Ansiedad y Miedo.

Al respecto, el presidente del Concejo, Leandro González, resaltó que “este ciclo estuvo dirigido a familias y la comunidad escolar. Estas capacitaciones sobre Educación Socio-emocional fueron pensadas para brindar estrategias y habilidades en relación al contexto que estamos viviendo y cómo el mismo ha afectado o modificado el proceso educativo y la continuidad pedagógica. Debemos trabajar mucho la cuestión de salud mental y emocional tanto en niños como en los adultos para poder afrontar los procesos que estamos viviendo y los que se vienen en el mundo”.

Algunos temas que desarrolló la profesional, Cecilia Marino, se centraron en los cambios a partir de la pandemia, el estrés, las necesidades y emociones de los docentes: flexibilidad y adaptación versus cansancio y agotamiento y algunas prácticas sencillas para el bienestar emocional.

Los encuentros se transmitieron en la plataforma de YouTube, en el canal del Concejo Municipal. El ciclo completo se puede ver en los siguientes links:

1- [https://www.youtube.com/watch?v=VIfATLI8G8M](https://www.youtube.com/watch?v=VIfATLI8G8M "https://www.youtube.com/watch?v=VIfATLI8G8M")

2- [https://www.youtube.com/watch?v=iMjINxe9hMs](https://www.youtube.com/watch?v=iMjINxe9hMs "https://www.youtube.com/watch?v=iMjINxe9hMs")

3- [https://www.youtube.com/watch?v=yH_nyNHTXPY](https://www.youtube.com/watch?v=yH_nyNHTXPY "https://www.youtube.com/watch?v=yH_nyNHTXPY")