---
category: La Ciudad
date: 2021-08-23T06:15:14-03:00
thumbnail: https://assets.3dnoticias.com.ar/BRIGADISTAS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Operativo contra el fuego en la ciudad de Santa Fe
title: Operativo contra el fuego en la ciudad de Santa Fe
entradilla: Comenzó el viernes, pero siguen combatiendo el fuego en la zona. Los brigadistas
  se trasladaron al Puerto local para combatir los focos de incendios

---
El día viernes, a través del Sistema de Alerta Temprana y triangulación de información entre Protección Civil y Prefectura Naval Argentina se detectaron distintos focos de incendios en la región Metropolitana de la ciudad de Santa Fe. Por tal motivo, el sábado a primera hora se puso en marcha un operativo conjunto en el Puerto de Santa Fe, para trabajar en el fuego en zonas de islas al este de la ciudad.

Durante la mañana se realizó un vuelo vigía desde Esperanza a la zona afectada para evaluar la situación. En total trabajaron más de 30 personas de manera articulada: participaron integrantes del Ministerio de Gobierno, Justicia y Derechos Humanos, a través de la Secretaría de Protección Civil, de Prefectura Naval Argentina, del Ministerio de Ambiente y Cambio Climático, integrantes de la Brigada de Atención y Prevención de Emergencias Provincial y Brigadistas del Servicio Nacional de Manejo del Fuego.

Los brigadistas fueron transportados por un helicóptero con helibalde enviado por el Ministerio de Ambiente y Desarrollo Sostenible de la Nación al territorio afectado.

Sumado a eso, un avión hidrante operó de apoyo a los equipos de tierra, con base en el Aeródromo de Esperanza.

El operativo continuó durante el domingo.

**Focos de incendio activos**

Las provincias de San Luis, Jujuy y Santa Fe mantenían este domingo incendios activos en sus territorios, mientras que Catamarca, Misiones, Córdoba, Corrientes y Salta registraban solamente focos contenidos o controlados; según el reporte diario del Servicio Nacional de Manejo del Fuego.

En Santa Fe, el fuego afecta tres diferentes áreas del departamento La Capital; en Jujuy se concentra en la jurisdicción de San Pedro; en tanto que en San Luis el incendio consume desde el pasado miércoles pajonales, espinillos y pastizales en la zona de Pampa del Tamboreo, departamento de Coronel Pringles.

Por su parte, Catamarca presentaba incendios contenidos en tres sectores del departamento San Pedro y en dos áreas del departamento Valle Viejo.

Además, el Servicio informó que fueron controlados los focos que afectan los departamentos puntanos de Junín, Pueyrredón y Pedernera; el cordobés de Santa Rosa de Calamuchita; al jujeño de Palpalá; al misionero de Apóstoles; al correntino de San Martín; y al salteño de Cafayate.