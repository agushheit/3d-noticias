---
category: Agenda Ciudadana
date: 2021-03-05T06:15:48-03:00
thumbnail: https://assets.3dnoticias.com.ar/facebook.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Facebook
resumen: Facebook e Instagram lanzan una herramienta para bloquear la publicidad política
  en esas redes en la Argentina
title: Facebook e Instagram lanzan una herramienta para bloquear la publicidad política
  en esas redes en la Argentina
entradilla: 'Las plataformas tecnológicas presentaron un control para que los usuarios
  puedan bloquear los avisos de temas políticos y electorales. '

---
Facebook e Instagram lanzaron una herramienta para que los usuarios de la Argentina puedan bloquear la publicidad política en esas plataformas tecnológicas. Se trata de un control que permite ver menos anuncios publicitarios de candidatos, partidos y figuras políticas, así como de temas electorales, cuando la Argentina se encamina este año a un proceso electoral que será decisivo para el Gobierno y la oposición de Juntos por el Cambio.

La herramienta se lanzó este jueves en 90 países en simultáneo, entre ellos la Argentina, y estará en Facebook e Instagram de forma paulatina. Se trata de una iniciativa para darle más control a los usuarios de esas redes sociales, sobre todo después de los problemas que tuvo Facebook en los Estados Unidos, donde la publicidad política estuvo suspendida desde la semana previa a las elecciones de noviembre hasta el día de hoy.

“Estamos dando a las personas el poder de decidir qué tipo de anuncios quieren ver en Facebook e Instagram a través de esta herramienta, lo que les permitirá tener mayor control sobre su experiencia en las plataformas", dijo en un comunicado de prensa Marcos Tourinho, líder del equipo de elecciones para América Latina en Facebook.

"La función nace de nuestras conversaciones con usuarios, activistas y organizaciones alrededor del mundo, quienes nos expresaron que las personas quieren tener más control de la visualización de anuncios sobre procesos electorales y saber más sobre quiénes publican dicho contenido”, agregó Tourinho.

En la Argentina todavía no están habilitados estos controles, que son similares a los que ya existen para los avisos publicitarios de temas relacionados con la paternidad, las mascotas y el consumo de alcohol. Cuando los habiliten en el país, en los próximos días, los usuarios tendrán que ir a la solapa de Configuración, donde podrán seleccionar "Temas de Anuncios", entre los que podrán elegir "Ver menos" de "Elecciones y política". 

El primer país donde se lanzaron estos controles fue el año pasado en los Estados Unidos. Pero después directamente se prohibió ahí la publicidad política en Facebook, en la semana previa a las elecciones de noviembre pasado. Recién este jueves se volvieron a habilitar en los Estados Unidos los avisos publicitarios de contenido político y electoral en Facebook, en simultáneo con el lanzamiento global de esta nueva herramienta para otros 90 países, entre los que están la Argentina, Chile, Perú, México, Ecuador y Colombia, a nivel regional en América latina.

Facebook (dueña también de Instagram) considera que la publicidad política es una cuestión muy sensible, que contribuye en una pequeña parte a su negocio general y por eso está implementando políticas de transparencia activa en cuestiones electorales. Desde hace dos años tiene vigente una herramienta de transparencia de la publicidad electoral, "con la información sobre quiénes son los responsables de esos avisos, cuánto pagó por ellos y a qué audiencias los dirigió", dijeron desde la compañía.

Sus principales directivos tuvieron que responder ante el Congreso de los Estados Unidos por el escándalo de Cambridge Analytica, donde se manipularon los datos de usuarios para intentar influir en las elecciones en Estados Unidos, por lo cual la Comisión Federal de Comercio de ese país le impuso una multa de US$5.000 millones, en julio de 2019 -ratificada un año después por la Corte Suprema de Justicia- como sanción por las malas prácticas en el manejo de la seguridad de los datos de los usuarios. A partir de esa situación y otras similares en otros países, la compañía dirigida por Mark Zuckerberg lanzó una serie de iniciativas para mejorar el rol de Facebook en los procesos electorales.