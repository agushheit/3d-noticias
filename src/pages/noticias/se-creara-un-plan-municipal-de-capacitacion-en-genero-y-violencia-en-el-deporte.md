---
category: La Ciudad
date: 2020-11-27T10:17:27Z
thumbnail: https://assets.3dnoticias.com.ar/./genero-deporte.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Género y violencia en el deporte
title: Se creará un Plan Municipal de Capacitación en Género y Violencia en el Deporte
entradilla: Los clubes de la ciudad tendrán instancias de formación sobre las problemáticas
  en materia de género y diversidad sexual para prevenir la violación de derechos
  tanto en el deporte como en la vida cotidiana de los clubes.

---
El Concejo Municipal aprobó en la última sesión la creación del Plan Municipal de Capacitación en Género y Violencia en el Deporte. Dicho plan será un “instrumento para allanar el camino hacia la desnaturalización de los estereotipos de género y a la prevención de violaciones de derechos, ya sea en el deporte o en la vida cotidiana de los clubes” según consta en la ordenanza trabajada en conjunto por los concejales Laura Mondino (FPCyS – Santa Fe Puede Más), Jorgelina Mudallel (PJ) y Guillermo Jerez (Barrio 88).

De la capacitación –que será gratuita– deberá participar todo el personal y autoridades de los clubes, entidades y asociaciones civiles deportivas y/o sociales en las que se practique y se impulse la competencia de deportes en la Ciudad. En cuanto a los contenidos, serán aquellos que reciben todas las personas que se desempeñan en la función pública en el marco de la Ley Micaela (Nº 27.499, a la cual adhirieron tanto la Provincia como la Ciudad de Santa Fe).

Laura Mondino afirmó que “hoy en este Concejo estamos dando un paso importantísimo” y que la “enorgullece, además, que se trata de un proyecto consensuado por distintos espacios políticos, porque demuestra que la construcción del feminismo es transversal, traspasa todos los partidos y construye desde la convicción de que este mundo, así como está, es muy injusto para nosotras”.

“Celebro que tengamos una gestión municipal convencida de que la perspectiva de género debe ser transversal a todas las políticas públicas que se llevan adelante. Y sé que las autoridades de los clubes, las comisiones directivas, quienes toman las decisiones, van a escuchar a esas mujeres que desde hace años vienen reclamando un espacio. Desde ahora, con esta legislación, el Estado las avala” concluyó.

A su turno, Jorgelina Mudallel “La enseñanza en perspectiva de género en los clubes de la ciudad de Santa Fe es realmente muy importante. Queremos que haya Ley Micaela en todos los ámbitos deportivos de la ciudad, tanto como en la provincia y en el país, por lo cual estamos muy conformes con el trabajo con representantes de diferentes bloques para lograr la aprobación de esta ordenanza”.

#### **Protocolos de acción**

En el mismo sentido, se aprobó un proyecto de la concejala Inés Larriera para establecer protocolos de acción frente a hechos de violencia con razón de género, orientación sexual y/o identidad de género, que tengan lugar en clubes, entidades y asociaciones civiles deportivas y/o sociales. Para esto, la Dirección de Mujeres y Disidencias y la Dirección de Deportes podrán realizar convenios de colaboración con la Secretaría de Estado de Igualdad y Género del Gobierno de la Provincia de Santa Fe.

También están incluidas instancias de capacitación y formación en relación con la ruta institucional de atención y canalización de denuncias; promoviendo el acompañamiento y patrocinio de las abogadas de la Dirección de Mujeres y Disidencias Municipal para realizar las denuncias.

Si sufrís o conocés a alguien que sufra violencia de género, podés llamar al **0800-777-5000** o al **144** (disponibles las 24 horas, todos los días del año) o acercarte al **Área municipal de las Mujeres y Disidencias**: 25 de mayo 2884.

También podés radicar tu denuncia en los Centros Territoriales de Denuncias (Zona Centro: Las Heras 2883. Tel: 4815578. Horario: de lunes a viernes, de 8 a 20. Zona Norte: Aristóbulo del Valle 7404. Tel: 4833446. Horario: de lunes a viernes, de 8 a 20). O a la Comisaría de la Mujer (Lisandro de la Torre 2665. Tel: 4619923. Atiende las 24 horas, 7 días a la semana).