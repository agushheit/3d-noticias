---
category: La Ciudad
date: 2022-01-13T06:00:52-03:00
thumbnail: https://assets.3dnoticias.com.ar/POSTASHOT.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Ola de calor: La Municipalidad instala postas sanitarias en balnearios y
  espacios públicos'
title: 'Ola de calor: La Municipalidad instala postas sanitarias en balnearios y espacios
  públicos'
entradilla: "El objetivo es concientizar sobre las consecuencias de la exposición
  al sol y la actividad física en horarios desaconsejados. Se emplazan en el Parque
  del Sur, la Costanera Este y el Parque Garay. \n"

---
A partir de hoy, tres balnearios de la ciudad cuentan con postas sanitarias del municipio para intentar mitigar y concientizar sobre la ola de calor que atraviesa la capital provincial. Esta mañana, se presentó la iniciativa en la posta emplazada en el Parque del Sur, las otras se instalaron en la Costanera Este y el Parque Garay. Cada una de las postas dispone de la atención especializada de una enfermera, personal del Cobem y de la Dirección de Deportes.

Además, cuentan con los medios necesarios para tomar la tensión arterial, la temperatura y oximetría. Asimismo, en casos de observar casos de golpes de calor o deshidratación, se dispone de agua fresca para rehidratar. También hay rociadores para refrescar el cuerpo y bajar la temperatura.

César Pauloni, director de Salud del municipio destacó que, paralelamente, las postas sanitarias servirán como la primera instancia a la que las vecinas y vecinos pueden acudir en caso de sentir síntomas relacionados con el golpe de calor o la deshidratación. Asimismo, la medida obedece a las altas temperaturas que atraviesa la ciudad: “Colocamos estos dispositivos en los tres lugares que conglomeran la mayor cantidad de personas. Tratamos de llevar información a quienes ocupan estos espacios y, quizás, no tienen los cuidados necesarios”, dijo.

**Primer contacto sanitario**

El personal especializado recorrerá los balnearios y solárium mencionados brindando recomendaciones sobre el cuidado pertinente y “en caso de que las personas sientan síntomas compatibles con el golpe de calor o la deshidratación, se puedan acercar a las postas y generar un primer contacto sanitario, hacer un primer control y, de ser necesario, articular con el sistema sanitario para hacer un eventual traslado a un centro de salud”.

Por otra parte, se trata de concientizar a las personas que realizan actividades físicas en horarios no indicados. En ese sentido, las postas estarán abiertas en los horarios donde resulta más peligroso exponerse al sol. “Los horarios pico de calor son entre las 11 y las 17, desde la Municipalidad desaconsejamos realizar actividad física a esa hora y exponerse al sol. Por tal motivo utilizaremos esa franja horaria para intentar colaborar”, explicó Pauloni.

**Plazas húmedas**

Cabe mencionar que, además de las postas sanitarias de los parques Sur, Garay y la Costanera Este, también se realizarán acciones en el norte de la ciudad. “Hay otros espacios donde la Municipalidad hizo una fuerte inversión en espacios verdes, como en Scarafía y Los Troncos, donde hay plazas húmedas, también habrá presencia de la Municipalidad llevando adelante acciones de cuidado para las personas que allí transiten”, concluyó Pauloni.

Vale recordar que estos dispositivos estarán activos todo el tiempo que dure el alerta por altas temperaturas, emitido por el Servicio Meteorológico Nacional.