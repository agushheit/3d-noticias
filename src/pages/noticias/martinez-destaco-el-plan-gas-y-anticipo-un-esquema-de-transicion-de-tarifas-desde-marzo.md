---
category: Agenda Ciudadana
date: 2021-01-04T10:08:30Z
thumbnail: https://assets.3dnoticias.com.ar/040121-Dario-Martinez.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Télam'
resumen: Martínez destacó el Plan Gas y anticipó un esquema de «transición» de tarifas
  desde marzo
title: Martínez destacó el Plan Gas y anticipó un esquema de «transición» de tarifas
  desde marzo
entradilla: El secretario de Energía sostuvo que la Revisión Tarifaria Integral (RTI)
  que realizó el anterior gobierno es «inaplicable», ya que «las llevan a un valor
  impagable» para la mayoría de los argentinos.

---
El secretario de Energía, Darío Martínez, señaló este domingo que el lanzamiento del **Plan Gas** permitirá frenar la caída en la producción a la vez que anticipó que, desde marzo, se pondría en marcha un esquema de «transición» de nuevas tarifas de energía sobre la base de un sistema de segmentación por ingresos.

«El Presidente (Alberto Fernández) ha mandado a los entes reguladores a trabajar en ese proceso de negociación con las empresas de transporte y distribución hasta marzo, donde estaría la nueva tarifa de transición», señaló Martínez en declaraciones a Radio 10.

**<br/>**

**<span style="font-family: Helvetica, sans-serif; font-size: 1.1em; font-weight: 700;"> Subsidios</span>**

En cuanto a los subsidios, señaló que se están buscando «todas las bases de datos que se puedan obtener» de modo de «usar los subsidios de una manera más eficiente» y así aplicar aumentos diferenciados por grupo social y poder adquisitivo.

«Con el nivel de pobreza que tiene la Argentina, en ese sector de la población no hay espacio para incrementos de tarifa o, si lo hay, será simbólico», dijo Martínez, y señaló que a un segundo grupo que integran jubilados y asalariados «no (se) le puede aumentar la tarifa más de lo que le aumenta su ingreso, tiene que ir por debajo de eso».

«Estamos buscando en la base de datos para armar quienes sí puedan hacer frente al costo de las tarifas, que será un porcentual muy bajo de la población», aseveró.

«Con el nivel de pobreza que tiene la Argentina, en ese sector de la población no hay espacio para incrementos de tarifa o, si lo hay, será simbólico».

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.1em; font-weight: 700;">Plan Gas</span>**

En cuanto al Plan Gas resaltó su objetivo de «frenar el declive que tenemos por falta de inversiones, que se traslada directamente a importación de energía», aunque aclaró que «en invierno vamos a tener que seguir importando», ya que la demanda de gas será superior a la capacidad instalada de producción.

Tras la reciente firma con Bolivia de la quinta adenda de importación de gas, mediante la cual se redujo la inyección hacia Argentina de 20 millones a 14 millones de metros cúbicos, el ministro dijo que la diferencia de 6 millones será reemplazada «con el **Plan GasAr** y a través de un nuevo proceso de licitación a los mismos valores para así abrir los picos de invierno».

«El **Plan Gas** pone a los trabajadores en los equipos y a las Pymes sumándose de manera creciente. Hemos establecido dentro del plan de inversiones que las empresas tienen que tener cada vez más inversiones en pesos en desmedro de las hechas en dólares, o sea un valor agregado nacional creciente en la cadena de producción», subrayó.

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.1em; font-weight: 700;">Petróleo</span>**

En relación con la producción petrolera, dijo que estará condicionada a la demanda internacional, aunque consideró a que el país enfrenta un escenario favorable con la recuperación del precio del crudo.

«Se ha recuperado a valores interesantes para las productoras argentinas y tenemos un esquema de retenciones a la baja, con lo cual hay una gran ventana», dijo Martínez.

«Tenemos la expectativa de aumentar la producción de petróleo y para eso estamos trabajando en una ley de incentivo a las inversiones, en un mundo que mira de reojo las inversiones en hidrocarburos por lo que pasó el año pasado», cerró.