---
category: La Ciudad
date: 2021-07-26T06:00:06-03:00
thumbnail: https://assets.3dnoticias.com.ar/W7EHXERL25GOHOREQS5KNTVF6A.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Con la ley de alquileres los propietarios prefieren vender
title: Con la ley de alquileres los propietarios prefieren vender
entradilla: Según el Presidente del Colegio de Corredores Inmobiliarios de Santa Fe,
  la venta aumentó un 30% porque la ley de alquileres genera "inseguridad jurídica.

---
“Esta nueva ley de alquileres trajo más problemas que soluciones”, sentenció Jorge Pighín, presidente del Colegio de Corredores Inmobiliarios de Santa Fe (1ª Circunscripcion) y aseguró que el mercado de venta se incrementó casi un 30% en estos últimos tres o cuatro meses porque los propietarios encuentran más “seguridad jurídica” en la venta que en tener un inquilino.

El referente contó que desde que se terminaron los Decretos de Necesidad y Urgencia que impedían los desalojos y congelaban los contratos, las ventas en el país aumentaron 30%. “Desde el 31 de marzo prefieren vender y no alquilar por la seguridad jurídica. No hablo de rentabilidad porque en este país son muy pocos los que tienen buenas rentabilidades en sus emprendimientos”.

En este sentido, explicó que el inmueble constituye un resguardo del capital pero que “con esta nueva dirección en las políticas del país los propietarios prefieren vender y no tener un inquilino”. “Prefieren vender y después ven si vuelven a comprar. Por eso hay tantos problemas hoy en el mercado locativo para conseguir una vivienda”, opinó.

Con respecto a la venta de lotes, dijo que "es un nicho que se estaba trabajando bien" por "estas promesas de los Procrear", aunque agregó: “No sé cómo cómo saldrán porque con estos costos de vida y con la inflación no sé qué cuota pagarán”. Además, dijo que es imposible que el metro cuadrado de la construcción siga el ritmo del dólar, por lo que “hay que esperar a ver cuál va a ser el valle que tenga el valor del dólar y volver a hacer retasaciones”.

**La grieta inmobiliaria**

El titular del colegio santafesino relató que junto a la Federación de Inmobiliarias de la República Argentina manifestaron siempre el rechazo a la ley de alquileres promulgada el 30 de junio de 2020. “En su momento expusimos los argumentos que hoy están a la vista. Hoy un inquilino tiene que pagar un 45% de aumento cuando nosotros con los contratos antiguos en los que hacíamos un acuerdo entre partes teníamos entre un 15% y un 16% semestral que te daba un 32,5 o un 34,5. O sea que hoy el locatario está perdiendo casi un 10%”.

“Nosotros hablamos de leyes de alquileres, pero nunca se habla del flagelo de la inflación. Si nuestros gobernantes fueran competentes y tuviéramos una inflación de un dígito puedo asegurar que ni ley de alquileres necesitaríamos”, dijo Pighín. “Cada vez que el Estado interviene entre los privados es para mal, nunca para bien. Lo que pasa es que todo esto fue fogoneado por un sector social donde salieron a festejar con bombos y platillos este logro que no fue un logro”.

La posición del sector inmobiliario es clara. Según el profesional, la Ley 23.091, anterior a la modificación del Código Civil del 2015, fue “la más noble que hubo”. Y disparó: "La ley de alquileres actual fue la más equitativa que tuvo el país en los últimos tiempos: perjudicó a las dos partes por igual".

Consultado sobre si existe una “grieta” entre propietarios e inquilino, Pighin opinó: “Hay un sector social que fogonea con que el inquilino es el más débil”, mientras que “el propietario es el que le da la vivienda al inquilino; el propietario es el que invierte en la construcción; el propietario es el que da trabajo”. En esta línea, manifestó que los propietarios deberían recibir incentivos para ocupar los inmuebles y ejemplificó: “Hoy salimos a aplaudir un nuevo blanqueo de gente que se llevó la plata que nunca declaró y porque la trae los beneficiamos con determinadas cuestiones. ¿Y por qué no al dueño? La época de castigar ya pasó”.

Al no tener las clases presenciales durante la pandemia, se registraron muchas rescisiones de contrato sin preaviso por parte de los estudiantes que alquilan en la ciudad. El presidente del Colegio de Corredores Inmobiliarios valoró que muchos propietarios decidieron no cobrar esas indemnizaciones en solidaridad con sus inquilinos. “No digo todos, pero muchos sí, entendiendo la situación”.

Sobre los rumores de incorporar Billetera Santa Fe para operar en el mercado inmobiliario, aclaró que no es competencia del Colegio de Corredores Inmobiliarios firmar un convenio con la Provincia pero que si el gobierno los invita a participar se trasladará la decisión de adherir a cada matriculado.

De todas formas, dijo: “Si nosotros nos ponemos a sacar el número fino, la billetera te hace una devolución de 5.000 pesos mensuales sobre 16.666 pesos. Entonces, ¿qué le conviene más a una familia: tener esos 5.000 pesos para el gasto de góndolas o para pagar esto?”.

“Los inmobiliarios ya hace años que venimos otorgándoles a los locatarios en su primer contrato o renovaciones entre tres y seis cuotas sin intereses para que paguen esos gastos. Es una muy buena actitud que tiene el profesional inmobiliario pensando en el locatario, más cuando son locatarios excepcionales que hemos tenido por 10, 11, 15 años. Es más hay alguno que hasta bonifican el sellado de la renovación”, agregó.

**Hecha la ley, hecha la trampa**

Pighín aseguró que luego de la implementación de la nueva ley se dio una “proliferación de ilegales” que “están ofreciendo y haciéndole firmar cualquier cosa a los inquilinos”, e informó que reciben muchas consultas telefónicas sobre esto.

“Les están haciendo firmar comodatos por dos años, con documentos que las garantías tienen que firmar por el total. Hay una infinidad de cuestiones que realmente son muy preocupantes”, se lamentó, y dio una serie de recomendaciones para gestionar de forma segura el contrato de alquiler, comenzando por chequear en la web que el corredor inmobiliario esté matriculado.

“Tengan cuidado, lean antes de firmar, hagan las consultas. El contrato mínimo son 36 meses; los ajustes anuales con el ICL. Después de transcurridos lo primeros seis meses de contrato el inquilino puede avisar y al tercer mes de ese aviso puede rescindir sin pagar indemnización. El único que puede cobrar honorarios por la intermediación es el corredor inmobiliario, no otra profesión”.