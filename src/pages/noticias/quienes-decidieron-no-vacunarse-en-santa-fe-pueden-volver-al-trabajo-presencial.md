---
category: Agenda Ciudadana
date: 2021-08-07T06:15:49-03:00
thumbnail: https://assets.3dnoticias.com.ar/Noinmunizzados.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Quienes decidieron no vacunarse en Santa Fe, ¿pueden volver al trabajo presencial?
title: Quienes decidieron no vacunarse en Santa Fe, ¿pueden volver al trabajo presencial?
entradilla: Actualmente hay un vacío legal que no da herramientas jurídicas a las
  empresas santafesinas para convocar al trabajo presencial a quienes voluntariamente
  decidieron no inocularse contra el Covid.

---
Con el operativo de inmunización contra el Covid-19 en Santa Fe en una etapa avanzada, hay incertidumbre sobre la cotidianeidad de los santafesinos que por voluntad propia optaron por no vacunarse. El gobernador Perotti ya remarcó que "el que no se vacuna no hará vida social como el resto".

En el ámbito laboral, no hay regulación que indique si las personas no vacunadas podrán trabajar de manera presencial en compañía de la mayoría que se encuentra inmunizada frente al coronavirus. En este contexto, las empresas no pueden convocar a los empleados que decidieron no vacunarse a trabajar de forma presencial si éstos deciden no presentarse a su puesto de trabajo.

Según lo expuesto por el ministro de Trabajo de Santa Fe, Juan Manuel Pusineri, en diálogo con la prensa, "en el ámbito de las empresas, éstas tienen como posibilidad convocar a la gente vacunada y cada empresa decide si la convoca o no", agregando que "por ahora no tenemos planteados conflictos".

Esta habilitación de la que habla el ministro de la cartera laboral provincial se trata de la resolución nacional 4/2021, lanzada conjuntamente por los ministerios de Salud y Trabajo de la Nación. Esta plantea que los empleadores públicos y privados podrán convocar al trabajo presencial a los trabajadores luego de 14 días de la aplicación de la primera dosis.

Alejandro Taborda, quien es presidente de la Unión Industrial de Santa Fe (Uisf),a propósito de esto, manifestó: "Estamos aconsejando a todas las empresas al diálogo constructivo con los sindicatos y operarios, haciendo hincapié en la necesidad de la vacunación. Tenemos buenas referencias de que la mayoría de operarios se ha querido vacunar, muchos con dos dosis".

Según Taborda, los trabajadores santafesinos que decidieron no vacunarse se tratan de "excepciones muy puntuales", agregando que "en el relevamiento que se hizo en el centro norte de la provincia de Santa Fe junto con la Federación Industrial no vemos que sea una generalidad. No creemos que sea un problema grave. Hoy por hoy no hay ningún reglamento legal que exija eso y por ahora no vemos la necesidad de que haya algo al respecto".

Hay diferentes factores que influyen en la opción de convocar a los empleados al trabajo presencial, teniendo en cuenta los problemas que acarrearía al empleador el hecho de que un trabajador se contagie con un cuadro agravado de Covid por el hecho de presentarse a su puesto de trabajo. A esto se le suma que no se puede recaer en una posición de pérdida de empleo de parte del trabajador que se niegue a presentarse en su trabajo, aun estando vacunado.

"Nosotros estamos en contra de la prohibición de despidos porque trae toda una interferencia. Estamos mostrando signos de recuperación industrial y del empleo, y creemos que todas estas regulaciones lo que hacen es perjudicar a quien no tiene trabajo. No es necesaria esta regulación porque las empresas no están despidiendo gente", manifestó Taborda sobre el decreto que prohíbe los despidos en el país.

**"Hay cambios que han venido para quedarse"**

"En las empresas la presencialidad se fue recuperando paso a paso, pero hay cambios que han venido para quedarse", según expreso Taborda. Y agregó: "Muchas de las funciones de las empresas que antes eran por costumbre presenciales se irán transformando en trabajo home office sin ningún problema. Lo mismo pasa con las reuniones virtuales, siendo continuas, mixtas, dejando las reuniones presenciales para cosas muy confidenciales".

El referente del sector empresario enumeró tres impactos próximos: "La presencialidad se va a recuperar, habrá mucho trabajo bimodal y hay trabajos que se están realizando por home office que ya no volverán más a la presencialidad. Eso ha descongestionado oficinas, ha hecho más productivos muchos de los trabajos".