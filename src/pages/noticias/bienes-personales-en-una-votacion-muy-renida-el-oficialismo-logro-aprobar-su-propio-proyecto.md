---
category: Agenda Ciudadana
date: 2021-12-22T06:00:06-03:00
thumbnail: https://assets.3dnoticias.com.ar/BIENESPERSONALES.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: 'Bienes Personales: en una votación muy reñida, el oficialismo logró aprobar
  su propio proyecto'
title: 'Bienes Personales: en una votación muy reñida, el oficialismo logró aprobar
  su propio proyecto'
entradilla: 'El oficialismo juntó 127 votos, apenas uno más de los 126 que reunió
  Juntos por el Cambio. El 29 de diciembre el Senado tratará los cambios.

'

---
En una votación extremadamente reñida, el oficialismo sacó una ventaja mínima logrando la aprobación de su propio proyecto de reforma de Bienes Personales en la Cámara de Diputados y devolvió con cambios la iniciativa al Senado, que tratará la norma la semana que viene.

Luego del enfrentamiento de la semana pasada en el recinto de la Cámara baja, cuando la oposición logró torcerle la mano al Frente de Todos al voltear el proyecto de Presupuesto 2022 del Gobierno nacional, esta vez el oficialismo se alzó con el triunfo con 127 votos afirmativos, apenas uno más que los 126 rechazos que cosechó la iniciativa.

El proyecto oficialista de Bienes Personales incorpora cambios respecto de la media sanción que salió por unanimidad en el Senado hace casi dos meses.

Juntos por el Cambio no aceptó las modificaciones y defendió la media sanción de la Cámara alta, pero no le alcanzó.

Según adelantó el presidente de la comisión de Presupuesto y Hacienda, Carlos Heller, el Senado tiene el compromiso de sesionar el próximo 29 de diciembre para tratar los cambios introducidos en Diputados y convertir en ley la normativa propuesta.

Al comienzo de su exposición como miembro informante del oficialismo, el economista y presidente del Banco Credicoop acusó a Juntos por el Cambio de querer "apropiarse" del proyecto de reforma de Bienes Personales y recordó que en el Senado el proyecto surgió por iniciativa de legisladores del Frente de Todos.

"Venimos a introducir algunos cambios que respetan el sentido de la iniciativa (del Senado) pero que atienden a una cuestión que no tuvo en cuenta y que nosotros responsablemente debemos tener en cuenta que es el tema fiscal", indicó.

Según dijo el diputado kirchnerista, "este proyecto tiene como objeto aliviar la carga tributaria de los sectores de menos recursos y al mismo tiempo generar los ingresos para no acrecentar el déficit fiscal".

"Tenemos que generar que los que más posibilidades tienen efectivamente aporten en mayor proporción y aliviando la situación de los que menos tienen", insistió Heller.

En líneas generales, el proyecto plantea elevar de dos a seis millones de pesos el tope del mínimo no imponible para Bienes Personales, pero además el oficialismo propone llevar la alícuota máxima del 1.25% al 1.5% para los patrimonios declarados por encima de los 100 millones de pesos y hasta los 300 millones.

Entre los 18 millones de los 100 millones, se mantendrá la alícuota del 1.25%.

A propuesta del diputado Hagman, se incorporó en el dictamen una modificación para que aquellos patrimonios superiores a los 300 millones pagarán una alícuota del 1.75%.

"Estamos hablando de un universo más chico, quizás similar al del aporte solidario de las grandes fortunas. Esto va a permitir mejorar la recaudación del Estado, bajar el déficit fiscal", resaltó Hagman.

Otra modificación impulsada por el oficialismo tiene que ver con el el establecimiento de alícuotas diferenciales respecto a los bienes que se encuentren en el exterior, llegando a una tasa del 2.25% para los patrimonios declarados por encima de los 18 millones de pesos.

"Estimamos que percibirán aumento de alícuota algo menos de 16.000 contribuyentes, es decir, el 1.5% del total de los sujetos que presentan declaración de Bienes Personales", afirmó, y destacó que esa medida permitirá beneficiar a medio millón de contribuyentes.

En tanto, la iniciativa actualiza el piso para los inmuebles destinados a casa-habitación, de 18 a 30 millones de pesos.

"Esta decisión beneficiará a alrededor de medio millón de contribuyentes, que dejarán de pagar impuestos o que verán sus pagos significativamente reducidos por estos cambios", destacó Heller.

Están excluidos del pago del tributo las tenencias en títulos y en bonos y los depósitos en moneda argentina y extranjera en bancos.

Un eje saliente del proyecto es la incorporación de un mecanismo de actualización anual y automática según el Índice de Precios al Consumidor Nivel General (IPC) del mínimo no Imponible para el conjunto de los bienes y para los tramos de la escala de alícuotas.

Este instrumento evitará que en adelante haya que aplicar nuevas actualizaciones por decreto o ley.

Al tomar la palabra por la oposición, el diputado Luciano Laspina (PRO) anunció que Juntos por el Cambio insistiría con la media sanción del Senado, sin aceptar los cambios propuestos del oficialismo.

"En principio por una cuestión de urgencia,. No podemos correr el riesgo que por demoras en el trámite parlamentario más de 600.000 argentinos de la clase media pasen a pagar Bienes Personales por las demoras, ineficacias, torpezas de la administración publica, o por la demora en indexar y ajustar el mínimo no imponible de Bienes Personales.

"Tenemos además un compromiso muy claro de no aumentar impuestos, razón por al cual no podemos acompañar el dictamen de mayoría. Esta sesión especial fue pensada para aliviar la carga tributaria para el sector privado. No para inventar nuevas alícuotas y nuevos impuestos como quiere hacer el oficialismo. Es exactamente al revés. Hemos dado quórum para tratar una baja de impuestos, no una nueva suba de impuestos", se quejó el macrista.

Para el santafesino, "esta ley que está tratando el oficialismo de aprobar hoy es una gran noticia para Uruguay y para (el presidente de ese país) Luis Lacalle Pou que se está llevando todos los santos patrimonios de la Argentina que ahora viven en Punta del Este y Montevideo refugiándose de la voracidad de este Gobierno", lamentó.

"La única idea que se les cae es ver como cazan en el zoológico y se le están escapando los animales", exclamó el opositor.

A su turno, el diputado nacional de UCR-Evolución Alejandro Cacace acusó al Frente de Todos de "no dejar pasar una sola oportunidad sin aumentar impuestos".

"Desde la ley de Emergencia para acá han habido veinte oportunidades en que sean creado o aumentado impuestos y esta es una más", cuestionó el puntano.

En la misma sintonía, la diputada de la Coalición Cívica Paula Oliveto Lago aseguró que Juntos por el Cambio "viene a cumplir el contrato electoral de no aumentar impuestos".

El presidente del bloque de la UCR, Mario Negri, aseguró que los oficialistas son los "campeones en subir impuestos".

"Ya nos enseñaron en dos años de gobierno cómo pueden subir impuestos, porque entre los nuevos tributos y las alzas se acumulan 20 subas", reprochó el cordobés.

"En nuestro gobierno de Cambiemos, la alícuota de Bienes Personales bajó todos los años, del 1,25 en 2015 a 0,25% en 2018. Apenas asumió el Frente de Todos le encajaron el 1,25% de nuevo para los bienes radicados en el país", recordó

El presidente del bloque de la Coalición Cívica, Juan Manuel López, calificó como una "buena noticia" que Juntos por el Cambio "cumpla con la promesa electoral" de "bajar impuestos" a "pocos días de haber ganado la elección".

La diputada del Frente de Izquierda Myriam Bregman cargó contra Juntos por el Cambio por su negativa a subir impuestos a los altos patrimonios.

"Siguiendo la lógica del diputado Laspina solo habría que cobrarle impuestos a los pobres. Porque si les cobramos a los ricos, se asustan y se van", chicaneó.

"Macri perdió varios puntos del PBI bajándole impuestos a los ricos. ¿Y? ¿Bajó la pobreza? ¿Bajó la desocupación?", preguntó.

En sentido opuesto, Javier Milei (La Libertad Avanza) consideró que la iniciativa del oficialismo es "inmoral" porque supone un "castigo a 16.000 personas alegando que se beneficia a 500.000".

"¡Dejen de perseguir a la gente! Implica un trato desigual ante la ley", argumentó el diputado liberal.

  
**Juntos por el Cambio no pudo conseguir quórum propio**

Antes de que el presidente de la Cámara de Diputados, Sergio Massa, diera por iniciada formalmente la sesión convocada por Juntos por el Cambio a las 14:30, la principal bancada opositora no había logrado reunir quórum en el recinto, consiguiendo sentar a 125 legisladores en las bancas, cuatro menos de los necesarios para habilitar la reunión.

En esas circunstancias, Massa avisó que el bloque oficialista iba a entrar al recinto para garantizar el quórum y así dar el debate: la estrategia del Frente de Todos era dejar en evidencia que Juntos por el Cambio no tiene capacidad para conseguir quórum por sí mismo, y que requiere necesariamente de la ayuda del oficialismo para tratar temas de su interés, como por ejemplo la reforma del impuesto de Bienes Personales.