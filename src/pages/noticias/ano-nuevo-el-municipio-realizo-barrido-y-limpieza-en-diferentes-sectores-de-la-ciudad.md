---
category: La Ciudad
date: 2021-01-01T14:44:47Z
thumbnail: https://assets.3dnoticias.com.ar/2021-01-01-operativo-limpieza.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: 'Año nuevo: el municipio realizó barrido y limpieza en diferentes sectores
  de la ciudad'
title: 'Año nuevo: el municipio realizó barrido y limpieza en diferentes sectores
  de la ciudad'
entradilla: El operativo especial alcanzó este 1° de enero a grandes avenidas y plazas
  de la capital provincial.

---
La Municipalidad llevó a cabo este viernes 1° de enero, a partir de las 7 horas, un **operativo especial de barrido y limpieza** en diferentes sectores de la ciudad como las costaneras Este y Oeste, Recoleta, peatonal San Martín, bulevar Muttis, entre otras.

Además, **se prestó el servicio en las grandes avenidas** como bulevar Pellegrini y Gálvez, Alem y 27 de Febrero, Aristóbulo del Valle, Freyre, Facundo Zuviría, Galicia, entre otras. También se realizó un operativo especial de limpieza en las plazas Pueyrredón, 25 de Mayo, Constituyentes e Italia (frente a la Legislatura).

La Municipalidad, **a través de la Secretaría de Ambiente**, dispuso de tres camiones y más de 40 personas de las áreas de Ambiente y Paisajismo para atender esta limpieza especial de las calles y plazas.

***

![](https://assets.3dnoticias.com.ar/Foto operativo limpieza.jpg)

***

En cuanto a los residuos secos generados de las reuniones familiares (como botellas, latas, cartón, etc), se recuerda a los vecinos que pueden llevarlos a los **puntos limpios que la Municipalidad dispone por la ciudad**. Los mismos se encuentran en:

* Municipalidad de Santa Fe,
* Estación Belgrano,
* Distrito Noroeste,
* Distrito Este- Prado Español,
* Distrito de la Costa,
* Las Paltas-Colastiné,
* Mercado Norte,
* Costanera Este y Oeste,
* Plaza Rosario-Parque del Sur,
* Parque Garay- IMUSA
* Jardín Botánico- IMUSA.