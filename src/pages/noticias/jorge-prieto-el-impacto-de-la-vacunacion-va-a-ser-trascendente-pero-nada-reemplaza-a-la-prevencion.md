---
category: Agenda Ciudadana
date: 2020-12-03T10:15:47Z
thumbnail: https://assets.3dnoticias.com.ar/prieto.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: 'Jorge Prieto: “El impacto de la vacunación va a ser trascendente, pero nada
  reemplaza a la prevención”'
title: 'Jorge Prieto: “El impacto de la vacunación va a ser trascendente, pero nada
  reemplaza a la prevención”'
entradilla: El secretario de Salud de la provincia aclaró que la aplicación de la
  vacuna contra el Covid 19 será un complemento de “las demás herramientas que tenemos
  que seguir sosteniendo”.

---
El secretario de Salud de la provincia, Jorge Prieto, destacó que “va a ser trascendente el impacto de la campaña de vacunación nacional contra el Covid 19”, aunque advirtió que “complementará a las demás herramientas que tenemos que seguir sosteniendo, porque nada va a reemplazar a la prevención”.

“La cantidad de vacunas que van a llegar serán incorporadas a una logística muy compleja, con la estrategia de vacunar a la mayor cantidad de personas en el menor tiempo posible, antes de que aparezca un próximo brote que tenemos estimado, pero no sabemos para cuándo”, expresó Prieto, este martes, luego de brindar el correspondiente informe epidemiológico de la provincia.

“Para esto se requiere un complemento al programa de vacunación que se viene desarrollando, vacunando primeramente al personal esencial de salud, seguridad y educación, personas mayores de 60 años o aquellas -entre los 18 y 59 años- que tengan factores de riesgo,”, explicó el funcionario.

En ese marco, el secretario de Salud de la provincia, recordó que “el gobierno hizo una convocatoria para un registro de vacunadores voluntarios para Covid, cuya inscripción está abierta desde el 1° de diciembre y hasta el día 15 de ese mes, destinada a mayores de 18 años, que tengan formación en salud o sean estudiantes, y quieran colaborar en esta etapa tan fundamental que hace a la lucha contra la pandemia”.

Asimismo, Prieto explicó que “esta primera etapa de inmunización dejará una brecha para que el virus siga operando, ya que un gran porcentaje de la población sana no va a poder recibir la vacuna. Como consecuencia de ello, pido no abandonar las medidas de prevención como el aislamiento oportuno ante la sospecha, los testeos, las cuarentenas indicadas, y respetar las medidas sanitarias que se vayan estableciendo”, detalló.

Finalmente, el funcionario aseguró que “esta vacunación va a marcar la historia en lo que refiere a cualquier programa de inmunización. Por eso, hemos pedido la colaboración para que se sumen y, entre todos, podamos luchar contra este virus”.