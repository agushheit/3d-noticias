---
category: Agenda Ciudadana
date: 2022-01-20T06:15:58-03:00
thumbnail: https://assets.3dnoticias.com.ar/DIPUTADOS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: El Gobierno tomó distancia del proyecto para hacer obligatoria la vacuna
  contra el Covid
title: El Gobierno tomó distancia del proyecto para hacer obligatoria la vacuna contra
  el Covid
entradilla: '"No representa la postura del oficialismo en este momento", indicaron
  fuentes de la Casa Rosada luego de que se diera a conocer la iniciativa presentada
  por dos diputados del Frente de Todos.

  '

---
El Gobierno debió aclarar hoy que el proyecto de ley presentado por dos diputados del Frente de Todos para incorporar la vacuna contra el coronavirus en el calendario nacional obligatorio no forma parte de su agenda en este momento.

La iniciativa fue presentada por los diputados nacionales Juan Carlos Alderete (Buenos Aires) y Verónica Caliva (Salta) y abrió un debate sobre la conveniencia de establecer la obligatoriedad de la vacuna.

En ese contexto, fuentes de Presidencia aclararon que el proyecto es de "un grupo de diputados y no del bloque" del Frente de Todos y que "no representa la postura del oficialismo en este momento".

Si bien recordaron que cada diputado "puede llevar adelante las iniciativas que crea convenientes", en el Gobierno remarcaron que ese proyecto en particular no es impulsado por el bloque oficialista en su conjunto ni tampoco por el Poder Ejecutivo.

En el proyecto, Alderete y Caliva propusieron que la vacuna contra el Covid-19 sea incorporada en el calendario nacional de vacunación gratuito y obligatorio. Los legisladores apuntan a que sea obligatoria para los mayores de 18 años y menores de entre 3 y 17 años que presenten comorbilidades o enfermedades de riesgo.

"Incorpórase al Calendario Nacional de Vacunación, con carácter gratuito y obligatorio, el esquema completo de vacunación contra el SARS CoV-2 (COVID-19) para todas las personas mayores de dieciocho (18) años que habitan la República Argentina de acuerdo a las prioridades y programas fijados por el Ministerio de Salud de la Nación", señala el primer artículo del texto.