---
category: Agenda Ciudadana
date: 2021-01-08T10:28:00Z
thumbnail: https://assets.3dnoticias.com.ar/08-01-21-parana-no-se-toca.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Ambientalistas aceptan la propuesta de veda parcial en el río Paraná
title: Ambientalistas aceptan la propuesta del gobierno provincial de imponer veda
  parcial en el río Paraná
entradilla: 'La organización ecologista El Paraná No se Toca aceptó la propuesta formulada
  por el Gobierno santafesino de aplicar «una veda parcial» para la pesca comercial
  y deportiva con devolución en el río Paraná. '

---
En el marco del conflicto desatado en la provincia por la decisión de la justicia de suspender la pesca en ese curso:

«Si bien no tenemos la propuesta oficial, en principio la aceptamos. Estamos abiertos al diálogo, pero siempre y cuando haya control, información y participación ciudadana», dijo la asesora legal de _El Paraná No se Toca_, Romina Araguas.

La organización ecologista fue la que presentó en su momento la medida cautelar que determinó la veda total para la pesca comercial y deportiva en el río Paraná.

A través de Fiscalía de Estado, la provincia elevó el miércoles una propuesta de veda parcial para la pesca comercial y habilitación de la pesca deportiva con devolución.

Al respecto, la ministra de Ambiente y Cambio Climático de Santa Fe, Erika Gonnet, dijo que «la propuesta elaborada propone habilitar la pesca comercial durante 3 días por semana, ampliando así los días de prohibición a los ya establecidos por la Ley Nº 12.703».