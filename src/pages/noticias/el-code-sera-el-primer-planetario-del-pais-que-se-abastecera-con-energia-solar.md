---
category: La Ciudad
date: 2021-11-21T06:15:13-03:00
thumbnail: https://assets.3dnoticias.com.ar/CODE.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: El Code será el primer planetario del país que se abastecerá con energía
  solar
title: El Code será el primer planetario del país que se abastecerá con energía solar
entradilla: El observatorio espacial no solamente quedará modernizado, sino que se
  convertirá en un espacio sustentable. Este viernes se firmaron los planos para que
  el lugar incorpore paneles solares.

---
Las obras para la renovación del Centro de Observadores del Espacio (Code) continúan su avance y muestran poco a poco las tareas concretadas. Pero más allá del fin de modernizar el planetario santafesino, una de las apuestas es convertirlo en el primer observatorio espacial del país que cuente con autonomía energética, a través de la colocación de paneles solares.

 "El viernes firmé los nuevos planos con las modificaciones y ampliaciones. En la terraza del auditorio vamos a tener paneles solares, para que el edificio con estos paneles tenga una autoalimentación de energía eléctrica, es algo que no estaba previsto", resaltó Jorge Coghlan, director del Code, en diálogo con El Litoral, y agregó: "Estamos muy contentos de poder tener un espacio moderno y que ayude al medio ambiente".

 Según indicó Coghlan estas tareas que incluye la colocación de paneles no estaban contemplados dentro de los plazos y el proyecto original, "esto hará que la obra tenga una prolongación mayor a la prevista, pero será para mejor", sostuvo. Al mismo tiempo comentó que las tareas, que están a cargo de la empresa Cocivial, estarían completas entre abril y mayo del 2022.

 **A buen ritmo**

 La obra de renovación del espacio ubicado en Av. Almirante Brown 4998 (Costanera Oeste) marcha a buen ritmo, luego del reinicio de los trabajos que se dieron a partir del 1° de septiembre, bajo un presupuesto oficial de $ 78.929.650,18.

 "Las obras tienen un avance del 30 %", destacó el director del Code y valoró que el trabajo siempre fue permanente. También mencionó que respecto a lo que se había hecho anterior a la nueva licitación tuvo que hacerse de nuevo: "Estaba mal hecho o en malas condiciones por el paso del tiempo, por eso se tuvieron que demoler parte de las terrazas que estaban inconclusas".

 Uno de los trabajos que se concretarán durante los próximos días es el reemplazo de la terraza del auditorio, ya que es muy antigua. "La nueva terraza va a quedar al mismo nivel de la terraza grande que va a tener el domo del planetario. Tenemos pensado que se agregue una escalera tipo caracol que será para subir desde el hall de entrada y habrá una escalera interior para bajar", señaló Coghlan. A su vez destacó que se colocará un elevador mecánico que servirá para carga y personas, "está pensado para personas mayores y minusválidos, porque llegar a la terraza por escaleras siempre fue un problema. De esta manera será un Code más inclusivo".

  **Detalles**

 El planetario digital contará con una cúpula geodésica de 9 metros de diámetro en la que podrán ubicarse 50 butacas reclinables. El proyector emitirá imágenes especialmente hechas para la representación del cielo (sin distorsión del horizonte al cenit) a fin de que el espectador esté en el centro, entre los planetas y los asteroides, y que con las vibraciones del sonido se sienta dentro de la escena.

 "Las proyecciones en el domo del planetario que tiene la simulación del cielo, se hace a través de un proyector de última generación. Se simularán viajes al espacio, de exploración planetaria, es muy amplio lo que podrá observarse", adelantó el director del Code.

 Además, con las obras se planea hacer una intervención de las instalaciones para contar con nuevos recorridos, generar nuevas áreas de proyección en la planta baja, alrededor del planetario digital, y habilitar el patio central como articulador de actividades. El perímetro se transformará con una piel de vidrio, con permeabilidad visual, que invite al uso de los espacios. Y la terraza será mejorada para hacer observaciones de cielo.