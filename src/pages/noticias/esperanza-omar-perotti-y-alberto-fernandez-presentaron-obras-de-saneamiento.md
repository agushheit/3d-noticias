---
category: Estado Real
date: 2021-01-05T10:41:02Z
thumbnail: https://assets.3dnoticias.com.ar/050121-perotti-obras.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: 'Esperanza: Omar Perotti y Alberto Fernández presentaron obras de saneamiento'
title: 'Esperanza: Omar Perotti y Alberto Fernández presentaron obras de saneamiento'
entradilla: En esa ciudad se construye una nueva estación de bombeo y la renovación
  de la cloaca máxima, tareas que demandan una inversión de 243 millones de pesos.

---
El gobernador de Santa Fe, Omar Perotti, participó este lunes de una videoconferencia con el Presidente de la Nación, Alberto Fernández, quien presentó **30 obras trascendentes en ejecución en 16 provincias de todo el país con una inversión total de 13.552 millones de pesos**.

De ese monto, **en la ciudad de Esperanza se invertirán 243 millones de pesos en las obras** de estación de bombeo y renovación de cloaca máxima por un monto de 182.172.452 pesos, y cañería de impulsión cloacal oeste por 60.892.857 pesos.

Perotti participó de la videoconferencia desde la intersección de las calles Rivadavia y Humboldt de esa ciudad del departamento Las Colonias, que es donde se ejecutan los trabajos.

En la oportunidad, el gobernador expresó que «aquí se está poniendo al día la ciudad, no solamente con estas obras, más de 240 millones, sino que se está dando la previsibilidad, lo que hasta aquí no tuvo y la posibilidad de que sus barrios crezcan con la posibilidad real y concreta de cloacas y agua».

«Eso es saneamiento y nos llevó a definir esta ciudad, pero podría haber sido otra porque el convenio que tenemos firmado con Nación se viene ejecutando en su totalidad. Son 1.400 millones de pesos para ir licitando obras durante todo el año 2020 y esto se suma a las posibilidades reales de mano de obra», añadió el titular de la Casa Gris.

«En Reconquista, Rafaela, Venado Tuerto, Firmat, Santa Fe, Rosario, Funes, Granadero Baigorria y Villa Gobernador Gálvez estamos desplegando los procesos para obras en marchas que va a ser la característica de Nación, provincia y los municipios trabajando juntos», aseguró Perotti.

«Esto significa presidente que trabajando juntos se pone de pie la provincia, trabajando juntos se pone de pie a la Nación. Hoy muchos vecinos de la ciudad de Esperanza ven como esta obra está en marcha y, con ello, la expectativa de más agua y más cloacas en toda la provincia de Santa Fe. Gracias por hacerlo posible», finalizó el gobernador.

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.1em; font-weight: 700">INTEGRAR A LA ARGENTINA</span>**

En el final del acto, el Presidente Alberto Fernández aseguró estar «muy feliz de que comencemos el año anunciando 30 obras que se desarrollan en todo el país, porque el mayor objetivo que tuve cuando encaré la campaña es que la Argentina se integre como país, y la forma de integrarla es esta, permitiendo que los canales comunicantes entre los argentinos sean los mejores, y que los argentinos vivan mejor allí en el lugar donde viven, por eso son tan importantes las obras que tienen que ver con el agua o cloacas, como lo que vemos hoy en la ciudad de Esperanza».

«Me alegra también que empecemos el año con esto –prosiguió el mandatario nacional–, porque quiero que sea el año en el que la Argentina se ponga de pie después del durísimo 2020 que hemos vivido, y para hacerlo necesitamos de un Estado que impulse obras de esta naturaleza. Lo previmos desde el primer momento, por eso vemos que en el Presupuesto nacional la inversión en obra pública se multiplicó por dos: siempre creímos que la obra pública y la construcción de viviendas iban a ser el gran motor de la economía argentina», expresó Fernández.

Mientras, el ministro de Obras Públicas de la Nación, Gabriel Katopodis, destacó que «el sistema cloacal de Esperanza va a permitir la posibilidad de contar con ese servicio básico y esencial muy deseado por los vecinos de esa localidad, un servicio tan necesitado en la Argentina». En ese sentido, dijo que «**siete millones de argentinos no tienen agua y veinte millones no tienen cloacas**. No hay dudas de que tenemos que arrancar el primer día del año y continuar todo lo que venimos haciendo en el 2019 para achicar esa brecha que tiene el país».

De la videoconferencia participaron también: la intendenta de Esperanza, Ana Meiners, el administrador general de la Dirección Nacional de Vialidad, Gustavo Arrieta, y los gobernadores de La Pampa, Sergio Ziliotto, y La Rioja, Ricardo Quintela, entre otros.

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.1em; font-weight: 700">OBRAS EN ESPERANZA</span>**

Los objetivos de las obras que impulsa la Nación en conjunto con la Provincia, buscan mejorar el funcionamiento del sistema cloacal y lograr la futura incorporación a las redes cloacales de 3.500 lotes en toda la ciudad, con una obra dimensionada para ampliar el radio de servicio con un horizonte de 30 años.

En la ciudad de Esperanza se lleva adelante actualmente la construcción de la nueva estación de bombeo cloacal «Bosch» que permitirá recibir los líquidos del colector de la cuenca adyacente y futuros loteos.

Además, se realiza la renovación del colector máximo para evitar vertidos de líquidos cloacales al canal pluvial. Para ello se ejecutará la cañería de impulsión y colector cloacal máximo hasta el nuevo ingreso a la Planta Depuradora de Líquidos Cloacales.

Asimismo, en la planta depuradora se ejecuta la estación elevadora que impulsará los líquidos cloacales a un nuevo desarenador con dos canales de funcionamiento; una cámara de descarga de camiones atmosféricos; el ingreso para cada laguna anaeróbica y salas de tablero eléctricos, oficinas y laboratorio.

También se está realizando el tendido de 800 metros de línea aérea de media tensión trifásica para alimentar las nuevas instalaciones.

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.1em; font-weight: 700">OBRAS A EJECUTARSE</span>**

La obra de impulsión y ejecución de colectores cloacales en la zona oeste de la ciudad de Esperanza permitirá el completamiento del actual sistema de desagües cloacales. Consta del tendido de 2.064 metros de cañerías de impulsión de 250 mm. de diámetro; tendido de 1.554 metros de caño colector de 400 mm. de diámetro.

**Los trabajos beneficiarán a 2.300 vecinos**, contemplan una inversión de 60.892.857 pesos y su plazo de ejecución es de 6 meses.

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.1em; font-weight: 700">CONVENIO FIRMADO CON NACIÓN</span>**

Cabe recordar que el gobierno nacional y la provincia de Santa Fe firmaron en abril pasado un convenio de ejecución de obras por 1.450 millones de pesos de inversión de la Nación en una primera etapa.

Se encuentran iniciadas cinco obras de agua potable y desagües cloacales, y nueve obras que se encuentran próximas a iniciarse o en proceso de licitación/adjudicación. Se verán beneficiados directamente 100.000 vecinos en ocho ciudades.

**Obras financiadas a través del Ente Nacional de Obras Hídricas de Saneamiento (ENOHSA)**

**- Reconquista:** se prevé tender las redes cloacales en siete barrios en beneficio de 6.000 vecinos, lo que representa un crecimiento del 10 % en la cobertura del servicio para la ciudad. 130 millones de pesos de inversión. Estas obras ya fueron iniciadas.

**- Esperanza:** se mejorará el funcionamiento del sistema cloacal y será posible la futura incorporación a las redes de 3.500 lotes en toda la ciudad. Las obras están dimensionadas para ampliar el radio de servicio cloacal con un horizonte de 30 años. 243 millones de pesos de inversión. Una obra iniciada y otra en proceso devaluación de ofertas.

**- Rafaela:** se incorporarán a las redes cloacales 10.000 nuevos usuarios y comenzó la construcción de un nuevo Centro de Atención al usuario en un predio propio de la empresa. Inversión de 125 millones de pesos. Obras iniciadas.

**- Rosario:** Tendido de desagües cloacales en el barrio Belgrano Sur, con conexiones que beneficiarán a más de 7.000 vecinos. Obra adjudicada con una inversión de 124 millones de pesos.

**- Casilda:** Está programada la construcción de una estación de rebombeo para mejorar las presiones del servicio en la zona sureste y el tendido de una cañería de refuerzo que hará posibles futuras expansiones de redes en barrio Nueva Roma. Inversión de 72 millones de pesos para obras que se encuentran en proceso de licitación y adjudicación de ofertas.

**- Firmat:** Colector primario por calle 1° de Mayo para dotar de cloacas a barrio Patria, con una inversión de 110 millones de pesos, en período de evaluación de ofertas.

**- Funes:** Se desarrollarán las obras para que puedan contar con desagües los vecinos del barrio Centro Norte, realizándose una inversión de 83 millones de pesos por medio de una licitación que se encuentra en período de evaluación de ofertas.

**- Santo Tomé:** El aporte del Gobierno Nacional permitirá vincular a la red de agua potable, operada por la Municipalidad, a los barrios Loyola y General Paz. En este caso la inversión es de 4 millones de pesos y se encuentra en proceso de adjudicación.

**- Santa Fe:** Se ampliará la cobertura del servicio cloacal en barrio Ciudadela, y se contará con infraestructura primaria para la futura incorporación de 43.000 nuevos usuarios a dicho servicio.

La inversión nacional asciende a 203 millones de pesos, encontrándose en período de evaluación de ofertas presentadas.