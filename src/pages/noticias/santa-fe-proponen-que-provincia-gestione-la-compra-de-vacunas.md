---
category: Agenda Ciudadana
date: 2021-05-24T06:15:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/Corral.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'Santa Fe: proponen que Provincia gestione la compra de vacunas'
title: 'Santa Fe: proponen que Provincia gestione la compra de vacunas'
entradilla: José Corral, alentó que el Gobierno provincial ponga en marcha las tratativas
  para comprar vacunas contra el Covid.

---
“Proponemos que el Gobierno de Santa Fe gestione la compra de vacunas”, aseguró José Corral -dirigente radical y referente de Juntos por el Cambio desde sus inicios-, y puso como ejemplo el caso de provincias que tomaron la iniciativa con resultados favorable: “Jujuy ya obtuvo el compromiso de un millón de dosis de una de las vacunas chinas”. Además, el excandidato a gobernador recordó que, en nuestra provincia, el Senado -por una iniciativa de los senadores radicales-, ya respaldó la posible gestión del Gobierno provincial para conseguir vacunas.

José Corral mencionó que actualmente “en el mundo hay siete vacunas disponibles contra el Covid, y la posibilidad de salir de las restricciones más severas -como las que estamos atravesando en estos días-, que provocan serios daños a la economía, pero también a la salud mental de las personas y a la educación, es conseguir más vacunas”.

Para el exintendente de la capital provincial “las restricciones actuales son en gran medida el fracaso de las políticas de salud llevadas adelante durante este año y algunos meses de pandemia, y también el fracaso de la gestión de las vacunas: recién estarían llegando en estos días al país las vacunas Astrazeneca, que originalmente fue la apuesta más importante del Gobierno nacional, que en su momento no abrió otras líneas de gestión para diversificar el riesgo que supone la producción y aplicación de vacunas en un tiempo tan corto”. En ese sentido, apuntó que “otros países de la región, como el caso de Chile, han completado prácticamente la vacunación de la población, y eso les permite evitar restricciones tan costosas”.

Finalmente, José Corral recordó que “el producto bruto interno de la Argentina cayó en 2020 el 10 %, y eso representa el triple de lo que cayó en promedio en el mundo, que oscila el 3,5 %, sin hablar de que hemos batido el récord de semanas sin clases presenciales. Y a todo eso hay que sumar los otros efectos que tiene en la salud vista desde una mirada integral, especialmente en la salud mental, sobre todo de niños, niñas y adolescentes”, concluyó.