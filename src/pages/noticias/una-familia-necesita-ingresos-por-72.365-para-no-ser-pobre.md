---
category: Agenda Ciudadana
date: 2021-11-19T06:00:31-03:00
thumbnail: https://assets.3dnoticias.com.ar/FLIA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: Una familia necesita ingresos por $72.365 para no ser pobre
title: Una familia necesita ingresos por $72.365 para no ser pobre
entradilla: 'Según datos del INDEC, para no ser indigente hacen falta $30.925.

'

---
Una familia de cuatro miembros necesitó en octubre un ingreso mensual de $72.365 para no ser considerada pobre, según el costo de la Canasta Básica Total (CBT), que subió en ese mes 2,6%, informó el INDEC.

El mismo grupo familiar necesitó $30.925 de acuerdo con el costo de la Canasta Básica Alimentaria (CBA), para no ser considerado indigente, que en la misma comparación aumentó un 2,7%.

Tanto la suba mensual de la CBT, que define el nivel de pobreza, y la de la CBA, que marca el nivel de indigencia, aumentaron en octubre por debajo de la inflación del mes del 3,5%.

En la comparación interanual, la canasta básica total registra un aumento del 45,0%, mientras que la básica alimentaria crece desde octubre del año anterior, un 49,3%.

Ambas canasta aumentaron su costo por debajo del alza de los precios minoristas en los últimos doce meses, que se ubicó en el 52,1%, según el Índice de precios al Consumidor (IPC).

Desde diciembre pasado, el aumento de la CBT es del 45%, y supera por cuatro puntos porcentuales el alza de precios minoristas en el mismo período del 41,8%.

En cambio, la canasta alimentaria aumentó un 33,5% en los primeros diez meses del año, y se encuentra siete puntos porcentuales debajo de la evolución de la inflación en ese período.

Según las cifras oficiales, una familia de cinco integrantes requirió un ingreso de $76.112 para no encontrarse en situación de pobreza.

Para un grupo familiar de tres miembros, los ingresos debieron llegar en octubre a $57.611, para eludir la pobreza.

Las familias de cinco integrantes, para no estar en situación de indigencia necesitaron un ingreso mensual de $32.527 y en el caso de una grupo de tres integrantes debieron contar con $  
24\.260.

En el caso de un adulto mayor, el valor de los ingresos mensuales en octubre para no ser pobre debió ser de $23.419 y para no caer en la indigencia de 10.008.