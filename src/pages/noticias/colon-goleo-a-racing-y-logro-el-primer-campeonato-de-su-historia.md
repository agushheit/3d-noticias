---
category: Deportes
date: 2021-06-05T06:15:11-03:00
thumbnail: https://assets.3dnoticias.com.ar/colon-campeonjpg.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Colón goleó a Racing y logró el primer campeonato de su historia
title: Colón goleó a Racing y logró el primer campeonato de su historia
entradilla: 'El "Sabalero " derrotó 3 a 0 a la "Academia", en la definición de la
  Copa de la Liga Profesional de Fútbol 2021. '

---
Colón de Santa Fe se consagró campeón de la Copa de la Liga Profesional al derrotar a Racing en San Juan.

Fue por 3 a 0 en el estadio del Bicentenario. El primer gol del partido lo convirtió Rodrigo Aliendro a los 12 minutos del segundo tiempo. Luego, a los 26', Christian Bernardi amplió la ventaja. En el final lo liquidó Alexis Castro.

De esta forma, el equipo de Eduardo Domínguez hace historia y conquista el primer título de su historia.

Con el triunfo, además, el "Sabalero" se aseguró la clasificación a la Copa Libertadores de América de 2022.

En diciembre, Colón tendrá la chance de sumar otra estrella cuando enfrente a Boca Juniors, ganador de la pasada Copa Diego Maradona, en el marco del "Trofeo de Campeones".

**Los goles**

**12´ 1er gol de Colón:** Aliendro en una jugada colectiva abre el marcador para los santafesinos, tras un centro por la derecha que de zurda por el 1-0.

**26´ 2do gol de Colón:** Gran definición de Bernardi por sobre Gómez tras una jugada colectiva para poner el segundo para los santafesinos.

**41´ 3er gol de Colón**: Alexis Castro de contra y frente al arco anotó el tercero de los "Sabaleros"