---
category: La Ciudad
date: 2021-07-08T08:59:39-03:00
thumbnail: https://assets.3dnoticias.com.ar/epe.png
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: "“La ciudad necesita un plan de obras de electricidad”"
title: "“La ciudad necesita un plan de obras de electricidad”"
entradilla: Leandro González y Lucas Simoniello solicitaron a la EPE la elaboración
  de un “Plan de Potencia” en el que consten obras de infraestructura eléctrica planificadas
  en el corto y mediano plazo.

---
La elaboración de un “Plan de Potencia”, en el que se defina un programa de trabajo para la ejecución de obras de infraestructura eléctrica en el corto y mediano plazo, es el principal objetivo de uno de los proyectos presentados por el presidente del Concejo Leandro González y el concejal Lucas Simoniello. En el proyecto solicitan que, de manera conjunta, la Empresa Provincial de la Energía y el Ejecutivo Municipal definan las obras a realizarse -tanto de mejora de las instalaciones existentes como la ampliación de las mismas- definiendo esquemas de participación pública y privada.

Al respecto, señalaron que “contar con un Plan de obras de Infraestructura es una deuda pendiente. Así como tenemos un Plan de Saneamiento, en el que se indica, por ejemplo, la expansión de la red de desagües cloacales y la red para el abastecimiento de agua potable, debemos tener información disponible sobre las obras de infraestructuras proyectadas y los planes y plazos de ejecución en materia de electricidad y gas natural, que en la actualidad es escasa”.

“Debemos mejorar la planificación y la información pública para así generar más previsión y reglas claras” aseguró Simoniello y agregó que “esto imposibilita definir etapas de ejecución, gestionar recursos para el financiamiento de la infraestructura, generar compromisos de inversión, anticiparse a posibles impactos ambientales e incorporar las energías renovables y además condiciona la rentabilidad de muchísimos proyectos de desarrollo urbano en la ciudad, al no estar preestablecida la factibilidad en el suministro del servicio”.

Por último, Simoniello señaló que “la calidad de vida de los vecinos y vecinas se define, en parte, por la disponibilidad y acceso a los servicios básicos. Por eso la planificación urbana y las distintas políticas que se despliegan en el territorio, como el nuevo Código de Habitabilidad que estamos discutiendo en el Concejo, tienen que estar respaldadas con este tipo de información actualizada que estamos solicitando”.

**Sobre el plan**

Según consta en el proyecto, para diseñar el plan se pide sistematizar y mapear de forma periódica información como: el mapa de densidad de potencia actual; las estaciones transformadoras, subestaciones transformadoras aéreas y cámaras, centros de distribución; número de usuarios y consumos promedios por áreas, solicitudes de suministro y proyecciones de los análisis de factibilidad del mismo; obras de infraestructura proyectadas para los próximos diez años con descripción de sus escalas y etapas de ejecución; espacios públicos factibles de emplazar infraestructura eléctrica.