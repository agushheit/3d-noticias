---
category: Deportes
date: 2021-04-18T06:00:10-03:00
thumbnail: https://assets.3dnoticias.com.ar/UNION.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: UN UNIÓN QUE NO FUE UNIÓN
title: UN UNIÓN QUE NO FUE UNIÓN
entradilla: El Tate cayo ante talleres de Córdoba 3 a 0 en el estadio Mario Kempes,
  en un partido clave en sus aspiraciones de consolidarse en fase de clasificación
  en la Zona B de la Copa de la Liga.

---
En medio de ese panorama, la primera parte del cotejo ofreció grandes emociones, con un Talleres dominando la pelota y contando con los mejores argumentos para abrir el marcador. Cuando transcurrían jugados 13 minutos de ese período inicial, Franco Fragapane estableció el 1-0. Con ese marcador llegó el descanso, aunque pudo ser más abultado.

Posteriormente, ya en la etapa complementaria, Unión intentó adelantar líneas y aumentar tareas ofensivas con la misión de revertir la historia adversa. Sin embargo, lo cierto es que los anfitriones siguieron mostrándose superiores y pudieron ampliar diferencias. Fue a los 29 minutos, por intermedio de una intervención de Nahuel Tenaglia.

Luego, en la recta final del cotejo, Unión fue desesperadamente a buscar por lo menos un descuento. Así, claro está, le otorgó más espacios a Talleres para poder golear. Y eso pasó: en el tiempo de descuento, Guilherme Parede puso el 3-0 con el que el local se trepó al tercer escalón del Grupo B, metiéndose de lleno en la zona de clasificación.

En la próxima fecha Unión se enfrentara ante Independiente el domingo 25-04

¿Este será un ultimátum para Azconzábal?

**Talleres (3):** 1-Marcos Díaz; 14-Nahuel Tenaglia, 2-Rafael Pérez, 4-Piero Hincapié y 21-Angelo Martino; 8-Juan Ignacio Méndez, 5-Federico Navarro y 20-Franco Fragapane; 7-Diego Valoyes, 28-Carlos Auzqui y 77-Guilherme Parede. DT: Alexander Medina.

**Unión (0):** 1-Sebastián Moyano; 16-Federico Vera, 17-Brian Blasi, 6-Matías Nani y 28-Juan Carlos Portillo; 26-Ezequiel Cañete, 5-Nelson Acevedo y 18-Nicolás Peñailillo; 34-Kevin Zenón, 11-Juan Manuel García y 15-Gastón González. DT: Juan Manuel Azconzábal.