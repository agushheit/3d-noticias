---
category: Estado Real
date: 2021-04-25T06:00:37-03:00
thumbnail: https://assets.3dnoticias.com.ar/QUOM.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Provincia realizó un encuentro con mujeres de las comunidades Qom en la
  ciudad capital
title: La Provincia realizó un encuentro con mujeres de las comunidades Qom en la
  ciudad capital
entradilla: Con el objetivo de reflexionar y visibilizar sus costumbres e identidad
  cultural, y recuperar el valor de sus prácticas alimentarias.

---
En el marco de la Semana de los Pueblos Originarios, los ministerios de Desarrollo Social y Cultura, llevaron adelante un encuentro con mujeres de las Comunidades Qom de la ciudad de Santa Fe. El objetivo fue reflexionar y visibilizar sus costumbres e identidad cultural, y recuperar el valor de sus prácticas alimentarias.

En ese sentido, el ministro de Desarrollo Social, Danilo Capitani indicó: “La Semana de los Pueblos Indígenas es una ocasión para reivindicar la cultura y los derechos de los pueblos, nutriendo el patrimonio cultural de la provincia y para reflexionar sobre su condición de pueblos preexistes en distintas localidades del territorio santafesino".

Por su parte, el secretario de Integración Social e Inclusión Socioproductiva, Gustavo Chara expresó:” entendemos que es fundamental estos espacios de intercambio y aprendizaje desde la diversidad cultural para seguir construyendo una sociedad más justa plural e igualitaria”

“Consideramos el valor intrínseco de las recetas representativas de cada pueblo originario que habita la provincia de Santa Fe, por tal motivo es que estamos aunando esfuerzos para recuperar y preservar las mismas y así fortalecer el respeto de la cultura alimentaria de cada pueblo”, finalizó.

**Volver a nuestras raíces**

De manera interministerial, se trabaja en un proyecto que promueve el respeto por el derecho a la diversidad cultural de los Pueblos Originarios, recuperando el valor de sus prácticas alimentarias.

Con el objetivo de recuperar la cultura alimentaria, culinaria y la medicina tradicional de los pueblos, se comienza a realizar un registro de las preparaciones culinarias y medicinales, su valor cultural y afectivo para luego ser plasmado a través de distintos recursos gráficos, para su difusión de generación en generación.

**Semana de los pueblos indígenas**

Del 19 al 25 de abril se conmemora la Semana de los Pueblos Indígenas con el objetivo de reivindicar la cultura y sus derechos, recordando el primer Congreso Indigenista Interamericano celebrado el 19 de abril de 1940 en la ciudad de Patzcuaro, en México.

Con el objetivo de salvaguardar y perpetuar las culturas originarias de todo el continente, este primer congreso abordó la situación social y económica de los pueblos originarios. Además, creó el Instituto Indigenista Interamericano, dependiente de la Organización de los Estados Americanos.

**Presentes**

También participaron, el subsecretario de Pueblos Originarios, Personas Mayores y Discapacidad, Hugo Burgues, la directora de Pueblos Originarios, Rosana Esquivel, integrantes de la dirección provincial de Programa Alimentario También participaron, el subsecretario de Pueblos Originarios, Personas Mayores y Discapacidad, Hugo Burgues, la directora de Pueblos Originarios, Rosana Esquivel, integrantes de la dirección provincial de Programa Alimentario.