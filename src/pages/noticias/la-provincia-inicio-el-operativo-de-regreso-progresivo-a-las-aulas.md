---
category: Agenda Ciudadana
date: 2020-12-02T10:39:35Z
thumbnail: https://assets.3dnoticias.com.ar/ESCUELAS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: La provincia inició el operativo de regreso progresivo a las aulas
title: La provincia inició el operativo de regreso progresivo a las aulas
entradilla: Desde este martes 1 de diciembre, los alumnos y alumnas de los establecimientos
  provinciales volvieron a encontrarse con sus docentes y compañeros.

---
El gobierno provincial inició acciones tendientes a la vuelta de las clases presenciales en las escuelas santafesinas, de acuerdo al decreto Nº 1446 que instruye al Ministerio de Educación a planificar encuentros que propicien el regreso a la presencialidad, mediante la organización de grupos de estudiantes, durante la primera quincena del mes de diciembre.

En ese marco, la directora de la Escuela primaria Nº 880 “Domingo Guzmán Silva” (ubicada en Regimiento 12 de Infantería 2049, en la ciudad capital), Claudia Grillo, brindó precisiones sobre operativo de regreso progresivo a las aulas.

“Nosotros tenemos el edificio dividido en dos sectores: el central y el sector este. Los alumnos vienen en grupos de ocho y saben que tienen un lugar de ingreso. Nosotros nos comunicamos con los padres y madres y tenemos muy aceitado este mecanismo porque venimos entregando bolsones desde el pasado mes de abril, por lo cual, este edificio nunca permaneció cerrado”, explicó la directora.

“Luego de ingresar en grupos de ocho chicos y chicas, los recibe el personal. Hay dos docentes que esperan a los niños y les toman la temperatura corporal para, luego, hacerlos formar. Seguidamente, los chicos y chicas se higienizan las manos con jabón y se dirigen a un aula donde realizan una dinámica de trabajo que, por ahora, no tienen que ver con los cuadernillos. Los chicos van a tener la posibilidad de asistir una hora a la escuela porque son 500 alumnos”, aclaró Grillo.

Asimismo, la directora agregó que “los alumnos más pequeños, de 1° grado, vienen en grupos de cuatro, cada uno con su papá o mamá, y la idea es que solo sean ocho ya que no tuvieron contacto con sus señoritas a lo largo de este tiempo”.

## **Nueva modalidad de convivencia dentro de la escuela**

Más adelante, Grillo explicó que “el aula que se utiliza y el lavamanos no se ocupan después, es decir, que el siguiente grupo que ingresa al establecimiento ocupa otra aula y usa otro lavamanos. A lo sumo pude haber dos grupos de manera simultánea en la escuela, o sea, un total de 16 chicos, uno en cada ala del establecimiento. Los alumnos que van a ocupar un aula del ala central, entran por el SUM; y quienes ocupan un aula del ala este o del taller de educación manual, ingresan por el taller. En ningún momento estos grupos van a tener contacto. Dentro del aula van a estar los docentes y, una vez que esa aula se desocupa, se limpia y se cierra para poder ser utilizada al día siguiente”, concluyó.

## **Rosario, un regreso con alegría**

Por otro lado, la directora de la Escuela N.º 71 del Complejo Educativo Francisco de Gurruchaga de Rosario, Mariana Sánchez, explicó que “hoy iniciamos este retorno que tiene que ver con un reencuentro, con compartir actividades lúdicas, actividades artísticas. No tiene características de clases, sino que los chicos puedan volver a ser alojados en la escuela”.

“El objetivo es que la escuela, como institución social, pueda dar fin al año 2020, no al ciclo porque cierra en 2021, pero poder dar algún cierre acompañados de sus compañeros y docentes”, agregó.

“El 19 de noviembre nosotros recibimos la circular enviada por la ministra de Educación, la cual la estuvimos trabajando. Venimos trabajando muy bien la virtualidad, así que hicimos un sorteo con los chicos para conformar las burbujas, donde cada uno tiene un día y una hora asignados”, detalló la directora.

En ese marco, sostuvo que recibieron “mucho apoyo de las familias, les hace muy bien este contacto. Se vio la alegría con la que ingresaron, lo contentos que están de estar acá. Fue todo un desafío, pero trabajamos mucho los protocolos de Nación y provincia con las familias, así que veníamos preparados para este reencuentro”, concluyó Sánchez.