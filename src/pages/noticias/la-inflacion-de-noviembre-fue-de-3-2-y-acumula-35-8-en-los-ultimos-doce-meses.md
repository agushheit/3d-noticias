---
category: Agenda Ciudadana
date: 2020-12-17T10:36:28Z
thumbnail: https://assets.3dnoticias.com.ar/1712-indec.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Infobae'
resumen: La inflación de noviembre fue de 3,2% y acumula 35,8% en los últimos doce
  meses
title: La inflación de noviembre fue de 3,2% y acumula 35,8% en los últimos doce meses
entradilla: El Indec dio a conocer el indicador que muestra una baja respecto de octubre,
  cuando fue el pico con 3,8%. Desde enero el aumento del IPC fue del 30,9%.

---
El Instituto de Estadísticas y Censos (Indec) publicó el dato de inflación de noviembre que alcanzó el 3,2%, dato con el que se muestra por abajo de octubre, que fue el pico del año con 3,8%, pero que se mantiene por arriba del 3%, escalada que comenzó en agosto pasado. En los últimos 12 meses, llegó al 35,8 por ciento.

Con el aumento de la inflación de noviembre, y luego de cuatro meses consecutivos con un indicador en crecimiento, **la inflación acumula un alza de 30,9% durante los primeros once meses de 2020**. Esa alza representa 17,4 puntos porcentuales menos que en el mismo período de 2019.

Los resultados se ubicaron por debajo del consenso que habían mostrado las consultoras privadas que miden el incremento del costo de vida, que habían pronosticado que el indicador estaba mostrando una aceleración y que tuvo su pico en octubre pasado, y que el promedio rondaba por arriba del 3,5 por ciento de aumento del costo de vida durante el mes pasado.

![](https://assets.3dnoticias.com.ar/1712-tabla-indec.jpg)La desaceleración “respondió en parte a menores aumentos en los productos Estacionales, que desaceleraron hasta 2,0% mensual luego de haber crecido 9,6% mensual en octubre y 7,9% mensual en septiembre”, explicaron fuentes oficiales. 

También, explicaron, obedeció al menor incremento de los Regulados, que volvieron a desacelerar hasta 1,2% mensual (vs. 1,5% mensual en octubre). Cabe recordar que las tarifas de los principales servicios públicos continúan congeladas.

La inflación Núcleo, aquella que no incluye los precios de productos o servicios estacionales y regulados, aceleró hasta 3,9% mensual; en octubre, había subido 3,5 por ciento.

En el análisis de cada segmento, Alimentos y Bebidas desaceleró hasta 2,7% mensual, luego de haberse disparado un 4,8% en octubre, con menor suba de lácteos, aceites y frutas y caída en verduras. 

También tuvieron una suba menor que la del mes anterior Equipamiento y mantenimiento del hogar (3,9% mensual vs. 4,5% en octubre), Prendas de vestir y calzado (3,7% mensual vs. 6,2% en octubre) y Transporte (3,6% mensual vs. 4,2% en octubre).

Por el contrario, los segmentos que aceleraron sus precios con relación al mes anterior fueron Recreación y Cultura (5,1% mensual vs. 2,6% en octubre) empujado por la mayor apertura de actividades como gimnasios y alquiler de canchas, Salud (3,7% vs. 3,1% en octubre) y Bebidas alcohólicas y tabaco (3,0% vs. 1,9% en octubre). Los menores registros se dieron en Educación (0,4% mensual) y Comunicación (con una baja de 0,6% mensual).

![](https://assets.3dnoticias.com.ar/1712-indec1.jpg)

Las consultoras privadas que miden el costo de vida previamente a que se conozca el dato del Indec señalaron que con los aumentos en alimentos y varios “descongelamientos” previstos para los próximos meses, la inflación comenzó a acelerarse en el último tramo del año y preveían una suba cercana al 3,5% mensual para noviembre y ya observan que el costo de vida de diciembre estará más cerca del 4%. Con estos datos, las consultoras privadas ya observan un cierre del año con una inflación acumulada de alrededor del 35 por ciento.

Así, por ejemplo, Econviews estima un cierre del año con una inflación acumulada punta a punta de 35,5%; para la consultora LCG al cierre de diciembre el costo de vida del primer año del Frente de Todos será de 35 por ciento. La medición más alta quedó para la consultora Analytica, que anticipó que como consecuencia del aumento de algunos de los precios como combustibles y prepagas “se va a duplicar la tasa de inflación de los precios regulados” y el dato de inflación anual será de 36,5 por ciento.

![](https://assets.3dnoticias.com.ar/1712-indec2.jpg)

El ministro de Economía, Martín Guzmán, viene repitiendo que este año la inflación bajará 20 puntos porcentuales respecto del dato de 2019. Y a 16 días de que cierre este 2020 y un mes de que se conoció el dato final, parece que no estaría tan equivocado. Según el último Relevamiento de Expectativas del Mercado (REM) del Banco Central, la inflación minorista interanual para diciembre se ubicará en 36,7%, con un aumento de 0,9% frente al pronóstico de octubre. Asimismo, la suba del Índice de Precios al Consumidor (IPC) para los meses del verano promediará el 4% mensual.

El promedio entre los consultores que mejor pronostican esta variable dentro del REM para el corto plazo indica que la inflación nivel general para 2020 se ubicaría en 36,4% i.a., esto es, 0,3 p.p. inferior a la mediana del relevamiento de la totalidad que participa en la encuesta (36,7% i.a.) pero 1,3 p.p. superior a la previsión que este conjunto de pronosticadores brindó en la encuesta previa (35,1% i.a.).