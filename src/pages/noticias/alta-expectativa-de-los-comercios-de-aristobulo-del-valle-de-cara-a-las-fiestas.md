---
category: La Ciudad
date: 2021-12-08T06:00:04-03:00
thumbnail: https://assets.3dnoticias.com.ar/aristobulo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Alta expectativa de los comercios de Aristóbulo del Valle de cara a las fiestas
title: Alta expectativa de los comercios de Aristóbulo del Valle de cara a las fiestas
entradilla: 'Es lo que perciben desde la Asociación de Comerciantes de Aristóbulo
  del Valle. A pesar de esto, advirtieron fallas en el funcionamiento de Billetera
  Santa Fe en los últimos días.

'

---
De cara a diciembre, los comercios de Santa Fe coinciden en que las expectativas son muchísimas en vísperas de las fiestas, época fuerte para las ventas históricamente. Esta no es la excepción en los comercios de avenida Aristóbulo del Valle, una de las principales arterias comerciales de la ciudad, en donde los comerciantes aseguran tener "muy buenas expectativas" para cerrar un año difícil desde lo económico.

 Georgina Giay, vicepresidenta de la Asociación de Comerciantes de Aristóbulo del Valle (Acav), manifestó: "Las expectativas son muy buenas y son muchísimas, en estos días se está viendo mas movimiento en la avenida. Por ese lado estamos muy contentos".

**Fallas en Billetera Santa Fe**

Sin embargo, en los últimos días se repitió la queja de parte de numerosos usuarios de Billetera Santa Fe por el mal funcionamiento de la app de pago virtual. Esto fue graficado por los comerciantes: "Estamos viendo que hay días en los que tenemos muchos problemas para cobrar con Billetera Santa Fe, el sistema se cae. Eso es lo que todos los comerciantes tememos a la fecha".

Sobre la proporción de clientes que abonan su compra con la billetera virtual, Giay aseguró que se trata "del 50% o más", asegurando que "hay gente que si el negocio no tiene Billetera Santa Fe no entra".

Además, esta modalidad de pago modificó la práctica del consumidor santafesino, puntualizando en los días con mayores índices de ventas en los comercios. "Antes los días de más ventas eran viernes y sábados. Ahora está cambiando esa modalidad a lunes, martes y miércoles por la Billetera Santa Fe", aseguró Giay sobre esto.

"Esto fue ina inyección bastante importante en cuanto al flujo de personas que vienen a los comercios. Por más que ya no tengan el reintegro de los $5.000 la gente se acostumbró al pago virtual. Mucha gente desconocía ese sistema y lo adoptaron", continuó la referente de Acav.

**Sorteo de vouchers**

Como parte de un programa para incentivar el consumo, la Municipalidad lanzará a partir del día jueves una iniciativa para la gente que compre en la avenida Aristóbulo del Valle, para que participen de un sorteo de diez vouchers de $5.000. Esto se hará a nivel ciudad con aproximadamente siete vouchers por cada avenida comercial.