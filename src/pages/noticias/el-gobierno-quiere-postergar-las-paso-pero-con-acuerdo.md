---
category: Agenda Ciudadana
date: 2021-04-02T06:15:15-03:00
thumbnail: https://assets.3dnoticias.com.ar/paso.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: El gobierno quiere postergar las PASO pero con acuerdo
title: El gobierno quiere postergar las PASO pero con acuerdo
entradilla: El Gobierno resolvió esperar hasta el miércoles para mover sus fichas,
  ya que el martes se reunirá la plana mayor de Juntos por el Cambio en una reunión
  presencial para fijar postura sobre el tema.

---
Mientras Juntos por el Cambio se tomará hasta el martes para resolver su postura sobre la postergación de las PASO, luego de los ruidos internos que provocó la visita de Jorge Macri y Cristian Ritondo a la Casa Rosada, el oficialismo ya decidió avanzar con el proyecto, a la expectativa de alcanzar un acuerdo amplio con la oposición para darle mayor legitimidad a la medida.

El miércoles, entonces, será la visita del ministro de Interior, Eduardo "Wado" de Pedro, a la Cámara de Diputados, donde se reunirá con los jefes de bloque y referentes parlamentarios del oficialismo y de la oposición en un encuentro reservado.

Para meter cuña en la oposición, la jugada del oficialismo incluye la decisión de trabajar en base a un proyecto presentado por la diputada de la UCR Evolución Carla Carrizo, que propone llevar las PASO de agosto a septiembre, y las elecciones generales de octubre a noviembre, acortando la distancia entre las fechas de ambos turnos electorales.

En Diputados, De Pedro dará las razones que justifican la necesidad de modificar el calendario electoral, algo que ya había adelantado el miércoles el jefe de la bancada del Frente de Todos, Máximo Kirchner, a Ritondo y Jorge Macri.

Estos dos dirigentes del ala moderada del PRO recibieron pases de factura de parte los jefes partidarios de Juntos por el Cambio, por prestarse al juego de "manipulación" y "aprovechamiento político" del Gobierno, al que acusan de haberles tendido una emboscada al instalar la idea de que ya había un "pacto" consumado sobre el corrimiento de la fecha de elecciones.

Lo cierto es que el Gobierno nunca comunicó un "pacto", sino que fue la puesta en escena del comienzo de un diálogo hacia una decisión en la que el oficialismo quiere incluir a la oposición, en lugar de cortarse solo, pese a ya contar con los votos suficientes.

Fuentes del oficialismo aseguran que ya tienen los votos asegurados para avanzar pero que a pesar de eso la voluntad es llegar a un consenso más amplio.

En Juntos por el Cambio, pese los chisporroteos y los "tirones de oreja" para Jorge Macri y Ritondo, en general no se vio por ahora un rechazo a la propuesta de Máximo Kirchner para postergar la fecha de comicios, con excepción de la presidenta del PRO, Patricia Bullrich, quien consideró que "no es bueno cambiar las reglas en un año electoral".

Otros referentes de JxC, como Alfredo Cornejo (UCR) y Maximiliano Ferraro (Coalición Cívica) se limitaron a desmentir un supuesto pacto entre oficialismo y oposición.

"Cualquier modificación a las reglas de juego electoral debe realizarse institucionalmente y convocando a los partidos políticos y jefes de bloques parlamentarios", escribió Ferraro en Twitter, cuando Juntos por el Cambio había entrado en estado de ebullición por la foto de Ritondo y Jorge Macri con De Pedro, Sergio Massa, Axel Kicillof, Máximo Kirchner y compañía.

En realidad, el presidente de la Coalición Cívica no planteaba nada diferente de lo que el oficialismo estaba dispuesto a llevar a cabo, que es el abordaje parlamentario de la iniciativa.