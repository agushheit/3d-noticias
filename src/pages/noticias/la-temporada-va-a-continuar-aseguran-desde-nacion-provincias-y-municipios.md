---
category: Agenda Ciudadana
date: 2021-01-10T10:58:09Z
thumbnail: https://assets.3dnoticias.com.ar/100121-temporada-verano.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: '3DNoticias - Fuente: Télam'
resumen: "«La temporada va a continuar», aseguran desde Nación, provincias y municipios"
title: "«La temporada va a continuar», aseguran desde Nación, provincias y municipios"
entradilla: Ante el incremento de casos de coronavirus y la posibilidad de que se
  apliquen restricciones en centros turísticos, el ministro Lammens afirmó que «la
  temporada de verano va a continuar» hasta marzo.

---
La Nación, las provincias y los intendentes –sobre todo los de la costa atlántica- coinciden en la necesidad de extremar los cuidados por el incremento de casos de coronavirus para que no se frustre la temporada de verano que, aunque en niveles menores a otros años, ya logró movilizar a cuatro millones de argentinos desde comienzos de diciembre.

Ante el incremento de casos de coronavirus y la posibilidad de que se apliquen restricciones en centros turísticos, el ministro de Turismo y Deportes, Matías Lammens, aseguró en declaraciones radiales que «la temporada de verano va a continuar» hasta marzo.

No obstante, **el funcionario apeló a la responsabilidad individual para que se extremen los cuidados sanitarios y se evite la suspensión del actual periodo estival de vacaciones**.

En coincidencia con gobernadores e intendentes de diferente color político que le piden a la sociedad incrementar los cuidados ya conocidos como el distanciamiento social, el lavado de manos, evitar aglomeraciones y usar tapaboca, el ministro de Turismo sostuvo que <span style="color: #EB0202;">**«lo importante es que cumplamos con las reglas del cuidado sanitario y no nos relajemos**</span>. Si cumplimos con los cuidados, podemos estar en cualquier lado sin correr riesgos, y además estamos cuidando la economía, el trabajo y por supuesto, la salud», agregó.

Sin abandonar el eje del cuidado sanitario y el control de la situación epidemiológica, Lammens consideró que desde el gobierno nacional «se está dando el mayor nivel de certidumbre para que la gente vaya tranquila a disfrutar de las vacaciones».

«Es una temporada diferente, pero es muy importante que pudimos iniciarla el 1 de diciembre y la vamos a continuar. Es importante saber cuántos turistas hay en cada destino y conocer la cantidad de contagios que se producen en esos lugares», observó.

Por su parte, la vicegobernadora bonaerense, Verónica Magario, señaló en su cuenta oficial de la red social Twitter que «a fin de continuar con la temporada turística, establecimos nuevos parámetros de riesgo sanitario para que cada localidad responda a sus necesidades. Mantengamos los cuidados y evitemos los eventos masivos para que todas y todos puedan disfrutar sus vacaciones en la Provincia».

El intendente del municipio bonaerense de Pinamar, Martín Yeza (PRO), comparte la preocupación de que se tenga que suspender la temporada de verano por el constante incremento de contagios en los centros turísticos.

«El cierre de temporada es lo que más me aterroriza. Por eso, vamos a hacer lo máximo posible para evitarlo, por ahora es solo una hipótesis y por eso estas medidas buscan reducir el costo al mínimo posible, invitando a la ciudadanía a que tengamos los cuidados de hace un mes», señaló Yeza en declaraciones radiales.

Luego de la reunión realizada el viernes en el Museo del Mar en la ciudad bonaerense de Mar del Plata entre el gobernador Axel Kicillof y los intendentes de los municipios de la costa atlántica, el jefe comunal de Pinamar contó que los objetivos son el aumento de los controles y la concientización en el espacio público.

Al respecto, uno de los que participó de ese encuentro fue el ministro de Producción bonaerense, Augusto Costa, quien también fue directo: «el objetivo principal es garantizar la temporada turística. **Lo principal es cuidar la salud, cuidar la temporada, el trabajo y la producción**». Además, planteó que habrá «una ronda de consultas con intendentes para tomar las medidas más adecuadas para este momento de la situación sanitaria, para que se pueda garantizar la continuidad de la temporada turística y se pueda revertir esta tendencia preocupante».

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.15em; font-weight: 700">Nuevas restricciones </span>**

El gobierno de la provincia de Buenos Aires dispuso una serie de medidas para contener el incremento de contagios de coronavirus, que incluyen la **suspensión de las actividades entre la 1 y las 6 horas de la mañana en algunos municipios, y la aplicación de multas de hasta 3 millones de pesos para los organizadores y asistentes a fiestas clandestinas**.

Según se anunció hoy en un comunicado, en los municipios que se encuentren en fases 3 y 4 del distanciamiento social «se suspenderá entre las 1 y las 6 horas la actividad comercial, artística, deportiva, cultural, social y recreativa, con excepción de las actividades productivas manufactureras, agropecuarias y todas aquellas definidas como esenciales».

El gobierno de Axel Kicillof decidió, además, reducir las actividades sociales, recreativas y familiares a grupos de hasta 10 personas en espacios cerrados y abiertos y restringir el uso de transporte de pasajeros urbano a personas alcanzadas por las actividades y servicios definidos como esenciales.

Las nuevas disposiciones entrarán en vigencia el lunes 11, a partir de la hora 1:00, mientras que al martes se actualizarán las disposiciones sobre la base de la nueva situación epidemiológica y sanitaria de la provincia, según se indicó.

Distintas provincias analizan alguna restricción nocturna, pero la gran mayoría coincide en que no habrá frenos a la circulación mientras que otras, como Córdoba, ya anunciaron que no aplicarán ningún tipo de límites, aunque todas coinciden en apelar a la responsabilidad social para evitar medidas más drásticas que deriven en el cierre de la temporada estival.

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.16em; font-weight: 700">Balance</span>**

Por otra parte, al cumplirse los primeros 40 días del inicio de temporada, Lammens realizó un balance e informó que desde el 1 de diciembre «cuatro millones de personas salieron de vacaciones, según los permisos gestionados en la aplicación **Cuidar Verano**».

En ese sentido, desde el Ministerio de Turismo explicaron que esa cifra aumentaría a casi cinco millones de argentinos porque hay algunas provincias que no piden certificado.

En cifras, se explicó que este sábado 9 hay un millón 600 mil personas vacacionando por algún centro turístico del país, siendo la costa atlántica el principal destino con más de 600 mil veraneantes.