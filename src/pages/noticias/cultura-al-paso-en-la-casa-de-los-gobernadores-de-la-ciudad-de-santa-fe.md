---
category: La Ciudad
date: 2020-11-29T13:15:59Z
thumbnail: https://assets.3dnoticias.com.ar/CASA-CULTURA.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Cultura al Paso
title: Cultura al Paso en la Casa de los Gobernadores de la ciudad de Santa Fe
entradilla: Desde el mes de diciembre, y todos los domingos durante el verano, se
  desarrollarán diferentes actividades culturales en el marco del programa "Cultura
  al Paso".

---
El Ministerio de Cultura llevará adelante un proyecto cultural en la Casa de los Gobernadores de la ciudad de Santa Fe, en el marco del programa "Cultura al Paso". **La iniciativa comenzará en el mes de diciembre y se desarrollará todos los domingos de verano**.

La propuesta incluye la tradicional visita guiada, que es realizada por personal idóneo, durante la cual los visitantes, con los correspondientes protocolos y en un número acorde al cuidado que requiere el sitio, podrán recorrer las instalaciones y conocer la historia de la construcción, como así de quienes fueron sus moradores.

Además, se está trabajando en la edición de un libro donde se narrará esta historia.

**Cada domingo se instalará una Pasarela cultural a cielo abierto, con distanciamiento social** y con los recaudos pertinentes, donde harán intervenciones entre 6 y 12 artistas cada domingo, en la vereda de la casona, que es muy concurrida por ser paso natural de los transeúntes camino de la costanera.

Habrá arte callejero temático o libre, según lo pertinente.

Asimismo, cada noche se coronará con un espectáculo en el marco del ciclo “Abierto a Cielo”, que el Ministerio pone en marcha en distintos puntos de la provincia. Será un show intimista, con una ambientación “tipo bar” y con filmación para que pueda ser transmitida por la señal del Canal de la Provincia.

Por último, las visitas guiadas también incluirán el acceso a una muestra de arte de diversos artistas santafesinos, con curaduría de personal idóneo.

**La iniciativa se extenderá a otros espacios culturales de la ciudad de Santa Fe**, conforme la evolución de las condiciones sanitarias y de salud de la población lo permitan.