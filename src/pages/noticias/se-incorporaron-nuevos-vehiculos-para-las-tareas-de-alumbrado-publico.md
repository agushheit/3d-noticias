---
category: La Ciudad
date: 2021-08-10T06:00:55-03:00
thumbnail: https://assets.3dnoticias.com.ar/Camionetas.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Se incorporaron nuevos vehículos para las tareas de alumbrado público
title: Se incorporaron nuevos vehículos para las tareas de alumbrado público
entradilla: 'La Municipalidad adquirió dos camionetas con todo el equipamiento necesario
  para la Dirección de Alumbrado y Electromecánica. '

---
De la Dirección de Alumbrado Público y Electromecánica ubicada sobre calle bulevar Pellegrini 3790, parten a diario las dos nuevas camionetas que adquirió la Municipalidad para renovar la flota, con el objetivo de reforzar las tareas que se llevan a cabo en el marco del Plan de iluminación, así como para el mantenimiento de la red existente.

Estos dos vehículos se adquirieron mediante licitación pública por un monto que rondó los 12 millones de pesos. Se trata de dos camionetas tipo pick up cabina simple con equipo hidroelevador con barquilla simple. “Desde que iniciamos la gestión, el alumbrado público fue una de las prioridades del intendente Emilio Jatón; e invertir en equipamiento, en este caso para la Dirección de Alumbrado y Electromecánica, era una de las urgencias”, manifestó el secretario de Obras y Espacio Público, Matias Pons Estel.

Vale destacar que con esta nueva adquisición se logra una flota equipada con tres tipos de vehículos para la Dirección de Alumbrado Público y Electromecánica. Uno grande para las reparaciones en lugares como la Circunvalación donde se necesita un camión de gran porte y altura; después hay otros medianos; y ahora estas dos camionetas que permitirán ingresar a los barrios donde se dificulta hacerlo con los camiones.

“La adquisición de estos dos vehículos 0 kilómetros es importante porque la flota se va desgastando. Son camiones que salen a las 7 de la mañana y vuelven a las 16 horas, y el otro turno parte a las 19 y retorna a las 4 de la madrugada. Tienen muy poco tiempo parados y muchos kilómetros recorridos; por eso renovar la flota oxigena porque permite hacer un mantenimiento de los que hoy están en calle”, destacó el funcionario.

**Trabajos en marcha**

En febrero de este año, la Municipalidad comenzó con una serie de licitaciones enmarcadas en el Plan de Iluminación que el intendente, Emilio Jatón, incluyera en el Presupuesto 2021. El objetivo del programa es recuperar el alumbrado existente en toda la ciudad, lo que contempla el mantenimiento de la red actual pero también implica dotar de nueva iluminación a zonas que actualmente poseen un servicio deficiente.

Estas dos acciones están en marcha en forma paralela. “Por un lado, se concreta un nuevo sistema de iluminación con columnas led. En estos momentos se están terminando las obras en los barrios Scarafía y Liceo Norte, están por comenzar en Yapeyú Oeste y ya está en marcha en Santa Marta, Guadalupe Este, Siete Jefes, Roma y Fomento 9 de Julio”, detalló el secretario de Obras y Espacio Público.

En esta línea, agregó: “Por otro lado se hacen tareas de mantenimiento. Este año superamos los 100 millones de pesos para el mantenimiento de la red actual que se hace con personal municipal. Se hicieron dos licitaciones públicas de 15 millones de pesos cada una de reposición de materiales y está en marcha una tercera”.

Para finalizar, Pons Estel dijo: “Se trata de dos trabajos que están en paralelo. En cuanto a lo nuevo queremos llegar con un 40% de la ciudad iluminada con luces led; y por otro lado, la adquisición de equipos como estas camionetas que son necesarias para el mantenimiento diario y continuo”.