---
category: La Ciudad
date: 2021-07-15T06:15:45-03:00
thumbnail: https://assets.3dnoticias.com.ar/laqesquinaencendida.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Se vacunará sólo en el CEF n° 29 y no habrá testeos hasta nuevo aviso
title: Se vacunará sólo en el CEF n° 29  hasta nuevo aviso
entradilla: La Esquina Encendida presenta un problema eléctrico y debe ser reparado.
  Los hisopados se suspenden hasta que mejore el tiempo.

---
La lluvia y un problema eléctrico complicaron la jornada de este miércoles en lo que respecta a las acciones contra el Covid-19 en la ciudad de Santa Fe.

Por un lado, desde el gobierno informaron que el centro de vacunación en la Esquina Encendida, ubicado en Avenida Facundo Zuviría y Estanislao Zeballos, estará cerrado desde este miércoles y hasta nuevo aviso, debido a un inconveniente con la instalación eléctrica.

Por este motivo, quienes tengan el turno asignado en el citado lugar deben concurrir al Centro de Educación Física n° 29, ubicado en Av. Galicia y Las Heras.

**No hay testeos**

Al mismo tiempo, fuentes gubernamentales anticiparon que debido a las inclemencias en el tiempo no se realizan testeos en los puestos fijos instalados en la capital provincial (Estación Belgrano, El Alero de Av. French, Estación Mitre y El Alero de Acería).

Autoridades explicaron que la lluvia “complica toda la logística”, por la espera de la gente a la intemperie.

En ese sentido, adelantaron que sólo funcionará el que está ubicado frente al Monumento de la Bandera en Rosario.