---
category: Estado Real
date: 2021-06-13T06:00:36-03:00
thumbnail: https://assets.3dnoticias.com.ar/TERRITORIO.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Perotti recorrió el centro de testeos fijo ubicado en la estación Belgrano
  de la ciudad de Santa Fe
title: Perotti recorrió el centro de testeos fijo ubicado en la estación Belgrano
  de la ciudad de Santa Fe
entradilla: El gobernador afirmó este sábado que “hemos tomado la decisión de seguir
  incrementando el nivel de testeos, particularmente a partir de este fin de semana
  y por toda la semana siguiente en la ciudad capital”.

---
El gobernador de la provincia, Omar Perotti, recorrió este sábado el centro de testeos ubicado en la estación Belgrano de la ciudad de Santa Fe, que tiene como objetivo seguir intensificando los controles para cortar la cadena de contagios de Covid-19. En la visita estuvo acompañado por la ministra de Salud, Sonia Martorano; el secretario de Salud, Jorge Prieto y el director de la Región Santa Fe de Salud, Rodolfo Roselli.

“Más testeos nos ayudan a individualizar rápidamente los casos positivos, si tenemos la gente aislada tenemos menos contagios, si tenemos menos contagios tenemos menos internación, y si tenemos menos internaciones, tenemos menos muertes”, detalló el gobernador.

“La estrategia que lleva adelante la provincia para enfrentar la pandemia se basa en muchos testeos, en la atención de todo el sistema de salud, en la expansión al máximo nivel de las camas críticas, y se sostiene la instancia de vacunación y la asistencia a los sectores que no pueden estar desplegando su actividad. Esos puntos son los centrales”, amplió Perotti.

Para finalizar, el titular de la Casa Gris sostuvo que “obviamente como ha habido restricciones, actividades que se han hecho parcialmente, la provincia los asiste económicamente, $1.600 millones con exenciones impositivas y $1.400 millones para asistencia directa a cada uno de estos sectores”.

Por su parte, la ministra Martorano, destacó que “se sumaron cuatro puntos fijos a todos los que teníamos de testeo, y además un camión itinerante de Detectar, de manera tal que, en toda la ciudad de Santa Fe, como se está haciendo en otros puntos de la provincia, se está incrementando exhaustivamente el nivel de testeos”.

“Superamos los 10 mil testeos diarios. Esto es importantísimo porque, en esta etapa de la segunda ola poder detectar inmediatamente que un caso sea positivo o negativo, poder hacer el aislamiento, el rastreo de contacto y seguimiento, es fundamental”, epresó la ministra.

También agregó que “todo santafesino o santafesina que tenga una duda, que fue contacto, que tiene un síntoma, y recordemos que solamente una rinitis, una carraspera, es considerado un síntoma para ir a hacerse un testeo”.

Además, acompañaron a las autoridades en la recorrida, el secretario de Prácticas Socio Comunitarias, Ignacio Martínez Kerz; la subsecretaria de Proyectos Científicos Tecnológicos, Eva Rueda; y la concejala Jorgelina Mudallel.

**DETALLE DE LAS UNIDADES DE TESTEO**

Respecto a las unidades de testeos en la cuidad capital, Mudallel afirmó que “a partir de este sábado 12 de junio y por toda la semana que viene la provincia puso a disposición 4 Unidades Territoriales de Testeos de Covid.

En este momento testeamos en la Estación Belgrano, tenemos otra en Urquiza y J.J. Paso, otra en el alero de Coronel Dorrego y en el Centro Municipal Deportivo”.

“A eso se le suman los Detectar Federal móviles que van a estar en distintos barrios de la ciudad para que la gente se pueda acercar y hacerse el test sin turno previo. Es una herramienta más para disminuir el contagio y celebramos que hoy la provincia ponga a disposición estos lugares para seguir combatiendo esta pandemia. Van a estar funcionando todos los días de 9 a 13”, finalizó la concejala.