---
category: La Ciudad
date: 2021-10-02T06:15:44-03:00
thumbnail: https://assets.3dnoticias.com.ar/aire.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'En Santa Fe sigue el barbijo y vuelven los boliches al aire libre: más aforo
  para bares'
title: 'En Santa Fe sigue el barbijo y vuelven los boliches al aire libre: más aforo
  para bares'
entradilla: 'Se habilitó de forma escalonada el rubro confiterías y salones de fiestas,
  con 70% de aforo permitido: los asistentes deberán acreditar al menos una dosis
  aplicada. '

---
Tras los anuncios de Nación con nuevas flexibilizaciones, el gobierno santafesino adhirió al (DNU) N° 678/21 del Poder Ejecutivo Nacional y adelantó las "nuevas medidas de convivencia en  pandemia" , que ya rigen desde este sábado. Lo primero: el uso del barbijo seguirá siendo obligatorio.

 "El uso de elementos de protección que cubran nariz, boca y mentón será obligatorio en espacios cerrados de ingreso público y al aire libre cuando se diera la concurrencia simultánea o la cercanía de personas ajenas al grupo conviviente", dice el artículo 4 del Decreto provincial Nº 947, que ya rige desde este sábado.

 Pero la gran novedad es que se habilitará de forma progresiva y como una suerte de prueba piloto el rubro boliches y salones de fiestas de eventos, bajo estrictas condiciones sanitarias. "En todo el territorio provincial, a partir de la cero hora de este 2 de octubre de 2021, se habilita el  funcionamiento de discotecas y locales bailables, salones de fiestas para bailes o similares, en sus espacios al aire libre", establece el artículo 9.

 Pero las condiciones para estos rubros son contar con habilitación de las autoridades locales, las que determinarán los espacios al aire libre autorizados y el aforo permitido para los mismos, lo que deberá estar informado debidamente en el ingreso; y cumplir con la regla general de no excedencia de ocupación máxima del 70% del espacio habilitado al aire libre.

 Asimismo, podrán abrir sus puertas los viernes, sábados y vísperas de feriado, de las 20 a la hora 3 del día siguiente. "Se permitirá el ingreso sólo de personas que cuenten con al menos una dosis de vacuna, aplicada con una antelación mínima de 14 días corridos", dice el acto administrativo.

 El Ministro de Trabajo, Juan M. Pusineri, declaró que en el caso del rubro boliches y salones de fiestas y eventos (que no pudieron trabajar en lo que va de pandemia), éstos serán habilitados para volver a trabajar pero "bajo esquema progresivo" y en una especie de prueba piloto. Por eso, las exigencias antes descriptas.

 **Comercio y gastronomía**

Los locales gastronómicos (bares, restaurantes, heladerías y otros autorizados a funcionar como tales, con concurrencia de comensales), desarrollarán su actividad sin excederse del factor máximo de ocupación de las superficies (70% de aforo) los viernes, sábados y vísperas de feriados, entre 6 y la hora 2 del día siguiente. El resto de los días de la semana, entre las 6 horas y la una 1 hora del día siguiente. 

 Asimismo, el comercio mayorista y minorista de venta de mercaderías, con atención al público en los locales, desarrollarán sus actividades sin excederse del factor máximo de ocupación de las superficies (70 % de aforo máximo), y se extenderá el horario de trabajo todos los días de la semana hasta 21 horas, con excepción de los kioscos y similares que podrán permanecer abiertos hasta las 24 horas para la atención al público residente en su cercanía. Esto no afecta a las farmacias de turno. 

 **Deportes**

La actividad en hipódromos, con concurrencia de público, deberá ajustarse al máximo de 1.000 personas asistentes; sin contar en esa cifra a los protagonistas del espectáculo y auxiliares intervinientes, dice el decreto. Respecto de las competencias automovilísticas y motociclísticas, se habilita la asistencia de hasta mil personas, "con público local y visitante", añadió Pusineri.

 Continuarán habilitadas la actividad deportiva en modalidad entrenamiento y recreativa de deportes individuales o grupales; gimnasios, natatorios y establecimientos afines, entre las 7 y las 22 horas; la pesca deportiva y recreativa en la modalidad desde costa y embarcados, navegación recreativa o deportiva, y las actividades de guarderías náuticas, a los fines del retiro y depósito de las embarcaciones, entre las 7 y las 20 horas. 

 También, las actividades religiosas en iglesias, templos y lugares de culto, correspondientes a la Iglesia Católica Apostólica Romana y entidades religiosas inscriptas en el Registro Nacional de Cultos, entre las 8 y las 21 horas; y los cines, entre las 10 y la 1 hora del día siguiente.

 Finalmente sigue permitido el funcionamiento de locales de juegos infantiles (miniclubs y peloteros entre las 8 y las 24 horas; con excepción de los ubicados en shoppings y paseos comerciales cerrados.