---
category: Agenda Ciudadana
date: 2021-09-03T06:15:41-03:00
thumbnail: https://assets.3dnoticias.com.ar/GREMIOS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: La provincia convocó a gremios docentes a paritarias, pero no se hablará
  de salarios
title: La provincia convocó a gremios docentes a paritarias, pero no se hablará de
  salarios
entradilla: El encuentro que convocó el gobierno provincial será para discutir temas
  relacionados a carrera docente, boleto educativo gratuito y comité mixto. Insisten
  en adelantar el aumento de octubre.

---
El gobierno provincial convocó a los gremios docentes a una reunión por paritarias para este viernes 3 de septiembre, en la que se omitió abordar una posible recomposición salarial esperada por el sector. El encuentro tendrá en agenda el abordaje de las temáticas referidas a carrera docente, boleto educativo gratuito y comité mixto.

Así fue anunciado en un escueto comunicado emitido por la Asociación del Magisterio de Santa Fe (Amsafé). Desde el gremio docente deslizaron que no se hablaría de cifras porcentuales que viene reclamando el sector. Lo que persiguen los gremios docentes es la reapertura de la discusión salarial, en el marco de una inflación de más del 45% y el cumplimiento de los compromisos asumidos al inicio del año.

“Desde Amsafe reivindicamos el ámbito paritario y seguimos exigiendo la urgente convocatoria para discutir salario, tal y como se hizo a nivel nacional” reza el comunicado emitido.

El cumplimiento de los acuerdos aún pendientes desde marzo encabeza la lista de los reclamos, entre los que se cuentan la incorporación a partir de abril como suma remunerativa y bonificable lo que se había pagado en negro y que aún no se concretó. A esto le sigue el reclamo del adelante de la cuota de aumento prevista en paritarias para octubre.

La secretaria general de Amsafe, Sonia Alesso, había dicho días atrás al respecto: "Es una cuestión que tiene que ver con un hecho de la realidad; la inflación superó la pauta salarial que se había discutido a principios de año. Todos los gremios están pidiendo esa actualización; no hay razón para que la provincia no actualice el salario – planteó- porque, además, ya se está viendo que esta misma situación de rediscusión se está dando en el sector privado".