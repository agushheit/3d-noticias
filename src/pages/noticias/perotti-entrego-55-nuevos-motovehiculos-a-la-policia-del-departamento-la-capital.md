---
category: Estado Real
date: 2021-06-26T06:00:31-03:00
thumbnail: https://assets.3dnoticias.com.ar/MOTOS.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Perotti entregó 55 nuevos motovehículos a la policía del departamento La
  Capital
title: Perotti entregó 55 nuevos motovehículos a la policía del departamento La Capital
entradilla: "“Sabemos que falta mucho, pero hay que seguir equipando y capacitando,
  no hay otra forma”, expresó el gobernador durante el acto en el que también participó
  el ministro de Seguridad, Jorge Lagna."

---
El gobernador Omar Perotti, junto al ministro de Seguridad, Jorge Lagna, entregaron este viernes 55 motovehículos 0 km para la Policía del departamento La Capital.

En la oportunidad, Perotti expresó que “es una importante entrega que se está haciendo en las principales ciudades de toda la provincia. Hoy le toca aquí en la ciudad de Santa Fe, a toda la Unidad Regional I, con el deseo de que sean herramientas que ayuden a mejorar las tareas de prevención y a dar respuestas”.

“Sabemos que falta mucho, pero hay que seguir equipando y capacitando, no hay otra forma y queremos que esa mayor presencia no solo sea en número, sino que sea con buen equipamiento y buena capacitación”, finalizó el gobernador.

A su turno, el ministro de Seguridad mencionó que hoy la provincia “está modernizando la fuerza policial con mayor equipamiento y mayor tecnología, y prueba de ello son estas 55 motos de última generación, y un total de 260 que estamos entregando a lo largo y ancho de toda la provincia, para brindarles a nuestros motoristas las mejores herramientas, con una inversión de 360 millones de pesos solamente en este ítem”.

Al respecto, Lagna agregó que “estamos convencidos de que esta herramienta es fundamental, en estos tiempos, para atacar el delito que aflige mucho al vecino, que es el hurto y el robo, el delito predatorio, sobre todo en los lugares más poblados, en las zonas bancarias o de mayor circulación. Esto nos brinda la posibilidad de una respuesta más rápida, unida a un salto tecnológico que va a tener la Policía con nuestras centrales de inteligencia, que ahí va a unir al 911 con los sistemas de videovigilancia, los sistemas municipales, es decir, estamos haciendo una inversión en 2 años de 7.400 millones de pesos”, dijo el funcionario, quien agregó que “está en marcha y muy pronto habrá conclusiones de una licitación para adquirir 400 patrulleros, estamos renovando la flota”.

**RECURSO HUMANO**

Por otro lado, “está el aspecto del recurso humano -continuó Lagna- estamos con capacitación permanente y con mayores ingresos de policías. No podemos inventar policías, lo que podemos hacer es capacitarlos mejor y dar herramientas para que año tras año los ingresantes sean más. Este año van a hacer 1.600, el año que viene van a superar los 2.500. Para eso encaramos distintas alternativas, como las escuelas regionales de Policía como la que ya está funcionando el Murphy, pronto vamos a concretar la de Rafaela, y estamos trabajando para concretar la de Reconquista y otras más”.

Finalmente, el titular de la cartera de Seguridad remarcó que “hay un programa que ataca esos aspectos: el humano y el tecnológico. Seguiremos trabajando día a día, sin fórmulas mágicas, invirtiendo una parte importante del dinero de los santafesinos en estos ítems. No es una declamación, se ve en el presupuesto y se ve en un accionar policial más cercano a la gente”.

Por su parte, la jefa de la Policía de Santa Fe, Emilce Chimenti, expresó su “alegría por entregar esta cantidad tan importante de motos 0Km, las cuales van a permitir un mejor patrullaje en las ciudades donde van a ser entregadas” al tiempo que agradeció “el esfuerzo que hace el gobierno de la provincia para dotar” a la fuerza “de mejores herramientas para nuestro trabajo”.

**PRESENTES**

De la actividad también participaron la jefa de la Unidad Regional I, Marcela Muñoz; los intendentes de Santa Fe, Emilio Jatón, de Recreo, Omar Colombo, de Santo Tomé, Daniela Qüesta, y de Laguna Paiva, Elvio Cotterli; el presidente comunal de Sauce Viejo, Pedro Uliambre; los secretarios de Seguridad Pública, Germán Montenegro, de Política y Gestión de la Información, Jorge Fernández; y el subjefe de Policía, Ariel Zancocchia, entre otras autoridades provinciales y municipales.