---
category: Agenda Ciudadana
date: 2022-11-18T12:41:52-03:00
thumbnail: https://d7kqr73uznmf6.cloudfront.net/2020-10-07.jpg
layout: Noticia con video
link_del_video: https://youtu.be/r0njz_m05E4
author: 3d noticias
resumen: 'Arroyo Leyes: Juntos por el Cambio pide un plan de seguridad.'
title: 'Arroyo Leyes: Juntos por el Cambio pide un plan de seguridad.'
entradilla: |2-

  Este viernes 18 y sábado 19 los vecinos de Arroyo Leyes se convocan para firmar un petitorio y entregárselo al ministerio de seguridad de la provincia.

---
![](https://assets.3dnoticias.com.ar/arroyo leyes convocatoria.png)

Este viernes 18 y sábado 19 los vecinos de Arroyo Leyes se convocan para firmar un petitorio y entregárselo al ministerio de seguridad de la provincia.

Luego del grave suceso ocurrido este jueves por la madrugada, cuando siete personas vestidas con uniforme policial ingresaron a la comisaria 20, redujo a los agentes y se llevaron armas, un patrullero y un auto particular.

Alfredo Lorenzatto, integrante de la comisión comunal de Juntos por el Cambio, afirmo a nuestro medio que "los vecinos de Arroyo Leyes queremos que se tome en serio el problema de la inseguridad, que se terminen las excusas y empiecen a empatizar con las victimas de inseguridad. No es la primera vez que solicitamos a las autoridades provinciales. No tenemos tampoco proyectos de la comuna sobre seguridad, como cámaras, alarmas comunitarias, etc."

Compartimos la charla con Alfredo Lorenzatto.