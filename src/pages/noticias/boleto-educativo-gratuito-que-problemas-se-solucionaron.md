---
category: Agenda Ciudadana
date: 2021-03-25T07:43:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/boleto-gratuito.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: 'Boleto Educativo Gratuito: qué problemas se solucionaron'
title: 'Boleto Educativo Gratuito: qué problemas se solucionaron'
entradilla: En estos días creció la cantidad de personas que accedieron al Boleto
  Educativo Gratuito en Santa Fe. Adecuaron la plataforma a situaciones particulares.

---
El Boleto Educativo Gratuito del gobierno de la provincia de Santa Fe ya sumó más de 170.000 beneficiarios. El programa comenzó a implementarse con el inicio del ciclo escolar y con el transcurrir de los días suma nuevos usuarios. Las inscripciones continúan llevándose adelante en [https://www.santafe.gob.ar/boletoeducativo/.](https://www.santafe.gob.ar/boletoeducativo/. "https://www.santafe.gob.ar/boletoeducativo/.")

Al respecto, el director provincial de Boleto Educativo y Franquicias, Juan Rober Benegui, afirmó que “estamos muy contentos y satisfechos con la primera semana de aplicación del programa. Actualmente se inscribieron 170.000 personas. Como todo programa que recién inicia, tuvimos algunas complicaciones que se fueron resolviendo. Este lunes arrancó la segunda burbuja escolar y hemos reportado menor cantidad de complicaciones”.

Benegui destacó que “nos encontramos con usuarios que ya tienen activo el beneficio y lo están usando, docentes que nos contaron cuánto es el ahorro mensual que les implica el Boleto Educativo Gratuito, este número es lo más lindo de escuchar”; y precisó que “si bien la intención en primer lugar es garantizar el acceso a la educación en Santa Fe; en segundo lugar y en término de ahorro económico, el programa lo que busca es que ese dinero que destinaban al transporte lo puedan usar en cualquier otro consumo. A veces nos sorprende el monto que implica para cada familia santafesina, la verdad que el ahorro es significativo; y más significativo cuando se trasladan en servicios interurbanos”.

Sobre las complicaciones que se advirtieron los primeros días de uso de la aplicación, Benegui detalló que “en los servicios de media y larga distancia de la provincia de Santa Fe a los usuarios en la aplicación le figuraba un sistema Sube, que no es el que utilizan. Eso se solucionó. La mayoría de las solicitudes ya están aprobadas. El sistema sigue procesando y la inscripción permanece abierta. Por eso le pedimos a todos los docentes, asistentes escolares y alumnos que no se hayan inscripto aún, que lo hagan para hacer uso del beneficio lo antes posible”.

Por otro lado, resaltó: “Hemos recibido reclamos de algunos establecimientos educativos que no figuraban a la hora de inscribirse en la aplicación. Con un cotejo previo con el Ministerio de Educación los fuimos agregando y vamos a seguir agregando a medida que los usuarios nos marcan estos errores. Cuando implementamos el programa sabíamos que seguramente iba a haber cuestiones a corregir y mejorar para optimizar el funcionamiento y eso estamos haciendo”.

“Ahora estamos trabajando en el desarrollo y adecuación para situaciones particulares que nos marcan, sobre todo, docentes y asistentes escolares que necesitan mayor cantidad de viajes para poder trasladarse diariamente”, afirmó el funcionario del gobierno provincial de Santa Fe.

Por último, el director provincial recordó que “tenemos distintas oficinas de atención en la ciudad de Santa Fe, una en El Molino, Fábrica Cultural; otra en la terminal de ómnibus; y otra en El Alero, de barrio Acería”; y reiteró que “es un programa muy grande y por esta primera semana estamos muy satisfechos. Tenemos complicaciones que van surgiendo y a medida que el programa avanza los vamos resolviendo”.