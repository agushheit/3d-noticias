---
layout: Noticia con imagen
author: "Fuente: La Capital"
resumen: Protocolos para viajar
category: Agenda Ciudadana
title: Cómo serán los protocolos para viajar a la costa y a las sierras
entradilla: Desde diciembre se flexibilizarán las medidas para ingresar a las
  provincias. Los requisitos para que los rosarinos puedan vacacionar
date: 2020-11-15T14:48:11.572Z
thumbnail: https://assets.3dnoticias.com.ar/carlos-paz.jpg
---
La pandemia de coronavirus también obligará a unas vacaciones diferentes. Los destinos turísticos alistan los protocolos para recibir a los veraneantes y los rosarinos están a la expectativa. Ya no hará falta tener el hisopado negativo para entrar ni se deberá guardar cuarentena. Sin embargo, seguirán vigentes las medidas de distanciamiento para evitar más contagios y congestionar los hospitales. Dónde y cómo se podrá veranear este año. Aquí los requisitos, precios y protocolos que se están preparando en las sierras, en la costa bonaerense y en el sur del país.

En las ciudades turísticas estará todo organizado para garantizar el distanciamiento. Entre otras medidas se solicita que se vivan las vacaciones con el grupo familiar, en las llamadas “burbujas”, y no con amigos.

A su vez, en todos los lugares se mantendrán las distancias de más de un metro y medio en locales gastronómicos, en las playas y espacios públicos, además del uso de barbijo, que seguirá siendo obligatorio, el de alcohol en gel y la limitación de gente para evitar aglomeraciones.

En tanto, los destinos turísticos aguardan con ansias la llegada de los visitantes luego de ocho meses de inactividad. Saben que ésta es la oportunidad para salvar el año. Aun así, los habitantes temen posibles rebrotes de coronavirus. Desde la provincia de Buenos Aires expresaron que quien se contagie de Covid-19 deberá regresar a su ciudad de origen, o podrá instalarse en uno de los centros de aislamiento que prometió la Nación.

**Cuánto saldrá vacacionar**

Desde los destinos turísticos más concurridos adelantaron que los precios serán “acordes a la inflación” y que estarán entre un 30 y un 50 por ciento más caros que el año pasado.

Sin embargo, el programa “PreViaje” lanzado por el gobierno nacional fomenta el turismo porque otorga un crédito del 50 por ciento para el traslado o para los gastos en el destino.

Aún se desconoce qué pasará con los colectivos de larga distancia, por esto se supone que la gente se desplazará en su propio vehículo.

**Las sierras como destino**

Los rosarinos que elijan viajar a Córdoba podrán hacerlo a partir de enero, en tanto que los propietarios de casas allí estarán habilitados para ingresar a la provincia en diciembre. En ningún caso se solicitará el hisopado, ni se exigirá estar 14 días aislados. Tampoco se pedirá el permiso de circulación, aunque cada localidad podrá determinar sus propios requerimientos.

Desde la agencia Córdoba Turismo aclararon que “todas las medidas que se tomen para habilitar el turismo estarán sujetas a la situación sanitaria”. Es decir que, si llega a haber un rebrote de coronavirus, tal como sucedió en el verano europeo, todo puede volver atrás.

En tanto, desde los centros turísticos de Córdoba ya lanzaron paquetes y ofertas especiales para tentar a los turistas. Por ejemplo, el alquiler de una cabaña de nivel intermedio para dos personas, y por siete noches, en Villa General Belgrano, rondará los 35 mil pesos. Y si fuera para cuatro personas, el precio ascenderá a 41 mil pesos.

A su vez, degustar un menú típico en un local gastronómico de esa localidad, rondará los 600 pesos como mínimo.

Por su parte, los hoteles, las cabañas y demás alojamientos deberán cumplir con las normativas indicadas para todo el país para vivir el distanciamiento social y obligatorio.

En Carlos Paz, los alojamientos están ofreciendo pagos en 12 cuotas con todas las tarjetas con el objetivo de alentar el turismo.

Los destinos internacionales también pelean por tentar a los turistas. Las playas del Caribe solo solicitan presentar el pasaporte y no exigen hisopados ni declaraciones juradas.

**Bariloche se abre el 4 de diciembre**

No son pocos los rosarinos que están averiguando paquetes turísticos en Bariloche, El Calafate y San Martín de los Andes, “porque buscan lugares bien abiertos y no tan concurridos”, explicó Rosario Baraldi desde su agencia de viajes, sorprendida ante este destino preferido.

La Secretaría de Turismo de Bariloche indicó que la provincia de Río Negro “comenzará a tener movimiento turístico interno a partir de mañana, y el 4 de diciembre abrirá sus puertas a las demás provincias y países vecinos”.

Estas fechas se determinaron luego de la realización de una prueba piloto para evaluar la efectividad de los protocolos y los dispositivos de control que se implementarán.

Para ingresar a Bariloche se requerirá un certificado médico que acredite que la persona y sus acompañantes de viaje no se encuentran comprendidos en los grupos de riesgo. A su vez, deberán descargar la App “Circulación Río Negro” y 24 horas antes de viajar completar la declaración jurada donde conste que no presentan síntomas compatibles con Covid-19.

Junto con esto, se solicitará la contratación de un seguro de asistencia al viajero con cobertura de coronavirus.