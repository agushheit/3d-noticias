---
category: Agenda Ciudadana
date: 2021-01-03T11:03:09Z
thumbnail: https://assets.3dnoticias.com.ar/03012021-toque-de-queda.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: El Litoral'
resumen: Avalan un posible «toque de queda sanitario» durante la noche por el aumento
  de casos de Covid
title: Avalan un posible «toque de queda sanitario» durante la noche por el aumento
  de casos de Covid
entradilla: Las reuniones y juntadas de fin de año, principalmente las fiestas clandestinas,
  generaron un incremento en la cantidad de personas infectadas.

---
Infectólogos que asesoran al Gobierno ante la pandemia de coronavirus avalaron, este sábado, la posibilidad de que se avance con un «toque de queda sanitario», en horario nocturno, ante el aumento de casos que se registró en las últimas semanas.

El incremento de contagios de COVID-19, que se dio en las últimas semanas de 2020, adjudicado principalmente a las juntadas y reuniones por fin de año, puso en alerta a la Casa Rosada, que no pudo disfrutar de la llegada de las primeras dosis de la vacuna Sputnik V.

Ese aumento vino de la mano de un crecimiento en la cantidad de fiestas ilegales, particularmente en zonas de veraneo.

Ante ese panorama, el Gobierno baraja por estas horas la posibilidad de implementar un «toque de queda sanitario» que imponga restricciones en horarios nocturnos, tal como hicieron varios países europeos con el comienzo de la segunda ola de casos: ya cuenta con el visto bueno de los expertos que lo asesoran en el manejo de la pandemia de coronavirus.

«Hay un aumento de casos exponencial. Si esto sigue así, yo creo que va a haber medidas restrictivas importantes, guste o no», sostuvo el infectólogo Eduardo López.

En declaraciones radiales, el experto consideró que «hay varias medidas» que podrían adoptarse, «entre ellas el toque de queda sanitario, que consiste en restringir la circulación y las actividades que conlleven un riesgo en el aumento de casos».

En ese sentido, explicó que «Francia lo hizo a partir de las 18:00 horas, España a partir de las 20:00 horas», ante lo cual manifestó que la Argentina debería tomar esa medida «a las 22:00 horas».

De todos modos, el infectólogo remarcó que «hay que tratar de encontrar un equilibrio» entre el cuidado sanitario y la economía, ya que nuevas restricciones perjudicarían a algunos sectores como el entretenimiento, la gastronomía y el turismo.

«Los fines de semana largos disminuyen los testeos, el día 5 o el 6 (de enero) habría que mirar todos los números para tomar una decisión acorde. No deseo que nadie deje de tener sus vacaciones y esparcimiento pero, si queremos tomar el riesgo de enfermarnos, nos estamos equivocando como sociedad», finalizó Eduardo López.

En el mismo sentido se expresó su par, Tomás Orduna, quien también destacó que por la situación actual «se va camino hacia la decisión de restricción nocturna».

«Los contagios están creciendo y, si no se controla, se va a tener que tomar una decisión la semana que viene», añadió el infectólogo.

La curva de contagios de coronavirus en la Argentina había logrado una tendencia a la baja, algo que cambió los primeros días de diciembre.

Hasta el momento en el país se registraron 1.629.594 casos positivos de COVID-19, virus que causó 43.319 muertes.