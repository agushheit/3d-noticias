---
category: Agenda Ciudadana
date: 2021-11-29T06:15:05-03:00
thumbnail: https://assets.3dnoticias.com.ar/VIZZOTTIDELTA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: Vizzotti advirtió que hay 7 millones de personas que todavía no se acercaron
  a recibir la segunda dosis
title: Vizzotti advirtió que hay 7 millones de personas que todavía no se acercaron
  a recibir la segunda dosis
entradilla: 'La ministra de Salud explicó que esa cifra alarmante de cantidad de personas
  que aún no se aplicaron la segunda dosis, fue el motivo principal por el cual se
  resolvió acelerar la puesta en marcha de un pase sanitario.

'

---
La ministra de Salud, Carla Vizzotti, advirtió hoy que hay siete millones de personas en condiciones de recibir la segunda dosis de la vacuna contra el coronavirus pero que aún no se acercaron a un centro vacunatorio.

“Tenemos siete millones de personas en condiciones de completar la segunda dosis y todavía no lo han hecho. Recibieron su primera dosis en el pico, con una percepción de riesgo alta, y en noviembre y en diciembre la percepción del riesgo y la intensidad de los cuidados ha bajado, entonces dijeron ‘voy después’", lamentó la funcionaria.

La ministra afirmó que en el país hay "vacunas en stock" suficientes para dar cobertura a todas esas personas que aún no pasaron por un centro de vacunación para que le apliquen la segunda dosis.

En declaraciones radiales, Vizzotti explicó que "la segunda dosis tiene una relevancia importante" y que el Gobierno tiene "la expectativa de que esas personas" que vienen demorando su acercamiento a un centro de vacunación, lo hagan.

"Las vacunas están, les han enviado turnos. Hay que trabajar muy fuerte en que diciembre sea el mes de avanzar fuerte en eso y el verano, en refuerzos”, acotó.

"La buena noticia es que tenemos la vacunación, tenemos las vacunas y está en nuestras manos avanzar muy rápido en quienes no iniciaron el esquema y en las terceras dosis”, indicó.

Esa cifra alarmante de cantidad de personas en todo el país que aún no se aplicaron la segunda dosis pese a haber recibido turnos para hacerlo fue el motivo principal por el cual el Ministerio de Salud resolvió acelerar la puesta en marcha de un pase sanitario que solo tendrán los ciudadanos que puedan acreditar el esquema completo de vacunación al menos 14 días antes de participar en eventos masivos.

"El pase va a estar en la app Cuidar, estamos trabajando con Innovación hace semanas. Hubo consenso de que es el momento para acelerar la vacunación e implementar esta estrategia", detalló sobre el documento sanitario, que aún no entró en vigencia.

También influyó en la decisión de implementar el pase sanitario el hecho de que se haya tomado conocimiento de la proliferación de una nueva cepa de coronavirus en el sur de África, Ómicron, la cual ya fue detectada en lugares de Europa y Australia.

Sobre esta variante del Covid-19, Vizzotti dijo "falta mucho camino por recorrer para ver las características del virus y la cínica", pero advirtió: "Se vio que tiene 32 mutaciones, que es el doble de lo que tenía la delta".

"La buena noticia es que las personas que han sido detectadas y estaban vacunadas han sido asintomáticas o con cuadros leves", agregó.

En tanto, la funcionaria consideró que "no es una casualidad" que la cepa ómicron haya sido descubierta en el sur de África, dadas las desigualdades en el acceso mundial a las vacunas.