---
category: Estado Real
date: 2021-03-08T07:05:46-03:00
thumbnail: https://assets.3dnoticias.com.ar/boletorural.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Provincia avanza con la implementación del boleto educativo rural
title: La Provincia avanza con la implementación del boleto educativo rural
entradilla: Funcionarios de Educación mantuvieron reuniones explicativas con municipios
  y comunas de los departamentos Castellanos y Las Colonias.

---
Con la premisa de garantizar el transporte rural de estudiantes, docentes y asistentes escolares, el Ministerio de Educación de Santa Fe continuó con las reuniones explicativas con municipios y comunas para asegurar el Boleto Educativo Rural (BER), un componente del Boleto Educativo Gratuito.

En el encuentro con 56 municipios y comunas de los departamentos Castellanos y Las Colonias, que estuvo a cargo de la secretaria de Gestión Territorial Educativa, Rosario Cristiani, se informó a las autoridades municipales y comunales el funcionamiento y los alcances del sistema para lograr la adhesión de los gobiernos locales el BER y avanzar hacia las firmas de futuros convenios que garanticen la plena aplicación de la iniciativa.

El impacto de este beneficio para la educación rural es significativo si se considera que, de los 4.500 establecimientos educativos santafesinos, más de 1.200 pertenecen a la ruralidad, es decir que alcanza casi a la tercera parte del total de escuelas provinciales.

Para referirse a la trascendencia del BER, la secretaria de Gestión Territorial Educativa manifestó “que en el inicio de nuestra gestión firmamos un acta compromiso con todas las autoridades de municipios y comunas que establecía que durante estos cuatro años había una línea de acción estratégica que se llama ‘Todas las chicas y los chicos en la escuela aprendiendo’ y esto es el corazón de la política educativa de este gobierno”.

En ese marco, la funcionaria provincial señaló que “nosotros pretendemos que todas las niñas, niños y adolescentes en edad de educación obligatoria estén en las aulas. Por ello el boleto educativo rural es un sistema que se establece para remover el obstáculo que muchas veces significa el traslado hacia las escuelas y de este modo garantizar el derecho a la educación”.

Asimismo, Cristiani expresó que “esta iniciativa del gobierno provincial tiene como base un concepto al cual todas y todos adherimos que es el de la justicia educativa. Todas y todos tiene el derecho de estar en las escuelas y para eso trabajamos, y el boleto rural es un modo de llegar a cada una de las escuelas rurales y en eso destacamos que esta decisión tiene rango de política pública porque es universal”

Los actos se realizaron en las instalaciones del predio de la Asociación Bancaria de Rafaela y en el Salón Blanco de la municipalidad de Esperanza. En la oportunidad acompañaron a la secretaria Cristiani, el senador del departamento Castellanos, Alcides Calvo; la directora provincial de Equidad y Derechos del ministerio de Educación, Vanina Flesia; las intendentas de Esperanza y Frontera, Ana María Meiners y Victoria Civalero, respectivamente; el delegado de la Región III de Educación, Gerardo Cardoni y la coordinadora pedagógica de la regional, Cecilia Perini.

**Boleto Educativo Rural**

El acceso de este beneficio permite que estudiantes, docentes o asistentes escolares de todos los niveles y modalidades puedan trasladarse al establecimiento educativo rural al que asisten a clases o trabajan diariamente, siempre que esté situado en el ámbito rural, en un medio de transporte acordado con el municipio o comuna correspondiente.

La solicitud del BER se canaliza ingresando los datos en el formulario disponible en [www.santafe.gov.ar/boletoeducativorural](http://www.santafe.gov.ar/boletoeducativorural)

Para consultas ante inconvenientes, los y las interesadas podrán enviar un correo a la dirección electrónica [boletoeducativorural@santafe.gov](mailto:boletoeducativorural@santafe.gov)., o comunicarse a la siguiente línea telefónica 0800-555 74423, de lunes a viernes de 8 a 18 hs.