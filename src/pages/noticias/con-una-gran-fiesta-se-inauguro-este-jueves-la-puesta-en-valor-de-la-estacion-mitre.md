---
category: La Ciudad
date: 2021-10-29T06:00:47-03:00
thumbnail: https://assets.3dnoticias.com.ar/ESTACIONMITRE.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Con una gran fiesta, se inauguró este jueves la puesta en valor de la Estación
  Mitre
title: Con una gran fiesta, se inauguró este jueves la puesta en valor de la Estación
  Mitre
entradilla: n ocho meses, la Municipalidad concretó trabajos de recuperación en la
  fachada, techos y andenes, se realizó la forestación y la recuperación del cabín
  ferroviario.

---
 _Este jueves, a las 20 horas, artistas, músicos y activistas culturales formarán parte de esta celebración._

Comenzó la cuenta regresiva para la inauguración de la primera etapa de recuperación de la Estación Mitre y su entorno. Esta noche, los andenes serán el escenario para que músicos, artistas y activistas culturales recreen la fiesta de la que formarán parte vecinos, organizaciones e instituciones del barrio. La cita será a las 20 horas.

Esta mañana, los secretarios de Desarrollo Urbano y de Educación y Cultura, Javier Mendiondo y Paulo Ricci, respectivamente, y la concejala Laura Mondino, brindaron detalles de los trabajos que se hicieron, de la programación cultural prevista para esta noche y la invitación a la comunidad para elegir un nombre al gran espacio público que la ciudad recupera.

“Es un día de mucha alegría, un día histórico. La recuperación del viejo edificio de la Estación Mitre es una realidad, luego de ocho meses de intenso trabajo”, dijo Mendiondo, y en ese momento recordó cuando el 6 de diciembre del 2020, bajo el mismo timbó donde hoy se dio la conferencia, el intendente Emilio Jatón les mostró a todas las organizaciones, instituciones y vecinos del barrio el proyecto. “Hoy podemos decir que es un hecho”, selló.

Acto seguido detalló algunas de las obras que se concretaron: “Realizamos tareas tanto en el interior como en el exterior del edificio. Se trabajó en su fachada, en la puesta en valor y recambio de las cubiertas que estaban dañadas, hay sanitarios públicos nuevos, entre otras. Adentro se arreglaron los pisos, las paredes donde funciona El Birri, pero además del edificio hay una primera etapa de recuperación del Parque”.

En referencia al entorno al edificio de la vieja terminal de trenes, Mendiondo contó que “hoy tiene nuevas veredas, iluminación, forestación porque se replantaron 42 timbós y muchas otras especies. Es una intervención integral donde podemos decir que hoy arranca la nueva vida de un lugar que había sido abandonado por el Estado y solo la activación de las organizaciones que habitaron este espacio pudieron conservar su uso; ahora estamos reanimando un espacio que parecía destinado al olvido”.

**Una gran fiesta**

Por su parte, Ricci dio detalles sobre los espectáculos culturales que estarán este jueves para celebrar la conclusión de esta primera etapa de recuperación. “Esta noche vamos a tener una fiesta, habrá un acto, pero también una celebración a la que convocamos a los vecinos y vecinas de todos los barrios aledaños que confluyen en la Estación a que se acerquen y festejen junto a nosotros, para que vean cómo se recuperó el edificio, los andenes”, dijo el secretario de Educación y Cultura.

Esta noche, en la fachada por General López se hará el acto oficial de inauguración de las obras. Al finalizar, habrá una intervención teatral y luego en los andenes continuarás los espectáculos. Todos serán recibidos por las murgas Cristo Obrero y Circo Del Litoral. Después, dará inicio la actuación del grupo “Mirá que Quema” acompañado de los Bailarines y Gestores del Festival Constante sede Santa Fe, Gastón Onetto, David Leonhardt y Josefina Reyes.

Al finalizar se presentará el Grupo de Teatro Comunitario Tripa Corazón y por último actuará Son D’ Tomar. “Artistas, músicos y activadores culturales van a estar celebrando y acompañando la conclusión de esta primera etapa y el comienzo de lo que sigue para la recuperación de todo este parque”, cerró Ricci.

**Elegir un nombre**

Por último, Mondino se refirió a la recuperación de la Estación Mitre que “es un gran símbolo para toda esta zona sur de la ciudad”; y aprovechó para contar: “Venimos trabajando en el Concejo con un proyecto para, junto con la comunidad, asignarle un nombre al Parque porque creemos que estos espacios cuando son recuperados por el Estado no hay nada mejor que sean apropiados por los vecinos y vecinas”.

Para cerrar, la concejala expresó: “Por eso nos parece que junto a todas las organizaciones e instituciones que hoy trabajan en forma conjunta en una mesa de gestión con la Municipalidad podamos asignarle un nombre, que sea de manera participativa. Esto es algo muy positivo porque ya tenemos esta experiencia en otros barrios con otros espacios que el municipio va recuperando y son muy lindos”