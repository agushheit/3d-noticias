---
layout: Noticia con imagen
author: "Fuente: LT10"
resumen: El Cullen trabaja al límite
category: La Ciudad
title: El Cullen trabaja al límite y piden a la población que tome conciencia
entradilla: El director del nosocomio explicó cómo están funcionando el efector
  principal de la región y llamó a los santafesinos a cuidarse para evitar
  contagios y ocupación de camas por otros factores como accidentes.
date: 2020-11-21T12:31:46.421Z
thumbnail: https://assets.3dnoticias.com.ar/poletti.jpg
---
**Este mediodía, el director del hospital José María Cullen, Dr. Juan Pablo Poletti brindó una conferencia de prensa** en la que habló de la situación actual del nosocomio en el marco de la pandemia y de una particularidad que preocupa que es el aumento de ingresos por accidentes de tránsitos o episodios violentos.

"El hospital está acostumbrado a trabajar con el 100% de ocupación, con la diferencia, en pandemia de que se aumentó de 30 a 47 camas críticas. En este momento estamos trabajando a lo que se llama a "cama llena". Es una logística día día, para ir moviendo camas para brindar asistencia ya sea para paciente covid o no covid. Pensamos que íbamos a poder disponer de más camas covid, pero sigue habiendo accidentes, heridos" comenzó relatando Poletti.

Además, detalló que esta situación se debe a que "la ciudad ha vuelto a su tránsito habitual, a estar la población afuera. Se liberaron actividades propias de la actividad económica".

**"El hospital está al límite, tenemos que ir reacomodando camas y ni bien tengamos la posibilidad con alta epidemiológica covid con obra social son trasladados a sanatorios privados o está el convenio público-privado para derivar pacientes críticos respirados ya sea con un pago a cargo del estado"**, agregó sobre la situación del coronavirus. Además, aportó que desde el Ministerio de Salud se dispuso la apertura de camas críticas en otros centros médicos como el Nuevo Iturraspe y el Viejo Iturraspe para así alivianar el efector central.

Por otro lado, el Dr. Poletti se tomó unos minutos para hacer un llamado a la población en el que pide a los ciudadanos que tomen conciencia en este fin de semana largo: "Está pasando algo que no pasaba, que en el Comité covid estamos viendo el pronóstico para el fin de semana porque el sábado pasado que llovió fue baja la consulta, y en el caso de este finde largo, le pedimos a la gente que salga con responsabilidad, que cumpla con las normativas vigentes" debido a que **esta semana el covid "ha estado activo, ha aumentado el porcentaje de consultas y de internaciones"**.