---
category: Agenda Ciudadana
date: 2021-09-22T18:00:46-03:00
thumbnail: https://assets.3dnoticias.com.ar/SUBASALARIOS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'Acuerdan suba del salario mínimo: llegará a 33.000 pesos en febrero del
  2022'
title: 'Acuerdan suba del salario mínimo: llegará a 33.000 pesos en febrero del 2022'
entradilla: 'El incremento del 17% se añade a la actualización del 35% que se firmó
  en abril y será en tres etapas: un 9% en septiembre ($ 31.104), un 4% en octubre
  ($ 32.000) y el restante 3% en febrero ($ 33.000).'

---
El Consejo del Salario acordó este martes una suba del Mínimo Vital y Móvil para llevarlo a $ 33.000 en febrero próximo, con revisión en marzo, con lo cual ese haber acumulará en un año un 52,8% de incremento.

La nueva suba del 17% se añade a la actualización del 35% que se firmó en abril y será en tres etapas: un 9% en septiembre ($ 31.104), un 4% en octubre ($ 32.000) y el restante 3% en febrero ($ 33.000).

La reunión se realizó de manera virtual y en la sede de la cartera laboral estuvo el ministro Claudio Moroni.