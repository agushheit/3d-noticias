---
category: Estado Real
date: 2021-08-22T06:00:32-03:00
thumbnail: https://assets.3dnoticias.com.ar/PROVINCIAS.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Perotti: “cada pueblo y ciudad debe tener las mismas oportunidades para
  que podamos equilibrar la provincia”'
title: 'Perotti: “cada pueblo y ciudad debe tener las mismas oportunidades para que
  podamos equilibrar la provincia”'
entradilla: El gobernador visitó las localidades de Pilar y Humberto Primo. Destacó
  el fortalecimiento de la red de traslados con la incorporación de dos ambulancias
  de última generación al sistema.

---
El gobernador de la provincia, Omar Perotti, visitó este sábado las localidades de Pilar (Las Colonias) y Humberto Primo (Castellanos), oportunidad en la que destacó el crecimiento de la red de traslados de urgencias que implementa la provincia con la incorporación, en una primera etapa, de 72 nuevas ambulancias de alta complejidad; al tiempo que volvió a subrayar su compromiso de que “cada pueblo y ciudad tenga las mismas oportunidades para que podamos equilibrar la provincia”.

En Pilar, el mandatario participó de la presentación de una de las flamantes unidades de urgencia con que cuenta el Samco y agradeció “a cada uno de los integrantes de este centro de salud por la dedicación, esfuerzo y entrega para dar respuestas desde el primer día de la pandemia. El cariño y el trato de cada uno de ustedes, que están colocado una vacuna, siempre estuvo presente y eso me lo manifiestan en cada rincón de la provincia”, aseguró Perotti, quien incluyó en ese agradecimiento al cuerpo de Bomberos.

El gobernador agregó que “lo han hecho con todos los santafesinos y las santafesinas, a quienes no solamente le transmiten la esperanza que es la vacuna sino también lo que sentimos: esa acción de servicio que cada uno de nuestro personal tiene que estar dando en un momento tan difícil para todos, incluso para ellos mismos, que desde el primer día no han tenido prácticamente descanso”, enfatizó.

Más adelante, Perotti afirmó que “Pilar se ha ganado el respeto y la consideración por su trabajo en salud. No es una circunstancia de ocasión, sino que se ha trabajado mucho y bien. Se ha referenciado durante años este centro para tranquilidad de la gente de la localidad y la región. Y eso es lo que queremos seguir sosteniendo”, dijo al referirse al ambicioso plan que lleva adelante la provincia para incrementar el parque automotor destinado al sistema de traslados de urgencias.

En ese sentido, Perotti remarcó su intención de que cada persona tenga la posibilidad de que, ante una necesidad de traslado hacia un centro de atención más complejo, se haga con unidades altamente equipadas. “Para tranquilidad de cada uno de los habitantes de Pilar, el lugar donde alguien va a ser trasladado seguramente está a 100 kilómetros o más. Pero cuando esté aquí arriba, ya se va encontrar con todo el equipamiento necesario. Por eso decimos que son terapias intensivas móviles”, sostuvo el gobernador.

Perotti recordó que estas inversiones tienen como objetivo “el cuidado de nuestra gente. Son 365 los pueblos y ciudades en la provincia. Nosotros creemos en la descentralización y la trasferencia de recursos a los municipios y las comunas. En cada municipio y comuna hay como mínimo una obra del gobierno, ya sea del Plan Incluir, de Obras Menores, una ruta, un camino de la ruralidad, una mejora en su centro de salud, en sus escuelas, en infraestructura comunitaria, desagües y obras hídricas. Es nuestra responsabilidad dar certezas. El Estado está para cuidar a todos y a cada uno de los santafesinos y las santafesinas”, concluyó el gobernador.

A su turno, el secretario de Emergencia y Traslados, Eduardo Wagner, recordó que “vinimos hace un año a recorrer este hermoso hospital y había un parque automotor muy deteriorado, roto y abandonado. Hoy, Pilar tiene la posibilidad de tener una cama más de terapia intensiva, como dice el gobernador. Esto va a permitir que el paciente llegue en las mejores condiciones al lugar indicado. Es una cama más de terapia que se incorpora a la red de emergencias que tiene la provincia”.

Por su parte, el presidente comunal de Pilar, Diego Vargas, agradeció “en nombre de toda la comunidad” las gestiones del gobernador para con las localidades del interior provincial y dijo que eso es posible cuando hay un Estado santafesino presente, que sin dudas vincula a un pueblo con la misma necesidad que tiene una ciudad. Porque todos tenemos la necesidad de ser escuchados, pero más aún de recibir respuestas”.

Acompañaron al gobernador, además, la directora de la Agencia Provincial de Seguridad Vial, Antonela Cerutti; el senador de Las Colonias, Rubén Pirola, y autoridades y personal del Samco local y del Sies.

**EN HUMBERTO PRIMO**

Posteriormente, Omar Perotti se trasladó hasta la localidad de Humberto Primo, en el departamento Castellanos, donde supervisó el nuevo equipamiento para traslados de urgencias con que cuenta el hospital local. En la oportunidad, destacó “la importancia de Humberto Primo y la región, una zona de producción y trabajo”, dijo, y agregó que “tenemos que tratar de que cada uno de nuestros pueblos se vaya afirmando y creciendo. Queremos y decimos siempre que hay que equilibrar poblacionalmente esta provincia. No sirve que se concentre en grandes ciudades, la gente tiene que vivir en el lugar donde nació y tener oportunidades en ese lugar”.

El gobernador recordó que “tenemos una provincia con distancias importantes y complejidades mayores en centros que no siempre están al lado de cada localidad. Pero lo que podemos tener es la capacidad de trasladarlo como si ya estaría en ese lugar. El equipamiento que está aquí es el mismo con el que te van a recibir en el lugar done vas. Y poder estar antes con esa atención es, sin dudas, clave para el tiempo que una vida se puede salvar o no. Cuidar la salud es importante y generar oportunidades también”, remarcó.

Más adelante, el mandatario hizo un balance de lo que va de su gestión al mencionar que “nos tocó arrancar mal, sin recursos para pagar los sueldos en tiempo y forma, para pagar el aguilando. Nos tocó ver la obra pública parada porque había una deuda superior a los seis mil millones de pesos. Pero pudimos recomponer eso cuidando la plata, siendo austeros, administrando bien. Y en pandemia, tener este ritmo de obra en todo el territorio provincial es la forma en la que entendemos que hay que gobernar”, señaló.

Acompañado por el presidente comunal y el senador departamental Alcides Calvo, Perotti anunció que “vamos a seguir con obras en esta localidad. Hay otra expectativa importante donde tenemos un grado de avance y tiene que ver con la educación con el edificio de la escuela (técnica). Vamos a avanzar en todos los aspectos necesarios y atinentes a la concreción de este anhelado edificio”, finalizó el gobernador de la provincia.

**EN TACURAL**

Por último, el gobernador de la provincia recorrió la obra que se lleva a cabo el Centro de Salud de Tacural Carlos Franzini, que demandó una inversión cercana a los seis millones de pesos. En la oportunidad, acompañado por el intendente local, Adrián Sola, también supervisó la marcha del programa Caminos de la Ruralidad. “Se viene trabajando a muy buen ritmo y es uno de los ejemplos que uno quiere continuar en toda la provincia. Hemos pasado de un presupuesto inicial de 100 a 1.000 millones, con la idea de que todo el sector rural pueda tener una mejor calidad de vida, con la entrada y salida de la localidad independientemente de los momentos de lluvia, para así mover la producción”, evaluó Perotti.

Además, sostuvo que “queremos seguir afianzando lo que hoy tenemos en los 365 pueblos y ciudades de la provincia: obras del gobierno provincial. Creemos y confiamos plenamente que los pueblos tienen que tener actividad y la posibilidad de arraigar a su gente”. Asimismo, el gobernador dijo que “tenemos que salir de una provincia que se ha concentrado demasiado en grandes ciudades y que podemos equilibrarla poblacionalmente. Para eso hay que trabajar en obras que en muchos lugares generen los nexos. Eso se logra escuchando a las autoridades locales; porque detrás de un escritorio no se consiguen esas soluciones”, aseguró.

Por último, Perotti señaló: “Tenemos que ganar calidad de vida, tienen que sentirse orgullosos del pueblo en el que viven, que en su pueblo pasan cosas, se progresa y avanza cuando la provincia y la comuna están presentes. En definitiva, que el Estado muestre en un momento duro como el que tenemos, que está. Hay muchas posibilidades de seguir haciendo obras en la zona junto al senador Calvo y los presidentes comunales”, anticipó el mandatario provincial.