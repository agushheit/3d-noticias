---
category: Agenda Ciudadana
date: 2021-09-05T06:15:59-03:00
thumbnail: https://assets.3dnoticias.com.ar/EGRESADOS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Desde octubre las provincias evaluarán si autorizan viajes estudiantiles
title: Desde octubre las provincias evaluarán si autorizan viajes estudiantiles
entradilla: Por la baja en el número de casos y el incremento de las vacunaciones
  de la población en general, y en particular de los más jóvenes, analizarán la posibilidad
  de rehabilitar los viajes de egresados.

---
A partir del 1 de octubre cada provincia podrá evaluar la posibilidad de autorizar los viajes de egresados y de estudios en todo el territorio nacional, en el marco del trabajo conjunto entre los ministerios de Turismo y Deportes y de Salud, en coordinación con las 24 jurisdicciones y la participación del sector privado, sustentado en una situación sanitaria con un descenso sostenido en el número de casos de coronavirus.

La decisión forma parte de la estrategia para la recuperación gradual y cuidada de las actividades planificadas desde el gobierno nacional, a partir del avance del proceso de vacunación en todo el país y la favorable situación epidemiológica y sanitaria resultante, informaron fuentes oficiales.

Según datos de la cartera de Salud, desde hace 14 semanas se registra un descenso sostenido del número de casos confirmados y de la ocupación de las camas de terapia intensiva, hecho que se verifica principalmente en los grandes conglomerados urbanos, ya que ninguno de ellos se encuentra en situación de alarma sanitaria.

Hasta hoy, se aplicaron 44.144.690 dosis de vacuna contra la Covid-19 en 28.267.178 personas, de las cuales 15.877.512 ya cuentan con esquema completo, con un 85,1 por ciento de la población mayor de 18 años tenga al menos una dosis de vacuna y el 47,4 por ciento de ese grupo el esquema completo.

En ese contexto, el 73 por ciento de las personas mayores de 50 años, es decir aquellas con mayor riesgo de requerir internación y con mayor mortalidad, ya poseen esquema de vacunación completo.

Durante la reunión del Consejo Federal de Salud que se realizó esta semana en San Luis se planteó la relevancia de sostener el ritmo de vacunación priorizando las segundas dosis, con el objeto de alcanzar aún mejores indicadores para finales de septiembre, de manera de poder sostener el plan de recuperación gradual y cuidada de actividades.

En este sentido, el retorno de la actividad turística en todo el territorio nacional se desarrolla en el marco del cumplimiento de protocolos sanitarios elaborados por el Ministerio de Turismo y Deportes de la Nación en conjunto con el Instituto de Calidad Turística (ICTA), la Cámara Argentina de Turismo (CAT) y las provincias, a través del Consejo Federal de Turismo.

La reactivación de la actividad turística estudiantil de forma responsable y segura se desarrollará bajo un estricto monitoreo de los indicadores epidemiológicos y del cumplimiento de los protocolos sanitarios vigentes, teniendo en cuenta las lecciones aprendidas en la etapa previa, así como también la particularidad de esta actividad, por lo que se implementarán medidas complementarias por parte de las jurisdicciones con el acuerdo de la autoridad sanitaria nacional, indicaron las fuentes.

En el gobierno nacional destacan la importancia que la actividad tiene para distintas localidades del país, que genera una gran cantidad de puestos de trabajo y mejora los niveles de ocupación y de desarrollo de la actividad turística nacional.

También consideran que la posibilidad de re vinculación y reinicio de este tipo de actividades tendrá un impacto positivo en la salud integral de los adolescentes que han realizado un invaluable aporte al cuidado colectivo durante la pandemia, siendo la socialización uno los principales atributos positivos de esta etapa.