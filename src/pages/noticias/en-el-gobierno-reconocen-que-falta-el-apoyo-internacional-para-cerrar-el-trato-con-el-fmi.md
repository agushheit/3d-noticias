---
category: Agenda Ciudadana
date: 2022-01-04T06:00:10-03:00
thumbnail: https://assets.3dnoticias.com.ar/fmno.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: En el Gobierno reconocen que falta el apoyo internacional para cerrar el
  trato con el FMI
title: En el Gobierno reconocen que falta el apoyo internacional para cerrar el trato
  con el FMI
entradilla: De todas maneras esperan lograr el consenso necesario con el avance de
  las negociaciones. Buscan respaldo interno.

---
En el Gobierno nacional admiten que el acuerdo con el Fondo Monetario Internacional (FMI) aún no cuenta con los respaldos necesarios por parte de la comunidad internacional, pero confían en lograrlos con el avance de las negociaciones.

“No está el apoyo pleno de la comunidad internacional. Cada gobierno decidirá cuando expresarse”, afirmó este lunes una calificada fuente del Ministerio de Economía, quien admitió que esa es una de las trabas que aún existe en la negociación.

De allí que se considere relevante la reunión que el Ministro de Economía, Martín Guzmán, encabezará con gobernadores provinciales el próximo miércoles, desde las 17, en el Museo del Bicentenario.

“Vamos a explicar cuál es el estado de la negociación y les informaremos la situación geopolítica actual”, indicaron cerca del jefe del Palacio de Hacienda.

Dada esta situación, funcionarios del Palacio de Hacienda mantendrán durante esta semana reuniones con especiales con empresarios para seguir explicando detalles de las conversaciones con el FMI, y eventualmente las complicaciones que demoran la firma.

La semana que viene será el turno de los jefes de bloques legislativos, quienes luego deberán llevaran adelante la discusión en el Congreso Nacional.

Desde el Palacio de Hacienda aclararon que la reunión con los gobernadores será descriptiva de cómo se está encarando la conversación, pero sin que quede margen para que introduzcan sus percepciones.

“Vamos a comunicar las metas, los criterios de desempeño que se van a tomar, y que nos falta para llegar al acuerdo”, insistieron en la cercanía de Guzmán.

El programa en discusión es un acuerdo de Facilidades Extendidas y tendrá tres tópicos: sendero fiscal, emisión monetaria, y estado de las reservas.

“Estos son los tres puntos centrales sobre los que se basará la presentación del miércoles”, aseveraron las fuentes y agregaron que “será la base del plan plurianual que se presentará”.

En un cambio de visión respecto a programas pasados, el FMI aceptaría incluir a los acuerdos de precios como parte del plan para contener la inflación.

“El FMI está esperando el resultado de las rondas de acuerdos de precios que está llevando adelante el secretario de Comercio, Roberto Feletti”, revelaron las fuentes.

“Es necesario que exista un compromiso de toda la comunidad para llevar adelante una tarea que demandará muchos años. Por eso es importante el acuerdo de precios y que los salarios tengan un sendero nominal normal”, afirmaron.