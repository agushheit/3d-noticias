---
category: La Ciudad
date: 2021-09-23T06:15:49-03:00
thumbnail: https://assets.3dnoticias.com.ar/TUNELCULLEN.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Levantarán la manga que une el Cullen con el Hospital Militar ante la baja
  de casos de Covid
title: Levantarán la manga que une el Cullen con el Hospital Militar ante la baja
  de casos de Covid
entradilla: Lo anticipó el director del nosocomio Juan Pablo Poletti. Los pacientes
  con coronavirus serán atendidos con el hospital Iturraspe viejo.

---
Dada la baja sostenida en el tiempo en los casos de coronavirus y la posibilidad de nuevas flexibilizaciones, además de brindar atención a patologías no Covid que quedaron relegadas en estos 15 meses de pandemia en la ciudad, las autoridades de los distintos nosocomios locales junto a autoridades del Ministerio de Salud están en las tareas de reorganizar el servicio en la ciudad de Santa Fe.

En conferencia de prensa, el director del Hospital Cullen, Juan Pablo Poletti afirmó: "Desde hace un mes y tal cual reflejan las estadísticas en la ciudad y a nivel país, bajó el número de ingresos Covid y ahora estamos reorganizándonos para que el hospital tenga disponibilidad de salas no Covid y dejar lo Covid para aislamiento de sospechosos. Este martes nos reunimos con autoridades del Hospital Iturraspe y el Iturraspe viejo y el ministerio para articular y concentrar los casos positivos de Covid en un solo lugar y dejar a los nosocomios con quirófano central y urgencias-emergencias disponibilidad de camas para patologías que estuvieron relegadas (no Covid) durante estos 15 meses. La idea sería habilitar como terapia intensiva, solo para coronavirus, el Hospital Iturraspe viejo".

Poletti destacó: "Si debemos hablar de la realidad del Hospital Cullen, hasta ayer eran seis los pacientes en sala general y dos en la terapia intensiva que cursan la enfermedad, pero a ello hay que agregarle seis casos más en la terapia de pacientes que teniendo el alta epidemiológica aún necesitan permanecer internados producto de las secuelas".

"Por otra parte y pensando en la reorganización también se está estudiando levantar la manga instalada en Avenida Freyre, pedido realizado por comerciantes de la zona", admitió el director del Cullen. "Ya hablamos con la gente que realiza las tareas de armado de la manga. Pero dejaremos armado en el Liceo el Hospital Militar ante la necesidad de tener disponibilidad de camas para la atención de pacientes", concluyó Poletti.