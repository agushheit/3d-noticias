---
category: Agenda Ciudadana
date: 2021-08-23T06:00:13-03:00
thumbnail: https://assets.3dnoticias.com.ar/RECONQUISTA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Perotti recorrerá los departamentos San Javier, Vera y General Obligado
title: Perotti recorrerá los departamentos San Javier, Vera y General Obligado
entradilla: Visitará obras viales donde se invierten más de $1.500 millones, participará
  de la incorporación de una ambulancia, mantendrá taller con estudiantes y tendrá
  un encuentro con productores, comerciantes y vecinos. .

---
El gobernador de la provincia, Omar Perotti, inicia este lunes una extensa jornada de trabajo en el norte santafesino donde recorrerá los departamentos San Javier, Vera y General Obligado.

En primer lugar, el mandatario santafesino estará en Romang donde, desde las 9.30 horas, participará de la incorporación de una ambulancia de alta complejidad que será destinada al hospital de dicha localidad. Será en la plaza de la ciudad, donde luego tendrá un taller con estudiantes.

En Colonia Sager, recorrerá la habilitación de la pavimentación de la RPN °36 tramo Romang-Vera, etapa 1, donde se invierten 1.030 millones de pesos. La longitud de la obra es de 16,20 kilómetros y en su recorrido alcanza la localidad de Colonia Sager, además de cruzar el arroyo “El Gusano”.

La obra consistió en la construcción de terraplenes excavación de caja, construcción de alcantarillas transversales, alcantarillas laterales de caños de hormigón, demolición y retiro de alcantarillas existentes, rectificación y limpieza de cunetas existentes, construcción de cordón de hormigón, colocación de baranda metálica, señalización vertical, iluminación de enlaces y zona urbana, ejecución de mensuras de parcela, reacondicionamiento de puentes existentes y construcción de casillas para contadores.

A las 11:30 hs, Perotti mantendrá un encuentro con productores, comerciantes y vecinos de la región sobre la RPN °36, en la Plazoleta de Colonia Sager.

**VISITA A DEPARTAMENTO VERA**

A las 13:15 hs, el gobernador recorrerá la habilitación de la pavimentación de la RPN °88-s tramo RPN N°3-Unidad Penitenciaria de Santa Felicia. La obra demandó un monto de $ 473 millones.

La obra consistió en alcantarillas transversales y laterales y pavimento flexible: 5.375 metros sobre RP 88-S y 890 metros dentro del predio - tramo RPNº 3 - Unidad Penitenciaria de Santa Felicia.

**VISITA A RECONQUISTA**

A las 15 hs, Perotti llegará a Reconquista, donde acompañado por el intendente local, Enrique Vallejos, visitará una serie de obras que llevan adelante en la cabecera del departamento General Obligado.