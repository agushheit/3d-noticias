---
layout: Noticia con imagen
author: "Fuente: Diario UNO"
resumen: Regreso a las aulas
category: La Ciudad
title: Dieron precisiones de cómo y cuándo será el regreso a las aulas de manera
  semipresencial
entradilla: La ministra Adriana Cantero dio precisiones de cómo será el cierre
  de ciclo 2020 y el comienzo de 2021 en la provincia. Qué pasará con aquellos
  que están en el último año del primario y del secundario.
date: 2020-11-17T14:27:46.129Z
thumbnail: https://assets.3dnoticias.com.ar/clases-presenciales.jpg
---
Finalmente, este lunes, la ministra de Educación de la provincia, Adriana Cantero brindó más precisiones de cómo será el cierre del ciclo lectivo 2020 y el comienzo del 2021en la provincia de Santa Fe. Fue en una entrevista que brindó en Radio 2 de Rosario donde manifestó: "Estamos trabajando en un calendario superpuesto; es decir, la extensión del ciclo 2020 hasta fines de marzo y el comienzo del ciclo 2021 a partir de la mitad de marzo. Es un año atípico e inédito, por lo tanto, también lo es la finalización y el comienzo de los ciclos”.

“A partir del 17 de febrero todos los chicos que están en los grupos prioritarios, es decir aquellos que cursan 7º grado y 5º y 6º año de la secundaria, van a iniciar una cursada de intensificación pedagógica para cerrar competencias fundamentales y su proceso de evaluación de fin de nivel. Creemos que para mediados de febrero la curva epidemiológica estará mejor y se podrá tener algo de presencialidad”, agregó la ministra.

![](https://assets.3dnoticias.com.ar/adriana-canterojpg.jpg)



Además, este grupo de alumnos va a tener que hacer el curso de intensificación pedagógica, pero “la regularidad con la que irán a tener contacto con sus docentes y las actividades dependerá de cómo cada escuela va evaluando el proceso de sus grupos”, dijo.

También tuvo momento para referirse a los actos de colación que según lo que informó serán en marzo, tras la finalización del ciclo lectivo 2020 y también la curva epidemiológica dirá si ya se van a poder realizar de manera presencial.

Asimismo fue consultada respecto de los demás alumnos y dijo: "Aún todavía no está cerrado, estamos mirando el día a día pero sí debemos decirles a los chicos de primer grado y primer año que deben estar atentos al reinicio de actividades el 17 de febrero, tal cual hacen todos los años quienes tienen que recuperar algún objetivo, ir a clases de consulta u otras actividades, ya que será ahí el momento para trabajar en el fortalecimiento de los aprendizaje fundamentales que vayan a darse en 2021”.

“El ciclo 2021 va a comenzar formalmente a mitad de marzo, es decir que vamos a tener unos días de ese mes donde estaremos iniciando el nuevo ciclo y cerrando evaluaciones del ciclo 2020”, cerró.