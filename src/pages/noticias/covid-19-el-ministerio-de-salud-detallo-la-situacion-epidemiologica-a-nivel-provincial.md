---
category: Estado Real
date: 2021-04-20T06:20:01-03:00
thumbnail: https://assets.3dnoticias.com.ar/enfermeros.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: 'Covid-19: El Ministerio de Salud detalló la situación epidemiológica a nivel
  provincial'
title: 'Covid-19: El Ministerio de Salud detalló la situación epidemiológica a nivel
  provincial'
entradilla: "“Nuestros médicos, enfermeros y personal de salud están dando todo y
  nuestros docentes hacen un gran esfuerzo”, señaló la ministra del área, Sonia Martorano."

---
La ministra de Salud provincial, Sonia Martorano, recorrió este lunes el predio de la ex Rural de la ciudad de Rosario, donde continúa el operativo de vacunación para adultos mayores y brindó detalles sobre la situación epidemiológica por la que está atravesando el territorio santafesino.

“El trabajo de la provincia ha sido muy intenso, optimizando el sistema, vacunando con las dos dosis a los profesionales del área de salud, tanto los de la primera línea de lucha como aquellos que son prioridad como odontólogos, bioquímicos y farmacéuticos. La ocupación de camas críticas es muy alta y la situación es muy compleja. Nuestros médicos, enfermeros y personal de salud están dando todo”, indicó Martorano.

En cuanto al operativo de vacunación, la titular de la cartera sanitaria destacó que “hemos vacunado a los docentes que se inscribieron, y proporcionalmente somos la provincia que más vacunó a su sistema educativo. Además estamos comenzando la vacunación de mayores de 60 y seguimos buscando a adultos mayores de 80 que aún no se inscribieron. Vale recordar que ya realizamos operativos de inoculación en geriátricos y residencias de personas con discapacidad”, continuó.

Asimismo, la ministra añadió que se amplió la capacidad de testeos en toda la provincia y se continúa con campañas de prevención, pero insistió con la necesidad de usar las mascarillas correctamente, que mantengan la distancia social, que se eviten aglomeraciones, y que se laven las manos frecuentemente.

“Aún así los contagios crecen. Hoy muchas camas críticas están ocupadas por contagios prevenibles. Estamos viendo un aumento desproporcionado en los jóvenes a quien les pido racionalidad y cuidado”, dijo preocupada Martorano

A lo que agregó: “Una fiesta clandestina, una juntada en un campo a la madrugada, hoy no son una simple juntada, son peligros inmensos. Les pido a los padres de esos chicos que nos acompañen, y haya firmeza para que la molestia del reclamo de salir de casa no sea reemplazada por una cama en una Unidad de Terapia Intensiva”.

“El pedido también va para adultos que no se cuidan, les pedimos respeto y compromiso, por una acción individual se puede parar el sistema productivo, generar restricciones que afectan a la vida de todos”, concluyó Martorano.

**SITUACIÓN EN EL CENTRO NORTE PROVINCIAL**

Por otra parte, el secretario de Salud santafesino, Jorge Prieto, explicó la situación epidemiológica por la que atraviesa el centro norte provincial: “Nos encontramos preocupados en lo que hace a la ocupación de camas, lo cual varía minuto a minuto, si bien nos encontramos trabajando en una articulación con los efectores de salud privados”.

“Esta segunda ola impacta totalmente diferente a lo que fue el año pasado. Hoy la patología Covid prevalece en las personas jóvenes”, continuó.

A su vez, Prieto dijo que “el llamado que hacemos a la sociedad es que podemos seguir funcionando pero con mayor responsabilidad si nos encontramos en un lugar y dejamos de comer, coloquémonos el barbijo, la ventilación cruzada es una de las medidas principales y recordemos que hoy tenemos la vacuna que a medida que la vamos recibiendo, se van aplicando a la población objetivo”.

“No es el lugar, el sitio, sino lo que ocurre dentro del mismo. El compromiso de todos y todas nos va a ayudar a llegar con el menor número posible de casos y que ninguna persona quede sin atender arriba de una ambulancia, en una guardia hospitalaria o en un sanatorio”, concluyó Prieto.

**LLEGADA DE NUEVAS DOSIS DE VACUNAS**

Por otra parte, la provincia de Santa Fe recibió esta mañana 62.300 vacunas de AstraZeneca las cuales ya fueron repartidas estratégicamente por diferentes localidades.

Este lote se suma a las 7.700 primeras dosis de Sputnik V que arribaron el pasado miércoles 14, mientras se espera la llegada de más vacunas durante los próximos días.