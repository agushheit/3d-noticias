---
category: La Ciudad
date: 2020-12-11T11:54:05Z
thumbnail: https://assets.3dnoticias.com.ar/control.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: El Litoral'
resumen: Quedó firme la cesantía dispuesta a personal de control del Municipio
title: Quedó firme la cesantía dispuesta a personal de control del Municipio
entradilla: Hace dos años, se resolvió apartar de la Dirección a Silvia Cejas, tras
  detectarse irregularidades. En ese momento tenía tutela gremial. Ahora cesó esa
  protección y el intendente Jatón la dejó efectiva.

---
**Todo se hizo público en junio de 2018**, cuando el entonces intendente José Corral inició un sumario administrativo para investigar potenciales irregularidades en la Dirección de Control de Actividades y Vía Pública; separó preventivamente por 90 días a Silvia Cejas, la directora a cargo, e intervino la repartición.

Habían llegado denuncias al Municipio advirtiendo el **mal accionar del personal que se ocupaba de controlar la nocturnidad -locales bailables, eventos, etc.-, los espectáculos públicos, las actividades comerciales y el ordenamiento de la venta ambulante**. Y Fiscalía Municipal elaboró los informes para fundamentar la decisión.

A fines de ese año, el Departamento Ejecutivo dispuso la conclusión del sumario y la aplicación de cesantía prevista en el artículo 62 inc. c) del Estatuto para personal Municipal - Ordenanza N° 8.527. Sin embargo, la efectividad de dicha sanción estaba sujeta a que se excluyera la tutela sindical de la que gozaba Cejas por haber resultado **electa delegada gremial, y cuyo mandato finalizaba el 1 de noviembre de 2019**. Esa protección gremial se extendió un año más, con lo cual ya concluyó.

En consecuencia, y compartiendo el criterio de Fiscalía Municipal, el ahora intendente Emilio Jatón dispuso que se haga efectiva la cesantía de Silvia Cejas y rubricó la correspondiente resolución. Acompañan con su firma el secretario de Gobierno, Nicolás Aimar; y la secretaria de Control y Convivencia Ciudadana, Virginia Coudannes.

# **El rol de Fiscalía Municipal**

En todo momento, los hechos investigados se tramitaron bajo secreto de sumario administrativo, para garantizar así la eficacia de la investigación.

La ordenanza 10.610 establece que la Sindicatura Municipal tiene como competencias a su cargo “emitir informes sobre la gestión y el sistema de control interno vigente en cada jurisdicción y entidad, formulando recomendaciones para su fortalecimiento y mayor eficiencia”, como así también “formular a los órganos comprendidos en su ámbito de competencia recomendaciones tendientes a asegurar el adecuado cumplimiento normativo, la correcta aplicación de las reglas de auditoría interna y de los criterios de economía, eficiencia y eficacia”. 

Además, debe “poner en conocimiento del Intendente y de los titulares de las jurisdicciones los actos que hubieren acarreado o estime puedan acarrear perjuicios para el patrimonio público” y “vigilar el cumplimiento de la normativa legal y de las normas emitidas por los órganos rectores del sistema de administración financiera”.