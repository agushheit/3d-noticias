---
category: Agenda Ciudadana
date: 2021-09-16T06:00:16-03:00
thumbnail: https://assets.3dnoticias.com.ar/BACHEOJUEVES16.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: Trabajos de bacheo previstos para este jueves
title: Trabajos de bacheo previstos para este jueves
entradilla: La Municipalidad informa las calles por las que avanza el cronograma de
  tareas previsto en el Plan de Bacheo 2021. Puede haber cortes de circulación y desvíos
  de colectivos a medida que se desarrolla la obra.

---
**Actualmente se trabaja en:**

Almirante Brown y Padre Genesio, sentido sur-norte

General Lopez al 3200

Necochea y Rosalía de Castro

Necochea y Sargento Cabral

Necochea e Ituzaingó

Marcial Candioti y Gobernador Candioti

El municipio informa que todas las tareas se realizan siempre en sentido del tránsito. Puede haber cortes de circulación y desvíos de colectivos, a medida que avanzan las obras y se realizan intervenciones mayores. Además, se detalla que los trabajos están sujetos a las condiciones climáticas.

**Avenida Freyre**

Por tareas relacionadas con el funcionamiento del hospital reubicable, permanece interrumpida la circulación vehicular en ambas manos de avenida Freyre al 2100, tanto para peatones como para el tránsito vehicular.

**Esto implica el desvío de las siguientes líneas de colectivos:**

Línea 9: de recorrido habitual por Mendoza a Saavedra, Juan de Garay, 4 de Enero y Mendoza, a recorrido habitual

Línea 14: de recorrido habitual por avenida Freyre a Primera Junta, Zavalla, Mendoza, Saavedra y Monseñor Zaspe, a recorrido habitual

Línea 16: de recorrido habitual por avenida Freyre a Moreno, Francia, Primera Junta y San Lorenzo, a recorrido habitual

**Por otro lado, las paradas serán:**

Línea 9: avenida Freyre y Primera Junta, avenida Freyre y Mendoza, Mendoza y Saavedra, Saavedra y Lisandro de la Torre, y Juan de Garay y Francia

Línea 14: Mendoza y avenida Freyre, Mendoza y Saavedra, Saavedra y Lisandro de la Torre , Saavedra y Juan de Garay, Saavedra y Moreno, y Monseñor Zaspe y San Lorenzo

Línea 16: avenida Freyre y Moreno, Saavedra y Moreno, Francia y Corrientes, Francia y Lisandro de la Torre, Francia y Mendoza, y Primera Junta y San Lorenzo

**Operativos de vacunación**

La Municipalidad comunica que, en el marco del operativo de vacunación que se realiza en la ciudad, habrá controles en el tránsito vehicular en las inmediaciones de:

La Redonda, Arte y Vida Cotidiana, en Salvador del Carril y Belgrano, de 7 a 16 horas

La Esquina Encendida, en Estanislao Zeballos y Facundo Zuviría, de 7 a 18 horas

El Hospital de Niños “Dr. Orlando Alassia”, en Mendoza al 4100, de 8 a 12 horas

Del mismo modo, se recuerda que continúan las tareas en torno al ex Hospital Iturraspe (avenida Perón y bulevar Pellegrini) y el Cemafe (Mendoza al 2400).