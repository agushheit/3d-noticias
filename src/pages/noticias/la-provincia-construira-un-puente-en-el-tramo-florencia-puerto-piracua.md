---
category: Estado Real
date: 2021-08-17T06:00:18-03:00
thumbnail: https://assets.3dnoticias.com.ar/PUERTO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La provincia construirá un puente en el tramo Florencia - Puerto Piracuá
title: La provincia construirá un puente en el tramo Florencia - Puerto Piracuá
entradilla: Con una inversión de más de 124 millones de pesos, la estructura tendrá
  32 metros de luz y estará compuesta de hormigón y acero. La licitación se realizará
  el miércoles desde las 17 horas.

---
La Dirección Provincial de Vialidad que está bajo la órbita del Ministerio de Infraestructura, Servicios Públicos y Hábitat, realizará el miércoles 18 de agosto a las 17 horas, la apertura de sobres con ofertas para la construcción de un nuevo puente sobre Arroyo "El Cinco", ubicado en el departamento General Obligado, unos 23 kilómetros al Este de la ciudad de Florencia, en la conexión con el complejo turístico de Puerto Piracuá.

El nuevo puente se emplazará unos 180 metros al sur del puente de madera existente, que tiene 27 metros de luz y un ancho de 3,5 metros, por estas razones, la obra implica realizar desvíos en la traza actual. El proyecto contempla la demolición del puente de madera existente, así como la limpieza y retiro de especies arbóreas en el cauce.

Sobre las obras, el administrador de Vialidad Provincial, Oscar Ceschi, analizó: "Este tipo de obras son fundamentales para los fines turísticos que nos plantea el gobierno de Omar Perotti. Analizamos que lo mejor sería construir esta estructura que mejorará el acceso a Puerto Piracuá, un lugar con una naturaleza única en el extremo noreste de nuestra provincia". Y agregó: "En los últimos meses más allá de pavimentaciones y reparaciones de rutas, incorporamos este tipo de obras que dan infraestructura a muchos lugares que lo necesitan para desarrollar sus actividades económicas con mayor tranquilidad y seguridad, y fortalecer el turismo provincial".

**La obra**

Con un presupuesto oficial de 124.110.553,46 y un plazo de ejecución 12 meses, el puente proyectado es de tipo mixto (hormigón y acero) y tendrá una luz de 32,28 metros compuesta por cuatro tramos de 8,07 metros. El ancho interior es de 8,30 metros y consta de una defensa vehicular en cada lateral, completando un ancho total de 9,14 metros.

La estructura, estará constituida por 7 vigas longitudinales principales y un tablero de hormigón armado con cemento ARS (de Alta Resistencia a los Sulfatos) de 20 centímetros de espesor. Del mismo material, se construirán dos pilotes-columnas fundados a una profundidad que permita absorber el empuje de los suelos, cargas verticales y de frenado. Los estribos, están preparados para recibir prolongaciones de tramos para posibilitar adaptaciones constructivas, y estarán protegidos mediante una protección flexible de hormigón con geoceldas, para evitar la formación de socavones.