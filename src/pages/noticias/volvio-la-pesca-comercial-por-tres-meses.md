---
category: Agenda Ciudadana
date: 2021-01-12T09:47:10Z
thumbnail: https://assets.3dnoticias.com.ar/12121-pesca.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: '3DNoticias - Fuente: El Litoral'
resumen: Volvió la pesca comercial por tres meses
title: Volvió la pesca comercial por tres meses
entradilla: 'El secretario de Agroalimentos, Jorge Torelli, confirmó que volvió la
  pesca comercial limitada a tres días: lunes, miércoles y jueves como propuso el
  gobierno provincial.'

---
«La actividad volvió con las observaciones judiciales y con aumento de controles por parte del Ministerio de la Producción, Ciencia y Tecnología» señaló el funcionario este lunes. **La medida pone en paralelo a Santa Fe con la vecina provincia de Entre Ríos.**

Cabe recordar que la cautelar revocada por la Cámara validó también la pesca deportiva con devolución, reclamo también presentado por las autoridades provinciales.

La pesca comercial durante los tres primeros meses del año llevará además la realización de un estudio sobre la situación del recurso ictícola para luego tomar las determinaciones de la actividad para el resto del año.

La Cámara de Apelaciones de feria de Rosario revocó parcialmente la cautelar dictada en primera instancia ante el planteo del gobierno provincial. Allí resolvió **dejar sin efecto la veda para la pesca deportiva** y morigeró la comercial dejándola firme para martes, viernes, sábados, domingos y feriados. 

También la Cámara exhortó al Ministerio de Producción a los efectos de que realice el máximo esfuerzo posible en el ejercicio de la competencia de control y fiscalización del recurso pesquero.

Torelli dijo que «el Ministerio de Producción, Ciencia y Tecnología venía intensificando los controles a través de puesta en valor de los puestos de fiscalización, donde el pescador entrega su pescado y se genera un documento, que es la guía que permite entrar luego a los frigoríficos, para que lo que entra sea lo mismo que lo que se certifica».

Más adelante acotó: «veníamos trabajando con las fuerzas de seguridad provinciales en los distintos retenes, controlando no solamente la documentación, sino la carga y las medidas (de los pescados). Tenemos casi 700 kilómetros de costa y es complicado controlarla toda, pero recibiendo las denuncias podemos constatar si existe un ilícito y tomar acción al respecto. Y vamos a reforzar eso por pedido del juez».

En el año 2019, la exportación de pescados de río desde Santa Fe totalizó 33.745.000 dólares producto de más de 28 millones de kilos de producto que tuvieron como destino Colombia (44%), Bolivia (39), Brasil (11), Nigeria (4) y el resto a otros destinos.

En cambio, en los diez primeros meses del año pasado lo vendido al exterior hasta octubre fue de 13.860.000 dólares, producto de haber embarcado 15.240.000 kilos de especies para Bolivia (48%), Colombia (36) y Brasil (11), en los primeros lugares.