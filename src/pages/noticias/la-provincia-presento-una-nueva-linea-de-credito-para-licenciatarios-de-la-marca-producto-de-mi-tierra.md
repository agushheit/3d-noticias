---
category: Estado Real
date: 2021-09-25T06:00:12-03:00
thumbnail: https://assets.3dnoticias.com.ar/mitierra.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La provincia presentó una nueva linea de crédito para licenciatarios de la
  marca Producto de mi Tierra
title: La provincia presentó una nueva linea de crédito para licenciatarios de la
  marca Producto de mi Tierra
entradilla: Con el objetivo de fortalecer a las pequeñas pymes agroalimentarias.

---
El Gobierno de la provincia, a través de la Secretaría de Desarrollo Territorial y Arraigo del Ministerio de Producción, Ciencia y Tecnología, presentó este viernes una nueva línea de créditos destinada a licenciatarios de la marca “Producto de Mi Tierra”.

Al respecto, el secretario de Desarrollo Territorial y Arraigo, Fabricio Medina, valoró que “la línea de financiamiento apunta a fortalecer a las pequeñas pymes agroalimentarias licenciatarios de la marca “Producto de Mi Tierra”, como así también a aquellos locales comercializadores de la marca que ven y detectan un diferencial en estos productos fortaleciendo así la industria familiar. Apuntamos a consolidar este tipo de unidades productivas es por eso que ponemos a disposición esta línea de financiamiento acorde a las necesidades que ellos tenían”.

Asimismo, Medina precisó que “esto surge de un relevamiento que hemos hecho con todos los emprendimientos. El financiamiento se orientará a adquirir maquinarias, equipo y bienes para aumentar su capacidad productiva y, en algunos casos también enfocados a adquirir capital de trabajo para fortalecer las unidades productivas”.

Por último, el funcionario manifestó que “desde el Ministerio de Producción, Ciencia y Tecnología venimos llevando adelante una fuerte articulación con cada una de estas empresas en estrategias de comercialización de sus productos a través de distintas páginas de comercialización”.

**LÍNEA DE CRÉDITO**  
La línea será articulada con la Fundación ArgenINTA y el Instituto Nacional de Tecnología Agropecuaria (INTA). Asimismo el financiamiento proviene de un fondo rotatorio y cuando los créditos sean devueltos, existirá la posibilidad de otorgar nuevos créditos a más beneficiarios.

Este financiamiento apunta a aquellos emprendimientos productivos y/o comerciales, constituidos como personas físicas o jurídicas, bajo algunas de las siguientes modalidades: Emprendimientos que poseen la licencia habilitante de la marca “Producto de Mi Tierra. Provincia de Santa Fe”, o bien que se encuentren inscriptos para la obtención de la citada licencia, según Resolución N° 353/15, sin límite de facturación anual. También, empresas comercializadoras de productos que se encuentran vinculadas de manera oficial a la marca “Producto de Mi Tierra, Provincia de Santa Fe”, mediante el canal de venta instrumentado en el programa “Góndolas locales”, sin límite de facturación anual.

El monto máximo a otorgar se determinará en función de las características del proyecto y de los ingresos del solicitante, mientras que la suma a solicitar tendrá un límite de acuerdo a cada categoría:

>> Categoría 1: Hasta $ 300.000 para créditos con una amortización total a ser realizada en un período de hasta 24 meses, contados desde la fecha de desembolso.

>> Categoría 2: Hasta $150.000 para créditos con una amortización total a ser realizada en un período de hasta 24 meses, contados desde la fecha de desembolso.

En todos los casos, el monto máximo del crédito estará supeditado al cumplimiento de una relación cuota/ingreso que no podrá ser superior al 25%. En el caso de las personas físicas y/o jurídicas registradas en el régimen de Monotributo, la justificación de ingresos deberá ser realizada mediante la presentación de la constancia vigente en AFIP. De manera complementaria, podrá presentarse una acreditación de ingresos por actividades en relación de dependencia mediante la presentación de los últimos tres (3) recibos de sueldo.

**MARCA PRODUCTO DE MI TIERRA**  
“Productos de mi Tierra” es una marca colectiva que otorga el Estado provincial a las producciones agroalimentarias destacadas, realizadas por micro y pequeñas agroindustrias, así como también de la economía social, teniendo por objetivo principal promocionar y potenciar el trabajo de los productores santafesinos y santafesinas.