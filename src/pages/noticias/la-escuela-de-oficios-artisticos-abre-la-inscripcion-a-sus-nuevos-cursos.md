---
category: Estado Real
date: 2021-02-18T07:17:36-03:00
thumbnail: https://assets.3dnoticias.com.ar/cursos.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: La Escuela de Oficios Artísticos abre la inscripción a sus nuevos cursos
title: La Escuela de Oficios Artísticos abre la inscripción a sus nuevos cursos
entradilla: El programa de formación impulsado por el Ministerio de Cultura de la
  provincia de Santa Fe, en conjunto con la Universidad Tecnológica Nacional, brindará
  cursos de Ecodiseño, Maquillaje Artístico y Sonido.

---
Entre el 17 y el 28 de febrero, la Escuela de Oficios Artísticos mantendrá abierta la inscripción a sus nuevos cursos: Ecodiseño, Maquillaje Artístico y Sonido. La convocatoria a la segunda etapa de cursos de formación está destinada a toda la población mayor de 18 años con residencia en la provincia de Santa Fe a la fecha de apertura de la presente convocatoria. La inscripción a los cursos puede realizarse a través de la web www.industriascreativas.gob.ar.

La Escuela de Oficios Artísticos es un programa impulsado por el Ministerio de Cultura de la Provincia de Santa Fe en conjunto con la Universidad Tecnológica Nacional (UTN) y financiado por el Consejo Federal de Inversiones. Se trata de una escuela pública y gratuita con cursos de formación en Producción, Técnica, Comunicación, Arte y Diseño para proyectos culturales y artísticos de la Provincia de Santa Fe. La Escuela se desarrolla de manera virtual, siempre con la finalidad de contribuir a la profesionalización de trabajadores en la producción de bienes y servicios culturales. A través de esta propuesta, busca originar la infraestructura necesaria para la formación en oficios culturales con un sentido federal e inclusivo, intentando llegar a la mayor cantidad de habitantes de la provincia de Santa Fe.

La Escuela de Oficios Artísticos plantea la realización de cursos de 8 clases de duración, con un fuerte anclaje territorial: cada uno de los 19 departamentos de la provincia cuenta con un aula virtual de la que participan hasta 50 estudiantes. De este modo, en cada curso participan 950 estudiantes, en un proyecto de formación que cuenta con resolución ministerial y certificación de UTN.

El programa aborda contenidos multidisciplinarios con gran diversidad de lenguajes que se articulan para crear productos y experiencias más fructíferas. De esta manera, aporta a la formación de trabajadores competentes en saberes técnicos específicos e implicados en el proceso creativo general.

El principal objetivo es conjugar el oficio y el arte para enriquecer el acervo cultural en todo el territorio provincial.

**SOBRE LOS CURSOS**

Luego de una primera etapa de formación desarrollada entre octubre y diciembre de 2020 (con los cursos Estrategias de Comunicación Cultural, Producción de eventos culturales y Vestuario Escénico), la Escuela de Oficios Artísticos desarrollará una segunda etapa con tres propuestas que se llevarán adelante entre marzo y abril de 2021:

**>> Sonido**

A cargo de Jorge Ojeda y Guillermo Palena, esta propuesta está orientada a personas interesadas en aprender técnicas vinculadas con la producción de sonido y la operación, tanto para quienes gestionen espacios artísticos, culturales o lugares donde se realicen otro tipo de eventos. Brinda herramientas básicas del oficio, tales como el manejo de consolas y sistemas específicos de sonido de espectáculos. Aporta también elementos prácticos y teóricos basados en conocimientos adquiridos en la experiencia y práctica del oficio para introducir a los estudiantes en el conocimiento sobre el trabajo, las áreas, los componentes y todo lo que comprende el montaje del sonido en vivo.

**>> Maquillaje Artístico**

Este curso, dirigido a maquilladores en general y personas sin conocimiento previo sobre el oficio, hace un recorrido por los orígenes del maquillaje, su evolución a través de las décadas y los estilos que marcan tendencia hasta la actualidad. Se abordan leyes y técnicas para comenzar a maquillar con materiales pertinentes y se trabaja sobre la importancia y el cuidado de la piel. Se recorren las distintas etapas en la caracterización de personajes para teatro, cine, TV o pasarelas. Además, profundiza sobre técnicas de visagismo, colorimetría, cosmetología, artes escénicas, iluminación y efectos especiales.

_Capacitadora:_ Yamila Gutierrez

**>> Ecodiseño**

A cargo de la capacitadora Romina Palma, se trata de una propuesta integral destinada a todo público: desde educadores que quieran aprender nuevas habilidades, especialistas de energías renovables, empresarios y emprendedores que se interesan en la responsabilidad social y ambiental en el desarrollo ético y sostenible, trabajadores/as sociales, profesionales y estudiantes con interés por el diseño sostenible.

El curso ofrece herramientas integrales para aquellas personas que se dedican a la producción de bienes y servicios: diseñadores, artesanos, trabajadores de ámbitos vinculados a lo textil, al trabajo con madera, vidrios u otros elementos como producción de instrumentos musicales, de vestuarios, trabajadores de la industria editorial o artesanal. En este sentido, propone una perspectiva que integre saberes, fortalezca acciones culturales para el desarrollo de hábitats y comunidades sostenibles, genere un pensamiento crítico sobre la diversidad cultural y las diferentes perspectivas que se pueden abordar para encontrar soluciones comunitarias, agregando así valor a su propio trayecto profesional además de a sus productos.

Desde una perspectiva de género, se propone brindar herramientas para trabajar en equipo, aportar al reconocimiento de los mejores instrumentos para el desarrollo de cada producto en términos de sustentabilidad y empleabilidad, generar habilidades para fortalecer los entramados productivos de manera equitativa para proyectar así nuevos hábitos culturales para un vivir sostenible.

Los estudiantes podrán comprender las ventajas que ofrecen las TIC y utilizarlas de forma adecuada; generar capacidades locales para poner en marcha procesos de transición hacia una cultura sostenible integral, tanto a pequeña escala como a nivel de grandes comunidades biorregionales y organizaciones.

Entre los contenidos principales del curso se encuentran los diversos materiales en los que el Ecodiseño se puede aplicar (plástica, textil, madera, papel, etc.) y el impacto que tienen en el medio ambiente, economía circular, metodologías sostenibles, trabajo en red, empleos verdes, marcos de referencia y desarrollo de proyectos.