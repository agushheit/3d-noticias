---
layout: Noticia con imagen
author: .
resumen: Jardines de Infantes Santa Fe
category: La Ciudad
title: "Carpa negra frente al municipio: los jardines buscan una respuesta"
entradilla: La Agrupación de Jardines de Infantes de Santa Fe reactivó su plan
  de lucha ante la falta de respuestas por parte de los gobiernos provincial y
  municipal.
date: 2020-11-03T19:14:56.497Z
thumbnail: https://assets.3dnoticias.com.ar/carpa-negra.jpg
url: ""
---
En la mañana del martes, representantes de Jardines de Infantes instalaron una carpa negra en la explanada del municipio exigiendo una respuesta al pedido de asistencia económica y retorno a las actividades. La misma será trasladada frente a casa de gobierno el próximo jueves.

A casi ocho meses de inactividad producto de la pandemia, la ayuda que reciben por parte del estado es insuficiente. Los aportes que realiza el gobierno provincial están atrasados y desde el municipio continúan negándose a la posibilidad de apertura. En cuanto a la ayuda prometida por parte del Municipio, hasta el momento sólo se realizaron los relevamientos pero no hubo novedades en cuanto a los montos ni las fechas de pago.

Fueron recibidos por el Secretario de Educación y Cultura Paulo Ricci con quien evaluaron la propuesta de asistencia por parte del ejecutivo municipal. "El diálogo fue muy productivo, pudimos aclarar varios puntos sobre el subsidio que propuso el municipio para los jardines maternales. En realidad no es un subsidio, es una especie de contrato en donde los jardines devolvemos con matrículas el dinero con el que nos van a ayudar." sostuvo Fernando Collado representante del sector. "Se acordó que el número de matrículas no sería fijo sino acorde a los espacios con los que cuenta cada institución pero podrán ser hasta 5 alumnos por establecimiento."

Consultados sobre los pormenores del convenio, Collado comentó que "vamos a discutir varios anexos para agregar a este decreto. Si bien las matrículas podrán ser utilizadas en el año lectivo 2021 y 2022 nuestra prioridad hoy es salvar a los jardines luego de un año muy complicado y teniendo en cuenta que el próximo año también será difícil.

Por su parte, Claudia Rodríguez, integrante de la agrupación de Jardines sostuvo que "fue un encuentro positivo. Una de las cuestiones que fue planteada y que vamos a conversar con la provincia es la habilitación del Sr. Gobernador a las actividades deportivas para menores de 12 años. Hoy nos encontramos con que muchos espacios fueron habilitados para trabajar en forma de taller que es una de las propuestas de trabajo de los jardines para la temporada de verano pero con actividades artísticas, culturales y deportivas. Les pedimos que nos ayuden a encontrarle la vuelta para que, controles las condiciones y bajo los protocolos adecuados, todos podamos trabajar."

El municipio se comprometió a acompañar y apoyar este último pedido el próximo jueves cuando el gobierno provincial reciba al sector.

**La propuesta del Municipio**

El pasado 15 de octubre, el Concejo Municipal de Santa Fe aprobó un programa de ayuda municipal para sectores afectados por la pandemia.

En cuanto al acompañamiento a instituciones educativas, deportivas y culturales entre otros se estableció un subsidio mensual para Jardines Maternales y de Infantes de Gestión Particular, de lo equivalente al 30% del sueldo mínimo de empleados de comercio por cada trabajador registrado que realice tareas de educación o cuidado de niños y niñas. Se otorgará hasta el reinicio de las actividades presenciales con niños y niñas, suspendidas por razones sanitarias.

Por otra parte, se fijó un subsidio mensual de $15.000 por tres meses para Centros Culturales habilitados cuya actividad exclusiva sea el dictado de cursos, talleres, clases, seminarios y/o actividades de carácter educativo y/o formativo relacionadas con cualquier disciplina artística y que a su vez ofrezcan presentaciones de espectáculos en vivo de artes performáticas (música, actuación, danza, circo, etc.) y exposiciones o exhibiciones de artes visuales, de diseño y audiovisuales.\
\
Asimismo, se dispuso un subsidio de $50.000 dividido en dos cuotas mensuales y consecutivas para los Clubes Deportivos de la ciudad de Santa Fe que cuenten con personería jurídica y menos de cinco mil socios.\
\
También se hará una transferencia a las prestatarias del Servicio de Transporte Público de Pasajeros en Colectivos una suma equivalente a la cantidad de boletos emitidos e informados por el SUBE en el ejercicio anterior, actualizados a la tarifa vigente al dictado del presente y en proporción al beneficio previsto para el Boleto Educativo Municipal previsto en la Ordenanza N° 12.620.