---
category: La Ciudad
date: 2022-01-20T06:15:32-03:00
thumbnail: https://assets.3dnoticias.com.ar/kayak.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Disfrutá el verano en Santa Fe Capital: 3×2 en alojamiento y descuentos
  en prestaciones turísticas'
title: 'Disfrutá el verano en Santa Fe Capital: 3×2 en alojamiento y descuentos en
  prestaciones turísticas'
entradilla: Gastronomía, alfajores santafesinos, heladerías, excursiones, paradores
  de playa y actividades náuticas, forman parte de las ofertas.

---
La Municipalidad y la Asociación Empresaria Hotelera Gastronómica diseñaron un esquema de beneficios orientados a turistas que se alojen en la ciudad de Santa Fe durante los meses de enero y febrero. 

Con la premisa de potenciar la llegada de turistas a la ciudad de Santa Fe durante los meses de verano, se pone en funcionamiento un esquema de beneficios en hoteles, gastronomía, alfajores santafesinos, heladerías, excursiones, paradores de playa y actividades náuticas en Kayak y SUP. Los turistas que contraten los hoteles adheridos, se alojan 3 noches y pagan 2, tendrán además beneficios en prestaciones turísticas durante la estadía.

Matías Schmüth, secretario de Producción y Desarrollo Económico de la municipalidad, detalló que hasta el momento son 8 los hoteles de 3 y 4 estrellas que se adhirieron a la promoción. En ese sentido, los turistas alojados en esos espacios tendrán un voucher para acceder a los descuentos de los otros sectores mencionados. “Se trata de actividades que también queremos potenciar y que son importantes para el desarrollo del turismo, como la gastronomía, alfajorerías y heladerías, donde tendrán un 15 por ciento de descuento en los locales adheridos, los paradores de playa que brindarán equipamiento gratuito, actividades náuticas, que han crecido mucho que también brindarán un 15 por ciento de descuento en sus clases”, indicó Schmüth.

**Potenciar el turismo**

En consonancia, Rodrigo Gayá, presidente de la Asociación Empresaria Hotelera Gastronómica Santa Fe (AEHG ) manifestó: “Para el sector hotelero gastronómico es importante instrumentar estrategias que potencien o permitan la captación de turistas en la temporada estival, en este sentido nuestros asociados han realizado un esfuerzo para consolidar las promociones que se han trabajado en conjunto con la Municipalidad de Santa Fe y con la expectativa de incrementar la llegada de turistas, en este difícil momento que está atravesando nuestra actividad”.

Con respecto al trabajo conjunto entre el sector público y el sector privado Gayá indicó: “hoy en día el sector privado no puede ir sino está de la mano del sector público, lo público es el apoyo, es la herramienta para que nosotros podamos avanzar, es fundamental”, concluyó.

Por otra parte, Alejandro Grandinetti, secretario de Turismo de la Provincia, valoró la iniciativa y destacó que se trata de una medida “que tiende a integrar a todos los sectores, es una excelente decisión del Municipio”.

**Comercios adheridos**

Además se ofrecen descuentos en las empresas de viaje que hacen turismo receptivo: Costanera 241 “Mira Santa Fe” (+54 342 5950508).

Las promociones están vigentes desde hoy hasta fines de febrero. Los interesados podrán acceder al detalle de hoteles y prestadores adheridos consultando los portales [www.santafenoticias.gob.ar](http://www.santafenoticias.gob.ar/); [www.aehgsantafe.org.ar](http://www.aehgsantafe.org.ar/) y el IG: @turismosantafecapital.

Los Hoteles adheridos hasta el momento son: Hotel Los Silos (Dique 1 Puerto de Santa Fe), Hostal Santa Fe (San Martín 2954), Hotel Conquistador (25 de Mayo 2676), Hotel Amarras (Dique 2 Puerto de Santa Fe), Hotel Castelar (25 de Mayo 2349), Zavaleta Hotel (Hipolito Yrigoyen 2349), Hotel Suipacha (Suipacha 2375) y Hotel Bertaina (Hipólito Irigoyen 2255).

En tanto, los bares y restaurantes que ofrecen un 15% de descuento a los turistas que contraten la promoción hotelera son: La City Sport (La Rioja 2609), Liverpool Bar (Castellanos 2269), Huaw Bar (Rivadavia 3469), 1980 Boulevard (Bv, Gálvez 2281), 1980 Costanera Este, Restaurante Ateneo Inmaculada (Cruz Roja Argentina 1565), Costa Litoral (Dique 1 Puerto de Santa Fe), Choperia Santa Fe (San Jerónimo 3498), Restaurante España (San Martín 2644).

Los comercios de alfajores (15% turistas) son Gayalí (San Martín 2898) y Merengo (General López 2618) y las actividades náuticas (15% Turistas) son: Setúbal Kayaks (Playa Grande-Costanera Este) y SUP Santa Fe (Playa Grande-Costanera Este).

En tanto, las heladerías que contarán con promociones para turistas son: Necochea (Bv. Gálvez 1945), Veneto (9 de Julio 3499), Danubio (25 de Mayo 3198), Carmiel (Bv. Pellegrini 1218), San Remo (Av. Freyre 3201), Portobello (Aristóbulo del Valle 6801), Bariloche (Lisandro de la Torre esq. Francia) , A la Americana (Gral. López 2987).