---
category: La Ciudad
date: 2021-07-11T06:15:04-03:00
thumbnail: https://assets.3dnoticias.com.ar/BAJANTE.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: El Río Paraná llegaría en 10 días a los 10 cm en Santa Fe
title: El Río Paraná llegaría en 10 días a los 10 cm en Santa Fe
entradilla: Este sábado midió 30 cm en el puerto de la capital provincial. De concretarse
  el pronóstico del INA, sería la altura más baja de los últimos 50 años. El panorama
  en la región.

---
Desde hace semanas el Instituto Nacional del Agua viene advirtiendo que julio es un mes “crítico” en relación a la bajante histórica que atraviesa el Río Paraná. Por estos días, el nivel vuelve a descender y llegaría a los 10 cm en 10 días.

Este sábado al mediodía, el hidrómetro ubicado en el Puerto de la ciudad de Santa Fe midió 30 cm, cuatro menos que en la madrugada del viernes y diez menos que el lunes (40 cm).

La marca más baja del año y de las últimas cinco décadas fue 22 cm, la altura que se registró el 28 de junio pasado. Esa medida superó la alcanzada en 2020 que fue de 44 cm en el mes de mayo.

**No caminar por la Setúbal**

La poca agua que presenta por estos días el Río Paraná y sus afluentes hacen que se modifiquen los paisajes ribereños de la región. En la capital santafesina, la Laguna Setúbal se muestra con extensas orillas y el suelo lagunar (mal llamado banco de arena).

Autoridades municipales, la Prefectura e incluso los especialistas que estudian el comportamiento de los ríos, advirtieron que es muy peligroso transitar a pie o en algún vehículo por el suelo de la Setúbal.

Como explicó en notas anteriores el geólogo santafesino, Carlos Ramonell ese suelo no es homogéneo, sino que presenta zonas con sedimentos antiguos y resistentes y otras con fangos muy blandos de depositación reciente. De este modo, puede ocurrir que se esté transitando cómodamente en terreno firme y de repente caer en pozos de dragado.

**ROSARIO**

En la ciudad de Rosario este sábado el Río Paraná midió 15 cm y para los próximos días, desde el INA esperan que siga bajando. 7 cm para el 13 de julio y -0,09 mts para el 20 de este mes.

**PARANÁ**

En la ciudad de Paraná el panorama es similar. Este sábado midió por debajo del cero: -0,02 mts. Desde el INA pronostican que para el 13 de julio descienda a -0,07 mts y para el 20 de este mes - 0,20 mts