---
category: Agenda Ciudadana
date: 2021-09-27T06:00:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/PRESENCIALES.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: La provincia entre las de mayor presencialidad en escuelas
title: La provincia entre las de mayor presencialidad en escuelas
entradilla: Según el Ministerio de Educación, esa modalidad, sin burbujas, ya se implementó
  en el 96% de las escuelas santafesinas y apunta a sumar establecimientos.

---
Santa Fe alcanzó la presencialidad plena de los estudiantes en el 96 por ciento de las escuelas de la provincia, y apunta a sumar a los establecimientos remanentes junto al Ministerio de Salud, informó la cartera educativa provincial.

"Al día de hoy tenemos el 96 por ciento de las escuelas con ese nivel de presencialidad", dijo la ministra del área, Adriana Cantero, acerca del cursado de clases sin burbujas.

La funcionara se refirió a la reunión que mantuvieron ayer los ministros de educación de las provincias en el Consejo Federal, que acordó avanzar en el objetivo de la presencialidad plena en todo el país.

Cantero explicó que "eso no es una novedad para Santa Fe" porque el 96 por ciento de las escuelas ya pudo implementarla, y el porcentaje restante está relacionado con establecimientos de grandes ciudades, como Rosario, con alta matrícula.

"Trabajamos mucho con Salud para aproximarles a las escuelas que restan, las que aún siguen en bimodalidad, los monitoreos, los testeos y los cuidados que exigen los protocolos", dijo Cantero.

La funcionaria agregó: "Nuestra intención es producir esa asistencia" plena en la totalidad de las escuelas santafesinas.

"Estamos muy próximos a llegar al ciento por ciento de presencialidad plena. Somos una de las provincias que más presencialidad ha logrado y no vamos a abandonar ese cuidado", añadió la ministra en declaraciones radiales.

Y explicó que el 4 por ciento que aún no logró ese objetivo se explica por "escuelas de alta matrícula en grandes ciudades y con espacios que, en proporción, tienen dificultades para sostener el distanciamiento" entre alumnos.

A la vez, Cantero ratificó que se mantendrá "el uso obligatorio del tapabocas, porque se trata de un espacio público que reúne gran cantidad de personas".