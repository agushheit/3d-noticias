---
category: Estado Real
date: 2021-07-22T06:00:17-03:00
thumbnail: https://assets.3dnoticias.com.ar/TOSTADO.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La provincia realizará obras hídricas y viviendas para Tostado
title: La provincia realizará obras hídricas y viviendas para Tostado
entradilla: El Ministerio de Infraestructura, Servicios Públicos y Hábitat llevó adelante
  las licitaciones para las obras hídricas de reconstrucción de 85 alcantarillas y
  para construir 45 viviendas en Tostado.

---
Al respecto, la ministra de Infraestructura, Silvina Frana, señaló: "Esto forma parte de programas de viviendas tanto provinciales como nacionales que llevamos adelante para integrar y equilibrar el territorio. Esto se enmarca en una fuerte decisión de hacer una apuesta a la obra pública para responder a demandas, algunas más nuevas, otras más históricas. Y porque además, cuando uno invierte en obra pública moviliza la economía, y genera empleo. Y eso la verdad que en este momento tan difícil que le tocó atravesar al mundo entero, es una decisión que va a ayudar a que se recupere la economía en forma más rápida”.

“La intervención en los Bajos Submeridionales es una política de Estado, así lo definió el gobernador Omar Perotti y el presidente Alberto Fernández, que posteriormente quedó plasmado en una agenda de trabajo. Y desde la provincia de Santa Fe, tanto el ministerio de Infraestructura, como el de Producción, y el ministerio de Cambio Climático, están participando en esta agenda de trabajo, y siempre en el marco de entender un objetivo de desarrollo sostenible, y de aquellas cosas que potencian el esquema productivo y sustentan el objetivo de arraigo, para que la gente pueda desarrollarse en el lugar donde nació y vive”, concluyó la ministra Frana.

Por su parte, el ministro de Producción, Ciencia y Tecnología, Daniel Costamagna, haciendo un balance de su visita al departamento 9 de Julio, que incluyó el acto de entrega de una motoniveladora, la recorrida por una desmotadora de algodón en Gregoria Perez de Denis y la participación en el remate ganadero en Tostado, destacó: “Fue un día de mucha alegría, de mucho trabajo y de mucho compromiso".

Para concluir, el ministro se refirió a la licitación para la reconstrucción de alcantarillas en RP N°13: "Desde el punto de vista de lo productivo me pone muy orgulloso estar trabajando sobre la ruta 77 y la ruta 13 junto a Silvina Frana y la gestión. Estas dos rutas son troncales, vive y trabaja mucha gente. Es ahí donde proyectamos un lugar en el que se puede crecer mucho, por eso nuestra decisión política es trabajar en conjunto con el comité interjurisdiccional, que integramos con el ministerio de Infraestructura, pero fundamentalmente con aquellos que viven y padecen los excesos del agua".

Finalmente, el intendente, Enrique Mualen, mencionó: “Muchas veces soñamos y no es posible concretar nuestro sueño. En este caso puntual, lo hicimos acompañados, con el gobierno de la provincia, con una decisión política de aportar fondos para dar inicio a estas cuatro cuadras de pavimento. Además, hemos habilitado las luminarias de más de tres kilómetros con fondos provinciales, a través de vialidad en el acceso sur de nuestra ciudad; y poder pensar que pronto estaremos ejecutando las obras de las alcantarillas, y sumaremos 45 viviendas para nuestros vecinos, tiene que ver con decisiones políticas de un Estado nacional y provincial presentes para el desarrollo del departamento”.

**Alcantarillas transversales**

El presente proyecto corresponde a la reconstrucción de alcantarillas existentes sobre las Rutas Provinciales Nº 13 y Nº 77-s, que han colapsado o se encuentran deterioradas. Actualmente, estas alcantarillas se encuentran en estado de abandono, acumulando sedimentos y obstáculos de todo tipo que limitan la acción de escurrimiento para la cual fue diseñado, y no permite el escurrimiento óptimo según los caudales requeridos.

La Obra completa (Etapas I, II y III) permitirá mejorar el escurrimiento de las aguas a través de la RP Nº 13 y RP Nº 77-s, colaborando en un correcto funcionamiento del sistema de drenaje de la zona.

En este sentido, el secretario de Recursos Hídricos, Roberto Gioria, comentó los detalles de las obras: “Se realizarán trabajos de excavación y demolición de estructuras existentes, construcción de nuevas alcantarillas y ejecución de las obras de canalización necesaria, así como también todas las tareas necesarias para asegurar el funcionamiento continuo y seguro de las rutas, y su puesta en servicio conforme a las especificaciones que están en los pliegos”.

Cabe destacar que las obras se ubican dentro del sistema hídrico denominada Región de los Bajos Submeridionales, uno de los humedales más grandes y desconocidos de la Argentina, con 3,3 millones de hectáreas de extensión.

Etapa I Ruta Provincial Nº13: Con un presupuesto oficial de $ 105.334.304,26, y un plazo de ejecución de 8 meses, se presentaron 4 ofertas: MEN INGENIERÍA S.A. cotizó $115.725.634,88; PIRÁMIDE CONSTRUCTORA S.A. cotizó $115.369.374,34; INGENIERÍA Y PILOTES S.R.L. cotizó $135.722.170,29; y una cuarta y última oferta correspondiente a la firma EMPRESA CONSTRUCTORA PILATTI S.A. que cotizó $90.827.842,46.

Etapa II Ruta Provincial Nº77-s: Con un presupuesto oficial de $56.283.824,67 y un plazo de ejecución de 8 meses, se presentaron 5 ofertas: BAUZA INGENIERÍA S.R.L. cotizó $51.782.732,19; MEN INGENIERÍA S.A. cotizó $94.919.757,38; EFE CONSTRUCCIONES DE CARLOS A. FIERRO cotizó $52.000.000; PIRÁMIDE CONSTRUCTORA S.A. cotizó $66.343.179,73; y una quinta y última oferta correspondiente a la firma EMPRESA CONSTRUCTORA PILATTI S.A. que cotizó $45.564.386,83.

Etapa III Ruta Provincial Nº77-s: Con un presupuesto oficial de $ 44.242.547,32 y un plazo de ejecución de 8 meses, se presentaron 5 ofertas: BAUZA INGENIERÍA S.R.L. cotizó $40.706.380,27; MEN INGENIERÍA S.A. cotizó $74.298.086,24; EFE CONSTRUCCIONES DE CARLOS A. FIERRO cotizó $40.400.000; PIRÁMIDE CONSTRUCTORA S.A. cotizó $56.089.473,99; y una quinta y última oferta correspondiente a la firma EMPRESA CONSTRUCTORA PILATTI S.A. que cotizó $37.438.487,79.

**45 viviendas**

Se trata de la implementación de conjuntos de viviendas, que se ejecutarán con fondos nacionales bajo el programa Casa Propia - Construir Futuro, perteneciente al Ministerio de Desarrollo Territorial y Hábitat de la Nación.

Posee un plazo de 12 meses de ejecución y un presupuesto total que supera los 190 millones de pesos.

Al respecto, Frana mencionó que "poder llevar adelante estas licitaciones responde a una política concreta, de generar trabajo y concretar sueños a partir de la vivienda y el acceso a los servicios, a un hábitat digno. Conocemos la situación y la demanda habitacional de muchas familias que hace años quieren acceder a su casa propia, proyectar su futuro, y arraigarse en su localidad".

Por su parte, el secretario de Hábitat de la provincia, Amado Zorzón, sostuvo que "lo importante es seguir trabajando articuladamente, con una política federal de vivienda y hábitat, buscando siempre responder y solucionar la demanda habitacional de la provincia, lo que para nosotros representa un compromiso. Ya hemos realizado licitaciones en Rafaela, Gálvez y El Trébol, bajo esta modalidad de Casa Propia. El principal problema que se nos presentó fue el acceso a lotes con servicios, por eso creamos el Rutfu, que es el Registro Único de Tierras Fiscales Urbanizables, que busca ampliar la accesibilidad de las familias al suelo urbano en cada lugar de la provincia, y generar suelo apto para construir viviendas acordes a las necesidades actuales".

Finalmente, el director provincial de Vivienda y Urbanismo, José Kerz manifestó que "el tema de la vivienda es una prioridad para el Gobierno de la Provincia, y la presencia del Estado es fundamental para financiar la construcción. Todas contarán con infraestructura básica, y serán de dos y tres dormitorios".

**Las viviendas**

Con un presupuesto oficial de $ 191.794.814,64, se recibió una oferta correspondiente a la firma PILATTI S.A. que cotizó $267.576.137,43. Asimismo la empresa formuló una mejora de la oferta en un 6% en todos sus rubros resultando un monto de $251.521.569,19.

Las viviendas contarán con dos dormitorios, cocina – comedor, baño y lavadero y espacio para cochera, rondando los 65 m² de superficie cubierta y 5 m2 de superficie en galería, en promedio, con una platea ejecutada para ampliar la vivienda con un tercer dormitorio.

Asimismo, cada vivienda estará dotada de instalaciones y conexiones de agua potable, cloacas y energía eléctrica, e instalaciones aptas para gas envasado. Los desagües pluviales irán a la vía pública, y cada unidad contará con un termotanque solar.

**Presentes**

Participaron de los actos, los presidentes comunales: Gabriel Gentili de Villa Minetti, Javier Herrera de Pozo Borrado, Jorge Dalke de San Bernardo , Valeria Díaz de Gregoria Pérez de Denis.

También acompañaron a las autoridades, el secretario de Desarrollo Territorial y Arraigo, Fabricio Medina, el subsecretario de Desarrollo Hídrico y Comité de Cuenca, Mariano Diez, representantes de la Sociedad Rural de Tostado, representantes de Federación Agraria Argentina, del Comité de Cuencas e Hídrovía de Tostado, de Villa Minetti, y Pozo Borrado, productores de la cuenca lechera, de SENASA, del INTA Tostado, entre otras autoridades de las comunas del departamento 9 de Julio.