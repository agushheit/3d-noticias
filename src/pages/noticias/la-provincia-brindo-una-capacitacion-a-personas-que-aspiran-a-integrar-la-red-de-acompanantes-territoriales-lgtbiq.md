---
category: Estado Real
date: 2022-01-20T06:00:24-03:00
thumbnail: https://assets.3dnoticias.com.ar/ACOMPAÑANTES.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La provincia brindó una capacitación a personas que aspiran a integrar la
  red de acompañantes territoriales LGTBIQ+
title: La provincia brindó una capacitación a personas que aspiran a integrar la red
  de acompañantes territoriales LGTBIQ+
entradilla: El programa, perteneciente al Ministerio de Igualdad, Género y Diversidad,
  busca fortalecer la articulación y llegada territorial de las políticas públicas
  vinculadas a la diversidad sexual.

---
El Ministerio de Igualdad, Género y Diversidad brindó este miércoles una capacitación a personas que se inscribieron en el programa de Red de Acompañantes Territoriales LGBTIQ+, llevado a cabo por el Gobierno de Santa Fe a través de la mencionada cartera. La actividad contó con la participación y exposición de la activista travesti y psicóloga social, Marlene Wayar.

La formación es para todas aquellas personas que se postularon para conformar la Red de Acompañantes Territoriales LGBTIQ+, es una propuesta busca fortalecer la llegada de las políticas públicas vinculadas a diversidad sexual en todo el territorio provincial. Esta capacitación que consiste en cuatro encuentros con modalidad virtual tiene como objetivo fortalecer las redes comunitarias contra las violencias y las desigualdades de género, considerando especialmente los desafíos que la pandemia ha planteado.

La Red reúne y articula, de manera coordinada, un conjunto de acciones de diversos actores para aprovechar su potencial territorial y dar respuestas rápidas y efectivas contra las violencias y las desigualdades basadas en el género, así como para transformar las dinámicas jerárquicas y las desigualdades de género que atraviesan nuestras sociedades, para fomentar el acceso efectivo de las personas LGBTIQ+ a derechos fundamentales.

En la presentación de este programa participó la ministra de Igualdad, Género y Diversidad, Florencia Marinaro, quien detalló: “Iniciamos un ciclo que busca crecer a través de redes comunitarias. Siempre decimos que las diferencias, las desigualdades y la discriminación no son temas individuales, sino públicos y políticos. Necesitamos la articulación de la comunidad, la sociedad y el Estado. En ese sentido, contamos con este programa de formación y de encuentro para generar herramientas en los territorios y así poder acompañar el proceso de ejercicio de derechos".

Luego, agregó: "Tenemos una provincia enorme y diversa, que no cuenta con los mismos recursos institucionales en todos los lugares y en donde no existe el mismo acceso al empleo y a los derechos. Por eso lo fundamental es poder crear un espacio informativo para todos y todas, en todo el territorio de la provincia y que eso se siga registrando".

Además, formaron parte la subsecretaria de Políticas de Igualdad y Diversidad del Ministerio de Igualdad, Género y Diversidad, Nerea Tacari e integrantes de los equipos del Ministerio de igualdad, Género y diversidad.

**SOBRE EL PROGRAMA**

El programa de la Red de Acompañantes Territoriales LGBTIQ+ tiene el objetivo de articular y garantizar la llegada de las políticas públicas a cada una de las localidades de la provincia, poniendo en marcha diferentes acciones relacionadas y adaptadas a cada uno de los territorios y construyendo herramientas para el fortalecimiento de las redes comunitarias contra las violencias y las desigualdades de género. En este sentido, es una premisa abordar los desafíos en el marco de la pandemia y post pandemia por Covid-19 en toda la provincia.