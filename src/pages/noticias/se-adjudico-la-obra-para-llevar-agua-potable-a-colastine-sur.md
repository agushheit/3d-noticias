---
category: La Ciudad
date: 2021-09-09T06:15:14-03:00
thumbnail: https://assets.3dnoticias.com.ar/colastine.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Se adjudicó la obra para llevar agua potable a Colastiné Sur
title: Se adjudicó la obra para llevar agua potable a Colastiné Sur
entradilla: 'Los trabajos estarán a cargo de la empresa Pilatti SA. El presupuesto
  oficial supera los $ 43 millones. El barrio más antiguo de la ciudad tendrá una
  red con 310 conexiones domiciliarias. '

---
Este martes llegó la esperada firma del contrato de adjudicación de la obra que llevará agua potable a Colastiné Sur, el barrio que con sus más de 350 años se lo considera el más antiguo de la ciudad de Santa Fe, pero que hasta entonces no cuenta con el servicio básico.

En marzo de este año el intendente Emilio Jatón se comprometió frene a los vecinos llevar adelante estos trabajos. Si bien entre la primera y segunda licitación se hicieron ajustes técnicos y presupuestarios, la obra ya fue adjudicada a la empresa constructora Pilatti SA que aún no tiene definido la fecha de inicio de los trabajos, que prevén un plazo de ejecución de 120 días. Por ello, si toda marcha acorde a lo establecido, los vecinos de Colastiné Sur tendrá en los primeros meses del 2022 agua potable.

El presupuesto oficial que presentó el municipio es de $ 43.590.823,91. La obra consiste en la ejecución de la red de distribución de agua potable para habilitar el servicio a la zona que se ubica a la vera del río Colastiné, lindando al sur y al norte con la Ruta Nacional Nº 168 y sobre el entorno de la calle que comunica este sector con el Club de Caza y Pesca.

**310 conexiones domiciliarias**

La red proyectada cuenta con una longitud de 5.047,33 metros de cañería de PVC C6 de diámetro 75 mm y 583.96 m de diámetro 110 mm. En la red distribuidora se proyecta la instalación de válvulas esclusa, 9 de Dn 80 mm y 5 de Dn 100 mm, que permitan el cierre de cada tramo en forma independiente. De esta manera, se garantiza que se podrán realizar tareas de mantenimiento en cada tramo, mientras que se continúa con la prestación del servicio de manera normal en el resto del predio.

Se instalarán 10 hidrantes a resorte, que permitan la purga de la red y que puedan eventualmente ser usados para incendio. También se prevé la colocación de juntas de goma, ramales y accesorios correspondientes. En el sector considerado se proyecta la ejecución de 310 conexiones domiciliarias sobre cañería de Dn 75 mm y 27 conexiones domiciliaras de agua sobre cañería de Dn 110 mm.

El proyecto sobre el cual se ejecuta la obra fue elaborado desde la Secretaría de Asuntos Hídricos y Gestión de Riesgos de la Municipalidad conforme a las pautas que exige Aguas Santafesinas (Assa).

"Dada la necesidad de los vecinos de la zona de contar con este vital elemento, se requiere en forma prioritaria la ejecución de la infraestructura básica consistente en: captación de agua superficial con toma del río Colastiné (pontón flotante); Impulsión (bombas y cañerías); Tratamiento (planta clarificadora de captación superficial) e instalaciones complementarias; Reserva elevada; Se prevé la instalación de un tanque elevado con 80 m3 de capacidad a fin de garantizar la continuidad del suministro, además, las instalaciones prevén el sistema completo de automatismo", se explica en los detalles técnicos de la obra.