---
category: La Ciudad
date: 2021-08-14T06:15:02-03:00
thumbnail: https://assets.3dnoticias.com.ar/FERIADO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Servicios municipales por el feriado nacional
title: Servicios municipales por el feriado nacional
entradilla: La Municipalidad informa sobre los servicios que se garantizan a la población
  durante este fin de semana largo.

---
Con motivo del feriado nacional del 17 de agosto, día en que se conmemora el fallecimiento del General José de San Martín, la Municipalidad de Santa Fe informa los horarios de los servicios de recolección de residuos, cementerio y estacionamiento medido para este fin de semana largo. Cabe agregar que el feriado se trasladó al lunes 16.

**Recolección de residuos**

Este lunes, el servicio de recolección no tiene cambios y será el habitual de un lunes; y tampoco habrá modificaciones para sábado y domingo.

**Cementerio**

El lunes, las puertas permanecerán abiertas de 8 a 12 horas, con motivo del feriado, para las visitas del público.

En tanto, sábado y domingo el horario será el habitual: de 9 a 16 horas para el sábado, y de 8 a 12 horas para el domingo.

**SEOM**

El Sistema de Estacionamiento Medido (SEOM) no estará operativo.

**Mercado Norte**

El Mercado Norte, ubicado en la intersección de Santiago del Estero y Urquiza, abrirá este lunes, en el horario de 9 a 13 horas. Mientras que serán los habituales para los días sábado, de 9 a 19 horas; y domingo, de 9 a 13 horas.