---
layout: Noticia con imagen
author: "-"
resumen: Subas en el precio de la carne
category: El Campo
title: "Precio de la carne: se esperan subas hacia fin de año"
entradilla: De acuerdo a estimaciones de la cadena en los próximos meses habría
  menos carne y, en consecuencia, con mayores precios.
date: 2020-10-26T16:43:26.492Z
thumbnail: https://assets.3dnoticias.com.ar/carne.jpg
---
Según advirtieron especialistas del sector ganadero los precios de la carne vacuna experimentarán una suba antes de fin de año debido a que el consumo interno destacó factores como el bajo porcentaje de ocupación en feedlots, la suba de precios de la vaca y el incremento de insumos como el maíz.

“Aún queda un poco de oferta, pero solo para un par de meses más. Estamos sentados sobre una bomba: va a haber un faltante importante”, afirmó el consultor ganadero Víctor Tonelli, en diálogo con Canal Rural.

En esa línea, un trabajo elaborado por la Consultora remarcó que en el caso de la exportación, este sector exhibe una firmeza que se traslada a los precios de la vaca, que en los últimos días alcanzó valores cercanos al novillo. “Los animales más livianos con destino al mercado interno se encuentran con un techo en el precio debido a la recesión económica.”



El presidente de la Cámara de la Industria y Comercio de Carnes y Derivados (Ciccra), Miguel Schiariti, advirtió a BAE Negocios que las recientes cotizaciones del Mercado de Liniers “dieron por tierra con el rumor de los últimos días en relación a aumentos en el precio de la carne”.

Para el directivo, en el corto plazo se empezará a sentir la falta del novillito gordo que sale de los corrales. Esta restricción en la oferta de carne empujará los precios de venta al público.