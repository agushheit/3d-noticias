---
category: La Ciudad
date: 2021-04-09T06:36:39-03:00
thumbnail: https://assets.3dnoticias.com.ar/card1.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Concejo Santa Fe
resumen: Solicitan mejorar las instalaciones del CARD
title: Solicitan mejorar las instalaciones del CARD
entradilla: |2-

  El Concejo Municipal aprobó el proyecto para solicitar al Gobierno de la Provincia mejoras al equipamiento deportivo y la pista para atletas del Centro de Alto Rendimiento Deportivo (CARD).

---
Este jueves, el cuerpo legislativo local se pronunció con respecto al estado y mantenimiento del CARD, considerando se tomen medidas que garanticen las condiciones estructurales y organizativas para poner en funcionamiento la totalidad de las actividades que allí se desenvuelven.

Al respecto, el presidente del Concejo, Leandro González, mostró su preocupación por el estado actual del lugar y explicó que “el complejo presenta un estado de abandono, tanto en su infraestructura edilicia, la limpieza en general, como en el mantenimiento del espacio verde y vegetación. Creemos que es necesario asegurar el mantenimiento óptimo del Centro para que los y las empleadas, entrenadoras, atletas y toda aquella persona que quiera iniciarse en esta disciplina pueda hacerlo de manera óptima y segura en un espacio que cumpla con las normas de higiene y que, además, les incentive a desarrollarse como profesionales en esta actividad deportiva”.

González apuntó que “el CARD es cuna de muchos deportistas amateurs y de élite. Hace poco, Germán Charaviglio, quien se viene sacrificando enormemente para competir y llevar la bandera de Santa Fe a lo más alto, fue quien dio cuenta de la situación de desidia que vive el complejo y en particular el equipamiento del Card”.

“Lo que pretendemos con esta comunicación es que se asegure el acceso y uso del mismo, que se retome plenamente el funcionamiento del CARD, en el marco de las restricciones o protocolos que obligadamente habrá que seguir teniendo por la pandemia. En este sentido, sería importante que se retomaran las reuniones de la mesa de gestión compartida a nivel institucional, para tratar de manera común aquellos problemas o situaciones que pueden ser usuales en el polo deportivo en su conjunto”, concluyó el concejal.

![](https://assets.3dnoticias.com.ar/card.jpg)