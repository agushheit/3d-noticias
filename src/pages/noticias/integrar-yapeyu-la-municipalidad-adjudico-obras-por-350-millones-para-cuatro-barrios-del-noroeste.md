---
category: La Ciudad
date: 2021-10-01T06:00:30-03:00
thumbnail: https://assets.3dnoticias.com.ar/YAPEYU.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Integrar Yapeyú: la Municipalidad adjudicó obras por $ 350 millones para
  cuatro barrios del Noroeste'
title: 'Integrar Yapeyú: la Municipalidad adjudicó obras por $ 350 millones para cuatro
  barrios del Noroeste'
entradilla: ".Las tareas de provisión de agua, desagües, asfalto, cordón cuneta, iluminación,
  recuperación del bulevar 12 de Octubre, entre otras, mejorarán la calidad de vida
  de 2.300 familias."

---
El intendente Emilio Jatón firmó los contratos de adjudicación para el inicio de las obras por 350 millones de pesos del Integrar Yapeyú, en el Noroeste de la ciudad, que apuntan a la integración social y urbana en los barrios Yapeyú, San Agustín, La Ranita y Loyola Norte. Estas acciones mejorarán la calidad de vida de 2.300 familias. “Cuando la dignidad llega a todos los ciudadanos, empiezan a cambiar las cosas”, dijo emocionado el mandatario local.

Esos fondos permitirán llevar adelante un conjunto de trabajos que priorizan la expansión de la red de agua potable, obras de desagües, trabajos viales –pavimento, cordón cuneta y mejorado granular-, alumbrado público, y otras tareas tendientes a llevar servicios básicos a los vecinos y vecinas de la zona.

“¿Saben cuánto tiempo esperaron los vecinos estas obras? Años, décadas, de exclusión y olvido”, remarcó Jatón. Y dijo que “hoy nosotros con agua, con servicios, con asfalto, con iluminación, con desagües, con sueños, queremos cambiar esta realidad. Este día forma parte de los sueños de esta gestión, por eso estamos emocionados; porque cuando uno hace lo que dice, eso tiene mucho valor”.

En otra parte de su discurso, adujo que “este barrio nació y creció en base al trabajo de los vecinos, hay un fuerte trabajo de las instituciones, de las vecinales, de la Iglesia; hay mucho trabajo de aquellos que se comprometieron a hacer de este lugar, un lugar donde vivir”. Y añadió que “hoy estos barrios son noticia por otras cosas, y nosotros queremos cambiar ese eje, queremos transformar este lugar y lo estamos haciendo, estamos cumpliendo”.

Jatón reconoció que el proyecto Integrar Yapeyú es producto de un año y medio de trabajo de los equipos municipales con las instituciones del barrio, e indicó que son fondos nacionales: “Tenemos que agradecer a Nación porque nos han escuchado y entendido la necesidad de estos barrios, y también al gobernador. No se puede cambiar la realidad si no trabajamos todos juntos”.

Cabe destacar que el conjunto de obras es financiado por el Banco Internacional de Reconstrucción y Fomento (BIRF), a través del Ministerio de Desarrollo Territorial y Hábitat de la Nación, y cuenta con el aval del Gobierno provincial. En tanto, el proyecto ejecutivo, la búsqueda del financiamiento y la licitación fueron realizados por la Municipalidad de Santa Fe, a través de la Agencia Santa Fe Hábitat.

**Día de emoción**

En el acto, que se llevó a cabo en la sede de la Vecinal Yapeyú, estuvo presente la titular de la Agencia Santa Fe Hábitat, Paola Pallero, quien explicó las obras a los vecinos. “Hoy recuerdo cómo fuimos trabajando este proyecto junto a vecinos, vecinas, instituciones, la vecinal, hicimos consultas públicas para conocer qué obras priorizar. Así que agradecemos la participación que para nosotros no es algo más, es parte de las políticas públicas del Estado municipal”, remarcó.

También estuvieron presentes el secretario General del municipio, Mariano Granato; el presidente del Concejo, Leandro González; la concejala Laura Mondino, entre otros ediles y funcionarios de la gestión, que otorgaron obsequios a los referentes de los distintos barrios.

Emocionados hasta las lágrimas, los representantes de instituciones mencionaron la relevancia de las obras. Gabriela Benegas, del Merendero La Casa de Agus, dijo que “esto es muy importante para los vecinos y para el futuro de nuestros niños”. Mientras, Isolina Rolón,de la Estación Villa Teresa, sostuvo que “verdaderamente esto es un orgullo, nací en el barrio y las promesas que nos hicieron un día están cumplidas”.

Con la voz entrecortada, Gladis Ordoñez, contó que sus padres fueron de los primeros habitantes de Yapeyú: “Antes se llamaba barrio de Emergencia, hace 60 años, cuando no había nada. Hoy estarán orgullosos desde el cielo”. También estuvo Florencia Santos y terminó hablando Víctor Miranda, de la vecinal Ceferino Namuncurá: “Es emocionante, es un logro del trabajo de la Red de Instituciones que tiene mucho valor en el barrio”.

Luego, el intendente firmó los contratos de obras con los representantes de las empresas adjudicatarias, Sergio Wilkelman, por Wilkelman S.R.L., por las obras de infraestructura y saneamiento (lote 1) que fueron adjudicadas por $ 278.208.627,75 y que tienen un plazo de ejecución de 12 meses. En tanto, el otro contrato se rubricó con Marcelo Ruiz, de la empresa Tecsa SA, para el reacondicionamiento del bulevar 12 de octubre, por un monto de $ 69.501.865,18 y que tiene un plazo de ejecución de 6 meses.

**Agua potable para 750 familias**

La obra consiste en la ejecución de la red de abastecimiento de agua potable para 30 manzanas de los barrios Loyola Norte y La Ranita. La red proyectada cuenta con una longitud de 6.700m de cañería y se prevé la ejecución de 750 conexiones domiciliarias con llave de paso y una canilla de servicio dentro de cada lote.

Cabe destacar que esta obra es posible gracias a la ejecución del Acueducto Santa Marta, proyectado para proveer a los barrios ubicados en el extremo noroeste de la ciudad de Santa Fe.

Esta obra consiste en el tendido de una cañería desde Camino Viejo a Esperanza hasta la intersección de las calles Hugo Wast y Reinares. Tendrá una extensión de 2.367m y en su recorrido se prevé realizar –a futuro, cuando se concreten otras obras complementarias- los empalmes sobre cañerías primarias existentes que mejorarán la presión en los barrios aledaños.

En cuanto a la red de distribución, las obras se realizarán en el área delimitada por las siguientes calles: Teniente Loza al norte, Vuelta de Obligado al oeste, Hugo Wast al sur y Gdor. Menchaca al este.

**Desagües pluviales en La Ranita**

El área de proyecto está localizada en el sector de Barrio La Ranita que históricamente presenta anegamientos y trae aparejados graves inconvenientes a los aproximadamente 1.000 habitantes que viven en un radio de 6 manzanas. Estos problemas se dan principalmente debido a la existencia de depresiones en el terreno y la falta de comunicación hidráulica.

A partir de los proyectos para el mejoramiento de la red vial, la solución propuesta permitirá que los excedentes hídricos aledaños escurran hacia el Oeste con destino final al Reservorio N° 6. Las obras contemplan las bocas de tormenta y conductos pluviales necesarios para el correcto escurrimiento de los excedentes pluviales.

El proyecto en La Ranita consiste en un conjunto de obras de saneamiento destinadas a la paulatina regularización y acceso a los servicios básicos, entre ellas la anulación de desagües de aguas grises hacia la calzada y el sistema pluvial proyectado.

Para las obras de mejorado de calles y cordón cuneta se priorizará a los frentistas de Larrechea entre Teniente Loza y Neuquén, y de Chubut entre Schmidl y Menchaca, que presentan los mayores niveles de informalidad y los factores de riesgo ambiental más graves. La regularización del sistema de desagües individuales se extenderá luego a todo el sector de La Ranita.

**Bulevar 12 de Octubre**

Se llevará adelante la renovación integral del bulevar 12 de Octubre, entre las avenidas Hugo Wast y Teniente Loza. Con este proyecto se prevé la puesta en valor del cantero central de una de las arterias principales del sector, tanto institucional como comercialmente. La idea es que se convierta en un espacio de paseo, priorizando el uso de las áreas peatonales para la circulación, la recreación y el esparcimiento, y contribuyendo a la integración física del área a la trama urbana.

La intervención prevé realizar tareas de demolición y remoción de pisos, tapas, cordones, pavimento, pisos, rejas, canteros y contrapisos. En el cantero central, se incorporarán 17 columnas bajas –peatonales- de iluminación con tecnología Led, además de 50 lámparas Led nuevas y el reacondicionamiento de 25 columnas existentes, a fin de complementar la iluminación general.

Además, se incluye la renovación e incorporación de mobiliario urbano: 51 bancos de hormigón, 26 cestos de residuos, etc). El bulevar también contará con 8.929 m2 de solados en veredas laterales y en el cantero central de hormigón armado con sistema podotáctil (son una señalización que se siente al caminar sobre ella y que permite garantizar la accesibilidad a personas con baja visibilidad). Además, se plantarán especies ornamentales, cubresuelos, arbustivas y 12 árboles nuevos.

**Más obras viales y de iluminación**

Asimismo, las obras tienen como objetivo mejorar la conectividad vial de la zona, consolidar el recorrido de una línea de colectivos -vinculando tres establecimientos educativos-, y urbanizar calles irregulares que presentan problemas de escurrimiento pluvial. Hay varias calles de los barrios involucrados que serán mejoradas algunas de ellas con pavimento, otras con cordón cuneta y estabilizado granular o manda peatón con sus correspondientes trabajos en desagües.

En cuanto a iluminación, en los barrios Loyola Norte y La Ranita se colocarán 103 columnas de tecnología Led. A estas se suman, las obras de alumbrado en el barrio Yapeyú Oeste, a través del Programa Argentina Hace.