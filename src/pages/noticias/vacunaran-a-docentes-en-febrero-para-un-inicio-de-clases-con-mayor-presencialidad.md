---
category: Agenda Ciudadana
date: 2021-01-08T10:24:01Z
thumbnail: https://assets.3dnoticias.com.ar/080121-clases.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Télam'
resumen: Vacunarán a docentes en febrero para un inicio de clases con mayor presencialidad
title: Vacunarán a docentes en febrero para un inicio de clases con mayor presencialidad
entradilla: Nicolás Trotta ratificó el inicio del calendario escolar en las fechas
  previstas y recordó que los docentes se encuentran en el sector prioritario para
  la vacunación.

---
La vacunación contra el coronavirus para los docentes empezará en febrero con la segunda partida de la vacuna Sputnik V, aseguró el ministro de Educación, Nicolás Trotta, quien ratificó el inicio del calendario escolar en las fechas previstas para que la presencialidad «sea el ordenador del sistema educativo en el 2021».

«Las evidencias son muy distintas hoy a las que teníamos en marzo del 2020: la presencialidad tiene que ser el ordenador del sistema educativo este año, con la escuela como un espacio seguro para los niños», dijo a Télam el ministro Trotta.

El titular de la cartera educativa aseguró que «en febrero vamos a empezar el proceso de vacunación para todo el sector docente. Esto es algo que se fijó como prioridad en nuestro gobierno cuando, por ejemplo, países como Australia y Finlandia no tuvieron esa prioridad».

En este sentido, Trotta iniciará a partir del lunes visitas a todas las provincias para planificar el inicio del ciclo lectivo en cada jurisdicción.