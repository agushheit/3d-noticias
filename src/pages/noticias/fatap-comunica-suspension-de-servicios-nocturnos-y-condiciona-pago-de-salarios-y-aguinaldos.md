---
category: Agenda Ciudadana
date: 2022-06-28T19:15:49-03:00
thumbnail: https://assets.3dnoticias.com.ar/colectivojpeg.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: FATAP Comunica Suspensión de Servicios Nocturnos y Condiciona Pago de Salarios
  y Aguinaldos
title: FATAP Comunica Suspensión de Servicios Nocturnos y Condiciona Pago de Salarios
  y Aguinaldos
entradilla: |-
  Suspenderán los servicios entre las 22:00 hs del día
  miércoles 29/06/22 y las 06:00 hs del día jueves 30/06/22, para dar prioridad a los que funcionan en horas pico.

---
Asimismo, ha condicionado el pago de los salarios del mes de junio y el medio aguinaldo a la asignación de los fondos necesarios para ello, alertando que, en caso de no contar con recursos suficientes, los pagos podrían efectuarse de modo parcial y escalonado.

La medida es consecuencia de:

1) La publicación de la Resolución MT N°401/22, que asigna al sector un presupuesto de $38.000 millones para el año 2022, a pesar de que existe dictamen favorable de las Comisiones de Transporte y Presupuesto y Hacienda de la H. Cámara de Diputados de la Nación en asignar al sector $59.500 millones por el mismo período.

2) La imposibilidad material de atender salarios, aguinaldos y los costos erogables, tales como combustibles, lubricantes, neumáticos, reparaciones y seguros, elementos todos imprescindibles para mantener operables los servicios.

Para atender dichas obligaciones, los fondos presupuestados en el marco del FondoCompensador del Transporte, deberían ser distribuidos por el Ministerio de Transporte al sistema en el orden de los $6.500 millones para el mes en curso, que deberían ser

cancelados antes del cuarto día hábil del mes de julio próximo, sin los cuales los condicionamientos insertos en el acuerdo paritario se tornarán operativos.

A la situación descripta deben sumarse los constantes incrementos en el precio del gasoil, sumado al agotamiento del stock disponible y la falta de reposición del consumido, hechos que deterioran aún más la ecuación económica de los servicios.

Lamentamos los contratiempos que esta medida ocasionará a nuestros usuarios y a la población en general, sin perjuicio de que FATAP continuará incansablemente apelando a la responsabilidad de las autoridades competentes para que adopten las medidas de fondo necesarias para evitar la paralización total del sistema, la desaparición de empresas de capital nacional y la consecuente pérdida de los puestos de trabajo que generan.