---
category: Agenda Ciudadana
date: 2021-06-23T09:13:59-03:00
thumbnail: https://assets.3dnoticias.com.ar/vacunacion.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de LT10
resumen: 'Vacunatorio Vip en Santa Fe: denuncian a otro funcionario provincial'
title: 'Vacunatorio Vip en Santa Fe: denuncian a otro funcionario provincial'
entradilla: Se trata de René Alzugaray, subsecretario de legal y técnica de del ministerio
  de Salud, quien tiene 36 años. El diputado Carlos Del Frade pide al poder ejecutivo
  que de respuesta sobre este nuevo caso de vacunado vip.

---
El diputado provincial Carlos Del Frade realizó un pedido de informe en la Cámara baja sobre un nuevo caso de vacunado vip en la provincia de Santa Fe.

Una persona le acercó pruebas al diputado sobre el caso del abogado René Alzugaray, subsecretario de legal y técnica, quién fue vacunado el 29 de enero de este año con la vacuna Sputnik (momento en el que solo se inoculaban los médicos).

El legislador le pide explicaciones al gobierno provincial sobre este caso porque es nada más ni nada menos que el encargado de investigar los vacunados vip y las irregularidades que se dieron en el proceso de vacunación. Por ejemplo, el caso del ex director de la EPE Alberto Joaquín quién se vacunó en el Hospital Eva Perón de Baigorria mucho antes del momento en el que le tocaba (por citar, solo uno de los casos más escandalosos).

Carlos del Frade, sostuvo por LT10 que “estos son casos de corrupción. El estado debe clarificar las situaciones y castigar a los responsables. Es indispensable conocer la lista de todos los vacunados de la provincia (salvaguardando la intimidad de las personas) como ya lo pidieron en Diputados Giustiniani y Donnet. Hay que separar la paja del trigo para no dañar lo que están haciendo bien las cosas”.  

Finalmente, Del Frade remarcó que “cuando llegaron las primeras vacunas hubo privilegios. Esto debe quedar claro y hay que sancionar a los responsables. No puede haber hijos y entenados. Esto es una daño muy grave a la democracia”.  

**La versión del minsiterio de Salud**

Desde el ministerio, señalaron por LT10 que  el abogado de 36 años René Alsugaray es considerado personal estratégico y por este motivo fue inoculado en ese momento. Por lo tanto, están negando que exista irregularidad en este caso.