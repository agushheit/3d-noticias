---
category: Agenda Ciudadana
date: 2020-12-11T11:42:03Z
thumbnail: https://assets.3dnoticias.com.ar/casino1.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: 'Casinos y bingos: autorizados en el territorio santafesino'
title: 'Casinos y bingos: autorizados en el territorio santafesino'
entradilla: La provincia autorizó la actividad de casinos y bingos en el territorio
  santafesino a través del Decreto 1709 que habilita a los establecimientos concesionados
  ubicados en Santa Fe, Rosario y Melincué.

---
La provincia de Santa Fe autorizó, en todo su territorio, la actividad de casinos y bingos concesionados: Casino Puerto de Santa Fe S.A., de Rosario y Melincué. Lo hizo mediante el **Decreto 1709**.

En el artículo 2° establece que la autorización para el desarrollo de las actividades referidas en el artículo está sujeta a la condición de la implementación y cumplimiento del protocolo específico aprobado por el Ministerio de Salud y el Ministerio de Trabajo, Empleo y Seguridad Social de la Provincia.

En este sentido, sostiene que se debe respetar el límite previsto en el [DNU N° 956/2020](https://www.boletinoficial.gob.ar/detalleAviso/primera/237844/20201130 "DNU 956/2020") del gobierno nacional, en cuanto a la distancia mínima de dos metros entre las personas y la restricción del uso de las superficies cerradas permitiendo como máximo el uso del 50% de su capacidad, entre otros aspectos referidos al Distanciamiento Social Preventivo y Obligatorio en gran parte del territorio nacional.

En cuanto a los procedimientos de fiscalización, se establece que las **autoridades municipales y comunales, en concurrencia con las autoridades provinciales** competentes, serán los **encargados de garantizar el cumplimiento de las medidas** dispuestas en virtud de la vigencia del distanciamiento social, preventivo y obligatorio, de los protocolos vigentes y de las normas dispuestas en virtud de la emergencia sanitaria.

El artículo 3° establece que las autoridades locales quedan facultadas en sus distritos a disponer mayores restricciones respecto a los días, horarios, requisitos y modalidades particulares, para el desarrollo de la actividad. Cabe destacar que el artículo 1° se indica que se deben tener en cuenta las limitaciones horarias prescriptas por el [Decreto Provincial N° 1527/2020.](https://www.santafe.gob.ar/noticias/recursos/documentos/2020/11/2020-11-27NID_269243O_1.pdf "Decreto 1527/2020")

Además, se encomienda al Ministerio de Salud de la provincia el control del cumplimiento de las condiciones para la realización de las actividades dispuestas por el presente decreto y el monitoreo de la evolución epidemiológica y sanitaria en función de las mismas.

Sin perjuicio de lo anterior podrá recomendar al Poder Ejecutivo la suspensión total o parcial con carácter preventivo, de la excepción autorizada en el presente Decreto, cuando la evolución de la situación epidemiológica lo aconseje.