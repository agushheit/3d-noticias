---
category: La Ciudad
date: 2021-01-26T09:54:29Z
thumbnail: https://assets.3dnoticias.com.ar/curso.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad Santa Fe
resumen: 'Formación laboral: se encuentra abierta la preinscripción para los cursos
  de verano'
title: 'Formación laboral: se encuentra abierta la preinscripción para los cursos
  de verano'
entradilla: |2-


  La Municipalidad de Santa Fe abre la convocatoria para tres cursos de verano en formación laboral. Serán gratuitos y con modalidad semipresencial.

---
Con la premisa de mejorar las condiciones para la inclusión laboral de las personas desocupadas o con necesidades de recalificación, la Municipalidad lanza tres cursos semipresenciales y gratuitos, con cupos limitados, para santafesinos y santafesinas mayores de 16 años. Los interesados podrán optar entre auxiliar de panadería, maquillaje social y reparación de PC.

Los cursos de capacitación laboral se enmarcan en el esfuerzo que realiza el municipio para generar empleo, como verdadera política de Estado, en un sistema productivo que requiere cada vez más formación y especialización. Se busca brindar herramientas para mejorar los perfiles laborales tanto para trabajos en relación de dependencia como para el desarrollo por cuenta propia. **Para preinscribirse hay tiempo hasta el miércoles 27 de enero y se deberán enviar los datos personales al correo: capacitaciones.empleo@santafeciudad.gov.ar.**

Con modalidad semipresencial, las clases se iniciarán el lunes 8 de febrero y tendrán una duración total de 30 horas cátedra. En el transcurso de las mismas se abordarán temas específicos, en distintos niveles de complejidad, que se irán adaptando a las necesidades e intereses de los inscriptos, en busca de mejorar y ampliar sus posibilidades laborales. Para conectarse a las clases virtuales sólo será necesario disponer de un dispositivo digital con acceso a internet; mientras que para las clases prácticas la modalidad será presencial.

La propuesta es impulsada desde la Dirección de Empleo y Trabajo Decente municipal, que integra la Red de Oficinas de Empleo del Ministerio de Trabajo, Empleo y Seguridad Social de la Nación.

En cuanto a los objetivos que persigue cada uno de los cursos, el de Reparación de PC está destinado a capacitar a personas que no tengan conocimiento de computadoras, dispositivos y periféricos para que, al egresar, dispongan de herramientas teóricas y técnicas vinculadas al hardware y software de la PC. Los asistentes se formarán para asistir al usuario de productos informáticos, brindándole servicios de instalación, mantenimiento, resolución de problemas y apoyo a la contratación de productos o servicios informáticos.

El curso de Auxiliar de panadería permitirá identificar las herramientas y utensilios de cocina, aprender cómo se manipulan las mismas y cuáles son los métodos de cocción de distintos productos de panadería y pastelería, como panes, pizzas, fugazzas, facturas, tortas y tartas dulces. También se enseñará sobre comercialización, armado de costos, ideas básicas de packaging, fotografía, marca y redes sociales.

Por su parte, la capacitación en Maquillaje Social permitirá a los alumnos adquirir conocimientos sobre los cuidados y protección de la piel, los insumos y elementos necesarios en cosmética, así como cuáles son las técnicas de maquillaje recomendadas para entrar en el mercado laboral de esta rama tan creativa y en auge. Se enseñará a preparar la piel, colocar bases, correctores y polvos según la morfología del rostro, corregir claros y oscuros. Y, por otro lado, se abordarán técnicas de maquillaje de ojos, correcciones, delineados y sombreados según su forma para lograr resaltarlos.