---
layout: Noticia con imagen
author: "-"
resumen: "Licencias de conducir"
category: Estado Real
title: "Licencia de conducir: nuevo lugar para hacer los cursos de capacitación"
entradilla: "Serán a partir de este miércoles en la Estación Belgrano. El turno
  se puede obtener a través de la página web de la Municipalidad."
date: 2020-10-22T20:53:59.393Z
thumbnail: https://assets.3dnoticias.com.ar/licencia.jpg
---
La Municipalidad informa que el próximo miércoles 21 de octubre, los cursos de capacitación obligatorios para obtener la licencia de conducir también podrán hacerse en la Estación Belgrano, espacio que se suma al del Centro de Educación Vial. Para participar de las charlas deberá obtenerse un turno a través de la página [santafeciudad.gov.ar](<>)

Desde la Secretaría de Control y Convivencia Ciudadana comunicaron que los cursos se dictarán en el predio de la Estación Belgrano (bulevar Gálvez 1150), de lunes a viernes en dos horarios: de 8.00 a 10.00 y de 10.30 a 12.30, con 10 vacantes por turno.

Según explicaron, se dictarán exclusivamente las charlas de capacitación y no se desarrollará ningún otro tipo de trámite. Además, se indicó que se cumplirán estrictamente los protocolos de seguridad e higiene vigentes al día de la fecha, sin excepción.