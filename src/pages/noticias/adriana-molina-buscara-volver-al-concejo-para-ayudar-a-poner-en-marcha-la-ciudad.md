---
category: La Ciudad
date: 2021-07-04T06:15:45-03:00
thumbnail: https://assets.3dnoticias.com.ar/MOLINA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Adriana Molina buscará volver al Concejo para ayudar a "poner en marcha la
  ciudad"
title: Adriana Molina buscará volver al Concejo para ayudar a "poner en marcha la
  ciudad"
entradilla: 'Encabezará la lista del espacio que dirige José Corral en Juntos por
  el Cambio. Asegura que a la ciudad le falta "liderazgo y planificación". '

---
Adriana "Chuchi" Molina encabezará la lista de concejales de Juntos por el Cambio en el espacio que lidera José Corral para competir en las Paso 2021, el próximo 12 de septiembre. Ya da por sentado que se sumarán competidores; algunos carteles de Mario Barletta junto a Hugo Marcucci en la vía pública parecen anticiparlo: "Es un espacio plural y hay diferentes miradas, como siempre que hay coaliciones, así que probablemente haya internas; y creo que éstas son una oportunidad para los ciudadanos y ciudadanas deban elegir sabiendo que es una instancia más; después se colabora y se trabaja juntos", aseguró la ex secretaria de Gobierno municipal y dos veces concejala.

El bloque renovará tres bancas este año, y aunque la lista está en pleno proceso de conformación, ya se decidió que Molina será secundada por el actual concejal Carlos Pereira y por Ángeles Álvarez, que llega desde la juventud radical. "Será el bautismo de la paridad y en nuestro caso tenemos mujeres de sobra", celebra "Chuchi", una militante de la primera hora en la conquista de derechos.

\-¿Por qué quiere volver a tener un espacio de protagonismo dentro de la política de la ciudad?

\-Quiero ser concejala porque entiendo que la ciudad necesita ponerse en marcha y puedo aportar toda la energía, la experiencia, el compromiso y el diálogo que este momento difícil necesita; colaborar y contribuir a que la ciudad se ponga en marcha. Ese es el gran desafío.

\- Hace más de un año y medio que el equipo no está liderando la gestión de la ciudad ¿Qué hicieron durante este tiempo para no desconectarse de los problemas y preparar la propuesta para el Concejo?

\-Tenemos un equipo técnico. Es muy importante el espacio de Sembrar, porque es donde volcamos toda la experiencia y los recursos que teníamos al terminar la gestión. Estuvimos trabajando mucho allí, siempre en contacto con los vecinos. Y, además, volví a otro de mis amores que es la docencia universitaria, un espacio que también prepara mucho.

\-¿Y cómo ve la ciudad?

\-Lo primero que surge de las conversaciones que tenemos diariamente con vecinos y vecinas es la necesidad de que haya decisiones firmes, que haya liderazgo y un equipo que permita la planificación. Porque si hay algo que este contexto nos ha dado es mucha incertidumbre, personal y colectiva. Es un momento en el que se necesitan determinaciones y respuestas claras. Desde el comerciante o el dueño de un gimnasio que no sabe si va a poder abrir los fines de semana ni en qué horario, hasta el gastronómico que no sabe si comprarle la carne al proveedor para el fin de semana; ni hablar en educación. Por supuesto que hay decisiones que son del Presidente o del Gobernador, pero también el municipio tiene mucho para hacer para aliviar la situación de quienes vivimos en la ciudad. A través de una planificación que dé certezas. Hay responsabilidades y hay que hacerse cargo de estas situaciones y hay que tomar las decisiones.

También se observan problemas cotidianos vinculados a la prestación de los servicios: iluminación, limpieza, transporte público, control del tránsito. Y hay que planificar de otro modo, siempre con la idea de colaborar, sin poner trabas para ver cómo hacemos para que esto se ponga en marcha y para que en definitiva vivamos mejor.

Además, ya tenemos que ponernos a pensar en la ciudad pospandemia porque todos los paradigmas urbanísticos en el mundo están cambiando, y de modo vertiginoso. Algunos llegaron para quedarse y además se aceleran, pienso en usos de los entornos, de los espacios, en las costumbres, en la movilidad ligada al teletrabajo, las políticas de cuidado y la gestión de riesgo vinculado a lo ambiental. Temas que tenemos que ponernos a pensar ya porque son cambios que tienen impacto en las sociedades, pero sobre todo en los centros urbanos. Esto que descubrimos de la cercanía, son cuestiones que hay que pensarlas y trabajarlas, y tenemos la oportunidad de nuestros equipos y en eso estamos trabajando.

\-En el Concejo hay que construir acuerdos con otros para que esos proyectos prosperen. ¿Lo ve posible con los actores actuales, más allá de la nueva conformación post elecciones?

\-El diálogo sustentado en propuestas es central, pero en eso tenemos experiencia. Cuando José fue presidente del Concejo éramos minoría, y siendo oficialismo. Fue complejo, pero tuvimos que encontrar acuerdos.

\-La diferencia es que ahora no serán gobierno.

\-No es algo que me parezca un obstáculo para mí. El punto es tener propuestas, porque sobre eso trabajás y construís. Por supuesto que será complejo porque probablemente será con los otros tres espacios que hoy están representados.

\-Ustedes compartieron espacio político con el Frente Progresista, que hoy gobierna la ciudad. ¿Creen que es posible volver a construir diálogo y a coincidir en políticas públicas con los ediles de ese espacio?

\-Creo que nosotros tenemos que tener este objetivo de que la ciudad se ponga en marcha.

\-Parece difícil si ya les están diciendo que todavía no la pusieron en marcha...

\-Será encontrando acuerdos y propuestas; tenemos que hacerlo en conjunto porque todos representamos diferentes actores y queremos que las personas vivan mejor. Las elecciones son una instancia de decisión, entonces quizás aparecen más las distintas miradas, pero siempre con esta intención de cooperación y de consenso para lograr los acuerdos necesarios.

**PUERTAS ADENTRO**

\-¿Cómo es el diálogo dentro de Juntos por el Cambio, con los otros referentes que también aspiran a ingresar al Concejo, con el sector de Mario Barletta y Hugo Marcucci, por ejemplo?

\-Siempre hay diálogo, porque son personas que están en Juntos por el Cambio con quienes hemos compartido proyectos, aunque hoy tenemos miradas diferentes.

**-¿Cuáles son esas diferencias?**

\-Creo que hay una situación donde hoy estamos de este modo, pero aceptamos la pluralidad sin dramatizar la situación de las Paso.

**¿Por qué "Chuchi"?**

Fue su primera secretaria de Gobierno cuando llegó a la intendencia en 2011, tras compartir dos de los cuatro años en los que ella ocupó una banca en el Concejo. Hoy, José Corral vuelve a elegir a Adriana Molina.

**-¿Qué condiciones destaca?**

\-J.C.: La ciudad necesita un liderazgo de gobierno local sobre los temas que planteamos. Y si bien a veces no cuenta con todos los recursos, sí está la posibilidad de reclamar y de liderar frente a los otros niveles del Estado. Y en "Chuchi" veo esa condición de liderazgo natural y de empatía con la gente. Además, tiene experiencia, fue secretaria de Gobierno, dos veces concejal, es profesora universitaria, es una activa defensora de los derechos de la mujer. Y tiene esa impronta que se necesita de aportar en un sentido constructivo, siempre como colaboración, porque es un momento para que todos empujemos para el mismo lado.

Me consta la capacidad de "Chuchi" de construir consenso para lo cual es necesario tener claras tus ideas, pero también empatizar, poder ponerte en el lugar del otro, y Chuchi es la mejor de nosotros para eso porque tiene esa condición de poder acordar y encontrar puntos en común, incluso con quien piensa diferente.