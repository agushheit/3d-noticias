---
category: La Ciudad
date: 2021-03-30T07:14:31-03:00
thumbnail: https://assets.3dnoticias.com.ar/cullen.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: Se realizaron 150 trasplantes renales en el Hospital Cullen
title: Se realizaron 150 trasplantes renales en el Hospital Cullen
entradilla: Fue posible desde la creación de la Unidad de Trasplantes del efector
  y la donación de órganos de pacientes vivos y cadavéricos.

---
El Ministerio de Salud provincial informó que la Unidad de Trasplantes del hospital José María Cullen de la ciudad de Santa Fe llegó a las 150 cirugías renales desde su creación, gracias al trabajo articulado con el CUDAIO. El director del efector, Juan Pablo Poletti, expresó que “es una alegría enorme para nosotros haber concretado este número en un hospital público, trasplantes gratuitos, en personas sin cobertura social y que de igual modo pueda acceder a este cambio de vida, dejando la diálisis y pudiendo trasplantarse para comenzar una nueva vida”.

“Desde que se formó el equipo de trasplante al día de hoy, ha ido evolucionando para que todo lo relacionado a trasplante renal dependa del hospital, prácticamente no hay tercerización, como lo había antes que, por ejemplo, tenía que venir algún instrumentador o cirujano de afuera. Hoy todo el trabajo lo realiza personal del hospital o de CUDAIO”, agregó.

“Quiero agradecer la predisposición del personal del hospital, desde la admisión, internación, camillería hasta quirófano, que ante una urgencia con heridos o traumatizados había que convivir en este área entre la emergencia y un trasplante, aún en la época COVID donde no se suspendió”, concluyó Poletti.

**UNIDAD DE TRASPLANTES**

El jefe de Nefrología y de la Unidad de Trasplante, Mariano Arriola, explicó que “la primera cirugía se hizo con un donante vivo, que a 8 años de la cirugía el paciente está muy bien y, a partir de ahí, en forma exponencial fuimos creciendo en números, ya en el año 2013 hicimos 9 trasplantes, 11 en 2014, hasta llegar al 2020 donde contamos con un promedio de 25 trasplantes. En lo que llevamos del 2021, en tres meses se realizaron ya 7 cirugías”.

Asimismo agregó que “estas cirugías son posibles gracias a la donación de órganos de pacientes vivos, un 20% relacionados o no relacionados genéticamente, y un 80% en lo que respecta a personas fallecidas donde dependemos exclusivamente de la expresión de voluntad de donar por parte de la sociedad para que puedan ayudar a un paciente a salir de diálisis y mejorarles la calidad de vida”.

“Hay que resaltar que estamos con índices de excelencia en cuanto a calidad y a sobrevida del paciente y del trasplante. Esto es evaluado por autoridades del INCUCAI y por el CUDAIO. Eso nos da muchas fuerzas para continuar con el programa y todo lo logramos gracias al apoyo de las autoridades del hospital y del Ministerio de Salud”, indicó el jefe de la unidad.

**CALIDAD DE VIDA DEL PACIENTE**

Al ser consultado sobre la calidad de vida de los pacientes una vez realizado el trasplante, el doctor Mariano Arriola detalló que “la diálisis es un puente entre la enfermedad renal crónica y el trasplante pero el mejor tratamiento para pacientes con esta patología es sin dudas el trasplante y de esta manera el paciente vive mucho más y mejor”.

A su vez, continuó: “Luego se realiza un control en el consultorio postrasplante mensualmente, tenemos un seguimiento de casi 600 personas porque han migrado pacientes de otros programas donde no han tenido cobertura y todos aquellas personas de la región centro norte que se encuentran en esta situación, se controlan con nuestro equipo”.

La Unidad de trasplante informó que continúa mejorando el ingreso del paciente a la lista de espera. “Estamos muy bien en ello, hoy tenemos una lista de 140 a 150 pacientes. Todo es estudiado por los equipos multidisciplinarios lo cual es una tarea muy ardua ya que estas personas son quienes van a recibir las próximas donaciones”, aseguró Arriola.

**TRASPLANTE DURANTE LA PANDEMIA**

Al ser consultado sobre el desempeño de los equipos de trabajo de la unidad de trasplante durante la pandemia, Mariano Arriola indicó que “lo único que se suspendió por unos meses fueron los trasplantes con donantes vivos por recomendación de la Sociedad Argentina de Trasplante y por parte del INCUCAI, pero con los trasplantes cadavéricos continuamos trabajando de forma normal, respetando todos los protocolos, cuando bajaron los números pudimos llevar adelante el programa sin ninguna dificultad”.

“Asimismo, el año pasado en el país hubo una merma del 40% de trasplante y donantes y aquí en el hospital Cullen seguimos trasplantando el mismo promedio, no bajó. Esto es de destacar porque en periodos de pandemia, en donde la salud pública estaba ocupada con todo lo relacionado a COVID, nunca se dejó de prestar la atención necesaria para que los pacientes se pudieran realizar la cirugía en el efector”, concluyó.