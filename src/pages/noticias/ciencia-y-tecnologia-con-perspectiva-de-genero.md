---
category: Agenda Ciudadana
date: 2020-12-12T12:22:10Z
thumbnail: https://assets.3dnoticias.com.ar/zoom.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Convocatoria para la presentación de proyectos de CIENCIA Y TECNOLOGÍA CON
  PERSPECTIVA DE GÉNERO
title: CIENCIA Y TECNOLOGÍA CON PERSPECTIVA DE GÉNERO
entradilla: La provincia presentó la convocatoria para la presentación de proyectos
  de ciencia y tecnología con perspectiva de género. La inscripción está abierta hasta
  el 21 de diciembre.

---
El gobierno provincial presentó la **Convocatoria a Proyectos de Ciencia y Tecnología con Perspectiva de Género, destinada a grupos de investigación e instituciones públicas o privadas santafesinas, sin fines de lucro**, del sistema ciencia, tecnología e innovación, cuya inscripción permanecerá abierta hasta el 21 de diciembre.

Del encuentro, realizado por videoconferencia, participaron autoridades de la Secretaría de Estado de Igualdad y Género, el Ministerio de Producción, Ciencia y Tecnología y de la secretaría de Ciencia, Tecnología e Innovación.

En la oportunidad, la secretaria de Ciencia, Tecnología e Innovación, Marina Baima, señaló que “planteamos con fuerza la temática del género en la ciencia y la tecnología y en diálogo con la Secretaría de Estado Igualdad y Género, porque representa la intención de nuestro gobernador (Omar Perotti) de pensar de forma sólida, sostenida y con hechos la política de género, en todas las áreas de gobierno”.

“Debemos generar las condiciones para que más mujeres y diversidades logren transformaciones desde su lugar, articulando y conociendo las áreas de investigación, desarrollo e innovación (I+D+i), porque con datos actualizados generaremos una ciencia de calidad y de impacto para el desarrollo, es decir, vamos a poner en valor a la ciencia santafesina”, explicó Baima.

Por su parte, la subsecretaria de Relaciones Institucionales de la Secretaria de Estado de Igualdad y Género, Lorena Battilana, afirmó que “el gobierno a provincial tiene como objetivo la transversalidad de la perspectiva de género. El gobernador y la secretaria Celia Arena trabajan articuladamente con el resto de los ministerios”, aseguró.

Respecto de la convocatoria para la presentación de proyectos, la funcionaria sostuvo que “estamos entusiasmadas porque creemos que superará nuestras expectativas. Nuestra meta es la igualdad y consideramos que la formación y el trabajo en equipo es el camino”.

Por su parte, Luz Marina Nardone, integrante del directorio del Consejo Nacional de Investigaciones Científicas y Técnicas (Conicet), destacó que “las iniciativas propuestas suman una mirada federal que también necesita de una perspectiva de género y que tenemos que construirla entre todas y todos. Cuando hablamos de producción, gestión y comunicación de proyectos tecnológicos con perspectiva de género, hay que incluir la articulación en la gobernanza para fortalecer las agendas políticas”.

Finalmente, la asesora técnica de la Secretaría de Estado de Igualdad y Género, Constanza Estepa, explicó que los ejes temáticos de la convocatoria “están dirigidos a la generación y al desarrollo de los temas prioritarios para Santa Fe, ampliando la visión de las políticas públicas a través de la incorporación de la categoría «género». Asimismo, se enmarcan en la Agenda 2030 sobre el Desarrollo Sostenible aprobada por la Organización de las Naciones Unidas (ONU) en 2015”.

## **LA CONVOCATORIA**

<span style="color: #cf0000;">**Estará abierta hasta el 21 de diciembre y se financiarán hasta 150.000 pesos por proyecto ganador.** </span>

Esta iniciativa tiene como objetivo promover la equidad de género en las instituciones, organismos y empresas ligadas a la ciencia, la tecnología y la innovación, en pos de aumentar la participación de las mujeres y diversidades en la dirección de proyectos vinculados a la ciencia y la tecnología.

Los equipos de trabajo deberán respetar la paridad de género, es decir, estar compuestos por al menos un 50% de mujeres. Se valorará especialmente aquellos que incluyan población travesti trans entre sus integrantes y los que incorporen jóvenes investigadores/as. El proyecto de investigación deberá ser integrado por no menos de ocho personas, una co-dirección y una dirección.

**Las personas interesadas en postular proyectos, podrán consultar bases y condiciones** [**aquí.**](https://www.santafe.gob.ar/index.php/web/content/view/full/236886/(subtema)/236715)