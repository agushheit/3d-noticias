---
category: La Ciudad
date: 2021-08-04T06:00:11-03:00
thumbnail: https://assets.3dnoticias.com.ar/OPERATIVOJATON.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Jatón: “La vacunación a los jóvenes es una gran noticia para la ciudad”'
title: 'Jatón: “La vacunación a los jóvenes es una gran noticia para la ciudad”'
entradilla: El intendente recorrió esta mañana el centro de vacunación para adolescentes
  de entre 12 y 17 años con factores de riesgo. Hasta el viernes pasado, se inscribieron
  1.755 personas en esa franja etaria.

---
La ciudad de Santa Fe inició la campaña de vacunación contra el Covid-19 a adolescentes entre 12 y 17 años con comorbilidades. En ese marco, el intendente Emilio Jatón visitó esta mañana las instalaciones del Hospital de Niños “Orlando Alassia”, donde funciona el centro de vacunación para más de 1.750 jóvenes de la capital provincial que hasta ahora se inscribieron para recibir la dosis de Moderna.

Luego de recorrer las instalaciones, el intendente destacó: “Es un día muy importante para la ciudad de Santa Fe. Hoy se inició el proceso de vacunación para adolescentes y quería ver cómo está funcionando el operativo y, además, hablar con los chicos y con sus padres”.

Cabe mencionar que en esta etapa se vacuna a los adolescentes con factores de riesgo. “Es muy importante el acompañamiento de los padres, de la familia, los jóvenes no van a venir solos, por eso es necesario hablar con los padres, ver cómo está el dispositivo y empezar a ver la pandemia desde otro lugar, que es del lado de la vacunación, no hay otro camino”, consignó el intendente y añadió: “Yo pido que vean los registros clínicos de este hospital, hay niños de 13 años internados por Covid que la están pasando muy mal”.

En consonancia, el mandatario indicó que “hay personas que creen que todo pasó y eso es peligroso. Hay que seguir cuidándonos, por un lado, y por otro lado, continuar con la vacunación masiva “.

**Centro de vacunación**

La vacunación para esa franja etaria con comorbilidades se realiza en el Hospital Alassia entre las 8 y las 16 horas. Se otorgan entre 550 y 600 turnos diarios, para alcanzar a vacunar a los inscriptos entre martes, miércoles y jueves.