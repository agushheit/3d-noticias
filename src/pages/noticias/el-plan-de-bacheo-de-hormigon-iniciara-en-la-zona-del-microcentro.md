---
category: La Ciudad
date: 2021-01-16T03:43:20Z
thumbnail: https://assets.3dnoticias.com.ar/plan-bacheo-microcentro.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: El plan de bacheo de hormigón iniciará en la zona del microcentro
title: El plan de bacheo de hormigón iniciará en la zona del microcentro
entradilla: El municipio comenzará con el arreglo en Bv. Pellegrini; Avenida Freyre;
  Avenida Dr. Zavalla; Av. JJ Paso; y Rivadavia. Este viernes se inició la licitación
  para la ejecución de la zona de avenidas troncales.

---
Para empezar a erradicar el problema de antaño que presentan en su traza vial las calles y avenidas de la ciudad, este viernes el municipio anunció su plan de bacheo 2021, que comenzará a ejecutarse desde la próxima semana. «El año pasado licitamos un plan de bacheo que se acercaba a los $ 20 millones, y abarca desde Avenida Freyre hasta JJ Paso; de JJ Paso hasta Rivadavia. Se trabajará con asfalto y concreto», informó el intendente Emilio Jatón en conferencia de prensa.

Durante este acto, que se llevó adelante en la Asociación de Comerciantes, Industriales, Profesionales de la Avenida Aristóbulo del Valle (Av. Aristóbulo del Valle 6726), comenzó la licitación con la apertura de sobres de las seis empresas oferentes para ejecutar los trabajos en las avenidas troncales, incluidas en la segunda etapa del plan.

«Uno de los lugares que más impacto tiene es Av. Aristóbulo del Valle donde se trabajará con asfalto y hormigón. La etapa incluye 10 avenidas, un total de 3.000 metros cuadrados, y el plazo de ejecución es entre 4 y 6 meses y comenzaría en un mes y medio», comentó Jatón.

Griselda Bertoni, secretaria de Obras y Espacio Público, indicó que una vez que se termine la licitación y se designe a la empresa adjudicataria, se comunicará el cronograma de trabajos para que comerciantes y ciudadanos en general se preparen por las demoras y cortes que se generarán en el tránsito por las obras pertinentes.

![](https://assets.3dnoticias.com.ar/infografia-16121.webp)

<br/>

## **Sensaciones**

«Una vez alguien puso de adjetivo "Santa Fe, la ciudad de los baches". La última vez que se habló de los baches y arreglos fue a principio de 2018, imagínense cómo se deterioran las ciudades a partir de la vida ciudadana», recordó el intendente y agregó: «nosotros empezamos a trabajar con personal municipal, sabemos que la ciudad está detonada porque la transitamos, y sabemos del impacto que tienen las carpetas asfálticas de la ciudad: se rompen autos; hay accidentes, y eso se tiene que terminar».

Bertoni señaló que «en el microcentro hay dos acciones. La acción municipal que hará el bacheo de hormigón que ya se inició sobre calle San Jerónimo. A la par, la empresa comienza el bacheo asfáltico, para el cual hoy se terminaron de firmar los contratos para que la empresa empiece».

En este sentido, la secretaria de Obras y Espacio Público mencionó que «en el microcentro los baches son infinitos. Sabemos que en el noreste hay 33 mil metros cuadrados de baches, por ejemplo».

Durante el diálogo con la prensa, Jatón anticipó que ya piensan en un segundo plan de bacheo y Bertoni coincidió en que seguirán con planes de bacheo durante los próximos tres años.

<br/>

## **Sectores a intervenir**

* **Etapa 1 | Microcentro:** la zona comprendida será: Bv. Pellegrini; Avenida Freyre; Avenida Dr. Zavalla; Av. JJ Paso; y Rivadavia. Total, a intervenir: 4.500 metros cuadrados.

  <br/>
* **Etapa 2 | Avenidas:** Av. Blas Parera; Av. Aristóbulo del Valle; Av. Peñaloza; Av. Facundo Zuviría; Av. Gral. Paz; Av. Almirante Brown; Av. Presidente Perón; Av. Freyre; Bv. Gálvez; y Av. López y Planes. Serán 3.000 metros cuadrados de obra.

  <br/>
* **Etapa 3 | Noreste:** zona comprendida entre las calles: French; Padre Genesio; Urquiza; y Talcahuano. En total incluirá 2.900 m²

  <br/>
* **Etapa 4 | Este:** la zona aproximada de intervención incluirá: Luciano Torrent; Av. Alem; Belgrano; y Grand Bourg. La extensión de obra será de 2.900 metros cuadrados.