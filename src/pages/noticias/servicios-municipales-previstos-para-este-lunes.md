---
category: Agenda Ciudadana
date: 2022-11-19T10:42:26-03:00
thumbnail: https://assets.3dnoticias.com.ar/municipales.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3d noticias
resumen: Servicios municipales previstos para este lunes
title: Servicios municipales previstos para este lunes
entradilla: Con motivo del Día de la Soberanía Nacional, la Municipalidad informa
  los horarios de los servicios de recolección de basura, cementerio, transporte,
  estacionamiento medido y Mercado Norte.

---
_Con motivo del Día de la Soberanía Nacional, la Municipalidad informa los horarios de los servicios de recolección de basura, cementerio, transporte, estacionamiento medido y Mercado Norte, que se brindarán durante esa jornada._

La Municipalidad informa los servicios que se prestarán durante el próximo lunes 21 de noviembre, por la batalla de la Vuelta de Obligado, acontecida en 1845, oportunidad en que se conmemora el Día de la Soberanía Nacional.

En ese sentido, se indicó que la recolección de basura y el barrido de calles serán prestados con normalidad.

Asimismo, los EcoPuntos permanecerán cerrados, al igual que las sedes del IMUSA.

**Transporte y estacionamiento**

El transporte urbano de pasajeros por colectivo tendrá frecuencias similares a los días domingo. Y con respecto al Sistema de Estacionamiento Medido (SEOM), no estará operativo.

**Cementerio**

En cuanto al Cementerio Municipal, se informa que los horarios de visita serán de 7.30 a 12.30 y de 15 a 18.30. Las inhumaciones, por otra parte, serán de 7.15 a 11.30.

**Corralón y Tribunal de Faltas**

El Corralón Municipal contará con guardias mínimas, al igual que el Tribunal de Faltas (San Luis 3078), que permanecerá abierto de 8 a 13.

**Mercado Norte**

Finalmente, cabe señalar que el tradicional Mercado Norte, ubicado en Urquiza y Santiago del Estero, abrirá sus puertas de 9 a 13 horas.