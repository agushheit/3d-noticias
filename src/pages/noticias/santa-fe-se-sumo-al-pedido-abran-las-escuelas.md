---
category: La Ciudad
date: 2021-02-10T07:39:38-03:00
thumbnail: https://assets.3dnoticias.com.ar/9397a8d0-58cd-4f89-a6f6-162046d98623.jpg
layout: Noticia con imagen
link_del_video: https://youtu.be/yCyh-W0j0SQ
author: 3D Noticias
resumen: Santa Fe se sumó al pedido "Abran las Escuelas"
title: Santa Fe se sumó al pedido "Abran las Escuelas"
entradilla: El reclamo se desarrolló frente al ministerio de Educación. “Es imprescindible
  el retorno”, plantearon los dirigentes de Juntos por el Cambio que encabezaron la
  actividad.

---
Con clases abiertas en distintos puntos del país, la propuesta impulsada por Juntos por el Cambio y a la que se sumaron todos los dirigentes de la oposición, junto a docentes, estudiantes y padres, se realizó una jornada de reclamo para promover el regreso de la presencialidad en las escuelas.

La iniciativa se replicó en más de 100 lugares "simbólicos" a nivel federal. En la ciudad de Santa Fe, el encuentro se realizó frente al ministerio de Educación, y en Rosario, sobre el Monumento a la Bandera.

En la capital provincial, participaron referentes de Juntos por el Cambio (UCR, Coalición Cívica, PRO) que habían promovido la iniciativa en una conferencia de prensa días atrás, pero también padres y madres de alumnos agrupados bajo el lema Padres Organizados, miembros de centros de estudiantes secundarios que concurren a escuelas de la ciudad y público en general.

![](https://assets.3dnoticias.com.ar/6d1dd09d-bad5-401e-89ac-a9a9d59dade5.jpg)

Desde el sector, sostienen que las clases de forma presencial se vieron suspendidas durante todo el 2020, y el acceso al sistema virtual mostró las desigualdades existentes en los alumnos, ya que **menos de un 50% tuvo acceso por internet a las clases virtuales.**

En la clase abierta, el exintendente José Corral manifestó que “este año la presencialidad en la escuela debe ser la regla, y la virtualidad una excepción. Pero para eso es necesario planificar: ya no se puede decir que la pandemia nos sorprendió. Ha transcurrido más de un año con escuelas cerradas”.

Por su parte, Mario Barletta consideró que la alternancia de la asistencia, de una semana en la escuela y otra no, "no es lo más adecuado", y explicó que la idea del encuentro es canalizar junto a los padres la necesidad de la vuelta a las escuelas.

También mencionó que en junio presentó el proyecto "Volver a las aulas" e insistió con que en el marco de la virtualidad se profundizaron las desigualdades educativas entre los chicos.