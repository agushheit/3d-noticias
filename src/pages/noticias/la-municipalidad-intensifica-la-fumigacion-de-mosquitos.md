---
category: Agenda Ciudadana
date: 2021-02-10T07:05:50-03:00
thumbnail: https://assets.3dnoticias.com.ar/fumigacion.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad de Santa Fe
resumen: La Municipalidad intensifica la fumigación de mosquitos
title: La Municipalidad intensifica la fumigación de mosquitos
entradilla: Se reforzaron los trabajos para controlar los vectores de mosquitos en
  los espacios verdes. Asimismo, se detalla el cronograma de fumigación que se realiza
  en toda la ciudad.

---
Si a las altas temperaturas se agregan lluvias intensas, el resultado es mayor cantidad de mosquitos. Por tal motivo, la Municipalidad dispuso un operativo especial de fumigación en los parques, paseos, plazas, ciclovías y espacios verdes de toda la capital provincial.

Al respecto, el secretario de Ambiente municipal, Edgardo Seguro, detalló que “se trabaja durante todo el año para controlar la presencia de los mosquitos”. No obstante, añadió que “con las altas temperaturas y las precipitaciones registradas, aumenta la cantidad de mosquitos ya que hay una eclosión del vector”.

Seguro mencionó que para bajar la cantidad de mosquitos, el municipio preparó un cronograma de fumigaciones en toda la ciudad. Sin embargo, aclaró que esa campaña no tiene relación con el combate del mosquito que transmite dengue: “El aedes aegypti se encuentra sólo en las casas, no en la vía pública; se trata de dos mosquitos totalmente diferentes”, especificó.

En ese sentido, el funcionario remarcó la importancia de descacharrar los hogares para evitar la presencia de los transmisores de dengue. “Hay que llevar tranquilidad a los vecinos, informar que se trata de mosquitos diferentes y recordar que para dengue no está indicada la fumigación, salvo que haya casos positivos”, concluyó.

![](https://assets.3dnoticias.com.ar/fumigacion1.jpg)

**Cronograma de fumigación**

Las tareas se realizan con equipos móviles y mochilas. “Los fumigadores recorren las plazas y espacios verdes en función de la presencia del vector. Cuando baja el vector disminuye la fumigación, lo cual es importante porque si se fumiga excesivamente, se acaba con los depredadores del dengue y estos dejarían de estar en los hogares para trasladarse a los espacios naturales”, relató el secretario.

_Los lunes por la mañana_: Ciclovia III completa, desde Alem hasta Cassanello por Vélez Sarsfield. Estación de trenes FFCC. Belgrano, andenes, la zona del Liceo y Espacios verdes sobre Vélez Sarlfield. En tanto, por la tarde: Las Flores I y II, calle Europa entre Estanislao Zeballos y Regimiento 12 de Infantería, Europa al 7900 hasta Gorriti, callejón Funes, callejón El Sable, Peñaloza y los parques y paseos de calle Blas Parera.

_Los martes por la mañana_: Parque del Sur, Estación Mitre y los espacios verdes circundantes. La Baulera y la zona del Convento de San Francisco. En tanto, por la tarde: Parque Garay, cantero central de bulevar Pellegrini y Naciones Unidas, hasta el zanjón Suipacha inclusive.

_Los miércoles a la mañana_: barrios La Esmeralda y Las Delicias. En tanto, por la tarde: Distrito Noroeste, Altos del Valle, Nueva Santa Fe y Favaloro.

_Los jueves a la mañana_: ciclovías, Parque Federal, La Redonda y el ex-Jardín Botánico. En tanto, por la tarde, Predio Ferial, Estación Terminal de Ómnibus, Molino Marconetti y las plazas y los espacios verdes aledaños.

_Viernes por la mañana:_ Costanera Oeste, desde el monumento al Brigadier hasta la rotonda de Artigas, plaza de la Basílica de Guadalupe, Paseo Mutis, Escuela Avellaneda y Parque de la Locomotora. Por la tarde: ambos carriles de la Costanera Este, el Paseo Néstor Kirchner, la zona de confiterías y los espacios verdes linderos, y barrio El Pozo.

Vale recordar que en caso de lluvia se suspende el servicio.