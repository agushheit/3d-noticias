---
category: Agenda Ciudadana
date: 2021-02-23T05:58:03-03:00
thumbnail: https://assets.3dnoticias.com.ar/pelotero.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de LT10
resumen: Vuelven a abrir los peloteros a partir de marzo
title: Vuelven a abrir los peloteros a partir de marzo
entradilla: Después de un año recuperan la actividad restringida por la cuarentena
  por coronavirus. Se trata de uno de los últimos rubros en reabrir sus puertas.

---
Luego de un año de inactividad, el 1º de marzo abrirán sus puertas los peloteros de la ciudad de Santa Fe. Es uno de los sectores, junto con los salones de fiestas, que más demora tuvieron en la reactivación. Pese a los reclamos que realizaron durante 2020 y este año para poder retomar recién ahora lograron la habilitación para hacerlo.

Los propietarios de peloteros presentaron durante meses protocolos a la provincia y al municipio santafesino pero la negativa del gobierno era reiterada. Muchos debieron cerrar y dedicarse a otro rubro ante la situación económica desfavorable que atravesaron el año pasado, pese a la ayuda económica otorgada por la Provincia.

Fabrizio, dueño de un local para festejos de cumpleaños infantiles, a través del móvil de LT10 manifestó su conformidad con la vuelta, y precisó que comenzarán trabajando con protocolos de bar. En principio, será con el 50% de la ocupación y llevando adelante todas las medidas sanitarias.

Al respecto, Fabrizio mencionó que debido a eso "no veíamos cual era el desentendimiento entre municipalidad y la Provincia, ya que Rosario y Rafaela estaba trabajando con protocolo de bar y utilización de juegos y nosotros no".

Según el comerciante, hubo una comunicación entre ambos gobiernos. "Nos dieron una habilitación provisoria, con protocolo de bar, y en los próximos días vamos a recibir la habilitación como peloteros. El ministerio de Salud ya dio el visto bueno para eso, ahora pasa a Trabajo que deberá aprobar el protocolo exclusivo para peloteros", detalló sobre los pasos a seguir.