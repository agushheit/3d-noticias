---
category: Estado Real
date: 2021-01-26T10:02:11Z
thumbnail: https://assets.3dnoticias.com.ar/restricciones_nocturnas.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: La provincia evaluó los controles para garantizar el cumplimiento de la restricción
  de circulación nocturna
title: La provincia evaluó los controles para garantizar el cumplimiento de la restricción
  de circulación nocturna
entradilla: Funcionarios del Ministerio de Seguridad se reunieron con autoridades
  municipales del Gran Santa Fe, de la Policía provincial y de las Fuerzas Federales.

---
El gobierno provincial, a través del Ministerio de Seguridad, mantuvo una reunión con autoridades de los municipios del área metropolitana de Santa Fe, de la Policía de de la Provincia y de las Fuerzas Federales, para analizar y mejorar las medidas implementadas para garantizar el cumplimiento del decreto que restringe la circulación nocturna.

Al respecto, el coordinador de la secretaría de Seguridad Social, Facundo Bustos, detalló que “seguimos aplicando el decreto 6/21 del gobernador Perotti que adhiere al decreto nacional, mediante el cual restringe la circulación de personas en determinados horarios, de 1:30 a 6:00 los fines de semana y feriados y los restantes días se debe cumplir la restricción vehicular de 00:30 a 6:00. A partir de esto, estamos realizando chequeos dinámicos en la vía pública en pos del cumplimiento del decreto y también que no se estén realizando fiestas clandestinas”.

En este sentido, Bustos mencionó que “este fin de semana tuvimos que desarticular una, en conjunto con la Municipalidad de la ciudad de Santa Fe, en calle Aguado al 8000, en la cual se aprehendió al organizador de la fiesta”.

Por su parte, el funcionario señaló que “la ciudadanía está tomando conciencia acerca de que la pandemia sigue vigente y que si bien teníamos un achatamiento de la curva, vimos con preocupación cómo fueron subiendo los casos paulatinamente y logramos que esto no se transforme en un rebrote”.

De la reunión participaron la jefa y subjefa de la Unidad Regional I, Marcela Muñoz y María Laura Ponce, respectivamente; representantes de Prefectura Naval, Policía Federal y Gendarmería; y funcionarios de los municipios del área metropolitana.