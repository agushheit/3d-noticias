---
category: La Ciudad
date: 2021-01-07T10:10:49Z
thumbnail: https://assets.3dnoticias.com.ar/070121-costanera.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Diario UNO'
resumen: El municipio ya tiene casi listo el esquema de controles para el fin de semana
title: El municipio ya tiene casi listo el esquema de controles para el fin de semana
entradilla: 'El intendente Emilio Jatón espera poder tener este jueves 7 la reunión
  que se suspendió ayer con la ministra de Salud, Sonia Martorano. Allí le presentaría
  el diseño de los operativos. '

---
**Se aguardan las medidas provinciales sobre restricción de circulación vehicular y de personas.**

El municipio de Santa Fe ya tendría casi cerrado el diseño de todos los operativos de control y prevención para evitar aglomeraciones de personas en espacios públicos durante el fin de semana en un momento donde volvieron a subir los contagios de coronavirus. Ese esquema, que hace foco en las playas santafesinas –si es que el clima lo permite–, se terminará de adaptar a las disposiciones que tome el gobierno provincial luego de lo que fue la reunión de los gobernadores con el presidente Alberto Fernández.

Por el momento, todos están esperando que se cristalicen en un decreto las definiciones que se mencionaron en el encuentro  que tuvieron Fernández y los gobernadores este miércoles al mediodía. 

A partir de entonces, la Provincia ajustaría los detalles de las restricciones a la circulación que ya adelantó la ministra de Salud, Sonia Martorano, y eso podría tener algún impacto en el esquema de controles que el municipio diagramó para evitar que mucha gente se congregue en los espacios públicos, como ya sucedió en fines de semanas anteriores.

**El principal punto que preocupa a las autoridades es Playa Este.** Por ese motivo, este miércoles al mediodía se realizó una reunión entre funcionarios provinciales, municipales y referentes del Sindicato Único de Guardavidas de Santa Fe (Sugara). 

Allí **se acordó mayor presencia policial y de agentes municipales**, en lo que se avanzó en las primeras horas de la tarde. Además, también se sumarán médicos y enfermeros municipales a los operativos para que ayuden a bajar el mensaje de la necesidad de que los ciudadanos tomen conciencia de la importancia de tener cuidados más estrictos para evitar que aumenten los contagios en la ciudad.

Este miércoles, también al mediodía, estaba previsto un encuentro que el intendente Emilio Jatón le había solicitado a la ministra de Salud provincial. Sin embargo, la reunión de Fernández con los gobernadores cambió los planes y todo se suspendió.

Ahora esperan poder tener este jueves la chance de realizar una reunión de trabajo –que aún no está confirmada– donde **el municipio le entregaría a Provincia el diseño de los operativos para este fin de semana y esperan recibir información sobre lo que se dispondrá sobre las restricciones vehiculares y de personas**. 

Pero en el gobierno provincial están esperando la redacción del decreto nacional para tener un marco de contención a las determinaciones que se tomen en Santa Fe.

Hasta el momento, **lo único que trascendió es que el Presidente les sugirió a los gobernadores implementar restricciones de circulación entre las 23 y las 6 horas**. El detalle de esas medidas podría tener un impacto directo, por ejemplo, en actividades como la gastronomía y el esparcimiento. 

Si el encuentro entre Jatón y Martorano se produce este jueves, se espera que el viernes se comuniquen en conferencia de prensa todas las medidas.