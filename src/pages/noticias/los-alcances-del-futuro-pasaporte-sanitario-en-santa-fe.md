---
category: La Ciudad
date: 2021-12-02T06:00:36-03:00
thumbnail: https://assets.3dnoticias.com.ar/pasaportesantafe.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Los alcances del futuro pasaporte sanitario en Santa Fe
title: Los alcances del futuro pasaporte sanitario en Santa Fe
entradilla: 'Si bien resta conocer la normativa nacional, en Santa Fe el pasaporte
  sanitario se implementaría para evento masivos y no para bares y restaurantes.

'

---
Desde ya hace varios días, en la previa de la reunión del Consejo Federal de Salud (Cofesa) que se realizó en Buenos Aires la pasada semana, y que contó con la presencia del secretario de Salud de Santa Fe, Jorge Prieto, la Provincia viene insistiendo con la importancia de la aplicación del pasaporte sanitario en todo el territorio santafesino.

Este miércoles por la mañana, el ministro de Trabajo, Empleo y Seguridad Social, Juan Manuel Pusineri, se refirió a la llegada del futuro pasaporte sanitario a la provincia de Santa Fe y sus alcances.

"La primer definición es que va a existir pasaporte sanitario; solo resta conocer la normativa de orden nacional referida a su implementación", sostuvo Pusineri.

"La provincia de Santa Fe va adherir a la normativa nacional que se implemente para el pasaporte sanitario, documento que va a alcanzar fundamentalmente a los eventos que requieran masividad, sean lugares abiertos o cerrados", manifestó el ministro de Trabajo y continuó agregando: "En una primera instancia, el pasaporte sanitario no va alcanzar a la actividad del polo gastronómico como bares y restaurantes. Es decir que, la normativa nacional que disponga su funcionamiento, va a enfocarse en eventos culturales, discotecas y salones de fiesta, actividades donde exista contacto físico entre los participantes".

Según indicó el funcionario provincial, "el pasaporte sanitario se incluiría en todos los estadios de fútbol de Santa Fe, pero son eventos que incluyen exclusivamente normativas nacionales".

Respecto de los eventos deportivos, como los partidos de fútbol, Pusineri manifestó que “también se encontrarán alcanzados por el pasaporte, con la diferencia que en este tipo de eventos rige la normativa nacional. Para el resto de las actividades mencionadas, se requiere específicamente una normativa provincial”.

Respecto de la situación particular de la actividad de los salones de fiesta, el ministro recordó que durante casi dos años se han ido brindando distintas respuestas a las inquietudes manifestadas por los empresarios de eventos: “Con motivo de la pandemia hemos tenido un diálogo permanente que nos ha permitido encontrar distintas alternativas: a través de subsidios económicos, el sistema de aforos, los horarios”.

Finalmente, Pusineri subrayó que “la situación en el ámbito de salud es buena pero somos prudentes frente a las nuevas variantes que van apareciendo. Nuestra mayor aspiración es que todas las actividades se puedan llevar adelante con la mayor libertad y flexibilidad posibles”.