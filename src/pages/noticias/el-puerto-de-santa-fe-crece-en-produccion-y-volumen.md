---
category: El Campo
date: 2021-01-25T09:13:17Z
thumbnail: https://assets.3dnoticias.com.ar/puerto.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de la Provincia de Santa Fe
resumen: El puerto de Santa Fe crece en producción y volumen
title: El puerto de Santa Fe crece en producción y volumen
entradilla: La actividad productiva no se detiene en el Puerto Santa Fe, hoy se cargan
  buques y barcazas con maíz quebrado y a partir de febrero comenzarán las exportaciones
  de trigo.

---
Durante el 2020 el Puerto de Santa Fe reactivó la actividad productiva en su Terminal Agrograneles, cerrando el año con 23 embarques concretados y más de 60.000 toneladas exportadas, solo por agua; ya que también se han consolidado y exportado productos y subproductos en contenedores, que finalmente salieron en camiones.

La semana pasada se cargaron 2 barcazas por un total de 3.000 toneladas, 1.500 toneladas en cada una. Este viernes, se cargaron otras dos barcazas por un total de 3.200 toneladas.

En relación al ritmo de la actividad portuaria, el presidente del Ente Administrador del Puerto de Santa Fe, Carlos Arese, comentó que “hoy están funcionando en simultáneo, tres partidoras de granos. Recordemos que comenzamos trabajando con una sola, pero la necesidad de tener listas las cargas en tiempo y forma nos llevó a incorporar otras dos más. Y ya estamos analizando la posibilidad de incorporar una cuarta”.

"Desde el inicio de la gestión, el gobernador Omar Perotti nos pidió trabajar en la reactivación portuaria, y así lo hicimos con el apoyo constante del Ministerio de Infraestructura, Servicios Públicos y Hábitat, que conduce la ministra Silvina Frana, porque el puerto de Santa Fe venía desarrollando diferentes actividades, pero no potenciaba su perfil productivo ni de exportación. Actualmente estamos transformando esa realidad", agregó.

Sobre la recepción de los primeros camiones con trigo de la cosecha 20/21, Arese aseguró que “además del maíz, la soja y el sorgo, hoy tenemos más de 20.000 toneladas de trigo en los silos del puerto, que a partir del mes que viene, todo ese trigo también va estar saliendo en buques o barcazas por agua. Las perspectivas para el año que comienza son muy alentadoras, los silos están casi al límite de su capacidad de acopio y los clientes muy conformes”.

**PUERTO PRODUCTIVO**

El Puerto trabaja con exportaciones de pymes de la región, que encuentran en Santa Fe una posibilidad de sacar sus productos por agua sin necesidad de intermediarios. “Nos gusta decir que este es el puerto productivo de las pymes, nos alegra mucho saber que damos oportunidades a los pequeños productores”, finalizó Arese.