---
category: La Ciudad
date: 2020-12-11T16:20:09Z
thumbnail: https://assets.3dnoticias.com.ar/presos.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Diario UNO'
resumen: Buscan a cuatro presos que se fugaron de la Comisaría de Santo Tomé
title: Buscan a cuatro presos que se fugaron de la Comisaría de Santo Tomé
entradilla: La quema de colchones que ocurrió en la tarde del domingo fue la pantalla
  para seguir limando barrotes y terminar en la fuga de la madrugada de este viernes.

---
Los sucesos ocurrieron durante la madrugada de este viernes, cuando los **presos alojados en la Comisaría 12ª de Santo Tomé** lograron romper las rejas del penal, ascender a los techos y escaparse.

Si bien los policías celadores lograron percibir los movimientos en los techos, actuaron en consecuencia cuando varios de estos empezaron a escaparse. Allí se trenzaron en lucha y **de los siete que escaparon lograron recapturar a tres**, mientras otros agentes con armas largas rodeaban la dependencia para evitar que la totalidad de los presos logre evadirse.
<br/>

## **Quienes aún están siendo buscados por la justicia son:**

<br/>

**Gonzalo Exequiel Chía** de 24 años quien estaba condenado por robo calificado;

**Jorge David Fernández** de 19 años, detenido por robo;

**Cristian Abel Rodríguez** de 19, detenido por tentativa de robo calificado y portación de arma de fuego;

**Lautaro Moncada** de 20, detenido por tentativa de hurto calificado y lesiones dolosas.
<br/>
<br/>

[![Mira el video](https://assets.3dnoticias.com.ar/video.jpg)](https://assets.3dnoticias.com.ar/videopresos.mp4 "Reproducir")