---
category: Agenda Ciudadana
date: 2022-03-04T10:10:01-03:00
thumbnail: https://assets.3dnoticias.com.ar/united-nations-gb47d2d4a7_1920.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Noticias Argentinas
resumen: La ONU investigará si hubo violación de los derechos humanos en la invasión
  a Ucrania
title: La ONU investigará si hubo violación de los derechos humanos en la invasión
  a Ucrania
entradilla: 'Lo aprobó por amplia mayoría y con el voto de la Argentina en el Consejo
  que atiende ese área dentro del organismo internacional. '

---
El Consejo de Derechos Humanos de la ONU aprobó este viernes por amplia mayoría la creación de una comisión que determinará si el Ejército de Vladimir Putin cometió crímenes de lesa humanidad en Ucrania. 

La comisión independiente está conformada por tres expertos para verificar si es cierto lo que denuncia Kiev, en el sentido que constituyen crímenes de guerra y se atenta contra los derechos humanos. La resolución del Consejo, formado por 47 países, fue aprobada con 32 votos a favor, 13 abstenciones y únicamente dos votos en contra (Eritrea y Rusia).

En el debate previo a la votación, iniciado el jueves, la alta comisionada de las Naciones Unidas para los Derechos Humanos, Michelle Bachelet, señaló a Rusia por atacar objetivos civiles, incluidos escuelas, hospitales y zonas residenciales. También se mencionó que todo esto causó ya el éxodo de más de dos millones de desplazados internos y refugiados.

Una de las ciudades castigadas es Kharkiv, cuyo centro administrativo civil fue bombardeado y el edificio quedó destruido, provocando la muerte de varios empleados y decenas de heridos. La ciudad sigue asediada por las fuerzas rusas y los bombardeos se sucedieron sin que los objetivos rusos fueran claros, indican los medios internacionales. El Kremlin define las operaciones como “militares”.

La Argentina votó a favor de la resolución que el Consejo de Derechos Humanos de la ONU, un organismo que este año preside Buenos Aires. Esto significa entonces, que la Argentina designará a los integrantes de la mencionada comisión.