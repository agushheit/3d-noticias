---
category: Agenda Ciudadana
date: 2021-03-11T06:31:56-03:00
thumbnail: https://assets.3dnoticias.com.ar/escuelas.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: 'El Gobierno dará un mayor impulso a las Becas Progresar 2021: beneficiará
  a 750 mil estudiantes'
title: 'El Gobierno dará un mayor impulso a las Becas Progresar 2021: beneficiará
  a 750 mil estudiantes'
entradilla: El Gobierno decidió invertir 27.000 millones de pesos en el programa Becas
  Progresar 2021, por lo que se producirá un incremento en los montos que reciben
  750 mil estudiantes.

---
El Gobierno del presidente Alberto Fernández decidió hoy invertir 27.000 millones de pesos en el programa Becas Progresar 2021, por lo que se producirá un incremento en los montos que reciben 750 mil estudiantes que van desde el 40 hasta más del 163 por ciento, en tanto que se adicionarán dos cuotas, alcanzando así 12 pagos en el año.

En ese marco, el jefe de Estado recibió en su despacho de la Casa Rosada al ministro de Educación, Nicolás Trotta, y a la directora ejecutiva de la Anses, Fernanda Raverta.

Luego, ambos ofrecieron detalles del programa Becas Progresar 2021 en una conferencia de prensa.

El plan de asistencia financiera busca acompañar a 750 mil jóvenes en esta primera etapa para que cuando terminen sus estudios primarios o secundarios continúen en la educación superior o se formen profesionalmente.

"En 2021 se adicionan dos cuotas más a todas las líneas de Progresar, alcanzando 12 pagos por año. Las becas de educación obligatoria se incrementarán un 106% y los cursos de formación profesional alcanzarán un 147%", indicó Trotta.

Además, detalló que, por ejemplo, "las carreras estratégicas para los niveles terciario y universitario aumentarán un 68%".

Por su parte, Raverta indicó que "es una decisión política del Gobierno nacional acompañar a los chicos en todos sus trayectos vitales", en este caso en la educación.

"Lo que queremos hacer es acompañar a quienes hayan decidido estudiar, darle continuidad a su trayecto vital", afirmó Raverta, y agregó que es "compromiso y obligación del Estado acompañar" a los estudiantes que lo necesiten.

El programa dependiente del Ministerio de Educación, fue creado en 2014 con la finalidad de garantizar la igualdad de oportunidades en el ámbito educativo para que los alumnos puedan darle continuidad a sus estudios.

Busca fortalecer el ingreso, la permanencia y el egreso de los estudiantes, promover a la finalización de la educación obligatoria y fomentar la educación superior y los cursos de formación profesional.

Para eso, se otorga una ayuda económica, que guarda relación con la instancia educativa en que se encuentren, y acompañamiento sociopedagógico para quienes cumplan determinados requisitos según el nivel.

Progresar cuenta con cuatro líneas de becas: Progresar Superior, para estudiantes terciarios y universitarios; Progresar Obligatorio, para la finalización de la escuela primaria y secundaria; Progresar Enfermería y Progresar Trabajo, para cursos de formación profesional.