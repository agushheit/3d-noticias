---
category: Agenda Ciudadana
date: 2021-09-17T06:15:02-03:00
thumbnail: https://assets.3dnoticias.com.ar/FMI.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Al no incluir pagos al FMI, la prioridad del Presupuesto 2022 es un crecimiento
  económico inclusivo
title: Al no incluir pagos al FMI, la prioridad del Presupuesto 2022 es un crecimiento
  económico inclusivo
entradilla: Desde el Ministerio de Economía explicaron que el eventual pago de los
  vencimientos por US$ 20.000 millones generaría una "reducción de la inversión en
  infraestructura por $1.300.000 millones.

---
El proyecto de Presupuesto 2022 prioriza un esquema de política fiscal expansiva orientado a la recuperación económica, con crecimiento inclusivo, posible por la no inclusión del pago de los vencimientos por US$ 20.000 millones (4% del PBI) previstos con el Fondo Monetario Internacional (FMI) para el año que viene.

Fuentes del Ministerio de Economía resaltaron el "brutal enorme daño que le haría a la Argentina tener que pagarle al FMI los compromisos que asumió el gobierno anterior".

En ese sentido, precisaron que el pago de los más de US$ 19.000 millones en 2022 implicaría "eliminar todo el gasto de capital desde los actuales 2,4 puntos del Producto Bruto Interno a cero, y los subsidios a la energía y el transporte".

También significaría "eliminar la AUH, el programa Potenciar Trabajo, las Becas Progresar, todos los programas sociales y partidas como la Tarjeta Alimentar, no comprar vacunas, no realizar transferencias a provincias y universidades.

"Sería una situación de descalabro macroeconómico totalmente insostenible y de gran angustia social", agregaron.

Si se previera el pago de la totalidad de los vencimientos se deberían reducir asignaciones, programas sociales, y partidas asociadas a la Salud y Educación

"Este proyecto de ley está pensado para cuidar a la Argentina y asegurar un crecimiento sostenible, y por ello supone que no se enfrentan los vencimientos de deuda insostenibles acordados por la administración anterior", señaló el texto del proyecto de ley girado al Congreso Nacional.

Se aclara, además, que si se "previera el pago de la totalidad de los vencimientos (del acuerdo celebrado con el FMI en el 2018), se colocaría al Estado Nacional en una grave situación con una carga de pagos insostenible de alrededor de US$ 19.000 millones, lo que obligaría a postergar prioridades de políticas públicas esenciales".

De hecho, si esos vencimientos se presupuestaran, debería "propiciarse una reducción significativa en partidas presupuestarias que resultan prioritarias para atender los objetivos de política que han distinguido la acción de gobierno de esta Administración, generando probablemente una crisis social de magnitudes absolutamente desproporcionadas en relación a nuestra historia".

El eventual pago de esos vencimientos generaría una "reducción de la inversión en infraestructura por $1.300.000 millones y los subsidios económicos por $1.100.000 millones".

Además, se deberían "reducir asignaciones, programas sociales, y partidas asociadas a la Salud y Educación".