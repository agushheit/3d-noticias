---
category: La Ciudad
date: 2021-04-17T06:00:25-03:00
thumbnail: https://assets.3dnoticias.com.ar/COMPOST.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: La ciudad tiene su primera compostera comunitaria
title: La ciudad tiene su primera compostera comunitaria
entradilla: Tiene entre sus objetivos disminuir la cantidad de residuos que se lleva
  a disposición final del relleno sanitario, y causar el menor impacto en el cambio
  climático.

---
Con el desafío de reducir el impacto ambiental y la generación de basura que contamina el medio ambiente, la RedCicladores (un colectivo que articula el espacio Igualdad Santa Fe e integran las asociaciones Dignidad y Vida Sana, Capibara y Fridays For Future, más ciudadanos independientes) impulsó la creación de la primera compostera comunitaria en Santa Fe.

Joaquín Azcurrain, coordinador de RedCicladores, dialogó con El Litoral sobre esta iniciativa que apunta a compostar residuos orgánicos en el barrio Escalante, más precisamente en la plaza homónima (en 9 de julio y Milenio de Polonia).

"Venimos trabajando en la propuesta integral de gestión de residuos, ecológica y participativa. Buscamos que el proyecto tenga una fuerte impronta de participación ciudadana y vecinal", resaltó Azcurrain, y agregó: "Queremos que la ciudadanía tenga un rol en todo lo que tiene que ver con un nuevo modelo de gestión de residuos, y que se haga fuerte hincapié en la reducción".

La presentación de esta compostera se da en el marco del "mes del compostaje", que se conmemora entre el 22 de marzo (Día del Agua) y el 22 de abril (Día de la Tierra). "El jueves empezamos con el armado de la compostera, que se hizo con materiales reciclables, sobre todo con pallets: es como un gran cajón con tapa. Resolvimos que para esta primera etapa sea un grupo de vecinos capacitados que se encargue de 'alimentar' la compostera, porque hay muchos residuos que no se pueden tirar, por ejemplo, la carne, alimentos para perros, verduras condimentadas tampoco", mencionó, y aclaró que se colocará un cartel para indicar qué es lo que se permite y lo que no (ver infografía).

**Puesta en marcha**

La prueba piloto de la primera compostera comunitaria nació tras el contacto con vecinos de los barrios Escalante y Fomento 9 de Julio. "Vimos experiencias a nivel municipal que hay en el país y las ventajas son muchas. Hay un aspecto que tiene que ver con la reducción y sacar basura de la calle, por un lado; y de llevar la menor cantidad de residuos al relleno sanitario, para evitar la sobrecarga y el adelantamiento de la vida útil, por el otro", señaló el coordinador de RedCicladores. A su vez, precisó que entre los otros objetivos planteados proyectan la creación del "empleo verde" y de causar el menor impacto en el cambio climático.

"En la propuesta que vamos a elevar al Concejo Municipal el próximo lunes, la reducción en términos de compostaje está planteada en tres pasos: composteras domiciliarias y comunitarias, para las que pedimos una política de promoción desde el municipio. El tercer paso sería a una escala mayor de compostaje y apunta a algo industrial, con una planta de compostaje en el predio del relleno sanitario, a un costado de la planta de clasificación de los residuos secos. Esto daría la posibilidad de generar 'empleo verde'", detalló Azcurrain.

Sobre la utilidad que se le puede dar al material orgánico que genere esta compostera comunitaria, el coordinador indicó que "el cajón se puede abrir por la parte de abajo y de ahí puede utilizarse la tierra para los usos que se precise, ya sea para alguna maceta en la que se quiera poner una planta; un relleno en el patio es para un uso más doméstico". Por otro lado, en una compostera a escala industrial -según las experiencias observadas en otros municipios-, se utiliza la tierra para rellenar caminos o vender a quinteros.