---
category: Agenda Ciudadana
date: 2021-05-29T06:15:26-03:00
thumbnail: https://assets.3dnoticias.com.ar/restricciones.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Cuáles serán las restricciones a partir del lunes en la provincia de Santa
  Fe
title: Cuáles serán las restricciones a partir del lunes en la provincia de Santa
  Fe
entradilla: 'En principio, volverían a entrar en vigencia las medidas que había anunciado
  en su momento el Gobierno provincial. Se aguardan precisiones sobre algunas actividades. '

---
Este domingo 29 de mayo llegará a su fin el confinamiento estricto dispuesto por el presidente Alberto Fernández. Por ende, volverían a entrar en vigencia las restricciones que había anunciado en su momento el Gobierno provincial y que luego fueron suplantadas por las nacionales.

De mantenerse las mismas medidas, desde el lunes lo comercios podrán volver a atender al público hasta las 17 y los locales gastronómicos hasta las 19. En tanto, quienes comercialicen productos alimenticios también podrán atender hasta las 19.

La circulación vehicular estará limitada las 24 horas para personas que cumplan funciones laborales o quienes tengan un justificativo de fuerza mayor. Sí se podrá usar el transporte público.

Mientras que se aguardan precisiones sobre la actividad en clubes o gimnasios. El decreto la prohibía, pero luego se les había permitido funcionar con ventilación cruzada o al aire libre.

También se restringen las reuniones sociales en domicilios particulares, salvo para el grupo conviviente y para la asistencia de personas que requieran especiales cuidados y los encuentros sociales en espacios públicos al aire libre. A su vez, en el decreto habían sido suspendidas todo tipo de actividades y eventos religiosos en lugares cerrados.

En materia de educación, el próximo lunes podrían volver las clases semipresenciales. El regreso dependerá de cómo continúe la situación epidemiológica.