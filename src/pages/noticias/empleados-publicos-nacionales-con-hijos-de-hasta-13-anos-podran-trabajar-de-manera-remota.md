---
category: Agenda Ciudadana
date: 2021-01-06T10:43:34Z
thumbnail: https://assets.3dnoticias.com.ar/060121-trabajo-remoto.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Telam'
resumen: Empleados públicos nacionales con hijos de hasta 13 años podrán trabajar
  de manera remota
title: Empleados públicos nacionales con hijos de hasta 13 años podrán trabajar de
  manera remota
entradilla: 'Abarca a las jurisdicciones, entidades y organismos de la administración
  pública nacional, empresas y sociedades del Estado o con participación estatal mayoritaria. '

---
Las y los agentes de las jurisdicciones, entidades y organismos de la administración pública nacional, empresas y sociedades del Estado o con participación estatal mayoritaria que tengan hijos a cargo de hasta 13 años inclusive «deberán realizar sus tareas de modo remoto» a partir de este martes.

Así **lo dispuso el gobierno nacional al actualizar el sistema laboral dispuesto en el marco de la pandemia de coronavirus**, se informó oficialmente.

Esos trabajadores que «cuenten con hijos o hijas, tutelados o tuteladas, o familiares menores a cargo de hasta trece (13) años de edad inclusive, deberán realizar sus tareas de modo remoto, excepto necesidades de servicio indicadas por la autoridad superior», explica la resolución.

De esta manera, la Secretaría de Gestión y Empleo Público, que depende de la Jefatura de Gabinete de Ministros, dispuso una actualización del artículo 4º de la Decisión Administrativa Nº 390/20, que se implementó el 16 de marzo de 2020, en la que se habilitaban las licencias para madres y padres del sector público con hijos en edad escolar.

La secretaria de Gestión y Empleo Público, Ana Castellani, indicó que «la licencia se otorgó al inicio de la pandemia porque entendimos que las madres y los padres tenían que acompañar a sus hijos en el proceso de escolarización y se hacía muy difícil compatibilizar ambas tareas en el hogar».

En este sentido, explicó: «ahora que hay receso escolar, entendemos que pueden realizar las tareas de manera remota».

En declaraciones a Télam, Castellani planteó que «acompañar el seguimiento pedagógico de los chicos en las casas y, además, dar una carga de trabajo a los adultos, era muy difícil de sostener».

«Lo que hicimos –sabiendo que hay receso escolar y mientras dure ese receso– fue levantar la licencia y garantizarles a los padres de hijos de menos de 14 años la realización de sus tareas de manera remota», añadió la funcionaria.

En la resolución se explicó que «lo que se establece con esta nueva norma es que quienes tengan menores a cargo de hasta 13 años de edad pueden cumplir sus tareas preferentemente desde sus hogares, excepto en los casos que, por necesidades derivadas de la esencialidad del servicio que prestan, sean convocados de modo presencial por las autoridades superiores de la jurisdicción».

En esta nueva disposición «se establece que el límite de edad dispuesto (hasta 13 años inclusive) no será considerado en caso de agentes con hijos o hijas, tutelados o tuteladas a cargo que posean certificado de discapacidad oficial, para quienes continúa la prestación laboral preferente por trabajo remoto».