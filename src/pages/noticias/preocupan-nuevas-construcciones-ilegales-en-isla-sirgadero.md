---
category: La Ciudad
date: 2021-06-20T06:00:16-03:00
thumbnail: https://assets.3dnoticias.com.ar/sirgadero.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Preocupan nuevas construcciones ilegales en Isla Sirgadero
title: Preocupan nuevas construcciones ilegales en Isla Sirgadero
entradilla: Sobre el riacho Santa Fe, en un predio usurpado, se multiplican las instalaciones
  ilegales, incluso se está construyendo una rampa para bajada de lanchas sin habilitación.

---
En un terreno usurpado, en cercanías del Yacht Club Santa Fe, proliferan los nuevos asentamientos, con rellenos de esa zona sin control de los elementos depositados, con todas consecuencias del posible daño ecológico e hídrico. A los emprendimientos comerciales ya existentes, ahora se suma la construcción de una rampa para bajada de lanchas que deberían contar con la correspondiente habilitación municipal, de ambiente y catastro de la provincia y, que aseguran autoridades del club cercano, “no cuentan con nada de eso, nadie controla nada”.

“En su momento, estos mismos usurpadores, además, destruyeron quincho, sanitarios y demás infraestructura que el club había construido, lo que motivó una causa judicial, actualmente en trámite; y, desde el 20 de mayo, se puede observar en el lugar un cartel que hace referencia a la Asociación Santafesina de Piraguas. A todo ello, se suma que el sector se encuentra en división de condominio”, aseguran desde Yacht Club.

En ese predio, se había decidido mantener un “lugar isleño cercano”, con toda su forestación autóctona y su costa para realizar actividades recreativas del Yacht Club, como las colonias de vacaciones infantiles y campamentos donde se inculcaban los valores de cuidado de la geografía isleña, que ahora se ve afectada por la ocupación irregular.