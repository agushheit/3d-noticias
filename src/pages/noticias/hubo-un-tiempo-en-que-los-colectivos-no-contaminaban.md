---
category: Agenda Ciudadana
date: 2022-12-02T10:22:33-03:00
thumbnail: https://assets.3dnoticias.com.ar/biodiesel.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: " Hubo un tiempo en que los colectivos no contaminaban."
title: " Hubo un tiempo en que los colectivos no contaminaban."
entradilla: Un repaso por la experiencia biodiesel 100, en la ciudad de Santa fe.

---
A comienzos de 2019, el Gobierno de la Provincia de Santa Fe, a través de la resolución conjunta 002/2019 de la secretaria de Transporte y 094/19 de la Secretaria de Energía, estableció un cronograma para la implementación en el transporte público por colectivos en la provincia del proyecto B100, para llegar a fin de 2019 con todas las unidades utilizando biodiesel al 100%. El mismo contaba con CUATRO fechas, a modo de metas temporales:

. 01/06/2019, 25% de las unidades deben funcionar con biodiesel.

. 01/08/2109, 50% de las unidades deben funcionar con biodiesel.

. 01/10/2019, 75% de las unidades deben funcionar con biodiesel.

. Finalmente, el 100% de la flota debería usar biodiesel a partir del 1/12/2019.

El 28 de marzo de 2019 el gobernador de la Provincia de Santa Fe presentaba las conclusiones de las experiencias del uso del biodiesel, plasmado luego en tres documentos: Informe final B25; Informe final B100 y Experiencia Biodiesel. A partir de entonces, se conformaron mesas de trabajo con las empresas de transporte de la provincia, municipios y expertos del ministerio de transporte provincial. A modo de síntesis de dichos informes, antes de abocarnos al caso de nuestra ciudad, podemos resaltar:

v Se utilizaron dos modelos de unidades VW 18.280EOD y MB OH1621, para los 6 meses de prueba del combustible.

v En cuanto al rendimiento, la diferencia en el consumo fue del 4% comparado con el Gas Oíl, valor que el informe considera “prácticamente indetectable”.

v El Biodiesel puro B100 no interfirió en el normal funcionamiento de las unidades afectadas ni en los plazos indicados por el fabricante para renovación de filtros y fluidos, ni la calidad de los elementos mencionados.

v Por último, se detallan las ventajas observadas en la utilización del B100:

v Una particularidad del biodiesel es su lubricidad. De hecho, se lo considera un “aditivo” lubricante. Contribuye a reducir el desgaste mecánico y tiende a prolongar la vida útil del motor.

v Contiene oxígeno incorporado que genera una mejor combustión con menores contaminantes en los gases de escape, y una reducción importante en los humos de escape.

v Se degrada cinco veces más rápido que el gasoil convencional, al punto que puede ser usado como disolvente frente a derrames de gasoil mineral.

**La experiencia en la ciudad de santa fe: datos del sistema en el año de implementación (2019).**

El parque móvil total de la ciudad, estaba compuesta por 270 unidades, entre flota activa y de reserva, distribuidas de la siguiente manera: Empresa Recreo SRL, 43%; Autobuses Santa Fe, 18% y el restante 39% de la flota total de la Empresa Ersa. El total de kilómetros recorridos por dicha flota rondan los 15 millones de kilómetros anuales que en general, consumen 6.600 mil litros de gasoil (0,44 lts. /km). - En la estructura de costos del sistema, el gasoil implica el 15% del total de los costos anuales. Para completar los datos de contexto, durante el 2019 se realizaron 36.985.442 viajes.

De acuerdo a las consultas realizadas a las empresas prestatarias del servicio, durante el mes de enero del 2020, tanto Recreo como Autobuses, declaraban que el 100% de sus flotas activas y de reserva utilizaban el Biodiesel, incluso se hizo antes de los plazos previstos en las normativas.

Sin embargo, la empresa Ersa, presento algunos inconvenientes a la hora de la implementación. En notas elevadas a las autoridades provinciales de aplicación, detallaron los inconvenientes sufridos. El 15/05/2019, solicitaron prórroga para cumplimentar la primera etapa (25% de las unidades), la cual fue aprobada el 1/06/2019. Sin embargo, aseguraban que, para la conclusión de la segunda etapa, ya funcionaban el 100% de su flota con dicho combustible. Es decir, que antes del 1/12/2019, fecha de la última etapa, ya se utilizaba el biodiesel.

El 22 de octubre de 2019, la empresa presenta la nota 1744/2019 solicitando se les exima del uso del biodiesel de forma inmediata. Argumentan, pérdida de potencia en los motores mayores a la informada en los estudios previos; incremento “importante” en el consumo de aceite, desgaste prematuro de aro de pistón y disminución drástica de la vida útil de los motores. Si embargo, los problemas de rendimiento en la performance mecánica, es discutible atribuirlos al combustible, puesto que los mismos modelos de chasis y motores se utilizan en las distintas empresas en la ciudad, y como ya expresamos, las empresas Autobuses y Recreo usan en el 100% de la flota el bio.

Entro otros argumentos presentados en la nota mencionada, incluían el incremento en los costos, tanto por la necesidad de uso de aceite sintético en lugar el mineral, que “incrementa el precio un 57%”. Además, se acortan los periodos de reemplazo de lubricantes. Se incrementa el costo de mantenimiento de las instalaciones de combustible. Incremento del consumo de combustible del orden del “14%” respecto al consumo de Gasoil. Por último, aduce que el incremento del precio del biodiesel desde el inicio de la implementación a la fecha de presentación de la notal, fue del 54%.

El uso del BIODIESEL AL 100% se abandonó a mitad del AÑO 2020, sin explicaciones.