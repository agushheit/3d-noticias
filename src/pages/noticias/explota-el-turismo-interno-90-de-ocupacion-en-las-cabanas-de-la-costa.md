---
category: Agenda Ciudadana
date: 2020-12-06T11:59:12Z
thumbnail: https://assets.3dnoticias.com.ar/cabanas.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: LT10'
resumen: 'Explota el turismo interno: 90% de ocupación en las cabañas de la Costa'
title: 'Explota el turismo interno: 90% de ocupación en las cabañas de la Costa'
entradilla: Tras la habilitación de la actividad en la provincia, se disparó la demanda
  en el corredor de la ruta provincial N°1. Los empresarios del sector anticipan un
  verano con mucha actividad.

---
El gobierno habilitó desde este sábado el turismo interno en Santa Fe, y el procedente de otras provincias a partir del 21 de diciembre. Los resultados de la decisión no se hicieron esperar: en las cabañas del corredor de la ruta 1, se estima una ocupación por arriba del 90% para este fin de semana largo.

Así lo indicó por LT10 Guillermo Kees, referente del sector. "Hay muchísima demanda. Va a estar espectacular", celebró, y recordó que los complejos debieron permanecer cerrados por nueve meses, a raíz del aislamiento preventivo y obligatorio dispuesto desde el inicio de la pandemia de coronavirus.

El empresario comentó que están recibiendo "muchísimas consultas", y no solo procedentes de Santa Fe, sino también de Entre Ríos, Córdoba, Buenos Aires y Salta.

"Es el momento para disfrutar a pleno la Costa santafesina", sugirió, y anticipó un verano con mucha actividad. "Creo que todos los cabañeros y demás prestadores de servicios turísticos vamos a estar muy ocupados", se ilusionó.

Acerca de la histórica bajante del río, aseveró que de todas formas "el Paraná es majestuoso" y que "todo el conjunto de islas para ir navegando hasta llegar a él tiene una riqueza visual inigualable".

Asimismo, describió: "hay muchísima fauna; es impresionante cómo se ha recuperado el verde en estos últimos 10 días. Es realmente increíble".

Finalmente, consultado respecto a los precios, informó que para alquilar una cabaña se está cobrando entre 800 y 1.000 pesos por persona por día, y que a aquellos turistas que pauten siete noches se les regala una.