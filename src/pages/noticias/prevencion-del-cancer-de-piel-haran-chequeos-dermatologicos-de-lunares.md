---
category: La Ciudad
date: 2021-10-17T06:15:34-03:00
thumbnail: https://assets.3dnoticias.com.ar/LUNARES.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'Prevención del cáncer de piel: harán chequeos dermatológicos de lunares'
title: 'Prevención del cáncer de piel: harán chequeos dermatológicos de lunares'
entradilla: Será durante dos días. La iniciativa es impulsada por el laboratorio La
  Roche-Posay con el apoyo del municipio local.

---
"Chequeo de lunares y prevención del cáncer de piel" es la campaña que se desarrollará en la ciudad de Santa Fe los días 18 y 19 de octubre: el lunes se hará en la explanada de la Estación Belgrano (Bv. Gálvez 1150), y el martes en la intersección de Suipacha y San Martín, de 10 a 18 horas ambas jornadas.

 Esta iniciativa se realiza de forma gratuita en 25 puntos del país, y es impulsada por el laboratorio La Roche-Posay, en conjunto con distintos gobiernos locales y con el aval de la Asociación Argentina de Oncología Clínica (AAOC). El espacio contará con tres consultorios en simultáneo con capacidad para realizar 72 chequeos diarios. Cada consulta será de 15 minutos, con un intervalo de cinco minutos entre pacientes para la sanitización de los consultorios.

 El cáncer de piel representa un tercio de los casos de cáncer detectados anualmente en el país, un total 200 mil casos nuevos por año. Sin embargo, el 90% de los casos pueden ser tratados si se detectan a tiempo, por lo que es clave realizarse chequeos de lunares con periodicidad. Este tipo de cáncer es el más curable pero es esencial la educación, prevención y detección temprana.

 Desde la Dirección de Salud municipal advierten que el daño solar es acumulable, y las exposiciones reiteradas e indiscriminadas producen a largo plazo un envejecimiento prematuro, lesiones precancerosas y cáncer de piel. La forma más eficaz para prevenir el cáncer de piel es la educación: saber cómo, cuándo y cuánto exponernos al sol.

 El cáncer de piel es curable en la mayoría de los casos, por eso es importante conocerse y hacerse un autoexamen en forma periódica. Si se observan o encuentran manchas nuevas o que se hayan modificado, se debe consultar con un especialista.

 **Cómo proteger la piel**

*  Evitar la exposición solar directa entre las 10 y las 16.
* Ponerse a la sombra.
* Usar sombrero de ala ancha.
* Utilizar anteojos con filtro UV certificado y camisa con mangas largas.
* Usar cremas protectoras solares de calidad reconocida que bloqueen radiaciones UVA y UBV, y reponer cada 2 horas.
* No exponer al sol a los chicos de menos de 1 año.
* Realizar un chequeo dermatológico.