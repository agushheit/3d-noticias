---
category: La Ciudad
date: 2021-03-26T08:25:38-03:00
thumbnail: https://assets.3dnoticias.com.ar/colectivos.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de El Litoral
resumen: 'Colectivos y nocturnidad: citan a dos funcionarios municipales'
title: 'Colectivos y nocturnidad: citan a dos funcionarios municipales'
entradilla: Ediles de la oposición pidieron que asistan el subsecretario de Movilidad
  y Transporte, Lucas Crivelli, y la secretaria de Control y Convivencia municipal,
  Virginia Coudannes.

---
Con dos decretos aprobados, el Concejo convocó al subsecretario de Movilidad y Transporte, Lucas Crivelli, y a los miembros del Órgano de Control, a un encuentro de trabajo con el objeto de que se brinden detalles sobre la actual situación del sistema de transporte público de pasajeros por colectivos en la ciudad. La iniciativa, de Carlos Pereira (UCR-Juntos Por el Cambio).

Al fundamentar su iniciativa, el legislador afirmó que "el transporte público por colectivos se encuentra atravesando la peor crisis de los últimos casi 20 años" y sostuvo que esa situación se debe, en gran medida, "a la falta de capacidad de la actual gestión municipal".

"Los graves problemas de financiamiento del transporte público y el recorte de frecuencias arrancaron antes de la llegada del Covid y luego se vieron agravados por el impacto de la pandemia", sostuvo el concejal de la UCR en Juntos por el Cambio. Y recordó que "el gran recorte de frecuencias se inició a partir del 1 de Enero de 2020, con una reducción en la cantidad de coches en circulación que llegó al 50% en comparación con las unidades que circulaban hasta el 2019".

"Después de 15 meses de asumida la gestión actual no hemos podido intercambiar con los responsables del área del Ejecutivo una sola opinión ni mirada, del principal problema que debe afrontar la Municipalidad de Santa Fe", señaló a continuación. Y agregó que, en ese contexto, el Ejecutivo se limitó a deslindar responsabilidades: "Esta gestión municipal no tuvo ni la capacidad de negociación ni la voluntad política para defender los subsidios nacionales. El Gobierno Nacional decidió el congelamiento de la tarifa para el 2020 y, mientras ciudades como Rosario vieron crecer el subsidio en una cifra cercana al 200 %, en Santa Fe llegó a un 20%".

 

**Controles nocturnos**

Asimismo, a instancias de Juan J. Saleme (bloque PJ), también se convocó a la secretaria de Control y Convivencia municipal, Virginia Coudannes, a una reunión informativa para abordar pautas de control en la ciudad -en el marco de la emergencia sanitaria por la Covid-19-, "particularmente con relación a las actividades habilitadas y el cumplimiento de controles en la nocturnidad".

Saleme pidió "días, no meses", para que convoque y se concrete la reunión con la funcionaria. "Realmente estamos preocupados porque nos está costando trabajar sin información; aprobamos pedidos de informes pero no los responden (desde el Municipio) o los responden de una forma muy escueta, que no sirven para nada. Sin información, no podemos legislar para los vecinos". Expresó su malestar por la falta de diálogo con el Gobierno local, y recordó otra convocatorias (por ejemplo, a la responsable de Riesgo Hídrico de la ciudad), que aún no se concretaron. 

Y Suárez apuntó: "Seguramente todos tuvieron oportunidad de ver un recital en el Anfiteatro Juan de Garay; aquí preocupa la situación respecto de los protocolos. Vemos las reuniones sociales en la Plaza Pueyrredón, las 'picadas' y los ruidos en la Costanera. No digo que sea fácil gobernar en el contexto sanitario actual, pero creo que (Coudannes) puede contar todo aquello que le compete a su función de control". Sugirió "encontrar un camino para que haya una gestión más dinámica de la pandemia, con más controles" (en las reuniones públicas).