---
layout: Noticia con imagen
author: "Fuente: Diario UNO"
resumen: Caso Fernando Báez Sosa
category: Agenda Ciudadana
title: Piden que vayan a juicio los ocho rugbiers detenidos acusados de matar a
  Fernando Báez Sosa
entradilla: La fiscal Verónica Zamboni pidió el juicio de los 8 jóvenes
  detenidos por el hecho junto a sobreseimiento de otros dos que están libres.
date: 2020-11-19T12:00:25.245Z
thumbnail: https://assets.3dnoticias.com.ar/rugbiers.jpeg
---
La fiscal que investiga el crimen de Fernando Báez Sosa, el joven asesinado a golpes a la salida de un boliche de la ciudad de Villa Gesell en enero pasado, pidió hoy que sean sometidos a juicio los ocho rugbiers detenidos por el hecho, y dispuso el sobreseimiento de otros dos que se encontraban en libertad, informaron a Télam fuentes de la investigación.

El pedido fue formulado por la fiscal Verónica Zamboni, titular de la Unidad Funcional de Instrucción 5 de Villa Gesell, ante el juez de Garantías David Mancinelli, a un día de que se cumplan diez meses del crimen y de que venciera el plazo para hacerlo.

Zamboni solicitó que los ocho rugbiers que cumplen prisión preventiva por el hecho en la alcaidía 3 del penal de Melchor Romero, en La Plata, sean juzgados por el delito de "homicidio doblemente agravado por alevosía y por el concurso premeditado de dos o más personas", que contempla como pena la prisión perpetua.

Se trata de Máximo Thomsen (20), Ciro Pertossi (20), Luciano Pertossi (19), Lucas Pertossi (Piden21), Enzo Comelli (20), Matías Benicelli (21), Blas Cinalli (19) y Ayrton Viollaz (21), a quienes Zamboni considera coautores del crimen del joven.

![](https://assets.3dnoticias.com.ar/fernando-baez.jpeg)

En su requerimiento, la fiscal pidió además que los dos rugbiers que se encuentran en libertad imputados como "partícipes necesarios", Juan Pedro Guarino (19) y Alejo Milanesi (19), sean sobreseídos, por lo que ahora el juez deberá dar vista a los abogados de la familia de Fernando, Fernando Burlando y Fabián Améndola. Luego, se dará vista a la defensa, a cargo de Hugo Tomei, que tendrá 15 días para realizar eventuales planteos de sobreseimiento, cambio de calificación o causa de justificación, y a continuación el juez tendrá cinco días para expedirse.

Una fuente de la investigación indicó que la fiscal estableció que otro joven mencionado en la causa, identificado como Tomás Colazzo, "no tuvo participación en el hecho".

En caso de disponer la elevación a juicio a partir de este requerimiento, el magistrado deberá consultar a todos los imputados que vayan a juicio si desean hacerlo con un tribunal popular o mediante un juicio oral con tres jueces.

En ese sentido, fuentes judiciales señalaron que la intención de la defensa es solicitar que el debate sea por jurados, tal como contempla la legislación bonaerense para los delitos graves, con penas en expectativa mayores a los 15 años de prisión, como en este caso.

## **El crimen que conmocionó al país**

El ataque de Fernando se produjo en la madrugada del 18 de enero último en pleno centro de Villa Gesell, frente al boliche Le Brique, donde el joven y dos de sus amigos fueron atacados. Fernando recibió golpes y patadas en la cabeza que le provocaron la muerte antes de que pudiera ser atendido por médicos, mientras que sus agresores huyeron del lugar.

El episodio quedó registrado en distintas cámaras de seguridad y en teléfonos celulares de otras personas que se hallaban en la zona, por lo que los sospechosos fueron identificados y detenidos horas más tarde en la casa que alquilaban, a pocas cuadras del lugar del crimen.