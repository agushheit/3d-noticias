---
category: Agenda Ciudadana
date: 2021-10-22T06:15:20-03:00
thumbnail: https://assets.3dnoticias.com.ar/operativo.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Agencia Provincial de Seguridad Vial llevó a cabo un operativo inter fuerzas
  en las rutas nacionales n° 11,34 y 168
title: La Agencia Provincial de Seguridad Vial llevó a cabo un operativo inter fuerzas
  en las rutas nacionales n° 11,34 y 168
entradilla: Las acciones buscan aumentar el nivel de seguridad en el tránsito del
  las rutas de la región; y detectar conductas de riesgo y presuntos delitos a la
  seguridad pública.

---
La Agencia Provincial de Seguridad Vial (APSV) llevó a cabo un operativo extraordinario denominado “Interfuerzas” en las rutas nacionales N° 11, 34 y 168, del que también participaron fuerzas de seguridad de las provincias de Chaco, Corrientes y Santiago del Estero, con el objetivo de reforzar el trabajo preventivo de seguridad y la pronta detección de situaciones irregulares en la conducción vehicular.

Al respecto, el subsecretario de la APSV, Osvaldo Aymo, destacó el “trabajo mancomunado entre las diferentes provincias en corredores viales muy importantes para el ingreso y el traspaso de la ciudad de Santa Fe. Estar presentes en las rutas representa un aporte a la seguridad vial y el resguardo de la vida de todas y todos los santafesinos”, señaló.

La iniciativa se coordinó entre las direcciones generales de la Policía Caminera de la provincia de Chaco, de la Policía Vial de Corrientes, Policía de Seguridad Vial de Santa Fe, y la provincia de Santiago del Estero; quienes dispusieron realizar este operativo simultáneo en las jurisdicciones mencionadas a los efectos de prevenir delitos y/o contravenciones.

La directora de la APSV, Antonela Cerutti, precisó que “el operativo interfuerzas en la provincia de Santa Fe cuenta con 14 puntos distribuidos en las rutas nacionales 34, 11 y 168. Los operativos, que continuarán llevándose a cabo cada 15 días son muy importantes, porque sabemos que salvan vidas”.

El operativo interfuerzas, que se llevó a cabo durante la mañana de este jueves, contó con recursos humanos y logísticos de las Unidades Operativas Regionales dependientes de la PSV de Santa Fe, realizando despliegues operativos de saturación y señalización preventiva, ordenamiento de tránsito, selección y detención de vehículos para la realización del control de documentación, exceso de pasajeros y uso de dispositivos de seguridad, test de alcoholemia, como así el labrado de actas administrativas provinciales encuadradas en las reglamentaciones nacionales y provinciales vigentes.

La directora de la Policía de Seguridad Vial, Marcela Muñoz, señaló que “estamos realizando controles en todos los puntos limítrofes para detectar cualquier irregularidad en la conducción vial y de esta forma reforzar el trabajo preventivo de seguridad. Estamos realizando controles de alcoholemia, ordenamiento de tránsito, y control de documentación para establecer corredores seguros en las rutas”.

Cabe destacar que ya se realizaron experiencias similares a principios del mes de octubre y en septiembre con muy buenos resultados; oportunidad en la que se controlaron 650 vehículos y se labraron 129 actas de infracción.

Además, en el marco de estos operativos, se detectó una solicitud de captura/detención, infracciones a la Ley Nacional de Defensa de la Riqueza Forestal, un aprehendido por tenencia indebida de arma de fuego, y averiguación de procedencia de carga de animales, entre otras irregularidades.