---
category: Agenda Ciudadana
date: 2021-11-16T06:15:20-03:00
thumbnail: https://assets.3dnoticias.com.ar/KRITALINA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: El FMI pide a la Argentina un plan con amplio apoyo político y social, y
  que ataque a la inflación
title: El FMI pide a la Argentina un plan con amplio apoyo político y social, y que
  ataque a la inflación
entradilla: 'Un vocero del Fondo dijo que se continúa trabajando para "llegar a entendimientos
  plenos sobre un plan integral que pueda abordar de manera duradera los desafíos
  económicos y sociales más apremiantes"

'

---
El FMI pidió que la Argentina presente un plan con "amplio apoyo político y social", y que el país ataque la "alta inflación", en la primera reacción tras las elecciones legislativas del domingo.

El organismo consideró "importante" que el plan que negocia con la Argentina para refinanciar una deuda de USD 44.000 millones tenga un fuerte respaldo del arco político.

El presidente Alberto Fernández anunció que enviará al Congreso un proyecto de ley con un plan económico dentro del marco de un programa con el organismo.

Un vocero del FMI dijo que se continúa trabajando para "llegar a entendimientos plenos sobre un plan integral que pueda abordar de manera duradera los desafíos económicos y sociales más apremiantes de Argentina, incluida la alta inflación, que perjudica desproporcionadamente a los más vulnerables".

"Es importante que este plan tenga un amplio apoyo político y social. Nuestro objetivo sigue siendo ayudar a Argentina y a su gente", señaló el vocero.

El pedido de respaldo al plan que reclama el Fondo Monetario está vinculado con que el acuerdo que se podría sellar abarcaría al menos tres períodos de gobierno.

Tras las elecciones, Fernández anunció que enviará al Congreso un proyecto de ley con un plan que incluirá la propuesta de renegociación con el Fondo Monetario.

Fernández sostuvo que "en la primera semana de diciembre de este año, enviaremos al Congreso de la Nación un proyecto de ley que explicite el programa económico plurianual para el desarrollo sustentable".

La nueva ley se deberá tratar ya con la nueva composición del Congreso, debido a que los nuevos legisladores asumirán el 10 de diciembre próximo, y casi con certeza en sesiones extraordinarias.