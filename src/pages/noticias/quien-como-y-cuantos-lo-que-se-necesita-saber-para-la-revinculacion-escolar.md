---
category: Agenda Ciudadana
date: 2022-02-23T11:34:06-03:00
thumbnail: https://assets.3dnoticias.com.ar/clases.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Por Claudia Balagué / Exministra de Educación de Santa Fe - Para el Diario
  La Capital
resumen: 'Quién, cómo y cuántos: lo que se necesita saber para la revinculación escolar'
title: 'Quién, cómo y cuántos: lo que se necesita saber para la revinculación escolar'
entradilla: La diputada provincial analiza por qué no alcanza con acercar listados
  de estudiantes a municipios y comunas

---
Esta semana fue noticia la decisión del gobernador Omar Perotti de “salir a buscar casa por casa a los chicos para traerlos a la escuela”. Inevitablemente recordé los titulares de la prensa, cuando hace 9 años, con el entonces gobernador Antonio Bonfatti, presentamos el plan Vuelvo a Estudiar, que se puso en marcha el 4 de febrero de 2013 y duró hasta el final de nuestra gestión.

Entre ambas noticias, en apariencia similares, las diferencias parecen detalles pero en realidad modifican radicalmente la escena: hacen a que la puerta de la escuela se abra efectivamente o no.

La primera diferencia es la universalidad del plan Vuelvo a Estudiar, es decir su alcance a cada santafesino y santafesina para poder acceder a la secundaria, y no solo a quienes terminaron la primaria el año anterior, en quienes circunscribe su estrategia el gobernador Perotti.

El problema del abandono escolar, de acuerdo con las estadísticas de Santa Fe, publicadas por el Ministerio de Educación de la Nación, se da en la secundaria, con un pico entre tercero y cuarto año (el 10 por ciento), y si hubiera que poner el acento en algún grupo, este sería prioritario para ir a buscarlos casa por casa.

En las altas cifras de abandono, de acuerdo con la especialista Irene Kit, se cuentan jóvenes que debieron asumir roles de adultos, que no pueden lidiar con las pautas organizativas de la escuela o que presentan baja confianza en las propias capacidades de aprendizaje; sumada esta pandemia que mantuvo escuelas cerradas, estudiantes y docentes sin recursos de conectividad, familias golpeadas por la tragedia que ya no pudieron priorizar la educación. A todas esas casas hay que ir una por una.

La segunda gran diferencia salta al preguntar cómo se busca casa por casa. “Vamos a entregar el listado de todos los alumnos que terminan las escuelas primarias y no figuran ingresando a ninguna escuela secundaria a los intendentes y presidentes comunales, para ir a buscar a los chicos a sus casas, para hablar con sus familias y traerlos a la escuela”, dijo Perotti.

Como define el Centro de Implementación de Políticas Públicas para la Equidad y el Crecimiento (Cippec), el abandono es el resultado de un proceso en el que intervienen múltiples factores: de cada estudiante, de su familia, de la escuela a la que asiste y del contexto más general.

Para abordarlo se requiere un equipo especial, preparado para cocrear estrategias con la familia y con la escuela, para acompañar la revinculación, incluso articulando con instituciones territoriales, como lo hicimos con el plan Vuelvo a Estudiar.

También es imprescindible conocer cuántos son los estudiantes que están en esas circunstancias para armar la estrategia. Ese dato no está -o no es público (las estadísticas santafesinas publicadas en el sitio oficial llegan hasta 2019)-. Como referencia, la doctora Claudia Romero dice que en junio de 2020 el Ministerio de Educación Nacional detectó 1,1 millón de chicos que se habían desvinculado de sus escuelas.

¿Qué porcentaje corresponde a Santa Fe? No lo sabemos. Solo expresiones globales, matizadas por valoraciones como la del propio gobernador: “El número de deserción escolar no es preocupante”.

La tercera diferencia hiere el contrato social con la ciudadanía. En nuestro sistema, es el Ministerio de Educacion el que se hace responsable del derecho a la educación. Todos los demás actores pueden y deben colaborar, pero no recibir un listado para hacer lo que puedan, con los recursos que tengan.

El Ministerio de Educación de Santa Fe tenía la estructura, el presupuesto y la experiencia para llevar adelante una política de inclusión que mostró resultados ratificados por las estadísticas públicas, avalados por organizaciones y especialistas nacionales e internacionales, que estudiaron la experiencia, y por las miles de familias que cumplieron el sueño de la secundaria completa: en años de trabajo sostenido se revirtió el abandono escolar a la mitad (pasó del 12 por ciento al 6 por ciento).

¿Por qué se desmanteló? La respuesta es otra diferencia fundamental en la concepción de la responsabilidad para garantizar la educación, dejando de lado los caprichos, las mezquindades políticas y asumiendo el compromiso que la ciudadanía les delega a sus gobernantes.

Se trata de una diferencia vital, en la que se dirime la oportunidad de ser parte del futuro o la condena a la postergación y a más violencia.