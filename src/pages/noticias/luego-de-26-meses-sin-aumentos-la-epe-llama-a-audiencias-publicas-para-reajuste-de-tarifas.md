---
category: Agenda Ciudadana
date: 2021-04-11T06:15:08-03:00
thumbnail: https://assets.3dnoticias.com.ar/TARIFASHOT.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Luego de 26 meses sin aumentos, la EPE llama a audiencias públicas para reajuste
  de tarifas
title: Luego de 26 meses sin aumentos, la EPE llama a audiencias públicas para reajuste
  de tarifas
entradilla: El Ministerio de Infraestructura, Servicios Públicos y Hábitat convocó
  a audiencia pública para escuchar y recabar las opiniones de los santafesinos y
  santafesinas.

---
En este sentido, la ministra de la cartera, Silvina Frana, recordó que para los usuarios residenciales la tarifa no se aumentó en los últimos 26 meses. “Durante 2020, la provincia en línea con el gobierno nacional tomó la decisión de congelarlas debido al fuerte impacto de la pandemia, en lo sanitario, económico y social. Hoy hay un pedido concreto de la EPE de readecuar las tarifas analizando las políticas salariales, por lo que desde el Ministerio estamos convocando a audiencia pública, para estudiar la propuesta y escuchar las opiniones de los santafesinos”.

Por su parte, el presidente de la EPE, Mauricio Caussi, aseguró que “desde el gobierno provincial estamos haciendo un esfuerzo muy grande por cuidar el bolsillo de los santafesinos, por cuidar la competitividad de nuestras pequeñas y medianas empresas. Luego de estos 26 meses vamos a tener un aumento, pero no mirando solamente la ecuación de la rentabilidad de la empresa, sino mirando el bolsillo de la gente y la competitividad de las pymes”, concluyó.

La audiencia pública será presidida por el secretario de Empresas y Servicios Públicos, Carlos Maina, quien tendrá la facultad de conducir el acto, designar un coordinador, impartir instrucciones, efectuar declaraciones y dar las órdenes que considere necesarias para su normal desarrollo.

Cabe destacar que, en primer término, el representante de la Empresa Provincial de la Energía expondrá los fundamentos para modificar las tarifas y luego se continuará con los oradores inscriptos en el registro.

Desde el viernes 16 de abril y hasta el miércoles 21, los interesados en participar de la audiencia pública deberán inscribirse en forma obligatoria en el registro habilitado en la página web del gobierno de la provincia.