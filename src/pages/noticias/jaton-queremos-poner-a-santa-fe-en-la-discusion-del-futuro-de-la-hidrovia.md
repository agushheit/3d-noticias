---
category: La Ciudad
date: 2021-03-24T07:34:44-03:00
thumbnail: https://assets.3dnoticias.com.ar/jaton.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad Santa Fe
resumen: " Jatón: “Queremos poner a Santa Fe en la discusión del futuro de la Hidrovía”"
title: " Jatón: “Queremos poner a Santa Fe en la discusión del futuro de la Hidrovía”"
entradilla: Distintas instituciones y representantes políticos de esta capital se
  reunieron para visibilizar el interés de la gestión municipal por ser parte del
  debate sobre el presente y futuro de la Hidrovía Paraguay–Paraná.

---
El intendente Emilio Jatón encabezó esta mañana una mesa de diálogo que tuvo como objetivo primordial discutir el presente y futuro de la Hidrovía Paraná–Paraguay con miras a la próxima concesión en puerta. La reunión se concretó en la sede del Liceo Municipal, ubicada en el  Puerto de la ciudad.

El encuentro contó con la presencia de integrantes de la Mesa de Entidades Productivas conformada por integrantes de la Bolsa de Comercio, Sociedad Rural, Unión Industrial, Asociación Dirigentes de Empresas (ADE), Cámara Argentina de la Construcción, Cámara de Comercio Exterior y el Centro Comercial. Asimismo, participó por zoom Silvina Frana, ministra de Infraestructura, Servicios Públicos y Hábitat de la provincia de Santa Fe, representantes del Ente Portuario y de la Universidad Nacional del Litoral.

Al término del encuentro, el intendente remarcó la necesidad de que la capital provincial sea tenida en cuenta en la nueva traza. Con respecto al balance de la reunión, el mandatario destacó que el encuentro “fue muy positivo” y aseguró que “queremos poner a Santa Fe en la discusión del futuro de la Hidrovía”.

En ese sentido, Jatón repasó: “Por estas horas, a nivel nacional, se está discutiendo el futuro de este puerto para los próximos 20 ó 30 años. Habrá un proceso licitatorio en el cual podríamos quedar afuera de lo que es el dragado y balizamiento del Puerto, eso sería un destino atroz para la ciudad de Santa Fe”. Añadió que  “por tal motivo, hoy nos juntamos a debatir, consensuar, opinar y conocer todas las voces para ver cómo salimos de esto”.

**Un documento en común**

Una de las decisiones consensuadas durante el encuentro fue la redacción de un documento “que vamos a presentar al Consejo Federal de la Hidrovía pidiendo la participación del municipio de Santa Fe en las discusiones del futuro de nuestro Puerto”, explicó el intendente.

Por su parte, Carlos Arese, presidente del Ente Administrador del Puerto Santa Fe, calificó el encuentro impulsado por el municipio como “excelente” y consignó: “Recién lo conversé con Emilio Jatón; nos pareció una muy buena reunión donde hemos alineado un objetivo común: respaldar al Puerto ante la nueva concesión de la Hidrovía”.

En esa línea, Arese indicó que “se han alineado todas las fuerzas políticas y las fuerzas vivas de la ciudad en torno a que hay que defender el Puerto de Santa Fe, en un momento en el que está activo. Entonces eso nos posiciona mejor ante esta situación histórica que es la licitación de la Hidrovía”, expresó, y añadió: “Quedó claro el apoyo de todos para defender los intereses de la ciudad de Santa Fe”.

**Dragado y balizamiento**

Por otra parte, el presidente del puerto destacó: “Como explicaba el intendente, el dragado del canal de acceso lo viene haciendo el Ente Portuario con recursos propios, es sumamente costoso y resulta engorroso poder hacerlo. Lo que estamos pidiendo es que la nueva concesión de la Hidrovía tenga en cuenta el dragado de los canales de acceso de nuestro puerto y otros puertos, como el de Reconquista”.

Desde la Mesa de Entidades Productivas de la ciudad de Santa Fe se considera que el nuevo contrato para la Concesión del Dragado, Señalización y Balizamiento de la Hidrovía debe contemplar: incrementar el dragado a 33 pies de profundidad hasta el Puerto de Santa Fe; incorporar en este contrato el dragado, señalización y balizamiento del canal de acceso al Puerto de Santa Fe; mantener la tarifa plana, que sostiene un sistema virtuoso favorecedor del desarrollo agropecuario del interior del país; extender la concesión hasta Confluencia (km 1.238), lo que permitiría un desarrollo federal e integrado del país.

**La importancia de la Hidrovía**

Para comprender la escala de la Hidrovía, cabe destacar que es una de las vías navegables naturales de mayor longitud del planeta. Con 3.442 km de longitud, desde Puerto Cáceres (Brasil) hasta Nueva Palmira (Uruguay), incluye a 5 países: Brasil, Bolivia, Uruguay, Paraguay y Argentina. Buenos Aires, Chaco, Corrientes, Entre Ríos, Formosa, Misiones y Santa Fe, son las provincias argentinas que integran este corredor por el ingresan más de 6.000 buques por año.

Constituye la opción de transporte más relevante de nuestra producción agrícola y es la vía de salida de aproximadamente el 70% de la exportación nacional. Por ella se transportan más de 100 millones de toneladas de carga de distintos rubros:

\-     mercadería transportada en contenedores (Puerto de Buenos Aires y Terminal Dock Sud);

\-    la carga de la industria automotriz (Puerto de Zárate); de la industria metalúrgica, en particular su producción siderúrgica;

\-    pasajeros en la modalidad de cruceros;

\-    un gran porcentaje de las operaciones de cabotaje y transbordo de cargas;

\-    gas natural, petróleo crudo y productos refinados, cuyo transporte reviste una importancia vital para el abastecimiento energético de nuestro país.