---
category: La Ciudad
date: 2021-11-18T06:15:54-03:00
thumbnail: https://assets.3dnoticias.com.ar/inflacion.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: La inflación de octubre en Santa Fe fue del 3,2%, la mayor de los últimos
  cuatro meses
title: La inflación de octubre en Santa Fe fue del 3,2%, la mayor de los últimos cuatro
  meses
entradilla: 'El índice inflacionario interanual arrojó un 52,9% respecto a octubre
  de 2020. El rubro alimentos fue el de mayor incidencia en el porcentaje final según
  datos del Ipec.

'

---
El Instituto Provincial de Estadística y Censos (Ipec) comunicó este miércoles cómo fue la inflación registrada en el mes de octubre en la provincia. El índice inflacionario mensual respecto a septiembre fue del 3,2% y es el mayor porcentaje registrado para la bota santafesina en los últimos cuatro meses, luego de que en el mes de junio se haya registrado también un 3,2%.

A su vez, el 3,2% de inflación informado por el Ipec en el mes de octubre ubica a la provincia levemente por debajo de la media nacional. En el plano país, la Argentina había registrado según los datos del Indec un valor inflacionario del 3,5% en octubre.

Según el informe del organismo provincial, el rubro Alimentos fue el que tuvo una mayor incidencia en la conformación del porcentaje inflacionario para el mes de octubre. En el detalle se muestra que este rubro incidió en el 1,05% del 3,2% total. Desglosando en otros sectores se refleja que los mayores aumentos se vieron en Indumentaria, atención médica y gastos para la salud, ambas categorías aumentando un 4,7% respecto a septiembre.

En el número interanual se pone de manifiesto el peor efecto de la inflación en el bolsillo del santafesino. Este índice relevado al mes de octubre dio un 52,9% de inflación respecto al mismo mes de 2020.

Volviendo al rubro alimenticio, el grupo de alimentos que registró una mayor suba en su precio respecto a septiembre fueron las verduras. Según los datos del Ipec, esta categoría aumentó un 10,5% en solo un mes.