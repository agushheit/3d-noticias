---
category: Estado Real
date: 2021-02-21T07:00:29-03:00
thumbnail: https://assets.3dnoticias.com.ar/AGUASRECO1.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Aguas Santafesinas Comenzó Obras Cloacales En Barrios De Reconquista
title: Aguas Santafesinas Comenzó Obras Cloacales En Barrios De Reconquista
entradilla: Los trabajos tienen como fin expandir el servicio en los barrios Itatí,
  El Zorzón, Santa Rosa, Don Pepito y Distrito IT.

---
Aguas Santafesinas comenzó las obras para la expansión del servicio cloacal en los barrios Itatí, El Zorzón, Santa Rosa, Don Pepito y Distrito IT, que se ejecutan con fondos del Estado Nacional en la ciudad de Reconquista. Las obras iniciadas tienen un plazo de ejecución de 15 meses y están a cargo de la firma Proyección Electroluz SRL.

Una vez finalizadas se verán beneficiados 3.000 vecinos tras el tendido de 12.000 metros de cañería cloacal y la ejecución de 653 conexiones domiciliarias.

El emprendimiento es resultado concreto del convenio firmado el año pasado por el gobernador Omar Perotti y el ministro de Obras Públicas de la Nación, Gabriel Katopodis, para realizar obras de acceso al agua potable y saneamiento cloacal en nueve ciudades de la provincia, con un presupuesto de más de 1.450 millones de pesos, aportados por el Estado nacional.

**La Obra**

El proyecto demanda la realización de 74 bocas de registro y 16 cámaras de acceso a cañería de impulsión, la cual dispondrá de cámara disipadora.

Además, se incorporará una nueva estación elevadora de líquidos cloacales con la utilización de 35 metros cúbicos de hormigón armado. La estación estará dotada de dos bombas sumergibles y contará con sala de tableros, sala de filtros de aire, obras civiles y pavimento de acceso.

Por otra parte, cabe recordar que prosiguen las obras en los barrios Villa Clelia y Gral. Obligado que beneficiarán a 2.000 vecinos, contemplan el tendido de 7.000 metros de cañería cloacal de 160 y 200 mm de diámetro, la ejecución de 491 nuevas conexiones domiciliarias y 58 bocas de registro y 5 enlaces a bocas de registro existentes.

Las mismas están a cargo de la firma COCYAR S.A. y tienen un plazo de ejecución de 6 meses.

El conjunto de las obras mencionadas demandará una inversión estimada en 130 millones de pesos.

Las obras permitirán la incorporación al servicio de 6.000 nuevos usuarios lo que representa un incremento del 10% de la actual cobertura del radio de cloacas.