---
category: Agenda Ciudadana
date: 2021-03-04T00:36:57-03:00
thumbnail: https://assets.3dnoticias.com.ar/anses.png
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Clarin
resumen: La ANSeS confirma que sin una nueva moratoria el 96% de las mujeres no podrá
  jubilarse
title: La ANSeS confirma que sin una nueva moratoria el 96% de las mujeres no podrá
  jubilarse
entradilla: El organismo que maneja el sistema previsional confirmó los datos que
  publicó Clarín.

---
Según la ANSeS, si no se extiende y amplía la moratoria que vence en julio de 2022, el 96% de las mujeres que hoy tienen entre 55 y 59 años no podrán jubilarse cuando cumplan 60 años.

El dato figura en un Informe del organismo previsional al que accedió Clarín.  Dice el texto: "Un porcentaje muy importante de las personas que se acercan a la edad de retiro, especialmente las mujeres, nos permite adelantar que, salvo que se implemente una nueva moratoria que extienda el plazo de regularización, irán paulatinamente decreciendo las tasas de cobertura previsional en un futuro inmediato”. Y agrega: “Esta situación recrudece a partir de julio de 2022, cuando la moratoria se dé por finalizada y entonces, el 96% de las mujeres que lleguen a la edad de retiro, no contará con los 30 años de aportes y no podrá acceder a una jubilación”.

Las mujeres pueden jubilarse a los 60 años y los varones a los 65 años, si cuentan con un mínimo de 30 años de aportes.

> Según el INDEC, hay 1.103.603 mujeres de entre 55 y 59 años y 906.232 varones de entre 60 y 64 años.

El Informe explica que “en la actualidad hay vigentes dos moratorias que permiten regularizar años de aportes previsionales. La correspondiente a la Ley 24.476, que quedó abierta con carácter permanente y permite a hombres y mujeres regularizar períodos comprendidos entre el 01/01/1955 al 30/09/1993. La otra moratoria vigente hasta julio de 2022, aunque solamente para las mujeres, es la Ley 26.970, que permite regularizar años de aportes del período comprendido entre el 01/01/1955 al 31/12/2003”.

Así, ejemplifica el Informe: “las mujeres que cumplieron 60 años en el 2020, nacidas en 1960, pueden regularizar hasta 25 años de servicio. Las que cumplieron 59 años van a poder regularizar solo 24, las que cumplieron 58 años, solo 23, y así. Esto demanda que estas mujeres, para poder acceder a su jubilación, tengan 5 años o más de aportes a la seguridad social, por fuera del período 1955-2003.

Lo mismo ocurre para el caso de los varones. Aquellos que cumplieron 65 años en el año 2020, nacidos en 1955, pueden regularizar hasta 20 años de servicio. Los varones que cumplieron 64 años, 19, los que cumplieron 63 años, 18, etc. Es decir, los varones tienen que acreditar como mínimo 10 años de aportes a la seguridad social por fuera del período de regularización”.

Esto pasa, según un Informe de la Secretaría Política Económica, (Dirección Nacional de Economía, Igualdad y Género) porque “además de tener peores salarios en general, las mujeres enfrentan mayores niveles de informalidad (36% versus 34,2%) y mayores niveles de desocupación (10,8% versus 8,9%) que los varones”. Y cargan con el cuidado de los hijos y el trabajo del hogar, lo que la aleja del mercado laboral.

Un caso contundente es el trabajo en casas particulares: sobre un total 1,7 millones, sólo están registradas menos de 500.000. El empleo informal asciende al 70%.

> Para acceder a las moratorias tanto mujeres como varones deben demostrar que están en “vulnerabilidad social” en base a una evaluación socio-patrimonial. Luego,  ANSeS calcula la deuda por los años regularizados que se descuentan de los haberes, en 60 cuotas, ajustables por la movilidad.

No obstante, los que no tengan aportes por haber estado inactivos o desempeñarse en la informalidad o cuenten con 10, 20 o 25 años de aportes, varones y mujeres a los 65 años, por la ley 26.970 aprobada en junio de 2016 pueden acceder a la PUAM (Pensión Universal al Adulto Mayor) que equivale al 80% de la jubilación mínima que en marzo será de apenas $ 16.457 mensuales para todos por igual porque no toman en cuenta los años de aportes que tengan registrados. Y también si demuestran que están en “vulnerabilidad social” en base a una evaluación socio-patrimonial de la familia. La PUAM no da derecho a la pensión por viudez por el fallecimiento del cónyuge.

De aquí se desprende que una mujer con 60 años, aunque cuente con aportes pero que no puede actualmente jubilarse por moratoria, y esté desocupada, deberá esperar 5 años para acceder a la PUAM para cobrar un haber un 20% inferior a la jubilación mínima.