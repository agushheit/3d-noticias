---
category: La Ciudad
date: 2021-06-07T06:15:03-03:00
thumbnail: https://assets.3dnoticias.com.ar/ASADO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: En cuatro años, el asado subió un 434% en Santa Fe y con el salario actual
  se compran 30 kg menos
title: En cuatro años, el asado subió un 434% en Santa Fe y con el salario actual
  se compran 30 kg menos
entradilla: La carne picada y la pulpa aumentaron un 450%. Con el salario mínimo vital
  y móvil de 2017 se podrían comprar 56 kg de asado; este año solo se pueden adquirir
  30 kg, casi la mitad.

---
Los datos son reveladores de una de las conversaciones cotidianas más frecuentes en materia inflacionaria: el aumento de la carne. En cuatro años, los cortes más tradicionales y de mayor consumo tuvieron un aumento promedio en Santa Fe del 435 por ciento. El ranking está liderado por la carne picada especial, que en abril de 2017 costaba $120,57 el kg y que en el mismo mes, pero de este año cuesta $654,88; un incremento del 443 por ciento. Le sigue la paleta, con una suba del 441 por ciento; hace cuatro años costaba $129,38 el kg y ahora $700,23.

Luego llega el asado (costilla) que suele ser para muchos una medida inflacionaria. En abril de 2017, tenía un costo de $143,87; un 434 por ciento menos de lo que sale hoy: $769,30.

El problema, más allá de la fuerte suba, es que los salarios no acompañaron el aumento de precios. En 2017, con el salario mínimo vital y móvil -que estaba en $8.060- se podían acceder a 56 kilos de asado; hoy con ese mismo salario -que está en $23544- se pueden comprar 30 kilos; casi la mitad menos.

Es decir, durante el mismo período de análisis, el salario mínimo vital y móvil creció un 165 por ciento; un 269 por ciento menos de lo que subió el kg. de costilla en Santa Fe.

Le sigue la nalga con un incremento durante el mismo período del 431 por ciento (paso de costar $148,60 a $789,31) y el cuadril con un 428 por ciento (costaba $149,70 en abril de 2017 y ahora sale $791,15)

Las cifras que publicó el diario UNO Santa Fe son oficiales y fueron recabadas de los índices de precio al consumidor publicados por el gobierno de Santa Fe a través del Instituto Provincial de Estadísticas y Censos (Ipec).

Si se amplía el análisis a un total de siete productos, el incremento baja y pasa a ser del 417 por ciento. Los artículos vinculados al rubro que hacen caer el porcentaje total son el hígado que registró un incremento del 367 por ciento (en 2017 salía $40,68 y ahora se consigue a $189,99) y hamburguesas (por 4 unidades) que tuvieron una suba del 376 por ciento (salían $57,71 y ahora $274,74).

**¿Es cara la carne?**

De los cuatro años analizados, se desprende que el salto de precios más importante se produce en los últimos dos, 2020 y 2021. "¿Qué es lo que no está caro en la Argentina”, se preguntó Antonio D’Angelo, vicepresidente de la Cámara de Frigoríficos Provincial?

En diálogo con un medio local, el empresario señaló: "A la la industria de la provincia de Santa Fe la energía le subió el 88 por ciento de abril para mayo. Yo no digo que la carne esté barata, digo que en la Argentina está todo caro; se han perdido los parámetros".

Para D'Angelo, "la carne comienza a ser un bien escaso porque nadie quiere producirlo. Y todo bien escaso aumento sus precios. Entonces, acá empieza a haber factores estructurales, que por más que tengamos el folkore de decir «el argentino no puede perder el asado». Si continuamos así vamos a perder todo, no solo el asado".