---
category: La Ciudad
date: 2021-11-22T06:15:33-03:00
thumbnail: https://assets.3dnoticias.com.ar/SALADA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Santa Fe: ya es ley la prohibición de instalar una "Salada"'
title: 'Santa Fe: ya es ley la prohibición de instalar una "Salada"'
entradilla: El Senado de la provincia aprobó la norma que declara en situación de
  riesgo al sistema comercial y suspende por un año las "ferias internadas"

---
Las pequeñas y medianas empresas del sur santafesino lograron que se dé un paso en lo que consideran una “igualdad de condiciones” para poder comerciar en su territorio de influencia. Es que la Cámara de Senadores de [Santa Fe](https://www.unosantafe.com.ar/santa-fe-a2403.html) le dio sanción definitiva a la ley que declara en situación de riesgo, por el lapso de un año, al sistema comercial de la provincia. Mientras se extienda la duración de esta norma, no podrán instalarse en el territorio las denominadas ferias internadas. Concretamente, la ley es un revés para las intenciones de desplegar una “Salada” en territorio santafesino, y ofrece un "tiempo providencial" para discutir un marco regulatorio equitativo para todos los comercios y empresarios pyme.

El proyecto que aprobó la Legislatura provincial, presentado por el senador del departamento Iriondo, Hugo Rasetto, fue impulsado por pymes y entidades nucleadas en Asociación de Entidades Empresarias del Sur Santafesino (Adeesa) y otras instituciones gremiales empresarias, a fin de poner un freno al avance del “Rey de la Salada”. Con esto, lograron avanzar un escalón fundamental para lograr “un hito en pos de ponerlas en igualdad de condiciones con el resto de los comercios y empresas”, dicen.

Un punto importante de la ley es que establece plazos para promover “el tratamiento y la sanción de una normativa que regule la radicación de establecimientos comerciales de múltiples puntos de venta, denominados ferias internadas, multipunto, cooperativas de comerciantes o de formatos similares, a fin de garantizar la igualdad ante la ley, la trazabilidad de la mercadería, la trazabilidad impositiva, el cumplimiento de todas la obligaciones fiscales, laborales, previsionales y de todo otro requisito normativo de orden nacional, provincial y municipal”.

En rigor, los senadores ya le habían dado el sí a un proyecto de postergación, que pasó a Diputados, donde se aprobó con modificaciones. Y finalmente, el jueves que pasó, la Cámara alta le dio sanción definitiva. “Los diputados querían estar seguros de que las ferias de emprendedores regionales no quedaran enganchadas en esta disposición, de ahí que se introdujeron cambios, que fueron consensuados con los senadores y finalmente se consiguió unificar criterios y sacar la ley”, explicó a Maximilano Ferraro, uno de los vicepresidentes de Adeessa, en diálogo con La Capital.

**Marco regulatorio**

El dirigente empresario recordó que había en Diputados otros proyectos, como los de Maximiliano Pullaro y Juan Cruz Cándido, y que incluso se había aprobado en su momento una norma similar cuando Miguel Lifschitz era gobernador de la provincia. “Ocurre que en aquella época no se terminó de fijar un marco regulatorio, y todo volvió hacia atrás”, rememoró. Y abundó: “Justamente, estos 365 días nos dan tiempo para sentarnos de nuevo y estudiar ese marco. Fueron muchas idas y venidas, tuvimos que hablar bastante con todos los legisladores, explícales la situación. Los diputados acordaron con Rasetto las modificaciones, y salió el proyecto que se aprobó por unanimidad en Senadores”.

El proyecto de ley prevé, incluso, que la suspensión de las ferias internadas se pueda prorrogar por un año más. “A nosotros nos urgía esta suspensión porque es lo que nos permite trabajar en el mientras tanto sobre la normativa de fondo”, dijo Ferraro.

**Del lado de Santa Fe**

El conflicto con La Salada se remonta a las declaraciones que Jorge Castillo, el poderoso empresario detrás de la famosa feria situada en el Gran Buenos Aires, había hecho a La Capital en agosto de este año. “Me interesa instalarme del lado de Santa Fe”, dijo en la ocasión, en referencia a un predio ubicado sobre el kilómetro 417 de la ex ruta nacional Nº 9, cerca del Automóvil Club Argentino de la localidad de Tortugas.

Fiel a su estilo y con la ley a punto de sancionarse, Castillo redobló la apuesta hace pocas semanas, al afirmar que "lo que pasa en Santa Fe ya lo viví en Mendoza. Lo que no está prohibido está permitido. Yo no sé a qué se oponen si no saben de qué se trata. Voy a conseguir el permiso", disparó en su momento. Y afirmó: "Le decimos feria, pero hacemos un paseo de compras. Yo hago el shopping para los pobres. Cuando más pobres hay, más se necesita La Salada. Va a haber productores de leche, de carne, de pan".

Por su parte, Marcelo Maciel, presidente de Adeessa, fue claro en la postura de los empresarios pyme locales: “Este tipo de instalaciones no cumplen con cuestiones bromatológicas, sanitarias, ni tributarias y no llevan una correcta trazabilidad de los productos. Lo que queremos es un marco legal igualitario para todos. La iniciativa para encontrar una salida a esta situación de disparidad es de especial importancia para el comercio formal, severamente golpeado por la pandemia".

La entidad empresaria difundió también declaraciones del secretario de Comercio Interior, Juan Marcos Aviano, quien ratificó la postura de las entidades nucleadas en Adeessa y de otras agrupaciones comerciales al afirmar que este tipo de emprendimientos “en Santa Fe no tiene cabida”. Aviano explicó sobre la posibilidad de una Salada en Tortugas: “La voluntad política es que no, porque estaría incumpliendo la ley de superficies comerciales”. En este sentido aclaró que esta norma explica cómo deben instalarse negocios “para evitar la concentración económica y deslealtad comercial”.

**Un paso importante**

Para las empresas del sur santafesino, el objetivo detrás de la nueva normativa no se reduce a impedir la llegada de La Salada, sino "obtener una prórroga que otorgue un tiempo providencial para que todos los actores puedan concertar una regulación que coloque a todos los comerciantes en un pie de igualdad".

En esa sintonía se expresó Ferraro, cuando le dijo a este diario que la ley "es un paso importante, no definitivo, que le da cierta tranquilidad a las pymes de la provincia de que se va a trabajar en buscar igualdad de condiciones para todos. Estos grupos económicos han sido momentáneamente frenados y tenemos que trabajar para lograr la normativa de fondo, como en su momento se hizo con las grandes cadenas de supermercados con la ley 12.069 de grandes superficies comerciales. Este proyecto busca lo propio con este tipo de ferias que atentan contra los emprendimientos locales y el comercio formal en todo el sur santafesino", remató.