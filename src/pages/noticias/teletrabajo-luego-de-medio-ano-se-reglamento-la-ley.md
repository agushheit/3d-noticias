---
category: Agenda Ciudadana
date: 2021-01-21T08:45:52Z
thumbnail: https://assets.3dnoticias.com.ar/teletrabajo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Diario Uno
resumen: 'Teletrabajo: luego de medio año se reglamentó la ley'
title: 'Teletrabajo: luego de medio año se reglamentó la ley'
entradilla: La ley fue aprobada en julio del 2020 y el gobierno presentó hoy los lineamientos
  principales para el teletrabajo.

---
El Gobierno nacional reglamentó hoy el Régimen Legal del Contrato del Teletrabajo, aprobado el 30 de julio del año pasado por el Senado, que regula esta modalidad laboral en actividades que, por su naturaleza y particulares características, lo permitan.

Los principales elementos indican que el trabajador solo debe comunicarse durante la jornada laboral, que la provisión de elementos de trabajo es no remuneratoria y que no se puede exigir el cambio de modalidad cuando se comenzó el contrato bajo la modalidad a distancia.

La reglamentación de la ley 27.555 fue publicada en el Boletín Oficial a través del decreto 27/2021, que lleva las firmas del presidente Alberto Fernández; del jefe de Gabinete, Santiago Cafiero, y del ministro de Trabajo, Claudio Moroni. En el anexo publicado hoy, se detalla que los artículos 2°, 3° y 4° vinculados al contrato de teletrabajo, los derechos y obligaciones y la jornada laboral quedaron sin reglamentar.

**Los puntos generales**

El primer artículo establece que "las disposiciones de la Ley Nº 27.555 no serán aplicables cuando la prestación laboral se lleve a cabo en los establecimientos, dependencias o sucursales de las y los clientes a quienes el empleador o la empleadora preste servicios de manera continuada o regular".

Tampoco "en los casos en los cuales la labor se realice en forma esporádica y ocasional en el domicilio de la persona que trabaja, ya sea a pedido de esta o por alguna circunstancia excepcional".

El artículo 5°, referido al derecho a la desconexión digital, precisa que cuando "la actividad de la empresa se realice en diferentes husos horarios o en aquellos casos en que resulte indispensable por alguna razón objetiva, se admitirá la remisión de comunicaciones fuera de la jornada laboral".

"En todos los supuestos, la persona que trabaja no estará obligada a responder hasta el inicio de su jornada, salvo que concurran los supuestos contenidos en el artículo 203 de la Ley Nº 20.744 (t.o. 1976)", dice la normativa y aclara que "no se podrán establecer incentivos condicionados al no ejercicio del derecho a la desconexión".

"Los incrementos vinculados a la retribución de las horas suplementarias no serán considerados incentivos", puntualiza el texto.

**Trabajar en el hogar**

En cuanto a las tareas de cuidado, toda persona que ejerza el derecho a interrumpir la tarea, "deberá comunicar en forma virtual y con precisión" el momento en que comienza la inactividad y cuando finaliza. "En los casos en que las tareas de cuidado no permitan cumplir con la jornada legal o convencional vigente se podrá acordar su reducción de acuerdo a las condiciones que se establezcan en la convención colectiva", agrega el artículo 6°.

Sobre la provisión de elementos de trabajo, se remarca que "no es remuneratoria, lo cual se extiende a la compensación de gastos, aún sin comprobantes" por lo que "no integran la base retributiva para el cómputo de ningún rubro emergente del contrato de trabajo, ni contribuciones sindicales o de la seguridad social".

Además, en ambos casos, siempre y cuando la relación entre ambas partes "no se encuentre abarcada en el ámbito de aplicación de una convención colectiva", puedan acordar entre ellas las pautas para su determinación.

**La presencialidad vs. el teletrabajo**

Respecto de la reversibilidad, deberá "ajustarse a los deberes impuestos en los artículos 9º y 10° del Código Civil y Comercial de la Nación y 62 y 63 de la Ley de Contrato de Trabajo".

"Recibida la solicitud de la persona que trabaja, con la sola invocación de una motivación razonable y sobreviniente, el empleador o la empleadora deberá cumplir con su obligación en el menor plazo que permita la situación del o de los establecimientos al momento del pedido", según el artículo 7°. En ningún caso dicho el plazo podrá ser superior a 30 días.

"A los efectos de evaluar la imposibilidad de cumplir con esta obligación se tendrá especialmente en cuenta el tiempo transcurrido desde el momento en que se dispuso el cambio de la modalidad presencial hacia la modalidad de teletrabajo", añade el decreto.

Por otra parte, los empleados que hubiesen pactado la modalidad de teletrabajo desde el inicio de la relación laboral "no pueden revocar su consentimiento ni ejercer el derecho a que se les otorguen tareas presenciales, salvo lo dispuesto en los Convenios Colectivos del Trabajo o en los contratos individuales".

En tanto, el artículo 13°, sobre la representación sindical, sostiene que "en los casos en que se pacte la modalidad de teletrabajo al inicio de la relación, aquella debe llevarse a cabo previa consulta con la entidad sindical". Y añade que la representación de quienes antes prestaban servicios presenciales continuará siendo en el establecimiento laboral.