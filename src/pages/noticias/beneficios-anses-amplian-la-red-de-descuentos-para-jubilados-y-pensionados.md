---
category: Agenda Ciudadana
date: 2021-11-03T06:15:52-03:00
thumbnail: https://assets.3dnoticias.com.ar/RAVERTA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: 'Beneficios Anses: amplían la red de descuentos para jubilados y pensionados'
title: 'Beneficios Anses: amplían la red de descuentos para jubilados y pensionados'
entradilla: Con el objetivo "potenciar Beneficios Anses en todo el país", el convenio
  permitirá a jubilados, pensionados y titulares de asignaciones acceder a descuentos
  en los comercios de cercanía utilizando la tarjeta de débito.

---
La directora ejecutiva de la Anses, Fernanda Raverta, y el secretario de Comercio Interior, Roberto Feletti, firmaron un convenio que permitirá a jubilados, pensionados y titulares de asignaciones acceder a una red de descuentos a través de la tarjeta de débito con la que cobran sus prestaciones.  
  
El acuerdo tiene por objetivo "potenciar Beneficios Anses en todo el país, protegiendo los ingresos y promoviendo las ventas de los comercios de barrio", indicó el organismo en un comunicado de prensa difundido este martes.  
  
“Desde que asumimos nos propusimos recuperar el tiempo perdido en relación a la capacidad de compra de las familias argentinas, con aumentos en las asignaciones familiares de los trabajadores registrados, con las jubilaciones y pensiones y también con la Asignación Universal por Hijo y con la Tarjeta Alimentar”, afirmó Raverta durante la firma del convenio.  
  
En este sentido, destacó que "estabilizar los precios y proteger el bolsillo de las familias argentinas es una tarea central para sostener las políticas de mejoras de ingresos que estamos llevando adelante”.  
  
Por su parte, Feletti explicó que su intervención en las últimas semanas “tuvo que ver con frenar la suba de precios”.  
  
“Tenemos que lograr un acuerdo social, que habrá que consensuar y discutir, de una canasta amplia y diversificada de productos que sea de acceso a todos y todas las trabajadoras de este país”, agregó.  
  
Finalmente, el secretario general de la Anses, Santiago Fraschina, destacó que “el objetivo del convenio es generar una mesa de trabajo con la Secretaría de Comercio para adherir a más pequeños y medianos comercios a Beneficios Anses”.  
  
“Actualmente tenemos un promedio de 450 comercios que se adhieren todos los meses a Beneficios Anses, pero sabemos que tenemos que aumentar esa cantidad y ese es el objetivo fundamental de este convenio”, concluyó.