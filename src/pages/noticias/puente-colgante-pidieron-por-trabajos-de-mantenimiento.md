---
category: La Ciudad
date: 2021-08-06T06:00:32-03:00
thumbnail: https://assets.3dnoticias.com.ar/PUENTE.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Puente Colgante: pidieron por trabajos de mantenimiento'
title: 'Puente Colgante: pidieron por trabajos de mantenimiento'
entradilla: Se trata de obras para resguardar la integridad de los transeúntes, la
  estabilidad del viaducto y evitar la pérdida de valor histórico del Puente Colgante.

---
Mediante un proyecto de comunicación, el senador por el departamento La Capital, Marcos Castelló, solicitó la realización de obras necesarias en el Puente Colgante para resguardar la integridad de los transeúntes, la estabilidad del viaducto y evitar la pérdida de valor histórico.

La presentación de la iniciativa se realizó este jueves en la Legislatura provincial y en la misma se solicitó por medidas necesarias para que se lleven adelante las obras que demanda el mantenimiento, la puesta en valor y recuperación del Puente Colgante.

El legislador justicialista expuso ante sus pares que en el año 2015 se encargó un estudio atento a las necesidades de mantenimiento, pero las obras nunca se llevaron a cabo. Y en ese sentido, Castelló sostuvo que “esto pone en serio riesgo la estabilidad del viaducto” y que es necesario “resguardar la integridad de los transeúntes como así también evitar la pérdida de valor histórico de nuestro puente”.

El proyecto del senador adjunta un informe de mas de 15 puntos, donde se enumeran las tareas necesarias para realizar el mantenimiento correspondiente de la superestructura del Puente Colgante, con la premisa de integrar los nuevos elementos a los originales, sin perder la esencia ni romper la unidad visual de la obra.

Además de ser un peligro latente para quienes lo transitan, Castelló consideró que “la conservación del puente es muy deseada por gran parte de los santafesinos, ya que es un símbolo para la ciudad y un motivo de orgullo. Entendemos que el proyecto de la puesta en valor, atento a este valor histórico, reivindica nuestra identidad cultural devolviendo a la memoria urbana el esplendor de construcciones emblemáticas”.