---
category: Agenda Ciudadana
date: 2021-08-15T06:15:19-03:00
thumbnail: https://assets.3dnoticias.com.ar/ciberestafa.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Ciberestafas en Santa Fe: en qué consisten los nuevos engaños vía WhatsApp'
title: 'Ciberestafas en Santa Fe: en qué consisten los nuevos engaños vía WhatsApp'
entradilla: El jefe de la Sección Cibercrimen de la AIC explicó cómo protegernos de
  las ciberestafas. Aseguró además que el 90% de ellas se realizan desde la cárcel.

---
A causa de la pandemia se aceleró la digitalización en todo el mundo de casi todos los aspectos de la vida cotidiana, lo que, aparte de los claros beneficios en rapidez y comodidad que trajo dejó una “zona liberada” para los delincuentes que se dedican a realizar ciberestafas a aquellas personas más distraídas, hasta desde la cárcel.

En diálogo con el programa La Mañana del UNO Rodrigo Álvarez, jefe de la Sección Cibercrimen de la Agencia de Investigación Criminal de Santa Fe, explicó que desde el organismo trabajan sobre todo en la concientización de los usuarios, para que vean que existen medidas de seguridad disponibles en sus redes sociales que se pueden activar para evitar ser víctimas y no perder sus cuentas.

“En este caso lo que más nos aboca a nosotros, como responsables de la seguridad de las personas, es que en el 90% de las investigaciones son personas privadas de la libertad, y que en sus 24 horas de no hacer nada se adelantan con algunas cositas de viveza criolla que nosotros que estamos ocupados por ahí no le prestamos atención; ellos van aprovechando estas ventajas y desventajas de la tecnología para estafar”, contó el titular de la sección de Cibercrimen.

Todas las redes sociales que están en auge, como TikTok, Twitter, Instagram, Facebook, WhatsApp, Telegram, Signal, tienen doble factor de autenticación, y son más o menos similares. “Es como activar el token o la tarjeta de coordenadas del banco. Es exactamente lo mismo, es una doble barrera de seguridad”, precisó Álvarez.

“Hay que estar alerta, lamentablemente es una barrera que tenemos que empezar a crear de desconfianza, de no ser tan crédulos y aplicar mucho más el sentido común.”

Activar la doble autenticación en WhatsApp es muy sencillo, y puede evitar que caigamos en las últimas modalidades de estafa, que implican el robo de la cuenta para suplantar nuestra identidad, y luego pedir dinero a nuestros contactos.

¿**En qué consiste la nueva estafa de WhatsApp?**

Según la experiencia del analista, los estafadores se comunican por mensaje o llamada de WhatsApp mediante un engaño, que puede ser informar sobre el turno para la segunda dosis de vacunación o haciéndose pasar por la empresa de mensajería. “La excusa es que mandaron un código de seguridad porque hubo un intento fallido de iniciar sesión en otro dispositivo; el usuario distraído cae en la trampa, le solicitan un código numérico de seis dígitos que le mandaron por SMS, el usuario se los pasa y a partir de ahí pierde acceso a la red social”.

“Lo que pasa en realidad es que, cuando nosotros cambiamos de dispositivo y queremos migrar la cuenta, tenemos que hacer esto; tenemos que cargar nuestro número telefónico, insertar la SIM en el dispositivo nuevo, cargar nuestro número, descargar la aplicación WhatsApp, nos piden otro número, nos llega desde la empresa un SMS con este código para verificar y vincular la línea telefónica con la red social. Y a partir de ahí ya podemos utilizar la red social en un nuevo dispositivo”, explicó Álvarez, y acá es donde se aprovechan los delincuentes. Como ellos no tienen la tarjeta SIM en su dispositivo, nos piden el código que nos llegó a nosotros y a partir de ahí ellos tienen acceso a la red social.

“Hasta este momento, siempre lo aclaro, no hay delito, porque en Argentina la suplantación de identidad no es un delito”, advirtió. “Hay un proyecto de ley, en Caba es una contravención, pero a nivel nacional no es un delito suplantar la identidad de nadie”.

Ya en completo control de nuestra cuenta, el estafador empieza a comunicarse con nuestros contactos haciéndose pasar por nosotros, y empieza a pedir dinero con excusas como un viaje por una urgencia, una circunstancia médica, un robo, etc. “A cualquiera de nuestros contactos que pique, le solicitan dinero, haciéndose pasar por nosotros obviamente, y luego brindan una cuenta bancaria, que lógicamente no es nuestra. Han caído, han depositado dinero creyendo que realmente la persona que estaba solicitándolo era quien decía ser y ahí es donde se concreta la estafa”.

“Otra de las excusas que utilizan es la venta de dólares, le hacen creer a las personas que estamos vendiendo dólares a muy buen valor, transfieren y luego no hay dólar”, informó Álvarez. En este sentido, también es común que llegue un mensaje que diga «hola soy María, este es mi nuevo número» o algún nombre común, para que creamos que es una persona conocida, y ahí nos ofrezcan dólares o pidan plata.

**Levantar todas las barreras**

Al tener activa la verificación en dos pasos, también llamada factor de doble autenticación, ponemos una nueva barrera entre nosotros y el ladrón de datos, que deberá pensar en nueva excusa para pedirnos ese pin.

“Es muy simple activarlo: es cuestión de ir a los tres puntitos de los ajustes de WhatsApp, se despliega una pestaña, son cuatro opciones, la última dice ajuste, hacemos clic ahí, privacidad, vamos a seguridad y, verificación en dos pasos, nos va a pedir que carguemos por primera vez seis números que tenemos que cargarlos y recordarlos, algo que sea fácil de recordar, pero difícil de descifrar. No 123456 ni 000000”, dijo Álvarez. “En Instagram la diferencia es que en vez de pedir que nosotros carguemos seis dígitos para luego recordar, utiliza el doble factor de verificación con un mensaje SMS o con un mail”.

En este sentido, reflexionó: “Cuando llegan este tipo de mensajes en principio sí vamos a creer que es la persona que dice ser, pero ya cuando empiece a pedir dinero o cuando empieza a vender dólares de la nada es cuando tenemos que utilizar el sentido común. Decir, ¿realmente esta persona me pediría plata por WhatsApp así de la nada? En todo caso, cortamos la comunicación y los llamamos por teléfono”.

**Las personas detrás de las estafas**

Según el integrante de la Agencia de Investigación Criminal de Santa Fe, en los últimos días no ha habido víctimas, pero sí toman conocimiento de personas a las que le han suplantado la identidad, pero no llegó a concretarse la estafa. “Hoy si no hay un depósito, una transferencia donde engañaron al usuario no hay delito para nosotros investigar, porque la suplantación de identidad no la podemos investigar porque no es delito”.

“Es lo más preocupante, que ya escapa de nosotros, que personas privadas de su libertad tengan acceso a telefonía móvil y a internet, y que puedan hacer esto desde la cárcel es un tema pendiente de parte del Ministerio que se ocupa de estas personas”, opinó Álvarez. “Esto viene pasando hace muchos años con diferentes modalidades de estafas, siempre se supo. Hemos hecho investigaciones que nos ha llevado a penales de Córdoba, hemos imputado a personas que ya estaban privadas de su libertad con estafas, con asociaciones ilícitas, lavado de activos, con una condena que se les agrega a la que ya tienen y así todo siguen teniendo acceso a telefonía y a internet”.

Y agregó: “Es muy raro que los legisladores no se ocupen de evitar este tipo de cosas o quienes están a cargo de ese ministerio. Tiene que ver también con el filtro que tiene el servicio penitenciario”.

Según Rodrigo Álvarez, jefe de la Sección Cibercrimen de la Agencia de Investigación Criminal de Santa Fe, la mayoría de las estafas online por suplantación de identidad se realizan desde las cárceles.

“El dispositivo móvil hoy es tan importante como la billetera”

Puntualizó el jefe de la sección Cibercrimen, advirtiendo que hay que utilizar todas las herramientas que nos da la tecnología porque en nuestros celulares tenemos cargado, no solamente datos bancarios y billeteras, sino datos personales y nuestra intimidad.

“Hay que hacer de cuenta que ese teléfono es la vía física en la que le ponemos llave y traba a la puerta”, remarcó.

Finalmente, Álvarez aseguró que proteger el celular con patrón de bloqueo es seguro y que, tal vez se puede descifrar por las marcas que deja la grasitud de nuestro dedo, pero que es muy difícil y una misión estilo “CSI”. Sin embargo, alertó sobre otra situación muy común por la que todos pasamos: llevar a arreglar el celular.

“Si se nos rompe, siempre tener en cuenta que si lo vamos a llevar a un servicio técnico tenemos que cerrar todas las sesiones de las redes sociales que tenemos abiertas porque no sabemos a quién le estamos entregando el teléfono. La persona que lo va a arreglar tiene acceso a todo eso. Una vez que logra arreglarlo podría tranquilamente ver el contenido. Entonces luego se filtran vídeos, fotos íntimas, y no se sabe de dónde: generalmente viene desde ahí”, alertó Álvarez. “Así que cerremos sesión, hagamos un backup en la nube, pongámosle contraseña y evitemos este tipo de delitos, que también están bastante en vigencia, que es "sextorsión" o difusión sin consentimiento de contenido”.