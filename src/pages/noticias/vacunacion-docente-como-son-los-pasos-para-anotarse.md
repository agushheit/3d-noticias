---
category: Agenda Ciudadana
date: 2021-02-26T07:42:20-03:00
thumbnail: https://assets.3dnoticias.com.ar/vacunación.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Agencias
resumen: 'Vacunación docente: ¿Cómo son los pasos para anotarse?'
title: 'Vacunación docente: ¿Cómo son los pasos para anotarse?'
entradilla: El Ministerio de Educación habilitó la plataforma este jueves. El procedimiento
  para realizar la inscripción.

---
El Ministerio de Educación junto con el Ministerio de Salud y la Secretaría de Innovación Pública habilitaron este jueves el registro online del personal docente y no docente de todo el país con el objetivo de organizar el proceso de vacunación en todo el territorio nacional.

Se trata de una plataforma que está online a partir de este jueves y permitirá recabar información de la cantidad, localización e identidad de las personas que trabajan en establecimientos educativos para la organización de la logística y aplicación de las vacunas por parte del sistema de salud en todo el país. 

Desde el Gobierno aclararon que registrarse no implica solicitar un turno ni una reserva para aplicarse la vacuna. Una vez que las dosis estén disponibles y el cronograma de vacunación esté confirmado, las personas inscriptas recibirán una notificación de las autoridades de su jurisdicción.

**Cómo registrarse para la vacunación** 

\-Se debe ingresar a la web de Vacunación contra la COVID-19 para personal de establecimientos educativos: www.argentina.gob.ar/coronavirus/vacuna/docentes

\- Elegí la provincia donde vivís y completá tus datos personales y los del establecimiento educativo donde trabajás. Si lo hacés en varios, informá el establecimiento en el que tenés más horas.

\-Tené en cuenta que no estás sacando un turno ni haciendo una reserva para que te apliquen la vacuna. Cuando las dosis estén disponibles y el cronograma de vacunación confirmado, vas a recibir una notificación de las autoridades de tu jurisdicción.

\- Si vivís en las provincias de Buenos Aires, San Luis o Tucumán, debés consultar los respectivos sitios provinciales.

**El orden de vacunación para los docentes** 

1) Personal de dirección y gestión, supervisión e inspección; docentes frente a alumnos de nivel Inicial (incluye ciclo maternal), de primer ciclo del nivel primario, (1°, 2° y 3° grado) y de educación especial (un total de 457.694 personas).

2) Personal de apoyo a la enseñanza, todo otro personal sin designación docente pero que trabaja en establecimientos educativos de la educación obligatoria en distintas áreas y servicios (247.413 personas).

3) Docentes de nivel primario, del segundo ciclo de 4° a 6°/7° grados (148.692 en total).

4) Docentes de nivel secundario, de educación permanente para jóvenes y adultos en todos sus niveles e instructores de formación profesional (331.099 personas).

5) En el último grupo estarán los docentes y no docentes de institutos de educación superior y universidades. (273.185 personas).