---
category: Estado Real
date: 2021-09-14T06:00:24-03:00
thumbnail: https://assets.3dnoticias.com.ar/DELTA13.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Variante Delta: la provincia solicita extremar los cuidados a quienes regresan
  del extranjero'
title: 'Variante Delta: la provincia solicita extremar los cuidados a quienes regresan
  del extranjero'
entradilla: " “Se está trabajando en cuidados estrechos de fronteras con un equipo
  interinstitucional de Migraciones, Protección Civil, Seguridad y Salud”, afirmó
  Prieto."

---
El Ministerio de Salud provincial solicitó extremar los cuidados y cumplir con los protocolos sanitarios a todas aquellas personas que regresen del exterior ante la circulación predominante de variante delta de Coronavirus a nivel mundial.

En este sentido, el secretario de Salud, Jorge Prieto, indicó que “tenemos en la provincia el ingreso de la variante Delta. Se trata de personas que provienen del exterior y transitan la enfermedad con una incubación más corta; muchos casos son transmisores asintomáticos o poco sintomáticos lo cual tiene que ver con contar con esquemas completos de vacunación; y con poblaciones de niños y niñas que cursan de manera muy diferente la enfermedad en cuanto a lo que se refiere a hospitalización o formas severas”.

En relación a ello, afirmó que “hoy en la provincia son 10 los casos confirmados con esta variante. Para esto se está trabajando en cuidados estrechos de fronteras con un equipo interinstitucional de Migraciones, Protección Civil, Seguridad y Salud en lo que hace a la vigilancia epidemiológica. Este número reducido de casos permite confirmar que un buen aislamiento, seguimiento y control sanitario ha permitido estos casos distribuidos en 8 casos en Rosario, 1 Rafaela, y 1 en Santo Tomé, confirmados hasta el momento, todos con antecedentes de viajes al exterior”, continuó.

**Situación en la ciudad de Santo Tome**

El pasado fin de semana, desde la cartera sanitaria se detectaron 3 casos positivos de una probable variante Delta (se trata de personas con antecedentes de viajes al exterior). Se realizó como en todos los casos positivos la secuenciación para la vigilancia epidemiológica.

Al respecto, Prieto destacó que “la provincia dispone de test para realizar tamizaje con el que se puede determinar la alta presunción para determinadas variantes cuya confirmación diagnóstica se hace finalmente por secuenciación en la ciudad de Rosario, Rafaela e instituto Malbrán. Es por ello que se dice como alta probabilidad para lo cual se realizaron los aislamientos correspondientes, la anidación de todos los posibles contactos estrechos por esta alta presunción debido a la dilación del resultado”.

Y continuó: “Los tres casos habían ingresado a la provincia el 4 de septiembre con un hisopado programado a los siete días, los cuales dieron positivos aún estando asintomáticos”.

**Recomendaciones**

En este sentido, el secretario de Salud solicitó que aquellas personas que ingresan al país “no deben descuidar el aislamiento por más que tengan test rápido con un resultado negativo al ingreso al país, porque de esta forma vamos a continuar retrasando y postergando la posible circulación de una variante que tiene una alta contagiosidad”.

“Los controles de rutina nos permiten visualizar la importancia de extremar los cuidados y seguir con las recomendaciones que se imparten al ingreso del territorio provincial desde el call center 0800 quien remite las normativas vigentes en el territorio y determina el día que deben ser sometidos a los test diagnósticos. Recibimos de los repatriados la información a través del 0800, se realiza el seguimiento, se les consulta esto del test rápido pero se van haciendo consultas para evaluar si están o no incubando la enfermedad. Posteriormente se los cita para realizar el test diagnóstico a través de la PCR a partir del séptimo día”, informó.

Y aclaró que “si alguno de ellos da positivo como en estos casos, el equipo de abordaje de las máximas autoridades regionales, municipales o comunales, son notificados junto a la región de salud, al 0800, a la dirección provincial de epidemiología y a las autoridades de la cartera sanitaria. En común acuerdo y ya protocolizado se implementan las políticas sanitarias para el abordaje territorial que tienda a contener la situación emergente, aislando todos los contactos estrechos, haciendo el seguimiento clínico e indicando oportunamente los hisopados correspondientes”.

“Como consecuencia de estos casos positivos, al momento tenemos 22 personas que ya están aisladas y sugerimos dada la trazabilidad de las relaciones humanas que muchas veces pasan inadvertidas, o se descuidan, que se comuniquen ante la duda de ser o no posible contacto estrecho de un caso confirmado con las autoridades sanitarias que lo informarán y asesorarán al respecto”, concluyó.