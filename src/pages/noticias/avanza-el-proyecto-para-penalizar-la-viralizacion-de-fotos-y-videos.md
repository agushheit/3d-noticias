---
category: Agenda Ciudadana
date: 2020-11-28T13:06:01Z
thumbnail: https://assets.3dnoticias.com.ar/BARISONE.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: LT10'
resumen: Ley de Respeto a la Intimidad
title: Avanza el proyecto para penalizar la viralización de fotos y videos
entradilla: La iniciativa impulsada por la viralización de las imágenes del accidente
  donde murió el exjugador de Unión obtuvo media sanción en Diputados.

---
La cámara de Diputados de Santa Fe le dio media sanción este jueves al proyecto de Ley de Respeto a la Intimidad, impulsado por la familia de Diego Barisone, al viralizarse las imágenes del accidente donde perdió la vida el exjugador de Unión cinco años atrás.

La iniciativa busca impedir la repetición de esas conductas con penas de prisión y multas. "Este es un problema que no solo vivimos nosotros sino mucha gente. Hay que evitar la publicación y la transmisión de este tipo de imágenes que luego no hay forma de parar”, sostuvo a través del móvil de LT10 Gerardo Barisone papá de Diego. 

Además del dolor por la pérdida, los padres de Barisone “tienen que llevar una vida en la que conviven con las fotos del accidente de su hijo”, expreso a LT10 el diputado Joaquín Blanco, autor del proyecto. Por eso, “es una ley que hace falta”, señaló y agregó que lo que se busca es ponerse en el lugar de la víctima y que tenga un espacio en la Justicia donde pueda denunciar la publicación de fotos y videos. 

Respecto al tipo de contenido al que refiere el proyecto, hay dos fuentes: desde el morbo, como las imágenes de accidentes, homicidios y demás hechos donde circulan las fotos de los fallecidos o accidentados. 

Por otro lado, material íntimo de parejas que se puedan utilizar para acosar o amenazar con publicar. Son dos cuestiones que se extendieron en los últimos años y la ley “viene a llenar un vacío legal” y que se pueda determinar la cadena de responsabilidades hasta llegar a quienes generaron esas imágenes. 

De esta manera, agrega un artículo al código de conviviencia de la provincia "para que sancione con una pena de hasta 30 días de prisión o hasta 60 si es un hecho agravado, y con multas a aquellos que generen o que de forma intencional compartan contenido audiovisual que lesione la integridad y el honor de personas que son víctimas". 

Además, el legislador manifestó que el objetivo no es solo sancionar sino "generar conciencia del daño que se puede hacer compartiendo en un simple mensaje de Whatsapp, un meme o un mal chiste en un grupo de amigos. Hay que tener en cuenta que detrás de esas imágenes hay familias que están sufriendo".

### El caso de la difusión de las fotos de Maradona

En el velorio de Diego Maradona se difundieron imágenes de los empleados de la funeraria junto al ataúd del astro del fútbol. En este caso, los trabajadores fueron rápidamente identificados y despedidos “por faltar a las obligaciones profesionales” que era estar al cuidado del cuerpo, señaló por LT10 Marcelo Temperini, abogado especialista en derecho informático y seguridad de la información. 

No obstante, está pendiente saber quién difundió las imágenes, “una cosa es sacarse la foto y otra cosa es difundirla”, puesto que hay muchos trabajos donde hay acceso a información confidencial. 

Por ende, el ilícito es la divulgación sin tener el permiso. Por lo cual, se debe hacer una pericia para saber desde qué teléfono se envió el material. Por otro lado, se supo que existen otras imágenes de Maradona ya fallecido en su casa, en este caso, se puede intimar al dueño del contenido para que no lo haga público.