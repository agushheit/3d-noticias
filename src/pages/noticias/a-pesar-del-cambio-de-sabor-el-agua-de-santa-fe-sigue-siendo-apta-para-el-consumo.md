---
category: La Ciudad
date: 2021-07-20T06:00:41-03:00
thumbnail: https://assets.3dnoticias.com.ar/AGUASDETAMARINDOjpg.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: A pesar del cambio de sabor, el agua de Santa Fe sigue siendo apta para el
  consumo
title: A pesar del cambio de sabor, el agua de Santa Fe sigue siendo apta para el
  consumo
entradilla: Mientras se readecúa la red para contrarrestar la bajante histórica, desde
  ASSA recuerdan que la percepción de los componentes organolépticos del agua varía
  de individuo a individuo.

---
Aguas Santafesinas refuerza la captación de agua en las dos tomas superficiales que tiene en la ciudad de Santa Fe para prestar el servicio, tanto el río Santa Fe como en el Colastiné. “La semana pasada -dijo el vocero de la empresa, Germán Nessier, a El Litoral-, se incorporó una bomba de mayor caudal a la que ya teníamos ubicada allí desde el año pasado y se están tendiendo más de mil metros de nuevas cañerías para poder transportar el agua de otras dos bombas que se van a ubicar en esta toma para asegurar la captación. En tanto, en la toma del río Santa Fe se rehabilitó y se puso en funcionamiento una bomba de refuerzo a la que ya existía, además de las que dispone el muelle de toma de esta fuente de agua, donde también se vinieron realizando desde el año pasado modificaciones sobre la profundización de las bombas y rehabilitación de equipos para asegurar la captación, según explicó el representante de Aguas Santafesinas.

“En función del escenario que se vaya dando, la empresa va evaluando cada alternativa que tiene en cualquiera de las instalaciones, en la ciudad de Santa Fe y en las demás ciudades a lo largo de la provincia, desde Reconquista hasta Rosario, donde de acuerdo a las necesidades de cada una de las instalaciones se han hecho medidas de refuerzo como también sucedió en la ciudad de Rincón, donde tenemos el acueducto de la costa, donde se instalaron bombas de refuerzo y seguramente haya que reubicar en función de los niveles del río”, detalló el funcionario..