---
category: Agenda Ciudadana
date: 2021-03-25T08:25:52-03:00
thumbnail: https://assets.3dnoticias.com.ar/vizzotti-1.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: Vizzotti no descartó restricciones de circulación "por franjas horarias",
  pero luego el Gobierno le bajó el tono
title: Vizzotti no descartó restricciones de circulación "por franjas horarias", pero
  luego el Gobierno le bajó el tono
entradilla: '"Hoy no se está estudiando ninguna restricción para una franja horaria
  ni ningún tipo de confinamiento", aclararon luego fuentes gubernamentales.'

---
La ministra de Salud, Carla Vizzotti, señaló hoy que si se detecta aumento de casos de coronavirus, el Gobierno va a "tomar medidas lo más temprano posible" pero no se va a "apuntar a un confinamiento", sino a la "disminución de la circulación de personas por franjas horarias".

No obstante, fuentes de la Casa Rosada luego aclararon que "hoy no se está estudiando ninguna restricción para una franja horaria ni ningún tipo de confinamiento", aunque deslizaron que "en un escenario sanitario similar" al de principios de enero pasado, "no se descarta la aplicación de una medida de estas características".

"Una medida de restricción de circulación nocturna ya fue aplicada con éxito luego del pico de casos a fin de año por las fiestas y demostró ser efectiva ya que después los casos se estabilizaron y bajaron. En un escenario sanitario similar, no se descarta la aplicación de una medida de estas características, pero hoy no se está estudiando ninguna restricción para una franja horaria ni ningún tipo de confinamiento", aclararon las fuentes.

Y agregaron: "Dentro de las lecciones aprendidas en el primer año de la pandemia en cuanto a la circulación de las personas, se evidencia la importancia de tomar medidas lo más temprano posible en la unidad geográfica mínima".

En declaraciones radiales, la titular de la cartera sanitaria se refirió a la posibilidad de una segunda ola de contagios de COVID-19 y señaló que el país está "en una situación de alerta hace semanas, no solamente con la situación de la Argentina sino con los países vecinos y el aumento en algunas jurisdicciones que ya están experimentando un aumento comparativo en relación a las últimas semanas".

"El día de ayer (martes) hay que evaluarlo en el contexto. Un solo día no es una tendencia. Hay que hacer el promedio semanal pero las provincias con más densidad poblacional han tenido un aumento importante de los casos", advirtió la integrante del Gabinete. E insistió: "Hay provincias que han tenido un aumento importante, tenemos que ver cómo evolucionan".

Al respecto, Vizzotti llamó a "reforzar los cuidados para retrasar lo más que se pueda el aumento de casos mientras sigue la campaña de vacunación".

Tras advertir sobre la dura situación epidemiológica que enfrentan los países de la región y el riesgo de contraer alguna de las nuevas variantes del virus Sars-Cov-2, la ministra de Salud explicó: "A la fecha no tenemos circulación predominante en ninguna de las variantes nuevas".

En referencia al pedido oficial para no salir del país, la funcionaria nacional destacó que "bajaron un 50 por ciento las reservas de viajes al exterior". "No es una buena idea el turismo a países donde circulan las nuevas cepas", agregó.

De todos modos, la titular de la cartera sanitaria explicó que "en principio la recomendación es la cuarentena en su domicilio" para aquellos que regresen del exterior, ante lo cual explicó que "en este momento no se está pensando en el aislamiento en hoteles".

Vizzotti también se refirió a la campaña de vacunación contra el COVID-19 y a la provisión de dosis. "La estamos fortaleciendo. Hay posibilidades de escalar el número de vacunados por día a medida que vaya llegando un número de vacunas más importante", indicó.

Asimismo, remarcó que los mayores de 70 años tendrán "prioridad" para vacunarse a medida que lleguen nuevas dosis y subrayó: "Apuntamos a que estén vacunados a fines de abril".

"La Argentina es uno de los países que recibe dosis en forma constante, ya tenemos casi 5 millones. Se está avanzando de una manera sostenida y las vacunas van a seguir llegando. Quienes se vacunaron, deben seguir cuidándose. Y quienes estén esperando el turno, también, para atrasar el aumento de casos", añadió.​.

Finalmente, la ministra explicó que se evalúa la extensión de vacunación entre la primera y segunda dosis: "Es una posibilidad, lo analizamos. Se van a aplicar las dos dosis, pero se puede diferir la segunda dosis priorizando vacunar más con la primera sabiendo que la primera es muy importante y que la segunda agrega un poco de eficacia y duración de la inmunidad".