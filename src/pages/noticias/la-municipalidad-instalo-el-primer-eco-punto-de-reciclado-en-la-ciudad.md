---
category: La Ciudad
date: 2021-10-20T06:15:18-03:00
thumbnail: https://assets.3dnoticias.com.ar/ecopunto.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: La Municipalidad instaló el primer ECO Punto de Reciclado en la ciudad
title: La Municipalidad instaló el primer ECO Punto de Reciclado en la ciudad
entradilla: "Está ubicado en la sede de la Dirección de Deportes (Av. Alte. Brown
  5294). Habrá atención personalizada y funcionará de lunes a domingos, de 9 a 19.
  \n\n"

---
En la Avenida Alte. Brown 5294 donde funciona la Dirección de Deportes, en la Costanera Oeste, se instaló el primer ECO Punto de Reciclado. Se trata de un lugar de recepción de materiales reciclables y residuos especiales como pilas, diseñados especialmente para que los vecinos puedan disponer sus residuos domiciliarios y luego la Municipalidad proceda a recuperarlos.

 Es importante destacar que estos ECO Puntos de Reciclado contarán con atención personalizada, y estarán abiertos de lunes a domingos, de 9 a 19 horas. Este que se instaló en la Costanera Oeste es el primero de varios que se colocarán en distintos lugares de la ciudad, en los próximos meses.

 El secretario de Ambiente de la Municipalidad, Edgardo Seguro brindó más detalles y al respecto dijo: “La intención es que los vecinos traigan a estos lugares los elementos secos y habrá una oficina de atención ambiental donde vamos a charlar con ellos, contarles sobre algunas campañas especiales que haya como de recolección de AEEs o de pilas, o de recepción de reciclados, entre otros”.

El funcionario municipal destacó la buena respuesta de los santafesinos y aseguró que a partir de las distintas campañas que hay, los vecinos solicitan más puntos como éste. “Nos piden más lugares fijos, de mayor envergadura. Este es un espacio con una oficina municipal donde se puede hablar con el vecino; y se suman a las campanas que tenemos fijas en 17 puntos y a los Eco Canjes que tenemos en diferentes puntos. Estos puntos fijos estarán todo el tiempo abiertos y todos los días”.

 **¿QUÉ MATERIALES SE RECIBEN?**

Los ECO Puntos de Reciclado reciben residuos reciclables como papel, cartón, plástico, metal, vidrio y telgopor, así como también pilas en desuso (AA, AA, prismáticas y botón).

Este es el primero de cinco que se instalarán. Los otros se habilitarán el próximo mes uno y los otros tres en diciembre; y aún se están definiendo los lugares, aunque serían en espacios públicos con mucha concurrencia de gente.

Antes de finalizar, el secretario de Ambiente destacó la buena respuesta del santafesino sobre todo con residuos más peligrosos como las pilas. “Desde hace algunos años el santafesino comenzó a separar los residuos, pero no hay una buena recolección del seco que si bien es lunes y jueves, los camiones pasan y juntan todo lo que encuentran, entonces con los Ecobarrios se puede diferenciar y se le da la posibilidad a aquellos que quieren darle una adecuada disposición lo puedan hacer”, manifestó.

Vale aclarar que todo este material recolectado es recepcionado por la asociación civil Dignidad y Vida Sana que trabaja en el Relleno Sanitario. “Y todo este material lo vamos a poner en valor para algunos emprendedores para que tengan la posibilidad de transformar estos materiales”, finalizó.