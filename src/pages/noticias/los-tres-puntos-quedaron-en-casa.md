---
category: Deportes
date: 2020-12-31T11:01:55Z
thumbnail: https://assets.3dnoticias.com.ar/3112-deportes0.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Por Marisa Lemos
resumen: Los tres puntos quedaron en casa
title: Los tres puntos quedaron en casa
entradilla: El Tatengue le ganó 2 a 0 a Lanús en un encuentro por los playoff por
  la Zona B, Grupo A.

---
En la jornada de ayer, **Unión logro su primer triunfo de la fecha 3 por la fase de complementación de la Copa Diego Armando Maradona**, en el Estadio 15 de Abril, con goles de Márquez y Gastón González, en un caluroso encuentro en la capital santafesina.

El Granate arrancó mejor, con Vera en el centro del campo y con buen criterio para administrar la pelota. Y De la Vega sobre la derecha, para aprovechar el uno contra uno ante Corvalán, y así poder desequilibrar. Tuvo la jugada más clara del primer tiempo el delantero rubio, tras capturar un rebote en el área y sacar un derechazo violento al primer palo, que Moyano resolvió con el pecho para desviar al córner.

A los 28 minutos del primer tiempo, Aguirre rechazó con un centro desde la derecha y la pelota fue disputada por Nardoni y Vera, pero el rebote favoreció a Gastón González. El zurdo envió el centro, pero el que corrigió la trayectoria fue Burdisso, que le sirvió el gol en bandeja al Cuqui Márquez para marcar el primer tanto de la velada.

En el inicio del segundo tiempo, un pelotazo de Moyano, que fue a rebotar en Troyansky, dejó al juvenil Gastón Gonzales mano a mano con el arquero rival en el área. En el primer despeje fallido rebotó contra el defensor, que luego fue a parar los pies de Gastón G, para marcar el segundo tanto para el local, aunque todavía quedaba mucho hilo en el carretel.

El visitante nunca se quedó atrás e hizo un par de cambios para lograr el descuento, pero no lo logró. Con la cabeza puesta en la sudamericana, Lanús anhelaba alcanzar a los equipos de arriba. En este momento tiene todas sus fichas puestas en su próximo encuentro, ante Vélez, el 6 de enero a las 21:30 horas.

**Luego de 3 derrotas consecutivas, Unión volvió a sonreír y sigue con la ilusión intacta de llegar a la final**. Su siguiente partido es el lunes 4 de enero de 2021, a las 17.10 horas frente a Aldosivi en Mar del Plata por la fecha 4.