---
category: La Ciudad
date: 2021-03-29T06:15:29-03:00
thumbnail: https://assets.3dnoticias.com.ar/BACHEO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Trabajos de bacheo previstos para este lunes
title: Trabajos de bacheo previstos para este lunes
entradilla: La Municipalidad informa las calles por las que avanza el cronograma de
  tareas previsto en el Plan de Bacheo 2021

---
En enero pasado, la Municipalidad puso en marcha un plan de bacheo para reparar más de 12.000 metros cuadrados de calles de la capital provincial. El programa se desarrolla por fases, priorizando las zonas de la ciudad que evidencian mayor circulación.

En la zona del micro y macrocentro, está previsto que los trabajos se desarrollen en:

. Lisandro de la Torre entre San Lorenzo y Av. Freyre

· San Lorenzo entre Catamarca y Bv. Pellegrini

· Saavedra entre Junín y Zazpe

· Urquiza y 3 de Febrero

· 4 de Enero y Corrientes

· 4 de Enero y J. De Garay

· 4 de Enero entre Junín y Bv. Pellegrini

· 1° de Mayo entre Uruguay y Jujuy

· 9 de Julio y 1° Junta

· San Jerónimo y Tucumán

· San Jerónimo entre Juan de Garay y Moreno

· 25 de mayo y Crespo

Los trabajos siguen siempre en sentido del tránsito. Los cortes se realizan a medida que van avanzando los trabajos. Se iniciarán por calle Lisandro de la Torre y se seguirá el orden de la lista.

En tanto, en avenidas troncales, la obra continuará por:

· Aristóbulo del Valle, entre French y Los Pinos, mano sur-norte

· Aristóbulo del Valle, entre avenida Gorriti y 12 de Infantería, mano norte-sur

Por otra parte, en el marco de los trabajos ejecutados en la obra Conducto Pluvial Mariano Comas, operará un corte total del tránsito vehicular en:

Mariano Comas, entre San Lorenzo y avenida López y Planes. Por este motivo, se organiza el desvío de calle Mariano Comas, por San Lorenzo. El tránsito pesado, en tanto, se desviará en Urquiza hacía el bulevar Pellegrini

El municipio informa que todas las tareas se realizan siempre en sentido del tránsito. Del mismo modo, indica que puede haber cortes de circulación y desvíos de colectivos que se efectúan a medida que avanza la obra y se realizan intervenciones mayores.

Además, se detalló que la realización de los trabajos está sujeta a las condiciones climática y puede haber cortes de circulación y desvíos de colectivos a medida que se desarrolla la obra.