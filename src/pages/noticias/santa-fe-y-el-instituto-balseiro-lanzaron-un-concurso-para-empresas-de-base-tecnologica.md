---
category: Estado Real
date: 2021-05-14T08:08:15-03:00
thumbnail: https://assets.3dnoticias.com.ar/balseiro.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: Santa Fe y el Instituto Balseiro lanzaron un concurso para empresas de base
  tecnológica
title: Santa Fe y el Instituto Balseiro lanzaron un concurso para empresas de base
  tecnológica
entradilla: "“Es el reconocimiento a nuestro sector científico-tecnológico, uno de
  los activos más importantes que tiene la Argentina”, aseguró el gobernador Perotti."

---
El gobernador Omar Perotti encabezó este jueves el acto de presentación del premio “Santa Fe para transformar Santa Fe”, destinado a empresas de base tecnológica, y organizado por la Secretaría de Ciencia, Tecnología e Innovación de la provincia y el Instituto Balseiro, en el marco de la 11° edición del Concurso IB50K.

El premio especial “Santa Fe para transformar Santa Fe” consiste en un aporte de 5 mil dólares al mejor proyecto finalista que esté orientado a la innovación para la transformación de la cadena agroalimentaria y agroindustrial, y está destinado a equipos conformados en su mayoría por santafesinos.

En el marco del lanzamiento, el gobernador señaló que “el deseo de la provincia de Santa Fe es acompañar este tipo de iniciativas, detrás de la búsqueda de un vínculo creciente del sector científico tecnológico con la sociedad, y el desarrollo de empresas de base tecnológica”.

En esa línea, dijo que Santa Fe va a “aportar no solamente al acompañamiento del desarrollo de las capacidades de nuestros talentos, sino también a la posibilidad concreta de trabajar desde nuestra provincia, con las posibilidades de demostrar que este sector va a ser uno de los más importantes de la Argentina, y donde Santa Fe tiene que ser uno de los protagonistas”.

Por otro lado, Perotti ratificó su decisión de “tener el mayor desarrollo científico-tecnológico y generar los mayores niveles de conocimiento, para que la gente pueda tener un mejor futuro, y es allí donde nosotros tenemos nuestro firme compromiso”.

Finalmente, en relación al programa, destacó que “es el reconocimiento a nuestro sector científico - tecnológico, a uno de los activos más importantes que tiene la Argentina, tener un sistema científico integrado, que ha distinguido al país en el mundo, y es una de las características más salientes”.

Y concluyó: “Traerlos con un concurso es ponerlos más cerca de cada uno de los santafesinos y santafesinas, y con esa participación ya estamos ganando, porque van a ser parte de este proceso que nos dará un trabajo para seguir potenciando nuestro ecosistema científico - tecnológico tan virtuoso en la Argentina, y con fuerte presencia en nuestra provincia que nos llena de orgullo”.

**SANTA FE PARA TRANSFORMAR SANTA FE**

El premio apunta a generar soluciones aplicadas en toda la cadena, incluyendo nuevos cultivos, generación de proteínas, alimentos funcionales y digitalización de procesos, entre otros. Para otorgar el premio se valorará la usabilidad, escalabilidad e integración de la solución tecnológica propuesta; contar con un Mínimo Producto Viable; la perspectiva de género; y el anclaje territorial.

“Nos llena de orgullo ser padrinos del Balseiro, y estar acompañando a los mejores emprendedores tecnológicos, que ayuden a posicionar a la provincia de Santa Fe en un polo de la economía del conocimiento”, señaló la secretaria de Ciencia, Tecnología e Innovación, Marina Baima durante el lanzamiento, del que también participó el ministro de Producción, Ciencia y Tecnología, Daniel Costamagna.

Además, expresó que “el objetivo es formar talento joven, y adquirir habilidades para complejizar la matriz productiva santafesina a través de la ciencia, tecnología e innovación, para formar futuras empresas de base tecnológica”.

La propuesta se enmarca en la 11ª edición del Concurso IB50K, y entregará una suma total superior a 50 mil dólares entre los proyectos que resulten ganadores y se repartirán importantes premios especiales que provienen exclusivamente de aportes realizados por empresas e instituciones.