---
category: Deportes
date: 2021-01-07T10:00:32Z
thumbnail: https://assets.3dnoticias.com.ar/07012021_union-aldosivi.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Por Marisa Lemos
resumen: Tricota Tatengue
title: Tricota Tatengue
entradilla: Con goles de Luna, Cañete y García, el tate se impuso frente a Aldosivi
  en una magnífica noche Marplatense.

---
**Unión de Santa Fe le ganó 3 a 1** al tiburón por la cuarta fecha de la Zona A de la Fase Complementación de la Copa Diego Maradona.

Bajo esa órbita, el primer tiempo fue favorable para el equipo visitante. Es que, cuando pasaban a penas cinco minutos de la primera parte, Mauro Luna Diale puso el 1-0 con el que los equipos se fueron al entretiempo.

Durante el complemento, Francisco Grahl lo empató a los 78' de penal, luego de una mano en el área del capitán Claudio Corvalán.

Tres minutos después, Ezequiel Cañete, volvió a poner en ventaja al equipo tatengue con un fuerte derechazo que fue clavada en el ángulo.

Ya terminando la velada, apareció un centro desde la derecha, tras una trepada de Corvalán, encontró dentro del área chica a Juan Manuel García, que con su empeine derecho puso el 3 a 1 definitivo.

**Con este resultado, Unión queda como escolta de su zona y pretende darle pelea a Rosario central que en este momento se encuentra puntero**. 

El próximo encuentro del tatengue será frente a Patronato el día 9 de enero, a las 19:00 horas, en el Estadio 15 de abril.