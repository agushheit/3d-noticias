---
category: Agenda Ciudadana
date: 2021-03-14T06:00:56-03:00
thumbnail: https://assets.3dnoticias.com.ar/ALBERT.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Unidad, vacunas y reactivación económica, las apuestas del FdT para llegar
  "entero" a las elecciones
title: Unidad, vacunas y reactivación económica, las apuestas del FdT para llegar
  "entero" a las elecciones
entradilla: Para el segundo semestre, la Casa Rosada planea lanzar un "operativo seducción",
  probablemente de índole económico, en un intento por robustecer su caudal de respaldo
  en los comicios de octubre.

---
Para el segundo semestre, la Casa Rosada planea lanzar un "operativo seducción", probablemente de índole económico, en un intento por robustecer su caudal de respaldo en los comicios de octubre.

El Gobierno sabe que el camino hacia las elecciones de medio término de este año podría tornarse pedregoso si la Argentina es azotada por una segunda ola de la pandemia de Covid-19 durante los meses de otoño e invierno, tras las adversidades vividas durante el último año.

Un rebrote podría afectar los indicadores económicos que, en las últimas semanas, según la Casa Rosada, han mostrado síntomas de recuperación después de la parálisis registrada en 2020 en la estructura productiva doméstica, con motivo de la emergencia sanitaria por el avance del coronavirus.

según pudo averiguar, consideran que no están dadas las condiciones hoy por hoy para regresar a una instancia de cuarentena estricta, aunque una eventual disparada de los contagios en otoño e invierno podría ubicar al Gobierno ante la disyuntiva de volver a cerrar el grifo o no, en medio de esta "nueva normalidad" por la que transita la sociedad argentina.

De cualquier manera, la gestión de Alberto Fernández apuesta por redoblar el paso en la campaña de vacunación, en el caso de que el país reciba las dosis que está previsto que lleguen en las próximas semanas, incluyendo las de AstraZeneca, tras el acuerdo entre la Argentina, México, la Universidad de Oxford y ese laboratorio para la provisión del fármaco.

Las vacunas cumplen un rol preponderante en el tridente de factores que el Gobierno considera cruciales para llegar "entero" a las elecciones legislativas previstas para este año, junto con la "unidad" del oficialismo y la "reactivación económica" por la que está bregando la administración Fernández, buscando a su vez acuerdos con sindicatos y formadores de precios.

En este sentido, la Casa Rosada planea lanzar un "operativo seducción" para después del primer semestre del año, a fin de expandir su caudal de respaldo en las urnas, luego de un primer intento de reconciliarse con sectores desencantados de la clase media con el proyecto que prevé una suba del mínimo no imponible del impuesto a las ganancias.

Tras los meses más fríos del año, que podrían resultar complejos en el caso de que se produzca un rebrote de Covid-19, el Gobierno prevé encarar el tramo decisivo de la campaña electoral con una serie de "incentivos", probablemente de índole económico, para robustecer sus aspiraciones de triunfo en octubre.

Claro que una victoria electoral, en unos comicios en los que el Gobierno someterá a escrutinio sus primeros dos años de labor, depende de una serie de factores que trascienden al llamado "voto convencido", especialmente en medio de un contexto extraordinario con motivo de la pandemia de coronavirus.

En la Casa Rosada trabajan en un plan para atraer más respaldo con métodos pragmáticos de gestión, característicos de la forma de conducción que encarna Fernández, más allá de algún sobresalto como el registrado este sábado en Lago Puelo, en Chubut, durante una recorrida por zonas afectadas por incendios forestales en la Comarca Andina del Paralelo 42.

**Un ruido político "innecesario"**

En este sentido, falló el operativo de seguridad montado en torno de la figura presidencial en la Patagonia y un grupo de militantes anti-minería atacaron el vehículo en el que se trasladaba el jefe de Estado, en una agresión que fue repudiada por el arco político nacional en su conjunto.

El Gobierno viene bregando por la "unidad nacional", aunque por estos días y con miras a las elecciones programadas para octubre próximo, su primer objetivo es mantener calmas las tensiones que se registran puertas adentro en el FdT -y que las propias fuentes del oficialismo admiten que existen-.

A propósito, en la Casa Rosada consideran que las disputas por la presidencia del PJ Bonaerense generan en la actualidad un ruido político "innecesario", con el diputado nacional y líder de La Cámpora Máximo Kirchner y el intendente de Esteban Echeverría, Fernando Gray, como protagonistas.

El Gobierno entiende que es clave la "unidad" del FdT, aunque probablemente los roces internos recrudezcan cuando sea el momento del cierre de listas y de la designación de candidatos: ¿quién tendrá la última palabra entonces? ¿El presidente o la "jefa" de la coalición, Cristina Fernández de Kirchner?

Si bien las tensiones existen, "están todos adentro", dijeron las fuentes consultadas por NA, en referencia a las distintas vertientes del Partido Justicialista (PJ) que forman parte de la gestión Fernández. Sin embargo, admiten en el Gobierno que cuando llegue la hora de discutir sobre la conformación de las listas, todos los que estén sentados a la mesa en esa negociación "van a empezar a tironear del mantel".

Mientras tanto, una encuesta de la consultora Management & Fit realizada después del escándalo de las vacunaciones de privilegio mostró un deterioro -para el Gobierno- en las mediciones sobre "aprobación de gestión" de Fernández.

Ese indicador, de acuerdo con M&F, se ubica en el nivel más bajo desde que el jefe de Estado comenzó su labor al frente de la Casa Rosada, con un 36,8 por ciento de valoraciones positivas, largamente por debajo de la marca de 48,24% (de los votos) que registró en las elecciones de 2019.

El nivel de desaprobación de su gestión llega al 54,9%, con un diferencial de imagen de -5%. De cualquier manera, en Balcarce 50 le restan importancia a estos indicadores: "Son una consecuencia de la pandemia y en comparación con otros gobiernos de la región, estamos mucho mejor que el resto", dijeron las fuentes consultadas por NA.