---
category: Agenda Ciudadana
date: 2021-11-11T06:00:04-03:00
thumbnail: https://assets.3dnoticias.com.ar/DOLAR.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: El dólar blue rompe todos los récords y llega a niveles impensables en medio
  de la tensión
title: El dólar blue rompe todos los récords y llega a niveles impensables en medio
  de la tensión
entradilla: 'El Banco Central debió vender USD 180 millones. La divisa estadounidense
  que se negocia en el mercado marginal dio un brinco y alcanza un nuevo récord histórico.
  ¿Qué harán para frenarla? '

---
A medida que se acercan las elecciones legislativas del domingo, el dólar blue reafirma su raid alcista y hoy alcanzó un sorprendente nuevo récord de $205, ante una mayor presión dolarizadora.

La divisa negociada en el mercado marginal subió $5,50 respecto de la jornada anterior, en medio de más demanda y especulación de un mercado que está calculando qué ocurrirá a partir del lunes.

En este escenario de incertidumbre, el Banco Central debió vender unos USD 180 millones para contener la alta demanda en el mercado cambiario.

Se suman voces que señalan las dificultades del Gobierno para frenar una devaluación en las próximas semanas, pero el ministro de Economía, Martín Guzmán, ratificó que se mantendrá el esquema actual de depreciación gradual del peso por detrás de la inflación.

El hecho de que la cotización blue haya logrado romper el techo de los $200 constituye una señal de alerta, coinciden analistas y consultoras.

En el mercado mayorista, plaza donde interviene el Banco Central con operaciones de regulación de liquidez, el billete verde es pactado a $100,14, con una ganancia mínima de tres centavos.

Además, la brecha cambiaria quiebra el techo del 100%, ahora en 104%, el margen más amplio desde noviembre de 2020.

Guzmán dijo que mantendrá su política de no devaluar en forma brusca el tipo de cambio oficial.

Las declaraciones se produjeron luego de que el representante argentino ante el FMI, Sergio Chodos, reconoció que el tipo de cambio es uno de los temas en el que más se trabaja en las negociaciones con los técnicos del organismo multilateral.

El Fondo Monetario quiere saber cuál es el plan de salida gradual del cepo cambiario que está dispuesto a aplicar el gobierno.

En medio de lo que el mercado califica de "mamushka de cepos", porque las medidas de restricción se van superponiendo unas sobre otras, el Banco Central y la CNV ya impusieron unas 17 restricciones para tratar de frenar la sangría de reservas.

En los últimos días también se sumó la Unidad de Información Financiera (UIF), que advirtió a las sociedades de bolsa sobre la necesidad de supervisar al centavo las operaciones de CCL y MEP, y sobre todo la procedencia de los fondos.

En el mercado causaron sorpresa, además, las declaraciones de Guzmán, quien frente a un auditorio de empresarios dijo que "el Banco Central no ha perdido reservas. De hecho, las ha acumulado".

**Un informe que hizo más ruido.**

Según un informe del Estudio Broda que hizo ruido en el mercado, tras el pago al FMI de USD 1.900 millones el próximo 22 de diciembre, las reservas netas quedarían en famélicos USD 1.000 millones.

Es más, el reporte alerta que las "reservas liquidas propias disponibles serían USD 5.000 millones negativas.

Ante este escenario de escasez de divisas, en 2022 la Argentina tiene que pagar USD 19.000 millones (USD 4.000 vencen en marzo).

Sin plata ni financiamiento, el futuro económico se presenta cada vez más complejo y eso se refleja en las cotizaciones de la divisa.

El Gobierno continúa reclamando al Fondo dejar de lado el cobro de sobretasas para países más sobre endeudados que, según el Estudio Broda, representan unos USD 1.295 millones anuales.

También busca un acuerdo de más largo plazo, que por ahora los países que integran el organismo han decidido no tocar.

El dólar blue a $205, la brecha cambiaria en 104% y el Central teniendo que intervenir con bonos AL30 y GD30 para frenar la suba del "contado con liquidación", reflejan la debilitada situación del Gobierno frente a una posible corrida cambiaria.

Desde el Banco Central dicen que el lunes seguirá, como hasta ahora, la política de actualización del dólar oficial por debajo de la inflación.

Para el economista Rodrigo Álvarez, el Gobierno busca desterrar la idea extendida en el mercado de habrá saltos abruptos en el  
tipo de cambio, y se inclina por mostrar que tiene las espaldas para una devaluación en cuotas.

Pero el economista alerta que "cuando vamos a las experiencias, la suavidad siempre es relativa a la velocidad de la inflación".

Para Alvarez, si el Gobierno quiere recuperar algo de todo el terreno perdido, "tendría que devaluar por encima del ritmo de la inflación varios meses".

El Central continúa en noviembre con su diaria intervención reguladora de liquidez en el mercado mayorista.

En noviembre acumula un saldo negativo de unos 142 millones de dólares.