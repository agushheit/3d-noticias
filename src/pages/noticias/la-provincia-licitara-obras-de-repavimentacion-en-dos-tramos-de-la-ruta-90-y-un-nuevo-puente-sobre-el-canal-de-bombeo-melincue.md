---
category: Estado Real
date: 2021-07-05T06:00:34-03:00
thumbnail: https://assets.3dnoticias.com.ar/MELINCUE.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Provincia licitará obras de repavimentación en dos tramos de la ruta 90,
  y un nuevo puente sobre el canal de bombeo Melincué
title: La Provincia licitará obras de repavimentación en dos tramos de la ruta 90,
  y un nuevo puente sobre el canal de bombeo Melincué
entradilla: Se invertirán más de 2.100 millones para las obras que incluyen a los
  departamentos General López y Constitución. Asimismo, se construirá un nuevo puente
  sobre Canal de Bombeo Melincué.

---
El Ministerio de Infraestructura, Servicios Públicos y Hábitat llevará adelante tres licitaciones el día martes 6 de julio, para ejecutar la repavimentación de dos secciones en la Ruta Provincial N°90, que conecta los departamentos Constitución y General López en la zona sur del territorio; y para la construcción de nuevo puente sobre Canal de Bombeo -Estación de Bombeo N° 2 – Melincué, departamento General López.

**REPAVIMENTACIÓN RUTA PROVINCIAL 90**

Sobre las obras viales, el administrador de Vialidad Provincial, Oscar Ceschi explicó que "son construcciones que van cumpliendo su vida útil y, luego de analizarlo junto al gobernador Omar Perotti y la ministra Silvina Frana, se decidió que la solución es reconstruir la ruta. Son casi 60 kilómetros de intervención, que incluyen una rotonda en la intersección con la ruta 18, la mejora de varios accesos y la repavimentación del enlace con la ruta 21, siempre pensando en las condiciones de circulación tanto para la producción como para los habitantes de la zona".

El funcionario de Vialidad, también detalló la importancia del corredor y otros trabajos en ejecución, y destacó que "es una de las rutas más importantes para ambos departamentos, porque cruza transversalmente la región llegando a los puertos de la zona de Villa Constitución y toda la Ruta 21, así como a la autopista Rosario - Buenos Aires. Es por eso que tenemos en marcha trabajos de repavimentación en la zona de Máximo Paz, de bacheos hasta Chapuy y ahora sumaremos estas dos repavimentaciones".

**LOS TRABAJOS**

En el departamento General López, se trabajará en unos 28,8 kilómetros desde Ruta Nacional N°8 hasta el enlace con la Ruta Provincial N°6-S, en cercanías de las localidades de Melincué y Elortondo. Sobre el trayecto se realizará el reciclado con cemento en una profundidad de 35 centímetros, carpeta de concreto asfáltico en capas de 8 y 4 centímetros. El plazo de ejecución es de 12 meses y el presupuesto oficial se encuentra fijado en $965.650.485,86. La licitación se realizará el martes 6 de julio a las 12:30 horas en el Club Atlético de Elortondo.

En jurisdicción del departamento Constitución las tareas se desarrollarán en 29,8 kilómetros localizados entre la Ruta Nacional N°9 y el acceso a Cañada Rica, en inmediaciones a Sargento Cabral. En el segmento, la traza existente es de hormigón y ante la gran cantidad de losas deterioradas, se realizará el retiro y triturado de las mismas. Posteriormente, se procederá al saneamiento con suelo cal en un espesor de 40 centímetros, una base de estabilizado granular cementado de 21, una compuesta por arena asfalto de 2, sobre la cual se ejecutará la carpeta de rodamiento de 6 centímetros de espesor y 7,05 metros de ancho.

Entre las tareas complementarias se destaca la construcción de una rotonda en el enlace con la Ruta Provincial N°18 con nueva iluminación y señalización; y la repavimentación del cruce con Ruta Provincial N°21, en la localidad de Villa Constitución. Además, se incluirán tareas de banquinas e instalación de luminaria led en los enlaces con Ruta Nacional 9 y los accesos a Rueda, Godoy, Stephenson y Sargento Cabral.

Ante la magnitud de las intervenciones, se dispuso un plazo de 24 meses de obra y se estipuló un presupuesto oficial de $1.166.815.466,38. El acto licitatorio se realizará el martes 6 de julio a las 16:30 en la Sociedad Italiana de la localidad de Godoy (Italia 660).

**CONSTRUCCIÓN DE NUEVO PUENTE SOBRE CANAL DE BOMBEO – MELINCUÉ**

Respecto a las obras hídricas, el secretario de Recursos Hídricos, Roberto Gioria explicó que “se complementan con los trabajos que están actualmente en ejecución para la refuncionalización de las estaciones de Bombeo 1 y 2 para la regulación hídrica de la laguna de Melincué. Se ubica en las inmediaciones de la estación de bombeo Nº 2 donde vamos a construir un puente nuevo, contiguo al existente (que cruza el canal de vinculación entre la laguna Melincué y el canal San Urbano), sobre el canal de acceso a la nueva estación en ejecución”.

“Lo que buscamos con estas obras es facilitar el drenaje desde la laguna de Melincué hacia el canal San Urbano ante eventos pluviométricos de magnitud normal, y poder drenar los excedentes de la laguna y evitar así su desborde. Trabajamos sobre una solución integral para todos los habitantes de la región, puesto que la localidad de Melincué se desarrolló frente a esta laguna, y cualquier exceso en la cuenca repercute de manera inmediata en la localidad”, cerró Gioria.

Posee un presupuesto oficial de $20.398.476,34, un plazo de ejecución de 8 meses. El acto licitatorio se realizará el día martes 6 a las 11 horas en las dependencias de la Sala de Cultura "Liberato Rebora", sita en calle Laprida y Rivadavia de la localidad de Melincué.