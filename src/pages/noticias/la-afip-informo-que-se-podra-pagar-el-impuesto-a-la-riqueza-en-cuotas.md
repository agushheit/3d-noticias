---
category: Agenda Ciudadana
date: 2021-03-11T07:30:10-03:00
thumbnail: https://assets.3dnoticias.com.ar/afip1.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: La AFIP informó que se podrá pagar el Impuesto a la Riqueza en cuotas
title: La AFIP informó que se podrá pagar el Impuesto a la Riqueza en cuotas
entradilla: La normativa, que será publicada en el Boletín Oficial, busca contribuir
  al cumplimiento de las obligaciones de los contribuyentes.

---
La Administración Federal de Ingresos Públicos (AFIP) habilitó hoy la posibilidad de cancelar el Aporte Solidario y Extraordinario a través de un régimen de facilidades de pago.

El organismo estableció que las personas alcanzadas por el denominado "Impuesto a la Riqueza", podrán optar entre el 23 de marzo y el 28 de abril, inclusive, por realizar un anticipo del 20% de la deuda consolidada e ingresar el saldo resultante en cinco pagos mensuales y consecutivos.

La Resolución General N° 4942 dispone que la tasa de financiación para quienes opten por esta vía será la correspondiente a la de los intereses resarcitorios del organismo.

La normativa, que será publicada en el Boletín Oficial, busca contribuir al cumplimiento de las obligaciones de los contribuyentes.

Para adherir al régimen de facilidades de pago es requisito concretar el anticipo del 20% de la deuda consolidada, aclaró el organismo en un comunicado.

Señaló que las cinco cuotas restantes se abonarán el 16 de cada mes a partir del mes siguiente de concretado el pago inicial.

El plazo para presentar la declaración jurada de los contribuyentes alcanzados por el Aporte Solidario y Extraordinario es el 30 de marzo.

En tanto, la solicitud de adhesión al régimen no podrá ser rectificada y se considerará aceptada, siempre que se cumplan en su totalidad las condiciones y los requisitos previstos en la normativa.

El Aporte Solidario y Extraordinario alcanza a las personas humanas y sucesiones indivisas residentes en el país por la totalidad de sus bienes en el país y en el exterior y a las personas humanas y sucesiones indivisas no residentes, por la totalidad de sus bienes en el país, siempre que superen la suma de $200 millones de pesos.

La fecha para la valuación de los bienes es la de entrada en vigencia de la Ley, es decir el 18 de diciembre de 2020.

La ley aprobada por el Congreso a finales del año pasado establece el destino que tendrán los fondos recaudados del Aporte Solidario y Extraordinario para enfrentar la Pandemia.

"Son cinco las asignaciones especificadas: un 20% será para la compra y/o elaboración de equipamiento médico, elementos de protección, medicamentos, vacunas y todo otro insumo crítico para la prevención y asistencia sanitaria", remarcó.

Señaló que "otro 20% se destinará a subsidios a las micro, pequeñas y medianas empresas, con el principal objetivo de sostener el empleo y las remuneraciones de sus trabajadores".

Sostuvo que "un porcentaje idéntico será para el programa integral de becas Progresar con el fin de reforzar este programa que acompaña a las y los estudiantes con un incentivo económico y un importante estímulo personal en todos los niveles de formación durante su trayectoria educativa y/o académica".

Puntualizó que otro 15% "se asignará para el Fondo de Integración Socio Urbana, enfocado en la mejora de la salud y de las condiciones habitacionales de los habitantes de los barrios populares y un 25% a programas y proyectos que apruebe la Secretaría de Energía de la Nación, de exploración, desarrollo y producción de gas natural".