---
category: El Campo
date: 2021-04-29T07:56:51-03:00
thumbnail: https://assets.3dnoticias.com.ar/biocombustible.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de LT10
resumen: Malestar de la Cámara Panamericana de Biocombustibles Avanzados
title: Malestar de la Cámara Panamericana de Biocombustibles Avanzados
entradilla: Esta cámara no está de acuerdo con la ley que impulsan diputados kirchneristas
  con el aval del Gobierno. Aseguran que la norma que pretenden atentan contra el
  medio ambiente.

---
La Cámara Panamericana de Biocombustibles Avanzados manifestó su desacuerdo con la nueva ley de biocombustibles que impulsan diputados kirchneristas con el aval del Gobierno nacional.  

Axel Boerr, presidente de la Cámara Panamericana de Biocombustibles Avanzados, sostuvo por LT10 que esta norma no está en línea con preservar el medio ambiente y buscar una transición energética.

Además, el presidente de la Cámara Panamericana de Biocombustibles Avanzados que “esta ley baja el corte de los biocombustibles y le da a la secretaría de Energía la posibilidad de bajarlos aún más. Se debería mantener el 10 y el 12 % de cortes. Respecto al tema de precio, le da una discrecionalidad parecida a la secretaría de Energía”.

Finalmente, Axel Boerr remarcó que “los biocombustibles se incorporan por el cambio climático no para ahorrar plata. El ahorro de emisiones va a permitir que la industria pueda crecer porque tenemos un tope. La única forma es reducir los humos de gasoil que emite el transporte. Por estos motivos, no estamos de acuerdo”.