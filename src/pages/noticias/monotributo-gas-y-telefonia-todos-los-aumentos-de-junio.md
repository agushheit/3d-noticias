---
category: Agenda Ciudadana
date: 2021-06-02T09:08:46-03:00
thumbnail: https://assets.3dnoticias.com.ar/gas.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Monotributo, gas y telefonía: todos los aumentos de junio'
title: 'Monotributo, gas y telefonía: todos los aumentos de junio'
entradilla: Los nuevos valores se verán reflejados en las facturas de julio. Además,
  se esperan más aumentos en los siguientes meses. De todas formas, el único sector
  que no sufrirá subas es la nafta.

---
El mes de junio comienza con aumentos de precios regulados para los hogares de todo el país. Se Los sectores afectados serán la cuota del monotributista, el gas y la telefonía. A la par, suben las asignaciones familiares, el salario mínimo vital y móvil (SMVM), y jubilaciones. Mientras que la nafta es el único sector que no se verá afectado.

También, aumentará –por tercera vez- el Salario Mínimo Vital y Móvil. El valor rondará en 4%. Y las siguientes subas están pactadas para julio, agosto, noviembre y febrero del año sucesivo. De esta manera, se llegaría al monto de $29.160 (suba de 35% en siete cuotas). A la par, se ajustarán las prestaciones de desempleo.

Por otro lado, las naftas no subirán. Es la primera vez que ocurre desde los últimos 13 aumentos tras el congelamiento de sus precios en agosto de 2020. El Gobierno, en conjunto con YPF, planea no incrementar sus valores en lo que queda del año o, al menos, hasta las elecciones legislativas de noviembre.

**La cuota del monotributo sube más de un 35,3%**

El ajuste será tanto para las cuotas como para las escalas. En abril se aprobó la ley y, recién a finales de este mes, se promulgó. Seguido de la reglamentación que no llegó a modificar el pago final de mayo.

Por otro lado, AFIP recategorizará de oficio a quienes les corresponden una categoría distinta a la actualización. Este año, quien pasó al régimen de autónomos voluntariamente y facturó hasta un 25% más que el tope de régimen, puede volver a la categoría monotributista. Mientras que aquellos que superaron por un valor mayor, pasarán al régimen general y, a su vez, tendrán una pila de beneficios para reducir la carga tributaria.

**El gas aumenta entre un 6 y 7%**

La suba será aplicable para el "valor agregado de distribución" (VAD), que es el margen de las distribuidoras. Según fuentes del sector gasífero, por ahora, será el único ajuste en el año. Aunque queda abierta la posibilidad de aplicar otro incremento, tras las elecciones legislativas, para los productores del sector.

Además de este ajuste, los usuarios soportarán también un aumento en los importes de las tarifas en junio, julio y agosto, los meses más fríos del año, ya con la corrección tarifaria aplicada.

Enargas aplicará una recomposición del 6% en las tarifas en el componente "transporte y distribución", mientras que las distribuidoras habían solicitado una recomposición de sus márgenes en torno al 50% por la "emergencia económica", pero el gobierno -por ahora- rechazó el pedido.

**La telefonía móvil aumenta entre un 7 y un 10%**

El aumento es la segunda parte del iniciado en mayo. Ocurrió luego de que la Justicia dictara una cautelar contra el DNU que las declaró “servicio público”. Desde ENACOM, advirtieron que irán a la Corte Suprema para frenar las subas.

Se espera que la inflación de mayo esté entre 3,5% y 4%. Y, según especialistas, en junio podría desacelerarse por un mayor efecto del cierre de actividades debido a la pandemia del coronavirus. Además, influiría el menor ritmo de depreciación del cambio oficial del Banco Central.

**Aumentan jubilaciones y AUH**

Las Jubilaciones y Asignación Universal por Hijo (AUH) tendrán subas de 12,12% en línea con el aumento trimestral. Los siguientes serán en septiembre y diciembre. Por lo tanto, los montos quedarán:

* Jubilación mínima de $ 20.571 a $ 23.064
*  AUH y AUE General total de $ 4017 a $ 4503
*  AUH y AUE Zona 1 total de $ 5223 a $ 5856
*  AUH para niños discapacitados General total de $ 13.090 a $ 14.676
*  AUH para niños discapacitados Zona 1 total de $ 17.017 a $ 19.079
*  Pensión Universal para el Adulto Mayor (PUAM) de $ 16.457 a $ 18.451
*  Pensión No Contributiva (PNC) de $ 14.188 a $ 16.145
*  Pensión No Contributiva (PNC) para madre de siete hijos o más de $ 20.268 a $ 23.064