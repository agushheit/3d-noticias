---
category: Estado Real
date: 2021-08-06T06:15:01-03:00
thumbnail: https://assets.3dnoticias.com.ar/SUPERIOR.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La provincia diseña las opciones para la vuelta de las clases presenciales
  en los institutos superiores
title: La provincia diseña las opciones para la vuelta de las clases presenciales
  en los institutos superiores
entradilla: El regreso a la presencialidad se efectivizará el próximo lunes 9 de agosto,
  de manera gradual, progresiva y cuidada a la modalidad de alternancia (presencialidad
  y bimodalidad).

---
El Ministerio de Educación formalizó las alternativas para el regreso a la presencialidad en los institutos superiores del sistema educativo provincial, que involucra a cerca de 69.000 alumnos y alumnas, que cursan sus estudios en 180 establecimientos, tanto públicos como privados.

El regreso a clases presenciales en el nivel superior de la educación santafesina se concretará el próximo lunes 9 de agosto, de forma escalonada y gradual, con diferentes alternativas que suponen desde presencialidad en las prácticas docentes y prácticas profesionalizantes de las tecnicaturas hasta presencialidad en burbuja bimodal de modo similar a como se desarrollan las actividades en la educación inicial, primaria y secundaria, con todo los cuidados que se recomiendan en las normativas federales y provinciales.

Al respecto, la subsecretaria de Educación Superior, Patricia Moscato, precisó que “el nivel superior a partir del 9 de agosto empieza una vuelta gradual, progresiva y cuidada a la modalidad de alternancia (presencialidad y bimodalidad). Este regreso está pensado en función de las características institucionales, y por lo tanto desde el Ministerio hemos propuesto cuatro opciones posibles de regreso que van desde una menor presencialidad a una en alternancia o bimodal plena”.

Asimismo, Moscato indicó que “las opciones se han pensado respetando la diversidad y la heterogeneidad del nivel superior, donde por ejemplo, tenemos institutos de más de 3000 estudiantes con comisiones de hasta 70 alumnos y alumnas; al mismo tiempo tenemos casas de estudio con mucho menor matricula, con comisiones de 15 o 20 estudiantes en salones amplios, y por ello hemos diseñado diferentes opciones, que suponen un análisis de cada territorio, donde la intención es adaptar esta propuesta a la diversidad regional e institucional para comenzar a pensar en una gradualidad de presencialidad en todo el nivel”.

**Cuatro opciones**

En función de la diversidad que plantea el nivel, se determinó que el consejo académico de cada instituto, o su equivalente según la orgánica institucional, puedan evaluar entre las siguientes opciones, aquella que asumirá el Instituto para el regreso a la presencialidad.

La opción más básica es aquella que supone empezar el próximo lunes 9 con las prácticas profesionalizantes y prácticas docentes, en las organizaciones asociadas para las tecnicaturas y en las escuelas asociadas para los profesorados.

La segunda opción implica que a estas prácticas se le agregue el cursado de las unidades curriculares en el último año de la carrera.

La tercera opción incluye la presencialidad planteada en las alternativas anteriores (prácticas docentes, prácticas profesionalizantes, último año de la carrera) y se le agrega las unidades curriculares del segundo año de la carrera.

La última opción sería con presencialidad o bimodalidad en todas las unidades curriculares de la carrera.