---
category: Agenda Ciudadana
date: 2022-04-18T09:14:30-03:00
thumbnail: https://assets.3dnoticias.com.ar/worker-gdbf7ec4b4_1920.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Uno Santa Fe'
resumen: 'Paritarias: los gremios hablan de "acuerdos cortos" que sean trimestrales
  o cuatrimestrales'
title: 'Paritarias: los gremios hablan de "acuerdos cortos" que sean trimestrales
  o cuatrimestrales'
entradilla: La totalidad de los gremios avizoran para este año una suba de precios
  que rondaría casi el 60 por ciento. La postura  es negociar acuerdos más cortos,
  que puedan ser revisados y/o actualizados.

---
**Una treintena de organizaciones sindicales ya negocia el reajuste de sus salarios en el marco de paritarias convocadas o no de manera oficial por el Ministerio de Trabajo, mientras la totalidad de los voceros gremiales coincide en que el adelantamiento de las negociaciones debe "ganarle al elevado proceso inflacionario", por lo cual los dirigentes se inclinan por "acuerdos cortos".**

La cartera laboral firmó el miércoles 6 de abril la Resolución 388/22, publicada en el Boletín Oficial del día siguiente, que oficializó la convocatoria al diálogo paritario para más de veinte gremios y las cámaras empresariales de cada sector, aunque otras organizaciones sindicales ya habían comenzado a negociar varias semanas antes.

Es el caso de la Federación Argentina de Empleados de Comercio y Servicios (Faecys), el sindicato con más afiliados del país, que el 21 de marzo inició el diálogo convencional para actualizar salarios con las tres cámaras de la actividad, luego de evaluar "los indicadores y variables económicas, lo que fue totalmente determinante", según comunicó entonces el gremio mercantil.

**El propio titular de la organización, Armando Cavalieri, había sostenido en ese momento que "las consecuencias de la aceleración de precios en los últimos meses proporcionaron datos alarmantes y, a partir de esos parámetros, se adoptó la decisión de generar acuerdos más cortos; es decir, del orden trimestral".**

La totalidad de los gremios avizoran para este año **una suba de precios que rondaría casi el 60 por ciento, por lo que la postura adoptada por los sindicalistas es negociar acuerdos más cortos, trimestrales o cuatrimestrales, que puedan ser revisados y/o actualizados**.

Uno de los sindicatos que adoptó esta decisión es la Unión Obrera de la Construcción (Uocra) de Gerardo Martínez, que fue incorporada por Trabajo en la apertura temprana de paritarias prevista en su reciente Resolución 388, que lleva la firma de Claudio Moroni.

En 2021, la Uocra firmó una paritaria cuyo último ajuste -del 4 por ciento- se cobrará en mayo próximo, por lo que totalizará para el año una mejora salarial del 56,4%.

En relación a las perspectivas para 2022, desde el sindicato de la construcción confirmaron a Télam que la expectativa con la que iniciarán las negociaciones tras la convocatoria oficial "es firmar un acuerdo que se ubique por encima de la inflación, aunque la modalidad se perfilará cuando comience el diálogo".

"Aún no hubo negociaciones formales, pero sí un encuentro preliminar. Es preciso hacer los números y proyecciones", agregaron las fuentes.

**En la Uocra sostienen que ante el proceso inflacionario probablemente sea necesario acordar tramos cortos trimestrales o cuatrimestrales, aunque todavía no descartaron el esquema anual pero siempre con cláusula de revisión.**

Otro de los gremios convocados por Trabajo para adelantar la negociación salarial es la Federación Gráfica Bonaerense (FGB) que lidera Héctor Amichetti, cuya paritaria 2021 había vencido el 31 de marzo.

Tras ser incluido en la Resolución de la cartera laboral que convocó a 26 gremios y cámaras empresariales a iniciar las negociaciones, el sindicato de los gráficos llegó a un acuerdo el miércoles último consistente en un incremento salarial del 50 por ciento a pagar entre abril y octubre próximos, en tres tramos, y con cláusula de revisión.

"Luego de quince días de discusión y varios plenarios de delegados, se acordó con la cámara (Federación Argentina de la industria Gráfica y Afines) una mejora del 50 por ciento para el semestre abril-octubre, con cláusula de revisión en noviembre. Esa recomposición se abonará un 21 por ciento en abril, otro 14 desde julio y un 15 en octubre", aseguró Amichetti a Télam.

El plenario general de delegados del sindicato gráfico aprobó el acuerdo el miércoles anterior al inicio del fin de semana largo por las Pascuas, y ya fue presentado en la cartelera laboral.

**El sindicato mecánico (Smata) que conduce Ricardo Pignanelli, otro de los gremios fuertes que fue convocado por Trabajo para iniciar la negociación salarial, negocia desde hace muchos años -a partir de 2011- acuerdos paritarios trimestrales en función del costo de vida, y ya abrió la discusión en el sector de los talleres mecánicos.**

Pignanelli señaló la semana pasada que el sindicato tiene 72 convenios salariales, entre los cuales se encuentra el de talleres mecánicos, que "está muy retrasado", según palabras del propio dirigente.

En ese panorama, Smata inició las conversaciones formales para casi 18 mil trabajadores.

"Los talleres de alta monta andan muy bien, pero los urbanos y provinciales tienen las escalas salariales atrasadas. El Smata negocia una dinámica trimestral sobre el índice del costo de vida, que se basa en el Pacto Social de precios y salarios determinado por Juan Domingo Perón en 1973", explicó el secretario general.

Para Pignanelli, el objetivo que se impone para la negociación es ganarle a la inflación, porque en los últimos años, repasó, "se perdieron 24 mil pequeñas y medianas empresas y entre 2016 y 2019 se evaporó el 25 por ciento de poder adquisitivo", a lo que se sumó el impacto de la pandemia de Covid-19 que "debilitó todavía más a la industria".

"Es la hora de discutir en paritarias aumentos salariales y puestos de trabajo. Luego vendrá la negociación por la distribución de la riqueza", añadió Pignanelli.

**Junto a Uocra, Gráficos y Smata, Trabajo convocó a iniciar las discusiones salariales a los sindicatos de Camioneros, Sanidad, Gastronómicos, seguridad, maestranza, plásticos, pasteleros, del seguro, de la alimentación, textiles, de la televisión, trabajadores de aguas gaseosas, del vestido, químico y petroquímico, molinero, viajante, madera y Uecara: tres de ellos -entre ellos la Federación Gráfica- ya lograron un acuerdo.**

Mientras que los gráficos encabezados por Amichetti acordaron para el semestre abril-octubre una mejora del 50 por ciento y una revisión en noviembre, el sindicato del Seguro convino una recomposición del 37 por ciento para el período abril-septiembre y el gremio del vestido cerró en 53,4 por ciento en dos tramos (julio-octubre) más una gratificación extraordinaria del 7 por ciento del salario básico.

En paralelo a estas negociaciones en marcha, otros gremios ya habían cerrado sus paritarias por cuerda separada, más allá de la resolución de la cartera laboral: es el caso de los mineros en sus diversas ramas de la actividad, el gremio de la carne roja y avícola y el sector mutualista del sindicato de entidades deportivas y civiles (Uedyc).

Entre los gremios más influyentes por su poder de negociación, el titular de la Federación de Choferes de Camiones, Hugo Moyano, adelantó que en la mesa de discusión con las cámaras exigirá el pago de una suma fija de 20.000 pesos, para lo cual tomó como parámetro el acuerdo celebrado con ese fin entre los titulares del Senado y Diputados, Cristina Fernández de Kirchner y Sergio Massa, con el secretario de la Asociación del Personal Legislativo, Norberto Di Próspero.

**Desde la Asociación Bancaria, en tanto, su secretario general y diputado nacional por el Frente de Todos, Sergio Palazzo, declaró hace unos días el estado de alerta y movilización ante "la irrisoria propuesta empresaria".**

En cuanto a la Resolución 388/22, el ministro de Trabajo subrayó en sus fundamentos que en el marco de la mesa de diálogo y la concertación encabezada por el presidente Alberto Fernández era necesario "alinear expectativas y fortalecer la reactivación productiva de manera articulada con la creación de empleo formal y la mejora de los ingresos".

Además, en el texto se mencionaron los recientes encuentros entre la CGT y la UIA, en los que las partes coincidieron -remarcó la cartelera laboral- en "la necesidad de concertar acciones que permitan afrontar las dificultades económicas locales, agravadas por el conflicto en Ucrania, a fin de mejorar el poder adquisitivo de los salarios, cuidar la canasta básica de consumo de los argentinos y preservar la reactivación de la economía".