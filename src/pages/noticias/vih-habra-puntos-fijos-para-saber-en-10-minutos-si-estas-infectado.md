---
category: La Ciudad
date: 2020-11-30T11:23:55Z
thumbnail: https://assets.3dnoticias.com.ar/test-hiv.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: El Litoral'
resumen: 'VIH: habrá puntos fijos para saber en 10 minutos si estás infectado'
title: 'VIH: habrá puntos fijos para saber en 10 minutos si estás infectado'
entradilla: 'En la ciudad de Santa Fe se harán test rápidos desde el martes, y de
  forma sostenida, en el hospital Sayago. Una semana después se sumarán Cemafe y la
  Secretaría de Igualdad y Género. '

---
**Un 30 % de los 12 mil santafesinos que portan VIH no lo sabe**, estiman las autoridades de Salud. 

Detectar tempranamente a esas 3.600 personas es clave para construir una barrera en la transmisión de la enfermedad, que hasta finales del año pasado había infectado a más de 38 millones de personas en el mundo.

Con ese objetivo, la provincia iniciará este martes -día en que se visibiliza globalmente la lucha contra el avance del virus- una política sostenida de testeo rápido, con puntos fijos permanentes a los que cualquier persona podrá acercarse y conocer en minutos si está o no infectado.

"Queremos testear a la mayor cantidad de gente, para diagnosticar más y que rápidamente reciban tratamiento", explica al otro lado del teléfono Gerardo Perafan, el médico que está a cargo del Programa Provincial de VIH, Sífilis, Hepatitis y Tuberculosis, pero que trabaja la temática desde 1999, cuando empezaron a hacer tests rápidos en el hospital Centenario de Rosario. "La provincia nunca los implementó, hasta ahora", asegura. La pandemia demoró muchas cosas, y también esta política que estaba prevista arrancar en marzo.

El primer lugar que comenzará a hacerlos en la capital provincial es el hospital Sayago. Entre la segunda y tercera semana de diciembre comenzarán en Cemafe y en una sede de la Secretaría de Igualdad y Género de la provincia va a designar un tercer espacio, con la aspiración de sumar otros. Va a haber horarios fijos y gente capacitada para brindar asesoramiento y con todas las normas de bioseguridad. Y en toda la provincia se van a incorporar en Rafaela, Villa Constitución, Ceres y Rosario, donde ya se realizan.

##    
Población clave y beneficios

La meta del programa provincial es avanzar con las premisas que a nivel mundial propone OnuSida, el Programa Conjunto de las Naciones Unidas: es el objetivo 90-90-90, que consiste en que el 90% de las personas que viven con el VIH conozcan su estado serológico; que el 90% de las personas diagnosticadas con el VIH reciban terapia antirretrovírica continuada, y que el 90% de las personas que reciben terapia antirretrovírica tengan supresión viral. "de esta manera podemos disminuir un 90% la transmisión del virus".

"La idea es que se acerque quien lo desee, pero especialmente la población clave", dice Perafan. ¿Quiénes son? La OMS incluye a aquellas personas de determinados grupos que corren un mayor riesgo de adquirir el VIH: hombres que tienen relaciones sexuales con hombres; personas que se inyectan drogas; personas recluidas en cárceles y otros entornos cerrados; trabajadores del sexo y sus clientes; y personas transgénero.

Además, esta política servirá para empezar a rescatar a la gente que abandonó el tratamiento, lo cual genera dos beneficios: que esa población viva mejor y que no contagie.

 **Se estima que de los 8.400 santafesinos que se sabe que están infectados, solo reciben tratamiento, tanto en el sector público como en el privado, unos 6.500**. El resto, más de mil infectados los ha abandonado.

Y permite conocer a las personas que, a pesar de que portan el virus, tienen carga viral indetectable: "Ellos no transmiten el virus, con lo cual les mejoramos la calidad de vida", afirma Perafan. Por estos motivos defiende los test rápidos como "una política fundamental; es la mejor forma de llegar a la gente". Y agradece el apoyo que recibe del Ministerio de Salud, y de la ministra Sonia Martorano, en la provisión de todos los insumos.

## ¿Cómo es?

Quienes se acerquen a un centro de testeo y consejería van a recibir primero el asesoramiento de personal capacitado. Luego, si lo desea, se realiza el test, que es idéntico al de glucemia: se obtiene una gota de sangre y en 10 minutos se le entrega el resultado. Si es positivo, un médico especializado lo vincula inmediatamente al sistema de salud, realiza la confirmación diagnóstica con una carga viral, y en caso de que sea necesario, inicie el tratamiento.

"La ventaja es la rápida incorporación al sistema de salud. Lo que suele pasar es que si le decimos a una persona que vuelva en una semana a buscar el resultado, no va", remarca el médico.

A mediano plazo, se aspira a incorporar los test rápidos de Hepatitis B y C y de sífilis, para disminuir la cantidad de personas que viven con estas enfermedades.

##   
¿Cómo actúa?

El virus de la inmunodeficiencia humana (VIH) ataca el sistema inmunitario y debilita la defensa contra muchas infecciones y contra determinados tipos de cáncer. A medida que el virus destruye las células inmunitarias e impide el normal funcionamiento de la inmunidad, la persona infectada va cayendo gradualmente en una situación de inmunodeficiencia.

## ¿Cómo se contagia?

Se transmite a través del intercambio de determinados líquidos corporales de la persona infectada, como la sangre, la leche materna, el semen o las secreciones vaginales. También se puede transmitir de la madre al hijo durante el embarazo y el parto. No es posible infectarse en los contactos ordinarios cotidianos como los besos, abrazos o apretones de manos o por el hecho de compartir objetos personales, agua o alimentos.

## ¿Tiene cura?

EL VIH continúa siendo uno de los mayores problemas para la salud pública mundial, se ha cobrado ya casi 33 millones de vidas. Sin embargo, habida cuenta del acceso creciente a la prevención, el diagnóstico, el tratamiento y la atención eficaces del VIH, incluidas las infecciones oportunistas, la infección por el VIH se ha convertido en un problema de salud crónico llevadero que permite a las personas que viven con el virus llevar una vida larga y saludable.

## Obras sociales sin cobertura en pandemia

En este momento, la provincia brinda tratamiento a más de 4 mil personas en los hospitales públicos. Pero durante la pandemia "tuvimos que agregar 150 personas que tienen obra social, pero por la cuarentena no les daban la medicación: en total sumamos entre 700 y 800 tratamientos", cuenta el coordinador, quien aspira a que "ahora, que están todas las oficinas de las obras sociales abiertas, comiencen a entregar la medicación", aporta el Coordinador del Programa Provincial de VIH, Gerardo Perafan.