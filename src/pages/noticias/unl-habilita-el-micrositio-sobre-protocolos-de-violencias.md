---
category: Agenda Ciudadana
date: 2021-06-04T08:18:48-03:00
thumbnail: https://d7kqr73uznmf6.cloudfront.net/2020-10-07.jpg
layout: Noticia con video
link_del_video: https://youtu.be/aRYAC1eVm80
author: Prensa UNL
resumen: UNL habilita el micrositio sobre protocolos de violencias
title: UNL habilita el micrositio sobre protocolos de violencias
entradilla: 'Esta plataforma inclusiva brinda asesoramiento, canales directos de comunicación,
  información y contención en torno a los protocolos para el abordaje y acompañamiento
  ante situaciones de violencias. '

---
Con el objetivo de garantizar que la Universidad Nacional del Litoral sea un ambiente libre de cualquier tipo de hostigamiento, discriminación y violencias por razones de identidad de género, orientación sexual, razones laborales, académicas, clase, raza, etnia, nacionalidad o religión, la UNL presenta una plataforma virtual que hace eje en los protocolos para el abordaje, cuidado y acompañamiento integral ante situaciones de violencias en la institución.

Habilitado este 3 de Junio, en un nuevo aniversario del Ni una Menos, este espacio web -www.unl.edu.ar/protocolodeviolencias- se presenta como una herramienta accesible, inclusiva y dinámica que apunta a brindar asesoramiento, información y contención a toda la Comunidad UNL. Allí se puede acceder a canales directos de comunicación, preguntas frecuentes y, entre otras cosas, todos los documentos oficiales.

La iniciativa, impulsada por la UNL y la Federación Universitaria del Litoral (FUL), responde al compromiso institucional de construir una Universidad libre de todo tipo de violencias y, además, es una nueva acción vinculada al cumplimiento de lo establecido en las resoluciones oficiales de los protocolos. En este sentido, se aseguró que “la UNL promueve el desarrollo integral de toda su comunidad universitaria fomentando el diálogo y la vinculación interpersonal desde una perspectiva del cuidado, el respeto y la no discriminación”.

**Herramienta digital**

La plataforma apunta a brindar un ámbito de contención y confianza para que las personas afectadas puedan consultar o denunciar su situación, previniendo y garantizando un ambiente libre de cualquier tipo de acción violenta, discriminatoria y/o de acoso en los espacios físicos de la Universidad y sus dependencias o anexos y a través de medios telefónicos, virtuales o de otro tipo.

El micrositio, alojado en la web institucional de la Universidad, es impulsado por el Programa Social de la Secretaría de Bienestar, Salud y Calidad de Vida, el Programa Género, Sociedad y Universidad y la FUL. Además, contó con el diseño y la coordinación de la Dirección de Comunicación Institucional.

El recorrido por el mismo permite acercar la información relativa a las políticas institucionales para prevenir y abordar las violencias, con especial referencia a las violencias de género y contra la identidad o diversidad sexual y establece con claridad los canales de comunicación con el equipo responsable. Además, cuenta con información clara y precisa sobre cómo realizar una denuncia y una serie de preguntas frecuentes para despejar dudas.

**Consultas y denuncias**

Estudiantes, docentes, personal nodocente, investigadores o investigadoras ante actos de violencias, discriminación, acoso u hostigamiento, en cualquiera de sus modalidades y por cualquier motivo, pueden solicitar el asesoramiento, acompañamiento e intervención de un equipo de profesionales. Se garantiza la confidencialidad, voluntariedad y no revictimización a través de un abordaje integral e interdisciplinario de cada consulta.

A través de la web www.unl.edu.ar/protocolodeviolencias se puede chatear con el equipo de profesionales, realizar consultas y coordinar entrevistas personales, con el Programa Social de la Secretaría de Bienestar, Salud y Calidad de Vida de la UNL.

Los medios de comunicación, a los que se accede de manera directa mediante la plataforma, son:

* Correo electrónico: protocolodeviolencias@unl.edu.ar
* Formulario online: www.unl.edu.ar/protocolodeviolencias
* Teléfono -llamadas y/o Whatsapp): +54 9 3424 05-9660.