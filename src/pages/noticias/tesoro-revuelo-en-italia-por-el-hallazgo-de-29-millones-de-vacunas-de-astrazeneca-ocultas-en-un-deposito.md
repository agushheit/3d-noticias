---
category: Agenda Ciudadana
date: 2021-03-25T06:49:17-03:00
thumbnail: https://assets.3dnoticias.com.ar/vacunas01.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: La Nación
resumen: "“Tesoro”: revuelo en Italia por el hallazgo de 29 millones de vacunas de
  AstraZeneca ocultas en un depósito"
title: "“Tesoro”: revuelo en Italia por el hallazgo de 29 millones de vacunas de AstraZeneca
  ocultas en un depósito"
entradilla: Las dosis estaban listas para partir hacia Gran Bretaña, lo que desató
  una nueva tormenta porque la farmacéutica no cumplió con los plazos de entrega a
  la UE

---
_ROMA_.- En medio de un nuevo confinamiento en casi toda Italia, ayer más de 500 muertos en 24 horas, una campaña de vacunación demorada por escasez de fármacos y cuestionamientos por la desorganización, la noticia del hallazgo de 29 millones de dosis de la vacuna de AstraZeneca ocultas en un depósito de la empresa Catalent, en Anagni, cerca de esta capital, “listas para partir hacia Gran Bretaña”, provocó hoy una nueva tormenta.

Fue el diario La Stampa quien reveló la existencia de este “tesoro” de “29 millones de dosis” en una fábrica de envasado de este producto de Anagni –localidad a unos 60 kilómetros al sureste de Roma–, gracias a una investigación de la Comisión Europea. De confirmarse se tensarían aún más las ya difíciles relaciones entre la casa farmacéutica anglo-sueca, Londres y la Unión Europea (UE): en efecto, AstraZeneca había prometido a la UE 120 millones de dosis en los primeros tres meses del año, algo que no cumplió, ya que sólo entregó 16,6 millones (la mitad de las que están ocultas).

**EL PEOR MOMENTO: LA CRISIS SANITARIA EN AMÉRICA DEL SUR EN PLENO RÉCORD DE CASOS**

Las maniobras de AstraZeneca fueron descubiertas en una visita realizada por el comisario francés Thierry Breton al establecimiento de la ciudad de Leida, en Holanda, que gestiona la empresa Halix. Este, junto a otro establecimiento en Bélgica, es uno de los dos que utiliza AstraZeneca para producir el fármaco en territorio de la UE. El problema es que no cuenta aún con la autorización de parte de la Agencia Europea del Fármaco (EMA). Y sin el vía libre de la EMA las dosis no pueden ser entregadas a los países miembros del bloque. Pero esto no vale para Gran Bretaña –que ya está afuera tras el Brexit– que, de hecho, ya en los últimos tres meses del año pasado habría importado justamente esas vacunas producidas en el establecimiento holandés.

Siempre según La Stampa, este flujo se bloqueó el 1 de febrero, cuando entró en vigor el reglamento de la UE para el control de las exportaciones. Pero la cadena de producción del establecimiento nunca se detuvo, ya que Halix tiene la capacidad de producir al menos 5-6 millones de dosis por mes. ¿Dónde habían ido a parar entonces esas dosis?

Fue para resolver este misterio que el comisario francés señaló la situación a las autoridades italianas, que enseguida efectuaron una inspección en el establecimiento de la empresa Catalent de Anagni, utilizada por AstraZeneca para el “fill&finish”, es decir, el envase.

“El primer informe enviado a Bruselas dice que en los frigoríficos de los depósitos de este sitio del Lacio hay 29 millones de dosis de la vacuna”, reveló La Stampa, que destacó que estaban listas para partir hacia Gran Bretaña y no hacia países de la UE, pese a los retrasos notables en los suministros del coloso farmacéutico anglo-sueco.

Al parecer la EMA todavía no le dio el visto bueno al establecimiento de Halix porque AstraZeneca no entregó la información necesaria. El vía libre se espera para mañana. Aunque en Bruselas sospechan de que el retraso en la entrega de datos sea el fruto de un táctica de la empresa para garantizarle Gran Bretaña una vía preferencial en el suministro.

> Explanations needed urgently! #AstraZeneca is stocking tens of millions of doses while not complying with their European contract. This is unacceptable. The urgency is too great. We should categorically refuse any export of Astra Zeneca made in Europe. https://t.co/UYBISrTIZ1
>
> — Manfred Weber (@ManfredWeber) March 24, 2021

La primera reacción a la investigación de La Stampa llegó de parte de Manfred Weber, presidente del grupo Popular Europea en el Parlamento UE: “¡Hacen falta explicaciones necesarias y con urgencia! AstraZeneca está guardando decenas de miles de dosis sin respetar el contrato con Europa. Esto es inaceptable. La urgencia es enorme. Deberíamos rechazar categóricamente cualquier exportación de AstraZeneca producida en Europa”, denunció, en un tuit.

**AJUSTE EN EL VATICANO: EL PAPA LES BAJÓ EL SUELDO A CARDENALES, ALTOS CARGOS Y SACERDOTES**

En medio del escándalo, pasado el mediodía, el primer ministro, Mario Draghi, expresidente del Banco Central Europeo (BCE), al frente de un ejecutivo de unidad nacional, intentó calmar las aguas. Y dio su versión de este nuevo thriller, digno de película. “El sábado, la Comisión Europea le pidió al primer ministro poder verificar algunos lotes de vacunas en un establecimiento de producción de Anagni (Roma). El primer ministro informó al ministro de Salud, Roberto Speranza, que envió una inspección, que tuvo lugar entre el sábado y el domingo”, indicó Draghi en un comunicado de Palazzo Chigi, sede del gobierno, que precisó que fueron carabineros especiales quienes hicieron la inspección.

“Los lotes inspeccionados resultaron con destino a Bélgica”, aseguró también el gobierno, que subrayó, además, que “todos los lotes en salida suelen ser controlados”.

Poco antes de las 16 (hora local), un comunicado de AstraZeneca también trató de apagar el incendio. “Ante las noticias de las 29 millones de dosis halladas en Italia, 13 millones están a la espera de controles de calidad antes de ser enviadas a países Covax (es decir, pobres); y las 16 millones de dosis restante para ser enviadas a Euorpa”, aseguró. “Es incorrecto describir esto como un acopio: el proceso de fabricación de vacunas es muy complejo y requiere tiempo”, afirmó, al destacar que “especialmente las dosis de vacunas deben esperar los controles de calidad”.