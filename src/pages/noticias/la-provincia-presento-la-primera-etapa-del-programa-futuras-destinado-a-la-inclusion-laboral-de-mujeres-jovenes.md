---
category: Estado Real
date: 2021-10-16T06:00:01-03:00
thumbnail: https://assets.3dnoticias.com.ar/FUTURA.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La provincia presentó la primera etapa del programa futuras destinado a la
  inclusión laboral de mujeres jóvenes
title: La provincia presentó la primera etapa del programa futuras destinado a la
  inclusión laboral de mujeres jóvenes
entradilla: Articula instancias de capacitaciones, prácticas laborales e incorporación
  efectiva a puestos de trabajo a partir de estímulos económicos a las participantes
  y a empresas.

---
La provincia de Santa Fe, a través del Ministerio de Trabajo, Empleo y Seguridad Social, puso en marcha el Programa Futuras, que tiene como meta promover y facilitar la inserción laboral de mujeres entre 18 y 30 años de las áreas urbanas y periurbanas de las principales ciudades de la provincia, que hayan culminado el último año de la escuela secundaria.

Futuras constituye una línea de gestión específica para mejorar las condiciones de empleabilidad de mujeres jóvenes, de escasos recursos económicos, a través de proyectos de capacitación y prácticas laborales e incentivos económicos a las empresas que incrementen su dotación con las beneficiarias del programa.

En ese sentido, el ministro de Trabajo, Juan Manuel Pusineri, destacó que “hoy tenemos el desafío de avanzar en la recuperación económica y del empleo para las y los santafesinos. La pandemia no tuvo el mismo impacto para todos: se profundizaron las desigualdades sociales, especialmente en lo que se refiere al acceso al mercado laboral de mujeres jóvenes. El Programa Futuras es una de las respuestas que estamos implementando para revertir esta situación puntual”.

Por su parte, la directora Provincial de Promoción del Empleo Digno, Fernanda Medina, presentó la implementación de los primeros cursos en los rubros textil y gastronomía donde participarán 80 jóvenes. Asimismo, Medina explicó la mecánica del Programa: durante los tres meses que abarca el trayecto formativo, el gobierno realiza un aporte del 30% del salario mínimo vital y móvil a cada una de las participantes. En la fase siguiente de prácticas laborales el aporte aumenta al 60%. En la última etapa, que tiene que ver con la inserción laboral, el estímulo económico del Estado se dirige hacia el sector privado. Por ello, la funcionaria agregó que “estamos generando los vínculos necesarios con pequeñas y medianas empresas para que las participantes sean incorporadas efectivamente como trabajadoras en el menor tiempo posible y así fortalecer el tejido productivo local, además de reparar desigualdades históricas”.

En el acto, realizado en el Distrito Noroeste de la ciudad de Rosario, también estuvieron presentes la coordinadora del Centro de Capacitación de la Cooperativa “Mujeres Solidarias”, Susana Olivé; la directora de la Regional Rosario del MTEySS, Rita Colli; la directora del Programa Santa Fe Más, Julia Irigoitia; y el director Regional de la zona centro del Ministerio de Trabajo, Empleo y Seguridad Social de la Nación, Cristian Recchio.