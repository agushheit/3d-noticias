---
category: Agenda Ciudadana
date: 2021-08-19T06:15:27-03:00
thumbnail: https://assets.3dnoticias.com.ar/GREMIOS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Educación convocó a los 5 gremios docentes a la segunda ronda paritaria
title: Educación convocó a los 5 gremios docentes a la segunda ronda paritaria
entradilla: El encuentro se realizará este jueves a partir de las 12.30 en el Palacio
  Pizzurno, y participarán además de los dirigentes sindicales los integrantes del
  Consejo Federal de Educación.

---
El Ministerio de Educación convocó para este jueves a los cinco gremios docentes con representación nacional (Ctera, Sadop, AMET, CEA y UDA) a reanudar la paritaria federal del sector para discutir un aumento salarial y otras condiciones laborales y de ingresos.

Sindicalistas y funcionarios habían pasado a un cuarto intermedio en la inauguración de las negociaciones para analizar la propuesta oficial y la contraoferta gremial.

El ministro de Educación, Nicolás Trotta, había señalado la importancia de la apertura del diálogo para monitorear el acuerdo firmado este año y analizar las condiciones de trabajo de los docentes de forma previa al encuentro de noviembre próximo, como lo había determinado el decreto que reinstitucionalizó la negociación convencional.

Trotta había señalado de forma previa a ese cuarto intermedio que este mes se percibió el aumento de julio sobre el salario mínimo docente garantizado y, en septiembre, se incrementará otra vez, al igual que las sumas por Fondo Nacional de Incentivo Docente (Fonid) y material didáctico, más allá de lo cual "es necesario una reapertura paritaria y el diálogo sobre los términos del acuerdo alcanzado en febrero" último, puntualizó.

Las partes también analizarán este jueves, además del incremento de los haberes, la posibilidad de sellar un convenio colectivo de trabajo sectorial y las cuatro iniciativas legislativas que impulsa el Gobierno -Justicia educativa, Formación Docente, Tecnología para la educación y Educación superior-, para lo cual Trotta propuso una mesa de trabajo.

**Qué demandas llevan los gremios**

La Confederación de Trabajadores de la Educación (Ctera), que lidera Sonia Alesso, ratificó esta mañana la necesidad de incrementar los salarios entre un 45 y 50 por ciento, y sostuvo que hasta ahora "no hubo una propuesta de mejora concreta".

Ayer, en su visita a Santiago del Estero, Trotta señaló su expectativa para acordar con los gremios docentes "una nueva paritaria" y remarcó que de prosperar esa instancia será "la séptima" negociación firmada entre el Estado nacional y los cinco sindicatos.

La Unión Docentes Argentinos (UDA) que lidera el también secretario de Políticas Educativas de la CGT, Sergio Romero, ratificará el reclamo sobre un convenio colectivo sectorial, la necesidad de incrementar el ingreso inicial y el de los jubilados.

Los sindicatos reclaman un inmediato aumento del salario inicial y demandan "un esfuerzo para que miles de docentes emerjan de la línea de pobreza", dijo Romero, quien ratificó que existe "un marcado atraso respecto de los salarios ante el avance de los precios y la pérdida de poder adquisitivo", y que "la gran preocupación es que en todas las provincias los haberes iniciales se ubican por debajo de la línea de pobreza".

El ingreso inicial de un docente es el que se determina en la paritaria nacional y, luego, cada provincia puede mejorarlo si está en condiciones de reabrir la discusión.

En la última paritaria federal, los cinco gremios docentes con representación nacional y el Gobierno habían acordado un aumento del 34,6 por ciento en tres tramos.

El salario inicial docente era de 27.500 pesos y, luego de ese acuerdo paritario, pasó a 31 mil pesos en marzo, a 34.500 en julio y a 37 mil en septiembre, en tanto el Fondo de Incentivo Docente (Fonid) se incrementó un 45 por ciento desde marzo último.