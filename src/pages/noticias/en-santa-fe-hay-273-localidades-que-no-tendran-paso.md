---
category: Agenda Ciudadana
date: 2021-08-18T06:15:29-03:00
thumbnail: https://assets.3dnoticias.com.ar/paso.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: En Santa Fe hay 273 localidades que no tendrán Paso
title: En Santa Fe hay 273 localidades que no tendrán Paso
entradilla: 'En esas localidades se presentaron listas únicas por los frentes o partidos
  que compiten y ya fueron oficializadas para las elecciones generales. '

---
En el Tribunal Electoral de la provincia de Santa Fe se comenzaron a confeccionar durante el fin de semana las 6.789 urnas que serán utilizadas en las elecciones Paso del 12 de septiembre. Además, ya oficializó para las elecciones generales a las listas que compiten en 273 localidades de la provincia que no tienen internas.

Cumpliendo con una de las principales disposiciones de la Acordada 83/21 de la Cámara Electoral Nacional para los comicios y para evitar aglomeración de electores/as, además de las medidas sanitarias correspondientes, sumarán 1.032 locales de votación en toda la provincia, siendo únicamente 66 los que contarán con más de ocho mesas de votación.

Asimismo, se va a considerar un horario de votación prioritario de 10.30 a 12.30, para personas con factores de riesgo de Covid-19. Si bien durante esa franja horaria no se impedirá la votación de quienes concurran a sufragar sin integrar al grupo priorizado, tales electores/as –que no lo integran-deberán ceder su turno a quienes acrediten por los medios pertinentes integrar los grupos de riesgo priorizados.

En lo que refiere a la exclusión de listas únicas para las Paso en Santa Fe, en el Auto 2105, hace referencia a la Ley N° 12.367 (t.o. por Ley 12.966/09) que en su artículo 2 dispone que "...cuando la elección general a cargos públicos electivos refiera exclusivamente a cargos municipales y comunales y se verifique en uno o más municipios o comunas que todos los partidos políticos, confederación de partidos políticos o alianzas electorales participantes de la elección, hayan presentado una única lista de candidatos, cada uno de ellos, no se realizará el acto comicial de elección de precandidatos. En tales casos el Tribunal Electoral de la Provincia, previa verificación de los requisitos establecidos en la legislación vigente, oficializará las listas proclamando candidatos a quienes las integren, para la elección general...".

En el departamento La Capital las localidades donde no habrá Paso son: Candioti, Arroyo Aguiar, Campo Andino, Llambi Campbell, Emilia, Cabal y Arroyo Leyes.