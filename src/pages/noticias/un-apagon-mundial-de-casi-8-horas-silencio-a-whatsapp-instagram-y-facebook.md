---
category: Agenda Ciudadana
date: 2021-10-05T06:00:56-03:00
thumbnail: https://assets.3dnoticias.com.ar/secayo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Un apagón mundial de casi 8 horas silenció a WhatsApp, Instagram y Facebook
title: Un apagón mundial de casi 8 horas silenció a WhatsApp, Instagram y Facebook
entradilla: Las aplicaciones dejaron de funcionar al mediodía. Las causas de la falla
  global aún no habían sido esclarecidas por la compañía de Mark Zuckerberg.

---
WhatsApp, Instagram,Facebook y Facebook Messengersufrieron este lunes una caída global de varias horas que forzó a millones de usuarios a buscar alternativas para poder establecer sus comunicaciones habituales en los ámbitos familiar, laboral y social.  
  
Las aplicaciones salieron de servicio a las 12.15 (hora de Argentina). Pasadas las 19 había vuelto a funcionar Instagram, mientras algunos usuarios de la Argentina comenzaron a recibir cerca de las 19.30 mensajes a través de WhatsApp y las páginas de Facebook parecían restablecerse, aunque sin que la empresa hubiera informado acerca de la normalización plena del servicio.  
Un portavoz de Facebook en Argentina confirmó temprano que se estaban experimentando problemas en las aplicaciones y que los técnicos estaban trabajando para normalizar el servicio, sin que se difundiera información segura sobre los motivos del apagón.  
  
"Sabemos que las personas están experimentando problemas para acceder a nuestras aplicaciones y productos. Estamos trabajando para normalizar el servicio lo más rápido posible y sentimos los inconvenientes que esto puede causar", aseguró un portavoz de Facebook a través de la vocería de prensa en Argentina.  
  
La caída de estas redes sociales llevó a los usuarios a buscar alternativas en otros servidores, lo que hizo colapsar a algunos sitios por exceso de tráfico.  
  
Las causas de la falla global aún no habían sido esclarecidas por las compañías de Mark Zuckerberg.  
  
Según los datos de Downdetector, Whatsapp acumuló de inmediato más 20.100 mensajes de usuarios denunciando la caída del servicio, de los cuales el 39% notificaba problemas en el envío de mensajes, el 32% en la aplicación y el 29% en el sitio web de la red social.  
  
En cuanto a Instagram, se registraron 3.763 reportes de mal funcionamiento, de los cuales el 35% informaba de fallas en el sitio web, el 33% mala conexión de servidor y el 32% problemas en la aplicación.  
  
Por último, 3.507 usuarios locales reportaron problemas con la aplicación Facebook, específicamente con el sitio web, en un 68%.  
  
En dialogo con la prensa, el especialista en seguridad informática Christian Borhello detalló que "generalmente son tres los problemas que pueden suceder cuando hay cortes en el servicio: ataques de denegación (mucho tráfico), un fallo interno por infraestructura o la caída de los DNS (Domain name service).  
  
Según el responsable de Segurinfo, lo más probable es que haya sido esta última lo que dañó temporalmente el servicio que imposibilitó a los usuarios a acceder a las aplicaciones de redes sociales.  
  
"Los DNS pueden fallar o no dependiendo de la infraestructura con la que cuenta la empresa y esta relacionado de forma directa con la configuación interna, que repercute en el funcionamiento del servicio", explicó.  
  
Además, comunicó que "no es un problema que tarda mucho en resolverse, pero que al ser en todo el mundo puede ser que se vaya arreglando por zonas".  
  
**Antecedentes**

Si bien WhatsApp e Instagram habían sufrido una caída en marzo y julio de este año, la última vez que se registraron fallas en los tres servicios en simultáneo fue en julio de 2020, según el portal Adsl Zone.  
  
Sin embargo, la caída más grande de la historia de la aplicación WhatsApp fue la registrada el 13 y 14 de marzo de 2019 y tuvo una duración de al menos 14 horas, que también afectó a las otras tres aplicaciones de la empresa.  
  
A pesar de que las primeras especulaciones sobre la causa de la caída apuntaban a un ciberataque, rápidamente la compañía informó que se trataba de "una falla técnica en la configuración de sus servidores", consignó el portal especializado Andina Link.  
  
En ese entonces, Facebook contaba con 2.300 millones de usuarios, WhatsApp más de 1.500 millones e Instagram alrededor de 1.000 millones. Actualmente, según el sitio Statista, Facebook acumuló más de 2.700 y 1.300 millones en Facebook Messenger, Instagram sumó unos 200 millones más, mientras que WhatsApp alcanzó los 2.000 millones de usuarios activos.  
  
En Twitter, la red social a la que "vuelven" los usuarios cuando las demás fallan, estallaron las quejas por la "mala calidad" del servicio, muchas de las cuales alentaron también ​el reemplazo de WhatsApp por Telegram, una aplicación de mensajería que ya cuenta con 500 millones de usuarios activos y que "no ha tenido apenas caídas en los últimos años", detalló Adsl Zone.  
Denuncia contra Facebook

Por otra parte, medios internacionales dieron a conocer que la interrupción de los servicios se produjo un día después de que Frances Haugen, una exempleada de la compañía, apareciera en la televisión estadounidense para revelar su identidad tras filtrar documentos a las autoridades alegando que Facebook sabía que sus productos estaban alimentando el odio y dañando la salud mental de los niños, consignó la agencia AFP.  
Haugen, una experta en datos de 37 años, trabajó para empresas como Google y Pinterest pero dijo que Facebook era "sustancialmente peor" de que lo que había visto antes.  
  
A partir de estas declaraciones, la empresa Facebook quedó envuelta en una tormenta y el diario The Wall Street Journal detalló cómo la compañía sabía que sus productos, incluido Instagram, estaban dañando a las niñas, especialmente en lo que respecta a la imagen corporal, consignó AFP.  
  
Hasta el momento Facebook no se expresó sobre lo dicho en la entrevista a Haugen.