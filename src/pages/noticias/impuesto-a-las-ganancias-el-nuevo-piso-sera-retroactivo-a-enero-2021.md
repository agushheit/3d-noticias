---
category: Agenda Ciudadana
date: 2021-03-09T06:09:12-03:00
thumbnail: https://assets.3dnoticias.com.ar/ganancias.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: 'Impuesto a las Ganancias: el nuevo piso será retroactivo a enero 2021'
title: 'Impuesto a las Ganancias: el nuevo piso será retroactivo a enero 2021'
entradilla: Hubo una serie de modificaciones en el proyecto que se tratará el martes
  en Diputados. Se estableció que en abril se recuperarán las cargas abonadas en enero,
  febrero y marzo y el aguinaldo quedará exento.

---
El proyecto para subir el piso salarial a partir del cual se paga Ganancias y que lo fija en 150.000 pesos brutos se aplicará de modo retroactivo a enero de 2021, lo que implica que quienes hayan tenido descuentos por ese impuesto en enero, febrero y marzo recibirán un reintegro en el mes de abril.

Así lo confirmaron en las últimas horas a Télam desde el equipo técnico del presidente de la Cámara de Diputados, Sergio Massa.

La decisión fue acordada entre el propio Massa, el Ministerio de Economía y la Afip, detallaron esas fuentes.

Además, desde la Cámara baja informaron que el medio aguinaldo (Salario Anual Complementario) que se paga en junio y diciembre, en el caso de los asalariados con remuneraciones de hasta 150.000 pesos brutos, quedará expresamente exento del tributo

"Los mencionados beneficios, que fueron incorporados teniendo en cuenta pedidos de la CGT y legisladores tras mantener reuniones con el presidente de la Cámara, ya han sido validados con Ministerio de Economía y Afip", transmitieron desde la Presidencia de Diputados a través de un comunicado.

La devolución de los descuentos por Ganancias de enero, febrero y marzo sumará un beneficio adicional para los 1.267.000 trabajadores y jubilados que dejarán de pagar el tributo una vez que se apruebe la ley.

Para hacer una proyección (aunque en cada caso dependerá de la estructura familiar del trabajador y si descuenta de Ganancias ítems como alquiler y otros), un asalariado soltero y sin hijos con salario de 110.000 pesos en bruto (91.300 pesos netos) cobrará el mes próximo un reintegro de 4.974 pesos.

La exención de Ganancias para el medio aguinaldo fue uno de los cambios que los gremialistas propusieron a Massa, incluso es uno de los puntos de un proyecto propio que presentó el diputado Facundo Moyano (FdT-Buenos Aires).

Con la aplicación retroactiva y que el SAC quede exento del tributo, estiman cerca de Massa, el Estado promoverá un impacto positivo en los bolsillos de trabajadores y jubilados de alrededor de 10.000 millones de pesos.

Esa inyección de fondos, aseguran, "se volcará de manera directa al consumo".

"El Presidente pidió en la Asamblea acelerar el tratamiento de Ganancias porque para el Gobierno la recuperación del ingreso es una prioridad", aseguró Massa en declaraciones a Télam.

El proyecto de reforma de Ganancias modifica el artículo 30 de la Ley 20.628 (texto ordenado en 2019) para elevar el monto mínimo no imponible.

Con esa modificación, los trabajadores que cobren hasta 150.000 pesos y los jubilados que perciben hasta ocho jubilaciones mínimas ya no pagarán el impuesto.

"Este alivio fiscal que se está impulsando para trabajadores y jubilados, que implicará un esfuerzo presupuestario para el Estado, se va a compensar por la mayor recaudación que generará el consumo adicional", evaluaron desde el equipo de asesores del titular de la Cámara.

"La suba del mínimo no imponible, más la retroactividad a enero y la exención del medio aguinaldo, impactarán de manera positiva en el bolsillo de trabajadores y jubilados", completaron.