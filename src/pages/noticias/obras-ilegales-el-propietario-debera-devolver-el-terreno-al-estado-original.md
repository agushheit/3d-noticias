---
category: La Ciudad
date: 2021-02-11T07:17:47-03:00
thumbnail: https://assets.3dnoticias.com.ar/obras.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Obras ilegales: el propietario deberá devolver el terreno al estado original'
title: 'Obras ilegales: el propietario deberá devolver el terreno al estado original'
entradilla: Desde el Gobierno provincial intimarán al dueño del predio y exigirán
  que restablezca las condiciones hídricas y ambientales del lugar.

---
El viernes pasado la Municipalidad clausuró obras ilegales que estaban en ejecución a la vera del riacho Santa Fe. Ahora, el gobierno provincial intimará al dueño del terreno para que el lugar vuelva a su estado original tanto en condiciones hídricas como ambientales.

![](https://assets.3dnoticias.com.ar/obras2.jpeg)

Además de no tener habilitación para construir, **el terreno está a menos de 100 metros de la línea rivera y es un área protegida por tratarse de bosques nativos. Por tanto, viola ordenanzas municipales y leyes provinciales.**

No obstante, los trabajos que se realizaron implicaron movimiento de suelo,  tala de árboles con más de 20 años de antigüedad y construcción de un terraplén al costado de la ruta nacional 168, cerca del acceso a Alto Verde.

> Quienes llevaban adelante la obra **no pudieron explicar cuál era el objetivo de los trabajos y reconocieron que no tenían ningún permiso municipal o provincial.** Por eso, desde la Municipalidad procedieron a notificar al dueño y al apoderado del terreno sobre las infracciones que estaban cometiendo y obligarlos a parar la construcción.

![](https://assets.3dnoticias.com.ar/obras3.jpeg)

Al respecto, Edgardo Seguro, secretario de Ambiente de la Municipalidad explicó que aunque no se trata de tierras fiscales y el terreno tenga un propietario “uno no puede hacer lo que quiera sino lo que está permitido”. En este sentido, ilustró que se podría hacer una “canchita” pero no una obra y menos una que cambie el curso del agua y evitar que se inunde “porque forma parte de un humedal y está en la línea de rivera justamente protegiendo al bosque nativo”.

Finalmente, Roberto Gioria, secretario de Recursos Hídricos de la provincia, aseguró que desde Ambiente establecerán criterios para que se planten una cierta cantidad de ejemplares, ya que es imposible restablecer los originales.

En cuanto al movimiento del suelo en una zona de defensa, Gioria manifestó que se le darán pautas al propietario del para la compactación y sobre la pendiente que debe haber en el predio.

![](https://assets.3dnoticias.com.ar/obras1.jpeg)

Respecto a las excavaciones que se realizaron debajo del puente, dieron intervención a Vialidad para que examine si se afectó la estructura, pero adelantó que seguramente se deba realizar una compactación especial para evitar problemas de erosión.

El funcionario también aclaró que el propietario del terreno todavía no recibió la notificación oficial de la intimación.

Mientras que además de la medida provincial, desde la Municipalidad se podría imponer una sanción económica.