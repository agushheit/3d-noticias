---
category: Agenda Ciudadana
date: 2021-03-10T06:55:21-03:00
thumbnail: https://assets.3dnoticias.com.ar/hartos.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de El Litoral
resumen: Hartos de los robos, vecinos harán un cacerolazo contra la inseguridad
title: Hartos de los robos, vecinos harán un cacerolazo contra la inseguridad
entradilla: 'Será el próximo viernes a las 20. La Red de Vecinales por la Seguridad
  presentó dos petitorios, uno a la cartera de Seguridad y otro al municipio: aseguran
  que faltan luminarias.'

---
Hartos de los robos, los vecinos y las vecinas de Fomento 9 de Julio decidieron en asamblea realizada días atrás llevar adelante un cacerolazo el próximo viernes a la hora 20, en Pedro Ferré y Facundo Zuviría. El objetivo es claro: que la manifestación sirva para mostrar la situación de inseguridad que afecta en ese barrio de la ciudad y en otros: hoy por hoy, el delito ya no distingue zonas geográficas.

Este martes por la tarde fue la primera reunión del año de la Red de Vecinales por la Seguridad de la ciudad, en la sede de Villa Setúbal. El temario, amplio y abierto, estuvo también caldeado por ese mismo hartazgo que genera el delito de cada día. “Durante los últimos 20 días, en Fomento 9 de Julio la gente la pasó muy mal: todo de terror”, advirtió en diálogo con El Litoral Susana Spizzamiglio, presidenta de esa vecinal.

La referente aseguró que desde la vecinal se registraron entre 2 y 3 robos diarios (en las últimas tres semanas), por vía de arrebatos en la calle, motochorros y en cualquier otra modalidad delictiva. “Llegaron a notificarse hasta siete robos en 24 horas; si bien la Seccional 11a. nos notificó 15 robos en todo febrero, estamos seguros de que fueron muchos más”, dijo.

 Otro punto que marcó la vecinalista es que si bien el personal policial de esa seccional realizó controles y patrullajes, “los hicieron en zonas donde no ocurren hechos delictivos. Nosotros tenemos localizados los sectores donde ocurren los robos, las ‘zonas calientes’ de la jurisdicción, y se las indicamos… Pero van a patrullar en otras áreas. Es como si se te está quemando la casa y los bomberos van a apagar el incendio a 10 cuadras de distancia. No se comprende”, se quejó.

Debido a la cantidad de episodios delictivos, en la asamblea vecinal se elaboraron dos petitorios, uno remitido al Ministerio de Seguridad y otro al municipio. Se pidieron controles de autos y motos, “y particularmente a la Municipalidad que atienda la reparación de luminarias: tenemos un 25-30% de déficit de iluminación, luces que no funcionan. Hace un mes y medio que en 4 de enero al 4900 los vecinos no tienen luz. En Saavedra y Pje. Cullen, también están a oscuras desde hace bastante tiempo”, advirtió.

La semana pasada hubo una reunión en Rosario. Allí se generó una mesa de consensos con la Mesa de Vecinales Unidas de esa ciudad, de recién creación, y en la misma instancia participaron representantes de Villa Gob. Gálvez: “Lo bueno es que la experiencia de la Red de Vecinales de nuestra ciudad se está replicando en el sur provincial. Eso nos da más noción de trabajo conjunto”, valoró Spizzamiglio.

![](https://assets.3dnoticias.com.ar/cacerolazo.jpg)