---
category: Agenda Ciudadana
date: 2022-11-16T11:20:23-03:00
thumbnail: https://assets.3dnoticias.com.ar/IMG_20220928_092227.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Redacción 3Dnoticias
resumen: Corte de servicio de las lineas 1.2.3.9 y 15 por asambleas.
title: Corte de servicio de las lineas 1.2.3.9 y 15 por asambleas.
entradilla: Las líneas pertenecientes a la empresa ERSA , desde las 10 han comenzado
  a cortar el servicio por asamblea de UTA

---
Desde las 10, los colectivos de las líneas 1, 2, 3, 9 y 15, todas de la empresa ERSA, comenzaron a llegar a sus respectivas paradas y no retomaran el servicio por la realización de asambleas, por distintos reclamos, según informaron fuentes de UTA. 

Los reclamos : 

* descuentos por días pedidos por enfermedad
* inseguridad en distintos lugares de la ciudad
* pésimo estado de los coches y frecuencias.

Se prevé dos hora como mínimo sin servicio de las líneas mencionadas. 