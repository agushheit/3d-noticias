---
category: Agenda Ciudadana
date: 2021-12-15T06:15:39-03:00
thumbnail: https://assets.3dnoticias.com.ar/inflacion.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: La inflación de noviembre fue del 2,5% y acumula 45,4% en lo que va del año
title: La inflación de noviembre fue del 2,5% y acumula 45,4% en lo que va del año
entradilla: 'El aumento interanual de noviembre trepó hasta un 51,2%. La estimación
  del gobierno para la inflación anual había sido de 29 puntos, 16 por debajo de la
  registrada a falta de diciembre.

'

---
El índice de Precios Minoristas aumentó durante noviembre 2,5%, informó hoy el Instituto Nacional de Estadística y Censos (Indec). En cuanto a la inflación en la categoría de variación interanual, comparando noviembre de 2020 respecto a noviembre 2021 los precios aumentaron un 51,2 %.

Si bien el número se mantuvo por debajo del 3,5% que había registrado tanto en septiembre como en octubre, el incremento anual acumulado llegó al 45,4%, posicionándose 16 puntos por encima de la estimación que había hecho Martín Guzmán en el Ministerio de Economía.

Alimentos y bebidas no alcohólicas aumentó 2,1% mensual versus 3,4% en octubre, con desaceleración de casi todos los rubros, en la que influyó el programa de Precios Cuidados. Los productos Estacionales, aumentaron solo 0,5% mensual, frente al 8,1% en octubre, con fuerte caída en los precios de las Verduras.

También redujo su tasa de inflación los Regulados hasta 1 % mensual, ante el 1,9% de octubre, con subas en electricidad, y en transporte público en el interior del país, mientras que “la inflación Núcleo” fue de 3,3% mensual.