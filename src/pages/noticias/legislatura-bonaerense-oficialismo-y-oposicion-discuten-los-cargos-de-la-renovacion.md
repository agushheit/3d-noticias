---
category: Agenda Ciudadana
date: 2021-12-06T06:15:17-03:00
thumbnail: https://assets.3dnoticias.com.ar/LEGISLAPORTEÑA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: 'Legislatura bonaerense: oficialismo y oposición discuten los cargos de la
  renovación'
title: 'Legislatura bonaerense: oficialismo y oposición discuten los cargos de la
  renovación'
entradilla: "En un contexto de una paridad extrema se negocian las autoridades de
  las Cámaras: cuatro vicepresidencias en el Senado; y la presidencia en Diputados,
  más otras cuatro vices.\n\n"

---
Un total de 23 senadores y 46 diputados asumirán en sus bancas el próximo jueves y, de cara a la sesión preparatoria, los bancadas parlamentarias definen los nombres de quienes comandarán los bloques y las autoridades de ambas Cámaras que tienen en agenda proyectos clave como el Presupuesto y la Ley Impositiva.  
  
La sesión preparatoria en la Legislatura se realizará a las 15 y allí juran los legisladores que asumirán el 10 y diseñarán la nueva conformación del cuerpo legislativo que permitirá al Frente de Todos (FdT) recuperar la mayoría en el Senado bonaerense, que ahora está en manos de Juntos por el Cambio con 26 de los 46 escaños, y mantener la primeria minoría en Diputados.  
  
Desde ahora, el oficialismo tendrá 23 bancas, mientras que Juntos contará con otras 23, pero es la presidenta del Senado, Verónica Magario, quien decide en caso de empate en el voto de un proyecto de ley.  
  
Con esa nueva composición, el gobernador Axel Kicillof espera poder sancionar este mes el Presupuesto -en el que se prevé un gasto total para el año próximo de $ 3,1 billones, y un pedido de nuevo endeudamiento de $90 mil millones-, la Ley Impositiva - que contempla un aumento de los impuestos patrimoniales del 35% promedio para la mayoría de los contribuyentes- y los acuerdos para la designación de vacantes en el directorio del Banco Provincia.  
  
En cambio, la aprobación de los pliegos de los distintos magistrados y reemplazos de los jueces de la Suprema Corte y una posible reforma a la Ley de Ministerios serán iniciativas para el mediano plazo.   
  
En tanto en Diputados, el peronismo mantuvo la primera minoría y tendrá 42 escaños; Juntos, 41; Cambio Federal, 2; el FIT otros 2; Avanza Libertad, 3; y dos unibloques: 17 de Noviembre y Partido Fe.  
  
En este contexto de una paridad extrema en la Legislatura se negocian las autoridades de las Cámaras: cuatro vicepresidencias en el Senado; y la presidencia en Diputados, más otras cuatro vices.  
  
Las conversaciones continuarán toda la semana y recién una vez que se alcancen los acuerdos en torno a esos cargos, comenzarán a definirse las jefaturas de los distintos bloques políticos.  
  
Envalentonados por el triunfo electoral, algunos referentes de JxC amenazaron con disputarle la presidencia de ese cuerpo al oficialismo, hecho que -dijeron- podrían lograr al aunar número con los liberales de José Luis Espert.  
  
Fuentes del Frente de Todos señalaron que confían en que se mantenga el criterio de mantener los espacios de gobernabilidad para el oficialismo y por eso están convencidos de que ese lugar seguirá en manos de la coalición oficialista.  
  
Desde ese espacio coinciden en que Federico Otermín, del espacio del jefe de Gabinete provincial, Martín Insaurralde y con excelente vínculo con La Cámpora, continuará al frente de Diputados.  
  
Señalan, también, que el intendente de Almirante Brown, Mariano Cascallares -quien no sólo le sacó 19 puntos a JxC en el distrito sino que encabezó la lista de la Tercera, sección en la que el peronismo ganó- tiene garantizado un lugar de relevancia en esa Cámara.  
  
En ese escenario, el massista Rubén Eslaiman quedaría en una de las vicepresidencias, y resta definir si el histórico dirigente kirchnerista cercano a Kicillof, Carlos "Cuto" Moreno, podrá permanecer allí; mientras que la oposición negociará las otras dos.  
  
Desde Juntos buscan mantener en alguna de las vices a Adrián Urreli, referenciado en el jefe comunal de Lanús, Néstor Grindetti; mientras que el intendente de La Plata, Julio Garro, impulsa a Fabián Perechodnik para un lugar preponderante en la Cámara baja.  
  
Para ello, Garro buscó en las últimas horas el aval de Horacio Rodríguez Larreta y María Eugenia Vidal en un encuentro en el cual se coincidió que La Plata merece un espacio importante "no sólo por ser la capital de la provincia, sino por el caudal de votos que aportó a Juntos en las elecciones".  
  
En el sector grafican que "La Plata es a JxC lo que La Matanza es para el FdT".  
  
Otras figuras que suenan para las vicepresidencias son el exintendente de Quilmes, Martiniano Molina, quien hizo una buena performance electoral en ese distrito al encabezar la boleta de la Tercera, y Santiago Passaglia, con peso en la Segunda sección.  
  
En cuanto a las presidencias de bloques, el actual jefe en Diputados y dirigente de La Cámpora, Facundo Tignanelli, no logró renovar su banca en las elecciones, ya que fue posicionado en el noveno lugar de la lista de la Tercera sección y finalmente el kirchnerismo alcanzó ocho bancas.  
  
Entonces, el dirigente de La Matanza, de extrema confianza de Máximo Kirchner, sólo podría lograr un escaño si alguno de los ocho diputados electos en esa lista renuncia.  
  
Como parte de las conversaciones, tampoco se descarta que Cascallares pueda asumir en la banca y pedir licencia si se lo convoca para un cargo en el Ejecutivo, con lo que se liberaría un espacio y Tignanelli, que integró esa misma boleta, podría asumir y allí mantenerse como jefe del bloque.  
  
En caso que eso no suceda, las fuentes señalan que habrá que alcanzar un acuerdo entre los múltiples sectores que componen el FdT, el massismo, los intendentes y La Cámpora.  
  
En Juntos, diversos exponentes del espacio coinciden en las altas posibilidades con que cuenta el radical Maximiliano Abad para continuar como titular del bloque.  
  
Por otro lado, en el FdT del Senado, algunas voces promueven que continúen en las vicepresidencias Alfredo Fisher, como representante del interior bonaerense, y Luis Vivona; aunque una de esas sillas podría ir también para el Frente Renovador.  
  
Todas las fuentes consultadas coinciden en que ello también estará atado a cómo se resuelvan los cargos en Diputados para alcanzar equilibrios en el reparto de poder.  
  
Desean, ante todo, que exista una distribución equitativa no sólo entre los sectores que integran el Frente sino entre las secciones electorales que componen la provincia, primer distrito electoral del país.  
  
Como siempre, ello se definirá a último momento cuando se encuentren los consensos que dejen conformes a todos los miembros del espacio y a los máximos referentes a nivel provincial y nacional.  
  
En paralelo, se estima que Juntos pedirá una de las vicepresidencias del Senado para el exintendente de San Miguel y exministro Joaquín de la Torre y otra para el radical Alejandro Cellillo o para el "lilito" Andrés De Leo.  
  
Todo indica que presidirá el bloque del Frente de Todos en la Cámara alta la exministra de Gobierno, Teresa García, en reemplazo de Gervasio Bozzano, quien finaliza su mandato.  
  
"Es natural que asuma Teresa: cuenta con experiencia, con el OK del gobierno y con una buena gimnasia legislativa. Se va a necesitar mucho debate para reunir el quórum y ella tiene la muñeca para lograrlo", resaltaron desde los diferentes espacios del PJ.  
  
En tanto, para la jefatura de Juntos son varios los anotados para ocupar el cargo que deja Roberto Costa: Larreta, Vidal y Diego Santilli promueven el nombre de Alejandro Rabinovich, quien, además, cuenta con el apoyo del jefe comunal de Mar del Plata, Guillermo Montenegro.  
  
Mientras que el jefe comunal de Tres de Febrero, Diego Valenzuela -avalado por otros intendentes del Conurbano- aspira a que sea su esposa, la senadora Daniela Reich, quien quede a cargo del bloque.  
  
En simultáneo, el exjefe comunal de Vicente López y actual ministro de Gobierno porteño, Jorge Macri, y el expresidente del club Boca Juniors Daniel Angelici, buscan que la bancada quede en manos de Christian Gribaudo, lo que genera rechazos internos del radicalismo y del sector de Elisa Carrió.  
  
A la vez, suenan los nombres de la bahiense Nidia Moirano y del platense Juan Pablo Allan (respaldado por el intendente Garro).  
  
Con final abierto, las negociaciones llegarán a su fin el mismo jueves. Y, se sabe, puede haber sorpresas.