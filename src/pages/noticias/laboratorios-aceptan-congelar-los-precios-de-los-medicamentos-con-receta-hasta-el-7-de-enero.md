---
category: Agenda Ciudadana
date: 2021-11-09T06:15:02-03:00
thumbnail: https://assets.3dnoticias.com.ar/medicamentos.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: Laboratorios aceptan congelar los precios de los medicamentos con receta
  hasta el 7 de enero
title: Laboratorios aceptan congelar los precios de los medicamentos con receta hasta
  el 7 de enero
entradilla: "Así se lo comunicaron al secretario de Comercio Interior, Roberto Feletti,
  impulsor de la medida que busca bajar la escalada inflacionaria. \n"

---
El Gobierno y los laboratorios acordaron hoy mantener los precios de los medicamentos hasta el 7 de enero de 2022.

Los laboratorios se lo comunicaron al secretario de Comercio Interior, Roberto Feletti, pero aclararon que será sólo en el caso de los "prescriptos".

El Gobierno pretende también impulsar la prescripción de medicamentos por nombre genérico, con el fin de impulsar una baja en los precios.

Las tres cámaras de laboratorios, nacionales y multinacionales, dieron a conocer su aceptación al congelamiento de precios propuesto por el gobierno.

Según indicaron las cámaras CAEME, CILFA y Cooperala en un comunicado conjunto, se aceptó el planteo oficial de retrotraer los precios al 1 de noviembre último, para medicamentos recetados.

"En el marco de colaboración y buen diálogo iniciado en el Ministerio de Salud el día jueves último, respecto de la evolución de la economía y de los precios de los medicamentos, CAEME, CILFA y COOPERALA han manifestado a la Secretaría de Comercio que las empresas asociadas a cada una de las cámaras han mostrado su disposición a retrotraer los precios de los medicamentos de prescripción de todos los segmentos al precio de venta al público del lunes 1 de noviembre y, asimismo, su voluntad de mantener dichos precios estables hasta el día 7 de enero del 2022", sostuvieron.

La aceptación refrenda el acuerdo que el jueves último alcanzaron gobierno y los laboratorios, que debían confirmar hoy.

Según un comunicado conjunto, las cámaras señalaron que "han manifestado a la Secretaría de Comercio que las empresas asociadas a cada una de las cámaras han mostrado su disposición a retrotraer  
los precios de los medicamentos de prescripción de todos los segmentos al precio de venta al público del lunes 1° de noviembre".

El comunicado señala que "cada laboratorio asociado ejercerá la facultad de adherir individualmente a esta iniciativa", de congelamiento de precios.

Los laboratorios señalaron que aceptaron la propuesta del gobierno "en el marco de colaboración y buen diálogo iniciado en el Ministerio de Salud el día jueves último, respecto de la evolución de la economía y de los precios de los medicamentos".

Las cámaras expresaron que decidieron "mantener esos precios estables hasta el día 7 de enero del 2022″.