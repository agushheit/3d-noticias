---
category: Agenda Ciudadana
date: 2021-07-20T06:15:06-03:00
thumbnail: https://assets.3dnoticias.com.ar/vacunatorio.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Santa Fe ya piensa en la disposición de "vacunatorios libres" a partir de
  agosto
title: Santa Fe ya piensa en la disposición de "vacunatorios libres" a partir de agosto
entradilla: En estos centros se podrán vacunar personas sin turno que no se hayan
  inscripto en el registro, completando a comienzos de agosto la aplicación de primeras
  dosis a los inscriptos, según Martorano.

---
En la continuidad de la vacunación contra el Covid en la provincia, la cartera sanitaria espera completar la aplicación de primeras dosis al total de la población inscripta en el registro "Santa Fe Vacuna".

La ministra de Salud, Sonia Martorano, destacó que se espera que a fines de julio y principios de agosto se finalice con la aplicación de primeros componentes de las vacunas a quienes se inscribieron, lo que posibilitaría disponer de "vacunatorios libres" en Santa Fe luego de que se cumpla con este objetivo.

Éstos son postas de vacunación en donde no es necesario estar inscripto ni estar turnado para recibir la vacuna, pensadas para alcanzar a la población que no se pudo inscribir en el registro oficial.

"No estamos lejos" fue la respuesta de la ministra en diálogo con el programa radial "De 10" emitido por la emisora LT10, sobre la implementación de vacunatorios libres en Santa Fe, aunque subrayó que "es prioritario vacunar a aquellos que ya manifestaron su voluntad y se inscribieron. Toda la fuerza de vacunación, que ronda los 43.000 turnos diarios, tiene que estar para completar la vacunación de quienes ya están inscriptos. Para esto falta poco".

"Si seguimos a este ritmo de vacunación y llegada de vacunas me atrevo a decir que a fin de julio y principios de agosto ya estaremos finalizando con la vacunación de todos los inscriptos y podríamos disponer de algunas "postas" donde la gente que no se haya inscripto por algún motivo se pueda vacunar por una cuestión de cercanía", afirmó Martorano.

En la provincia de Buenos Aires ya comenzó la disposición de estos vacunatorios libres sin necesidad de turno para hacerlo, a pesar de que no se haya completado la inmunización con primeras dosis a toda la población registrada. Se están disponiendo durante los fines de semana en distintos puntos del Gran Buenos Aires, abarcando a toda la población mayor de 35 años que desee vacunarse y no se haya registrado en el sitio oficial.

La ministra de Salud santafesina, Sonia Martorano, puntualizó en que esto "ya se está haciendo territorialmente en los centros de Salud, donde nosotros nos acercamos a nuestra población adscripta, vamos a los barrios, teniendo en cuenta si no se anotaron por un tema de acceso o información. Para generar equidad nos estamos acercando, los inscribimos y se vacunan en el territorio. Una vez que terminemos con los inscriptos pondremos toda la fuerza en los vacunatorios libres".

A propósito de la etapa actual del operativo de vacunación en Santa Fe, donde se está inoculando a personas de entre 18 y 25 años con primeras dosis, Martorano manifestó: "Lo que notamos es que hay mucha adscripción a la vacuna, que la ven como algo lejano. Los que no se anotaron es porque pensaron que era algo lejano. No notamos una negativa a la vacuna".

"No sabemos si con este virus vamos a tener inmunidad de rebaño. Lo que deberíamos tener es al 70% de la población vacunada con las dos dosis. En septiembre estaremos colocando todas segundas dosis, por lo que podríamos llegar a ese porcentaje, pero dependerá de como sea la circulación de este virus. Uno es optimista pensando en septiembre u octubre porque cuanto más abramos las ventanas y poder reunirnos al aire libre se disminuye la posibilidad de contagios", concluyó.