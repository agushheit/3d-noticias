---
category: Agenda Ciudadana
date: 2022-12-01T08:29:13-03:00
thumbnail: https://assets.3dnoticias.com.ar/nafta.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: Aumentan los combustibles de hoy en todo el país.
title: Aumentan los combustibles de hoy en todo el país.
entradilla: Se pone en práctica el acuerdo con las petroleras, las naftas subirán
  4% mensual hasta febrero y 3,5% en marzo

---
El cronograma acordado con las empresas **YPF, Raizen** (titular de la marca Shell), **Trafigura** (de la red Puma) y **Axion** prevé subas de 4% en diciembre; 4% en enero; 4% en febrero y 3,8% en marzo.

El incremento se encuentra determinado por el acuerdo de Economía con las petroleras, mediante el cual estas últimas se incorporaron al programa Precios Justos, cuyo objetivo es anclar el índice de precios. Por ese acuerdo, el Estado se compromete a poner en garantía el **acceso a divisas** para las empresas, sobre todo para el abastecimiento de lubricantes, a reducir temporalmente impuestos en la importación de combustibles para garantizar el abastecimiento para los sectores del agro, sobre todo durante los meses de enero y febrero, son los más importantes.