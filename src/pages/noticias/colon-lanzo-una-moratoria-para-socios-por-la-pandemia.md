---
category: Deportes
date: 2020-12-17T10:36:07Z
thumbnail: https://assets.3dnoticias.com.ar/1712-colon.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Colón lanzó una "moratoria para socios" por la pandemia
title: Colón lanzó una "moratoria para socios" por la pandemia
entradilla: A través de sus redes sociales y en su web oficial, Colón lanzó "una moratoria
  para los socios" para los que tienen deuda desde febrero de 2020.

---
Los clubes vieron afectadas sus economías de una manera singular durante la pandemia, ya que muchos de sus socios dejaron de pagar la cuota como consecuencia de la crisis desatada. Colón, en particular, también sufrió las consecuencias, con lo cual los dirigentes con el afán de recuperarlos **lanzaron una "moratoria" para que puedan recomponer su estado de cuenta hasta el 31 de diciembre**.

A esta singular moratoria para socios durante la pandemia Colón la dio a conocer en sus redes sociales y a través de la web oficial, donde se comenzó informando: "El Club Atlético Colón recuerda a sus asociados que ha lanzado una moratoria para los socios que vieron afectada su economía familiar producto de la pandemia".

"La misma tiene validez para los socios que mantienen deuda de cuotas societarias desde febrero de 2020, quienes **podrán recomponer su estado de cuenta con la institución abonando hasta el 31 de diciembre de 2020 SIN PERDER LA ANTIGÜEDAD**. En este sentido, a partir del 1 de enero de 2021, se aplicarán las bajas de acuerdo al art. 20 del Estatuto de la Institución", detalla Colón sobre el alcance de esta moratoria.

***

![](https://assets.3dnoticias.com.ar/1712-colon1.jpg)

***

Mientras que luego, el comunicado de Colón destaca: "Los interesados en acogerse a la moratoria podrán asistir a la sede de la Institución **de lunes a viernes de 8 a 20 hs o los sábados 9 a 13 hs** **o a través de la nueva plataforma de gestión online de la institución**".

"Los socios podrán abonar en sede social en efectivo o con **tarjetas de débito o crédito** Visa o MasterCard de cualquier banco. En tanto, las **tarjetas de crédito Visa y MasterCard del Banco Macro dan la posibilidad de abonar en 6 pagos sin intereses**. Para finalizar, mediante la nueva plataforma [socios.clubcolon.com.ar](https://socios.clubcolon.com.ar/ "Plataforma web") podrán pagar con tarjetas de débito o crédito disponibles en Mercado Pago", aclara Colón sobre los medios por los cuales los socios pueden acogerse a dicha moratoria.

Mientras que en el final del comunicado, se indica: "Sin perjuicio de lo expresado, se hace saber que **los medios de pago mencionados son los únicos autorizados por la Institución**, desconociendo autenticidad a otras formas de pago que están circulando por redes sociales, dejando aclarado que el Club Colón nada tiene que ver al respecto y que no se hace responsable por el accionar y los daños que pudieran causar las personas involucradas en los mismos, no teniendo constancia de que reúnan los requisitos exigidos por la Ley de Entidades Financieras y por la normativa del B.C.R.A. para poder operar en actividades financieras".