---
category: La Ciudad
date: 2021-04-09T07:23:26-03:00
thumbnail: https://assets.3dnoticias.com.ar/castracion.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad
resumen: La campaña municipal de castración gratuita sigue en los distintos barrios
title: La campaña municipal de castración gratuita sigue en los distintos barrios
entradilla: El puesto montado en la Vecinal de Pompeya Oeste recibió esta mañana la
  visita del intendente Emilio Jatón. Se castran 20 animales por día en cada uno de
  los puntos habilitados.

---
El intendente Emilio Jatón visitó uno de los puntos donde se lleva a cabo la campaña de castración móvil, destinada a animales de compañía. Este jueves estuvo en la sede de la Vecinal Pompeya Oeste donde charló con los vecinos y vecinas que se acercaron para hacer atender a su mascota.

“El balance de los primeros 15 días fue un éxito. Durante ese período castramos a 1.500 animales y vacunamos 2.000, sumado que ahora agregamos vacunación y puestos de adopción los fines de semana en las plazas, eso hace que la gente acceda a la vacunación de manera gratuita en diferentes espacios verdes”, destacó la subdirectora de Salud Animal, Anahí Montiel.  
El plan de castración, vacunación y atención de animales de compañía es un compromiso que asumió el intendente desde el inicio de la gestión, con el objetivo de llegar a todos los barrios con esta política pública que apunta no sólo a la salud animal sino de toda la comunidad.

Analía Bravo, presidenta de la vecinal Progreso Nueva Pompeya, manifestó luego de dialogar con el intendente: “La gente se involucra bastante con la campaña, que nunca se hizo en el barrio. Es muy accesible. Nosotros anotamos para dar turnos en la misma vecinal, y eso le hace las cosas más fáciles a los vecinos”.

Por otro lado, Bravo destacó que la Municipalidad está realizando “20 castraciones por día, además de las vacunaciones; estas últimas son sin turno previo”. Al mismo tiempo, expresó: “Estamos muy contentos porque hay una muy buena convocatoria. Hasta el 23 de abril estaremos acá, así que el que no pueda o no sepa sacar turnos por internet, que se acerque a la vecinal y nosotros los anotamos, les damos las recomendaciones necesarias y sólo les queda venir nomás”.

Antes de finalizar, destacó e hizo un llamado a los vecinos y vecinas: “Está lleno de perros por las calles del barrio, así que es muy importante esta campaña. Y esto es para perros, perras, gatas y gatos, de esa manera se reduce la población y es salud para los vecinos”.

**En toda la ciudad**

Montiel recordó que los puestos de castración continúan una semana más en cada uno de los lugares donde están: “Después cambiaremos de barrios, así que las vecinas y vecinos que aprovechen a sacar el turno en la vecinal o en la página web”.

“El objetivo es castrar 20 animales por día en cada uno de los puntos y eso se está cumpliendo”, dijo Montiel y subrayó la importancia de que las vecinas y vecinos cumplan con los turnos: “Pedimos por favor la responsabilidad de los vecinos que accedan al turno y respeten la fecha que les damos. En el caso de que no puedan venir, que lo den de baja, para que otra gente pueda acceder a esa posibilidad”.

Por último, la subdirectora de Salud Animal dijo que “las vacunaciones son ilimitadas. Puede venir la gente que quiera. La campaña seguirá durante todo el año”.

Vale recordar que la Municipalidad continúa con este servicio en forma simultánea en los ocho distritos, y además en el IMUSA del Jardín Botánico y del Parque Garay; y en la Protectora de Animales. También se ofrece vacunación y se presta atención veterinaria, todo de manera gratuita, de 8 a 12.

Los turnos son programados y pueden obtenerse en la web oficial [www.santafeciudad.gov.ar/turnos](http://www.santafeciudad.gov.ar/turnos). Allí se debe buscar el apartado denominado “Castraciones”, se elige la opción “Solicitar un nuevo turno” y se completa el formulario con los datos pedidos. Posteriormente se elige el día, el horario y la sede a la cual concurrir.

**Cronograma de abril**

**Hasta el 16 de abril:**

– Distrito Noreste: Vecinal La Esmeralda, La Esmeralda 2471

– Distrito de la Costa: Vuelta del Paraguayo, salón parroquial

– Distrito Centro/Este: Vecinal Fomento 9 de Julio, Pedro Ferre 2928

**Hasta el 23 de abril:**

– Distrito Oeste: Vecinal Ciudadela Norte, Gorostiaga 3955

– Distrito Noroeste: Club Cabal, Servando Bayo 6730

– Distrito Norte: Vecinal Pompeya Oeste, San José 7756

– Distrito Suroeste: Vecinal Santa Rosa, Tucumán 4550

**Del 19 al 30 de abril:**

– Distrito de la Costa: La Casa del Nogal, Ruta 1 kilómetro 2

– Distrito Centro/Este: Club Echeverría, Sarmiento 4068

– Distrito Noroeste: APAM, Alfonsina Storni 2500

**Del 26 de abril al 7 de mayo:**

– Distrito Oeste: Vecinal Barranquitas Unión y Progreso, López y Planes 4069

– Distrito Noroeste: Vecinal Ceferino Namuncurá, 12 de octubre 9501

– Distrito Norte: Vecinal Nueva Pompeya, French 3878

– Distrito Suroeste: Vecinal Barrio Roma, Tucumán 3957