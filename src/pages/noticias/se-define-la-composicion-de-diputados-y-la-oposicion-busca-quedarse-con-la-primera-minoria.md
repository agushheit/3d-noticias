---
category: Agenda Ciudadana
date: 2021-11-14T06:00:22-03:00
thumbnail: https://assets.3dnoticias.com.ar/URNA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: Se define la composición de Diputados y la oposición busca quedarse con la
  primera minoría
title: Se define la composición de Diputados y la oposición busca quedarse con la
  primera minoría
entradilla: El Frente de Todos enfrenta un escenario delicado, ya que la impensada
  derrota en las PASO demolió sus aspiraciones iniciales de conseguir quórum propio.

---
Los resultados de las primarias abiertas, simultáneas y obligatorias (PASO) dejaron un panorama muy alentador para Juntos por el Cambio, que si mejora levemente su desempeño en las elecciones de este domingo logrará su objetivo de arrebatarle al oficialismo la primera minoría en la Cámara de Diputados.

El Frente de Todos, en tanto, enfrenta un escenario delicado, ya que la impensada derrota en las PASO demolió sus aspiraciones iniciales de conseguir quórum propio en la Cámara baja, y ahora se conformaría con que la pérdida de legisladores sea la menor posible.

Si Juntos por el Cambio llegara a superar en cantidad de legisladores al Frente de Todos luego del recambio parlamentario del 10 de diciembre, se podría desatar una cruenta disputa por la presidencia de la Cámara baja, un cargo que tiene una relevancia singular porque se encuentra en la línea de sucesión presidencial.

La primera candidata a diputada nacional de Juntos por el Cambio por la Ciudad de Buenos Aires, María Eugenia Vidal, ya avisó que si fuera por ella, la oposición debería reclamar la presidencia de Diputados, aunque luego aclaró que la decisión deberá tomarse por consenso entre los cinco espacios de la coalición.

En la actualidad, el Frente de Todos tiene 120 diputados y si se repitiesen los guarismos de las PASO, quedaría en 117 legisladores, con la salvedad de que el presidente de la Cámara de Diputados, Sergio Massa, no vota a excepción de necesidad de desempate en los debates.

Juntos por el Cambio se encontró con un escenario impensado en las primarias, por encima de las expectativas previas, y llegaría también a los 117 diputados si se reprodujesen tal cual los resultados de las PASO, en una situación de virtual empate con el oficialismo.

La propia Vidal remarcó que para la elección general del domingo el objetivo es lograr un interbloque sólido de 120 diputados, superior en número a la bancada del Frente de Todos.

Ese interbloque tendría capacidad para conseguir quórum con la colaboración de otras bancadas minoritarias de la oposición, y también tendría el poder de bloquear sesiones impulsadas por el oficialismo con temarios ajenos al interés de JxC.

Hasta este año, el Frente de Todos no contaba con mayoría propia pero en casi todas las sesiones disponía de la ayuda de bloques minoritarios de la oposición que colaboraban con el quórum.

La nueva correlación de fuerzas que se verá expresada a partir de fin de año, de proyectarse los resultados de las PASO, aleja ese objetivo.

Para colmo, las posibilidades de perder el quórum y la mayoría propia en el Senado son altas después del resultado del domingo, lo cual le traerá más de un dolor de cabeza a la vicepresidenta Cristina Kirchner.

Entre las novedades de la elección se encuentra el fortalecimiento de los extremos del espectro ideológico: el Frente de Izquierda, que ponía en juego sus dos únicas bancas, alcanzaría cuatro luego en la nueva Cámara baja, mientras que los libertarios entrarían en escena con al menos cuatro bancas.  
bancas.

El Frente de Izquierda sumaría a Myriam Bregman por la Capital Federal, a Nicolás del Caño y Romina del Plá por la provincia, y a Alejandro Vilca por Jujuy.

Más de la mitad de las bancas para Diputados se definirán en los cuatro distritos más grandes del país a nivel demográfico, por lo que en esas provincias radicará el mayor atractivo e interés de la jornada electoral: Buenos Aires (35 diputados), Ciudad de Buenos Aires (13 diputados), Córdoba (9 diputados) y Santa Fe (9 diputados).

En la provincia de Buenos Aires, el oficialismo arriesga 17 bancas en la Cámara baja, ya que esa era la cantidad de cargos que había obtenido con la elección que traccionó la candidatura a senadora de Cristina Kirchner en 2017, el número con el que debe cotejarse estos comicios de medio término.

Juntos por el Cambio, por su parte, pone en juego 14 bancas en el distrito más grande del país y busca cosechar al menos 16.