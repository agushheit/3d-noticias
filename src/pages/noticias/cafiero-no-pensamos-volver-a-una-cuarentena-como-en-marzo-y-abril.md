---
category: Agenda Ciudadana
date: 2021-01-18T09:00:52Z
thumbnail: https://assets.3dnoticias.com.ar/cafiero.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: 'Cafiero: "No pensamos volver a una cuarentena como en marzo y abril"'
title: 'Cafiero: "No pensamos volver a una cuarentena como en marzo y abril"'
entradilla: El jefe de Gabinete subrayó que la Argentina es el país que más vacunó
  en la región, pero que el proceso se ralentiza porque "hay un cuello de botella"
  en la producción mundial de vacunas.

---
El jefe de Gabinete, Santiago Cafiero, aseguró que no está previsto volver a una cuarentena estricta como la de 2020, al tiempo que remarcó que hay "un cuello de botella" en la producción de vacunas contra el coronavirus en todo el mundo, lo que ralentiza el proceso de vacunación en el país.

"Hay un cuello de botella en la producción de vacunas en el mundo, no es un tema de la Argentina. La Argentina es uno de los pocos países que arrancó su campaña de vacunación. En la región, Argentina es el que más vacunó", indicó Cafiero.

El funcionario subrayó que si bien "muchos se anticiparon a decir que no iban a llegar las vacunas en diciembre", la Argentina "fue uno de los primeros países en la región que arrancó a vacunar a su población y hoy sigue siendo uno de los países que más vacuna dio en la región".

"Si podemos contener esta aceleración de casos y el plan de vacunación empieza a generar mayor masividad, el Gobierno en todo momento planifica que la mayor presencialidad posible se dé durante el mes de marzo", manifestó sobre la posibilidad del regreso a clases, una iniciativa por la que viene presoniando la oposición.

También aseguró que "toda la comunidad educativa quiere que las clases vuelvan" por lo que "no hay que ideologizar" el tema.

"Macri quiere enojar a los que ya están enojados y angustiar a los que ya están angustiados. Y él trata de tener algún rédito político, pero en realidad no aporta nada porque el trabajo se viene haciendo", aseveró.

Por último, consideró que "hoy el sistema de salud logró robustecerse y el sector productivo se adaptó con protocolos", por lo que las nuevas restricciones "tienen que ver con cuestiones conductuales sociales o de nocturnidad".

"Vamos a evitar que la curva se acelere y vamos a cuidar la actividad económica. Hoy por hoy, con los casos que tenemos, con cómo está respondiendo el sistema de salud, nosotros estamos privilegiando sostener la actividad económica y la actividad productiva y cuidar que la curva de contagios no se dispare. No pensamos en volver a una cuarentena como en marzo y abril", concluyó.