---
layout: Noticia con imagen
author: .
resumen: Policía de Santa Fe
category: Estado Real
title: "Egreso de cadetes del ISeP en Rosario y Recreo: Se incorporaron 391
  nuevos policías a la provincia"
entradilla: Por protocolo y, en el marco de la pandemia Covid-19, la entrega de
  diplomas se hizo a los cinco mejores promedios de cada lugar.
date: 2020-11-15T14:39:33.541Z
thumbnail: https://assets.3dnoticias.com.ar/policia.jpg
---
El gobierno de la provincia, a través del Ministerio de Seguridad, entregó este viernes los diplomas a 391 cadetes de la Policía de Santa Fe que egresaron del Instituto de Seguridad Pública (ISeP). Por cuestiones de protocolo y, en el marco de la pandemia de Covid-19, la entrega se realizó a los cinco mejores promedios de Rosario y Recreo, y luego se hará lo mismo al resto de los flamantes agentes, cumpliendo las medidas sanitarias para estos casos.

Presente en el lugar, el subsecretario de Formación y Capacitación del Ministerio de Seguridad de la provincia, Andrés Rolandelli, explicó que “hoy egresa la segunda cohorte de efectivos policiales que se integran a la Policía de Santa Fe, en un año muy complejo y en el que tuvimos que readaptar el cursado y el acto de promoción por la pandemia”.

En ese marco, el funcionario destacó que “los nuevos agentes de policía deben saber que la formación y la capacitación es constante durante toda la carrera, y deben poner su mejor empeño y esfuerzo en ello”.

Luego de dos años de formación continua, los 391 nuevos miembros de la fuerza provincial comenzarán a prestar servicio en las 19 unidades regionales que componen el territorio de la provincia. La carrera policial consta de dos años de cursado intensivo en las instalaciones del ISeP y otros dos de capacitación, mientras trabajan en el destino asignado.

De la actividad participaron, también, el director general del Instituto, Gabriel Leegstra; y los integrantes del Consejo interinstitucional ISeP: Nancy Marchi por el Ministerio de Seguridad, Cristian Bataglino por el Ministerio de Educación, Carlos Lemos, representante de la Policía de Santa Fe.

**EL ISEP**

El Instituto de Seguridad Pública es una herramienta esencial para seleccionar y formar a los aspirantes a ingresar a la Policía de la provincia, y para capacitar y actualizar al personal policial que integra la fuerza.

Cuenta con dos delegaciones, Centro-Norte y Sur, con sede en Recreo y Rosario, respectivamente, donde se trabaja con dos grandes líneas de acción: los concursos de Ascensos Policiales y las diversas convocatorias de ingreso de personal para distintas áreas de la cartera de Seguridad.

Allí se dicta la carrera de Auxiliar en Seguridad, de dos años de duración; y la Tecnicatura Superior en Seguridad Pública y Ciudadana.

El cursado y posterior aprobación de la carrera de Auxiliar en Seguridad resultan necesarios para la adquisición del estado policial efectivo. Durante esta etapa de formación se desarrollan y perfeccionan aspectos básicos de la personalidad profesional que requiere un agente policial en sus condiciones ética, intelectual, social y física.