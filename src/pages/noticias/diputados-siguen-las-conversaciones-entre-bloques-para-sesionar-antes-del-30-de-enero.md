---
category: Agenda Ciudadana
date: 2021-01-14T10:09:45Z
thumbnail: https://assets.3dnoticias.com.ar/sesiones-diputados.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: '3DNoticias - Fuente: Télam'
resumen: 'Diputados: siguen las conversaciones entre bloques para sesionar antes del
  30 de enero'
title: 'Diputados: siguen las conversaciones entre bloques para sesionar antes del
  30 de enero'
entradilla: El acuerdo para sesionar para JXC depende de que en esa primera sesión
  extraordinaria se debatan temas acordados y evitar proyectos que necesitan mayores
  acuerdos.

---
Los bloques parlamentarios del oficialismo y de la oposición de la Cámara de Diputados buscarán avanzar en los próximos días en acuerdos para realizar una sesión antes del 30 de enero, en la que se tratarían un conjunto de proyectos consensuados.

Fuentes parlamentarias anticiparon que serán claves las negociaciones que se establezcan para acordar tanto el protocolo de trabajo parlamentario como el temario de los proyectos que se incluirían en esa primera sesión extraordinaria que podría realizarse el 20 o el 27 de enero próximos.

El temario que el oficialismo busca debatir en principio contemplaría un conjunto de proyectos como el de sostenibilidad de la deuda externa, incentivos a la construcción y una prórroga a la ley de biocombustibles, que no está entre las iniciativas incluidas en la convocatoria a extraordinarias.

Las conversaciones informales entre el titular de la Cámara bajan, Sergio Massa, y los jefes de los principales bloques, continuarán para avanzar en los acuerdos necesarios para sesionar antes del 30 de enero.

Las fuentes aseguraron que, si bien ya hay un principio de acuerdo entre el oficialista Frente de Todos y Juntos por el Cambio para renovar el protocolo de trabajo que funcionó hasta fin de año, el principal bloque opositor supeditó la firma de ese instrumento a la redacción de un anexo que contemple todo el temario a debatir.

Allegados al titular del interbloque de JXC aseguraron que «Negri le dijo a Massa que no había inconvenientes en prorrogar el protocolo hasta el 30 de enero, pero atento al temario de extraordinarias que contiene una gran cantidad de temas, JXC quiere que se ponga en un anexo los temas de la sesión para tranquilidad de todos».

Las fuentes dijeron que JXC quiere que en esa primera sesión extraordinaria se debatan temas acordados y evitar, de esa forma, que se incluyan otros proyectos que necesitan mayores acuerdos, **como la reforma judicial y los cambios a la ley del Ministerio Público**, que el principal bloque opositor **rechaza de plano**.

El protocolo contempla las **sesiones presenciales con excepciones** de los diputados que forman parte del grupo de riesgo o los que estén impedidos de viajar, siempre con el **sistema de registro por VPN** (_virtual private network_).

Las fuentes recordaron que los bloques deben firmar el protocolo para continuar con el trámite parlamentario de los proyectos que deben ser tratados la semana próxima en comisión -entre lunes y martes- para llegar a la sesión del 20 de enero.

De lo contrario, agregaron las fuentes, seguirán buscando acuerdos parlamentarios para sesionar el 27 de enero.

Entre los temas que se podrían tratar en esa primera sesión del período extraordinario figurarían además un proyecto que prevé un resarcimiento económico para los familiares de los tripulantes del ARA San Juan y otro que fija una meta presupuestaria para el área de Ciencia y Tecnología.