---
category: Agenda Ciudadana
date: 2022-12-19T11:36:56-03:00
thumbnail: https://assets.3dnoticias.com.ar/2022-12-14NID_276869O_1.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: Vacunatorios móviles; horarios y ubicaciones en la ciudad.
title: Vacunatorios móviles; horarios y ubicaciones en la ciudad.
entradilla: Es ante el aumento de casos a nivel nacional. Es ante el aumento de casos
  a nivel nacional. “Es muy importante que todos y todas completemos los esquemas"
  manifestaron desde el Ministerio de Salud santafesino.

---
En la capital santafesina, los mismos estarán funcionando en: San Martín 2020 de lunes a sábados de 9.00 a 12.00; en Aristóbulo del Valle y Hernandarias de lunes a viernes de 17.30 a 20.00 horas y sábado de 9.00 a 12.00; mientras que el tercero estará ubicado en las instalaciones del Hipermercado Chango Más, Ruta 168, viernes y sábados de 17.00 a 20.00 horas.

Por otro lado, en la ciudad de Rosario, el operativo de plaza San Martín (Dorrego y Cordoba) funciona de lunes a viernes de 8.30 a 12.30, mientras que hasta el viernes habrá otros dos dispositivos en Gálvez y Alem de 8.00 a 12 mientras que el tercero estará ubicado en Rouillón y Seguí de 8.00 a 12.00 horas.

Asimismo, cada santafesino y santafesina podrá localizar su vacunatorio más cercano a su hogar en la web provincial [https://www.santafe.gob.ar/ms/.](https://www.santafe.gob.ar/ms/. "https://www.santafe.gob.ar/ms/.")

Al respecto, el subsecretario de Promoción de la Salud, Sebastián Torres, indicó que “desde el Ministerio de Salud estamos trabajando en poder reforzar la estrategia de vacunación con el objetivo de que la gente tenga mayor acceso a las vacunas ya que estamos en presencia de un nuevo aumento de casos de covid-19 tanto a nivel nacional como provincial”.

A su vez, el subsecretario destacó que “estamos trabajando con todos los centros de salud, los vacunatorios de los hospitales y los operativos itinerantes a través de los camiones sanitarios que dispone el Ministerio de Salud, trabajando en conjunto con la cartera de Desarrollo Social, para abarcar diferentes áreas y garantizar la accesibilidad de la población a la vacunación”.

“Es muy importante que todos y todas completemos los esquemas. La vacunación es la forma más efectiva de evitar desarrollos graves de Coronavirus y tengamos presente los cuidados personales como el uso de barbijo, el lavado de manos, como así también, no compartir elementos personales ante este nuevo aumento de casos”, concluyó Torres.