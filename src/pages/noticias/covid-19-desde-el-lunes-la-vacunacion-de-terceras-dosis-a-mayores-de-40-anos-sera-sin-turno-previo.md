---
category: Agenda Ciudadana
date: 2022-01-28T17:22:42-03:00
thumbnail: https://assets.3dnoticias.com.ar/090121-vacunacion-sputnik.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3d noticias
resumen: 'Covid-19: Desde el lunes, la vacunación  de terceras dosis a mayores de
  40 años será sin turno previo '
title: 'Covid-19: Desde el lunes, la vacunación  de terceras dosis a mayores de 40
  años será sin turno previo '
entradilla: 'También podrán acceder embarazadas, junto a sus parejas, personal docente
  y de seguridad. Deben haber cumplido 4 meses de la aplicación de la segunda dosis. '

---
La ministra de Salud, Sonia Martorano, informó que a partir del próximo lunes 31 de enero las personas mayores de 40 años; las embarazadas, junto a su pareja; docentes y personal de seguridad, podrán acceder sin turno previo a su tercera dosis de vacuna contra el Covid-19.

En este sentido, la ministra precisó que “a partir del lunes se hace extensivo, a toda la provincia de Santa Fe, la vacunación libre con terceras dosis a mayores de 40 años; a las embarazadas, junto a su pareja; personal docente y de seguridad, siempre y cuando hayan cumplido 4 meses después de haberse colocado la segunda vacuna. Febrero va a ser el mes fuerte de terceras dosis".

A continuación Martorano detalló que “durante este mes de enero, la vacunación viene evolucionando de gran manera. Este mes se colocaron 750 mil dosis en la provincia, lo que nos permitió llegar al 84% de la población con esquema completo y el 28% de los ciudadanos y ciudadanas ya cuentan con su dosis de refuerzo”.

Asimismo, la titular de la cartera sanitaria recordó que “quienes han cursado la enfermedad, se les aconseja esperar 90 días y esto tiene que ver con que después de tener Covid, quedan anticuerpos en el organismo y tienen protección. Al colocar después la vacuna, lo que permite es tener los anticuerpos por mas tiempo”.

**ESCUELAS SEGURAS**  
Al ser consultada por el inicio de clases que se aproxima, la ministra de Salud señaló que “estamos preparándonos fuertemente para lo que se denomina escuelas seguras y para eso el primer punto es la vacunación. Cabe destacar que el 96% de los docentes tienen el esquema completo y el 72% ya tiene su tercera dosis. En cuanto a niños, niñas y adolescentes, la franja que va de 3 a 11 años, el 68% tiene dos dosis y en la franja de 12 a 17, el 81% tienen ambas vacunas. Esto nos va a garantizar poder iniciar las clases”.

“Además esta semana nos reunimos con la ministra de Educación, Adriana Cantero, para trabajar en las cuestiones de protocolos, ventilación, lavado de manos, barbijos y demás”, informó la funcionaria.

**DESCENTRALIZACIÓN DE LA VACUNACIÓN Y TESTEOS**  
Por otra parte, la ministra de Salud recordó la nueva estrategia que está llevando adelante la provincia en cuanto a la descentralización y expansión de los vacunatorios: “Ya se han habilitado en Santa Fe dos vacunatorios, uno en el Centro Unión Empleados de Comercio y el otro en la Asociación Trabajadores del Estado (ATE); y uno en Rosario en el sindicato Luz y Fuerza. Asimismo, estamos conversando con otros vacunatorios para que se sumen”, destacó.

A su vez, Martorano detalló la situación en cuanto a los testeos que se están realizando en la provincia. Al respecto precisó que “los turnos para ser hisopados en los centros de testeos se consiguen mediante la web provincial. Lo que estamos notando es que ha bajado la cantidad de personas que lo solicitan, lo cual tiene que ver con el momento epidemiológico”.

Además, la funcionaria aclaró que “cuando se dice que para los menores de 60 años no siempre es necesario el hisopado, no quiere decir que no deba concurrir a un centro de diagnóstico o al centro de salud, implica que se puede hacer un diagnóstico clínico, se hace un certificado y lo mas importante, se realiza el aislamiento necesario”.

“Todavía hay alta circulación de Ómicron lo cual implica que quien tenga un contacto estrecho con alguien que fue positivo y tenga los síntomas característicos de esta enfermedad como dolor de cabeza, dolor de garganta, fiebre, son signos de alarma para hacer la consulta y aislarse para cortar la cadena de contagios, porque esta variante afecta las vías aéreas superiores)”, señaló Martorano.

**SITUACIÓN DE CAMAS CRÍTICAS**  
Por último, la titular de la cartera sanitaria mencionó la situación por la que están atravesando las terapias intensivas de los hospitales públicos santafesinos.

En este sentido la ministra explicó que “los números de contagios pareciera que se están amesetando, generalmente a los 10 días de pasado el pico tenemos el aumento en las internaciones. Hay un nivel alto pero que no satura el sistema”.

“Hay muchas internaciones en camas generales y hoy tenemos un 70% en terapias intensivas pero no es todo Covid, el Coronavirus es entre el 12% y el 14%”, concluyó Martorano.