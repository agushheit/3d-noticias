---
category: Deportes
date: 2020-12-19T11:14:15Z
thumbnail: https://assets.3dnoticias.com.ar/1912-atletismo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Comenzó en Rosario el 100° Campeonato Nacional de Mayores de Atletismo «Braian
  Toledo»
title: Comenzó en Rosario el 100° Campeonato Nacional de Mayores de Atletismo «Braian
  Toledo»
entradilla: 'En el certamen, atletas de todo el país competirán entre el 18 y el 20
  de diciembre para lograr marcas que sumen puntos en la clasificatoria a los Juegos
  Olímpicos de Tokio 2021. '

---
El viernes 18 de diciembre, en la ciudad de Rosario, comenzó a desarrollarse al 100° Campeonato Nacional de Mayores «Braian Toledo», organizado por la Confederación Argentina de Atletismo, la Federación Santafesina de Atletismo, la Asociación Rosarina de Atletismo y el gobierno de la provincia.

El evento, que **tiene lugar en el Estadio Municipal Jorge Newbery** y se extenderá hasta el próximo 20 de diciembre, convoca a atletas de todo el país que buscarán alcanzar marcas que favorezcan su clasificación a los Juegos Olímpicos de Tokio 2021. 

**Se trata del primer certamen nacional de esta naturaleza**, y será realizado en el marco de estrictos protocolos indicados por las autoridades sanitarias nacionales y provinciales.

Acerca de la importancia del evento, la secretaria provincial de Deportes, Claudia Giaccone, expresó: «Es una alegría, en un año tan complejo, estar celebrando este campeonato nacional de atletismo en nuestra provincia. Esto demuestra que ha habido un trabajo previo, que no es casualidad. Santa Fe trabajó muchísimo para que el deporte se mantenga en pie, con todos los cuidados, con protocolos, desde aquel 8 de junio que abrimos las puertas de todas las instituciones para todas las disciplinas. Y cerrar este año con este certamen en nuestra provincia nos enorgullece y da cuenta del gran trabajo que venimos haciendo con la Federación Santafesina y con las asociaciones».

Giaccone, además, destacó el compromiso de la actividad deportiva santafesina con los cuidados sanitarios requeridos durante la pandemia: «El Ministerio de Salud de Santa Fe nos ha acompañado para darnos siempre los requisitos básicos para que esto no complique la situación sanitaria. El deporte cumplió, obedeció protocolos y permitió que las aperturas fueran in crescendo».

A su turno, Julio Nóbile, atleta especializado en lanzamiento de martillo que participará de la competición, destacó la realización del torneo: «Me estoy preparando para el año que viene, para los campeonatos sudamericano y panamericano. Estamos apuntando a eso. Se va a competir el nacional de mayores y lo dejaremos todo. La clave está en disfrutar y después las cosas se van dando solas. Soy todavía juvenil, voy a competir con los mayores y me interesa ganar experiencias en torneos».

## **EN HONOR A BRAIAN TOLEDO**

El certamen lleva el nombre del atleta olímpico Braian Toledo, quien falleció a los 26 años el pasado mes de febrero producto de un accidente de tránsito en Marcos Paz, su localidad natal. Toledo fue medalla de oro en los Juegos Olímpicos de la Juventud Singapur 2010 y finalista olímpico en Río 2016.

En ese marco, Giaccone se refirió al homenaje: «El sábado va a estar presente la mamá de Brian, y en el podio de jabalina vamos a estar reconociéndola, junto a la secretaria de Deportes de la Nación, Inés Arrondo».

## **TELEVISACIÓN**

La señal de TyC Sports será la encargada de emitir en directo, entre las 9 y las 11 horas los días viernes 18 y sábado 19. Y en diferido, entre las 13 y las 15 horas del domingo 20.

## **PARTICIPANTES**

La pista del Estadio Municipal Jorge Newbery recibirá atletas como Germán Chiaraviglio, Belén Casetta, Jennifer Dahlgren y Carlos Layol; y los maratonistas ya clasificados a Tokio 2021 Joaquín Arbe, Marcela Gómez y Eulalio Muñoz, además de los santafesinos que participaron en los Juegos Olímpicos de la Juventud Buenos Aires 2018 Julio Nóbile y Lazaro Bonora; y el histórico Adrián Marzo, en otros.