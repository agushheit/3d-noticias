---
category: Estado Real
date: 2021-03-04T00:15:42-03:00
thumbnail: https://assets.3dnoticias.com.ar/vacuna1.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: Comenzó la vacunación a los primeros 800 docentes en Rosario y Santa Fe
title: Comenzó la vacunación a los primeros 800 docentes en Rosario y Santa Fe
entradilla: Desde las 14hs del miércoles se recibieron a 400 docentes en cada ciudad.
  Hoy jueves desde las 9hs se prosigue en Rafaela, Reconquista y Venado Tuerto.

---
La provincia, a través del Ministerio de Salud, comenzó con el operativo de vacunación a docentes y no docentes de escuelas públicas y privadas de nivel inicial, 1°, 2° y 3° grado, y de escuelas especiales, con las dosis de la vacuna Sinopharm que arribaron ayer, especialmente para esta población.

“Por tratarse de una vacuna nueva y porque apenas ayer estuvieron disponibles en la provincia, quisimos empezar hoy por la tarde citando a 400 docentes en las ciudad de Rosario y a otros 400 en la ciudad de Santa Fe”, explicó inicialmente la ministra de Salud Sonia Martorano.

“De manera simultánea estamos sosteniendo tres grandes operativos de vacunación destinados a tres poblaciones distintas: Al personal de salud, que ya se vacunó casi en un 90 por ciento; a la población mayor de 70 y 80 años; y ahora llegó el turno de los trabajadores de la educación. Por eso queremos avanzar lo más rápido posible pero garantizando la seguridad y la eficiencia que este inédito despliegue de inmunización requiere”, agregó la ministra.

E informó Martorano: “Mañana desde las 9, en la totalidad de las cinco regiones sanitarias (Rosario, Venado Tuerto, Reconquista, Santa Fe y Rafaela), se iniciará el operativo global a docentes de modo tal de poder ir aplicando las primeras 37.800 dosis de la vacuna Sinopharm, enviadas el martes por el gobierno nacional”.

“Estamos realmente muy contentos de inmunizar a esta población específica para que puedan volver a las aulas paulatinamente; y reencontrarse con los niños. Ellos mismos nos transmiten su alegría y agradecimiento, ya a un año de haberse cumplido la detección del primer caso de Covid-19 en el país”, expresó en otro orden la ministra Sonia Martorano.

Y finalmente agradeció “el enorme esfuerzo de los equipos de vacunación”, tanto los provinciales como los pertenecientes a colegios profesionales, universidades públicas y privadas, e instituciones (Cruz Roja, entre otras).

En la ciudad capital

En Santa Fe, también a las 14 comenzó la vacunación a los primeros 400 trabajadores de la Educación, concretamente en el Centro de Educación Física 29 (CEF 29).

“Estamos asombrados, satisfechos y agradecidos porque la organización del operativo para docentes se hizo en menos de 24 horas. Y ellos comenzaron llegando en tiempo y forma, respetando sus turnos, de modo tal que a las 16 horas los equipos ya habían vacunado al 50 por ciento de la población que hoy citamos”, dijo, por su parte, el secretario de Salud Jorge Prieto.

“Es de destacar –agregó Prieto– el trabajo interministerial de modo tan coordinado, prácticamente no hubo espera entre que los docentes llegaron, se registraron y se los inoculó”.

Posteriormente, explicó que sobre el lugar de descenso hasta llegar al ingreso del estadio del CEF 29 (ciudad de Santa Fe) los docentes lo hacen mediante una carpa que debió climatizarse, dadas las condiciones de extremo calor imperantes en la ciudad, algo que la gente agradeció.

“Vamos intentando aprender y mejorar en cada operativo de vacunación –simultáneo y para distintas poblaciones– sobre la misma marcha en que acontecen; y conforme van llegando las vacunas al país y a la provincia, algo que la ministra Martorano viene señalando y explicando muy claramente”, aseguró el secretario de Salud.

Y concluyó Jorge Prieto: “Hoy cerramos otra jornada de inmunización para festejar. La emoción y el agradecimiento que los docentes le devuelven a los equipos que vienen trabajando sin descanso, es la mayor satisfacción al final del día”.

![](https://assets.3dnoticias.com.ar/vacuna.jpg)