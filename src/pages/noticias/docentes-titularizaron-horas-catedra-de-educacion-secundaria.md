---
category: Agenda Ciudadana
date: 2021-02-15T03:15:28-03:00
thumbnail: https://assets.3dnoticias.com.ar/titularizacion.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: Docentes titularizaron horas cátedra de educación secundaria
title: Docentes titularizaron horas cátedra de educación secundaria
entradilla: La provincia ya ofreció 542 cargos del concurso de secundaria.

---
La Provincia de Santa Fe titularizó, el 11 de febrero, 542 cargos del Nivel Secundario en un procedimiento por el cual desde diciembre de 2020, los docentes escalafonados mediante un novedoso proceso de ofrecimiento online, eligieron todas las vacantes de su interés para poder alcanzar la estabilidad laboral.

En un proceso totalmente institucionalizado y acorde a las normativas vigentes, se titularizaron cargos acorde a lo establecido por la resolución 1186/2019, previo paso por rigurosos controles de funcionalidad y de incompatibilidad que garantizan los concursos de titularización, acordados oportunamente con los sindicatos docentes.

Los ofrecimientos previstos en el concurso se están desarrollando de un modo único, innovador e inédito en nuestra provincia. Al respecto, el subsecretario de Educación Secundaria, Gregorio Vietto, señaló que “en una clara muestra de la decisión de la ministra Cantero y del gobernador Omar Perotti,  la provincia de Santa Fe ratifica con hechos su compromiso por seguir trabajando juntos en la consolidación de la carrera docente y ahora se prepara para titularizar más de 42.000 horas cátedra de Educación Secundaria, Técnica y de Educación Permanente de Jóvenes y Adultos”.

**EL CONCURSO**

Esta propuesta de titularización, innovadora e inédita en nuestra provincia e incluso en nuestro país, se basa en la misma lógica de los ofrecimientos presenciales, respetando los criterios definidos y aplicando procedimientos similares a los realizados habitualmente.

La diferencia fundamental radica en la elección online de las vacantes por parte de los aspirantes, a través de la plataforma Mi Legajo. En sus versiones anteriores éste concurso convocaba a miles de personas a un espacio único y reducido. Esta nueva variante garantiza el distanciamiento social y los cuidados sanitarios esenciales pues es un proceso totalmente a distancia: cada docente desde cualquier PC o dispositivo móvil podrá acceder al procedimiento sin ningún tipo de problema.

En este contexto, a partir del 22 de febrero se iniciará la titularización de 42.698 horas cátedra. Para ello los y las aspirantes con presencia en los escalafones en Materias deberán ingresar al Portal de Gestión Educativa y luego a Mi Legajo; y en el Menú Ofrecimiento deberán seleccionar la opción Titularización / Selección de Vacantes.

La metodología a implementar resulta similar a la realizada en los ofrecimientos presenciales, es decir, se ofrecerán en primer lugar las vacantes disponibles para los aspirantes escalafonados con Orden 1, luego a los de Orden 2, y así sucesivamente hasta el agotamiento de vacantes disponibles o la ausencia de aspirantes. Al finalizar cada número de orden se publicarán las vacantes remanentes en la web para ser conocidas por todos los demás docentes involucrados.

El sistema prevé que a través del nuevo módulo de Selección de Vacantes, los aspirantes podrán elegir todas las vacantes que sean de su interés y estén disponibles en función del escalafón correspondiente (en el cual se encuentran escalafonados) para el Orden habilitado.

Los siguientes días son los establecidos para la selección y confirmación de vacantes. En este punto es importante resaltar que el sistema estará disponible durante las 24 horas de los días indicados:

**>> ORDEN 1 A 5**

Orden 1 (2 días hábiles): 22/02/2021 al 23/02/2021

Orden 2 (2 días hábiles): 26/02/2021 al 01/03/2021

Orden 3 (2 días hábiles): 04/03/2021 al 05/03/2021

Orden 4 (2 días hábiles): 10/03/2021 al 11/03/2021

Orden 5 (2 días hábiles): 16/03/2021 al 17/03/2021

**>>> ORDEN 6 A 10**

Orden 6 (1 día hábil): 22/03/2021 al 22/03/2021

Orden 7 (1 día hábil): 25/03/2021 al 25/03/2021

Orden 8 (1 día hábil): 29/03/2021 al 29/03/2021

Orden 9 (1 día hábil): 31/03/2021 al 31/03/2021

Orden 10 (1 día hábil): 06/04/2021 al 06/04/2021

**>> ORDEN 11 AL 20**

Orden 11 al 20 (2 hábiles): 08/04/2021 al 09/04/2021

>> ORDEN 21 HASTA FINALIZAR ESCALAFONES

Orden 21 hasta el FINAL (2 días hábiles): 15/04/2021 al 16/04/2021