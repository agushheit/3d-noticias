---
category: Estado Real
date: 2021-07-19T06:00:26-03:00
thumbnail: https://assets.3dnoticias.com.ar/TECNOLOGICO.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Santa Fe acelera su desarrollo tecnológico
title: Santa Fe acelera su desarrollo tecnológico
entradilla: En un evento en el que participaron representantes del ecosistema de innovación
  santafesino, se presentó la convocatoria CDAT 2021 de la Agencia Santafesina de
  Ciencia, Tecnología e Innovación.

---
En el marco de la agenda de trabajo, la secretaría de Ciencia, Tecnología e Innovación del Ministerio de Producción, Ciencia y Tecnología presentó junto a representantes del sector, la convocatoria CDAT 2021. A través de la Agencia Santafesina de Ciencia, Tecnología e Innovación, se aportan 90 millones de pesos para Centros de Desarrollo y Aceleración de tecnologías.

Participaron del evento de manera semipresencial en el Centro Científico tecnológico y de manera virtual a través del canal de Youtube representantes de Centros de Desarrollo y Aceleración de tecnologías, emprendedores tecnológicos y representantes académicos del sector, quiénes conversaron sobre los desafíos y oportunidades que implica esta apuesta.

“Celebramos hoy esta nueva convocatoria, que armamos junto a todo el ecosistema santafesino para fortalecer los espacios de innovación. Realmente es algo que nos llena de orgullo. Es algo digno de destacar el hecho de generar sinergias público-privadas, esto nos permite tener los diagnósticos claros y poder conversar todos juntos”, manifestó en la presentación Marina Baima, secretaria de Ciencia, Tecnología e Innovación (SeCTeI).

“Agradecemos al gobernador Omar Perotti por confiar en este ecosistema, que ya venía impulsando y es algo realmente importante. En Santa Fe hay un gran potencial, por eso podemos trabajar, todos juntos, pensando y articulando, incluyendo a los jóvenes a quienes convocamos a que nos den sus propuestas y que se animen a dar el salto, nosotros vamos a estar siempre para acompañarlos”, remarcó la funcionaria.

Luego el Prof. Dr. Ing. Fernando Stefani disertó sobre: "Política Argentina en I+D, Resumen de Situación y Perspectivas". También se desarrolló un conversatorio para debatir acerca de la importancia de la existencia y mejora de los Centros de Desarrollo de Tecnologías para el desarrollo de las industrias de base tecnológica, que estuvo moderado por la Coordinadora de Proyectos de la SecTeI Micaela Mezzadra.

En el cierre, la Secretaria de Vinculación e Innovación del Instituto Balseiro, Dra. María Luz Martiarena disertó sobre “Conocimiento y desarrollo. Propuestas para avivar la llamar”.

Para ver las disertaciones completas se puede acceder aquí. [https://www.youtube.com/watch?v=3bS0EmnKp2w](https://www.youtube.com/watch?v=3bS0EmnKp2w "https://www.youtube.com/watch?v=3bS0EmnKp2w")

**Sobre la convocatoria**

CDAT 2021 “Ampliación de las capacidades para centros de desarrollo y aceleración de tecnologías en la provincia de Santa Fe”, fue creada con el objetivo de fortalecer e impulsar espacios destinados a brindar apoyo en la creación y consolidación de empresas de base tecnológicas para acelerar su crecimiento y viabilizar proyectos empresariales innovadores, a través de infraestructura tecnológica, apoyo técnico, comercial y administrativo.

Se pretende fomentar el desarrollo de un ecosistema innovador y dinámico que genere oportunidades significativas de promoción, desarrollo y transferencia o adopción de tecnologías emergentes, con inversión en infraestructura tecnológica y gestión del conocimiento, para producir alto impacto económico y social.

El cierre para la presentación en formato electrónico es el viernes 23 de julio. Más información sobre la convocatoria aquí.

**Presentes**

Participaron de manera presencial: Marcela Taín, subsecretaria de Gestión y Planificación de SeCTeI; Hugo Menzella, Director de IPROByQ CONICET; Esteban Lombardia, CEO de Terragene; Alejandro Vila, Director de la Incubadora de la Universidad Nacional de Rosario; Lucas Salvatierra, Director de la Inubadora de la Universidad Católica Argentina; Diego Zinggerling, por la Universidad Nacional de Rafaela; María José Soler, Directora de Endeavor Rosario; Ricardo Cassini, Director de Michroma; Marina Calleia, Directora de Innovación de la Municipalidad de Rosario; Matías Casalongue, Estudiante de Biotecnología de la Universidad Nacional de Rosario; y Marcos Mammarella, CTO de DeepAgro.

También acompañaron de manera virtual: Roxana Paez, IncuVA INTA; Laura Rodríguez de Sanctis, Bolsa de Comercio de Rosario; Ingrid Drago, Bolsa de Comercio de Rosario; Ignacio Sansevich, Polo Tecnológico Rosario / Zona I; Ezequiel Manavela Chiapero, CITES; Diego Zabala, Fundción Synergy; Hernán Castro, Aceleradora Glocal; Victoria Cerrano, Laboratorio de Innovación y Emprendimientos de la Universidad Austral; Maximiliano Benzecri, Aceleradora X4; Sebastian Lagorio, Xerendip; Romina Casadevall, Aceleradora GridX; y Luciana Gregoret, ARGENPIA Avellaneda.