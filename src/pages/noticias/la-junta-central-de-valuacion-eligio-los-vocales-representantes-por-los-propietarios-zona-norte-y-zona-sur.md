---
category: Estado Real
date: 2020-12-22T11:11:47Z
thumbnail: https://assets.3dnoticias.com.ar/2212sorteo.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: La Junta Central de Valuación eligió los vocales representantes por los propietarios
  zona Norte y zona Sur
title: La Junta Central de Valuación eligió los vocales representantes por los propietarios
  zona Norte y zona Sur
entradilla: La elección se realizó mediante sorteo. Cada vocal permanecerá en su cargo
  durante cuatro años.

---
El gobierno de la provincia, a través del Ministerio de Economía, llevó adelante este lunes una reunión de los funcionarios que integran la Junta Central de Valuación, encabezada por el administrador provincial del Servicio de Catastro e información Territorial, Héctor Rodríguez.

Durante el encuentro, se procedió al sorteo de los postulantes para integrar el Organismo como vocales representantes por los propietarios zona Norte y zona Sur. «Por Decreto en esta oportunidad debíamos convocar a martilleros y corredores inmobiliarios de acuerdo al procedimiento previsto, invitando a los colegios para que remitan un listado de corredores y martilleros para luego proceder a la elección», indicó Rodríguez.

Del sorteo, resultaron electos por la zona Norte Gerardo Owsianski (Martillero de la Ciudad de San Justo), en tanto que por la zona Sur, Bravo Natalia (Martillero de Ciudad de Rosario).

«Cada uno de los postulantes debía completar requisitos para poder participar. A partir de allí se presentó la nómina de personas que participaron del sorteo y se procedió a la elección», agregó.

Respecto a la labor que deberán llevar a cabo los vocales, el administrador provincial del Servicio de Catastro aseguró que «es intensa porque deben participar de decisiones en las que interviene la Junta, como por ejemplo cambio de zonas en el territorio provincial, tasaciones y valuaciones que hace el Estado para poder expropiar», fija los avalúos fiscales para el impuesto inmobiliario, aprueba los valores para electroductos.

Por último, Rodríguez indicó que cada vocal dura en su cargo cuatro años, momento en que vuelve a llamarse a convocatoria.

De la reunión participaron: el Administrador provincial de Impuestos, Martín Avalos; el jefe de Departamento Profesional Conservación del Suelo, Jorge Carbone; el director del IPEC, Gabriel Frontons; el intendente de Pérez, Pablo Corsalini; el presidente comunal de Arroyo Leyes, Eduardo Lorinz; el representante de las Comunas, Eduardo Gaya; el representante de los propietarios zona Norte, el Secretario de la Junta Central de Valuación Raúl Ybañez; el administrador Regional Santa Fe del servicio de Catastro, Alfredo Migone; el presidente del Colegio Profesional de Profesionales Martilleros y Corredores Públicos, Ramón Moreira; el presidente del Colegio de Corredores Inmobiliarios de Santa Fe 1° Circunscripción, Jorge Pighin y el presidente del Colegio de Corredores Inmobiliarios de Santa Fe, sede Rosario, Andrés Gariboldi.