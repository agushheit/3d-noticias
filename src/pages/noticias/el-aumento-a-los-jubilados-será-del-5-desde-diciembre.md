---
layout: Noticia con imagen
author: "Fuente: NA"
resumen: "Aumento a jubilados 5% "
category: Agenda Ciudadana
title: El aumento a los jubilados será del 5% desde diciembre
entradilla: El presidente Alberto Fernández confirmó que habrá un aumento para
  los jubilados en pleno debate por el proyecto de movilidad del oficialismo.
date: 2020-11-19T12:24:59.323Z
thumbnail: https://assets.3dnoticias.com.ar/jubilados.jpeg
---
El presidente Alberto Fernández aseguró que el Gobierno dará para fin de año "una corrección de las jubilaciones" y aseveró que la pretensión es que "no" queden "por debajo de la inflación". La cuestión se puso sobre la mesa a partir de la suspensión de la fórmula de ajuste mientras se debate una nueva de actualización automática. La misma según se supo en la tarde de este miércoles será del 5%.

"Somos conscientes de que tenemos que hacerlo. Lo estamos viendo, la idea es hacer un aumento para todos. Estamos trabajando en eso. Hay un aumento previsto para todos en diciembre", anticipó el mandatario en declaraciones formuladas esta mañana al canal A24.

Las declaraciones del Jefe de Estado llegan en momentos en que se debate la nueva fórmula previsional del Ejecutivo para regresar a la fórmula de cálculo de jubilaciones que planteaba la Ley 26417/2008.

## **Avanza en el Congreso la propuesta oficial de movilidad jubilatoria**

La fórmula tiene como antecedente directo la establecida por la Ley 26.417, que estuvo vigente desde 2008 hasta su derogación en 201,7 y sus elementos constitutivos básicos, los salarios y la recaudación (en un 50% cada uno).

Cabe remarcar que ahora la fórmula de actualización automática que se había modificado durante el gobierno de Mauricio Macri está suspendida, y los ajustes se hacen por decreto a la espera de que se establezca el nuevo mecanismo que recién regirá desde marzo de 2021.

Economía informó que la fórmula que se propone es "sustancialmente la misma a la sancionada en el 2008 y que resultó en una mejora sostenida en el poder adquisitivo de los jubilados y jubiladas hasta el 2015".

Se espera que la fórmula, que deberá comenzar a regir en el 2021, a diferencia de la sancionada en 2017, generará un incremento del poder adquisitivo de los haberes, gracias al aumento del salario real y de los recursos de ANSES.