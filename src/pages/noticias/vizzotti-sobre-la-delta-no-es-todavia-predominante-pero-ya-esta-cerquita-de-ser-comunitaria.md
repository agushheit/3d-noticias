---
category: Agenda Ciudadana
date: 2021-08-24T06:15:35-03:00
thumbnail: https://assets.3dnoticias.com.ar/VIZZOTTIDELTA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: 'Vizzotti, sobre la Delta: "No es todavía predominante pero ya está cerquita
  de ser comunitaria"'
title: 'Vizzotti, sobre la Delta: "No es todavía predominante pero ya está cerquita
  de ser comunitaria"'
entradilla: La ministra de Salud señaló que "por ahora la variante predominante en
  el país es la Manaos", y aseguró que se sigue "muy atentamente" la evolución de
  la circulación de la variante Delta.

---
La ministra de Salud, Carla Vizzotti, aseguró este lunes que si bien la circulación de la variante Delta de coronavirus "no es predominante" en la Argentina, "ya está cerquita de ser comunitaria".

"Se sigue vigilando la circulación, que a la fecha no es todavía predominante pero ya está cerquita de ser comunitaria", sostuvo Vizzotti en declaraciones formuladas esta mañana a Radio 10 desde Rusia, donde viajó junto a la asesora presidencial Cecilia Nicolini con el objetivo de profundizar el trabajo colaborativo en relación a la producción y provisión de vacunas con el Instituto Gamaleya.

La ministra de Salud explicó a qué se refiere el concepto de circulación "comunitaria" cuando se habla de la variante Delta o de cualquier otro virus o cepa.

"Cuando no se encuentra un vínculo entre los casos o con un viajero, se dice que es comunitaria, pero eso no significa que sea predominante", dijo.

Agregó, en ese sentido, que "por ahora la variante predominante en la Argentina es la Manaos".

Por eso, indicó que se sigue "muy atentamente" la evolución de la circulación de la variante Delta, que en Europa y otros países demostró ser más contagiosa que otras, mientras se continúa "aislando personas que tienen relación con los viajes" procedentes del exterior.

Vizzotti puntualizó que en Córdoba y en la Ciudad de Buenos Aires hay algunos casos que no tienen relación entre ellos y precisó que esto es "sobre todo en el caso de Córdoba, que no habían encontrado nexo con viajero", que fue el caso cero y que falleció este fin de semana.

La ministra confirmó también queel martes llegarán "sustancia activa, componente 1 y componente 2" de la vacuna Sputnik V "en el avión de Aerolíneas Argentinas" que regresa de Moscú, hacia donde partió el domingo.Al mismo tiempo precisó que en la actualidad"se superó el 60% del total de la población que inició el esquema de vacunación, y el 80% de los mayores de 18 años".

"Para esta semana se espera tener la liberación de ANMAT de alrededor de 600 mil dosis de componente 2" de la vacuna SputniK que elabora en argentina el laboratorio Richmond"

Respecto de los próximos arribos de vacunas, VIzzotti expresó que "para esta semana se espera tener la liberación de ANMAT de alrededor de 600 mil dosis de componente 2" de la vacuna SputniK que elabora en argentina el laboratorio Richmond", lista para ser distribuida y aplicada.

En tanto, agregó queel laboratorio AstraZeneca "anunció" al Gobierno nacional que "antes de fin de mes" se estarán "recibiendo 2,2 millones de dosis".

Vizzotti explicó que "se avanzó mucho en agosto" en el plan de vacunación,tal como el Gobierno nacional se lo había propuesto y detalló que se comenzó el mes "con 7 millones de segundas dosis aplicadas", alcanzando "los 11 millones y medio" en la actualidad.

"Esperamos aplicar dos millones y medio más en los próximos días que quedan para completar en agosto las 7 millones de segundas dosis que dijo el Presidente (Alberto Fernández)", amplió.

Apuntó que "se están usando las vacunas de Moderna para completar los esquemas, tanto de Sputnik como de AstraZeneca", y agregó que se sigue "generando evidencia junto con el Fondo Ruso en el estudio de intercambiabilidad" de vacunas de distintos laboratorios, para la segunda dosis.

Esto es para poder avanzar y completar el esquema de vacunación de quienes ya deben recibir la segunda dosis, sobre todo a los mayores de 50 años.

"Esperamos aplicar dos millones y medio más en los próximos días que quedan para completar en agosto las 7 millones de segundas dosis que dijo el Presidente Alberto Fernández"

La idea, dijo, es "ir bajando el intervalo de aplicación de 90 a 60 días en quienes iniciaron esquema con Sputnik V", además de darles la segunda dosis a quienes ya están en fecha de hacerlo o, incluso, superaron el período de 90 días.

"Seguimos aplicando ese millón y medio de dosis de Moderna que se destinaron a segundas dosis", añadió la titular de la cartera de Salud.

El martes llegarán "sustancia activa, componente 1 y componente 2" de la vacuna Sputnik V "en el avión de Aerolíneas Argentinas" que regresa de Moscú

El martes llegarán "sustancia activa, componente 1 y componente 2" de la vacuna Sputnik V "en el avión de Aerolíneas Argentinas" que regresa de Moscú

Acerca de vacunación de adolescentes, apuntó que son "pocos los países" que están vacunando a esa franja etaria y puntualizó que en Argentina empezarán a inmunizar "a los adolescentes priorizados justamente por el riesgo individual".

"Tenemos la perspectiva de tener novedades pronto en relación a la posibilidad de contar con vacunas para adolescentes. Antes de fin de año, sin lugar a dudas", adelantó la ministra de Salud.

En ese plano, dijo que "a menores de 12 años están vacunando en muy pocos países del mundo".

Mencionó a "China y Emiratos Árabes con las vacunas de Sinovac y Sinopharm", y a Chile que, dijo, "lo hará a partir de octubre".

"Estamos trabajando fuertemente, como lo hicimos siempre, para poder recabar la mayor información posible y definir si esa información es suficiente para que ANMAT nos recomiende utilizarlas", concluyó Vizzotti.