---
category: Agenda Ciudadana
date: 2021-10-01T06:00:47-03:00
thumbnail: https://assets.3dnoticias.com.ar/volvemis.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Cómo sacar el permiso de circulación para eventos masivos y poder ingresar
  a la cancha
title: Cómo sacar el permiso de circulación para eventos masivos y poder ingresar
  a la cancha
entradilla: El gobierno anunció un nuevo permiso para eventos masivos, deportivos
  y culturales. Cómo es el trámite que se vinculará de forma automática con la app
  "Cuidar".

---
El gobierno anunció la creación de un certificado de circulación para eventos masivos, deportivos y culturales. El mismo servirá para poder ingresar a los estadios de fútbol a partir de este fin de semana, cuando se habiliten los encuentros con un aforo del 50 por ciento.

La secretaria de Innovación del gobierno Nacional, Micaela Sánchez, explicó cómo será el esquema de validación de vacunación para poder efectivamente ingresar a los estadios.

Señaló que el mecanismo será "similar al que venimos llevando adelante desde el inicio de la pandemia y que está vinculado con los permisos de circulación que todos ya conocen".

Indicó que en la página argentina.gob.ar/volvemos va a estar a disponibilidad la tramitación de los permisos. Advirtió que "menores de 18 años pueden ingresar simplemente con la portación de la entrada, credencial o carnet. Los mayores tienen que realizar el permiso que es para eventos masivos".

Sánchez precisó que "se van a declarar datos personales y de que efectivamente recibieron al menos una dosis de la vacunación contra el Covid-19".

Explicó que en ese momento, el sistema valida en forma automática con el registro de vacunación del Ministerio de Salud de si efectivamente esa persona recibió al menos una dosis de vacunación contra el Covid-19".

De ser así, se genera el permiso de circulación y automáticamente queda vinculado con la aplicación cuidar que todos ya conocen; que además de la circulación para el trabajo tiene la circulación como por ejemplo desarrollar una actividad turística y tiene además la validación (hora) para asistir a eventos masivos".

"Con ese esquema quedaría resuelto el ingreso", definió la secretaria de Innovación del gobierno Nacional.