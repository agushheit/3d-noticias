---
category: La Ciudad
date: 2021-04-24T06:15:26-03:00
thumbnail: https://assets.3dnoticias.com.ar/HSPITAL.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Hospital de campaña frente al Cullen: "La próxima semana estará instalado
  y operativo"'
title: 'Hospital de campaña frente al Cullen: "La próxima semana estará instalado
  y operativo"'
entradilla: Así lo confirmó el ministro de Defensa, Agustín Rossi. Si bien no brindó
  fechas precisas, aseguró que "el hospital de campaña sumará camas comunes y de terapia
  intensiva.

---
Este viernes por la tarde, el ministro de Defensa, Agustín Rossi, confirmó que la instalación del hospital de campaña en el Liceo Militar General Belgrano, frente al hospital José María Cullen, se dará la próxima semana. Si bien no hay aún fechas precisas, el funcionario nacional sostuvo que la unidad médica militar, contará con camas comunes y de terapia intensiva con la posibilidad de agregar tubos de oxígeno.

"Vamos a utilizar la posibilidad y cercanía del Liceo Militar frente al Cullen para que el hospital de campaña funcione de manera articulada con el nosocomio. Consulté el pedido del gobernador Omar Perotti con el presidente de la Nación y le pareció una idea adecuada. La semana que viene ya va a estar instalado en la ciudad de Santa Fe y en condiciones de ser operado por el personal del hospital Cullen y por personal médico militar", aseguró Agustín Rossi en diálogo con radio LT10.

"La estrategia que tenemos es colocar los hospitales de campaña disponibles siempre en una situación en paralelo con alguna estación sanitaria existente, con el objetivo de aumentar la capacidad de atención médica de cualquier localidad", subrayó Rossi y continuó agregando: "Es un hospital que tiene camas comunes, de sala general y también de terapia intensiva que se le pueden agregar tubos de oxígeno. La idea es aumentar la capacidad sanitaria y que las mismas autoridades médicas del hospital Cullen determinen si pueden internarse pacientes no covid o covid leves con las necesarias medidas de aislamiento".

"Existirá personal médico de las Fuerzas Armadas pero el grueso de los especialistas que atiendan el hospital de campaña serán del Cullen, como también el suministros de insumos médicos. La operación y dirección del hospital de campaña deberá estar en manos de los directivos del Cullen", resaltó Agustín Rossi y finalizó: "Los hospitales de campaña están diseñados para instalarse en zonas inhóspitas en la atención de soldados. No hay que pensar en grandes lujos, pero sí en herramientas vitales para asistir y sumar en situaciones de emergencia a cualquier hospital con mayor equipamiento".

El presidente Alberto Fernández anunció este viernes en Rosario que un hospital de campaña del Ejército Argentino se levantará frente al Hospital Cullen, en el Liceo Militar, en la ciudad de Santa Fe.

En conferencia de prensa, Fernández manifestó: "Vamos a llevar a la ciudad de Santa Fe, para ayudar contra el Covid, el hospital de campaña del Ejército Argentino para que se instale frente al Liceo Militar y ayude a los tratamientos médicos, a la vacunación, al testeo; a todo lo que haga falta para que en Santa Fe aliviemos también el problema del Covid".

La posibilidad de montar un hospital de campaña había sido deslizada esta semana por el director del Cullen, Juan Pablo Poletti. "Hay hospitales del ejército de campaña, nosotros pondríamos el recurso humano; no podemos quedarnos de brazo cruzados y decir \`no tenemos más camas´. Hay que salir a buscar ideas".