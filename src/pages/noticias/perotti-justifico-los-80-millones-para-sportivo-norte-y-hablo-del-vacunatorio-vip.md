---
category: Agenda Ciudadana
date: 2021-06-16T09:09:29-03:00
thumbnail: https://assets.3dnoticias.com.ar/perottij.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de LT10
resumen: Perotti justificó los $80 millones para Sportivo Norte y habló del vacunatorio
  VIP
title: Perotti justificó los $80 millones para Sportivo Norte y habló del vacunatorio
  VIP
entradilla: El gobernador argumentó que ese club se encuentra en un sector muy carenciado
  de Rafaela. Además, negó una presunta vacunación VIP y sostuvo que "siempre puede
  haber errores". Sus declaraciones completas.

---
El gobernador Omar Perotti sorprendió este martes al responder en rueda de prensa sobre dos cuestiones candentes en la coyuntura política actual: la existencia de presuntos "vacunatorios VIP" en la provincia, y el otorgamiento de 80 millones de pesos por decreto al club Sportivo Norte de Rafaela, del que él es hincha.  

“Son temas que ‘alguien’ quiere instalar”, comenzó, en evidente alusión a algún actor de la oposición. Luego, resaltó lo hecho en materia de inoculación y el aumento de los hisopados: “pasamos el millón de personas vacunadas, y a tener testeadas 10 mil personas diarias; y estamos turnando a más de dos semanas consecutivas con la posibilidad de que cada santafesino y santafesina sepa hacia dónde marcha”.

“No existe ninguna situación de privilegio”, afirmó Sukerman

“Que puede haber errores, siempre puede haber errores”, admitió, pero aseguró que en Santa Fe “no existe una decisión de privilegio”. En ese sentido, recordó que él mismo “se vacunó cuando tuvo que vacunarse” y que “nadie del Gabinete se vacunó antes de que le toque”.

Asimismo, destacó: “frente a alguna denuncia infundada me presenté a la Justicia personalmente. Yo y mi familia fuimos todos sometidos a muestras para constatar que no nos habíamos vacunado. Ese es el valor que nosotros le damos a la vacunación”.

“El que no está inscripto no se vacunó en esta provincia y no se lo va a vacunar”, sentenció.

**Millonarios subsidios**

En cuanto a los 80 millones de pesos otorgados por decreto al club del que es hincha, Sportivo Norte de Rafaela -algo que le valió fuertes críticas desde la oposición y las quejas de las demás instituciones deportivas-, Perotti respondió haciendo un cuestionario al periodista que se lo preguntó:

—Perotti: ¿Conocés la ciudad (de Rafaela)?

—Periodista: Sí.

—Perotti: ¿Conocés el sector donde está el club (Sportivo Norte)?

—Periodista: Sí.

—Perotti: ¿Vos qué harías en ese lugar? Le darías a todos esos chicos, que no tienen a la redonda un lugar donde practicar un deporte, donde hacer patín, voley, básquet... (Un lugar) donde las escuelas poder encontrar un techo para poder hacer su actividad física... ¿Dudarías en construir algo?

—Periodista:...

—Perotti: Ahí está la respuesta, hermano.