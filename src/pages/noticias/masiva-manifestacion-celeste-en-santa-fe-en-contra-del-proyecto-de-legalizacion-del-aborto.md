---
category: La Ciudad
date: 2020-11-30T11:28:48Z
thumbnail: https://assets.3dnoticias.com.ar/pro-vida.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Masiva manifestación "celeste" en Santa Fe en contra del proyecto de legalización
  del aborto
title: Masiva manifestación "celeste" en Santa Fe en contra del proyecto de legalización
  del aborto
entradilla: Hubo una larga caravana de autos que culminó en una concentración frente
  a la Legislatura. El martes comienza a discutirse la iniciativa en el Congreso.

---
Una masiva manifestación "celeste" se congregó hoy frente a la legislatura de Santa Fe. Se trató de una movilización, que se replicó en varias provincias del país, en rechazo en rechazo al proyecto de legalización del aborto que comenzará a ser tratado el martes en la Cámara de Diputados.

La larga caravana de autos partió a las 17 de este sábado desde Estanislao Zeballos al 600. Luego, a las 18:30, se realizó una concentración y acto central sobre la explanada de la legislatura santafesina.

Con banderas argentinas y carteles, los manifestantes santafesinos se sumaron a una convocatoria realizada por diferentes organizaciones de la sociedad civil. Se trata de la primera acción del sector "pro vida" que nuevamente se vuelve a expresar en contra de la iniciativa impulsada por el gobierno nacional.

Con la etiqueta #LaMayoríaCeleste, la marcha era acompañada desde las redes sociales con posteos y publicaciones de manifestantes en distintas partes del país.

Entre las organizaciones convocantes se encuentra la Unidad Provida, que nuclea a más de 150 organizaciones de la sociedad civil. En un comunicado de prensa, advirtió sobre "la presión que sufren los profesionales de la salud, quienes cada vez más encuentran niños de edad gestacional avanzada abortados".

Las organizaciones subrayaron el momento "inoportuno" para enviar el proyecto, aún en plena pandemia de coronavirus

A nivel nacional, según los convocantes, hubo movilizaciones durante toda la jornada de hoy en 500 ciudades de todo el país, con modalidades como marchas y caravanas de autos y motos.

En Córdoba, los manifestantes se trasladaron en caravana por las calles de la capital; en Rosario fueron hasta el Monumento a la Bandera. En Mendoza, la concentración estuvo pactada en el Parque San Martín; en Tucumán, dos columnas partieron desde la Rotonda de Yerba Buena y el Parque 9 de Julio para llegar al Monumento del Bicentenario; y en Salta capital, la marcha partió desde el Monumento 20 de febrero en dirección a la estatua de Martín Miguel de Güemes.

"Hay más de 100 causas prioritarias por las que mueren las mujeres, y mucho más en pandemia. Mueren 15 veces más embarazadas que quieren tener a sus hijos, 20 veces más mujeres por desnutrición o HIV, y 300 veces más por cáncer de mama", sostuvo Ana Belén Mármora, activista de Unidad Provida en Buenos Aires.

![](https://assets.3dnoticias.com.ar/pro-vida1.jpg)

Desde la Comisión Episcopal para la Vida, los Laicos y la Familia, que encabeza monseñor Pedro Laxague, alentaron "fervientemente" a participar de la manifestación "a favor del derecho humano a la vida de toda persona garantizado en la misma Constitución nacional", según un comunicado.

En Capital Federal, por ejemplo, participaron de la marcha el arzobispo de Buenos Aires y cardenal primado de la Argentina, Mario Poli; y el obispo auxiliar Enrique Eguía Seguí y el vicario episcopal de las villas de la ciudad, Gustavo Carrara, entre otros obispos de la arquidiócesis porteña.

Horas antes de la marcha, desde el Episcopado advirtieron que la fachada de la catedral de Merlo-Moreno sufrió pintadas de color verde, que identifica a los partidarios de la legalización de la interrupción voluntaria del embarazo, y se dejó en la vereda una leyenda, también con pintura verde: "Ni Fernández ni Bergoglio. Aborto legal".

La Alianza Cristiana de Iglesias Evangélicas (Aciera), que nuclea a las comunidades evangélicas, también se sumó a la convocatoria en el Congreso, y su delegación estuvo encabezada por el vicepresidente de la entidad, el pastor Osvaldo Carnival.

La Cámara de Diputados abrirá la semana próxima el debate en comisiones del proyecto de Interrupción Voluntaria del Embarazo (IVE) con la presencia de funcionarios del Poder Ejecutivo Nacional y la exposición de unos 50 referentes (25 a favor y 25 en contra) tras lo cual buscará emitir dictamen de la iniciativa el viernes 4 de diciembre.

La intención de los diputados del Frente de Todos, según fuentes parlamentarias, apunta a aprobar en la segunda semana de diciembre el proyecto en la cámara baja y enviarlo luego al Senado para tener sancionada la ley antes de fin de año, ya en el marco de las sesiones extraordinarias del Congreso.

El martes, a las 9, se abrirá la discusión sobre el proyecto de legalización del aborto con las exposiciones que brindarán los ministros de Salud, Gines González García; de Desarrollo Social, Daniel Arroyo, y la secretaria Legal y Técnica de la Presidencia, Vilma Ibarra.

Tras los informes de los ministros, el mismo martes a las 14 comenzará la ronda de expositores -50 en total, 25 a favor y 25 en contra del proyecto de legalización del aborto -que tendrán 7 minutos cada uno al igual que en 2018- y la discusión continuará el miércoles durante toda la jornada.