---
category: La Ciudad
date: 2021-11-11T06:00:05-03:00
thumbnail: https://assets.3dnoticias.com.ar/ECOPUNTOS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Municipalidad habilitó dos ECO Puntos de Reciclado más en la ciudad
title: La Municipalidad habilitó dos ECO Puntos de Reciclado más en la ciudad
entradilla: Ahora se suman uno en la Estación Belgrano sobre Boulevard casi Dorrego;
  y otro, en el Parque Alberdi, en Rivadavia y Cortada Falucho.

---
Se trata de lugares de recepción de materiales reciclables y residuos especiales como pilas, diseñados especialmente para que los vecinos puedan disponer sus residuos domiciliarios y luego se puedan recuperar.

Es importante destacar que estos ECO Puntos de Reciclado contarán con atención personalizada, y estarán abiertos de lunes a domingos, de 9 a 19 horas. El que se instaló en la Costanera Oeste fue el primero y tiene una destacable aceptación por parte de los vecinos; y el próximo mes se sumarán dos más llegando a fin de año con cinco puntos en la ciudad. Se trata de una política pública que el intendente Emilio Jatón está fortaleciendo con el objetivo de promover conciencia ambiental, reutilizar materiales y descentralizar dicho servicio.

El secretario de Ambiente de la Municipalidad, Edgardo Seguro brindó más detalles y al respecto dijo: “Hace unas tres semanas comenzó a funcionar el de la Costanera y ahora habilitamos dos puntos más. El primero tiene una muy buena recepción. Estamos retirando material entre una y dos veces por día. Cuando empezamos a instalar el de la Estación Belgrano, la gente ya traía y nos dejaba material al costado para reciclar”.

En esta línea, agregó: “Estamos dando respuesta a algo que la gente requería, a tener más puntos en la ciudad para descartar material para reciclar. Esto se suma a las 17 islas con campanas que hay en diferentes espacios. En diciembre se sumarán dos ECO Puntos más. La intención es que los vecinos traigan a estos lugares los elementos secos y habrá una oficina de atención ambiental donde vamos a charlar con ellos, contarles sobre algunas campañas especiales que haya como de recolección de AEEs o de pilas, o de recepción de reciclados, entre otros”.

Antes de finalizar, Seguro hizo referencia a la campaña de recolección de pilas y dijo: “Desde que empezamos la campaña hace un poco más de un mes, ya juntamos unos 420 kilos. Fue bueno porque la gente tenía pilas acumuladas de muchos años. Y en estos ECO Puntos también se van a poder recibir aparatos eléctricos y electrónicos todos los días y pilas, algunos días a la semana que serán comunicados”.

**¿Qué materiales se reciben?**

Los ECO Puntos de Reciclado son contenedores en donde se reciben residuos reciclables como papel, cartón, plástico, metal, vidrio y telgopor; y cuando se juntan pilas en desuso serán las AA, AAA, prismáticas y botón. También tiene una ventanilla con atención personalizada donde se entregan semillas, plantines y chips para la huerta.

En cuanto a recolección de aparatos eléctricos y electrónicos se pueden llevar monitores, pantallas y aparatos con pantallas de superficie superior a los 100 cm2; pequeños electrodomésticos como planchas, secadores de cabello y pequeños ventiladores; y aparatos de informática y de telecomunicaciones pequeños como computadoras, mouse, pantalla, teclado, impresoras, copiadoras y teléfonos celulares con dimensiones exteriores que no superen los 50 cm.

Vale aclarar que todo este material recolectado es recepcionado por la asociación civil Dignidad y Vida Sana que trabaja en el Relleno Sanitario; los aparatos eléctricos y electrónicos son trasladados y procesados en un espacio situado en el complejo ambiental; y a las pilas se les da un tratamiento especial. “Y todo este material lo vamos a poner en valor para algunos emprendedores para que tengan la posibilidad de transformar estos materiales”, finalizó Edgardo Seguro.