---
category: Agenda Ciudadana
date: 2021-04-29T08:38:15-03:00
thumbnail: https://assets.3dnoticias.com.ar/amsafe.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: PREOCUPACIÓN ANTE LA FALTA DE VACUNACIÓN A DOCENTES DEL DEPARTAMENTO LA CAPITAL
title: PREOCUPACIÓN ANTE LA FALTA DE VACUNACIÓN A DOCENTES DEL DEPARTAMENTO LA CAPITAL
entradilla: Desde AMSAFE La Capital expresaron la preocupación ante la falta de vacunación
  de los docentes de Artística junto con todos los reemplazantes de los diferentes
  niveles y modalidades.

---
Desde AMSAFE La Capital expresaron la preocupación ante la falta de vacunación de los docentes que se encuentran desempeñando sus tareas pedagógicas frente a alumnos, pertenecientes a las Escuelas de Artes del departamento La Capital, tales como, ISCAA Nº 10 Instituto Superior de Cine y Artes Audiovisuales, Nº 9902 CREI, Nº 9900 Instituto Coral, Nº 3023 Escuela de Artes Visuales Juan Mantovani, Nº 3200 Escuela Provincial de Teatro, Nº 9901 Escuela de Música-Orquesta Juvenil y de Niños.

"Ante dicha situación exigimos al Gobierno que sean vacunadxs, de manera inmediata, lxs docentes de Artística junto con todxs lxs reemplazantes de los diferentes niveles y modalidades" sostuvieron a través de un comunicado oficial.

Desde AMSAFE La Capital informan que continuaran recepcionando las demandas de los afiliados que en esta situación sanitaria siguen sosteniendo la presencialidad. El reclamo se realiza comunicándose a la delegación a la que pertenecen ó a través de cada referente gremial.