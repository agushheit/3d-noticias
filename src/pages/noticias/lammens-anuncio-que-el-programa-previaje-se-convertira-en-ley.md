---
category: Agenda Ciudadana
date: 2021-12-05T06:15:08-03:00
thumbnail: https://assets.3dnoticias.com.ar/LAMMNESPRRE.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Lammens anunció que el programa Previaje se convertirá en ley
title: Lammens anunció que el programa Previaje se convertirá en ley
entradilla: En el marco de la inauguración de la FIT 2021, el ministro de Turismo
  y Deportes aseguró que el Gobierno impulsa la conversión del plan en una política
  de Estado.

---
El ministro de Turismo y Deportes, Matías Lammens, formuló este sábado anuncios de coyuntura y de fondo para el sector en su discurso inaugural de la FIT 2021, como el llamado a una mesa de trabajo para analizar la cancelación de ventas de pasajes al exterior en cuotas, la conversión del Plan Previaje en una política de Estado y el objetivo de crear 250 mil puestos de trabajo en turismo.  
  
Entre los planes del Gobierno, Lammens destacó al Previaje, que en su segunda etapa "quintuplicó los números de la primera edición y creo que vamos a terminar el año por encima de los 60.000 millones de pesos contra los 10.000 del año pasado".

> "Queremos que sea una ley muy robusta, que deje a Previaje como una política de Estado, porque previaje fue la política pública más importante de la historia en términos de turismo"Matias Lammens

  
Sobre la iniciativa de convertir este plan en ley, anunció que presentarán el proyecto "a cada uno de los gobernadores, a cada una de las cámaras de las provincias, la estamos trabajando con Gustavo (Hani); en esta semana o la que viene vamos a estar con todas las cámaras recibiendo la jefe de Gabinete (Juan Manzur), para que venga a la puerta del sector privado".  
  
"Queremos que sea una ley muy robusta, que deje a Previaje como una política de Estado, porque previaje fue la política pública más importante de la historia en términos de turismo", aseguró.  
  
Mediante esa ley, siguió, "queremos que tengan diferentes exenciones impositivas quienes vengan a invertir en turismo, la psibilidad de un bono fiscal que les reconozca esas inversiones, queremos un diferimiento de las contribuciones patronales".  
  
Lammens dijo que, "en definitiva, estamos buscando que cada vez pese más, que el porcentaje del PBI vinculado a la actividad de turismo sea mayor" y agregó que "terminamos con 10 puntos en 2019; buscamos que sea de 13 a 14 puntos y generar en los próximos años 250.000 puestos de trabajo".  
  
"Entiendo las preocupaciones de una parte del sector con lo que pasó con los pasajes en cuotas", expresó Lammens luego que en el discurso previo Gustao Hani, presidente de la FIT 2021, de Faevyt y la CAM, reclamara "reglas claras" y previsibilidad en la toma de decisiones desde el Gobierno respecto a la cancelación de la venta de pasajes al exterior en cuotas.  
  
Tras asegurar que "vamos a trabajar", señaló que al recibir los primeros reclamos, "rápidamente se convocó a una mesa de trabajo, que no tengo dudas que va a dar resultados".

> "El sector turístico y la Argentina tiene que definitivamente desarrollar una burguesía nacional que priorice los intereses de la Argentina por encima de los personales"Matias Lammens

Lammens explicó que quienes conducen el país "tenemos responsabilidades en el gobierno nacional, que es cuidar los dólares, cuidar las reservas que tenemos, porque para seguir creciendo Argentina las necesita para tener un crecimiento sostenido pero también entendemos que el turismo es una de las industrias que nos va a ayudar en ese crecimiento, en la generación de divisas".  
  
"Entendemos también que necesitamos conectividad y para que esa conectividad exista, que los vuelos vengan, tienen que ir y venir con turistas de los dos lados, así que vamos a trabajar para buscarle una solución a eso, es el compromiso que asumimos con Gustavo (Hani) y con todas las cámaras", acotó..  
  
El ministro aclaró que "el sector turístico y la Argentina tiene que definitivamente desarrollar una burguesía nacional que priorice los intereses de la Argentina por encima de los personales; es muy difícil pero aquellos países que los tienen son los que han logrado un desarrollo importante, que tienen niveles de desempleo marginales y que no tienen prácticamente pobreza".  
  
Lammens explicó que "si bien el turismo es una de las principales fuentes de ingresos de divisas, también es verdad que para un país que tiene un problema de restricción interna, como Argentina, que necesita los dólares para seguir creciendo, necesita dar vuelta el déficit en la cuenta de viajes que tuvo en los últimos años", algo que "es un problema y creemos que tenemos una gran oportunidad para revertirlo".  
  
También recordó que debido a la pandemia "debimos dar un golpe de timón que tuvo que ver con el presupuesto, y reconvertir todo lo que teníamos para promoción y ponerlo en cada una de las empresas, sobre todo las pymes, que son el 96% de las empresas del sector, para sostener esos trabajos del sector, más de un millón en todo el país".  
  
Luego enumeró los planes puestos en práctica en ese marco como el de Asistencia al Trabajo y la Producción, los Repro y otros del Minturdep, con financiamiento internacional, que sumaron "solamente el año pasado más de 60.000 millones de pesos en el sector, que fue uno de los sectores que más ayudó el Gobierno", al cual consideran "un sector estratégico para el desarrollo de la economía de los próximos años".