---
category: Agenda Ciudadana
date: 2021-07-13T08:24:29-03:00
thumbnail: https://assets.3dnoticias.com.ar/pepe-flor-sole.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: 'José “Pepe” Roura: “Reprensentamos los mejores valores de Juntos por el
  Cambio”'
title: 'José “Pepe” Roura: “Reprensentamos los mejores valores de Juntos por el Cambio”'
entradilla: “Sabemos que Santo Tomé puede cambiar, pero ese cambio debe ir acompañado
  de planificación, experiencia, profesionalismo y fundamentalmente del trabajo junto
  a los vecinos de la ciudad."

---
El candidato a concejal de Juntos por el Cambio José “Pepe” Roura brindó detalles de las personas que lo acompañan en la lista para las próximas elecciones del 12 de septiembre. “Estamos muy contentos con el equipo que conformamos, porque se trata de santotomesinos y santotomesinas que aman su ciudad, pero además son todos profesionales de  la UNL, y referentes y expertos en la tarea que realizan. Por eso estamos convencidos que logramos en esta lista representar los mejores valores de Juntos por el Cambio y la mejor opción para Santo Tomé”, afirmó. 

La lista “Santo Tomé puede cambiar”, se encuentra integrada por Soledad Santa Cruz que es Terapista Ocupacional diplomada en Género y desde hace años trabaja en la búsqueda de oportunidades laborales y la capacitación de jóvenes. Además, forma parte del equipo del equipo de Florencia González y juntas trabajaron en el relevamiento nutricional de niños y niñas de la ciudad, y en talleres de empoderamiento para mujeres. 

“También nos acompañan Osvaldo Perone y Mariana Marozzi, dos médicos veterinarios comprometidos con la tenencia responsable de mascotas y  el proteccionismo. Como así también Pablo López, un médico de 29 años que realizó su práctica profesional en el centro de salud de barrio Iriondo y trabajó en la guardia del Samco durante la pandemia”, agregó José “Pepe” Roura acerca de quienes conforman la lista que encabeza. 

“Hemos decidido dejar de ser meros espectadores viendo el progreso de las localidades que nos rodean, trabajar junto a los vecinos e instituciones, escucharnos y pensar la ciudad de los próximos 40 o 50 años, desde la formación y la experiencia que tenemos en distintos temas que hacen al futuro de Santo Tomé: planeamiento urbano, transporte, calidad de vida de nuestros niños, acceso al trabajo para los jóvenes y cuidado responsable de nuestros animales”, afirmó.