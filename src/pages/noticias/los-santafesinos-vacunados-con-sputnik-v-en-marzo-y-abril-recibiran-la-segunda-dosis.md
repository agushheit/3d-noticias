---
category: Agenda Ciudadana
date: 2021-07-19T06:15:45-03:00
thumbnail: https://assets.3dnoticias.com.ar/SPUTNIK.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Los santafesinos vacunados con Sputnik V en marzo y abril recibirán la segunda
  dosis
title: Los santafesinos vacunados con Sputnik V en marzo y abril recibirán la segunda
  dosis
entradilla: Según se informó desde el Ministerio de Salud provincial, recibirán la
  segunda dosis en los próximos días. Sigue la vacunación a menores de 30 años.

---
Desde el Ministerio de Salud de la provincia de Santa Fe se continúa con el operativo de vacunación y según informaron entre este fin de semana y la semana que comienza, quienes hayan recibido la primera dosis de Sputnik V entre fines de marzo y principios de abril, serán llamados a vacunarse con el segundo componente.

La ministra de Salud, Sonia Martorano, confirmó que las personas que se hayan vacunado con Sputnik V contra el coronavirus hasta el 6 de abril serán las convocadas para recibir la segunda dosis y además destacó el avance de la campaña y descartó que haya una mayoría de “antivacunas” entre los más jóvenes.

La ministra confirmó que de los santafesinos inscriptos para la vacunación contra el Covid-19, un 1.800.000 ya recibieron su primera dosis, con lo cual se espera que “para fin de julio se haya colocado la totalidad de primeras dosis y avanzando con la segunda dosis”.

Consultada sobre las segundas dosis que la población de mayores de 70 años espera con ansiedad –sobre todo de cara a la llegada de la variante Delta que exige las dos dosis, tal cual apuntó Martorano–, explicó: “Estamos con tres vacunas, Sinopharm, AstraZeneca y Sputnik, las tres muy buenas. La Sinopharm se aplica en su segunda dosis a las cuatro semanas, las dos dosis son iguales y tenemos. La AstraZeneca estaba en 12 semanas –entre una y otra dosis– pero con la variante Delta la recomendación es bajarla a ocho semanas, pero las dos son iguales. Entonces la Sputnik es un cuello de botella”.

Tras recordar que la primera dosis de la vacuna rusa tiene el 77 por ciento de cobertura, admitió: “No están llegando las segundas dosis, es un componente diferente”. Luego, en ese marco anunció que este fin de semana serán convocados quienes se hayan vacunado con esa droga hasta el 6 de abril.

“Dependemos de la llegada (de las Sputnik) pero se empieza, en breve, a producir en Argentina y no va a permitir avanzar”, destacó como signos positivos de avance. También recordó que se analiza la combinación de vacunas.

Otra buena nueva es el arribo de la vacuna Moderna desde Estados Unidos: “Puede darse en personas de entre 12 a 17 años, y hay que ver si es probable que se pueda combinar”, apuntó.