---
category: La Ciudad
date: 2021-06-28T06:00:33-03:00
thumbnail: https://assets.3dnoticias.com.ar/vacunatorio.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Se extiende el horario del vacunatorio en Santo Tomé
title: Se extiende el horario del vacunatorio en Santo Tomé
entradilla: Ante la cantidad de turnos récord asignados para el lunes, se decidió
  extender el horario para evitar aglomeraciones. Información importante para quienes
  ya recibieron su turno.

---
De acuerdo a lo informado por la coordinación del Centro de Vacunación Covid de Santo Tomé, este lunes 28 de junio se asignaron 1.000 turnos para la aplicación de dosis, una cantidad récord desde el inicio de la campaña.

Los mismos fueron distribuidos por el sistema de asignación de turnos provincial entre las 8.30 y las 13 horas. No obstante, con el objetivo de evitar posibles aglomeraciones y facilitar la atención de los vecinos y vecinas, el vacunatorio extenderá su horario de atención hasta las 15.

De esta manera, las personas con turnos asignados también podrán presentarse entre las 13 y las 15 horas para recibir su dosis, independientemente de la notificación que hayan recibido a través del correo electrónico o por mensaje de texto.

Teniendo en cuenta que la cantidad de turnos programados es prácticamente el doble de lo habitual para el lapso de tiempo asignado, solicitamos a los asistentes respetar el distanciamiento social y las medidas de prevención, así como también, la comprensión del caso por las incomodidades que esta situación pudiera causar.