---
category: La Ciudad
date: 2021-05-03T06:00:57-03:00
thumbnail: https://assets.3dnoticias.com.ar/ESPACIOS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: '"Justos y Protectores de todos los días" lanza su primera convocatoria a
  participar'
title: '"Justos y Protectores de todos los días" lanza su primera convocatoria a participar'
entradilla: Es una propuesta virtual que busca el abordaje correcto a las situaciones
  de violencia.

---
"Espacios Educativos" creó esta propuesta educativa mayoritariamente virtual, que, mediante recursos y herramientas didácticas, permite sensibilizar y concientizar acerca de la necesaria acción que se debe asumir en los abordajes de las situaciones de violencia, fundamentalmente a través de la posición en el rol del Protector.

Las inscripciones están abiertas y cualquier información de este curso virtual puede solicitarse a contactos@espacioseducativos.ar o al teléfono 0342 155 910865. Comenzará el sábado 29 de mayo y tendrá una duración de diez semanas en las que se desarrollarán encuentros sincrónicos y asincrónicos.

**Sensibilizarnos para proteger**

Leonardo Simoniello, coordinador de Espacios Educativos, dio precisiones de este nuevo lanzamiento y manifestó que "la propuesta aborda recursos, conceptos y relatos históricos que permitirán visualizar distintos hechos de violencia ocurridos en el mundo y en la historia, y los roles ocupados por las personas, grupos o entidades en los mismos, haciendo énfasis en quienes ocuparon un rol de protección. Creemos que las situaciones de violencia comprenden cuatro roles diferentes (la víctima, el victimario, el observador pasivo y el observador activo o protector) y esta propuesta pone el énfasis en este último".

El curso está dirigido a educadores, formadores y a todo aquel que pretenda involucrarse en la temática y las herramientas pedagógicas y digitales brindadas a lo largo del cursado. Las mismas pueden ser utilizadas para replicar en el aula o en grupos en donde se trabaje.

El objetivo del espacio es reflexionar acerca de las justicias, las injusticias y las situaciones de violencia en nuestros días, desde las estructurales hasta las domésticas o grupales de la cotidianeidad, y a la vez brindar herramientas que contribuyan a generar transformaciones en las y los participantes del proyecto, a través del redimensionamiento del espacio de la protección.

En las sociedades actuales, el observador pasivo ha ganado un "lugar especial", y fue ocupando espacios cada vez mayores. Es necesario reflexionar para poder revertir esta postura, y para que cada vez sean más quienes decidan actuar para dejar de ser simples observadores de situaciones que deben ser modificadas, evitadas y repudiadas.

**Líneas de trabajo y organización del curso**

La capacitación se desarrolla en cuatro módulos. Los tres primeros se brindan a través de una plataforma digital (Classroom), en la que se abordan las temáticas mediante recursos audiovisuales y teórico-prácticos. El cuarto consta de una jornada presencial, mediante la visita al dispositivo Surgir, creado también por Espacios Educativos.

Inicialmente es necesario ofrecer a los participantes un espacio de "nivelación" para quienes no estén familiarizados con el uso de las plataformas digitales utilizadas, ya que el curso ofrece una variedad de herramientas interactivas, novedosas e innovadoras para la "educación digital".

En el módulo 1 se aborda la importancia de conocer las violencias en la historia, partiendo de conceptos y definiciones para hablar de las violencias, el genocidio contra los armenios y genocidio nazi, los crímenes de persecución política, de persecución racial y de guerra, y las violencias sistemáticas y persistentes contra los Pueblos Indígenas, terminando con una reflexión sobre un mapamundi de violencias.

En el módulo 2 se vincula a los espacios de protección y las personas protectoras, trabajando inicialmente con los Justos de las Naciones y otros protectores, la comunidad Internacional protectora, la protección a nivel de los Estados, de la Sociedad Civil y las personas protectoras, finalizando con una reflexión en base a un mapamundi de la protección.

El módulo 3 se aborda a la justicia como factor de protección, relacionándola con la democracia, la participación y las acciones de protección realizadas por las y los vecinos de las comunidades, desarrollando conceptos vinculados con la resistencia no violenta, la Teoría del Observador y la cultura de paz.

El módulo 4 es la visita al espacio Surgir, en la que se retoma y analiza los acontecimientos revisados en los tres módulos anteriores, en relación a la teoría de los cuatro roles, particularmente en cuanto al rol de la protección, a partir de dispositivos y propuestas lúdicas. El propósito de este módulo es poder dimensionar los roles que se ocuparon y que cada quien ocupa en diferentes situaciones de violencia, procurando hacer énfasis en la observación pasiva, y su transformación en protección.

**Destinatarios**

Con referencia a las personas o grupos para quien se pensó esta propuesta, Leonardo Simoniello manifestó: "La capacitación está dirigida básicamente para personas interesadas en las temáticas que se abordan, docentes, responsables, facilitadores, tutores, referentes de jóvenes, adultos y adultos mayores. Los grupos o destinatarios indirectos son los grupos humanos pertenecientes a instituciones educativas, organizaciones sociales, espacios culturales, deportivos, literarios. Es decir, 'Justos y Protectores de todos los días' es una herramienta para multiplicar sus objetivos en la sociedad de hoy".