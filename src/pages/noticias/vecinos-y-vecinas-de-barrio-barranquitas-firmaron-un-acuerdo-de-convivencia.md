---
layout: Noticia con imagen
author: .
resumen: Acuerdo de convivencia
category: La Ciudad
title: Vecinos y vecinas de barrio Barranquitas firmaron un acuerdo de convivencia
entradilla: Culminaron los talleres de promoción de derechos, organizados por la
  Municipalidad, para los habitantes de los bloques. En la oportunidad, se
  rubricó el convenio que establece pautas para mejorar la convivencia.
date: 2020-11-05T13:22:16.985Z
thumbnail: https://assets.3dnoticias.com.ar/barranquitas.jpg
---
Concluyeron los talleres de Promoción de derechos y vinculación ciudadana brindados por la Municipalidad a los vecinos y vecinas que residen en las 29 viviendas de las torres I y II de barrio Barranquitas. Con el objetivo de fortalecer los lazos ciudadanos y la resolución pacífica de los conflictos, los habitantes del complejo firmaron un acuerdo de convivencia. Se trata de una serie de normas para lograr una buena vecindad que, además, figuran en las carteleras ubicadas en los ingresos de cada edificio.

Desde el principio, se mantuvo como premisa la promoción de los derechos ciudadanos y el ejercicio de los mismos, con la intención de establecer buenas prácticas de convivencia, por medio de la escucha activa y la resolución pacífica de los conflictos. Para ello se brindaron herramientas para el uso y el cuidado de los espacios comunes de las torres y sus alrededores.

Para iniciar el trabajo, el municipio explicó a los habitantes las tareas a realizar en los talleres de vinculación. Posteriormente se identificaron los principales problemas y se elaboró un código con normas y obligaciones. Durante toda la experiencia, los habitantes demostraron su interés y compromiso, concurriendo a cada uno de los encuentros fijados y participando activamente en los debates.

Los agentes municipales, por su parte, tuvieron a su cargo las explicaciones respecto de las competencias de las diferentes áreas: ruidos molestos, mascotas, acción social y cuidados, hábitat, alumbrado público e higiene, entre otros.

![](https://assets.3dnoticias.com.ar/barranquitas2.jpg)

**Protagonistas**

El trabajo colaborativo, propiciado a través de la Dirección municipal de Derechos y Vinculación Ciudadana, se enmarca en una labor conjunta entre esa área, la Agencia de Hábitat y el Centro Municipal de Distrito, las cuales acompañaron a vecinos y vecinas durante todo el proceso.

El director de Derechos y Vinculación Ciudadana, Franco Ponce de León, explicó el proceso encarado con los residentes -que finalizó el sábado pasado-, y destacó la importancia de que ellos mismos, “a través de los talleres, se pudieran poner de acuerdo en bases mínimas que se deben generar para la convivencia”. Según detalló, “el objetivo de estos talleres, tal como nos solicitó el intendente Emilio Jatón, era estar cerca de los vecinos y las vecinas para ayudarlos y ayudarlas a que juntos puedan generar ese lazo, esa comunión para vivir en una casa grande, como este edificio”.

En ese marco, subrayó que entre los propios habitantes “fueron surgiendo planteos para respetar los horarios, por ejemplo, para la música. Lo importante es que ellos y ellas pudieron ponerse de acuerdo y, a través de la firma del acuerdo, comprometerse a respetar estos puntos”.

Además, recordó que quienes habitan los bloques “son personas que vienen de diferentes barrios de la capital, lo que genera diferencias porque provienen de distintos ámbitos y tienen historias e idiosincrasias propias de cada lugar”. Por eso, la premisa del municipio fue “a través de los talleres, buscar la unión, el hilo conductor que genere las bases para convivir”.

Verónica Valenzuela, una de las vecinas que participó de la actividad, recordó que “hace dos meses venimos trabajando con la gente de la Municipalidad acerca de las normas de convivencia y cómo podíamos colaborar entre los vecinos para mejorarla”. Tal como explicó, “fuimos generando estas normas y ahora las podemos implementar, colocando los carteles para respetar los derechos del otro”.

Otra de las habitantes de las torres, Susana Casafuti, aseguró que los talleres “nos sirvieron de mucho para comunicarnos entre los vecinos”. Ella residía anteriormente en barrio Don Bosco, por lo que consideró que habitar ahora en un edificio “es nuevo para mí y tenemos que acostumbrarnos, para estar bien entre todos”.

Por último, Jesica Villalba, también residente de los bloques, afirmó que las capacitaciones “solucionaron algo porque no había un acuerdo y la mayoría de los que vivimos acá nunca tuvimos que convivir en un edificio”. A modo de ejemplo, contó que se estableció una organización para la limpieza de los espacios comunes, dividiendo a los habitantes en grupos. Ese cronograma de tareas se escribe en las carteleras ubicadas en los ingresos a las torres, fomentando la comunicación de todos los habitantes.

![](https://assets.3dnoticias.com.ar/barranquitas1.jpg)