---
category: La Ciudad
date: 2020-12-05T12:33:05Z
thumbnail: https://assets.3dnoticias.com.ar/deportes.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Espectáculos y actividades deportivas para el fin de semana largo
title: Espectáculos y actividades deportivas para el fin de semana largo
entradilla: 'La Municipalidad diagramó una serie de actividades que incluyen un circo
  recorriendo el Parque del Sur, ferias y música en vivo. '

---
Poco a poco el arte y las actividades deportivas vuelven a las calles, las playas y los espacios abiertos. En ese sentido, la primera semana de diciembre, la Municipalidad diagramó una agenda de actividades para disfrutar del Verano Capital con los protocolos de prevención necesarios.

En cuanto a las propuestas artísticas, hoy -viernes 4 de diciembre- se realizarán actividades en Colastiné Norte. 

Desde las 19, en la Plaza del barrio Las Paltas, se podrá disfrutar de las variadas ofertas que propone la Feria de Colastiné Norte. 

Paralelamente, se podrá disfrutar de la música en vivo de Limonada Latina. 

Cabe aclarar que la Municipalidad contempla un dispositivo de distanciamiento para que las vecinas y vecinos puedan volver a los espectáculos de música en vivo con los protocolos adecuados.

## **Sábado y domingo**

De igual manera se pensaron las propuestas para el fin de semana. 

En primer lugar, el sábado, a las 19, se llevará a cabo una Feria de Emprendedores de Capital Activa. Será a las 19, en el estacionamiento de la Estación Belgrano, ubicado en calle Avellaneda. 

En ese marco, la propuesta musical, con protocolos de distanciamiento, estará a cargo de Janise.

En tanto, el domingo, a las 17.30, en el Parque del Sur, se podrá disfrutar de la presentación del Pequeño Circo Ambulante. 

La original propuesta será con números circenses de los artistas que se llevarán a cabo recorriendo el emblemático espacio verde de la ciudad.

## **Deportes al aire libre**

Con respecto a las propuestas deportivas para esta nueva edición de Verano Capital, durante el fin de semana largo se dispusieron de cuatro espacios. 

Se trata de las sedes ubicadas en las playas de la Costanera Este, la del Centro de Deportes de la Costanera Oeste y las del Espigón I y II.

Desde el viernes y hasta el martes, de 10 a 12 y de 17 a 20 horas, se podrán realizar las siguientes actividades: juegos recreativos, beach vóley, beach rugby, beach handball, beach fútbol, beach tenis, funcional y ritmos. 

Cabe señalar que, además de los 90 guardavidas que supervisarán las playas, la Municipalidad dispuso la presencia de más de 100 profesores de Educación Física que estarán a cargo de las actividades deportivas y recreativas.