---
category: El Campo
date: 2021-06-04T08:20:50-03:00
thumbnail: https://assets.3dnoticias.com.ar/sra.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Sociedad Rural Argentina
resumen: Se conformó la nueva Comisión Directiva de la Sociedad Rural Argentina
title: Se conformó la nueva Comisión Directiva de la Sociedad Rural Argentina
entradilla: En la Asamblea General Ordinaria de socios de la Sociedad Rural Argentina
  (SRA), celebrada el 31 de mayo, Nicolás Pino fue elegido nuevo presidente de la
  entidad. Estará acompañado por Marcos Pereda como vicepresidente.

---
En la elección votaron 1627 socios y, a pesar de las restricciones vigentes con motivo de la pandemia, a la Asamblea presencial asistieron alrededor de 200 personas. Durante la misma, se entregaron las medallas a los Socios Honorarios, a los que cumplieron las bodas de oro con la SRA y a los Socios Vitalicios.

Nicolás Pino nació en Buenos Aires, tiene 55 años, está casado y tiene dos hijas. Es técnico en Producción Agropecuaria en la Universidad Católica Argentina, se desempeña como productor agropecuario y comercializador de carnes. Realiza actividades de cría, recría, engorde y genética de animales en dos establecimientos ubicados en las provincias de Buenos Aires y Entre Ríos.

En su trayectoria en la entidad integró la Comisión Directiva con los cargos de secretario y vicepresidente. También participó en la Fundación Sociedad Rural Argentina como responsable productivo del Colegio Agropecuario de Realicó (La Pampa). Además, fue secretario del Comisariato General, comisario general de la Exposición Rural y vicepresidente de La Rural S.A.

De este modo, la nueva Comisión Directiva queda conformada de la siguiente manera:

[http://www.sra.org.ar/wp-content/uploads/2021/06/CD-SRA.pdf](http://www.sra.org.ar/wp-content/uploads/2021/06/CD-SRA.pdf "http://www.sra.org.ar/wp-content/uploads/2021/06/CD-SRA.pdf")