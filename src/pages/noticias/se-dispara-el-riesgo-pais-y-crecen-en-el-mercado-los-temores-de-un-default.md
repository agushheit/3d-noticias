---
category: Agenda Ciudadana
date: 2021-11-24T06:00:56-03:00
thumbnail: https://assets.3dnoticias.com.ar/merval.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: Se dispara el riesgo país y crecen en el mercado los temores de un default
title: Se dispara el riesgo país y crecen en el mercado los temores de un default
entradilla: 'El contexto externo también perjudicó a la Bolsa porteña, en especial
  el pesimismo por rebrotes europeos de Covid-19, una alza esperada en tasas de la
  Reserva Federal y la fuerte devaluación de la lira turca.

'

---
El riesgo país se disparó hoy al filo de los 1.800 puntos, los bonos cayeron y las acciones líderes de la Bolsa porteña retrocedieron 2,3%.

Analistas adjudicaron la caída accionaria a una reacción tras la pérdida de valor sufrida por los ADRs en Nueva York el lunes, que fue feriado en la Argentina.

El índice S&P Merval de BYMA cayó a 83.770,51 unidades, que sumó a la pérdida del 10% que acumuló por el panel de los papeles de las principales empresas la semana pasada. Las acciones cayeron en 6 de últimas 7 ruedas.

A pesar de estos datos negativo de los últimos días, el panel líder acumula una mejora del 63% en el año.

En Wall Street, los papeles de empresas argentinas operaban dispares, tras ceder más del 6% promedio el lunes.

Entre las subas aparecían los papeles de Central Puerto y Loma Negra (rebotan hasta 3%), mientras que entre las bajas sobresalían los activos de Telecom e IRSA (perdían hasta 2,8%).

"El mercado dio la sensación de haber perdido una buena parte de las expectativas optimistas", indicó un reporte de Rava Bursátil.

El contexto externo también perjudicó a la Bolsa porteña, en especial el pesimismo por rebrotes europeos de Covid-19, una alza esperada en tasas de la Reserva Federal y la fuerte devaluación de la lira turca.

Los bonos en dólares retrocedieron hasta 4,7% y pierden pierden 15% desde las primarias de septiembre (PASO).

Analistas de mercado sostienen, además, que los rumores de desdoblamiento cambiario afectaron negativamente a los productos financieros.

El Riesgo País elaborado por el banco JP Morgan subió 0,9%, a 1.796 puntos.

Llegó a alcanzar los 1.809 puntos, nuevo máximo histórico desde el canje de la deuda en 2020, pero luego se estabilizó por debajo de los 1.800 puntos.

En el mercado reina el pesimismo sobre la posibilidad de un acuerdo con el FMI. Incluso, un informe de Portfolio Personal Inversiones (PPI) alertó que la probabilidad acumulada de default se mantiene por encima del 82%.