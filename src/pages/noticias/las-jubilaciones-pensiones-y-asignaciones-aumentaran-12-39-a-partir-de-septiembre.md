---
category: Agenda Ciudadana
date: 2021-08-11T06:15:30-03:00
thumbnail: https://assets.3dnoticias.com.ar/jubilaciones.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Las jubilaciones, pensiones y asignaciones aumentarán 12,39 % a partir de
  septiembre
title: Las jubilaciones, pensiones y asignaciones aumentarán 12,39 % a partir de septiembre
entradilla: Los haberes acumularán una suba de 36,2% en lo que va de 2021, el haber
  jubilatorio mínimo pasará a ser de $ 25.922 y la Asignación Universal por Hijo y
  por Embarazo llegarán a los $ 5.063.

---
La Administración Nacional de la Seguridad Social (Anses) informó este lunes que las jubilaciones, pensiones y asignaciones universales y familiares aumentarán un 12,39% a partir de septiembre, de acuerdo con el ajuste dispuesto por la Ley de Movilidad 27.609, por lo que los haberes acumularan una suba de 36,2% en lo que va de 2021.

De esta forma, el haber jubilatorio mínimo pasará a ser de $ 25.922, mientras que la Asignación Universal por Hijo (AUH) y por Embarazo (AUE) llegarán a los $ 5.063, beneficiando a 7,1 millones de jubilados y pensionados y a más de 9 millones de niños, niñas y adolescentes que perciben asignaciones universales o familiares.

Se trata del tercer aumento otorgado mediante la Ley de Movilidad que permitirá a más de 16 millones de personas que reciben un haber o asignación por parte de la Anses tener un poder de compra superior al de la inflación proyectada para los primeros nueve meses del año.

Esta proyección se basa en que la suba del 36,2% por movilidad será superior al aumento de precios del 34,4% que anticipa el último Relevamiento de Expectativas de Mercado (REM) que publicó esta semana el Banco Central.

Además, el incremento acumulado por la nueva Ley de Movilidad en 2021 será unos 2,5 puntos porcentuales por encima del incremento que hubiese otorgado la anterior fórmula de ajuste sancionada en 2017 por el gobierno del expresidente Mauricio Macri que, según informó la Anses, hubiera sido del 33,7%.

"Esto demuestra que un modelo de país que apuesta al trabajo, al consumo y a la producción, mediante una fórmula que pondera salarios y recaudación, otorga aumentos superiores a los de la fórmula anterior que contemplaba, principalmente, la variable inflacionaria", aseguró el organismo que preside Fernanda Raverta en un comunicado.

En lo que va de 2021 también se otorgaron tres bonos para complementar los aumentos asignados por movilidad a los haberes mínimos.

En los meses de abril y mayo hubo se otorgaron refuerzos de $1.500 pesos a quienes percibían un haber mínimo y, en agosto, se está pagando un bono por $ 5.000 a quienes perciben hasta $ 46.129,40 (dos haberes mínimos), y para los que perciban entre ese monto y $ 51.129,40, el equivalente hasta alcanzar esta última cifra.

Además, cerca de 130 mil jubiladas, jubilados, pensionadas y pensionados que percibían los haberes más altos dejaron de pagar el impuesto a las ganancias desde este mes y recibirán una devolución promedio superior a los $ 23.000, que se abonará en cinco cuotas, las dos primeras en agosto.

En total, el incremento del 12,39% alcanzará a 7,1 millones de jubiladas, jubilados, pensionadas y pensionados, a más de 9 millones de niños, niñas y adolescentes que perciben de Asignaciones Universales por Hijo (4,4 millones) o Asignaciones Familiares (4,6 millones) y a más de 800 mil cónyuges.

El aumento impactará también en las asignaciones por Embarazo, Prenatal, Nacimiento, Adopción y Matrimonio.

Si se comparan los haberes y asignaciones entre diciembre de 2019 y septiembre de 2021, el haber jubilatorio mínimo habrá pasado de $ 14.068 a $ 25.922, un incremento del 84,26% en ese período sin contar los bonos, los reintegros del IVA por compra con tarjeta de débito y la gratuidad de medicamentos que ofrece el PAMI que incrementan el poder adquisitivo del salario.

Por su parte, el haber máximo pasó de $ 103.064 a $ 174.433 (+69,25%) en ese mismo período, mientras que la Asignación Universal por Hijo y por Embarazo pasó de $ 2.746 a $ 5.063 (+84,38%).

**La recuperación de las jubilaciones**

Según un informe elaborado por el Centro de Economía Política Argentina (CEPA), la recuperación real de las jubilaciones, pensiones, AUH y demás para el trimestre junio-septiembre será de 3,5%.

"Aunque es prematuro, se estima que, la fórmula para la actualización de diciembre también podría tener un muy buen desempeño, considerando que la reapertura salarial afectaría positivamente sobre los indicadores de salario y la reactivación hacer lo propio sobre el indicador de recaudación", señaló el CEPA sobre el porcentaje de ajuste de los haberes que se dará en el último mes de 2021.

Por último, respecto de la evolución de la jubilación mínima, apuntó que desde principios de 2019 en adelante "se observa que la curva se mantiene relativamente estable, con los saltos relacionados con el impacto de los bonos respectivos".

"Es decir, no se recuperan aún los valores perdidos durante los 4 años de gestión de Cambiemos, pero las jubilaciones no pierden contra la inflación", aseguró el documento.