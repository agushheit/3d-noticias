---
category: Agenda Ciudadana
date: 2020-12-05T12:54:28Z
thumbnail: https://assets.3dnoticias.com.ar/CONTROLVIAL.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: La Provincia refuerza los controles viales y la prevención en rutas por el
  fin de semana largo
title: La Provincia refuerza los controles viales y la prevención en rutas por el
  fin de semana largo
entradilla: La APSV implementará una mayor cantidad de operativos de control en rutas
  de la provincia por el aumento de los desplazamientos debido al turismo interno.

---
La Agencia Provincial de Seguridad Vial (APSV), que depende del Ministerio de Seguridad, reforzará la prevención en rutas de la provincia e incrementará los operativos y las recomendaciones a los conductores para los traslados que se realizarán a partir de este viernes con motivo del inicio de un fin de semana largo por el feriado del 8 de diciembre. El objetivo, es reforzar la presencia en rutas y la difusión de las medidas de prevención para evitar siniestros viales.

## **Controles**

La APSV dispuso una mayor cantidad de controles en zonas de quintas y casas de fin de semana, tanto de región norte como sur, en vista de que puede incrementarse la circulación en rutas producto de la habilitación de los traslados dentro de la provincia.

El organismo provincial reforzará la fiscalización del tránsito en Ruta Provincial 1 y Ruta Nacional 11, que atraviesan localidades de la zona costera en región norte de la provincia y en Ruta 9 que conecta la ciudad de Rosario con localidades aledañas.

El subsecretario de la APSV, Osvaldo Aymo, expresó que “este fin de semana es muy probable que tengamos un importante incremento de la circulación vehicular en rutas, por ello queremos estar preparados, detectar las conductas de riesgo antes de que ocasionen accidentes que se pueden evitar respetando las normas de tránsito y aumentando la precaución.

La presencia del Estado es fundamental, pero requerimos de la conciencia ciudadana de los viajeros, para que eviten tomar riesgos innecesarios en la ruta, y puedan disfrutar de estos viajes con la familia sin sufrir ningún inconveniente”.

Tanto las rutas provinciales como las nacionales y autopistas importantes contarán con la presencia de personal de la PSV, en colaboración con inspectores de las jurisdicciones locales y la ANSV.

## **Conductas de riesgo**

Los controles serán coordinados por la APSV y ejecutados por la Policía de Seguridad Vial en distintas rutas y accesos de la provincia. Se fiscalizará el consumo de alcohol en todos los puestos de control las 24 horas, el uso de dispositivos de seguridad, la portación de la documentación exigible (licencia de conducir, póliza de seguro, comprobante de la RTO), distribución de pasajeros y conductores vulnerables en condiciones de riesgo como los motociclistas que circulen sin casco o con más de un acompañante.

## **Concientización**

Por otro lado la APSV reforzará la difusión de las principales recomendaciones que permitan tener un viaje seguro.

El objetivo es brindar información sobre distintas situaciones que pueden presentarse durante el viaje, y cómo actuar ante cada una de ellas para evitar riesgos y aumentar la seguridad vial.

Entre las recomendaciones la APSV destaca revisar el estado mecánico general del vehículo y especialmente comprobar los frenos; programar el viaje teniendo en cuenta las personas que viajan, si hay niños o personas mayores, etc., a fin de establecer paradas y tiempos de descanso y no ingerir alcohol.

“Es indispensable que todos los pasajeros lleven abrochado el cinturón de seguridad y que los niños viajen siempre atrás en los SRI (Sistemas e Retención Infantil) correspondientes a cada edad.

Se deben llevar encendidas las luces bajas las 24 hs; tomar las precauciones de distancia y visibilidad antes de realizar un adelantamiento; respetar los límites de velocidad reglamentarios y no consumir alcohol antes ni durante la conducción”, informó Aymo.

La documentación reglamentaria que se debe portar es la siguiente:

* Licencia de conducir vigente
* Cédula única de identificación del automotor
* Póliza de seguro vigente
* Última patente del año en curso paga
* DNI
* RTO (Revisión Técnica Obligatoria)
* Si el vehículo funciona con GNC, se debe contar con autorización para utilizar este tipo de combustible

  ***

El estado del tránsito en las rutas se puede consultar a través de la información que difunde la APSV en sus cuentas de redes sociales:

##### **Facebook:** [**seguridadvialsantafe**](https://www.facebook.com/SeguridadVialSantaFe/ "APSV en Facebook")

##### **Twitter:** [**@redsegvial**](https://twitter.com/RedSegVial "APSV en Twitter")