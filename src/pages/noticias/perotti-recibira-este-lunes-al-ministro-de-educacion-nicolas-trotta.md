---
category: Estado Real
date: 2021-02-01T07:56:16-03:00
thumbnail: https://assets.3dnoticias.com.ar/SM-perotti-trotta-310121.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: Perotti recibirá este lunes al ministro de Educación Nicolás Trotta
title: Perotti recibirá este lunes al ministro de Educación Nicolás Trotta
entradilla: El gobernador de la provincia y el titular de la cartera educativa nacional
  trabajarán en los preparativos para el reinicio de las clases presenciales.

---
El gobernador Omar Perotti recibirá este lunes, en la ciudad capital, al ministro de Educación de la Nación, Nicolás Trotta, para trabajar en los preparativos por el reinicio de las clases presenciales en la provincia, en el marco de la pandemia del Covid 19. Del encuentro, que se realizará a las 11:30 horas en Casa de Gobierno, participará también la ministra de Educación de la provincia, Adriana Cantero.

Finalizada la reunión de trabajo, las autoridades brindarán una conferencia de prensa. Luego, a partir de las 15, Trotta y su comitiva mantendrán una reunión informativa y de intercambio de experiencias y labores, con funcionarias y funcionarios de Ministerio de Educación provincial. Por otra parte, el ministro Trotta se reunirá también con representantes de los gremios docentes santafesinos.

Una vez concluida esta reunión, el ministro de Educación de la Nación visitará uno de los dispositivos del programa Verano Activo, ubicado en el Polideportivo Parque Garay. Vale recordar que se trata de una iniciativa orientada a niñas, niños y adolescentes que tuvieron un vínculo frágil e intermitente con la escuela durante la pandemia.

Verano Activo se enmarca a su vez en el programa nacional Acompañar-Puentes de Igualdad, que promueve la revinculación con los aprendizajes de alumnas y alumnos, impulsando y facilitando el sostenimiento de sus trayectorias escolares.

Finalmente, a las 16:30 horas, Trotta supervisará las obras del Jardín N° 246 en barrio Las Vegas, financiadas por el gobierno nacional. Se trata de la construcción de la sede propia del establecimiento educativo que había sido paralizada años atrás y se reanudó a partir del mes de octubre pasado. Los trabajos habían sido licitados en 2016 y habían quedado sin avances.

La comitiva que acompañará al ministro Trotta estará integrada por el jefe de Gabinete ministerial, Matías Novoa; la secretaria general del Consejo Federal de Educación, Marisa Díaz; la subsecretaria de Educación Social y Cultural, Laura Sirotzky; y el secretario de Cooperación Educativa y Acciones, Pablo Gentili.