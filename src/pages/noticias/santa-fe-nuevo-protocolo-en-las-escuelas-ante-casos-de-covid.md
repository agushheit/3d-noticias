---
category: La Ciudad
date: 2021-11-09T06:00:54-03:00
thumbnail: https://assets.3dnoticias.com.ar/protocovid.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: LT10
resumen: 'Santa Fe: nuevo protocolo en las escuelas ante casos de covid'
title: 'Santa Fe: nuevo protocolo en las escuelas ante casos de covid'
entradilla: "Los parámetros atienden el escenario actual de la pandemia apostando
  a la máxima presencialidad. Cambian los tiempos de aislamiento para casos sospechosos,
  positivos y contactos estrechos.\n\n"

---
El Ministerio de Educación actualizó los protocolos para proceder en casos de aislamiento, siguiendo las recomendaciones de las autoridades sanitarias provinciales. Estas medidas tienen en cuenta que la pandemia no ha finalizado, pero reconocen los nuevos escenarios de los indicadores sanitarios.

En ese sentido, la ministra de Educación, Adriana Cantero, sostuvo que “habida cuenta que la pandemia no ha terminado pero nos va mostrando una mejora en las condiciones, los ministros de Salud de todo el país han estado analizando los protocolos que se aplican en casos sospechosos y los cuidados con las suspensiones y actividades que afectan a la cotidianidad escolar. En este caso, a partir de un trabajo entre los ministerios de Educación y Salud de nuestra provincia les hemos comunicado a todas las escuelas el nuevo protocolo educativo para intervenir en estos casos donde se reducen los períodos de los días de suspensión de presencialidad, tratando todos de recuperar al máximo de sostener la presencialidad plena y cuidada en las escuelas”.

**Nuevo protocolo**

El nuevo documento plantea recomendaciones relacionadas a la detección precoz de casos sospechosos, identificación de contactos estrechos y posteriores acciones de mitigación frente a la enfermedad por covid19 en instituciones escolares.

Al respecto, el protocolo incluye principios básicos para la prevención como la limitación de contactos manteniendo una distancia de al menos 1,5 metros evitando aglomeraciones; la continuidad del uso adecuado de barbijo, durante toda la jornada académica tanto en personal docente, no docente y alumnos; la higiene de manos frecuente como medida básica para evitar la transmisión; la necesidad de ventilación continua de los espacios y la limpieza del centro educativo, entre otras recomendaciones, teniendo en cuenta que las mismas son dinámicas y se pueden modificar según la situación epidemiológica.

Con respecto al caso sospechoso, si este se confirma deberá continuar aislado en su domicilio por 10 días. Los integrantes del curso o burbuja, si el caso se confirma, se continuará el aislamiento por 10 días desde el último contacto con el caso; si dio como resultado negativo, se retoman las clases presenciales. Además puede realizar una PCR a partir del 7mo. día, que si resulta negativa puede levantarse el aislamiento. En caso de que la misma sea positiva, será considerado caso confirmado.

Por otra parte, también se acordó que la línea 0800 que atiende especialmente las consultas escolares orientará las consultas realizadas en este mismo sentido.