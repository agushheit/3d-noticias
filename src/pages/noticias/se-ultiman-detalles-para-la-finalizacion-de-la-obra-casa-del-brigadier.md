---
category: Estado Real
date: 2021-01-11T11:14:04Z
thumbnail: https://assets.3dnoticias.com.ar/110121-casa-Brigadier.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: 'Casa del Brigadier: se ultiman detalles para finalizar la obra '
title: Se ultiman detalles para la finalización de la obra Casa del Brigadier
entradilla: El gobierno provincial está finalizando con las tareas de recuperación
  integral con una inversión que se aproxima a los 48 millones de pesos.

---
El gobierno provincial, a través del Ministerio de Infraestructura, Servicios Públicos y Hábitat, está culminando con las tareas de restauración, puesta en valor y refuncionalización de la Casa del Brigadier Estanislao López ubicada en la intersección de la avenida General López y calle 9 de Julio de la ciudad de Santa Fe. La obra que comenzó a finales de marzo de 2019, ya tiene un avance del 99% restando trabajos de limpieza de la propiedad.

La secretaria de Arquitectura y Obras Públicas, Leticia Battaglia, mencionó que «la obra extendió su plazo inicial de 9 meses porque durante el proceso aparecen cuestiones constructivas que se resuelven in situ. Recordemos que se trata de una construcción patrimonial que tiene paredes de adobe, frisos, cielorrasos, los cuales tuvieron que ser reconstruidos por especialistas, y sin alterar la historia e imagen de la casa. Por esto, desde un inicio, se trabajó con la Comisión Nacional de Monumentos».

En este sentido, Battaglia destacó el trabajo interdisciplinario «desde un inicio hubo gran coordinación por parte de la Dirección Provincial de Arquitectura e Ingeniería para restaurar este inmueble con la responsabilidad y seriedad que merece cualquier obra pública, y con hallazgos que pertenecen a la historia patrimonial de la ciudad. Se han encontrado piezas diversas y pequeñas, domésticas como utensilios de cocina, se recuperaron baldosas cerámicas originales de la época. Todo esto tiene un alto valor arqueológico que lo realizaron artesanos calificados y la labor de la empresa constructora que es destacable».

«Los trabajos apuntaron a mantener la autenticidad e integridad del edificio, y en muchos casos reconstruyendo con los mismos materiales de la época, para mantener la originalidad del patrimonio y las diversas intervenciones que tuvo en los distintos períodos históricos», finalizó la secretaria de Arquitectura.

<br/>

## **La Casona**

**Construida sobre muros de adobe en el año 1812** por el protomédico Manuel Rodríguez, la casa fue habitada por el brigadier Estanislao López entre los años 1819 y 1838. El brigadier vivió en esta casa hasta su muerte en junio de 1838 (su viuda y sus hijos continuaron viviendo aquí después de su fallecimiento).

A principios del siglo XIX, el solar fue adquirido por el mencionado doctor, suegro del Brigadier López, quien reedifica la casa. Manuel Rodríguez llegó a Santa Fe en 1790 y obtuvo el cargo de protomédico, la máxima posición médica de la época. Fue el primero en tratar la lepra en la ciudad de Santa Fe, y por esta razón, el hospital, que había sido un leprosario, lleva su nombre.

En 1872, el nuevo propietario de la casa, Daniel De la Torre, realiza importantes modificaciones italianizantes y le agrega el segundo piso, como hoy se la conoce.

Posteriormente, **en 1942, la casa es declarada Monumento Histórico Nacional** y desde 1946 es la sede del Archivo General de la Provincia de Santa Fe (hasta enero de 2017).

**En 1963, el gobierno de la provincia se la compró a los últimos propietarios.**