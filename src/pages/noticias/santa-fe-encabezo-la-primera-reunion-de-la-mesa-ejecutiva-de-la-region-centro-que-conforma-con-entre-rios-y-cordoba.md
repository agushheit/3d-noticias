---
category: Agenda Ciudadana
date: 2021-02-12T08:00:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/mesaejecutiva.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Santa Fe encabezó la primera reunión de la mesa ejecutiva de la región centro
  que conforma con Entre Ríos y Córdoba
title: Santa Fe encabezó la primera reunión de la mesa ejecutiva de la región centro
  que conforma con Entre Ríos y Córdoba
entradilla: Se trata del bloque regional que tiene como objetivo el trabajo interprovincial
  de temas vinculados a género, ambiente, conectividad, economía del conocimiento,
  bioeconomía y comercio exterior, entre otros.

---
En el marco de la primera reunión de la Mesa Ejecutiva de la Región Centro del 2021, su presidenta y representante de la provincia de Santa Fe, Candelaria González del Pino, sostuvo que “la agenda de trabajo plasmada para todo el año es el reflejo del compromiso que nuestro gobernador Perotti y los gobernadores de Córdoba y Entre Ríos tienen con la región”.

En referencia a los ejes que se abordaron durante el encuentro González del Pino, Secretaria de Gestión Federal de la Provincia, aseguró que “esta primera reunión es un repaso de la XIV Reunión Institucional de la Región Centro, las decisiones, la agenda, el acta acuerdo, que han quedado como fruto del trabajo del bloque durante el 2020” y agregó que “nuestros gobernadores nos dieron indicaciones concretas de cuáles son los ejes de gestión en los que quieren que profundicemos desde la Región, entre ellos los temas vinculados a género, ambiente, conectividad, economía del conocimiento, bioeconomía y comercio exterior”.

En ese sentido, la funcionaria afirmó: “Estos ejes de gestión son los que hoy tratamos de plasmar en una agenda concreta, conjuntamente con el Consejo Federal de Inversiones, que siempre nos acompaña en estas instancias” y destacó como primera gran acción “el compromiso del CFI de acompañarnos en el estudio de pre factibilidad para llevar adelante obras que se necesitan para habilitar el corredor bioceánico, ver cuál es el que más favorece a la logística de nuestras provincias, no solo de la Región Centro, sino también de otras provincias de nuestro país para poder tener salida hacia el Pacífico”.

Del encuentro participaron también el secretario de Integración Regional y representante por Córdoba de la Región Centro, Jorge Montoya; y el presidente del Ente Región Centro e Integración Regional de Entre Ríos, Claudio Ava Aispuru. Además, los secretarios administrativos de las tres provincias, Lucila Martín, David Urreta y Juan Coniglio y, Nicolás Cevela, Director de Coordinación del equipo técnico del Consejo Federal de Inversiones.