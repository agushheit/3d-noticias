---
category: Agenda Ciudadana
date: 2021-05-09T06:15:44-03:00
thumbnail: https://assets.3dnoticias.com.ar/alberto.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: INFOBAE
resumen: 'Coronavirus: Alberto Fernández cerró los detalles del proyecto de ley para
  regular las restricciones y llegará al Congreso la semana próxima'
title: 'Coronavirus: Alberto Fernández cerró los detalles del proyecto de ley para
  regular las restricciones y llegará al Congreso la semana próxima'
entradilla: 'El Presidente recibió al Jefe de Gabinete en Olivos antes de partir en
  su gira a Europa. Quiere evitar la intervención de la Corte Suprema en las medidas
  sanitarias. '

---
Después del revés por la decisión de la Corte Suprema de avalar la postura de la Ciudad de Buenos Aires sobre la educación en el marco de las restricciones por la segunda ola de coronavirus, el Gobierno terminó de definir hoy los detalles del proyecto de ley que anunció hace diez días Alberto Fernández para regular la pandemia desde el Congreso.

Antes de viajar a Europa, el Presidente recibió al jefe de Gabinete, Santiago Cafiero, en Olivos, donde revisó el texto de la iniciativa y pidió que se realicen algunos ajustes. La propuesta, según pudo saber Infobae, estará basada en el Decreto de Necesidad y Urgencia (DNU) vigente, pero con “mayor sustentación sanitaria”, y se presentará el lunes en el Congreso.

El primer mandatario parte de gira al viejo continente esta noche, pero antes organizó un encuentro presencial con Cafiero para leer la última versión del proyecto que redactaron la Jefatura de Gabinete y la secretaría de Legal y Técnica, que conduce Vilma Ibarra, con datos provistos por el Ministerio de Salud, a cargo de Carla Vizzotti. La Casa Rosada enviará la iniciativa al Congreso la semana próxima, probablemente el lunes. El objetivo principal es que se le brinde tratamiento exprés y que se apruebe antes de que venza el actual DNU, el 21 de mayo, para evitar la intervención de la Justicia en las medidas sanitarias nacionales.

Fuentes de la Casa de Gobierno dijeron a Infobae que ya hubo acercamientos informales a la oposición en el Congreso. Desde Juntos por el Cambio confirmaron que dialogaron sobre el borrador del proyecto, pero apelaron a la cautela y prefirieron no opinar hasta no ver la propuesta concreta. En diálogo con Infobae, un legislador de la oposición adelantó una perspectiva de partida, en un marco de prudencia: “El fallo de la Corte ayuda a dejar claro que algunas cosas son competencia exclusiva de los gobiernos provinciales. No importa que sea ley o DNU, en esos temas el gobierno nacional no se puede meter”.

La sintonía entre el Frente de Todos y Juntos por el Cambio por la postergación de las PASO podría continuar con el proyecto para regular las medidas contra la pandemia, pero aún no se conocen los detalles de la iniciativa del oficialismo

La sintonía entre el Frente de Todos y Juntos por el Cambio por la postergación de las PASO podría continuar con el proyecto para regular las medidas contra la pandemia, pero aún no se conocen los detalles de la iniciativa del oficialismo

El viernes se concretó el acuerdo para postergar las PASO por motivos sanitarios y hay sintonía entre el oficialismo y la oposición, que habrían superado las fuertes diferencias que suscitaron las miradas sobre las clases. Pero los detalles de la iniciativa por la pandemia podrían provocar desencuentros. Desde el Ministerio del Interior que conduce Eduardo “Wado” de Pedro confirmaron a este medio que ya hubo diálogos, en los últimos días, con los gobernadores. Por ahora, la mayoría de los jefes provinciales evitaron referirse a la medida.

Según adelantaron fuentes oficiales a Infobae, la eventual “ley de la Pandemia”, como la denominó un funcionario, se basó en el modelo que implementó la canciller de Alemania, Angela Merkel, y parte de los textos de los últimos DNU, a los que se sumarán argumentos jurídicos y sanitarios.

La idea de regular las medidas sanitarias había sido propuesta por Vilma Ibarra el año pasado, pero el Presidente continuó con las restricciones a través de Decretos de Necesidad y Urgencia. Se mantuvo firme en esa tesitura a pesar de los cuestionamientos de la oposición, que reclamaba poder tener voz y voto sobre las medidas, en especial cuando el Congreso comenzó a funcionar de manera virtual y, después, semi-presencial. No fue hasta que escaló el conflicto con la Ciudad por las clases que Alberto Fernández decidió que las restricciones frente al coronavirus se resuelvan por vía legislativa.

La Casa Rosada quería que la educación se impartiera virtualmente, para evitar los contagios, mientras que el jefe de gobierno porteño se plantó para que se mantuviera la presencialidad. Ante la falta de consenso político, Horacio Rodríguez Lareta acudió a la Corte Suprema de Justicia, que después de dos semanas falló a favor de la Ciudad. Cinco días antes de que se conociera la decisión del Máximo Tribunal, Alberto Fernández anunció -en paralelo al lanzamiento de un nuevo DNU donde mantenía el mandato de que se cerraran las aulas- que enviaría al Congreso un proyecto para regular la pandemia. Sin brindar mayores detalles, adelantó que la eventual ley fijaría parámetros sanitarios prestablecidos, y le daría atribuciones al Poder Ejecutivo nacional y a los provinciales para poder disponer medidas frente al aumento de contagios y de la mortalidad.

En principio, la oposición, en la voz de referentes como el diputado radical Mario Negri y el presidente de la UCR, Alfredo Cornejo, expresó alarma por la posibilidad de que la iniciativa representara una entrega de “superpoderes” al Presidente. Sin embargo, entre el martes y el viernes se logró un acuerdo para postergar las PASO, un tema que dividía al oficialismo y la oposición, y que cerró con un pacto de modificación por única vez, que se aprobará en el Congreso en las próximas semanas. En un contexto de relativa buena relación, después de la fuerte pelea político-judicial por las clases, el terreno se muestra favorable para que el proyecto avance.