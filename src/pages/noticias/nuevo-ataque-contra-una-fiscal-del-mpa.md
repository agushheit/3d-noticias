---
category: Agenda Ciudadana
date: 2020-12-26T10:10:15Z
thumbnail: https://assets.3dnoticias.com.ar/2412mpa.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: El Litoral'
resumen: Nuevo ataque contra una fiscal del MPA
title: Nuevo ataque contra una fiscal del MPA
entradilla: 'Dos mujeres fueron detenidas por «insultar, amenazar y agredir físicamente»
  a la fiscal Ángela Capitanio, tras finalizar un juicio en el que un hombre de 35
  años resultó condenado. '

---
**En Santa Fe, tres semanas atrás, había sido agredida una fiscal de la Unidad Gefas.**

El Colegio de Magistrados y Funcionarios del Poder Judicial y el Ministerio Público de la Acusación (MPA) de la provincia repudiaron el ataque sufrido este miércoles por la fiscal de Rafaela, la Dra. Ángela Capitanio.

El hecho, por el cual fueron detenidas dos mujeres, se produjo al término de un juicio oral, en el que un hombre de 35 años resultó condenado a 10 años de prisión, por haber sometido a su ex pareja al ejercicio de la prostitución durante más de una década, entre otros delitos comprobados.

«La funcionaria del MPA fue insultada, amenazada y agredida físicamente en inmediaciones del edificio de Tribunales de la ciudad de Rafaela», informó el MPA. Por su parte, desde el Colegio de Magistrados expresaron «su profunda preocupación y repudio por una nueva agresión acaecida en el día de la fecha hacia una fiscal de esta provincia, con motivo y en ocasión del cumplimiento de sus funciones».

<br/>

<span style="font-family: helvetica; font-size: 1.3em; font-weight: 800;"> **En una marcha** </span>

La referencia a una «nueva agresión», hace alusión a los hechos de público conocimiento ocurridos la mañana del 3 de diciembre frente al palacio de tribunales de la ciudad de Santa Fe, donde familiares de detenidos y condenados por abusos sexuales increparon a la fiscal María Celeste Minniti, quien se desempeña en el equipo de la Fiscalía de Violencia de Género, Familiar y Sexual (Gefas) de la Fiscalía Regional Nº 1. El acontecimiento, que derivó en una investigación en la que se encuentra imputada una mujer, se produjo en medio de una marcha, en la puerta de tribunales.

En el caso de Capitanio «fue abordada y agredida físicamente por personas allegadas a un condenado por delitos ligados a la explotación de la prostitución, luego de dictado el veredicto de dicho juicio», refirió el consejo directivo que agrupa a jueces de toda la provincia.

«Ya son reiterados los hechos de este tipo que se vienen sucediendo en los tribunales de esta provincia, por lo que este Colegio se ve en la necesidad de reiterar el reclamo para que se adopten las medidas de seguridad que correspondan en los edificios tribunalicios, y que los magistrados y funcionarios puedan desempeñar sus funciones con la debida seguridad, libertad e independencia», finaliza la nota difundida a los medios.

<br/>

<span style="font-family: helvetica; font-size: 1.3em; font-weight: 800;"> **Dos mujeres** </span>

A propósito del episodio violento, que también tuvo como saldo la agresión de una policía, el MPA informó que fueron aprehendidas dos mujeres. El fiscal de turno, Martín Castellano, dispuso que las dos personas queden detenidas hasta tanto se realice la correspondiente audiencia imputativa.

Momentos antes del inusitado ataque, el tribunal de juicio que integraron los jueces Cristina Fortunatto, Nicolás Rogiani y Cecilia Alamo, condenó a 10 años de prisión a Adolfo Luis San Lorenzo, de 35 años.

San Lorenzo fue condenado como autor de los delitos de explotación económica de prostitución ajena calificada (por violencia, situación de vulnerabilidad, convivencia y por la edad de la víctima); violación de domicilio, coacción agravada (por el uso de arma de fuego); lesiones leves agravadas (por el vínculo); daño y robo calificado (por el uso de arma de fuego cuya aptitud para el disparo no puede acreditarse).

<br/>

<span style="font-family: helvetica; font-size: 1.3em; font-weight: 800;"> **Quince años** </span>

La fiscal Capitanio subrayó que «gran parte de los delitos por los que fue condenado fueron cometidos en un claro contexto de violencia de género, en perjuicio de la mujer con la que tuvo una relación de pareja y de la que nació un niño que, en la actualidad, es menor de edad».

«El primero de los ilícitos comenzó en el año 2003, mientras que los otros fueron en enero y en octubre de 2018, respectivamente», agregó.

Capitanio detalló que «con el fin de obtener un beneficio económico, San Lorenzo explotó económicamente el ejercicio de la prostitución de su pareja, que en 2003 era una adolescente menor de edad. La llevaba en moto o en bicicleta a la ruta nacional 34, más precisamente al acceso a la localidad de Susana o a la estación Oil de Rafaela, desde donde la víctima se desplazaba para realizar la actividad».

«Esta situación se mantuvo durante quince años hasta el jueves 18 de enero de 2018, oportunidad en que la mujer pudo escaparse de su domicilio junto al hijo que tuvo con San Lorenzo», añadió.