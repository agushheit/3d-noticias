---
category: Agenda Ciudadana
date: 2022-11-17T10:06:03-03:00
thumbnail: https://assets.3dnoticias.com.ar/cuidacoches.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: Otra vez los cuidacoches.
title: Otra vez los cuidacoches.
entradilla: 'En la tarde de ayer miércoles, un grupo de cuidacoches agredieron a un
  empleado de un local cercano a la terminal de colectivos, y destrozaron el negocio.  '

---
En la tarde de ayer miércoles, un grupo de cuidacoches agredieron a un empleado de un local cercano a la terminal de colectivos, y destrozaron el negocio. Es un hecho mas de una larga cadena de situaciones violentas y extorsivas que dan cuenta los vecinos de nuestra ciudad.

Existe una ordenanza, vale la aclaración, impulsada por el actual Intendente Emilio Jatón, que creó el “Programa Inclusivo de Cuidadores de Vehículos”. La ordenanza 12635, de mayo de 2019, está en vigencia.

¿A que obliga la ordenanza?

Según lo votado durante la sesión -artículo 2º- “_se entenderá por cuidadores de vehículos a toda persona que realice la actividad informal de ofrecer el servicio de cuidar rodados estacionados en la vía pública, contando para ello con la acreditación y/o carnet habilitante emitido por la Municipalidad de Santa Fe y cumpla con todos los requisitos que se establezcan_”.

La actividad quedará prohibida en lugares donde haya estacionamiento medido.

No podrán exigir el pago de canon alguno a los titulares de vehículos que dejen estacionados sus rodados en la vía pública.

La Secretaría de Control elaborará una planilla de inscripción, la cual será puesta a disposición de los interesados, estableciendo los siguientes requisitos: ser mayor de 18 años; especificar nombre y apellido, número de DNI, estado civil, domicilio, estudios realizados; certificado médico; fotos color tipo carnet; certificado de buena conducta; e inscribirse en algunos de los programas de capacitación y/o promoción del empleo municipal, provincial o nacional.

El interesado deberá presentar acreditación de domicilio en la ciudad de Santa Fe con documental adecuada y la constancia de no estar inscripto en el Registro de Deudores Alimentarios Morosos.

Para el caso de espectáculos de masiva concurrencia, inmediaciones de ferias o mercados donde se realicen operaciones de carga y descarga de mercaderías, lugares donde se realizan eventos deportivos, culturales o de entretenimientos, la Secretaría de Control planificará cada evento asignando cupos y lugares de labor. Días y horarios, debiendo coordinar con el organizador del evento dicha autorización.

¿El registro?

A inicios de 2021, en el Diario El Litoral, el actual secretario general municipal Mariano Granato confirmó que se evaluaba modificar la norma que regula a los cuidacoches para “ajustarla a la realidad”, para que su implementación en la práctica sea mas factible. Y se conoció que estaban “relevados 140 cuidadores de vehículos.”

El pasado 8 de septiembre, se presentaron pedidos de informes en el concejo municipal, para saber “qué tareas de control está haciendo el Municipio; si esos trabajos se están articulando con otras áreas locales o provinciales; si fue creado el registro de cuidadores de vehículos que establece esta ordenanza; cuántas son las credenciales habilitadas para ejercer efectivamente la labor de cuidadores; cuáles son las zonas delimitadas en las que pueden trabajar; donde se puede consultar el listado de cuidadores; si se realizaron los cursos de capacitación previstos; qué ocurre con las denuncias de los vecinos, y otra series de preguntas**”**, señaló la concejala Adriana Molina.

Mas de 3 años de “regulación” de la actividad, y hasta hoy, nada sabemos sobre que hará el municipio con los cuidacoches: no cumple con las ordenanzas, no las modifica y no controla lo que pasa en las calles.