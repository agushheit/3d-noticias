---
layout: Noticia con imagen
author: Nota de la Redacción de 3DNoticias
resumen: Actividades en Rosario
category: Agenda Ciudadana
title: Sukerman supervisó el cumplimiento de los protocolos de actividades al
  aire libre en Rosario
entradilla: Junto al subsecretario Fiscalización de la provincia, Facundo Osia,
  recorrieron clubes de la costa, la playa pública y La Florida.
date: 2020-11-24T12:09:30.826Z
thumbnail: https://assets.3dnoticias.com.ar/sukerman-en-la-playa.jpeg
---
El ministro de Trabajo, Empleo y Seguridad Social de la provincia de Santa Fe, Roberto Sukerman, encabezó en el día de hoy un operativo de control en clubes de la costa, la playa pública y La Florida en Rosario, con el objetivo de verificar el cumplimiento de protocolos de actividades al aire libre en esta primera etapa de prueba piloto que dispusieron en conjunto las autoridades provinciales y municipales. El funcionario provincial evaluó como positiva esta instancia que permitirá en avanzar en otras actividades que están programadas.

El titular de la cartera de Trabajo, que estuvo acompañado por el subsecretario de Fiscalización de la provincia, Facundo Osia, manifestó: “estamos muy conformes con lo que vimos en los clubes, un gran trabajo de protocolos y de cumplimiento de los mismos por parte de los socios. En referencia a la playa pública y al traslado de pasajeros hacia las islas, las actividades se desarrollan con normalidad bajo la supervisión de personal de control tanto provincial como municipal que trabajan de manera integral”.

“Les agradecemos a los rosarinos el comportamiento y les pedimos que continuemos respetando las medidas dispuestas por las autoridades, de esa manera vamos a poder acceder a la habilitación de nuevos espacios y actividades. No hay que relajarse porque la pandemia no terminó”, aseveró el ministro.

Sukerman destacó que “a nivel provincial desde el inicio de la pandemia se han presentado más de 50.000 protocolos correspondientes a las actividades autorizadas y se han realizado más de 10.000 inspecciones en toda la provincia en un trabajo en conjunto con municipios y comunas. Agradecemos a todos los sectores por el compromiso adoptado en este sentido”.