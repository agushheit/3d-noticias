---
category: La Ciudad
date: 2021-02-16T06:56:14-03:00
thumbnail: https://assets.3dnoticias.com.ar/liceo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad de Santa Fe
resumen: Se encuentra abierta la preinscripción para las cinco escuelas del Liceo
  Municipal
title: Se encuentra abierta la preinscripción para las cinco escuelas del Liceo Municipal
entradilla: La Municipalidad recuerda que los interesados en ingresar a alguna de
  las cinco escuelas, deberán anotarse vía web.

---
Desde el pasado lunes 8 de febrero se encuentra abierta la preinscripción para los interesados en sumarse a las propuestas del Liceo Municipal “Antonio Fuentes del Arco” en sus cinco escuelas: Diseño y Artes Visuales (Edav), Idiomas, Música, Danzas y la Escuela de Expresión Estética Infantil (Edeei). La propuesta está destinada a alumnos ingresantes.

En el caso de la Escuela de Idiomas, el plazo para completar la preinscripción se extiende hasta el 19 de febrero. El día 22 de febrero, en tanto, se realizará el sorteo correspondiente para aquellos cursos en que se exceda el cupo máximo. Por otra parte, hasta el 26 de febrero hay plazo para anotarse en el resto de las escuelas.

La propuesta académica de nivel superior y vocacional del Liceo y los formularios de preinscripción a las distintas propuestas se encuentran disponibles en la web oficial: [https://santafeciudad.gov.ar/secretaria-de-educacion-y-cultura/liceo-municipal/.](https://santafeciudad.gov.ar/secretaria-de-educacion-y-cultura/liceo-municipal/. "https://santafeciudad.gov.ar/secretaria-de-educacion-y-cultura/liceo-municipal/.") En tanto, los interesados en realizar consultas pueden escribir a liceo.municipal@santafeciudad.gov.ar.