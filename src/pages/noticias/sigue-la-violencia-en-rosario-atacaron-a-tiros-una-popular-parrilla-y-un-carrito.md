---
category: Agenda Ciudadana
date: 2021-11-23T06:00:48-03:00
thumbnail: https://assets.3dnoticias.com.ar/balas.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: 'Sigue la violencia en Rosario: atacaron a tiros una popular parrilla y un
  carrito'
title: 'Sigue la violencia en Rosario: atacaron a tiros una popular parrilla y un
  carrito'
entradilla: 'Tres personas resultaron heridas durante los ataques cometidos por delincuentes
  que circulaban en moto.

'

---
Los ataques a balazos sobre distintos objetivos en la ciudad de Rosario no ceden y en las últimas horas, sicarios abrieron fuego contra una parrilla y restaurante, con un saldo de un mozo herido, y contra un carrito de comidas rápidas, en el que recibieron lesiones leves dos personas.

Los hechos se registraron anoche, a última hora, en el popular restaurante El Establo, situado en Pellegrini, entre Italia y España, y en el carrito Jorgito Junior, ubicado en 27 de Febrero, entre Laprida y Maipú.

En "El Establo", dos sicarios que circulaban en una moto se apostaron en la verada y realizaron al menos cuatro disparos. Como producto del ataque un mozo recibió un roce de bala en un brazo y fue derivado al Hospital de Emergencias Clemente Álvarez (Heca), y por la mañana de este lunes le dieron el alta.

Pocos minutos después se produjo un ataque similar en el carrito Jorgito Junior, donde dos personas recibieron heridas leves. Natalia, encargada, de "El Establo", sostuvo que en el local había entre 150 y 200 personas, y que hubo gente que se tiró al piso, aunque la mayoría de los concurrentes no se percataron de lo que ocurría.

"La sacamos barata", señaló la encargada, mientras que indicó que en el lugar había muchas familias con chicos. Además, sostuvo que no cuenta con indicios que expliquen el ataque, más que enmarcarlo que en las balaceras que tuvieron como blanco en las últimas semanas a estaciones de servicio y otros locales, ya que el restaurante no sufrió ninguna clase de intimidación.

En medio de una serie de episodios violentos, tres escuelas fueron atacadas a balazos en horas previas a las últimas elecciones legislativas. Sin que esté claro la relación de todos los episodios, la violencia también fue sufrida por los clásicos rivales futbolísticos de la ciudad, Newell´s Old Boys y Rosario Central.

En el primer caso, el domingo 14 de noviembre por la madrugada fue descabezada en el Parque Independencia la estatua de Isaac Newell, el fundador de la institución "tripera", mientras que al otro día la sede de Central fue atacada con bombas molotov y un vigilador recibió quemaduras de gravedad.  
Por otro lado, este domingo atacaron a balazos la sede local del Sindicato de Camioneros, sin que se registraran heridos.