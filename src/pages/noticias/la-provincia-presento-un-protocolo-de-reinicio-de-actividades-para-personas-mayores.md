---
category: Estado Real
date: 2020-12-01T11:26:35.000+00:00
thumbnail: https://assets.3dnoticias.com.ar/ADULTOS-MAYORES.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: La provincia presentó un protocolo de reinicio de actividades para personas
  mayores
title: La provincia presentó un protocolo de reinicio de actividades para personas
  mayores
entradilla: 'Comprende a Centros de Día (de rehabilitación y recreativos) y de Jubilados,
  Clubes de Abuelos, Vecinales y otras organizaciones vinculadas a adultos mayores. '

---
El gobierno de Santa Fe presentó el protocolo de reinicio de actividades para personas mayores de 60 años, que comprende a Centros de Día (rehabilitación y recreativos) y de Jubilados, Clubes de Abuelos, Vecinales y otras entidades que trabajen con personas mayores.

El encuentro se llevó a cabo en el Auditorio del Ministerio de Desarrollo Social y estuvieron presentes el ministro Danilo Capitani; la directora provincial de Adultos Mayores, Lucia Billoud; y el secretario de Integración Social e Inclusión Socioproductiva, Gustavo Chara.

El protocolo busca flexibilizar progresivamente el aislamiento físico, el cual está teniendo consecuencias psicosociales en las personas mayores que, producto de la restricción de salidas y reuniones sociales, está llevando a situaciones donde se presentan sentimientos de soledad, depresión y angustia. Con esta apertura progresiva, la dirección de Adultos Mayores del Ministerio de Desarrollo Social busca que las personas mayores puedan recuperar sus ámbitos de socialización indispensables, en cualquier etapa del curso vital, para que toda persona goce de calidad de vida.

La apertura se realiza con un criterio progresivo que combina el retorno a la nueva normalidad con la garantía y el pleno cumplimiento de las medidas de seguridad y de tipo sanitario. En todos los casos, en esta primera instancia, se permitirán actividades al aire libre hasta 10 participantes con distanciamiento de 2 metros, tapaboca y alcohol en gel o solución hidroalcohólica. En parques, plazas y espacios abiertos de vecinales, centros de jubilados, clubes de abuelos, etc.

En la oportunidad, Capitani explicó que el protocolo “se hizo en función de las indicaciones del gobernador Perotti de poner en primer lugar la seguridad y el bienestar de nuestros adultos mayores. Para nosotros es muy importante trabajar con las vecinales, con los clubes de abuelos, que son los que nuclean a las personas mayores, que son un eslabón particularmente sensible en esta pandemia”.

“Los mayores en medio de esta circunstancia han tenido que extremar los cuidados y se han visto afectados por el tema del aislamiento, de no ver a sus afectos con la consecuencia psicosocial”.

El protocolo establece medidas generales de protección, distancias, desinfección en los espacios comunes y de trabajo, restricciones de ingreso y egreso, tiempo de duración de la actividad y cantidad de personas que pueden participar de la misma. Comprende a centros de día (de rehabilitación y recreativos), centros de jubilados, clubes de abuelos, vecinales y otras organizaciones vinculadas a personas mayores. Está disponible en la página web del gobierno de Santa Fe. [Descargar acá](https://bit.ly/2JdMv0x)

Se aclara, además, que por la dinámica de la pandemia por Covid 19, los criterios podrán variar y deberán adecuarse a la situación epidemiológica del momento a nivel local y regional.

Por su parte, Billoud indicó que “los mayores han sido un sector de la población que se ha tenido que cuidar más y que se ha visto más afectada por el aislamiento que conllevan consecuencias personales y sociales como sentimientos de soledad y tristeza. Por eso creemos en la necesidad de desarrollar un protocolo que permita paulatinamente la apertura de actividades de tipo recreativo o lúdico, cognitivas y de rehabilitación que permiten una sociabilización indispensable para que toda persona pueda tener una buena calidad de vida”.

Por último, Chara manifestó que ”el contexto nos obliga a ser muy dinámicos, ya que cambian día a día las situaciones y no podemos estar desatentos en este sentido, por lo que creemos que tenemos que tomar ciertas definiciones que tienen que ver con generar herramientas, como este protocolo para que los adultos y adultas comiencen a recuperar, ya pensando en la pos pandemia, las actividades que tengan como eje el cuidado de su salud mental”.

***