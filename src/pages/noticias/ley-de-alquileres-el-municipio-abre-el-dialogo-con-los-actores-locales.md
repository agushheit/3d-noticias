---
category: La Ciudad
date: 2021-12-14T06:00:21-03:00
thumbnail: https://assets.3dnoticias.com.ar/leydealquileres.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Ley de Alquileres: el municipio abre el diálogo con los actores locales'
title: 'Ley de Alquileres: el municipio abre el diálogo con los actores locales'
entradilla: A un año y medio de su implementación, la Municipalidad de Santa Fe continúa
  con encuentros para conocer la mirada de expertos en la temática y definir acciones
  tendientes a optimizar la normativa vigente.

---
La Estación Belgrano fue sede de una mesa de diálogo que reunió a funcionarios municipales, concejales y representantes de instituciones y de la sociedad civil para poner en debate el presente de la Ley de Alquileres en la ciudad de Santa Fe, a un año y medio de su implementación.

“Nos parece importante desde la Oficina del Inquilino del municipio generar este encuentro para escuchar a las personas que trabajan en el tema, escuchar los pro y contra de esta ley, sabiendo que hay presentaciones en el Congreso Nacional para modificarla”, expresó Franco Ponce de León, director de Derechos y Vinculación Ciudadana de la Municipalidad.

El funcionario destacó la importancia de conocer en detalle “qué ha modificado la ley en la ciudad de Santa Fe, en un tema tan importante como son los alquileres”.

Pedro Peralta, presidente de la Federación de Inquilinos y Walter Govone, presidente de la Cámara de Empresas y Corredores Inmobiliarios de la provincia participaron de la reunión convocada por el municipio, junto con representantes de entidades locales.

Peralta consideró que el problema no es la ley, de la que destacó a favor la ampliación del plazo de alquiler a tres años y cambios en las garantías, sino que subyacen cuestiones de fondo como la elevada inflación, sumado a requisitos por parte de las inmobiliarias y la poca oferta existente: “Demonizar una ley no es la solución, sí hay que corregirla y se puede mejorar, pero creemos que no es necesario derogarla”.

Por su parte, Govone coincidió en que el aumento interanual de los contratos de locación “quedó atado al índice de inflación e impacta de lleno en los valores de los alquileres que se van actualizando año tras año”. Además, valoró que la normativa vigente tiene puntos positivos y otros negativos, al tiempo que celebró que en la “mesa de diálogo estemos todos los actores sentados para exponer las diferentes puntos de vista lo que está sucediendo en Santa Fe y encontrar las mejores opciones para que no se generen conflictos y trabajar de antemano”.

La Oficina del Inquilino funciona bajo la órbita de la Dirección de Derechos y Vinculación Ciudadana en el ex Predio Ferial, en Las Heras 2883, de lunes a viernes de 7.15 a 13. También, se reciben consultas en el correo electrónico derechosyvinculacionciudadana@santafeciudad.gov.ar y en la página web oficial del municipio, en el apartado de acceso a trámites mediante la Oficina Virtual.