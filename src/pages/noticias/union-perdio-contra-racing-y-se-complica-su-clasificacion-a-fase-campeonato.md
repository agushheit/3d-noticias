---
category: Deportes
date: 2020-11-30T11:34:44Z
thumbnail: https://assets.3dnoticias.com.ar/union-racing.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Por Clara Blanco
resumen: Unión perdió contra Racing y se complica su clasificación a Fase Campeonato
title: Unión perdió contra Racing y se complica su clasificación a Fase Campeonato
entradilla: 'El Tatengue cayó ante La Academia por 1 a 0 con gol de Carlos Alcaráz
  en el primer tiempo. Ahora deberá esperar el resultado de Arsenal – Atlético Tucumán. '

---
Unión viajó hasta Avellaneda para enfrentar a Racing por la fecha 5 de la Copa Diego Maradona. Pero el Tatengue no se trajo nada a Santa Fe, ya que perdió por 1 a 0. El único gol del partido lo marcó Carlos Alcaráz a los 40 minutos del primer tiempo.

El conjunto de Juan Manuel Azconzábal salió a la cancha con un equipo alternativo, ya que priorizó la Copa Sudamericana. Pero Unión volvió a sufrir su falta de eficacia y cayó en Avellaneda.

Con este resultado, los dirigidos por Juan Manuel Azconzábal continúan como únicos escoltas con 7 puntos, pero pueden ser alcanzados el lunes cuando Arsenal enfrente al ya clasificado Atlético de Tucumán. Si los de Sarandí ganan ante los tucumanos, igualarán la línea del Tatengue a sólo una fecha de definir las Fases Campeonato y Complementación.

El próximo partido del conjunto de Azconzábal será el sábado 5 de diciembre a las 19:20 en el 15 de Abril. Su último rival en fase de grupo será el Decano.