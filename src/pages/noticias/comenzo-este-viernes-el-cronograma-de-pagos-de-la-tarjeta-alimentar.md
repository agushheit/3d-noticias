---
category: Agenda Ciudadana
date: 2021-08-21T06:00:49-03:00
thumbnail: https://assets.3dnoticias.com.ar/ALIMENTAR.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Comenzó este viernes el cronograma de pagos de la tarjeta Alimentar
title: Comenzó este viernes el cronograma de pagos de la tarjeta Alimentar
entradilla: Se acredita el monto total para quienes cuentan con la tarjeta en forma
  física. En tanto, quienes cobren a través de la AUH lo harán desde el miércoles
  25 hasta el martes 31 de agosto.

---
El Ministerio de Desarrollo Social de la Nación inició este viernes la acreditación del monto total de la tarjeta Alimentar, correspondiente al mes de agosto, para quienes cuentan con la tarjeta en forma física y que permite comprar todo tipo de alimentos, a excepción de bebidas alcohólicas.

"La tarjeta AlimentAR es clave para que la mesa de los argentinos no vuelva a estar vacía. Con esta herramienta, contribuimos al acceso a la seguridad alimentaria de nuestras familias, a la vez que genera un gran impulso a la economía local", afirmó el ministro Juan Zabaleta en un comunicado de prensa.

Según el cronograma, este viernes se acredita el monto total para quienes cuentan con la tarjeta en forma física.

Para quienes cobren a través de la Asignación Universal por Hijo (AUH), y tengan hijas e hijos de entre 0 y 14 años inclusive, el calendario de pago será el siguiente:

· Documentos terminados en 0 y 1: miércoles 25 de agosto.

· Documentos terminados en 2 y 3: jueves 26 de agosto.

· Documentos terminados en 4 y 5: viernes 27 de agosto.

· Documentos terminados en 6 y 7: lunes 30 de agosto.

· Documentos terminados en 8 y 9: martes 31 de agosto.

La cartera de Desarrollo Social recordó que la Tarjeta Alimentar "fue destacada y calificada por la ONU como una buena práctica para avanzar en el objetivo de ponerle fin a la pobreza, combatir el hambre, garantizar la seguridad alimentaria y reducir las desigualdades".

"El reconocimiento -resalta- fue publicado este mes en el sitio de Objetivos de Desarrollo Sostenible (ODS) perteneciente al organismo internacional, donde también se reconoce el impacto positivo de esta política en el marco de la crisis abierta por la pandemia".

De acuerdo con el informe oficial, con la "ampliación del programa para familias con hijas e hijos de hasta 14 años inclusive, la tarjeta Alimentar llega a 3.885.067 personas. De ese total de personas alcanzadas, 3.764.278 son hijas e hijos menores de 14 años, 48.820 son hijos o hijas con discapacidad y 71.969 son personas embarazadas.