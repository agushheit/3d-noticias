---
category: Agenda Ciudadana
date: 2022-12-17T08:26:38-03:00
thumbnail: https://assets.3dnoticias.com.ar/2022-12-16NID_276904O_1.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: Recomendaciones ante el aumento de casos de COVID – 19
title: Recomendaciones ante el aumento de casos de COVID – 19
entradilla: El Ministerio de Salud provincial brindó diferentes recomendaciones ante
  el aumento de casos de Covid-19 en todo el territorio santafesino.

---
Del mismo modo invitó a la población a acercarse a los diferentes vacunatorios a colocarse su refuerzo correspondiente.

En este sentido, la ministra de Salud provincial, Sonia Martorano, indicó que "ante el aumento de casos en el mundo, la región y puntualmente en nuestra provincia, lo cual ha generado internaciones de pacientes en terapia y en sala general, la recomendación es el uso del barbijo".

"En el área de salud en estos momentos es de carácter obligatorio, tanto los y las profesionales como los pacientes que ingresan con sus acompañantes, deben colocarse un barbijo", subrayó la ministra de Salud.

Asimismo, la titular de la cartera sanitaria remarcó que "en los ámbitos cerrados y no ventilados, por tanto claramente en el transporte, tanto de corta como de larga distancia, hay que colocarse el barbijo".

Frente a este nuevo aumento de casos, la funcionaria recordó que "esto es algo muy similar a lo que vivimos hace un año con la variante Omicron, entonces de la misma manera vamos a recordar el lavado de manos, el uso de alcohol en gel ambientes ventilados, en las fiestas, que mejor que celebrarlas en el patio o en el jardín y si estamos en un lugar cerrado, recuerden abrir las ventanas.

Otra de las iniciativas en la que hizo hincapié la titular de la cartera sanitaria es los operativos móviles de vacunación, "todos los centros de salud tienen vacunas, también hospitales y algunos gremios dónde se está vacunando y a estos le estamos sumando para tener mayor cercanía y accesibilidad puestos móviles en las grandes ciudades, tanto en Rosario como Santa Fe".

"Tenemos una buena repercusión, la gente se acerca, pero igualmente le seguimos insistiendo a todos los mayores de 18 años que hayan pasado 120 días de su última dosis acercarse a colocarse un refuerzo de vacuna contra el covid", mencionó.

Por último, la ministra recordó que "el número que estamos informando de casos no se va a correlacionar directamente con la realidad porque hoy los testeos están dirigidos a mayores de 50, pacientes con comorbilidades, personal de salud y seguridad. De cualquier modo, pueden ingresar a la página web, sacar un turno para testearse en alguno de nuestros puestos, no se requiere pedido médico".

"Tenemos internados en terapia, con respirador y también hay en sala general. Hay más cantidad de casos, tenemos que cuidarnos y aprender a vivir con este virus", cerró Martorano.