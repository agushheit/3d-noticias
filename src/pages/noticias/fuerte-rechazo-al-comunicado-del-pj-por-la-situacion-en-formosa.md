---
category: Agenda Ciudadana
date: 2021-01-26T13:35:19Z
thumbnail: https://assets.3dnoticias.com.ar/gildo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Fuerte rechazo al comunicado del PJ por la situación en Formosa
title: Fuerte rechazo al comunicado del PJ por la situación en Formosa
entradilla: Desde hace meses se vienen denunciando violaciones a los Derechos Humanos.
  Un comunicado del Partido Justicialista tildó de "oportunismo y operaciones mediáticas"
  las denuncias.

---
Durante la cuarentena estricta del 2020 la ciudad de Formosa fue el blanco de muchísimas denuncias por reiteradas violaciones a los derechos humanos.

En los últimos días, los formoseños denuncian situaciones de hacinamiento en centros de aislamiento por COVID, restricciones forzosas de la libertad y violaciones varias a los derechos humanos.

La oposición salió en defensa de los ciudadanos denunciantes solicitando la intervención de la provincia y el cese de las situaciones de maltrato. Incluso el Senador Naidenoff presentó un hábeas corpus solicitando el cumplimiento del aislamiento de dichas personas en sus domicilios.

Ante la controversia suscitada en el país por la situación que se vive en la capital formoseña, la Orden Nacional del Partido Justicialista emitió un comunicado polémico y contundente:

_"En una pandemia, la principal obligación de todo gobierno es defender la salud y la vida de su comunidad._

_En nuestro país una provincia se destaca especialmente por los indicadores sanitarios logrados en defensa de su población: Formosa. La política sanitaria desplegada por el gobierno provincial logró tener la menor cantidad de contagios y la menor cantidad de muertes por COVID-19 en la República Argentina._

_La feroz campaña política y mediática desatada nuevamente en contra de esa provincia no puede tener como verdadero fundamento, por ende, una preocupación sanitaria, ni mucho menos un interés por el bienestar del pueblo formoseño._

_Los insólitos pedidos de intervención federal y las permanentes operaciones motorizadas por sectores de la oposición al Gobierno Nacional dejan en evidencia la intencionalidad política de la permanente campaña de desprestigio contra Formosa. Se dicen democráticos y republicanos, pero buscan, con estas artimañas, lograr los espacios que el voto popular de los formoseños les niega contundentemente._

_Los agravios contra Gildo Insfrán no son casualidad. No sólo es un gobernador que puede mostrar una gran gestión en su provincia, sino también es el Presidente del Congreso Nacional del Partido Justicialista y un dirigente de amplia y destacada trayectoria como militante y referente de nuestro movimiento político._

_El objetivo de la oposición, finalmente, es atacar al peronismo y al gobierno peronista que les ganó en las urnas. Por este mismo motivo llamaron a incumplir la cuarentena, militan en contra de la vacuna y fomentan la grieta en todos los ámbitos posibles._

_Esta forma de hacer política no solo daña a la democracia y a las instituciones que tanto dicen defender, sino que, además, en tiempos de pandemia, esta actitud mezquina y egoísta puede costar la vida de muchos compatriotas._

_La vida humana, además de ser el valor fundamental a proteger, es también el límite de la mentira. Creer lo contrario es un acto de la mayor bajeza e indignidad._

_Para poner a la Argentina definitivamente de pie, debemos seguir defendiendo unidos la salud y la vida de todos y cada uno de nuestros compatriotas."_

_José Luis Gioja — Presidente Consejo Nacional del Partido Justicialista_

_Daniel Scioli — Vicepresidente del Consejo Nacional del Partido Justicialista_

_Luía Corpacci — Vicepresidente del Consejo Nacional del Partido Justicialista_

_Leonardo Nardini — Vicepresidente del Consejo Nacional del Partido Justicialista_

_Rosana Bertone — Vicepresidente del Consejo Nacional del Partido Justicialista_

_Antonio Caló — Vicepresidente del Consejo Naci_onal _del Partido Justicialista_