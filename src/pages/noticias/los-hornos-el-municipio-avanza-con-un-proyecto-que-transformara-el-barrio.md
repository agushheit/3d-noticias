---
category: La Ciudad
date: 2021-08-20T18:00:32-03:00
thumbnail: https://assets.3dnoticias.com.ar/LOSHORNOS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Los Hornos: el municipio avanza con un proyecto que transformará el barrio'
title: 'Los Hornos: el municipio avanza con un proyecto que transformará el barrio'
entradilla: 'Equipos municipales y de la Nación relevaron la zona comprendida entre
  las calles Gorostiaga, Gobernador Freyre, Derqui y San Juan. '

---
La Municipalidad de Santa Fe concreta el paso previo a la llegada del Plan Integrar al barrio Los Hornos: el relevamiento de las familias que viven en el polígono comprendido entre las calles Gorostiaga, Gdor. Freyre, Derqui y San Juan, en el noroeste de la ciudad. Sobre la base de la información recolectada por equipos municipales y de la Nación, se desarrollará el proyecto de mejoramiento elaborado por la Agencia Santa Fe Hábitat en el marco del Plan Integrar.

Los proyectos de mejoras contemplan obras viales, desagües pluviales y cloacales, alumbrado público, conexiones de agua potable y la generación de espacios públicos. En ese sentido, las obras serán financiadas por la Secretaría de Integración Socio Urbana de la Nación, el monto estimado de los trabajos es de 400 millones de pesos. Cabe recordar, que Los Hornos fue relevado en 2017 por el Registro Nacional de Barrios Populares (Renabap).En ese momento se registraron 300 familias.

Desde la Agencia Santa Fe Hábitat, Paola Pallero precisó que en total, ya se relevaron 500 lotes (entre 600 y 700 familias) en el área a intervenir y remarcó que las futuras obras “implican brindar calidad de vida no solo para esos vecinos y vecinas, sino también para todo el entorno y para la ciudad en su conjunto. Es una intervención que significará mejores condiciones de seguridad, de accesibilidad, de conectividad. Desde el municipio creemos que de esta manera avanzamos en la integración social y urbana que buscamos y, en consecuencia, en una mejor convivencia”.

“El objetivo de este relevamiento es mejorar las condiciones de accesibilidad, conectividad y seguridad a los vecinos y vecinas del barrio y su entorno”, precisó Pallero y añadió: “Este proyecto implica transformar realmente el barrio, dándole las obras de infraestructura que le falta desde hace años. Recordemos que Los Hornos era un barrio donde había ladrilleras y cavas, por lo que los vecinos sufren anegamientos constantes”.

**Trabajo en equipo**

En cuanto al inicio del proyecto, comenzó meses atrás con la conformación de la Red Barrial, un espacio participativo donde el municipio reúne a referentes comunitarios, organizaciones de la sociedad civil, funcionarios y equipos territoriales municipales. En ese ámbito se planificó en forma conjunta la estrategia de intervención territorial.

“Al igual que en todos los proyectos de intervención integral que lleva adelante el municipio, también en este caso armamos mesas de trabajo con los vecinos y vecinas e instituciones barriales para delinear acciones conjuntas”, señaló Pallero. “Hubo reuniones y talleres específicos sobre distintos aspectos del proyecto para que la comunidad sea la protagonista central de las transformaciones y se apropie del proceso. Vamos a seguir con esta metodología de trabajo, que es la que venimos aplicando en todos los barrios donde intervenimos y que tiene que ver con la participación de las familias”.

Cabe destacar, que el barrio Los Hornos está inserto en una condición urbana favorable ya que está delimitado por avenidas importantes, un entorno externo consolidado y con una gran cobertura de servicios e infraestructuras. Sin embargo, hacia el interior posee un tejido denso en condiciones precarias, sin servicios básicos y áreas que presentan degradación ambiental.

Las tareas de relevamiento territorial se realizaron con la participación de organizaciones sociales, vecinos, vecinas y referentes barriales para detectar las demandas, necesidades y, a la vez, recolectar información sobre el estado general del barrio para terminar de definir un diagnóstico urbano, social y ambiental.

**Obras propuestas**

Las obras proyectadas en Los Hornos apuntan a la integración socio urbana del área mencionada, de modo de brindar accesibilidad y conectividad con el entorno.

Las tareas priorizan los ejes circulatorios viales, el ordenamiento urbano, la rectificación y entubado de desagües, el completamiento de servicios sanitarios básicos y el mejoramiento del espacio público.

Red vial: se prevé el completamiento de la red vial a los fines de promover la conexión urbana con el entorno consolidado que presenta el sector. De esta manera, se consolidarán las calles Dr. Zavalla, Gorostiaga, Lavaisse, Llerena, Huergo y Ruperto Godoy. Al mismo tiempo se prevé consolidar los pasajes con el sistema manda peatón, como un sistema de circulación secundaria.

Red peatonal: se consolidará el sistema de red peatonal en la totalidad del sector.

Desagües pluviales: se ejecutarán las conexiones necesarias para garantizar el escurrimiento de las obras viales a realizarse en este proyecto.

Red de agua potable: se garantizará el servicio de red de agua potable a la totalidad del barrio, con las conexiones domiciliarias y canillas de servicio para todas las familias destinatarias de la obra.

Desagües cloacales: extensión y conexión del sistema de desagües cloacales de la totalidad del sector.

Saneamiento ambiental: una gran parte de la población del barrio se dedica a la recolección y clasificación de residuos, a la vez que existe una gran fracción de suelo vacante que funciona como un gran basural que recibe residuos de diferentes sectores de la ciudad. Ante esta situación, se propone la reconversión y puesta en valor de este foco de residuos a través de un equipamiento que posibilite formalizar la actividad.

Alumbrado público por columnas: se proyecta el completamiento del alumbrado en calles, pasillos y pasajes donde el sistema de alumbrado es inexistente. Se resolverá con la colocación de columnas y artefactos LED.

Espacio público: dentro del polígono de intervención no existe un espacio público de calidad. Si bien existe una plaza cercana consolidada, la población del barrio no se apropia de ella. Se propone la recuperación de una de las fracciones de suelo vacante para la construcción de un espacio público deportivo y recreativo.

Escrituración: se prevé la regularización dominial de familias que están dentro del polígono de intervención.

**Otros barrios**

Los mismos trabajos que se realizarán en Los Hornos, se proyectan, además, en los barrios: 29 de Abril, Varaderos Sarsotti y Transporte. “Al igual que en Los Hornos, donde se trabajó junto a las instituciones Los Sin Techo, Nuestra América, Movimiento Solidario y el Movimiento de Trabajadores Excluidos (MTE), en los otros relevamientos vamos a trabajar con la Red de Instituciones para priorizar e identificar los problemas puntuales de cada barrio”, concluyó Pallero.