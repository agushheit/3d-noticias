---
category: Estado Real
date: 2021-11-17T06:00:07-03:00
thumbnail: https://assets.3dnoticias.com.ar/acuicultura.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La provincia contará con recuerdos por 12,8 millones de pesos provenientes
  del Fondo Nacional de Acuicultura
title: La provincia contará con recuerdos por 12,8 millones de pesos provenientes
  del Fondo Nacional de Acuicultura
entradilla: En el marco de la inauguración de la Unidad Demostrativa Agroecológica
  y Piscícola de Monte Vera, se firmó un convenio con Nación a fin de desarrollar
  y potenciar la acuicultura.

---
El ministro de Producción, Ciencia y Tecnología, Daniel Costamagna, participó este martes de la inauguración de la Unidad Demostrativa Agroecológica y Piscícola santafesina, espacio con sede en Monte Vera en el que se desarrollará la reproducción de alevinos para facilitar a productores interesados en expandir la piscicultura en la provincia. Además, el acto permitió la firma de un convenio con el Ministerio de Agricultura, Ganadería y Pesca de la Nación con el objetivo de potenciar y desarrollar la acuicultura en el territorio santafesino.

Participaron del acto también el director nacional de Acuicultura, Guillermo Abdala, la coordinadora del Programa Acuícola Provincial, Eliana Eberle, el director de Producción Pecuaria y Aviar, Rubén Alcaraz y la coordinadora del Acuario Río Paraná, Victoria Mántaras.

Durante el encuentro, Costamagna indicó: “Hoy presentamos una opción que tiene un techo muy alto y que incluye la generación de valor en producciones alternativas de recursos naturales, con la impronta de incluir a los jóvenes, y a las familias en la generación de trabajo”.

El crecimiento de la acuicultura a nivel mundial “es un gran desafío, ya abastece un porcentaje importante del consumo de pescado en el mundo y sin lugar a dudas es un deber pendiente del país y de la provincia de Santa Fe”, aclaró el funcionario.

Abdala, por su parte, resaltó: “En Argentina en el 2020 se comenzó a caminar el desarrollo sectorial de la acuicultura sustentable y en esa sintonía una de las primeras provincias con la que dialogamos fue Santa Fe, que ya tenía un desarrollo, y que se puede consolidar desde los sectores privados, que se dedican a la cría y engorde en piscicultura”.

Finalmente, Eberle expresó: “Hoy es un gran día para la acuicultura, alcanzando unos de los principales objetivos que es abrir centros operativos que funcionen no sólo como centros de capacitación y como gestores de proyectos técnicos, sino también como generadores de las semillas para futuros productores”.

“Vamos a estar generando de manera controlada, bajo condiciones in vitro, alevines que luego alcanzarán el tamaño óptimo para ser sembrados en jaulas flotantes o estanques en tierra. Con 700 kilómetros de costa y una muy buena calidad de aguas y configuración de suelos, Santa Fe tiene un gran potencial para brindar una alternativa para la pesca extractiva, con mucha gente capacitada”, concluyó Eberle.

**CONVENIO CON NACIÓN**

Durante el encuentro, además, se rubricó un convenio con el Ministerio de Agricultura, Ganadería y Pesca de la Nación que permite a la provincia contar con un fondo de 12,8 millones de pesos, dentro del régimen de promoción para la acuicultura, en el marco de la Ley Nº 27.231.

Asimismo, la provincia presentó un proyecto de fortalecimiento de capacidades del sector acuícola en todo el territorio santafesino, mediante el que se propone dar impulso al desarrollo de las capacidades provinciales en diferentes actividades acuícolas que implican la capacitación y asesoramiento continuo a productores e instituciones, la experimentación piscícola, la obtención de alevines de especies de valor comercial nativas y la puesta en marcha de estaciones de producción en sistemas naturales y en estanques como modelos de desarrollo a lo largo del territorio.

A fin de concretar dicho proyecto, se requiere la financiación de 50 jaulas-/unidades piscícolas flotantes, un tanque de transporte de fluidos rompeolas de 3500 litros para el adecuado transporte de “semilla”, insumos y herramientas para la construcción de un muelle como espacio de promoción y diversificación productiva y la cobertura de gastos inherentes a la investigación y ejecución.

Sobre el acuerdo, Costamagna manifestó: “Sin dudas este convenio va a posibilitar fortalecer todo el trabajo que se ha iniciado aquí y en San Javier, con la generación de alevinos y a partir de ahí el desarrollo de las otra etapas, como así también la construcción de jaulas y el trabajo que conlleva, en estanques y en el río”.

En tal sentido, Abdala apuntó: “Hoy estamos financiando a la provincia con un monto superior a los 12 millones de pesos, y estamos visualizando cómo transmitir éste capital a la producción, para generar más empleo, más generación de valor”.

Al respecto, Eberle aclaró que “éstos fondos son para el desarrollo de la acuicultura, destinados a productores, son herramientas clave para el transporte de peces, para generar módulos de incentivo a los pequeños productores, y para poder ampliar infraestructura”.

**ACUICULTURA**

La acuicultura se ha ido desarrollando rápidamente a nivel mundial debido a la acción combinada de un fuerte aumento de la demanda de los productos pesqueros y el cambio de paradigmas en la producción y el consumo a la hora de seleccionar productos sostenibles.

Santa Fe presenta todo el potencial territorial para iniciar un fuerte desarrollo pero nuestros acuicultores necesitan encaminarse hacia los principios de la sostenibilidad, atribuyendo la misma importancia al medio ambiente, a la viabilidad económica y a la aceptabilidad social en su desarrollo actual y futuro.

La acuicultura es la actividad más dinámica en la producción global de alimentos, con una insuperable tasa de conversión de 1,2 kg de alimento para producir uno de pescado. Esta dinámica posibilita un desarrollo rápido en la actividad, una recuperación de la inversión inicial y potencialidades de crecimiento gracias a la demanda.