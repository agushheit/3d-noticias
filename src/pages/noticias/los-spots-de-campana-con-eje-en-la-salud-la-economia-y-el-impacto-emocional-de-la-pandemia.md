---
category: Agenda Ciudadana
date: 2021-08-09T06:00:20-03:00
thumbnail: https://assets.3dnoticias.com.ar/vidaleta.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Los spots de campaña, con eje en la salud, la economía y el impacto emocional
  de la pandemia
title: Los spots de campaña, con eje en la salud, la economía y el impacto emocional
  de la pandemia
entradilla: Las distintas fuerzas dieron a conocer videos en los que enfatizan sobre
  los temas que les preocupan o destacan aspectos de los perfiles de sus candidatos.

---
Los spots de campaña rumbo a las PASO del 12 de septiembre de cada coalición y partido político, con mayor o menor presupuesto, y en algunos casos con el asesoramiento creativo de publicistas, combinan las alusiones a la salud y a la economía, en esta ocasión con el particular agregado del impacto emocional que está produciendo la pandemia de coronavirus.

El primer spot en radio y televisión del Frente de Todos se titula "Estamos empezando a salir", difundido desde hace unos días, y apunta a que en medio de la pandemia y con la campaña de vacunación en marcha "se está empezando a salir hacia la vida que queremos", explicaron a Télam fuentes de los especialistas en la comunicación del FdT.

En el trabajo se ven argentinos de todas las edades reencontrándose y volviendo a las actividades habituales tras la etapa global del virus.

"Empezamos a salir"-FdT

"Hay un mensaje positivo, de esperanza y que también brinde confianza en el rumbo que se eligió en 2019. La mayoría de los argentinos saben y comprenden que la pandemia es algo que le pasó al planeta y por eso lo fundamental ahora será decirle a la gente cómo vamos a salir de la pandemia, el futuro", señalaron.

En ese sentido, los voceros reafirmaron que la línea discursiva de la campaña del FdT -que postula a Victoria Tolosa Paz en la provincia de Buenos Aires y a Leandro Santoro en CABA- se basará en mostrar las medidas "para continuar alentando el consumo y reactivando la producción y la obra pública que genera trabajo y enciende la economía".

"La gente está esperando que el Gobierno le diga con claridad cómo se va a salir de la pandemia y eso es lo que se va a ver en los spots", definieron sobre la campaña en la que a cargo de la estrategia general está Juan Courel y como creativo el publicista Leandro Raposo, fundador de la agencia Cyranos.

Courel fue secretario de Comunicación Pública de la provincia de Buenos Aires y presidente del Consejo Consultivo de los Medios Públicos de la República Argentina; además coordinó la comunicación de las campañas presidenciales de Alberto Fernández en 2019 y de Daniel Scioli en 2015.

También manifestaron que están confiados en que 2022 "va a ser un año mucho mejor" que el actual y por eso buscan transmitírselo a la gente "que se contagie de optimismo". Asimismo, aclararon que desde el oficialismo "no se hará una utilización política de la pandemia".

Desde el FdT evaluaron que la oposición "intenta camuflar la reciente experiencia de gobierno: esconden a (Mauricio) Macri, se cambian el nombre y quieren confundir al electorado porque, en realidad, lo que en esta elección se está poniendo en juego es cómo salimos de la pandemia, si para adelante con más producción y trabajo o si volvemos al pasado", en referencia al gobierno anterior.

Por su lado, Juntos por el Cambio -que en el distrito bonaerense se abrevió en Juntos- eligió estilos diferentes para sus dos precandidatos a diputado: Facundo Manes y Diego Santilli.

Así, Santilli se muestra acompañando siempre al jefe de Gobierno porteño, Horacio Rodríguez Larreta, quien también aparece como eje central de la campaña capitalina, con una narrativa que hace eje en los conceptos de "escuchar", "hacer", "orden" y "libertad", según se pudo ver en el primer spot, llamado "Experiencia", y que justamente busca mostrar la gestión de gobierno porteña.

"Arrancamos"-Diego Santilli

"Santilli pone su voz como pone el cuerpo en la gestión, y finaliza con un compromiso que hace que todo sueño sea alcanzable: trabajar, trabajar y trabajar", reseñaron desde su entorno sobre la frase final del video, que fue producido por el equipo de comunicación del exvicejefe de gobierno porteño junto al publicista Carlos Pérez (cabeza de la agencia BBDO Argentina),

Manes, en cambio, eligió grabar su spot en la localidad bonaerense de Salto, la ciudad en la que se crió y pasó sus primeros años, donde el dueño de la imprenta que le dio su primer trabajo y un amigo de la infancia lo describen como "uno más" entre los vecinos y una persona de bien, con frases como "la honestidad no se aprende, la traés o no la traés".

"Dar el paso"-Facundo Manes

El cierre es de Manes, que promete: "La única cosa que no voy a cambiar es mi manera de ser".

Desde su equipo de campaña indicaron que, en el spot, ideado por el creativo Gastón Douek, el neurocientífico "ratifica su rol de outsider", ya que "no habla como un político sino como un tipo común", mientras "pone la pelea en otra dimensión, que no es la grieta y sale del laberinto por arriba".

En la CABA, Juntos por el Cambio también lleva propuestas muy distintas. En un spot completamente liderado por Larreta se presenta a la precandidata María Eugenia Vidal como la persona "con el compromiso y la dedicación de siempre" y como "la mejor combinación de futuro y experiencia", todo bajo el eslogan "Es lado a lado".

"Para mí, María Eugenia es amistad, es empatía, es poner el cuerpo", la elogia su jefe político, mientras en un segundo spot Vidal aparece en la gestión porteña y dialoga con el ministro de Salud, Fernán Quirós (el funcionario de mejor imagen del oficialismo en el distrito), y el personal del SAME.

Siempre dentro de ese espacio, el candidato de Republicanos Unidos, Ricardo López Murphy, sorprende con un spot que marca: "En Argentina todos tenemos un López", una pieza audiovisual que lo destaca a él mismo, en particular, como "un López de esos que no tiene prontuario" y con "un archivo que resiste más de 40 años".

"Es un López como el tuyo, como el mío, vecino, trabajador, compañero", asegura la pieza, que promete "un Congreso fuerte" con la presencia del exministro de Defensa y de Economía de la Alianza que tuvo un enfrentamiento con las filas de su propio partido de origen -la UCR- cuando promovió un ajuste en el sistema universitario.

"Todos tenemos un López"-Ricardo López Murphy

En su segundo spot, López Murphy mira a cámara mientras cuenta los ejes de su propuesta, con el foco en que "si seguimos votando a los mismo, los problemas no sólo serán los mismos sino que van a empeorar". Además, se muestra junto a las fuerzas de seguridad, las víctimas del delito y las Pymes.

Finalmente, con un video muy sencillo, que arranca con frases conocidas del fallecido expresidente Raúl Alfonsín como aquella del "un médico allí a la izquierda en la tribuna" u otra en la que proponía doctores para "curar" el país, los radicales porteños rebeldes a su conducción local desafían el aparato de Rodríguez Larreta con la postulación del exsecretario de Salud del macrismo Adolfo Rubinstein y la lista denominada "Adelante Ciudad".

"Muchos dicen que la Argentina no tiene cura y encima nos agarró la pandemia. Tal vez ya recurrimos a muchos curanderos y es hora de que nos convoquen a los médicos", enuncia Rubinstein tras las palabras de Alfonsín.

Aparece, entonces, el actor Luis Brandoni (número 13 de esa lista, pero su cara más conocida) que pide "Poné adelante la salud. Rubinstein en la Ciudad. Manes en la Provincia", en un guiño al electorado cercano al neurólogo y precandidato de la UCR bonaerense.

El spot fue realizado por la agencia Persuasión, encabezada por Martín Baintrub y Carlos Falco, indicaron a Télam desde "Adelante Ciudad".

También los partidos de izquierda difunden en medios radiales y televisivos spots de campaña con sus propuestas.

La precandidata a diputada nacional del Nuevo MAS por la provincia de Buenos Aires, Manuela Castañeira, presenta cinco spots, cuya dirección estuvo a cargo de Damián Finvarb, en los que se manifiesta sobre los principales ejes de su campaña.

Allí propone "un salario mínimo de 100.000 pesos", plantea una salida anticapitalista para que la juventud tenga derecho a un futuro, rechaza el extractivismo contaminante y la explotación petrolera en la Costa Atlántica, acompaña los reclamos sociales y políticos y propone renovar la izquierda con nuevas candidaturas y un debate estratégico que permita ser alternativa.