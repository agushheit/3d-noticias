---
category: La Ciudad
date: 2021-04-22T07:55:11-03:00
thumbnail: https://assets.3dnoticias.com.ar/la-recoleccion-residuos-barrios-la-ciudad-trae-controversia.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Diario Uno
resumen: 'Residuos: "Es un retroceso ambiental, sanitario y social"'
title: 'Residuos: "Es un retroceso ambiental, sanitario y social"'
entradilla: 'Lo dijo el concejal Carlos Pereira de Cambiemos y aseguró: "Es una gran
  mentira que nunca hubo recolección diferenciada de residuos"'

---
Desde el año pasado la Municipalidad de Santa Fe decidió hacer cambios en el sistema de recolección de la basura. En doce barrios se dejó de buscar los lunes y jueves los residuos secos, y se armó una estructura en la que se pasa una sola vez por semana en turnos o matutinos o vespertinos. En el resto de la ciudad se sigue con lunes y jueves para secos, pero no llega separado el material a la planta recicladora, aunque alentaron a la comunidad a seguir dividiendo porque "sirve igual".

El concejal Carlos Pereira, del bloque Cambiemos y exfuncionario de la gestión municipal de José Corral, criticó las declaraciones que dio Edgardo Seguro, secretario de Ambiente, a UNO Santa Fe. "La primera cosa que pregunto: ¿hay recolección de secos lunes y jueves? El municipio no tiene una sola publicación, ningún parte de prensa, ninguna publicidad, nada en redes que diga que en la ciudad se sigue recolectando secos lunes y jueves. Si eso se mantiene es lo mismo que nada porque sin difusión es como que no existiera", disparó el radical.

• LEER MÁS: ¿Sabías que cambiaron los días de recolección de residuos secos y húmedos en 12 barrios?

PUBLICIDAD

  

Cabe destacar que en el Concejo Municipal hay pedidos de informes a la Municipalidad presentados, pero no recibieron ninguna respuesta. Asimismo los bloques peronistas y de Cambiemos han pedido la presencia en el recinto de Seguro a partir de este problema pero hasta el momento solo se encuentran con evasivas.

"Lo que nosotros estamos diciendo es que se reemplazó un sistema universal (para toda la ciudad), con sus pros y contras, por experiencias pilotos que consisten en un camión en nueve barrios, un día a la semana y unas 12 campanas ubicadas en el espacio público que casi es un chiste. Una política seria de campanas exigiría no menos de 400 puntos en la ciudad", aseguró.

El concejal Carlos Pereira

El concejal Carlos Pereira

Seguro dijo que en la ciudad de Santa Fe "nunca hubo recolección diferenciada". Pereira respondió esta declaración de manera contundente: "Es un gran mentira. Se puede ver en las redes viejas del municipio, eran continuas las publicaciones y había mucha publicidad en medio. Que el nivel de cumplimiento de la gente era bajo en algunos barrios es una cuestión. Pero en otros era alto. Ahí entra otro tema que es el compromiso de la gente para hacer algo y donde no hay forma de intervención estatal".

"Es cierto que ante el incumplimiento de algunos finalmente todo se mezclaba, pero el resultado final era mucho mejor que lo que pasa actualmente. La propia Asociación Dignidad y Vida Sana puede dar fe que lo que le está entrando y lo que pueden reciclar es mucho menos que antes. Creo que una alternativa sería tener un camión que pasa un día a la semana en otro horario y campanas desparramadas en toda la ciudad. Pero hay que hacerlo en toda la ciudad y no en el 10% del territorio. El 90% de los vecinos que hoy quieren reciclar no tienen alternativa de disposición final. Por eso le va tan bien a campañas como los Tres Sustentables porque hay gente que junta y no sabe dónde llevarlo. Podrían llevarlo a campanas pero les queda muy lejos. La ciudad tiene 87 vecinales y solo nueve están con esta experiencia piloto", agregó.

Y sentenció a Seguro: "Da como un logro que la gente junte en la calle. Es un enorme retroceso, desde lo ambiental, desde lo sanitario y desde lo social. Una cosa es organizar a través de asociaciones o cooperativas que se haga en forma organizada y con todos los recaudos. Nosotros habíamos logrado en calle San Martin, una asociación pasaba comercio por comercio con carros manuales recolectando cartón y papel y lo llevaban hasta un camión que los esperaba en una de las calles aledañas. Justificar el cirujeo en la pobreza es inadmisible".

Por otra parte atendió otra de las afirmaciones del secretario de Ambiente, quien pronunció que "10 años después la planta tiene problemas", por el uso que se le dio durante la gestión Corral. "La planta tiene problemas todos los días. Es un equipamiento que se vive rompiendo y hay que mantenerlo en forma permanente. Eso antes se hacía. Hoy no se hace y casi nada funciona en la planta. Decir que es un equipamiento viejo es una excusa para justificar que no reparan nada (lo mismo pasa con los camiones de la Municipalidad, o las grúas de alumbrado publico o con las motoniveladoras)", aseguró.

"La política de 12 campanas, nueve barrios y eliminar la recolección diferencia lunes y jueves, no fue una consecuencia del Covid. Ya había arrancado antes de la pandemia. Decir que «la planta no tiene capacidad de procesar todo junto si entrara toda la ciudad», es otra excusa. Lo cierto es que hoy estamos por debajo de lo que se trataba durante 2019, con capacidad ociosa. La propia Asociación dice que han procesado menos. Es una mentira que han procesado y vendido lo mismo. Jatón prometió que iba a licitar ni bien llegue al gobierno. Y ahora parece que se olvidó del tema. Algunos concejales lo están reclamando también", concluyó Pereira en diálogo con UNO.