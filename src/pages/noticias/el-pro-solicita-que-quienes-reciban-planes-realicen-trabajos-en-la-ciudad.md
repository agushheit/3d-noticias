---
category: La Ciudad
date: 2021-07-13T08:28:45-03:00
thumbnail: https://assets.3dnoticias.com.ar/SM-Mastropaolo-Ceresola-120920.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: El PRO solicita que quienes reciban planes realicen trabajos en la ciudad.
title: El PRO solicita que quienes reciban planes realicen trabajos en la ciudad.
entradilla: Concejales del PRO santafesino ingresaron una iniciativa para que las
  personas que reciben subsidios del Estado, como contraparte, deban realizar alguna
  tarea para el municipio.

---
Este proyecto elaborado por la Concejal Luciana Ceresola y su par de bancada Sebastián Mastropaolo (PRO-Juntos por el Cambio), tiene por objeto que las personas beneficiarias de planes o ayudas sociales realicen tareas de limpieza, mantenimiento, jardinería, reciclaje o cuidado del medio ambiente en espacios públicos de nuestra ciudad, EN favor de la dignidad del trabajo y para reforzar la cultura del esfuerzo.

El Programa RE.CU.PE.RAR está dirigido a todas las personas mayores de edad radicadas en la ciudad de Santa Fe que reciben una asistencia monetaria, ya sea por parte del Estado Nacional, Provincial o Municipal.

En tal sentido Ceresola explicó que “es fundamental que toda persona deba realizar una tarea o actividad como condición necesaria para poder acceder al beneficio de cualquier programa de asistencia estatal”. Y destacó enfáticamente que “es importante recuperar la cultura del trabajo que últimamente ha sido tan denostada. Los planes sociales deben ser un puente para las personas que carecen de un trabajo formal hasta que lo consigan. Mientras tanto deben hacer una contraprestación, eso hace a la dignidad de las personas que lo reciben y a la equidad en la distribución de los recursos públicos.”

En los últimos 20 años nuestro país incrementó la cantidad programas y planes de asistencia social en gran número. Hoy el 55% de los argentinos cobra algún tipo de ayuda del Estado, la mayoría sin realizar ninguna tarea devolutiva. “Con estas políticas que no tienen límites en el tiempo y no se exige una contraprestación a cambio, se deteriora la búsqueda del esfuerzo y la realización personal de los individuos. Sólo se fomenta un asistencialismo clientelar nocivo para la sociedad”, concluyó la Concejal del PRO Luciana Ceresola.