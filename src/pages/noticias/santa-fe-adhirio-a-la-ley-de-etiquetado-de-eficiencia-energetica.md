---
category: La Ciudad
date: 2020-11-28T13:23:26Z
thumbnail: https://assets.3dnoticias.com.ar/PACO-GARIBALDI.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Eficiencia Energética
title: Santa Fe adhirió a la ley de Etiquetado de Eficiencia Energética
entradilla: La ciudad se convierte en la primera de la provincia en tener una norma
  propia.

---

El etiquetado de viviendas (o certificación de eficiencia energética de viviendas), tiene como objeto brindar a la ciudadanía una herramienta adicional de decisión a la hora de alquilar o comprar un inmueble. 

Calificar energéticamente una vivienda significa conocer cuánta energía necesitaría la misma para satisfacer los requerimientos de calefacción en invierno, refrigeración en verano, producción de agua caliente sanitaria e iluminación durante un año y bajo condiciones estandarizadas de ocupación y utilización. 

La iniciativa adhiere a la Ley Provincial Nº 13.903 y está orientada a adecuar el instrumento normativo que posee el municipio en materia edilicia, conocido como Reglamento de Edificaciones Privadas. Paco Garibaldi, autor del proyecto, expresó: “Queríamos que la ciudad esté a la vanguardia en materia ambiental. 

Esto permitirá que se genere un registro oficial de etiquetadores de vivienda, por ejemplo, lo que implicará la oportunidad de creación de más empleos verdes, que son los trabajos decentes que también colaboran con la sostenibilidad ambiental para profesionales que presten servicios en relación a energías. 

Hoy en día la discusión sobre el consumo de energía está en todos los ámbitos y pensar en un futuro ecologista nos exige que demos pasos firmes con iniciativas como las que acabamos de aprobar”. 

También agregó que: “el proyecto de reglamentación de edificaciones privadas, podrá incorporar incentivos a todos aquellos edificios recién construidos que cumplan con los parámetros de eficiencia energética”, anunciando que todas aquellas construcciones que realicen las reformas que permitan aumentar la eficiencia energética, lograrán obtener estímulos impositivos.

### Eficacia energética: La Ley

La Ley establece un procedimiento de certificación de inmuebles a través de una “Etiqueta” similar a la que vemos en los artefactos eléctricos y electrónicos, es decir, un documento en el que deben figurar los datos mínimos que debe contener. 

Prevé un índice de prestaciones con 7 niveles de escala de eficiencia, a los fines de medir el inmueble según su adaptación e incorporación. 

Una vez calificada la vivienda, se emite un certificado con 10 años de validez asentado en la escritura en el Registro de la Propiedad de Santa Fe, a fin de dar publicidad registral de los inmuebles destinados a vivienda que cuenten con el etiquetado. 

“Como dato, una vivienda con etiquetado de eficiencia A consume casi 10 veces menos que una casa con etiquetado de eficiencia G.

Tener este tipo de información, además de ser beneficioso para el planeta, es un alivio muy importante para el bolsillo, porque permite reducir el costo pagado por los servicios del hogar. 

Son pequeñas cosas que generan grandes cambios: Reducir el consumo energético, aprovechar mejor lo que utilizamos y generar nuevas oportunidades de empleo para los santafesinos”, finalizó el concejal.