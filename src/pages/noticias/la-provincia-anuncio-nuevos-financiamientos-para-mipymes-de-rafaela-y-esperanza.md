---
category: Estado Real
date: 2021-01-27T10:01:30Z
thumbnail: https://assets.3dnoticias.com.ar/pymes.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: La Provincia anunció nuevos financiamientos para mipymes de Rafaela y Esperanza
title: La Provincia anunció nuevos financiamientos para mipymes de Rafaela y Esperanza
entradilla: A través de las asociaciones para el desarrollo comienzan a reutilizarse
  los fondos rotatorios.

---
En el marco del Decreto 355/20, a partir del cual el Gobierno Provincial destinó recursos para que las agencias y asociaciones para el desarrollo del territorio provincial generen fondos rotatorios para otorgar préstamos, la Asociación para el Desarrollo y la Innovación Competitiva Agencia Rafaela (ACDICAR), anunció este martes la puesta en marcha del recupero de fondos y lanza una nueva línea de financiamiento destinada a mipymes de la región, actividad que estuvo encabezada por el ministro de Producción, Ciencia y Tecnología, Daniel Costamagna.

Durante la presentación, llevada a cabo en la sede del Centro Comercial e Industrial de Rafaela y la Región, Costamagna valoró: “Estos créditos que se vienen dando desde hace un año están destinados a emprendedores, a comerciantes y a muchos sectores que han tenido en 2020 un año complejo y difícil producto de la pandemia. En este sentido, llevamos un largo camino recorrido, primero con los aportes no reintegrables de la Asistencia Económica de Emergencia, que ascienden a más de 900 millones de pesos y luego con financiamientos como los que presentamos hoy aquí”.

“La provincia tuvo un resurgimiento de su actividad económica, industrial, comercial y agroindustrial, pero determinados sectores continúan con problemas para los cuales seguimos brindando herramientas y buscando alternativas de acompañamiento. También se trabajó con las mutuales, algo inédito en la provincia, y en ese esquema de trabajo establecimos un fondo de 300 millones de pesos, desde el cual se ha posibilitado que muchas personas, que no tenían acceso a un crédito bancario, hayan tenido la posibilidad de trabajar y mantenerse en pie”, agregó.

Ahondando en las acciones llevadas adelante para apuntalar a las pymes santafesinas, el Ministro también señaló: “El esquema de financiamiento a través de agencias para el desarrollo implicó recursos del Estado Provincial por más de 160 millones de pesos. Al mismo tiempo, Santa Fe fue la primera provincia en el convenio de Fogar, que es un Fondo de Garantía Recíproca, puesto en marcha en conjunto con el Gobierno Nacional y que posibilita otorgar garantías por 2400 millones de pesos”.

Por su parte, el vicepresidente de ACDICAR, Gabriel Gentinetta, manifestó: “Desde el inicio de la pandemia venimos trabajando con el Ministerio de Producción, Ciencia y Tecnología. Teníamos un fondo rotatorio y le empezamos a dar forma para utilizarlo con préstamos para asistir a las empresas. Coincidió con este Programa del Ministerio, a partir del cual se le otorgó a la entidad un aporte de 5 millones de pesos con un fin específico: atender a las empresas afectadas por la emergencia sanitaria. Lo que vimos, en la primera etapa, es que fue muy necesario en los comercios la transformación digital, todo comenzó a reactivarse de esa manera. En ese contexto le planteamos esto al Ministro un nuevo proyecto y le pedimos el acompañamiento. Ese proyecto se aprobó y hoy nos están entregando dos millones y medio de pesos, nosotros lo ampliaremos y junto al Centro Comercial ofreceremos la línea a todos los comercios”.

Del lanzamiento participaron también el secretario de Desarrollo Territorial y Arraigo, Fabricio Medina; el director Provincial de Institucionalidad para el Desarrollo, Iván Camats; el secretario de la Producción local, Diego Peiretti; y el presidente del Centro Comercial e Industrial de Rafaela y la Región, Diego Castro, entre otras autoridades de la región.

**PRESENTACIÓN EN ESPERANZA**

Posteriormente, en el salón Blanco de la Municipalidad de Esperanza, Costamagna junto a la intendenta local, Ana Meiners, participó de la puesta en marcha del recupero de fondos y lanzamiento de una nueva línea de financiamiento destinada a mipymes de la región en la Asociación Civil para el Desarrollo de Esperanza y la región (ACADER).

Del acto participaron también el secretario de Desarrollo Territorial y Arraigo, Fabricio Medina; el director Provincial de Institucionalidad para el Desarrollo, Iván Camats; el secretario de la Producción de Esperanza, Martín Bircher; y el presidente de ACADER, Marcelo Motura, entre otras autoridades

**CARACTERÍSTICAS DE LOS CRÉDITOS**

El secretario de Desarrollo Territorial y Arraigo, Fabricio Medina, informó algunas de las características de las líneas: “Se tratan de créditos de hasta 150 mil pesos, con tasas del 13,5%, con plazo de amortización de hasta 18 meses y con un período de gracia de hasta 6 meses. El destino es para bienes de capital, bienes de usos e intangibles y capital de trabajo para la implementación de mejoras en el negocio y transformación digital.