---
category: Estado Real
date: 2021-01-05T11:47:19Z
thumbnail: https://assets.3dnoticias.com.ar/tostado.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: El Ministerio de Salud declaró a Tostado en cuarentena sanitaria
title: El Ministerio de Salud declaró a Tostado en cuarentena sanitaria
entradilla: Rige a partir de las 0 horas del 5 de enero del 2021 y por el término
  de 14 días.

---
El gobierno provincial, a través del Ministerio de Salud, declaró a la ciudad de Tostado, departamento 9 de Julio, en cuarentena sanitaria. **La medida rige a partir de las 0 horas del 5 de enero del 2021 y por el término de 14 días**.

Durante ese tiempo, **se insta a la población de Tostado a permanecer en su domicilio y limitar sus desplazamientos a los estrictamente necesarios por razones alimentarias, de salud o fuerza mayor**; siendo obligatorio en tales casos el uso de elementos de protección de nariz, boca y mentón correctamente colocados.

De esta manera, quedan suspendidas las excepciones dispuestas por el Aislamiento Social, Preventivo y Obligatorio (ASPO) y las habilitaciones dispuestas por el Distanciamiento Social, Preventivo y Obligatorio (DISPO).

El Ministerio de Salud continuará con el monitoreo de la evolución epidemiológica y sanitaria de ciudad de Tostado, a los fines de recomendar las acciones a seguir conforme la situación lo aconseje.

Las autoridades locales, en concurrencia con la Policía de la Provincia y las demás autoridades provinciales correspondientes, coordinarán los procedimientos de fiscalización necesarios para garantizar el cumplimiento de los protocolos vigentes y de las normas dispuestas en virtud de la emergencia sanitaria, y asegurar que la circulación de personas y vehículos corresponda estrictamente a las actividades habilitadas.

Esta medida responde a lo expresado por el Comité de Crisis Departamental y la Secretaría de Salud de esta cartera, que **teniendo en cuenta la densidad poblacional de la localidad y el número de casos que se han sucedido en un corto plazo de tiempo, se ha comprobado que la movilización de personas aumenta potencialmente la transmisibilidad**.

Por este motivo, se estimó conveniente disponer medidas que posibiliten contener la propagación viral, a los fines de proteger la salud comunitaria y generar una contención sanitaria que permita mayor bloqueo territorial.