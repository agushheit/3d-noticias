---
category: La Ciudad
date: 2021-03-18T07:32:11-03:00
thumbnail: https://assets.3dnoticias.com.ar/suarez.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Carlos Suarez
resumen: Solicitan que se habiliten bailes en eventos con la metodología de burbujas
title: Solicitan que se habiliten bailes en eventos con la metodología de burbujas
entradilla: 'El pedido lo realizó el concejal de UCR-Juntos por el Cambio Carlos Suárez,
  a través de un proyecto de comunicación. '

---
El pedido lo realizó el concejal de UCR-Juntos por el Cambio Carlos Suárez, a través de un proyecto de comunicación. “Se perdieron muchas fuentes laborales relacionadas a salones de fiesta y organización de eventos. Debemos cuidar y  sostener las que siguen en peligro y permitirle a este sector trabajar con protocolos específicos para la actividad que desarrollan. ”, agregó el edil.

El concejal de la UCR-Juntos por el Cambio Carlos Suárez, ingresó un proyecto de comunicación donde solicita al Ejecutivo que gestione ante el gobierno provincial la habilitación del baile por burbujas en eventos organizados en salones de fiesta. “Es ilógico que si a bares y cines vamos en burbujas sociales y familiares, no se permita aplicar el mismo protocolo de burbuja para poder bailar en eventos como cumpleaños de 15, casamientos, recepciones escolares y recitales”, afirmó el legislador. 

Carlos Suárez además hizo hincapié que esta situación también es un duro golpe para quienes desarrollan las actividades relacionadas a eventos como salones de fiesta, servicios de catering, sonido e iluminación, etcétera. “Estos sectores, que son los más golpeados por la pandemia, vienen reclamando hace tiempo que se les autoricen protocolos específicos para poder reactivar la actividad que desarrollan. Por eso nos parece importante que todos nos involucremos en este tema, porque ya se perdieron muchas fuentes laborales en esta actividad de gente que no pudo seguir desarrollándola, pero hay otros que siguen resistiendo y debemos hacer todo lo posible para salvar esos trabajos que hoy están en riesgo”. 

Con relación a la metodología a implementar para que los asistentes a los eventos puedan bailar, el concejal de UCR-Juntos por el Cambio detalló: “La idea es que se realicen tandas de baile por burbujas y respetando el distanciamiento social. Para esto, en la pista deberá haber demarcados cruces y círculos en el piso, donde se ubicarán los asistentes”. Carlos Suárez, además recordó, que quienes desarrollan la actividad, están solicitando que se realice una prueba de 60 días de estos protocolos, para determinar la efectividad del mismo.