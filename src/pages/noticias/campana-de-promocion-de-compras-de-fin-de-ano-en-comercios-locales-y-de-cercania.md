---
category: La Ciudad
date: 2020-12-16T12:01:08Z
thumbnail: https://assets.3dnoticias.com.ar/centro-comercial.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Campaña de promoción de compras de fin de año en comercios locales y de cercanía
title: Campaña de promoción de compras de fin de año en comercios locales y de cercanía
entradilla: Se realizarán sorteos de canastas navideñas y vouchers con órdenes de
  compra. También se coordinaron acciones de prevención para garantizar paseos seguros
  y el cumplimiento de los protocolos sanitarios.

---
La Secretaría de Comercio Interior y Servicios del Ministerio de Producción, Ciencia y Tecnología presentó este martes, en la sede de la Asociación de Comerciantes de Aristóbulo del Valle en la ciudad de Santa Fe, **la campaña de promoción comercial para las ventas de fin de año**, una acción impulsada por el Gobierno Provincial en conjunto con los centros comerciales y paseos a cielo abierto de toda la provincia, que **busca incentivar las ventas locales** y que los santafesinos y santafesinas asistan a comercios de cercanía.

Con el objetivo de acompañar al sector comercial local e incrementar un mayor intercambio entre los clientes con los productores y comerciantes de Santa Fe, a partir de una articulación entre el sector público y privado, se generarán promociones, sorteos y se brindará información sobre los derechos que tienen los consumidores. 

Al tiempo que **se prevé un trabajo conjunto con los ministerios de Salud, Seguridad y Cultura** para que la asistencia a los centros comerciales de cercanía sea cómoda, segura y garantizando el cumplimiento de los protocolos, evitando aglomeraciones que pongan en riesgo a los clientes.

A partir de esta campaña, a cada centro comercial, cámara de comercio o centro económico, y a los paseos comerciales a cielo abierto, <span style="color: #cf0000;">se les entregará una caja navideña con productos regionales santafesinos para ser sorteada, un voucher de $10.000 y dos de $5.000, para orden de compra. Todo ello será sorteado y las órdenes de compra serán para aplicar en comercios asociados o parte de los paseos.</span>

Durante la presentación, el secretario de Comercio Interior y Servicios, Juan Marcos Aviano, explicó que "estamos iniciando la campaña Comprá Santa Fe, con acciones de sorteos, de beneficios, y acompañando desde la provincia a cada paseo comercial a cielo abierto de las principales ciudades y a cada entidad comercial de 70 localidades a lo largo y ancho de todo el territorio provincial. Se prevé la entrega de vouchers con órdenes de compra para realizar sorteos, cajas navideñas con productos fabricados por empresas y trabajadores santafesinos".

"Estamos acompañando al sector comercial, organizando también las **campañas que tienen que ver con salud, seguridad y cultura**, sabiendo que la policía en las calles también acompaña para que las compras sean más seguras, que el consumidor y el comerciante se sientan protegidos y acompañados. Con el Ministerio de Salud estamos organizando acciones de información por el Covid-19 y entregando barbijos a los transeúntes; también con los artistas callejeros, promoviendo el compromiso ciudadano”, agregó.

Por último, el funcionario provincial indicó que “se trata de distintas acciones que buscan, como nos ha indicado el gobernador Omar Perotti, un repunte de todo el arco productivo de la provincia, sobre todos aquellos sectores más complicados y comprometidos por la emergencia sanitaria y la profundización de la crisis económica. Esperamos cerrar un año con el respiro de que los comerciantes puedan vender más en estas fiestas, deseando que el 2021 sea el año de la recuperación de toda la economía de la provincia”.

Por su parte, el titular del Centro Comercial de la ciudad de Santa Fe, Martín Salemi, manifestó que "esta tiene que ser una de las tantas actividades que tenemos que realizar y que cuenta con el acompañamiento de la Secretaría de Comercio. En 2021 necesitamos recuperar el consumo. Todas las políticas que estamos trabajando con la provincia van en ese sentido y creemos importante llegar a los primeros meses del año próximo con un incremento del consumo. Cada Asociación tendrá su forma de implementar el sistema de sorteos y lo explicará a sus asociados”.

Finalmente, la titular de la Asociación de Comerciantes de Avenida Aristóbulo del Valle, Graciela García, dijo que en el caso de esa arteria comercial van “paso a paso, avanzando y trabajando mucho desde todos los lugares para que esta navidad sea provechosa para el comercio. Estamos recibiendo este aporte para premiar a nuestros clientes y trabajaremos mucho con las redes sociales. Esperamos las acciones para que nuestras avenidas se vean lindas y tengamos fiestas provechosas, cuidando todos los protocolos”.