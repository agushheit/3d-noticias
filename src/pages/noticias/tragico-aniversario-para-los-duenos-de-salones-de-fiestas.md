---
category: La Ciudad
date: 2021-03-20T06:00:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/salones.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: '"Trágico Aniversario" para los dueños de salones de fiestas'
title: '"Trágico Aniversario" para los dueños de salones de fiestas'
entradilla: 'Los representantes del sector se manifestaron frente a la Estación Belgrano.
  “Todos están trabajando menos nosotros. '

---
Este viernes por la tarde, en la explanada de la Estación Belgrano se manifestaron los dueños de los salones de fiestas y afines al rubro, junto también con algunos propietarios de peloteros quienes, estos últimos, ya empezaron a trabajar con algunas restricciones. La consigna era asistir vestidos de negro y con barbijo.

“Es una especie de festejo amargo y triste tras un año de estar cerrados, nos tienen olvidados”, comenzó diciéndole Andrés Gavilán, referente del sector (que cortó su trabajo habitual una semana antes que el 20 de marzo de 2020) a diario El Litoral. “Todo el mundo trabajando menos nosotros. Hay fiestas por todos lados, pero a este rubro solo nos permiten abrir como bar”, agregó.

Sobre el final Gavilán manifestó: “La ayuda de provincia está, pero no alcanza. Esto ya dejó de ser una forma de protesta. Necesitamos mostrar lo mal que la estamos pasando, ponernos a la par y solidarizarnos con quienes perdieron su trabajo. Todo aquel que tenía un salón de fiestas, el que hacía eventos, el que sacaba fotos, tenía un sueño casi cumplido y lo tuvo que rifar para poder sobrevivir durante un año”.