---
category: Agenda Ciudadana
date: 2021-03-09T06:49:27-03:00
thumbnail: https://assets.3dnoticias.com.ar/paritaria.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La provincia ofreció el 35% de aumento a los docentes
title: La provincia ofreció el 35% de aumento a los docentes
entradilla: El ministro de Trabajo, Juan Manuel Pusineri, explicó que el aumento se
  pagará en tres tramos en los meses de marzo, julio y septiembre. Además, está prevista
  una cláusula de revisión para el mes de octubre.

---
El ministro de Trabajo, Empleo y Seguridad Social, Juan Manuel Pusineri, informó tras la reunión paritaria con los gremios docentes que el Gobierno de Santa Fe realizó una oferta del 35% de aumento salarial para el sector: “La propuesta consiste en un incremento anual de un 35% que se distribuye en tres tramos: un primer aumento del 18% para marzo, un segundo tramo en el mes de julio del 8%, y para septiembre el restante 9%”, detalló.

Además, explicó que para marzo “se contempla la duplicación del monto de material de estudio para que, una vez que sea aplicado ese 18%, se lo vuelva a computar o se lo duplique alcanzando la suma de 1.357 pesos”. Por otra parte, está prevista una cláusula de revisión para el mes de octubre: “Atendiendo a la evolución de la inflación y del salario, se van a analizar las alternativas que existen respecto de estas variables económicas, salariales e inflacionarias que van a estar en juego”.

Con respecto a las negociaciones, Pusineri señaló que la oferta presentada en el día de hoy se viene trabajando desde principios del mes de febrero cuando se iniciaron los primeros encuentros: “Llegamos con un piso de conversación para la mejor oferta que puede formular el gobierno y que ha sido muy conversada, muy discutida y con referencias inequívocas en la cuestión nacional”.

En esta línea, el ministro hizo hincapié en que la principal referencia fue la paritaria docente nacional: “En eso existe una base de consenso, y me parece que ese marco referencial es inevitable en este contexto para el resto de los sectores, porque así también lo hemos conversado con ellos”.