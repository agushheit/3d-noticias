---
category: La Ciudad
date: 2021-11-04T06:15:39-03:00
thumbnail: https://assets.3dnoticias.com.ar/GENDARMERIA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: "¿Cómo son los operativos para frenar el delito en Santa Fe?"
title: "¿Cómo son los operativos para frenar el delito en Santa Fe?"
entradilla: Se cumplió una semana del arribo de 102 efectivos federales. Aseguran
  que ya se realizaron 30 operativos de saturación entre esta capital y Santo Tomé.

---
El lunes de cumplió una semana del inicio de los trabajos en conjunto entre la policía de Santa Fe y fuerzas federales (gendarmes y prefectos). Se trata de los operativos diseñados para ponerle un freno a la escalada de delitos predatorios (robos y asaltos) y aquellos de violencia extrema, como el que ocurrió la semana pasada en Av. Blas Parera y Florencio Fernández.

Hasta el momento son 102 los efectivos federales que arribaron a la ciudad a prestar servicio en materia de seguridad ciudadana junto a la policía santafesina.

El Jefe de la Unidad Regional I de Policía, Martín García, precisó "que desde la llegada de las fuerzas federales se diagramaron operaciones policiales en distintas partes de la ciudad para disuadir, prevenir y desalentar todo lo que es el delito predatorio".

En esa línea, describió que "en total, por operativo que realizamos en conjunto, son alrededor de 60 efectivos (entre fuerzas federales y provinciales). Tienen distintas modalidades: chequeos (junto a personal municipal) y móviles en distintos puntos de la ciudad".

Precisó que las tareas se centralizaron en primer lugar en cuatro barrios. "Los saturamos por un horario determinado y después nos trasladamos a otros puntos. También realizamos algunas intervenciones en la ciudad de Santo Tomé. En total se han realizado más de 30 operativos entre las dos ciudades con la afectación de más de 60 efectivos cada uno".

Por otro lado, detalló que en algunos barrios ya se observa un descenso de los delitos. "Hemos puestos una base de la policía de acción táctica en Playa Norte, en un destacamento que era de la seccional octava. Lo hemos transformado en una base operativa. Se dotó de un móvil policial y caminantes que trabajan en todo lo que es dicha seccional".

Se trata de una de las zonas más calientes de Santa Fe, donde el delito predatorio aterra a los vecinos de barrios como Coronel Dorrego y Guadalupe Oeste. En ese sentido, y según García, "en Coronal Dorrego ha bajado (el delito) con el servicio de caminante que hemos implantado y los distintos móviles que tenemos en el lugar".

"Ayer se realizó el operativo multiangencial en barrio Acería, el cual se va a extender por un tiempo determinado, donde tenemos recursos de la Unidad Regional, de Acción Táctica, Seguridad Vial, Comunitaria; también participa el MPA. La Agencia de Investigación Criminal realizó más de 18 inspecciones de talleres de motos, autos y casas de celulares. Nosotros realizamos la saturación del barrio con el secuestro de un arma de fuego, de una tumbera", añadió en declaraciones al programa "De 10" que se emite por "LT10".

Indicó que "los dos delitos que estamos tratando de atenuar son los predatorios, que se da más en la zona del macrocentro. También el tema de los enfrentamientos entre distintas bandas que se está produciendo en el norte de la ciudad".

En esa línea, consultado sobre la balacera de Blas Parera, el jefe de la URI comentó: "Estamos teniendo la resolución de conflictos interpersonales de una manera muy violenta, conflictos que se dirimen lamentablemente a través de enfrentamientos armados, de arma blanca y de fuego"

Para García, los niveles de violencia en la ciudad se bajan "con una buena prevención, con las intervenciones barriales, y con una buena investigación". Indicó que "en estos operativos se suma la Agencia de Investigación Criminal y la Policía de Acción Táctica. Las investigaciones que realiza la Agencia de Investigación Criminal están orientadas al narcomenudeo. Estamos tratando de disuadir los conflictos interpersonales que se están produciendo a través de estas saturaciones".