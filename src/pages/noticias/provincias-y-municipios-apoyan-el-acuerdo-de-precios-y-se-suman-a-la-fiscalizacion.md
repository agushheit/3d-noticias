---
category: Agenda Ciudadana
date: 2021-10-22T06:15:42-03:00
thumbnail: https://assets.3dnoticias.com.ar/kissi.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Provincias y municipios apoyan el acuerdo de precios y se suman a la fiscalización
title: Provincias y municipios apoyan el acuerdo de precios y se suman a la fiscalización
entradilla: La decisión del Gobierno nacional de controlar y hacer cumplir la resolución
  que congela por 90 días los precios de 1.432 productos de consumo masivo sumó este
  jueves el apoyo de provincias y municipios.

---
Las reacciones en las provincias:  
  
**Provincia de Buenos Aires**

El gobernador bonaerense Axel Kicillof apoyó que "se tomen cartas en el asunto para que nadie se haga el piola" para "cuidar el bolsillo de la gente" y consideró que "no hay justificativo para el aumento de los precios con estos costos".  
  
En declaraciones a radio La Red y El Destape, Kicillof razonó que "cuando la economía crece rápidamente -porque la demanda y el consumo estaban interrumpidos por los cierres generados por el cuidado de la salud- hay que mirar con mucho cuidado el tema de los precios".  
  
Por su parte, el ministro de Producción, Ciencia e Innovación Tecnológica bonaerense, Augusto Costa, afirmó que la resolución del Gobierno nacional implica "dar una señal a todos los consumidores, a todos los que necesitan tener previsibilidad de precios".  
  
"Va a haber un Estado cuidando los bolsillos durante los próximos tres meses para evitar que haya abusos, para evitar que haya aprovechamiento y confiamos en el compromiso de los empresarios para cumplir lo que necesitamos, que es que sean responsables", sostuvo en una visita a Bahía Blanca.  
  
Varios intendentes bonaerenses manifestaron este jueves su intención de sumarse al control de la medida, como los jefes comunales de Ensenada, Mario Secco; Morón, Lucas Ghi; y Pilar, Federico Achával.  
  
Secco aseguró que ordenará a los inspectores municipales a realizar un estricto control de precios para que en caso de incumplimientos en el acuerdo de precios por parte de los empresarios, "sacarle tarjeta roja a los vivos".  
  
Ghi afirmó que los jefes comunales tienen "la obligación institucional" de controlar que se cumpla con el acuerdo dispuesto por la Secretaría de Comercio Interior, puesto que la medida busca "que la comunidad pueda consumir a precios razonables".  
  
Achával dijo que los municipios intentarán que se cumpla la decisión del Gobierno nacional para "lograr que esos productos estén en la góndola al precio fijado".  
  
Junto a los intendentes, la Defensoría del Pueblo provincial irá "al territorio" para "controlar que se cumplan los precios" acordados no solo para demostrar el "compromiso del Estado" en esa acción sino también para que "los ciudadanos se animen a denunciar", prometió su titular, Guido Lorenzino.  
  
**Santa Fe**

En tanto, el secretario de Comercio Interior y Servicios de Santa Fe, Juan Marcos Aviano, afirmó: "Entendemos debe ser el ámbito en el cual nos convoquen, para organizar y articular la manera de llevar adelante esto".  
  
"Creo que se puede lograr, como se venía logrando con el programa Precios Cuidados", señaló Aviano, quien aclaró que la provincia comenzará las conversaciones con "los municipios y comunas que quieran articular y sumarse a la fiscalización".  
  
**Mendoza**

Desde Mendoza, el diputado nacional José Luis Ramón le dijo a Télam que "sería excelente" si convocan a los consumidores, representados a través de las asociaciones de consumidores.  
  
"Las asociaciones de consumidores tienen todas las posibilidades de hacer el relevamiento de los controles: el del observatorio de precios que es la cadena de comercialización que nace en la tranquera de un campo tambero y puede seguir toda la cadena de comercialización hasta que la leche llega a la góndola", afirmó.  
  
**Chubut**

Por otra parte, el ministro de Agricultura, Ganadería, Industria y Comercio de Chubut, Leandro Cavaco, confirmó que la provincia "se sumará al monitoreo de control de precios en línea con lo que dispuso el Gobierno nacional".  
  
"Estamos esperando que formalmente nos den el listado de los productos para controlar y ahí estaremos, tal como lo venimos haciendo con el programa de Precios Cuidados", dijo el funcionario.  
  
**Jujuy**

Desde Jujuy, el director de Control Productivo y Comercial, Carlos Luque, afirmó que "la provincia habitualmente hace control interno de precios cuidados y eso lo vamos a seguir haciendo".  
  
"Como provincia veríamos con buenos ojos que nos den la capacidad de sancionar la ausencia de productos, tener la potestad de sancionar a las firmas que no cumplen con el pacto", dijo el funcionario ante la pregunta de Télam.  
  
**La Pampa**

En La Pampa, la directora General de Defensa del Consumidor, Florencia Rabario, confirmó que la provincia "hará un acompañamiento para el cumplimiento y el control de precios" en los productos dispuestos por el Gobierno nacional.  
  
Rabario sostuvo que la medida "era realmente necesaria y lamentablemente tuvo que ser aplicada en virtud de que no hubo acuerdo".  
  
**Advertencias**

El Gobierno nacional renovó este jueves la advertencia de que serán sancionadas las empresas que no cumplan la resolución de la Secretaría de Comercio Interior.  
  
"El Gobierno vigilará la competencia equitativa entre las empresas y actuará con todos los recursos de la ley para sancionar a las que no cumplan la resolución", publicó este jueves -en su cuenta de Twitter- el jefe de Gabinete, Juan Manzur.  
  
Desde el empresariado, el presidente de la Cámara Argentina de Comercio y Servicios (CAC), Mario Grinman, afirmó este jueves que el sector al que representa no es formador de precios sino que constituye el "último eslabón" en la cadena de comercialización.  
  
"Los empresarios queremos ayudar en todo lo que se pueda para que los precios no suban", dijo el representante de la CAC en declaraciones a radio 10 y La Red.  
  
En ese sentido, la portavoz de la Presidencia, Gabriela Cerruti, destacó hoy que 80% de los actores involucrados aceptó el acuerdo de precios y reafirmó que se trata de un diálogo que "está abierto para que el resto de las empresas que tengan diferencias puedan sumarse".  
  
Por último, el ministro de Desarrollo Social, Juan Zabaleta, instó a los empresarios a "reconocer" la asistencia que el Gobierno nacional les otorgó durante los primeros meses de la pandemia de coronavirus con un acompañamiento a esta medida.