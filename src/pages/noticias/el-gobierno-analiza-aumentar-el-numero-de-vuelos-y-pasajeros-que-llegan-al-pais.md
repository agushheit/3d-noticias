---
category: Agenda Ciudadana
date: 2021-09-30T06:00:14-03:00
thumbnail: https://assets.3dnoticias.com.ar/AEROPUERTO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: El Gobierno analiza aumentar el número de vuelos y pasajeros que llegan al
  país
title: El Gobierno analiza aumentar el número de vuelos y pasajeros que llegan al
  país
entradilla: A partir de octubre también se podrían poner en funcionamiento más aeropuertos,
  entre los que se encontrarían Córdoba, Mendoza y Ushuaia.

---
El Gobierno analiza aumentar el número de vuelos que llega al país, al igual que el cupo de pasajeros habilitados a ingresar a la Argentina, además de poner en funcionamiento más aeropuertos a partir de octubre, adelantó el ministro de Transporte, Alexis Guerrera.  
  
En declaraciones realizadas a los periodistas en Casa Rosada, el funcionario aseguró que se están "terminando de definir" los detalles de la flexibilización, luego del anuncio del Gobierno en cuanto al levantamiento de ciertas restricciones a los viajes.  
  
"Seguramente el cupo se va a ampliar", expresó Guerrera y comentó que este miércoles analizó la cuestión con el jefe de Gabinete Juan Manzur.  
  
Luego de un acto en el que se firmaron convenios con municipios entrerrianos para la construcción de infraestructura para el transporte, el ministro también aludió a la posibilidad de que "haya algún otro aeropuerto que pueda operar de manera internacional, lo que nos va a permitir sumar mayor cantidad de pasajeros".  
  
En esa hipótesis mencionó las pistas de Córdoba, Mendoza y Ushuaia, pero advirtió que para tomar una decisión en ese sentido "tiene mucho que ver que (los aeropuertos) estén preparados".  
  
"Nosotros queremos que la gente pueda viajar; somos los primeros que queremos que el país se reactive en el tema turismo y que la gente pueda salir a hacer sus negocios adentro y afuera de la Argentina", remarcó.

![El ministro también aludió a la posibilidad de que "haya algún otro aeropuerto que pueda operar de manera internacional".](https://www.telam.com.ar/advf/imagenes/2016/10/57f3cd1815e34_1004x565.jpg 'El ministro también aludió a la posibilidad de que "haya algún otro aeropuerto que pueda operar de manera internacional".')El ministro también aludió a la posibilidad de que "haya algún otro aeropuerto que pueda operar de manera internacional".

Advirtió que, si bien se está "saliendo de la pandemia" -porque así "lo indican las estadísticas"-, **l**os viajes deben realizarse "de acuerdo con lo que el sistema sanitario del país permita" y en base a las regulaciones que fijan los aeropuertos para evitar un "rebrote".  
  
La última decisión administrativa 793/2021 del Gobierno nacional dispuso un cupo semanal de 11.900 plazas hasta el 5 de septiembre y de 16.100 hasta el 1 de octubre, que equivalen a 2300 ingresos diarios para argentinos que regresan al país, residentes y extranjeros no residentes que hubieran sido autorizados por la Dirección Nacional de Migraciones.  
  
Esos límites **podrían empezar a modificarse a partir del 1 de octubre**, de cara a la apertura de fronteras anunciada para el 1 de noviembre próximo.