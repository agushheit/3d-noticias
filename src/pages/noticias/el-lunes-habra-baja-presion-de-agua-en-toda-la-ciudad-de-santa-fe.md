---
category: La Ciudad
date: 2021-03-13T06:15:57-03:00
thumbnail: https://assets.3dnoticias.com.ar/assa.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: El lunes habrá baja presión de agua en toda la ciudad de Santa Fe
title: El lunes habrá baja presión de agua en toda la ciudad de Santa Fe
entradilla: Se deberá a tareas eléctricas en la sala de bombas de la planta potabilizadora
  a la red de distribución.

---
Aguas Santafesinas informa a los usuarios de la ciudad de Santa Fe que el próximo lunes 15 entre las 6 y las 8 disminuirá la presión de agua potable en toda la ciudad.

El motivo es hacer un cambio de uno de los transformadores de media tensión que alimenta la sala de bombas de la planta potabilizadora a la red de distribución para la revisión y mantenimiento de uno de los equipos existentes.

Posteriormente podría registrarse turbiedad la cual se supera dejando circular agua por unos minutos hasta que recupere su apariencia habitual.