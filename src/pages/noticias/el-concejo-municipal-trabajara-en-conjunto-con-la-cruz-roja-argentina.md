---
category: Agenda Ciudadana
date: 2021-03-31T07:13:11-03:00
thumbnail: https://assets.3dnoticias.com.ar/cruz-roja.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Concejo de Santa Fe
resumen: El Concejo Municipal trabajará en conjunto con la Cruz Roja Argentina
title: El Concejo Municipal trabajará en conjunto con la Cruz Roja Argentina
entradilla: Este martes las instituciones acordaron actividades conjuntas mediante
  la firma de un convenio de colaboración. Plantean capacitaciones en distintos puntos
  de la ciudad de primeros auxilios y cuidados generales.

---
Este martes las instituciones acordaron actividades conjuntas mediante la firma de un convenio de colaboración. Uno de los puntos fundamentales que plantean son capacitaciones en distintos puntos de la ciudad de primeros auxilios y cuidados generales.

El presidente del Concejo Municipal, Leandro González y el referente de Cruz Roja filial Santa Fe Osvaldo Ferrero, junto al vicepresidente Marcos José Bobbio firmaron un convenio de colaboración que busca promover un fuerte compromiso de trabajo para estimular el conocimiento en prácticas de protección y promoción de la salud, así como también, abordar de manera conjunta las problemáticas vinculadas al desempeño en situaciones de emergencias y urgencias.

De esta manera, el convenio establece desarrollar y emprender cursos de capacitación en materia de Reanimación Cardiopulmonar y Primeros Auxilios, contribuir al desarrollo de actividades en lugares más seguros, planes de evacuación y gestión del riesgo y programas destinados a jóvenes, entre otros.

Al respecto, el presidente del Concejo, Leandro González, explicó que “no es la primera vez que el Concejo trabaja junto a una institución tan prestigiosa como la Cruz Roja Argentina, de una reconocida trayectoria a nivel nacional pero también con una marcada participación en distintos momentos difíciles en la ciudad de Santa Fe, como fue la inundación o misma su participación en el marco de la pandemia por Covid-19”.

“Mediante el convenio -afirmó-, buscamos desde el Concejo ser un nexo con la Cruz Roja y la sociedad santafesina, llevando adelante acciones para promover y mejorar la calidad de vida y la salud, prevenir enfermedades, accidentes y brindar más y mejor información a la ciudadanía. Cruz Roja fomenta hace años capacitaciones en torno a la salud, lo cual nos parece importante y fundamental promover, más en este presente marcado por la pandemia”.

**Hacer la diferencia entre la vida y la muerte**

Por su parte, el referente de Cruz Roja filial Santa Fe, Osvaldo Ferrero, destacó que “después de varias reuniones hoy firmamos un convenio marco en cumplimiento de nuestro rol de auxiliar a los poderes públicos, como lo hacemos con todos los niveles del estado. En este caso, el convenio habilita a capacitar y formar a la comunidad en general y particularmente, por ejemplo, a los empleados públicos tanto del Concejo como de otras instituciones. De esta manera, tratar de formar o concientizar que haciendo las capacitaciones en primeros auxilios hay mayores chances de vida y eso es muy importante”.

“Nuestra institución tiene basta experiencia en formar, capacitar y preparar a la sociedad. Estamos convencidos que toda la ciudad tiene que saber primeros auxilios, tiene que saber qué hacer y qué no hacer ante una situación de emergencia. Estar preparados muchas veces hace la diferencia entre la vida y la muerte. Nuestro objetivo es llegar a toda la población y en ese sentido vamos a trabajar”, agregó José Alcántara, coordinador de capacitaciones.

![](https://assets.3dnoticias.com.ar/cruz-roja1.jpg)

![](https://assets.3dnoticias.com.ar/cruz-roja2.jpg)