---
category: Agenda Ciudadana
date: 2022-01-11T06:15:07-03:00
thumbnail: https://assets.3dnoticias.com.ar/BONDI.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Hay 30 choferes afectados por Covid-19 y podría restringirse el servicio
  de colectivos
title: Hay 30 choferes afectados por Covid-19 y podría restringirse el servicio de
  colectivos
entradilla: Es la situación en una de las dos empresas que gerencian el servicio en
  la capital provincial. Entre los que están contagiados y los que son contactos estrechos
  se complica cumplir con las frecuencias. Y

---
La empresa de colectivos Autobuses Santa Fe envió este lunes una nota a la Municipalidad donde manifestó que por el rebrote de contagios de Covid-19, el servicio de transporte público de pasajeros podría verse afectado en los próximos días.

 En la actualidad, del total de personal tienen 30 trabajadores aislados por casos positivos o por ser contacto estrecho, "número muy relevante para nuestra operatividad", señalan. A esta cifra se suman las bajas habituales que se dan en esta época del año, por los períodos de licencias ordinarias, "por lo que la situación es sumamente preocupante", advierten.

 Para contrarrestar estas bajas, aseguran haber incorporados nuevos conductores y reprogramado licencias, pero estas medidas "no están siendo suficientes y la situación tiende a superarnos", explican.

 Por este motivo, avisan al Municipio que las frecuencias podrían verse afectadas "intentando que las demoras sean mínimas y por el menor tiempo posible".

 La empresa Autobuses gerencia las líneas 4, 5, 8, 10, 11, 13, 14, 16, 18 y Ronda B.

 **La respuesta del municipio**

Desde la Municipalidad de Santa Fe indicaron que "por el momento se trata de una nota advirtiendo la posible falta de personal". No obstante, aclararon que en caso de que efectivamente se afecten las frecuencias se tomarán las medidas correspondientes. Eso implicaría, por ejemplo, sanciones a las empresas que deben prestar el servicio.

 A su vez, el municipio indicó que "hasta el día de la fecha el servicio se viene prestando de acuerdo a la grilla de verano".