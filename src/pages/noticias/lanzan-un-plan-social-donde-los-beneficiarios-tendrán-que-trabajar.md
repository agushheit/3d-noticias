---
layout: Noticia con imagen
author: "Fuentes: LT10 / NA"
resumen: Nuevo Plan Social
category: Agenda Ciudadana
title: Lanzan un plan social donde los beneficiarios tendrán que trabajar
entradilla: Así se definió este jueves en una nueva reunión del gabinete
  económico. “Hay que fomentar la generación de empleo", señaló el ministro de
  Desarrollo Social, Daniel Arroyo.
date: 2020-11-19T12:28:23.218Z
thumbnail: https://assets.3dnoticias.com.ar/plan-social.jpeg
---
**El Gobierno lanza un plan social de $9.400 en el que cual los beneficiarios deberán trabajar**.

Así lo definieron esta tarde en una nueva reunión del gabinete económico, que se llevó a cabo en el Salón de los Científicos de la Casa Rosada, y a la que fueron convocados funcionarios relacionados con la cuestión social.

El ministerio de Desarrollo Social puso en marcha el plan Potenciar Inclusión Joven, destinado a jóvenes de 18 a 29 años, que consiste en una beca de $8.500 pesos a quienes presenten un proyecto productivo o social, y el Potenciar Trabajo que busca la reconversión de planes sociales en programas con una contraprestación laboral específica, para casi casi 600 mil beneficiarios.

“El Estado no se retira. De las personas que han cobrado el IFE, una parte ha reingresado al mercado laboral. Los que aún no han podido recuperar sus ingresos son en esencia dos sectores: las y los jóvenes y las personas que se quedaron sin trabajo” explicó el ministro de Desarrollo Social, Daniel Arroyo.

Arroyo afirmó, además: “**Hay que fomentar la generación de empleo y en ese sentido hemos hecho un cambio total de los planes sociales asociándolos una contraprestación laboral específica**”.

En el encuentro se analizó el despliegue de las políticas económicas y sociales que está instrumentando el gobierno nacional para acompañar la recuperación en el nivel de actividad, teniendo en cuenta la situación de los sectores críticos y, también, las dificultades sociales en los sectores más vulnerables.