---
category: Agenda Ciudadana
date: 2022-02-02T11:19:35-03:00
thumbnail: https://assets.3dnoticias.com.ar/desmonte.png
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Greenpeace
resumen: Argentina perdió el equivalente a 12 canchas de fútbol por hora por los desmontes.
title: Argentina perdió el equivalente a 12 canchas de fútbol por hora por los desmontes.
entradilla: “Es muy preocupante el aumento de los desmontes ilegales. Es urgente acabar
  con esta impunidad", advierten.

---
Un informe de Greenpeace revela que durante 2021 casi la mitad de la deforestación del norte se produjo en Santiago del Estero, y más del 80 por ciento fue ilegal. En Chaco las topadoras arrasaron 18 mil hectáreas, a pesar de que los desmontes están suspendidos por la justicia provincial. La organización ecologista volvió a reclamar la penalización de la destrucción de bosques.

“Es muy preocupante el aumento de los desmontes ilegales. Es urgente acabar con esta impunidad, para lo cual se hace necesario que la destrucción de bosques se convierta en delito penal. Estamos ante una emergencia sanitaria, climática y de biodiversidad. Más deforestación significa más cambio climático, más inundaciones, más desalojos de comunidades campesinas e indígenas, más desaparición de especies en peligro de extinción y más enfermedades; un verdadero ecocidio”, señaló Hernán Giardini, coordinador de la campaña de Bosques de Greenpeace.

El reporte de la organización ambientalista realizado mediante la comparación de imágenes satelitales, estima que durante 2021 la deforestación en el norte de Argentina alcanzó las 110.180 hectáreas, lo que equivale a más de cinco veces la superficie de la Ciudad de Buenos Aires. Se desmontaron 52.290 hectáreas en Santiago del Estero, 29.165 hectáreas en Formosa, 18.068 hectáreas en Chaco y 10.657 hectáreas en Salta.

“Cabe destacar que casi la mitad de los desmontes se produjeron en Santiago del Estero. Más grave aún es que más del 80% de la deforestación en dicha provincia fue ilegal, ya que se realizó en bosques clasificados en las Categorías I, Rojo y II, Amarillo, donde no se permiten desmontes. También es importante advertir la ilegalidad de las 18.068 hectáreas deforestadas durante 2021 en Chaco, ya que los desmontes se encuentran suspendidos por la justicia provincial”, señala el informe.

Según datos del Ministerio de Ambiente y Desarrollo Sustentable de la Nación, entre 1998 y 2020 la pérdida de bosques nativos en el país fue de cerca de 7 millones de hectáreas. Las principales causas son el avance de la frontera agropecuaria (ganadería y soja transgénica que en gran medida se exportan a China y Europa) y los incendios forestales.

“Resulta evidente que las multas no son suficientes para desalentar desmontes e incendios forestales; y los responsables rara vez son obligados a reforestar. En muchos casos es clara la complicidad de funcionarios”, advierte el reporte de Greenpeace.