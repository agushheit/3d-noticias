---
category: La Ciudad
date: 2022-01-21T06:15:56-03:00
thumbnail: https://assets.3dnoticias.com.ar/alerta.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Rige un alerta meteorológico
title: Rige un alerta meteorológico
entradilla: "Según un informe del Servicio Meteorológico Nacional, alcanza a tres
  departamentos de la provincia, incluido La Capital.\n\n"

---
La Municipalidad informa que el Servicio Meteorológico Nacional (SMN) publicó un alerta por lluvias y tormentas que afecta parcialmente a los siguientes departamentos de la provincia de Santa Fe: La Capital, San Jerónimo y Garay. El aviso es para la noche de este jueves.

Según el organismo, el área será afectada por lluvias y tormentas de variada intensidad, algunas localmente fuertes o severas. Las mismas pueden estar acompañadas de intensas ráfagas, granizo, fuerte actividad eléctrica y, fundamentalmente, abundante caída de agua en cortos períodos. En ese sentido, se comunicó que se esperan valores de precipitación acumulada entre 40 y 70 mm, pudiendo ser superados de manera localizada.

**Recomendaciones**

Ante el alerta, se recomienda a los vecinos no sacar a la vía pública objetos o residuos que puedan obstaculizar el normal escurrimiento del agua de lluvia. Además, se recuerda que, después de las precipitaciones, se deben realizar acciones de descacharrado en patios, vaciando o descartando recipientes donde se pudo acumular agua.

Las consultas o reclamos se pueden realizar al Sistema de Atención Ciudadana, llamando al 0800-777-5000.