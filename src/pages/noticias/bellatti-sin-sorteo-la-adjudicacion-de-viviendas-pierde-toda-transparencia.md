---
category: Agenda Ciudadana
date: 2021-01-20T09:31:59Z
thumbnail: https://assets.3dnoticias.com.ar/viviendas.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NCN
resumen: 'Bellatti: “Sin sorteo, la adjudicación de viviendas pierde toda transparencia”'
title: 'Bellatti: “Sin sorteo, la adjudicación de viviendas pierde toda transparencia”'
entradilla: La diputada provincial Rosana Bellatti (Socialistas-FPCyS) mostró su preocupación
  por la falta de precisiones en la metodología a implementar en la adjudicación de
  las nuevas soluciones habitacionales.

---
La diputada provincial Rosana Bellatti (Socialistas-FPCyS) celebró la firma de convenios de colaboración entre el gobernador Omar Perotti y el ministro de Desarrollo Territorial y Hábitat de la Nación, Jorge Ferraresi, para la construcción de más de 4000 viviendas en la provincia de Santa Fe en el periodo 2021, al tiempo que mostró su preocupación por la falta de precisiones en la metodología a implementar en la adjudicación de las nuevas soluciones habitacionales.

Es que según declaraciones vertidas por el funcionario nacional durante el acto de suscripción del acuerdo, se anularían los tradicionales sorteos de vivienda, un proceso que desde hace años garantiza a las familias santafesinas transparencia e igualdad de condiciones en el acceso a la casa propia a través de ayuda estatal.

“Siempre es una excelente noticia la construcción de viviendas, porque no solamente soluciona el problema habitacional de muchas familias que de otra forma se les hace imposible concretar el sueño de tener su vivienda propia, sino también porque sabemos que la construcción es uno de los ejes vertebradores en el crecimiento y desarrollo económico, y en momentos de pandemia como los que estamos atravesando con una fuerte retracción de la economía e innumerables perdidas de puestos de trabajos, representa un verdadero alivio en la situación particular de muchos santafesinos”, señaló la legisladora.

No obstante, Bellatti puso foco en las declaraciones del ministro Ferraresi, quien afirmó que no se hará más por sorteo la adjudicación de viviendas. “Es justamente el sistema que se venía implementando en Santa Fe y que garantiza igualdad de oportunidades a las familias, sobre todo en aquellas personas que no tienen ingresos demostrables. Los sorteos son fundamentales para seguir garantizando la transparencia en todo el proceso, desde la inscripción a la adjudicación”.

La diputada se preguntó “¿quién o cómo se digitarán los beneficiarios de una vivienda estatal?”, sosteniendo que sin esta metodología “la designación de quienes recibirán una vivienda terminará siendo arbitraria, lo que significa un verdadero retroceso para el pueblo santafesino, reapareciendo prácticas ilegales que ya estaban desterradas en la provincia”.

Por otro lado, también preocupa los anuncios en el llamado “recupero”, en lo que respecta descontar de planes sociales y ayudas estatales las cuotas de las viviendas. “Es el Estado cobrándose a sí mismo, que además termina no asistiendo a las familias que realmente lo necesitan”, aseguró la legisladora, remarcando que en un contexto de pandemia descontar del IFE las cuotas de aquellas personas que acceden a un plan de vivienda “es algo innecesario cuando nuestra provincia tienen políticas orientadas a un sector de la población que no tiene ingreso fijo permanente o estable”.