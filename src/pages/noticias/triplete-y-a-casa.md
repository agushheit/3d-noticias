---
category: Deportes
date: 2021-02-15T03:35:03-03:00
thumbnail: https://assets.3dnoticias.com.ar/colon.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Marisa Lemos
resumen: Triplete y a casa
title: Triplete y a casa
entradilla: |-
  Con goles del "Pulga" Rodríguez, Tomás Sandoval y Nicolás Leguizamón, el "Sabalero" se impuso por 3 a 0
  al "Ferroviario".

---
Colón debutó con una goleada 3-0 sobre Central Córdoba en Santiago del Estero. Los goles para el

visitante los hicieron Luis Miguel Rodríguez (12' 1T), Tomás Sandoval (10' 2T) y Nicolás Leguizamón (32'

2T).

El equipo santafesino se puso en ventaja durante el transcurso del primer tiempo con un gol del Pulga

Rodríguez tras un centro raso desde la derecha. El capitán pasó a la defensa y colocó la pelota contra el palo

derecho que nada pudo hacer. En el complemento el visitante pudo aumentar el marcador. Primero Tomás

Sandoval, a los 11, y luego Nicolás Leguizamón a los 34 decretaron el triunfo para el equipo santafesino.

Con este resultado Colón se situó en el primer puesto del Grupo A con tres puntos, lugar que comparte con

Banfield que antes se había impuesto ante Racing. La próxima fecha recibirán a San Lorenzo, mientras que

los de Gustavo Coleoni visitarán a Atlético Tucumán.

En la próxima fecha, Colón recibirá a San Lorenzo el lunes 22 de febrero desde las 21.30.

**FORMACIÓN:**

**CENTRAL CÓRDOBA - COLÓN**:

_Central Córdoba_: Alejandro Sánchez; Gonzalo Bettini, Oscar Salomón, Federico Andueza, Jonathan Bay; Juan Galeano, Cristian Vega, Ariel Rojas, Santiago Rosales; Lucas Brochero y Claudio Riaño.

_Colón_: Leonardo Burián; Bruno Bianchi, Paolo Goltz, Gonzalo Piovi; Alex Vigo o Eric Meza, Rodrigo; Aliendro, Federico Lértora, Santiago Pierotti, Alexis Castro; Luis Rodríguez o Nicolás Leguizamón y Tomás Sandoval.

_Hora_: 21.30

_TV:_ FOX Sports

_Árbitro_: Mauro Vigliano