---
category: Agenda Ciudadana
date: 2021-01-20T09:14:16Z
thumbnail: https://assets.3dnoticias.com.ar/prietto.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de la Provincia de Santa Fe
resumen: 'Covid-19: Nuevo protocolo para identificación de caso positivo y aislamiento'
title: 'Covid-19: Nuevo protocolo para identificación de caso positivo y aislamiento'
entradilla: El secretario de Salud dio detalles sobre la reglamentación vigente.

---
La provincia de Santa Fe a través del Ministerio de Salud informó el nuevo protocolo, para la determinación de caso positivo de Covid-19 y los criterios para el aislamiento de contactos estrechos. 

Al respecto, el secretario de Salud provincial Jorge Prieto explicó que “en función de la dinámica epidemiológica actual de la pandemia, a partir del nuevo protocolo se considera como caso positivo a todo aquel que sea confirmado mediante hisopado nasofaríngeo, con resultado de laboratorio ya sea mediante PCR o test de antígeno (test rápido), y no ya por criterio clínico o nexo epidemiológico”.

Asimismo, se dispone una modalidad de “cuarentena acortada”, para los contactos estrechos de casos positivos.

“En todos aquellos contactos estrechos de un caso confirmado se podrá disminuir el aislamiento obligatorio a 10 (diez) a partir del último contacto con el mismo; siempre y cuando no desarrollen en esos 10 días uno o más síntomas compatibles con la enfermedad”, enfatizó el funcionario

Y agregó Jorge Prieto: “Los 4 (cuatro) días restantes –hasta completar los 14 establecidos en los protocolos anteriores– se deberá monitorear estrictamente la aparición de síntomas, extremar medidas preventivas y de distanciamiento, no participar de reuniones sociales, asegurar la ventilación de los ambientes, no permanecer en espacios con aglomeración de personas, barbijo, lavado de manos, entre otros”.

“Asimismo evitar contacto con personas con factores de riesgo y en caso de inicio de síntomas volver a estricto aislamiento, con el debido seguimiento del equipo médico tratante, sea este público o privado”, concluyó el secretario de Salud.