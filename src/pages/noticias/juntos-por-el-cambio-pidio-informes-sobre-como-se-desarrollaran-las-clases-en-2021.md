---
category: Agenda Ciudadana
date: 2021-01-15T04:24:26Z
thumbnail: https://assets.3dnoticias.com.ar/informe-escuelaJxc.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: '3DNoticias - Fuente: Télam'
resumen: Juntos por el Cambio pidió informes sobre cómo se desarrollarán las clases
  en 2021
title: Juntos por el Cambio pidió informes sobre cómo se desarrollarán las clases
  en 2021
entradilla: Desde el Ministerio de Educación ratificaron que están trabajando junto
  con las 24 jurisdicciones en el regreso a las escuelas en forma presencial y que
  se avanzará en el proceso de vacunación a los docentes.

---
Senadores nacionales del interbloque de Juntos por el Cambio pidieron al Gobierno nacional que explique la política que adoptará sobre el regreso de las clases presenciales del ciclo 2021, en el contexto de la pandemia de coronavirus.

En un proyecto de comunicación, los legisladores pretenden conocer si se realizaron estudios sobre el impacto que la no reanudación de las clases presenciales podría llegar a tener sobre el estado de la educación en cada una de las provincias.

Además, qué medidas se adoptarán para mitigar la afectación y el daño producido en los alumnos durante la pandemia en 2020 y cuál será la postura del Ministerio de Educación en caso de que los sindicatos del sector se nieguen a comenzar las clases presenciales.

«El gobierno tolera playas repletas, casinos, colonias de vacaciones para niños y espectáculos con protocolos y al mismo tiempo pone en duda el inicio del ciclo lectivo y delega la decisión de clases presenciales a las provincias», expresó uno de sus autores, el radical fueguino Pablo Blanco.

El pedido de informes, además, lleva las firmas de Stella Maris Olalla, Silvia Elías de Pérez, Víctor Zimmermann, Pamela Verasay, Mario Fiad, Pedro Braillard Poccard, Alfredo De Ángeli, Laura Rodríguez Machado, María Belén Tapia y Silvia del Rosario Giacoppo, integrantes del principal bloque de la oposición.

En Santiago del Estero, el ministro de Educación, Nicolás Trotta, ratificó hoy que están trabajando junto con las 24 jurisdicciones en el regreso a las escuelas en forma presencial porque es el «organizador de nuestro sistema educativo» y brindó detalles de cómo será el proceso de vacunación a los docentes.

Además, calificó al expresidente Mauricio Macri de «cínico y especulador» por sus dichos para exigir la vuelta a clases.

Trotta detalló que a mediados de 2020 se aprobaron «los protocolos para el regreso a la presencialidad segura» y explicó que en un país federal «es una decisión de las jurisdicciones, pero todos compartimos la necesidad de priorizar la presencialidad y poner en valor los aprendizajes que tuvimos en el 2020».