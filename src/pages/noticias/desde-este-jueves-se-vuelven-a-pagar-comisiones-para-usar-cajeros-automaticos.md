---
category: Agenda Ciudadana
date: 2021-04-02T08:15:08-03:00
thumbnail: https://assets.3dnoticias.com.ar/banco.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: Desde este jueves se vuelven a pagar comisiones para usar cajeros automáticos
title: Desde este jueves se vuelven a pagar comisiones para usar cajeros automáticos
entradilla: Se aplica a las tarjetas de débito que no sean del banco emisor. No tendrán
  costo adicional las operaciones de titulares de cuentas sueldo, jubilaciones y beneficiarios
  de planes sociales.

---
A partir de este jueves las operaciones en cajeros automáticos volverán a tener costo de comisión para terminales que no sean del banco emisor de la tarjeta de débito, aunque seguirá siendo gratuito para titulares de cuentas sueldo, jubilaciones y beneficiarios de planes sociales como la Asignación Universal por Hijo (AUH), entre otras.

La suspensión del cobro de comisiones para las operaciones en cajeros automáticos había sido dispuesta por el Banco Central (BCRA) en marzo del año pasado para facilitar la distribución del efectivo durante el Aislamiento Social, Preventivo y Obligatorio (ASPO) y luego prorrogado en sucesivas ocasiones.

"Sólo los titulares de tarjetas de débito asociadas a cuentas sueldo, pago de jubilaciones o planes sociales podrán seguir utilizando sin costo alguno cualquier cajero, sin importar a qué banco o red pertenecen", anunció el BCRA días atrás en un comunicado. Para esos usuarios, la gratuidad para retirar el efectivo alcanza al monto total del salario, la jubilación o la prestación social, dependiendo del caso.

El resto de los usuarios podrá utilizar gratuitamente solo los servicios de cajero automático prestados por su entidad. En caso de optar por cajeros de otra entidad de la misma red se deberá pagar, en promedio, un cargo fijo de $ 65,95 por operación y, en caso de usar un cajero de otro banco de distinta red, un promedio de $ 77,47.

El Banco Central ya había notificado a mediados de marzo que rehabilitaría el cobro de comisiones por algunas operaciones a partir del 1 de abril, según corresponda en cada caso, tales como el retiro de efectivo en cajeros de distintas redes (Banelco o Red Link) o bancos. De esta manera, el funcionamiento de los cajeros volverá a ser desde hoy el mismo que era previo a la pandemia.

Los costos varían de acuerdo a la entidad de la cual se sea cliente: Nación ($114,23 y $142,78); Galicia ($80 y $95); Macro ($101,04 y $108,90); Santander ($75,52 y $93,21); BBVA ($61,71 y $72,60); Comafi ($78,65 y $93,17); Itaú ($62,65 y $73,41) y Credicoop ($53,24 y $61,71) para el uso de cajeros de misma red pero de otro banco y para uso de otra red, respectivamente.

Los cargos también aplican para operaciones como consultas de saldo en cuenta, movimientos realizados, hacer transferencias o pagar la factura de un servicio públicos, aunque actualmente la mayoría de los clientes realiza esas operaciones de forma online a través de dispositivos móviles o computadoras.

Además de los cerca de 18.000 cajeros automáticos que hay en el país, el BCRA también habilitó el retiro de efectivo en desde supermercados, farmacias, estaciones de servicio, centro de cobranza (como Rapipago o Pago Fácil) y otros comercios adheridos, solo presentando la tarjeta de débito y el DNI, que suman otras 17.000 en todo el país.