---
category: Agenda Ciudadana
date: 2021-01-23T10:15:11Z
thumbnail: https://assets.3dnoticias.com.ar/larreta.png
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: La Política Online
resumen: Larreta quiere sumar a Lifschitz a su proyecto presidencial del 2023
title: Larreta quiere sumar a Lifschitz a su proyecto presidencial del 2023
entradilla: '"Sería un golazo tener al socialista como candidato", repiten en el entorno
  de Larreta que envió a su armador a Rosario.'

---
El secretario general del PRO, Eduardo Macchiavelli, es funcionario de CABA y uno de los armadores del proyecto presidencial de Horacio Rodríguez Larreta. En su paso por Rosario, se reunió con Federico Angelini, los concejales que responden al dirigente santafesino y la mesa partidaria con el objetivo de ponerle fin a la interna en el macrismo local y analizar el escenario electoral.

En ese sentido, hubo unanimidad en la mesa para dejar atrás las diferencias que se arrastran desde hace años entre Roy López Molina y Angelini que derivó en el quiebre del bloque en el Concejo Municipal. Todos coincidieron en la necesidad de encarar un proceso de paz y comenzar a diseñar una estrategia de poder.

Para tal fin, se planteó una hoja de ruta, explicaron desde el entorno de Rodríguez Larreta, que en primer lugar, llegar a un entendimiento entre los dos sectores del PRO y avanzar en acuerdos electorales con los partidos más chicos que integran el Frente Progresista como el Partido Demócrata Progresista que a nivel nacional, su secretaria general Ana Copes acordó hace tiempo con el macrismo.

Por otro lado, aguardan el alineamiento de todos los sectores del radicalismo local a la decisión del Comité Nacional de integrar Juntos por el Cambio, algo que en la provincia está sucediendo desde que el espacio de Pullaro aceleró los vínculos con los correligionarios que ya integran el frente con el PRO como Julián Galdeano, José Corral y Mario Barletta, entre otros.

> **_La UCR de Pullaro presiona a Lifschitz para que avance en un acuerdo con Cambiemos_**

Y por último, la posibilidad de concretar una alianza con el socialismo; "la candidatura a senador de Miguel Lifschitz por un frente común sería un golazo", se entusiasman en el larretismo aunque están al tanto de los obstáculos que existen y lo poco que pueden hacer.

De hecho, en las reuniones que Macchiavelli mantuvo, exclusivamente con dirigentes del PRO, reconocieron que sería difícil que Lifschitz decida renunciar a la presidencia de la Cámara de Diputados para irse a jugar una bancada nacional donde le quitaría territorialidad para pelear en tres años por la Casa Gris.

Además, las presiones que recibe el ex gobernador desde adentro del Partido Socialista santafesino para alejar la posibilidad de un acuerdo. En las últimas horas, el presidente del partido a nivel provincial, Enrique Estévez, volvió a rechazar esa estrategia a través de su cuenta de Twitter.

En la reunión que organizó Maximiliano Pullaro en su casa entre Martín Lousteau y Lifschitz, el socialista se mostró reticente a confluir en un frente con el PRO y reiteró críticas a la gestión de Macri y a sus límites ideológicos como socialista. En el entorno de Larreta recuerdan que en Ciudad de Buenos Aires, Roy Cortina integra el gobierno de la ciudad hace año y en distritos como Mendoza y Jujuy, el PS integra Juntos por el Cambio.

Además, Lifschitz sabe que sin el PRO, las posibilidades de volver al gobierno de Santa Fe son casi nulas en un escenario de polarización y si una parte importante del radicalismo que lo acompañó, ahora decide confluir en Juntos por el Cambio.

Por otro lado, los dos intendentes más importantes de la provincia; Pablo Javkin de Rosario y Emilio Jatón de Santa Fe, que necesitan buena sintonía con el gobernador peronista Omar Perotti, son díscolos al acuerdo con Cambiemos. A eso se le suma que el rosarino mantiene una puja con Lifschitz por el liderazgo del Frente Progresista.

> **_El radicalismo apura el acuerdo con Lifschitz y Cambiemos, pero Javkin busca bloquearlo_**

A priori, Javkin rechaza una alianza con Juntos por el Cambio y amenazan con presentar una lista propia -no descartan tentar a la ex funcionaria nacional, María Eugenia Bielsa- y sacarle votos por izquierda. Pero en el larretismo saben que Javkin buscará renovar en 2023 y para ello, falta mucho.

Por lo pronto, en el entorno de Rodríguez Larreta preparan un armado competitivo para el centro del país y tienen claro que para llegar a la presidencia deberán mejorar la performance en provincia de Buenos Aires y ganar CABA, Santa Fe, Mendoza, Córdoba y Entre Ríos.