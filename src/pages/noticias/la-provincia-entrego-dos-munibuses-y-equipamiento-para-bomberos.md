---
category: Estado Real
date: 2021-10-30T06:00:25-03:00
thumbnail: https://assets.3dnoticias.com.ar/bomberos.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La provincia entregó dos munibuses y equipamiento para bomberos
title: La provincia entregó dos munibuses y equipamiento para bomberos
entradilla: Además, las autoridades entregaron prendas de vestir para el personal
  del Grupo Especial de Rescate, herramientas hidráulicas y equipamiento específico
  para la intervención de los agentes en incendios y rescates.

---
El gobierno provincial, a través del Ministerio de Seguridad, entregó este viernes dos minibuses para traslado de personal, uniformes para el Grupo Especial de Rescate y equipamiento específico para la Dirección General de Bomberos Zapadores.

Al respecto, el secretario de Política y Gestión de la Información, Jorge Fernández, señaló que “hay una inversión muy importante del gobierno provincial en lo que respecta a equipamiento y tecnología para la Policía de Santa Fe. Es un paso muy significativo para mejorar todos los aspectos de nuestras fuerzas de seguridad. En este caso, entregamos dos minibuses para traslado de personal y equipamiento específico para la intervención de los agentes en rescates e incendios”.

Por su parte, el director general de Bomberos Zapadores, Andres Lastorta, explicó que “esta entrega es una distinción bien merecida por el trabajo específico que realizan los bomberos, nos permite mejorar la calidad del servicio y darle una impronta distinta a la labor que realizamos a diario”.

Estuvieron presentes también el secretario de Gestión Social e Institucional de la Seguridad, Bruno Rossini; el coordinador del Ministerio de Seguridad, Facundo Bustos; y el subjefe de Policía de la Provincia; Diego González.

**Entrega de equipamiento**

En esta oportunidad, la provincia entregó dos minibuses 0 km, tipo furgón con capacidad para traslado de hasta 20 personas, totalmente equipado, con balizas, equipo lumínico y sonoro, y ploteo con inscripciones y escudo de la Dirección General de Bomberos. Además, se entregaron 5 tijeras hidráulicas con accesorios para rescate vehicular, 400 CASCOS de incendio y 40 prendas para el grupo especial GERZ

Asimismo, en el mes de noviembre, se prevé una entrega de 40 equipos de respiración autónoma completos, 40 cilindros de repuesto.