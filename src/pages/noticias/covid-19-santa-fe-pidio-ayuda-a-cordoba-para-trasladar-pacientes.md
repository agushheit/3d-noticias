---
category: Agenda Ciudadana
date: 2021-05-21T08:40:19-03:00
thumbnail: https://assets.3dnoticias.com.ar/covid-peatonaljpg.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Diario El Litoral
resumen: 'Covid-19: Santa Fe pidió ayuda a Córdoba para trasladar pacientes'
title: 'Covid-19: Santa Fe pidió ayuda a Córdoba para trasladar pacientes'
entradilla: La vecina provincia tiene el triple de camas y en algunos hospitales está
  libre el 40 o 60 %.

---
El colapso sanitario que vive Santa Fe por estas horas, con una ocupación de camas exigida al máximo, obligó al Ministerio de Salud a ponerse en contacto con la cartera cordobesa para solicitar que reciban pacientes santafesinos. La información fue confirmada a El Litoral por funcionarios del gobierno de la vecina localidad y por uno de los infectólogos que asesora al presidente Alberto Fernández. 

A pesar de que ambas provincias tienen casi la misma cantidad de habitantes, Córdoba dispone del triple de camas críticas que Santa Fe. Y, aunque su sistema sanitario también está exigido, todavía tienen capacidad para atender la alta demanda de internados por Covi-19.

Mientras que Santa Fe tiene hoy 1.086 camas de terapia intensiva, entre el sistema público (404) y privado (682), en Córdoba hay 3.753. Al inicio de la pandemia, en marzo de 2020, aquí había unas 150 camas UTI sólo en el sector público (no se informó cuántas tenía el sector privado) y en Córdoba 1.019, contabilizando ambos sistemas de salud. Los dos gobiernos reforzaron la infraestructura de atención crítica, pero los números informados por ambos Ministerios de Salud muestran que Córdoba lo ha hecho mucho más.

Consultado por El Litoral, el médico cordobés Hugo Pizzi, quien además es uno de los asesores del gobierno nacional, aseguró que “Córdoba está bien (en la disponibilidad de camas críticas) con una ocupación en algunos hospitales del 40 o 60%. Solo el Rawson está con su capacidad al límite”. Destacó, además, la infraestructura de los dos hospitales universitarios, el Hospital Nacional de Clínicas y Hospital Universitario de Maternidad y Neonatología: “Son fenomenales, están destinados a pacientes Covid y han sido completamente equipados por el Ministerio de Educación”.