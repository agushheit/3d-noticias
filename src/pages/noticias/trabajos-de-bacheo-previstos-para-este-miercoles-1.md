---
category: La Ciudad
date: 2021-03-31T07:30:11-03:00
thumbnail: https://assets.3dnoticias.com.ar/bacheo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad de Santa Fe
resumen: " Trabajos de bacheo previstos para este miércoles"
title: " Trabajos de bacheo previstos para este miércoles"
entradilla: La Municipalidad informa las calles por las que avanza el cronograma de
  tareas previsto en el Plan de Bacheo 2021. Puede haber cortes de circulación y desvíos
  de colectivos a medida que se desarrolla la obra.

---
En enero pasado, la Municipalidad puso en marcha un plan de bacheo para reparar más de 12.000 metros cuadrados de calles de la capital provincial. El programa se desarrolla por fases, priorizando las zonas de la ciudad que evidencian mayor circulación.

* En avenidas troncales, está previsto que los trabajos se desarrollen en:
* Aristóbulo del Valle, entre French y Damianovich, mano sur-norte
* Aristóbulo del Valle, entre avenida Gorriti y 12 de Infantería, mano norte-sur

Por otra parte, en el marco de los trabajos ejecutados en la obra Conducto Pluvial Mariano Comas, operará un corte total del tránsito vehicular en:

Mariano Comas, entre San Lorenzo y avenida López y Planes. Por este motivo, se organiza el desvío de calle Mariano Comas, por San Lorenzo. El tránsito pesado, en tanto, se desviará en Urquiza hacía el bulevar Pellegrini

El municipio informa que todas las tareas se realizan siempre en sentido del tránsito. Del mismo modo, indica que puede haber cortes de circulación y desvíos de colectivos, a medida que avanza la obra y se realizan intervenciones mayores.

Además, se detalló que la realización de los trabajos está sujeta a las condiciones climáticas.

Por el operativo de vacunación

La Municipalidad comunica que, en el marco del operativo de vacunación que se concreta en la ciudad, habrá controles en el tránsito vehicular, entre las 8 y las 18 horas, en las inmediaciones de:

* La Esquina Encendida, en la intersección de Facundo Zuviría y Estanislao Zeballos