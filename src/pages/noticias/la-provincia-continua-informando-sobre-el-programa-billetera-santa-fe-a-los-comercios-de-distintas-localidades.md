---
category: Estado Real
date: 2021-09-04T06:00:17-03:00
thumbnail: https://assets.3dnoticias.com.ar/BILLETERAANDANTE.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La provincia continúa informando sobre el programa Billetera Santa Fe a los
  comercios de distintas localidades
title: La provincia continúa informando sobre el programa Billetera Santa Fe a los
  comercios de distintas localidades
entradilla: El objetivo es brindarles información del programa y fiscalizar el debido
  uso de la herramienta.

---
En el marco del Programa Billetera Santa Fe, la directora Provincial de Promoción de la Competencia y Defensa del Consumidor, María Betania Albrecht, junto al senador provincial, Alcides Calvo, recorrieron las localidades de Colonia Aldao y Sunchales, del departamento Castellanos, para tomar contacto con comerciantes locales a fin de brindarles información del programa y fiscalizar el debido uso de la herramienta.

Luego de la recorrida Albrecht destacó que, “creemos fundamental promocionar los derechos de las y los consumidores y auxiliar a los comerciantes en el uso de la aplicación, ya que el programa vino no sólo a fomentar el consumo, sino también acompañar a los comercios”.

“Estuvimos recorriendo distintas localidades de la provincia, en este caso Sunchales y Colonia Aldao, a fin de establecer diálogos con los comercios para que ellos nos cuenten el impacto de la Billetera y no sólo nos trasladen dudas e inquietudes, sino también ayudarlos e informarles acerca del funcionamiento para evitar prácticas abusivas”, concluyó la funcionaria.

**PROCEDIMIENTO DE RECLAMOS**

En primera instancia se debe reclamar ante la operadora del sistema Plus Pagos, a través del chat boot al número 3416470678 o por medio de un correo electrónico a info@pluspagos.

Luego, la Dirección Provincial de Promoción de la Competencia y Defensa del Consumidor es quien resulta competente para recibir, tramitar y resolver adecuadamente cualquier reclamo relativo a este programa. Aquí, los reclamos se pueden llevar adelante de la siguiente manera:

\-Presencial, solicitando turno en: [https://turnos.santafe.gov.ar/turnos/web/frontend.php](https://turnos.santafe.gov.ar/turnos/web/frontend.php "https://turnos.santafe.gov.ar/turnos/web/frontend.php")

>>En Santa Fe: Ministerio de la Producción, Pellegrini 3100 - 0800 555 6768 (opción 3) de 8 a 12 horas en días hábiles.

>>En Rosario: Mitre 930 – 3º Piso - (0341) 4721519-4721511 de 8 a 12 horas en días hábiles.

O de manera online, en la Ventanilla Única Federal a través del siguiente link: [https://www.argentina.gob.ar/produccion/defensadelconsumidor/formulario](https://turnos.santafe.gov.ar/turnos/web/frontend.php "https://turnos.santafe.gov.ar/turnos/web/frontend.php")

Finalmente, también se pueden presentar reclamos por Billetera Santa Fe ante las Oficinas Municipales o Comunales de Información al Consumidor (OMICs/OCICs), creadas por convenio con el Gobierno Provincial y que, trabajan con facultades descentralizadas en materia de protección de consumidores. El listado de OMICs/OCICs se puede consultar ingresando al siguiente enlace: [https://www.santafe.gob.ar/index.php/web/content/download/259414/1365404/](https://turnos.santafe.gov.ar/turnos/web/frontend.php "https://turnos.santafe.gov.ar/turnos/web/frontend.php")