---
category: Agenda Ciudadana
date: 2021-06-14T06:00:27-03:00
thumbnail: https://assets.3dnoticias.com.ar/CANSINO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Oficializaron la autorización de la vacuna Convidecia, del laboratorio Cansino
title: Oficializaron la autorización de la vacuna Convidecia, del laboratorio Cansino
entradilla: Tras la aprobación de la Administración Nacional de Medicamentos, Alimentos
  y Tecnología Médica, la ministra de Salud, Carla Vizzotti firmó la resolución autoriza
  el uso del fármaco fabricado en China.

---
El Ministerio de Salud oficializó la autorización "con carácter de emergencia" de la vacuna Convidencia contra el coronavirus, luego de la aprobación de la Administración Nacional de Medicamentos, Alimentos y Tecnología Médica (Anmat).

Así fue oficializado a través de la Resolución 1671/2021, que lleva la firma de la ministra Carla Vizzotti, y que fue publicada este sábado en el Boletín Oficial.

El viernes el Gobierno nacional anunció que avanza un acuerdo para la adquisición de 5,4 millones de dosis de esta vacuna, que se sumarán a las 19 millones ya arribadas a la Argentina.

La vacuna Convidencia (Recombinant Novel Coronavirus Vaccine (Adenovirus Type 5 Vector) es de una sola dosis y fue desarrollada por laboratorio Cansino Biologics Inc (Beijing institute of Biotechnology). Es la tercera desarrollada por China, luego de Sinovac y Sinopharm.

La aprobación fue autorizada "en virtud de lo establecido por los artículos 8° y 9° de la Ley N° 27.573 y de conformidad con las recomendaciones de la Anmat", según indica el texto.

La cartera de Salud adelantó el viernes la noticia desde su cuenta de Twitter: "La ministra de Salud @carlavizzotti firmó la Resolución 2021-1671 que autoriza el uso de emergencia de la vacuna Convidencia de la empresa Cansino Biologics INC y avanza el acuerdo por 5.4 millones de dosis en función de su disponibilidad".