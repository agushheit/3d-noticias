---
category: La Ciudad
date: 2022-01-26T06:00:58-03:00
thumbnail: https://assets.3dnoticias.com.ar/ENRESS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'Aguas Santafesinas: el Enress recomendará un aumento gradual y ajustado
  a paritarias'
title: 'Aguas Santafesinas: el Enress recomendará un aumento gradual y ajustado a
  paritarias'
entradilla: El organismo tiene recortadas (por una laguna legal) sus facultades para
  aprobar o rechazar el aumento pedido por Aguas que sumaría un 68%, en dos tramos
  del 40% primero y el 20% luego sobre el acumulado

---
Al igual que en 2018, y como una forma de insistir en la necesidad de recuperar sus facultades mediante un nuevo marco legal, el Ente Regulador de los Servicios Sanitarios emitirá su resolución de recomendación sin cifras que den a los periodistas un título de fácil redacción, ante el pedido de aumento de Aguas Santafesinas Sociedad Anónima.

 Como se sabe, la empresa estatal inició el proceso para obtener una suba en las tarifas que de aprobarse tal como fue solicitado implicaría un 40% en el bimestre posterior y un 20% más sobre el acumulado, que implicaría un 68%. Como se verá, en el caso de la principal prestadora de los servicios de agua potable y cloacas a la última palabra no la tendrá el Enress que ahora debe opinar, sino la Casa Gris.

 Un vínculo provisorio, bajo la figura de una SA, liga a la empresa que fue reestatizada en 2006 con el regulador, que hoy puede negar o aprobar los aumentos de tarifas de los casi 400 prestadores dispersos en el resto de la provincia, entre cooperativas, municipalidades y comunas, pero no puede hacerlo con Assa. En el caso de la concesionaria heredera de la ex Dipos y de la ex Aguas Provinciales es el gobierno provincial el que hace pesar su poder concedente. Una resolución del ministerio de Infraestructura, Servicios Públicos y Hábitat a cargo de Silvina Frana resolverá cuál será la actualización tarifaria, tras casi cuatro años de congelamiento.

 **Un trío al unísono**

 El Directorio del Enress tiene hoy tres integrantes, ningún presidente y la imperiosa necesidad de lograr la unanimidad para que puedan ser válidas sus resoluciones, porque tiene el quórum y la mayoría necesaria justos. Dos de ellos fueron titulares del ente: Oscar Pintos y Anahí Rodríguez asumieron esa representación en distintos momentos, años atrás. Además, el trío se completa con el director Leonel Marmiroli, que junto a la directora Rodríguez tuvieron a su cargo la coordinación del debate durante la audiencia pública virtual que trató el aumento tarifario de Assa. Pintos participó desde su casa, por padecer Covid-19.

 Esta semana comenzaron el intercambio de borradores y los contactos telefónicos entre los tres. Es probable que haya una resolución sus respectivas firmas en cuestión de días, tal vez esta misma semana.

 Cuentan para sus recomendaciones con los informes de cada una de las gerencias del ente que responden al pedido de Assa formulado a mediados de diciembre de 2021, y con lo que les dejó la audiencia pública (a la que se puede volver a ver en el Facebook de Prensa Enress).

 **Criterios**

 Aunque todavía no está escrito el borrador en común de los tres directores, El Litoral pudo saber que no habrá cifras ni porcentajes, tal como hizo el ente en 2018 ante el último pedido de aumento de Aguas. Se propondrán en cambio pautas, criterios y lineamientos para que luego actúe la Casa Gris.

 En pocas palabras, está claro que se buscará que se sigan los criterios de gradualidad y oportunidad que años atrás fijara la Corte Suprema de Justicia de la Nación ante otras discusiones tarifarias en el país, y que eso en buen romance quiere decir un impacto menor al que ahora pide Aguas, aunque se llegue al mismo resultado al final del año. Y la otra variable que mencionarán seguramente los directores es que no se supere la variación de los salarios (algo que fue central en las ponencias de los diputados Clara García del PS y Oscar Martínez del PJ durante la audiencia pública, así como del defensor del pueblo a cargo Jorge Henn).

 Se dice que en el Enress la Gerencia de Análisis Económico y Financiero advierte que está justificado subir tarifas (luego de cuatro años sin novedades) y con una inflación que trastocó los ingresos y egresos de Assa. También que quienes evalúan el estado de la Infraestructura y la operación de los servicios lo ven necesario pero que deberían mejorarse con la autorización varios aspectos en cuanto a su eficiencia, y que -en cambio- para la Gerencia de Asuntos Legales las cosas no se han hecho como correspondía y por lo tanto no debiera premiarse al prestador con un aumento.

 Para los tres directores, ese igual número de visiones, en parte opuestas, deja un margen de posibilidades mayor. Y también parte de la conducción del Ente tendrá en cuenta que hasta aquí, en los dos últimos años, han permitido a otros servicios en manos de asociaciones cooperativas o de autoridades comuanles y municipales aumentos que rondaron el 55%.

 Obviamente, la resolución contemplará los dos hechos extraordinarios que le dan un marco de aceptación general a un pedido de aumentos de recursos para prestar los servicios sanitarios: la pandemia y la sequía.

 Habrá consideraciones sobre el valor del servicio de agua potable en términos de prevención y cuidado de la salud y también el razonamiento de que los costos fueron mayores -y la recaudación menor- como consecuencia del aislamiento de años atrás. También habrá algún párrafo sobre la ausencia momentánea, cuando no la lamentable pérdida de personal altamente calificado en la operación de los servicios que murió por Covid antes de que llegaran las vacunas.

 Tiene un detalle importante en la justificación del aumento por parte de las empresa los sobrecostos propios de la bajante del Paraná y sus cursos tributarios, que son las fuentes de captación superficiales de buena parte de las plantas potabilizadoras de Assa.

 En ese plano el Enress repetirá su análisis de 2018 respecto de la operación y el funcionamiento de los Grandes Acueductos. Al igual que con los costos de esos mega sistemas de conducción de agua, para las inversiones en bombas impulsoras, tomas de agua cruda y más kilovatios en todo el proceso desde río hasta las canillas el criterio del Enress es que eso debe provenir del erario público, no del bolsillo de los usuarios. Y en ese sentido se recuerda que ha sido el Estado nacional el que aportó recursos para enfrentar la bajante prolongada y crítica.

 El criterio es que -como los acueductos- el tema de la sequía excede a la prestadora. Que se carga ese gasto en su presupuesto que debe ser atendido por el Estado.

 **Críticas**

Habrá que ver si parte de las críticas que formulan las gerencias técnicas (en especial el área de Infraestructura y Legales) llegan a la resolución de recomendación. Y por lo que se rumorea en el Enress hay material de sobra para formularlas: compras de vehículos con demasiada frecuencia, incorporación de 150 personas a la nómina salarial, y sobre todo un derrumbe del plan de inversiones en micromedición que no puede explicarse solo con los problemas que trajo el coronavirus.

 Lo dijo la diputada García: se ponían por año en los gobiernos del Frente Progresista unos 18 mil y hasta 25 mil medidores domiciliarios, pero en cada uno de los dos últimos apenas 4 mil.