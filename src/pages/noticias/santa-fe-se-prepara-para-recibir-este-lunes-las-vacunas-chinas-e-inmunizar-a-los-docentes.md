---
category: La Ciudad
date: 2021-02-28T06:30:16-03:00
thumbnail: https://assets.3dnoticias.com.ar/sinopharm.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Santa Fe se prepara para recibir este lunes las vacunas chinas e inmunizar
  a los docentes
title: Santa Fe se prepara para recibir este lunes las vacunas chinas e inmunizar
  a los docentes
entradilla: El Ministerio de Salud de Nación decidió el número de vacunas Sinopharm
  que se distribuirá en cada provincia por la población de cada distrito. Se comenzarán
  a distribuir el lunes.

---
La distribución de las vacunas Sinopharm, que llegó al país esta semana proveniente de China, comenzará este fin de semana en todo el país y, en una primera etapa, estarán comprendidas 492.400 dosis, según informó el Ministerio de Salud.

El anuncio fue formulado por la ministra del área, Carla Vizzotti, quien explicó que la distribución de las dosis se hará "en base a la población de cada distrito", según se afirmó en un comunicado. Lo hizo en el marco del escándalo que se suscitó por la existencia de un "vacunatorio VIP" en la cartera sanitaria de la Nación.

Las dosis comenzarán a salir mañana y llegarán entre lunes y martes a cada provincia, en el marco de un proceso de distribución que será "público y y bajo pautas de total transparencia", según se indicó.

Según la nómina dispuesta por el gobierno nacional, a la provincia de Santa Fe llegarán 37.800 vacunas, lo que representa un 7,79% del total de la que arribaron procedentes de China. Están autorizadas para ser aplicadas a personas de hasta 60 años, por lo tanto, serán destinadas al personal docente y no docente afectado al dictado de clases presenciales.

"La cantidad de dosis que se entrega a cada jurisdicción depende estrictamente del porcentaje de población y se adapta a la unidad mínima de embalaje, que es de 400 ó 900 dosis por conservadora", indica el texto oficial.

Según se precisó, el stock de dosis de Sinopharm pendiente de distribución es de 507.600, a las que se sumarán 96.000 dosis más que llegarán mañana a las 7.10 horas en un vuelo de línea de Air France KLM. De esta manera, se completará el millón de dosis acordadas con China y anunciadas por el Ministerio de Salud.

La llegada de las vacunas Sinopharm permitirá empezar a inmunizar al personal docente en todo el país, según anunció el gobierno esta semana.

El jefe de Gabinete, Santiago Cafiero, explicó -a través de su cuenta de Twitter- que el gobierno busca "garantizar el mayor grado de presencialidad posible (en las escuelas), de forma segura y cuidada, y para lograrlo la vacunación es un paso muy importante".