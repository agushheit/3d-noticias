---
category: Cultura
date: 2022-05-21T18:33:54-03:00
layout: Noticia con video
author: ''
resumen: '"Bailando" nuevo videoclip de Los Auténticos Decadentes con Miranda como
  invitados'
title: '"Bailando" nuevo videoclip de Los Auténticos Decadentes con Miranda como invitados'
entradilla: |2-

  Nuevo single oficial de Capítulo D, segundo álbum de la trilogía discográfica ADN
thumbnail: https://assets.3dnoticias.com.ar/decadentes y miranda.png
link_del_video: https://youtu.be/Mbix6B4DrdA

---
**“Bailando”** es una canción original del grupo español **_Alaska & los Pegamoides_** que tuvo una gran popularidad en los años 80. Reivindicando su espíritu disco, **Los Auténticos Decadentes** presentan esta versión fresca y desprejuiciada con la participación especial de **Miranda!** El single retrata con humor e ironía los estragos de la noche y el alcohol en el cuerpo y es acompañado de un videoclip registrado en Buenos Aires bajo la dirección de **Octavio Lovisolo**.

**Bailando ft. Miranda!** fue grabada por **Gustavo Borner** en los estudios Sony Music México y **Ale Sergi** en Estudio Fantasma de Buenos Aires. La producción artística es de **Martín “Moska” Lorenzo**, **Mariano Franceschelli** y **Los Auténticos Decadentes**. Co-producida, mezclada y masterizada por **Gustavo Borner** en los estudios Igloo Music de Los Angeles.

**ADN** es la trilogía de álbums que desgrana las influencias musicales de **LAD** a través de nuevas versiones e incluye colaboraciones con importantes referentes de música de América Latina como **Los Palmeras**, **Attaque 77**, **Andrea Prodan**, **Ulises Bueno**, **Los Pericos**, **Bandalos Chinos**, **León Gieco**, **Santaferia** y **Beto Cuevas** de Chile, **Rubén Rada** de Uruguay, **Panteón Rococó**, **Natalia Lafourcade** y **Don Lupe Esparza** de México y **Tokyo Ska Paradise de Japón**, entre otros. A los ya editados Capítulos **A** y **D**, se sumará Capitulo **N** a fines de 2022.

El material, que ha cosechado elogios a lo largo de el continente, tiene doble fecha de presentación oficial: 16 de Julio en el Palacio de los Deportes de Ciudad de México, donde la banda se presentará por primera vez con una espectacular puesta 360; y luego el 6 de Agosto en el Estadio Luna Park de Buenos Aires.

Desde 1986 **Los Auténticos Decadentes** han construido un camino lleno de éxitos, buenos recuerdos y mucho rock. A lo largo de más de 35 años banda editó 12 discos de estudio, un MTV Unplugged y 3 DVDs en vivo, logrando múltiples distinciones y premios, pero principalmente ganando el corazón del público.

![](https://assets.3dnoticias.com.ar/decadentes y miranda.png) 