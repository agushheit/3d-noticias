---
category: Agenda Ciudadana
date: 2021-06-16T09:11:37-03:00
thumbnail: https://assets.3dnoticias.com.ar/padres-organizados-ministerio-1jpg.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de LT10
resumen: Vuelve el reclamo de Padres Organizados para el dictado de clases presenciales
title: Vuelve el reclamo de Padres Organizados para el dictado de clases presenciales
entradilla: El jueves se concentrarán frente a Casa de Gobierno para llevar adelante
  este y otros pedido. Lo que solicitan.

---
El regreso de la presencialidad a las aulas en la provincia de Santa Fe continúa siendo el eje del reclamo de Padres Organizados.

Si bien esta semana se anunció desde Educación que en localidades que reportaron menos de 10 contagios en los últimos días se retoma la bimodalidad, presencial y virtual, nuestra ciudad está lejos de esa realidad epidemiológica y sanitaria.

Desde la organización, que sostiene desde las recomendaciones y estudios de la Sociedad de Pediatría sobre la escaza tasa de contagios en los establecimientos educativos insisten con el retorno a las aulas de los alumnos santafesinos.

Al reclamo lo llevarán nuevamente este jueves a Casa de Gobierno, donde a las 18 horas se convocarán en caravana para dar vueltas a la manzana del edificio estatal.

Por LT10, Clara Ruda, integrante de la agrupación, detalló que exigen presencialidad, un plan concreto para chicos desvinculados y apertura de comedores.