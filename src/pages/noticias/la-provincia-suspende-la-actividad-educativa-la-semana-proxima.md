---
category: Estado Real
date: 2021-05-22T06:15:53-03:00
thumbnail: https://assets.3dnoticias.com.ar/perotti.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Provincia suspende la actividad educativa la semana próxima
title: La Provincia suspende la actividad educativa la semana próxima
entradilla: La medida rige para todo el territorio santafesino ante el crecimiento
  de la curva epidemiológica en el marco de la segunda ola de Covid 19.

---
El gobierno de la provincia, informó que el territorio santafesino se encuentra atravesando el mayor crecimiento de la curva epidemiológica en el marco de la segunda ola de Covid 19, con una riesgosa saturación en la utilización de camas críticas. Es por ello que urge fortalecer el cuidado personal y colectivo para preservar la salud de todas y todos.

En ese sentido, el DNU del presidente de la Nación y la consecuente adhesión del Estado provincial nos indica reducir al máximo la circulación de personas hasta el 31 de mayo.

En ese marco, la escuela como formadora de ciudadanía, una ciudadanía responsable, solidaria y comprometida con el bien de todas y todos, aportará a la concreción del objetivo de disminuir la movilidad poblacional suspendiendo la actividad educativa durante los tres días que se suceden luego de los feriados del 24 y 25 de mayo. Así, las autoridades del Ministerio de Educación de Santa Fe han determinado para los días miércoles 26, jueves 27 y viernes 28 de mayo poner el acento en la mayor disminución de la circulación de personas, comprometiéndose con el objetivo fundamental de las restricciones impuestas para toda la población y resolver con los docentes un tiempo de trabajo profesional en beneficio de acciones fundamentales para la enseñanza y para el cierre del primer período escolar.

Por otra parte, es necesario aclarar, frente a las consultas realizadas sobre la educación virtual, que la misma en nuestra provincia solo es posible sostener con un grupo mínimo, ya que la inmensa mayoría de nuestra población escolar carece de conectividad, por lo tanto, dar continuidad con esos recursos, implicaría la posibilidad de unos pocos.

Además, la educación en la distancia presupone una amplia serie de actividades y dispositivos que exceden lo virtual, tales como la entrega de materiales impresos, la orientación focalizada de pequeños grupos de alumnos, tutorías, y consultas, en una frecuencia reducida. Esto supone igual, aunque administrada y acotada, circulación de personas, que es lo que se pretende evitar.

Esta realidad sigue poniendo en evidencia la necesidad del tratamiento de la ley de conectividad en la provincia.

Por último, resulta oportuno explicitar que se continúa con la modalidad de monitoreo día a día de la evaluación de la situación sanitaria y en función de ese proceso al final de la semana próxima se resolverá el formato escolar a seguir.

No obstante, en ese período será estratégico dedicar un tiempo a ciertas lecturas que requieren reflexión para orientar la tarea que sigue. Por tal motivo han propuesto:

1\. Leer el Calendario escolar 2021. Analizar las actividades que involucran el hacer de cada uno y planificar en consecuencia;

2\. Volver al documento de Evaluación formativa que les fuera distribuido el año pasado y que se constituye en material de trabajo permanente. Es este un buen momento para proponer en ese marco, las acciones que orientarán la evaluación del primer período escolar.