---
category: La Ciudad
date: 2021-11-09T06:00:51-03:00
thumbnail: https://assets.3dnoticias.com.ar/salones.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Salones de fiestas piden trabajar con un aforo del 100%
title: Salones de fiestas piden trabajar con un aforo del 100%
entradilla: 'Los empresarios dueños de salones de eventos solicitan además la extensión
  horaria. Solo están habilitados con un 50% en lugares cerrados y hasta las 3

'

---
Según la resolución Nº 495 de la provincia, desde el pasado sábado 9 de octubre se habilitó el funcionamiento de discotecas, locales bailables y salones de fiesta para bailes en espacios cerrados, pero con un máximo de ocupación del 50% de su capacidad, y funcionando hasta las 3.

Desde la Cámara de Eventos y Afines de Santa Fe dijeron que viendo las nuevas habilitaciones en distintos rubros, la semana pasada y tras una reunión con Juan Marcos Aviano, se le solicitó extender el horario de cierre por un lado y la capacidad de los salones.

“Hoy estamos trabajando con el 50% de aforo, dos días a la semana con baile; y de esta manera es imposible con la cantidad de eventos que tenemos; ahora vienen las graduaciones de los chicos del secundario, que generalmente son entre semana; y al no tener la posibilidad de baile, y con horario acotado, no podemos hacer las fiestas de fin de año de los egresados”, advirtió Ivan Hawryluk a los micrófonos de LT8. “Los chicos y padres firmantes empiezan a exigir postergar el evento, con los problemas de agenda que esto genera”, lamentó.

El ejecutivo de la cámara de eventos argumentó que “hay espectáculos públicos, las canchas habilitadas al 100%, entendemos que no hay razón para que nosotros estemos trabajando con un 50% de aforo y horario restringido”. “Los contratos que tenemos hasta fin de año son muchísimos, necesitamos tener esa extensión horaria”, concluyó.