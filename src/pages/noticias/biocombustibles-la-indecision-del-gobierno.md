---
category: El Campo
date: 2021-03-17T07:08:34-03:00
thumbnail: https://assets.3dnoticias.com.ar/ximena.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Diputada García
resumen: Biocombustibles, la indecisión del gobierno
title: Biocombustibles, la indecisión del gobierno
entradilla: La Diputada Nacional Ximena García junto a otros diputados de Juntos por
  el Cambio, impulsan una una sesión especial en la Cámara de Diputados con el objetivo
  de tratar la prórroga a la Ley de Biocombustibles.

---
La Diputada Nacional Ximena García junto a otros diputados de Juntos por el Cambio, impulsan una una sesión especial en la Cámara de Diputados para el 25 de marzo, con el objetivo de tratar la prórroga a la Ley de Biocombustibles que vence el próximo 12 de mayo. Cabe recordar que el Frente de Todos se había comprometido a promover su tratamiento en sesiones extraordinarias, ello no ocurrió aún cuando cuenta con media sanción de la Cámara de Senadores por unanimidad.

Inexplicablemente el gobierno nacional esquiva su tratamiento. Si bien los precios de los biocombustibles para el periodo enero - abril de este año fueron actualizados por Decreto, los elevados costos de producción paralizaron a estas empresas durante el 2020,

Otro tema no menor es el beneficio alcanzable a nivel ambiental que significa potenciar este tipo de energías. Si bien mediante Ley 27.270 nuestro país ratificó el Acuerdo de París, no hemos logrado construir un programa de gobierno a mediano y largo plazo en materia ambiental y productiva. Es crucial conocer que los biocombustibles de primera generación ahorran entre un 55% y un 70% de las emisiones de gases de efecto invernadero (GEI) respecto del combustible fósil al que reemplazan (nafta y el gasoil). Es decir, debemos apoyar el desarrollo productivo sostenible que nos permita crecer sin dañar nuestro entorno.

Este estado de cosas genera incertidumbre en la comunidad y pone en peligro la continuidad de las 54 plantas de biocombustibles con sede en diez provincias. En este punto la diputada resaltó que “solo en Santa Fe existen 18 plantas donde se produce el 80% del biodiesel del país”.

El sector es sumamente importante para nuestro crecimiento productivo regional y nacional ya que hay inversiones por al menos 2.000 millones de dólares, y genera casi 200.000 puestos de trabajo de forma directa e indirecta: 7.500 en el sector de biodiesel para mercado interno y externo, 160.000 en el de bioetanol a base de azúcar y 25.000 en el bioetanol a base de maíz.

“Este proyecto que hoy acompañamos refleja el reiterado reclamo de las Cámaras que reúnen a las empresas productoras de biocombustibles de todo el país. La prórroga de la ley es fundamental y no admite dilaciones. No debemos titubear en nuestras prioridades ya que la producción basada en un desarrollo sostenible nos garantizará un futuro de crecimiento” concluyó la legisladora.