---
category: Estado Real
date: 2021-10-26T06:00:20-03:00
thumbnail: https://assets.3dnoticias.com.ar/perottiarijon.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Perotti: "El agua es estrategica para la vida y la economía de toda la región
  oeste de la provincia de Santa Fe"'
title: El gobernador participó este lunes del ingreso de cañerías del Acueducto Desvío
  Arijón-Rafaela. Además, encabezó una jornada educativa sobre la importancia del
  agua.
entradilla: El gobernador participó este lunes del ingreso de cañerías del Acueducto
  Desvío Arijón-Rafaela. Además, encabezó una jornada educativa sobre la importancia
  del agua.

---

El gobernador Omar Perotti participó este lunes del ingreso de cañerías del Acueducto Desvío Arijón-Rafaela, y encabezó una jornada educativa sobre la importancia del agua.

El acueducto Desvío Arijón-Rafaela entró en su etapa final y su relevancia es motivo de tareas escolares en diferentes instituciones educativas de Rafaela, y es consecuencia del abordaje curricular trabajado durante el año, referido al cuidado y a la importancia del agua. La megaobra incluye la ampliación de la planta, cinco estaciones de rebombeo y el tendido de 130 kilómetros de conductos, con una inversión de 14 mil millones de pesos que beneficiará a casi 142.000 habitantes.

En la oportunidad, Perotti afirmó que “todos los rafaelinos son testigos de los anuncios que hubo en estos años. Eso es lo que nos llevó a decir siempre que había que ser custodios de esta obra. Y hoy ser testigos del ingreso de la cañería nos parecía un hecho importante, trascendente y de regalo a una ciudad en su cumpleaños”, dijo el gobernador.

Y agregó: “Antes de la pandemia viajamos a los Países Árabes a ratificar nuestro compromiso y nuestro deseo de seguir contando con el financiamiento y con el apoyo para este proyecto, porque sabíamos que era imprescindible ese financiamiento para que nos ayudaran a abastecer una parte de todas las obras que la provincia requiere. Detrás de traer el agua está la salud humana y la calidad de vida, pero también está la preocupación de toda una región que es productora de alimentos. El agua es estratégica para la vida y la economía de toda la región oeste de la provincia de Santa Fe”, sostuvo el mandatario.

Seguidamente, Perotti resaltó que “este gobierno va a acompañar siempre al que trabaja, al que invierte y al que produce. Y la mejor forma de estar a la par es con obras. Hoy nos toca ésta, pero sin duda la región suma esta obra central como es la provisión del agua a otras claves como el gasoducto para todos los barrios y el sector industrial; las posibilidades de una ruta que nos va a garantizar transitabilidad, seguridad vial y movimiento de nuestra producción; un hospital terminado y nuevo; la educación y también la Universidad Nacional de Rafaela tendrá un importante avance en todos sus edificios para seguir acompañando la formación de nuestra gente”, manifestó el gobernador.

Por último, Perotti anunció que fruto del trabajo conjunto realizado con el Ministerio de Obras Públicas de la Nación, “Rafaela está incorporada en los proyectos de Parques Regionales que las ciudades importantes del país van a tener”, finalizó.

Por su parte, el secretario de Empresas y Servicios Públicos, Carlos Maina, destacó que “esta obra es muy importante por su magnitud”, y recordó que los trabajos se iniciaron “en el año 2018, y no fue fácil esta última parte, ya que en enero de 2020 el porcentaje de avance era muy bajo, sobre todo en los tramos 4 y 5. Los 3 primeros tramos estaban en ritmo de un 50%, pero lamentablemente el 4 y 5 estaban prácticamente paralizados”.

En este sentido, Maina destacó que “ordenamos muchas cosas, para tener mejor ritmo de obra, y a esta altura del año estamos a muy pocos metros de terminar la colocación de cañería de todo el acueducto”, y precisó que “en los próximos meses restará colocar el equipamiento que ya está llegando y en marzo, abril, estaremos empezando con las primeras pruebas”.

Por último, el intendente de Rafaela, Luis Castellano, afirmó que “es un día histórico para Rafaela, en el marco de los 140 años de la ciudad está ingresando el acueducto a la ciudad. Una obra trascendental, una obra importantísima no solo para el presente sino, fundamentalmente, para el futuro y el desarrollo de Rafaela”.

Participaron también de la actividad, el senador nacional Roberto Mirabella; el senador por el departamento Castellanos, Alcides Calvo; la secretaria de Gestión Territorial Educativa, Rosario Cristiani; la jefa de Gabinete municipal Amalia Galanti; y el presidente de la Federación de Entidades Vecinales, Javier Grande; entre otras autoridades.

**ACUEDUCTO PARA RAFAELA**

La alimentación a la ciudad de Rafaela desde la planta de Desvío Arijón implicará un importante cambio en las características del agua potable de la ciudad, reduciendo niveles de salinidad y haciendo posible la incorporación al servicio en el futuro de otros sectores del ejido urbano.

En la ciudad de Rafaela el agua proveniente del acueducto se almacenará en una nueva reserva de cinco millones de litros, que se suman a la existente de 10 millones de litros.

Actualmente, Rafaela se abastece de agua potable a través de un sistema combinado: un acueducto de 60 kilómetros proveniente de Esperanza; y agua de perforaciones locales, potabilizada mediante el proceso de desalinización denominado ósmosis inversa.

La llegada del Acueducto Desvío Arijón permite sacar de servicio la principal planta de ósmosis inversa de la ciudad, manteniendo con volúmenes reducidos el aporte del Acueducto Esperanza-Rafaela.

**JORNADA PEDAGÓGICA**

Previamente, durante la mañana, el gobernador Omar Perotti encabezó, en el Complejo Educativo Barrio San José de Rafaela, una jornada sobre la importancia del agua, en el marco de la propuesta pedagógica institucional y comunitaria “Nos apropiamos del acueducto”, de la que participan alumnas y alumnos de los niveles inicial, primario y secundario de unas 17 escuelas y del anexo del Centro de Educación Física Nº 19.

Dicha jornada contó con producciones pedagógicas, artísticas y científicas realizadas por las niñas, niños y adolescentes, quienes aplicaron los saberes trabajados en el aula, a través del uso de los cuadernos pedagógicos utilizados desde principio de año, reflejando la importancia de la emblemática obra del acueducto y el cuidado del agua.