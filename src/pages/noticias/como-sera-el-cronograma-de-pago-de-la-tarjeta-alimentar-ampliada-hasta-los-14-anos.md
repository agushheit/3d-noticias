---
category: Agenda Ciudadana
date: 2021-05-22T06:00:57-03:00
thumbnail: https://assets.3dnoticias.com.ar/TARJETA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Cómo será el cronograma de pago de la Tarjeta Alimentar, ampliada hasta los
  14 años
title: Cómo será el cronograma de pago de la Tarjeta Alimentar, ampliada hasta los
  14 años
entradilla: 'El ministro de Desarrollo Social confirmó el pago correspondiente a mayo
  y precisó: "Automáticamente todas las madres con chicos de hasta 14 años que tienen
  la Asignación entran en el sistema".'

---
El ministro de Desarrollo Social, Daniel Arroyo, confirmó que este viernes se depositará el importe de la Tarjeta Alimentar para que cobren los beneficiarios que ya tienen el plástico, mientras que aquellas familias que fueron incorporadas en la reciente ampliación de la cobertura -madres con hijos de hasta 14 años inclusive- recibirán el pago de mayo en "la cuenta de la Asignación Universal por Hijo" a partir de la semana próxima.

"Los que tienen ya la tarjeta, se carga y van a comprar los alimentos. Quienes no la tienen, lo que hacemos en este mes de mayo es transferir la plata a la cuenta en la que tienen la Asignación Universal por Hijo y durante junio con los bancos vamos a organizar la entrega de las tarjetas", detalló Arroyo.

Además, recordó que la Tarjeta Alimentar, que implica una transferencia directa a las familias con el objeto de adquirir alimentos, "se carga el tercer viernes de cada mes".

**Quiénes son beneficiarios y cuánto deben cobrar**

Sobre la ampliación del universo de beneficiarios, anunciada en la última reunión del Consejo Federal Argentina contra el Hambre, subrayó que tras esa medida las transferencias para comprar alimentos pasarán a beneficiar a "casi 4 millones de chicos" de hasta 14 años inclusive, cuando hasta el mes pasado alcanzaban a 1.900.000 niños de hasta 6 años.

Y sobre los importes a cobrar, recordó: "La tarjeta es de 6000 pesos para la madre de un chico hasta los 14 años inclusive, de 9000 pesos para quien tiene dos y de 12000 para quien tiene tres o más chicos de hasta 14 años".

En relación a quienes no poseen el plástico de la tarjeta Alimentar y que este mes empezarán a cobrar el importe (madres y familiares a cargo de niños, niñas y adolescentes de entre 6 y 14 años), Arroyo aclaró que "no tienen que realizar ningún trámite específico" para recibir el beneficio ya que, precisó, "automáticamente todas las madres con chicos de hasta 14 años que tienen la Asignación entran en el sistema".

Y en el mismo sentido profundizó: "Las personas a las que se amplió la tarjeta, que no la tenían hasta ahora y que les corresponde, porque tienen chicos de hasta 14 años, este mes (por mayo, porque el beneficio se cobra en la tercera semana de cada mes) van a recibir el dinero en la cuenta de la Asignación Universal por Hijo y durante junio van a recibir la tarjeta", adelantó.

"La tarjeta Alimentar no es para extraer dinero del cajero y sí para comprar alimentos: los alimentos que quieren y como quieren las familias. Nuestra sugerencia siempre es comprar leche, carnes, frutas y verduras e ir por el lado de la buena nutrición", aseguró Arroyo, quien por otro lado reconoció que en la actualidad "el problema más grave" que tiene la Argentina, junto con la pandemia, "es el precio de los alimentos".

En ese punto, planteó que "comer tendría que ser barato en la Argentina", dado que se trata de un país productor de alimentos, y entonces llamó a poner el foco sobre "la intermediación, que es muy determinante".

"Si el tambero saca el litro de leche a 18 o 20 pesos, y termina la leche a 70, 80 pesos en la góndola, en la intermediación hay un proceso importante que hay que tener en cuenta", advirtió.

**Programa Canasta Ahorro**

Sobre el cierre de la exportación de carne por 30 días, Arroyo lo consideró "una decisión muy importante del presidente" que apunta a "cuidar la mesa de los argentinos", y en ese sentido destacó que desde la cartera de Desarrollo Social se puso en marcha el programa Canasta Ahorro, compuesto por 28 productos fundamentales a precios reducidos, como leche en polvo y sachet, yogur, queso crema, aceite, azúcar, harinas y yerba mate.

"En la Canasta Ahorro la leche está a 55 pesos, el yogur a 65 o el puré de tomate a 40 pesos", resaltó a modo de ejemplo y remarcó que el objetivo de la iniciativa es "que el productor le venda directamente al consumidor" para lo cual se fomenta el despliegue de "los pequeños productores" como también la realización de "ferias y mecanismos para que el productor directamente le venda al consumidor y así bajar los precios".

Arroyo también dio detalles del avance de las urbanizaciones del Registro Nacional de Barrios Populares (Renabap), iniciativa que consiste en la apertura de calles, construcción de veredas y viviendas, mejoramiento de cocinas y baños, como también reparación de instalaciones eléctricas, y que ya alcanzó, dijo, "a 400 barrios este año".

Por último, en relación a la pobreza y su impacto sobre niños y adolescentes, señaló: "En la Argentina tenemos 57% de pobreza en los chicos de hasta 14 años, y una forma de empezar a resolver ese problema es que todos (los niños) vayan al jardín, que arranquen de igual condición, pero eso estamos impulsando la construcción de jardines de infantes, que todos los chicos y chicas vayan a sala de 3, 4 y 5", planteó.

Por esa razón, siguió Arroyo, desde la cartera de Desarrollo Social están construyendo "800 jardines", en un plan de trabajo que apunta a garantizar "el derecho a la primera infancia".