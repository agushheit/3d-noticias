---
category: La Ciudad
date: 2021-05-09T06:15:30-03:00
thumbnail: https://assets.3dnoticias.com.ar/DESVIOS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Cortes de tránsito y desvíos de colectivos para este domingo
title: Cortes de tránsito y desvíos de colectivos para este domingo
entradilla: A partir de las disposiciones del Ministerio de Seguridad, se informa
  cuáles serán las zonas en las que no se podrá circular y las modificaciones en los
  recorridos del servicio público de pasajeros.

---
Este domingo, en virtud del partido de fútbol que disputarán Colón y Unión en el estadio “Brigadier General Estanisalo López”, la Policía de la provincia llevará adelante una serie de operativos. Estas tareas se concretarán desde las 14, con el objetivo de impedir el acercamiento de personas a la zona de la cancha.

Según informó el Ministerio de Seguridad de la provincia, los lugares donde se ubicarán los puestos de control, que incluyen desvíos al tránsito vehicular y peatonal, son:

– Puente Carretero, a la altura del club Cilsa. El tránsito vehicular será desviado por avenida Mar Argentino

– Avenida Intendente Irigoyen, a la altura de los reductores de velocidad. Se desviará el tránsito que ingresa de la colectora por Mar Argentino hacia Ruta Nacional 11

– J.J. Paso y San José, hacia la rotonda ubicada frente al club Colón

– Dr. Zavalla y pasaje Alighieri, en ambas manos

– Jujuy y Dr. Zavalla

– Dr. Zavalla y Uruguay. Se desviará todo el tránsito al este, por calle Uruguay

– Jujuy y Gobernador Freyre. Se desviará todo el tránsito al este, por calle Uruguay

– J.J. Paso y San Lorenzo, en ambas manos. Se desviará todo el tránsito por calle Dr. Zavalla

– Pietranera y Nicolás Rodríguez Peña

– Bolívar y Vera Mujica, lado oeste

– Nicolás Rodríguez Peña y Libertad

– Entre las manzanas 4 y 8

– Libertad y 10 de Junio

Cabe aclarar que sí se permitirá el ingreso/egreso de personas y/o vehículos que acrediten domiciliarse en el vecindario, con su DNI en mano.

A partir de las 15, la Municipalidad se sumará a la Policía provincial en tareas de control de venta de alcohol en la zona del estadio. Serán cerca de 20 inspectores y agentes de la Guardia de Seguridad Institucional (GSI) que colaborarán con la fuerza pública.

**Cambios de recorrido del transporte urbano**

El municipio informa que desde las 14 operarán las siguientes modificaciones en el recorrido de las líneas de colectivo que circulan por la zona de la cancha:

– Línea 4 (vuelta): de recorrido habitual por Pietranera y J.M. Pérez a recorrido habitual

– Línea 5 (vuelta): de recorrido habitual por J.D. de Solís, Amenábar, Saavedra, Pietranera y J. M. Pérez a recorrido habitual

– Línea 8 (ida): de recorrido habitual por Vera Mujica, Independencia, Zavalía, Gobernador Cabal, Vera Mujica/San Lorenzo, Entre Ríos, Dr. Zavalla a recorrido habitual

– Línea 14 (vuelta): de recorrido habitual por Dr. Zavalla, Amenábar, Saavedra, J.J. Paso a recorrido habitual

Los cambios enumerados se mantendrán vigentes hasta la culminación del partido.