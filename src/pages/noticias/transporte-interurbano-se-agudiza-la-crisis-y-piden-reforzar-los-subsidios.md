---
layout: Noticia con imagen
author: .
resumen: Transporte Interurbano
category: Agenda Ciudadana
title: "Transporte Interurbano: Se agudiza la crisis y piden reforzar los subsidios"
entradilla: El transporte interurbano de Santa Fe atraviesa una situación
  crítica tras siete meses de suspensión de la actividad debido a las medidas
  dispuestas para contener la pandemia de coronavirus.
date: 2020-11-08T18:43:55.005Z
thumbnail: https://assets.3dnoticias.com.ar/terminal.png
---
A pesar de la habilitación del servicio de media y larga distancia por parte del gobierno nacional, la autorización final quedó a cargo de cada jurisdicción y el gobernador Omar Perotti todavía no resolvió cuándo y cómo se reanudará este servicio en la provincia.

Actualmente, sólo están autorizadas aquellas empresas que realizan traslados de hasta 60 kilómetros dentro del territorio provincial, mientras que las que realizan trayectos más largos no están prestando ningún tipo de servicio desde el 20 de marzo. El panorama epidemiológico tampoco ayuda, teniendo en cuenta que Santa Fe es uno de los lugares más afectados por la proliferación del virus.

En diálogo con 3D Noticias, el presidente de ATAP (Asociación de Transporte Automotor de Pasajeros de Santa Fe), Leandro Sólito, aseguró que el sector ya venía en crisis previo a la pandemia a nivel nacional y que la situación de Aislamiento preventivo y obligatorio dispuesto por el presidente de la nación agudizó la problemática en la región.

Ver en nuestro canal de [youtube](https://youtu.be/31OlKHRkmAs)

“Sin lugar a dudas, en esta nueva normalidad, el servicio va a implicar cambios muy profundos. El transporte va a funcionar con capacidades limitadas, menos pasajeros y protocolos para evitar los contagios y va a depender de la ayuda que nos brinde el gobierno provincial para que las empresas soporten esta situación adversa que nos toca vivir” sostuvo Solito.

Hoy las empresas se están sosteniendo con los subsidios que llegan desde Nación y Provincia, pero este sigue siendo el mismo que el configurado en enero de 2020, antes de la pandemia. “Los pagos están al día y la relación con los gremios es muy buena. Obviamente hay discrepancias, como en todo, pero hoy la prioridad es cuidar a los empleados y pensar en continuar con el servicio” remarcó.

Al ser consultado sobre el posible regreso de la actividad, sostuvo que, ante todo, debe haber prudencia. “Si vuelve, prima facie, va a ser para los esenciales, con franjas horarias o con frecuencias muy reducidas” por lo que la vuelta parcial de la actividad no sería una solución integral.

Por último, ante la vuelta del transporte aéreo y las medidas sanitarias que se aplican, consultamos la implementación de medidas en el ámbito del transporte terrestre: “hay empresas que nunca dejaron de trabajar por recorrer distancias menores a 60 km con la ciudad capital, que se pusieron más rigurosos con la higiene y desinfección de los coches, aplicaron los protocolos que elaboró el Ministerio de Transporte de la Nación con requisitos tales como la suspensión de la entrega de viandas, ya no se entregan mantas, se clausuraron los baños a bordo porque si no era necesaria una persona que desinfecte el espacio cada vez que un pasajero lo utilice. Son cuestiones que hay que atenderlas, ser receptivos y estar abiertos a las sugerencias y disposiciones.”