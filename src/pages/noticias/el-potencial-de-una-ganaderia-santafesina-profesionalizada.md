---
category: El Campo
date: 2021-06-29T09:46:14-03:00
thumbnail: https://assets.3dnoticias.com.ar/vacas.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: El potencial de una ganadería santafesina profesionalizada
title: El potencial de una ganadería santafesina profesionalizada
entradilla: El ingeniero Carlos Collins asegura que es posible aumentar la relación
  vaca-ternero en Santa Fe en pocos pasos y tener mayor productividad ganadera. La
  diferencia entre "tener" y "producir".

---
La ganadería santafesina puede dar mucho más y uno de los desafíos pasa por profesionalizar el trabajo en las unidades productivas. Sería clave incrementar la relación vaca-ternero por estas pampas, ya que es más baja que a nivel nacional. Mientras que en el país la relación vaca-ternero promedia el 65%, en Santa Fe oscila entre el 50 y 55%.

En la provincia hay 27 mil unidades productivas y el 51% son campos que cuentan con entre 1 y 500 vacas, es decir se trata de pequeños a medianos productores ganaderos. En muchos casos, sin espalda financiera para la incorporación de tecnología y la contratación de un ingeniero agrónomo que pueda atender cuestiones más allá del obligatorio tema sanitario a cargo de un veterinario.

El ingeniero agrónomo Carlos Collins , asesor ganadero en un campo de cría en la zona de Calchaquí, advierte que la actual situación de la producción ganadera muestra la necesidad de una política estratégica y una planificación integral para el desarrollo del sector. En ese sentido, brindó detalles del escenario actual en la provincia para entender la situación.

“La relación vaca- ternero en Santa Fe siempre fue baja, más baja que en el resto del país. Es un clásico de Santa Fe. Por cada vaca tendría que haber un ternero al año, pero no todas las vacas pueden tener un ternero al año por cuestiones vinculadas a la mala alimentación y problemas sanitarios”, puntualizó Collins.

"En Santa Fe hay 27 mil unidades productivas y el 51% son campos que cuentan entre 1 y 500 vacas

Se refirió al panorama ganadero del Norte santafesino donde se encuentra gran parte de las vacas de cría de la provincia en Agrovisión Profesional, el medio de comunicación institucional del Colegio de Ingenieros Agrónomos de Santa Fe.

El asesor ganadero relató que la relación vaca-ternero depende mucho de las cuestiones climáticas, tanto de lluvias como de sequía, por la falta de previsiones. Indicó que en Santa Fe la relación vaca-ternero hoy está entre un 50% y 55%, mientras que a nivel nacional es entre 63 y 65%.

Collins considera que esta problemática está atada al perfil de los dueños de los animales. “En la zona hay mucha gente tenedora de ganado, es un resguardo, como si fueran dólares. No son realmente productores ganaderos”, destacó.

Frente a esta radiografía del productor ganadero de la zona Collins planteó que “hay evidencia de falta de incorporación de tecnología, de falta de un profesional en esos rodeos“.

“Hay todo un tema de alimentación de la vaca, hay problemas con la vaca que no está bien alimentada para generar un celo y una preñez. Hay cuestiones reproductivas y cuestiones sanitarias del rodeo. En la cuestión nutricional es donde puede intervenir el ingeniero agrónomo”, señaló el especialista en ganadería.

Collins propuso acciones para el universo ganadero de la provincia que está compuesto por 27 mil unidades productivas pero específicamente habló de las actividades que podrían llevar adelante aquellos con de 1 a 500 vacas, el 51% de las unidades productivas santafesinas.

"En la zona hay mucha gente tenedora de vacas, como si fueran dólares, no son realmente productores ganaderos

“Cuando hablas del tema nutrición tenés que hablar de sistema de pastoreo, reservas, trabajar toros para que no sean servicios continuos, tener mangas. Cuestiones básicas para ordenar un rodeo, clasificarlo, sacar vacas que no entran en servicio. Los productores de chicos a medianos chicos tienen poco poder financieramente hablando para incorporar tecnología, contratar a un profesional en ese sentido. El tema sanitario lo hacen pero sólo para la vacunación obligatoria, para análisis reproductivo ya no lo hacen”, reseñó Collins.

En rigor, explicó que el porcentaje de destete tiene un significado y está directamente relacionado con la tasa de extracción del rodeo. Así, si se aumenta la base de terneros, recría, stock en el campo y se trabaja mejor el tema nutricional se va a generar mayor volumen de animales comercializados.

Pero hacer pastoreo en sistemas naturales lleva un tiempo, como todo en ganadería, que es a largo plazo, ya que hay que dejar un descanso para que pueda semillarse, desarrollarse, y los productores más chicos tienen dificultades para llevar adelante ese proceso.

**Poseedores de vacas**

Collins entiende que algunos no incorporan tecnología o profesionales asesores por falta de políticas que lo acompañen en ese proceso y otros porque “son poseedores de vacas, como una caja de ahorro, y no viven del campo”.

Son justamente los establecimientos productivos que integran esta última categoría los que “no tienen interés de que la producción sea más eficiente”.

“Es complejo, no hay un incentivo o castigo para mejorar eso y viste como es el argentino. Se prefiere sólo pagar al cuidador del campo, lo sanitario que es obligatorio y saca por año lo suficiente para mantenerlo y como no vive de ese sistema productivo no mide su eficiencia”, apuntó Collins, quien agregó que normalmente se trata de un productor que ha heredado el campo o gente grande que no cambió su metodología de trabajo.

**Tecnología**

Para Collins, para llegar a unificar “este mosaico de productores” se necesita del Estado para que “por castigo o beneficio obligue a ese productor a ser más eficiente”. Una cuestión difícil de abordar, más en un sector que tiene muchos altibajos producto de la demanda mundial de carne.

“Santa Fe tiene una variedad de suelos, de cultivos. En ganadería tendríamos que estar en un nivel de dos o tres veces de lo que estamos ahora. A nivel nacional estamos en 56 kilos de carne de producción anual, es muy bajo. En países más avanzados en temas productivos están en 80, 110 ó 120 kilos. Países como Canadá o Estados Unidos tienen una cantidad más elevada de kilos por animal por año. Esto es para darnos idea de la potencialidad del país y, simplemente, es con la incorporación de tecnología y profesionalización de la actividad“, indicó Collins.

Para el asesor ganadero es clave aumentar la participación del veterinario, que ya participa, y es fundamental enfocarse en el tema nutricional y ahí es importantísimo el rol del ingeniero agrónomo. Ordenar el rodeo lleva indefectiblemente a aumentar la productividad. Collins asegura que se puede lograr aumentar la producción unos 20 ó 30 kilos por año con manejo de pastoreo racional, división del rodeo, teniendo reservas, generando mayor cantidad de esos pastos naturales.

“Si pasás a otro nivel e incorporás más tecnologías de procesos aumentás 10 kilos y con tecnología de insumos aumentás un 20 más y después pasas a intensivos que necesita una mayor interdisciplinariedad, que vaya más a fino como sumar un nutricionista”, resaltó.

**Incentivo estatal**

Hay todo un proceso que se puede implementar y Collins advierte que se necesitan políticas de Estado para aumentar su producción en las unidades productivas de medianas a chicas. Es que todo cuesta dinero y estos productores son los que tienen más dificultades financieras.

No obstante, Collins asegura que el aporte que pueda realizar el Estado en verdaderos créditos para el sector se compensaría por una menor afectación de los fondos de la provincia al momento de eventos climáticos que es cuando el productor demanda un rescate oficial.

“Para dividir un potrero y hacer un alambrado se necesita dinero. El Estado podría dar créditos y a su vez solicitar que esos productores entreguen un programa de crecimiento, no es sólo dar plata. Hay que poder llegar territorialmente, hay muchos productores que no están bancarizados, sobre todo los chicos de 50 vacas que hoy subsisten”, detalló.

Consideró necesario un plan general, a largo plazo, porque “no es algo que se pueda hacer de un año para otro ni en cuatro años como duran los gobiernos”.

Collins consideró que “tiene que haber reglas claras y un costo para el que no quiera incorporar tecnología”.

Aseguró que la situación “es un problema para el Estado” porque cuando hay sequía o inundación tienen que ir a rescatar a los productores afectados mientras que si se trabaja profesionalmente se estabiliza el rodeo de las condiciones climáticas, se hace reserva y manejo del agua, todo es diferente. “El productor que no incorpora tecnología al Estado le sale más caro, si no es por sequía es por inundación“, subrayó.

Collins apuntó a que también es fundamental que toda la cadena cárnica esté en armonía -frigoríficos, matarifes, la parte comercial y de insumos- para mejorar la producción.

No hay vueltas, para Collins, la política enfocada en aumentar la producción se logra con profesionalización. Un profesional que trabaje en el desarrollo de esa producción, para generar un alimento inocuo y seguro, cuidando el medioambiente. Y al aumentar todos estos índices se generan mayores ingresos y se pueden manejar escenarios de mínima y de máxima, para tener un impacto menor ante una eventualidad.

“Es una pena por las condiciones que tenemos en Argentina que no estemos mejor. La carne tiene un alto valor agregado. Podríamos estar en otro nivel y no seguir viendo cómo arrancamos”, finalizó Collins, con la esperanza de que el sector ganadero de la provincia pegue un salto de calidad.