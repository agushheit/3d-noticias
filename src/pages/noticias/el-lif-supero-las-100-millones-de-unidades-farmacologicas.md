---
category: Estado Real
date: 2021-01-29T10:06:22Z
thumbnail: https://assets.3dnoticias.com.ar/lif.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: El LIF superó las 100 millones de unidades farmacológicas
title: El LIF superó las 100 millones de unidades farmacológicas
entradilla: 'El Laboratorio Industrial Farmacéutico avanza en la producción pública
  de medicamentos.

'

---
Durante el año 2020, el Laboratorio Industrial Farmacéutico (LIF) produjo 35 especialidades medicinales diferentes, con un total de 101.303.167 unidades (en diferentes formas farmacológicas: ampollas, comprimidos, frascos, goteros y pomos).

Toda la producción del LIF está destinada a proveer al sistema público de salud de la provincia, garantizando el acceso a medicamentos estratégicos y gratuitos a la sociedad santafesina. En este sentido, gracias a la intervención del propio personal del Área de Logística del LIF se puede llegar a todo el territorio provincial: mensualmente se realizan entregas a las droguerías de las 5 regiones del sistema de salud de Santa Fe (Rafaela, Reconquista, Rosario, Santa Fe, Venado Tuerto), a las que se suman el Hospital Cullen, el Hospital Provincial del Centenario y la Droguería Central.

El LIF es una Sociedad del Estado de la provincia de Santa Fe, su objetivo es la producción y abastecimiento de productos farmacéuticos y servicios para la salud de la población, mejorando la calidad de vida de todos los santafesinos.

Con respecto al balance del 2020, el gerente general del LIF, Diego Bruno, sostuvo que: “Hemos cumplido con los objetivos que nos planteamos a inicios de año: por un lado, la producción planificada anual fue alcanzada y superada, con un número mayor a los 100 millones de unidades farmacológicas; por el otro, en una coyuntura de emergencia sanitaria, pudimos responder también a las demandas excepcionales de la pandemia, gestionando la adquisición Elementos de Protección personal y de las drogas específicas para los pacientes que necesitaron Respiración Mecánica Asistida”.

En lo que respecta a la producción pública de medicamentos, vale destacar que entre los insumos que más se produjeron en el 2020, se destaca la Metformina LIF 500mg (29.450.170 de comprimidos) que es un hipoglucemiante oral que ayuda a reducir la glucosa en sangre, indicado en el tratamiento de diabetes mellitus tipo 2, combinado con dieta y ejercicio físico. El segundo fue el Enalapril Maleato LIF 10 mg que está indicado para tratar la hipertensión (presión arterial alta), la insuficiencia cardíaca sintomática, para prevenir la asintomática y evitar accidentes cerebro vasculares.

Para finalizar con el balance, Bruno concluyó: “Tengo que agradecer al personal del LIF por su labor cotidiano ya que, a pesar de la contingencia, hemos trabajado en conjunto y redoblando esfuerzos para poder cumplir con nuestro compromiso con la salud pública. Desde personal directamente afectado y aislado por el virus, hasta parte del equipo que por ser mayores de 60 años o con niños menos de 6 que han trabajado desde sus casas, cada integrante de la comunidad LIF aportó desde sus tareas para cumplir con nuestro rol social y estratégico en la salud pública de Santa Fe”.