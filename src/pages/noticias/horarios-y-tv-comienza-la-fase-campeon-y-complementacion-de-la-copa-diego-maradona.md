---
category: Deportes
date: 2020-12-11T16:30:44Z
thumbnail: https://assets.3dnoticias.com.ar/copa.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: El Litoral'
resumen: 'Horarios y TV: Comienza la Fase Campeón y Complementación de la Copa Diego
  Maradona'
title: 'Horarios y TV: Comienza la Fase Campeón y Complementación de la Copa Diego
  Maradona'
entradilla: Este viernes comienza la primera fecha de la Fase Campeón y Complementación
  de la Copa Diego Maradona.

---
En cuanto a los equipos santafesinos, Colón jugará la Fase Campeón y enfrentará este sábado a Gimnasia La Plata, a partir de las 17.10 y con transmisión de Fox Sports Premium.

Por el lado del Tate, los de Azonzábalal jugarán la Fase Complementación y recibirán a Defensa y Justicia, el domingo desde las 17.10 y con transmisión de Fox Sports Premium.

***

[![Infogram](https://assets.3dnoticias.com.ar/infogram.jpg)](https://infogram.com/copy-copa-diego-maradona-fecha-6002-1h0r6rppek95l2e "Infogram")

