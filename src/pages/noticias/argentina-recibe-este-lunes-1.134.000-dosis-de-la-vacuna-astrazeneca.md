---
category: Agenda Ciudadana
date: 2021-06-21T06:15:01-03:00
thumbnail: https://assets.3dnoticias.com.ar/ZINOPHARM.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Argentina recibe este lunes 1.134.000 dosis de la vacuna AstraZeneca
title: Argentina recibe este lunes 1.134.000 dosis de la vacuna AstraZeneca
entradilla: Además, a última hora de la tarde también arribará en vuelo de Aerolíneas
  Argentina proveniente de China, un cargamento con vacunas del laboratorio Sinopharm.

---
La Argentina recibe este lunes más de un millón de vacunas contra el coronavirus del laboratorio AstraZeneca provenientes de los Estados Unidos y un cargamento de dosis de Sinopharm que traslada un vuelo especial de Aerolíneas Argentinas desde China.

El segundo de los dos vuelos de Aerolíneas Argentinas que transportarán desde China las vacunas Sinopharm partió este domingo al mediodía desde el aeropuerto internacional de Ezeiza, mientras el primero, que salió el sábado, ya despegó de regreso desde Beijing y está previsto que el lunes por la tarde arribe a la Argentina.

A la vez, un nuevo envío de Oxford-AstraZeneca con un total de 1.134.000 dosis, cuya sustancia activa fue producida en mAbxience, en Garín, y terminadas en la planta AMRI de Albuquerque (Estados Unidos), arribará al país este lunes en un avión de la empresa Latam Cargo Colombia.

**Hacia y desde China**

Este mediodía de domingo despegó desde Ezeiza el segundo vuelo de Aerolíneas Argentinas rumbo a China. La aeronave Airbus 330-200, matrícula LV-GHQ, bajo el número AR1066, decoló a las 13.18 y, tras realizar una escala técnica en Madrid para reabastecimiento de combustible, arribará a Beijing a las 0.15 del martes (hora local en China, las 13.15 del lunes, en la Argentina).

Está previsto que el avión permanezca en suelo chino hasta las 3.15 (hora local, las 16.15 del domingo en Argentina) y emprenderá el regreso haciendo una escala de reabastecimiento en el aeropuerto de Barajas, para aterrizr finalmente, en el aeropuerto de Ezeiza a las 19.10 del lunes 21.

Mientras tanto el primero de los vuelos aterrizó en Beijing a las 13.15 de nuestro país, las 0.15 de China y luego de permanecer por espacio cuatro horas en tierras chinas, emprendió el regreso a las 16.47 de nuestro país, estando previsto que esté de regreso en la Argentina este lunes alrededor de las 19, luego de la escala técnica en Madrid.

"Con estos vuelos, realizamos 26 operaciones y superamos los 15 millones de dosis transportadas", dijo el presidente de Aerolíneas Argentinas, Pablo Ceriani. "Otra excelente noticia que permitirá continuar con la mayor campaña de vacunación y que más argentinos y argentinas puedan vacunarse. Estamos cumpliendo una tarea fundamental cuando el país más lo necesita", agregó.

"Con estos dos vuelos seguimos alimentando la campaña de vacunación y la esperanza de poder salir pronto de esta crisis sanitaria sin precedentes. Estamos orgullosos de poder cumplir un rol tan importante cuando el país más lo necesita", señaló Ceriani a la prensa.

Se trata de la vigesimoquinta y vigesimosexta operación de traslado de vacunas realizada por la empresa, y con estas se completarán siete misiones a Beijing para traer vacunas.

Además, este tipo de carga, por indicación estricta del laboratorio, solo puede ser trasladado en la bodega de las aeronaves.

Hasta el sábado, Argentina llevaba 20.615.390 de dosis de vacunas distribuidas entre las 24 jurisdicciones.

Hasta el sábado, Argentina llevaba 20.615.390 de dosis de vacunas distribuidas entre las 24 jurisdicciones.

Aerolíneas Argentinas lleva completados un total de cinco vuelos a la República Popular China, en los cuales fueron trasladas 3.659.200 de vacunas producidas por el China National Pharmaceutical Group Corp.

Además, se realizaron hasta el momento 19 vuelos a Moscú, en los que se transportaron 9.473.290 dosis, por lo que en 24 operaciones realizadas se trajeron un total de 13.132.490 de dosis.

En tanto, para esta semana se espera también que se concreten nuevos vuelos a Moscú en busca de cargamentos de la vacuna Sputnik V.

**El Plan de Vacunación**

Hasta este sábado Argentina lleva 20.615.390 de dosis de vacunas distribuidas entre las 24 jurisdicciones, de las cuales ya fueron aplicadas 17.837.121, según los datos del Monitor Público de Vacunación.

Según los registros oficiales, el plan nacional de vacunación avanzó la semana pasada de "manera constante" entre los menores de 60 años, mientras que de los mayores de esa edad ya está "casi el 90 por ciento" inmunizado en todo el país, al menos con una primera dosis.

En tanto, el 68 por ciento de las personas entre 55 y 59 años inició su esquema de vacunación, como así también el 59 por ciento de quienes tienen entre 50 y 54 años y el 44 por ciento de las personas entre 45 y 49 años.

En el marco del Plan Estratégico de Vacunación contra el coronavirus, el más grande de la historia, en el último mes se aplicaron en todo el país 7.643.753 dosis, con un promedio diario de 239.575 inoculaciones, mientras que en ese lapso arribaron a la Argentina 7.979.000 vacunas, según datos del Ministerio de Salud de la Nación que abarcan desde el 18 de mayo hasta el 18 de junio.

En este sentido, la semana del 31 de mayo al 6 de junio fue la que más vacunados tuvo, con 2.078.867 personas, con un promedio diario de 296.981. En tanto, del 18 al 23 de mayo hubo 956.100 inoculados, con un promedio de 159.350 por día; del 24 al 30 de mayo hubo 1.107.829, con 158.261 diarios; del 7 al 13 de junio hubo 2.045.879, con 292.268 de promedio; y del 14 al 18 de junio se vacunó a 1.455.078 personas, lo que significó 291.016 por jornada.

Uno de los resultados más importantes que tuvo la escalada de vacunación es haber progresado sustantivamente con los vacunados mayores de 60 años, ya que "casi el 90 por ciento de estas personas cuentan con la primera dosis de vacuna contra la Covid-19", señaló la ministra de Salud, Carla Vizzotti.

Respecto a las dosis arribadas al país en el último mes, también la semana del 31 de mayo al 6 de junio fue la de mayor cantidad recibidas: un total de 2.966.750, de las cuales 2.148.600 fueron AstraZeneca y 818.150 Sputnik V componente 1.

En tanto, del 18 al 23 de mayo llegaron 204.000 dosis del sistema Covax; del 24 al 30 de mayo arribaron 2.581.200 (1.080.00 Sputnik V entre componentes 1 y 2; 843.600 AstraZeneca y 657.600 Covax); del 7 al 13 de junio hubo 1.292.850 arribos (481.850 Sputnik V componentes 1 y 2; y 811.000 AstraZeneca); y finalmente del 14 al 18 de junio llegaron 934.200 AstraZeneca.

Además, de acuerdo a datos presentados por el Ministerio de Salud, la letalidad por Covid-19 disminuyó respecto del año pasado: en 2020 fue de 2,8% y en lo que va de 2021 es de 1,4%. De acuerdo a las franjas etarias, en menores de 60 años la letalidad cayó de 0,6% en 2020 y a 0,4% en 2021. En los mayores de 60 años disminuyó de 15,9% a 8,4% este año.