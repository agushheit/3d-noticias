---
category: Agenda Ciudadana
date: 2021-06-18T09:54:58-03:00
thumbnail: https://assets.3dnoticias.com.ar/barletta.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de El Ciudadano Web
resumen: Se empiezan a ordenar las piezas en la interna de Juntos por el Cambio
title: Se empiezan a ordenar las piezas en la interna de Juntos por el Cambio
entradilla: El PRO, la UCR macrista y la Coalición Cívica, sumaron a los radicales
  NEO y se perfilan al menos cuatro propuestas para las primarias. La periodista Carolina
  Losada se mostró junto a Barletta y Anita Martínez.

---
En las últimas semanas, la interna santafesina de Juntos por el Cambio empieza a perfilarse. De cara a la elección de diputados y senadores nacionales, los sectores históricos del PRO, la UCR macrista y la Coalición Cívica que pujan por la conducción del espacio sumaron al radicalismo NEO que lideran Maximiliano Pullaro y Felipe Michlig y además empiezan a aparecer caras nuevas, como la periodista de América TV y Radio Rivadavia Carolina Losada, quien en las últimas horas se mostró junto a Mario Barletta y Anita Martínez. A un mes del cierre de las candidaturas nacionales, las huestes santafesinas de Juntos por el Cambio buscan reunir el mayor volumen político posible para ir a una interna competitiva en las Paso de agosto y quedar bien posicionados de cara a las generales de noviembre.

Mientras a nivel nacional la colación atraviesa momentos de tensión por la puja que se abrió en la provincia y la ciudad de Buenos Aires, en Santa Fe se van reuniendo las piezas de un complejo rompecabezas que este año tendrá nuevos actores –los radicales del NEO– y figuras que llegan de afuera de la política, como la periodista Losada –rosarina y de buena llegada al presidente de la UCR nacional Alfredo Cornejo–, quien evalúa dejar las pantallas para sumarse a la oferta electoral de Juntos por el Cambio.

    https://twitter.com/carolinalosada/status/1405231112763449348?ref_src=twsrc%5Etfw%7Ctwcamp%5Etweetembed%7Ctwterm%5E1405231112763449348%7Ctwgr%5E%7Ctwcon%5Es1_&ref_url=https%3A%2F%2Fwww.elciudadanoweb.com%2Fcon-caras-nuevas-se-empiezan-a-ordenar-las-piezas-en-la-interna-de-juntos-por-el-cambio%2F

Este miércoles Losada se reunió en Rosario con Mario Barletta (UCR) y Anita Martínez (PRO). La actual concejala rosarina publicó la foto del encuentro en sus redes sociales con el siguiente mansaje: “La Argentina nos necesita juntos. Hoy conversamos de Santa Fe con Carolina Losada y Mario Barletta”. La periodista compartió la imagen con un escueto agregado: “Más juntos que nunca para la Santa Fe que necesitamos”.

Mientras tanto, en los otros campamentos de Juntos por el Cambio se empiezan a delinear los límites de cada sector interno. El diputado nacional, ex presidente del PRO santafesino y actual vicepresidente del partido a nivel nacional, Federico Angelini, encabeza uno de los espacios referenciados en las figuras de Mauricio Macri y Patricia Bullrich.

Otro dirigente del PRO –el concejal Roy López Molina– junto al dirigente de la UCR y ex candidato a gobernador José Corral construyen su propia propuesta al amparo del jefe de Gobierno porteño Horacio Rodríguez Larreta. En ese espacio quieren fichar a dirigentes del PDP santafesino.

    https://twitter.com/GugaLusto/status/1403375787579957255?ref_src=twsrc%5Etfw%7Ctwcamp%5Etweetembed%7Ctwterm%5E1403375787579957255%7Ctwgr%5E%7Ctwcon%5Es1_&ref_url=https%3A%2F%2Fwww.elciudadanoweb.com%2Fcon-caras-nuevas-se-empiezan-a-ordenar-las-piezas-en-la-interna-de-juntos-por-el-cambio%2F

Recién llegados a la coalición, los popes de la UCR-NEO (Pullaro, Michlig y el ex vicegobernador y actual presidente del radicalismo provincial Carlos Fascendini) también apuntalan su propio espacio. La semana pasada Pullaro y Michlig estuvieron en Buenos Aires y compartieron foto con Rodríguez Larreta y con el senador nacional Martín Lousteau, de la UCR-Evolución.

Sin confirmaciones ni lanzamientos oficiales, el macrismo santafesino podría tener hasta cuatro propuestas para las primarias de legisladores nacionales, lideradas por Angelini, Pullaro, Corral-López Molina y Barletta-Losada. Pero todavía falta mucho y aún quedan casilleros por completar.