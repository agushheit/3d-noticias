---
category: Agenda Ciudadana
date: 2022-02-16T11:25:01-03:00
thumbnail: https://assets.3dnoticias.com.ar/amsafecap.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3d noticias
resumen: Convocan a paritaria provincial docente
title: Convocan a paritaria provincial docente
entradilla: La Reunión de Paritaria Docente es convocada para hoy Miércoles 16 de
  febrero por la tarde.

---
Según publica el gremio docente en su pagina web, la representación de AMSAFE presentará, entre otros, los siguientes temas:

– Salario.

– Presencialidad Segura

– Condiciones de Trabajo: Salud, Infraestructura, situación epidemiológica, protocolos, insumos, etc.

– Transporte, Boleto Rural, Boleto Educativo.

– Carrera docente: Concursos, traslados, suplencias y escalafonamientos pendientes.

– Ofrecimiento de todos los cargos y horas cátedra vacantes.