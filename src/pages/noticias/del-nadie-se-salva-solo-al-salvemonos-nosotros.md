---
category: Agenda Ciudadana
date: 2021-02-24T05:59:32-03:00
thumbnail: https://assets.3dnoticias.com.ar/ximenagarcia.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Ximena García
resumen: Del "Nadie se salva solo", al "Salvémonos nosotros"
title: Del "Nadie se salva solo", al "Salvémonos nosotros"
entradilla: 'Una vez más somos testigos de la vergonzosa manipulación que el Frente
  de Todos realiza con los recursos públicos. '

---
Una vez más somos testigos de la vergonzosa manipulación que el Frente de Todos realiza con los recursos públicos. En esta oportunidad no solo se trata de desprolijidades y desorganizaciones sino de intencionados abusos en pos de beneficiar a un grupo reducido de argentinos por su cercanía familiar o política. 

Según el Plan Estratégico de Vacunación COVID-19 (Res. 2883 del 29/12/20) la vacunación debía ser equitativa e igualitaria y por etapas, donde en primer lugar se inoculaba al personal de salud, a las personas mayores de 60 años, a las Fuerzas Armadas y de Seguridad, a grupos de riesgo y al personal docente y no docente.

No obstante esta clara priorización, ha trascendido a partir de manifestaciones en medios de comunicación masivos y del listado publicado por el Hospital Posadas de Buenos Aires, que han sido inoculados miembros de los poderes públicos del Estado y sus allegados, que aún no han explicado en su totalidad las circunstancias de dicho beneficio. 

La vacunación VIP e irregular no ha hecho más que desnudar una realidad que hoy es demasiado tangible. El peronismo entiende que el Estado, y sus recursos económicos, humanos y materiales, se encuentran a su disposición por el solo hecho de haber ganado las últimas elecciones. Con sus actos denostan a las instituciones, pretendiendo esconder detrás de la remoción de un ministro y de una actitud inocente y desprevenida un sin fin de decisiones premeditadas y consentidas.  

Con sus conductas han pisoteado los principios y derechos consagrados en nuestra Constitución Nacional y las propias normas que ellos dictaron para la administración de las vacunas contra el COVID-19. Han demostrado lo más nefasto y arraigado de su ideología político-partidaria que se encuentra nutrida de privilegios para algunos pocos. Han pasado sin frenos del, nadie se salva solo, al, salvémonos nosotros, cumpliendo así, una una vez más con la arcaica premisa, "para los amigos todo, para los enemigos ni justicia".

Por estos motivos, y asumiendo una posible vulneración a los principios y derechos consagrados en nuestra Constitución Nacional es que desde Juntos por el Cambio presentamos más de diez pedidos de informes reclamando datos certeros sobre la campaña de vacunación y sobre los legisladores inoculados irregularmente como Sergio Massa, Eduardo Valdez y Jorge Taiana. Solicitamos además la inmediata creación de la Comisión Bicameral de Seguimiento del Plan de Vacunación Contra el COVID-19. El acceso a la información es una de las premisas fundamentales para lograr un adecuado control y transparencia en el Estado. 

Es importante destacar que la causa judicial que investiga al ex ministro Gines González García y que impulsa el fiscal Eduardo Taiano está avanzando rápidamente a partir del allanamiento ordenado por la jueza federal María Eugenia Capuchetti que se suma a otras medidas solicitadas para allanar el Hospital Posadas y recabar las filmaciones de las cámaras de seguridad apostadas en ambos edificios. Esta causa se suma a otras diez iniciadas por delitos por violación de deberes de funcionario público, propagación de enfermedades, malversación de caudales, tráfico de influencias, entre otros.