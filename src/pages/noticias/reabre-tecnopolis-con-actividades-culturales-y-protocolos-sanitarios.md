---
category: Agenda Ciudadana
date: 2021-01-06T09:37:52Z
thumbnail: https://assets.3dnoticias.com.ar/060121-tecnopolis.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Télam'
resumen: Reabre Tecnópolis con actividades culturales y protocolos sanitarios
title: Reabre Tecnópolis con actividades culturales y protocolos sanitarios
entradilla: "«La reserva de entradas se habilitó el lunes y en dos horas se agotaron»,
  dijo la directora del parque. El uso de barbijo será obligatorio y  se hará foco
  en la higienización del lugar."

---
**Tecnópolis volverá a abrir sus puertas este miércoles con actividades culturales gratuitas para todas las edades y con protocolos sanitarios** para garantizar el cuidado de artistas y visitantes, que agotaron la reserva de entradas en un par de horas, se informó hoy.

Bajo el lema **​Reencuentros Cuidados**​, la megamuestra de ciencia, arte y tecnología más grande del país ofrecerá una nueva modalidad de encuentros artísticos con formato burbuja que se extenderán hasta el 16 de febrero.

«Trabajamos todo el año en poner el parque a disposición de los enfermos leves de Covid-19 y ahora lo recuperamos desde el 1 de diciembre. Apenas lo hicimos, pensamos en hacer los atardeceres con propuestas artísticas. Queremos que los bienes culturales estén disponibles para la gente después del año tan difícil que tuvimos», dijo a Télam María Rosenfeldt, directora del parque.

«Además, otro objetivo fue reactivar el trabajo del sector que viene tan golpeado en el 2020», destacó sobre el trabajo de la industria cultural.

**Durante 27 jornadas, las y los visitantes podrán disfrutar de propuestas culturales presenciales y gratuitas al aire libre con capacidad limitada en cuatro espacios del predio**, diseñados especialmente para garantizar el cumplimiento de todos los protocolos y recomendaciones sanitarias en el marco del distanciamiento social vigente.

«Vamos a trabajar mucho el concepto de cuidado. La experiencia no va a ser la misma, ya que no se va a poder recorrer libremente Tecnópolis, pero sí en cada uno de los cuatro espacios tenemos diversas propuestas culturales», aseveró Rosenfeldt a Télam.

A diferencia de las ediciones anteriores, **el público debe reservar sus entradas con anticipación a través de la página web de Tecnópolis**, [www.tecnopolis.gob.ar](https://tecnopolis.gob.ar/).

«La reserva de entradas se habilitó ayer y en dos horas se agotaron. Evidentemente la demanda es muy fuerte», reconoció la directora del parque.

«Cada espacio tiene distintas dinámicas, hay algunos que tienen un aforo de 200 personas por cada uno de los dos turnos, mientras que en la explanada donde tenemos un gran escenario de 13.000 m², hay diferentes burbujas que contemplan el distanciamiento de 2 metros y que pueden estar hasta 3 personas en cada burbuja», explicó.

El uso de barbijo será obligatorio y se hará foco en la higienización del lugar, ya que **de un espectáculo a otro va a pasar una hora y media para poder desinfectar**.

La apertura de los Atardeceres en Tecnópolis tendrá lugar mañana a las 20 horas con un espectáculo musical y audiovisual que contará con la participación de destacados intelectuales y artistas como Darío Sztajnszrajber, Urraka, El Choque Urbano, La Arena Circo, Mariel Puyol, Milo Moya, Agustin Franzoni y Facundo Fack Nieto, entre otros. El cierre musical estará a cargo de Bersuit Vergarabat.

**En el ingreso al Parque se exhibirá una muestra homenaje a Diego Maradona con fotografías del archivo de la Agencia Nacional de Noticias Télam.**

«Estamos entusiasmados y con mucha responsabilidad, articulando con otras instituciones del Estado como es el caso de Télam para hacer una muestra de Maradona en la explanada. Desde la General Paz ya se ven las fotos, que son impresionantes», valoró Rosenfeldt.

**Todos los viernes se realizarán ​proyecciones de películas nacionales con música en vivo​ en un ciclo presentado en conjunto con el INCAA y Canal Encuentro.**

Los sábados, Tecnópolis y Pakapaka presentarán El show de Zamba y Nina​, una propuesta audiovisual e interactiva para jugar, bailar y reír. En el cierre de la semana, los domingos se podrá disfrutar de un club de cine.

En el escenario del Arco, en la entrada principal del Parque, se presentarán diferentes propuestas artísticas de jueves a domingos, desde el Ciclo Tec Conversa con Darío Sztajnsrajber junto a invitadas e invitados especiales, hasta shows de stand-up presentados por Diego Wainstein, encuentros de improvisación y percusión​, espectáculos infantiles y conciertos especiales de distintos géneros con destacados artistas como el Chango Spasiuk, La Chicana, Bruno Arias, La Delio Valdez, La Bomba de Tiempo, Santiago Vázquez y El Choque Urbano, entre otros.