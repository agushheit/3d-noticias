---
category: La Ciudad
date: 2021-12-29T06:00:07-03:00
thumbnail: https://assets.3dnoticias.com.ar/JATON.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'El balance del intendente, con una proyección: cómo conseguir un desarrollo
  más equitativo'
title: 'El balance del intendente, con una proyección: cómo conseguir un desarrollo
  más equitativo'
entradilla: 'Emilio Jatón mencionó que "su compromiso es con los sectores vulnerables,
  pero su deber es gobernar para el conjunto de la sociedad".  '

---
"Todos debemos entender que si no estimulamos el desarrollo de Santa Fe, si no fomentamos inversiones locales, si no favorecemos la creación de empleo, las políticas orientadas a los sectores populares serán siempre políticas de asistencia", remarcó el mandatario.

El intendente Emilio Jatón repasó lo hecho durante este 2021 y contó cómo proyecta el año que está a punto de iniciar. A continuación, sus palabras. 

 La gestión ha atravesado dos años especialmente complejos, no solo por las dificultades macroeconómicas del país y la crisis social en la que vivimos sino también por la pandemia y sus efectos. Muchos proyectos que teníamos diseñados se debieron postergar y otros tantos quedaron obsoletos frente a la nueva realidad que provocó esta gran crisis.

 Con la Pandemia las relaciones cambiaron para siempre, tanto en los planos personal como institucional, y tuvimos que repensar estrategias, políticas públicas y hasta la vinculación con los y las vecinas desde una perspectiva muy distinta. Sin embargo, nada de eso nos detuvo y hoy la ciudad de Santa Fe está en marcha: con proyectos concretos, pero también con austeridad y transparencia.

 La pandemia también nos hizo ver con claridad que el futuro no puede ser una simple continuación del pasado. La experiencia individual y colectiva de estos dos últimos años significará una transformación sustancial de muchas de nuestras antiguas formas de hacer cosas: del trabajo al estudio, de los usos del espacio público a la movilidad urbana o la relación con el ambiente.

 En primer lugar, orientamos nuestro esfuerzo a recuperar a los sectores más vulnerados para comenzar a reparar un abandono de larga data, y para contribuir a mitigar las dificultades del presente. Siempre lo digo, no hacemos obras, hacemos proyectos sociales que transforman la ciudad: Camino Viejo, Beruti, recuperación de la Estación Mitre, Cabal y Las Lomas, restauración del Anfiteatro.

 No se trata solo de infraestructura o de enumerarla (se pueden ver todas las acciones en [https://santafeciudad.gov.ar/transparencia/obras/](https://santafeciudad.gov.ar/transparencia/obras/ "https://santafeciudad.gov.ar/transparencia/obras/")), sino también de un conjunto de acciones tendientes a generar nuevos frentes urbanos en una ciudad con desequilibrios marcados, a mejorar la calidad de vida de la ciudadanía en términos de acceso a servicios y a desarrollar una participación popular en decisiones del municipio.

 El diálogo es primordial y por eso construimos proyectos colectivos junto a vecinos y vecinas, lo que hace más sólidos los cimientos del Estado. Nunca más las obras se definirán en la soledad de un escritorio y por eso vamos a poner en marcha durante 2022 un Laboratorio de Innovación Pública y Social (UrbanLab) para que los y las santafesinas pensemos en la Santa Fe del futuro.

 Así como es cierto que nuestro compromiso personal, moral y político, es con los sectores más vulnerables de nuestra ciudad, no es menos cierto que nuestro deber es gobernar para el conjunto de la sociedad y desde el UrbanLab convocaremos a todos. No es el gobierno el que debe imaginar en soledad el futuro que queremos compartir, sino la sociedad en su conjunto. Y este es el momento adecuado para hacerlo

 Todos debemos entender que si no estimulamos el desarrollo de Santa Fe, si no fomentamos inversiones locales, si no favorecemos la creación de empleo, las políticas orientadas a los sectores populares serán siempre políticas de asistencia. De ese modo, ni contribuiríamos a resolver el problema de los más relegados, ni contribuiríamos a hacer de esta una ciudad mejor, más integrada, más homogénea, con menos conflictos y menos inseguridad.

 Nuestra ciudad cumplirá en 2023 sus primeros 450 años de vida. Como todo aniversario, puede ser simplemente una celebración del tiempo pasado; pero puede ser también un momento de reflexión sobre el futuro. Vamos a trabajar para que la ciudad del futuro no sea resultado de las inercias del pasado sino el producto de nuestros deseos e intereses, de nuestra imaginación y de nuestras ideas.

 En una época marcada por la movilidad, nuestra ciudad compite con otras muchas. Compite por recursos humanos, para retener los que existen, para atraer otros, para desarrollar y formar nuevos. Compite por inversiones, imprescindibles para el desarrollo urbano, para generar empleo, para crear bienes privados y públicos de calidad. No podremos retener ni atraer personas y capitales si no tenemos una ciudad dinámica y ambiciosa. 

 Sobre todo, una ciudad innovadora, una ciudad que esté a la vanguardia en términos de cuidado ambiental y de espacio público, de movilidad urbana, de tecnología, de producción, de calidad de la gestión gubernamental, de generación y uso de la información, de cultura, de planeamiento urbano. Sobre todo, una ciudad con la capacidad de repensarse permanentemente, porque ninguna solución será, en un futuro dinámico y acelerado, definitiva.

 No tengo dudas de que existen en nuestra ciudad, en nuestra sociedad santafesina, todas las capacidades para hacer de Santa Fe una ciudad ejemplar. Hay aquí empresarios talentosos, científicos brillantes, un mundo cultural variado y dinámico, una sociedad civil activa y robusta, un sector público comprometido. Contamos con todos los recursos para que Santa Fe sea una ciudad cargada de futuro, una ciudad ejemplar. 

 Queremos, quiero, que construyamos una ciudad para todos: una ciudad justa e integrada, pero también una ciudad pujante, próspera, productiva, dinámica, en contacto con la provincia, con el país y con el mundo, una ciudad segura y sostenible, una ciudad moderna, una ciudad innovadora y ambiciosa. Una ciudad para todos los santafesinos y santafesinas, una ciudad que no será posible sin los santafesinos y santafesinas.