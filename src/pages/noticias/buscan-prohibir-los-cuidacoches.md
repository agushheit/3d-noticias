---
category: La Ciudad
date: 2021-07-02T08:52:25-03:00
thumbnail: https://assets.3dnoticias.com.ar/SM-Mastropaolo-Ceresola-120920.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: Buscan prohibir los cuidacoches en la ciudad de Santa Fe
title: Buscan prohibir los cuidacoches en la ciudad de Santa Fe
entradilla: Los  Concejales  del  bloque  Pro  de  la  ciudad  de  Santa  Fe  presentaron  PROYECTO  DE
  ORDENANZA prohibiendo definitivamente a los “trapitos” y limpiavidrios en la vía
  pública.

---
El proyecto, elaborado por los ediles Luciana Ceresola y Sebastián  Mastropaolo (Pro-Juntos por el Cambio), tiene por objeto prohibir el servicio de estacionamiento, cuidado de vehículos  y  limpieza  de  vidrios,  sin autorización legal, en  perjuicio  del  derecho  de  uso  del espacio público. 

Existen  sujetos que  ilegalmente  se  apropian  de  este espacio  pretendiendo  brindar  un servicio  de  estacionamiento  irregular,  muchas  veces  bajo  amenazas  o  agresiones. “No podemos  permitir la apropiación  del  espacio público,  espacio democrático por  excelencia  y debemos además velar   por   la   seguridad   del   ciudadano   muchas   veces   amenazado e intimidado” 

El texto sanciona con uno (1) a cinco (5) días de trabajos no remunerados de solidaridad con la comunidad o multa de ciento cincuenta (150) a mil quinientas (1.500) unidades fijas. Cuando  exista  organización  previa,  la  pena  para  los  partícipes  es de  cinco  (5)  a  quince  (15) días de arresto y se eleva al cuádruple para los jefes y/o coordinadores. Agravándose además si se presta en inmediaciones donde esté programado un evento masivo. 

Incontables son los hechos de inseguridad que se viven a diario a causa de los “trapitos” y limpiavidrios que se encuentran a lo largo y ancho de nuestra ciudad. Esta propuesta busca lograr una eficiente regulación de la vía pública en pos de la prevención y tratamiento de los conflictos urbanos, bregando por el derecho de todos los ciudadanos al uso de ese espacio público contra la apropiación ilegal del mismo.  

El Departamento Ejecutivo Municipal tuvo en sus manos la herramienta para hacerlo. “Hemos  realizado  pedidos  de  informes  ante  su  incumplimiento. Aun  así,  no  lo ha  puesto  en marcha y las calles de la ciudad se ven inmersas en un descontrol cada vez peor dónde incluso podemos hacer referencia a la violencia física y al trabajo infantil. Es el momento de actuar”, finalizó la edil Luciana Ceresola