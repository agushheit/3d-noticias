---
category: Agenda Ciudadana
date: 2020-12-17T11:23:55Z
thumbnail: https://assets.3dnoticias.com.ar/1712-ELECCIONES1.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: NCN'
resumen: La UCR presentó un proyecto para que no se suspendan las PASO
title: La UCR presentó un proyecto para que no se suspendan las PASO
entradilla: 'Por su parte, el Frente de Todos sigue firme en su idea de suspender
  las elecciones Primarias Abiertas Simultáneas y Obligatorias (PASO) en el 2021. '

---
Desde el Frente de Todos alegan que sería «por única vez» y que esa excepción es «un gesto político» que haría la dirigencia en medio de una severa crisis económica y sanitaria que generó la llegada de la pandemia en Argentina. 

Sin embargo, desde la vereda opositora se niegan rotundamente a esa iniciativa, e incluso alegan que suspender las PASO es «atentar contra la democracia», razón por la cual, en las últimas horas diputados nacionales de la Unión Cívica Radical (UCR) presentaron un proyecto de Ley para que se prohíba su suspensión.

Las elecciones PASO son un tema del que se ha hablado en las últimas semanas debido al debate que generó en la dirigencia política cuando se supo que el oficialismo quiere suspender, solo por el 2021, las primarias.

Desde el oficialismo alegan que el gasto es «muy grande» y que no es prioridad en un contexto de pandemia como el de ahora -estiman que el gasto representa más de 13 millones de pesos-, por ende, con el apoyo de más de 20 gobernadores, el Frente de Todos introdujo al Congreso de la nación un proyecto el pasado viernes en el que solicita la suspensión de dichas elecciones.

«Es un planteo de los gobernadores y está fundado en dos cuestiones, que no se ha superado la pandemia en muchas provincias y que las PASO requieren un gasto de 13.000 millones de pesos», fue lo que dijo el Presidente de la nación respecto al proyecto de ley.

Ante esta iniciativa oficialista, los legisladores y principales figuras de la oposición, especialmente de la coalición Juntos por el Cambio, han mostrado su rechazo públicamente, y alegan que suspender las PASO es «atentar contra los procesos democráticos» y que más que pensar en la pandemia, «el FDT busca su propio beneficio».

En esa línea, en las últimas horas, legisladores de la Unión Cívica Radical (UCR) presentaron un proyecto de ley para que se prohíba la suspensión de las PASO.

Fue la diputada nacional radical Carla Carrizo, quien presentó el proyecto de prohibición de suspensión de las primarias PASO para elegir cargos nacionales e impulsa reducir el tiempo entre esos comicios y los generales, al proponer que la competencia interna dentro de un partido se realice el segundo domingo de septiembre.

![](https://assets.3dnoticias.com.ar/1712-elecciones2.jpg)

###### Diputada Carla Carrizo

<br/>

# **Qué dice la iniciativa opositora**

En la iniciativa se establece que las PASO rigen para seleccionar los cargos públicos electivos de todas las agrupaciones políticas y «como parte integrante del proceso electoral y del sistema electoral argentino no pueden ser suspendidas para ninguna categoría de cargos públicos electivos nacionales».

De esta manera, se suma una nueva iniciativa sobre el tema, ya que diputados del Frente de Todos que recogen pedidos de los mandatarios provinciales buscan suspender las elecciones primarias de agosto debido a la pandemia de coronavirus.

Además, Carrizo propone en su iniciativa que las elecciones primarias para definir los candidatos que se postularán en octubre se elijan el segundo domingo de septiembre, en lugar del segundo domingo de agosto como se establece en la ley sancionada en 2015.

A su vez, también propicia que los gastos totales de cada agrupación política para las elecciones primarias no puedan superar el 50% del límite de gastos de campaña para las elecciones generales.

En el caso de que un partido político o una alianza presentase lista única en las PASO, no podrá superar el 25% del límite de gastos de campaña para las elecciones generales, propone el proyecto.

También determina que si una lista no cumple con la paridad de género la Junta Electoral procederá a ordenarlo, «desplazando al o los candidatos de la corriente interna que tendría que ocupar el cargo de conformidad con dicho sistema de asignación e integrando a la lista al candidato/a que sigue de la misma corriente interna que cumpla la condiciones para cumplir con dicha cuota».

El proyecto redactado por Carrizo fue respaldado por sus pares de bloque Dolores Martínez; Emiliano Yacobitti, Ximena García, Lorena Matzen, Gabriela Lena; y Miguel Nanni.

![](https://assets.3dnoticias.com.ar/1712-elecciones3.jpg)

###### Diputado Alejandro Topo Rodríguez

<br/>

# **La opinión de los legisladores**

Sobre la opinión de los legisladores, el tema genera mucho debate y grietas: en el Frente de Todos el apoyo es total, en Juntos por el Cambio el rechazo es absoluto, y en otros bloques, aceptarían la iniciativa a cambio de negociaciones y consensos.

Uno de los últimos en opinar sobre el tema fue el diputado oficialista Pablo Yedlin, quien alegó que suspender las PASO es «un proyecto oportuno».

«El año que viene va a ser un año muy complejo y la vacunación no va a impedir la circulación viral, por eso no podemos tener casi todo el 2021 de campaña electoral, ya que con el armado de listas se empieza en los primeros meses del año», dijo, y añadió: «Las PASO se han transformado en una gran encuesta nacional. Muy pocas veces se han resuelto internas en las PASO, por lo que en un año muy particular se podrían definir las diferencias en una clásica interna partidaria, pero sin la movilización de personas y el gasto de recursos que genera una primaria abierta obligatoria».

Por su parte, desde Consenso Federal le hicieron un guiño a la iniciativa oficialista, pero con algunas condiciones.

Fue el diputado nacional Alejandro Topo Rodríguez quien alegó que «estaríamos dispuestos a acompañar un proyecto de suspensión de las primarias (PASO) a cambio de que se incorpore la boleta única de papel».

Por último, la diputada Jimena Latorre, también en días pasados en entrevista con NCN consideró respecto al tema:

«Las PASO son una herramienta reglamentada por Ley y no se debe bajo ninguna circunstancia modificar las reglas de juego a conveniencia de los gobernantes de turno».

Y añadió: «La justificación del gasto de las PASO es una excusa que ahora pone el oficialismo. Ellos mismos enviaron durante la pandemia la Reforma Judicial, que tiene un costo mucho más elevado, y no tuvieron ningún reparo en contemplar el gasto que generará».

  
 