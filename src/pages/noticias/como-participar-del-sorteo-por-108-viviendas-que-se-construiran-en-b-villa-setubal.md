---
category: La Ciudad
date: 2021-07-28T06:00:51-03:00
thumbnail: https://assets.3dnoticias.com.ar/BARRIOSETUBAL.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: "¿Cómo participar del sorteo por 108 viviendas que se construirán en Bº Villa
  Setúbal?"
title: "¿Cómo participar del sorteo por 108 viviendas que se construirán en Bº Villa
  Setúbal?"
entradilla: Serán de uno, dos y tres dormitorios; contarán con servicios y 78 cocheras.
  Los requisitos aquí.

---
Este martes, el gobierno anunció la construcción de 108 viviendas con infraestructura básica, desarrolladas en propiedad horizontal, para barrio Villa Setúbal de la ciudad de Santa Fe.

Las unidades generan una esperanza entre miles de santafesinos que todavía sueñan con tener la casa propia. Las viviendas serán de uno, dos, y tres dormitorios y contarán con servicios y 78 cocheras. La obra tiene un presupuesto oficial total de $1.003.835.904,71. Se dividirán en dos grupos de licitaciones que se realizarán el 23 y 31 de agosto de 2021, en el Estadio de la Universidad Tecnológica Nacional - Facultad Regional Santa Fe (UTN-FRSF).

En esos terrenos de Villa Setúbal, se construirá además un edificio para actividades de posgrado para la Universidad Tecnológica Nacional, que tendrá una inversión de más de 1.000 millones de pesos; se trata de de un proyecto anhelado hace décadas por la casa de altos estudios, donde se espera se desarrollen actividades de investigación en áreas referidas a las nuevas tecnologías de información y comunicación”.

**Inscripción**

Las familias interesadas en acceder a las viviendas deberán inscribirse en el Registro Digital de Acceso a la Vivienda ingresando al portal web del gobierno provincial: www.santafe.gob.ar/ruip. El trámite es online, gratuito, no requiere intermediarios y tiene carácter de declaración jurada.

Una vez finalizado el plazo de inscripción se publicará el padrón de las familias aptas para participar del sorteo en la página web oficial de la provincia. Es importante destacar que quienes ya se encuentren registrados en el sistema, deberán ingresar nuevamente para actualizar sus datos.

**Requisitos**

Para participar del sorteo de viviendas es necesario ser ciudadano argentino o extranjero naturalizado, no ser propietarios de inmuebles, ni haber sido beneficiarios de viviendas otorgadas por el gobierno provincial/municipal otorgado en los últimos 10 años.

Es obligatorio conformar un grupo familiar en el que al menos dos personas estén unidas en matrimonio o unión convivencial, ser padre o madre con hijos, hermanos o abuelos con nietos. Además, el titular y cotitular de la ficha de inscripción no debe revestir carácter de deudor alimentario y ni poseer causas por concurso o quiebra.

En el caso de parejas sin vínculo legal y sin hijos de la unión deberán cumplir con algunos de los siguientes requisitos: mantener en sus DNI el mismo domicilio con antigüedad de un año, poseer contrato de alquiler timbrado por el Banco a nombre de ambos o contar con la constancia de Unión Convivencial (Registro Civil). En todos los casos debe cumplirse una antigüedad convivencial mínima de 365 días.

Residir o trabajar, como mínimo, desde hace 2 años en la localidad en la que se concursa (la residencia o antigüedad laboral deberá ser acreditada con el domicilio del DNI, contrato de alquiler timbrado por el banco o recibos de sueldos).

Si se trata de un salario, jubilación o pensión, debe declararse el promedio mensual neto (de bolsillo) de los últimos seis meses.

Por último, los miembros en su conjunto, deben reunir un mínimo de ingresos mensuales y demostrables de $38.485 y un máximo de $140.000 (el equivalente a 6,5 sueldos mínimo vital y móvil). Serán considerados como ingresos válidos: salario, monotributo, autónomos, jubilación, pensión y Asignación Universal por Hijo, entre otros subsidios.

**Las obras**

El director de Vivienda y Urbanismo, José Kerz, brindó detalles de las obras: “Se realizarán dos licitaciones de 54 viviendas cada una, que contarán con infraestructura básica y con cocheras. Y se ejecutará la construcción del edificio para la UTN para desarrollar actividades de posgrado”.

Cada vivienda estará dotada de instalaciones y conexiones para los servicios públicos de agua potable, cloacas y energía eléctrica. Los desagües pluviales descargan a la vía pública.

Las 39 cocheras (módulos de estacionamiento) proyectadas en cada grupo licitatorio, se desarrollan en aproximadamente 13 metros cuadrados cada una de ellas.

Asimismo, se construirá un edificio destinado a la UTN-FRSF para el desarrollo de actividades de posgrado, destinadas al despliegue de nuevas tecnologías de información y comunicación (TICs), y estará ubicado en la manzana identificada como 5909 S, comprendida entre las calles Tacuarí, Vélez Sarsfield, Marco Sastre y Llerena. El edificio se desarrolla en planta baja y planta alta y posee una superficie cubierta de aproximadamente 3000 metros cuadrados