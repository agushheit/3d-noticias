---
category: Agenda Ciudadana
date: 2021-05-25T08:06:25-03:00
thumbnail: https://assets.3dnoticias.com.ar/pymes-dosjpg.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa CAME
resumen: La industria pyme se mantiene 17,1% debajo de 2019
title: La industria pyme se mantiene 17,1% debajo de 2019
entradilla: Tras 14 meses de pandemia no logra recuperar el índice previo

---
En la comparación interanual respecto de abril de 2019, no hubo ningún sector que haya fabricado más el mes pasado que dos años atrás. Y si bien en abril hubo un rebote de 76,9% anual comparado al 2020, resulta insuficiente para recuperar los niveles pre-pandémicos.

lEn abril 2020, la elaboración de las pymes industriales había caído 53,1% anual, con lo cual era esperable una recuperación fuerte en las tasas de variaciones, a pesar de que todavía el 55% de las empresas relevadas declaran que su situación es entre “regular” y “mala”.

lEsa apreciación se debe a que, comparando la producción de abril pasado con la del mismo mes de 2019, resulta 17,1% inferior.

lA pesar del rebote, fue un mes de mucha incertidumbre para las pymes, por la llegada de la segunda ola del covid-19. Desde mediados de abril, algunos pedidos comenzaron a frenarse y finalizó con una caída de 1,4% frente a marzo.

![](https://www.redcame.org.ar/advf/imagenes/2021/05/60aa4cd04f921_706x397.png)

lCon ese resultado, en el primer cuatrimestre del año la industria manufacturera pyme acumula un crecimiento de 14,8% frente al mismo periodo del año pasado, pero está 9,1% debajo del primer cuatrimestre de 2019.

lTodos los ramos relevados subieron en la comparación anual, observándose los mayores incrementos en **Productos electrónico, mecánicos e informática** (+262,1%), **Textiles** (+188,8%) y **Productos de metal, maquinaria y equipos** (+179,1%).

lLos datos surgen de la **Encuesta Mensual Industrial** de **CAME** entre 300 industrias pymes del país. El Índice de Producción Industrial Pyme (IPIP) se ubicó en 66,7 puntos en abril, frente a 67,6 en marzo y 37,7 en abril del año pasado.

l“**_Es muy difícil vender en estos momentos porque en cada entrega de insumos que nos hacen los proveedores, los precios vienen con aumentos promedio del 5%, 7%, 10% o a veces hasta el 15%, entonces vendes y no sabes a qué precio repones_**”, explicó un empresario del rubro químico de la ciudad de Neuquén.

![](https://www.redcame.org.ar/advf/imagenes/2021/05/60aa4cd05ab62_706x348.png)

A pesar de que los niveles de consumo interno vienen muy rezagados, las restricciones para importar están siendo de ayuda para el productor industrial, en un contexto donde el sector agropecuario viene creciendo fuerte y traccionando sobre otros vinculados como metalmecánica, material de transporte, productos de madera, cartón o maquinaria y equipo.

Si bien la producción sigue por debajo de 2019, el uso de la capacidad instalada se ubicó en 66,8% en abril, según el promedio declarado por las industrias medidas. El nivel fue bastante superior al de abril 2019 (57,5%). El 57% de las firmas relevadas trabajaron dicho mes con más del 70% de su capacidad instalada.

De todos modos, hay que tener en cuenta que en el último año no pocas empresas redujeron sus instalaciones, ya sea porque debieron achicar sus negocios, porque la maquinaria en mal estado no fue reparada, o porque vendieron capital, entre otras cosas. Un caso es **Calzado y marroquinería,** que en promedio trabajó con el 70% de su capacidad instalada, a pesar que todavía su producción es 40% inferior a la de abril 2019.

![](https://www.redcame.org.ar/advf/imagenes/2021/05/60aa4cd0692f6_706x356.png)

**_“Los insumos importados están viniendo con menor calidad”_**, dijo un productor de maderas y muebles de la ciudad de Catamarca; **_“No podemos planificar entregas porque el ingreso de las importaciones de los insumos que necesitamos son imprevisibles, no sabemos cuándo llegan”_**, fue el comentario de un industrial de electrodomésticos de Mendoza; **_“Estamos teniendo demoras de hasta 90 días para recibir insumos”_**, observó un fabricante de mosaicos de la zona oeste del Gran Buenos Aires.

![](https://www.redcame.org.ar/advf/imagenes/2021/05/60aa4ce3daf51_706x354.png)

En abril, la elaboración en todas las ramas industriales pymes encuestadas estuvo por debajo de 2019. La menor diferencia ocurrió en: **Alimentos**, que creció 13,8% anual en abril, pero se ubicó 4,1% por debajo de 2019; **Químicos**, que aumentó 33% se mantuvo 10,9% debajo de abril 2019; y **Papel cartón, edición e impresión**, que prosperó 44,2%, pero mantuvo una brecha de 15,2% contra abril 2019.

En el otro extremo, hay cinco sectores que, a pesar del fuerte rebote anual de abril, aún mantienen una brecha alta frente a abril 2019. Además del caso mencionado antes de **Calzado y marroquinería,** están: **Textil**, que se mantiene 23,5% debajo de 2019, o **Material de transporte**, con una distancia de 21,6%.

**Datos relevantes de la Encuesta Cualitativa**

* El **85%** de las industrias pymes consultadas tuvieron incrementos anuales en su producción, el **7,9%** se mantuvo igual y el **7,1%** tuvo caídas.
* En cuanto a la rentabilidad, el **48%** declaró que fue positiva en abril, frente al 47% de marzo pasado y el 10% de abril 2020.
* El **61%** de las empresas están teniendo demoras por encima de lo normal en recibir mercadería para completar su proceso de producción. El plazo promedio de esas demoras es de **33 días**, promediando los **44 días** en el sector **Caucho y plástico**.
* El **38%** de las pymes recibe insumos con aumentos de precios en cada entrega, con un alza promedio de **11,3%**.

**Notas Metodológicas**

**Consideraciones Generales**

El Índice de Producción Industrial PYME (IPIP) mide el desempeño mensual en la producción manufacturera de las pequeñas y medianas industrias (PYMIS) argentinas. La información se obtiene durante los primeros 20 días del mes en base a encuestas directas realizadas entre 300 Pymes industriales del país. Releva todos los meses un equipo de 30 encuestadores localizados en las ciudades capitales, Ciudad de Buenos Aires y Gran Buenos Aires (Zona sur, zona norte y zona oeste). A su vez, un equipo de 6 supervisores desde CAME controlan la calidad de la información recolectada y coordinan el equipo de encuestadores.

Las empresas relevadas han sido seleccionadas en función de tres variables:

1. Tipo de producto elaborado por la empresa: se determinaron productos que reflejen de manera más fehaciente el nivel de actividad del sector, ya sea en forma directa como indicador representativo de la producción, o en forma indirecta por constituir el insumo principal de otro sector.
2. Localización geográfica de la empresa: se seleccionaron empresas localizadas en regiones con predominancia en la producción de los productos pre-seleccionados.
3. Calidad de la información: se relevan empresas donde pudo comprobarse la calidad y precisión de los datos aportados.

El IPIP está dividido en 11 sub ramas industriales. Para determinar el valor del IPIP se elaboran números índices de cada una de esas ramas, que ponderadas por la participación de cada sector en la actividad, determinan el índice general. La variación anual en la producción surge al comparar ese índice con el valor del mismo mes del año inmediatamente anterior.

El índice general refleja la evolución de la producción industrial en términos de volumen físico. Los números índices resultan útiles para homogeneizar a las variables, facilitando su comparación a lo largo del tiempo. El año base de la serie se estableció en enero de 2008.

**Consideraciones Particulares**

El IPIP está dividido en 11 sub ramas industriales. Para determinar el valor del IPIP se elaboran números índices de cada una de esas ramas y del nivel general que reflejan la evolución de la producción industrial en términos de volumen físico. Los números índices resultan útiles para homogeneizar a las variables, facilitando su comparación a lo largo del tiempo. El año base de la serie se estableció en diciembre de 2008.

La ponderación de cada una de las ramas industriales se realizó en base al Censo Económico 2004 y al Mapa PYME elaborado por la Sepyme. La ponderación asignada a cada sector se puede leer en la siguiente tabla:

![](https://www.redcame.org.ar/advf/imagenes/2021/05/60aa4ce3ea5c2_706x404.png)