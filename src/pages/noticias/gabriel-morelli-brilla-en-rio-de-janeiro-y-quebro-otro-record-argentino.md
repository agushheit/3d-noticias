---
category: Deportes
date: 2020-12-13T14:09:30Z
thumbnail: https://assets.3dnoticias.com.ar/natación.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: FUENTE: DIEZ EN DEPORTES - DEPORTES ARGENTINA
resumen: Gabriel Morelli brilla en Rio de Janeiro y quebró otro récord argentino
title: Gabriel Morelli brilla en Rio de Janeiro y quebró otro récord argentino
entradilla: Gran trabajo nacional en la tercera jornada del Troféu Brasil que se realiza
  en el natatorio del Club Vasco Da Gama, donde el sanjustino volvió a dar la nota,
  al igual que la santafesina Julia Sebastián.

---
Por la tercera fecha del Campeonato Brasilero de natación, que se disputa hasta este sábado en las instalaciones del club Vasco Da Gama, en Río de Janeiro, los argentinos volvieron a destacarse con dos medallas doradas, una plateada y tres bronces. Los santafesinos Julia Sebastián y Gabriel Morelli, quien bajó su propio récord argentino, se quedaron con los 200 metros pecho.

Bajando su marca de los Juegos Panamericanos de Lima 2019, Morelli obtuvo el primer puesto en los 200 pecho con un tiempo de 2'12"56/100. Así, el representante de Unión, baja su propio récord que había establecido en los Juegos Panamericanos de Lima 2019 (2'13"83/100).

Tras su buen paso por la Liga Internacional de Natación, Julia Sebastián, representante de Minas Tenis Clube, se subió a lo más alto del podio con una marca de 2'28"05/100, en tanto Macarena Ceballos fue tercera con 2'29"58/100.

En los 100 metros libre, Andrea Berrino terminó segunda con un tiempo de 56"07/100 y Federico Grabich fue tercero con 49"48/100. Por su parte, Virginia Bardach finalizó en el tercer lugar de los 200 metros combinados con 2'18"20/100.