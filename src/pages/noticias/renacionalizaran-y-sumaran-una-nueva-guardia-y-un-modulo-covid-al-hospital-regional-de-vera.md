---
category: Estado Real
date: 2021-06-21T06:00:40-03:00
thumbnail: https://assets.3dnoticias.com.ar/VERA.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Renacionalizarán y sumarán una nueva guardia y un módulo Covid al hospital
  regional de Vera
title: Renacionalizarán y sumarán una nueva guardia y un módulo Covid al hospital
  regional de Vera
entradilla: Las mejoras incluyen 21 camas en la UTI, una nueva guardia y un sector
  independiente para atención de pacientes Covid 19 con capacidad de 12 camas. El
  presupuesto oficial supera los 244 millones de pesos.

---
El Ministerio de Infraestructura, Servicios Públicos y Hábitat, a través de la Secretaría de Arquitectura y Obras Públicas, abrirá los sobres para ejecutar la obra de “Construcción de nueva guardia, refuncionalización UTI/UCI clínica e instalación de nuevo módulo Covid 19” en el Hospital Regional de Vera, con un plazo de ejecución de 270 días y un presupuesto oficial de $ 244.993.390,80, con base al mes de febrero 2021.

El concurso de precios se realizará el miércoles 23 de junio a las 15 horas en el Hospital Regional de la localidad.

Al respecto, la ministra Silvina Frana habló sobre la estrategia de equilibrar los territorios a través de la obra pública: “Entre los principales objetivos que trazó la gestión del gobernador Omar Perotti está la de generar arraigo en cada una de las localidades, para que los santafesinos crezcan y se desarrollen en el lugar donde viven, y eso se logra mejorando la infraestructura, la salud, la educación y optimizando las condiciones de producción y empleo. Por esto estamos haciendo inversiones en el centro-norte provincial, para brindar condiciones de vida más dignas y oportunidades concretas de crecimiento”.

Y agregó: “En este proceso de equilibrar, la consigna es ejecutar pequeñas o medianas obras que mejoren la vida cotidiana de la gente, haciendo intervenciones importantes como estas, en hospitales y centros de salud en diferentes localidades”, indicó.

Las obras están divididas en cuatro sectores de actuación: la sala modular Covid 19; las tareas en exteriores y generales del nosocomio; la refuncionalización de uno de los pabellones de internación, y la construcción de una nueva guardia.

**MÓDULO COVID 19**

Se incorpora un área específica para atención de pacientes Covid 19, que incluirá un área semi cubierta para la llegada de ambulancias, una sala para 12 camas con todos los servicios, baños para pacientes y personal, office sucio y limpio, estar y vestuario para personal.

**NUEVA GUARDIA**

Este sector incluirá un espacio semi cubierto para la llegada de ambulancias, una sala de espera pública que se relaciona con la plazoleta de juegos infantiles, una oficina de admisión, baños, dos consultorios médicos, un consultorio de enfermería con baño, una sala de observaciones con tres camas con baño y shock room con su propia sala de enfermería.

**MÁS CAMAS**

La refuncionalización del sector de internación permitirá la construcción de una UTI con 17 camas y una UCI con 4 camas, de las cuales podrán destinarse dos a “pacientes aislados”. Asimismo, todas las camas, contarán con poliductos con todos los servicios de gases medicinales, lo cual permite total flexibilidad de uso.

Además, se contempla la reubicación de la sala de Rayos X, la construcción de un dormitorio y sala de estar para médicos y una morgue.

**EXTERIORES Y GENERALES**

Se dispondrá de un nuevo sector de playón de máquinas e infraestructura, donde se ubicará una sala de gases que abastecerá a todo el hospital con aire comprimido, vacío y oxígeno. También se colocarán equipos de acondicionamiento de aire para la guardia, la UTI / UCI y el nuevo módulo Covid 19.

Además, en este sector, se contempla un nuevo acceso para ambulancias, estacionamiento, veredas exteriores, garita de vigilancia, parquización, iluminación y plazoleta de juegos infantiles.