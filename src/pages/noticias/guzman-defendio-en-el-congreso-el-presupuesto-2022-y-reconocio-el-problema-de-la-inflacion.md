---
category: Agenda Ciudadana
date: 2021-12-14T06:15:21-03:00
thumbnail: https://assets.3dnoticias.com.ar/guzman.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Guzmán defendió en el Congreso el presupuesto 2022 y reconoció el problema
  de la inflación
title: Guzmán defendió en el Congreso el presupuesto 2022 y reconoció el problema
  de la inflación
entradilla: |2-

  El ministro de Economía defendió la ley de leyes ante la comisión de Presupuesto y Hacienda. Reconoció que el principal problema del país en el año lo marcó "la dinámica inflacionaria".

---
El ministro de Economía, Martín Guzmán, defendió este lunes el presupuesto 2022 ante la comisión de Presupuesto y Hacienda de la cámara de Diputados. Allí reconoció que el principal problema durante 2021 en el país estuvo marcado por "la dinámica inflacionaria". Además indicó que esto "le da continuidad a la visión que desde el Gobierno nacional se plantea para propiciar la recuperación de una doble crisis: económica y sanitaria".

Al explicar los alcances del proyecto que el oficialismo aspira a debatir el jueves próximo en el recinto de la Cámara baja, Guzmán dijo que el proyecto de Ley de Presupuesto para 2022 se aplica en un marco en el que, según el ministro, se sigue "pugnando contra una doble crisis, la que comenzó en el 2018 y la que le siguió con la pandemia”.

En el marco de su presentación, el ministro sostuvo que "el problema principal que se ha enfrentado la Argentina en 2021 ha sido la dinámica inflacionaria y es un objetivo de la economía atacar". Guzmán agregó que "no ha habido caída de reservas internacionales" y puso de relieve que "para el 2022 y en adelante la acumulación de reservas también es un objetivo" del Gobierno nacional.

“Hoy Argentina está viviendo un fuerte proceso de recuperación económica, de manera sólida, que permitirá que el PBI crezca el 10%. En los tres meses desde que enviamos el proyecto a hoy, hubo un fortalecimiento en ese crecimiento”, aseguró el ministro.

Guzman brindó su exposición ante la comisión de Presupuesto y Hacienda, que preside Carlos Heller. Allí sostuvo, además, que “el proyecto busca satisfacer cinco objetivos, la inclusión social, dinamismo productivo, estabilidad macroeconómica, federalismo y soberanía”.

"Desde el Gobierno nacional -señaló en ese sentido- esperamos continuar trabajando junto a todos los representantes del pueblo y de las provincias, y a todo el Congreso de la Nación para fortalecer un sendero de desarrollo y estabilidad". También señaló que la inversión en 2021 fue “superior al 30%”. y destacó que "estamos en un sendero de ordenamiento de las cuentas fiscales que consideramos virtuoso”.

El ministro precisó que hay "una fuerte reducción del déficit primario del sector público nacional no financiero", que descendió este año del 6,4 al 3,5 por ciento. El titular del Palacio de Hacienda sostuvo, además, que “el proyecto busca satisfacer cinco objetivos, la inclusión social, dinamismo productivo, estabilidad macroeconómica, federalismo y soberanía”.

Además, el funcionario dijo que este plan de gastos y recursos apunta a “la continuidad de la recuperación en el corto plazo” y a generar “condiciones para que el crecimiento y desarrollo económico de Argentina se pueda sostener en plazos más largos”.

En otro tramo de su alocución, Guzmán destacó “el fortalecimiento del mercado de deuda pública en pesos" ya que era sustancial que "el sector público tuviese la capacidad de financiarse en su propia moneda” , al tiempo destacó que se ha "fortalecido el mercado de capitales"

Sobre las exportaciones, el ministro dijo que tuvieron un crecimiento lo cual "es muy positivo para la Argentina" ya que para tener "un crecimiento económico se necesito un aumento sostenido de las exportaciones netas". El proyecto de Presupuesto 2022 prevé un crecimiento del 4% del PBI, una inflación del 33% y un dólar a $131,1 para el año próximo. Cabe destacar que la iniciativa no contempla el pago al Fondo Monetario Internacional.