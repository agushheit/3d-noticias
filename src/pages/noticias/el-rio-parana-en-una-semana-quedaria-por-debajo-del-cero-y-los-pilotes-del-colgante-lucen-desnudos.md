---
category: La Ciudad
date: 2021-07-16T06:00:38-03:00
thumbnail: https://assets.3dnoticias.com.ar/BAJANTE.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: El río Paraná, en una semana, quedaría por debajo del cero y los pilotes
  del Colgante lucen desnudos
title: El río Paraná, en una semana, quedaría por debajo del cero y los pilotes del
  Colgante lucen desnudos
entradilla: En 24 horas, el río bajó 11 centímetros en el puerto local y dejó una
  marca de de 0,14 metros, como lo habían anticipado todos los pronósticos.

---
Los pilotes del Puente Colgante, referencia de muchos santafesinos para conocer la altura en la que se encuentra el río, lucen como nunca antes y se alcanzan a ver casi completos. Al lado de uno de ellos, aparecieron grandes montículos de roca y piedra, algo que no había ocurrido hasta el momento. A su lado, el viaducto Oroño no se queda atrás y muestra sus pilotes al descubierto.

Este jueves al mediodía, el Paraná volvió a bajar. En apenas 24 horas, Prefectura Naval Argentina registró un descenso de 11 centímetros. Midió este mediodía 0,14 metros, contra los 0,25 metros del miércoles.

Las nuevas previsiones del Instituto Nacional del agua no son alentadoras. El organismo no descarta que para el 20 de julio el Paraná, a la altura de Santa Fe, llegue a tener un registro negativo de -0,05 metros.

El instituto añade que "la tendencia descendente observada en los niveles va a predominar en los próximos tres meses. Este mes es especialmente crítico, con afectación a todos los usos del recurso hídrico, exigiendo especialmente a la captación de agua fluvial para consumo urbano".

"Esta situación se refleja en todos los puertos del Paraná y también en el río Paraguay. El río Iguazú, al igual que en 2020, presenta un aporte del orden de la séptima parte del valor normal", informó.

Además, advierte que "su tendencia bajante y alturas por debajo del nivel límite de aguas bajas" van a continuar. "No se espera una mejora sensible en los próximos meses. Julio será particularmente crítico, con afectación a todos los usos del recurso hídrico, especialmente la captación de agua en las tomas urbanas", detalló el instituto.

En esa línea, subrayó que "con la tendencia prevista, todo el tramo del río Paraná en territorio argentino alcanzaría niveles similares o peores que los registrados en 1944, el año más bajo del río en la historia desde 1884”.