---
category: Estado Real
date: 2021-04-09T06:28:56-03:00
thumbnail: https://assets.3dnoticias.com.ar/mochila-pedagogica.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: 'Cantero: “Con Mochila Pedagógica queremos saldar algunas deudas sociales
  que tenemos”'
title: 'Cantero: “Con Mochila Pedagógica queremos saldar algunas deudas sociales que
  tenemos”'
entradilla: El ministerio de Educación presentó la “Mochila Pedagógica” programa destinado
  a la educación en contextos de privación de la libertad.

---
El ministerio de Educación lanzó este jueves, en un acto que se desarrolló en “El Molino Fábrica Cultural” de la ciudad de Santa Fe, el proyecto “Mochila Pedagógica” destinada a todas y todos los docentes y alumnos de las 31 instituciones educativas que funcionan en unidades penitenciarias provinciales.

La iniciativa surgió de la dirección provincial de Educación en Contextos de Privación de la Libertad, área nueva en el organigrama del ministerio y la única a nivel nacional que cuenta con el rango de dirección provincial poniendo de manifiesto la importancia que le otorga la gestión, a la educación destinada a los y las alumnas de la modalidad.

En la oportunidad, la ministra de Educación, Adriana Cantero, precisó los alcances del proyecto manifestando que: “La mochila está destinada especialmente a todos los estudiantes que transitan las aulas de las unidades penitenciarias, dependientes de nuestra dirección provincial de Educación en Contextos de Privación de la Libertad y estamos de alguna manera también presentando esa dirección provincial con este planteo que tenemos desde la educación santafesina de ir trabajando en hacer visible lo que muchas veces la sociedad invisibiliza”.

En este contexto, Cantero agregó que: “Las estadísticas indican que la mayoría de los jóvenes que han transitado el camino triste y complejo del delito y que están privados de la libertad han sido estudiantes con trayectorias incompletas y han quedado marginados del sistema educativo por lo tanto pensamos también en lo importante que es educar para la vida en libertad y para poder habilitar esa reinserción social es esencial lograr saldar la deuda del déficit educativo”.

Para finalizar, la ministra de Educación se refirió a la necesidad de actuar bajo protocolos en el marco de la pandemia y expresó lo siguiente: “En estos contextos de alto nivel de cuidado debemos también saber trabajar aquí con los protocolos y por ello estamos poniendo de relieve cuál es el aporte que hace el sistema educativo a la garantía de una convivencia social diferente, y nosotros pensamos que cuanto más educación podamos sembrar entre nuestra gente esa convivencia seguramente podrá ser mejor y en la población de las unidades penitenciarias seguramente podríamos haber fortalecido la capacidad de realización”.

En tanto, el director provincial de Educación en Contextos de Privación de la Libertad, Matías Solmi, reseñó el dispositivo presentado precisando que: “La Mochila Pedagógica es un proyecto de la dirección provincial de Educación en Contextos de Privación que tiene útiles escolares y el libro “Ladrilleros” de la escritora entrerriana Selva Almada, el cual relata historias de sujetos populares del litoral argentino”.

Asimismo, Solmi agregó: “Nosotros creemos importante poder trabajar este libro junto con esta mochila que va a ser entregada a docentes y estudiantes porque son acciones que hacen foco en los ejes centrales de la política educativa de la gestión, educar para la libertad y educar pensando la dimensión de un sujeto político como lo son las y los estudiantes en contexto de encierro”.

En el acto también estuvieron presentes el ministro de Gobierno Roberto Sukerman, la secretaria de Gestión Territorial Educativa, Rosario Cristiani; el secretario de Asuntos Penitenciarios, Walter Gálvez, el subsecretario del Servicio Penitenciario, Jorge Bortolozzi y el director provincial de Justicia Penal Juvenil, Federico Lombardi.

**Mochila Pedagógica**

Los equipos de gestión, pedagógicos y técnicos de la Dirección Provincial de Educación en Contextos de Privación de la Libertad diseñaron una mochila pedagógica que contiene útiles escolares y un material central que es una propuesta pedagógica para trabajar en el 2021 acompañado de un libro de la autora Selva Almada llamado “Ladrilleros” con diferentes versiones en tipografía y letra según los grados de alfabetización de las internas y los internos.

La propuesta también está pensada para poder sostener las clases en caso de un cierre de las unidades penitenciarias o acceso restringido de docentes respecto a la pandemia Covid-19.