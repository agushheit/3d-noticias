---
category: La Ciudad
date: 2021-11-26T06:15:07-03:00
thumbnail: https://assets.3dnoticias.com.ar/BONDI.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: UTA levantó el paro de colectivos y habrá servicios
title: UTA levantó el paro de colectivos y habrá servicios
entradilla: 'Así lo informó el gremio UTA este jueves por la tarde a través de un
  comunicado, por lo que habrá colectivos urbanos y media distancia este viernes

'

---
Cuando parecía que las partes no acordaban, este jueves por la tarde se dieron charlas importantes y el gremio de UTA (Unión Tranviarios Automotor) determinó levantar el paro de colectivos de 24 horas previsto para este viernes.

De esta forma, los servicios de transporte urbano y de media distancia serán normales. Así lo dio a conocer el sindicato de choferes a través de un comunicado. Una noticia tranquilizara para la gente, que en muchos casos debieron hacer malabares u organizarse de otra manera para asistir a sus diferentes compromisos.

En primera instancia, estaba establecido un paro desde las cero de este viernes, hasta ver si se podían acomodar las cosas, pero los diálogos se enfatizaron y por lo menos se acordó que se no se interrumpa el servicio, tan esencial para la vida de los santafesinos.

De todas maneras, todavía quedan varios detalles por resolver. Por lo pronto, son noticias trascendentales para los ciudadanos.