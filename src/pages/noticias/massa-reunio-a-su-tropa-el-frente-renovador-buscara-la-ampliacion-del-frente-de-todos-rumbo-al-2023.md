---
category: Agenda Ciudadana
date: 2021-12-05T06:15:06-03:00
thumbnail: https://assets.3dnoticias.com.ar/frenterenovador.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: 'Massa reunió a su tropa: el Frente Renovador buscará "la ampliación del
  Frente de Todos rumbo al 2023"'
title: 'Massa reunió a su tropa: el Frente Renovador buscará "la ampliación del Frente
  de Todos rumbo al 2023"'
entradilla: 'Apuntan a partidos vecinalistas y espacios políticos más chicos, pero
  además a ir a buscar a cámaras de comercio, sociedades rurales y colegios profesionales
  para involucrarlos.

'

---
El referente del Frente Renovador, Sergio Massa, encabezó un encuentro junto a su dirigentes bonaerenses del espacio político con el objetivo de  
impulsar una mesa política para "preservar el equilibrio" del Frente de Todos y "preparar el camino de ampliación" de la coalición oficialista de cara a las elecciones de 2023.

Así lo indicaron a NA fuentes partidarias, que precisaron que el pedido de ampliar el espacio apunta a "partidos vecinalistas y espacios políticos más chicos, pero además a ir a buscar a cámaras de comercio, sociedades rurales y colegios profesionales para involucrarlos".

"Vamos a la institucionalización del Frente de Todos (FdT)", arengó el tigrense frente a intendentes, legisladores bonaerenses y diputados nacionales del Frente Renovador.

Según indicaron desde el massismo, la institucionalización llegaría una vez que se concrete la formación de "un espacio de toma de decisiones políticas e institucionales donde estén todos" los partidos que integran la coalición gobernante.

De esta manera, se buscaría un método similar al que empleó el Frente Amplio uruguayo durante sus gestiones a la cabeza del Ejecutivo nacional, donde la mesa política de la fuerza trataba cuestiones partidarias y asuntos de la agenda de gobierno.

El encuentro, que se desarrolló en el partido bonaerense de San Fernando, sirvió para "dialogar y revalidar posiciones en relación a temas centrales de la agenda a futuro, tanto en la provincia de Buenos Aires como en el país".

Además, el presidente de la Cámara de Diputados y accionista de la coalición gobernante pidió a su tropa "apoyar al Gobierno en la negociación con el FMI".

En tanto, la Mesa Ejecutiva Provincial del massismo anticipó que va a convocar a un Congreso Nacional del Frente Renovador para el mes de febrero.

En ese marco, coincidieron en que "las políticas educativas, de generación de empleo e inversión y seguridad deben ser prioritarias, como también la necesidad de sumar la temática medioambiental".

El encuentro contó con la presencia de la presidenta de AySA, Malena Galmarini; del presidente del Banco de Inversión y Comercio exterior (BICE), José Ignacio de Mendiguren; y del presidente de ENACOM, Ambrosini Claudio.

También participaron los intendentes Juan Andreotti (San Fernando), Blanca Cantero (Pte. Perón), Sebastián Ianantuony (Gral. Alvarado), Freddy Zavatarelli (Gral. Pinto), Carlos Puglielli (San Andrés de Giles), Javier Osuna (Gral. Las Heras), Alberto Gelené (Las Flores), Carlos Bevilacqua (Villarino) y Javier Gastón (Chascomús), y el exjefe comunal Luis Andreotti.

Entre los presentes se encontraron los diputados nacionales Ramiro Gutiérrez, Carlos Selva, Cecilia Moreau, Mirta Tundis, Daniel Arroyo, Jimena López, Marcela Passo, Mónica Litza y Alicia Aparicio.

Además, participaron los diputados provinciales Eslaiman Rubén, Galán Débor, Morán Micaela, D’Onofrio Jorge, Russo Nicolás, Mignaquy Javier, Di Césare Germán, Faroni Marcela, Bevilacqua Fernanda, Garate Pablo, Padulo Luciana, Puglielli Carlos, Malpelli Tachu, Rasquetti Ayelén; y los senadores bonaerenses "Juanci" Martínez, José Luis Pallares, Sofía Vanelli, entre otros.