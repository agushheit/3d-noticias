---
category: Agenda Ciudadana
date: 2022-12-07T09:31:20-03:00
thumbnail: https://assets.3dnoticias.com.ar/municipales.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: Servicios municipales para los próximos feriados
title: Servicios municipales para los próximos feriados
entradilla: La Municipalidad informa los horarios de los servicios de recolección
  de basura, del Cementerio, el transporte, y el Mercado Norte, entre otros que se
  brindarán este jueves y viernes

---
La Municipalidad informa los servicios que se prestarán durante el próximo jueves 8 de diciembre, Día de la Inmaculada Concepción de la Virgen María, y viernes 9 de diciembre, feriado nacional con fines turísticos.

**Ambiente**

En ese sentido, se indicó que la recolección de basura y el barrido de calles serán prestados con normalidad. En tanto, los EcoPuntos permanecerán cerrados tanto el jueves como el viernes.

Respecto de las sedes del IMUSA, permanecerán cerradas ambos días.

**Colectivos**

Por otra parte, el transporte urbano de pasajeros por colectivo tendrá frecuencias similares a los días domingo.

**Cementerio Municipal**

En cuanto al Cementerio Municipal, se informa que los horarios de visita serán de 7.30 a 12.30 y de 15 a 18. Mientras tanto, las inhumaciones serán de 7.15 a 11.30.

**Corralón y Tribunal de Faltas**

El Corralón Municipal contará con guardias mínimas, al igual que el Tribunal de Faltas (San Luis 3078), que permanecerá abierto, tanto el jueves como el viernes, de 8 a 13.

**Mercado Norte**

Finalmente, cabe señalar que el tradicional Mercado Norte, ubicado en Urquiza y Santiago del Estero, abrirá el jueves y viernes, de 9 a 13.