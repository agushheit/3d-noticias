---
category: El Campo
date: 2021-01-11T10:48:46Z
thumbnail: https://assets.3dnoticias.com.ar/110121-paro-campo.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: '3DNoticias - Fuente: El Litoral'
resumen: La Mesa de Enlace ratificó el paro que inicia este lunes
title: La Mesa de Enlace ratificó el paro  que inicia este lunes
entradilla: Habrá un cese de comercialización de granos por 72 horas y los productores
  se manifestarán en las rutas.

---
La Comisión de Enlace de Entidades Agropecuarias (CEEA), más conocida como «Mesa de Enlace», **confirmó este domingo el paro del campo a partir de la medianoche**, en protesta por el cierre del registro de exportaciones de maíz que el gobierno dispuso el 30 de diciembre y hasta el 1 de marzo.

La medida fue ratificada por tres de las cuatro entidades que componen ese núcleo de representación rural: Confederaciones Rurales Argentinas, Sociedad Rural Argentina y Federación Agraria Argentina.

Coninagro, la cuarta entidad, no apoyará en esta instancia el paro de actividades, pero envió al ministro de Agricultura, Luis Basterra, una carta en la que, luego de reconocer que no considera este el momento apropiado para un paro, critica las medidas recientes del gobierno.

El comunicado rural señala: «los integrantes de la CEEA venimos trabajando hace muchos años en los consensos de nuestras entidades, pero también supimos respetar las diferencias que pudieran surgir en la toma de decisiones para manifestar el descontento hacia medidas desfavorables para el sector agropecuario. **Es por eso que CRA, FAA y la SRA ratifican el cese de comercialización de todos los granos desde las 0:00 horas del lunes 11 de enero hasta las 24 horas del miércoles 13 de enero**».

El comunicado apela a la «responsabilidad y solidaridad de los productores para que la medida sea un llamado de atención, que se conozca el malestar que existe en el campo producto de una suma de decisiones del Gobierno que se vienen acumulando desde el año pasado y que deterioran el potencial productivo del campo».

El objetivo del cese de comercialización, señalan las entidades firmantes, «no es perjudicar a nadie, ni puede ocasionar desabastecimiento de granos o aumento en los precios. **Queremos que se visualice que el cierre de exportaciones del maíz y las otras decisiones desfavorables que afectan la producción del campo argentino, también deterioran el futuro del país**».

La ratificación de la medida estaba cantada ante la falta de una respuesta concreta del gobierno a las reuniones que en la semana mantuvieron representantes del campo con el ministro de Agricultura, Luis Basterra, y la fuerte presión de las bases rurales, que se manifestaron en numerosas asambleas a lo largo de este fin de semana. 

El sábado hubo reunión de productores en el cruce de las rutas A12 Y Ruta 34, donde se ratificó la voluntad de parar y manifestarse en las rutas, pero sin cortar el tránsito por las mismas. «El gobierno es solidario con el bolsillo ajeno, es hora de que lo haga con el propio, que se ajuste», dijo un comunicado al cabo del encuentro, que contó con la presencia de representantes de la SRA, Confederación de Asociaciones Rurales de la Provincia de Santa Fe, la Sociedad Rural de Rosario y la Cámara de Comercio de Funes.

También hubo Asambleas en Armstrong, Santa Fe, en el cruce de las Rutas 9 y 178, y este domingo una Asamblea ciudadana en Crespo, Entre Ríos. En el noreste de Santa Fe, en Reconquista y Avellaneda, se instaló además una autodenominada «Carpa Republicana» que se contactó de modo virtual con una Asamblea Multisectorial en Las Breñas, provincia de Chaco.

Ya este sábado, el presidente de la Confederación de Asociaciones Rurales de Buenos Aires y La Pampa (Carbap, el brazo más fuerte de CRA), Matías de Velazco, había enviado a los presidentes de las rurales una nota precisando que el cese de comercialización de granos comenzaría (como se ratificó hoy) a las 0 horas del y se extendería hasta las 24 del miércoles 13, en tanto siguiera vigente el cierre del registro para exportar maíz.