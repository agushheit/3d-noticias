---
category: Agenda Ciudadana
date: 2020-12-11T16:24:11Z
thumbnail: https://assets.3dnoticias.com.ar/traferri-000.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Diario UNO'
resumen: El senador Armando Traferri se presentó a prestar declaración por la causa
  juego clandestino
title: El senador Armando Traferri se presentó a prestar declaración por la causa
  juego clandestino
entradilla: El senador provincial llegó al Centro de Justicia Penal acompañado por
  su abogado. Prefirió no hacer declaraciones, pero dijo que se sentía "tranquilo".

---
El senador Armando Traferri se presentó esta mañana en el Centro de Justicia Penal para prestar declaración informativa ante los fiscales que investigan una red de juego clandestino que tiene a dos exfiscales imputados y detenidos Patricio Serjal y Gustavo Ponce Asahad.

Acompañado por su abogado defensor, el legislador oriundo de San Lorenzo ingresó al edificio por la puerta principal del tribunal y se dirigió por escaleras hasta el segundo piso donde funcionan las oficiales de la Fiscalía.

Al ser abordado por los periodistas, Traferri se negó a hacer declaraciones. Solo respondió con un lacónico “Sí, por qué no?” cuando le preguntaron si se sentía tranquilo.

El senador por el departamento San Lorenzo ya había adelantado que se presentaría este viernes ante los fiscales Matías Edery y Luis Schiappa Pietra luego de que el exfiscal Ponce Asahad, preso e imputado por recibir coimas, lo vinculara a una organización de juego clandestino.

El abogado defensor del legislador aclaró que su cliente se presentaba para una declaración informativa y que “no está imputado de ningún delito” por parte de los fiscales.