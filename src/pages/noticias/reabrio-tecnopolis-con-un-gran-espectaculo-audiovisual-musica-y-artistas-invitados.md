---
category: Agenda Ciudadana
date: 2021-10-03T06:15:58-03:00
thumbnail: https://assets.3dnoticias.com.ar/TECNOPOLIS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Reabrió Tecnópolis con un gran espectáculo audiovisual, música y artistas
  invitados
title: Reabrió Tecnópolis con un gran espectáculo audiovisual, música y artistas invitados
entradilla: 'La megamuestra se encuentra abierta al público desde este sábado y hasta
  diciembre, en su décima edición. '

---
Miles de personas, en su mayoría familias, asistieron a la reapertura de Tecnópolis y, aún con lluvia, pudieron disfrutar de todas las actividades que ofrece en su décima edición la megamuestra ubicada en la localidad bonaerense de Villa Martelli, lindera a la Capital Federal.

Tecnópolis, que reúne exposiciones gratuitas de ciencia, tecnología y cultura permanecerá abierta al público hasta 12 de diciembre, los viernes, sábados, domingos y feriados de 12 a 20 y con reserva previa.

Durante el acto formal de inauguración, el jefe de Gabinete de Ministros, Juan Manzur, aseguró que "en plena crisis y plena pandemia vamos a cuadriplicar las inversiones en ciencia y tecnología" y dijo que "estamos transitando quizás la última etapa de esta tragedia que nos tocó vivir, la pandemia".

Acróbatas, zanquistas y malabaristas recibieron pasadas las 11 a las primeras familias que ingresaron por el Polo Industria y Ciencia Nacional, donde estuvo ubicado el avión de Aerolíneas Argentinas, que fue una las atracciones más visitadas y el dinosaurio gigante de cuello largo que con el movimiento de su cola impactó a los nuevos visitantes, sobre todo los más chicos que llevaban su cabeza hacia atrás para poder tener total dimensión del ejemplar.

La muestra está dividida en siete polos temáticos a partir del lema "Cultivar lo humano". Foto: Victoria Egurza.

La muestra está dividida en siete polos temáticos a partir del lema "Cultivar lo humano". Foto: Victoria Egurza.

También, cerca del arco de ingreso, se ubicó la réplica del dibujo del canal Paka Paka del libertador José de San Martín, empuñando su sable, que fue la figura de fondo del acto inaugural.

El ingreso del público fue controlado por personal de Tecnópolis, que contabilizó a las personas para no superar el límite máximo de 30 mil. A cada uno de los visitantes se le aplicó alcohol en sus las manos, en el contexto del protocolo instrumentado por la pandemia.

Personal de control de Tecnópolis consultado por Télam reveló que unas 47 mil personas habían reservado su entrada para visitar hoy el lugar, aunque solo se permitió el ingreso de 30 mil visitantes para cumplir con los protocolos sanitarios.

Tecnópolis, que reúne exposiciones gratuitas de ciencia, tecnología y cultura permanecerá abierta al público hasta 12 de diciembre, los viernes, sábados, domingos y feriados de 12 a 20 y con reserva previa

La lluvia intermitente no ahuyentó a quienes decidieron visitar el lugar para "pasar un día en familia" y disfrutar de las distintas propuestas que incluye la megamuestra en sus siete polos temáticos: Industria y Ciencia Argentina, Cultura Argentina, Cultivar Lo Humano, Expresiones Urbanas, Desarrollo Sostenible, Infancias y Humedal.

Shirly Arauz (41), quien miraba contenta y aplaudía a sus sobrinas que se reían junto a un payaso haciendo burbujas grandes, contó a Télam que viajaron desde la localidad de Grand Bourg, partido bonaerense Malvinas Argentinas, para visitar el lugar junto a su madre de 62, su hija de 14, y sus sobrinas.

"Vinimos a ver de todo y a disfrutar. Además, soy fanática de Ataque77 así que me quedo hasta el final", agregó, en referencia al recital que se realizó cerca de las 19. "Hoy nos quedamos todo el día llueva o trune", aseguró.

En la fila para ver los dinosaurios, uno de los atractivos más visitados del Polo de las Infancias, estaba Erica Pintos (24), recuperadora urbana, que fue a Tecnópolis con amigos y en familia en un grupo conformado por 11 personas de chicos de 1 a 12 años y jóvenes que viven en Presidente Derqui, en Pilar.

La joven contó a Télam que fueron al parque a "pasar el día porque los chicos hace mucho no salen a pasear".

En los alrededores del mismo Polo, estaba Vanina Zarza (35) que fue junto a su familia, sus dos hijas de 5 y 11 años y su marido Rubén desde Lomas de Zamora, en un viaje que duró una hora y media.

"Habíamos escuchado en las noticias que Tecnópolis iba a reabrir y buscamos en el sitio web más detalles. Hay que apoyar este tipo de proyectos y la puesta en valor de la ciencia hay que defenderlo siempre", dijo Vanina a Télam.

En la fila de Unidad Coronavirus, dentro del predio ferial del Polo Cultivar lo humano, estaba Edgardo Furmani junto a su pareja, jóvenes docentes de nivel secundario que fueron a pasar el día a Técnópolis con su hija Jazmín de 5.

"Es la primera vez que la traemos a Jazmín, venimos desde Cañuelas con la idea de mostrarle los dinosaurios y también todo lo que es ciencia y tecnología como para que ella se vaya metiendo en este mundo", expresó Edgardo.

Dentro del predio, mientras llovía, Carina Astrada (40) almorzaba en una de las mesas bajo techo, junto a sus dos hijos, su hermana, y sus sobrinos de entre 4 y 10 años. La mujer contó a Télam que viajaron desde la localidad de Tigre en tren y colectivo para llegar al predio.

"Nos enteramos que reabría Tecnópolis a través de Facebook, porque nosotras ya habíamos venido antes y los seguimos en las redes" contó la mujer y agregó que a los chicos "lo que más les hacía ilusión a ellos eran los dinosaurios y el avión, también les mostramos los autos y arte".

Bernarda Llorente, presidenta de Télam, estuvo presente en la reapertura de Tecnópolis.

El ministro de Ciencia, Tecnología e Innovación, Daniel Filmus, encabezó la reapertura, desde donde aseguró que apoyar el desarrollo tecnológico de la Argentina es "un objetivo central muy a corto plazo".

"Hay varios futuros posibles, pero hay un solo futuro que exige que haya ciencia y tecnología, que haya un Estado presente para desarrollar la ciencia y la tecnología, la capacidad de producción, de agregar valor a partir del trabajo de los argentinos", dijo Filmus, quien señaló que el desarrollo científico y tecnológico es un "objetivo central muy a corto plazo" del gobierno nacional.

Filmus recordó que hace diez años fue la entonces presidenta Cristina Fernández de Kirchner quien puso en marcha el parque y quien durante el acto de inauguración calificó a Tecnópolis como una "invitación a imaginar el futuro".

En ese sentido, Filmus sostuvo que una muestra de esa envergadura "parecía imposible" pero con los años se convirtió en un "lugar de resistencia cuando el país era para pocos y la mayoría sufría".

Junto a María Rosenfeldt, directora de Tecnópolis, Filmus se refirió también a los desafíos que impone la pandemia de coronavirus, y en ese sentido aseguró que "estos momentos que pasamos nos plantean que cada vez es mas necesaria la ciencia y la tecnología. Los países que monopolizan el conocimiento nos mostraron que también están monopolizando la decisión de quien vive y quien muere, entonces el debate, la discusión de nuestra soberanía pasa también por la ciencia y la tecnología".

El ministro aseguró que es necesario "despertar en nuestros chicos la vocación por la ciencia" y que la el desarrollo tecnológico debe ser entendido como "una política de estado", ya que "no nos puede pasar mas que vayamos y volvamos" en torno al impulso que recibe la ciencia en el país.

Desde las 11.30 el público comenzó a ingresar al predio para disfrutar de la megamuestra. Foto: Victoria Egurza.

Desde las 11.30 el público comenzó a ingresar al predio para disfrutar de la megamuestra. Foto: Victoria Egurza.

El jefe de Gabinete, Juan Manzur, afirmó esta tarde a través de su cuenta de Twitter que “con la reapertura de este símbolo de la Argentina que es Tecnópolis, recuperamos la esperanza de un futuro mejor para todos".

"Junto a su directora, María Rosenfeldt y a funcionarios nacionales recorrimos la megamuestra en el día que volvió a abrir sus puertas al público”, dijo el funcionario.

Manzur en la reapertura de Tecnópolis: "Estamos transitando quizás la última etapa de la pandemia"

“Junto a los primeros visitantes que concurrieron al predio de Villa Martelli, pudimos disfrutar de algunos de los stands educativos de esta nueva edición. Felicito a los miles de trabajadores que hacen posible esta gran propuesta científica, tecnológica y artística”, destacó.

Este mediodía el jefe de Gabinete sostuvo que "estamos transitando quizás la última etapa de la pandemia" de coronavirus y dijo que se recobró "la esperanza de un futuro mejor" al encabezar la reapertura de la megamuestra de Tecnópolis, a la que calificó como "un símbolo de la Argentina".

"Estamos transitando quizás la última etapa de esta tragedia que nos tocó vivir, la pandemia", afirmó Manzur al encabezar el acto de reapertura de Tecnópolis en el predio de Villa Martelli, en el partido bonaerense de Vicente López.

El jefe de Ministros sostuvo que "tuvimos un impase" y destacó que "acá estamos de nuevo firmes y convencidos de que tenemos que ir por la cultura, la danza, el arte, la ciencia, la educación; estamos convencidos de que el camino es ese".

"Si nos preguntan que sentimos, el mensaje es uno solo, recobramos la esperanza de un futuro mejor, a partir de esto que es un símbolo de la Argentina tecnológica, es lo que buscamos y hacia donde vamos", sostuvo Manzur junto a los ministros de Educación, Jaime Perzcyk; de Ciencia y Tecnología, Daniel Filmus; de Cultura, Tristán Bauer y la directora de Tecnópolis, María Rosenfeldt.

El jefe de Gabinete pidió respetar los protocolos médicos pero destacó que "ya podemos empezar de a poquito a flexibilizar" las restricciones por la pandemia, tras pedir un aplauso para las personas vacunadas entre el público que recorría la feria.

"Felizmente nuestro gobierno hizo todo que lo está a su alcance para que hoy tengamos estas herramientas, que se llaman vacunas, para empezar a resolver la pandemia", agregó.

Además, destacó que a pesar de las dificultades "si algo no puede ser el Estado es ser desertor" y destacó que "en plena crisis y plena pandemia vamos a cuadriplicar las inversiones en ciencia y tecnología".

"Somos un equipo de trabajo que tenemos esta enorme responsabilidad en este momento muy difícil de seguir adelante, con nuestros sueños y nuestros ideales intactos; esto es lo que representa Tecnópolis", finalizó.

El ministro de Educación, Jaime Perczyk, afirmó que la reapertura de la megamuestraes una "fiesta” que “marca el camino de la normalización y la recuperación del encuentro con el otro en un espacio públicoen el que se puede socializar" tras la pandemia de coronavirus.

"Hoy hay dos buenas noticias comola peregrinación a Lujány esta reapertura porque está manifestando la fe de un pueblo y el reencuentro en un espacio público para toda la sociedad, más allá de cómo piensa cada uno", dijo Perczyk desde Tecnópolis.

“Se empieza a recuperar la calle y volver a tener la vida que teníamos, recuperando las relaciones sociales”, destacó el titular de la cartera educativa en diálogo con la señal de noticias C5N.

“Esta reapertura de Tecnópolis marca un camino de cómo se puede salir de la pandemia”, subrayó Perczyk, quien además agregó que “esta reapertura s una fiesta porque significa recuperar la normalidad y encontrarse con el otro, con gente que uno no ve hace un año y medio”.

**"Cultivar lo humano"**

La titular del predio explicó que trabajaron el lema "Cultivar lo Humano" con "la idea de recuperar la cultura desde todo nuestro territorio y nuestra región".

Rosenfeldt remarcó "el gran trabajo de articulación con todos los ministerios y agencias del Estado para que en cada uno de los espacios de Tecnópolis se visibilicen las políticas públicas realizadas en pandemia, con una Unidad Coronavirus, y muestras de lo realizado por científicos y científicas y desarrollos científico- tecnológicos".

"La idea es también mostrar que la respuesta frente a muchas de las problemáticas o injusticias que atravesamos es desde el trabajo colectivo", añadió en diálogo con Télam.

Este fin de semana habrá shows en vivo en el Micro estadio de Juana Azurduy, donde se subirá al escenario Ataque 77, Zamba y Nina, además de La Pipetuá y el trapero Bhavi.

También estará la banda infantil Bigolates de Chocolates y se estrenará el capítulo de ambiente de Zamba, además de una presentación de Canticuenticos en el marco del Día mundial del Hábitat que se celebra el lunes.

Como parte de las propuestas propias de Tecnópolis, las y los visitantes podrán disfrutar de talleres de instrumentos musicales con Hacelo sonar, de huerta y shows para toda la familia de la mano de artistas como Iván Noble, Les Ivans y Koufekin.

**Polos temáticos**

Los siete polos temáticos que están distribuidos en el parque, con actividades y muestras específicas en cada uno de ellos, serán el Polo Industria y Ciencia Argentina, Cultura Argentina, Cultivar Lo Humano, Expresiones Urbanas, Desarrollo Sostenible, Infancias y Humedal.

Uno de los espacios para los niños y niñas es Tierra de Dinos, donde los monumentales dinosaurios animatrónicos al aire libre "dan la bienvenida al público para pasar a adentrarse al mundo de la paleontología, la geología y la biología", indicó un comunicado el Ministerio de Cultura.

En el espacio Fábrica, de diseño e innovación, además de juegos, talleres de exploración y construcción, forma y materialidad, se podrá observar la muestra permanente de los Laboratorios Experimentales de Diseño.

En Bichos se abordará la gran diversidad de los animales clasificados como artrópodos, con foco en los insectos y a través de ecosistemas simulados se los podrá ver en sus hábitats para que el público viva una experiencia inmersiva.

Por otro lado, Ciencia en movimiento es una propuesta para "acercarse de manera entretenida a los principales fundamentos de la matemática, la química y la física" con experimentos y juegos para todas las edades, con un laboratorio de química y La casa de Newton para desafiar la gravedad.

Otro polo será PIBS (Proyecto Integrado de Biodesarrollos Sostenibles), dedicado a la educación ambiental, que dispondrá de tres circuitos -agua, tierra y energía- integrados por diversas instalaciones y dispositivos.

La novedad será la Unidad Coronavirus, un recorrido que se iniciará en un laboratorio científico y acompañará en distintas estaciones el devenir de la pandemia y temas como la prevención, el contagio y la inmunización resaltando los aportes científicos de nuestro país para combatir el virus y la colaboración con otras naciones.

También el Consejo Nacional de Investigaciones Científicas y Técnicas (Conicet) dirá presente en la feria para contar las diferentes formas de impacto que tienen los avances de la ciencia en la vida cotidiana y estimular vocaciones científicas y tecnológicas.

Por último, habrá un "espacio inmersivo" para conocer las actividades de la Conae en materia de desarrollo espacial, los hitos de las misiones satelitales, el Plan Espacial Nacional y Acceso al espacio y, entre otras actividades, en la Sala de Cine se revivirá el momento de puesta en órbita del Saocom 1B, un hecho histórico.

En el Microestadio, los sábados habrá shows aptos para todo público y los domingos espectáculos para las infancias de reconocidos artistas.

La Nave de la Ciencia será un espacio dedicado a la divulgación de la ciencia para el público general con conversatorios, contenidos de divulgación y espectáculos vinculados con la temática.

Para visitar Tecnópolis los tickets gratuitos deben sacarse a través de la página oficial del predio.

La megamuestra estará abierta hasta el 12 de diciembre, los días viernes, sábados, domingos y feriados de 12 a 20 horas.