---
category: Deportes
date: 2021-10-15T06:15:58-03:00
thumbnail: https://assets.3dnoticias.com.ar/AGENTINAPERU.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Argentina derrotó a Perú en el Monumental y sigue invicto
title: Argentina derrotó a Perú en el Monumental y sigue invicto
entradilla: "Fue por 1 a 0 con gol de Lautaro Martínez, a los 42 minutos. Ya son 25
  los encuentros que acumula sin perder el equipo dirigido por Lionel Scaloni. \n\n"

---
Argentina derrotó este jueves por la noche a Perú por la fecha 12da. de las eliminatorias sudamericanas para el Mundial de Qatar 2022. 

El conjunto albiceleste se impuso 1 a 0 nuevamente con el apoyo de los hinchas en el estadio Monumental.

Fue Lautaro Martínez de cabeza, a los 42 minutos, el que logró romper el cero. Una combinación en pared de Nahuel Molina con De Paul fue clave en la construcción de la jugada que derivó en el gol del delantero. 

 En el segundo tiempo, Yoshimar Yotún falló un penal para el combinado conducido por el argentino Ricardo Gareca. 

 El equipo dirigido por Lionel Scaloni estiró el invicto de 25 partidos que arrastra desde julio de 2019 y, a la vez, dio un nuevo paso hacia la clasificación de la Copa del Mundo, que comenzará en noviembre del año próximo.

 El seleccionado nacional se ubica en el segundo puesto con 25 puntos, a seis del líder Brasil, aún con el fallo pendiente del clásico suspendido.

 Este partido significó el cierre para la última triple fecha de Eliminatorias de este año, ya que a partir de noviembre se volverán a jugar dos partidos por ventana internacional. Los próximos compromisos de Argentina serán el 11 de noviembre en Montevideo, en el estadio Campeón del Siglo ante Uruguay; y cinco días después contra Brasil, como local, en el estadio del Bicentenario de San Juan.

 