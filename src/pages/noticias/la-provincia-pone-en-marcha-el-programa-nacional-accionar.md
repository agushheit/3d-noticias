---
category: Estado Real
date: 2021-04-01T08:06:23-03:00
thumbnail: https://assets.3dnoticias.com.ar/importar.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: La provincia pone en marcha el programa nacional AccionAR
title: La provincia pone en marcha el programa nacional AccionAR
entradilla: "“Sólo un abordaje integral, conjunto y simultáneo nos va a permitir terminar
  con problemas estructurales que golpean a la población infantil”, destacó el ministro
  de Desarrollo Social, Danilo Capitani."

---
El Ministerio de Desarrollo Social, a través de la Secretaría de Planificación, Coordinación y Control de Gestión, firmó un convenio de cooperación técnica y asistencia recíproca con el Consejo Nacional de Coordinación de Políticas Sociales (CNCPS).

El ministro de Desarrollo Social, Danilo Capitani y la Presidenta del Consejo, Victoria Tolosa Paz, rubricaron el acuerdo en la sede de gobierno de la provincia de Santa Fe en Rosario, mediante el cual se acordó la ejecución conjunta y coordinada de acciones que permitan la articulación e intercambio de información de políticas sociales, la producción de información socioeconómica y la planificación, coordinación y seguimiento de políticas públicas para mejorar la gestión.

Al respecto, Capitani remarcó que “el acuerdo alcanzado a través de este convenio nos permite trabajar de manera conjunta entre la nación y la provincia en referencia a la obtención y análisis de datos fundamentales para la implementación de políticas públicas que beneficien a los ciudadanos y ciudadanas mas vulnerables”.

“Sólo un abordaje integral, conjunto y simultáneo nos va a permitir terminar con problemas estructurales que golpean a la población infantil”, concluyó el ministro.

Por su parte, Tolosa Paz explicó que “el programa Nacional AccionAR, es una herramienta de información georeferenciada que permite detectar indicadores críticos en materia social, y posibilita coordinar acciones conjuntas para la ejecución del Plan en cada provincia. El mismo contempla un mapa que permite encarar una búsqueda activa de una problemática social como el hambre a fin de implementar políticas contra la desnutrición infantil”.

**PRESENTES**

También participaron los subsecretarios nacionales de Políticas Sociales, Juan Manuel Granillo, y de Coordinación Técnica y Análisis de la Información, Eduardo Brau; la directora de Gestión Institucional para la Información del SINTyS, Cintia Mansilla; los secretarios de Políticas de Inclusión y Desarrollo Territorial, Fernando Mazziotta, y Planificación, Coordinación y Control de Gestión, Santiago Lamberto; y los representantes de las Direcciones de Planificación, Coordinación y Capacitación, Jorge Mauro, Betina Bedetta y María Cecilia Ré, respectivamente.