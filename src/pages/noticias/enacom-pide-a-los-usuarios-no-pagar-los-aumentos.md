---
category: Agenda Ciudadana
date: 2021-04-29T07:54:42-03:00
thumbnail: https://assets.3dnoticias.com.ar/01012021-Enacom.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Enacom pide a los usuarios no pagar los aumentos
title: Enacom pide a los usuarios no pagar los aumentos
entradilla: Desde el organismo señalaron que las subas previstas para mayo no fueron
  autorizadas por el Gobierno.

---
El vicepresidente del Ente Nacional de Comunicaciones (Enacom), Gustavo López, instó a los usuarios a no pagar facturas de cable, telefonía e internet si vienen con aumentos ya que, recordó, "no están autorizados" por el Gobierno.

"Si llega una factura el 1° de mayo con aumento, directamente no hay que pagarla porque sacaremos una resolución con el no corte para todo el mundo", advirtió López en una entrevista con Radio 10.

El funcionario admitió que las empresas "desconocen una norma del Enacom, un decreto del Presidente".

"Además, en un momento de pandemia en el que están faltando camas, los mandamos a la casa y la gente tiene que estar pensando si le alcanza para pagar internet o pagar el teléfono", agregó el vicepresidente del ente.

López sostuvo que las empresas "anuncian que van a cobrar 8% (de incremento) en mayo y 7% en junio, (sumado) a lo que ya recibieron en el primer trimestre sería un 30% de incremento", por lo que se aseveró: "No sé en qué mundo viven".

El funcionario recordó que el decreto 690/2020 "está vigente" y que el precio de los servicios de telefonía, cable e internet "lo regula el Estado."

El decreto, vigente y operativo desde diciembre pasado, autoriza al Enacom a iniciar los procedimientos administrativos pertinentes para aplicar sanciones ante incumplimientos regulatorios.

"Los privados no pueden fijar el precio libremente y no pueden, obviamente, anunciar ningún aumento que no esté autorizado", aclaró al respecto.

En línea con las palabras de López, el Ente Nacional de Comunicaciones (Enacom) -en un comunicado de prensa- había informado el martes que "no ha autorizado ningún incremento de precios para los meses de mayo o junio de 2021 en los servicios de telefonía fija, telefonía móvil, Internet o televisión paga".

"Oportunamente, se informará cualquier aumento que se autorice. Por ese motivo, cualquier comunicación en contrario surgida de las prestadoras de servicios en la cual se haya anunciado una modificación de precios carece de valor legal y no ha sido convalidada por este Ente", agregó el Enacom.

"Asimismo, se recuerda que las empresas que no hayan cumplido con las resoluciones que fijaron los incrementos autorizados y no hayan respetado el monto de los mismos o no hayan otorgado la Prestación Básica Universal consagrada por el Decreto de Necesidad y Urgencia 690/20 no podrán aplicar variaciones de precios a sus clientes", concluyó el comunicado.