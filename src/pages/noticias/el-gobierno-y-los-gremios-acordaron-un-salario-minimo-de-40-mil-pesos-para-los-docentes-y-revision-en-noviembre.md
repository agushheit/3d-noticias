---
category: Agenda Ciudadana
date: 2021-08-20T06:15:26-03:00
thumbnail: https://assets.3dnoticias.com.ar/TROTTA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: El Gobierno y los gremios acordaron un salario mínimo de 40 mil pesos para
  los docentes y revisión en noviembre
title: El Gobierno y los gremios acordaron un salario mínimo de 40 mil pesos para
  los docentes y revisión en noviembre
entradilla: Es para todo el país y consistirá en 38 mil pesos desde el 1 de octubre,
  de 39 mil a partir del 1 de noviembre y de 40 mil pesos desde el 1 de diciembre
  próximo.

---
El Gobierno y los cinco sindicatos docentes nacionales acordaron en paritarias un nuevo salario mínimo garantizado para todo el país de 38 mil pesos desde el 1 de octubre, de 39 mil a partir del 1 de noviembre y de 40 mil desde el 1 de diciembre próximo, lo que totalizó una mejora del 45,5 por ciento, informaron fuentes gremiales.

Esas sumas convenidas para el nuevo salario garantizado o inicial nacional incluyeron el adicional por conectividad y el Fondo Nacional de Incentivo Docente (Fonid), informó el secretario general de la Unión Docentes Argentinos (UDA), Sergio Romero, quien confirmó a Télam que la negociación incluyó cláusula de revisión en noviembre próximo.

El salario mínimo garantizado alcanzará los 40 mil pesos en diciembre para un cargo o su equivalente, indicó un comunicado el Ministerio de Educación, que confirmó el incremento del 45,5 por ciento en el contexto de "la séptima reunión de la paritaria nacional durante esta gestión", señaló el ministro de Educación, Nicolás Trotta.

"Es fundamental realizar el mayor esfuerzo paritario en cada una de las jurisdicciones educativas para cumplir el desafío de que el salario docente supere a la inflación y tenga una progresiva recuperación en su capacidad de compra", dijo Trotta en el comunicado.

En la última paritaria federal, los cinco gremios docentes con representación nacional y el Gobierno habían acordado un aumento del 34,6 por ciento en tres tramos.

El salario inicial docente era de 27.500 pesos y, luego de ese acuerdo, pasó a 31 mil pesos en marzo, a 34.500 en julio y a 37 mil en septiembre, en tanto el Fondo de Incentivo Docente (Fonid) se incrementó un 45 por ciento desde marzo último.

Las partes acordaron reunirse en noviembre próximo, conforme lo indica el decreto 457/07, que determinó que las negociaciones anuales deben ser convocadas ese mes para "generar consensos con anticipación al inicio de los respectivos ciclos lectivos".

El Ministerio de Educación había convocado anoche a los cinco sindicatos nacionales (Ctera, Sadop, UDA, AMET y CEA) a la segunda ronda de negociaciones paritarias en el Palacio Pizzurno a partir de este mediodía y, luego de una reunión de hora y media, gremialistas y funcionarios convinieron el nuevo salario mínimo inicial garantizado.

Sindicalistas y funcionarios habían pasado a un cuarto intermedio en la inauguración de las negociaciones, luego de que los gremios rechazaran una primera propuesta oficial por considerarla "insuficiente".

El ingreso inicial es el que se determina en la paritaria nacional y, luego, cada provincia puede mejorarlo. Foto: Paula Rivas

En declaraciones a Télam, el titular de la UDA consideró que "el porcentaje de aumento obtenido y la negociación fueron buenos".

Indicó que el acuerdo incluyó una cláusula de revisión en noviembre y que, junto con los representantes de Educación, Trabajo y del Consejo Federal de Educación (CFE), también se analizó "la necesidad de avanzar hacia un consenso respecto de un convenio colectivo de trabajo marco para la actividad".

"Las paritarias docentes son fruto de la Ley de Financiamiento Educativo, pero no existe un convenio colectivo marco sectorial y es necesario acordarlo", afirmó Romero.

El también secretario de Políticas Educativas de la CGT aseguró a esta agencia que el ministro Trotta "se comprometió a intervenir en los conflictos jurisdiccionales, como ocurre otra vez en Chubut, ya que en muchas provincias no se convoca a paritarias".

El titular del Sadop, Jorge Kalinger, adelantó que Educación podría convocar en noviembre a la Comisión Negociadora del Convenio Marco y ratificó "la importancia de concertar el instrumento colectivo sectorial".

"El Sadop valora el acuerdo firmado y el compromiso de convocar otra vez en noviembre para realizar un seguimiento de la evolución del salario y destaca la importancia del ámbito paritario, que sirvió una vez más para mejorar el ingreso y las Condiciones y Medio Ambiente de Trabajo (Cymat) de los docentes privados nacionales", puntualizó la secretaria gremial de la entidad, Marina Jaureguiberry.

En su momento, Trotta había señalado la importancia de la apertura del diálogo para monitorear el acuerdo firmado este año y analizar las condiciones de trabajo de los docentes de forma previa al encuentro de noviembre próximo.

Del encuentro de hoy participaron, además de Trotta, el asesor de la cartera laboral, Alejandro Ferrari; el jefe de Gabinete de Educación, Matías Novoa Haidar; el secretario general del CFE, Mario Oporto, y los ministros integrantes de su Comité Ejecutivo Juan Lichtmajer (Tucumán), Daniela Torrente (Chaco), Felipe de los Ríos (San Juan), María Velázquez (Santa Cruz) y Agustina Vila (Buenos Aires).

También lo hicieron los dirigentes de la Confederación de Trabajadores de la Educación (Ctera), Sonia Alesso y Roberto Baradel; el titular de la Confederación de Educadores Argentinos (CEA), Fabián Felman; Romero por la UDA, y Kalinger y Marina Jaureguiberry, del Sindicato Argentino de Docentes Privados (Sadop).

Del mismo modo, participaron el secretario gremial de la Asociación del Magisterio de Enseñanza Técnica (AMET), Ernesto Cepeda, y varios paritarios de la Ctera.

El ingreso inicial de un docente es el que se determina en la paritaria nacional y, luego, cada provincia puede mejorarlo si está en condiciones de reabrir la discusión.