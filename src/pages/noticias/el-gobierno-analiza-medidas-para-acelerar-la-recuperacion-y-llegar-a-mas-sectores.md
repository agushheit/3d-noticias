---
category: Agenda Ciudadana
date: 2021-09-14T06:00:57-03:00
thumbnail: https://assets.3dnoticias.com.ar/KULFAS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: El Gobierno analiza medidas para acelerar la recuperación y llegar a más
  sectores
title: El Gobierno analiza medidas para acelerar la recuperación y llegar a más sectores
entradilla: '"Estamos escuchando el mensaje electoral y eso significa redoblar los
  esfuerzos, acelerar la concreción de las iniciativas y que generemos algunas correcciones
  sobre las que ya estamos trabajando", afirmó Matías Kulfas.'

---
El ministro de Desarrollo Productivo, Matías Kulfas, aseguró este lunes que el Gobierno ya esta discutiendo qué nuevas medidas adoptar para que "la recuperación se acelere y llegue a más sectores", al afirmar que es parte del balance del resultado de las PASO.

Kulfas formuló declaraciones al término de un acto que encabezó el presidente Alberto Fernández en el Museo del Bicentenario, en el cual se presentó el proyecto de Ley de Compre Argentino, Desarrollo de Proveedores y Compras para la Innovación, destinado a apoyar el crecimiento de empresas de origen nacional en sectores estratégicos de la economía.

"Hay decenas de interpretaciones sobre el mensaje de las urnas, desde ayer a hoy escuchamos una gran cantidad y todas son respetables; nosotros estamos analizando esa situación y esperamos esta semana poder hacer ese balance, incluso la posibilidad de nuevas medidas que profundicen el sentido de reactivación en la que estamos trabajando", aseguró.

El ministro de la cartera productiva también consideró que "seguramente hay más cosas que se pueden hacer, y eso es lo que se está discutiendo para que esa recuperación se acelere y llegar a más sectores que todavía no lo están sintiendo, y eso es lo que se analizará esta semana".

Ante otra consulta de la prensa, Kulfas descartó "completamente" la posibilidad de un "cimbronazo cambiario" y aseguró que "el Banco Central tiene herramientas para prevenir ese tipo de maniobras especulativas que son típicas de etapas electorales".

"Lo dijimos y lo ratificamos, el Banco Central está en absoluto control de la situación cambiaria, ha acumulado reservas sobre todo gracias a un superávit comercial importante que se ha producido en la Argentina en los primeros meses del 2021, y que han sido de los mejores en varios años en la Argentina", dijo Kulfas.

Consultado acerca de la posibilidad de reemplazar las indemnizaciones previstas en la Ley de contrato de Trabajo y en las convenciones colectivas por un seguro de desempleo, similar al que se aplicar en la construcción, y que fue propuesta por sectores empresarios, el ministro se mostró receptivo y señaló que "estamos dispuestos a discutir alternativas".

El ministro aclaró no obstante que eso es posible "siempre y cuando esas iniciativas no avancen para mermar o destruir derechos de los trabajadores, pero cualquier alternativa que apunte a mejorar la situación laboral, bienvenida sea", y subrayó que "el eje central es la reactivación y la recuperación de la economía, que es lo que se ha empezado a ver".

"Estamos dialogando para encontrar las mejores soluciones, en el marco de una economía que venía muy golpeada por lo significó el gobierno de (Mauricio) Macri, que fue muy complicado" a lo que se sumó "la pandemia que agudizó los problemas", dijo.

Kulfas también señaló que pese a las dificultades la Argentina se encuentra "en el camino de una reactivación que ha permitido ya recuperar buena parte de los empleos perdidos en pandemia", y sostuvo que "hay sectores como la industria, la construcción, la economía del conocimiento, que tienen niveles bastante por encima del período previo a la pandemia en 2019, así que ese es el camino que hay que profundizar".