---
category: Estado Real
date: 2021-01-14T09:58:15Z
thumbnail: https://assets.3dnoticias.com.ar/Perotti-Ferraresi.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Perotti y Ferraresi suscribieron convenios para la construcción de más de
  4.000 viviendas
title: Omar Perotti y Jorge Ferraresi suscribieron convenios para la construcción
  de más de 4.000 viviendas
entradilla: Este miércoles refrendaron las actas de adhesión de la provincia al Fondo
  Nacional Solidario de Vivienda y al Programa Casa Propia - Construir Futuro.

---
El gobernador Omar Perotti encabezó el acto que tuvo lugar en el Salón Blanco y en el que participó el ministro de Desarrollo Territorial y Hábitat de la Nación, Jorge Ferraresi, donde **la provincia adhirió al Fondo Nacional Solidario de Vivienda y al Programa Casa Propia - Construir Futuro**. Además, rubricaron las actas de ratificación al programa de Desarrollo de Áreas Metropolitanas y el financiamiento internacional de mejoramientos de barrios en Santa Fe, Rosario y Reconquista.

En primer término, el mandatario provincial, junto al ministro de la Nación firmaron el convenio mediante el cual Santa Fe adhirió al Fondo Nacional Solidario de Vivienda, programa que **tiene por objetivo construir 120.000 viviendas en todo el país durante el trienio 2021-2023**.

Seguidamente, suscribieron también el acta donde la provincia se sumó al **programa Casa Propia - Construir Futuro**, mediante el cual las partes **tienen como objetivo alcanzar la construcción de 4.242 viviendas en todo el territorio santafesino**.

«Es una gran alegría compartir este momento aquí en Santa Fe», aseguró el gobernador quien reconoció que «una aspiración de todos los argentinos y santafesinos, sin dudas, es poner en marcha viviendas, y lo que nos mueve aquí en esta reunión es tener la posibilidad concreta de tener esta jornada de trabajo con los lineamientos que se vienen definiendo con las distintas provincias, sumada Santa Fe, en los convenios respectivos para desarrollar esta tarea conjunta con el gobierno nacional, la provincia y los municipios».

Perotti destacó el financiamiento que realizará la Nación en la provincia, al señalar que «significa una inversión de más de 28 mil millones de pesos. Esto determina la posibilidad de que, en las distintas modalidades, y con la base inicial de 4.242 viviendas, luego tengamos la posibilidad de hablar de más de 6.000 viviendas construidas en la provincia en este 2021».

El gobernador expresó que «una política de magnitud nacional requiere un acompañamiento pleno", y destacó la presencia y participación en el encuentro de legisladores locales y nacionales. "Me parece que es una reunión representativa y con una convocatoria acorde a la magnitud del desafío que tenemos en la provincia particularmente y en la Nación, de llevar la vivienda no solamente como una posibilidad de dignificar a una familia, sino llevar allí el movimiento laboral necesario para estas condiciones».

«Creemos firmemente que en la coordinación de estas acciones está la posibilidad cierta y concreta para hacerlo», sostuvo el mandatario provincial, quien aseguró que «sin duda vamos a tener uno de los programas más exitosos de vivienda en todo el territorio nacional».

<br/>

### **FONDOS PARA MEJORAMIENTO DE BARRIOS**

Durante el encuentro se rubricó el acuerdo por el que la provincia accederá al financiamiento internacional para obras de mejoramiento de barrios en ciudades de los departamentos La Capital, Rosario y General Obligado (Reconquista), por un monto de inversión superior a los 949 millones de pesos.

Durante su discurso, el ministro Ferraresi dijo que para el gobierno nacional «la vivienda es un concepto de derecho, y ese derecho lo tenemos que construir en función de generar utopías a través de los sueños, pero también de generar realidades. Y esas realidades empiezan a plasmarse a partir de una política muy general que tiene que ver con la firma de convenios de Nación con cada una de las provincias, y cambiar el paradigma con respecto al tema de la vivienda».

Al respecto, aseguró que «no vamos a hablar a partir de este tiempo de viviendas sociales. Nosotros creemos que la vivienda tiene que tener la dignidad y generar el disparador para clases sociales ascendentes, entonces la vivienda tiene que ser una aspiración de crecimiento social, y trabajar a la par en esta temática que tiene que ver con el gobierno generando los recursos desde la Nación».

El funcionario nacional mencionó también que «otras de las cuestiones que vamos a modificar tiene que ver con el tema del recobro. Vamos a generar por ley un Fondo Solidario de Recupero con el cual absolutamente el 100% de las viviendas se van a pagar, que va a ser un porcentaje de lo que cada uno reciba: los que tienen planes sociales pagarán porcentajes de sus planes sociales, el que tiene su trabajo a partir de su salario», explicó.

«Para ello –continuó-, estamos articulando herramientas para que con el Banco Central y Anses sea en código de descuento directo de cada uno de los recibos, de cada una de las asignaciones que tenga cada persona que adquiera la vivienda».

En ese sentido, el ministro agregó que «no va a ser más en función de un sorteo y un recupero financiero, sino que va a ser una inversión social, y esa inversión tiene que ver con una política ascendente desde lo social, y con un disparador productivo para que la Argentina, a partir de la construcción de viviendas, tenga dos cuestiones fundamentales: una será l trabajo directo, otra la generación de producción de materiales, ya que el 99% de lo que se utiliza en la Argentina es de producción nacional».

En este proceso, «las provincias son los brazos ejecutores fundamentales, estas van a articular con los municipios cuáles son las maneras de llevar adelante la construcción en cada uno de los lugares», explicó Ferraresi quien manifestó que «en los próximos tres años esperamos construir 120 mil nuevas viviendas en la República Argentina, lo que también va a depender de la velocidad de la ejecución, para poder generar un sistema dinámico que nos permita a cada provincia tener las viviendas que se merece y las viviendas que son capaces de realizar».

«Por eso, también una de las tareas claves e importantes de los gobiernos locales y provinciales tiene que ser la generación de suelo urbano, y este punto tener la concepción y la definición de que el uso lo definen los municipios. Entonces, en esta nueva dinámica de generar suelo urbano el poder del Estado es hacerlo en función de este crecimiento social, de este crecimiento solidario, este crecimiento que tenga que ver con un derecho», concluyó Ferraresi.

Finalmente, la senadora nacional María de los Ángeles Sacnun, destacó: «que en el organigrama del gobierno nacional haya un Ministerio exclusivamente dedicado al área de vivienda, de desarrollo territorial y de hábitat, es una decisión política que contrasta con el gobierno de Mauricio Macri. Acabamos de escuchar a intendentes e intendentas decir que han quedado múltiples obras sin concluir, y esta es una preocupación del ministro y el gobernador Perotti».

<br/>

### **EL HOGAR ESCUELA EVA PERÓN SERÁ UNA REALIDAD**

Por último, Santa Fe ratificó el acta por la cual **el gobierno nacional se compromete a financiar** la obra del proyecto **Hogar Escuela Eva Perón en la ciudad de Granadero Baigorria** en su primera etapa, mientras que **el gobierno provincial se hará cargo de su ejecución**.

El convenio se realizará en el marco del programa de Desarrollo de Áreas Metropolitanas del Interior (DAMI) con financiamiento del Banco Interamericano de Desarrollo (BID).

Del acto participaron también la vicegobernadora, Alejandra Rodenas; la ministra de Infraestructura, Servicios Públicos y Hábitat de la provincia, Silvina Frana; el secretario de Hábitat, Vivienda y Urbanismo, Amado Zorzón; las diputadas nacionales por la provincia Santa Fe, Patricia Mounier, y por la provincia de Buenos Aires, Magdalena Sierra; el diputado provincial Ricardo Olivera, legisladores y legisladoras nacionales y provinciales intendentes y presidentes comunales, entre otros.