---
category: Estado Real
date: 2021-07-06T08:45:04-03:00
thumbnail: https://assets.3dnoticias.com.ar/control.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: Durante junio se detectaron 125 conductores con alcoholemia positiva en rutas
  santafesinas
title: Durante junio se detectaron 125 conductores con alcoholemia positiva en rutas
  santafesinas
entradilla: La Agencia Provincial de Seguridad Vial realizó más de 2.500 operativos,
  con el objetivo de detectar consumo de alcohol al volante.

---
La Agencia Provincial de Seguridad Vial (APSV), dependiente del Ministerio de Seguridad de Santa Fe, realizó 2.506 operativos de alcoholemia durante el mes de junio en distintos puntos estratégicos de rutas y accesos del territorio santafesino.

Durante los controles, llevados a cabo por personal de la Policía de Seguridad Vial (PSV), se fiscalizaron 52.984 vehículos y se realizaron 6.625 test de alcoholemia tanto a conductores particulares como profesionales, de los cuáles 125 resultaron positivos.

Durante los primeros seis meses de 2021, la PSV realizó 1.593 operativos de alcoholemia y controló 38.520 vehículos. Además, realizó 30.804 tests de alochol, de los cuales 1.153 dieron positivo.

Al respecto, la directora de la APSV, Antonela Cerutti, destacó que “continuamos con la decisión de fiscalizar sistemáticamente, junto a la Policía de Seguridad Vial, a todos aquellos conductores que transiten las rutas que atraviesan nuestra provincia. Junto al gobernador Omar Perotti y el ministro de Seguridad, Jorge Lagna, estamos fuertemente comprometidos con reforzar todas aquellas políticas en materia de seguridad vial que ayuden a prevenir accidentes y que además, resguarden la vida de cada santafesina y santafesino”.

**Alcohol y conducción**

El alcohol está presente en al menos 1 de cada 4 siniestros viales con fallecidos y que incluso una mínima cantidad ya afecta la aptitud para conducir, la percepción se distorsiona, se alargan los tiempos de reacción y se genera una falsa sensación de seguridad.

El alcohol no es un estimulante, es un depresor del sistema nervioso central y afecta las funciones inhibidoras del cerebro, las intelectuales, las sensoriales y las motoras. La principal recomendación del organismo provincial es no consumir alcohol si se va a conducir y en este caso designar a otra persona que no haya bebido para tomar el volante.