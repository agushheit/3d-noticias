---
layout: Noticia con imagen
author: "Fuente: El Litoral"
resumen: Fondo Mantenimiento Vial
category: La Ciudad
title: "Fondo de Mantenimiento Vial: un tributo que se cobra, pero no se refleja"
entradilla: Hay abonos que llegan hasta los 7 mil pesos. La venta de autos
  durante el 2020 es muy positiva. Y las calzadas santafesinas, desde hace
  muchos años, no son las ideales.
date: 2020-11-25T12:17:09.073Z
thumbnail: https://assets.3dnoticias.com.ar/calles.jpg
---
El **Derecho de mantenimiento por el uso de la red vial municipal**, o más conocido como "impuesto al bache", fue creado por el Concejo mediante la ordenanza Nº 12.064, en 2013. Dicho tributo lo abonan todos los santafesinos que compran un auto o moto, o realizan la transferencia de un vehículo.

El monto del impuesto al bache varía todos los años, siempre dependiendo del valor del móvil adquirido (Ver Valores vigentes). Por ejemplo: en la actualidad los vehículos de más de 1 millón de pesos -y hasta 1 millón y medio de pesos-, deben pagar una carga extra de 6.400 pesos. Mientras que los que superan dicho límite, abonan 7.000 pesos. Por su parte, los motovehículos cuyo valor de venta se encuentra entre 240 y 500 mil pesos, pagan un impuesto de 2.700 pesos. Y aquellos que están por encima del medio millón, tienen un arancel de 4.000 pesos.

En este año tan particular por la pandemia, en nuestro país (¿cuándo no?), el dólar otra vez se fue por las nubes. Entonces la gente que puede, opta por hacer negocios en pesos. Y una de las principales inversiones elegidas por los argentinos es la compra de autos. La capital no es la excepción a esta regla.

El Litoral consultó algunas concesionarias locales y confirmó esta teoría. Además, a nivel nacional, ya se habla de un año récord en cuanto a las ventas. Dichas transacciones, casi en su totalidad, superan cada una el millón de pesos. Es decir que aquí en la capital provincial, en este 2020 (desde junio para adelante) la recaudación municipal por el impuesto al bache debiera ser bastante sustanciosa. No se pudo acceder a una cifra -al menos estimativa- de lo recaudado hasta el momento, porque recién el pasado lunes 16 de noviembre el Gobierno local presentó el Presupuesto Municipal 2021.

**El objetivo principal del impuesto al bache siempre fue que un contribuyente pague el monto especificado, sabiendo que ese dinero se invierte en tener las calles en condiciones**. A esa premisa se le desprende una pregunta: ¿esto se ve reflejado en la realidad? La respuesta es no.

En un recorrido por distintas avenidas y calles de diversos barrios de la ciudad, El Litoral pudo constatar que hay baches "para todos los gustos": grandes, chicos, medianos, profundos. Aristóbulo del Valle al 8700; Juan del Campillo y Pje. Marsengo; 1° de Mayo y Bulevar; Obispo Gelabert y Urquiza; Francia e Hipólito Irigoyen; San Martín y Corrientes; Gral. López y Urquiza; Avda. Freyre casi Crespo; y 1° de Mayo esquina Amenábar, son algunos de los tantos ejemplos.

![](https://assets.3dnoticias.com.ar/bache.jpg)

En muchos casos las roturas, abarcan todo el ancho de una calle; a otros los rellenan con adoquines; están aquellos que, si llueve, se tapan con agua y terminan siendo algo muy peligroso. Y si el agua no se absorbe, empiezan a crecer plantas dentro de los socavones. En definitiva, hay una importante variedad y cantidad de baches diseminados por la ciudad.

### **Valores vigentes**

El artículo 38 de la Ordenanza Nº 12.707 (Ordenanza Tributaria Municipal Anual, aprobada por el Concejo el 20 de febrero de este año) modifica el artículo 59° de la Ordenanza N° 12.226, de 2015. Establece que el monto a abonar en concepto de Derecho de Mantenimiento por el Uso de la Red Vial Municipal (impuesto al bache) consistirá en un monto fijo según las siguientes escalas:

**a) Para altas y/o transferencias de vehículos automotores:**

* Vehículos cuyo valor no supere los $ 200.000: $ 1.200,00
* Vehículos de más de $ 200.000 y hasta 600.000: $ 4.200,00
* Vehículos de más de $ 600.000 y hasta $ 1.000.000: $ 5.800,00
* Vehículos de más de $ 1.000.000 y hasta $ 1.500.000: $ 6.400,00
* Vehículos de más de $ 1.500.000: $ 7.000,00

**b) Para altas y/o transferencias de vehículos de más de dos ejes** que resulten alcanzados por las disposiciones de la Ordenanza 11.014 (vehículos de tránsito pesado), $ 5.400,00.

**c) Para altas y/o transferencias de motovehículos:**

* Motovehículos cuyo valor no supere los $ 40.000: $ 460,00
* Motovehículos de más de $ 40.000 y hasta $ 80.000: $ 720,00
* Motovehículos de más de $ 80.000 y hasta $ 160.000: $ 1.100,00
* Motovehículos de más de $ 160.000 y hasta $ 240.000: $ 2.300,00
* Motovehículos de más de $ 240.000 y hasta $ 500.000: $ 2.700,00
* Motovehículos de más de $ 500.000: $ 4.000,00