---
category: El Campo
date: 2021-01-10T11:06:06Z
thumbnail: https://assets.3dnoticias.com.ar/100121-agro-santafe.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: '3DNoticias - Fuente: El Litoral'
resumen: Vigilia de productores en todo el país antes del paro del lunes
title: Vigilia de productores en todo el país antes del paro del lunes
entradilla: 'Desde mediados de semana se realizan asambleas. En Pergamino los asistentes
  votaron solicitar a las entidades que se retiren del Consejo Agroindustrial Argentino
  si no apoyan incondicionalmente el reclamo. '

---
**«Solicitamos a las entidades que integran el Consejo Agroindustrial Argentino que apoyen incondicionalmente nuestro reclamo o en caso contrario exigimos a las entidades que allí nos representan que se retiren de inmediato del mismo».**

El contundente reclamo hacia quienes actualmente dialogan con el gobierno nacional en representación del sector se oyó con fuerza en la asamblea que la agrupación Campo + Ciudad organizó en Pergamino el pasado jueves 8. Fue el punto más saliente de la votación entre los presentes, además de adherir al **paro dispuesto por la Mesa de Enlace por 72 horas desde el próximo lunes**. Por último, aprobaron una vigilia: «para asegurarnos que nuestra demanda sea satisfecha», indicaron.

En tanto, las reuniones siguieron desarrollándose en distintos puntos del país. Ayer hubo una asamblea en el sur santafesino, en el cruce de las rutas A012 y nacional 34, y hoy habrá otra en Armstrong desde las 17.30 horas.

En el norte, la Carpa Republicana que se instaló al ingreso de Avellaneda comenzó a recibir visitantes y en carteles se observaron reclamos más allá de los intereses del sector. Por ejemplo, contra el recorte a los jubilados.

Desde Entre Ríos, en tanto, la filial de FAA convoca a una asamblea mañana domingo en Crespo. En un comunicado, consideran que el freno a las exportaciones de maíz es «nueva usurpación», en un paralelismo con el intento de Juan Grabois de apropiarse de un campo de la familia Etchevehere.

«Este Gobierno está tomando medidas que son una nueva usurpación de nuestro futuro. Están preanunciando más medidas. Las decisiones de los que nos gobiernan son equivocadas y van agravando la situación. Porque prohibir es arbitrario, como en el caso de exportar maíz».

El texto considera que «los argentinos vemos que esto va a terminar con familias más pobres y que engañan cuando dicen que con las retenciones del campo quitan la pobreza». Por ese motivo, se preguntan: «¿por qué será entonces que nuestros países vecinos, sin retenciones, tienen un menor índice de pobreza y además crecen?»

Finalmente, sostienen: «tenemos autoridades electas y queremos saber de qué lado están. Somos ciudadanos que reclamamos justicia y poder vivir con dignidad, con cultura del trabajo, sin restricciones a nuestras libertades».

Para expresar este descontento y hacer llegar la voz del pueblo, **convocan a una Asamblea Ciudadana este domingo 10 de enero a las 17 horas en La Rotonda de Crespo, Entre Ríos**.