---
category: La Ciudad
date: 2021-12-20T06:15:26-03:00
thumbnail: https://assets.3dnoticias.com.ar/bancopng.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Cómo se produjo el vaciamiento del Banco Provincial de Santa Fe según los
  fiscales
title: Cómo se produjo el vaciamiento del Banco Provincial de Santa Fe según los fiscales
entradilla: 'Se hizo la audiencia de apelación de los fiscales contra la resolución
  que declaró prescripta la investigación del vaciamiento del Banco Provincial de
  Santa Fe

'

---
Los fiscales Mariela Jiménez y Jorge Nessier representaron al Ministerio Público de la Acusación en la audiencia de apelación llevada a cabo el pasado viernes en el marco de la investigación de maniobras ilícitas por al menos 500 millones de dólares que provocaron el vaciamiento del Banco Provincial de Santa Fe. El camarista Jorge Andrés estuvo a cargo de la conducción de la audiencia y deberá resolver el planteo.

“A esta instancia se llegó a raíz de la impugnación que realizamos luego de la resolución de primera instancia en la que se declaró extinguida la acción penal por prescripción respecto de todos los hechos denunciados en 1990 por integrantes de la Comisión Gremial de Empleados del Banco Provincial de Santa Fe”, informaron los representantes del MPA.

De la audiencia de Cámara también participaron tres abogados defensores de las 10 personas investigadas que fueron integrantes del directorio del Banco Provincial entre 1983 y 1990.

**Al menos 500 millones de dólares**

“En nuestro planteo ante el camarista sostuvimos que en primera instancia no se consideraron diversas cuestiones de gravedad institucional y que, en caso de que se hubieran tenido en cuenta –y más allá de los 30 años transcurridos–, no se hubiera declarado prescripta la acción penal”, manifestaron los fiscales.

En tal sentido, recordaron que “se trata de la investigación en la que se intenta demostrar la responsabilidad de los sospechados, en lo que implicó el vaciamiento de la entidad bancaria más importante de la provincia de Santa Fe y que provocó un perjuicio económico al Estado santafesino estimado inicialmente en no menos en 500 millones de dólares”.

Los fiscales puntualizaron que “existen sobrados motivos para considerar que los actos de corrupción que se investigan, sus incidencias en enriquecimientos patrimoniales y la incalculable afectación de las arcas del Estado provincial hacen viable su consideración como «delito constitucional» y, por ende, imprescriptible tal como lo establece el artículo 36 de la Constitución Nacional”. Asimismo, refirieron a la “existencia de antecedentes jurisprudenciales en otras grandes estafas cometidas contra el Estado, como el caso IBM-Banco Nación”.

En la audiencia, los fiscales Jiménez y Nessier plantearon que “la pérdida de la capacidad financiera de la entidad fue seguida por su privatización, tras lo cual dejó de ser la herramienta financiera de nuestra provincia”. Y añadieron que, “como es sabido, su quiebra y posterior paso a manos privadas generó también una grave afectación a la historia laboral de muchos trabajadores, que se vieron obligados a permanecer en una entidad –ahora mixta– con otras condiciones laborales, o que tuvieron que migrar hacia otras áreas del Estado y perdieron sus antecedentes y trayectoria”.

“Los medios a través de los cuales se habría provocado el vaciamiento se centraron en tres ejes: las contrataciones espurias y simuladas; los créditos irregulares e incobrables y el traspaso ilegítimo de fondos a la Provincia de Santa Fe”, enumeraron Jiménez y Nessier. En ese sentido, informaron que “son más de 200 las entidades, grupos económicos y personas que se vieron beneficiadas con el otorgamiento de créditos millonarios que nunca fueron recuperados por el Banco”.

**Una investigación estancada**

Nessier y Jiménez también subrayaron en la audiencia que “la investigación tuvo idas y vueltas entre la Justicia Federal y la provincial, y estuvo estancada casi 20 años”. Especificaron que “durante 14 años estuvo en un juzgado provincial y otros cinco años más en la secretaría de un juzgado federal, y en ninguno de los dos casos se adoptó medida alguna”.

Asimismo, refirieron al informe elaborado en 2013 por la Procuraduría de Criminalidad Económica y Lavado de Activos (Procelac) de la Nación. “En ese documento se afirma que el derrotero errático del legajo no fue más que una estrategia para eludir la investigación y dispendiar actividades jurisdiccionales a lo largo del tiempo con miras a lograr la finalización de la causa por prescripción”, destacaron.

“Recién en abril de 2019 se remitió el expediente al MPA, y se dio impulso a una investigación penal que es la que estamos trabajando ahora”, especificaron los fiscales.

Por otra parte, los funcionarios del MPA argumentaron que, “en tanto uno de los involucrados reviste el carácter de funcionario público, estamos ante una circunstancia que suspende el curso de la prescripción”.

**Cuestión ya resuelta**

Los fiscales hicieron hincapié en que “existen diversos factores por los cuales la acción penal no está extinguida” y trajeron a colación “lo que ya dispusieron otros órganos jurisdiccionales”. Mencionaron que “en este mismo proceso y ante planteos formulados por los abogados defensores, la Cámara Federal de Rosario ya señaló que no existe ningún obstáculo para seguir la investigación”.

En tal sentido, los fiscales del MPA remarcaron que, “al desarrollar nuestros argumentos, recordamos que la decisión tomada en los tribunales provinciales de primera instancia que declara la prescripción, en realidad, implica volver a expedirse sobre una cuestión ya tratada y resuelta e impide averiguar qué fue lo que realmente ocurrió”.

De acuerdo con lo informado por la Oficina de Gestión Judicial (OGJ), mañana y el martes se llevarán a cabo las audiencias en las que se tratarán dos pedidos de constitución de querellantes. Uno fue realizado por la Fiscalía de Estado de la provincia de Santa Fe, y otro por la Asociación Bancaria Argentina.