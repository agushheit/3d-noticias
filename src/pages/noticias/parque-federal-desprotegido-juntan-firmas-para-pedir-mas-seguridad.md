---
category: La Ciudad
date: 2021-04-08T06:14:23-03:00
thumbnail: https://assets.3dnoticias.com.ar/foto-parque-federal.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de El Litoral
resumen: 'Parque Federal desprotegido: juntan firmas para pedir más seguridad'
title: 'Parque Federal desprotegido: juntan firmas para pedir más seguridad'
entradilla: Robos, vandalismo e incluso abusos sexuales se registran en distintos
  sectores del paseo. Desde la Asociación del parque reclaman por la presencia de
  la Guardia de Seguridad Institucional y remarcan falta de luces.

---
El reciente caso de un depravado detenido a plena luz del día en el Parque Federal tras manosear a una mujer encendió las alarmas de vecinos y de la Asociación que vela por el cuidado del predio. 

A este hecho, se le suman los reiterados episodios de inseguridad que sufren quienes lo visitan a diario y los actos de vandalismos en este espacio público ubicado en el centro norte de la ciudad de Santa Fe. En este marco, la citada entidad intermedia lanzó una convocatoria a toda la sociedad para sumar adhesiones y llevarle un petitorio al municipio. El reclamo concreto es pedir presencia de personal de la Guardia de Seguridad Institucional a modo de prevención de los delitos anteriormente citados. 

 Alejandro Álvarez, presidente de la Asociación Parque Federal dialogó con El Litoral y brindó un panorama completo de la actualidad de este pulmón verde de la capital provincial.  

**—En 2020 El Litoral publicó un informe sobre lo oscuro que estaba el parque, ¿mejoró esa situación?**

— Está igual o un poco peor. Según el momento en que se observe. Hay algunos arreglos esporádicos, en el que se recupera algún sector pero en general alcanza con venir y circular por calle Pedro Víttori y mirar la “boca de lobo” que es una parte significativa del predio. En realidad, hay que decir que el parque es una obra inconclusa; está pronto a cumplir 25 (en septiembre). La última gran obra en el lugar fue en el 2011, seis meses después que abrió La Redonda. 

Desde entonces, se fue deteriorando el espacio. Por ejemplo, de los reflectores que colocaron en el piso sólo funcionan los que están cerca del edificio de La Redonda (N. del R: sobre Salvador del Carril). Otro caso, son las columnas de iluminación del sendero de los senderos peatonales; casi una decena de esos postes se han caído, quedaron los cables expuestos y no se recuperaron. Algo similar ocurre con las tres torres que se mantuvieron del antiguo predio ferroviario que actualmente están todos esos reflectores inactivos.  

**—En el petitorio reclaman la vuelta de la GSI al parque, ¿Cómo fue la experiencia cuando se contó con esa presencia?** 

—La GSI se instaló con un puesto fijo en 2011, en una casilla que prestó el gobierno provincial, se ubicó a la altura de calle Agustín Delgado y funcionó hasta los primeros meses de 2015. Comenzó con una dotación de dos o tres agentes por turno, prácticamente se cubría todo el día. Era una presencia municipal preventiva. Tras cuatro años fue cayendo el número de personal y equipamiento y fueron reubicados por motivos de la emergencia hídrica de ese año. Al puesto lo prendieron fuego y lo retiraron. Tras esa salida, el parque quedó muy desprotegido.  

Desde esos momentos se notó el avance de hechos de inseguridad y violencia. Llegamos a tener casos de abusos sexuales a plena luz del día, como el ocurrido días atrás. También vemos gente que prende fuego, con todo lo que ello significa. La circulación de motos en la bicisenda es realmente preocupante también. Este martes por la noche tuvimos que llamar al 0800 municipal porque había autos subidos al parque, donde está el espacio de juegos “Gastón Gori”. Lo irracional es que en el lugar hay dársenas y lugar para estacionar.  

![](https://assets.3dnoticias.com.ar/Parque-Federal-09.jpg)

**Mesa de consenso y gestión** 

Desde hace años y por ordenanza funciona en el Parque Federal una mesa de consenso y gestión que cuenta con la participación de los gobiernos municipal y provincial, los vecinos y entidades intermedias e instituciones de conocimiento como facultades e institutos. 

Sobre este punto, Álvarez señaló que durante 2020 hubo reuniones virtuales pero sobre el punto “seguridad”, las respuestas han sido negativas. “Lo único que se ofrece es un patrullaje esporádico con las camionetas de la GSI. Nosotros planteamos un esquema de intervención, que se cumplió en un principio con la pandemia y las normas de distanciamientos pero pasó el tiempo y se dejó”, dijo. 

Al ser consultado sobre la acción policial, el Presidente de la Asociación del Parque Federal expresó que fue escaso el involucramiento de la fuerza de seguridad con el paseo. “Tenemos dos comisarías muy próximas (N del R: la Seccional 5ta y la 11°) y sin embargo no hay participación en las reuniones. Las pocas veces que hubo presencia en los encuentros de la Mesa, expusieron la falta de personal, equipos y movilidad”, afirmó. 

 

**PETITORIO**

La idea de la Asociación del Parque Federal es juntar firmas hasta fines de abril o principios de mayo y luego elevar el petitorio al municipio. Desde la entidad detallaron que quienes deseen apoyar esta iniciativa pueden buscar planillas para juntar firmas en la Feria del Parque (domingos de 16 a 20 hs en Quintana y la Bicisenda) o por correo electrónico a:  parquefederal@gmail.com.