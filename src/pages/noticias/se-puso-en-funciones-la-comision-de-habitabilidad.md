---
category: La Ciudad
date: 2021-11-17T06:00:52-03:00
thumbnail: https://assets.3dnoticias.com.ar/estacion.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Se puso en funciones la Comisión de Habitabilidad
title: Se puso en funciones la Comisión de Habitabilidad
entradilla: El intendente Emilio Jatón presentó formalmente el nuevo espacio. Asimismo,
  se coordinó la agenda a futuro que tiene como objetivo trabajar las reglamentaciones
  de la ordenanza del nuevo Código de Habitabilidad.

---
Tras aprobarse recientemente la norma que ordena la construcción en la capital provincial, este miércoles se puso en funcionamiento la Comisión de Habitabilidad. El primer encuentro se realizó en la Estación Belgrano y contó con la presencia del intendente Emilio Jatón, integrantes del gabinete municipal y representantes del Concejo, de las universidades, de los Colegios de Ingenieros, de Arquitectos y de Técnicos de la Construcción.

La comisión también está conformada por Bomberos Zapadores, representantes de la Cámara Argentina de la Construcción y la Comisión Municipal de Discapacidad. Cabe recordar que la norma recién aprobada establece que se debe convocar dentro de los primeros 15 días a esta Comisión creada por el nuevo Código de Habitabilidad para trabajar en la reglamentación de la misma.

Luego del encuentro, que se extendió por más de dos horas, el secretario de Desarrollo Urbano del municipio, Javier Mendiondo, indicó: “Tal como lo indica la norma, pautamos una serie de encuentros de la Comisión de Habitabilidad de aquí en más para avanzar en la reglamentación de esta Ordenanza que es importantísima y, junto con el Código de Patrimonio, viene a saldar una deuda histórica de esta ciudad”.

En ese sentido, el funcionario repasó que el actual Reglamento de Edificaciones data del año 1976: “El mismo no tenía en cuenta aspectos ambientales, ni referidos a la calidad de vida o a la importancia del espacio urbano; por lo tanto, estamos felices de tener una ordenanza que será un instrumento para construir ciudades más humanas y justas”.

“La Comisión tiene como objetivo profundizar en los detalles específicos referidos a temas de accesibilidad, tramitaciones o aspectos técnicos como por ejemplo retardadores hídricos, para que el 1 de marzo -cuando entre vigencia esta ordenanza- tengamos todos los instrumentos reglamentarios para poder ponerla en práctica”, concluyó Mendiondo.

**Trabajo conjunto**

Por su parte, María Elena Ghietto, representante de la Cámara Argentina de la Construcción Delegación Santa Fe, mostró su satisfacción por la puesta en marcha de la Comisión. “Es excelente esta convocatoria porque, una vez que estuvo presentado en el Concejo el nuevo Código, lo inmediato era convocar a la comisión y empezar ese trabajo en equipo”.

“Desde la Cámara Argentina de la Construcción Delegación Santa Fe apoyamos y vamos a contribuir en este espacio que se empezó a implementar hoy y que es fundamental para que este nuevo código crezca y pueda empezar a funcionar a partir de los 180 días que rigen”, añadió Ghietto.

En tanto, Ramiro Piva, en representación de la Universidad Nacional del Litoral (UNL) y Gabriel Biagioni, por la Universidad Católica de Santa Fe (UCSF), expresaron su conformidad con los ejes desarrollados en el primer encuentro. “Fue una reunión importante, sobre lo que respecta a la habitabilidad. Tiene aspectos muy potenciales, muy positivos para los que trabajamos en la profesión y para quienes educamos desde las universidades y las facultades de arquitectura; consideramos que esta nueva ordenanza se va a reflejar en la calidad urbana de la ciudad”, valoró.

En tanto, Biagioni detalló algunos de los puntos que se analizaron vinculados al cambio de paradigma “que supone el nuevo código: desde aspectos reglamentarios, los términos de caducidad, el manejo de los expediente, hasta lo que tiene que ver con la sustentabilidad de los nuevos edificios, el paradigma de las cocheras, cómo se va a trabajar en relación a las unidades y la cantidad de estacionamientos, y con temas relacionados a la calidad urbana y de habitabilidad”. En ese sentido, indicó que, además, se trabajó sobre temas vinculados con los materiales, que el código antiguo no prevé, “como construcción en seco, en tierra y demás”.

**Asistentes**

Participaron de esta primera reunión, por el Ejecutivo Municipal, Javier Mendiondo, José Citroni y Fernando Arizpe; por el Concejo Municipal, Lucas Simoniello y Carlos Suárez; por el Colegio de Arquitectos, Julio Cavallo y Pedro Choma; por el Colegio de Ingenieros, Juan M. Costa; por el Colegio de Técnicos de la Construcción, Ismael Bruno. En tanto, por la Universidad Nacional de Litoral, Ramiro Piva; por la Universidad Católica de Santa Fe, Gabriel Biagioni; por la Universidad Tecnológica Nacional, Marcelo Avendaño; por la Cámara Argentina de la Construcción, María Elena Ghietto; también por referentes de Bomberos Zapadores, Claudia García y Nicolás Levequi.