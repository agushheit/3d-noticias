---
category: Agenda Ciudadana
date: 2021-12-11T06:00:46-03:00
thumbnail: https://assets.3dnoticias.com.ar/DEMOCRACIA1.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: 'Cristina contra el Fondo: "El FMI ha vivido condicionando a la democracia
  Argentina"'
title: 'Cristina contra el Fondo: "El FMI ha vivido condicionando a la democracia
  Argentina"'
entradilla: '“Necesitamos que el Fondo nos ayude a recuperar miles de millones de
  dólares que se han ido a los paraísos fiscales", resaltó la vicepresidenta en el
  cierre del acto en Plaza de Mayo por los 38 años de democracia.

'

---
En el cierre del acto por los 38 años de democracia, la vicepresidenta Cristina Kirchner advirtió hoy que el Fondo Monetario Internacional "ha condicionado la democracia" argentina en las últimas décadas y ahora, tras el gobierno de Mauricio Macri están "otra vez adentro" del país para "controlar las cuentas".

“Necesitamos que el Fondo nos ayude a recuperar miles de millones de dólares que se han ido a los paraísos fiscales. Presidente comprométase a que cada dólar que encuentre en el exterior, de los que se la llevaron sin pagar impuestos, se lo vamos a dar al Fondo”, resaltó Cristina Kirchner.

En el escenario montado frente a la Casa Rosada, Cristina Kirchner señaló que Alberto Fernández al asumir "se encontró con una el regreso del FMI a la Argentina, con el regalito de 44.000 millones de dólares".

"Y la verdad que, más allá de todo, permítanme ser un poco desconfiada. Saben que pasa, el FMI ha vivido condicionando a la democracia Argentina. No es de ahora, recuerdo cuando el Presidente Alfonsín asumió un día como hoy hace 38 años, y recibió un país que había quintuplicado su deuda externa, sin reservas en el Banco Central, con asomadas militares cada tanto, con 30.000 desaparecidos, y con el año 89 no pudo terminar su mandato", afirmó la vicepresidenta.