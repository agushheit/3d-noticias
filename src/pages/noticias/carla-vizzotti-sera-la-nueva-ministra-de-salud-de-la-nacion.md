---
category: Agenda Ciudadana
date: 2021-02-20T06:30:01-03:00
thumbnail: https://assets.3dnoticias.com.ar/VIZZOTTI.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Carla Vizzotti será la nueva Ministra de Salud de la Nación
title: Carla Vizzotti será la nueva Ministra de Salud de la Nación
entradilla: Reemplazará en el cargo a Ginés González García, a quien el Presidente
  Alberto Fernández le solicitó la renuncia.

---
Carla Vizzotti asumirá como ministra de Salud en lugar de Ginés González García, quien tras el escándalo por la vacunación vip, debió presentar la renuncia a pedido del Presidente de la Nación.

Ese episodio cayó muy mal en la Casa Rosada, donde consideraron que se enturbia el plan de vacunación nacional.

La salida de González García se da a poco de cumplir un año y dos meses de gestión y haber comenzado la vacunación contra el coronavirus entre el personal de salud y los mayores de 70 años.

Vizzotti se reunió en ultimas horas de este viernes, con el presidente Alberto Fernández y el jefe de Gabinete, Santiago Cafiero, para tomar posesión del cargo. Una de las preguntas que tuvo que responder la flamante ministra fue si estaba al tanto de lo que ocurría en el ministerio.

La especialista en medicina interna por la Universidad de Buenos Aires, fue durante la pandemia que provocó el coronavirus, la encargada del equipo del plan Detectar, pieza clave de la estrategia sanitaria para contener el incremento de casos y fue la cara del gobierno nacional en la comunicación diaria contra el coronavirus junto a Alejandro Costa, subsecretario de Estrategias Sanitarias.