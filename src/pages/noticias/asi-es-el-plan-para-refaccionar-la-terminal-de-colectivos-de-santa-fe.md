---
category: La Ciudad
date: 2021-10-08T06:00:56-03:00
thumbnail: https://assets.3dnoticias.com.ar/TERMINAL.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Así es el plan para refaccionar la terminal de colectivos de Santa Fe
title: Así es el plan para refaccionar la terminal de colectivos de Santa Fe
entradilla: "El gobierno nacional financiará el proyecto del municipio para refuncionalizar
  el edificio y mejorar los espacios públicos del entorno. \n\n"

---
Una de las obras que tiene previsto anunciar para la capital santafesina el ministro de Transporte de la Nación, Alexis Guerrera, es la refuncionalización de la terminal de ómnibus Manuel Belgrano, para lo cual se prevén destinar 104 millones de pesos. Se trata de uno de los proyecto que el intendente Emilio Jatón presentó al gobierno nacional para obtener financiamiento.

 En el marco de la elaboración del "Master Plan de Renovación y Desarrollo Urbano", para el área de la Estación Terminal de Ómnibus Gral. Manuel Belgrano y su entorno inmediato, la propuesta plantea la remodelación y puesta en valor del edificio y la recalificación del espacio público circundante.

 La idea es mantener la fisonomía original del edificio, operando sobre los cerramientos y en los espacios interiores, sin modificar la superficie cubierta actual.

En tal sentido, se pretenden mejoran las condiciones de los accesos y renovar solados, a partir de la unificación de materiales y niveles de piso. El diseño contempla todas las premisas de accesibilidad: eliminando todo tipo de barreras arquitectónicas, incorporando vados en zonas de cruce peatonal y losetas podotáctiles, para personas con capacidades diferentes.

 En cuanto a las intervenciones en el interior del edificio, la propuesta plantea la recuperación de los espacios de halles y circulaciones. En tal sentido, se propone el ordenamiento de la cartelería de publicidad y señalética, la restauración de los cerramientos exteriores, y la nueva iluminación de los espacios interiores mediante el uso de tecnología LED. 

 Además, se planificó optimizar los núcleos de circulación vertical, anexando un ascensor para dar acceso a los pisos superiores a las personas con problemas de movilidad, e interconectando los entrepisos existentes mediante una pasarela sobre el acceso principal. Se construirán nuevos núcleos sanitarios en el piso I y II a fin de liberar el resto de la superficie de las plantas y así ganar flexibilidad en los futuros usos.