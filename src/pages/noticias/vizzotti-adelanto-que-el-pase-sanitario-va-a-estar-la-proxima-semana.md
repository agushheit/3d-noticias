---
category: Agenda Ciudadana
date: 2021-12-03T06:15:03-03:00
thumbnail: https://assets.3dnoticias.com.ar/VIZZOTTI2.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: Vizzotti adelantó que el pase sanitario va a estar la próxima semana
title: Vizzotti adelantó que el pase sanitario va a estar la próxima semana
entradilla: 'La ministra de Salud comentó que la implementación de este instrumento,
  está siendo acordada en un trabajo conjunto con las áreas de Deportes, Turismo,
  Cultura y las jurisdicciones provinciales.

'

---
La ministra de Salud, Carla Vizzotti, adelantó hoy que el pase sanitario con el que se busca restringir la asistencia de personas que no tengan el esquema de vacunación completo en eventos con alta concurrencia "va a estar seguramente la próxima semana".

Según dijo durante una conferencia de prensa en Santa Fe, la implementación de este instrumento, que busca seguir incentivando la vacunación contra el Covid-19, está siendo acordada en un trabajo conjunto con las áreas de Deportes, Turismo, Cultura y las jurisdicciones provinciales en el marco del Consejo Federal de Salud (Cofesa).

La funcionaria llegó a la capital santafesina para visitar junto a su par provincial Sonia Martorano, el Instituto Nacional de Enfermedades Respiratorias Emilio Coni, que este jueves cumple 60 años.

Allí subrayó que "el pasaporte sanitario lo que busca es que nos cuidemos entre todos... poner en relevancia lo que significa completar los esquemas de vacunación".

Vizzotti aclaró que "no hay una resistencia a vacunarse en la mayoría de la sociedad, sino que luego de haber pasado la segunda ola y la percepción de riesgo tan alta, hay algunas personas que no priorizaron vacunarse y esta estrategia es para estimular la vacunación".

En este sentido, identificó que los más reacios a completar el esquema de vacunación se encuentran mayormente en "la franja de edad de entre 18 y 39 años que han perdido los turnos de la segunda dosis".

"Eso es seguramente porque perciben que tienen menos riesgo y porque la situación epidemiológica es muy favorable", analizó.

No obstante, enfatizó que "el mensaje para esa franja etaria es que es clave recibir la segunda dosis, reforzar la inmunidad para que en el momento en que aumente la circulación viral estemos todos lo más inmunes posible".

"Estamos en un momento relevante porque tenemos la película de lo que está pasando en el hemisferio norte, tenemos la emergencia de una nueva variante", advirtió.

De todos modos, remarcó que el país transita "un momento epidemiológico favorable y de mucho optimismo".

"El momento es de accionar Nación, provincias, los municipios, los medios de comunicación y la sociedad, para ejercer su derecho a vacunarse y entre cada una de las áreas minimizar el riesgo de aumento de casos y hospitalizaciones", finalizó la ministra.