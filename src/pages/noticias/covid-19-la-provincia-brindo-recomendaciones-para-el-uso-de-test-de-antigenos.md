---
category: Agenda Ciudadana
date: 2021-04-29T08:52:03-03:00
thumbnail: https://assets.3dnoticias.com.ar/antigeno.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: 'Covid-19: La provincia brindó recomendaciones para el uso de test de antígenos'
title: 'Covid-19: La provincia brindó recomendaciones para el uso de test de antígenos'
entradilla: Desde el Ministerio de Salud aclararon que toda práctica debe ser realizada
  por los profesionales habilitados para tal fin; y siguiendo el protocolo vigente.

---
El Ministerio de Salud provincial brindó recomendaciones para el correcto uso de las pruebas rápidas para la detección de Coronavirus, que se encuentran disponibles en farmacias de distintas localidades del territorio santafesino.

Al respecto, desde la cartera sanitaria destacaron que toda práctica de detección de antígenos virales de SARS-Cov-2, como así también anticuerpos anti SARS-Cov-2, debe realizarse siguiendo los protocolos vigentes en el contexto del sistema de salud y por profesionales habilitados.

En ese sentido, el director de Bioquímica, Germán Henrich, recomendó “fuertemente que los tests diagnóstico, deben realizarse dentro de un ámbito de la salud y por un bioquímico”. Del mismo modo, remarcó que “a diferencia de las pruebas presuntivas (como puede ser un test de embarazo) este tipo de pruebas se denominan de diagnóstico”.

A su vez, desde la Dirección de Bioquímica destacaron que se trata de actividades reservadas a los bioquímicos y bioquímicas ya que son quiénes garantizan su correcta manipulación, utilización, manejo adecuado de residuos peligrosos, informes de resultados y notificación.

Asimismo, la disposición 9688/2019 de la Administración Nacional de Medicamentos, Alimentos y Tecnología Médica (ANMAT), en su artículo Nº22, destaca “la condición de uso exclusivo a profesionales e instituciones sanitarias corresponde a aquellos productos médicos que, debido a su riesgo intrínseco, o en razón de su modo de uso o a las medidas colaterales necesarias para su uso, no resultan seguras si no son utilizados por un profesional habilitado para utilizar dicho producto de acuerdo con sus incumbencias profesionales (productos de uso profesional exclusivo). Estos productos solo podrán ser vendidos a profesionales e instituciones sanitarias, o bajo prescripción o por cuenta y orden de las mismas”.