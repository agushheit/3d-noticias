---
category: Agenda Ciudadana
date: 2021-07-22T06:15:32-03:00
thumbnail: https://assets.3dnoticias.com.ar/CERTIFICADO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'Paso a paso: cómo tramitar en Argentina el certificado digital de vacunación'
title: 'Paso a paso: cómo tramitar en Argentina el certificado digital de vacunación'
entradilla: Quienes ya recibieron una o las dos dosis de algunas de las vacunas contra
  el covid ya pueden contar con el certificado digital de vacunación

---
La campaña de vacunación continúa a paso firme en todo el país, por eso se están llevando a cabo reaperturas de muchos espacios que estuvieron vedados hasta hace poco tiempo atrás para evitar posibles contagios de coronavirus. Quienes ya recibieron una o las dos dosis de algunas de las vacunas contra el covid ya pueden contar con el certificado digital de vacunación para circular con la constancia de que ya fueron inoculados.

Al momento de la vacunación el personal de salud entrega un comprobante físico con datos como la fecha y el tipo de vacuna, pero existe la opción del certificado digital que es mucho más práctica y puede llevarse en el celular. Para tenerla solo debemos descargar algunas de las aplicaciones del Gobierno y así podremos contar con la documentación que respalda que recibimos una o dos dosis contra el coronavirus.

Los habitantes de la provincia de Buenos Aires pueden descargarse la app Vacunate PBA y allí tener su certificado digital de vacunación. Mientras que quienes residen en otro territorio pueden hacer lo propio con la app Mi Argentina.

Mi Argentina puede descargarse de los sitios habituales que utilizan los sistemas iOS (propio de los aparatos IPhone) y de Android y desde allí tramitar el certificado digital de vacunación.