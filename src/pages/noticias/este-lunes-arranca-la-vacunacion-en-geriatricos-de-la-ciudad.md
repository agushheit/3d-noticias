---
category: La Ciudad
date: 2021-02-22T06:30:52-03:00
thumbnail: https://assets.3dnoticias.com.ar/vacunaadultos.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Este lunes arranca la vacunación en geriátricos de la ciudad '
title: 'Este lunes arranca la vacunación en geriátricos de la ciudad '
entradilla: Según la ministra de Salud, Sonia Martorano, serán 20 o 30 instituciones
  de Santa Fe y otros 80 de Rosario. Quieren terminar el viernes con los 431 geriátricos
  de la provincia.

---
Este lunes comenzará la vacunación en los geriátricos de la provincia. En total serán más de cien instituciones el primer día y el objetivo es terminar el viernes, indicó la funcionaria.

En la provincia existen 431 geriátricos, que abarcan una población de 17 mil personas entre residentes y trabajadores. La ministra reconoció que se trata de “una meta ambiciosa”, sin embargo, señaló que se armaron los equipos necesarios para llegar con los tiempos pautados.

Después de varias postergaciones, la funcionaria confirmó que el lunes, desde las 8, se harán presentes los equipos de vacunación para iniciar la campaña.

El titular de la Asociación de Residencias Geriátricas, Luis López, confirmó el anuncio oficial y dijo: “Nos informaron que este lunes empezaban a vacunar al personal, a los profesionales y a los residentes”.

“Es una gran noticia y como prestadores nos da mucha tranquilidad. Esperamos que la semana que viene la vacunación se haya completado en la mayoría de los geriátricos”, agregó López.

**Los docentes esperan la vacunación**

El Ministerio de Educación de Santa Fe se comprometió a vacunar a los docentes, pero aún no se terminó de inocular al personal de salud, que es el primero en la lista de prioridades, por lo que la fecha para que lleguen las dosis a los maestros y profesores es incierta.

Martín Lucero, secretario general de Sadop, se refirió a esta situación, y también expresó su repudio al hecho de que existan personas privilegiadas que fueron vacunadas cuando todavía no era su turno. “Lo ocurrido con la vacunación VIP a nivel nacional es una falta de respeto a la ciudadanía”.