---
category: Agenda Ciudadana
date: 2021-02-13T06:00:04-03:00
thumbnail: https://assets.3dnoticias.com.ar/perottitostado.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Perotti Inauguró Viviendas En Tostado Y Entregó Más De 200 Millones De Pesos
  Para El Departamento 9 De Julio
title: Perotti Inauguró Viviendas En Tostado Y Entregó Más De 200 Millones De Pesos
  Para El Departamento 9 De Julio
entradilla: Durante una intensa jornada de actividades en esa ciudad, el gobernador
  licitó la construcción de 29 unidades habitacionales y anunció obras de pavimentación.

---
El gobernador de la provincia Omar Perotti visitó este viernes la ciudad de Tostado, en el departamento 9 de Julio, donde entregó cinco viviendas y licitó la construcción de 29 nuevas unidades habitacionales, anunció obras de pavimentación y recorrió el Vivero Municipal, al que entregó un aporte no reintegrable.

Perotti estuvo acompañado en su recorrida por el intendente de Tostado, Enrique Mualem; los secretarios provinciales de Hábitat, Urbanismo y Vivienda, Amado Zorzón; de Prácticas Comunitarias, Ignacio Martínez Kerz; y de Integración y Fortalecimiento Institucional, José Luis Freyre; la subsecretaria de Mujeres, Género y Diversidades, Florencia Marinaro; y el director provincial de Vivienda y Urbanismo (DPVU), José Manuel Kerz, entre otros.

En primer lugar, el gobernador entregó cinco viviendas (ubicadas en Río Negro al 2700) de 1 dormitorio, pertenecientes al Programa Familia en Emergencia Habitacional, cuya construcción demandó una inversión de 8.182.811,20 pesos.

Posteriormente, en una actividad desarrollada en el Club San Lorenzo, encabezó la apertura de sobres con ofertas para la construcción de 29 viviendas, obras que cuentan con un presupuesto oficial de 107.213.006 pesos.

A la licitación se presentaron cuatro empresas que realizaron los siguientes ofrecimientos: la firma Coirini S.A. cotizó los trabajos en $ 107.049.181,11; Constructora Comercial Arquitecto Juan Manuel Campana lo hizo por $ 97.999.587,12; la UT conformada por las firmas MT S.R.L. - Orión Ingeniería S.R.L., que ofertó $ 97.228.494,14; y Capaze S.R.L., que cotizó $ 107.750.000.

**Aprovechar Cada Rincón De La Provincia**

“La verdad que es uno de los días más gratos para todos cuando se puede entregar una vivienda; y también es una alegría para quienes están en la expectativa de tener un día como hoy, como estas familias, porque ven que los programas van marchando, ven que su posibilidad está más cerca”, dijo el gobernador, quien agregó que “todos tenemos que poner el esfuerzo para que esa posibilidad esté más cerca, por eso las gestiones para que el municipio pueda recuperar y tener tierras: es clave para poder llevar adelante estos programas”.

En ese sentido, Perotti insistió en la importancia de “poder aprovechar cada rincón de esta provincia y donde hay un lote que tiene agua, que tiene cloacas, no puede estar vacío. Allí hay infraestructura que se pagó, que se invirtió y que se tiene que aprovechar para algunas familias. Por eso, la prioridad en la construcción con los planes que hemos acordado con el Presidente de la Nación de 4.000 viviendas van a ser en lotes con servicios, para aprovechar esa infraestructura existente”.

“Cuatro mil viviendas son muchas, pero tenemos 100 mil inscriptos en la provincia esperando una casa -explicó Perotti-. Esta es la dimensión del desafío, de lo que se acumuló en años sin poder achicar esa brecha. Y nosotros decidimos empezar, juntos, con el esfuerzo de cada municipalidad, de cada comuna con sus terrenos, de cooperativas, de gremios: el que tenga algo a disposición, hay que sumarlo. Esto es lo que nos permitirá multiplicar la cantidad de soluciones habitacionales con las distintas modalidades, por terrenos, por programas, sean cooperativas, sean mutuales, que quieran estar sumando, para sus afiliados en cada uno de los pueblos o ciudades”.

Asimismo, señaló que durante la jornada también se entregaron “recursos a municipios, comunas e instituciones”, entre otras actividades que, en total permitirán “asignar más de 200 millones de pesos al departamento 9 de Julio, lo que es parte de un proceso fuerte de descentralización y de poner los recursos cerca de la gente, que es donde más rinde y mejor se cuidan”.

**Un Sueño Plasmado**

El intendente Enrique Mualem dio la bienvenida “a las primeras familias beneficiarias en este día especial” y destacó: “Estas cinco viviendas son un sueño plasmado”, y resaltó que “se está trabajando con Hábitat para concretar viviendas para los que menos tienen, los que no tienen la posibilidad de entrar en un sorteo”.

Mientras tanto, Zorzón agregó que en la DPVU “tenemos distintos programas y los estamos implementando, contemplando a las 365 localidades de la provincia y trabajando con los intendentes y presidentes comunales, con las organizaciones, instituciones y vecinos. Desde el gobierno tenemos energía e intenciones para llevar adelante la tarea que nos corresponde, nuestro compromiso es fuerte y tenemos el mandato para lograr la mayor cantidad de soluciones habitacionales, lo que comprende la casa pero también la infraestructura, para que los vecinos accedan también a los servicios”.

Finalmente, Carolina Gauchat, en representación de los beneficiados, agradeció “al gobernador Omar Perotti por el apoyo hacia el intendente que desde el primer momento tuvo como prioridad las obras habitacionales”.

**Lote Propio**

Asimismo, el gobernador entregó actas acuerdo para la construcción de 16 viviendas en Lote Propio. La inversión alcanzará los 48.000.000 de pesos y beneficiará a Logroño, Villa Minetti, Tostado (cada localidad recibió 15.000.000 para la construcción de cinco unidades habitacionales) y Pozo Borrado (recibirá 3.000.000 para una vivienda)

**Dar Respuestas**

El intendente puso de relieve que “una de las falencias graves que tenemos y que constantemente reclama la gente es el tema de la vivienda”, pero “hoy se entregaron casas a empleados municipales, recibieron el convenio para comenzar por cinco viviendas de Lote Propio, y se licitó la construcción de otras 29: es un trabajo conjunto que estamos haciendo con el gobierno de la provincia y es una manera de empezar a tener respuestas a los ciudadanos”.

Por su parte, al referirse al Plan de Lote Propio, Zorzón recordó que data de 1996, durante la gestión de Jorge Obeid, y destacó “la función primordial del Estado para llevar adelante la ayuda, a través de financiamiento a muchas personas que a veces cuentan con un terreno pero no con la posibilidad económica para tener una vivienda. Esto tiene que ser una política de Estado”.

**Pavimentación**

Durante el acto también se anunció la firma del acta de inicio de obras de pavimentación de intertrabado de hormigón y rampas de accesibilidad en la Plaza “Coco Díaz” de la ciudad de Tostado, obras que se concretarán en el marco del Plan de Mejoramiento de la Infraestructura Federal, y que se llevará a cabo a través de la Cooperativa de Trabajo Esfuerzo Rafaelino Ltda. Las tareas demandarán una inversión de 12.163.536,91 pesos.

Finalmente, Perotti visitó el Vivero Municipal, al que entregó un aporte no reintegrable de 800.000 pesos para la reactivación del espacio, monto que será destinado a la contratación de mano de obra, traslado de la huerta agroecológica a cielo abierto, la instalación de un sistema de riego, el mejoramiento del arbolado público y la implantación de un invernadero.