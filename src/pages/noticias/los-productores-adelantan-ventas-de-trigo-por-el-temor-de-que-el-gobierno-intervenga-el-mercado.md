---
category: El Campo
date: 2021-02-23T06:59:31-03:00
thumbnail: https://assets.3dnoticias.com.ar/trigo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: Los productores adelantan ventas de trigo por el temor de que el Gobierno
  intervenga el mercado
title: Los productores adelantan ventas de trigo por el temor de que el Gobierno intervenga
  el mercado
entradilla: A tres meses de que comience la siembra del cereal, ya se vendieron más
  de un millón de toneladas de la nueva campaña, según un informe de la Bolsa rosarina.

---
A más de 90 días de que los productores comiencen a sembrar la nueva campaña de trigo -y a diez meses de la cosecha- ya hay 1,2 millones de toneladas de la campaña 2021/22 vendidas por el temor que genera una posible intervención del Gobierno en el mercado triguero o un incremento en los derechos de exportación.

“El volumen de trigo negociado a la fecha marca un récord absoluto, apuntalado por el temor latente a un incremento en las alícuotas de retenciones o a las restricciones cuantitativas o comerciales de otra índole”, explica un informe de la Bolsa de Comercio de Rosario (BCR).

El volumen de trigo de la nueva campaña que suele estar comercializado para finales de febrero es realmente marginal porque todavía se está comercializando la producción que se terminó de trillar en enero e incluso quedan “stocks de arrastre” de la campaña previa.

El informe de la BCR precisa que de las 17 millones de toneladas del ciclo 2020/21 todavía faltan vender más de 6 millones de toneladas. “Al 11 de febrero se lleva comercializado 10,25 millones de toneladas de trigo de la actual campaña, o un 60% de la producción. Las compras fueron realizadas en su mayoría por el sector exportador, que concentra el 87% del total, quedando el remanente en poder de la industria molinera”, precisa el estudio.

En las últimas semanas, los exportadores desaceleraron sus compras del cereal porque ya cubrieron la mayor parte de lo que se proyecta embarcar este año (compraron casi 9 millones de toneladas de las 10 millones de toneladas previstas).

Al 3 de febrero, la industria molinera había comprado 1,4 millones de toneladas, un 20% de las 6,4 millones que se estima procesarán para el consumo en el mercado interno.

**Las exportaciones**

Los envíos de trigo al exterior en febrero continúan muy por debajo de los años anteriores. Se embarcaron 440.000 toneladas hasta el 10 de febrero, un 50% menos que en 2019.

“En lo que llevamos de la campaña comercial 2020/21, las exportaciones presentan una caída interanual acumulada del 57%. La menor cosecha este año, junto con los conflictos gremiales que complicaron la actividad portuaria, atentó contra el ritmo de embarque”, concluye el informe de la Bolsa rosarina.