---
category: La Ciudad
date: 2021-10-13T06:00:35-03:00
thumbnail: https://assets.3dnoticias.com.ar/BARRIOTRASPORTE.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: En barrio Transporte buscan recuperar la vecinal y lograr el progreso que
  nunca llegó
title: En barrio Transporte buscan recuperar la vecinal y lograr el progreso que nunca
  llegó
entradilla: 'En pleno siglo XXI, calles de tierra y zanjas a cielo abierto caracterizan
  a esta jurisdicción del norte de la ciudad.

'

---
Tres avenidas importantes alojan en su interior a barrio Transporte. Ellas son Aristóbulo del Valle, Facundo Zuviría y Gorriti, que junto con calle Risso delimitan la jurisdicción. Es un barrio "del norte", como catalogan quienes viven entre bulevares a todos los barrios que están a más de 30 cuadras del sector más pintoresco de la capital provincial.

 Y esa marca, esa identificación "del norte", duele. Duele porque el barrio es humilde pero de gente muy trabajadora. Es una jurisdicción que creció en habitantes pero que no fue acompañada con obras, con mejoras para una buena calidad de vida.

 "Vivo en Transporte hace más de 50 años. Yo siento que el barrio se está muriendo de a poco. Estamos muy abandonados. Cómo se explica que todavía tengamos zanjas a cielo abierto y solamente tres calles asfaltadas", dice Miriam Vigil, una vecina.

 En Transporte habitan unas 8 mil personas. Todas sus viviendas fueron construidas por familias que pusieron ladrillo por ladrillo con la idea de tener un hogar propio. Pero sienten que no fueron acompañados, que "el barrio creció guacho, a la buena de Dios".

 Entre las cosas buenas, destacan el servicio de recolección de residuos y las conexiones de gas natural, que no llega a todos pero por lo menos a una buena parte del barrio.

 **Nada de brazos cruzados**

 Debido a la falta de obras y, en líneas generales del acompañamiento estatal, un grupo de vecinos se puso al hombro un propósito: recuperar la vecinal para empezar a gestionar mejoras.

 "Cuando cae un político con el mate y el termo y te prometen muchas cosas, el barrio se indigna. Nos toman por tontos. Nosotros elegimos no indignarnos. Fue así que formamos una agrupación que se llama Unidad Barrial con el objetivo de sacar Transporte adelante. Desde 2017 que no tenemos vecinal, y vamos a recuperarla", cuenta Miriam Vigil.

 Consultada sobre la posibilidad de comenzar de nuevo en otro barrio de la ciudad, la vecina fue más que clara: "Edifiqué mi casa en un terreno que me dio mi papá, que pensaba que esto sería un barrio hermoso con el correr de los años. Un barrio que iba a progresar. Acá crecieron mis hijos. Irme no es una opción. Abrir la ventana un día y ver que las zanjas ya no están sí es mi mayor deseo".

 Un problema cuando llueve. La mayoría de las calles de Transporte son de tierra. En períodos de abundantes lluvias se vuelven intransitables. Foto: Mauricio Garín

 **En sus orígenes**

 El crecimiento poblacional del barrio nació de sucesivos loteos de distintas quintas, como los campos de los Chumilla, los Zanezzi y los Escher. Con él, llegaron los servicios de energía eléctrica, gas natural y agua potable; pero otros continúan siendo una cuenta pendiente, como los desagües pluviales y las cloacas.

 Raúl Guarda es uno de los primeros vecinos de Transporte. Testigo de todo lo que allí ocurrió cuenta: "Cuando se empezaron a lotear las tierras, Transporte empezó a crecer. Por entonces, era un barrio de viviendas dispersas. Por el año 1970, cuando empecé a edificar mi casa, me acuerdo que desde el techo veía animales pastando, molinos de viento y algunas casas a lo lejos. El barrio se hizo gracias a la gente, y sin el apoyo de los gobiernos de turno. A más de 50 años de creado Transporte creemos que el Estado no nos acompañó, nos marginó. Basta con ver la mayoría de las calles de tierra y las zanjas a cielo abierto en pleno Siglo XXI", finalizó Raúl.

 Barrio Transporte debe su denominación a los depósitos y talleres de colectivos urbanos que había en la zona cuando al servicio lo prestaba la Municipalidad, hace varias décadas atrás. Según publicaciones de El Litoral del año 1990, "era el tiempo en que bastaba llamar a las cosas por su nombre, sin eufemismos ni dobles mensajes". De ahí, identificar un barrio con algo que lo caracterizaba: los talleres del transporte.

 **El artista del barrio**

 Gran artista. Sergio Zerdá es músico y uno de los vecinos más queridos del barrio por su carisma. En cuarentena brindo talleres gratuitos de manera virtual. Foto: Mauricio Garín

 Transporte alberga desde hace 30 años a un hombre con muchísimo talento y de un importante recorrido musical por distintos géneros que lo llevó a compartir escenario con diversos artistas conocidos. Se trata de Sergio Zerdá, quien tiene sus raíces en Salta, pero se crió en la ciudad de Santa Fe.

 Sergio comenzó su carrera en el folclore, pasó por el rock y actualmente se encuentra en un rol de productor y componiendo temas propios. Sus estudios en dirección de orquesta y percusión le dieron los conocimientos necesarios para poder saltar de un género a otro.

 El barrio es su hogar. Para él es un lugar tranquilo que cubre sus necesidades...

 En plena cuarentena por la pandemia, cuenta que brindó talleres virtuales de manera gratuita a los fines de que el encierro no sea una pesadilla para nadie. Ahora retomó las clases particulares, con alumnos que llegan desde otros puntos de la ciudad e incluso ciudades vecinas

 En cuanto a su carrera artística cuenta: "Arranque tocando folclore con los grupos locales y después tuve la suerte de tocar varios géneros. Me tocó hacer la gira El Manantial con Marcela Morelo, también con los Toro -Claudio y Facundo-, con Candela Vargas… En cumbia con grandes como Coty Hernández. Y en rock con Sebastián Casís.

 