---
category: Agenda Ciudadana
date: 2021-02-23T07:15:12-03:00
thumbnail: https://assets.3dnoticias.com.ar/alquiler.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Clarín
resumen: 'Tarjeta AlquilAr: cómo es la propuesta que busca no prorrogar el congelamiento
  de alquileres'
title: 'Tarjeta AlquilAr: cómo es la propuesta que busca no prorrogar el congelamiento
  de alquileres'
entradilla: El sector inmobiliario presentó una serie de “propuestas reales para promover
  la normalización del mercado de alquileres”.

---
El sector inmobiliario busca establecer un sistema de cuotas para las deudas y reactivación del ProcreAr. La propuesta apunta a que no se prorrogue el congelamiento de alquileres. “Proponemos que los inquilinos con necesidades para abonar el alquiler puedan ser asistidos por el Estado Nacional a través de una ‘tarjeta AlquilAR’", plantearon a través de un comunicado.

La semana pasada se aprobó un nuevo registro de propiedades alquiladas que generó polémica y revuelo de parte los propietarios. En un comunicado titulado "Los inmobiliarios de Argentina somos parte de la solución", el Consejo Federal de Colegios Inmobiliarios de Argentina (CoFeCI) señaló: “la ‘tarjeta AlquilAR’ funcionaría de la misma manera que por ejemplo la tarjeta AlimentAR”.

En ese sentido, la entidad indicó que la “iniciativa podría ayudar de manera específica a cada inquilino en situación vulnerable, sin perjuicio de distorsiones en todo el sistema de alquileres”.

El CoFeCI elaboró su propuesta en el marco de un plenario realizado en Rosario, Santa Fe.

"En la República Argentina, la pandemia ha transformado la realidad de todos. Los Profesionales Inmobiliarios representados en el Consejo Federal de Colegios Inmobiliarios de Argentina somos conscientes de que hay personas y familias que viven situaciones delicadas, con pérdidas de la fuente laboral, acumulación de deudas y sin acceder a soluciones reales", se detalló en el comunicado.

En el mismo se agregó que "de acuerdo al relevamiento realizado por los distintos Colegios Profesionales, en Argentina, casi el 90% de los inquilinos pagan en término el alquiler, otros lo hacen con retraso y sólo un 5% ha adherido a la propuesta del DNU para prorrogar el pago de sus obligaciones".

"Por lo tanto, desde CoFeCI, entendemos que continuar con la prórroga indefinida del DNU con las actuales condiciones establecidas profundiza la problemática de los inquilinos, propietarios e inmobiliarios, generando una incertidumbre angustiante", enfatizó la propuesta.

En ese sentido, los inmobiliarios indicaron que "una medida excepcional para una emergencia no puede prolongarse en el tiempo porque termina resultando contraproducente respecto a los objetivos por los que fue creada".

Además de una tarjeta "AlquilAR", el Consejo recomendó "aplicar los programas de financiamiento ahora 18 y 24 para aquellos inquilinos que hayan generado deudas durante la prórroga del DNU" que congeló los precios de los alquileres.

Para incentivar la construcción de nuevas vivienda, la propuesta del CoFeCI apunta también a "la eximición de impuestos nacionales y provinciales a las unidades habitacionales de hasta 60m2 destinadas para alquiler, que se construyan por el termino de nueve años", dado que según aclararon "éste sería el aporte fundamental para disminuir el déficit habitacional, generando además mano de obra genuina.

Por último, en su comunicado, el Consejo indicó: "Proponemos que se reactive la entrega de las viviendas de los distintos programas nacionales, como por ejemplo Procrear o similares, que posee miles de unidades habitacionales construidas listas o a terminar para ser habitadas".

"Esto impactaría de manera muy positiva en las familias que siguen pagando alquiler mientras esperan acceder a la vivienda prometida", destacó finalmente.