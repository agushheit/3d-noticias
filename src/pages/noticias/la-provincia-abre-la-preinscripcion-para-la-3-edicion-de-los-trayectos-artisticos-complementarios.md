---
category: Cultura
date: 2021-03-26T07:33:31-03:00
layout: Noticia con imagen
author: ''
resumen: La provincia abre la preinscripción para la 3° edición de los Trayectos Artísticos
  Complementarios
title: La provincia abre la preinscripción para la 3° edición de los Trayectos Artísticos
  Complementarios
entradilla: Desde este viernes, los interesados podrán inscribirse en el sitio web
  de la subsecretaría de Educación Artística del Ministerio de Cultura. El inicio
  de las clases será el 5 de abril.
thumbnail: https://assets.3dnoticias.com.ar/cultura.jpg
link_del_video: ''

---
El gobierno provincial abre, desde este viernes, la preinscripción para la 3° edición de los Trayectos Artísticos Complementarios, cuyo inicio de clases será el 5 de abril. El trámite online podrá realizarse en [https://educacionartisticasantafe.gob.ar/sea/,](https://educacionartisticasantafe.gob.ar/sea/, "https://educacionartisticasantafe.gob.ar/sea/,") sitio web de la subsecretaría de Educación Artística del Ministerio de Cultura.

**TRAYECTOS PROPUESTOS**

**>> Interpretación con objetos: diseño, construcción y puesta en escena**

Se propone un proceso de aprendizaje orgánico que involucre física, psíquica y emocionalmente a los participantes, ya que tanto en la construcción como en la interpretación de un objeto o un personaje, se requiere un aprendizaje eminentemente práctico. Descubrimiento y desarrollo del Pensamiento Dramático, capacidad de resolver en términos de Acción.

Focalización expresiva y emocional. Revalorización del reciclaje de objetos. Nueva Mirada: “Objeto:Escultura”. Escultura en Acción. Relación con el Arte Fluxus. Incorporación de técnicas específicas.

Este trayecto se cursa los jueves, de 15 a 16:30 horas, y está a cargo de Analía Ortiz Céspedes. Por Google Meet.

**>> Improvisación como herramienta de comunicación**

Una propuesta para entrenar la creatividad, acercando herramientas que permitan a les participantes, un mayor conocimiento de sus capacidades a la hora de comunicar. Se trabajará sobre la palabra, la escucha, la gestualidad, la confianza y la aceptación de las ideas propias y de los demás, para que cada situación sean posibilidades de nuevos caminos dentro del teatro, la música, la escritura, la danza. El que se crea, es principalmente un espacio de diversión, fortaleciendo lazos y así permitiendo hacer conscientes las posibilidades y aptitudes tanto grupales como individuales.

Este trayecto se cursa los lunes, a las 15, y está a cargo de Juan Biselli. Por Zoom.

**>> Afroamérica en movimiento: traducciones corporales de saberes, cantos, danzas y mitos africanos llegados a Sudamérica**

A través de danzas, cantos, ritmos, mitos e historias se propone una visita a los legados africanos en América Latina, especialmente en el Cono Sur. Se parte desde el movimiento corporal como un espacio clave, entendiendo que la danza es para todas y todos, o más bien para aquel o aquella que esté dispuesto a sentirla, independientemente de la edad, del color, el origen y las posibilidades corporales. Se concibe un aprendizaje a través del movimiento, el ritmo y el canto, donde las personas puedan vivenciar diferentes estados corporales a partir de las emociones que sugiera cada movimiento, cada gesto y\\o personaje. Se contará con material bibliográfico y audiovisual que permitan sumergirse en la historia de cada práctica, en sus mitos y presentar dichas prácticas en sus contextos.

Este trayecto se cursa los lunes, de 19 a 20:30, y está a cargo de María Laura Corvalán y Betina Pellegrini. Por Google Meet.

**>> Producción ejecutiva de proyectos artísticos**

Abordaje sobre los conceptos generales y las herramientas básicas para la producción ejecutiva de teatros, conciertos, shows, presentaciones, muestras, y otros eventos de acuerdo a las inquietudes de los inscriptos. Reconocimiento de las necesidades de cada lenguaje/disciplina y las estrategias y acciones específicas para la materialización de los proyectos.

En un momento posterior, profundizar en las fases concretas del trabajo: (preproducción - diseño de producción - producción - prensa y difusión - organización del evento - seguimiento)

Este trayecto se cursa los martes, de 10:30 a 12, y está a cargo de Melania Barreiros. Por Google Meet.

**>> Música y escena**

El trayecto está destinado a brindar recursos para la aplicación del sonido en relación con otras expresiones artísticas en el escenario. En el suceder de los encuentros se analizará el sonido y sus elementos, estructuras formales, herramientas de proceso del sonido y recursos y criterios para su aplicación interdisciplinaria en escena.

Este trayecto se cursa los jueves, de 19 a 20:30, y está a cargo de Sergio Aquilano. Por Jitsi.