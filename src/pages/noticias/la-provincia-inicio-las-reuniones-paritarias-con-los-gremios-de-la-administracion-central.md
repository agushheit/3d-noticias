---
category: Estado Real
date: 2021-02-05T06:42:32-03:00
thumbnail: https://assets.3dnoticias.com.ar/gremios.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: La provincia inició las reuniones paritarias con los gremios de la administración
  central
title: La provincia inició las reuniones paritarias con los gremios de la administración
  central
entradilla: El encuentro se desarrolló en el Ministerio de Economía junto a ATE y
  UPCN. “La demostración de nuestra apuesta al diálogo es habernos reunido en la fecha
  pactada”, destacó Pusineri.

---
Los ministros de Trabajo, Empleo y Seguridad Social, Juan Manuel Pusineri, y de Economía, Walter Agosto, encabezaron este jueves la primera reunión paritaria de 2021 con los representantes de los gremios ATE y UPCN.  
  
Luego del encuentro, Pusineri destacó “el cumplimiento que viene sosteniendo el gobierno, en conjunto con los gremios, de los compromisos que va asumiendo. El año pasado y a principio de este año habíamos fijado fecha para las reuniones paritarias y la demostración de nuestra apuesta al diálogo social, a la negociación colectiva, es habernos reunido hoy, en la fecha pactada. Esto hace a la credibilidad y a la buena fe en la negociación colectiva”.  
  
En este sentido, el ministro precisó que se acordó iniciar la semana que viene con “la instrumentación del pase a planta de personal contratado, cronogramas de reuniones jurisdiccionales y titularización de subrogancias”. Además, se puso “en agenda el tema de ir hacia la normalidad de la prestación de servicios, tanto en el regreso a las escuelas como en la administración central”.  
  
Por último, Pusineri se refirió a la política salarial y señaló que “necesitamos tener las referencias nacionales, por eso pactamos una reunión para el próximo viernes”. Asimismo, indicó que “la voluntad del gobierno es tener un acuerdo lo antes posible, porque mientras antes podamos acordar es mejor para los trabajadores y para el Estado”.  
  
Por su parte, el secretario general de UPCN, Jorge Molina, aseguró que durante el encuentro se planteó “la idea que los salarios superen a la inflación, y la instrumentación de tres temas concretos: uno referido al personal contratado, que la semana que viene se va a comenzar a implementar en cada jurisdicción; la confirmación de subrogancias; y la puesta en funcionamiento de las comisiones jurisdiccionales, que significan mesas en cada Ministerio con los autoridades”.  
  
“También hemos hablado de la necesidad de pautar normas en la vuelta a la normalidad laboral de los compañeros que hoy todavía no están trabajando por alguna circunstancia. Es nuestro interés respetar el protocolo y entendemos que tenemos que ir avanzando a una normalidad que nos permita tener una vida mucho más ordenada”, agregó Molina.  
  
En tanto, el representante de ATE, Jorge Hoffman, sostuvo que “fue una buena reunión y un buen comienzo. En el caso de los contratados, podemos decirle a los compañeros que la semana que viene, a través de cada una de las jurisdicciones, se va a comenzar el pase a planta de los trabajadores contratados en la anterior gestión. Esto es muy importante”.  
  
Del encuentro, también participó el secretario Recursos Humanos y Función Pública del Ministerio de Economía, Guillermo Mateo.