---
category: Agenda Ciudadana
date: 2020-12-02T10:37:14Z
thumbnail: https://assets.3dnoticias.com.ar/VACUNACION.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: 'Vacunación contra el Covid: La provincia convoca a voluntarios para comenzar
  la campaña masiva'
title: 'Vacunación contra el Covid: La provincia convoca a voluntarios para comenzar
  la campaña masiva'
entradilla: A partir de este martes, pueden inscribirse los interesados en colaborar,
  tengan o no experiencia. Se trata de un operativo inédito.

---
La provincia de Santa Fe abrió la inscripción a voluntarios y voluntarias que deseen sumarse a la campaña de vacunación contra el Coronavirus, que se lanzará próximamente en las cinco regiones de Salud.

La convocatoria se extiende a personas con o sin experiencia, mayores de 18 años, que sean personal y técnicos de salud y/o estudiantes vinculados a la salud, quienes recibirán la debida capacitación.

Para ello, deberán completar en el siguiente link: nombre, apellido, número de documento, fecha de nacimiento, teléfono de contacto, correo electrónico, dirección, localidad, formación y/o profesión.

El cierre del registro tendrá lugar el 15 de diciembre, fecha a partir de la cuál empezarán las capacitaciones que se organizarán y dictarán en cada una de las cinco regiones.

En relación a la convocatoria, y a la importancia de la campaña, la ministra de Salud, Sonia Martorano, dirigió un mensaje a todos los santafesinos y santafesinas: “Pronto comenzará esta mega campaña de vacunación. Es uno de los desafíos sanitarios más importantes en décadas”, destacó.

Y concluyó: “Esperamos contar con la colaboración y solidaridad de todos y todas quienes puedan hacerlo, porque es la única forma en que vamos a poder llevarla adelante, de la mejor manera, alcanzando en tiempo y forma a la población destinataria”.