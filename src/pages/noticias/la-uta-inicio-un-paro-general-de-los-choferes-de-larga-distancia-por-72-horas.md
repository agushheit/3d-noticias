---
category: Agenda Ciudadana
date: 2021-08-28T06:15:50-03:00
thumbnail: https://assets.3dnoticias.com.ar/UTA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: La UTA inició un paro general de los choferes de larga distancia por 72 horas
title: La UTA inició un paro general de los choferes de larga distancia por 72 horas
entradilla: Mario Calegari, jefe de prensa de la UTA, dijo a Télam que la medida de
  fuerza, iniciada este viernes, se debe "principalmente a que muchas empresas del
  sector no han pagado salarios, deben sueldos y aguinaldos".

---
Los trabajadores nucleados en la Unión Tranviarios Automotor (UTA) iniciaron este viernes un paro nacional de los choferes del sector de larga distancia por 72 horas, en reclamo de aumento de salarios, entre otras demandas, informaron voceros gremiales.

Mario Calegari, jefe de prensa de la UTA, dijo a Télam que la medida de fuerza, iniciada este mediodía, se debe "principalmente a que muchas empresas del sector no han pagado salarios, deben sueldos y aguinaldos", además de "no haber avanzado un acuerdo en las negociaciones salariales en la mesa paritaria".

Agregó que "todo esto lleva ya varios meses de discusión y los trabajadores conductores están en la misma situación".

A través de un comunicado de prensa, la UTA explicó los motivos del paro nacional de los choferes de larga distancia.

"Nuestro pedido ha sido en todo momento claro, pretendiendo únicamente trabajar, y cobrar los salarios que legítimamente nos corresponden. No vamos a permitir salarios de pobreza", señaló el escrito gremial.

Además, desde el gremio, que conduce el histórico dirigente Roberto Fernández, apuntaron contra los empresarios del transporte de pasajeros ante la "total negativa a acordar los sueldos de los trabajadores, y a pretender negociar condiciones de trabajo a cambio de los salarios".

Asimismo, acusaron a los dueños de las líneas de larga distancia por actuar "sin el menor sentido de sensibilidad hacia los trabajadores, que vienen poniéndole el cuerpo al sector de larga distancia para mantener la actividad en el sistema", en el marco de la pandemia de coronavirus.

Desde el gremio acusan a los a los dueños de las líneas de larga distancia por actuar "sin el menor sentido de sensibilidad hacia los trabajadores".

Desde el gremio acusan a los a los dueños de las líneas de larga distancia por actuar "sin el menor sentido de sensibilidad hacia los trabajadores".

Voceros del Ministerio de Trabajo, que conduce Claudio Moroni, informaron a Télam que anoche se realizaron "intensas gestiones" para intentar destrabar el conflicto entre el sector sindical y empresario y señalaron que las negociaciones continuaban este viernes por la mañana sin ningún resultado hasta el momento.

"No vamos a permitir que nos precaricen las condiciones laborales de los compañeros representados, como prenda de cambio ante un justo pedido de aumento salarial", remarcaron desde la UTA y denunciaron que "la gran mayoría de las empresas del sector mantiene impagos los salarios o viene llevando a cabo un mecanismo de pagos parciales".

"Adeudan salarios, aguinaldos, sumas no remuneratorias acordadas, Decreto 14/2020, viáticos, etcétera", detallaron.

Actualmente, el Estado Nacional aporta 600 millones de pesos mensuales al sector a través de la resolución 283/2021 del Ministerio de Transporte, que se suman a los 260 millones de pesos mensuales que reparte el Ministerio de Trabajo en concepto del programa Repro II.

Sin embargo, los trabajadores continúan cobrando en cuotas.

"Todo ello demuestra que estamos ante la desidia de un sector empresario sin escrúpulos, y sin límites", apuntaron desde el sindicato y juzgaron que los empresarios del transporte "parece que pretenden mantener sus márgenes de utilidad en base a la declaración de pandemia, o a costa de la precarización de las condiciones de trabajo".

"Pretenden que manejemos más horas, menos conductores, al margen de la ley y burlando todos los derechos de los trabajadores. Nos pretenden hacer trabajar en exceso de la jornada convencional, violentando las conquistas de décadas por el movimiento obrero organizado", completaron desde la UTA.