---
category: Agenda Ciudadana
date: 2021-02-05T06:55:32-03:00
thumbnail: https://assets.3dnoticias.com.ar/hidrovia.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de El Litoral
resumen: Tensión en el PJ por el negocio de la Hidrovía
title: Tensión en el PJ por el negocio de la Hidrovía
entradilla: 'El senador nacional Jorge Taiana (Frente de Todos- Buenos Aires) advirtió
  que su gobierno "ha concesionado" una estratégica vía fluvial "sin los adecuados
  controles que defiendan el interés nacional". '

---
El senador nacional Jorge Taiana (FdT-Buenos Aires) pidió que se invite al ministro de Transporte Mario Meoni para que informe sobre "todo lo relacionado con la concesión de la modernización, ampliación, operación y mantenimiento del sistema de señalización de la Vía Navegable Troncal" que surca las aguas del Paraná y del Río de la Plata, y además solicitó al Congreso la conformación de una comisión bicameral de seguimiento.

Taiana, titular de la comisión de Relaciones Exteriores del Senado, hizo el pedido a través de un proyecto de comunicación en el que precisó que el pedido de informes a Meoni refiere a la concesión de la llamada Vía Navegable Troncal que está "comprendida entre el kilómetro 1.238 del Río Paraná y la zona de aguas profundas naturales en el Río de la Plata exterior hasta la altura del kilómetro 239,1 del canal Punta Indio, utilizando la ruta por el Canal Ingeniero Emilio Mitre". 

Mencionó 4.600 embarcaciones al año, que trasladan el 75% de las exportaciones del país. En la actualidad, dragado y balizamiento están a cargo de la empresa Hidrovía SA, aunque la concesión tiene vigencia hasta el próximo 30 de abril de 2021. 

Taiana, en suma, pidió al ministro que informe "sobre el futuro llamado a licitación pública nacional e internacional, para la modernización, ampliación, operación y mantenimiento de la citada Vía Navegable Troncal". 

Solicitó precisiones sobre "la construcción del Canal Magdalena" para "contar con una salida directa al mar, desde los puertos fluviales hacia los puertos de nuestro litoral marítimo y viceversa". Cuestiona la transferencia a Meoni de la convocatoria a licitación para la concesión de la vía navegable, a tres meses del fin de la concesión.

Taiana pidió que la Casa Rosada informe sobre la licitación de la vía fluvial Paraná-Río de la Plata-Magdalena. Foto: Archivo 

Consideró que se ha visto "obligado", como senador por la provincia de Buenos Aires, "a solicitarle al señor ministro" información sobre si se ha contemplado en la licitación "el dragado y balizamiento del canal Magdalena" que "ha sido incorporada en el Presupuesto de la Nación para el año 2021, en la Planilla Anexa 2 al Artículo 11", bajo el nombre de Proyecto de Obra salida de Hidrovía al Atlántico por Aguas Argentinas y vinculación Puertos Atlánticos con Fluviales.

 

**El kirchnerismo reclama dar marcha atrás** 

"Dado que la República la Argentina ha concesionado esta estratégica vía fluvial sin los adecuados controles que defiendan el interés nacional, es que dejo también planteada la necesidad de conformación en el Senado de una comisión bicameral de seguimiento de la concesión de la vía fluvial Paraná-Río de la Plata-canal Magdalena", dice en los fundamentos el senador por la provincia de Buenos Aires, Jorge Taiana, sobre su propio gobierno, al fundamentar un pedido de informes que muestra profundas diferencias en el oficialismo. 

De inmediato, la Corriente Nacional Primero La Patria, una agrupación kirchnerista, respaldó su pedido e incluso fue más allá al exigirle a la Casa Rosada dar marcha atrás con el concesionamiento y su llamado a licitación. 

El documento fue divulgado por la Agencia Paco Urondo (también K) y critica "las ambigüedades y omisiones" del decreto 949/2020 del presidente Alberto Fernández que delega en el Ministerio de Transportes el llamado y adjudicación de la licitación pública e internacional, "pasando por encima de la Sociedad del Estado Administradora de Hidrovía Federal S.E. creada en agosto 2020 e integrada por el Estado Nacional (51% del capital social) y las provincias argentinas con ribera sobre el río Paraná: Santa Fe, Buenos Aires, Entre Ríos, Chaco Formosa e incluso Misiones (49% en conjunto)".

Según la agrupación nacionalista "el trasfondo del decreto es la potenciación del Puerto de Montevideo para la salida al mundo de nuestras exportaciones, promovida desde hace años por las grandes empresas extranjeras que controlan nuestro comercio exterior y respaldadas por el gobierno de Mauricio Macri durante su gestión. Ese respaldo implicó dejar sin efecto el acuerdo celebrado en 2015 –segunda presidencia de la Compañera Cristina- en el marco de la Comisión Administradora del Río de la Plata para que todas las exportaciones que vinieran por el Paraná desembocaran en el canal Magdalena, totalmente bajo control nacional", subrayan. 

"Esta vía permitiría la salida directa de nuestras exportaciones al Río de La Plata y el Océano Atlántico, reduciendo sustancialmente los costos al eliminarse el desvío por el Canal Punta Indio y el de acceso al puerto de Montevideo". 

Dice que "frente a la nueva embestida de la derecha neoliberal y los monopolios exportadores, históricos continuadores del imperialismo hoy metamorfoseado en la globalización financiera, manifestamos nuestro apoyo militante al pedido de informes de Jorge Taiana".

 

**La administradora**

Hacia el final de los considerandos, Taiana planteó que el ministro Meoni debe informar "las razones por las cuales se desestimó la creación de la Administradora Federal Hidrovía Sociedad del Estado", e inmediatamente agregó: "Hemos tomado conocimiento de declaraciones públicas de representantes de las empresas exportadoras, manifestando su oposición a la creación de esa empresa estatal". 

Tras lo cual Taiana recordó que en agosto de 2020, tres meses antes del decreto que delegó en la cartera de Transporte la licitación sobre los canales fluviales, "se concretó el acuerdo federal Hidrovía, realizado en la provincia de Santa Fe, cuyo objetivo era establecer la creación de la Administradora Hidrovía Sociedad del Estado, integrada por el Estado Nacional con una participación del 51% del capital social, y por las provincias de Buenos Aires, Corrientes, Chaco, Entre Ríos, Formosa, Misiones y Santa Fe, que retendrían un 49% de la futura sociedad estatal".