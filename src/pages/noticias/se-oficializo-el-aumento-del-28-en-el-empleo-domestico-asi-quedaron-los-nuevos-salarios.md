---
category: Agenda Ciudadana
date: 2020-12-15T12:38:51Z
thumbnail: https://assets.3dnoticias.com.ar/empleadas-domesticas.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: El Litoral'
resumen: 'Se oficializó el aumento del 28% en el empleo doméstico: así quedaron los
  nuevos salarios'
title: 'Se oficializó el aumento del 28% en el empleo doméstico: así quedaron los
  nuevos salarios'
entradilla: El incremento será en tres etapas: 10% a partir del 1° de diciembre; 8%,
  no acumulativo, a partir del 1° de febrero de 2021; y 10%, no acumulativo, a partir
  del 1° de abril de 2021.

---
El Gobierno Nacional oficializó, a través de la Resolución 3/2020 publicada este martes en el [Boletín Oficial](https://es.scribd.com/document/488206184/aviso-238517  "Boletín Oficial"), el aumento del 28% sobre las remuneraciones horarios y mensuales mínimas en el empleo doméstico.

**Este aumento salarial se hará en tres etapas:** 10% a partir del 1° de diciembre; 8%, no acumulativo, a partir del 1° de febrero de 2021; y 10%, no acumulativo, a partir del 1° de abril de 2021.

En la Resolución, se aclara que estos nuevos valores salariales deben aplicarse en todo el país y que "las remuneraciones establecidas en la presente mantendrán su vigencia hasta tanto no sean reemplazadas por las fijadas en una nueva resolución".

Este incremento del 28% sobre la escala salarial fue dictaminado en concordancia entre las representaciones sectoriales de trabajadores, de empleadores y de los ministerios integrantes. "El plazo de vigencia del acuerdo alcanzado se extiende desde 1 de diciembre de 2020 y hasta el 31 de mayo del 2021", aclaran.

**Así quedaron los nuevos salarios del empleo doméstico:**

**📰 Escala salarial  |** [Abrir documento](https://www.scribd.com/document/488206171/anexo-6195909-1 "escala salarial")

**📰 Boletín oficial |** [Abrir documento](https://es.scribd.com/document/488206184/aviso-238517 "Boletín Oficial")