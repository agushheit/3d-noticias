---
category: La Ciudad
date: 2021-01-10T11:10:47Z
thumbnail: https://assets.3dnoticias.com.ar/100121-santa-fe.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: '3DNoticias - Fuente: El Litoral'
resumen: Santa Fe restringirá el tránsito de madrugada
title: Santa Fe restringirá el tránsito de madrugada
entradilla: La medida entrará en vigencia desde este lunes en consonancia con el decreto
  nacional 4. El gobernador escuchó a intendentes y presidentes comunales.

---
**El gobernador Perotti firmó este sábado el decreto para restringir la circulación en territorio santafesino para actividades no esenciales a partir del lunes**. 

La medida -que tiene como base el decreto 4 del presidente de la Nación- dispone el **cierre sanitario nocturno de domingos a jueves de 0.30 a 6 horas. Y los viernes, sábados, domingos y vísperas de feriado de 1.30 a 6 horas.** 

El decreto determina que las actividades habilitadas deberán ajustar sus horarios de funcionamiento a ese esquema, excepto que se trate de aquellas definidas como esenciales en la emergencia.

La firma del decreto llega el día después de las reuniones virtuales que el viernes mantuvo Perotti con el comité de expertos en salud y luego con intendentes y presidentes comunales para acordar las restricciones. En ambos encuentros estuvo acompañado, entre otros, por la ministra de Salud, Sonia Martorano.

Perotti insistió en señalar ante intendentes: «hemos hecho esfuerzos importantes que queremos cuidar. Fortalecimos el sistema de salud en todas las localidades, aumentando considerablemente el número de camas». Admitió que «hay una percepción de relajamiento con los cuidados básicos. Nosotros vamos en otro sentido. Tenemos que reforzar las conductas de prevención, optimizando mensajes y acciones. Por eso, buscamos lograr horarios consensuados y vamos a generar todas las acciones de concientización necesarias. El objetivo es mantener el mayor nivel de actividad posible restringiendo circulación nocturna. Tenemos que seguir apuntalando la responsabilidad del cuidado».

El titular del Poder Ejecutivo explicó que «lo que hoy se evaluó y está sujeto a revisión hasta tanto se defina el contenido del decreto, es la posibilidad que de domingo a jueves las actividades se extiendan hasta las 00:30 horas, y los viernes y sábados hasta la 01:30 horas en toda la provincia. Además, **se habilitaría a los municipios y comunas a tener la facultad de determinar su propio horario de culminación de actividades**. También se avanzó en que, en el caso de que requieran hacer restricciones según la situación epidemiológica de su localidad, puedan hacerlo».

Perotti añadió que «el Ministerio de Salud también podría hacer observaciones sobre la base de criterios epidemiológicos sobre la situación de tal o cual lugar y en virtud de ello podría solicitar medidas restrictivas para esos lugares».

Respecto a la restricción de circulación durante la noche, el mandatario resaltó que «hubo un pedido unánime de los intendentes y presidentes comunales para que el Ministerio Público de la Acusación (MPA) y la Justicia acompañen los controles que realicen las fuerzas de seguridad de la provincia».

A su turno, Martorano hizo hincapié en el operativo de vacunación. «Estamos preparados para ponerlo en marcha de nuevo en toda la provincia de Santa Fe con la colaboración de los municipios y comunas, a partir de un invalorable esfuerzo en conjunto. Nos interesa continuar observando cómo sigue la curva de casos y evaluar como continuar en ese contexto», señaló la ministra de Salud.