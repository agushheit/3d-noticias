---
category: La Ciudad
date: 2021-01-22T04:06:01Z
thumbnail: https://assets.3dnoticias.com.ar/museo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad de Santa Fe
resumen: Santa Fe capital tendrá su Pantalla Pública para el cine
title: Santa Fe capital tendrá su Pantalla Pública para el cine
entradilla: 'En el Auditorio Federal del Museo de la Constitución, viernes 22 de enero,
  a las 19.30, se proyectará Balnearios, como parte de un ciclo que continuará durante
  enero y febrero. Entrada gratuita y se deberá reservar.

'

---
En una ciudad que décadas atrás supo tener más de 25 salas de cine funcionando en distintos barrios y que actualmente cuenta con pocos espacios de exhibición, la Municipalidad se propone generar una pantalla para la difusión del cine no comercial, con ciclos pensados en torno a distintos temas, que contemple también el rescate del valioso patrimonio audiovisual que tiene Santa Fe. “Pantalla Pública” es el nombre de esta iniciativa que se llevará a cabo durante este año, en el Auditorio Federal del Museo de la Constitución, aprovechando sus condiciones técnicas, el acondicionamiento de la sala y su generosa espacialidad, que permitirá garantizar el distanciamiento físico que exigen los protocolos sanitarios.

El objetivo de fondo, detalla el secretario de Educación y Cultura de la Municipalidad, Paulo Ricci, “es generar y visibilizar que la ciudad puede tener una pantalla más para las expresiones audiovisuales, que sea pública”, retomando un objetivo que ya se había planteado con “Ventana al río”, el ciclo que se anunció en marzo del 2020 como parte de las actividades por el Mes de las Mujeres y Disidencias, y que no se pudo llevar a cabo por el aislamiento obligatorio decretado en el inicio de la pandemia.

La intención es generar programaciones de manera colaborativa con el Cine Club Santa Fe y otras instituciones que tengan interés en las artes audiovisuales para darle a la capital una pantalla grande donde se puedan ver ficciones, no ficciones, documentales, retrospectivas, rescates cinematográficos; y para que esa pantalla difunda el cine que no tiene distribución comercial, que en Santa Fe no se podría ver si no fuera por la tarea que realiza Cine Club en su propia sala del Cine América.

**A pie o en bici**

En esta oportunidad se trabajó también junto al colectivo Santa Fe en Bici, como ocurrió en otros eventos, para generar un “sendero seguro” que podrán transitar quienes lleguen caminando o en bicicleta hasta el espacio ubicado en Avda. Circunvalación y 1° de Mayo.

Para eso se acordó la presencia de agentes de tránsito y se delimitará una ciclovía temporal que inicia sobre la calzada, en 1º de Mayo y J. J. Paso, de manera que la vereda existente quede disponible para peatones.

Por otra parte, se recuerda que en el interior del predio se cuenta con bicicleteros para amarrar las bicis mientras se disfruta de la proyección, así como la presencia de integrantes de la Guardia de Seguridad Institucional.

**Historias de otros veranos**

El ciclo que permite retomar esta iniciativa de la gestión municipal empieza el viernes 22 de enero, a las 19.30, con la proyección de Balnearios (Argentina, año 2002, duración 80 min), de Mariano Llinás. En las claves del falso documental, el guionista y director construye “una extravagante y risueña enciclopedia de costumbres e historias de los balnearios de la Argentina. Ciudades sumergidas, bañeros, hoteles de principios de siglo, sirenas, barquilleros, diques, balnearios municipales, animales marinos y castillos de arena, se encuentran en un ensayo variado y desconcertante”, como reseña el sitio Filmaffinity, donde se mencionan también las intervenciones de Verónica Llinás, Mario Mactas, Alejandro Zucco y César Zucco.

La programación continúa el 29 de enero con “My summer of love” (2004), de Paweł Pawlikowski. Los viernes de febrero se proyectarán: “Las vírgenes suicidas” (1999), de Sofía Coppola; “500 days of summer” (2009), de Marc Webb; “Y tu mamá también” (2001), de Alfonso Cuarón; y “El verano de Kikujiro” (1999), de Takeshi Kitano.

Detrás de esta selección está la idea de ofrecer al público “un ciclo que sea de calidad, de cine de autor, de distintas nacionalidades y épocas, que tienen al verano como protagonista, porque la historia transcurre y se resuelve en un verano en particular o en varios, con temáticas que tienen que ver con esta estación del año”, contó Ricci.

Siete películas para disfrutar en una temporada en la que el municipio trabaja para generar espacios y alternativas para prevenir las aglomeraciones. En esa línea, el secretario de Educación y Cultura señaló que “es importante avanzar en este proyecto que teníamos de generar una pantalla pública y ofrecer la alternativa de ir a un lugar que tal vez no es tan típico de la temporada. Estar en una linda sala acondicionada y disfrutar de una buena película es una manera de pasar el verano y por eso decidimos que las películas cuenten historias de otros veranos”.

| --- |
|  |

**Continuidad**

Género, derechos humanos, documentales y cine santafesino estarán entre los ejes de los próximos ciclos que se desarrollarán a partir de marzo, para sostener una cartelera que proponga hasta dos funciones semanales.

Para esos ciclos específicos se trabajará en colaboración con otras entidades e instituciones como embajadas, para programar cine de distintas nacionalidades que no tienen la posibilidad de estrenos comerciales y por eso requieren de otro tipo de circuitos, anticipó Paulo Ricci. “También queremos que tenga lugar el cine de realizadores locales contemporáneos; y propiciar el rescate de la historia del cine documental y de ficción santafesino, para poder dar cuenta de muchos materiales que no se vieron en estos últimos meses o que directamente nunca llegaron a la pantalla grande”.

**Reserva de entradas y protocolo**

La entrada es gratuita, con capacidad limitada de la sala, por lo que deberá reservarse previamente en la sección [Pantalla Pública ](https://lt.dptagent.com/22e208a3d0c4e8f312d85b15be5b953d-919442e3fb9157317d319f740d4c9753)de la plataforma Capital Cultural.

En el ingreso al auditorio se solicitarán los datos personales para corroborar la reserva del lugar. También se procederá a tomar la temperatura y sanitizar las manos.

Una vez en el interior, se exigirá el uso de tapabocas durante todo el tiempo de permanencia. Se solicitará también atender las indicaciones del personal de sala que se encargará de ubicar al público atendiendo a la modalidad de ocupación denominada “burbujas sociales de recreación”, que permite agrupar hasta seis personas que concurren juntas y mantener la distancia física entre grupos de personas no vinculadas.