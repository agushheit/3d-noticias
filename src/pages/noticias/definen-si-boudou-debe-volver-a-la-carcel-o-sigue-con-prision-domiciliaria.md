---
category: Agenda Ciudadana
date: 2020-12-13T13:45:13Z
thumbnail: https://assets.3dnoticias.com.ar/boudou.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: NA'
resumen: Definen si Boudou debe volver a la cárcel o sigue con prisión domiciliaria
title: Definen si Boudou debe volver a la cárcel o sigue con prisión domiciliaria
entradilla: El magistrado le otorgó este viernes 24 horas a la asesoría de menores
  de la Defensoría General para que remita un informe sobre la situación familiar.

---
Luego del fallo de la Corte Suprema de Justicia que **dejó firmes las condenas en la causa Ciccone**, el juez Daniel Obligado tendrá que **definir en breve si el exvicepresidente Amado Boudou debe volver a la cárcel o continuar cumpliendo prisión domiciliaria**.

El magistrado le otorgó este viernes 24 horas a la asesoría de menores de la Defensoría General para que remita un informe sobre la situación familiar de Amado Boudou, una de las cuestiones que debe analizar el magistrado antes de tomar la decisión.

Sucede que **uno de los argumentos de la defensa del exministro de Economía fue que su esposa estaba sola sin ayuda al cuidado de dos hijos menores**, unos mellizos de dos años, por lo que sus abogados consideran que tiene que seguir cumpliendo su sentencia alojado en su casa, priorizando así los Derechos del Niño.

Además, el juez Obligado escuchará las opiniones de las partes acusadoras, la Unidad de Información Financiera y la Oficina Anticorrupción.

Posteriormente, estará en condiciones de **resolver si Amado Boudou tiene que volver a prisión o no**, para cumplir la **condena de cinco años y 10 meses de prisión** que se le dictó en 2018.