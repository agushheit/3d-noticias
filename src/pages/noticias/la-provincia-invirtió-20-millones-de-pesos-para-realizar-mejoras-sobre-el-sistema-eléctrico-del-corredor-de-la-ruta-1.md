---
layout: Noticia con imagen
author: 3DNoticias
resumen: Electricidad Ruta 1
category: Estado Real
title: La provincia invirtió 20 millones de pesos para realizar mejoras sobre el
  sistema eléctrico del corredor de la Ruta 1
entradilla: La EPE ejecutó obras y mantenimiento en el marco del plan anual,
  para atender el pico de la demanda estacional.
date: 2020-11-25T12:30:19.961Z
thumbnail: https://assets.3dnoticias.com.ar/ruta1.jpeg
---
La Empresa Provincial de la Energía (EPE) desplegó un conjunto de obras de mejoras y mantenimiento sobre las regiones del corredor de la Ruta Provincial N°1 que componen parte del Gran Santa Fe y abarcan a Colastiné Norte, Rincón, Arroyo Leyes y Santa Rosa de Calchines, para lo cual destinó $ 20.000.000.

El presidente de la EPE, Mauricio Caussi, afirmó que **este subsistema eléctrico regional es uno de los que más crecimiento tuvo en los últimos años y por su topografía necesita una intervención más profunda, para el correcto funcionamiento del abastecimiento de energía**.

Caussi señaló que “durante este año, en el marco del Plan de Mantenimiento y Mejoras, la empresa levantó en esa región más de 340 columnas de hormigón armado y postes de plástico reforzado con fibra de vidrio, que sostienen líneas aéreas de media y baja tensión”.

Además, indicó Caussi, “esta zona a partir de este año se alimenta desde la Estación Transformadora Rincón y tiene como alternativa de provisión la Estación Calchines, generando un sistema duplicado de abastecimiento”.

En lo que respecta a la construcción de redes de baja tensión, domiciliarias, se renovaron y construyeron 3.500 metros de líneas con cables preensamblados, reemplazando a los circuitos convencionales.

También **se instalaron 14 equipos seccionadores en el sistema de media tensión en las distintas localidades**, que posibilitarán una mejor operación del servicio, acotando las zonas con eventuales fallas, para una rápida reposición del suministro.

En lo que va del año, se solucionaron más de 500 anomalías en el sistema de distribución regional: 149 en Colastiné Norte, 155 en Rincón, 100 en Arroyo Leyes y 107 en Santa Rosa de Calchines.