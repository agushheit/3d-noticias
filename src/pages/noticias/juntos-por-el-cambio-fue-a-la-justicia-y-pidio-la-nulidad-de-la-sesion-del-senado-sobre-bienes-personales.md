---
category: Agenda Ciudadana
date: 2021-12-31T06:15:09-03:00
thumbnail: https://assets.3dnoticias.com.ar/jxc.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Juntos por el Cambio fue a la Justicia y pidió la nulidad de la sesión del
  Senado sobre Bienes Personales
title: Juntos por el Cambio fue a la Justicia y pidió la nulidad de la sesión del
  Senado sobre Bienes Personales
entradilla: 'Los senadores nacionales del interbloque presentaron un amparo contra
  la Cámara alta, con el argumento de que el Frente de Todos no logró quórum para
  iniciar la sesión dentro del tiempo establecido por el reglamento.

'

---
El interbloque de senadores de Juntos por el Cambio presentó hoy un amparo ante el Poder Judicial para pedir que declare la nulidad de la sesión que realizó el Senado el último miércoles y en la que se aprobaron los cambios sobre el impuesto a los Bienes Personales.

La presentación fue llevada adelante por el jefe del interbloque, Alfredo Cornejo, y los presidentes de bloque Luis Naidenoff (UCR); Humberto Schiavoni (Frente PRO); Juan Carlos Romero (Justicialista 8 de Octubre); Lucila Crexell (Movimiento Neuquino); Ignacio Torres (Integración y Desarrollo Chubutense) y Beatriz Ávila (Justicia Social).

En el amparo, que recayó en el Juzgado Contencioso Administrativo N° 5, pidieron que el tribunal "declare la nulidad absoluta de la sesión especial de la Cámara de Senadores llevada cabo el día 29 de diciembre de 2021" porque "fue llevada adelante en abierta y franca violación al art. 15 del Reglamento de la Cámara de Senadores".

Los senadores explicaron en la presentación que ese artículo del reglamento establece que luego del horario de citación a una sesión, hay treinta minutos para comenzar y que "al momento cumplirse el límite estipulado, la Cámara no contaba con el quórum exigido para llevar adelante la sesión".

"Tal como es de público conocimiento y como ha quedado registrado en la filmación de la sesión especial (que se ofrece como prueba), vencido el plazo de los treinta minutos no se llegó a reunir el quorum que exige el Reglamento. Pese a ello, varios minutos luego de vencido el plazo, la presidencia dio comienzo a la sesión, momento en el cual sí había quorum", indicaron en el amparo.

En la misma línea, los senadores de la principal bancada opositora sostuvieron que "queda claro que la conducta de la presidenta del Senado una vez más se aparta del Reglamento y obra arbitrariamente, vulnerando el procedimiento constitucional de formación y sanción de las leyes".

Los referentes de Juntos por el Cambio afirmaron que "la presente acción no obedece al eventual malestar que pudiera ocasionar un resultado adverso a las pretensiones de una fuerza política, ni a una cuestión meramente formal, sino que encuentra su basamento en uno de los aspectos sustanciales del procedimiento parlamentario, como lo es el quorum, y que en esencia está vinculado con la necesidad de contar con una mayoría mínima representativa de la voluntad popular".

Junto al amparo los senadores presentaron a modo de prueba el decreto parlamentario 53-21 con el cual se convocó a sesión especial para el 29 de diciembre a las 15, el video de la sesión y el reglamento de la Cámara alta, entre otras.