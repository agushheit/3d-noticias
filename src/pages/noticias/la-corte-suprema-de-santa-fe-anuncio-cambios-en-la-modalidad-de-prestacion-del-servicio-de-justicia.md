---
category: Agenda Ciudadana
date: 2020-12-02T10:45:31Z
thumbnail: https://assets.3dnoticias.com.ar/corte.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: El Litoral'
resumen: La Corte Suprema de Santa Fe anunció cambios en la modalidad de prestación
  del servicio de justicia
title: La Corte Suprema de Santa Fe anunció cambios en la modalidad de prestación
  del servicio de justicia
entradilla: Se habilitó la realización de todo tipo de audiencias, ya sea presencial
  o remota, y se autorizaron las notificaciones entre profesionales y la presentación
  de demandas en lote.

---
La Corte Suprema de Justicia de Santa Fe, en reunión de acuerdo del día de la fecha, dispuso nuevas medidas para mejorar y ampliar la prestación del servicio de justicia en todo el territorio provincial en el marco de las nuevas medidas anunciadas por los gobiernos nacional y provincial en relación a la pandemia de Covid-19.

***

#### Descargar [**ACTA**](https://assets.3dnoticias.com.ar/acta.pdf)

***

En tal sentido, se decidió profundizar la utilización de las herramientas tecnológicas que permitan realizar todo tipo de audiencias y en todos los fueros, pudiendo éstas efectuarse de manera presencial o por vía remota, según los casos.

También se decidió habilitar las notificaciones entre profesionales al igual que la presentación de demandas en lote utilizando el sistema de Autoconsulta online. Ambas funcionalidades estarán disponibles a partir del 9 de diciembre próximo.

***

#### Descargar [**ACTA**](https://assets.3dnoticias.com.ar/acta1.pdf)

***

En cuanto al horario de oficina y atención en todo el ámbito los Tribunales de Santa Fe; de las circunscripciones Rosario y Venado Tuerto; de los circuitos judiciales de Santo Tomé, Laguna Paiva, San Carlos Centro; del distrito Judicial Esperanza y Rafaela, comprendiendo este último los circuitos judiciales de San Vicente y Sunchales, comenzarán a regir a partir del 3.12.2020 nuevos esquemas de trabajo con franjas horarias desdobladas en turnos matutinos y vespertinos.