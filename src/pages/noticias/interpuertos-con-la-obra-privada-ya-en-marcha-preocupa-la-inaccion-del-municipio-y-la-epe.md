---
category: La Ciudad
date: 2021-11-20T06:15:21-03:00
thumbnail: https://assets.3dnoticias.com.ar/interpuertossi.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Interpuertos: con la obra privada ya en marcha, preocupa la inacción del
  municipio y la EPE'
title: " Interpuertos: con la obra privada ya en marcha, preocupa la inacción del
  municipio y la EPE"
entradilla: 'En la sesión del Concejo Municipal de este viernes se aprobó una iniciativa
  para que el Ejecutivo remita información sobre el Plan Maestro del Parque Logístico
  Interpuertos en el noroeste de la ciudad.

'

---
El Concejo Municipal aprobó este viernes una solicitud para que el municipio remita información sobre el Plan Maestro del Parque Logístico Interpuertos, el proyecto que permitirá transformar un sector de noroeste de la ciudad en un moderno centro de transferencia de cargas y donde la obra privada ya está en marcha, aunque se desconocen los pasos que prevé dar el Municipio para favorecer el desarrollo del lugar.

El pedido se suma a otro que fue aprobado durante la anterior sesión, en el que se solicitó al Ejecutivo local que realice las gestiones correspondientes para que la EPE concrete las obras necesarias para dotar de energía eléctrica y reforzar la potencia instalada en el sector Noroeste de la ciudad, en particular, para atender las necesidades de demandas de industrias, distribuidores y empresas comercializadoras y de servicios ubicadas en el Mercado de Abasto, Parque Industrial Los Polígonos e Interpuertos.

El pedido de información de esta semana fue presentado también por el interbloque de concejales de Juntos por el Cambio. En el recinto, el concejal Carlos Pereira (UCR-Juntos por el Cambio) se mostró preocupado porque “el Municipio no ha realizado inversiones ni parece haber definido claramente cuestiones centrales para el desarrollo de este Parque Logístico, como es el plan de desagües y drenajes de todo el sector; ingresos y egresos a los lotes del Parque; iluminación, y otras cuestiones relacionadas con la configuración urbana”.

En ese sentido, recordó que “Interpuertos es una iniciativa del propio Estado Municipal; hay inversores privados que han confiado y que ya están invirtiendo una importante cantidad de recursos económicos que servirán para la generación de empleo y desarrollo de nuestra ciudad”. Pese a eso, Carlos Pereira remarcó que “el Municipio no sólo no está realizando las obras que debería hacer en virtud de los compromisos asumidos por las ordenanzas vigentes, sino que ni siquiera están claros los parámetros de desarrollo del lugar, indispensable para que los privados puedan avanzar con las obras edilicias”.

Vale destacar que la empresa Pilar S.A. fue la primera en adquirir un lote en el Parque Logístico Interpuertos y está llevando adelante una importante inversión que no sólo potenciará ese sector de la ciudad sino que, y fundamentalmente, da trabajo a una importante cantidad de santafesinas y santafesinos, y lo continuará haciendo en el futuro cuando las empresas que allí se instalen empiecen a funcionar.

**De qué se trata**

El Parque Logístico Interpuertos estará ubicado sobre la circunvalación, en la continuidad de Teniente Loza y también de Monseñor Rodríguez, frente al Area Industrial Los Polígonos.

Se trata de una ubicación estratégica: presenta accesos directos a las Rutas Nacionales 9, 11, 18, 34 y 19 y por situarse en este lugar, los empresarios transportistas coinciden en llamarla “La rotonda del país”. El proyecto del parque logístico fue elaborado hace algunos años en forma conjunta por el Gobierno de la Ciudad de Santa Fe, con la Asociación Autotransportes de Carga (AAUCAR) y la Asociación Argentina de Transportistas de Hacienda.

En mayo de 2014 el Concejo Municipal sancionó la Ordenanza N° 12094, que creó el Parque Logístico “Interpuertos” y sometió la gestión del mismo y la venta de los lotes a los mismos procedimientos establecidos en la Ordenanza 11730 del área Industrial Los Polígonos.

“Hasta donde es de conocimiento público, sabemos que dos de estos lotes ya han sido vendidos, uno a un inversor privado y otro a la propia entidad AAUCAR. Asimismo, tenemos entendido que los montos fueron cobrados y han ingresado a rentas generales del Municipio”, señaló Carlos Pereira, que advirtió también que “por vigencia de la Ordenanza 11730 (que creó el Area Industrial Los Polígonos pero aplica sus mismas cláusulas a Interpuertos) el total del producido de las ventas de las tierras debe ser invertido en el propio sector, cosa que hasta ahora no se ha evidenciado”, concluyó.