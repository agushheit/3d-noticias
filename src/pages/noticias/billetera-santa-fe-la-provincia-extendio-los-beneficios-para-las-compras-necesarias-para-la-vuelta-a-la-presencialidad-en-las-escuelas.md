---
category: Estado Real
date: 2021-02-05T06:50:53-03:00
thumbnail: https://assets.3dnoticias.com.ar/billeteravirtual.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: 'Billetera Santa Fe: extendieron los beneficios en compras escolares para
  la vuelta a la presencialidad'
title: 'Billetera Santa Fe: La provincia extendió los beneficios en compras escolares
  para la vuelta a la presencialidad'
entradilla: Se reintegrará el 30% entre el 15 de febrero y el 15 de marzo, durante
  todos los días, en librerías adheridas; y al mismo tiempo se ofrecerá una canasta
  escolar a precio sugerido de 20 artículos a 2490 pesos.

---
El Gobierno de la provincia de Santa Fe, a través de los Ministerios de Producción, Ciencia y Tecnología y de Educación, lanzó este jueves una extensión de los beneficios del Programa Billetera Santa Fe para otorgar promociones en las compras de artículos escolares de cara al reinicio de clases en todo el territorio.

La iniciativa comprende la extensión de los beneficios de la Billetera Santa Fe en comercios del rubro a todos los días de la semana (hoy rige para lunes, martes y miércoles), lo que implica el reintegro del 30% entre el 15 de febrero y el 15 de marzo; y al mismo tiempo se ofrecerá una canasta escolar a precio sugerido de 20 artículos a 2490 pesos en librerías adheridas.

Del acto de lanzamiento, llevado a cabo en una librería céntrica de la ciudad de Santa Fe, participó el ministro de Producción, Ciencia y Tecnología, Daniel Costamagna, el secretario de Comercio Interior y Servicios, Juan Marcos Aviano, el subsecretario de Educación Secundaria, Gregorio Vietto, el gerente general de Plus Pagos, Alberto Murad y representantes del centro comercial local.

El ministro Costamagna destacó que “es un gran desafío de Billetera Santa Fe” y una “apuesta importante, que tiene que ver con el conocimiento” para acompañar “la vuelta a clases, porque lo útiles son elementos imprescindibles para la canasta de toda la familia”.

Sobre Billetera Santa Fe, resaltó que “este proyecto se propone fuertes reintegros, para reactivar y dinamizar el consumo; y acompañar al comercio en un año complejo”; y precisó que “estamos trabajando para avanzar en productos sensibles, cómo el caso de las carnes; y con la buena noticia de la cantidad de usuarios, que es importante, y está creciendo día a día”.

Por último, Costamagna invitó a “los comercios a que se sigan adhiriendo, lo que nos va a permitir tener funcionando a pleno este programa, en todo el territorio de la provincia, para que todos puedan aprovechar este reintegro que no tiene precedentes. Es un esfuerzo enorme del gobernador y de la gestión de gobierno” cerró el ministro.

En cuanto a las expectativas del acuerdo realizado que va a sumar a la canasta familiar de cada santafesino que tenga niñas, niños y/o adolescentes en edad escolar, Gregorio Vietto, subsecretario de Educación Secundaria, remarcó que "esto es parte de un trabajo coordinado entre los ministerios. Estamos cumpliendo con algo que el gobernador nos pidió que es trabajar juntos y este programa es importantísimo para la línea de acción "todas las chicas y los chicos en la escuela aprendiendo". Por lo tanto, políticas como estas se suman al Boleto Educativo Gratuito, al programa Escuela Segura y a todos los esfuerzos que hoy desde el ministerio de Educación estamos realizando para que este año podamos tener un regreso seguro en las escuelas.”

La canasta que se pondrá a disposición incluye: Lápices de color por 12 unidades, bolígrafo, lápices de grafito, Voligoma, mapas Rivadavia Nº 3, bolígrafo Bic surtido, crayones, kit escolar 3 pinceles, témpera por 12 unidades, cuaderno tapa flexible  por 42, cuaderno tapa dura araña por 42, repuesto rayado - cuadriculado por 400 hojas, repuesto hojas Nº 3 rayado - cuadriculado por 288, rapel glacé lustre x 10 hojas, papel glacé metalizado por 10 hojas, goma tinta – lápiz, tijera, marcadores escolares, carpeta escolar, lápiz corrector punta metálica, sacapuntas, mochila lisa escolar, cartuchera y cuadernillo.

Detallando las características de la canasta, Aviano destacó: “La canasta a 2490 pesos ronda un 20% por debajo del costo promedio que detectamos en base a relevamientos que realizaron los inspectores de nuestra Secretaría en librerías de las ciudades de Santa Fe y Rosario”.

Asimismo, el secretario de Comercio Interior informó: “A partir de mañana se inicia un proceso de inscripción para aquellas librerías que van a promocionar la canasta escolar y a su vez, en paralelo se trabajará con Plus Pagos para aumentar las bocas de ventas del rubro. Al momento son 35 las librerías que están adheridas a Billetera Santa Fe”.

**BILLETERA SANTA FE**

Billetera Santa Fe es un programa de beneficios y descuentos que busca incentivar la demanda de bienes y servicios mediante el otorgamiento de reintegros de dinero a todas las personas que realicen compras en comercios radicados en la provincia.

Esta herramienta permite sumar poder de compra otorgando reintegros de un 30 por ciento en la adquisición de alimentos, indumentaria, calzados, juguetería, librería, bares, restaurantes, farmacias, gastronomía y turismo, y un 20 por ciento en electrodomésticos.

Los interesados deben registrarse en www.santafe.gov.ar/billeterasantafe, para obtener un usuario y contraseña