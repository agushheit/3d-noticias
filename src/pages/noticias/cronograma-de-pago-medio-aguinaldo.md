---
category: Estado Real
date: 2021-06-15T06:52:06-03:00
thumbnail: https://assets.3dnoticias.com.ar/aguinaldo.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: Cronograma de pago medio aguinaldo
title: 'Cronograma de pago medio aguinaldo '
entradilla: El gobierno de la provincia detalló el cronograma de pago del medio aguinaldo
  para empleados activos y beneficiarios de pensiones sociales.

---
El gobierno de la provincia detalló el cronograma de pago del medio aguinaldo para empleados activos y beneficiarios de pensiones sociales.

**Miércoles 16 de junio**

* Personal perteneciente al escalafón policial
* Beneficiarios de pensiones social ley 5110
* Ex combatientes de Malvinas
* Ex presos políticos 
* Madres de desaparecidos

**Jueves 17 de junio**

* Personal activo perteneciente al resto de los escalafones
* Personal docente de las escuelas privadas

**Viernes 18 de junio**

* Todos los pasivos
* Autoridades Superiores Poder Judicial, Poder Legislativo y Poder Ejecutivo.