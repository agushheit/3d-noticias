---
category: Agenda Ciudadana
date: 2020-12-15T12:49:05Z
thumbnail: https://assets.3dnoticias.com.ar/festram.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Diario UNO'
resumen: Festram se opone a que cada municipio y comuna acuerde su política salarial
title: Festram se opone a que cada municipio y comuna acuerde su política salarial
entradilla: Claudio Leoni, secretario General de Festram, confirmó la iniciativa del
  gobierno de descentralizar la paritaria. "Va en contra del modelo de negociación
  colectiva", afirmó.

---
Claudio Leoni, secretario general de la **Federación de Trabajadores Municipales de la Provincia de Santa Fe**, reconoció la intención del gobierno provincial de enviar un proyecto de ley a la Legislatura con el objetivo de "descentralizar" la discusión paritaria.

El gremialista expresó su **desacuerdo por la iniciativa de proponer que cada municipalidad o comuna establezca la política salarial de los trabajadores municipales**.

“Nos preocupa porque la negociación de la paritaria en cada municipio o comuna no garantizará el un piso mínimo salarial para los empleados municipales de la provincia”, afirmó Leoni en declaraciones a LT8 de Rosario.

“Proponen que cada municipalidad y comuna acuerde su política salarial. Nosotros en cambio, queremos garantizar un salario mínimo a todo trabajador de una municipalidad o comuna de la provincia de Santa Fe”, señaló Leoni.

Al mismo tiempo, el titular de la Festram precisó sobre la propuesta del Ejecutivo: “Cada municipio negociaría con cada sindicato municipal que tenga personería gremial. Esto nos preocupa porque no garantiza un piso mínimo en el salario de los trabajadores municipales de la provincia″.

Y más adelante agregó: “Esta iniciativa del gobierno consiste en **descentralizar las negociaciones paritarias**, va en contra de la ley de negociación colectiva que tiene el sindicalismo argentino”.

“La Corte dice que la Constitución provincial entra en tensión con la constitución nacional. Para nosotros es una herramienta muy importante que todos los trabajadores tengan un piso mínimo de sueldo, y no que cada municipio negocie un salario con el sindicato de esa localidad. En definitiva, una iniciativa de este tipo no mejora las condiciones de los trabajadores”, concluyó Leoni.

  
 