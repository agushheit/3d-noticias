---
category: Agenda Ciudadana
date: 2022-01-26T06:15:06-03:00
thumbnail: https://assets.3dnoticias.com.ar/camarabaja.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Los temas clave que se debatirán en las sesiones extraordinarias del Congreso
title: Los temas clave que se debatirán en las sesiones extraordinarias del Congreso
entradilla: "Desde el 1° hasta el 28 de febrero tratarán una agenda que incluirá la
  reforma del Consejo de la Magistratura, el Consenso Fiscal 2021, y otras iniciativas
  económicas.\n\n"

---
El Gobierno convocó este martes a sesiones extraordinarias del Congreso desde el 1° hasta el 28 de febrero para tratar una agenda que incluirá la reforma del Consejo de la Magistratura, el Consenso Fiscal 2021, y otras iniciativas económicas, además de la creación de dos universidades nacionales.  
  
**Reforma del Consejo de la Magistratura**

Del total de 18 temas incluidos en la convocatoria, uno de los proyectos del temario es la reforma del Consejo de la Magistratura, a raíz de que la Corte Suprema declaró inconstitucional la ley aprobada en 2006 que redujo de 20 a 13 los miembros del organismo tripartido encargado de seleccionar jueces, aplicar sanciones o remover magistrados.  
  
El presidente Alberto Fernández propone elevar de 13 a 17 los miembros del organismo para aumentar la representación de los jueces de tres a cuatro, de los abogados de dos a cuatro y de los académicos y científicos de uno a dos.  
  
Así, si el proyecto reúne los votos suficientes en el Parlamento, el organismo quedaría integrado por cuatro jueces, seis legisladores, cuatro representantes de los abogados de la matrícula federal, un representante del Ejecutivo y dos representantes del ámbito académico y científico.  
  
**Economía**

En el orden económico, además del proyecto de ley por el que se aprueba el Consenso Fiscal suscripto por el Poder Ejecutivo nacional y representantes de las provincias el 27 de diciembre pasado, también está la iniciativa de creación del Régimen de Fomento al Desarrollo Agroindustrial.  
  
El objetivo de esa última medida es consolidar el desarrollo de la cadena sectorial a partir de la promoción de nuevas inversiones, generar 700.000 nuevos puestos de trabajo en el sector hasta 2030 e incorporar tecnologías de última generación a partir de una serie de beneficios fiscales e incentivos productivos.  
  
En materia económica también se incluyó el proyecto de Ley de Promoción de la Electromovilidad; el de promoción de Inversiones en la Industria Automotriz–Autopartista y su Cadena de Valor; el de Compre Argentino y Desarrollo de Proveedores y la iniciativa por la cual se prorrogan los plazos establecidos en la Ley 27.613, sobre el Régimen de Incentivo a la Construcción Federal Argentina y Acceso a la Vivienda.  
  
**Educación y Salud**

En materia educativa, la convocatoria firmada por el presidente prevé la creación de las universidades nacionales del Delta y del Pilar.  
  
En la temática de salud, se incluyó el proyecto de Ley de Promoción de la Formación y del Desarrollo de la Enfermería; el de Prevención y Control de la Resistencia Antimicrobiana; el que busca dar respuesta Integral al VIH, Hepatitis Virales y otras Infecciones de Transmisión Sexual (ITS) y Tuberculosis (TBC) y el tendiente a establecer el marco regulatorio de la cadena de producción, industrialización y comercialización de la planta de cannabis, sus semillas y sus productos derivados para uso industrial y/o medicinal, incluyendo la investigación científica, con vistas a satisfacer el mercado local y generar exportaciones.  
  
**Ambiente**

En lo referido a Ambiente, el temario enumera el proyecto de ley por el cual se crea el Área Marina Protegida Bentónica "Agujero Azul"; el que cataloga como tal al Parque y Reserva Nacional Ansenuza y Bañados del Río Dulce, en Córdoba, y el que crea el Parque y Reserva Nacional Islote Lobos, en Río Negro.  
  
Además, a título genérico se incluyeron el tratamiento de Acuerdos -por parte del Senado- y las declaraciones de transferencias de Inmuebles.  
  
Conocido el decreto de convocatoria, el presidente de la Cámara de Diputados, Sergio Massa, se lo comunicó a las autoridades de los diferentes bloques e inició el diálogo de cara a la conformación de las comisiones, tras el recambio legislativo del pasado 10 de diciembre.  
  
La intención de referentes de diferentes bloques es comenzar a trabajar en comisiones la próxima semana para avanzar con los dictámenes que permitan la convocatoria a una sesión.