---
category: Estado Real
date: 2021-01-27T09:58:47Z
thumbnail: https://assets.3dnoticias.com.ar/iapos.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno Santa Fe
resumen: 'Coronavirus: Iapos atendió más de 85 mil llamadas de afiliados desde septiembre'
title: 'Coronavirus: Iapos atendió más de 85 mil llamadas de afiliados desde septiembre'
entradilla: La obra social cuenta con un dispositivo propio de atención telefónica,
  para diagnóstico, atención médica, seguimiento y traslado de pacientes con Covid-19.

---
Desde el mes de septiembre, el 0800 Covid del Instituto Autárquico Provincial de Obra Social (Iapos) atendió 85.890 llamadas de afiliados de todo el territorio provincial, para diagnóstico, atención médica, seguimiento y el traslado de pacientes con Coronavirus.

Este dispositivo de atención cuenta con un centro de contacto que funciona las 24 horas, integrado por un equipo de salud compuesto por operadores telefónicos, médicos, bioquímicos, enfermeros y psicólogos, dos postas sanitarias para la realización de hisopados en las ciudades de Santa Fe y Rosario, y unidades móviles para la toma de muestras en domicilio, como así también para el traslado de profesionales y pacientes en los casos que requieran atención médica presencial o internación. Todos estos componentes forman un sistema integrado, con prestaciones totalmente gratuitas para los afiliados de la Obra Social.

En el mes de octubre, se registró el pico de llamadas, con 1.100 diarias, coincidiendo con el momento más álgido de la pandemia. Un 41 % del total (34.859 llamadas) fueron derivadas a un médico para la evaluación del paciente a través de una videoconsulta, con el objeto de determinar la necesidad de un hisopado, o para recomendar directamente el aislamiento en función de criterios clínicos.

**Funcionamiento**

En caso de presentar síntomas el afiliado se comunica con el 0800 555 6549 - Opción 2 IAPOS, donde es atendido por un operador que -en caso de ser necesario- deriva la consulta a un profesional médico.

El profesional se contacta inmediatamente con el paciente -ya sea por teléfono o videoconsulta- y, en función de la evaluación de signos y síntomas, determina la realización del hisopado correspondiente. Los hisopados se realizan con turnos programados en los efectores y postas sanitarias establecidas o a domicilio. En cualquier caso, las muestras son derivadas al laboratorio para el análisis respectivo, y los resultados son comunicados telefónicamente a cada afiliado dentro de las 48 horas.

Los pacientes con diagnóstico de Covid positivo reciben de inmediato la indicación de aislamiento e ingresan a un régimen de seguimiento médico por videoconsulta hasta la fecha del alta clínica. En el transcurso de la enfermedad cada paciente accede en promedio a 3 o 4 consultas de seguimiento, para controlar la evolución de su estado de salud, y se asesorarlo en materia de cuidados paliativos.

Si la condición del paciente se complica, el profesional a cargo del seguimiento puede disponer una visita médica a domicilio, o el traslado en unidades móviles para los casos que requieren internación.

**Número en el departamento La Capital**

En el departamento La Capital, desde septiembre, se realizaron 12.109 hisopados a afiliados del Iapos, lo que permitió detectar 6.293 casos positivos (el 52%).

Estos casos confirmados han requerido la realización de 22.261 consultas de seguimiento, facilitadas por la utilización de plataformas digitales de telemedicina. Asimismo, se realizaron 1.011 consultas médicas domiciliarias, y en los casos que así lo requirieron, se concretaron 141 traslados para la atención o internación del paciente en un efector de salud.