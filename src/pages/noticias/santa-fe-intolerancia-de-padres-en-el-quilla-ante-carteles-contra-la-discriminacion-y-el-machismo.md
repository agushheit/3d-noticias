---
category: Agenda Ciudadana
date: 2021-10-26T06:00:32-03:00
thumbnail: https://assets.3dnoticias.com.ar/VARONES.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Santa Fe: intolerancia de padres en El Quillá ante carteles contra la discriminación
  y el machismo'
title: 'Santa Fe: intolerancia de padres en El Quillá ante carteles contra la discriminación
  y el machismo'
entradilla: El presidente del club Enrique Serrao defendió la cartelería y dijo que
  algunos padres "son retrógrados"

---
"Soy varón y se que el machismo no va más", "soy varón y se que puedo ser sensible", "soy varón y me gusta otro varón", son algunos de los mensajes que se encuentran en las paredes del Club Náutico El Quilla en la ciudad de Santa Fe, a partir de una iniciativa de la dirección del establecimiento deportivo y la municipalidad para desterrar estereotipos de género.

La acción despertó la ira de un grupo de padres que expresó "con mis hijos no te metas" y amenazaron con desafiliarse del club. En diálogo con el presidente de El Quillá, Enrique Serrao, dijo sobre la idea: "Es para tratar de alguna manera de luchar contra la discriminación, contra el machismo absoluto, contra una serie de cosas que ustedes habrán visto en los carteles. Están todas orientadas a defender a la mujer, a que el hombre pueda expresarse, que no tenga miedo, que comunique. Una serie de cosas que me parece que son elementales en una sociedad como la que estamos desarrollando". Y agregó: "Creo que hay que orientar a los chicos, que fueron los que menos problemas tuvieron con toda esta cartelería. Lo asumen con total naturalidad e inclusive en los talleres que se habían hecho previamente con los chicos de fútbol y otros deportes expresaron su beneplácito".

"Después surgieron las críticas en redes que son bastante contaminantes. La gente grande es la que expresó cosas que me parece que son retrógradas, pero bueno, todo el mundo tiene derecho a pensar con libertad. No todos los padres están en esta posición. Me parece que no se leyó expresamente lo que dicen los carteles", sostuvo Serrao. Y se mostró sorprendido por las reacciones negativas: "En el club están compitiendo chicas y chicos en fútbol, por ejemplo. Igual en hockey, y no hay un malestar por eso. Lo que estamos estimulando es que este tipo de cosas se sigan haciendo. No es tan descabellado".

Al mismo tiempo, Serrao presentó la renuncia en la dirección del club pero no estaría relacionado a la reacción de los padre enojados: "Estoy esperando una reunión que vamos a tener y voy a tomar la determinación. La renuncia no está expresamente ligada a esta este circunstancia particular, es por otra cuestión. Sino estaría renunciando a seguir con lo que yo avalé que teníamos que hacer. Voy a seguir pensando como pensaba previamente a todo esto y la posible renuncia es por otra cuestión"