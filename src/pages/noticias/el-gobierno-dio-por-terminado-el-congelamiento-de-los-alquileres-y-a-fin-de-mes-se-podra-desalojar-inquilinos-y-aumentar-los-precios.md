---
category: Agenda Ciudadana
date: 2021-03-03T07:08:10-03:00
thumbnail: https://assets.3dnoticias.com.ar/alquileres.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Clarín
resumen: El Gobierno dio por terminado el congelamiento de los alquileres y a fin
  de mes se podrá desalojar inquilinos y aumentar los precios
title: El Gobierno dio por terminado el congelamiento de los alquileres y a fin de
  mes se podrá desalojar inquilinos y aumentar los precios
entradilla: Lo confirmó el ministro Jorge Ferraresi. La decisión es no volver a prorrogar
  el DNU que los mantenía sin modificaciones y comenzar a aplicar la fórmula prevista
  en la nueva ley.

---
El 31 de marzo vence el DNU firmado por el presidente Alberto Fernández que mantenía el congelamiento de los alquileres y no se volverá a extender su plazo de vigencia. A partir de entonces, los contratos deberán guiarse por la nueva fórmula de actualización que se fijó en la Ley de Alquileres y los inquilinos que habían usado el beneficio durante la pandemia deberán comenzar a pagar en cuotas la diferencia acumulada.

Tampoco se extenderá la suspensión de desalojos, que quedará en manos entonces de una medicación judicial. Jugará ahí un rol fundamental el registro de propiedades de alquiler, ya que la Justicia pedirá esos datos a la Administración Federal de Ingresos Públicos (AFIP) al abrir la causa y si no figura no se procederá al desalojo.

Así lo confirmó este martes el ministro de Desarrollo Territorial y Hábitat, Jorge Ferraresi, a la Agencia Télam. Según pudo saber TN.com.ar a partir de abril “se trabajará con las herramientas que generó la nueva Ley de Alquileres”.

“Vamos a poder generar condiciones económicas para ver cómo se ajustan los alquileres, el BCRA fija el índice que va a resguardar fundamentalmente que la evolución esté en línea con la posibilidad económica que tengan los inquilinos”, afirmó el ministro y dijo que cuando venza el congelamiento se podrá “ver el universo de cuáles son las familias vulnerables”. También destacó la importancia de “empezar a poner en función la Ley de Alquileres”.

> _Jorge Ferraresi_
>
> El BCRA fija el índice que va a resguardar fundamentalmente que la evolución esté en línea con la posibilidad económica que tengan los inquilinos

En cuanto a los desalojos recordó que también a través de esta misma ley “tienen que pasar por el Ministerio de Justicia para mediación” y que por eso “ya no va a haber extensión del DNU”. “No hay extensión, lo más fuerte en la reglamentación de la Ley de Alquileres es la mediación de los desalojos y la inscripción en AFIP de los contratos”, ratificó.

![](https://assets.3dnoticias.com.ar/alquileres-tn-030321.webp)

Según dijo, con estos cambios ”viene todo un tiempo nuevo, por eso está la convocatoria a que los inquilinos inscriban el contrato”. “Hay una realidad totalmente distinta, con nuevos mecanismos y herramientas y va a haber un control”, agregó.

**El sueño de la casa propia**

Ferraresi llegó el Gobierno de Alberto Fernández de la mano de la vicepresidenta Cristina Fernández de Kirchner, tras el cuestionamiento sobre funcionarios que no funcionan y de la crisis por la toma de tierras en distintos puntos del país. El desafío que enfrenta es ir buscando una solución al déficit habitacional de la Argentina, aunque se trate de políticas de largo plazo que excederán a su gestión.

En este contexto, el ministro consideró que “la solución es que la Argentina construya vivienda”, y confió en que “las 264.000 soluciones habitacionales -previstas en el plan trienal 2021-2023- irán modificando el mercado de alquileres”. “La mejor política es que el que alquila pueda tener una propiedad por el mismo valor de un alquiler, y no desde un sistema financiero, sino desde un sistema de recupero solidario”, consideró.

> _Jorge Ferraresi_
>
> No hay extensión, lo más fuerte en la reglamentación de la Ley de Alquileres es la mediación de los desalojos y la inscripción en AFIP de los contratos.

“El Plan Casa Propia se trata de viviendas dignas para todos los argentinos; salió una ley por extraordinarias que nos permite reactivar 55.000 viviendas en todo el país”, destacó al respecto y mencionó también “el esquema de viviendas Procrear”. “El 12 de marzo vamos a estar lanzando un plan para la generación de suelo que tenga que ver con la asistencia técnica para que las jurisdicciones vayan generando suelo urbano”, adelantó.

En cuanto a las hipotecas UVA, cuyo congelamiento concluyó el 31 de enero, destacó que no hubo quejas y se van actualizando de manera ordenada. “No hemos tenido por el momento ningún reclamo, los bancos tienen que aplicar 35% al límite del ingreso familiar; el primer mes de convergencia no hemos tenido denuncia de alguien que no haya podido completarla”.