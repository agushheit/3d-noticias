---
category: La Ciudad
date: 2021-07-20T06:15:52-03:00
thumbnail: https://assets.3dnoticias.com.ar/LICEO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Liceo Municipal: se abre la preinscripción para el ingreso a Idiomas'
title: 'Liceo Municipal: se abre la preinscripción para el ingreso a Idiomas'
entradilla: 'Este lunes comenzó la preinscripción para cursar el módulo 1 de inglés,
  francés, italiano, alemán y portugués. '

---
Está previsto brindar las clases presenciales en la sede de la Escuela de Idiomas y en los espacios descentralizados, y se cuenta también con recursos para garantizar la continuidad de los estudios en modalidad virtual.

Del 19 al 26 de julio, la Escuela de Idiomas del Liceo Municipal “Antonio Fuentes del Arco” abre la preinscripción para el módulo I de cinco idiomas. Esta oferta está destinada para adolescentes y adultos que quieran empezar a estudiar inglés; y para adultos interesados en aprender alemán, francés, italiano o portugués.

El trámite se podrá realizar online, en la web de la Municipalidad, para cursar en la sede de la Escuela (Estación Belgrano, Bv. Gálvez 1150) y en el edificio del Liceo (Molino Marconetti, dique II del Puerto de Santa Fe). En el marco del Programa “El Liceo hace comunidad”, que descentraliza a la institución acercando su política educativa a través de distintas sedes barriales, la preinscripción de inglés para mayores de 16 años de edad se podrá realizar de 9 a 17 horas, en la Estación Mediateca (Pasaje Mitre y Tucumán); y de 13 a 17 horas, en el Jardín Municipal Las Flores (Regimiento 12 de Infantería esquina Europa).

**Cómo sigue**

Para el 27 de julio está prevista la realización del sorteo para conformar los listados de los distintos cursos. Ese procedimiento se hará nuevamente con un sistema informático que se implementó a partir de este año y que demostró ser práctico, transparente y ejecutivo. Luego, se publicarán los resultados en la web institucional.

La inscripción definitiva de quienes resulten sorteados se extenderá entre el 2 y el 6 de agosto. Quienes no resulten favorecidos en esta instancia, quedarán en una lista de espera, que se renueva con cada llamado y que permite aprovechar todos los lugares disponibles. Para más información comunicarse al teléfono 4827620, de lunes a viernes, de 8 a 14.

En cuanto al cursado, su continuidad estará garantizada, atendiendo a la situación sanitaria, indicó el subdirector ejecutivo del Liceo Municipal, Juan Cruz Giménez. La experiencia adquirida desde el inicio de la pandemia, permitirá combinar las clases presenciales, si fuera necesario, con recursos virtuales para garantizar los procesos de aprendizaje.

“La propuesta de la Escuela de Idiomas permite acceder cada seis meses a una oferta académica pública, gratuita y de calidad, en los distintos niveles”, remarcó el funcionario. El valor que tiene la institución para las vecinas y vecinos de la ciudad se refleja en cada convocatoria para el inicio de la cursada. En este sentido, a fines de junio se recibieron unas 700 solicitudes para rendir los exámenes de acreditación de saberes en los cinco idiomas.