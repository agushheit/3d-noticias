---
category: La Ciudad
date: 2021-09-25T06:00:07-03:00
thumbnail: https://assets.3dnoticias.com.ar/PEGATINA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Más de 200 actas de infracción labradas por cartelería electoral ilegal o
  sin sacar
title: Más de 200 actas de infracción labradas por cartelería electoral ilegal o sin
  sacar
entradilla: 'La ciudad de Santa Fe empezó a verse más limpia de afiches proselitistas,
  pero aún se ven columneros y pegatinas que quedaron de las PASO, sobre todo en los
  paseos públicos.

'

---
De acuerdo a los plazos establecidos por el Código de Publicidad y Propaganda de la ciudad (Ordenanza N° 12.090 y sus actualizaciones, N° 12.225 y N° 12.621), los partidos y frentes políticos que participaron de las PASO del pasado 12 de septiembre tenían tiempo hasta este miércoles 22 de septiembre para proceder al retiro de su cartelería electoral en el ejido urbano de la ciudad. Muchos lo hicieron, otros no tanto.

 A este viernes, desde la secretaría de Control y Convivencia del Municipio se habían labrado más de 200 actas de infracción. El número abarca tanto la cartelería colocada en la etapa anterior a las elecciones primarias (aquella colocada fuera de lugares permitidos, es decir, de modo ilegal), y estos días ya pasados del plazo permitido.

Antes de las elecciones, el Gobierno local retiró unos 800 carteles en infracción, y en estos días "estaremos en más de 1.200 carteles retirados por el personal municipal", le dijeron al El Litoral fuentes del área de Control. "El jueves salimos a verificar y multar, en los casos en que aún no había sacado sus afiches proselitistas", añadieron.

 Para el retiro se organizó un operativo con la GSI y la secretaría de Obras Públicas, con el objetivo de mantener limpia la ciudad: "Ese es nuestro objetivo prioritario -subrayaron-. Las actas de infracción que se labren luego serán juzgadas por el Tribunal de Faltas. Y el Juez de Faltas, de acuerdo a la gravedad de cada contravención, determinará el monto dinerario de la multa", completaron. Estas sanciones dinerarias tienen un piso de 12 mil pesos y un máximo de 120.000 pesos.

 Según establece el cronograma electoral provincial, para las elecciones generales (que se realizarán el 14 de noviembre), la fecha de inicio de la campaña es el 15 de octubre. El 4 de noviembre comienza a regir el plazo de 10 días de exhibición de afiches proselitistas en la vía pública (10 días antes del comicio general). Y desde la fecha de la elección, habrá otros diez días para proceder al retiro, pero claro: lo más importante es que ya se sabrán los nombres de quiénes serán electos para el Concejo santafesino.