---
category: La Ciudad
date: 2021-11-25T06:15:24-03:00
thumbnail: https://assets.3dnoticias.com.ar/concejo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'Concejo de Santa Fe y Presupuesto 2022: ítem seguridad y toma de deuda por
  $ 400 millones, en foco'
title: 'Concejo de Santa Fe y Presupuesto 2022: ítem seguridad y toma de deuda por
  $ 400 millones, en foco'
entradilla: En la primera reunión para evaluar la ordenanza “madre”, un edil opositor
  puso reparos sobre la incidencia presupuestaria en materia de seguridad.

---
Este miércoles se realizó la primera reunión técnica entre funcionarios municipales -entre ellos Federico Crisalle, secretario de Gobierno, y Luciano Mohamad, secretario de Hacienda- y concejales de todos los bloques para analizar la ordenanza financiera “madre” del Ejecutivo santafesino: el Presupuesto Municipal 2022. A juzgar por las declaraciones posteriores al encuentro, se dejaron entrever algunas rispideces sobre ítems puntuales de la herramienta que necesita el intendente Emilio Jatón para gobernar sin sobresaltos el año próximo.

 El primero en hablar con la prensa fue el edil opositor, Carlos Suárez (UCR-Juntos por el Cambio). Si bien consideró que “fue una buena reunión”, “requerimos más información fina sobre el Presupuesto, que en principio es deficitario: no es equilibrado pues contempla mayores gastos que recursos”, puso sus reparos.

 “Nos preocupan algunas subejecuciones presupuestarias que vemos en determinadas áreas del proyecto de Presupuesto 2022. Y más aún nos preocupa el ítem seguridad, pues hay poca incidencia para esta área dentro del Presupuesto general. Estos son los puntos centrales sobre los cuales solicitaremos más información”, añadió.

 También Suárez dijo haber pedido conocer el nivel de endeudamiento actual del municipio, “habida cuenta de que se nos está pidiendo autorización para avanzar en dos tipos de endeudamiento: uno sobre emisión de letras, y otro por $ 400 millones (aludiendo a la solicitud de autorización de toma de empréstito del Ejecutivo al Concejo para destinar a obras públicas)”.

 También desde su sector político se solicitaron detalles sobre el nivel de recaudación, es decir, cómo fue la evolución durante la etapa de pandemia y ahora, en una suerte de nueva normalidad. “Son elementos centrales que necesitamos para evaluar esta herramienta financiera”.

 **Fuerte apuesta a obras**

 La edila oficialista Laura Mondino (FPCyS) dio un contexto: dijo que el Presupuesto de este año “tuvo la impronta de integrar a la ciudad de Santa Fe y de poder empezar a trabajar en los barrios que fueron mayormente olvidados durante mucho tiempo, con obras emblemáticas que hoy se ejecutan”.

 Pero ahora, “el Presupuesto 2022 plantea una fuerte apuesta a la obra pública y la gestión urbana: esto es, las calles de acceso a los barrios de la ciudad, más iluminación, mejoras en espacios públicos… Venimos de tiempos donde la Municipalidad tiene una deuda en muchos barrios que no tuvieron obras significativas”, insistió.

 “El endeudamiento es una de los temas que se va a evaluar y estudiar. Hoy el Ejecutivo está pidiendo al Concejo autorización para tomar deuda por $ 400 millones justamente para pavimento, iluminación y gestión urbana. Este pedido indica que hoy es el tiempo de reconstrucción de la gestión urbana”, subrayó Mondino.

 Y consultada sobre la asignación de partidas para seguridad (a contrapelo de las declaraciones de Suárez), la edila declaró: “Estamos convencidos de que la seguridad no es sólo más policías sino más y mejor gestión urbana: mejorar las calles de acceso a los barrios, tener más iluminación. Pero además, se trabaja en otro eje: más cámaras de vigilancia y un aumento de alarmas comunitarias”.

 El actual Presidente del Cuerpo, Leandro González, aseguró que “hay una buena predisposición (en general de los bloque políticos) para abordar el Presupuesto 2022”, el cual tiene “estrategias innovadoras” para el momento que se vive en la ciudad y en el país. “Es el plan de gobierno que el intendente está trazando para el próximo año”, cerró.