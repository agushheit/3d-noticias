---
category: La Ciudad
date: 2021-09-03T06:15:04-03:00
thumbnail: https://assets.3dnoticias.com.ar/MORATORIA.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Municipalidad prorrogó la moratoria 2021
title: La Municipalidad prorrogó la moratoria 2021
entradilla: Mediante Resolución se dispuso extender la vigencia de la Ordenanza que
  establecía el Régimen de Regularización de Derechos

---
La Secretaría de Hacienda municipal emitió la Resolución N° 221 en la que se establece la extensión del Régimen de Regularización de tributos que había sido dispuesto en la Ordenanza N°12.771. Se trata de la moratoria lanzada este año, para que los contribuyentes atrasados en el pago de sus tributos puedan ponerse al día, recibiendo a cambio una importante reducción de su carga fiscal.

Cabe resaltar que la ordenanza, sancionada por el Concejo Municipal, había sido emitida como un Mensaje del intendente Emilio Jatón con la intención de colaborar con los contribuyentes, en el marco de la situación económica y sanitaria generada por la pandemia de Covid-19.

Según se informó, la prórroga será hasta el 15 de octubre próximo. La misma fue determinada a los efectos de facilitar el cumplimiento voluntario del pago de los tributos adeudados.

En ese sentido la norma establece para las deudas generadas hasta el 31/03/2020 una reducción de la tasa de interés, que se lleva del 3% al 2% mensual; siendo las opciones para cancelar:

1) Pago contado, con un descuento del 90% de los intereses

2) Planes de pago:

Hasta 12 cuotas, con un descuento del 50% de intereses por mora y sin interés de financiación.

Hasta 24 cuotas, con un descuento del 30% de intereses por mora y un interés de financiación del 1,5% mensual.

Hasta 36 cuotas, sin reducción de intereses por mora e intereses de financiación del 2% mensual.

Por otra parte, para aquellas deudas contraídas donde la pandemia tuvo su mayor impacto, entre el 01/04/2020 y el 30/04/2021, los intereses por mora son condonados en un 100% y se podrá financiar en planes de hasta 24 cuotas sin interés de financiación, a tasa 0%.

El secretario de Hacienda municipal, Luciano Mohamad, informó que “estamos hablando de una reducción total de intereses para las deudas generadas en pandemia, desde abril de 2020 a abril de 2021, como también la posibilidad de financiar las mismas sin interés. En tanto, las deudas anteriores a abril de 2020 gozan de una quita 90% de intereses para pagos de contado, teniendo la posibilidad también de formalizar convenios con reducciones de intereses que van desde el 50%”, explicó.

En ese sentido, recomendó “acogerse a los beneficios dispuestos por esta moratoria, dado que, vencido su plazo de vigencia, se originarán mayores sacrificios para el contribuyente”.

¿Qué tributos se pueden pagar?

Tasa General de Inmuebles (TGI)

Derecho de Registro e Inspección (DREI)

Contribuciones por mejoras

Derecho de Cementerio

Otros tributos mencionados en el artículo N° 10 de la Ordenanza Nº 12.771

¿Dónde se puede abonar?

Puede gestionarse el pago de la deuda en la página web oficial www.santafeciudad.gov.ar/moratoria2021 para lo cual sólo es necesario contar con al menos una boleta del tributo que se pretende cancelar. También puede abonarse en el Palacio Municipal, para una atención personalizada, en el horario de 7:30 a 13, para lo cual se recomienda obtener un turno en [https://santafeciudad.gov.ar/turnos-online-para-gestiones-municipales/.](https://santafeciudad.gov.ar/turnos-online-para-gestiones-municipales/. "https://santafeciudad.gov.ar/turnos-online-para-gestiones-municipales/.")