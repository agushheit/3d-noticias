---
category: Estado Real
date: 2021-01-20T09:29:43Z
thumbnail: https://assets.3dnoticias.com.ar/aprecod.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de la Provincia de Santa Fe
resumen: La provincia constituyó el Consejo Consultivo de Aprecod
title: La provincia constituyó el Consejo Consultivo de Aprecod
entradilla: El mismo funciona como un órgano asesor de la agencia. El ministro Capitani
  participó de la actividad y expresó que “necesitamos el acompañamiento de todos
  para enfrentar la problemáticas de las adicciones”

---
El ministro de Desarrollo Social, Danilo Capitani, encabezó el encuentro donde se constituyó el Consejo Consultivo de la Agencia de Prevención del Consumo de Drogas y Tratamiento Integral de Adicciones (Aprecod), entidad que funciona como órgano asesor de la agencia.

En la oportunidad, Capitani expresó: “Queremos que el Consejo sea un espacio de participación efectiva. Es importante la visión de cada uno y tenemos que trabajar juntos en la articulación de políticas públicas”.

Luego, expresó que “el tema de la adicciones se ha ido agravando por la pandemia, por lo cual necesitamos el acompañamiento de todos para trabajar sumando esfuerzos en esta difícil tarea de enfrentar esta problemática” .

Desde la creación de Aprecod está previsto el Consejo Consultivo como órgano asesor pero esta es la primera vez que se constituye formalmente lo que permite generar una coordinación de trabajo, entre las áreas del Estado santafesino y las organizaciones sociales del territorio, en toda la labor a desarrollar en la problemática de adicciones y consumos problemáticos .

**PRESENTES**

Del encuentro, llevado a cabo en el auditorio del Ministerio de Desarrollo Social también participaron representantes de los ministerios de Gobierno, Justicia, Derechos Humanos y Diversidad, de Cultura y de Salud de Santa Fe; y delegados de organizaciones sociales de las zonas sur, centro y norte de la provincia.

**APRECOD**

El objetivo de Aprecod es la implementación de las políticas públicas en materia de consumos problemáticos y adicciones a través de la articulación de acciones y coordinación técnica-operativa con las diferentes jurisdicciones y actores sociales.

La agencia trabaja en forma coordinada en la prevención y el abordaje de situaciones de consumo, propiciando la construcción de una Red Provincial, georeferenciada en tres macro regiones:

Centro-Norte: comprende los departamentos La Capital, Garay, San Justo, San Javier, San Cristóbal, Nueve de Julio, Vera y General Obligado.

Centro-Oeste: comprende los departamentos Belgrano, Iriondo, San Jerónimo, San Martín, Las Colonias y Castellanos.

Sur: comprende los departamentos Rosario, San Lorenzo, Caseros, Villa Constitución y General López.