---
category: Estado Real
date: 2021-04-17T06:00:42-03:00
thumbnail: https://assets.3dnoticias.com.ar/INCLUIR.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Provincia desembolsó más de $560 millones del Plan Incluir a municipios
  y comunas de todo el territorio santafesino
title: La Provincia desembolsó más de $560 millones del Plan Incluir a municipios
  y comunas de todo el territorio santafesino
entradilla: Por decisión del gobernador Perotti se realizó una transferencia a distintas
  localidades. Es el mayor programa de obras y acciones del Estado en las 365 localidades
  santafesinas.

---
El gobierno de la provincia, por disposición del gobernador Omar Perotti, realizó una primera transferencia de fondos por más de $560 millones, de un total de $4.000 millones, a municipios y comunas en el marco del Plan Incluir. Este primer desembolso tiene por objetivo que las localidades inicien el llamado a licitación para las distintas obras que requieren ejecutar.

Este plan pone de manifiesto una premisa de la actual gestión: gobernar para todos. Por eso, su implementación se basa en la consideración equitativa de los 365 municipios y comunas de la provincia, terminando con la discrecionalidad y la discriminación a los gobiernos locales; superando ampliamente el anterior Plan Abre.

El Plan Incluir parte de una retroalimentación permanente con los intendentes, los presidentes comunales y las organizaciones no gubernamentales de cada uno de los sectores para poder entender cuál es la mejor inversión que el gobierno de la provincia puede hacer en cada lugar. Actualmente existen proyectos de obras para grandes conglomerados de más de 100 millones de pesos y otros de menos de 5 millones para las comunas que lo requieran, demostrando la diversidad de necesidades a lo largo y ancho del territorio provincial.

Cabe señalar que del Plan Incluir participan los ministerios de Desarrollo Social, Seguridad; Salud; Educación; Infraestructura, Servicios Públicos y Hábitat; Ambiente y Cambio Climático; Cultura; Gestión Pública; la Secretaría de Estado de Igualdad y Género; la Empresa Provincial de la Energía; Aguas Santafesinas SA; municipios, comunas y organizaciones sociales.