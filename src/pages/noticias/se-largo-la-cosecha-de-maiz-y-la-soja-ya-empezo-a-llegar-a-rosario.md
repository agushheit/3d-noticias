---
category: El Campo
date: 2021-02-17T07:04:11-03:00
thumbnail: https://assets.3dnoticias.com.ar/maiz.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Agrovoz
resumen: Se largó la cosecha de maíz y la soja ya empezó a llegar a Rosario
title: Se largó la cosecha de maíz y la soja ya empezó a llegar a Rosario
entradilla: Arrancó oficialmente la trilla de los cultivos de verano. Las expectativas
  de producción mejoraron mucho con las últimas lluvias.

---
Aunque todavía en un grado más que incipiente, la cosecha gruesa 2020/21 dio su puntapié inicial en los últimos días.

En lo que respecta a la soja, la Bolsa de Comercio de Rosario (BCR) informó que el 11 de febrero pasado se descargó en la planta de Cofco Internacional, en Timbúes, el primer lote del presente ciclo.

Nuevamente, es una partida cosechada por Eduardo Tessore, en Potrero Norte, Formosa. Fueron 28,8 toneladas de la variedad Syngenta 5x1 y la corredora que intervino en la comercialización fue Grassi SA.

[![Twitter](https://assets.3dnoticias.com.ar/BCR-prensa-1.webp)](https://twitter.com/BCRprensa/status/1360300068645965824?s=20 "Twitter")

“Conforme lo establecido por el reglamento de esta Bolsa de Comercio, recibidores oficiales de su Cámara Arbitral actuaron sobre la mercadería descargada en la terminal mencionada. Luego del análisis, realizado en el laboratorio de cercanía de la BCR, se determinó que la partida cumplió con la norma de calidad para la comercialización de soja”, indicó la entidad rosarina

Como es tradicional, este lote se rematará en el recinto de la Bolsa en abril, en lo que constituye una suerte de inicio “formal” de la comercialización de la oleaginosa de la nueva campaña.

## **Maíz**

En cuanto al cereal, la Bolsa de Cereales de Buenos Aires informó que en Santa Fe y en Entre Ríos se avanzó con la recolección de los primeros lotes tempranos, que ofrecieron rendimientos por debajo de las expectativas relevadas a comienzos de la campaña.

Producto del déficit hídrico registrado durante el período crítico, los rindes en el centro-norte santafesino y en el centro de Entre Ríos se sitúan en torno a 55 quintales por hectárea.

## **Pronósticos**

En este contexto, en las últimas semanas también las Bolsas mencionadas actualizaron sus perspectivas productivas para la cosecha 2020/21.

La entidad porteña, en el caso de la soja, redujo su pronóstico inicial de 46,5 millones de toneladas a 46 millones. Para el maíz, estima una caída más pronunciada: de 47 millones a 46 millones de toneladas.

[![Twitter](https://assets.3dnoticias.com.ar/Estimaciones-agricolas-BC.webp)](https://twitter.com/estimacionesbc/status/1359928186730139649?s=20 "Twitter")

La Bolsa rosarina, en cambio, es más optimista: en su último informe nacional aseguró que las lluvias de enero hicieron que “el fantasma del desastre productivo de 2018 quede atrás” y elevó sus expectativas.

En su reporte de enero, antes de que llegaran las lluvias, había ubicado la zafra de soja en 47 millones de toneladas. Ahora, la subió a 49 millones.

“Las oportunas lluvias de fines de enero desbarataron los efectos de una gran ola de calor y pusieron punto final a una sequía que venía condicionado al cultivo desde sus inicios en la franja central del país. Las buenas a muy buenas condiciones de la soja de primera y de segunda permiten estimar un rinde promedio nacional de 29,2 quintales por hectárea. Esto es casi un quintal más que el promedio nacional de los últimos ocho años”, indicó la BCR.

Para esta entidad, fueron “siete días que cambiaron el rumbo de la soja en Argentina”, con “un inesperado caudal de lluvias que le dio un vuelco a la campaña”.

[![Twitter](https://assets.3dnoticias.com.ar/BCR-mercados.webp)](https://twitter.com/BCRmercados/status/1360944382837329926?s=20 "Twitter")

“La franja central dejó atrás la falta de agua, y ahora con excelentes reservas en los suelos el cultivo atraviesa sus etapas críticas sin limitantes ni estrés. El impacto ha sido contundente en el cambio de la condición del cultivo en Santa Fe, Córdoba, Entre Ríos y el norte de Buenos Aires”, resumió la entidad.

Con respecto al maíz, la Bolsa de Rosario también realzó la estimación, desde las 46 millones de toneladas del informe anterior a 48,5 millones de toneladas.

Entre otros aspectos, la entidad destacó el excelente estado de los cultivos tardíos en Córdoba, donde se prevé un rinde promedio de 87,8 quintales por hectárea, muy por encima de los 80 quintales calculados un mes atrás.

[![Twitter](https://assets.3dnoticias.com.ar/BCR-mercados-1.webp)](https://twitter.com/BCRmercados/status/1361306770929573894?s=20 "Twitter")