---
category: Deportes
date: 2021-04-13T07:57:10-03:00
thumbnail: https://assets.3dnoticias.com.ar/atletismo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: Santa Fe logró el segundo puesto en la tabla general del 101° Campeonato
  Nacional de Atletismo
title: Santa Fe logró el segundo puesto en la tabla general del 101° Campeonato Nacional
  de Atletismo
entradilla: El certamen se llevó a cabo en Concepción del Uruguay del 9 al 11 de abril.

---
La selección provincial obtuvo el segundo lugar en el 101° Campeonato Nacional de Atletismo, que tuvo lugar en la ciudad entrerriana de Concepción del Uruguay del 9 al 11 de abril.

El certamen, desarrollado durante jornadas de intensas lluvias, finalizó con la Federación Metropolitana en el primer lugar con 363 puntos, Santa Fe con 315,50 y Buenos Aires con 311.

Al respecto, el ministro de Desarrollo Social, Danilo Capitani, expresó sus felicitaciones y reconoció el “esfuerzo, la dedicación y el sacrificio que hacen siempre, con el apoyo de sus familias que los acompañan en todos los momentos, y por representar tan bien a la provincia de Santa Fe”.

Por su parte, la secretaria de Deportes, Claudia Giaccone, resaltó que “es un gran torneo para nuestra provincia, porque además de lograr el segundo lugar en la tabla general sumando las dos ramas, los varones después de muchos años han quedado primeros. Estamos felices del gran rendimiento que tuvieron nuestros atletas en el nacional, sabemos el gran nivel y potencial que hay en Santa Fe y por tal motivo seguiremos acompañando el desarrollo”.

La delegación santafesina obtuvo 26 medallas: 10 de oro, 9 de plata y 7 de bronce, a saber:

>> Marcha 20.000: Giuliani Sebastian – 1.47.27.65 (Oro), Magallanes Ariel (Plata), Majda Maria Del Rosario (Oro) y Kloster Sofia (Plata).

>> Salto en Largo: Brian López (Oro).

>> 1.500 mts: Zabala José (Bronce).

>> 400 mts: Pedro Emmert (Bronce).

>> Posta 4×100: López Brian/Zuliani Pablo/Pedro Emmert/Pafundi Alejo (Oro) y Okon Abril/Garetto Leila/Perez Rocio/Moyano Celina (Plata).

>> 3.000 con obstáculos: Johnson Carlos August (Oro) y Lozano Carolina (Oro).

>>Lanzamiento de Martillo: Nobile Julio Nahuel (Plata) y Cacciabue Tobias (Bronce).

>> Lanzamiento de Bala: Menoni Milagros (Bronce).

>> Salto Triple: López Brian (Bronce).

>> 5000 mts: Zabala José (Bronce).

>> Lanzamiento de Disco: Bonora Lázaro (Plata).