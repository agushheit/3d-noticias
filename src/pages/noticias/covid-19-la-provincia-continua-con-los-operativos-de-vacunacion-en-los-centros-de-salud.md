---
category: Agenda Ciudadana
date: 2021-08-18T06:00:56-03:00
thumbnail: https://assets.3dnoticias.com.ar/SALUD.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Covid-19: la provincia continúa con los operativos de vacunación en los
  centros de salud'
title: 'Covid-19: la provincia continúa con los operativos de vacunación en los centros
  de salud'
entradilla: El objetivo es llegar a las personas que no cuentan con los medios para
  inscribirse en el registro provincial de vacunación.

---
El Ministerio de Salud provincial llevó adelante este martes un operativo de vacunación en el centro de salud Nº 21 del barrio Cabin 9, de la localidad de Pérez. “Se trata de una estrategia de rastrillaje donde se buscan personas que aún no se hayan podido inscribir en el registro de vacunación provincial”, indicó la directora de Primer y Segundo nivel de atención en salud, Laura Ortube.

“Este trabajo se viene realizando desde hace más de dos meses en todos los centros de salud provinciales. Una vez que se localizan estas personas que aún no han sido vacunadas, se los acompaña al centro de salud, se los inscribe en el momento y se les ofrece la vacunación”, agregó.

Al ser consultada por la cantidad de inscriptos, Ortube señaló que “todavía seguimos encontrando una gran cantidad de pacientes que aún no están vacunados ni inscriptos, ya sea por no tener el acceso a la tecnología o la ayuda de algún familiar o conocido, otros porque tienen alguna situación o inconveniente con su número de documento y algunos todavía tienen algunas dudas con relación a la vacunación”.

“En cuanto a esto último, la idea es que los equipos territoriales les acerquen información a la gente, explicarles que corren mayor riesgo si no están vacunados que sí lo están y acompañarlos al centro de salud para charlar con su médico de cabecera y puedan consultar todas las dudas en relación a este tema”, concluyó.