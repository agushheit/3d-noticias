---
category: Agenda Ciudadana
date: 2021-07-22T06:00:49-03:00
thumbnail: https://assets.3dnoticias.com.ar/DOSIS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: Llegó otro vuelo de Aerolíneas con más de 760 mil dosis de Sinopharm
title: Llegó otro vuelo de Aerolíneas con más de 760 mil dosis de Sinopharm
entradilla: Se trata del noveno vuelo a China, de la operación de 10 viajes, para
  traer este mes 8 millones de dosis.

---
La aeronave Airbus 330-200, cuyo viaje forma parte del acuerdo suscripto con el China National Pharmaceutical Group Corp por un total de 24 millones de vacunas a entregar hasta septiembre a razón de 8 millones por mes, aterrizó en Ezeiza a las 18.17.

En tanto, para este jueves a la misma hora se prevé la llegada del último vuelo de la línea de bandera que completará esta primera serie de envíos durante julio.

La empresa completó 14 vuelos desde Beijing (China) en los que se trasladaron 11.339.000 dosis de Sinopharm, 22 viajes desde Moscú (Rusia) que sumaron 11.813.375 de Sputnik V y 2 desde Memphis (Estados Unidos) con 3.500.000 dosis de Moderna.

Desde que se inició la campaña de inmunización arribaron hasta ahora 39.177.430 vacunas, de las cuales 12.144.000 corresponden a Sinopharm; 11.868.830 a Sputnik V, (9.375.670 del componente 1 y 2.493.160 del componente 2); 9.140.600 a las de AstraZeneca y Oxford cuyo principio activo se produjo en la Argentina; 3.500.000 a Moderna; 1.944.000 a AstraZeneca por el mecanismo Covax de la OMS y 580.000 a AstraZeneca-Covishield.

De acuerdo al Monitor Público de Vacunación, hasta esta mañana se distribuyeron 32.609.944 dosis en todas las jurisdicciones, al tiempo que se aplicaron 28.264.151. De ese total, hay 22.573.282 personas inoculadas con la primera dosis y 5.690.869 cuentan con el esquema completo.