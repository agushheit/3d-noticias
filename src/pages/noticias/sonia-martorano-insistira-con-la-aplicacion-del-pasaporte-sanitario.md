---
category: Agenda Ciudadana
date: 2021-11-23T06:15:27-03:00
thumbnail: https://assets.3dnoticias.com.ar/sonia.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Sonia Martorano insistirá con la aplicación del pasaporte sanitario
title: Sonia Martorano insistirá con la aplicación del pasaporte sanitario
entradilla: 'Lo planteará en la reunión del viernes del Consejo Federal de Salud.
  El documento acredita la vacunación contra el coronavirus

'

---
La ministra de Salud de Santa Fe, Sonia Martorano, planteará el viernes que viene en la habitual reunión del Consejo Federal de Salud (Cofesa) evaluar la aplicación del pasaporte sanitario, una idea que ya se piensa en Europa, continente en el que una nueva ola está causando nuevos estragos en la población de diversos países. Austria aplicó la vacunación obligatoria, Francia admite que la nueva ola ingresó como "un rayo" y en Bélgica hubo fuertes enfrentamientos en las calles para frenar otro avance del coronavirus.

En septiembre del año pasado se había presentado un proyecto en la cámara Baja pero con el formato nacional, donde se unificaban los pedidos de testeos a la población que circula por el país.

“No estamos exentos de tener una nueva ola y por eso estamos avanzando con los refuerzos. Me parece interesante plantear el pasaporte sanitario porque en la actualidad hay acceso a las dos dosis de vacuna. Queremos instalar la idea porque la crisis mundial tiene que ver con los no vacunados fundamentalmente. Lo del pasaporte lo hemos hablado y al observar lo que pasa en el resto del planeta, no me parece nada mal. Este viernes hay Cofesa y lo trataremos de charlar con los demás ministros”, adelantó Martorano a La Capital.

Santa Fe ya tiene al 70 por ciento de sus habitantes con esquema completo de vacunación y se estima que para fin de año o comienzos del próximo llegaría al 80 por ciento. Un porcentaje mundialmente aceptado como satisfactorio.

En Europa, en el pasaporte sanitario se acreditarán los datos personales, de la vacuna aplicada, con el genérico del fármaco, el nombre comercial, el número de dosis administradas, profesionales actuantes, país de procedencia y responsable de la vacunación.

Podría tener un formato digital, papel y QR para poder presentarlo de diversas formas y portarlo de varias maneras. El Cofesa es un organismo creado en 1981 para coordinar acciones de salud. Está integrado por los 24 ministros de Salud de las provincias argentinas.