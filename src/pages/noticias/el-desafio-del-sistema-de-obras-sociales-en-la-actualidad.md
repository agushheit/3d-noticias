---
category: Agenda Ciudadana
date: 2022-12-11T10:18:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/pulse-trace-163708.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: El desafío del sistema de obras sociales en la actualidad.
title: El desafío del sistema de obras sociales en la actualidad.
entradilla: "¿Qué pasa con las obras sociales? ¿Cómo están funcionando hoy? ¿Cuáles
  son los desafíos que deben afrontar?  Se realizó una charla sobre   el sistema de
  obras sociales en la actualidad. "

---
Para la ocasión, fue invitado Esteban Battioni, bioquímico egresado de la U.N.L; Master en política y gestión de la salud egresado de la Universidad de Bolonia. Profesor de Salud pública en Facultad de Ciencias Médicas de la U.N.L, y en la Universidad de Concepción del Uruguay.

Frente a un puñado de asistentes, y un grupo nutrido de personas con la modalidad “en línea”, Battioni comenzó la exposición hablando de la historia del sistema. Luego se explayó sobre como es la performance del sistema en la actualidad. Afirmó que _“\[…\] partir del año 2000 se genera lo que es el plan médico obligatorio, el PMO, una batería de prestaciones, que las obras sociales están obligadas a prestar y que cubren el 95% de las enfermedades que hay en el país. Y eso las obras sociales lo están cumpliendo muy bien. Hoy vos tenés que una cirugía programada en una obra social, se realiza entre los 20 y los 30 días de la petición. Si comparas contra el sistema público, hoy una cirugía programada tiene seis meses de espera. Por lo tanto, el PMO ordenó el tema de las prestaciones y de esta manera las obras sociales lo están prestando bastante bien”._

Sin embargo, el sistema de obras sociales hoy tiene _“una doble encrucijada, por un lado, por las prestaciones, porque hubo muchas leyes que se sancionaron, como la de salud reproductiva, como la de la identidad de género, de las cirugías congénitas, cardíacas y la Ley de Salud mental, por ejemplo” Dijo, además, que “está bien otorgarlas, pero si se le otorga financiación extra, porque si no tenemos más prestaciones para el mismo presupuesto”. Y por otro lado, “las obras sociales se enfrentan a un gran problema de financiamiento. Las obras sociales están atadas al trabajo, y si el desempleo aumenta, la financiación disminuye. En este contexto pos pandemia el empleo privado creció solo un 2% y se verifica un 20% más de monotributistas. Para tener una idea del desequilibrio que se presenta, un empleado en el sector privado aporta $9.900 pesos en promedio por empleado por mes, mientras que un monotributista aporta en promedio al sistema $ 1.700. Ambos tienen garantizados las mismas prestaciones, a pesar de las diferencias de aportes”._

_“Vamos hacia una crisis de financiamiento de las obras sociales, que es estructural, que va más allá de la administración buena, mala o pésima, de los sindicatos.”, concluyó._

[https://youtu.be/Ke8eyREdteI](https://youtu.be/Ke8eyREdteI "https://youtu.be/Ke8eyREdteI")