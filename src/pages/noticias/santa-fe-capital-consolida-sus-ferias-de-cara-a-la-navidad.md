---
category: La Ciudad
date: 2021-12-22T06:00:17-03:00
thumbnail: https://assets.3dnoticias.com.ar/FERIASNAVIDEÑAS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Santa Fe Capital consolida sus ferias de cara a la Navidad
title: Santa Fe Capital consolida sus ferias de cara a la Navidad
entradilla: 'La Municipalidad informa días y horarios en que funcionan los distintos
  espacios del mercado santafesino dedicados a venta de alimentos, artesanías y manualidades.

'

---
A días de celebrar la Navidad, la Municipalidad recuerda los días y horarios de las Ferias del Mercado Santafesino, que se llevan adelante gracias a la participación de trabajadores y trabajadoras de la economía social y popular.

Ferias alimentarias, de artesanías y manualidades, cada una con su impronta barrial, se desarrollarán desde hoy y hasta el 24 de diciembre inclusive, en distintos puntos de la capital provincial. Estos espacios se consolidan, entonces, en la ciudad, convirtiéndose en una alternativa muy elegida para las compras navideñas.

Desde la Secretaría Integración y Economía Social del municipio, Emilio Scotta, asistente ejecutivo de Mercados y Ferias, expresó que las ferias navideñas “son una oportunidad para seguir conociendo los espacios que hay en la ciudad. Hay algunas que han modificado sus días para brindar a las familias alimentos ricos, sanos, a buen precio y de producción local para la cocina navideña”, informó.

A esto sumó que hay espacios “con productos locales, hechos por artesanos y artesanas manualistas a buen precio y que permiten sostener la economía personal y familiar de muchos santafesinos y santafesinas, al tiempo que generan un círculo virtuoso económico en la ciudad, para seguir promoviendo la economía local”.

**En la Pueyrredón**

La emblemática Feria del Sol y la Luna, en Plaza Pueyrredón, funcionará de martes a jueves de 17 a 23 y el viernes 24 de 10 a 19 horas.

La edición navideña incluye programación infantil desde las 19 horas: el martes 21 se presentará “El gato y los ratones” del elenco municipal de Títeres y el Payaso Califleto. El miércoles 22 se presentarán el Mago Fernán y el Payaso Califleto, mientras que el jueves 23 será el turno del elenco municipal de Títeres con “El país de los colores” y el Mago Fernán.

**Ferias de alimentos**

El miércoles 22 de diciembre se llevará a cabo, como ya es tradición, la Feria Agroecológica de Candioti Sur de 9 a 13 horas, en Caseros y Marcial Candioti, en Plaza Emilio Zola. En tanto, en la zona del microcentro, se realizará la Feria del Mercado del Futuro, de 9 a 14 horas, en Tucumán y 9 de Julio, en Plaza San Martín.

El miércoles 22 y el viernes 24, de 8 a 13 horas, se desarrollará la Feria de las 4 Vías en Mariano Comas y Facundo Zuviría, en Plazoleta Laureano Maradona. En el norte, esperará ambos días por sus visitantes la Feria Popular de Berutti, de 8 a 13 horas, en Blas Parera y Berutti, en Parque Juan B. Justo.

El jueves 23, la Feria de La Verdecita se desarrollará de 8 a 13 horas, en Balcarce 1650, Plaza Pueyrredón.

**Más ferias en plazas**

El jueves 23 de diciembre habrá cuatro ferias que reunirán a emprendedores en distintos espacios públicos: la Plaza Constituyentes alojará de 18 a 24 horas, en la esquina de 4 de Enero y Santiago del Estero, a la Feria Incuba; mientras que la Feria Más Igualdad funcionará en 4 de Enero y Junín.

El jueves también se realizará la Feria Paseo Capital, de 17 a 20 horas en Plaza Aramburu (Arturo Illia, entre 1 de Mayo y 9 de Julio).

En tanto, jueves y viernes, de 16 a 21 y de 9 a 14 horas, respectivamente, se llevará a cabo la Feria Mujeres Emprendedoras CAMEES en San Martín y Juan de Garay, en Plaza Fragata Sarmiento.

**Acompañando procesos colectivos**

Hoy en Santa Fe Capital son más de 40 las ferias populares que funcionan con distintas periodicidades a lo largo y ancho de la ciudad. Scotta dio cuenta del trabajo que impulsa el municipio para promover la economía social: “El objetivo es acompañar la formación de colectivos de personas con objetivos comunes, como la organización de una feria, logrando una mancomunión entre todos los participantes que requieren un lugar para comercializar y visibilizar el emprendimiento que llevan adelante”. Para ello, además de realizar encuentros, el municipio aporta infraestructura que posibilite la puesta en marcha de la feria.

En este proceso -que tiene su origen en organizaciones, instituciones y vecinos y vecinas autoconvocadas- el funcionario precisó que “hemos logrado afianzar un montón de grupalidades y procesos de trabajo colectivos, en los cuales pudimos generar algunos métodos para fortalecerlos. De este modo podemos discutir y abordar la política de ferias que llevamos en cada uno de los espacios”, dijo.

En ese contexto, el municipio busca “brindar a cada feriante, trabajador y trabajadora de la economía popular, un espacio público digno, con oportunidades para generar redes, convivencia y posibilidades de trabajo y comercialización, como una gran vidriera para los emprendimientos que llevan adelante”, concluyó.