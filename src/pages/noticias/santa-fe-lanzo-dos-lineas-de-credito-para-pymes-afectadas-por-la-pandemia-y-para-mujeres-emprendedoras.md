---
category: Agenda Ciudadana
date: 2021-03-20T06:15:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/PYME.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Santa Fe lanzó dos líneas de crédito para pymes afectadas por la pandemia
  y para mujeres emprendedoras
title: Santa Fe lanzó dos líneas de crédito para pymes afectadas por la pandemia y
  para mujeres emprendedoras
entradilla: Ambas líneas serán canalizadas a través de la Agencia para el Desarrollo
  de Santa Fe y la Región (ADER) y estarán destinadas a actividades afectadas por
  la pandemia de Covid.

---
El Gobierno de Santa Fe lanzó dos líneas de crédito a tasa negativa por un monto total de seis millones de pesos cada una, para actividades productivas afectadas por la pandemia y mujeres emprendedoras, informaron fuentes oficiales.

Ambas líneas serán canalizadas a través de la Agencia para el Desarrollo de Santa Fe y la Región (ADER) y estarán destinadas a actividades afectadas por la pandemia de Covid, añadieron los informantes.

Según se informó, los créditos serán por un monto total de hasta 350 mil pesos con una tasa de interés del 13,5% anual, un plazo total de 24 meses, un período de gracia de seis meses y a 18 cuotas.

La primera está destinada a empresas de actividades comerciales, industriales y de servicios “afectadas por las medidas establecidas en el marco de la emergencia sanitaria Covid y comprende un monto de 6 millones de pesos”, explicó el Ministerio de Producción a través de un comunicado.

La segunda apunta a la inclusión financiera con perspectiva de género y está dirigida “a emprendimientos productivos, actividades comerciales o industriales, liderados o integrados en su mayoría por mujeres y disidencias”, por el mismo monto.

Durante la presentación, el ministro de Producción, Daniel Costamagna, valoró “la estructura que tienen las agencias y asociaciones para el desarrollo” así como las mutuales santafesinas, y puntualizó que el año pasado fueron asistidos financieramente 1.500 emprendedores por 162 millones de pesos por esa vía.

“En plena pandemia nos permitió, en forma conjunta con las mutuales y esta gran estructura, acompañar a los emprendedores, a los comercios, a los servicios y a las micro pymes”, dijo el ministro.

Por su parte, la secretaria de Estado de Igualdad y Género, Celia Arena, dijo acerca de la línea de crédito con perspectiva de género que “es muy importante, ya que las mujeres tienen uno de los indicadores preocupantes y que estamos trabajando para resolver, que es la dificultad en el acceso al crédito de las empresas por ellas lideradas”.

“Es una acción muy positiva y una decisión del Gobierno de avanzar con este tipo de políticas que tienen que ver con la autonomía de las mujeres y diversidades”, concluyó.