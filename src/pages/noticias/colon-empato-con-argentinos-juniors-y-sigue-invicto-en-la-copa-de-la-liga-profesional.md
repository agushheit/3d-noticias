---
category: Deportes
date: 2021-04-06T05:36:48-03:00
thumbnail: https://assets.3dnoticias.com.ar/colon-argentinos.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Marisa Lemos
resumen: Colón empató con Argentinos Juniors y sigue invicto en la Copa de la Liga
  Profesional
title: Colón empató con Argentinos Juniors y sigue invicto en la Copa de la Liga Profesional
entradilla: El “Sabalero” igualó sin goles y sigue como líder indiscutido de la Zona
  A.

---
Colón, puntero de la zona 1 de la Copa de la Liga Profesional, y Argentinos empataron sin goles en Santa Fe en un partido parejo y de a ratos intenso por la octava fecha.

El equipo santafesino se encontró con un Argentinos bien plantado en la cancha que disimuló de manera eficaz las bajas por Covid-19, lesiones y suspensiones.

 La visita jugó de igual a igual, concentrado en cada una de sus líneas, y anuló al jugador más importante de Colón, Luis Rodríguez, quien no encontró espacios.

En la próxima fecha, Colón visitará a River Plate y Argentinos recibirá a Central Córdoba de Santiago del Estero.