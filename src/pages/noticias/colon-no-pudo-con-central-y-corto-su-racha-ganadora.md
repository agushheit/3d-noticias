---
category: Deportes
date: 2021-03-24T06:47:17-03:00
thumbnail: https://assets.3dnoticias.com.ar/colon.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Marisa Lemos
resumen: 'Colon no pudo con Central y cortó su racha ganadora '
title: 'Colon no pudo con Central y cortó su racha ganadora '
entradilla: 'En un partido que en los minutos finales tuvo situaciones de riesgo para
  todos los gustos, Central aguantó el empate y sacó un buen punto en casa del líder.

'

---
La alta tensión del final, el ida y vuelta frenético y las situaciones en un arco y en el otro le pusieron al partido la cuota de dramatismo que hasta ese momento había aparecido a cuentagotas. Central intentó frenar la arremetida frenética de Colón y lo hizo cometiendo errores lógicos y poniendo la cara en determinadas ocasiones, pero jugándosela por esa ambiciosa intención de querer llevarse los tres puntos. Muchas veces un 0 a 0 califica, pero esta vez no fue la ocasión. Aún sin poder sumar de a tres, desde la osadía y las ganas fue quizá uno de los partidos que más tranquilo debe haber dejado al Kily en lo que va del torneo.

Y con esa sensación se despidió Colón en el atardecer santafesino. Estuvo muy cerca de ganarlo en ese último tramo del partido. También, podría haberlo perdido si Leonardo Burián, con ayuda del palo, no se estiraba para tapar el cabezazo de Facundo Almada. Fueron las únicas acciones claras, más allá de un pase del Pulga Rodríguez para Facundo Farías que quedó mano a mano, pero en diagonal, resolvió mal.

Por eso mismo Colón detuvo su racha ganadora de cinco partidos consecutivos. Sigue en la cima del Grupo A, claro. Sin embargo, dio un paso atrás desde el aspecto futbolístico. En definitiva, nunca mostrar en el Brigadier López porqué consiguió 15 puntos en la Copa de la Liga Profesional de Fútbol.

“Hay cosas que corregir pero este punto nos sirve. Me voy contento, pero tenemos que ganar el próximo partido en el Gigante”, dijo el Kily González. Su rival será Central Córdoba, otro equipo que sorprendió en el torneo. Colón enfrentará a Platense.