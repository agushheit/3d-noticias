---
category: La Ciudad
date: 2021-01-07T10:31:58Z
thumbnail: https://assets.3dnoticias.com.ar/070121-107.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: El Litoral'
resumen: Con reformas edilicias y tecnológicas se puso en valor la Central del 107
title: Con reformas edilicias y tecnológicas se puso en valor la Central del 107
entradilla: Sumó equipamiento informático de última generación con un software provincial
  único de gestión de emergencias, urgencias y traslados. Se sectorizaron oficinas,
  habitaciones de descanso y una posta sanitaria medicalizada.

---
Para dar respuestas a la demanda creciente por la pandemia de Covid-19 y a pacientes por otras patologías, **se concretó la puesta en valor de la Central Inteligente del 107 de la ciudad de Santa Fe**. Esta nueva dependencia operativa fue pensada como una central de planificación de actividades, desde la cual se dará respuesta a las emergencias, urgencias y traslados de toda la región.

«La Central de Emergencia es el “corazón” del 107. Ahora se puso en valor, se mejoró la estructura edilicia; se sectorizó en dos, en un área está la central de emergencia y en la otra está el área de traslados; y en el medio se ubica la coordinación de la sala de radio, donde trabajan tres radio-operadores por turno, que hacen turnos de 8 horas, un médico regulador que evalúa las llamadas e interviene para enviar la ambulancia», detalló Eduardo Wagner, secretario de Emergencias y Traslado del Ministerio de Salud de la Provincia.

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.15em; font-weight: 700">Mejoras en la Central </span>**

La nueva Central de Emergencia posee una superficie de 150 metros cuadrados; cuenta con dos plantas e ingresos, tanto peatonal como vehicular. 

En la planta baja, se encuentra un hall central donde también funcionan oficinas de facturación; dispositivo de adscripción de personas privadas de la libertad; habitación de descanso para lavadores de ambulancia; habitación de descanso para supervisor de guardia; departamento de enfermería; departamento de choferes; departamento de capacitación; sector de reposición de insumos; un salón de usos múltiples en el que se prevén reuniones periódicas de representantes del 107, Policía, Bomberos, Defensa Civil y Gendarmería Nacional para debatir planes de catástrofe de la región y organizar las coberturas de seguridad de eventos sociales y deportivos. También en este nivel, se encuentra una posta sanitaria medicalizada los 365 días del año las 24 horas.

En la planta superior, se sitúan dos áreas operativas desde la cual se gestionarán las emergencias, derivaciones y traslados; y se encuentran en ambas salas sistemas informáticos en la que quedarán los registros de documentación clínica de la región.

El nuevo edificio sumó equipamiento informático de última generación, y se utiliza un software provincial único de gestión de emergencias, urgencias y traslados, en línea con las otras centrales distribuidas en las distintas regiones sanitarias de la provincia.

**Para la comunicación, se utiliza un sistema TETRA (VHF digital) encriptado, para que nadie externo pueda interferir para modular o escuchar la operatividad.** «Esto sirve para dar un respaldo legal, ya que las llamadas quedan grabadas ante cualquier irregularidad cometida a la hora de dar respuestas a un servicio», sostuvo el secretario.

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.15em; font-weight: 700"> Reconocimiento a trabajadores del 107</span>**

«Hay que reivindicar a los trabajadores del 107, porque desde que empezó la pandemia se habla del hospital público, de las camas de terapia intensiva, de médicos de terapia, y muchas veces este servicio del 107 no es valorado o dimensionado en la magnitud que tiene, y ellos son los primeros en buscar al paciente para llevarlo al hospital o sanatorio», señaló Wagner.

También destacó que el 107 actúa no solo en casos de coronavirus, sino que brinda servicio a heridos de armas de fuego, accidentes, entre otras situaciones.

El servicio del 107 en Santa Fe y su área metropolitana cuenta con 12 ambulancias, «hay dos en Santo Tomé; una en Sauce Viejo, apostada en el aeropuerto; una en Rincón; y las otras ocho en la ciudad de Santa Fe», puntualizó el secretario de Emergencias y Traslado.

Además de la central de emergencia ubicada en la capital provincial (Dr. Zavalla 3367), y que abarca hasta la zona norte provincial, hay otra central que está ubicada en Rosario y cubre la zona sur de «la bota».

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.15em; font-weight: 700"> Autoridades presentes</span>**

En la presentación de la nueva Central de Emergencias del 107 estuvieron presentes: el gobernador Omar Perotti, el intendente Emilio Jatón, Daniela Qüesta, intendenta de Santo Tomé, Silvio González, intendente de San José del Rincón, Pedro Uliambre, presidente comunal de Sauce Viejo y Omar Colombo, intendente de Recreo.