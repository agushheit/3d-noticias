---
category: Estado Real
date: 2021-04-17T06:15:23-03:00
thumbnail: https://assets.3dnoticias.com.ar/CEMAFE.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Provincia sumó camas en el Cemafe para la atención de pacientes ante el
  aumento de los casos de Covid
title: La Provincia sumó camas en el Cemafe para la atención de pacientes ante el
  aumento de los casos de Covid
entradilla: Son un total de 20. El objetivo es descomprimir la ocupación en hospitales
  de la ciudad capital

---
La provincia de Santa Fe, a través del Ministerio de Salud, sumó 20 camas más para la atención de pacientes en un marco de gran ocupación en efectores públicos y privados que estarán dispuestas en el Centro de Especialidades Médicas Ambulatorias de Santa Fe (CEMAFE).

Al respecto, Jorge Prieto, secretario de Salud de la provincia, indicó: "La segunda ola –que ya impacta en la provincia– tiene una dinámica absolutamente diferente a la del año pasado. En 2020 habíamos podido reorganizar y ampliar en un 127 por ciento la capacidad del sistema sanitario, y priorizar la atención e internación de casos de Covid; lo que fue también posible por la considerable reducción de la circulación, un menor ingreso por accidentes y casos de violencia. Hoy la situación no es la misma: nos encuentra con un nivel de ocupación alto por patologías no Covid".

Y prosiguió: “El Ministerio de Salud está trabajando junto a todos los efectores con internación para diseñar una estrategia conjunta en función de este nuevo escenario. Lo primero que la provincia dispuso fue la creación de 20 plazas para internación en este Centro de excelencia en abordaje ambulatorio, en una coyuntura que hoy nos exige rediseñar minuto a minuto el funcionamiento de la red, para garantizar que ninguna persona se quede sin recibir asistencia como sucedió hasta el momento”.

Posteriormente, tras reiterar que las internaciones en áreas de alta y mediana complejidad de hospitales y sanatorios provinciales no se vincula a casos de coronavirus, Prieto explicó las razones. "Este es el esperado efecto tras la primera ola: las patologías crónicas sin atender y algunas consultas que ingresan de forma tardía y que deben ser resueltas en el tercer nivel de atención", sintetizó.

Del mismo modo, el secretario de Salud destacó: “El factor crítico son nuestros recursos humanos, lo más valioso; y eso es algo que no podemos comprar. El personal está agotado, dejó todo, y tenemos un gran agradecimiento por el enorme esfuerzo que vienen realizando. Los equipos de salud se han puesto esto al hombro; y tienen una responsabilidad enorme, por eso no tenemos más que palabras de agradecimiento y reconocimiento”.

Durante la conferencia de prensa que brindara el funcionario provincial en el Cemafe, estuvo acompañado por el director del Cemafe; Fabián Mendoza y los directores de los hospitales Iturraspe y Cullen, Francisco Viilano y Juan Pablo Poletti, respectivamente.

**Reordenamiento**

En otro orden, y tras hacer un enérgico llamado a acatar las restricciones y profundizar los cuidados personales y sociales, Prieto describió: “El impacto de la segunda ola va a ser corto, pero muy expansivo. Y vemos con preocupación la cantidad de personas muy jóvenes con cuadros graves de Covid”.

En el mismo sentido se expresó Villano: “El jueves fue un día bastante crítico por la disponibilidad de camas, si bien teníamos muchos pacientes Covid internados, unos 25, entre área crítica y no crítica, hay otros lugares del hospital que están ocupados”, y agregó: “Teníamos una sola cama disponible en todo el hospital para patologías de clínica general y Covid, y hoy ya tenemos 10 camas. Esto se pudo lograr porque se habló con el sector privado y se pudo derivar pacientes”.

Para atender la segunda ola de contagios, el director del Iturraspe dijo que “se ampliaron nuevamente camas en el sector de terapia intensiva y se suspendieron las actividades quirúrgicas programadas que no tienen ninguna complicación”.

Por su parte, Poletti explicó: “Estamos trabajando en un sistema de red. Más allá de que el Cullen no tenga camas, sabemos que el Iturraspe viejo o el nuevo, sí las tiene. Más allá de la logística que se haga para tener mayor número de camas, hay que destacar que se trabaja con un sistema de gestión integrado de camas”.

Finalmente, Mendoza aseguró que “atento a la coyuntura es imperioso que el Cemafe se adapte para contener la creciente demanda; y para ello no solo ponemos a total disposición todos nuestros recursos, sino que estamos incorporando médicos y médicos teniendo en cuenta esta realidad”.