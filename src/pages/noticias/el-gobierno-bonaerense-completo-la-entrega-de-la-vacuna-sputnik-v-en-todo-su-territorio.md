---
category: Agenda Ciudadana
date: 2021-01-09T10:39:13Z
thumbnail: https://assets.3dnoticias.com.ar/090121-vacunacion-sputnik.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: '3DNoticias - Fuente: Télam'
resumen: El Gobierno bonaerense completó la entrega de la vacuna Sputnik V en todo
  su territorio
title: El Gobierno bonaerense completó la entrega de la vacuna Sputnik V en todo su
  territorio
entradilla: La campaña se desarrolló en los 135 municipios y este viernes se completó
  con la entrega de 450 dosis al municipio de Capitán Sarmiento.

---
El gobierno de la Provincia de Buenos Aires completó la distribución de la primera tanda de la vacuna Sputnik V en los 135 municipios bonaerenses, al entregar 450 dosis al municipio de Capitán Sarmiento, informó el Ministerio de Salud bonaerense.

El jefe de Gabinete del Ministerio de Salud bonaerense, Salvador Giorgi, junto con la subsecretaria técnica, administrativa y legal, Victoria Anadón, visitaron Capitán Sarmiento en donde supervisaron la llegada de 450 dosis de vacunas contra el coronavirus que le corresponden al distrito y, con esta entrega de las dosis, se completó la distribución de la Sputnik V en los 135 municipios bonaerenses.

En una rueda de prensa posterior, Giorgi explicó la finalización de la entrega para la primera tanda de vacunas: «hoy llegamos con la vacuna a los 135 distritos de la Provincia, en 160 hospitales entre municipales y provinciales ya se está vacunando y tenemos a más de 600 mil bonaerenses registrados en la página».

El funcionario provincial recordó que, en esta primera etapa, el plan de vacunación «inicia con los trabajadores de Salud, continúa con los mayores de 60 años, luego con personas entre 18 y 59 años con factores de riesgo y después con la población de policías, docentes y auxiliares docentes».

«A este ritmo, pensamos que para mayo/abril estará vacunada toda esa población, para que en el otoño/invierno se pueda evitar una segunda ola trágica, como están teniendo los países del hemisferio norte», sostuvo en un comunicado.

Por su parte, Anadón, remarcó la emoción y orgullo de poder acompañar a todos los distritos en esta primera etapa de la vacunación: «nos emociona ver la emoción (sic) de los y las trabajadoras de la Salud por lo que implica para sus vidas, sus familias, esta primera etapa de vacunación».

La funcionaria agregó que acompañaron «el primer día en Quilmes y hoy en Capitán Sarmiento, pensando también en lo que se va a vivir en toda la provincia cuando llegue la segunda tanda de dosis y, entonces, la población en general podrá acceder a una nueva vida, que es lo que todos estamos esperando».

En tanto, el intendente de Capitán Sarmiento, Javier Iguacel, destacó: «estamos muy contentos de haber recibido las vacunas y de poder colaborar con la provincia para que esto sea un éxito. Es una jugada más de este partido que se está dando. Es la primera vez que podemos atacar al coronavirus y no nos tenemos que defender de él», concluyó.