---
category: Agenda Ciudadana
date: 2021-01-29T10:32:23Z
thumbnail: https://assets.3dnoticias.com.ar/epe.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Con información de Diario Uno
resumen: La EPE aumenta sus tarifas a partir del mes de febrero
title: La EPE aumenta sus tarifas a partir del mes de febrero
entradilla: La empresa informó que trasladará el incremento del precio mayorista de
  la electricidad dispuesto a nivel nacional, que no se había aplicado en 2019. Será
  una suba promedio del 4 por ciento para usuarios no residenciales

---
La Empresa Provincial de la Energía (EPE) subirá las tarifas a partir del 1º de febrero próximo para usuarios no residenciales. La luz costará un 4 por ciento más caro en promedio. Este aumento impactará más en el sector industrial, que deberá abonar un 6,7 por ciento, en tanto que los comerciantes notarán un incremento del 6,1 por ciento. La suba también llegará a los hogares, que recibirán las boletas de luz con un 0,6 por ciento de incremento.

La Empresa Provincial de la Energía (EPE) explicó que la suba se debe a que el precio mayorista del servicio no fue trasladado a los usuarios ni en mayo ni en agosto de 2019.

A su vez, desde la EPE indicaron que el cuadro tarifario no se modificaba desde marzo de 2019, lo cual significó un ahorro mayor a los 2 mil millones de pesos para los usuarios.

Cabe aclarar que del 1.400.000 usuarios de la EPE, 252.690 acceden a tarifas subsidiadas, de los cuales 164.134 son beneficiarios de la tarifa social provincial, 85.497 corresponden a la tarifa provincial de jubilados, 684 son electrodependientes, 1.781 instituciones deportivas, 32 empresas recuperadas y 562 parques industriales.

La factura de una distribuidora eléctrica está conformada por la compra de energía, el valor agregado de distribución (VAD) y los impuestos municipales, provinciales y nacionales que se aplican a la tarifa.

El cuadro tarifario para todos los tipos de usuarios, se encuentra disponible en la página web www.epe.santafe.gov.ar.

La EPE abastece a más de 1.400.000 usuarios, con una densidad de 11 suministros por kilómetro cuadrado. La empresa opera 74 estaciones transformadoras, más de 54.000 kilómetros de líneas de alta, media y baja tensión, de las cuales 20.367 kilómetros son redes aéreas destinadas a abastecer el sector rural.

Por otra parte, la EPE recordó la vigencia de un conjunto de medidas comerciales que dispuso el gobierno de la provincia durante el 2020, para mitigar los efectos de la pandemia.

Las disposiciones aprobadas establecieron que, para las pequeñas demandas, la tasa de actualización en el período de Aislamiento Social, Preventivo y Obligatorio (ASPO) se mantiene a valores históricos, para los usuarios comprendidos en el decreto provincial 283. Para aquellos que no están alcanzados por dicha norma, hay una baja del 24 al 12%.

**Planes de pago**

La empresa explicó que desde el año pasado se pueden realizar planes de pagos en 6 cuotas mensuales y consecutivas, con un interés sobre saldo del 9 por ciento anual. También existe la posibilidad de pagar en 12 cuotas con un interés del 15 por ciento, o en 24 cuotas con el 24%de interés.

En el caso de abonar de contado, no se aplican intereses para los compromisos que vencían entre fines de marzo y setiembre de este año.

Es importante destacar que para los grandes usuarios se aplicó una reducción en la tasa de actualización para las deudas anteriores al 21 de marzo de este año, que bajó del 49 por ciento al 30.

En tanto, para los alcanzados por el decreto 283, que exceptuaba del corte del servicio, la tasa será del 0 por ciento durante el período del aislamiento obligatorio, y los no comprendidos en dicha norma, la tasa de actualización disminuye del 30 al 15 por ciento.