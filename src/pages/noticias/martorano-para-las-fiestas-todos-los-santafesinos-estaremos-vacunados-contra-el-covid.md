---
category: Agenda Ciudadana
date: 2021-10-16T06:00:54-03:00
thumbnail: https://assets.3dnoticias.com.ar/MARTORANO.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'Martorano: "Para las fiestas todos los santafesinos estaremos vacunados
  contra el covid"'
title: 'Martorano: "Para las fiestas todos los santafesinos estaremos vacunados contra
  el covid"'
entradilla: " Esta semana la provincia cerró el círculo etario con el comienzo de
  la vacunación de los menores de 11 años. Cómo piensan terminar el operativo antes
  de fin de año.\n\n"

---
La ministra de Salud Sonia Martorano tiene todos los números sobre la mesa. Es un tetris de primeras y segundas dosis, de distintas marcas, con diferentes protocolos de frío y cuidados, destinadas a cada sector de la población santafesina que todavía aguarda recibir la vacuna contra el coronavirus. A eso le suma los que ya se vacunaron, los que faltan. Todo encaja, se ordena y resulta efectivo. Pero los números cambian a cada rato con los nuevos inscriptos que aparecen en el sistema. Y entonces hay que empezar de nuevo. Lejana parece haber quedado aquella mañana del martes 29 de diciembre del 2020, cuando en una sala del hospital Cullen se aplicó la primera vacuna en Santa Fe. Era apenas el comienzo de un año calendario que pretende terminar con la tarea de inmunización cumplida. Parece una vida. En verdad son millones.

 Hace las cuentas y dice primero, Martorano: "Hoy, a esta hora, hay 2.076.271 santafesinos de todas las edades con esquema de vacunación completo", y piensa en voz alta: "Nos van quedando pequeños grupos por vacunar. Es el 60 por ciento de la población y con lo que falta alcanzaremos en poco tiempo al 70 por ciento, ya estamos hablando de otro tema", dice, en la carrera por alcanzar la inmunización de rebaño.

 Es el análisis cotidiano, hora a hora, de quien al inicio de la pandemia ocupaba el cargo de secretaria de Salud provincial y, tras el alejamiento de la cartera sanitaria del doctor Carlos Parola, cuando el mundo se caía a pedazos ella tomó las riendas de este caballo que parecía desbocado: la lucha contra el sars-cov2, la lucha por proteger la salud de la población, por seguir vivos.

"Por suerte, todos los días aumenta el número de inscriptos para vacunarse", dice la ministra en la entrevista a fondo con El Litoral. "2.000.820 inscriptos", apunta, "ya aumentaron como 100 mil esta semana", agrega. Más tarde dirá que "antes de las fiestas de fin de año toda la población de la provincia estará inmunizada contra el coronavirus". Es la frase que todos esperan, es el alivio. Es el título. Algo que hasta hace muy poco parecía un imposible. Porque el contraste es muy grande. En menos de un mes Santa Fe pasó de tener las salas de Terapia de los principales hospitales casi desbordadas de pacientes con el virus, con una carpa sanitaria militar de emergencia montada frente al hospital Cullen, a tener hoy apenas 42 nuevos casos diarios en toda la "bota". Y sólo 2 en la Capital.

 Esto es consecuencia directa del efecto de la vacuna contra el virus y de los cuidados que perduran en la población que tomó consciencia de la gravedad de la situación. La regla de tres simple: distanciamiento + tapabocas + higiene de manos. El resultado es + vida, – muerte.

 **Lo que falta**

 Los números siguen dispersos sobre el escritorio del despacho de la ministra, al igual que en su cabeza. Para completar el esquema de vacunación "a Santa Fe le faltan sólo 420 mil dosis -hace las cuentas Martorano-, las que llegarán desde Nación entre lo que resta de octubre y noviembre, tal como está previsto. Las demás dosis ya están acá". Y están siendo aplicadas día tras día. Son 4.576.895 las vacunas que ya se aplicaron, de un total de 5.005.185 recibidas, lo que arroja un porcentaje del 91,44 aplicado (al 14 de octubre).

 Pero lo que falta no le preocupa demasiado a la ministra. "Hemos llegado a colocar 800 mil dosis en un mes, así que 400 mil no es problema", piensa, "depende de la capacidad del envío y no de la colocación". Luego evalúa la situación según el tipo de vacuna: "Me falta algo de Astrazéneca, que lo puedo combinar con Pfizer… ¿qué más me falta? -piensa y dice-. Sinopharm no es problema, porque hay 'a rolete'. Me falta algo de Sputnik, que en caso de ser necesario se combinará con Pfizer. Así que vamos a tener vacunas, vamos a tener...", repite, se repite. "El mayor problema es lo que me falta de Astra (Zéneca), que en todo caso la combinaremos con Pfizer, que viene en el esquema de intercambiabilidad", termina su razonamiento ante El Litoral.

 Para inocular contra el virus a la población, en la ciudad de Santa Fe -al igual que en otras localidades- se montó un gran operativo vacunatorio. Se abrieron varios centros a los que asisten a diario los vecinos a recibir en forma gratuita su vacuna. Estos son La Esquina Encendida, La Redonda, el viejo Hospital Iturraspe, el Alassia. También vacunaron en el Cemafe y el CEF 29, de avenida Galicia. Allí los reciben unos 350 vacunadores, en su mayoría mujeres, que conforman el escuadrón de la vida y el amor. Al igual que las y los voluntarios que coordinan el operativo, acompañan y guían a la gente.

 **Vacunaditos**

Primero les tocó a los sanitaristas y los adultos mayores, luego se fue descendiendo en edades, siempre priorizando a los grupos de riesgo. Esta semana comenzó el turno para los más chiquitos, a los que les indicaron la Sinopharm. Ya vacunaron a 3.500 menores de entre 11 y 3 años con vulnerabilidad y ahora están haciendo lo mismo con el resto de ese grupo de la población que no tiene problemas de salud (son 450 mil). "La vacunación de los de 12 a 17 años va más despacio por un tema de logística", dice Martorano. Es que les aplican la Pfizer, que demanda mayores cuidados y preparados de la vacuna. "Es muy compleja, tiene una logística tremeeenda", expresa. "Así que distribuirla en toda la provincia es algo muy complejo -insiste-. Pero ya llevamos colocadas más de 30 mil dosis, de un total de 38 mil jóvenes de 17 años a vacunar. Y luego nos van a quedar los de 16, para finalmente en noviembre abocarnos a los niños y niñas".

  "Así que si me pedís hoy un panorama, tenemos al 60 por ciento de la población vacunada y, de seguir viniendo vacunas (como se espera), para diciembre podemos tener el panorama completo", sentencia la ministra de Salud provincial a El Litoral, que está contenta porque "los más chiquitos empezaron a desinhibirse y se están inscribiendo, yo me imaginaba que iba a suceder así", dice. Lo que le preocupa son los adolescentes. "A algunos les llega el turno un viernes y no van, porque tienen miedo de sentirse mal y perderse el fin de semana. ¡Que concurran a los turnos!, porque después te caen todos el lunes y te la complejizan", se enoja. "Así que vayan cuando les toca, es un tema de solidaridad", les pide.

 -¿Cuáles son hoy las prioridades?

 -Lo urgente es completar esquemas. Hoy la prioridad es colocar las segundas dosis. Así que si llegaron Sputnik, ¡a colocar Sputnik a todo el mundo! ¿Hay que completar de Sinopharm?, ¡completemos! Y colocar Pfizer de primera (dosis) y la semana que viene, de segundas -parece dar órdenes en su respuesta-. Mientras tanto, ir colocando despacito las primeras dosis de 12 a 17 años, porque Pfizer lleva más tiempo, y de 3 a 11 con Sinopharm, que es una vacuna generosa, "gauchita", que la podés llevar a todos lados sin problemas y no da fiebre… linda vacunita. Aunque a los chiquitines les tenemos que dedicar más tiempo, y les revisamos el carné de vacunación para colocarles la del sarampión y las que les falten. Así que vamos bien, vamos bien.

 **Inmunidad**

El coronavirus afecta en menor medida a los más chicos. "Aunque el problema allí es que se transforman en el reservorio del virus, donde quedaría viviendo el Covid y generando el circulo por el cual podrían enfermarse los adultos. En cambio, cuando vacunemos a los chicos evitaremos la circulación", dice Martorano.

 Sobre el final de la entrevista la ministra dice: "Estoy haciendo cuentas, parezco una contadora (se ríe). Me faltan tantas acá, tantas allá, estas pasalas para acá, a ver qué le pido a Nación, para poder completar la vacunación lo antes posible", dice, y luego confirma: "Sí, sí, para fines de diciembre estaremos con todos los esquemas completos, para las fiestas todos vacunados", celebra.

 Lo que resta luego es aplicar una tercera dosis a los inmunodeprimidos y los trabajadores de salud, que fueron los primeros en vacunarse y ya transcurrió mucho tiempo con exposición al virus. "La idea es esa, sí", afirma Martorano, y termina: "Después vendrá para todos, pero ya no será una tercera dosis, sino que será una vacuna más del calendario, porque probablemente será un refuerzo anual. Es lo que yo entiendo, porque en verdad es tan poco lo que sabemos de esta enfermedad que habrá que aguardar lo que diga la Organización Mundial de la Salud".

 A modo de reflexión, la ministra destaca que "primero, la vacuna era un bien escaso en el mundo entero, deseado por todos, porque tenía que ver con la vida o la muerte. En Santa Fe hicimos un trabajo impecable, respetamos un orden determinado, y terminado este mega operativo, con todos vacunados, pienso que si viene una nueva dosis se aplicará en los hospitales, como cualquier otra vacuna, como la antigripal", finaliza. Es mediodía y le queda por delante tomar varias decisiones. Sus papeles con los números permanecen sobre su escritorio.