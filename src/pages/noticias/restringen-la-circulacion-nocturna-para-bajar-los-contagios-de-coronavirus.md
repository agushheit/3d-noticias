---
category: Agenda Ciudadana
date: 2021-01-08T01:20:37Z
thumbnail: https://assets.3dnoticias.com.ar/080121-cafiero.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Télam'
resumen: Restringen la circulación nocturna para bajar los contagios de coronavirus
title: Restringen la circulación nocturna para bajar los contagios de coronavirus
entradilla: La medida será reglamentada con un decreto que fija una serie de límites
  a la circulación, entre ellas la de personas que no realicen tareas esenciales durante
  la madrugada.

---
El Gobierno publicará en las próximas horas un decreto que fija una serie de medidas para limitar la circulación, entre ellas la de personas que no realicen tareas esenciales durante la madrugada, ante la preocupación de las autoridades nacionales y de los gobernadores por el aumento de los contagios de coronavirus y el riesgo de colapso en los sistemas sanitarios.

El presidente Alberto Fernández, el jefe de Gabinete, Santiago Cafiero; la secretaria Legal y Técnica, Vilma Ibarra; y la secretaria de Acceso a la Salud, Carla Vizzotti, mantuvieron una reunión para terminar de delinear el texto del decreto que restringirá la circulación durante la madrugada.

«El decreto se está redactando», indicó una alta fuente gubernamental a los periodistas acreditados en Casa Rosada y señaló que el Gobierno nacional «fijará las pautas generales que serán luego adoptadas por cada provincia».

Cafiero adelantó que las nuevas disposiciones «regirán a partir de mañana», con su publicación en el Boletín Oficial.

En este marco, el gobernador de Chaco, Jorge Capitanich, dijo en diálogo con la prensa en la Casa Rosada que la circulación de personas estará prohibida entre las 0 y las 6 horas de cada día, a excepción de los trabajadores esenciales.

Capitanich afirmó que esa restricción ya se aplica en Chaco y hay un acuerdo general sobre esa idea en todas las provincias, porque una prohibición total resultaría imposible por el impacto en materia económica y social.

El DNU presidencial da un marco general, pero el cumplimiento dependerá de las jurisdicciones. De hecho, el gobernador de Córdoba, Juan Schiaretti, declaró que la restricción nocturna no será aplicada en el distrito, en un discurso que pronunció en el Centro Cívico de la capital provincial.

Las nuevas medidas se presentan como el correlato de la reunión que el Presidente mantuvo por videoconferencia con los gobernadores y con el jefe de Gobierno porteño, Horacio Rodríguez Larreta, durante tres horas y media, ante la creciente preocupación por el incremento de casos positivos de Covid-19.

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.15em; font-weight: 700">Las cifras de la pandemia</span>**

Según el último reporte difundido anoche por el Ministerio de Salud, otras 191 personas murieron y 13.441 fueron reportadas con coronavirus en las últimas 24 horas en la Argentina, con lo que suman 43.976 los fallecidos registrados oficialmente a nivel nacional y 1.676.171 los contagiados desde el inicio de la pandemia.

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.15em; font-weight: 700">Medidas consensuadas</span>**

El Presidente le propuso a los mandatarios provinciales limitar la nocturnidad, acotar los viajes al exterior, y exigir test PCR en el momento que un pasajero aéreo llega a cualquier provincia, en especial destinos donde hay mayor número de casos.

También **se sugirió que los gobernadores tengan margen de aplicación de la medida**, según los distritos, puedan acotar a 10 el número de personas en reuniones y se bloquee el uso del transporte público para quienes no son trabajadores esenciales.

En el encuentro del miércoles hubo acuerdo entre el Gobierno nacional y las provincias en «no parar la economía, ni frustrar vacaciones ni la actividad en lugares turísticos», según indicaron los participantes del encuentro.

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.15em; font-weight: 700">La temporada</span>**

Si bien no dio mayores precisiones sobre el contenido del decreto, Cafiero aseguró que las restricciones que serán anunciadas en las próximas horas "no van a alterar la temporada" de verano.

«Queremos que la temporada continúe y sea de cuidados. Queremos defender el trabajo, la producción y los comercios. Necesitamos que se extremen cuidados para que la temporada pueda continuar. La temporada no se va a ver alterada en lo más mínimo por esto», sostuvo el ministro coordinador.

**Tal como se decidió el miércoles, una vez conocido el texto del decreto, cada provincia irá adaptando la norma a las particularidades y la situación sanitaria de su distrito.**

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.15em; font-weight: 700">Ciudad de Buenos Aires</span>**

Desde la ciudad de Buenos Aires, el jefe de Gobierno, Horacio Rodríguez Larreta –contagiado de coronavirus y aislado en su domicilio– planifica una estrategia centrada en reducir los encuentros sociales.

Fuentes del Ejecutivo porteño indicaron que el foco de la estrategia de la Ciudad está puesto en reducir los encuentros sociales y en recuperar la responsabilidad social y cívica que supimos tener en los primeros meses del aislamiento por la pandemia.

En principio, en la Ciudad no creen en la necesidad de restringir la circulación y, además, afirman que la ocupación del sistema sanitario sigue siendo estable.

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.15em; font-weight: 700">Provincia de Buenos Aires</span>**

En tanto, en la provincia de Buenos Aires se analiza la tasa de incidencia por comuna, con el objetivo de que aquellas ciudades que están en una mejor situación epidemiológica no tengan que acatar las nuevas restricciones que vayan a implementarse.

No todos los municipios atraviesan una situación epidemiológica compleja, por lo que el Gobierno no evalúa implementar un cierre generalizado.

Esta semana, un total de 15 distritos de la provincia retrocedieron de fase por el aumento de contagios de coronavirus: Arrecifes, Balcarce, Lincoln, Lobería, Puán, Suipacha, Salliqueló; Adolfo Alsina, Coronel Dorrego, General Alvear, General Villegas, Las Heras, Magdalena, Maipú y Saavedra.