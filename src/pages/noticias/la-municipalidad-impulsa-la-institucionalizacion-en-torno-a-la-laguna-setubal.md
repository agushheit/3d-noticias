---
category: La Ciudad
date: 2022-01-08T06:00:22-03:00
thumbnail: https://assets.3dnoticias.com.ar/LAGUNABAJA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Municipalidad impulsa la institucionalización en torno a la laguna Setúbal
title: La Municipalidad impulsa la institucionalización en torno a la laguna Setúbal
entradilla: 'Funcionarios municipales, integrantes del Conicet, representantes de
  clubes náuticos y concesionarios de paradores de playa continúan con los encuentros
  tendientes a avanzar en una regulación colectiva.

'

---
El municipio continúa con los encuentros tendientes a avanzar en el ordenamiento de la laguna Setúbal y las actividades que en ella se realizan. Con ese objetivo, se efectúan una serie de reuniones de las que participan funcionarios municipales de diferentes áreas, integrantes del Conicet, representantes de clubes náuticos y concesionarios de paradores de playa.

En este espacio, entre otros puntos, se debate la ubicación de luminarias que dan al espejo de agua, la disposición de las zonas destinadas a bosques nativos, la delimitación de espacios para la práctica de deportes y el área de navegación. Además, uno de los temas recurrentes es la bajante que se registra y la aparición de nuevos islotes.

El subdirector de Deportes del municipio, Axel Menor, indicó que desde el municipio se impulsa la institucionalización de la laguna, es decir, que se conforme un grupo con autonomía para tomar decisiones y definir qué iniciativas pueden llevarse adelante.

En ese sentido, mencionó que la intención es “producir el necesario ordenamiento lógico de la laguna, pero la idea es que se haga entre todos, tanto dentro del agua como en el sector de playa y en los terrenos que están ocupados y dan a la Setúbal”. Además, añadió que “la idea es que lo integre la mayor cantidad de participantes posibles, es decir, todos aquellos que tengan convivencia con la laguna y sus espacios aledaños”.

De este modo, confió en que próximamente se sumen a los encuentros, autoridades provinciales y de Prefectura Naval, para poder atender cuestiones relacionadas con, por ejemplo, la seguridad.

La próxima reunión está programada para fines del mes de enero.