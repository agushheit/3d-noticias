---
category: La Ciudad
date: 2021-12-29T06:00:25-03:00
thumbnail: https://assets.3dnoticias.com.ar/SALUDLABORAL.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D
resumen: Más de 800 trabajadores municipales participaron de la campaña de salud laboral
title: Más de 800 trabajadores municipales participaron de la campaña de salud laboral
entradilla: La iniciativa incluyó varias instancias, entre ellas, una encuesta para
  conocer sobre enfermedades, hábitos de vida y costumbres nutricionales, en una tarea
  conjunta entre el municipio y Asoem

---
Recientemente se conocieron los resultados de la campaña de prevención de enfermedades crónicas no transmisibles y promoción de hábitos saludables que desarrollaron en conjunto la Municipalidad de Santa Fe y el gremio Asoem.

La iniciativa alcanzó a un total de 813 trabajadores, que fueron consultados a través de una encuesta sobre patologías crónicas, alergias, calendario de vacunación, actividad física, hábitos de vida y costumbres nutricionales. Además, se llevaron a cabo mediciones de parámetros vitales y antropométricos. Al finalizar la encuesta, a cada trabajador se le recomendó realizar un control médico cuya urgencia estuvo sujeta a los resultados que arrojó la valoración del estado de salud.

La propuesta se desarrolló desde el 4 de octubre al 21 de noviembre de este año en diferentes dependencias municipales, con el objetivo de hacer una valoración del estado de salud de los agentes municipales, como así también de sensibilizar y educar en la prevención de factores de riesgo.

A partir de los resultados obtenidos, se propone a mediano plazo generar intervenciones dirigidas a la modificación del estilo de vida y la adquisición de hábitos saludables: alimentación saludable, actividad física, control del peso y perímetro abdominal, y abandono de hábitos nocivos, especialmente el tabaquismo.

**Informe final**

De las 813 personas encuestadas, 49,8% (405) fueron de sexo femenino y 50,2% (408) de sexo masculino. El 70,5% del total de encuestados estuvo concentrado en los grupos etarios comprendidos entre los 30 y 64 años.

En cuanto a antecedentes médicos, del total de encuestados, el 17,8% (145) auto-referenció tener alguna alergia; un 29,6% (241) del total reportó haber tenido Covid-19 desde el comienzo de la pandemia; y el 51,4 % (418) de los trabajadores declaró haber sido sometido a alguna cirugía en algún momento de su vida. Del total de trabajadores, el 84,4% (686) manifestó tener el Calendario de Vacunación al día

En lo relativo a hábitos nutricionales y de vida, el 32,7% del total declaró realizar actividad física de dos a tres veces por semana, mientras que el 27,9% reconoció hábitos de riesgo al manifestar la no realización de actividad física alguna en ningún momento de la semana. Del mismo modo, 25.5% informó que fuma.

Casi el 60,0% de los trabajadores manifestaron tomar al menos dos litros de agua al día, mientras que los alimentos consumidos con mayor frecuencia (cinco días a la semana o más) fueron las verduras (52,8%), las frutas (42,7%) y los panificados (41,3%). En tanto, los menos consumidos fueron los frutos secos (45,9% manifestó no consumirlos nunca), seguido del pescado (32,1%).

Por su parte, el 46.9% de los agentes municipales que participaron en la campaña, presenta al menos una patología crónica como hipertensión arterial, obesidad y diabetes. Además, el 61,2% de las mujeres y el 75,2% de los varones registra exceso de peso. El 26% mostró cifras elevadas (mayores o iguales a 140/90) de tensión arterial con mayor prevalencia en sexo masculino, y el 11.6% supero los 140 g/dl de glucemia en sangre, luego de dos horas de ayuno, siendo mayor en el sexo femenino.

En ese contexto, se aconsejó control médico urgente al 3,1% de los trabajadores municipales, mientras que a más de la mitad de los trabajadores (57,3%) se le recomendó realizarlo dentro del mes y al 35,1% de forma habitual. Al analizarse el parámetro por sexo, se observó que el control médico dentro del mes o de manera urgente, se recomendó en mayor proporción al sexo masculino (45%) que al sexo femenino (30,1%).