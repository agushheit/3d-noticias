---
category: Estado Real
date: 2021-01-18T09:00:46Z
thumbnail: https://assets.3dnoticias.com.ar/playas.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: 'Santa Fe inicia un intenso operativo de prevención en salud: Aborda Verano'
title: 'Santa Fe inicia un intenso operativo de prevención en salud: Aborda Verano'
entradilla: De manera interministerial, la provincia establecerá puntos fijos dando
  información y haciendo controles sobre dengue y Covid en playas y terminales.

---
La provincia de Santa Fe, a través del Ministerio de Salud, informó que desde este fin de semana comenzaron operativos territoriales intensivos de promoción y prevención en salud, desplegados inicialmente en lugares estratégicos de las regiones más pobladas y con mayor circulación de personas: Rosario y Santa Fe.

Los mismos se enmarcan en el plan denominado Aborda Verano y estarán básicamente enfocados en dos objetivos: concientizar y evitar enfermedades como dengue (y otras transmitidas por mosquitos) y el Covid-19.

En el caso de la Región Santa Fe de Salud habrá tres puntos fijos, según explicó el responsable de la misma, Rodolfo Rosselli: en la zona de playas, en la Costanera Oeste; y en la zona de bulevar, en la Plaza Pueyrredón, ambos los días viernes, sábados y domingos.

El tercero estará en Estación Terminal de Ómnibus, de lunes a viernes por la tarde, y los sábados y domingos mañana y tarde.

“A todas las personas que ingresen y egresen por la Terminal se le realizarán, sin excepción, tests de anosmia (olfato) en tres puestos fijos para identificar posibles casos sospechosos de Covid, se les brindará información y consejería sobre cómo prevenir esta enfermedad y el dengue. Habrá presencia en los horarios pico de circulación que se reforzará los fines de semana. Ante cualquier caso sospechoso se activarán los protocolos de rigor, con intervención del 0800 COVID”, profundizó Rosselli.

Asimismo, en zona de playas y de plazas se generarán instancias de diálogo, consejería y entrega de materiales informativos y preventivos.

Finalmente, destacó que del Aborda Verano también participarán activamente los ministerios de Educación y de Seguridad provinciales.

**Región Rosario**  

Mientras tanto y con los mismos objetivos, en la ciudad de Rosario habrá tres puestos fijos en el marco del plan Aborda Verano.

Uno en Arturo Illia y Moreno, en el horario de 10 a 13; otro en Av. Eudoro Carrasco y Gallo (Florida) de 12 a 16, y el tercero en Cochabamba y Oroño, de 8:30 a 12:30.

Al respecto Sebastián Torres, coordinador de Abordajes Territoriales de Salud provincial, precisó que “promotores sanitarios estarán presentes en cinco puntos brindando información y consejería sobre dengue y Covid, repartiendo tapabocas, entre otras actividades de prevención”.

![](https://assets.3dnoticias.com.ar/playa1.jpeg)