---
category: La Ciudad
date: 2022-01-21T06:15:13-03:00
thumbnail: https://assets.3dnoticias.com.ar/utasanta.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'El boleto de colectivo aumenta un 40% en la ciudad: así quedaron las tarifas'
title: 'El boleto de colectivo aumenta un 40% en la ciudad: así quedaron las tarifas'
entradilla: 'La Municipalidad de Santa Fe estableció las nuevas tarifas para el transporte
  de colectivos. En el boleto frecuente, la suba supera el 40 por ciento.

  '

---
A partir del 1 de febrero, el precio del boleto de colectivos en la ciudad pasará de 42,35 pesos a un valor de 59,35 pesos. Así lo reveló la directora de Movilidad de la Municipalidad de Santa Fe, Andrea Zorzón.

Se trata de una suba del más del 40 por ciento en el boleto frecuente. El resto de las tarifas queda de la siguiente manera:

Cabe recordar que las empresas habían solicitado una suba del 85% y la última actualización tarifaria se otorgó hace un año. Andrea Zorzón, señaló que la medida se toma "luego de un análisis exhaustivo de los costos y teniendo en cuenta la situación económica de las familias santafesinas, pero también el sostenimiento del sistema"

Indicó que "las empresas se han comprometido a mejorar el estado general de las unidades, la limpieza de los colectivos y una mejora de la frecuencia del servicio. Hemos analizado varios componentes de la estructura de costos y hemos decidido en este aumento que está por debajo de la inflación acumulada del año 2021; también es menor al aumento solicitado por las empresas que había sido del 85 por ciento".

Comentó que la semana que viene se va a convocar "a la mesa de la emergencia (de transporte) que había sido sancionada por el Concejo Municipal donde vamos a intercambiar información sobre la actualidad del servicio que tenemos, pero también haciendo algunos análisis para poder optimizar y mejorar los recorridos".

En esa línea destacó que "hay líneas que realizan recorridos sobre las mismas calles y hay otros barrios que no tienen servicio. Vamos a estar trabajando en poder optimizar esa situación".

Con relación a los subsidios, Zorzón definió a la situación como "compleja", y añadió: "No tenemos previsibilidad sobre cómo va a ser el subsidio para el interior del país. El Ministerio de Transporte de la Nación ya dispuso una resolución para el área metropolitana de Buenos Aires, incrementando los subsidios y no ha pasado lo mismo en el interior del país. Recordemos que es una situación muy inequitativa la que viven las ciudades del interior. El boleto en Capital Federal está costando 18 pesos, es una situación muy desfavorable para los usuarios del interior"