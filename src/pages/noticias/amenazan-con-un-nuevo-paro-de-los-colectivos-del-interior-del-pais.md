---
category: Agenda Ciudadana
date: 2022-06-23T07:54:35-03:00
thumbnail: https://assets.3dnoticias.com.ar/colectivo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Con información de Ambito.com y LT9 '
resumen: Amenazan con un nuevo paro de los colectivos del interior del país
title: Amenazan con un nuevo paro de los colectivos del interior del país
entradilla: El gremio de la F.A.T.A.P. advirtió que "de persistir" la deuda de Nación
  hacia el sector, decidirá suspender el servicio nocturno, y darle "prioridad a los
  que funcionan en hora pico".

---
El gremio que nuclea a los colectivos del interior del país advirtió que "analiza" suspender los servicios nocturnos del interior del país desde la semana próxima "por la deuda del Gobierno nacional" con el sector.

En un comunicado difundido a los medios, la Federación Argentina de Transportadores por Automotor de Pasajeros (F.A.T.A.P.), dijo "lamentar que de persistir la situación actual las empresas prestadoras del servicio de transporte público urbanos y suburbanos del interior del país tendrán que suspender los servicios entre las 22 hs y las 6 de la mañana, y darle prioridad a los que funcionan en hora pico".

FATAP sostuvo que la medida "se comenzaría a implementar la próxima semana".

Según consignó, es consecuencia de dos factores:

1) Los incumplimientos reiterados del Gobierno nacional, que de los $ 70.000 MIL MILLONES necesarios para dar cobertura mínima al presupuesto AÑO 2022, el ESTADO NACIONAL por medio del DNU 331/22 ha decidido asignar solo $ 38.000 MIL MILLONES, lo que representa un del 45% menos de lo solicitado y que está destinado a cubrir gastos básicos, como salarios y mantenimiento de las unidades.

2) Los constantes incrementos en el precio del gasoil, sumado al agotamiento del stock disponible y la falta de reposición del consumido.

"A pesar de que el sector del Transporte Público por Automotor de Pasajeros del INTERIOR del País, ha realizado un gran esfuerzo para explicar las muy desfavorables condiciones en que la actividad se desarrolla, hasta el momento no ha obtenido respuestas acordes a la gravedad de la situación que la afecta. En este contexto, resulta imposible atender salarios, aguinaldos y los costos erogables, tales como combustibles, lubricantes, neumáticos, reparaciones y seguros, elementos todos imprescindibles para mantener operables los servicios. Lamentamos profundamente los contratiempos que esta medida ocasionará a nuestros usuarios y a la población en general, pero la adoptamos con la convicción de que es la única manera de evitar la paralización total del sistema, la desaparición de empresas de capital nacional y la consecuente pérdida de los puestos de trabajo que generan", finalizó el texto del gremio.

Por su parte, Gerardo Ingaramo,  de FATAP anuncio hoy en LT9, que "tenemos problemas con el abastecimiento de combustibles por las protestas de camioneros y además por los precios del gasoil que estamos pagando, un 25% o 30% más caro que en Buenos Aires".  "Las medidas empresarias dependen de si llegan los subsidios, en la medida aprobada ayer en Diputados y si logramos colar el tema del precio del gasoil, no las llevaremos a cabo, es el día a día" se expresa Gerardo Ingaramo x @lt9lider, en el programa del periodista Osvaldo Medina.