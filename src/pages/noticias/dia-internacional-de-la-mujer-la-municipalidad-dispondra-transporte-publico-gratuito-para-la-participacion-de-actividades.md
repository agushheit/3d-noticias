---
category: La Ciudad
date: 2021-03-07T06:00:41-03:00
thumbnail: https://assets.3dnoticias.com.ar/8M.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: '8M: La Municipalidad dispondrá transporte público gratuito para la participación
  de actividades'
title: 'Día Internacional de la Mujer: La Municipalidad dispondrá transporte público
  gratuito para la participación de actividades'
entradilla: Se invita desde el Municipio a participar de las actividades organizadas
  en el marco del Día Internacional de la Mujer el próximo lunes 8 de marzo.

---
Para el próximo lunes 8 de marzo, día internacional de la Mujer, la Municipalidad dispuso la gratuidad del transporte público de pasajeros en el horario de 16.30 a 21. El objetivo es que "mujeres, lesbianas, trans, travestis y no binaries puedan participar de las actividades previstas por la Mesa Ni Una Menos Santa Fe en el marco del Día Internacional de la Mujer" informó el Municipio local.

Además, la gestión de Emilio Jatón invita a participar del Mes de las Mujeres y Disidencias, que se desarrolla en torno al 8 de marzo. Las Estaciones municipales, la Pantalla Pública del Auditorio Federal del Museo de la Constitución Nacional, el Teatro Municipal "1° de Mayo" y el Anfiteatro Juan de Garay serán escenario para el desarrollo de actividades que promueven la reivindicación de derechos.

**En las Estaciones**

En las Estaciones municipales se convocará a mujeres de cada barrio para participar de la pegatina colectiva de afiches feministas en el marco de la campaña nacional Vivas nos queremos. La propuesta se desarrollará durante marzo, atendiendo a la organización de cada Estación. Esta convocatoria se plantea como "un ritual de encuentro, un festejo colectivo y un homenaje a las que nos faltan".

Además, habrá otros encuentros con rondas de charlas, deportes y mateadas, también a consensuar en cada espacio. Esta campaña comenzó en México y hoy se disemina por toda América Latina, incluida la Argentina.

**En la Marechal**

"Mes de la Mujer en la Marechal" es el ciclo cultural que se realizará durante marzo en la Sala Marechal del Teatro Municipal "1° de Mayo".

La propuesta comenzará el sábado 13 con "La Fábrica de Monólogos" a las 21. El domingo 14 a las 18 se presentará "Paquito”, con música infantil. El sábado 20, a las 21, será el turno de "Adictas a vos", mientras que el domingo 21, a las 18, los espectadores podrán disfrutar de un relato de dos mundos de la mano de "Dolly y el Hombre Sombra".

El ciclo continuará el sábado 27, a las 21, con "Hechos a medida" y el domingo a las 18 con "Drácula, una mordida de alegría".

Las entradas se adquieren en el espacio cultural ubicado en San Martín 2020 a un valor de 200 pesos, de lunes a sábados, de 9 a 13 y de 17 a 21 hs. En cada función es obligatorio el uso de barbijo y respetar las medidas de bioseguridad vigentes en el marco de la pandemia por Covid-19.

**Cine dirigido por mujeres**

Con el apoyo de Cine Club Santa Fe, en el Auditorio Federal del Museo de la Constitución (Avda. Circunvalación y 1° de Mayo) ya comenzó un Ciclo de cine de directoras contemporáneas argentinas.

La primera fecha tuvo lugar el viernes 5 de marzo con la proyección de “Alanis” de Anahí Berneri, y las próximas fechas serán siempre a las 19.30: el viernes 12 con “La mujer de los perros” de Laura Citarella y Verónica Llinás; el viernes 19 con “Mi amiga del parque” de Ana Katz; y cerrará el viernes 26 con “Zama” de Lucrecia Martel.

Antes de cada película se proyecta una entrevista a la directora realizada por santafesinas que trabajan en distintas ramas del sector audiovisual y una vez finalizada la función se podrá disfrutar de propuestas de bebida y gastronomía.

La entrada es gratuita, con capacidad limitada de la sala, por lo que deberá reservarse previamente en la sección Pantalla Pública de la plataforma Capital Cultural (www.santafeciudad.gov.ar/capitalcultural).

**En el Anfiteatro**

En el recientemente recuperado Anfiteatro Juan de Garay, situado en el Parque del Sur, también habrá música con el Ciclo de Mujeres para disfrutar entre el 19 y el 21 de marzo a partir de las 20.

El viernes 19 se presentará Srta. Miraflores y Nada más y nada menos. El sábado 20, subirán al escenario Vanda y Victoria Sanada. El domingo cierran las músicas Azul Castello y Luciana Tourne.

Las entradas se retiran en la boletería del Teatro Municipal “1° de Mayo” (San Martín 2020), a razón de dos por personas. Se pueden retirar de lunes a sábados, de 9 a 13 y de 17 a 21 horas.