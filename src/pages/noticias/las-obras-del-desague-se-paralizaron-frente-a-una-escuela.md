---
category: La Ciudad
date: 2021-08-19T06:15:25-03:00
thumbnail: https://assets.3dnoticias.com.ar/ESPORA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Las obras del desagüe se paralizaron frente a una escuela
title: Las obras del desagüe se paralizaron frente a una escuela
entradilla: '"Es un peligro para los chicos que asisten, que el desagüe Espora haya
  quedado a medio hacer", dijo Nidia, directora de la escuela "General José de San
  Martín"'

---
Los trabajos en la obra del desagüe Espora se paralizaron otra vez. En esta oportunidad se debe a un problema administrativo relacionado con el desembolso de fondos para refinanciamiento de la misma. Frente a ello, los vecinos se mostraron preocupados por la noticia. En esta oportunidad Nidia, directora de la escuela Nº 568 "General José de San Martín", dijo que están preocupados por el peligro que esto representa, en su caso, para los alumnos que asisten al establecimiento.

"Junto con los vecinos elevamos a las autoridades un escrito para que controlen y arreglen algunas cuestiones que son un peligro para quienes transitan por el lugar", dijo Nidia. "Nosotros hicimos poner un cerco en inmediaciones de la escuela, pero no puedo pedirle a los vecinos que hagan lo mismo. Lo que hicimos es avisar a los padres de alumnos sobre el peligro de la tierra en este lugar, que fue apisonada el viernes cuando se dejó la obra, medio a los apurones, sobre todo teniendo en cuenta el peligro que puede representar un socavón".

Por otro lado, advirtió que "las veredas están rotas por la misma obra ya que pasaban por allí caños de agua y así quedaron, al igual que mucha de las tapas de los pozos negros". "Rogamos que no llueva porque con los desagües tapados y sin saber cómo es el estado de los pozos negros, sería una complicación mayor para todos", destacó la docente. Por otro lado dijo que desde 2019 están esperando por realizar algunas obras en el establecimiento, como por ejemplo pintar, pero esperan la finalización de la obra.

"El peligro que representa para los chicos es lo que más nos preocupa, pero por suerte a nuestro favor trabajamos con burbujas por lo que tenemos a la mitad de la población escolar. En total la matrícula es de 530 alumnos desde los cuatro años hasta los 13 y a ello hay sumarle que funciona una escuela nocturna que además tiene el problema de la poca iluminación de la zona", dijo la directora.

Por otra parte, se lamentó, al igual que los vecinos, del nuevo parate. "Nosotros estábamos esperando mucho la obra, pero sabemos que es muy larga. En el 2019 se avanzó, en 2020 no se hizo nada por la pandemia, pero ahora en el 2021 se para frente a la escuela".

**El desagüe Espora**

La obra comprende la construcción del conducto hídrico, bocas de tormenta y registro, conexión con las demás obras hídricas y cámaras de transición. Con una extensión de más de 4 km, atraviesa de este a oeste, desde 1º de Mayo y Risso, llega hasta Camino Viejo a Esperanza. El agua que recolecte seguirá de allí hasta la repotenciada Estación de Bombeo Nº 5 y saldrá al río Salado por gravedad o de manera mecánica.

Aliviará los barrios Sarmiento, San Martín, San José, Belgrano, Facundo Quiroga, Villa Las Flores, Scarafia, Los Ángeles y El Tránsito, de manera directa. Trabajará en conjunto con los desagües Larrea, Guanella y Ayacucho, para sanear toda la Cuenca Flores, aproximadamente desde Estanislao Zeballos hasta Gorriti, y desde 1º de Mayo hasta Camino Viejo a Esperanza.

El desagüe Espora tendrá una capacidad de desagote de más de 82.000.000 millones de litros de agua de lluvia por hora. Son unos 70.000 vecinos los que se verán beneficiados con la obra. El desagüe cuenta con distintas secciones. Tiene una altura de 1,20 en toda su extensión, pero modifica el ancho a lo largo de su recorrido.

**Qué dijo el municipio**

Este miércoles, Mariano Granato, secretario general de la Municipalidad, sostuvo: "Cuando la empresa local retomó la obra, fuimos descubriendo errores en los trabajos previos que fueron generando más gastos de los previstos. La obra tiene un 85% de concreción y necesita una readecuación de costos para terminar".

Además, agregó: "Estuvimos haciendo gestiones con Nación la semana pasada y nos dijeron que la idea es solucionar esta situación que se presentó. Somos optimistas de que Nación va a arreglar este tema para que rápidamente se retomen las obras, con el ritmo que corresponde y se termine en tiempos razonables".