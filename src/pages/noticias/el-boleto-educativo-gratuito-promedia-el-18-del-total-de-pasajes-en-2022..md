---
category: Agenda Ciudadana
date: 2022-12-01T07:47:24-03:00
thumbnail: https://assets.3dnoticias.com.ar/boleto-gratuito.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: 'El  boleto educativo gratuito promedia el  18% del  total de pasajes en  2022. '
title: 'El  boleto educativo gratuito promedia el  18% del  total de pasajes en  2022. '
entradilla: Según los datos obtenidos del sistema SUBE, de los 22.583.509 pasajeros
  que accedieron al Transporte Público hasta octubre, un 18% lo hizo utilizando el
  BEG.

---
Según los datos obtenidos del sistema SUBE, de los 22.583.509 pasajeros que accedieron al Transporte Público hasta octubre, un 18% lo hizo utilizando el BEG.

El boleto educativo gratuito (BEG) es una política implementada por el Gobierno de la Provincia, y que cumple sus primeros dos años de implementación. “En lo que va del 2022 son más de 300 mil los beneficiarios y beneficiarias de toda la provincia que se inscribieron al programa, que tiene un presupuesto anual de 6.200 millones de pesos”, afirmaron desde el gobierno de la provincia.

3D NOTICIAS accedió a los datos del transporte urbano de pasajeros obtenidos a través del sistema SUBE, para conocer cuál fue el impacto de dicha política pública en la ciudad de Santa Fe.

Septiembre fue el mes que más se utilizó el beneficio, con un total de 609.489 pasajes, lo que implica un 22% de los pasajes totales de ese mes. Por el contrario, el mes con menor utilización del BEG fue marzo, con un 13% de los boletos “cortados” ese mes. Con respecto a las líneas que más trasladan usuarios del BEG, encontramos la línea 2 con el 10%, luego la 16 con el 9%.

Desde la implementación del Boleto educativo gratuito, la participación del Boleto educativo gratuito en el total de pasajes fue la siguiente:

![](https://assets.3dnoticias.com.ar/cuadro para beg.png)

_En lo que respecta a los pasajes totales, comparando enero/octubre 2019 (31.153.235_ pasajes) contra el mismo periodo de 2022 (22.583.509)_, se verifica una disminución de la cantidad de pasajes, que ronda un 28%._