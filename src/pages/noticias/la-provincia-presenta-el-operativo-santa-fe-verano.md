---
category: Estado Real
date: 2021-01-14T10:07:08Z
thumbnail: https://assets.3dnoticias.com.ar/operativo-stafe-verano.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: La provincia presenta el Operativo Santa Fe Verano
title: La provincia presenta el Operativo Santa Fe Verano
entradilla: Se trata de una iniciativa de prevención y concientización frente a la
  pandemia del COVID-19 que la provincia llevará adelante en todo el territorio provincial.

---
El gobierno provincial presentará hoy, jueves 14 de enero, a las 10:30 horas, el **operativo Santa Fe Verano**, una iniciativa de prevención y concientización frente a la pandemia del COVID-19. 

La actividad se llevará a cabo en la intersección de la Ruta Nacional N.º 11 y calle Fresnos de Sauce Viejo.

Estarán presentes en la actividad la ministra de Salud, Sonia Martorano; los secretarios de Turismo, Alejandro Grandinetti; y de Política y Gestión de la Información, Jorge Fernández; el subsecretario de la Agencia Provincial de Seguridad Vial, Osvaldo Aymo; y la directora de la Agencia de Seguridad Vial, Antonela Cerutti.

**Santa Fe Verano** es un programa multiagencial en el que participan los Ministerios de Salud, Seguridad y Cultura, junto con la secretaría de Turismo y la Agencia de Desarrollo (ADER).

El objetivo es cuidar la temporada estival y realizar diferentes actividades, entre las que se destacan los test de anosmia y detectar, como así también el control de las nuevas medidas de restricción a la circulación.