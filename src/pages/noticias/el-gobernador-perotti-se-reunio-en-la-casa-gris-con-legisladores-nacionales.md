---
category: Estado Real
date: 2021-12-14T06:00:18-03:00
thumbnail: https://assets.3dnoticias.com.ar/casagris.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: El gobernador Perotti se reunió en la Casa Gris con legisladores nacionales
title: El gobernador Perotti se reunió en la Casa Gris con legisladores nacionales
entradilla: Se dialogó sobre “acciones concretas y conjuntas como, por ejemplo, trabajar
  en el fortalecimiento del sistema de Justicia Federal", detalló el ministro Marcos
  Corach luego de la reunión.

---
El gobernador Omar Perotti se reunió este lunes con legisladores nacionales en el Salón Blanco de Casa de Gobierno.

Luego del encuentro, el ministro de Gestión Pública, Marcos Corach, precisó que “tanto el gobernador, como el ministro de Seguridad (Jorge Lagna), expresaron las dificultades con las que nos encontramos; así como el nivel de inversión y lo previsto para los próximos meses en términos de seguridad”.

“Si bien la seguridad fue una parte importante de la reunión, también hablamos del presupuesto, del fallo de la Corte Suprema y del proyecto del diputado Roberto Mirabella sobre nombramientos de fiscales nacionales. Además, el gobernador anunció que mañana se estaría reuniendo con el procurador general para ver cómo avanzar con el tema de la Justicia Federal”, precisó el ministro.

En este sentido, Corach manifestó que se dialogó sobre “acciones concretas y conjuntas, lo mismo que con los legisladores provinciales”, como por ejemplo “trabajar junto con nosotros en el fortalecimiento del sistema de Justicia Federal”.

Asimismo, señaló que “tenemos inversiones y licitaciones en marcha. La inversión en seguridad es importante, la vamos a seguir manteniendo, porque tenemos los recursos para hacerlo. Como dice el gobernador, si nosotros nos comprometemos a hacerlo tenemos los recursos para sostenerlo”.

Por último, Corach señaló que “el gobernador toma nota, incorpora las mociones y propuestas, y las vamos a llevar adelante en una mesa de construcción y diálogo, para darle continuidad a estos ámbitos”.

Por su parte, el diputado nacional, Roberto Mirabella, describió que "tomamos nota de una gran cantidad de cosas que se plantearon. Enfrentamos una situación muy compleja, que en los últimos 10 a 15 años se le abrió la puerta al delito y al narcotráfico. Esto avanzó de una manera abrumadora como en ningún lugar de la Argentina, por eso creemos que esta situación que vive Santa Fe es excepcional. El diputado Barletta proponía que declaremos la emergencia nacional en seguridad para la provincia".

"Nosotros vamos a acompañar ese proyecto porque creemos que toda la Argentina tiene que tomar dimensión de lo que está ocurriendo en la provincia. Fue una muy buena reunión donde todos expresaron sus puntos de vista, propuestas y críticas", puntualizó Mirabella.

Por último, la senadora nacional, Carolina Losada, indicó que “fue una reunión amena, de la que nos llevamos definiciones”; en tanto que el senador Dionisio Scarpín señaló que “ante una crisis de esta magnitud, toda instancia de diálogo es buena, y esto es lo que prima en el día de hoy”.

**Presentes**  
Participaron de la actividad, los ministros de Economía, Walter Agosto; y de Seguridad, Jorge Lagna; la ministra de Infraestructura, Servicios Públicos y Hábitat, Silvina Frana; el secretario de Justicia, Gabriel Somaglia; y el fiscal de Estado, Rubén Weder.

Los legisladores nacionales que estuvieron presentes fueron la senadora Carolina Losada; los senadores Dionisio Scarpín y Marcelo Lewandowski; las diputadas Laura Castes, Victoria Tejeda, Alejandra Obeid, Magalí Mastaler, Ximena García, Mónica Fein; y los diputados Roberto Mirabella, Federico Angelini, Enrique Estévez, Juan Martín, Eduardo Toniolli, Gabriel Chumpitaz y Mario Barletta.