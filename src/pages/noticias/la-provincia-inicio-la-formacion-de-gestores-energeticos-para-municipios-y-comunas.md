---
category: Estado Real
date: 2021-07-30T06:00:19-03:00
thumbnail: https://assets.3dnoticias.com.ar/INVERSIONES.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La provincia inició la formación de gestores energéticos para municipios
  y comunas
title: La provincia inició la formación de gestores energéticos para municipios y
  comunas
entradilla: La iniciativa es realizada desde el Ministerio de Ambiente y Cambio Climático
  en articulación con el Consejo Federal de Inversiones.

---
La provincia de Santa Fe, a través del Ministerio de Ambiente y Cambio Climático, llevó adelante el primer encuentro de la capacitación de Gestores Energéticos para Municipios y Comunas, en el marco del Programa de Eficiencia Energética y Generación Distribuida en Municipios y Comunas (PGEM) propuesto por el Consejo Federal de Inversiones (CFI). La iniciativa forma parte de una de las acciones que viene desarrollando la provincia en pos de la Transición Energética Santafesina.

El curso es dictado en colaboración con la Universidad Nacional de Rafaela (UNRaf), a través de un temario diseñado por el Ministerio y el CFI. Más de 40 participantes serán capacitados en la metodología específica para la realización de relevamientos energéticos en municipios y comunas.

La capacitación tiene como finalidad promover el uso eficiente y racional de la energía, fomentando la implementación de proyectos de inversión en Eficiencia Energética y Generación Distribuida, que permitan reducir el consumo energético, propiciando el cuidado del ambiente y una mejora en la competitividad de las economías regionales.

La apertura del encuentro estuvo encabezada por la ministra de Ambiente y Cambio Climático, Erika Gonnet, quien agradeció al secretario general del CFI, Ignacio Lamothe, y a la Universidad Nacional de Rafaela. Gonnet declaró: “La cuestión ambiental tiene que ser transversal y tener un anclaje fuerte en el territorio, por eso consideramos que los municipios y comunas son ejes claves para trabajar la transición energética”.

Cabe resaltar que Santa Fe es la primera provincia del país en brindar un curso para la formación de Gestores Energéticos destinados a integrantes de municipios y comunas. En ese sentido, la Ministra destacó: “Esta jornada es de mucha importancia para nosotros porque la capacitaciones de Gestores Energéticos se había realizado en otras oportunidades pero siempre vinculadas al sector productivo e industrial”.

Los Gestores capacitados serán los responsables de llevar adelante la asistencia técnica en municipios o comunas, trabajo que consiste en desarrollar una auditoría energética relevando datos de consumos, equipos, entre otros ejes, para luego generar propuestas energéticas superadoras. En ese sentido, el secretario de Desarrollo Ecosistémico y Cambio Climático, Ing. Jorge Caminos, comentó: “El dictado de este curso será fundamental en la formación de los próximos Gestores Energéticos de Municipios y Comunas, quienes trabajarán en las localidades llevando adelante diferentes tipos de auditorías energéticas”.