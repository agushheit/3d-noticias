---
category: La Ciudad
date: 2021-07-09T09:39:15-03:00
thumbnail: https://assets.3dnoticias.com.ar/mb-hm.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: 'Hugo Marcucci será pre-candidato a concejal  '
title: 'Hugo Marcucci será pre-candidato a concejal  '
entradilla: Ex senador provincial y ex diputado nacional, parte del equipo de trabajo
  de Mario Barletta y competirá en la interna de Juntos por el Cambio. "Nos gustaría
  que en el 2023 Barletta vuelva a ser intendente de la ciudad ".

---
El ex senador provincial y ex diputado nacional Hugo Marcucci será pre-candidato a concejal por la ciudad de Santa Fe, por una de las listas que disputará las Paso de Juntos por el Cambio. 

El dirigente radical es parte del equipo del ex intendente Mario Barletta quién encabezará la lista como pre-candidato a diputado nacional junto a Carolina Losada que será pre-candidata a senadora nacional. 

En primer lugar, Marcucci sostuvo que su propuesta política está en sintonía con el "modo Barletta" que se puso en funcionamiento en el 2007 cuando se llegó a la intendencia. 

Además, el dirigente radical agregó que "nosotros queremos ponerle límites al kirchnerismo en el Congreso nacional y luchar por fondos para nuestra ciudad porque claramente Santa Fe está discriminada (casi todo va al conurbano). No sólo luchamos por un banca de concejal sino que también tenemos la ilusión que Barletta vuelva a la intendencia en 2023". 

Consultado sobre cómo ve la ciudad de Santa Fe, Marcucci manifestó que "yo estoy convencido que hay que pensar la postpandemia. Queremos llevar nuestras ideas al Concejo sobre producción, empleo, educación, seguridad y aportar desde el diálogo".