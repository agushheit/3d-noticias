---
category: Agenda Ciudadana
date: 2021-02-25T06:30:22-03:00
thumbnail: https://assets.3dnoticias.com.ar/LARRETA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Justicia realizó un procedimiento en el Ministerio de Salud porteño por
  las vacunas
title: La Justicia realizó un procedimiento en el Ministerio de Salud porteño por
  las vacunas
entradilla: La Justicia dio impulso a dos denuncias recibidas el martes por la suscripción
  de convenidos del Gobierno porteño para entregar la vacuna a obras sociales y empresas
  de medicina prepaga.

---
El fiscal Carlos Stornelli imputó hoy al jefe de Gobierno porteño, Horacio Rodríguez Larreta; y a su ministro de Salud, Fernán Quirós; al requerir que se investigue la presunta "privatización" de la vacunación contra el coronavirus en el distrito, luego de que se suscribieron convenios para entregar dosis a obras sociales y empresas de medicina prepaga para uso exclusivo de sus afiliados.

Los investigadores buscan determinar cuántas dosis recibió la Ciudad, cómo se distribuyeron en los centros vacunatorios, con qué empresas de medicina privada y bajo qué criterios se suscribieron convenios y, finalmente, a quiénes se inoculó.

Al solicitar las primeras medidas de prueba, el fiscal Stornelli reprodujo la parte de la denuncia en la que Salvo aseveraba que "Horacio Rodríguez Larreta estaría privatizando la campaña de vacunación que se lleva adelante frente a la pandemia que aqueja a toda la población, beneficiando a grupos privados en detrimento de la población en general".

La denunciante había señalado que "esta práctica para facilitar vacunaciones de privilegio y la ausencia de información y/o documentación que permita determinar la trazabilidad de las vacunas, en el marco de la mayor pandemia que recuerde nuestra Ciudad, resultarían constitutivos de delito previsto en el artículo 248 del Código Penal".

**La postura del Gobierno porteño**

Por la mañana, en conferencia de prensa, el ministro Quirós afirmó que el Gobierno del distrito está dispuesto a "mostrar y explicar" ante la Justicia el plan de vacunación que se implementa en la ciudad.

De esta forma, respondió a las dos presentaciones realizadas ante la Justicia: una, por la presunta "privatización de la vacunación contra el coronavirus" al "entregarles dosis a algunas obras sociales y medicinas prepagas para uso exclusivo de sus afiliados"; y otra por la supuesta aplicación de vacunas a "militantes de Juntos por el Cambio en una asociación vecinal de Parque Chacabuco cuya referente es una dirigente radical".

En su habitual rueda de prensa en la sede del Gobierno porteño, en el barrio de Parque Patricios, explicó que desde el Gobierno porteño se realizaron acuerdos con siete empresas de medicina prepaga para llevar adelante la campaña de vacunación, del mismo modo en que se hace habitualmente para la distribución de otras vacunas, con el fin de "que cada ciudadano porteño tenga la misma capacidad y accesibilidad" a la inmunización.

"No tenemos ningún problema con ninguna denuncia. Si algún ciudadano tiene alguna duda, aquí estamos para mostrar y explicar lo que hacemos e hicimos y para transparentar los datos. Lo más importante es explicarle a la gente", dijo el funcionario porteño.