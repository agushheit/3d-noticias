---
category: Agenda Ciudadana
date: 2021-06-15T08:03:57-03:00
thumbnail: https://assets.3dnoticias.com.ar/cantero.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de LT10
resumen: Cantero no descarta que puedan darse clases los sábados y feriados
title: Cantero no descarta que puedan darse clases los sábados y feriados
entradilla: La ministra de Educación dijo que, con el objetivo de “robustecer la jornada
  escolar”, se analiza “otro sistema de cursado”. “Todas las posibilidades son analizadas”,
  aseguró.

---
En el marco de la discusión por la modalidad del dictado de clases durante las restricciones por la segunda ola de coronavirus, la ministra de Educación de Santa Fe deslizó una posibilidad que anticipa una nueva polémica: que se den clases los sábados y feriados durante el segundo semestre del año.

Esa posibilidad, dijo Adriana Cantero ante un medio rosarino, dependería de una mejora en los indicadores sanitarios y del éxito del plan de vacunación. “Tal vez se podría pensar en robustecer un poco más la jornada escolar y tener otro sistema de cursado para cada nivel”, sostuvo.

Si bien aclaró que la cartera educativa aún no ha tomado “definiciones concretas”, reconoció que “hay muchas alternativas que se analizan pormenorizadamente”. Y al ser consultada sobre la incorporación de horas cátedra los sábados y feriados entre dichas alternativas, insistió: “Todo puede ser. Todas las posibilidades son analizadas. Si estamos mejor en el segundo semestre, tendríamos que trabajar en alternativas para robustecer el cursado”.

Por otra parte, señaló que el Ministerio no tiene “previsto el adelantamiento del receso invernal”, aunque volvió a destacar que “en una dinámica tan grande como impone la pandemia siempre se están analizando” todas las opciones.

**Semipresencialidad**

Sobre el regreso a las aulas en distritos con bajo nivel de contagios de covid, la ministra Cantero recordó que hoy se retomó "la presencialidad escolar con alternancia en 147 pequeñas localidades de la provincia".

Se trata de localidades que "no han tenido casos o han reportado menos de 10 casos en los últimos 14 días; por eso allí se puede empezar a trabajar con la semipresencialidad en los niveles inicial y primario”.  

“Esperamos con las medidas de restricción y cuidado la curva epdidemiológica pueda detenerse y descender; y en la medida que vayan mejorando esos indicadores, ir recuperando presencialidad en cada uno de los lugares que sea posible", confió la funcionaria.