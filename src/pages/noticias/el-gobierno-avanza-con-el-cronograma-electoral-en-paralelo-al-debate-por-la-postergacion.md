---
category: Agenda Ciudadana
date: 2021-03-15T06:00:34-03:00
thumbnail: https://assets.3dnoticias.com.ar/elecciones.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: El Gobierno avanza con el cronograma electoral
title: El Gobierno avanza con el cronograma electoral
entradilla: El debate sobre postergar o producir cambios en las elecciones por única
  vez, con motivo de la pandemia, sigue pendiente en el Congreso.

---
El Gobierno avanza en el cumplimiento del cronograma electoral previsto para este año, con la realización de las Primarias, Abiertas Simultáneas y Obligatorias (PASO) y los comicios generales, mientras el debate sobre postergar o producir cambios en las elecciones por única vez, con motivo de la pandemia, sigue pendiente en el Congreso.

Así, el jueves 25 de este mes vencerá el plazo para la presentación de las ofertas de la licitación pública convocada por el Ministerio del Interior para efectuar el recuento provisional de los resultados de los comicios, cuyos pliegos incorporaron, por primera vez, observaciones y sugerencias sobre el proceso aportadas por fuerzas políticas, especialistas, compañías informáticas y la Cámara Nacional Electoral.

El primer paso de este período de recepción de aportes se dio a principios de enero, con la publicación en el Boletín Oficial del anteproyecto -de 103 páginas- de la licitación pública a fin de que todos los interesados pudieran hacer llegar sus observaciones y propuestas de eventuales modificaciones.

En total, se recibieron 55 observaciones y/o sugerencias de los diferentes actores involucrados en el proceso electoral, que generaron 67 respuestas y consideraciones elaboradas por parte de los funcionarios especializados de la cartera de Interior.

Tras analizar las observaciones, 21 de los aportes fueron incorporados en la confección del pliego definitivo, a través de modificaciones en su redacción y en los alcances de los 16 puntos que componían el pliego original de la licitación.

La titular de la Dirección Nacional Electoral, Diana Quiodo, valoró los aportes presentados al anteproyecto porque contribuyeron con "una diversidad de miradas y puntos de vista" que enriquecieron la versión final del pliego, se informó oficialmente.

"Por indicación del ministro (Eduardo) 'Wado' de Pedro pusimos en marcha este mecanismo, que sin dudas arroja un balance muy positivo", agregó Quiodo y agradeció "a todas las personas, instituciones y empresas que participaron en este proceso, porque sus aportes fueron muy valiosos y redundarán en una mayor calidad y transparencia del proceso electoral".

Tras la incorporación de propuestas y aportes, que en la cartera de Interior destacan como una decisión inédita para el procedimiento habitual de organización de comicios, el Boletín Oficial publicó a mediados de febrero la licitación 22/2020, que establece las condiciones requeridas para las empresas que quieran competir por la realización del escrutinio provisorio de las PASO y de las elecciones generales.

Las consultas podrán hacerse en esa web gubernamental hasta el 19 de marzo y se establece como fecha tope para presentar las ofertas el 25 de marzo, cuando en un acto público programado para las 10, en forma electrónica y automática, se generará el acta de apertura de las presentaciones recibidas hasta ese momento.

En esta oportunidad, y a diferencia de los contratos del 2019, los montos que requieran los trabajos deberán expresarse en pesos, no convertibles al valor del dólar, de acuerdo con lo especificado en los pliegos de la licitación.

Los resultados del escrutinio provisorio son aquellos que se difunden en la noche de los comicios y posibilitan conocer, pocas horas después del cierre de la votación, qué partido o agrupación ganó, aunque el único recuento que tiene validez es el definitivo que está a cargo de la Justicia Electoral, que demora entre una y dos semanas, y que en general, en forma histórica, no arroja variaciones sustanciales.

Fuentes oficiales destacaron que, no obstante, el contexto en el que el Gobierno propone adaptar el calendario electoral a las variables epidemiológicas de los próximos meses, debido a la situación sanitaria por la Covid-19 y las variaciones del virus que amenazan a la región, la licitación está prevista en el cronograma electoral y la ley vigente, y por eso el procedimiento avanza en las fechas establecidas, como corresponde.

Ello no implica, sin embargo, tomar posición alguna respecto al debate y alternativas que se discuten en torno a una posible postergación del calendario electoral o a la suspensión, mucho menos probable, de las PASO por una única vez, aclararon desde el Ministerio del Interior.

La organización de los comicios deberá tener en cuenta la pandemia del Covid-19, que obligará a tomar recaudos adicionales al Estado y también a las prestatarias privadas que realicen la transmisión de los resultados provisorios

Las fuentes subrayaron que a lo largo de todo el 2020, al igual que este año, dicha cartera recibe y escucha todas las opiniones y que, en su carácter de órgano de aplicación a cargo de la organización y realización de las elecciones, trabaja para garantizar un proceso transparente y de calidad.