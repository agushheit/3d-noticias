---
category: Deportes
date: 2021-07-11T06:15:17-03:00
thumbnail: https://assets.3dnoticias.com.ar/FIDEO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Argentina campeón de América
title: Argentina campeón de América
entradilla: La Selección derrotó 1 a 0 a Brasil con gol de Ángel Di María a los 21
  minutos del primer tiempo y cortó una racha de 28 años sin títulos.

---
La Selección argentina de fútbol se consagró este sábado campeón de América al vencer 1 a 0 en la final a Brasil, en un encuentro disputado en el estadio Maracaná de Río de Janeiro.

El único gol del partido lo marcó Ángel Di María a los 21 minutos del primer tiempo.

De esta forma, Argentina cortó casi tres décadas sin títulos (28 años pasaron desde el último festejo, precisamente una Copa Amperica, la de 1993 con Alfio Basile como DT), con finales perdidas en este torneo y la Copa del Mundo de 2014. Además, obtuvo el primer título en la Era Messi.

![](https://assets.3dnoticias.com.ar/argentina.jpg)