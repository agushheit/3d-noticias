---
category: Agenda Ciudadana
date: 2021-10-17T06:00:31-03:00
thumbnail: https://assets.3dnoticias.com.ar/boleto.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Aniversario del boleto educativo: "Removemos obstáculos para que todos puedan
  acceder a las educación" aseguró Perotti'
title: 'Aniversario del boleto educativo: "Removemos obstáculos para que todos puedan
  acceder a las educación" aseguró Perotti'
entradilla: Para celebrar el primer año del sistema, que cuenta con más de 230 mil
  inscriptos, el gobierno de la provincia realizó el Festival Santa Fe Joven en la
  ciudad de Rosario.

---
A un año de la creación del programa Boleto Educativo Gratuito (BEG), el gobierno provincial realizó este sábado el Festival Santa Fe Joven, que se llevó a cabo en el Parque Nacional a la Bandera de la ciudad de Rosario, y contó con la participación de alrededor de 10 mil asistentes.

“Se cumple un año de la puesta en marcha y el deseo de que pueda seguir avanzando, que no nos quede ningún estudiante sin esta posibilidad. Que el que se suma al sistema educativo se vaya incorporando y tendrá que seguir creciendo, ese es el deseo de cuando se pensó: remover cualquier obstáculo para que alguien pueda estar accediendo a la educación”, aseguró el gobernador Omar Perotti, tras recorrer el lugar acompañado por el director Provincial de Boleto Educativo y Franquicias, Juan Rober Benegui.

“La marcha hasta aquí es más que positiva”, evaluó Perotti, y continuó: “La puesta en marcha por primera vez de un sistema requiere ajustes y se van haciendo bien. Tenemos la expectativa de recuperar líneas y frecuencias de transporte, sobre todo en el interior de la provincia, y que las podamos utilizar sumando el beneficio del Boleto Educativo Gratuito”, dijo el mandatario.

Por otra parte, el gobernador valoró que este año haya “16 mil estudiantes más en el sistema educativo. Queremos que no quede ninguno afuera, y esto es parte de la búsqueda para que tengamos a todos los chicos y chicas de 4 años en su salita que hemos universalizado, y ninguno de los que terminó la primaria tienen que dejar de inscribirse en la secundaria, vamos a buscarlos a todos, se hizo un relevamiento casa por casa”.

Finalmente, el gobernador pronosticó que “la provincia o región que consiga el mayor desarrollo educativo es la que va a tener mejor oportunidades y calidad de vida para su gente, y nosotros queremos ser la provincia que tenga la mayor cantidad de alumnos que terminen la escuela secundaria, formados y preparados de la mejor manera, para decidir el ingreso al mundo laboral o seguir una carrera terciaria o universitaria”.

**FESTIVAL ANIVERSARIO**

Del encuentro participaron numerosos artistas locales, además de feria de emprendedores, propuestas gastronómicas e intervenciones artísticas de distintos géneros. También se hicieron presentes diferentes áreas del gobierno de la provincia de Santa Fe para exponer y brindar información respecto a los diferentes programas o políticas que impactan directamente en las y los jóvenes de todo el territorio santafesino, tales como Ciencia y Tecnología, Cultura, ATR Juventudes, Billetera Santa Fe y Salud, entre otros.

Otra de las propuestas fue la feria de prototipos tecnológicos, espacio que tiene como objetivo consolidar y fortalecer el ecosistema de innovación de la provincia, potenciando aquellos proyectos que den respuesta a los desafíos de nuestra provincia. También se desarrolló la Copa BEG, el torneo amateur de FIFA en el que todos los participantes se llevaron un regalo.

**MÁS DE 230 MIL INSCRIPTOS**

Sobre la iniciativa impulsada por el gobierno provincial a fin de celebrar el primer aniversario del BEG, el director Provincial de Boleto Educativo y Franquicias, Juan Rober Benegui, destacó: “Se cumple un año de la norma que dio inicio al Boleto Educativo Gratuito, programa que ya cuenta con más de 230 mil inscriptos en toda la provincia, por lo tanto es un motivo para celebrar, sobre todo por el beneficio que conlleva en términos económicos y el ahorro que significa para las familias”.

"Fue una promesa de campaña del actual gobernador Omar Perotti, cuya implementación no pudimos hacerla en el 2020, ya que la pandemia y la falta de presencialidad en las aulas lo impidieron, pero aprovechamos ese tiempo para diseñar la puesta en marcha del programa", concluyó Benegui.