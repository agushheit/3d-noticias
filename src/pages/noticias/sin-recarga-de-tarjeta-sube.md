---
category: Agenda Ciudadana
date: 2022-06-27T07:56:40-03:00
thumbnail: https://assets.3dnoticias.com.ar/sube.png
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: Sin recarga de tarjeta SUBE
title: Sin recarga de tarjeta SUBE
entradilla: 'Confirmaron un paro por 72 horas en la ciudad de Santa Fe: no se cargarán
  los días martes 28, miércoles 29 y jueves 30. '

---
A través de sus redes sociales, la cámara de quiosqueros de Santa Fe (CAKS) confirmo la medida de fuerza. Según el comunicado emitido, la medida de fuerza se realiza por “…un margen digno de ganancias para las familias Kiosqueras del País, para que se termine la especulación con las tarjetas de SUBE, por un valor único en todo el país de las tarjetas, para que las empresas monopólicas no destruyan en sistema comercian micro Pyme nacional. Por esos motivos debido a la falta de respuestas del Estado Nacional vamos al Paro con el servicio de carga de Sube martes 28, miércoles 29 y jueves 30 apoyemos a los colegas de todo el país.”