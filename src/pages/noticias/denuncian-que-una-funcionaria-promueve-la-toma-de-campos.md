---
category: Agenda Ciudadana
date: 2021-06-23T09:10:32-03:00
thumbnail: https://assets.3dnoticias.com.ar/usurpaciones.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de LT10
resumen: Denuncian que una funcionaria promueve la toma de campos
title: Denuncian que una funcionaria promueve la toma de campos
entradilla: 'Se trata de una militante del movimiento Evita que forma parte de la
  gestión del intendente Vallejos.  La diputada Lucila Lehmann manifestó si indignación
  por LT10 y pidió que la Justicia tome cartas en el asunto.  '

---
Durante los últimos días, más de 800 familias tomaron un campo de 33 hectáreas en Reconquista que pertenece a la familia Cian, de pequeños productores agropecuarios.

Lucila Lehmann, diputada nacional por Santa Fe, sostuvo por LT10 que esta toma se dio en el marco de las declaraciones del presidente de la Nación (sobre las tierras improductivas) y que está incentivada por una militante del movimiento Evita, Haydee Vargas, que forma parte de la gestión municipal del intendente Vallejos.

![](https://www.lt10.com.ar/multimedia/in1624363478802.jpeg)

Además, la legisladora agregó que camiones contratados por la municipalidad bajaron en el terreno lonas y maderas para armar los ranchitos.

 “Esta gente está haciendo política con terrenos privados y promoviendo delitos desde el mismo Estado. La Justicia está actuando bien en este caso. Lo insólito es el pedido del intendente Vallejos que pide que se corra la Justicia", remarcó Lucila Lehmann.