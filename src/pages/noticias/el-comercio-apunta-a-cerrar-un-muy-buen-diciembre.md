---
category: La Ciudad
date: 2021-12-03T06:00:55-03:00
thumbnail: https://assets.3dnoticias.com.ar/peatonal.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: El comercio apunta a "cerrar un muy buen diciembre"
title: El comercio apunta a "cerrar un muy buen diciembre"
entradilla: 'El año pasado las ventas por Navidad arrancaron tarde: el 15 de diciembre.
  Este cierre de 2021 "las expectativas son muy positivas por los niveles comerciales
  que ya se observan", indicaron del Centro Comercial

'

---
Luego de un año donde se comenzó a concretar la flexibilización de actividades, horarios y aforos, diciembre se enmarca como un mes clave para el comercio santafesino de cara a las ventas de Navidad, que el pasado año, se dieron sobre "la hora" y en un contexto sanitario complejo.

"Las expectativas son positivas porque venimos de un año donde se lograron distintas y variadas aperturas de actividades, posicionándonos en la actualidad, casi en los niveles pre pandemia. Esta situación se termina derramando en el comercio con flujos positivos de venta", sostuvo a UNO Santa Fe, Martín Salemi, referente del Centro Comercial.

"El año pasado las ventas navideñas comenzaron muy tarde, a partir del 15 de diciembre, casi sobre la fechas de celebración, cuando normalmente inician a finales del mes de noviembre. Desde este mes se comenzó a ver más actividad comercial en los diversos centros de ventas y todos los comerciantes están esperando un cierre de año muy positivo", subrayó Salemi.

"Las ventas, ya desde el pasado mes de noviembre, a diferencia del año pasado con la pandemia y una situación sanitaria más compleja, comenzaron a verse incrementadas por un mayor flujo de santafesinos recorriendo los diversos centros comerciales. Las fiestas de fin de año, despedidas, recepciones y casamientos activaron el comercio e inyectaron dinero y buenas sensaciones", manifestó el referente del Centro Comercial.

Sobre las ventas de Navidad, Salemi se mostró entusiasmado: "Esperamos muy buenas ventas navideñas donde el objetivo es alcanzar y superar los niveles de diciembre de 2019. La realidad es que el comercio está vendiendo y las expectativas apuntan a las fiestas de fin de año, de la mano del programa Billetera Santa Fe que impulsó y dinamizó mucho las ventas de los santafesinos, y además, generó que otros bancos salgan al mercado con promociones y descuentos importantes, como en el mes de octubre donde ofrecieron 40 por ciento de descuento y cuotas fijas".

**Cómo trabajará el comercio el feriado del 8 de diciembre**

El próximo miércoles 8 de diciembre es feriado nacional por el Día de la Inmaculada Concepción de María. En relación al último feriado entre semana del año, Martín Salemi explicó que "el día previo se va a trabajar con normalidad".

Por su parte, en relación al feriado en particular, informaron desde el Centro Comercial que el día 8 de diciembre, todos los comercios permanecerán cerrados.