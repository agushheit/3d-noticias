---
category: Deportes
date: 2021-03-18T06:36:01-03:00
thumbnail: https://assets.3dnoticias.com.ar/basket.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: La provincia abrió la inscripción a los programas Aporte del Capital Privado
  y Aporte al Deporte Federado
title: La provincia abrió la inscripción a los programas Aporte del Capital Privado
  y Aporte al Deporte Federado
entradilla: Los interesados pueden inscribirse hasta el 30 de abril.

---
El Ministerio de Desarrollo Social, a través de la Secretaría de Deportes y del Consejo Provincial del Deporte (Coprode), abrió la inscripción a los programas “Aportes del Capital Privado” (Ley Nº 10.554) para infraestructura deportiva en clubes; y “Aportes al Deporte Federado”, destinado a federaciones y asociaciones. Los interesados pueden inscribirse hasta el 30 de abril.

Al respecto, el ministro de Desarrollo Social, Danilo Capitani, expresó que “las instituciones deportivas son una herramienta importante para el desarrollo de la comunidad, no sólo a través de la contención social, sino también de la transmisión cultural y de valores. Tenemos una fuerte decisión política de fortalecer el apoyo que venimos realizando desde la gestión, en momentos críticos como los que nos toca vivir”.

La iniciativa “Aportes del Capital Privado”, destinada a instituciones deportivas de primer grado, tiene como objeto apoyar obras en los clubes vía deducción de ingresos brutos de los contribuyentes.

Aquellas entidades que se hayan presentado años anteriores podrán hacerlo también en esta oportunidad, teniendo como requisito adicional haber realizado la rendición completa de fondos hasta la fecha.

En tanto, la propuesta “Aportes al Deporte Federado” está dirigida a federaciones y asociaciones deportivas y destinada a calendarios deportivos nacional y provincial, y actividades relacionadas con las competencias de las distintas disciplinas deportivas federadas.

**Más información**

Quienes requieran más información pueden comunicase con la Secretaría de Deportes:

>> En la ciudad de Santa Fe, al teléfono 0342 4574834/36/37 o dirigirse a Bv. Gálvez 1228, de lunes a viernes de 7 a 13 horas.

>> En Rosario, deben concurrir a calle San Martín 2910, de 8 a 14 horas, o comunicarse al teléfono 0341 4772583.

También podrán realizar consultas a los mails: administracionrosario@santafe.gov.ar o clubesdeportes@santafe.gov.ar

Para acceder a los requisitos referentes al programa Aportes del Capital Privado ingresar en el siguiente link: [https://bit.ly/38IbCCa](https://bit.ly/38IbCCa "https://bit.ly/38IbCCa")

Consejo Provincial del Deporte

Las actividades de este Consejo refuerzan la participación de la sociedad civil en las políticas públicas, dado que la asignación del dinero que se invierte desde el Estado es el resultado del consenso o la votación de los representantes de las instituciones deportivas.