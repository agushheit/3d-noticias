---
category: Agenda Ciudadana
date: 2021-09-24T06:15:12-03:00
thumbnail: https://assets.3dnoticias.com.ar/CONCEJOFEDERAL.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Perczyk pidió el regreso a la presencialidad plena en las escuelas de todo
  el país
title: Perczyk pidió el regreso a la presencialidad plena en las escuelas de todo
  el país
entradilla: Las autoridades educativas provinciales profundizarán la identificación
  y búsqueda de estudiantes que hayan interrumpido su escolaridad y garantizarán los
  contenidos educativos según las diferentes realidades.

---
El regreso a la presencialidad plena en las 24 jurisdicciones del país y la creación de un fondo federal para buscar a los estudiantes que hayan interrumpido o se hayan desvinculado del proceso educativo debido a la emergencia sanitaria fueron los ejes de la 111° asamblea del Consejo Federal de Educación, la primera de la gestión del ministro Jaime Perczyk.

El titular de la cartera de Educación pidió el regreso a la "presencialidad plena" y la creación de un fondo federal para buscar a los estudiantes que hayan interrumpido o se hayan desvinculado del proceso educativo por la emergencia sanitaria.

**El encuentro**

Durante la asamblea, Perczyk aseguró: "Tenemos que lograr que todos los chicos y las chicas estén en sus escuelas", y agregó que de este modo "avanzamos hacia la presencialidad plena y la recuperación de conocimientos".

Entre los temas abordados se recomendó "a todas las provincias el retorno a la presencialidad plena en el Sistema Educativo Nacional, considerando todos los establecimientos de los niveles y modalidades".

La medida está en línea con las primeras declaraciones del ministro, quien en declaraciones radiales dijo hoy que "no nos podemos permitir como sociedad que los chicos se vayan de la escuela, hay que poner eso como faro, como obsesión".

También dijo que la escuela "es el lugar" en el que tienen que estar los estudiantes y agregó que "buscarán uno por uno a los alumnos que se desvincularon de la escolaridad" en el contexto de pandemia.

Con este objetivo se anunció hoy la creación del Fondo Federal "Volvé a la Escuela" que, con una inversión de $5.000.000.000, buscará a las alumnas y alumnos que interrumpieron el proceso educativo o se desvincularon de la escuela.

Según divulgó el Ministerio de Educación el 14 de septiembre, la mayoría del 1.800.000 de alumnos que necesitan reafirmar su presencia en la escuela pertenece al nivel medio.

De ese total, el 54% pertenece al nivel secundario (1.014.414 adolescentes y jóvenes); el 40%, al nivel primario (751.446 niños/as); y el 6%, al nivel inicial (104.746 niños/as).

Durante la Asamblea, además se acordó que "tanto las jurisdicciones como la comunidad educativa extremen los cuidados preventivos de salud, en especial: uso del barbijo, lavado de manos, ventilación adecuada y utilización de todos los espacios escolares abiertos que hagan posible los procesos de enseñanza".

Además, las ministras y los ministros de todas las provincias del país y de la Ciudad Autónoma de Buenos Aires aceptaron la propuesta para designar a Marcelo Mango como secretario general del Consejo Federal de Educación.

La propuesta de vuelta a la presencialidad plena fue saludada por autoridades educativas de los distintos distritos del país.

La ministra de Educación de Corrientes, Susana Benítez, expresó: "Hemos aprobado por unanimidad una nueva resolución del Consejo Federal de Educación en la cual se dispone que todas las alumnas y todos los alumnos de nuestro país volverán a las clases presenciales, pero ya sin las restricciones del distanciamiento que teníamos marcado anteriormente".

En tanto, el ministro de Educación de Córdoba, Walter Grahovac, indicó: "Apoyo la decisión de retomar las clases presenciales en la totalidad de la matrícula porque la presencialidad es, además de importante, insustituible para la práctica docente en la recuperación del vínculo pedagógico con estudiantes".

Por su parte, la directora general de Cultura y Educación de la provincia de Buenos Aires, Agustina Vila, sostuvo: "Los acuerdos alcanzados por todas las jurisdicciones, en esta nueva asamblea del Consejo Federal de Educación, reafirman la presencialidad plena y cuidada que venimos llevando adelante y nos permiten seguir profundizando las acciones para intensificar la enseñanza con diversos dispositivos que reconozcan las singularidades de las trayectorias educativas de las y los estudiantes".

A su turno, la presidenta del Consejo de Educación de Santa Cruz, María Cecilia Vázquez, afirmó: "Nos sumamos a la iniciativa de profundización del acompañamiento de trayectorias a las y los estudiantes en la pospandemia, destacando la importancia de articular esfuerzos técnicos y financieros con el Ministerio de Educación de la Nación".

Finalmente, el ministro de Educación de Chaco, Aldo Lineras, destacó: "Son importantes para la provincia las definiciones acordadas en este encuentro, en el marco actual de retorno progresivo a la presencialidad plena", y agradeció a las autoridades nacionales por el recibimiento, "reiterando el compromiso permanente de seguir participando activamente de estas acciones federales".

Asimismo, la resolución también establece la coordinación entre autoridades sanitarias y educativas para llevar adelante "el plan de vacunación y testeo Covid-19 a fin de garantizar su aplicación a los y las estudiantes, docentes y personal auxiliar".