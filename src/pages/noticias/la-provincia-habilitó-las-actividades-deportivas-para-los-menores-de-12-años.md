---
layout: Noticia con imagen
author: "Fuente: Gobierno de la Provincia de Santa Fe"
resumen: "Actividades deportivas "
category: Estado Real
title: La provincia habilitó las actividades deportivas para los menores de 12 años
entradilla: El gobernador Omar Perotti firmó el decreto que entró en vigencia
  desde la 0 hora de este sábado, en todo el territorio santafesino.
date: 2020-10-30T12:00:00.000Z
thumbnail: https://assets.3dnoticias.com.ar/menores-12.jpg
---
El gobierno provincial estableció, mediante el decreto N° 1227 firmado por el gobernador Omar Perotti, que desde la 0 hora de este sábado se habilita la práctica de actividades deportivas individuales para los menores de 12 años, con o sin interacción con oponentes, sin contacto físico en este último caso. La norma incluye también a las actividades de entrenamiento individual.

La habilitación comprende las disciplinas mencionadas en los considerandos del mencionado decreto y las oportunamente establecidas en el decreto N° 0474/20 y sus normas complementarias.

El anexo único del decreto N° 0474/20 enumera distintas actividades deportivas habilitadas: deportes individuales (atletismo -excluidas las postas-, equitación, arquería, BMX, running, ciclismo, canotaje, remo y kajak individual, gimnasia artística y con aparatos, patín artístico, etc.). Deportes individuales en interacción con oponentes (tenis, pelota a paleta, tenis con paleta o criollo, fútbol tenis, tenis de mesa, padel) y actividades de entrenamiento individual para otras disciplinas (judo, artes marciales, etc.).

**OBSERVACIÓN DE LAS NORMAS DE PROTOCOLO**

Asimismo, el decreto firmado por el mandatario santafesino aclara que aquellas actividades habilitadas por el artículo precedente que involucren la utilización de infraestructura de clubes, centros o complejos deportivos o similares deberán observar, en su desarrollo, las normas del protocolo base (PROBA) aprobado por el Ministerio de Salud y la Secretaría de Deportes dependiente del Ministerio de Desarrollo Social.

En ese marco, “las autoridades municipales y comunales, en concurrencia con las autoridades provinciales competentes, coordinarán los procedimientos de fiscalización necesarios para garantizar el cumplimiento de los protocolos correspondientes a las actividades habilitadas y de las normas dispuestas en virtud de la emergencia sanitaria; quedando facultadas a proponer mayores restricciones, requisitos y definir horarios y demás modalidades particulares en sus distritos para el desarrollo de las mismas”, destaca el decreto.