---
category: Agenda Ciudadana
date: 2021-06-01T08:42:10-03:00
thumbnail: https://assets.3dnoticias.com.ar/niños.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Lt10 '
resumen: Se abre en Santa Fe una nueva convocatoria para adopciones
title: Se abre en Santa Fe una nueva convocatoria para adopciones
entradilla: Pueden inscribirse todas las personas o parejas que deseen. Los requisitos
  detallados.

---
Por segunda vez en el año, el Registro Único de Aspirantes a Guarda con fines Adoptivos (Ruaga) de la provincia de Santa Fe abrirá este martes un nuevo período de inscripción para personas con interés en adoptar.

Magdalena Galli Fiant, directora del Ruaga Santa Fe, brindó una conferencia de prensa y dio detalles de esta nueva convocatoria: **"Pueden inscribirse todas las personas o parejas que deseen empezar este camino de la adopción".**

La directora mencionó que la inscripción es gratuita y puede hacerse de forma online en la página de la provincia ([http://www.santafe.gov.ar/](http://www.santafe.gov.ar/ "http://www.santafe.gov.ar/")[ ](https://www.santafe.gov.ar/)), cliqueando en el botón que dice "RUAGA", ubicado en la parte superior. "Ahí se encontrarán con el formulario F1 que es el que se debe descargar para completar y luego enviarlo por mail. Es un trámite gratuito, accesible y al alcance de todos", sostuvo Galli Fiant.

![](https://www.lt10.com.ar/multimedia/in1622493798324.jpeg)

Con respecto a los requisitos para la inscripción al registro, que estará abierto hasta el 10 de junio, la directora indicó: "Pedimos sus datos personales, de cada uno de los postulantes, también pedimos sugerencias o cuestiones que quieren tratar en los encuentros informativos". En este sentido, y para despejar inquietudes, una vez recibida la información, el Ruaga hará una convocatoria a un encuentro informativo, que se hará de forma virtual.

![](https://www.lt10.com.ar/multimedia/in1622493808618.jpeg)