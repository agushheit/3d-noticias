---
category: Agenda Ciudadana
date: 2021-01-26T20:00:00Z
thumbnail: https://assets.3dnoticias.com.ar/gobernadores.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Con información de Infobae
resumen: Uno por uno, quienes son los gobernadores que quieren suspender las PASO
title: Uno por uno, quienes son los gobernadores que quieren suspender las PASO
entradilla: a mayoría de los mandatarios provinciales pretenden dar de baja, por única
  vez, las elecciones primarias. Alberto Fernández incluyó en la agenda de las sesiones
  extraordinarias del Congreso un proyecto que las suspende

---
La mayoría de los gobernadores del país quieren suspender las PASO en el 2021. Por única vez y con la justificación de que el escenario sanitario sigue siendo demasiado complejo como para habilitar un enorme movimiento de personas en dos oportunidades consecutivas. Además, aducen que la reducción del gasto económico les permitiría seguir derivando fondos para el esquema sanitario y social que sostienen desde el inicio de la pandemia.

El viernes pasado el presidente Alberto Fernández incluyó el proyecto para suspender las PASO en la agenda de las sesiones extraordinarias que tendrá el Congreso. Así habilitó a los legisladores a tratar el tema y les hizo un guiño a los mandatarios provinciales, que le habían pedido en dos oportunidades que apoye la suspensión de los comicios.

La primera vez que recibió el pedido fue en diciembre, luego de que todos los gobernadores, menos el jefe de Gobierno porteño, Horacio Rodríguez Larreta, firmaron el Consenso Fiscal. En esa oportunidad 21 de los 24 mandatarios le plantearon la necesidad de suspender las elecciones primarias con antelación. Fernández les dijo que debía ser por consenso y los avaló para que impulsen un proyecto.

![Los gobernadores Gustavo Saenz, Gerardo Morales, Ricardo Quintela, Gerardo Zamora, Raúl Jalil, Juan Manzur y Jorge Capitanich ](https://www.infobae.com/new-resizer/RvAjYx3-fJSTqWMAW4x7z7UVXsI=/420x236/filters:format(jpg):quality(85)//cloudfront-us-east-1.images.arcpublishing.com/infobae/OL3IXDQ3BNHAPCMO66T6U2OCRE.jpg)

Una semana después de ese encuentro el legislador tucumano Pablo Yeldlin presentó un proyecto para suspender las PASO. El documento ingresó al Congreso pero quedó congelado durante más de un mes. El año electoral se puso en marcha y entonces la posibilidad de modificar las reglas del juego comenzaron a complicar las negociaciones.

Finalmente, el proyecto quedó incluido en la agenda de las sesiones extraordinarias y debería ser tratado en el corto plazo. El Presidente les encargó a los gobernadores que junten los votos para poder aprobar la ley, lo que implica una tarea extremadamente compleja.

Hay varios motivos que detallan la complicación del trabajo que deben hacer los legisladores que responden a los mandatarios. El principal es que el gobernador de Buenos Aires, Axel Kicillof, dijo que no es un tema importante para su gestión la suspensión de las PASO y será muy difícil que los legisladores del oficialismo apoyen la medida.

![Los mandatarios Sergio Ziliotto, Arabela Carreras, Mariano Arcioni, Oscar Herrera Ahuad, Gildo Insfrán y Gustavo Bordet ](https://www.infobae.com/new-resizer/aTPQu8rUiFw13SiiOzLXKlUBqzs=/420x236/filters:format(jpg):quality(85)//cloudfront-us-east-1.images.arcpublishing.com/infobae/ZUP2GVISIJDNHGFSBXZG3QSPSA.jpg)

El planteo del mandatario bonaerense tiene que ver con que en la Legislatura de la provincia un proyecto similar avanzaría debido a que en el Senado la oposición tiene la mayoría y Juntos por el Cambio ya planteó, en su mayoría, que no avalará la suspensión de los comicios.

El consenso político que pidió Fernández tiene que replicarse en el Congreso. La mayoría de los gobernadores están a favor de suspender las elecciones, algunos se bajaron de la iniciativa y le pusieron paños fríos al tema, otros optaron por no manifestarse y solo Horacio Rodríguez Larreta dejó en claro que no considera necesario suspender las primarias.

El primero en proponer la suspensión de las PASO fue el sanjuanino Sergio Uñac. El 2 de noviembre planteó la posición a través de sus redes sociales. “Teniendo en cuenta las Elecciones Legislativas 2021, y respetando los mandatos constitucionales y el recambio de las autoridades elegidas por voto popular, considero oportuno que se evalúe la suspensión de las PASO establecidas por Ley Nº 26.571. Esto significaría menor riesgo sanitario y mayor ahorro económico”, escribió en su momento.

![El gobernador de San Juan, Sergio Uñac, fue el primero en proponer que las PASO sean suspendidas (Presidencia)](https://www.infobae.com/new-resizer/BIYYORcYLlJ4HRMQs9I1ZostADI=/420x236/filters:format(jpg):quality(85)//cloudfront-us-east-1.images.arcpublishing.com/infobae/YJFUCWAFMNDPVAAR5KNC3PWJIE.jpg)

Luego, el salteño Gustavo Sanz, un mes después, decidió suspender las PASO locales. Con el paso del tiempo el chaqueño Jorge Capitanich y el tucumano Juan Manzur se pusieron a la cabeza del reclamo. Así el reclamo fue tomando volumen y se convirtió en un consenso amplio dentro del esquema de los gobernadores. El último fin de semana fue el correntino Gustavo Valdés el que puso la cara y dijo: “Las PASO nunca sirvieron para nada, el Congreso las debería suspender por única vez”.

Hay 19 gobernadores que están a favor de la suspensión. Son Gustavo Saenz (Salta), Gerardo Morales (Jujuy), Raúl Jalil (Catamarca), Gerardo Zamora (Santiago del Estero), Jorge Capitanich (Chaco), Ricardo Quintela (La Rioja), Juan Manzur (Tucumán), Sergio Uñac (San Juan), Juan Schiaretti (Córdoba), Omar Perotti (Santa Fe), Gustavo Valdés (Corrientes), Omar Gutiérrez (Neuquén), Gustavo Melella (Tierra del Fuego), Arabela Carreras (Río Negro), Gustavo Bordet (Entre Ríos), Gildo Insfrán (Formosa), Sergio Ziliotto (La Pampa), Oscar Herrera Ahuad (Misiones) y Mariano Arcioni (Chubut).

![Los gobernadores Omar Perotti, Omar Gutiérrez, Sergio Uñac, Juan Schiaretti, Gustavo Melella y Gustavo Valdés ](https://www.infobae.com/new-resizer/yV7ca1fPp4tFe5GaWEo18gWnJZk=/420x236/filters:format(jpg):quality(85)//cloudfront-us-east-1.images.arcpublishing.com/infobae/LRNEXNXVUBCLXGO6LBR7JJPDNY.jpg)

Axel Kicillof (Buenos Aires) ya dejó en claro que el tema no es prioritario para su gestión. No inclinará la balanza para ningún lado. Rodolfo Suárez (Mendoza) sostiene que solo se deben suspender las PASO en los distritos donde no haya competencia. A algunos de sus colegas les había marcado, en primera instancia, su voluntad de suspenderlas, pero después no se expresó. En el grupo de los mandatarios entienden que está condicionado el gobernador Alfredo Cornejo, uno de los líderes de Juntos por el Cambio, espacio que no avala la suspensión.

Alberto Rodríguez Saá (San Luis) y Alicia Kirchner (Santa Cruz) no se expresaron en público sobre el tema y pretenden mantenerse el silencio. Horacio Rodríguez Larreta (Ciudad de Buenos Aires) es el único de los mandatarios que se expresó en contra de la suspensión. Es uno de los puntales de la oposición, que en esta oportunidad se vio dividida por el tema, ya que los gobernadores radicales presionan para dar de baja las primarias.

A los gobernadores les costará tener el número necesario para aprobar la ley. La reforma de una ley electoral necesita de la mitad más uno de los legisladores que integran la Cámara. En la Cámara de Diputados necesitan 129 votos y en el Senado 37 votos. Necesitan una mayoría especial que, por el momento, no tienen.