---
category: La Ciudad
date: 2021-12-30T06:15:14-03:00
thumbnail: https://assets.3dnoticias.com.ar/LAERTAMARILLA.png
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Alerta amarilla: cómo prevenir los golpes de calor ante las altas temperaturas'
title: 'Alerta amarilla: cómo prevenir los golpes de calor ante las altas temperaturas'
entradilla: " Desde la Municipalidad recomiendan no exponerse al sol entre las 10
  y las 16 horas, tener cuidado en las comidas y la hidratación, y estar atentos a
  ciertos síntomas. \n\n"

---
El Servicio Meteorológico Nacional publicó un alerta por temperaturas extremas (calor) para esta zona, para los próximos días. El nivel es amarillo cuyos efectos son considerados leves a moderados en la salud. No obstante, es considerado peligroso para los grupos de riesgo, como bebés y niños pequeños, mayores de 65 años y personas con enfermedades crónicas.

Ante esta situación, la Municipalidad de Santa Fe brinda recomendaciones sobre cómo prevenir el golpe de calor. A modo preventivo, la Dirección de Salud municipal recuerda la importancia de no exponerse al sol directo durante las 10 y las 16, ni de manera prolongada. Además, es menester usar ropa clara y liviana, colocarse protector, permanecer en espacios ventilados o acondicionados y, sobre todo, evitar hacer ejercicio en la hora pico: es totalmente desaconsejable hacer actividad física con una temperatura superior a los 30 grados.

**Atentos a los síntomas**

El agotamiento por altas temperaturas, que es el estadio previo al golpe de calor, se puede reconocer por una sudoración excesiva. Sed intensa o sequedad de las mucosas, calambres musculares, debilidad y cansancio. Los dolores en la zona abdominal y la falta de apetito son otros de los síntomas que hay que tener en cuenta, como así también las náuseas, vómitos o dolores de cabeza.

En el caso de los bebés, se puede detectar por la piel muy irritada o sudor abundante en el cuello, las axilas y el pecho. También se puede evidenciar por la piel pálida y fresca o la irritabilidad plasmada en el llanto incontrolable.

Cuando se da un cuadro de golpe de calor se puede observar la piel roja, caliente y seca. Por otro lado, la respiración y la frecuencia cardíaca se aceleran. También se puede observar un dolor de cabeza intenso, sensación de mareo, náuseas, vómitos.

**Cómo prevenir y qué hacer**

Para evitar el golpe de calor es indispensable cuidar la alimentación y la hidratación. En el caso de los lactantes se aconseja amamantar con mayor frecuencia y en el caso de los niños pequeños y los adultos mayores, hay que ofrecer constantemente agua potable, o jugos naturales.

En tanto, se desaconseja la ingesta de gaseosas, bebidas con cafeína o alto contenido en azúcares. Tampoco se aconsejan las comidas con alto valor calórico. El consumo de bebidas alcohólicas también está desaconsejado. Otra acción que hay que evitar es dejar a los niños en habitáculos sin ventilación, como por ejemplo el interior de un auto.

Ante la situación de un golpe de calor, a la persona se la debe posicionar boca arriba, en un sitio fresco, a la sombra y bien ventilado, así como también, quitarle la ropa que no necesite y aflojar la que le oprime. También puede ayudar colocar compresas de agua fría en la cabeza, la cara, el cuello, la nuca y el pecho, e ir cambiándolas a medida que se calienten.

En caso de que esté consciente, se le deben ofrecer líquidos como agua segura, de rehidratación oral o bebidas isotónicas. Si está en estado de inconsciencia, es necesario activar de inmediato el sistema de emergencias sanitarias.