---
category: Agenda Ciudadana
date: 2021-09-04T06:15:16-03:00
thumbnail: https://assets.3dnoticias.com.ar/MUTANT.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Las claves de "Mu", la variante que es vigilada por la OMS y sería resistente
  a las vacunas
title: Las claves de "Mu", la variante que es vigilada por la OMS y sería resistente
  a las vacunas
entradilla: También es conocida como B.1.621. Pasó a ser una Variante de Interés,
  porque sus mutaciones indican un potencial de “escape inmune”.

---
La Organización Mundial de la Salud puso bajo vigilancia una nueva variante del coronavirus que podría ser capaz de contagiar a pesar de las vacunas. Bautizada "Mu", siguiendo su política de nombrar las variantes en función del alfabeto griego y no por su expansión geográfica, la variante B.1.621 se detectó por primera vez en Colombia a principios de año.

La información apareció este lunes en el boletín epidemiológico semanal de la OMS. En él se explica que la variante "Mu" pasó a ser una Variante de Interés bajo vigilancia, porque sus mutaciones indican un potencial de "escape inmune", es decir, que pueden desafiar las vacunas.

La variante apareció desde entonces en brotes esporádicos en América del Sur y en Europa. Su prevalencia global es mínima, de en torno al 0,1% pero es importante en Ecuador (13%) y Colombia (39%). "Aunque la prevalencia global de la variante Mu entre los casos secuenciados ha disminuido y es actualmente inferior al 0,1%, su prevalencia en Colombia (39%) y Ecuador (13%) ha ido aumentando de forma constante", explicó la OMS.

La organización dependiente de Naciones Unidas subraya que se necesitan más estudios para comprender mejor sus características y su circulación conjunta con la omnipresente variante Delta.

"Está en análisis su impacto y su capacidad de producir contagios masivamente, van pocos días desde que se aisló, hay que verlo todavía, estamos observando", dijo el ministro de Salud de la provincia de Buenos Aires, Nicolás Kreplak, al ser consultado sobre la variante Mu.

En la misma línea, aseguró que mientras mucha población no esté vacunada "aumenta mucho el riesgo de más mutaciones", pero insistió en que "aparentemente, como todo lo que viene sucediendo hasta ahora, las vacunas funcionarían, pero son observaciones totalmente preliminares".

Todos los virus, incluido el SARS-CoV-2, causante de la Covid-19, mutan con el tiempo y la mayoría de las mutaciones tienen poca o ninguna incidencia en las características del virus. No obstante, algunas mutaciones pueden afectar las propiedades del virus e influir, por ejemplo, en su capacidad de propagación, la gravedad de la enfermedad que provoca o la eficacia de las vacunas, medicamentos u otras medidas para combatirlo.