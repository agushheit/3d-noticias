---
category: La Ciudad
date: 2021-03-04T00:25:41-03:00
thumbnail: https://assets.3dnoticias.com.ar/alquiler.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Diario Uno
resumen: 'Descongelamiento de alquileres en Santa Fe: "Quedarán muchos inquilinos
  sin techo"'
title: 'Descongelamiento de alquileres en Santa Fe: "Quedarán muchos inquilinos sin
  techo"'
entradilla: Pedro Peralta, representante local del Frente Nacional de Inquilinos,
  advirtió que la decisión del gobierno nacional es una "gran imprudencia". En Santa
  Fe, hay 20.000 familias que alquilan

---
Tras conocerse la decisión del gobierno nacional de no extender el congelamiento de los alquileres a partir del 1 de abril próximo, quienes alquilan o deben alquilar empiezan a tener temor por su futuro y es lógico. Un mercado que se achica cada vez más debido a la escasa oferta, más una inflación que no da tregua a lo que se suma la crisis generada por la pandemia que dejó a miles sin empleo o en la cuerda floja laboral, son causas más que suficientes para entenderlo de esa manera. Y en Santa Fe la situación es muy preocupante. Esta mañana, desde el sector de propietarios e inmobiliarias, ya abrieron el paraguas en torno a la medida nacional y adelantaron que se vendrán subas de precios. Ante esta situación, UNO Santa Fe consultó a Pedro Peralta, representante en Santa Fe del Frente de Inquilinos Nacional (FIN), quien advirtió que son muchas las familias que corren el riesgo de quedarse sin techo.

> "Se viene una crisis habitacional en Santa Fe y el resto del país como nunca se vio", dijo Peralta, tras conocerse la medida del gobierno nacional por los alquileres y la situación que se genera para los inquilinos.

Vale recordar que ayer el Gobierno Nacional anunció que no habrá una nueva extensión del congelamiento de alquileres y suspensión de desalojos. El ministro de Desarrollo Territorial y Hábitat, Jorge Ferraresi, explicó que en su lugar se hará uso de las herramientas que generó la nueva ley de alquileres.

“No se entiende la medida del gobierno nacional, porque la emergencia sanitaria sigue. Recién se está empezando a vacunar a la población, a un espectro muy reducido, lo cual implica que por lógica se tendría que extender la emergencia habitacional”, señaló Pedro Peralta, del FIN.

Peralta estimó que solo “en la ciudad de Santa Fe hay 20.000 familias alquilando. Es decir, tenemos 20.000 familias que hoy no saben si se quedan, si renuevan, si tienen que buscar un alquiler más barato, si tienen que volver a la casa de los padres, de la familia, vivir hacinados, con una deuda, en medio de una emergencia sanitaria. Precisamente uno de los eslóganes era esto de «nos cuidamos entre todos, quedate en casa», pero ¿en qué casa te vas a quedar con esta situación? Parece irrisorio”, analizó.

El representante del FIN en Santa Fe dijo que lo más urgente hoy es “que se extienda la prórroga a los inquilinos, por lo menos hasta que se termine definitivamente la pandemia, y que después tiene que haber otro paso que es el que a nosotros nos interesa, que es que se empiece a regular el mercado inmobiliario, que se empiece a aplicar la ley y que el Estado dé acceso digno a la vivienda, que el Estado pueda garantizar un techo a todos los inquilinos”.

**Mediación**

Para Peralta, más que nunca, “tiene que haber una oficina que defienda los derechos del inquilino y del propietario también, ahora más que nunca va a ser necesaria una instancia de mediación, urgente, porque si se levanta la prohibición de los desalojos, habrá muchos casos de desalojo, ¿y quién va a mediar para defender al inquilino?”.

“Va a quedar mucha gente en la calle –subrayó. Ya estaban haciéndolo, o intentándolo, desde diciembre, intimando a las garantías, cosa que no se puede hacer, estaban queriendo desalojar al inquilino cuando había un decreto que lo amparaba. Imaginate lo que va a ser ahora, porque encima a los inquilinos que tenían deuda se la van a querer cobrar con intereses, van a poner abogados, van a embargar sueldos”.

Peralta agregó que “se viene una situación muy grave. Nosotros como frente hace meses lo venimos advirtiendo en todos los estamentos del Estado, en lo local, provincial y nacional. Acá en Santa Fe está la oficina del inquilino que depende de Atención Ciudadana, pero tiene un alcance limitado”, describió.

El dirigente rememoró que "esta crisis no es de ahora, es de varios años antes, desde los años 70, al dolarizarse las propiedades, los terrenos y las casas empieza a haber una emergencia habitacional que hoy decanta en casi 10 millones de inquilinos en todo el país".

"Nosotros –recordó– tenemos también una propuesta de alquiler social. El Estado debe encargarse de la construcción de casas para poner alquiler social para aquellas personas que no pueden pagar alquileres de forma particular con inmobiliaria. Y también lo que pedimos es que el Estado intervenga en la regulación del mercado para que no sea tan leonino el costo a la hora de ir a alquilar".

**Recomendaciones**

Frente a las situaciones que pueden empezar a darse con el descongelamiento de precios de alquileres y la posibilidad de desalojos, Peralta recomendó primero, “negociar con el propietario, con la inmobiliaria, la continuidad de los contratos que se venzan. Después si tiene deuda, renegociar el refinanciamiento de deuda. que el decreto establece que se puede hacer hasta en 12 cuotas, con intereses, que fija el BCRA, y puede ser mayor al 3%. No es lo que ellos quieran, no son intereses punitorios ni moratorios, es un interés que se fija por la financiación de la deuda. Eso los inquilinos no lo saben, pero las inmobiliarias ya lo están haciendo. Y le van a querer cobrar todo”, volvió a advertir.

“En el caso de que el inquilino no sepa cómo seguir la negociación, se puede comunicar con nosotros al Frente de Inquilinos Nacional, a mi Facebook. o mi correo electrónico (peraltapedromiguel@gmail.com). Y por supuesto, que acudan a la oficina Derechos y Vinculación Ciudadana de la Municipalidad de Santa Fe (3425315450)”.

“Apelamos a la voluntad política de los Estados municipal, provincial y nacional, de hacerse cargo de un problema histórico, porque las consecuencias van a ser muy graves, como nunca lo vivimos. ¿Nunca vivimos un contexto de pandemia? Bueno, lo mismo se viene con la crisis habitacional”, proyectó.