---
category: La Ciudad
date: 2021-01-28T09:32:48Z
thumbnail: https://assets.3dnoticias.com.ar/bombeo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad de Santa Fe
resumen: 'Reparaciones y nuevo equipamiento: los trabajos del municipio en las estaciones
  de bombeo'
title: 'Reparaciones y nuevo equipamiento: los trabajos del municipio en las estaciones
  de bombeo'
entradilla: La Municipalidad trabaja de manera planificada para llevar al 100% la
  capacidad de las estaciones de bombeo del cordón Oeste y así optimizar su funcionamiento
  los días de lluvia.

---
La Municipalidad de Santa Fe avanza con distintos trabajos para optimizar el funcionamiento de las estaciones de bombeo del cordón Oeste. Las tareas están concentradas en las estaciones de la 1 a la 4, ubicadas entre los barrios Chalet y Barrio Schneider, pero que drenan a todos los barrios del Distrito Oeste y Distrito Suroeste. Las mismas no poseen descarga por gravedad, por lo que la seguridad hídrica depende del buen funcionamiento de las bombas.

Los trabajos buscan llevar al máximo el potencial de funcionamiento de las estaciones para reforzar la protección contra inundaciones. Las intervenciones demandan una inversión de 4,7 millones de pesos, financiadas a través de un aporte no reintegrable realizado por el gobierno de la provincia.

**Acciones planificadas**

En el cordón Oeste, cada una de las estaciones tienen un juego de electrobombas (bombas eléctricas) y una motobomba (mecánica) que funciona con combustible. Por eso, parte de los trabajos consiste en la reparación y mantenimiento de las motobombas con el objetivo de evitar el deterioro y optimizar la utilidad de este equipamiento que fue diseñado para uso continuo y se acciona ante lluvias extraordinarias, cuando los reservorios están crecidos.

Al respecto, la secretaria de Asuntos Hídricos y Gestión de Riesgos, Silvina Serra, informó que "nuestros operarios se encuentran a la par de los mecánicos poniendo en un estado óptimo las bombas y se están capacitando para que, en adelante, el mantenimiento sea Municipal. La planificación incluye un mantenimiento periódico de cada una de las bombas de las estaciones de bombeo, que es lo que nos garantizaría que, en el momento que las necesitemos, funcionen al ciento por ciento de su capacidad".

Además, se realiza el recambio de electrobombas modernas y de mayor capacidad en estaciones donde hay faltantes o ya no funcionan las existentes. Este reemplazo de equipamiento supone readaptar tanto la obra civil como eléctrica de cada estación, lo que incluye cambios en el espacio en el que se aloja la bomba, la salida y el sistema de cañerías y tablero de operaciones. "Estas obras las van a hacer nuestros operarios y ya se están realizando las compras de materiales para hacer estas modificaciones", puntualizó la funcionaria.

Sobre ambos trabajos, Serra detalló que "son las dos principales modificaciones que estamos haciendo para poder llevar al 100% la capacidad de las estaciones de bombeo. Consideramos que con estas acciones vamos a poder tener una muy buena respuesta al momento de lluvias intensas".

En paralelo, se ejecutan otras reparaciones en las estaciones para garantizar su habitabilidad, que incluye tareas de pintura y limpieza, cuidado del entorno y reparaciones en las estructuras edilicias, que son ocupadas las 24 horas del día por personal municipal, y a fin de brindar mayor seguridad tanto al operario como a la infraestructura, se trabaja en un proyecto de cercos perimetrales y sistema de monitoreo por cámaras.

**Mediciones**

Con el objetivo de contar con datos que permitan al municipio evaluar el comportamiento de los reservorios ante determinadas lluvias y monitorear la capacidad de las estaciones de bombeo, el trabajo de la Secretaría de Asuntos Hídricos y Gestión de Riesgos durante 2020 estuvo focalizado en realizar mediciones durante las lluvias.

Los estudios efectuados a lo largo del año pasado permiten, para Serra, "tener hoy evaluaciones de estaciones de bombeo muy valiosas para determinar si su capacidad es la adecuada o no. Seguimos realizando mediciones para evaluar de todas las estaciones de bombeo su capacidad, pero, en general, encontramos que si nosotros podemos lograr llevar al 100% la capacidad de las estaciones, tendríamos una buena respuesta para toda la vertiente oeste de la ciudad durante un evento importante de lluvia”.

![](https://assets.3dnoticias.com.ar/bombeo1.jpg)