---
category: Estado Real
date: 2021-01-09T11:25:46Z
thumbnail: https://assets.3dnoticias.com.ar/090121-casamiento-espacios-culturales.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Se celebraron los primeros casamientos en espacios culturales de la capital
  provincial
title: Se celebraron los primeros casamientos en espacios culturales de la capital
  provincial
entradilla: Fue en El Molino, espacio del Ministerio de Cultura de la Provincia, la
  primera ceremonia organizada en Santa Fe, en conjunto con el Registro Civil del
  Ministerio de Gobierno, Justicia, Derechos Humanos y Diversidad.

---
A mediados de diciembre pasado, la Provincia habilitó la celebración de matrimonios en El Molino y El Alero de Coronel Dorrego, como continuidad de la propuesta que comenzó a implementarse en Rosario en el Galpón 13 y el Petit Salón de la Plataforma Lavardén, dos espacios que también están bajo la órbita de la cartera de Cultura.

Siempre bajo los protocolos sanitarios, se celebrarán dos viernes al mes y podrán participar familiares y amigos. El Molino, Fábrica Cultural –ubicado en Bv. Gálvez y República de Siria– y El Alero de Coronel Dorrego –en Bv. French y Sarmiento–, recibirán a quienes reserven tu turno en [www.santafe.gob.ar](https://bit.ly/3hT4LZV "Portal de Trámites")

***

![](https://assets.3dnoticias.com.ar/2021-01-09-Candioti-Barrionuevo.webp)

En esta oportunidad, acompañaron a las parejas Juan Candioti e Itatí Barrionuevo, quienes animaron las ceremonias nupciales con música en vivo.