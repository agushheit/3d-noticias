---
category: Agenda Ciudadana
date: 2022-01-08T08:00:57-03:00
thumbnail: https://assets.3dnoticias.com.ar/ALBBERTOGUZMAN.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: La reunión entre el Gobierno y la oposición por el FMI entró en "stand by"
  y se hará en la segunda mitad de enero
title: La reunión entre el Gobierno y la oposición por el FMI entró en "stand by"
  y se hará en la segunda mitad de enero
entradilla: 'En medio del malestar en el oficialismo y las dudas en la oposición,
  Massa y Morales mantuvieron una charla para que el encuentro no se caiga y se realice
  el 17 o 18 de enero.

'

---
El encuentro entre el presidente Alberto Fernández, el ministro de Economía, Martín Guzmán, y los gobernadores y jefes parlamentarios de Juntos por el Cambio que estaba previsto para la semana próxima se postergó para la segunda quincena de enero, en medio de un clima enrarecido entre oficialismo y oposición.

"Por ahora está en stand by", señaló una fuente calificada del Gobierno a Noticias Argentinas respecto de la reunión pautada para conversar sobre la negociación con el FMI.

En Juntos por el Cambio afirmaron hasta último que, para ellos, el encuentro seguía en pie pero, al mismo tiempo, indicaron que desconocían si sería "el lunes o el martes, y también el lugar", según señalaron a este medio dos fuentes de ese espacio político.

El Gobierno demoró esas definiciones y, finalmente, bajó la expectativa en torno a la posibilidad de que la reunión se lleve a cabo la semana próxima y hasta puso en duda la participación del Presidente junto a Guzmán.

No obstante, fuentes al tanto de las negociaciones confirmaron a NA que el presidente de la Cámara de Diputados, Sergio Massa, y el gobernador de Jujuy y líder de la UCR, Gerardo Morales, mantuvieron una conversación en la que acordaron que la reunión "se haga el 17 o 18 de enero, en el Congreso".

De esta manera, el referente del Frente de Todos y el gobernador de Juntos por el Cambio buscaron despejar las dudas que sobrevolaron a la eventual reunión y garantizar que, pese a la desconfianza entre el Gobierno y la alianza opositora, se lleve a cabo.

Las fuentes consultadas señalaron que el "stand by" se debe al aislamiento de Massa por el diagnóstico positivo de Covid-19 que recibió su esposa, Malena Galmarini, aunque en la Casa Rosada lo atribuían al malestar con la actitud de los dirigentes nacionales de Juntos por el Cambio.

Los gobernadores de la UCR y el jefe de Gobierno porteño, Horacio Rodríguez Larreta, primero rechazaron la invitación de Guzmán para concurrir junto al resto de los mandatarios provinciales del país al encuentro del último miércoles, en el que el ministro expuso sobre la negociación con el FMI.

A esa reunión, Morales y los gobernadores Gustavo Valdés (Corrientes) y Rodolfo Suárez (Mendoza) enviaron representantes, a diferencia de Rodríguez Larreta que decidió mantenerse al margen, lo que expuso una vez más las diferencias internas en la coalición.

Finalmente, la mesa nacional de la coalición opositora ratificó que sus gobernadores y sus jefes parlamentarios irían a una reunión con Guzmán la semana próxima, pero criticaron la primera exposición del ministro, reclamaron a lleve "más precisiones" al encuentro previsto y pidieron que cualquier diálogo sobre el tema de la deuda con el FMI pase por el Congreso.

Esto generó malestar en la Casa Rosada y enfureció a los diputados del bloque oficialista que encabeza Máximo Kirchner, quienes salieron al cruce de los dirigentes opositores cuando todavía estaba la posibilidad de que la reunión se hiciera la semana próxima.

Cecilia Moreau, vicepresidenta del bloque oficialista, apuntó contra Rodríguez Larreta en Twitter y expresó: "Lo escucho y no puedo creer lo cínico que es, dice que la solución a la deuda que tomaron con el Fondo debe discutirse en el Congreso. La decisión del endeudamiento histórico al que nos llevaron Macri y Vidal, la tomaron solos. No consultaron ni a sus 'aliados'".

En tanto, el presidente de la Comisión de Presupuesto de Diputados, Carlos Heller, recordó que "durante la Presidencia de Mauricio Macri, se endeudaron con el FMI en 44 mil millones de dólares y se comprometieron a devolver esos préstamos en plazos imposibles" pero "no consultaron al Congreso".

"Ahora, convocados por el Gobierno a una reunión informativa en el camino de ir resolviendo el desastre que dejaron, ponen un pliego de condiciones para asistir. Son dos veces irresponsables: antes, cuando contrajeron la deuda; y ahora, cuando no se involucran en la solución", disparó Heller y sentenció: "Basta de chantaje".

En el interbloque opositor hubo quienes interpretaron esas críticas como un intento por "romper" el encuentro pautado, mientras que otros comentaron que el oficialismo intentaba aplazar la reunión porque Massa y Máximo Kirchner no iban a poder concurrir en esa semana.

Más allá de esas versiones, en las últimas horas Massa informó que se aisló preventivamente y eso ofreció un motivo más concreto para aplazar el encuentro.