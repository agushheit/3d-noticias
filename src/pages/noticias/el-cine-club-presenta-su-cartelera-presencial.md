---
category: La Ciudad
date: 2021-07-15T06:00:12-03:00
thumbnail: https://assets.3dnoticias.com.ar/CINE.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: El Cine Club presenta su cartelera presencial
title: El Cine Club presenta su cartelera presencial
entradilla: No se necesita reservar para las funciones. Para mayor orden se solicita
  llegar 10 minutos antes para cumplir con protocolos pertinentes.

---
Cine América reabre sus puertas este jueves, siguiendo todos los protocolos correspondientes, relacionados con el uso de barbijos, el distanciamiento social y el control de temperatura.

**TOUR DE CLÁSICOS**

**PEPI, LUCI, BOM Y OTRAS CHICAS DEL MONTÓN** de Pedro Almodovar, España, 1980, 82m. SAM 13. Viernes: 19:30hs.

**CORRE LOLA CORRE** de Tom Tykwer, Alemania, 1998, 81m. SAM 13. Sábado: 19:30hs.

**CASABLANCA** de Michael Curtiz, EEUU, 1942, 102m. ATP. Domingo: 17:00hs.

PINK FLOYD THE WALL de Alan Parker, Reino Unido, 1982, 95m. SAM 13. Lunes: 19:15hs.

**ANTES DEL AMANECER** de Richard Linklater, EEUU, Austria, Suiza, 1995, 101m. SAM 13. Martes: 17:00hs.

EASY RIDER de Dennis Hooper, EEUU, 1969, 95m. SAM 13. Miércoles: 17:00hs.

Además:

![](https://assets.3dnoticias.com.ar/15.png)![](https://assets.3dnoticias.com.ar/FFEEDNSTAGRAM CINE AMERICA 0.png)![](https://assets.3dnoticias.com.ar/v tour GRALES $200 SOCIOS $150 (6).png)