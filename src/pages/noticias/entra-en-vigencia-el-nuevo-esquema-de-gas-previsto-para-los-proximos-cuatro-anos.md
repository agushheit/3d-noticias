---
category: Agenda Ciudadana
date: 2021-01-02T10:31:10Z
thumbnail: https://assets.3dnoticias.com.ar/01012021-Gas.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Telam'
resumen: Entra en vigencia el nuevo esquema de gas previsto para los próximos cuatro
  años
title: Entra en vigencia el nuevo esquema de gas previsto para los próximos cuatro
  años
entradilla: Regirá el suministro para las distribuidoras de todo el país y las generadoras
  de electricidad. Como parte del plan, el Gobierno busca asegurar el incremento de
  la producción, las inversiones y el empleo.

---
El nuevo esquema de contratos de gas, que regirá el suministro para las distribuidoras de todo el país y las generadoras de electricidad, entró formalmente en vigencia, como parte de un plan mediante el cual el Gobierno busca asegurar el incremento de la producción, las inversiones y el empleo.

El Plan de Promoción de la Producción del Gas Natural Argentino-Esquema de Oferta y Demanda 2020-2024, aprobado por el Decreto 892 del 13 de noviembre, concretó a mediados de diciembre la adjudicación de 67 millones de metros cúbicos diarios (m³) de gas por los próximos cuatro años.

Además, antes de cada período invernal se prevé una nueva licitación para asegurar la cobertura del pico de demanda, la cual se anticipa se deberá completar con las importaciones desde Bolivia y los cargamentos de Gas Natural Licuado, como ocurre desde 2008.

El Gobierno nacional cerró con Bolivia la quinta adenda al acuerdo de venta de gas que ambos países mantienen desde 2006, y en el cual se explicitó que el país, durante 2021, recibirá un 30% menos de envíos por las dificultades productivas y operativas que atraviesan los yacimientos bolivianos.

La mayor parte de la adjudicación de los 70 millones de m³ realizada en diciembre quedó en manos de YPF que aportará al nuevo esquema 20,9 millones de m³ diarios que provendrán de sus operaciones en la formación no convencional de Vaca Muerta, con un precio de US$ 3,66 por millón de BTU.

Tecpetrol, la empresa energética del Grupo Techint que tiene comprometidas inversiones por US$ 1.400 millones para los próximos 5 años, fue el segundo mayor oferente y se adjudicó 9,94 millones de m³ diarios, también provenientes de la Cuenca Neuquina, a un precio de US$ 3,65 por millón de BTU.

Se destacaron las ofertas de la petrolera Pan American Energy por 8,4 millones m³ a valores de entre US$$ 3,49 y US$ 3,59 por millón de BTU que abastecerá desde sus operaciones en las Cuencas del Golfo y Austral.

Además de las ofertas de volúmenes y precios, las petroleras debieron detallar el plan de inversiones con la curva de producción y su incremento estimado para 2024.

De la misma manera, las productoras tienen pendiente –con la prórroga oficial– la presentación del programa de aumento en las contrataciones de pymes locales, regionales y nacionales, cuyo incremento a lo largo del programa deberá ser de entre un 30% y un 40%.

El Gobierno prevé que con la puesta en marcha del Plan Gas.Ar logrará llevar la producción del fluido a 30.000 millones de metros cúbicos en cuatro años, generar un ahorro fiscal de US$ 2.500 millones y evitar la salida de divisas por US$ 9.200 millones.

La Secretaría de Energía publicó en el último día hábil del año la Resolución 447 que aprobó formalmente la asignación de volúmenes de gas para cada productor y distribuidora, paso necesario para la concreción de los contratos entre las petroleras productoras con las distribuidoras y Cammesa.