---
category: La Ciudad
date: 2021-08-24T06:15:09-03:00
thumbnail: https://assets.3dnoticias.com.ar/CERESOLA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Ceresola
resumen: 'Solicitan que se le retiren los planes sociales a quienes corten calles '
title: 'Solicitan que se le retiren los planes sociales a quienes corten calles '
entradilla: Concejales del PRO presentaron una iniciativa para que a las personas
  que son beneficiarios de planes sociales se les retire dicha asistencia estatal
  en el caso de que provoquen cortes de la vía pública.

---
El proyecto fue ingresado al Cuerpo Deliberativo local por los Concejales del PRO (Juntos por el Cambio), Luciana Ceresola y Sebastián Mastropaolo. 

El texto hace referencia a la necesidad de establecer un mecanismo sancionatorio para aquellas personas (físicas o jurídicas) que gozan de algún beneficio o ayuda social monetaria por parte del Estado Nacional, Provincial o Municipal y cometan contravenciones o delitos en ocasión de manifestaciones públicas. 

La norma apunta fundamentalmente al delito de entorpecimiento de la normal circulación del transporte y peatones en la vía pública, dentro del ejido de la ciudad de Santa Fe de la Vera Cruz.

“Las organizaciones sociales, con el pasar del tiempo, se han politizado utilizando de manera abusiva el corte total o parcial de calles como modo de expresión, a los fines de obtener por este medio concesiones de diversa naturaleza”, dijo Ceresola. Y a modo de aclaración agregó, “Manifestarse es un derecho constitucional, pero el mismo no es absoluto y el ejercicio irrazonable de estos derechos, más aún cuando muchas veces están motivados en intereses políticos diferentes a los esgrimidos públicamente, violenta no solo el derecho de libre circulación del resto de los habitantes, sino también de manera indirecta una consecuencia el de trabajar, comerciar, enseñar y aprender, entre otros”.

Esa sanción debe consistir, según describe la redacción del proyecto, en la suspensión del beneficio recibido. La misma deberá ser aplicable desde el inicio de las actuaciones. Una vez que el proceso penal derive en una condena firme el beneficio debe ser eliminado. Sin embargo, la Concejal Luciana Ceresola aclara que, “la suspensión de los pagos puede ser revertida si el autor de la infracción repara los daños causados a través de tareas comunitarias a determinar por el Ejecutivo Municipal”.

En la misma dirección, la Edil Ceresola ya con anterioridad presentó un proyecto que tiene por objeto que las personas beneficiarias de planes o ayudas sociales realicen tareas de limpieza, mantenimiento, jardinería, reciclaje o cuidado del medio ambiente en espacios públicos de nuestra ciudad. 

“Es importante recuperar la cultura del trabajo que últimamente ha sido tan denostada. Los planes sociales deben ser un puente para las personas que carecen de un trabajo formal hasta que lo consigan. Mientras tanto deben hacer una contraprestación, eso hace a la dignidad de las personas que lo reciben y a la equidad en la distribución de los recursos públicos” expresó Luciana Ceresola.