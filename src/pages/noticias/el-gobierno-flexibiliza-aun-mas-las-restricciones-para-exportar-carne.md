---
category: Agenda Ciudadana
date: 2021-12-10T06:15:43-03:00
thumbnail: https://assets.3dnoticias.com.ar/CARNE.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: El Gobierno flexibiliza aún más las restricciones para exportar carne
title: El Gobierno flexibiliza aún más las restricciones para exportar carne
entradilla: 'También se anunció un acuerdo con los frigoríficos exportadores para
  destinar una oferta adicional de carne, en diciembre, de 20 mil toneladas.

'

---
El Gobierno anunció la apertura total para las exportaciones de vacas categoría D, E y F con destino a mercados emergentes, un pedido específico de las entidades del agro. Además, destacó el cumplimiento de todos nuestros compromisos internacionales: Cuotas Hilton, 481, Israel, EE.UU, Chile y Colombia.

El ministro de Agricultura, Julián Domínguez, confirmó el sostenimiento de los 7 cortes preferidos por los consumidores en el mercado interno: asado, tapa de asado, vacío, matambre, falda, paleta, nalga y/o cuadrada, y anticipó un acuerdo con los frigoríficos exportadores para destinar una oferta adicional de carne, en diciembre, de 20 mil toneladas.

El Gobierno presentó además un plan para potenciar la ganadería que incluirá créditos por unos $100.000 millones para aumentar la productividad.

Fue en un encuentro con los titulares de las cuatro entidades agropecuarias de la Comisión de Enlace. El plan, que establece un esquema de beneficios "directo para los productores", el sostenimiento de los cortes preferidos para el mercado interno a precios accesibles; la creación con rango institucional de un Consejo Consultivo con representantes de toda la cadena; y un nuevo esquema de exportaciones destinado a mercados emergentes.

El ministro de Agricultura dijo que "la decisión del presidente Alberto Fernández es aumentar la producción de carne, tanto para facilitar el acceso a todos los argentinos como para generar previsibilidad y confianza a toda la cadena".

El Plan, realizado junto al INTA, el SENASA y las universidades públicas, contempla beneficios como un nuevo esquema de exportaciones para la comercialización de todas las vacas categorías D, E y F.

También el impulso al aumento del peso de faena y al porcentaje de destete, con créditos a tasas subsidiadas destinados a productores; el estímulo a la inversión en genética y sanidad animal; y la creación con rango institucional de un Consejo Consultivo que tendrá el tarea de realizar el seguimiento de las medidas tomadas y posibles readecuaciones futuras y estará integrado por el Consejo Federal Agropecuario.

Incluirá, además, a CONINAGRO, CRA, FAA y SRA; las cámaras que representan a la Industria; los representantes de los trabajadores; la Cámara Argentina de Feedlot; la Mesa de la Carne; el IPCVA; la Cámara Argentina de Matarifes y Abastecedores; la Asociación de Productores Exportadores de la Argentina (APEA); y por las Universidades de Veterinaria y Agronomía de todo el país.

"Vamos a aumentar la productividad porque queremos producir más carne, con un horizonte claro para los próximos dos años: recuperar nuestro stock ganadero", sostuvo Domínguez.

Dijo que se está trabajando también sobre "la certificación verde, la estrategia Marca País, y para garantizar la trazabilidad de nuestra ganadería sustentable".

Participaron del encuentro el titular de la Sociedad Rural Argentina (SRA), Nicolás Pino; el presidente de Federación Agraria Argentina (FAA), Carlos Achetoni; su par de Confederaciones Rurales Argentinas (CRA), Jorge Chemes; y por Coninagro, Elbio Laucirica.

Domínguez, en tanto, estuvo acompañado por el gobernador de Santa Fe, Omar Perotti, y el de Santiago del Estero, Gerardo Zamora, que participó en forma remota. También estuvieron la vicegobernadora de Entre Ríos, María Laura Stratta; el ministro bonaerense de Desarrollo Agrario, Javier Rodríguez; y la titular de la cartera pampeana de Producción, Fernanda González.