---
category: Estado Real
date: 2021-03-18T06:45:28-03:00
thumbnail: https://assets.3dnoticias.com.ar/pusineri.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de LT10
resumen: El gobierno amenaza con descontar los días de paro a los docentes
title: El gobierno amenaza con descontar los días de paro a los docentes
entradilla: Luego de las nuevas reuniones paritarias realizadas este miércoles con
  Amsafe y UDA, el ministro de Trabajo pidió “reflexión” a los maestros públicos y
  volvió a ratificar la propuesta salarial de la Provincia.

---
Funcionarios provinciales mantuvieron hoy sendas reuniones con dirigentes de Amsafe y UDA, en el día en que se iniciaron las clases en las escuelas públicas, tras el paro de 48 horas de los docentes. Sin embargo, los nuevos encuentros no habrían redundado en un acercamiento de las partes, según puede deducirse de las declaraciones posteriores de la ministra de Educación Adriana Cantero y, fundamentalmente, de su par de Trabajo Juan Manuel Pusineri. 

“La política salarial está ratificada. Es la mejor política salarial del país”, remarcó Cantero ante la prensa, aunque aclaró que sí podrían conversarse “algunas otras cuestiones para ver posibilidades de aproximación”, vinculadas a lo pedagógico. 

En cuanto al nuevo paro por 48 horas resuelto por Amsafe para la semana próxima, deslizó: “la pretensión que nosotros tenemos es que los chicos tengan clases”. Y en ese sentido, no descartó volver a reunirse con los sindicatos antes del fin de semana. 

En el mismo sentido, Pusineri fue categórico al afirmar que “no está en evaluación hacer ningún cambio en la política salarial”, ya que lo ofrecido a los maestros "supera la paritaria nacional y las paritarias provinciales de provincias comparables con Santa Fe y de CABA”. 

Por ello, llamó “a la reflexión a la docencia, a los dirigentes gremiales” y les solicitó “tener en cuenta que al no aceptar la política salarial también se imposibilita efectuar el pago del aumento durante el mes de marzo”. 

Además, Pusineri advirtió: “No como represalia por el derecho de huelga, que está garantizado y está reconocido por la Constitución, pero sí (que sepan que) al no prestarse el servicio cabe la posibilidad de descontar los días que se pierden de clase”. 

“No tenemos esa decisión tomada pero es una posibilidad”, dijo al respecto Cantero. “Los días no trabajados podrían ser perfectamente no pagados”, remató. 

“En este contexto nos movemos y es que estamos llamando a la dirigencia y a los docentes, que sabemos que tienen muchas ganas de volver a las aulas, que tienen ganas de reencontrarse con los alumnos, a que revean la posición para la semana que viene”, cerró el titular de la cartera laboral.