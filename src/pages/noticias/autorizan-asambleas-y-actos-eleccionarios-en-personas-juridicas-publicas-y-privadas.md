---
category: Agenda Ciudadana
date: 2021-08-11T06:00:52-03:00
thumbnail: https://assets.3dnoticias.com.ar/ASAMBLEA.png
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Autorizan asambleas y actos eleccionarios en personas jurídicas públicas
  y privadas
title: Autorizan asambleas y actos eleccionarios en personas jurídicas públicas y
  privadas
entradilla: Será a partir del día 1 de septiembre de 2021.

---
La Inspección General de Personas Jurídicas informa que “conforme Decreto 1374/2021, se dispone en todo el territorio provincial, que a partir del día 1 de septiembre de 2021, se autorizan la realización en forma presencial de las asambleas y actos eleccionarios de las personas jurídicas públicas y privadas, inducidos los colegios y consejos profesionales; los que podrán concretarse cumplimentando las medidas de prevención sanitarias dispuestas por la Resolución General 8/2020 por IGPJ”.

Podrán concretarse cumplimentando las medidas de prevención sanitarias dispuestas por la Resolución General 8/2020 de la Inspección General de Personas Jurídicas del Ministerio de Gobierno, Justicia y Derechos Humanos y las demás condiciones específicas que en su caso establezcan el Ministerio de Salud, la Inspección general de Personas Jurídicas, o las autoridades municipales o comunales donde las actividades se realicen.

La misma dispone que, el órgano de administración de cada persona jurídica deberá establecer las condiciones de la reunión a fin de respetar las pautas sanitarias, previéndose como condiciones necesarias las siguientes:

a) Las actividades previstas en la presente sólo podrán tener lugar dentro de la franja horaria comprendida entre la hora 8 y la hora 20.

b) Antes de que cada asistente ingrese al lugar se deberá tomar su temperatura corporal.

c) Se exigirá a los asistentes la previa higienización de calzado y de las manos, aportando los elementos a tal fin.

d) Se exigirá a los asistentes la utilización de barbijo que cubra nariz, boca y mentón durante todo el tiempo que permanezcan en el lugar.

e) Si el lugar de realización de la reunión es cerrado, se mantendrá la ventilación permanente del mismo.

f) Se controlará la densidad de ocupación, de cada dos metros cuadrados de espacio circulable por cada asistente, tanto al aire libre como en lugares cerrados y controlar el distanciamiento entre los asistentes.

g) Se preverán lugares de ingreso y egreso independientes.