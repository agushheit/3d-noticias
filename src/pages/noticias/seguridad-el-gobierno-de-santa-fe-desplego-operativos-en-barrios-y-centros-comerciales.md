---
category: Estado Real
date: 2020-12-26T12:12:09Z
thumbnail: https://assets.3dnoticias.com.ar/seguridad.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: 'Seguridad: el gobierno de Santa Fe desplegó operativos en barrios y centros
  comerciales'
title: 'Seguridad: el gobierno de Santa Fe desplegó operativos en barrios y centros
  comerciales'
entradilla: Los operativos de Interceptación de Delitos Predatorios ( IDP) y de Intervención
  Estratégica Situacional (IES) fueron fundamentales en la prevención del delito.
  Los números y los resultados.

---
El gobierno de la provincia, a través de la subsecretaría de Control del ministerio de Seguridad, llevó adelante 174 operativos IES y 72 IDP, que fueron el resultado de las Mesas de Seguridad con Gobiernos locales y el trabajo en conjunto de las fuerzas provinciales, locales y federales.

El subsecretario de Prevención y Control, Alberto Mongia catalogó de positivo el balance de los operativos en cuanto a resultados y prevención: «Las Mesas de Seguridad son el puntapié inicial a la hora de llevar un operativo que, junto con el diálogo permanente con los que conforman esas mesas, hace que sepamos cómo desembarcar y dónde, con qué operativo, la hora y los agentes con los que contamos. Las medidas policiales que imponemos nacen de las mesas políticas que el ministro Sain conformó apenas asumimos, y los intendentes y presidentes comunales así lo entendieron», afirmó.

Por otra parte, el funcionario explicó que «todo esto se llevó adelante en una pandemia que exigía poner atención, además del delito, en el cumplimiento de las normas vigentes por covid y las medidas de aislamiento que exigía el gobierno, de modo que aún en modo virtual nunca perdimos el contacto ni la comunicación con ninguno de los actores políticos y policiales que se necesita a la hora de diagramar las intervenciones».

En cuanto a la cantidad de operativos y resultados, el subsecretario señaló que «los operativos se diagraman de acuerdo a la exigencia y la urgencia que requiere el barrio o los centros comerciales. Para eso hacemos un mapeo del delito y con esa información desembarcamos con agentes policiales, patrulleros, caminantes, motorizados, a caballo o en camionetas, con las que contamos a partir de la adquisición de tecnología y herramientas que fuimos adquiriendo en este año. Nada es al azar y todo se diagrama con inteligencia y estrategias, para nosotros el resultado es ampliamente positivo», informó.

Por último, Mongia destacó que «conformar mesas operacionales con la cámara de comercio, supermercadistas, paseos comerciales de Rosario y la región y saber que estuvimos a la altura de las circunstancias, para nosotros es un deber cumplido y un desafío por delante».

<br/>

<span style="font-family: helvetica; font-size: 1.35em; font-weight: 800;"> **Los Operativos IES**</span> 

Estos operativos tienen por objetivo lograr mayor seguridad en la población, incrementando la presencia policial sobre distintos sectores barriales de la ciudad mediante chequeos de personas y vehículos, en forma conjunta y simultánea con la colaboración de recursos y efectivos de la Dirección General de Policía de Acción Táctica (DGPAT), Dirección General de Policía Comunitaria (DGPC), Dirección General de Seguridad Rural (DGSR) y Dirección General de Policía de Seguridad Vial (DGPSV).

<br/>

<span style="font-family: helvetica; font-size: 1.35em; font-weight: 800;"> **IDP**</span> 

Este dispositivo establece patrullas de prevención dentro de un esquema dinámico, que están en condiciones de alcanzar rápidamente puntos ubicados fuera de la planificación diaria. Las nuevas capacidades informáticas permiten generar mapas interactivos, actualizados minuto a minuto, de flujos de circulación de circuitos delictivos.

Esta mecánica, ya incorporada a la planificación de servicios de la guardia rural Los Pumas, permite la identificación de flujos de circulación de delitos, la realización de cortes estadísticos por horario, localidades, grupo etario e incluso género e identidad de los protagonistas.