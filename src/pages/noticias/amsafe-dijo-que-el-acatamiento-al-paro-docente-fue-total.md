---
category: La Ciudad
date: 2021-10-14T06:15:23-03:00
thumbnail: https://assets.3dnoticias.com.ar/amsafecap.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Amsafé dijo que el acatamiento al paro docente fue total
title: Amsafé dijo que el acatamiento al paro docente fue total
entradilla: El delegado de Amsafé La Capital, Rodrigo Alonso, aseguró en el acto frente
  a la Regional IV de Educación que el 100 por ciento de los docentes se plegaron
  a la medida de fuerza

---

Este miércoles se realizó el paro de 24 horas votados por los docentes nucleados en Amsafé y la concentración frente a la sede de la Regional IV de Educación. En el acto que se realizó en ese lugar, las 10, el delegado de la seccional Amsafé La Capital, Rodrigo Alonso, aseguró que el acatamiento a la medida de fuerza fue del 100 por ciento de los docentes.

Los docentes de Amsafé se congregaron frente a la Regional IV de Educación pese al mal tiempo, en la primera jornada de protesta ante la negativa de la aceptación de la propuesta paritaria provincial.

A pesar de la lluvia decenas de maestros se concentraron en la sede de Avellaneda 3381. Allí, los dirigentes gremiales hicieron un repaso por la situación que atraviesan los docentes de la provincia y explicaron cuáles son las demandas que le están haciendo al gobierno de la provincia.

Alonso se refirió a los dichos de la ministra de Educación de la provincia, Adriana Cantero, quien al conocer la semana pasada las medidas de fuerza que votaron los docentes las calificó de "desmedidas". En ese sentido, el delegado de la seccional Amsafé La Capital, expresó: "Desmedido fue que sobre el año 2020 estuvimos seis meses sin convocatoria a paritarias, sin aumento salarial. En diciembre tuvimos un aumento del 25 por ciento con una inflación que fue mucho mayor. Nos empujaron a una presencialidad plena, sin importarles si dábamos clases en el aula o el pasillo, o en zoom; no hubo resoluciones sobre el tema de la creación de cargos".

"Desmedido fue no poner un peso en los edificios escolares", cuestionó y agregó: "Tienen que dejar de amenazar y extorsionar con que nos van a descontar los días del paro, la amenaza y la extorsión profundizan el conflicto".

**La propuesta**

El gobierno provincial les acercó a los maestros una oferta salarial del 17 por ciento en tres tramos. El primero, del 10 por ciento, se abonaría en octubre; el 5 por ciento en diciembre y el 2 por ciento restante en enero de 2022. Si a ese porcentaje se le suma el 35 por ciento otorgado en el primer semestre del año, el incremento salarial total sería del 52 por ciento.