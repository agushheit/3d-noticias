---
category: Agenda Ciudadana
date: 2022-12-14T08:50:51-03:00
thumbnail: https://assets.3dnoticias.com.ar/colectivojpeg.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: "¿Sin colectivos en Navidad?"
title: "¿Sin colectivos en Navidad?"
entradilla: "“El Estado Nacional debe $ 6.000 Millones de Noviembre y $ 5.800 Millones
  de Diciembre” dicen desde FATAP"

---
El lunes 12 Fatap emitió un comunicado de prensa donde advierte que “el Estado Nacional debe $ 6.000 Millones de Noviembre y $ 5.800 Millones de Diciembre”. Asimismo, solicitan se regularice esa deuda para poder hacer frente a los aguinaldos.

**_“Durante las próximas semanas los servicios deberán ser restringidos ante la falta de recursos que permitan afrontar los gastos básicos del sistema, necesarios para la normal continuidad de las prestaciones”._**

En el comunicado expresan además que fueron innumerables los reclamos y presentaciones realizados, para que cumplan con “los compromisos asumidos en la oportunidad de la negociación paritaria del sector, que determina la carencia de los aportes necesarios para hacer frente a las obligaciones con los trabajadores”

En dialogo con LT10, Gerardo Ingaramo presidente de la Federación Argentina de Transportadores por Automotor de Pasajeros dijo que **“siempre pasa lo mismo. Estamos disponiendo de dinero que no tenemos porque el Gobierno Nacional para el interior no libera, pero para Buenos Aires sí”.**

Cuando se le pregunta por el paro para el fin de semana de navidad, Ingaramo dice que **“nosotros estamos manifestando desde la Federación los inconvenientes que se están creando porque no nos liberan los fondos comprometidos para el mes de noviembre y de diciembre que corresponden a la paritaria nacional”. Asegura que habrá paro de UTA si no se cumplen con los aguinaldos en termino.**

Finalmente se mostró preocupado puesto que la implementación para saldar los compromisos necesitan tiempo, ya que el gobierno debe firmar un DNU para ello, y necesita un tiempo para entrar en vigencia.