---
category: Estado Real
date: 2021-05-27T08:59:04-03:00
thumbnail: https://assets.3dnoticias.com.ar/empleo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: 'Inscripción al Repro II: qué rubros abarca y cómo anotarse'
title: 'Inscripción al Repro II: qué rubros abarca y cómo anotarse'
entradilla: El Programa de Recuperación Productiva (Repro II) aumentará los montos
  máximos del beneficio a $ 22 mil para los sectores críticos y de salud

---
La inscripción al Programa de Recuperación Productiva (Repro II) abrió este miércoles y se mantendrá hasta el 1º de junio próximo en la web de la Administración Federal de Ingresos Públicos (AFIP) para los salarios devengados este mes, que contará con un aumento del monto máximo del beneficio de $ 18.000 a $ 22.000 para los sectores críticos y de salud.

El gobierno incorporó el último fin de semana al Repro II como nuevos sectores a las denominadas actividades críticas, lo que permitirá asistir hasta 1,4 millón de trabajadores de comercios de rubros esenciales, no esenciales y centros de compras del área metropolitana.

A la vez, amplió la cobertura y monto para trabajadores independientes monotributistas y autónomos.

El universo potencial de beneficiarios del Repro II bajo la modalidad “crítica” es de 880.000 trabajadores en relación de dependencia, y, con esta ampliación, dicha cifra superará el 1,4 millón.

Además, las empresas pertenecientes a los sectores críticos particularmente afectados por la actual coyuntura contarán con una reducción del 100% de las contribuciones patronales destinadas al Sistema Integrado Previsional Argentino (SIPA), según se desprende de la nueva normativa.

También se dispuso que el requerimiento de información estará limitada a dos indicadores, facturación y liquidez corriente, y que se exime de la obligación de presentar balance.

Además, para evaluar la variación de la facturación, se compara todos los días de mayo de 2019 contra los primeros 24 días de mayo de 2021, lo que implica que el Programa asume que la empresa no tendrá facturación entre el 25 y el 31 de mayo cuando rigen las medidas de estricto aislamiento para contener la expansión de la segunda ola de coronavirus, se explicó.

**Cuáles son los "rubros críticos"**

En cuanto a la ampliación del listado de actividades consideradas “críticas”, el gobierno decidió la incorporación de comercio de rubros no esenciales como indumentaria, textiles, cuero, calzado, electrónica, electrodomésticos, muebles y vehículos, entre otros.

También se suman comercios de ramas esenciales pero que en los últimos meses han estado afectados por la caída de la circulación, como por ejemplo, kioscos, panaderías, heladerías y estaciones de servicio, y también se contemplan en esta instancia centros de compra localizados en el AMBA.

**Trabajadores independientes y monotributistas**

Con respecto a las trabajadoras y los trabajadores independientes monotributistas y autónomos en sectores críticos, se amplía tanto en el monto del beneficio como en la cobertura de la política.

El monto del beneficio alcanza a los $ 22.000 por trabajadora o trabajador independiente y alcanzará a sectores de gastronomía, turismo, transporte, industrias culturales, actividades deportivas y de esparcimiento, comercio de rubros no esenciales y de ramas esenciales pero que en los últimos meses han estado afectados por la caída de la circulación, peluquerías y centros de belleza.

Para acceder al beneficio, las trabajadoras y los trabajadores deberán contar con al menos dos aportes en los últimos seis meses en el régimen de monotributo o autónomo.

También deberá presentar una reducción de la facturación mayor al 20% en términos reales, entre mayo de 2019 y del 1 al 24 de mayo de 2021.

Para el caso de ser empleadora o empleador, deberá registrar un nivel de liquidez corriente inferior a un valor establecido por el Comité de Evaluación y Seguimiento del Repro II.

La inscripción al Repro es totalmente virtual, y se realiza desde el sitio web [https://www.argentina.gob.ar/trabajo/repro2](https://www.argentina.gob.ar/trabajo/repro2 "https://www.argentina.gob.ar/trabajo/repro2")