---
category: Agenda Ciudadana
date: 2021-10-22T06:00:49-03:00
thumbnail: https://assets.3dnoticias.com.ar/conectividad.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Con argumentos encontrados, Diputados aprobó el endeudamiento para la ley
  de Conectividad
title: Con argumentos encontrados, Diputados aprobó el endeudamiento para la ley de
  Conectividad
entradilla: Con 32 votos positivos y 11 abstenciones se autorizó al Ejecutivo provincial
  a tomar deuda para llevar adelante el plan de Inclusión Digital y Transformación
  Educativa Santa Fe Más Conectada.

---
La Cámara de Diputados de la provincia sancionó la ley por la cual se autoriza al Poder Ejecutivo provincial un endeudamiento por hasta 100 millones de dólares para llevar adelante el Programa de Inclusión Digital y Transformación Educativa Santa Fe Más Conectada, más conocida como ley de conectividad.

En una votación que tuvo a 32 diputados a favor y 11 que pidieron abstenerse de votar se terminó aprobando una de las leyes que el gobierno de Omar Perotti viene pidiendo desde sus primeros días de gestión. Entre los votos a favor estuvieron los de los socialistas. Mientras que las 11 abstenciones fueron de los 10 diputados que integran el Bloque Evolución- UCR, que preside Maximiliano Pullaro, y la diputada Amalia Granata.

El diputado Ricardo Olivera (Frente de Todos) dijo que "esto se constituye en una política de Estado que va más allá del gobierno de Perotti y pone en valor a más de 3 millones de santafesinos y santafesinas". Además, aseguró que este proyecto sobre conectividad "busca equidad en una brecha que es cada vez más grande".

"Este proyecto da soluciones a 200 localidades de forma directa y a 365 de forma indirecta. Además enumeró que se crearán 17 jardines de infantes, siete escuelas primarias, 17 secundarias, tres escuelas para alumnos con capacidades diferentes", sostuvo el diputado quien dijo que hay que empezar a hablar de un Estado de Bienestar Digital a partir de lo que pasó en la pandemia.

La Cámara de Diputados aprobó el endeudamiento por 100 millones de dólares para la ley de conectividad.

Clara García (PS) cuestionó los tiempos del Ejecutivo y las faltas de respuestas. Sin embargo, valoró que a fines de junio de este año se retomó el diálogo y los funcionarios provinciales entendieron cuál era la información que se les estaba pidiendo. "Queríamos saber cuál era el proyecto educativo y cuál era el proyecto de virtualidad", dijo García. Por eso el gobierno envió un proyecto de comunicación donde brindó algunas precisiones.

"Quiero creer en la institucionalidad, quiero tener confianza. Tenemos que ser celosos guardianes porque tenemos antecedentes muy malos", finalizó García.

Sin embargo, Pullaro cuestionó el giro del socialismo. "No nos explicamos el cambio tan rotundo de decisión política de algunos bloques", disparó sin hacer menciones directas.

"Seguimos teniendo las mismas dudas que hace un año. En varias oportunidades pedimos diálogo y más allá de la llegada de los funcionarios a la Cámara fue muy difícil tener precisiones que aún hoy no tenemos. Por eso nos abstenemos", argumentó.

"Acá se está votando un endeudamiento y se les deja las manos libres para hacer lo que quieran. No confiamos en lo más mínimo en el gobierno de Omar Perotti porque vienen los funcionarios y cuando cruzan la calle ya se olvidaron de lo que acordaron con la Legislatura", dijo Pullaro para terminar de distanciarse de la postura expresada por García.

Por su parte, Rubén Giustiniani (Igualdad) remarcó la importancia de generar infraestructura en conectividad ya que el 70 por ciento de la provincia no está conectada. "La pandemia lo puso en evidencia", dijo.

Luego el diputado valoró las tasas del crédito que consideró que son ventajosas respecto de otras que tuvo la provincia en préstamos anteriores. Sin embargo, cuestionó otro de los artículos donde advierte que no se debería ceder soberanía. En ese aspecto recordó los problemas que hubo con la Fragata Libertad, con el juez Thomas Griesa. "Estas cláusulas tienen consecuencias muy gravosas, como las que vamos a tener en el juicio del Ciadi por Aguas Santafesinas. Esas consecuencias van a ser millonarias sobre el presupuesto de la provincia", advirtió.

"En realidad, esta no es una ley de conectividad. Lo que hoy estamos votando es un permiso para un endeudamiento. Punto. Todo lo demás no está", aclaró Giustiniani quien dijo que se estaba aprobando una ley que no le gusta pero que el Ejecutivo lo tiene que tomar como un gesto político.

En el marco de un proceso eleccionario las piezas se mueven. Como ya sucedió en la sesión del jueves 7 de octubre, la conformación de la Cámara y del mapa político de la provincia está cambiando. El socialismo, con discursos críticos hacia el gobierno, igual vota junto al PJ. Mientras que los radicales de Evolución se distancian de las posiciones del Frente Progresista. Habrá que esperar a las elecciones del 14 de noviembre para saber si se profundizan estos cambios políticos en la provincia.