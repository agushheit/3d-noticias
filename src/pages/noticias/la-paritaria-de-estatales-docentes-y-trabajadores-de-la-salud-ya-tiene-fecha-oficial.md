---
category: Agenda Ciudadana
date: 2021-01-21T08:44:30Z
thumbnail: https://assets.3dnoticias.com.ar/paritaria.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Diario Uno
resumen: La paritaria de estatales, docentes y trabajadores de la salud ya tiene fecha
  oficial
title: La paritaria de estatales, docentes y trabajadores de la salud ya tiene fecha
  oficial
entradilla: Desde el gobierno de Santa Fe confirmaron que comenzarán a principios
  de febrero. Los primeros serán los gremios de ATE y UPCN

---
Desde el gobierno de la provincia ya adelantaron las fechas en que oficialmente se comenzará a discutir las paritarias para los distintos sectores para este 2021. Fue Juan Manuel Pusineri, ministro de Trabajo de la provincia quien confirmó la noticia a los micrófonos de Radio 2 de Rosario: "Acordamos la convocatoria para el inicio de las paritarias con UPCN y ATE para el 4 de febrero, con los docentes el 5 y los profesionales de la salud para la semana que comienza el 8 de febrero".

Cabe aclarar que los docentes ya habían llegaron a un acuerdo el 29 de diciembre, al aceptar la última oferta realizada antes de que termine el año. Por su parte los gremio de los trabajadores estatales, es decir ATE y UPCN cerraron las pautas salariales atrasados del 2020 con la provincia, la primera semana de enero.

Por último, los profesionales de la salud fueron los últimos en acordar con el gobierno de Perotti el 18 de enero, luego de algunas medidas de fuerza. Siprus sigue reclamando el llamado por parte del gobierno para discutir salarios y mantienen vigente un paro.