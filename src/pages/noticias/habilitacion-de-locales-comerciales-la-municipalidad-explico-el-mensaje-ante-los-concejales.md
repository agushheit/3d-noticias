---
category: Agenda Ciudadana
date: 2022-12-13T06:35:35-03:00
thumbnail: https://assets.3dnoticias.com.ar/habilitacion.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: 'Habilitación de locales comerciales: la Municipalidad explicó el Mensaje
  ante los concejales'
title: 'Habilitación de locales comerciales: la Municipalidad explicó el Mensaje ante
  los concejales'
entradilla: Durante una reunión con los ediles, el secretario de Producción municipal
  dio detalles del Mensaje 22, remitido a ese cuerpo por el Ejecutivo.

---
_La intención es acortar los tiempos de respuesta, concentrar todos los trámites en una sola dependencia y establecer mecanismos de control y seguridad más claros._

Este lunes, funcionarios municipales dieron detalles ante concejales, del Mensaje N°22/22 enviado por el Ejecutivo, sobre la Habilitación de locales comerciales. El encuentro se llevó a cabo en la sala de reuniones Agustín Zapata Gollán del Concejo y estuvo encabezado por el secretario de Producción y Desarrollo Económico municipal, Matías Schmüth.

En la oportunidad, Schmüth destacó el compromiso del intendente Emilio Jatón en modificar de manera positiva esta ordenanza que está a punto de cumplir una década desde su sanción. “Los números que indican cómo se resuelve un trámite de habilitación muestran que un 40% se rechaza y eso no significa que no se abre un negocio sino que se hace sin su correspondiente habilitación. Los problemas que llevan a la no habilitación son cuestiones formales, de planos o de final de obra, que terminan siendo un compromiso para el emprendedor que ni siquiera es el dueño del inmueble”, detalló el secretario.

En esta línea, agregó que “con esta nueva ordenanza se busca cambiar la lógica, es decir, que a la hora de habilitar se tengan en cuenta cuestiones más vinculadas a la seguridad, al riesgo y a las actividades que se desarrollan”. Cabe recordar que la normativa vigente rige desde 2013 y “fue útil para su momento pero luego de 10 años nos dejó muchos inconvenientes que habitualmente vemos y se originan a la hora de hacer un trámite”, expresó.

El funcionario expresó que “a quien quiere emprender, la Municipalidad debe tratarlo como socio estratégico, brindándole la mayor cantidad de respuestas posibles y hoy, la normativa vigente, entorpece ese proceso”. A modo de ejemplo, indicó que “en la mayoría de los casos, el titular del inmueble no es el mismo que realizará la actividad, sino que lo alquila. Desde el Ejecutivo nos parece que debemos separar estas cuestiones y establecer reglas más claras, entre ellas, regular el uso del espacio público”. A fin de cuentas, fijó que la intención es “facilitar las cuestiones formales, que los plazos sean más cortos, potenciar la seguridad e intensificar los controles”.

**Dar respuestas**

Por su parte, el concejal Julio Garibaldi, mencionó que todo apunta a que “el trámite de la habilitación de locales sea más ágil. Toda persona que quiera invertir en Santa Fe, trabajar y generar empleo debe encontrar en el Estado un socio, que le ofrezca trámites de habilitación sencillos, sin perder control y apuntando a la seguridad. La ordenanza va a ese sentido”, mencionó.

Según evaluó el edil, “la normativa actual rige desde 2013 y pone mucho énfasis en el inmueble en sí mismo. A partir de ahora, se busca centralizar todos los trámites en un área municipal, evitando el recorrido oficina por oficina, a los fines de agilizar el trámite”, cerró.