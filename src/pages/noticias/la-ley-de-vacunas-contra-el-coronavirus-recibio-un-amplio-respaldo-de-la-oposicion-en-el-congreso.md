---
category: Agenda Ciudadana
date: 2021-04-11T06:15:23-03:00
thumbnail: https://assets.3dnoticias.com.ar/LEYVACUNACION.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La ley de vacunas contra el coronavirus recibió un amplio respaldo de la
  oposición en el Congreso
title: La ley de vacunas contra el coronavirus recibió un amplio respaldo de la oposición
  en el Congreso
entradilla: La Ley Nacional recibió el apoyo mayoritario de las bancadas que integran
  los interbloques de Juntos por el Cambio en Diputados y fue respaldada por varios
  senadores de esa coalición

---
La Ley Nacional 27.573 sobre vacunas destinadas a generar inmunidad contra la Covid 19, que fue objetada en los últimos días por diferentes referentes de la oposición, recibió el apoyo mayoritario de las bancadas que integran los interbloques de Juntos por el Cambio en Diputados y fue respaldada por varios senadores de esa coalición.

El objeto de la norma era declarar de "interés público la investigación, desarrollo, fabricación y adquisición de las vacunas destinadas a generar inmunidad adquirida contra la COVID-19 en el marco de la emergencia sanitaria", según se sostiene en los considerandos de la norma

La ley, promulgada por el Poder Ejecutivo el 6 de noviembre de 2020 exime del pago de derechos de importación y de todo otro impuesto, gravamen, contribución, tasa o arancel aduanero o portuario, de cualquier naturaleza u origen, incluido el impuesto al valor agregado, a las vacunas y descartables importados por el Ministerio de Salud.

**En el Congreso**

En la votación tras el debate de la iniciativa que se celebró en la Cámara baja durante el 7 de octubre, el proyecto recibió 230 votos positivos, 8 negativos, y 11 abstenciones, en tanto que cuatro legisladores no votaron y hubo la misma cantidad de ausentes.

En el Frente de Todos, la iniciativa se respaldó con 116 votos y apenas se verificaron dos ausencias en la bancada oficialista que encabeza el diputado nacional Máximo Kirchner.

El bloque del PRO se registró 44 voluntades afirmativos, tres votos negativos, cuatro abstenciones y hubo dos ausencias.

En la bancada de la Unión Cívica Radical se verificaron 40 afirmativos, tres negativos, dos abstenciones y sólo un legislador optó por no votar.

Entre los bloques de Acción Federal, Consenso Federal, Córdoba Federal, Frente Cívico y Social de Catamarca, Frente de la Concordia Misionero, Frente Progresista Cívico y Social, Frente de Izquierda y de los Trabajadores; PTS Frente de Izquierda, Juntos Somos Río Negro, Justicialista, Movimiento Popular Neuquino, Partido por la Justicia Social, Producción y Trabajo; Socialista, Unidad y Equidad Federal, Coalición Cívica se registraron 30 afirmativos, dos negativos, dos no votaron y cinco se ausentaron.

En Diputados el proyecto recibió 230 votos positivos, 8 negativos, y 11 abstenciones.

En el Senado, la norma se debatió y aprobó el 29 de octubre de 2020, con 56 votos por la positiva, 12 por la negativa, cuatro ausencias y ninguna abstención.

En el Frente de Todos hubo dos ausencias y 39 votos favorables, mientras que cinco senadores del PRO la aprobaron y otros tres la rechazaron.

El bloque radical hubo siete votos positivos, seis negativos y una ausencia, en tanto que en las bancadas de Avanzar San Luis, Frente Cívico y Social de Catamarca, Juntos Somos Río Negro, Justicialista 8 de octubre, Mediar Argentina, Misiones, Movimiento Neuquino, Producción y Trabajo, Santa Fe Federal se expresaron cinco afirmativos, tres negativos y una ausencia.

**La Ley**

Las provincias, los municipios y cualquier institución privada cuentan con el aval de las leyes argentinas para comprar vacunas contra el coronavirus, en el caso de que lograran acordar con los laboratorios que, hasta hoy, prefieren negociar con los estados nacionales por la escasez de dosis y por las complejidades que surgen a la hora de sellar los contratos.

La adquisición de vacunas ocupó el centro del debate en los últimos días, pese a que el capítulo 6 de la ley 27.563, sancionada por el Congreso y promulgada en noviembre, dejó en claro ya hace meses que "no está prohibido en la Argentina" la compra de dosis por afuera de las negociaciones que lleva adelante el Gobierno desde la llegada del coronavirus al país.

Así fue explicado esta semana por el jefe de Gabinete, Santiago Cafiero, y este sábado por el presidente de la Comisión de Salud de Diputados, Pablo Yedlin, quien aclaró que en el caso de que las provincias decidan llevar adelante una compra de dosis (de vacunas registradas en la Anmat) "tienen los mismos beneficios que la Nación".

La presidenta del PRO, Patricia Bullrich, había señalado esta semana que se les prohíbe la compra de vacunas a provincias, municipios e instituciones, en el marco de nuevas críticas lanzadas por la alianza opositora Juntos por el Cambio (JxC) hacia las medidas sanitarias adoptadas por el Gobierno frente a la segunda ola de contagios de Covid-19.

Sin embargo, el ministro de Salud porteño, Fernán González Bernaldo de Quirós, saldó el debate y aseguró que "no es viable" la compra de vacunas para ser aplicadas hoy mismo porque "las empresas fabricantes ya tienen comprometida su producción anual hace muchos meses".

Este sábado, en declaraciones radiales, el jefe de Gabinete, Santiago Cafiero, ratificó que "nunca hubo un impedimento" para que las distintas jurisdicciones del país, incluida la ciudad de Buenos Aires, compren vacunas contra el coronavirus y apuntó a que parte de la oposición parece buscar "más un efecto político electoral con sus declaraciones".

"La estrategia de compra del gobierno nacional fue llevar adelante la compra de 65 millones de dosis y estamos expectantes de que se cumplan los plazos de los contratos, pero hay dificultades a nivel global y Argentina no es la excepción", indicó Cafiero.