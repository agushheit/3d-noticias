---
category: La Ciudad
date: 2021-06-21T06:00:38-03:00
thumbnail: https://assets.3dnoticias.com.ar/ACTITUDS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Actitud Solidaria pide abrigo para personas en situación de calle
title: Actitud Solidaria pide abrigo para personas en situación de calle
entradilla: Desde hace 11 años, la ONG brinda alimento y contención en distintos puntos
  de la ciudad. La pandemia mermó las colaboraciones y la disponibilidad de gente
  que ayude. Pero las necesidades aumentan.

---
Frazadas, guantes, medias, gorros de lana, bufandas, camperas. Todos esos elementos que usamos cuando llegan los meses de frío son los mismos que precisan con mayor urgencia aún quienes pernoctan en la calle. Por eso, el pedido de Actitud Solidaria es de atención indispensable. Martín Mónaco está al frente de la organización de voluntarios que atienden por día a unas 90 personas en esa situación de vulnerabilidad.

Desde hace 11 años llevan adelante esa tarea, pero, sin dudas, los últimos dos fueron particulares. La pandemia hizo que mermen las colaboraciones económicas, porque las personas que colaboraban se quedaron sin empleo o vieron disminuidos sus ingresos. Y también disminuyó la dotación de gente que todos los días cocina y recorre las calles para llevar una vianda caliente, una infusión, algún postre, abrigo y, por supuesto, un rato de atención y contención para quienes no tienen nada.

Además de la ropa, hacen falta alimentos no perecederos "que nos permitan cocinar a diario a las personas en situación de calle y llevar las viandas que siempre van acompañadas de infusiones y postres; a esto se suma alimento balanceado para quienes tienen la compañía de una mascota, y elementos de aseo personal (jabones, hojas de afeitar, shampoo, etcétera), junto con barbijos y alcohol en gel".

"También es importante que la gente se sume con un aporte económico que nos permite cubrir los gastos de alquiler, impuestos, compra de insumos y algo muy necesario como es el refuerzo nutricional en frutas, verduras, carne", aportó Mónaco, quien aclaró que "no reciben ningún subsidio del Estado para funcionar y dependemos de personas de buen corazón". Es más, señaló que ya hicieron varias gestiones ante la Municipalidad, pero aún no lograron una reunión.

"Dependemos pura y exclusivamente de las personas que quieran brindarse, con sus colaboraciones y con su tiempo; la invitación es para personas de todas las edades", sostuvo Mónaco mientras apuntó que actualmente se atiende a más de 90 personas, con apenas 21 voluntarios.

**DÓNDE LLAMAR**

Para responder al pedido de colaboraciones, alimentos no perecederos, ropa de abrigo tiempo como voluntario, comunicarse a los números: 342-4382479 / 342-5218263.