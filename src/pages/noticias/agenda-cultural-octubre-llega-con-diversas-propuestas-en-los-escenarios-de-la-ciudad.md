---
category: Cultura
date: 2021-10-01T06:00:40-03:00
layout: Noticia con imagen
author: 3D Noticias
resumen: 'Agenda cultural: octubre llega con diversas propuestas en los escenarios
  de la ciudad'
title: 'Agenda cultural: octubre llega con diversas propuestas en los escenarios de
  la ciudad'
entradilla: Los museos, el Teatro Municipal, el Mercado Progreso y el Anfiteatro renuevan
  su programación con opciones para diferentes públicos.
thumbnail: https://assets.3dnoticias.com.ar/CULTURA.jpg
link_del_video: ''

---
La actividad cultural se sigue recuperando con propuestas de calidad por parte de la Municipalidad y el apoyo que brinda a iniciativas independientes y privadas que se desarrollan en los distintos escenarios de la ciudad. Desde el jueves habrá numerosas propuestas de música en vivo, danza, teatro, intervenciones artísticas, muestras y ferias para disfrutar en el primer fin de semana de octubre.

El mes comienza con una celebración especial ya que el próximo martes 5 de octubre, el Teatro Municipal cumple 116 años. La Banda Sinfónica y el Coro municipales brindarán un concierto gratuito ese mismo día, desde las 20 horas, en la Sala Mayor. El repertorio se compone de los primeros y segundos premios del Concurso Nacional de Composición y Arreglos “Ariel Ramírez”, que se realizó como parte de las acciones que rinden este año homenaje al pianista y compositor, al cumplirse 100 años de su nacimiento. Se podrán disfrutar las composiciones originales y arreglos, que a través del certamen pasan a nutrir el repertorio de la Banda y el Coro, dirigidos por Omar Lacuadra y Víctor Malvicino, y Juan Sebastián Barbero, respectivamente. Las entradas se retiran previamente en la boletería, ubicada en San Martín 2020: de lunes a sábado, de 9 a 13 y de 17 a 21; y los domingos, de 17 a 21 horas.

**Bienalsur y Sala Cero**

Continúan abiertas al público las muestras de la Bienal Internacional de Arte Contemporáneo de América del Sur (Bienalsur), que organiza la Universidad Nacional de Tres de Febrero (UNTREF) y llegó por primera vez a Santa Fe. En el Museo Municipal de Artes Visuales “Sor Josefa Díaz y Clucellas” (San Martín 2068) pueden recorrerse hasta el 31 de octubre “Cómo llegan las flores a la tela” y “Manos randeras tejiendo recuerdos”, de miércoles a sábados, de 9.30 a 12.30 y 16 a 19, y los domingos de 16 a 19 horas. En el Centro Experimental del Color (Estación Belgrano, Bv. Gálvez 1150) se encuentra “Paraje Vecino”: de martes a viernes, de 9.30 a 12.30, y los sábados y domingos, de 17 a 20 horas, hasta el 14 de noviembre. La entrada es libre y gratuita.

El viernes 1 de octubre desde las 19 horas habrá música en vivo con Ana Suñé, Andrea Eletti y Juan Candioti, en la Sala Cero del Museo de la Constitución (Avda. Circunvalación y 1º de Mayo). Será en el marco de la intervención artística “Superficies de inscripción”, de Rosa García y Victoria Tolissa, seleccionada a partir de una convocatoria abierta que realizó la Municipalidad.

**Un viaje sonoro por el continente**

También el viernes, a las 19 horas, el ciclo de música Sala 5/360 presenta al Cuarteto de Cellos que integran Julia Avveduto, Enrique Catena, Pilar Ferrando y Sofía Ferrero. Con esta formación invitan a explorar la amplia sonoridad del violoncello, a través de un repertorio de música popular del siglo XX, que une todo nuestro continente. Desde Santa Fe a Estados Unidos, las canciones y obras que presentarán evocan historias de escritoras, trabajadores, momentos cinematográficos, combinando la música académica con rítmicas del folclore latinoamericano. Las entradas para este concierto que se desarrollará en la Sala 5 de la Estación Belgrano (Bv. Gálvez 1150, planta alta), son gratuitas y se entregarán en puerta a partir de las 17, hasta completar la capacidad de la sala.

**Slam de poesía y música en vivo**

El Slam de Poesía Oral de Santa Fe cumplió siete años. Para celebrarlo, sus organizadores convocan este jueves 30 de septiembre desde las 19.30, en el patio del Mercado Progreso, con entrada gratuita. En paralelo con las lecturas de poesía en el escenario, habrá una feria de editoriales y propuestas artísticas, gastronomía y bebidas. Las inscripciones para participar con lecturas se realizan en @slam.santafe

Desde el viernes 1 hasta el domingo 3 de octubre, la Municipalidad invita a disfrutar de diversas propuestas musicales en el renovado escenario ubicado en Balcarce 1635, todas con entrada gratuita hasta completar el aforo habilitado por protocolo. El viernes, desde las 19 horas, se presentará la cantautora Gisela Trento, acompañada en bajo por Federico Weder. La banda Cul de Sac? desembarca también esa noche con su habitual potencia rockera, plasmada en más de 20 años de trayectoria, con Hernán Bruno en voz, Lucas Prochietto en bajo y coros, Lucas Negretti en batería y coros, Héctor “Tío” Grazioli en guitarra y coros, y Marce Burgui en teclado y voz.

La noche del sábado será a puro rock, también desde las 19, con Mambonegro y Guazú. La primera banda reúne a músicos experimentados, en un contundente power trío con Alejandro Collados en batería y voz, ex integrante de Cabezones; Leonardo Moscovich en guitarra y voz, y Martín Zaragozi, en bajo y voz, dos ex La Cruda. El resultado de esta conjunción son letras concisas, un mensaje claro y bases con fuertes marcas de rock pesado clásico, metal y tintes de stoner rock. También con un amplio recorrido en la escena local y regional, llegará el turno de Guazú, la banda de rock alternativo, con vestigios de industrial, post rock y noise que conforman Carlos Di Nápoli en bajo y voz, Ignacio Farías Sunier en batería y Sebastián Malizia en guitarra.

En la tarde del domingo se podrá recorrer desde las 17 la feria de emprendimientos Capital Activa, y disfrutar de los shows en vivo de Charly Avveduto Cuarteto y Maloca Trío. La primera formación interpretará un repertorio de standars de jazz y bossa nova, a cargo de reconocidos músicos a nivel local y nacional como Charly Avveduto en saxos, Francisco Lo Vuolo en piano, Mariano Ferrando en bajo y Hugo García en batería. Después llegará el turno de la música de Brasil, con el trío que integran Gonzalo Díaz en guitarra, voz y arreglos; Ivana Papini, en clarinete y voz; y Pablo Ferreyra, en pandeiro y voz. En un camino que se inició en 2016 con el choro brasileño a través de exponentes como Pixinguinha y Jacob do Bandolim, el repertorio actual contiene el trabajo para desarrollar composiciones propias y abraza obras de Hermeto Pascoal, Severino Araújo, João Bosco, y temas clásicos de Chico Buarque.

**Sala Mayor**

Martín Bossi llega al Teatro Municipal con “Comedy Tour”, el nuevo espectáculo teatral humorístico musical con el que recorrerá las principales ciudades y provincias de la Argentina. Se presentará el viernes 1 a las 20 horas y el sábado 2 de octubre a esa misma hora, con una segunda función a las 22. Con libro y dirección de Emilio Tamer, Bossi interpreta durante una hora y media monólogos de extrema actualidad, música y canciones. Las plateas altas y bajas sin numerar tienen un valor de $2000; los palcos bajos con ubicación, $1800; los palcos altos con ubicación, $1500. Las entradas se venden en la boletería del Teatro y por plateavip.com.ar

La compañía de danzas que creó y dirige Cecilia Romero Kucharuk presenta “HAM en escena: ¿Existe la distancia que nos une?”. Será el domingo a partir de las 20 horas, con un elenco integrado por Azul Lazzaroni, Emanuel Virgilio, David Leonhardt, Rosina Heldner, Esmeralda Dotti y Micaela Bertossi. La coreografía es una creación de Facundo Ebbeneger junto a Romero Kucharuk, con la asistencia de dirección de Mariel Barcos. Las entradas generales cuestan $500.

**Viernes a domingo en Marechal**

El ciclo Bardero finaliza el viernes a las 21 horas, en la sala experimental del Teatro, con el unipersonal “Extraño tu perfume (Manifiesto raro del Teatro del Bardo)”. La dramaturgia de Sebastián Boscarol y Valeria Folini –directora de la puesta- “está inspirada en textos no dramáticos de la literatura queer – trans contemporánea y en la biografía del actor, ficcionalizando ambas fuentes a través de la hibridación de los lenguajes verbales y no verbales, en una fragmentación, montaje y desmontaje que emula el transcurrir de la vida de una persona, intentando desandar en escena, un concepto socialmente habilitado: la identidad de género”. Las entradas generales tienen un valor de $500, mientras que si se adquieren dos tickets, se abonan $400 por cada uno.

Con puesta en escena y dirección general de Gustavo Lauto, el sábado 2 y el 9 de octubre a las 21 horas se presenta “El cumple de Corita”. Una serie de situaciones disparatadas se disparan en esta historia para público adulto que se desarrolla en la mañana del cumpleaños número 50 de la protagonista, una maestra jardinera a punto de jubilarse que recibe un regalo de parte de su mejor amiga. El elenco está integrado por Alejandra Digliodo, Juan Manuel Preti, Mariela Cerruto, Marta Defeis y Claudio Casco. Las entradas generales cuestan $500.

Musicar Dúo invita al concierto de cámara “De Argentina y España. Historias y canciones de cámara”, el domingo 3, desde las 20.30 horas. Jonathan Guete y Juan Cruz Prandi en guitarra, junto a la soprano Camila María Beltramone, proponen una selección de composiciones para voz y guitarra, y dúo de guitarras, de autores argentinos y españoles. Las melodías y poesías de cada una de las obras invitan a transitar por los discursos y paisajes musicales propios del nacionalismo español y argentino, y del tango de Piazolla. Durante el concierto, los intérpretes recorrerán los aspectos más significativos de las obras y su vinculación con las vivencias del compositor, invitando al público a vincularse con los diferentes aspectos que dieron lugar a su creación. Las entradas generales están a la venta a un valor de $500.

**Cumbia para celebrar la primavera**

También el domingo, pero desde las 18, Ezequiel “El Brujo”, Los Bam Band Orquesta, Pastor de los Santos y Jazmín Catalano invitan a “Primavera en el Anfi”, un encuentro para celebrar la nueva estación en el emblemático escenario del Parque del Sur. Las entradas anticipadas están en venta en la boletería del Teatro Municipal a un valor de $700; mientras que el día de la función costarán $900.