---
category: Agenda Ciudadana
date: 2021-04-23T07:49:09-03:00
thumbnail: https://assets.3dnoticias.com.ar/vacunaadultos.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Sauce Viejo: Abrió la inscripción para vacunarse contra la gripe'
title: 'Sauce Viejo: Abrió la inscripción para vacunarse contra la gripe'
entradilla: La campaña de vacunación está destinada a grupos de riesgo, entre los
  que se encuentran mayores de 65 años, mujeres embarazadas y niños y niñas de 6 a
  24 meses.

---
La Comuna de Sauce Viejo informó que puso en marcha la inscripción de personas para aplicarse la vacuna contra la gripe. Desde el gobierno sauceño recordaron que la campaña está destinada a grupos de riesgo definidos por el Ministerio de Salud de la Nación.

En ese sentido, las dosis prioritarias son para personas mayores de 65 años, menores de esa edad con factores de riesgo, niños y niñas de entre 6 y 24 meses y mujeres embarazadas.

Si tenés más de 65 años o tenés factores de riesgo podes inscribirte haciendo click en el siguiente enlace:[ bit.ly/SauceVac.](https://docs.google.com/forms/d/e/1FAIpQLSfWURByUOKBVYzjP02GOdEgO8AnGBoftQjtj9nVZU79THQWfA/viewform)

Además, la comuna sauceña puso a disposición las líneas telefónicas 4950212 y 4950548 para quienes necesiten ayuda para registrar su inscripción.