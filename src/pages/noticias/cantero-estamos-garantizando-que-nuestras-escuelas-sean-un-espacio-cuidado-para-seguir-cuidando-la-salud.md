---
category: Estado Real
date: 2021-01-29T10:08:28Z
thumbnail: https://assets.3dnoticias.com.ar/cantero.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: 'Cantero: “Estamos garantizando que nuestras escuelas sean un espacio cuidado”,
  para “seguir cuidando la salud”'
title: 'Cantero: “Estamos garantizando que nuestras escuelas sean un espacio cuidado”,
  para “seguir cuidando la salud”'
entradilla: La ministra de Educación brindó detalles sobre el retorno de las clases
  presenciales; para lo cuál el gobierno provincial invirtió 5.278 millones de pesos.

---
La ministra de Educación de la provincia, Adriana Cantero, junto a sus pares de Salud, Sonia Martorano, y de Trabajo, Empleo y Seguridad Social, Juan Manuel Pusineri, brindó detalles del trabajo realizado interministerialmente para garantizar “que nuestras escuelas sean un espacio cuidado”, para “seguir cuidando la salud”, destacó.

Desde mitad de 2020, “venimos planificando condiciones de trabajo para acordar aspectos sanitarios fundamentales entre los ministerios de Salud y Educación; y diseñar cómo sería esta escuela, que ya empezamos a transitar a fines del año pasado, y que este año esperamos que involucre a mucha más población escolar”, indicó.

A continuación, Cantero precisó que “estamos pensando en escuelas de grupos pequeños, con distanciamiento social, con implementación del protocolo federal; y además, con alternancia, es decir, algunos días de clases en la escuela y otros de trabajo en casa”.

La ministra destacó que “cuando convocamos a restablecer un vínculo presencial en las escuelas, lo hacemos con un gran trabajo previo de organización y asistencia; y con la tranquilidad de que nuestras escuelas son un espacio cuidado; y que vamos a seguir cuidando la salud”.

En este sentido, reiteró que “el calendario escolar se mantiene cómo está, y cómo fue aprobado, por eso sostenemos la fecha de inicio, 15 de marzo. Lo que si vamos a ir viendo a medida que transitemos el año escolar, una escuela flexible, porque los dispositivos se abrirán o cerrarán en función de las condiciones que impone la pandemia”.

En tanto, Cantero recordó que “el 17 de febrero las escuelas tienen que convocar a los 7mo grados, 5to de escuela Secundaria y 6to de técnica, son los grupos prioritarios que terminan nivel, y con los que tenemos que cerrar el ciclo 2020, con una mirada focalizada en las competencias fundamentales que hay que asegurar para pasar a los niveles superiores”.

**INVERSIÓN Y PLANIFICACIÓN**

En la oportunidad, Cantero destacó que “hemos realizado un trabajo de inversiones y planificaciones” en las escuelas. “En primer lugar, tuvimos que reformular el sistema alimentario en módulos, y pasamos de dar 175 mil raciones a atender 130 mil familias. La cifra de inversión en comedores triplicó la inversión de 2019, en ese año se gastaron 1.300 millones de pesos y más de 3700 (millones) en 2020”, completó.

En cuanto a la infraestructura y equipamiento, la titular de la cartera educativa manifestó que se realizó un diagnóstico de las escuelas, “cómo las recibimos, para mirar en que estado estaban y poder abordar la inversión, para adecuar los espacio a la pandemia”. Por ejemplo, “adecuar lavamanos, modificar el diseño del espacio escolar; y también, las remodelaciones priorizadas para la escuela pospandémica”.

“En esta inversión, entre fondos nacionales y provinciales, tenemos casi 1000 millones de pesos en 2020. En la inversión provincial, solamente, hay una duplicación de los fondos destinados a reparaciones: 314 millones se invirtieron en 2019, y 678 en 2020 comparando partida por partida”; y resaltó que para la “adecuación Covid fueron asistidos todos los edificios escolares; se relevaron 3700 edificios y todos fueron asistidos con subsidios para lavamanos, y la compra de termómetros infrarojos y de insumos”.

Asimismo, Cantero puntualizó que también “tuvimos un incremento de personal, no soló en cargos y horas cátedra, sino también en la contratación del personal que hizo el relevamiento de los edificios, y en una ayuda especial para aspirantes a reemplazantes abonada en noviembre y diciembre; una provincia del país que tuvo esta ayuda. La cifra supera los 900 millones de pesos”.

Además, “tuvimos que invertir en recursos pedagógicos, en la plataforma, en el campus virtual, en los cuadernos pedagógicos, porque recibimos una provincia con una conectividad desigual, por lo tanto el trabajo con la virtualidad fue altamente complicado; y también en la compra de teléfonos celulares para asistir a los adolescentes de las zonas rurales”.

“Con Verano Activo, programa que se está desplegando en todo el territorio provincial, tuvimos la iniciativa de ir a buscar, en el marco del Plan Acompañar, a los alumnos que fragilizaron la trayectoria escolar en 2020. Al día de la fecha hemos visitado, casa por casa, a más de 22 mil chicos, para trabajar en la reinserción de su vínculo”.

**VACUNACIÓN**

Sobre la vacunación de docentes, Cantero precisó que “lo estamos analizando en el Consejo Federal, hay distintas propuestas; lo que si ya estamos con el plan de cómo ubicar en todo el territorio provincial los espacios para vacunar, los dos ministerios, para esa campaña masiva”.

En tanto, la ministra de Salud, Sonia Martorano, precisó que “el plan de logística de vacunación es Nacional y Federal, nosotros vamos siguiendo los lineamientos del Ministerio de Salud de la Nación”; y destacó que “la Argentina a nivel global está bien posicionada porque ya estamos vacunando, pero hay que tener en cuenta que todo el planeta está solicitando ese insumo que es la vacuna”.

“En este contexto, se van haciendo grupos priorizados. A nivel nacional, la priorización se dio en los grupos de salud que han estado más expuestos; es un grupo que son 70 mil, y recién tenemos 24 mil, y hasta no completar este grupo no podemos seguir avanzando”.

“Vamos a ir priorizando en base al calendario nacional; seguridad, docentes, son grupos que están priorizados; pero este calendario se va a ir adecuando a la cantidad y al momento de llegada de vacunas”, precisó Martorano; y destacó que “estamos hablando de un megaoperativo de vacunación, nunca visto, para vacunar mucha gente, en simultáneo y en poco tiempo”.