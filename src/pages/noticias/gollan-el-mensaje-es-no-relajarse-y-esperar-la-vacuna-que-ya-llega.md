---
category: Agenda Ciudadana
date: 2021-01-16T04:11:15Z
thumbnail: https://assets.3dnoticias.com.ar/covid-amba.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: '3DNoticias - Fuente: Télam'
resumen: 'Gollán: «El mensaje es no relajarse y esperar la vacuna, que ya llega»'
title: 'Gollán: «El mensaje es no relajarse y esperar la vacuna, que ya llega»'
entradilla: El ministro de Salud bonaerense expuso también que la cantidad de casos
  «se triplicó en el AMBA y se duplicó en el interior» en tres semanas, pero destacó
  que en los últimos días se aquietó.

---
El ministro de Salud bonaerense, Daniel Gollán, pidió cuidarse entre todos para cuidar también la temporada turística en el marco de la pandemia de coronavirus, e insistió en que el mensaje es no relajarse y esperar la vacuna, que ya llega.

«Es una temporada atípica, pero pudimos tener temporada», dijo el funcionario este jueves a la mañana en declaraciones a radio Con Vos.

En la entrevista, el ministro bonaerense contó que recorrió decenas de playas y remarcó que están muy ordenadas, las personas usan barbijo y están en burbujas.

Consultado sobre la disponibilidad de camas en los municipios turísticos, contó que en Pinamar están sin pacientes Covid y añadió que en Mar del Plata «están bien en el sector público».

Explicó que desde el Gobierno se analizan diferentes indicadores, como las llamadas a la línea 148, que «había empezado a picar hacia arriba y en los últimos cuatro días hubo no un descenso pero sí un amesetamiento».

«De los otros indicadores, también tenemos buenas noticias: las internaciones crecieron, pero las internaciones son mucho más cortas, de 6 o 7 días y los pacientes salen bien de terapia», indicó.

Gollán explicó que es probable que ello se deba a que las cargas virales son menores, añadió que la aplicación precoz del plasma da excelentes resultados y se mostró optimista de cara al futuro inmediato.

«Los casos graves se están aquietando. En AMBA subieron los pacientes de terapia de 437 a 610 y ahora se está aquietando en 590», apuntó.

El ministro expuso también que la cantidad de casos se triplicó en el AMBA y se duplicó en el interior en tres semanas, pero destacó que en los últimos días se aquietó.

«Hacia fin de año, todos nos relajamos por el cansancio, por el fin de año, por la vacuna y las Fiestas», dijo y analizó que «sirvieron mucho estas medidas de difusión y la gente misma hizo su propio aprendizaje al ver que empezaron a crecer los casos».

Asimismo, expresó que se advierte un mecanismo de autorregulación de la gente y añadió que «la mayoría de la gente se cuidó todo este año y ese capital de concientización sanitaria está».

«La vacuna ya comenzó. Qué picardía que se muera un ser querido a un mes o dos de poder vacunarnos. Las vacunas están dando excelentes resultados no solo en términos de no enfermarse, sino para transformar esta enfermedad en un caso leve», advirtió.

El ministro dijo que la vacunación nos cambia la dinámica y señaló que «con cuidados, la realidad en 6 o 7 meses empezará a ser diferente respecto del año pasado en lo cultural, educativo y productivo».

Por último, vaticinó que «si no hay ningún martes 13, hacia fin de año tendremos otra realidad, con menos cuidados».