---
layout: Noticia con video
author: 'Fuente: LT10'
resumen: Semana del Helado Artesanal
category: La Ciudad
title: 'Semana del Helado Artesanal: ¿cuáles son las heladerías con promociones?'
entradilla: En la previa de la Noche de las Heladerías, desde el próximo lunes los
  negocios ofrecerán promociones para disfrutar de un auténtico y delicioso helado
  artesanal. Mirá en el mapa dónde encontrarlas.
date: 2020-11-21T12:35:03.219+00:00
thumbnail: https://assets.3dnoticias.com.ar/2020-10-07.jpg
url: https://youtu.be/bWvZ-XTYGJU
link_del_video: ''

---
Helado Artesanal, por lo que las heladerías ofrecerán una promo para disfrutar: con la compra de un kilo o más te llevas 1/4 gratis.

Esta oferta es válida para compra en el local o por teléfono.

[Consultá en el mapa las heladerías que tienen esta promoción](https://www.google.com/maps/d/u/0/viewer?mid=1cOpoT0YXbM0CpDZ5AI_yKI3z-knOSioB&ll=-31.623945046452437%2C-60.70062284999999&z=13)

Además, la fábrica de pastas La Cosa Nostra también ofrece sus helados artesanales. Encontralos en Avda. Freyre 1952 o en la sucursal de Avda. Aristóbulo del Valle 5357.

## **Noche de las Heladerías**

Este año, La Noche de las Heladerías adaptará su formato y se celebrará virtualmente el sábado 28 de noviembre a partir de las 21 horas. La misma formará parte de La Semana del Auténtico Helado Artesanal.

Entre estos días, con la compra de un kilo en las heladerías adheridas, se otorgará una entrada gratis (hasta agotar stock) para el Festival del Helado con un exclusivo show de Coti vía streaming, programado para el sábado 28 de noviembre a las 21 horas, que se desarrollará en vivo desde el teatro Vorterix y con la conducción de Diego Ripoll. Además, el espectáculo contará con la presencia de los maestros heladeros de todo país, sorpresas y sorteos.