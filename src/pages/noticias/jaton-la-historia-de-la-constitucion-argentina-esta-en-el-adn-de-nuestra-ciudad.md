---
category: La Ciudad
date: 2021-05-02T06:00:57-03:00
thumbnail: https://assets.3dnoticias.com.ar/Constitucion.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Jatón: “La historia de la Constitución argentina está en el ADN de nuestra
  ciudad”'
title: 'Jatón: “La historia de la Constitución argentina está en el ADN de nuestra
  ciudad”'
entradilla: El intendente encabezó el acto por el Día de la Constitución Nacional.En
  su discurso, aseguró que “forjar un país con oportunidades para todos fue el sueño
  de más de un constituyente”.

---
Este sábado, en el predio del Museo de la Constitución, se realizó la ceremonia por el 168° aniversario de la Carta Magna. De la actividad, encabezada por el intendente Emilio Jatón, participaron el gobernador, Omar Perotti; la vicegobernadora, Alejandra Rodenas; el presidente provisional de la Cámara de Senadores, Rubén Pirola; la vicepresidenta Primera de la Cámara de Diputados, Lucila Deponti; el presidente del Concejo Municipal de la ciudad, Leandro González; y el presidente de la Corte Suprema de Justicia de la provincia, Rubén Falistocco. Por videoconferencia, estuvo el ministro de Educación de la Nación, Nicolás Trotta. De este modo, y por segundo año consecutivo, la capital de la provincia fue sede de los actos conmemorativos por el Día de la Constitución Nacional.

Durante su discurso, Jatón recordó que el motivo de la actividad es “rendir homenaje a nuestra Constitución Nacional para celebrar su día como piedra fundamental del Estado y la sociedad”. Pero también apuntó, por el Día del Trabajador y de la Trabajadora, la necesidad de “reconocer, destacar y agradecer el esfuerzo, la dedicación y el compromiso, sobre todo en estos tiempos que atravesamos, de todos los hombres y mujeres que luchan, que emprenden, que trabajan, que crean y que se la juegan por un futuro mejor”.

En ese marco, el intendente reconoció que “en la velocidad de nuestra vida cotidiana y de los cambios acelerados que nos toca atender y comprender, perdemos de vista que Santa Fe le rinde homenaje constante a los hombres de la Convención Constituyente”. Y por ello, señaló la importancia de “estar orgullosos de nuestra contribución a la República y mantenerla viva. La historia de la Constitución argentina está en el ADN de nuestra ciudad. Lo vemos y lo recordamos en nuestras calles, en nuestras plazas, en nuestros parques y edificios”, dijo.

Luego de ratificar los derechos consagrados en la Carta Magna, Jatón reflexionó que “estamos en un tiempo complejo, que nos interpela constantemente. No solo el COVID nos atraviesa, tenemos muchas deudas que aún no podemos resolver: sociales y urbanas que, como sociedad, nos fragmentan en nuestras posibilidades de crecer y de tener un futuro digno”. Sin embargo, aseguró que desde el Ejecutivo local “estamos trabajando fuertemente para superar esas barreras, desde lo más básico como el acceso al agua, a los servicios y al hábitat digno”.

“Estamos abriendo calles donde sólo había tierra, llevando luz a barrios que fueron sistemáticamente olvidados y garantizando la limpieza de cada punto de la ciudad con el compromiso de todos los vecinos”, dijo. A esto añadió que “trabajamos para que la cultura llegue a cada barrio y para hacer de Santa Fe una ciudad integrada en la que la convivencia sea el motor de desarrollo”.

Según dijo, “forjar un país con oportunidades para todos, seguramente fue el sueño de más de un constituyente”. Y concluyó con el deseo de que “en días como los de hoy, donde homenajeamos a nuestra Constitución, recordemos sus valores y los pongamos en acción”.

**Participantes**

Posteriormente, Perotti agradeció a las autoridades que se hicieron presentes y recordó la promesa del ministro de Educación de la Nación, de incorporar a la ciudad de Santa Fe como parte del recorrido de las escuelas, para visitar el Museo de la Constitución. Según dijo, “eso es parte de algo que tendremos que seguir trabajando junto al intendente, como una manera de generar acciones que fortalezcan lo que ha sido la historia de Santa Fe y su indiscutido protagonismo en la organización nacional, cuna de la Constitución”.

En ese sentido, agregó que “nos cabe a los santafesinos, tener la inteligencia y la capacidad de volver a vincular el lugar con los nuevos argentinos, pero fundamentalmente, ese gran acuerdo que permitió la organización nacional y sembrar en esa educación, que los saberes incorporen la necesidad del diálogo y los consensos”.

Para concluir, el gobernador estableció ante el intendente Jatón “el compromiso de seguir trabajando juntos para esta Santa Fe cuna de la Constitución que como ciudad y como provincia pueda transmitir ese sentimiento a todo el país”.

Desde el Palacio Sarmiento, en la ciudad de Buenos Aires, el ministro Trotta aprovechó la oportunidad para “saludar a todos los trabajadores, en este momento tan complejo que estamos transitando, principalmente a los trabajadores de la sanidad y a nuestros maestros y profesores”.

Del mismo modo manifestó su “alegría por poder acompañarlos a la distancia por segundo año consecutivo y recordó su visita al Museo de la Constitución” como así también su promesa de que el mismo “sea un espacio de referencia en el campo educativo para todo el país. Que todas nuestras escuelas tengan una visita obligada por ese espacio de reflexión, sumamente importante y necesario que va a reafirmar la historia”, aseguró,

Y se despidió bregando por que el año próximo, “nos podamos encontrar de manera presencial en la hermosa ciudad de Santa Fe y reflexionar juntos, en el acto, frente a un escenario repleto de niños y niñas con sus guardapolvos blancos, para que recorran ese Museo y reafirmemos la trascendencia de la Constitución argentina como la herramienta de consenso y de futuro para nuestro país”.

También asistieron el secretario General de la Municipalidad, Mariano Granato; el secretario de Gobierno municipal, Nicolás Aimar; y el secretario de Educación y Cultura, Paulo Ricci; junto con el ministro de Gestión Pública de la provincia, Marcos Corach; el ministro de Gobierno, Justicia, Derechos Humanos y Diversidad de la provincia, Roberto Sukerman; el secretario de Gobierno de la provincia, Oscar Urruty; y los integrantes de la Asociación parque Biblioteca de la Constitución Nacional, Adriana Molina, Gustavo Vittori y Mariela Uberti; entre otras autoridades.