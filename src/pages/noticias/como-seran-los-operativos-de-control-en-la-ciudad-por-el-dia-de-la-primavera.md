---
category: La Ciudad
date: 2021-09-21T06:00:47-03:00
thumbnail: https://assets.3dnoticias.com.ar/PRIMAVERA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Cómo serán los operativos de control en la ciudad por el día de la primavera
title: Cómo serán los operativos de control en la ciudad por el día de la primavera
entradilla: Montarán un dispositivo conjunto entre la municipalidad y la provincia.
  Habrá controles en la vía pública y en locales gastronómicos. Cómo pretenden evitar
  las “clandes”.

---
Se viene el Día de la Primavera y del Estudiante, este martes, y en las horas previas dieron a conocer cómo serán los operativos de control para evitar desmanes y contagios de coronavirus, en medio de la pandemia.

Los anuncios se realizaron este lunes en una conferencia de prensa realizada en el hall del Ministerio de Seguridad, y estuvieron a cargo de Facundo Bustos, coordinador del Ministerio de Seguridad; Martín García, jefe de la URI; y Fernando Peverengo, secretario de Control de la Municipalidad.

Los operativos comenzarán este lunes a la medianoche y continuarán el martes desde el mediodía, en los puntos críticos donde suele concentrarse mayor cantidad de personas: Costanera Este y Oeste, Bulevar (polo gastronómico), y los Parques del Sur y Garay.

“Organizamos una serie de operativos conjuntamente con la Municipalidad para tener el control sobre los espacios públicos a donde los estudiantes históricamente se han volcado a festejar la llegada de la Primavera”, comenzó diciendo Bustos. Y agregó luego que dichos controles serán en la ciudad de Santa Fe como también en el área metropolitana (las demás ciudades de la zona).

Las disposiciones sanitarias provinciales vigentes indican que están permitidas las reuniones sociales en espacios públicos de hasta 20 participantes por burbujas, con el debido distanciamiento social y los protocolos de higiene como el uso del tapabocas, los dos metros de distancia y el alcohol en gel. “Esa disposición se va a respetar”, dijo Bustos. “Desde la policía haremos un fuerte trabajo preventivo sobre la concentración de personas y acompañaremos a la Municipalidad en sus operativos de control”, agregó luego el funcionario.

“Desde la coordinación entre estos organismos prestaremos buena predisposición para que se pueda festejar el día de la Primavera en paz, sabiendo que el año pasado no se pudo hacer, así que esperamos tener un día en paz, con la prevención y el control necesarios”, finalizó Bustos.

**Controles**

Por su parte, desde el Municipio el secretario de Control enfatizó en que “los controles se realizarán de manera preventiva, de forma conjunta y acompañando los festejos”, dijo Peverengo. “Los inspectores controlarán los ruidos molestos, la venta de bebidas alcohólicas, el tránsito y los horarios de funcionamiento de los comercios gastronómicos”.

“Queremos que los jóvenes puedan disfrutar de este día, ya que el año pasado no lo pudieron hacer, con un mensaje reflexivo, para que al día siguiente se hable de que fue una fiesta y nada más”, dijo Peverengo.

\-¿Cómo piensan evitar las fiestas clandestinas?.

\-De forma constante seguimos las redes sociales y estamos atentos a la difusión de cualquier tipo de convocatoria en ese sentido. Apelamos a la responsabilidad, nuestro intendente, Emilio Jatón, estuvo reunido con gastronómicos y trabajadores de la nocturnidad en tal sentido, para el día de mañana reestablecer esas actividades. Vamos a seguir haciendo estos operativos de forma coordinada con la policía, pero obviamente la sorpresa no es grata cuando estas actividades se siguen realizando de manera clandestina -dijo el secretario de Control municipal.

**Seguridad**

Por último, el jefe de la UR1 de Policía habló de “optimizar los recursos”, ya que “tenemos que garantizar la seguridad no sólo en la ciudad de Santa Fe sino también en toda el área metropolitana, sobre todo en la Costa, El Chaquito, Santo Tomé y Recreo”, dijo García. “Nuestro trabajo será preventivo y disuasivo, tratando de hacer respetar las burbujas”, finalizó la autoridad policial.