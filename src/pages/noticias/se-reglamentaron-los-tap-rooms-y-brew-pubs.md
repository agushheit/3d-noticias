---
layout: Noticia con imagen
author: .
resumen: Cerveza Artesanal Santafesina
category: Estado Real
title: Se reglamentaron los Tap Rooms y Brew Pubs
entradilla: "El Concejo Municipal aprobó un proyecto relativo a las modalidades
  de producción y fabricación de cerveza artesanal. "
date: 2020-11-13T17:50:13.026Z
thumbnail: https://assets.3dnoticias.com.ar/noche.jpg
---
El Concejo aprobó una iniciativa de la concejala Valeria López Delzar (Frente Progresista Cívico y Social – Creo) que redefine las dos modalidades de producción y fabricación de cerveza artesanal (Tap Rooms y Brew Pubs). La iniciativa trabajada en conjunto con la cámara de cerveceros modifica el Reglamento de Ordenamiento Urbano, Ordenanza Nº 11.748 y su modificatoria Nº 12.439, incorporando a los Tap Rooms (fábrica-bar) y los Brew Pubs (bar-fábrica).

A partir de esta Ordenanza se entiende como Tap Room o Fábrica-Bar al espacio donde la actividad principal es la fabricación de cerveza artesanal, coexistiendo con una sala de degustación, un espacio para capacitaciones, ventas de productos en promoción, y recorridos o visitas a estos lugares. Asimismo, se define al Brew Pub o Bar-Fábrica al espacio donde la gastronomía coexiste con el emplazamiento de una fábrica de cerveza artesanal. “Este tipo de normativa contribuye a la reactivación del sector productivo y consolida el desarrollo turístico en la ciudad de Santa Fe” aseguró la edila.

La reglamentación de estos espacios responde a inquietudes planteadas por emprendimientos comerciales ligados a la producción artesanal de cerveza, lo que permite ampliar y diversificar la oferta en los rubros gastronómicos, productivos y afines. También otorga la posibilidad de sumar a la oferta turística de la ciudad las visitas guiadas a fábricas de cerveza artesanal para luego acceder a un espacio de degustación, o viceversa, a fin de conocer más sobre nuestra tradición cervecera y acerca del proceso de elaboración del producto final.