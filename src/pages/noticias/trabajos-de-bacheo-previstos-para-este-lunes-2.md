---
category: La Ciudad
date: 2021-04-19T06:15:38-03:00
thumbnail: https://assets.3dnoticias.com.ar/Bacheo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Trabajos de bacheo previstos para este lunes
title: Trabajos de bacheo previstos para este lunes
entradilla: La Municipalidad informa las calles por las que avanza el cronograma de
  tareas previsto en el Plan de Bacheo 2021. Puede haber cortes de circulación y desvíos
  de colectivos a medida que se desarrolla la obra.

---
En enero pasado, la Municipalidad puso en marcha un plan de bacheo para reparar más de 12.000 metros cuadrados de calles de la capital provincial. El programa se desarrolla por fases, priorizando las zonas de la ciudad que evidencian mayor circulación.

En la zona del micro y macrocentro, está previsto que los trabajos se concreten en:

· La Rioja y 4 de Enero

· La Rioja, entre Francia y Saavedra

· La Rioja, entre San Jerónimo y 9 de Julio

· La Rioja, entre San Jerónimo y San Martín

· Tucumán, entre San Jerónimo y 9 de Julio

· Primera Junta, entre San Martín y San Jerónimo

· Eva Perón, entre 25 de Mayo y San Martín

· Moreno, entre 1° de Mayo y 4 de Enero

· 3 de Febrero, entre 4 de Enero y Urquiza

· Salta, entre San Martín y San Jerónimo

En tanto, en avenidas troncales, la obra continuará por:

Aristóbulo del Valle y French, mano norte-sur

Aristóbulo del Valle, entre avenida Gorriti y Los Paraísos, mano sur-norte

**Por el operativo de vacunación**

La Municipalidad comunica que, en el marco del operativo de vacunación que se realiza en la ciudad, habrá controles en el tránsito vehicular en las inmediaciones de:

La Esquina Encendida, en la intersección de Facundo Zuviría y Estanislao Zeballos, de 8 a 20 horas