---
category: La Ciudad
date: 2021-12-01T06:15:37-03:00
thumbnail: https://assets.3dnoticias.com.ar/SAUL.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Perman debería someterse a un PCR para asumir como concejal
title: Perman debería someterse a un PCR para asumir como concejal
entradilla: El concejal electo Perman mantiene su posición de no vacunarse contra
  el Covid-19. La ceremonia de asunción sería jueves o viernes de la semana que viene

---
Saúl Perman fue una de las grandes sorpresas que dejaron las elecciones generales en Santa Fe. El personaje de la ciudad que pasea en su bicicleta repartiendo abrazos y militando por la salud física y mental se prepara para jurar como concejal la próxima semana (hasta este martes a la tarde aún no estaba definido el día).

Con la decisión y ratificación de Saúl Perman de no vacunarse contra el Covid-19, las autoridades del Concejo deben resolver de qué forma se realizará su asunción, ya que se trata de un acto presencial en el cual los legisladores electos prestan juramento de desempeñar el cargo fiel y legalmente.

Este escenario inédito se presenta en el marco de dos situaciones particulares: por un lado los dos proyectos presentados la semana pasada en el Concejo (uno de Mastropaolo del PRO, y el otro de Guillermo Jerez de Barrio 88) vinculado a la obligatoriedad de estar vacunado para participar de forma presencial de las reuniones y sesiones; y por el otro, la implementación (en poco tiempo, según el gobierno) del pasaporte sanitario para ingresar a eventos, partidos, fiestas y actividades específicas en lugares cerrados.

En este contexto, se analiza la posibilidad de que Saúl Perman tenga que acreditar una prueba de PCR negativo por Covid-19 para que pueda participar de la ceremonia, asuma el cargo y realice la jura de forma presencial.

Así lo indicaron tanto desde el entorno del candidato electo, como desde el Concejo Municipal a UNO Santa Fe. Por estas horas, continúa el diálogo entre las dos partes para definir un protocolo especial y atender una situación singular.

En la sesión preparatoria, además de asumir los ocho concejales electos, se elegirán las nuevas autoridades del cuerpo legislativo, como lo son el presidente, vicepresidentes y la conformación de las comisiones. Hasta este martes a la tarde, no estaba definido el día, aunque confirmaron que no pasará de la semana que viene (jueves 9 o viernes 10 de diciembre).

Fuentes cercanas a Perman confirmaron que los proyectos presentados en el Concejo, vinculados a la obligatoriedad de la vacuna, no van a torcer su decisión de no vacunarse.

En esa línea, indicaron que en caso de que exista acuerdo parlamentario y que avancen algunas de esas normativas, Perman se prepara para sesionar a través de Zoom. "La vacunación es optativa", enfatizó el consultado.

"Si el cuerpo (por el Concejo) lo establece así, en principio sesionaría de forma virtual", añadió. En cuanto a los motivos por los cuales decide no vacunarse, deslizó: "Él no se vacuna por decisión propia. No es obligatoria. Cuando la vacuna pase su fase experimental, ahí evaluará la posibilidad de colocársela, que muy probablemente lo haga". Desde el entorno de Perman se mostraron sorprendidos por los llamados que comenzaron a recibir de movimientos antivacunas. En ese sentido, uno de los asesores aclaró que Perman "no es antivacuna y no participa de ningún grupo antivacuna".

Sebastián Mastropaolo, edil que propone que todos los concejales acrediten la vacunación contra el Covid-19 para poder sesionar de forma presencial, planteó que el candidato de "La Causa", además de no vacunarse, "tampoco usa barbijo en lugares cerrados y no respeta ningún tipo de protocolo". "Hay una doble situación que nos preocupa”, aseveró Mastropaolo.

El legislador del PRO apuntó: "Cada uno puede hacer lo que quiera y tiene la libertad de elegir. El problema es cuando esa libertad vulnera los derechos de otras personas, que están cumpliendo con diferentes protocolos”.

En el mismo sentido, expresó en declaraciones a Radio EME que “el Concejo estableció los protocolos y puede solicitar cuestiones protocolares para cuidar la integridad física de todos los miembros. Estamos buscando un camino para que pueda desarrollar la actividad normal, para la cual fue elegido, pero que respete los derechos del resto de las personas que van a estar dentro del Concejo Municipal”.

Por otra parte, comentó que “lo más preocupante es el mensaje que se quiere dar, de desafiar a una pandemia a través de la energía y la buena alimentación". Al mismo tiempo, indicó que la herramienta que podrá utilizar será la virtualidad.

**Vacunación voluntaria, pero con pasaporte sanitario**

El plan estratégico para la vacunación contra el coronavirus en la Argentina establece en su artículo 6: "La vacunación, en el marco del Plan Estratégico para la Vacunación contra la Covid-19 será voluntaria, gratuita, equitativa e igualitaria y deberá garantizarse a toda la población objetivo, independientemente del antecedente de haber padecido la enfermedad".

En este contexto de vacunación optativa, los gobiernos provincial y nacional avanzan con la idea del pasaporte sanitario. La ministra de Salud de la Nación, Carla Vizzotti, adelantó que "se espera que diciembre sea el mes de completar el esquema de vacunación" contra el coronavirus, y que se logre "acelerar" la campaña de inmunización "con la implementación del pase sanitario", con el objeto de "disminuir la transmisión y además favorecer y sostener las actividades económicas".

Y enfatizó: "Nos estamos preparando para minimizar el efecto contagio de cualquier rebrote de una nueva cepa como lo que estamos viendo en Europa".

"Hace meses que estamos trabajando con Innovación para que cuando sea necesario, se utilice (un pase) y así estimular a la vacunación porque tenemos un 8 por ciento de mayores de 18 años que todavía no iniciaron el esquema de vacunación mientras que hay alrededor de siete millones de vacunas para ser aplicada la segunda dosis y todavía las personas no se acercaron".