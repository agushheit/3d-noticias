---
category: Agenda Ciudadana
date: 2022-12-17T08:28:13-03:00
thumbnail: https://assets.3dnoticias.com.ar/THD-1a.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: Un nuevo estudio revela que está aumentando la polarización en las Américas
title: Un nuevo estudio revela que está aumentando la polarización en las Américas
entradilla: El aborto, el cambio climático y la inmigración son los principales temas
  de discusión.

---
El nuevo estudio de LLYC y Más Democracia, analiza conversaciones polarizantes en redes sociales en los Estados Unidos, México, y Brasil, entre otros. El estudio demuestra los efectos del consumo de contenidos polarizantes a diario y cuenta con la participación de varios expertos del ámbito de la psicología y la neurociencia. El aborto, el cambio climático y la inmigración son los principales temas de discusión.

MIAMI--(BUSINESS WIRE) --“THD: The Hidden Drug”, el nuevo informe de LLYC y Más Democracia dio lugar a una campaña que demuestra cómo la adicción a la polarización en las redes sociales alcanza en determinados casos el rango de droga: una droga escondida tras la aparente normalidad del uso de estas plataformas digitales. Utilizando técnicas de Big Data e Inteligencia Artificial, el informe cuenta con la colaboración de expertos en neurociencia y psicología para analizar conversaciones en redes sociales en países como Estados Unidos, México, Brasil, entre otros.

JF Muñoz, CEO de Estados Unidos de LLYC: “Esta adicción a las redes, y en especial a contenidos polarizantes, genera, tanto en las personas como en la sociedad, síntomas similares a los de una droga tipo C. Esta tendencia es sumamente relevante en un mundo en el que sigue creciendo el uso masivo de redes sociales.”

En el informe se advierte además de un incremento progresivo de esta “adicción a la conversación”, que en Estados Unidos alcanza niveles del 15% anuales de crecimiento continuo; esto es, el nivel de involucración o engagement de los usuarios de uno y otro lado del espectro político en los territorios de conversación.

A continuación, se presentan los hallazgos más relevantes del estudio.

LA POLARIZACIÓN EN LAS AMÉRICAS

1\. Temáticas

Los temas que más polarizan la conversación en Estados Unidos son: el aborto, el cambio climático, la inmigración, el racismo y los derechos humanos. En países de Latinoamérica son el aborto, la libertad de expresión y los derechos humanos.

En Estados Unidos, la libertad de expresión es el tema que más polariza a la sociedad, mientras que los derechos humanos es la que mayor volumen de conversación moviliza. En México la conversación está creciendo de forma muy marcada en las temáticas libertad de expresión, derechos humanos, racismo, aborto y feminismo.

México es un país menos polarizado que Estados Unidos y Brasil; y existe un mayor consenso en el feminismo y cambio climático respecto al resto de países iberoamericanos, mostrando una polaridad mucho más baja y un nivel de involucramiento más moderado.

2\. Posturas de opinión

En Estados Unidos, Colombia y Brasil es el sector progresista el que domina el campo de la conversación. Por el contrario, en México, Argentina, República Dominicana y Panamá es la parte conservadora la predominante.

3\. La polarización y los movimientos sociales

El racismo presenta el mayor consenso en Estados Unidos, pero ve rebajada hasta en un 74% el índice de polarización durante varios meses por el consenso en redes sociales al respecto.

Por otro lado, en los países de América Latina, la conversación sobre el feminismo crece en volumen 18% anual. Mientras que, en Estados Unidos, aumenta la conversación sobre el aborto, con un crecimiento del 76%.

Para Mariano Sigman, neurocientífico y autor de El poder de las palabras, “es difícil medir el riesgo exacto de una adicción; en algunos casos es bien conocido, pero en otros, como la polarización, no. La incomprensión hace que odie hasta tal extremo que se decide que la única forma de resolverlo es matándolos a todos en una guerra. Este puede ser el verdadero riesgo de una droga como la polarización”.

El informe analiza 600 millones de mensajes en redes sociales, recogidos entre el 1 de septiembre de 2017 y el 31 de agosto de 2022. Para descargar el informe completo, vídeo e imágenes de la campaña, haz clic aquí.

Sobre LLYC

LLYC es una firma global de consultoría de comunicación, marketing digital y asuntos públicos que ayuda a sus clientes a afrontar sus retos estratégicos con soluciones y recomendaciones basadas en la creatividad, la tecnología y la experiencia, buscando minimizar los riesgos, aprovechar las oportunidades y cuidar el impacto reputacional. En el actual contexto disruptivo e incierto, LLYC contribuye a que sus clientes alcancen sus metas de negocio a corto plazo y a fijar una ruta, con una visión de medio y largo plazo, que defienda su licencia social para operar y aumente su prestigio.