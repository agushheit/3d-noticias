---
category: La Ciudad
date: 2022-01-23T06:00:39-03:00
thumbnail: https://assets.3dnoticias.com.ar/cuidaio.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Santa Fe, entre los primeros lugares en donación de órganos y tejidos
title: Santa Fe, entre los primeros lugares en donación de órganos y tejidos
entradilla: "En el país hay alrededor de 7000 mil personas que esperan un órgano o
  un tejido; en la provincia son más de 500. Un proyecto que nació en los '80 y se
  sostuvo como política de Estado. El impacto de la ley Justina.\n\n"

---
"Tuvimos uno de los hospitales que más procuró órganos en todo el país, que es el Hospital de Emergencias Clemente Álvarez de Rosario, junto con Tucumán, Corrientes y Mendoza". Así lo aseguró el director del Cudaio, Mario Perichón, aclarando que esta situación no se produjo sólo específicamente en el marco de la pandemia, "sino que siempre fueron hospitales con una alta concentración de emergencias médicas y de allí podemos sacar la mayor concentración de donantes".

De todos modos, comentó que también hubo sorpresas agradables durante la pandemia. "Cuando empezamos la gestión decidimos llevar a los cinco hospitales regionales el programa de Hospital Procurador, que implica independizar a los hospitales del Cudaio, para que éste sea un contralor y regulador de la ley y que los hospitales entiendan que la procuración de órganos es un servicio más que da a la población. Empezamos con el Hospital de Venado Tuerto el 4 de abril de 2020. Teníamos que ir a hacer la capacitación y empezó la pandemia, por lo tanto recién la pudimos hacer en noviembre. Curiosamente en julio de 2020 hubo siete donantes, y fue el hospital que más procuró en el país durante ese mes".

Perichón aclara que la tarea no acabó allí, sino que, lejos de eso, "cuando aflojó la pandemia estuvimos en el Hospital de Reconquista, en el de Rafaela que, sacando Rosario y Santa Fe, eran los hospitales donde nos faltaba la capacitación. A su vez el Incucai mandó fondos para comprar equipamiento médico para esos hospitales como una estrategia de que el hospital sea la unidad de procuración en si misma. Y a éstos se suman el Clemente Alvarez de Rosario, el Cullen de Santa Fe y el Eva Perón de Granadero Baigorria donde el personal ya está capacitado y están los equipamientos. En este marco, hay que remarcar que Santa Fe está siempre entre los primeros lugares en procuración de órganos y tejidos".

\- La ley 27447, conocida como Ley Justina, que busca facilitar la donación de órganos, ¿tuvo un efecto concreto?

\- Creo que sí, aunque es muy difícil de medir. La Argentina tiene una historia en procuración de órganos que a veces no se conoce, salvo quienes estamos en este tema. Hasta 2003 el nuestro era uno de los primeros países de América en esa materia y ese año se tomó una decisión avalada por un convenio con España, que es líder, para generar el Programa Federal de Procuración de órganos. A partir de allí, la Argentina crece en este tema, incluidos años complejos: por la pandemia bajó la cifra de donantes en todo el mundo, pero si se compara, el porcentaje en Europa fue menor al que se logró en la Argentina. Lo pudimos sostener con el diario del lunes, porque sabíamos lo que había pasado en Europa.

La Ley Justina lo que vino a hacer es a corregir una serie de defectos que tenía la ley anterior que era del año 2005 y había legislado el consentimiento presunto pero este concepto había quedado en una nebulosa por la redacción del artículo, sobre todo de la participación y los tiempos de la Justicia. Entonces, la ley corrigió ese punto y, además, habilitó que se realice la donación cruzada (como en el caso del periodista Jorge Lanata), práctica que en la Argentina no estaba habilitada.

La ley es de 2018 y luego vino la pandemia por el nuevo coronavirus, así que hubo poco tiempo para hacer una evaluación.

Siempre decimos que a nivel nacional nuestro sistema sanitario atiende pacientes y todo el recurso humano y las leyes laborales están formados o hechas para ese tipo de proyectos. Nosotros no atendemos pacientes, atendemos muertos; a partir de allí cambia totalmente la mirada y a veces es difícil que se comprenda.

\- La ley insta a que la persona que no quiere ser donante tenga que manifestarlo.

\- Eso ya estaba previsto en la ley de 2005 pero los legisladores pusieron una coma "salvo última opinión del fallecido". Eso lo corrige la ley Justina. Hoy somos todos donantes y si no lo queremos ser tenemos que expresarlo.

**Personas en lista de espera**

En el país hay alrededor de 7 mil personas que esperan un órgano o un tejido; en la provincia son más de 500. Son listas separadas. El órgano con mayor demanda es el riñón y, entre los tejidos, figura la córnea. Aquí Perichón marca una diferencia necesaria: "El órgano tiene que estar abastecido con sangre y requiere una compatibilidad genética; en el tejido, como la córnea que es avascular, requiere para su trasplante de datos clínicos. En la provincia son 52 las personas en lista de espera de córnea.

\- ¿Cuáles son los objetivos de Cudaio para 2022?

\- Primero, volver lo más rápido posible a la normalidad y de esa manera recuperar también el recurso humano sobre el que hay un fuerte impacto por Ómicron: nuestros médicos coordinadores están en las terapias intensivas y, por lo tanto, son los más expuestos.

En segundo lugar, tenemos conversaciones con diputados del oficialismo y la oposición, y el Tribunal de Cuentas para impulsar la modificación de algunos artículos de la Ley provincial de Trasplante que es del año 1995. La pandemia mostró claramente algunos errores que surgen a partir de no haber actualizado algunos artículos.

\- ¿Por ejemplo?

\- Somos una entidad autárquica y descentralizada por ley pero no lo somos en la práctica. Tenemos nuestro propio presupuesto pero no lo podemos ejecutar salvo a través del trámite burocrático en el Ministerio de Salud. Ese punto, teniendo en cuenta que somos una actividad tan particular nos complica bastante. Los equipos de procuración de órganos en el mundo son altamente demandados porque la formación del recurso humano involucra conocimientos de leyes, logística, terapia intensiva, de laboratorios de alta complejidad. Y también hay que adaptar la norma a la nueva Constitución que fue modificada en 1994 y amplió una serie de derechos que no están contemplados en la ley de trasplante de nuestra provincia.

Cuando las sesiones ordinarias se habiliten deberíamos debatir este tema. Estamos trabajando en un proyecto de cambio de algunos artículos para hacer más ágil nuestro trabajo.

El Incucai lo hizo en su momento con la Ley Justina, que cambió todo el perfil institucional para que pudiera disponer de más agilidad para resolver los problemas cotidianos.

**Política de Estado**

Entre coordinadores, administraciones, trabajadores sociales, bioquímicos, biotecnólogos, médicos, enfermeros, son alrededor de cien personas las que componen el equipo del Cudaio. "Estuve fuera del país, me formé en el exterior y si la gente supiera lo que vale el recurso humano en procuración de órganos y lo que se compite en el mundo para incorporarlo, sobre todo en Europa… El entrenamiento que tienen estas personas es altamente complejo", añade Perichón.

El titular del Cudaio admite que, si bien en nuestro país es difícil encontrar políticas de Estado, "el Incucai y el Cudaio lo son: porque la ley de trasplantes en la Argentina que fue la primera en Latinamérica se hizo en el año '78 y después vino la democracia: cambiaron presidentes, ministros de salud y siempre creció la procuración de órganos".

**Colecta de sangre**

El mes de enero es crítico para la donación de sangre y este año se sumó a las altas temperaturas y las vacaciones, que operan como disuasivos para esta acción voluntaria, el incremento de contagios por Covid-19. Por esa razón desde el Cudaio se dispuso habilitar un espacio fijo en la ciudad de Rosario para recibir cada jueves de enero por la mañana a voluntarios y voluntarias en la sede de la Gobernación de esa ciudad, mientras que en la capital provincial el espacio elegido es El Molino (bulevar y Pedro Vittori).

"En nuestra página oficial (cudaio.gob.ar) exponemos la actividad que desarrollamos e invitamos a la gente a que se acerque a los espacios públicos o privados para que las reservas de sangre, que se están viendo resentidas en esta época del año, no caigan aún más", explicó Andrea Acosta, a cargo del área de Hemoterapia de la entidad.

El llamado es abierto a toda la comunidad mientras se cumplan requisitos básicos, que también están descriptos en el sitio oficial: Desayunar de forma habitual, pesar más de 50 kg., concurrir con DNI, haber transcurrido 12 meses luego de realizarse un tatuaje /o piercing. Si se tuvo covid habrá que esperar 28 días después del alta para donar, y en caso de haber recibido la vacuna, se deberán esperar 72 horas.

La convocatoria es con inscripción previa en: www.cudaio.gob.ar/colectas-sangre/