---
category: Agenda Ciudadana
date: 2021-01-18T09:00:48Z
thumbnail: https://assets.3dnoticias.com.ar/gabinete_federal.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: LT10
resumen: El Presidente encabeza el martes la segunda reunión del gabinete federal
title: El Presidente encabeza el martes la segunda reunión del gabinete federal
entradilla: Fernández viajará a La Rioja con una amplia comitiva encabezada por el
  jefe de Gabinete, Santiago Cafiero; y los ministros del Interior, Eduardo "Wado"
  De Pedro, y de Desarrollo Territorial y Hábitat, Jorge Ferraresi.

---
El presidente Alberto Fernández encabezará el próximo martes la segunda reunión del gabinete federal en Chilecito, La Rioja, en tanto el miércoles promoverá otro encuentro, también en esa "capital alterna", con los 10 gobernadores del norte grande (NG), ambas con el objetivo de buscar el desarrollo de esa regiones.

Fernández viajará con una amplia comitiva encabezada por el jefe de Gabinete, Santiago Cafiero; y los ministros del Interior, Eduardo "Wado" De Pedro, y de Desarrollo Territorial y Hábitat, Jorge Ferraresi, entre otros, indicaron a Télam fuentes oficiales.

De esa forma, el gabinete federal, una idea que lanzó el jefe de Estado en campaña electoral y plasmó por ley del Congreso, se volverá a reunir tras haber debutado el 21 de diciembre pasado en Río Grande, Tierra del Fuego, otra de las "capitales alternas" del país.

"Lo que hacemos con el gabinete federal es ir a la capital alterna, en este caso Chilecito, para realizar una jornada de trabajo entre las áreas del Gobierno nacional, la provincia y los municipios", explicó Cafiero, en declaraciones a la prensa, durante la semana.

En ese sentido, Cafiero apuntó que la "idea de trabajo es abonar a mayor federalismo y buscar un equilibrio territorial, en el cual la Argentina tiene un déficit muy marcado".

Asimismo, el ministro coordinador señaló que la agenda de trabajo en Chilecito tiene que ver con "cuestiones productivas y energéticas, de empleo, de promoción de la ciencia y la tecnología y cultural".

Por su parte, el gobernador de La Rioja, Ricardo Quintela, señaló que en la reunión del gabinete federal se firmarán "convenios de desarrollo".

"Me siento orgulloso y privilegiado porque La Rioja es la provincia que más visitó el Presidente y a nuestra provincia siempre nos pone como ejemplo, y tiene un compromiso afectivo", dijo el gobernador a la prensa local.

En Río Grande, en diciembre, se efectuaron anuncios que comprendieron a los ministerios de Transporte; Mujeres, Géneros y Diversidad; Obras Públicas; Interior; Desarrollo Productivo; Desarrollo Territorial y Hábitat.

También hubo anuncios por parte de la Secretaría de Energía y el Ente Nacional de Obras Hídricas de Saneamiento (ENOHSA).

Las otras capitales alternas son Caleta Olivia (Santa Cruz); Comodoro Rivadavia (Chubut); Bariloche (Río Negro); Cultral Có (Neuquén); General Pico (La Pampa); Mar del Plata y La Matanza (Buenos Aires); Rosario (Santa Fe); Río Cuarto (Córdoba); Concordia (Entre Ríos); Goya (Corrientes) y Oberá (Misiones).

También integran el flamante universo de capitales alternas Presidencia Roque Sáenz Peña (Chaco); Formosa (Formosa); San Pedro de Jujuy (Jujuy); Orán (Salta); Tinogasta (Catamarca); Monteros (Tucumán); La Banda (Santiago del Estero); Caucete (San Juan); San Luis (San Luis) y Guaymallén (Mendoza).

En relación a la reunión del miércoles del Presidente con los gobernadores del Norte Grande, la primera que se realizará con ese bloque regional, el debate girará sobre los "temas más importantes de la región", indicó Quintela.

"Al igual que la Patagonia, queremos que se desarrollen rápidamente nuestras comunidades", sostuvo el mandatario riojano.

El Consejo Regional Argentino Norte Grande reúne a los gobernadores del noroeste (NOA) y el noreste (NEA) y fue creado en diciembre pasado en Chaco, con el objetivo de impulsar el desarrollo estratégico y corregir asimetrías estructurales en infraestructura.

Está presidido por el gobernador de Chaco, Jorge Capitanich, por seis meses, y lo acompañan sus pares de Formosa, Gildo Insfrán; de Catamarca, Raúl Jalil; de Tucumán, Juan Manzur; de Jujuy, Gerardo Morales; de Corrientes, Gustavo Valdés; de Misiones, Oscar Herrera Ahuad; de Santiago del Estero, Gerardo Zamora; y de Salta, Gustavo Sáenz, además de Quintela.

El mandatario riojano indicó que el miércoles presentarán propuestas "para achicar las asimetrías" y "crecer más equitativamente".

"Estamos dando una batalla por el federalismo", dijo y enumeró que algunos temas del encuentro serán el "gasoducto del norte y la recuperación de las líneas férreas", que tiene como objetivo "garantizar mayor competencia en lo que se produce".

"Es necesario rediscutir los servicios energéticos, acuíferos, gasíferos y el combustible", evaluó Quintela, y analizó fundamental para el desarrollo del país avanzar con el "corredor bioceánico" para que Argentina comercialice con Europa y con África por el Atlántico.

"Hay que potenciar el comercio con el sudeste asiático que es un mercado importantísimo que demanda muchísimo de lo que se produce localmente", afirmó el gobernador y adelantó que "no está en agenda la coparticipación" pero "sí las ventajas comparativas de las provincias".