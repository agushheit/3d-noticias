---
category: Agenda Ciudadana
date: 2022-02-22T09:25:23-03:00
thumbnail: https://assets.3dnoticias.com.ar/2022-02-21NID_273869O_1.jpeg
layout: Noticia con imagen
link_del_video: C:\Users\usuario\Downloads/2022-02-21NID_273869O_1.mp4
author: ''
resumen: Se presentó el censo 2022 en la  Ciudad  de Santa Fe.
title: Se presentó el censo 2022 en la  Ciudad  de Santa Fe.
entradilla: 'Se llevará a cabo el miércoles 18 de mayo. Por primera vez en el país
  será de modalidad bimodal: digital y presencial.'

---
El director del Instituto Nacional de Estadística y Censos de Argentina (INDEC), Marco Lavagna; y el director del Instituto Provincial de Estadísticas y Censos (IPEC), Gabriel Frontons, presentaron este lunes, en la ciudad de Santa Fe, los detalles del Censo 2022 de la República Argentina, que se llevará a cabo el miércoles 18 de mayo.

Por primera vez en el país, el Censo 2022 será bimodal (digital y presencial). Al respecto, Lavagna describió que “es una de las grandes innovaciones que hemos hecho. Los censos deben evolucionar a medida que va evolucionando la sociedad. Hace 10 años no existía la pregunta de identidad de género, la sociedad fue evolucionando y hoy no poner preguntas sobre identidad de género estaríamos no dando información sobre un tema que la sociedad está demandando”.

Sobre la importancia del censo, el director del INDEC destacó que la relevancia “que tienen los datos a la hora de tomar decisiones. A veces parece que las estadísticas son algo lejano, pero los censos nos permite volver a conocernos, a reconocernos, a encontrar nuestra diversidad cultural, nos permite conocer como país, conocer nuestras debilidades y nuestras fortalezas sobre aquellos temas que hay que trabajar. Los censos nos muestran como somos, de ahí la importancia”.

Por último, Lavagna se refirió a los resultados finales y dijo que “vamos a tenerlos en un año y medio. Eso no significa que durante ese tiempo no vayamos a dar información. La primera información la vamos a dar esa misma noche o al día siguiente, a los 3 meses vamos a dar el primer informe preliminar, a los 8 meses vamos a dar el informe final y al año y medio vamos a tener toda la base disponible con el mayor nivel de detalle para que toda la sociedad acceda”.

Por su parte, Frontons sostuvo que “tuvimos una reunión del comité censal a los efectos de coordinar el trabajo que tiene que ver con el operativo. El censo tiene una gran importancia que nos va a permitir conocer de manera actualizada la estructura demográfica socioeconómica de la provincia, la distribución espacial de la población, vamos a contar de manera correcta a todas las personas, a los hogares y a las viviendas. Es el operativo estadístico más grande que se realiza en la provincia e involucra el trabajo de más de 50.000 personas”.

Censo digital

Desde el 16 de marzo y hasta el 18 de mayo de 2022 a las 8 de la mañana, las personas que lo prefieran podrán autocompletar el cuestionario digital para censar a todos los integrantes de su hogar. El Censo digital se podrá completar desde cualquier dispositivo digital con conexión a internet, a través de los siguientes pasos:

> > Ingresar en censo.gob.ar.

> > Seleccionar el botón “Censo digital” ubicado en el margen superior derecho de la pantalla.

> > Seguir las instrucciones para generar el código único de la vivienda.

> > Acceder al cuestionario digital para responder las preguntas por todas las personas que integran el hogar.

> > Guardar el comprobante del Censo digital (código alfanumérico de seis dígitos que se entrega al completar el cuestionario) para el miércoles 18 de mayo, día del Censo.

Censo presencial

El 18 de mayo, día que será feriado nacional (Decreto 42/2022), las personas censistas visitarán todas las viviendas del país entre las 8 y las 18 h para solicitar el comprobante del Censo digital o realizar la entrevista tradicional a quienes no hayan completado el cuestionario en línea.

Relevamientos especiales

Además de la modalidad digital y del Día del Censo, habrá tres operativos especiales:

> > Viviendas colectivas: se hará un relevamiento digital durante los diez días previos al Día del Censo en cárceles, geriátricos y hospitales, por citar tres ejemplos.

> > Personas en situación de calle: se censará a las personas que viven en la calle durante la noche previa al Día del Censo.

> > Áreas rurales: se desarrollará un operativo presencial que se extenderá durante los diez días previos al censo.

Novedades del censo

Las personas serán contabilizadas según su lugar de residencia habitual, es decir, donde pasan la mayor parte del tiempo durante la semana y en el lugar donde demandan servicios, por ejemplo. Hasta ahora, el censo contabilizaba a las personas en el lugar en el que habían pasado la noche anterior.

El cuestionario censal tendrá hasta 61 preguntas. Los cambios que introduce el nuevo cuestionario son:

> > Sexo e identidad de género: se preguntará a todas las personas el sexo registrado al nacer (incluyendo la categoría “X”) y cómo se consideran/autoperciben (“mujer”, “mujer trans / travesti”, “varón”, “varón trans / masculinidad trans”, “no binario”, “otra identidad / ninguna de las anteriores”).

> > Autorreconocimiento indígena u originario: se preguntará a todas las personas si se reconocen indígenas o descendientes de pueblos indígenas u originarios, aclarando el pueblo y, además, si hablan o entienden la lengua de ese pueblo declarado.

> > Autorreconocimiento afrodescendiente o de antepasados negros o africanos: todas las personas responderán si se reconocen afrodescendientes o de antepasados negros o africanos.

> > Dificultades / discapacidad: se relevará si en los hogares hay personas con dificultades para caminar o subir escaleras; recordar o concentrarse; comunicarse, entender o ser entendidas por otras personas; oír, aun con el uso de audífonos; ver, aun con anteojos; y comer, bañarse o vestirse solas.

El Censo 2022 en números

Participarán más de 600 mil personas, entre censistas urbanos y rurales, coordinadores nacionales y provinciales y otros puestos que integran la estructura censal, que será capacitada a través de un campus virtual

desarrollado íntegramente por el INDEC.

Se censarán más de 15 millones de viviendas y aproximadamente 45 millones de personas.