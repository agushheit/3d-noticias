---
category: Estado Real
date: 2021-03-20T06:15:44-03:00
thumbnail: https://assets.3dnoticias.com.ar/lagna.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Perotti tomó juramento al nuevo Ministro de Seguridad, Jorge Lagna
title: Perotti tomó juramento al nuevo Ministro de Seguridad, Jorge Lagna
entradilla: "“Tengo una gran expectativa en esta nueva etapa, porque ya se dieron
  pasos muy importantes y necesarios en estos últimos meses”, afirmó el gobernador
  de la provincia."

---
El gobernador Omar Perotti puso este viernes en funciones y tomó juramento al flamante ministro de Seguridad de la provincia de Santa Fe, Jorge Lagna, quien se venía desempeñando en el cargo de secretario de Gestión Institucional y Social.

En la oportunidad, el gobernador afirmó que “el 10 de diciembre de 2019 fijamos los lineamientos de trabajo para la provincia. Dejamos muy claro que tenemos el compromiso de combatir a las mafias y cortar los vínculos con el delito. Con esos lineamientos seguiremos trabajando, con esos lineamientos nos hemos esforzado todos estos meses de trabajo, en los que quiero agradecerle a Marcelo Sain por su compromiso”.

“Tengo expectativa en esta nueva etapa porque ya se dieron pasos muy importantes y necesarios”, continuó Perotti. Describió que esas acciones “tienen que ver con el compromiso de la fuerza mayoritaria, de entender que hay cuidar al vecino y que hay que hacer todos los esfuerzos para cuidarlo mejor todos los días. Esa es la tarea: dejar de lado cualquier instancia que integrantes de la fuerza protejan o tengan algún vínculo con el delito. Eso es un paso importante que hay que seguir profundizando”.

Más adelante, el gobernador afirmó: “Habrá nuevos agentes. En estos días se sumaron más de 400 y se sumarán otros 300 federales. Mientras tanto, ya se sentaron las bases que nos van a permitir dar un gran salto en equipamiento, en tecnología, en incorporaciones mucho más numerosas para la formación de la institución policial, en profundizar la capacitación y, sin duda, en otorgarle un rol preponderante a la mujer dentro de la institución. En esos lineamientos seguiremos”, concluyó Perotti.

En tanto, el flamante ministro manifestó su “agradecimiento” al gobernador e indicó: “Estuve trabajando 14 meses en un programa concreto de seguridad, que lo vamos implementando paso a paso. La idea es seguir profundizándolo, tener mayor presencia preventiva en las calles y un mayor diálogo con todos los estamentos políticos, ONGs y vecinales. Hay que escuchar y ponerse a trabajar en consecuencia”.

En ese sentido, precisó que “en Rosario ya hemos planeado operativos de saturación preventivos en las calles del centro y de algunos barrios, donde voy a estar acompañando a la fuerza policial en el terreno. Me verán mucho en el territorio trabajando, dialogando y con el apoyo férreo del gobernador en algo fundamental que se traduce en las políticas, porque cuando leemos el presupuesto sabemos qué quiere un gobernante; y en el presupuesto hay una inversión muy fuerte en seguridad y educación”.

Por último, Lagna detalló: “Pronto vamos a tener más vehículos en las calles y más tecnología. Vamos a seguir mejorando la capacitación de los policías”. Al respecto, insistió en que “hicimos una gran inversión edilicia y estamos creando escuelas regionales de policías; en un mes comenzará a funcionar la primera en la localidad de Murphy. Tenemos planeadas otras en Rafaela y Reconquista. Hay un programa, hay un plan y lo vamos a llevar a cabo para darle más tranquilidad a todos los vecinos de la provincia”, concluyó.