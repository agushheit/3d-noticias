---
category: La Ciudad
date: 2022-01-23T06:00:29-03:00
thumbnail: https://assets.3dnoticias.com.ar/arbolcaido.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Extracción de árboles: la prioridad serán las zonas con mayor circulación'
title: 'Extracción de árboles: la prioridad serán las zonas con mayor circulación'
entradilla: 'Así lo definieron desde el municipio. Zona de bulevares, plazas, escuelas
  y paradas de colectivos serán las áreas elegidas a raíz de una "valoración de riesgos".

  '

---
En las últimas horas se generó un intenso debate en las distintas redes sociales de los santafesinos a raíz de la decisión de la Dirección de Arbolado de la municipalidad de extraer árboles añejos que ya "no brindan servicios ambientales". La extracción del arbolado frente a la Estación Belgrano puso en la mesa de discusión el accionar municipal.

¿Era necesario talar árboles en pleno enero con una intensa ola de calor?; ¿Se podría haber esperado algunos meses más para que baje un poco la intensidad del sol?; ¿Es necesario arruinar así la visual de uno de los paseos más emblemáticos de la ciudad como la Estación Belgrano?; ¿Existen verdaderamente políticas ambientales?, fueron algunas de las preguntas que comenzaron a viralizarse en las redes sociales de las nuevas generaciones santafesinas preocupadas por la naturaleza.

Y esas respuestas fueron rápidamente atendidas por el ejecutivo municipal, tanto en las redes sociales como en ronda de prensa. Fue precisamente, la ingeniera agrónoma Milagros Gasser, directora de Arbolado Urbano quien salió a comunicar la situación con la extracción de árboles sobre Bulevar y en diversos puntos de la ciudad.

"Las tareas realizadas en los últimos días, como frente a la Estación Belgrano, están incluidas dentro de un plan de trabajo que se viene realizando en la ciudad. No se trata de poda, sino de extracción de árboles a raíz de la elaboración de una valoración de riesgos", comenzó argumentando Gasser y continuó: "También recibimos varias consultas de vecinos de la zona sobre el estado sanitario y estructural de los árboles, sobre un corredor tan importante como es Bulevar Gálvez".

"Como área buscamos sostener la integridad de los ejemplares en la ciudad, y sobre todo, el análisis del aspecto seguridad del espacio público, donde los árboles son parte de los riesgos potenciales, como así también los servicios ecosistémicos que brinda", subrayó la directora de Arbolado Urbano municipal.

La extracción de árboles frente a la Estación Belgrano puso sobre la mesa de debate el estado de situación del "bosque urbano" de la ciudad.

En relación a la intervención frente a la Estación Belgrano, sobre la vereda norte de Bulevar Gálvez al 1.100, la funcionaria municipal explicó que "se debió a la existencia de árboles añosos, huecos, colonizados con hongos y hormigas, especies que ya no brindaban sombra y no eran árboles vitales ni sanos, como sí existen en el entorno de bulevar. A la par de la extracción, se realizó una plantación completando la alineación existente con las mismas especies plantadas en la zona, bauhinias o popularmente conocidas como pezuña de vaca. Son especies de reposición, enmarcadas dentro del plan director de arbolado que define tipologías y especies para distintos sectores de la ciudad, siempre pensando en el mantenimiento del bosque urbano de la ciudad de Santa Fe".

Árboles con cemento en su interior

"En la extracción de los árboles frente a la Estación Belgrano hemos roto cadenas de motosierras por encontrarnos en el interior de las especies con bloques de cemento, una situación que no es la vez que sucede en la ciudad, pero sí que no pensábamos encontrar. Eran árboles totalmente huecos rellenos en su interior con cemento", argumento Gasser.

Sobre los lugares y áreas donde el municipio pondrá el foco en la extracción de árboles añejos, la ingeniera agrónoma explicó: "Realizamos un monitoreo constante en diversas áreas de la ciudad, otorgándole prioridad a las zonas con alto flujo de circulación. Hay puntos y usos del espacio que definen a un área como prioritaria, como escuelas, plazas, bulevares y hasta parada de colectivos".

"Acá en bulevar, en 2021, a metros de la Estación Belgrano, cayeron tres ejemplares en pocos meses, por lo tanto lo que estamos haciendo es un trabajo preventivo con los conocimientos técnicos de los profesionales del área con el objetivos de conformar espacios seguros y a la vez mantener la integridad de los árboles existentes", manifestó Gasser y finalizó: "Hay un sector de la ciudad que posee el arbolado heredado, árboles añejos que han sufrido un cambio de paradigma. Lo que respecta a la silvicultura urbana en muy poco tiempo ha cambiado mucho a raíz de nuevos conocimientos y experiencias. Hay un gran porcentaje del arbolado urbano de la ciudad en deterioro y estamos haciendo, primero un relevamiento, y luego extracción de los árboles riesgos; en la medida de lo posible se van plantando y reponiendo por especies nuevas".