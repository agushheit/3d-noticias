---
category: Estado Real
date: 2021-01-13T07:22:34Z
thumbnail: https://assets.3dnoticias.com.ar/Perotti-billetera-SantaFE.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: La provincia lanzó el programa de beneficios y descuentos «Billetera Santa
  Fe»
title: La provincia lanzó el programa de beneficios y descuentos «Billetera Santa
  Fe»
entradilla: El instrumento virtual  tiene como objetivo incentivar la demanda de bienes
  y servicios mediante el otorgamiento de importantes reintegros de dinero a quienes
  realicen compras en comercios de la provincia.

---
Omar Perotti hizo la presentación formal, en Casa de Gobierno, del programa de beneficios y descuentos Billetera Santa Fe. La iniciativa tiene como objetivo incentivar la demanda de bienes y servicios mediante el otorgamiento de reintegros de dinero a todas las personas que realicen compras en comercios radicados en la provincia.

Durante el acto, Perotti estuvo acompañado por el ministro de Producción, Ciencia y Tecnología, Daniel Costamagna; el secretario de Comercio Interior, Juan Marcos Aviano; y el gerente del Banco de Santa Fe, Luis Núñez, entre otros funcionarios y presencias destacadas.

<br/>

## **PROGRAMA DE BENEFICIOS Y DESCUENTOS**

**Billetera Santa Fe** es un instrumento financiero y virtual que permitirá sumar poder de compra en los bolsillos de la gente al otorgar descuentos de un 30 por ciento en la adquisición de alimentos, indumentaria, calzados, juguetería, librería, bares, restaurantes, farmacias, gastronomía y turismo, y un 20 por ciento en electrodomésticos.

El reintegro será solventado por el Gobierno de Santa Fe en un 85 por ciento, y el 15 por ciento restante estará a cargo del comercio. 

Se trata de un esfuerzo compartido, donde el gobierno hace el mayor aporte posible para ayudar a dinamizar la economía y mejorar el poder de compra de los santafesinos y las santafesinas. 

El dinero se acreditará, en un plazo que va entre las 24 y 48 horas hábiles posteriores a la realización de la compra, en el «Saldo Virtual de la Billetera», con un tope de reintegro de 5.000 pesos por mes. Las promociones estarán vigentes todos los días en el caso de los alimentos; en tanto que los lunes, martes y miércoles será para los demás rubros.

<br/>

## **FAVORECER AL CONSUMO**

«Es un gusto estar presentando esta nueva herramienta que se suma a otras que ya tenemos en funcionamiento y que seguramente permitirá tener un mayor nivel de actividad y de consumo en la provincia», dijo Perotti durante la presentación. Y agregó: «este es un programa del gobierno para favorecer el consumo y la producción. Con sus compras, con el compromiso de nuestros comerciantes y la ayuda del gobierno provincial, estamos poniendo a Santa Fe de pie».

El gobernador destacó también el trabajo conjunto de los sectores público y privado, y explicó que a la Billetera Santa Fe «se accede a través de la tarjeta Plus Pago, desde un teléfono celular. Con esto queremos sumar poder de compra a la gente, incentivar el consumo, con importantes descuentos en diferentes productos, y durante todo el año seguir sumando productos santafesinos en ese nivel de oferta».

Del evento encabezado por el gobernador Perotti participaron también el presidente de la Federación de Centros Comerciales de la Provincia de Santa Fe (Fececo), Eduardo Taborda; el presidente del Centro Comercial de Santa Fe, Martín Salemi; el tesorero de la Asociación Empresaria de Rosario, Diego Casadidio y el gerente comercial de Plus Pagos, Pablo López.

<br/>

## **PRODUCCIÓN, ECONOMÍA Y EMPLEO**

«Tenemos dos objetivos: priorizar la producción santafesina y priorizar el poder de compra de los santafesinos; esa combinación es la que tenemos que conseguir, por eso esta es una clara herramienta de desarrollo local que hay que potenciar», dijo Perotti.

El mandatario provincial explicó a continuación: «a esto lo hacemos porque incentiva el consumo. Y para reponer lo que se vende, se necesita producción. Si hay producción, se necesita más empleo. Y si hay empleo, hay sueldos. Y esos sueldos van al consumo. Allí la economía empieza a moverse, y es lo que pretendemos conseguir con esta herramienta. Por eso, necesariamente el Estado tiene que intervenir muy fuerte allí, coordinando, entusiasmando a este sector. Detrás de eso, la conectividad y la infraestructura ayudan, pero la decisión tiene que ser invertir en Santa Fe, eso se valora y el gobierno acompaña al que emprende y trabaja».

<br/>

## **FORMACIÓN Y ECONOMÍA DEL CONOCIMIENTO**

Asimismo, el mandatario santafesino valoró también «la formación y capacitación de nuestra gente: a lo bueno que tiene la provincia en sus tierras, en su industria, en su potencial y diversidad, tenemos que tener también la mejor gente preparada, porque eso es lo que marca la diferencia».

Perotti destacó además la necesidad de «generar con el sector científico el mayor empuje, y vamos muy bien: el nivel de inversión científico-tecnológico desde el esquema privado, con nuevas empresas, a partir de allí Santa Fe tiene todo para ser líder en economía del conocimiento, la estructura que tenemos, la capacidad de nuestra gente vinculada a la capacidad de desarrollo de pequeñas empresas, con nuevos _start-ups_, incubadoras, aceleradoras de estos emprendimientos. 

En los próximos meses Santa Fe va a tener más resultados positivos en esta línea, y nosotros también vamos a estar acompañando y poniendo recursos en esa dirección».

<br/>

## **JUNTOS PARA PONER A SANTA FE DE PIE**

De este modo, el programa **Billetera Santa Fe** se propone generar un incentivo del consumo a través de un reintegro al cliente por las compras realizadas en los comercios de los rubros promovidos. Los locales comerciales notarán un incremento de sus ventas, y el cliente será beneficiado con el reintegro de **Saldo Pluspagos** en su _app_.

Se trata de reactivar el círculo virtuoso de la economía. De generar más producción y, por ende, más consumo. Todo esto, promoviendo el desarrollo local, apoyando a las pymes a través de la inclusión financiera. Este es el camino que eligió la provincia: el sector privado y el público están trabajando juntos. Incorporando tecnología, a partir de uno de los mayores capitales de nuestra provincia y su gente: el capital humano.

Se trata de invertir desde la provincia para crear trabajo y facilitar la vida cotidiana de nuestro pueblo a partir de esta **Billetera Santa Fe**. Como señaló el gobernador Perotti, al concluir su presentación: «Este es el camino, juntos siempre para poner a Santa Fe de Pie».