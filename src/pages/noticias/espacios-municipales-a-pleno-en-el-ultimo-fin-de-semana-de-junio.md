---
category: Agenda Ciudadana
date: 2022-06-24T09:05:24-03:00
thumbnail: https://assets.3dnoticias.com.ar/2-Teatro_Municipal_Junio_en_la_Marechal_Dolly_y_el_hombre_sombra-1024x683.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: Espacios municipales a pleno, en el último fin de semana de junio
title: Espacios municipales a pleno, en el último fin de semana de junio
entradilla: Propuestas culturales  como la segunda edición de +Feria, y  numerosos
  ciclos de música y espectáculos.

---
Después de un fin de semana largo, con gran participación del público en las actividades culturales y turísticas, los espacios municipales se preparan para despedir este mes con una agenda cargada de eventos, espectáculos y paseos por la ciudad.

Del jueves 24 al domingo 26, se desarrollará la segunda edición de +Feria, en la planta alta de la Estación Belgrano (Bv. Gálvez 1150). Cada jornada de este encuentro de arte contemporáneo, se extenderá entre las 16 y las 21. Entre las innovaciones de esta segunda edición, se contará con la participación de 23 galerías y proyectos de arte,  provenientes de seis provincias argentinas y CABA, y se implementará un sistema de adquisición de obras a través de premios que otorgarán la Municipalidad y el sector privado.

Además de los stands de las galerías se exhibirá una propuesta de la galería invitada OHNO, de CABA; habrá una muestra patrimonial, conversatorios, un Circuito de Museos y Galerías, presentaciones de libros y mesas editoriales. La programación de música en vivo contará el viernes 24 a las 20, con Electric Groove Trío, en la Sala Ariel Ramírez; el sábado a las 21.30, +Fiesta en Tribus Club de Arte (República de Siria 3572), con Lalalas, de Rosario, Cinturón de Bonadeo y Vulva Soul, de Santa Fe. El cierre musical será el domingo a las 20 horas, con Dani Umpi (Uruguay), en la Sala Ariel Ramírez.

Junio en la Marechal

El ciclo del Teatro Municipal “1º de Mayo”, continúa hasta fin de mes con una programación de espectáculos locales para conmemorar la habilitación de la sala que lleva el nombre del poeta y dramaturgo Leopoldo Marechal, un 28 de junio pero de 1973. El viernes 24 a las 21 llega “Electroiman”. El grupo escénico Res vuelve a los escenarios con esta tercera entrega de su trilogía, precedida por “Man” y “Mandojo”, codirigida por Arthur Bernard Bazin, de Francia; y Juan Berrón, de Santa Fe, también responsables de la coreografía. Con un proceso de ensayos híbrido, entre lo presencial y lo virtual, “Electroiman” lleva a escena ocho intérpretes que forman una red, como metáfora de nuestro mundo subjetivo.

El grupo de intérpretes está integrado por Camila Tacca, Antonio Rocha, Gabriel Paredes, Ignacio Francia, Lucía Garofalo, Sergio Coronel, Ariel Godoy  y Gastón Onetto. La música original es de Esteban Coutaz; el vestuario, de Karina Budassi (Hue Indumentaria);  las luces de Pablo Vallejo. La producción cuenta con el apoyo del Instituto Nacional del Teatro. Las entradas generales para esta función se venden a $300; mientras que para la función del sábado 25, también a las 21 y en la Sala Marechal, cuestan $700.

El domingo 26 a las 16, se presenta para toda la familia “Dolly y el Hombre Sombra (Relato de dos mundos)”, con dramaturgia, puesta en escena y estética general de Soledad Maglier. Con recursos que incluyen proyecciones, sombras, juegos de luces led y telas para dividir el espacio escénico en dos mundos, una niña narra el sueño en el que conoce al Hombre Sombra. En esa aventura también se encontrará con otros personajes que no son lo que dicen ser, aunque contará con la protección de una guardiana. El elenco está integrado por Lola Maglier, Alejandro Fort Villa, Celeste Medrano, Candelaria Rivoira, Leonardo Zilonka, Carolina Milne y Simón Sarnari. Las entradas generales están a la venta a un valor de $300.

Por otro lado, el miércoles 29 de junio a las 18 y a las 19, se presenta “Teatro Adentro. Una aventura tras bambalina”, del elenco de Teatro de Títeres Municipal. Mónica y Robert Von Theatre son los personajes que guían al público entre risas y suspenso, por las distintas salas y misterios del Teatro Municipal. La entrada es gratuita, con capacidad limitada. Se entregarán hasta dos pases por persona en la boletería del Teatro ubicado en San Martín 2020. Menores de 3 a 6 años pueden asistir sin entrada. 

Ballet Nacional de Ucrania, en dos funciones

Después de agotar las localidades para este jueves 23 a las 21, el Ballet Nacional de Ucrania dará una nueva función ese día, pero a las 18.30, también en la Sala Mayor del Teatro. La compañía llega por primera vez a la Argentina con “Giselle”, considerada como una obra maestra del ballet romántico, tanto por el tratamiento de los ideales románticos, como por el empleo de la más refinada técnica teatral del siglo XIX. En su interpretación participan la primera bailarina del Teatro Académico Nacional de Ópera y Ballet de Ucrania, Kateria Kukhar; y el bailarín principal del mismo recinto y director artístico del Gran Ballet de Kiev, Oleksandr Stoianov. El elenco está integrado por destacados artistas, graduados de escuelas de coreografía de alto nivel, en su mayoría de la Escuela Coreográfica Estatal de Kiev. 

La gira de la compañía comenzó en Polonia, Francia y Noruega; y en Argentina, el itinerario incluye escenarios de Córdoba, Rosario, Salta, Tucumán y Mendoza. Como explican desde la producción, en este tour, el reconocido ballet decidió no bailar “El lago de los cisnes”, de origen ruso, “por respeto al pueblo ucraniano y a los fallecidos que ha dejado la guerra”. De igual forma, anticiparon que “se podrá disfrutar de la técnica, los deslumbrantes vestuarios, y del arte que produce la compañía líder en ballet mundial a través de la danza”. 

Las plateas y palcos bajos, se venden a $5000; los palcos altos, $4000; tertulia tercio medio, $3000; generales, $2000. Se pueden adquirir en la boletería ubicada en San Martín 2020 y en plateavip.com.ar.

Mi ciudad como Turista

El sábado 25 a las 16, la Municipalidad invita a vecinos y vecinas a sumarse al recorrido guiado por la Costanera Oeste. El punto de encuentro será en el Faro, y como ya es costumbre los guías podrán ser identificados por las sombrillas verdes.

La Manzana Jesuítica, ubicada en San Martín 1540, se puede recorrer de miércoles a sábado de 9.30 a 12 y de 16 a 19; con visitas guiadas regulares a las 9.30, 11, 16 y 17 horas.

Todos los paseos de la propuesta Mi Ciudad como Turista son gratuitos y cuentan con audioguías bilingües para que cada persona realice el recorrido a su tiempo. Las audioguías se pueden descargar desde la web del municipio o escuchar en el perfil de Spotify.

Canto Libre en Concierto

La agrupación musical Canto Libre -Coro y pequeña orquesta santafesina- que dirigen los profesores Silvia Alejandra Martini y Daniel Arturo Sánchez darán su concierto “La Película”, el sábado 25 a las 20.30, en la Sala Mayor. En esta puesta celebran sus 25 años, con la Orquesta Sinfónica Juvenil de la Provincia de la Escuela de Música Nº 9901, que dirige Manuel Marina, como invitada especial.

La dirección musical estará a cargo de Ignacio Sánchez (arreglos musicales y corales); la puesta en escena, de Cecilia Castro; la edición del documental, de Amiga Audiovisual; mientras que Gian Franco Barbotti será el maestro de ceremonias. 

El espectáculo es organizado por la Asociación Cultural Amigos de Canto Libre; con el apoyo de la Municipalidad de Santa Fe, a través de la Secretaría de Educación y Cultura; y  el Ministerio de Cultura de la provincia. Las entradas generales están a la venta en la boletería del Teatro, a un valor de $1000.

En el cierre de este fin de semana, se realizará la Gala Candance Evolutions. Será el domingo 26 a las 18, en la Sala Marechal. Las anticipadas se venden a $600; y el día de la función, se abonan $800.

Presentación de “Hacer la América”

Las actividades en la Sala Mercado Editorial (Mercado Progreso, Balcarce 1635) continúan en el marco de la exhibición “Ediciones santafesinas 1939 – 1979”, que reúne ejemplares representativos de la producción de ese período para recorrer la literatura santafesina y reconocer el legado de los proyectos editoriales.

El jueves 23 de junio a las 18 se presentará el ensayo “Hacer la América”, de Susana Andereggen, que recibió una mención en el Premio Literario Municipal 2020 y que fue publicado por Editorial De l´aire, Santa Fe. Estará a cargo de Sandra Franzen, con la participación de Claudia Karina Rosciani y Susana Merke. La obra “nos introduce en diferentes aristas del proceso inmigratorio desde una ingeniosa propuesta, tomando objetos que pertenecieron a los antiguos colonos y que se conservan en el seno de sus familias. A través de ellos, el ensayo intenta reconstruir las subjetividades que movilizaron a estos inmigrantes a cruzar el océano Atlántico. La escritura, en pos de recuperar dicha subjetividad, se entreteje apelando a discursos literarios, históricos, sociológicos, psicoanalíticos, en los que se retoman además, voces de distintos autores y de la tradición oral.

El ciclo de presentaciones de libros sigue el jueves 30 de junio, con la novela “Supernova” (Editorial Contramar, 2021), de Diego Oddo, acompañado por Susana Ibáñez y Pablo Cruz.

Feria vintage y música en vivo

“Sobre ruedas: Vintage y Urbano en el Mercado” es la propuesta para el sábado 25 de junio, entre las 15 y las 20, en el patio del Mercado Progreso. La Feria del Park dará marco a todo el evento, que contará con la música en vivo de Tomajök y de Insolente Crew. Juguetes, cultura urbana, stickers, growshops, skate shops, ropa vintage y second hand, son algunos de los objetos que reúne esta feria, que ya tiene un año de existencia y que se fue consolidando en el espacio público, en particular en Candioti Park. En ese tiempo, sus organizadores destacan que la feria se fue convirtiendo en una comunidad vintage urbana muy interesante y con mucha proyección, que profundiza su concepto, lo amplía y enriquece, sumando incluso a feriantes de Paraná.

Tomajök es el proyecto y nombre artístico de Tomás Del Porto, que se inició a fines de 2019 y cuenta con un formato banda conformado por Tomás Del Porto en voz, guitarra y sintes, Pilar Ferrando en bajo, Lucía Ciccazzo en batería y Agostina Firpo en voz. Sus shows tienen carácter performático, con intervenciones de personajes con máscaras de animales. Tienen publicado un single y un EP de cinco canciones. Su lanzamiento más reciente es la canción “Manipuleiyon”, que fue publicada junto a su videoclip.

Ans (Agustin Aguero) y Pit Beat (Maximiliano Bianchini), integrantes de Insolente Crew, presentarán oficialmente “Cold Hands”, el último disco de ocho temas de Ans (el anterior fue “Pure State”); y un set de Insolente Pit, creador del grupo y BeatBoxer profesional.

Festival interdisciplinar

Collage emocional es el nombre del encuentro que convocará el domingo 26, de 15 a 19, también en el espacio de Balcarce 1635. La propuesta incluye una feria de ilustradores y editores, poesía, música en vivo y danza. Antonia Mercado, María Victoria Rittiner Basaez, Pablo Ignacio Ferreira y Gabriel Vaschetto, se unen para crear un ambiente sonoro, apoyados en sus distintas formaciones y experiencias artísticas. 

Con lecturas participarán la paranaense Beverly, docente, dedicada a la música, el teatro y a la poesía, con experiencia y premios en distintos slam y torneos de poesía oral; y la artista visual Florencia Carreras, que escribe poesía desde el año 2015, con trabajos publicados y una novela en proceso.

La programación de este festival interdisciplinar también contará con las bailarinas, intérpretes, estudiantes y docentes de danza contemporánea: Cecilia Tolosa, Ludmila Grillo, Luisina Vottero, Milagros Tourn y Valeria Fontanetto. 

El cierre del encuentro será con el grupo de free dance instrumental, Galindez, que integran Esteban Coutaz en sintetizadores y piano, Aníbal Chicco en bajo, y Martín Margüello en batería y guitarra eléctrica. Con un repertorio propio de temas instrumentales diversos, invitan al movimiento, al baile y al trance, en recitales donde la improvisación adquiere un rol determinante.

Museos y salas, con entrada gratuita

– Centro Experimental del Color (Bv. Gálvez 1150). “Reprogramación Obsolética”, con la coordinación de Agustín Miguez. Producciones de Nazapig, Campi Helwig, Pablo Brandolini, Ainara Iungman y Champurria Digital, que participaron del taller Toma La Píldora Roja, de Proyecto Trucha. Miércoles a viernes de 9.30 a 12.30; sábados, domingos y feriados, de 16 a 19 horas. Sábado 25 de junio a las 20 horas, activación en el marco de +Feria.

\-Museo Municipal de Artes Visuales “Sor Josefa Díaz y Clucellas” (San Martín 2068). “Trazas de un jardín”, de Jesica Bertolino, con el acompañamiento curatorial de Daniela Arnaudo; y “Señales abstractas de especies futuras”, de Laura Benech. Miércoles a sábados, de 9.30 a 12.30 y de 16 a 19; y domingos y feriados, de 16 a 19. Todos los jueves a las 17 se concreta un recorrido comentado con Jesica Bertolino, quien realiza un mural como parte de su muestra.

\-Museo de la Constitución Nacional (Avenida Circunvalación y 1º de Mayo). Jueves y viernes, de 8.30 a 12.30 y de 14 a 18; sábados y domingos, de 10.30 a 12.30 y de 15 a 18; y feriados, de 15 a 18. En la Sala Cero se encuentra la muestra “A la deriva (memorias que son ríos, ríos que son memorias)”, de Pablo Cruz y Celeste Medrano, con curaduría de Leandro Calamante.

\-Sala Mercado Editorial (Mercado Progreso, Balcarce 1635). “Ediciones santafesinas 1939 – 1979”. Miércoles, de 17 a 19; los jueves y viernes, de 9 a 13; y los domingos, de 17 a 21.

\-Sala Ariel Ramírez (Estación Belgrano, bulevar Gálvez 1150, planta alta). Sábados y domingos, de 16 a 19 horas, con visitas mediadas cada 30 minutos.