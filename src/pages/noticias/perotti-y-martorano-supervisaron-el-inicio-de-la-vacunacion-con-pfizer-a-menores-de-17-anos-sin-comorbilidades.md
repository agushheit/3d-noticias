---
category: Estado Real
date: 2021-09-24T06:00:48-03:00
thumbnail: https://assets.3dnoticias.com.ar/VACUNA17AÑOS.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Perotti y Martorano supervisaron el inicio de la vacunación con Pfizer a
  menores de 17 años sin comorbilidades
title: Perotti y Martorano supervisaron el inicio de la vacunación con Pfizer a menores
  de 17 años sin comorbilidades
entradilla: Fue este jueves, en La Esquina Encendida, en la ciudad capital. “Santa
  Fe ha superado el 53% de la población vacunada con dos dosis y vamos a seguir incrementando
  ese porcentaje”, afirmó el gobernador.

---
El gobernador Omar Perotti y la ministra de Salud, Sonia Martorano, recorrieron este jueves el vacunatorio ubicado en La Esquina Encendida, en la ciudad capital, en el marco del comienzo de la vacunación con Pfizer a menores de entre 12 y 17 años sin comorbilidades.

El operativo de vacunación se inició con 950 turnos para los jóvenes de 17 años, nacidos entre los meses de septiembre y diciembre de 2003. El martes llegaron a la provincia 19.890 dosis de la vacuna, de las cuales 5.890 se destinaron a la ciudad de Santa Fe.

En ese marco, Omar Perotti sostuvo que “la Nación había planteado que alcancemos el 50% de la población con dos dosis y la provincia de Santa Fe ya lo ha superado con el 53%, y vamos a seguir incrementando ese porcentaje”, aseguró.

Asimismo, el mandatario destacó “la importancia de continuar con el proceso de vacunación; del despliegue que realizamos en todo el territorio de la provincia para localizar a aquellas personas que aún no se han vacunado, por estar en situación de calle o porque, debido a su actividad, están en parajes muy alejados. Y por eso la monodosis está destinada a ese sector de la población específico, para barrer el territorio, sumando más santafesinos y santafesinas vacunadas”, explicó Perotti.

Por su parte, la ministra de Salud, Sonia Martorano, señaló que “a seis meses de haber comenzado este mega operativo de vacunación con los mayores de 90 años, hoy estamos aplicando dosis a los de 17 años”. Y explicó que Pfizer “tiene una logística muy compleja; no solo con la cadena de frío, que son -80°C, sino también en la preparación, por lo que estamos trabajando en la capacitación de los equipos territoriales”.

En ese sentido, la funcionaria mencionó que “hoy tenemos personal de vacunación del centro-norte provincial, que están desde muy temprano para recibir la capacitación in situ”.

Además, la titular de la cartera sanitaria remarcó que hoy en la provincia “tenemos el 53% de la población general con esquema completo de vacunación, con las dos dosis, lo que nos permite tener una situación epidemiológica diferente”, y recordó que “la ocupación de camas está por debajo del 60%; y la positividad de los testeos, en el 4%, destacando que “nunca tuvimos niveles tan bajos”. También, Martorano consignó que “los casos positivos de los últimos 14 días, menos de 100, dan cuenta además de que es una situación buena, que va de la mano de la vacunación y de seguir cuidándonos”.

**Seguir con los cuidados**

Consultado sobre los anuncios del gobierno nacional sobre mayores flexibilizaciones, el gobernador Perotti señaló: “Vamos a estar analizando, queremos recoger miradas y tomar las opiniones del grupo de expertos y de los directores de hospitales" que asesoran desde el comienzo de la pandemia al gobierno. "En los próximos días nos vamos a reunir para definir cómo seguir", adelantó el mandatario.

En ese marco, el gobernador destacó que “en cada provincia tenemos margen y libertad para ver cómo es el escenario y analizar la situación de cada actividad. En Santa Fe trabajamos con la opinión de los expertos, siempre fuimos responsables y garantizamos los cuidados y el nivel de actividades", aseguró Perotti, al tiempo que agregó que eso se ve reflejado "con los números que muestran a la provincia encabezando la recuperación económica. No nos vamos a apartar de ese camino y no vamos a entrar en polémicas innecesarias. Cada provincia conoce su realidad", afirmó.

Y continuó: “Hay hábitos que queremos sostener, manteniendo los cuidados de nuestra población. Esto, junto con el esquema de vacunación, nos permitió mantener este cuadro sanitario y nos tiene que posibilitar tener más actividades, que es lo que seguramente va a contemplar el próximo decreto. Y más que nuevas actividades, quizás, mayores márgenes de asistencia a muchas de ellas”, aclaró Perotti.

En esa línea, el gobernador mencionó que también “el clima nos brinda más posibilidades al aire libre y es un elemento a tener en cuenta y a sumar en el número de actividades y de asistentes en cada una de ellas”.

Por último, respecto de la decisión a nivel nacional de flexibilizar el uso del tapabocas al aire libre, el mandatario santafesino opinó que “cuando alguien está desarrollando una actividad aeróbica, deportiva y demás, entendemos que, efectivamente, tiene que estar esa posibilidad. Creo que todos tenemos que seguir con el barbijo. Nos incorpora un cuidado y una señal de que no bajamos la guardia, a pesar de que las cosas van mejorando y fruto de la vacunación, no del azar. Sí me parece correcto separar en qué actividades usarlo y en cuáles no. No obstante, tener el barbijo siempre con nosotros es una señal de protección, de cuidado personal y del tercero”, finalizó el gobernador Perotti.