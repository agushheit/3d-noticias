---
category: Agenda Ciudadana
date: 2021-08-25T06:15:30-03:00
thumbnail: https://assets.3dnoticias.com.ar/INCENDIOS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Los incendios en las islas seguirán en los próximos días y convocan a más
  brigadistas '
title: 'Los incendios en las islas seguirán en los próximos días y convocan a más
  brigadistas '
entradilla: Desde Protección Civil pedirán el uso de aviones hidrantes para frenar
  la multiplicación de los focos en los humedales. Afirman que los incendios se mantendrán
  de no cambiar el tiempo.

---
Ya transcurre el quinto día desde la aparición de distintos focos de incendio en los humedales cercanos a la ciudad de Santa Fe. Desde la Secretaría de Protección Civil de la provincia trabajan con un helicóptero hidrante sobre las zonas afectadas.

Además, convocaron a brigadistas de Bomberos Voluntarios por falta de recurso humano en los operativos y se espera que los incendios en las islas se repitan durante los próximos días, con la posibilidad de sumar aviones hidrantes.

En dialogo Roberto Rioja, secretario provincial de Protección Civil, quien está a cargo de los trabajos para sofocar los incendios en los humedales aledaños a Alto Verde. Al ser consultado por cómo es la situación en las islas, manifestó: "El helicóptero hidrante está trabajando hace cinco días y ya estamos trabajando con brigadistas adentro de la isla".

Los principales focos ignífugos se registran en islas cercanas a Santa Fe, que amaneció con cortinas de humo traídas con el viento intenso proveniente del sureste, constante en toda la jornada. Afirmaron que la cantidad de puntos con presencia de fuego "va variando", agregando Rioja que "esta (por las 16) es la hora más complicada y después del mediodía se empieza a notar".

"Nosotros empezamos a trabajar en los focos que ya veníamos trabajando el día anterior, pero siguen apareciendo nuevos focos por el viento que hay", afirmó el referente de Protección Civil. La cantidad de incendios varía entre los dos y los ocho puntos en donde se generan las grandes cortinas de humo.

La brigada que está trabajando en las islas se encuentra sobrepasado y al respecto Rioja afirmó: "Al cierre vamos a evaluar si tenemos que ampliar la cantidad de brigadistas y si tenemos la posibilidad de trabajar con aviones hidrantes dependiendo del lugar. Mañana a la mañana tendremos una evaluación para saber cómo vamos a seguir trabajando en esto".

A los 14 brigadistas que conforman la brigada provincial del manejo del fuego está previsto sumar a las tareas personal de Bomberos Voluntarios, quienes "ya se los puso en alerta en la medida en la que vayan apareciendo más puntos de incendio", según esbozó Rioja. Desde el sábado por la mañana ya funciona la base operativa de la brigada en el Puerto de Santa Fe.

Sobre la presencia de humo y el fuerte olor presente en distintos barrios de Santa Fe, el secretario de Protección Civil afirmó que "la rotación del viento provoca eso". Y agregó: "en la isla el viento provoca que el incendio se vaya intensificando. Seguiremos trabajando y seguramente mañana tendremos aún más trabajo. Si siguen estos vientos y al estar tan seco todo seguramente debemos esperar varios días más con este problema".

Actualmente la secretaría dependiente del gobierno provincial está trabajando en conjunto con el Ministerio de Medioambiente y Cambio Climático monitoreando la situación, además del trabajo con Nación y la cartera ambiental de la provincia de Entre Ríos.