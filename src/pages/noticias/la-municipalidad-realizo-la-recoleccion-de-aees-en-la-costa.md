---
category: La Ciudad
date: 2021-07-20T06:00:54-03:00
thumbnail: https://assets.3dnoticias.com.ar/AEES.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Municipalidad realizó la recolección de AEEs en la Costa
title: La Municipalidad realizó la recolección de AEEs en la Costa
entradilla: Este sábado se llevó a cabo una nueva edición de la campaña de Aparatos
  Eléctricos y Electrónicos en desuso.

---
La Municipalidad concretó este sábado, la quinta edición de la campaña de recolección de Aparatos Eléctricos y Electrónicos en desuso (AEEs). A pesar del frío intenso que se registró por la mañana, los vecinos y las vecinas de la ciudad se acercaron al Distrito de la Costa, donde la Secretaría de Ambiente organizó el operativo para recepcionar los artefactos.

Con el objetivo de garantizar el cumplimiento de los protocolos establecidos en el marco de la pandemia de Covid-19, se estableció un circuito para vehículos y peatones que se acercaban a concretar la donación. En esta oportunidad se recolectaron 1.300 kilos, que se suman a lo juntado en las ediciones anteriores, haciendo un total de más de 12 toneladas desde abril.

Diego reside en la zona de Las Paltas de Colastiné y llegó al punto de recepción de residuos eléctricos y electrónicos con celulares, teléfonos viejos y computadoras. “Viene bien, porque dejarlos en la calle genera suciedad y son tóxicos los residuos, así que me parece una idea genial del municipio”, cuenta después de haber recibido semillas para huerta y chips de leña. “Con mi pareja ya tenemos huerta en casa y la vamos a ampliar con todo esto. Es también una muy buena iniciativa para uno mismo generar sus propios alimentos saludables y evitar tantos tóxicos”, adujo.

**Responsabilidad**

El secretario de Ambiente del municipio, Edgardo Seguro, resaltó la importante adhesión que generó la campaña entre los santafesinos y las santafesinas, y opinó que es una muestra de la responsabilidad de la ciudadanía. En ese sentido, mencionó que “la gente trae equipos que han quedado en desuso; monitores viejos es lo más voluminoso que tienen en sus hogares. Pero hay equipos de computación que todavía funcionan en parte; estamos viendo de dárselos a las instituciones, a personas que no tienen cómo comprarlos”.

El sábado, la gente de la costa que se acercó a dejar los aparatos, recibió semillas y el calendario de huerta, pero también se llevó chips que son producidos en Colastiné, en la planta de reducción de restos de poda del municipio, que permite, por un lado, no llevar tantos residuos verdes para relleno sanitario y, por otro, no usar tantos camiones. “El año pasado hubo más de 2.000 camiones que fueron al relleno sanitario y ahora es uno por día, no 20 diarios como se hacía antes. El resto se procesa acá, recuperamos leña, chips, que se entregan a las personas o usados en las plazas de la ciudad”, explicó el funcionario.

En el Distrito de la Costa, además, hay campanas fijas de recepción de residuos secos. “Estuvimos viendo cómo la gente las tiene incorporadas: llegan con sus autos y dejan los residuos secos acá. Luego, con nuestros camiones estamos sacando los residuos secos de las campanas”, indicó.

**Procesamiento**

Cabe recordar que el material recaudado se traslada a la planta procesadora ubicada dentro del complejo ambiental. Allí se separan los artefactos que pueden repararse de aquellos que ya no sirven. En un paso posterior, se desarman los que van a la disposición final, con el objetivo de reciclar algunas piezas que tendrán utilidad.

Los aparatos eléctricos y electrónicos de las anteriores campañas ya fueron procesados. “Los residuos fueron dispuestos adecuadamente, lo que no era peligroso fue al relleno sanitario y lo peligroso, fue despachado al relleno de seguridad. Pero nos quedó un volumen importante, más de un 60% que es recuperable, como cobre, hierro y aluminio que fue la Asociación Civil Dignidad y Vida Sana, y que forma parte de su forma de subsistir. Por otra parte, en la planta trabajan seis personas de la asociación, como parte de nuestro trabajo con las asociaciones civiles, que nos permiten contribuir al ambiente, pero también lograr una sostenibilidad económica y social”, amplió Seguro.

Finalmente, invitó a todos aquellos que posean aparatos eléctricos y electrónicos en desuso a acercarse el próximo 7 de agosto al Distrito Noroeste (Teniente Loza 6970) donde se dará continuidad a las acciones de recolección.