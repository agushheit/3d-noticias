---
category: Agenda Ciudadana
date: 2021-06-26T06:00:48-03:00
thumbnail: https://assets.3dnoticias.com.ar/PRORROGA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Una dosis de Sputnik V o AstraZeneca reducen entre 70% y 80% la mortalidad
  por coronavirus
title: Una dosis de Sputnik V o AstraZeneca reducen entre 70% y 80% la mortalidad
  por coronavirus
entradilla: " Carla Vizzotti explicó que “la primera dosis genera casi el 80 por ciento
  de la inmunidad, la segunda, en general, eleva esa respuesta y la hace más durable
  en el tiempo”."

---
Una dosis de las vacunas Sputnik V y AstraZeneca reducen entre un 70% y un 80% la mortalidad por coronavirus, según resultados preliminares de un estudio realizado por el Ministerio de Salud de la Nación sobre una muestra de más de 400.000 personas mayores de 60 años, según se indicó oficialmente.

El trabajo "da cuenta de la efectividad de las vacunas y avala la estrategia argentina de priorizar la aplicación de la primera dosis para alcanzar a mayor número de la población", indicó un comunicado de la cartera de Salud.

En el caso de contar con el esquema completo de vacunación, el análisis concluye que la disminución de la mortalidad con cualquiera de esas vacunas supera el 90% en este grupo.

"La primera dosis genera casi el 80% de la inmunidad, la segunda, en general, eleva esa respuesta y la hace más durable en el tiempo", explicó la ministra de Salud, Carla Vizzotti, quien sostuvo que la estrategia de inmunización de diferir la segunda dosis para cubrir a la mayor cantidad de población de riesgo en el menor tiempo posible "fue una decisión adecuada y los resultados de este estudio confirman que se ha logrado una alta protección en la población vacunada".

Esa metodología adoptada por la Argentina en marzo pasado fue llevada adelante también por países como Canadá, Reino Unido y otras naciones de Europa.

Sin embargo, Vizzotti sostuvo que se van a "seguir completando todos los esquemas de vacunación de todas las personas con todas las vacunas", tal como quedó definido "desde el primer momento para la estrategia argentina" y explicó que "se monitoreará la evidencia científica y la situación local para tomar decisiones oportunas".

**La campaña de vacunación**

La campaña de vacunación contra el coronavirus en el país comenzó a finales del año pasado y de acuerdo a los últimos datos oficiales ya se aplicaron 19.205.808 de dosis (15.374.817 con la primera dosis y 3.830.991 con esquema completo).

Para este viernes se espera el arribo de una nueva partida de 1.181.500 vacunas AstraZeneca, con lo que se llegará a casi 25 millones de dosis desde el inicio de plan de inmunización.

En tanto, dos aviones de Aerolíneas Argentinas despegarán este fin de semana desde Buenos Aires hacia Moscú para buscar vacunas de los dos componentes de la Sputnik V, y la semana próxima comenzará a fabricarse en el país la dosis complementaria del inmunizante de origen ruso.

**El estudio**

El estudio realizado con 471.682 personas de 60 años o más a las que se les aplicó una o dos dosis de vacuna Sputnik V o de AstraZeneca reviste importancia para evaluar la efectividad de las vacunas contra la Covid-19 considerando las variantes circulantes, principalmente la Alpha oriunda del Reino Unido, Gamma identificada en Manos, y Lambda (Andina).

Y especialmente "el nivel de circulación viral entre otras variables analizadas 'en la vida real'" a diferencia de la eficacia evaluada en contextos de ensayos clínicos, indicó el texto oficial.

Según el último informe de vigilancia genómica de SARS CoV-2 difundido por el Ministerio de Salud el pasado miércoles, la variante Gamma es la predominante en el país.

En base a 1.077 muestras de personas sin antecedentes de viaje a destino internacional secuenciadas por el Instituto Nacional de Enfermedades Infecciosas (INEI) - Anlis "Dr. Carlos Malbrán", las que más prevalecen en todas las regiones de la Argentina son Gamma, con el 41%; Lambda, con el 37%; y Alpha, con el 14%.

Además, indicó que, del total de las muestras secuenciadas, cerca del 52% corresponden a "variantes de preocupación" por sus características de ser más contagiosas.

**Estrategias**

En marzo pasado, el Ministerio de Salud junto a las 24 jurisdicciones del país y expertos de la Comisión Nacional de Inmunizaciones (CoNaIn) consensuaron priorizar las primeras dosis de vacuna contra la Covid-19 y diferir la aplicación de las segundas dosis para tener mayor cantidad de personas vacunadas en el menor tiempo posible.

Además, acordaron aplicar la segunda con un intervalo mínimo de 12 semanas, con el compromiso de reevaluar la estrategia cumplido ese lapso.

El próximo miércoles la CoNaIn volverá a reunirse para definir los pasos a seguir en el contexto actual.

El subsecretario de Estrategias Sanitarias, Juan Manuel Castelli, aclaró que la respuesta inmunológica de las vacunas se demuestra en los estudios de Fase I y Fase II con cada vacuna.

“El proceso de evaluar la respuesta inmunológica específica de una vacuna se realiza sobre una parte del virus, como por ejemplo la proteína S, por lo tanto, el estudio de anticuerpos para evaluar la respuesta no está recomendado porque es algo que se estudia durante el ensayo clínico previo”, explicó Castelli.

Los anticuerpos, como sucede con otras vacunas, pueden estar presentes o ausentes y eso no significa que no haya protección, y en este caso, la respuesta celular se activa e induce la producción de nuevos anticuerpos ante la exposición al virus.

Los resultados preliminares del estudio se presentarán al grupo de trabajo conformado por sociedades científicas y grupos de investigación que colaboran con el Ministerio de Salud en el análisis de la inmunidad relacionada a la Covid-19 y características virales.