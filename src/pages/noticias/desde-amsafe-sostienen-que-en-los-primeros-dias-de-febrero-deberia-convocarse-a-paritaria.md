---
category: La Ciudad
date: 2022-01-19T06:00:40-03:00
thumbnail: https://assets.3dnoticias.com.ar/amsafecap.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Desde Amsafé sostienen que "en los primeros días de febrero" debería convocarse
  a paritaria
title: Desde Amsafé sostienen que "en los primeros días de febrero" debería convocarse
  a paritaria
entradilla: 'Así lo precisó Rodrigo Alonso, secretario general de Amsafé La Capital.
  Afirmó que se exigirá "un salario que no pierda con la inflación y que permita recuperar
  lo perdido en los últimos años"

  '

---
Luego del cierre de la última paritaria docente entre los gremios y el ejecutivo provincial, que dispuso un 17% de aumento en tres tramos, desde Amsafé anunciaron que esperan una nueva convocatoria del gobierno.

Según la Asociación del Magisterio de Santa Fe la paritaria debería tener lugar "los primeros días de febrero", luego de que en enero los docentes hayan cobrado el aumento del 2% correspondiente al último tramo de la anterior paritaria.

 Al respecto Rodrigo Alonso, secretario general de Amsafé La Capital, quien consultado por la fecha en que desde el gremio consideraban adecuada para retomar la discusión paritaria afirmó: "El último acuerdo paritario con respecto al salario tiene una fecha de vencimiento para los últimos días de enero, que es cuando se percibe el último aumento acordado del 2%. Entendemos que los primeros días de febrero es el momento en donde deberían convocarnos para poder discutir no solo el salario sino todo lo que tiene que ver con las condiciones de trabajo".

La última paritaria salarial significó un aumento del 10% para el pasado mes de octubre (cobrado por docentes de Sadop y los docentes públicos por planilla complementaria), otro 5% en diciembre y un 2% restante que se cobró en enero. Con estos porcentajes, los maestros santafesinos percibieron un aumento anual del 52%, número casi similar a la inflación anual.

La polémica estuvo suscitada por el lapso de tiempo transcurrido hasta la aceptación de la propuesta, la cual fue ratificada en más de una ocasión por las carteras de Trabajo y de Educación. Incluso se postuló que aquellos días en donde se llevaron a cabo las tres jornadas de paro de parte de Amsafé sean descontados del salario.

**"Recuperar poder adquisitivo"**

Consultado por cual sería un porcentaje justo de incremento salarial, Alonso insistió en que "no solamente tiene que ser un salario que no pierda ante la inflación sino que también una recomposición salarial que nos vaya permitiendo recuperar lo que hemos perdido en los últimos años".

"Nosotros decimos que cualquier discusión no puede atravesar la necesidad que tenemos los trabajadores y las trabajadoras de mejorar el poder adquisitivo del salario. Creemos que este 2022 tiene que ser un año en donde los docentes debemos empezar a recuperar el poder adquisitivo del salario y con ese objetivo iremos a discutir en el ámbito paritario", concluyó.