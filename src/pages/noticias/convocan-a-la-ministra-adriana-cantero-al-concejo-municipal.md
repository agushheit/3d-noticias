---
category: Agenda Ciudadana
date: 2021-05-29T06:00:13-03:00
thumbnail: https://assets.3dnoticias.com.ar/garibaldi.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Convocan a la ministra Adriana Cantero al Concejo Municipal
title: Convocan a la ministra Adriana Cantero al Concejo Municipal
entradilla: El concejal Paco Garibaldi, presentó un proyecto donde se convoca a la
  Ministra de Educación para que asista al recinto del HCM, donde pueda brindar una
  explicación general sobre el desarrollo del plan educativo.

---
En el marco de la segunda ola de Covid-19 y a más de 15 meses de iniciada la pandemia en Argentina, y ante los grandes desafíos que presenta el dictado de clases mientras se atraviesa esta crisis sanitaria, la convocatoria al diálogo político se torna en una necesidad para toda la comunidad educativa.

Por esta razón, el Concejal de Santa Fe, Paco Garibaldi, presentó un proyecto donde se convoca a la Ministra de Educación de la Provincia de Santa Fe, Adriana Cantero, para que asista al recinto del HCM, donde pueda brindar una explicación general sobre el desarrollo del plan educativo que se prevé para el presente ciclo lectivo para la provincia y aporte información específica sobre lo proyectado para la ciudad de Santa Fe