---
category: La Ciudad
date: 2021-01-30T09:21:30Z
thumbnail: https://assets.3dnoticias.com.ar/bacheo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad de Santa Fe
resumen: El plan de bacheo 2021 está en marcha
title: El plan de bacheo 2021 está en marcha
entradilla: Este año, la Municipalidad reparará más de 13.000 m2 de calles deterioradas.
  Es un plan por fases, que prioriza las zonas de mayor circulación de la ciudad.

---
El Plan de Bacheo que presentó el intendente Emilio Jatón la semana pasada ya comenzó. El mandatario recorrió días atrás los trabajos que se llevan adelante en San Jerónimo y Presidente Illa. En este caso, se trata de la primera de cuatro fases que se llevarán a cabo.

Las obras en San Jerónimo y Presidente Illia se realizan con hormigón y se concretan con mano de obra municipal. También con hormigón se avanzó estos días en San Jerónimo y Monseñor Zaspe, en San Jerónimo e Illia y en Saavedra al 1200.

En paralelo, y producto de una licitación ya realizada, arrancará próximamente un bacheo asfáltico que comprenderá de sur a norte, calles 9 de Julio, San Martín, San Jerónimo y 4 de Enero.

Vale mencionar que se aprovecha la época de verano para reconstruir las calles de este sector que luego ve acrecentada su circulación cuando comienza el movimiento fuerte en instituciones gubernamentales y educativas ubicadas en el casco histórico.

**Micro y macrocentro**

Luego de la recorrida junto al intendente, Griselda Bertoni, secretaria de Obras y Espacio Público, anticipó que la semana próxima se comenzará a bachear una zona clave para la ciudad como es el micro y macrocentro. “Próximamente se iniciará una obra muy ambiciosa: con una inversión de casi 20 millones de pesos para bachear un importante sector de la ciudad”, destacó la funcionaria.

Las tareas de asfalto incluidas en la Fase 1 abarcan 4.500 m2 de calles a reconstruir: el sector está enmarcado entre bulevar Pellegrini y Gálvez (al norte), avenida Freyre y Dr. Zavalla (al oeste), Rivadavia (al este), y J.J. Paso (al sur).

Con respecto a los trabajos en el Sur de la ciudad, Bertoni indicó: “Esta zona era prácticamente intransitable. Se trata de un sector donde hay mucho tránsito, donde los vehículos tenían que pasar a muy baja velocidad por los desplaces que tenían las losas”.

“Cuando se trabaja con hormigón hay que levantar la losa entera”, explicó la funcionaria y añadió que por ese motivo “cuando se interviene se necesita mucho movimiento de máquinas como lo que estamos viendo”. Otra de las zonas donde se intervino fue en Monseñor Zazpe y Buenos Aires donde se hicieron trabajos de media calzada. Posteriormente, se concretarán reparaciones sobre calle Saavedra.

Cabe destacar que se trabajará en forma paralela con cuadrillas municipales y las empresas que ganaron las diferentes etapas con el objetivo de optimizar los trabajos.

**Todas las fases**  

Se repararán con hormigón y asfalto, según corresponda, más de 13.000 m2 de calles deterioradas, comenzando por las de mayor circulación. La Fase 2 incluye a todas las avenidas que van de Norte a Sur en la ciudad: General Paz, Peñaloza, Presidente Perón, Blas Parera y Aristóbulo del Valle (licitadas en la fecha), Facundo Zuviría, Almirante Brown, Freyre, López y Planes, bulevar Pellegrini-Gálvez. Estas tareas iniciarán en el mes de marzo.

En tanto, las fases 3 y 4 abordan el Noreste:: la zona comprendida entre las calles French, Padre Genesio, Urquiza y Talcahuano. Y Candioti Norte y Sur: la zona comprendida entre las calles Luciano Torrent, Avenida Alem, Belgrano y Grand Bourg.