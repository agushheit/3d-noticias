---
category: Agenda Ciudadana
date: 2022-01-06T06:15:36-03:00
thumbnail: https://assets.3dnoticias.com.ar/guzman.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: 'Argentina y el FMI enfrentados por la política para reducir el déficit:
  “No aceptaremos condicionamientos”'
title: 'Argentina y el FMI enfrentados por la política para reducir el déficit: “No
  aceptaremos condicionamientos”'
entradilla: "El ministro de Economía, Martín Guzmán, afirmó que las exigencias del
  organismo son una amenaza para la recuperación económica de la Argentina. Reclamó
  a la comunidad internacional que apoye la propuesta argentina. \n"

---
El Gobierno argentino jugó una carta fuerte: puso sobre la mesa las diferencias que mantiene con el Fondo Monetario Internacional (FMI) para lograr un acuerdo por la refinanciación de la deuda, acusó al organismo de impulsar políticas que atentarán contra la recuperación económica del país, y le pidió a la comunidad internacional que apoye la propuesta argentina.

En ese sentido, el ministro de Economía, Martín Guzmán, le pidió a la comunidad internacional que apoye la propuesta argentina.

"La diferencia entre lo que plantea el FMI y lo que plantea el Gobierno argentino consiste en diferenciar un programa de ajuste del gasto real, que detendría con seguridad la recuperación, versus un programa que le dé continuidad a la recuperación de la economía", señaló Guzmán.

Y enseguida explicó que "en el sendero fiscal no hay acuerdo", aunque no dio precisiones sobre las metas anuales de reducción del déficit fiscal que propone cada parte.

El funcionario presentó este miércoles el estado de las negociaciones con el FMI a gobernadores junto al presidente, Alberto Fernández, en un encuentro que se realizó en el Museo del Bicentenario.

Luego de las palabras de Guzmán, el jefe del Poder Ejecutivo reforzó la idea: “Para nosotros no es posible plantear una deuda que sea sustentable desde un ajuste. La palabra ajuste está desterrada para nosotros”.

De esta forma la Argentina blanqueó las diferencias con el FMI por la demora en cerrar un acuerdo, remarcando que la deuda a renegociar es producto de un crédito otorgado incumpliendo las normas del organismo.

Ante una consulta del gobernador de Chaco, Jorge Capitanich, el presidente afirmó que “no vamos a firmar un ajuste tarifario desmedido. No hay posibilidad de tarifazos en la Argentina. Se tendrán que corregir (las tarifas) de manera razonable.

Guzmán afirmó que “la Argentina propone bajarlo (el déficit) en forma gradual con la posibilidad que el Estado pueda favorecer las condiciones de producción. La Argentina propone reducir el déficit a través de crecimiento”. En inmediatamente acusó al FMI: “Lo que pide el FMI es diferente que lo que hemos presentado”.

“Lo que pide el FMI es diferente de lo que hemos presentado y la diferencia consiste en un programa que con alta probabilidad detendría el crecimiento, y es un programa de ajuste real, versus un programa de recuperación fuerte”, sentenció.

En ese marco, Guzmán lanzó una exhortación: “Es importante que estemos alineados en defender los intereses de la Argentina. Cada uno tiene que definir la camiseta que se quiere poner”.

“No es una cuestión de visiones sino de intereses en juego. Algunos pueden plantear un programa recesivo que baje el nivel de exportaciones y permita elevar el superávit comercial para pagar deuda (pero) primero que todo está asegurar la recuperación económica de la Argentina”, insistió.

Descripto el escenario, Guzmán le reclamó a la comunidad internacional que “apoye la posición de la Argentina”.

El titular de Hacienda sostuvo que "se tomó un deuda insostenible que sólo podía pagarse si había un shock de confianza y sabemos que nada de eso ocurrió. La situación económica empeoró de forma profunda", apuntó.

"Necesitamos poder refinanciar esas deudas", dijo, pero advirtió que el acuerdo debe ser "sin tener condicionamientos desde el pago al FMI desde las políticas. Es muy importante que tengamos la capacidad soberana de llevar adelante las políticas económicas de la Nación", sostuvo. "No existe un buen acuerdo, lo que existe es un acuerdo bueno en términos relativos", añadió.

Guzmán explicó que cuando el gobierno de Fernández asumió el 10 de diciembre de 2019 "el perfil de vencimientos de todo el sector público eran pagos por 23.000 millones de dólares en el 2020, casi 25.000 millones de dólares en el 2021 y casi 32.000 millones de dólares en los años 2022 y 2023".

De todas formas, ante la ausencia de números concretos la presentación quedó en grandes lineamientos sin detalles lo cual impide dimensionar la diferencia entre las propuestas.

El programa se basa en tres criterios de desempeño: sendero fiscal; sendero de reservas internacionales, financiamiento monetario de la política fiscal.

Sin acuerdo en el terreno fiscal, tampoco hay claridad en el financiamiento. Guzmán admitió que el único punto de acuerdo con el FMI es que el ritmo de crecimiento de las reservas sea de entre USD3.000 y 4.000 millones por año.

En forma lateral señaló que para los próximos vencimientos con el FMI se solicita la restitución de los DEGs.

El programa que la Argentina está negociando con el FMI será la base del plan plurianual prometido por el Gobierno.

"El acuerdo con el FMI no va a resolver todos los problemas de endeudamiento externo de la Argentina; hay mucho trabajo por hacer; es de tan magnitud el problema que va a llevar años resolverlo", añadió.