---
category: Agenda Ciudadana
date: 2021-10-06T06:00:10-03:00
thumbnail: https://assets.3dnoticias.com.ar/VACUNIÑOS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: La Sociedad Argentina de Pediatría avaló la decisión oficial de vacunar a
  niños contra la Covid-19
title: La Sociedad Argentina de Pediatría avaló la decisión oficial de vacunar a niños
  contra la Covid-19
entradilla: 'El presidente de la entidad, Omar Tabacco, explicó que el Ministerio
  de Salud les aportó la evidencia sobre la efectividad de las dosis de  Sinopharm
  en esa población. '

---
El presidente de la Sociedad Argentina de Pediatría (SAP), Omar Tabacco, avaló la decisión del Gobierno de vacunar contra el coronavirus a los niños y niñas de entre 3 y 11 años con dosis de Sinopharm, inmunizante al que recomendó luego de haber accedido a la evidencia científica en poder del Ministerio de Salud.  
  
"Queremos que los chicos se vacunen, para su seguridad, para llegar a la inmunidad de rebaño y para reforzar la presencialidad escolar. Sin lugar a dudas, recomendamos la vacuna", aseveró Tabacco en declaraciones formuladas esta mañana a Radio Nacional.  
  
En relación al reparo inicial que había expresado ayer la SAP, el especialista dijo que el anuncio realizado desde el Ministerio de Salud el viernes pasado, en relación a la vacunación de 6.000.000 de niñas y niños de entre 3 y 11 años "lo tomó por sorpresa", por eso la entidad se reunió con esa cartera para ver "cuál era la evidencia científica que ellos disponían, ya que no estaban publicadas".  
  
El aval de la SAP se produjo un día después de que el Consejo Federal de Salud (Cofesa) decidiera avanzar con la estrategia de vacunar contra el coronavirus a alrededor de 600.000 niños de 3 a 11 años.  
  
Durante la reunión del Cofesa, el lunes, las autoridades sanitarias de las 23 jurisdicciones, la Ciudad Autónoma de Buenos Aires y el Ministerio de Salud de la Nación, consideraron a la vacunación pediátrica como "un paso fundamental en el Plan Estratégico de Vacunación contra Covid-19".  
  
En ese contexto la ministra de Salud, Carla Vizzotti, destacó que “la vacunación pediátrica es un paso fundamental para la última etapa de control de la pandemia”.  
  
“De esta manera, no solamente tenemos una herramienta para proteger a los niños y niñas en forma individual, priorizando principalmente a quienes tienen factores de riesgo, sino que también cumple un rol fundamental para disminuir la transmisión del virus y fortalecer la presencialidad cuidada en las escuelas”, agregó.  
  
Sobre el fármaco, Vizzotti afirmó que “se trata de una vacuna de virus inactivada, que tiene plataforma conocidísima utilizada en vacunas como la de hepatitis A, polio inactivada o Salk y antigripal”.  
  
“Tiene estudios de fase I y II publicados en Lancet, tiene estudios de eficacia y de efectividad en mayores de 18 años, tiene datos robustísimos de seguridad y eficacia en Argentina con datos nuestros”, finalizó.  
  
En el marco de esa decisión oficial, Tabacco señaló que desde la cartera de Salud "nos mostraron el camino que ha recorrido la Anmat con las entidades regulatorias de China y el laboratorio productor de la vacuna, en la vigilancia de un estudio de la fase tres de la utilización de Sinopharm para este grupo etario y otros resultados estadísticos de un potencial rebrote de Delta".  
  
Y añadió que desde SAP "queremos que los chicos se vacunen, para su seguridad, para llegar a la inmunidad de rebaño y para reforzar la presencialidad escolar", ya que al revisar la evidencia científica, "sin lugar a dudas recomendamos la vacuna".  
Tabacco adelantó que el plan de vacunación "será de dos dosis como en los adultos y la iniciativa es vacunar activamente en las escuelas".  
  
En ese sentido, el Cofesa convino en iniciar el primer envío de las vacunas para niños y niñas durante el jueves 7 y viernes 8 de octubre, para iniciar la distribución a todo el país y definieron el próximo martes, 12 de octubre, para el lanzamiento nacional de la vacunación pediátrica en Argentina.  
  
Los propósitos de la vacunación "consisten en disminuir las poco frecuentes pero posibles formas graves de enfermedad y la mortalidad por Covid-19 en este grupo, así como la cantidad de personas susceptibles y de este modo la transmisión viral y fortalecer la presencialidad escolar", consideró el Cofesa.  
  
Las autoridades sanitarias acordaron en "considerar oportuno coadministrar las vacunas contra Covid-19 con otras vacunas del CNV (Calendario Nacional de Vacunación) para completar esquemas de vacunación acordes a la edad".  
  
Por su parte, el médico pediatra y directivo del Hospital Garrahan, Oscar Trotta, respaldó el lunes la inmunización contra el coronavirus para menores de entre 3 y 11 años con la vacuna Sinopharm, a la cual consideró "segura y efectiva".  
  
El pediatra consideró que la campaña de vacunación para esa franja etaria "es necesaria en virtud de que ese grupo poblacional era el que estaba siendo más vulnerable a infectarse con coronavirus y sobre todo con la variante Delta".  
  
"Sinopharm es una vacuna segura, de la misma característica y tecnología de producción que vacunas del calendario como la de hepatitis A", añadió Trotta.  
  
"Es efectiva, dando una gran respuesta inmunológica en el organismo, con muy pocos efectos secundarios", agregó.  
  
En tanto, los gobiernos provinciales comenzaron los operativos de preinscripción de menores para recibir la vacuna Sinopharm.  
  
En la provincia de Buenos Aires más de 400 mil niños y niñas en esa franja etaria se inscribieron durante el fin de semana para recibir la vacun[**a**](https://www.telam.com.ar/notas/202110/570596-300-mil-ninos-bonaerenses-inscripcion-vacuna.html), según informó el ministro de Salud del distrito, Nicolás Kreplak.  
  
"Yo no soy especialista, pero hoy a la mañana abrí el cajón de medicamentos y el único requisito que tengo para mí y para mi familia a la hora de aplicar un remedio o una vacuna es la aprobación de Anmat; siempre fue así", dijo ayer por la mañana el gobernador de ese distrito, Axel Kicillof, respecto de la autorización de ese organismo para que la vacuna Sinopharm sea aplicada a niños de entre tres y 11 años, que por esas horas había provocado reparos en la SAP.  
  
En la provincia de Misiones, el ministerio de salud provincial comenzó hoy con las preinscripciones para inoculación con Sinopharm a través de la App Alegramed para los niños 3 a 11 años.  
  
En el sur del país, el gobernador de Neuquén, Omar Gutiérrez, informó que en la provincia hay más de 3.000 dosis de vacuna Sinopharm "que las vamos a destinar rápidamente para que dé comienzo la vacunación pediátrica", y resaltó que "tenemos una lluvia de inscripciones" al detallar que unos 15 mil niños y niñas de entre 3 y 11 años ya se encuentran registrados para recibir la vacuna por lo que felicitó y agradeció "a las familias" que los anotaron.  
  
En la provincia de Tucumán, la cartera de Salud anunció que implementarán en las escuelas operativos de vacunación contra el coronavirus destinados a menores de 12 años.  
  
El viernes pasado, el ministro de Educación Jaime Perczyk sostuvo que "la vacunación tiene que ver con recuperar presencialidad plena desde el nivel inicial, recuperar tiempos y aprendizajes en la escuela y la normalidad creciente en el campo educativo".  
  
El funcionario agregó que la vacunación "aporta, también a recuperar a los chicos que se nos fueron del nivel educativo porque invitándolos a vacunarse van a volver a la escuela".  
  
Por su parte, la ministra de Salud, Carla Vizzotti, celebró la decisión oficial de sumar a los niños al Plan Estratégico de Vacunación: "La Argentina –dijo Vizzotti- termina el 2021 con toda su población de más de tres años cubierta y protegida. Contamos con el stock para iniciar esa vacunación y completar los esquemas", destacó.  
  
Para la funcionaria, en los próximos tres meses "cambiará la historia de la pandemia en la Argentin