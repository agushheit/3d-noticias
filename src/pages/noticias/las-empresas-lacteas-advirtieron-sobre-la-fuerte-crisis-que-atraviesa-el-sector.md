---
category: El Campo
date: 2021-01-27T10:07:17Z
thumbnail: https://assets.3dnoticias.com.ar/lacteos.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Con información de Infobae
resumen: Las empresas lácteas advirtieron sobre la fuerte crisis que atraviesa el
  sector
title: Las empresas lácteas advirtieron sobre la fuerte crisis que atraviesa el sector
entradilla: Aseguraron que los precios se encuentran en sus pisos históricos. Y que
  subieron menos que el resto de los productos de la canasta básica alimentaria

---
Las empresas lácteas advirtieron sobre la “profunda crisis” que atraviesa el sector y señalaron que son una de las más perjudicadas por el incremento de sus costos en comparación con la suba que registraron los precios de sus productos.

“Los precios de los lácteos se encuentran en sus pisos históricos. En promedio con el resto de los componentes de la canasta básica alimentaria su aumento fue significativamente menor. Ese descalce está entre los más profundos de toda la industria alimentaria de la Argentina”, explicaron fuentes de la industria.

> Desde las empresas lácteas advirtieron sobre la “profunda crisis” que atraviesa el sector y señalaron que son una de las más perjudicadas por el incremento de sus costos

Según las cifras oficiales difundidas por el Indec, los productos lácteos tuvieron durante 2020 un incremento de precios de 7,4%. Este porcentaje corresponde al promedio de aumentos autorizados por la Secretaría de Comercio Interior para los productos que formaban parte de los programas de Precios Máximos y Precios Cuidados.

Con todo, ese aumento promedio está por debajo del nivel general de la inflación (36,1%) y del correspondiente al segmento de alimentos y bebidas, que fue de 37,6% (según la canasta básica total GBA).

![La industria fue considerada entre los sectores esenciales durante la pandemia](https://www.infobae.com/new-resizer/Vvt4-TUvsxc52DorIU4kI8U-6oQ=/420x280/filters:format(jpg):quality(85)//cloudfront-us-east-1.images.arcpublishing.com/infobae/2B7TQF4FLFBOHGPW3WVVCPPNUQ.jpg)

Por otra parte, de acuerdo a la información de la industria, durante el año pasado el aumento de los costos fue de 49% en promedio. Este cálculo incluye logística, materia prima, paritarias y energía. “Todos estos rubros se incrementaron por encima de la inflación anual publicada por el Indec, de un 36,1%, mientras que el promedio, por ejemplo, en las leches fluidas en sachet fue del 5%”, señalaron.

“La mayoría de los productos lácteos se encuadran en el otro programa oficial que es Precios Máximos y la mayoría han permanecido congelados durante todo 2020. Esta situación de costos cercana al 50% y aumento promedio en precios de sólo 7% genera situaciones de quebrantos no sólo en las empresas sino también en toda la cadena”, explicaron desde el sector.

> Esta situación de costos cercana al 50% y aumento promedio en precios de sólo 7% genera situaciones de quebrantos no sólo en las empresas sino también en toda la cadena

También señalaron que al calcular un 49% de aumento en los costos, no se contempló el costo asociado al impacto de la pandemia de covid-19. El sector lácteo fue uno de los que formaron parte de las industrias consideradas esenciales. “Se garantizó el suministro de productos sin que las empresas recibieran ningún tipo de ayuda estatal, como los programas ATP o Repro”, indicaron.

“La situación actual se hace compleja e insostenible para muchas empresas que se dedican sobre todo a volcar la mayor parte de su producción en el mercado interno. De hecho, las compañías lácteas que cotizan en bolsa han arrojado sistemáticamente balances negativos durante todo el año pasado”, agregaron.

![Las empresas advierten sobre una posible caída en la producción](https://www.infobae.com/new-resizer/29z_vWjdPFdAomPbgpremtqT0c8=/420x236/filters:format(jpg):quality(85)//cloudfront-us-east-1.images.arcpublishing.com/infobae/SO6B5YPJ4RBVPCBLI7O2JQ5GBU.jpg)

En el sector ya se habla de que podría pasar algo similar a lo que ocurrió con la producción cárnica algunos años atrás, cuando se perdieron decenas de miles de vientres. Estiman que la baja rentabilidad puede hacer que también caiga la cantidad de vacas productoras de leche y que la agricultura crezca en detrimento del sector lácteo.

Desde la industria destacaron que del costo de un sachet de leche, en promedio, el productor tambero se queda con el 37% mientras que la industria lo hace con el 21% del precio de la góndola.