---
category: La Ciudad
date: 2021-09-14T06:00:45-03:00
thumbnail: https://assets.3dnoticias.com.ar/VACUPOSTPASO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Tras las elecciones, se retoma la vacunación en la ciudad de Santa Fe
title: Tras las elecciones, se retoma la vacunación en la ciudad de Santa Fe
entradilla: 'Con pocos turnos entregados habrá operativos de inoculación en el hospital
  Iturraspe Viejo, en el Alassia, en la Esquina Encendida y en La Redonda. Se aplicarán
  primeras y segundas dosis de AstraZeneca en Santo Tomé. '

---
Luego del domingo electoral que vivió la ciudad de Santa Fe, desde el Ministerio de Salud de la Provincia informaron a El Litoral cómo es el cronograma de vacunación para este lunes.

A diferencia de otras jornadas de inoculación, la de este inicio de semana es con la entrega de pocos turnos. En el hospital Iturraspe Viejo se otorgaron 150 turnos; 2.500 para la Esquina Encendida; 50 mayores de 12 años fueron citados para el hospital de niños “Orlando Alassia”; en La Redonda habrá una jornada especial para quienes reprogramaron su turno. Además, desde Salud mencionaron que en el Cemafe este lunes no habrá vacunación.

En la vecina ciudad de Santo Tomé para esta jornada se otorgaron 686 turnos para aplicar AstraZeneca, algunos recibirán su primera vacuna y otros su segunda dosis.