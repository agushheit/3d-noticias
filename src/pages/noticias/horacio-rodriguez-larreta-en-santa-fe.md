---
category: La Ciudad
date: 2021-07-05T06:15:33-03:00
thumbnail: https://assets.3dnoticias.com.ar/LARRETAENSTAFE.png
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Ceresola
resumen: Horacio Rodríguez Larreta en Santa Fe
title: Horacio Rodríguez Larreta en Santa Fe
entradilla: |
  “Juntos por el Cambio es con todos”: Larreta estuvo en Santa Fe y les brindó su apoyo a los
  concejales del PRO Luciana Ceresola y Sebastián Mastropaolo.

---
Este sábado, el jefe de Gobierno porteño y líder del PRO, Horacio Rodríguez Larreta, estuvo en Santa Fe para avanzar en las conversaciones sobre las candidaturas para las elecciones legislativas de este 2021.

Tras un almuerzo con referentes del espacio, como el excandidato a gobernador Miguel del Sel, Larreta se dirigió al local del PRO ubicado en Bulevar y Marcial Candioti para reunirse con los concejales de la ciudad de Santa Fe, Luciana Ceresola –que busca renovar este año su banca en el Concejo– y Sebastián Mastropaolo.

En el encuentro, Larreta se refirió al proyecto del espacio desde este año al 2023, cuando piensa postularse a la presidencia de la Nación y les brindó su apoyo a los concejales santafesinos para estas legislativas. Además, remarcó la necesidad del PRO de ganar en la provincia y de “sostener” a Juntos por el Cambio. “Juntos por el Cambio es con todos”, afirmó.

Los concejales del PRO de la ciudad, coincidieron en que la visita de Larreta fue muy positiva para el espacio y que seguirán trabajando para ampliar el espacio en la ciudad y la provincia de cara al 2023. Además, destacaron y agradecieron la presencia del jefe de Gobierno porteño en el local y se comprometieron a sostener el crecimiento que el espacio viene logrando en la ciudad.![](https://assets.3dnoticias.com.ar/LARRETA2.png)![](https://assets.3dnoticias.com.ar/LARRETA3.png)