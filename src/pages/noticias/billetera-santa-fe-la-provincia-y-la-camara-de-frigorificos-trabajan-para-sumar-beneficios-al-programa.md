---
category: Estado Real
date: 2021-01-30T09:26:43Z
thumbnail: https://assets.3dnoticias.com.ar/billetera.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: 'Billetera Santa Fe: La provincia y la Cámara de Frigoríficos trabajan para
  sumar beneficios al programa'
title: 'Billetera Santa Fe: La provincia y la Cámara de Frigoríficos trabajan para
  sumar beneficios al programa'
entradilla: El objetivo es incorporar nuevos sectores a la iniciativa de beneficios
  y descuentos que busca incentivar la demanda de bienes y servicios.

---
El secretario de Agrolimentos, Jorge Torelli, mantuvo este viernes un encuentro con representantes de la Cámara de Frigoríficos de Santa Fe, para intercambiar ideas y proponer medidas activas para colaborar con el Programa Billetera Santa Fe y sumar beneficios para consumidores y comerciantes usuarios de la herramienta.  
  
Luego de la reunión, Torelli indicó que “por pedido del ministro Daniel Costamagna llevamos adelante este diálogo, a partir del cual avanzaremos en incorporar beneficios para aquellos comercios adheridos al programa, hecho que redundará en mayores promociones para los consumidores”.  
  
A continuación, el secretario destacó que “los integrantes de la entidad que aglutina a la industria frigorífica de la provincia se mostraron muy interesados en participar y elaborarán una propuesta para luego firmar un convenio con el gobierno provincial”.  
  
**BILLETERA SANTA FE**  
Billetera Santa Fe es un programa de beneficios y descuentos que busca incentivar la demanda de bienes y servicios mediante el otorgamiento de reintegros de dinero a todas las personas que realicen compras en comercios radicados en la provincia.  
  
Esta herramienta permite sumar poder de compra otorgando reintegros de un 30 por ciento en la adquisición de alimentos, indumentaria, calzados, juguetería, librería, bares, restaurantes, farmacias, gastronomía y turismo, y un 20 por ciento en electrodomésticos.  
  
Los interesados deben registrase en [www.santafe.gov.ar/billeterasantafe](http://www.santafe.gov.ar/billeterasantafe), para obtener un usuario y contraseña.