---
category: La Ciudad
date: 2020-12-29T11:00:53Z
thumbnail: https://assets.3dnoticias.com.ar/2912-siprus.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Diario UNO'
resumen: Los profesionales de la salud vuelven a parar y esta vez lo hacen por 72
  horas
title: Los profesionales de la salud vuelven a parar y esta vez lo hacen por 72 horas
entradilla: Desde Siprus señalaron que la medida es en reclamo a la falta de propuesta
  salarial del gobierno provincial. La semana pasada, el paro había sido de 24 horas

---
Los profesionales de la salud de la provincia, nucleados en Siprus, **anunciaron un paro de 72 horas para esta semana**. La medida de fuerza es en respuesta a la falta de propuesta salarial del gobierno de Santa Fe.

**El paro se llevará a cabo los días martes, miércoles y jueves de esta semana** y será sin concurrencia a los lugares de trabajo. Cabe recordar que la semana pasada, los gremios de la salud (Amra y Siprus) junto a los estatales (ATE y Upcn) definieron una medida de acción directa por 24 horas que se cumplió el miércoles.

Desde el sindicato, denuncian que «a tres días de terminar el año, el gobierno no ha vuelto a convocar a paritaria». Y agregan: «Es un fin de un año de desvalorización de los trabajadores de la salud, de pérdida salarial, de sumas en negro, en el marco de una pandemia histórica».

«Quedó demostrado que quienes sostenemos el sistema de salud estuvimos a la altura. El gobierno, no», afirmaron.

«En este duro contexto, Siprus y Fesprosa (Federación Sindical de Profesionales de la Salud de la República Argentina) como gremio mayoritario y más representativo del sector profesional, nunca dejó de reclamar. Llevamos adelante medidas locales, provinciales y nacionales exigiendo condiciones dignas de trabajo y salariales», señalaron a través de un comunicado de prensa.

«Empezamos el 2020 en la calle el 2 de enero, reclamando por la extensión del cronograma de pagos de sueldos. Terminaremos el año de la misma manera: luchando por nuestros derechos», recordaron.

![](https://assets.3dnoticias.com.ar/2912-siprus1.webp)