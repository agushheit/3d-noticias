---
layout: Noticia con imagen
author: "Fuente: Diario Uno"
resumen: "Transporte urbano"
category: Agenda Ciudadana
title: "Transporte: retoman las negociaciones y hay riesgo de paro si no
  logran un acuerdo"
entradilla: "Este jueves comenzarán las negociaciones paritarias entre la Unión
  Tranviarios Automotor (UTA) y la Federación que nuclea a los empresarios que
  brindan servicios urbanos e interurbanos del interior del país (FATAC)."
date: 2020-10-22T18:30:35.248Z
thumbnail: https://assets.3dnoticias.com.ar/colectivos.jpg
---
Fuentes gremiales informaron a UNO Santa Fe que la audiencia está prevista que comience a las 13 horas en la Secretaría de Trabajo de la Nación. Desde el gremio de los choferes indicaron que el sindicato mantiene el "estado de alerta". 

El encuentro tendrá un condimento especial, vinculado al acuerdo al que llegó este martes la UTA con los empresarios del transporte del Área Metropolitana de Buenos Aires.

La información fue dada a conocer a través de un comunicado de prensa firmado por Roberto Fernández, titular de la UTA. Consistió en un incremento salarial del 30 por ciento en el salario básico y el pago de una suma no remunerativa de $ 20.000 en tres cuotas mensuales.

Con respecto a la forma en la cual se hará efectivo el aumento, la UTA aclaró que el monto correspondiente al mes de septiembre "será abonado el cuarto día hábil de noviembre". Sobre la suma no remunerativa de $ 20.000, la entidad sindical precisó que se abonarán de la siguiente manera: $7.000 en el cuarto día hábil de noviembre; otros $ 7.000 en el cuarto día hábil de diciembre y $ 6.000 el 30 de diciembre de 2020".

Difícilmente el conflicto podría encontrar una salida mañana. Por ello, no se descarta el escenario de una conciliación obligatoria, que evite un posible paro y que las negociaciones puedan continuar.

Desde el gremio a nivel local señalaron que la definición la van a tomar los secretarios generales y dependerá de los resultados que arroje la negociación.

Por otro lado, desde el sector empresario marcaron diferencias con el acuerdo firmado en Buenos Aires. Aseguran que la suba fue acordada en base a nuevos "aportes nacionales", recursos que el interior no recibiría. Si bien el presupuesto nacional no está cerrado aún, se prevé un incremento de subsidios para el interior del país del 20 por ciento, suba que los empresarios ya consideran escasa.