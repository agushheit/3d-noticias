---
category: La Ciudad
date: 2021-12-07T06:15:26-03:00
thumbnail: https://assets.3dnoticias.com.ar/TGI.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'Santa Fe: 2022 viene con aumentos en tributos de un 34% y subas en la TGI'
title: 'Santa Fe: 2022 viene con aumentos en tributos de un 34% y subas en la TGI'
entradilla: "Se aprobó el proyecto de actualización de la Ordenanza Tributaria Anual.
  Suben los valores del metro cuadrado de cada Zona Inmobiliaria, lo que traccionará
  en un incremento progresivo de la Tasa General de Inmuebles.\n\n"

---
Apenas con mínimas modificaciones en el proyecto original, otra de las normas financieras clave que aprobó el Concejo Municipal es la actualización de la Ordenanza Tributaria Municipal Anual, norma que contiene todos los tributos, tasas y derechos que deben abonar por cualquier tipo de trámite o gestión los contribuyentes de la ciudad. Esto va desde los montos mínimos generales para las actividades económicas y comerciales, hasta los costos de las playas de estacionamiento, la solicitud de un libre multa o el costo de una inhumación, por citar apenas algunos ejemplos.

 La primera novedad es que el proyecto de actualización remitido por el Ejecutivo plantea un aumento del valor del metro cuadrado por cada una de las Zonas Inmobiliarias (ZI) de esta capital en un 33,3%. La ZI es un componente que integra el cálculo del Avalúo Fiscal Municipal, por lo cual esto impactaría directamente en subas progresivas (trimestrales) de la Tasa General de Inmuebles (TGI) durante el año entrante, siempre dependiendo de cada área del ejido urbano.

 No hay subas solicitadas en las alícuotas del Derecho de Registro e Inspección (Drei) que debe tributar mensualmente cada comerciante de la ciudad, por ejemplo; sí en los montos mínimos generales.

 En el proyecto de actualización de esta norma financiera se solicita aumentos en promedio de un 34% de todos los tributos, tasas y derechos municipales, como por ejemplo el Derecho de Cementerio y de Espectáculos Públicos; subas en los montos mínimos generales de actividades privadas que deben tributar al Gobierno local y la Tasa de Mantenimiento de la Red Vial (el llamado "impuesto al bache"), entre otros puntos.

 Finalmente, en la Tributaria se aumentó el derecho de plataformas (que tributan las empresas con concesión para operar en la terminal de ómnibus), algo que beneficiará a los maleteros de la ciudad. Esto se incorporó en despacho consensuado previo al inicio de la sesión.

 Por último, se incorporó en la Ordenanza Tributaria el artículo "Instrumentos de Promoción", que alude al Código de Habitabilidad. Así, las construcciones nuevas que incorporen el sistema de terrazas verdes o cubiertas naturadas, en techos y azoteas, obtendrán un descuento del 10% en la TGI. Para el caso de edificaciones preexistentes, que lo añadan, se estipula un descuento 15% en el importe de la TGI.

 Para percibir el descuento los propietarios deberán presentar anualmente un formulario de declaración jurada donde un profesional matriculado asevere que se sigue conservando y manteniendo correctamente y en forma periódica el sistema de Terrazas Verdes o Cubiertas Naturadas, dice el despacho sancionado.