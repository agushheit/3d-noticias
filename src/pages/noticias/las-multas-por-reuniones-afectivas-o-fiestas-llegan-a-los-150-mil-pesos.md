---
category: La Ciudad
date: 2021-04-13T07:25:21-03:00
thumbnail: https://assets.3dnoticias.com.ar/clandestinas.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de LT10
resumen: Las multas por reuniones afectivas o fiestas llegan a los 150 mil pesos
title: Las multas por reuniones afectivas o fiestas llegan a los 150 mil pesos
entradilla: Así lo indicó la secretaria de Control municipal, Virginia Coudannes.
  En el primer fin de semana con nuevas restricciones en la ciudad, más de 20 bares
  y restaurantes fueron notificados sobre los protocolos vigentes.

---
El que pasó fue el primer fin de semana con vigencia de las nuevas restricciones impuestas en la ciudad por la segunda ola de coronavirus, en consonancia con lo dispuesto a nivel provincial y nacional. 

Por LT10, la secretaria de Control del Municipio, Virginia Coudannes, hizo un balance de lo actuado y dijo que se realizaron 50 verificaciones y más de 20 bares y restaurantes fueron notificados sobre los horarios y capacidades que deben respetar. Estas advertencias son previas a una sanción o clausura.

Por otra parte, también se labraron actas de infracción ante denuncias de vecinos por fiestas clandestinas e incluso reuniones afectivas o familiares. 

La funcionaria local comentó que las mismas fueron desde las 20 a las 70 personas, y que la multa correspondiente oscila entre los 100 y los 150 mil pesos. 

El monto debe ser afrontado por el dueño del domicilio o el organizador del evento. 

Todo ello, independientemente de las actuaciones penales que pueden caber a los responsables.