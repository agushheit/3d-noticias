---
category: Agenda Ciudadana
date: 2022-01-15T06:15:52-03:00
thumbnail: https://assets.3dnoticias.com.ar/FERPUT.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: Alberto Fernández se reunirá el próximo 3 de febrero en Moscú con Vladimir
  Putin
title: Alberto Fernández se reunirá el próximo 3 de febrero en Moscú con Vladimir
  Putin
entradilla: 'Así lo indicaron altas fuentes de la Casa Rosada, que detallaron que
  se trata de "una reunión bilateral que estaba pendiente y versará sobre colaboración
  en vacunas".

  '

---
El presidente Alberto Fernández mantendrá un encuentro bilateral con su par de Rusia, Vladimir Putin, el próximo 3 de febrero en la ciudad de Moscú.

Así lo indicaron a Noticias Argentinas altas fuentes de la Casa Rosada, que detallaron que se trata de "una reunión bilateral que estaba pendiente y versará sobre la colaboración en vacunas".

Además, precisaron que Fernández y Putin se reunirán en el Kremlin para conversar sobre temas vinculados a "inversiones y colaboración en ciencia y temas de interés común".

"La escala en Rusia será de 48 horas", puntualizaron fuentes con acceso al despacho presidencial, que afirmaron que el próximo 4 de febrero se encontrará con el presidente de China, Xi Jinping.

El viaje de Alberto Fernández a Moscú se había gestado a principios del pasado mes de diciembre, cuando el Presidente se reunió en la Casa Rosada con autoridades del Fondo Ruso de Inversión Directa y directivos de empresas rusas.

Luego de ese almuerzo, fuentes de la Casa de Gobierno habían revelado que el jefe de Estado había sido invitado oficialmente a visitar Moscú, pero hasta el momento el viaje no se había concretado producto de la pandemia de coronavirus.

Se trata del primer encuentro cara a cara entre Fernández y Putin, dado que las conversaciones que mantuvieron hasta el momento fueron de manera virtual.

Los jefes de Estado tenían previsto mantener un encuentro bilateral durante la Cumbre del G20 que se realizó a fines del pasado mes de octubre en la ciudad italiana de Roma, pero a último momento Putin decidió no formar parte del cónclave internacional.

La Cumbre entre ambos mandatarios se producirá un día antes del arribo del jefe de Estado a China, en un viaje que estaba programado desde hace semanas, ya que participará de la inauguración oficial de los Juegos Olímpicos de invierno en Pekín.

Los viajes de Fernández a Moscú y Pekín se producirán en medio de las negociaciones finales entre el Gobierno y el Fondo Monetario Internacional (FMI) por el refinanciamiento de la deuda que contrajo la administración de Mauricio Macri con el organismo de crédito internacional.

Pese a que China y Rusia son dos países con los cuales Estados Unidos, principal accionista en el Fondo, mantiene conflictos comerciales y políticos, en la Casa Rosada aseguran que las visitas a esos países "no tendrá consecuencias" en la negociación con el FMI.

La noticia del viaje a Rusia se conoció a menos de 48 horas de que el canciller Santiago Cafiero emprenda su viaje a Washington para reunirse con el secretario de Estado, Antony Blinken.

Fuentes de la Cancillería argentina precisaron a NA que Cafiero partirá rumbo a Estados Unidos el próximo domingo por la noche.

"Estará 48 horas en Washington y regresa a preparar el viaje a China", concluyeron.