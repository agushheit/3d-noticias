---
category: Agenda Ciudadana
date: 2020-12-13T13:54:45Z
thumbnail: https://assets.3dnoticias.com.ar/barrio-las-vegas.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: El Litoral'
resumen: 'Las Vegas: las dos caras de un barrio convulsionado'
title: 'Las Vegas: las dos caras de un barrio convulsionado'
entradilla: "Durante la semana, mataron a un menor y quemaron más de una decena de
  viviendas. \nSe habla de conflictos históricos entre bandas antagónicas: los Rojos
  contra los Azules. "

---
**La escalada de violencia termina opacando el trabajo social gigante que se realiza en el barrio**: cerró temporalmente el centro de salud y los merenderos e instituciones barriales temen abrir sus puertas por los tiroteos que estallan periódicamente.

**Barrio Las Vegas de Santo Tomé fue noticia esta semana**. Este sector del suroeste de la localidad venía padeciendo desde hace tiempo las consecuencias de hechos violentos que terminaron de estallar el pasado martes 8 de diciembre por la noche, con el asesinato a tiros de un joven de tan solo 17 años, presuntamente por conflictos interpersonales. 

Esto derivó en **tiroteos entre facciones que se dieron en varias oportunidades y a plena luz del día**. La onda expansiva de la explosión implicó más de una decena de viviendas precarias quemadas, que ardieron durante distintos días. 

Las consecuencias: una población aterrorizada, primeras planas de los medios de comunicación, cierre temporal del dispensario, y la suspensión del trabajo de algunos comedores comunitarios y otras instituciones barriales. Lamentablemente otra vez la sangre tiñe lo que verdaderamente es Las Vegas, un barrio carenciado de gente trabajadora, pero postergado por quienes debieron y deben preocuparse por modificarlo.

Esto puede haber sido porque está lejos del centro, prácticamente saliendo de "santoto". El conglomerado urbano tiene sus límites geográficos demarcados por la Autopista Santa Fe Rosario al oeste, calle Estados Unidos de México al este, y bañados o campos en sus extremos norte y sur. 

Estimaciones demográficas realizadas por referentes barriales con más de 25 años de trabajo comunitario en el lugar, calculan que en la actualidad existen unas 5.000 personas viviendo en Las Vegas. De ese número, cerca de 2.000 son menores de edad. Tiene tres calles troncales: Arenales arriba, Lisandro de la Torre en el medio y Alberdi abajo, cortadas por unas 13 arterias transversales. El 80 % de las viviendas son de material, mientras que el 20 restante son las consideradas "precarias". 

En los años 90 se registraba solo un 30 % de la población actual, mayormente familias trabajadoras de bajos recursos que no tenían donde vivir y optaron por levantar sus casitas básicas en esa zona. Con los años fue mutando lentamente hasta convertirse en lo que es hoy.

<br/>

# **Los Rojos contra los Azules**

Tal como afirman los vecinos consultados, las olas cíclicas de violencia en el barrio son protagonizadas por dos bandas antagónicas del lugar: los Rojos y los Azules. 

Al mejor estilo de las reyertas federales del siglo XIX, se disputan el territorio a pura sangre y fuego para tratar de acaparar -en este caso actual y local- ciertos negocios turbios. 

"Los Azules son los del oeste, al fondo de Las Vegas, mientras que los Rojos son 'los del otro lado'. Siempre se dirimen las cosas así, a los tiros. Cada 3 o 4 años se renueva la tensión y se enfrentan de esa manera, a veces con muertos y otras veces con quema de casas, como está pasando", sostiene otra fuente barrial consultada.

"Literalmente se odian y desde hace muchos años. Ahora los Rojos quemaron las casitas de los Azules y prácticamente esas familias del fondo se fueron. El barrio está completamente atemorizado, a punto tal que los lugares donde daban viandas dejaron de hacerlo por miedo", agregó. 

Los habitantes apelan al sentido común y consideran que las soluciones son fáciles:

"Deberían instalar un destacamento policial, como se pide desde hace años; mejorar los servicios en general porque -por ejemplo- la iluminación del barrio es escasa y es una boca de lobo; las malezas tapan las precarias viviendas que quedaron al oeste. Así estamos, y por eso todo el trabajo bueno que se hace en el barrio, se termina opacando como consecuencia de esta ausencia del Estado", señaló afligido el vecino, que por décima vez en su vida vuelve a padecer otra seguidilla de violencia como la actual.

<br/>

# **La mirada barrial de un referente social**

Luis Martínez es un exconcejal de Santo Tomé, involucrado desde hace décadas con los barrios de la localidad. Como referente social y político, realizó la siguiente apreciación sobre Las Vegas tras años de labor barrial en el sector:

"Es importante tener en cuenta la situación geográfica y la ubicación del barrio porque la vida social, cultural, laboral y sanitaria se desarrolla en un sector que no tiene conexiones viales que lo vinculen con facilidad con el resto de la ciudad. Por lo tanto lo bueno, lo malo, lo regular e irregular se desarrolla en un contexto bien delimitado. 

Todo se magnífica en el barrio. Se magnífica cuando se entregan los bolsones con alimentos y las familias hacen una cuadra de cola; se magnífica en la cantidad de meriendas, almuerzos y cenas que preparan las organizaciones para paliar el hambre; se magnífica cuando hay cortes de energía eléctrica y de agua potable, los que demoran días en conseguir una respuesta; se magnífica en años que se viene necesitando y reclamando un edificio nuevo para el centro de salud y el jardín de infantes.

También se magnifica el accionar delictivo: fácil acceso a las armas, narcomenudeo, robos, violencia en todas sus expresiones. Pero este u otro análisis de la realidad es eso, solo una mirada más sobre lo que "otros" padecen. ¿Qué hacer en este contexto? Desde hace décadas militamos en distintos espacios y plataformas con una consigna clara: debemos construir una ciudad donde quepamos todos y nadie sobre. 

Para ello el rol del estado debe avanzar con la culminación del centro de Salud y Jardín de infantes; proyectos socio productivos en el barrio; mesa de gestión territorial que coordine acciones para garantizar el acceso a los alimentos; una red de servicios básicos sanitaria en todo el barrio; incorporación de adolescentes y jóvenes al sistema educativo, capacitaciones, espacios de encuentro que generen proyectos de vida. Con esa base y el accionar preventivo de las fuerzas de seguridad y de la Justicia, iremos cambiando la calidad de vida de muchas familias".

<br/>

# **Contención acechada**

Como suele ocurrir, los hechos de violencia en los barrios pobres oscurecen el trabajo solidario permanente que mucha gente realiza  en este tipo de lugares. 

"Como sucede en estas barriadas postergadas, muchas familias marginales se fueron ensamblando, muchos chicos quedaron solos con sus madres o también con padrastros, y las condiciones de vulnerabilidad extrema obligaron a la deserción escolar. Al no tener padres que los contengan, en el presente aquellos chicos de finales de los 90 son los que protagonizan estos hechos de violencia", aseguraron a El Litoral los vecinos que viven desde hace décadas en el barrio y trabajan con los más necesitados.

En el presente se concreta un trabajo social muy importante. Hay varios merenderos o copas de leche que funcionan hace tiempo, como el ubicado en Lisandro de la Torre y Bieler Haas, el situado donde antes estaba Cáritas y el existente detrás de la Escuela N°49 "Agustín Araya" (la única del barrio), a la altura de calles Riccheri y Seguí. 

**Claramente Las Vegas no es solo violencia como muchos creen, y sin lugar a dudas la estigmatización duele**.

Rojos contra azules. "Cada 3 o 4 años se renueva la tensión y se enfrentan de esa manera, a veces con muertos y otras veces con quema de casas, como está pasando", dicen los referentes. 

<br/>

# **CAPS cerrado por los tiros**

La escalada de agresiones llevó a que el Centro de Atención Primaria de la Salud "Lisandro de la Torre" cierre temporalmente sus puertas. 

Seguirá así este lunes 14, a la espera de respuestas a los reclamos de los trabajadores. Patricia More, delegada gremial del Samco Santo Tomé y representante del nombrado Caps, explicó que la determinación se toma porque se dieron balaceras prolongadas delante de la institución sanitaria. 

"Estuvieron tiroteándose frente al dispensario durante una hora, hasta que llegó la Policía. Para volver a trabajar estamos pidiendo una reunión en la que las autoridades de Seguridad y Salud tomen las decisiones que correspondan para preservar la integridad de los empleados. Hace 4 meses sucedió otro hecho violento dentro del lugar, y hasta una paciente le pegó a una médica que ya solicitó el traslado y no volverá", describió.

More reiteró el pedido que ya se hizo a la Región Salud Santa Fe y a otros funcionarios de distintas carteras, para constituir un Corredor Seguro con mayor presencia policial y acompañamiento del personal de Salud. 

"Lo hicieron dos días y después nunca más aparecieron. Los remises no quieren entrar al barrio y eso complica el trabajo. Prometieron un botón de pánico institucional pero nunca llegó. En estas circunstancias, se hace muy difícil volver a abrir las puertas", confesó Patricia.

<br/>

# **¿Teléfono apagado?**

Como consecuencia del presente de Las Vegas, los recurrentes tiroteos en barrio El Chaparral, la superpoblación de detenidos en la Comisaría N°12 y la fuga de 7 internos del lugar acontecida el día viernes 11, la gestión municipal volvió a pedir soluciones concretas y urgentes al gobierno provincial. 

El secretario de Gobierno, Martín Giménez, anticipó que, debido a la falta de respuestas por parte del Ministerio de Seguridad, se solicitó formalmente una audiencia con el gobernador Omar Perotti. 

El Ejecutivo Municipal también acompañó a los vecinos de los destinos sectores complicados de la ciudad en el pedido de mayor seguridad, sin obtener las respuestas esperadas debido a lo que consideran como "una trama política". 

"El ministro de Seguridad, Marcelo Saín, no me atiende el teléfono", dijo en la semana la intendenta Daniela Qüesta. "Ya nos cansamos de mandar notas y de realizar presentaciones, sin obtener respuestas", acotó Giménez.