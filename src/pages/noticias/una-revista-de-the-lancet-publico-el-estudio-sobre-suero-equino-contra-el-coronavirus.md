---
category: Agenda Ciudadana
date: 2021-04-11T06:00:44-03:00
thumbnail: https://assets.3dnoticias.com.ar/sueroquino.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Una revista de The Lancet publicó el estudio sobre suero equino contra el
  coronavirus
title: Una revista de The Lancet publicó el estudio sobre suero equino contra el coronavirus
entradilla: El laboratorio biotecnológico Inmunova anunció que a el tratamiento del
  coronavirus desarrollado por científicos del Conicet fueron difundidos en la publicación
  EClinicalMedicine del prestigioso grupo editorial.

---
El laboratorio biotecnológico Inmunova anunció este domingo la publicación, en una revista médica del grupo The Lancet, de los resultados del estudio clínico de las fases 2 y 3 sobre el suero equino hiperinmune para el tratamiento del coronavirus y desarrollado por científicos del Conicet, que ya se aplicó a más de 1.200 pacientes en 16 provincias.

Se trata EClinicalMedicine, que es publicada por la prestigiosa editorial médica y cuenta con un sistema de revisión por pares y de acceso abierto.

La publicación representa un aval para este suero desarrollado de manera conjunta entre científicos públicos y privados.

El estudio detalla los tratamientos con anticuerpos policlonales equinos en pacientes con enfermedad Covid-19 moderada a severa, e incluye un "ensayo clínico Fase 2/3 adaptativo, randomizado, multicéntrico, doble ciego y controlado con placebo" realizado en nuestro país y que, a partir de su publicación, podrá ser revisado por la comunidad científica de todo el mundo.

"El suero hiperinmune anti-SARS-CoV-2 (CoviFab®) es un medicamento biológico para tratar la enfermedad Covid-19 moderada a severa, cuyo desarrollo llevó adelante Inmunova a partir de su experiencia en ingeniería de proteína", informó la empresa de biotecnología en un comunicado.

Según Inmunova, la inmunoterapia de anticuerpos policlonales equinos logra que los pacientes que reciben el tratamiento puedan bloquear los agentes infecciosos y evitar que se propague en el organismo.

De acuerdo a los resultados publicados "esta terapia demostró un adecuado perfil de seguridad en la población evaluada; los eventos adversos de especial interés fueron leves y no requirieron la interrupción de la infusión ni impidieron la administración de la segunda dosis".

Además, mostró una tendencia favorable -continúa el comunicado-, aunque no significativa estadísticamente, "al beneficio clínico en los parámetros más importantes para medir la progresión de los pacientes: reducción en la mortalidad de 44% en los pacientes con Covid-19 severa, de la internación en terapia intensiva en 29% y el requerimiento de asistencia respiratoria mecánica en 33%, frente al placebo".

**La opinión de Salvarezza**

El ministro de Ciencia, Tecnología e Innovación, Roberto Salvarezza, subrayó que el suero hiperinmune “no solo se trata de un desarrollo con innovación, sino que aparte tiene una originalidad: los anticuerpos policlonales, que no había sido desarrollado durante la pandemia”.

“La ciencia argentina estuvo en el centro de la pandemia y gracias a la actividad de científicos, tecnólogos, universidades y el Conicet, se logró desarrollar kits de diagnóstico, fabricación de respiradores, barbijos antivirales y antimicrobianos, y faltaban alternativas como fue el suero hiperinmune”, expresó.

Sobre el proyecto público-privado, el ministro afirmó que “ha sido un ejemplo a destacar” la participación de organismos como “la Universidad de San Martín, el Conicet y el Anlis” y dos empresas de base tecnológica como Inmunova y Biol.

“Todos los desarrollos que pudimos hacer fueron a partir de un clúster productivo donde teníamos a los científicos trabajando con laboratorios y empresas nacionales capaces de llevar de un prototipo de laboratorio a un producto que esté disponible para los hospitales y para la gente”.

**La satisfacción de Innova**

Por su parte, Fernando Goldbaum, socio fundador y director científico de Inmunova, afirmó a esta agencia que "el hecho de que el ensayo esté publicado en una revista de primera línea realza que nuestro trabajo y de la Anmat (Administración Nacional de Medicamentos, Alimentos y Tecnología Médica) va por la línea correcta".

Explicó que "el ensayo se realizó entre agosto y diciembre del año pasado y nos llevó varios meses que se publicara en una revista internacional de primera línea".

"Estamos muy contentos porque es un proceso de revisión muy rigurosos y en este caso lo revisaron seis expertos en dos rondas, lo cual llevó a que el trabajo se presentara de la forma más adecuada y de mucho control de pares que es lo que se hace cuando se trabaja en medicina basada en la evidencia científica".

Goldbaum destacó que "es un logro de un grupo muy grande de gente. En este trabajo somos 36 los autores principales, pero hay 180 más que trabajaron en los casi 20 centros en los que se realizaron los ensayos".

**Dónde se aplica el suero**

Con respecto a la aplicación de suero hiperinmune, indicó que "se está aplicando en centros públicos y privados en 16 provincias" y señaló que poseen "una base de datos porque la Anmat nos dio la aprobación en condiciones especiales que significa que tenemos que seguir recabando información acerca de la seguridad y la eficacia".

"En esa base tenemos más de 1.200 pacientes tratados donde está depositada la evolución de ellos. Por suerte los resultados están siguiendo la misma tendencia e inclusive mejores, aunque todavía los estamos analizando", manifestó.

**El desarrollo**

La elaboración de suero a partir de anticuerpos de caballos fue un trabajo de articulación pública-privada encabezado por el laboratorio Inmunova y el Instituto Biológico Argentino (BIOL), la Administración Nacional de Laboratorios e Institutos de Salud "Dr. Carlos G. Malbrán" (Anlis), con la colaboración de la Fundación Instituto Leloir (FIL), mAbxience, Conicet y la Universidad Nacional de San Martín (Unsam).

"En condiciones de pandemia lo que hicimos por las urgencias de contar con nuevos medicamentos fue un ensayo combinado que incluía una fase 2 y una 3", apuntó Goldbaum.

El proyecto fue uno de los seleccionados por la Agencia Nacional de Promoción de la Investigación, el Desarrollo Tecnológico y la Innovación en la convocatoria "Ideas Proyecto Covid-19" de la Unidad Coronavirus, creada por el Ministerio de Ciencia, Tecnología e Innovación, e integrada por el Conicet.

Algunos de los ensayos se hicieron en el Sanatorio Güemes, Hospital General de Agudos "Dr. Ignacio Pirovano"; Hospital Italiano de Buenos Aires; Centro Gallego de Buenos Aires; Clínica Adventista Belgrano; Sanatorio Sagrado Corazón; Hospital de Infecciosas "Francisco Muñiz"; Clínica Zabala; Sanatorio Agote; Fundación Favaloro; y Hospital Español.

**El visto bueno de la Anmat**

Estos resultados que publicó The Lancet ya habían sido presentados en la Anmat, que el 22 de diciembre 2020 autorizó el medicamento para el tratamiento de pacientes adultos con enfermedad moderada a severa, dentro de 10 días del inicio de los síntomas.

"Los resultados que mandamos a publicar son los mismos que le presentamos a las autoridades regulatorias", precisó Goldbaum y resaltó que "tanto en el análisis de la revista como el que hizo todo el personal de Anmat vieron que la seguridad era muy buena".

Para la obtención del suero equino hiperinmune se inyecta en caballos una proteína del virus SARS-CoV-2 que sirve como antígeno y al introducirse en el organismo de estos animales induce una respuesta inmunitaria provocando la formación de dichos anticuerpos que neutralizan la capacidad del virus de entrar a las células.

El director científico de Inmunova informó que el viernes brindarán un simposio en el cual mostrarán los resultados del Hospital de Campaña de Corrientes, que es donde se ha aplicado en más de 450 pacientes.