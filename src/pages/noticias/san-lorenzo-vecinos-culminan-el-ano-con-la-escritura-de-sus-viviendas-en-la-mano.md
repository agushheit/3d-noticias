---
category: La Ciudad
date: 2020-12-30T12:35:21Z
thumbnail: https://assets.3dnoticias.com.ar/3012-escrituras.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: 'San Lorenzo: vecinos culminan el año con la escritura de sus viviendas en
  la mano'
title: 'San Lorenzo: vecinos culminan el año con la escritura de sus viviendas en
  la mano'
entradilla: 'El intendente Emilio Jatón entregó 15 escrituras, que se suman a las
  100 entregadas a lo largo del año. '

---
La Municipalidad relevó la situación de 500 familias, de las cuales 424 comenzarán el trámite para su regularización dominial.

Este martes, 15 familias del barrio San Lorenzo cerraron el año de la mejor manera posible. La Estación, ubicada en Entre Ríos 4090, fue el punto de encuentro con el intendente Emilio Jatón para recibir la entrega de escrituras de sus viviendas. Además, participaron del encuentro Paola Pallero, titular de la Agencia de Hábitat, y Gisela Martínez, presidenta de la vecinal.

Luego del otorgamiento de las escrituras, Pallero destacó: «Junto al intendente Emilio Jatón participamos de esta jornada que nos pone muy felices». En ese sentido, la funcionaria indicó que la iniciativa está enmarcada «en una de las políticas principales dentro de la Agencia de Hábitat de la Municipalidad, que es la regularización dominial, es decir otorgar la tenencia segura de sus viviendas. Hoy estamos muy contentos y participamos de la emoción de los vecinos y vecinas de que puedan acceder a la escritura definitiva de sus hogares».

***

![](https://assets.3dnoticias.com.ar/3012-escrituras1.webp)

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.2em; font-weight: 700">Balance</span>**

A modo de balance, Pallero expresó que con la puesta en marcha del Plan Integrar «el municipio comenzó a generar condiciones de integración sociourbana en los barrios más vulnerables de la ciudad. Dentro de esas políticas está la regularización dominial, que tiene como objetivo otorgar seguridad jurídica a todas las familias que habitan en lotes municipales».

En tanto, la funcionaria repasó que durante el 2020 «relevamos la situación de aproximadamente 500 familias, de los barrios San Agustín, Loyola, Liceo Norte, Colastiné Sur. En lo que va del año entregamos unas 115 escrituras. Asimismo, fueron enviados al Colegio de Agrimensores unos 424 lotes para la subdivisión y mensura que es el requisito previo a la regularización dominial».

Otros de los barrios donde se formalizaron las escrituras de vecinas y vecinos a lo largo del año fueron: Estanislao López, San Pantaleón, San Martín, 12 de Octubre, Barranquitas Pro-mejoras, Coronel Dorrego, Nueva Esperanza y Santa Rita.

***

![](https://assets.3dnoticias.com.ar/3012-escrituras2.webp)<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.2em; font-weight: 700">El mejor fin de año</span>**

Maximiliano Pavón nació y se crio en San Lorenzo; sin embargo, dudaba que algún día pudiera formalizar la situación de su vivienda. «Nunca esperamos recibir la escritura», dijo y fundamentó: «Cada trámite que hacíamos años atrás se demoraba, no te contestaban. Volvimos a iniciar el trámite para probar y la verdad es que esta vez nos trataron re bien. Se hizo la escritura súper rápido, no nos cobraron un peso, nos llamaban, nos pasaban a buscar. Sinceramente estoy muy contento».

Maximiliano repasó que «en el barrio hay muy pocas casas que están escrituradas y a nosotros nos facilitaron mucho las cosas. La Municipalidad se encargó de todo el papelerío, siempre nos trataron muy bien, así que estamos muy contentos».

Por su parte, Rosa Almirón aseguró que está «feliz, chocha, agradecida a todos los que gestionaron esta escritura. No pagamos ni un solo peso». En tal sentido, la vecina indicó: «esperábamos desde hace rato la escrituración, que hoy salió a nombre mío y de mi hija. El día de mañana va a ser una herencia para ella, cosa que yo no podría haber hecho. Es una gran manera de terminar el año, a pesar de todas las dificultades estamos terminando el 2020 con la escritura de nuestra casa. Estoy muy agradecida».