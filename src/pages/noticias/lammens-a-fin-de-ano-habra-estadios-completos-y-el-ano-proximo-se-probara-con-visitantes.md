---
category: Deportes
date: 2021-10-14T06:00:01-03:00
thumbnail: https://assets.3dnoticias.com.ar/trubunas.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: 'Lammens: "A fin de año habrá estadios completos y el año próximo se probará
  con visitantes"'
title: 'Lammens: "A fin de año habrá estadios completos y el año próximo se probará
  con visitantes"'
entradilla: '"Para el año que viene está previsto realizar pruebas con hinchas visitantes
  en los estadios", indicó el Ministro de Turismo y Deportes'

---
El ministro de Turismo y Deportes, Matías Lammens, indicó que "a fin de año habría estadios completos en el fútbol argentino" y que "en 2022 se realizarían pruebas para el retorno del público visitante".  
  
"Estimamos que a fin de año podría haber aforo completo para los estadios del fútbol argentino", manifestó esta noche Lammens durante una entrevista realizada en el canal A24.  
  
"Y para el año que viene está previsto realizar pruebas con hinchas visitantes en los estadios", amplió el ministro.  
  
Respecto de este último tema remarcó que "evidentemente la falta de hinchas visitantes no solucionó el problema de la violencia, porque sigue habiendo enfrentamientos entre barras de un mismo club".  
  
"Y no es posible que no podamos convivir con los que piensan diferente. Yo voy a la cancha desde que era chico y siempre había hinchas en la tribuna de enfrente, pero no quería agarrarme a piñas con ellos", puntualizó finalmente el expresidente de San Lorenzo de Almagro.