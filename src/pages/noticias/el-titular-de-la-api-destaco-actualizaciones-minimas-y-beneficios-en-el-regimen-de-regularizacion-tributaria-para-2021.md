---
category: Estado Real
date: 2021-01-24T10:27:12Z
thumbnail: https://assets.3dnoticias.com.ar/api.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: EL TITULAR DE LA API DESTACÓ “ACTUALIZACIONES MÍNIMAS” Y “BENEFICIOS” EN
  EL RÉGIMEN DE REGULARIZACIÓN TRIBUTARIA PARA 2021
title: EL TITULAR DE LA API DESTACÓ “ACTUALIZACIONES MÍNIMAS” Y “BENEFICIOS” EN EL
  RÉGIMEN DE REGULARIZACIÓN TRIBUTARIA PARA 2021
entradilla: EL TITULAR DE LA API DESTACÓ “ACTUALIZACIONES MÍNIMAS” Y “BENEFICIOS”
  EN EL RÉGIMEN DE REGULARIZACIÓN TRIBUTARIA PARA 2021

---
El titular de la Administración Provincial de Impuestos (API), Martín Ávalos, se refirió a la Ley 14.025, de Régimen de Regularización Tributaria, que fue promulgada por el gobierno santafesino y publicada en el Boletín Oficial. Entre las medidas se pueden destacar la estabilidad fiscal para las pymes santafesinas, manteniendo así la carga tributaria para las empresas durante el presente año. Ningún sector productivo – Comercio, Agro e Industria - verá incrementada su carga tributaria en este año.

Sobre los puntos salientes de la norma, el funcionario mencionó que “en el caso del Impuesto Inmobiliario Urbano y Rural la provincia propuso (a la Legislatura, que aprobó el proyecto) la suspensión del coeficiente de convergencia, con lo cual la repotenciación del Impuesto Inmobiliario que se daba año a año desde 2018, en que fue implementado ese sistema de coeficientes, este año no se va a dar. Sólo se propuso una actualización mínima que está muy por debajo de los índices de inflación con los que cerró el año pasado y que en promedio es del 22% en el inmobiliario Urbano y del 24% en el inmobiliario Rural”.

Sobre este último aspecto Ávalos agregó que “los pequeños productores que tengan hasta 50 hectáreas no van a tener ninguna actualización del impuesto, es decir que van a pagar lo mismo que pagaron en 2020, que a su vez es lo mismo que pagaron en 2019”.

Mientras, en lo referente a “Ingresos Brutos, Sellos y demás, no se hacen modificaciones. Lo único que hay para destacar son beneficios, es decir, una serie de exenciones tanto para el régimen simplificado como aquellos contribuyentes al régimen general que hayan estado afectados por la pandemia”, dijo el titular de API, quien explicó que “a todos los sectores afectados por la pandemia se establecen exenciones de los saldos de declaraciones juradas entre los vencimientos de septiembre de 2020 a marzo de 2021 inclusive”.

“Todo esto está acompañado por un amplio régimen de regularización que lo dividimos en dos etapas-prosiguió Ávalos-: una primera, con deudas que hayan sido generadas hasta el 31 de octubre de 2020 y qué son las deudas afectadas por la pandemia; y una segunda etapa, qué son aquellas deudas que hayan sido devengadas hasta el 29 de febrero de 2020, es decir que diferenciamos entre las deudas que se pudieron haber generado como consecuencia de la pandemia y las deudas que ya venían siendo generadas por los sectores contribuyentes, que no estaban cumpliendo o no habían podido cumplir”, finalizó Avalos.

**Beneficios a sectores afectados en pandemia**

Se establecen exenciones de las cuotas del Régimen Simplificado y de los saldos de declaraciones juradas de ingresos brutos y del impuesto a los sellos por el periodo septiembre 2020 a marzo de 2021. En el caso de impuesto inmobiliario se eximen las cuotas 2 a 6 de 2020 y 1 y 2 de 2021.

Los beneficios alcanzan a todos las categorías del régimen simplificado y a los contribuyentes del régimen general con las siguientes actividades: Bares, restaurantes y similares; servicios de alojamiento, hotelería, residenciales, campings y similares; servicios de agencias de viaje y turismo y similares; servicios de transporte automotor de pasajeros para el turismo, de excursiones y similares; servicios profesionales y personales vinculados al turismo; servicios de explotación de playas y parques recreativos; venta al por menor de artículos o artesanías regionales; servicios vinculados a la organización de ferias, congresos, convenciones o exposiciones y similares; servicios para eventos infantiles; organización de eventos; servicios de soporte y alquiler de equipos, enseres y sonido para eventos; alquiler temporario de locales para eventos; servicios de peloteros; alquiler de canchas para práctica de deportes; jardines maternales; servicios de salones de baile y discotecas.

Se implementa un régimen de regularización tributaria para deudas devengadas hasta el 31/10/2020, en condiciones más favorables a las anteriores en el plazo de financiación, intereses y condonación de multas.

Para el caso de deudas exclusivamente generadas durante la pandemia y hasta el 31/10/2020, los planes no incluirán intereses resarcitorios ni de financiación y el plazo podrá ser hasta 48 cuotas.

La ley incorpora descuentos especiales para el pago de contado anticipado, que alcanzan al 20%, o descuento por adhesión al débito automático, del 10%, en los impuestos inmobiliario urbano y rural e impuesto único sobre vehículos.

Por último, respecto a la Economía del Conocimiento, se dispone la adhesión a la ley nacional que promociona dicha industria generando para las empresas promovidas estabilidad fiscal y una exención por 10 años del impuesto a los ingresos brutos, sellos e inmobiliario.