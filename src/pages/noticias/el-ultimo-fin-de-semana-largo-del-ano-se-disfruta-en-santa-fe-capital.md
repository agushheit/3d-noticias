---
category: La Ciudad
date: 2021-11-19T06:00:50-03:00
thumbnail: https://assets.3dnoticias.com.ar/FINDELARGO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: El último fin de semana largo del año se disfruta en Santa Fe capital
title: El último fin de semana largo del año se disfruta en Santa Fe capital
entradilla: " Capital de la Música y Salón Litoral serán los dos grandes eventos pero
  a las propuestas gratuitas y para todas las edades se suman las iniciativas privadas "

---
La Municipalidad invita a disfrutar del fin de semana largo con encuentros de música en vivo, paseos turísticos, teatro local y muestras en las principales salas culturales y escenarios de la capital. Entre los grandes eventos se destaca desde el jueves 18 y hasta el domingo 21 de noviembre, [Capital de la Música](https://santafenoticias.gob.ar/capital-de-la-musica-la-municipalidad-inicia-un-programa-para-fortalecer-el-sector-musical/), en la Estación Belgrano, con entrada gratuita. Y la inauguración de la muestra del Salón Litoral 2021, en el Museo Municipal de Artes Visuales “Sor Josefa Díaz y Clucellas”. Después de cuatro décadas, el gobierno local recupera ese emblemático salón que proyectó las artes visuales santafesinas en toda la región.

Además la ciudad ofrece una gran cantidad de paseos y actividades gratuitas y para todas las edades en distintos espacios públicos. Habrá circuitos turísticos guiados, se reedita la búsqueda del tesoro en la Manzana Jesuítica, la muestra de María Elena Walsh se despide de la ciudad con un gran evento y muchas cosas más.

El sábado desde las 17 se podrá disfrutar de Calle Recreativa en la Costanera Oeste con propuestas para disfrutar en familia. Además habrá clases abiertas de Zumba en la Dirección de Deportes Municipal – Av. Alte. Brown 5200 – el sábado y el domingo desde las 17 y también en el Parque del Sur el domingo a las 17.

A toda la oferta libre y gratuita que ofrece la Municipalidad de Santa Fe se suman actividades organizadas por el sector privado que hacen de la ciudad un destino de interés para visitantes de todos lados pero también para quienes viven en la capital provincial y su área metropolitana.

**Paseos y búsqueda del tesoro**

Una de las propuestas que integran la agenda son los paseos guiados gratuitos por puntos emblemáticos de la ciudad. El calendario de paseos de Mi Ciudad como Turista, una propuesta pensada para que los santafesinos redescubran su ciudad y para que los turistas conozcan lo más emblemático de Santa Fe, comenzará el sábado 20 a las 16.30 con una versión abreviada del Camino de la Constitución. El punto de encuentro será la Plaza 25 de Mayo (3 de Febrero y San Martín).

En tanto el domingo, también desde las 16.30, la invitación es a recorrer el Paseo Bulevar y el punto de encuentro será la Plaza Pueyrredón (Bv. Gálvez 1600). Finalmente el lunes 22 se ofrecerá el paseo Casco Histórico, esta vez a las 11 y el punto de encuentro será Plaza 25 de Mayo. A lo largo de esos recorridos se conocerán datos interesantes sobre el surgimiento y crecimiento de la ciudad, así como la participación de santafesinas y santafesinos en distintos hechos históricos de nuestro país.

En todos los casos los paseos son gratuitos, pedestres, con distancias que oscilan entre los 800 y 1500 metros y son guiados por profesionales de turismo que estarán identificados con sombrillas verdes.

También durante el fin de semana largo vuelve la Búsqueda del Tesoro en la Manzana Jesuítica – San Martín 1540 -, una actividad orientada a chicos y chicas entre 6 y 11 años. Será el sábado 20 y domingo 21 a las 15 con entrada gratuita, pero es necesario inscribirse previamente completando el siguiente [formulario](https://forms.gle/X39BX5ZZp26rQa218). Además el histórico solar, que integra el Camino Internacional de los Jesuitas, tendrá visitas guiadas gratuitas el viernes 19 a las 9, 11, 15.30 y 17. Mientras que el sábado 20 y domingo 21 se podrá acceder a las visitas guiadas a las 11 y a las 17.30.

Finalmente se recuerda que todos los paseos que integran la propuesta de Mi Ciudad como Turista pueden recorrerse de manera autoguiada utilizando las audioguías elaboradas por la Municipalidad en [Spotify](https://open.spotify.com/show/0TghWaTuOn4LAnbja2YuMC?si=71188c96e1d64d2c) o descargarlas desde la web de [Santa Fe Capital](https://santafeciudad.gov.ar/secretaria-de-produccion-y-desarrollo-economico/turismo/mi-ciudad-como-turista/).

**Dos eventos centrales**

Durante el fin de semana largo también habrá importantes propuestas culturales gratuitas en diferentes escenarios de la ciudad. Así desde el jueves 18 al domingo 21 se podrá disfrutar de Capital de la Música, un nuevo programa de la Municipalidad para fortalecer a músicos locales.

En el lanzamiento del programa se podrá disfrutar de recitales, en dos escenarios montados en la Estación Belgrano -Bv. Gálvez 1150-, muestra de Luthiers y capacitaciones sobre gestión cultural y comunicación. Los recitales comenzarán a partir de las 18 y se podrá disfrutar de solitas y bandas de la talla de Maca Revolt, Tavo Angelini, Astro Bonzo y Experimento Negro, entre otros artistas. La programación completa se puede consultar haciendo click [aquí](https://www.santafeciudad.gov.ar/capitalcultural/).

Otro de los destacados del fin de semana largo es la inauguración del Salón Litoral 2021 en el Museo Municipal de Artes Visuales “Sor Josefa Díaz y Clucellas”. El Salón quedará inaugurado el jueves 18 a las 20 y se podrá visitar durante el fin de semana largo de 17 a 20. Después de cuatro décadas, la Municipalidad recupera el evento de gran prestigio para la ciudad y la región. En la apertura se entregarán el Premio Adquisición, cinco premios estímulo a la producción artística y las menciones honoríficas a obras seleccionadas por el Jurado del certamen.

**Somos Música y Eureka en Bandada**

La muestra[ “María Elena Walsh para todos” de Barrilete Museo de los Niños](https://santafenoticias.gob.ar/una-muestra-sobre-maria-elena-walsh-llega-a-la-estacion-barranquitas/), de Córdoba, se podrá visitar hasta el sábado 20 a las 19 horas, en la Estación Barranquitas (Juan del Campillo y Terraplén Irigoyen). Esta tarde, habrá música en vivo para despedir esta exposición que llegó a la ciudad a partir de una organización conjunta entre la Municipalidad, Barrilete y la Asociación Espacios Educativos. El ensamble del programa de orquestas para las infancias Somos Música, que se reúne semanalmente en la Estación, interpretará bajo la dirección de Victoria Díaz Geromet un breve repertorio de canciones que aprendieron en este ciclo y dos obras del repertorio de María Elena Walsh, que prepararon para esta ocasión.

Después llegará Eureka en Bandada, el grupo que integran cinco payasas -Eureka, Infinita, Bony, Troncha y Lupita-, con una propuesta que entrelaza el juego con las artes circenses, la percusión corporal, el canto colectivo y el intercambio espontáneo que sucede con el público. En sus presentaciones interpretan canciones populares de María Elena Walsh, Santos Tala, Jaime Ross, entre otros, y composiciones propias. La idea es de Maia Casas, también letrista, cantante, percusionista y malabarista; acompañada por Stella Sánchez en bajo, percusión y voz; Analía Bosque, en acordeón, guitarra, voz, arreglos y composiciones; Lorena Ledesma, en percusión y como letrista; y Silvia Salomone, en guitarra, percusión y voz.

Durante la semana, la muestra se podrá visitar con turno previo: de martes a viernes a las 10, a las 12, a las 14 y a las 16; y los sábados, a las 14 y a las 16 horas. Para solicitarlo por Whatsapp, comunicarse al número 3425064024 o por correo electrónico, escribiendo a cultura@santafeciudad.gov.ar.

**El Teatro Municipal, a pleno**

La cartelera del Teatro Municipal reúne esta semana conciertos, espectáculos de humor, teatro y danza. El colectivo de Artistas Líricos de Santa Fe organiza junto a la Municipalidad el “Concierto del reencuentro”, que forma parte del Festival Nacional “Líricos a la gorra”, que impulsan Cantantes Líricos Asociados de la República Argentina (CLARA) durante este mes, en todo el país. Será el miércoles 17 de noviembre a las 20 horas, con entrada libre y gratuita. Se podrán retirar hasta dos pases por persona en la boletería de San Martín 2020.

Para este encuentro, cantantes de distintos puntos de la provincia llegan a la ciudad para interpretar reconocidas obras del repertorio lírico universal. Participan: Lucía Maidana, Betina Briasco, Eliana Bertinetti, Cecilia Vaca Cardozo, Andrés Novero, Ismael Barrile, David Laborie, Juliana Sturba, Lucila Cova, Viviana Barrios, Carolina Basualdo, Mariano Sanz, Joaquín Mendoza, José Zanaschi y Susana Caligaris, quienes junto a los demás cantantes se unirán para algunos números corales, con el acompañamiento de reconocidos pianistas como Mario Spinozzi, Luciano Stizzolli, Martín Salum y Gabriel Gredilla.

Pela Romero llega con su unipersonal, “Basta de amores de mierda”, el jueves 18 a las 20 horas. “Cómo detectar, evitar o salir de relaciones tóxicas. Esas que abundan tanto hoy en día entre los seres humanos”, son las claves en las que se presenta el espectáculo. Las entradas se venden en la boletería del Teatro y en forma online por plateavip.com.ar: tertulias laterales sin ubicación, $1200; tertulia tercio medio con ubicación, $1300; palcos altos con ubicación, $1400; plateas y palcos bajos con ubicación, $1500.

El IX Concierto Homenaje “De Municipales para Municipales” será el viernes 19 a las 20.30 horas, con la participación de la Banda Sinfónica y el Coro Municipal. Esta celebración organizada cada año por Asoem se extiende con una invitación especial a todos los trabajadores y trabajadoras de Santa Fe, Recreo, Monte Vera, Arroyo Leyes, Rincón. Durante el evento, se hará entrega de los premios y menciones especiales del VIII Concurso Fotográfico “Construyendo la cultura del trabajo”.

En el concierto, la Banda Sinfónica y el Coro harán un recorrido por las tres etapas que tuvieron en sus 40 años de historia, distinguidas cada una por quien estuvo a cargo de la dirección: el Maestro Jorge Chiappero Favre, en los orígenes del organismo; el Maestro Juan Rodríguez, entre 2009 y 2019; Víctor Malvicino y Omar Lacuadra, en la actualidad.

El repertorio incluirá temas interpretados solo por la Banda, como “Persis”, de James L. Hosay y “Libertango”, de Astor Piazzolla; bajo la dirección de Malvicino; “My spirit sang all day”, de Gerald Finzi y “Campo Afuera”, de Carlos Di Fluvio, bajo la dirección del Mtro. Juan Barbero y del codirector Jorge Senn, respectivamente. Con la conducción del Mtro. Omar Lacuadra, los dos organismos compartirán escenario en un arreglo de “El olvidao”, de Hugo Garnica, que realizó José López y fue premiado en el Concurso de Composición y Arreglos “Ariel Ramírez”, que organizó este año la Municipalidad. En el cierre, y como homenaje a ese gran compositor santafesino, se escuchará “Santafesino de veras”, la canción que compuso junto a Miguel Brascó.

La programación de esta semana en la Sala Mayor finaliza el domingo a las 20, con la muestra anual de danzas folklóricas israelíes del Círculo Israelita Macabi. Las entradas generales se venden a $500.

En tanto, en la Sala Marechal el viernes a las 21, Patricia Álvarez interpreta una nueva función de “Montaña verde rodea nube blanca”, con dirección general de Mariano Dufour. En esta experiencia performática aborda “las transformaciones de lo vital (soplo) en un cuerpo, entre sus relaciones con lo otro (oriental/occidental), la naturaleza, la relación con su deseo”. Las entradas generales se venden a $450.

Los domingos 21 y 28 a las 21, en la Sala Marechal se presenta “La Invocación. Versión absurda de la historia de un invento que cambió nuestras vidas”. Serán las últimas funciones de esta puesta interpretada por Eduardo Fessia, Gabriela Feroglio, Miguel Pascual y Ruy Gatti, con dramaturgia de Sebastián Roulet, también a cargo de la dirección con María Flavia del Rosso. La soledad, las supersticiones, el amor, las divisiones políticas, y un invento revolucionario, son los ejes que atraviesan esta obra seleccionada en la categoría Producción Emergente por el jurado de Escena Santafesina 2019. Las entradas anticipadas y con descuento para estudiantes y jubilados, se venden a $400; mientras que el día de la función costarán $500.

**La H No Murió**

El viernes 19 a las 21, La H No Murió llega a Santa Fe con su homenaje a Hermética. El cantante Claudio O’ Connor y el guitarrista Tano Romano, miembros originales de la legendaria banda del metal argentino, hacen este tributo junto con el bajista Karlos Cuadrado y el baterista Javier Rubio en el anfiteatro del Parque del Sur.

Las entradas se venden a $2000 en Santa Fe Rock Tienda (San Martín 2347, Galería Florida, local 40, Santa Fe), Terco Tour (25 de Mayo 453, Paraná) y online www.entradasclandestinas.com En puerta, se podrán adquirir a $2300.

**Paseos turísticos y actividades deportivas**

Santa Fe es una ciudad con mucha historia y parte de ella se puede disfrutar en los museos y espacios culturales que también ofrecen una gran cantidad de propuestas para este fin de semana.

En la Casa de los Gobernadores se ofrecen visitas guiadas teatralizadas con entrada libre y gratuita. La casa es un antiguo palacete ubicado sobre el emblemático Bv. Gálvez al 1200 que se puede visitar los domingos a las 17, 18 y 19. Cada salida tiene un cupo máximo de 30 personas y se organizan los grupos por orden de llegada. En tanto en la Casa del Brigadier – Av. Gral López 2792 – las visitas guiadas gratuitas los sábados, domingos y feriados son a las 16.30, 17.30 y 18.30. En este caso se debe reservar turno [aquí](https://www.instagram.com/casadelbrigadier/).

Y, finalmente, otro de los imperdibles de la capital provincial es la visita guiada al Paseo de la Cervecería. Es un recorrido que incluye la visita a una de las plantas más importantes de Latinoamérica donde se producen varias marcas internacionales (además de las marcas locales), al Museo de la Cerveza y el recorrido por el Patio de la Cervecería que rememora los antiguos Patios Cerveceros donde se popularizó el Liso santafesino a comienzos del siglo XX. Este recorrido es gratuito y se puede hacer de martes a sábado y se debe reservar turno al (0342) 156123666.

Quienes este fin de semana deseen disfrutar al aire libre pueden recorrer la Reserva Ecológica UNL ubicada en Costanera Este. Las visitas son sábado y domingo de 9.30 a 17.30 con entrada libre y gratuita.

También en la Laguna Setúbal se podrá disfrutar de paseos en kayaks contratando los servicios de las empresas locales que ofrecen paseos con todas las medidas de seguridad. Para más información se puede consultar [aquí](https://www.instagram.com/kayvaisantafe/) o en [este link](https://www.instagram.com/setubal_kayaks/).

Otra forma de disfrutar la Laguna es practicando SUP – Stand Up Paddle -. Este fin de semana la Escuela de SUP Santa Fe tendrá, además de las salidas y clases regulares, el evento Luna Llena, una invitación para disfrutar de la caída del sol desde el agua, meditar y luego disfrutar de un fogón con música y gastronomía. Para más información comunicarse al teléfono 342476011.

Finalmente las Agencias de Viajes y Turismo locales ofrecen diferentes excursiones, paseos turísticos y paquetes que incluyen alojamiento. Para más información contactarse con [Mirá Santa Fe ](https://mirasantafe.costanera241.tur.ar/)y [Expertur](mailto:expertur@expertur.tur.ar).

**Información Turística**

Durante el fin de semana largo los Centros de Informes Turísticos de la Municipalidad de Santa Fe funcionarán con horarios especiales. La oficina ubicada en Terminal de Ómnibus (Belgrano 2910) abrirá el viernes de 7 a 20 y sábado, domingo y lunes de 8 a 20. En tanto la oficina ubicada en el Dique 1 del Puerto funcionará el viernes de 8 a 20 y sábado, domingo y lunes de 10 a 19. Por último el Centro de Informes de Peatonal San Martín 2020 atenderá al público el viernes de 7 a 13 y de 16 a 20; el sábado de 9 a 14 y de 15 a 20 y finalmente el lunes lo hará con horario corrido de 10 a 16.