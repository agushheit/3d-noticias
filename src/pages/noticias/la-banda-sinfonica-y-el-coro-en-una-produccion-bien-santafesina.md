---
category: La Ciudad
date: 2020-12-30T11:17:52Z
thumbnail: https://assets.3dnoticias.com.ar/3012-coro-municipal.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: La Banda Sinfónica y el Coro, en una producción bien santafesina
title: La Banda Sinfónica y el Coro, en una producción bien santafesina
entradilla: Los organismos estables de la Municipalidad despiden el año con la interpretación
  de «Santafesino de veras». La producción audiovisual es parte de los festejos por
  el centenario de Ariel Ramírez.

---
Con diversas estrategias y producciones que se viralizaron en las redes sociales, la Banda Sinfónica y el Coro de la Municipalidad siguieron activos durante este año, con ensayos y trabajos en los que interactuaron con otras instituciones y organismos que dependen de la Secretaría de Educación y Cultura. En el cierre de una etapa tan particular, protagonizan la grabación de «Santafesino de veras», de Ariel Ramírez, con letra de Miguel Brascó.

El tema se registró en el estudio profesional de la Mediateca, conformando pequeños grupos de instrumentistas y coreutas para cumplir con los protocolos sanitarios.

También participaron de la filmación en locaciones de la capital, que acentúan los versos de la canción con imágenes en el Puente Colgante, en el Jardín Botánico, en el Parque del Sur, en el Parque del Museo de la Constitución y en la sala Mayor del Teatro Municipal.

Lilia Salsano se sumó a esta iniciativa como pianista invitada para que estuviera presente la sonoridad de ese instrumento que el maestro Ramírez instaló como protagonista en el lenguaje del folklore argentino.

El estreno será este miércoles 30 de diciembre, a las 21, en la plataforma Capital Cultural, dentro de «Ariel Ramírez: un año para celebrar 100 años», una propuesta que lanzó la Municipalidad en sus redes sociales, en coincidencia con la fecha de nacimiento del compositor.

Anticipando una serie de homenajes, programaciones, rescates y acciones en torno a su figura y a su obra, Francisco Lo Vuolo interpretó en esa ocasión un fragmento de «Santafesino de veras», con el piano E. Pitzer 2751 Leipzig que perteneció a Ramírez y es patrimonio del Museo del Teatro.

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.2em; font-weight: 800">Acciones emblemáticas</span>**

«Esta hermosa producción de los organismos musicales de la ciudad de Santa Fe se suman a dos acciones que para nosotros son emblemáticas», dijo el secretario de Educación y Cultura municipal, Paulo Ricci. «Por un lado, los contenidos generados para la plataforma Capital Cultural, que a seis meses de su lanzamiento ya cuenta con un catálogo de producciones culturales y educativas de excelencia; y por otro lado, la celebración del centenario de Ariel Ramírez, que iniciamos el 4 de septiembre y durante todo 2021 nos dará la posibilidad de revisitar la figura y la obra de ese inmenso compositor e intérprete santafesino».

Y remarcó que la calidad y el profesionalismo de las producciones de la Banda Sinfónica y el Coro Municipal de Santa Fe «nos llenan de orgullo como santafesinos porque comprobamos una vez más el nivel de nuestros músicos y el compromiso que han tenido durante todo este año para adaptarse y seguir generando propuestas innovadoras».

***

![](https://assets.3dnoticias.com.ar/3012-coro-municipal1.webp)

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.2em; font-weight: 800"> Un hito en el repertorio</span>**

Esta versión de «Santafesino de veras», opera también como un homenaje a músicos que integraron el grupo fundador de la Banda Sinfónica Municipal, en 1981. Así lo consideró el actual director, Omar Lacuadra, en referencia a Jorge Chiappero Favre que introdujo el tema en el repertorio del organismo, con arreglos propios; y a Juan Rodríguez, que lo mantuvo en los años que fue director.

«Desde hace mucho tiempo esta canción es un hito en nuestro repertorio, con arreglos para cada instrumento. La filmación en lugares emblemáticos de la ciudad corona la música con imágenes que potencian el mensaje de este proyecto», señaló.

Para el Coro, que tiene entre sus compositores de referencia a Ariel Ramírez y Carlos Guastavino, «los arreglos son sencillos porque están pensados para interactuar con la Banda, que sean efectivos y la melodía se entienda perfectamente», agregó Juan Barbero, a cargo del conjunto vocal.

***

![](https://assets.3dnoticias.com.ar/3012-coro-municipal2.webp)

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.2em; font-weight: 800"> Música en cuarentena</span>**

Para estos organismos musicales que tienen su actividad centrada en las actuaciones en vivo, la pandemia obligó a encontrar nuevos caminos para llegar al público. En las primeras semanas del aislamiento preventivo idearon la manera de seguir haciendo música, con los recursos que tenían en sus casas y el trabajo en línea con editores, como Federico Teiler, que participó como técnico de la Mediateca.

Así, la Banda pudo estrenar durante el mes de mayo «Yorkshire Ballad», de James Barnes; y «Con aire», de Nahuel Ramayo; mientras el Coro registró su versión de «Y dale alegría a mi corazón», de Fito Paez, con la colaboración de integrantes de la Banda.

Con la presentación de la plataforma Capital Cultural, a mediados de 2020, y los protocolos que se habilitaron para el registro fílmico y sonoro, los organismos contaron con mejores recursos para las realizaciones. En pequeños ensambles de instrumentos, los músicos grabaron en la Mediateca; y el Coro se integró a la Escuela de Danzas del Liceo Municipal, en otra producción que celebró el Día del Folclore.

«Extrañamos los conciertos en vivo, sobre todo los didácticos, porque son una actividad esencial para la difusión del canto coral, pero reconocemos también que dentro de nuestras posibilidades pudimos llegar al público con nuevos formatos», valoró Juan Barbero.

Omar Lacuadra resumió este año al frente de la Banda, que dirige junto a Víctor Malvicino, como una etapa de «reinvención» en la que las redes sociales les permitieron potenciar la llegada del organismo. «En 2021 cumplimos 40 años. Somos de las únicas Bandas Municipales que hay en el país, junto con las de Córdoba, Mar del Plata y Buenos Aires; compartiendo además la escena local con muchos organismos profesionales», dice con orgullo anticipando un año de festejos y nuevos proyectos.