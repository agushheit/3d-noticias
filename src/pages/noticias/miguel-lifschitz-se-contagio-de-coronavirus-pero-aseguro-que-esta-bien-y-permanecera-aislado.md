---
category: Agenda Ciudadana
date: 2021-04-12T06:15:15-03:00
thumbnail: https://assets.3dnoticias.com.ar/miguelito.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Miguel Lifschitz se contagió de coronavirus, pero aseguró que está bien y
  permanecerá aislado
title: Miguel Lifschitz se contagió de coronavirus, pero aseguró que está bien y permanecerá
  aislado
entradilla: El ex gobernador santafesino y actual titular de la Cámara Baja provincial
  afirmó que se encuentra en buen estado general.

---
El ex gobernador y actual presidente de la Cámara de Diputados de la provincia, Miguel Lifschitz confirmó a través de su cuenta en Twitter que contrajo coronavirus. "Ayer comencé con algunos síntomas y hoy me hice el test de Covid19 y me dio positivo. Estoy bien en general. Por unos días estaré aislado", posteó en la tarde de este domingo el funcionario en su cuenta oficial de las redes sociales.

Todo el arco de funcionarios, que comenzó a interiorizarse de la novedad, le deseó al ex titular de la casa gris, pronta recuperación. El ex mandatario se contagió al igual que el actual titular del Ejecutivo santafesino, Omar Perotti quien el 3 de diciembre también confirmó su resultado positivo a través de las redes sociales.

"Quiero contarles que me confirmaron el diagnóstico positivo de COVID-19. Me encuentro bien, seguiré cumpliendo con el aislamiento y siempre atento a los consejos médicos, continuaré trabajando desde mi casa. Les pido a todos y todas que se cuiden y cuiden a sus familias.", había escrito en su cuenta oficial Perotti el pasado 3 de diciembre.

Además de ambos mandatarios, tanto miembros del gabinete del actual gobernador como diputados provinciales debieron resguardarse tras haberse infectado de Covid19. La confirmación de Lifschitz se da en un contexto de una suba pronunciada de los casos tanto a nivel nacional, como en Santa Fe.