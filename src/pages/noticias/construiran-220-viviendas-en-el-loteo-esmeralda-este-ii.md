---
category: La Ciudad
date: 2021-01-11T11:23:10Z
thumbnail: https://assets.3dnoticias.com.ar/frana-perotti.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: '3DNoticias - Fuente: El Litoral'
resumen: 'Loteo Esmeralda Este II: se construirán 220 viviendas'
title: Construirán 220 viviendas en el loteo Esmeralda Este II
entradilla: Se licitarán en cuatro grupos diferentes e incluirá infraestructura básica.
  El presupuesto oficial total ronda los 800 millones de pesos. Se edificarán dúplex
  de dos dormitorios.

---
El Ministerio de Infraestructura, Servicios Públicos y Hábitat, a través de la Dirección Provincial de Vivienda y Urbanismo, licitará las obras para construir 220 viviendas con infraestructura básica en el loteo Esmeralda Este II de la ciudad de Santa Fe.

Las licitaciones se desarrollarán en tres grupos de 80, 68, y 72 viviendas y un grupo para la infraestructura básica, y se realizarán entre el 28 y 29 de enero del corriente en el Centro Cultural Provincial Francisco Paco Urondo, ubicado en calle Junín 2457.

Al respecto, el director Provincial de Vivienda y Urbanismo, José Kerz, brindó detalles de estas licitaciones: «luego de un año muy complicado, iniciamos este 2021 con obras muy importantes, como este grupo de licitaciones para construir 220 viviendas en Esmeralda Este II. Las viviendas van a hacer dúplex de dos dormitorios, y otras adaptadas para discapacitados motrices desarrolladas en planta baja. Todas van a contar con la infraestructura básica, es decir, con las instalaciones de agua, luz, cloaca y gas».

«Desde el gobierno provincial, y en particular, desde la Dirección de Vivienda, tenemos la voluntad y la firme decisión de generar las políticas y las estrategias que garanticen el acceso a la casa propia y a un hábitat digno con los servicios e infraestructura, porque entendemos que es un derecho fundamental tienen todos los santafesinos», subrayó Kerz.

Por su parte, el secretario de Hábitat, Urbanismo y Vivienda, Amado Zorzón, destacó: «estas licitaciones, cuyo presupuesto total está cerca  de los 800 millones de pesos, tienen que ver con la firme decisión política del gobernador Omar Perotti y la ministra Silvina Frana, de poder brindar soluciones concretas a la alta demanda habitacional, con un Estado presente, generando más oportunidades para que cada familia pueda cumplir con el sueño de la casa propia».

<br/>

## **DETALLES DE LAS OBRAS**

Con un presupuesto oficial total estimado en $ 795.848.947, las viviendas se desarrollarán en dúplex y otras adaptadas para discapacitados motrices desarrolladas en planta baja. Los prototipos varían de acuerdo al ancho del terreno de emplazamiento, proyectándose en terrenos con un ancho de 3,60 m. 4,00 m. y 5,00 m. dispuestos según las características propias de la manzana.

Las viviendas tipo dúplex constan en planta baja de estar comedor, cocina con mesada, lavadero y baño, en planta alta 2 dormitorios y baño completo, poseen aberturas de aluminio exterior y de madera interior, cielorrasos de placa de roca de yeso y termotanque solar. 

Se entregan totalmente terminadas para ser habitadas, pintadas y con todas las instalaciones de los servicios de agua, luz, cloaca y gas en funcionamiento. También se incluye el prototipo VCD de 2 dormitorios que está destinado para discapacitados motrices, que se desarrolla íntegramente en planta baja cumpliendo con todos los requerimientos para este fin.

<br/>

## **GRUPO DE LICITACIONES**

\-Licitación Pública 09/20: ejecución de 80 viviendas prototipos «VDXA 5.0M», «DU 3.6M» y «VCD» de 2 dormitorios; Presupuesto oficial $ 273.168.147,20; Apertura de ofertas: jueves 28/01/21, a las 9 horas.

\-Licitación Pública 10/20: ejecución de 68 viviendas prototipos «VDXA 5.0M», «DU 3.6M» y «VCD» de 2 dormitorios; Presupuesto oficial $ 231.865.347,88; Apertura de ofertas: jueves 28/01/21, a las 11 horas.

\-Licitación Pública 13/20: ejecución de 72 viviendas prototipos «VDXA 5.0M», «DU 3.6M» y «VCD» de 2 dormitorios; Presupuesto oficial $ 247.127.575,32; Apertura de ofertas: viernes 29/01/21, a las 9 horas.

\-Licitación Pública 15/20: reacondicionamiento infraestructuras básicas y ejecución de red cloacal de conjunto; Presupuesto oficial $ 43.687.876,66; Apertura de ofertas: viernes 29/01/21, a las 11 horas.