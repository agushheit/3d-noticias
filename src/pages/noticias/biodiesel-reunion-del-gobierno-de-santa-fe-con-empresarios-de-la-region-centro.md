---
category: El Campo
date: 2021-04-01T08:03:08-03:00
thumbnail: https://assets.3dnoticias.com.ar/biodisel.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: 'Biodiesel: Reunión del gobierno de Santa Fe con empresarios de la Región
  Centro'
title: 'Biodiesel: Reunión del gobierno de Santa Fe con empresarios de la Región Centro'
entradilla: Durante el encuentro se coincidió en la necesidad de avanzar en la sanción
  definitiva de la prórroga del actual Régimen de Regulación y Promoción para la Producción
  y Uso Sustentables de Biocombustible (Ley 26.093).

---
Una delegación del Gobierno de Santa Fe encabezada por el senador nacional Roberto Mirabella, junto a la ministra de Ambiente y Cambio Climático de la Provincia, Erika Gonnet; la secretaria de Gestión Federal, Candelaria González del Pino; y el secretario de Industria Claudio Mossuz; recibió en la Casa Gris al Foro de Empresarios de la Región Centro Capítulo Santa Fe, en un encuentro donde se analizó la situación del Biodiesel.

Durante la reunión, se coincidió en la necesidad de avanzar en la sanción definitiva de la prórroga del actual Régimen de Regulación y Promoción para la Producción y Uso Sustentables de Biocombustible (Ley 26.093) que vence en mayo para, a partir de allí, poder trabajar en conjunto en un nuevo Plan Nacional para el sector.

El proyecto para prorrogar el régimen de promoción actual hasta el 31 de diciembre de 2024 ya obtuvo en octubre y por unanimidad la media sanción del Senado pero aún continúa en consideración de la Cámara de Diputados y no ha bajado al recinto.

Por parte del Foro de Empresarios de la Región Centro Capítulo Santa Fe participaron del encuentro: Maximiliano Ferraro, coordinador del Foro y vicepresidente de Asociación Civil de Entidades Empresarias del Sur Santafesino (ADEESSA); Víctor Sarmiento, presidente de la Federación Industrial de Santa Fe; Germán Dobler, Gerente General en la Bolsa de Comercio de Santa Fe; Enrique Lasgoity en representación de la Bolsa de Comercio de Rosario; Javier Martín por la Unión Industrial de Santa Fe; Sergio Bernardi, titular de la Cámara Industrial de Extrusado y Prensado Santa Fe (CIDEP). En tanto, también formó parte del encuentro Marcelo Cogno, director de Infraestructura y Fortalecimiento Industrial del Ministerio de Producción de la Provincia.

Durante el encuentro, el senador nacional Roberto Mirabella exhortó a "poder discutir con tranquilidad un nuevo marco regulatorio y una nueva ley que sea tan innovadora, avanzada y audaz como fue la ley que se impuso en el año 2006".

![](https://assets.3dnoticias.com.ar/biodisel1.jpeg)

Además, recordó que antes de la vigencia del actual esquema normativo "este sector del biocombustible no existía en la Argentina hace 20 años atrás" y resaltó que en cambio "hoy en la Argentina es uno de los jugadores principales a nivel mundial junto con Estados Unidos, Brasil, Finlandia, Alemania en la producción de bio".

En su alocución, Mirabella también indicó: "tengo una posición tomada frente a este tema". "Mi posición política frente a este tema es esa: votar la prórroga", subrayó.

Destacó, a su vez, que en la cámara alta "no solamente votamos la prórroga hasta el año 2024 en el Senado de la Nación sino que además yo en particular presenté un proyecto de ley, un nuevo Plan Nacional de Biocombustibles, para los próximos 15 años".

Asimismo, la ministra de Ambiente y Cambio Climático de la Provincia, Erika Gonnet, sostuvo que junto "a las cuestiones económicas que están atravesando este tema" señaló "la preocupación que tenemos desde nuestro ministerio en que se le de visibilidad a la temática de biocombustibles y a su producción desde nuestra Provincia" ya que "tenemos un compromiso, desde nuestro proyecto de ley de Acción Climática, en bajar el nivel de emisiones". "Y el uso de biocombustible es un actor fundamental en esto", aseguró.

A su turno, la Secretaria de Gestión Federal, Candelaria González del Pino, se refirió a la importancia de la reunión en la que "tomamos conocimiento de primera mano de los planteos que el foro y los distintos sectores realizan como así también poner en valor la media sanción que hoy está en Diputados para prorrogar la ley de promoción de los biocombustibles y poder pensar a futuro una nueva ley para un nuevo régimen para una nueva realidad". La funcionaria provincial hizo especial hincapié en que la ley vigente "diversificó la matriz energética" y que también "generó muchísimo empleo de calidad".

Por otro lado, los empresarios destacaron la importancia de la reunión y agradecieron al gobierno de la Provincia por la convocatoria. En ese sentido, el coordinador del Foro Capítulo Santa Fe, Maximiliano Ferraro, expresó que fue "una reunión fue muy productiva" y remarcó que "la convocatoria realizada desde el Gobierno realmente está acorde a la necesidad del sector empresario de la región centro". "Vamos a trabajar en conjunto con el gobierno para lograr la prórroga de la ley que hoy es estratégica no solo para Santa Fe, Córdoba y Entre Ríos sino para 10 provincias argentinas y obviamente para todo el país", confió.

El Presidente de la Federación Industrial de Santa Fe, Víctor Sarmiento, consideró, además, que es "fundamental tener la posibilidad de una prórroga" debido a que "prácticamente ya está venciendo el plazo y creemos desde este lugar que hay que tomarse el tiempo para hacer una ley que contenga a todos los sectores y especialmente a las pymes".

En igual sentido se manifestó Enrique Lasgoity de la Bolsa de Comercio de Rosario y que participa por la Bolsa en el Consejo Agroindustrial Argentino en el tema biocombustibles. "Me parece muy importante la reunión para un discusión amplia y franca sobre los problemas de los biocombustibles donde pudimos expresar nuestras opiniones intercambiar ideas y ver los cursos de acción para lograr en principio la prórroga de la ley por un período adecuado para poder rediscutir posibles modificaciones de una industria tan importante como la de biocombustibles", concluyó.