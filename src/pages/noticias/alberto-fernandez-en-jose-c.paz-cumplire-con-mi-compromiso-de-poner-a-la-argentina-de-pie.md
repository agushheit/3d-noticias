---
category: Agenda Ciudadana
date: 2021-09-23T06:00:35-03:00
thumbnail: https://assets.3dnoticias.com.ar/JOSECPAZ.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: 'Alberto Fernández, en José C. Paz: "Cumpliré con mi compromiso de poner
  a la Argentina de pie"'
title: 'Alberto Fernández, en José C. Paz: "Cumpliré con mi compromiso de poner a
  la Argentina de pie"'
entradilla: El Presidente se refirió a la actitud de la oposición ante las nuevas
  medidas anunciadas el martes al encabezar el acto de inauguración de las instalaciones
  de la nueva sede de la Universidad Nacional de José C. Paz.

---
El presidente Alberto Fernández reiteró que el gobierno nacional ha oído "el mensaje" de las urnas y que por ese motivo "redoblará los esfuerzos" para recuperar los puestos de trabajo y acercar las universidades a la gente con un sentido federal y pidió a los empresarios que inviertan y produzcan para ayudarlo a volver a "poner a la Argentina de pie".

El mandatario dio este mensaje al encabezar este miércoles la inauguración de la Facultad de Medicina de la Universidad de José C. Paz, el primer acto tras la jura de los nuevos ministros del Gabinete nacional, con gran concurrencia de militantes.

Acompañado por el intendente local Mario Ishii, Alberto Fernández remarcó la necesidad de seguir construyendo casas de altos estudios “cercanas, para que todos los argentinos encuentren la posibilidad de desarrollarse” en su lugar de origen.

Fernández había llegado al lugar minutos después de las 12 y caminó varios metros rodeado de funcionarios y escoltado por la fanfarria de la Escuela de Suboficiales del Ejército Sargento Cabral.

En una postal que no tiene registro en los últimos meses, el Presidente realizó el trayecto hasta el escenario acercándose reiteradamente a las vallas para saludar a la militancia que llegó hasta el nuevo edificio de la Facultad, ubicado en Héctor Arregui 689 de esa localidad del noroeste bonaerense.

Ya en el escenario, el Presidente elogió a intendente Ishii, a quien le destacó “un tesón inquebrantable por darle educación a su pueblo”, en referencia a la UNPaz, la universidad que es el corolario de ese trabajo y cuyas obras se realizan con impulso municipal.

La casa de estudios cuenta actualmente con 35 mil alumnos: 67 por ciento mujeres y 85% primera generación de universitarios.

“Mario tuvo en Néstor, en Cristina y tiene en mí, un socio decidido para que la educación llegue a cada argentino, a cada argentina, en todo el país”, señaló el Presidente, al tiempo que destacó que hay “que construir universidades cercanas para todos los argentinos, donde encuentren la posibilidad de desarrollarse”.

Por otra parte,Fernández calificó al domingo en el que se desarrollaron las primarias abiertas, simultáneas y obligatorias (PASO) como “aciago”, pero destacó la capacidad del Gobierno para “escuchar” lo que se le reclamó.

"Cumpliré con mi compromiso de poner a la Argentina de pie", dijo tras afirmar: "Hemos oído el mensaje".

Allí fue cuando sostuvo que, a diferencia de lo que hizo el macrismo cuando perdió elecciones, que “cerró” el “Ministerio de Salud, el de Trabajo y el de Ciencia y Técnica”, el suyo se dedica a abrir universidades.

“Todos nosotros estamos convencidos, otros no, que definitivamente hay un país donde todos tenemos que tener un lugar de desarrollo”, sostuvo y pidió “empresarios comprometidos que no especulan y que inviertan” para “que los argentinos recuperen el trabajo”.

“A la semana del resultado electoral yo no cerré el Ministerio de Trabajo.Lo que hice fue promover el aumento del salario mínimo para que todos los salarios crezcan y le ganen a la inflación”, dijo y afirmó que se deben “cambiar planes sociales por trabajo”.

**La oposición y las medidas sanitarias**

Respecto a la oposición, el Presidente también hizo un contrapunto entre lo que decían cuando eran los días de aislamiento y lo que dicen ahora, cuando comienzan a mesurarse las restricciones.

"Ahora, nos acusan de libertinos los que nos acusaban de encerradores", dijo Fernández en relación a las críticas de las medidas respecto a la flexibilización sanitaria, yrecordó cuando en el peor momento de la pandemia cuestionaban hasta con marchas las medidas.

**Inauguración**

Fernández inauguró 20.370 metros cuadrados cubiertos para la Universidad, pensados para unos 21.600 alumnos, con 94 aulas en la planta baja de gran tamaño y sector de morgue o aula de técnica con conexión propia al exterior y baños.

En la planta alta, se distribuyen siete laboratorios que se utilizarán para Investigación Micro, Química Orgánica, Enfermería, Fisiología, Nutrición, Terapia Intermedia, Paro cardíaco y Medicina Complementaria, Simulación y Microbiología.

Antes que el Presidente, hizo uso de la palabra el gobernador Axel Kicillof, quien afirmó que la inauguración de hoy viene “a desmentir esa visión prejuiciosa” que quieren hacer creer los opositores cuando sostienen “que las universidad no son para todos”.

“Los sectores vulnerables van a la universidad, la universidad tiene que ir a los sectores vulnerables. Tiene que estar cerca, tiene que ser gratis y tiene que ser para todos y todas”, afirmó.

Ishii, por su parte, sostuvo que, con esta inauguración “ya no hay excusas” ya que los vecinos “pueden llegar caminando” hasta la facultad.

El jefe comunal se refirió al trabajo realizado por la Nación, la Provincia y por el municipio durante la pandemia luego de que la actual oposición “dejara un país hecho hilachas”.

También criticó a los medios de comunicación por suministrarle “tanto veneno a la población” y pidió que “dejen gobernar” al Presidente.

Finalmente, el rector Darío Kusinsky agradeció el trabajo de Ishii, quién “siempre tuvo claro que la educación y la salud son fundamentales para el progreso de su pueblo”; hizo lo propio con el Presidente y con el gobernador, por el “importante crecimiento fruto de la confianza que tienen en las instituciones” de altos estudios; y dedicó un párrafo a “Cristina y Néstor” Kirchner, quienes “impulsaron la creación” de la universidad “de José C. Paz "y tantas otras".

En el escenario también estuvieron presentes el jefe de Gabinete, Juan Manzur; el ministro de Educación, Jaime Perczyk; el gobernador bonaerense, Axel Kicillof; y el diputado Máximo Kirchner, entre otros.

Asistieron además la vicegobernadora bonaerense Verónica Magario, el Jefe de gabinete de la provincia, Martín Insaurralde; ministro de Infraestructura y Servicios Públicos provincial, Leonardo Nardini; los candidatos a diputada Victoria Tolosa Paz y a diputado Daniel Gollan, y los intendentes bonaerenses Fernando Moreira (San Martín), Lucas Ghi (Morón), Federico Achaval (Pilar); Gustavo Menéndez (Merlo); Mariel Fernández (Moreno); Andrés Watson (Florencio Varela); Fernando Espinoza (La Matanza); Mariano Cascallares (Almirante Brown); Leo Boto (Luján), Mauro García (General Rodríguez); Damián Selci (Hurlingham), Julio Zamora (Tigre), Juan Ignacio Ustarroz (Mercedes) y Facundo Diz (Navarro).

También formaron parte de la actividad los ministros de Obras Públicas, Gabriel Katopodis; de Trabajo, Claudio Moroni, y de Transporte, Alexis Guerrera; el secretario general de la Presidencia, Julio Vitobello, y las titulares de la AFIP, Mercedes Marcó del Pont, y de Aysa, Malena Galmarini.