---
category: La Ciudad
date: 2021-02-27T10:00:08-03:00
thumbnail: https://assets.3dnoticias.com.ar/desaguei.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Se presentó una importante obra de pavimentación y desagüe para calle Beruti
title: Se presentó una importante obra de pavimentación y desagüe para calle Beruti
entradilla: Jatón suscribió un convenio con Perotti, en el marco del Plan Incluir.
  La obra prevé dotar a esa importante arteria del noroeste de la ciudad de Santa
  Fe de pavimento y desagüe.

---
El intendente Emilio Jatón y el gobernador Omar Perotti suscribieron un convenio para llevar adelante una importante obra proyectada para calle Beruti, cuya licitación se abrirá próximamente para la ejecución de la primera etapa, de un total de dos. La obra prevé dotar a esa importante arteria del noroeste de la ciudad de Santa Fe de pavimento y desagüe en una extensión superior a las 20 cuadras.

El monto de inversión es de $ 234.119.571,13 con financiamiento provincial, ya que las tareas se enmarcan en el Plan Incluir (decreto N° 2360 de diciembre pasado). En tanto, la Municipalidad asume a su cargo el diseño, la contratación, la inspección y el mantenimiento de la obra.

En la ocasión, el intendente remarcó: “Si hay una obra que es necesaria en la ciudad es ésta; solo basta ponerse diez minutos en esta calle para saber la importancia que tiene para los barrios Los Troncos, Acería y para toda la zona”. Y dijo que “muchas veces en los gobiernos es difícil ser justos, y de la única manera que podemos serlo es cuando discutimos las desigualdades con el objetivo de tratar de equilibrar las ciudades”.

“Hoy es un día importante. No duden que esta obra se va a hacer, se va a terminar y que les va a cambiar la vida en serio. Hoy empieza un proceso, vamos a firmar un convenio, después vendrá la licitación, ustedes nos ayudarán a controlar la obra y finalmente disfrutaremos todos juntos de hacer una ciudad diferente”, les dijo Jatón a los vecinos, en el acto que se realizó en la esquina de Beruti y Viñas, frente a la Iglesia Rayo de Luz del Complejo Educativo “Juan Marcos”, del barrio Acería.

Por su parte, Perotti dijo que “nos gusta firmar acuerdos, abrir licitaciones, con los vecinos en el lugar, cara a cara con ustedes, en el barrio, porque venimos a comprometernos con ustedes en cada obra que anunciamos. A veces se anuncian obras pero no está la plata, acá los recursos están”.

Tras los discursos, el intendente y el gobernador procedieron a firmar el convenio. La obra incluye dos etapas:

\- _Etapa I, entre Arzeno y Camino Viejo: en el tramo de Beruti entre Arzeno y Cafferata se completará el bacheo. Entre Cafferata y Camino Viejo, en tanto, se realizarán trabajos de pavimento de hormigón de 10 metros de ancho junto con los desagües pluviales._

\- _Etapa II, entre Camino Viejo y Furlong: en el tramo de Beruti entre Camino Viejo y pasaje Geneyro se realizará un pavimento de hormigón a dos calzadas, de 7 metros, con un cantero central de 4 metros y desagües pluviales. Por otra parte, en el tramo que se extiende entre pasaje Geneyro y Furlong, se demolerá la carpeta asfáltica existente y se colocará un nuevo pavimento de hormigón, completando con los correspondientes desagües pluviales._

_![](https://assets.3dnoticias.com.ar/etapasa.jpg)_