---
category: La Ciudad
date: 2021-02-01T06:44:12-03:00
thumbnail: https://assets.3dnoticias.com.ar/muni.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad de Santa Fe
resumen: La Municipalidad elaboró un protocolo contra la violencia laboral
title: La Municipalidad elaboró un protocolo contra la violencia laboral
entradilla: 'Con el asesoramiento de la OIT, Santa Fe es el primer municipio del país
  en aprobar un procedimiento de actuación temprana y eficaz para prevenir, sancionar
  y erradicar conductas de violencia y acoso.

'

---
La Municipalidad elaboró un protocolo para erradicar la violencia en el ámbito laboral, constituyéndose en el primer municipio del país en establecer un plan de acción en línea con las disposiciones del Convenio Nº 190 de Organización Internacional del Trabajo (OIT), al que el país adhirió recientemente.

Este instrumento establece un procedimiento de actuación temprana y eficaz para prevenir, sancionar y erradicar conductas de violencia y acoso en el ámbito del trabajo o con conexión con el mismo y que tengan lugar en el seno de la Administración Pública Municipal; y regula la actuación de las diferentes áreas municipales involucradas en su atención.

La iniciativa se concretó en un trabajo conjunto con la Asociación Sindical de Obreros y Empleados de la Municipalidad de Santa Fe (Asoem), contando con la colaboración de la Oficina Argentina de la Organización Internacional del Trabajo (OIT).

“Es una herramienta para mejorar la calidad del trabajo y del empleo público”, sostuvo Federico de los Reyes, director ejecutivo de Función Pública municipal. “A pedido del intendente Emilio Jatón, estamos impulsando un cambio de paradigma respecto a la violencia laboral, que en muchos ámbitos está naturalizada o se invisibiliza. Buscamos poner de relieve que el maltrato, los insultos o cualquier tipo de sometimiento o daño que se genere debe ser corregido y sancionado”, añadió.

“Al igual que con la violencia de género, que en este caso es uno de los tipos de violencia que se puede dar en los ámbitos de trabajo, la violencia laboral obedece a prácticas que se encuentran muy arraigadas en la conducta de trabajadores y trabajadoras. Con el Protocolo se clarifican los circuitos desde que se toma conocimiento de los hechos, pasando por una medida alternativa que ponga fin al conflicto hasta llegar, eventualmente, a la aplicación de una sanción administrativa”, señaló el funcionario.

**Ejes del protocolo**

Entre los puntos más destacados de la iniciativa se prevé llevar a cabo políticas preventivas y campañas de formación, sensibilización y difusión destinadas tanto a empleados y empleadas como a funcionarios y funcionarias de la administración pública municipal a fin de visibilizar, desnaturalizar y erradicar la violencia y el acoso en el trabajo.

Se suman asimismo métodos de solución alternativos para los conflictos y la creación de un Observatorio Municipal sobre Violencia Laboral, de composición paritaria, que tiene por finalidad elaborar políticas o acciones, programas de prevención con objetivos medibles, campañas de sensibilización, programas destinados a abordar factores que aumentan la probabilidad de violencia y acoso en el mundo del trabajo y el contralor sobre legislación municipal que vaya en contra de los principios allí expuestos.

Finalmente establece la incorporación de la temática como contenido obligatorio de los planes de estudio de la Escuela de Administración Municipal.

**Diferentes violencias**

El instrumento detalla los diferentes tipos de violencias o acosos a las que pueden ser sometidos los trabajadores, describiéndolos como comportamientos o prácticas que puedan causar un daño físico, psicológico, sexual o económico, que puede provenir tanto de niveles jerárquicos superiores, del mismo rango o inferiores e incluye la violencia y el acoso por razón de género.

Destaca que los hechos de violencia o acoso pueden generarse por palabras, gestos o acciones u omisiones o cualquier otra forma que tiendan a molestar, sobresaltar o maltratar, degradar, intimidar, denigrar o provocar vergüenza a otra persona, pudiendo, de persistir y avanzar en alguna de las situaciones descriptas, agravarse hasta derivar incluso en la comisión de un delito.

En ese sentido, Soledad Artigas, de la Dirección de Mujeres y Disidencias, destacó: "Este protocolo es un marco jurídico esencial para el abordaje de la violencia de género. Es central que las trabajadoras sepan que cuentan con estas herramientas y, sobre todo, con la decisión política de esta gestión de ser intolerantes a cualquier situación de violencia en el ámbito laboral".

![](https://assets.3dnoticias.com.ar/violencia.jpg)