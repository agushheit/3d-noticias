---
category: La Ciudad
date: 2021-03-26T08:21:13-03:00
thumbnail: https://assets.3dnoticias.com.ar/2ola.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: Martorano se reunió con Jatón para evaluar medidas en conjunto ante la segunda
  ola de contagios por Coronavirus
title: Martorano se reunió con Jatón para evaluar medidas en conjunto ante la segunda
  ola de contagios por Coronavirus
entradilla: En el encuentro se repasaron los escenarios epidemiológicos y la importancia
  de sostener los cuidados personales.

---
Ante el aumento de casos y la llegada de la segunda ola de posibles contagios por Coronavirus, la ministra de Salud, Sonia Martorano, recibió al intendente de la ciudad de Santa Fe, Emilio Jatón, para conformar un plan de trabajo y evitar el incremento de contagios. “Es fundamental sostener el uso de barbijo, el distanciamiento social, lavado de manos y la ventilación de los ambientes”, destacó la funcionaria.

“Llevamos adelante reuniones periódicas donde articulamos políticas sanitarias, repasamos la situación epidemiológica para trabajar de manera conjunta ante el crecimiento de casos”, remarcó la titular de la cartera sanitaria.

A su vez, Emilio Jatón consideró que ha habido “relajamiento en los cuidados individuales y colectivos”, y agregó: “Estamos cerca de la segunda ola, coincidimos con la ministra, por eso es muy importante empezar a trabajar estrategias en conjunto con el ministerio y el municipio”.