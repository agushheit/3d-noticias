---
category: Agenda Ciudadana
date: 2021-01-08T10:25:49Z
thumbnail: https://assets.3dnoticias.com.ar/080121-control.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: El Litoral'
resumen: Perotti espera el DNU de Nación para luego adecuar las horas de circulación
  en Santa Fe
title: Perotti espera el DNU de Nación para luego adecuar las horas de circulación
  en Santa Fe
entradilla: "«Nuestra idea no es restringir actividades, pero lo que habrá que ver
  es la instancia de cómo se disminuye la movilidad social en la provincia», dijo
  el gobernador."

---
En declaraciones públicas, el gobernador Omar Perotti se refirió a las nuevas medidas de toque de queda sanitario nocturno, cuya letra final está definiendo, en estos momentos, el gobierno nacional. «Vamos a ver el decreto, a partir de qué fechas lo está planteando, y con base en eso poner una fecha de inicio y de fin en la provincia de Santa Fe».

«Lo que hasta aquí podemos decir es lo siguiente: nuestra idea no es restringir actividades; creo que todos en cada una de las actividades, de las que sean, de los rubros que sean, han aplicado protocolos, saben cómo cuidarse y demás. Lo que queremos con esto es garantizar, como lo hicimos desde el primer día, que las actividades esenciales, que todos los proveedores, que todo el esquema industrial pueda estar trabajando plenamente. Y eso lo vamos a sostener», aseguró.

No obstante, «lo que habrá que ver es la instancia de cómo se disminuye la circulación, que es lo que se define nacionalmente, y cómo adecuamos esos horarios para la circulación en la provincia», añadió el titular de la Casa Gris.

«No es algo nuevo (la medida de limitación de circulación social). Ya lo hemos visto en distintas etapas (de la pandemia) donde se ha planteado este tipo de decretos». Al respecto agregó que «en el mundo ha quedado probado que la única instancia de freno a la velocidad de contagio es quitar circulación. Con lo cual, no hay ningún secreto frente a las medidas que van a tomarse».

En la reunión por videoconferencia con los gobernadores y el Presidente, Perotti dijo que se le planteó a Alberto Fernández «tratar de tener medidas conjuntas, reservando para cada una de las provincias la posibilidad de adecuarse a su particularidad. **Cada uno en su provincia tiene regiones con muchos casos, con casos crecientes, y algunas que no, entonces que no queden englobados todos en una misma medida**».

«Lo que estamos haciendo es esperar el decreto, ver su redacción final y después proceder a la adecuación de la provincia a ese decreto nacional con las particularidades que se ha dado», concluyó Perotti.