---
category: La Ciudad
date: 2021-08-25T06:00:42-03:00
thumbnail: https://assets.3dnoticias.com.ar/ESTACIONES.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: En las Estaciones se asesorará a vecinos en derechos de usuarios y consumidores
title: En las Estaciones se asesorará a vecinos en derechos de usuarios y consumidores
entradilla: Es una iniciativa que impulsa la Municipalidad con el fin de descentralizar
  los servicios enfocados en los consumidores, usuarios e inquilinos

---
Con el fin de promover y defender los derechos de las vecinas y los vecinos en los diferentes barrios de la ciudad, se pone en marcha el programa “Los derechos en tu barrio”. Se trata de una iniciativa de escucha y asesoramiento en materia de convivencia, derechos humanos y de consumidores que impulsa la Municipalidad y que se llevará a cabo en las Estaciones de la ciudad.

A través de esta propuesta de la Dirección de Derechos y Vinculación Ciudadana, se busca garantizar en cada barrio el acceso a herramientas de promoción y defensa de derechos. Comienza hoy -martes 24- en la Estación de Coronel Dorrego, de 9 a 11.30, y en la de Loyola, de 11.30 a 13.

La iniciativa busca descentralizar los servicios municipales enfocados en derechos ciudadanos con el fin de acercarle a las y los santafesinos la información necesaria para empoderarse como consumidores, usuarios o inquilinos. “Los derechos en tu barrio” tiene como objetivo potenciar lo logrado en esta materia, y llevarlo a cada rincón de la ciudad. En cada lugar, intervendrán especialistas que trabajan desde lo local en estas temáticas.

Al respecto, el director de Derechos y Vinculación Ciudadana, Franco Ponce de León, resaltó: “Desde el comienzo de la gestión fue un objetivo, e inclusive un pedido del intendente Emilio Jatón, brindar herramientas para lograr articular la relación entre el gobierno local con las y los ciudadanos”.

En tal sentido, el funcionario afirmó: “Con esta propuesta, estamos garantizando la vinculación ciudadana, poniendo en el eje la dificultad de algunos sectores para acceder a las oficinas situadas en el centro, sin perder de vista, además, que por lo general quien nos necesita es aquel que ya encuentra alguno de sus derechos vulnerados; esto nos mueve a llevar adelante la escucha activa en las Estaciones, que son espacios de encuentro y convivencia”.

**Cronograma**

Las áreas que participarán de la iniciativa son: Oficina de Información y Protección a Usuarios y Consumidores, la Oficina del Inquilino, Mediaciones Comunitarias, y la Dirección de Derechos Humanos e Integración.

**El cronograma es el siguiente:**

– Martes 24 y 31 de agosto, y 7 de septiembre: de 9 a 11.30 horas en la Estación Coronel Dorrego (Larrea 1735).

– Martes 24 y 31 de agosto, y 7 de septiembre: de 11.30 a 13 horas en la Estación Loyola (Furlong 8400).

– Miércoles 25 de agosto, y 1 y 8 de septiembre: de 9 a 11.30 horas en la Estación Alto Verde (Manzana 6).

– Miércoles 25 de agosto, y 1 y 8 de septiembre: de 11.30 a 13 horas en la Estación Varadero Sarsotti (Tacuaritas S/N).

– Jueves 26 de agosto, y 2 y 9 de septiembre: de 9 a 11.30 horas en el Distrito Noreste (Avenida Aristóbulo del Valle 9260).

– Jueves 26 de agosto, y 2 y 9 de septiembre: de 11.30 a 13 horas en la Estación San Agustín (Fray Francisco de Aroca 9549).

Cabe destacar que en la sede de calle Salta 2840, como también por oficina virtual, se pueden realizar las consultas, denuncias y reclamos de manera gratuita, recibiendo asimismo el asesoramiento jurídico y la asistencia profesional para su resolución, a través de derivaciones, mediaciones, facilitaciones y conciliaciones según corresponda.