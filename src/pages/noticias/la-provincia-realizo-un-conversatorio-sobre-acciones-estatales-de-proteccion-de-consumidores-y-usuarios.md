---
category: Estado Real
date: 2021-03-21T06:00:41-03:00
thumbnail: https://assets.3dnoticias.com.ar/CONVERSATORIO.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Provincia realizó un Conversatorio sobre acciones estatales de Protección
  de Consumidores y Usuarios
title: La Provincia realizó un Conversatorio sobre acciones estatales de Protección
  de Consumidores y Usuarios
entradilla: En el marco de la agenda que está desarrollando este mes el Ministerio
  de Producción, Ciencia y Tecnología en la materia, se destacó el rol del consumidor
  como protagonista de la cadena económica.

---
En el marco de las actividades que el Ministerio de Producción, Ciencia y Tecnología lleva adelante por el Mes del Consumidor, se desarrolló el conversatorio online Conversaciones sobre consumo y derechos: “Consumir, alimentar, nutrir”.

Dando inicio a la actividad, la directora Provincial de Promoción de la Competencia y Defensa del Consumidor, María Betania Albrecht, destacó la importancia para la gestión provincial de la realización de este tipo de actividades, articulando diversos actores institucionales que influyen en los derechos de las y los consumidores y usuarios: “Estamos con esta actividad dando inicio a un ciclo de conversatorios que se realizarán durante este 2021 y que comenzamos a implementar el año pasado, cumpliendo con las funciones de promoción de derechos, prevención, información y educación a las y los consumidores y usuarios”, detalló la funcionaria.

Durante el encuentro, la representante de la Agencia Santafesina de Seguridad Alimentaria (Assal), María Celeste Nessier, expuso sobre aquellos puntos de encuentro que las líneas de actuación de la agencia tienen contacto con el consumidor, y la importancia que tiene el rol de éste como protagonista en la seguridad alimentaria.

En tanto, en representación de la Secretaría de Comercio Interior y Servicios, Marcelo Gelcich, se refirió a la seguridad alimentaria desde la perspectiva del bloque jurídico que compete a la Secretaría y su aplicación.

Finalmente, Albrecht hizo referencia a las diferentes actividades que se fueron realizando y se continuarán realizando durante el mes de marzo en lo que se denominó el “Mes de las y los Consumidores y Usuarios”, en conmemoración al Día Mundial que se celebró el 15 de marzo.