---
category: Estado Real
date: 2021-08-29T06:00:09-03:00
thumbnail: https://assets.3dnoticias.com.ar/VIALIDAD.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Vialidad provincial comenzó la entrega de árboles para la forestación de
  rutas y accesos
title: Vialidad provincial comenzó la entrega de árboles para la forestación de rutas
  y accesos
entradilla: En esta primera etapa se aprobaron 21 proyectos, con el objetivo de plantar
  más de 5000 especies nativas.

---
La dirección provincial de Vialidad, dependiente del Ministerio de Infraestructura, Servicios Públicos y Hábitat, en el marco del programa que promueve la forestación y parquización de caminos y rutas provinciales, inició la entrega de especies arbóreas para crear espacios sustentables, diseñados paisajísticamente y respetando las restricciones, para favorecer la seguridad vial.

La iniciativa acompaña al programa “Plantar para el Futuro”, elaborado por el Ministerio de Ambiente y Cambio Climático, que promueve la creación de nuevas áreas verdes y lugares de descanso en rutas provinciales.

**El programa**

Busca contribuir a la seguridad vial de los usuarios, respetando las superficies que deben permanecer libres de vegetaciones leñosas y teniendo como beneficio la creación de nuevas áreas verdes y lugares de descanso en espacios aptos.

Entre los detalles, se fundamenta que las forestaciones viales ayudan a conservar la biodiversidad y a mejorar el paisaje; protegen los suelos de procesos erosivos y de la degradación, evitando que las calzadas se conviertan en depósitos de basura o elementos contaminantes; y armonizan los procesos productivos próximos a las zonas de caminos.

**Ejecución**

Para esta etapa, está prevista la forestación en las localidades de Ingeniero Chanourdie, Villa Guillermina, Josefina, Eusebia, Colonia Aldao, Sunchales, Cayastá, Helvecia, La Penca y Caraguatá, San Javier, Gessler, Las Bandurrias, San Martín de Las Escobas, Coronel Arnold, Máximo Paz, Garabato, Tartagal y Tostado.

En tal sentido, se priorizaron aquellas especies nativas o autóctonas de cada ecorregión tales como aguaribay, cina cina, palmera pindó, algarrobo blanco, tusca y sauce criollo.

En tanto, en actividades conjuntas con el corredor vial N° 9, se efectuaron ordenamientos de la zona de caminos y posterior plantación de árboles nativos en la ruta provincial N° 6, en la localidad de Nelson, departamento La Capital. Respecto del corredor vial N°6, se encuentra en desarrollo la reforestación y puesta en valor de las zonas de descanso en la ruta provincial N°14, en el tramo Piñero- Bigand, entre los departamentos San Lorenzo y Caseros.