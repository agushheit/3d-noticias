---
category: La Ciudad
date: 2021-01-28T10:55:25Z
thumbnail: https://assets.3dnoticias.com.ar/colectivos.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: '3D Noticias '
resumen: 'Aumentó el colectivo en Santa Fe '
title: 'Aumentó el colectivo en Santa Fe '
entradilla: La tarifa plana costara $42,35

---
El municipio santafesino dio a conocer hoy el fuerte incremento que sufrirán las tarifas de colectivos en la ciudad. Desde el 8 de febrero, el cuadro tarifario será el siguiente:

* Boleto Frecuente: $ 42,35 (pesos cuarenta y dos con treinta y cinco centavos)
* Centro: $ 33,00 (pesos treinta y tres con cero centavo)
* Tarifa Escolar: $ 18,80 (pesos dieciocho con ochenta centavos)
* Tarifa Jubilado: $ 23,50 (pesos veintitres con cincuenta centavos)
* Tarifa Estudiantil (terciario - universitario): $ 28,20 (pesos veintiocho con veinte centavos)
* Tarifa Seguros: $ 9,34 (pesos nueve con treinta y cinco centavos)

Cabe recordar que la tarifa se encontraba congelada desde enero del año pasado, por una imposición del gobierno nacional que, a cambio de subsidios, impedía los aumentos.

Tras esa definición, se analizó cómo fue el efecto inflacionario de los últimos 16 meses sobre los costos del transporte y se redeterminó el precio del boleto en función de la fórmula dispuesta por ordenanza del Concejo Municipal. No se consideró la cantidad de usuarios de 2020 debido a que la pandemia fue un año atípico para el sector; decisión que hubiera impactado aún más en el ajuste tarifario.

Las localidades con similares escalas a Santa Fe ya establecieron nuevos cuadros tarifarios con incrementos que llevan los boletos a alrededor de $ 43 en Córdoba y de $ 45 en Rosario.

Aún resta saber cuáles serán los ajustes a los recorridos que propone el ejecutivo. En declaraciones a “el litoral” el subsecretario de Transporte, Lucas Crivelli explicó:  “Apuntamos a equilibrar algunas líneas de colectivos porque tenemos líneas que llevan muchas cantidad de pasajeros y otras muy poco, y necesitamos que todas funcionen con una cantidad similar”.

Luego de conocida la noticia, las redes sociales dieron cuenta del descontento por el incremento del boleto cercano al 50%.

![](https://assets.3dnoticias.com.ar/tuit_coles.jpg)

![](https://assets.3dnoticias.com.ar/tuit_coles1.jpg)

![](https://assets.3dnoticias.com.ar/tuit_coles2.jpg)