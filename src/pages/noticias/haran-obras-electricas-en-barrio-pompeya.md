---
category: La Ciudad
date: 2021-01-16T03:57:59Z
thumbnail: https://assets.3dnoticias.com.ar/16121-EPE.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: '3DNoticias - Fuente: El Litoral'
resumen: Harán obras eléctricas en barrio Pompeya
title: Harán obras eléctricas en barrio Pompeya
entradilla: La Empresa Provincial de la Energía (EPE) abrió este jueves la licitación
  pública Nº 3516 para remodelar y ampliar la red eléctrica en el barrio Pompeya.

---
En la oportunidad, presentaron ofertas las firmas Bauza Ingeniería, $ 64.130.229,48; MEM Ingeniería, $ 45.833.898,45; y Electromecánica Tacuar SRL, $ 50.482.400,32. Estas cotizaciones serán analizadas por una comisión interdisciplinaria, que informará sobre la firma adjudicataria de los trabajos.

La actividad estuvo encabezada por la ministra de Infraestructura, Servicios Públicos y Habitat, Silvina Frana; el presidente de la EPE, Mauricio Caussi; el senador provincial Marcos Castelló; y representantes de las empresas cotizantes.

Caussi informó que, en esta primera etapa, «los trabajos en el barrio Pompeya comprenden la zona delimitada por Avenida Peñaloza, Gorriti, Zavalla y Vieytes, interviniendo en 18 manzanas del barrio con nueva red de baja tensión con cable preensamblado, construcción de 660 metros de línea aérea de media tensión, tendido de 1.040 metros de cable subterráneo sobre calle 1° de Mayo, entre Gorriti y Ayacuchco y el montaje de 5 nuevas subestaciones transformadoras».

Y explicó que «estas obras se dan en el contexto del programa EPE Social, que nos permite impulsar la infraestructura eléctrica en los barrios y al mismo tiempo promover la inclusión social, incorporando al vecino al sistema comercial de la empresa».

En la oportunidad, Frana dijo que «esta es una semana muy promisoria en términos de realizaciones, porque estamos abriendo un número muy importante de licitaciones para mejorar la prestación del servicio eléctrico. Este gobierno tiene la vocación de reivindicación de los derechos de los sectores más postergados y lo estamos haciendo con este tipo de obras que hoy lanzamos».

Por su parte, Castelló agradeció la decisión del Gobierno y resaltó que «esta es una gestión que no se hace solo en la oficina, sino que uno la ve cotidianamente en el territorio, a través del impulso que le dan los funcionarios con realizaciones concretas, que le mejoran la calidad de vida a la gente».

Por último, Caussi resaltó el esfuerzo de la Provincia para abrir en el primer mes del año, licitaciones públicas para sumar obras, equipamiento, materiales y servicios por $ 1.800 millones, con el fin de reforzar y ampliar un servicio esencial, como lo es el abastecimiento de energía eléctrica.