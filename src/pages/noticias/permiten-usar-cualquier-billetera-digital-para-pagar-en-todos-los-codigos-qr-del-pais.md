---
category: Agenda Ciudadana
date: 2021-11-29T06:00:32-03:00
thumbnail: https://assets.3dnoticias.com.ar/PAGOBILLETERA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: Permiten usar cualquier billetera digital para pagar en todos los códigos
  QR del país
title: Permiten usar cualquier billetera digital para pagar en todos los códigos QR
  del país
entradilla: "El objetivo del nuevo sistema es integrar todos los medios de pago electrónicos
  u reemplazar por las Transferencias 3.0 las operaciones en efectivo. \n\n"

---
El sistema de Transferencias 3.0, el mecanismo de pagos digitales con códigos QR, comenzará a funcionar este lunes plenamente en la Argentina y permitirá usar cualquier billetera virtual, ya sea de un banco o una fintech, para abonar en todos los comercios que acepten este medio de cobro, sin importar la empresa que provea el código.  
  
El sistema habilitará a los comercios a recibir el dinero de forma instantánea, irrevocable y con la comisión más baja del mercado, lo que ayudará a simplificar las operaciones y a competir contra el efectivo que, pese al avance de los mecanismos de pago virtual, sigue siendo la forma de pago usada en cerca de 8 de cada 10 transacciones.  
  
Será el punto final de un sistema en el que, desde hace casi dos años, trabajan en forma conjunta el Banco Central de la República Argentina (BCRA), los bancos públicos y privados, las fintech, las empresas de tarjetas de crédito y débito y las cámaras compensadoras y administradoras de pagos (Coelsa, Fiserv, Red Link y Prisma).  
  
"Lo que buscamos con Transferencias 3.0 es integrar a todo el sistema de pagos electrónicos; va en el mismo sentido que hemos hecho con el cheque electrónico y con la factura de crédito electrónica", aseguró el presidente del BCRA, Miguel Pesce, en una reunión con las cámaras de comercio días atrás.  
  
"La intención es reemplazar al efectivo por las Transferencias 3.0 como el medio predominante de pago en nuestra economía y que este sistema sea eficaz y eficiente, tanto para la persona que va a pagar como para la persona que va a cobrar", agregó.  
  
A partir de Transferencias 3.0, el plazo de acreditación máximo para los comercios será de 25 segundos, con una comisión que estará en del rango del 0,6% al 0,8%, dependiendo del tipo de aceptador y su política de competencia de precio.  
  
"Esta interoperabilidad es casi única en el mundo. Queremos que para el comercio sea tan fácil aceptar dinero en efectivo como un sistema de pago con billetera digital. Por eso propiciamos el pago inmediato a los comerciantes. El pago inmediato nos pondrá mucha presión a todos los actores para hacer mejores servicios en el mercado", señaló esta semana Maia Eliscovich, head de Ualá Bis, en una conferencia organizada por la Cámara Argentina de Fintech.

> "Lo que buscamos con Transferencias 3.0 es integrar a todo el sistema de pagos electrónicos; va en el mismo sentido que hemos hecho con el cheque electrónico y con la factura de crédito electrónica".MIGUEL PESCE

  
  
El objetivo de todos los actores es el mismo: facilitar y simplificar los pagos digitales, agrandar el mercado de uso de dinero electrónico con más competencia y, al mismo tiempo, reducir el uso de efectivo de modo de expandir la inclusión financiera y formalizar a un gran sector de la economía.

![El pago mediante cdigo QR es uno de los que ms creci durante la pandemia](https://www.telam.com.ar/thumbs/bluesteel/advf/imagenes/2020/12/5fccd153d055e_900.jpg =100%x100%)

  
Para alcanzar la interoperabilidad de los códigos QR fue necesaria cooperación de tanto la infraestructura de pagos como de la normativa, de los desarrollos de producto de cada una de las empresas interesadas en leer los QR y de las decisiones comerciales de cada fintech o banco que participa.  
  
Por eso, las primeras pruebas del sistema comenzaron en diciembre del año pasado, con la apertura de códigos por parte de empresas clave del sistema en grandes comercios y cadenas con presencia en todos el país, con la idea de asegurar un correcto funcionamiento y evitar una mala experiencia de usuario.  
  
Mercado Pago, una de las principales empresas del sector con más de 1,5 millones de comercios adheridos a pagos con QR en la Argentina, avanzó desde entonces en la incorporación de más de 5.200 locales de 18 cadenas como Burger King, Farmacity y La Anónima, entre otras, en las que ahora se puede pagar con las aplicaciones BNA+ y Cuenta DNI, de los bancos Nación y Provincia; Yacaré, Reba y ANK; y MODO, la aplicación que desarrollaron más de 30 bancos para agilizar los pagos y transferencias.  
  
En junio de 2021 se concretaron más de 8,5 operaciones por adulto con medios electrónicos de pago -tarjetas de débito, crédito o transferencias electrónicas-, alcanzando una marca mensual máxima desde que se posee registro de la serie, mientras que el total de operaciones realizadas en el primer semestre del año superó en 40% al promedio del mismo período de 2020, según datos del Informe de Inclusión Financiera del BCRA.  
  
Si bien existe una tendencia creciente en el uso de todos los medios de pago electrónicos desde hace unos años -excepto en las tarjetas de crédito-, el mayor dinamismo se observó entre las transferencias electrónicas cuya participación en el total de operaciones pasó de 4% en enero de 2016 a 23% en junio de 2021.  
  
El efecto de la pandemia en los hábitos de consumo, la proliferación en el uso de billeteras electrónicas y los cambios regulatorios para facilitar la interoperabilidad de los medios de pago son algunos de los factores que explican este cambio de comportamiento.  
  
De hecho, uno de los medios de pago que más creció en este tiempo fueron los pagos mediante código QR que tuvieron tasas de variación de 136% en cantidades y de 140% en montos reales en comparación los valores promedio de 2019 y 2020.  
  
"El uso masivo del QR dependerá de la educación que se lleve a cabo sobre los usuarios. Hoy 8 de cada 10 transacciones siguen siendo en efectivo y 6 de cada 10 personas no usan cuenta bancaria por eso el desafío es la inclusión financiera y superar la barrera del efectivo", afirmó Agustín Viola, director de Mercado Pago.