---
category: Estado Real
date: 2022-01-21T06:00:37-03:00
thumbnail: https://assets.3dnoticias.com.ar/agrotecnicas.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La provincia recibirá más de 29 millones de pesos para acondicionar escuelas
  agro técnicas
title: La provincia recibirá más de 29 millones de pesos para acondicionar escuelas
  agro técnicas
entradilla: Es una asignación específica con carácter extraordinario proveniente del
  Instituto Nacional de Educación Tecnológica (INET) y serán beneficiadas nueve escuelas
  de la provincia.

---
El Ministerio de Educación recibirá un aporte especial para acondicionar escuelas agrotécnicas con residencias o albergues. El total de los fondos es de 643 millones de pesos y serán distribuidos en establecimientos con esta modalidad de todo el país beneficiando a más de 11.600 estudiantes.

El propósito de esta iniciativa es contribuir a restablecer las condiciones de habitabilidad para el desarrollo integral de las y los jóvenes estudiantes albergados en las distintas provincias del país. También contempla la revalorización y la optimización del espacio en el retorno a clases presenciales y asegura la disponibilidad de insumos indispensables para garantizar la cobertura de las necesidades de salud, higiene de las y los estudiantes.

En este sentido, el secretario de Educación, Víctor Debloc, dijo: “Atender y acompañar las residencias estudiantiles de las escuelas agrotécnicas es un acto de justicia educativa. Tener instituciones con estudiantes y docentes albergados nos convoca a dotarlos de seguridad, higiene, sanitización continua y equipamientos apropiados”.

“Estos aportes nacionales son sumamente importantes porque vienen a reforzar y materializar una formación profesional de buena calidad en orden al perfil contemporáneo de la formación agrotécnica”, sentenció el funcionario.

A partir de esta asignación se podrá adquirir equipamiento destinado a completar los dormitorios, cocinas, comedores y baños; favorecer la práctica de juegos y deportes, talleres productivos, las actividades de expresión y recreativas. Asimismo, se repartirán elementos de consumo cotidiano como productos de aseo personal para las y los estudiantes albergados. También se podrán realizar pequeñas reparaciones edilicias y compra de repuestos y utensilios de uso diario para promover mejoras en la organización de los tiempos y espacios vinculados con la vida en el albergue.

En esta línea, el director provincial de Educación Técnica, Fernando Hadad, explicó: “Trabajamos con el INET en un relevamiento de los albergues de las escuelas agrotécnicas. En el mismo se tuvo en cuenta la cantidad de camas disponibles y en función de ello se determinó la asignación de los fondos”.  
  
Por último, Hadad dijo: “Con este beneficio desde el Ministerio se podrán trabajar las condiciones de habitabilidad para el desarrollo integral de las y los jóvenes estudiantes. También, fortalecer y recuperar el vínculo pedagógico de estudiantes con vulnerabilidad socioeducativa pertenecientes a zonas con baja densidad poblacional o de difícil acceso, cuyas trayectorias escolares han sido interrumpidas a causa de la pandemia.”

Escuelas beneficiadas  
Los establecimientos educativos que recibirán los aportes según las regiones educativas son: En la Región I de Educación la Escuela N° 300 "Colonia General Belgrano" de Villa Minetti, de la Región II, las escuelas Nº 295 "General Manuel Obligado" de la localidad de La Potasa y la N° 487 "Carlos Alfredo Spontón" de Malabrigo.

En la Región IV, los fondos serán destinados a las escuelas Nº 336 "Mario César Videla" de la localidad de San Justo, Nº 351 "Carmen Allio Martínez" de Pedro Gómez Cello y la Nº 377 "Mercedes Mascías" de Colonia Mascías.

En tanto en la Región VIII de Educación las escuelas N° 301 “Mariano Moreno” de Landeta y la Nº 380 de María Juana. Por último, la escuela Nº 308 "Malvinas Argentinas" de la localidad de Ceres perteneciente a la Región IX.