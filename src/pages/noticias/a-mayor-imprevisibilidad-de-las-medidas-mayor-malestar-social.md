---
category: Agenda Ciudadana
date: 2021-05-14T08:15:51-03:00
thumbnail: https://assets.3dnoticias.com.ar/leandro-gonzalez.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Leandro Gonzalez
resumen: "“A mayor imprevisibilidad de las medidas, mayor malestar social”"
title: "“A mayor imprevisibilidad de las medidas, mayor malestar social”"
entradilla: Para Leandro González, presidente del Concejo Municipal, las medidas restrictivas
  del gobierno provincial le quitan previsión a la vida de las personas y agregan
  mayor incertidumbre a la cotidianeidad de las familias.

---
A más de 400 días de iniciado el aislamiento y el distanciamiento social obligatorio en el marco de la pandemia por Covid-19, el presidente del Concejo Municipal, Leandro González, expresó que tanto el gobierno nacional como el gobierno provincial deberían adoptar medidas “con mayores márgenes de previsibilidad para la vida de las personas”.

“Las medidas que adopten tanto el gobierno nacional como el gobierno provincial deben atender la crisis sanitaria pero también deben apuntar a devolverle previsibilidad a las actividades de las personas. Pasó más de un año de pandemia y seguimos viendo decisiones sin previsibilidad. El malestar social que percibimos esta semana con algunas restricciones, es consecuencia directa de la imprevisibilidad que generan las medidas del gobierno en la vida diaria de las personas”, manifestó el presidente del Concejo Municipal.

“Si bien es cierto que el actual es un momento muy complejo de toma de decisiones para todos los niveles del Estado entendemos que con criterios claros, comunicados en tiempo y forma, y con sensibilidad y contención de los sectores afectados, seguramente las reacciones de la sociedad no serán las mismas”.

“Nos enteramos el sábado de una serie de medidas que, en horas y días posteriores tienen cambios, muchas veces contradictorios entre sí. Estas idas y vueltas generan un desconcierto generalizado que trae aparejado además un desacato a las medidas y un descreimiento, que es lo peor que nos puede ocurrir como Estado”, enfatizó.

**“Falta de diálogo previo”**

Además de remarcar la necesidad de generar previsibilidad tanto para la sociedad como para aquellas actividades económicas y productivas que sean afectadas por algunas de las medidas que se tomen, González señaló que es fundamental mantener instancias de diálogo entre el Estado y los diferentes actores sociales involucrados.

“El diálogo previo es de vital importancia. No es lo mismo tomar decisiones al comienzo de la pandemia que tomarlas hoy, con la experiencia acumulada. Fue de público conocimiento por declaraciones de representantes de la Cámara de Gimnasios, por ejemplo, que se enteraron del cierre a través de los medios de comunicación, sin capacidad de previsión siquiera para poder avisarle a sus clientes cómo actuar ese mismo día. Incluso estuvimos reunidos horas previas con ellos, y ni nosotros ni ellos teníamos esa información”.

“Esto desde la política no puede pasar. Si cuando se brindó asistencia económica a diferentes actividades se las convocó al diálogo y se las informó previamente, a la hora de informar una restricción también es necesario poder informar con antelación”, afirmó el concejal del Frente Progresista.

**Plan de administración**

En este sentido, Leandro González junto al concejal Lucas Simoniello presentaron un proyecto en el Concejo Municipal para que los estados superiores (nacional y provincial) generen un plan de administración teniendo en cuenta una serie de factores como: la definición de variables e indicadores que determinen el riesgo epidemiológico en función de la capacidad de operativa y de respuesta del sistema de salud; la consecuente adopción de medidas sanitarias de cuidado pero buscando la minimización del impacto que generen las posibles restricciones y atendiendo las particularidades; la readecuación de los protocolos de bioseguridad y distanciamiento a partir del diálogo y el involucramiento de los sectores económicos, sociales y productivos; entre otras acciones articuladas.

Entendemos que con más de un año de experiencia la forma de proceder no puede ser la misma y la improvisación debe ser vencida por la organización y la seriedad”, afirmó el presidente del Concejo.