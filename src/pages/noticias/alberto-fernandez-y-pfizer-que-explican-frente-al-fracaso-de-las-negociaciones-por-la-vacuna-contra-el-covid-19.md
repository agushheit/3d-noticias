---
category: Agenda Ciudadana
date: 2020-12-21T13:21:52Z
thumbnail: https://assets.3dnoticias.com.ar/2112pfizer.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Infobae'
resumen: 'Alberto Fernández y Pfizer: qué explican frente al fracaso de las negociaciones
  por la vacuna contra el COVID-19'
title: 'Alberto Fernández y Pfizer: qué explican frente al fracaso de las negociaciones
  por la vacuna contra el COVID-19'
entradilla: El Presidente sostiene que la empresa exige un contrato que desampara
  al Estado ante un eventual juicio penal. Pfizer argumenta que las diferencias están
  vinculadas con el precio del transporte de las dosis.

---
El 10 de julio, Alberto Fernández recibió en Olivos a Nicolás Vaquer, gerente general de Pfizer, y al director científico de la Fundación INFANT, Fernando Polack. Fue un cónclave auspicioso: el Presidente avanzaba en la negociación de millones de dosis contra el COVID-19, y la compañía de los Estados Unidos asociada al laboratorio alemán BioNTech concretaba la posibilidad de ejecutar la Fase 3 de la vacuna con 4.500 voluntarios argentinos.

El Presidente estaba entusiasmado con Pfizer y desplegó su poder político para evitar que la burocracia y ciertos obstáculos legales complicara la posibilidad de tener 3 millones de dosis de la vacuna antes que concluyera diciembre de 2020.

En este sentido, Alberto Fernández envió al canciller Felipe Solá a la embajada de Estados Unidos para facilitar la eventual importación de las vacunas y logró que el Congreso aprobara una ley a la medida del consorcio farmacéutico integrado por empresas de Estados Unidos y Alemania.

Pfizer entendió el gesto presidencial y anunció que las primeras vacunas en la Argentina se aplicarían al mismo tiempo que en los Estados Unidos. Fue una actitud muy valorada en la Casa Rosada que ya había asumido que solo un voluminoso stock de vacunas podía atenuar los estragos mortales de una segunda ola de pandemia.

***
[![Mira el video](https://assets.3dnoticias.com.ar/2112-video2.webp)](https://assets.3dnoticias.com.ar/2112-video.mp4 "Reproducir")
***

El 14 de diciembre pasado, una enfermera que trabaja en la unidad de Cuidados Intensivos del Hospital Lenox Hill de Manhattan, recibió la primera vacuna de Pfizer en Estados Unidos. «No se sintió diferente a cualquier otra vacuna. Estoy esperanzada y aliviada», dijo Sandra Lindsay a los medios de New York.

Y hoy en Chile, tras una intensa negociación liderada por Sebastián Piñera, llegarán al aeropuerto de Santiago las primeras dosis de Pfizer que serán aplicadas en todo el país transandino. «Vamos a empezar con las personas que han estado en la primera línea, en las unidades críticas, atendiendo a los pacientes críticos», adelantó Piñera en conferencia de prensa.

En Argentina no hay unan sola dosis de Pfizer. Y la explicación sobre el fracaso de las negociaciones depende del interlocutor. Alberto Fernández tiene una argumentación jurídica basada en la responsabilidad penal del Estado, mientras que Pfizer apela a simples razones comerciales para justificar la ausencia de un acuerdo que dejó sin vacunas a la Argentina cuando una segunda ola de COVID-19 ya azota a las principales capitales de Europa.

***

![](https://assets.3dnoticias.com.ar/2112albertoygines.webp)

***

«**Pfizer nos pidió una Ley de Vacunas, y nosotros cumplimos. Y ahora nos piden otra ley para evitar responsabilidades penales, si las vacunas causan daños físicos. Esa inmunidad jurídica no se la vamos a dar**. Ellos son responsables de las vacunas. No es el Estado Nacional. El Estado compra y ellos venden. No entiendo por qué tenemos que darle una norma que los pone al margen de las responsabilidades civiles y penales», explicó Alberto Fernández en Olivos.

El consorcio farmacéutico que asocia a compañías de Estados Unidos y de Alemania tiene otra perspectiva de las diferencias que mantiene con Balcarce 50. Las dosis se transportan en 70 grados bajo cero y Pfizer no acepta la mínima excepción al momento de evaluar las condiciones de carga, descarga y almacenamiento de sus propias vacunas.

Pfizer utiliza DHL para desplazar sus vacunas y confía en su experiencia como transportador global. DHL llevó las dosis a Israel y hará lo mismo con las vacunas que aterrizarán hoy en el Aeropuerto de Santiago de Chile. Pfizer apuesta por DHL y es muy cauta al momento de reemplazar a este proveedor con exitosa trayectoria.

En este contexto, **Pfizer explica que no hubo acuerdo porque la Argentina no quiere pagar los costos del transporte que ejecutaría DHL**. La vacuna tiene dos dosis y su valor de mercado es cercano a los 20 dólares. A ese precio debía sumarse el costo de DHL como transportador autorizado del consorcio farmacéutico norteamericano/alemán, explicaron voceros de Pfizer en el exterior.

***

![](https://assets.3dnoticias.com.ar/2112dhl.webp)

***

**Es decir: el Presidente alega razones jurídicas y Pfizer circunstancias económicas.**

«Tengo confianza y la esperanza de poder cerrar la negociación, creo que esta semana lo vamos a tratar de resolver», opinó anoche el ministro Ginés González.

**Si el Gobierno no convalida a DHL u otro con estándares internacionales, Pfizer otra vez dirá que no**. Y continuará vendiendo sus vacunas en la región. Uruguay sigue en la lista.