---
layout: Noticia con imagen
author: Nota de la Redacción de 3DNoticias
resumen: Cortes en Ruta 168
category: La Ciudad
title: "Colastiné Sur: por falta de agua, vecinos cortaron la Ruta 168"
entradilla: Si bien eran abastecidos por dos camiones cisterna municipales y de
  Aguas Provinciales, uno se rompió hace una semana y con un tanque no alcanza
  para las 4000 personas que viven en el lugar.
date: 2020-11-24T12:06:04.224Z
thumbnail: https://assets.3dnoticias.com.ar/colastine-sin-agua.jpeg
---
**Desde hace una semana vecinos de Colastiné Sur no tienen agua potable** y el problema parece no solucionarse. Si bien hay camiones cisterna que van a abastecer a los vecinos del lugar eso no alcanza y las necesidades básicas para higiene y alimentación no pueden cumplirse.

Por esta razón, un grupo de vecinos decidió reunirse en la tarde del lunes y tomaron la decisión de cortar la Ruta 168 hasta que llegue un nuevo camión cisterna para abastecerse de agua ya que el último fue y se retiró a la hora de la siesta.

“Hace una semana que no tenemos agua y la situación se volvió insostenible. Hay familias enteras y también tenemos el club de pesca que no puede llevar adelante la actividad”, relató una de las vecinas.

Además, destacó que este lunes feriado el camión cisterna llevó algo de agua, pero a las tres de la tarde se fue y no todos pudieron abastecerse.

Otro de los vecinos dijo que "el agua, en muchos casos no es transparente y no sirve para el consumo humano".

Claudia es vecina del lugar y habló con LT10. Explicó que “la situación se viene dando desde hace varios meses”. Además, señaló que “el camión que trae el agua está roto y están demorando mucho en arreglarlo” y no lleva agua para abastecerlos.

"Dependemos de dos camiones: uno de Agua Provinciales y otro de municipalidad. El camión de Aguas Provinciales sólo deja agua en la calle principal y es el municipal la deja agua en todas las cortadas", afirmó.

Según se pudo saber, eran dos los camiones municipales que llevan agua los lunes, martes y miércoles, pero uno está roto y con un solo tanque no alcanza.

Hoy, al entrar el camión en una jornada de mucho calor, llegó solo a suministrar a la mitad de la localidad y muchos vecinos no llegaron a adquirir agua.

### **Agua en camiones**

De un tiempo a esta parte todas las semanas un camión lleva agua a los habitantes del lugar. Pertenece a la municipalidad y abastece a 4000 personas.

El camión aguatero se carga en La Guardia y luego llega al barrio donde recorre las calles mientras los vecinos se acercan para llenar los bidones y baldes. Una postal que se repite todas las semanas.

### **Una posible solución**

El intendente Emilio Jatón anunció el pasado lunes una obra que se realizará para abastecer de agua a uno de los lugares más histórico de Santa Fe que, además, como una gran paradoja está rodeado de agua.

Según afirman desde la Municipalidad de Santa Fe, se trabaja con distintas alternativas, pero lo claro es que se realizarán perforaciones para crear una red para los vecinos.