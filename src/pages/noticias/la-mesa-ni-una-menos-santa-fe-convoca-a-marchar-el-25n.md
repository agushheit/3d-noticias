---
category: La Ciudad
date: 2021-11-25T06:15:19-03:00
thumbnail: https://assets.3dnoticias.com.ar/Marcha25N.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'La Mesa Ni Una Menos Santa Fe convoca a marchar el #25N'
title: 'La Mesa Ni Una Menos Santa Fe convoca a marchar el #25N'
entradilla: "Por su parte, el gobierno provincial de Santa Fe y la Asociación Generar
  también anunciaron actividades.\n\n"

---
"Este  25 de Noviembre las mujeres y disidencias   volvemos a las calles juntes y organizadas para hacer oír nuestro grito “La tierra y nuestros cuerpos no se negocian, ni se venden”", aseguran en un comunicado desde la Mesa Ni Una Menos Santa Fe".

 "Convocamos  a concentrarnos en la Plaza del Soldado a las 17 hs para marcha juntes hasta la Casa de Gobierno  donde se realizará el acto que conmemora el 25 de noviembre, Día Internacional de Lucha por la Erradicación de las Violencias contra las Mujeres, recordando a las hermanas Mirabal: Patria, Minerva y María Teresa, símbolo mundial contra la violencia de género, nacidas en la República Dominicana y cuyo múltiple asesinato fue perpetrado por la policía secreta de ese país y ordenado por el dictador Trujillo", invitan a la comunidad.

 "Estaremos una vez mas en las calles, porque la violencia no se detiene. Seamos la voz de las que ya no están. Habrá colectivos gratuitos", aclaran.

 **Muestra de emprendimientos**

Por otra parte, desde las 11, las emprendedoras  que concurren al Centro de Día de la Asociación Generar exhibirán y pondrán en venta sus productos. Asimismo se hará entrega de nuevos microcréditos del Banco Solidario Santa Fe.  

 **CONMEMORACIÓN OFICIAL**

Por su parte, el gobierno santafesino realizará una presentación y un taller de sensibilización abierta de Ley Micaela en la Ciudadanía. La actividad se llevará a cabo el 25 de noviembre a las 11, en Patio de los paraboloides en El Molino, Fábrica Cultural de la ciudad Santa Fe, con participación de la ministra de Igualdad, Género y Diversidad, Celia Arena.

 **Atención para mujeres en situación de violencia**

Si vos o alguien que conocés vive alguna situación de violencia, llamá gratis al 144 o buscá algún centro de atención cercano