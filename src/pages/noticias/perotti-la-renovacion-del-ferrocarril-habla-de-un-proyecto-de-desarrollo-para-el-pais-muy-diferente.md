---
category: Estado Real
date: 2021-05-16T06:00:04-03:00
thumbnail: https://assets.3dnoticias.com.ar/FERROCARRIL.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Perotti: “La renovación del ferrocarril habla de un proyecto de desarrollo
  para el país muy diferente”'
title: 'Perotti: “La renovación del ferrocarril habla de un proyecto de desarrollo
  para el país muy diferente”'
entradilla: 'Omar Perotti junto a Santiago Cafiero, y los ministros de Transporte,
  Alexis Guerrera, y de Defensa, Agustín Rossi, inauguraron en San Cristóbal, el nuevo
  Taller Ferroviario de Pares Montados. '

---
El gobernador de la provincia, Omar Perotti, acompañado por el Jefe de Gabinete de la Nación, Santiago Cafiero, y los ministros de Transporte, Alexis Guerrera, y de Defensa, Agustín Rossi, presidió el acto de inauguración del nuevo Taller Ferroviario de Pares Montados en la ciudad de San Cristóbal. Se trata de una nueva acción gubernamental dirigida a potenciar el crecimiento y la generación de empleo en la provincia de Santa Fe. Con una inversión de 2.445.000 de dólares, la flamante unidad de trabajo cuenta con un torno paralelo para el refilado de ruedas y otro vertical para el calado de rodamientos y cubos.

El establecimiento, cuenta ahora con una prensa para la colocación de ruedas en los ejes y un puente grúa para el manejo de los pares montados. Para su funcionamiento, se tuvo presente la incorporación y capacitación de 16 nuevos empleados y empleadas. También, está prevista la ampliación de obras bajo piso en el taller, situación que permitirá la reparación de vagones sin necesidad de desmontarlos.

El acto comenzó pasado el mediodía en las instalaciones del “Taller San Cristóbal”, ubicado en la ciudad cabecera del departamento homónimo y emblema del ferrocarril por donde circula a través de las vías el 60% de la carga proveniente del Noroeste argentino hacia los puertos del Gran Rosario.

En lo que consideró como “un día histórico” para el centro norte santafesino, el gobernador Perotti dijo que “cuando vemos movimiento de trenes, es porque hay que transportar carga generada por la producción. Esto genera la necesidad de asegurar el mantenimiento del ferrocarril. Y aquí es donde cobran importancia los talleres, como este”. Y agregó: “Hoy, los habitantes de San Cristóbal vuelven a ver el ferrocarril con un nuevo proyecto y desafío, muy importantes para potenciar la Hidrovía, la mayor obra de impacto federal”.

Además, Perotti señaló: “Volver al ferrocarril es volver a pensar en mejorar la conectividad. El Belgrano Cargas llega hasta Bolivia y allí se abre una posibilidad para transportar la producción de nuestros molinos harineros, con costos más bajos. El tren, desde sus inicios, pudo conectar los pueblos y ciudades porque era la única vía de comunicación entre las comunidades. También llegaban los trenes sanitarios, pero tras su desaparición dejaron a esas estaciones aisladas, incomunicadas y sin cobertura de salud. La vuelta del ferrocarril, sin duda, entusiasma y motiva”. En la oportunidad, el mandatario santafesino aprovechó para anunciar la obra complementaria para la pavimentación de la ruta provincial 39.

Más adelante, el mandatario santafesino sostuvo que estas inversiones “hablan claramente de una mirada diferente, de un proyecto de desarrollo de país que nos contenga a todos. Lo que recibe Santa Fe, lo potencia y lo devuelve multiplicado. Muchas de las preocupaciones que tienen hoy los grandes centros urbanos van a ser menores cuando el desarrollo del interior argentino, a través del ferrocarril, pueda mostrar todo su potencial”.

**Vocación de trabajo**

“Aún en pademia, podemos decir que Santa Fe no ha detenido su vocación de trabajo y producción”, agregó Perotti. En este punto, destacó “la ayuda del Gobierno Nacional para enfrentar la crisis sanitaria. Sin duda, el aporte de Nación fue decisivo para incrementar en más del 120% las camas críticas y el número de respiradores. De igual manera, pidió a todos los santafesinos y santafesinas que “se sigan cuidando y actuando con responsabilidad social. Porque menos circulación, significa menos contagios”, recordó.

A su turno, el ministro de Defensa de la Nación, Agustín Rossi, expresó que “históricamente, las localidades del interior expulsaban a sus habitantes por la falta de empleo y oportunidades. Por eso estas inversiones tienen un valor y un significado muy importante, principalmente a la hora de dar respuestas en este contexto económico que nos toca atravesar”.

“Esta pandemia –continuó-, nos exigió dar lo mejor de nosotros. Invertimos una enorme cantidad de esfuerzos para el sistema de salud, pero también tuvimos que empezar a hacernos cargo de la deuda que no generamos nosotros. En esta provincia, tenemos la suerte de que hay un gobernador que tiene absolutamente claro cómo es el entramado productivo y que sabe cuáles son las obras necesarias para el desarrollo santafesino y del país”.

Por su parte, el ministro de Transporte, Alexis Guerrera, manifestó que “esta inversión es un claro ejemplo cuando el Estado está presente y es eficiente. Desde 2029, Trenes Argentinos Cargas aumentó su capacidad de transporte un 56% gracias a la modernización de las vías y los talleres. Esta es una decisión firme de nuestro Presidente de no retroceder más”.

A su turno, el Jefe de Gabinete, Santiago Cafiero, agradeció al gobernador la invitación a la inauguración del flamante taller de San Cristóbal y subrayó: “Ponerlo en funcionamiento es muy gratificante. Es cumplir con este tipo de meta concretas. Porque nuestro compromiso histórico es mejorar la calidad de vida a todos los argentinos”. E insistió: “El eje, el ordenador, es el trabajador. Es el que produce y es con él cuando se resuelve esa deuda pendiente que tiene la política para disminuir la pobreza”.

**Presentes**

Del acto participaron también los ministros provinciales de Gestión Pública, Marcos Corach, y de Producción, Daniel Costamagna; el secretario de Transporte, Osvaldo Miatello; el intendente de San Cristóbal, Horacio Rigo; el senador provincial, Felipe Michlig, el secretario general de la Unión Ferroviaria, Sergio Sasia; el presidente de Trenes Argentinos Cargas, Daniel Vispo; la Secretaria de Coordinación de Políticas Públicas, Luisina Giovannini; la diputada nacional Vanesa Massetani, y el diputado provincial Oscar Martínez entre otras autoridades.