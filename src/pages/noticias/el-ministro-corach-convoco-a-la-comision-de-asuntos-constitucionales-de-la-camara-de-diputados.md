---
category: Estado Real
date: 2021-10-21T06:00:12-03:00
thumbnail: https://assets.3dnoticias.com.ar/asuntos.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: El Ministro Corach convocó a la comisión de asuntos constitucionales de la
  cámara de diputados
title: El Ministro Corach convocó a la comisión de asuntos constitucionales de la
  cámara de diputados
entradilla: Fue en el marco del tratamiento en la Legislatura del proyecto de ley
  del Poder Ejecutivo de Inclusión Digital y Transformación Educativa "Santa Fe +
  Conectada”.

---

Durante el encuentro dicha comisión acercó al Ministro los resultados surgidos en las audiencias públicas que se fueron sucediendo en la Cámara en el marco del proyecto de ley Inclusión Digital y Transformación Educativa "Santa Fe + Conectada”, por el cual se analizó el pedido de endeudamiento solicitado por el Poder Ejecutivo.

Por su parte, el Ejecutivo se compromete a integrarlas en el decreto reglamentario de la Ley una vez aprobada.

**SOBRE EL PROYECTO**

Cabe señalar que este proyecto apuesta a implementar un programa estratégico de conectividad, que tiene como ejes el acceso universal a las Tecnologías de la Información y las Comunicaciones (TICs), garantizando la inclusión digital de la población bajo estándares de calidad, la optimización del uso del espectro radioeléctrico y la capacitación e investigación en tecnologías de las comunicaciones.

Sobre los aspectos financieros del Programa, cabe señalar que el monto total para su implementación alcanza los U$S 124.670.000 de los cuáles el préstamo del Banco de Desarrollo de América Latina -CAF - (U$S 100 millones), constituye un 80%, mientras que el monto restante (U$S 24.670.000) se prevé financiar con recursos propios del Estado provincial.

El endeudamiento supone como condiciones financieras la Tasa de Interés LIBOR + Margen (1.97%) y un plazo de amortización de 15 años con cinco años de gracia para el capital. En tanto que el plazo de ejecución estimado del programa es de cuatro (4) años.