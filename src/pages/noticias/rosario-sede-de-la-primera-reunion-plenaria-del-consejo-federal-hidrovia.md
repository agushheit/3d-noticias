---
category: Agenda Ciudadana
date: 2021-02-22T06:30:11-03:00
thumbnail: https://assets.3dnoticias.com.ar/hidrovia-parana-paraguay.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Rosario, sede de la primera reunión plenaria del Consejo Federal Hidrovía
title: Rosario, sede de la primera reunión plenaria del Consejo Federal Hidrovía
entradilla: En Rosario se analizarán detalles acerca del llamado a licitación internacional
  tras 25 años en manos de un consorcio integrado por la empresa belga Jan de Nul
  y la argentina Emepa S.A.

---
El Consejo Federal Hidrovía celebra este lunes en la ciudad de Rosario su primera reunión plenaria, ocasión en la que brindará un informe de gestión sobre el estado de la concesión de la Hidrovía Paraguay-Paraná.

En el encuentro, convocado por el ministro de Transporte, Mario Meoni, del que participan funcionaros de organismos nacionales, provinciales y productivos, se analizarán detalles acerca del llamado a licitación internacional de la vía fluvial, cuya concesión concluirá luego de 25 años en manos de un consorcio integrado por la empresa belga Jan de Nul y la argentina Emepa S.A.

La Hidrovía es una vía navegable que en su tramo argentino va desde el kilómetro 1.238 del río Paraná y el kilómetro 239 del canal de Punta Indio, en el Río de la Plata, y lo que se concesiona son obras para la modernización, ampliación, operación y mantenimiento del sistema de señalización y tareas de dragado y redragado y mantenimiento de la vía navegable troncal.

En la Argentina, la vía de navegación es gestionada por el Gobierno nacional junto a las provincias de Buenos Aires, Chaco, Corrientes, Entre Ríos, Formosa, Misiones y Santa Fe, que firmaron el Acuerdo Federal Hidrovía el 29 de agosto pasado.

El ministro Meoni señaló -en declaraciones formuladas a Télam a principios de mes- que en la reunión de Rosario se coordinará "la planificación estratégica de un transporte fluvial integrado y federal".

Del encuentro, que comenzará a las 10 y que se realizará de manera presencial y remota, participarán los miembros del Consejo, integrado por representantes de los Ministerios del Interior, Desarrollo Productivo y Transporte, y de las provincias de Buenos Aires, Corrientes, Chaco, Entre Ríos, Formosa, Misiones y Santa Fe, además de una serie de invitados de siete universidades nacionales, cinco ministerios, cámaras de productores, fuerzas de seguridad y asociaciones civiles.

Al firmar el Acuerdo Federal de la Hidrovía, en agosto pasado, el presidente Alberto Fernández dijo que se trata de "un canal de desarrollo para muchas provincias y para muchos productores norteños de la patria, ese norte que sistemáticamente fue olvidado".

Para el Presidente, "ha llegado la hora de que la Argentina crezca con otro equilibrio, que el desarrollo no solo esté concentrado en el puerto de Buenos Aires, sino que se distribuya en cada lugar del país".

La Hidrovía Paraguay-Paraná está regulada por medio de un acuerdo firmado por Argentina, Bolivia, Brasil, Paraguay y Uruguay en junio de 1992.

"La Hidrovía Paraguay-Paraná fue concesionada en 1995. Desde entonces, pasaron 25 años y varios Gobiernos y nos toca a nosotros confeccionar los pliegos de una nueva licitación cuyo llamado será dentro de treinta días, sin que esté prevista una prórroga de la actual concesión", expresó Meoni en su columna del 3 de febrero pasado.

El funcionario dijo que "se trata de una licitación compleja", que "requiere de un cuidadoso tratamiento técnico y un desarrollo orgánico y soberano de las políticas de Estado que comprendan las condiciones necesarias de una Argentina productiva".

Meoni anunció que en paralelo a la licitación se creará una agencia nacional "que funcionará de inmediato como órgano de contralor de las obligaciones del concesionario, lo que nunca ocurrido en los últimos 25 años".

La convocatoria a la reunión, por resolución 23/2021 del Ministerio de Transporte, indica que se celebrará mañana a las 10 en la sede del Gobierno de Santa Fe en la ciudad de Rosario, ubicada en calle Santa Fe 1950.

El temario incluye la constitución formal y presentación del Consejo Federal Hidrovía (CFH), la enunciación de sus objetivos y un informe de gestión sobre el estado actual de la concesión de la Hidrovía Paraguay-Paraná por parte del Ministerio de Transporte.