---
category: Agenda Ciudadana
date: 2021-02-04T03:46:22-03:00
thumbnail: https://assets.3dnoticias.com.ar/paso.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Infobae
resumen: 'Suspensión de las PASO: los gobernadores de Juntos por el Cambio buscarán
  convencer a la oposición'
title: Los gobernadores de Juntos por el Cambio buscarán convencer a la oposición
  para que apoye la suspensión de las PASO
entradilla: Gerardo Morales y Gustavo Valdés buscan aval para derogar las primarias
  en su propia fuerza política. La postura de la Casa Rosada y el debate interno en
  el peronismo.

---
La discusión por la **suspensión de las PASO** comienza a meterse lentamente en la agenda política del oficialismo y la oposición. Traspasa la voluntad de los gobernadores y se mete en los pasillos de la Casa Rosada y el Congreso. Existe un solo objetivo para los mandatarios que quieren que las primarias se suspenda: **buscar un consenso político amplio que se replique en el Parlamento.**

**Alberto Fernández** fue claro ante la consulta de todos los gobernadores. En público o en privado siempre les dijo que la ley para suspender las PASO solo puede avanzar si hay un acuerdo amplio. Se trata de la modificación de las reglas electorales en el mismo año de las elecciones. **Una decisión de ese calibre no puede ser solo aprobado por un sector del arco político.**

En Argentina hay cuatro mandatarios de **Juntos por el Cambio** y tres posturas diferentes sobre que hacer con las elecciones primarias este año. **Gerardo Morales (Jujuy)** y **Gustavo Valdés (Corrientes)** militan la suspensión de los comicios por única vez. **Rodolfo Suárez (Mendoza)** sostiene que donde haya competencia, se sostengan las PASO y donde no, que se eliminen. **Horacio Rodríguez Larreta (CABA)** está en contra de suspender los comicios.

***

![](https://assets.3dnoticias.com.ar/gobernadores-jxc.webp)

***

Los dos gobernadores que respaldan la suspensión de las PASO, y que estuvieron en la Casa Rosada durante la mañana del miércoles reunidos con Eduardo “Wado” De Pedro, **hablarán en los próximos días con el jefe de Gobierno porteño para intentar convencerlo de que el mejor camino a seguir es bajar las primarias por única vez.** Evitar concentrar gente en los colegios y en las calles, y llevar a cabo una única elección en octubre.

Para agosto la Argentina va a estar en pleno proceso de vacunación y, según adelantan algunos funcionarios, como es el caso del viceministro de Salud bonaerense Nicolás Kreplak, la llegada del invierno puede generar una segunda ola de contagios. A ese contexto se suma el retraso de la llegada de las vacunas, lo que impactó de lleno en el proyecto de vacunación y modificó el cronograma dispuesto por el ministerio de Salud.

El plan de vacunación ya no tiene precisiones. Rusia no da señales claras de en que momento llegarán las dosis de la Sputnik V, **Aztrazeneca prometió enviar 1.200.000** de vacunas antes de que termine febrero, por fuera de las dosis ya acordadas, y el Gobierno negocia con China la llegada de **1 millón de dosis en el corto plazo** de la vacuna que desarrolla el laboratorio Sinopharm.

***

![](https://assets.3dnoticias.com.ar/paso-agosto.webp)

###### Las elecciones primarias se deberían realizar el segundo domingo de agosto (NA)

<br/>

Con ese escenario plagado de incertidumbre, **Morales y Valdés consideran que lo más acertado es suspender las PASO.** No son solo ellos los que tienen esa idea: **19 de los 24 gobernadores que tiene la Argentina piensan lo mismo.** Algunos militan la causa, otros prefieren el silencio y hay otros que son cuidadosos en sus palabras y tratan de no correrse demasiado de la línea oficial.

**Los dos mandatarios de la UCR quieren dar la discusión en Juntos por el Cambio**. Saben que será una tarea extremadamente compleja y que **el probable resultado es negativo para sus aspiraciones.** Sin embargo, no quieren quedarse con la opinión atragantada y en los días que vienen van a poner el tema sobre la mesa.

En el espacio político más importante de la oposición se mantienen en la misma postura desde hace tiempo. **No se pueden cambiar las reglas electorales en el mismo año en que se realizarán las elecciones**. Eso piensan. En noviembre del año pasado la UCR, el PRO y la Coalición Cívica habían sacado un comunicado en el que apuntaban contra el peronismo por querer modificar el cronograma electoral. Ahora quienes también quieren hacerlo están adentro del espacio.

Morales y Valdés discutirán bajo el techo opositor la necesidad de suspender las elecciones frente a un escenario sanitario imposible de descifrar en el corto plazo. Lo que ambos tienen en claro es que no tensarán la cuerda demasiado dentro de Juntos por el Cambio. **Si la postura mayoritaria de la coalición es mantener las PASO, entonces ellos se llamarán a silencio, bajarán las gestiones y se dedicarán a ver que hacen con sus elecciones provinciales.**

En Corrientes es posible que Valdés desdoble la elección en el corto plazo. En esa provincia se elige gobernador y, tradicionalmente, se suelen adelantar los comicios para no hacerlos coincidir con los nacionales. En Jujuy, Morales va a extender la definición sobre el calendario electoral hasta tener más certezas sobre el escenario sanitario de su provincia, donde hay un rebrote de casos de coronavirus.

El Gobierno es otro actor fundamental en la discusión. **Alberto Fernández no se pondrá a la cabeza de las negociaciones para aprobar la ley.** En el Ministerio del Interior insisten que solo un cambio abrupto de la curva de contagios puede cambiar el cronograma electoral. En la Casa Rosada intentan disminuir la discusión a la mínima expresión. El tema no es primordial.

***

![](https://assets.3dnoticias.com.ar/postura-pte-af-paso.webp)

###### Alberto Fernández se mantiene en el medio de la discusión por las PASO y no encabeza las negociaciones para derogarlas.

<br/>

En el grupo que se resiste a la suspensión de las elecciones no solo está Juntos por el Cambio, sino también La Cámpora. **La agrupación ultra K quiere aprovechar las primarias para discutir candidaturas en todas las provincias y hacer crecer su representación en los distintos escalones del estado.** Es justamente ese movimiento el que intentan evitar los gobernadores, que miran con recelo, históricamente, a la agrupación que conduce Máximo Kirchner.

**A los gobernadores les incomoda la posibilidad de que kirchnerismo les arme listas en sus provincias para competir con los candidatos oficiales** y los fuercen a una negociación para repartir lugares, poder e influencia. La Cámpora afronta un importante desafío electoral en el 2021 para posicionarse en el armado estatal de cara al 2023. Más lugares dan más visibilidad y, a futuro, más influencia. No quieren perder esa oportunidad.

En Juntos por el Cambio advierten que el tema va tomando protagonismo en la agenda política y buscan evitar que divida aguas puertas adentro. “Esto no depende de la oposición, sino del peronismo. Si no hay ley es porque el peronismo no está de acuerdo. Tiran la pelota para este lado para que no se vea que se matan adentro”, asumen en la UCR.

***

![](https://assets.3dnoticias.com.ar/mk-campora.webp)

###### La Cámpora, agrupación que conduce Máximo Kirchner, se resiste a dar de baja las PASO.

<br/>

En el PRO, muy cerca de Rodríguez Larreta, sostienen que la posición del jefe de Gobierno porteño no se modificó y aseguran que **“es muy difícil”** que la cambie. Sin embargo, abren una pequeña puerta para tratarlo. “No es muy posible que avance la idea. Quizás se pudiera discutir si también discutimos la implementación de la boleta única”, indicaron desde Uspallata. A priori, saben que es imposible que eso suceda. El peronismo no discutirá el cambio en el formato de votación.

En la Coalición Cívica son terminantes. No hay margen para tratar la suspensión de las PASO. No se pueden cambiar las reglas en el año electoral, sea quien sea quien pida esa modificación. Si es necesario, darán la discusión interna con los gobernadores, pero están plantados en una postura muy definida.

La convocatoria para las **PASO** la tiene que hacer el Gobierno con una antelación no menor a los **noventa días** previos a su realización. Las primarias se deben llevar a cabo el segundo domingo de agosto. Sobre la base de esa cuenta, el **8 de mayo** es la fecha límite para definir si los comicios se van a llevar a cabo o no.

***

![](https://assets.3dnoticias.com.ar/gobernadores-paso.webp)

###### De los 24 gobernadores, 19 quieren que las PASO se suspendan.

<br/>

Los gobernadores que defienden la suspensión quieren que la decisión, con base en un acuerdo político, se tome ya. Rápido. Más tiempo pasa, más se dilata el proceso de debate y menos tiempo queda para aprobar la derogación de las PASO por única vez. **Tienen que conseguir los votos y, al día de hoy, los votos no están.**

Juntos por el Cambio, en su mayoría, no quiere acompañar la suspensión. La Cámpora tampoco. Los legisladores bonaerenses, muchos referenciados en el kirchnerismo, no levantarán la mano para no ir en contra de **Máximo Kirchner y Axel Kicillof,** que ya dejó en claro que para él no es prioridad esta discusión. Hay voluntad pero faltan votos. Y, además, el consenso debería ser más importante que juntar una mayoría especial en la Cámara de Diputados.

Por estas horas algunos mandatarios se preguntan cómo van a hacer campaña en el medio del proceso de vacunación y de una temporada de invierno donde los casos de coronavirus pueden volver a subir. Piensan que la gente los mirará extrañados cuando se vean obligados a hacer recorridas de campaña. La pandemia no terminó y la crisis económica es un tema de todos los días. Las PASO están en una agenda paralela donde se cruzan los intereses políticos.