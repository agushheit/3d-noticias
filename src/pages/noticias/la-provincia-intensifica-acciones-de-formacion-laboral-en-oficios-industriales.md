---
category: Estado Real
date: 2021-08-26T06:00:12-03:00
thumbnail: https://assets.3dnoticias.com.ar/PARQUEINDUSTRIAL.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La provincia intensifica acciones de formación laboral en oficios industriales
title: La provincia intensifica acciones de formación laboral en oficios industriales
entradilla: En el marco del programa Santa Fe Capacita se dio inicio a un curso de
  capacitación exclusivo para mujeres en instalaciones del Parque Industrial Sauce
  Viejo.

---
El ministro de Trabajo, Empleo y Seguridad Social Juan Manuel Pusineri junto al senador por el departamento La Capital, Marcos Castelló, visitaron las instalaciones del Instituto de Formación Profesional del Parque Industrial Sauce Viejo durante el inicio del “Curso de Electricidad Industrial”, el cual forma parte de la oferta del programa “Santa Fe Capacita”. Los funcionarios fueron recibidos por el representante de la Comisión Directiva del predio, Ángel Poma Re.

Las 14 participantes del curso son exclusivamente mujeres y el mismo está a cargo del docente Ivan Pisick, ingeniero del Instituto Nacional de Tecnología Industrial (INTI).

En ese marco, Pusineri celebró el comienzo de esta instancia destacando las posibilidades que brinda la articulación positiva entre estado, empresas y asociaciones civiles, así como la ventaja de poder formarse el mismo lugar donde se desarrollan tareas laborales similares: “Cuando trabajamos seriamente, ponemos en movimiento la dinámica productiva de nuestra región, generando propuestas formativas pensadas desde las necesidades del sector productivo y orientada hacia la adquisición de competencias técnicas específicas para agregar valor al trabajo local”.

Por su parte, la directora de Capacitación y Formación Laboral, Valeria March recordó que este tipo de capacitaciones se están llevando adelante en otros predios industriales de la provincia con muy buenos resultados: “Hoy Sauce Viejo se suma a una oferta de más de 600 cursos de formación laboral que estamos desarrollando con una enorme diversidad de destinatarios y modalidades de cursado, buscando mejorar la empleabilidad de las y los santafesinos e impulsando la inclusión de las mujeres en nuestro entramado productivo”.

También se hicieron presentes durante la visita Angelina Macua (Parque Industrial Sauce Viejo; Alejandro Taborda (UISF); Ivana Taborda y Cecilia Benso (CAMSFE), y Mercedes Mondino (Bahco).