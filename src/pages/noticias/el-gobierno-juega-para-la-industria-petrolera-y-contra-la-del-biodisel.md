---
layout: Noticia con imagen
author: "Fuente: LT10"
resumen: Industria del Biodiesel
category: Agenda Ciudadana
title: El gobierno juega para la industria petrolera y contra la del biodiesel
entradilla: "“Hacer menos biodiesel significa primarizar más nuestra economía”,
  afirma el titular de la Cámara Santafesina de Energías Renovables.  "
date: 2020-11-11T22:11:37.213Z
thumbnail: https://assets.3dnoticias.com.ar/silos.jpg
---
Lo afirmó por LT10 Juan Facciano, titular de la Cámara Santafesina de Energías Renovables, tras redactar una carta pública al presidente Fernández. “Hacer menos biodiesel significa primarizar más nuestra economía”, remarcó.

La Cámara Santafesina de Energías Renovables publicó una carta abierta la presidente de la Nación donde le pide que deje de favorecer al sector petrolero en desmedro de la industria del biodiesel y advierte del daño que generan estas políticas en las provincias productoras, entre las que se encuentra Santa Fe (una de las más importantes).

Juan Facciano, presidente de la Cámara Santafesina de Energías Renovables, expresó por LT10 que “la crisis de nuestro sector no tiene nada que ver con la pandemia porque data de diciembre del 2019. Nuestra industria está regulada por la Secretaría de Energía de la Nación, que establece el precio. El motivo puntual es que nos congelaron los precios y en octubre nos dieron un valor que está por debajo del costo de producción. Claramente, están favoreciendo a los petroleros”.

Además, el presidente de la Cámara Santafesina de Energías Renovables agregó que “Santa Fe junto a California están entre los mayores productores de biodiesel del mundo. Nuestro país importa más del 30% del gasoil que se consume y hoy tenemos una capacidad ociosa del 70% de biodiesel que se produce acá. La industria petrolera no quiere una diversificación de la matriz energética con un producto 100% renovable y de origen nacional. El gobierno es funcional a las petroleras”.

En un tramo de la entrevista, el dirigente recordó que el país tiene 54 plantas de biodiesel distribuidas en 10 provincias. Dentro de estas, 27 plantas son para el mercado interno, que hoy están paralizadas, y están en 6 provincias. “En esta industria trabajan más de 200 mil personas. Hacer menos biodiesel significa primarizar más nuestra economía”, subrayó.

Finalmente, el presidente de la Cámara Santafesina de Energías Renovables remarcó que “el año pasado, la industria de biodiesel de Santa Fe generó 400 millones de dólares (en toda su cadena). El biodiesel es una industria de industrias. Tener paralizado este sector perjudica a un entramado productivo cuyas víctimas son las provincias productoras”.

#### **La Carta al presidente de la Nación**

Como Ud. sabe, la industria de biocombustibles es hija de una Ley impulsada por Néstor Kirchner, quien tuvo el honor de tenerlo a usted como Jefe de Gabinete. Con profundo dolor, hoy las industrias del sector nos vemos obligadas a cerrar las plantas en nuestro país y dejar a varios miles de personas sin sus fuentes laborales.

No hay ley, en lo que va del siglo veintiuno, que haya creado un desarrollo industrial nacional tan importante como ésta.

Por una ley promovida por un peronista como Néstor Kirchner, durante su gobierno, peronista de verdad, y que hace una semana todo el senado aprobó prorrogar, se crearon miles de puestos de trabajo y más de 50 industrias en 10 provincias. Ese mismo Nestor Kirchner, cuyas primeras acciones fueron poner a la Argentina profunda y federal de pie, rescatando provincias del peor olvido y la más humillante exclusión. Así las cosas, y con esta decisión carente de la más mínima justificación técnica, una producción vital para casi la mitad del país, garante de un federalismo genuino -ese mismo que Néstor Kirchner pregonó-, esa producción con industrialización de la ruralidad será borrada de un plumazo. Ni con Macri, unitarista por excelencia, odiador serial de una Nación industrial, nos pasó lo que está por venir. Hoy, en un país cuyo interior tiene 44% de pobreza y 14% de desocupación, destruir empleo a propósito en nombre del peronismo es casi una nueva categoría criminal, inhumanidad cínica, agravada por la traición al vínculo.

Porque de nada sirve una Secretaría de Energía en Neuquén si desde allí se atenta contra las demás provincias integrantes de esta extraordinaria Nación, en defensa de un unitarismo petrolero, sectario y sin control alguno.

Hoy no tenemos opción, tenemos que cerrar y dejar a miles de familias sin trabajo con lo que eso implica en este contexto. Hemos sido ignorados desde el año 2016 y lamentablemente esa situación se extendió durante este año 2020, cuando el sector tenía enormes expectativas con la vuelta al gobierno del sector que lo impulsó.

En el caso del biodiesel de soja, mientras el país importa gasoil con dólares que escasean, millones de toneladas de soja se encuentran acopiadas sin liquidar que se podrían transformar en biodiesel para sustituir ese gasoil y en harina de exportación, generando miles de millones de dólares que Argentina necesita con urgencia.

Está el concepto de que el gasoil importado tiene menos costo que el biodiesel nacional. Nos preguntamos, con el mayor de los respetos: ¿a cuál valor del dólar hay que analizar el verdadero costo del gasoil importado? ¿Al oficial de $80, al que es muy difícil acceder? ¿O al de contado con liquidación de $150,  que es el costo de oportunidad para muchas empresas, que deben recurrir para cumplir sus obligaciones e importaciones porque no logran acceder al tipo de cambio oficial? A este análisis hay que añadirle que, por cada tonelada de biodiesel que se deja de producir, van a posponerse la liquidación de 4 toneladas de harina de soja. Además, el gasoil se paga con dólares por adelantado y el biocombustible con pesos a lo largo de toda la cadena industrial, y a plazos de  hasta  60 días.

Lamentablemente está el concepto que el gasoil importado paga impuestos específicos. Acá nos preguntamos: ¿Cuáles son los impuestos genuinos de Argentina? ¿Los impuestos como el IVA y Ganancias que pagan las industrias nacionales de biocombustibles o los impuestos específicos que Argentina le cobra a un gasoil que se produce en el extranjero? Nosotros  pensamos que Argentina necesita urgente dólares que respalden la moneda nacional y no cobrar impuestos específicos en pesos a productos importados.

Mientras las importaciones de combustible presionarán las reservas del Banco Central, la industria de biocombustible estará tristemente cerrada y no será por culpa de la pandemia.

El Presidente Alberto Fernández pidió al pueblo argentino que se le haga saber cuándo, sin quererlo, por supuesto, se desviaba del peronismo que sabemos él profesa. Y enfatizamos por lo expuesto: peronismo y kirchnerismo que ÉL profesa. No perdemos las esperanzas, ni la fe en sus promesas de campaña, ni en su firmeza actual para poner a la Argentina de pie y reconstruirla. Pues bien, no se pone de pie a la República, ni se la reconstruye, empujando al mar -con topadoras manejadas por empresarios del petróleo que vuelven a creer que el país les pertenece- a las decenas de pymes de los biocombustibles y sus miles de pymes asociadas.

No queremos un precio que nos garantice rentabilidad mientras el país no logra recuperar la economía.

¡Ayúdenos, Señor Presidente! ¡Confiamos en que usted no permitirá que otra industria desaparezca, ni que miles de personas se queden sin trabajo!