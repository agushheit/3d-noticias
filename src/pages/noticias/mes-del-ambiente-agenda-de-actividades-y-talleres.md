---
category: La Ciudad
date: 2021-06-06T06:15:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/AMBIENTE.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Mes del Ambiente: agenda de actividades y talleres'
title: 'Mes del Ambiente: agenda de actividades y talleres'
entradilla: La Municipalidad lleva adelante acciones relacionadas con la separación
  de residuos en origen, compostaje familiar y construcción de composteras, viverismo,
  elaboración de insecticidas naturales, entre otros

---
. Los interesados en participar de las actividades que detallamos a continuación, deberán inscribirse al correo electrónico: ambiente@gmail.com.

En el marco del Mes del Ambiente, la Municipalidad lleva adelante una iniciativa denominada “separá para reciclar”, que incorpora y coordina diferentes acciones haciendo foco en los vecinos como protagonistas casi exclusivos del verdadero cambio de paradigma.

En ese marco y con la educación ambiental como base para el Desarrollo Sostenible de la Ciudad, se organizaron una serie de Talleres Ambientales a través de plataforma virtual, para brindar a los vecinos las herramientas para la gestión de los residuos y la protección del ambiente.

En los mismos, se abordarán temas relacionados con la separación de residuos en origen, compostaje familiar y construcción de composteras, viverismo, elaboración de insecticidas naturales, entre otros. Los interesados en participar de las actividades que detallamos a continuación, deberán inscribirse al correo electrónico: ambiente@gmail.com

**Las propuestas**

– Taller de compostaje: Se realizará el jueves 17, a las 15 horas. Apunta a dar una respuesta a los ciudadanos preocupados por cuestiones ambientales y particularmente por los residuos que produce, generando la oportunidad de incorporar una herramienta de gestión de los residuos que, a la vez, permitirá reciclar los nutrientes en el abono para ser utilizado por la misma familia en el mejoramiento de sus plantas o producción de alimentos. Como beneficio extra, se reducirá drásticamente la generación de residuos.

– Taller separación de residuos: Se hará el lunes 14, a las 14 horas. Cada vecino produce en promedio 1 kg de residuos por día, la mitad de ese peso corresponde a restos orgánicos, esta fracción presenta problemas tanto en su disposición inicial como en su traslado y disposición final. Por otra parte, al no separarse del resto de los residuos, genera una pérdida de calidad en el transporte y dificultad de separación de los materiales valorizables en la Planta de Clasificación, por este motivo es que incentivamos a través de este Taller la Separación en Origen a fin de reducir los residuos con destino al Relleno Sanitario y valorizar los reciclables propiciando la economía circular. Junto con este, se dicta el Taller de Consumo Responsable y Reciclado de Plástico que se hará el miércoles 9, las 14 horas.

– Construcción de composteras: Se llevará a cabo el viernes 25, a las 14 horas. El compost es el producto que se obtiene luego de reciclar la materia orgánica. Es un proceso natural y muy fácil de realizar; y se puede hacer en el balcón, patio o jardín, con los residuos orgánicos que se generan en la cocina. La dinámica es súper simple y es un excelente fertilizante, pero se debe elegir la compostera que más se adapte a las necesidades y comodidades; para eso este taller brinda al vecino la posibilidad de construir la propia con elementos básicos.

– Taller de biopreparados: Se llevará a cabo el jueves 24, a las 15 horas. Al momento de realizar la huerta se debe tener en cuenta no solamente el diseño de la misma sino también la elaboración de preparados que ayuden al control de plagas y enfermedades que pueden aparecer durante el ciclo de cultivo. En este taller se les dará a los vecinos los conocimientos para elaborar estos productos a partir de restos de origen vegetal o sustancias de origen mineral que ayudan a disminuir los problemas de plagas y enfermedades o mejorar el desarrollo de los cultivos.

– Taller de viverismo: Se hará el martes 29, a las 19 horas. Este taller está pensado para aquellos vecinos que están haciendo sus primeros pasos en emprendimientos de carácter de vivero a escala familiar. Contando con información específica para ornamentales, nativas y aromáticas.