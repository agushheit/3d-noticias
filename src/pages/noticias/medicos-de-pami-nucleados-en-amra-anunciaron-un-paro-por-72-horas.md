---
category: Agenda Ciudadana
date: 2021-02-09T08:13:36-03:00
thumbnail: https://assets.3dnoticias.com.ar/jubilados-1.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Médicos de Pami nucleados en Amra anunciaron un paro por 72 horas
title: Médicos de Pami nucleados en Amra anunciaron un paro por 72 horas
entradilla: La medida de fuerza comenzará el miércoles 10 desde las 00 y se extenderá
  por 72 horas.

---
Médicos de Pami nucleados en Amra anunciaron su tercer paro en las últimas semanas. A diferencia de los dos anteriores, que fueron por 48 horas, el anunciado para el día miércoles 10 de febrero será por 72.

A través de un comunicado, Amra enumeró los puntos por los cuales llevarán adelante los reclamos:

*  Especificar la instrumentación del otorgamiento de turnos vía electrónica, esta metodología de ninguna manera debe limitar ni condicionar la cantidad de consultas médicas que los profesionales realicen (y posteriormente facturen) ya que, en ese supuesto, estarían trabajando sin percibir remuneración alguna;


* Atento a que el nuevo modelo no reconoce el arduo trabajo que los médicos realizan fuera del horario de atención dispuesto para afiliados de PAMI (redacción de recetas electrónicas, solicitud de estudios, etc.), consideramos que tanto las cápitas, como las consultas y visitas domiciliarias deben ser abonadas al 100% atento que, tal como fija nuestra Constitución Nacional, el trabajo debe ser remunerado (no se estaría cumpliendo esta manda constitucional);


* Visitas domiciliarias: urgente modificación y/o implementación de sistema de seguridad para la realización de las mismas, atento que los médicos no sólo arriesgan su salud y vida por el Coronavirus COVID-19, sino también muchas veces se encuentran expuestos a condiciones y lugares de inseguridad que hacen imposible la realización de la visita;


* Urgente cobertura sanitaria y salarial a trabajadores que resultaron infectados de COVID-19 en ocasión del cumplimiento de sus funciones, con retroactividad al inicio de la licencia correspondiente y proveer la entrega de elementos de Bioseguridad, hecho éste que han omitido durante toda la pandemia de COVID-19;


* Visita a geriátricos: negativa expresa a realizar las mismas, tanto por el riesgo de contagio a la comunidad mayor de edad como de los médicos de incurrir, involuntariamente, en el acto ilícito previsto en el Artículo 205 del Código Penal;


* El nuevo modelo debe necesariamente mejorar las condiciones laborales y contractuales de precariedad que padecen los médicos de cabecera del INSSJP.

Finalmente, aclaran que no llevarán adelante las visitas a geriátricos y domiciliarias "hasta tanto sean garantizados dichos extremos y expresamente exonerados los trabajadores de responsabilidad alguna".

![](https://assets.3dnoticias.com.ar/EtvDWZPXYAMl2Df.jpg)

    https://twitter.com/AMRASantaFe/status/1358894519513677826?ref_src=twsrc%5Etfw%7Ctwcamp%5Etweetembed%7Ctwterm%5E1358894519513677826%7Ctwgr%5E%7Ctwcon%5Es1_&ref_url=https%3A%2F%2Fsantafe.telefe.com%2Fnoticias%2Fmedicos-de-pami-nucleados-en-amra-anunciaron-un-paro-por-72-horas%2F