---
category: Agenda Ciudadana
date: 2022-01-13T06:15:05-03:00
thumbnail: https://assets.3dnoticias.com.ar/gines3.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: 'Reapareció Ginés González García: "No hice nada incorrecto, quizás cometí
  alguna estupidez"'
title: 'Reapareció Ginés González García: "No hice nada incorrecto, quizás cometí
  alguna estupidez"'
entradilla: 'El ex ministro de Salud también se quejó de la falta de apoyo del presidente
  Alberto Fernández ante el escándalo del Vacunatorio VIP.

'

---
A casi un año de su salida en medio del escándalo del denominado Vacunatorio VIP, el ex ministro de Salud Ginés González García reapareció públicamente y consideró que no hizo "nada incorrecto", aunque reconoció que quizás cometió "alguna estupidez" y también se quejó de la falta de apoyo por parte del presidente Alberto Fernández.

El ex funcionario nacional se refirió a la forma en que se produjo su renuncia de la cartera sanitaria y señaló que le dejó "un dolor grande, muy difícil de ser superado". "(Pero estoy) Con la tranquilidad de que no hice ninguna cosa que no estuviera habilitado, no hice nada incorrecto, pero quizás cometí alguna estupidez por admitir que di escasos... como consecuencia de que se negaron a ser vacunados en el lugar en el que les tocaba, el Hospital Posadas, y aparecieron por el Ministerio en un día en que yo no estaba", manifestó el ex embajador argentino en Chile.

En diálogo con FM Delta, González García se refirió al rol que tuvo el periodista Horacio Verbitsky en aquel escándalo que derivó en su salida del Ministerio de Salud: "A la distancia pienso... Me lo dicen muchos que por ahí no pudo haber sido fortuito, algunos creen que fue una emboscada, una cama. Yo no tengo pruebas".

Asimismo, el ex funcionario también lamentó el modo en que se produjo su salida del Gobierno: "Me hubiera gustado hablar yo, pero me enteré por la prensa de que ya me habían pedido la renuncia. Me hubiera gustado explicar que eran casos por ser personal de salud, personal estratégico, tener la edad o alguna patología. Me dolió, me bajoneó, me resultó injusto". De todos modos, reconoció que el presidente Alberto Fernández estaba bajo presión: "La gente está muy irritada, no es fácil gobernar en este tiempo".

Sin embargo, reiteró su enojo con el mandatario: "No me parecía justificable lo que pasó conmigo y se lo dije. En última instancia, si uno quiere conducir, tiene que bancar; si no, es difícil. Hubiera querido hablar, decir la verdad, que no era mi verdad, sino la verdad y nada más".

Por otra parte, al comparar la situación epidemiológica actual con la que había a comienzos de 2021, González García afirmó que "en ese momento había más respeto por la enfermedad, más miedo y hoy eso se perdió mucho, sobre todo en la gente más joven, por lo que ahora hay un comportamiento social totalmente distinto al que había hace un año". "Ahora tenemos a la población vacunada. La mejor manera de estar protegidos, individual y colectivamente, es estando vacunados", añadió.

Finalmente, el ex ministro de Salud estimó que habrá "una nueva generación de vacunas que van a extender la inmunidad, porque no puede haber un mundo en el que todos tengan que vacunarse cada tres meses: es imposible no sólo económicamente, sino también logística y socialmente. Los laboratorios líderes están trabajando en lograr una inmunidad más amplia y más duradera".