---
category: La Ciudad
date: 2021-12-21T06:15:21-03:00
thumbnail: https://assets.3dnoticias.com.ar/pasaportejpg.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Comienza a regir el pase sanitario: todo lo que hay que saber desde este
  martes en Santa Fe'
title: 'Comienza a regir el pase sanitario: todo lo que hay que saber desde este martes
  en Santa Fe'
entradilla: 'Alcanzará a todos los mayores de 13 años. ¿Qué actividades están alcanzadas
  y cómo acreditar el esquema completo de vacunación?

'

---
Desde este martes, comenzará a regir en la provincia de Santa Fe el pase sanitario. La medida, establece la necesidad de que toda persona que haya cumplido los 13 años de edad y que asista a las actividades de mayor riesgo epidemiológico y sanitario deberá acreditar que posee un esquema de vacunación completo contra el Covid-19 aplicado al menos 14 días corridos antes de la asistencia a la actividad o evento.

El mismo, debe ser exhibido ante el requerimiento de personal público o privado, designado para su constatación, y al momento previo de acceder a la entrada del evento o actividad.

**Actividades alcanzadas por el pase sanitario**

\- Viajes grupales de egresados, de estudiantes, jubilados, o similares.

\- Actividades en discotecas, locales bailables o similares que se realicen en espacios cerrados.

\- Actividades en salones de fiestas para bailes, bailes o similares que se realicen en espacios cerrados.

\- Eventos masivos organizados de más de MIL (1.000) personas que se realicen en espacios abiertos, cerrados o al aire libre.

\- Concurrencia a centros culturales, teatros, cines y gimnasios.

\- Participación en reuniones o celebraciones sociales en salones de eventos, fiestas y similares.

\- Asistencia a casinos y bingos.

\- Concurrencia a atracciones turísticas, en predios delimitados con controles para el ingreso de la concurrencia.

\- Realización de trámites presenciales ante organismos públicos provinciales.

**Dos opciones de documentación**

Una opción para acreditar el esquema de vacunación completo es a través de la aplicación denominada “Cuidar” **–** Sistema de prevención y cuidado ciudadano contra el Covid-19 – apartado “Información de Salud” en su versión para dispositivos móviles.

Otra alternativa es la página web[ www.santafe.gob.ar ](https://id.argentina.gob.ar/ingresar/), plataforma que cuenta con “el botón “pase sanitario” donde se puede acceder a la aplicación Mi Argentina, donde se encuentran los elementos digitales para comprobar que una persona tiene el esquema completo de vacunación”.

Las personas que no pudieran acceder a la aplicación podrán acreditarlo con certificado de vacunación contra el Covid-19 en soporte papel y/o formato digital, acompañado con el Documento Nacional de Identidad, en el cual consten las dosis aplicadas y notificadas al Registro Federal de Vacunación Nominalizado (Nomivac).

Por último, las autoridades municipales y comunales podrán disponer en sus respectivos distritos la necesidad de contar con el esquema de vacunación completo, para participar en otras actividades adicionales a las definidas en el presente Decreto.

También se recomendó a aquellos que ya tienen las app instaladas que vuelvan a a hacerlo, ya que las mismas presentaron distintos inconvenientes.