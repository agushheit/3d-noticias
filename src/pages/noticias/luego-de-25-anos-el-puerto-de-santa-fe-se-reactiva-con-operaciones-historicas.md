---
category: La Ciudad
date: 2021-03-21T06:00:28-03:00
thumbnail: https://assets.3dnoticias.com.ar/PUESRTOjpg.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Luego de 25 años, el puerto de Santa Fe se reactiva con operaciones históricas
title: Luego de 25 años, el puerto de Santa Fe se reactiva con operaciones históricas
entradilla: 'Desde junio de 2020 a la fecha, se realizaron 30 embarques que transportaron
  casi 100.000 toneladas de granos, algo que no sucedía desde 1996. '

---
El arribo esta semana de un convoy de cuatro barcazas, que cargaron más de 6.000 toneladas de granos, elevó la cifra récord de 30 embarques hasta la fecha y un acumulado desde junio pasado de casi 100.000 toneladas transportadas por agua.

“Para encontrar estos niveles de actividad en el puerto tenemos que remontarnos a 1996”, recordó el presidente del Ente Administrador del Puerto de Santa Fe, Carlos Arese. “Esta reactivación no es casualidad. El primer desafío fue generar confianza entre los productores y exportadores de la región, para que pudieran ver al puerto de Santa Fe como una alternativa viable para poder exportar sus productos por vía fluvial”, explicó.

“Desde el inicio de la actual gestión, planteamos al puerto como una plataforma de negocios para las pymes de la región, una opción de exportar sin intermediarios. En la actualidad, por ejemplo, abastecemos de granos (maíz quebrado, entre otros) a las industrias lácteas, avícolas y porcinas de Uruguay en un 70 por ciento de su demanda”, subrayó.

Precisamente, el país vecino es el destino al cual se apunta para a producción regional, pero los próximos objetivos serán abordar nuevos nichos, como Taiwán y el Extremo Oriente, como así también diversificar los productos exportados. “En este momento los silos del puerto trabajan al límite de su capacidad. El maíz que se está exportando se recibe en camiones y se quiebra aquí, agregando valor en nuestra propia planta. Por día, hay entre 50 y 80 camiones operando en la zona del puerto”, agregó.

**Inversiones y estrategias**

Asimismo, Arese sostuvo que la creciente operatividad de la estación fluvial es la consecuencia de importantes inversiones portuarias realizadas y estrategias planteadas tiempo atrás. “Nada de esto sería posible sin la previsión y las inversiones realizadas por el puerto, tanto en materia de dragado del canal de acceso y zona de maniobras, así como otras inversiones en infraestructura portuaria. Ahora tenemos una clara visión de un puerto productivo, un desafío que pudo revertir aquella imagen de instalaciones paralizadas, inactivas y obsoletas”, dijo.

Y agregó: “Con los sucesivos embarques fuimos recuperando la gimnasia portuaria, el músculo productivo del puerto santafesino: mejoramos los procesos y los rendimientos y lo hicimos invirtiendo en el reacondicionamiento de una infraestructura que estaba prácticamente en decadencia por el paso del tiempo. Estas acciones permitieron que las pymes de la región encuentren la posibilidad de exportar sus productos abaratando sus costos logísticos”.

"Desde el inicio de la gestión, el gobernador Omar Perotti nos pidió trabajar en la reactivación portuaria, y así lo hicimos, con el apoyo constante del Ministerio de Infraestructura, Servicios Públicos y Hábitat, que conduce la ministra Silvina Frana, porque el puerto de Santa Fe venía desarrollando diferentes actividades, pero no potenciaba su perfil productivo ni de exportación. Actualmente estamos transformando esa realidad", manifestó.

“Ver la intensa actividad y a los trabajadores en la planta, mientras se cargan y descargan camiones y se cargan barcazas, nos da una idea de lo que está sucediendo hoy en el puerto de Santa Fe. Los trabajadores volvieron a ser portuarios y eso es una gran alegría para ellos y para todos los santafesinos”, concluyó Arese.