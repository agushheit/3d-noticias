---
category: Agenda Ciudadana
date: 2021-09-11T06:15:51-03:00
thumbnail: https://assets.3dnoticias.com.ar/GRIPE.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Cómo serán los ensayos que buscan una vacuna única contra coronavirus, gripe
  y bronquiolitis
title: Cómo serán los ensayos que buscan una vacuna única contra coronavirus, gripe
  y bronquiolitis
entradilla: La vacuna trivalente, es una misma inyección que contiene los componentes
  contra los tres virus principales respiratorios; el coronavirus, el virus de la
  influenza y el virus sincicial.

---
El proyecto para diseñar una vacuna trivalente destinada a personas mayores que en una misma inyección contenga componentes contra la Covid-19, la gripe y la bronquiolitis comenzará con un ensayo clínico de un nuevo inoculante contra el virus sincicial respiratorio que causa la bronquiolitis, del que la Argentina será parte, dijo este viernes a Télam el médico e investigador Gonzalo Pérez Marc.

"La vacuna trivalente, que en una misma inyección contenga componentes contra los tres virus principales respiratorios que afectan a las personas mayores que son el coronavirus, el virus de la influenza (que causa la gripe) y el virus sincicial respiratorio (que causa la bronquiolitis) no existe todavía, pero existirá; y a eso es a lo que apuntamos con esta serie de ensayos", describió Pérez Marc.

Jefe de Investigación y Docencia del Departamento Materno-Infantil del Hospital Militar Central, Pérez Marc y su equipo --entre quienes se encuentra el infectólogo Fernando Polack-- vienen teniendo un rol central en la investigación de vacunas contra el coronavirus.

Por un lado, fueron el centro de investigación que más personas reclutó en la fase 3 de la vacuna desarrollada por Pfizer y, en efecto, encabezaron la publicación con los resultados en la revista The New England Journal of Medicine.

Pero, además, participan de un ensayo clínico de la vacuna conocida como "vegetal" desarrollada por Medicago que se está llevando adelante también en Canadá, Estados Unidos, Reino Unido y Brasil, y para el cual el Hospital Militar aportó la mayor cantidad de participantes.

Ahora, el desafío es comenzar con una serie de ensayos clínicos que, en principio, prueben por separado vacunas innovadoras contra el sincicial respiratorio y la influenza, lo que se encuentra actualmente en una etapa regulatoria y, en caso de que sea aprobada por la Administración Nacional de Medicamentos, Alimentos y Tecnología Médica (Anmat) empezaría en noviembre.

El desafío es comenzar con una serie de ensayos clínicos que, en principio, prueben por separado vacunas innovadoras contra el sincicial respiratorio y la influenza, lo que se encuentra actualmente en una etapa regulatoria. Foto: Marcelo Ochoa.

El desafío es comenzar con una serie de ensayos clínicos que, en principio, prueben por separado vacunas innovadoras contra el sincicial respiratorio y la influenza, lo que se encuentra actualmente en una etapa regulatoria. Foto: Marcelo Ochoa.

Los ensayos pasados y los que vendrán, el impacto de la pandemia en las vacunas y en la atención médica y el futuro de las vacunas son algunos de los temas sobre los que el investigador dialogó con Télam.

\- Télam: ¿Por qué haría falta una vacuna trivalente para personas mayores?

\- Gonzalo Pérez Marc: A partir del envejecimiento mundial, las personas mayores se han convertido en una población a cuidar y a mejorar su calidad de vida, y por supuesto están mucho más expuestos a los virus respiratorios que surgen, algunos de manera estacional y otro no, como el coronavirus.

A lo que se apunta es a tener una vacuna única contra los tres virus principales respiratorios que son el coronavirus, la influenza y el sincicial respiratorio, que es el de la bronquiolitis y que no se lo tienen muy en cuenta en adultos mayores, pero si uno ve algunas investigaciones genera un daño enorme a la salud de las personas mayores con comorbilidades casi a nivel de la gripe.

\- T: ¿Ya existe una vacuna de estas características?

\- GPM: No, pero existirá seguramente. Primero hay que investigar vacunas para cada uno de estos tres virus y después combinarlas.

Los estudios que vamos a iniciar a partir de noviembre (una vez que estén aprobados por la Anmat) y van a seguir a lo largo de dos años son un pool de ensayos que investigan vacunas con nuevas tecnologías.

Si cada una de estas vacunas es eficaz y segura, que es lo que pensamos que va a suceder, luego se avanzaría en la conformación de una vacuna con los tres componentes; como sucede con la triple viral que se le aplica a los niños.

Lo que pasó con Covid es que inició una revolución en cuanto al desarrollo de vacunas para los virus respiratorios, y esa revolución continúa con estos estudios que implican un cambio en el foco.

\- T: Los ensayos comienzan con la vacuna contra el virus de la bronquiolitis, ¿es una vacuna nueva?

\- GPM: Sí, se trata de una vacuna basada en una plataforma de proteínas recombinantes. También será nueva la vacuna contra la gripe y utilizará alguna de estas plataformas como ARN mensajero o proteínas recombinantes. En cuanto a la de Covid-19 utilizaremos alguna de las que están que usen estas tecnologías porque las ventajas de estas vacunas es que se pueden adaptar muy rápidamente a diferentes virus y variantes.

\- T: También participan del ensayo de la conocida como "vacuna vegetal", ¿en qué etapa se encuentra este estudio y cómo es está vacuna?

\- GPM: Ya hemos vacunado más de 7.100 personas con las dos dosis.

En cuanto a las características de la vacuna se la conoció como "vacuna vegetal" peo no es así. Se trata de una particular similar al virus que se produce adentro de una planta (N. benthamiana).

Lo que se hace es incorporar material genético de la proteína Spike del SARS-CoV-2 y lo que hace la planta es permitir que se desarrolle una partícula con todas las características de la proteína del coronavirus en una membranita de grasa, entonces se genera una partícula muy similar al virus; esto, sumado a que se le incorpora un potenciador de la respuesta inmunológica, genera una cantidad muy alta de anticuerpos neutralizantes.

Se trata de una plataforma muy innovadora que se venía investigando en vacunas para el virus H1N1 y ébola, pero esta prueba actual permite chequear la seguridad y el funcionamiento en una escala mucho mayor.

\- T: Teniendo en cuenta que en muchos de los países donde se realizan los ensayos clínicos un alto porcentaje de la población está ya vacunado, ¿cómo se están diseñando los estudios de vacunas a futuro?

\- GPM: Los nuevos estudios de vacuna contra Covid-19 tienen que apuntar a ser lo que se denomina una vacuna de booster o de refuerzo, pero de ninguna manera se puede pensar una vacuna de nuevo, es decir, que contemple población no vacunada.

A la vez es importante que se sigan desarrollando vacunas, porque si uno piensa la población mundial y que se está evaluando la posibilidad de que en los años que siguen las personas tengan que vacunarse cada año, se necesitan miles de millones de dosis; entonces lo mejor es tener muchas empresas para que no haya escasez.

\- T: Además de "revolucionar" el diseño de vacunas, ¿qué otra "revolución" nos dejará esta pandemia?

\- GPM: La forma de comprender a las enfermedades respiratorios en general. Por ejemplo, respecto de las prácticas hospitalarias, a todos los pacientes deberíamos atenderlos con barbijo; antes de la Covid-19 jamás usábamos un barbijo para atender a un niño; esto es algo que hoy nos resulta imposible y que debería ser un cambio hacia el futuro.