---
category: La Ciudad
date: 2021-06-14T06:00:38-03:00
thumbnail: https://assets.3dnoticias.com.ar/VHIPODROMO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'Villa Hipódromo: registran un avance del 80% las obras que mejorarán el
  barrio'
title: 'Villa Hipódromo: registran un avance del 80% las obras que mejorarán el barrio'
entradilla: 'Se concretan trabajos de cordón cuneta, pavimento, mejorado, y extensión
  de la red de agua potable para 500 vecinos; así como desagües pluviales. '

---
Hace seis meses, donde había zanjas hoy está el cordón cuneta; en las calles donde había tierra hoy está el asfalto o ripio; y donde la oscuridad era la protagonista por las noches, hoy la tecnología led cambió esa realidad. Además, unos 500 vecinos y vecinas abren sus canillas y tienen agua potable y segura; y lo más importante para los habitantes de este barrio es que cuando llueve el agua no ingresa a las casas porque los nuevos desagües cumplen la función correctamente.

Estos cambios ya son una realidad en Villa Hipódromo. Si bien faltan más acciones y finalizar algunas tareas, las obras se concretan a buen ritmo. Tienen un 80% de avance y en unos tres meses, otra será la postal del barrio. El intendente Emilio Jatón recorrió días atrás los trabajos y dialogó con los vecinos. "Dijimos que íbamos a integrar la ciudad y lo estamos haciendo, estamos cumpliendo con nuestra palabra", dijo.

El punto de encuentro fue Lamadrid y Cassanello, y desde allí Jatón caminó por las calles que ahora tienen asfalto. Luego llegó hasta Gaboto y Aldao, donde hace seis meses fue recibido por los vecinos, quienes les agradecieron porque las obras comenzaron a ejecutarse. "Son obras necesarias, imprescindibles y que estaban postergadas. Tras un acuerdo con el gobierno provincial y con el financiamiento de la Secretaría de Hábitat hoy las estamos logrando. Hace 50 años que la mayoría de los vecinos viven aquí y no tenían estas mejoras. Hoy llegó el asfalto", manifestó Jatón.

Luego detalló: "En Lamadrid habrá un bulevar con cantero central y debajo están los desagües con las bocas de tormenta. Villa Hipódromo siempre fue noticia porque se inundaba con poca lluvia. Hoy los vecinos nos dicen que, después de mucho tiempo, no se están inundando y para nosotros es una gran noticia".

Antes de finalizar, Jatón agregó: "Cuando el Estado está presente y con las obras que los vecinos hace mucho tiempo reclaman, se produce una sinergia en la que ellos se ponen contentos y nosotros también. Hay una plaza que transitamos durante muchos años y se ve su estado y su entorno, pero eso va a cambiar y es gracias al trabajo conjunto entre la provincia, el municipio y los vecinos".

**Emoción y agradecimiento**

Hace 50 años que Eduardo Romero vive en Villa Hipódromo y este jueves conversó con el intendente y aprovechó para agradecerle por poder ver cómo poco a poco su barrio está cambiando. "Estoy muy contento. Está quedando muy lindo, es una gran emoción. Antes con cada lluvia nos inundábamos todos y ahora pusieron cámaras y se va el agua más rápido. Y lo mejor de todo es que vamos a tener el asfalto. Y hay mucha y buena iluminación. Parece de día. Tenemos todo nuevo. Un gran avance", dijo con la voz tomada por la emoción.

Lidia Fierro vive sobre calle Lamadrid y con alegría escucha todas las mañanas cómo los motores de las máquinas se ponen en marcha. "Siento mucha felicidad. Hace 58 años que vivo acá y es la primera vez que veo una mejora de esta calidad. Toda la vida me inundé y hace dos lluvias, y a pesar que la obra no está terminada, que no entra el agua a mi casa. Es algo increíble", manifestó.

De la misma manera que su vecino, también su voz se quebró de la emoción y manifestó: "Son muchas las obras y el cambio es total. Al principio no lo podíamos creer, pero ahora es una maravilla. No importa lo que todavía falte porque ya es un hecho. Me re emociona porque nací, me crié, crié a mis hijos, ellos hoy no lo están viendo, pero les mandó fotos y no lo creen. Por eso tengo mucha felicidad porque alguien se acordó de Villa hipódromo".

**En detalle**

Por su parte, el secretario de Obras y Espacio Público municipal, Matías Pons Estel, dio más detalles de esta intervención: "Es una obra que ronda los 100 millones de pesos. Hoy está en un 80 a 85% de avance de obra. Tenemos estipulado unos tres meses todavía. Por un lado, se intervino en el sistema pluvial, era un sector muy anegado y se comprobó en las últimas lluvias que se está resolviendo".

También se trabajó en la parte vial ya que en casi todo el barrio se concretó el cordón cuneta y ripio y, en las calles troncales, se hizo el asfalto. "La importancia de toda esta infraestructura es la posibilidad de vincular el barrio completamente con el resto de la ciudad, a través de las avenidas principales", destacó Pons Estel.

El funcionario anticipó que hay otras acciones pendientes en el barrio que se van a comenzar a hacer dentro de tres meses, cuando se termine esta intervención. Eso incluye algunas calles que no están incluidas en la licitación, como los pasajes que se harán con equipos y fondos municipales; también se concretará una readecuación del sistema de iluminación con el cambio de tecnología.

Asimismo, hay un proceso participativo para concretar el arreglo de la plaza del barrio que es un lugar de encuentro de los vecinos y vecinas. A través de las Redes institucionales y los representantes del Estado en el territorio se lograron consensos para armar un proyecto y remodelarla.