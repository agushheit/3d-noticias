---
layout: Noticia con imagen
author: .
resumen: Puente Santa Fe - Santo Tomé
category: La Ciudad
title: Las gestiones nacionales pasan y la promesa del Puente Santa Fe - Santo
  Tomé queda
entradilla: La obra del puente Santa Fe-Santo Tomé fue excluida nuevamente del
  Presupuesto 2021 por parte del gobierno nacional.
date: 2020-11-04T13:40:14.880Z
thumbnail: https://assets.3dnoticias.com.ar/proyecto-nuevo-puente-2.jpg
---
El presidente del Concejo Municipal, **Leandro González**, presentó un proyecto planteando la necesidad de respaldo de parte de los senadores nacionales a la inclusión de **la obra del nuevo puente Santa Fe - Santo Tomé**, el cual fue excluido del proyecto de **Presupuesto 2021** por parte del gobierno nacional, dilatando nuevamente la ejecución de una deuda histórica que fue prometida en 2007 por el ex presidente Néstor Kirchner.

Ante la ausencia de partidas presupuestarias destinadas a la ejecución de esta obra de interconexión vial sumamente necesaria, González solicitó que la senadora y los senadores nacionales por Santa Fe introduzcan las modificaciones pertinentes al Proyecto de Presupuesto Nacional para el ejercicio 2021.

El **Presupuesto 2021**, enviado al Congreso el pasado 28 de octubre a través del Mensaje Nº 79/2020 del Poder Ejecutivo Nacional, ya fue aprobado por la Cámara de Diputados y actualmente se encuentra en el Senado para su tratamiento definitivo.

“Los senadores nacionales deben reclamar la inclusión de las partidas presupuestarias para avanzar en la concreción del Puente Santa Fe-Santo Tomé, **una deuda histórica del gobierno nacional**”, expresó el titular del Concejo local.

![](https://assets.3dnoticias.com.ar/puente-santa-fe-santo-tome.jpg)

En tal sentido, **González** recordó que la obra del puente ya cuenta con un proyecto ejecutivo y un acuerdo para su traza, aprobados luego de discutirse incluso en audiencias públicas. Una nueva traza vial para la conexión entre Santo Tomé y la ciudad capital no sólo permitiría aliviar los flujos circulatorios y optimizar la conectividad en el área metropolitana del **Gran Santa Fe** sino que, además, representaría una significativa mejora estratégica y comercial para la infraestructura del Corredor Bioceánico que une las costas del Atlántico y el Pacífico.

“**Los gobiernos pasan y la deuda histórica queda**. Ninguna de las gestiones nacionales de 2007 en adelante mostraron voluntad política de compensar a la provincia por las obras de la Autovía Nacional N° 19. En este primer año de gestión del presidente **Alberto Fernández**, queda reflejada una decisión política de no asignar presupuesto para esta **obra largamente esperada por vecinos y vecinas de Santa Fe y Santo Tomé**". Al mismo tiempo, señaló que "en otros temas **ya hemos visto falta de federalismo**, como en el caso de los subsidios al transporte público de colectivos, y por eso nos preocupa esta situación”.

"Al tener ya media sanción en Diputados, **estamos planteando a los senadores nacionales insistir por la construcción del nuevo puente y evitar que santafesinos y santafesinas sufran una nueva injusticia presupuestaria por parte del gobierno federal**, como la deuda reconocida por la Corte Suprema de Justicia y que involucra la devolución de fondos de coparticipación retenidos por años, que hoy supera los 120 mil millones de pesos”, manifestó el edil radical.

Vale recordar que el puente carretero que une a Santa Fe con Santo Tomé, cuenta con más de 80 años de vida y es recorrido por más de 40 mil vehículos diarios.