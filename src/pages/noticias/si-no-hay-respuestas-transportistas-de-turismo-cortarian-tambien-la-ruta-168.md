---
category: Agenda Ciudadana
date: 2021-05-12T09:23:10-03:00
thumbnail: https://assets.3dnoticias.com.ar/colectivos.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Diario Uno
resumen: Si no hay respuestas, transportistas de turismo cortarían también la Ruta
  168
title: Si no hay respuestas, transportistas de turismo cortarían también la Ruta 168
entradilla: 'Sería a la altura de la Fuente de la Cordialidad. El corte en la autopista
  a Rosario seguiría. Si bien el conflicto es nacional, reclaman una solución al gobierno
  provincial. '

---
El corte de transportistas de empresas de turismo que comenzó este martes por la mañana en la autopista Santa Fe-Rosario no sólo continuaba -se registraban serías dificultades de circulación en la zona de la rotonda del Club Colón-, sino que además referentes del sector adelantaron a El Litoral que la medida se sostendrá en tanto y en cuanto no reciban respuestas del gobierno provincial. Y no sólo eso: este miércoles podrían cortar también la Ruta Nº 168, a la altura de la Fuente de la Cordialidad.

“La idea es permanecer acá el tiempo que sea. La medida de protesta va a seguir, y si no tenemos algún tipo de solución mañana (por este miércoles) se cortará la Ruta 168, en la Fuente de la Cordialidad”, dijo a este diario Leandro Zamora, referente de los transportistas. Dependiendo del número de micros que lleguen y se sumen al reclamo, seguramente habrá un “doble corte” este miércoles (autopista y Ruta 168) en los dos principales accesos a la ciudad capital, con la cual no faltarán dificultadas en la circulación vehicular.

El corte en la autopista a Rosario se reactivó apenas pasada la hora 16 de este martes, en las dos manos: era cada 30 minutos. Zamora aseguró que al momento no tuvieron “ningún llamado ni comunicación” por parte del área de Transporte provincial. 

**El motivo del reclamo**

“Ya pedimos algún tipo de asistencia asistencia económica al gobierno, pero nunca llegó y seguimos postergados; desde que empezó la pandemia no pudimos trabajar. Y nos estamos endeudando para poder pagar los impuestos: ni eso nos condonaron. Toda la cadena del sector, desde las agencias de turismo hasta la hotelería, está muy mal”, se quejó Zamora.

También, “nos sentimos discriminados de que sí funcionen las líneas regulares de viajes de larga distancia, y no las líneas de las empresas de transporte de turismo”, añadió.

El reclamo de los transportistas es nacional: 12 provincias que acompañaban la medida. En Buenos Aires, referentes del sector estaban reunidos con autoridades del Ministerio de Transporte nacional. Según pudo saber este medio, la clave de conflicto es qué tipo de ayuda económica se acordará.

**"La situación es compleja"**

Osvaldo Miatello, secretario de Transporte provincial, le dijo a este diario que la problemática está está encuadrado nacionalmente. "Hay negociaciones a nivel nacional. Nosotros tenemos voluntad de diálogo y estamos trabajando en propuestas. Hay una amplia diversidad de actores (dentro del sector del turismo), algunos nacionales y otros provinciales. De manera que la situación es compleja y estamos buscando alguna alternativa para viabilizar una ayuda".

"Ahora, la verdad es que esta forma de protesta, que implica perjudicar a una gran parte de la sociedad, no ayuda en nada a encontrar una solución, que se está buscando. Nuestra vocación de diálogo está, en tanto y en cuanto esta medida de corte no siga perjudicando a la sociedad", dijo el funcionario.