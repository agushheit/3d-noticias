---
category: Estado Real
date: 2022-01-29T10:39:42-03:00
thumbnail: https://assets.3dnoticias.com.ar/2022-01-28NID_273698O_1.jpeg
layout: Noticia con video
link_del_video: https://youtu.be/0OnGQo4QySs
author: 3d noticias
resumen: La delegación oficial de la Provincia brilló en el Festival Nacional De Folklore
  de Cosquín.
title: La delegación oficial de la Provincia brilló en el Festival Nacional De Folklore
  de Cosquín.
entradilla: Con la Plaza Próspero Molina colmada de público, el elenco santafesino
  realizó su presentación, en la que se rindió homenaje al chamamé, los pueblos originarios
  y a Ariel Ramírez.

---
La delegación oficial santafesina se presentó en la Sexta Luna del Festival Nacional de Folclore Cosquín 2022, desplegando la danza y la música del chamamé, representando a todos y cada uno de los territorios, visibilizando la identidad de toda una provincia.

La propuesta artística santafesina tomó como eje la danza y la música del chamamé para invitar al público a realizar un viaje imaginario a través de la poética de un género que representa a vastos territorios de la provincia.

Un elenco de danza que puso en juego mucho más que movimiento, imágenes, música y palabras, y fue tejiendo una trama escenográfica que acercó a espectadores y espectadoras lo más profundo de la historia y el arte de Santa Fe.

Durante el emotivo show, que pudo verse por la Televisión Pública, se rindió homenaje al chamamé santafesino, a los pueblos originarios y al maestro Ariel Ramírez, de quien en 2021 se cumplió el centenario de su natalicio.

Con 65 artistas en escena y el chamamé como tema central, los artistas se lucieron sobre el escenario en la Plaza Próspero Molina de la ciudad de Cosquín ante una multitud de espectadores y el apoyo de quienes se acercaron con banderas desde las distintas localidades de la provincia santafesina.

Luego del espectáculo, que se vio demorado por la realización del partido de la selección nacional de fútbol contra su par chileno, debido a que la Televisión Pública transmitió tanto el cotejo como el festival, el ministro de Cultura de la provincia de Santa Fe, Jorge Llonch, aseguró: “Este emocionante espectáculo no va a quedar acá. Será parte de la programación del ciclo Santa Fe en tu Corazón 2022. Lo llevaremos a cada rincón de la provincia”.

Asimismo, el titular de Cultura felicitó “a cada una y cada uno de quienes integraron esta delegación oficial santafesina, que nos llenó de orgullo”.

**Tras el show, conferencia de prensa**

Posteriormente a la finalización del espectáculo de danza y música protagonizado por la delegación santafesina, se llevó adelante una conferencia de prensa a la que asistieron el secretario de Gestión Cultural, Jorge Pavarin; el director de Programación Artística, Claudio Cherep; el director artístico del elenco de baile, Renzo Cremona, y las cantantes Patricia Duré, Patricia Gómez y Daniela Massaro.

“Tanto el público en general como la propia prensa resaltaron la calidad de la puesta en escena y sobre todos los ejes que se abordaron, puntualmente la idea de reivindicación del chamamé que es algo que se dice que en Cosquín suele faltar”, sostuvo Cherep.

Ante otras consultas, el funcionario remarcó: “Lo que expusimos no tiene que ver con un trabajo individual sino con un trabajo colectivo de distintas áreas del Ministerio de Cultura de Santa Fe, como ser la investigación del chamamé que se llevó adelante y que ya tenía un año de trabajo, el recorrido por los pueblos originarios que es parte del programa Patentes y la reivindicación del maestro Ariel Ramirez. Todos temas que se abordaron durante el año que pasó”.

En tanto, Pavarin resaltó “el compromiso de la provincia en una estrategia de territorialidad que se expresó en una representación capaz de abarcar quince de los diecinueve departamentos santafesinos y se comprometió a que aquello que empezó con la presentación de la delegación pueda convertirse en un programa que recorra todo Santa Fe con estos artistas y con otros de los propios pueblos que visitaremos”.

Por su parte, tanto Patricia Gómez como Renzo Cremona agradecieron “la posibilidad de poder estar en nombre de su provincia y especialmente al Ministerio de Cultura de la provincia por ser fieles y respetar esta idea de territorio y de refrendar las identidades locales permitiéndole a artistas de distintos lugares poder mostrar lo que hacen habitualmente”.

Finalmente, Cherep destacó que “además, para sumarse a la movida de Cosquín más allá del escenario, la movida callejera es muy importante, tanto para la gente que viene a Plaza como para quien no. La provincia acompañó al artista santafesino Daniel Franich, que recorrió las calles coscoínas con un repertorio propio también vinculado al chamamé en sintonía con lo que vimos en el escenario”.

El director de Programación Artística señaló que “lo que llevó adelante Franich se llama «Estación Chamamé» y es un recorrido itinerante por las calles y las peñas, que fue alentado por el Ministerio”.

**Elogios a la puesta en escena**

Tanto el público en general como la propia prensa resaltaron la calidad de la puesta en escena y sobre todo los temas que se abordaron. “Todos destacaron la idea de reivindicación del chamamé”, insistió Cherep.

Como para que no queden dudas de que esa puesta no quedará sólo en la memoria y los registros fotográficos y audiovisuales, Patricia Gómez agradeció la idea del ministro Llonch de programarla para que recorra toda la provincia. “Lo que pasó en la noche de Cosquín fue maravilloso. Tanto con el público como con los artistas y los bailarines”, sostuvo la artista.

El propio titular de la cartera cultural agradeció a la delegación, se mostró “orgulloso de la manera en la que representaron a Santa Fe en el Festival Nacional de Folclore”, y aseguró que “lo sucedido va a que trascender Cosquín para convertirse en la cara visible de la provincia”.

### ![](https://assets.3dnoticias.com.ar/2022-01-28NID_273698O_1.jpeg)