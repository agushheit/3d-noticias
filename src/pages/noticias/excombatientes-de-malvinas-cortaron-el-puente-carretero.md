---
category: La Ciudad
date: 2021-08-11T06:15:03-03:00
thumbnail: https://assets.3dnoticias.com.ar/puente.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Excombatientes de Malvinas cortaron el puente Carretero
title: Excombatientes de Malvinas cortaron el puente Carretero
entradilla: Se trata de agrupaciones unidas de soldados continentales movilizados
  por la causa histórica de Malvinas. Reclaman por pensiones al gobierno provincial.

---
Este martes por la tarde, un grupo de soldados continentales movilizados bajo la causa histórica de Malvinas, decidieron cortar el tránsito sobre el Puente Carretero en la cabecera santafesina, en reclamo de pensiones sociales al gobierno de la provincia.

La medida duró más de dos horas y generó durante ese tiempo un caos vehicular en la zona. Finalmente, cerca de las 19, los manifestantes decidieron levantar el corte y acampar frente a Casa de Gobierno, en la plaza 25 de Mayo, a la espera de respuestas.

Un grupo de exsoldados, que no estuvieron combatiendo en las islas, pero sí en el continente, cortaron el tránsito en el Puente Carretero este martes por la tarde luego de manifestarse por la mañana frente a Casa de Gobierno en reclamo de pensiones.

El corte se registró en la cabecera santafesina del viaducto que une las ciudades de Santa Fe y Santo Tomé y fue total en las dos manos. La policía desvió el tránsito por la Circunvalación.

Minutos antes de las 19, los manifestantes decidieron levantar el corte a la espera de respuestas por parte del gobierno provincial, pero según informaron, no descartan más acciones de protesta en las próximas horas.

El corte de tránsito en la cabecera santafesina del puente Carretero fue levantado por la manifestantes, los cuales decidieron acampar frente a Casa de Gobierno a la espera de respuestas este miércoles por parte de las autoridades provinciales