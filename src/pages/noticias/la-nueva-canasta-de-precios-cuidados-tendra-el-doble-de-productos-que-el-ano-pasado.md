---
category: Agenda Ciudadana
date: 2021-01-12T09:33:26Z
thumbnail: https://assets.3dnoticias.com.ar/12121-precios-cuidados.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: '3DNoticias - Fuente: Télam'
resumen: La nueva canasta de Precios Cuidados tendrá el doble de productos que el
  año pasado
title: La nueva canasta de Precios Cuidados tendrá el doble de productos que el año
  pasado
entradilla: A la nueva canasta de Precios Cuidados se suma un incremento en la oferta
  de categorías de productos, con 13 nuevos rubros, con mayor variedad en marcas,
  sabores y fragancias de un mismo tipo de artículo.

---
La nueva canasta de artículos del **programa Precios Cuidados** que entra en vigencia el 12 de enero alcanzará a 660 productos, el doble de los que tuvo hasta enero del año pasado, con la incorporación de carnes y más artículos para celíacos e higiene femenina, preciaron esta tarde fuentes del Ministerio de Desarrollo Productivo.

Esta mayor cantidad de productos se da con dos objetivos: generar referencias de precios en góndolas que no tenían presencia dentro del programa y robustecer las referencias actuales, sobre todo en aquellas categorías donde se verifica una diferenciación significativa en cuanto a variedad –fragancias, sabores– y presentaciones –tamaño, empaque–, agregaron las mismas fuentes.

La renovación del programa **Precios Cuidados 2021** tiene como principal objetivo la incorporación de nuevos productos de primeras y segundas marcas que actúan como referencia para las y los consumidores, así como una mayor diversidad de productos elaborados por pymes.

De esta forma, **Precios Cuidados** ofrecerá el doble de productos que, en enero de 2020, con una canasta compuesta por 660 artículos de alimentación, bebidas e higiene personal y limpieza, entre otros rubros.

**A la nueva oferta se incorporaron más de 260 productos representativos del consumo de los argentinos.**

En lo que respecta a los precios, el aumento promedio para el trimestre será de 5,6%.

Desde la Secretaría de Comercio Interior subrayaron que **se suman 38 nuevas primeras marcas y productos destacados**.

A esto se suma un incremento en la oferta de categorías de productos, con 13 nuevos rubros, con mayor variedad en marcas, sabores y fragancias de un mismo tipo de artículo

Las autoridades adelantaron que se triplicará la oferta de leches larga vida, que pasará de 4 a 12 productos, al tiempo que el rubro lácteo en general incrementará su presencia en el programa, al pasar de 58 a 82 artículos

Además, en el nuevo **Precios Cuidados** se duplicará la cantidad de artículos esenciales Covid, con un total 59 productos frente a 30 que existen en la actualidad, entre los que se destacan lavandinas, alcoholes en gel, jabones, limpiadores antibacteriales, paños y guantes, entre otros.

Por su parte, la canasta para personas celíacas alcanzará los 37 productos específicos. Suma ocho nuevos artículos de premezclas, galletitas y alfajores, además de los alimentos de **Precios Cuidados** que no contienen gluten.

También habrá más productos de gestión menstrual como: protectores diarios (marca Carefree) y toallas higiénicas.

Desde el Ministerio de Desarrollo Productivos subrayaron que el nuevo programa duplicará la oferta de frutas y verduras, debido a que contará con 2 variedades de frutas y 8 verduras de estación, con un esquema de renovación mensual.

En carnes, el acuerdo contempla la presencia de cortes populares como espinazo, carne picada, _roastbeef_ y tapa de asado.

La semana pasada, la Secretaría de Comercio Interior, Paula Español, adelantó  que el programa iba a mantener las características de siempre: seguirá siendo anual, con revisiones trimestrales y con precios congelados por tres meses.

La aplicación de Precios Cuidados se divide en cinco regiones: Área Metropolitana de Buenos Aires (AMBA); el resto del territorio bonaerense; Centro, Cuyo y Litoral; Noreste (NEA) y Noroeste (NOA); y Patagonia.

> «Ese espíritu del programa de mantener los mismos precios en todas las bocas de expendio de una misma región durante tres meses es clave», afirmó Español.

<br/>