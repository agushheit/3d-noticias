---
category: La Ciudad
date: 2021-10-23T06:00:40-03:00
thumbnail: https://assets.3dnoticias.com.ar/ESCRITURAS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Casa propia: Jatón entregó escrituras a otras 59 familias de la ciudad'
title: 'Casa propia: Jatón entregó escrituras a otras 59 familias de la ciudad'
entradilla: En un acto que se desarrolló en la Estación Belgrano, vecinos de los barrios
  Yapeyú, San Agustín y Ceferino Namuncurá recibieron los títulos de propiedad de
  sus viviendas. Ya se llevan entregadas 260 títulos.

---
La Municipalidad de Santa Fe entregó escrituras a 59 familias de los barrios Yapeyú, San Agustín y Ceferino Namuncurá. El acto tuvo lugar en la Estación Belgrano, con la presencia del intendente Emilio Jatón y la titular de la Agencia Santa Fe Hábitat, Paola Pallero. Con estos títulos se llegan a las 263 escrituras ya entregadas por la actual gestión municipal. Además, hay 880 legajos que se encuentran en proceso de escrituración.

Del acto participaron vecinos que desde hace décadas esperaban recibir el título de propiedad de sus viviendas. En ese contexto, el intendente aseguró que “hoy llegó el día” y detalló cómo es el proceso de realización del trámite: “Los visitaron en sus casas, constataron que los terrenos están legalmente constituidos y hoy llegó el momento, hoy van a tener su escritura. Se la llevan a su casa y a partir de ahí les va a cambiar la vida”, celebró.

El mandatario santafesino convocó a los flamantes propietarios a que “disfruten este día de mucha felicidad” y destacó que “a partir de ahora con la escritura en mano, ustedes van a saber que nadie más los va a poder sacar de su casa”.

**Plan integral**

Jatón contextualizó que la regularización dominial se inscribe en una política integral que incluye la transformación de los barrios del Noroeste a través del plan Integrar Yapeyú, que alcanza a cuatro barrios como San Agustín, Loyola, La Ranita y Yapeyú. Los trabajos consisten en incorporar iluminación con tecnología led, agua potable, además de construir desagües pluviales, brindar conectividad vial en sentido este-oeste y la puesta en valor de Avenida 12 de Octubre, para garantizar seguridad de accesibilidad a más de 2.300 familias.

Jatón planteó que “este retraso de la llegada del Estado en esos barrios algún día tiene que terminar, algún día tiene que desaparecer, algún día tenemos que vivir todos en las mismas condiciones, tengamos o no tengamos plata. Eso es lo que empezamos a hacer, es lo que empezamos a trabajar y eso es lo que ustedes van a sentir en todo este período”, afirmó.

**Política habitacional**

Por su parte, Pallero planteó que la regularización dominial “refleja el trabajo que venimos llevando adelante con los colegios de agrimensores y arquitectos, muestra el trabajo institucional y, sobre todo, la transparencia que venimos teniendo en el proceso de escrituración”.

La funcionaria aseguró que “se trata de una política habitacional que venimos marcando fuerte desde la Agencia de Hábitat y desde la Municipalidad de Santa Fe”. Al respecto, aseguró que la escritura “representa el documento más importante que puede tener una familia, es la seguridad jurídica de su vivienda, es saber que pueden ampliar o mejorarla, saber que pueden sacar un crédito o, simplemente, que va a quedar para sus hijos”.

**Construir hogar**

La concejala Laura Mondino participó del acto junto con el presidente del cuerpo legislativo Leandro González y sus pares María Laura Spina y Mercedes Benedetti.

En la ocasión, Mondino destacó la política de regularización dominial asegurando que la escritura “es el documento que ustedes les van a dejar a sus hijos, a sus nietos, y es lo que va a fortalecer el concepto de hogar y de familia. Así que para nosotros es una alegría estar acá y compartirla con ustedes, porque seguramente más de uno estará muy contento y emocionado en un día tan importante como hoy”.

Mauricio Ruiz Díaz desde hace 13 espera tener la escritura de su vivienda. “Con mi señora estamos hace 15 ya, algo impensado que cayó de Dios. Emilio lo hizo realidad y estoy muy contento por mi familia”. El vecino de barrio Yapeyú compartió que ahora podrá dejarle la casa a sus tres hijos: “Estoy muy contento de esta gestión. Las mejoras que se van haciendo son algo muy lindo que mis hijos van a poder disfrutar”.

Luis Martin Fernández es de barrio San Agustín I y hace 40 años que esperaba la entrega de las escrituras de su vivienda. Antes de construir su casa en el actual terreno del que hoy es propietario, que en sus comienzos fue un rancho de chapa y cartón, vivió en su auto junto con su esposa. Con el correr de los años, pudo edificar y hoy cumplió un sueño, su realidad es otra: “La estábamos esperando, sabíamos que algún día nos iban a dar la escritura y estamos re contentos”.