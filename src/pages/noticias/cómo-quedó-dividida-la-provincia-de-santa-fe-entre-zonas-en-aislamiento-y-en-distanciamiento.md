---
layout: Noticia con imagen
author: .
resumen: "Santa Fe: ASPO y DISPO"
category: Agenda Ciudadana
title: Cómo quedó dividida la provincia de Santa Fe entre zonas en aislamiento y
  en distanciamiento
entradilla: El gobierno de Santa Fe prorrogó el Aislamiento Social, Preventivo y
  Obligatorio (ASPO) en las ciudades más afectadas por la pandemia. Pasan a la
  fase de Distanciamiento (DISPO) las que tienen menor cantidad de casos.
date: 2020-11-10T14:53:25.734Z
thumbnail: https://assets.3dnoticias.com.ar/aspo1.jpg
---
Mediante un decreto, se prorrogó el Aislamiento Social, Preventivo y Obligatorio (ASPO) en las ciudades más afectadas por la pandemia de coronavirus y se dispuso que las que tienen menor cantidad de casos pasen a la fase de Distanciamiento (DISPO).

De esta forma, prosiguen en aislamiento hasta el 29 de noviembre los departamentos Rosario, General López, Caseros, San Lorenzo y Constitución (todos del sur de la provincia), y también La Capital, Las Colonias y Castellanos, los tres en el centro de la provincia.

En tanto, pasan a la fase de Distanciamiento algunos departamentos del centro sur (San Martín, Belgrano, Iriondo y San Jerónimo) y todos los situados en el norte del distrito (San Cristóbal, 9 de Julio, Vera, General Obligado, Garay, San Javier y San Justo).

En cuanto a los cambios más importantes por actividad, las obras privadas de construcción fueron habilitadas para contar con un máximo de 30 trabajadores en todo el territorio provincial, en tanto los locales gastronómicos podrán permanecer abiertos hasta las 0.30hs, con ocupación del 30% de su superficie para los locales cerrados.

También están habilitadas las actividades religiosas de hasta 30 personas, el servicio de cuidado de adultos mayores, personas con discapacidad, niños, niñas y adolescentes, y de peluquería, manicuría, cosmetología y podología: con turno previo y con una ocupación máxima del 50% del local.

Además se permite la práctica de deportistas olímpicos y profesionales que cuenten con la autorización nacional, las salas de grabación y ensayo, el servicio de personal doméstico y salidas breves para caminatas sin extenderse más allá de las 20hs, entre otras.

En contrapartida, no está permitida en Santa Fe la reunión de personas en momentos de descanso, esparcimiento, comidas sin el estricto cumplimiento de la distancia social de dos metros entre los concurrentes y sin ventilación adecuada del ambiente.