---
category: La Ciudad
date: 2021-12-28T06:00:45-03:00
thumbnail: https://assets.3dnoticias.com.ar/temporarios.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Abrió el Registro de Alquileres Turísticos Temporarios
title: Abrió el Registro de Alquileres Turísticos Temporarios
entradilla: Pueden inscribirse inmuebles destinados a alojamiento desde un día a tres
  meses. El trámite, que permitirá ampliar la oferta de plazas disponibles en Santa
  Fe, es 100 % virtual.

---
La Municipalidad de Santa Fe informa que ya abrió el Registro de Alquileres Turísticos Temporarios de casas y departamentos para alojamiento desde una noche hasta tres meses.

El trámite es 100 % digital desde la Oficina Virtual de la Municipalidad de Santa Fe a través de este [enlace](https://oficinavirtual.santafeciudad.gov.ar/web/inicioWebc.do?opcion=noreg&redirige=d2ViL2ZpY2hhQXN1bnRvLmRvP3J1dGEmYXNhc19pZGVfYXN1PTUyMw==) y también puede realizarse de forma presencial en la Dirección de Turismo que funciona de lunes a viernes de 8 a 14 en la Estación Belgrano, en Bulevar Gálvez 1050.

Franco Arone, director de Turismo del municipio, destacó las ventajas de la implementación de este registro para el municipio: “Como primera medida, permite ampliar la plaza de alquileres o alojamientos en la ciudad de Santa Fe. Estimamos que entre 1200 y 1500 plazas se incorporarán formalmente al registro”. En ese sentido, agregó que “hoy tenemos unas 3000 plazas y si logramos incorporar las más de mil disponibles como alquileres temporarios, nos va a permitir salir a buscar eventos que hoy no llegan a la ciudad porque no nos dan las plazas de alojamiento”.

El funcionario definió que esta iniciativa se enmarca en “el ordenamiento de la oferta turística de la ciudad: a partir de ahora toda esta oferta que permanecía invisible, porque no la podíamos promocionar, vamos a poder incorporarla y promocionar una modalidad de alojamiento que es muy demandada, fundamentalmente en el sector empresario y los jóvenes”.

El listado con el registro de inmuebles estará disponible en la web de la Municipalidad de Santa Fe, en QR de Información Turística y Centros de Informes.

**Más beneficios**

Otro de los puntos que destacó Franco Arone es que “brinda seguridad al anfitrión, que es el dueño del inmueble, y al huésped, porque la ordenanza prevé que se pidan una serie de requisitos como la verificación de instalaciones de gas, electricidad y otros servicios que son básicos para garantizar la seguridad”.

También destacó como beneficios que “promueve el trabajo decente y genera una sana y leal competencia frente a otras modalidades de alojamiento. Actualmente, al no estar regulado, es una competencia desleal frente a los hoteles que tienen que hacer una serie de tributaciones que no hacen las casas y departamentos de alquiler turístico temporario”.

Quienes se sumen a un inmueble al registro, desde el municipio se promocionará dentro de la oferta turística de la ciudad en Centros de Informe y sitio web, además de ofrecer asesoramiento legal para la promoción del inmueble. El registro se inscribe en la Ordenanza N° 12.731, a partir de un trabajo coordinado con la Asociación Empresaria Hotelera Gastronómica de Santa Fe.

**Cómo es el trámite**

La inscripción al registro tiene vigencia por un año. El trámite puede ser realizado por propietarios, usufructuarios, apoderados, administradores, locadores, corredores, martilleros, intermediarios, mandatarios, simples tenedores y/o cesionarios que intervengan en alguna forma en la comercialización, promoción, contratación, administración, explotación, gestión o intermediación de la oferta de inmuebles en la modalidad locativa anteriormente definida.

Como requisito, se debe presentar en términos generales:

* Escritura del inmueble autenticada que demuestre la titularidad del mismo o el título del que surja el derecho que permita la autorización para su explotación.
* Constancia del Sistema Registral de AFIP en cumplimiento de la Resolución 3687/2014. Actividad AFIP: “Servicios de hospedaje temporal n.c.p”
* Certificado de Sistema Eléctrico
* Póliza de seguro por responsabilidad civil por daños contra terceros.
* Declaración jurada en la que conste el cumplimiento de las disposiciones sobre Seguridad e Higiene vigentes en la Ciudad. (Ver botón Descargas/Enlaces)
* Fotos del inmueble a alquilar

Una vez finalizado el trámite, el municipio otorga un número de registro a cada inmueble, el cual debe estar visible en las plataformas de intermediación para su comercialización. “Así nos aseguramos que se está ofreciendo un alojamiento que está debidamente habilitado, que tiene normas de seguridad, que asegura la protección de la minoridad para evitar situaciones de trata, y una serie de cuestiones que da la garantía de que es un alojamiento cuidado”, concluyó Arone.