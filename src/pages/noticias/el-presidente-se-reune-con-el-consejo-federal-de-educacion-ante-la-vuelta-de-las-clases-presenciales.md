---
category: Agenda Ciudadana
date: 2021-02-12T08:00:38-03:00
thumbnail: https://assets.3dnoticias.com.ar/presidente.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: El presidente se reúne con el Consejo Federal de Educación, ante la vuelta
  de las clases presenciales
title: El presidente se reúne con el Consejo Federal de Educación, ante la vuelta
  de las clases presenciales
entradilla: La asamblea, de la que también participará el ministro de Educación de
  la Nación, Nicolás Trotta, está agendada para este viernes a las 11.30 en la residencia
  de Olivos.

---
El presidente Alberto Fernández encabezará este viernes una reunión del Consejo Federal de Educación (CFE), con la participación de los ministros del área de las 24 jurisdicciones del país, para analizar cómo será la vuelta a clases en las escuelas y los detalles de los protocolos que regirán en la pandemia de coronavirus.

El encuentro, del que también participará el ministro de Educación de la Nación, Nicolás Trotta, está agendado para las 11.30 en la residencia de Olivos, informaron fuentes oficiales.

La reunión se hará tras la recorrida que efectuó Trotta por todo el país para ultimar detalles sobre la vuelta a las clases presenciales bajo el concepto de "presencialidad cuidada", en la cual se propone implementar un sistema híbrido con todos los protocolos enunciados el 2 de julio del 2020.

En el marco del Consejo se tratarán las pautas generales de vuelta a las aulas, aunque serán las jurisdicciones las que decidirán las formas en su territorio. La idea es avanzar en primer término con aquellos alumnos de los últimos años del nivel primario y del secundario, dividiendo los cursos en un máximo de 15 alumnos.

El calendario prevé la presencialidad como regla con un piso de 180 días, adecuando los grupos de estudiantes y las instituciones educativas a los protocolos sanitarios aprobados el 2 de julio que, actualmente, se aplican en más de 11 jurisdicciones.