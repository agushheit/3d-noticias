---
category: Agenda Ciudadana
date: 2021-05-03T06:15:55-03:00
thumbnail: https://assets.3dnoticias.com.ar/vacuna-sputnik-v.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Llegan a Santa Fe 50.400 vacunas Sputnik V
title: Llegan a Santa Fe 50.400 vacunas Sputnik V
entradilla: Más de 7.000.000 de personas ya recibieron la primera dosis. Comienza
  este domingo la distribución de 650.400 dosis del componente 1 de la Sputnik V en
  todas las provincias.

---
Entre el domingo y el martes se llevará a cabo la distribución, en todas las jurisdicciones del país, de 650.400 dosis del componente 1 de la vacuna Sputnik V con la finalidad de dar continuidad al plan estratégico que desplegó el Gobierno nacional para combatir el Covid-19. Con esta entrega, se superan las 10.000.000 de dosis distribuidas en todo el país.

Estas vacunas producidas por el Centro Nacional de Investigación de Epidemiología y Microbiología Gamaleya de Moscú, forman parte de la partida de un total de 765.545 dosis que arribaron al país el viernes último por la tarde en un vuelo de Aerolíneas Argentinas.

De acuerdo al criterio de distribución dispuesto por el Ministerio de Salud de la Nación, en base a la cantidad de población de cada distrito, este domingo la provincia de Buenos Aires recibirá 100.200 dosis; la Ciudad Autónoma de Buenos Aires, 43.800; Santa Fe, 50.400; y Neuquén, 9.600.

Mañana llegarán a la provincia de Buenos Aires otras 151.200 vacunas; a Catamarca, 6.000; a Chaco, 17.400; a Chubut, 9.000; a Córdoba, 54.000; a Corrientes, 16.200 ; a Entre Ríos, 19.800; a Jujuy, 10.800; a La Pampa, 5.400; a La Rioja, 5.400; a Formosa, 8.400; a Mendoza, 29.400; a Misiones, 18.000; a Río Negro, 10.800; a Salta, 20.400; a San Juan, 11.400; a San Luis, 7.200; Santiago del Estero, 13.800; a Tierra del Fuego, 2.400; y a Tucumán, 24.000. Finalmente, el martes Santa Cruz recibirá 5.400.

Entre el domingo y el viernes de la semana pasada, el país recibió casi 2.800.000 dosis mediante cinco vuelos. Los cuatros primeros –tres de la línea de bandera nacional y uno de la alemana Lufthansa- llegaron los días domingo, lunes, miércoles y jueves provenientes de Pekín con 2 millones de Sinopharm y el restante fue el viernes, con más de 760 mil de la Sputnik V.

Según el Monitor Público de Vacunación, el registro online que muestra en tiempo real el operativo de inmunización en todo el territorio, hasta esta mañana habían sido distribuidas 9.870.770 vacunas y las aplicadas totalizan 7.997.902: 7.025.492 personas recibieron la primera dosis y 972.410 ambas.