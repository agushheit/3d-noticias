---
category: La Ciudad
date: 2021-11-12T06:00:52-03:00
thumbnail: https://assets.3dnoticias.com.ar/IAPOS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: El Iapos se muda al renovado edificio de calle Rivadavia y la atención se
  verá afectada
title: El Iapos se muda al renovado edificio de calle Rivadavia y la atención se verá
  afectada
entradilla: 'Pasaron casi cuatro años desde que el instituto abandonó dicho inmueble.
  El miércoles 17 será el último día de atención en las oficinas del puerto.

'

---
Con motivo del traslado definitivo de las oficinas del IAPOS a su remodelado edificio en la ciudad de Santa Fe, la obra social informó que los días 18 y 19 de noviembre de 2021 no habrá atención al público en la sede del Puerto, Francisco Miguens 200 de esta capital.

Desde el Instituto informaron que la atención habitual se retomará a partir del martes 23 de noviembre, en Rivadavia 3452.

En tanto, los trámites de afiliaciones continuarán realizándose con normalidad en la sede de San Martín 3145 de la ciudad Capital.

El regreso se produce casi cuatro años después. El renovado edificio posee siete pisos; la planta baja esta destinada a la atención al público. Luego, hasta el sexto, son todas áreas administrativas. El séptimo cuento con salas de capacitación y reuniones. "Son todas áreas abiertas, excepto en alguna que tenga que ver con alguna documentación que resguardar, todas las áreas son totalmente compartidas, abiertas con mayor contacto entre las personas. Es un concepto distinto", había anticipado meses atrás a UNO el director de Iapos, Oscar Broggi

El Iapos informó que en la página web www.iapossantafe.gob.ar, se encuentran detallados los trámites que se pueden realizar de manera digital.

En ese sentido, rcuerdan que la obra social cuenta con distintos canales para la adquisición de ordenes de consulta y bonos asistenciales, como también diversos centros de atención en la ciudad de Santa Fe, los cuales pueden consultarse en dicho sitia web .

Por consultas, los afilliados podrán comunicarse al 0800 444 4276, los días hábiles de 8 a 16 horas., o por correo electrónico a iapos_consultas @santafe.gov.ar.