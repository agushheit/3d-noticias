---
layout: Noticia con imagen
author: 3DNoticias
resumen: Reactivación Productiva
category: El Campo
title: Se exportaron más de 50.000 toneladas y se concretaron 20 embarques desde
  el puerto de Santa Fe
entradilla: Este martes se concretó el embarque número 20, consolidando la
  reactivación productiva de la terminal portuaria ubicada en la capital
  provincial.
date: 2020-11-25T12:25:04.093Z
thumbnail: https://assets.3dnoticias.com.ar/exportacion.jpeg
---
Este martes se concretó el embarque número 20 de maíz quebrado realizado por un buque en el Puerto de Santa Fe, superando así las 50.000 toneladas exportadas sólo por agua. Las embarcaciones que llegan al puerto para este tipo de operaciones son buques de aproximadamente 100 metros de eslora y cargan en cada viaje el equivalente a unos 80 camiones.

**Son embarques de maíz quebrado, un proceso que se realiza en la propia planta de la Terminal Agrograneles del puerto**, en la cual reciben los granos y se le agrega valor en la propia terminal portuaria. También se consolidan contenedores con granos que, por ahora, están saliendo por tierra pero que, en un futuro, no descartan que podrían salir por agua.

En tal sentido, el presidente del Ente Administrador Puerto Santa Fe, Carlos Arese, manifestó: “Hoy el Puerto de Santa Fe trabaja muy activamente. Recibimos los granos, los procesamos en la planta y salen por agua. Estamos muy conformes por el presente, pero también muy entusiasmados por el futuro inmediato, ya que estamos recibiendo granos de la campaña 20/21 y trabajando para que puedan salir por agua”.

"En los últimos 5 años había salido un sólo barco. Nosotros, en menos de un año, podemos hablar de 20 embarques a la fecha y tenemos proyectados muchos más. Esto es una gran noticia para la producción santafesina. Hemos encontrado la escala justa de los barcos que pueden entrar a nuestro puerto, con 100 metros de eslora, y que se adaptan a la situación actual de la bajante del río", concluyó Arese.

Por su parte, el secretario de Agroalimentos del Ministerio de la Producción, Ciencia y Tecnología, Jorge Torelli, que estuvo acompañando el embarque N.º 20 comentó: "Santa Fe no deja de sorprendernos con la puesta en valor y la producción del puerto de Santa Fe, en una etapa muy adversa por pandemia y por la bajante del río. Esta continuidad y cantidad de embarques significan un potencial muy grande para la producción de Santa Fe y la región".

### **PUERTO EN MOVIMIENTO**

El puerto recibe entre 30 y 50 camiones por día. Hoy los silos, que tienen una capacidad de 60 mil toneladas, están casi completos de granos. Almacenan sorgo, soja, maíz y el recientemente llegado trigo.

El 16 de junio pasado ingresó el primer buque al Puerto Santa Fe después de muchos años de parálisis productiva y, desde entonces, la actividad portuaria no se detuvo. Con una frecuencia semanal, afirman desde el puerto, han llegado a concretar hasta tres embarques en una misma semana.

### **EL DRAGADO**

En el mes de abril, el Ente Administrador Puerto Santa Fe tomó la oportuna decisión de realizar un dragado de profundización en el Canal de Acceso al puerto, motivado por la bajante extraordinaria del Río Paraná, retirando más de 50.000 metros cúbicos de sedimentos, a través de la operación de una empresa de dragado de nivel internacional.

Esa tarea se ve complementada diariamente con el dragado de la zona de maniobras, efectuada con la draga propia del Puerto Santa Fe. Esta decisión es, sin dudas, lo que permitió que en los meses sucesivos el puerto pueda operar con normalidad en un contexto de bajante generalizado. Sin esta importante inversión en materia portuaria hoy hubiera sido imposible realizar estos embarques, afirman desde el puerto.

### **PRESENTES**

Acompañaron la actividad, el subsecretario de Planificación Estratégica, David Priolo, y el secretario Privado Andrés Dentesano; ambos del Ministerio de Infraestructura, Servicios Públicos y Hábitat.