---
category: Agenda Ciudadana
date: 2021-08-26T06:15:42-03:00
thumbnail: https://assets.3dnoticias.com.ar/DENGUE.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Comienza la temporada de Dengue y especialistas piden extremar medidas
title: Comienza la temporada de Dengue y especialistas piden extremar medidas
entradilla: Entre 2020 y 2021 se registraron 3.276 casos vía laboratorio en el país.
  Aseguran que la menor circulación de personas colaboró en la baja de contagios de
  Dengue.

---
Las restricciones a la circulación por la pandemia de coronavirus lograron que, en la última temporada, Argentina tenga la menor cantidad de casos de dengue en la región. Sin embargo, en el marco del Día Internacional contra esta enfermedad, los especialistas reclamaron “solidaridad y vigilancia activa” de toda la ciudadanía para prevenirla.

El 26 de agosto se celebra el Día Internacional contra el Dengue con el objetivo de fomentar la reflexión y dar a conocer cómo prevenir esta enfermedad que se presenta, principalmente en países de clima tropical, y es trasmitida por mosquitos Aedes aegypt.

Entre mediados de 2020 y mediados de 2021, Argentina registró un 8,67 casos confirmados de dengue cada 100 mil habitantes, mientras que estas cifras en Brasil alcanzan los 390; en Paraguay 337 y en Bolivia 72,9, precisó la Dirección Nacional de Enfermedades Trasmisibles.

Hugo Feraud, titular del organismo, señaló en diálogo con Télam que “es una tarea de todos y todas, por más que los estados pongan énfasis en la prevención, es una cuestión de compromiso de la sociedad. Si mi patio tiene reservorios (donde pueda reproducirse el mosquito) voy a afectar a toda una manzana que hizo las cosas bien”.

La última temporada registró un “bajo número de casos, en comparación a las estadísticas históricas”, remarcó Feraud, y precisó que en total “confirmados por laboratorio hubo 3.276 casos, de los cuales un fue grave” y que “los serotipos circulantes fueron 1, 2 y 4”.

Las restricciones a la circulación impuestas por el Gobierno nacional para mitigar la propagación del coronavirus también contribuyeron a la baja de casos de dengue porque “evitaron que personas infectadas vayan de un lugar a otro” así como también que se reprodujeran los casos “importados”.

“De todos modos, el virus del dengue tiene un comportamiento cíclico que tiene que ver con los ciclos de lluvia y las temperaturas por eso hay años de menos casos versus años donde hay un incremento, depende de muchos factores”, apuntó Feraud en vísperas del comienzo de la temporada.

En términos de “apoyo en insumos y transferencias a las provincias” el Estado realizó “una inversión de 3.071 millones de pesos, además se realizaron controles en 109 mil viviendas, en 9.600 manzanas de 11 provincias”, apuntó el funcionario.

Feraud destacó la importancia de que la población reconozca “qué es un cacharro, qué puede servir para la reproducción del Aedes aegypti” y enfatizó en que “si bien la historia natural de la enfermedad en un principio la circunscribe al NOA (noroeste argentino) y NEA (noreste argentino); con el correr de los años se fue generalizando hacia la región central del país”.