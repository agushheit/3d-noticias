---
category: Agenda Ciudadana
date: 2021-02-06T07:24:14-03:00
thumbnail: https://assets.3dnoticias.com.ar/dni.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: 'Santa Fe Más Cerca: La provincia realiza tramites de DNI y pasaportes en
  distintos barrios de la ciudad capital'
title: 'Santa Fe Más Cerca: La provincia realiza tramites de DNI y pasaportes en distintos
  barrios de la ciudad capital'
entradilla: Es a través del Dispositivo Integrador Territorial de la Secretaria de
  Justicia. Se sumarán otras áreas con asesoramiento, charlas y capacitación.

---
La provincia, a través de la Secretaría de Justicia, dependiente del Ministerio de Gobierno, Justicia, Derechos Humanos y Diversidad, implementa el dispositivo “Santa Fe + Cerca” en los barrios de la ciudad capital, a través de trámites para obtener el Documento Nacional de Identidad (DNI) y pasaportes a los que los santafesinos y santafesinas acceden con turnos previamente solicitados.

Este viernes el dispositivo llegó a República del Oeste, donde el secretario de Justicia, Gabriel Somaglia, destacó los alcances del mismo: “Es un programa que busca acercar territorialmente a la comunidad los servicios que brinda la Secretaria. El objeto es llevarlos a los distintos barrios de la ciudad, sobre todo en aquellos donde se dificulta el acceso, sea por el transporte, la pandemia o el desconocimiento de trámites que se tienen que realizar”.

Asimismo, el secretario agregó: “Hay varios trámites que se pueden hacer vía web, pero entendemos que hay un gran número de personas que no tienen esa herramienta y es necesario que el Estado esté presente territorialmente. Tenemos la satisfacción de que en la primera semana, aproximadamente 250 personas se acercaron y se llevaron sus respectivos DNI”.

Cabe señalar que la atención es de 8 a 12.30 en las distintas sedes vecinales y se otorgan 65 turnos por día para mantener el distanciamiento social. La próxima semana continuará los días lunes 8, jueves 11 y viernes 12 de febrero en la Vecinal Yapeyú Oeste, ubicada en Teniente Loza y Dr. Paredes, con el Registro Civil, el Centro de Asistencia Judicial y la Inspección General de Personas Jurídicas (IGPJ).

Por su parte, el director del Centro Asistencial, Claudio Ainbinder, explicó: “El Centro atiende víctimas y familiares de delitos, también hay consultas jurídicas y judiciales de todo tipo. Como nos pidió el gobernador Perotti, redoblamos el esfuerzo y el trabajo para estar cerca de la gente, de quienes más lo necesitan. Entendemos a la Justicia como un derecho humano fundamental”.

**Trabajo conjunto**

El próximo jueves, a las 11, la Inspección General de Personas Jurídicas brindará una capacitación a vecinales, ONGs y asociaciones, junto con abogados y contadores, para asesorar sobre balances y trámites online.

Los últimos días de la semana se llamará “Viernes Joven” y el operativo a través del Registro Civil dará prioridad a los menores de 14 años que tengan que renovar el DNI.

Se suman también el Registro Único de Aspirantes a Guarda con fines Adoptivos, Registro de la Propiedad, Subsecretaria de Acceso a la Justicia y la Secretaria de Género.