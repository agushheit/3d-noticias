---
category: La Ciudad
date: 2021-01-09T12:09:41Z
thumbnail: https://assets.3dnoticias.com.ar/090121-piletones.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: '3DNoticias - Fuente: El Litoral'
resumen: El municipio dispondrá de 200 agentes para controlar las playas y los piletones
title: El municipio dispondrá de 200 agentes para controlar las playas y los piletones
entradilla: 'Serán controles fijos en la Costanera Este y los parques Garay y del
  Sur, e itinerantes a lo largo de toda la Costanera Oeste. Se sumarán patrullajes
  dinámicos y los policías caminantes. '

---
**Para evitar la aglomeración de personas, la Municipalidad de Santa Fe concretará este fin de semana un operativo especial en playas y parques de la ciudad.** 

Tal como había anticipado el intendente Emilio Jatón, las tareas serán concretadas por más de 200 personas, entre las que se cuentan agentes sanitarios, integrantes del Cobem y la GSI, inspectores de tránsito, profesores de educación física, guardavidas e integrantes de la Policía de la provincia.

A ellos se sumarán guías de turismo de la Agencia para el Desarrollo de Santa Fe y su Región (ADER) que se ubicarán en tres postas situadas en la Costanera Este, la Costanera Oeste y el kilómetro 0,5 de la Ruta Provincial 1. Los mismos estarán acompañados por integrantes de la Agencia Provincial de Seguridad Vial y completarán tareas de concientización entre quienes circulen por esos lugares.

Los operativos se montarán de forma permanente en el Parque del Sur, el Parque Garay y la Costanera Este, y de modo itinerante a lo largo de toda la Costanera Oeste de la capital, el viernes, sábado y domingo de 14 a 19 horas. **En todos estos espacios se fijarán postas de cuidados que realizarán test de anosmia, toma de temperatura y difusión de medidas de seguridad entre la población.**

En Costanera Este se apostarán dos controles para el ordenamiento del tránsito: uno en el ingreso al paseo y otro a la altura de la rotonda, en el inicio de la avenida Néstor Kirchner. Allí se realizarán controles de olfato y temperatura, igual que a quienes ingresen a pie. Del mismo modo, habrá recorridas permanentes por el área de playa efectuadas por efectivos policiales caminantes a los que se sumarán actores, mimos y payasos que recordarán las medidas de prevención generando conciencia a través del arte. La intención es completar tareas disuasivas entre los presentes y evitar las aglomeraciones.

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.15em; font-weight: 700">Mayor control </span>**

El secretario de Integración y Economía Social de la Municipalidad, Mariano Granato, señaló que «en virtud de la situación actual de la pandemia, en términos de cantidad de contagio y principalmente el relajamiento de los y las ciudadanas en todo el país, la Municipalidad ha definido fortalecer las medidas de control y de acompañamiento a los vecinos y las vecinas de la capital provincial».

En ese sentido, informó que «este fin de semana se van a duplicar los esfuerzos del Estado local en los puntos considerados críticos, es decir, donde se aglomera más gente, que principalmente son las zonas de las Costaneras y los parques balnearios». 

El funcionario recordó que, durante la semana, se mantuvieron varias reuniones con el gobierno provincial y otros actores para analizar la situación sanitaria y el comportamiento de la comunidad a fin de dar un abordaje conjunto a la problemática.

Granato indicó que «en esos espacios se concretó una inversión pública muy fuerte para embellecerlos y generar condiciones de disfrute de la ciudadanía, pero es momento de fortalecer las medidas de cuidado, ya que la cantidad de casos de Covid-19 lejos de decrecer, sigue en aumento».

Por este motivo, el secretario afirmó que las tareas se intensificarán este viernes a partir de las 14 horas, extendiéndose del sábado y el domingo. 

Según explicó, los operativos se realizan desde el inicio de la temporada con presencia del Estado municipal, pero en esta oportunidad se van a duplicar. «La consigna es poner a disposición todas las herramientas con que cuenta el municipio, pero apelando especialmente a la responsabilidad social, entendiendo que, si no hay un compromiso de la ciudadanía para atravesar esta crisis, ningún esfuerzo será suficiente», insistió. 

De igual modo, remarcó la importancia de «fortalecer el mensaje de que la pandemia no ha terminado, el virus está entre nosotros y **tenemos que aprender nuevas formas de convivir para disfrutar del espacio público, pero cuidándonos**», concluyó el secretario de Integración y Economía Social.