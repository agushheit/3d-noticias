---
category: La Ciudad
date: 2021-03-06T07:00:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/CULTURA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Agenda cultural para disfrutar el fin de semana en la ciudad
title: Agenda cultural para disfrutar el fin de semana en la ciudad
entradilla: Marzo arrancó en la capital provincial con una agenda nutrida de actividades
  para toda la familia, organizadas por la Municipalidad con entrada libre y gratuita.

---
Habrá propuestas recreativas, culturales y deportivas para todas las edades. Entre otras cosas, se propone recorrer el Jardín Botánico, realizar actividades culturales o disfrutar de los recorridos de Mi Ciudad como Turista acompañados por audioguías disponibles en distintas plataformas virtuales.

**Expresiones culturales**

Durante el verano, la Municipalidad brinda actividades vinculadas a la danza, la música, la lectura y oficios en distintos puntos de Santa Fe capital para todas las edades. Las propuestas se dictan en las Estaciones de La Boca, Barranquitas, Coronel Dorrego y CIC de Facundo Zuviría, además del Mercado Progreso, la explanada de la Estación Belgrano y Candioti Park.

De esta manera, se descentraliza el acceso a actividades recreativas al aire libre con estricto protocolo sanitario, que incluye inscripción previa en cada actividad y el uso de burbujas para garantizar el distanciamiento social. Las inscripciones se realizan en los espacios donde se dictan los talleres.

En este sentido, Carnavales en los Barrios se despide en Cabaña Leiva. Será el viernes 5, de 19 a 21, en la plaza Amanda Lértora de Porta, ubicada en Juan Bautista La Salle y Edmundo Rosas. La Murga Libre Expresión se presenta como anfitriona de las comparsas Marivera, Herederos del Sol y Amanecer.

El Mercado Norte sale a la plaza, el viernes a partir de las 19, renovando la invitación para que se acerquen vecinas y vecinos de la Plaza Constituyentes. Los emprendimientos radicados en el Mercado salen al espacio público para compartir sus propuestas. También habrá opciones gastronómicas y espectáculos musicales con la participación del grupo Las Morsas, tributo a The Beatles; cantantes del movimiento Artistas Líricos de Santa Fe y Mutar Dúo.

En el Auditorio Federal del Museo de la Constitución (Av. Circunvalación y 1° de Mayo) comienza un ciclo de cine de directoras contemporáneas argentinas. El viernes 5, a las 19.30 se proyectará “Alanis”, de Anahí Berneri. La entrada es gratuita, con capacidad limitada de la sala, por lo que deberá reservarse previamente en la sección Pantalla Pública de la plataforma Capital Cultural.

El viernes 5, de 15.30 a 18.30 horas, habrá un nuevo encuentro de Vereda para Jugar en la Estación Loyola Sur (José Pedroni y Furlong), en las intervenciones lúdicas que la Municipalidad realizó en las veredas de la Estación y el Jardín de Loyola Sur, a partir de un proyecto conjunto con la Red de Instituciones del barrio.

Por otra parte, continúan las actividades programadas en el Centro Experimental del Color (Estación Belgrano, Bv. Gálvez 1150) para celebrar el 80 aniversario del poeta y ceramista Héctor Rolando “Kiwi” Rodríguez. El viernes 5, el Equipo Pedagógico de Museos invita a “Bogando estrellas”, un taller de escritura y cerámica para niñas y niños de 6 a 12 años. Los cupos son limitados a 10 participantes y para solicitar la inscripción se debe escribir un correo electrónico a kiwi.barroypalabras@santafeciudad.gov.ar.

**Ferias**

Como cada fin de semana se realizan distintas ferias de artesanos, emprendedores y recicladores de antigüedades en la ciudad. En Colastiné Norte, se llevará a cabo la Feria Renace y Emprende el sábado 6, de 16 a 21 horas, a la altura de la ruta 1 km 2.7 sobre colectora oeste, calle Duraznos entre Padre Mario Mendoza y Los Eucaliptus. Además de la tradicional Feria Del Sol y la Luna, el sábado 6 y domingo 7, de 16 a 21, en la Plaza Pueyrredón.

También se desarrollarán las siguientes ferias: Emprendedores del Sur funcionará el sábado 6 de 13 a 20 horas en Av. Illia y 9 de julio; Sueño del Norte el sábado 6 de 17.30 a 21, en Baigorrita 9300; Feria Parque Federal el domingo 7 de 17 a 21 en el Parque Federal; Feria Incuba el domingo 7 de 18 a 21 en Plaza Constituyentes; Paseo Costanera Oeste el domingo 7 de 16 a 21 en Almirante Brown al 6000; Feria del Boulevard el sábado 6 de 17 a 21 en Pedro Vittori y Bv. Gálvez; y Feria Mujeres Agroecológicas el sábado 6 de 8 a 13 en Iturraspe y 4 de enero.

También se pueden visitar la Feria Barrio Abierto de Las Flores, el sábado 6, de 18 a 21, en Millán Medina y Arenales; la Feria de Pie, el sábado 6 de 9 a 19, en la Plaza Fournier (Castelli y Gobernador Freyre); la Feria Murat, sábado 6 y domingo 7 de 17 a 22, en la Plaza Guadalupe; la Feria Yeah, el domingo 7 de 15 a 21, en Costanera en Av. 7 Jefes y Maipú; y Emprendedores de Santa Rosa, domingo 7 de 18 a 21, en Padre Quiroga 2274 (Plaza Arenales).

**Paseos por el Botánico**

Los santafesinos y visitantes podrán recorrer el Jardín Botánico “Ing. Lorenzo Parodi”, ubicado en avenida Gorriti al 3900. Allí, con turnos previos, se pueden hacer dos circuitos educativos.

Uno de ellos es un recorrido para realizar un avistaje de aves, reconocimiento de la flora, registro de especies y ciencia ciudadana. Esta opción se puede hacer los miércoles y viernes, de 8 a 12, y los sábados, de 8 a 12 y de 18 a 20 horas.

La segunda alternativa se denomina “Vivero Abierto” y consiste en conocer el trabajo de los viveros municipales. Los trabajadores explicarán la tarea que desempeñan y cada uno de los procesos de la producción. Como cierre podrán hacer un plantín y se llevarán uno como regalo. Además, si el visitante lo desea, podrá también recorrer el lugar y disfrutar de la flora y la fauna que lo cobija. Esta opción se podrá hacer los miércoles y viernes, de 8 a 11.

Ambas alternativas son con turno previo que se deberá sacar enviando un mail a: jardinbotanicosantafe@gmail.com.

**Mi Ciudad como Turista**

Mi ciudad como turista es una propuesta que ofrece, por un lado, visitas guiadas gratuitas a santafesinos y visitantes con modalidad free tour y; por el otro, recorridos autoguiados que se pueden llevar a cabo sin la necesidad de asistir a un horario específico.

En cualquiera de sus dos modalidades se podrá conocer puntos emblemáticos de la ciudad a través del relato vivencial de quienes los transcurren diariamente, con distancias cortas (entre los 800 a 1500 metros). Para los que quieran participar de la experiencia este sábado 6 será a las 18.30 y el paseo será por el Puerto. El punto de encuentro es el Palomar (Rivadavia y La Rioja).

Para escuchar y/o descargar las audioguías se puede ingresar a la página web de la ciudad, o también se pueden escuchar desde el perfil de Santa Fe Capital en Spotify. Vale destacar que en ambas plataformas se encuentran disponibles los recorridos de Paseo del Puerto, Paseo Costanera, Paseo Peatonal San Martín, Manzana Jesuítica, Paseo Boulevard, Casco Histórico, entre otros.

Las audioguías son un desarrollo de la Municipalidad orientado a generar herramientas de autogestión útiles para la ciudadanía y los visitantes que permitan reforzar el compromiso como municipio sustentable, generando políticas amigables con el ambiente.

**Manzana Jesuítica**

Por otra parte, se pueden realizar las visitas regulares a la Manzana Jesuítica, que cuenta con la modalidad autoguiada, de miércoles a sábados, de 9 a 12 y de 16.30 a 19.30; y con guía los mismos días, a las 10 y a las 18 horas. Este recorrido comienza en la puerta principal de ingreso del tradicional colegio Inmaculada Concepción, en San Martín 1540.

Con esta visita se intenta recuperar la tarea evangelizadora, educativa y científica de la Compañía de Jesús, una de las órdenes católicas más antiguas afincadas en la capital provincial, y también da cuenta del paso de destacados personajes nacionales e internacionales por sus instalaciones como el Papa Francisco y Jorge Luis Borges, entre otros. La visita estará a cargo de personal municipal especializado que, como siempre, estará identificado con las sombrillas verdes.

**Barrio Abierto**

El trabajo de la Municipalidad junto a las Redes de Instituciones Barriales continúa el sábado 6, desde las 19, en barrio las Flores. En la plaza ubicada en Arenales y Millán Medina se podrá disfrutar de una feria de emprendedoras y emprendedores del barrio y presentaciones a cargo de artistas invitados. Las niñas y niños van a poder disfrutar del show de Califleto (Guille Chemes).

A tono con un mes lleno de propuestas ligadas al carnaval, también se presentará la Comparsa Los Herederos del Sol; y habrá una clase abierta de zumba, a cargo de la Vecinal Villa Las Flores. El cierre será a pura música con la Murga de estilo uruguayo Rezonga La Ronca, que presentará cuplés de su último espectáculo, “Una clase de murga”. La encargada de ponerle el broche final a la jornada será la cantante de cumbia Evelyn Maidana junto a su grupo.

**Capital Cultural**

La plataforma educativa y cultural Capital Cultural brinda cada semana nuevos contenidos para disfrutar desde casa. El sábado 6, a las 19 horas, se podrá ver el quinto capítulo de la serie que recupera los 60 años del Taller de La Guardia. A través de las voces de sus docentes, se da a conocer más profundamente la etapa final de la elaboración de las piezas cerámicas: la quema. Las distintas técnicas para cocinar el barro y las instancias que atraviesa el proceso que lo transforma.

Además, la tradicional ceremonia de quema a fuego abierto que reúne a estudiantes, docentes y familias del Taller La Guardia y todo lo que implica esa actividad.

**Artesanías en la Plaza Pueyrredón**

Como cada fin de semana, el sábado 6 y el domingo 7 de marzo, desde las 17 horas, artesanos y las artesanas que participan de la Feria del Sol y la Luna se reúnen en la Plaza Pueyrredón (Bv. Gálvez 1600). Con 40 años de tradición artesanal, la feria es Patrimonio Cultural y Artístico de la capital santafesina.

**Deportes al aire libre**

Continúan las distintas propuestas de deporte en diferentes puntos de la ciudad. Con acceso libre y gratuito, habrá alternativas de actividades al aire libre en las sedes y horarios que se detallan a continuación. En el Espigón II, este viernes se podrá practicar beach vóley de 19 a 20; beach fútbol de 17 a 18; también funcional de 18 a 19; y juegos recreativos de 17 a 18. Mientras que en el Parque del Sur habrá este viernes beach vóley e iniciación deportiva, ambas actividades de 17 a 20.

Por otro lado, en el Parador Este se realizará el viernes beach handball, de 17 a 18; beach vóley, de 17 a 20; beach fútbol de viernes, de 17 a 20; funcional, de 17 a 18; ritmos, de 18 a 19; y caminatas recreativas, de 18 a 19 horas.

En la sede de Deportes, ubicada en Avenida Almirante Brown 5294, se podrá realizar funcional de 18 a 19; beach vóley, de 17 a 20; beach tenis y beach fútbol, también de 17 a 20; juegos recreativos, de 18 a 20; sóftbol, de 17 a 20; vóley, de 18 a 20; handball; y tenis de 17 a 20 horas. Y, en la costa, en la sede La Guardia habrá este viernes ritmos y gimnasia para adultos, de 9 a 11 horas.

En la sede del Centro Gallego, ubicado en Avenida Galicia 1357, habrá este viernes yoga de 8 a 9 y de 9 a 10; y funcional, de 10 a 11. Por la tarde, además de funcional, se podrá realizar newcom, de 19 a 20; y minivóley, de 17 a 19 horas.

En el Polideportivo La Tablada, se desarrollará este viernes la actividad de funcional de 8 a 9 y de 15 a 16; zumba de 16 a 17; tae-kwondo de 17 a 18; Boxeo de 18 a 20; y ritmos de 20 a 21 horas. En Alto Verde, en el Polideportivo ubicado en Manzana 6, este viernes habrá básquetbol; fútsal de 9 a 10; vóley playa de 10 a 11; natación menores nivel 3 de 11 a 12; natación menores nivel 2 de 10 a 11; natación menores nivel 1 de 11 a 12; natación para niños (N 1) de 9 a 10; hockey de 9 a 10; natación adultos mayores de 10.30 a 11.30; y las actividades para adultos mayores de 9.30 a 10.30 horas.

Por la tarde las propuestas este viernes son las siguientes: Sipalki- Do de 15.30 a 16.30; fútbol femenino y masculino de 18 a 19; actividades primera infancia de 16 a 17; natación menores nivel 3 de 17 a 18; natación menores nivel 2 de 16 a 17; natación menores nivel 1 de 18 a 19; nataciones menores (N 1) de 17 a 18; y aquagym de 18 a 19 horas.

En la sede de Deportes, el viernes se puede realizar aerolocal en el horario de 8 a 9; de 9 a 10 hay ritmos y de 10 a 11 funcional. También, de 9.30 a 10.30 se desarrollará la clase de Taichi. Por la tarde, de 19 a 20 y de 20 a 21 hay ritmos.

Para finalizar, también hay actividades en la Estación Belgrano. Este viernes se puede realizar funcional de 8 a 9 y ritmos de 19.30 a 20.30. Por otra parte, a las 10 y a las 18 se llevarán a cabo clases de yoga.