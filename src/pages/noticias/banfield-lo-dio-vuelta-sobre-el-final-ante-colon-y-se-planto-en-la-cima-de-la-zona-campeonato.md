---
category: Deportes
date: 2020-12-29T16:16:09Z
thumbnail: https://assets.3dnoticias.com.ar/2912-banfield-colon.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Por Marisa Lemos
resumen: Banfield se plantó en la cima de la zona Campeonato
title: Banfield lo dio vuelta sobre el final ante Colón y se plantó en la cima de
  la zona Campeonato
entradilla: El Sabalero abrió el marcador con un golazo del Pulga Rodríguez, pero
  Banfield se impuso sobre el cierre 2-1 con los goles de Galoppo y Asenjo, para liderar
  el grupo B de la Copa Maradona.

---
Durante el transcurso de la noche, Banfield dio vuelta el partido que perdía en el primer tiempo y le ganó a Colón en Santa Fe por 2 a 1 para mantenerse con puntaje ideal al cabo de tres jornadas en el Grupo B, que sigue siendo el mejor equipo de la fase Campeón.

Fue un encuentro intenso, y de ida y vuelta, donde el local tenía la obligación de quedarse con los tres puntos y el equipo visitante peleaba para mantenerse invicto en dicha zona.

La velada mostró dos defensas sólidas y bien estructuradas que dejaban todo a la hora de defender y atacar el arco. A los 35 minutos del primer tiempo, el equipo sabalero abrió el marcador con un golazo del ídolo del equipo Luís Miguel «la pulga» Rodríguez.

A la vuelta del segundo tiempo, ambos equipos estaban expectantes y con ganas de seguir sumando goles. En los primeros instantes del ST llegó un cabezazo del volante Giuliano Galoppo para abrir el marcador del lado visitante.

La media hora restante el juego se tornó picante de ambos lados, ya que no se conformaban con un simple empate.

Superada la hora pautada, llegó el tanto de Banfield que nació desde un tiro libre, cerrándose con un cabezazo impecable de Maurico Asenjo, que no solamente le dio la victoria al taladro, sino que también lo posicionó en la cima del grupo B, manteniendo viva la esperanza de una final por el título.

<br/>

***

<span style="font-family: Helvetica, sans-serif; font-size: 1.1em; font-weight: bold">LOS EQUIPOS FORMARON DE LA SIGUIENTE MANERA:</span>

<br/>

**🔴⚫ Colón:** Leonardo Burián; Alex Vigo, Bruno Bianchi, Rafael Delgado y Gonzalo Escobar; Rodrigo Aliendro, Federico Lértora, Yeiler Goez y Luis Miguel Rodríguez; Wilson Morelo y Facundo Farías. **DT:** Eduardo Domínguez.

<br/>

🟢⚪ **Banfield:** Mauricio Arboleda; Emanuel Coronel, Alexis Maldonado, Luciano Lollo y Claudio Bravo; Martín Payero, Jorge Rodríguez y Giuliano Galoppo; Mauricio Cuero, Agustín Fontana y Fabián Bordagaray. **DT:** Javier Sanguinetti.

***