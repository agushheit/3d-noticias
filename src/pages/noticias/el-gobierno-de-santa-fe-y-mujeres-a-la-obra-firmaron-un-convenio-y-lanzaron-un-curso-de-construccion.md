---
category: Estado Real
date: 2021-01-23T08:49:25Z
thumbnail: https://assets.3dnoticias.com.ar/genero.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de la Provincia de Santa Fe
resumen: 'El Gobierno de Santa Fe y Mujeres a la Obra firmaron un convenio y  lanzaron
  un curso de construcción '
title: 'El Gobierno de Santa Fe y Mujeres a la Obra firmaron un convenio y  lanzaron
  un curso de construcción '
entradilla: A través de la Secretaría de Estado de Igualdad y Género brindarán  talleres
  de formación de mujeres y personas de la diversidad sexual.

---
El Gobierno de Santa Fe, a través de la Secretaría de Estado de Igualdad y Género, junto a la Asociación Civil Mujeres a la Obra, se aliaron en la tarea de capacitar y formar mujeres y personas de la diversidad sexual en el rubro de la construcción y afines. Este viernes se presentó el curso de formación en herramientas de construcción en seco en El Molino Fábrica Cultural, del que participaron unas 50 personas de forma presencial, cumpliendo con todos los protocolos exigidos en el marco de la pandemia COVID-19,  que serán formadoras en la temática, además de un grupo que participó mediante una transmisión en vivo en las redes sociales de la organización.

“Desde la gestión buscamos impulsar este tipo de iniciativas porque realizan un abordaje para aportar a la autonomía económica para mujeres y diversidades sexuales, brindando herramientas en un rubro en el que históricamente estas poblaciones no han tenido lugar”, sostuvo Florencia Marinaro, subsecretaria de Mujeres, Género y Diversidad de la provincia. 

“Esta articulación con Mujeres a la Obra va a permitir que estos saberes no solo lleguen a las personas que participan hoy del taller, si no que se multipliquen ya que la idea es que ellas mismas luego formen a otros y a otras en sus barrios, organizaciones, vecinales y otros espacios. Desde la gestión tomamos el compromiso de trabajar con las organizaciones como esta, que forman parte de la Mesa de Articulación Público Comunitaria, y que trabajan en los territorios”, agregó. 

"Además firmamos este convenio con la Asociación para coronar e impulsar este proceso de trabajo que venimos llevando en conjunto desde 2020 y que durará por lo menos dos años más, donde seguiremos apostando a la autonomía económica de mujeres y disidencias, derribando estereotipos y acompañando a las organizaciones de la comunidad", finalizó la funcionaria.

“Desde la Asociación Civil Mujeres a la Obra buscamos contribuir a establecer relaciones más igualitarias en los contextos y sectores laborales del rubro de la construcción, principalmente a través de la educación.

Por un lado, aportando herramientas, recursos e información que permitan a quienes participen ampliar las oportunidades de trabajo para conseguir mayor independencia y libertad; y por el otro, desmitificando aquellas representaciones sociales históricas que asignan determinados roles según el género”, sostuvo Ileana Rossi Seluy de la organización. 

“Estamos muy contentas por poder volver con nuestras capacitaciones, sabemos que muchas las están esperando hace tiempo. Creemos que este acuerdo con la Secretaría de Estado de Igualdad y Género va a potenciar este proyecto permitiendo que llegue a más personas a lo largo de toda la provincia. Estamos muy contentas, agradecidas, y convencidas de que el 2021 será un gran año”, finalizó.

El curso de formación en herramientas de construcción en seco, que ya tiene unas 85 personas inscriptas, es dado por las especialistas Andrea Dalmolin, Arquitecta (FADU-UNL, 2016) y Vanesa Alejandra Villetti Arquitecta (FADU-UNL, 2000).

Participaron de presentación además la concejala de Santa Fe, Jorgelina Mudallel; Milagros Baroni Bustos y parte del equipo de la Secretaría de Estado de Igualdad y Género y de la Asociación Civil Mujeres a la Obra.