---
category: Estado Real
date: 2021-01-06T10:21:07Z
thumbnail: https://assets.3dnoticias.com.ar/060121-veda-pesquera.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: La provincia apeló la veda pesquera dispuesta por la justicia
title: La provincia apeló la veda pesquera dispuesta por la justicia
entradilla: La evaluación por parte del gobierno provincial coincide con el cuidado
  del recurso, pero encuentra incongruencias al respecto y observa consecuencias innecesarias.

---
Ante la medida cautelar que establece **veda total para la pesca tanto deportiva como comercial en aguas del Río Paraná**, y sujeto a la reciente notificación y a los plazos de la Justicia en la instancia de Feria que comenzó recientemente, el gobierno de Santa Fe, a través de Fiscalía de Estado, presentó hoy los recursos procesales pertinentes.

La concesión del recurso de apelación implica además la suspensión del fallo hasta tanto se expida la Cámara. Por otro lado, **a lo largo de la semana se completará la presentación formal por parte del gobierno de Santa Fe, que busca unificar un proceso regional de veda parcial para la pesca comercial y la habilitación de la pesca deportiva con devolución**.

La medida establece la veda total sin evaluar consecuencias sociales y económicas que el Juez no consideró siquiera tangencialmente en su resolución. 

Asimismo, carece de sentido la veda total de la pesca deportiva, puesto que **a los fines de la conservación del recurso era suficiente disponer la obligatoriedad de la devolución como lo indica la ley**. Así no se afecta la fauna ictícola y no se perjudica al turismo de cercanía, que ya tuvo serias restricciones como consecuencia de la pandemia por el COVID-19, y que fomentado desde el gobierno provincial proyecta una temporada en crecimiento con muchas reservas ya realizadas.

Por otra parte, la medida establece veda «en aguas del río Paraná», lo que debería entenderse como limitada al cauce principal de dicho río, quedando fuera de la zona vedada arroyos, lagunas y otros ríos de la cuenca del Paraná.

A los fines de la protección de los recursos pesqueros, tratándose el río Paraná de un recurso compartido con otras provincias, resulta incongruente que estas puedan seguir pescando sin las restricciones impuestas por la sentencia en análisis, lo que además implica un trato desigual violatorio del principio de igualdad.

Por último, y a los fines de llevar tranquilidad, el gobierno de Santa Fe se encuentra trabajando en este proceso de respuesta a la Justicia y a la vez evaluando las posibles medidas a implementar a fin de sostener el desarrollo de este sector productivo.

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.1em; font-weight: 700">Presencia de funcionarios junto a las y los trabajadores</span>**

La ministra de Ambiente y Cambio Climático, Erika Gonnet, se reunió con las familias que se manifestaban en la cabecera del Puente Rosario-Victoria, a quienes les detalló la postura del gobierno de Santa Fe y la apelación presentada.

Esto, a los fines de llevar tranquilidad y la decisión del gobierno de agotar los recursos procesales pertinentes, sumado a la evaluación de las posibles medidas a implementar a fin de sostener el desarrollo de este sector productivo.

«El gobierno provincial coincide con el cuidado del recurso ictícola dispuesto por la medida cautelar, algo en lo que veníamos trabajando dándole participación al Consejo Provincial Pesquero; pero observamos que el fallo tiene incongruencias respecto a ese cuidado; sumado a que no se realizó un enfoque social, económico y productivo», resumió la ministra.

Junto a Gonnet, se sumó al encuentro con las y los pescadores el secretario de Políticas de Inclusión y Desarrollo Territorial del Ministerio de Desarrollo Social, Fernando Mazziotta.

En los cortes llevados a cabo en ciudad de Santa Fe, estuvo presente el secretario de Prácticas Socio Comunitarias, Ignacio Martínez Kerz, en representación del Ministerio de Desarrollo Social. En la oportunidad, dialogó con los pescadores.