---
layout: Noticia con imagen
author: 3DNoticias
resumen: Santa Fe adhiere al duelo
category: Agenda Ciudadana
title: Santa Fe adhiere al duelo nacional por el fallecimiento de Diego Maradona
entradilla: "El gobierno de la provincia de Santa Fe adhirió, a través del
  decreto N° 1496, al duelo de 72 horas por el fallecimiento de Diego Armando
  Maradona. "
date: 2020-11-26T13:37:20.232Z
thumbnail: https://assets.3dnoticias.com.ar/velorio-diego.jpeg
---
**En los fundamentos se hace un reconocimiento de la trayectoria deportiva del mejor futbolista del siglo XX**.

El gobierno de la provincia de Santa Fe adhirió, a través del decreto N° 1496, al duelo de 72 horas por el fallecimiento de Diego Armando Maradona, que fue dispuesto por el Poder Ejecutivo Nacional en todo el ámbito de la República Argentina.

El decreto destaca la trayectoria deportiva de Maradona, tanto en equipos nacionales como internacionales; así como en la Selección Argentina, obteniendo los títulos de Campeón Mundial Juvenil en 1979, Campeón Mundial en 1986 y Subcampeón Mundial en 1990 con la Selección Argentina.

Entre los fundamentos se señala que el futbolista fallecido, supo despertar enormes alegrías y sentimientos colectivos que lo hicieron acreedor del reconocimiento y gratitud de los argentinos. El merecido homenaje al más grande futbolista de la historia.

[Decreto](https://www.santafe.gov.ar/noticias/recursos/documentos/2020/11/2020-11-25NID_269218O_1.pdf)