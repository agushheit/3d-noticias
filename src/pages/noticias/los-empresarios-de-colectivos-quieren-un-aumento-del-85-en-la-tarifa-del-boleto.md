---
category: La Ciudad
date: 2021-12-17T06:00:50-03:00
thumbnail: https://assets.3dnoticias.com.ar/BONDI.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Los empresarios de colectivos quieren un aumento del 85% en la tarifa del
  boleto
title: Los empresarios de colectivos quieren un aumento del 85% en la tarifa del boleto
entradilla: 'La semana pasada los titulares de las empresas que brindan el servicio
  de transporte de pasajeros ingresaron el pedido a la municipalidad. El último aumento
  fue en febrero de 2021

'

---
Los titulares de las empresas de colectivos que brindan servicio en la ciudad de Santa Fe, Autobuses Santa Fe, Ersa Urbano y Empresa Recreo, ingresaron a la municipalidad un pedido de aumento de tarifa, desde enero 2022, con una suba que alcanza el 85%. La solicitud se hace en consonancia con lo dispuesto por el Decreto Nº 725/2019 y por la ordenanza Nº 12.774 que dispuso la Emergencia en el Transporte y que está a punto de vencer. Sobre esto último además agregaron que luego de sus 180 días de implementación, no hubo avances en las reuniones de la mesa creada para que las partes puedan tratar el conflicto y buscar soluciones. El último incremento del boleto fue en febrero de este año.

De esta manera, el costo del pasaje pasaría de $42,35 a $78,36, es decir un 85% a partir de enero 2022. El precio del mismo resulta de la base del promedio de gastos que tienen las empresas, entre lo que se destaca el aumento de los choferes que se obtuvo en paritarias y el costo del gasoil, que es el principal insumo para prestar el servicio además de tener problemas para conseguir repuestos como por ejemplo los neumáticos.

Por otro lado también hicieron referencia a que en cuanto a la partida de asistencia al transporte por parte de Nación, para este 2022 está previsto un recorte de $1.000 millones para el servicio del interior del país. Todo esto confluye en el pedido de aumento en los valores mencionados.

La misiva termina haciendo referencia a que si bien la decisión de no trasladar el costo total del sistema pasa por una cuestión política de no afectar la economía del usuario, "si esa decisión no se acompaña con recursos, indefectiblemente afecta la economía del sistema y la de sus prestadores".

Igualmente ahora debe esperarse la decisión del Poder Ejecutivo quien deberá aceptar el aumento y ponerlo en práctica o establecer cuál será el que corresponde en 2022 para el transporte público de pasajeros.