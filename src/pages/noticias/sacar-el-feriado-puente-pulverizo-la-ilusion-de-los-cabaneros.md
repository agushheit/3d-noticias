---
category: Agenda Ciudadana
date: 2021-05-14T08:13:43-03:00
thumbnail: https://assets.3dnoticias.com.ar/cabanas.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de LT10
resumen: Sacar el feriado puente “pulverizó la ilusión” de los cabañeros
title: Sacar el feriado puente “pulverizó la ilusión” de los cabañeros
entradilla: Desde la Cámara de Cabañeros aseguran que los teléfonos no estaban sonando,
  y de hacerlo era para cancelar las pocas reservas que tenían.

---
Pese a que ningún decreto los obligó a cerrar, las restricciones en la circulación de personas afectaron fuertemente a los cabañeros santafesinos.

Además de las medidas restrictivas, la reciente cancelación del feriado puente del 24 de mayo, "pulverizó la ilusión" del sector según describió Guillermo Kees, referente del los cabañeros.

"Justo estábamos ayer en una reunión de la Cámara de Cabañeros con el secretario de Turismo al que le trasladábamos nuestra preocupación" explicó Kees sobre la situación de que "aún estando abierto, la demanda había caído. Nuestra burbuja es la familia, que se cuida mucho y tomó la recomendación del gobierno y se queda en la casa. Eso hizo que no hubiera reservas y se venía el finde largo y siempre tenemos la ilusión de que el público santafesino responde muy bien".

Asimismo acotó que en "esta ocasión no estaba sonando el teléfono, y si sonaba era para cancelar las reservas".

Si bien resaltó que no existe ningún decreto que los cierre, "la realidad es esa, a la que se sumó la eliminación del feriado largo, y eso terminó por pulverizar la ilusión de los cabañeros".

La situación es tan crítica que la gente incluso rechazaba la noche gratis del viernes que se ofrecía, debido a que la restricción en la circulación no permite andar por las calles después de las 20 hs en vehículos particulares.

"La demanda cayó, y aniquilar un feriado largo, que es lo que te moviliza y ayuda a ponerte al día con gastos y personal, es directamente aniquilar al sector" concluyó Kees.