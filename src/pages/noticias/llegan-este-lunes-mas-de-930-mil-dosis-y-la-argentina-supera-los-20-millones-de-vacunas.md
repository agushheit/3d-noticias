---
category: Agenda Ciudadana
date: 2021-06-14T06:15:16-03:00
thumbnail: https://assets.3dnoticias.com.ar/MASVACUNAS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Llegan este lunes más de 930 mil dosis y la Argentina supera los 20 millones
  de vacunas
title: Llegan este lunes más de 930 mil dosis y la Argentina supera los 20 millones
  de vacunas
entradilla: De esta total, 9.415.745 corresponden a Sputnik V, 4.000.000 a Sinopharm,
  580.000 a AstraZeneca - Covishield, 1.944.000 AstraZeneca a través del mecanismo
  Covax y 4.737.400 dosis de AstraZeneca-Universidad de Oxford.

---
Una partida con más de 930 mil dosis de la vacuna AstraZeneca, elaboradas con el principio producido en la Argentina, llega este lunes desde los Estados Unidos, con lo cual el país superará los 20 millones de fármacos contra el coronavirus recibidas desde el inicio de la pandemia. 

El arribo del vuelo AM1407 está previsto para las 17.15 con 934.200 dosis de AstraZeneca, de producción conjunta con México, y será el cuarto cargamento de este tipo de vacunas, que se sumarán a las 843.600 dosis recibidas el 27 de mayo, a las 2.148.600 que arribaron el 31 y a las 811.000 que llegaron en la noche de este sábado, en un vuelo de la compañía Aeroméxico, directamente desde el Distrito Federal.

El vuelo de la compañía mexicana fue recibido por la ministra de Salud, Carla Vizzotti, quien manifestó que "este es un embarque muy especial, porque es el primero que viene desde México y porque es un adelanto de la parte que le correspondía a México y que nos fue cedida por la situación que atraviesa la Argentina".

Además, la titular de la cartera sanitaria destacó que "con las más de 930 mil dosis que llegarán el lunes superaremos los 20 millones para completar el plan de vacunación".

En tanto, el jefe de Gabinete, Santiago Cafiero, dijo este domingo que "los cuidados que se fueron teniendo y el esfuerzo de toda la ciudadanía hizo que los casos bajaran", aunque advirtió que "está lejos de ser superada la pandemia".

A la vez destacó la cantidad de vacunados y dijo que el Gobierno se maneja "con parámetros objetivos, que no son caprichosos".

Con la partida de inmunizadores de mañana, Argentina alcanzará la cifra de 20.677.145 de vacunas: 9.415.745 corresponden a Sputnik V (7.875.585 del componente 1 y 1.540.160 del componente 2), 4.000.000 de Sinopharm, 580.000 AstraZeneca - Covishield, 1.944.000 AstraZeneca a través del mecanismo Covax y 4.737.400 serán dosis de AstraZeneca-Universidad de Oxford.

**Más y más vacunas**

En tanto, el viernes se anunciaron nuevos contratos con Sinopharm por 2 millones más de dosis y con Cansino por 5,4 millones de vacunas Convidencia, cuya autorización "con carácter de emergencia" concretó en las últimas horas el Ministerio de Salud, tras la aprobación de la Administración Nacional de Medicamentos, Alimentos y Tecnología Médica (Anmat).

La vacuna Convidencia (Recombinant Novel Coronavirus Vaccine (Adenovirus Type 5 Vector) es del laboratorio Cansino Biologics Inc (Beijing institute of Biotechnology), y se trata de una vacuna de una sola dosis, la tercera desarrollada por China, luego de Sinovac y Sinopharm.

La funcionaria anunció también la llegada del contrato celebrado con Sinopharm por el que se recibirán 2 millones de dosis en junio y 4 millones el mes siguiente.

Esta mayor cantidad de dosis arribadas, permitió que, a través del Plan Estratégico de Vacunación, el Gobierno nacional junto a las 24 jurisdicciones del país, volvieran a alcanzar esta semana el récord de vacunaciones diarias con 376.815 inoculaciones aplicadas.

Este domingo se informó oficialmente que casi el 30% de la población de todo el país ya recibió la primera dosis de la vacuna

De acuerdo a los datos suministrados, ya cumplió con la inmunización de la primera dosis en el 28,63% del total de la población, porcentaje que representa a 12.991.480 personas vacunadas en las 24 jurisdicciones desde diciembre pasado, de un universo de 45.376.763 habitantes del país, según el registro del Indec en 2020.

La cartera sanitaria informó que ya están vacunados con la primera dosis el 84,5% de todas las personas mayores de 60 años, el 86% de las personas entre 60 a 69 años también recibieron una dosis, el 88% de los que tienen entre 70 y 79 años fueron vacunados, y el 79,5% de los de 80 años o más, fueron inoculados con al menos una dosis.

De acuerdo a los datos del Monitor Público de Vacunación, el registro online que muestra en tiempo real el operativo de inmunización en todo el territorio argentino, hasta esta mañana de domingo fueron distribuidas 18.853.790 vacunas, de las cuales 16.286.902 ya fueron aplicadas: 12.991.480 personas recibieron la primera dosis y 3.295.422 ambas.

Vizzotti dijo que "el panorama es muy alentador" porque "cada vacuna es una posibilidad menos de internación" pero advirtió que "hay que fortalecer los cuidados en estos meses. Seguir cuidándonos mientras vacunamos", remarcó.