---
category: La Ciudad
date: 2021-11-14T22:36:46-03:00
thumbnail: https://assets.3dnoticias.com.ar/c1f6e3bbaba13ba51967823d390e7e9e85322031.png
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Amplio triunfo de Molina en la elección de concejales en Santa Fe
title: Amplio triunfo de Molina en la elección de concejales en Santa Fe
entradilla: 'Con más 95 por ciento las mesas escrutadas, la candidata del Juntos por
  el Cambio llega a los 57.991 votos.

'

---
Con el 95 por ciento de las mesas computadas (890) la candidata de Juntos por el Cambio, Adriana Molina va camino a un amplio triunfo en la elección de concejales en Santa Fe. Amplía su ventaja sobre sus adversarias y alcanza los 57.991 votos.

Es seguida por la oficialista Laura Mondino, Frente Progresista Cívico y Social, con un total de 39.642 sufragios.

De cerca, y con 31.736 votos, se ubica la candidata del Frente de Todos, Jorgelina Mudallel.

Más atrás, le siguen Saúl Perman (Mejor) con 18.593 sufragios. Luego, Juan José Piedrabuena (Unión Federal) con 17.163. Más atrás, la candidata de Barrio 88, Eliana Ramos (9.847 votos) y Maximiliano Puigpinos (Frente Federal Vida y Familia) con 9.142.