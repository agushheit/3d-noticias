---
layout: Noticia con imagen
author: .
resumen: Presupuesto 2021
category: Estado Real
title: El Senado tratará este jueves el Presupuesto 2021
entradilla: El Senado realizará este jueves una nueva sesión especial para
  tratar el proyecto de Presupuesto 2021, que sería convertido en ley en caso de
  ser aprobado. Parte de la oposición se abstendrá en la votación.
date: 2020-11-12T13:34:59.461Z
thumbnail: https://assets.3dnoticias.com.ar/senado.jpg
---
La presidenta del Senado, Cristina Kirchner, convocó a una sesión especial para las 14:00hs. El Frente de Todos buscará un respaldo político fuerte para enviar una señal al FMI en el marco de la negociación con el Gobierno por el refinanciamiento de la deuda.

Durante el rápido debate del proyecto en la Comisión de Presupuesto y Hacienda, los senadores del Frente de Todos mostraron su interés en que la aprobación de la ley cuente con respaldo opositor para robustecer la postura del Gobierno en las negociaciones con el Fondo Monetario.

Así lo sugirió el jefe del bloque Frente de Todos, José Mayans, en aquella reunión de la semana pasada, donde afirmó que "sería importante dar esta herramienta para poder ir al Fondo Monetario Internacional".

Por su parte, el presidente de la Comisión de Presupuesto, Carlos Caserio, dijo en el mismo sentido que "si el Senado acompaña con su voto, para el país sería un adelanto significativo de que la política acompaña al que gobierna".

Fuentes del interbloque de Juntos por el Cambio indicaron que la mayoría de la bancada se abstendrá y solo votará en contra de algunos artículos, como una forma de mostrar un relativo apoyo político, mientras que los más ligados a los gobernadores votarían a favor.

El Presupuesto 2021 prevé un crecimiento de la economía del 5,5%, una inflación cerca de 29% y un déficit fiscal de 4,5% del PBI, al tiempo que estima un crecimiento de las exportaciones del 10,2% para 2021 y un aumento de las importaciones de 20,4%.

Durante el debate en comisión, el secretario de Hacienda, Raúl Rigo, había indicado ante las preguntas de la oposición que las asistencias del IFE y el ATP implementadas en el marco de la pandemia "no están" en el presupuesto "pero hay programas que buscan tender este puente".

En este sentido, Rigo subrayó que "hay casi 100 mil millones de pesos asignados para financiar acciones que tienen que ver con la promoción de la actividad económica y subsidio de tasas de interés".

Además, señaló que el Presupuesto propone un déficit primario de 4,5% y apunta a que "vaya tendiendo hacia la convergencia fiscal", al tiempo que indicó que el Ministerio de Economía prevé "que parte del financiamiento sea monetario y otra porción provenga del endeudamiento".

El funcionario señaló que "el gasto en infraestructura se va a duplicar, en términos de porcentaje del PIB, en 2021 con respecto al 2019", al pasar del 1,1% en ese año al 2,2% para el próximo, en "una clara apuesta a la reactivación y al rol activo del Estado a través de la obra pública".

También dijo que "el crecimiento real del gasto primario en el Presupuesto es posible porque otros gastos están disminuyendo y son los intereses de la deuda pública".