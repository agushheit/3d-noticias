---
category: Agenda Ciudadana
date: 2021-10-09T06:00:16-03:00
thumbnail: https://assets.3dnoticias.com.ar/puerto.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Millonarios aportes de Nación para los puertos santafesinos
title: Millonarios aportes de Nación para los puertos santafesinos
entradilla: Cuatro convenios dejó rubricados el ministro Alexis Guerrera, incluyendo
  aportes por $ 200.000.000 para la estación fluvial en Santa Fe y una suma similar
  para los muelles de Reconquista.

---
El gobernador de Santa Fe, Omar Perotti y el ministro de Transporte de la Nación, Alexis Guerrera, firmaron un Convenio Marco que tiene por objeto sentar las bases para desarrollar actividades de cooperación y colaboración para el mejoramiento de los niveles de eficacia y eficiencia de las operaciones portuarias para los muelles santafesinos.

 Además, se firmó un convenio para el mejoramiento de la infraestructura y servicios portuarios del Puerto de Santa Fe, que demandará una inversión por parte del Ministerio de Transporte de $ 200.000.000, y contempla obras en la Terminal de Contenedores y Cargas Generales; la Terminal Multipropósito; la Terminal de Agrograneles y la adquisición de nuevos equipos.

 En este caso el acuerdo fue rubricado por Guerrera y el presidente del Ente Administrador del Puerto de Santa Fe, Carlos Arese.

 El tercer convenio fue firmado junto al Ente Administrador Puerto Reconquista (EAPRe) y tiene como objeto la reactivación del Puerto Reconquista y contempla una inversión de $ 200.000.000.

 Se llevará a cabo la reparación y modernización de los frentes de atraque; construcción de silos de almacenamiento, galpones, sistemas de carga incluida la fijación de la cinta transportadora al muelle; reparación del pavimento portuario; dragado del canal de acceso y adquisición de equipamiento para la señalización y la operatividad del puerto, entre otras tareas. Firmaron el ministro nacional y el interventor del Puerto de Reconquista, Martín Deltin.

 El cuarto convenio de cooperación fue firmado entre el Ministerio de Transporte de la Nación, la provincia de Santa Fe, las municipalidades de las ciudades de Santa Fe y Santo Tomé y la Universidad Nacional del Litoral (UNL) para la elaboración del estudio sobre la viabilidad técnica, económica y operativa respecto del servicio ferroviario de pasajeros en el tramo ciudad Santa Fe–Santo Tomé.

 