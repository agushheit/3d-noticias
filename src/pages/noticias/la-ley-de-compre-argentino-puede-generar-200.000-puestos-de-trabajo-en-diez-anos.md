---
category: Agenda Ciudadana
date: 2021-09-14T06:15:53-03:00
thumbnail: https://assets.3dnoticias.com.ar/TAMBORENEA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: La ley de Compre Argentino puede generar "200.000 puestos de trabajo en diez
  años"
title: La ley de Compre Argentino puede generar "200.000 puestos de trabajo en diez
  años"
entradilla: Así lo manifestó el presidente de la Cámara Argentina de Industrias Electrónicas,
  Electromecánicas y Luminotécnicas, José Tamborenea, al participar en la presentación
  del proyecto de ley.

---
El presidente de la Cámara Argentina de Industrias Electrónicas, Electromecánicas y Luminotécnicas (Cadieel), José Tamborenea, aseguró este lunes que la política de Compre Argentino puede generar "200 mil puestos de trabajo en diez años".

Así lo manifestó al participar en la presentación del proyecto de Ley de Compre Argentino, Desarrollo de Proveedores y Compras para la Innovación junto al presidente Alberto Fernández y el ministro de Desarrollo Productivo, Matías Kulfas, en un acto en Casa Rosada este mediodía.

"Esta ley proviene de escuchar, de los oídos que nos han prestado los Secretarios y el Ministro, de haber compartido tiempo y espacio para articular algo coherente y de largo plazo, que cuenta con todos los industriales para este apoyo", le dijo Tamborenea a Fernández.

En ese sentido, aseguró que "esta es la vía para el desarrollo" y manifestó que "si esto no está llegando a la gente sí está llegando a nuestras fábricas" ya que "estamos empezando a recuperar el empleo y las productividades".

Y puntualizó que "si esta política se continúa por unos diez años vamos a estar hablando en vez de 40.000 de 200.000 puestos de trabajo".

En otro tramo, el industrial señaló que "si esto sale bien obviamente que va a repercutir en mucho más trabajo, mejor redistribución del ingreso y va a aportar a las arcas del Estado cuantiosas cifras para ser mejor administradas".

"Cuenta con todo el apoyo de la industria para llevar esto adelante, porque nosotros hemos vivido desde el régimen de compras públicas la no aplicación del mismo y eso nos ha costado -y eso lo hablo a título personal- el 33% de la plantilla", agregó Tamborenea.

Y concluyó: "los países industrializados han defendido el régimen de compras públicas y con esto han engrandecido la producción y sus capacidades, además de la masa laboral".

El proyecto de Ley de Compre Argentino, Desarrollo de Proveedores y Compras para la Innovación apunta a generar 30.000 nuevos empleos privados, fomentar la inversión de $ 40.000 millones anuales adicionales en innovación y ahorrar US$ 500 millones de divisas al año.

Presentado este mediodía por Fernández y Kulfas, propone la modificación de la actual Ley 27.437, sancionada en 2018, que "tiene grandes limitaciones para que el Estado pueda utilizar su poder de compra como herramienta para el desarrollo".

Los principales objetivos son promover una mayor participación de la industria nacional en las compras públicas; generar un aumento en el empleo; y estimular inversiones y transferencias de tecnología hacia los sectores de la economía nacional con mayores capacidades tecnológicas y productivas.