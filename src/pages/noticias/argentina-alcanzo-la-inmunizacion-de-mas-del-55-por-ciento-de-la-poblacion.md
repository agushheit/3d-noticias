---
category: Agenda Ciudadana
date: 2021-08-03T06:00:38-03:00
thumbnail: https://assets.3dnoticias.com.ar/INMUNI.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Argentina alcanzó la inmunización de más del 55 por ciento de la población
title: Argentina alcanzó la inmunización de más del 55 por ciento de la población
entradilla: Detrás de la vacunación está la puerta de salida que nos permitirá reencontrarnos
  con la vida que queremos", anunció el presidente Fernández. Además, las aplicaciones
  en los mayores de 18 años treparon al 74,35%.

---
Más de la mitad de la población total de la Argentina ya recibió al menos una dosis de la vacuna contra el coronavirus y, en el caso de los jóvenes mayores de 18 años esa cobertura alcanzó el 74,35%, en el marco del Plan Estratégico de Vacunación que lleva adelante el Gobierno nacional y que durante julio se robusteció con la llegada récord de casi 17 millones de vacunas y 11 millones de aplicaciones en todo el país, informaron fuentes oficiales.

Argentina alcanzó el pasado domingo la inmunización del 55,49% de la población total con al menos una dosis, mientras que en los mayores de 18 años las aplicaciones treparon al 74,35%.

El presidente Alberto Fernández celebró los porcentajes alcanzados durante un acto en el que se presentó la ampliación del programa Ahora 12 y vinculó la inmunización con el crecimiento de la producción y de la economía del país.

"Detrás de la vacunación está esa puerta de salida que nos permitirá reencontrarnos con la vida que queremos", resaltó Fernández y puso ejemplos cotidianos de las posibilidades que habilita la inmunización total, como "dejar de lado el barbijo, compartir afectos, abrazar a los seres queridos, ir a ver fútbol o a escuchar música".

Las jurisdicciones más avanzadas en términos de inmunización con la primera dosis son CABA (66,75%), La Pampa (60,54%), San Luis (60,16%) y Río Negro (59,09%) y en cuanto a la población mayor de 18 años los distritos con mejores índices son La Rioja (82,34%), Santiago del Estero (79,97%), San Luis (79,41%) y Santa Fe (78,91%).

**Arribo de vacunas**

El total de vacunas contra la Covid-19 recibidas durante el mes pasado fue de 16.895.200 y se alcanzaron las 11.241.066 aplicaciones, con un promedio diario de 362.615 inoculaciones.

El arribo de vacunas también registró una cifra récord, a partir de las 16.895.200 dosis recibidas en 19 vuelos a lo largo del séptimo mes del año.

De ese total, 1.744.000 son de Sputnik V (1.141.000 del componente 1 y 603.000 del componente 2), 8.768.000 de Sinopharm, 2.883.200 de AstraZeneca y Oxford y 3.500.000 de Moderna.

Desde el inicio de la campaña, la Argentina recibió 42.601.930 vacunas, de las cuales 14.768.000 corresponden a Sinopharm; 11.868.830 a Sputnik V, (9.375.670 del componente 1 y 2.493.160 del componente 2); y 9.941.100 a las de AstraZeneca y Oxford, cuyo principio activo se produjo en la Argentina.

Otras 3.500.000 fueron de Moderna; 1.944.000 de AstraZeneca por el mecanismo COVAX de la OMS y 580.000 de a AstraZeneca-Covishield.

De acuerdo con el Monitor Público de Vacunación, hasta la mañana del lunes se distribuyeron 38.086.954 dosis en todas las jurisdicciones y se aplicaron 32.335.391: con la primera dosis 25.178.202 personas, mientras que 7.157.189 cuentan con el esquema completo de inmunización.

En tanto, esta semana llegarán al país desde Rusia 300 litros más del componente 2 de la vacuna Sputnik V para continuar completando el esquema de vacunación, según lo anunció la semana pasada la ministra de Salud, Carla Vizzotti.

A su vez, el miércoles de la pasada semana, arribaron al país 500 litros del componente 2 de la vacuna Sputnik V que "permitirán seguir produciendo" más de 750 mil dosis para continuar completando el esquema de vacunación, de acuerdo a lo señalado por el Laboratorio Richmond, a cargo de la producción de la vacuna.

Indicó el laboratorio que, con ese cargamento, se fabricarán en la Argentina 760.000 dosis del componente 2 de la vacuna desarrollada en Rusia que "en breve estarán a disposición" para las personas.

El laboratorio Richmond Lab en la Argentina es el encargado, a partir de un acuerdo con el Instituto Gamaleya y el Fondo Ruso de Inversión Directa (RDIF), de producir la vacuna Sputnik V en el país.

**La combinación de componentes**

Por otro lado, la ministra se refirió también días atrás, a los estudios que se desarrollan sobre la aceptación de la combinación de vacunas: "Los datos que se vienen obteniendo en el mundo en la combinación de las vacunas (Sputnik-AstraZeneca) son muy buenos", aseguró.

En ese sentido, resultados preliminares del primer estudio realizado a nivel mundial sobre el uso combinado de la vacuna de AstraZeneca y la primera dosis de Sputnik V demostraron eficacia contra la infección por coronavirus y ausencia de efectos secundarios en las personas inmunizadas, según informó el Fondo Ruso de Inversión Directa (RDIF, por sus siglas en inglés).

"Los datos que se vienen obteniendo en el mundo en la combinación de las vacunas (Sputnik-AstraZeneca) son muy buenos"

El médico infectólogo e integrante del comité de expertos que asesora al Gobierno nacional Tomás Orduna dijo tras la detección de la variante Delta en el país que "a pesar de que una sola dosis puede no ser protectiva para tener la enfermedad, difícilmente el paciente haga un cuadro que lo complique para pensar en muerte".

"El paciente tiene una gran chance de no pasar a un cuadro crítico", explicó el jefe del Servicio de Medicina Tropical y del Viajero del Hospital Muñiz, aunque admitió estar preocupado por la expansión de la variante Delta en el país.

Advirtió que la transmisión comunitaria de la variante Delta es algo que "va a ocurrir", pero el objetivo de las autoridades sanitarias es demorar esa situación para tener la mayor cantidad de personas vacunadas "con doble dosis" y un menor impacto de esa variante.