---
category: Agenda Ciudadana
date: 2021-04-28T08:56:24-03:00
thumbnail: https://assets.3dnoticias.com.ar/monotributo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Beneficio para monotributistas que estén al día con el pago
title: Beneficio para monotributistas que estén al día con el pago
entradilla: El beneficio alcanza a 399.000 monotributistas y 85.000 autónomos que
  hayan pagado en tiempo y forma su mensualidad. Cómo acceder.

---
La Administración Federal de Ingresos Públicos (AFIP) reintegrará más de $ 973 millones a monotributistas y autónomos que abonaron durante 2020 sus obligaciones por débito automático o tarjeta de crédito.

El organismo informó en un comunicado que "el beneficio al cumplimiento alcanza a más de 484.000 pequeños contribuyentes de todo el país: 399.000 monotributistas y 85.000 autónomos". La medida implementada por el organismo que encabeza Mercedes Marcó del Pont consiste en la devolución de una porción del componente impositivo abonado a lo largo del año.

Alcanza a quienes hayan pagado en tiempo y forma su mensualidad, y las personas beneficiadas recibirán de devolución el equivalente a una cuota. El pago será acreditado automáticamente en las cuentas bancarias o tarjetas de crédito adheridas como medio de pago por cada uno de las y los contribuyentes cumplidores.

Aquellos contribuyentes que hayan iniciado la actividad el año pasado y por ello no hubieran abonado 12 meses completos tendrán la devolución del 50%, siempre que la cantidad de cuotas pagadas en término fueran de 6 a 11.

El monto acreditado -o los motivos por los cuales no se realizó el reintegro- podrá consultarse en el servicio "CCMA - Cuenta Corriente de Monotributistas y Autónomos", al que se ingresa con clave fiscal.