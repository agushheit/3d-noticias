---
category: Estado Real
date: 2021-09-02T06:00:19-03:00
thumbnail: https://assets.3dnoticias.com.ar/COMPETITIVIDAD.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Perotti: “Santa Fe tiene que dar un salto de competitividad muy alto”'
title: 'Perotti: “Santa Fe tiene que dar un salto de competitividad muy alto”'
entradilla: 'El gobernador de la provincia participó este miércoles del inicio del
  ciclo estratégico de conversaciones: “Acelerando la Transición de las PyMEs santafesinas
  hacia la Industria 4.0”.'

---
En el marco del Día de la Industria, el gobernador Omar Perotti participó este miércoles, en El Molino Fábrica Cultural, del inicio del ciclo estratégico de conversaciones: “Acelerando la Transición de las PyMEs santafesinas hacia la Industria 4.0”, que el gobierno provincial lleva adelante con el apoyo del Consejo Federal de Inversiones (CFI).

En la oportunidad, el mandatario expresó que “tenemos una realidad muy distinta con estas empresas que nacen globales para poder existir. Entonces hay que ver cómo cada uno acelera la formación, la capacitación y la comprensión de estos procesos, porque es central que quiénes conducen la empresa manejen estas herramientas, comprendan el proceso y la potencialidad de las mismas, e incorporen en los planteles a quienes permitan el pleno uso de ellas para mejorar lo que hacemos todos los días, frente a esta instancia de cambio”, aseguró Perotti.

Asimismo, el gobernador dijo que otro aspecto central se refiere al programa “Santa Fe Más Conectada, para que el desarrollo de un programa pueda pasar en todo nuestro territorio, que sea la posibilidad de que lo desarrollen algunas empresas donde hay buena conectividad”, y agregó que Santa Fe ocupa “el lugar número 14 en la llegada de fibra de Internet a cada uno de los hogares, y el número 9 en velocidad”, mientras que “en muchos aspectos productivos somos los número 1, los números 2 y los números 3; nunca el 9”, recordó.

“Esta es una señal -siguió Perotti-, de que aquí hay que dar un salto y por eso insistimos tanto en la necesidad de que la Cámara de Diputados de la provincia apruebe la ley que ya tiene media sanción desde octubre del año pasado del Senado, que permita a la provincia tomar el único crédito aprobado hasta aquí de la Corporación Andina de Fomento, de 100 millones de dólares a la tasa más baja que la provincia haya tenido en su historia”, propuso el gobernador.

En ese sentido, mencionó que “Santa Fe tiene que dar un salto de competitividad muy alto”, y destacó que “la provincia tiene la capacidad en sus actores de todos los sectores productivos para hacerlo”.

A su vez, remarcó que ese salto de competitividad “nos tiene que permitir también ser sustentables porque la sustentabilidad y el máximo de innovación son las dos cosas que tenemos que tener en la provincia. Éste es el salto fundamental que podemos dar si encaramos y llevamos adelante este proceso en forma conjunta”, afirmó Perotti.

Por último, el mandatario santafesino agradeció “los esfuerzos hechos durante la pandemia, por el trabajo coordinado que nos permite hoy, con orgullo, ser la provincia que encabeza la recuperación económica en la Argentina y la recuperación del empleo directo registrado. Esto es un esfuerzo en conjunto que los tiene a ustedes como actores principales”, expresó.

Por su parte, el secretario de Asuntos Estratégicos de la Nación, Gustavo Béliz, reflexionó que “hoy está muy en boga hablar de la industria 4.0, de la economía 4.0, de la transformación productiva 4.0: es muy importante que esto ocurra y para que esto funcione tenemos que hablar también de una actitud 4.1, porque sin seres humanos que estén muy compenetrados en los requerimientos, en las habilidades, en las actitudes que se tienen que tomar para encarar estas transformaciones todo quedará en una mera cuestión de técnica, de modificación simplemente tecnológica de los modos de producción y de entender nuestra economía”, manifestó el funcionario.

En tanto, el ministro de Producción, Ciencia y Tecnología de la provincia de Santa Fe, Daniel Costamagna, expresó que “estas tecnologías cruzan transversalmente a todo el sistema productivo de la provincia, son cuestiones tangibles que tienen nuestras empresas y que posibilitan darle a todo este gran estructurado productivo una competitividad enorme en los mercados internacionales, basados en productos de calidad y en un proceso de eficiencia y control de los procesos admirable”.

Además, Costamagna añadió que “el gran desafío es generar los procesos de capacitación, de estudio y articulación, que estamos llevando adelante, que van a permitir que todos los que componen el eje productivo santafesino puedan utilizar de la mejor manera posible estas tecnologías”.

A su turno, la secretaria de Gestión Federal, Candelaria González del Pino, indicó que “hoy empezamos este ciclo de conversaciones que venimos pensando hace bastante tiempo con distintas áreas de gobierno, fundamentalmente cuando el gobernador Omar Perotti presentó el programa Santa Fe + Conectada, financiado por CAF (Banco de Desarrollo de América Latina) y priorizado por la Secretaría de Asuntos Estratégicos de la Nación. Cuando se dio ese lanzamiento, se dio un proceso de intercambio con otras áreas de gobierno para ver y empezar a generar herramientas que permitan acompañar este proceso y también medir el impacto que podría tener este programa de Santa Fe + Conectada tanto en las áreas como en los distintos sectores de la sociedad”, expresó la funcionaria, habida cuenta que “sólo el 18% de los hogares santafesinos tienen una buena conexión a Internet y el 72% de las escuelas no tiene conexión para uso pedagógico”, sostuvo.

Por último, el presidente de la Federación Industrial de Santa Fe (Fisfe), Víctor Sarmiento, destacó que “llevamos adelante una articulación muy fuerte con el gobierno provincial para poner en marcha todos los sectores que no habían sido considerados como esenciales, lo que hizo que Santa Fe lidere la recuperación productiva a nivel nacional”.

De la actividad participaron también el presidente de Terragene, Esteban Lombardía; el secretario de Industria, Claudio Mossuz; la secretaria de Cooperación Internacional e Integración Regional, Julieta de San Félix; secretaria de Ciencia y Tecnología, Marina Baima; el secretario de Agroalimentos, Sergio Torelli; el presidente de la Unión Industrial de Santa Fe (UISF), Alejandro Taborda; y el vicepresidente de la Cámara de Frigoríficos de Santa Fe (Cafrisa), Antonio D’angelo, entre otras autoridades.

**“SANTA FE + CONECTADA”**

La iniciativa se desarrolla en el marco del Programa “Santa Fe + Conectada” que busca garantizar conectividad de calidad a las 365 localidades de la provincia. Asimismo, tiene por objetivo ampliar y modernizar la infraestructura del sistema de conectividad para promover la inclusión digital, el arraigo, la transformación educativa, la gestión empresarial y del Estado, y la reducción de brechas tecnológicas, productivas y de género con impacto concreto para el desarrollo sostenible del territorio.

La provincia accedió a un financiamiento con el Banco de Desarrollo para América Latina (CAF) para llevar adelante este proyecto, el cual promoverá además la implementación de soluciones de innovación tecnológica conforme a las necesidades productivas territoriales.