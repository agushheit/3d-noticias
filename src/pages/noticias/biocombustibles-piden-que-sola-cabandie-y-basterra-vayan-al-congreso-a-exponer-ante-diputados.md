---
category: Agenda Ciudadana
date: 2021-05-04T08:10:35-03:00
thumbnail: https://assets.3dnoticias.com.ar/biocombustibles.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NCN
resumen: 'Biocombustibles: Piden que Solá, Cabandié y Basterra vayan al Congreso a
  exponer ante Diputados'
title: 'Biocombustibles: Piden que Solá, Cabandié y Basterra vayan al Congreso a exponer
  ante Diputados'
entradilla: La ley de Biocombustibles que se discute en la Comisión de Energía de
  la cámara de Diputados sigue generando polémica y las adhesiones no se terminan
  de concretar para lograr dictamen favorable.

---
La ley de Biocombustibles que se discute en la Comisión de Energía de la cámara de Diputados sigue generando polémica y las adhesiones no se terminan de concretar para lograr el dictamen favorable que buscan desde el Frente de Todos. En ese contexto, los legisladores lavagnistas de Consenso Federal pidieron que tres ministros del gabinete de Alberto Fernández expongan en la cámara baja y den explicaciones sobre esta iniciativa.

En concreto, Consenso Federal le solicitó al diputado Omar Félix, -presidente de la comisión- que cite a tres ministros del Gobierno para que puedan emitir sus opiniones respecto al tema.

El pedido de la visita de los ministros al Congreso la realizó el diputado nacional y presidente del bloque Consenso Federal en Diputados, Alejandro ‘Topo’ Rodríguez, quien pidió que se cite a tres ministros del Gabinete: Felipe Solá, el titular de Ambiente, Juan Cabandié y el de Agricultura, Luis Basterra.

El pedido de Rodríguez fue una solicitud de manera verbal al presidente de la comisión de energía y combustibles en la última reunión, en la que Omar Félix se comprometió a convocarlos

El legislador lavagnista, luego de hacer su solicitud formal en la última reunión de comisión, comentó que al canciller Felipe Solá «vamos a pedirle su opinión sobre el proyecto oficial de Biocombustibles y cómo se vincula eso con la posición argentina en la XXVII Cumbre Iberoamericana respecto de ‘los planes de pago eficientes y efectivos por servicios ambientales'», detalló Rodríguez a través de sus redes sociales.

Por otro lado, explicó que al ministro Juan Cabandié lo abordarán respecto al tema de «la falta de progresividad en la política de Biocombustibles, dando lugar a que baje drásticamente la producción y el uso de biodiesel, que disminuye 70% la emisión de gases de efecto invernadero comparado con combustibles fósiles», describió.

A su vez, y respecto al ministro Basterra, aseguró que su visita en el Congreso «servirá para saber el impacto del proyecto oficial de Biocombustibles en la industrialización de la ruralidad, la inversión productiva y el sostenimiento de puestos de trabajo agroindustriales para 10 provincias argentinas».

Aún así, desde el oficialismo buscarán avanzar esta semana con el dictamen favorable a un nuevo marco regulatorio de la ley de Biocombustibles que tendrá vigencia hasta el 2030, y establece la reducción del 10 al 5 por ciento en el corte obligatorio entre gasoil y biodiesel.

Esperando que el oficialismo cumpla con el compromiso de convocar a los ministros del Gabinete, las principales cámaras del sector cuestionaron la semana pasada la iniciativa que propone reducir del 10% al 5% el corte obligatorio del gasoil con biodiésel, y volvieron a pedir la prorroga de la normativa que vence el 12 de mayo próximo.

El proyecto que presentó el Frente de Todos tiene como puntos centrales los porcentajes de los cortes entre gasoil y biodiesel y bioetanol con caña de azúcar y con maíz, y otorga facultades a la Secretaria de Energía para subir o bajar esos porcentajes.

Sin embargo, la mayoría de los empresarios rechazaron la reducción del corte obligatorio así como legisladores de Juntos por el Cambio, con excepción del Centro de Azucareros y la Cámara Santafesina de Energías Renovables.

Por ejemplo, en la última reunión de comisión, el vicepresidente de la comisión de Energía, Omar Demarchi (Juntos por el Cambio), dijo que «la iniciativa del Gobierno genera incertidumbre y lleva al borde de la quiebra a un sector que genera valor agregado, contribuye al ambiente, utiliza tecnología nacional, con inversiones ya hechas por más de 3.000 millones de dólares y genera decenas de miles de puestos de trabajo en forma directa e indirecta.»

«Lo razonable es una prórroga de la actual ley 26.093 para abrir un tiempo de intercambio productivo y enriquecedor, y no cometer el error de retroceder 15 años de avance«, agregó.

En cambio, el presidente del Centro de Azucareros, Jorge Feijóo, se mostró satisfecho por el proyecto del Frente de Todos, ya que fueron incluidas pautas que son beneficiosas para ese sector.

Feijoo destacó que el bioetanol de caña de azúcar abastecerá un mínimo del 6% de la demanda total de naftas; que las reducciones del corte de bioetanol de caña de azúcar sólo pueden deberse por razones de escasez y serán temporarias; y que el bioetanol de caña de azúcar tendrá volumen y precio regulado.

Otro punto del proyecto que genera debate es el artículo que establece que la Secretaría de Energía podrá «elevar el referido porcentaje mínimo obligatorio cuando lo considere conveniente en función del abastecimiento de la demanda, la balanza comercial, la promoción de inversiones en economías regionales y/o razones ambientales o técnicas».

De forma inversa, el corte podrá reducirse hasta el 3% «cuando el incremento en los precios de los insumos básicos para la elaboración del biodiésel pudiera distorsionar el precio del combustible fósil en el surtidor, o ante situaciones de escasez de biodiésel por parte de las empresas elaboradoras».

El proyecto mantiene beneficios impositivos al fijar que el biodiésel y el bioetanol no estarán gravados por el Impuesto a los Combustibles Líquidos (ICL) y por el Impuesto al Dióxido de Carbono (ICO2).