---
category: La Ciudad
date: 2021-11-06T06:00:29-03:00
thumbnail: https://assets.3dnoticias.com.ar/transporte 14.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Colectivos: el gobierno anunció que el transporte urbano será gratuito el
  14 de noviembre'
title: 'Colectivos: el gobierno anunció que el transporte urbano será gratuito el
  14 de noviembre'
entradilla: 'Así lo informó el gobierno provincial, luego de una resolución del Ministerio
  de Transporte Nacional. Ese día se desarrollarán las elecciones generales.

'

---
La Secretaría de Transporte, del Ministerio de Producción, Ciencia y Tecnología, recordó que la provincia de Santa Fe, luego de adherir a la resolución del Ministerio de Transporte de la Nación, garantizará que el transporte urbano y suburbano será gratuito para las elecciones del próximo domingo 14 de noviembre.

Se trata de una medida establecida por la Resolución Nº 312/2021 del Ministerio de Transporte nacional y apunta a garantizar el ejercicio de los derechos políticos establecidos en la Constitución Nacional.

Al respecto, el secretario de Transporte, Osvaldo Miatello, manifestó: “El gobernador Omar Perotti firmó el Decreto que adhirió a esta resolución, que estableció la gratuidad del traslado, tanto en las elecciones Primarias Abiertas Simultáneas y Obligatorias (PASO) como en las generales. Desde el Gobierno Provincial consideramos fundamental garantizar un derecho tan importante como lo es el voto”.

El funcionario aclaró que la medida incluye a los transportes urbanos e interurbanos, e indicó: “Las empresas van a tener una compensación por este traslado gratuito, de modo que vamos a exigirles y controlar el cumplimiento de una frecuencia que cubra la correspondiente a, por lo menos, la de un día sábado”.