---
category: La Ciudad
date: 2021-12-23T06:00:03-03:00
thumbnail: https://assets.3dnoticias.com.ar/CAPACITACION.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Más de 60 jóvenes culminaron cursos de capacitación laboral
title: Más de 60 jóvenes culminaron cursos de capacitación laboral
entradilla: 'Durante todo el año, fueron cerca de 400 las mujeres y los varones de
  entre 18 y 24 años que concretaron diferentes instancias de acercamiento al mundo
  del trabajo. '

---
Este miércoles, la Estación de Barranquitas se pobló de jóvenes entre 18 y 24 años que recibieron sus certificados, por haber participado de Cursos de Introducción al Trabajo (CIT) y Entrenamientos para el Trabajo. El intendente, Emilio Jatón; y la secretaria de Integración y Economía Social del municipio, Ayelén Dutruel, fueron los encargados de otorgar los diplomas que acreditan el esfuerzo realizado por mujeres y varones de varios barrios de la ciudad que eligieron capacitarse, aprender y estar mejor preparados para salir al mundo laboral.

“Hoy cerramos un proceso que desarrollamos durante todo el año”, destacó Dutruel, y añadió que las capacitaciones, además de brindar herramientas para la inserción laboral y la alfabetización digital, “permiten que desde el municipio generamos vínculos con los jóvenes: qué les interesa, qué problemáticas tienen y cómo podemos acompañarlos en el sus procesos formativos y sus proyectos de vida”.

**Trabajo sostenido**

Durante este año, se realizaron 10 Cursos de Introducción al Trabajo (CIT), en los cuales participaron más de 300 jóvenes. Dichos talleres se desprenden del Programa Jóvenes por Más y Mejor Trabajo. Buscan ser una herramienta de formación y acompañamiento en busca de la inclusión social de jóvenes de 18 a 24 años que, por diversas circunstancias sociales, familiares y personales se encuentran fuera del sistema educativo y del mercado laboral formal.

Los cursos se desarrollan en tres módulos: Proyecto formativo ocupacional, Derecho laboral y salud ocupacional, y Alfabetización digital, los cuales se relacionan entre sí buscando abordar las problemáticas en la inserción laboral. La duración del curso es de tres meses, distribuidos en 36 encuentros, con una frecuencia de tres encuentros semanales de tres horas de duración, de forma presencial.

En tanto, el Entrenamiento Para el Trabajo (EPT) es una instancia formativa cuyo objeto es enriquecer y aumentar las posibilidades de inserción en el mercado laboral. Se realizan vinculaciones con empresas donde se transitan experiencias, aprendizajes y se enriquecen competencias relacionadas con un puesto de trabajo. En este caso, 91 jóvenes fueron seleccionados para EPT de enero a diciembre, y actualmente se realizan 46 EPT.

Dutruel afirmó que “estos procesos de aprendizaje fueron acompañados por los equipos de las Estaciones habilitando la escucha, la participación, la proximidad y el intercambio, y profundizando en los diferentes ejes temáticos que van surgiendo con la interacción cotidiana, recuperando saberes adquiridos, recapitulando experiencias en el mercado laboral y evaluando las posibilidades reales que su contexto presenta, sin perder de vista sus aspiraciones y el desarrollo de sus potencialidades”.

En ese sentido, la secretaria anticipó que durante el 2022 se ampliará la oferta vinculada al trabajo para los jóvenes “porque es lo que nos vienen planteando como interés, pero sobre todo como problemática. Sabemos que el mundo del trabajo hoy cambió y que nosotros tenemos que estar a la altura de la circunstancia para acompañarlos en ese proceso”, dijo por último.

**Mejor preparados**

A sus 20 años, Alexis González, trabaja desde hace tiempo. Sin embargo, acaba de finalizar el Curso de Introducción al Trabajo y siente que “era un empujoncito que necesitaba. Mis papás siempre me enseñaron que el trabajo es importante; a mí siempre me gustó tener lo mío y crecer en ese ámbito, y este curso me sirve para eso: para seguir aprendiendo y algún día llegar a algo más importante”, dice.

En consonancia, Alexis cuenta que “hasta ahora nunca tuve un trabajo en blanco, pero siento que ahora estoy un poco más preparado. Seguramente esto me va a ayudar a conseguir algo mejor”, asegura.

Por su parte, Ignacio García tiene 19 años y vive en Santa Rosa de Lima. Durante la entrega de diplomas, relató que de los cursos le quedó “todo lo que compartimos y me gustaría seguir aprendiendo de todo un poco”. Si bien, por el momento sigue con los estudios, Ignacio asegura que “más adelante me gustaría buscar un buen empleo para tener un mejor futuro”.

Elías López, vecino de Barranquitas Sur, coincidió en destacar el compañerismo que se vivió en los talleres. “Me interesaron estos cursos porque te ayudan a capacitarse para conseguir un buen trabajo, aprendí a expresarme, cómo me tengo que manejar; entre todos fuimos aprendiendo cómo estar preparados para salir a buscar un empleo”.