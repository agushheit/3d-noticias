---
category: La Ciudad
date: 2021-09-05T06:00:22-03:00
thumbnail: https://assets.3dnoticias.com.ar/cullen.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'Manga en Av. Freyre: comerciantes se reunieron con el director del hospital
  Cullen'
title: 'Manga en Av. Freyre: comerciantes se reunieron con el director del hospital
  Cullen'
entradilla: Anta la baja de los contagios, los trabajadores piden que se busque una
  alternativa para que siga funcionando, pero sin perjudicar el tránsito.

---
Un grupo de comerciantes de Av. Freyre se reunió este viernes con el director del hospital Cullen, Dr. Juan Pablo Poletti, por las dificultades que genera la permanencia de la manga que une al nosocomio con las carpas del hospital de campaña.

La manga se colocó y anuló el paso de los vehículos a principios de mayo, es decir hace ya cuatro meses. Ahora ante la baja de los casos, los afectados entienden que el hospital de campaña tiene que seguir sus funciones pero que ya no es necesario cortar el tránsito.

"(Poletti) insistió que sanitariamente no era conveniente", manifestó tras el encuentro Fernanda Nicolini, una de las perjudicadas.

"Hay alrededor de 12 comercios que ya cerraron y otros 20 que lo están evaluando porque no dan más. Se les está haciendo muy difícil pagar los sueldos", explicó. Según Nicolini, un 70% de comercios de la zona están afectados y que las ventas bajaron entre un 30% y un 50%.

Ante esta complicación, esperan ser escuchados y que se busque una alternativa para seguir haciendo funcionar el hospital de campaña pero sin perjudicar la transitabilidad por una arteria de intenso movimiento.

En ese sentido, los trabajadores adelantaron que enviarán una nota al Ministerio de Salud y al intendente Emilio Jatón.