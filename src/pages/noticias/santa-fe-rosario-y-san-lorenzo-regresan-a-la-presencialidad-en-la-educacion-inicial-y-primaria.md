---
category: Agenda Ciudadana
date: 2021-05-09T06:00:26-03:00
thumbnail: https://assets.3dnoticias.com.ar/PRESENCIALIDAD.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Página 12
resumen: 'Santa Fe: Rosario y San Lorenzo regresan a la presencialidad en la educación
  inicial y primaria'
title: " Santa Fe: Rosario y San Lorenzo regresan a la presencialidad en la educación
  inicial y primaria"
entradilla: La decisión se basó en mejoras en los índices epidemiológicos.

---
Las ciudades de Rosario y San Lorenzo, en Santa Fe, retomarán las clases presenciales a partir del próximo lunes, luego de percibir una mejora de los indicadores epidemiológicos con baja de contagios de coronavirus.

El ministro de Educación, Nicolás Trotta, su colega provincial, Adriana Cantero, y el gobernador de Santa Fe, Omar Perotti, informaron a través de una videoconferencia que en los niveles inicial y primario regresarán las clases en las aulas, con la aplicación de protocolos para garantizar el cuidado de la salud.

"La situación epidemiológica ya no es de alarma, sino que se enmarca en la categoría de riesgo alto, que permite el retorno administrado a las aulas, según la resolución 394 que se tomó el martes en el Consejo Federal de Educación de acuerdo a lo dispuesto por el DNU vigente", informaron desde el Ministerio de Educación nacional.

Trotta celebró que la decisión "confirma las medidas que hay que adoptar para priorizar desde los hechos y no desde los discursos la presencialidad escolar en el marco de la pandemia, siempre cuidando la salud de estudiantes, docentes, auxiliares y directivos".

"Santa Fe está dando una muestra de enorme responsabilidad en el tránsito de la pandemia. Este paso es muy trascendente porque sale del espacio especulativo para entrar en el espacio de la responsabilidad política", añadió el ministro.

En ese sentido, sostuvo que "San Lorenzo y Santa Fe permiten marcar un horizonte para otras jurisdicciones", al tiempo que remarcó que "lamentablemente, otras jurisdicciones incumplen las normas y desconocen los consensos construidos en el marco de una institución consagrada por la Ley de Educación Nacional como lo es el Consejo Federal de Educación".

En la misma línea, Trotta pidió a otros distritos que "adopten el camino acordado ya que nunca es tarde para reflexionar y desplegar medidas que permitan conjugar el derecho a la educación y a la salud".

El mensaje tiene como destinatarios a la Ciudad de Buenos Aires y varias ciudades mendocinas que se encuentran hasta el 21 de mayo en alerta epidemiológica, el nivel más alto de la escala según la cantidad de contagios y de camas de terapia intensiva en uso y donde por ello se aplican restricciones más severas, ya que ambos distritos mantienen la presencialidad en las aulas.

Durante la reunión virtual, los funcionarios reforzaron la importancia de analizar los datos objetivos del sistema sanitario y la curva de contagios para retomar de manera responsable la presencialidad en las aulas.

Además, desde la cartera de Educación resaltaron que "el regreso a las aulas en los niveles de la educación secundaria y de adultos será analizada al finalizar la semana epidemiológica entrante que coincide con el viernes 14 de mayo".

Otro de los ejes del encuentro fue la importancia de disminuir los encuentros sociales fuera de la escuela y las instancias de fiscalización de las restricciones reforzadas en otras actividades para cuidar a la escuela.