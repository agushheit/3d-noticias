---
category: Agenda Ciudadana
date: 2021-11-22T06:00:08-03:00
thumbnail: https://assets.3dnoticias.com.ar/ucrt.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: 'UCR: la búsqueda de nuevas autoridades estará centrada en la figura de un
  "presidenciable"'
title: 'UCR: la búsqueda de nuevas autoridades estará centrada en la figura de un
  "presidenciable"'
entradilla: 'El cambio será fundamental, ya que tanto el Comité Nacional como la Convención
  Nacional son las que definen las alianzas electorales y las candidaturas partidarias. '

---
Después de levantar el perfil dentro de la coalición Juntos por el Cambio, la UCR seguirá con elecciones internas en la que deberá renovar próximamente las autoridades del Comité Nacional y la Convención Nacional de ese partido, en un recambio que buscará darle las herramientas y empezar a perfilar las figuras que pelearán por más protagonismo dentro de JXC de cara al 2023.  
  
Este cambio de autoridades será fundamental ya que tanto el Comité Nacional como la Convención Nacional son las que definen las alianzas electorales y las candidaturas partidarias, y el radicalismo tiene fuertes expectativas de lograr que salga de sus filas el próximo candidato presidencial de Juntos por el Cambio (JXC), el frente que formó en el 2015 junto a la Coalición Cívica, PRO y sectores peronistas.  
  
En diciembre el exgobernador mendocino y senador electo Alfredo Cornejo debería dejar su puesto al frente del Comité Nacional y se deberá elegir una nueva conducción.  
  
**Los candidatos posibles**

Hasta el momento, el gobernador jujeño Gerardo Morales y el senador Martín Lousteau expresaron sus ambiciones de ocupar ese puesto, y hace meses juntan votos -entre los 96 delegados al comité de todas las provincias- para lograrlo.  
  
Esta semana se les sumó el gobernador correntino Gustavo Valdés, que después del amplio triunfo en su distrito se propuso como el candidato de consenso.  
  
Para sorpresa de varios, Morales (entre los radicales más críticos a Mauricio Macri) se reunió en los últimos días con la titular del PRO, Patricia Bullrich, y no descartaron compartir fórmula presidencial.  
  
En tanto la estrategia de Lousteau es avanzar desde su bastión en la UCR porteña hacia las provincias a través de su llegada a la militancia universitaria del partido, pero para eso deberá romper la desconfianza de los sectores que temen que resigne una candidatura presidencial de la UCR a cambio de una promesa del jefe de Gobierno porteño, Horacio Rodríguez Larreta, de que será su sucesor en la Ciudad.  
  
Por ahora la elección sería en diciembre, pero el sector de Lousteau pide postergarlo hasta marzo, buscando más voluntades.

> En los próximos días definiremos si la elección de nuevas autoridades partidarias será en diciembre o en marzo" 
>
> Alejandra Lordén, vicepresidenta Comité UCR

  
"En los próximos días definiremos si la elección de nuevas autoridades partidarias será en diciembre o en marzo. El objetivo es llevar adelante este proceso, junto a la definición de los liderazgos de los bloques legislativos, de forma transparente y sin sobresaltos, para seguir posicionándonos dentro de la coalición como una opción previsible e institucionalizada", aseveró a Télam la vicepresidenta del Comité, Alejandra Lordén.  
  
**El panorama electoral en el Congreso**

Como aclaró Lordén, junto al nuevo titular del partido se eligen los presidentes de los bloques, en una negociación simultánea que a veces otorga la presidencia a un sector y los bloques a otros.   
  
Hace varios períodos que el radical Mario Negri comanda el bloque de la UCR así como el interbloque de JXC en Diputados, y tanto su oponente en Córdoba, el diputado electo Rodrigo de Loredo como el aliado de Lousteau, el diputado Emiliano Yacobitti, salieron a desafiarlo.  
  
En el Senado el interbloque está en manos del formoseño Luis Naidenoff, y también hay aspirantes para ese puesto, como el propio Cornejo.  
Pero la disputa por comandar los interbloques de JXC (durante todo el macrismo en manos de los radicales) no es sólo de la UCR: en Diputados el PRO prometió hacer valer su peso numérico para buscar la presidencia.  
  
También la Convención Nacional de la UCR cambiará de manos: no reelegirán al chaqueño Jorge Sapia (cercano a Ángel Rozas), que durante todo su mandato pidió que su partido abandonara JxC y trajo varios dolores de cabeza a la dirigencia orgánica, por sus declaraciones contra sus socios y sus propios correligionarios.  
  
Hoy la dirigencia y las principales figuras del partido tienen ambiciones presidenciales, pero creen que sería casi imposible fuera del paraguas de JXC.  
  
"Por supuesto que 2023 está en la mira, demostramos que somos competitivos y que tenemos recursos humanos, valores e ideas que justifican nuestras aspiraciones. Pero ninguna interna está por encima del proyecto colectivo del radicalismo", dejó en claro Lordén.  
  
En la misma línea se había mostrado esta semana el propio Cornejo, que dijo que "el árbol no debe tapar el bosque" y la prioridad de esa fuerza política es "la unidad" de la coalición opositora, por lo que consideró que "no es momento" de instalar candidatos de cara a las presidenciales de 2023.   
  
"Nuestro mérito fue mantener unida a la oposición", dijo Cornejo esta semana en declaraciones periodísticas, consultado sobre el resultado del domingo último.  
  
Habrá que ver si todos en su partido piensan igual. En este marco los ojos se posan en Lousteau, el menos orgánico de sus correligionarios, y en el neurocientífico Facundo Manes, que aterrizó en la política seguro de su futuro como presidenciable.  
Esta semana justamente Manes repitió que la UCR buscará equilibrar la correlación de fuerzas y disputarle a sus socios del PRO la hegemonía del espacio, inalterada casi desde el mismo momento de la fundación de JxC.  
  
"La coalición 2015-2019 ya no existe más", dijo Manes, diputado electo por Juntos en la provincia de Buenos Aires tras enfrentar a la lista del PRO.   
  
En declaraciones periodísticas, afirmó que "el radicalismo se puso de pie" y si bien "antes había una dinámica de un solo color", habrá que "acostumbrarse a una coalición opositora con identidades diferentes".  
  
"Los radicales queremos liderar la coalición y vamos a competir en todas las categorías y todas las jurisdicciones", de cara a las presidenciales del 2023, avisó el neurocientífico, como para que no haya sorpresas.