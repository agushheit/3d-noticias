---
category: Estado Real
date: 2021-01-20T09:18:38Z
thumbnail: https://assets.3dnoticias.com.ar/sputnik.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de la Provincia de Santa Fe
resumen: 'Covid-19: Llegaron las segundas dosis de vacunas Sputnik V a la provincia
  de Santa Fe'
title: 'Covid-19: Llegaron las segundas dosis de vacunas Sputnik V a la provincia
  de Santa Fe'
entradilla: Permitirán completar el tratamiento de los equipos de salud que ya se
  aplicaron la primera dosis.

---
En el marco de la campaña nacional, y del mega operativo de vacunación histórico, este martes llegaron a la provincia de Santa Fe 12.300 vacunas Sputnik V de la segunda dosis que completa el tratamiento contra el Covid-19.

A la ciudad de Santa Fe, arribaron 500 vacunas que fueron recibidas en el hospital José María Cullen por el secretario de Salud, Jorge Prieto y el director del hospital Juan Pablo Poletti.

En la oportunidad, Prieto destacó “esto garantiza y da una seguridad a todos aquellos que voluntariamente han accedido a recibir la vacuna”. Además, agregó: “Recordemos que esta vacuna es un tratamiento. Hay que tener en cuenta son dos dosis consecutivas con un intervalo mínimo de 21 hasta los 60 días”.

En ese sentido, Juan Pablo Poletti destacó que “fue gratificante ver la aceptación que tuvo en todas las áreas covid y los servicios satélites de la terapia intensiva quienes fueron los primeros en vacunarse y ahora estamos todos ansiosos para poder completar la inmunización con esta segunda dosis que acaba de llegar y de esta manera que puedan trabajar más tranquilos”.

“Tenemos hasta el momento 850 vacunados con la primera dosis, y en esta primera tanda llegaron 500 vacunas con la segunda dosis. Asimismo quedaremos a la espera de las 350 restante para completar casi el 70% del personal activo que está trabajando y el 100% que se desempeña en el área covid del hospital”, continuó Poletti

“La vacuna es un componente más en esta lucha contra el covid, pero sin lugar a dudas y hasta que se pueda inmunizar al 70% de la población para poder cortar la cadena de contagios, tenemos que seguir cuidándonos con el distanciamiento social, el uso del tapa bocas, lavado de manos y sobre todo ser responsables porque esto no terminó, estamos en un pico alto y la única manera de que esto pase es cuidándonos”, concluyó el director del hospital.

**HOSPITAL PROVINCIAL DEL CENTENARIO DE ROSARIO**

Por otro lado, al hospital Provincial del Centenario de la ciudad de Rosario, llegaron esta mañana una tanda con 500 vacuna de la segunda dosis de Sputnik V.

Al respecto, la directora del efector, Claudia Perouch, indicó que “esta segunda dosis, a diferencia de la primera tanda, viene en frascos monodosis, lo cual simplifica la logística de vacunación. Afortunadamente tuvimos muy buena adherencia dentro del personal del hospital, llevamos vacunadas a 1060 personas”.

“Con esta tanda que acaban de llegar, empezaremos a vacunar el día 23 de enero, que justo se cumple el lapso de 21 días que se requiere”, añadió.

Finalmente y al ser consultada sobre la aceptación o no del personal a recibir la vacuna de manera voluntaria, la directora del hospital destacó que “al principio hubo mucha resistencia pero luego, a partir de ver que muchos compañeros y compañeras no tenían síntomas graves tras la inoculación de la primera dosis, comenzaron a aumentar los voluntarios a colocarse la vacuna”.

“En los sectores críticos tenemos una cobertura de 80-90% y a medida que pasan los días, gente que había planteado no vacunarse, ahora pide la vacuna. Esta educación y promoción que se hace entre los compañeros a servido para lograr esta adherencia que es muy buena y ojalá sigamos así”, concluyó Perouch.

![](https://assets.3dnoticias.com.ar/sputnik1.jpeg)