---
category: La Ciudad
date: 2021-04-28T07:54:13-03:00
thumbnail: https://assets.3dnoticias.com.ar/Lucas-Candioti-1.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Le aceptaron la renuncia al funcionario grabado en una fiesta clandestina
title: Le aceptaron la renuncia al funcionario grabado en una fiesta clandestina
entradilla: El ahora exsubsecretario de Comercio Exterior y Nuevas Tecnologías ya
  no integra el Ministerio de la Producción, luego de que se viralizaran imágenes
  de él bailando en un evento ilegal.

---
El último sábado, personal policial e inspectores municipales de Santo Tomé desbarataron una fiesta clandestina con 50 personas que se desarrollaba en un restó concesionado por el country Aires del Llano. 

Las imágenes del evento no tardaron en viralizarse, y entre ellas se podía observar a un funcionario provincial. Se trata de Lucas Candioti, quien ocupaba el cargo de subsecretario de Comercio Exterior y Nuevas Tecnologías del Ministerio de la Producción.

Según se supo, Candioti renunció por estas horas, y aunque en un primer momento se dudaba de que las autoridades le hayan aceptado la dimisión.

Si se lo hubiera mantenido en el cargo, las sanciones podían ir desde tres meses de trabajo voluntario en hospitales hasta el descuento del 50% del sueldo por un trimestre.