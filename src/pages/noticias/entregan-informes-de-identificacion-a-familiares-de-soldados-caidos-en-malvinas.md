---
category: Agenda Ciudadana
date: 2021-11-14T06:00:26-03:00
thumbnail: https://assets.3dnoticias.com.ar/INFORME.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Entregan informes de identificación a familiares de soldados caídos en Malvinas
title: Entregan informes de identificación a familiares de soldados caídos en Malvinas
entradilla: Tras un trabajo conjunto del Comité Internacional de la Cruz Roja y el
  Equipo Argentino de Antropología Forense, familiares de cinco soldados argentinos
  sepultados, recibieron el informe final que confirma su identidad.

---
El ministerio de Justicia y Derechos Humanos entregó a familiares de cinco soldados argentinos sepultados en la tumba C.1.10 del Cementerio Argentino de Darwin, de las Islas Malvinas, el informe final sobre la identificación de sus restos en el marco del Plan Proyecto Humanitario (PPH), iniciado por el Gobierno argentino en 2012.  
  
El informe de identificación fue entregado durante un acto realizado en la cartera de Justicia, a las familias del subalférez Guillermo Nasif; del cabo primero Carlos Misael Pereyra, y del Gendarme Juan Carlos Treppo, mientras que la hija del cabo primero Víctor Samuel Guerrero fue informada sobre la reasociación de los restos óseos de su padre, quien ya estaba identificado desde 1982.  
  
El Ministerio también comunicó la reconfirmación de identidad a la familia del primer alférez Ricardo Julio Sánchez, y se encuentra en proceso de entrega del informe sobre un sexto soldado cuyos restos también fueron identificados, según informó la cartera encabezada por Martín Soria en un comunicado.  
  
Los informes corresponden al trabajo realizado por el Comité Internacional de la Cruz Roja (CICR) y el Equipo Argentino de Antropología Forense (EAAF), que ya permitieron las identificaciones de 115 soldados caídos en combate y que permanecían en sepulturas sin nombre en las Islas Malvinas, y con la leyenda "Soldado argentino sólo conocido por Dios".  
  
El ministro destacó en el acto de entrega de los informes que "acompañar a las familias de nuestros héroes y brindar certeza sobre sus seres queridos es un compromiso de Estado" que "pudo llevarse a cabo por un esfuerzo nacional e internacional sin precedentes".  
  
Por su parte, el secretario de Justicia, Juan Martín Mena, sostuvo que "luego de muchos años de intenso trabajo, estamos concluyendo una nueva etapa en la más grata tarea de honrar a nuestros héroes nacionales y acompañar a sus familias con la correcta identificación de sus cuerpos".  
  
"Seguiremos trabajando para acercar certeza y el más alto reconocimiento a cada caído en combate por defender nuestro territorio y nuestra soberanía", expresó.  
  
En tanto, Carolina Guerrero, hija del cabo primero de Gendarmería, Víctor Samuel Guerrero, caído en Malvinas en un helicóptero abatido el 30 de mayo de 1982, manifestó estar "muy agradecida con el trabajo que se hizo" y dijo que "es muy importante para los familiares que no tenían la identificación total".  
  
"Mi hermano y yo dimos muestras de sangre para que pudieran cotejar con los restos que encontraron en tumbas colectivas, donde estaba el alférez Sánchez, que también estaba en el helicóptero en el que estaba mi papá", afirmó tras el encuentro que se hizo en el Ministerio.  
  
En ese sentido, expresó que "hoy, al ya tener los resultados, nos entregaron los informes de la Cruz Roja que lo confirman" y señaló: "me siento orgullosa de ser hija de Víctor Samuel Guerrero".  
  
"Con todas estas cosas que se hacen, siempre van a estar vivos nuestros héroes de Malvinas y tienen que ser recordados", sostuvo y señaló que su "papá siempre quiso ser gendarme, dio la vida por la Patria. Estas cosas cierran una herida, porque al corroborar que esos restos son de mi papá, sabemos fehacientemente que está ahí y que quedó en Malvinas".  
  
A su vez, Elsa Beatriz Cremona, esposa de Carlos Misael Pereyra, destacó que "con este informe casi que podemos cerrar el círculo".  
  
"Nos faltaría ahora poder viajar a las Islas a visitarlo, porque hoy sí tenemos un lugar para ir a llorarlo, porque durante 39 años no lo tuvimos", manifestó.  
  
En el acto de entrega de informes participaron, además, el responsable del equipo interdisciplinario que tuvo a cargo la implementación del PPH del ministerio de Justicia y Derechos Humanos, Mariano Flores y el jefe de la Delegación Regional del Comité Internacional de la Cruz Roja (CICR) para Argentina, Brasil, Chile, Paraguay y Uruguay, Alexandre Formisano.  
  
También estuvieron el representante para Argentina del CICR, Gabriel Valladares; el jefe de la Unidad Forense del CICR, Luis Fondebrider; los investigadores del Equipo Argentino de Antropología Forense, Carlos Somigliana y Virginia Urquizu; y la escribana adscripta de la Escribanía General de Gobierno, Vanina Capurro.  
  
La cartera de Justicia informó que de ese modo concluyó la segunda parte del PPH y recordó que se trata de una política de Estado que puso en marcha la entonces presidenta de la Nación Cristina Fernández de Kirchner cuando solicitó al CICR, a través de una carta enviada el 2 de abril de 2012, que actuara como intermediario neutral con el fin de ayudar a identificar 122 tumbas de soldados argentinos sepultados en el Cementerio de Darwin.  
  
En ese sentido, recordó que la expresidenta afirmó en esa oportunidad, en 2012: "He dirigido una carta al titular de la Cruz Roja Internacional para que tome las medidas pertinentes e interceda ante el Reino Unido para poder identificar a los hombres argentinos que no han podido ser identificados, porque cada uno merece tener su nombre en una lápida (…) Cada hermana, cada viuda, cada padre tiene ese derecho".  
  
De ese modo, en 2013 se constituyó un equipo interdisciplinario, bajo la coordinación del ministerio de Justicia y Derechos Humanos de la Nación, para llevar adelante la tarea, articular con los organismos involucrados y acompañar y asistir a las familias.