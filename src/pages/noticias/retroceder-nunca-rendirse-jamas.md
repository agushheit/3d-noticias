---
category: Deportes
date: 2021-04-13T07:02:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/union-boca.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Marisa Lemos
resumen: 'RETROCEDER NUNCA, RENDIRSE JAMAS '
title: 'RETROCEDER NUNCA, RENDIRSE JAMAS '
entradilla: 'En vísperas de sus 114 años el tatengue gano 1 a 0  a Boca y se acomodó
  nuevamente en la tabla de la zona 2 de la Copa de la Liga Profesional.

'

---
El tatengue recibio a Boca en territorio santafesino por la novena fecha de la zona 2 de la Copa de la Liga Profesional.En una tarde interesante, unión se impuso ante el xeneize con un gol del Chileno Peñailillo a los cuatro minutos del segundo tiempo.

En este partido se podría decir que cada uno tuvo su momento, durante el transcurso del primero tiempo fue superior el equipo porteño, con varias jugadas importantes pero nada relevante como para alertar a Moyano.Cumplido el primer cuarto de hora, las acciones seguían de la misma manera, con Unión maniatando los intentos de Boca, con mucho orden y concentración, y esperando que la visita "se venga" para tratar de sorprender con alguna contra, pero era cada vez más difícil..Desde los 20 minutos Boca comenzó a inquietar, sobre todo por las bandas, a través de las subidas de Buffarini por derecha y Fabra por izquierda, y el mayor contacto que Edwin Cardona tenía con la pelota.

Con un Unión muy aplicado, presionando constantemente y tratando de que todos sus jugadores estén siempre detrás de la línea de la pelota cuando la poseía el rival. Concentrado y sin perder el ritmo, Peñailillo marco el tanto de la noche a los 4 minutos del segundo tiempo, el cual  que le daría la victoria al tatengue después de varios partidos sin saborear la gloria 

No tardó mucho tiempo en llegar lo que Unión mostró apenas comenzó el complemento, por lo menos inquietando a la defensa visitante un par de veces en los primeros instantes.

El gol de Unión cambió totalmente el esquema del tecnico de Boca, ya que  tuvo que realizar cambios para tratar de mejorar la tactica y el planteo de su equipo.Cumplidos los 15 minutos del complemento, Boca buscaba con vehemencia llegar al empate, pero Unión lo esperaba muy bien parado (y movido) en defensa; y hasta con algunas chances propicias como para preocupar a la última línea de la visita. 

A los 34 minutos, Boca estuvo cerca de llegar al empate, cuando Tevez no llegó a tocar la pelota entrando por el segundo palo luego de un córner lanzado por Zárate desde la izquierda. Y a los 40, otra vez el "Apache" llevó peligro a la valla de Moyano, rematando desde 25 metros con la derecha, disparo que se fue apenas medio metro desviado, cerca del ángulo superior derecho del arco de las "Bombas". 

Eso fue lo último rescatable en cuanto a lo que se refiere al juego propiamente dicho. Porque a Boca le llegó la desesperación y a Unión se lo veía cada vez más tranquilo.

 Así llegó el final. Lo que todos pensaban que era una victoria asegurada para Boca llego unión para cambiarles la bocha. MORALEJA DE LA HISTORIA:”NUNCA CANTES VICTORIA ANTES DE TIEMPO”.