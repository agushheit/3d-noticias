---
category: La Ciudad
date: 2021-11-09T06:00:56-03:00
thumbnail: https://assets.3dnoticias.com.ar/barletta.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: LT10
resumen: 'Barletta: “Necesitamos jueces que apliquen la ley sin mirada ideológica”'
title: 'Barletta: “Necesitamos jueces que apliquen la ley sin mirada ideológica”'
entradilla: "Lidera la lista de Diputados Nacionales de Juntos por el Cambio en Santa
  Fe y participó del “Debate Abierto” que se realizó en la UNR.\n\n"

---
El ex intendente y primer candidato al Congreso Nacional de la lista de Juntos por el Cambio Santa Fe, participó este lunes del “Debate Abierto”, organizado por la Universidad Nacional de Rosario en el marco del Congreso Nacional e Internacional sobre Democracia.  
  
“Necesitamos jueces y fiscales capacitados que apliquen la ley sin mirada ideológica”, comenzó su descargo al ser consultado por sus propuestas en materia de seguridad. “Debemos reformar la justicia para garantizar el acceso igualitario a todos los ciudadanos. Una justicia eficiente simple y expeditiva que no facilite la impunidad de los corruptos. Con capacitación permanente y actualización de las herramientas tecnológicas de seguridad, incluyendo robótica e Inteligencia artificial".

Comentó que el primer proyecto que buscan materializar es el de “Ley Infancias Protegidas”, que agrava las penas a los delincuentes por el uso y la cooptación de menores para cometer delitos. Y proponen una investigación automática a partir de la primera denuncia y por violencia de género.

“Necesitamos urgente un despliegue de policías comunales, policía de cercanía, en las escuelas, paradas de colectivos, plazas, salidas de noche, para fortalecer la prevención y que los ciudadanos se sientan protegidos".

Mas tarde, al ser consultado por sus propuestas en materia de educación sostuvo que se debe “empezar por asegurar una buena alimentación porque en los primeros años de vida se desarrolla nuestro sistema neuronal. Un niño mal nutrido, es un niño que no aprende por eso proponemos la construcción de 9 plantas de Alimentos Nutritivos como la hecha hace unos años por la UNL. Así daríamos un gran golpe a la desnutrición y al hambre en todo el país.”