---
category: Agenda Ciudadana
date: 2021-03-29T06:15:56-03:00
thumbnail: https://assets.3dnoticias.com.ar/ganancias.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Ganancias: ¿Cuántos santafesinos dejarían de pagar el impuesto?'
title: Ganancias ¿Cuántos santafesinos dejarían de pagar el impuesto?
entradilla: Diputados dio media sanción a la iniciativa que eximirá de tributar ganancias
  a más de un millón de trabajadores en el país. ¿Qué impacto tendrá en Santa Fe?

---
Este domingo, en una sesión especial y maratónica, la Cámara de Diputados de la Nación dio media sanción al proyecto de reforma del Impuesto a las Ganancias.

Según el gobierno, más de un millón de trabajadores y 200 mil jubilados en el país dejarían de pagar ese tributo de promulgarse la ley (resta la aprobación del Senado)

 En la provincia de Santa Fe, un 63,5 por ciento de santafesinos (97.858 de personas) dejará de pagar ganancias. En el departamento La Capital por ejemplo el alivio fiscal alcanzaría a 17.152 personas, que representa un 61,8 por ciento de los contribuyentes que dejará de pagar en relación a los que el tributo alcanza desde el mes de enero de 2021, que son 27.773.

El proyecto eleva el mínimo no imponible a 150.000 pesos brutos mensuales, con lo cual a los trabajadores que cobren hasta 124 mil pesos netos de sueldo no se les descontará el impuesto en la Cuarta Categoría de Ganancias

De un total de 154.065 personas en el territorio provincial santafesino que pagan Impuesto a las Ganancias desde enero de 2021, 97.858 dejarían de pagar por el "nuevo piso" propuesto por el proyecto a tratarse, lo que representa un 63,5 por ciento.

Rosario encabeza la lista de departamentos con mayor número de personas que dejarían de pagar el tributo, con un total de 41.965, representa un porcentaje del 62,4 por ciento.

En ese ranking, lo sigue el departamento La Capital con 17.152 habitantes que dejarán de pagar el gravamen sobre 27.773 que lo vienen haciendo. En tercer lugar, se ubica el departamento Castellanos, con un número de trabajadores y jubilados alcanzados con la nueva ley de 5.878 personas, un 64,6 por ciento menos que los 9.092 que están pagando desde enero.

En términos porcentuales, donde tendría mayor impacto sería en el departamento San Javier, ya que un 75,8% dejaría de pagarlo (517 ciudadanos) Lo siguen el departamento San Justo con un 73% (1.012 de 1.386 personas) y el departamento General Obligado, en el norte de la provincia, con un 72,3%, con 3.422 habitantes alcanzados por el nuevo piso de $150.000 sobre 4.736 que pagan desde principios de año.