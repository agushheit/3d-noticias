---
category: Estado Real
date: 2020-11-28T13:37:18Z
thumbnail: https://assets.3dnoticias.com.ar/MPA.png
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: LT10'
resumen: Cuento del Tío
title: El MPA investiga 31 estafas con la modalidad del "cuento del tío"
entradilla: Recientemente, una mujer fue condenada en un juicio abreviado, y la misma
  suerte correrá un hombre la próxima semana. Podrían ser parte de la misma banda.

---
En un juicio abreviado, recientemente condenaron a 3 años en suspenso a una mujer de 23 años por el delito de asociación ilícita, en carácter de miembro. La mujer había sido detenida en Rafaela.

En tanto, un hombre de 29 años que integraría esta banda será juzgado el próximo lunes, también mediante un procedimiento abreviado. Interviene en ambos casos la fiscal, María Laura Urquiza.

La Unidad de Delitos Complejos del MPA investiga treintena casos similares por este tipo de estafas en lo que va del 2020. Hasta el momento, cuatro víctimas lograron recuperar parte del dinero que les fue sustraido mediante engaños. Tan sólo en noviembre, se registraron cuatro robos a personas, todos ellos adultos mayores, que fueron engañados para entregar sus ahorros.

### **CÓMO EVITAR ESTAS ESTAFAS**

Por correo electrónico o por whataspp: Los malintencionados envían información o mensajes que pueden parecer de fuentes confiables, empresas telefónicas, organismos estatales u organismos oficiales. 

Los mensajes pueden incluir archivos adjuntos o enlaces que descargan un virus informático en celulares o computadoras. 

De esta forma, los delincuentes se apropian de información personal, contraseñas o datos bancarios. La pandemia puede ser una oportunidad para los ciber-delincuentes. 

Algunos intentan obtener acceso a las cuentas de sus víctimas simulando ser asistentes que los ayudarán a cobrar los presuntos “bonos” del Gobierno Nacional u otras ayudas económicas. 

El requisito, dicen, es aportar su número de DNI y acceso a su cuenta de banco. Otro mensaje ofrece una improbable “ayuda alimenticia” de supermercados del país por motivo de cuarentena. 

Otros ardides incluyen falsos bancos que actualizan datos para postergar cobros de deudas. Los argumentos de estos engaños son amplios. Acceso gratis a Netflix, GB gratis para navegar por internet, cobros de servicios, recargas gratuitas en la tarjeta de transporte SUBE. 

El cambio de dólares “viejos” por billetes “nuevos” es una treta que se volvió a poner en marcha. Las llamadas nocturnas de aprovechan de la somnolencia de la víctima y del efecto sorpresa. En estos casos pueden hacerse pasar por bancos, empresas de telefonía, del ANSES o empresas que asignan premios. 

Otros fraudes y estafas incluyen la simulación de identidades de amigos o familiares que quieren “devolver” dinero con la finalidad de ingresar en los domicilios. Los llamados con advertencia de un pariente secuestrado, la exigencia del pago de un rescate son engaños que generan gran alarma entre sus víctimas.

### ¿Cómo actuar?

* Cortar y llamar al 911


* Nunca brindar datos personales, de tarjetas de créditos o cuentas bancarias.


* Los organismos públicos no se comunican telefónicamente para ofrecer subsidios. Tampoco ofrecen asesoramiento pago para la gestión de subsidios. La gestión es gratuita y personal en la entidad bancaria.
* Si el llamado comunica un premio, cortar y cotejar con la entidad en cuestión.
* No gestione créditos en los cajeros automáticos, ni transfiera dinero a desconocidos.
* No brinde a terceros las claves de acceso al cajero o al homebanking.
* En caso de los secuestros virtuales, recuerde que se trata de una estafa. Corte, denuncie al 911 y llame a su familiar para verificar que esté bien.
* En el domicilio: los delincuentes pueden simular que son empleados de empresas de servicios públicos. Si usted no llamó para acordar una entrevista no abra la puerta.
* Algunas bandas más organizadas montan verdaderas puestas en escena. Por eso es importante recordar: nunca abrir la puerta a extraños.

### Realizar la denuncia

Para combatir a las redes de estos delincuentes y oportunistas es muy importante que se realicen las denuncias. Estos delitos mutan. Para poder prevenirlos con mayor eficacia es muy importante contar con la información que aporten las víctimas: 

• Llamar al 911 (Sistema de emergencias).

• Centros Territoriales de Denuncias (CTD) para denunciar todo tipo de hechos delictivos. Por el momento están cerrados, pero abrirán cuando termine la medida de aislamiento obligatorio. 

• Ministerio Público de la Acusación (MPA): línea gratuita 0800-444-3583 para denunciar venta de drogas, tenencia ilegal de armas de fuego y mal accionar policial.