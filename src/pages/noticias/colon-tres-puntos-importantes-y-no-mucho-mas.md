---
category: Deportes
date: 2021-09-20T06:00:44-03:00
thumbnail: https://assets.3dnoticias.com.ar/COLO3.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Colón: tres puntos importantes y no mucho más'
title: 'Colón: tres puntos importantes y no mucho más'
entradilla: Colón volvió a la victoria superando a Central Córdoba por 1-0. El único
  gol del partido lo convirtió Rodrigo Aliendro a los 32' del primer tiempo.

---
Colón ganó y eso al fin y al cabo es lo más destacado. No fue un buen partido del equipo sabalero, pero le alcanzó para superar a un rival limitado. El gol de Rodrigo Aliendro con una dosis de fortuna abrió el marcador y no se modificó más para el definitivo 1-0.

Lo pudo empatar en el final el Ferroviario, pero surgió la figura de Leonardo Burián. Lo más destacado del equipo local se dio en el primer tiempo, pero fundamentalmente luego del gol de Aliendro. Pero en el complemento retrocedió demasiado y terminó sufriendo.

El inicio del partido mostró a los dos equipos con dinámica, Colón teniendo la pelota y Central Córdoba apostando a la contra. Por lo cual, los primeros minutos eran de ida y vuelta y si bien las opciones concretas de gol no aparecían, sí lo hacían las aproximaciones a las áreas.

Ambos equipos llegaban con facilidad hasta tres cuartos de cancha, pero no estan finos en los metros finales. El partido se jugaba de área a área y el mediocampo era de tránsito rápido. Así transcurría el primer cuarto de hora, con buenas intenciones, pero careciendo de eficacia en los metros finales.

Colón tenía más la pelota, pero no acertaba en el último pase. Y a veces se exponía a la contra del Ferroviario, que se agrupaba con mucha gente en campo propio y buscaba ataques directos con los los extremos. A los 23' probó Facundo Mura con un remate que controló César Rigamonti. Y a los 25' Milton Giménez cabeceó desviado.

Con el correr de los minutos el partido se hizo más deslucido, ya no era tanto de ida y vuelta. El juego se cortaba más, aunque siempre el dominador era el Sabalero. El equipo visitante renunciaba a tener el balón y el mismo era monopolio del equipo rojinegro.

Sin embargo, a los 30' Leonardo Sequeira picó por derecha, le ganó fácilmente en velocidad a Bruno Bianchi, pero su remate se fue cruzado. Pero el Rojinegro. a los 32' reaccionó rápidamente con un disparo de Rodrigo Aliendro de media distancia que se desvió en Oscar Salomón y decretó el 1-0.

A Colón que le costaba ingresar al área, comenzó a resolver el partido con un remate de media distancia y con una dosis de fortuna. Y a partir de ese gol, el equipo local se fue soltando, dominando absolutamente a su rival y merodeando el segundo gol.

Ya en tiempo de descuento, el que probó desde media distancia fue Cristian Ferreira con un disparo que Rigamonti desvió al córner. Colón terminó justificando de sobre manera el triunfo parcial, ya que después del gol de Aliendro dominó a voluntad y por muy poco no anotó el segundo.

En el inicio del segundo tiempo y con la obligación de ir a buscar el empate, el Ferroviario se adelantó unos metros y comenzó a jugar más cerca del área rojinegra. Sin inquietar a Leonardo Burián, pero siendo más peligroso que en la primera etapa. Le faltaba concretar en los metros finales.

Colón tenía menos el balón, pero a su vez contaba con más espacios en ataque. Y eso sin dudas que era una ventaja como para llegar al segundo gol. A los 16' intentó Ferreira con un disparo cruzado, luego de un pase de Aliendro. Le faltaba mayor precisión ante una defensa que otorgaba ventajas.

Cuando se jugaban 19' Central Córdoba tuvo una chance clarísima para empatar. Sequeira metió un centro y Milton Giménez se elevó solo dentro del área y el balón se fue por encima del horizontal. Colón retrocedía mucho y el equipo visitante se animaba, más allá de sus limitaciones.

El partido era abierto y Colón cuando se lo proponía estaba cerca de anotar el segundo. A los 36' Christian Bernardi hizo una buena maniobra, dentro del área y cuando tenía todo para definir su remate se fue desviado. Fue la opción más clara del Sabalero en la etapa complementaria.

Y ya en el final, Burián con una tapada extraordinaria le impidió el empate al Ferroviario. A los 42' el centro desde la derecha, la peinó Giménez y dentro del área chica remató Claudio Riaño. El balón rebotó en el pecho del arquero uruguayo y el balón se fue al córner.

No hubo tiempo para más, el Sabalero terminó jugando con cinco defensores, resignó futbolistas en ofensiva y terminó defendiendo dentro de su área. Ganó por aquel gol de Aliendro que se desvió en Salomón, pero a lo largo de los 90' no dejó la mejor imagen.

Fue una victoria importante, quizás no importaba tanto las formas, sino el resultado. Después de cinco partidos sin ganar, era primordial volver a sumar de a tres. Lo hizo, aunque no le sobró nada, por lo cual tendrá que seguir mejorando, pero siempre es mejor hacerlo con un buen resultado.

**Síntesis**

Colón: 1-Leonardo Burián; 4-Facundo Mura, 6-Paolo Goltz, 2-Bruno Bianchi, 40-Rafael Delgado; 23-Christian Bernardi, 14-Federico Lértora, 29-Rodrigo Aliendro, 11-Alexis Castro; 16-Cristian Ferreira y 35-Facundo Farías. DT: Eduardo Domínguez.

Central Córdoba: 23-César Rigamonti; 25-Gonzalo Bettini, 4-Oscar Salomón, 6-Franco Sbuttoni, 12-Jonathan Bay; 5-Cristian Vega, 18-Jesús Soraire; 7-Leonardo Sequeira, 27-Carlo Lattanzio, 21-Lucas Brochero y 45-Milton Giménez. DT: Sergio Rondina.

Goles: PT 32' Rodrigo Aliendro (C),

Cambios: ST 0' Silvio Martínez x Brochero (CC), 22' Yeiler Góez x Ferreira (C), 32' Claudio Riaño x Sequeira (CC), Lucas Melano x Lattanzio (CC), 38' Facundo Garcés x Bernardi (C), 40' Juan Galeano x Soraire (CC), Abel Argañaraz x Vega (CC).

Amonestados: Bernardi, Farías, Lértora, Lattanzio, Salomón,

Árbitro: Darío Herrera.

Estadio: Brigadier López.