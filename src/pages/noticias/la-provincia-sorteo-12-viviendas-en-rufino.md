---
category: Estado Real
date: 2021-06-27T06:00:33-03:00
thumbnail: https://assets.3dnoticias.com.ar/RUFINO.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Provincia sorteó 12 viviendas en Rufino
title: La Provincia sorteó 12 viviendas en Rufino
entradilla: Las viviendas son de dos dormitorios y cuentan con acceso a todos los
  servicios básicos. El monto de ejecución de la obra supera los 33 millones de pesos.

---
El gobierno provincial, a través de la dirección provincial de Vivienda y Urbanismo (DPVyU), realizó este viernes el sorteo correspondiente a las 12 viviendas de Rufino, departamento General López. El acto se realizó en la Escuela Técnica Nº 286 “Gerónimo Segundo Rufino”, ante la presencia de escribanos públicos de la provincia, y se transmitió en vivo por las redes sociales del Ministerio de Infraestructura, Servicios Públicos y Hábitat.

En la oportunidad, a través de un mensaje grabado, la ministra de Infraestructura, Servicios Públicos y Hábitat, Silvina Frana, resaltó "la esperanza que produce el sueño de acceder a una vivienda, a un hábitat digno. Estas son las políticas que nos alegran y que nos hacen trabajar con un fuerte compromiso por nuestra gente, a pesar de todos los inconvenientes, pero no perdiendo de vista para qué estamos en la función pública: para servir a todos los santafesinos y santafesinas".

A su turno, el secretario de Hábitat, Urbanismo y Vivienda, Amado Zorzón, indicó que "llegar a esta instancia donde se van a sortear 12 viviendas tiene que ver con la política pública establecida por el gobierno de la provincia, a través de la cual el gobernador Omar Perotti nos encomendó que trabajemos para lograr la mayor cantidad posible de intervenciones, que permitan resolver el tema de la vivienda y el hábitat dignos en cada una de las localidades de la provincia".

Por su parte, la delegada política de la dirección de Vivienda en Rosario, Estrella Scolari, manifestó que "la cantidad de inscriptos en este sorteo nos indica que cada vez son más las familias que quieren arraigarse en su ciudad o comuna. Esto representa un compromiso para nosotros como gobierno, ya que nos impulsa a seguir trabajando en la construcción de más viviendas para que más santafesinos y santafesinas accedan a su casa propia".

**PRESENTES**

Del acto participaron también el intendente de Rufino, Natalio Lattanzi; la concejal Daniela Robles y el concejal Mauro Marcos, entre otras autoridades.

**EL SORTEO**

El sorteo se llevó a cabo respetando los cupos establecidos por la DPVyU: dos para el cupo para personas con discapacidad (una de ellas, motriz), una para bomberos voluntarios, dos para fuerzas de seguridad, una para demanda general con antigüedad, y las seis restantes para demanda general. Al mismo tiempo, se sortearon los cupos de familias suplentes para los casos que no cumplan con los requisitos establecidos.

Cabe destacar que las familias que resultaron pre-adjudicatarias, a partir de ahora, deberán presentar la documentación que acredite la veracidad de los datos declarados en su ficha de inscripción del Registro Digital de Acceso a la Vivienda. Las mismas serán citadas por la DPVyU en día y horario a definir en los correos electrónicos que declararon al momento de inscribirse.

Los resultados se publicarán en los próximos días en el sitio oficial de la provincia [http://www.santafe.gob.ar/](http://www.santafe.gob.ar/ "http://www.santafe.gob.ar/") [https://www.santafe.gov.ar/index.php/web/content/view/full/235911/](https://www.santafe.gov.ar/index.php/web/content/view/full/235911/ "https://www.santafe.gov.ar/index.php/web/content/view/full/235911/")(subtema)/235903.

**LAS VIVIENDAS**

Las viviendas son de dos dormitorios, de las cuales 11 son prototipo VC –vivienda común- y la restante corresponde a la vivienda adaptada para personas con discapacidad, prototipo VCD. Se encuentran ubicadas en la intersección de las calles Sarmiento, J. Moran, A.Illia y General López.

Las primeras 11 son de 60 metros cuadrados, constan de dos dormitorios, cocina - comedor, lavadero y baño, con instalaciones de agua fría y caliente en baño, cocina y lavadero, con tanque de reserva de 500 litros instalado en una torre contigua a la vivienda. La vivienda del prototipo VCD 2D, desarrollada en un terreno de “esquina” de 25 metros de frente por 30 metros de fondo, adaptada para discapacidad motriz con desplazamiento en silla de ruedas.

Finalmente, todas poseen instalación eléctrica en todos los ambientes, lo mismo para el gas envasado en cocina, calefón y calefactor, instalaciones para agua potable (fría y caliente) y desagües cloacales a pozo absorbente e instalación pluvial a cordón. Además, cuentan con calefones solares para ahorro energético y cuidado ambiental.