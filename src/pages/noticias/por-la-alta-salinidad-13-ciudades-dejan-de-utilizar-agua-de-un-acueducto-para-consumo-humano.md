---
category: Agenda Ciudadana
date: 2021-08-10T06:00:22-03:00
thumbnail: https://assets.3dnoticias.com.ar/tequilaysal.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Por la alta salinidad, 13 ciudades dejan de utilizar agua de un acueducto
  para consumo humano
title: Por la alta salinidad, 13 ciudades dejan de utilizar agua de un acueducto para
  consumo humano
entradilla: La recomendación fue elevada por el Enress. Se trata del acueducto centro
  oeste, que se abastece del río Coronda. Deberán volver al consumo mediante fuentes
  subterráneas.

---
Los últimos controles del Ente Regulador de Servicio Sanitarios (Enress) determinaron un incremento en la salinidad de aguas del río Coronda, afluente del cual se nutre el acueducto Centro - oeste; conducto que es operado por Aguas Santafesinas S.A. para servir de agua a 13 localidades.

Los registros de sales naturales disueltas sobre dicho río fueron mayores a los obtenidos en otros puntos de abastecimiento. A partir de esta situación, el organismo recomendó que esas poblaciones, que reunen a más de 70 mil personas, dejen de utilizar ese servicio para consumo humano. El fenómeno obedece a la bajante del Paraná y al mayor aporte de los saladillos.

A las 13 localidades que por estos días volvieron a abastecerse de agua subterránea, se le suma Coronda (que también se nutre del río homónimo, pero no mediante el acueducto). Ellas son: Monje, Díaz, San Genaro, Clason, Centeno, Totoras, Las Rosas, Las Parejas, Bouquet, Montes de Oca, María Susana y Los Cardos.

La directora del Enress, Anahí Rodríguez, le dijo a UNO Santa Fe que hubo un pedido a esos gobiernos, municipales y comunales, "de volver a implementar los sistemas de tratamiento de agua subterréna, que eran los sitemas que estaban previstos antes de la puesta en servicio del acuducto centro-oeste".

Explicó quela medida es "a los fines de morigerar estos desvíos en algunos parámetros de calidad que nosotros hemos notado y ante la imposibilidad de que el actual escenario se pueda corregir de manera inmediata".

Algunas localidades, como el caso de Coronda, ya iniciaron con el operativo de distribución de agua en bidones. Fue necesario que la provincia disponga en esa ciudad costera una planta de ósmisos inversa portátil para garantizar la provisión.

La responsable el Enress aclaró que se trata de agua para el consumo diario: "Es agua destinada a nutrición y cocción de alimentos". Advirtió que, para el resto de las tareas, como el aseo personal, se podrá seguir utilizando el agua proveniente del acueducto.

Destacó que esta situación permanecerá "hasta que la autoridad de aplicación o Aguas Santafesinas pueda corregir estos desvíos en los parámetros de calidad o bien, en el caso de que no sea posible, volver a utilizar los pozos (fuente subterránea) y proveer a la ciudadanía de cinco litros por día por persona para consumo humano".

Indicó que fueron notificados todos los "prestados; Assa, que es quien opera el acueducto, y también la autoridad de aplicación, que es el Ministerio de Infraestructura".

El director de Assa, Hugo Morzan, subrayó "que se están monitoreando todas las localidades". Quien es además el presidente de la Comisión de seguimiento de bajante extraordinaria del Río Paraná, detalló que se realiza un seguimiento "del aforo de los arroyos a través de la secretaría de asuntos hídricos". Y aclaró que "Assa, como prestadora mayorista de agua, realiza todos los controles de conductividad".

Hay una especial atención sobre María Susana y Los Cardos, localidades que también fueron alertadas por el Enress debido a que no tienen distribución de agua a no ser por el acueducto. En ese sentido, Morzán informó: "Estamos viendo si en vez de realizar una perforación, osmosamos el agua que directamente nosotros llevamos a través del acueducto; se iba a resolver hoy (por el lunes) técnicamente; porque hay que ver dónde desechar el agua de la ósmosis que tiene alta salinidad".

Morzán apuntó que el objetivo es poder darle a esas poblaciones, que hoy cuentan con un servicio de agua con alta salinidad, la alternativa de no tener que comprar un agua envasada, y que tengan el agua osmosada, correspondiente.

Consultado sobre cuánto tiempo podría demorar en normalizarse el servicio, el responsable de Assa dijo: "Es una situación, al igual que la bajante, inédita y no tenemos antecedente. En 1944 teníamos niveles similares a estos (por la bajante); sobre la salinidad no tenemos antecedentes porque no teníamos el acueducto en Monje en ese momento. Vamos notando que hubo un pico de alta salinidad que hoy está un poco más atenuado, sigue siendo alto, pero más atenuado. Nosotros lo que estamos haciendo es verificar esta situación porque entendemos que, de persistir la bajante del río, persistiría esta situación de alta salinidad y por eso estamos buscando alterantivas para el consumo".