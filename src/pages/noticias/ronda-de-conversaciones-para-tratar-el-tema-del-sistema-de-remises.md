---
category: Agenda Ciudadana
date: 2021-03-24T08:31:52-03:00
thumbnail: https://assets.3dnoticias.com.ar/Foto-de-Ivana-Carino-1.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Concejo Santo Tomé
resumen: Ronda de conversaciones para tratar el tema del sistema de remises
title: Ronda de conversaciones para tratar el tema del sistema de remises
entradilla: Los y las ediles del concejo se reunieron para iniciar con las rondas
  de consultas para tratar la regulación del servicio de transportes de remises.

---
Con la intención de empezar a trabajar en la modificación de la Ordenanza vigente  que regula el servicio de remises de la ciudad, esta mañana se reunieron las concejalas y concejales del cuerpo junto con José Roura,  Presidente del Órgano de Control de Transporte Público de la ciudad de Santa Fe; y Sergio Ludeña, miembro de CETRAMP (Centro de estudios de Transporte) y ex Secretario de Transporte de la ciudad de Santa Fe. El objetivo fue iniciar rondas de consultas y establecer un intercambio de experiencias en materia de regulación del servicio de transporte de remises de la ciudad.

Al respecto la Presidenta del Concejo, Gabriela Solano, señaló: “el año pasado adquirimos el compromiso de trabajar sobre el sistema de remises que tiene nuestra ciudad, con algunas particularidades. Lo que nos preocupa es el aumento de las tarifas que se va dando a lo largo del año.  Agradezco a las gestiones de la concejala Florencia González, quien contactó a Roura y Ludeña para tomar como referencia su experiencia en la vecina ciudad y de esta manera empezar a trabajar en conjunto con el cuerpo legislativo del Concejo, en relación a la tarifa del servicio y diferentes cuestiones que hacen a la regulación del servicio”.

En ese sentido, la concejala Florencia González (UCR – Reformista) expresó: “esta es una deuda que tenemos con los vecinos y vecinas de la ciudad, así que queremos empezar por discutir la tarifa que tiene este servicio y distintas cuestiones que tienen que ver con la problemática diaria relacionada con el transporte”.  

De esta manera, el Concejo Municipal de la ciudad empezará a trabajar para establecer una fórmula polinómica, que contemple el conocimiento y experiencia de los protagonistas, que permita elaborar una metodología de cálculo y que atienda a los conceptos que deban ser tenidos en cuenta para la elaboración de costos y así establecer el valor tarifario del servicio de remises en la ciudad. 

El tema se tratará de manera conjunta con el cuerpo legislativo y, además de regular tarifas, prevé trabajar en cuestiones como sistema de licencias y habilitaciones, antigüedad de los trabajadores y trabajadoras, entre otras.