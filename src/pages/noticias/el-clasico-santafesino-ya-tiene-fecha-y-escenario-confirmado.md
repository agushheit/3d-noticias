---
category: Deportes
date: 2021-06-30T08:49:51-03:00
thumbnail: https://assets.3dnoticias.com.ar/clasicojpg.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: El Clásico Santafesino ya tiene fecha y escenario confirmado
title: El Clásico Santafesino ya tiene fecha y escenario confirmado
entradilla: La Liga Profesional reveló el fixture del nuevo torneo, donde quedó establecido
  en qué fecha y estadio se realizara el Clásico Santafesino entre Colón y Unión

---
El fixture del nuevo campeonato de la Liga Profesional (LPF) se sorteó este martes con el regreso del formato de todos contra todos a una sola rueda de 25 fechas. Algo que generaba muchas expectativas en Colón y Unión para saber cuándo se iba a concretar una nueva edición del Clásico Santafesino. Uno de los detalles en este caso, es que se invierte la localía, por lo que será finalmente en el estadio 15 de Abril, ya que el último fue en el Brigadier López (1-1).

Luego de dos torneos con formato de Copa, el campeonato volverá a disputarse como una liga de 25 fechas que se desarrollará entre el 16 de julio y el 12 de diciembre. El calendario tiene fechas con partidos entre semana y no se suspenderán duelos por las ventanas FIFA que deberá afrontar la Selección Argentina por las Eliminatorias.

La novedad está en la implementación del VAR. Al igual que sucedió en la última Copa de la Liga Profesional, los partidos contarán para los promedios del descenso, que volverán a implementarse partir de 2022.

Cuando finalice el campeonato, el ganador jugará una final por el "Trofeo de Campeones" ante Colón, que se adjudicó la última Copa de la Liga Profesional tras vencer a Racing por 3-0 en la final.

La clasificación a las copas internacionales se regirá por la tabla acumulada, que ya tiene computados los 13 partidos correspondientes a la primera fase de la última Copa. Los primeros cuatro de la tabla acumulada ingresarán a la Copa Libertadores 2022 y del quinto al décimo puesto ganarán una plaza a la Sudamericana 2022.

Colón ya tiene asegurado el pasaje a la Libertadores 2022 y Banfield lo propio para la Sudamericana, tras derrotar a Vélez en el desempate de la Copa Diego Maradona. Boca es el otro equipo que tiene cerrado, al menos, su participación en la Copa Sudamericana 2022 por haber ganado la Superliga 2019/20 (48 puntos en 23 fechas) y la posterior Copa Maradona (derrotó a Banfield por penales en la final).

Lo importante en este caso es que el Clásico Santafesino entre Unión y Colón se concretará en la 25ª y última fecha –al igual que en la Copa de la Liga Profesional, donde levantó críticas– en el estadio 15 de Abril. Sería el fin de semana del 12 de diciembre.