---
category: Agenda Ciudadana
date: 2020-12-06T11:49:18Z
thumbnail: https://assets.3dnoticias.com.ar/belgrano.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: La provincia solicitó la custodia de 30 hectáreas de terrenos urbanizables
  en la capital provincial
title: La provincia solicitó la custodia de 30 hectáreas de terrenos urbanizables
  en la capital provincial
entradilla: Actualmente están bajo la órbita de la Agencia de Administración de Bienes
  del Estado y pasarían a manos de la provincia con fines urbanizables.

---
El gobierno de la provincia, a través del Ministerio de Infraestructura, Servicios Públicos y Hábitat, solicitó a la Agencia de Administración de Bienes del Estado (AABE), la custodia de 30 hectáreas en la zona de la Estación Belgrano y en los barrios Cabal y Los Troncos, para realizar tareas de mantenimiento, vigilancia y urbanización.

En este sentido, la ministra de Infraestructura, Servicios Públicos y Hábitat, Silvina Frana, comentó acerca de las gestiones efectuadas con Nación para que la provincia tome la custodia de terrenos en la ciudad de Santa Fe: “La urbanización de los barrios populares y el registro de terrenos fiscales son algunas de las prioridades que el gobernador Omar Perotti nos pidió desde el inicio de la gestión, con el objetivo de elaborar políticas concretas para comenzar a saldar el déficit habitacional a través de diferentes programas -Santa Fe sin ranchos, Lote Propio, Núcleos Básicos, RUTFU- y equilibrar los territorios con la infraestructura necesaria y accesos dignos a la educación, la salud y los servicios públicos”.

“A través de la Secretaría de Hábitat, gestionamos con el AABE la custodia de terrenos muy importantes para el desarrollo urbanístico de la ciudad. 

Ya completamos toda la documentación y estamos a la espera de las resoluciones nacionales y posterior firma de convenios para que la provincia tenga la custodia de los terrenos en barrios Candioti Norte, Cabal y Los Troncos”, indicó el secretario de Hábitat, Urbanismo y Vivienda, Amado Zorzón. 

Y agregó: “A partir de la custodia, la provincia podrá hacerse cargo del mantenimiento y vigilancia de los terrenos, evitando futuros asentamientos y proyectar desarrollos urbanísticos de barrios populares y nuevos planes de viviendas”, aseguró.

El funcionario se mostró optimista sobre el visto bueno de las autoridades nacionales ante la solicitud de custodia de las tierras fiscales y adelantó que a corto plazo se estaría firmando el convenio. 

Pero evitó dar precisiones acerca de la ubicación de los terrenos para no alentar posibles asentamientos ni especulaciones, dado la sensibilidad del tema y la enorme necesidad que genera el déficit habitacional actual.