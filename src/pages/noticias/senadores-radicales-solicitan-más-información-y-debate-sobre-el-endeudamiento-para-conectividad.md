---
layout: Noticia con imagen
author: .
resumen: Santa Fe + Conectada
category: Estado Real
title: Senadores radicales solicitan más información y debate sobre el
  endeudamiento para conectividad
entradilla: Felipe Michlig objetó la falta de precisión y detalles del mensaje
  del Poder Ejecutivo que solicita autorización para endeudarse para el
  financiamiento de "Santa Fe + Conectada".
date: 2020-11-11T23:07:42.927Z
thumbnail: https://assets.3dnoticias.com.ar/michlig.jpg
---
El presidente del bloque de Senadores de la UCR, Felipe Michlig, objetó la falta de precisión y detalles del mensaje del Poder Ejecutivo que solicita autorización para endeudarse por la suma de 100 millones de dólares con el Banco de Desarrollo de América Latina (CAF), para el financiamiento del Programa de Inclusión Digital y Transformación Educativa “Santa Fe + Conectada”, al momento que explicó las razones de la abstención –en la sesión de este jueves– de los siete legisladores radicales, mientras que con el voto afirmativo de 11 senadores del justicialismo el proyecto obtuvo media sanción.

El representante de la bancada radical, en su calidad de miembro informante expresó: “No nos oponemos al proyecto de conectividad y a la necesidad de conseguir financiamiento para invertir con ese objetivo (por ello los siete senadores de la UCR votamos en la sesión anterior a favor de la preferencia para que el tema sea tratado hoy), sino que estamos objetando la forma de encarar el endeudamiento por parte del Poder Ejecutivo, que nos llevó a abstenernos en esta oportunidad, con la convicción de que en Diputados se abrirá un mayor debate y, en todo caso, el proyecto volverá en revisión a esta Cámara”.

Tal posición es compartida por el conjunto de los senadores radicales entre ellos: Felipe Michlig (San Cristóbal), Lisandro Enrico (General López), Rodrigo Borla (San Justo), Orfilio Marcón (General Obligado), Germán Giacomino (Constitución), Hugo Rasetto (Iriondo) y Leonardo Diana (San Jerónimo).

**Objeciones**

En tal sentido Michlig fundamentó pormenorizadamente la posición radical enumerando las siguientes razones: el Poder Ejecutivo no construyó espacios de diálogo; el CAF todavía no autorizó el préstamo y no existe certeza de cuáles serían las condiciones financieras definitivas del préstamo; muy bajo nivel de detalle en cuanto a la instrumentación. No se está hablando de una obra puntual sino de una serie de inversiones elásticas, y tal autorización resultaría  una especie de “cheque en blanco”; se proponen redes de fibras en tramos donde hay otras redes que se encuentran subutilizadas con una inversión redundante; y la posibilidad de incrementar  hasta un 20% el monto, es decir, otros 20 millones de dólares.

“Además, vemos que se propone conectar a las ciudades, pero no a todo el territorio provincial, omitiendo la ruralidad y las necesidades de infraestructura que requieren las actividades productivas que se desarrollan en este ámbito”.

“Un aspecto no menor tiene que ver con que del endeudamiento propuesto, solo 64 millones de dólares se destinarán a obras de redes. Unos 30 millones se destinarán para obras y equipamiento escolar. Pero unos seis millones se destinarán para contratos profesionales, capacitación, asesoramiento, auditoría, evaluación” remató Felipe Michlig.

**Mejorar la conectividad**

Por otra parte, el representante del departamento San Cristóbal agregó: “En principio no objetamos la necesidad de llevar a cabo inversiones en lo que implique mejorar la conectividad a lo largo y a lo ancho de la provincia. Más aún si es para superar asimetrías existentes entre las zonas más densamente pobladas y las localidades del interior provincial”.

“Compartimos el interés del gobierno provincial en acompañar y desarrollar acciones para el mejoramiento de las redes existentes. Consideramos estratégico el rol que los servicios TIC están brindando actualmente a la sociedad, más en el contexto de pandemia en el que nos encontramos. Es aquí donde consideramos que el Estado debe llegar y participar activamente  en los lugares donde todavía no se haya desarrollado, o donde todavía no se hayan construido nuevas redes”.

**Mayor claridad**

“Tampoco tenemos pruritos con la toma de financiamiento para concretar proyectos de infraestructura cuando las mismas tienen alto impacto social, cuando haya claridad de qué se tratan esos proyectos; cuando hubo diálogos y consensos entre oficialismo y oposición para definir los componentes y las obras que se van a financiar, cuando el financiamiento tiene las condiciones ya fijadas, cuando se tiene información sobre el estado financiero de la provincia, si los endeudamientos ya autorizados fueron tomados y cuando no se piden cheques en blanco”.

[](<>)“En este caso hay un propósito que nadie puede objetar, pero hay ausencia de otros aspectos y esa ausencia es muy crítica. Quizá esa ausencia podría haberse allanado si el Poder Ejecutivo construía los espacios de diálogo para agregar información que hoy no se tiene, clarificar ciertos aspectos y aventar algunas dudas, consensuar obras y otros aspectos más”, remarcó Michlig.