---
category: Agenda Ciudadana
date: 2022-05-06T14:35:53-03:00
thumbnail: https://assets.3dnoticias.com.ar/2022-05-06NID_274599O_1.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: SECUESTRARON ARMAS Y DROGAS EN EL MARCO DE UNA CAUSA QUE INVESTIGA AMENAZAS
  AL INTENDENTE DE VENADO TUERTO
title: SECUESTRARON ARMAS Y DROGAS EN EL MARCO DE UNA CAUSA QUE INVESTIGA AMENAZAS
  AL INTENDENTE DE VENADO TUERTO
entradilla: El allanamiento se produjo en el interior de un estacionamiento de esa
  ciudad. En la causa quedó imputado un hombre cuyas iniciales son M.N.N.

---
Personal de la Policía de la Provincia realizó un allanamiento en un estacionamiento en la ciudad de Venado Tuerto donde hallaron armas de fuego y municiones, entre otros elementos que fueron secuestrados por ser considerados de interés en la causa que investiga la denuncia de abuso de arma de fuego y amenazas contra el intendente de esa ciudad, Leonel Chiarella.

El procedimiento, realizado a requerimiento de la fiscalía regional, se concretó este jueves en el inmueble ubicado en Avenida Santa Fe, entre calles Las Heras y Esperanto.

Allí se realizó una requisa en la que se secuestraron cuatro armas de fuego calibres 9mm, 45mm y 22mm; 700 cartuchos de diversos calibres, entre ellos 9mm, 45mm, 22mm y 380; nueve cargadores; un cargador especial de 50 cartuchos; documentación de vehículos; y un vehículo marca Volkswagen Bora de color gris, dominio EKK-887.

También se incautaron dos barriles de 25 kilos cada uno conteniendo en su interior una sustancia blanquecina, polvorienta, identificada según su etiqueta como Lidocaine Hidrocloride UCP; dos bidones de 10 litros de Fenoclor Activador; tres botellas de líquido ACT de uso profesional de 1.000ml; cinco latas metálicas de líquido sin identificar; dos balanzas de precisión, una grande y una manual; una prensa; dos moldes de hierro, uno con placa L.N. y el otro con placa con dibujo de una corona; una estructura de hierro; tres bolsas pequeñas conteniendo una sustancia blanquecina, polvorienta; dos gatos hidráulicos; un taco de madera y un trozo de perfil doble, jarras plásticas, embudos, licuadora y pulverizadores.

En la causa quedó imputado un hombre cuyas iniciales son M.N.N.