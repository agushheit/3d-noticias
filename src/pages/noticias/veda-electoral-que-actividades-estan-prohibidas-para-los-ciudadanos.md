---
category: La Ciudad
date: 2021-09-11T06:15:25-03:00
thumbnail: https://assets.3dnoticias.com.ar/VEDA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Veda electoral: ¿qué actividades están prohibidas para los ciudadanos?'
title: 'Veda electoral: ¿qué actividades están prohibidas para los ciudadanos?'
entradilla: Desde las 8 de la mañana de este viernes cesó la campaña proselitista.
  Los candidatos y los partidos ya no pueden difundir sus propuestas.

---
Según establece la Cámara Nacional Electoral (CNE), a partir de este momento los partidos políticos no podrán efectuar ningún acto de campaña hasta luego del escrutinio, que arrancará el domingo a partir de las 18. Pero también se verán afectados los ciudadanos en algunas actividades.

Según información de la CNE, la veda electoral alcanzará a todo tipo de propaganda política de candidatos, funcionarios o comunicadores sociales, como la difusión de encuestas y sondeos preelectorales. En ese sentido, las encuestas ya no se pueden informar desde este sábado pasado.

Además, y más allá de que estamos transitando la pandemia de coronavirus Covid-19, el Código Electoral Nacional prevé la prohibición de todo tipo de espectáculo masivo (cultural, social o deportivo) durante el domingo de elecciones y hasta tres horas después de finalizado el proceso electoral, es decir, a las 21.

Por otro lado, la venta de bebidas alcohólicas estará prohibida desde las 20 del próximo sábado (12 horas antes de la apertura de urnas) y hasta tres horas de finalizado el comicio.

Los ciudadanos que no respeten la veda podrán ser denunciados por violación al Código Electoral y sancionados, si la Justicia comprueba la falta.

El Código prohíbe además “la portación de armas, el uso de banderas, divisas u otros distintivos durante el día de la elección, doce horas antes y tres horas después de finalizadas”.

**Todas las prohibiciones**

Toda la información respecto a qué se puede hacer y qué no está plasmada en el Código Nacional Electoral, el cual en su artículo 71 marca precisamente las prohibiciones, que son las siguientes:

Admitir reuniones de electores o depósito de armas durante las horas de la elección a toda persona que en los centros urbanos habite una casa situada dentro de un radio de ochenta metros (80 m) alrededor de la mesa receptora. Si la finca fuese tomada a viva fuerza deberá darse aviso inmediato a la autoridad policial;

Los espectáculos populares al aire libre o en recintos cerrados, fiestas teatrales, deportivas y toda clase de reuniones públicas que no se refieran al acto electoral, durante su desarrollo y hasta pasadas tres horas de ser clausurado;

Tener abiertas las casas destinadas al expendio de cualquier clase de bebidas alcohólicas hasta transcurridas tres horas del cierre del comicio;

Ofrecer o entregar a los electores boletas de sufragio dentro de un radio de ochenta metros (80 m.) de las mesas receptoras de votos, contados sobre la calzada, calle o camino;

A los electores, la portación de armas, el uso de banderas, divisas u otros distintivos durante el día de la elección, doce horas antes y tres horas después de finalizada;

Realizar actos públicos de proselitismo y publicar y difundir encuestas y sondeos preelectorales, desde cuarenta y ocho horas antes de la iniciación del comicio y hasta el cierre del mismo. (Inciso sustituido por art. 4 de la Ley N°25.610 B.O. 8/7/2002)

La apertura de organismos partidarios dentro de un radio de ochenta metros (80 m.) del lugar en que se instalen mesas receptoras de votos. La Junta Electoral Nacional o cualquiera de sus miembros podrá disponer el cierre transitorio de los locales que estuvieren en infracción a lo dispuesto precedentemente. No se instalarán mesas receptoras a menos de ochenta metros (80 m.) de la sede en que se encuentre el domicilio legal de los partidos nacionales o de distrito.

Publicar o difundir encuestas y proyecciones sobre el resultado de la elección durante la realización del comicio y hasta tres horas después de su cierre. (Inciso incorporado por art. 4 de la Ley N°25.610 B.O. 8/7/2002).