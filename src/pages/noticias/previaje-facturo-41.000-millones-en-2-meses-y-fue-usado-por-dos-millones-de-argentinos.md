---
category: Agenda Ciudadana
date: 2021-11-06T06:00:50-03:00
thumbnail: https://assets.3dnoticias.com.ar/previajes.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Previaje facturó $41.000 millones en 2 meses y fue usado por dos millones
  de argentinos
title: Previaje facturó $41.000 millones en 2 meses y fue usado por dos millones de
  argentinos
entradilla: "Los datos corresponden sólo a septiembre y octubre y aún faltan cargar
  los tickets de noviembre y diciembre, para viajes a realizar entre febrero y diciembre
  de 2022. \n"

---
El plan Previaje superó los 41.000 millones de pesos de facturación en septiembre y octubre pasados, con lo que cuadruplicó el total de la edición 2020 y fue utilizado por unos 2.000.000 de argentinos, a quienes se les devuelve el 50% del gasto para ser reutilizado en nuevos viajes, informó el Ministerio de Turismo y Deportes (Minturdep),.  
Esa cartera precisó que también tuvo gran adhesión el **Previaje Pami**, la versión creada este año para jubilados, que otorga un **reembolso del 70% de lo abonado en crédito** para su uso en la cadena turística y fue utilizado pos unas 200.000 personas.  
  
**Impulso al sector**

Los servicios más beneficiados dentro de esa cadena fueron las agencias de viajes y la hotelería, que suman el 82% de las ventas (45% y 37%, respectivamente), en tanto un 12% corresponde a transporte aéreo y el 6% restante a diversas prestaciones.  
  
El Minturdep, que el año pasado diseñó este plan para reactivar el turismo durante la pandemia, aclaró que los datos corresponden sólo a septiembre y octubre y aún faltan cargar los tickets de noviembre y diciembre, para viajes a realizar entre febrero y diciembre de 2022.

**El Top 10 de los destinos más elegidos**

Tres destinos patagónicos encabezan las preferencias de los turistas en esta edición de Previaje: San Carlos de Bariloche, Ushuaia y El Calafate, seguidos de Mar del Plata y Puerto Iguazú.  
  
El Top 10 se completa con Salta, Mendoza, San Martín de los Andes, Ciudad de Buenos Aires y Villa Carlos Paz, según el ranking dado a conocer por el Minturdep, que se extiende a 30 destinos distribuidos en todas las regiones del país.  
  
El jefe de esa cartera, Matías Lammens, manifestó que “PreViaje expandió la demanda y puso a las empresas a trabajar en un momento difícil, con una gran inyección de liquidez para el sector".  
  
"El programa -siguió- genera un círculo virtuoso: apoya a las agencias de viajes para que vendan paquetes anticipados y también a las Pymes donde cada turista usa el crédito”.  
  
El ministro señaló que gracias a Previaje se pudo "adelantar la temporada y estimular las escapadas en los últimos meses del año", por lo que hay expectativas "de ver niveles de ocupación muy altos en los destinos de todo el país durante el fin de semana largo de noviembre”.  
  
Desde el Ministerio señalaron que quienes ya compraron sus vacaciones para enero de 2022 tienen tiempo hasta el 20 de este mes para cargar los comprobantes, en el sitio previaje.gob.ar.  
  
Los que quieran usar el beneficio desde febrero hasta fines de 2022 pueden comprar los servicios y cargar las facturas hasta el 31 de diciembre venidero.