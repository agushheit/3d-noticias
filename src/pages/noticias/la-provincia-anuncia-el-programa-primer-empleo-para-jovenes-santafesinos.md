---
category: Estado Real
date: 2021-08-12T06:00:52-03:00
thumbnail: https://assets.3dnoticias.com.ar/EMPELOJOVEN.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La provincia anuncia el programa “Primer Empleo” para jóvenes santafesinos
title: La provincia anuncia el programa “Primer Empleo” para jóvenes santafesinos
entradilla: Será en un acto encabezado por el gobernador Omar Perotti, este jueves
  desde las 11 horas en Casa de Gobierno.

---
El gobernador Omar Perotti junto al ministro de Empleo, Trabajo y Seguridad Social, Juan Manuel Pusineri, encabezarán este jueves 12 de agosto a las 11 horas la presentación del Programa “Primer Empleo”.

El acto de lanzamiento tendrá lugar en el Salón Blanco de Casa de Gobierno, en la ciudad de Santa Fe, con la presencia de los senadores nacionales María de los Ángeles Sacnún y Roberto Mirabella; el senador por el departamento Rosario, Marcelo Lewandowski; la directora Provincial de Inclusión Socioproductiva, Julia Irigoitía; el presidente de Fisfe, Víctor Sarmiento; y la gerenta general de Sonder, Silvana Dal Lago.

“Primer Empleo” es un programa provincial de intermediación laboral entre personas y empresas que promueve la inserción laboral formal de jóvenes de entre 18 y 30 años. Durante el tiempo en que los participantes realizan la experiencia, las empresas receptoras perciben una ayuda económica correspondiente a un porcentaje del salario mínimo, vital y móvil por cada joven incorporado al programa. El mismo entrará en vigencia en los próximos días y la inscripción se realizará a través de la web www.santafe.gov.ar

En el evento participarán funcionarios nacionales, provinciales y empresarios que se encuentran participando en distintas líneas de gestión de promoción del empleo, como así también de formación y capacitación laboral de la provincia.