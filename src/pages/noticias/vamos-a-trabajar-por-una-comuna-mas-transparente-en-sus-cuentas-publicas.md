---
category: Agenda Ciudadana
date: 2021-11-09T09:35:54-03:00
thumbnail: https://assets.3dnoticias.com.ar/lorenzato.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Vamos a trabajar por una comuna más transparente en sus cuentas públicas”
title: "“Vamos a trabajar por una comuna más transparente en sus cuentas públicas”"
entradilla: 'Así lo afirmó el candidato de juntos por el cambio, Alfredo Lorenzatto
  luego de presentar el pedido de información pública para conocer el estado de las
  cuentas comunales. '

---
Luego del debate entre los tres candidatos a Presidente Comunal de Arroyo Leyes, Alfredo Lorenzatto confirmo que presentaría el pedido de información pública. “Nos presentamos esta mañana, tal como lo propuse en el debate, para solicitar detalle de las cuentas de la comuna, y pedimos, además, que se conteste antes del domingo”.

En el formulario se detallan los siguientes pedidos de información:

* Informes sobre la ejecución anual de presupuesto; 
* balances mensuales de Ingresos y Gastos. 
* Informes sobre las deudas contraídas y pendientes de cancelación. 
* Detalle de la recaudación mensual. 
* Informes en relación al personal que desempeña tarea en la Comuna.

“Queremos una comuna más transparente, que rinda cuentas a los vecinos”, afirmó.

![](https://assets.3dnoticias.com.ar/lorenzato1.jpg)