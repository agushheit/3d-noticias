---
category: La Ciudad
date: 2021-01-01T15:36:47Z
thumbnail: https://assets.3dnoticias.com.ar/01012021-Pirotecnia.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Diario UNO'
resumen: 'Pirotecnia: decomisaron 30 comercios por venta no autorizada en la ciudad'
title: 'Pirotecnia: decomisaron 30 comercios por venta no autorizada en la ciudad'
entradilla: Hubo puntos de control vehicular y alcoholemia por toda la ciudad. También
  se realizó la inspección por venta de bebidas alcohólicas, ruidos molestos y reuniones
  sociales.

---
La Municipalidad llevó adelante, en la noche del 31 de diciembre y madrugada del 1° de enero, un operativo de control y prevención de tránsito y alcoholemia, pirotecnia y reuniones sociales en el marco de los festejos por el Año Nuevo y de las medidas dispuestas en el contexto de pandemia.

En vísperas de las fiestas, el municipio diseñó una planificación operativa específica para el control de pirotecnia con el objetivo de dar cumplimiento a la Ordenanza Nº 12.429 que declara a la capital como «territorio libre de pirotecnia».

En ese sentido, se llevaron adelante 12 operativos conjuntos desde el 22 de diciembre, en articulación con la Brigada de Explosivos de la policía de la provincia en los que se controlaron 39 comercios, habilitados y no habilitados. Se obtuvieron como resultado 30 decomisos.

La secretaria de Control y Convivencia Ciudadana del municipio, Virginia Coudannes, señaló que «es para destacar el gran trabajo realizado en el operativo especial de Año Nuevo por los agentes municipales, resaltando la importancia del cuidado y la convivencia en una fecha tan especial. Es importante la presencia del Estado municipal en la calle como lo venimos realizando desde hace un año, esta noche no fue la excepción y ha sido un objetivo nuevamente cumplido».

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.1em; font-weight: 700">Control vehicular</span>**

Desde la Secretaría de Control y Convivencia Ciudadana se informó que se desarrollaron las actuaciones de control vehicular en distintos puntos de la ciudad. De los mismos participó personal municipal de las áreas de control y de la Guardia de Seguridad Institucional (GSI).

![](https://assets.3dnoticias.com.ar/01012021control-vehicular.webp)

Los controles de tránsito se realizaron en las zonas de Bv. Gálvez, Pedro Vittori, Costanera Este, Costanera oeste, zona norte, Ruta 168 y Ruta 1, entre otros. Vale destacar que en estos últimos dos puntos el despliegue operativo fue en articulación con la Agencia Provincial de Seguridad Vial (APSV). A partir de los controles de tránsito, se retuvieron 6 vehículos, de los cuales 5 fueron por alcoholemia positiva y 1 por falta de documentación.

La Secretaría de Control también prestó colaboración y apoyo al operativo de policía en el control de reuniones sociales que excedieran el número de personas permitido por las disposiciones provinciales vigentes en el marco de la pandemia. 

También controles operativos de expendio de bebidas alcohólicas, ruidos molestos, en su mayoría por denuncias al 0800. Y controles de nocturnidad y vía pública en general que arrojaron como resultado 4 actas de infracción por reuniones sociales.

Fueron más de 40 agentes del área trabajando en el operativo especial y 8 móviles operativos en recorridas por toda la ciudad, especialmente en la zona de la Costa, en un operativo que inició a la 1 y se extendió hasta las 8 hs de este 1° de enero.