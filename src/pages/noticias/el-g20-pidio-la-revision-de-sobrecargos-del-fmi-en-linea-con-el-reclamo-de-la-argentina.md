---
category: Agenda Ciudadana
date: 2021-11-01T06:00:22-03:00
thumbnail: https://assets.3dnoticias.com.ar/G20.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: El G20 pidió la revisión de sobrecargos del FMI, en línea con el reclamo
  de la Argentina
title: El G20 pidió la revisión de sobrecargos del FMI, en línea con el reclamo de
  la Argentina
entradilla: El requerimiento quedó planteado en la resolución final de la cumbre.
  También se logró que se establezca un nuevo Fondo de Resiliencia y Sostenibilidad
  para proporcionar financiación asequible a largo plazo.

---
Los líderes de los países miembros del G20 solicitaron este domingo, en su declaración final de la cumbre en la ciudad de Roma, la revisión de la política de sobrecargos del Fondo Monetario Internacional, en línea con el planteo que el Gobierno argentino viene realizando en los últimos meses en distintos foros y en sus negociaciones bilaterales con el organismo de crédito.  
  
Así lo plantearon entre los puntos centrales los líderes de los países del G20 refirmando el pedido realizado por sus ministros de Finanzas del 13 de octubre, en el que planteaba al FMI la revisión de su política de sobrecargos.  
  
Además, reclamaron al organismo de crédito multilateral la creación de un nuevo Fondo de Resiliencia y Sostenibilidad para proporcionar financiación asequible a largo plazo a los países de ingreso medios y bajos, y pidieron la canalización voluntaria de parte de los Derechos Especiales de Giro (DEG) asignados para ayudar a los países vulnerables.  
  
El presidente Alberto Fernández se reunió el sábado en Roma con la directora gerenta del Fondo Monetario Internacional (FMI), Kristalina Georgieva, en una nueva instancia para avanzar en las negociaciones por un nuevo acuerdo que contemple menos sobrecargos y le de oxigeno al país para posponer vencimientos por los casi US$ 45.000 millones de dólares de deuda.  
  
El objetivo de conseguir una reducción de la sobretasa que paga el país por haber recibido un préstamo que superó el porcentaje de participación que tiene la Argentina en el FMI, y que representa cerca de mil millones de dólares adicionales por año, es un punto central de la negociación que el Gobierno mantiene con el organismo para reestructurar la deuda de US$ 45.000 millones asumida por la gestión Cambiemos.  
  
En ese sentido, la delegación argentina encabezada por el Jefe de Estado tenía la expectativa de que la Cumbre de Líderes del G20, de la que participó durante el fin de semana en la capital italiana, culminara con un documento que apoye la baja de las sobretasas, tal como finalmente se confirmó.  
  
“Nuestros ministros de Finanzas esperan con interés que se siga debatiendo la política de sobrecargos en el Directorio del FMI en el contexto de la revisión intermedia de los saldos precautorios”, expresaron en ese sentido los mandatarios firmantes del Comunicado Final de la Cumbre del G20.

También se logró en la declaración final un pedido del G20 a que se establezca "un nuevo Fondo de Resiliencia y Sostenibilidad para proporcionar financiación asequible a largo plazo para ayudar a los países de ingreso bajo, a los pequeños estados insulares en desarrollo y a los países vulnerables de ingresos medios a reducir los riesgos para la estabilidad de la balanza de pagos futuros".  
  
Los países líderes confirmaron que seguirán trabajando en opciones para ampliar significativamente la canalización voluntaria de parte de los Derechos Especiales de Giro (DEG) asignados para ayudar a los países vulnerables, de acuerdo con las leyes y regulaciones nacionales.  
  
"Acogemos con satisfacción -indicaron en ese sentido- la nueva asignación general de Derechos Especiales de Giro (DEG, moneda del FMI), implementada por el Fondo Monetario Internacional (FMI) el 23 de agosto de 2021, que ha puesto a disposición el equivalente a 650 mil millones de dólares en reservas adicionales a nivel mundial”.  
  
Y agregaron en uno de los párrafos destacados del comunicado: “Estamos trabajando en opciones para que los miembros con fuertes posiciones externas amplíen significativamente su impacto a través de la canalización voluntaria de parte de los DEG asignados para ayudar a los países vulnerables, de acuerdo con las leyes y regulaciones nacionales”.  
  
El presidente Fernández, al exponer durante la tercera Sesión Plenaria de la Cumbre, explicó que "en el caso de América Latina y el Caribe, la canalización de los Derechos Especiales de Giro (DEG) hacia la banca regional de desarrollo es clave, pues pueden capitalizarla y apalancarla, otorgar garantías anti-cíclicas y promover inversiones privadas".  
  
En el mismo sentido, el ministro de Economía, Martín Guzmán, había reiterado en Roma a fines de la semana pasada que se apure la definición de "las reglas" para reasignar los DEG recibidos por países que no lo necesitan, para transferirlos a aquellos estados que los necesitan con urgencia.  
  
Se trata de una tema que ya fue tratado y alentado por el G20 durante la ultima cumbre de ministros de Finanzas realizada en Washington DC a mediados de octubre, y que el FMI mantiene en etapa de discusión.  
  
Esto podría significar que los países del G20 devuelvan a las naciones vulnerables US$ 100.000 millones de la suma total de los Derechos Especiales de Giro (DEG) emitidos por el FMI para afrontar la crisis sanitaria, parte de la cantidad global de 650.000 millones de dólares de los DEG emitidos por el FMI.  
  
Sobre el peso de las deudas soberanas en el actual contexto de emergencia sanitaria global y los esfuerzos realizados por combatir la pandemia, los líderes expresaron acoger "con satisfacción los progresos realizados en el marco de la Iniciativa de Suspensión del Servicio de la Deuda (DSSI por sus siglas en inglés) del G20, que también ha sido acordada por el Club de París”.

> “Subrayamos la importancia de que los acreedores privados y otros acreedores bilaterales oficiales ofrezcan un tratamiento de la deuda en condiciones al menos igual de favorablesDeclaración final del G20

  
“Subrayamos la importancia de que los acreedores privados y otros acreedores bilaterales oficiales ofrezcan un tratamiento de la deuda en condiciones al menos igual de favorables, de acuerdo con el principio de comparabilidad de trato”, sentenciaron.  
  
Finalmente, reiteraron su compromiso con el fortalecimiento de la resiliencia financiera a largo plazo y el apoyo al crecimiento inclusivo "mediante la promoción de flujos de capital sostenibles, el desarrollo de los mercados de capital en moneda local y el mantenimiento de una Red de Seguridad Financiera Global fuerte y eficaz, cuyo centro sea un FMI fuerte, basado en cuotas y con recursos adecuados”.  
  
“Seguimos comprometidos con la revisión de la adecuación de las cuotas del FMI y continuaremos el proceso de reforma de la gobernanza del FMI en el marco de la 16ª Revisión General de Cuotas, incluyendo una nueva fórmula de cuotas como guía, antes del 15 de diciembre de 2023”, argumentaron en favor de avanzar hacia una nueva arquitectura financiera global, otro de los recurrentes planteos de la Argentina desde diciembre de 2019.  
  
En ese sentido, el presidente dijo ante sus pares que "sin la construcción de un nuevo paradigma financiero internacional ninguno de estos desafíos será superado", y aseguró que "desde las periferias del mundo" se necesita "financiamiento genuino para el desarrollo, sin la complicidad local de quienes lo fugan a paraísos fiscales".