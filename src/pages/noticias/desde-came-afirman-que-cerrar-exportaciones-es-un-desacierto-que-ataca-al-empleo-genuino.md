---
category: El Campo
date: 2021-05-19T08:46:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/carne.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa CAME
resumen: Desde CAME afirman que cerrar exportaciones es un desacierto que ataca al
  empleo genuino
title: Desde CAME afirman que cerrar exportaciones es un desacierto que ataca al empleo
  genuino
entradilla: El sector de Economías Regionales de la Confederación Argentina de la
  Mediana Empresa (CAME) alertó sobre la decisión oficial de cerrar las exportaciones
  de carne para intentar contener el aumento de precios internos.

---
El sector de Economías Regionales de la Confederación Argentina de la Mediana Empresa (CAME) alertó sobre la decisión oficial de cerrar las exportaciones de carne por 30 días para intentar contener el aumento de precios internos. “Es recurrir a viejas recetas que, de antemano, sabemos que no funcionan. Cerrarnos al mundo es restringir el crecimiento del sector productivo y atacar el empleo genuino”, aseguró Eduardo Rodríguez, titular del área.

El 8% de las exportaciones de carne vacuna proviene de las economías regionales, pero la preocupación no sólo reside en los productores del sector cárnico, sino en el sector agroindustrial en su conjunto. Argentina ha ido perdiendo competitividad, por lo que abrir nuevos mercados y/o mantenerlos es un trabajo y una inversión de años. El riesgo es demasiado alto pero la prioridad pareciera estar en otro lado, pese a los dólares que necesita nuestro país y ante un alarmante nivel de pobreza.  

Intentar frenar la inflación a través de una medida que en el pasado no ha dado buenos resultados, implica condenar el presente y el futuro de uno de los sectores más pujantes de la economía nacional. “Antes de poner tanto en juego y tomar decisiones drásticas, antes de tener que dar un paso al costado y dejar los mercados en manos de nuestros competidores, analicemos la conformación de los precios al interior de cada una de las cadenas de valor mediante los Observatorios de Precios. Les aseguro que hay mucho material para trabajar”, afirmó el presidente de Economías Regionales de CAME, área que desde fines de 2020 difunde infografías en las que, entre otros aspectos, visibiliza la pesada carga tributaria que tienen los alimentos y deben pagar los consumidores.