---
category: Agenda Ciudadana
date: 2021-08-08T06:15:18-03:00
thumbnail: https://assets.3dnoticias.com.ar/MALVINAS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Un equipo de forenses llega el lunes a Malvinas para trabajos de identificación
  de restos óseos
title: Un equipo de forenses llega el lunes a Malvinas para trabajos de identificación
  de restos óseos
entradilla: Los trabajos de campo se realizarán cuatro años después del primer Plan
  Proyecto Humanitario desarrollado en el cementerio de Darwin, donde se pudo identificar
  a 115 soldados argentinos-

---
Un equipo internacional de seis expertos forenses -entre ellos dos argentinos-, coordinado por el Comité Internacional de la Cruz Roja (CICR), llegará este lunes 9 de agosto a las islas Malvinas para iniciar una nueva etapa de identificación de restos de excombatientes caídos en la guerra de 1982.

Los trabajos de campo se realizarán cuatro años después del primer Plan Proyecto Humanitario desarrollado en el cementerio de Darwin, relevamientos que permitieron identificar a 115 soldados argentinos gracias a las muestras de sangre aportadas por sus familiares.

A diferencia del trabajo realizado durante los meses de junio y julio de 2017, cuando fueron exhumados 122 cuerpos de 121 sepulturas con la denominación "Soldado argentino solo conocido por Dios", en este caso la labor se centrará solo en una tumba colectiva –la denominada C 1 10- ubicada en Darwin, cerca de la cruz mayor que preside el camposanto.

Esta tumba múltiple no había sido incorporada en el primer proyecto humanitario ya que no se trataba de una sepultura anónima: una placa colocada en 2004, cuando el cementerio fue remodelado y las viejas cruces de lapacho blancas se reemplazaron por lápidas de mármol negro de granito, consignaba que en esa fosa se encontraban los restos del alférez de Gendarmería Julio Sánchez, y de los soldados de Fuerza Aérea Héctor Aguirre, Luis Sevilla y Mario Luna.

Pero, cuando hace cuatro años se exhumaron las tumbas anónimas, se comprobó que esos tres soldados se encontraban enterrados en otras tres sepulturas individuales que habían permanecido anónimas hasta ese momento, por lo que se hizo necesario esclarecer la identidad de los restos sepultados en la tumba C 1 10.

El equipo que llegará el lunes a las islas estará conformado por Mercedes Salado Puerto, del Equipo Argentino de Antropología Forense (EAAF), propuesta por el Gobierno argentino; el forense inglés John Clark, a propuesta del Reino Unido; el jefe de la unidad forense del CICR, el argentino Luis Fondebrider; el coordinador de esta fase 2 del plan, Laurent Corbaz; y dos especialistas, uno de Estados Unidos y otro de Australia.

Salado Puerto, Fondebriber, Clark y Corbaz ya formaron parte del equipo de doce especialistas que trabajó en 2017 en el cementerio.

La duración de los trabajos en Darwin "dependerá de la situación en la cual se encuentren los restos que estuvieron involucrados en la explosión de un helicóptero, en mayo del 82".

La duración de los trabajos en Darwin "dependerá de la situación en la cual se encuentren los restos que estuvieron involucrados en la explosión de un helicóptero, en mayo del 82".

Pero esta vez, la pandemia de coronavirus y los protocolos de las islas obligarán al equipo a realizar una semana de aislamiento antes de comenzar con los trabajos forenses en el campo.

Una vez que los seis expertos lleguen el lunes al aeropuerto militar de las islas, en la base de Mount Pleasant, se dirigirán a Puerto Argentino, donde cumplirán con la semana de aislamiento preventivo en uno de los pocos hoteles en la capital de las islas.

El lunes 16 el equipo ya podrá trasladarse a Darwin –ubicado a casi 90 kilómetros de Puerto Argentino- y allí montará un laboratorio, tal como se hizo en la primera etapa, en el cual se realizará un primer análisis de las muestras óseas exhumadas de la tumba colectiva.

Según informó a Télam Virginia Urquizu, coordinadora de la unidad de casos del EAAF, la duración de los trabajos en Darwin "dependerá de la situación en la cual se encuentren los restos, que estuvieron involucrados en la explosión de un helicóptero, en mayo del 82".

El equipo que llegará el lunes a las islas estará conformado por Mercedes Salado Puerto, del Equipo Argentino de Antropología Forense (EAAF), propuesta por el Gobierno argentino; el forense inglés John Clark, a propuesta del Reino Unido; el jefe de la uni

El equipo que llegará el lunes a las islas estará conformado por Mercedes Salado Puerto, del Equipo Argentino de Antropología Forense (EAAF), propuesta por el Gobierno argentino; el forense inglés John Clark, a propuesta del Reino Unido; el jefe de la uni

Tras la exhumación, las muestras óseas se enviarán al laboratorio de genética forense que el EAAF tiene en la ciudad de Córdoba, donde se realizarán los procesamientos y cruzamientos de información entre los perfiles genéticos de las muestras y las aportadas por familiares en el proceso de 2017.

"Creemos que hacia fines de agosto el trabajo puede terminar y, tras los estudios en el laboratorio de Córdoba, se calcula que para fines de octubre o principios de noviembre tendremos resultados definitivos", estimó Daniel Filmus, secretario de Asuntos Relativos a Malvinas, Antártida y Atlántico Sur, en diálogo con Télam.

A diferencia del proceso de hace cuatro años, en este caso no se necesitará una contraprueba en otro laboratorio externo, ya que el que posee el EAAF en Córdoba "tiene un alto estándar de trabajo internacional", explicó Urquizu, "debido a los resultados de la primera parte del plan proyecto humanitario de 2017".

A diferencia del trabajo realizado durante los meses de junio y julio de 2017, cuando fueron exhumados 122 cuerpos de 121 sepulturas con la denominación "Soldado argentino solo conocido por Dios", en este caso la labor se centrará solo en una tumba colect

A diferencia del trabajo realizado durante los meses de junio y julio de 2017, cuando fueron exhumados 122 cuerpos de 121 sepulturas con la denominación "Soldado argentino solo conocido por Dios", en este caso la labor se centrará solo en una tumba colect

Pero la exhumación en Darwin de la tumba colectiva no será el único trabajo que realizará el equipo forense.

También se explorará la zona llamada Caleta Trullo, donde funcionó un hospital de campaña durante la guerra, para verificar la posible existencia de una tumba y, en caso de encontrarse restos en ese lugar, se avanzará en su identificación.

"Se va a hacer una exploración en esa zona ya delimitada, a raíz del reciente testimonio de un excombatiente británico que dijo que podría haber un cuerpo de un soldado argentino", indicó Filmus.

"Es una zona donde hubo denuncias de que posiblemente podría haber restos, en la parte aledaña a un hospital de campaña, pero también pueden ser restos propios del hospital por amputaciones, así que se analizará el terreno", explicó la integrante el EAAF.

Por su parte, la titular de la Comisión de Familiares de caídos en Malvinas, María Fernanda Araujo, dijo a Télam que aguardan este nuevo proceso de identificación con "una gran esperanza de conocer la verdad y llevar tranquilidad a las familias de los héroes caídos".

"Vivimos este proceso con muchísima emoción, también lo vivimos con alegría, porque sabemos que el reencuentro de nuestros seres amados que descansan en Darwin con sus familias trae paz y sanidad", indicó Araujo, hermana del soldado Elvio Araujo, cuyos restos fueron identificados durante el primer plan.

El proceso de identificación a los soldados argentinos se inició en 2012, durante el gobierno de Cristina Fernández de Kirchner, cuando se envió una carta al CICR para solicitar su intervención con el objetivo de hacer posible la identificación de los restos de los combatientes fallecidos y enterrados como NN en el cementerio de Darwin.

En 2013 se conformó un equipo de trabajo bajo la coordinación del Ministerio de Justicia y Derechos Humanos para elaborar protocolos que permitieran obtener información de cada familia sobre su ser querido caído en Malvinas.

Desde entonces, un equipo constituido por miembros del EAAF, funcionarios de los ministerios de Justicia y Desarrollo Social más la Escribanía General de Gobierno y el Centro Ulloa de asistencia psicológica realizó entrevistas y tomó muestras en todo el país a los familiares de los combatientes fallecidos, con el objetivo de crear un banco de sangre.

Finalmente, en diciembre de 2016, la Argentina y el Reino Unido firmaron el primer acuerdo para iniciar las tareas de identificación en junio de 2017.

Cuatro años después, en marzo pasado, se firmó el segundo acuerdo para posibilitar los trabajos en las islas, que comenzarán a partir de este lunes.