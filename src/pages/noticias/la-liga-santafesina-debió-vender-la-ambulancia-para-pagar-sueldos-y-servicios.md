---
layout: Noticia con imagen
author: "Fuente: LT10"
resumen: "Liga Santafesina "
category: La Ciudad
title: La Liga Santafesina debió vender la ambulancia para pagar sueldos y servicios
entradilla: Además, sigue en vilo el tema del predio recientemente adquirido y
  cuyo pago debe realizarse en el mes de diciembre, en un año en el que no
  generaron ingresos.
date: 2020-11-20T11:24:42.484Z
thumbnail: https://assets.3dnoticias.com.ar/liga.jpeg
---
El parate total por la pandemia por el coronavirus generó estrago en la economía de todos, y de esta realidad no escapó una asociación local como lo es la Liga Santafesina de fútbol. Durante todo el año no pudieron generar ingresos y para solventar los gastos básicos como servicios, sueldos y aportes, debieron desprenderse de lo que tenían más a mano, la ambulancia.

Gustavo Pueyo, presidente de la Liga explicó que “la ambulancia se vendió para costear gastos, costos sociales que teníamos. Se está negociando todavía el tema del predio, que en diciembre tenemos que tomar una decisión, y charlando con el Gobierno, que nos tiró unos contactos para ver si podemos encontrar una solución”.

Por otro lado, contó que el miércoles hubo una reunión de los representantes de clubes santafesinos con Claudia Giaccone -secretaría de Deportes de la Provincia- en la que se desasnaron algunas dudas, ya que “en este año sin jugar se la pasaron llenando papeles” para recibir aportes y ayudas.

“Ahora la Municipalidad dio 50 mil pesos por club y sin muchos requisitos”, sin embargo, existen problemas con temas como la luz, el agua, ya que “por más decreto y tarifa cero, si no hay no, no hay”.

En cuanto a la ayuda en específico para la Liga, Gustavo agregó que en el principio de la pandemia, y “de onda, prestamos el hotel de la liga. Entonces desde el Gobierno nos dijeron que en contraprestación nos iban a ayudar. Se llenaron papeles, pero todavía no salió, quizás eso hubiese ayudado a no vender la ambulancia”.

Con respecto a la autorización de los deportes en grupo y la vuelta a la actividad, detalló que “lo ve medio verde todavía” desde el punto de vista sanitario. Sin embargo, están gestionando para que a partir del 1 de diciembre se libere más la cantidad de jugadores permitidos.