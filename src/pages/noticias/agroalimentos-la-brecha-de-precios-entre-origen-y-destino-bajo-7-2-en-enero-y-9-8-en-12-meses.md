---
category: Estado Real
date: 2022-02-16T12:33:08-03:00
thumbnail: https://assets.3dnoticias.com.ar/paprika-g1afeb2c02_1920.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Came
resumen: 'Agro alimentos: la brecha de precios entre origen y destino bajó 7,2% en
  enero y 9,8% en 12 meses'
title: 'Agroalimentos: la brecha de precios entre origen y destino bajó 7,2% en enero
  y 9,8% en 12 meses'
entradilla: 'Los productos que más vieron reducir sus brechas fueron: berenjena (-61,4%),
  calabaza (-60,3%), lechuga (-58,8%) y mandarina (-58,6%). Pero, en general, el 67%
  de los alimentos medidos achicaron su brecha en enero. '

---
n enero, por los principales productos agropecuarios que componen la mesa familiar, los consumidores pagaron en promedio 3,45 veces más de lo que cobraron los productores. La medida es un promedio ponderado de los 24 agroalimentos que integran la canasta IPOD, y resultó 7,2% menor a la de diciembre. Si se hace un promedio simple de esas brechas, como se venía realizando hasta 2021, la diferencia fue de 5,06, un 13% menos que en diciembre pasado. 

La mejora mensual del Índice de precios en Origen y Destino (IPOD), considerando las estacionalidades de los productos, ocurrió porque los precios que se le pagaron al productor subieron muy por encima de los precios que se le cobraron al consumidor. Efectivamente: aumentaron 59,2% los precios de origen, contra 20% que subieron los precios en góndola.

Los productos que más vieron reducir sus brechas fueron: **berenjena** (-61,4%), **calabaza** (-60,3%), **lechuga** (-58,8%) y **mandarina** (-58,6%). Pero, en general, el 67% de los alimentos medidos achicaron su brecha en enero.

En enero, en 20 de los 24 productos relevados, los comercios pyme de cercanía tuvieron mejores precios que los hipermercados. La mayor diferencia se encontró en el precio de la **lechuga**, donde el valor promedio en los comercios pyme fue 18,7% menor a los hiper y en el **limón**, donde fue 17,3% menor. Es que si bien los precios al consumidor de ambos productos subieron en el mes, en los hipermercados el aumento fue muy superior.

Los datos surgen del **Índice de Precios en Origen y Destino (IPOD)** que elabora el **sector de Economías Regionales de CAME** en base a los precios de origen de las principales zonas de producción y a más de 700 precios de cada producto en destino, relevados no sólo en verdulerías y mercados por un equipo de 30 encuestadores, sino también mediante un monitoreo de los precios online de los principales hipermercados del país, durante la segunda quincena del mes.

Resultados de enero

**IPOD frutihortícola:** bajó 14% en el mes, pero subió 4,9% en 12 meses. La brecha fue de 6,15 veces si se toma el promedio ponderado según la participación de cada producto en las ventas del Mercado Central de Buenos Aires de ese mes y de 5,63% si se mide el promedio simple.

**IPOD ganadero:** subió 1,1% en el mes y bajó 5,8% en el año. La brecha fue de 2,96 veces (promedio ponderado).

**Participación del productor en el precio final:** subió de 26,3% en diciembre a 31,8% en enero, si se toma el promedio simple, y fue de 37,4% ponderándola por producto.

En enero, la mayor brecha entre precio de origen y destino se dio en la **zanahoria,** con una diferencia de 23,1 veces. Le siguió el **zapallito** (13,5 veces), la **naranja** (9,4 veces), el **limón** (7 veces) y la **calabaza**, también con una brecha de 7 veces.

Los productos con menores brechas, en tanto, fueron: **lechuga** (1,3 veces), **brócoli** (1,5 veces), **berenjena** (1,6), **huevos** (1,8) y **pollo** (2).

En enero hubo 9 productos con subas en sus brechas y 15 con bajas, que en el balance determinaron una mejora mensual significativa, al reducirse 13% la brecha si se mide el promedio simple y 7,2% midiendo el ponderado.

IPOD **berenjena:** bajó 61,4% por el aumento de 175% en los precios de origen que compensó la suba de 6,4% en los precios de góndola. Es el segundo mes consecutivo que los precios de origen tienen un fuerte salto, acumulando un alza de 260% desde noviembre, cuando el kg pagado al productor pasó de $29,9 a $107,8.

IPOD **calabaza:** bajó 60,3%, de 17,6 veces en diciembre a 7 veces en enero. Esa mejora se debe a la reducción de 10% en los precios al consumidor y al aumento de 126,6% en los precios al productor. Frente a enero de 2021, es decir, en 12 meses, los precios de góndola bajaron 0,7% y los precios de origen subieron 83,3%.

IPOD **zanahoria:** la brecha tocó un pico de 23,1 veces, subiendo 66,5% en el mes. Ese comportamiento fue impulsado por un fuerte salto en los precios al consumidor, que subieron 68% en los comercios pyme de cercanía y 84% en los hipermercados. En cambio, los precios al productor solo aumentaron 8%. La suba de precios en góndola se debe al clima, según explicaron productores y proveedores consultados. Por un lado, a las lluvias abundantes que se registraron en enero en zonas productoras como Buenos Aires que, al no cosechar, hizo escasear el producto en todo el país. Incluso escaseó en las zonas sin problemas, porque los destinos afectados se llevaron el producto desde esos orígenes, como ocurrió en Mendoza o San Juan, que se quedaron sin producto. El otro factor, fue la humedad, que arruinó la zanahoria en un tiempo más breve de postcosecha. La misma situación ocurrió con la lechuga, dado que la fuerte ola de calor redujo tanto su oferta como su calidad comercial.

IPOD **zapallito:** la brecha subió 63,7% en enero, con un alza de 42% en los precios al consumidor y una caída de 13% en los precios al productor. Fue el segundo producto con mayor incremento en la brecha del mes. El aumento se debe a una combinación entre menor oferta y menor calidad comercial del producto.

La participación promedio del productor en el precio final de los 24 productos relevados subió 26,4%, de 26,3% en diciembre a 31,8%. Y si se pondera esa participación por el peso de cada producto en las ventas del Mercado Central de Buenos Aires y en base a datos del Ministerio de Agricultura, Ganadería y Pesca de la Nación, sube a 37,4% en enero (de 36,6% en diciembre).

La mayor participación la lograron los productores de **lechuga**, que recibieron en promedio el 74,4% de su precio de venta minorista. La peor ocurrió en la **zanahoria,** donde el productor obtuvo apenas $4,3 de cada $100 que pagó el consumidor por ese producto.

El **IPOD** es un indicador elaborado por el **sector de Economías Regionales de CAME** para medir las distorsiones que suelen multiplicar por varias veces los precios de los productos agropecuarios, desde que salen del campo hasta que llegan al consumidor. Estas distorsiones son muy dispares según producto, región, forma de comercialización y época del año.

En general, las diferencias se deben a un conjunto de comportamientos. Por un lado, los especulativos, adoptados por diferentes actores de la cadena de valor que abusan de su posición dominante en el mercado –básicamente, los hipermercados, los galpones de empaque y cámaras de frío–. Por el otro, factores tales como la estacionalidad, que afecta a determinados productos en algunas épocas del año, las adversidades agroclimáticas, y los costos de almacenamiento/acopio y transporte, entre otros.