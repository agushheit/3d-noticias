---
category: Agenda Ciudadana
date: 2021-03-01T07:15:32-03:00
thumbnail: https://assets.3dnoticias.com.ar/DDHH.png
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Organismos de derechos humanos repudian "mensajes de odio" y alertan por
  "desestabilización"
title: Organismos de derechos humanos repudian "mensajes de odio" y alertan por "desestabilización"
entradilla: Fue en un documento conjunto firmado por entidades que van desde Abuelas
  de Plaza de Mayo a la APDH y el CELS.

---
Organismos de Derechos Humanos repudiaron este domingo "enérgicamente" los "mensajes de odio vertidos en una concentración opositora en Plaza de Mayo", en la cual se mostraron bolsas que simulaban contener cadáveres con los nombres de figuras públicas que se vacunaron contra el coronavirus, y alertaron sobre "sectores que pretenden desestabilizar el proceso de reconstrucción".

"A días de conmemorarse los 45 años del golpe de Estado más sangriento de nuestra historia, es muy doloroso presenciar semejante exhibición de violencia montada por estos sectores minoritarios de la sociedad que, lejos de contribuir a la construcción de un país justo y solidario, desde el inicio de la pandemia han obstaculizado las políticas de salud y de cuidado desplegadas por el Gobierno nacional", dijeron en un documento conjunto.

Añadieron que "al odio y la violencia siempre los hemos combatido con amor y el reclamo de justicia".

"La escena abyecta de unos cadáveres embolsados en el piso de la plaza es la confirmación del negacionismo y el desprecio por la democracia que promueven estos grupos, y nada bueno para la comunidad puede construirse desde allí". enfatizaron.

También aseveraron los organismos que "seguiremos cuidándonos y acompañando las políticas que fortalecen la salud pública y la ampliación de oportunidades, para que no haya más muertes por coronavirus, ni hambre y exclusión, ni clases privilegiadas por políticas económicas".

"La escena abyecta de unos cadáveres embolsados en el piso de la plaza es la confirmación del negacionismo y el desprecio por la democracia que promueven estos grupos".

En otro tramo advirtieron que "los sectores realmente privilegiados de la Argentina suelen ocultarse tras operaciones mediáticas como la de ayer, al igual que lo hacen con las maniobras judiciales destinadas a aleccionar y perseguir a dirigentes populares".

"Debemos estar alertas y mantener la unidad frente a las provocaciones de estos sectores que, escondiendo la mano, pretenden desestabilizar el proceso de reconstrucción en marcha", afirmaron.

**Los firmantes**

El documento fue firmado por Abuelas de Plaza de Mayo, Madres de Plaza de Mayo Línea Fundadora, Familiares de Desaparecidos y Detenidos por Razones Políticas, H.I.J.O.S. Capital, Asamblea Permanente por los Derechos Humanos, APDH La Matanza y Asociación Buena Memoria.

También lo rubricaron el Centro de Estudios Legales y Sociales, Comisión Memoria, Verdad y Justicia Zona Norte, Familiares y Compañeros de los 12 de la Santa Cruz, Fundación Memoria Histórica y Social Argentina, Liga Argentina por los Derechos Humanos y Movimiento Ecuménico por los Derechos Humanos.