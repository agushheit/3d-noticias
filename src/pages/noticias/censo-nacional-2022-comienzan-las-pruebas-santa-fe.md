---
category: La Ciudad
date: 2021-11-29T06:15:42-03:00
thumbnail: https://assets.3dnoticias.com.ar/CNSO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Censo Nacional 2022: comienzan las pruebas Santa Fe'
title: 'Censo Nacional 2022: comienzan las pruebas Santa Fe'
entradilla: 'Iniciarán este lunes y se extenderán hasta el 12 de diciembre. Permitirá
  poner a prueba los instrumentos que se emplearán en el operativo censal definitivo.

'

---
El Instituto Nacional de Estadística y Censos (Indec) y direcciones provinciales iniciarán este lunes y hasta el 12 de diciembre el Censo Experimental de población, hogares y viviendas, en la localidad santafesina de Gálvez y en dos áreas seleccionadas de la Ciudad de Buenos Aires. Este relevamiento estadístico forma parte de las tareas preparatorias del Censo Nacional de Población, Hogares y Viviendas que se desarrollará el año próximo, informó hoy el Indec.

Esta prueba se realizará en simultáneo en la localidad santafesina de Gálvez, ubicada en el departamento San Jerónimo, y en viviendas seleccionadas de las Comunas 2 y 5 de la Ciudad Autónoma de Buenos Aires (CABA), entre los barrios de Palermo y Villa Crespo.

La coordinación del operativo estará a cargo del INDEC, la Dirección General de Estadística y Censos de la Ciudad y el Instituto Provincial de Estadística y Censos de Santa Fe.

El Censo Experimental permitirá poner a prueba los instrumentos que se emplearán en el operativo censal definitivo.

Por primera vez, el Censo se podrá completar de dos maneras: mediante un cuestionario en línea (Censo digital) o de forma presencial, a través de la tradicional entrevista, que se realizará el domingo 12 de diciembre en esta etapa preparatoria.

"Con el Censo digital, la ciudadanía tiene una forma rápida y segura para completar el cuestionario, en el momento y el lugar que quiera. En lo que va del año hicimos distintas pruebas piloto, en las que registramos que 9 de cada 10 personas dijeron preferir la modalidad digital antes que la entrevista presencial", detalló el director del INDEC, Marco Lavagna.

En la localidad santafesina de Gálvez se relevará la totalidad de las viviendas urbanas, mientras que en la Comuna 2 de CABA, se visitarán las que están ubicadas dentro del perímetro de las calles que conforman la avenida Córdoba, Mario Bravo, Charcas y Gallo.

En la Comuna 5 porteña, serán relevadas las viviendas que se encuentran dentro de la fracción delimitada por la avenida Corrientes, Estado de Israel, Guardia Vieja y Gascón.

Otra de las novedades del 11º Censo de la Argentina es que, por primera vez, será un "censo de derecho": las personas se contabilizarán según su lugar de residencia habitual, es decir, donde pasan la mayor parte del tiempo durante la semana.

Hasta ahora, todos los relevamientos censales se hicieron bajo la denominación de "censo de hecho": la población se registraba en el lugar donde había pasado la noche previa al Día del Censo.

Los temas del cuestionario giran en torno a las características de las viviendas y los hogares, y la estructura de la población.

Para esta ronda censal, se incorporó una pregunta relativa a personas con discapacidad en el hogar, se amplió el alcance de la indagación sobre autorreconocimiento étnico -afrodescendientes y pueblos indígenas- a toda la población y se incluyó una nueva pregunta sobre la identidad de género.

El Censo Experimental es una prueba que no reemplaza al Censo 2022, por lo tanto, todas las personas censadas en esta etapa deberán contestar el cuestionario definitivo entre el 16 de marzo y el 18 de mayo del año próximo.