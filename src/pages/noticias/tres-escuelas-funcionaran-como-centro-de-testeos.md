---
category: La Ciudad
date: 2022-01-03T06:15:58-03:00
thumbnail: https://assets.3dnoticias.com.ar/hipoescuelas.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Tres escuelas funcionarán como centro de testeos
title: Tres escuelas funcionarán como centro de testeos
entradilla: 'Por el calor y la alta demanda, el Ministerio de Salud definió trasladar
  tres carpas de testeos a establecimientos educativos. ¿Cuáles son?

'

---
Desde este lunes, tres centros de testeos -que funcionaban en carpas en la ciudad de Santa Fe- serán corridos y trasladados a establecimientos educativos.

Fuentes del Ministerio de Salud indicaron a UNO que este domingo se ajustaban algunos detalles para que en las instituciones elegidas puedan comenzar a hisopar desde primeras horas de la mañana.

De esta forma, el centro de testeo que funcionaba en la Estación Mitre pasa a la escuela Normal N°32, ubicado en Saavedra al 1722; el que se encontraba en el Playón Municipal de Blas Parera al 6250 (zona del Hipódromo) se traslada a la la escuela N°19 Juan de Garay (Blas Parera al 7600); y el que brindaba servicio en el Alero de Dorrego (Av. French 1701) se moverá a la escuela N°880 Domingo Silva, ubicada sobre Regimiento 12 de Infantería 2.049.

En tanto, la carpa que funcionaba en adyacencias a la estación Belgrano fue trasladada y corrida al interior de la nave central de dicha estación

La cartera sanitaria toma esta medida en un contexto de crecimiento exponencial de casos de coronavirus y ante el colapso que presentaron -durante las últimas jornadas- algunos puntos de testeos.

A ello, se le suman las altas temperaturas que castigan tanto a los trabajadores como a las personas que concurren a hisoparse que deben estar, muchos de ellos, al rayo del sol, soportando sensaciones térmicas que llegaron a superar los 40ºC.

Desde el Ministerio indicaron con relación a la capacidad operativa de testeos diarios que, si bien "es un traslado", existen posibilidades de "ampliación" al correrlos a estos establecimientos educativos.

**Los otros centros de testeos**

Estación Belgrano (Todos los días de 8 a 12)

Cemafé (Lunes a viernes de 8 a 12)

Viejo Hospital Iturraspe. (Lunes a viernes de 8 a 12)

Cabe recordar que los únicos centros de testeos que realizan hisopado por la tarde son el de la Estación Belgrano y Viejo Iturraspe, en el horario de 17 a 20, de lunes a viernes

Por el calor y la alta demanda, el Ministerio de Salud definió trasladar tres carpas de hisopados a establecimientos educativos.

**Largas filas el domingo**

El domingo a la mañana las filas para testearse por Covid en los tres Centros ubicados en la ciudad de Santa Fe se desbordaron. "Están sobrepasados", reconocieron funcionarios del Ministerio de Salud.

En esta línea aclararon que "la recomendación siempre es q los q tienen servicio de salud vayan al privado", y recordaron que las obras sociales deben cubrir estas pruebas.

Al ser consultados frente a este panorama por una posible aumento de recursos para la atención para aquellos que deban realizarse el test, desde la cartera de Salud provincial respondieron: "Se están evaluando posibles ampliaciones, de hecho mañana (por este lunes) empiezan a funcionar centros de testeos en las escuelas".