---
category: La Ciudad
date: 2021-10-21T06:15:35-03:00
thumbnail: https://assets.3dnoticias.com.ar/iturraspe.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: El Ministerio de Salud de Santa Fe se "muda" al Viejo Hospital Iturraspe,
  donde también tratarán adicciones
title: El Ministerio de Salud de Santa Fe se "muda" al Viejo Hospital Iturraspe, donde
  también tratarán adicciones
entradilla: "La cartera sanitaria provincial ocupará un sector del emblemático edificio.
  Las novedades: habrá además consultorios externos, seguirá el centro para pacientes
  con cáncer, y otro para tratar personas adictas. \n\n"

---
El Viejo Hospital Iturraspe, ese imponente y emblemático edificio en Av. Gob. Freyre y Bv. Pellegini, será la nueva “sede” del Ministerio de Salud de la provincia de Santa Fe. La decisión se había tomado en marzo de 2020, y ahora empieza a cobrar impulso la refuncionalización del inmueble, con varias novedades.

 El nosocomio hoy funciona con áreas Covid, no Covid, terapia intensiva (20 camas) y clínica general, un vacunatorio y un centro de hisopados. La pandemia está dando una tregua, y esto agilizó pensar en el horizonte a futuro de este espacio. Todo indicaba que el hospital se encaminaría a ser exclusivamente un centro para el tratamiento de pacientes oncológicos (hoy funciona allí la Agencia contra el Control del Cáncer), pero no: tendrá otras funciones.

 “A futuro, pensamos inaugurar los consultorios externos (con entrada por Av. Freyre), y también está previsto, además de que se utilice un amplio sector para albergar las instalaciones del Ministerio de Salud, habilitar un centro para la atención de pacientes con adicciones”, adelantó en diálogo con El Litoral el Dr. Osvaldo Marelli, director del Viejo Iturraspe.

 “Es un proyecto interministerial (Salud, Desarrollo Social, Trabajo), y siempre contamos en esto con el apoyo del gobernador Omar Perotti”, ponderó. La iniciativa de contar con un centro para el abordaje de adicciones “es algo muy necesario. Se piensa en un tratamiento integral e interdisciplinario; es decir, que una vez que el paciente esté desintoxicado y tras haber seguido los escalones terapéuticos, pueda desarrollar talleres de trabajo y así que tenga más herramientas para volver a su barrio y conseguir un empleo”, añadió Marelli.

 Antes de la migración del hospital (en 2019), funcionaba un servicio de salud mental. “Con ello, hay muchos profesionales capacitados: la idea es sumarlos en este proyecto. Aún no hay plazos para la mudanza, pero la idea está en marcha”, cerró el médico.