---
category: Agenda Ciudadana
date: 2020-11-27T10:48:26Z
thumbnail: https://assets.3dnoticias.com.ar/./tavernier.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Cooperativas y mutuales
title: La Provincia lidera la agenda nacional de asambleas presenciales de cooperativas
  y mutuales
entradilla: Desde la Dirección de Cooperativas y Mutuales del Ministerio de Producción,
  Ciencia y Tecnología se confeccionó un protocolo Covid-19 específico para la actividad.

---
El Gobierno de Santa Fe, a través del Ministerio de Producción, Ciencia y Tecnología -en un trabajo conjunto con el Ministerio de Salud-, creó un protocolo Covid-19 específico que posibilitará desarrollar las asambleas presenciales de cooperativas y mutuales, convirtiéndose en la primera provincia del país en llevar adelante una iniciativa de estas características.

Al respecto, el director Provincial de Economía Social, Agricultura Familiar y Emprendedorismo, Guillermo Tavernier, destacó: “Estamos trabajando con dinamismo para dar respuestas a las necesidades de estas entidades en todo el territorio santafesino. Al comienzo de la cuarentena, el Instituto Nacional de Asociativismo y Economía Social (INAES) informó que, en el marco de las medidas sanitarias dispuestas para hacer frente a la pandemia de coronavirus, quedaban suspendidas las asambleas en forma presencial y, con el correr de los meses, se posibilitaron las virtuales”.

“Gracias al trabajo conjunto con el Ministerio de Salud, se presentó ante todas las autoridades del Consejo Provincial de Asociativismo y Economía Social el protocolo Covid-19. Se trata de una herramienta que permitirá elaborar herramientas y delinear políticas de trabajo para desarrollo del sector con los cuidados necesarios exigidos por el órgano competente frente a la pandemia”, concluyó el funcionario provincial.