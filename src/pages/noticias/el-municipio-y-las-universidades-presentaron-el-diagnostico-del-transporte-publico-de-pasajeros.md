---
category: La Ciudad
date: 2021-11-06T06:00:07-03:00
thumbnail: https://assets.3dnoticias.com.ar/TRANSPORTE.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: El municipio y las universidades presentaron el diagnóstico del transporte
  público de pasajeros
title: El municipio y las universidades presentaron el diagnóstico del transporte
  público de pasajeros
entradilla: 'La Mesa de Movilidad oficializó el informe elaborado por la Municipalidad
  en conjunto con las tres universidades con sede en la capital de la provincia. '

---
En la Estación Belgrano el intendente Emilio Jatón participó de la presentación del documento elaborado por tres universidades sobre el sistema de transporte de pasajeros en la capital de la provincia. De la actividad participaron todos los integrantes del Gabinete municipal, junto con representantes del Concejo Municipal, de las casas de altos estudios y entidades relacionadas con la temática.

El informe se enmarca en la mesa de Movilidad que conforman el municipio y las universidades Católica de Santa Fe, Nacional del Litoral y Tecnológica Nacional. El mismo comenzó a elaborarse en 2020, en plena pandemia de Covid-19, y concluyó a mediados del año en curso. En ese momento se concretó un pormenorizado análisis del sistema de transporte, con la intención de proyectar un nuevo modelo a futuro.

En la presentación, Jatón aseguró que “para ir de la ciudad que tenemos a la que queremos hay un largo tránsito en el que hay muchas cosas para cambiar, pero lo estamos haciendo. El transporte siempre fue un tema importante para nosotros, desde el primer día”, afirmó.

En ese sentido, reseñó que “en plena pandemia nos sentamos con los rectores y los expertos de las universidades para plantearles que teníamos un problema con el transporte público de pasajeros y que no era una cuestión menor, porque sabemos que impacta en cada lugar de la ciudad e involucra a todos los habitantes”.

“No se pueden tomar decisiones si no tenemos datos”, aseveró el intendente, al referirse a la importancia del informe. “Este documento es de mucho valor, es un punto de partida. A partir de él sabemos que necesitamos otra mirada acerca del transporte y no sólo el transporte público, porque la gente se moviliza de otra manera: hay más bicicletas y más motos”, relató.

Ahora, oficializada la evaluación ante los actores del sistema, Jatón señaló que “entre todos podemos empezar a planificar y trabajar para ello. Queremos bicisendas, colectivos adaptados a la ciudad y transporte eléctrico para bajar el uso del carbono que nos preocupa a todos”.

“Tenemos todo para cambiar el transporte público y depende de nosotros seguir trabajando en esta línea. Para ello, estamos buscando financiamiento exterior”, informó.

**Movilidad conectada en redes**

El decano de la UTN Facultad Regional Santa Fe, Eduardo Donet, explicó que la casa de altos estudios que representa, “lidera la Mesa de Movilidad en conjunto con UNL y UCSF”. En particular, señaló que “la dimensión que trabaja Universidad Tecnológica Nacional es la mirada de la movilidad con centralidad en el Transporte Público de Pasajeros como articulador del sistema de multimodalismo”.

Según evaluó, que la ciudad cuente con un diagnóstico sobre el transporte “permite entender cómo y de que manera lo utilizamos para redefinir, desde su articulación, las mejores y más eficientes maneras de movilidad en la ciudad actual”. No obstante, destacó la importancia de esa evaluación, a la hora de “prever la ciudad futura”.

Por último, Donet consignó que próximamente, la Mesa de Movilidad abordará otros ítems, en el marco de la labor que lleva a cabo. En ese sentido, mencionó el análisis de puntos de articulación de redes de movilidad, los concentradores de flujos de personas, las zonas calmas y los concentradores en estaciones de transferencias de modos como nodos articuladores del sistema, entre otros. El decano indicó que la intención es “diseñar una forma de movilidad conectada en redes que vaya perfilando, con los proyectos de planeamiento urbano, la ciudad del futuro. En síntesis, que sea una ciudad más amigable, segura y vivible”.

**Trazos directores**

El secretario de Desarrollo Urbano del municipio, Javier Mendiondo, explicó que se presentó “a distintos actores de la ciudad, lo que denominamos el diagnóstico del sistema de transporte de pasajeros, en el marco de la mesa de movilidad que el municipio firmó con las universidades locales”. Según detalló, con este documento “estamos desarrollando un análisis del sistema de transporte que nos va a permitir proyectar un nuevo modelo, a pedido del intendente Emilio Jatón, para el año próximo”.

Mendiondo insistió en que “será un nuevo modelo de transporte que va a determinar la manera más adecuada de movernos en la ciudad, de acceder tanto a los barrios como a los espacios públicos y de garantizar el derecho a la movilidad”.

El secretario señaló que “los resultados del estudio son multidimensionales y requieren un análisis pormenorizado, pero nos permiten decir que hoy estamos a la mitad del uso prepandémico del sistema transporte”. Del mismo modo, permiten observar que “el modo de moverse en la ciudad se modificó notablemente, lo que nos genera un gran desafío para ver cómo nos proyectamos a futuro”.

“Lo que se presentó hoy es la fotografía, el diagnóstico del sistema del transporte público de pasajeros y a partir de ahora, en próximas instancias, habrá estudio de análisis de costos y los lineamientos o trazos directores para un nuevo sistema de transporte”, adelantó Mendiondo.

**Presentes**

También participaron del encuentro, referentes del Órgano Control del Transporte Público de Pasajeros, las empresas prestatarias del servicio, los gremios UTA, ATAP, AATHA y AAUCAR, la Sociedad Rural, la Unión Industrial, el Centro Comercial, la Bolsa de Comercio, ADE, el Mercado de Productores, el Parque Tecnológico Litoral Centro, la Comisión de Transporte de la Cámara de Diputados de la provincia, la Cámara de Comercio Exterior, la Cámara Argentina de Comercios y Servicios y la Cámara Argentina de la Construcción.