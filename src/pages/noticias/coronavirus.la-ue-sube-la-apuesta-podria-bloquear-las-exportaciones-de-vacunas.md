---
category: Agenda Ciudadana
date: 2021-01-29T10:24:24Z
thumbnail: https://assets.3dnoticias.com.ar/vacuna.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Con información de La Nación
resumen: 'Coronavirus. La UE sube la apuesta: podría bloquear las exportaciones de
  vacunas'
title: 'Coronavirus. La UE sube la apuesta: podría bloquear las exportaciones de vacunas'
entradilla: Millones de dosis de vacunas podrían quedar bloqueados en pocos días más,
  después de que Bruselas decidió dar a los reguladores nacionales de los 27 países
  del bloque el poder de rechazar los pedidos de exportación.

---
Mientras las capitales europeas ponen en pausa, una tras otra, sus campañas de vacunación contra el Covid-19 por falta de dosis suficientes,la Unión Europea (UE), indignada por el incumplimiento de los acuerdos firmados por parte de algunos laboratorios farmacéuticos, anunció un nuevo mecanismo que podrá impedirles exportar sus vacunas fuera del continente.

Millones de dosis de vacunas podrían así quedar bloqueados en pocos días más, después de que Bruselas decidió dar a los reguladores nacionales de los 27 países del bloque el poder de rechazar los pedidos de exportación.

La primera víctima de esa decisión sería Gran Bretaña. En plena campaña de vacunación masiva, el Reino Unido compró 40 millones de dosis a Pfizer-BioNTech, producidas en la planta que el gigante farmacéutico tiene en Bélgica.

La medida, cuyos detalles serán conocidos este viernes, llega como respuesta al anuncio del grupo AstraZeneca, que la semana pasada afirmó que solo podría entregar a la UE el 25% de los 100 millones de dosis previstos para fines de marzo, debido a problemas en su planta de producción belga. Bruselas no acepta esa explicación y sospecha que el laboratorio británico-sueco beneficia a Gran Bretaña, con el desvío de parte de su producción europea hacia ese país (fuera del bloque tras el Brexit).

Como medida de presión suplementaria, las autoridades belgas lanzaron este jueves una investigación en el sitio de producción que AstraZeneca tiene en Seneffe, 40 kilómetros al sur de Bruselas, "con el objetivo de comprobar si realmente el grupo experimenta problemas de producción".

![Las oficinas de AstraZeneca en Bruselas](https://bucket3.glanacion.com/anexos/fotos/39/3597839w380.jpg)

La vacuna de AstraZeneca debía ser autorizada este viernes por las autoridades sanitarias europeas y la llegada del primer envío de esa compra era esperada con ansiedad por el bloque, que experimenta serios atrasos en su campaña de vacunación. Sobre todo, después de que otra de las vacunas disponibles en la UE, la de Pfizer-BoiNTech, también enfrenta problemas de distribución.

Por el momento, solo el 2% de los 446 millones de habitantes del bloque recibieron la primera dosis de la vacuna, comparada con el 11% en el Reino Unido.

La falta de dosis en la UE ya forzó al gobierno español a anunciar una pausa en la campaña de vacunación en Madrid. La próxima región podría ser Cataluña. En Francia sucedió lo mismo con la región parisina, donde las autoridades sanitarias solicitaron a los hospitales suspender las citas para aplicar la primera dosis hasta comienzos de marzo, a fin de preservar las inyecciones disponibles para quienes deben recibir la segunda dosis en los próximos 20 días.

El ministro de Salud Pública alemán, Jens Spahn, cree que "las próximas diez semanas serán duras".

"Fabricar vacunas es muy complejo y la necesidad de aumentar la capacidad de producción puede provocar atrasos. Pero, en ese caso, el problema debe impactar a todos de la misma manera y no solo a la UE", advirtió.

Con las campañas de vacunación casi paralizadas y AstraZeneca que se niega a desviar dosis fabricadas en las dos plantas que posee en Gran Bretaña hacia el bloque, Bruselas decidió alzar el tono. En una misiva enviada el miércoles, Charles Michel, presidente del Consejo Europeo -que reúne a los líderes de los 27 países-, se congratuló por la decisión. También agregó que la UE debe "explorar todas las opciones y hacer uso de todos los medios legales y medidas de presión contemplados en los tratados".

**Restricciones**

En Bruselas, muchos esperan que no sea necesario llegar a poner en ejecución la advertencia.

"En un mundo ideal, una vacunación debería realizarse sin problemas. Pero no estamos en un mundo ideal", analizó un diplomático europeo. "Por eso es que, con ciertos Estados, comprendidos nuestros vecinos, suele ser necesario aplicar restricciones a la exportación e incluso prohibir la exportación de ciertos productos", agregó, al recordar la decisión británica de vedar a la exportación ciertos medicamentos contra el coronavirus.

El parlamentario alemán Peter Liese, miembro de la Democracia Cristiana (CDU), de la canciller Angela Merkel, fue aun más lejos: "Si la única solución es reducir los envíos de vacunas al Reino Unido para beneficiar a la UE, está muy bien".

En su pulseada con Bruselas, AstraZeneca asegura que está obligada por contrato a cumplir con las dosis prometidas a Gran Bretaña y fabricadas en las dos plantas que posee en ese país, antes de hacer envíos a la UE.

Bruselas respondió que, según el acuerdo firmado, las cuatro plantas europeas de la farmacéutica deberían producir las dosis adquiridas por el bloque: 400 millones de dosis por un total de 336 millones de euros.

En Londres, el jefe de Gabinete, Michael Gove, declaró que Gran Bretaña ayudará a la UE "solo si sobran dosis en el país". Y agregó: "Lo más importante es asegurarnos de que nuestro propio programa de vacunación se desarrolla como lo planeamos".