---
category: Agenda Ciudadana
date: 2021-08-07T06:15:36-03:00
thumbnail: https://assets.3dnoticias.com.ar/alberto.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Cuáles son las nuevas disposiciones sanitarias del gobierno Nacional
title: Cuáles son las nuevas disposiciones sanitarias del gobierno Nacional
entradilla: Se confirman más aperturas con el objetivo de recuperar actividades de
  manera “responsable y cuidadosa”.

---
El Gobierno Nacional emitirá un nuevo DNU con varias aperturas, en relación a la pandemia del coronavirus, disposiciones que ya fueron anunciadas.

Estas aperturas fueron confirmadas por Alberto Fernández, que dijo que se incluirán cuestiones relacionadas al turismo, viajes y eventos masivos.

El anuncio fue en el contexto del nuevo plan de recuperación de actividades de manera "responsable y cuidadosa".

Ante esta situación, Alberto Fernández oficializó que se avanzara en un esquema de "aperturas sostenidas y progresivas", siempre y cuando se mantengan estables los indicadores epidemiológicos.

Al respecto, el Presidente explicó que el DNU "será el primer paso del plan para recuperar actividades de manera responsable y cuidadosa".

"Ampliaremos poco a poco la cantidad de personas que pueden reunirse. Esta situación nos permite avanzar con la presencialidad escolar, un logro colectivo y algo que a todos nos preocupó. Este es un punto de inflexión", manifestó.

**Las nuevas disposiciones sanitarias**

Parámetros para definir situaciones de alarma epidemiológica y sanitaria. Serán consideradas situaciones de alarma epidemiológica y sanitaria en aglomerados urbanos de más de 300.000 habitantes, aquellas en las que:

La ocupación de camas totales de terapia intensiva sea superior al 80 por ciento.

La variación porcentual del número de pacientes internados en UTI por COVID-19 de los últimos siete días, respecto de los siete días anteriores sea superior al 20 por ciento.

Continúan en vigencia las medidas de cuidado individual como la distancia mínima de 2 metros entre personas, el uso de tapabocas, la ventilación de los ambientes en forma adecuada y constante, y la higiene frecuente de las manos.

**Actividades permitidas en todo el territorio nacional**

**En espacios cerrados**

La práctica de deportes, eventos sociales, culturales, recreativos y religiosos; y las actividades realizadas en cines, teatros, clubes, locales gastronómicos, centros culturales, gimnasios, casinos y bingos, con un aforo del 70 por ciento.

La asistencia a centros de día y clubes de adultos mayores con un aforo del 70 por ciento cuyos asistentes cuenten con el esquema completo de vacunación contra la COVID-19, y hayan transcurrido 14 días desde entonces.

Las reuniones sociales en domicilios particulares de hasta 10 personas si se realizan en espacios interiores, y de hasta 20 personas si se realizan al aire libre.

**En espacios abiertos**

La realización de excursiones y actividades turísticas de acuerdo a los protocolos y normativa vigente, que se efectúen exclusivamente en transportes que permitan mantener ventilación exterior adecuada de manera constante y cruzada.

Las reuniones y actividades en espacios públicos de hasta 100 personas.

La autoridad jurisdiccional podrá disminuir el aforo establecido según los parámetros epidemiológicos y sanitarios.

**Viajes grupales**

Las jurisdicciones podrán autorizar la realización de viajes en grupos de hasta 10 personas para actividades recreativas, sociales y comerciales, en tanto tengan como origen y destino aglomerados que no sean considerados en Situación de Alarma Epidemiológica y Sanitaria; que se realicen en base a los protocolos; que no incluyan pasajeros que hayan estado en el exterior durante los últimos 14 días o sean convivientes de quien revista tal condición; no hayan sido contacto estrecho de un caso confirmado de COVID-19 durante los 14 días previos al viaje, ni hayan tenido un diagnóstico positivo de COVID-19 en el mismo período.

**Clases presenciales**

Se mantendrán las clases presenciales y las actividades educativas no escolares presenciales en todo el país, dando efectivo cumplimiento a los parámetros de evaluación, estratificación y determinación del nivel de riesgo epidemiológico y condiciones establecidas en las Resoluciones Nros. 364 del 2 de julio de 2020, 370 del 8 de octubre de 2020, 386 y 387 ambas del 13 de febrero de 2021 del Consejo Federal de Educación, sus complementarias y modificatorias.

**Quedan suspendidas en todo el país las siguientes actividades**

Eventos masivos, entendidos como todo acto, reunión o acontecimiento de carácter eventual, cuyo objeto sea artístico, musical, festivo o deportivo, capaz de producir una concentración mayor a 1.000 asistentes.

Reuniones sociales en domicilios particulares de más de 10 personas si se realizan en espacios interiores y de hasta 20 personas si se realizan al aire de libre.

Viajes grupales de egresados jubilados y jubiladas y grupales de estudiantes o similares.

Actividades y reuniones sociales en espacios públicos al aire libre de más de 100 personas.

Actividades en discotecas, salones de fiestas, bailes, o actividades similares.

Los gobernadores y el jefe de Gobierno de la Ciudad Autónoma de Buenos Aires, en atención a las condiciones epidemiológicas y sanitarias, podrán disponer restricciones temporarias y focalizadas adicionales en los lugares bajo su jurisdicción.

Actividades que se suspenden en aglomerados urbanos en situación de alarma epidemiológica y sanitaria por el término de nueve días

La circulación nocturna de las personas, entre las 20 y hasta las 6 del día siguiente.

Eventos masivos (artísticos, musicales, festivos o deportivos).

Las actividades económicas, industriales, comerciales y de servicios en lugares cerrados con 50 por ciento de aforo.

Las actividades recreativas, culturales, religiosas y deportivas con 30 por ciento de aforo.

**Medidas de fronteras**

Se estableció el cupo semanal de 11.900 plazas para argentinos y argentinas y residentes hasta el 6 de septiembre y, en adelante, un cupo semanal de 16.100 plazas si se habilitan nuevos corredores seguros.

Dentro de las plazas mencionadas, 700 pasajeros podrán ser priorizados por el Estado Nacional por razones de urgencia o para atender cuestiones esenciales impostergables o de representación oficial o diplomática, a los efectos de facilitar su viaje al país.

**Sigue prohibido el ingreso de extranjeros.**

Se eliminan las restricciones de vuelos directos por países con la posibilidad de habilitar vuelos especiales para facilitar el ingreso de argentinos desde esos países (Brasil, Chile, Reino Unido, etc).

Los gobernadores y gobernadoras podrán solicitar la apertura de nuevos corredores seguros (terrestres y/o aéreos) siempre que cuenten con protocolo aprobado por la autoridad sanitaria provincial, previendo en su caso los mecanismos de testeo, aislamiento y traslado de las muestras respectivas para la secuenciación genómica que deberá demás estar aprobado por el Ministerio de Salud de la Nación.

**Sigue prohibido el ingreso por frontera terrestre.**

Dentro de los nuevos corredores seguros a implementar a partir del 6 de septiembre del 2021, y según la situación de cobertura de vacunación, sanitaria y epidemiológica en origen y en destino, desde el 6 de septiembre los gobernadores podrán proponer la implementación de una experiencia piloto de ingreso de extranjeros provenientes de Chile y la República Oriental del Uruguay solo con esquemas completos de vacunación y cuarentena obligatoria.