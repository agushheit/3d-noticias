---
category: Agenda Ciudadana
date: 2021-03-02T06:29:20-03:00
thumbnail: https://assets.3dnoticias.com.ar/perotti.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Diario Uno
resumen: El miércoles arranca la vacunación de los docentes en toda la provincia
title: El miércoles arranca la vacunación de los docentes en toda la provincia
entradilla: Así lo confirmó el gobernador Omar Perotti. En la ciudad de Santa Fe el
  lugar elegido será el Centro de Educación Física Nº 29

---
En Santa Fe se espera recibir 37.800 dosis producidas en China contra el coronavirus que ya llegaron al país y por estas horas eran distribuidas a la provincias, destinadas a docentes, no docentes y al personal educativo en general. El gobernador Omar Perotti confirmó que este miércoles se comenzará en la provincia el operativo de vacunación en los docentes y no docentes afectados al dictado de clases presenciales. El lugar elegido es el Centro de Educación Física Nº 29, ubicado en Galicia y las Heras.

Para determinar la cantidad de personas que deberían ser inoculadas en cada provincia, la Nación habilitó con el fin de organizar a este sector un registro de vacunación la semana pasada y se espera la confirmación de la fecha de inicio.

El ministro de Educación, Nicolás Trotta, quien ya había adelantado que el Gobierno pretende iniciar esta semana la vacunación de los docentes contra el coronavirus, aseguró este domingo que el proceso podría estar completo "en tres meses, aproximadamente".

La inoculación a docentes y no docentes de todo el país abarca a más de 1,4 millones de personas.