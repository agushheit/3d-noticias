---
category: Agenda Ciudadana
date: 2021-10-02T06:15:37-03:00
thumbnail: https://assets.3dnoticias.com.ar/MENORES.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: El Gobierno anunció que se aplicará la vacuna Sinopharm a niñas y niños entre
  3 y 11 años
title: El Gobierno anunció que se aplicará la vacuna Sinopharm a niñas y niños entre
  3 y 11 años
entradilla: Lo informó la ministra de Salud, Carla Vizzotti, quien aseguró que el
  país ya cuenta con un stock de "10 millones Sinopharm" y "entre el 4 y el 11 de
  octubre recibirá un lote de 1,5 millón más-

---
El Gobierno anunció este viernes que los niños de entre 3 y 11 años serán inmunizados contra el coronavirus con la vacuna china Sinopharm, tras la aprobación de la Administración Nacional de Medicamentos, Alimentos y Tecnología Médica (Anmat), lo que permitirá completar la inoculación de esa franja etaria de la población antes de fin de año.

"La Argentina termina 2021 con toda su población de más de tres años cubierta y protegida. Contamos con el stock para iniciar esa vacunación y completar los esquemas", dijo la ministra de Salud, Carla Vizzotti en una conferencia de prensa junto al ministro de Educación Jaime Perczyk.

Previamente, la ministra le había informado al presidente Alberto Fernández en su despacho de Casa Rosada la novedad. El jefe de estado celebró la noticia y manifestó su satisfacción por la expectativa de que "la Argentina termina 2021 con toda su población de más de tres años cubierta y protegida".

**Conferencia de prensa con anuncio de la ministra de Salud**

Según precisó Perczyk durante la conferencia, se trata de alrededor de 6 millones de niñas y niños de esa franja etaria que aún no contaban con vacuna para su inmunización.

Por su parte, Vizzotti precisó que completará los esquemas de vacunación de niños y adolescentes con vacunas Sinopharm y Pfizer, con un intervalo de 21 a 28 días entre la primera y segunda aplicación.

En el caso de las vacunas Sinopharm, la ministra explicó que su utilización fue autorizada por la Anmat tras haberse realizado ensayos clínicos de las fases 1 y 2, con estudios en China y en Emiratos Árabes Unidos.

"Se aplicaron 500 millones de esas vacunas en China, Emiratos Árabes y Bahréin, y se utiliza una similar en Chile", señaló Vizzotti para llevar tranquilidad sobre la eficacia del inmunizante.

Refirió que la Argentina ya cuenta con un stock de "10 millones" de vacunas, a las que se sumarán "entre el 4 y el 11 de octubre un lote de 1.150.000 más y otro de 1,6 millones, con lo que completará 12 millones de dosis a fin de este mes".

"Para eso guardamos las vacunas", apuntó Vizzotti, y añadió que con una población en la Argentina de "cinco millones de adolescentes y entre 5,5 y seis millones de niños de entre 3 y 11 años, es relevante para avanzar" en la vacunación.

De esos cinco millones de adolescentes, 700 mil están vacunados con la primera dosis y 250 mil con ambas, indicó Vizzotti, quien recordó que en el país hay "entre nueve y 10 millones de vacunados por mes", con los cual se llegó al 50% de inmunizados y el objetivo es "seguir escalando, porque a las segundas dosis de adultos se suman adolescentes y niños de entre 3 y 11 años".

Para la funcionaria, en los próximos tres meses "cambiará la historia de la pandemia en la Argentina", porque los mayores de 3 años de edad "estarán protegidos", algo que "va a ser un hito importante".

La titular de la cartera de Salud adelantó que entre el lunes próximo y el 27 de este mes llegarán 7,2 millones de dosis de Pfizer para ser aplicadas a un universo de cinco millones de adolescentes.

En este marco, Vizzotti, anunció una convocatoria para el próximo lunes del Consejo Federal de Salud para evaluar cómo siguen los pasos para la vacunación de niños y jóvenes contra el coronavirus, y se iniciará la distribución de las dosis en stock.

Vizzotti manifestó que las aplicaciones estarán enmarcadas dentro de "la vacunación escolar, de inmunización Covid y de calendario", para incluir "a los que perdieron contacto con la escuela".

La titular de la cartera sanitaria subrayó la "importancia que tiene desde el beneficio individual y para el impacto colectivo, y por su relevancia para sostener la presencialidad plena" en las escuelas de todo el país.

La aplicación de la vacuna pediátrica "es una herramienta preventiva teniendo en cuenta que los menores de 11 años no habían sido alcanzados todavía" por el plan de vacunación a nivel nacional, especificó Vizzotti.

En tanto, Perzyck expresó que la medida "tiene que ver con un esfuerzo diplomático, político y sanitario, que va a permitir fortalecer tres cuestiones que pidió el Presidente", y enumeró: "La recuperación de la presencialidad plena en todo el nivel educativo desde inicial, la primaria, la secundaria, formación superior y universidad; recuperar aprendizajes en la escuela; y recuperar la normalidad creciente en el sistema educativo".

El ministro de Educación destacó que se trabajará de manera coordinada con los gobiernos provinciales "en recuperar a los chicos que se fueron del sistema educativo, y en garantizar poder vacunar en los tiempos escolares".

En ese sentido, contó que "los que se desvincularon a distancia de la escuela o fueron intermitentes fueron un millón" de alumnos, y la idea ahora es "ir uno por uno a sus casas para la integración plena", porque "es un derecho ir a la escuela".

Perzyck resaltó que la vacunación "aporta también a recuperar a los chicos que se se fueron del nivel educativo, porque invitándolos a vacunarse van a volver a la escuela".

En esa línea aportó que se creó un fondo por 5.000 millones de pesos para el Programa 'Volver a la Escuela' para "tener más días, más horas" de estudio, a través de "contraturnos, los sábados, con escuelas de verano o con clases de apoyo", porque se busca "enseñar más".