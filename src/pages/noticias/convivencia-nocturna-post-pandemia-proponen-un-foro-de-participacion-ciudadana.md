---
category: La Ciudad
date: 2021-08-14T06:00:48-03:00
thumbnail: https://assets.3dnoticias.com.ar/concejo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: '"Convivencia Nocturna post pandemia": proponen un Foro de Participación
  Ciudadana'
title: '"Convivencia Nocturna post pandemia": proponen un Foro de Participación Ciudadana'
entradilla: El Concejo aprobó un decreto para convocar a una mesa de trabajo para
  tratar el funcionamiento de bares, restaurantes, salones de fiestas y locales bailables,
  entre otras cuestiones.

---
En la última sesión, el Concejo aprobó la convocatoria a un Foro de Participación Ciudadana para abordar la temática de la Convivencia Nocturna post pandemia. El propósito es que los diferentes sectores involucrados puedan expresar sus ideas y repensar la nocturnidad.

La refuncionalización de los locales bailables, el funcionamiento de bares, restós y salones de fiestas, la posibilidad de eximir de Tributos Municipales a aquellos sectores que no hayan podido funcionar y la realización de operativos especiales de control de fiestas “clandestinas” son algunos de los temas que se sugieren abordar, a los que se suma también la posibilidad de permitir la habilitación de locales bailables exclusivo para jóvenes menores de edad.

El concejal Carlos Suárez, autor del proyecto, expresó que “necesitamos discutir la nocturnidad post pandemia. Han cambiado radicalmente muchos aspectos y tenemos que ocuparnos y prepararnos para el futuro, para que cuando se levanten las restricciones y sea aún más la cantidad de gente que sale a divertirse, la ciudad pueda dar respuesta”. El concejal se refirió a la relocalización de los boliches sobre la ruta 168: “debemos pensar cómo hacemos para que la ciudad se apropie de ese lugar, para que la infraestructura disponible pueda ser utilizada en otros horarios del día por toda la ciudadanía”.