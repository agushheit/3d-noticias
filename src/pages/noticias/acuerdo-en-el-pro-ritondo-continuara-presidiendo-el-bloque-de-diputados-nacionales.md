---
category: Agenda Ciudadana
date: 2021-11-24T06:00:04-03:00
thumbnail: https://assets.3dnoticias.com.ar/ritondo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: 'Acuerdo en el PRO: Ritondo continuará presidiendo el bloque de diputados
  nacionales'
title: 'Acuerdo en el PRO: Ritondo continuará presidiendo el bloque de diputados nacionales'
entradilla: 'El diputado nacional, cercano a María Eugenia Vidal, quedó en condiciones
  de pelear por la conducción del interbloque de Juntos por el Cambio, que actualmente
  ocupa el radical Mario Negri.

'

---
Las figuras centrales del PRO alcanzaron un acuerdo en la Cámara de Diputados y ratificaron que Cristian Ritondo continuará presidiendo el bloque del partido amarillo.

"Mantuve una excelente reunión con el Diputado Álvaro González y llegamos a un acuerdo de integración general que le propondremos a todos los diputados donde Cristian Ritondo seguirá Presidiendo el bloque de Diputados del @proargentin para que sea consensuado por el conjunto", tuiteó el diputado nacional electo Gerardo Milman, mano derecha de Patricia Bullrich, quien conduce al sector de "los halcones" del PRO desde afuera del Congreso nacional.

Según había trascendido, la presidenta del PRO pretendía colocar como jefe de la bancada a Waldo Wolff, pero desistió al no contar con el consenso interno suficiente para desplazar a Ritondo, que sigue gozando del apoyo mayoritario.

Muy cercano a María Eugenia Vidal, Ritondo ahora quedó en condiciones de pelear por la conducción del interbloque de Juntos por el Cambio, que actualmente ocupa el radical Mario Negri.

Sin embargo, un sector importante de la UCR y de la Coalición Cívica apoya la continuidad del cordobés por su experiencia y buen desempeño verificado al frente de esa función.

Las negociaciones que culminaron en la ratificación de Ritondo las llevaron Milman y Álvaro González, siendo este último la principal espada del jefe de Gobierno porteño, Horacio Rodríguez Larreta, en la Cámara de Diputados.

González, que ocupa la vicepresidencia del bloque del PRO, medió para conseguir la continuidad de Ritondo al frente de la bancada amarilla.

"Nosotros no estamos discutiendo el rol de Ritondo sino el perfil de un bloque que debe ser representativo de la angustia social y de lo que la sociedad espera que seamos capaces de representar", había destacado más temprano Bullrich en declaraciones periodísticas.

"Yo construí el partido bajo la representación social y así entramos a las elecciones y no me voy a mover un ápice de ahí. No voy a discutir cargos, no voy a discutir posiciones internas. Voy a discutir de representación con la sociedad", completó la ex ministra de Seguridad.

Antes de la confirmación, Bullrich ya había adelantado el lunes por la noche que no iba a cuestionar la continuidad de Ritondo como presidente del bloque: "Como Presidenta del PRO, sostengo la continuidad institucional de nuestro Bloque con @cristianritondo como Presidente. Y lucharé para que nuestros votantes se sientan representados en cada decisión. Un Bloque contra el populismo y por un país donde el esfuerzo sea la vara".

Más allá de este acuerdo, el desafío para el sector de "los halcones", que tiene en sus filas a diputados como Fernando Iglesias y Waldo Wolff, además de los electos Milman, Sabrina Ajmechet y Hernán Lombardi, será articular consensos con el ala moderada del partido, aunque sin bajar las banderas de lo que consideran los valores fundacionales del PRO.