---
category: La Ciudad
date: 2021-10-16T06:00:37-03:00
thumbnail: https://assets.3dnoticias.com.ar/PEDRODEVEGA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Los Hornos: se harán obras de pavimento, desagües e iluminación en Pedro
  de Vega'
title: 'Los Hornos: se harán obras de pavimento, desagües e iluminación en Pedro de
  Vega'
entradilla: 'Emilio Jatón firmó con el representante de la empresa Winkelmann SRL
  el contrato para concretar las tareas que fueron proyectadas por la Municipalidad
  y financiadas por el Plan Incluir. '

---
La Municipalidad firmó el contrato de obra que dará inicio de las tareas de pavimento, desagües e iluminación sobre calle Pedro de Vega, entre la avenida Peñaloza y Gobernador Freyre, en un sector de barrio Los Hornos. Se trata de un proyecto del intendente Emilio Jatón que cuenta con fondos provinciales por $ 52.629.624.

Además, el municipio está ejecutando el plan “Santa Fe se Ilumina”, por lo que se colocarán más de 100 luminarias led sobre Gobernador Freyre, entre Pedro Ferré hasta Estanislao Zeballos, y en Gorostiaga desde Peñaloza hasta Gobernador Freyre, en el marco de la línea Avenidas de tu Barrio.

“Es un barrio que durante varios años no tuvo obras y empezamos a trabajar con los vecinos y logramos que los fondos lleguen a Los Hornos. Ahora se firma el contrato con la empresa, el proceso fue largo pero en los próximos días la calle Pedro de Vega dejará de ser de tierra y con zanja para pasar a tener pavimento, cordón cuneta e iluminación”, manifestó Jatón con mucha alegría, luego de firmar el contrato con Hugo Nagel de la empresa Winkelmann SRL que se encargará de los trabajos.

Siguiendo esta línea, agregó: “Es una obra muy esperada y comprende toda la extensión de Freyre a Peñaloza. Acá hay un centro de salud y el acceso era difícil y ahora va a tener asfalto, con los desagües en cada esquina, con iluminación led, con rampas para discapacitados; es una obra completa que le va a cambiar la vida a la gente”.

Luego, el mandatario santafesino hizo mención a la obra de iluminación que se extenderá a otras calles del barrio: “Pedro de Vega va a quedar totalmente iluminada y eso es progreso pero también seguridad. También les traigo la noticia de que además de esta calle vamos a iluminar, en el marco de Avenidas de tu Barrio, Gobernador Freyre y Gorostiaga, es decir que barrio Los Hornos casi en su totalidad va a quedar iluminada”.

**Un sueño cumplido**

Silvia Sterbler trabaja hace tres años en el Centro de Salud de barrio Los Hornos y destacó la importancia de estas obras porque “pone en valor el barrio; es una calle que va a unir Gobernador Freyre con Peñaloza, va a mejorar la circulación y permitir que los vecinos le cambien la cara a sus viviendas porque no es lo mismo tener calle de tierra que asfalto”, dijo y agradeció “a todos los que hicieron este sueño realidad”.

Por su parte, la concejala Laura Mondino se sumó a la alegría del barrio y al respecto dijo: “Este barrio, como otros, hace 20 ó 30 años que están olvidados y que necesitaban obras como esta de calle Pedro de Vega dónde se va a hacer el pavimento y además la iluminación, que van a mejorar significativamente toda la zona. Esta preocupación que estamos teniendo con la inseguridad, desde el municipio consideramos que el principal aporte es la iluminación, sobre todo, en aquellas calles y avenidas que son de acceso”.

**En detalle**

La pavimentación y desagües en la calle Pedro de Vega resultan de suma trascendencia para los 17.584 vecinos que se encuentran dentro de la jurisdicción de la vecinal República de Los Hornos en virtud de las mejoras que ellas traerán a su calidad de vida. Estos 500 metros de calle constituyen una vía de penetración al barrio desde la avenida Peñaloza.

Estos 500 metros de pavimento se harán de hormigón en una calzada de 8 metros de ancho. En toda la extensión de la obra se ejecutarán las bocas de tormenta y de registro y troneras necesarias, con sus correspondientes cañerías de desagües de diámetros variables y conexiones a la red principal existente. Asimismo, se contemplan las acometidas para el saneamiento de las cunetas existentes.

Además se harán obras complementarias como rampas peatonales, señalización horizontal y regularización de veredas y desagües domiciliarios, como así también la provisión de columnas de iluminación y artefactos led.