---
category: Agenda Ciudadana
date: 2021-03-21T08:15:21-03:00
thumbnail: https://assets.3dnoticias.com.ar/API.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Le piden informes al gobierno por incompatibilidades del titular de la API
title: Le piden informes al gobierno por incompatibilidades del titular de la API
entradilla: La Cámara de Diputados aprobó un proyecto que cuestiona el doble rol del
  titular de la oficina impositiva que, además de dirigir el organismo, ejercería
  funciones como ejecutor fiscal.

---
La Cámara de Diputadas aprobó el pedido de informes al gobierno provincial para que explique por qué el titular de la Administración Provincial de Impuestos (API), Martín Ávalos, lleva a cabo procedimientos de apremio (cobro de deudas) por sí mismo y, por lo tanto, percibe importantes honorarios que provienen de los contribuyentes, cuando el cargo que ocupa es incompatible con dicha tarea.

La presentación del bloque Socialistas advirtió en su momento que el accionar del funcionario choca con las obligaciones establecidas en la Ley de Ética Pública N°13.230 y lo “hacen pasible de sanción o de remoción”.

“En noviembre del año pasado presentamos un pedido de informes sobre una situación inédita en la historia reciente del organismo recaudador”, sostuvo la diputada Lorena Ulieldín, autora de la iniciativa, a la vez que apuntó contra “la falta de gestión y planificación del organismo recaudador, y la imprevisión de sus autoridades, que quedaron en evidencia con el aumento desmedido, arbitrario e injustificado sobre el impuesto a las Patentes”.

**Accionar dudoso**

Según Ulieldín, el actual administrador de Ingresos Públicos debe dar explicaciones sobre “su accionar dudoso” ya que la realización de procedimientos de apremio por su parte “carece de un sentido de utilidad pública” al contar la máxima autoridad de la API “con una dotación de procuradores fiscales a los cuales debería coordinar y verificar, entre otras atribuciones de coordinación”.

La legisladora consideró que “está claro que la elección del administrador provincial de realizar estas actividades por sí mismo, y no otras (como por ejemplo la atención al público), persigue un claro interés personal”, dado el “importante ingreso extra” que se suma “al sueldo que de por sí ya cobra como alto funcionario público”.

Dicha conducta vulnera las estipulaciones de la Ley de Ética Pública, según la legisladora, ya que el funcionario “privilegia sus propios intereses particulares en lugar del interés público”.

Además, Ulieldín señaló que “siendo una de las funciones inherentes al cargo de administrador provincial la de velar por el cumplimiento de las obligaciones fiscales de los contribuyentes”, es decir que paguen, “como ejecutor de ese pago estaría percibiendo un beneficio personal al recibir honorarios del particular por su ejecución”.

A su vez, la diputada señaló que dicho accionar “viola el principio de abstenerse de intervenir en todo asunto comprendido en alguna de las causas de excusación previstas en la ley procesal civil”, vulnerando específicamente el artículo que establece “la incompatibilidad de los funcionarios cuando estos tengan interés en el pleito y cuando sean acreedores”.

**Improvisación como marca de gestión**

Por otra parte, la legisladora recordó también que fue el propio Avalos, como responsable del área, el que no advirtió la situación que se generaría con la actualización de los aforos de patentes incorporando los nuevos valores de los vehículos para tomar la escala. “La improvisación del gobierno provincial campea en todos lados y la API no es la excepción. En este caso, no advirtieron que la simple aplicación de la normativa vigente iba a producir el aumento sobre ese impuesto y en la magnitud en la que efectivamente se produjo”, describió.

En ese marco, Ulieldín consideró “insólito” que el actual secretario de Ingresos Públicos “intente presentar la reducción del impuesto como un logro del gobierno cuando la solución vino de la mano de cinco proyectos, de todos los sectores políticos, que se presentaron en la Legislatura para subsanar la impericia del Poder Ejecutivo antes de que remitiera su propio proyecto de ley”, según aclaró.

**Las explicaciones solicitadas**

Dentro del pedido de informes aprobado en la Cámara baja se solicita saber si la norma que designa al administrador provincial de impuestos como ejecutor fiscal es una auto-resolución del administrador general, una resolución de la Secretaría de Ingresos Públicos, una resolución del Ministerio de Economía o un decreto del Poder Ejecutivo.

También se pregunta si a los administradores regionales o a otras autoridades superiores de la API “se les concedió también esta prerrogativa incompatible de doble función y por ende doble remuneración”.

Asimismo, solicitan, respetando el secreto fiscal de los contribuyentes, “la nómina y fecha de los procesos de apremio que se iniciaron desde el 10 de diciembre de 2019, detallando los casos que se cobraron y los honorarios estipulados, con expresa mención de los tramitados de forma directa o bajo patrocinio letrado por el administrador provincial de Impuestos”.

Por último, la iniciativa pregunta “qué controles hizo el Área de Control de Gestión sobre el cobro de honorarios por apremios para verificar que se ajusten a la normativa vigente, toda vez que el administrador provincial de Impuestos es la máxima autoridad de control y, concomitantemente, sería sujeto controlado”.