---
category: La Ciudad
date: 2021-12-14T06:00:06-03:00
thumbnail: https://assets.3dnoticias.com.ar/mosquitodengue.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: En Santa Fe no se registran casos de dengue, pero hay alertas por el verano
title: En Santa Fe no se registran casos de dengue, pero hay alertas por el verano
entradilla: " En todo el país de enero a octubre de este año se registraron 3.972
  infectados por el Aedes aegypti. En la capital provincial continúan las campañas
  de prevención."

---
Las lluvias de los últimos días en la ciudad y alrededores, sumado a las altas temperaturas, hacen que comience la época en que los mosquitos encuentran sus condiciones ideales para proliferar. Si bien desde el Ministerio de Salud de la Provincia confirmaron a El Litoral que hasta el momento no hay casos activos de dengue en toda la bota santafesina, las alertas se encienden ante la llegada de la temporada estival.

 A nivel nacional, de enero a octubre (hasta la semana 42 de este año), se confirmaron por laboratorio 3.972 casos de dengue, lo que marca una tendencia a la baja si se consideran los 58.435 casos confirmados a lo largo de 2020, récord histórico de contagios del vector, según datos de la Dirección Nacional de control de enfermedades transmisibles y el último boletín epidemiológico del Ministerio de Salud.

Este medio entrevistó a Mariana Maglianese, responsable de Control Vectores de la región Sana Fe de Salud, para conocer qué situación se registró en el último tiempo, qué se prevé para este verano en relación a la llegada del Aedes aegypti a la provincia y cómo se trabaja en la prevención. 

"Hay que referenciar lo que sucedió años anteriores. Por ejemplo en el 2019-2020 hubo una cantidad de brotes extraordinarios en la provincia de Santa Fe (5.005 casos), que condijo con lo que pasó en la nación (más de 70 mil) y a nivel de toda Latinoamérica (más de 3 millones de contagios registrados), es decir que fue una epidemia importante y extraordinaria", recordó Maglianese.

 Respecto a lo que sucede actualmente, la especialista indicó que "en esta época del año, a partir de octubre, el mosquito se encuentra con un régimen de lluvias intermitentes, fotoperíodo (12 horas de luz natural y 12 de oscuridad), sumado a las altas temperaturas, hay mejores condiciones para el nacimiento del mosquito". 

 Una de las señales de alerta en la que se pone foco es en el regreso de los viajes al exterior, sobre todo a países endémicos como Brasil, Paraguay o Bolivia, donde todo el año está presente la enfermedad. "Tenemos una época del año en el que el mosquito y la enfermedad está presente de forma habitual, y en estos países endémicos hay un incremento de casos normal para esta época. Al haber una apertura mayor y más personas que viajan a estas zonas". 

 **Prevención: descacharrado y repelente**

 Ante posibles brotes y para mantenerse alerta por casos de dengue, Maglianese destacó que "hay que estar atentos y hacer vigilancia a los casos de la gente que viene con la enfermedad. Pero lo que hay que prevenir en estos momentos, es que la gente que se está por ir de vacaciones tenga en cuenta utilizar la protección con repelente que es 100 % efectiva". 

 Una de las acciones que más funciona para prevenir la propagación del mosquito es el descacharrado. Sobre esta tarea, que puede llevar adelante cualquier vecino con el solo hecho de evitar dejar recipientes con agua estancada, la responsable de Control Vectores comentó: "De enero hasta junio no paramos de hacer trabajo de descacharrado en territorio", y agregó que el momento ideal es cuando las temperaturas están por debajo de los 8°C, "no se encuentran hembras queriendo encontrar recipientes para oviponer, por eso el descacharrado es mucho más efectivo en invierno que en verano", destacó.

 La tarea de descacharrado se interrumpió durante julio y agosto cuando se dio el pico de casos de Covid-19, por lo que desde la Provincia aprovecharon este tiempo para realizar capacitaciones internas sobre vectoriales. "En septiembre se retomó el trabajo en terreno y con esto apuntamos a bajar la densidad de mosquitos", señaló Maglianese y agregó que en las últimas semanas se realizaron estas tareas en barrios como San Martín, Pompeya, Los Ángeles, y esta semana iniciaron en Los Troncos.

 "La idea es seguir con el descacharrado sistemático y pensarlo como una política sostenida todo el año en todas las localidades de la provincia, y así bajar la densidad del mosquito y el riesgo de circulación", aseguró y remarcó que "el patio de mi casa puede tener 20, 30 o 40 metros, pero el mosquito puede volar 100 metros a la redonda, por eso introducimos el concepto de ´manzana saludable\` y de pensar en que nuestros vecinos tienen que ser nuestros aliados". 

 **¿Aumento de casos?**

 En una nota realizada por Télam, el director Nacional de Control de Enfermedades Transmisibles, Hugo Feraud, alertó sobre estudios preliminares que indicaron que para la nueva temporada 2021-2022 podría haber más casos, "debido a que hubo un aumento de notificaciones en Brasil en relación al año pasado". 

Según detalló el último boletín epidemiológico de vigilancia publicado por la cartera sanitaria de Nación, este año se registraron tres serotipos circulantes en el país, la mayor parte de los casos tipificados pertenecieron al DEN-1, en un 93 %; mientras que los casos que correspondieron al DEN-2 fueron del 3 % y los del DEN-4, un 4 %. 

Los últimos casos confirmados se registraron en junio en las provincias de Jujuy, La Rioja y Salta. Los datos históricos informados por el área muestran los picos de casos que se dan en ciclos de tres a seis años aproximadamente, donde luego del récord de casos del año pasado, el 2019 solo había registrado 2.794 casos de dengue confirmados por laboratorio mientras que en el 2018 no superaba los 2.100.

En 2016, cuando se registraba el récord histórico antes del registro de 2020, se informó un incremento de infecciones que se posicionaron en los 41.207 casos. Mientras, en el 2014 los registros fueron sólo de 490 y cinco años después, en 2009, se confirmaron 26.923. Durante 2001 y 2005 no se registraron casos en la Argentina, años cercanos a cuando el vector comenzó a aparecer en los registros de nuestro país, con 330 infecciones confirmadas en 1998.

Fuentes del Ministerio de Salud, confirmaron que las fases de análisis clínico de las vacunas desarrolladas a nivel local contra el vector se están realizando para mejorar la antigenicidad y "está mejorando con resultados prometedores". Según las fuentes, la vacuna está en proceso de prueba porque no cubría todos los serotipos circulantes en el país y pronto se presentarán papers con novedades al respecto.