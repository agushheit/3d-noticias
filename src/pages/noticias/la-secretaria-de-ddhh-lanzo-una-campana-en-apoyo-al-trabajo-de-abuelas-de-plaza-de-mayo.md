---
category: Agenda Ciudadana
date: 2021-10-23T06:00:43-03:00
thumbnail: https://assets.3dnoticias.com.ar/MADRES.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: La Secretaría de DDHH lanzó una campaña en apoyo al trabajo de Abuelas de
  Plaza de Mayo
title: La Secretaría de DDHH lanzó una campaña en apoyo al trabajo de Abuelas de Plaza
  de Mayo
entradilla: '"Esta campaña tiene que ver con la lucha de Abuelas de Plaza de Mayo
  y se hace conjuntamente con ellas", indicó el titular del organismo, Horacio Pietragalla
  Corti.'

---
La Secretaría de Derechos Humanos lanzó la campaña "Argentina Unida Te Busca", para colaborar con el trabajo que realizan las Abuelas de Plaza de Mayo desde hace 44 años en la búsqueda de nietos apropiados durante la última dictadura cívico militar, y su titular Horacio Pietragalla Corti destacó que "ahora la búsqueda va a ser transversal y va a participar el Estado argentino".  
  
La campaña, queconsistirá en una primera etapa en la difusión de diferentes piezas en las redes de la Secretaría y el Ministerio, coincide con el Día Nacional por el Derecho a la Identidad que se conmemora este viernes y los 44 años que cumple la organización que encabeza Estela de Carlotto.  
  
"Esta campaña tiene que ver con la lucha de Abuelas de Plaza de Mayo y se hace conjuntamente con ellas", explicó Pietragalla Corti en declaraciones a Télam y dijo que "la idea es concientizar a nuestra sociedad de que todavía hay aproximadamente 250 personas que no conocen su verdadera historia, que no conocen su identidad".  
  
La campaña, que en una segunda etapa será territorial, "va a durar los dos años que quedan de nuestro Gobierno", aseguró el funcionario y manifestó que se trata de "llegar a esas personas que pueden haber fantaseado con ser adoptadas y que lo dejaron ahí, y que puede haber cosas de su intimidad que le hacen ruido; la idea es hacer despertar".  
  
"La diferencia es que ahora la búsqueda va a ser transversal, con el Estado argentino en la búsqueda", destacó y manifestó que "la idea es que todos los ministerios, a lo largo de estos dos años, vayan haciendo acciones para ir buscando y llevando adelante esa concientización que necesitamos para recuperar a esos hombres y mujeres".  
  
En ese sentido, dijo que todos los ministerios "estarán involucrados, el Poder Ejecutivo, y vamos a hacer una propuesta al Poder Legislativo".  
  
"La idea es que todos podamos sumarnos a esta campaña, por eso la consigna: Argentina Unida te Busca", señaló.  
  
Pietragalla Corti contó, en ese contexto, que se busca que "sea una campaña bien federal" y sostuvo que "ya hablé con (el ministerio de) Medio Ambiente, por ejemplo, y esta consigna estará en sus oficinas de atención al turismo".  
  
En la difusión de las piezas también se sumará el Consejo Federal de Derechos Humanos, que es donde participan las provincias a través de las secretarias provinciales de Derechos Humanos, contó.  
  
"Tenemos dos años para ir concientizando para que todo el Estado vaya trabajando la temática y se sienta que el Estado está buscando y quiere devolver esas identidades que robó hace muchos años", expresó.  
  
En ese sentido, dijo que son "130 los nietos recuperados y aproximadamente 250 son los que faltan encontrar" y destacó que "claramente esta campaña tiene que ver con acompañar la lucha de Abuelas, que el Estado no acompañó, para tratar de devolver parte de eso".  
  
"Yo soy parte de esa lucha también", señaló y recordó que "recuperé mi identidad en abril de 2003".  
  
Pietragalla Corti explicó que la campaña "consiste en concientizar a las personas que tienen entre 40 y 46 años y pueden estar dudando de su identidad" y manifestó que para eso "vamos a indagar para tratar de llamar la atención de esa franja etaria".  
  
En ese sentido, señaló que en con las piezas de difusión "estamos apelando al recuerdo de la infancia".  
  
"La primera pieza tiene que ver con un cassette y una birome Bic. Quienes somos de esta generación sabemos que los casettes se rebobinaban con una birome", dijo.  
  
Asimismo, expresó que "también va a salir un Topi, que era un muñequito que se tambaleaba y nunca se caía", típicos de ese tiempo.  
  
El Día Nacional por el Derechos a la Identidad fue instituido en 2004 y coincide con la fecha en la que se fundó Abuelas de Plaza de Mayo, la emblemática entidad de derechos humanos surgida para buscar a los niños y niñas nacidos en cautiverio, presidida por Estela de Carlotto, que hoy cumple 44 años de existencia.  
  
La Secretaría informó en un comunicado que la campaña relaciona dos objetos simbólicos que tenían una vinculación especial con los hábitos de los jóvenes en los años '70 y '80, y se trata de la imagen de un cassette y una birome con el fin de rebobinar la cinta.  
  
En ese marco, la pieza de difusión pregunta: "¿Sabés que relación hay entre estos dos objetos?". Y remata: "Si entendés esta imagen, seguramente naciste entre 1975 y 1983".  
  
"Si sabés que los cassettes se podían rebobinar con una birome, entonces probablemente naciste entre 1975 y 1983 y podrías ser uno de los nietos o nietas que faltan", señala uno de los mensajes que acompaña la campaña de difusión.  
  
La acción de comunicación está dirigida a hombres y mujeres nacidos entre 1975 y 1983, que duden sobre su identidad, y a la población en general que pueda aportar información para la búsqueda de nietos y nietas que faltan. sostuvo el comunicado.  
  
Además de comunicarse por las distintas redes sociales de la Secretaría -@sdhargentina-, se pueden realizar denuncias anónimas en el sitio de Abuelas –www.abuelas.org.ar- o llamando a la Comisión Nacional por el Derecho a la Identidad (Conadi) al 0800-2222-66234.