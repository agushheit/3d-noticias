---
category: Agenda Ciudadana
date: 2021-05-10T06:00:40-03:00
thumbnail: https://assets.3dnoticias.com.ar/dnu.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: LT10
resumen: Alberto Fernández arribó a Lisboa para dar comienzo a su gira por Europa
title: Alberto Fernández arribó a Lisboa para dar comienzo a su gira por Europa
entradilla: El presidente Alberto Fernández arribó esta mañana a Lisboa, la capital
  de Portugal, para dar comienzo a su gira europea, la cual se extenderá hasta el
  viernes.

---
En tanto, la comitiva que acompaña a Fernández está conformada por la primera dama, Fabiola Yañez; el canciller Felipe Solá; y el ministro de Economía, Martín Guzmán.

También el secretario General de la Presidencia, Julio Vitobello; el secretario de Asuntos Estratégicos, Gustavo Beliz; y el secretario de Comunicación y Prensa, Juan Pablo Biondi.

Hoy mismo, el Presidente mantendrá una reunión con su par portugués, Marcelo Rebelo de Sousa, informaron fuentes oficiales.

Asimismo, mañana la comitiva participará de un almuerzo ofrecido por el primer ministro, Antonio Costa, y por la tarde partirá el vuelo que los conducirá a España.

El lunes, la comitiva presidencial participará de un almuerzo ofrecido por el primer ministro, Antonio Costa, y por la tarde viajarán a España. Ya el martes, en Madrid, el Presidente irá con sus acompañantes al Palacio de la Zarzuela, donde tendrá un encuentro con el rey Felipe VI. Luego mantendrá una reunión de trabajo en el Palacio de la Moncloa con su par español, Pedro Sánchez.

El miércoles, Fernández encabezará por la mañana un encuentro con empresarios en la embajada argentina en París, y luego será recibido en el Palacio del Eliseo por el presidente Emmanuel Macron, para luego partir hacia Italia. Un día más tarde, el jefe de Estado visitará al papa Francisco en el Palacio Apostólico, donde mantendrá una audiencia privada con la máxima autoridad de la Iglesia Católica.

El mandatario almorzará con su par de Italia, Sergio Mattarella, en el Palacio del Quirinale, para luego tener una reunión de trabajo, en el Palacio Chigi, con el presidente del Consejo de Ministros, Mario Draghi. La comitiva argentina emprenderá el regreso a Buenos Aires el viernes, en un vuelo que partirá desde el aeropuerto de Roma.