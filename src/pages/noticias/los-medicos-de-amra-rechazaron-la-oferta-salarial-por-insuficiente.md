---
category: Agenda Ciudadana
date: 2021-03-20T06:15:33-03:00
thumbnail: https://assets.3dnoticias.com.ar/AMRA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Los médicos de AMRA rechazaron la oferta salarial por insuficiente
title: Los médicos de AMRA rechazaron la oferta salarial por insuficiente
entradilla: Los profesionales de la salud se opusieron a la propuesta salarial que
  hizo la Provincia y pidieron a las autoridades que mejoren la oferta.

---
Los médicos nucleados en AMRA seccional Santa Fe informa que, tras someter mediante votación la propuesta salarial, elevada por el ejecutivo provincial en el marco de la negociación paritaria 2021, con una suba del 35 por ciento en tres tramos más cláusula de revisión, el 75 por ciento de los afiliados decidieron rechazar la oferta por considerarla insuficiente.

En un comunicado, los profesionales de la salud pidieron a las autoridades provinciales que en las próximas 48 horas los vuelva a convocar y formalicen una oferta salarial superadora a los fines que se pueda volver a ser analizada y sometida a votación.

En caso de que no haya una nueva reunión paritaria y una mejora en la oferta, se llevarán adelante medidas de fuerza.