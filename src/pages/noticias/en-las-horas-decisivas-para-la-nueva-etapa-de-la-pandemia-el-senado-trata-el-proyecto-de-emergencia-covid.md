---
category: Agenda Ciudadana
date: 2021-05-20T09:35:45-03:00
thumbnail: https://assets.3dnoticias.com.ar/senado.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: En las horas decisivas para la nueva etapa de la pandemia, el Senado trata
  el proyecto de Emergencia Covid
title: En las horas decisivas para la nueva etapa de la pandemia, el Senado trata
  el proyecto de Emergencia Covid
entradilla: "El debate se dará este jueves, antes de la definición oficial sobre nuevas
  medidas para afrontar la pandemia, y en un contexto político complejo. \n"

---
El Senado sesionará hoy para tratar el proyecto del Gobierno que busca darle un marco normativo a la aplicación de restricciones para enfrentar la pandemia de coronavirus, en un clima de tensión política entre oficialismo y oposición y a la vez de preocupación compartida por el aumento de los contagios.

La presidenta de la Cámara alta, Cristina Kirchner, convocó a la sesión para las 14:00, con un extenso temario en el que se destaca la iniciativa enviada por el Poder Ejecutivo sobre "parámetros epidemiológicos y sanitarios regulatorios de la emergencia Covid-19", entre una larga lista de proyectos de declaración ajenos a la situación de la pandemia.

El proyecto ingresó al parlamento con un rechazo inicial por parte de Juntos por el Cambio que, si bien no impediría su aprobación en el Senado donde el Frente de Todos cuenta con una holgada mayoría, sí pone en duda su sanción definitiva cuando deba pasar por la Cámara de Diputados.

Ese rechazo de la oposición apunta a que el proyecto, según entienden, violenta la autonomía de las provincias, un tema que fue objeto de una fuerte discusión entre el Gobierno nacional y el de la Ciudad de Buenos Aires a cargo del referente de Juntos por el Cambio Horacio Rodríguez Larreta.

Además de las diferencias en torno a este proyecto, la relación entre el oficialismo y la oposición se tensó debido al intento del Frente de Todos de avanzar por estos días con la reforma de la Procuración General.

Sin embargo, los últimos reportes de la situación de la pandemia generaron alarma tanto en el gobierno del presidente Alberto Fernández como en la administración de Larreta y las provincias gobernadas por la coalición opositora.

Por esa razón, el contexto del debate que tendrá el Senado también estará dado por la inminente definición sobre medidas más fuertes para reducir la circulación en las que, en principio, coinciden los gobiernos de ambos signos políticos.

En el Gobierno nacional pretendían tener aprobada la ley antes del vencimiento el próximo viernes del último DNU pero, al no ver ese objetivo cumplido, mientras la Cámara alta sesione la Casa Rosada estará dando las últimas puntadas a un nuevo decreto.

El proyecto enviado por el Gobierno busca darle un marco normativo a la aplicación de no distingue entre zonas geográficas sino que establece "parámetros epidemiológicos y sanitarios" según los cuales las provincias, la Ciudad de Buenos Aires y el Gobierno nacional, podrán adoptar medidas para evitar la propagación del virus.

En este sentido, establece los criterios para determinar las condiciones de Bajo, Medio o Alto Riesgo Epidemiológico y de Alarma Sanitaria, por los cuales se regirán los distintos tipos de restricción.

El proyecto indica que los gobernadores continuarán ejerciendo "las facultades concurrentes de control y fiscalización" de las medidas de prevención, pero establece una suerte de cláusulas "gatillo" que aplicarán de forma automática cuando se alcance una situación epidemiológica más grave.

En base a la variación de los casos y el nivel de la ocupación de camas de terapia intensiva, entre otros parámetros, se tomarán medidas más fuertes mientras que cuando un distrito baje a una situación más favorable, con disminución de contagios y ocupación del sistema sanitario, se suspenderán algunas restricciones.

Respecto de las clases presenciales, el proyecto indica que su suspensión "se dispone únicamente para los lugares de Alarma epidemiológica" y prevé que, previa consulta con la provincias, el Poder Ejecutivo "podrá morigerar o dejar sin efecto esa restricción".

En los lugares de Medio riesgo sanitario, "serán las autoridades locales las que dispondrán de restricciones temporales y focalizadas", mientras que en los lugares del Alto riesgo y de Alarma, de observarse un empeoramiento de la situación, "se faculta al Poder Ejecutivo Nacional a disponerlas de formas razonable y proporcionada, previa consulta con los gobernadores".