---
category: Agenda Ciudadana
date: 2021-04-19T06:15:33-03:00
thumbnail: https://assets.3dnoticias.com.ar/clases.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: 'Buenos Aires: La Cámara de Apelaciones porteña falló a favor de la presencialidad
  de las clases'
title: 'Buenos Aires: La Cámara de Apelaciones porteña falló a favor de la presencialidad
  de las clases'
entradilla: El tribunal resolvió "hacer lugar a la queja interpuesta y en consecuencia
  admitir el recurso de apelación; disponer la suspensión de lo dispuesto en el artículo
  2°, párrafo tercero, del DNU.

---
La Cámara de Apelaciones en lo Contencioso Administrativo, Tributario y de Relaciones de Consumo de la Ciudad de Buenos Aires falló este domingo a favor de la presencialidad de las clases en el distrito, al pronunciarse sobre una presentación hecha por organizaciones de docentes contra el Decreto de Necesidad y Urgencia (DNU) del Gobierno nacional que establece la suspensión de la actividad por 15 días en el Área Metropolitana de Buenos Aires (AMBA) para contener la segunda ola de coronavirus.

El tribunal resolvió "hacer lugar a la queja interpuesta y en consecuencia admitir el recurso de apelación; disponer la suspensión de lo dispuesto en el artículo 2°, párrafo tercero, del DNU y ordenar al Gobierno de la Ciudad de Buenos Aires (GCBA) que en el marco de su autonomía y competencias propias disponga la continuidad de la presencialidad de las clases en el ámbito del territorio de la Ciudad de Buenos Aires", según el fallo.

Después de conocerse el fallo, el jefe de Gobierno porteño, Horacio Rodríguez Larreta, anunció que brindará una conferencia de prensa a las 21.30 para referirse a la situación de las clases presenciales en la Ciudad a partir del lunes.

La Cámara de Apelaciones está compuesta por los jueces Marcelo López Alfonsín, Laura Alejandra Perugini y Nieves Macchiavelli.

La camarista Macchiavelli había sido recusada por los abogados Adrián Albor y María Cecilia Fernández por ser hermana del secretario de Ambiente del Gobierno porteño, Eduardo Macchiavelli, quien también se desempeña como secretario general del partido Pro del distrito.

"La doctora Macchiavelli es hermana del principal armador político de Rodríguez Larreta. Eduardo Macchiavelli es funcionario del GCBA y secretario general del partido político al que pertenece el alcalde porteño. Así las cosas, esta parte posee un fundado temor de que no pueda decidir con ecuanimidad", habpían plantearon los letrados Albor y Fernández.

Sin embargo, López Alfonsín y Perugini, los otros dos jueces del tribunal, rechazaron esa presentación, al considerar que "no existe la mentada conexidad, razón por la cual no se vislumbra una vinculación entre los mencionados expedientes que le otorguen legitimación a los presentantes en esta causa".

La Justicia falló ante la presentación de un grupo de docentes nucleados en la ONG Abramos las Escuelas y el espacio Centro de Estudios de Políticas Públicas.

Por su parte, la Unión de Trabajadores de la Educación (UTE) de Capital ratificó que el lunes realizará un paro, al advertir desde sus redes sociales que "en las escuelas siempre enseñamos a respetar el orden de prelación de las leyes, la Constitución Nacional y el pleno ejercicio de la democracia en el marco del Estado de Derecho".