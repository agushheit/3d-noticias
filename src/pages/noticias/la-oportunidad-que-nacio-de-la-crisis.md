---
category: El Campo
date: 2020-12-13T14:37:59Z
thumbnail: https://assets.3dnoticias.com.ar/veterinarias.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: La oportunidad que nació de la crisis
title: La oportunidad que nació de la crisis
entradilla: 'La imposibilidad de procesar la leche de cabra ordeñada en la Escuela
  Granja de Esperanza obligó a suscribir un convenio con una planta láctea de la zona. '

---
**Nuevas oportunidades comerciales para un producto de alta calidad y demanda.**

Las crisis suelen generar oportunidades, y este año de pandemia no fue la excepción. Si bien el programa caprino de la Facultad de Ciencias Veterinarias de la UNL se ajusta a un objetivo académico, necesitaba resolver la salida industrial a la producción anual de su tambo caprino, momentáneamente interrumpida por la pandemia. Por eso, avanzaron en un acuerdo con una láctea de Colonia Cavour, para sumar conocimientos, y permitir que eventualmente los alumnos se sumen a la etapa final del proceso.

"Eventualmente pudimos desarrollar en conjunto con esta Pyme láctea con varios años de trayectoria nuestra propia producción", afirma el Méd. Vet. Sebastián Palmero, docente de la cátedra de caprinos de la alta casa de estudios. "Al no poder hacerlo en la Escuela, salió la oportunidad de hacer los quesos con ellos. La empresa ha dado un puntapié inicial al habernos recibido, y generar un desarrollo tecnológico con las distintas variedades de este queso", agrega.

Campolitoral recorrió las instalaciones de la Unidad Académico Productiva de la Universidad donde nació el proyecto. Se trata de un predio que alquila la Asociación Cooperadora, y donde se desarrollan distintos módulos productivos. En este caso, el grupo caprino ya tiene 11 años en el estudio de la especie, y viene incursionando en los posibles mercados de la leche de cabras. "Y se ve con mayor frecuencia la utilidad y consumo de este producto", admite el docente.

# **Vuelta de tuerca**

Palmero explica que -pandemia mediante- este año pudieron hacer el tambo un poco más extenso a comparación de otros años. "El tambo tiene la impronta académica del aprendizaje en la fase de ordeño, como puntapié inicial para conocer los puntos críticos de esta fase productiva; y llevar adelante la producción de leche". Este año lograron 2970 litros, alcanzando una producción histórica, "que con recursos ajustados, la solidaridad y el voluntariado de los chicos pudimos llevarlo adelante".

Si bien el programa tuvo sus vaivenes, el producto ya está registrado desde hace 8 años, y esperan que este nuevo vínculo les permita dar mayor difusión a la actividad de la producción a baja escala.

"Por otro lado, queremos difundir la idea que **los pequeños productores en pocas hectáreas pueden llevar adelante perfectamente esta actividad**. Es que este nuevo vínculo nos permitió que los productores analicen, y puedan contar con un espacio de salida de su producción que antes no existía", explica.

# **La pasión, el único secreto**

Martín Noroña es el gerente de la planta láctea "La Chacra", que se subió a esta iniciativa, para completar una visión del negocio lácteo que arrancó hace tiempo, pero que tiene mucho que ver con la calidad.

***

![](https://assets.3dnoticias.com.ar/queseros.jpg)

***

"Ya habíamos trabajado algunos quesos especiales; pero a través del convenio con la UNL pudimos acceder a procesar la leche de cabra, que ellos no podían trabajar por la pandemia, con la particularidad que uno de nuestros maestros que queseros ya había trabajado ese tipo de leche, por lo que fue más fácil adecuarnos", le dice a Campolitoral desde una oficina vidriada que permite contemplar el trabajo cotidiano de la planta. "Acá no hay nada que ocultar, todo tiene que ser transparente", expresa con relación al compromiso con la "camiseta" de todos los integrantes de la organización.

"En referencia a la experiencia caprina, sostuvo que fue algo muy lindo, también porque los chicos de la escuela aprenden a hacer quesos en nuestra planta. Y eso permite que vean como es la dinámica dentro de una empresa eso es un disparador, surgen nuevas ideas".

***

![](https://assets.3dnoticias.com.ar/cabra.jpg)

***

Según Noroña el queso de cabra es muy rico, es muy sano y es recomendado. "Hay gente que no es tolerante a leche de vaca y sí a la de cabra, inclusive se usa como sustituta de la leche materna".

Actualmente, están produciendo un queso Sardo del estilo del Pecorino (una variedad italiana de queso curado elaborado a partir de la leche de oveja); e inclusive llegar a producir un camenbert (queso de leche de vaca producido en la región francesa de Normandía), con la idea de ir cambiando el tipo de producto, para brindar la posibilidad de generar mejores quesos, y que ellos puedan obtener mejores ganancias".

# **ADN quesero**

Noroña recuerda que arrancaron en 2001 "cuando uno de mis hermanos estaba haciendo una maestría en lácteos en Buenos Aires". Acomodaron un viejo granero en Colonia Rivadavia y la cosa empezó a crecer, por eso las exigencias eran mayores y se mudaron a Colonia Cavour, donde se levantó la actual planta, unos 10 años atrás. "El traspaso implicó que tengamos que trabajar de otra forma, con otra dinámica, asesorados en las cuestiones de calidad", reconoce.

"Tuvimos suerte con una fábrica amiga que nos permitió adquirir maquinaria en buen estado, logrando una planta modelo con capacidad de hasta 50 mil litros por día o bajar a 20 mil con la misma cantidad de gente. Tenemos técnicos, asesores, y eso nos permitió mejorar la calidad de lo que estábamos haciendo". 

Con el tiempo, empezaron a concursar para medirse y comenzaron a ganar premios. "Nos fuimos posicionando con un buen producto en cuanto a la relación precio calidad", admite.

Además, en 2012 empezaron con el sistema Kosher, que implica aumentar las exigencias de trabajo. "El mercado judío ortodoxo tiene exigencias elevadas, y hoy aplicamos a 3 auditorías (una de las pocas plantas que lo cumplen en el país). Ellos precisan tener un auditor para cada paso del proceso, desde el ordeñe", afirma.

También explica que prefieren bajar la ganancia pero mantener la calidad, **apuntando a los almacenes y evitando las grandes cadenas**. Hoy llegan fuerte a toda la provincia de Santa Fe y el norte del país.

"**No hay tantos secretos en la industria quesera, respetar las materias primas es fundamental**", sostiene. "Hoy tenemos una política que nos ha mantenido, que es apelar a la calidad, apuntamos a un producto de despensa, no trabajar con los grandes monopolios, preferimos llegar con una distribuidora más chica que crezca con nosotros, pero manteniendo la calidad de las materias primas y de los insumos es la gran receta. Quien los consumió lo sabe, y uno lo ve en los volúmenes de venta sobre como vamos creciendo".

Finalmente, apunta a la coherencia. "Lo importante es mantenerse, no tanto el anhelo de llegar. Y en estos momentos, durante 10 años lo pudimos hacer con una conducta, con la idea de mantener conforme al cliente".

# **Dibuje, maestro**

Marcos Riva es el "maestro quesero" de La Chacra, y nos explicó lo que ello significa. "Ser maestro quesero implica una responsabilidad enorme", afirma con modestia. "El maestro primero tiene que tener la camiseta puesta; sentir lo que se hace y cuidar a los quesos como si fueran sus hijos", agrega con orgullo.

Para Marcos, ser jefe de planta es estar a cargo de la organización y de que no falte nada, sabiendo todo lo que pasa, con la calidad el producto como premisa. Así, **trabajan unos 20 mil litros diarios, un 60 % quesos de pasta blanda frescos, y el resto quesos semiduros; con ojos y duros**.

Los tiempos de proceso no varían, aunque sí los de maduración. "Un blando cremoso o por salut, desde que llega la leche a la góndola son 30 días. Una barra son 60. Y los duros, entre 90 y 120 días. Y un sardo estacionado sale con un año de estacionamiento. **La calidad de leche es lo más importante, no hay otra regla**", afirma.

Con respecto a la nueva línea de trabajo, destaca que con la Facultad están haciendo quesos de cabra, que fueron introducidos en la línea de producción. Se los hace como quesos especiales, con otros cuidados, y por ahora salen blancos o saborizados.

"Para nosotros es un desafío, porque **aparte de un producto nuevo e innovador, valoramos trabajar junto con la Facultad** intercambiando conocimientos con ellos, y el que los chicos vengan a aprender acá también. Eso ha ido sumando otros proyectos en conjunto como para capacitar al personal, para que esto pase a ser una fábrica escuela que permite capacitar a los operarios que permitan entrar a la industria".

Según Riva, lograr calidad implica implementar un sistema de trabajo, y no cualquiera hace un queso de calidad. "Estamos orgullosos de lo que hacemos, que en nuestra planta no tenemos un sector destinado a las devoluciones, y eso no es fácil. El queso es el resultado de un sistema de calidad bien implementado. Y tener un grupo de trabajo acorde a esa idea". **Lo dice un maestro.**