---
category: La Ciudad
date: 2021-09-28T06:00:27-03:00
thumbnail: https://assets.3dnoticias.com.ar/CHAUMANGA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Ya desarmaron la manga y trabajan en rehabilitar el tránsito en avenida Freyre
title: Ya desarmaron la manga y trabajan en rehabilitar el tránsito en avenida Freyre
entradilla: Estiman que la sincronización de semáforos podría estar operativa a partir
  de las ocho de este martes. El transporte urbano volvería a circular a partir del
  miércoles.

---
Este lunes, pasadas las 13 comenzó el operativo para desarmar la manga que conectaba el hospital Cullen con el de Campaña que se había montado en el predio del Liceo Militar.

El municipio trabaja en la sincronización de semáforos para habilitar el tránsito vehicular, algo que -estiman- podría suceder a partir de las ocho de este martes. En tanto, el servicio de transporte urbano volvería a circular por Av. Freyre a partir del miércoles.

"Estamos al aguardo a que el hospital Cullen haga el levantamiento de la manga y una vez que se haga el mismo, a las 24 horas, estaríamos comenzando el levantamiento del vallado y la coordinación semafórica que tuvo que modificarse en su momento para poder habilitar los giros correspondientes", explicó el subsecretario de Movilidad y Transporte, Lucas Crivelli.

Con respecto al transporte urbano, destacó que "si todo funciona como corresponde el miércoles podríamos estar normalizando el servicio de transporte urbano de pasajeros restituyendo las líneas a su recorrido original",

"En su momento se hicieron los cambios necesarios para que puedan seguir prestando los servicios con un recorrido que pasaba por otras cuadras", recordó y remarcó: "La apertura al tránsito no es inmediata, del mismo modo que tuvimos que cortar el tránsito 48 horas antes de que se coloque la manga. Hoy los giros de los semáforos están con otra circulación. Si nosotros habilitamos el tránsito en este momento sería bastante peligroso en cuanto a la seguridad vial".

Este lunes por la mañana, el director del hospital Cullen, Juan Pablo Poletti, explicó que el hospital de Campaña iba a quedar armado pero vacío por si en un futuro cercano se necesita disponer de ese lugar ante un repunte de casos Covid. La manga que hoy se está retirando quedará guardada junto a la carpa del Ejército para ser reconectada en caso de ser necesario.