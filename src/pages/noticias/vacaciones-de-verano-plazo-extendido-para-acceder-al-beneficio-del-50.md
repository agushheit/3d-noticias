---
layout: Noticia con imagen
author: "Fuente: NA"
resumen: Vacaciones de verano
category: Agenda Ciudadana
title: "Vacaciones de Verano: plazo extendido para acceder al beneficio del 50%"
entradilla: El Ministerio de Turismo y Deportes de la Nación anunció este sábado
  la extensión hasta el 20 de noviembre de dicho lapso. Es para viajar en 2021.
date: 2020-11-16T13:27:00.708Z
thumbnail: https://assets.3dnoticias.com.ar/cataratas.jpeg
---
El Ministerio de Turismo y Deportes de la Nación anunció este sábado la extensión hasta el 20 de noviembre del plazo establecido para la compra de servicios turísticos para viajar en enero de 2021 y acceder a la devolución del 50 por ciento de los gastos en crédito, en el marco del **programa PreViaje** que busca dar impulso a la economía.

> **Además, se extenderá hasta el 30 de noviembre el plazo para quienes quieran viajar en febrero de 2021 y acceder al beneficio.**

"Mediante esta prórroga se busca maximizar el aprovechamiento de los beneficios de este programa de preventa turística, la mayor inversión del Estado nacional en su historia en turismo, para impulsar la reactivación de uno de los rubros más golpeados por la pandemia", señaló en un comunicado.

La cartera turística destacó que "hasta el momento, más de 170 mil argentinos y argentinas utilizaron el programa, con una inversión de $3.000 millones y generando $1.500 millones para utilizar el año próximo".

"Cerca del 50% de los gastos realizados fueron en agencias de viaje, mientras que un 40% fue en alojamientos y un 9% en transporte", sostuvo.