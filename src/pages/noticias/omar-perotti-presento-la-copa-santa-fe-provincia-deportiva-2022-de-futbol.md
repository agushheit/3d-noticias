---
category: Agenda Ciudadana
date: 2022-06-25T11:52:30-03:00
thumbnail: https://assets.3dnoticias.com.ar/2022-06-25NID_275099O_1.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: OMAR PEROTTI PRESENTÓ LA COPA SANTA FE PROVINCIA DEPORTIVA 2022 DE FÚTBOL
title: OMAR PEROTTI PRESENTÓ LA COPA SANTA FE PROVINCIA DEPORTIVA 2022 DE FÚTBOL
entradilla: Es el primer deporte que comenzará sus competencias en la rama femenina
  y masculina, de las cinco disciplinas que incluye la propuesta.

---
El gobernador Omar Perotti, junto a la Federación Santafesina y las 19 ligas, presentó este viernes en Rafaela, la Copa Santa Fe Provincia Deportiva 2022 de Fútbol, la primera disciplina que dará inicio al certamen, en las ramas femenina y masculina, con la participación de 78 equipos en total.

En la oportunidad, Perotti manifestó la “alegría de presentar esta Copa Deportiva. Quiero agradecerles a cada una de las instituciones deportivas de la provincia por la disposición de todos y cada uno de los clubes durante la pandemia, desde sus inicios, para acompañar de la mejor manera a toda la comunidad. Es un gesto destacable. Un agradecimiento enorme que trasciende lo deportivo, mostrando un compromiso como dirigentes a toda la comunidad”, dijo el gobernador.

Y agregó: “No podemos reemplazar de ninguna manera el trabajo de cada uno de ustedes en los clubes. En cada club se forja una persona, se transmiten valores del deporte que queremos renovar con esta copa, que queremos que se multipliquen y expandan. Eso para nosotros tiene un valor incalculable, es donde queremos que cada uno se fortalezca”.

Por otro lado, Perotti también se refirió a la irrupción de la mujer en el deporte y sostuvo que “eso lleva a mejorar la infraestructura y a tener más instalaciones. Eso va a ir acompañando el crecimiento en la participación de más mujeres”.

“Queremos que esto sea algo que contagie, que cada uno de ustedes se vaya feliz de haber participado, que vuelvan, y que acerquen a más mujeres a hacer deportes. Esta copa hay que cuidarla porque expresa, en cada una de sus ediciones, el sueño de muchos. Queremos estar a la par, este es el compromiso de la provincia en el agradecimiento a todas las instituciones y deportistas”, concluyó el gobernador.

SANTA FE, UNA PROVINCIA COMPROMETIDA CON EL DEPORTE

A su turno, el ministro de Desarrollo Social, Danilo Capitani, dijo que "es una alegría enorme que se pueda llevar adelante esta copa después de dos años de parate. Hace dos meses inaugurábamos los Juegos Suramericanos, después los Juegos Santafesinos y hoy aquí la Copa de Santa Fe, esto es entender el compromiso que tiene Santa Fe con el deporte. A todos los y las participantes el mayor de los éxitos y gracias a todos los que pusieron su granito de arena para que esto se haga realidad".

Por su parte, la ministra de Igualdad, Género y Diversidad, Florencia Marinaro, manifestó que “es un orgullo estar aquí con las jugadoras inaugurando esta copa, que por primera vez cuenta con mujeres en las 5 disciplinas”; y destacó la importancia de que no haya “impedimento de jugar el deporte que les guste, de seguir sus sueños. Cuentan con nosotras para construir una Santa Fe más igualitaria, para que el juego sea un disfrute, y para que las puertas de los clubes estén abiertas a niños, niñas y jóvenes, para que lleven a la provincia de Santa Fe a lo más alto”.

En tanto, la secretaria de Deportes de la provincia, Florencia Molinero, agradeció a “toda la dirigencia deportiva por hacer esta Copa y trabajar en equipo para que podamos llevarla adelante; y al gobernador por siempre apoyar al deporte”; y manifestó que “es emocionante ver cómo el deporte va creciendo. El esfuerzo de cada uno de los deportistas y la Copa Santa Fe vienen a visibilizar esto: a todos los deportes de la provincia. Santa Fe es una provincia deportiva, el semillero del deporte, y por eso hoy lanzamos esta copa”, concluyó.

En esa línea, el Intendente de la ciudad de Rafaela, Luis Castellano, expresó que “para nosotros es un verdadero regalo poder estar hoy lanzando la parte de fútbol, tanto femenino como masculino, en Rafaela. Es un regalo porque es la copa del reencuentro, es la copa que después de los años de pandemia, que también el deporte y los clubes la han sufrido, hoy volvemos a poner el deporte en alto. Esta copa vuelve con la fuerza que necesitamos para que siga potenciando el deporte. Además, es un regalo porque nuestra Liga Rafaelina está cumpliendo 100 años”.

Por último, el presidente de la Federación Santafesina de Fútbol, Carlos Lanzaro, aseguró que “es el reconocimiento más grande que hay para una provincia, 19 ligas hoy me dan la posibilidad de representarlas, pero están todos presentes. Representan a más de 450 clubes en nuestra liga”.

“El reconocimiento -continuó- a los jugadores y jugadoras que participan. Bienvenidos a los árbitros, las árbitras que nos acompañan y a los periodistas que nos siguen. Cuidemos esta copa, es nuestro producto. Esto tiene un futuro y la posibilidad de que siga creciendo. Disfruten del deporte”, finalizó Lanzaro.

Durante la actividad, el senador Alcides Calvo entregó una declaratoria de interés al gobernador Omar Perotti por parte de la Cámara de Senadores de la provincia a la Copa Deportiva Santa Fe de Fútbol.

COPA SANTA FE PROVINCIA DEPORTIVA DE FÚTBOL 2022

La Copa Santa Fe Provincia Deportiva de Fútbol 2022, incluye la participación de clubes que militan en la Asociación Rosarina y las Ligas Cañadense, Casildense, Deportiva del Sur (Alcorta), Regional del Sud (Villa Constitución), Departamental de Fútbol San Martín, Esperancina, Galvense, Interprovincial (Chañar Ladeado), Ocampense, Rafaelina, Reconquistense, Regional Ceresina, Regional Sanlorencina, Totorense, Santafesina, Venadense, Verense y Paivense, las 19 competencias que nuclea la Federación Santafesina de Fútbol. Cabe destacar que las categorías tanto femeninas como masculinas obtendrán premios económicos.

La segunda edición de la rama femenina incluirá 16 selecciones de sus respectivas Ligas, y comenzó este viernes 24 de junio, con 4 zonas de 4 equipos cada una, donde jugarán todos contra todos, y según su ubicación clasificarán a las Copas de Oro, Plata, Bronce y Estímulo, hasta conseguir el plantel campeón.

En tanto, la quinta edición en la rama masculina, tendrá en cancha a 62 clubes y un total de 104 partidos, donde habrá tres etapas iniciales (ida y vuelta) durante los meses de junio y julio, para definir quiénes entraran en las fases de octavos, cuartos y semifinal, y son a un solo partido en los meses de agosto, septiembre y octubre, y la final donde habrá partido y revancha.

PRESENTES

Estuvieron presentes también el ministro de Gestión Pública, Marcos Corach, intendentes y presidentes comunales, autoridades provinciales, municipales y comunales concejales, dirigentes, técnicos, y jugadores de Ligas y clubes de fútbol de toda la provincia.