---
layout: Noticia con imagen
author: .
resumen: Procedimiento electoral
category: Agenda Ciudadana
title: Juntos por el Cambio duro contra la posible modificación del
  procedimiento electoral
entradilla: Expresan su total rechazo a cualquier intento de modificación del
  procedimiento electoral.
date: 2020-11-14T17:55:47.688Z
thumbnail: https://assets.3dnoticias.com.ar/diputados.jpg
url: ""
---
En un comunicado, la principal coalición opositora sostuvo que “frente a las variadas y reiteradas voces por parte de ciertos sectores del oficialismo santafesino proponiendo la modificación del régimen electoral y la vuelta a un sistema de ley de lemas, los diputados nacionales de Juntos por el Cambio – Santa Fe, expresan su total rechazo a cualquier intento de modificación del procedimiento electoral.”

En el mismo comunicado sostiene que “la derogación de la ley de lemas lograda en nuestra provincia años atrás, ha sido un histórico avance en favor de la transparencia y la democracia electoral, dejando en el pasado sistemas electorales que permitían la elección de candidatos desoyendo las voluntades mayoritarias, por lo que cualquier intento de reinstaurar sistemas de ese tipo, no es más que un retroceso inadmisible de calidad democrática”.

“Asimismo, ratificamos la necesidad de un acabado cumplimiento del proceso de internas abiertas denominado PASO, tal como ha venido sucediendo hace ya varios años desde la sanción de la ley vigente, y repudiamos las operaciones mediáticas de algunos referentes partidarios que, con la excusa de la pandemia, pretenden modificar el régimen electoral para eludir someterse a la voluntad ciudadana por haber perdido apoyo popular”.

Por último, sostienen que “las internas abiertas se instituyeron como una herramienta democrática que no puede ser abandonada simplemente porque no le convienen a un sector político. En tiempo de restricciones de derechos disfrazadas de cuidado social, se hace más necesario que nunca preservar todos los mecanismos democráticos que hacen a la soberanía popular”.