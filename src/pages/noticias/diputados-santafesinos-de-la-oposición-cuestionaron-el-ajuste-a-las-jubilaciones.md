---
layout: Noticia con imagen
author: "Fuente: Diario UNO"
resumen: Ajuste a jubilaciones
category: Estado Real
title: Diputados santafesinos de la oposición cuestionaron "el ajuste a las
  jubilaciones"
entradilla: Legisladores del PRO y de la UCR criticaron el proyecto del Gobierno
  para actualizar las jubilaciones por salario y recaudación de la Ansés.
date: 2020-11-11T22:43:59.953Z
thumbnail: https://assets.3dnoticias.com.ar/11-11-20.jpg
---
Algunos diputados nacionales por Santa Fe ya se expresaron sobre la nueva fórmula que el gobierno propone para actualizar las jubilaciones nacionales. Los primeros en levantar la voz para cuestionar el proyecto del Frente de Todos fueron los legisladores de Juntos por el Cambio, que también recordaron la oposición del justicialismo cuando en 2017 el entonces Presidente, Mauricio Macri, envió al Congreso la ley de movilidad jubilatoria que fue aprobada con fuertes disturbios afuera de la casa de las leyes.

El aspecto más polémico del proyecto es que deja de tomar a la inflación como un parámetro para medir el aumento que se les debe dar a los jubilados con el objetivo de cuidar la sustentabilidad del sistema. En otras palabras, el crecimiento de las jubilaciones no puede ser mayor al de los recursos con los que se van a pagar esos haberes.

Uno de los primeros en lanzar las críticas fue el presidente del PRO en la provincia de Santa Fe, Federico Angelini. "En la discusión respecto de la fórmula, la principal variable que se tiene que tener en cuenta es cuál es la mejor forma para garantizar que los haberes jubilatorios se actualicen correctamente y no pierdan con la inflación", escribió en Twitter.

![](https://assets.3dnoticias.com.ar/tweet-angellini.jpg "Tweet")

Ver en [Twitter](https://twitter.com/fangelini/status/1326181232149999620?s=20)

La diputada nacional del PRO por Santa Fe, Gisela Scaglia, también salió a cuestionar al Gobierno de Alberto Fernández. "Después de un año de ajustar las jubilaciones por decreto anuncian la fórmula jubilatoria y consolidan el recorte: no tomarán en cuenta la inflación. En un país que está en franca recesión se guiarán solo por la recaudación y los salarios #AjustanJubilados", dijo en redes sociales.

![](https://assets.3dnoticias.com.ar/tweet-scaglia.jpg "Tweet")

Ver en [Twitter](https://twitter.com/GiScaglia/status/1326185513875886080?s=20)

Por su parte, la diputada radical por Santa Fe, Ximena García, recordó las promesas de campaña del Presidente para contraponerlas con el proyecto que el Gobierno analiza enviar al Congreso en los próximos días.

![](https://assets.3dnoticias.com.ar/tweet-garcia.jpg "Tweet")

Ver en [Twitter](https://twitter.com/XimenaGarciaBl/status/1326195468284669952?s=20)