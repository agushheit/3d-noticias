---
category: La Ciudad
date: 2022-01-03T06:00:40-03:00
thumbnail: https://assets.3dnoticias.com.ar/BAJANTE.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: El Río Paraná se acerca al cero en la ciudad de Santa Fe
title: El Río Paraná se acerca al cero en la ciudad de Santa Fe
entradilla: La última medición arrojó 17 cm, con lo que se adelantó a las predicciones
  del INA. Bajó 19 cm en las primeras 36 horas del año

---
Por cuarta vez en los últimos seis meses, el Río Paraná mide menos de 20 centímetros en el Puerto de la ciudad de Santa Fe. 

 La última información que brindó Prefectura Naval Argentina es que el hidrómetro midió 17 cm, así como lo hizo a mediados de julio, en septiembre y en octubre del 2020. De esta manera, el río bajó dos centímetros con respecto al día anterior y 19 cm si se mira la altura del 31 de diciembre.

 Este panorama se refleja hace algunas semanas en el poco caudal de agua con el que se presentan las riberas santafesinas. En la capital provincial, lo más palpable es la Laguna Setúbal, en donde volvió a verse el suelo, al norte de los pilotes del antiguo puente ferroviario. 

 También quedan expuestas las extensas orillas con arena, barrio y la vegetación que hace más de un año ya forma parte del paisaje lagunero de la ciudad capital. 

 **Qué dice el INA**

 El último reporte emitido por el Instituto Nacional del Agua fue publicado el 29 de diciembre del año pasado y las proyecciones se adelantaron. 

 El organismo prevía una altura de 33 cm para el 4 de enero y de 16 cm para el 11 de este primer mes de 2021. 

 Sin embargo, advirtieron que “prevalecerá próximamente una condición general de disminución de los caudales entrantes al tramo argentino del río Paraná”. 

 Y agregaron que “los niveles fluviales continuarán en la franja de aguas bajas”, al tiempo de asegurar que la “tendencia climática al 28 de febrero de 2022 es aún desfavorable”.

 