---
category: Agenda Ciudadana
date: 2021-02-16T07:53:15-03:00
thumbnail: https://assets.3dnoticias.com.ar/hidrovía.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Por Juan Carlos Alderete para NCN
resumen: Hidrovía en manos argentinas
title: Hidrovía en manos argentinas
entradilla: 'En estos días se está discutiendo la concesión de la hidrovía de los
  ríos Paraná y Paragua que ha despertado distintas alertas sobre el deterioro de
  la soberanía nacional en términos fluviales y marítimos. '

---
En estos días se está discutiendo la concesión de la hidrovía de los ríos Paraná y Paraguay luego de la reciente firma del Decreto Nacional 949/2020 que ha despertado distintas alertas sobre el deterioro de la soberanía nacional en términos fluviales y marítimos. El Decreto da marcha atrás de los anuncios realizados el 28 de agosto de 2020 cuando se efectuó en Santa Fe la firma para la creación de la Administradora Federal Hidrovía Sociedad del Estado, que suponía la creación de una nueva empresa estatal que tendría una participación del 51% por parte del Estado Nacional y un 49% divido en 7 provincias (Buenos Aires, Chaco, Corrientes, Entre Ríos, Formosa, Misiones y Santa Fe) para controlar los procesos de licitación y llevar adelante obras complementarias y accesorias a la hidrovía.

El Decreto Nº 949/2020 constituye una nueva vuelta de tuerca en sentido contrario al interés nacional y corresponde reclamar su derogación.

Desde el gobierno de Menen los puertos se han privatizado y no tienen, prácticamente, fiscalización estatal. Las empresas declaran lo que entra y sale sin ningún control. Por lo que nuestras fronteras pluviales están a merced de las subdeclaraciones y peor aun, del contrabando. La situación es aun más delicada porque la exportación de granos está concentrada en unas pocas empresas y desde el menemismo hemos perdido nuestra flota mercante.

La dependencia, siempre es más cara.

Por esto sostenemos que es necesario que el gobierno a través de las instituciones estatales y los mecanismos de administración federal y soberana se haga cargo de la hidrovía y su dragado. Actualmente los trabajos de draga se realizan utilizando una flota permanente compuesta por 6 dragas de succión y 1 de corte, de un valor aproximado de 30 millones de dólares cada una. Es decir, que la inversión en la flota no es superior a los 200 millones de dólares, el equivalente a un año de recaudación de la hidrovía.

Además, vemos necesaria la construcción del Canal de Magdalena que posee múltiples ventajas en abaratamiento de costos, recaudación y control del comercio exterior de nuestra nación. Tambien habilitaria la comunicación directa de la hidrovía y nuestro sistema fluvial con el océano Atlántico y mar Argentino liberandonos del pasó obligado que hoy se tiene por aguas uruguayas a través del canal de Punta Indio que lleva al puerto de Montevideo.

Complementariamente impulsamos la derogación de DNU del gobierno de Macri que vetó los artículos 10 y 15 de la Ley de Promoción de Industria Naval y la reglamentación que nucna hizo de la ley 27,419 de “DESARROLLO DE LA MARINA MERCANTE Y LA INTEGRACIÓN FLUVIAL REGIONAL”, que prioriza la carga en barcos argentinos, como un camino hacia desarrollar una flota naval nacional con “reserva de carga”

Estamos ante una oportunidad histórica de dar un paso adelante en la soberanía nacional. ¡VAMOS POR UNA HIDROVÍA EN MANOS ARGENTINAS!

\*Juan Carlos Alderete es diputado nacional por el Frente de Todos