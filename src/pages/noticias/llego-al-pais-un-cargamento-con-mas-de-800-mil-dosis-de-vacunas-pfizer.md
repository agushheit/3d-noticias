---
category: Agenda Ciudadana
date: 2021-10-07T06:00:38-03:00
thumbnail: https://assets.3dnoticias.com.ar/PFIZEROCTUBRE.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Llegó al país un cargamento con más de 800 mil dosis de vacunas Pfizer
title: Llegó al país un cargamento con más de 800 mil dosis de vacunas Pfizer
entradilla: El avión de American Airlines con las dosis aterrizó a las 15.36 al aeropuerto
  internacional de Ezeiza y este jueves arribará otro vuelo con 776.880 unidades.

---
Un cargamento de 848.250 dosis de vacunas Pfizer contra el coronavirus llegó al país, mientras que este jueves arribará otro vuelo con 776.880 unidades del inoculante elaborado por el mismo laboratorio, según se informó oficialmente.  
  
Se trata de un total de 1.625.130 dosis de la vacuna Pfizer que se sumarán al Plan Estratégico de Vacunación contra el coronavirus que el Gobierno nacional lleva adelante en todo el país.  
  
El cargamento con 848.250 dosis de la vacuna Pfizer llegó a las 15.36 al aeropuerto internacional de Ezeiza, en el vuelo AA931 de American Airlines.  
  
En tanto, este jueves a las 11.30 se espera la llegada de otro cargamento con 776.880 unidades de la inmunización de ese laboratorio, en una aeronave de la misma compañía aérea.  
  
A su vez, entre este jueves y el viernes llegarán a todas las jurisdicciones 1.004.500 dosis del componente 2 de la vacuna Sputnik V, con el objetivo de continuar con el fuerte avance en la aplicación de los esquemas completos de vacunación en el marco del Plan Estratégico que despliega el Gobierno nacional para combatir la Covid-19, se informó oficialmente.  
  
De acuerdo al criterio dispuesto por el Ministerio de Salud de la Nación en base a la cantidad de población de cada distrito, a la provincia de Buenos Aires le corresponderán 389.250 dosis; a la Ciudad Autónoma de Buenos Aires 67.500; a Catamarca 9.000; a Chaco 27.000; a Chubut 13.500; a Córdoba 83.250; a Corrientes 24.750; a Entre Ríos 30.375; y a Formosa 13.500.  
  
En tanto, a Jujuy le corresponderán 16.875 dosis; a La Pampa 7.875; a La Rioja 9.000; a Mendoza 43.875; a Misiones 28.125; a Neuquén 14.625; a Río Negro 16.875; a Salta 31.500; a San Juan 16.875; a San Luis 11.250; a Santa Cruz 7.875; a Santa Fe 78.625; a Santiago del Estero 21.375; a Tierra del Fuego 3.375 y a Tucumán 38.250.  
  
Según las cifras actualizadas esta mañana del Monitor Público de Vacunación, se distribuyeron en todo el país un total de 57.884.464 dosis de vacunas, de las cuales 53.190.856 ya fueron aplicadas.  
  
De ese total, 29.962.361 personas iniciaron su esquema de vacunación y 23.228.495 lo completaron con las dos dosis, lo que representa más de la mitad de la población del país.  
  
"El 50% de los argentinos y argentinas tiene su esquema de vacunación completo", escribió la ministra de Salud, Carla Vizzotti en su cuenta de la red social Twitter.  
  
En ese sentido, manifestó en el posteo que "esto es una realidad gracias a la confianza de la población en las vacunas y al trabajo de las y los vacunadores".  
  
"Antes de fin de año todos los mayores de 3 años tendrán su vacuna disponible", publicó la funcionaria.  
  
La funcionaria, en ese contexto, dijo esta mañana en declaraciones a radio La Red que padres y madres "tienen que tener la tranquilidad" de que "son seguras" las vacunas que recibirán sus hijos como parte del plan de inoculación contra el coronavirus que lleva a cabo el Gobierno nacional.  
  
Vizzotti sostuvo que se trata de "un fármaco seguro que tenemos en Argentina" y afirmó que emplea "una plataforma muy conocida" y utilizada en los inmunizantes pediátricos elaborados para combatir la gripe.

![Foto: Presidencia.](https://www.telam.com.ar/advf/imagenes/2021/10/615e11e1cebf3_1004x565.jpg "Foto: Presidencia.")

En ese sentido, sostuvo que la Sinopharm "se ha utilizado en millones de personas", y sus estudios sobre seguridad "están publicados" en la revista científica Lancet.  
  
"En China y Emiratos Árabes ya se han aplicado en menores de 12 años y todos los datos de seguridad también son muy alentadores", manifestó.  
  
Vizzotti señaló que "la mejor vacuna es la que tenemos disponible lo antes posible" y explicó que inmunizar a los niños ayudará "a fortalecer la presencialidad escolar, proteger a los menores de riesgo y evitar contagios en las familias".