---
layout: Noticia con imagen
author: "FUENTE: Telam"
resumen: Préstamo "Mi moto"
category: Agenda Ciudadana
title: "Créditos para motos: todo lo que tenes que saber"
entradilla: 'El Banco Nación ofrece financiamiento para acceder a motocicletas
  de fabricación nacional de hasta $200.000. Préstamo "Mi moto", tanto para
  clientes y no clientes del BNA. Paso a paso: cómo solicitar el préstamo.'
date: 2020-11-18T14:10:58.026Z
thumbnail: https://assets.3dnoticias.com.ar/motos.jpeg
---
El Banco Nación informó que ya ofrece la línea de crédito para la compra de motocicletas de fabricación nacional, que permitirá comprar motos de hasta $200.000, a un plazo de 48 meses y con una tasa final del 28,5 por ciento.

El monto máximo a financiar por usuario es hasta $200.000, a un plazo único de 48 meses, sistema de amortización francés, y alcanza a clientes o no clientes de la entidad. Y la línea cuenta con una bonificación de 10 puntos porcentuales en la tasa de interés, ya incluida en el 28,5% final.

En diálogo con LT10, Lino Stefanutto presidente de la Cámara de Fabricantes de Motovehículos explicó que "este plan es una gran ayuda para la industria" tras una caída abrupta en los últimos meses debido a la pandemia.

## **Mi moto: características del préstamo**

* El monto máximo a financiar por usuario es hasta $200.000
* A un plazo único de 48 meses con sistema de amortización francés
* Alcanza a clientes o no clientes de la entidad.

**Clientes del BNA:**Tendrán una financiación será de hasta el 35% de los ingresos netos del solicitante.

**No clientes del BNA:** Los no clientes la financiación será de hasta el 30% de los ingresos netos del solicitante.

Las más de 200 concesionarias de motos de todo el país ofrecerán casi 7.500 motos de 34 modelos de fabricación nacional, como la Honda CG150 Titán, a $ 199.900; la Zanella ZB110, a $ 78.990; la Gilera VC150, a 98 mil pesos; o la Corven Hunter RT, a 100 mil pesos.

## **Paso por paso para solicitar el préstamo**

* Ingresar al sitio web del Banco Nación: https://www.bna.com.ar/
* Seleccionar "Programa Mi Moto"
* Ingresar CUIL y correo electrónico
* Validar la identidad
* Recibe un mail informando el monto máximo del préstamo permitido
* Ingresar a la tienda de BNA: https://www.tiendabna.com.ar/mi-moto y elegir moto
* Esperar el llamado de la sucursal.
* Será citado para firmar la documentación.
* El banco notifica a la concesionaria el préstamo.
* Entrega de moto: la concesionaria coordina con el cliente.