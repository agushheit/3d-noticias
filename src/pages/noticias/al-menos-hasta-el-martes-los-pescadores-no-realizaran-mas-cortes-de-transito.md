---
category: La Ciudad
date: 2021-01-06T11:20:19Z
thumbnail: https://assets.3dnoticias.com.ar/060121-corte-pescadores.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Los pescadores no realizarán más cortes de tránsito
title: Al menos hasta el martes los pescadores no realizarán más cortes de tránsito
entradilla: Los pescadores que cortaron las rutas este lunes y martes en los alrededores
  de la ciudad de Santa Fe adelantaron que no habrá más medidas de fuerza al menos
  hasta el martes venidero.

---
Advirtieron que es muy probable que después de esa fecha vuelvan los cortes de tránsito.

Así lo hizo saber Nelson Yapura, referente del sector que en estos dos días realizó protestas en varios sectores de la provincia, **luego de la decisión de un juez que dispuso la veda pesquera comercial y deportiva en todo el territorio santafesino con el objetivo de proteger el ecosistema ante la bajante** (medida que regirá hasta el 31 de marzo).

El pescador dijo que «decidimos realizar un cuarto intermedio para deliberar en las instituciones y también con los pescadores independientes y las cooperativas, y retomar la lucha la semana que viene, y a la espera que el gobierno nos convoque con alguna solución».

En ese marco, se decidió que «por lo menos hasta el martes no va a haber cortes, creo que después van a volver, ojalá que no pase eso, que la Justicia tome conciencia y deje salir a los pescadores mientras se dirime este problema».

Yapura aseguró que «nosotros estamos abiertos al diálogo, pero también a conservar nuestra fuente de trabajo. **No podemos parar nuestra actividad, y tampoco queremos subsidios, nosotros queremos, como productores de alimentos, que se nos restaure nuestra fuente de trabajo**».

Asimismo, el representante de los trabajadores agregó que «no queremos pescar todos los días, de hecho, ahora no podemos pescar los fines de semana, y si tenemos que sacrificar 1 o 2 días más, estamos dispuestos a sentarnos en una mesa y dialogar».

Finalmente, Yapura recordó que «la pandemia dejó a mucha gente sin trabajo, y muchos volvieron al agua para ejercer el oficio, lo que hace que haya más familias que viven de esto», y por lo tanto son muchos los que se ven afectados por la veda.