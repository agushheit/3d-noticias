---
category: La Ciudad
date: 2020-12-15T13:20:49Z
thumbnail: https://assets.3dnoticias.com.ar/aceria.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: La provincia avanza con la construcción de viviendas en barrio Acería de
  Santa Fe
title: La provincia avanza con la construcción de viviendas en barrio Acería de Santa
  Fe
entradilla: 'Actualmente está finalizando la construcción de 102 viviendas y se están
  comenzando a levantar otros 90 módulos habitacionales. '

---
El Gobierno de Santa Fe, a través del Ministerio de Infraestructura, Servicios Públicos y Hábitat, puso en marcha las obras de construcción de 90 viviendas en el barrio Acería de la ciudad de Santa Fe, cuyo presupuesto oficial supera los 308 millones de pesos.

Al respecto, la ministra de Infraestructura, Servicios Públicos y Hábitat, Silvina Frana, resaltó que “seguimos trabajando para mejorar la calidad de vida de los vecinos, tal como nos encomendó el gobernador Omar Perotti. Porque tenemos la firme convicción y la voluntad de saldar el déficit habitacional y llegar a cada rincón de la provincia, con políticas concretas, con una mirada federal y con un objetivo claro que es contrarrestar los desequilibrios territoriales, generar arraigo e igualar en oportunidades”.

En tanto, el secretario de Hábitat, Urbanismo y Vivienda provincial, Amado Zorzón, destacó que “no alcanza solo con entregar viviendas, sino también con asegurar que las mismas cuenten con todos los servicios básicos para garantizar una calidad de vida digna”, y añadió: “Esta intervención es de las primeras que se han retomado porque son necesarias e imprescindibles. Queremos que este sea el mensaje para las familias del barrio y para todos los que tienen expectativa de poder llegar a tener una vivienda en la provincia de Santa Fe".

Finalmente, el director Provincial de Vivienda y Urbanismo, Jose Manuel Kerz, enfatizó sobre el trabajo integral que vienen realizando desde la provincia “sin interrumpir ningún proceso, sino todo lo contrario; pusimos en marcha e iniciamos nuevas obras con el único fin de trazar una política de Estado entorno a la vivienda y al hábitat”.

“Es este caso, estamos finalizando con 102 viviendas que actualmente están en ejecución en el barrio, y paralelamente, ya hemos adjudicado a la empresa la construcción de las 90 viviendas licitadas a fines de septiembre, que consisten en **dúplex de dos dormitorios, con una superficie de entre 55 y 65 metros cuadrados**. Además, las unidades están **equipadas con calefones solares** para permitir un ahorro energético en el calentamiento de agua”, detalló Kerz.

***

## **LAS VIVIENDAS**

Desde la Dirección Provincial de Vivienda y Urbanismo se viene desarrollando una intervención integral en el complejo habitacional en el barrio Acería, destinado a la relocalización de los residentes de las viviendas agrupadas del plan Nº 218 construido en el año 1988 y que presentan severos daños edilicios y de infraestructura.

Cabe recordar que ante esta situación, se planteó la licitación en etapas de nuevas obras, el traslado paulatino de las familias y la inmediata demolición de los monoblocks deteriorados.

Con respecto a las 90 viviendas que se comenzarán a construir, se elaboró el proyecto general y se dividió la licitación en dos grupos: la Licitación Pública Nº02/20 correspondiente a la ejecución de 34 viviendas, y la Licitación Pública Nº 03/20 referida a la construcción de 56 viviendas. Ambas licitaciones fueron adjudicadas a la empresa COEMYC S.A.

**Las obras también incluyen trabajos de infraestructura para dotar de servicios al nuevo barrio**: red eléctrica de media y baja tensión, alumbrado público, agua potable, desagües cloacales, apertura de una calle con estabilizado granular y cordón cuneta.