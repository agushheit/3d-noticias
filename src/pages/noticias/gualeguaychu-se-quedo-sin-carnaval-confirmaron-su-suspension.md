---
category: Agenda Ciudadana
date: 2020-12-13T13:42:23Z
thumbnail: https://assets.3dnoticias.com.ar/carnaval.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Agencias'
resumen: 'Gualeguaychú se quedó sin carnaval: confirmaron su suspensión'
title: 'Gualeguaychú se quedó sin carnaval: confirmaron su suspensión'
entradilla: Debía realizarse entre enero y febrero del año próximo. Sin embargo, la
  comisión directiva del llamado Carnaval del País decidió que no era viable.

---
La Comisión organizadora del Carnaval de Gualeguaychú decidió **suspender por la pandemia de Covid-19 y por unanimidad uno de los espectáculos más importantes del país** que se realiza todos los veranos en esa ciudad entrerriana.

**En las últimas horas, la Comisión oficializó la decisión**, luego de varias reuniones que mantuvieron los directivos de las cinco comparsas animadoras.

El presidente de la Comisión, Ricardo Saller, subrayó que "el Carnaval tradicional no se hace", aunque señaló que se reunirán "más adelante para ver si la situación sanitaria y los decretos permite hacer algún otro evento o no".

La organización del tradicional carnaval justificó la medida al tomar en cuenta "la situación epidemiológica que atraviesa la ciudad y el mundo entero en el marco de la pandemia por coronavirus, y tendiendo a cuidar la salud y seguridad de los espectadores".

En declaraciones radiales, Saller expresó: "La postura fue unánime, porque todos los clubes entendimos que había que tomar una decisión para dejar de generar falsas expectativas y que todos los espectadores y los trabajadores ya sepan que no se va a poder contar con el carnaval".

**El colorido Carnaval de Gualeguaychú es uno de los principales atractivos de la temporada de verano** y año a año atrae alrededor de medio millón de turistas, por lo que **tiene un importante rol en la economía entrerriana**.

  
 