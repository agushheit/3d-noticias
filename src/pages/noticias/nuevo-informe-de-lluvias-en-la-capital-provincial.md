---
category: La Ciudad
date: 2021-04-11T06:00:01-03:00
thumbnail: https://assets.3dnoticias.com.ar/lluvias.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Nuevo Informe de lluvias en la capital provincial
title: Nuevo Informe de lluvias en la capital provincial
entradilla: La Municipalidad brindó una actualización de los datos de lluvia y actuaciones
  con motivo de las precipitaciones de este viernes y sábado.

---
La Municipalidad de Santa Fe informa, ante los más de 200 mm de lluvia caídos desde el viernes hasta el mediodía de este sábado, que el sistema de bombeo y de drenaje urbano se encuentra totalmente operativo, en tanto que las cuadrillas están trabajando desde la jornada de el viernes en calle, realizando la limpieza de bocas de tormenta y desagües tapados. Hasta este sábado al mediodía no hubo  evacuados en la ciudad.

La secretaria de Asuntos Hídricos y Gestión de Riesgo municipal, Silvina Serra, indicó que el evento meteorológico de las últimas 24 horas se caracterizó por una confluencia entre cantidad de milimetraje caído (más de 200 mm), y con una alta intensidad (de 100 mm/h) en determinados momentos. “Tuvimos un último evento que ocurrió en la madrugada y que fue muy intenso, con 100 mm por hora, por lo tanto, el acumulado en milímetros más la intensidad está hablando de un evento importante y, para una ciudad como la de Santa Fe que se caracteriza por tener pendientes tan planas, esta tormenta de frente importante resulta un evento significativo”, explicó.

La funcionaria indicó que “ya conociendo la ocurrencia de este evento, realizamos tareas previas a la lluvia de recorrido de puntos críticos, de zonas que se anegan frecuentemente por distintas razones. Este trabajo previo, hizo que cuando comenzó la lluvia, los conductos se encontraban en buenas condiciones y de hecho ayer, con más de 100 milímetros, no tuvimos anegamientos, o fueron temporarios y el agua drenó bien”.

Los datos de lluvia caída en la capital provincial este sábado 10 de abril, hasta las 9 horas de la mañana:

DOAE (COBEM): 73,25 mm

Centro: 101,50 mm

Alto Verde: 71,75 mm

CIC F. Zuviría: 68,50 mm

La intensidad máxima de lluvia del día de la fecha fue de 100 mm/h, a las 6:45 horas, en el Centro. En tanto la ráfaga máxima de viento fue de 24 km/h, de Este Sureste (ESE) a las 7.30 horas, en Delegación Alto Verde.

Llovieron 205 mm en Santa Fe

El acumulado de precipitaciones desde este viernes 9 y sábado 10, hasta las 9 horas, es el siguiente:

DOAE (COBEM): 135,50 mm

Centro: 205,50 mm

Deleg. Alto Verde: 142,25 mm

CIC F. Zuviría: 132,75 mm

**Reclamos recibidos**

Se registraron a través de la línea de Atención Ciudadana, 27 reclamos en total relacionados con las lluvias en la jornada de este sábado hasta las 9 horas: recursos hídricos (9) y gestión de riesgo (18); los mismos están vinculados a anegamiento de calles, obstrucción de desagües e ingreso de agua a viviendas. Se recuerda que quienes tengan inquietudes o reclamos, pueden comunicarse al 0800-777-5000.