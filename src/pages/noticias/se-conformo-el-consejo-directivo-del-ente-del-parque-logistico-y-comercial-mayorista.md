---
category: La Ciudad
date: 2021-08-23T06:00:59-03:00
thumbnail: https://assets.3dnoticias.com.ar/PARQUE.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Se conformó el consejo directivo del Ente del Parque Logístico y Comercial
  Mayorista
title: Se conformó el consejo directivo del Ente del Parque Logístico y Comercial
  Mayorista
entradilla: Funcionarios municipales participaron del encuentro en el que se designaron
  las autoridades del Ente y se elaboró una agenda de trabajo para el resto del año.

---
La participaron del encuentro tuvo como objetivo conformar al Consejo Directivo del Ente del Parque Logístico y Comercial Mayorista. De la reunión, realizada en el Ministerio de Producción, Ciencia y Tecnología, tomaron parte el secretario de Producción y Desarrollo Económico de la Municipalidad, Matías Schmüth, representantes del Gobierno provincial, la Municipalidad de Santo Tomé, la comuna de Sauce Viejo, los centros comerciales de las tres localidades y la Cámara de Mayoristas de la provincia.

En la oportunidad, se designaron las autoridades del Ente Autárquico creado por Ley para la administración del Parque. Además, se estableció una agenda de trabajo para los próximos meses, que incluye la realización de la mensura correspondiente al terreno donde se ubicará el Parque.

Cabe recordar que se trata de 97 hectáreas situadas en las inmediaciones del Aeropuerto de Sauce Viejo, que fueron donadas por Nación. El proyecto prevé que allí se instalen empresas comerciales proveedoras de mercadería a nivel mayorista para generar un gran polo que abastezca al Gran Santa Fe, con la posibilidad de potenciarse hacia la Región Centro.

Concluido el encuentro, Schmüth remarcó la importancia de establecer un cronograma de trabajo del Ente, para avanzar en las gestiones que permitan hacer realidad el proyecto del Parque Logístico y Comercial. En ese sentido, señaló que la iniciativa será sumamente beneficiosa para el sector mayorista de la región y la ciudad de Santa Fe en particular, ya que generará un importante desarrollo económico.

**Las autoridades**

Durante la reunión, se conformó el Consejo Directivo, compuesto por Juan Marcos Aviano como presidente, en representación del gobierno de la provincia; Aldo Egestti como vicepresidente, por la Cámara Santafesina de Distribuidores Mayoristas; Martín Salemi como secretario, por el Centro Comercial de Santa Fe; Matías Schmüth como tesorero; y los vocales Eduardo Terreno por Santo Tomé, Pedro Uliambre por Sauce Viejo, Rubén Ruzicki por el Centro Comercial de Santo Tomé. Finalmente, los síndicos serán Julio Cauzzo, del Centro Comercial de Sauce Viejo, y María Laura Grau, de la Secretaría de Comercio Interior y Servicios.

La superficie afectada por incendios reportados entre el 1 de enero y el 8 de agosto fue de 135.162 hectáreas; pero esta estadística no incluye datos de Chaco, San Juan, Santa Fe y Tierra del Fuego.

**Brigada de Atención y Prevención de Emergencias**

Son 15 los integrantes de la brigada que tiene su sede operativa en un ala refaccionada de lo que fue el Liceo Militar General Manuel Belgrano, ubicado en la localidad de Recreo, a escasos 15 kilómetros de la ciudad de Santa Fe. Pero, además, cuenta con una guardia permanente en La Ribera, un predio ubicado en la cabecera del puente Rosario-Victoria, que cedió el Ministerio de Medio Ambiente de la provincia.