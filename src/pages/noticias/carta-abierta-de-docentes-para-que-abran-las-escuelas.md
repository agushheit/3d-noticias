---
layout: Noticia con imagen
author: .
resumen: "Docentes: carta abierta"
category: Agenda Ciudadana
title: Carta abierta de docentes para que abran las escuelas
entradilla: Un grupo de docentes escribió y publicó una carta para que la gente
  pueda adherir con su firma. El objetivo principal es poder volver a las
  instituciones de forma presencial.
date: 2020-11-10T17:30:45.367Z
thumbnail: https://assets.3dnoticias.com.ar/agenda-docentes.png
---
En el marco de la pandemia por el coronavirus, las clases de forma presencial se encuentran entre las actividades que no se desarrollaron durante el año. Algunos afortunados, en las zonas menos afectadas por el virus, lograron regresar a las escuelas en este último mes. Sin embargo, no es la realidad de la mayoría.

Una consecuencia directa de esta situación es el abandono  de las clases virtuales por parte de los chicos, ya sea por cansancio o por cuestiones de conectividad, que quedó al descubierto en esta pandemia.

Por ese motivo, un grupo de docentes de la ciudad de Santa Fe y alrededores, denominados "Aula Abierta", decidieron escribir una carta al Gobierno  para pedir la inmediata apertura de las instituciones, de modo que los alumnos puedan asistir de forma presencial.

Son representantes de todos los niveles y modalidades. Laura, una de las docentes, en diálogo con El Cuarto Poder manifestó su preocupación: "entendemos que es necesario abrir las escuelas, nos basamos en que la educación es un derecho fundamental que habilita otros derechos".

En el mismo orden, se mostró preocupada por el abandono escolar de los alumnos y por el cansancio tanto físico, psicológico como emocional de los docentes. "Pedimos concretamente la vuelta a las escuelas, con protocolos claros y responsables" concluyó Laura.

**CARTA ABIERTA: HAY QUE VOLVER A LA ESCUELA**

Somos un grupo de docentes preocupados por esta situación y entendemos que es necesario abrir las escuelas y volver a las clases presenciales -suspendidas por el Poder Ejecutivo Nacional el 15 de marzo de este año- lo antes posible. Los chicos quieren volver, los padres quieren que sus hijos vuelvan.

Las escuelas cerradas profundizan la desigualdad social, se están provocando daños que pueden ser irreparables. Vemos y sabemos que hay en muchos chicos cuadros depresivos, desánimo, incertidumbre. Los intentos de vinculación con cada chico y con cada familia son en algunos casos enormes pero insuficientes.

La opinión científica recomienda abrir las escuelas, la experiencia en el mundo indica que hacerlo no provoca un aumento sensible en la propagación del Covid-19 y en muchos países que han aumentado las restricciones no se han vuelto a cerrar las escuelas. Estas experiencias son muy valiosas porque nos dan la posibilidad de copiar a los países y ciudades que han sido exitosas y mirar con atención a los que han tenido que retroceder para no cometer esos errores.

El esfuerzo que están haciendo muchos directivos, tutores, maestros, profesores, preceptores, personal administrativo y personal auxiliar es cada vez más difícil de sostener, en algunos casos hay un profundo agotamiento no sólo físico sino también psíquico y mental.

Y lo más trágico es que pese a todo este esfuerzo hay muchísimos alumnos que permanecen desconectados de la escuela.

Un docente no puede ser reemplazado por una computadora. Aplazar la vuelta a clases y en lugar de eso ofrecer computadoras a los alumnos desconectados no es la solución. El trabajo de un docente en la escuela con los alumnos es irreemplazable. Es el docente en la escuela el que observa las particularidades de cada curso y de cada alumno y ahí en ese contexto planifica y corrige. Nos parece además que una propuesta así implica desvalorizar nuestro trabajo y sienta un peligroso precedente para el futuro.

Horas y horas de zooms y videos educativos están lejos de representar herramientas idóneas para que los chicos aprendan y, probablemente, esa exposición prolongada a las pantallas tendrá, tarde o temprano, secuelas para su salud.

El retorno a la presencialidad debe ser con las mejores condiciones de seguridad sanitarias, con protocolos claros y con absoluta responsabilidad para reducir riesgos de contagio en la escuela. El debate sobre el tema debe ser sincero, no extorsivo.

La participación de un docente en lo que queda de 2020 debe ser voluntaria, si un docente es parte de la población de riesgo debe postergar su participación en la presencialidad por lo menos hasta febrero de 2021, el docente que regresa a la presencialidad en una escuela no debe hacerlo en ninguna otra escuela en la que dé clases.

La concurrencia para los estudiantes durante 2020 también debe ser voluntaria, si alguno vive muy lejos puede elegir seguir de manera virtual y los demás si quieren hacerlo tienen que poder hacerlo. En el caso del AMBA sabemos que hay muchísimos chicos que viven en el Gran Buenos Aires y prefieren estudiar en escuelas de la Ciudad de Buenos Aires, son casos que deben ser considerados especialmente.

Si realmente todos los docentes consideramos a la escuela como una institución fundamental y queremos luchar contra la desigualdad y la pobreza la vuelta a la escuela es un imperativo ético. Es insostenible poder tomar algo en un bar mientras los alumnos no pueden verse en la escuela con sus compañeros y sus docentes, o decir que la escuela es una institución esencial mientras las mantenemos cerradas.

La magnitud de la situación requiere un regreso a la presencialidad. Más allá del cumplimiento de ¨pactos preexistentes¨ que algunos hayan rubricado, todos debemos tener en claro que con las escuelas cerradas los chicos pierden y si pierden los chicos, perdemos todos porque perdemos el futuro.

Hay que volver a la escuela. 

Es urgente.

* **Adherir a la petición**: <http://aula-abierta.ar/>