---
category: Agenda Ciudadana
date: 2021-07-27T06:15:07-03:00
thumbnail: https://assets.3dnoticias.com.ar/EMERGENCIAH.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Santa Fe en "emergencia hídrica" por la bajante del Paraná
title: Santa Fe en "emergencia hídrica" por la bajante del Paraná
entradilla: Se decidió por el "impacto y las consecuencias de la bajante más importante
  de los últimos 77 años en la cuenta hídrica de los ríos Paraná, Paraguay e Iguazú"

---
El presidente Alberto Fernández declaró el "Estado de Emergencia Hídrica" por 180 días en la región de la cuenca del río Paraná que afecta a las provincias de Formosa, Chaco, Corrientes, Santa Fe, Entre Ríos, Misiones y Buenos Aires, ante la histórica bajante que se registra en esa región.

La medida quedó instrumentada a través del Decreto 482/2021, publicado recientemente en el Boletín Oficial, y faculta al jefe de Gabinete, Santiago Cafiero, en su carácter de presidente del Consejo Nacional para la Gestión Integral del Riesgo y la Protección Civil, a “delimitar las áreas afectadas por la emergencia”.

Además, el punto central de esta decisión es que a diferentes áreas del Poder Ejecutivo se les asignó una misión en particular, todas tendientes a disminuir los efectos negativos de la bajante en las zonas alcanzadas por el fenómeno ambiental.

**Las tareas que deberá cumplir cada organismo**

De esta manera, les permite a los Ministerios de Desarrollo Productivo, que conduce Matías Kulfas, y de Trabajo, a cargo de Claudio Moroni, a adoptar las acciones “necesarias con el objeto de preservar la continuidad de la actividad productiva y la conservación de los puestos de trabajo en los sectores afectados”.

Lo mismo podrá hacer la Administración Federal de Ingresos Públicos (AFIP), que dirige Mercedes Marcó del Pont, “respecto de aquellos y aquellas contribuyentes cuyo establecimiento productivo se encuentre afectado por la emergencia, siendo este su principal actividad.

En tanto, el Banco Nación dispondrá la asistencia en materia crediticia, mientras que el Ministerio de Transporte, en manos de Alexis Guerrera, “observará las posibilidades de navegación y acceso a puertos”.

El ministro de Ambiente y Desarrollo Sustentable, Juan Cabandié, estará a cargo de “controlar los incendios en las zonas de islas y márgenes mientras dure la emergencia”, mientras que su par de Seguridad, Sabina Frederic, deberá dar “adecuada respuesta a las demandas específicas vinculadas a la emergencia” e “integrar las acciones de las distintas áreas involucradas a través del Sistema Nacional de Alerta Temprana y Monitoreo de Emergencias (SINAME)”.

Entre las múltiples tareas que tendrá el Ministerio de Economía, manejado por Martín Guzmán, están las de “asegurar el normal abastecimiento de la demanda de energía eléctrica” y “dar prioridad a la navegabilidad del transporte de los combustibles”, con el objetivo de asegurar los servicios de gas natural y de electricidad.

A su vez, el Ministerio de Obras Públicas, de Gabriel Katopodis, tomará medidas “a los efectos de que se realicen las obras de infraestructura necesarias para mitigar los efectos de la emergencia en las zonas afectadas, mientras dure la misma”.

El Instituto Nacional del Agua (INA) tendrá que brindar “asesoramiento y prestar colaboración en el ámbito de su competencia, a los servicios técnicos necesarios para llevar adelante” todas las otras iniciativas mencionadas. Asimismo, quedaron suspendidos los plazos administrativos “vinculados con las áreas alcanzadas por la presente declaración de emergencia”, en las regiones afectadas.

Por último, Cafiero está autorizado a realizar “las reestructuraciones presupuestarias que fueren necesarias a los efectos de asignar los créditos, cargos y cualquier otra adecuación que se requiera para el financiamiento” de todos estos proyectos.

El déficit de precipitaciones en las cuencas brasileñas del río Paraná y del río Iguazú y la sequía son factores determinantes.