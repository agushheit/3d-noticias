---
category: La Ciudad
date: 2021-02-11T06:42:47-03:00
thumbnail: https://assets.3dnoticias.com.ar/sfc.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad de Santa Fe
resumen: Santa Fe y Rosario refuerzan acciones de cooperación en materia turística
title: Santa Fe y Rosario refuerzan acciones de cooperación en materia turística
entradilla: La ciudad de Santa Fe presentará mañana en Rosario, su agenda cultural
  para disfrutar el fin de semana de Carnaval. En tanto, Rosario hizo lo propio este
  miércoles en un local de la Costanera Este.

---
Con el Puente Colgante de fondo, funcionarios de Rosario presentaron este miércoles, las actividades turísticas que esa ciudad del sur provincial ofrece para el fin de semana largo de Carnaval. Acompañados por autoridades de la capital provincial, los visitantes detallaron la agenda cultural que tienen preparada para el próximo feriado.

Para completar la actividad, la ciudad de Santa Fe concretará una presentación idéntica mañana en Rosario. Será a partir de las 13 horas en el hotel “Puerto Norte”. De este modo se alcanza una acción de promoción turística coordinada, focalizada en el fin de semana de Carnaval. Por otro lado, significa el inicio de una serie de gestiones de cooperación, para incrementar el flujo turístico entre las dos ciudades.

Cabe mencionar que ambas ciudades generan casi el 50% del movimiento turístico en la provincia. Además, las dos cuentan con el sello Safe Travels, otorgado por el Consejo Mundial de Viajes y Turismo (WTTC), el cual garantiza que son un destino seguro para recibir visitantes en el contexto de pandemia.

![](https://assets.3dnoticias.com.ar/rosario.jpg)

**Red de beneficios**

La conferencia de prensa de este miércoles se realizó en el restaurante “1980”, ubicado en la Costanera Este y contó con la participación del secretario de Producción y Desarrollo Económico de la Municipalidad de Santa Fe, Matías Schmüth; y el director Ejecutivo de Turismo del municipio, Franco Arone. En tanto, en representación de Rosario, estuvieron la subsecretaria de Turismo, Alejandra Mattheus; y el director Comercial del Ente Turístico, Bruno Rearte.

En la oportunidad, Schmüth detalló la coordinación que existe entre ambas localidades, para promocionar mutuamente la oferta turística vigente. Según dijo, las dos administraciones municipales vienen “trabajando y articulando desde hace mucho tiempo” en materia turística.

En el mismo sentido, reveló que “Santa Fe y Rosario tienen el privilegio de impulsar el 50% del turismo que se da en la provincia, por lo que nos pareció que el camino a seguir es el de trabajar articuladamente e intercambiar experiencias”.

Por otra parte, mencionó que el sector turístico “fue muy golpeado por la pandemia de COVID-19 y sigue siendo afectado por la crisis que estamos atravesando”. Por este motivo, indicó que la intención es que los prestadores de estos servicios “se sientan acompañados con nuestra presencia, con nuestra cercanía y con acciones que puedan ser la base para el desarrollo de la actividad turística”.

En cuanto a la presentación que la capital de la provincia realizará mañana en Rosario, Arone estableció que “armamos una agenda muy nutrida de actividades público-privadas, coordinadas con agencias de viajes”. En ese sentido, destacó los paseos guiados y gratuitos, las actividades náuticas y las excursiones vinculadas a la historia, la cultura y la gastronomía, que son operadas por las agencias de turismo receptivo.

Además consignó que el 40% “de la oferta que generamos para este fin de semana son paseos guiados y gratuitos, lo cual hoy, el sector hotelero puede promocionar su plaza y agregarle los recorridos, que están reconocidos a nivel nacional e internacional”.

Por su parte, Mattheus aseveró que “Santa Fe es una ciudad muy cercana no sólo en lo territorial, sino también en nuestros afectos” Y añadió: “Hay un gran intercambio entre las dos ciudades y un trabajo mancomunado desde el inicio de la gestión y más específicamente con la pandemia”.

Con respecto a los servicios previstos para los santafesinos durante el fin de semana de carnaval, destacó que “Rosario ofrece una red de más de 50 beneficios para los turistas, a los que se puede acceder a través de los establecimientos hoteleros”.

En tanto, Rearte confirmó que habrá propuestas turísticas durante los 4 días del fin de semana largo: “El Bus turístico es uno de los nuevos atractivos que tenemos. Es una muy buena oportunidad para recorrer la ciudad, se pueden bajar en cualquiera de las postas, hacer combinaciones e ir de una punta a la otra de Rosario”.

Con respecto a los beneficios previstos, Rearte indicó que quienes se hospeden en lugares habilitados “podrá acceder al listado de los beneficios que ya están disponibles”. Para más información se puede acceder al link [https://rosario.tur.ar/web/noticias_int.php?id=131](https://rosario.tur.ar/web/noticias_int.php?id=131 "https://rosario.tur.ar/web/noticias_int.php?id=131")