---
category: Estado Real
date: 2021-09-02T06:00:38-03:00
thumbnail: https://assets.3dnoticias.com.ar/DISCAPACIDAD.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: En la provincia se vacunaron el 95 por ciento de personas con discapacidad
title: En la provincia se vacunaron el 95 por ciento de personas con discapacidad
entradilla: En Santa Fe recibieron al menos una dosis contra Covid-19, 59.627 personas
  con discapacidad.

---
El Ministerio de Salud, informó que, al día de la fecha, se inscribieron en el Registro Provincial 62.758 personas con discapacidad, de las cuales el 95 por ciento ya fue vacunada con una dosis y el 60 por ciento ya recibió las dos dosis.

En cuanto a personas con discapacidad y riesgo entre 18 y 59 años, se informó que más del 94 por ciento recibió la primera dosis, y más del 57 por ciento se vacunó con ambas dosis.

A comienzos de agosto, se inició la inoculación a menores con discapacidad y riesgo (de 12 a 17 años), donde se avanzó fuertemente llegando a más del 60 por ciento de esta población.

De esta manera, el subsecretario de Inclusión para Personas con Discapacidad, Patricio Huerga explicó que "la vacunación en Santa Fe contra Covid-19 a personas con discapacidad avanza rápidamente, ubicándonos en una de las provincias con mayor cantidad de PcD inoculadas al día de hoy. Y para ello trabajamos incansablemente en Hogares, Centros de Día, instituciones y con el público en general con discapacidad, para efectuar el Plan Estratégico de Vacunación. Es así como ya 37.626 PcD recibieron las dos dosis, de un total de 62.758 inscriptas en el Registro Provincial".

"Además avanzamos fuertemente en la vacunación de menores con discapacidad y riesgo, que tienen entre 12 y 17 años, ya que 3.609 fueron inoculadas desde el 3 de agosto hasta el día de la fecha", continuó el funcionario.

El Plan Estratégico de Vacunación contra el Covid-19 a Personas con Discapacidad de Santa Fe, se viene implementando desde febrero en Hogares, Centros de Día, Centros Educativos Terapéuticos, Centros de Formación Laboral, a personas electrodependientes, con Fibrosis Quística, en internaciones domiciliarias, Centros de Rehabilitación y personas con discapacidad en general. El objetivo de este Plan de Vacunación es lograr lo antes posible la inoculación de las personas con discapacidad, para evitar cualquier tipo de complicación en su salud.