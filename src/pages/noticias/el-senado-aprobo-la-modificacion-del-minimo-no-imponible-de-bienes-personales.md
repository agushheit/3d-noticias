---
category: Agenda Ciudadana
date: 2021-10-29T06:00:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/SENADO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: El Senado aprobó la modificación del mínimo no imponible de Bienes Personales
title: El Senado aprobó la modificación del mínimo no imponible de Bienes Personales
entradilla: La iniciativa, que contó con el apoyo del oficialismo y de la oposición,
  busca eximir del alcance del gravamen a los contribuyentes cuyos bienes, resulten
  en un valor igual o inferior a los ocho millones de pesos.

---
El Senado de la Nación aprobó el proyecto que modifica el artículo 24 de la Ley Impuestos a los Bienes Personales, que permitirá incrementar el mínimo no imponible para que una menor cantidad de contribuyentes se encuentre comprendido en el pago de este tributo.  
  
La iniciativa, que contó con el apoyo del oficialismo y de la oposición, busca eximir del alcance del gravamen a los contribuyentes cuyos bienes, en su conjunto, resulten en un valor igual o inferior a los ocho millones de pesos.