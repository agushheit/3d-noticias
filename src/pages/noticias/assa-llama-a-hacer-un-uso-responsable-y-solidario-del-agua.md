---
category: Agenda Ciudadana
date: 2021-06-24T08:40:24-03:00
thumbnail: https://assets.3dnoticias.com.ar/assa.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa ASSA
resumen: Assa llama a hacer un "uso responsable y solidario" del agua
title: Assa llama a hacer un "uso responsable y solidario" del agua
entradilla: Desde Aguas Santafesinas piden cuidar el agua en los domicilios ya que
  por la bajante del río no se puede "asegurar al 100% la prestación del servicio"

---
“Es necesario una colaboración en el comportamiento de nuestra población referida al cuidado del agua, sostener usos esenciales y evitar usos superfluos”, dijo Guillermo Lanfranco, gerente de Comunicación de Aguas Santafesinas (Assa). Desde la empresa estatal aseguran que se está viviendo la bajante histórica más importante del río Paraná debido a su extensión en el tiempo y al nivel alcanzado. “Frente a eso estamos haciendo todo lo posible para sostener la capacidad de captación de nuestras plantas”, dijo Lanfranco.

Aguas Santafesinas cuenta con siete plantas sobre el sistema del río Paraná, desde Reconquista a Rosario. Según el gerente de Comunicación, las dos tomas de Santa Fe, una sobre el río Colastiné y otra sobre el río Santa Fe, no están teniendo dificultades actualmente.

Sin embargo, informó que se está reforzando en lo posible la capacidad de bombeo cambiando la profundidad de bombas, con el objetivo de que no se afecte la prestación del servicio: “Algo que no podemos asegurar al 100 por ciento porque la variación del río está siendo muy profunda”, resaltó.

“Vamos a hacer todo lo posible y tomar todas las medidas necesarias para poder mantener los niveles actuales de captación”, aseguran desde Assa.

En este sentido, Germán Nessier, vocero de la empresa, afirmó: “Aguas santafesinas viene trabajando en tareas encaminadas a asegurar y sostener la capacidad operativa de las tomas de captación de agua para prestar el servicio tanto en la ciudad de Santa Fe como en las demás localidades donde tiene plantas potabilizadoras de fuente superficial a lo largo de la provincia”.

El representante contó que estos trabajos se vienen realizando gracias al apoyo del gobierno de la provincia que desde que se inició este proceso de bajante histórica y extraordinaria del sistema del río Paraná. “Se ha continuado realizando acciones tendientes a mantener la capacidad operativa y mejorar el funcionamiento de todos los equipos con rehabilitación de bombas, profundización de las mismas, limpieza de los chupones de las bombas y tareas asociadas a los procesos propiamente dichos de potabilización”, explicó Nessier.

“Vale decir que además de todos los esfuerzos que realiza la empresa es importante que los usuarios hagan su aporte porque forman parte del sistema de prestación de servicio de agua potable”.

Reconoció asimismo: “En estas circunstancias, tanto por la bajante extraordinaria y por la pandemia de Covid que estamos atravesando donde el agua es un aliado especial para el cuidado de la salud, es muy valioso que cada ciudadano asuma un compromiso pensando en las circunstancias que atravesamos con los demás usuarios que deben recibir el servicio”.

“Además de ser sus destinatarios deben hacer su aporte haciendo un uso responsable y solidario de un servicio que es escaso y que la empresa pone a disposición toda su capacidad instalada”, concluyó Nessier.