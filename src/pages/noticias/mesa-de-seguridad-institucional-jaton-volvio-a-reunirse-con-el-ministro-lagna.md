---
category: La Ciudad
date: 2021-04-29T08:33:53-03:00
thumbnail: https://assets.3dnoticias.com.ar/seguridad2.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad de Santa Fe
resumen: 'Mesa de Seguridad Institucional: Jatón volvió a reunirse con el ministro
  Lagna'
title: 'Mesa de Seguridad Institucional: Jatón volvió a reunirse con el ministro Lagna'
entradilla: En el encuentro se avanzó en la coordinación de los operativos de seguridad,
  tanto en calles de la ciudad como en barrios puntuales, en armerías y en desarmaderos
  de vehículos.

---
Este miércoles se concretó un nuevo encuentro de la mesa de Seguridad Institucional, entre el intendente Emilio Jatón y el ministro de Seguridad de la provincia, Jorge Lagna. En la oportunidad, las autoridades y sus equipos técnicos conversaron sobre los operativos que ambos estados llevan adelante y coordinaron la concreción de tareas para los próximos días.

En concreto, se evaluaron los controles realizados en conjunto, en el marco de las restricciones dispuestas por la Nación y acatadas por el gobierno provincial por la pandemia de COVID-19. Entre otras cosas, se repasó la cantidad de fiestas clandestinas desbaratadas, de vehículos retenidos y de personas aprehendidas por incumplimiento de la normativa. Sobre ese particular, se recordó que los operativos continuarán con la misma intensidad, al menos hasta el 2 de mayo próximo, a la espera de las nuevas definiciones del Gobierno nacional. En ese sentido, se analizó la baja en la siniestralidad que se registra, y el modo en que estas actuaciones colaboran para descomprimir la ocupación de camas de los efectores de salud, en la capital de la provincia.

Por otra parte, se abordó la realización de inspecciones en armerías y desarmaderos, y se estableció la necesidad de continuarlas. En esta línea, se habló de los controles de motovehículos en calle y se diagramó la ubicación de los mismos para las semanas que vienen. También se tocó el tema de las intervenciones multiagenciales, para las cuales se convocará a la Fiscalía, de modo que sirva de apoyo en las tareas de municipio y provincia.

Una vez culminada la reunión, Jatón aseveró que “estas instancias de trabajo y coordinación sirven y mucho, porque hay datos que no se pueden dar a conocer pero ya tenemos determinado dónde y cómo vamos a continuar con los operativos”. En ese sentido se refirió a los controles callejeros, en armerías y en desarmaderos: “Sabemos a dónde ir, de qué manera y cuándo”, aseguró.

Por otra parte, el intendente comentó que se trazó un mapa de los barrios que requieren en mayor medida estas intervenciones “en base a datos que elaboran el municipio y la Policía, porque los datos son válidos para tomar decisiones”.

Posteriormente, Lagna concluyó en que “acordamos intensificar el trabajo conjunto” y acotó que “esta mirada local de la ciudad tiene que intensificarse: cuando tenemos el mismo convencimiento de la otra parte, en este caso del intendente, las cosas van a mejorar”.

En materia de controles, el ministro se mostró “satisfecho con los resultados obtenidos” pero también destacó “la responsabilidad social que hubo”, respecto de las restricciones horarias impuestas por el Decreto de Necesidad y Urgencia. “El trabajo conjunto que hicimos con el área de control del municipio, las fuerzas nacionales y la Policía hizo que haya un alto nivel de acatamiento y responsabilidad social de la gente”, mencionó.

También agregó a la lista de temas abordados, la realización de foros barriales, cuya concreción será virtual y el cronograma de reuniones se establecerá oportunamente. A ello agregó que próximamente se destinarán a la ciudad 40 motovehículos, de un total de 400 nuevos móviles que la provincia compró pero aún no concluye el trámite de patentamiento.

Finalmente, se refirió a la futura instalación del Centro de Operaciones Especiales y Análisis Criminal en la capital provincial, cuya licitación para la compra de tecnología, avanza. En ellos se fortalecerán los sistemas de videovigilancia para ayudar a la prevención del delito y a las áreas municipales de control.

![](https://assets.3dnoticias.com.ar/seguridad1.jpg)

**El clásico**

Por la 13ª fecha del torneo de la Liga Profesional de Fútbol, para el domingo 9 de mayo está programado el partido entre Colón y Unión. Este cotejo suscita un especial interés de las partes, teniendo en cuenta la necesidad de “disminuir la circulación y tratar de que los hospitales tengan menos cantidad de contagiados”, afirmó Jatón. Al respecto, aseguró que para lograr ese objetivo “todo es válido”.

Por su parte, el ministro se mostró “preocupado por el desborde que puede darse ante un festejo, en el caso de que gane alguno de los dos equipos” y afirmó que “sería muy complicado sostener esos desbordes de alegría de los hinchas”.

De este modo, el intendente se refirió al pedido que encabeza Santa Fe para que la empresa a cargo de la televisación del fútbol argentino, libere el encuentro futbolístico. “Estaríamos disminuyendo la cantidad de población que circule y vaya a los bares”, señaló.

En ese sentido, las autoridades dispusieron la realización de una reunión en la que se aborde exclusivamente ese tema. La misma se concretará la semana próxima, en día y horario a definir, y contaría con la presencia de los representantes de ambos clubes.