---
category: Estado Real
date: 2021-10-04T06:00:03-03:00
thumbnail: https://assets.3dnoticias.com.ar/2021-10-03NID_272527O_1.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La provincia construirá los primeros 9 centros de desarrollo infantil
title: La provincia construirá los primeros 9 centros de desarrollo infantil
entradilla: En los próximos días comenzarán a licitarse los primeros nueve espacios
  educativos para la primera infancia, financiados por el gobierno nacional.

---
El Ministerio de Infraestructura, Servicios Públicos y Hábitat, a través de la Secretaría de Arquitectura y Obras Públicas, desarrollará en una primera etapa las licitaciones de los primeros nueve Centros de Desarrollo Infantil (CDI), elaborados bajo el programa “Red de Infraestructura del Cuidado”, de los Ministerios de Obras Públicas y de Desarrollo Social de la Nación, y financiados por el gobierno nacional. La inversión de estos nueve centros supera los 330 millones de pesos, y cuentan con un plazo de ejecución de 240 días calendario.

En esta etapa, estos nuevos espacios educativos para la primera infancia se ubicarán en diferentes localidades: Rafaela , Reconquista, Vera, Tostado, Gálvez, Las Rosas, El Trébol, Fighiera, y Cañada de Gómez . Asimismo están proyectados para licitarse, en una segunda etapa, en Pérez, Santa Fe, Desvío Arijón, Gato Colorado, Rosario y Capitán Bermúdez.

Al respecto, la ministra Silvina Frana destacó el trabajo conjunto con el gobierno nacional a fin de fortalecer la infraestructura educativa para la primera infancia: “Cuando se logran concretar estas agendas de trabajo, significa una instancia virtuosa que permite dar inicio a una obra que va a transformar y dar oportunidades de desarrollo a las niñas y niños desde la primera infancia. Estamos construyendo el futuro para que muchos niños y niñas accedan a la educación, la promoción y protección de derechos, reduciendo las inequidades territoriales existentes".

"Transformar realidades generando políticas públicas que permitan la igualdad de oportunidades a las poblaciones más vulnerables, atendiendo el crecimiento y desarrollo de los niños, habla de un Estado presente y esto es lo que impulsa tanto el gobernador Omar Perotti como el presidente Alberto Fernández, con acciones concretas y con obras que promuevan una atención integral para el desarrollo, la estimulación temprana y la psicomotricidad", cerró Frana.

**CENTROS DE DESARROLLO INFANTIL**

A través del Ministerio de Obras Públicas de la Nación, se puso en marcha la “Red de Infraestructura del Cuidado”, con foco en la salud, los géneros y la niñez.

Los CDI son resultados de un trabajo conjunto de los Ministerios de Obras Públicas y de Desarrollo Social de la Nación, que en una primera etapa construirán unos 300 centros en todo el país, con el objetivo de reducir las brechas existentes de pobreza, género e inequidades territoriales.

Estas unidades educativas contarán con una superficie de 220 metros cuadrados, e incluirán a niños y niñas de 45 días a 4 años de edad, que funcionarán en dos turnos con capacidad de albergar 48 personas por turno, para recibir asistencia nutricional, estimulación temprana y psicomotricidad.

El prototipo tipológico propuesto cuenta con: planteo arquitectónico desarrollado en un solo nivel con una circulación principal que estructura las distintas actividades; área de administración con baño privado; cocina y depósito con ingreso propio para proveedores y personal; una sala maternal (de 45 días a 1 año) y lactario; una sala de deambuladores (1 año a 2 años); una sala de 2 años a 3 años adaptable para funcionar como comedor; una sala de 3 años a 4 años adaptable para funcionar como comedor; un bloque sanitario para infantes, además de un sanitario específico para personas con movilidad reducida; y patios internos que adecúan las condiciones ambientales requeridas, que contarán con espacios lúdicos y juegos infantiles.

**LAS LICITACIONES**

Rafaela - acto licitatorio: lunes 4/10 - 12:30 horas - Cine Belgrano, sito en calle Boulevard Santa Fe 555 de la ciudad de Rafaela. Presupuesto oficial: $37.477.957,82.

Reconquista - acto licitatorio: miércoles 6/10 - 10:30 horas - Casa del Bicentenario, sita en calle Bv. España 787 de la ciudad de Reconquista. Presupuesto oficial: $36.087.904,78.

Vera - acto licitatorio: miércoles 6/10 - 13:30 horas - Aero Club Vera, sito en calle Lisandro de la Torre y Soldado Gómez de la ciudad de Vera. Presupuesto oficial: $36.316.461,81.

Tostado - acto licitatorio: miércoles 6/10 - 17 horas - Club Atlético Tostado, sito en Ruta Pcial. Nº 2. Presupuesto oficial: $37.284.346,08.

Gálvez - acto licitatorio: jueves 7/10 - 9 horas - Casa de la Cultura, sita en calle Pasteur 444 de la ciudad de Gálvez. Presupuesto oficial: $36.087.904,78.

Las Rosas - acto licitatorio: jueves 7/10 - 12:30 horas - Club Atlético Williams Kemmis - Chacabuco y Lamadrid de la Localidad de Las Rosas. Presupuesto oficial: $36.316.461,81.

El Trébol - acto licitatorio: jueves 7/10 - 15 horas - Centro Cultural Cervantes, sito en calle Rosario 780 de la localidad de El Trébol. Presupuesto oficial: $37.720.617,36.

Fighiera - acto licitatorio: martes 12/10 - 9:30 horas - Calle Tomasini 561 de la localidad de Fighiera. Presupuesto oficial: $38.366.172,50.

Cañada de Gómez - acto licitatorio: martes 12/10 - 12:30 horas - Centro de la Juventud, sito en calle Balcarce y Sarmiento de la ciudad de Cañada de Gómez. Presupuesto oficial: $36.807.904,78.