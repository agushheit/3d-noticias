---
category: Agenda Ciudadana
date: 2022-01-24T06:00:47-03:00
thumbnail: https://assets.3dnoticias.com.ar/MOLARE.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: Juntos por el Cambio apura las internas provinciales ante posible desdoblamiento
  electoral
title: Juntos por el Cambio apura las internas provinciales ante posible desdoblamiento
  electoral
entradilla: "Hubo conversaciones sobre la posibilidad de que los gobernadores peronistas
  adelanten los comicios provinciales y los separen de las presidenciales. \n"

---
Un sector de Juntos por el Cambio empezó a conversar sobre la necesidad de preparar a la alianza opositora en distintas provincias ante la posibilidad de que los gobernadores peronistas fijen las elecciones locales de 2023 antes de las presidenciales.

El tema se conversó pocos días atrás en una reunión que mantuvieron el presidente de la UCR, Gerardo Morales, y su par del PRO, Patricia Bullrich, según confirmaron a NA fuentes del espacio al tanto del contenido de la charla.

Durante ese cónclave, celebrado en la sede nacional de la UCR ubicada a pocas cuadras del Congreso Nacional, Bullrich y Morales limaron asperezas luego de los cruces que se habían dado por las declaraciones del radical sobre la responsabilidad de la gestión de Mauricio Macri en el endeudamiento con el FMI.

Pero además, al realizar un análisis sobre la situación política de la coalición opositora, evaluaron como algo muy posible que los gobernadores peronistas quieran "despegarse del Gobierno nacional" en 2023 y adelanten los comicios en sus respectivos territorios.

"Capaz tenés una campaña electoral que arranca en febrero del año que viene", señaló un dirigente de Juntos por el Cambio consultado por Noticias Argentinas respecto de la conversación entre los presidentes de los dos partidos más grandes de la alianza opositora.

Según precisó, Bullrich y Morales coincidieron en la importancia de encontrar "los mecanismos para resolver este año las internas en cada provincia, sobre todo en las que no van a tener PASO y que van a adelantar su cronograma electoral".

La principal preocupación frente a ese hipotético escenario es, precisamente, que en alguna provincia las elecciones se adelanten y, sin primarias de por medio, encuentren a los referentes locales de Juntos por el Cambio separados o enfrentados por las candidaturas.

Uno de los casos testigo es el de San Juan, donde el gobernador Sergio Uñac logró en diciembre pasado eliminar las primarias abiertas y obligatorias, lo que para algunos es un primer paso antes de desdoblar las elecciones locales de las nacionales.

La hipótesis que manejan en la coalición opositora es que podrían ser varios los que decidan despegar la pelea electoral provincial de la competencia por la Presidencia de la Nación, luego de la derrota que sufrió el Frente de Todos en las elecciones legislativas de 2021 en más de la mitad del país.

De hecho, luego de esa derrota el gobernador de San Luis, Alberto Rodríguez Saá, salió a despegarse del Gobierno nacional, al aclarar que su gestión "no es kirchnerista ni fernandista", en lo que podría ser un anticipo de una estrategia política centrada exclusivamente en la provincia.