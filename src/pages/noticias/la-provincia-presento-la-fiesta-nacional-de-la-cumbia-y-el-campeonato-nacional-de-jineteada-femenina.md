---
category: Estado Real
date: 2021-12-04T06:15:12-03:00
thumbnail: https://assets.3dnoticias.com.ar/festichola.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La provincia presentó la Fiesta Nacional de la Cumbia y el Campeonato Nacional
  de Jineteada Femenina.
title: La provincia presentó la Fiesta Nacional de la Cumbia y el campeonato nacional
  de jineteada femenina.
entradilla: Los eventos se realizarán durante este fin de semana y se encuentran visibles
  en la plataforma “Viví Santa Fe”.

---
La Secretaría de Turismo del Ministerio de Producción, Ciencia y Tecnología presentó y brindó detalles de la realización de la quinta Fiesta Nacional de la Cumbia Santafesina y de la octava edición del Campeonato Nacional e Internacional de Jineteada Femenina.

De la presentación, realizada en la sede de la mencionada Secretaría, participaron el secretario de Turismo, Alejandro Grandinetti y el subsecretario de Desarrollo Territorial Turístico, Norberto Ruscitti.

El primero de los eventos tendrá lugar desde este viernes y hasta el próximo domingo 5 de diciembre en el hipódromo de la ciudad de Santa Fe; y las entradas se pueden adquirir de manera online en la plataforma www.vivisantafe.com, donde además, se le obsequiaran entradas a todos aquellos que reserven algún alojamiento a través de la misma. Asimismo, se encuentra vigente un sorteo a través de la cuenta de instragam de la Secretaría de Turismo de la Provincia (@santafetur), donde los ganadores obtendrán entradas sin cargo para dicha jornada.

Al respecto Grandinetti indicó: “Estamos realmente muy contentos con la vuelta de actividades, festivales y encuentros, que le están dando una fisonomía muy importante a la provincia de Santa Fe. Llegamos a acuerdos con organizadores de eventos, por ejemplo con la Fiesta Provincial de la Cumbia, en donde entregaremos dos entradas gratis para aquellos que contraten alojamiento a través de la plataforma www.vivisantafe.com. Al mismo tiempo estamos realizando sorteos en las cuentas oficiales en redes sociales de la Secretaría”.

Ahondando en la Fiesta de la Cumbia, Grandinetti también destacó: “Hay una gran iniciativa solidaria de los organizadores y las bandas, ya que en diferentes lugares de la ciudad se puede canjear entradas gratuitas por alimentos no perecederos, pañales y otras necesidades. Esto tiene un trasfondo solidario y comunitario que expresa lo que significa este género para la ciudad y la provincia”.

**CAMPEONATO NACIONAL E INTERNACIONAL DE JINETEADA FEMENINA**

La octava edición del Campeonato Nacional e Internacional de Jineteada Femenina, se desarrollará este sábado 4 de diciembre a partir de las 18 horas en Alberdi y Las Rosas, Complejo Patria de la localidad de Recreo. “Nos complace tener este evento con características internacionales y que pone el foco en la mujer”, subrayó el secretario de Turismo.

Asimismo, Ruscitti manifestó: “Este evento, que pone a la mujer en el centro de la escena, se llevará a cabo en Recreo y contará con jinetas de todo el país. Desde el Gobierno Provincial estamos acompañando fuertemente a esta actividad, que será una gran fiesta y que es el único evento de estas características de todo el país”.

Finalmente, Alfredo Baruffato responsable de la organización del evento, destacó: “Con el Gobernador Perotti, por primera vez, hay un interés provincial en esta actividad nacional e internacional. El campeonato es organizado por la Asociación Civil Santafesina de Fiesta Tradicionalistas y Jineteadas, que es única en su tipo en el país. En su octava edición esta Jineteada contará con mucha presencia de la provincia, también desde el Ministerio de Igualdad, Género y Diversidad”.

**VIVÍ SANTA FE**

Cabe recordar que ambos eventos se encuentran visibles en la plataforma “Viví Santa Fe”, donde también se puede conocer, realizar reservas y comprar toda la oferta turística de la provincia santafesina.