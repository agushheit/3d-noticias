---
category: Deportes
date: 2021-04-13T06:08:39-03:00
thumbnail: https://assets.3dnoticias.com.ar/colon-basket.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Marisa Lemos
resumen: Colón se quedó en el final y perdió con Tiro
title: Colón se quedó en el final y perdió con Tiro
entradilla: 'El sabalero no pudo con los de Morteros y cayó 87 a 80. De esta manera
  cerró su participación en la sede llevada a cabo en la ciudad de Paraná.

'

---
Por la última fecha de la Liga Argentina en la sede organizada en Atlético Echagüe Club de la capital entrerriana Colón fue derrotado por Tiro Federal 87 a 80 y de esta manera cierra su presentación con dos triunfos y dos derrotas.

 Agustín Carnovale fue el jugador más destacado en el rojinegro con 18 puntos, 5 rebotes y 5 asistencias. Mientras que el goleador del partido fue el base de Morteros, Emiliano Correa con 27 tantos, 11 rebotes y 12 asistencias.

En cuanto al desarrollo del juego, fue parejo hasta el inicio del último parcial, dónde Tiro supo aprovechar los ataques y Colón se volvió errático en los tiros y no pudo aprovechar las oportunidades para acercarse en el marcador.

Ahora, los dirigidos por Ricardo De Cecco deberán pensar en el próximo rival que será Sportivo América, encuentro que se jugará en la ciudad de Rosario el próximo 20 de abril.

**SINTESIS:**

Colón: Ríos 11, García 4, Carnovale 18, Fernández 9, Bombino 17, Gutiérrez 2,  Duarte 2, Ramos 4 y Harris 13. DT: R. De Cecco

Tiro Federal: Antoniuk 8, Lucero 2, Correa 27, Giaveno 0, Banegas 18, Basualdo 12, Tambucci 10 y Cabezas 10. DT: G. Lucato.

Parciales: 19-16, 33-32 y 60-60