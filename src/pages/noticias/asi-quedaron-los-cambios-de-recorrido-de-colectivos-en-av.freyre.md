---
category: La Ciudad
date: 2021-05-08T06:15:23-03:00
thumbnail: https://assets.3dnoticias.com.ar/cullen.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Así quedaron los cambios de recorrido de colectivos en Av. Freyre
title: Así quedaron los cambios de recorrido de colectivos en Av. Freyre
entradilla: Las líneas 16, 9 y 14 sufrieron modificaciones a raíz de la instalación
  de la pasarela que conecta el Cullen con el hospital de campaña.

---
El gobierno local informó los cambios de recorrido que sufrieron tres líneas de colectivos que habitualmente pasan por Avenida Freyre en cercanías del hospital José María Cullen de la ciudad de Santa Fe.

El motivo de las modificaciones es la instalación de una pasarela que conecta el centro de salud pública con el flamante hospital de campaña que se instaló en dependencias del Liceo Militar General Manuel Belgrano.

Se indicó que la Línea 16, cambia en el sentido sur - norte. Se desvía por calle Moreno, sigue por Francia y en lugar de retomar por Salta, lo hará por Primera Junta para volver al trayecto habitual por Av. Freyre. Mientras que en el sentido norte - sur llega por Saavedra hasta Zazpe donde a partir de ahí retoma su circulación habitual.

En tanto, la Línea 9 quedó de la siguiente manera: de recorrido habitual por Mendoza a Saavedra, Juan de Garay, 4 de Enero y Mendoza, a recorrido habitual. Mientras que la Línea 14: de recorrido habitual por avenida Freyre a Primera Junta, Zavalla, Mendoza, Saavedra y Monseñor Zaspe, a recorrido habitual.

**Paradas**

Línea 9: Avenida Freyre y Primera Junta, avenida Freyre y Mendoza, Mendoza y Saavedra, Saavedra y Lisandro de la Torre, y Juan de Garay y Francia.

Línea 14: Mendoza y avenida Freyre, Mendoza y Saavedra, Saavedra y Lisandro de la Torre, Saavedra y Juan de Garay, Saavedra y Moreno, y Monseñor Zaspe y San Lorenzo.

Línea 16: Avenida Freyre y Moreno, Saavedra y Moreno, Francia y Corrientes, Francia y Lisandro de la Torre, Francia y Mendoza, y Primera Junta y San Lorenzo.

![](https://assets.3dnoticias.com.ar/recorridos.jpg)