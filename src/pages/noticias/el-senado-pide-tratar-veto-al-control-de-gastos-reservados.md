---
category: Agenda Ciudadana
date: 2021-02-12T08:00:27-03:00
thumbnail: https://assets.3dnoticias.com.ar/traferri.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: El Senado pide tratar veto al control de gastos reservados
title: El Senado pide tratar veto al control de gastos reservados
entradilla: 'Las tensiones de los diputados opositores con el ministro Marcelo Sain
  se trasladaron al Senado. Duras críticas sobre los resultados en esa cartera desde
  la bancada radical y del sector del Nes del justicialismo. '

---
Sobre tablas, y por unanimidad, la Cámara de Senadores aprobó un proyecto de comunicación que formalmente le solicita al Poder Ejecutivo que habilite, en extraordinarias, el tratamiento de su veto contra la ley que pretende regular y controlar los gastos reservados en los tres poderes del Estado.

El texto lleva las firmas de tres de los presidentes de los bloques de la Cámara alta: Armando Traferri (PJ-Nes-San Lorenzo), Felipe Michlig (UCR-San Cristóbal) y Hugo Rasetto (UCR-US-Iriondo). De todas formas, fue votado también por los integrantes del bloque del oficialismo Lealtad, ya que no hubo pedidos de abstención o manifestaciones en contrario.

Fue el jefe del interbloque radical quien puso la mira en el ministro de Seguridad Marcelo Sain, cuando justificó desde su banca el pedido para poder discutir el rechazo del Poder Ejecutivo a que “se transparenten” los montos gastados sin rendición de cuentas, en los tres poderes.

El veto ya fue rechazado en la Cámara de Diputados y si el Senado hiciera lo mismo la ley quedaría confirmada. Sin embargo, si el gobernador Omar Perotti no habilita su tratamiento ahora, la Cámara deberá esperar hasta el 1ro de mayo.

Un extenso y por momento áspero debate tuvo como centro al funcionario mencionado, y en especial, sus magros resultados.

El discurso más duro provino del jefe del bloque Juan Domingo Perón. Traferri dijo que “necesitamos saber cuál es el plan que tiene este gobierno”, y advirtió: “todos tenemos que hacernos cargo” de que los dirigentes políticos santafesinos “estamos en deuda con la sociedad”.