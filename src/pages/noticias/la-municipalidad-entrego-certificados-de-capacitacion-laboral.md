---
category: La Ciudad
date: 2021-12-16T06:00:08-03:00
thumbnail: https://assets.3dnoticias.com.ar/Capacitacionesmuni.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Municipalidad entregó certificados de capacitación laboral
title: La Municipalidad entregó certificados de capacitación laboral
entradilla: Fue este martes, en la sede de Capital Activa. Se otorgaron diplomas a
  santafesinos y santafesinas que completaron cursos y participaron de iniciativas
  innovadoras organizadas por el municipio.

---
El intendente Emilio Jatón encabezó este martes, la entrega de certificados a vecinos y vecinas de la ciudad que durante el último trimestre del año, acciones de formación y cursos de capacitación organizados por la Secretaría de Producción y Desarrollo Económico del municipio. Se trata de iniciativas concretadas en conjunto con el Ministerio de Trabajo, Empleo y Seguridad Social de la Nación.

La actividad se enmarca en las políticas públicas implementadas por la Municipalidad en pos de fomentar el empleo, la formación y la inserción laboral. Para ello, se trabaja en la generación de puentes entre las empresas de la ciudad que buscan trabajadores y aquellas personas desempleadas o en busca de empleo.

En la oportunidad, Jatón afirmó que “los problemas como el desempleo no se pueden encarar desde un sólo lugar y el hecho de que participen la Nación y la provincia habla a las claras de que estamos viendo el problema pero también parte de las soluciones”.

“La vida es un desafío a diario y depende de nosotros con qué herramientas encaramos ese desafío. El empleo forma parte de la vida; sentir que uno puede generar algo con sus propias manos es lo más dignificante que nos puede pasar. Para que eso suceda tenemos que saber qué hacer con esas manos, tenemos que capacitarnos porque así es más fácil enfrentarlo”, subrayó.

Además, remarcó el esfuerzo personal de cada uno de los participantes y aseguró que desde la administración pública “brindaremos todos los elementos que sean necesarios y fortaleceremos esta oficina de empleo. Cuenten con nosotros, pero nunca duden en capacitarse porque el trabajo nos ayuda a ser mejores personas”, concluyó.

Por su parte, el secretario de Producción y Desarrollo Económico municipal, Matías Schmüt, mencionó que “el intendente Emilio Jatón se puso como objetivo para su programa de gobierno, relanzar y potenciar la oficina de empleo”. Además, agradeció la presencia de funcionarios nacionales y provinciales, “porque sin las herramientas de las que ellos disponen, sería imposible llevar adelante todo lo que hacemos en la oficina de empleo”.

“En dos años llevamos más de 500 personas capacitadas y es muy importante que esta oficina de empleo sea visible. Y para eso, nada mejor que otorgar sus certificados a quienes participaron”, agregó Schmüth.

Posteriormente, el Jefe de la Agencia Territorial Santa Fe del Ministerio de Trabajo, Empleo y Seguridad Social de la Nación, Daniel Mendoza, expresó que este tipo de actividades “nos causa una enorme alegría, porque vemos reflejado el trabajo administrativo que hacemos diariamente; vemos que las políticas del Estado llegan a quienes las requieren”.

En igual sentido, agregó que “fortalecer la oficina de empleo es generar más trabajo y nuestro compromiso desde el Ministerio de trabajo de la Nación es apostar a Santa Fe, aportando nuestro granito de arena a través de los programas que tenemos en marcha”.

Finalmente, el subsecretario de Empleo del Ministerio de Trabajo, Empleo y Seguridad Social de la provincia, Eduardo Massot, remarcó “el esfuerzo” que significó completar las capacitaciones, para quienes recibieron sus certificados. “El compromiso de los ciudadanos y las ciudadanas con el empleo, llevando adelante la capacitación que les permita acceder a un mejor empleo, no sólo es un compromiso personal sino también con su familia y la sociedad en su conjunto”.

**Cursos y programas**

En primera instancia, se otorgaron diplomas a quienes completaron los cursos de Instalación y mantenimiento de filtros de piscinas, Instalación de termotanque solar, y Reparación de aire acondicionado. Los mismos se organizaron en conjunto con la Cooperativa Automutual.

Posteriormente recibieron su certificado quienes participaron del proyecto “Menús inclusivos”. Se trata de una iniciativa novedosa, surgida a partir de la pandemia por Covid-19 y la modificación de hábitos que el virus trajo consigo. En cumplimiento de las medidas sanitarias determinadas en una primera instancia, y con el fin de minimizar las posibilidades de contagio de Coronavirus, los negocios gastronómicos comenzaron a implementar menús digitales a través del código QR. Hoy se estima que en la ciudad, el 70% de los comercios del rubro utiliza esta tecnología; pero la misma no es accesible para discapacitados auditivos o visuales. Ante esto, la Municipalidad y la Asociación Hotelera Gastronómica de Santa Fe propusieron incorporar en los menús con Código QR la interpretación en Lengua de Señas Argentina (LSA) en formato audiovisual y en PDF, posibilitando el acceso a personas ciegas o con baja visión.

Acto seguido fue el turno de quienes formaron parte del programa “Promover”, que asiste a personas con discapacidad a que construyan o actualicen su proyecto de formación y ocupación. De este modo, se las acompaña en el desarrollo de trayectorias laborales, en experiencias de formación o de entrenamiento para el trabajo, en la generación de actividades productivas de manera independiente y/o en la inserción en empleos de calidad.

A continuación, se reconoció a quienes completaron el curso de Formación de asistentes personales para apoyo a la autonomía de personas con discapacidad, realizado en conjunto con el Ministerio de Trabajo, Empleo y Seguridad Social de la provincia, en el marco del programa “Capacitate”. El mismo tuvo como objetivo, formar asistentes personales que promuevan la vida independiente y participación social de las personas con discapacidad usuarias de esta forma de apoyo.

También se entregaron diplomas a quienes cursaron los módulos de Introducción al trabajo, y Apoyo para la búsqueda de empleo. Se trata de herramientas que posibilitan a las personas mejorar sus oportunidades de empleo, reflexionando sobre los objetivos de educación y laborales de cada uno, analizando su experiencia y conocimientos, y las demandas de los puestos de trabajo.

Por último, el reconocimiento fue para quienes formaron parte del programa Entrenamientos para el Trabajo (EPT), basado en la formación en ámbitos laborales de aquellos santafesinos que buscan empleo, favoreciendo su inclusión laboral. Quienes realizan los entrenamientos reciben capacitación en diversos puestos, lo que posibilita que las empresas conozcan en profundidad a quienes podrían convertirse en sus futuros trabajadores.

**Experiencias en primera persona**

Viviana Iglesias realizó el curso de Auxiliar de personas con discapacidad, en el marco del programa “Promover”. Según explicó, la experiencia le permitió “afianzar conocimientos y tener herramientas para trabajar con personas con discapacidad y darles una mejor calidad de vida. Las profesoras nos dieron herramientas muy sabias para desempeñarnos en nuestra vida”.

Por su parte, Melina Ríos, participó en el programa de Introducción al trabajo y contó sus vivencias a lo largo del mismo, incluso con un entrenamiento laboral en un local ubicado en el shopping. “Aprendí a armar mi currículum y relacionarme con otras personas en el ámbito laboral”, dijo. “Me encanta que desde la Municipalidad se incentive a los jóvenes a buscar su empleo”, concluyó.

Finalmente, Rodrigo Banegas y Estella Cejas, dos personas no videntes, fueron los encargados de confeccionar los códigos QR para los Menús inclusivos en bares y restaurantes. Ambos se mostraron muy entusiasmados por la experiencia y detallaron que las personas ciegas o con visión disminuida “utilizamos lectores de pantallas que muchas veces son inaccesibles en algunas plataformas, por lo que nos dedicamos a chequear la accesibilidad de esta propuesta”.

Estella aseguró que fue una “experiencia laboral muy rica para nosotros y por el gran aporte que significa”. Rodrigo, por su parte, contó que “nos sentimos muy bien y muy cómodos trabajando con la gente de la Asociación Hotelera Gastronómica que nos hizo las cosas muy sencillas”.

**Presentes**

Del acto también participaron el director de Empleo y Trabajo Decente de la Municipalidad de Santa Fe, Guillermo Cherner; la subdirectora de Inclusión de personas con Discapacidad de la Municipalidad de Santa Fe, Gabriela Bruno; y en representación de la Asociación Hotelera Gastronómica de Santa Fe, Rodolfo Verde y Martín Castro, entre otras autoridades.