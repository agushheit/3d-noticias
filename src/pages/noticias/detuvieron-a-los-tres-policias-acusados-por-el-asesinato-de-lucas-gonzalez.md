---
category: Agenda Ciudadana
date: 2021-11-21T06:15:47-03:00
thumbnail: https://assets.3dnoticias.com.ar/LUCAS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: Detuvieron a los tres policías acusados por el asesinato de Lucas González
title: Detuvieron a los tres policías acusados por el asesinato de Lucas González
entradilla: 'El magistrado Martín Del Viso estaba negociando la entrega con el abogado
  de los uniformados acusados por el crimen y era duramente cuestionado por la familia
  del futbolista.

'

---
Los tres efectivos de la Policía de la Ciudad acusados por el asesinato de Lucas González quedaron detenidos, luego de que el juez Martín Del Viso aceptará el pedido de los fiscales y ante la presión tanto del Gobierno porteño como del abogado de la familia del joven futbolista, Gregorio Dalbón.

Tres días después del crimen ocurrido en el barrio de Barracas, el inspector Gabriel Isassi, el oficial mayor Fabián López y el oficial José Nievas finalmente fueron encarcelados.

En la noche del pasado viernes, el titular de la Fiscalía Nacional en lo Criminal y Correccional 32, Leonel Gómez Barbella, y el de la Procuraduría de Violencia Institucional (Procuvin), Andrés Heim, habían solicitado al magistrado que avanzara en la detención de los uniformados, que ya habían sido apartados de la fuerza por orden del ministro de Justicia y Seguridad de la Ciudad, Marcelo D´Alessandro.

Los fiscales advertían sobre el riesgo de fuga y la posibilidad de que pudieran entorpecer la investigación.

Según trascendió, Gomez Barbella y Heim habían planteado que los imputados "tergiversaron los hechos al momento de informarlos a la autoridad judicial para mejorar su situación procesal, circunstancia que daría cuenta no sólo de su voluntad de sustraerse del proceso penal que se le sigue, sino también entorpecer la investigación".

Ante la demora en el accionar de Del Viso, el abogado de la familia González había reclamado que se avanzara en el sentido fijado por los fiscales: "Estamos hablando de tres asesinos que trabajaban de policías, que debían cuidarnos y lo que hicieron fue asesinar a un chico, quitarle todos sus sueños, destruir toda una familia y vaya a saber ahora dónde están, porque parece una tortuga el juez que tiene la causa: no entiendo por qué no dispone la orden de detención, habida cuenta de que están cercados y están a punto de ser detenidos, sólo falta la orden de detención del juez. Es una vergüenza que estos tres asesinos estén en libertad".

Previamente, D´Alessandro había separado a Isassi, López y Nievas, así como también había reclamado al juez que actuara "de forma inmediata".

Los tres uniformados, que formaban parte de la brigada de Sumarios e Investigaciones de la Comuna 4 de la Policía de la Ciudad, quedaron detenidos en horas de la tarde de este sábado, formalmente acusados de la comisión del delito de "homicidio agravado por haber sido perpetrado por miembros de las fuerzas policiales abusando de sus funciones": el Código Penal establece una pena de prisión perpetua. Horas antes, Del Viso había negociado la entrega de los uniformados con el abogado que los representa, Alfredo Oliván.

En tanto, el cuerpo del adolescente iba a ser velado a partir de las 18:00 en la Cochería Colonial, en Ruta 36 y Senzábal, de Florencio Varela, con una nutrida concurrencia de jugadores de las inferiores de su club.

Lucas González, de 17 años, fue asesinado el pasado miércoles cuando regresaba a su casa tras haber participado en uno de los partidos de prueba de jugadores en la 6ta de Barracas Central: el auto que manejaba uno de sus amigos fue encerrada por un vehículo de civil sin identificación en el que viajaban los tres efectivos de la Policía de la Ciudad, quienes balearon a los jóvenes y luego informaron falsamente que se trata de delincuentes.