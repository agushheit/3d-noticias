---
category: El Campo
date: 2021-03-26T08:36:07-03:00
thumbnail: https://assets.3dnoticias.com.ar/srnado.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NCN
resumen: No hubo quórum para tratar la prórroga a la ley de Biocombustibles
title: No hubo quórum para tratar la prórroga a la ley de Biocombustibles
entradilla: 'Juntos por el Cambio no alcanzó el quórum necesario para habilitar la
  sesión especial de la Cámara de Diputados para tratar la prórroga de la ley de Biocombustibles,
  que ya tiene media sanción del Senado. '

---
«No habiéndose conformado el quórum reglamentario, declaró fracasada la sesión especial convocada para el día de la fecha», señaló el presidente de la Cámara de Diputados, Sergio Massa.

Al exponer en la manifestación de minoría, el jefe del interbloque opositor en la Cámara baja, Mario Negri, reconoció que «era muy difícil alcanzar el número» de legisladores para lograr el quórum.

«En el 2006 en el Gobierno de Néstor se impulsó la ley de Biocombustibles, que significaba que los combustibles fósiles debían incorporar en su procesamiento y comercialización los biocombustibles. Eso significaba ingresar en el cambio de la matriz energética. Argentina fue de los primeros países que comenzó y fue creciendo aceleradamente entre 2006 y 2015», argumentó el cordobés sobre la importancia de prorrogar el régimen regulatorio con beneficios para el sector, que vence en mayo próximo. .

En la previa, Juntos por el Cambio había logrado unificar el pedido de sesión especial con el interbloque Federal que conduce Eduardo «Bali» Bucca, movimiento que sumó presión a Massa para que convocara formalmente a la sesión.

Sin embargo, estuvo lejos de reunir quórum, y si lo hubiera conseguido habría necesitado una mayoría de dos tercios para aprobar la iniciativa, dado que se llegó a la sesión sin dictamen de comisión. 

La bancada liderada por Negri apenas consiguió el respaldo de la monobloquista tucumana Beatriz Ávila (Partido por la Justicia Social), y ni siquiera Bucca se hizo presente en el pleno, pese a que desde su interbloque, a través de la bancada schiarettista de Córdoba Federal, habían impulsado la sesión.

El Frente de Izquierda, que suele dar quórum a todas las sesiones, tampoco aportó al quórum, como tampoco el interbloque Unidad Federal para el Desarrollo encabezado por el mendocino José Luis Ramón, ni el bloque Acción Federal que integran el riojano Felipe Álvarez y el santacruceño Antonio Carambia, ni la monobloquista neuquina Alma Sapag.

Juntos por el Cambio también sufrió deserciones de algunos diputados propios que provienen de provincias de perfil petrolero, interesadas en que no prospere la ley de biocombustibles por ser un competidor directo en el mercado de las energías basadas en hidrocarburos.

A las 12:00, cuando ya se habían cumplido 30 minutos del horario citado en la convocatoria a la sesión, Massa otorgó diez minutos de prórroga, pero no alcanzó. La demora en el tratamiento de la prórroga del régimen de biocombustibles tiene que ver con que el Gobierno decidió enviar una nueva ley de Biocombustibles, más equilibrada respecto de los intereses que representan las provincias petroleras, con la intención de que se apruebe antes del vencimiento de la normativa vigente 26.093 en mayo próximo.

La prórroga del régimen recibió había recibido media sanción por unanimidad en el Senado en octubre pasado, pero luego fue frenada en Diputados pese al compromiso del oficialismo de tratarlo en el verano.

El freno se debió a que el Gobierno decidió no incluir la prórroga en el temario de sesiones extraordinarias.