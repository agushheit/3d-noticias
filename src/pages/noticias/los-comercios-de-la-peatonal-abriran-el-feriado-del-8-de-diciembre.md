---
category: La Ciudad
date: 2021-12-07T06:15:48-03:00
thumbnail: https://assets.3dnoticias.com.ar/PEATONAVIDAD.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Los comercios de la Peatonal abrirán el feriado del 8 de diciembre
title: Los comercios de la Peatonal abrirán el feriado del 8 de diciembre
entradilla: 'Así lo informaron desde la Asociación de Amigos Comerciantes de Calle
  San Martín. "Las puertas de los comercios se abrirán en horario normal", indicaron.

'

---
Este miércoles 8 de diciembre es feriado nacional por el Día de la Inmaculada Concepción de María. Los comercios de la peatonal San Martín abrirán sus puertas en horario normal pensando en las ventas navideñas.

En relación al tema, Martín Salemi, referente del Centro Comercial santafesino, indicaba en las últimas horas a UNO Santa Fe que "el último feriado entre semana del año "seguramente los comercios abrirán sus puertas".

Este lunes la confirmación llegó por parte de la Asociación de Amigos Comerciantes de calle San Martín, que en un comunicado informaron que “la mayoría de los comercios de la Peatonal, el feriado del día 8 de diciembre, mantendrán sus puertas abiertas en horario normal”.

**Ventas navideñas**

"El año pasado las ventas navideñas comenzaron muy tarde, a partir del 15 de diciembre, casi sobre la fechas de celebración, cuando normalmente inician a finales del mes de noviembre. Desde este mes se comenzó a ver más actividad comercial en los diversos centros de ventas y todos los comerciantes están esperando un cierre de año muy positivo", subrayó Salemi a este medio.

Sobre las ventas de Navidad, el titular del Centro Comercial de Santa Fe, se mostró entusiasmado: "Esperamos muy buenas ventas navideñas donde el objetivo es alcanzar y superar los niveles de diciembre de 2019. La realidad es que el comercio está vendiendo y las expectativas apuntan a las fiestas de fin de año, de la mano del programa Billetera Santa Fe que impulsó y dinamizó mucho las ventas de los santafesinos, además, generó que otras entidades bancarias salgan al mercado con promociones y descuentos importantes, como en el mes de octubre donde ofrecieron 40 por ciento de descuento y cuotas fijas".