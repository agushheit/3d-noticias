---
category: Agenda Ciudadana
date: 2021-08-23T06:00:33-03:00
thumbnail: https://assets.3dnoticias.com.ar/MALVINAS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: El Comité de la Cruz Roja confirmó que no hubo nuevos hallazgos de excombatientes
  argentinos
title: El Comité de la Cruz Roja confirmó que no hubo nuevos hallazgos de excombatientes
  argentinos
entradilla: La confirmación llegó dos días después de que el mismo ente encontrara
  restos de cinco personas en la exhumación realizada sobre una tumba colectiva C.1.10,
  en el cementerio de Darwin.

---
El Comité Internacional de la Cruz Roja (CICR) confirmó que no hubo nuevos resultados en las excavaciones realizadas en la Caleta Trullo, Islas Malvinas, ya que no se encontraron restos que pudieran corresponder a excombatientes argentinos.

La misión está a cargo del CICR en el marco del desarrollo de la segunda etapa del Plan Proyecto Humanitario (PPH2) para la identificación de restos de excombatientes argentinos caídos en la Guerra de Malvinas de 1982.

El CICR, que realiza en las islas Malvinas una nueva etapa de identificación de excombatientes argentinos caídos en la guerra de 1982, notificó al Estado argentino y británico que no se halló material de personas en la zona de Caleta Trullo, a 60 kilómetros de Puerto Argentino, según informaron desde el Comité Internacional de la Cruz Roja.

El secretario de Asuntos Relativos a Malvinas, Antártida y Atlántico Sur, Daniel Filmus, había confirmado el jueves pasado que el equipo forense del CICR, luego de trabajar en la tumba colectiva C.1.10, se trasladaría a Caleta Trullo "con el propósito de realizar la búsqueda e identificación de una posible tumba de guerra temporaria, que podría contener restos de soldados argentinos no identificados". Sin embargo, este domingo se confirmó que no hubo hallazgos.

El próximo 26 de agosto el jefe del equipo forense, Luis Fondebrider, viajará desde las Islas Malvinas a la ciudad de Córdoba para llevar las muestras de tejido esquelético encontrados el pasado jueves al Laboratorio de Genética Forense del Equipo Argentino de Antropología Forense (LGF-EAAF), cuyos resultados se esperan para fines de octubre.

El proceso de identificación de los soldados argentinos se inició en 2012, durante el gobierno de Cristina Fernández de Kirchner, cuando se envió una carta al CICR para solicitar su intervención.

Luego en 2013 se conformó un equipo de trabajo bajo la coordinación del Ministerio de Justicia y Derechos Humanos para elaborar protocolos que permitieran obtener información de cada familia sobre su ser querido caído en Malvinas y en el año 2016, se firmó el acuerdo entre los gobiernos de Argentina y el Reino Unido por el cual se encomendó a la Cruz Roja la tarea de identificación de 121 tumbas (122 cuerpos) en el Cementerio de Darwin, cuyas lápidas decían "Soldado Argentino Sólo Conocido por Dios".