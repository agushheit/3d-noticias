---
category: La Ciudad
date: 2021-12-07T06:00:03-03:00
thumbnail: https://assets.3dnoticias.com.ar/FERIATURISMO.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Municipalidad promociona Santa Fe en la Feria Internacional de Turismo
title: La Municipalidad promociona Santa Fe en la Feria Internacional de Turismo
entradilla: Allí se reúnen todos los destinos del país y del mundo para mostrar sus
  productos, atractivos y servicios. En ese marco la Municipalidad, junto a empresarios
  de la ciudad, lleva a cabo acciones de promoción.

---
La Feria Internacional de Turismo se desarrolla con un gran nivel de convocatoria y la Municipalidad de Santa Fe, junto al sector empresario local, promociona allí la oferta turística de la ciudad. La FIT tiene una extensión de 4 días, pero los dos primeros – sábado y domingo – fueron los dedicados al público general por lo que la actividad fue realmente intensa y, en este marco, se llevaron adelante diversas acciones en el stand 1250 del sector nacional.

El puesto de la Municipalidad en la FIT fue el epicentro de muchas historias. Durante el sábado y el domingo el personal municipal brindó información turística sobre servicios y atractivos. También se repartieron entre los asistentes diferentes productos típicos de la ciudad como cervezas, cacao, alfajores, dulce de leche, merchandising institucional y se sortearon 4 estadías para dos personas. Además, el martes al finalizar la feria, se sorteará un alfajor santafesino tipo torta entre todas las personas que se anotaron para el sorteo.

Todo esto es posible gracias al trabajo de la Municipalidad junto a las diferentes empresas locales y entidades que integran el Safetur.

**Actividad institucional**

La feria es además un momento de encuentro entre diversos destinos. Es un momento para entablar lazos con el resto del país. Por eso luego del acto de apertura (que se realizó el sábado 4 a las 13) el intendente Emilio Jatón, acompañado por el secretario de Producción y Desarrollo Económico de la Municipalidad Matías Schmüth y el director de Turismo Franco Arone, recorrió la Feria junto a autoridades nacionales.

“Esta acción de promoción es realmente muy importante para nuestra ciudad. Es la oportunidad que tenemos los y las santafesinas de mostrar al resto del país y del mundo todo lo que nos enorgullece de nuestra Santa Fe”, dijo el intendente.

Durante la tarde del sábado, Jatón mantuvo reuniones en el stand para fortalecer lazos e intercambiar experiencias de gestión con autoridades como Pablo Sismanian, director de Productos Turísticos del Ministerio de Turismo y Deportes de la Nación; Gonzalo Robredo, presidente del Ente Autárquico de Turismo de la Ciudad de Buenos Aires; Sebastián Slobayen, ministro de Turismo de la provincia de Corrientes; Aldo Alvarez, secretario de Turismo de Concordia y Luis Macagno, secretario de Turismo de San Luis. Además recibió al secretario de Turismo de la provincia de Santa Fe, Alejandro Grandinetti, con quien intercambiaron visiones sobre la realidad del sector en la provincia y la región.

Finalmente Jatón recibió a representantes del sector privado. En ese sentido mantuvo reuniones con Fernando Desbots, presidente de la Federación Empresaria Hotelera y Gastronómica de la República Argentina (Fehgra) y Matías Sket, presidente de la Asociación Argentina de Organizadores y Proveedores de Exposiciones, Congresos, Eventos y Burós de Convenciones (Aoca).

“Ha sido una jornada muy intensa, en la que pudimos intercambiar visiones con otras autoridades vinculadas al turismo de diferentes provincias y ciudades. También tuvimos la oportunidad de escuchar a representantes del sector privado y eso es fundamental para poder trazar mejores políticas para el sector”, señaló Jatón al finalizar las reuniones.

Finalmente el intendente destacó “el esfuerzo del personal de la Dirección de Turismo, que trabajó previamente para que todo sea excelente aquí y también en estos días que son de una gran intensidad”.

La jornada del sábado culminó con la visita al stand de la Municipalidad de Santa Fe de Matías Lammens, ministro de Turismo y Deportes de la Nación, y Yanina Martinez, secretaria de Promoción Turística de la Nación.

En tanto, el domingo se mantuvieron encuentros con Andrés Ziperovich, director de Desarrollo Turístico de la Subsecretaría de Turismo de la provincia de Buenos Aires, y Gustavo Hani, presidente de la Cámara Argentina de Turismo.

La agenda continuó con la realización de la charla de presentación del destino en el Auditorio 3 de la Feria a cargo de Franco Arone y de personal de la Municipalidad. La presentación “Inteligencia Turística: Innovación e Inclusión como modelo de Desarrollo Turístico en Santa Fe Capital” expuso los diferentes desarrollos con los que cuenta la ciudad de Santa Fe en materia turística como las audioguías, los códigos QR para acceder a información turística, planos georeferenciados, entre otras propuestas. Además se presentó el Sistema Inteligente de Calidad Turística (Sicat), desarrollado desde la Dirección de Turismo para el manejo y optimización de recursos e información del organismo de turismo.

**Balance positivo**

La FIT es uno de los eventos más importantes para el sector turístico en el país y la región. La edición 2021 es la primera experiencia en este nuevo contexto de pospandemia y por eso mismo era muy esperada. El evento se llevó a cabo cumpliendo medidas de cuidado como uso obligatorio de tapabocas y sanitización con un gran marco de público.

Se estima que al menos unas 90 mil personas circularon durante el fin de semana en el Predio de La Rural recorriendo los diferentes stands de la Feria, recolectando información y conociendo diferentes destinos; y la ciudad de Santa Fe fue uno de ellos.

La presencia de la Municipalidad de Santa Fe en la FIT es el resultado del trabajo conjunto entre el sector público y privado. En ese sentido es muy importante la sinergia que se genera en el marco del Safetur, donde se reúnen las entidades más importantes del turismo de la ciudad de Santa Fe; también fue fundamental el apoyo de asociaciones como ADER y empresas locales como El Quilla, Cerveza Santa Fe, Milkaut y Merengo.