---
category: Agenda Ciudadana
date: 2021-02-19T06:08:11-03:00
thumbnail: https://assets.3dnoticias.com.ar/afip.png
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Agencias
resumen: 'Ganancias: buscan eliminar del cómputo aguinaldo, horas extras y viáticos'
title: 'Ganancias: buscan eliminar del cómputo aguinaldo, horas extras y viáticos'
entradilla: El oficialismo buscará "en los próximos días" lograr la rebaja del Impuesto
  a las Ganancias, luego de que Alberto Fernández firmara el Decreto para su tratamiento
  en sesiones extraordinarias.

---
A comienzos de este febrero, Massa presentó un proyecto para elevar el piso de remuneración a partir del cual se comienza a pagar el impuesto a las ganancias, fijado en unos 152 mil pesos, pero dentro del Frente de Todos surgieron más propuestas que se plasmarán en un texto que llegará mañana a la Cámara de Diputados. 

**Los ítems a exceptuar del impuesto a las ganancias**

Según el borrador que se está terminando de trabajar, la iniciativa toma como base algunos conceptos de un proyecto que presentó el propio Moyano en 2017, entre los que figuran la exención del pago de Ganancias para:

* El aguinaldo o sueldo anual complementario (SAC)
* Las horas suplementarias que excedan la jornada legal o convencional
* El adicional por horas nocturnas
* Las sumas percibidas por feriados obligatorios y días no laborables
* Los adicionales por productividad, eficiencia, desarraigo, trabajo insalubre.
* Los viáticos y plus por comida.
* Las indemnizaciones legales por despido, muerte o accidente

Con la firma de un decreto que fue publicado hoy en el Boletín Oficial, el presidente Alberto Fernández habilitó la incorporación al temario de las sesiones extraordinarias del proyecto de reforma del impuesto a las ganancias pero por cuestiones reglamentarias, la media sanción no podrá ser lograda hasta después del 1° de marzo, con el inicio de las sesiones ordinarias.

De acuerdo con las negociaciones en marcha, todavía no está claro si el proyecto de Massa se tratará por un carril separado al que motorizan los diputados sindicales o si habrá incorporaciones al texto original para aprobarlo de una sola vez.

**Las modificaciones de la oposición**

En esa línea, la oposición también pretende sumar modificaciones, enfocadas sobre todo en la situación de los trabajadores autónomos y monotributistas que no fueron incorporados en el proyecto. 

"La ampliación del mínimo no imponible para jubilados (de 6 jubilaciones mínimas a 8 jubilaciones mínimas) en el proyecto del oficialismo sobre el impuesto a las ganancias no es suficiente. La jurisprudencia de la Corte Suprema y tribunales inferiores han declarado la inconstitucionalidad del pago del impuesto a las ganancias para los jubilados", señalaron desde Juntos por el Cambio.