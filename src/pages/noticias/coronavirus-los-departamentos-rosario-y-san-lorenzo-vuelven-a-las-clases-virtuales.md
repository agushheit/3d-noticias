---
category: Agenda Ciudadana
date: 2021-05-03T06:15:13-03:00
thumbnail: https://assets.3dnoticias.com.ar/08-01-21-docentes.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'Coronavirus: los departamentos Rosario y San Lorenzo vuelven a las clases
  virtuales'
title: 'Coronavirus: los departamentos Rosario y San Lorenzo vuelven a las clases
  virtuales'
entradilla: El resto de los departamentos de la provincia de Santa Fe continuarán
  con las clases presenciales con burbujas.

---
Los departamentos San Lorenzo y Rosario, que se encuentran en estado de alerta epidemiológica de acuerdo a lo publicado por el ministerio de Salud de la Nación, suspenderán las clases presenciales y, a partir de este lunes, volverán a las clases virtuales**.**

**Trotta con Cantero**

El ministro de Educación de la Nación, Nicolás Trotta mantuvo una reunión con los ministros de Educación de Santa Fe (Cantero), provincia de Buenos Aires, Capital Federal y Mendoza, Se analizaron las estrategias de continuidad pedagógica de los estudiantes de las localidades en las que rige la suspensión de la presencialidad por la alerta epidemiológica.

"En esta situación excepcional queremos poner la mirada especialmente en las trayectorias educativas de estudiantes con discapacidad asegurándoles los apoyos y recursos que requieran; y en aquellos/as con trayectorias discontinuas, truncas o que necesiten promoción acompañada", dijo Trotta.