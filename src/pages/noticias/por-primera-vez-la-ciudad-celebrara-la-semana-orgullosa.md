---
category: La Ciudad
date: 2021-12-11T06:00:06-03:00
thumbnail: https://assets.3dnoticias.com.ar/ORGULLO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Por primera vez, la ciudad celebrará la Semana Orgullosa
title: Por primera vez, la ciudad celebrará la Semana Orgullosa
entradilla: ".Las acciones buscan acompañar la marcha del orgullo que organizan junto
  a la Municipalidad."

---
Para acompañar la Marcha del Orgullo que se realizará el domingo 12 en Santa Fe, la Municipalidad organizó la Semana Orgullosa, la primera que se hace en la ciudad capital. En este sentido, habrá distintas actividades en diferentes espacios.

Comenzará hoy, en la Sala Marechal del Teatro Municipal (San Martín 2020) donde se llevará a cabo la muestra del taller de teatro Existencias Trans, en el marco de Varieté Escenas. Será a las 19 y a las 21. Luego, el domingo 12 se hará la marcha y para eso se invita a la ciudadanía a participar. La misma es organizada por la Mesa del Orgullo, comenzará a las 16.30 con la concentración en la Plaza San Martín para luego dirigirse hasta la Plaza 25 de Mayo.

En ese sentido, a partir de un proyecto aprobado en el Concejo Municipal, la Municipalidad habilitó la gratuidad del boleto de colectivos entre las 15 y las 18.30 en las líneas con destino a Plaza San Martín; y de las 19.30 hasta las 23 en las líneas cuyas paradas están en un radio de 500 metros de la Plaza 25 de Mayo y con el sentido del centro a los barrios.

El martes 14, a las 17.30, en el Predio Ferial (Las Heras 2883) se realizará una barrileteada y merienda con niñeces diversas. Es organizado por Varones Trans y No Binaries de Santa Fe junto con la Municipalidad. La intención es comenzar a visibilizar las niñeces diversas y por eso, junto a la organización Varones Trans y No Binaries de Santa Fe, se realiza este encuentro con el fin de compartir un espacio cuidado libre de violencia y discriminación.

Por último, el jueves 16 se realizará un nuevo encuentro en el Mercado Progreso, en el marco de la agenda cultural que lleva a cabo la Municipalidad una vez al mes. Se denominará Fiesta Orgullosa y, como las actividades anteriores, es importante destacar que la producen y la llevan adelante los integrantes del colectivo disidente.

Comenzará a las 19 con una obra de teatro con el título Extraño tu Perfume, de un grupo de Paraná, Entre Ríos, donde se reflejan las problemáticas que atraviesan. El cierre estará a cargo de una cantante trans de Buenos Aires: Sahya Sathia. También habrá distintos puestos de feriantes y propuestas locales.

La subdirectora de Ampliación de Derechos de la Municipalidad, Florencia Costa, resumió: “La intención de estas actividades es pensar y generar acciones para visibilizar las disidencias desde la Municipalidad junto a las organizaciones. Pero también para que este colectivo, que está todo el año luchando y viviendo distintas situaciones de violencia y discriminación, tenga espacios cuidados, donde puedan sentirse libres de ser lo que realmente quieren ser”.

**Acompañamiento y acciones**

Siguiendo esta línea, Florencia Costa describió las distintas acciones que se llevan a cabo desde la Municipalidad con el fin de garantizar derechos a los integrantes del colectivo disidente. “Hacemos acompañamiento al cambio registral. Es un proceso importante y muchas veces las personas trans no tienen el vínculo afectivo para atravesar este paso, entonces estamos al lado ellas brindando asesoramiento y contención”, dijo en primer lugar la funcionaria.

Luego, agregó: “También brindamos distintos tipos de asistencia y ayudamos a que se inscriban a programas sociales de todo tipo. Se acercan hasta la oficina en Las Heras 2883 a contarnos las diferentes problemáticas y demandas y vemos a qué programas aplican”. La funcionaria también remarcó que el abordaje de cada caso es integral, ya que se contemplan todas las necesidades de las personas que se acercan a la Dirección de Mujeres y Disidencias y se busca articular para darles las respuestas que corresponden desde cada nivel del Estado.

Por otro lado, también se hace una labor sostenida para que los espacios municipales tengan las herramientas para garantizar el trato digno y el respeto de las disidencias. “Estamos realizando capacitaciones sobre diversidad, trato digno, sobre qué implica la ley de identidad de género, entre otros”, explicó Costa y siguió: “Este trabajo lo venimos sosteniendo en todas las áreas de la Municipalidad, sobre todo en las Estaciones de Convivencia y Cuidados y también en el Centro de Salud Nuevo Horizonte; dimos ESI en los jardines municipales y dejamos abierta la puerta para que cualquier institución se sume a las diferentes propuestas”.