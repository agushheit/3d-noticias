---
category: La Ciudad
date: 2020-12-18T10:48:33Z
thumbnail: https://assets.3dnoticias.com.ar/1812playa.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Diario UNO'
resumen: 'Fiestas en Santa Fe: qué pasará con los controles, boliches y comercios'
title: 'Fiestas en Santa Fe: qué pasará con los controles, boliches y comercios'
entradilla: El intendente Emilio Jatón brindó algunas precisiones sobre lo que hasta
  el momento ya está diagramado en la ciudad ante la llegada de los festejos para
  Navidad y Año Nuevo.

---
El intendente Emilio Jatón junto a parte de su gabinete presentaron las propuestas turísticas que tendrá la ciudad este verano tan particular por la pandemia y por la situación de que muchos santafesinos realizarán turismo interno. 

Además de ello, la oportunidad fue propicia para que se le consultara sobre determinaciones de lo que pasará en la ciudad de Santa Fe en lo que respecta a los festejos por las fiestas de Navidad y Año Nuevo.

Por el momento se determinó que se realizarán controles de alcoholemia en distintas partes de la ciudad y que los paradores que se han habilitado en las distintas playas y solárium no van a poder abrir sus puertas ni el 24, ni el 31 de diciembre, así como también que habrá restricciones horarias.

En cuanto al número de personas que se establecerá para poder reunirse, tanto en casa como en lugares abiertos como  la costanera santafesina, por ejemplo, dijo que están a la espera del decreto provincial para luego trabajar en lo que se hará localmente. 

Quienes sí tuvieron respuesta fueron los **gastronómicos** a quienes **se les habilitó mesas con hasta seis comensales** y no cuatro como era lo permitido hasta ahora.

En lo que refiere al comercio, si bien aún no se llegó a una determinación en cuanto a la apertura y horarios para trabajo en las fiestas, sí **permitirán que el 24 y 31 de diciembre el estacionamiento sea libre**.