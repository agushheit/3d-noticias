---
category: Agenda Ciudadana
date: 2021-07-04T06:15:36-03:00
thumbnail: https://assets.3dnoticias.com.ar/PATOBULLRICH.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Patricia Bullrich renunció a competir como candidata a legisladora
title: Patricia Bullrich renunció a competir como candidata a legisladora
entradilla: '"Estoy segura de que para esa elección no soy imprescindible, ya que
  contamos con excelentes candidatos en todo el país", manifestó Bullrich en una extensa
  carta que publicó en sus redes sociales.'

---
La presidenta del PRO, Patricia Bullrich, renunció este sábado a competir por una banca en las próximas elecciones legislativas y dijo que "apuesta al 2023", mientras que la exgobernadora bonaerense, María Eugenia Vidal, y el alcalde porteño, Horacio Rodríguez Larreta, la felicitaron por su decisión.

"Estoy segura de que para esa elección no soy imprescindible, ya que contamos con excelentes candidatos en todo el país", manifestó Bullrich en una extensa carta que publicó en sus redes sociales.

En ese sentido, sostuvo estar "igualmente segura de que la mía debe ser otra tarea: la de colaborar, desde la conducción de mi partido, en la construcción del futuro, y no disputando un cargo en la Legislatura".

Bullrich renunció así a competir por una banca en el marco de la puja interna que se mantiene en el PRO para encabezar la lista del espacio en CABA con la exgobernadora bonaerense, cuya postulación es impulsada por Rodríguez Larreta, con vistas a las próximas elecciones Primarias Abiertas Simultáneas Obligatorias (PASO).

"No renuncio a acompañar a cada argentino para que cumpla sus sueños", comenzó su carta la exministra de Seguridad de Mauricio Macri y manifestó que "como presidenta del PRO recorreré cada rincón del país para que, en esta próxima elección de medio término, logremos la mayoría legislativa".

Asimismo, señaló: "Yo decido no ser candidata para potenciar mi responsabilidad y mi compromiso político" para "hacer mío, con más libertad y fuerza, el afianzamiento de la esperanza de una sociedad que quiere salir, de una buena vez, del populismo y trabajar para superar la pobreza, la inflación, la corrupción, el delito como forma de gobierno".

"Por eso apuesto al 2023 y a llegar a cada hogar de nuestra Argentina para llevar una propuesta innovadora, que abracen con entusiasmo millones de ciudadanos, hartos y agobiados por lo que nos pasa como país", subrayó.

En otra parte de la extensa carta que publicó, Bullrich expresó que "dejar la apetencia de un cargo para seguir escuchando a cada ciudadano, esté donde esté, va a ser mi forma de demostrar que estoy convencida de que la construcción de una transformación fundamental se realiza también con renunciamientos", y agregó: "Este es el mío".

"Quiero, con mi renuncia, acercarme a la ciudadanía desde donde siento que puedo hacerlo con más profundidad y contribuir a mejorar la calidad de nuestra convivencia solidaria", escribió sobre el final del texto y destacó que "en definitiva, no renuncio ni renunciaré, en la medida de mis fuerzas, a conducir el país hacia ese horizonte en el que cada argentino pueda ver cumplidos sus sueños".

Vidal y Rodríguez Larreta saludaron a Bullrich por su renuncia con sendos posteos en sus cuentas de la red social Twitter, publicados poco después de que la presidenta del PRO difundiera su decisión.

Rodríguez Larreta, por su parte, manifestó: "gracias, @PatoBullrich, por tu gran muestra de responsabilidad, compromiso y convicción. Sin duda sos una referente en todo el país" y agregó: "vamos a seguir trabajando juntos para fortalecer la UNIDAD de @juntoscambioar, y lograr la mejor oferta electoral en la Ciudad, la Provincia de Buenos Aires y en todas las provincias de Argentina".

En tanto, el intendente del partido bonaerense de Lanús, Néstor Grindetti, también felicitó a Bulrrich en la misma red social, donde señaló: "Felicitaciones Patricia por la decisión que tomaste, tenés por delante la inmensa tarea y el compromiso de hacer más grande aún a nuestro partido en todo el país".

"Es fundamental para esta nueva etapa fortalecer en la unidad de @proargentina y @juntoscambioar para volver a ser gobierno en el 2023 con el objetivo de devolver a la argentina un proyecto de desarrollo y crecimiento sostenido", manifestó el intendente del PRO.

Por su parte, Ricardo López Murphy anunció su intención de competir con María Eugenia Vidal en una PASO, al considerar que "es el momento de consolidar el frente más amplio posible para enfrentar al kirchnerismo y ponerle un freno al populismo en el Congreso".