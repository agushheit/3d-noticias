---
category: La Ciudad
date: 2021-09-15T15:00:16-03:00
thumbnail: https://assets.3dnoticias.com.ar/amenaza1.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Ceresola
resumen: Ceresola y Suarez fueron amenazados de muerte
title: Ceresola y Suarez fueron amenazados de muerte
entradilla: Ediles del JxC recibieron amenazas de muerte por un tema vinculado a su
  actividad en el Concejo Santafesino.

---
En el día de hoy, en las oficinas del Concejo Municipal de Santa Fe dejaron un sobre para la Concejal representante del PRO en nuestra ciudad, Luciana Ceresola, y otro para el Concejal de la UCR Carlos Suarez, que contenían sendas amenazas de muerte.

La amenaza concreta dice “así vas a terminar” escrito en letras manuscritas con lapicera. Además, el sobre tiene una sustancia que a simple vista parecen ser cenizas y una publicidad de una casa de servicios fúnebres de la Capital provincial. Esta acción está directamente relacionada con la labor legislativa de los ediles mencionados. Ambos concejales han abordado en forma permanente temas difíciles que toda ciudad moderna debe enfrentar y no miran distraídos para otro lado, buscando una posición cómoda. Los problemas de los ciudadanos deben ser atendidos sin rodeos.

Todos los integrantes del Interbloque de Juntos por el Cambio del Honorable Concejo Municipal repudian enérgicamente las amenazas recibidas por estos dos legisladores de nuestra ciudad que no hacen más que trabajar todos los días por sus vecinos.

![](https://assets.3dnoticias.com.ar/amenaza2.jpeg)A pesar de la gravedad del hecho, “desde Juntos por el Cambio dejamos en claro que no vamos a claudicar en la defensa de los derechos de los ciudadanos de bien que todos los días aportan con sus impuestos al crecimiento de nuestra querida ciudad”, manifestaron desde este espacio político.