---
category: La Ciudad
date: 2021-09-22T06:00:18-03:00
thumbnail: https://assets.3dnoticias.com.ar/LOSADA1.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Losada
resumen: 'Carolina Losada: “El voto a Juntos por el Cambio es un voto a la esperanza”'
title: 'Carolina Losada: “El voto a Juntos por el Cambio es un voto a la esperanza”'
entradilla: Tras la victoria en las PASO, “Cambiemos con ganas” convocó a todas las
  fuerzas del espacio para trabajar con vistas a las elecciones generales de noviembre.
  “Juntos vamos a dejar atrás al kirchnerismo”.

---
“El voto del pasado 12 de septiembre fue un voto de esperanza, y ahora juntos vamos trabajar para comenzar a construir el país que soñamos” expresó la candidata a senadora nacional Carolina Losada, luego de que el diputado provincial, Maximiliano Pullaro reconociera finalmente la victoria de la lista “Cambiemos con Ganas” durante las internas que definieron las candidaturas de Juntos por el Cambio en Santa Fe.

Luego del escrutinio provincial definitivo, y a través de un comunicado, Pullaro desestimó el escrito presentado ante la Justicia electoral en el que señalaba que el resultado podía revertirse luego del conteo definitivo. De esta manera, Juntos por el Cambio deja superado el obstáculo y comienza el trabajo con vistas a las generales del próximo 14 de noviembre.

La lista ganadora, encabezada por Carolina Losada y Dionisio Scarpin como candidatos a senadores nacionales y Mario Barletta y Germana Figueroa Casas como candidatos a diputados nacionales, se encamina a consolidar el triunfo de Juntos por el Cambio obtenido en las PASO el próximo 14 de noviembre.

“El verdadero desafío es ahora, por eso ya nos comunicamos con todos y cada uno de quienes competían con nosotros para trabajar juntos y dejar atrás el populismo que es el principal obstáculo que tiene Santa Fe y Argentina para transformar realmente la vida de la gente y construir el país que merecemos”, dijo Losada.

Por su parte, su compañero de lista Dionisio Scarpin recalcó que “el objetivo, más allá del proceso intermedio que significaron las internas, es trabajar de manera conjunta para alcanzar las dos bancas en el senado de la Nación para Juntos por el Cambio, y obtener la mayor cantidad de diputados nacionales por la provincia de Santa Fe, con el fin de traer un mayor y necesario equilibrio y representatividad en el Congreso”.

“Concluido el escrutinio en Santa Fe y ratificando el triunfo que hemos obtenido, lo importante es el triunfo que tuvo Juntos por el Cambio. A partir de ahora, con mucha fortaleza y ganas vamos a seguir trabajando para dejar definitivamente atrás a Cristina y todo lo que significa dejar atrás al populismo, el clientelismo, la corrupción”, expresó el candidato a diputado nacional Mario Barletta.

En esa línea, Germana Figueroa Casas subrayó la importancia de la unidad que se manifiesta en Juntos por el Cambio. “Escuchamos muy bien lo que nos dijo la gente con su voto, y es por eso que vamos a estar juntos para sumar en el resultado y triunfar en Santa Fe con esta lista, que aún con nuestras diferencias, compartimos valores y principios basados en la defensa de la República”.

“Los desafíos son muchos y necesitamos estar más juntos que nunca. Solo así lograremos consolidar y aumentar la diferencia respecto del Frente de Todos, y darle el rol de equilibrio que representa el Congreso de la Nación. No nos van a robar las ganas de creer que eso es posible”, concluyó Losada.

![](https://assets.3dnoticias.com.ar/LOSADA2.jpg)