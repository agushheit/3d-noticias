---
category: Agenda Ciudadana
date: 2021-02-11T07:14:42-03:00
thumbnail: https://assets.3dnoticias.com.ar/corral.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa José Corral
resumen: 'José Corral: “Ratificamos la estrategia de la UCR de fortalecer una alternativa
  al kirchnerismo”'
title: 'José Corral: “Ratificamos la estrategia de la UCR de fortalecer una alternativa
  al kirchnerismo”'
entradilla: El dirigente santafesino se reunió con el presidente del partido, Alfredo
  Cornejo. “Compartimos nuestra convicción de que hay que fortalecer una alternativa
  al kirchnerismo en todo el país, especialmente en Santa Fe”

---
José Corral, dirigente del radicalismo y referente de Juntos por el Cambio en Santa Fe, se reunió con el presidente de la UCR nacional, Alfredo Cornejo. 

Tras el encuentro -en el que estuvo acompañado por los diputados nacionales Juan Martín y Niky Cantard, y la integrante de la Mesa de Conducción del partido, Adriana Molina-, aseguró que “estamos convencidos de la necesidad de sostener y ampliar el espacio que se conformó en su momento en Gualeguaychú -hoy Juntos por el Cambio-, y que permitió lograr alternancia en el país. Desde la UCR siempre trabajamos por un proyecto nacional que sepa reconocer y adaptarse a la situación de cada provincia incorporando a quienes quieran sumarse y tengan valores compartidos, aceptando la diversidad”.

Para el excandidato a gobernador “es una buena noticia que los partidos que componen la coalición (UCR, Pro, Coalición Cívica) estén trabajando juntos, como se demostró en la convocatoria por la apertura de las escuelas, y tal como ocurre en el parlamento y en los bloques legislativos. Y es importante también que se sumen otros espacios y partidos para fortalecer una alternativa al kirchnerismo, y representar una opción también para quienes apostaron la moderación en la figura de Alberto Fernández y se vieron defraudados”.

José Corral concluyó que “si bien en la provincia de Santa Fe ganamos las elecciones nacionales de 2015, 2017 y 2019, trabajamos en conjunto con el Comité Nacional para ampliar la base de sustentación de Juntos por el Cambio, junto a todos aquellos que compartan sus valores e ideas”.