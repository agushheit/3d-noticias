---
category: Agenda Ciudadana
date: 2021-01-04T10:03:59Z
thumbnail: https://assets.3dnoticias.com.ar/040121-fernandez.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Télam'
resumen: Fernández dijo que aceptaría «encantado» conducir el PJ si se pone al partido
  «en marcha»
title: Fernández dijo que aceptaría «encantado» conducir el PJ si se pone al partido
  «en marcha»
entradilla: Consultado sobre la intención del peronismo de promoverlo como jefe del
  Consejo Nacional del PJ, Fernández señaló que se lo plantearon «muchos gobernadores».

---
**El presidente Alberto Fernández sostuvo que aceptaría «encantado» conducir el PJ nacional si la dirigencia asume «de verdad» el compromiso de poner «en marcha» al partido, a la vez que consideró que la posibilidad de que Máximo Kirchner presida el PJ Bonaerense constituye un «recambio que hace falta».**

Consultado sobre la intención del peronismo de promoverlo como jefe del Consejo Nacional del PJ, Fernández señaló que se lo plantearon «muchos gobernadores». 

«Creo que la política es debate, discusión y confrontar ideas», sostuvo el mandatario, y añadió: «como decía Néstor Kirchner, todos somos dueños de la verdad relativa y, confrontando las verdades relativas, podemos encontrar una verdad superadora».

En declaraciones a radio AM 750, Fernández evaluó que **«hace falta un partido en movimiento y activo, no un partido obediente que esté solo para decir “Sí, presidente”».**

«Quiero que se renueve, tenemos extraordinarios gobernadores y muchos de ellos pueden ayudar en esta lógica», sumó el mandatario.

Sobre el pedido de numerosos intendentes bonaerenses para que Máximo Kirchner encabece el PJ provincial, Fernández sostuvo que es un «recambio que hace falta».

**«Siento que Máximo, por sus características, puede dar muchísimo. Es alguien con muchas convicciones y una conducta intachable, pero al mismo tiempo un hombre de mucho diálogo», afirmó Fernández.**

Tras señalar que «los que intentan pintar un Máximo encerrado y necio están muy alejados de la realidad», el mandatario indicó que «tiene mucho sentido darle un espacio a una vertiente más joven dentro del peronismo, y que tiene mucho para dar».