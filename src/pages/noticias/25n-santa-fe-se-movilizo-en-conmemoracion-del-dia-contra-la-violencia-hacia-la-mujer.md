---
category: La Ciudad
date: 2021-11-26T06:00:07-03:00
thumbnail: https://assets.3dnoticias.com.ar/niunamenossantafe.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: '25N: Santa Fe se movilizó en conmemoración del día contra la violencia hacia
  la mujer'
title: '25N: Santa Fe se movilizó en conmemoración del día contra la violencia hacia
  la mujer'
entradilla: 'En un nuevo 25N, cientos de mujeres acompañaron la marcha desde la Plaza
  del Soldado hacia Plaza 25 de mayo. La convocatoria fue organizada por la mesa de
  Ni Una Menos Santa Fe.

'

---
En conmemoración de un nuevo 25N, día en contra de la violencia hacia la mujer, cientos de mujeres marcharon en Santa Fe. La marcha a las 15 horas desde la Plaza del Soldado rumbo hacia la Plaza 25 de Mayo, frente a Casa de Gobierno.

Las actividades organizadas por la agrupación Ni Una Menos comenzaron el domingo con la inauguración de obras en el Memorial a las víctimas de violencia de género, en la Costanera Oeste, donde se añadieron los nombres de nueve mujeres asesinadas por la violencia machista entre el 3 de junio de 2019 y la misma fecha de 2021, aunque la ONG destacó que en octubre se cumplió un año sin femicidios en la ciudad de Santa Fe.

El presidente Alberto Fernández aseguró este jueves 25N que "una Argentina sin violencias requiere del compromiso absoluto del Estado y de la voluntad transformadora de toda la sociedad. En ello trabajamos. Se lo debemos a quienes ya no están, a las que han llevado adelante esta lucha y a todas las mujeres y diversidades de nuestro país", evocó desde Twitter.

Además, el Gobierno confirmó su "irrenunciable compromiso" en "desarrollar toda clase de medidas para paliar la profunda desigualdad y llevar adelante las políticas de justicia distributiva que no pueden escindirse de la perspectiva de género", en un mensaje que transmitió la asesora presidencial Dora Barrancos desde la Casa Rosada junto a las mujeres que integran el Gabinete nacional.

La ministra de las Mujeres, Géneros y Diversidad, Elizabeth Gómez Alcorta, apuntó que esta fecha "es un llamado a la acción y una oportunidad para visibilizar y resignificar las luchas colectivas contra las desigualdades y violencias de género que nos trajeron hasta acá" y ratificó que trabaja "todos los días para construir una Argentina Sin Violencias".

Gómez Alcorta también anunció hoy en la Cámara de Diputados de la Nación que se abrirá "un proceso social participativo territorial profundamente federal" de seis meses para diseñar una reforma de la Ley 26.485 de "Protección Integral para Prevenir, Sancionar y Erradicar la Violencia contra las Mujeres en los ámbitos en que desarrollen sus relaciones interpersonales" porque, dijo, "no queremos una norma que nazca de los escritorios".

"Necesitamos todas las voces y las miradas y necesitamos de una norma que se construya con muchísima legitimidad, como construimos las feministas, que es en redes, colectivamente, llegando a consensos, aprendiendo que ninguna sabe todo y otra puede hacer un buen aporte", resaltó y explicó que de ese modo cuando se tenga "la norma escrita" se la sienta "de todos".