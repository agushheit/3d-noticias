---
category: Agenda Ciudadana
date: 2021-09-24T06:00:19-03:00
thumbnail: https://assets.3dnoticias.com.ar/MANZUR.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Presentaron el programa "Mi Pieza" para asistir a mujeres de los barrios
  populares
title: Presentaron el programa "Mi Pieza" para asistir a mujeres de los barrios populares
entradilla: '"Estas mejoras se hacen con el Impuesto País y el aporte extraordinario
  de las grandes fortunas", informó el ministro de Desarrollo Social, Juan Zabaleta,
  quien presidió el acto junto al jefe de Gabinete, Juan Manzur. '

---
El jefe de Gabinete, Juan Manzur, y el ministro de Desarrollo Social, Juan Zabaleta, presentaron el programa "Mi Pieza", en el partido bonaerense de Moreno, un plan de mejoramiento de viviendas populares que en la primera etapa estima alcanzar a unas 27 mil mujeres, con una inversión de 5.000 millones de pesos, y un subsidio que oscilará entre 100 mil y 240 mil pesos.

"Estoy acá para mirar hacia adelante después de la tragedia que sufrió la Argentina", aseguró Manzur durante el acto.

Zabaleta, por su parte, agradeció "a todas las mujeres que se organizaron para cuidar a sus familias ante la pandemia, y pudieron dar respuesta en el territorio junto a las herramientas que dio el Estado".

La iniciativa está orientada a mujeres, mayores de 18 años, argentinas o con residencia permanente y residentes en barrios populares, que cuenten con el Certificado de Vivienda Familiar.

"Estas mejoras se hacen con el Impuesto País y el aporte extraordinario de las grandes fortunas", remarcó el ministro Zabaleta, y destacó que con el programa no se beneficia solo la dueña de casa: "Es el corralón de materiales, es el albañil o la albañila trabajando, es la rueda virtuosa del derecho a la vivienda, el trabajo y el consumo", explicó.

Oficialmente se informó que las mujeres beneficiarias podrán realizar obras de mejoramiento de techo, pared, pisos o aberturas, división de interiores, refacciones menores de plomería y/o electricidad y ampliación de viviendas.

"Esto es una gran alegría. Es una política que venimos trabajando hace unos meses. Es una necesidad imperiosa para los barrios", subrayó Fernanda Miño, secretaria de Integración Socio Urbana.

También estuvieron presentes, junto a Manzur, Zabaleta y Miño, el ministro de Desarrollo Social bonaerense, Andrés Larroque; la candidata a diputada del Frente de Todos, Victoria Tolosa Paz; y Mariel Fernández, la intendenta de Moreno.

"Las mujeres tenemos un papel fundamental en las familias", dijo también Miño, al asegurar que el programa beneficiará a todo el núcleo familiar.

"Soy la del medio de 9 hermanos y ojalá mi mamá hubiera tenido la oportunidad de tener este programa", dijo por último la funcionaria.

**Los detalles del programa**

Para obtener el financiamiento se debe ingresar a argentina.gob.ar/mipieza y completar el formulario de inscripción. Si hay más de 25 mil inscriptas, la selección se realizará mediante sorteo.

Los montos otorgados serán de 100 mil o 240 mil pesos y el cobro será a través de ANSES en dos cuotas del 50%. La segunda la recibirán luego de validar el avance de la obra a través de una aplicación en el celular.

Mi Pieza es compatible con cualquier prestación social, incluido el programa Potenciar Trabajo, pero quienes no completen el avance de obra quedarán inhabilitadas para nuevas líneas de asistencia económica.

El programa se financia por medio del Fondo de Integración Socio Urbana (FISU), que se compone con el 15% del Aporte Solidario Extraordinario a las grandes fortunas y el 9% del impuesto PAIS.

En una primera etapa, se espera que el programa alcance a unas 27 mil mujeres, con una inversión de 5.000 millones de pesos.