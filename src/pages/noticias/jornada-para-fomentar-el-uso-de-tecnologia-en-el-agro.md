---
category: El Campo
date: 2020-12-28T10:17:29Z
thumbnail: https://assets.3dnoticias.com.ar/2812-agro.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Jornada para fomentar el uso de tecnología en el agro
title: Jornada para fomentar el uso de tecnología en el agro
entradilla: Estuvo destinada a productores que busquen mejorar la toma de decisiones
  en los distintos sistemas productivos y la producción de alimentos de forma sostenible
  basándose en la ciencia, tecnología e innovación.

---
Bajo el lema «Soluciones Tecnológicas para el agro», se realizó este lunes la **Jornada AgTech Santa Fe 2020**, actividad impulsada por el Ministerio de Producción, Ciencia y Tecnología de la provincia en conjunto con el Ministerio de Agricultura, Ganadería y Pesca de la Nación y el INTA. 

La actividad estuvo destinada a productores que busquen mejorar la toma de decisiones en los distintos sistemas productivos y la producción de alimentos de forma sostenible basándose en la ciencia, tecnología e innovación.

El evento, **llevado a cabo de manera virtual**, se transmitió en vivo desde el canal de Jornadas Agtech y contó con dos bloques con las ponencias «Agronomía digital, herramientas que simplifican la toma de decisiones para la producción integral», a cargo del Ing. Agr. Fernando Salvagiotti (INTA Oliveros), y «Agricultura de precisión como herramienta para recuperar la fertilidad de los suelos», a cargo de Sebastián Gambaudo (docente jubilado de la Facultad de Ciencias Agrarias-UNL e investigador jubilado de INTA).

Asimismo, durante ambas instancias se pudieron conocer las experiencias de distintos emprendimientos del rubro, tal es el caso de Okaratech, que desarrolló una plataforma web y aplicaciones para dispositivos móviles para proveer soluciones integrales a las empresas agropecuarias; Sima, que desarrolló un sistema de agricultura inteligente para monitorear lotes, geolocalizar datos, analizar información y tomar decisiones de manejo; Inteliagro, dedicada a la gestión comercial inteligente; Geoagro, que desarrolla criterios para reconocer, validar y caracterizar ambientes para una producción sustentable; y Rastros, emprendimiento que ideó herramientas para incrementar la eficiencia en los procesos y optimización de recursos.

Al respecto, el secretario de Agroalimentos, Jorge Torelli, quien participó de la jornada, destacó que «pudimos presenciar dos excelentes disertaciones de especialistas en la materia y luego cinco casos de Agtechs santafesinas con productos de muy buena calidad. Este tipo de empresas son las que marcarán el rumbo de la agricultura y la ganadería en nuestra provincia», y agregó que «hoy el 80% del producto bruto interno de Santa Fe proviene de manufactura de origen agropecuario o productos primarios».

En ese marco, «el potencial del Agtech es enorme, ya que viene a generar el salto tecnológico necesario para aumentar nuestra productividad, cuidando y respetando el medio ambiente y, por sobre todas las cosas, mejorar la calidad de vida de los santafesinos».

Por su parte, la secretaria de Ciencia, Tecnología e Innovación de la provincia, Marina Baima, indicó que «esta jornada remarcó las políticas públicas y tecnológicas para el agro santafesino. Con la presencia de importantes startups de nuestra provincia, que representan la amplitud y profundidad de este ecosistema Agtech, al tiempo que mostró cómo puede potenciarse este sector trabajando en conjunto».

Durante el evento «se pudieron conocer experiencias de emprendimientos en agricultura de precisión, buenas prácticas ganaderas y agrícolas, agrotics e inocuidad alimentaria, entre otros», dijo Baima, quien explicó que «hablamos de un sector emprendedor joven, con 10 años promedio de antigüedad; son microempresas pero con una importante facturación».

«Los proyectos concretos resuelven puntos competitivos a nivel local, para los cuales, desde el Ministerio de Producción, Ciencia y Tecnología, tenemos herramientas que tienen que ver con la inversión en etapa temprana para que sus proyectos sean realidad y la transferencia tecnológica» -continuó la funcionaria.

«La Jornada Agtech mostró el desafío de transformar Santa Fe a partir del talento, el conocimiento, la sustentabilidad, la digitalización y la internacionalización, donde los sectores de desarrollo y del crecimiento económico son apalancados por la innovación, la ciencia y la tecnología a partir de la transformación de los sectores agrícolas y del procesamiento de alimentos», concluyó.

Del encuentro participaron también subsecretarios provinciales de Coordinación Agroalimentaria, María Eugenia Carrizo, y de Comercio Exterior y Nuevas Tecnologías, Lucas Candioti; y los directores nacionales de Agricultura, Agustín Pérez Andrich, y de Innovación Tecnológica y Buenas Prácticas Agropecuarias, Andrés Méndez.