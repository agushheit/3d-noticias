---
layout: Noticia con imagen
author: "Fuente: Diario UNO"
resumen: Críticas al Presupuesto
category: La Ciudad
title: Fuertes críticas de la oposición al presupuesto de Jatón
entradilla: Los concejales de Juntos por el Cambio, Carlos Pereira e Inés
  Larriera, cuestionaron el proyecto de presupuesto 2021 presentado por el
  intendente.
date: 2020-11-18T13:10:58.091Z
thumbnail: https://assets.3dnoticias.com.ar/jat%C3%B3n.jpg
---
Los concejales Carlos Pereira e Inés Larriera (UCR-Juntos por el Cambio) criticaron el presupuesto presentado por el Intendente Emilio Jatón para 2021 por la escasa inversión en obra pública, pero mantiene los mismos incrementos de la TGI, que sólo este año llega a triplicar el índice de inflación anual.

“**El presupuesto presentado por Emilio Jatón para el año 2021 contempla muy poca obra pública, y hay que tener en cuenta que estamos finalizando su primer año de gestión con cero obra pública por parte del Municipio**”, advirtieron los concejales de la UCR en Juntos por el Cambio, Carlos Pereira e Inés Larriera, que además alertaron que “por el lado de los recursos municipales el proyecto contempla un impuestazo en la TGI”.

Sobre el primer punto, apuntaron que “no hay, con recursos municipales, ninguna obra de pavimento ni de cordón cuneta, ni ningún desagüe ni obra de cloacas importante. De hecho llama la atención que se anunciaron como obras importantes tareas de mantenimiento de calles, de edificios municipales y de espacios públicos”.

Pero lo que más alertaron Pereira y Larriera está en el plano impositivo, ya que en el caso de la Tasa General de Inmuebles, el proyecto remitido por la gestión Jatón “no modifica la redacción de la Ordenanza Tributaria que se aprobó el año pasado y que nosotros no acompañamos”.

“**Hay contribuyentes que en enero de 2021 van pagar un 95,60 % más que en enero de 2020, es decir prácticamente el triple que la inflación anual**”.

Frente a esto, los concejales afirmaron que “este esquema se va a repetir el año que viene; no es que en enero se va a congelar el incremento, sino que va a continuar aumentando trimestralmente durante 2021 nuevamente”.

![](https://assets.3dnoticias.com.ar/tgi.jpg)

Lo que se aprobó el año pasado -y que permanecerá vigente- es que la Tasa se incrementa trimestralmente en diferentes porcentajes según los distintos barrios. Las alzas fueron fijadas en el 20 % trimestral en algunos sectores; el 17 % en otros; el 15 % en gran parte, y el 8 % en el resto de la ciudad. “Si bien se puso un techo del 60 %, cuando uno anualiza estos incrementos, los vecinos están pagando ya un 60 % más de TGI que en enero de este año, cuando la inflación no llega al 30 %. Y para enero de 2021 van a estar pagando hasta un 95 % más que en enero de este año”, apuntaron.

> Para poner como ejemplo, en los barrios Bulevar Galvez y Pellegrini, el Puerto, Costanera o la Peatonal, tomando como base $ 100 que pagaban en enero de 2020; ya en octubre 2020 de este año pagaron $ 163; y en enero 2021 pagarán $ 195,60

“Este esquema de incremento de la tarifa no se tocó, por lo tanto, el año que viene va a volver a incidir sobre la Tasa, y vamos a tener un incremento abusivo”, criticaron Inés Larriera y Carlos Pereira.

Y concluyeron: “**Este año la tasa creció muy por arriba de la inflación, y lo va a volver a hacer el año que viene. No es justo trasladar a los vecinos todos los problemas que tiene el Estado**”.