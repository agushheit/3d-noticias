---
category: Agenda Ciudadana
date: 2020-12-10T10:54:26Z
thumbnail: https://assets.3dnoticias.com.ar/perotti.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: La Nación'
resumen: El PJ de Santa Fe quedó al borde de la fractura por un escándalo de corrupción
title: El PJ de Santa Fe quedó al borde de la fractura por un escándalo de corrupción
entradilla: El  fiscal destituido y detenido desde agosto por cobrar coimas acusó
  a un senador del PJ y a otros dirigentes del partido de ser enlaces  para el pago
  de sobornos  entre el empresario Leonardo Peiti y la Justicia.

---
"Hay una pata política fortísima en esta investigación. Y no viene de ahora", afirmó el exfiscal Gustavo Ponce Asahad durante una declaración de más de tres horas y media en la que nombró en varios pasajes al **senador del PJ Armando Traferri, un veterano dirigente de San Lorenzo**, un enclave importante en la provincia por su poderío económico, porque es **donde están ubicadas las terminales agroexportadoras**.

Traferri, que capitanea el sector Nuevo Espacio Santafesino, a la que pertenece la actual vicegobernadora Alejandra Rodenas, también mencionada en la declaración del exfiscal, formó parte del armado de unidad del PJ que llevó el año pasado a que Omar Perotti ganara la gobernación, pero en su gestión las diferencias afloraron por varios costados desde un inicio.

Ahora **los seis senadores peronistas cercanos al gobernador se van a independizar del bloque peronista liderado por Traferri**, que impulsó leyes junto con la oposición que incomodaron a Perotti, sobre todo en cuestiones relativas a la cartera de Seguridad, en manos de Marcelo Saín, a quien desde Nuevo Espacio Santafesino le adjudican ser uno de los impulsores de esta investigación.

**Ponce Asahad acusó a Traferri de tener influencias directas en la Fiscalía de Rosario para proteger a Leonardo Peiti, un empresario que manejaba una red de casinos clandestinos** con la protección de este fiscal y su jefe Patricio Serjal, quienes están imputados por cobrar sobornos de 5000 dólares por mes a cambio de evitar que este hombre que sea investigado. 

**El empresario también le pagaba, según admitió Ponce Asahad, a Los Monos**, luego de que balearan un edificio de su propiedad y le dejaran un mensaje escrito en la pared: "Leo pagá".

Como Traferri tiene fueros, los fiscales deben esperar qué reacción tomará la Legislatura de Santa Fe, ante un posible desafuero, algo que aún no se comenzó a evaluar durante el feriado largo y es difícil que prospere. En una próxima audiencia, que podría ser la semana que viene, según las fuentes judiciales consultadas por LA NACION, se van a volcar las pruebas reunidas hasta ahora que probarían la participación del senador en la asociación ilícita.

Ponce Asahad enumeró las reuniones y llamados que había mantenido con Traferri desde 2017 hasta mediados de este año como intermediario del fiscal regional Serjal, con quien hasta el viernes compartía calabozo. La jueza Eleonora Verón ordenó el envío de la declaración de Ponce Asahad a la Corte Suprema de Justicia provincial y a la Legislatura de Santa Fe. El exfiscal pidió ser trasladado a otro lugar de detención en el ámbito federal porque su seguridad podría correr riesgos.

"El abogado (Angelo) Rossini (de Peiti, el empresario de juego clandestino) quería reunirse conmigo. Le dije por qué no se reunía con Serjal (el fiscal regional, también preso). Me dijo que no y que el senador (Traferri) estaba al tanto. Automáticamente lo llamé al senador y le dije de la reunión. Me dijo «armala para mañana». El senador me dijo «andá tranquilo y escuchalo»", contó Ponce en la audiencia en la que declaró.

"Cuando entré a la cochera, él salió a mi encuentro. Es un hombre que usa moños extravagantes. Me dijo: «Peiti se va a entregar, va a hablar y lo vamos a tumbar a Serjal. Así que hay que salir a declarar y tirarle con munición gruesa. Lo único que no hay que nombrar es al senador». Y me ofreció 40 mil dólares. Le dije que no tenía nada más que hablar. Esto para mí fue una emboscada del senador Traferri, de Rossini y de Peiti contra mí", agregó en su extensa declaración donde se quebró emocionalmente varias veces.

Traferri admitió en diálogo con LA NACION que aún no había llegado a la Cámara alta de forma oficial la declaración de Ponce Asahd. Se excusó de opinar de la situación por  solo saber  por los canales instituciones de qué se trata.

**Esta causa estalló luego de la declaración como arrepentido del empresario Leonardo Peiti, imputado por juego clandestino** -que nunca estuvo en la cárcel-. Su testimonio derivó en las detenciones de Ponce Asahad el 4 de agosto pasado y seis días después de Serjal, a quienes -según confesó- pagaba 5000 dólares por mes para que custodiaran desde el Ministerio Público de la Acusación que no hubiera investigaciones sobre los casinos clandestinos en el sur de Santa Fe.

El nombre de Traferri había sido mencionado en la primera audiencia imputativa del 5 de agosto. En la investigación que llevaron adelante los fiscales Matías Edery y Luis Shiappa Pietra apareció en uno de los teléfonos que usaba la secretaria de Peiti un llamado que había realizado el exdiputado Darío Scataglini, quien un día después de que se revelara esta llamada fue apartado de la Legislatura, donde trabajaba con el legislador y presidente del PJ de Santa Fe Ricardo Olivera, pero no pertenecía al Frente de Todos, sino que habría llegado a ese lugar por un pedido de un alto funcionario judicial.

Scataglini llamó a la secretaria del empresario imputado el 10 de julio pasado a las 20.41. "Necesitaría hablar con Leo", le dijo a la mujer y luego agregó: "Llamo de parte de Traferri". No habló directamente con Peiti, porque uno de los empleados de la fiscalía le había dicho que lo estaban escuchando y que debía destruir el teléfono.

"La vinculación de Peiti con el MPA se produce a través de Traferri con Serjal en una reunión que mantuvieron en la oficina de calle Montevideo. En la sede de la regional, en la parte delantera. ¿Por qué puedo aseverar eso? Me saludó Traferri cuando bajó y me pidió mi teléfono. Me hizo una llamada perdida para que quedara registrado. Serjal me dijo que era por una causa relacionada por Peiti. Por el padre y el hermano que era en otra jurisdicción", declaró el fiscal destituido tras el escándalo de las coimas.

**No solo Traferri aparece mencionado en la declaración del exfiscal, sino también otros miembros de la justicia provincial y federal y también funcionarios del gobierno**. Todos los que aparecen nombrados pertenecen o están cercanos al sector político que encabeza Traferri.

Ponce Asahad mencionó además cómo funcionaba esa supuesta red de complicidad ligada a la política, cuando describió una situación nunca aclarada del todo que se produjo en noviembre de 2018 durante un allanamiento -que se originó por la declaración de un supuesto arrepentido ligado al narco Esteban Alvarado- a una casa en la isla que usaba la vicegobernadora Alejandra Rodenas y su pareja el actual ministro de Cultura Jorge Llonch como residencia de fin de semana.

El nombre de la vicegobernadora había aparecido en otra audiencia contra Alvarado, acusado del homicidio del prestamista Lucio Maldonado y asociación ilícita y lavado de activos, y contra tres jefes policiales que presuntamente trabajaban para él. 

Los fiscales Edery y Schiappa Pietra revelaron dos escuchas telefónicas, en las que el acusado le ordenaba a su abogado Claudio Tavella, quien fue condenado la semana pasada en un juicio abreviado y recuperó su libertad: que "me de una mano (Alejandra) Rodenas".

<br/>

## **Un exministro de Reutemann**

Otro nombre que aparece mencionado por Ponce Asahad en su declaración es el del exministro de Gobierno de la gestión de Carlos Reutemann, Carlos Carranza. La acusación del exfiscal es fuerte porque señala que este exfuncionario habría recibido de manera irregular "un tercer" auto Toyota. Dos recibió Serjal y uno este abogado, según el exfiscal.

Fue el primer caso que puso en la mira de la Legislatura al fiscal Regional de Rosario Serjal, sospechado de archivar una causa contra un empresario al que luego le compró dos autos cero kilómetro por dos millones de pesos, en una causa investigada por la Unidad de Delitos Económicos.

La raíz de la sospecha es una causa judicial por delitos económicos que fue iniciada por Omar Santero, de carrocera Sudamericana, contra Rómulo González, actual titular de Auto Rosario, que comercializa la marca Toyota. La causa fue favorable a González por una resolución con la firma de los fiscales de grado David Carizza y Natalia Benvenutto y fue después confirmada por Serjal.

Un mes después de esta decisión, Serjal adquirió dos autos cero kilómetro en Auto Rosario. Según Ponce Asahad un tercero habría ido a parar a este abogado que pasó por el Ejecutivo durante la gestión de Carlos Reuteman.