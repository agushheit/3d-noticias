---
category: La Ciudad
date: 2022-01-27T06:15:52-03:00
thumbnail: https://assets.3dnoticias.com.ar/colectivos1.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'González: "Los subsidios al transporte quedan entre la General Paz y la
  Casa Rosada"'
title: 'González: "Los subsidios al transporte quedan entre la General Paz y la Casa
  Rosada"'
entradilla: "El miembro del interbloque del Frente Progresista señala \"falta de federalismo\"
  en el reparto y plantea cambios de fondo.\n\n"

---
Mediante un proyecto ingresado al Concejo Municipal, Leandro González solicitó al Estado Nacional, en particular ante el Ministerio de Transporte de la Nación, una “urgente” reevaluación del mecanismo de distribución de los subsidios nacionales al Transporte Público de Pasajeros.

 Dicho pedido plantea que “el reparto actual de subsidios resulta absurdamente desigual entre Buenos Aires y otras provincias y ciudades del país, repercutiendo directamente en la tarifa del servicio público, con perjuicio sobre los y las usuarias del mismo.

 Cabe destacar que este proyecto, acompañado por los integrantes del interbloque del Frente Progresista Lucas Simoniello, Laura Mondino, Paco Garibaldi, Valeria Lopez Delzar y Mercedes Benedetti,  fue comentado y dialogado en la Mesa de Seguimiento de Transporte Público, (devenida de la declaración de Emergencia del Transporte que declaró el Concejo el año pasado), que tuvo lugar este miércoles en la Estación Belgrano y en la que participaron los funcionarios municipales Mariano Granato y Andrea Zorzón, los concejales González y Carlos Pereira, representantes de ERSA, Autobuses Santa Fe y UTA; e integrantes del órgano de control.

 González explicó que “los gastos son iguales para cualquier prestataria del servicio, es necesario que un nuevo mecanismo permita clarificar la ecuación económica de la estructura de costos”, y además, ejemplificó: “Los mayores beneficios lo sigue teniendo el Área Metropolitana de Buenos Aires (AMBA), que recibe $ 110 de subsidio por cada pasajero, permitiendo tener así un boleto de $ 13 -contemplando todos los descuentos que reciben algunos grupos- en Santa Fe llegan $ 40 y en el resto del país esta asimetría también prevalece. Es una clara muestra de que los subsidios quedan entre la Avenida General Paz y la Casa Rosada, y nunca importó el color político, siempre fue igual pero hoy es más notorio”.

 También González propone crear un sistema único de cuantificación de los parámetros básicos -cantidad de kilómetros, de pasajeros, recaudación recibida, valor promedio de la tarifa, porcentajes de bonificaciones locales de las tarifas-, para unificar criterios en la distribución de subsidios nacionales en todo el país. “Esto nos va a permitir proceder a la implementación de un nuevo método de reparto más justo en todas las jurisdicciones. De este modo, determinar y prever de manera inequívoca cuál es la repartición destinada por el Estado Nacional para usuarios de los sistemas del interior”, explicó el presidente del Concejo.

 Con respecto al  Presupuesto Nacional 2022, con la consecuente implicancia de regirse por el Presupuesto 2021, el cual establece un monto de compensaciones tarifarias para el resto del país por un monto total anual de $ 28.000 millones y sobre las cuales aún no existen definiciones respecto a si ese monto se va a actualizar o no, el concejal planteó que “es necesario que haya madurez política de todos los sectores para que los municipios del interior no sean presa de discusiones políticas que poco tienen que ver con las necesidades de vecinos y vecinas de nuestras ciudades. En Santa Fe, es un gran esfuerzo el que está realizando el intendente Jatón y el equipo de gestión para cuidar cada peso de los santafesinos, se aprobó un presupuesto con la buena predisposición de todas las fuerzas políticas. Ahora es necesario encontrar el diálogo para resolver las cuestiones presupuestarias nacionales”.

 Cabe recordar que las restricciones propias de la pandemia de COVID-19 mermaron la cantidad de usuarios del transporte público en sus distintas modalidades, “entre otras variables no se ha logrado recuperar el flujo de pasajeros transportados, sea por la estricción en los aforos, los contagios o los cambios en la elección de la movilidad de las personas, situación que se traslada en la determinación de las tarifas”, precisó González.

 Asimismo, las empresas reclaman que la pandemia ha diezmado el sostenimiento del material rodante, “los recursos no alcanzaron para su reposición, en el medio de una fuerte caída de la oferta de repuestos por la falta de importación, sin stock de insumos básicos de la actividad y con variaciones de precios que superan el 100% de aumento en el año, que distorsionan cualquier posibilidad de gestión de transporte previsibles”, concluyó.