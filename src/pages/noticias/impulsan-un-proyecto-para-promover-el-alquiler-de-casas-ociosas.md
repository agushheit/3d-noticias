---
category: Agenda Ciudadana
date: 2021-06-22T07:44:17-03:00
thumbnail: https://assets.3dnoticias.com.ar/de-ponti-inquilinosjpg.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Lucila Deponti
resumen: Impulsan un proyecto para promover el alquiler de casas ociosas
title: Impulsan un proyecto para promover el alquiler de casas ociosas
entradilla: La iniciativa apunta a mejorar los precios frente a la especulación financiera.
  "Esperamos desanimar el uso especulativo y pensar el acceso a la vivienda como un
  bien social", explicó la Diputada Lucila De Ponti.

---
La diputada [Lucila De Ponti](https://www.unosantafe.com.ar/lucila-ponti-a27846.html) presentó un [proyecto](https://www.unosantafe.com.ar/proyecto-a37812.html) junto a la Asociación de Inquilinos de Rosario para la creación de un Registro Provincial de [Viviendas](https://www.unosantafe.com.ar/viviendas-a33453.html) Ociosas, con el objetivo de promover su incorporación para su uso efectivo residencial, comercial, cultural o público. La iniciativa plantea incorporar al mercado inmobiliario aquellos inmuebles que hoy se encuentran vacíos, aumentando la oferta disponible y reduciendo el precio de los alquileres.

**"Para bajar los alquileres es necesario que el precio deje de estar determinado en el marco de la especulación financiera y sea definido en relación a los ingresos de la población", explicó la legisladora del Movimiento Evita. "La vivienda tiene que ser un bien social en vez de un elemento para la especulación financiera", aseguró.**

![](https://assets.3dnoticias.com.ar/alquileres.jpg)

"En primera instancia es fundamental conocer cuantas viviendas ociosas hay en la provincia para generar acciones para desincentivar la tenencia de inmuebles para uso especulativo y promuevan que estas viviendas sean alquiladas, por eso proponemos también un aumento del impuesto inmobiliario para quienes decidan mantener las viviendas deshabitadas". En este sentido, el registro permitirá tener un panorama más preciso de la cantidad de inmuebles sin uso. "Es indispensable incluir la problemática de los alquileres para pensar una política de acceso a la vivienda digna porque un enorme porcentaje de la sociedad, mientras intenta alcanzar el sueño de la vivienda propia, alquila durante muchos años de su vida. Calculamos que un 30% de la población santafesina habita en viviendas alquiladas, es función del Estado poder garantizar que lo hagan en condiciones accesibles", finalizó.

Por su parte, Emmanuel Canelli -presidente de la Asociación de Inquilinos de Rosario- amplió los detalles de la iniciativa. “El proyecto plantea varios ejes, el primero es que aquellas viviendas que consuman por seis meses menos de 46 Kwh/mes sean consideradas ociosas (lo que consume una heladera por mes); el segundo eje es que a esas viviendas se les incremente en un 50% el API y con estos fondos se busca financiar políticas para el sector inquilino; el tercer eje, es que se mantienen las excepciones en los pagos, planteados en las leyes impositivas provinciales; y por último es que se introduce una cláusula en la cual se establece que, de aprobarse, se implementaría en el ejercicio fiscal posterior a finalizar la pandemia”, explicó.

“En Rosario, lo último que conocemos es un estudio del Centro de Estudios Scalabrini Ortiz, en el cual destaca que casi hay 80 mil viviendas vacías. Es aquí donde el estado debe poner el eje, haciendo gala de su rol de árbitro, pero, sin dejar de ver que actores somos los que corremos con las de perder. Lo que solicitamos conocer es, cuántas viviendas ociosas hay, dónde están esas viviendas, por qué están vacías, quiénes son sus propietarios y si están en buen estado. Para que desde allí el Estado pueda generar políticas que incentiven a colocarlas efectivamente en el mercado del alquiler y logren desalentar la especulación inmobiliaria de los grandes tenedores de viviendas, los corredores inmobiliarios y las constructoras”.