---
category: Agenda Ciudadana
date: 2021-07-12T06:15:03-03:00
thumbnail: https://assets.3dnoticias.com.ar/SENADO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: El oficialismo buscará convertir en ley tres iniciativas con la mira puesta
  en la economía
title: El oficialismo buscará convertir en ley tres iniciativas con la mira puesta
  en la economía
entradilla: Los proyectos sobre biocombustibles, Monotributo y Bienes Personales serán
  tratadas en el transcurso de esta semana luego de recibir dictamen favorable en
  las comisiones.

---
El Senado podría convertir en ley esta semana el nuevo marco de producción de biocombustibles que propone un corte del 5% para el gasoil y el biodiésel, y del 12% entre naftas y bioetanol, al que se suman otras iniciativas de tinte económico que prevén un alivio para monotributistas y cambios en el Impuesto a los Bienes Personales.

Las tres iniciativas recibieron dictamen favorable el miércoles pasado en distintos plenarios de comisiones de la Cámara alta, tras las exposiciones de funcionarios del Poder Ejecutivo.

La propuesta sobre biocombustibles, que ya fue votada por la Cámara de Diputados, establece un corte del 5% para el gasoil y el biodiesel, y del 12% entre naftas y bioetanol, marco que reemplazará al régimen que vence este lunes para este sector productivo.

El proyecto, además, establece que, en el caso del combustible elaborado a base de caña de azúcar, los volúmenes deberán ser del 6% de la mezcla mínima obligatoria, y el mismo porcentaje para el bioetanol a base de maíz.

La iniciativa busca reemplazar el régimen que rige en el país desde hace quince años, que venció en mayo pasado y fue extendido temporalmente por el Poder Ejecutivo.

Uno de los datos destacados relacionados con el tema fue que en noviembre pasado el Senado sancionó un proyecto que prorroga por cuatro años la norma anterior.

La iniciativa luego fue girada a Diputados, donde no avanzó y en su lugar se presentó el nuevo régimen que obtuvo sanción la semana pasada.

En representación de la oposición, el senador Alfredo De Angeli (Juntos por el Cambio-Entre Ríos) lamentó que no haya habido un debate más amplio sobre esta propuesta, mientras que su par de bloque, la legisladora por Córdoba Laura Rodríguez Machado, calificó al proyecto como "antifederal, anti ambiental y anti empleo".

En la sesión de esta semana, el Senado también buscará aprobar proyectos sobre Monotributo y Bienes Personales, que también ya cuenta con el aval de la Cámara baja.

La reforma de la Ley del Monotributo eliminará el retroactivo y los aportantes no deberán afrontar ninguna deuda por la diferencia entre lo que pagaron entre enero y junio y los nuevos valores establecidos por la ley 27.618.

En tanto, el proyecto que modifica los Impuestos a las Ganancias y Bienes Personales busca fomentar el ahorro en pesos al ampliar las exenciones en los mencionados gravámenes para los activos de inversiones financieras en moneda nacional.

Ninguna de las iniciativas recibió modificaciones durante el debate en comisiones, por lo que se descuenta que serían convertidas en ley

Ninguna de las iniciativas recibió modificaciones durante el debate en comisiones de la semana pasada, por lo que se descuenta que serían convertidas en ley por la Cámara alta.

El proyecto que modifica los Impuestos a las Ganancias y Bienes Personales busca fomentar el ahorro en pesos al ampliar las exenciones para los activos de inversiones financieras en moneda nacional.

El proyecto que modifica los Impuestos a las Ganancias y Bienes Personales busca fomentar el ahorro en pesos al ampliar las exenciones para los activos de inversiones financieras en moneda nacional.

Los integrantes de la comisión de Presupuesto que preside el cordobés Carlos Caserio escucharon la semana pasada los argumentos del subdirector General de Fiscalización de la Administración Federal de Ingresos Públicos (AFIP), Julián Ruiz, en favor de ambos proyectos al sostener que beneficiaban a los contribuyentes argentinos.

En el caso del régimen de monotributo, Ruiz dijo que "viene a complementar lo que se votó en abril sobre el sostenimiento y la inclusión fiscal de los monotributistas".

Ruiz agregó que este proyecto "beneficiará a cuatro millones 100 mil" aportantes y anunció que "se vuelve a los valores vigentes a diciembre de 2020".

"La idea es que los que pasaron al régimen general puedan regresar al del monotributo. También se establece una moratoria con condonación de multas e intereses. Se trata de un plan de sesenta cuotas y cuya alícuota máxima de interés será del 1,5% mensual", sostuvo el funcionario.

Respecto del proyecto sobre Bienes Personales, Ruiz subrayó que "la totalidad de los intereses generados en pesos por plazos fijos y demás instrumentos, como los plazos fijos UVA, no paguen Impuesto a las Ganancias".

"Queremos beneficiar las inversiones en pesos, en detrimento de otro tipo de inversiones", afirmó.

Además, destacó que, así como se exime del tributo de Bienes Personales a las cajas de ahorro, "se busca extender ese beneficio a las obligaciones negociables en pesos".

"A partir de este proyecto, queremos eximir a esos instrumentos cuando tengan como objetivo una inversión productiva", concluyó Ruiz.