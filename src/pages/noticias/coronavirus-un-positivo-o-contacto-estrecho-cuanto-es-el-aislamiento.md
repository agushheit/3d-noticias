---
category: Agenda Ciudadana
date: 2022-01-04T06:15:47-03:00
thumbnail: https://assets.3dnoticias.com.ar/contacto.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Coronavirus: un positivo o contacto estrecho, ¿cuánto es el aislamiento?'
title: 'Coronavirus: un positivo o contacto estrecho, ¿cuánto es el aislamiento?'
entradilla: 'El gobierno nacional dispuso otro esquema de aislamiento para positivos
  y contactos estrechos de casos de coronavirus. El gobierno de Santa Fe dio precisiones

'

---
La semana pasada y luego de una nueva reunión de Cofesa (Consejo Federal de Salud), se informó una nueva disposición en cuanto al esquema y modalidad de aislamiento para casos positivos de coronavirus y también para quienes se consideren contactos estrechos.

Desde el gobierno de Santa Fe también recordaron cómo se cuentan los días según las distintas situaciones:

• **“Casos confirmados”**, **con esquema de vacunación completa** (menos de cinco meses después de haber recibido su esquema primario completo o aplicada la dosis de refuerzo), deberán aislarse por siete días corridos y tres días corridos inmediatos siguientes deben maximizar los cuidados y minimizar los contactos de riesgo.

• **“Casos confirmados”, sin vacunación o con esquema de vacunación incompleto**, aislamiento por 10 días corridos.

• "**Contacto estrecho", asintomático con esquema de vacunación completo** (menos de cinco meses después de haber recibido su esquema primario completo o aplicada la dosis de refuerzo), deberá aislarse por cinco días corridos y los cinco días corridos inmediatos siguientes deben maximizar los cuidados y minimizar los contactos.

• "**Contacto estrecho", asintomático sin vacunación o con esquema incompleto** deberá aislarse por 10 días corridos, que podrá ser reducido a siete en caso de contar con un test de PCR negativo.

• Las personas que ingresen del exterior guardarán aislamiento conforme las determinaciones y por el plazo que establezcan las autoridades nacionales competentes.

Vale remarcar que se considera contacto estrecho a quien estuvo con una persona diagnosticada Covid positivo 48 horas antes del inicio de síntomas, por 15 minutos o más tiempo, sin barbijo, sin circulación o sin distanciamiento.

**Dónde y cómo funcionan los centros de testeo**

En la ciudad de Santa Fe hay seis puestos fijos de testeo masivo contra el coronavirus ubicados en distintos barrios de la ciudad. Se puede asistir sin turno previo y presentando uno o más síntomas compatibles con coronavirus o habiendo sido contacto de casos confirmados.

**Están ubicados en**

\-Estación Belgrano, Bulevar Gálvez 1150 funciona de 8 a 12 y 17 a 20.

\-Estación Mitre, General López 3698 se traslada desde este LUNES 3 de enero a la escuela Normal Nº 32, ubicado en Saavedra 1722.

\-El Alero de Coronel Dorrego, French 1701 desde este LUNES 3 enero se moverá a la escuela Nº 880 Domingo Silva, ubicada en Regimiento 12 de Infantería 2049.

\-Playón Municipal de Villa Hipódromo, Ángel Cassanello 4200 desde este LUNES 3 de enero se traslada a la la escuela Nº 19 Juan de Garay (Blas Parera al 7600).

\-Cemafé, Mendoza 2419, (de 8.30 a 12.30) de 9 a 13.

\- Iturraspe viejo, Bulevar Pellegrini 3551 de 8 a 12 y 17 a 20.