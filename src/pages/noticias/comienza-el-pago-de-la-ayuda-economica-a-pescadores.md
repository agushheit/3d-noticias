---
category: Estado Real
date: 2020-12-22T10:15:04Z
thumbnail: https://assets.3dnoticias.com.ar/2212pescadores.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Comienza el pago de la ayuda económica a pescadores
title: Comienza el pago de la ayuda económica a pescadores
entradilla: 'Será a partir de este martes y de acuerdo a la terminación de número
  de documento en cualquier boca de pago de Santa Fe Servicios cercano a su domicilio. '

---
El gobierno de la provincia de Santa Fe, a través del Ministerio de Desarrollo Social, informa que **a partir del martes 22 de diciembre comienza el pago de la ayuda económica para los pescadores** en el marco de la situación de emergencia sanitaria por Covid 19 y de la bajante histórica de los ríos.

**El pago se realizará según la terminación de número de documento** en cualquier boca de pago de Santa Fe Servicios cercano a su domicilio.

<span style="color: EB0202;">Los beneficiarios deberán concurrir con su DNI.</span>

## **El cronograma dispuesto es el siguiente:**

<br/>

* Martes 22/12 atenderán a DNI terminados en 0.

  ***
* Miércoles 23/12 atenderán a DNI terminados en 1.

  ***
* Lunes 28/12 atenderán a DNI terminados en 2.

  ***
* Martes 29/12 atenderán a DNI terminados en 3.

  ***
* Miércoles 30/12 atenderán a DNI terminados en 4.

  ***
* Lunes 4/1 atenderán a DNI terminados en 5.

  ***
* Martes 5/1 atenderán a DNI terminados en 6.

  ***
* Miércoles 6/1 atenderán a DNI terminados en 7.

  ***
* Jueves 7/1 atenderán a DNI terminados en 8.

  ***
* Viernes 8/1 atenderán a DNI terminados en 9.

  ***