---
category: Agenda Ciudadana
date: 2021-04-11T06:00:03-03:00
thumbnail: https://assets.3dnoticias.com.ar/expocarne.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: El Gobierno amenaza con cerrar la exportación de carne si siguen los aumentos
  de precios
title: El Gobierno amenaza con cerrar la exportación de carne si siguen los aumentos
  de precios
entradilla: No nos va a temblar el pulso", dijo la secretaria de Comercio Interior
  Paula Español, que se refirió a la amenaza de cerrar la exportación.

---
La secretaria de Comercio Interior Paula Español reconoció que consideran cerrar la exportación de carne si continúan en aumento los precios. “No nos va a temblar el pulso”, advirtió.

La funcionaria planteó la diferencia entre los precios de alimentos de góndola y los de los alimentos frescos. “El precio de la carne ha subido muchísimo más a finales del año pasado y también este año. Ahí es cierto que nosotros avanzamos con un acuerdo, con el que vamos a seguir trabajando”, explicó en diálogo con una radio porteña el viernes. “Hay que entender que tenemos que tomar algunas medidas”, agregó.

Español consideró que hay determinados incrementos o presiones de precios que se dan como resultado de las especulaciones. En ese sentido, destacó la importancia de frenar las subas. “La verdad es que, si seguimos viendo este tipo de comportamientos, no nos va a temblar el pulso a la hora de cerrar las exportaciones de carne”, remarcó.

Además, tampoco descartó la posibilidad de aumentar retenciones. “En un contexto así, la verdad es que hay que avanzar con ciertas herramientas de la administración del Comercio de este tipo de productos. Medidas que pueden ser subas de retenciones, encajes, cupos. Es decir, hay medidas que se pueden tomar y que realmente se están evaluando”, apuntó.

Sin embargo, la funcionaria que depende del ministro de Desarrollo Productivo Matías Kulfas, no confirmó que se tratase de una decisión tomada y le bajó el tono a la advertencia.

Detalló que el objetivo se centra en contener el avance de la inflación, luego de que entre diciembre de 2020 y febrero la minorista acumulara un aumento del 12,1%, impulsada sobre todo por el aumento del precio de los alimentos. El número de marzo aún no fue difundido, pero economistas y consultoras estiman nuevamente que girará en torno e incluso por encima del 4%. Esto compromete seriamente la meta establecida en el presupuesto 2021, que es del 29% para todo el año.

En este sentido, Español aclaró que también están trabajando en una política macroeconómica integral. Se refirió puntualmente al programa Precios Cuidados, el congelamiento de precios que se hizo el año pasado y programas de ferias de venta directa entre productor y consumidor.

“Somos conscientes de que aumentaron los costos, pero tenemos que entender que tenemos que cuidar la mesa de las argentinas y los argentinos”, señaló en relación con el malestar de empresarios que se vieron perjudicados debido a que, entre la regulación de precios y el aumento de costos, se redujo su margen de ganancias. “En la mayoría de los productos de Precios Cuidados no va a haber prácticamente incremento de precios”, adelantó.

Tras escuchar las declaraciones de la funcionaria, Miguel Schiariti, titular de la Cámara de la Industria, Comercio de Carnes y Derivados de la República Argentina expresó su malestar. “Es una bravuconada, una de las tantas de este Gobierno”, dijo. “No creo que se animen a hacerlo por varios motivos. En primer lugar, porque este país necesita los 2.500 o 3.000 millones de dólares que ingresan por la exportación de carne”, señaló.

“Además generaría un aumento de la desocupación de la mano de obra, con lo que en las zonas industriales y grandes centros urbanos de las ciudades en donde están los frigoríficos tendrían un rebote en las urnas que va a ser negativo para el oficialismo”, apuntó Schiariti.

El titular de la cámara de carnes recordó la similitud de este tipo de medidas con las adoptadas por Guillermo Moreno cuando ocupó el cargo entre 2006 y 2013. No creo que esta Secretaria de Comercio quiera convertirse en el próximo Moreno, después de saber que el efecto de esa medida implica la pérdida del stock ganadero”, cerró.