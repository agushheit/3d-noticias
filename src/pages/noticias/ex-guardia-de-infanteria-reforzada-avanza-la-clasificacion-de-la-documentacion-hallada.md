---
category: Agenda Ciudadana
date: 2020-12-21T13:08:24Z
thumbnail: https://assets.3dnoticias.com.ar/2112archivo-memoria.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: 'Ex Guardia de Infantería Reforzada: avanza la clasificación de la documentación
  hallada'
title: 'Ex Guardia de Infantería Reforzada: avanza la clasificación de la documentación
  hallada'
entradilla: Los documentos serán clasificados, identificados y trasladados al Espacio
  de Memoria ex comisaría Cuarta para ser incorporados al Archivo Provincial de la
  Memoria.

---
La Secretaría de Derechos Humanos y Diversidad de la provincia y la Policía avanzan en las tareas conjuntas para clasificar la documentación hallada en un sector de la ex Guardia Infantería Reforzda (GIR).

Para ello, este viernes la secretaria de Derechos Humanos y Diversidad, Lucila Puyol; la subsecretaria Anatilde Bugna; y la directora de Memoria, Verdad y Justicia, Valeria Silva, se reunieron con la jefa de la URI de Policía de la Provincia, Marcela Muñoz; la Sub Jefa de Unidad, Laura Ponce; la jefa de Ayudantia, Carla Camburzano; la Jefa Relaciones Policiales, Jaquelina Chiarlo; y el jefe de Divisiones Informaciones, comisario César Ramos.

**En la visita al lugar, las funcionarias constataron que parte de la documentación corresponde al período de la última dictadura**. Se dispone la clasificación, identificación y posterior traslado al Espacio de Memoria ex comisaría Cuarta para ser incorpora al Archivo Provincial de la Memoria.

La documentación hallada fue localizada en una habitación cerrada y húmeda y **fue localizada por personal de la División Informaciones de la Policía**.