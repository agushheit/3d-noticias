---
category: La Ciudad
date: 2021-03-29T06:00:40-03:00
thumbnail: https://assets.3dnoticias.com.ar/mondino.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'Mondino: "Es necesario que haya nuevas miradas para fortalecer el socialismo" '
title: 'Mondino: "Es necesario que haya nuevas miradas para fortalecer el socialismo" '
entradilla: 'La concejala santafesina encabeza junto al diputado nacional Enrique
  Estévez la lista "Socialismo en movimiento", que competirá en las elecciones internas
  del Partido Socialista. '

---
Con una composición representativa de toda la provincia, "Socialismo en movimiento" es la propuesta que encabezan el diputado nacional Enrique Estévez y la concejala santafesina Laura Mondino para las elecciones internas del Partido Socialista, que se llevarán a cabo el próximo 18 de abril.

Laura Mondino es politóloga, presidenta del interbloque del Frente Progresista Cívico y Social (FPCyS) y referente del Ejecutivo municipal en el Concejo de Santa Fe. Desde 2018 forma parte de la Junta Provincial del Partido Socialista (PS).

**Desafíos y expectativas**

**- ¿Cuáles son los principales desafíos que enfrenta hoy el PS?**

\- "Socialismo en movimiento" está compuesta por compañeros y compañeras de toda la provincia. Es una lista paritaria, estoy muy orgullosa de encabezar junto a Enrique Estévez porque sería la primera vez que la ciudad de Santa Fe ocupa un lugar en ese espacio de conducción partidaria. A su vez, a nivel nacional Mónica Fein es la primera candidata mujer a presidir el Partido, con un claro desafío de superar la trampa de la grieta y de convocar a distintos sectores sociales para el armado de una alternativa nacional progresista.

Entendemos que eso legitima un esquema político que apunta a ser representativo de la tradición socialista y a la vez abierto a nuevas voces y miradas vinculadas a los desafíos actuales. Es necesario que haya nuevas miradas para fortalecer el socialismo.

Creo que es importante reforzar la idea de un socialismo en territorio, conectado de manera cotidiana y permanente con las problemáticas de nuestros barrios y de cada localidad de nuestra provincia. Frente al panorama que nos deja la pandemia en lo social, las conducciones políticas debemos tener lucidez para poder construir alternativas bien cercanas al día a día de la gente.

Muchos compañeros y compañeras de todo el territorio venimos planteando la importancia de mantener la unidad partidaria. Más allá de los matices, una vez pasadas las elecciones seguiremos trabajando por la unidad del partido con las convicciones de siempre.

Venimos de doce años y tres grandes experiencias en la gestión provincial: Hermes Binner, Antonio Bonfatti y Miguel Lifschitz. Hoy gobernamos la ciudad de Santa Fe, la más importante de la provincia y del país que conduce nuestro espacio político. Aquí el intendente Emilio Jatón y el equipo que lo acompaña llevan adelante una gran tarea de reconstrucción del entramado social, con acciones en territorio que apuntan a rearmar un esquema de participación comunitaria que estaba completamente quebrantado.

**- ¿Cómo se llegó a la conformación de esta lista?**

\- Se llega básicamente con la convicción de que es necesario reafirmar aquellos consensos básicos que dan forma al proyecto político del socialismo. Durante las gestiones del FPCyS se evidenció un avance significativo en materia de derechos en todas las áreas, desde la salud hasta la educación y la cultura, pasando por los derechos de las mujeres, infancias y juventudes. Se abrieron puertas a las diversidades y se ponderó el valor de lo público como bandera.

Esta lista que presentamos sintetiza de alguna manera aquella voluntad de bregar por un socialismo de uñas cortas y manos limpias, con políticas públicas innovadoras y con un fuerte anclaje en lo territorial. Queremos recuperar ese abanico y a ese saber, propio de la experiencia, sumarle las miradas de personas que en todo el territorio trabajan permanentemente para actualizar esos principios en función de los desafíos que la realidad presenta. Hoy hay compañeros y compañeras al frente de comunas, presidiendo o integrando concejos municipales y en los distintos ámbitos institucionales de la vida política. Entendemos que hoy el PS está frente a una oportunidad histórica de crecimiento.

**- ¿Qué horizonte vislumbra para el socialismo en la provincia de Santa Fe?**

\- Creo que en el horizonte asoma la necesidad de recuperar la mejor tradición del socialismo. Soy parte de una generación que vivió en tiempo real el proceso político que encarnó Hermes Binner en la provincia de Santa Fe y que adoptó ese legado como un compromiso ético.

El PS tiene hoy la responsabilidad de convocar a las fuerzas del Frente Progresista para consolidar ese colectivo, trabajar sobre las coincidencias para transformar la realidad y convocar a sectores y movimientos sociales a sumarse a este proyecto.

Con mucha humildad, con construcciones colectivas y liderazgos abiertos, con la autocrítica, la escucha y las discusiones internas que sea necesario dar; es tiempo de orientar los pasos que conduzcan a nuestro espacio político a recuperar la provincia en el corto plazo. En este 2021, el Frente debe tener alternativas electorales que nos permitan, tanto a nivel nacional como en las instancias locales, pensar en propuestas progresistas de gobierno.