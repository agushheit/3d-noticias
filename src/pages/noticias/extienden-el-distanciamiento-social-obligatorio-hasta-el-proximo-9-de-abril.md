---
category: Agenda Ciudadana
date: 2021-03-13T06:00:14-03:00
thumbnail: https://assets.3dnoticias.com.ar/dispo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Extienden el distanciamiento social obligatorio hasta el próximo 9 de abril
title: Extienden el distanciamiento social obligatorio hasta el próximo 9 de abril
entradilla: Fue una resolución tomada en la reunión del Comité de Vacunación que encabezó
  el Presidente. No habrá nuevas restricciones, pero se desalentarán los viajes al
  exterior.

---
El Gobierno resolvió extender hasta el próximo 9 de abril la vigencia del Distanciamiento Social, Preventivo y Obligatorio (Dispo) dispuesto por la pandemia del coronavirus en la reunión del Comité de Vacunación que encabezó el presidente Alberto Fernández en la Casa de Gobierno.

La medida, que no incluirá nuevas restricciones de importancia, estará contendida en un decreto que el Poder Ejecutivo publicará en el Boletín Oficial, añadieron fuentes oficiales.

Por instrucciones del Presidente, el Gobierno ratificó que se "restringirá lo menos posible" en esta etapa para "sostener" los niveles de "actividad industrial, económica y social" y así "ganar tiempo" a la espera de la "llegada de más vacunas al país".

"Estamos en un punto de inflexión. Esta semana las jurisdicciones empezaron a acelerar" la campaña de vacunación, señalaron fuentes cercanas al Presidente, que confirmaron además que en los próximos días se dará a conocer la "normativa" en la que estarán precisados todos los "cuidados" recomendados para retrasar la llegada de una eventual segunda ola de contagios al país.

En la reunión del Comité se ratificó la estrategia oficial para evitar la propagación del virus a partir de una mayor toma de conciencia sobre los "riesgos y las medidas de prevención".

En este sentido, la ministra de Salud, Carla Vizzotti, designada como vocera de la reunión, remarcó que "Argentina trabaja en fortalecer los cuidados con el objetivo de retrasar el aumento de casos a medida que avanza la vacunación".

"Hay que alertar y desalentar a la población para que viaje a lugares donde haya circulación persistente de esa cepa por el riesgo individual y sanitario y trabajar muy fuerte sobre todo en control de la vuelta", sostuvo la funcionaria, quien enfatizó sobre los estrictos controles que se harán sobre los ciudadanos que regresen al país desde el exterior.

Según la ministra, quienes regresen al país deberán tener "el test negativo" y cumplir con el "aislamiento de siete días", sobre lo que se va a trabajar con mayores "controles", en sintonía con las jurisdicciones, a los fines de minimizar la posibilidad de que ingresen nuevas variantes del coronavirus.

**La provincia adhirió a la extensión**

Este viernes, el gobierno provincial adhirió al decreto nacional que promulga la extensión del estado de emergencia sanitaria en el país hasta el 31 de diciembre de 2021, en el marco de la pandemia de coronavirus. La medida que adoptó Nación y que adhirió provincia dispone puntos tales como el aislamiento preventivo en todos los casos en los que deba aplicarse.

La medida adoptada por el gobierno nacional, con el aval del presidente Alberto Fernández, fue adherida por la provincia de Santa Fe, mediante la firma del Gobernador Omar Perotti. El decreto regirá en la provincia y el país a partir del próximo viernes y hasta fin de año.