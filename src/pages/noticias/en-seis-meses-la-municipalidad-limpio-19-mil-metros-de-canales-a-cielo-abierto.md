---
category: La Ciudad
date: 2021-07-27T06:00:42-03:00
thumbnail: https://assets.3dnoticias.com.ar/CANALES.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: En seis meses, la Municipalidad limpió 19 mil metros de canales a cielo abierto
title: En seis meses, la Municipalidad limpió 19 mil metros de canales a cielo abierto
entradilla: Mediante el trabajo con camiones y retro palas, avanzan las tareas tendientes
  a sanear los canales para evitar anegamientos y mantener el sistema de drenaje de
  la ciudad en condiciones

---
Desde el comienzo del año, la Municipalidad realiza trabajos de limpieza en canales a cielo abierto ubicados en diferentes barrios de la ciudad. De este modo, hasta el 15 de julio, se concretaron estas tareas a lo largo de 18.879 metros. El objetivo es disminuir el riesgo hídrico y optimizar la red de drenaje urbano de la capital provincial. Las tareas incluyen el saneamiento en canales profundos, zanjas y cunetas de todos los puntos de la capital provincial.

El director de Gestión Operativa de la secretaría de Asuntos Hídricos y Gestión de Riesgo, Alberto Mitri, manifestó: “Esta tarea es constante y sistemática, se parte de una planificación y una vez que se hace todo el circuito se vuelve a empezar. Esto nos permite tener el sistema en condiciones para eventualidades climáticas o crecidas de ríos porque son estos canales los que tienen que evacuar el agua de la ciudad”.

El funcionario explicó: “En todo el ejido urbano hay desagües entubados que concluyen en los canales y son estos últimos los que conducen a los reservorios; en definitiva hay que mantener todo el sistema para que el drenaje de la ciudad esté en forma óptima para la temporada de lluvias que es a partir de noviembre”.

Antes de iniciar las labores, que se hacen mediante camiones y retro palas, se ejecutó un relevamiento para evaluar las zonas que requerían intervención inmediata. De este modo, los trabajos se concretaron siguiendo un plan preestablecido. Además, la limpieza se efectúa desde aguas abajo hacia aguas arriba, que es la forma correcta y, paralelamente, se avanza en la desobstrucción de canales entubados

Vale recordar que estas obras se enmarcan en el proyecto de Mantenimiento de la Red de Drenaje Urbano, perteneciente al programa de Gestión operativa para la protección contra inundaciones. La limpieza de cada uno de estos canales es de fundamental importancia para un drenaje eficiente de los excesos pluviales hacia cada una de las estaciones de bombeo.

**En detalle**

Desde la Secretaría de Asuntos Hídricos y Gestión de Riesgo de la Municipalidad se informó cuáles son las obras que se realizan por estos días, con maquinaria y personal propio. Estas se concentran en barrio San Martin, Europa y Boneo; en barrio Santa Rita, en calle Misiones entre Servando Bayo y pasaje Baigorrita; y en barrio Transporte, donde se concreta limpieza de cunetas en pasaje Lasaga, entre Risso y Padre Genesio, y se trabaja con un camión desobstructor para intervenir en los cruces vehiculares.

Por otra parte, y en cuanto a los canales -tarea que ejecuta la empresa adjudicataria Alamco SA-, se trabaja en el canal Teniendo Loza cerca de Blas Parera y luego se continuará por el Reinares que termina en el desagüe Gorriti; y finalmente, la desobstrucción de desagües pluviales seguirá en Belgrano y Pedro Centeno, y en J.J. Passo y Roque Saenz Peña. Ya se saneó el canal sobre Aristóbulo del Valle desde Alfonsina Storni hasta el límite con Monte Vera; también concluyeron las tareas sobre el canal Suipacha, además J. J. Paso hasta su descarga, y se hizo lo mismo en Tacca y Rodríguez Peña en barrio Centenario.