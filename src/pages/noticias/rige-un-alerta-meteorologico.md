---
category: La Ciudad
date: 2021-03-25T07:21:24-03:00
thumbnail: https://assets.3dnoticias.com.ar/lluvia.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad Santa Fe
resumen: Rige un alerta meteorológico
title: Rige un alerta meteorológico
entradilla: Según un informe del Servicio Meteorológico Nacional, alcanza a tres departamentos
  de la provincia, incluido La Capital.

---
La Municipalidad de Santa Fe informa que el Servicio Meteorológico Nacional (SMN) publicó una alerta temprana por lluvias y tormentas que afecta parcialmente los siguientes departamentos de la provincia de Santa Fe: La Capital, San Jerónimo y Garay. El mismo rige para este jueves 25 de marzo, desde la madrugada hasta horas de la noche, y parte del viernes.

El aviso meteorológico es por lluvias y tormentas de variada intensidad, algunas fuertes, que pueden estar acompañadas de abundante actividad eléctrica, ocasional caída de granizo y principalmente abundante caída de agua en cortos períodos de tiempo. Se esperan valores de precipitación acumulada entre 30 y 70 mm, pudiendo ser superados de forma puntual.

**Recomendaciones**

Ante el alerta, se recomienda a los vecinos no sacar a la vía pública objetos o residuos que puedan obstaculizar el normal escurrimiento del agua de lluvia. Además, se recuerda que, después de lluvia, realicen acciones de descacharrado de patios, vaciando o descartando recipientes donde se pudo acumular agua.

Se recuerda que las consultas o reclamos se pueden realizar al Sistema de Atención Ciudadana, llamando al 0800-777-5000.