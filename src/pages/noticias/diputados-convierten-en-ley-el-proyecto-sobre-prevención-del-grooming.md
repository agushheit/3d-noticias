---
layout: Noticia con imagen
author: "Fuente: Telam"
resumen: Prevención del Ciberacoso
category: Estado Real
title: "Diputados: convierten en ley el proyecto sobre prevención del grooming"
entradilla: La Cámara de Diputados aprobó este jueves por unanimidad, y
  convirtió en ley, el proyecto de creación de un Programa Nacional de
  Prevención y Concientización del Grooming o Ciberacoso contra Niñas, Niños y
  Adolescentes.
date: 2020-11-12T13:40:39.817Z
thumbnail: https://assets.3dnoticias.com.ar/grooming.jpg
---
El objetivo del programa es prevenir, sensibilizar y generar conciencia en la población sobre la problemática del grooming o ciberacoso, a través del uso responsable de las Tecnologías de la Información y la Comunicación (TIC) y de la capacitación de la comunidad.

El **grooming** está definido como "**la acción por la que una persona, por medio de comunicaciones electrónicas, telecomunicaciones o cualquier otra tecnología de transmisión de datos, contacte a una persona menor de edad, con el propósito de cometer cualquier delito contra la integridad sexual de la misma**".

Los objetivos del programa son: generar conciencia sobre el uso responsable de las Tecnologías de la Información y Comunicación; garantizar la protección de los derechos de las niñas, niños y adolescentes frente al grooming o ciberacoso; capacitar a la comunidad educativa a los fines de concientizar sobre la problemática del grooming o ciberacoso; diseñar y desarrollar campañas de difusión; y brindar información acerca de cómo denunciar este tipo de delitos.

Además, se dispone la inclusión, como pantalla de inicio de teléfonos celulares, teléfonos inteligentes, tablets, y otros dispositivos tecnológicos, la siguiente información:

* Peligrosidad de sobreexposición en las redes de niñas, niños y adolescentes.
* Información acerca de la existencia de delitos cibernéticos.
* Aconsejar el rechazo de los mensajes de tipo pornográfico.
* Advertir sobre la peligrosidad de publicar fotos propias o de amistades.
* Recomendar la utilización de perfiles privados en las redes sociales.
* Sugerir no aceptar en redes sociales a personas desconocidas.
* Hacer hincapié en el derecho a la privacidad de datos y de imágenes.
* Aconsejar el mantenimiento seguro del dispositivo electrónico y la utilización de programas para proteger el ordenador contra el software malintencionado.
* Brindar información respecto a cómo actuar ante un delito informático.
* Informar respecto a la importancia de conservar pruebas tales como conversaciones, mensajes, capturas de pantalla, en caso de haberse producido una situación de acoso.
* Facilitar información acerca de dónde se deben denunciar este tipo de delitos.

Durante el breve debate de esta madrugada, la diputada Alicia Aparicio (Frente de Todos) destacó que "la lucha contra el grooming avanza en nuestro Parlamento" y que "la capacitación es fundamental para que los adultos responsables puedan tomar conciencia de los peligros de internet e involucrarse para poder prevenir".

La, también oficialista, Laura Russo manifestó que “en este contexto de pandemia global se ha intensificado el uso de las redes sociales y de la conectividad en general".

Además, explicó que “el ciberacoso es la antesala de otros delitos como la trata de personas, la pornografía infantil, entre otros”.

Por su parte, la radical Roxana Reyes, presidenta de la Comisión de Familia, Niñez y Juventudes, dijo que "el plan prevé una capacitación a la comunidad educativa para concientizar sobre esta problemática, también se prevé diseñar campañas de difusión a través de los medios masivos de comunicación; y brindar información acerca de cómo denunciar este tipo de delitos en la Justicia”.

Su compañera de bloque, la bonaerense Karina Banfi, consideró que "el ciberacoso recurre a una serie de actitudes que toman los adultos para ganar confianza de un menor con fines sexuales. La creación de este programa nacional llega en un momento clave, las denuncias de ciberacoso y grooming en las redes aumentaron en un 58 % desde que comenzó la pandemia".