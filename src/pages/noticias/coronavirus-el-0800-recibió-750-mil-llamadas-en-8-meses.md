---
layout: Noticia con imagen
author: .
resumen: "Detección de Covid: el 0800"
category: Estado Real
title: "Coronavirus: El 0800 recibió 750 mil llamadas en 8 meses"
entradilla: "Desde marzo, el Gobierno provincial implementó esta línea
  telefónica para consultas de posibles pacientes con Covid-19, bloqueo de
  contactos y seguimiento de la evolución de los casos.  \t \t  \t"
date: 2020-11-13T18:06:52.129Z
thumbnail: https://assets.3dnoticias.com.ar/salud.jpg
---
El Ministerio de Salud detalló que desde el inicio de la pandemia, en el mes de marzo, la línea telefónica 0800-555-6549 recibió más de 750 mil llamados  y detectó más del 70% de los casos positivos de Covid-19 que se registraron en la provincia de Santa Fe. Actualmente, la línea telefónica registra unas 6000 llamadas diarias.

Al respecto, el secretario de Salud, Jorge Prieto, expresó que “la demanda ha superado cualquier expectativa. Comenzamos a funcionar un 13 de marzo con 10 operarios y 3 médicos voluntarios, a mediados de abril hasta mitad del mes de junio, se mantuvo constante, pero a fines de octubre las llamadas recibidas ascendieron a 640.000”.

“La cantidad de personal ha subido exponencialmente para dar respuesta a toda la población. El gobierno estuvo y está presente frente a toda necesidad que el ciudadano tenga oportunamente, para conocer, acompañar y contener frente a una pandemia que ha desconcertado al mundo y aún lo sigue haciendo”, continuó.

Prieto precisó que “hoy el 0800 cuenta con 180 operarios, 80 médicos que trabajan las 24 horas en horarios rotativos, más 100 practicantes del último año de medicina, esto nos permitió que se atendieran el 93% de las llamadas entrantes y sólo se registraron 7% de llamadas perdidas de toda la provincia" y destacó "que la atención es personal y es médica”.

“La idea es contener, seguir acompañando y continuar trabajando con una herramienta que ha dado una gran lucha frente a esta pandemia con una estrategia de gobierno tan importante como fue este 0800, número único y registro histórico dentro de la provincia”, concluyó.