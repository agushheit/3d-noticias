---
category: La Ciudad
date: 2021-06-30T08:34:26-03:00
thumbnail: https://assets.3dnoticias.com.ar/luces-led.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad de Santa Fe
resumen: " Plan de iluminación: la Municipalidad instala más de 3.500 nuevas luminarias
  este año"
title: " Plan de iluminación: la Municipalidad instala más de 3.500 nuevas luminarias
  este año"
entradilla: El programa, que pretende dotar a la capital de la provincia de una mejor
  red de iluminación, avanza en grandes avenidas y barrios de toda la ciudad. Además,
  se anunció que el plan continuará durante el año próximo.

---
En febrero pasado, la Municipalidad dio comienzo a una serie de licitaciones enmarcadas en el Plan de Iluminación que el intendente, Emilio Jatón, incluyera en el Presupuesto del año en curso. El objetivo del programa es recuperar el alumbrado existente en toda la ciudad, lo que contempla el mantenimiento de la red actual pero también implica dotar de nueva iluminación a zonas que actualmente poseen un servicio deficiente.

El secretario General del municipio, Mariano Granato, detalló que la iniciativa avanza en dos frentes: la actualización tecnológica, mediante el recambio de luces de sodio por led, y el nuevo sistema que incluye columnas y equipos con nueva tecnología. Según indicó, para el fin de este año se habrán colocado más de 3.500 nuevas luminarias en 66 barrios de la ciudad.

En ese marco se inscriben los trabajos que ya terminaron en los bulevares Pellegrini y Gálvez, y las calles 25 Mayo y Rivadavia. Además, en estos tramos se suman los espacios públicos ubicados en el entorno de estas arterias, tal es el caso de la plaza España, el parque Alberdi y el Palomar. En estas zonas, Granato recuerda que había “equipamiento que no funcionaba, por lo que se procedió al cambio completo de toda la luminaria”.

En lo que va del primer semestre de 2021, se concretaron licitaciones para renovar equipamiento en barrios como Santa Marta, Scarafía, Liceo Norte, Siete Jefes, Guadalupe Noreste y Central Guadalupe, entre otros sectores. Al respecto, detalló que en la mayoría de estos casos, se instalarán nuevas columnas con iluminación led.

En paralelo, se encuentra licitada y en proceso de adjudicación una obra para la colocación de 103 columnas y sus correspondientes artefactos en Yapeyú Oeste. Otras obras significativas se concretarán en Guadalupe Noreste y Siete Jefes, cuyas licitaciones también ya se concretaron: en el primer barrio se instalarán 405 columnas con equipos led, cableado y tableros, mientras que en el segundo serán 310 columnas. En tanto, las últimas licitaciones realizadas beneficiarán a los barrios Roma y Fomento 9 de Julio, donde se colocarán 170 luminarias en ambos barrios.

El secretario afirmó que los inconvenientes en la iluminación “fue lo primero que detectamos como dificultad central, después de un año de pandemia que ha limitado mucho las capacidades del Estado”. Por este motivo, el Ejecutivo se abocó a la diagramación de “un plan ambicioso de recuperación que está siendo ejecutado, para colocar luminarias nuevas con equipamiento de última tecnología que mejore la infraestructura de la ciudad”, explicó.

“Vamos a terminar el 2021 con más del 40% de la ciudad iluminada con tecnología led. Y el plan no termina este año, sino que continuará en 2022 con la intención de ir mejorando la calidad de la infraestructura y la iluminación, que hace a la calidad de vida de los vecinos y las vecinas de la capital provincial, algo que el intendente Emilio Jatón pide a los equipos de trabajo”, enunció.

**A buen ritmo**

En ese sentido, se mostró conforme con el ritmo de avance de los trabajos: “Era una demanda de la sociedad que estamos satisfaciendo en tiempo y en forma, después de un año que ha sido terrible para las arcas municipales”.

Granato mencionó que la ciudad, “en muchas de sus zonas, tiene una infraestructura muy vieja y deteriorada, y el plan de iluminación intenta mejorar esa infraestructura reemplazando los focos colgantes con columnas y la tradicional iluminación de halógeno por led, que da mejor rendimiento”.

En cuanto a los presupuestos que permiten encarar estas obras, el secretario general indicó que en algunos casos serán municipales, mientras que en otros serán provinciales. Y también se cuenta con fondos nacionales, a los que la Municipalidad accede luego de presentar los proyectos correspondientes, tal es el caso del plan “Argentina Hace”.