---
category: Agenda Ciudadana
date: 2021-05-22T06:00:23-03:00
thumbnail: https://assets.3dnoticias.com.ar/hynes.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Erica Hynes pide definiciones sobre la compra de vacunas
title: Erica Hynes pide definiciones sobre la compra de vacunas
entradilla: La diputada provincial Erica Hynes indicó que es importante saber si el
  gobierno tomó la decisión política de comprar vacunas contra el Covid 19.

---
La diputada del socialismo indicó que es importante saber si el gobierno tomó la decisión política de comprar vacunas contra el Covid 19 y si cuenta con equipos técnicos para realizarlo. Pidió transparencia y celeridad en el manejo del tema.

Hynes mostró su preocupación sobre el acceso a vacunas contra el Covid-19 de la provincia de Santa fe, luego de que el tema fuera debatido ampliamente en la última sesión ordinaria de la Cámara de Diputados y Diputadas y en medio de un segundo confinamiento obligatorio dictado por el gobierno nacional.

“Desde hace meses el acceso a las vacunas es un tema central en la agenda legislativa, con diferentes iniciativas presentadas en las que se insta a comprar dosis para Santa Fe y a garantizar una distribución equitativa. Ayer mismo aprobamos una resolución de la diputada Claudia Balagué para realizar una jornada de divulgación científica y concientización sobre la importancia de la declaración como bien público de las vacunas contra el Covid-19”, indicó. “En este contexto es importante saber si el gobierno provincial tomó la decisión política de comprar vacunas y si cuenta con equipos técnicos para emprender estas negociaciones internacionales que no son fáciles”, agregó Erica Hynes.

La diputada destacó que varios gobiernos provinciales iniciaron el camino para adquirir vacunas, pero no hay información oficial sobre el tema en Santa Fe. “Lo que aparece en los medios es que el gobierno duda si debe seguir esperando vacunas del plan nacional, que llegan con un flujo bastante lento, o si toma una actitud más proactiva y se pone a la cabeza de una misión para conseguir en el exterior”, indicó en declaraciones radiales.

**Una discusión sobre la desigualdad**

Erica Hynes dijo que el acceso de las vacunas es complejo aun para los estados nacionales que tienen enormes dificultades ya que se trata de un bien escaso. “Hay países que producen vacunas y tienen dosis que superan a la cantidad de su población, y hay otros como Haití que no han aplicado ni una sola dosis. La discusión de las vacunas es una discusión sobre desigualdad como tantas en esta pandemia”.

En Argentina, el tema se cruza a su vez con la discusión acerca de la transparencia del plan de vacunación. “En nuestra provincia tuvimos casos de discrecionalidad para dar las dosis y de poca transparencia de todo el proceso. No es solo una cuestión de cuántas vacunas conseguimos sino del plan de vacunas que debe ser un plan estratégico, bien claro y transparentado para dar tranquilidad a la población”.

“Se está pidiendo mucho esfuerzo a la gente, estamos todos en una situación de angustia y de duelo porque todos hemos perdido seres queridos y es una obligación muy importante de transparentar el qué y el cómo”, agregó.

El debate sobre el acceso a las vacunas en diputados se dio también en el senado provincial que ayer aprobó un proyecto que otorga facultades al gobierno provincial para que pueda negociar la compra de vacunas sin demoras, incluso en moneda extranjera y con cláusulas de confidencialidad en caso de ser requeridas.

“No es cuestión de instar o habilitar”, dijo Hynes, “hay que tomar la decisión de hacerlo si se quiere aumentar la velocidad con la que se está inoculando. Es una cuestión de tiempo, de ganar días a la enfermedad inmunizando a la mayor parte posible de la población”.

“En este momento crítico lo más importante es que la gente no se enferme y no se muera y en ese sentido apoyamos la gestión y la urgencia del momento. Nos interesa aportar y cooperar, pero no por eso vamos a dejar de criticar y marcar las cosas que no se están haciendo bien y dejar pasar la discusión por conseguir más vacunas es una de ellas”, concluyó.