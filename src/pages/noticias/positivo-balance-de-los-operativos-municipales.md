---
category: La Ciudad
date: 2022-01-03T06:00:51-03:00
thumbnail: https://assets.3dnoticias.com.ar/OPERATIVOS.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Positivo balance de los operativos municipales
title: Positivo balance de los operativos municipales
entradilla: El área de control informó las actuaciones llevadas adelante durante la
  noche y la madrugada del 1 de enero.

---
La Municipalidad concretó un operativo especial de barrido y limpieza en diferentes sectores de la ciudad. Como es habitual, estos trabajos tuvieron como objetivo recoger los residuos generados durante el festejo de Año Nuevo en las zonas más concurridas de la capital provincial. Tal como estaba previsto, las tareas estuvieron a cargo de cuadrillas municipales compuestas por tres camiones, dos barredoras mecánicas y 25 barrenderos.

De este modo, desde las primeras horas de la mañana se trabajó en el barrido manual y mecánico, así como también el levante de montones en las costaneras Este y Oeste; la Ruta 168, a la altura de los boliches; los bulevares Muttis, Pellegrini y Gálvez; la Recoleta y calle Rivadavia; la Peatonal San Martín, y las avenidas Galicia, Alem, Freyre, Aristóbulo del Valle, Facundo Zuviria, y López y Planes. En paralelo, un servicio especial de placeros comenzó a recorrer espacios verdes de la ciudad, a los fines de reforzar la limpieza.

El secretario de Ambiente y Cambio Climático del municipio, Franco Ponce de León, confirmó que las tareas se desarrollan a buen ritmo y que continuarán durante toda la jornada. Sobre el operativo desarrollado durante la madrugada, destacó el funcionamiento de los 10 contenedores para depósito de residuos que se ubicaron sobre la Costanera Oeste y Este.

“Las tareas de limpieza se realizaron sin inconvenientes, de la forma en que habíamos planeado, con la incorporación de los contenedores y placeros en algunos puntos más concurridos de la ciudad”, dijo. Del mismo modo, destacó el trabajo realizado en conjunto con las empresas Cliba y Urbafe.

Por otra parte, instó a vecinos y vecinas de la ciudad a acercarse mañana domingo a los Eco Puntos, que permanecerán abiertos en los horarios habituales, para depositar todos los residuos secos generados durante estos días, en las reuniones familiares. “A partir de mañana, pueden acercarse a los Eco Puntos de reciclado que están destinados a papel, cartón, telgopor, plásticos, metales como latas y vidrios”, insistió.

**Controles en calle**

Desde el área de Control y Convivencia Ciudadana se realizó un balance positivo de las actuaciones concretadas entre la noche del viernes y la madrugada de este sábado, especialmente, en cuanto a verificaciones vehiculares y controles de alcoholemia, expendio de bebidas y venta de pirotecnia.

Del operativo participaron más de 60 agentes a bordo de diez móviles, quienes recorrieron la ciudad prestando colaboración a la Policía, en espacios públicos. Igualmente, personal de Tránsito tuvo a su cargo los controles vehiculares en distintos puntos de la ciudad, siguiendo la planificación establecida.

Según se informó, se retuvieron nueve autos y una moto por alcoholemia positiva, mientras que otros diez motovehículos fueron retenidos por otras faltas.

La Dirección de Controles Especiales verificó el expendio de bebidas alcohólicas y ruidos molestos, el horario de cierre de boliches y eventos realizados en la ciudad. Esos controles en vía pública, arrojaron como resultado la confección de 14 actas y 1 cédula.

En lo que refiere a pirotecnia, vale recordar que antes de la celebración de Navidad comenzó a desarrollarse un operativo específico con el objetivo de dar cumplimiento a la Ordenanza N° 12.429 que declara a la ciudad capital como territorio libre de pirotecnia. Esta tarea se realizó en conjunto con el Ministerio de Seguridad de la Provincia, y contó con el apoyo de la Brigada de Explosivos.

El secretario de Control y Convivencia Ciudadana del municipio, Fernando Peverengo, se refirió a las actuaciones completadas y destacó la puesta en marcha del Sendero Seguro, encarado con personal de la Guardia de Seguridad Institucional (GSI). Cabe recordar que el mismo consistió en acompañar la llegada y salida de los jóvenes desde bulevar Gálvez hasta la zona de boliches, ubicada en la Ruta 168.

Según contó, luego de la experiencia de Navidad se decidió sostener el Sendero Seguro en las mismas condiciones que el 25 de diciembre, pero se agregó presencia de agentes para disminuir la velocidad de los vehículos que cruzaban por el Puente Oroño.

En términos generales, realizó un balance positivo de los operativos y agradeció la colaboración de todos los agentes que participaron de los mismos.