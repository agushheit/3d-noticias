---
category: La Ciudad
date: 2021-04-19T06:00:58-03:00
thumbnail: https://assets.3dnoticias.com.ar/BALLA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Piden revocar prisión domiciliara de Jorge Balla, represor condenado por
  delitos de lesa humanidad
title: Piden revocar prisión domiciliara de Jorge Balla, represor condenado por delitos
  de lesa humanidad
entradilla: El Foro contra la Impunidad y la Justicia se presentará el martes ante
  el Fiscal Federal Suárez Faisal, para solicitarle que el represor Jorge Balla, continúe
  cumpliendo su pena de prisión perpetua en una cárcel común.

---
El ex militar está cumpliendo su condena en su domicilio particular, donde según denuncia una familia vecina, los amenaza con torturarlos e incluso trepa la medianera que los divide. El Foro afirma también que Balla "hacía funcionar una peluquería en el lugar donde no podía ser visitado más que por una decena de personas".

"Consideramos ineludible que esa Fiscalía Federal no omita interponer recurso de apelación contra esa resolución que abunda en el tratamiento privilegiado e irregular del condenado", afirma un escrito del organismo. Balla cumple su condena en su casa, vecina a la de una joven pareja integrada por Juan y Daiana, que tienen un bebé de menos de un año. Esa familia sufrió por parte del condenado agresiones en junio y diciembre de 2020, las mismas incluyó el hecho de que Balla se trepara reiteradamente al tapial que separa las dos casas.

Debido a esto, desde el foro rechazan los argumentos que le permitieron al condenado gozar del derecho a la prisión domiciliaria. "No aceptaremos que el sargento Balla siga trepando al tapial", aseguraron y agregaron: "a comienzos de este año dimos a conocer las prácticas ilegales e intimidatorias de Jorge Alberto Balla, condenado a prisión perpetua por múltiples homicidios agravados como crímenes de lesa humanidad".

Desde el Foro puntualizaron que "aprovechando la ausencia de controles efectivos, el ex sargento del ejército, autor de la masacre alevosa de Ituzaingó y Las Heras durante la última dictadura, hacía funcionar una peluquería en el lugar donde no podía ser visitado más que por una decena de personas y, desde hacía meses, daba a conocer su voluntad de volver a matar a una joven pareja de vecinos quienes viven con su hija de menos de un año al lado de quien debería estar en una cárcel. Un modo elegido por el genocida Balla para exhibir poder y ejercer hostigamiento contra sus vecinos, Daiana y Juan, era treparse al tapial que separa los patios de sus viviendas y hacer saber que no sólo los observaba, sino que tampoco le era difícil concretar sus amenazas de matar al 'mugriento', al 'montonero', según el modo en que elige nombrar a Juan como candidato a sus disparos cobardes". denunciaron.

Debido a esta situación, el próximo martes, el Foro junto a los organismos de derechos humanos de Rafaela y la familia agredida por Balla, realizarán una presentación en la fiscalía federal, en la que pedirán que apelen la decisión, "además de resaltar todas las omisiones e irregularidades con que se armó la resolución, señalamos que Oscar Pellegrini fue el especialista en que también la fiscalía específicamente confió para examinar a Balla, constatando lo que veníamos denunciando: no hay razones para que el genocida Balla no cumpla su condena en establecimiento penitenciario. Esta medida resulta urgente tanto para que dejemos de padecer las provocadoras muestras de impunidad de Balla como para proteger la libertad y la salud de quienes no cometieron delitos: Daiana, Juan y su beba".

El pedido fue informado, además, ante la Secretaría de Derechos Humanos de la Nación y la Cámara de Diputados de la Provincia de Santa Fe, instituciones que ya se han expresado repudiando los privilegios de Balla. También fue anoticiada la Procuraduría de Crímenes contra la Humanidad de la Procuración General de la Nación.

**La causa**

La conocida como "la masacre de Ituzaingo y Las Heras" tuvo lugar el 19 de enero de 1977 y consistió en un operativo conjunto del Ejército y la Policía provincial, donde fueron asesinados Jorge Luis Piotti, Ileana Esther Gómez, Osvaldo Pascual Ziccardi y Carlos Mario Frigerio, y una vecina del departamento atacado, Elina Jagou de Carlen.

El 27 de agosto de 2019 el Tribunal Oral Federal de Santa Fe, condenó a nueve ex militares y policías por graves delitos de lesa humanidad cometidos durante la última dictadura militar y absolvió a tres ex policías que fueron enjuiciados por los mismos delitos.

Además de Jorge Balla, fueron condenados a prisión perpetua el ex coronel Ramón Abel Recio y el ex suboficial principal del Ejército Luis Alfredo Gómez, los tres como coautores del homicidio doblemente calificado en perjuicio de Osvaldo Pascual Ziccardi, Ileana Gómez, Carlos Frigerio y Jorge Luis Piotti. Además, condenado a la misma pena el ex policía de Santa Fe Oscar Cayetano Valdez, en su caso como partícipe necesario de homicidio doblemente calificado por las mismas cuatro víctimas. Luego, recibió 15 años de prisión el enfermero Raúl Giménez, autor de homicidio simple en perjuicio de Nora Meurzet y Antonio Martín Mendicute. También fueron condenados a 15 años los ex policías Rubén Ángel Vázquez y Luis Bellini como coautores del delito de homicidio simple en perjuicio de Luis Fadil, Alicia Ramírez y Mario Galuppo.