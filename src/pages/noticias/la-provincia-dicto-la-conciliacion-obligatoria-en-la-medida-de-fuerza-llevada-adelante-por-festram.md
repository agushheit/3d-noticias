---
category: Estado Real
date: 2021-08-21T06:15:02-03:00
thumbnail: https://assets.3dnoticias.com.ar/FESTRAM.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La provincia dictó la conciliación obligatoria en la medida de fuerza llevada
  adelante por Festram
title: La provincia dictó la conciliación obligatoria en la medida de fuerza llevada
  adelante por Festram
entradilla: La audiencia se sustanciará bajo modalidad virtual el próximo martes.

---
El Ministerio de Trabajo, Empleo y Seguridad Social tomó intervención en la situación que están llevando adelante los agentes municipales nucleados en la Federación de Sindicatos de Trabajadores Municipales de la Provincia de Santa Fe (FESTRAM) en todo el territorio de la provincia, dictando la conciliación obligatoria entre las partes.

La decisión tiene por fundamento la necesidad de retomar con normalidad los servicios municipales que fueron suspendidos durante el lapso de implementación de la medida de fuerza, los cuales han sido declarados esenciales en un contexto de pandemia mundial.

Asimismo, se aclara que los municipios y comunas afectados están cumpliendo con el acuerdo salarial actualmente vigente, hasta el mes de noviembre del presente año.

Por todo esto, el MTEySS ha convocado para una audiencia conciliatoria a las partes involucradas el día martes 24 de agosto a las 10 hs. La misma se llevará adelante mediante sistema de videoconferencia.