---
category: La Ciudad
date: 2021-01-05T09:56:51Z
thumbnail: https://assets.3dnoticias.com.ar/010521-Roselli.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Diario UNO'
resumen: 'Covid: «La situación de la ciudad de Santa Fe es terrible», dicen desde
  Salud'
title: 'Covid: «La situación de la ciudad de Santa Fe es terrible», dicen desde Salud'
entradilla: Analizan imponer multas de valor económico alto para desalentar las reuniones
  sociales y no descartan volver a medidas más restrictivas para evitar la circulación
  de personas.

---
El director de la Región de Salud Santa Fe, Rodolfo Rosselli, advirtió que la situación de la capital provincial con respecto a los contagios por Covid-19 es terrible. 

Lo dijo este lunes en diálogo con UNO Santa Fe, luego de declarar en conferencia de prensa que al Ministerio de Salud le preocupa más la ciudad de Santa Fe que otras localidades de la provincia, porque «estamos viendo que hay un número sostenidamente alto».

Al ser consultado por las acciones a nivel local que se están pensando, el funcionario provincial apuntó: «con el intendente Emilio Jatón hemos estado hablando y reunidos, él está también muy preocupado por esta situación. Tanto es así que se están tratando de desarticular, en lo posible, las reuniones sociales, fundamentalmente clandestinas, con multas de valor económico alto para desalentarlas. Esperamos ver que se tome real conciencia, sobre todo en la población joven, entre los 20 y 30 años, que es el sector de la sociedad que menos acatamiento a las medidas preventivas tiene».

Suponemos que en los próximos diez días vamos a ver un incremento sostenido y creciente en casos.

«La situación epidemiológica es altamente complicada, estamos muy preocupados. Especialmente por el comportamiento social, el bajo acatamiento a las medidas de protección que han ocurrido en estos días de las fiestas y estamos esperando que para los próximos diez días seguramente va a haber un aumento significativo en el número de casos», sostuvo.

Y agregó: «se evalúan todas las medidas de restricción posibles. Vamos a ver, es una decisión que se toma conjuntamente con Nación, pero sí se están evaluando. Se podrían llegar a considerar medidas de restricción nuevamente ante el aumento de casos. El mayor problema es gran número de personas sin elementos de protección personal, no uso de barbijos y muchas personas en reuniones sociales».

Ante la pregunta acerca de qué valores de contagiados diarios podrían representar una alerta para tomar medidas restrictivas de cara a una segunda ola, Rosselli respondió que «no se trata solo del número, sino que es la progresión. Se va viendo un aumento y número sostenido y creciente. Es una situación que se va a ir viendo, pero suponemos que en los próximos diez días vamos a ver un incremento sostenido y creciente en casos. Probablemente dentro de un mes vamos a estar hablando de una segunda ola, un incremento».

Al ser consultado sobre el estado de las camas generales y críticas en hospitales de la ciudad, Rosselli dijo: «hay un alto nivel de ocupación, superior al 80 por ciento. No solo motivado por enfermos de Covid, sino que también hay accidentes de tránsito, heridos de arma blanca, etc. Pero nos preocupa más en el marco de la situación de la problemática por el Covid-19. Es muy alto el porcentaje de ocupación».

La vacunación se reinició este lunes en las cinco regiones para trabajadores tanto de los efectores públicos como de los privados.

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.15em; font-weight: 700">Los efectos de la Sputnik</span>**

Este lunes llegaron 2.300 dosis más a la ciudad y se espera, «para mediados de enero, que haya en la provincia unas 400 mil dosis», dijo Rosselli. En ese total, va a haber partidas de la segunda dosis de la vacuna Sputnik V.

Se colocaron 405 vacunas hasta este lunes en la ciudad de Santa Fe. De ese total, hubo 30 personas que sufrieron efectos adversos, supuestamente asociados a la vacuna.

Sobre los efectos adversos, Rosselli describió: «en todos los casos, los efectos persistieron por 24 horas y se recuperaron. En esta ciudad, menos del 10 por ciento sufrieron efectos secundarios. No hay ausentismos de ninguna persona que se haya colocado esta vacuna. Hacemos seguimientos a cada uno. Fueron todos casos leves. Los efectos están caracterizados en general por dolores locales y enrojecimientos, en algunos casos fiebre, cefalea o cuadros gripales».

«La vigilancia se hace de dos maneras: activa y pasiva. A todas las personas se les entrega un número de teléfono para que notifiquen cualquier efecto y, en otros casos, se las llama por teléfono de manera diaria. Son las reacciones que se dan, como en cualquier otra vacuna. Incluso hay otras vacunas que tienen efectos secundarios mucho más importantes, como la cuádruple por ejemplo», concluyó el funcionario.