---
category: El Campo
date: 2022-12-06T07:49:25-03:00
thumbnail: https://assets.3dnoticias.com.ar/campo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Bolsa de Comercio de Rosario
resumen: La campaña gruesa 2022/23 está en peores condiciones que en el 2008/09
title: La campaña gruesa 2022/23 está en peores condiciones que en el 2008/09
entradilla: En el 2008 hubo grandes tormentas a fines de noviembre que no se han dado
  en este 2022. También hubo una recuperación de las lluvias a partir de fines de
  enero que es poco probable en el 2023

---
De acuerdo al último informe de la Guía Estratégica para el Agro, «en el 2008 hubo grandes tormentas a fines de noviembre que no se han dado en este 2022. También hubo una recuperación de las lluvias a partir de fines de enero que es poco probable en el 2023». ¿Cuándo vuelve el agua a la región pampeana?

¿Por qué se toma como referencia lo que pasó en el ciclo 2008/09?

Las peores dos campañas de las últimas dos décadas han sido la 2017/18 y la 2008/09. La primera empezó con abundante agua en los suelos, pero las lluvias desaparecieron desde mediados de diciembre hasta mediados de marzo. En cambio, en el ciclo 2008/09, la falta de agua precedía a la siembra: la sequía estuvo presente durante todo el ciclo de triguera. El trigo 2008/09 fue un gran fracaso productivo a nivel nacional tal como sucedió en este este año. Y en soja, lamentablemente, el ciclo 2008/09, con 18 M ha sembradas y expectativas de producción de 50 Mt, terminó con una cosecha de solo 31 Mt.

A mediados de marzo, GEA/BCR alertaba la posibilidad de una tercera “Niña” consecutiva. A medida que se iba profundizando el escenario de falta de agua en la región pampeana durante la campaña triguera, se insistía en que el productor tenía que prepararse para enfrentar el peor escenario de granos gruesos de los últimos 20 años.

Finales de noviembre 2008 vs 2022

En aquel entonces, el área más afectada era el este, en especial el sureste: Buenos Aires, Entre Ríos y el SE santafesino. A esta altura del año (2022), gran parte del norte y este de la región pampeana muestran los niveles más bajos de agua en el suelo considerando las estadísticas de los últimos 30 años. La situación es más grave que en el 2008 en Santa Fe, Entre Ríos y norte de Córdoba. Esto se refleja en la comparación del mapa de anomalía de las reservas hídricas al 27 de noviembre de 2008 con la actual (30 de noviembre). Lamentablemente, sin los eventos de lluvias que se dieron en diciembre del 2008, la tendencia es que la situación se agrave al 20 de diciembre de este año.

Región núcleo: las lluvias de finales de noviembre del 2008 cuadriplicaron a las del 2022

“Noviembre asfixiante por temperaturas históricas”, la cita es de un informe de la campaña 2008/09. Es una notable coincidencia con noviembre del 2022: las máximas de la semana pasada se mantuvieron en un promedio de 40ºC y 42ºC en la región núcleo. Pero lo que no sucedió esta vez son las lluvias que cerraron el mes en el 2008: una gran tormenta había dejado acumulados de 80 a 140 mm, muy lejos de los 10 a 40 mm de finales de noviembre 2022. Aparte, en la primera quincena de diciembre del 2008 se sucedieron lluvias que complementaron las tormentas de noviembre en la región central, mejorando aún más la situación de las reservas.

¿Se pueden repetir las lluvias claves que salvaron a la región central en enero del 2009?

En aquel ciclo hubo una situación que fue crítica para la producción argentina de soja. Citábamos en un informe de campaña 2008/09: “la recuperación de humedad se sostuvo a partir del 25 de enero (del 2009). Hubo lluvias de 60 a 140 mm que llegaron a Córdoba, Santa Fe, Entre Ríos, Chaco y a parte del norte de Buenos Aires. Esto fue muy importante para la recuperación”.

¿Qué posibilidades hay de que vuelva a suceder algo así? El consultor Elorriaga responde: “Lamentablemente, vemos que una recuperación sostenida del estado hídrico de los suelos podría darse a partir de febrero o marzo de la mano de condiciones globales más favorables para Argentina. No pude descartarse que suceda lo que todos estamos deseando: eventos extraordinarios que sean efectivos. Pero lejos de ser generalizados se darían de forma puntual, en determinados momentos y en algunas zonas que sean favorecidas”.

¿Cuándo puede volver el agua a la región pampeana?

“Los pronósticos para la primera semana del nuevo mes no muestran ningún cambio significativo que modifique el patrón que impone la persistencia de faltantes pluviales”, explica el Dr. Aiello. Elorriaga agrega que se esperan lluvias entre el 9 y el 10 de este mes. Si bien llegarían a la región central, se esperan acumulados muy modestos. “Las mejores posibilidades las tiene la provincia de Buenos Aires, como sucedió hasta ahora. De todas maneras se tratarían de lluvias modestas”, dice el consultor.