---
category: Agenda Ciudadana
date: 2021-11-26T06:16:59-03:00
thumbnail: https://assets.3dnoticias.com.ar/leyalquileres.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: El oficialismo busca avanzar en cambios a la ley de alquileres
title: El oficialismo busca avanzar en cambios a la ley de alquileres
entradilla: 'El Gobierno trabaja en una iniciativa destinada a modificar la norma,
  la cual "no trajo los resultados que se esperaban" en beneficios para inquilinos
  y dueños. '

---
El oficialismo buscará avanzar en los próximos días en la Cámara de Diputados con proyectos para dar incentivos a la construcción de viviendas y líneas de créditos hipotecarios, mientras trabaja en una iniciativa destinada a modificar la ley de alquileres que generó dificultades en su puesta en práctica.  
  
La decisión del oficialismo de avanzar en una nueva ley de alquileres fue adelantada por el presidente de la Cámara de Diputados, Sergio Massa, quien llamó a "discutir" en el Congreso una nueva ley de alquileres que brinde "certezas" al propietario y "garantías y tranquilidad" al inquilino.  
  
"El Congreso sancionó, no con mala fe sino buscando una solución a un problema, una ley que fracasó, y que pretendía asegurar a inquilinos y propietarios y terminó disminuyendo la oferta, retrayendo la posibilidad de desarrollar el negocio y generando incertidumbre y dificultades para el inquilino", recordó Massa, en un acto por el Día de la Construcción.  
  
Esa idea fue ratificada esta por la portavoz de la Presidencia, Gabriela Cerrutti, al confirmar que "se está previendo presentar una modificación" a la ley de alquileres y señaló que los precios en que se están concretando esas operaciones "son temas de preocupación para el Gobierno".  
  
En esa misma línea, el ministro de Obras Públicas, Jorge Ferraresi, consideró que la ley de alquileres -impulsada por Mauricio Macri- es "uno de los tantos fracasos del PRO, al igual que los créditos UVA" y sostuvo que "tener una ley nacional de alquileres no es una herramienta viable, sino que cada jurisdicción debería tener su regulación".  
  
"En una Argentina tan heterogénea tiene que haber leyes más particulares y leyes provinciales, una ley así no se puede aplicar igual en la zona más rica del país y en la más pobre", insistió el funcionario.  
  
La ley de alquileres será abordada en la Cámara.  
En ese marco, una de las propuestas que se podría analizar es una iniciativa que presentará apenas asuma, después del 10 de diciembre, Marcela Passo, electa diputada del Frente de Todos y alineada con Massa, quien trabaja en un proyecto para modificar la ley actual de alquileres sobre dos puntos.  
  
Uno de los aspectos está vinculado a la actualización de los alquileres, que hoy se hace una vez por año de acuerdo a un índice que combina 50% la variación de los salarios y 50% la inflación y otro relacionado con la duración de los contratos, que se extendió de dos a tres años de plazo.  
  
"Es un tema que el gobierno está abordando desde distintos aspectos", aseguró Passo en declaraciones a A24, al señalar que toda la norma "se está evaluando junto a los equipos técnicos".  
  
"Está claro que esta ley generó problemas, como el incremento de los valores de los alquileres y el retiro de propiedades del mercado por eso hay que analizar todos los aspectos", añadió.  
  
En tanto, la diputada del Frente de Todos, Mara Brawer, sostuvo en declaraciones a Télam que "el funcionamiento o no de una ley se ve en su implementación".  
  
"En ese sentido, muchas veces hay leyes que se piensan como eficaces pero que cuando son puestas en marcha no lo son tanto, y la Ley de Alquileres es un ejemplo", consideró.  
  
Desde la oposición, en tanto, legisladores de Juntos por el Cambio admitieron que la ley actual trajo inconvenientes en su puesta en práctica y coincidieron en la necesidad de impulsar en la cámara baja alternativas a la norma vigente.  
  
Incluso, en el marco de la campaña, María Eugenia Vidal, planteó la necesidad de derogar alguno de los artículos más cuestionados de la ley, en el marco de un evento organizado por el Colegio Único de Corredores Inmobiliarios de la Ciudad de Buenos Aires (Cucicba) .  
  
La ley de alquileres fue sancionada por el Senado en junio de 2020, pero había recibido el aval de la Cámara de Diputados en noviembre del año anterior, durante la administración de Mauricio Macri.  
  
**Incentivo a la construcción**

De todos modos, y mientras analiza las modificaciones a la ley de alquileres, la intención del oficialismo es avanzar en la próxima sesión con un proyecto para prorrogar el régimen de incentivo a la construcción, que ya tiene dictamen de comisión y una ley para establecer créditos hipotecarios para la adquisición de viviendas, atados al índice de variación salarial y no a la inflación.  
  
El primer proyecto establece una prórroga de 150 días al Régimen de Incentivo a la Construcción Federal Argentina y Acceso a la Vivienda con una serie de beneficios impositivos a quiénes inviertan en la construcción privada y, por otro, la posibilidad de exteriorización de activos, es decir un blanqueo de capitales que busca promover la construcción privada.  
  
Asimismo, el oficialismo buscará aprobar otro proyecto que el Poder Ejecutivo envió al Congreso en 2020 que contempla líneas de créditos hipotecarios para viviendas, atadas al índice de variación salarial y no a la inflación, como el UVA, similares a Unidad Hogar, que ya se está aplicando en los Procrear.  
  
El incentivo a la construcción a través de líneas crediticias será otro de los temas a tratar. Foto: Pepe Mateos  
Según el diputado nacional del Frente de Todos Marcelo Casaretto, integrante de la comisión de Presupuesto, se trata de "dos aspectos muy importantes que la Cámara de Diputados está en condiciones de tratar".