---
category: La Ciudad
date: 2022-01-26T18:15:19-03:00
thumbnail: https://assets.3dnoticias.com.ar/BASURA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Qué pasa que hay tanta basura sin ser retirada en distintos barrios de la
  ciudad
title: Qué pasa que hay tanta basura sin ser retirada en distintos barrios de la ciudad
entradilla: 'Desde hace semanas en varias zonas, entre ellas el centro de la ciudad,
  están como si no hubiera recolección de residuos. Las altas temperaturas y la lluvia
  no ayudan.

  '

---
Desde el comienzo del verano se puede ver en distintos barrios de la ciudad de Santa Fe, no solamente en el norte donde los reclamos de los vecinos son constantes, sino ahora en la zona más cerca del centro, la acumulación de basura frente a viviendas, edificios y también en espacios públicos.

Las quejas son diversas, tanto de propietarios de viviendas como de consorcios de edificios que por más que hagan los reclamos pertinentes sobre lo que sucede en su cuadra o en el barrio no encuentran respuesta o por lo menos no lo consiguen a lo largo del tiempo.

Asimismo se agrega el mal olor por la ola de calor y la lluvia, en un momento en que los desagües se puedan tapar por los residuos desparramados por la rotura de las bolsas por quienes buscan elementos para reciclar o comida.

También se han formado minibasurales en muchas veredas producto de la inconducta de los propios vecinos, principalmente si en la zona hay espacios públicos.

Hasta el momento no se conoce ningún tipo de inconveniente que tengan las dos empresas que realizan la tarea de recolección de los residuos en la ciudad, en la hipótesis de que pueda haber un problema gremial, falta de personal o algún tipo de impedimento. Mientras tanto la postal de la ciudad de Santa Fe con basura que se junta en muchos lugares se amplía y los vecinos siguen a la espera de una solución.