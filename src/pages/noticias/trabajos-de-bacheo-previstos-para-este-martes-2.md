---
category: La Ciudad
date: 2021-04-13T07:59:34-03:00
thumbnail: https://assets.3dnoticias.com.ar/bacheo-santoto.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad de Santa Fe
resumen: Trabajos de bacheo previstos para este martes
title: Trabajos de bacheo previstos para este martes
entradilla: La Municipalidad informa las calles por las que avanza el cronograma de
  tareas previsto en el Plan de Bacheo 2021. Puede haber cortes de circulación y desvíos
  de colectivos a medida que se desarrolla la obra.

---
En enero pasado, la Municipalidad puso en marcha un plan de bacheo para reparar más de 12.000 metros cuadrados de calles de la capital provincial. El programa se desarrolla por fases, priorizando las zonas de la ciudad que evidencian mayor circulación.

En la zona del micro y macrocentro, está previsto que los trabajos se concreten entre bulevar Pellegrini y J.J.Paso, de las siguientes calles:

* San Lorenzo, entre La Rioja y Catamarca
* San Lorenzo, entre Catamarca y Vera
* San Lorenzo, entre Hipólito Yrigoyen y Crespo
* San Lorenzo, entre Crespo y Salvador Caputto
* Hipólito Yrigoyen, entre avenida Freyre y San Lorenzo
* Salvador Caputto, entre San Lorenzo y Saavedra
* San Jerónimo, entre La Rioja y Tucumán

En tanto, en avenidas troncales, la obra continuará por:

* Aristóbulo del Valle y French, mano norte-sur
* Aristóbulo del Valle, entre avenida Gorriti y Los Paraísos, mano sur-norte

**Por el operativo de vacunación**

La Municipalidad comunica que, en el marco del operativo de vacunación que se concreta en la ciudad, habrá controles en el tránsito vehicular, de 8 a 18 horas, en las inmediaciones de:

* La Esquina Encendida, en la intersección de Facundo Zuviría y Estanislao Zeballos