---
category: Agenda Ciudadana
date: 2021-08-22T06:15:38-03:00
thumbnail: https://assets.3dnoticias.com.ar/APERTURAS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Punto por punto: las nuevas extensiones de horarios y habilitaciones que
  rigen desde este sábado'
title: 'Punto por punto: las nuevas extensiones de horarios y habilitaciones que rigen
  desde este sábado'
entradilla: Las reuniones familiares podrán ser de hasta 20 personas como máximo y
  las competencias deportivas podrán contar con público. Amplían horarios.

---
El decreto 1.512, firmado este viernes por el gobernador Omar Perotti amplía los horarios de funcionamiento de los comercios y de los locales gastronómicos, y suma actividades como las profesiones liberales, las inmobiliarias y las tareas administrativas. Además, las competencias deportivas podrán contar con la presencia 150 espectadores.

En todo el territorio provincial, desde la cero hora de este sábado y hasta el 3 de septiembre inclusive, se podrán desarrollar de manera presencial, con el cumplimiento de los protocolos, las profesiones liberales, incluidos mandatarios, corredores y martilleros debidamente matriculados e inscriptos.

Lo mismo se aplica para la actividad inmobiliaria y aseguradora y para las tareas administrativas de sindicatos, entidades gremiales, empresarias, cajas y colegios profesionales, entidades civiles y deportivas y obras sociales.

También podrán desarrollarse, de manera presencial, las actividades administrativas de las empresas industriales, de la construcción, comerciales o de servicios.

El decreto aclara que “en caso de resultar necesario para evitar la aglomeración de personas, se dispondrá la asignación de turnos para la atención al público”.

**Deporte con público**

A partir de este sábado, las competencias deportivas provinciales, zonales o locales de carácter profesional o amateur, incluidas las automovilísticas y motociclísticas, que se desarrollen en espacios al aire libre o cubiertos con suficiente ventilación, podrán permitir el ingreso de hasta 150 espectadores, sin contar a los protagonistas del espectáculo, árbitros y auxiliares que participen de la actividad.

**Reuniones familiares**

En cuanto a las reuniones sociales y familiares, el decreto autoriza que se realicen en domicilios particulares con un máximo de hasta 20 personas, siempre que el espacio cuente con suficiente ventilación. El mismo número se mantiene para las reuniones sociales en espacios públicos.

**Comercio**

Asimismo, el decreto autoriza una hora más para la actividad del comercio mayorista y minorista de venta de mercaderías, con atención al público en los locales. A partir de este sábado podrá extenderse todos los días hasta las 20, con excepción de los quioscos que podrán permanecer abiertos hasta las 22 para la atención al público residente en su cercanía.

El factor de ocupación de la superficie cerrada de los locales, destinada a la atención del público, no podrá exceder del 30 por ciento.

**Gastronómicos**

Los locales gastronómicos (bares, restaurantes, heladerías y otros autorizados, con concurrencia de comensales), podrán abrir a las 6 y hasta la 1 de la madrugada los viernes, sábados y vísperas de feriados. El resto de los días de la semana deberán cerrar a las 22.

Fuera de estos horarios establecidos solo podrán realizar actividad en las modalidades de reparto a domicilio y de retiro en el lugar.