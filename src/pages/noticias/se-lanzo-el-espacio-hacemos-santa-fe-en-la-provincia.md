---
category: Agenda Ciudadana
date: 2021-06-07T06:00:22-03:00
thumbnail: https://assets.3dnoticias.com.ar/HACEMOS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Se lanzó el espacio "Hacemos Santa Fe" en la provincia
title: Se lanzó el espacio "Hacemos Santa Fe" en la provincia
entradilla: Es el espacio que identifica el liderazgo del gobernador de Santa Fe Omar
  Perotti. Se presentaron vía teleconferencia más de 70 mensajes de adhesión.

---
En un multitudinario acto que contó con la participación vía teleconferencia de más de 1.500 dirigentes de los 365 municipios y comunas de Santa Fe, entre funcionarios provinciales, intendentes, presidentes comunales, senadores y diputados provinciales, concejales, cuadros profesionales y jóvenes técnicos, se lanzó el espacio "Hacemos Santa Fe" que se identifica con el liderazgo del gobernador Omar Perotti.

El acto fue coordinado por la ministra de Igualdad, Género y Diversidad, Celia Arena, junto al secretario de Turismo, Alejandro Grandinetti; y la concejala de la ciudad de Santa Fe Jorgelina Mudallel.

La ceremonia se dividió en cuatro bloques donde se presentaron más de 70 mensajes de adhesión de intendentes, presidentes comunales, legisladores y dirigentes de toda la provincia. Asimismo, se emitieron las palabras del gobernador Omar Perotti el pasado 30 de diciembre en la reunión que tuvo en Sauce Viejo con intendentes y presidentes comunales, en donde afirmó: "Van a tener en mí a alguien para construir desde la unidad una posibilidad que todos los santafesinos unidos demostremos claramente que podemos poner a Santa Fe de pie".

El cierre estuvo a cargo del senador nacional por Santa Fe Roberto Mirabella quien expresó: "Es una emoción muy fuerte ver a tantos militantes, tantos dirigentes, tantas personas que hemos compartido durante muchos años caminos de trabajo, en la gestión y muchos otros que se han incorporado en estos años, mucha gente joven que entendieron la política como una vocación de servicio".

Además, destacó: "Celebro también ver a muchos dirigentes de otros espacios políticos que vieron que esto no era excluyente, no era sectario y que compartimos una manera de ver las cosas, una manera de hacer las cosas, una manera de gobernar y por eso se han sumado muchos a participar".

Asimismo, Mirabella indicó: "Le estamos dando forma a algo que ya existía, en los hechos existía, pero que no tenía un nombre, una identidad. Y esto hoy nos da todo eso. Nos da un sentido de pertenencia". También manifestó: "Esta organización y este espacio tendrá proyectos electorales en las 365 localidades de la provincia". No obstante, fue enfático al señalar que "va a trascender la coyuntura electoral". "Este espacio queremos consolidarlo en la política santafesina como proyecto y modelo de gestión. Llevamos 18 meses de gobierno y 15 meses fueron de pandemia. Ha sido una coyuntura inesperada y lo asumimos con mucha valentía y mucha decisión. El gobernador Perotti ha sido un gran piloto de tormentas, pero también fue un liderazgo muy fuerte", remarcó.

Por otro lado, remarcó: "Nosotros estamos convencidos en lo que hacemos". En ese sentido manifestó: "No necesitamos de acuerdos con el delito para gobernar o que desde Buenos Aires o de algún lugar nos digan lo que tenemos que hacer o nos quieran marcar la cancha. Como tampoco necesitamos diferenciarnos como sugieren algunos medios. No vean fantasmas donde no los hay. Integramos el Frente de Todos y somos parte fundacional. Es la unidad en la diversidad que construyó la provincia de Santa Fe y supimos construir nosotros en estos seis años lo que nos permitió hacer un proyecto para gobernar Santa Fe y hacer un proyecto para gobernar la Argentina. Es esta unidad detrás del liderazgo de Omar Perotti y detrás del liderazgo de Alberto Fernández y de Cristina Fernández de Kirchner las que nos van a consolidar en el gobierno y nos va a llevar a la victoria".

En ese marco, sostuvo: "Lo que nosotros necesitamos es menos división y más unión. Menos odio y más amor. Es sentir orgullo de ser santafesino. Cuando los santafesinos se amigan con los santafesinos, Santa Fe se hace gigante".

**Diversidad territorial**

Durante el acto, la ministra de Igualdad, Género y Diversidad, Celia Arena, reseñó: "Nos encontramos hoy acá un montón de dirigentes, hombres y mujeres, del peronismo y también de otros sectores que se suman porque entendemos que nos une el liderazgo de Omar Perotti en esta cuestión tan clara de entender esta forma de hacer política a través del hacer".

"Ver la diversidad territorial define también esta forma de entender y ver la política que todas y todos compartimos", continuó Arena quien además consideró: "Este espacio Hacemos Santa Fe surge claramente con el espíritu de consolidar una forma de hacer política que reconoce el liderazgo de Omar por la impronta que él le da a la forma de gestionar, una forma inclusiva, una forma colectiva, territorial, que amplía derechos, que crea consensos y que busca el crecimiento de nuestra provincia y el bienestar de todos los santafesinos y santafesinas".

Luego, el secretario de Turismo, Alejandro Grandinetti, planteó: "Se trata de un encuentro de aquellos dirigentes políticos que nos sentimos comprometidos, primero en la pasión de transformar la realidad y luego en la necesidad de encontrar un lugar de debate y de encuentro que es lo que estamos iniciando orgánicamente en el día de hoy".

En tanto, el senador provincial por el departamento La Capital, Marcos Castelló, argumentó: "Este espacio pretende estar cerca de la gente, este espacio nos reúne para humanizar la política, algo que venimos hablando con Omar desde hace muchísimo tiempo. Hoy es una realidad", celebró.

A su turno, el senador provincial por el departamento Caseros, Eduardo Rosconi, significó: "Este encuentro de compañeros de todo el ámbito provincial para expresar nuestra decisión de aportar al nacimiento de este nuevo espacio que se referencia en el liderazgo de Omar Perotti porque consideramos que la gestión de nuestro gobernador y todo su equipo de gobierno merecen nuestro apoyo y reconocimiento en donde hay un denominador común que es el hacer y destacamos la mirada federalista y territorial de nuestro gobernador en su relación directa con todos los municipios y comunas de la provincia".

Por su parte, la intendenta de Cañada de Gómez, Stella Maris Clérici, envió un mensaje a la juventud y concluyó: "Los jóvenes a través de su entusiasmo, de su pasión se hacen presente diariamente en nuestra militancia y en la gestión. Especialmente tratamos de trabajar con ellos, de enseñar, pero de aprender muchísimo y de ser el desarrollo de la provincia".

También se escucharon las palabras del intendente de Reconquista, Enrique Vallejos, quien señaló que "somos personas de servicio" y en ese sentido reafirmó: "Creemos en lo que hacemos, creemos en este espacio que se va conformando y que se viene consolidando hace muchos años, creemos que tenemos mucho para dar y confiamos en la conducción de Omar Perotti".

Además, el secretario de Integración y Fortalecimiento Institucional, José Luis Freyre, señaló: "La gestión que lleva adelante el gobernador Perotti en un contexto muy complicado ha ampliado la atención sanitaria, garantizado el acceso a la educación, obra pública en cada rincón de la provincia, combate decidido a la inseguridad, apoyo y respaldo a la producción. Por todas estas cosas, todos juntos, unidos, damos pleno respaldo a nuestro gobernador y a esta gestión", afirmó.

La concejala por la ciudad de Santa Fe Jorgelina Mudallel subrayó: "Este es el nacimiento de nuestro espacio y nos hace sentir parte de un proceso que si bien tiene una larga trayectoria en el territorio santafesino de alguna manera hoy estamos dando nacimiento a un espacio de construcción para los que sienten en la política la oportunidad de transformar la realidad, que es lo que nos une, que es lo que nos identifica. Hacemos Santa Fe nos convoca a pensar la política a partir de la acción, del hacer, de solucionar los problemas de nuestra gente. Y eso es lo que nos une y lo que nos acerca a nuestro compañero Omar Perotti", sumó.

Entre los más de 1.500 participantes del acto, estuvieron y enviaron sus opiniones y adhesiones los intendentes Stella Maris Clérici (Cañada de Gómez), Luis Castellano (Rafaela), Enrique Vallejos (Reconquista), Jorge Berti (Villa Constitución), Roly Santacroce (Funes), Elvio Cotterli (Laguna Paiva), Nizar Esper (Arroyo Seco), Horacio Compagnucci (Las Parejas), Ana María Meiners (Esperanza), Fernando Almada (El Trébol), Rubén Cuello (Calchaquí), Daniel Cinalli (Capitán Bermúdez), Mariano Cominelli (Fray Luis Beltrán), Carlos de Grandis (Puerto General San Martín), Enrique Mualem (Tostado), José Luis Sánchez (Villa Trinidad), Adrián Maglia (Granadero Baigorria) y Mario Fissore (Gálvez).

Asimismo, formaron parte los presidentes comunales Julián Vignati (Arteaga), Marcela Del Puerto (General Gelly), Nancy Ávalos (Villa Guillermina), Luciano Bertossi (Helvecia), Alejandro Ruggeri (Bigand), Walter Carenzo (Piñero), Gonzalo Goyechea (María Teresa), Stella Maris Zanazzi (Colonia Teresa), Bruno Iommi (Labordeboy), Diego Vargas (Pilar), Valeria Díaz (Gregoria Pérez de Denis), Luciano Lemaire (Gobernador Crespo), José Abraham (Pueblo Andino), Rodrigo Acevedo (Fortín Olmos), Humberto Viozzi (Tortugas), Adrián Tagliari (Llambi Campbell), Juan Rucci (Godeken), Juan Carlos Imsteyf (Nuevo Torino), Carlos Clausen (Casas), Luis Raffo (Bouquet), Daniel Siliano (Acebal), María Laura Mozzi (Villa Mugueta), Adrián Biyovich (Luis Palacios), Horacio Córdoba Ríos (Coronel Arnold), Jorge Massón (Ibarlucea), Daniel Pistilli (Colonia Belgrano), José López (Pavón) y Rodolfo Stangoni (Fighiera).

El acto fue seguido en tiempo real por más de 3.500 personas de todos los rincones de la provincia a través de las redes del espacio y del sitio web; www.hacemossantafe.com, en lo que fue el encuentro político virtual más grande que se recuerde en la provincia.