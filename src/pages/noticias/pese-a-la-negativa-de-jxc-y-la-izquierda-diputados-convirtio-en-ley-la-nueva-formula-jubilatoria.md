---
category: Agenda Ciudadana
date: 2020-12-30T12:14:23Z
thumbnail: https://assets.3dnoticias.com.ar/3012-jubilados.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: NCN'
resumen: Pese a la negativa de JxC y la Izquierda, Diputados convirtió en Ley la nueva
  fórmula jubilatoria
title: Pese a la negativa de JxC y la Izquierda, Diputados convirtió en Ley la nueva
  fórmula jubilatoria
entradilla: La Cámara de Diputados convirtió en ley el proyecto de movilidad jubilatoria,
  en una votación en la que el oficialismo consiguió 132 votos a favor para imponerse
  a los 119 rechazos.

---
Con la nueva fórmula, los haberes se ajustarán en forma trimestral con una fórmula que combina en un 50% la recaudación de la ANSES y en otro 50% la variación salarial.

Al defender el proyecto del Poder Ejecutivo, que cuenta con sanción del Senado, el diputado del Frente de Todos, Marcelo Casaretto, presidente de la comisión de Previsión y Seguridad Social, afirmó que «el objetivo del Gobierno es que el país crezca y que los beneficios de ese crecimiento llegue a los trabajadores y a los jubilados».

Como miembro informante del oficialismo, el entrerriano recordó que la iniciativa se debatió en una comisión bicameral mixta que él le tocó presidir y que arribó a un dictamen luego de 17 reuniones con especialistas, sumado a los tres encuentros del plenario de comisiones.

«Es proyecto que más discusión tuvo en la Argentina en este año parlamentario desde el punto de vista de la cantidad de reuniones», resaltó. Luego detalló que el esquema de movilidad jubilatoria tiene una «incidencia directa» sobre 7 millones de jubilados y pensionados, y 17 millones de personas si se considera también a los beneficiarios de políticas sociales de ANSES.

A su turno, el presidente de la Comisión de Presupuesto, Carlos Heller, ponderó el índice impulsado por el oficialismo, al sostener que «las paritarias se van a cerrar por arriba de la inflación porque la economía va a crecer». «Dennos tiempo», para que las políticas del gobierno nacional den resultados «pese a las dificultades», pidió.

Por el dictamen de minoría, desde la oposición, el diputado de Juntos por el Cambio, Alejandro Cacace, cuestionó la decisión del presidente Alberto Fernández, de suspender la movilidad aplicada durante el gobierno de Cambiemos y sostuvo que «el concepto de este proyecto es el ajuste: eso es lo que han venido a hacer».

El legislador puntano criticó el hecho de que el proyecto oficialista proponga incluir la recaudación como variable, y al respecto indicó que «prácticamente en ninguna parte del mundo se utiliza para ajustar los haberes de los jubilados».

«Proponen una recaudación que ha estado a la baja este año, pero que además si los salarios crecen más que la recaudación la ponen como límite, pero si la recaudación cae ahí los hacen a los jubilados socios en las pérdidas de la AFIP como lo han hecho durante este 2020», alertó.

También, la diputada nacional Paula Oliveto Lago, de la Coalición Cívica, se preguntó: «¿Con qué cara estamos defendiendo esto?». Y disparó: «en mi barrio esto se llama ajuste», a la vez que afirmó que «a los jubilados hace años que los viene jorobando la clase política». «Deberíamos estar tristes, no festejando, porque estamos incumpliendo lo que les prometimos a nuestros abuelos», completó.

Asimismo, el diputado del Frente de Izquierda Nicolás del Caño, autor de otro de los dictámenes de minoría, consideró: «Estamos asistiendo a un ajuste: acá no hay grieta». «Todos los gobiernos les meten la mano a los adultos mayores como lo hemos visto durante décadas», dijo.

A su turno, el diputado de Unidad Federal para el Desarrollo, José Luis Ramón, anticipó su respaldo a la nueva fórmula de movilidad jubilatoria que debate la cámara baja, al sostener que ese esquema impulsado por el oficialismo les da «previsibilidad a los jubilados para el futuro».

«Vamos a acompañar este proyecto y creemos que, aún no siendo la mejor fórmula, es una fórmula que mira en positivo, que mira una economía que se puede recuperar en nuestro país», aseveró Ramón, al fundamentar su respaldo al proyecto del Ejecutivo.

Para el legislador mendocino, «hay que proteger los verdaderos intereses económicos que son los del pueblo y no de los grandes empresarios que no paran de hacer negocio tras negocio».

Por su parte, el diputado de Consenso Federal, Jorge Sarghini, cuestionó la fórmula de movilidad jubilatoria que debate la cámara baja y consideró que el sistema es «insostenible porque hay un gran porcentaje de trabajadores en negro».

Sarghini dijo que para 2023 «los argentinos después de 3 períodos de Gobierno vamos a ser un 15% más pobres, entre ellos los jubilados que cobran la mínima».

La diputada nacional del Frente de Todos, Fernanda Vallejos destacó el nuevo cálculo de movilidad jubilatoria, al considerar que «se trata de la restauración de una fórmula que, durante su vigencia, permitió una recuperación del 26% de los haberes previsionales, contra una pérdida del 20 por ciento».

De esta forma, la legisladora ponderó la fórmula previsional que funcionó entre 2008 y 2017, en detrimento del coeficiente en 2018 y 2019 por la gestión de Cambiemos. «Venimos a cumplir con un contrato electoral con nuestro pueblo. Un compromiso con un modelo económico que privilegie a las mayorías, que recupere en términos reales los salarios, que vuelva a poner en una relación armónica los suelos, las jubilaciones, las tarifas de los servicios públicos, los precios sensibles de la economía, con el crecimiento de la demanda, la actividad y el empleo», sostuvo.

Al momento de los cierres, el jefe de la bancada de Juntos por el Cambio, Mario Negri, consideró que «la fórmula que quieren votar ignorando la inflación es tener una miopía sobre el grado de la crisis económica de la Argentina. Pedíamos una base mínima de inflación para no llenarnos de incertidumbre».

El jefe de bloque del Frente de Todos, Máximo Kirchner, finalizó el debate con un encendido discurso que recordó la votación de 2017. «El Jefe de Gabinete del macrismo, Marcos Peña, durante el proceso electoral, le aseguró a todos y a todas que no iba a haber una reforma previsional en la Argentina. Meses después, nos encontrábamos con la instalación de esta fórmula que hoy venimos a cambiar», dijo.

«Lo que estamos haciendo nosotros, es cumplir con lo que dijimos en la campaña electoral, que creíamos que esta fórmula era la mejor para que jubilados y jubiladas paulatinamente pudieran recuperar los ingresos que perdieron con el cambio de fórmula que hizo Mauricio Macri, y no solo por el cambio de fórmula, sino con la política macro», concluyó.