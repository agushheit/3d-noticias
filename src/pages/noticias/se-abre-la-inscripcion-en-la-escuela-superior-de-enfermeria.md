---
category: Agenda Ciudadana
date: 2022-12-08T10:37:29-03:00
thumbnail: https://assets.3dnoticias.com.ar/2022-12-08NID_276841O_1.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: Se abre la inscripción en la Escuela Superior de Enfermería
title: Se abre la inscripción en la Escuela Superior de Enfermería
entradilla: A partir de este lunes 12 y hasta el 16 de diciembre se puede realizar
  el trámite online. La carrera es una formación técnico profesional de 3 años de
  duración.

---
Se trata de una carrera presencial de tres años de duración que la Escuela Superior de Enfermería imparte en sus sedes de Rosario, Venado Tuerto, Cañada De Gómez, Rafaela, Santa Fe, San Jorge, Reconquista y Vera.

Al respecto, subsecretario de Planificación y Fortalecimiento Institucional del Ministerio de Salud, Leandro Constantini, indicó a los interesados que “es muy importante completar bien los datos personales, contacto telefónico y un correo electrónico de uso frecuente”.

Asimismo, destacó que “la modalidad de ingreso definitiva se realizará a través de un examen on line único y nivelador, previsto para la última semana de febrero de 2023, organizado y supervisado por la Subdirección Provincial de Gestión del Conocimiento del Ministerio de Salud”.

“Luego de esta preinscripción quienes se hayan registrado recibirán la confirmación por correo electrónico y el material de lectura correspondiente, como asimismo los requisitos e instructivos para el acceso al examen a realizarse a través del aula virtual de Gestión del Conocimiento”, indicó Constantini.

**Fundamentos**

Finalmente, el funcionario explicó que “la formación técnico profesional es una modalidad que responde a la vinculación entre la educación y el trabajo, con una propuesta didáctica integral e integradora de conocimientos, habilidades, destrezas y actitudes como también valores. Los ministerios de Salud y Educación de Santa Fe, aprobaron por resolución conjunta (MS Nº 0093-ME Nº 2487/2017) el nuevo diseño curricular para la formación en enfermería en las instituciones de educación superior no universitaria”.

“Este programa propone otorgar conocimientos y capacidades que habilitan al futuro enfermero/a a intervenir en el proceso salud-enfermedad-cuidado, que requiere de un pensamiento crítico para dar respuestas a las problemáticas del sujeto, familia y comunidad en el marco de la estrategia de Atención Primaria, garantizando el derecho a la salud”, concluyó