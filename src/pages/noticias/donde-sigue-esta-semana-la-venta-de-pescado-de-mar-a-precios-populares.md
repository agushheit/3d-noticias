---
category: La Ciudad
date: 2021-07-21T06:00:35-03:00
thumbnail: https://assets.3dnoticias.com.ar/pescado.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Dónde sigue esta semana la venta de pescado de mar a precios populares
title: Dónde sigue esta semana la venta de pescado de mar a precios populares
entradilla: 'Los productos podrán adquirirse este miércoles 21 en la feria de Las
  4 Vías en Mariano Comas 2700, y el sábado 24, en la Feria Alimentaria de Beruti,
  en intersección con Blas Parera. '

---
La Municipalidad de Santa Fe informa cómo continúa el cronograma de venta de pescado de mar a precios populares en la capital provincial. En ese sentido, para esta semana está prevista la continuidad de la propuesta en dos espacios:

– Miércoles 21 de julio, de 8 a 13 horas, en la Feria de las 4 Vías (Mariano Comas 2700)

– Sábado 24 de julio, de 8 a 13 horas, en la Feria Alimentaria de Beruti (Beruti y Blas Parera)

La lista de precios indica que se ofrece merluza a $ 480 el kilo, medallones a $ 350 las 10 unidades, corvina a $ 330, tubo de calamar a $ 900, pejerrey despinado a $ 650, cazuelas a $ 900 el medio kilo, lomito de atún a $ 600 y pollo de mar a $ 550. Cabe mencionar que se puede abonar con todos los medios de pago, incluida la Tarjeta Alimentar y que las ventas son hasta agotar el stock disponible para cada día.

Esta iniciativa se inscribe en el marco del programa municipal Mercado Santafesino y es impulsada a partir de un acuerdo alcanzado entre la Municipalidad y la Dirección Nacional de Políticas Integradoras del Ministerio de Desarrollo Social de la Nación, con el objetivo de acercar productos de calidad a precios accesibles a más vecinos y vecinas de la ciudad.

**Mercado santafesino**

La secretaria de Integración y Economía Social del municipio, Ayelén Dutruel, estuvo presente en la Plaza Pueyrredón, donde el pasado sábado se realizó la venta de pescado de mar a precios populares. En la ocasión, contextualizó que, desde el municipio, a través del programa Mercado local santafesino “venimos impulsando distintas líneas de trabajo en el marco de la economía social y popular, para fortalecer a este sector en la ciudad”.

Para el caso puntual de la venta de pescado de mar, la funcionaria precisó que “la idea es articular con los distintos trabajadores y productores de la región, trayendo el pescado de mar desde otras provincias, para fortalecer las propias ferias de la ciudad”. Al respecto, agregó que con el convenio realizado con Nación se promueve actuar “de manera articulada entre todos los niveles del Estado”.

“Además, fortalecemos todas las líneas que tienen que ver con el encuentro de los trabajadores, con la discusión sobre la economía social y popular, y cómo queremos que sea esa economía en nuestra ciudad. Por eso organizamos estos espacios de feria, que es lo que nos ha priorizado el intendente Emilio Jatón en esta gestión”, remarcó Dutruel.

Emilio Scotta, asistente ejecutivo de Mercados y Ferias del municipio, planteó que esta iniciativa busca “completar la oferta alimentaria que venimos fomentando y promocionando en las ferias de la ciudad”. La propuesta se centra en ofrecer pescado de mar “para no generar una competencia desleal con los pescadores de río de la ciudad y alrededores”, indicó el funcionario y aseguró que “llevar precios justos y alimentos sanos a las familias santafesinas es fundamental”.