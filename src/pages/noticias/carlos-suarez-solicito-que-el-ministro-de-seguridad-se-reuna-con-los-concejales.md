---
category: Agenda Ciudadana
date: 2021-02-04T03:40:19-03:00
thumbnail: https://assets.3dnoticias.com.ar/sain.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Carlos Suarez
resumen: Carlos Suárez solicitó que el ministro de Seguridad se reúna con los concejales
title: Carlos Suárez solicitó que el ministro de Seguridad se reúna con los concejales
entradilla: "“Somos quienes recibimos las demandas de los vecinos y por eso venimos
  solicitando que se reúna el Consejo de Seguridad. pedimos un encuentro con Marcelo
  Saín”, afirmó el edil de UCR-Juntos por el Cambio. "

---
El concejal de UCR-Juntos por el Cambio Carlos Suárez, presentó un proyecto de decreto, que también lleva la firma de Luciana Ceresola y Sebastián Mastropaolo, para que el presidente del Concejo Municipal convoque al Ministro de Seguridad de la provincia, Marcelo Saín, a una reunión con los ediles que integran el cuerpo. “La inseguridad es el principal flagelo de los vecinos y por eso, venimos solicitando que se reúna el Consejo de Seguridad. Claramente, no hay intenciones de convocar a este espacio, entonces pedimos que el ministro Saín se reúna con los concejales que somos quienes recibimos las demandas e inquietudes de la gente por este tema”, afirmó el edil.   
  
En la misma línea, Carlos Suárez señaló que este encuentro servirá además para despejar algunas dudas y contar con información acerca del equipamiento que el Ministerio de Seguridad va a destinar al departamento La Capital, proveniente de la ampliación de las erogaciones del presupuesto. “Es fundamental contar con el detalle de cómo se planifica equipar a la Unidad Regional Uno para combatir la inseguridad”, afirmó el concejal de UCR-Juntos por el Cambio.   
  
**Vecinalistas por la seguridad**  
  
Este martes por la tarde se llevó a cabo un encuentro de la “Red de Vecinales por la Seguridad”, con concejales e integrantes de las comisiones de Derechos y Garantías y de Seguridad Pública de la Cámara de Diputados de la provincia. “Esperamos que este trabajo articulado de los legisladores provinciales y locales pueda tener resultados, porque en definitiva es lo que al vecino le interesa”, afirmó el concejal de UCR-Juntos por el Cambio.  
  
Carlos Suárez recordó además que la Red de Vecinales viene trabajando en este tema desde hace tiempo y agregó: “nosotros acompañamos su tarea y hemos intensificado su trabajo con la presentación de todas las herramientas legislativas que podemos tener en el Cuerpo a los fines de tratar de paliar la situación”.  
  
De todas formas, el edil consideró que la principal responsabilidad de la inseguridad es del Ministerio de Seguridad, y por eso insistió en que el ministro “se debe reunir con los concejales y contarnos cuáles son las acciones que está llevando adelante para paliar la terrible situación que vive la ciudad”.

![](https://ci4.googleusercontent.com/proxy/_Gom5A0WLhMfzH2jO8Ra8PilKPTegBUVptk_1-T_i2dnSKtvRg5ryZmzEOzRIG97mFyHoZdZW9PgqhvT-aLQj8I5-M_aHXEXrNUagHIR_vYiPiwUEZ3O73INVWyaDNNknl0yr3YCJZOcwFyWvUz9QDueBmtcQAs=s0-d-e1-ft#https://mcusercontent.com/0bdb4673ba2649cdee2fbdd5b/images/c5af9212-136d-48be-91f3-d84a34b3822a.jpeg)