---
category: Agenda Ciudadana
date: 2020-12-22T10:10:29Z
thumbnail: https://assets.3dnoticias.com.ar/2212inflacion.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: La inflación de noviembre en la provincia fue del 3,6%
title: La inflación de noviembre en la provincia fue del 3,6%
entradilla: Índice de Precios al Consumidor (IPC) de Santa Fe, que elabora el gobierno
  de la provincia, se incrementó 3,6% en noviembre respecto del mes anterior.

---
Así lo informó el Instituto Provincial de Estadística y Censos (Ipec), que elabora el IPC sobre la base de nueve capítulos de una canasta que registró los principales aumentos en los ítems de Esparcimiento (6,2%) e Indumentaria (4,8%); en tanto Educación (1,5%) fue el ítem que menos aumentó.

***

![](https://assets.3dnoticias.com.ar/2212cuadro.webp)

***

![](https://assets.3dnoticias.com.ar/2212_cuadro_1.webp)

***

En el acumulado de los últimos 12 meses, el mayor aumento fue el de los precios de Esparcimiento (58,2%), seguido por Indumentaria (53%).

<br/>

[<span style="font-family: arial, helvetica; font-size446694: 16px; font-weight: 700; color: #ffffff; background: #446694; margin: 35px; overflow: hidden; padding: 10px;">INFORME INDEC DE NOVIEMBRE  </span>](https://assets.3dnoticias.com.ar/Indice-de-Precios-de-Noviembre.pdf "Ver informe")

<br/>