---
category: Agenda Ciudadana
date: 2021-05-26T08:15:47-03:00
thumbnail: https://assets.3dnoticias.com.ar/vacunas-geriatricos.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Diario Uno
resumen: Santa Fe comienza el esquema para completar la vacunación contra el Covid
  en geriátricos
title: Santa Fe comienza el esquema para completar la vacunación contra el Covid en
  geriátricos
entradilla: Desde este miércoles se realizará el operativo para aplicar las segundas
  dosis en residencias de personas mayores. Se hará con 15.600 vacunas de AstraZeneca
  que arribaron en el lunes

---
El Ministerio de Salud continúa con el operativo de vacunación en todo el territorio provincial. En este sentido y ante la llegada de nuevas dosis de AstraZeneca (mismo componente que Covishield) se completarán los esquemas de inmunización en los geriátricos. “Es un hecho histórico, estamos protegiendo a nuestros abuelos y abuelas”, afirmó la ministra de Salud, Sonia Martorano.

Mañana, 30 móviles en toda la provincia recorrerán los más de 300 geriátricos para vacunar a residentes y trabajadores de las instituciones. Ante esto, la titular de la cartera sanitaria destacó: “El objetivo de la vacunación es disminuir las internaciones y la mortalidad, por eso, la vacunación en residencias de personas mayores es fundamental, por la alta contagiosidad que tienen estos ámbitos”.

“Vamos a abarcar todos los geriátricos de la provincia como hicimos con la primera dosis, tenemos un gran equipo de profesionales que están llevando adelante el operativo de vacunación en más de 300 vacunatorios de una manera excepcional”, continuó Martorano.

Por su parte, la ministra destacó que el operativo para geriátricos durará entre 5 y 6 días y que permitirá abarcar la totalidad de las residencias. El año pasado se llevó adelante una auditoría en cada institución para diagramar protocolos de trabajo en caso de contagios y tener precisiones sobre la cantidad de personas de cara al operativo de vacunación.

Dentro de la población objetivo priorizada, compuesta por 1.200.000 personas, se inició la vacunación con adultos mayores, personal de salud, educativo y de seguridad, junto con personas de 18 a 59 años con comorbilidades. En este sentido, en febrero, se desplegó el operativo en geriátricos que duró una semana y permitió aplicar las primeras dosis. A partir del día de mañana concluirán los esquemas desde mañana.