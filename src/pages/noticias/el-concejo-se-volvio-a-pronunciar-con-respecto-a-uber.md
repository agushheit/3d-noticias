---
category: La Ciudad
date: 2021-03-26T07:23:48-03:00
thumbnail: https://assets.3dnoticias.com.ar/uber.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Concejo Municipal Santa Fe
resumen: El Concejo se volvió a pronunciar con respecto a UBER
title: El Concejo se volvió a pronunciar con respecto a UBER
entradilla: El Cuerpo aprobó un proyecto que solicita al área correspondiente del
  municipio que advierta e informe a los medios de comunicación sobre la ilegalidad
  de la actividad promocionada por UBER en la ciudad de Santa Fe.

---
Fue en la sesión de este jueves que el Cuerpo legislativo local se expidió con respecto a la empresa UBER y su ilegalidad a la hora de transitar en la ciudad de Santa Fe. Al respecto, se resolvió que desde la Subsecretaría de Transporte se informe a la comunidad que la empresa UBER no se encuentra encuadrada en la normativa local vigente de sistemas de transportes públicos.

Al respecto, el concejal Carlos Suárez “un nuevo capítulo de este tema que el Concejo ya por el mes de octubre sancionó por unanimidad una posición sobre que los únicos subsistemas de transportes en la ciudad habilitados son el de taxis y el de remisses. Todos los otros transportes no están permitidos. Y ahora vemos cómo estas empresas multinacionales, sin solicitar los permisos del caso, desembarcan por la fuerza tratando de mostrar los beneficios de la utilización de la aplicación. En este caso, por medio de la publicidad. Creo que tenemos que ratificar el marco normativo que existe hoy en nuestra ciudad y si hay que discutir esto se deberá hacer en los ámbitos y con los parámetros legales necesarios para tratar la norma”.