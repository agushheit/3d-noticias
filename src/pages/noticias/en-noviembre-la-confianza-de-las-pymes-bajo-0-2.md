---
category: Estado Real
date: 2022-12-03T08:33:44-03:00
thumbnail: https://assets.3dnoticias.com.ar/pymes-dosjpg.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Came
resumen: En noviembre, la confianza de las pymes bajó 0,2%
title: En noviembre, la confianza de las pymes bajó 0,2%
entradilla: El dato surge el Índice de Confianza Pyme (ICPyme) que elabora la Confederación
  Argentina de la Mediana Empresa (CAME).

---
La confianza de las pequeñas y medianas empresas se contrajo un 0,2% mensual en noviembre, cortando una racha de tres meses consecutivos en alza. La caída se explica exclusivamente por el sector industrial, cuya confianza bajó 3,9%. En cambio, en los comercios, subió 2%. En la industria hay preocupación por la menor cantidad de consultas y pedidos de presupuestos, la suba de costos y una compleja dinámica de ventas que se viene desacelerando mes a mes, e impactó fuerte en este sector. Por el lado de los comercios, el fin de semana largo y el comienzo del Mundial de fútbol durante este mes alentaron la suba.

El dato surge el Índice de Confianza Pyme (ICPyme) que elabora la Confederación Argentina de la Mediana Empresa (CAME) para captar el ánimo del empresario en base a tres variables: la situación presente de su empresa, sus expectativas de evolución futura y sus intenciones de inversión.

Dicho informe muestra también que las pymes trabajaron en noviembre con niveles de confianza de 60,8 puntos, teniendo como referencia una escala de 0 a 100 (en la que 0 es confianza nula y 100 confianza plena).

![](https://assets.3dnoticias.com.ar/red-pime-came.webp)

**Consideraciones del ICPyme en base a sus componentes:**

* La evaluación de la situación presente mejoró 1,3% en la industria y 8,7% en el comercio. La cercanía de las distintas fiestas de fin de año alimentó este componente tanto en comercios minoristas como en industriales pymes. El fin de semana largo y el comienzo del Mundial de fútbol durante este mes fueron algunas de las razones por las cuales la percepción de comercios de todo el país registró esta fuerte suba.
* El principal componente que impulsó la desmejora en la confianza pyme de noviembre fueron las expectativas futuras de las industrias, que bajaron 11,0%. Si bien también en el comercio mostraron un desempeño negativo, lo hicieron en menor medida (-2,7% mensual). El mayor problema del sector industrial es el abastecimiento de insumos que dificulta la producción actual y obliga a buscar sustitutos más caros y/o de menor calidad que afectan costos de producción y, en consecuencia, a las ventas.
* La intenciones de inversión en la industria, más pesimistas sobre el futuro, bajaron 1,2% en el mes. En el comercio, en cambio, subieron levemente (+0,8%). Los niveles son muy similares entre ambos sectores (50,7 puntos en la industria vs 51,2% en el comercio, siempre sobre una escala del 1 al 100).

![](https://assets.3dnoticias.com.ar/red-prime-came-2.webp)

![](https://assets.3dnoticias.com.ar/red-pime-came-3.webp)