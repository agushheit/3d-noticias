---
category: La Ciudad
date: 2021-09-23T06:00:46-03:00
thumbnail: https://assets.3dnoticias.com.ar/DISIDENCIAS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Se fortalecerá el trabajo en la Dirección de Mujeres y Disidencias
title: Se fortalecerá el trabajo en la Dirección de Mujeres y Disidencias
entradilla: Emilio Jatón firmó un convenio con el gobierno provincial para destinar
  $ 2 millones a consolidar el sistema integral y fortalecer las políticas de cuidados.

---
El intendente Emilio Jatón firmó este miércoles un convenio con el Ministerio de Igualdad, Género y Diversidad de la provincia para el fortalecimiento de la Dirección de Mujeres y Disidencias de la Municipalidad. Además, en la ocasión se rubricaron acuerdos con algunas localidades del departamento La Capital para poner en marcha el Programa Puntos Violetas, el cual ya funciona en Santa Fe capital.

A comienzos de este año, la provincia anunció este programa que implica la recepción de recursos técnicos y económicos para trabajar políticas con perspectiva de género, adaptadas a cada territorio. Este miércoles le tocó al departamento La Capital, y en el caso concreto de Santa Fe, se destinarán $ 2 millones para seguir trabajando en la temática.

El acto se realizó en el Centro Cultural de la ciudad de Sauce Viejo estuvo encabezado por la ministra de Género de la provincia, Celia Arena, y participaron presidentes comunales e intendentes de las localidades, además de Santa Fe, de Arroyo Aguiar, Arroyo Leyes, Cabal, Campo Andino, Candioti, Emilia, Laguna Paiva, Monte Vera, Recreo, San José del Rincón y Santo Tomé.

La titular de la Dirección de Mujeres y Disidencias de la Municipalidad, Soledad Artigas, dio más detalles sobre el convenio: “Es un acuerdo que se enmarca dentro del sistema de protección integral de mujeres y disidencias víctimas de violencia de género. Esto nos permite sobrellevar el trabajo que el municipio hace cotidianamente”.

Siguiendo esta línea, Artigas agregó: “Es importante firmar acuerdos con la provincia porque desde el gobierno local lo que se hace es acompañar a las mujeres y disidencias que sufren violencia de género y es clave articular con el segundo nivel que es la provincia. Y además, ayudan a fortalecer los programas que desde el municipio existen”.

La funcionaria confirmó que serán 2 millones de pesos lo que recibirá Santa Fe y que “se destinarán al fortalecimiento del sistema integral y por eso en la ciudad capital se utilizarán para las políticas de cuidado porque con la pandemia se vio como las mujeres y las disidencias tomaron protagonismo en el cuidado de niños, ancianos y en la intención es poder visibilizar ese trabajo que muchas veces genera desigualdad”.

**Punto violeta**

Vale aclarar que Santa Fe implementó en el verano del 2020 el primer Punto Violeta en la ciudad y poco a poco se fueron replicando en distintos espacios. En el acto se firmaron convenios con comunas y ciudades que aún no forman parte de este programa. “Siempre es bueno fortalecer los servicios locales que hacen mucho esfuerzo y son el contacto directo con las víctimas, por eso es importante consolidar todo ese sistema que ya existe”, agregó Artigas.

Antes de finalizar, la directora de Mujeres y Disidencias, dijo: “La violencia de género no la vamos a poder erradicar si trabaja el municipio solo, sino en red. Es importante hacerlo con el área metropolitana porque la gente circula por las localidades y es necesario fortalecer la red para que no recaiga el trabajo en un solo municipio; pero a su vez con muchos organismos públicos y de la justicia para poder prevenir y erradicar la violencia”.

**Importante**

Si sos víctima de violencia de género o conocés a alguien que la esté sufriendo, podés recibir acompañamiento y asesoramiento en la Municipalidad de Santa Fe. Acercate a la Dirección de Mujeres y Disidencias en el ex Predio Ferial Municipal (Las Heras 2883) o comunicate a los números 4571525 y 4571666 (de lunes a viernes de 8 a 20 horas); también podés llamar las 24 horas, todos los días del año, al 0800-777-5000.

Recordá que en caso de una emergencia tenés que comunicarte al 911 y que también está habilitada la línea gratuita nacional 144 (las 24 horas, todos los días del año).