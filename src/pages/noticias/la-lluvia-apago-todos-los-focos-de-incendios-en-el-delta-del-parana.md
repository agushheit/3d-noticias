---
category: La Ciudad
date: 2021-09-03T06:00:15-03:00
thumbnail: https://assets.3dnoticias.com.ar/INCENDIOS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: La lluvia apagó todos los focos de incendios en el Delta del Paraná
title: La lluvia apagó todos los focos de incendios en el Delta del Paraná
entradilla: En lo que va de 2021 “ya se incendiaron 700 mil hectáreas”.

---
El Gobierno de Santa Fe anunció hoy que la lluvia “apagó todas las quemas” en islas del río Paraná, mientras que la Multisectorial Humedales dijo que entre el año pasado y lo que va de 2021 “ya se incendiaron 700 mil hectáreas”.

Tanto el secretario de Protección Civil de Santa Fe, Roberto Rioja, como la organización ambientalista el Paraná No se Toca, indicaron a Télam que “la abundante lluvia" de las últimas horas “apagó todos los focos” de incendios en las islas del Delta.

“Si bien ya teníamos controlada la situación en la región media (Santa Fe-Santo Tomé-Paraná), las lluvias terminaron de apagar las quemas, y lo mismo ocurrió en el sur provincial a la altura de San Nicolás”, donde se desarrollaban grandes incendios, agregó Rioja.

En tanto, la Multisectorial Humedales calificó de "calamitosa" la situación de los incendios frente a San Nicolás, donde indicó que, antes de las lluvias, "se organizó un operativo de emergencia, por la aproximación del fuego a viviendas de isleños".

Añadió que ante “la falta de respuestas oficiales”, se armó “un operativo informal” en la desembocadura del Arroyo del Ceibo con el río Pavón (Entre Ríos), frente a San Nicolás, del que participaron la Multisectorial, MoRedeHu, Punta de Flecha, Cona y La Matriz Consciente.

La Multisectorial informó que en el lugar vecinos y brigadistas civiles "trabajaron arduamente con las llamas que se aproximaban a las viviendas", de los pobladores.

Detalló además que se realizaron "cortafuegos" y que se utilizaron motobombas "para humedecer los alrededores de una de las ocho casas humildes", habitadas por "una población de quince personas estables y otras tantas itinerantes".

Los ecologistas señalaron que desde hace una semana arden terrenos, "parte de los cuales son propiedad del diputado provincial bonaerense Santiago Passaglia, hermano del intendente de San Nicolás, Manuel Passaglia".

"Se estima que 10.000 hectáreas desaparecieron a manos del fuego. Es un cálculo aproximado porque los registros oficiales no se realizan", subrayó la Multisectorial.

Añadió que, según el Museo Regional de Ciencias Naturales Antonio Scasso, de San Nicolás "los focos de incendio en lo que va de 2021 ya suman casi 10.000".

Y que entre el año pasado y lo que va del 2021 "se han quemado más de 700.000 hectáreas de humedal en el Delta del Paraná", lo que representa casi "un 30 por ciento de su territorio", precisó la organización ambientalista.

Por último, responsabilizaron a las autoridades provinciales y nacionales "por permitir que el fuego avance produciendo un ecocidio irrecuperable de flora y fauna" y por "poner en riesgo vidas humanas de vecinos habitantes de esas tierras".