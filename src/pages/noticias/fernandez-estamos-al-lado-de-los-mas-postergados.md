---
category: Agenda Ciudadana
date: 2021-08-18T06:00:03-03:00
thumbnail: https://assets.3dnoticias.com.ar/CRISALBER.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: 'Fernández: "Estamos al lado de los más postergados"'
title: 'Fernández: "Estamos al lado de los más postergados"'
entradilla: Alberto Fernández remarcó que en el Frente de Todos existe unidad en reivindicar
  "lo que se hizo, aún con miradas diferentes, siempre en favor de los que menos tienen,
  de los postergados y desposeídos".

---
El presidente Alberto Fernández subrayó que su Gobierno está "junto a los más postergados" y aseguró que esa es una vocación política compartida entre la dirigencia del Frente de Todos (FdT), durante un acto que contó con la asistencia de los principales referentes de la alianza gobernante, entre ellos la vicepresidenta Cristina Fernández de Kirchner.

El acto expuso la unidad del frente, no sólo a partir del discurso del Presidente, sino también en los mensajes expresados por la vicepresidenta, por el gobernador bonaerense, Axel Kicillof; y por el presidente de la Cámara de Diputados, Sergio Massa.

Esos mensajes contrastaron la impronta del Gobierno, que busca "transformar la realidad con equidad", de la exhibida por la gestión de Cambiemos, que dejó al país con un "endeudamiento de 45 mil millones de dólares" con el FMI.

"Estamos al lado de los más postergados", afirmó el Presidente y advirtió que, de lo contrario, "no hay manera de equilibrar la balanza, en la que algunos ganan mucho y muchos pierden mucho".

Fernández encabezó en el barrio Isla Maciel, en la localidad bonaerense de Dock Sud, partido de Avellaneda, la entrega de la vivienda número 20.000 de su gestión, en el marco del programa Casa Propia, que prevé para fin de año tener en ejecución algo más de 100 mil hogares y la entrega de 30.000 unidades en todo el país.

Allí, junto al ministro de Desarrollo Territorial y Hábitat, Jorge Ferraresi; la vicegobernadora Verónica Magario; el presidente del bloque del Frente de Todos (FdT) de la cámara baja, Máximo Kirchner; y los precandidatos a diputados nacionales Victoria Tolosa Paz y Daniel Gollán, Fernández pidió a los argentinos que analicen qué tipo de medidas tomó su Gobierno.

"Vayan y miren y si encuentran alguna medida de las que tomamos que haya sido en perjuicio del pueblo, vengan y díganmelo", enfatizó el jefe de Estado y advirtió que la gestión anterior, liderada por Mauricio Macri, dejó "11 mil casas sin entregar", pero ya construidas y terminadas, en diciembre del 2015.

"¿Qué hizo que no se entreguen esas casas?", se preguntó el Presidente, "sabiendo que miles de familias que necesiten un techo" y, al ensayar una respuesta, expresó: "Fue una causa miserable; no querían decir que Cristina les dejó una casa" a cada beneficiario.

"Me llena de orgullo darle una casa a quienes lo necesiten, si fue Cristina, otro o yo, lo que más me alegra es que tengan casas, no quien la construyó", completó Fernández.

Con el Presidente en primer término, todos los referentes del FdT que hablaron en el acto pusieron el acento en las políticas públicas desarrolladas desde diciembre del 2019 y las compararon con las acciones de la administración macrista, pero la Vicepresidenta sumó su convicción de que a los Gobiernos populares se les impone una vara ética más alta desde los medios de comunicación para juzgarlos.

"Necesitamos imperiosamente otra forma de vincularnos, por eso Alberto te pido que no te enojes ni te pongas nervioso porque cuando uno es Presidente los errores, las fallas, las equivocaciones se magnifican en el caso de los Gobiernos populares y se exacerban para irritar, indignar, mientas se ha ocultado descarada y ostensiblemente la entrega de un país y el endeudamiento sin límites", reflexionó.

Cristina valoró y diferenció a quienes "militan, organizan, bajan al territorio, meten los pies en el barro" porque, dijo, "son de la política que salva la política y que no necesitan, como sí los arribistas o los oportunistas, de la operación de prensa del momento para contestar la política".

Fernández de Kirchner se refirió a los cuestionamientos que el jefe de Estado recibió en los últimos días por su aparición en una foto de la cena de cumpleaños de la primera dama, Fabiola Yañez, en julio del 2020, durante los primeros meses de la pandemia de coronavirus.

"El poder no se cuestiona a sí mismo, el poder siempre cuestiona al pueblo en sus errores", planteó.

Con ese enfoque, el Presidente aseguró que su Gobierno "representa los intereses" de quienes "no la pasan bien, tienen carencias y buscan un futuro mejor" y dijo estar "muy orgulloso" de los dos bonaerenses que encabezan la lista de candidatos a diputados nacionales del FdT, en referencia a Tolosa Paz y Gollan.

"Estuvieron acá, vivieron acá", apuntó Fernández y aseguró que son "auténticamente bonaerenses", con un guiño irónico hacia la exgobernadora macrista María Eugenia Vidal, quien ahora compite en territorio porteño.

Fernández remarcó que en el frente ninguno tiene "vergüenza" de pertenecer a ese espacio y que todos, "aún con miradas diferentes", reivindican lo realizado "siempre en favor de los que menos tienen, de los postergados y desposeídos".

"Veo candidatos de la provincia de Buenos Aires que comienzan su campaña diciendo 'yo no estuve en el gobierno de Mauricio Macri'. Impactante", afirmó el mandatario, en referencia al precandidato de Juntos, Facundo Manes, y a Diego Santilli, "que pasó de la Ciudad a hacer un curso acelerado para ver de qué se trata la provincia" de Buenos Aires.

Más temprano, en su discurso, Kicillof también puso de relieve que "cuando se vota, no solo se discute teoría, modelos o programas políticos, sino cómo transformarle la realidad, cambiarle la vida sin mezquindades y sin oportunismo pensando en quienes más lo necesitan y en sus derechos".

De igual manera, Massa pidió respaldo para el FdT y llamó a "votar hacia adelante", priorizando el "desarrollo" y "no la deuda".

"Les pido que votemos hacia adelante, no volvamos para atrás, porque hoy lo que está en juego es si priorizamos el desarrollo o si priorizamos la deuda", como hizo el Gobierno de Macri, dijo Massa.

Con el acto fue lanzado además el plan Hábitat Integral Isla Maciel, que generará 790 puestos de trabajo y que, en el marco del citado programa Casa Propia, desarrollará más de 200 viviendas entre las que están en ejecución, las que se iniciarán y las que estuvieron paralizadas entre 2016 y 2019.

Mediante el Programa Nacional de Mejoramiento Barrial, se financiarán allí obras de infraestructura de servicios, la construcción de un jardín maternal municipal, un centro cultural, la plaza y museo Maciel, y la remodelación y ampliación de la sede del programa Envión Avellaneda.