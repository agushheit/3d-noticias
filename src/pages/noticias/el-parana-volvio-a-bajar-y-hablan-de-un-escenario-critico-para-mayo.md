---
category: Agenda Ciudadana
date: 2021-04-27T06:58:11-03:00
thumbnail: https://assets.3dnoticias.com.ar/rio-parana.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Diario Uno
resumen: El Paraná volvió a bajar y hablan de un escenario crítico para mayo
title: El Paraná volvió a bajar y hablan de un escenario crítico para mayo
entradilla: En 11 días, el Puerto de Santa Fe registró una bajante de un metro que
  se estabilizaría la semana que viene. Afirman que podrían repetirse los niveles
  de 2020

---
La bajante en los ríos de la costa santafesina se hizo presente nuevamente, luego de una leve recuperación en febrero a causa de lluvias muy intensas en la región, aunque fueron escasas. Según los registros hidrométricos del Puerto de Santa Fe, en un lapso de diez días el nivel del río Paraná descendió casi un metro y la tendencia marca que es probable que volvamos a ver un escenario crítico de escasa agua como en 2020.

Todo indica que la bajante va a persistir, con una leve estabilización que se reflejaría en los índices hidrométricos de la capital santafesina en la última semana de abril. El 13 de abril el Puerto registró un piso de 2,51 metros, ya por debajo de los 2,60 metros que marca el límite de aguas bajas indicado por el Instituto Nacional del Agua (INA). Actualmente, las mediciones registran un nivel de 1,57 metros frente al Puerto de Santa Fe, según Prefectura Nacional.

UNO Santa Fe dialogó con el ingeniero Juan Borús, subgerente de Sistemas de Información y Alerta Hidrológico del INA. Respecto de la evolución de la bajante en la región, Borús sostuvo: "La tendencia climática nos impone que las lluvias van a continuar siendo escasas. Es probable que continuemos con una evolución similar a la del año pasado, con el momento más crítico de 2020 en la segunda quincena de mayo, cuando hubo lecturas espantosas y con la Setúbal en condición pésima.

En los próximos días la bajante en el Paraná se estará estabilizando a la altura de Goya en Corrientes y Reconquista en Santa Fe, donde se define el piso. Ese piso se está propagando por la cuenca del Paraná, por lo que la estabilización en Santa Fe se debería dar "entre el miércoles 28 y jueves 29 de abril.", según lo afirmado a UNO por Borús. El piso definido actualmente por el INA en Santa Fe es de entre 1,55 y 1,60 metros.

En este contexto, la probabilidad de que durante la segunda quincena de mayo y todo junio se observen niveles que se observaron el año pasado "no es nula, es tan significativa como que se mantengan los registros actuales en el río Paraná", según afirmó Borús, aunque resaltó que "asegurar hoy por hoy que haya uno u otro escenario es dificilísimo".

**Escenarios probables de la bajante**

El especialista del INA distinguió dos escenarios probables que podrían suceder según cómo evoluciona la bajante, analizando "si la seca va a afectar a la cuenca del río Paraná y por lo tanto afectando a toda la costa santafesina; o si además la seca va a afectar los bajos submeridionales y la cuenca del río Salado completa, afectando también a la laguna Setúbal".

Consultado por la crecida que se dio en enero y febrero en la cuenca del Paraná, el ingeniero indicó que "se trató de un evento muy particular con lluvias muy especiales, habiendo sido un solo evento pero muy intenso y típico de verano. Ahora estamos en otoño y la probabilidad de que un evento con lluvias como estas se repita es mucho menor".

Otra cuestión que hace un tiempo prolongado afecta al grado de precisión en el que los especialistas pronostican uno u otro escenario en cuanto a niveles hidrométricos probables es el de la variabilidad climática, ya sea interanual o en el mismo año en cuestión. En este sentido, Borús manifestó: "Mirando 10 o 15 años atrás se puede ver una variabilidad climática tan grande que es imposible asegurar que no habrá lluvias que mejoren la cosa. El horizonte de proyección hidrológica es sumamente reducido, el clima esta muy alterado y eso limita mucho la capacidad de proyección".

"Optamos por mostrar el escenario más probable y estamos tratando de mostrar los pronósticos para el martes 27 de abril y martes 4 de mayo con un rango de oscilación creíble. Ese rango no responde a un índice estadístico, sino a un análisis criterioso de cómo viene la situación interpretando cómo puede ser el margen inferior y superior en la banda de oscilación probable, pero por ahora es muy acotado", concluyó el ingeniero.