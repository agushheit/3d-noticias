---
category: Agenda Ciudadana
date: 2021-09-02T06:15:53-03:00
thumbnail: https://assets.3dnoticias.com.ar/PLATAFORMA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Acuerdo en el Senado para emitir una declaración de rechazo a Chile por la
  plataforma marítima
title: Acuerdo en el Senado para emitir una declaración de rechazo a Chile por la
  plataforma marítima
entradilla: Los senadores de los dos principales espacios políticos coincidieron en
  la necesidad de elaborar una declaración unánime de rechazo que será aprobada en
  la próxima sesión.

---
Los bloques políticos del Frente de Todos (FdT) y de Juntos por el Cambio (JxC) del Senado acordaron este miércoles elaborar una declaración en conjunto de rechazo a la resolución del Gobierno de Chile de fijar un nuevo límite que avanza sobre una zona de la plataforma marítima en el Atlántico sur que forma parte de la Argentina.

Durante una reunión de la comisión de Relaciones Exteriores, en la que el canciller Felipe Solá declaró que la única vía para resolver la controversia es la negociación con el vecino país, los senadores de ambos espacios políticos respaldaron la redacción de una declaración unánime de rechazo al decreto del presidente chileno, Sebastián Piñera, que será aprobada en la próxima sesión del Senado.

En la declaración, los senadores expresan "su más enérgico rechazo a la pretensión del gobierno de Chile de extender su plataforma continental al este del meridiano 67º 16´ 0, violando el Tratado de Paz y Amistad de 1984", que fue firmado por ambos países.

Durante la videoconferencia de este miércoles, el canciller declaró que "no hay otra manera" de resolver la controversia (surgida tras un decreto del presidente chileno Sebastián Piñera que fija un nuevo límite de la plataforma marítima) que no sea las de la vía de las negociaciones y afirmó que "no existe una tercera manera".

En la declaración, los senadores expresan "su más enérgico rechazo a la pretensión del gobierno de Chile de extender su plataforma continental al este del meridiano 67º 16´"

Solá también reclamó a todos los sectores políticos que "fijen posición" y que, además de hacerle llegar recomendaciones, se pronuncien sobre "quién tiene razón" en el litigio.

El pedido de Solá a la oposición se produce luego del documento del PRO emitido el último lunes, en medio de la escalada de la controversia con Chile, en el que se instaba a ambas naciones "a mantener conversaciones para superar la situación", algo que cosechó el rechazo del propio canciller y sectores del oficialismo.

"El PRO ha puesto en un plano de igualdad el reconocimiento de la ONU de los derechos argentinos sobre el mar austral con una decisión unilateral expresada en un decreto presidencial de Chile", había cuestionado este martes el titular del Palacio San Martín en su cuenta de la red social Twitter.

Y agregó: "Reniegan de nuestros derechos dejando de lado los intereses de la Patria. Sin palabras".

Un comunicado de la Secretaría de Relaciones Internacionales del PRO, firmado el lunes pasado por la titular del partido, Patricia Bullrich, y por el secretario de Relaciones Internacionales, Fulvio Pompeo, había pedido que ambas naciones "se reúnan e inicien un diálogo que permita zanjar las diferencias hoy existentes".

"Vamos a aclarar los tantos de manera tal de que quede claro que estamos todos del mismo lado y por qué estamos del mismo lado", comenzó su exposición esta tarde Solá ante los senadores de la Comisión de Relaciones Exteriores, que preside el puntano Adolfo Rodríguez Saá.

El funcionario pidió que todos los sectores "digan qué piensan del fondo de la cuestión más allá del consejo sabio de negociar" y consideró que lo que "lo que está en juego son 5 mil kilómetros cuadrados de plataforma argentina inobjetables y 25 mil km2 de lecho y subsuelo que forman parte del patrimonio de la humanidad y que Chile con este decreto intempestivo se apropia".

"Yo les digo que no hay otra manera de tomar esto que no sea con negociaciones bilaterales o laudo con un tribunal arbitral. No existe una tercera manera, ese es mi trabajo. Lo que les pido a quienes no comparten (esta posición) no es que me aconsejen, porque nosotros ya sabemos que tenemos que negociar; les pido que digan quién tiene razón de los dos", argumentó.

Solá esgrimió que si un sector no se pronuncia sobre la cuestión "es como mirar desde un balcón y decir 'conversen muchachos'".

Desde la oposición, el senador Julio Cobos (Juntos por el Cambio-Mendoza) apoyó en representación de su bancada la importancia de lograr un dictamen de consenso, sostuvo que la postura de Chile "es inentendible" y coincidió con que se debe tener "una postura común" en rechazo a la medida del Gobierno chileno.

El senador Juan Carlos Romero (Peronismo Federal-Salta) afirmó que "este es un tema que va más allá de las diferencias políticas" y que se debe sentar "una posición común" y por "unanimidad".

"Juntos por el Cambio está con postura unánime. Acá no hay ningún tipo de discrepancias", afirmó el senador Pablo Blanco (Tierra del Fuego-JxC), saliendo al cruce de expresiones de la senadora Juliana Di Tulio, quien señaló que "no estaba clara" la postura del PRO.

Solá leyó ante los senadores un escrito de tres páginas para dejar sentada la posición argentina, en la que calificó como "expansiva" la postura chilena y aseguró que en los últimos 36 años ese país no manifestó objeción alguna a los límites de la plataforma marítima.

El canciller citó aspectos del Tratado de Paz y Amistad firmado por Chile y Argentina en 1984, en el que sostiene que ese país "puede tener una zona económica exclusiva al oeste, no al este".

"Chile no puede pretender proyectar su soberanía más allá de lo acordado en el artículo 7 del Tratado de Paz y Amistad de 1984 que dice que tiene derecho a estar al oeste del meridiano y no al este", precisó.

Para Solá, "la conducta de Chile de 1984 hasta mayo de 2020, 36 años, resulta plenamente concordante con la interpretación que Argentina ha formulado de ese tratado y Chile dispuso de ocasiones de expresar su desacuerdo y optó por no hacerlo con los efectos que ello conlleva".

"En ningún momento Chile hizo planteo alguno que se parezca al actual. Hubiera bastado con que Chile emitiera una nota a nuestro país o a la comisión de límites de la plataforma continental (Convemar) tal como lo hizo Argentina en su momento", añadió.

En su exposición, aclaró que, de esta forma, al no objetar, Chile reconoció que "la demarcación del límite exterior de la plataforma continental argentina adquirió el carácter de definitivo y obligatorio según los artículos 66 y 68 de la Convemar".

El canciller sostuvo además que, en todo caso, si se está en desacuerdo, "el mecanismo de solución de controversias es el previsto en el Tratado de Paz y Amistad de 1984".

"A Chile le queda lo convenido en ese tratado, no recurrir a la Convemar, sino a un tribunal arbitral decidido por los dos países", declaró, y añadió que "a lo largo de casi dos siglos ambos países han logrado resolver sus controversias limítrofes".

En la videoconferencia, el presidente de la comisión de Relaciones Exteriores, Adolfo Rodríguez Saá, respaldó el pedido de Solá y lo calificó como "un claro pedido de respaldo a la política de defensa de los intereses de la Argentina encabezado por nuestro gobierno".

Por su parte, el secretario de Malvinas, Antártida y Atlántico Sur, Daniel Filmus, opinó que "gratamente" ven "que hay unanimidad de las fuerzas políticas argentinas respecto de la respuesta del gobierno argentino a Chile".