---
category: La Ciudad
date: 2021-03-13T06:00:49-03:00
thumbnail: https://assets.3dnoticias.com.ar/concejo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: El Concejo santafesino solicitó la presencia de la Dirección de Mujeres y
  Disidencias en comisarías
title: El Concejo santafesino solicitó la presencia de la Dirección de Mujeres y Disidencias
  en comisarías
entradilla: El objetivo es que personal capacitado pueda brindar asistencia y orientación
  a víctimas de violencia de género que quieran radicar una denuncia.

---
El Concejo Deliberante de Santa Fe solicitó que se evalúe la posibilidad de elaborar un convenio de cooperación con la Provincia de Santa Fe para que en las comisarías de la ciudad haya personal perteneciente a la Dirección de Mujeres y Disidencias municipal.

La Directora de la Dirección de Mujeres y Disidencias, Soledad Artigas, reconoció públicamente que reciben todos los días "denuncias de mujeres que en las comisarías no les toman las denuncias… El protocolo dice que todas las denuncias recibidas en las comisarías tienen que llamarnos a nosotras para que podamos acercarnos a esa mujer, citarla, ir a acompañarla, y eso no está pasando. Entonces la mujer queda sola frente a la policía”.

Si bien en la provincia existe una “Guía de Acompañamiento de Personas en Situación de Violencia por Motivos de Género”, esta no se aplica y no es conocida por todos. Quienes se acercan a las comisarías a denunciar muchas veces se encuentran con largas esperas, malos tratos, demoras y respuestas irrespetuosas e ilógicas.

Aunque la Comisaría de la Mujer funciona las 24 horas los siete días de la semana, muchas mujeres no pueden acceder por estar ubicada en zona céntrica -Lisandro de la Torre Nº 2665- y optan por acudir a las comisarías barriales.

La concejala aseguró que “resulta fundamental y vital la asistencia, orientación y contención de las víctimas que, tras un proceso de sufrimiento y temor, deciden pedir ayuda. Es acertado permitir que en cada comisaría haya una persona capacitada junto a la policía para asegurar el inicio de las actuaciones pertinentes”