---
category: Agenda Ciudadana
date: 2021-12-28T06:15:27-03:00
thumbnail: https://assets.3dnoticias.com.ar/MARTORANO.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Si se tensiona el sistema sanitario, Martorano no descarta establecer algunas
  restricciones
title: Si se tensiona el sistema sanitario, Martorano no descarta establecer algunas
  restricciones
entradilla: 'Sería para eventos masivos, como implementó Córdoba: "Seguramente será
  una decisión de Nación", aclaró la ministra. Sobre 60 mil testeos semanales en toda
  la provincia, la positividad subió el 15%. '

---
La foto de las interminables filas de personas esperando testearse estos últimos días, sumado al incremento inusitado de casos de Covid-19, encendió las alertas: ¿estamos a las puertas de una tercera ola de coronavirus? ¿Hay alguna forma de frenar la circulación viral, con todo "abierto y habilitado"? El contexto parece complicado, más teniendo en cuenta las reuniones masivas por las fiestas de fin de año y el arranque de la temporada veraniega.

 "Es cierto que los casos vienen en aumento, pero afortunadamente los cuadros son leves. Sabíamos que iba a haber una suba para diciembre; ésta fue importante, aunque estamos muy debajo de otras provincias. Creo que lo que pasa hoy vino de la mano de las habilitaciones, de la ampliación de los aforos, y sobre todo de las reuniones sociales multitudinarias, ahí está el gran tema...", pone en contexto la Ministra de Salud de la provincia.

Y da algunos números: hace unas semanas había un 3-4% de positividad -cantidad de positivos confirmados por PCR sobre el total de hisopados realizados en toda la provincia-. En la última semana se realizaron 6 mil testeos por día (es decir unos 40 mil semanales) y la positividad ascendió al 15%. La tendencia es al aumento, "pero esto tiene relación con que se está testeando y aislando", asegura.

  Otro dato que se mira de cerca: la ocupación de camas es del 55%. "Todavía no es un porcentaje preocupante", señala la funcionaria. También, tomando los internados del sector público y del privado, hay 12 pacientes en Unidad de Terapia Intensiva (UTI), y 15 en sala Covid en toda la provincia. "Aún es bastante bajo", afirma Martorano.

 Pero un elemento genera esperanza. "A la fecha, tenemos al 80% de toda la población santafesina con dos dosis. Y desde este martes se acorta la aplicación de la tercera dosis a cuatro meses para personal de Salud o personas de más de 60 años. Esto es para que estos grupos estén más protegidos. Pero por otro lado -advierte-, todavía tenemos muchas personas sin siquiera una dosis, y con esquema incompleto. Las estamos incentivando a que vayan a inocularse".

Y de la mano de la vacuna, "probablemente esto (la pandemia) vaya hacia alguna forma endémica de la enfermedad. Es decir, que el virus podría seguir circulando pero sin generar cuadros graves. Es lo que se espera a futuro. Pero hoy, cuidémonos por favor", pide a la sociedad.

No obstante, si la escalada de "positivos" sigue en ascenso en Santa Fe, ¿podrían establecerse nuevas restricciones?, consultó este diario a la ministra. "Córdoba fijó restricciones, porque sus números son importantes. Nosotros hoy apelamos al cuidado, testeo y vacunación. Y si vemos una escalada que tensione el sistema sanitario, probablemente desde Nación se tome alguna decisión (de restricciones), es lo esperable. Ahora estamos atentos y ocupados en contener", dijo, moderada.

**El contexto**

\-¿Cómo se explica este incremento tan rápido de los casos y de la positividad? ¿Tiene que ver con que el RO de Delta y de Ómicron que son mucho más altos (más transmisibilidad), con comportamientos sociales descuidados, etcétera?

\-Sí, hay de ambas cosas; volvieron los besos y los abrazos, el mate que se comparte y no está bien, más la alta contagiosidad de ambas variantes. Lo positivo es que de la mano de la vacuna, los cuadros de las personas infectadas se cursan de forma leve como un resfrío, con dolor de garganta, un poco de mucosidad y no mucho más.

\-¿Ya empezó la tercera ola? ¿Está preparado el recurso humano, instrumental y logístico en Santa Fe para hacerle frente a lo que viene?

\-Si es una tercera ola, lo veremos en las próximas semanas. Lo que nos preocupa es lo que pasa en otras provincias, con las cuales Santa Fe tiene mucha conectividad, por lo cual seguramente tendremos un pequeño pico de casos. Esperemos no llegar a lo que pasa ahora en Córdoba, pero vuelvo a hablar de la conectividad: muchos santafesinos van a vacacionar allá. Buenos Aires, lo mismo.

Toda la estructura sanitaria provincial está preparada. Lo que estamos tratando de reforzar ahora es el trabajo más territorial, en los centros de salud, consultorios... Que se vaya detectando que un dolor de garganta o una rinitis puede ser Covid, testear y aislar, y estar detrás de esto más que nada.

\-¿Delta sigue teniendo dominancia o pasará a ser Ómicron en breve?

\-Sí, hoy predomina Delta, pero Ómicron sigue aumentando y probablemente entre enero y febrero sea dominante.

\-¿La coexistencia de las dos variantes podría agravar la situación?

\-No necesariamente.

\-Tras el anuncio de Nación, ¿se vuelve en Santa Fe a la confirmación de caso por nexo clínico-epidemiológico, es decir, el contacto estrecho de un positivo pasa a ser positivo sin necesidad de testeo?

\-Algo de esto se habló a nivel nacional. En la provincia aún no está confirmado, porque hemos detectado muchos contactos estrechos que se hisoparon y dieron negativo. Entonces, tendrías un montón de gente que debiera estar aislada, y por ahí no es portadora del virus, aún siendo contacto estrecho de otra infectada. Depende del momento y de cada jurisdicción.

\-¿Qué pasa con las reinfecciones? ¿Están detectando personas que tuvieron al menos dos veces Covid?

\-Sabemos a nivel nacional que sí, que se están dando reinfecciones. ¿Puede ocurrir? Sí, claro. Porque recordemos que la vacuna evita la internación y el cuadro grave de Covid, pero no la infección ni la reinfección, aunque protege bastante de ser contagiado. Lo que vemos es que muchas personas vienen "positivas" de los viajes, de otras provincias. Los viajes de egresados, por ejemplo. Ahí se hace un seguimiento para que se cumpla con el aislamiento.

  -Alcanza con salir a la calle y ver a la gente sin barbijo, o con el barbijo de "adorno" colgando en el mentón; gente compartiendo el mate... ¿Tenemos que volver a protocolizar nuestras vidas, tras la experiencia de la segunda ola?

 -¡No compartir el mate, por favor! Hay que seguir usando el barbijo, que sigue siendo obligatorio en la provincia: acá nunca se suspendió su uso. Con vacunas y un barbijo bien colocado, la situación siempre será diferente.

Además, yo evitaría las reuniones sociales grandes en Año Nuevo; le diría a la gente que trate de estar con su grupo más habitual, con la familia. Cuidarnos un poco, y luego veremos qué pasa en las dos o tres semanas subsiguientes, que es donde vamos a ver claramente la tendencia. Porque pueden ocurrir varias cosas: que el pico sea alto; que aumenten las internaciones; que haya una tasa amesetada, o que aunque los casos aumenten, los cuadros sigan siendo leves. Nosotros decimos: si tenés síntomas o dudas, testeate. Pero más que nada, vacunate; si te falta la segunda dosis, que te la apliquen por favor. A estos grupos apuntamos.