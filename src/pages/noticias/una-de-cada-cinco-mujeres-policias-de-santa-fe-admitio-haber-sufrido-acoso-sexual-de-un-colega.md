---
category: Estado Real
date: 2021-03-14T06:15:29-03:00
thumbnail: https://assets.3dnoticias.com.ar/mujer.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Una de cada cinco mujeres policías de Santa Fe admitió haber sufrido acoso
  sexual de un colega
title: Una de cada cinco mujeres policías de Santa Fe admitió haber sufrido acoso
  sexual de un colega
entradilla: El 90% de las mujeres policías que sufrieron acoso sexual no denunció
  el hecho, según el Censo Policial elaborado por el Observatorio de Seguridad Pública
  de la provincia, junto con la UNL.

---
El Observatorio de Seguridad Pública de la provincia (OSP), dependiente del Ministerio de Seguridad provincial, llevó a cabo un Censo Policial en conjunto con la Universidad Nacional del Litoral (UNL). Allí fueron censados 19.126 efectivos policiales de todo el territorio santafesino, y se constató que una de cada cinco mujeres policías en Santa Fe sufrieron alguna vez un episodio de acoso sexual de parte de un colega o superior varón.

La titular del OSP, Luciana Ghiberto, sostuvo que "en el módulo donde preguntamos cómo se sentían las mujeres policías desempeñando sus trabajos en la fuerza, consultamos por casos de abusos o acoso sexual y encontramos que una de cada cinco policías había sido acosadas sexualmente por un colega o un superior varón.

Además, en ese mismo subtema, se constató que el 90% de las mujeres policías que sufrieron acoso sexual decidieron no denunciar esos hechos. Con respecto a esto, Ghiberto sostuvo que "en algunos casos, no lo hicieron porque eligieron solucionar los problemas ellas mismas, pero también porque tenían miedo de ser cambiadas de destino o que les pasara algo a partir de denunciar".

"Hoy tenemos más información, pero esta gestión ha creado la subsecretaría de Género y Bienestar en la Policía que tiene como una de sus líneas recibir a las mujeres policías que han sufrido violencia machista y poder acompañarlas en el proceso", continuó la referente del OSP.

**Corrupción en la fuerza**

Siguiendo con el desglose del Censo Policial, otro dato refleja que nueve de cada diez efectivos de la policía provincial consideraron que la situación de corrupción en la fuerza es "grave o muy grave", aunque un 80% destacó que en su entorno no la había percibido.

En la misma línea, el 53,8% de los efectivos encuestados, algo más de la mitad, indicaron que el hecho de denunciar un posible hecho de corrupción que hayan presenciado en su ejercicio le podría generar problemas en su trabajo.

A propósito de esto, Ghiberto sostuvo que "el 83% de los policías consideró que en su lugar de trabajo no percibió corrupción. Pero cuando se les preguntó sobre la corrupción policial como un problema en término más general, casi el 90% lo consideró un problema grave o muy grave".

**Un Censo Policial "inédito en Latinoamérica"**

La titular del OSP calificó el Censo Policial como "inédito en el país y en Latinoamérica porque es una empresa que es fuertemente ambiciosa ya que tiene características de un censo, con recolección de datos demográficos. Tiene un formato de encuesta cerrada con 240 preguntas, donde se abordaron temas como ingreso, formación, trabajo, licencias, cómo trabajaron durante la pandemia, entre otros".

Además, Ghiberto destacó la organización del estudio: "el Censo Policial ha sido organizado por el Ministerio de Seguridad en conjunto con la Universidad del Litoral, lo que da seriedad a la iniciativa en términos de anonimato y confidencialidad".

Según explicó la funcionaria, de los 21.490 efectivos policiales en todo el territorio santafesino que en octubre de 2020 estaban para censar respondió un 89%, lo que fue considerado por Ghiberto como "un índice de respuesta exitoso".