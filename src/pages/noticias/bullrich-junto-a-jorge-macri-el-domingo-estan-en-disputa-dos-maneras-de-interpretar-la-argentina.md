---
category: Agenda Ciudadana
date: 2021-11-12T06:00:43-03:00
thumbnail: https://assets.3dnoticias.com.ar/PATO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: 'Bullrich, junto a Jorge Macri: "El domingo están en disputa dos maneras
  de interpretar la Argentina"'
title: 'Bullrich, junto a Jorge Macri: "El domingo están en disputa dos maneras de
  interpretar la Argentina"'
entradilla: 'La titular del PRO recorrió Vicente López junto al intendente local y
  lanzó un llamado a votar por la coalición opositora en las legislativas del 14 de
  noviembre, con fuertes críticas al Gobierno.

'

---
La presidenta del PRO, Patricia Bullrich, visitó hoy el distrito de Vicente López acompañada por el intendente local, Jorge Macri, y remarcó que en las elecciones legislativas del próximo domingo estarán "en disputa dos maneras de interpretar la Argentina", con "la cultura del trabajo" de un lado y "el estancamiento" del otro.

Bullrich recorrió el centro comercial de Villa Martelli junto al jefe comunal y presidente del PRO bonaerense y a la concejal Soledad Martínez, para conversar con vecinos y comerciantes y pedir el voto para Juntos por el Cambio en los comicios que definirán la nueva composición del Congreso.

"El domingo está en disputa dos maneras de interpretar la Argentina: una es la de Juntos, que es la cultura del trabajo y la educación, y otra es la del gobierno, que nos lleva al estancamiento, donde hay más planes sociales que empleo", disparó la dirigente.

A su vez la ex ministra de Seguridad manifestó la confianza que reina en la principal coalición opositora de cara a las legislativas tras la victoria obtenida en las primarias de septiembre y expresó: "Estamos convencidos que el 14 de noviembre la gente nos va a acompañar, con más fuerza todavía”.

Por su parte, Jorge Macri subrayó que si se repiten los resultados de las PASO y se redoblan los esfuerzos la oposición va a "conseguir los diputados y senadores necesarios para poder transformar desde el Congreso el día a día de los argentinos".

"Hay una Argentina de pie que quiere trabajar y ser libre, eso es lo que está en discusión el domingo", concluyó el intendente durante la conversación que mantuvieron con los vecinos y comerciantes en el café "Su Favorita", donde el tema central de la charla fue la situación económica.

Por su parte, Martínez destacó la recepción que obtuvieron en las recorridas de campaña y subrayó: "Sentimos el cariño y el apoyo de los vecinos a lo que estamos haciendo con Jorge. Nada nos va a correr de la tarea diaria de mejorarle la vida a cada uno de ellos".