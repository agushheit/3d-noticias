---
category: La Ciudad
date: 2021-10-31T06:00:10-03:00
thumbnail: https://assets.3dnoticias.com.ar/iturraspe.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Las claves del servicio de Salud Mental que propone el "viejo" Hospital Iturraspe
title: Las claves del servicio de Salud Mental que propone el "viejo" Hospital Iturraspe
entradilla: "La propuesta incluye el abordaje de adicciones. Recursos profesionales
  adecuados, infraestructura y actividades que posibiliten la reinserción social y
  familiar de pacientes figuran entre las principales premisas.\n\n"

---
¿El "ex" Hospital Iturraspe? ¿El "viejo" Iturraspe? Mientras buscamos cómo denominar a ese enorme edificio de Bulevar Pellegrini y Avenida Freyre, que lleva el nombre del ex gobernador José Bernardo Iturraspe y cobija desde hace más de cien años a uno de los centros de salud más importantes de la región, todo indica que sigue en pie y bien activo.

 Reabierto el 13 de octubre de 2020 para albergar pacientes y tareas derivadas de la pandemia de Covid-19 (internaciones en sala común y en terapia intensiva, hisopados, vacunación), proyecta su crecimiento a futuro como Centro Oncológico y Servicio de Salud Mental y Hospital de Día. En este último caso y de concretarse, el nombre ya estaría resuelto: "Mauricio Goldenberg". 

 "La finalidad de esta propuesta es constituir un Hospital de Día dependiente del Servicio de Salud Mental del Hospital Oncológico 'José Bernardo Iturraspe', de referencia en la ciudad de Santa Fe, situado en un lugar estratégico por su accesibilidad e integración con el sistema público de salud, orientado a la atención de las necesidades de salud mental de la comunidad y, particularmente, al abordaje del consumo problemático de sustancias".

 Así lo indica el texto "teórico" con el que presenta la iniciativa, que se enmarca en leyes y en realidades cotidianas. Las primeras son la Ley Nacional de Salud Mental N° 26.657, y las provinciales N° 13.671, de prevención, tratamiento integral de consumo problemático de sustancias, internación y vinculación sociolaboral, y la N° 12.967, de promoción y protección integral de los derechos de las niñas, niños y adolescentes. Y las segundas se sintetizan en la necesidad de garantizar una atención ambulatoria, intensiva e interdisciplinaria a personas que atraviesan un padecimiento mental o un cuadro de adicción.

"La presente propuesta se sustenta en la convicción de que para que las personas y los grupos gocen de salud mental, además de tener acceso a distintos dispositivos dentro del sistema de salud según su padecimiento, es necesario considerar el acceso al trabajo, a la vivienda, a la cultura, al deporte y a la participación social entre otras dimensiones".

"Para esto, es necesario animarse a la transformación de los servicios ya existentes: es urgente pasar de un modelo centrado en la exclusión a otro basado en la integración; del espacio institucional cerrado al espacio comunitario abierto; del enfoque basado en la enfermedad y la peligrosidad a otro basado en el concepto de sujeto de derecho en su proceso de integración social, fundado en el resguardo o la restitución de sus derechos", sigue la presentación del proyecto que menciona, como se ve, muchos de los prejuicios que sustentan el imaginario sobre el padecimiento mental.

En los hechos, hay personas internadas por padecimientos mentales en la sala general, que es lo que establece el paradigma de atención de estos cuadros: no aislamiento y si integración en la atención de salud. "Se trata de que las instituciones monovalentes se transformen en polivalentes", resume Bertero para mencionar el objetivo del nuevo paradigma que establecen las leyes. Sin embargo, "lo que falta es reforzar la atención con recursos humanos en salud mental".

  **La elección del nombre**

 Para el médico especialista, parte de ese camino ya está recorrido en el hospital porque el servicio de Salud Mental funciona allí desde 1987. "Cuando retorna la democracia en la Argentina, uno de los ministros de Salud fue Aldo Neri, que tenía al Dr. Mauricio Goldenberg como asesor y pionero en la creación de servicios de salud mental en hospitales generales" o, dicho de otro modo, en la integración de pacientes de Salud Mental a la comunidad.

 La primera experiencia en ese sentido fue en el hospital Lanús en Buenos Aires, y en ese contexto se creó el servicio en el Iturraspe, "que fue uno de los primeros en la región". "Los médicos psiquiatras con formación en Psicoanálisis a cargo fueron Humberto Bellada y Alberto Gaspari", reconoce Bertero.

 "No es fácil el funcionamiento de este servicio en un hospital general", reconoce Bertero. Y apunta que es necesario un tratamiento ambulatorio intensivo que tal vez no llegue a internación. Pero para poder hacerlo "es necesario contar con un servicio integral que funcione y que en las guardias existan profesionales psiquiatras o personas entrenadas para una valoración de riesgo".

**Lazos y reinserción**

 En definitiva, "la propuesta es volver a tener el servicio de Salud Mental dentro del Hospital Iturraspe, con internaciones lo más breves que se pueda y continuidad de un tratamiento ambulatorio intensivo". Y sumar a la atención en Psiquiatría y Psicoterapia, un "brazo social y comunitario" con Psicología Social, talleres de oficios y otras actividades que faciliten la reinserción de pacientes más allá de la primera etapa de desintoxicación, en el caso del tratamiento de adicciones.

 "Vivimos en una comunidad donde hay ciertas ideas sociales ancladas en prejuicios que estigmatizan y definen que una persona con padecimiento mental debe ser internada o tiene que ir a una comunidad terapéutica", dice Bertero. El contrapunto de esta idea lo aporta el proyecto que propone desarrollar actividades que faciliten el vínculo social y familiar, y posibiliten la reinserción social.

 Precisamente, se prevé el funcionamiento del dispositivo de Hospital de Día, "con el objetivo de brindar un tratamiento alternativo a la hospitalización de tiempo completo, evitando la separación de la persona de su entorno familiar, laboral y educativo".

 

 