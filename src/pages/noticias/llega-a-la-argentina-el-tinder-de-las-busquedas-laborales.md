---
category: Agenda Ciudadana
date: 2021-03-11T07:38:50-03:00
thumbnail: https://assets.3dnoticias.com.ar/empleo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Rosario3
resumen: Llega a la Argentina el "Tinder de las búsquedas laborales"
title: Llega a la Argentina el "Tinder de las búsquedas laborales"
entradilla: Mediante sistemas de inteligencia artificial, la aplicación Workifit "matchea"
  entre candidatos a una vacante laboral y las empresas. Una iniciativa de emprendedores
  uruguayos con foco en Buenos Aires, Rosario y Córdoba

---
Con una inversión estimada en u$s400.000, se lanzó Workifit, una aplicación que tiene como foco aportar transparencia al mercado laboral, sin permitir la publicación de oportunidades confidenciales, donde los candidatos pueden estar de forma anónima y sólo ser vistos por las empresas que ellos autorizan.

El emprendimiento, liderado por un grupo de profesionales uruguayos, ya logró el financiamiento de la ANII (Agencia Nacional de Investigación e Innovación) en dos ocasiones, el apoyo de mentores del CIE (Centro de Innovación y Emprendimientos de ORT) y una alianza con la CUTI (Cámara Uruguaya de Tecnologías de la Información).

Además, fueron finalistas del concurso Elevator Day de Santander, con el premio de un préstamo tasa 0% por un valor de u$s15.000.

“Comenzamos a trabajar la idea, identificando un cambio importante en las prioridades en las nuevas generaciones respecto a la búsqueda laboral y lo que significa el trabajo para cada uno. Hoy ya no queremos simplemente encontrar un lugar donde ejercer nuestra profesión, sino que buscamos un entorno laboral donde podamos ser nosotros mismos, sentirnos cómodos, que la empresa comparta nuestros mismos valores, que podamos disfrutar del trabajo, y que al mismo tiempo nos impulse a crecer”, señaló Alexis Valín, CEO de Workifit.

Más de 170 empresas ya han contactado candidatos a través de Workifit, entre las que se encuentran Globant, Oracle, Prisma Medios de Pago, Santander, VU Security, GeneXus, Xmartlabs, Scanntech, Oktana y Arbusta. Los socios son Alexis Valín, CEO; Christian Bouvier, CTO; Ezequiel Jardim, Lead Engineer of Web Development; Diego Ernst, Lead Engineer of Mobile Development; y Alvaro Larrosa, Lead Engineer of Matching Sub-system.

En el segmento B2B, apuntan a empresas innovadoras, que buscan construir buenas culturas de trabajo, de base tecnológica, pero no sólo de desarrollo de software, sino que pueden ser de la industria fintech, e-commerce, marketing, sportech, retail, telecomunicaciones, entre otras. Pueden ser tanto pequeñas startups como grandes multinacionales.

En el segmento B2C, apuntan principalmente a jóvenes profesionales de carreras relacionadas al mundo digital, por ejemplo: Software Developers, Project Managers, Digital Marketing Analysts, Business Developers, Data Analysts, UX/UI Designers, etc.

El 85% de la población a nivel mundial no se siente motivada por su trabajo según la consultora internacional Gallup, y más de un 60% está considerando cambiar de trabajo. Sin embargo, las empresas demoran en promedio 56 días en poder cubrir sus vacantes.

“Esto refleja una desconexión entre los intereses de las personas y las empresas, y en gran parte se debe a que los procesos de vinculación laboral son muy complejos, con enormes listados, filtros, formularios, y al mismo tiempo donde falta información esencial para que ambas partes puedan avanzar de una forma más rápida. Workifit nace para simplificar estos procesos, haciéndolos más simples, dinámicos y transparentes”, agregó Valín.