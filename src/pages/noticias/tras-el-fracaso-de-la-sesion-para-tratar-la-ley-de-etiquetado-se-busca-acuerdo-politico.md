---
category: Agenda Ciudadana
date: 2021-10-06T06:15:19-03:00
thumbnail: https://assets.3dnoticias.com.ar/ETIQUETADO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Tras el fracaso de la sesión para tratar la ley de etiquetado, se busca acuerdo
  político
title: Tras el fracaso de la sesión para tratar la ley de etiquetado, se busca acuerdo
  político
entradilla: El Frente de Todos quedó a siete bancas del quórum necesario para tratar
  la iniciativa del rotulado, que podría perder estado parlamentario a fin de año.
  Sergio Massa mantuvo reuniones con los bloques opositores.

---
La Cámara de Diputados no pudo debatir este martes el proyecto de etiquetado frontal de alimentos debido a que Juntos por el Cambio (JxC) no dio quorum para realizar la sesión especial impulsada por el Frente de Todos (FdT), pero tras las acusaciones cruzadas entre el oficialismo y la oposición se avanza en un acuerdo para tratar la próxima semana un temario consensuado.  
  
Fuentes parlamentarias señalaron que el objetivo es poder concretar una sesión especial el próximo miércoles con la inclusión de los proyectos de etiquetado frontal de alimentos -iniciativa considerada clave para educar a la población sobre los efectos negativos de los excesos de grasas, sodio y azúcares-, de régimen previsional para los viñateros y para garantizar los derechos humanos de las personas en situación de calle de la Argentina.  
  
Para lograr ese objetivo, el presidente de la Cámara de Diputados, Sergio Massa, mantuvo en forma separada reuniones con los bloques políticos opositores, a quienes les aclaró que los temas previstos este día deberán incluirse en la agenda, y recibió las propuestas de esas bancadas para efectuar una sesión consensuada.  
  
En esos encuentros, JxC solicitó la incorporación de la ley ovina, la declaración de la emergencia educativa y cambios en la ley de alquileres, como habían solicitado en la reunión de Labor Parlamentaria.  
  
Desde el Interbloque Federal se pidió también la ley ovina, la derogación del decreto que permitió el blanqueo de capitales de la familia del expresidente Mauricio Macri, la derogación de la ley de alquileres y la cesión a Córdoba de los terrenos donde funcionó el campo de detención ilegal "La Perla" en la dictadura cívico-militar, informaron fuentes parlamentarias.  
  
Estas reuniones entre Massa y los diferentes bloques políticos sucedieron a las acusaciones cruzadas entre el FdT y JxC tras el fracaso de la sesión por la decisión de la principal oposición y de la mayoría del interbloque Federal de no colaborar para conformar el quorum reglamentario de 129 diputados.  
  
El jefe del bloque del FDT, Máximo Kirchner, acusó a la oposición de buscar "hacernos actuar bajo extorsión", y pidió que "deberían ser más pacientes" porque "para el 2023 faltan dos años, y eso también es la República".  
  
"Han vuelto a comportarse como siempre fueron cuando les tocó ser gobierno: suprimir a quienes pensaban diferentes, perseguirlos. No soportan la democracia a veces porque esta ley, por ejemplo, lo que hace no es ni siquiera prohibir el azúcar, sino poder ser conscientes de qué consumimos, de cómo nos alimentamos", aseveró Kirchner.  
  
Desde la oposición, el jefe del Interbloque de JxC, Mario Negri, aclaró que "quiero desmitificar que no bajamos por la ley de etiquetado, esa es una chicana, es una ley que se va a aprobar porque tiene consenso amplio. Hoy no se trató porque el kirchnerismo no quiso acordar los temas con la oposición. Esperemos que la semana que viene sea otra historia".  
  
El FdT pidió una sesión especial que tenía como tema central el etiquetado frontal de alimentos pero no pudo alcanzar el quorum ya que junto a algunos bloques provinciales pudo reunir 122 diputados, siete menos de los requeridos para alcanzar el número reglamentario para habilitar la primera sesión especial y tras tres meses de una virtual parálisis legislativa.  
  
Para llegar a los 122 diputados, el FdT contó con los propios representantes de las provincias del norte del ese espacio, que estaba en duda que participarían del debate, y sumó a los dos diputados de la Izquierda Socialista; uno del Frente Progresista de Santa Fe, Luis Contingiani, y dos de Unidad y Equidad, que preside José Luis Ramón.  
  
Además, sumó a dos diputados radicales por Mendoza, Claudia Najul y Federico Zamarbide, que según fuentes de Juntos por el Cambio "se cortaron solos" en respaldo del tratamiento de leyes vinculadas a beneficios para la vitivinicultura incluidos en la sesión.  
  
También, aportaron al número el diputado de Juntos Somos Río Negro, Luis Di Giácomo; los misioneros Diego Sartori, Flavia Morales y Ricardo Wellbach; el justicialista salteño Andrés Zottos, además de Esteban Bogdanich (FdT) y Pablo Ansaloni (UyEF), que llegaron tarde y lograron sumar un total de 124 diputados presentes.  
  
Luego de pasar lista de presentes y ausentes, Massa habilitó las expresiones en minoría de los diputados en el recinto.  
  
Al hablar en la sesión en minoría, el jefe del bloque de diputados del Frente de Todos, Máximo Kirchner, cuestionó a la oposición por no dar quórum en el recinto y acusó a ese espacio de buscar "hacernos actuar bajo extorsión", a la vez que les pidió que "deberían ser más pacientes" porque "para el 2023 faltan dos años, y eso también es la República".  
  
Faltaron 7 diputados para lograr el quorum parlamentario. (Foto: Prensa Diputados).

"Han vuelto a comportarse como siempre fueron cuando le tocó ser gobierno: suprimir a quienes pensaban diferentes, perseguirlos. No soportan la democracia a veces porque esta ley, por ejemplo, lo que hace no es ni siquiera prohibir el azúcar sino poder ser conscientes de qué consumimos, de cómo nos alimentamos", aseveró Máximo Kirchner en el recinto de la Cámara baja, en el marco de una sesión en minoría tras la decisión de la oposición de no dar quórum para el debate.  
  
Tras destacar la presencia de "los 122 diputadas y diputados", Máximo Kirchner puso de relieve que "hoy quedamos a siete votos no de dejar de ser una República sino de poder mejorar la calidad de vida de nuestra gente" y afirmó que "en medio de un proceso electoral lo que podemos ver hoy es que eso no sucede" por parte de la oposición.  
  
  
"Nuestro bloque lo habló con las organizaciones de etiquetado. Acordamos no traer otros temas que le interesa al bloque para no traer rispideces, lejos de querer sacar provecho. Hasta ese cuidado tuvimos de no provocar a nadie de no ser chabacanos, y chabacanas en la oposición", aseveró el legislador, al manifestar que esa bancada tiene "la mejor de las voluntades que parece que no vamos a poder solucionar a las 13.30".  
  
El primero en pedir la palabra en el recinto fue el jefe del interbloque Unidad y Equidad Federal, José Luis Ramón, quien aseguró que "ha quedado demostrado que en relación al tratamiento de los proyectos que van en beneficio de las familias versus aquellos que representan intereses de esos pequeños grupos que no quieren que nada cambie".  
  
También, de la Izquierda Socialista, Juan Carlos Giordano (FIT), aseguró: "Aplaudimos al etiquetado frontal pero se tendrian que votar leyes para combatir el hambre y la pobreza" y lamentó que "no se hayan incorporado al temario la ley de protección de humedales que no está y eso si es combatir a la derecha, un paso para evitar la destrucción ambiental".  
  
La sesión en minoría, de todos modos, contó con la presencia de organizaciones de consumidores en los palcos, a los cuales Massa les pidió disculpas por la falta de quorum y pidió un aplauso para los representantes de esas entidades que estuvieron para "acompañar la voluntad de este cuerpo de debatir".  
  
  
Al término de la reunión de la Comisión de Labor Parlamentaria, Negri había anticipado que ese espacio junto con otras bancadas no bajarían al recinto porque no estaban de acuerdo con la forma en que el oficialismo había realizado la convocatoria y dijo que a las 13.30 se realizaría una nueva reunión con los jefes de bloques para intentar ampliar el temario de la sesión, al sostener que "se debe sesionar hoy mismo",.  
  
"No queremos obstaculizar el debate pero tampoco queremos que nos atropellen. Estamos de acuerdo con el temario pero el oficialismo decidió no hacer lugar a nuestro planteo de ampliar el temario", afirmó Negri, en una conferencia que ofreció en el salón de Pasos Perdidos.  
  
También, en ese sentido, se expresó el jefe del interbloque Consenso Federal, Alejandro Topo Rodríguez, manifestó la "firme expectativa del bloque de sesionar" aunque cuestionó a "los bloques mayoritarios" que, dijo, "llegan a acuerdos y no nos convocan al diálogo y nos arrastran a la pelea".

El proyecto de etiquetado frontal fija pautas para una alimentación saludable, al obligar a la colocación de una serie de sellos frontales en los envases de alimentos.

  
Antes de dar por levantada la sesión, Massa pidió disculpas a integrantes de organizaciones de la sociedad civil presentes en los palcos para escuchar el debate porque "desgraciadamente la ley no pudo ser tratada", a instancias del arco opositor que no dio quórum.  
  
La sesión -convocada para las 11- había sido solicitada por la bancada del Frente de Todos, que encabeza Máximo Kirchner, para debatir un conjunto de iniciativas consensuadas con la mayoría de los bloques después de tres meses sin deliberar, debido a la campaña electoral y a la falta de acuerdo entre los distintos espacios parlamentarios.  
  
El temario de la convocatoria impulsada por el oficialismo incluía, además, otras iniciativas, como la que propone políticas transversales para las personas en situación de calle y otro referido a medidas vinculadas con la actividad de la vitivinicultura.