---
category: La Ciudad
date: 2022-01-09T06:00:27-03:00
thumbnail: https://assets.3dnoticias.com.ar/cuidaio.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: El Cudaio quiere alcanzar niveles de donación prepandemia y plantea una "alianza
  estratégica"
title: El Cudaio quiere alcanzar niveles de donación prepandemia y plantea una "alianza
  estratégica"
entradilla: 'El Centro Único de Donación, Ablación e Implante de Órganos se propone
  en 2022 alcanzar los niveles de procuración similares a los de hace dos años.

'

---
El 2022 dejó un total de 62 procesos de ablación en la provincia de Santa Fe, contra 46 del año 2021; lo que representa un crecimiento del 35% en donantes de órganos. Desde el Cudaio destacaron "el impacto que tuvieron esas donaciones en cantidad de trasplantes posibilitados, ya que se incrementó de 125 trasplantes en 2020 a 171 en 2021, es decir, un 37% más”.

A pesar de la suba registrada, la procuración de órganos sigue estando por debajo de los niveles de años prepandemia. De igual forma, las autoridades del organismo subrayaron el esfuerzo realizado durante el tiempo de pandemia.

El Dr. Armando Mario Perichón, director del Cudaio destacó que "el esquipo se puso al hombro tratar de superar valores del 2020 y se logró. Es lo más importante". Graficó que "con los valores del 2021, volvimos al 2016".

Según datos oficiales, en Santa Fe hay 549 pacientes en lista lista de espera, de los cuales 358 aguardan por un transplante renal. "Es incomparable analizar este escenario con años anteriores. España, en la primera ola la procuración cayó el 82 por ciento la procuración. A nivel nacional en 2020, Argentina cayó un 56 por ciento y la provincia de Santa fe un 40 por ciento", le comentó Perichón a UNO Santa Fe.

Recordó que "las terapias estaban ocupadas por los pacientes Covid y no podían donar. Entonces, no había manera de conseguir donantes porque no estaban en las terapias. Nosotros buscamos los donantes en las UTI; y había hospitales que eran solamente Covid".

En este contexto es que el Cudaio se propone para este 2022 recuperar todo lo que se perdió durante la pandemia. "Esperemos que se termine (en referencia a la pandemia) lo más rápido posible para volver a los valores prepandémicos", dijo Perichón.

Para salvar y mejorar la calidad de vida a través de la ablación de organos, el responsable del Cudaio planteo la necesidad de formar una "alianza estratégica", y planteó tres aspectos: "Un sistema de procuración eficiente (por eso tenemos que modificar la ley en Santa Fe), una campaña de concientización de la gente y el apoyo de los medios de comunicación".

"Esa es la forma", insistió. Para Perichon "una alianza entre medios de comunicación, comunidad donante y un sistema de procuración eficiente, pueden cambiar toda la historia"

"España cambió la historia de esa manera. Es el mayor procurador de órganos del mundo, lo hizo a través de esa alianza que generó estratégicamente. Lo pudieron hacer y hoy es el modelo a tener en cuenta", graficó.

Con relación a la iniciativa de modificar la ley provincial, explicó: "Es del año 95, quedó obsoleta y la pandemia mostró algunas dificultades para poder trabajar. Hemos tenido varias reuniones, con el oficialismo y oposición, para tener un consenso y modificar algunos de los artículos de la ley".

Y profundizó: "Nuestra particularidad es absoluta. Como sistema sanitario no trabajamos con vivos, trabajamos con muertos a los cuales tenemos que sacar los órganos en un período de tiempo corto para que puedan ser utilizados en el trasplante. Eso hace que necesitemos una celeridad en las decisiones que no lo tiene otra parte del sistema sanitario. Creo que tenemos que adecuar la ley para poder disponer de los tiempos. La justicia lo hizo, la ley Justina le dio términos a la justicia para expedirse".

**¿Como ser donante?**

En Argentina, todos los ciudadanos mayores de 18 años tienen el derecho de manifestar su voluntad en relación a la donación de órganos y tejidos.

Para hacerlo, Incucai ofrece un formulario al que se puede acceder y completar. De esta manera, formarás parte del Registro Nacional del Donante, listado en el que se encuentran todos los argentinos que ya se expresaron.

La expresión de voluntad, sea negativa o afirmativa, puede ser modificada en cualquier momento. El formulario se puede completar a través de la App Mi Argentina. Para acceder, hacé click [**_ACÁ_**](https://www.argentina.gob.ar/miargentina/servicios/credencial-donar-organos-digital)