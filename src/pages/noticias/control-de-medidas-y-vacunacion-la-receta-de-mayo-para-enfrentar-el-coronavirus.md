---
category: Agenda Ciudadana
date: 2021-05-02T06:15:40-03:00
thumbnail: https://assets.3dnoticias.com.ar/dnu.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: 'Control de medidas y vacunación: la receta de mayo para enfrentar el coronavirus'
title: 'Control de medidas y vacunación: la receta de mayo para enfrentar el coronavirus'
entradilla: El Gobierno nacional apuesta a que con las nuevas medidas se refuercen
  los controles de las restricciones, para reducir la circulación y bajar la tensión
  del sistema de salud.

---
El Gobierno nacional apuesta a que con las nuevas medidas anunciadas por el presidente Alberto Fernández, que entraron en vigencia este sábado con su publicación en el Boletín Oficial y se extenderán hasta el 21 de mayo, se refuercen los controles de las restricciones, para reducir la circulación y bajar la tensión del sistema de salud, a la par de que avance el proceso de vacunación de la población de riesgo.

Con el acento puesto en el control de las medidas territorialmente "focalizadas" y apostando a cubrir con la vacunación a la mayor cantidad de personas de grupos de riesgo, el Gobierno encara mayo y los primeros fríos con la esperanza de contener la circulación viral que ya pone en jaque al sistema sanitario de diversas zonas del país.

En las últimas semanas de abril se confirmó la llegada de la segunda ola de contagios al país, con cifras altas de casos y muertes, sobre todo en el Área Metropolitana Buenos Aires (AMBA) pero también con números récord de vacunas llegadas a Ezeiza, superando 11 millones de dosis ya en el país.

El correlato político de la pandemia incluyó, en los últimos días, impugnaciones judiciales aún irresueltas en la Corte Suprema por el tema de las clases presenciales.

Desde el primer minuto de este sábado se encuentra vigente un nuevo Decreto de Necesidad y Urgencia presidencial que dispone medidas focalizadas para diversas zonas de riesgo epidemiológico con la atención puesta en las áreas de “alerta” en las que se percibe una alta circulación viral y el consiguiente estrés del sistema sanitario.

En ese grupo, donde se establecen las medidas más rígidas para disminuir la circulación de personas, se encuentran más de 40 distritos bonaerenses, la Ciudad de Buenos Aires, el Gran Mendoza, Rosario y parte de su conurbano.

El DNU establece, entre otros puntos, horarios de cierre para locales comerciales y modos de comercialización específicos para el sector gastronómico, pero es de máxima relevancia la suspensión de las “clases presenciales y las actividades educativas no escolares presenciales en todos los niveles”.

El Gobierno nacional ya tomó buena parte de estas medidas hace dos semanas para el AMBA y entiende que fueron fundamentales para lograr que la curva de casos se desacelere y baje levemente, aun cuando su acatamiento fue hace 15 días rechazado por la administración de Horacio Rodríguez Larreta.

En aquella oportunidad, el dirigente de Juntos por el Cambio cuestionó ante la Corte Suprema la constitucionalidad del decreto presidencial y se valió de un fallo de la Justicia local para mantener la presencialidad educativa.

La premura del escenario pandémico no alteró sin embargo los ritmos judiciales de un máximo tribunal que habrá de emitir su opinión -en algún momento- sobre un DNU que ya se encuentra vencido y que fue sucedido por otro con características similares, aunque no idénticas, al anterior.

Las medidas anunciadas el viernes por el presidente Fernández fueron precedidas esta vez por una ronda de consultas entre expertos, funcionarios de diversas áreas y jurisdicciones y por dos videoconferencias entre el Jefe de Estado, los gobernadores provinciales y el jefe de Gobierno porteño.

**Las medidas en la Ciudad de Buenos Aires**

En la segunda de esas reuniones virtuales celebrada el miércoles, Rodríguez Larreta reconoció la efectividad de las medidas tomadas por la Casa Rosada, manifestó su preocupación por una meseta “alta” que genera tensión en el sistema de salud y dijo estar de acuerdo en “reforzar las medidas” para restringir la movilidad.

Sin embargo, luego del anuncio presidencial en el que se volvió a incluir la suspensión de clases presenciales “para reducir la circulación” de personas, el jefe de Gobierno de la Ciudad, volvió a rechazar la medida.

En ese sentido, Rodríguez Larreta mantendrá para las próximas semanas las clases presenciales en el nivel inicial y primario, mientras para el secundario se aplicará un sistema bimodal, que combine la virtualidad con la presencialidad.

Al anunciarlo, el jefe del Ejecutivo porteño insistió en la idea de que las aulas no son focos de contagio, que deben mantenerse abiertas ya que "son fundamentales en la formación de los chicos” y estimó que, en todo caso, los adolescentes son los que mejor pueden adaptarse a la educación remota ya que tienen “mayor autonomía y son los que más usan el transporte público para ir a la escuela”.

**El impacto en las provincias**

A su vez, el gobernador bonaerense Axel Kicillof dijo que la provincia adhiere al DNU presidencial, al tiempo que anunció una inversión de 70 mil millones de pesos destinadas a reforzar la contención social y productiva de los sectores afectados por las medidas.

En Mendoza, el gobernador Rodolfo Suárez anunció este sábado que convocará a los intendentes para debatir sobre la aplicación de las medidas dictadas por la Casa Rosada.

Omar Perotti también brindó el sábado su discurso ante el parlamento santafesino haciendo inevitable referencia a la difícil situación que genera la pandemia en todos los ámbitos, pero recién el domingo vencerá el decreto provincial que establece cómo se aplican las medidas en el territorio y deberá esperarse por uno nuevo.

Por ello, luego de la judicialización porteña y los cuestionamientos opositores de los últimos días, el Presidente no sólo aclaró que tomó y seguirá tomando “las decisiones que correspondan sin ninguna especulación política”, sino que anunció el envío de un proyecto de ley que establezca criterios epidemiológicos claros que permitan tomar decisiones concretas.

Fernández también retomó ayer una idea que había dejado de repetirse pero que volvió a tener actualidad: “El tiempo que ganamos con la prevención lo usamos para vacunar”.

En el “haber” de abril se podrá contar el avance para fabricar la vacuna Sputnik V en la provincia de Buenos Aires y haber superado casi en un 20 por ciento el objetivo mínimo de 4 millones de dosis mensuales del que se habla en Casa Rosada.

Mientras se negocia con diversos laboratorios para llegar a nuevos acuerdos, distintas oficinas del gobierno trabajan sobre la carpeta de AstraZeneca, una empresa que se comprometió a entregar más de 20 millones de dosis al país, que fabrica el componente activo de sus vacunas en Argentina, y que no cumplió aún con los plazos estipulados.