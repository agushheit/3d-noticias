---
category: Agenda Ciudadana
date: 2021-08-02T06:00:36-03:00
thumbnail: https://assets.3dnoticias.com.ar/FERIADOSAGOSTO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'Feriados: ¿hay fin de semana largo en agosto?'
title: 'Feriados: ¿hay fin de semana largo en agosto?'
entradilla: 'El 17 de agosto, se conmemora el Día del Paso a la Inmortalidad del General
  José de San Martín, y este año cae martes. '

---
Al tratarse de una de las fechas denominadas como “trasladable”, el día feriado se correrá al lunes 16, configurando de esta manera un fin de semana largo: sábado 14, domingo 15 y lunes 16 de agosto.

Al menos por las restricciones vigentes, se podrá viajar, siempre que se cumplan con los requisitos que tiene cada una de las provincias para hacerlo y se tomen las medidas de cuidado correspondientes ante la pandemia de Covid-19.

**¿Qué se conmemora el 17 de agosto?**

El General José de San Martín falleció el 17 de agosto de 1850 en la localidad francesa de Boulogne-sur-Mer a los 72 años. Conocido como el Libertador de América, se lo reconoce en la Argentina como el “Padre de la Patria”, en Perú con título de “Fundador de la Libertad del Perú”, “Fundador de la República” y “Generalísimo de las Armas”, mientras que en Chile su ejército lo ha destacado con el grado de Capitán General. San Martín fue el gran eje de las luchas antimonárquicas que terminaron de independizar a estos territorios del poder colonizador de España.