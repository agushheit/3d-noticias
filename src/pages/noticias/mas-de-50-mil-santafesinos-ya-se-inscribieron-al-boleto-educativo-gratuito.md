---
category: Agenda Ciudadana
date: 2021-02-22T06:00:13-03:00
thumbnail: https://assets.3dnoticias.com.ar/omarboleto.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Más de 50 mil santafesinos ya se inscribieron al Boleto Educativo Gratuito
title: Más de 50 mil santafesinos ya se inscribieron al Boleto Educativo Gratuito
entradilla: 'A pocas horas de habilitada la página web para anotarse en el sistema,
  las ciudades de Rosario y Santa Fe fueron las que recibieron el mayor número de
  interesados. '

---
Más de 50 mil santafesinos ya se inscribieron en las primeras 48 horas al Boleto Educativo Gratuito lanzado este jueves por el gobierno de la provincia de Santa Fe, y que tiene por objetivo asegurar la concurrencia de estudiantes, docentes y asistentes escolares a todos los establecimientos de la provincia, eliminando así una potencial barrera de acceso a la educación.

La inversión del Estado será de 4.500 millones de pesos por año y con potencialidad de alcanzar a unos 500.000 beneficiarios. Entre las localidades que más inscriptos registraron en las primeras horas de habilitado el sistema se encuentran Rosario con más de 20 mil y Santa Fe con más de 6 mil anotados. El resto de las localidades del territorio provincial aportó la diferencia para que llegue al número que muestra la aceptación del programa que comenzará a regir a partir del 15 de marzo.