---
category: La Ciudad
date: 2021-10-14T06:00:07-03:00
thumbnail: https://assets.3dnoticias.com.ar/MORATORIA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Este viernes vence la moratoria municipal 2021
title: Este viernes vence la moratoria municipal 2021
entradilla: "Los contribuyentes que quieran adherir tendrán tiempo hasta el viernes
  para acogerse al Régimen de Regularización de Derechos.\n\n"

---
La Municipalidad recuerda que este viernes concluye el plazo para adherir al Régimen de regularización de tributos, dispuesto en la Ordenanza N° 12.771. De este modo, los contribuyentes atrasados en el pago de sus impuestos tienen la posibilidad de ponerse al día con una importante reducción de su carga fiscal.

 Desde la Secretaría de Hacienda del municipio, se recordó qué tributos se pueden abonar, en el marco de la moratoria: Tasa General de Inmuebles (TGI), Derecho de Registro e Inspección (DREI), Contribuciones por mejoras, Derecho de Cementerio y otros mencionados en el artículo N° 10 de la Ordenanza.

 Para gestionar los planes pueden acceder en la web oficial www.santafeciudad.gov.ar/moratoria2021, donde encontraran los datos y link para realizar los trámites. También puede abonarse y gestionar los planes, en el Palacio Municipal, de 7:30 a 13 horas, para lo cual se recomienda obtener un turno en [https://santafeciudad.gov.ar/turnos-online-para-gestiones-municipales/.](https://santafeciudad.gov.ar/turnos-online-para-gestiones-municipales/. "https://santafeciudad.gov.ar/turnos-online-para-gestiones-municipales/.")

  **Beneficios**

 Para deudas contraídas entre el 01/04/2020 y el 30/04/2021, los intereses por mora son descontados en un 100% y se podrá financiar en planes de hasta 24 cuotas sin interés de financiación, a tasa 0%.

 En tanto, para deudas generadas hasta el 31/03/2020, se ofrece una reducción de la tasa de interés del 2% mensual y las opciones para cancelar son:

 1) Pago contado, con un descuento del 90% de los intereses por mora

 2) Planes de pago:

 Hasta 12 cuotas, con un descuento del 50% de intereses por mora y sin interés de financiación

 Hasta 24 cuotas, con un descuento del 30% de intereses por mora y un interés de financiación del 1,5% mensual

 Hasta 36 cuotas, sin reducción de intereses por mora y un interés de financiación del 2% mensual