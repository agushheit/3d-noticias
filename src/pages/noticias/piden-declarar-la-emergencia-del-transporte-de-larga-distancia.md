---
category: Agenda Ciudadana
date: 2021-02-03T07:10:36-03:00
thumbnail: https://assets.3dnoticias.com.ar/transporte-interurbano.webp
layout: Noticia con imagen
link_del_video: https://youtu.be/jEEkIRpZR1I
author: 3D Noticias
resumen: Piden declarar la emergencia del transporte de Larga Distancia
title: Piden declarar la emergencia del transporte de Larga Distancia
entradilla: Gustavo Gaona, vocero de la Cámara de Empresas de Larga Distancia (CELADI)
  habló de la situación del transporte de larga distancia, la quita de subsidios y
  pidió que se declare la emergencia en el sector.

---
La situación del transporte público de pasajeros de larga distancia viene en crisis desde antes de la pandemia.

Durante el 2020, el parate de la actividad obligó a las empresas a "licenciar" a más de 16 mil trabajadores en todo el país.

Gustavo Gaona, vocero de la Cámara de Empresas de Larga Distancia (CELADI), habló de la situación del transporte de larga distancia, la quita de subsidios y pidió que se declare la emergencia en el sector.

[![Mira el video](https://assets.3dnoticias.com.ar/entrevista-celadi.webp)](https://youtu.be/jEEkIRpZR1I "Reproducir")

Mirá la nota en nuestro canal de [youtube](https://youtu.be/jEEkIRpZR1I)