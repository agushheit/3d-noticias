---
category: Estado Real
date: 2021-09-09T06:00:51-03:00
thumbnail: https://assets.3dnoticias.com.ar/AGENTES.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Provincia y Nación anunciaron el desembarco de más agentes de fuerzas federales
  en la ciudad de Rosario
title: Provincia y Nación anunciaron el desembarco de más agentes de fuerzas federales
  en la ciudad de Rosario
entradilla: El gobernador de la provincia aseguró este miércoles que “no hay cobertura
  ni impunidad para nadie”, tras encabezar una reunión de trabajo con fuerzas de seguridad.

---
El gobernador Omar Perotti encabezó este miércoles una reunión de trabajo junto con el ministro de Seguridad de Santa Fe, Jorge Lagna, en el que se anunció el arribo de 160 nuevos agentes de fuerzas federales en Rosario. Durante el encuentro, del que participó también el subsecretario de Intervención Federal de Nación, Luis Morales, se analizó además la actual situación de seguridad en la provincia y se planificaron diversos operativos conjuntos de intervención en la mencionada ciudad.

Tras el cónclave, y en conferencia de prensa, el gobernador Perotti explicó que “hay una decisión tomada, que es cortar los vínculos con el delito, situación que no pasaba, que nos lleva a que haya un cambio profundo, un cambio que todavía en lo diario no tiene la dimensión que deseamos. Ustedes observan y ven cosas que no pasaban; es una bola de nieve que fue creciendo y nosotros tomamos la decisión de empezar a achicarla”.

En esa línea, dijo que “la decisión está tomada y no tiene vuelta atrás; no hay impunidad ni privilegios para nadie”. Y subrayó el fuerte trabajo que se viene realizando en materia de seguridad: “El número de detenidos lleva hoy a que tengamos comisarías sobrepasadas en cantidad de detenidos porque el Servicio Penitenciario ya no alcanza. Claramente habla de un trabajo diferente. La inversión fuerte en el sistema penitenciario tiene que ver con eso: acortar los plazos, ampliar cada una de las dependencias y construir alcaidías donde no había, precisamente para que no haya impunidad”.

“Tener una acción de prevención mayor es lo que nos está faltando”, afirmó el mandatario, y explicó que “eso se consigue con más agentes y más vehículos en la calle, con más tecnología, con mayor coordinación con las fuerzas federales; sin dudas que es el camino y no tiene vuelta atrás”.

A continuación, Perotti remarcó que “no es fácil, ni gratis”, pero “hay un compromiso firme”, y agregó que “haber mirado tantos años para otro lado, permite que algunas bandas se hayan convertido en organizaciones criminales y esto es lo que enfrentamos y no lo ocultamos; no barremos debajo de la alfombra”.

Finalmente, recordó que, al inicio de su gestión, se enviaron a la Legislatura provincial una serie de proyectos en materia de seguridad y no fueron aprobados: “Me alegra que algunos legisladores estén planteando reuniones, convocatorias. Me hubiese gustado que lo hicieran desde el momento que mandamos leyes para mejorar la seguridad en la provincia, que tengamos una declaración de emergencia en seguridad que fue negada”.

**Operativos conjuntos**

Por su parte, Lagna explicó que “el juicio por las balaceras del año 2018 nos viene insumido grandes recursos humanos y materiales: más de 300 hombres y 70 vehículos, no solo para garantizar la seguridad de Centro de Justicia Penal, sino el traslado permanente de agentes de la justicia y testigos”.

“Eso ha llevado también a que las custodias fijas o dinámicas que la Justicia prevé sobre objetivos determinados superen las 120; eso es mucho personal y equipamiento, más de 100 patrulleros abocados a esa tarea puntual”, agregó.

Luego, explicó que el cuadro de situación hizo que se “repensaran las modalidades operativas de trabajo”, y destacó la llegada a Rosario de 160 agentes federales: “En un principio planeamos operativos con fuerte presencia de fuerzas federales en el centro y el macrocentro. Hoy la realidad indica que tenemos que tener una presencia más activa en los principales barrios de la ciudad, en los más azotados por violencias altamente lesivas”.

A su turno, Morales detalló que “desde que asumió esta gestión, la intención fue que en Rosario había que generar un despliegue de políticas de seguridad diferente al que se venía aplicando. En función de eso se diagramaron de nuevo, cuestiones en cuanto a la complementación de las tres instancias del Estado como premisa, y también el aumento cuantitativo y cualitativo de fuerzas federales en complementación con la policía de la provincia de Santa Fe”.

Y anunció: “Hoy están empezando a operar los 160 efectivos que se habían anunciado el último viernes, con epicentro de fuerzas especiales, tanto de Gendarmería como de Prefectura, el grupo Albatros puntualmente, que tiene su epicentro en el Centro de Justicia Penal. En tanto que el resto de los efectivos va a estar llevando adelante un despliegue y operativos tanto en la zona céntrica de Rosario como en los barrios”.