---
category: Estado Real
date: 2022-01-12T06:00:38-03:00
thumbnail: https://assets.3dnoticias.com.ar/subidonelectrico.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La provincia de Santa Fe registró un nuevo récord histórico de demanda eléctrica
title: La provincia de Santa Fe registró un nuevo récord histórico de demanda eléctrica
entradilla: "A las 14.05 de este martes, la Empresa Provincial de la Energía marcó
  2.470 MW, superando la que se había anotado en febrero de 2019, que fue de 2.446
  MW.\n\n"

---
La Empresa Provincial de la Energía informó que este martes se registró un nuevo récord de demanda de potencia en el sistema eléctrico santafesino, alcanzando 2.470 MW a las 14.05 hs.

Estos niveles se dan en un contexto de continuidad de jornadas de temperaturas extremas que exigen las instalaciones que conforman el parque eléctrico provincial: 75 estaciones transformadoras, más de 56.000 kilómetros de redes de alta, media y baja tensión y más de 20.000 centros de transformación urbanos y rurales, entre otros.

La EPE destaca la labor que desarrolla todo su personal, que redobla sus esfuerzos para atender cualquier situación que se origine en el servicio. Cabe destacar que hoy se encuentran aislados por protocolo Covid, 372 agentes en toda la provincia.

A las 15.15, el Gran Santa Fe también anotó un nuevo récord histórico de demanda de potencia con 340,6 MW superando el del año 2019, que había sido de 336 MW.

**Consejos – Uso eficiente**

En este marco, la EPE insiste con el aporte que pueden realizar los usuarios con la utilización eficiente del servicio:

>>Usar los equipos de aire acondicionado a no menos de 24°. Cada grado por debajo de esa marca, produce un consumo mayor de electricidad, de entre 5 y 10 %.

>>Limpiar periódicamente los filtros de los equipos acondicionadores de aire y vigilar el mantenimiento del mismo.

>>Mantener cerradas las aberturas de los ambientes con aire acondicionado y evitar fugas que derivan en un derroche de energía y, por lo tanto, de consumo.

>>Utilizar lámparas led.

>>Reducir consumos ociosos de electricidad.

>>Tener en cuenta el etiquetado de eficiencia energética de aires acondicionados, heladeras y lavarropas al momento de adquirirlos. Las más eficientes son de clase A. A mejor clase, menor consumo.