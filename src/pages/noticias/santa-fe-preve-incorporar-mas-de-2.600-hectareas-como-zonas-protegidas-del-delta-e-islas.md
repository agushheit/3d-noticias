---
category: La Ciudad
date: 2021-01-03T10:26:30Z
thumbnail: https://assets.3dnoticias.com.ar/03012021-regenera-sta-fe.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: El Litoral'
resumen: Santa Fe prevé incorporar más de 2.600 hectáreas como zonas protegidas del
  delta e islas
title: Santa Fe prevé incorporar más de 2.600 hectáreas como zonas protegidas del
  delta e islas
entradilla: La iniciativa #RegeneraSantaFe  propone un esquema estratégico de ampliación
  e incorporación de áreas protegidas en zonas del Delta e Islas del río Paraná, para
  su protección y conservación.

---
«El Delta del Paraná es un humedal que en los últimos tiempos ha sufrido una importante degradación a causa de los incendios, por su importancia y por los múltiples valores ecosistémicos que brinda, creemos fundamental incorporar áreas protegidas al sistema provincial, haciendo foco exclusivo, en este caso, en el río Paraná y sus islas», detalló la ministra de Ambiente y Cambio Climático, Erika Gonnet.

Los humedales se encuentran entre los ecosistemas más productivos del planeta. La intervención del hombre y las circunstancias climáticas globales afectaron enormemente su capacidad y riqueza, y su conservación se presenta hoy como uno de los mayores desafíos a nivel mundial, explicaron desde la cartera provincial.

La propuesta provincial proyecta su mirada a lo largo de la zona costera del Paraná: «Sobre la base del desarrollo provincial propuesto por el gobernador Perotti, esta etapa incluye la incorporación de más de 2.600 hectáreas que comprenden a la Isla La Fuente en Reconquista, la Isla Sabino Corsi frente a Rosario, la Isla Pereyra en Villa Constitución, la Isla Los Mástiles frente a Granadero Baigorria y Capitán Bermúdez, y el Islote perteneciente a la localidad de General Lagos», detalló la funcionaria.

En ese sentido, Gonnet puso en valor «la ampliación de 1.000 hectáreas de la zona protegida de la Isla El Pelado, en la región de islas frente a Puerto Gaboto, lo que amplía considerablemente el área protegida que contempla al Parque Nacional Islas de Santa Fe». Al respecto, resaltó: que «desde el gobierno de Santa Fe estamos desarrollando una propuesta de ampliación para el Parque Nacional de nuestra provincia».

Estos espacios son estratégicos para fortalecer e impulsar acciones de conservación y preservación de los recursos naturales de la provincia. Además, se indicó, generan múltiples beneficios vinculados a la revalorización de la cultura, la identidad local, y la generación de empleo, respetando la vida de los isleños y sus costumbres.

«La iniciativa, que ya estamos poniendo en marcha administrativamente desde el ministerio, contempla el impulso de la diversificación económica y el desarrollo ecoturístico de la región», continuó la titular de la cartera de Medio Ambiente. 

<span style="color: #EB0202;"> **#RegeneraSantaFe** </span> promueve actividades productivas sostenibles; desarrollo de capacidades técnicas; diseño de espacios de aprendizaje y programas de capacitación para el desarrollo o mejora de capacidades; diseño de planes GIRSU y promoción de la economía circular; desarrollo de infraestructura para el ecoturismo.

La diversificación económica, la actividad turística y la conservación son pilares del desarrollo local, y al mismo tiempo, significan comprender que el verdadero desarrollo es aquel que respeta a la naturaleza y a su comunidad, promoviendo el cuidado, los valores y las características propias que son parte de nuestra identidad.

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.1em; font-weight: 700"> Cuidados del medio ambiente</span>**

La iniciativa <span style="color: #EB0202;"> **#RegeneraSantaFe** </span> propone, a través de soluciones inspiradas en la naturaleza, un desafío común basado en los cuidados de la vida, el ambiente, los bienes naturales, las relaciones entre seres vivos y los vínculos interpersonales.

«Además, <span style="color: #EB0202;"> **#RegeneraSantaFe** </span> brinda la posibilidad de, producto del consenso, coordinar con diferentes actores para avanzar de forma holística en una acción climática», concluyó la funcionaria.