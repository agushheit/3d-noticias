---
category: La Ciudad
date: 2021-04-10T06:15:45-03:00
thumbnail: https://assets.3dnoticias.com.ar/MUNICOVID.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Municipalidad cumplirá las medidas por Covid anunciadas por nación y provincia
title: La Municipalidad cumplirá las medidas por Covid anunciadas por nación y provincia
entradilla: Funcionarios de la Municipalidad detallaron cómo serán las nuevas medidas
  dispuestas en la ciudad en consonancia con lo dictaminado por la Provincia y la
  Nación.

---
Virginia Coudannes, secretaria de Control y Convivencia Ciudadana y Matías Schmüth, secretario de Producción y Desarrollo Económico de la Municipalidad de Santa Fe, brindaron esta mañana una conferencia de prensa para detallar el alcance de las medidas adoptadas en la ciudad hasta el 30 de abril, en cumplimiento de lo estipulado por la reciente normativa nacional (DNU 235/2021) y a lo anunciado por el gobierno provincial en las últimas horas, y que rige para todo el territorio santafesino.

En primer lugar, Schmüth anticipó: “A partir de las nuevas medidas decretadas por el gobierno nacional y acompañadas por el gobierno provincial, decidimos reunirnos con el sector gastronómico y, días previos, con otros sectores para ir evaluando las distintas situaciones que están atravesando. En particular, las modificaciones que se dan en los horarios de circulación hacen que el sector gastronómico vea reducido el horario en el que venía trabajando teniendo la posibilidad de hacerlo hasta las 0 horas y reduciendo el aforo al 30%, que es lo que se establece tanto en el decreto nacional como el provincial que adhiere al primero”.

“La ciudad de Santa Fe va a adherir plenamente a lo que establece Nación y Provincia. Iremos monitoreando cómo continúa la situación y vamos a trabajar mancomunadamente, tal como lo venimos haciendo con el gobierno provincial para tomar definiciones en conjunto. Hoy por hoy, vamos a tomar las mismas medidas y seguir los lineamientos de nación y provincia porque así lo estipulan los decretos respectivos”, afirmó el secretario de la Producción.

En ese sentido, el funcionario destacó: “Nos parecía oportuno previamente a estas normas empezar a reunirnos con los sectores para poder articular distintas acciones que nos permitan ir fortaleciendo y volviendo a tomar la importancia que tienen que tener los protocolos”. Y afirmó: “Estamos convencidos de que las actividades productivas se pueden desarrollar de manera cuidada. El año pasado adquirimos experiencia como para que estos protocolos se cumplan de manera adecuada, eso nos va a exigir desde el municipio fijar las capacidades y seguro vamos a articular distintas campañas de difusión y de concientización junto a los sectores privados con los que venimos trabajando en las distintas mesas de cada sector productivo”.

Con respecto a la responsabilidad social e individual frente a la pandemia, el funcionario amplió: “Hoy más que nunca tenemos que tomar la responsabilidad y entender que tenemos que enfrentar nuevamente la situación que nos toca atravesar con las medidas de cuidado; y cada uno de nosotros somos responsables de las mismas tanto el consumidor como el empresario”.

**Aforo y controles**

Una de las nuevas medidas que se tomarán en los locales gastronómicos están relacionadas con determinar cuál es la capacidad de cada uno de los locales. En tal sentido, Schmüth aclaró: “Desde la Municipalidad vamos a hacer una verificación en los locales para determinar las capacidades. Hemos acordado con el sector gastronómico que esas cifras queden reflejadas con cartelería obligatoria en los locales”. En ese marco, el funcionario añadió que “es importante en este momento el cuidado de cada uno y que pueda determinar si los locales cumplen con las medidas”.

Con respecto a las diferencias en los controles con respecto a la primera ola, Coudannes apeló al “aprendizaje que tenemos desde todos los sectores: en primer lugar, desde el sector estatal, pero también de las ciudadanas y ciudadanos, de que esto también depende de la responsabilidad individual y colectiva”. En consonancia, la funcionaria hizo referencia a los controles en la nocturnidad: “En los locales habilitados hay que hacer hincapié en la capacidad y el horario; esos son los ejes fundamentales”.

**Espacios abiertos**

Con relación a los controles, Coudannes destacó que se realizarán “en un trabajo articulado con la Policía, las Fuerzas de Seguridad, seguramente se generarán entramados con las Fuerzas Federales, en todo lo que tiene que ver con la circulación en los corredores y sobre todo en la disuasión en los espacios públicos”. En ese sentido, la funcionaria remarcó que, si bien los espacios públicos son competencia de la provincia, “nosotros también estamos acompañando y colaborando con diferentes dispositivos de control: de tránsito, de circulación y principalmente de nocturnidad”.

“Debemos hacer una especie de pacto entre todos los sectores para poder sobrellevar esta segunda ola de Covid-19”. En tal sentido, la funcionaria de la cartera de Control consignó: “Ayer tuvimos un encuentro con la jefa de la URI y con personal del Ministerio de Seguridad y con el secretario general de la Municipalidad, donde el principal planteo fue la disuasión en los espacios públicos, el ordenamiento, lo preventivo. La policía, con los nuevos agentes que tienen en la calle, está trabajando desde lo informativo y la prevención, eso nos parece estratégico y, desde nuestro lugar, poder acompañar acciones específicas sobre todo en espacios públicos donde sabemos que tienen más afluencia”.

**Transporte público**

Los funcionarios destacaron que es fundamental mantener la ventilación cruzada en los transportes públicos de pasajeros. “Un elemento fundamental en esta instancia es la aireación de los espacios, que tengan ventilación cruzada. Por tal motivo, vamos a trabajar con las puntas de línea, en la verificación y la sanitización de las unidades de colectivos. Asimismo, verificaremos la aireación, que las ventanillas estén abiertas, vamos a proveer unas suertes de fajas para que eso se pueda mantener”, ampliaron.

Por otra parte, en los diferentes recorridos, en puntos estratégicos de circulación de la ciudad, vamos a trabajar en la verificación y constatación de que las medidas se cumplan durante todo el recorrido. Además, “se llevarán adelante medidas preventivas en los lugares de mayor circulación y afluencia de pasajeros.