---
category: Agenda Ciudadana
date: 2021-05-12T09:28:46-03:00
thumbnail: https://assets.3dnoticias.com.ar/clases.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Diario Uno
resumen: Casi siete de cada 10 hogares santafesinos no cuenta con un buen acceso a
  Internet
title: Casi siete de cada 10 hogares santafesinos no cuenta con un buen acceso a Internet
entradilla: Así se desprende de un relevamiento sobre escolaridad a distancia en barrios
  populares de la ciudad de Santa Fe. Whatsapp es la principal herramienta de vínculo
  con la escuela durante la pandemia.

---
Un relevamiento sobre escolaridad a distancia y conectividad en los barrios populares de la ciudad de Santa Fe arrojó como resultado que casi 7 de cada 10 hogares no cuenta con un buen acceso a Internet, mientras que 3 de cada 4 alumnos consultados manifestó necesitar ayuda escolar por fuera de la escuela y la familia.

Los datos surgen de un Relevamiento Federal Educativo realizado en el marco de la campaña “Organización comunitaria en educación popular” llevado adelante por el Movimiento Evita. Según se desprende del trabajo, en la ciudad de Santa Fe apenas el 37,1% de los hogares cuenta con acceso fijo a Internet con buena señal, el 32,5% accede con datos móviles, 22,5% tiene problemas de conexión y el 7,9% no tiene acceso de ningún tipo.

![Un relevamiento sobre escolaridad a distancia en barrios populares de Santa Fe](https://media.unosantafe.com.ar/p/bc33383caf7dc8a6a0a0b5d9a10b5ace/adjuntos/204/imagenes/029/956/0029956053/placa3jpg.jpg?2021-05-11-11-03-58 =642x642)

“Estos datos nos muestran con crudeza las graves marcas que va a dejar la pandemia sobre los procesos educativos de estas generaciones y la necesidad de fortalecer el acompañamiento tanto estatal como comunitario a las familias que atraviesan situaciones más difíciles, donde la brecha digital se vuelve un problema ineludible”, explicó la diputada Lucila De Ponti.

Sobre este punto, el 76% de los consultados manifestó necesitar ayuda escolar por fuera de la escuela y la familia, tarea que se desarrolla en los espacios educativos comunitarios. Del mismo modo, el 66% de los alumnos aseguró que Whatsapp fue la principal herramienta de contacto con la escuela durante 2020.

![Conectividad y educación: datos de la ciudad de Santa Fe](https://media.unosantafe.com.ar/p/244d48736ee6fdb49e45b466725a4892/adjuntos/204/imagenes/029/956/0029956038/placa2jpg.jpg?2021-05-11-11-02-57 =642x642)

![Un relevamiento sobre escolaridad a distancia en barrios populares de Santa Fe](https://media.unosantafe.com.ar/p/72bc31b515b095dc4d675d8ca3ba565b/adjuntos/204/imagenes/029/956/0029956129/placa1jpg.jpg?2021-05-11-11-07-06 =642x642)

“La pandemia que más daño nos hace es la de la desigualdad, que se expresa de forma económica, educativa, de género, de oportunidades y de derechos. La Ley de Conectividad es una posibilidad concreta para empezar a saldar al menos uno de estos problemas, esperamos poder aprobarla rápidamente en la Cámara de Diputados para que Santa Fe Más Conectada empiece a ser una realidad”, aseguró.

Otro dato relevante del estudio es que las madres son mayoritariamente quienes más acompañan en las tareas para el hogar. Sobre este punto, Paula Canalis -referente del Movimiento Evita- insistió en la importancia de este aspecto. “El esfuerzo por sostener las trayectorias educativas fue asumido principalmente por las madres y en una situación de mucha precariedad por la falta de conectividad y de dispositivos electrónicos. El apoyo de los espacios comunitarios y las organizaciones sociales en el territorio también fue fundamental para sostener la vinculación educativa en los sectores vulnerados”, finalizó.

![Un relevamiento sobre escolaridad a distancia en barrios populares de Santa Fe](https://media.unosantafe.com.ar/p/ef33c13f0dc52fe9d0b22d3845021518/adjuntos/204/imagenes/029/956/0029956083/placa0jpg.jpg?2021-05-11-11-05-17 =642x642)

**Datos a Nivel Nacional**

Comparando los datos a nivel país, el estudio muestra que en la Argentina apenas el 22,4% de los hogares tiene conexión fija a Internet con buena señal, mientras que un 18,3% no tiene acceso de ningún tipo.

Del mismo trabajo se desprende que el 67,2% de los hogares comparte entre los miembros de la familia un mismo dispositivo tecnológico para acceder (teléfono, tablet o computadora) y el 55,1% de los consultados necesitó de ayuda educativa externa a la familia y la escuela.