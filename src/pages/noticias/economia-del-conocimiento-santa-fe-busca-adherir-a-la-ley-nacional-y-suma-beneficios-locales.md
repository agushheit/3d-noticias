---
category: Estado Real
date: 2020-12-23T09:45:31Z
thumbnail: https://assets.3dnoticias.com.ar/2312tecno.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: 'Economía del Conocimiento: Santa Fe busca adherir a la ley nacional'
title: 'Economía del Conocimiento: Santa Fe busca adherir a la ley nacional y suma
  beneficios locales'
entradilla: Se reglamentó la reducción de 5% a 0% de la tasa que pagaban empresas
  del sector del conocimiento como derecho de exportación. El gobierno local promueve
  la creación de más incentivos para compañías provinciales.

---
Tras la reglamentación de la Ley de Economía del Conocimiento a nivel nacional, la provincia de Santa Fe expresó la intención de adherir a dicha normativa y sumar, a través de la Ley Impositiva, beneficios adicionales para los próximos 10 años.

Si bien la implementación operativa para acceder a los beneficios de la Ley será emitida en resoluciones complementarias por la Subsecretaría de Economía del Conocimiento, funcionarios locales celebraron este paso y aseguraron que seguirán impulsando proyectos y programas para que las empresas del sector crezcan en la provincia.

La Ley de Economía del Conocimiento ofrece beneficios fiscales para fomentar las actividades del sector, como la reducción de manera segmentada del Impuesto a las Ganancias según el tamaño de la empresa y una rebaja de hasta 70% en las contribuciones patronales. Asimismo, para aquellas compañías que se inscriban en el Registro Nacional de Beneficiarios del Régimen de Promoción de la Economía del Conocimiento, reduce a 0% la tasa que pagaban como derecho de exportación.

«La eliminación de los derechos de exportación era uno de los principales reclamos del sector, que en Santa Fe tiene mucho potencial de desarrollo. Esta medida es un gran paso para fomentar la creación de puestos de trabajo de calidad, promover el aumento de las exportaciones y, como consecuencia, el ingreso de divisas», remarcó la secretaria de Ciencia, Tecnología e Innovación, Marina Baima.

## **Beneficios impositivos provinciales**

En el marco del proyecto de Ley Impositiva, se sumaron incentivos locales para las empresas de software, biotecnología, bioeconomía, servicios informáticos y digitales, producción audiovisual, nanotecnología, entre otros. Se trata de beneficios fiscales para aquellas empresas comprendidas en estas áreas que se inscriban en el «Registro Provincial de Beneficiarios de la Economía del Conocimiento», a las que se les brindará estabilidad fiscal por 10 años.

Además, contempla la exención del pago de impuestos sobre los Ingresos Brutos, sellos e inmobiliarios.

Asimismo, y con el objetivo de fomentar el crecimiento del sector del conocimiento en Santa Fe, desde la provincia se impulsan programas y proyectos que apuntan a convertir a la provincia en líder en talento y conocimiento.

«A través de un Fondo para co-inversión y co-financiamiento de _startups_, apostamos a acompañar a las empresas del sector, incentivar la mejora de oficinas de I+D+i, promover el escalado de ideas con impacto para que se transformen en nuevas startups y brindar incentivos para el desarrollo de emprendimientos en etapas tempranas», agregó Baima

También, para profundizar en las necesidades de las empresas proveedoras de servicios basadas en el conocimiento, la Provincia está trabajando junto a la Cancillería Argentina en un espacio de consultas sobre las negociaciones internacionales que lleva a cabo el gobierno nacional. 

Al respecto, la secretaria de Cooperación Internacional e Integración Regional, Julieta de San Félix, destacó la importancia del diálogo permanente con el ecosistema público-privado en materia de servicios: «Conocer las necesidades del sector no solo sirve para fortalecer y federalizar la posición negociadora, sino fundamentalmente para potenciar con medidas concretas a un sector de altísimo dinamismo».

Por su parte, desde la Agencia Santa Fe Global se busca contribuir al posicionamiento internacional de Santa Fe como una provincia exportadora de innovación e internacionalizar la Economía del Conocimiento. Para esto, y en conjunto con instituciones nacionales e internacionales, se trabaja activamente en programas de capacitación y sensibilización, asistencia técnica especializada, inteligencia comercial, acceso a plataformas digitales para la internacionalización de servicios, promoción comercial y del contacto con aceleradoras e inversores a nivel mundial.

«De esta forma se busca mejorar la competitividad y productividad de las empresas provinciales en mercados externos, contribuyendo y generando los incentivos adecuados para exportar bienes y servicios con mayor agregado de valor, posicionando el talento Santafesino en el exterior y generando el ingreso de divisas», aseguró Lucas Candioti, Subsecretario de Comercio Exterior y Nuevas Tecnologías.

El sector de la Economía del Conocimiento ocupa el tercer lugar como complejo exportador de la economía argentina, representando 8% de las exportaciones del país, y generando alrededor de 6.000 millones de dólares anuales.