---
category: Agenda Ciudadana
date: 2021-07-11T06:00:17-03:00
thumbnail: https://assets.3dnoticias.com.ar/SINOPHARM.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Un estudio determinó que la vacuna de Sinopharm alcanza una eficacia del
  84%
title: Un estudio determinó que la vacuna de Sinopharm alcanza una eficacia del 84%
entradilla: El dato surge de un estudio preliminar del Ministerio de Salud en personas
  de más de 60 años que ya recibieron las dos dosis, y que incluyó a las 24 jurisdicciones.

---
Un estudio preliminar del Ministerio de Salud en personas de más de 60 años, difundido a inicios de este mes, determinó que las vacunas contra el coronavirus incluidas en el plan oficial de inmunización mostraron una elevada efectividad para prevenir la mortalidad causada por la Covid-19; entre ellas, la Sinopharm, cuya eficacia llega al 84 por ciento tras la aplicación de las dos dosis previstas en su esquema, y de la cual la Argentina inició la importación de 24 millones de dosis, un operativo que irá de julio a septiembre.

Este es el segundo cargamento de los 10 programados para los próximos días que traerán en total 8.000.000 de dosis durante el mes de julio.

**El estudio**

El dato sobre el fármaco de origen chino surge de un estudio preliminar de Salud en personas de más de 60 años, que incluyó a las 24 jurisdicciones. Fue realizado sobre casos que incluyeron a las 24 jurisdicciones entre el 1 de enero y el 22 de junio de 2021 - meses de alta transmisión viral- y a la circulación predominante de las variantes Alpha (británica), Gamma (Manaos), y Lambda (andina).

El análisis se desarrolló en dos etapas: un diseño sobre casos y controles de diagnóstico negativo, que permitió estimar el efecto de la vacunación en todos los casos sospechosos notificados al Sistema Nacional de Vigilancia de la Salud (SNVS).

Sobre estos se discriminó entre los que resultaron con diagnóstico para SARS-CoV-2 detectable y no detectable.

En una segunda etapa se realizó un estudio longitudinal retrospectivo que permitió evaluar letalidad entre los casos confirmados.

La efectividad para mortalidad por Covid-19 indicó que la de Sinopharm llegó, con la aplicación de la primera dosis, al 61,6 por ciento en 147.908 casos analizados, y aumentó al 84 por ciento tras ser inoculadas la segunda dosis en la misma cantidad de individuos.

Con respecto a la efectividad según dosis y grupo etario, Sinopharm presentó: 67,8% y 80,2% (87.281 casos de personas de 60 a 69 años); 60,9% y 88,3% (40.669 sondeados de 70 a 79); y 35% y 77,6% para los mayores de 80 (19.958).

Esa vacuna se basa en una plataforma de virus inactivado que son aquellos que se inactivan normalmente mediante calor o productos químicos, que destruyen la capacidad del virus para replicarse, pero lo mantienen "intacto" para que el sistema inmune todavía lo pueda reconocer y genere una respuesta inmune, precisó la SADI en su página web.

**Aplicación pediátrica**

Por otra parte, el gobierno nacional solicitó a Sinopharm la información que llevó a autorizar la aplicación pediátrica de su vacuna contra el coronavirus en China con el fin de ser evaluada por las autoridades sanitarias argentinas, al igual que hará con la de Pfizer, que ya fue aprobada para adolescentes en Estados Unidos y Europa, conforme señalaron fuentes oficiales el martes pasado.

El objetivo de esa iniciativa oficial es que la Anmat pueda efectuar el análisis de la información del estudio que se realizó con niñas y niños de 3 a 17 años de la vacuna de Sinopharm, que es una de las que se está aplicando en Argentina en población adulta y de la que se espera que ingresen en los próximos meses 24 millones de dosis.

La solicitud se da en el marco de una estrategia del Ministerio de Salud para avanzar con la vacunación de niñas, niños y adolescentes, en particular de aquellos con comorbilidades.

Ocurre que, hace un mes, China aprobó el uso de emergencia de las vacunas contra Covid-19 de Sinopharm y de Sinovac Biotech (ambas que utilizan la plataforma de virus inactivado) en personas de 3 a 17 años.

"Después de su uso autorizado para adultos, las vacunas Sinopharm y Sinovac, ambas desarrolladas por instituciones farmacéuticas de China, han demostrado ser seguras para el grupo de edad de 3 a 17 años después de ensayos clínicos y revisiones de expertos, y ahora están autorizadas para uso de emergencia por el grupo de edad por autoridades relacionadas", dijo el experto del Centro Chino para el Control y la Prevención de Enfermedades, Shao Yiming, en la conferencia de prensa ese día.

La vacunación de la población pediátrica se está evaluando en todo el mundo y se están realizando ensayos clínicos de diversos inmunizantes.