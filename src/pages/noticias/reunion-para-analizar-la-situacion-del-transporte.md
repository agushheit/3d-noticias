---
category: La Ciudad
date: 2021-06-08T07:55:52-03:00
thumbnail: https://assets.3dnoticias.com.ar/zoom-transporte.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad de Santa Fe
resumen: Reunión para analizar la situación del transporte
title: Reunión para analizar la situación del transporte
entradilla: Cinco intendentes del interior del país se reunieron para solicitar una
  mayor distribución de subsidios y así garantizar el servicio de colectivo en Santa
  Fe, Rosario, Córdoba, Mar del Plata y Bahía Blanca.

---
El intendente Emilio Jatón se reunió con sus pares de las ciudades de Córdoba, Mar del Plata, Bahía Blanca y Rosario para analizar la situación del transporte público de pasajeros. Los mandatarios locales solicitaron a Nación una distribución más equitativa de los subsidios, teniendo en cuenta que los municipios del interior reciben menos recursos que el área metropolitana de Buenos Aires.

“Es urgente que Nación tenga una distribución más equitativa de los subsidios al transporte en todo el país. No se trata de una disputa del interior contra Buenos Aires, sino de que todos los argentinos puedan tener un servicio adecuado y sustentable”, indicó Jatón en el encuentro virtual que mantuvo con los intendentes, quienes también se pusieron de acuerdo en invitar a otros mandatarios del país.

De la reunión participaron los intendentes de Rosario, Pablo Javkin; de Mar del Plata, Guillermo Montenegro; de Bahía Blanca, Héctor Norberto Gay; y de Córdoba, Martín Llaryora. Emilio Jatón confía en que el diálogo institucional con Nación ayudará a resolver un conflicto que se acentuó con la pandemia, donde el corte de boleto cayó de manera abrupta y es necesario financiar el servicio con más subsidios.