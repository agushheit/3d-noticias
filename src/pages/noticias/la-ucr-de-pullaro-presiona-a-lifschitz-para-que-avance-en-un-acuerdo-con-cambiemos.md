---
category: Agenda Ciudadana
date: 2020-12-29T09:52:29Z
thumbnail: https://assets.3dnoticias.com.ar/2912-michlig.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: La política online'
resumen: La UCR de Pullaro presiona a Lifschitz para que avance en un acuerdo con
  Cambiemos
title: La UCR de Pullaro presiona a Lifschitz para que avance en un acuerdo con Cambiemos
entradilla: A través de un video, el radicalismo aliado al ex gobernador socialista
  advirtió que «no hay una avenida del medio» y reclaman sellar un frente con el macrismo.

---
El exministro de Seguridad de Lifschitz y actual diputado provincial, Maximiliano Pullaro, junto al senador Felipe Michlig le pusieron la voz en _off_ al video que lleva una consigna clara: «construir un Frente político amplio» para enfrentar al peronismo en las próximas elecciones.

***

[![Reproducir](https://assets.3dnoticias.com.ar/preview.webp)](https://youtu.be/3MsAb9893vg "Reproducir")

«Lifschitz tiene que dar un gesto porque no hay Lifschitz 2023 sin el radicalismo», afirmaron a LPO en el comité partidario y agregaron que para volver a la gobernación, el socialista «tiene que ser candidato en 2021 por un frente transversal y amplio, si no la UCR va a estar en otro lado».

No hay Lifschitz 2023 si no es candidato en las próximas elecciones y con un frente transversal y amplio, si no la UCR va a estar en otro lado

En el video, los radicales alertan sobre la «consolidación de un proyecto de concentración del poder político y económico en el Conurbano bonaerense» por parte del kirchnerismo y «para enfrentarlo no hay una avenida del medio», sostiene el senador Michlig.

«Como dirigentes de la Unión Cívica Radical vamos a realizar todos los esfuerzos para construir un Frente político amplio, porque entendemos, que el proceso histórico que se inicia nos necesita a los santafesinos poniéndole un límite claro a Cristina Kirchner en el senado de la República Argentina», concluyen.

La grabación se publicó en las redes de sus autores días después de que trascendiese una reunión en la casa de Pullaro a la que asistieron Martín Lousteau, Emiliano Yacobitti y el ex gobernador Miguel Lifschitz donde se abordó, fundamentalmente, la necesidad de la oposición de armar un frente transversal para ganarle al peronismo.

En ese esquema, Santa Fe tiene un peso fundamental y la decisión que tome el socialismo es clave. En las últimas elecciones, con la insistencia sobre la construcción de un tercer espacio que supere la grieta, lo que ha conseguido ha sido perder representatividad en el Congreso nacional. Sin embargo, los principales obstáculos que tiene Lifschitz para cerrar un acuerdo con Cambiemos es su propio partido.

Por ello, no son pocos los que sospechan que la dura advertencia de los radicales liderados por Pullaro tiene como destino final a los socialistas que se resisten a una fórmula con el macrismo más que al propio Lifschitz. El exministro de Seguridad tiene contactos diarios con el socialista y el tema es excluyente.

Si bien, era difícil imaginar a Lifschitz sellando un acuerdo con Mauricio Macri, sobre todo con la línea dura que hoy encarna, con Rodríguez Larreta la historia es otra. El socialista tiene una muy buena relación con el alcalde porteño que sumado a la presencia de Lousteau, se abre un espacio más interesante para explorar en Santa Fe.

«Ya lo vienen conversando con Miguel -Lifschitz- pero era necesario salir sin rodeos a dar una señal directa», afirmó una fuente partidaria y recordó que son los intendentes y presidentes comunales los que conocen el pulso del territorio y las pocas chances de las avenidas del medio:

«No quieren que en sus territorios se abran candidaturas alternativas y en términos pragmáticos, los intendentes del Frente Progresista se terminan plantando en contra del gobierno nacional», explican y señalan la buena recepción del video.

La posibilidad de un frente que reúna al socialismo, el radicalismo y el PRO es una verdadera preocupación para Omar Perotti y para el peronismo en general donde saben que el escenario de tres tercios que se planteó en la provincia fue fundamental para alzarse con el gobierno después de 12 años de gestión progresista.

Pero también lo es para el intendente de Rosario, Pablo Javkin, quien tiene un acuerdo sólido con el gobernador santafesino y que cuando se enteró de la reunión de Lousteau y Lifschitz en la casa de Pullaro, pegó el grito en el cielo.

Es que Javkin, que también tiene trato periódico con Rodríguez Larreta, le reprocha a Lifschitz porque no lo termina de reconocerlo como el nuevo líder del Frente Progresista y como en política nadie regala nada, el socialista junto a Pullaro aceleran la estrategia electoral hacia el 2021 prescindiendo del intendente rosarino, al menos en las primeras fotos.