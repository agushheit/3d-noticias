---
category: Agenda Ciudadana
date: 2020-12-22T10:42:04Z
thumbnail: https://assets.3dnoticias.com.ar/2212gremios.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Diario UNO'
resumen: 'No hubo acuerdo en la paritaria y los estatales santafesinos van al paro '
title: 'No hubo acuerdo en la paritaria y los estatales santafesinos van al paro '
entradilla: Desde ATE y UPCN evitaron dar detalles de la propuesta del gobierno. La
  medida de fuerza será este miércoles y sin concurrencia a los lugares de trabajo.

---
No hubo acuerdo en la paritaria central y **los sindicatos UPCN y ATE anunciaron un paro para este miércoles**. Este lunes por la tarde el gobierno se reunió con los docentes, médicos y profesionales de la salud.

Jorge Molina, Secretario General de Upcn destacó: «Estamos en la última semana del año y al no tener demasiado margen para replantear esta situación, ambas instituciones (UPCN y ATE) habíamos dispuesto que en el caso de que no haya un acuerdo, disponer una jornada de protesta el día miércoles».

El mismo, describió el sindicalista, consistirá en «un paro sin concurrencia a los lugares de trabajo, con mantenimiento de guardias mínimas y con plena atención en áreas Covid. El resto de la administración pública va a parar en protesta al no tener una respuesta satisfactoria después de un año tan difícil y tan complicado como el que vivimos todos».

Jorge Hoffmann, secretario general de ATE prefirió no dar detalles de la oferta presentada por el gobierno y fundamentó: «En una negociación no tiene sentido decir lo que no fue».

Igualmente, el titular de UPCN deslizó con relación al ofrecimiento rechazado: «Uno de nuestros argumentos era que no queríamos más sumas fijas (como lo explicamos varias veces) pero el porcentaje que nos ofrecen no es satisfactorio. Para nosotros es insuficiente».

Molina añadió que la «media de protesta es responsable y razonable, considerando todo el año vivido, la paciencia de los trabajadores que están esperando una respuesta salarial y que esto no ha sido satisfactorio. Creo que el paro va a ser contundente y ojalá tengamos una revisión de parte del gobierno para terminar el año de la manera que los trabajadores se merecen, con un reconocimiento salarial».

Consultado sobre si la negociación está cerrada, Hoffmann respondió: «hay posibilidades de volver a juntarse en la medida en que el gobierno cambie de postura. Si después del miércoles, el gobierno modifica su situación, escucha nuestros reclamos, estamos predispuestos a escucharlo y a debatir».