---
category: Estado Real
date: 2021-09-28T06:00:13-03:00
thumbnail: https://assets.3dnoticias.com.ar/PRIETO.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La provincia participó de una jornada de planificación para la pospandemia
title: La provincia participó de una jornada de planificación para la pospandemia
entradilla: Lo hizo junto a la Dirección Nacional de Fortalecimiento de los Sistemas
  Provinciales en el marco de la asistencia técnica en terreno a la provincia de Santa
  Fe.

---
El Ministerio de Salud de Santa Fe participó, junto a la Dirección Nacional de Fortalecimiento de los Sistemas Provinciales, de una jornada de planificación sobre la pos pandemia Covid-19.

La misma fue en el marco de la asistencia técnica en terreno a la provincia de Santa Fe, que se impulsa desde el gobierno nacional y se llevó a cabo este lunes en la ciudad de Rosario.

Al respecto, el secretario de Salud, Jorge Prieto, explicó que “acompañados por el gobierno nacional en todo en lo que hace a la utilización y a la importancia que tienen los programas de redes Sumar y Proteger, con el gran apoyo desde el punto de vista financiero en lo que hace al mega plan de vacunación, equipamiento y formación del recurso humano”.

“Las actividades interdisciplinarias que llevamos adelante junto al equipo de trabajo de la provincia nos va a poder fortalecer la reutilización de estos fondos, la reinversión”, continuó.

“La idea de hoy es trabajar la planificación de todo lo que hace a la pos pandemia. La ejecución de estos programas se viene realizando desde hace tiempo. Estamos observando el déficit asistencial a nivel de los efectores en virtud de lo que ha hecho el coronavirus”.

Finalmente, el secretario de Salud señaló que “la idea es comenzar a trabajar de ahora en más en un futuro para retomar la población adscripta para poder equipar, mejorar los indicadores y cumplir con los objetivos para que esto reditúa desde el punto de vista económico y, fundamentalmente, en lo que hace a la calidad de vida de la población”.

“La implicancia que tiene esto es poder fortalecer todas estas redes que aportan a la provincia un gran recurso”, concluyó Prieto.