---
category: El Campo
date: 2021-01-06T10:37:11Z
thumbnail: https://assets.3dnoticias.com.ar/060121-mesa-enlace.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: El Litoral'
resumen: La Mesa de Enlace anunció un paro por 72 horas
title: La Mesa de Enlace anunció un paro por 72 horas
entradilla: Los integrantes confirmaron un lockout de tres días a partir del lunes
  11 de enero.

---
**Las entidades que conforman la Comisión de Enlace decidieron decretar un paro desde el próximo lunes 11 hasta el miércoles 13 de enero en reclamo por el cierre temporal del registro de exportaciones de maíz.**

«Hemos decidido un cuarto intermedio de esta Comisión hasta mañana a las 17 horas, con el propósito de realizar las consultas internas en el seno de cada institución con el objeto de tomar una definición sobre las acciones a seguir», habían manifestado el pasado lunes 4 de enero en un comunicado impartido a los medios de comunicación.

**La Comisión de Enlace de Entidades Agropecuarias está integrada por la Sociedad Rural Argentina, Confederaciones Rurales Argentinas, Coninagro y Federación Agraria.**

Esta entidad remarcó que se «evaluó la medida que comprende el cierre del registro de exportación de maíz dispuesto por el Gobierno nacional, anuncio que se suma a un conjunto de políticas perjudiciales para el campo, implementadas a lo largo del año que acaba de concluir».

El pasado miércoles, el Ministerio de Agricultura, Ganadería y Pesca suspendió temporalmente el registro de Declaraciones Juradas de Venta al Exterior (DJVE) de maíz con fecha de embarque anterior al 1 de marzo próximo.