---
category: Estado Real
date: 2021-02-22T06:00:17-03:00
thumbnail: https://assets.3dnoticias.com.ar/lacteosperotti.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Provincia avanza en la Internacionalización Asociativa de las Pymes lácteas
  Santafesinas
title: La Provincia avanza en la Internacionalización Asociativa de las Pymes lácteas
  Santafesinas
entradilla: El objetivo es lograr la internacionalización de PyMES que realicen productos
  con agregado de valor.

---
El ministro de Producción, Ciencia y Tecnología, Daniel Costamagna, encabezó un encuentro virtual entre representantes de las áreas de Lechería y Comercio Exterior de su cartera con PyMES lácteas santafesinas, para avanzar en un proyecto asociativo que les permita internacionalizarse.

Acompañaron al ministro el secretario de Comercio Exterior, Germán Burcher y el director provincial de Lechería, Abel Zenklusen. Participaron también el supervisor del Centro Regional Santa Fe del Servicio Nacional de Sanidad y Calidad Agroalimentaria (Senasa), Ernesto Marelli; y el gerente de APyMIL, Mariano Viroglio.

Durante la reunión los funcionarios ahondaron sobre las ventajas de asociarse para exportar y los alcances que ello implica. Al respecto, Burcher indicó: “Nuestro objetivo de gestión es generar un esquema de asociatividad orientado al incremento del número de PyMES exportadoras, y hacerlo con productos de mayor valor agregado”.

“Buscamos la asociatividad como un espacio que permita generar sinergia entre grupos de PyMES, en un esquema donde las empresas no pierdan su individualidad, sino que, a través de estos esfuerzos conjuntos, se puedan posicionar mejor para llegar a otros mercados”, indicó el secretario.

**Asociatividad Para Exportar**

Estas acciones se enmarcan dentro del Programa de Asociatividad para Exportar, orientado a pequeñas y medianas empresas de nuestra Provincia y tiene por objetivo que PyMES con potencial para exportar o eventualmente exportadoras puedan disponer de asesoramiento personalizado por parte de un asistente técnico para contribuir a su proceso de internacionalización.

El Ministerio de Producción, Ciencia y Tecnología viene trabajando con Senasa junto al sector lácteo para identificar aquellas empresas que desean exportar pero que quizás aún les falta la inscripción en dicho organismo, o la certificación del Análisis de Peligros y Puntos de Control Crítico, o HACCP (sistema de gestión enfocado a asegurar la producción segura de cualquier empresa dentro de la cadena alimentaria).

Durante el encuentro Marelli profundizó acerca de los trámites e inscripciones necesarios que tienen que cumplimentar las empresas para exportar los productos del sector.

**Agregado De Valor**

Al respecto, Zenklusen comentó: “Estamos armando el proyecto de un grupo exportador de PyMES lácteas; para el cual la provincia brindará la asistencia de un licenciado en Comercio Exterior los primeros seis meses, con una agenda que contemple los casos particulares”.

“Nuestro interés es que este consorcio se expanda a más entidades, exportando productos de alto valor agregado, como los quesos”, concluyó el director.