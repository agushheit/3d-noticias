---
category: Agenda Ciudadana
date: 2022-01-23T06:00:41-03:00
thumbnail: https://assets.3dnoticias.com.ar/ahora12.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: La Secretaría de Comercio Interior avanza para definir los cortes cuidados,
  una canasta escolar y los fideicomisos
title: La Secretaría de Comercio Interior avanza para definir los cortes cuidados,
  una canasta escolar y los fideicomisos
entradilla: "El área que conduce Roberto Felletti define los precios de los cortes
  que a principios de mes fueron reservados al mercado interno.\n\n"

---
La Secretaría de Comercio Interior avanza en los últimos días en negociaciones para lanzar los precios de los siete cortes de carnes cuidados y una canasta escolar para el inicio de clases, al tiempo que vence a fin de mes el programa Ahora 12.  
  
Comercio Interior avanzará en los próximos días en fijar los precios de los cortes que a principios de mes fueron reservados al mercado interno en el Decreto 911/21 hasta el 31 de diciembre de 2023 inclusive: asado con o sin hueso, falda, matambre, tapa de asado, nalga, paleta y vacío.  
  
Asimismo, la Secretaría que conduce Roberto Feletti trabaja en un acuerdo con el sector industrial y comercial de librerías para lanzar, antes del inicio de las clases, una canasta escolar con útiles a precios accesibles.  
  
El presidente de la Cámara de la Industria de Artículos de Librería (CIAL), Marco Selan, dijo esta semana que "hemos empezado a trabajar hará tres semanas con la Secretaría de Comercio Interior en esta propuesta donde hay una lista de artículos que van a ser garantizados en las librerías que se consigan a un nivel de precio más que competitivo, como para que el consumidor pueda acceder a ellos".  
  
De las negociaciones participan la CIAL junto con la Cámara Argentina de Papelerías, Librerías (Capla) y Comercio Interior, para definir la canasta que tendrá entre 20 y 25 artículos que conforman la mochila básica de nivel inicial y primario.  
  
Desde Comercio Interior señalaron que el acuerdo está en curso y que la intención es lanzar la canasta escolar en el corto plazo, en vistas de que las clases comenzarán el mes próximo en la Ciudad Autónoma de Buenos Aires (CABA).

> La intención es lanzar la canasta escolar en el corto plazo, en vistas de que las clases comenzarán el mes próximo en CABA

  
Según un informe de la consultora Focus Market, el kit de útiles escolares compuesto por 21 productos para el nivel primario tiene un precio este año de $4.470, mientras que en 2021 costaba $2.450, con un aumento del 82%.  
  
La consultora también indicó que el combo escolar compuesto por útiles, mochila y guardapolvo tiene un precio este año de $7.469, mientras que en 2021 costaba $2.249, representando un aumento del 76%.  
  
Selan afirmó que "el índice de aumento ronda el 30%", y aclaró que "hay productos con menor porcentaje y otros que por su origen o los componentes con los que están hechos quizás han tenido aumentos superiores".  
  
Aseguró que "en lo que se refiere a útiles lo que trabajamos dentro del sector en todos los eslabones, entre la industria y el retail, es garantizar que haya productos al alcance de todos los bolsillos en la Argentina".  
  
"Hay una propuesta y oferta de productos muy amplio como para que todos los consumidores puedan armar una mochila para ir al colegio", agregó.  
  
Por otra parte, se espera que Feletti mantenga una reunión con el ministro de Agricultura, Julián Domínguez, para avanzar en el armado del fideicomiso de trigo y de maíz, que apunta a impactar en los costos de las cadenas de pan y pollo y bajar los precios al consumidor.  
  
Uno de los objetivos del fideicomiso es regular el precio de la bolsa de harina industrial, de 25 kilos, que se usa para la elaboración del pan, y así bajar la incidencia de este costo en el precio del kilo de pan que se vende al público en las panaderías.  
  
"Si logramos consolidar una canasta de frescos y una canasta de góndola, una canasta regulada de alimentos diversa, ahí yo me sentiría muy contento con la gestión porque podríamos cumplir el objetivo estructural que es nivelar ingresos con consumo de canasta alimentaria; lograr que la canasta alimentaria deje de ser un peso en el ingreso popular y esté ordenada, sea previsible, esa es la gran apuesta de ese proceso", afirmó Feletti.

En tanto, el 31 de enero vencerá la etapa actual del programa Ahora 12 y en la Secretaría ya hubo reuniones y se está avanzando con la intención de resolver la continuidad del programa la semana que viene.  
  
En esta etapa, Ahora 12 agregó la posibilidad de comprar determinados productos en 24 y 30 cuotas fijas mensuales, que se suman a las modalidades de 3, 6, 12 ó 18 pagos.  
  
De acuerdo con los últimos datos del Ministerio de Desarrollo Productivo, en noviembre la facturación del programa marcó un nuevo récord con un crecimiento de 13% respecto del mes anterior, con ventas por 111 millones de pesos.

> En noviembre la facturación del programa Ahora 12 marcó un nuevo récord con un crecimiento de 13% respecto del mes anterior

  
En el acumulado anual las ventas alcanzaron un crecimiento del 70% respecto de 2020, con ventas por 822 millones de pesos.  
  
Los rubros con mayores ventas mensuales fueron en noviembre electrodomésticos (35%), indumentaria (22%) y materiales para la construcción (10%).