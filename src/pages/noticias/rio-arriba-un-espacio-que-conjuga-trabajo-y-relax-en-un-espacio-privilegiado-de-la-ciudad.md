---
category: La Ciudad
date: 2021-01-22T04:24:58Z
thumbnail: https://assets.3dnoticias.com.ar/ventas_rio_arriba.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Publicación Comercial
resumen: Río Arriba, un lugar que conjuga trabajo y relax en un espacio privilegiado
  de la ciudad
title: Río Arriba, un lugar que conjuga trabajo y relax en un espacio privilegiado
  de la ciudad
entradilla: Ubicado en el prestigioso Puerto de Santa Fe, a la vera del río, el edificio
  cuenta con una vista única que permite crear un ambiente relajado y distinto al
  de la ciudad.

---
Desde hace unos años el Puerto de Santa Fe viene desarrollando un crecimiento a pasos agigantados, siguiendo un master plan que busca convertirlo en un área de viviendas con perfil náutico a 5 minutos del centro con todas las comodidades y la tranquilidad propia de vivir junto al río.

El área vacante ubicada en la intersección de las calles Angela T. Vigetti -al oeste- y Rosaura Schweizer -al sur-, presenta un importante desarrollo de frente franco al río, por un lado, y al espacio público verde previsto, por otro.  
Dada su extensión, su posición estratégica y los indicadores urbanísticos vigentes, se propuso una intervención integral que por jerarquía y escala generara una nueva dinámica funcional y vivencial en el sector, transformándolo del lugar recluido y sin vida que es actualmente, en una nueva “cara visible” de la ciudad hacia el río.

![](https://assets.3dnoticias.com.ar/rio arriba work center.jpg)  
Río Arriba nace como el primer emprendimiento de envergadura fuera de los sectores ya consolidados en el Distrito Puerto, ampliando las propuestas existentes, a la construcción de espacios de trabajos destinados a empresas, profesionales y todo tipo de entidades públicas o privadas, el proyecto lleva implícita en su concepción una recualificación de la zona, la que a partir de su revitalización se constituirá en nuevo polo de atracción dentro del Master Plan. Este emprendimiento se concibe con un carácter terciario de servicios y equipamientos diversos, con una impronta “portuaria” desde su imagen y con énfasis en criterios urbanísticos y paisajísticos de fuerte articulación ciudad-rio- entorno natural.

Desde la arquitectura moderna hasta la alta tecnología, el edificio está diseñado para generar confort en el espacio de trabajo.

Las torres están certificadas como edificio verde, gracias a su ubicación, su estética, sus terrazas verdes en las plantas superiores y en el cuarto piso, y al logro de la eficiencia energética mediante módulos de alertas ubicados estratégicamente para ese fin, entre otras características.

Ubicado en el prestigioso Puerto de Santa Fe, a la vera del río, el edificio cuenta con una vista única que permite crear un ambiente relajado y distinto al de la ciudad. Es de destacar además que, por su ubicación, uno de los factores preponderantes es la accesibilidad que posee, ya que se encuentra a escasos metros de los corredores viales que conectan en pocos minutos a los distintos puntos del país.

![](https://assets.3dnoticias.com.ar/locales comerciales a la venta en santa fe.jpg)

La premisa es estar al alcance de pequeñas y medianas empresas por lo que ofrecen financiación exclusiva. Para saber más, escribir a _consultas@rioarribaworkcenter.com_ o llamar al 3425040895 para coordinar una visita.

**El proyecto**

Se trata de la construcción de un agrupamiento de blocks multifuncionales que, a manera de “clusters” alojados en una PLATAFORMA MULTIMODAL, admitan la instalación de una variada multiplicidad de rubros y actividades, teniendo en cuenta la importante demanda que se verifica en el sector; es decir que la propuesta viene a constituir un novedoso aporte al desarrollo del Master Plan, ya que en lugar de clausurar la oferta de espacio la ampliará a todos los potenciales interesados, incluso a aquellos a los que, por su escala, les resulta infructuoso individualmente acceder a los últimos predios disponibles.

En una 1º Etapa que se encuentra en avanzada construcción la Torre I, con unos 128 módulos básicos o Unidades Funcionales de una superficie de hasta 50 m2. propios aproximadamente, las cuales podrán expandirse y/o agruparse a efectos de adaptarse a demandas de mayor superficie; a tal fin se disponen servicios y equipamientos comunes generales, contemplando agregados privados en unidades agrupadas que configuren superficies mayores. Esta dotación se prevé ampliar con la Torre II sobre el área de reserva ubicada hacia el este, en unas 77 unidades más, conectándose ambas torres por un auditorio o salas de usos múltiples de distinta escala, recepción y servicios centralizados, llegando a conformar un centro de convenciones que funcione como núcleo operativo y articulador del CENTRO DE NEGOCIOS.

En síntesis, este Complejo se concibe en el marco de las tendencias más evolucionadas dentro de los procesos de reconversión de áreas portuarias que se vienen desarrollando en los últimos tiempos en el mundo entero, y representa una alternativa urbanística, funcional y vivencial en relación a la sub–ocupación fragmentaria de parcelas que se ha venido practicando en el sector.

En tal sentido cabe resaltar que, a partir de la determinación proyectual de preservar gran parte de la arboleda ubicada en el frente sur del predio, y de liberar dicha área como parque-jardín abierto, se producirá una franca integración con el paseo ribereño construido recientemente y a través suyo, con el río y el entorno natural circundante, erigiéndose en un conjunto urbano excepcional que comenzará a definir el nuevo paisaje urbano costero de nuestra ciudad.

![](https://assets.3dnoticias.com.ar/2cb7a818-1303-4b37-87ef-3f7acb6a0e1d.jpg)

![](https://assets.3dnoticias.com.ar/6b437496-4aff-4041-8d0d-e5cea1ddca3e.jpg)

![](https://assets.3dnoticias.com.ar/07a5e164-62d5-4a12-b495-d78e250112de.jpg)

![](https://assets.3dnoticias.com.ar/locales comerciales a la venta en santa fe.jpg)

**SERVICIOS OFRECIDOS**

* Seguridad de punta

  · Sistema de observación con equipo digital de grabación IP

  · controles de acceso

  · vigilancia 24 hs

  · aberturas DVH

  · sistema de detección de incendios
* Conectividad

  · Servidor de comunicaciones telefónicas híbrido (IP, digital, analógico)

  · acceso a internet y sistema WIFI

  · un ACCESS POINT en cada nivel
* Parking

  · Cocheras bajo techo con capacidad para 140 vehículos

  · vigilancia las 24hs

  · bicicleteros gratuitos
* Auditorio

  · CAPACIDAD PARA 180 PERSONAS

  · EQUIPADO CON SISTEMA DE AUDIO Y PROYECCIÓN

  · FLEXIBILIDAD PARA REDISEÑAR EL ESPACIO SEGÚN LOS REQUERIMIENTOS DEL EVENTO
* Accesibilidad

![](https://assets.3dnoticias.com.ar/venta de oficinas en santa fe.jpg)