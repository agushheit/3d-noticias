---
category: Agenda Ciudadana
date: 2020-12-22T10:46:21Z
thumbnail: https://assets.3dnoticias.com.ar/2212fiscales.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Diario UNO'
resumen: 'Juego clandestino: piden el apartamiento de los fiscales por «falta de imparcialidad»'
title: 'Juego clandestino: piden el apartamiento de los fiscales por «falta de imparcialidad»'
entradilla: Los funcionarios señalados por el abogado del senador Traferri son Matías
  Edery y Luis Schiappa Pietra, de la Unidad Delitos Complejos.

---
La defensa del senador provincial del PJ Armando Traferri, quien es investigado por la justicia por su presunta vinculación con una red de juego clandestino, reclamó el apartamiento de los fiscales a cargo de la causa por considerar que «están desviados de su acción. Desde el primer momento, los fiscales perdieron la imparcialidad», afirmó este lunes José Luis Vázquez, asesor legal del legislador por el departamento San Lorenzo.

Los fiscales señalados por Vázquez son Matías Edery y Luis Schiappa Pietra, de la Unidad Delitos Complejos, quienes pidieron ante la Legislatura santafesina el cese de la inmunidad del senador para indagarlo e imputarlo como presunto organizador de una asociación dedicada al juego clandestino.

El pedido de los fiscales fue rechazado el viernes por la Cámara de Senadores en un trámite «exprés». Ahora los investigadores plantearán ante un juez la inconstitucionalidad del artículo 27 del Código Procesal Penal de Santa Fe que otorga a los legisladores no solo la imposibilidad de ir a prisión, sino que les da incluso inmunidad de proceso, vale decir, ni siquiera pueden ser acusados sin que les levanten ese privilegio.

En declaraciones al programa «El primero de la mañana», de LT8, el abogado defensor de Traferri dijo que elevó el pedido en las últimas horas de ayer, a través de un email con copia a los poderes Legislativo y Judicial, a la auditora de Gestión del Ministerio Público de la Acusación y a los fiscales general, Jorge Baclini, y regional de Rosario, María Eugenia Iribarren.

Vázquez aseguró que Edery y Schiappa Pietra «perdieron la imparcialidad desde el primer momento». Y añadió: «Los fiscales siguen en la misma línea que denunciamos acerca del ministro de Seguridad Marcelo Sain: desmerecer al Senado, a los senadores y sobre todo a Traferri. ¿Qué hicieron los fiscales luego de que les rechazaran el pedido de inmunidad? Salieron a en los medios desprestigiar a los senadores, lo mismo que viene a haciendo Sain. La línea está clara. Sain hace ocho meses que viene denostando al Senado vinculándolo con los narcos, llamando conservadores a los legisladores».

# **Garantías con otros fiscales**

Vázquez manifestó que su cliente no tendría ningún inconveniente en presentarse a declarar y responder las preguntas que sean necesarias ante otros fiscales que «garanticen imparcialidad».

«Con fiscales que ofrezcan garantías, sin dudas se presentará. Si estuvieran los fiscales (Miguel) Moreno o (Aníbal) Vescovo, no tendríamos ningún problema, porque son personas de bien. Esta gente (Edery y Schiappa Pietra) no lo son porque siguen instrucciones del Poder Ejecutivo. Están contaminados por la política del Gobierno provincial que inició una campaña de desprestigio del Senado y en especial del senador Traferri», afirmó el abogado de Traferri.

# **Otro día clave**

Este lunes será un día clave en la causa en la que se investiga una red de juego clandestino. Para las 15.30 está prevista la audiencia en la que el ex fiscal regional Patricio Serjal será reimputado como organizador de una asociación ilícita. Los fiscales Matías Edery y Luis Schiappa Pietra deberán allí delinear necesariamente el rol de los otros acusados de organizadores de la estructura ilegal.

Por eso se expondrá por primera vez en su plenitud la evidencia acumulada contra el senador por San Lorenzo, Armando Traferri, a quien los fiscales consideran el enlace entre Serjal el también exfiscal Gustavo Ponce Asahad, presos por cobrar retornos para permitir en distintos puntos de la provincia el despliegue del juego clandestino, y Leonardo Peiti, el empresario más importante de esta actividad ilegal.