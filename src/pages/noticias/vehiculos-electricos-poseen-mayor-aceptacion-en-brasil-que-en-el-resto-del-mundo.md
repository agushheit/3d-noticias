---
category: Estado Real
date: 2022-11-20T11:03:38-03:00
thumbnail: https://assets.3dnoticias.com.ar/scooters.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: https://portalmovilidad.com
resumen: " Vehículos eléctricos poseen “mayor aceptación” en Brasil que en el resto
  del mundo"
title: " Vehículos eléctricos poseen “mayor aceptación” en Brasil que en el resto
  del mundo"
entradilla: Una encuesta deja en evidencia las altas tasas de aceptación para vehículos
  eléctricos, micromovilidad y servicios relacionados. ¿Qué otros resultados arrojó
  el estudio?

---
Si bien las ventas de autos eléctricos continúan creciendo en Brasil, una nueva encuesta apunta a un **comportamiento esperado: los brasileños expresaron un mayor interés en los autos eléctricos, la micromovilidad y la tecnología a niveles más altos que los consumidores de otros países.**

Realizada por **McKinsey & Company**, la encuesta indica que los brasileños están muy **interesados ​​en soluciones como suscripciones de vehículos eléctricos y micromovilidad.**

Según la encuesta, hay cuatro factores principales que pueden impulsar la electromovilidad en Brasil: **consumidores, tecnología, infraestructura e incentivos regulatorios.**

“Sin embargo, el consumidor es la clave de la historia. Los brasileños tienen un enorme apetito por las nuevas tecnologías y nuestro estudio identificó que, en el país, **hay una intención de adopción por encima de la media de los países desarrollados** ”, asegura **Felipe Fava**, líder del McKinsey Future Mobility Center en América Latina.

Según **Fava**, además del deseo e interés del consumidor por este tipo de vehículos, **la preocupación medioambiental y la aparición de nuevas soluciones de movilidad** son factores relevantes para la consolidación de esta tendencia.

La encuesta muestra que el 70% de los consultados considera unirse a los servicios de suscripción de vehículos, principalmente debido a la posibilidad de explorar diferentes tipos de soluciones de movilidad (21%) y debido a una **posible reducción en los costos totales de propiedad** (18%), ya que no serían necesarios para adquirir vehículos eléctricos, pagando únicamente por su uso.

Otro destaque es que el **39% de los brasileños dice tener la intención de utilizar más medios de locomoción de micromovilidad**, como scooters eléctricos y bicicletas, en los próximos 10 años.