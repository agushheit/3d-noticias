---
layout: Noticia con imagen
author: "Fuente: UNL"
resumen: "#Campo: Riego Suplementario"
category: El Campo
title: Riego suplementario, ¿se puede instalar en la agenda del campo?
entradilla: Días pasados se realizó una conferencia sobre el riego suplementario
  con la premisa de instalar este tema en la agenda. ¿Por qué se necesita riego
  en la región pampeana?
date: 2020-11-18T14:15:05.720Z
thumbnail: https://assets.3dnoticias.com.ar/riego.jpeg
---
Días pasados se realizó una conferencia sobre el riego suplementario con la premisa de instalar este tema en la agenda. ¿Por qué se necesita riego en la región pampeana? Al respecto, el Dr. Roberto Marano, docente investigador de la Facultad de Ciencias Agrarias (UNL), contó detalles de esta propuesta.

“Si bien los promedios anuales de lluvias y evapotranspiración están equilibrados, esta región presenta períodos de semanas o algunos meses, especialmente en verano, en los cuales las lluvias son menores a la evapotranspiración y no es suficiente el agua almacenada en el suelo para suplir ese déficit. Esta situación se acentúa en regiones donde el suelo tiene menor capacidad de almacenar agua. Es recordada la sequía que ocurrió entre 1994 y 1998 por que fueron sucesivos años con falta de lluvias, pero también ocurrió en 2008, 2018 y en la actualidad (2020). Como resultado, se producen pérdidas de productividad del orden del 30 a 50%, destacándose que en la sequía del 2018 se perdieron 7.110 millones de dólares, sólo teniendo en cuenta mermas en maíz y soja” detalló el investigador.

Para Marano “el riego suplementario es la tecnología que puede resolver este problema, dado que permite suplir la falta de lluvia en períodos breves de tiempo. La fuente de agua utilizada fue subterránea, con acuíferos que permiten ser utilizados con criterios de sustentabilidad. También se cuenta con el río Paraná, que es una fuente de agua dulce muy importante, prácticamente no utilizada con fines de riego, excepto para el cultivo de arroz (30.000 ha en San Javier)”.

Los regantes pueden ser productores individuales. “Es el caso donde un único usuario utiliza la fuente de agua. En general predomina la fuente subterránea, pero también hay ejemplos de fuente superficial. En estos casos hay dificultades para el mantenimiento de los equipos, menos aprendizaje grupal (no hay con quien compartir experiencias, porque están separados entre sí). O bien pueden organizarse en una red colectiva: Es el caso con varios usuarios utilizando la misma fuente de agua (superficial, meteórica o subterránea). Para esta última situación hubo diversos estudios regionales para aprovechar el Paraná para riego, a saber: 10000 ha Monje y Barrancas en el Departamento San Jerónimo, 11.000 en Avellaneda y otras 10.000 ha en la cuenca cañera santafesina (ambos situados en el Departamento Gral. Obligado)” comentó.

Estos estudios “fueron impulsados fundamentalmente por productores agropecuarios -y otros actores sociales beneficiarios- y contaron con el aval inicial de las autoridades gubernamentales de turno, tanto provinciales como nacionales, que aportaron profesionales para llevar a cabo los proyectos con posibilidad de utilizar crédito internacional como fuente de financiamiento (BID, Banco Mundial a través de PROSAP). No obstante, ninguno de ellos prosperó, fundamentalmente por la falta de decisión en la toma de deuda pública y/o falta de consenso sobre el impacto que tendría esta tecnología sobre los recursos agua y suelo. Si bien la provincia de Santa Fe cuenta con una reciente Ley de Aguas (N° 13740) que indica que el riego es un uso agropecuario que requiere autorización, no está aún reglamentada y no se conocen cuáles son los criterios a considerar para autorizar un área de riego colectivo” pormenorizó el especialista.

Según Marano “en la década de los ´90 fue el auge de la instalación de los equipos de pivote central en la región pampeana y luego se fueron ampliando a otras regiones. Estas máquinas de riego son las preferidas por los productores por su versatilidad y automatismo (figura 2), pero requieren una importante inversión, la cual puede incluir uso de paneles fotovoltaicos para generar energía eléctrica a menor costo”.

En este sentido “los resultados productivos de quienes incorporaron riego con pivote central (Avellaneda, campañas 2013 a 2019) indican una diferencia promedio de rendimiento de 920 kg/ha en trigo, 3.000 kg/ha en maíz, 620 kg/ha en algodón y 925 kg/ha en soja. En tanto en Entre Ríos, los resultados fueron de 1.200 kg/ha en soja y 2.650 kg/ha en maíz. Considerando estas diferencias de productividad y los costos de inversión y operativos del riego, el período de repago de la inversión sería entre 6 y 8 años, por lo que cualquier plan crediticio debería ser con esos plazos” detalló el profesional.

Un estudio patrocinado por FAO en 2013 indica que “en Argentina se puede ampliar la superficie con riego suplementario en 4,26 millones de hectáreas a partir de agua superficial, con una inversión estimada en 16.500 millones de dólares. A la luz de las pérdidas económicas de 2018, este monto no resulta inalcanzable” destacó Marano.

El siguiente esquema sería una propuesta para instalar riego suplementario en la agenda productiva, al menos de la provincia de Santa Fe, según nos dice Marano.

Esta foto corresponde a un cultivo de trigo con pivote central ubicado en Avellaneda en esta campaña (2020). Puede observarse cómo el trigo ubicado en los extremos del círculo que está en secano tiene muy poco desarrollo.