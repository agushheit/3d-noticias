---
category: La Ciudad
date: 2021-12-06T06:00:02-03:00
thumbnail: https://assets.3dnoticias.com.ar/circunvalacion.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Licitarán trabajos clave para mejorar el estado de la Circunvalación de Santa
  Fe
title: Licitarán trabajos clave para mejorar el estado de la Circunvalación de Santa
  Fe
entradilla: Vialidad Provincial y la Universidad Tecnológica Nacional Regional Santa
  Fe rubricaron un convenio para elaborar el proyecto de las obras. En las próximas
  semanas se llamará a licitación.

---
El Ministerio de Infraestructura, Servicios Públicos y Hábitat, a través de la Dirección Provincial de Vialidad, suscribió un convenio específico con la UTN para la elaboración de un anteproyecto básico y los términos de referencia, en el marco del Plan de mejoras de seguridad vial y ciudadana en los accesos de la ciudad capital de Santa Fe.

 El acuerdo, suscripto por la ministra de Infraestructura, Silvina Frana, el administrador general de la DPV, Oscar Ceschi y el decano, Rudy Grether incluye el desarrollo de la documentación licitatoria de carácter técnico, el anteproyecto, planos, cómputos métricos, presupuesto y términos de referencia para que la empresa contratista elabore el proyecto ejecutivo.

 Consultada luego de la firma, Silvina Frana explicó que "hace poco más de un mes, el gobernador Omar Perotti contrajo un compromiso con el ministro de Obras Públicas de la Nación, Gabriel Katopodis, por el cual la Nación iba a comprometer 5.000 millones de pesos en una primera etapa para trabajar sobre los ingresos a la ciudad de Santa Fe". Y luego, agrego que "es un avance importantísimo para seguir dando cumplimiento a aquello que propuso el gobernador de mejorar todos los ingresos a la ciudad, que todos los que conocemos el lugar sabemos del deterioro y de la necesidad imperiosa que esto suceda".

 También destacó que "un primer proyecto tiene que ver con una parte de la iluminación que está llevando adelante la administración de Vialidad, pero esta firma, tiene que ver con la posibilidad concreta de que la UTN lleve adelante estos estudios, ya que tienen carreras afines y suficiente expertís con respecto de la infraestructura vial".

 Por su parte, Ceschi destacó la puesta en valor de toda la Circunvalación de Santa Fe, y detalló que los trabajos se realizarán desde la localidad de Candioti hasta calle Salta de la capital provincial. Y sostuvo que "el proyecto de iluminación fue enviado a Vialidad Nacional para su aprobación y a partir de allí, podremos realizar el correspondiente llamado a licitación, que esperamos hacerlo en las próximas semanas".

 En tanto, Grether consideró que "esta firma de convenio representa un desafío muy importante para la facultad, ya que estamos asumiendo una tarea compleja, pero con la tranquilidad de saber que contamos con las capacidades técnicas para llevar adelante esta clase de proyectos".

 **Convenio con Nación**

 Cabe destacar que el pasado 18 de octubre, el gobernador Omar Perotti y el ministro de Obras Públicas de la Nación, Gabriel Katopodis, rubricaron el compromiso de invertir 10.000 millones de pesos para mejorar los accesos viales de la ciudad de Santa Fe.

 En esa oportunidad, Perotti señaló que "estar generando estas mejoras nos va a cambiar el panorama de la Circunvalación. Y en estos días, la palabra Circunvalación tiene mucho impacto en nuestra ciudad, porque hace poco se puso en marcha el inicio de obras del Plan Circunvalar Ferroviario, el primero del país, con las implicancias que ello tiene en las mejoras de la transitabilidad, para ganar una infraestructura logística que nos permitirá mayor velocidad y transporte de cargas para el desarrollo de la región".

 Además, el gobernador recalcó que "la mirada central no la corrimos, porque nosotros tenemos un gobierno a la par del que trabaja, del que invierte y del que produce".

 **Las intervenciones**

Adecuación de estructuras de los pavimentos a través de sellado de fisuras, bacheos, reparación de losas y/o pavimentación, según corresponda.

Mejorado de banquinas a través del calce o rebaje, tareas de desmalezado, desbosque y/o destronque.

Ejecución de señalización horizontal y acondicionamiento de la señalización vertical, ya sea para reemplazo o incorporación de piezas nuevas.

Acondicionamiento de sistema de defensas flex beam (metálico) con reemplazos, alineamientos e incorporaciones donde sean consideradas necesarias.

Ejecución de barreras New Jersey (hormigón) donde sea necesario y hoy no existan.

Adecuación de transiciones entre sistemas de barreras semi rígidos y tratamientos extremos terminales.

Actividades sobre calzadas de los ramales asociados a las intersecciones existentes, hasta donde se vinculan en camino transversal y calzada principal.

 Cabe destacar, que no se incluyen modificaciones del trazado, ampliaciones, adecuación de desagües y drenajes, ni reformulaciones geométricas de las intersecciones.

 De acuerdo a las cláusulas del convenio, la universidad pondrá a disposición del organismo provincial tanto sus capacidades técnicas y humanas como su infraestructura edilicia para la realización de las tareas.

 **TRAMOS**

Refiere a los tramos: Ruta Nacional A-007 desde calle Lisandro de la Torre hasta el intercambiador de acceso a Santo Tomé (4,7 kilómetros); Ruta Nacional N°11, desde el intercambiador de acceso a Santo Tomé hasta la Ruta Provincial N.º 70 (20,5 kilómetros) y la Ruta Nacional N.º 11 desde la Ruta Provincial N.º 70 hasta Candioti (7,6 kilómetros).