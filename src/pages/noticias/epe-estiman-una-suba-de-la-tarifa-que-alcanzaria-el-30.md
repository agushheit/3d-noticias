---
category: Agenda Ciudadana
date: 2021-03-31T07:35:03-03:00
thumbnail: https://assets.3dnoticias.com.ar/epe.png
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Diario Uno
resumen: 'EPE: estiman una suba de la tarifa que alcanzaría el 30%'
title: 'EPE: estiman una suba de la tarifa que alcanzaría el 30%'
entradilla: El presidente de la EPE, Mauricio Caussi, dijo que la tarifa se definirá
  a mediados de abril y los usuarios pagarán con aumento en mayo.

---
El presidente de la Empresa Provincial de la Energía, Mauricio Caussi, anticipó esta mañana que la compañía definirá hacia mediados de abril un aumento de la tarifa del servicio eléctrico y que los usuarios comenzarán a recibir las facturas con los nuevos valores entre mediados y fines de mayo. La suba, estimó, podría rondar el 30 por ciento.

Caussi afirmó que tanto la EPE como el gobierno de la provincia hicieron fuertes esfuerzos para mantener la tarifa y la competitividad de la empresa, pero aseguró que el aumento de los costos (insumos, combustible, salarios) obliga a estudiar lo que llamó "una readecuación" de la tarifa.

"El objetivo no es la rentabilidad de la EPE sino proveer un servicio de calidad creciente y cuidar el bolsillo de los santafesinos", aseguró. Y añadió: "También, mantener la competitivad de la empresa", dijo en La Ocho.

Destacó que pese al constante aumento de los costos la EPE no dejó de hacer inversiones, especialmente en Rosario, y destacó que el usuario residencial no tuvo aumentos desde diciembre de 2019.

De todos modos, anticipó que la empresa va hacia un esquema tarifario que establezca diferencias entre los usuarios. "Vamos a cuidar especialmente a los jubilados y al segmento que tiene mayores dificultades en términos sociales", sostuvo y sobre el final resaltó que el aumento rondaría el 30 por ciento.