---
category: Agenda Ciudadana
date: 2021-05-23T06:15:24-03:00
thumbnail: https://assets.3dnoticias.com.ar/VCUNAS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Más de un millón de dosis de AstraZeneca llegarán entre este domingo y el
  lunes
title: Más de un millón de dosis de AstraZeneca llegarán entre este domingo y el lunes
entradilla: Serán 204.000 adquiridas por Argentina a través del mecanismo Covax y
  843.000 provenientes de Estados Unidos, que forman parte de la primera entrega del
  contrato suscripto por Argentina por 22,4 millones de dosis.

---
Más de un millón de dosis de vacunas de AstraZeneca arribarán al país entre este domingo y el lunes, informaron fuentes gubernamentales.

Según el cronograma oficial, para este domingo se espera el arribo al aeropuerto internacional de Ezeiza a las 6.15, en un vuelo de la compañía KLM, 204.000 dosis de AstraZeneca adquiridas por Argentina a través del mecanismo Covax.

En tanto, el lunes 24 por la tarde, llegarán otras 843.000 dosis de AstraZeneca provenientes de Estados Unidos que forman parte de la primera entrega del contrato suscripto por Argentina por 22,4 millones de dosis.

"Estamos esperando dosis de Covax que llegan este domingo y el inicio del cumplimiento del contrato directo con AstraZeneca con dosis que están llegando a partir del lunes”, señaló la ministra de Salud Carla Vizzotti a través de un comunicado.

La funcionaria aseguró además que “también tenemos esta semana dos vuelos de Rusia, de Sputnik V con un número importante de dosis”.

En tanto, otro avión de Aerolíneas Argentinas viajará a Moscú, en el decimosexto vuelo de la compañía de bandera a ese destino, para regresar el lunes por la tarde con otra tanda de esa vacuna.