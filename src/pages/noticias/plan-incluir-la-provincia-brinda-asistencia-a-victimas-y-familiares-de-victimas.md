---
category: La Ciudad
date: 2020-12-17T11:19:04Z
thumbnail: https://assets.3dnoticias.com.ar/1712-incluir.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: 'Plan Incluir: la provincia brinda asistencia a víctimas y familiares de
  víctimas'
title: 'Plan Incluir: la provincia brinda asistencia a víctimas y familiares de víctimas'
entradilla: El operativo jurídico gratuito se llevó a cabo en el barrio Acería de
  la ciudad de Santa Fe.

---
**En el marco del Plan Incluir la provincia está realizando en la ciudad de Santa Fe operativos de asistencia legal para víctimas y familiares de víctimas**. Este miércoles, las actividades se llevaron a cabo en la vecinal Juana Azurduy de barrio Acería.

El director del Centro a la Justicia, Caludio Aibinder, dijo que «en este marco de la pandemia, estamos saliendo a hacer asesoramiento jurídico gratuito, apoyo, contención, asistencia a víctimas y familiares de delito desde los Centros de Acceso a la Justicia y Asistencia Judicial».

«En esta primera etapa –continuó– estamos trabajando de manera directa en los barrios que tienen una incidencia directa del Plan Incluir, del corredor de Menchaca. Estuvimos en Las Lomas, Santo Domingo, Juventud del Norte, Cabal, Scarafía y hoy culminando la semana aquí en la vecinal Juana Azurduy de Acería».

Son alrededor de 200 las denuncias recibidas por diferentes delitos y problemas familiares de alimentos. Cabe destacar que en muchos casos también la asistencia legal se amplía a los requerimientos de los vecinos y situaciones particulares. El año que viene está previsto continuar en barrio Los Troncos, Yapeyú, San Agustín I y II y en la Costa.

## **PLAN INCLUIR**

El Plan Incluir es un dispositivo multiagencial y multiministerial que se lleva adelante junto a municipios y al Ministerio Publico de la Acusación (MPA), con el **objetivo mejorar la calidad de vida de vecinos y vecinas en los barrios y la población más vulnerables**, a partir de una intervención integral.

Tiene como eje de gestión el trabajo sobre los barrios y la población más vulnerables, los factores de riesgo asociados a la delincuencia, principalmente en lugares con mayores desventajas sociales, educativas, culturales, comunitarias, físicas y ambientales, con la incorporación de mesas participativas y organizaciones de la sociedad civil.

Del programa participan los ministerios de Desarrollo Social; Seguridad; Salud; Educación; Infraestructura; Ambiente; Cultura; Gestión Pública; la Secretaría de Estado de Igualdad y Género; la Empresa Provincial de la Energía; Aguas Santafesinas S.A.; municipios, comunas y organizaciones sociales.