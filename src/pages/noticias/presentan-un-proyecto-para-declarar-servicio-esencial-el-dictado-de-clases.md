---
category: Agenda Ciudadana
date: 2021-01-16T04:09:35Z
thumbnail: https://assets.3dnoticias.com.ar/aulas-en-pandemia.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: '3DNoticias - Fuente: Télam'
resumen: Presentan un proyecto para declarar «servicio esencial» el dictado de clases
title: Presentan un proyecto para declarar «servicio esencial» el dictado de clases
entradilla: Senadores del interbloque de Juntos por el Cambio presentaron un proyecto
  de Ley que argumenta que el ciclo de escolaridad obligatoria es un derecho humano
  fundamental.

---
Senadores del interbloque de Juntos por el Cambio presentaron este viernes un proyecto de Ley para declarar a la educación como servicio público esencial en el contexto de la pandemia de coronavirus Covid-19 y la discusión entre autoridades nacionales y gremios docentes por el comienzo de las clases.

La iniciativa lleva la firma, entre otras, de los senadores de Laura Rodríguez Machado, Mario Fiad, Stella Maris Olalla y Pablo Blanco, quien afirmó que la pandemia «ha dejado a la luz una verdadera tragedia educativa».

La presentación del proyecto se hizo al mismo tiempo en que representantes del Ministerio de Educación de la Ciudad de Buenos Aires y de los sindicatos docentes del distrito mantuvieron una primera reunión sobre el inicio de clases, en la que el Gobierno porteño ratificó que quieren empezar el 17 de febrero y los sindicatos docentes pidieron garantías sanitarias para volver a las aulas ante la pandemia.