---
category: La Ciudad
date: 2021-12-27T06:15:10-03:00
thumbnail: https://assets.3dnoticias.com.ar/empleo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Pymes santafesinas impulsan una nueva normativa para generar 2.1 millones
  de puestos laborales
title: Pymes santafesinas impulsan una nueva normativa para generar 2.1 millones de
  puestos laborales
entradilla: El Movimiento Nacional de las Pequeñas y Medianas Empresas asegura que
  las medidas tienen más beneficio que costo fiscal.

---
Alejandro Bestani se entusiasma con la posibilidad de presentar en pocas semanas la redacción de un proyecto de ley sectorial Pyme. Asegura que se pueden generar 2,1 millones de puestos de trabajo formal en un año, si se atienden los requerimientos del sector.

 El titular del Movimiento Nacional Pyme (Monapy) comentó que el sector surgió "en medio de la pandemia, como reacción empresaria sobre lo que significa nuestro universo en la economía.

 "Según los datos oficiales, representamos -dijo- el 70% de la generación de empleo, el 42% del PBI y el 70% de la generación de riqueza genuina. Con una eficiencia desconocida en otros sectores: el universo Pyme genera 170 mil millones de dólares -es el aporte al PBI- con un financiamiento de sólo U$ S 6.500 millones".

 Detalló que eso significa que "por cada dólar que nos prestan, aportamos 26 dólares al PBI. Esa ecuación es de 1 a 2 en el caso de grandes empresas. Y el Estado, por cada dólar que toma, aporta 0,5 dólar al PBI".

 **Propuesta de crecimiento**

 El titular del Movimiento Nacional Pyme explicó que aplicando un nuevo modelo de promoción que aborde los tres problemas básicos del sector, "hicimos una simulación mediante una encuesta de la Fundación Observatorio Pyme", con metodología científica y representatividad territorial y de sectores.

 "Se trabajó de manera meticulosa. Incluso haciendo cálculos conservadores, podemos generar 2.100.000 empleos en un año sólo expandiendo las posibilidades de las empresas que ya existen, que son 530 mil. Sin contar nuevas sociedades que puedan surgir".

 Detalló que "retocando los tres vectores sin costo fiscal", se puede obtener "expansión del PBI, más recaudación, más consumo, más actividad y más exportaciones. Se resuelven los problemas de la Argentina, que no genera riquezas desde hace años".

 Incluso señaló que se puede proyectar "expansión del PBI, más recaudación, más consumo, más actividad y más exportaciones. Se resuelven los problemas de la Argentina, que no genera riquezas".

 **Las propuestas**

 El modelo plantea, por ejemplo, que "todo empleo incremental de persona que no está empleada, se le paga salario completo y 11 % de cargas fiscales. Es gente que no estaba aportando por lo que no desfinancia ninguna caja. Si lo hacemos por 5 ó 6 años, permitimos que las Pymes se puedan capitalizar y fortalecer y es probable que toda la masa salarial se pueda pasar al 50% de las cargas.

 Propone además "llevar el financiamiento al sector al 5% del PBI; sólo por relaciones técnicas la expansión del PBI mínima es del 15%. Y hay un 50% de capacidad instalada ociosa, por lo que la inversión necesaria ya está hecha".

 Respecto de una reducción gradual de los impuestos, el esquema señala que el impacto fiscal no es negativo. "La economía argentina tiene 50% de informalidad; bajando el costo fiscal se va a reducir. La formalización aumenta la recaudación".

 Bestani aseguró que "el universo Pyme está dispuesto a dar empleo si se dan estas condiciones. Queremos una Agencia Nacional Pyme que tenga poder de penetrar y modificar la economía real argentina.

 "Lo primero que exigimos -señaló- es la compensación automática de los saldos. No tenemos financiamiento para operar y financiamos al Estado. Nos deben plata todas las provincias y no lo podemos usar para compensar impuestos".

 **LOS QUE "TEMEN"**

"La expansión del PBI si se implementan estas cosas, compensa en recaudación" el costo de las medidas propuestas, según Bestani. "Los que temen, viven del esfuerzo del universo Pyme. Pero con 50% de pobreza, tenemos que probar otros caminos".

 **TRES PROBLEMAS EN COMÚN**

Bestani explicó que a pesar de su peso decisivo en la economía formal, las Pymes no están representadas a la hora de definir las políticas económicas del país. "Tenemos 60 mesas en todo el territorio, con miradas representativas de todos los sectores y regiones", reseñó acerca de Monapy.

El directivo señaló tres problemas en común en las empresas de todo el país. "El primero es el desfinanciamiento crónico. Por cada peso que facturamos, tenemos solamente 4 centavos de financiación. Y las tasas son siempre las más caras para el universo Pyme.

Dijo que el fenómeno conlleva además el problema del "endeudamiento financiero" que se agudizó en los cuatro años hasta 2019, "con tasas de interés por encima del 100%".

Fue enfático al apuntar además que "todos los gobiernos anuncian créditos pero ninguno verifica que eso se moneticen. La plata no llega".

En segundo lugar, Bestani destacó que "no tenemos un régimen impositivo específico. ¿Cómo pagamos una presión tributaria que en la formalidad solamente está en el orden del 45%? Si se suman los saldos a favor, las retenciones, Ingresos Brutos por todo el país, subimos al 70%.

"Además -apuntó- está el régimen laboral, que es otra atrocidad. Habiendo recorrido el país de punta a punta, las cargas sociales van del 65% en adelante".

Detalló que "por la productividad de una persona se pagan dos salarios. Es el empleo que falta en la Argentina, el salario extra que se lleva el Estado infundadamente, inexplicablemente. Las grandes compañías están en las mismas condiciones, pero tienen otros márgenes y un financiamiento 12 veces superior al nuestro".

Además advirtió que "dentro del contexto del régimen laboral hay que dar una solución sí o sí a la industria del juicio. Ningún empresario Pyme va a tomar una persona más si la contingencia de un posible juicio le puede quebrar la compañía.

"No hay que avasallar derechos -dijo ante la consulta- y nadie habla de eliminar el derecho de indemnización. Lo que el empresario discute es el mes por año de antigüedad y las multas que llevan los juicios a cifras inverosímiles. Por eso estamos en 50% de pobreza y tenemos la tasa de desempleo que tenemos. No se van a generar empleos".

 **"UN MAMARRACHO"**

"Si los problemas están presentes en todos lados, ¿qué pasa con las leyes?, se preguntó el titular de Monapy. Y respondió con una evaluación negativa sobre la normativa aplicable al sector.

"Las leyes Pymes son un mamarracho. Son tres o cuatro, dictadas en 35 años, y ninguna anula a la anterior. Suman referencias circulares y el articulado es incomprensible".

Explicó que "los beneficios no se encuentran. La trampa está hecha en el compendio de leyes. Estamos formulando una ley para presentar y promover. Nos dimos cuenta que el trabajo lo tenemos que hacer las Pymes".