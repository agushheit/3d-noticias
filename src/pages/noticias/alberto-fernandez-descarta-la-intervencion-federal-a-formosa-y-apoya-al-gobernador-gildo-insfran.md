---
category: Agenda Ciudadana
date: 2021-01-25T09:23:56Z
thumbnail: https://assets.3dnoticias.com.ar/gildo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Infobae
resumen: Alberto Fernández descarta la intervención federal a Formosa y apoya al gobernador
  Gildo Insfrán
title: Alberto Fernández descarta la intervención federal a Formosa y apoya al gobernador
  Gildo Insfrán
entradilla: La oposición y organismos internacionales advirtieron por la violación
  de los derechos humanos en los centros de aislamiento de la provincia.

---
Alberto Fernández resiste y por el momento seguirá apoyando a Gildo Insfrán. Pese a las presiones de la oposición y de organismos internacionales que alertaron por la violación de derechos humanos en los centros de aislamiento de Formosa, el Presidente descarta la intervención federal de la provincia.

Con la llegada de la pandemia a la Argentina, el gobierno de Formosa es foco de denuncias debido a la administración de la cuarentena. Por decisión de Insfrán, miles de personas estuvieron varadas durante meses sin poder regresar a sus casas debido al cerrojo extremo que impuso el mandatario provincial en el territorio que gobierna desde 1995.

Mientras el resto del país aprende sobre la marcha a convivir con el coronavirus, en esta provincia del norte argentino las libertades continúan cercenadas. Las acusaciones de condiciones inhumanas y hacinamiento a las que son sometidas las personas que se encuentran en los centros de aislamiento provocó una serie de manifestaciones en contra de la decisión de obligar a formoseños a confinarse en una escuela, a pesar de haber dado negativo de coronavirus. La semana pasada dos concejalas opositoras al gobernador que fueron parte de los reclamos terminaron detenidas.

El Gobierno Nacional no se inmuta. Desde Casa Rosada le expresaron a Infobae que por el momento la decisión de Alberto Fernández es apoyar a Insfrán pese al pedido de intervención que planteó Amnistía Internacional. El organismo alertó días atrás por la violación de los derechos humanos en los centros de aislamiento donde se priva de la libertad a ciudadanos que dieron negativo de COVID-19 y que son obligados a permanecer junto a personas que sí están infectadas, y tras la detención de las ediles, Gabriela Neme y Celeste Ruíz Díaz, calificó el accionar del gobernador de “inadmisible en una democracia”.

“Tengo lesiones en la espalda y las muñecas por los empujones que me dieron. Ejercieron la coacción de una manera simbólica demasiado fuerte y después la agresión física”, relató una de las concejalas arrestadas por manifestarse contra el gobierno de Formosa. Al reclamo de Amnistía Internacional se sumaron varios dirigentes y legisladores de Juntos por el Cambio. El último fue el presidente de la Unión Cívica Radical, Alfredo Cornejo quien advirtió que “esta situación puede terminar de la peor manera”.

“Si no interviene el Gobierno Nacional, se van a profundizar estas restricciones a la libertad y puede terminar como pasó en Catamarca con María Soledad o como otros casos en Santiago del Estero”, manifestó el ex gobernador de Mendoza quien consideró que Alberto Fernández “debería ponerse a la cabeza de la situación”.

“Sin la intervención del gobierno nacional, Formosa va a estallar el día que haya un hecho aberrante, como una muerte. No deberíamos esperar a eso. El Gobierno Nacional debería estar metido de lleno en arreglar estos problemas en un marco democrático. Se dan todas las características para una intervención federal”, reclamó Cornejo en declaraciones a LaOnceDiez.

Sea la oposición o Amnistía Internacional, el Presidente va a resistir cualquier pedido de intervención. Hasta el momento no se ha referido públicamente al respecto, pero no se descarta por estas horas que emita su postura.

No obstante, el apoyo institucional lo hizo el Partido Justicialista. A través de un comunicado firmado por funcionarios, gobernadores y militantes del peronismo, el partido avaló el accionar del gobernador al asegurar que Formosa “se destaca especialmente por los indicadores sanitarios logrados en defensa de su población” ante la pandemia de coronavirus.

“La política sanitaria desplegada por el Gobierno provincial logró tener la menor cantidad de contagios y la menor cantidad de muertes por COVID-19 en la Argentina”, respaldó el PJ en un documento titulado “La vida humana: valor fundamental y límite a la mentira”.

El texto fue firmado por el titular del sello José Luis Gioja, Daniel Scioli, Lucía Corpacci, Leonardo Nardini, Rosana Bertone y Antonio Caló.

“Los insólitos pedidos de intervención federal y las permanentes operaciones motorizadas por sectores de la oposición al Gobierno nacional dejan en evidencia la intencionalidad política de la permanente campaña de desprestigio contra Formosa”, señaló el partido.