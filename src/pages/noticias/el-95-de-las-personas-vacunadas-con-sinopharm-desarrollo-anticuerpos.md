---
category: Agenda Ciudadana
date: 2021-07-22T06:15:33-03:00
thumbnail: https://assets.3dnoticias.com.ar/ANTICUERPOS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: El 95% de las personas vacunadas con Sinopharm desarrolló anticuerpos
title: El 95% de las personas vacunadas con Sinopharm desarrolló anticuerpos
entradilla: Así lo demostró un estudio de la Universidad de Sri Jayewardenepura, de
  Sri Lanka, realizado en conjunto con la Universidad de Oxford.

---
El 95% de las personas que recibieron las dos dosis de la vacuna de Sinopharm contra el coronavirus desarrollaron anticuerpos, en tanto que la respuesta frente a la variante Delta, detectada por primera vez en India, fue similar a la que se desarrolla después de la infección natural, según un reciente estudio realizado en Sri Lanka.

El trabajo, que todavía no fue revisado por pares, fue difundido por los investigadores de la Universidad de Sri Jayewardenepura ([https://www.sjp.ac.lk/news/the-sinopharm-vaccine-induces-robust-antibody-responses-in-individuals-in-sri-lanka/](https://www.sjp.ac.lk/news/the-sinopharm-vaccine-induces-robust-antibody-responses-in-individuals-in-sri-lanka/ "https://www.sjp.ac.lk/news/the-sinopharm-vaccine-induces-robust-antibody-responses-in-individuals-in-sri-lanka/")) que realizaron el estudio junto al Consejo Municipal de Colombo en Sri Lanka y la Universidad de Oxford.

La investigación arrojó que las personas de 20 a 39 años presentaron tasas de seroconversión (títulos de anticuerpos) muy elevadas (98,9%), aunque las tasas de seroconversión en individuos mayores de 60 años (93,3%) fueron menores.

Además, la vacuna indujo anticuerpos neutralizantes en el 81,25% de los receptores de la vacuna y estos niveles de anticuerpos neutralizantes fueron similares a los niveles observados después de una infección natural.

Según señaló el comunicado de la universidad, los niveles de anticuerpos contra Delta y Beta (detectada por primera vez en Sudáfrica) "fueron similares a los niveles posteriores a la infección natural, aunque los niveles de anticuerpos fueron más bajos para el Alfa (detectada por primera vez en Reino Unido)".

Y añadió que "los receptores de la vacuna solo tuvieron una reducción de 1,38 veces en los títulos de anticuerpos a Delta en comparación con la variante de Wuhan, en comparación con una reducción de 10 veces a Beta". La vacuna también indujo respuestas de células T y células B de memoria.