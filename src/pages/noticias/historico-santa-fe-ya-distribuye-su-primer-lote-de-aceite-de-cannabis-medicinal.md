---
category: La Ciudad
date: 2021-10-05T06:15:45-03:00
thumbnail: https://assets.3dnoticias.com.ar/lifcannabis.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'Histórico: Santa Fe ya distribuye su primer lote de aceite de cannabis medicinal'
title: 'Histórico: Santa Fe ya distribuye su primer lote de aceite de cannabis medicinal'
entradilla: "Se trata de 926 frascos de “Aceite de Cannabis Medicinal LIF”. Será para
  pacientes de Iapos y atendidos en centros de salud públicos, en forma gratuita.\n\n"

---
La provincia de Santa Fe terminó la elaboración y envasado del primer lote de “Aceite de Cannabis Medicinal LIF” para la atención de la salud de pacientes con epilepsia refractaria. Se trata de un avance histórico en la lucha por dejar atrás la estigmatización que tuvo este producto medicinal que contribuye al tratamiento de esta y otras patologías, tanto en niñas y niños como en adultos. Con ello, en adelante los pacientes ya no precisarán abastecerse de este producto desde el exterior, como lo venían haciendo. Y además, lo harán de forma gratuita.

 El Aceite de Cannabis Medicinal LIF fue elaborado en el Laboratorio Industrial Farmacéutico santafesino, ubicado en el norte de la ciudad de Santa Fe, muy cerca del hospital Iturraspe. Este primer lote se compone de 926 frascos con goteros de 30 ml, con una concentración de 10 por ciento de CBD. Por ese motivo tiene un porcentaje casi inexistente de THC, que es el componente psicoactivo.

 Este primer lote de aceite de cannabis medicinal será distribuido en los hospitales y centros de salud públicos de la provincia y los afiliados de la obra social Iapos en tratamiento por epilepsia refractaria. En la actualidad son 8 los pacientes en tratamiento, según lo informado desde la Provincia, pero estiman que tras este anuncio podría llegar a incrementarse.

 **Pedido del gobernador**

 “Trabajamos desde el año pasado en el Laboratorio a pedido exclusivo del gobernador Omar Perotti”, dijo la directora del Laboratorio Industrial Farmacéutico de Santa Fe (LIF), Lic. Élida Formente.

 “Primero analizamos las distintas alternativas y empezamos dos caminos paralelos”, explicó Formente en diálogo con El Litoral. Uno de los caminos fue importar la materia prima a granel desde Estados Unidos. Esa materia prima “es muy parecida al Charlotte”, dijo la funcionaria del LIF, que es la más utilizada para la elaboración del aceite de cannabis, “y, por otro lado, se importaron los principios activos en cristal -agregó-, para utilizar a futuro en los ensayos clínicos”.

 Con dicho proceso el LIF ya tiene elaborado el primer lote de aceite de cannabis medicinal. “Lo procesamos aquí y lo hacemos analizar en la Facultad de Bioquímica de la Universidad Nacional del Litoral”, que cuenta con las certificaciones necesarias, dijo Formente.

 “Todos los análisis dieron ok, en los parámetros establecidos. Así que pudimos envasar y etiquetar el producto en el laboratorio. Y se estableció un plan de gestión de riesgo para hacer un seguimiento de farmacovigilancia”, explicó la funcionaria a El Litoral.

 **Prescripción**

 El nuevo producto medicinal se brindará a través de una prescripción médica. Por ese motivo, en paralelo el Ministerio de Salud junto al LIF comenzaron un camino para hacer conocer este producto y generar la confianza necesaria en cuanto a su calidad y seguridad, para su futura prescripción. Lo mismo están haciendo con los usuarios, tanto con pacientes como con familiares de los mismos, en el caso de epilepsia refractaria en menores de edad. “Es para que no tengan miedo de usarlo”, dijo Formente. “Porque en algunos casos ha pasado que los padres no encuentran los mismos efectos al pasar de utilizar uno a otro producto”, explicó. “Eso se debe al origen de la materia prima”, agregó.

 **Cómo se pide**

 En adelante, los médicos deberán prescribir el tratamiento con receta doble archivada, una vez que se realiza cada pedido el mismo pasa a la Comisión de Medicamentos, integrada por distintos profesionales que analizan los casos “por vía de excepción” (que no están en el formulario terapéutico). Esa comisión es la que deberá resolver en cada caso si se acepta dicho tratamiento con el aceite de cannabis medicinal. “Por ello es fundamental el trabajo con los prescriptores”, dijo Formente.

 -¿En adelante se continuará elaborando con la materia prima importada de Estados Unidos, o también se está elaborando una producción local?

 -Por ahora importamos la materia prima. Pero al mismo tiempo importamos materia prima en cristal, para futuros ensayos clínicos. Porque queremos contribuir a que se pueda ampliar el ámbito del uso del aceite de cannabis medicinal, que no sea sólo para casos de epilepsia refractaria, sino también para otras patologías. Y para ello son necesarios los ensayos clínicos. En un futuro, sería ideal elaborar nuestra propia materia prima. Para ello firmamos un convenio con INTA y con el Ministerio de Producción para trabajar en la investigación de semillas y hacer pruebas piloto.

 -¿Con qué periodicidad se elaborarán nuevos lotes de aceite de cannabis medicinal?

 -Ahora haremos una nueva compra de materia prima para comenzar la elaboración del segundo lote. Sabemos que la demanda actual del sistema de salud público es muy poca y en Iapos es un poco más amplia. Pero con lo que elaboramos podemos atender las dos demandas. Pero creemos que la oferta generará demanda. Así que queremos estar preparados para dar respuesta.

 **Soberanía sanitaria**

 “Lo importante es que se mejora la accesibilidad, se bajan considerablemente los costos y se obtiene independencia en el camino hacia la soberanía sanitaria, con un producto propio”, reflexionó sobre el final de la entrevista la directora del LIF. “Es muy simbólico que lo tengamos no sólo en la provincia, sino acá, en esta ciudad”, agregó luego.

 También se avanza en la implementación de la Ley 27350 y la 13.602, que se promulgó y reglamentó en 2016 pero nunca se había implementado. Y a su vez la semana pasada se Creó la Comisión Reguladora de esta última ley, a través de la Resolución 2018/21. Mientras tanto se avanza en la creación de un Consejo Asesor, que es un pedido realizado por las madres de pacientes con epilepsia refractaria y las distintas asociaciones vinculadas al uso del aceite de cannabis medicinal. “Esto es muy importante porque es dar respuesta a una demanda de quienes son los protagonistas de este proceso que hoy llega a la elaboración del producto en Santa Fe”, finalizó Formente.