---
category: La Ciudad
date: 2020-12-21T11:29:22Z
thumbnail: https://assets.3dnoticias.com.ar/2112vacaciones.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Diario UNO'
resumen: 'Vacaciones 2020-2021: cuáles son los destinos elegidos por los santafesinos'
title: 'Vacaciones 2020-2021: cuáles son los destinos elegidos por los santafesinos'
entradilla: Si bien el coronavirus y la situación económica hacen dudar a los santafesinos
  para viajar fuera de la provincia, el sector del turismo comenzó a reactivarse con
  destinos sobre todo nacionales.

---
La llegada imprevista del coronavirus hizo que muchos santafesinos tuvieran que adelantar su regreso de vacaciones y otros suspender ese descanso que se planifica anualmente. 

Así, el sector del turismo sufrió también un duro golpe, que los mantuvo sin trabajar por varios meses, pero con la llegada nuevamente del verano, la apertura de fronteras y la implementación de protocolos, se siente un poco de alivio y ya comenzaron las consultas, así como también las reprogramaciones de viajes cancelados por el Covid-19. 

Es por ello que dialogamos con el titular de la agencia de turismo Lunasfe Viajes, Marcelo Vergara, para saber qué es lo que pasa en la ciudad de Santa Fe, si deciden viajar, si será en la Argentina o en el exterior y cuáles son los destinos más buscados.

«Los santafesinos son heterogéneos porque lo que buscan en cuanto destino turístico está ligado a las sierras o al mar», afirmó Vergara, y resaltó: «Si bien comenzó a sentirse nuevamente el movimiento en el sector, es poco aún. En diciembre, enero y febrero por lo que se puede ver, el destino elegido será dentro de la misma provincia y recién a partir de Semana Santa se comenzará a ver el movimiento para otros sitios».

Al hacer referencia a lo que pasa con el sector, el titular de Lunasfe Viajes concluyó: «La situación con el coronavirus y la utilización de protocolos fuera de casa, al mismo tiempo que la situación económica es lo que aún hace dudar a los santafesinos, es una regla general y te lo puede decir cualquiera de los 75 titulares de las agencias que prestan servicios en la ciudad».

En cuanto a los lugares elegidos para vacacionar, en la lista están: Córdoba, el noroeste argentino, Bariloche, Salta, Cataratas, «pero lo que más de moda está en este tiempo es San Martín de los Andes, Ushuaia y Calafate», manifestó. También remarcó que a través del programa de incentivo que lanzó el gobierno nacional, denominado PreViaje, con el cual se devuelve el 50% del costo del paquete a través de un crédito para pagar salidas, excursiones o compras, por ejemplo, fue una opción de los santafesinos, pero recién para el mes de marzo, ya que puede ser que para esa fecha haya más precisiones sobre la situación del coronavirus, sobre todo por la cuestión de la vacuna.

En lo que refiere a viajes al exterior, las consultas fueron casi nulas, solo algunos han preguntado por Brasil o Punta Cana y nada para Europa.

# **Las cancelaciones**

La pandemia trajo aparejado cancelaciones de viajes que en su mayoría se reprogramaron, ya que «por suerte los prestadores y las aerolíneas permitieron hacer el cambio de fecha o dejar el tique abierto hasta diciembre del 2020. Si bien cada aerolínea tuvo su política, fue muy bueno que no impongan un pago de multa ni un cambio en la tarifa. Solo se debía abonar diferencia si la decisión incluía un cambio de destino. En cuanto a viajes que se realizan en bus, fue más fácil el cambio de fecha y también en algunos casos se devolvió el importe del mismo», resaltó el empresario.

# **Protocolos, requisitos y seguros**

Se debe de tener en cuenta que al momento de contratar un viaje o paquete debe de solicitarse un seguro o una asistencia al viajero. «En Santa Fe la mayoría de las agencias está trabajando con dos opciones: seguro de cancelación extra-Covid-19 o una asistencia nacional que es válida para argentinos, residentes y extranjeros en todo el país», destacó el agenciero turístico.

En cuanto al seguro, «funciona realizando un embolso del viaje por diagnóstico positivo para el pasajero o su familia, en caso que sea más de uno quien realice el viaje. En a cuanto a costos, varía según el grupo familiar y los destinos», aclaró Vergara.

Para finalizar también hizo referencia en que en el momento de viajar se chequee cómo es el protocolo a seguir y los requisitos, según el destino deseado.