---
category: La Ciudad
date: 2021-11-23T06:15:29-03:00
thumbnail: https://assets.3dnoticias.com.ar/MMOTOS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'Santa Fe: en un operativo anti picadas, retienen 34 motos "flojas de papeles"'
title: 'Santa Fe: en un operativo anti picadas, retienen 34 motos "flojas de papeles"'
entradilla: Fue durante este fin de semana. La mayoría de los rodados no tenía chapa
  patente ni documentación. Evalúan como "exitosos" los controles nocturnos que se
  vienen realizando.

---
En un operativo nocturno realizado este fin de semana largo, fueron retenidos 34 motos en la Costanera Oeste. La intención fue desactivar posibles "picadas" en esa zona, donde siempre son noticia las carreras ilegales de estos vehículos. La mayoría de los rodados secuestrados no tenía chapa patente ni la documentación exigible.

En el despliegue estuvieron involucrados inspectores municipales asignados exclusivamente a esta tarea, en la que participó la Guardia de Seguridad Institucional (GSI) de la secretaría de Control y Convivencia Ciudadana, y personal policial. 

 Al dato se lo confirmó a El Litoral el subsecretario de Control municipal, Guillermo Álvarez. El funcionario aseguró que no se recepcionaron denuncias de picadas al 0800 (sistema de atención ciudadana del Gobierno local), "lo cual marca que hasta ahora los operativos vienen siendo exitosos. El fin de semana pasado tampoco se registraron denuncias. Es un buen indicador", evaluó. 

 En el caso de este fin de semana largo, intervinieron siete inspectores municipales más uno o dos móviles de la GSI con entre dos y cuatro agentes que colaboran con el personal de tránsito.

 El personal lleva también dos cámaras gopro, que permiten grabar al menos dos horas. Así, se va registrando todo el trayecto de este operativo, que básicamente consiste en recorrer toda la extensión de la Costanera Oeste, sector donde más avisos ciudadanos por picadas de motos se reciben. 

"Lo que se busca es no pararnos en puntos fijos, ya que se sabe que las motos se escapan al ver una posta de control; se meten en contramano, se van por las veredas, poniendo incluso en riesgo la integridad física de los transeúntes. Entonces, los operativos son móviles: se recorre, se filma, se graba y a medida que se detectan motos sin patentes o algún vehículo en infracción, se procede a la retención", amplió el subsecretario.

 "Se entiende que esta es una demanda histórica de los vecinos de la Costanera. Hay otras cosas que se pueden hacer: están proyectadas obras, como por ejemplo colocar 'manda peatones' en más puntos de ese sector de la ciudad. Esto también hará que se reduzca la velocidad de los vehículos que transitan", cerró Álvarez.

 Finalmente, en la tarde del sábado se realizaron controles de alcoholemia en la Ruta N° 1: fueron retenidas cinco motos y una pick up por dar positivo al test. Aquí se trabajó en conjunto con la Agencia Provincial de Seguridad Vial (APSV).