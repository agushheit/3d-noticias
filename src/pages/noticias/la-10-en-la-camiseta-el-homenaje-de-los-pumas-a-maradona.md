---
category: Deportes
date: 2020-12-06T12:19:29Z
thumbnail: https://assets.3dnoticias.com.ar/pumas.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: 'La 10 en la camiseta: el homenaje de Los Pumas a Maradona'
title: 'La 10 en la camiseta: el homenaje de Los Pumas a Maradona'
entradilla: Los Pumas homenajearon a Diego Armando Maradona con un parche con el número
  10 en la camiseta alternativa con la que jugó ante Australia.

---
El seleccionado argentino de rugby, Los Pumas, jugó ante Australia, ante quien empató 16 a 16, por la sexta y última fecha del torneo Tres Naciones, con un parche con el número "10" sobre el brazo izquierdo de la camiseta a modo de homenaje al astro Diego Armando Maradona, fallecido el 25 de noviembre pasado.

Argentina jugó con una casa alternativa de color azul, en vez de la tradicional celeste y blanca con franjas horizontales, y el parche, con número azul sobre fondo blanco, estuvo sobre el hombro.

La complicada semana de Los Pumas en Australia comenzó precisamente cuando el equipo realizó como único homenaje por la muerte del astro el colocarse en el brazo un trozo de cinta adhesiva negra a manera de brazalete, lo que fue muy cuestionado. Los que cuestionan esa situación no saben que el rugby admitió la culpa, y el excoach Marcelo Loffreda se situó como el responsable.

Un gesto mínimo hacia Maradona que despertó una fuerte crítica habida cuenta de que el mundo entero se rindió sin límites y realizó homenajes, conmovido por el fallecimiento repentino del "10", a menos de un mes de haber cumplido 60 años. Igualmente, los jugadores que no fueron responsables de dicho reconocimiento con el brazalete negro pidieron disculpas.

Además, la actitud de Los Pumas encontró como contraste el sábado pasado el inesperado homenaje de los All Blacks, que antes de realizar el tradicional Haka le ofrendó al equipo argentino, a través del capitán Sam Cane, una casaca negra con el "10" y el nombre de Maradona. Vale señalar en este sentido, que el equipo no había tomado dimensión, al no estar en Argentina, de lo que sucedía con Maradona, ya que estaban concentrados para jugar con los All Blacks.

Dicho sea de paso, a Australia no viajó ningún dirigente, simplemente por una cuestión complicada con los protocolos por el coronavirus, y por ello la determinación la tomó Marcelo Loffreda de utilizar el brazalete negro, que sin dudas fue muy poco frente a lo realizado y en comparación por lo demostrado por Nueva Zelanda.

Esa loable actitud no obtuvo respuesta de los jugadores con la camiseta celeste y blanca y, una semana más tarde, llegó el homenaje en ocasión del partido ante los Wallabies. Lo cual no deja de ser positivo, porque habla muy bien del equipo y del staff que lograron hacerlo cuando se pudo. Basta recordar que, por el flojo homenaje, el capitán Pablo Matera pidió disculpas, de algo que los jugadores no tuvieron un mínimo de incidencia.

  
 