---
category: La Ciudad
date: 2021-06-20T06:15:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/CASTRACIONES.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Municipalidad continúa con la campaña gratuita de castración
title: La Municipalidad continúa con la campaña gratuita de castración
entradilla: Con el objetivo de acercar el servicio a todos los barrios de la capital
  provincial, el municipio informa los lugares donde se realizarán castraciones durante
  las próximas dos semanas.

---
La Municipalidad difundió el nuevo cronograma de castración móvil destinada a animales de compañía. Se recuerda que el servicio es gratuito y se ofrece a todos los vecinos y vecinas de la capital de la provincia, con la intención de controlar la población felina y canina.

La atención se brinda con turnos programados que pueden obtenerse en la web oficial www.santafeciudad.gov.ar/turnos. Allí se debe buscar el apartado denominado “Castraciones”, se elige la opción “Solicitar un nuevo turno” y se completa el formulario con los datos pedidos. Posteriormente se escoge el día, el horario y la sede a la cual concurrir.

El esquema para las próximas dos semanas será el siguiente:

**Hasta el 25 de junio**

– Club Pucará, Regimiento 12 de Infantería 2750

– Vecinal Santa Marta, Chubut 6291

– Vecinal San Martin, Estrada 7151

**Del 22 al 25 de junio**

– Estación La Guardia, avenida De Petre y Hermana Serafina

**Del 22 de junio al 2 de julio**

– Vecinal Roma, Tucumán 3957

– Copa de leche Los Sin Techos, Domingo Silva y Lamadrid

**Entre el 28 de junio y el 2 de julio**

– Vecinal Guadalupe Oeste, Ignacio Risso 1745