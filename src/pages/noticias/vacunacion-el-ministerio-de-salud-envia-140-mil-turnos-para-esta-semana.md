---
category: Agenda Ciudadana
date: 2021-06-27T06:00:41-03:00
thumbnail: https://assets.3dnoticias.com.ar/VACUNAS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: 'Vacunación: el Ministerio de Salud envía 140 mil turnos para esta semana '
title: 'Vacunación: el Ministerio de Salud envía 140 mil turnos para esta semana '
entradilla: Piden revisar los mensajes en celulares, el correo o chequear el turno
  en la página oficial del gobierno.

---
Desde las cuentas oficiales del Ministerio de Salud, el gobierno provincial anunció que este fin de semana se darán 140 mil turnos para inocular a personas menores de 45 años sin comorbilidades en toda la provincia para la aplicación de la vacuna contra el Covid 19. Llaman a la población a estar atentos con el mensaje: "Este fin de semana revisá tu celular, tu correo o fijate en [http://santafe.gob.ar](http://santafe.gob.ar "http://santafe.gob.ar") para ver si sos el próximo en vacunarte".

Entre este viernes y  sabado, 140.000 personas inscriptas en el Registro Provincial de Vacunación contra el Coronavirus que tengan menos de 45 años. recibirán su turno para vacunarse entre el lunes 28 de junio y el domingo 4 de julio.

Del mismo modo, este sábado llegaron a la provincia de Santa Fe 96 mil dosis de la vacuna Sinopharm, que se suman a 87.200 Astra Zeneca y 60 mil Sinopharm.

La ministra de Salud, Sonia Martorano, dijo sobre esta etapa de vacunación: "Ya vacunamos 1,3 millones de santafesinos con, al menos, una dosis y más del 25% ya recibieron el esquema completo, desde diciembre, tenemos un objetivo muy claro, dosis que llega, dosis que se aplica, vacunamos a todo ritmo en Santa Fe”.

Por otra parte, Martorano aseguró que la próxima semana llegará al país el segundo componente de la vacuna Sputnik V que permitirá completar esquemas de más personas y concluyó: “Las dosis no se vencen, de todos modos, en el intervalo de 12 semanas se aplican las segundas dosis para obtener mejor desarrollo de anticuerpos”.

**Vacunación**

Santa Fe Vacuna es el plan de vacunación estratégico nacional, gratuito y voluntario que contará con distintas etapas definidas en base a criterios epidemiológicos específicos, como la exposición al virus o el riesgo a enfermedad grave a causa del mismo. El objetivo de la campaña de vacunación COVID-19 es lograr prevenir las complicaciones graves provocadas por la enfermedad COVID-19 en la población más vulnerable.

La vía de administración es IM (intramuscular) en la región deltoides del brazo. Las vacunas antivirus SARS COV2 se presentan en dos dosis con un intervalo mayor a 21 días entre dosis. La protección adecuada que impartirá la vacuna dependerá de la aplicación del esquema completo.

La vacuna, como todo proceso inmunológico, durante las primeras 48 horas después de su aplicación puede provocar síntomas como: dolor, picazón, sensibilidad, enrojecimiento o hinchazón en el lugar de aplicación; dolor de cabeza; escalofríos; pérdida del apetito; dolor muscular; dolor en las articulaciones; sudoración.