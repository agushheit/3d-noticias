---
category: Estado Real
date: 2021-04-01T08:08:36-03:00
thumbnail: https://assets.3dnoticias.com.ar/control.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: La Provincia refuerza controles en rutas durante Semana Santa para prevenir
  siniestros viales
title: La Provincia refuerza controles en rutas durante Semana Santa para prevenir
  siniestros viales
entradilla: La Agencia Provincial de Seguridad Vial dispuso una mayor cantidad de
  operativos para abordar el aumento del flujo vehicular que se espera por la celebración
  de la Pascua. Habrá 70 puestos de controles de alcoholemia.

---
La Agencia Provincial de Seguridad Vial (APSV), dependiente del Ministerio de Seguridad, dispuso a partir del miércoles por la tarde y hasta última hora del domingo 4 de abril una mayor presencia en las principales rutas y accesos de la provincia debido a los viajes que se realizarán por Semana Santa, previendo un incremento en la cantidad de vehículos que circularán por rutas del territorio santafesino.

Para ello se implementarán operativos de control y fiscalización en 70 puntos de la provincia donde se realizará ordenamiento del tránsito, detención de vehículos para control de documentación, de exceso de pasajeros y uso de dispositivos de seguridad, entre otras faltas, que apuntan a salvar vidas a través de la fiscalización de las conductas de riesgo.

Por otra parte se incrementarán los controles de alcoholemia con el propósito de desestimar el consumo de alcohol al conducir y evitar el riesgo generado por los efectos de esta sustancia en el organismo.

Al respecto el subsecretario de la Agencia Provincial de Seguridad Vial, Osvaldo Aymo, destacó que “toda la Policía Vial estará abocada a prevenir siniestros en las rutas a través de un refuerzo de personal y puntos de control. Una de las conductas más riesgosas es el consumo de alcohol, por eso estaremos presentes con 112 operativos en toda la provincia. La presencia sistemática del Estado es la forma más efectiva de disuadir a los conductores de adoptar esta práctica”.

Por otra parte, la Directora Provincial de la APSV, Antonela Cerutti, sostuvo que “este fin de semana se van a reforzar los controles en las rutas que atraviesan nuestra provincia, donde el tránsito se verá incrementado a raíz del turismo. En este contexto es muy importante la responsabilidad de todos en la conducción así como los cuidados que debemos mantener ante el aumento de contagios por Covid 19”.

**Cosecha gruesa**

Desde la Agencia Provincial de Seguridad Vial también recuerdan que nos encontramos en el período de Cosecha Gruesa que afecta principalmente a las rutas de la región sur de la provincia, sobre todo en la zona del llamado “cordón industrial”. Esta situación genera una afluencia masiva de transporte pesado en las rutas con marcada circulación nocturna; muchas veces con condiciones climáticas adversas y coincidente, además, con feriados que propician mayores viajes por parte de vehículos particulares. Es por eso que se están llevando a cabo acciones de monitoreo y control que permitan resguardar la seguridad de los transportistas y de los demás conductores que circulan por la zona.

**La niebla como factor de riesgo**

La niebla es otro factor que puede acompañar a los viajeros durante estos días de feridado ya que es un fenómeno climatológico característico de esta época del año que se manifiesta con mayor intensidad en ciertas áreas de la provincia y que configura un factor de riesgo determinante a la hora de trasladarse.

Este fenómeno atmosférico reduce drásticamente la visibilidad horizontal aumentando considerablemente las posibilidades de sufrir un siniestro vial si no se toman las medidas de precaución necesarias para una conducción prudente. Desde la APSV apelan a tomar todos los recaudos posibles para minimizar este riesgo y refuerzan algunas recomendaciones tales como:

\- Circular con las luces bajas encendidas

\- Aumentar la distancia con el otro vehículo

\- Reducir la velocidad sin hacerlo de manera brusca

\- Es importante no encerder luces altas ni inintermitentes

\- Ante una situación de nula visibilidad buscar un lugar seguro para detenerse

**Conductas responsables**

En el marco de una fuerte presencia de la Policía de Seguridad Vial (PSV) en las rutas que apuntará a evitar y prevenir situaciones de riesgo, la Agencia Provincial de Seguridad Vial apela a la conciencia de los conductores para contribuir a generar rutas más seguras. El organismo provincial reforzará la difusión de recomendaciones y pautas de conducción a través de sus redes sociales y medios de comunicación con el fin de generar ámbitos responsables de movilidad los cuatro días que se avecinan.

En este sentido, la APSV brindó algunos consejos de circulación para tener en cuenta a la hora de realizar viajes en ruta:

\- No beber alcohol si se va a conducir ya que incluso una pequeña cantidad altera las facultades para conducir, educe el tiempo de reacción y la capacidad de coordinación.

\- Respetar los límites de velocidad permitidos. Para ello es fundamental planificar el viaje y salir con tiempo.

\- Antes de realizar una maniobra de adelantamiento evaluar si la maniobra puede hacerse en condiciones seguras, sobre todo verificar la distancia y la visibilidad.

\- Usar el cinturón de seguridad, tanto el conductor como los demás pasajeros. Las embarazadas deben pasar el cinturón por debajo del abdomen, entre los senos y por encima de la clavícula. Los niños deben viajar en el asiento trasero con el Sistema de Retención Infantil (SRI) adecuado para cada edad.

Con respecto a los traslados dentro y fuera de la provincia, la Agencia Provincial de Seguridad Vial remarcó que es imprescindible mantener los cuidados sanitarios para evitar contagios de Covid 19 junto a las precauciones de seguridad vial. Para viajar se exige la declaración jurada que se obtiene mediante la aplicación Nacional “Cuidar”.