---
category: Agenda Ciudadana
date: 2021-01-21T08:52:49Z
thumbnail: https://assets.3dnoticias.com.ar/negri.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Lt10
resumen: Para Negri, el gobierno no genera confianza con el plan de vacunación
title: Para Negri, el gobierno no genera confianza con el plan de vacunación
entradilla: 'El titular del bloque de la UCR en la cámara baja insistió con el reclamo
  de información clara y concreta sobre las vacunas con las que contaremos y los plazos
  para aplicarlas.  '

---
En el marco del plan nacional de vacunación que lleva adelante el gobierno nacional con la Sputnik V y el miedo por la segunda ola de la pandemia que podríamos padecer en el otoño, LT10 habló con Mario Negri, diputado nacional, presidente del Bloque UCR y del Interbloque Juntos por el Cambio

El legislador opositor expresó que “nosotros queremos que el Gobierno brinde información clara a los argentinos y no se genere ni desconfianza, ni incertidumbre. Desde abril del año pasado, el ministro de Salud no volvió al Congreso”.

Además,  el diputado nacional agregó que “a la desconfianza con las vacunas la generó el Gobierno. El año pasado nos dijeron que Pfizer nos iba a dar 750 mil vacunas porque pusimos 1000 voluntarios. Hoy no sabemos por qué no hubo acuerdo con este laboratorio.  Luego nos enteramos un día que Vizzotti estaba en Moscú y que no era para los mayores de 60 porque lo dijo Putin”.

“Yo no tengo problema con el origen de la vacuna. Necesitamos tener la información para quedarnos tranquilos y confiar en los organismos científicos. Ojalá el plan de vacunas salga de la mejor manera”, subrayó el presidente del Bloque UCR.       

Finalmente, integrante de Juntos por el Cambio remarcó que “el Gobierno tiene que ordenar su gestión que es muy mala y caótica. Si no mejoran la conducción va a chocar”.