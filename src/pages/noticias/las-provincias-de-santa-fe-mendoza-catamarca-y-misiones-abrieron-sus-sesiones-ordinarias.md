---
category: Agenda Ciudadana
date: 2021-05-02T06:00:37-03:00
thumbnail: https://assets.3dnoticias.com.ar/sesiones.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Las provincias de Santa Fe, Mendoza, Catamarca y Misiones abrieron sus sesiones
  ordinarias
title: Las provincias de Santa Fe, Mendoza, Catamarca y Misiones abrieron sus sesiones
  ordinarias
entradilla: Cuatro provincias abrieron las sesiones ordinarias de sus parlamentos
  en asambleas legislativas, donde el eje central de los discursos fue la pandemia.

---
Las provincias de Santa Fe, Mendoza, Catamarca y Misiones abrieron las sesiones ordinarias de sus parlamentos en asambleas legislativas marcadas por las restricciones que impone la pandemia de coronavirus y con referencias a las medidas de restricción aplicadas para frenar la segunda ola de contagios.

**Santa Fe**

El gobernador de Santa Fe, Omar Perotti, inauguró el 139° período de sesiones ordinarias de la Legislatura y dijo que la provincia "se encuentra mucho mejor preparada para afrontar" la segunda ola de coronavirus.

En su discurso de apertura, Perotti le deseó "una pronta mejoría y recuperación al ingeniero Miguel Lifschitz", su antecesor en el cargo internado en terapia intensiva por un cuadro de Covid-19, a quien mencionó como "un luchador que le está dando pelea al Covid".

Ante un auditorio reducido en número de presencias por razones sanitarias, Perotti comenzó su alocución con un mensaje de afecto para los familiares de las víctimas del coronavirus, les agradeció a los habitantes de su provincia por el "enorme esfuerzo que están realizando" y consideró que en esta pandemia "nadie se salva solo".

El gobernador destacó que Santa Fe "se encuentra mucho mejor preparada para afrontar la segunda ola" de la pandemia y manifestó que "se duplicaron las camas críticas, y en algunas regiones se triplicaron", pasando de 154 a 398 en el sector público y que en articulación con el sector privado se cuenta con 1036.

Asimismo, afirmó que el número de respiradores disponibles en la provincia creció un 120%; anunció que en los próximos meses se invertirán 5.500 millones de pesos en infraestructura sanitaria y consideró el operativo de vacunación es un "esfuerzo sanitario, logístico y de recursos humanos sin precedentes, con 236 vacunatorios en 323 localidades".

**Mendoza**

En tanto, el gobernador de Mendoza, Rodolfo Suarez, defendió la apertura comercial y la presencialidad en las aulas en medio de la segunda ola de coronavirus, al abrir esta mañana el 181° período de sesiones ordinarias en la Legislatura, de manera semi-presencial.

Suarez agradeció en su discurso al personal médico por su labor en el marco de la pandemia, y pidió el compromiso de la ciudadanía en el respeto de los protocolos para reducir el impacto de la segunda ola, con el propósito de que "Mendoza y su economía sigan abiertas".

El gobernador hizo un balance de su gestión, anunció los planes previstos para este año y consideró que los establecimientos educativos son "sitios seguros y constituyen una herramienta de equidad social indispensable, particularmente para los grupos más vulnerables".

En ese sentido, expresó que por eso la escuela "debe estar abierta todo el tiempo que se pueda, de manera que el esfuerzo por mantener la presencialidad debe ocupar un lugar de prioridad excluyente en el diseño de cualquier estrategia epidemiológica".

**Catamarca**

Por su parte, el gobernador de Catamarca, Raúl Jalil, al inaugurar el 132° período de sesiones ordinarias de la Legislatura destacó las medidas "rápidas y efectivas" que se adoptaron para contener los casos de coronavirus y advirtió que la provincia tiene "la letalidad más baja del país".

Jalil, que estuvo acompañado en el Cine Teatro Catamarca por legisladores sentados butaca de por medio y respetando los protocolos sanitarios, hizo un discurso que tuvo como eje la situación sanitaria por la pandemia de coronavirus y dijo que "el año 2020 nos enfrentó a un desafío único".

"Enfrentar esta pandemia fue un trabajo amplio y en equipo con el apoyo de todos, donde tomamos decisiones rápidas para preservar la salud y el sistema sanitario, decisiones que finalmente fueron efectivas", destacó el gobernador y manifestó que "los fuertes controles para el ingreso a la provincia ayudaron a ganar tiempo para la construcción en un hospital Monovalente especializado para la atención de Covid-19".

Asimismo, expresó que "con un sistema de salud mejorado recibimos en julio del año pasado los primeros casos de Covid-19" y sostuvo que "pudimos contener la curva de contagios durante la primera ola siendo la última provincia en registrar casos".

Jalil anunció la construcción de "un polo de Salud Pública, la ampliación y de nuevos hospitales para interior de la provincia con fondos nacionales" y resaltó la campaña de vacunación que lleva adelante la provincia al señalar que "desde enero sostenemos un operativo de vacunación elogiado por especialistas del país".

Al referirse a la educación, afirmó que "para 2021 sabemos que uno de los grandes desafíos es la bimodalidad entre clases presenciales y teleclases, dependiendo de la evaluación sanitaria que se haga en cada jurisdicción".

El mandatario señaló que "la crisis generada por la pandemia no es solo sanitaria sino social y afecta directamente a la matriz de empleo por lo que se requiere un esfuerzo de contención social y asistencia al sector privado".

**Misiones**

Por su parte, el gobernador de Misiones, Oscar Herrera Ahuad, inauguró un nuevo período de sesiones ordinarias de la Legislatura con un discurso de más de dos horas, en el que ratificó "el rumbo de un modelo de gobierno" y llamó a evitar contagios "para sostener el sistema productivo, comercial y de servicios".

El gobernador destacó que su gestión se "consolida en varios ejes y líneas de acción", manifestó la necesidad de reforzar el sistema sanitario ante la pandemia y "trabajar en una fuerte concientización de cada uno sobre las medidas de cuidado".

Herrera Ahuad dijo que "debemos evitar contagiarnos para sostener el sistema productivo, comercial y de servicios" y manifestó que "hay que garantizar que la actividad económica siga funcionando, que la cadena de pagos no se interrumpa, y que cada trabajador siga percibiendo sus haberes".

Asimismo, expresó que desde el Fondo de Crédito Misiones se atendieron "necesidades que emergieron del contexto de pandemia de todos los sectores de la economía, triplicando el monto total del financiamiento en relación a los años anteriores en diferentes programas" y precisó que se brindaron "subsidios a trabajadores independientes del sector turístico que no calificaban en beneficios nacionales".

"Diseñamos e implementamos un plan de asistencia financiera a los prestadores del Instituto de Previsión Social, por una suma de 500 millones de pesos, cubierto en su totalidad por fondos misioneros, adelantando el pago de prestaciones para meses futuros, en virtud de la caída de las atenciones en los primeros meses del aislamiento obligatorio", destacó el mandatario provincial durante su discurso en el que hizo un balance de su gestión.