---
category: Agenda Ciudadana
date: 2020-12-30T12:10:23Z
thumbnail: https://assets.3dnoticias.com.ar/3012-senado.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Infobae'
resumen: 'Aborto: senador por senador, quiénes votaron a favor y quiénes en contra'
title: 'Aborto: senador por senador, quiénes votaron a favor y quiénes en contra'
entradilla: La Cámara Alta sancionó la normativa con 38 votos a favor, 29 en contra
  y 1 abstención.

---
En una histórica y emotiva sesión de 12 horas, la Cámara de Senadores de la Nación convirtió en ley el proyecto de Interrupción Voluntaria del Embarazo (IVE) frente a una multitud de militantes “celestes” y “verdes” que permanecieron durante toda la madrugada en la Plaza del Congreso.

La sanción tuvo más votos a favor de los esperados durante los últimos días: 38 senadores, entre peronistas, radicales y del PRO. Del otro lado, tuvo 29 en contra, también con un corte igual de transversal, además de una abstención. Fue el Frente de Todos el espacio que más votos aportó, aunque uno de sus máximos adversarios fue el jefe del bloque, José Mayans. Hubo dos ausencias que también restaron votos "celestes": el ex gobernador de San Luis, Adolfo Rodríguez Saá, y la senadora riojana Blanca Vega. La abstención de Guillermo Snopek, peronista de Jujuy, también se descontó a ese sector.

«Hoy somos una sociedad mejor que amplía derechos a las mujeres y garantiza la salud pública», celebró el presidente Alberto Fernández tras la sanción de la ley.

**En esta nota, la nómina oficial de cómo votó cada legislador:**

![](https://assets.3dnoticias.com.ar/29122020-acta-1.webp)