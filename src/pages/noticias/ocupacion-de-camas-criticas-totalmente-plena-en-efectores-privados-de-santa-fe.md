---
category: La Ciudad
date: 2021-04-20T07:24:24-03:00
thumbnail: https://assets.3dnoticias.com.ar/terapiajpg.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Diario Uno
resumen: Ocupación de camas críticas "totalmente plena" en efectores privados de Santa
  Fe
title: Ocupación de camas críticas "totalmente plena" en efectores privados de Santa
  Fe
entradilla: Según la Asociación de Clínicas y Sanatorios, el pasado fin de semana
  colapsaron las atenciones intensivas No Covid en los hospitales públicos de la ciudad,
  derivando pacientes a efectores privados.

---
Desde la Asociación Sanatorios, Clínicas y Sociedades de Asistencia Médica de la ciudad (Sanycli) manifestaron su gran preocupación por la incesante demanda en ocupación de camas críticas en efectores privados. A la actualidad, hay ocupación "totalmente plena" en las salas de cuidados intensivos de los centros de salud privados de Santa Fe, tanto de pacientes Covid como No Covid.

El presidente de Sanycli, Carlos Abraham, manifestó a UNO Santa Fe: "Tenemos ocupación totalmente plena. Todo el tiempo estamos monitoreando junto al Ministerio de Salud y en contacto directo con los hospitales la situación de camas, revisando cuándo se desocupa alguna para atender pacientes tanto Covid como No Covid en los efectores privados".

Actualmente en los efectores privados de la ciudad hay un 50/50 en la relación entre pacientes atendidos con patologías Covid y No Covid. Aquí pesa la cuestión sanitaria donde si no hay sectores de cuidados intensivos aislados según atenciones Covid y No Covid no se pueden mezclar las camas de pacientes con diversas patologías. Los sanatorios que tienen una sola unidad de cuidado intensivo deben destinar esa área para un solo propósito.

Según Abraham, la apremiante situación en cuanto a la escasez de camas se da "más allá de que se haya suspendido la atención programada, que no ocupa camas de cuidados intensivos". Y agregó: "Los efectores privados en Santa Fe no tienen una capacidad muy importante, ni tampoco pueden expandirse".

**Colapso de camas No Covid en Santa Fe**

El último fin de semana, los hospitales públicos tuvieron una saturación de camas No Covid, por lo que se gestionaron "entre tres y cuatro camas No Covid" para estos pacientes, según la información a la que accedió UNO. Sobre esto, el presidente de Sanycli destacó: "Tenemos un convenio con el Ministerio de Salud para estas situaciones".

Según las declaraciones del secretario de Salud, Jorge Prieto, la ocupación de camas en efectores públicos de la provincia es del 91% a la actualidad. En Santa Fe, "el hospital Iturraspe tiene solo dos camas disponibles de terapia intensiva, una Covid y una No covid", aseguró. En Rosario, los centros privados están "sin camas disponibles", según el funcionario.

En conjunción con la situación reflejada en los efectores públicos de la provincia, Abraham indicó que "es notorio que las edades de los pacientes en cuidados intensivos han bajado mucho, sobre todo en los hospitales. Hoy la franja etaria de entre 40 a 60 años es la más afectada".

> **"A este ritmo no van a alcanzar las camas"**

"La mayor preocupación reside en lo que está haciendo la sociedad en su conjunto. Si seguimos a este ritmo no van a alcanzar las camas. Hay un contrasentido en la consulta diaria por las camas y lo que sucede a nuestro alrededor. Es muy desalentador para el personal de Salud, hay un nivel de individualismo brutal que genera una desazón absoluta", indicó Abraham.

A su vez, Abraham subrayó que actualmente no hay problemas con el abastecimiento de insumos. "Todos los problemas que tuvimos el año pasado –con respecto por ejemplo a la escasez de barbijos– ya están solucionados. No se solucionó el tema de los precios porque no han bajado todavía, pero no hay faltantes", remarcó.

"Estamos haciendo todos los esfuerzos del mundo para garantizar la atención, pero si esto sigue de esta forma suponemos que puede haber una crisis, ojalá que no sea así", concluyó.