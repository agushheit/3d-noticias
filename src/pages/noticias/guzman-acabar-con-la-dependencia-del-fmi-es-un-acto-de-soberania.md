---
category: Agenda Ciudadana
date: 2021-10-25T06:00:03-03:00
thumbnail: https://assets.3dnoticias.com.ar/GUZMAN.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: 'Guzmán: "Acabar con la dependencia del FMI es un acto de soberanía"'
title: 'Guzmán: "Acabar con la dependencia del FMI es un acto de soberanía"'
entradilla: El ministro de Economía expuso en un panel titulado "Cómo salir de la
  trampa de la deuda eterna".

---
El ministro de Economía, Martín Guzmán, aseguró este domingo que el crédito por US$ 45.000 millones que el gobierno conducido por Mauricio Macri selló con Fondo Monetario Internacional (FMI) es "el principal problema" que heredó la actual administración nacional y que "acabar con la dependencia del FMI es un acto de soberanía", motivo por el cual se está trabajando para llegar a un acuerdo de reestructuración en el cual el principio de soberanía económica sea "absolutamente innegociable".  
  
Al respecto, recordó que el exdirector por Estados Unidos en el FMI (Mauricio Clave Carone) en aquel entonces reconoció públicamente que aquel programa fue un "apoyo político" a su campaña electoral y que es "ahora el pueblo argentinos el que lo está pagando".  
  
"Nosotros estamos tratando de refinanciar esa deuda en cuotas, de modo que que no impida el desarrollo de las oportunidades de nuestro pueblo", apuntó esta tarde Guzmán sobre las negociaciones que se están llevando con el organismo, en el marco de un panel titulado "Cómo salir de la trampa de la deuda eterna" del que participaron el exministro de Finanzas de Grecia, Yanis Varoufakis, y el economista y exministro de Conocimiento y Talento Humano del Ecuador, Andrés Arauz.  
  
"Acabar con la dependencia del FMI es un acto de soberanía. Que esté el FMI en la Argentina es un gran problema desde la construcción de la política económica desde la soberanía. Por eso negociamos una solución de una forma que ese principio sea absolutamente innegociable", señaló el mismito.

Durante su exposición, Guzmán recordó las dificultades que debió atravesar el Gobierno actual para alcanzar un acuerdo de reestructuración de deuda con lo acreedores privados en 2020, dado que "cuando uno negocia con Wall Street está negociando con los acreedores más poderosos del mundo".  
  
"Hay un campo muy complicado donde lo que abunda es el lobby y se ve todo el tiempo en al comunicación pública. Todo el mundo está empujando al acuerdo rápido a cualquier valor, que busca asustar y que contribuye a que los acuerdos no sean buenos", afirmó.  
  
En ese sentido, dijo que gracias a resistir las presiones se pudo alcanzar un acuerdo "sano" para el país que le permitió ahorrar unos US$ 35.000 millones en el plazo de 10 años, además de un recorte de capital de casi el 2%.  
  
De cara a lo que se está negociando con el organismo internacional, Guzmán aseveró que "el problema de la balanza de pagos hoy es el FMI y lo trajo Macri" y que, para saldar ese problema, "estamos buscando un acuerdo sobre nuestra programación económica".  
  
"Buscamos un acuerdo que a la larga sea sostenible. Ahora, es tan grande la deuda que va a llevar distintos pasos poder ir resolviendo este problema", destacó.  
  
Y se preguntó: "¿Se puede patear el tablero? ¿Decir la deuda se acabó, fuera el FMI? Hay que entender que el rival también juega y estamos hablando de una relación entre un Estado nación y el resto de los estados nación del mundo, de la integración de la Argentina al mundo, que favorezca las oportunidades de desarrollo de nuestra economía real"  
  
"Un mal acuerdo, que socave las posibilidades del pueblo argentino y que le generen un problema de credibilidad al Estado sería lo que le pasó al gobierno de Juntos por el Cambio. Hubo tanto optimismo en 2016 en parte del mundo que después, con el colapso del modelo económico, la decepción fue proporcional al daño que le provocó al pueblo argentino", dijo Guzmán.  
  
Sobre el cierre de su exposición, Guzmán recordó que "el Gobierno conduce pero es importante que los distintos sectores del poder económico acepten la premisa de nuestro gobierno de buscar un acuerdo que funcione, no cualquier acuerdo".  
  
"Esto sólo puede lograrse con un proyecto que ponga al pueblo trabajador como protagonista. El único espacio político que ofrece esto es el nuestro", concluyó.