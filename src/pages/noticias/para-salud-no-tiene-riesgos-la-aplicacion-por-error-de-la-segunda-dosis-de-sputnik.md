---
category: La Ciudad
date: 2021-05-08T06:00:42-03:00
thumbnail: https://assets.3dnoticias.com.ar/cemafe.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Para Salud "no tiene riesgos" la aplicación por "error" de la segunda dosis
  de Sputnik
title: Para Salud "no tiene riesgos" la aplicación por "error" de la segunda dosis
  de Sputnik
entradilla: Jorge Prieto, secretario de Salud, remarcó que se hará una medición de
  anticuerpos a los profesionales que fueron vacunados con la segunda dosis de la
  Sputnik V en el Cemafe y aún no habían recibido la primera.

---
Cinco psicólogos de la ciudad fueron vacunados por error con el segundo componente de la vacuna Sputnik V en lugar del primero. Tras el "error programático" (así lo calificaron las autoridades de Salud) sucedido el pasado martes en el Centro de Especialidades Médicas de Santa Fe (Cemafe), el director Fabián Mendoza hizo referencia a este error: "Fue identificado este error programático inmediatamente de sucedido. Se consultó e informó a las autoridades ministeriales que tienen que ver con la vigilancia farmacológica. Se comunicó a los pacientes y se les informó lo sucedido".

A su vez comentó que a los pacientes que recibieron las vacunas invertidas, "se les hará una medición de anticuerpos para ver la eficacia en el sistema inmunitario de estos pacientes, y ponemos a disposición a infectólogos y especialistas para esta situación", y agregó que "no debería haber ningún inconveniente en invertir el sentido de los componentes de las vacunas".

De la conferencia de prensa ofrecida para informar sobre esta polémica situación también participó Jorge Prieto, secretario de Salud, quien señaló que "el componente uno y el componente dos es el mismo, lo que cambia es la espícula viral. Eso hace que si yo contraigo coronavirus, me pongo en contacto con ese virus, mi cuerpo reconoce que lo tuvo y si yo generara pacientes que han tenido coronavirus y se inyectan la primera dosis, el reconocimiento es inmediato y la reacción es mayor".

Además, explicó y reiteró Prieto que "este caso donde se ha alterado el orden de los dos productos ya fue denunciado ante la SADI (Sociedad Argentina de Infectología). Hemos colocado 700 mil vacunas y solamente un frasco de cinco dosis fue invertido y esto no genera ningún riesgo. Lo que se hará es un seguimiento de anticuerpos".

Prieto admitió que se trató de un error técnico y de programación. Al mismo tiempo agregó que "tenemos un sistema de salud muy tensionado y agotado. Creo que es algo a remarcar y hay que hacer un gran reconocimiento al personal de salud que redobla el esfuerzo. Esto simplemente fue un error humano y hay que reconocer que se enfrentó la situación, se comunicó a los profesionales que no hay ningún riesgo en virtud de esto".

**Continuidad de vacunación**

El secretario de Salud informó que se están terminado de aplicar las vacunas a los mayores de 60 años y se continúa con los menores de 59 con comorbilidades, "que son estratificados. Primero van los diabéticos insulino dependientes; pacientes con obesidad mórbida; personas con déficit intelectual. La intención es continuar con la población objetivo que tenemos hasta el momento", sostuvo Prieto.

La semana que viene seguirá la aplicación de la segunda dosis de la Sinopharm para los docentes, personal de seguridad y algunas personas del área salud. "Se anunció que en la semana del 20 de mayo ingresarían más dosis para continuar con el plan de vacunación", anticipó el funcionario.