---
category: La Ciudad
date: 2021-05-24T06:00:06-03:00
thumbnail: https://assets.3dnoticias.com.ar/COSTANERA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Domingo de confinamiento; la Costanera lució con mucho movimiento
title: Domingo de confinamiento; la Costanera lució con mucho movimiento
entradilla: Pese al confinamiento y a las restricciones de circulación, el espacio
  de esparcimiento por excelencia de los santafesinos se presentó muy concurrido.

---
Desde el sábado, gran parte del país ingresó en un confinamiento estricto y riguroso; similar al del año pasado con la fase 1. El nuevo Decreto de Necesidad y Urgencia (DNU) del presidente Alberto Fernández, al cual adhirió el gobernador Omar Perotti, establece nuevas restricciones que tienen como objetivo disminuir la circulación de personas y bajar la escalada de contagios de coronavirus que pone en jaque al sistema sanitario. Pero la realidad mostró este domingo en la ciudad de Santa Fe, puntualmente en la Costanera Oeste, una concurrencia importante para las restricciones vigentes.

Durante la mañana de domingo, el clima nublado, frío y ventoso, apenas dejó observar sobre la Costanera Oeste santafesina algunas personas corriendo o caminando. Pero ya en horas de la tarde, con el sol que definitivamente les ganó a las nubes en el cielo, de la mano de una temperatura más agradable, el escenario a la vera de la Laguna Setúbal se presentó muy concurrido.

Parejas y grupos de amigos paseando o sentados en distintos puntos de la Costanera, corredores y ciclista haciendo actividad física, familias con sus hijos disfrutando del aire libre y una gran cantidad de gente en general caminando por la zona de Bulevar Gálvez con destino a la Costanera Oeste.

Según las nuevas medidas de convivencia vigentes desde el 22 de mayo hasta el 30 del mismo mes, sumando también el fin de semana del 5 y 6 de junio en todo el territorio provincial, no se necesita permiso para salir a caminar en las cercanías del domicilio siempre de 6 a 18. La pregunta sería: todos los santafesinos que se observaron en la Costanera Oeste este domingo particularmente, o en otros espacios públicos en general, ¿vivían en proximidad de dichos lugares?

**Salidas recreativas**

En el decreto provincial se establece que se podrán realizar salidas de esparcimiento en espacios públicos, al aire libre, de cercanía, en horario autorizado para circular (desde las 6 y hasta las 18), y dando cumplimiento a las reglas de conducta generales y obligatorias establecidas y en ningún caso podrán realizarse reuniones de personas, ni concentraciones, ni prácticas recreativas grupales, ni circular fuera del límite del partido, departamento o jurisdicción del domicilio de residencia.

Que en relación a las actividades señaladas y a los desplazamientos necesarios para realizar las mismas, precisa el Decreto que no será necesario contar con autorización para circular.

En relación a los controles policiales dentro del confinamiento, no se observaron retenes ni controles durante el día domingo en la zona de la Costanera Oeste santafesina, en el área comprendida entre el Puente Colgante y el Faro.