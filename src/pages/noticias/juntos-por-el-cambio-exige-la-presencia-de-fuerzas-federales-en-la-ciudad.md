---
category: La Ciudad
date: 2021-10-19T06:15:46-03:00
thumbnail: https://assets.3dnoticias.com.ar/chuchi.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Juntos por el Cambio exige la presencia de fuerzas federales en la ciudad
title: Juntos por el Cambio exige la presencia de fuerzas federales en la ciudad
entradilla: Adriana "Chuchi" Molina junto a los candidatos a Concejales y Senadores
  nacionales de JXC, pidieron la presencia de fuerzas federales en Santa Fe, necesarios
  frente a la situación de inseguridad que se vive en la ciudad.

---
Adriana "Chuchi" Molina" junto al resto de los integrantes de la lista de candidatos a concejales de Juntos por el Cambio, diputados nacionales del espacio y actuales ediles, brindaron una conferencia de prensa en en la Plazoleta Martín Fierro (San Martín y Cándido Pujato), sitio emblemático por tener un mural que recuerda a las víctimas de la inseguridad en la ciudad, sobre la situación en la ciudad y el pedido del conocer detalles sobre la llegada de agentes federales a nuestra ciudad.

En conferencia Molina dijo que la pregunta sería ¿qué pasa en la ciudad de Santa Fe y la capital provincial con las fuerzas federales?. Estamos atravesando un momento de inseguridad que debemos parar y para ello, las fuerzas federales hoy son lo más importante. Además debemos sumar que hasta el fin de semana en la ciudad se cuentan 65 homicidios y una ola creciente de robos, usurpaciones y violencia callejera.

Con respecto a la ocurrido en 2016, cuando durante la gestión de Mauricio Macri, llegaron a la provincia un importante numero de gendarmes, para ayudar a combatir la inseguridad dijo que "a Rosario en ese entonces llegaron 1659 efectivos y a Santa Fe 779"

"La seguridad es la mayor preocupación de los vecinos de Santa Fe y no tener un número, ni saber dónde irán los agentes federales. Además, la gestión municipal debe ponerse al frente de estos reclamos, porque si no hay un mapa del delito o articulación entre las fuerzas o si las herramientas de prevención no funcionan, las noticias que se conocerán sobre la ciudad serán perores a las que se conocen diariamente", comentó Molina

Chuchi Molina subrayó también que la ciudad no puede estar esperando que las soluciones lleguen desde otras esferas: “Mucho se avanzó en las gestiones anteriores, desde que se creó el Centro de Monitoreo, en 2009. Planteamos que la ciudad debe poner en marcha otra vez esa herramienta que es vital para la prevención del delito. Hoy falta liderazgo, decisión y vocación, que es lo que propondremos desde el Concejo con Juntos por el Cambio”.

“Por eso también ya presentamos propuestas, como “Ojos en alerta”, un sistema que se puede integrar al Centro de Monitoreo, con los vecinos dando avisos y alertas desde sus teléfonos con alguna aplicación como WhastApp. Y eso se puede hacer desde la ciudad”, sostuvo.

Vale señalar que también acompañaron en la actividad Carlos Pereira y Luciana Ceresola (actuales concejales, y candidatos a renovar), Sebastián Mastropaolo e Inés Larriera, también actuales ediles; Mauricio Amer; María de los Ángeles Álvarez; Ileana Alvarenga y Nicolás Rabosto.

**Pedido de informes**

Días atrás mediante un pedido de informes, el diputado Juan Martín junto a pares del interbloque -entre ellos Niky Cantard y Ximena García-, le plantearon a Aníbal Fernández que La Capital no puede quedar relegada. Pidieron expresamente “que se contemple la situación crítica que vivimos y lleguen efectivos federales a prestar servicios de manera permanente en Santa Fe y el conglomerado urbano”.

En ese pedido, se recordó que en 2016, durante el gobierno nacional de Cambiemos, la ciudad de Santa Fe “llegó a tener más de la mitad de los efectivos federales que prestaban funciones en el sur provincial. Había 1.357 en Rosario, y 779 en Santa Fe. Y no fue por casualidad: la ciudad de Santa Fe estaba ahí, requiriendo, colaborando, con decisión, mostrando la situación crítica que tenemos”.

Los legisladores también lamentaron “que no se haya seguido con la muy buena experiencia que fue el programa Barrio Seguro, llevado adelante durante las gestiones anteriores en el distrito Alto Verde, en trabajo articulado entre Nación, municipio y provincia”. Por eso también en el pedido de informes los legisladores preguntaron “si se le dará continuidad a ese programa en los hechos, y si lo piensan replicar para traer tranquilidad a otros barrios”.

Por su parte, los concejales aprobaron la semana pasada un proyecto de Carlos Pereira -que acompañaban también sus pares del bloque Juntos por el Cambio- en el que se solicita al Departamento Ejecutivo Municipal “la realización de las gestiones correspondientes a los efectos de que el refuerzo de Fuerzas Federales (Gendarmería, Prefectura y Policía Federal) dispuesto por el Ministerio de Seguridad de la Nación tenga como destino, también, la ciudad de Santa Fe en la misma proporción, conforme a la población, de los que son destinados a la ciudad de Rosario”.