---
category: Agenda Ciudadana
date: 2021-04-10T06:15:11-03:00
thumbnail: https://assets.3dnoticias.com.ar/ARCANDO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Solicitan la declaración de emergencia en materia de violencia de género
  y familiar en Santa Fe
title: Solicitan la declaración de emergencia en materia de violencia de género y
  familiar en Santa Fe
entradilla: Lo pidió de forma "urgente" la diputada provincial Cesira Arcando. Lo
  hizo tras la confirmación del femicidio de Marcela Maydana.

---
La diputada provincial Cesira Arcando, del Partido Fe, solicitó este viernes que de manera "urgente", se declare la emergencia en materia de violencia de género y familiar en la Provincia de Santa Fe, tras la confirmación del femicidio de Marcela Maydana, de 44 años.

"No vamos a permitir que nos sigan matando. Desde hace muchos años la Provincia de Santa Fe se encuentra en una situación alarmante. Hemos presentado proyectos relacionados con la violencia hacia las mujeres, como así también, en la asistencia para que las víctimas puedan salir de ese infierno que viven. Necesitamos urgentemente que se declare la emergencia en materia de violencia de género”, comentó la legisladora.

"Los casos de violencia de género y familiar cada vez son más recurrentes, siendo una de las principales causas de muerte en nuestro país. Debemos comenzar a actuar con mayor celeridad y rigurosidad ya que cada mujer que desaparece acaba sin vida", concluyó Arcando.

Este viernes por la tarde familiares y amigos de Marcela despidieron sus restos en lo que fue su último adiós. Este sábado a las 10.30, en los Tribunales santafesinos se realizará la audiencia imputativa y de prisión preventiva para el único detenido de la causa del femicidio, la ex pareja de la víctima.

_Con información de UNO Santa Fe_