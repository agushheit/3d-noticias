---
category: La Ciudad
date: 2021-07-27T06:00:54-03:00
thumbnail: https://assets.3dnoticias.com.ar/BIDONES.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Aumenta la venta de agua envasada en Santa Fe por el cambio de sabor del
  agua potable
title: Aumenta la venta de agua envasada en Santa Fe por el cambio de sabor del agua
  potable
entradilla: Así lo reflejaron distintos distribuidores de agua envasada en la ciudad.
  Un bidón estándar de 20 litros cuesta alrededor de $300 y es el más demandado ante
  el gusto atípico del agua de red.

---
En los últimos días los santafesinos notaron un cambio notorio en el gusto del agua potable utilizada para consumo propio, cuestión que está relacionada a la bajante histórica del Paraná en la costa de Santa Fe. En este contexto, desde empresas distribuidoras de agua envasada sostuvieron que aumentaron sus ventas ante el sabor atípico del agua de red en la ciudad.

Desde Aguas Santafesinas (Assa) insistieron en que la modificación del sabor percibido en los consumidores no varía la calidad del agua en sí, remarcando que el tratamiento del agua es el mismo, siguiendo las normas y estándares necesarios para hacerlo y que sigue siendo apta para el consumo.

Sin embargo, son cada vez más los santafesinos que optan por dejar de consumir el agua potable de red para comenzar a adquirir agua envasada, priorizando el sabor neutro característico pese al costo económico. Distintos proveedores de agua purificada en Santa Fe dijeron a UNO: "notamos que ha aumentado en la última semana, sobretodo por el cambio del gusto del agua".

Los productos que comercializan estas firmas son variados, aunque distinguieron que creció la demanda notoriamente en los bidones de 20 litros que pueden ser utilizados en modo dispenser. Estos tienen un costo que ronda los $300 y son la opción mas elegida por los santafesinos que no les cayó bien el sabor salado del agua de red como consecuencia de la bajante.

Otra elección disponible que fue nombrada por los proveedores es la de dispenser de agua conectados a la red, funcionando como purificadores del agua potable. Si bien esta es una buena opción para modificar el sabor extraño del agua, recalcaron que no se vieron cambios significativos en la venta por ahora.

El vocero de Assa, Germán Nessier, ya había aclarado a los micrófonos de La Radio de UNO que el sabor salado detectado en el paladar de los santafesinos "puede suceder porque desde alguna toma se puede captar agua con mayor concentración de minerales, algo propio de esta bajante extraordinaria, donde los números promedios del río para esta fecha se encuentran muy por debajo de lo normal".