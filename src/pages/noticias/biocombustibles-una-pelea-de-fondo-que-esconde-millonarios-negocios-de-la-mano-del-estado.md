---
category: El Campo
date: 2021-03-25T08:46:30-03:00
thumbnail: https://assets.3dnoticias.com.ar/soja.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de La Nación
resumen: 'Biocombustibles: una pelea de fondo que esconde millonarios negocios de
  la mano del Estado'
title: 'Biocombustibles: una pelea de fondo que esconde millonarios negocios de la
  mano del Estado'
entradilla: En mayo termina el régimen de promoción y la prórroga de los beneficios
  desnudó una batalla de intereses cruzados entre productores, petroleros y traders,
  verdaderos ganadores del sistema

---
Una tormenta de intereses cruzados, de argumentos encontrados y de millones de dólares se posaron estos días sobre el mundo de los biocombustibles. Allí conviven petroleros, productores de combustible verde y, sobre todo, los traders del mercado, verdaderos ganadores de un sector agazapado que silenciosamente mueve varios miles de millones de dólares por año.

El motivo de la disputa es la prórroga de la ley de promoción, una norma que se sancionó en 2006 y que originalmente se aprobó por 15 años y vence en dos meses. El nuevo proyecto es una iniciativa que impulsa el oficialismo y que no cuenta con el aval de toda la bancada.

Cada vez que alguien abre la tapa de su tanque de nafta entrará, al menos, 10% de combustible a base de soja, maíz y caña de azúcar que anualmente se producen en la Argentina. Esa parte no tributa el impuesto a los combustibles ni al dióxido de carbono que sí pagan los combustibles derivados del petróleo.

Justamente ese es uno de los temas esenciales: el sistema tuvo un costo fiscal de US$6000 millones de impuestos no recaudados, además de alrededor de US$1400 millones de pérdida de ingreso de divisas por las exportaciones de granos que no se realizaron.

La polémica se desata detrás de ese corte que para la nafta es de 12% de bioetanol y 10% para el caso de biodiesel para cada litro de gasoil, allí donde anidan los famosos y atractivos negocios regulados.

Las petroleras apuntan que el precio de ese 10% o 12% está muy por encima del valor del combustible fósil. Es decir, el precio final del litro de nafta en el surtidor se encarece. Solo para poner números: el 90% de combustible fósil representa el 39% del precio final de la nafta súper en la Ciudad de Buenos Aires, mientras que el 12% de bioetanol impacta en el 8% del valor de surtidor. Para el caso del gasoil, ese número llega a 9%. Lo que vence en mayo es el esquema de algunos beneficios fiscales para los biocombustibles, no su utilización ni obligación de corte con la nafta y el gasoil.

Si bien el proyecto que presentará el diputado Máximo Kirchner no está terminado, se especula que no dejará de exigirse la utilización de 10% de biodiesel en cada litro de gasoil y de 12% de bioetanol en el litro de nafta. Tampoco parece estar en discusión el beneficio impositivo de no abonar el impuesto a los combustibles, porque ese punto está protegido por la ley 23.966 de impuestos a los combustibles desde 2018. Lo que se caería es el esquema mediante el que las productoras de biocombustibles tienen una devolución anticipada de IVA o pueden amortizar aceleradamente para el impuesto a las ganancias para las nuevas plantas que se construyan.

Aquí sí se llega al meollo. El régimen actual tiene una trampa millonaria y eso es lo que verdaderamente divide aguas. Detrás del discurso ecologista o de la defensa de las economías regionales subyace un sistema de precios regulados por el Estado nacional y el otorgamiento de cupos discrecionales que cada mes asigna la Secretaría de Energía para cada planta.

Dicho de otra forma: actualmente, el Estado define quién vende, cuánto vende y a qué precio. No son pocos los que creen que esa caja es determinante para que se vean y se escuchen posturas encendidas. Algo así como que el biocombustible no solo se usa para que funcionen los vehículos, sino también para que se mueva la política.

Llegado a este punto es necesario contar, entonces, quiénes son los grandes mayoristas de este mercado en el que la Secretaría de Energía entrega mensualmente la llave del un majestuoso negocio. Allí sí hay nombres propios. El número uno de ese mundo es Juan Carlos Bojanich, CEO del Grupo Bahía Energía, y dueño del casino y varias salas de juego en la ciudad del Sur bonaerense, quien además tiene varias compañías autorizadas a pedir cupo a la Secretaría de Energía. También se destaca Rosario Bioenergy, Bionogoya, Establecimiento El Albardón y Hector Bolzan, todos propiedad del mismo grupo, Essential Energy Holding, de la familia Puccirello. Entre ambos venden a las petroleras, a precio regulado por el Estado, el 40% de todo el agrocombustible que se expide en el país.

Quizá el sistema más polémico es cómo funciona el reparto. Sin que se conozca cómo, mensualmente se publica una nómina de empresas con un determinado cupo. Si no se aparece en esa lista, no hay manera de vender producto a las petroleras. Además, ese listado tiene la cantidad autorizada y el precio. La petrolera no puede hacer mucho más que darse vuelta y comprar a esos intermediarios o productores. Esos dos grupos concentran el 40% del mercado. Obviamente, Bojanich fue uno de los voceros del sector que encabeza el apoyo al proyecto de prórroga del régimen actual.

Hay, además, algunos reparos técnicos. Por caso, hay quienes dicen en las automotrices que el corte de los combustibles genera ciertos problemas en algunos motores modernos. Las petroleras lo tienen medido: un litro de etanol rinde energéticamente el 75% de un litro de nafta; y el biodiesel, un 87% del litro de gasoil.

La biblioteca está dividida en el mundo en cuanto a la conversión de alimento en combustible. No hay consensos allí. Mientras, en la Argentina, todos patalean. Nadie quiere perder un negocio millonario.