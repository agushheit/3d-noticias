---
category: La Ciudad
date: 2021-10-08T06:00:48-03:00
thumbnail: https://assets.3dnoticias.com.ar/EMJADORES.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Con la presencia de Arnaldo Nardone, Santa Fe lanzó el programa "Embajadores
  Turísticos"
title: Con la presencia de Arnaldo Nardone, Santa Fe lanzó el programa "Embajadores
  Turísticos"
entradilla: "Se trata de un proyecto de la Municipalidad y el Bureau de Eventos para
  posicionar a la ciudad como destino del turismo de eventos. \n\n"

---
La Dirección de Turismo de la Municipalidad, junto con el Bureau de Eventos, está lanzando el programa “Embajadores Turísticos”, que quiere volver a posicionar a Santa Fe como una ciudad sede de destino de eventos. Se trata de cuatro talleres asociativos, corporativos, profesionales, educativos y deportivos, destinados a que todo el conjunto de los actores que pueden intervenir en la industria puedan aprender y capacitarse con un número uno del rubro: Arnaldo Nardone.

 Al respecto, Matías Tomati, representante del Bureau de Eventos de Santa Fe, reconoció que el lanzamiento del programa es un logro “muy grande”, que va a permitir que todas las personas que puedan estar desempeñándose en una profesión en otro lugar, digan “quiero que en Santa Fe se realice el próximo evento”.

 Por su parte, Franco Arone -Presidente del Ente Municipal de Turismo (SAFETUR) – manifestó a EL Litoral que hay una decisión estratégica del Intendente Emilio Jatón de trabajar en la reactivación del turismo y, particularmente, en lo que es el segmento de reuniones. “En ese contexto, la indicación permanente de trabajar codo a codo con el Bureau de Eventos de la ciudad de Santa Fe que es el ecosistema natural para hacer profesionales cada uno de los eventos”, aseguró el funcionario.

 Según Arone, “Embajadores” es un programa que venimos pensando desde la Municipalidad hace mucho tiempo, con el objetivo de “generar una red de embajadores que se animen a postular a la ciudad para captar eventos de todo tipo, educativos, asociativos, deportivos, vinculados a los negocios, a los incentivos, porque la ciudad tiene ese perfil”. “Entonces, el gran desafío que asume la municipalidad es el de profesionalizar la actividad, la decisión de incorporar por primera vez al turismo como política de Estado y eso no se hace solo, se hace con el sector privado”, añadió.

**Visita de lujo y fin de semana largo con buen pronóstico turístico**

Arone también consideró que este fin de semana extra largo es una oportunidad “única”: “la estamos aprovechando, estamos muy contentos porque es el referente número uno del mundo en turismo de reuniones y además es una persona que tiene un aprecio personal por la ciudad de Santa Fe y en estos dos días nos dio conceptos clave para mejorar nuestras chances de captar eventos y, fundamentalmente, el asesoramiento para que la ciudad de Santa Fe acceda, de manera profesional a consolidarse como destino de turismo de reuniones”. “Aspiramos a ser uno de los de los destinos turísticos líderes del litoral argentino”, añadió.

 Por otra parte, el titular del Safetur adelantó que para este fin de semana largo hay “muy buenos niveles de reserva”, porque “es el primer fin de semana en el contexto de la pandemia que nos permite mejores expectativa de ocupación y eso nos permite tener actividades con más amplitud de capacidad, mejorar las alternativas de paseos náuticos, actividades culturales, museos, siempre con responsabilidad, aplicando protocolos pero vamos a tener una ocupación excelente porque venimos trabajando hace tiempo codo a codo con el sector privado, con el Bureau”.

 **La palabra del experto**

A su turno, Arnaldo Nardone -el experto invitado para el lanzamiento del programa- destacó el nexo que tiene “con la ciudad y con todo el equipo del Bureau de Convenciones de Santa Fe”: “Esto fue una invitación para el lanzamiento de un programa innovador y muy creativo que ya empezó pero que van a seguir desarrollando, que se dio a llamar Programa Embajadores de la Ciudad de Santa Fe, que consiste en identificar a todos aquellos líderes y personas importantes en el área que desarrollan su actividad principal a diario, que tengan la posibilidad, debido a sus conexiones con las entidades regionales, nacionales e internacionales, de traer los eventos en los cuales ellos participan habitualmente, a la ciudad de Santa Fe; porque hay una regla muy clara que es que el 75% de los eventos en todos los segmentos del mercado necesitan, siempre, que la candidatura sea presentada por una persona que es miembro pleno de la entidad a la cual está postulando el evento y que, además, esté integrado hace unos cuantos años y haya participado en los eventos anteriormente”, detalló. “Es decir -prosiguió-, el Buró no puede presentar una candidatura a un Congreso de Cardiología, lo puede hacer el cardiólogo que participa”.

 **En palabras**

“El turismo es como cuando uno estudia Medicina, primero estudia y después se especializa. En mi caso en particular, empecé en el turismo en 1973 siendo apenas un chico de 18 años y curiosamente empecé en Buenos Aires; a partir de ahí me fui desarrollando y por los años noventa me dediqué de lleno al turismo de eventos y desde ahí se fueron dando situaciones en la política de las entidades y empecé a ocupar cargos importantes de presidencias a nivel latinoamericano y mundial, como la ICA (Asociación Internacional de Congresos y Convenciones), a integrar el Consejo Mundial, y todo eso me llevó a escribir tres libros con universidades y hoy es la pasión de mi vida, además de negocios”. Arnaldo Nardone.

 “Se trató -detalló Nardone- de identificar al primer grupo en cada una de las comunidades que tienen la posibilidad de hacerlo y se hicieron presentaciones específicas de cómo este programa los iba a ayudar en ese proceso de captación de eventos”. “De esa manera, se hizo una presentación al sector del gobierno, para entender que el gobierno también puede captar eventos importantes para del destino, y también la importancia que tiene el impacto económico para la ciudad y la provincia de Santa Fe; ahí, tuvimos a la comunidad científica, la comunidad universitaria, la comunidad corporativa, la comunidad de asociaciones profesionales y a todos les hicimos presentaciones similares”, explicó.

 **El turismo de convenciones en pandemia**

Nardone señaló que el turismo “ya se está reactivando, ya se están haciendo eventos importantes en todas partes del mundo, inclusive en América Latina, y eso ha dependido -en mayor o menor grado- de que el país haya abierto fronteras, aunque sea con condiciones y que no haya una cuarentena al ingresar si sos extranjero, como los casos de Dominicana, Panamá, Colombia, que empezó con los eventos en julio y ya tiene todo confirmado hasta diciembre.” “Eso es una muestra clara de la situación, en Europa, Estados Unidos y Asia hay una reactivación clara y aunque no vamos a volver inmediatamente a la normalidad pero nos vamos a acercar bastante a finales de 2022, principios de 2023”.

 En cuanto a Santa Fe, el especialista la señaló como una ciudad “con potencial. En tal sentido, aseguró que “Santa Fe tiene todo lo necesario y es un destino que tiene todo para poder consolidarse a nivel regional y nacional, por eso la idea de unir lo público y lo privado y que estas comunidades trabajen mancomunando esfuerzos y estrategias en conjunto con el Bureau de Convenciones de Santa Fe”.

 