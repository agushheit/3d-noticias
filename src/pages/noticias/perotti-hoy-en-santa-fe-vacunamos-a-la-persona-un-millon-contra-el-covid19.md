---
category: Estado Real
date: 2021-06-13T06:15:06-03:00
thumbnail: https://assets.3dnoticias.com.ar/1MILLON.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Perotti: “Hoy en Santa Fe vacunamos a la persona un millón contra el covid19”'
title: 'Perotti: “Hoy en Santa Fe vacunamos a la persona un millón contra el covid19”'
entradilla: Lo afirmó el gobernador santafesino durante una recorrida por el centro
  de vacunación La Esquina Encendida, de la ciudad de Santa Fe.

---
El gobernador de la provincia, Omar Perotti, recorrió durante la mañana del sábado, el centro de vacunación La Esquina Encendida, ubicado en la ciudad capital, donde se inoculó a la persona que estableció el registro sanitario del millón de personas vacunadas con la primera dosis contra el Covid19. “Así es, llegamos al millón de santafesinos y santafesinas vacunados con la primera dosis”, expresó Perotti junto a la ministra de Salud, Sonia Martorano.

“Tenemos el ciento por ciento del sistema de salud vacunado, como así también quienes colaboran con el mismo y lo integran en las tareas de hisopado, en los Detectar, en los centros de aislamiento y en las acciones de control”, destacó Perotti y amplió: “Todos los mayores de 60 años que estaban inscriptos, están vacunados; todos los docentes inscriptos, vacunados y se están incorporando a los reemplazantes en la medida que cada uno de ellos va ingresando”.

“Y este fin de semana terminamos con todos los anotados entre 18 y 59 años con comorbilidades; además, estamos llegando al ochenta por ciento del grupo con discapacidades”, sumó el gobernador.

La provincia de Santa Fe tiene 1.200.000 personas consideradas esenciales y de riesgo. “De ese 1.200.000 vacunamos ya un millón: al final del día habremos superado largamente esa cifra, es decir, estamos muy cerca de llegar a cubrir el ciento por ciento de todas las personas consideradas claves o de riesgo y poder tenerlas vacunadas”, manifestó el titular de la Casa Gris.

Para finalizar, Perotti manifestó que “hoy es un muy buen día para el Plan Santa Fe Vacuna, por eso vine hasta aquí personalmente a brindar mi agradecimiento a cada uno de los que integran los equipos de vacunación. En toda la provincia están con el mejor trato, con la mejor disposición, con mucho cariño y con amor, que es lo que se les pidió porque esto es lo que necesita un proceso de vacunación: tratar muy bien a la gente. Y lo están haciendo de la mejor manera”.

**120 MIL NUEVOS TURNOS DE VACUNACIÓN**

Por su parte, la ministra de Salud, Sonia Martorano, informó que “hemos disparado 120 mil turnos más con lo que teníamos de vacunas arribadas en estos días, asique120 mil santafesinos entre ayer y hoy van a estar recibiendo su turno con su fecha y sabiendo que su vacuna ya está asignada”.

“También es importante decir que hoy tenemos la mirada fuertemente puesta en seguir vacunando gente que puede tener alguna comorbilidad y no se anotó. Nosotros los estamos buscando en los centros de salud y en las zonas más vulnerables por si hubo algún problema de accesibilidad y no se anotaron. Los estamos anotando y los estamos vacunando”, sostuvo Martorano.

“Esta campaña continúa, estamos vacunando en este momento a 35 mil santafesinas y santafesinos por día. Y hoy es sábado, mañana es domingo y también se vacuna. Como dice el gobernador: ‘la mejor vacuna es el brazo de nuestros santafesinos´”, finalizó la titular de la cartera sanitaria.

De la recorrida en el centro de vacunación participaron además el secretario de Salud, Jorge Prieto; el secretario de Prácticas Socio Comunitarias, Ignacio Martínez Kerz; la directora provincial de Adultos Mayores, Lucía Billoud; y la concejala de la ciudad de Santa Fe, Jorgelina Mudallel.