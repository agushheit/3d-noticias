---
category: La Ciudad
date: 2022-01-20T06:15:34-03:00
thumbnail: https://assets.3dnoticias.com.ar/AUMENTOS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Los aumentos que se vienen en cinco servicios claves
title: Los aumentos que se vienen en cinco servicios claves
entradilla: 'Algunos se encuentran en pleno debate, otros aguardan por la aprobación
  o ya fueron confirmados. ¿Que pasará con la energía eléctrica, gas, agua y el transporte
  de pasajeros?

  '

---
El 2022 arrancó con debates de aumentos en servicios claves en la vida de los santafesinos. Quien dio el puntapié inicial fue la EPE, empresa que aguarda por una definición del ejecutivo provincial tras el incremento solicitado. Este miércoles se desarrollaron dos audiencias públicas; una a nivel local para discutir la suba elevada por Assa y otra a nivel nacional, vinculada al gas y de la cual participó la empresa Litoral gas. Además, se confirmó el aumento en el boleto urbano de colectivo y se aguarda por definiciones entorno al servicio de media distancia

**Empresa Provincial de la Energía**

El viernes 7 de enero comenzó el debate entorno a los nuevos valores de la tarifa del servicio de energía eléctrica en Santa Fe. La Empresa Provincial de la energía propuso un aumento del 25% en dos tramos y una fórmula de ajuste semestral para enfrentar los costos por fuera de los involucrados en la compra de electricidad en el mercado mayorista. Se aguarda que el ejecutivo provincial defina y autorice las subas para que la empresa comience a implementarlas a partir de febrero.

Cabe aclarar que solo se trata de una suba vinculada a la distribución del servicio, ya que la empresa no produce energía (solo la compra y la distribuye). En ese sentido, aún resta saber las decisiones que tomará el gobierno nacional vinculado a la electricidad, rubro que insume el 70% de los subsidios a la energía.

**Aguas Santafesinas S.A**

Este miércoles se desarrolló la audiencia pública convocada por el Directorio del Ente Regulador de Servicios Sanitarios (Enress) para determinar los próximos aumentos en la tarifa del servicio de agua potable se está desarrollando en forma virtual desde media mañana.

La empresa Aguas Santafesinas solicitó autorización para aplicar un incremento del 60% en dos etapas: 40% primero y 20% después. Esa suba llevaría la factura promedio de los usuarios residenciales a $1.600 por bimestre.

**Transporte urbano y media distancia**

A partir del 1 de febrero, el precio del boleto de colectivos en la ciudad pasará de 42,35 pesos a un valor de 59,35 pesos. Se trata de una suba de más del 40 por ciento en el boleto frecuente. Así lo comunicó este miércoles al mediodía la directora de Movilidad de la Municipalidad de Santa Fe, Andrea Zorzón.

Indicó que "las empresas se han comprometido a mejorar el estado general de las unidades, la limpieza de los colectivos y una mejora de la frecuencia del servicio. Hemos analizado varios componentes de la estructura de costos y hemos decidido en este aumento que está por debajo de la inflación acumulada del año 2021; también es menor al aumento solicitado por las empresas que había sido del 85 por ciento".

Con respecto al servicio de media distancia, la Asociación de Transporte Automotor de Pasajeros aguarda una definición del ejecutivo provincial vinculado a la actualización tarifaria. Se trata de los micros que conectan ciudades dentro del territorio santafesino, un servicio que se encuentra bajo la órbita del gobierno de Santa Fe. Fuentes de Atap señalaron esta semana que la suba solicitada supera del 70 por ciento.

**Litoral Gas**

La factura de gas de un usuario residencial en Santa Fe que pague la tarifa plena tendrá un aumento promedio del 20% a partir de marzo próximo si se acepta el pedido que presentó la distribuidora Litoral Gas en la audiencia pública convocada por el Enargas para analizar los pedidos de aumento en el sector del transporte y distribución de este servicio.

En línea con las solicitudes presentadas por las diferentes distribuidoras del país, la empresa pidió ajustar el margen de distribución un 79% en el marco de la discusión sobre el régimen de actualización de tarifas. Pero como esa prestación representa el 20% del costo final de la factura, el impacto en la boleta mensual de un usuario residencial sería de 20% o $ 293 en promedio mensual. Por otra parte, el 83% de ese universo está alcanzado por el régimen de descuentos de Zona Fría, tendrá descuentos de entre 30% y 50%. De ese modo, pagará $ 243 menos, en promedio, que la tarifa vigente (-16%) en el primer caso, y $ 600 menos promedio (-40%) en el segundo caso.

De aprobarse el nuevo esquema tarifario tal como lo pide la empresa, los usuarios pymes tendrían un aumento de 32% en la factura a partir de marzo, los de GNC 4% y los industriales 4%.

Cabe aclarar que aún resta saber lo que ocurrirá con el precio del suministro (ya que esta actualización corresponde a transporte y distribución). En ese sentido, el próximo lunes 31 de enero se prevé debatir los precios mayoristas del gas en el Punto de Ingreso al Sistema de Transporte.