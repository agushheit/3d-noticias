---
category: Agenda Ciudadana
date: 2021-01-23T10:58:51Z
thumbnail: https://assets.3dnoticias.com.ar/aulas.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Diario Uno
resumen: Gremios docentes rechazan la vuelta a clases y dicen que las aulas serán
  "burbujas de hacinamiento"
title: Gremios docentes rechazan la vuelta a clases y dicen que las aulas serán "burbujas
  de hacinamiento"
entradilla: El ministro de Educación Nicolás Trotta dijo que las condiciones están
  dadas para la presencialidad, los sindicatos se quejan de la falta de protocolos
  sanitarios.

---
Los gremios docentes expresaron hoy interrogantes respecto al regreso a la presencialidad en las escuelas que propone el Gobierno porteño a partir del 17 de febrero, al señalar que las aulas funcionarán como "burbujas de hacinamiento" y pidieron mayores certezas respecto de los protocolos que deberán cumplirse.

El secretario adjunto de Ademys, Jorge Adaro, lamentó que las autoridades porteñas estén planteando el regreso a clases a través de "burbujas de hacinamiento" en las aulas, y se quejó de que la posición de los docentes pidiendo certezas sobre las condiciones sanitarias se traduzca en "una gran campaña en su contra", instalando que no quieren el "regreso a clases".

"¿Cuál es el motivo para que las autoridades insistan en esto cuando hay un rebrote y no han puesto un solo peso para reacondicionar las aulas?", se preguntó Adaro en declaraciones a radio Continental.

En ese sentido, Adaro apuntó a la ministra de Educación porteña, Soledad Acuña, por proponer lo que consideró "burbujas de hacinamiento" en las aulas.

"Yo las llamo así porque en un aula con 30 chicos, ¿Cuál es la posibilidad de mantener el distanciamiento?. Ninguna. Y cuando uno se los plantea, ellos dicen: que cada escuela lo resuelva", planteó el gremialista.

Adaro se preguntó "cuál es el motivo para que los gobiernos insistan en la presencialidad, sin haber destinado un solo peso al acondicionamiento de las aulas".

Incluso planteó que "desde el año pasado" discuten con las autoridades "cuáles con las condiciones imprescindibles para retomar la presencialidad en las aulas y no han hecho nada", solo llevar adelante, según dijo, "una campaña terrible" que los muestra como "no queriendo volver a clases".

**Presencialidad**

Ayer el ministro de Educación de la Nación, Nicolás Trotta, afirmó que "están dadas las condiciones para volver a la presencialidad" en las aulas, con el cumplimiento estricto de todos los protocolos" elaborados por las autoridades sanitarias.

El Gobierno de la Ciudad de Buenos Aires anunció el retorno a la presencialidad a partir del 17 de febrero. Al respecto, Adaro dijo que "el anuncio del 17 habla de generalidades, porque no da protocolo concreto sobre el inicio de clases".

"Venimos planteando la falta de infraestructura, de docentes y auxiliares para sostener la higiene, también es un problema el traslado de alumnos a las escuelas, y no hemos tenido respuesta positiva o concreta", sostuvo.

Incluso, Adaro propuso en el caso de la Ciudad de Buenos Aires, "poner a disposición toda la flota de micros escolares para evitar aumento de pasajeros en transporte público", pero lamentó que "tampoco" hubo una gestión al respecto.

"Así que de ninguna manera podemos suponer que si no hay una política en este sentido no habrá una profundización de la pandemia" sintetizó. Consultado sobre si habrá medidas de fuerza respondió que "será una decisión que será debatida en asamblea por los docentes".

**Año electoral**

En tanto, Hugo Yasky, diputado nacional y secretario general de la Central de Trabajadores de la Argentina (CTA), lamentó en declaraciones a AM 750 que “han decidido tomar el tema como el eje central de una disputa en un año electoral" en referencia a Juntos por el Cambio y el tema de la presencialidad. Y agregó que "quieren trazar una línea entre civilización y barbarie donde la civilización son los que quieren que la escuela vuelva y los otros son la barbarie".

"Es fácil, le pusieron una fecha al inicio de clases y cómo se resuelve la pandemia cada director (de escuela) verá”, deslizó.

“Es una absoluta irresponsabilidad”, calificó luego y remató con que a los que preguntan "qué pasa con las condiciones sanitarias lo tratan como un saboteador y un enemigo público”.

Yasky también diferenció el abordaje del tema en la Ciudad de Buenos Aires respecto de las provincias. "En algunas se trabaja sobre la base de garantizar condiciones que hagan posible la presencialidad, sin negar la realidad y mirando día a día qué pasa con las cifras de contagios, viendo si logramos que la curva baje o la vacuna se aplique de manera masiva”, manifestó.

María José Gutiérrez, secretaria de Nivel Inicial UTE-Ctera, indicó que hay "muchísimas preguntas sobre el tema de los traslados, las vacunas, los testeos sobre los anuncios que hicieron ayer desde el Gobierno de la Ciudad".

En declaraciones a radio 990 ejemplificó con que "en nuestro nivel estamos hablando de chicos de 45 días hasta 4 años". "En ese primer segmento, de 45 días, ¿Cómo se los va a recibir si no es abrazándolos? ¿Cómo le vas a dar la mamadera?, y a los niños de 3 o 4 años, ¿Cómo vas a hacer para contenerlos si no los podes tocar?", preguntó.

"En nuestro nivel también tenemos un período de adaptación en el cual se indica a la familia entrar a la sala para que el niño no sienta el desapego. En ese caso, estamos duplicando la cantidad de gente en la sala, son cuestiones básicas de la actividad que no fueron tomadas en cuenta", ejemplificó Gutiérrez.

Consultada sobre cómo imagina el 17 de febrero, respondió: "Esperamos que todo se solucione, los maestros quieren empezar, sabemos la importancia de la presencialidad pero con las condiciones necesarias", dijo y citó la importancia de "vacunar a los docentes y tener las condiciones de salubridad necesarias".