---
category: Agenda Ciudadana
date: 2022-12-16T10:13:16-03:00
thumbnail: https://assets.3dnoticias.com.ar/Presentacion-Movilidad2-1200x1200.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: Transporte público  o cuando la política se regodea de sí misma
title: Transporte público  o cuando la política se regodea de sí misma
entradilla: Las necesidades políticas del gobierno municipal, quedaron alejadas de
  una realidad que reclama acciones ahora.

---
Colectivos rotos por las calles, perdiendo aceite y generando accidentes, frecuencias superiores a las establecidas por ordenanzas, coches sucios, es la cotidianeidad del sistema de colectivos en Santa Fe. **_Nada de eso se habló en la presentación del Mensaje que el Ejecutivo municipal enviará al Concejo para volver a licitar el transporte público de pasajeros por colectivo_**.

Las autoridades decidieron presentar un “nuevo sistema de colectivos” porque la ciudadanía “no confía en este sistema”. A tres años de gobierno, debería haberse practicado algún tipo de autocrítica, ante la falta de políticas activas para sostenerlo. Sobre todo, en épocas de aislamiento (ASPO), cuando desde el mismo Estado se prohibió el transporte colectivo, autorizando solo a personas con trabajos esenciales.

Durante mas de media hora, se presentaron datos de una encuesta de movilidad, mostrándole a los vecinos como se mueven, que medios de transporte utilizan, los motivos de los desplazamientos, matrices de origen y destino. También, se pusieron a disposición cuales será los parámetros de un nuevo sistema: que antigüedad de coches, que se valorará la incorporación de energías limpias en las unidades, grandes “novedades” en el control, etc. etc. **_Pero la razón de ser de cualquier pliego, está en los “recorridos”, ya que la licitación se hace para asignar empresas que exploten esos recorridos, con las condiciones que determina la gestión._**

\**“**La directora municipal de Movilidad insistió en que ___‘se planifica una red de norte a sur y de este a oeste, ampliando la actual cobertura’. **En cuanto a esos trayectos, estableció que **_‘los vamos a trabajar próximamente con las y los usuarios, a través de instancias de participación ciudadana, con las universidades y con base en los datos que ya obtuvimos´”.__

Las necesidades políticas de un gobierno municipal, ya que la licitación era una **_“promesa de campaña”_,** quedaron alejadas de una realidad que reclama acciones ahora: los colectivos funcionan las 24 horas del día, los 7 días de la semana, y como lo dijo el mismísimo Intendente en la presentación, en noviembre se realizaron 2.500.000 viajes, en las condiciones que ya nombramos: coches rotos, sucios, con frecuencias superiores a los 20 minutos.. Nada se dijo, ni se publicó, sobre cómo será el sistema de colectivos ahora, mañana, el año que viene, puesto que, hasta el momento en que se determinen los recorridos, se revisen los pliegos, se publiquen, se presenten los interesados, y se cumplan todos los pasos legales, puede pasar mas de un año.