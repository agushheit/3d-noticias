---
layout: Noticia con imagen
author: .
resumen: Llamado a concurso
category: Estado Real
title: El Ministerio de Educación revocó titularizaciones y llama a nuevo
  concurso para transparentar el proceso
entradilla: “Vamos a trabajar para que los agentes que pierden la titularidad
  puedan acceder a un proceso lógico, y conforme a las reglamentaciones que hay
  que cumplir”, aseguró la ministra Adriana Cantero.
date: 2020-11-17T14:31:06.806Z
thumbnail: https://assets.3dnoticias.com.ar/adriana-canterojpg.jpg
---
El Ministerio de Educación de Santa Fe estableció revocar tres expedientes de titularización docente y llama a nuevo concurso “conforme a la normativa vigente”, ante una intimación del Tribunal de Cuentas Provincial, luego de que dicho organismo detectara irregularidades en el nombramiento de unas 500 personas sin antecedentes ni títulos requeridos para el cargo.

“Vamos a trabajar para que esos agentes que pierden la titularidad puedan acceder a un proceso lógico, conforme a las reglamentaciones”, aseguró la ministra de Educación de la provincia, Adriana Cantero.

Y explicó: “Son concursos que, si bien estuvieron acordados en paritarias, se han omitido escalafones, juntas de calificaciones, y la posibilidad de comprobar los legajos, un montón de observaciones legales que hacen que se tenga que determinar la nulidad de esos procedimientos”.

En esa línea, la funcionaria detalló que “las resoluciones de los últimos 6 meses de la gestión anterior, que comprometían el gasto fijo de la provincia, fueron analizadas por la Comisión creada por el decreto 89, en razón de la Ley de Responsabilidad Fiscal”.

“Esa Comisión -prosiguió Cantero-, y en función de los informes obrantes del Ministerio de Educación, ha determinado por la cantidad de irregularidades detectadas la nulidad de esas resoluciones, algunas tomadas el 5 de diciembre de 2019, a cuatro días de terminar la gestión pasada”.

Además de las mencionadas observaciones, y fundamentalmente en tres de esos expedientes, se expidió el Tribunal de Cuentas en el mismo sentido, que realizó una observación legal a esos procedimientos, intimando al Ministerio de Educación de la provincia para que lo resuelva.

Ante este escenario, la ministra indicó que se resolvió “convocar a nuevos concursos y a los gremios docentes para conversar sobre la situación”.

Finalmente, desde la cartera educativa se aclaró que los docentes que fueron titularizados y se encuentran involucrados por esta situación, no dejarán de cobrar, aunque si verán reflejado un cambio de situación de revista y pasen a la condición de interinos.