---
category: Deportes
date: 2021-05-17T06:00:11-03:00
thumbnail: https://assets.3dnoticias.com.ar/BOCA.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Boca apeló a los penales para eliminar a un River diezmado y digno
title: Boca apeló a los penales para eliminar a un River diezmado y digno
entradilla: 'El Xenieze venció en los penales 4 a 2 ante el Millonario, disminuido
  por 15 casos de coronavirus, luego de igualar 1 a 1 con goles de Carlos Tevez (10m
  PT) y de Julián Álvarez (23m ST). '

---
Boca Juniors derrotó este domingo por penales (4-2) a un River Plate diezmado por los 15 casos positivos de coronavirus, luego del empate 1-1 en La Bombonera, y se clasificó a las semifinales de la Copa de la Liga Profesional de Fútbol.

Carlos Tevez marcó para Boca y Julián Álvarez lo hizo para River durante los 90 minutos. El defensor Julio Buffarini marcó el cuarto penal que definió el duelo a favor del "Xeneize" que cortó la racha de cinco eliminaciones seguidas ante River, incluida la final de la Copa Libertadores 2018 en Madrid. Su rival en semifinales será Racing Club.

Boca estuvo cerca de la reprobación. El contexto lo invitaba a tomar la iniciativa en el juego, pero no lo hizo. Se mantuvo fiel a la idea de Miguel Russo.

River, aún con limitaciones, no renunció a su intención de presionar y por momentos se hizo cargo de la pelota.

El debut del juvenil Alan Leonardo Díaz en el arco de River es para destacar. Los primeros minutos fueron de nervios comprensibles y no tuvo incidencia en el gol de Tevez.

Boca se puso en ventaja con un error en la dupla Angileri-Casco que propició el centro de Medina para Tevez quien cometió infracción a Maidana y con el hombro (su intención era pegarle de cabeza) abrió el marcador.

El árbitro Facundo Tello no observó falta del "Apache" y luego tuvo más errores. Pudo haber exhibido la tarjeta roja a Maidana y Villa.

River se asentó en los minutos finales del primer tiempo y llegó con peligro al área. Agustín Rossi estuvo impreciso en una salida, pero no lo aprovechó Montiel. En la siguiente jugada, Maidana cabeceó en soledad y la pelota se fue por encima del travesaño. Poco después, un tiro libre de Angileri pasó cerca del palo.

Boca tomaba malas decisiones con Villa y Pavón, se contenía con Medina; y Tevez se mostró siempre peligroso.

Carlitos exigió al juvenil Díaz en dos ocasiones durante la segunda etapa y esa falta de definición Boca la pagó cara con el empate de River.

Los ajustes que Gallardo realizó hicieron su efecto y Álvarez, de cabeza, estableció el 1-1 en el mejor momento de un River sin cinco de sus habituales titulares y poco recambio en el banco.

Lucas Beltrán, José Paradela y Tomás Galván le dieron juego a un River que se plantó mejor, con Ponzio de volante central desde la salida de Enzo Pérez por lesión, ya sin la discreta producción de Jorge Carrascal y Milton Casco.

La igualdad de River llenó de preguntas a un Boca más confundido que de costumbre. El resultado quedó abierto hasta el final.

River arriesgó en sus salidas con el buen pase de David Martínez desde el fondo y Boca nunca aprovechó las contras que se le generaron.

Los penales, una vía predilecta de Boca para acceder a instancias finales, le dieron al equipo de Russo el alivio ante un River que se retiró con la cabeza en alto de La Bombonera y con un futuro complicado a la espera de no sufrir más casos de Covid-19 con vistas a la Copa Libertadores.

Boca enfrentará a Racing en semifinales a sabiendas de que la imagen que dejó ante River no la puede repetir.

**Síntesis**

Boca: Agustín Rossi; Julio Buffarini, Carlos Izquierdoz, Marcos Rojo y Frank Fabra; Cristian Medina, Alan Varela y Agustín Almendra; Cristian Pavón, Carlos Tevez y Sebastián Villa. DT: Miguel Ángel Russo.

River: Leonardo Díaz; Gonzalo Montiel, Jonatan Maidana, David Martínez y Fabrizio Angileri; Milton Casco, Leonardo Ponzio, Enzo Pérez y Jorge Carrascal; Julián Álvarez y Agustín Fontana. DT: Marcelo Gallardo.

Gol en el primer tiempo: 11m. Tevez (B).

Gol en el segundo tiempo: 23m. Álvarez (R).

Cambios en el segundo tiempo: Al comenzar José Paradela por Enzo Pérez; 19m. Lucas Beltrán por Carrascal

; 20m. Tomás Galván por Casco (R); 28m. Jorman Campuzano por Varela (B) y Edwin Cardona por Almendra (B) y 35m. Daniel Lucero por Fontana

Amonestados: Fabra, Varela, Cardona y Villa (B). Enzo Pérez, Maidana y David Martínez (R).

Definición por tiros penales: Convirtieron para Boca: Tevez, Villa, Izquierdoz y Buffarini, y a Cardona se lo atajó Díaz. Para River anotaron Montiel y Álvarez, mientras que Agustín Rossi contuvo los remates de Angileri y Ponzio.

Árbitro: Facundo Tello.

Estadio: La Bombonera.