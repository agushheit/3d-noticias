---
layout: Noticia con imagen
author: Por Clara Blanco
resumen: Voley Femenino
category: Deportes
title: "Voley: Las Panteras y el seleccionado U18 ya entrenan en Las Parejas"
entradilla: Las jugadoras del equipo mayor y las juveniles se pondrán a punto en
  las instalaciones de Sportivo Las Parejas hasta el 29 de noviembre.
date: 2020-11-17T17:19:40.803Z
thumbnail: https://assets.3dnoticias.com.ar/voley-femenino.jpg
---
[](<>)Las Panteras, selección de voley femenino, ya se encuentra en la ciudad de Las Parejas entrenando. Junto con este combinado también arribaron las jugadoras U18. Ambos seleccionados practicarán en las instalaciones del club Sportivo Las Parejas hasta el 29 de noviembre.

Las deportistas junto con sus equipos técnicos fueron recibidas por el ministro de Desarrollo Social de la provincia, Danilo Capitani, junto con la secretaria de Deportes, Claudia Giaccone. “El deporte es un elemento esencial en la vida de los chicos y de los jóvenes, por eso todas las actividades vinculadas a la práctica deportiva deben ser respaldadas y protegidas, por eso estamos aquí”, aseguró Capitani. Por otro lado, Giaccone expresó que “en la provincia de Santa Fe trabajamos en una política social de alto impacto como es el deporte. Este tipo de eventos nos alienta a seguir adelante en nuestro objetivo que es masificar el ingreso de todos y todas a la práctica deportiva”.

Hernán Ferraro, técnico de la selección mayor, convocó a 19 jugadoras (que incluye a chicas de la Selección Sub-20) en el marco de la tercera tanda de entrenamientos que tiene como objetivo mejorar física, técnica y tácticamente. Mientras que Roberto Woelflin, quien está a cargo del U18, trabaja con un grupo de 16 chicas que se preparan para el Campeonato Sudamericano Femenino U18 que se realizará en Chile y será clasificatorio para el Campeonato Mundial de la categoría.