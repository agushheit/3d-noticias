---
category: Agenda Ciudadana
date: 2021-01-07T10:22:32Z
thumbnail: https://assets.3dnoticias.com.ar/070121-pedidos-ya.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Télam'
resumen: Santa Fe mantiene hasta junio el tope a la comisión de PedidosYa
title: Santa Fe mantiene hasta junio el tope a la comisión de PedidosYa
entradilla: Desde la Secretaría de Comercio Interior de la provincia extendieron hasta
  el 30 de junio próximo la vigencia del cobro de una comisión máxima de hasta el
  25% -más IVA- en los servicios de delivery de la firma.

---
La Secretaría de Comercio Interior de Santa Fe extendió hasta el 30 de junio próximo la vigencia del cobro de una comisión máxima de hasta el 25% -más IVA- en los servicios de _delivery_ de la firma PedidosYa, que se prestan en el sector gastronómico y comercial en general.

De esa manera, **la firma PedidosYa S.A. mantendrá, en territorio santafesino, el porcentaje de comisión acordado para todos aquellos comercios de alimentos y bebidas que no hubieran quedado incluidos en el acuerdo** celebrado entre la Federación Empresarial Hotelera Gastronómica de la República Argentina (Fehgra) y PedidosYa S.A.

«Esta medida significa continuar con alícuotas especiales y mantener la aplicación de la ley de abastecimiento en un momento de complejidades y donde la emergencia sanitaria permanece vigente», dijeron fuentes de la Secretaría de Comercio santafesina.

En esa línea, el secretario de Comercio Interior y Servicios, Juan Marcos Aviano, recordó que «apenas iniciado el aislamiento social preventivo y obligatorio nuestra gestión de gobierno se puso a trabajar junto al sector gastronómico a fin de lograr una mejora en las comisiones que cobraba la firma PedidosYa, logrando una sinergia entre el sector público y privado que permitió sentar las bases del **primer acuerdo a nivel país que beneficiará al sector de alimentos y bebidas**».

«Nuestra Secretaría emitió una Disposición fijando un tope de comisiones para todos aquellos comercios que no integraran alguna de estas tres cámaras, por lo cual la estimación es de alrededor de 3.000 comercios que también fueron beneficiados con un tope de comisión», agregó Aviano.

Por su parte, la directora de Promoción de la Competencia y Defensa del Consumidor, María Betania Albrecht, recordó que «antes de que venciera el acuerdo logrado y la disposición emitida, en diciembre de 2020, retomamos conversaciones con la firma y pudimos acordar un nuevo tope de comisiones, esta vez por un plazo mayor, de 6 meses, hasta el 30 de junio de 2021, más allá del acuerdo logrado a nivel nacional por todos aquellos comercios adheridos a Fehgra».

«Estamos muy conformes no solo de lograr acuerdos y beneficios para el comercio y el consumidor santafesino, sino también de **generar el antecedente nacional** con el cual Secretaría de Comercio Interior de la Nación convocó a las partes a nivel país y logró **un acuerdo que hoy está vigente hasta el mismo 30 de junio de 2021 para el sector gastronómico agremiado**», finalizó Albrecht.