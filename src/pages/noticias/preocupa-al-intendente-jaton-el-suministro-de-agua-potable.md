---
category: La Ciudad
date: 2021-07-31T06:00:22-03:00
thumbnail: https://assets.3dnoticias.com.ar/JATON.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Preocupa al intendente Jatón el suministro de agua potable
title: Preocupa al intendente Jatón el suministro de agua potable
entradilla: "“A fin de año el nivel podría descender un metro por debajo del cero”,
  dijo el mandatario tras reunirse con especialistas del INA y de Aguas Santafesinas.
  En ese contexto, buscan garantizar el suministro en la ciudad."

---
El escaso nivel de altura que tiene hoy el río Paraná es motivo de preocupación para quienes deben garantizar el suministro de agua potable a la ciudadanía. En ese sentido, el intendente Emilio Jatón mantuvo sendas reuniones este jueves y viernes con representantes del Instituto Nacional del Agua (INA) y de la empresa Aguas Santafesinas (ASSA), para evaluar la situación actual y proyectar el escenario a futuro.

La de este viernes al mediodía con el presidente de ASSA, Hugo Morzán, “fue una reunión operativa y de coordinación”, comenzó diciendo Jatón a la prensa, luego de la misma. “Cuando los momentos son críticos hay que coordinar y trabajar en conjunto, y eso es lo que hicimos con el director de ASSA, teniendo en cuenta lo que va pasando”, señaló el mandatario.

“Ayer me reuní con la gente del INA, mirando los pronósticos hacia fin de año, y hoy tratamos los temas operativos acerca de lo que es el consumo, la captación y el suministro de agua potable para la ciudad de Santa Fe, con pronósticos que se van a extender hasta fin de año, y con coordinaciones por parte de ASSA”, dijo el intendente.

“Ya previendo ello, hoy estábamos 4 cm. por debajo de la toma y a fin de año estaríamos un metro por debajo”, advirtió el intendente. “Así que imagínense el significado que tiene ello y lo que hay que trabajar para que en la Santa Fe sigamos teniendo suficiente agua”.

\-¿Es necesario trabajar sobre las tomas de agua para garantizar el servicio?

\-ASSA ya está trabajando en ello, porque (a este escenario) ya lo venían previendo. Por ello están trabajando con nuevas captaciones y nuevas bombas. Pero no solamente hablamos de eso, sino también hablamos sobre la ampliación de la planta potabilizadora, de los trabajos de cloacas que se están haciendo en barrios de la ciudad, y sobre los planes a futuro que tenemos juntos -enumeró el intendente.

Por su parte, desde Aguas Santafesinas, Morzán agregó: “El agua que se lleva a cada uno de los hogares es absolutamente apta para el consumo humano”. Y admitió “que hay percepción de mayor salinidad del agua”. Luego mencionó que “es algo que venimos midiendo en nuestro laboratorio”. Y destacó que “el agua está perfectamente controlada no sólo por Aguas Santafesinas, sino también por el ente regulador (Enress), por lo que le decimos a la gente que el agua que sale de la canilla en su domicilio puede ser consumible sin ningún tipo de inconvenientes”.

“Estamos trabajando para mejorar la calidad el agua. Esto se debe a la menor presencia del río Paraná que ingresa a través del Leyes y mayor presencia del agua de Los Saladillos. Esto nos afecta en la Toma Hernández. Pero estamos trabajando sobre la otra toma del Colastiné para ir mejorando la calidad”, explicó Morzán, “para tratar de volver al sabor que teníamos en la ciudad antes de la bajante”.

**Cuidar el agua**

“Usemos el agua de manera responsable y solidaria. Esto significa usarla para todo lo que la necesitamos, sin derrocharla. Podemos limpiar una vereda con un balde en vez de usar una manguera. La diferencia son 500 litros de agua”, graficó Morzán, al hacer el llamado a la población.

“Estas conductas que parecen insignificantes ante la magnitud del fenómeno que estamos viviendo, si las multiplicamos por miles, son sumamente importantes”, finalizó Morzán.

¿Qué pasará en el verano?

Cuando lleguen las temperaturas elevadas y crezca la demanda de uso de agua potable en la ciudad, de acuerdo al pronóstico del INA, coincidirá con el nivel más bajo del río. “Esto traerá nuevas complicaciones”, advirtió Morzán. “Estamos haciendo todos los esfuerzos e inversiones para no tener problemas”, dijo. “Así como la pandemia nos obligó a cuidarnos y cuidar al otro, ojalá esta situación crítica sirva para aprender el consumo responsable y solidario del agua”, finalizó.