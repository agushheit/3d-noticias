---
category: Agenda Ciudadana
date: 2021-02-28T06:00:28-03:00
thumbnail: https://assets.3dnoticias.com.ar/27F.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Protesta opositora en Plaza de Mayo y otros puntos del país
title: Protesta opositora en Plaza de Mayo y otros puntos del país
entradilla: La convocatoria fue realizada por dirigentes de Juntos por el Cambio a
  través de redes y medios de comunicación, como Patricia Bullrich y Elisa Carrió.
  Alfredo Cornejo no convocó, pero avaló la manifestación.

---
Manifestantes convocados por dirigentes opositores marchaban a la Plaza de Mayo, la residencia presidencial de Olivos y en plazas de otras ciudades del país en rechazo a las vacunaciones contra el coronavirus fuera de protocolo, que derivaron en la renuncia de Ginés González García al frente del Ministerio de Salud.

La convocatoria fue realizada por dirigentes de Juntos por el Cambio, que en declaraciones a medios de comunicación y redes sociales respaldaron la protesta como la presidenta del PRO, Patricia Bullrich, y la líder de la Coalición Cívica, Elisa Carrió, mientras el presidente de la UCR, Alfredo Cornejo, si bien no se sumó al llamado a la marcha, si avaló la manifestación.

La marcha también fue difundida en redes sociales con el hashtag [#27F]() y [#27FYoVoy](), mientras que otros sectores, como Republicanos Unidos, integrado por Ricardo López Murphy, Yamil Santoro y Darío Lopérfido, se sumaron a la convocatoria.

Portando banderas argentinas y carteles con consignas como "no al comunismo" y "basta de mantener vagos", los manifestantes se concentraron a las 17 en la plaza de Mayo.

Desde allí, un hombre expresó en declaraciones a la prensa que "el que haya cometido alguna avivada con la vacuna, tiene que irse" y afirmó que "lo que han hecho con la vacuna muestra que ellos son así, es la esencia de ellos".

El presidente de la Coalición Cívica, Maximiliano Ferraro, dijo en declaraciones realizadas en Plaza de Mayo que concurrió "para acompañar a la ciudadanía que quiere expresarse por la paz y está cansada por lo que vienen sucediendo" y afirmó que "tiene que ver con los valores de una Argentina grande que supimos conseguir".

Entre los dirigentes que participan de la protesta en Plaza de Mayo, que se replicó frente a la residencia presidencial de Olivos y ciudades del interior del país, como Rosario, Mendoza y Córdoba, estaban la exministra de Seguridad y presidenta del PRO, Patricia Bullrich; el senador radical, Martín Lousteau y los diputados Fernando Iglesias y Waldo Wolff.

También lo hicieron el jefe del bloque PRO en la Cámara de Diputados, Cristian Ritondo; el exfuncionario Hernán Lombardi, el actor Luis Brandoni y la ex diputada Cynthia Hotton, mientras que en Mar del Plata estuvo el diputado radical Mario Negri.

Carrió, quien había presentado una denuncia penal contra la vacuna por posible "envenenamiento" y que fue desestimada por la Justicia, había adelantado que no asistiría a la marcha por su salud.

La manifestación se concretaba tras la implementación de un sistema para el monitoreo y trazabilidad del proceso de vacunación contra el coronavirus al personal estratégico de los tres ámbitos del Estado, dispuesto esta semana por la ministra de Salud, Carla Vizzotti, quien permanece aislada desde ayer afectada de coronavirus.

El presidente Alberto Fernández, por su parte, pidió que el lunes su mensaje ante la Asamblea Legislativa sea seguido "en forma remota" ya que la pandemia de coronavirus "aún nos ataca".