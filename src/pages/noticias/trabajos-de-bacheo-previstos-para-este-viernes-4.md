---
category: La Ciudad
date: 2021-05-28T08:31:23-03:00
thumbnail: https://assets.3dnoticias.com.ar/bacheo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad de Santa Fe
resumen: Trabajos de bacheo previstos para este viernes
title: Trabajos de bacheo previstos para este viernes
entradilla: La Municipalidad informa las calles por las que avanza el cronograma de
  tareas previsto en el Plan de Bacheo 2021. Puede haber cortes de circulación y desvíos
  de colectivos a medida que se desarrolla la obra.

---
En enero pasado, la Municipalidad puso en marcha un plan de bacheo para reparar más de 12.000 metros cuadrados de calles de la capital provincial. El programa se desarrolla por fases, priorizando las zonas de la ciudad que evidencian mayor circulación.

En avenidas troncales. está previsto que los trabajos se concreten en:

* Aristóbulo del Valle y French, mano norte-sur
* Aristóbulo del Valle y Neuquén, mano norte-sur
* Aristóbulo del Valle y Callejón Funes, mano norte-sur
* Peñaloza, entre Milenio de Polonia y Estanislao Zeballos, mano sur-norte

El municipio informa que todas las tareas se realizan siempre en sentido del tránsito. Del mismo modo, indica que puede haber cortes de circulación y desvíos de colectivos, a medida que avanza la obra y se realizan intervenciones mayores.

Además, se detalla que la realización de los trabajos está sujeta a las condiciones climáticas.

**Avenida Freyre**

Por tareas relacionadas con el funcionamiento del hospital reubicable, permanece interrumpida la circulación vehicular en ambas manos de avenida Freyre al 2100, tanto para peatones como para el tránsito vehicular.

Esto implica el desvío de las siguientes líneas de colectivos:

* Línea 9: de recorrido habitual por Mendoza a Saavedra, Juan de Garay, 4 de Enero y Mendoza, a recorrido habitual
* Línea 14: de recorrido habitual por avenida Freyre a Primera Junta, Zavalla, Mendoza, Saavedra y Monseñor Zaspe, a recorrido habitual
* Línea 16: de recorrido habitual por avenida Freyre a Moreno, Francia, Primera Junta y San Lorenzo, a recorrido habitual

Por otro lado, las paradas serán:

* Línea 9: avenida Freyre y Primera Junta, avenida Freyre y Mendoza, Mendoza y Saavedra, Saavedra y Lisandro de la Torre, y Juan de Garay y Francia
* Línea 14: Mendoza y avenida Freyre, Mendoza y Saavedra, Saavedra y Lisandro de la Torre , Saavedra y Juan de Garay, Saavedra y Moreno, y Monseñor Zaspe y San Lorenzo
* Línea 16: avenida Freyre y Moreno, Saavedra y Moreno, Francia y Corrientes, Francia y Lisandro de la Torre, Francia y Mendoza, y Primera Junta y San Lorenzo

**Por el operativo de vacunación**

La Municipalidad comunica que, en el marco del operativo de vacunación que se realiza en la ciudad, habrá controles en el tránsito vehicular, de 8 a 18 horas en las inmediaciones de:

* La Esquina Encendida, en la intersección de Facundo Zuviría y Estanislao Zeballos