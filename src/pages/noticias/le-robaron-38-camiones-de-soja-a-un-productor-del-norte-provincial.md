---
category: Agenda Ciudadana
date: 2020-11-30T15:53:01Z
thumbnail: https://assets.3dnoticias.com.ar/camiones.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: LT10'
resumen: Le robaron 38 camiones de soja a un productor del norte provincial
title: Le robaron 38 camiones de soja a un productor del norte provincial
entradilla: Ocurrió el jueves pasado en un campo alquilado por una familia de productores
  agropecuarios santafesinos, en Quimili (Santiago del Estero). La policía recuperó
  gran parte de la carga sustraída.

---
Una familia de productores santafesinos del norte sufrió un robo impresionante de cereales en un campo que alquilan en la localidad santiagueña de Quimili. Los delincuentes se llevaron del campo más de 35 camiones de soja valuados en más de 22 millones de pesos.

Héctor Faint, uno de los integrantes de esta empresa agropecuaria familiar, contó que “me robaron más de 35 camiones, en un campo de 5000 hectáreas que alquilamos en Quimili. Fue el golpe más grande de la historia. Estas situaciones te dan ganas de tirar la toalla y dejar de producir”.

Además, Héctor Faint agregó que “entraron con un tractor y una cargadora de cereales a las 6 de la tarde y cargaron hasta las 5 de la madrugada. Lindero al campo que alquilamos, hay un ingeniero que tiene estaciones de servicio. Evidentemente, venían desde hace tiempo armando la logística para dar este tremendo golpe”.

La policía de Santiago del Estero se movió con velocidad y pudo recuperar gran parte de la cara robada. Algunos camiones estaban en las estaciones de servicio del principal sospechoso.

Finalmente, el productor santafesino remarcó que “fue impresionante el operativo de la policía. Por suerte, estos delincuentes no pudieron sacar las cartas de porte necesaria para trasladar el cereal. Lo que no sabemos aún es a quién se lo querían vender”.