---
category: Agenda Ciudadana
date: 2021-03-17T06:17:28-03:00
thumbnail: https://assets.3dnoticias.com.ar/elecciones.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: Fijaron las fechas de las PASO y las generales
title: Fijaron las fechas de las PASO y las generales
entradilla: La Cámara Nacional Electoral estableció que las elecciones primarias serán
  el 8 de agosto y las generales se realizarán el 24 de octubre. Aunque el Congreso
  tendrá la palabra final.

---
La Cámara Nacional Electoral (CNE) estableció este martes que las elecciones primarias se harán el 8 de agosto y las generales, el 24 de octubre, mientras que la campaña electoral para las PASO comenzará el 19 de junio.

Así quedó señalado en el cronograma electoral, aunque podría modificarse si el Congreso sanciona una ley para suspender las PASO de manera excepcional por la pandemia de coronavirus, en medio de una discusión que persiste entre el oficialismo y la oposición, e incluso dentro del propio Frente de Todos.

Este lunes, el presidente Alberto Fernández sostuvo que la realización de las primarias es una decisión del Congreso, aunque subrayó que "con lo que cuestan" se podrían pagar "las vacunas rusas" contra el coronavirus.

 

**Cronograma electoral** 

De acuerdo al cronograma electoral,  el 27 de abril será el cierre del padrón provisorio (180 días antes de la elección general, como marca la ley) y el 7 de mayo será la fecha de la publicación del padrón provisorio (10 días después de su cierre).

El 10 de mayo será el día límite para efectuar la convocatoria a las elecciones Primarias Abiertas, Simultáneas y Obligatorias (PASO), es decir 90 días antes de su realización y el 21 de ese mes será el fin del plazo para efectuar reclamos de electores sobre sus datos y solicitar eliminación de fallecidos 15 días a partir publicación del padrón provisorio.

El 9 de junio será el fin del plazo para solicitar reconocimiento de alianzas transitorias y confederaciones para participar en los comicios (60 días antes de las PASO), mientras que el 14 del mismo mes vence el plazo para solicitar la asignación de colores para las boletas para las PASO y elecciones generales.

El 17 de junio será el final del plazo para designar un responsable económico-financiero por agrupación política ante la DINE (Dirección Nacional Electoral).

Dos días después será el inicio de la campaña electoral para las PASO y también el fin del plazo para la presentación de listas de precandidatos ante las juntas electorales partidarias.

El 26 de junio finalizará el plazo para asignar espacios de publicidad en medios de comunicación audiovisual por sorteo público por la DINE y ocho días después, el 4 de julio, se iniciará la campaña electoral en medios de comunicación audiovisual para las PASO.

El 9 de julio, cuando resten 30 días para las PASO, será la fecha de la designación de autoridades de mesa y el fin de plazo para que los juzgados federales resuelvan sobre la aprobación formal de las boletas oficializadas, como también de la impresión y publicación de los padrones definitivos y el inicio de la difusión de mensajes institucionales de formación cívica y educación digital uso responsable y crítico de la información electoral disponible en internet.

El 14 de julio será el comienzo de la prohibición de actos públicos susceptibles de promover la captación del sufragio y el 19 finalizará el plazo para subsanar errores u omisiones existentes en el padrón (sólo enmienda de erratas u omisiones).

El 24 de julio comenzará la difusión de los lugares y mesas de votación y dos días después será la fecha límite para efectuar la convocatoria a las elecciones generales.

El 31 de julio comenzará la prohibición para realizar actos públicos de proselitismo y publicar y difundir encuestas y sondeos preelectorales y el 6 de agosto ( dos días antes de las PASO) finalizará la campaña electoral y comenzará la veda electoral.

Dos días después de las PASO, el 10 de agosto, se iniciará el escrutinio definitivo de esa votación, mientras que el 27 de agosto finalizará el plazo para asignar espacios de publicidad en medios de comunicación audiovisual por sorteo público por la DINE.

El 14 de se septiembre se iniciará la campaña electoral para las elecciones generales, 50 días antes de su realización. Ese mismo día se hará la designación de dos responsables económico-financieros por agrupación y finalizará el plazo para la registración de candidatos proclamados en las PASO.

El 19 de septiembre será el inicio de la campaña electoral en medios de comunicación audiovisual y el 24 será la impresión y publicación de los padrones definitivos; la resentación ante las Juntas Electorales Nacionales de los modelos de boletas; la ratificación de las autoridades de mesa designadas; el inicio de la difusión de mensajes institucionales de formación cívica y educación digital uso responsable y crítico de la información electoral disponible en internet.

El 29 de septiembre comenzará la prohibición de actos públicos susceptibles de promover la captación del sufragio (a 25 días de la elección general), el 7 de octubre será el fin del plazo para justificar la no emisión del voto en las PASO y el 9 comenzará la difusión de los lugares y mesas de votación para las generales.

El 16 de octubre comenzará a regir la prohibición de publicar o difundir encuestras o sondeos de opinión y pronósticos electorales y el 22 finalizará de la campaña electoral y comenzará la veda (prohibición de realizar actos públicos de proselitismo y publicar y difundir encuestas y sondeos preelectorales).

Dos días despues de las elecciones del 24 de octubre, finalizará el plazo para efectuar reclamos y protestas sobre vicios en la constitución y funcionamiento de las mesas y sobre la elección, mientras que también comenzará el escrutinio definitivo. El 23 de diciembre finalizará el plazo para justificar la no emisión del voto.