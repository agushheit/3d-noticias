---
category: Agenda Ciudadana
date: 2021-06-03T08:05:03-03:00
thumbnail: https://assets.3dnoticias.com.ar/vacunación.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Diario Uno
resumen: Perotti aseguró que en 10 días empieza la vacunación a menores de 60 sin
  comorbilidades
title: Perotti aseguró que en 10 días empieza la vacunación a menores de 60 sin comorbilidades
entradilla: El gobernador destacó el ritmo de vacunación ya que se aplican entre 20.000
  y 25.000 vacunas por día

---
El gobernador Omar Perotti realizó una evaluación del operativo de vacunación contra el Covid-19 que lleva adelante la provincia en territorio santafesino. En ese marco, destacó que en los 250 vacunatorios operativos, Santa Fe aplica entre 20 y 25 mil dosis por día, y que en los próximos 10 días se finalizará la vacunación de personas con comorbilidades y a continuación se avanzará con el grupo etario menor de 60 años. En esa línea, confirmó que ya recibieron al menos una dosis todos los inscriptos mayores de 60 años, personal de salud, policía, docentes, y el 60% de personas con discapacidad.

“Todos los que han visto de cerca nuestro operativo de vacunación, hablan muy bien de él, porque en la medida que llegan vacunas a la Argentina, y se distribuyen a cada provincia, actuamos con el mayor ritmo posible. Se ha vacunado siempre, incluso los fines de semana, tratando de que no quede stock”, explicó el titular de la Casa Gris, y sentenció: “El mejor lugar para la vacuna es el brazo de un santafesino o santafesina”.

En tanto que, respecto al avance de la campaña, el gobernador detalló: “Estamos vacunando a personas de entre 18 y 59 años con comorbilidades. Terminado ese grupo, en no más de diez días, seguiremos de manera descendente con personas sin comorbilidades por debajo de 60 años. Mientras tanto, seguimos vacunando para terminar con agentes del Servicio Penitenciario, los pocos policías pendientes, y a personas con discapacidad, un grupo que lleva más del 60% vacunado”.

Acerca del grupo de personas sin comorbilidades menores de 60 años, el gobernador aclaró que “hay muchos vacunados en ese rango etario: todos los docentes, policías y trabajadores del sistema de salud que se anotaron están vacunados, y el grueso de esos sectores esenciales son menores de 60 años”. Y sumó: “Tenemos un nivel de vacunación realmente importante, y con la expectativa de que los esfuerzos que Nación está haciendo para traer más vacunas a Argentina sigan funcionando muy bien”.

**Compra de vacunas**

En paralelo, Perotti hizo referencia también al esfuerzo que realiza la gestión para la compra de vacunas. “Hace más de dos meses que con distintas provincias argentinas se viene viendo cómo podemos sumar a la estrategia para traer vacunas a la Argentina. Pero acá hay que decir que lo que muchas veces sucede es que aparecen oferentes que, cuando se indaga un poco más, los grados de certeza empiezan a desvanecerse”.

Y agregó: “La necesidad es poder tener las vacunas inmediatamente, y traerlas ya mismo, pero esa posibilidad hoy no existe. Toda la información que tenemos la ponemos a disposición. Y si alguien sabe dónde comprar vacunas, nosotros tenemos la decisión y los recursos para hacerlo”.

Finalmente, acerca de la ley recientemente sancionada en la Legislatura provincial para la adquisición de vacunas, aseguró que se trata de un “buen gesto de acompañar con una herramienta legal, pero no significa que se ponga a Santa Fe primera en la lista de compra de alguna vacuna”.

Y concluyó: “Para nosotros es una necesidad, primero, poder identificar el lote de las vacunas y tener certezas de que el laboratorio que lo produce asegura su calidad y procedencia. Además, tienen que estar autorizadas por la ANMAT. Y ese lote debe haber sido conservado y tratado con los resguardos de cadena de frío que este tipo de vacunación exige. Insisto: hay que ser muy cuidadosos respecto de cuál es el interlocutor y la calidad de las vacunas que se ofrecen en el mercado. No hay provincia que no esté buscando y, hasta aquí, no hay provincia que esté teniendo”.