---
category: Estado Real
date: 2021-01-19T09:00:55Z
thumbnail: https://assets.3dnoticias.com.ar/restricciones.jpg
layout: Noticia con video
link_del_video: https://www.santafe.gov.ar/noticias/recursos/videosHD/2021/01/2021-01-18NID_269704O_1.mp4
author: Prensa Gobierno de la Provincia de Santa Fe
resumen: Hubo 140 personas aprehendidas y 24 vehículos secuestrados en la primera
  semana de aplicación del decreto que prohíbe la circulación nocturna
title: Hubo 140 personas aprehendidas y 24 vehículos secuestrados en la primera semana
  de aplicación del decreto que prohíbe la circulación nocturna
entradilla: Además, se podrá aprehender e identificar a asistentes y organizadores
  de fiestas clandestinas.

---
La provincia, a través del Ministerio de Seguridad, informó que se visualizaron resultados positivos a una semana de la aplicación del decreto que restringe la circulación nocturna en toda la provincia. En este sentido se realizan patrullajes constantes y controles dinámicos con el fin de supervisar los diversos puntos de la ciudad y evitar que los ciudadanos circulen luego del horario permitido.

En el caso de la ciudad de Santa Fe, el balance fue muy positivo y el coordinador del Ministerio de Seguridad, Facundo Bustos, explicó que "en una reunión conjunta con el Ministerio Público de la Acusación acordaron aprehender e identificar a las personas que no cumplan con el decreto y, a una semana de la puesta en vigencia del mismo, hubo un total de 140 personas aprehendidas y 24 vehículos secuestrados".

Asimismo, "logramos reducir la cantidad de eventos y desactivamos dos fiestas clandestinas, un cumpleaños en Cándido Pujado 3400 de Santa Fe y una fiesta en una quinta de Arroyo Leyes que tenia más de cien personas como asistentes", continuó el funcionario provincial.

Bustos recordó a la población que "es muy importante que se denuncien este tipo de eventos ya que permite desarticularlos e identificar tanto a los organizadores como a los asistentes a los mismos".