---
category: Estado Real
date: 2021-10-31T06:00:24-03:00
thumbnail: https://assets.3dnoticias.com.ar/industriales.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Perotti: "Que Santa Fe encabece las cifras de recuperación económica, inversión
  y trabajo registrado en Argentina es un merito enorme de los industriales"'
title: 'Perotti: "Que Santa Fe encabece las cifras de recuperación económica, inversión
  y trabajo registrado en Argentina es un merito enorme de los industriales"'
entradilla: El gobernador de la provincia participó este sábado de un almuerzo de
  reencuentro de empresarios locales, nucleados en la Unión Industrial de Santa Fe.

---
El gobernador de la provincia, Omar Perotti, participó este sábado al mediodía, en la ciudad capital, de un almuerzo organizado por la Unión Industrial de Santa Fe (UISF), en un reencuentro entre industriales locales. La actividad se llevó a cabo en los Salones del Puerto, en el Dique II del Puerto de Santa Fe.

En la oportunidad, Perotti agradeció a los industriales presentes “por el esfuerzo hecho en todo este tiempo; por haber sabido llevar de la mejor manera cada una de sus empresas en este enorme desafío que nos impuso la pandemia a todos”.

Además, agregó: “Que la provincia de Santa Fe esté hoy encabezando las cifras de recuperación económica, de inversión y trabajo registrado en la Argentina, es un mérito enorme de cada uno de ustedes porque están comprometidos con su actividad y con su comunidad”.

Asimismo, el mandatario santafesino ponderó “el entusiasmo de los industriales, aun en los momentos más duros de la pandemia, lo cual no fue una tarea fácil”, y reivindicó la “interlocución con el gobierno y las políticas públicas implementadas que permitieron el nivel de inversión y de crecimiento sostenido del sector”.

**SANTA FE, PROVINCIA ACTIVA**

En otro tramo, Perotti afirmó que “Santa Fe es una provincia activa, donde todos sus sectores industriales han recuperado un nivel de actividad superior al de varios años atrás; hemos crecido en las exportaciones; tenemos un puerto que ha activado nuevamente su carga de granos, y estamos en los primeros días de noviembre, de la llegada de barcazas con contenedores para ir generando, desde aquí, un vínculo con el puerto de La Plata y los destinos hacia Brasil y Asia, por ejemplo. Cuando decíamos hace muchos meses que Santa Fe se estaba poniendo de pie, no era una cuestión de discurso: era la realidad”, señaló el titular del Ejecutivo.

Asimismo, el mandatario agradeció a los industriales “el reconocimiento al trabajo común con el gobierno”, y elogió “todas las posibilidades que ha tenido el sector de exhibir un muy buen ritmo de actividad, aun en los inicios de la pandemia. Nuestra decisión es estar a la par del que trabaja, del que invierte y del que produce, y esto se nota. Y ahora aparecen los problemas lindos, como la necesidad de contar con más insumos y más energía porque el mercado crece”, ejemplificó, y aseguró que “vamos a brindarles a las empresas de los pueblos el mismo tratamiento de tarifas energéticas como si estuviesen asentadas en un parque industrial, porque eso genera arraigo”, justificó.

**ESTABILIDAD FISCAL**

Finalmente, el gobernador santafesino aseguró que “vamos a garantizarle al sector industrial la estabilidad fiscal hasta el 2023, que es lo que podemos hacer. Todas las pymes la tienen”. No obstante, reconoció que “debemos seguir consensuando la forma en que el esquema tarifario se va a sostener en la provincia, porque lo que pasa en una inundación o en una sequía se recupera. Pero aquí pasó una pandemia, con lo cual los daños no van a superarse en una temporada. Se necesita un Estado que esté presente y acompañando”, concluyó.

A su turno, el presidente de la UISF, Alejandro Taborda, manifestó que “pasamos momentos muy difíciles en años anteriores, pero la situación de la industria cambió; tenemos nubarrones pero estamos trabajando en eso. Como empresarios, hacemos todo los posible para solucionar esos problemas, pero si no tuviésemos el apoyo de los gobiernos nacional, provincial y municipal, sería difícil”, reconoció el dirigente empresarial.

En esa línea, Taborda agradeció al gobierno provincial “porque podemos tener un diálogo constructivo para solucionar problemas; para seguir creciendo; y para crear empleo. No es casual que hoy esté presente con nosotros el gobernador de la provincia; estamos trabajando juntos”, aseguró.

Por su parte, el secretario de Industria de la provincia, Claudio Mossuz, manifestó que “estamos contentos de poder tener nuevamente este encuentro tan tradicional, con integrantes de la Unión Industrial de Santa Fe, quizás un poco restringido en la asistencia producto de la pandemia, en un año que tendrá un cierre muy bueno para la provincia en lo que refiere a la actividad del sector”.

“Venimos de una semana de mucha actividad a partir de la visita de funcionarios del Ministerio de Desarrollo Productivo de la Nación, que presentaron todas las líneas de financiamiento para el impulso de la actividad industrial, como nunca las hubo”, agregó el secretario.

“En general, en la provincia, todos los rubros industriales están con números positivos respecto de 2019. El sector metalmecánico, por ejemplo, relacionado con la fabricación de maquinaria agrícolas y equipos, tuvo índices superiores a 2018”, explicó el funcionario.

Finalmente, Mossuz se mostró optimista en relación con las expectativas para 2022, en virtud de la consolidación de esta recuperación del sector industrial que “se va a afianzar”, aseguró.

**DISTINCIONES**

Durante la jornada, además, Perotti distinguió como “industriales ilustres” al fallecido escribano Gabriel Culzoni, reconocimiento recibido por su esposa e hijas; y a Ángel Poma Re,actual presidente del Parque Industrial de Santa Fe.

**PRESENTES**

Del encuentro participaron también el ministro de Trabajo, Empleo y Seguridad, Juan Manuel Pusineri; el presidente de la Cámara de Diputados de la provincia de Santa Fe, Pablo Farías; el diputado provincial, Nicolás Aimar; la diputada nacional, Erica Hynes; el secretario general de la Municipalidad de Santa Fe, Mariano Granato; el secretario de Producción de la Municipalidad de Santa Fe, Matías Schmuck; las concejalas locales, Jorgelina Mudallel y Laura Mondino; y el concejal, Julio Garibladi, junto con demás autoridades de entidades empresariales y educativas de la región.