---
layout: Noticia con imagen
author: .
resumen: Supermercados Santo Tomé
category: Agenda Ciudadana
title: Supermercados de Santo Tomé dejaron de entregar bolsas este fin de semana
entradilla: "La concejala de la UCR Florencia González se refirió a la decisión
  de los supermercados de Santo Tomé de dejar de entregar bolsas plásticas este
  fin de semana. "
date: 2020-11-18T13:44:02.421Z
thumbnail: https://assets.3dnoticias.com.ar/florgonzalez.jpg
---
Además, Florencia González indicó que ni el municipio ni el concejo fue consultado ante esta decisión adoptada para la Cámara, no hubo campañas informativas claras que informaran a los vecinos la novedad. “La única comunicación que hubo al respecto, fueron carteles colocados en los supermercados que se perdían en el resto de los afiches de las paredes”, afirmó.

La legisladora recordó que hay empresas santotomesinas que se dedican a la fabricación de bolsas y se preguntó “si estas empresas fueron informadas por el Municipio de esta medida, cómo la afecta de acuerdo a la mano de obra que generan y si tienen la posibilidad de refuncionalizar su actividad en el corto plazo para adaptarse a estas acciones que buscan erradicar las bolsas plásticas”.

![Canal de Youtube](supermercado.jpg "Canal de Youtube")

Mirá el video en nuestro [Canal de Youtube ](https://youtu.be/NQSwA8MTiU0)

La concejala de la UCR insistió que estas medidas son muy positivas para el ambiente, pero implican cambios culturales y deben trabajarse con todos los actores. “Por eso, ya estamos en contacto con emprendedores textiles de la ciudad, para que avancen en la confección de bolsas reutilizables, para ofrecerlas a los vecinos. También con distintas asociaciones ambientalistas de la ciudad, para concientizar e informar la medida junto con ellos. Como ya dijimos, son cambios muy positivos, pero que deben incluir a todos los santotomesinos”, señaló Florencia González.