---
category: Estado Real
date: 2020-11-28T13:43:12Z
thumbnail: https://assets.3dnoticias.com.ar/DICTADURA.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: 'Ultima dictadura: hallazgo importante'
title: La provincia informó sobre el hallazgo de importante documentación relacionada
  con la policía durante la última dictadura
entradilla: Se trata de numerosos archivos policiales que estaban en una vivienda
  particular. Aportan datos sobre el accionar represivo de la fuerza provincial en
  Santa Fe, Rosario, Reconquista y otras ciudades de la provincia.

---
La secretaria de Derechos Humanos y Diversidad, Lucila Puyol, junto a la directora provincial de Memoria, Verdad y Justicia, Valeria Silva, y Roberto Rioja, Secretario de Protección Civil, presentaron este viernes un informe sobre un importante hallazgo de documentación relacionada con hechos de la última dictadura cívico militar en Santa Fe.

En una conferencia realizada en la ex Comisaría Cuarta, hoy Espacio de Memoria y sede del Archivo Provincial de la Memoria, Puyol informó que “la documentación proveniente de la Policía hallada días atrás en una vivienda particular y que data de la última dictadura militar, acaba de ser trasladada para su clasificación al archivo, en el marco de la búsqueda de hechos de la última dictadura cívico militar en el país”.

Puyol relató que “un inquilino al acondicionar la casa para habitarla encontró documentación en una terraza y que, ante ello, se comunicó con nuestra Secretaría”. 

A partir de allí, acompañada por la directora provincial de Memoria, Verdad y Justicia, Valeria Silva, se trasladaron hasta el domicilio y al constatar la existencia de los documentos se convocó al fiscal general, Martín Suárez Faisal, donde se labró un acta que autorizó a la provincia a retirar toda esa documentación y que la parte pertinente a la búsqueda de hechos de la última dictadura cívico militar se deposite en la sede de la ex Comisaría Cuarta, hoy Espacio de Memoria y sede del Archivo Provincial de la Memoria”. 

Puyol remarcó que “era tanta la cantidad de papeles, que fue fundamental el trabajo de la Secretaría de Protección Civil, quienes pusieron todo el equipamiento necesario a disposición para retirar la documentación”. 

“Encontrar esto nos da mucha emoción, hace 20 años que nosotras estamos trabajando en estos temas, fuimos parte de la comisión de la Fiscal Tessio para encontrar libros en las comisarías y en este lugar (la ex Comisaría Cuarta) encontramos más de 500 libros de la época de la represión, que hoy son parte del Archivo de la Memoria y están en custodia de la Secretaría”, añadió la funcionaria provincial. 

“En lo personal también me emociona, porque en las carpetas sobre la libertad vigilada de las personas, está mi mamá, así que quiero expresarles un agradecimiento enorme a todos los trabajadores que participan de esta experiencia tan movilizadora, tan importante”, finalizó Puyol.

### DOCUMENTACIÓN POLICIAL

En tanto, la directora provincial de Memoria, Verdad y Justicia, Valeria Silva, subrayó que “en esta es una investigación que se está abriendo, ya hemos encontrado documentos que van desde el año 1968 hasta el año 1996. 

Toda la documentación es policial, mucha de ella está suelta, ya que posiblemente fueron atadas en una sola pieza, pero después fueron destruidas y desparramadas”. 

La documentación que no corresponde al período de la última dictadura será entregada al Ministerio de Seguridad que ya está interiorizado en el tema. 

En los papeles encontrados hay mucha documentación perteneciente al D3, del Departamento de Operaciones, el D5, el Departamento Judicial de la Policía de Santa Fe y el D4. 

La funcionaria provincial señaló que la documentación que todavía se está relevando “tiene que ver con las ciudades de Santa Fe, Rosario, Reconquista, pero hay de toda la provincia”.

### RECUPERACIÓN DE LA DOCUMENTACIÓN

Por su parte, el secretario de Protección Civil de la provincia, Roberto Rioja, contó cómo desde su área se realizó el trabajo para recolectar la documentación:

“Nos convocaron a Casa de Gobierno. Charlamos con las chicas, nos contaron lo que ellas habían visto y luego fuimos con gente especializada de nuestra Secretaría, para ver qué era lo que había que realizar”. 

Seguidamente se contactó a bomberos especializados, Rioja explicó que la documentación “estaba en un tercer piso, había que bajarlos con soga, embolsar, tener mucho cuidado porque había mucho material deteriorado. Para que le sirva a la Secretaría de Derechos Humanos había que sacarlo en buenas condiciones”. 

De la presentación participaron también la subsecretaria de Derechos Humanos y Diversidad, Anatilde Bugna y los ex presos políticos Francisco Alfonso Klaric y José Villarreal, entre otros.