---
category: Agenda Ciudadana
date: 2021-11-28T06:15:41-03:00
thumbnail: https://assets.3dnoticias.com.ar/CFK.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: 'Cristina Kirchner sobre el acuerdo con el FMI: "La lapicera siempre la tuvo,
  la tiene y la tendrá el Presidente"'
title: 'Cristina Kirchner sobre el acuerdo con el FMI: "La lapicera siempre la tuvo,
  la tiene y la tendrá el Presidente"'
entradilla: '"Que a nadie lo engañen sobre quién decide las políticas en Argentina",
  afirmó la vicepresidenta en las redes sociales.

'

---
La vicepresidenta Cristina Kirchner afirmó hoy que "la lapicera" para cerrar un acuerdo para reestructurar la deuda con el Fondo Monetario Internacional (FMI) "siempre la tuvo, la tiene y la tendrá el Presidente" Alberto Fernández, y destacó la "irresponsabilidad política de la oposición" por reclamarle que haga explícito su apoyo a un futuro entendimiento con el organismo.

"La lapicera no la tiene Cristina...Siempre la tuvo, la tiene y la tendrá el Presidente de la Nación. Y no lo digo yo, lo dice la Constitución Nacional. Que a nadie lo engañen sobre quién decide las políticas en la Argentina", apuntó la vicepresidenta.

En una carta publicada en su cuenta de Twitter, la ex mandataria disparó: "¿En serio que los mismos y las mismas que trajeron de vuelta el FMI a la Argentina, reiniciando el ciclo trágico de endeudamiento que Néstor Kirchner había clausurado en el año 2005, hoy no se hagan cargo de nada?".

El escrito, que lleva el título "De silencios y curiosidades. De leyes y responsabilidades", fue publicado a 24 horas de que se conozca el fallo que sobreseyó a Cristina en la causa por presunto lavado de activos, dádivas y asociación ilícita en la explotación de las empresas hoteleras Los Sauces y Hotesur, aunque  
no se pronunció al respecto.

"¿En serio que los mismos y las mismas que recorrieron el país y los canales de televisión recitando el mantra 'hay que quitarle la mayoría a Cristina en el Senado para que el Congreso no sea una escribanía del gobierno', ahora quieren que 'Cristina defina si el acuerdo con el FMI está bien o está mal'?", continuó.

En ese marco, la ex presidenta cuestionó el silencio de Juntos por el Cambio: "La actitud más curiosa proviene de la coalición opositora que ha ganado, a nivel nacional, las elecciones parlamentarias celebradas el 14 de noviembre pasado. Declaraciones como 'no vamos a decir nada del acuerdo con el FMI hasta que Cristina no opine' son moneda corriente en portales, programas de TV y redes sociales".

"¡Vamos! ¡Por favor! La política debe dejar de ser sólo un show para la televisión. A partir del 10 de diciembre de este año y por primera vez desde 1983, con el advenimiento de la democracia, el peronismo no tendrá quórum propio en la Cámara de Senadores de la Nación", enfatizó.

Asimismo, la vicepresidenta precisó: "Debo confesar que no me sorprende la irresponsabilidad política de la oposición. La historia de nuestro país está plagada de fuerzas políticas que llegaron al gobierno diciendo una cosa e hicieron exactamente lo contrario una vez que atravesaron la puerta de la Casa Rosada".

"Cuando se busca el voto popular en elecciones libres y sin proscripciones se debe ejercer la responsabilidad de esa representación. Más aún, cuando se han ganado las elecciones ¿O para qué quieren las bancas? ¿Para cobrar la dieta? ¿O tal vez para viajar al exterior con pasajes gratis y viáticos en dólares?  
¿Para posicionarse de cara al 2023?", ironizó.

En escrito, que ilustró con una imagen compuesta por la fachada de la Casa Rosada y el Congreso de la Nación, Cristina apuntó contra quienes le reclaman que explicite su postura sobre la negociación con el FMI, que genera divisiones dentro de la coalición oficialista.

En esa línea, apuntó: "Hace ya varias semanas desde los medios de comunicación hegemónicos, los sectores del poder real en la Argentina y, crease o no -según pude leer en letra de molde-, también desde el Fondo Monetario Internacional (FMI) y los brokers de Wall Street, se especula con 'el silencio de la vicepresidenta' y su posición respecto de un posible acuerdo con el FMI".

En ese momento, aprovechó para recordar que la reestructuración corresponde a la deuda de la Argentina por "los 57.000 millones de dólares que pidió el gobierno de Mauricio Macri en el año 2018, de los cuales se alcanzó a desembolsar en menos de un año, la bonita suma de 44.500 millones de dólares".

Luego de repasar el Proyecto de Ley de Fortalecimiento de la Sostenibilidad de la Deuda Pública, la vicepresidenta señaló: "El Senado de la Nación la aprobó con 65 votos favorables y una abstención. La Cámara de Diputados no se quedó atrás. Con fecha 11 de febrero del 2021, la convirtió en ley -bajo el número 27.612- con 233 votos afirmativos, 2 votos negativos y 2 abstenciones".

"Como se podrá observar, surge a simple vista que la totalidad de las fuerzas políticas de ambas coaliciones asumió la responsabilidad de decidir si se aprueba o no, lo que el Poder Ejecutivo negocie y acuerde con el FMI", enfatizó.

Y agregó: "Todo ello sin perjuicio de que es el titular del Poder Ejecutivo quien lleva adelante las negociaciones en ejercicio de su responsabilidad constitucional en esta materia".

Tras referirse a los efectos de la pandemia, advirtió sobre los escenarios que puede afrontar el país dependiendo del acuerdo con el FMI: "La definición que se adopte y se apruebe, puede llegar a constituir el más auténtico y verdadero cepo del que se tenga memoria para el desarrollo y el crecimiento con inclusión social".

"¡Y ojo! Que nadie está hablando de desconocer deudas. Creo que el kirchnerismo (y permítanme utilizar el 'ismo' para de algún modo homenajear la formidable gestión de quien fuera mi compañero de vida e identificar un proceso político del peronismo) tiene un atributo histórico que es el de haber pagado las deudas que generaron otros gobiernos", manifestó.

Por último, sentenció: "Hoy, como marca la Constitución y la ley 27.612, no es Cristina...Son los y las 257 diputados y diputadas y 72 senadores y senadoras quienes tienen la responsabilidad legal, política e histórica de aprobar o no cómo se va a pagar y bajo qué condiciones la deuda más grande con el FMI de todo el mundo y de toda la historia".

El texto cierra con una expresión de deseo de la ex presidenta: "Que Dios y la Patria los ilumine a todos y todas. Los argentinos y las argentinas lo necesitamos".