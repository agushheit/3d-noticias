---
category: La Ciudad
date: 2020-12-02T17:38:59Z
thumbnail: https://assets.3dnoticias.com.ar/jaton-playa.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: La Municipalidad delimita bosques nativos a la vera de la laguna Setúbal
title: La Municipalidad delimita bosques nativos a la vera de la laguna Setúbal
entradilla: 'Son seis los sectores y están ubicados en las Costaneras Este y Oeste.
  Poseen especies leñosas y están siendo identificados con cartelería específica. '

---
La intención es que concilien estos pequeños espacios verdes con la superficie de arena propia de la zona de playas.

A partir de la bajante del río, en la costa de la Laguna Setúbal se comenzó a dar un proceso en el que la naturaleza se expresa. En esta línea, cuando la Municipalidad comenzó con las tareas de limpieza de las playas para la temporada estival, aparecieron zonas con una gran cantidad de plantas leñosas como alisos, ceibos, sauces, entre otras especies de árboles. Entonces, desde la Municipalidad, se decidió delimitar y preservar esos sectores.

Paulo Chiarella, director de Infraestructura Verde de la Secretaría de Obras y Espacio Público, es el responsable de esta área y contó cómo comenzaron a trabajar con esta iniciativa con el objetivo de proteger y conservar este fenómeno que se produjo en la costa. Todo el trabajo que se hizo de la arena en la playa fue con el criterio de respetar cada uno de los puntos donde se observa el surgimiento de estos árboles.

El criterio del municipio es que este año convivan estos espacios verdes protegidos y la zona de arena con distintas actividades, propias de la temporada estival. “Antes, el arquetipo de las playas era de grandes extensiones continuas de arena y tiene que ver más con el paisaje marítimo. La naturaleza insiste todos los años y, este en particular, con la aparición de estas especies de leñosas. Entonces decidimos conciliar eso con la oferta de arena que también es muy importante”, dijo Chiarella.

En este sentido, va a haber algunos sectores naturales y otros de arena, y así se van a ir alternando como dos ofertas que se complementan de paisaje y de infraestructura de playa. “También es una protección de los servicios ambientales que nos brindan estos bosques porque son el soporte de una enorme biodiversidad; y otro servicio importante para nosotros es el de sombra”, destacó más adelante.

## **El proceso**

La Municipalidad detectó seis puntos sobre los que se van a concretar acciones de protección, tanto en la Costanera Este como en la Oeste. “En la parte Este, estos bosquecitos se están expresando de una manera más intensa para la mirada de la gente, está más clara la zona donde está creciendo aceleradamente esa vegetación leñosa; mientras que del lado Oeste está un poco más dispersa porque la naturaleza lo está haciendo de ese modo”, detalló.

En la costanera Este hay dos piezas bien claras que fueron perfiladas en los trabajos con las motoniveladoras; se trata de dos alisales incluso de diferentes edades uno de 6 meses y otro de 18 meses que se expresa muy claramente. Y los otros están en la zona Oeste, uno inmediatamente después de El Faro; y entre El Faro y los Espigones están apareciendo otros dos a tres más que también van a estar marcados.

La Municipalidad lo primero que hizo fue reconocer los sectores, después se los marcó e hizo el despeje de pastizales abajo y de esclarecimiento de la zona de arena para delimitarlas claramente y para poder colocar la señalética.

“También es una acción de divulgación sobre el valor que tienen esos bosques porque corremos el riesgo de que esos lugares se tomen como baldíos o se piense que están abandonados. Creemos que es una oportunidad para dialogar con la ciudadanía y que descubra, también, el gran valor que tienen estas áreas que se puede considerar primero como maleza y que se transformen en buenezas (este término es en contraposición a maleza para reivindicar la importancia de los pastizales naturales). Lo que más nos interesa reforzar es que es un proceso que está en desarrollo, son los primeros pasos de este ecosistema”, destacó el funcionario municipal.