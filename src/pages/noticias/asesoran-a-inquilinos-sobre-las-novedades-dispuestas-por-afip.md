---
category: La Ciudad
date: 2021-02-22T06:00:57-03:00
thumbnail: https://assets.3dnoticias.com.ar/afip.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Asesoran a inquilinos sobre las novedades dispuestas por AFIP
title: Asesoran a inquilinos sobre las novedades dispuestas por AFIP
entradilla: Se reglamentó el régimen de registración de contratos de alquiler de inmuebles
  previstos en la nueva Ley de Alquileres. El municipio brinda información pertinente
  tanto a inquilinos como locadores.

---
La Administración Federal de Ingresos Públicos (AFIP) reglamentó el régimen de registración de contratos de alquiler de inmuebles previsto en la nueva Ley de Alquileres (art. 16 de la 27.551). Se trata de una medida voluntaria para inquilinos/locatarios y obligatoria para inmobiliarias/locadores a partir del 1° de marzo y se establece un plazo de excepción hasta el 15 de abril del 2021 para los contratos que se efectivizaron a partir del 1° de julio de 2020 en adelante. Vale destacar que dicha resolución establece sanciones para quienes incumplan y no presenten la información requerida.

La resolución general N° 4933/21 de AFIP permite el reemplazo en la tarea por un intermediario (escribano o inmobiliaria), y establece que esta medida es retroactiva, es decir alcanza a los contratos posteriores a la nueva ley de alquileres, y que sigan vigentes.

**Más información**

Los contratos alcanzados por la resolución son: a) locaciones de bienes inmuebles urbanos, b) arrendamientos sobre bienes inmuebles rurales; c) Locaciones temporarias de inmuebles –urbanos o rurales- con fines turísticos, de descanso o similares; d) Locaciones de espacios o superficies fijas o móviles -exclusivas o no- delimitados dentro de bienes inmuebles como locales comerciales y/o "stands" en supermercados, hipermercados, shoppings, centros, paseos o galerías de compras, etc y e) contratos de locación celebrados electrónicamente mediante la utilización de plataformas digitales y/o aplicaciones móviles destinadas a tal fin