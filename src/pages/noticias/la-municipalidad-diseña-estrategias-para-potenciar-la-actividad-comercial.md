---
layout: Noticia con imagen
author: .
resumen: Estrategias para el comercio
category: Estado Real
title: La Municipalidad diseña estrategias para potenciar la actividad comercial
entradilla: Este miércoles se realizó un encuentro virtual con integrantes de
  asociaciones comerciales para definir campañas promocionales conjuntas ante la
  llegada de las fiestas de fin de año.
date: 2020-11-13T17:54:34.198Z
thumbnail: https://assets.3dnoticias.com.ar/zoom.jpg
---
En vísperas de las festividades de Navidad y Año Nuevo, la Municipalidad de Santa Fe convocó a las asociaciones comerciales para planificar, de forma conjunta, acciones tendientes a consolidar a la capital santafesina como un polo para la realización de compras.

Con ese objetivo, funcionarios de la Secretaría de Producción y Desarrollo Económico mantuvieron un encuentro virtual con representantes de asociaciones comerciales. El encuentro fue encabezado por el titular del área, Matías Schmüth, quien detalló que “la idea es ir pensando distintas acciones promocionales a realizar durante el mes de diciembre, siempre y cuando la situación sanitaria lo permita”.

El secretario afirmó que el diseño de estas acciones tiene como objetivo “fomentar el trabajo con los centros comerciales y potenciar a un sector muy golpeado durante este año, para que aumente sus ventas”.

Otro punto que se abordó en el encuentro es el fortalecimiento del turismo regional y su vinculación con el comercio, a través de descuentos y promociones. Al respecto, Schmüth señaló que “la ciudad es un polo de atractivo comercial para el centro y norte de la provincia, y seguramente en esta temporada de verano, muchos habitantes de la provincia elegirán Santa Fe Capital para visitar”.

**Asistentes**

Del encuentro participaron además, por la Municipalidad de Santa Fe, el director de Turismo, Franco Arone; y el director de Atención a Empresas, Andrés Doce. Representaron al Centro Comercial: el presidente, Martín Salemi, el gerente, Fabián Zanutigh, y la coordinadora de Centros Comerciales a Cielo Abierto, Sabrina Schmidt.

Además, hubo representantes de las asociaciones comerciales: Nancy Cheñenco por la Asociación de Comerciantes, Industriales, Profesionales y Amigos (ACIPA) de Avenida López y Planes; Georgina Gay y Sandra Tabarozzi por ACIPA Aristóbulo del Valle; Marcelo Ruscitti por ACIPA General Paz; María de los Ángeles Moyano por Paseo Boulevard; Adriana De Pedro de la Asociación Peatonal San Martín; Norberto Raselli por ACIPA Facundo Zuviría; y Rubén Morello, por calle Urquiza.