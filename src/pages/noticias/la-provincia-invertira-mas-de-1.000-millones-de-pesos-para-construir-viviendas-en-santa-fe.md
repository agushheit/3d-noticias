---
category: Estado Real
date: 2021-09-09T06:15:20-03:00
thumbnail: https://assets.3dnoticias.com.ar/VIVIENDAS.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La provincia invertirá más de 1.000 millones de pesos para construir viviendas
  en Santa Fe
title: La provincia invertirá más de 1.000 millones de pesos para construir viviendas
  en Santa Fe
entradilla: Se trata del plan de 108 viviendas con infraestructura y cocheras que
  se desarrollarán en el barrio Villa Setúbal de la ciudad capital. Además, se edificará
  un anexo para actividades de la UTN.

---
El Ministerio de Infraestructura, Servicios Públicos y Hábitat, a través de la Dirección Provincial de Vivienda y Urbanismo, realizó la apertura de sobres para ejecutar las obras para la construcción de 108 unidades habitacionales con infraestructura básica, desarrolladas en propiedad horizontal, 78 cocheras, para barrio Villa Setúbal de la ciudad de Santa Fe y un edificio anexo para actividades de la Universidad Tecnológica Nacional Regional Santa Fe.

Las unidades habitacionales serán de uno, dos, y tres dormitorios. La obra total posee un presupuesto oficial que supera los 1.000 millones de pesos.

Al respecto, el secretario de Hábitat, Amado Zorzón, comentó que “estas viviendas se enmarcan en un convenio con la Universidad Tecnológica Nacional, con el objetivo de trabajar fuertemente en el derecho a la vivienda y al hábitat. Esta es una de las tantas acciones concretas que trabajamos, en total van a ser 108 viviendas con infraestructura que se construirán en estos terrenos”.

Luego señaló que “tener más de 116 mil familias inscriptas en el registro de acceso a la vivienda muestra la alta demanda habitacional, y por eso maximizamos los esfuerzos. Este es el camino que nos transmitió el gobernador Omar Perotti, de generar políticas públicas y estrategias que garanticen el acceso a la casa propia y a un hábitat digno, porque entendemos que es un derecho fundamental que tienen todos los santafesinos y santafesinas”.

A su turno, el director Provincial de Vivienda, José Kerz, indicó que “una buena noticia para la ciudad, estar licitando viviendas con infraestructura. No sólo va a beneficiar a los vecinos que vendrán a vivir aquí, sino también al barrio entero”.

“La ayuda del Estado es muy importante para poder llevarlo a cabo. Estamos convencidos de este trabajo, junto con el gobernador Omar Perotti, y el equipo del Ministerio, seguiremos por este camino, redoblando los esfuerzos para mejorar el hábitat y que más santafesinos tengan acceso a su vivienda”.

**Las obras**

Cada vivienda estará dotada de instalaciones y conexiones para los servicios públicos de agua potable, cloacas y energía eléctrica. Los desagües pluviales descargan a la vía pública.

Las 78 cocheras (módulos de estacionamiento) proyectadas en cada grupo licitatorio, se desarrollan en aproximadamente 13 m2, cada una de ellas.

Asimismo, se construirá un edificio destinado a la UTN-FRSF para el desarrollo de actividades de posgrado, para el despliegue de nuevas tecnologías de información y comunicación (TICs), y estará ubicado en la manzana identificada como 5909 S, comprendida entre Tacuarí, Vélez Sarsfield, Sastre y Llerena. El edificio se desarrolla en planta baja y planta alta y posee una superficie cubierta de aproximadamente 3.000 m2.

**Ofertas**

En el primer grupo de licitaciones de 54 viviendas con infraestructura y 39 cocheras, realizado el 30 de agosto, con un presupuesto oficial de $342.461.936,80, se presentaron 9 ofertas: COIRINI S.A. cotizó $304.922.185,86; PILATTI S.A. cotizó $429.306.117,04, ésta empresa presentó una mejora de oferta quedando el monto final en $412.133.872,36; COEMYC S.A. cotizó $359.502.619,45; EPRECO S.A. cotizó $389.352.173,03; MUNDO CONSTRUCCIONES S.A. cotizó $381.587.003,86; COCYAR S.A. cotizó $364.567.511,40; VFM S.A. cotizó $328.800.441,89; CAPITEL CONSTRUCTORA S.A. cotizó $410.461.989,12; y una novena y última oferta correspondiente a la firma UT conformada por PIRÁMIDE S.A. y TECSA S.A. que cotizó la suma de $403.773.330,65.

Para el segundo grupo de 54 viviendas con infraestructura, 39 cocheras, y ejecución de edificios para actividades anexas UTN, con un presupuesto oficial de $661.373.967,91, se presentaron 9 ofertas: EPRECO S.R.L. cotizó $748.806.822,52; PECAM S.A. cotizó $740.473.936,11; UTE: TECSA y PIRAMIDE S.A. cotizó $663.113.970,34; COEMYC S.A. cotizó $671.589.295,12; DEPAOLI & TROSCE CONSTRUCTORA S.R.L. cotizó $922.933.910,89; CAPITEL S.A. cotizó $720.904.000,00; INGENIERO PEDRO MINERVINO S.A. cotizó $729.340.489,42; COIRINI S.A. cotizó $664.321.537, 26; y una novena y última oferta correspondiente a la firma COCYAR S.A cotizó la suma de $704.241.239,00.

**Presentes**

Acompañaron la actividad, autoridades de la Universidad Tecnológica Nacional - Facultad Regional Santa Fe (UTN-FRSF), representantes de empresas oferentes y público en general.