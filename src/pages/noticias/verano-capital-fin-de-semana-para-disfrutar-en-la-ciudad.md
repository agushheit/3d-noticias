---
category: La Ciudad
date: 2020-12-19T11:55:19Z
thumbnail: https://assets.3dnoticias.com.ar/1912-verano-capital.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: 'Verano Capital: fin de semana para disfrutar en la ciudad'
title: 'Verano Capital: fin de semana para disfrutar en la ciudad'
entradilla: 'Santafesinos y visitantes podrán vivir un fin de semana colmado de propuestas:  actividades
  culturales y recreativas, prácticas deportivas en playas y parques, La Diseña y
  calle recreativa en la Costanera Oeste.'

---
Entre este viernes y el domingo continúan las actividades culturales y deportivas para disfrutar en espacios abiertos. Cada una de ellas se realizará con demarcaciones que permiten un distanciamiento adecuado para poder disfrutar con los protocolos de prevención necesarios.

**El viernes 18  a las 19 horas, en la Playa Los Alisos de Barrio El Pozo, el músico Hernán Narváez**, con 19 años de trayectoria en la escena de la cumbia santafesina, repasará las canciones que interpretó a lo largo de su carrera, de sus dos discos solistas -de 2015 y 2019- y adelantará material de la próxima producción. Con nueve músicos en escena se podrá disfrutar de un show romántico y entretenido.

Además, **el sábado 19 a las 19 horas, en el Parador Laguna Beach**, ubicado en Almirante Brown 5294, **la banda de rock Cortamambo** brindará un show junto a la laguna con temas del EP que lleva el mismo nombre de la banda y está disponible en todas sus plataformas. Tomás Sosa en voz, Mateo Perezlindo en guitarra, Iván Mounier en batería y David Rivas en bajo integran el grupo desde 2016 y en este show despiden el año, mientras trabajan en la composición de nuevos temas para su próximo trabajo discográfico.

Y, por otro lado, **en la Vecinal de Colastiné Sur, el sábado a las 19 horas, desembarcará la propuesta de Barrio Abierto** con una feria de artesanos, eco canje de residuos secos por semillas y plantines para huerta familiar. También habrá **música en vivo a cargo de El Entrevero**, el proyecto que comparten Natalia Zehnder en acordeón, Marcos Galeano en guitarra y voz, José Alaluf en bajo, Mateo Malato en percusión y Leandro Goldsack en acordeón y voz. Los ritmos rioplatenses y los litoraleños se encuentran en un repertorio de canciones propias del grupo y composiciones de autores que son una referencia, como Ramón Ayala y Aníbal Sampayo.

**El domingo 20  a las 17.30 horas en el Parque Garay, las y los artistas del pequeño circo ambulante Circo del Litoral y del Circo Unión**, recorrerán el parque con números circenses para entretener a toda la familia.

# **Calle recreativa**

**Este sábado, la Costanera Oeste se transforma en calle recreativa**. La propuesta organizada por la Municipalidad se realizará **entre las 8 y las 12 horas, desde el Monumento al Brigadier López hasta el Faro**.

Calle recreativa es una iniciativa que, a través del trabajo conjunto entre el municipio y distintas organizaciones, pretende facilitar la circulación y promover la utilización masiva de la bicicleta como medio de movilidad amigable con el medio ambiente, incentivando la participación popular, la relación entre vecinos y, además, la apropiación del espacio público por parte de los ciudadanos.

# **Deportes al sol**

Las **actividades deportivas gratuitas** continúan durante este fin de semana en la capital santafesina en seis espacios a cielo abierto. Se trata de las sedes ubicadas en las playas de la **Costanera Este, la del Centro de Deportes de la Costanera Oeste, las del Espigón I y II y los Parques Sur y Garay**.

El viernes, de 10 a 11 y de 18 a 19 horas, se podrá asistir a **Funcional**, mientras que el viernes y el sábado, de 17 a 18 horas, habrá **Beach Handball** en los seis espacios. 

Además, durante todo el fin de semana, de 17 a 20 horas, se desarrollarán las actividades de **Beach Voley** y **Beach Fútbol**; de 17 a 18, habrá **Beach Rugby** (a excepción de la Costanera Este) y **juegos recreativos**; y de 18 a 19 horas, se brindarán **clases de ritmos**.

Cabe señalar que además de los 90 guardavidas que supervisarán las playas, la Municipalidad dispuso la presencia de más de **100 profesores de Educación Física que estarán a cargo de las actividades** deportivas y recreativas.

# **Más opciones**

Paralelamente, se realizarán otras actividades con el apoyo de la Municipalidad. En ese sentido, los **artesanos y las artesanas del Sol y la Luna invitan a comprar los regalos para las fiestas de fin de año**. Del 19 al 24 de diciembre realizarán su tradicional feria navideña en la **Plaza Pueyrredón**, ubicada en Bv. Gálvez 1600, con la participación de creadores de Santa Fe y la zona. El encuentro será del 19 al 23, de 16 a 22 horas; y el 24, de 10 a 19 horas. Cada tarde, Gustavo “Tuti” Nuñez, que es parte de la feria con su emprendimiento Autómatas cíclico, brindará un espectáculo para toda la familia. 

<span style="color: #EB0202;">Es obligatorio concurrir con uso de tapabocas y mantener el distanciamiento social.</span>

En el marco del **ciclo Jazz bajo las estrellas**, este **jueves 17 de diciembre, a las 21 horas**, se presentan en **La Birra es Bella** (Balcarce 1681) Fernanda Lagger en saxo, Gustavo Aznar en batería, y Guido Chiatti en bajo eléctrico y pedales, para interpretar composiciones propias y estándares de jazz.

Asimismo, en el **ciclo Atardeceres en Casa Tomada** que se lleva a cabo en Calle del Sol 2635 de San José del Rincón, se presentará **Galindez Dúo**, con Esteban Coutaz en piano y sintes, y Aníbal Chicco en bajo. La cita es el **domingo 20 en dos turnos, el primero a las 20.30 horas y el segundo a las 22 horas**, para garantizar las medidas de distanciamiento y cuidado del público.

Por otra parte, **este sábado 19, de 10 a 13 horas, en Capital Activa se brindará una capacitación sobre Fermentados - Microbiota y Probióticos**, a cargo de Ana Milena Giacomini y Gabriel Vinderola.

También se encontrarán **propuestas recreativas** el viernes, sábado y domingo, desde las 21, en **República del Oeste** (Av. Freyre 2765), y el sábado y domingo en **Club Atlético Villa Dora** (Ruperto Godoy 1231). Además, **Sergio Torres actuará el jueves, viernes y sábado, desde las 20 horas, en Piedras Blancas**; y el domingo **cerrará el fin de semana el Festival La Ciudad Suena**, que dará inicio a las 20.30 horas.

# **Ferias de fin de año**

Con motivo de la cercanía de las fiestas de fin de año, emprendedores, artesanos y diseñadores santafesinos presentarán sus producciones en distintas ferias de la ciudad.

Hasta el 22 de diciembre, de 16 a 21 horas, continúa **La Diseña en la Estación Belgrano** (Bv. Gálvez 1150), con la participación de 160 marcas de Santa Fe y su área metropolitana y Paraná. En esta edición especial **se suma el Taller de La Guardia**, con su habitual stand de venta y otro de exhibición celebratorio de su 60° aniversario. Además, **habrá un patio de comidas** gestionado por emprendimientos que participan en Capital Activa.

Paralelamente, **Montjuic Festival** se desarrollará el sábado y domingo, de 18 a 0.30 horas, **en la Residencia Stamati**, ubicada en la Costanera. El evento, que reúne gastronomía, stands de moda, arte, música y DJs, tendrá entrada libre y gratuita.

Además, **en Colastiné Norte se realizará la Feria Renace y Emprende**, de la que participarán artesanos, emprendedores y recicladores de antigüedades. Será el sábado y domingo, de 18 a 21 horas, en la **Plaza Padre Mario Mendoza**, ubicada en Las Mancluras y las Orquídeas.

El viernes, de 16 a 20 horas, se llevará a cabo la **Feria Artesanos del Sur** en Avenida Illia y 9 de Julio; de 16 a 21 horas la **Feria del Mercadito** en Plaza San Martín; y de 19 a 21 horas **Pícnic Nocturno Feria Igualdad en Plaza Constituyentes**.

El viernes y sábado, de 16 a 21 horas, se realizará la **Feria Despertar, Artesanos y Emprendedores** en Peñaloza 8751. En tanto que el sábado y domingo, de 16 a 20 horas, se podrá disfrutar de la **Feria Barrial Artesanal Sueños del Norte** en Baigorrita 9300, de la **Feria del Botánico en Avenida Gorriti** 3900  y de la **Feria del Orgullo en Plaza 25 de Mayo**.

Por otro lado, el sábado, de 16 a 21 horas, estará la **Feria Barrial Artesanal del Bulevar** en Pedro Vittori y Bv. Gálvez; de 17 a 21 horas la **Nueva Feria Artesanal Barrial Emprendedores Unidos** en Pasaje Iturri y Sarmiento y la **Feria de Artesanas y Emprendedoras Incuba** en Güemes y Córdoba de barrio Candioti Norte.

Para el domingo, de 16 a 20 horas, está prevista la **Feria Emprendedores de Santa Rosa** en Plaza Arenales y la **Feria Artesanal del Parque Federal**; de 17 a 21 horas, la **Feria Incuba en Plaza Constituyentes** y la **Feria Yeah de Artesanos y Emprendedores**; mientras que, de 16 a 21 horas, se llevará a cabo el **Paseo de Artesanos y Emprendedores Costanera Oeste**.

El martes se desarrollarán, de 16 a 21 horas, la **Feria Barrial Artesanal del Bulevar** en Pedro Vittori y Bv. Gálvez y la **Feria Incuba** en Vélez Sarsfield y Bv. Gálvez.

Por último, el miércoles 23 se llevará a cabo la **Feria Incuba en Plaza Constituyentes**.