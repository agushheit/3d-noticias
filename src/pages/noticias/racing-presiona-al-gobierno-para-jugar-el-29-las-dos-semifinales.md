---
category: Deportes
date: 2021-05-24T06:00:30-03:00
thumbnail: https://assets.3dnoticias.com.ar/RACING.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Racing "presiona" al Gobierno para jugar el 29 las dos semifinales
title: Racing "presiona" al Gobierno para jugar el 29 las dos semifinales
entradilla: 'Blanco, con línea directa a Balcarce 50, busca la “excepción” de jugar
  dentro del confinamiento. El 3 hay fecha Conmebol y perdería a los dos chilenos:
  Mena y Arias'

---
Presiones muchas, especulaciones varias, definiciones ninguna. En realidad, la única certeza es que “vamos a terminar todo en junio, para evitar que se caigan esos 27 contratos que corroboramos con Agremiados. Será ahora, ni bien se pueda”. Esa palabra de “Chiqui” Tapia, contundente, está vigente.

“Si fuera por Boca, Independiente y Colón, no hay objeciones con el primer borrador: el 2 las semifinales y el 5 ó el 6 la final, todo en el Bicentenario de San Juan”, confiaron ante la consulta de este diario cerca de Viamonte. Incluso, esa fecha ya iría por fuera del confinamiento inicial que marcó el Gobierno Nacional hasta el final de mayo.

¿Qué puede cambiar este borrador de aceptación casi generalizada con el visto bueno en tres de cuatro semifinalistas?: la fuerte presión de Víctor Blanco, presidente de Racing Club de Avellaneda, a esta altura claramente bajo la etiqueta de “cuarto en discordia”.

Blanco, conocedor del “Dejá Vu” y las saudades de los tiempos de Néstor, fanático de “La Academia”, juega con el sentimiento futbolero “K”. ¿Qué busca Racing?: que desde la Casa Rosada le den el visto bueno para jugar las semifinales el sábado 29 de mayo y la gran final el martes 1 de junio. De esa manera, gambetearía una posible complicación deportiva: el 3 de junio hay fecha de Eliminatorias y puede que el DT de Chile, Martín Lasarte, convoque a Eugenio Mena y/o Gabriel Arias para ese juego, casualmente contra la Selección Argentina de Messi en el Madre de las Ciudades de Santiago del Estero.

Pasa que la fecha del sábado 29 de mayo cae dentro del confinamiento y el pedido de Balcarce 50 a Viamonte fue “no jugar nada”. Luego, claro está, llegó la polémica con los partidos de las Copas Libertadores y Sudamericana que se jugarán igual en esta semana que se inicia.

Si el Gobierno cede a la presión de Blanco, las fechas de 29 para semifinales y 1, 5 o 6 para la final son las que pretende La Academia de Avellanda. De lo contrario, todo parece conducir a miércoles 2 para las semifinales y sábado 5/domingo 6 para la gran final en el Bicentenario de San Juan.

Incluso, un dato no menor a favor del Gobierno y en contra de la presión de Racing: las grandes compañías televisivas ven con muy buenos ojos que la gran final de la Copa de la Liga se juegue un fin de semana (ya sea sábado 5 o domingo 6 de junio) y no un día de semana como quiere Blanco.