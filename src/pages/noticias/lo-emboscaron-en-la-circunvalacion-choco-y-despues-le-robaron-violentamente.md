---
category: La Ciudad
date: 2020-12-02T17:40:53Z
thumbnail: https://assets.3dnoticias.com.ar/emboscada.png
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: LT10'
resumen: Lo emboscaron en la circunvalación, chocó y después le robaron violentamente
title: Lo emboscaron en la circunvalación, chocó y después le robaron violentamente
entradilla: Vecinos del country Los Molinos denuncian que constantemente son atacados
  con piedras y gomeras. Pero esta vez la situación terminó en un episodio de mucha
  violencia.

---
Esto pasó hace dos días en inmediaciones del acceso al country “Los Molinos” sobre la circunvalación oeste a la altura de la Bomba 5, justo detrás del Hipódromo.

Un conductor chocó contra una pieza de mampostería tirada sobre la carpeta asfáltica, rompió su auto, hizo un trompo, y terminó desbarrancando hacia uno de los lados de la autopista. Rápidamente, un grupo de entre 8 y 10 personas se abalanzaron sobre él con palos y armas de fuego, y violentamente lo golpearon para robarle todo lo que tenía encima.

Martín Danna, es presidente del Consejo del country Los Molinos y contó al Móvil de LT10 que en un principio les atribuían los piedrazos y gomerazos a “adolescentes que no medían las consecuencias”. Inicialmente se rompía algún vidrio o marcaba una chapa, pero no pasaba a mayores.

Sin embargo, “en las últimas horas tuvimos un episodio que fue la gota que colmó el vaso, y fue encontrar en la noche pedazos importantes de mampostería tirados en toda la calzada de la autopista a la altura de la parte trasera del hipódromo” explicó Danna.

Esta era una situación que se repetía una o dos veces por semana. Además, “lo de los piedrazos y gomerazos sucede a cualquier hora, y lo de los troncos y mampostería sucede a la tardecita porque es cuando no se ve porque no hay ninguna luz”.

![](https://assets.3dnoticias.com.ar/emboscada1.png)