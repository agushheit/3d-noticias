---
category: Agenda Ciudadana
date: 2021-12-29T06:15:06-03:00
thumbnail: https://assets.3dnoticias.com.ar/INTENDENTES.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: 'El Senado bonaerense aprobó la ley que habilita una reelección más para
  los intendentes: define Diputados'
title: 'El Senado bonaerense aprobó la ley que habilita una reelección más para los
  intendentes: define Diputados'
entradilla: 'Con 36 votos de legisladores del Frente de Todos y de la oposición, la
  Cámara alta aprobó el proyecto que permitirá que intendentes que llegaron al cargo
  en 2015 puedan presentarse a un tercer mandato en 2023.

'

---
El Senado y la Cámara de Diputados de la provincia de Buenos Aires trataban esta tarde la propuesta de modificación de la ley sancionada durante el Gobierno de María Eugenia Vidal que impide la reelección indefinida de intendentes.

Con 36 votos a favor de diputados bonaerenses del Frente de Todos y de la oposición quedó aprobada una reforma de la ley que regula las reelecciones de los intendentes, y que en los hechos permitirá que los jefes comunales que accedieron a su cargo por primera vez en 2015 y renovaron en 2019, puedan volver a presentarse como candidatos en 2023 para un tercer mandato.

Si bien tanto en el Frente de Todos como en Juntos por el Cambio el voto estuvo dividido, en ambas bancadas predominó el voto favorable a la reforma que derogó el artículo 7 de la ley, a propuesta del senador provincial del PRO Juan Pablo Allan y de Joaquín de la Torre (Peronismo Republicano).

El corazón de la ley no fue tocado, ya que continúa el tope de dos mandatos consecutivos por intendente (también concejales, legisladores y consejeros escolares). el cambio tiene que ver con que se tomará como primer mandato el período 2019-2023, y no el 2015-2019 como está establecido en la normativa vigente.

Con estos cambios, según quedó fijado en el texto de la normativa, se busca que "los mandatos de Intendentes, Concejales, Consejeros, Escolares, Diputados y Senadores que se hayan iniciado como resultado de las elecciones del año 2017, 2019 y 2021 serán considerados como primer período a los efectos de la aplicación de la presente Ley".

Por otra parte, se apunta a corregir un vacío de la ley del 2016 que abría una ventana para que los jefes comunales pudieran ir por un tercer mandato, si antes de cumplir con los dos años de uno de los dos períodos de gobierno se tomaban licencia.

Eso ocurrió con más de una veintena de intendentes que antes del 10 de diciembre de este año se pidieron licencia y podrán ser nuevamente candidatos en sus distritos en 2023.

La discusión continuará luego en la Cámara de Diputados bonaerense, donde los legisladores podrían convertir en ley la reforma.

"No hay ninguna duda de que todos los que estamos acá en este bloque seguimos diciendo que no a las reelecciones indefinidas. No tenemos ninguna duda ni nada para esconder, y lo ponemos sobre la mesa. Mostramos incluso nuestros disensos en este recinto, pero hay una mayoría en este bloque que orgullosamente va a acompañar el proyecto del senador Juan Pablo Allan y del senador Joaquín De la Torre. Y lo va a acompañar con la frente en alto, mirando a la cara a los bonaerenses", adelantaba el presidente del interbloque de Juntos por el Cambio, Christian Gribaudo.

"El espíritu de la ley es excelente y continúa vigente pero también tenía mucho para mejorar y una reglamentación que generaba presentaciones judiciales", sostuvo.

Por su parte, la presidenta del bloque del Frente de Todos Teresa García hizo una "reivindicación de la figura del intendente" luego de haber entablado relaciones permanentes cuando era ministra de Interior.

"No es una defensa de la corporación política sino una defensa de la política como herramienta de transformación. Anticipo el acompañamiento de la mayoría de nuestro bloque atendiendo las diferencias", indicó.

El Frente Renovador de Sergio Massa, que cuenta con 12 diputados y cuatro senadores provinciales, había anticipado su rechazo a una reforma que prolongue en los hechos los dos mandatos estipulados en la ley vigente.

"Vamos a votar en contra de las reelecciones indefinidas. Si el Presidente y el Gobernador tienen un límite de dos mandatos, no entiendo porque ese límite no deberían cumplirlo los intendentes", argumentaba antes de la sesión el presidente de la Cámara de Diputados, que se expresó a favor de la alternancia política.

Tampoco el vidalismo, con el que el massismo había acordado en 2016 para impulsar la ley, está de acuerdo con correr al 10 de diciembre de 2019 la fecha que se toma como referencia para computar el inicio del primero de los dos mandatos.

"La reelección no es un problema real de los argentinos, sino de los políticos. Como todos sabemos los problemas reales de los argentinos, que no pueden seguir esperando, son la pobreza, la inflación, la inseguridad, el narcotráfico y tantos otros", fundamentó Vidal en declaraciones periodísticas.

La Coalición Cívica de Elisa Carrió se plegó a la postura de un sector del PRO y del Frente Renovador en contra de la reforma.

El senador y presidente del bloque de la Coalición Cívica, Andrés De Leo, advirtió que "el efecto inmediato de esta ley" es la habilitación de la reelección de intendentes y legisladores".

"Dejamos con un respirador artificial la limitación de las reelecciones. Van a tener cuatro preciosos años para trabajar en la derogación de esta ley. Nosotros hemos demostrado que no estamos preparados para defender una ley”, sostuvo.

La legislatura bonaerense también tratará este martes un paquete de normas clave para el Gobierno de Axel Kicillof, como el proyecto de Presupuesto 2022, la Ley Impositiva y la reforma de la Ley de Ministerios.