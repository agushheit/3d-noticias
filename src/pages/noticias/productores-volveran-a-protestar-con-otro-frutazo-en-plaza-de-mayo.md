---
category: Agenda Ciudadana
date: 2020-11-30T11:19:57Z
thumbnail: https://assets.3dnoticias.com.ar/FRUTAZO.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: BAE Negocios'
resumen: Productores volverán a protestar con otro "frutazo" en Plaza de Mayo
title: Productores volverán a protestar con otro "frutazo" en Plaza de Mayo
entradilla: Organizaciones vinculadas a productores agroecológicos de frutas y verduras
  entregarán mercadería gratis en Plaza de Mayo a el próximo martes 1 de diciembre
  a partir de las 13.

---
La Federación Nacional Frutihortícola Argentina (Fenafrut) convocó para el próximo martes primero de diciembre a las 13 un reparto gratuito de frutas y verdutas en la Plaza de Mayo en Buenos Aires.

Este nuevo "frutazo" o "verdurazo" es una forma de "protesta pacífica" en contra de la "proliferación de mercados, galpones y depósitos informales", que "no garantizan la calidad, sanidad y trazabilidad en la comercialización", informó la Fenafrut en un comunicado.

  
 El sector frutihortícola reclamó la atención de las autoridades gubernamentales, nacionales y provinciales a través de las acciones llevadas a cabo por la Unión de Trabajadores de la Tierra (UTT), al usar estas metodologías de comercialización agroecológica en mercados registrados.

La Fenafrut participará en conjutno con otras entidades autoconvocadas como las Cámaras de Operadores Mayoristas Frutihortícolas, las Cámaras de la producción, Cooperativas de Trabajo de Carga y Descarga, Changadores libres y organizaciones de verduleros y fruteros.