---
category: La Ciudad
date: 2021-01-22T03:54:52Z
thumbnail: https://assets.3dnoticias.com.ar/clubes.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad de Santa Fe
resumen: Disponen protocolo para que los clubes tengan asambleas presenciales
title: Disponen protocolo para que los clubes tengan asambleas presenciales
entradilla: La Municipalidad de Santa Fe dispuso un protocolo para que los clubes
  de la ciudad puedan realizar elecciones o asambleas presenciales con más de 20 personas.

---
Los clubes de la ciudad de Santa Fe que deban efectuar asambleas o actos eleccionarios que supongan la asistencia de más de 20 personas ya cuentan con un procedimiento para poder realizarlos. Así lo dispuso el municipio a través de un decreto que establece, entre otras cosas, que los representantes jurídicos de las entidades tendrán que solicitar la habilitación sanitaria correspondiente.

En diciembre pasado, la Inspección General de Personas Jurídicas (IGPJ) de la provincia había condicionado la realización de asambleas y elecciones a la habilitación y el seguimiento de protocolos sanitarios municipales. Todo ello, en el marco de la extensión del decreto del Gobierno Nacional que establece el distanciamiento social, preventivo y obligatorio en nuestra provincia.

En ese sentido, el municipio dispuso que los clubes deban tramitar un permiso sanitario, a partir del cual quedan obligados a respetar condiciones mínimas. Por caso, las actividades solo podrán tener lugar entre las 8 y las 20, y siempre que las características del acto lo permitan deberá priorizarse un sistema de turnos para la concurrencia de las personas habilitadas a tales fines.

La solicitud de permiso sanitario deberá ser ingresada con una antelación mínima de siete días a la fecha en que se pretende realizar la elección o asamblea, para que las autoridades sanitarias evalúen las condiciones propuestas. Será obligación de las entidades presentar “descripción detallada de las características del acto a realizar” y “cantidad de personas habilitadas para la participación y estimación del cupo esperado de concurrencia”.

Otra obligación de los clubes será la de presentar “un plano o croquis” donde se realizará el evento (indicándose la superficie del espacio a utilizar, su lugar de emplazamiento y demás detalles de los parámetros de accesibilidad, de gestión y mitigación del riesgo). Luego se establecen las condiciones mínimas de resguardo sanitario para los concurrentes.

Entre otras cosas, los clubes deberán tomar la temperatura corporal a cada asistente antes del ingreso al lugar; estarán obligados a higienizar el calzado y las manos de cada persona, aportando los elementos correspondientes; y tendrán que exigir a los asistentes la utilización de barbijo que cubra nariz, boca y mentón durante todo el tiempo que permanezcan en el lugar.

También, se procurará el uso de espacios abiertos y, si no hubiera otra opción más que hacer la reunión en un lugar cerrado, se pedirá mantener la ventilación permanente. Asimismo, tendrán que controlar la densidad de ocupación, previendo dos metros de espacio libre en torno a cada asistente, tanto al aire libre como en lugares cerrados; y deberán preverse lugares de ingreso y egreso independientes.