---
category: La Ciudad
date: 2021-11-05T06:00:59-03:00
thumbnail: https://assets.3dnoticias.com.ar/ASUETO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Servicios previstos por el asueto del Día del Empleado Municipal
title: Servicios previstos por el asueto del Día del Empleado Municipal
entradilla: "La fecha se celebra el primer viernes de noviembre. La Municipalidad
  detalla los horarios de los diferentes servicios que se prestarán ese día.\n\n"

---
La Municipalidad informa los servicios que se prestarán durante el asueto correspondiente al viernes 5 de noviembre, jornada en la que se conmemora el Día del Empleado Municipal.

En tal sentido, en el Cementerio Municipal, el horario de las visitas será el habitual, de 7.30 a 12.30, y las inhumaciones también se harán dentro de ese horario. Sin embargo, no se realizarán cremaciones.

Por otra parte, las cajas municipales -tanto en el Palacio Municipal, como en las oficinas de distrito y la que funciona en el Depósito de Vehículos Retenidos (bulevar Pellegrini y Presidente Perón)- permanecerán cerradas.

No habrá cambios en el barrido y la recolección de residuos en la ciudad. También funcionará el sistema de estacionamiento medido (Seom) y la frecuencia de colectivos será la habitual para un viernes.

Cabe mencionar que el Día del Empleado Municipal se celebra el primer viernes de noviembre de cada año, según quedó establecido en el acta acuerdo rubricada entre la Municipalidad y la Asociación Sindical de Empleados Municipales (Asoem) en diciembre de 2008.