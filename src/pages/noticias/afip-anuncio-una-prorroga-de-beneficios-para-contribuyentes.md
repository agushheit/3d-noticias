---
category: Agenda Ciudadana
date: 2020-11-30T15:50:56Z
thumbnail: https://assets.3dnoticias.com.ar/afip.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: AFIP'
resumen: AFIP anunció una prórroga de beneficios para contribuyentes
title: AFIP anunció una prórroga de beneficios para contribuyentes
entradilla: Este lunes se renuevan distintos beneficios para pymes, monotributistas
  y pequeños contribuyentes.

---
La Administración Federal de Ingresos Públicos (AFIP) prorrogó hasta fin de año la vigencia de distintos beneficios destinados a aliviar el impacto económico de la pandemia sobre los contribuyentes y facilitar trámites en el marco de la emergencia sanitaria.

En un comunicado, el ente recaudador fiscal señaló que "las medidas instrumentadas por el organismo que encabeza Mercedes Marcó del Pont serán publicadas mañana (por este lunes) en el Boletín Oficial".

  
 

## **Monotributistas**

Ningún monotributista será dado de baja de oficio del régimen por la falta de pago de sus obligaciones durante noviembre. La normativa también suspendió las exclusiones de monotributistas correspondientes a octubre. A través de la Resolución General N°4863 se extienden los beneficios vigentes desde marzo. La normativa establece la baja del régimen cuando no se abonan diez cuotas consecutivas. La AFIP resolvió que octubre no será computado a los efectos de contabilizar el período necesario para la aplicación de las bajas automáticas.

Se prorroga la suspensión de los embargos hasta el 31 de diciembre para aquellos contribuyentes inscriptos en el Registro MiPyme. La Resolución General N°4868 también mantiene suspendida la iniciación de ejecuciones fiscales.

##   
   
**Presentaciones digitales**

Con el objetivo de facilitar la realización de distintos trámites se mantiene la obligatoriedad del uso del servicio Presentaciones Digitales hasta el 31 de diciembre. Para utilizarlo es necesario contar con clave fiscal y Domicilio Fiscal Electrónico, registrado y confirmado. La decisión está prevista en la Resolución General N°4867. La normativa también posterga, hasta el último día del año, la excepción de registrar los datos biométricos. De esta forma, los ciudadanos no deben concurrir a las dependencias para registrar su foto, firma y huella dactilar.

##   
**Planes de pago permanentes**

La Resolución General N°4866 extiende hasta el 31 de diciembre los beneficios en materia de tasas de interés, cantidad de cuotas y calificación de riesgo previstos para la "adhesión temprana" a planes permanentes.

  
 