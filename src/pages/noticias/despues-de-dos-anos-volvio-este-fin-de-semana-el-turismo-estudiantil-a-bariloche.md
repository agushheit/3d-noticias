---
category: Agenda Ciudadana
date: 2021-10-05T06:00:17-03:00
thumbnail: https://assets.3dnoticias.com.ar/barilo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Después de dos años volvió este fin de semana el turismo estudiantil a Bariloche
title: Después de dos años volvió este fin de semana el turismo estudiantil a Bariloche
entradilla: Son unos 1500 jóvenes, de cuatro empresas del rubro, cifra que se ampliara
  a otros 1500, en el transcurso de la semana, y que forman parte de una prueba piloto.

---
El turismo estudiantil retornó a San Carlos de Bariloche luego de una pausa de dos años debido a la pandemia de coronavirus, con el arribo de unos 1.500 jóvenes en cuatro contingentes durante este fin de semana, que se incrementará con una cantidad similar durante los próximos días y seguirá en aumento hasta los 5.000 por semana, según autoridades del sector.  
  
Estos primeros arribos constituyen una prueba piloto de la actividad, que desarrollan en conjunto el municipio y organizaciones empresariales, como la Asociación de Turismo Estudiantil de Bariloche (Ateba), que servirá para optimizar los estrictos protocolos vigentes.  
  
Entre éstos figura una prueba de PCR negativo a la que los jóvenes deben someterse antes del viaje, el uso obligatorio de barbijo y la constante higienización de sus manos con alcohol en gel, además de conservar un comportamiento de burbuja.  
  
Las excursiones, boliches bailables y hoteles están habilitados en su totalidad, y en todos los casos se realizan desinfecciones de forma permanente, de acuerdo a las instrucciones consensuadas entre las autoridades municipales y el sector empresario.  
  
Los jóvenes ya estaban este lunes en los hoteles, se los veía por las calles céntricas y en las próximas noches comenzarán a concurrir a las discotecas, que fueron acondicionadas en forma especial para recibirlos en el contexto de la pandemia.  
  
También los demás establecimiento de asidua concurrencia de estudiantes son sometidos a estos protocolos, como los gastronómicos, los comercios y fábricas de chocolates y los de ventas de recuerdos de Bariloche, entre otros.  
  
Desde Ateba, su presidente, Lato Denoya, precisó a Télam que durante las próximas dos semanas ingresarán otros 3.000 alumnos, y que el número aumentará hasta 5.000 por semana.  
  
Si los protocolos muestran su efectividad, como esperan, el sector empresarial planea solicitar autorización para el ingreso de 9.000 de estos turistas por semana, dado que la ciudad tiene sobrada capacidad para albergarlos.  
Tanto desde el ámbito privado como la municipalidad y el de los trabajadores del sector manifestaron su satisfacción por el reinicio de los viajes de egresados, ya que más de 2.000 familias de Bariloche trabajan en forma directa en esta actividad, y otro tanto lo hace en forma indirecta.  
Mientras pasean por la ciudad con sus coloridos uniformes que señalan a que empresa de viaje pertenecen, y felices de poder concretar esta experiencia que marca la finalización de un ciclo en sus vidas, los jóvenes dan un impulso importante a la llamada "industria sin chimeneas” de San Carlos de Bariloche.  
  
El turismo estudiantil fue durante años un factor determinante en la economía de este destino, con un numero cercado a los 120.000 pasajeros por año en los tradicionales viajes de egresados.  
  
Con su presencia, estos viajeros le daban estabilidad y previsibilidad a la actividad económica, no sólo por su volumen sino también por la cantidad de noches/cama ocupadas por los jóvenes en viaje de egresados, quienes normalmente permanecen en la ciudad durante unos ocho días.  
  
La pandemia de Covid-19 afectó a este sector como a la actividad turística en general, pero en el caso especifico de los viajes de egresados por su particularidad de llevarse adelante en forma grupal.