---
category: La Ciudad
date: 2021-08-15T06:15:12-03:00
thumbnail: https://assets.3dnoticias.com.ar/TRENES.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'Circunvalar ferroviario: cuál es la traza y qué mejoras traerá esta obra
  clave para la ciudad'
title: 'Circunvalar ferroviario: cuál es la traza y qué mejoras traerá esta obra clave
  para la ciudad'
entradilla: " Unirá Santo Tomé con Laguna Paiva. Evitará que los trenes pasen por
  la ciudad de Santa Fe y hará más eficiente el traslado de las cargas hacia los puertos. "

---
Elevar la mirada y posarla por encima de la perspectiva humana permite dimensionar la magnitud real de los hechos. Las imágenes capturadas por el drone de El Litoral muestran los caminos que atravesará la proyectada Circunvalación Ferroviaria, desde el norte de Santo Tomé hasta Laguna Paiva.

Para la capital provincial, esta obra tiene una gran relevancia. Las vías que cruzan de este a oeste la ciudad de Santa Fe son utilizadas por una gran cantidad de trenes que marchan cargados de granos hacia los puertos de Rosario. Pero son un problema: no sólo por las demoras que generan al tránsito urbano y por los frecuentes descarrilamientos de vagones, sino también porque en algunos barrios pasan casi pegados a viviendas muy precarias.

"Hemos visto hasta tres descarrilamientos seguidos, unos atrás del otro; nos llevaron la columna, parte de la reja", contó la vecina Mónica Castillo, quien vive muy cerquita de la vía que pasa por Pedro Vittori al 3800.

"Tengo miedo por los chicos, por mis nietos… viste que ellos se te van de la mano, te diste vuelta y ya no los tenés más al lado", relató Elsa Pérez, una vecina de Santa Rosa de Lima.

Para Carlos Pérez, otro vecino de ese barrio, "a experiencia con el tren no es buena, hemos tenido varios accidentes de autos, han derribado la casa de la esquina que ya reconstruyeron, y como no hay valla ni nada que te avise, seguramente va a seguir pasando".

**Los beneficios**

La construcción de una nueva Circunvalación Ferroviaria que bordeará Santa Fe por el oeste ya fue adjudicada por el gobierno nacional en 5 mil millones de pesos. El proyecto se diseñó durante la gestión de Cambiemos al frente de los gobiernos nacional y municipal, y fue tomado por las actuales autoridades, que le dieron impulso y adjudicaron los trabajos a varias empresas, según los trayectos, el 21 de julio pasado.

Martín Gainza, vicepresidente de Trenes Argentinos, estimó que "en uno o dos meses la obra podría comenzar, y durará alrededor de dos años".

La nueva traza solucionará los inconvenientes mencionados y, además, permitirá aumentar y hacer más eficiente el traslado de las cargas. Al pasar por el medio de zonas pobladas, con 62 pasos a nivel como es el caso de la capital provincial, los trenes tienen que reducir la velocidad. En la actualidad, las formaciones atraviesan la ciudad a 20 kilómetros por hora y la conexión entre el puerto de Timbúes, al sur provincial, y Laguna Paiva se demora diez horas. Trenes Argentinos estimó que con esta obra se incrementará la velocidad de traslado a 65 kilómetros por hora y se reducirá la demora a 2.30 horas.

Lo que se busca es, en definitiva, crecer en la cantidad de carga transportada y trenes que se operan, y mejorar la seguridad vial porque el tren ya no va a atravesar la ciudad de Santa Fe ni va a ser una barrera física que la divida.

"El tren va a salir del ejido urbano de la ciudad de Santa Fe. Hoy, los trenes de carga que pasan tienen entre 30 y 50 vagones. Con esto, van a pasar a tener hasta casi 100 vagones. Además, se va a mejorar la velocidad de circulación, con mayor seguridad y más transporte de carga", explicó Gainza, quien además mencionó que será una fuente de trabajo que, según estima, empleará a más de mil personas.