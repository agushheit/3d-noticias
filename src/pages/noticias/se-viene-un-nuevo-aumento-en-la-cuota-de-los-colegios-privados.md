---
category: Agenda Ciudadana
date: 2021-03-12T06:54:15-03:00
thumbnail: https://assets.3dnoticias.com.ar/escuela.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Diario Uno
resumen: Se viene un nuevo aumento en la cuota de los colegios privados
title: Se viene un nuevo aumento en la cuota de los colegios privados
entradilla: Al 8% implementado este mes se le sumará un incremento igual al aumento
  que tuvieron los docentes en la paritaria. El primer impacto sería de 18%

---
Con la propuesta de aumento salarial del 35% en tres etapas que acercó el gobierno provincial a los docentes, automáticamente se incrementará la cuota de escuelas privadas. Los aumentos sucederán con el mismo porcentaje de aumento salarial de los maestros y al momento de entrar en vigencia. Se espera que el impacto en los aranceles de marzo o abril sea del 18%.

Rodolfo Fabucci, director provincial de Educación Privada, adelantó a UNO Santa Fe que "si en este momento se dicta por decreto del gobernador el aumento salarial docente del 18% después de que hayan recibido la propuesta y la voten favorablemente, evidentemente va a impactar en la cuota al momento en el que se dicte el decreto del aumento".

"Si la propuesta del 35% de aumento a los docentes se divide en un 18% en marzo, 8% en julio y 9% en septiembre, la grilla se va a mover en esos mismos porcentajes automáticamente al momento en que entren en vigencia esos aumentos", destacó Fabucci.

El monto de la cuota de escuelas privadas está regulado hace ya 18 años por la disposición provincial 018 del año 2003. Al respecto, Fabucci manifiesta: "En ese lapso han pasado gobiernos peronistas, socialistas y nosotros no cambiamos ni queremos cambiar las reglas de juego, tanto para las escuelas como para las familias".

En este sentido, el Director de Educación Privada de la provincia manifiesta que el aumento salarial "impacta automáticamente porque depende del salario del docente que recién se inicia. Hoy en día un maestro de grado que recién se inicia, con el incentivo docente y el seguro mutual remunerativo y no remunerativo cobra $48.824".

"Nosotros mantenemos las disposiciones vigentes y las reglas de juego. Sino les estaríamos cambiando las reglas de juego a las entidades propietarias y a los padres, que saben que tienen cuotas que abonar en función de esto", argumentó Fabucci sobre la normativa del aumento de aranceles.

Actualmente la escala de cuotas vigente en las 850 escuelas privadas de la provincia es: escuelas con 100% de aporte estatal un máximo de $2.441, escuelas con aporte estatal del 80% el tope es de $6.591, las del 66% de aportes $8.788 de tope, 60% de aportes estatales con $9.521 de máximo y aquellos colegios con 40% de aportes del estado pueden cobrar hasta $10.985 de cuota.

**La Matrícula 2021 se sostuvo con respecto a la de 2020**

Frente a la consulta por las inscripciones a escuelas privadas para el ciclo 2021, Fabucci respondió que "en este momento no hay migraciones a escuelas públicas. Nosotros vemos esto a través del SIGAE (Sistema de Gestión Administrativa Escolar). Lo veremos reflejado en abril, cuando las escuelas ya carguen todos sus alumnos en el sistema y podemos comparar entre abril 2020 y 2021".

"Hasta ahora podemos asegurar que no hubo una caída, al contrario de eso hubo un sostenimiento. Si podemos encontrar migraciones entre escuelas privadas, por ejemplo con familias que busquen pagar una cuota de $6.000 en lugar de una de $10.000, aunque esto se da dentro del sistema privado", aclaró el director provincial de Educación Privada.

"Estamos teniendo pedidos de desdoblamiento por crecimientos vegetativos. Esto consiste en que los cursos que superen los 40 alumnos, por disposición del protocolo sanitario, se pueda abrir otro curso para el mismo grado. Esto dependerá de la capacidad edilicia y de las posibilidades de incrementar los cursos. En este momento hay inscriptos 248.000 alumnos en escuelas privadas de la provincia", destacó Fabucci.

El director provincial de Educación Privada sostuvo que tanto en los pueblos como en las grandes ciudades la situación de la matrícula en las escuelas privadas es la misma. Y para concluir, agregó: "hasta ahora tampoco cerró ninguna escuela privada en la provincia por falta de alumnos".