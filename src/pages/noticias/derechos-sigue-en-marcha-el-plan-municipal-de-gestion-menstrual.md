---
category: La Ciudad
date: 2021-04-20T06:22:13-03:00
thumbnail: https://assets.3dnoticias.com.ar/menstrual.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad Santa Fe
resumen: 'Derechos: sigue en marcha el Plan Municipal de Gestión Menstrual'
title: 'Derechos: sigue en marcha el Plan Municipal de Gestión Menstrual'
entradilla: El programa busca garantizar el acceso universal y gratuito a productos
  de higiene menstrual con la previa inscripción a un registro. También promueve la
  realización de talleres educativos y de concientización.

---
El Plan de Gestión Menstrual sigue a paso firme en la ciudad de Santa Fe. Para la implementación de esta propuesta que promueve el acceso universal y gratuito de productos de gestión menstrual en distintos espacios de la Municipalidad, se encuentra abierto el Registro Único para Personas Menstruantes, y próximamente se comenzará con talleres en los que se brindará información sobre salud menstrual y se darán a conocer las opciones disponibles para que cada persona menstruante pueda gestionar el período con el producto que desee utilizar.

La inscripción en el registro habilita a la entrega mensual de un paquete de toallitas absorbentes descartables por persona y está disponible para anotarse en los jardines municipales, en las Estaciones municipales y en los Centros de Distrito. En el mismo se debe dejar asentado nombre, apellido, DNI, domicilio y teléfono celular.    

La secretaria de Integración y Economía Social del municipio, Ayelén Dutruel, aclaró que esta política social se aplica desde hace unos meses, pero la intención es ir más allá y “empezar a hablar de estos temas a través de las rondas de mujeres y personas menstruantes y pensar que la menstruación no es un tema tabú y que el acceso a estos productos no es una elección sino una necesidad”.

La funcionaria contó que la Municipalidad tiene 40 puntos de relevamiento y de inscripción en la ciudad; y, hasta el momento, alcanza a unas 2.000 personas. “El registro se hace en todas las dependencias del municipio como Jardines, Estaciones y Distritos y luego -también en estos espacios- se hace la entrega cada mes”, recordó Dutruel y consideró que esto tiene que ver con “institucionalizar una política pública que surge a partir de una ordenanza”.

**Mayor igualdad**

Esta política pública y universal tiene su anclaje en la necesidad de garantizar el acceso a los productos de higiene menstrual, de histórico elevado costo. Florencia Costa, subdirectora de Ampliación de Derechos, puntualizó que “son muy caros porque están gravados con el mismo IVA con que se gravan los cosméticos, cuando en realidad se tratan de productos esenciales, de necesidad básica. No es un gasto que una persona pueda decidir hacerlo o no. Y debido a esto muchas personas menstruantes abandonan sus rutinas escolares o laborales por no poder acceder a estos productos”, afirmó.

En ese contexto, la funcionaria apuntó que “este tipo de políticas públicas reducen la brecha de desigualdad entre quienes pueden acceder a los productos y quienes no. Además, creemos que la menstruación no es un ‘tema de mujeres’ sino un tema de Estado y por eso es importante acompañar con políticas públicas que garanticen la igualdad de oportunidades para todes”.

Por su parte, la concejala Laura Mondino, impulsora de esta iniciativa que sancionó el Concejo, manifestó: “La ordenanza la aprobamos el año pasado pero se empezó a trabajar en 2018 e hicimos un recorrido bastante intenso porque es uno de los temas que cuesta instalar y poder reflejar la importancia que tiene para muchas personas menstruantes. Fue sinónimo de burlas y risas en el Concejo pero lo logramos”.

“Esto genera una desigualdad entre los hombres y las mujeres porque tenemos que asumir un gasto mensual que los varones no lo tienen y, de ahí, esta idea. Poder trabajar con la Municipalidad en el otorgamiento de las toallitas va a alivianar esa situación y por eso estamos contentas con que se continúe con este programa y que lleguemos a más lugares”, manifestó más adelante la concejala.

En la entrega de este lunes, que se realizó en la Estación Facundo Zuviría, también estuvo la secretaria de Políticas de Cuidado y Accesión Social, Victoria Rey; y la concejala Mercedes Benedetti.

**Plan integral**

El programa es impulsado por las direcciones municipales de Salud y Promoción Comunitaria y de Mujeres y Disidencias. En principio, propone la entrega mensual de un paquete por persona de toallitas absorbentes descartables. Sin embargo, esta iniciativa también apunta a incorporar elementos más amigables con el medio ambiente, como la copa menstrual y los dispositivos descartables.

A esta política pública que tiene su marco normativo en la Ordenanza N° 12.713 se sumará en un futuro cercano la realización de encuentros de socialización sostenidos en el tiempo con las personas inscritas al registro para abordar las diferentes opciones de gestión menstrual, a los fines de educar sobre las corporalidades menstruantes, contemplando la diversidad cultural como la necesidad de conocer el cuerpo propio. A través de estos talleres, el Estado municipal buscará visibilizar y concientizar sobre opciones sostenibles y respetuosas del medioambiente de productos reutilizables para la gestión menstrual.