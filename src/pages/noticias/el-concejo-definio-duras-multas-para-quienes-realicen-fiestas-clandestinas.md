---
category: La Ciudad
date: 2021-05-07T08:30:39-03:00
thumbnail: https://assets.3dnoticias.com.ar/concejo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Concejo de Santa Fe
resumen: El Concejo definió duras multas para quienes realicen fiestas clandestinas
title: El Concejo definió duras multas para quienes realicen fiestas clandestinas
entradilla: La multa podrá variar entre quince mil a veinticinco mil Unidades Fijas
  (UF). Lo recaudado será destinado a políticas sanitarias relacionadas a la emergencia
  sanitaria.

---
El Concejo Municipal aprobó, mediante ordenanza, las multas a quienes realicen fiestas clandestinas en el ámbito de la ciudad de Santa Fe mientras dure la situación de emergencia por la pandemia. Serán consideradas “clandestinas” aquellas reuniones de personas que se realicen tanto al aire libre o en espacios cerrados y sean contrarias a las disposiciones vigentes.

Serán sancionados tanto los propietarios, poseedores y/o tenedores del inmueble en el que se realice, así como las personas responsables de la organización. Los montos varían entre quince mil Unidades Fijas (UF) a veinticinco mil. Actualmente, el valor de la UF es de $45.78, por lo que las multas oscilarán entre los $686.700 a $1.144.500.

Además, se decomisarán los elementos necesarios para la realización del evento. En caso de que se trate de un inmueble, piezas o infraestructuras pertenecientes a un comercio habilitado, se lo inhabilitará por hasta 180 días.

Se establece, también, la posibilidad de que el juez de faltas aplique como penas accesorias, la realización de cincuenta a cien horas de trabajos no remunerados de solidaridad con la comunidad. El dinero que se recaude por las multas será destinado a políticas sanitarias relacionadas a la emergencia sanitaria.

Luciana Ceresola, una de las autoras del proyecto, afirmó que “en los últimos meses hubo una gran proliferación de fiestas clandestinas, vemos que muchas personas no se hacen eco de la emergencia sanitaria. Si bien estas sanciones fuertes no resuelven el problema de raíz, sí van a desalentar este tipo de acciones. Una fiesta clandestina no es una picardía, es un delito para la salud de todos”.

Carlos Suárez también fue autor de la iniciativa y expresó que “el espíritu de esta ordenanza no es castigar, sino antes que nada desalentar. Queremos apelar a la responsabilidad individual, ojalá las multas no tengan que aplicarse. De este modo esperamos ayudar al Estado Municipal evitando la generación de este tipo de eventos”.

El presidente del Concejo, Leandro González, afirmó que “debemos dar señales claras y contundentes respecto al rol del Estado y llamar a los vecinos y vecinas a ser solidarios. Sabemos que la concientización es central y que hoy más que nunca debemos hacer cumplir las medidas sanitarias y las recomendaciones. Hoy incumplir con las normas no es solo poner en jaque al sistema sino también al personal esencial. Que se hagan fiestas clandestinas es contraproducente para el personal de salud, pero también ataca la posibilidad que escuelas y clubes sigan abiertos con presencialidad”.

Federico Fulini resaltó que “Hay diversas situaciones que uno tiene que penalizar. En este caso, entendiendo el compromiso epidemiológico que genera que muchas personas estén encerradas sin ningún tipo de medidas sanitarias y que eso se transforme posteriormente en una serie de contagios, es que me parece sumamente necesario el incremento de las sanciones”.