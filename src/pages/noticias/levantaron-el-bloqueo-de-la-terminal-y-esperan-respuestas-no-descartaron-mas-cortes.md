---
category: Agenda Ciudadana
date: 2021-04-15T07:41:49-03:00
thumbnail: https://assets.3dnoticias.com.ar/paro.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Diario Uno
resumen: 'Levantaron el bloqueo de la Terminal y esperan respuestas: no descartaron
  más cortes'
title: 'Levantaron el bloqueo de la Terminal y esperan respuestas: no descartaron
  más cortes'
entradilla: Luego de tres días de protesta, transportistas de turismo autoconvocados
  levantaron los cortes y bloqueo de la Terminal de Ómnibus. No descartan volver a
  las calles el lunes si no obtienen respuestas.

---
Luego de 50 horas ininterrumpidas de protestas con el bloqueo de la Terminal de Ómnibus Manuel Belgrano y los cortes de diversas calles y avenidas, los transportistas de turismo autoconvocados decidieron finalmente, cerca de las 15 de este miércoles, levantar la medida de fuerza.

Las medidas de fuerza que se realizaron a nivel nacional, fueron en repudio a las restricciones impuestas por el gobierno nacional, en el marco de la pandemia de coronavirus. En dicha protesta, que en la ciudad de Santa Fe generó cortes de tránsito de forma total en avenida Alem y en los accesos al micro y macrocentro, los dueños de micros reclamaron a Nación (la CNRT los regula) un trato igualitario con las líneas regulares, que en la actualidad siguen prestando el servicio.

Vale recordar que este martes se realizó una audiencia clave en la ciudad de Buenos Aires y a pesar que en ese lugar la protesta se levantó, al igual que este miércoles en Santa Fe y Rosario, el reclamo sigue. “La reunión de este martes nos defraudó, nos desilusionó; los que dicen representarnos no lograron novedades positivas y no hubo ningún acuerdo”, señaló el vocero de los manifestantes en la capital de la provincia, Julio Ríos.

Al mismo tiempo del cónclave nacional, autoridades de la provincia de Santa Fe dialogaron este miércoles con los manifestantes que realizaron el bloqueo a la Terminal durante más de 50 horas. Tras el encuentro, los transportistas autoconvocados decidieron levantar el corte y esperar por respuestas.

El secretario de Turismo de Santa Fe, Alejandro Grandinetti, junto al secretario de Transporte provincial, Osvaldo Miatello, recibieron a representantes locales del sector en protesta y les ofrecieron "oficiar de mediadores con los organismos nacionales con el objetivo de encontrar una solución al conflicto", aseguraron referentes del rubro a UNO Santa Fe.

Por su parte, los transportistas de turismo independientes (más de 200 empresas en el territorio provincial), le exigieron al gobierno provincial quedar eximidos del pago de impuestos y recibir una ayuda para pagar los sueldos de los empleados tras más de 9 meses sin trabajar.

Así la situación, los manifestantes levantaron los cortes de calles y el bloqueo de la Terminal de Ómnibus en Santa Fe y Rosario a la espera de respuestas a sus pedidos. "Desde Provincia nos pidieron que cedamos las medidas de fuerza para luego volver a conversar. Vamos a darle un tiempo límite de 72 horas para ver que solución nos acercan. Si el fin de semana no tenemos novedades, el lunes a primera hora volveremos a las calles con las protestas", aseguraron los manifestantes a UNO Santa Fe.