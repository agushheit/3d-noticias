---
category: La Ciudad
date: 2021-01-15T04:04:23Z
thumbnail: https://assets.3dnoticias.com.ar/stafeycaba.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: '3DNoticias - Fuente: El Litoral'
resumen: Santa Fe y CABA afianzan lazos para potenciar el turismo en sus distritos
title: Santa Fe y CABA afianzan lazos para potenciar el turismo en sus distritos
entradilla: 'Representantes del municipio local, junto a sus pares de la Ciudad Autónoma
  de Buenos Aires firmaron un acuerdo para, entre otras cosas, promocionar y difundir
  el turismo de ambas localidades. '

---
Este jueves, en virtud de los intereses mutuos que unen al Ente de Turismo de la Ciudad Autónoma de Buenos Aires (CABA) y la Municipalidad de Santa Fe, ambas partes decidieron firmar un Convenio de Colaboración con el objetivo de afianzar los lazos de cooperación mutua en materia turística.

La intención es implementar políticas conjuntas con el fin de promocionar y difundir los atractivos turísticos de ambos destinos y formular un plan de trabajo para la instrumentación y ejecución de dichas políticas.

Diego Gutiérrez, Director General de Promoción Turística de CABA, visitó El Litoral junto a Ignacio Coronel (Jefe de Gabinete de la Dirección de Promoción Turística) antes de rubricar la firma.

«Desde que comenzó el 2020 tenemos una fuerte convicción y estrategia para llevar adelante la promoción del turismo nacional en la ciudad de Buenos Aires. Nuestra política está en empezar a hablarle al país de que Buenos Aires es un destino de turismo. En los últimos años consolidamos el turismo internacional como estrategia, pero a partir de 2020, con el cambio de gestión, la política claramente fue la de posicionar a Buenos Aires como un destino turístico nacional», afirmó Gutiérrez.

Hacer alianzas con grandes ciudades permitiría empezar a volcar y dinamizar el concepto de que Buenos Aires es una ciudad turística para visitar en todo el año.

«En esta situación pandémica que nos tocó, empezamos a trabajar mucho con distintos destinos: relacionarnos, conocer las políticas que llevan adelante y cómo nosotros podíamos sumarnos. Tenemos que ser complementarios, no competidores. Ahí fue que empezamos a trabajar con la ciudad de Santa Fe, y pensar cómo podíamos nutrirnos. A partir de esa relación que empezamos a tener, surgió una transferencia de gestión por un modelo de audioguías que están llevando hace unos meses con mucho éxito, y aprendimos a llevar adelante esa propuesta. Este convenio es el puntapié para empezar, mutuamente, a transferir conocimiento», aseguró.

A su turno, Coronel dijo: «queremos aprovechar las plazas que están cerca, como Rosario, que tiene mucho turismo de reuniones. Trabajar juntos y apoyar la planificación es clave. En esta promoción conjunta, es como visualizar la oferta de cada ciudad. En Buenos Aires, estamos desarrollando ahora el circuito turístico a cielo abierto. Propone una recorrida gastronómica, cultural, etc., por barrios. Son unos 4 kilómetros en total por paseo, que empezamos a implementar en esta época de pandemia».

<br/>

### **TURISMO DE ESCAPADAS**

«Lo que le pasa a Santa Fe es algo muy similar a lo que ocurre en la ciudad de Buenos Aires. El índice de estadía promedio es de dos noches, tres máximos. Entonces estudiamos el 'turismo de escapadas'. Y comenzamos a desarrollarlo generando diferentes ofertas, para que la gente pueda venir a Buenos Aires tres o cuatro veces al año, haciendo distintas visitas", explicó Diego Gutiérrez quien agregó que "el mismo modelo llevará adelante Santa Fe. Hay porteños que tienen que conocer cuáles son los atributos turísticos de Santa Fe y viceversa. Nosotros seremos un puente de la estrategia de promoción que genere Santa Fe».

<br/>

### **CONVENIO DE COLABORACIÓN**

Se llevará a cabo a través del desarrollo de las siguientes acciones específicas:

1\. Organizar en conjunto eventos de prensa, en donde se presentarán distintos temas relacionados con la promoción turística de la Ciudad Autónoma de Buenos Aires y la Ciudad de Santa Fe.

2\. Replicar la difusión en soportes web y redes sociales.

3\. Coordinar acciones en conjunto con medios de prensa para la difusión del programa de promoción turística.

4\. Generar contactos en conjunto con empresas de transporte, hoteles y prestadores de servicios turísticos, para incentivar beneficios en el marco de la acción que se propone.

5\. Producir contenidos comunicacionales para su difusión en vía pública, medios gráficos y/o audiovisuales. Esta Carta de Intención entrará en vigencia a partir de la fecha de su firma, por un período de DOS (2) años, o hasta la firma del nombrado Convenio de Colaboración.