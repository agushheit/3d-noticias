---
category: Agenda Ciudadana
date: 2021-02-19T06:20:26-03:00
thumbnail: https://assets.3dnoticias.com.ar/santoto.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad de Santo Tomé
resumen: Se prorroga el Plan de Regularización de Deudas de la Municipalidad de Santo
  Tomé
title: Se prorroga el Plan de Regularización de Deudas de la Municipalidad de Santo
  Tomé
entradilla: Se dispuso prorrogar hasta el próximo 14 de mayo. Abarca a los planes
  para todos los derechos, tasas y contribuciones locales.

---
La Municipalidad de Santo Tomé dispuso prorrogar hasta el próximo 14 de mayo el Plan de Regularización de Deudas para todos los derechos, tasas y contribuciones locales.

Cabe recordar que la moratoria abarca las deudas vencidas al 30 de septiembre de 2020, e incluye alternativas de pago de hasta 36 cuotas y quitas de intereses de hasta el 70%.

<br/>

## **SOLICITUD DE TURNOS**

Para aprovechar esta oportunidad, los interesados pueden solicitar un turno a través de la página web [www.santotome.gob.ar](https://www.santotome.gob.ar/). De esta forma, podrán acercarse a la Municipalidad y optar por el plan de pago más conveniente de acuerdo a sus necesidades.

Exclusivamente para el Derecho de Registro e Inspección (DREI), los turnos se deben solicitar telefónicamente al 4752599, o bien por WhatsApp a la línea 342 4065986, de lunes a viernes de 7.00 a 13.00 horas.

<br/>

## **TRIBUTOS INCLUIDOS**

\- Tasa General de Inmuebles (TGI)

\- Tasa de Urbanizaciones Privadas (TUP)

\- Tasa de Inmuebles Rurales (TIR)

\- Contribución por Mejoras

\- Tasa Retributiva Servicios Sanitarios (TRSS)

\- Derechos de Cementerio

\- Derecho de Registro, Inspección e Higiene (DReI)

\- Derechos de Edificación

<br/>

## **OPCIONES DE PAGO**

**1- Para la Tasa General de Inmuebles, Tasa de Urbanizaciones Privadas, Tasa de Inmuebles Rurales, Tasa Retributiva de Servicios Sanitarios, Contribución por Mejoras y Cementerio:**

* Contado, con quita del 70% de intereses.
* Hasta 12 cuotas, con quita del 30% de intereses por mora y financiación del 1,5 % mensual.
* De 13 a 24 cuotas, con quita del 15% de intereses por mora y financiación del 2,0% mensual
* De 24 a 36 cuotas, sin quita y con financiación del 2,0% mensual.

**2- Para el Derecho de Registro e Inspección:**

Con las mismas modalidades de pago detalladas en el punto anterior.

Además, se establecen facilidades para abonar la deuda en concepto de multas, con reducciones de hasta el 30% y financiación en hasta 36 cuotas, dependiendo del convenio elegido.

**3- Para el Derecho de Edificación:**

Con reducción de alícuotas de hasta el 30% y financiación en hasta 36 cuotas para la regularización de planos, dependiendo del convenio elegido.

<br/>

## **FORMAS DE PAGO**

Los convenios podrán abonarse de la siguiente manera:

* En efectivo
* Con tarjeta de crédito en hasta 6 cuotas (con beneficios de contado)
* Cuotas en efectivo y medios electrónicos de pago (incluyendo Link y Banelco).
* Débito automático con tarjeta de crédito, con reducción de la tasa de financiación a la mitad.