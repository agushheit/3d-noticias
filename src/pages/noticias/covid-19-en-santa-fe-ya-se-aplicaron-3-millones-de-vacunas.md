---
category: Agenda Ciudadana
date: 2021-08-12T06:15:36-03:00
thumbnail: https://assets.3dnoticias.com.ar/VACUNACION.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Covid-19: en Santa Fe ya se aplicaron 3 millones de vacunas'
title: 'Covid-19: en Santa Fe ya se aplicaron 3 millones de vacunas'
entradilla: Se completaron los esquemas de vacunas en el 100 por ciento del sector
  salud, docencia, seguridad y adultos mayores institucionalizados en geriátricos.

---
La provincia de Santa Fe, a través del Ministerio de Salud, informó que este miércoles 11 de agosto se alcanzó la colocación de los 3 millones de vacunas contra el Covid-19. De este modo, se completaron los esquemas (con dos dosis) en el 100 por ciento del personal de salud, docente, de seguridad y adultos mayores institucionalizados en geriátricos. Mientras que todos los mayores de 60 años y la población de 59 a 18 años con factores de riesgo; ya recibieron, al menos, una dosis de la vacuna contra el coronavirus.

“Es muy gratificante para nosotros poder anunciar que hemos alcanzado los 3 millones de dosis colocadas, no solo en la población objetivo priorizada sino también avanzando en mayores de 18 años sin comorbilidades”, expresó el secretario de Salud, Jorge Prieto.

Del mismo modo, continuó: “La inmunización contra el Covid-19 creció a pasos agigantados, incluso, más de 120 localidades ya completaron la vacunación con 1 dosis de todos los inscriptos en el Registro Provincial de Vacunación”.

“Esto fue posible –destacó el funcionario– gracias al enorme compromiso y al trabajo incansable de los equipos de salud; y al arribo constante de más vacunas de diversos laboratorios, fundamentalmente a partir de los meses de julio y agosto en donde priorizamos completar esquemas, muchos de ellos mediante la combinación de componentes de nuevos laboratorios de excelente seguridad y efectividad”.

“Asimismo, estos nuevos y constantes arribos –agregó Prieto– nos han permitido aplicar primeras dosis apenas llegaron a la provincia, en toda la población inscripta mayor de 18 años sin comorbilidades; que inicialmente no estaba en la población objetivo determinada por Salud de la Nación y en consenso con organismos internacionales y las 24 jurisdicciones sanitarias del país”.

Para cerrar, Prieto destacó la importancia del histórico operativo de vacunación y cerró: “Es muy gratificante notificar este número, hay más de 800 mil santafesinos que ya completaron el esquema de vacunación”.