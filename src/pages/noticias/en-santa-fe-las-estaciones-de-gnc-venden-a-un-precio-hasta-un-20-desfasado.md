---
category: La Ciudad
date: 2021-10-24T06:00:43-03:00
thumbnail: https://assets.3dnoticias.com.ar/gnc.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: En Santa Fe, las estaciones de GNC venden a un precio hasta un 20% desfasado
title: En Santa Fe, las estaciones de GNC venden a un precio hasta un 20% desfasado
entradilla: Los estacioneros trabajan a pérdida en un negocio con "rentabilidad nula".
  El precio del metro cúbico ronda los $45 y las estaciones no pueden actualizarlo
  por temor a quedar afuera del negocio.

---
La actualidad del negocio relacionado al expendio de GNC en las estaciones santafesinas dista mucho de ser la ideal. Con una "rentabilidad nula", los estacioneros marcan que "debería aumentar" el precio del metro cúbico al menos en un 5 o 6% para que los números cierren, aunque el problema en el rubro radica en una decisión de negocio.

Actualmente el costo del metro cúbico que oscila entre los 42 y 47 pesos en Santa Fe no es el "precio real" según los estacioneros, sino que es un precio al que todos los actores del rubro están obligados a mantenerse si no quieren desaparecer del mercado. Obligados a vender a un precio inferior al real se impone el llamado "dumping", en el que un competidor trabaja con precios inferiores al costo con el fin de condicionar a la competencia.

Dalmiro Saux, vocal de la Cámara de Expendedores de Combustibles y Afines de Santa Fe (Cecasf) no pudo asegurar que se plasmará una suba en el precio de venta del GNC a corto plazo, Saux afirmó que "debería aumentar, pero si no hay una posición unánime de aumento es muy difícil que se concrete, la competencia no da margen para hacerlo".

"Hoy la rentabilidad es nula, ahora el precio está un 5% abajo de lo que debería estar", afirmó el representante de los estacioneros de GNC. Si bien los precios en las estaciones de Santa Fe son muy variables, la realidad es que el piso del costo del metro cúbico es de $42,90 y el techo llega a $47, manteniéndose todos los establecimientos en esa franja de precios. Para graficar el desfasaje cabe destacar que en Paraná, Rafaela y San Francisco todas las estaciones están vendiendo el metro cúbico por encima de los $50.

La falta de regulación que existe con respecto a la fijación de un precio de venta determinado del metro cúbico de GNC provoca que cada estacionero de Gas para uso combustible ponga el precio que considere justo. Si bien desde la Federación Argentina de Expendedores de Nafta del Interior (Faeni), se impone que se debe comercializar GNC a la mitad de precio que el costo del litro de nafta Súper en la YPF de esa ciudad, la realidad es que este valor varía de una región a la otra. Esto genera disparidades en el precio de venta del GNC comercializado en una ciudad o en otra.

Consultado por el rol de los competidores en la formación del precio, Saux destacó: "Competencia hay en todas partes. El problema nace cuando un colega se planta en un precio, con lo que es difícil moverlo cuando uno lo tira para abajo. Esto provoca una especie de «dumping». En este momento hay una estación que hace mucho tiempo adoptó la política de poner un precio muy bajo y ancla a todos porque estamos muy cercanos unos de otros en cuestión de precios".

Frente al interrogante por un posible cierre de estaciones, el vocal de Cecasf mencionó: "No imagino un escenario de cierres de estaciones, al menos por ahora, pero sí de que provoca que todos los estacioneros tengamos dificultades financieras. Todos estamos con problemas con los bancos, con demoras en los pagos de facturas y de sueldos. El precio no es rentable para ningún estacionero de Santa Fe".

"No se puede subir el precio al valor real si una estación no lo hace. Somos muchos pero tampoco somos tantos, entonces se podría plantear al revés la situación: que uno levante el precio y que el resto acompañe, aunque esto sería adoptar un riesgo. Con estos precios y estas ventas es una situación mala, pero si se pudiera dar este aumento de un 5 o 6% deberíamos tener un precio rentable y más razonable", continuó.

**Ventas "amesetadas"**

Con este panorama y a pesar de que la crisis económica plantea como una opción económica la de optar por la instalación de un equipo de GNC para el uso del vehículo, los estacioneros afirmaron que "las ventas están amesetadas". Sobre esto, Saux subrayó: "Durante la cuarentena la cosa fue patética, después fue levantando poco a poco con el correr del tiempo pero hace un par de meses que se estancó y permanece muy estabilizado. Es difícil hacer una proyección".

"En realidad el sistema debería funcionar bien en una época de crisis y de “vacas flacas” de la gente, porque hay un combustible que vale la mitad que la nafta Súper. Actualmente la Súper vale $100 y el GNC alrededor de $45, las diferencias son muy grandes. Los equipos técnicamente son buenos y funcionan bien, pero la única verdad es la realidad y ahora no está aumentando la cantidad de conversión de autos a GNC", concluyó.