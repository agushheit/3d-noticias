---
category: La Ciudad
date: 2021-03-11T06:22:33-03:00
thumbnail: https://assets.3dnoticias.com.ar/liceo-norte.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad de Santa Fe
resumen: La Municipalidad licitó obras de alumbrado para Liceo Norte
title: La Municipalidad licitó obras de alumbrado para Liceo Norte
entradilla: Las obras contemplan la instalación de 61 columnas en distintos puntos
  del barrio. Dos empresas presentaron ofertas.

---
En el marco del Plan de Iluminación que el municipio lleva adelante, este miércoles se licitaron trabajos para instalar 61 columnas en barrio Liceo Norte, con un presupuesto oficial de $ 7.500.000.

Dos empresas presentaron ofertas, ambas por debajo del presupuesto: Bauza, por  6.643.700,57 y Tacuar, por $ 6.129.008.

Según se informó, las columnas se radicarán a lo largo de pasaje Larguía y de las calles José Cibils, Servando Bayo, Larguía, Chiclana y Almonasid.

Cabe recordar que el Plan de Iluminación puesto en marcha por el municipio incluye la recuperación del alumbrado público existente, la actualización tecnológica de las luminarias y la colocación de un nuevo sistema, mediante columnas con equipos LED en varios barrios de la ciudad.

![](https://assets.3dnoticias.com.ar/iluminación.jpg)