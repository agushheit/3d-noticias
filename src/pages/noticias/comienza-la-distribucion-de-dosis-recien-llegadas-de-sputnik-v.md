---
category: Agenda Ciudadana
date: 2021-03-21T06:00:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/VOZZOTTI.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Comienza la distribución de dosis recién llegadas de Sputnik V
title: Comienza la distribución de dosis recién llegadas de Sputnik V
entradilla: Así lo confirmó la ministra de Salud de la Nación, Carla Vizzotti, y aseguró
  que las vacunas llegarán el lunes a todas las jurisdicciones.

---
La ministra de Salud, Carla Vizzotti, confirmó que en la madrugada de este domingo comenzarán a distribuirse en todo el país 337.800 dosis del componente 1 de la vacuna Sputnik V de la partida que llegó el viernes desde Moscú, y sostuvo que serán utilizadas para fortalecer la inmunización de personas mayores de 70 años.

De ese modo, las vacunas, luego del procedimiento de desaduanaje, recepción, control térmico, conteo, fraccionamiento y acondicionamiento, llegarán entre mañana domingo y el lunes a todas las jurisdicciones, informó la Presidencia en un comunicado.

Según el criterio dispuesto por el Ministerio de Salud, en base a la cantidad de población de cada distrito y a la unidad mínima de embalaje, que es de 600 dosis por conservadora, la partida de Sputnik V se asignó del siguiente modo: 127.800 dosis a la provincia de Buenos Aires y 22.800 a la Ciudad Autónoma de Buenos Aires.

Las otras dosis se distribuirán 3.000 a Catamarca, 9.000 a Chaco, 4.800 a Chubut, 27.600 a Córdoba, 8.400 a Corrientes, 10.200 a Entre Ríos, 4.800 a Formosa, 6.000 a Jujuy, 3.000 a La Pampa, 3.000 a La Rioja, 15.000 a Mendoza, 9.600 a Misiones, y 5.400 a Neuquén.

Además, se distribuirán 6.000 a Río Negro, 10.800 a Salta, 6.000 a San Juan, 4.200 a San Luis, 3.000 a Santa Cruz, 25.800 a Santa Fe, 7.200 a Santiago del Estero, 1.800 a Tierra del Fuego y 12.600 a Tucumán.