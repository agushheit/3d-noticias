---
category: Agenda Ciudadana
date: 2021-02-17T08:09:24-03:00
thumbnail: https://assets.3dnoticias.com.ar/trotta.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: JPE
resumen: Antes del regreso a clases, Nicolás Trotta habló de un “proceso de reorganización
  pedagógica”
title: Antes del regreso a clases, Nicolás Trotta habló de un “proceso de reorganización
  pedagógica”
entradilla: El ministro adelantó que a fin de año los alumnos deberán acreditar conocimientos
  para avanzar a la próxima unidad en 2022.

---
A horas del regreso a las clases presenciales en cuatro jurisdicciones, entre ellos el 47% de los alumnos de la Ciudad de Buenos Aires, el ministro de Educación de la Nación, Nicolás Trotta, habló de un "proceso de reorganización pedagógica" y adelantó que los estudiantes a fin de año deberán acreditar conocimientos para avanzar.

"Vamos a transitar aulas mucho más heterogéneas porque en el hogar se consolidan las desigualdades. Hemos acordado un proceso de reorganización pedagógica", sostuvo Trotta en referencia a los contenidos que los alumnos perdieron debido a que las escuelas estuvieron cerradas en 2020 por la pandemia de coronavirus.

En declaraciones al canal TN, el funcionario adelantó, además, que "terminado el 2021 los estudiantes van a tener que acreditar los conocimientos para avanzar a la próxima unidad pedagógica".

Insistió Trotta en la necesidad de que la presencialidad sea "cuidada" y advirtió que "si hay un contagio lo que se aísla es la burbuja (los alumnos de ese curso) y todos los contactos que ha tenido si el que se contagia es el docente".

Y sumó: "Lo que hemos acordado todos de manera unánime el viernes es que cada decisión que se adopte en cuanto a intensificar la presencialidad, como llegado un caso extremo de una situación epidemiológica compleja, hay que adoptarlo en la mínima unidad geográfica. No se puede tomar a la Argentina como un todo".

En la Ciudad, casi la mitad de los estudiantes volverán a las aulas. Será en todas las escuelas públicas y privadas del distrito. El resto de los 785.152 alumnos volverá de manera escalonada.

Arrancarán los alumnos de nivel inicial (45 días a 5 años, incluyendo a los que van a la modalidad de educación especial); de 1°, 2° y 3° grado de primaria y del especial; de 1° y 2° año de secundaria; y de 1° y 2° año de escuelas técnicas. En total, según cifras del Ministerio de Educación porteño, serán 370.493 estudiantes.

En una segunda etapa, a partir del 22 de febrero, le tocará el turno al resto de los alumnos de la primaria porteña (4° a 7° grado) y el segundo ciclo de educación especial. Mientras que el 1° de marzo regresarán los años restantes del secundario y los alumnos que aún estén bajo sistema virtual de la modalidad especial.

**La paritaria nacional docente**

Trotta también se refirió al inicio de la paritaria nacional docente, cuya primera reunión se realizará el jueves, y adelantó que confía en llegar a un acuerdo con los gremios.

"Vamos a discutir, debatir y escuchar las posiciones de los cinco sindicatos nacionales. Creemos que vamos a llegar a un acuerdo", dijo Trotta.

Y agregó: "Somos el Gobierno que reinstitucionalizó la paritaria nacional docente. Firmamos cuatro paritiarias con los maestros".

Al ser consultado sobre la posibilidad de que no se llegue a un acuerdo y que se pueda suspender el inicio de clases, Trotta fue terminante: "No va a haber corte en la discusión porque vamos a llegar a un acuerdo".

Explicó también que la paritaria nacional tiene el sentido de "encauzar los diálogos" con los docentes de las distintas jurisdicciones y, en ese marco, asumió que "algunas provincias ya alcanzaron acuerdos salariales" en tanto que otras aún no lo lograron.