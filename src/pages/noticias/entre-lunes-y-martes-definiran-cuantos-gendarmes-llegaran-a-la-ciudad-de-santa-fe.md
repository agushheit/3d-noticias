---
category: La Ciudad
date: 2021-10-15T06:15:50-03:00
thumbnail: https://assets.3dnoticias.com.ar/GENDARMES2.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Entre lunes y martes definirán cuántos gendarmes llegarán a la ciudad de
  Santa Fe
title: Entre lunes y martes definirán cuántos gendarmes llegarán a la ciudad de Santa
  Fe
entradilla: Fuentes oficiales confirmaron que habrá una reunión operativa entre municipio,
  provincia y fuerzas federales. Este jueves, llegaron 575 efectivos a Rosario.

---
La semana que viene se definiría el arribo de gendarmería a la ciudad de Santa Fe. Fuentes oficiales adelantaron que entre lunes y martes se realizará una reunión de la mesa operativa entre fuerzas federales, Ministerio de Seguridad de la provincia y municipalidad de Santa Fe.

Según pudo saber este medio, el cónclave servirá para determinar cómo desembarcarán los efectivos en esta capital. En ese sentido, se aguardan detalles vinculados a la cantidad de agentes que llegarán y definiciones relacionadas a la coordinación de tareas en la lucha contra el delito.

Este jueves, el gobernador Omar Perotti, el ministro de Seguridad de la Nación, Aníbal Fernández y el director nacional de Gendarmería Nacional Argentina, Andrés Severino, encabezaron el acto formal de bienvenida de los 575 efectivos de esa fuerza a la ciudad de Rosario.

El nuevo esquema de reorganización de las fuerzas federales prevé también la creación de una nueva Unidad Móvil de Gendarmería Nacional en Rosario, dedicada a la lucha contra el narcotráfico y el delito complejo, que se integrará con 1.000 agentes más que desembarcarán escalonadamente hasta el mes de marzo próximo.

Paralelamente a la llegada de oficiales al sur provincial, concejales de esta ciudad solicitaron que el arribo de oficiales sea "en la misma proporción, conforme a la población, de los que son destinados a la ciudad de Rosario".

Luego de la presentación, el gobernador subrayó en el acto que “a la ayuda la pedimos y la hacemos explícita; porque por sí sola con nuestra institución policial no se puede enfrentar la magnitud del desarrollo de estas organizaciones criminales”.

Señaló que “la policía de la provincia de Santa Fe entró en una etapa de cambios profundos que no tiene vuelta atrás. Hasta aquí, ha habido jefes que se han retirado y parecían condecorados cuando la institución policial estaba cada día más degradada y la gente con más desconfianza. Hoy, muchos de ellos están condenados y presos; es una clara señal de que no hay impunidad”.

En esa línea, Perotti remarcó que “no hay vuelta atrás; hay una transición necesaria en la formación, en el equipamiento, en la capacitación y en la incorporación de nuevos agentes: 1.600 este año y 2.500 el año próximo”.

Y destacó la importancia del trabajo conjunto con el Gobierno Nacional: “Muchas veces se quiso ocultar, disimular o disminuir la magnitud de la situación que existe en el territorio, o pensar que era un menoscabo pedir ayuda nacional. Aquí no era una cuestión de capacidad; esas bandas barriales se fueron convirtiendo en organizaciones criminales y claramente superaron la capacidad de respuesta de la institución policial”.

Finalmente, remarcó la importancia de contar con “presencia activa, permanente, hasta que esto se neutralice, para evitar cualquier instancia de contagio a otros lugares del país. Por lo cual, no hay plazos; hay búsqueda de resultados y los resultados tienen que hablar fundamentalmente con los hechos”.

Por su parte, el ministro de Seguridad de la Nación, Aníbal Fernández, refiriéndose a los 575 efectivos de Gendarmería que llegaron a la ciudad, explicó: “No vienen y se van; vienen a quedarse. Por eso creamos esta región, para trabajar en todo lo que está generando complicaciones en la ciudad y que nosotros tenemos que ponerle fin con la ayuda de otros poderes”.

En otro tramo de su discurso, Fernández expresó que “hay un trabajo de inteligencia criminal llevado a cabo por la provincia que vamos a utilizar y que está profundizando el personal de Gendarmería en término de análisis de la situación específica, para ponerle coto a esa situación”.

Advirtió sobre la necesidad de frenar el avance del narcotráfico antes de que se traslade a otros lugares del país. “No es un tema que le competa únicamente al gobernador y al intendente; es un problema que se va a convertir en un problema de los argentinos si no tomamos a las cosas como corresponden”, indicó.