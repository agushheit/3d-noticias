---
category: Estado Real
date: 2021-08-03T06:00:59-03:00
thumbnail: https://assets.3dnoticias.com.ar/INFANCIAS.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La provincia lanzó el mes de las infancias en los espacios culturales de
  la ciudad capital
title: La provincia lanzó el mes de las infancias en los espacios culturales de la
  ciudad capital
entradilla: Inició el fin de semana con ferias, muestra de talleres, proyecciones,
  espectáculos y espacios de juego para celebrar a los niños y niñas.

---
Los espacios que el Ministerio de Cultura de la provincia gestiona en la ciudad de Santa Fe iniciaron el fin de semana pasado la celebración del Mes de las Infancias, con actividades para compartir en familia y con amigos.

La agenda comenzó el viernes con una tarde de sol a pleno en El Alero de barrio Acería, donde se realizó una muestra con los trabajos de las y los alumnos que fueron parte del taller de artes plásticas en la primera mitad del año.

Hasta allí pudieron acercarse los vecinos y vecinas para ver el resultado de meses de exploración artística y de la imaginación puesta al servicio de la creación. Esta muestra fue acompañada por proyecciones de cine, música en vivo y juegos que se pudieron disfrutar al sol, en el patio del espacio.

El sábado, en tanto, volvió la Feria Florecer que ya es un clásico en El Alero de Las Flores, donde las y los emprendedores y artesanos del barrio ofrecen sus creaciones.

La feria ya es una cita obligada de los sábados en Las Flores, donde vecinas y vecinos adquieren productos locales a precios populares y donde disfrutan, además, de las diversas propuestas culturales. En esta ocasión también hubo lecturas al aire libre y proyecciones de cortos de creadores santafesinos.

También El Molino, Fábrica Cultural abrió sus puertas y los tres pisos del edificio del ala este para jugar. Con la reserva previa de entradas para garantizar un cupo limitado de asistentes y respetar así los protocolos sanitarios, unas 500 personas pasaron por el espacio el sábado y el domingo.

Grupos familiares, de amigos y amigas pudieron recorrer el Universo textil, el Movimiento madera y la Experiencia papel, desplazándose en burbujas sociales y con uso obligatorio de tapabocas.

En cada piso las propuestas ponen el eje en cada una de las materialidades (papel, madera y textil) y allí las y los visitante pudieron experimentar en los laboratorios, con espacios de juego, ensayo y experimentación, observación y reflexión, con los cuales se interesan en el estudio de los colores, texturas, olores, luces y pigmentos para combinar con las materialidades de cada piso.

El domingo, además, se realizó la segunda fecha de la “Chocolatada literaria” en El Café del Molino, donde niños, niñas y adultos disfrutaron de un espectáculo de circo sobre cuentos infantiles, a cargo del grupo Lado B.

**Lo que viene**

El próximo fin de semana continuará la programación del Mes de las Infancias en los espacios culturales.

El viernes en El Alero Acería (Cafferata y Roca) seguirán las proyecciones de cine, además de juegos y actividades al aire libre. En El Alero de Las Flores (Azopardo y Millán Medina), el sábado, volverá la Feria Florecer y habrá cine y juegos. La entrada, en ambos casos, es gratuita y por orden de llegada, pero con limitación por protocolo.

El sábado también abrirá sus puertas el Espacio Cultura de Yapeyú (Misiones 6658) y desde las 14 habrá juegos al aire libre y proyecciones. La entrada será por orden de llegada.

El Molino continuará con su apertura los fines de semana, los sábados y domingos de 15 a 18 horas, con reserva previa de entradas, y los domingos con la “Chocolatada literaria” que cada semana ofrecerá un espectáculo nuevo (la entrada es por orden de llegada).

También el próximo fin de semana retornará la apertura Sapukay - La casa de Fernando Birri (Ubajay 1512 - San José del Rincón), con actividades recreativas y lúdicas, de 15 a 17 horas. La entrada es limitada y con reserva previa al teléfono 4971584 o al 3425205049.

Para más información sobre la programación, entradas y protocolos, consultar las redes sociales de cada espacio.