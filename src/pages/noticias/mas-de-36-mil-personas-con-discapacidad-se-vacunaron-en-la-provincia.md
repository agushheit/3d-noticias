---
category: Estado Real
date: 2021-06-09T08:27:28-03:00
thumbnail: https://assets.3dnoticias.com.ar/vacunacion1.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: Más de 36 mil personas con discapacidad se vacunaron en la provincia
title: Más de 36 mil personas con discapacidad se vacunaron en la provincia
entradilla: A partir del histórico operativo de vacunación, más del 70% ya recibió
  al menos una dosis contra el Coronavirus.

---
El Ministerio de Salud informó, que al día de la fecha, se vacunaron contra el Covid-19 el 71.3 por ciento de personas con discapacidad, en toda la provincia de Santa Fe. Este porcentaje representa un total 36.213 de personas de ese grupo poblacional, que fueron inscriptas en el Registro Provincial.

El Plan Estratégico de Vacunación contra el Covid-19 a personas con discapacidad de Santa Fe, se viene implementando desde febrero en Hogares, Centros de Día, Centros Educativos Terapéuticos, Centros de Formación Laboral, a personas electrodependientes, con Fibrosis Quística, en internaciones domiciliarias y Centros de Rehabilitación. El objetivo de este Plan de Vacunación es lograr lo antes posible la inoculación de las personas de riesgo con discapacidad, para evitar cualquier tipo de complicación en su salud.

De esta manera el subsecretario de Inclusión para Personas con Discapacidad, Patricio Huerga, explicó que: "Seguimos avanzando fuertemente con el Plan de Vacunación a personas con discapacidad en toda la provincia, actualmente le estamos dando prioridad a las personas con discapacidad de riesgo, entre 18 y 59 años; de las cuales faltarían vacunar 6.740 personas, de un total de 11.877. Sabemos de la urgencia y la importancia de vacunar a toda la población con discapacidad, por ello seguiremos trabajando y esforzándonos para completar el plan de vacunación a todas las personas con discapacidad".