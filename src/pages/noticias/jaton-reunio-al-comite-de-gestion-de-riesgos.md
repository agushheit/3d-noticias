---
category: La Ciudad
date: 2021-04-23T07:38:14-03:00
thumbnail: https://assets.3dnoticias.com.ar/gestion-de-riesgo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad de Santa Fe
resumen: Jatón reunió al Comité de Gestión de Riesgos
title: Jatón reunió al Comité de Gestión de Riesgos
entradilla: |2-

  Este jueves se concretó un nuevo encuentro virtual del comité que reúne a funcionarios del municipio y concejales de las distintas bancadas con el objetivo de analizar la situación sanitaria, en el marco de la pandemia.

---
El intendente Emilio Jatón encabezó este jueves una nueva reunión del Comité de Gestión de Riesgos. En este caso, el encuentro fue virtual y del mismo participaron, como es habitual, funcionarios del Gabinete municipal y concejales de la ciudad, en representación de las diferentes fuerzas políticas.

En la oportunidad, el intendente trazó un panorama sobre la situación sanitaria actual que se registra en la capital de la provincia y el área metropolitana. Del mismo modo, brindó un detalle de lo conversado con las autoridades del Gobierno provincial y del trabajo que se lleva adelante en conjunto entre ambos estados.

Una vez planteado el contexto, los funcionarios municipales enumeraron las medidas que entrarán en vigencia en la ciudad, este viernes a la hora cero, luego de que así lo determinara la autoridad sanitaria provincial. Posteriormente se expusieron las gestiones que el municipio lleva adelante para aunar criterios con los diferentes sectores económicos y productivos, con el objetivo de evitar una retracción de las actividades. Y para concluir, se escucharon las opiniones de los concejales y se pusieron en consideración algunas de las propuestas planteadas.

Concluido el encuentro, el secretario de Gobierno del municipio, Nicolás Aimar, detalló que “conversamos sobre las distintas medidas que se fueron tomando en la ciudad como consecuencia de los decretos nacional y provincial, que rigen la circulación y la actividad comercial en nuestra ciudad”. A ello sumó una instancia de intercambio de ideas luego de la cual, los integrantes del Poder Ejecutivo “tomamos nota de las propuestas hechas por los concejales de las diferentes fuerzas políticas, con la intención de seguir trabajando juntos en el ordenamiento de la ciudad”.

Aimar insistió en que “en este momento tan difícil que estamos atravesando, entendemos que es crucial el diálogo entre todos los actores y el compromiso para consensuar medidas que nos ayuden a administrar esta situación de pandemia”. Incluso, mencionó que “muchas sugerencias realizadas por los ediles a lo largo de las diferentes reuniones del Comité fueron incorporadas a los diálogos y en varias de ellas se pudo avanzar”.

En ese sentido, aseveró que “todo el arco político de la ciudad coincidió en la importancia de hacer todos los esfuerzos necesarios para sostener el sistema educativo en funcionamiento y no resentir la actividad económica, ya que entendemos que ambos vienen de procesos muy duros y complejos”. Por ello, mencionó que “para que esto sea factible es imprescindible ser muy cuidadosos y evitar la circulación innecesaria de personas, de modo de sostener aquellas actividades indispensables”.

Por último, mencionó el compromiso de todos los presentes en “transmitirle a la ciudadanía en su conjunto, la importancia de los cuidados individuales, más allá de la responsabilidad del propio Estado municipal de sostener los controles que son de su competencia”. Según remarcó “el objetivo es sostener la actividad económica abierta, las escuelas y los jardines en funcionamiento”.

![](https://assets.3dnoticias.com.ar/gestion-de-riesgo1.jpg)

**Asistentes**

Del encuentro participaron, en representación del municipio, el secretario General, Mariano Granato; el secretario de Producción y Desarrollo Económico, Matías Schmüth; la secretaria de Control y Convivencia Ciudadana, Virginia Coudannes; la secretaria de Políticas de Cuidado y Acción Social, Victoria Rey; y la directora de Gestión de Riesgo, Cintia Gauna; y el director de Derechos y Vinculación Ciudadana, Franco Ponce de León. Además, estuvieron, el presidente del Concejo Municipal, Leandro González, y los ediles, Laura Mondino, Sebastián Mastropaolo, Juan José Saleme, Guillermo Jerez, Carlos Suárez e Inés Larriera.

![](https://assets.3dnoticias.com.ar/gestion-de-riesgo2.jpg)