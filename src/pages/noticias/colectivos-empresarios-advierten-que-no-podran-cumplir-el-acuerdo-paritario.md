---
category: La Ciudad
date: 2022-01-19T06:00:41-03:00
thumbnail: https://assets.3dnoticias.com.ar/UTA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'Colectivos: empresarios advierten que no podrán cumplir el acuerdo paritario'
title: 'Colectivos: empresarios advierten que no podrán cumplir el acuerdo paritario'
entradilla: 'Por nota dirigida al secretario general de UTA aseguran que no podrán
  afrontar los aumentos salariales acordados. La causa: la incertidumbre en la distribución
  de subsidios al interior.'

---
La Federación Argentina de Transportadores por Automotor de Pasajeros (Fatap) avisó a la UTA que no podrá cumplir con el acuerdo paritario al que arribaron en diciembre de 2021. 

La caída del presupuesto nacional 2022, que preveía más subsidios para el transporte, “hará imposible la continuidad del acuerdo atento a que en la misma acta paritaria surge de manera expresa que se asumiera los compromisos con los trabajadores en función de tales aportes”, advierten. 

En consecuencia, ponen en aviso al sector sindical que “la situación de incertidumbre que nuevamente vive el interior del país, en cuanto a la distribución de recursos nacionales acordados, les impide prever y cumplir con los compromisos salariales”.

Aunque solicita a UTA generar instancias de consenso, nada asegura que no se abran frentes de conflicto que afecten la normal prestación del servicio.