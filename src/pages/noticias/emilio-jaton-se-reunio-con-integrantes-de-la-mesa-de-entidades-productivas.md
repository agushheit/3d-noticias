---
category: La Ciudad
date: 2021-11-04T06:00:40-03:00
thumbnail: https://assets.3dnoticias.com.ar/ENTIDADES.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Emilio Jatón se reunió con integrantes de la Mesa de Entidades Productivas
title: Emilio Jatón se reunió con integrantes de la Mesa de Entidades Productivas
entradilla: " La intención es coordinar acciones sobre el crecimiento de la ciudad
  y el área metropolitana, de manera conjunta."

---
La sala 5 de la Estación Belgrano fue el lugar donde se desarrolló una reunión entre el intendente Emilio Jatón y parte de su equipo de trabajo, con los integrantes de la Mesa de Entidades Productivas de la ciudad. En la oportunidad, se estableció una agenda de temas para trabajar en conjunto en lo que resta del año y durante 2022. La intención es coordinar acciones relacionadas con el crecimiento de la ciudad, pero también del área metropolitana.

Algunos de los temas que se pusieron sobre la mesa tienen que ver con el Puente Santa Fe-Santo Tomé; el tren Santa Fe-Santo Tomé; el Circunvalar Santa Fe; el contrato de dragado, la señalización y el balizamiento de la hidrovía Paraná-Paraguay; la Terminal de Ómnibus; el Código de Habitabilidad; y el Relleno Sanitario, entre otros. Además del intendente, estuvieron los secretarios General, Mariano Granato; de Desarrollo Urbano, Javier Mendiondo; y de Ambiente, Edgardo Seguro.

Como integrantes de la Mesa, participaron Julio Schneider y Mario Huber, en representación de ADE; Martín Vigo y Ulises Mendoza, de la Bolsa de Comercio; Javier Martín y Francisco Montes de Oca, de la Unión Industrial; Marcelo Perassi de la Cámara de Comercio Exterior; Ignacio Mántaras y Ricardo Argenti, de la Sociedad Rural de Santa Fe; Sergio Winkelmann de la Cámara Argentina de la Construcción delegación Santa Fe; y Martín Salemi y Jorge Baremberg, del Centro Comercial de Santa Fe.

Tras la reunión, Jatón consideró que “esta mesa de entidades productivas representa a todos los sectores de Santa Fe” y por eso este encuentro sirvió para “elaborar una agenda de trabajo conjunta. La intención fue discutir puntos de encuentro o de desencuentro, pero debatir -en una mesa de diálogo y con mucha escucha- los problemas reales de la ciudad”, agregó.

Más adelante, el intendente manifestó: “Hay problemas que exceden a la ciudad de Santa Fe y tienen que ver con el área metropolitana, pero son clave como el relleno sanitario, el transporte y algunos elementos de producción como el empleo. La agenda es amplia y lo que hicimos es cerrarla un poco, y poner días y horarios para una serie de reuniones en diciembre. Estos encuentros tienen que ser mucho más seguido; tuvimos algunos inconvenientes por el Covid y por eso tenemos que retomarlos”.

**Trabajo conjunto**

Por su parte, Ignacio Mántaras de la Sociedad Rural de Santa Fe aseguró que “esta Mesa de Entidades Productivas tiene una larga historia y una agenda propia orientada a la infraestructura, pero por cuestiones presupuestarias, exceden al municipio. Como representamos al empresariado de la región tenemos que trabajar una agenda metropolitana y urbana, y Santa Fe la tiene”.

Siguiendo esta línea, consideró: “Es importante que el empresariado esté al tanto de las cuestiones que se abordan desde el municipio y tienen que ver con el desarrollo urbano, con lo social y es clave que se encaren en conjunto. Lo importante de este encuentro es la instancia de diálogo público-privado y que sea con frecuencia”.

Teniendo en cuenta que la pandemia postergó algunos puntos, Mántaras consideró que “se tuvieron que repensar varios temas, pero principalmente hoy tienen que ver con la infraestructura que va a permitir el desarrollo de la ciudad y la región”. En esta línea mencionó: “El circunvalar ferroviario es importante, así como el puerto que se está activando y consideramos estratégico”.