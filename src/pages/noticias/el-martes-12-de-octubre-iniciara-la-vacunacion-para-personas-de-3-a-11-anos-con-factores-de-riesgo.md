---
category: Estado Real
date: 2021-10-05T06:15:10-03:00
thumbnail: https://assets.3dnoticias.com.ar/concejofederal.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: El martes 12 de octubre iniciará la vacunación para personas de 3 a 11 años
  con factores de riesgo
title: El martes 12 de octubre iniciará la vacunación para personas de 3 a 11 años
  con factores de riesgo
entradilla: Tras la reunión del Consejo Federal de Salud, la ministra de salud de
  la provincia, Sonia Martorano, dio detalles sobre el avance en el operativo de vacunación.

---
En el marco del histórico plan de inmunización para el Coronavirus, el Ministerio de Salud confirmó cómo se avanzará sobre las personas de 3 a 11 años. En este sentido, el próximo fin de semana llegarán las dosis de Sinopharm, para iniciar el 12 de octubre la vacunación.

Ante esto, la ministra de Salud, Sonia Martorano, detalló: “Participamos de un nuevo encuentro del Consejo Federal de Salud (Cofesa) y abordamos la vacunación de este grupo etario. El fin de semana llegarán las dosis, la población total de santafesinos de 3 a 11 años es de 450 mil, aunque ahora iniciaremos por los que tienen factores de riesgo”.

Al mismo tiempo aclaró que podrán inscribirse en el registro de Santa Fe Vacuna, y que ya se anotaron aproximadamente 100 mil personas.

La ministra Martorano hizo hincapié en que la vacuna Sinopharm tiene tecnología de “virus atenuado” y no requiere de una logística de frío especial. Así, agregó: “La plataforma de esta vacuna es similar a la de la gripe, es una vacuna muy segura, que empezaremos a aplicar el martes 12 de octubre avanzando en orden descendente y priorizando las personas con factores de riesgo”.

Los factores de riesgo son:

* Diabetes tipo 1 o 2 (insulinodependiente y no insulinodependiente).
* Obesidad grado 2 (índice de masa corporal -IMC- mayor a 35) y grado 3 (IMC mayor a 40).
* Enfermedad cardiovascular
* Enfermedad renal crónica
* Enfermedad respiratoria crónica
* Personas que viven con VIH
* Pacientes en lista de espera para trasplante de órganos sólidos y trasplantados de órganos sólidos.
* Personas con discapacidad residentes de hogares, residencias y pequeños hogares.
* Pacientes oncológicos y oncohematológicos con diagnóstico reciente o enfermedad “ACTIVA” (menos de 1 año desde el diagnóstico; tratamiento actual o haber recibido tratamiento inmunosupresor en los últimos 12 meses; enfermedad en recaída o no controlada).
* En pacientes que requieran o realicen tratamiento quimioterápico, se recomienda recibir el esquema completo de vacunación (2 dosis) al menos 14 días previos al inicio del tratamiento. De no ser posible, se sugiere demorar la vacunación hasta el momento en el cual exista una recuperación medular estable o se encuentren en fase de consolidación (según corresponda).
* Personas cursando una TB activa
* Personas con discapacidad Intelectual y del desarrollo que inhabilite las capacidades individuales de ejecutar, comprender o sostener en el tiempo las medidas de cuidado personal para evitar el contagio o transmisión de la COVID#19; y/o se encuentren institucionalizados, en hogares o residencias de larga permanencia; y/o requieran asistencia profesional domiciliaria o internación domiciliaria.
* Síndrome de Down

Para cerrar, Martorano remarcó el avance del operativo de vacunación y concluyó: “Hoy llegamos a 2 millones de santafesinos con dos dosis, lo que representa un 57% de la población total con esquema de inmunización completo seguimos avanzando”.