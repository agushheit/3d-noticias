---
category: Agenda Ciudadana
date: 2021-08-17T06:15:40-03:00
thumbnail: https://assets.3dnoticias.com.ar/PIEDRAS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: '"Marcha de las piedras": homenaje en Plaza de Mayo y en Olivos a las víctimas
  de la pandemia'
title: '"Marcha de las piedras": homenaje en Plaza de Mayo y en Olivos a las víctimas
  de la pandemia'
entradilla: 'Fue convocada a través de las redes sociales. La invitación se fue multiplicando
  a partir de los hashtags #MarchaDeLasPiedras y #VoluntariosMarchaDeLasPiedras.'

---
Un homenaje a las víctimas mortales que dejó la pandemia de coronavirus se llevó a cabo hoy en la Plaza de Mayo y frente a la Residencia oficial de Olivos, donde los asistentes dejaron piedras grabadas con los nombres de muchos de los fallecidos.

El homenaje, convocado a través de las redes sociales bajo la etiqueta "marcha de las piedras", contó con la asistencia de algunos políticos de la oposición.

En la Plaza de Mayo, frente a la Casa Rosada, la mayoría de los participantes del acto, familiares o amigos de los fallecidos a causa de la Covid-19, se acercaron en silencio hasta los sectores elegidos para depositar las piedras grabadas con los nombres de las víctimas y se retiraron del lugar después de unos breves minutos de reflexión y lamento por la pérdida.

La propuesta de movilizarse en un día feriado frente a la Casa Rosada y la residencia presidencial de Olivos surgió en las redes sociales, donde se fue multiplicando a partir de los hashtags #MarchaDeLasPiedras y #VoluntariosMarchaDeLasPiedras, tras un tuiteo inicial que propuso "llevar una piedra por cada muerto por Covid a Casa Rosada y dejarla ahí".

La movilización se fue poblando en las primeras horas de la tarde, tanto en Olivos como en la Plaza de Mayo, con piedras escritas con fibrones o carteles, en las que se intercalaban historias de vida con algunas consignas contra el manejo de la pandemia.

Un afiche colocado sobre el asfalto de la avenida Libertador, frente a la quinta de Olivos, decía, por caso: "Por la corrupción, negligencia e insensibilidad social, prohibido olvidar, QEPD".

Más de 109.000 personas murieron en el país a causa de la pandemia de coronavirus, que en Argentina comenzó en marzo de 2020.