---
category: Estado Real
date: 2021-01-12T09:31:03Z
thumbnail: https://assets.3dnoticias.com.ar/12121-cantero.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: 'Cantero: «Estamos preparando todas las alternativas posibles para recuperar
  la presencialidad»'
title: 'Cantero: «Estamos preparando todas las alternativas posibles para recuperar
  la presencialidad»'
entradilla: 'La ministra de Educación se refirió al inicio de clases y a la vacunación
  para docentes y asistentes escolares: «tenemos la planificación para que, cuando
  las dosis estén, se pueda vacunar con inmediatez».'

---
La titular de la cartera educativa, Adriana Cantero, brindó declaraciones sobre el inicio de clases, que dará inicio el 17 de febrero con los grupos prioritarios. También, se refirió a la estrategia de vacunación que llevará adelante el gobierno provincial para docentes y los asistentes escolares.

En primer lugar, Cantero expresó: «tenemos la planificación resuelta en cuanto a la posibilidad de volver a la presencialidad en Santa Fe; ya hemos presentado el plan de trabajo que ha sido aprobado y el formato de modalidad para el caso que tengamos que tener alternancia».

Del mismo modo, recordó: «vamos a estar expectantes de cómo evolucione la curva epidemiológica y lo que digan las autoridades para marzo. El comienzo de nuestro ciclo lectivo sería el 15 de marzo, pero el 17 de febrero estaríamos comenzando con los grupos prioritarios, que son los 7° grados y los últimos años de la educación secundaria y técnica».

En ese sentido, la ministra expresó: «estamos pensando en pequeños grupos para los últimos años, con un régimen de alternancia (una semana de clases presenciales y una virtual) y el trabajo lo más intenso que se pueda para que los chicos cierren el ciclo de la mejor manera, asegurando las competencias básicas para la prosecución del nivel educativo que sigue».

«Si los santafesinos nos volvemos a comprometer con el cuidado será posible ir poco a poco retomando la vida cotidiana», mencionó Cantero y agregó que «nosotros queremos recuperar presencialidad y, por eso, estamos preparando todas las alternativas posibles para que el 2021 contemple esa posibilidad».

<br/>

## **VACUNACIÓN A DOCENTES Y ASISTENTES ESCOLARES**

Sobre la vacunación para docentes y asistentes escolares, la titular de la cartera educativa mencionó: «tenemos ya la planificación, con selección de lugares y vacunatorios, para que cuando las dosis estén se pueda aplicar la vacuna con inmediatez a todo el sector educativo (alrededor de 85.000 personas) porque eso va a incidir en las alternativas más seguras para la vuelta a la presencialidad».

Finalmente, expresó: «tal vez estemos en una situación de que epidemiológicamente hablando nuestras autoridades indiquen que es posible el comienzo con la alternancia y lo vamos a ir haciendo mientras se va desplegando el plan de vacunación a docentes y asistentes escolares \[sic\]».