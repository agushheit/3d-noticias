---
layout: Noticia con imagen
author: " Fuente: Noticias del Gobierno de Santa Fe"
resumen: Boleto Educativo Gratuito
category: Estado Real
title: El Gobernador Omar Perotti oficializó la creación del Boleto Educativo
  Gratuito en el ámbito de la provincia
entradilla: El programa beneficia a estudiantes de nivel inicial, primario,
  secundario, terciario, universitario, docentes y personal auxiliar de
  establecimientos santafesinos.
date: 2020-10-28T17:58:54.708Z
thumbnail: https://assets.3dnoticias.com.ar/boleto.jpg
---
El gobernador Omar Perotti suscribió el Decreto N°1175 por el que se establece la creación, en el ámbito de la Secretaría de Transporte del Ministerio de Producción, Ciencia y Tecnología, el programa de Boleto Educativo Gratuito (BEG), destinado a usuarios del servicio público provincial de transporte de pasajeros interurbanos suburbanos y urbanos.

“El decreto es una instancia administrativa más para la puesta en marcha del Boleto Educativo Gratuito necesario para poder concretar convenios con universidades, municipios y demás entidades necesarias para poder arrancar el ciclo lectivo 2021 con el boleto en pleno funcionamiento. Si bien no hay clase presenciales esto es un paso más para poner el sistema a prueba en las próximas semanas”, aseguró el gobernador.

Por su parte, la ministra de Educación, Adriana Cantero, explicó que “en el marco del compromiso del gobierno provincial de asegurar el boleto educativo gratuito, iniciamos la etapa de construcción de un nuevo dispositivo para garantizar el traslado rural gratuito para docentes y alumnos que se ha inaugurado en las 33 localidades que iniciaron con el dispositivo de la presencialidad. Ese dispositivo contempla la participación de las comuna con presupuesto que asigna Educación y que se distribuye en las contrataciones situadas que cada niño adolescente y docente requiere según las características de su contexto para poder llegar a la escuela a garantizar el derecho a la educación”.

El programa -anunciado por el mandatario en el inicio de la gestión- está destinado a estudiantes de nivel inicial, primario, secundario y terciario pertenecientes a instituciones educativas públicas de gestión estatal y de gestión privada con aporte estatal que integran el sistema educativo provincial.

También podrá acceder el personal docente, auxiliar docente y escolar no docente que desempeñen tareas en instituciones educativas que integran el sistema educativo de la provincia, además de estudiantes universitario de instituciones públicas o privadas radicadas en el territorio provincial.

Además, se establece que podrán acceder los beneficiarios cuyo grupo familiar no supere el nivel mensual de ingresos equivalente a dos Canastas Básicas para un hogar de cuatro integrantes, establecidas por el Instituto Nacional de Estadísticas y Censos (Indec).

**TODO EL RECORRIDO**

El decreto firmado por el gobernador instituye que el BEG comprende la totalidad del trayecto que realiza cada beneficiario desde su domicilio hacia el establecimiento educativo al que concurre, independientemente de número de tramos, y se fija un precio equivalente a la tarifa básica del servicio común vigente al momento de la realización del viaje, salvo que se accedan a servicios diferenciales, sobre los cuales los beneficiarios deberán abonar la diferencia entre el costo del pasaje.

También se faculta a la Secretaría de Transporte a brindar asistencia a aquellos beneficiarios que concurran a establecimientos educativos rurales en distritos que no cuenten con un servicio público regular de transporte.

Por otra parte, la Secretaría de Transporte elaborará y suscribirá los convenios para garantizar el beneficio en todo el territorio provincial, fijar la documentación que deberán presentar las personas interesadas en accede al beneficio, y establecer etapas de implementación progresiva e inmediata del programa.

Finalmente, el decreto aclara que las credenciales o documentación que se otorgue a los beneficiarios tienen carácter personal e intransferible, y su uso indebido dará lugar a la suspensión del goce del beneficio, por el tiempo que establezca la autoridad de aplicación.