---
category: Agenda Ciudadana
date: 2021-01-26T09:15:56Z
thumbnail: https://assets.3dnoticias.com.ar/alimentar.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: Cómo sacar la tarjeta Alimentar
title: Cómo sacar la tarjeta Alimentar
entradilla: Es entregada por el Gobierno nacional en diferentes etapas en todo el
  país. Cuáles son los requisitos necesarios para obtenerla.

---
El saldo de la Tarjeta Alimentar tendrá un aumento del 50%.

De acuerdo con lo trascendido, el ministro de Desarrollo Social, Daniel Arroyo, explicó que este incremento es necesario como consecuencias de las subas que tuvieron los alimentos en el último año. "El problema mas grande hoy es el costo de los alimentos", por lo cual "evaluamos un aumento en el valor de la Tarjeta Alimentar”, afirmó en una entrevista con Radio Rivadavia. 

"Se ha complicado la calidad nutricional, se está comiendo mucha harina”, agregó. Por la crisis, el Gobierno evalúa aumentar el monto de la Tarjeta Alimentaria Cuáles son los nuevos montos de la Tarjeta Alimentar tras el aumento del 50% De este modo, el nuevo saldo de la Tarjeta Alimentar será el siguiente: 

* Familias con un hijo o hija de hasta seis años que cobran la AUH: $6.000 
* Familias con dos o más hijos de hasta seis años que cobran la AUH: $9.000 
* Mujeres embarazadas a partir de los tres meses de gestación que perciben la Asignación por Embarazo: $6.000 
* Personas con discapacidad que perciben AUH, sin límite de edad para los hijos: $6.000 si tienen un solo hijo y $9.000 si tienen dos o más hijos.

   Cabe señalar que esos montos se suman a los haberes habituales que cobran los beneficiarios por AUH y AUE todos los meses. AFIP habilitó la inscripción a los créditos a tasa subsidiada y Repro II para pago de salarios Cómo obtener la Tarjeta Alimentar La Tarjeta Alimentar es entregada por el Gobierno nacional en diferentes etapas en todo el país. 

  Para saber cuándo se puede retirar se debe consultar en [https://tarjetaalimentaria.argentina.gob.ar.](https://tarjetaalimentaria.argentina.gob.ar. "https://tarjetaalimentaria.argentina.gob.ar.") 

  Es importante tener en cuenta que solo podrán obtenerla aquellas personas que cobren la Asignación Universal por Hijo con hijas e hijos de hasta 6 años inclusive, embarazadas a partir de los 3 meses que cobren la Asignación por Embarazo para Protección Social y personas con discapacidad que cobren la Asignación Universal por Hijo, sin límite de edad. 

  Para acceder a los saldos mensuales de la Tarjeta Alimentar no es necesario inscribirse en ningún registro ni tramitarla ya que la otorgación es automática, cruzando bases de datos del Anses y de la Asignación Universal por Hijo ( AUH) o la Asignación por Embarazo. ANSES será quien notifique al titular que está en condiciones de retirar su tarjeta por el banco que determine cada provincia.

  Las notificaciones se realizarán vía telefónica o por SMS al número registrado en la base de ANSES. Para más información se pude ir a: [https://www.anses.gob.ar/tramite/tarjeta-alimentar](https://www.anses.gob.ar/tramite/tarjeta-alimentar "https://www.anses.gob.ar/tramite/tarjeta-alimentar") [m@perfil.com](mailto:perfilcom@perfil.com "Consultar por Email")