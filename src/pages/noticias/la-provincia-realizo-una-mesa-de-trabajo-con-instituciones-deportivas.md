---
category: Deportes
date: 2021-03-23T07:14:25-03:00
thumbnail: https://assets.3dnoticias.com.ar/deportes.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: La provincia realizó una mesa de trabajo con instituciones deportivas
title: La provincia realizó una mesa de trabajo con instituciones deportivas
entradilla: Es la primera vez que se reúnen representantes de 27 disciplinas deportivas
  de Santa Fe.

---
El Ministerio de Desarrollo Social, a través de la Secretaría de Deportes, realizó una mesa de trabajo con representantes y autoridades de todas las federaciones y asociaciones deportivas de la provincia.

La actividad, realizada en el Club Náutico Sportivo Avellaneda de la ciudad de Rosario, reunió en una jornada histórica a instituciones de 27 disciplinas distribuidas en todo el territorio provincial.

El ministro de Desarrollo Social, Danilo Capitani, indicó “que la pandemia nos cambió los proyectos, a nosotros como Estado provincial y a las instituciones de la sociedad civil. En este aspecto, el gobernador nos pidió que estemos al lado de los clubes; las instituciones deportivas son fundamentales, no solo en el proceso de desarrollo de los chicos sino fundamentalmente en lo que tiene que ver con la contención y la inclusión, con una vida saludable y el desarrollo personal”.

“Y esto se propicia a través del trabajo territorial como el que venimos realizando, garantizando estar presentes con las herramientas del Estado al lado de cada uno de los clubes. Por eso es importante esta vinculación, generando espacios de participación y escucha”, añadió Capitani.

Al respecto, la secretaria de Deportes, Claudia Giaccone, resaltó: “Estamos muy satisfechos con la convocatoria de esta reunión, tuvimos la participación de todas las instituciones madre de las distintas disciplinas deportivas de Santa Fe, en un mesa de trabajo que busca socializar, democratizar la información y fortalecer mediante la participación las políticas públicas vinculadas al deporte federado”.

“Surgieron debates sobre diferentes ejes y sin dudas será el primero de varios encuentros. Queremos escuchar a la comunidad deportiva en primera persona y, de esta forma, lograr que los programas y las iniciativas sean lo más efectivas posibles y alcancen a todo el territorio provincial”, amplió Giaccone.

El orden de temas, además de escuchar las dudas y propuestas, incluyó los criterios de categorización de las federaciones y asociaciones en referencia al Consejo Provincial del Deporte, las posibles modificaciones respecto el Reglamento de Becas, el Calendario de Competencias Deportivas y la actualización y revisión de los Protocolos Sanitarios.

Durante la reunión de trabajo estuvieron presentes autoridades de atletismo, básquetbol, ajedrez, voleibol, remo, ciclismo, tenis, judo, boxeo, tenis de mesa, canotaje, hockey, golf, vela, esquí náutico, bochas, patín, fútbol, automovilismo, taekwondo, esgrima, gimnasia, pelota paleta, tiro, rugby, natación y levantamiento olímpico.  

**PRESENTES**

También participaron los directores Provinciales de Deportes Federados, Lucas Ciarniello; de Clubes, Florencia Molinero; y de Deporte Comunitario, Sergio Córdoba.