---
category: Agenda Ciudadana
date: 2021-11-26T06:15:02-03:00
thumbnail: https://assets.3dnoticias.com.ar/EXTRANJEROS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: 'El BCRA prohibió vender pasajes al exterior en cuotas: busca desalentar
  la salida de divisas'
title: 'El BCRA prohibió vender pasajes al exterior en cuotas: busca desalentar la
  salida de divisas'
entradilla: 'El objetivo es desalentar el turismo a otros países y la consiguiente
  pérdida de dólares, indicaron fuentes del mercado financiero y de las agencias de
  turismo.

'

---
La máxima autoridad monetaria dispuso la medida a través de la Comunicación 7407, que informó a las entidades y emisoras de tarjetas que desde ahora no se podrán financiar en cuotas las compras efectuadas a través de tarjetas, pasajes hacia otros países y servicios turísticos en el exterior.

Además, el BCRA precisó que "los pasajes de avión con destino dentro del territorio nacional se podrán financiar con tarjeta de crédito dentro de los programas de Ahora 12 impulsados por el Gobierno nacional". Agregó que en la compra de boletos de avión "con destino al exterior se podrá financiar con tarjeta de crédito con una tasa de 43%", según un comunicado de la entidad.

Voceros de la entidad aclararon que los bancos "pueden dar un crédito para pagar el pasaje, tipo crédito personal".

Según esa medida aprobada hoy por el directorio de la entidad, todos los servicios contratados con el exterior que se paguen con tarjeta deberán ser saldados en un único pago o financiados con la tasa del 43% fijada para el "pago mínimo" de los resúmenes.

La resolución prohíbe la aplicación de cuotas para el pago de servicios turísticos tanto a emisoras de tarjetas en forma directa como a través de plataformas de viajes, según lo establecido por la comunicación de la entidad.

Según la determinación del BCRA, "las entidades financieras y no financieras emisoras de tarjetas de crédito no deberán financiar en cuotas las compras efectuadas mediante tarjetas de crédito de sus clientes -personas humanas y jurídicas- de pasajes al exterior y demás servicios turísticos en el exterior (tales como alojamiento o alquiler de auto).

Alcanza a las operaciones "realizadas en forma directa con el prestador del servicio o indirecta, a través de agencia de viajes y/o turismo, plataformas web u otros intermediarios", según la normativa del Banco Central.

**Las explicaciones de BCRA**

Si bien no lo dice la resolución, voceros del BCRA aclararon que las personas podrán seguir comprando pasajes al exterior financiados.

Pero ese financiamiento surgirá de la tasa de interés que cobre la tarjeta por la parte del resumen de gastos que no se pague al vencimiento. Se terminó el financiamiento sin interés.

Así, el usuario deberá elegir cómo financiarse, en función de los intereses que cobre la tarjeta de crédito y que se informan en el resumen de gastos.

También estará la opción de tomar un préstamo personal, con cuotas y tasas establecidas, y con ese dinero saldar en un solo pago la compra del pasaje o el paquete turístico.​

Los bancos o las tarjetas deberá fijar una tasa de interés o a cambio ofrecer un préstamo personal para la compra del pasaje o el paquete turístico.

Todos los servicios contratados con el exterior que se paguen con tarjeta de crédito deberán ser saldados en un único pago o financiados con la tasa del 43% fijada para el “pago mínimo” de los resúmenes.