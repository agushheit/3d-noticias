---
category: La Ciudad
date: 2021-08-17T06:15:33-03:00
thumbnail: https://assets.3dnoticias.com.ar/IAPOS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Iapos: la mudanza al renovado edificio será en tres etapas y en octubre
  abriría sus puertas'
title: 'Iapos: la mudanza al renovado edificio será en tres etapas y en octubre abriría
  sus puertas'
entradilla: El inmueble fue desalojado en enero de 2018 para dar inicio a las obras
  y la atención se trasladó al puerto local. Tres años y medio después, el Instituto
  vuelve a su sede de Rivadavia.

---
Solo restan procesos administrativos y de logística para que el Iapos comience el proceso de mudanza al renovado edificio de calle Rivadavia. Pasaron más de tres años y medio desde que el instituto abandonó dicho inmueble para que se dé inicio a los trabajos. Desde esa fecha, la obra social atiende al público en el puerto local.

Con el mobiliario listo, algo que llevó más tiempo de lo estimado, y el equipamiento completo, solo queda realizar el traslado al moderno edificio que se realizará en tres etapas.

Alrededor de 250 trabajadores se preparan para volver a un edificio pensado específicamente para el funcionamiento del Instituto Autárquico Provincial de Obra Social (Iapos), a diferencia de la vieja estructura que debía ajustarse a los límites que imponía el diseño un hotel, ya que para eso había sido construido inicialmente por la privilegiada ubicación.

Así se lo confirmó a Santa Fe el director de la obra social, Oscar Broggi. "Es un traslado complejo" advirtió el funcionario al recordar que se trata del puesto de trabajo de 250 empleados.

Adelantó que el proceso comenzará a fines de agosto y que la idea es culminar en septiembre. "En octubre queremos estar ya con toda la atención centralizada a nuestros afiliados sobre calle Rivadavia", afirmó.

Con relación a la mudanza comentó que comenzarán con la etapa 1 y 2, y profundizó: "Se mudarán las oficinas que hoy están ubicadas en la zona del puerto, que entre otros servicios brindan todo lo que es atención al público; luego, seguirán las oficinas que se encuentran en el edificio contiguo, que también se encuentran en el puerto y donde funcionan otras áreas administrativas. La tercera y última etapa incluye a las áreas que están sobre calle San Martín al 3100 (edificio propio de la obra social)".

El instituto se alista para que el traslado no afecte la atención, principalmente la presencial. En ese sentido, Broggi destacó que con la pandemia se aceitaron algunos trámites virtuales ya que "hay muchísimos canales que ya no son presenciales, algo que nos va a beneficiar en el nuevo edificio".

Con respecto a la distribución que tendrá la nueva sede destacó: "Son siete pisos, de los cuales la planta baja es para atención al público. Luego, hasta el sexto, son todas áreas de administración. En el último piso, el séptimo, hay salas de capacitación, reuniones. Son todas áreas abiertas, excepto en alguna que tenga que ver con alguna documentación que resguardar, todas las áreas son totalmente compartidas, abiertas con mayor contacto entre las personas. Es un concepto distinto".

Y añadió: "Cuenta con todas las instalaciones eléctricas, área de informática, tendido de fibra óptica, la climatización; es un edificio con conceptos nuevos, adaptado a normas medioambientes, con un sistema retardados de agua".

Con respecto a la inversión realizada, el titular del Iapos comentó que "estamos arribando a unos 200 millones de pesos, aproximadamente, en el costo final de obra. Vamos a tener seguramente algunas inversiones posteriores que no la estamos contabilizando que tienen que ver con tecnología que vamos a ir implementando".