---
layout: Noticia con imagen
author: .
resumen: "Coronavirus: nuevas normas "
category: La Ciudad
title: Desde este lunes se autorizan hasta 30 trabajadores en obras y se amplía
  el horario de bares
entradilla: Así se dispuso dentro de las nuevas normas de convivencia por el
  coronavirus en la provincia. Además, el gobernador, a través del Decreto Nº
  1307, establece la extensión del aislamiento.
date: 2020-11-10T14:25:25.924Z
thumbnail: https://assets.3dnoticias.com.ar/peatonal.jpg
---
A través del decreto Nº 1307 y Nº 1308, el gobernador de la provincia, Omar Perotti, establece la extensión del Aislamiento Social Preventivo y Obligatorio (ASPO) para los departamentos La Capital, Rosario, General López, Caseros, Constitución, San Lorenzo, Las Colonias y Castellanos, hasta el 29 de noviembre inclusive. Además, el gobernador amplió por decreto la cantidad de operarios que podrán tener las obras en construcción y extendió el horario de funcionamiento de restaurantes y bares.

En cuanto a bares, locales gastronómicos, restoranes y heladerías con y sin concurrencia  de comensales, establece que el horario de cierre se extiende hasta las 0.30hs, es decir, media hora más de lo que estaba permitido. Lo mismo ocurre para el *delivery*. Lo único que no cambia y permanece como hasta ahora es el sistema *take away* que en lugares sin comensales será hasta las 20hs.

Por otro lado, en lo que refiere a obra privada, amplía la cantidad de trabajadores, que pasa de 10 a 30, en todo el territorio provincial desde las 0.00hs. de este 9 de noviembre.

El decreto también volvió a fijar restricciones para circular entre las 20.30hs. y las 6hs. de la mañana, y detalló que la circulación “queda estrictamente restringida a la necesaria para desarrollar actividades autorizadas, haciendo uso  del transporte público  o del servicio de taxis y remises para esos fines.”

![](https://assets.3dnoticias.com.ar/ig-provincia.jpg)

Ver publicación en [Instagram](https://www.instagram.com/p/CHXkSZbgD8I/?utm_source=ig_web_copy_link)