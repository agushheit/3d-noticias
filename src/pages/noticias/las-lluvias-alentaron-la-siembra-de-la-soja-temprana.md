---
category: El Campo
date: 2022-11-16T18:21:54-03:00
thumbnail: https://assets.3dnoticias.com.ar/soja.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Bolsa de Comercio de Santa Fe.
resumen: Las lluvias alentaron la siembra de la soja temprana
title: Las lluvias alentaron la siembra de la soja temprana
entradilla: La intención de siembra de la soja temprana se mantuvo en el 1.000.000
  de hectáreas, 6% más que la campaña anterior. Hasta ahora, se lleva implantado entre
  el 28 y 30% de la estimación.

---
La semana comprendida entre el 9 y el 15 de noviembre comenzó con estabilidad climática, días soleados, temperatura media diaria en ascenso y buen tiempo. A inicios del jueves 10 se incrementó la nubosidad, inestabilidad, algunas precipitaciones de bajas intensidades, muy débiles y con escasos montos.  
  
Con el transcurso de los días, nuevamente se incrementó la temperatura, viento de dirección norte – sur, estabilidad, que a inicios del sábado 12 varió por el ingreso de un frente de tormenta. Se produjeron lluvias con irregulares registros hasta el domingo 13.  
  
En general, de bajas intensidades, por lo que se logró el máximo de eficiencia, infiltración total en los perfiles de los suelos y en toda el área del SEA. En promedio, el acumulado de los totales pluviométricos osciló entre 35 y 45 mm, con mínimos de 03 a 20, máximos de 75 a 83, pero en sectores puntuales superaron los 115 milímetros.  
  
Ante la realidad ambiental y en particular la disponibilidad de agua útil en los perfiles de los suelos, como consecuencia de la concreción de lluvias, se percibió la decisión de los productores y se evidenció en el proceso de siembra de soja temprana.  
  
Se observó el movimiento de sembradoras y equipos de acuerdo a las condiciones particulares de cada zona santafesina. Situación que, con el transcurso de los días aceleraría el ritmo de la implantación.  
  
La intención de siembra se mantuvo en 1.000.000 de ha, incrementándose en un 6 % la superficie de la campaña del año anterior que fue de 945.000 ha. Hasta la fecha se concretó el 28 al 30 % del total estipulado.  
  
En los distintos departamentos del SEA, se observó a los trigales con realidades dispares según los eventos acaecidos, en los del norte, en pleno proceso de recolección y en el resto, en finalización de la etapa de maduración.  
  
Para el intervalo comprendido entre el 16 y el 22 de noviembre, los pronósticos prevén desde su comienzo, estabilidad climática, días soleados, temperaturas medias diarias en ascenso, buen tiempo y escasa a nula nubosidad.  
  
A inicios del domingo 20 se incrementaría la nubosidad, de parcial a total durante la jornada y con ello, alta probabilidad de inestabilidad, la posibilidad de precipitaciones, dicho pronóstico se extendería hasta inicios o mediados del martes 22, las lluvias serían de variadas intensidades y montos relativamente bajos. Esta situación se manifestaría en toda el área del SEA. Las temperaturas fluctuarían entre mínimas de 11 a 24 ºC y máximas de 23 a 38 ºC

**Trigo**

En el norte santafesino, avanzó a buen ritmo el proceso de cosecha del trigo, como consecuencia de las condiciones ambientales, en particular en las áreas donde los registros pluviométricos de la semana fueron bajos y en otras, con el transcurso de los días soleados, mejoraron las características de los pisos de los lotes, lo que posibilitó la reanudación del proceso con mayor ritmo.  
  
Los rendimientos continuaron siendo muy variables y reflejaron la realidad ambiental que recorrió el ciclo del cereal, las etapas fenológicas afectadas y la zona geográfica particular. A medida que avanzó la recolección y se ingresó a nuevas áreas centrales del SEA, se obtuvo un incremento de los rendimientos promedios.  
  
Hasta la fecha los mínimos alcanzaron los 7 qq/ha y hasta 14, con valores máximos puntuales que lograron los 42 qq/ha, pero, además, en predios muy excepcionales oscilaron entre 46 – 48 - 50. Hasta el momento, el promedio general en los lotes cosechados de 18 a 22 y 22,5 qq/ha. Paralelamente, el resto de los trigales avanzó lentamente a sus últimos estados fenológicos.  
  
Lo observado y lo relevado fue:  
  
• un 45 % de las parcelas en estado bueno, con algunos sembradíos excelentes a muy buenos, especialmente en los departamentos del centro norte del SEA,  
  
• un 32 % regular, con diferenciación en el color de las hojas con amarillamiento o senescencia, lotes no uniformes, disparidad de altura y de las estructuras de las plantas, síntomas e indicadores de las condiciones ambientales y  
  
• un 23 % malo.

**Girasol**

El cultivo del girasol se encontró transitando normalmente su ciclo, sin inconvenientes, con adecuada y disponible humedad en los perfiles de los suelos, en casi la totalidad de los predios, como consecuencia de las precipitaciones.  
  
Se observó a la oleaginosa en muy bueno estado, con lotes puntuales excelentes en un 95 % de ellos, bueno en un 3 % y regular en un 2 %, del total del área implantada. El stand de plantas en general fue muy bueno y homogéneo, a excepción de los que por diferentes motivos fueron afectados y se debieron resembrar parcialmente, reaccionando favorablemente, pero en algunas parcelas se continuó observando cierta irregularidad y no uniformidad.

**Maíz Temprano**

Los cultivares de maíz de primera continuaron su crecimiento o desarrollo, en su totalidad beneficiados por las precipitaciones. Con el paso de los días, según las diferentes áreas, reaccionaron muy favorablemente tras los efectos e incidencia de la última helada del 01/11/2022, la cual impactó en distintos grados, según la etapa fenológica en que se encontraban los maizales

**Algodón**

Luego de las condiciones adversas desde el inicio del ciclo del algodón, heladas tardías y la acción de los fuertes vientos, los cultivares según áreas, aprovecharon las precipitaciones de la semana y recuperaron su estado.  
  
Las lluvias posibilitaron el avance de la implantación en nuevas parcelas, principalmente en la zona norte algodonera y las afectadas, se resembraron. El resto de los sembradíos germinaron y emergieron normalmente dada la óptima humedad disponible.  
  
Hasta la fecha se ha implantado:  
  
• sector este algodonero, con un 60 % de avance de siembra,  
• sector oeste algodonero, con un 23 % de avance de siembra.

**Agua útil**

En la superficie total del área de estudio del centro norte santafesino, constituida por los departamentos Nueve de Julio, Vera, General Obligado, San Cristóbal, San Justo, San Javier, Garay, Castellanos, Las Colonias, La Capital, San Martín y San Jerónimo, la disponibilidad de agua útil en los primeros 20 cm de los suelos en toda el área de estudio, se encontró con diferentes realidades, desde escasa en localidades puntuales y buena, muy buena en un alto porcentaje del SEA, como consecuencia de las precipitaciones ocurridas.  
  
La dinámica de los escenarios ambientales, las particularidades zonales y los múltiples factores actuantes, condicionaron o regularon la toma de decisiones finales, ante los futuros escenarios climáticos y de logística.