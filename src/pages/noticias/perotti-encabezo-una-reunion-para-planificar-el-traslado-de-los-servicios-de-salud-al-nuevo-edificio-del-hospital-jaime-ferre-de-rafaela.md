---
category: Estado Real
date: 2021-11-02T06:00:13-03:00
thumbnail: https://assets.3dnoticias.com.ar/peroenrafa.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Perotti encabezó una Reunión para planificar el traslado de los servicios
  de salud al nuevo edificio del hospital Jaime Ferre de Rafaela
title: Perotti encabezó una Reunión para planificar el traslado de los servicios de
  salud al nuevo edificio del hospital Jaime Ferre de Rafaela
entradilla: El gobernador sostuvo que este proceso “no es un elemento que queremos
  dejar improvisado. Además, se licitó la tercera etapa, que demandará una inversión
  superior a los 2.600 millones de pesos.

---
El gobernador Omar Perotti mantuvo este lunes una reunión con el intendente de la ciudad de Rafaela, Luis Castellano, y el director del Hospital Jaime Ferré de esa ciudad, Diego Lanzotti, en la que abordaron cuestiones referidas al proceso de traslado de los distintos servicios al nuevo edificio del nosocomio, cuya construcción presenta importantes avances.

Al término del encuentro, el mandatario provincial recordó que “ya tenemos fecha cierta sobre las inversiones y lo que van a demorar esas inversiones, y cómo se van preparando los distintos servicios para lo que va a ser en su tiempo el traslado”.

Al respecto, cabe recordar que la segunda etapa de la obra del nuevo edificio (que se emplaza sobre Bv. Lehmann, entre Arturo Frondizi, 25 de Mayo, y Calle Pública Sur N° 3) se encuentra en ejecución y cuenta con un avance del 81 por ciento. En tanto, este lunes se licitó la tercera etapa, que demandará una inversión superior a los 2.600 millones de pesos.

Esta tercera etapa, que tiene un plazo de 360 días, “es el núcleo más importante de la obra”, reconoció Perotti. No obstante, “hay una etapa que prácticamente está terminada, que es la que va a poder empezar a utilizarse mientras esté en obra, y lo que nos parece central es que todo el personal y los profesionales de los distintos servicios, vayan teniendo una preparación para lo que va a ser en su momento el traslado de la mayoría de esos servicios a aquel lugar”.

En ese sentido, el gobernador valoró la opinión y el aporte a partir de “la experiencia que cada uno tiene durante muchísimos años” para coordinar este proceso. Va a haber que estar atendiendo y planificando, con lo cual empezar con tiempo es clave, no es un elemento que queremos dejar improvisado o que se traslade algo sin que aquello esté probado, por lo cual, con la debida antelación, se ha abierto hoy la etapa de trabajo en ese sentido”.

Por otro lado, respecto de la continuidad de la obra, Perotti mencionó que tras esta tercera etapa “solamente va a quedar una etapa, pero no vamos a esperar los 360 días de plazo que tiene para su finalización, sino que se va a licitar mucho tiempo antes para que estén todas las etapas yendo a la finalización del hospital, seguramente en un año y medio”.

“Ojalá el nuevo hospital nos de 100 años de noble funcionamiento como nos ha dado este hospital -agregó el mandatario provincial-, y ojalá se puedan combinar estas dos infraestructuras de la mejor manera para que Rafaela y toda la región tengan un servicio de salud a la altura de las expectativas”; y remarcó que esa es “una discusión que se tiene que ir dando acerca de la utilización de los dos edificios: el nuevo cómo se va planteando, y el actual edificio del hospital, qué servicios continúa teniendo aquí y cómo lo hacemos”.

**NECESIDADES**  
El gobernador expresó también que “el área de salud en la provincia, y particularmente fuera de Santa Fe y Rosario, ha quedado con muchas necesidades de cobertura, así que necesitamos ir teniendo una adecuación a lo que es esta nueva realidad. También queremos saber cómo va a quedar el esquema post-pandemia”.

No obstante, Perotti afirmó que “sí se decidió avanzar fuerte en infraestructura en el interior, en terapias intensivas, en reforzar todos los centros de salud, en mejorar todos los servicios, en definitiva: todo lo que se pueda hacer en las instancias territoriales o locales poder brindarlo, evitando el esquema del traslado o dejando las instancias de traslado solamente para las altas complejidades. Eso lleva también a readecuar la mirada de cómo es la dotación de personal que, hasta aquí, se veía en un esquema de alta concentración en Santa Fe y Rosario”, concluyó.

**PEDIATRÍA PRIMERO**  
Mientras tanto, el director del Hospital Jaime Ferré, confirmó que el servicio de pediatría del nosocomio será el primero en trasladarse al nuevo edificio. “Esto ya lo veníamos hablando cuando desarrollamos esta etapa que se está terminando, lo que permitirá volver a unificar el servicio de pediatría que hoy lo tenemos atomizado en tres lugares, y queremos volver a unir el proceso de atención pediátrica en uno solo”.  
Asimismo, Diego Lanzotti destacó esta reunión como “un momento único, que es en el que el personal sanitario se pone al lado del de infraestructura y arquitectura a diseñar un proyecto de tamaña magnitud. Para nosotros significa la oportunidad de llevar experiencia hacia la parte técnica y que de esta unión de esfuerzos salga lo mejor”.

La reunión de trabajo se realizó en el Hospital Jaime Ferré y de la misma participaron también la ministra de Infraestructura, Servicios Públicos y Hábitat, Silvina Frana; la secretaria de Obras Públicas de la provincia, Leticia Battaglia; las secretarias de Obras Públicas de Rafaela, Bárbara Chivallero; y de Desarrollo Humano, Miriam Villafañe; el subsecretario de Salud municipal, Martín Racca; el responsable de la Región Rafaela de Salud, Eter Senn; la concejala Brenda Vimo; el jefe del Servicio de Emergencias 107, Emilio Scarinci, entre otros representantes y trabajadores del hospital.