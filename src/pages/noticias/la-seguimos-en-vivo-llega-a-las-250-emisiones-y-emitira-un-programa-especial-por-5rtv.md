---
category: Estado Real
date: 2020-12-31T11:28:20Z
thumbnail: https://assets.3dnoticias.com.ar/3112-laseguimosenvivo.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: La Seguimos en Vivo llega a las 250 emisiones y emitirá un programa especial
  por 5RTV
title: La Seguimos en Vivo llega a las 250 emisiones y emitirá un programa especial
  por 5RTV
entradilla: 'El ciclo provincial produjo El Especial #250, una compilación que se
  emitirá este jueves 31, a las 21:00 horas por el canal público de Santa Fe y las
  redes sociales del Ministerio de Cultura.'

---
El resumen del ciclo _La Seguimos en Vivo_ (LSEV) ,que el Ministerio de Cultura diseñó cuando se declaró la pandemia de Covid-19 con el fin de continuar y promover la actividad escénica en toda la provincia, registra todos los espectáculos que –vía streaming–, se transmitieron durante más de nueve meses en todo el territorio santafesino.

«LSEV permitió que la comunicación se concrete de otro modo y que en cada sala, centro cultural o teatro de la Provincia las presentaciones se vivieran como un reencuentro entre los públicos y las/os artistas», comentó el ministro de Cultura Jorge Llonch.

«_El Especial #250_ permitirá revivir cientos de momentos emotivos, con audiencias diversas, en espacios que tienen un alto valor simbólico para cada comuna y ciudad. Fue un desafío enorme reconvertir toda una programación al formato virtual, logrando junto a los ministerios de Salud y Trabajo un protocolo que les permita a técnicos, camarógrafos, editores, plomos y artistas retornar a la actividad a través del streaming», expresó el funcionario.

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.2em; font-weight: 700">Cifras elocuentes</span>**

Tras casi 10 meses de comenzado el ciclo, las estadísticas dan cuenta de la potencia de LSEV. Más de 1.500 artistas pasaron por la pantalla de @5RTV, el canal público, y las redes sociales del Ministerio de Cultura.

Se pusieron al aire 250 emisiones, en las que actuaron 1.550 artistas de las más diversas disciplinas y géneros: música, teatro, danza, circo, batallas de gallos, performances de DJ, etc.

Se registraron 280 mil reproducciones en las redes sociales del Ministerio de Cultura, y se programaron shows desde 20 sedes, en localidades de todas las regiones de la Provincia.

Para Llonch, «LSEV, además de haber servido para que se reactive el sector, es una muestra más de uno de nuestros ejes de gestión, que es promover y aportar a una Cultura Federal».

Para ver _El Especial #250_, el jueves 31 de diciembre, a las 21:00 horas, además de seguirlo por la pantalla de 5RTV, se puede acceder a las siguientes cuentas del Ministerio de Cultura:

**YouTube:** [https://www.youtube.com/channel/UCkuzOi3cva8y31FhxE5yOyQ](https://www.youtube.com/channel/UCkuzOi3cva8y31FhxE5yOyQ "https://www.youtube.com/channel/UCkuzOi3cva8y31FhxE5yOyQ")

**Facebook:** [https://www.facebook.com/](https://www.youtube.com/channel/UCkuzOi3cva8y31FhxE5yOyQ "https://www.youtube.com/channel/UCkuzOi3cva8y31FhxE5yOyQ")