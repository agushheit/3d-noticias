---
category: Agenda Ciudadana
date: 2021-10-31T06:00:06-03:00
thumbnail: https://assets.3dnoticias.com.ar/negociaciones.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: '"Negociar con firmeza es recuperar soberanía"'
title: '"Negociar con firmeza es recuperar soberanía"'
entradilla: 'El Presidente, acompañado por le ministro de Economía Martín Guzmán,
  dialogó con la directora del Fondo Monetario Internacional para avanzar en un acuerdo
  que permita posponer vencimientos por casi 45.000 millones '

---
El presidente Alberto Fernández sostuvo que "negociar con firmeza es recuperar soberanía", tras reunirse esta tarde en Roma con la directora gerente del Fondo Monetario Internacional (FMI), Kristalina Georgieva, para dar continuidad a las negociaciones de Argentina con el organismo.  
"Buen encuentro con la directora gerente del FMI para avanzar en negociaciones que nos permitan salir del lugar social y económicamente insostenible en donde el gobierno que me precedió dejó a nuestra amada Argentina", dijo Fernández, tras el encuentro que se realizó en la Embajada argentina en Italia.

Por su parte, Georgieva afirmó en su cuenta de Twitter que se trató de un "buen encuentro con el presidente" Fernández y precisó: "Acordamos que nuestros equipos deben trabajar juntos e identificar políticas sólidas para abordar los importantes desafíos económicos de Argentina en beneficio del pueblo argentino".

Fernández y Georgieva se reunieron esta tarde en Roma para dar continuidad a las negociaciones que realiza la Argentina con el organismo.

Del encuentro, que se prolongó por una hora y media, participaron también el ministro de Economía, Martín Guzmán, el secretario de Asuntos Estratégicos, Gustavo Beliz, la subdirectora del Departamento del Hemisferio Occidental del FMI, Julie Kozack; el primer subdirector gerente del organismo, Geoffrey W.S. Okamoto; y Dominique Desrruellem, también integrante del Departamento del Hemisferio Occidental.

Al término de la bilateral, ambas partes coincidieron en que "fue una buena reunión", indicaron fuentes oficiales.

De esta forma se busca alcanzar un acuerdo que permita a la Argentina repagar la deuda contraída por la gestión anterior.

En los próximos días habrá reuniones técnicas con el staff para continuar el proceso, añadieron las fuentes.

El objetivo del Gobierno nacional es avanzar en la definición de un acuerdo que permita extender plazos para evitar pagar US$ 19.000 millones en 2022, y otros US$ 18.000 millones en 2023 y cerca de US$ 5.000 millones en 2024, según el programa del gobierno anterior.

Se trata del segundo encuentro en seis meses entre Fernández y Georgieva, y es de fundamental importancia para el objetivo de conseguir una reducción de la sobretasa que paga el país por haber recibido un préstamo que superó el porcentaje de participación que tiene la Argentina en el Fondo Monetario, y que representa cerca de mil millones de dólares adicionales por año.

En el Gobierno aseguran que el tema de la sobretasa en el G20 está bien encaminado y consideran que, si el mundo desarrollado pide cambio climático y sostenibilidad, "todo eso cuesta" y por eso es necesario que, a cambio de los esfuerzos de los países para adecuarse, se bajen los costos del crédito.El ministro Guzmán y Anton Siluanov en Roma. Foto: Ministerio de Economia

Las fuentes oficiales dijeron que hasta hoy contabilizan un apoyo de 62% de los accionistas en el FMI, 13 puntos debajo de lo necesario para que se apruebe la reducción de la sobretasa desde 3,05% a 1,05% que espera la gestión de Fernández.

En tanto, indicaron que está en discusión una tercera línea con los recursos del fondo denominado de resiliencia -cuyo fondeo podría provenir de los Derechos Especiales de Giro (DEG) que giraron a países que no los necesitan- para que haya más plazo y con un monto que sea lo más amplio posible, lo que se sumaría a los programas del Fondo de stand by y facilidades extendidas.

Más allá de los pedidos puntuales que se incluyen en la negociación, desde el Ejecutivo subrayan que está la intención de querer llegar a un acuerdo con el FMI, pero sin que implique reformas estructurales para la Argentina, algo que aclaran que el Presidente plantea a Georgieva desde el primer día en que comenzó la negociación para reestructurar la deuda.