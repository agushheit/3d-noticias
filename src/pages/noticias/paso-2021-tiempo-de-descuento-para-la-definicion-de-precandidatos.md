---
category: Agenda Ciudadana
date: 2021-07-25T06:15:27-03:00
thumbnail: https://assets.3dnoticias.com.ar/PASODOBLE.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: 'PASO 2021: tiempo de descuento para la definición de precandidatos'
title: 'PASO 2021: tiempo de descuento para la definición de precandidatos'
entradilla: Frente al cierre del plazo para oficializar las nóminas ante la justicia
  electoral, los partidos terminaron de darle forma al tablero definitivo de nombres
  para competir en las primarias.

---
Los espacios políticos con aspiraciones a competir en las Primarias Abiertas, Simultáneas y Obligatorias (PASO) del 12 de septiembre apuraron este sábado la definición de los últimos lugares en sus listas de precandidatos, frente al cierre esta medianoche del plazo para oficializar las nóminas ante la justicia electoral.

Con negociaciones de último momento, los partidos y frentes terminaron de darle forma al tablero definitivo de nombres para competir en las PASO, que se celebrarán por sexta vez en el país tras su creación en el 2009.

Tras sortear ese primer test nacional, las fuerzas políticas medirán fuerzas en las legislativas del 14 de noviembre para disputar las 127 bancas de la Cámara de Diputados y las 24 del Senado en juego en los comicios de medio término cuyo calendario se alteró por la pandemia de coronavirus.

Cuatro distritos son determinantes para sumar votos y bancas:

• Buenos Aires: 35 diputados

• CABA: 13 diputados

• Córdoba: 9 diputados, además de los senadores

• Santa Fe: 9 diputados, además de los senadores

Es decir, esas provincias aportan 66 de las 127 bancas que se renuevan en la Cámara baja.

Las miradas están puestas en las definiciones del gobernante Frente de Todos (FdT), que mantuvo el hermetismo sobre quiénes serán los elegidos para liderar las listas y motorizar la campaña hacia las PASO, que se inicia formalmente este sábado.

El oficialismo hará ese recorrido electoral con un mensaje de "unidad" para enfrentar la pandemia de coronavirus y de "esperanza" en el futuro, una vez lograda la inmunización y reactivadas en forma plena las políticas públicas demoradas por la pandemia de coronavirus.

Uno de los motores de la campaña del oficialismo será la convocatoria a un ejercicio de memoria del electorado sobre la gestión de Mauricio Macri, entre el 2015 y el 2019, que hoy está representada en la alianza opositora Juntos por el Cambio (JxC).

**Provincia de Buenos Aires**

Para anclar ese mensaje en los votantes de la provincia de Buenos Aires, la de mayor peso electoral del país, todo indicaría que el FdT pondría a la cabeza de su lista a dos dirigentes de sólida trayectoria: la presidenta del Consejo Nacional de Coordinación de Políticas Sociales, Victoria Tolosa Paz; y el ministro de Salud bonaerense, Daniel Gollan, ambos con el visto bueno del presidente Alberto Fernández y de la vicepresidenta Cristina Fernández de Kirchner.

A menos de dos años de que Cambiemos dejará el Gobierno nacional, JxC buscará enfocar su discurso de campaña en la gestión de la pandemia y en las consecuencias económicas del coronavirus, conscientes de que la gestión de Mauricio Macri quedó devaluada en la mirada de un amplio sector del electorado.

Además, la alianza opositora enfrenta internas en los distritos más grandes del país, donde pulsearán dirigentes enrolados en el PRO y otros en la UCR.

Por ejemplo, Juntos dirimirá en las PASO quiénes serán sus candidatos en la provincia de Buenos Aires, luego de la mudanza a CABA de la exgobernadora María Eugenia Vidal. En esa compulsa medirán fuerzas la propuesta macrista encabezada por el exvicejefe porteño Diego Santilli y la nómina radical que lleva al neurocientífico Facundo Manes, tras la declinación de Gustavo Posse, intendente de San Isidro, de presentar una tercera opción en esa instancia.

**CABA**

En la Ciudad de Buenos Aires, a la interna ya prevista entre Vidal y Ricardo López Murphy, se sumó en las últimas horas un grupo de radicales descontentos con el acuerdo entre Horacio Rodríguez Larreta y Martín Lousteau, que desde la agrupación "Adelante Ciudad" presentará la tercera nómina de JxC, con exponentes como el exsecretario de Salud de Macri, Adolfo Rubinstein; Facundo Suárez Lastra; Ricardo Gil Lavedra; Jesús Rodríguez y el actor Luis Brandoni, cercanos a Patricia Bullrich.

Para el desafío electoral que representa la Ciudad de Buenos Aires, administrada por Horacio Rodríguez Larreta y con buen anclaje de JxC en el electorado, el FdT tiene desde hace tiempo entre sus favoritos al asesor presidencial y legislador Leandro Santoro.

Córdoba y Santa Fe, dos de los ochos provincias que renuevan senadores junto con Tucumán, Catamarca, Corrientes, La Pampa, Mendoza y Chubut, también presentan desafíos tanto para JxC como para el FdT.

**Santa Fe**

En Santa Fe se configura una interna entre una lista del PRO, que se adjudica el apoyo del expresidente Macri y otras figuras nacionales, contra tres listas que tienen un predominio radical, en busca de renovar los 3 escaños en la Cámara de Senadores y las 9 bancas en Diputados.

El FdT, en tanto, confirmó que el ministro de Defensa, Agustín Rossi, asumirá la pelea en ese distrito por un lugar en la Cámara alta.

**Córdoba**

En Córdoba tampoco hay desenlace fácil para JxC, con tensiones entre un sector alineado con el senador porteño Martín Lousteau, que motoriza al candidato Rodrigo De Loredo (yerno del exministro Oscar Aguad); otro en el que se ubica el diputado Mario Negri y el exintendente capitalino Ramón Mestre; y un tercero de perfil macrista que tiene en sus filas al exministro de Turismo Gustavo Santos y a Luis Juez.

En tanto, el FdT resolvió el viernes que el primer puesto de la lista de diputados nacionales por Córdoba será para el secretario de Obras Públicas de la Nación e intendente en licencia de Villa María, Martín Gill, mientras que para la Cámara Alta competirán el actual senador Carlos Caserio, a quien su banca se le vence en diciembre, y la actual diputada Gabriela Estévez, dirigente de La Cámpora.

Las pujas hacia adentro de las fuerzas por los lugares llegarán a su fin esta medianoche, cuando las listas sean presentadas ante la justicia electoral y, de esta manera, se habrá iniciado el camino formal hacia las PASO, que servirá de encuesta previa a las elecciones parlamentarias del 14 de noviembre.

Las nóminas ingresarán luego a un período de 48 horas dispuesto por la ley electoral para que las respectivas juntas partidarias avalen o impugnen la conformación, por lo que podrían darse cambios de último momento antes de la instancia de revisión final de la Dirección Nacional Electoral (DINE).