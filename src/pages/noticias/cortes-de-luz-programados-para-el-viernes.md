---
category: Agenda Ciudadana
date: 2021-04-30T10:22:07Z
thumbnail: https://assets.3dnoticias.com.ar/epe.png
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: EPE
resumen: Cortes de luz programados para el viernes
title: Cortes de luz programados para el viernes
entradilla: Los cortes afectarán a la ciudades de Santa Fe y Sauce Viejo.

---
La Empresa Provincial de la Energía informa que interrumpirá el servicio de electricidad el viernes 30 en Santa Fe y Sauce Viejo, de acuerdo al siguiente detalle:

**SANTA FE**

\-De 8 a 10: Tucumán, Irigoyen Freyre, San Martín y Rivadavia.

Retiro de Línea.

**SAUCE VIEJO**

\-De 12 a 14: calle 16, Canadá, Ruta Prov. N° 11 y río Coronda.

Reemplazo de columnas de Baja Tensión.

Las interrupciones se pueden verificar en la página web de la empresa, [https://www.epe.santafe.gov.ar/reclamosweb/cortes.php](https://www.epe.santafe.gov.ar/reclamosweb/cortes.php "https://www.epe.santafe.gov.ar/reclamosweb/cortes.php")