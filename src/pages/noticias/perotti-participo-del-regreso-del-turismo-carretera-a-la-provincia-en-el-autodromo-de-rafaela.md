---
category: Estado Real
date: 2021-09-12T06:00:20-03:00
thumbnail: https://assets.3dnoticias.com.ar/AUTOMOVILISMO.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Perotti participó del regreso del turismo carretera a la provincia, en el
  autódromo de Rafaela
title: Perotti participó del regreso del turismo carretera a la provincia, en el autódromo
  de Rafaela
entradilla: " Hubo público limitado y protocolos Covid-19. “El automovilismo empieza
  a tomar su ritmo”, afirmó."

---
El gobernador Omar Perotti participó del regreso a la provincia del Turismo Carretera y el TC Pista, que este viernes 10 y sábado 11 de septiembre disputaron la 11° fecha de su calendario en el autódromo Juan R. Báscolo, de Rafaela. El escenario santafesino marcó, además, la apertura de las Copas de Oro y Plata, respectivamente.

Cabe señalar que, en el autódromo, se desplegó un operativo sanitario y de seguridad para garantizar el desarrollo de las competencias y con la presencia limitada de público.

En ese marco, Omar Perotti se refirió al reinicio de esta actividad particular “en la que tenemos que ir volviendo con los recaudos, con las pruebas que se van dando en espectáculos que van teniendo más público. Pensar en 3.000 espectadores en un escenario que ha tenido desde 60 a 100 mil personas por carrera en el Turismo Carretera, sin dudas que no es nada, pero es una primera señal para mostrar claramente que el Turismo Carretera tiene en Rafaela y en la provincia de Santa Fe, el deseo de que haya actividad y todo lo que mueve alrededor”.

En ese contexto, el gobernador de la provincia sostuvo que “el automovilismo empieza a tomar su ritmo. Hay mucho trabajo detrás de las categorías máximas y de las categorías zonales, el deporte tiene detrás de cada piloto muchas instancias de trabajo”.

Además, expresó que “el deseo es ir mostrando cada vez más eventos como estos de manera creciente. Ojalá el año que viene ya nos encuentre con mayores posibilidades de público, porque son las actividades a las que tenemos que ir volviendo. Lo vamos a hacer entre todos, con la responsabilidad de cada uno, día a día”, dijo Perotti.

Más adelante, el gobernador admitió que “este escenario se da en un buen momento de descenso de casos y de ocupación de camas críticas. Si seguimos con el avance de la vacunación, debería ir acompañando”.

Y agregó: “No tenemos que perder de vista que la cepa Delta está en el país; hasta aquí controlada, nosotros hemos tenido los primeros 10 casos que se están monitoreando. Lo fundamental para enfrentarla son los cuidados y la vacunación. Si podemos ir haciendo eso, seguramente nuestra población tendrá masivamente la colocación de su vacuna y eso nos permitirá, tener antes de fin de año y ojalá todo el año que viene, no solamente buena temporada deportiva sino de todas las otras actividades que lo están buscando: el turismo, el hotelería, la gastronomía. Sin dudas que hay expectativas muy fuertes de cada uno de ellos”, concluyó Omar Perotti.

Durante su participación en el evento, el gobernador estuvo acompañado por el intendente de Rafaela, Luis Castellano; el senador nacional, Roberto Mirabella; la secretaria de Deportes de la provincia, Florencia Molinero; el presidente de la Asociación de Corredores de Turismo Carretera (ACTC), Hugo Mazzacane; y el presidente del Club Atlético Rafaela, Silvio Fontanini.

**La carrera de los millones en Rafaela**

Seguidamente, el intendente Castellano manifestó que “en Rafaela podamos volver a tener el TC, refuerza lo simbólico y pone de nuevo la posibilidad de tener algo que nos identifica históricamente. Un esfuerzo enorme hecho por la ACTC, por el gobierno de la provincia, por el Club Atlético Rafaela para llevar adelante una competencia que es claramente distinta, pero que pone en pie hacía el futuro también de una manera muy particular. Todo se desarrolló de la mejor manera y tenemos la expectativa que el año que viene podramos abrir mucho más al público”.

Por su parte, el presidente de la Asociación de Corredores de Turismo Carretera, Hugo Mazzacane, anunció que “el próximo año Rafaela tendrá la carrera de los millones, así que agradecemos a todos y acá será esta emblemática competencia”.

Para finalizar, Silvio Fontanini entregó una placa en conmemoración de los 80 años de la primera carrera del Turismo Carretera en Rafaela y señaló que “agradecemos a las autoridades y celebramos 80 años de la primera carrera aquí”.