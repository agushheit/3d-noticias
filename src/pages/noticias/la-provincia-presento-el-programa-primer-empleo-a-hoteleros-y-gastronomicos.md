---
category: Estado Real
date: 2021-11-25T06:00:12-03:00
thumbnail: https://assets.3dnoticias.com.ar/PRIMEREMPLEO.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La provincia presentó el programa Primer Empleo a hoteleros y gastronómicos
title: La provincia presentó el programa Primer Empleo a hoteleros y gastronómicos
entradilla: "El objetivo de la iniciativa, destinada a jóvenes de entre 18 y 30 años,
  es generar mecanismos de inserción formal en el sector privado de jóvenes en situación
  de desocupación.\n\n"

---
Los ministerios de Trabajo, Empleo y Seguridad Social, y de Producción, Ciencia y Tecnología presentaron este miércoles a la Federación Empresaria Hotelera y Gastronómica de Santa Fe el programa provincial Primer Empleo, cuyo objetivo es generar mecanismos de inserción formal en el sector privado de jóvenes en situación de desocupación.

Al respecto, el subsecretario de Empleo e Inclusión Social, Eduardo Massot, precisó que “uno de los objetivos principales que ha tenido el gobierno de la provincia de Santa Fe siempre ha sido asegurar una estabilidad dentro de los sectores productivos, comerciales y de servicios; tratando de que tengan una expansión tanto en lo económico, como en el empleo; así nace Primer Empleo”.

“En síntesis, Primer Empleo es una gran ayuda económica para empresarios y una oportunidad de empelo y de trabajo para los jóvenes; una herramienta eficaz y eficiente para la generación de empleo en la provincia”, concluyó el subsecretario.

En tanto, el secretario de Comercio Interior y Servicios, Juan Marcos Aviano, detalló que “desde el Ministerio de la Producción estamos acompañando estas presentaciones porque entendemos que, si bien el programa apunta a generar nuevos puestos de trabajo en la franja joven, claramente es una acompañamiento más al sector empresario”; como son “las líneas de financiamiento crediticio, el alivio y la estabilidad fiscal, las cuestiones tarifarias; es decir todo el conjunto de medidas que el gobierno de la provincia ha venido tomando desde la pandemia y en esta salida”.

Por último, la directora Provincial de Capacitación y Formación Laboral, Valeria March, señaló que “el programa fomenta la inserción laboral para jóvenes de 18 a 30 años, donde el Estado provincial aporta una parte del salario del trabajador nuevo. Los requisitos de los jóvenes son que no hayan tenido aportes y que realmente sea el primer empleo. Esta es una ayuda económica durante seis meses de una jornada completa exceptuando personas que cuenten con certificado de discapacidad”.

“Los empleadores y futuros trabajadores deben registrarse en un portal de intermediación linkeado a la pagina del gobierno de la provincia”, finalizó la directora.

**PROGRAMA PRIMER EMPLEO**

Se trata de una política de gestión para generar mecanismos de inserción formal en el sector privado de jóvenes en situación de desocupación. Está destinado a jóvenes de entre 18 y 30 años en situación de desocupación, que residan actualmente en la provincia de Santa Fe y que nunca hayan tenido acceso a un empleo formal registrado.

Pueden anotarse todos los empleadores del sector privado que cumplan con las normativas de AFIP y que no tengan sanciones laborales en el momento de su inscripción (salvo que, teniéndolas, las hayan cancelado o tengan un convenio de pagos vigente). La provincia otorgará una ayuda económica por cada trabajador/a incorporado/da a su primer empleo, hasta un monto equivalente al 85% del salario mínimo, vital y móvil vigente.

Cada joven seleccionado realizará una experiencia de inserción laboral de entre 4 y 6 meses. Las personas con discapacidad podrán extender el plazo de duración hasta 12 meses.