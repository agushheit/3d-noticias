---
category: La Ciudad
date: 2021-12-21T06:00:27-03:00
thumbnail: https://assets.3dnoticias.com.ar/GUARDIA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Tras un finde "caótico" en el Cullen, Poletti pide responsabilidad social
  en las fiestas
title: Tras un finde "caótico" en el Cullen, Poletti pide responsabilidad social en
  las fiestas
entradilla: 'Hubo 40 ingresos del viernes al domingo sólo por accidentes: el promedio
  diario es de 12. '

---
No fue un buen fin de semana el que pasó para el Hospital José María Cullen de la ciudad de Santa Fe. Hubo 24 ingresos por accidentes de tránsito sólo este domingo. Pero en total, hubo 40 accidentados (de motos, automóviles, peatones y ciclistas) contando todo el "finde": 17 por día, cuando el promedio diario de ingresos por lesiones en el espacio vehicular es de 12 pacientes. 

 Pero además, este domingo por la mañana se hizo un "cuello de botella", con 11 ingresos a terapia intensiva, y un 100% de ocupación en camas críticas. Esto marca un antecedente que enciende las alertas a pocos días de las fiestas de Navidad y Año Nuevo. ¿Podría ocurrir una escalada de ingresos (por accidentes y lesionados por armas) que saturen las capacidades operativas del hospital local?

 El director del nosocomio, Dr. Juan Pablo Poletti, se mostró prudente. En diálogo con El Litoral, afirmó que en el Cullen "hay capacidad suficiente: nadie se queda sin atención aquí. Además, el trabajo en red con otros hospitales es permanente y está muy bien afinado".

 No va a haber un refuerzo del recurso humano para atender lo que podría dejar como saldo las fiestas. Poletti aclaró que la guardia ya está reforzada, tanto en enfermería como en personal médico: "Ese refuerzo está desde el momento más álgido del Covid". Hay cinco médicos más el Jefe de Guardia, más dos médicos en el área respiratoria Covid.

 Más allá de lo que puede pasar durante las fiestas, el médico dejó un mensaje a la sociedad, y retomó el concepto de cuidado responsable: "Es clave la responsabilidad individual de cada ciudadano. Así como tenemos la posibilidad de volver a hacer actividades relativamente normales, como ir a bares, boliches, reuniones familiares, festejos, etcétera, esto debe aprovecharse pero viviéndolo con responsabilidad", pidió.

 "Es central seguir usando el barbijo en espacios cerrados; si una reunión es al aire libre, debe mantenerse cierto distanciamiento social. Y a la hora de conducir un vehículo, quien maneje no debe haber tomado alcohol antes. Porque lamentar una víctima fatal en el marco de las fiestas es muy dramático. Todo esto es para no tener que vivir lo que está pasando en países de Europa, donde se retornó al confinamiento total", puso en contexto Poletti.

 **El saldo de estos días**

 "Este domingo último, con los ingresos por terapia intensiva, se pasaron pacientes a algunos pisos del hospital, y otros se trasladaron al Iturraspe. Y si pese a esto nos llega alguna urgencia, contamos con 5 respiradores en shock room y cuatro más en el área de sala de observación de la guardia.", director del Hospital Cullen.

 Si se saturara la guardia por los ingresos durante las fiestas, "siempre nos hemos arreglado trabajando en red con los otros hospitales", añadió el profesional. Y puntualizó que si llegan pacientes positivos de Covid-19, se derivan al Viejo Iturraspe; si escasean las camas de internación general, se derivan al Protomédico o al Mira y López; y si ingresa un paciente con obra social, se lo envía al medio privado. 

 "Cuando ya no hay lugar, se suspenden las internaciones de cirugías programadas y se internan a pacientes de urgencia", explicó. Así se ha articulado toda la estructura sanitaria para que, justamente, no colapse en sistema. Y si no colapsó con el peor momento de la pandemia, no creo que colapse para Navidad y Fin de Año", agregó Poletti.