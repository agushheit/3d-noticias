---
category: La Ciudad
date: 2021-11-28T06:00:14-03:00
thumbnail: https://assets.3dnoticias.com.ar/sain.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Desde Provincia niegan espionaje y hablan de conspiración contra el gobernador
  Perotti
title: Desde Provincia niegan espionaje y hablan de conspiración contra el gobernador
  Perotti
entradilla: 'En Casa de Gobierno buscan bajarle el tono al tembladeral por los allanamientos
  al Ministerio de Seguridad y la investigación contra Sain. '

---
“No hay crisis institucional, ni política, ni sistémica”. Con esa frase, una alta fuente de la Casa Gris salió a bajarle el precio al tembladeral que generaron los allanamientos a las oficinas del Ministerio de Seguridad en Santa Fe y Rosario, en el marco de una investigación para determinar si el ex titular de la cartera Marcelo Sain y sus principales colaboradores, que siguieron con el actual ministro Jorge Lagna hasta este viernes cuando presentaron la renuncia, formaron una estructura que se dedicó a hacer espionaje ilegal sobre dirigentes políticos, empresarios, gremialistas, funcionarios judiciales y periodistas. “Esperemos que actúe la Justicia”, sostuvo esta fuente, que remarcó que la gestión de Omar Perotti jamás dio directiva alguna para que se realicen tareas de espionaje. ¿Están seguros de que Sain no lo hizo?, preguntó La Capital. “Que hable la Justicia”, fue la respuesta.

Más allá del intento de la Casa Gris por “bajarle la espuma” al escándalo, las consecuencias judiciales y políticas son imprevisibles. Por lo pronto, Lagna fue ratificado como ministro de Seguridad a pesar que dentro del perottismo hay quienes creen que "puede ser parte del problema" y mantenía reuniones con Omar Perotti en Rosario este sábado para llenar todos los casilleros de una cartera que quedó vaciada con las renuncias de los funcionarios que trajo Sain. La idea del gobierno provincial es llegar al miércoles a la reunión convocada por Perotti con los presidentes de los bloques de la oposición en Diputados con el nuevo equipo formado casi en su totalidad, para lanzar “la nueva etapa de esta gestión en seguridad”.

Más allá de la versión más formal del gobierno provincial, el núcleo de dirigentes políticos más cercanos al gobernador se abroquelan alrededor de una idea: “Hay sectores políticos y judiciales que se quieren llevar puesto a Perotti, esto no es nuevo”.

Es decir, inscriben los allanamientos de este viernes y la causa por espionaje ilegal en el marco de la feroz disputa entre Perotti, el grupo de senadores que responde a Armando Traferri que trabaja en tándem con sectores de la oposición, e integrantes de máxima jerarquía del Poder Judicial.

“La causa es una operación de la nada. Lo del espionaje es todo verdura; no existe salvo en la fantasía mental de algunos”, dijeron fuentes muy próximas al gobernador.

Y se plantaron frente a lo que consideran la prioridad de la hora: “Nosotros vamos a seguir gobernando. Tenemos que rearmar el ministerio porque no hay más equipo”.

En rigor, ya el martes los funcionarios que llegaron a la provincia con Sain habían acordado con Perotti que iban a presentar las renuncias. La fuente consultada por La Capital dijo que los allanamientos de este viernes solo apuraron los tiempos. Lo cierto es que había una interna insostenible, que trababa la gestión en seguridad y daba señales confusas a una policía que parece no responder a los mandatos políticos, en el marco de una crisis de la seguridad pública nunca vista.

El vocero insistió en que todo es una operación sin sustento real. “Esto es muy grave: en los allanamientos no se llevaron carpetas de espionaje sino investigaciones sobre las bandas criminales. ¿Para dárselas a quién?”.