---
category: Agenda Ciudadana
date: 2021-01-15T04:28:12Z
thumbnail: https://assets.3dnoticias.com.ar/presidenteleyIVE.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: '3DNoticias - Fuente: Télam'
resumen: El Presidente promulga la ley de IVE
title: El Presidente promulga la ley de IVE
entradilla: El acto es en el Museo del Bicentenario de la Casa Rosada. El proyecto
  fue convertido en ley por el Senado el 30 de diciembre pasado.

---
El presidente Alberto Fernández aseguró que es un día de alegría porque cumplió su palabra para que se votara la ley de Interrupción Voluntaria del Embarazo, que fue parte de las promesas de su campaña electoral.

«Hoy es un día de alegría porque cumplí mi palabra», dijo el mandatario tras firmar los decretos de promulgación de las leyes de la Ley de Interrupción Voluntaria del Embarazo (IVE) y el Plan de los 1.000 Días, hito en los derechos históricos de las mujeres, en un acto que encabezó en el Museo del Bicentenario.

El mandatario aseguró que «hoy se está dando un paso importantísimo para que la sociedad sea un poco más igualitaria para las mujeres» y dijo que estaba muy feliz de terminar con el patriarcado.