---
category: Agenda Ciudadana
date: 2021-11-11T06:15:01-03:00
thumbnail: https://assets.3dnoticias.com.ar/aire.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: LT10
resumen: Los aires acondicionados cuestan casi el doble que en el 2020
title: Los aires acondicionados cuestan casi el doble que en el 2020
entradilla: Así se refirió el representante de la Cámara de Artefactos del Hogar de
  Santa Fe. También aseguró que los santafesinos prefieren planes de pago en cuotas
  de hasta 12 meses.

---
A medida que se acerca el verano y comienza a hacerse sentir el calor, se hacen más frecuentes las consultas por equipos de aire acondicionados.

José Lilino, presidente de la Cámara de Artefactos del Hogar de Santa Fe explicó al Móvil de LT10 que se trata de un producto que "está casi al doble de lo que estaba el año pasado", y esto se debe, sobre todo a factores que influyeron impositivamente, como ser los impuestos a lo electrónico en el sur del país, donde se encuentran las plantas.

Por otro lado, se refirió a la forma en la que los santafesinos prefieren comprar. "A la gente no le picó tanto el concepto de las cuotas que sean más de 12, el 60 o 70% se hace con el Ahora 12, un poco con Ahora 18 y casi nada con lo que es el ahora 24 y 30" detalló.