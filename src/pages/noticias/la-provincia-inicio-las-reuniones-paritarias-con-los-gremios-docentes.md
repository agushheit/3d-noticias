---
category: Estado Real
date: 2021-02-06T07:19:56-03:00
thumbnail: https://assets.3dnoticias.com.ar/gremios.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: La provincia inició las reuniones paritarias con los gremios docentes
title: La provincia inició las reuniones paritarias con los gremios docentes
entradilla: Junto a representantes de AMSAFE, SADOP, UDA y AMET se abordaron cuestiones
  salariales y las condiciones de trabajo que posibiliten retomar la presencialidad
  en las escuelas.

---
Los ministros de Educación, Adriana Cantero, y de Trabajo, Empleo y Seguridad Social, Juan Manuel Pusineri iniciaron este viernes el diálogo paritario con los representantes de los sindicatos docentes de AMSAFE, SADOP, UDA y AMET.

En la oportunidad, se acordó la constitución de tres comisiones técnicas que trabajarán durante la semana entrante en cuestiones referidas al reinicio de las clases presenciales y a las constitución de comités mixtos y concursos; y una tercera abocada a aspectos salariales.

Al respecto, Pusineri destacó que “el gobierno provincial continúa cumpliendo con los compromisos asumidos oportunamente; entendiendo que la promoción del diálogo social y de la negociación colectiva es el mejor instrumento para avanzar en los acuerdos entre todas las partes involucradas”.

Asimismo, subrayó que “el Ministerio de Educación ya ofreció 542 cargos en escuelas secundarias y se apresta a ofrecer 40.000 horas cátedra en las próximas semanas, lo cual demuestra que lo que se ha planificado se está implementando poco a poco”.

Respecto de la política salarial, Pusineri aspira a que en el encuentro del viernes próximo puedan contar con algunos términos de referencia originados en la paritaria nacional docente, que empieza a reunirse en los próximos días, y en el acuerdo sobre precios y salarios que está siendo promovido desde el gabinete económico nacional, con la participación de trabajadores y empresarios.

**Revisión de concursos docentes**

Consultado sobre la revisión de concursos docentes, Pusineri recordó que el Tribunal de Cuentas de la provincia ha observado un trámite con serias irregularidades en su confección. Las titularizaciones identificadas como irregulares han sido dadas de baja, los docentes han vuelto a su condición de interinos, pero sin que su salario se vea afectado. “Nuestra aspiración es que los concursos vuelvan a sustanciarse dentro del marco legal existente a fin de que no queden dudas acerca de la legalidad de estos procesos”, concluyó el funcionario.