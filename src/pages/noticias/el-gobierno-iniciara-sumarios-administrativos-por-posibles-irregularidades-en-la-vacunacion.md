---
category: Agenda Ciudadana
date: 2021-04-03T06:00:14-03:00
thumbnail: https://assets.3dnoticias.com.ar/VACUNACIONGATE.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: El gobierno iniciará sumarios administrativos por posibles irregularidades
  en la vacunación
title: El gobierno iniciará sumarios administrativos por posibles irregularidades
  en la vacunación
entradilla: El gobernador Perotti decidió abrir sumarios administrativos a funcionarios
  de Recursos Humanos y de Administración del ministerio de Educación involucrados
  el proceso de vacunación.

---
El gobierno de la provincia de Santa Fe ratificó todo el proceso operativo de vacunación contra el Covid 19 que comenzó con el suministro de la primera dosis de la vacuna a los grupos esenciales del servicio de salud y del ámbito docente, de acuerdo con el cronograma establecido.

De este modo, ya fueron vacunados todos los profesionales del área de salud que se encuentran en la primera línea de atención, la gran mayoría de los docentes y se está culminando con la vacunación de los grupos más vulnerables: adultos mayores de 80 años, residencias geriátricas y residencias de personas con discapacidad.

Ante la versión periodística de un posible error en el suministro de la vacuna contra el Covid a trabajadores del Ministerio de Cultura de la provincia, delegación Rosario, el gobernador expresó: “No vamos a tolerar ningún error. Hay un objetivo claro y que nadie pude salirse del mismo que es la vacunación ordenada por prioridades. Los responsables de una acción en contra de los objetivos que he propuesto, deberán afrontar las sanciones que le correspondan” indicó Perotti.

En este sentido, desde el gobierno de la provincia se informó que el chequeo de los docentes anotados que podían acceder al proceso de vacunación, fue realizado por el Ministerio de Educación, que suministró ese registro de docentes, no docentes y asistentes escolares al Ministerio de Salud para que sea cruzado con los datos obrantes en su circuito de información.

Frente a la aparición de versiones periodísticas señalando anomalías en la confección del registro de solicitantes y la existencia de otras dependencias públicas involucradas en el proceso de vacunación, la Provincia ratificó que ninguna otra repartición o ministerio estatal provincial generó un listado para vacunar a agentes pertenecientes al ámbito educativo.

Es importante señalar que existen docentes de educación artística que están vinculados al Ministerio de Cultura. Por su función de docencia en las aulas y en proximidad con los alumnos, le corresponde estar vacunados. Quien no tiene horas cátedra, no puede ni debe estar en el listado enviado por funcionarios del Ministerio de Educación.

Al mismo tiempo, desde el gobierno de la provincia se hizo un llamado a la responsabilidad de todos. Si un agente sabe fehacientemente que no le corresponde el turno y es llamado a vacunarse, debe ejercer la responsabilidad cívica que implica su función en el Estado y reportar este hecho. Es importante evitar situaciones donde un agente estatal se aplica una dosis aprovechándose de una circunstancia anómala para luego darle publicidad en las redes sociales a ese acto. El episodio se transforma en una conducta cínica e irresponsable.

**Sumarios administrativos**

El gobernador Omar Perotti ha decidido abrir un sumario administrativo a los funcionarios de las áreas de Recursos Humanos y de Administración del Ministerio de Educación para investigar primero, si existieron o no posibles situaciones anómalas en el proceso de vacunación y luego, determinar eventuales responsabilidades en caso de que hubieren ocurrido en el marco del actual dispositivo sanitario.

El mismo gobernador, ratificó “que no hay espacio para cometer errores o negligencias de ningún tipo, desde ningún ministerio, en medio del más trascendente proceso de vacunación masiva que viene realizando nuestro país y que en la provincia, tiene un desarrollo sin inconvenientes y así continuará”.

Perotti expresó que “si hubiera alguna situación para corregir, se hará con la presteza y la velocidad que requiere la situación. Pero que ni en mi caso particular (quien podría recibir su dosis) como ningún ministro o funcionario va a acceder a la vacunación, hasta tanto la situación sanitaria lo indique y respetando todos los protocolos obrantes”.