---
category: Estado Real
date: 2021-04-05T06:00:48-03:00
thumbnail: https://assets.3dnoticias.com.ar/PEROTTI.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Covid-19: el Gobernador Perotti junto a los ministros Martorano y Corach
  se reunieron con intendentes y presidentes comunales'
title: 'Covid-19: el Gobernador Perotti junto a los ministros Martorano y Corach se
  reunieron con intendentes y presidentes comunales'
entradilla: El encuentro se desarrolló de forma virtual y tuvo como objetivo analizar
  la situación en el territorio provincial y coordinar acciones de prevención.

---
El gobernador Omar Perotti, junto a la ministra de Salud Sonia Martorano y, de Gestión Pública, Marcos Corach, mantuvieron una reunión virtual con intendentes y presidentes comunales santafesinos para analizar la situación del Covid-19 en el territorio provincial y coordinar acciones.

El encuentro se desarrolló vía Zoom, donde las autoridades de la provincia les solicitaron a los jefes territoriales multiplicar los esfuerzos de prevención y redoblar el compromiso y la responsabilidad frente a la segunda ola del virus, para evitar la posibilidad de volver a las restricciones y tener que tomar medidas drásticas.

Durante el encuentro, también dialogaron acerca de las nuevas cepas que circulan en el territorio provincial y la llegada de más vacunas en los próximos días.