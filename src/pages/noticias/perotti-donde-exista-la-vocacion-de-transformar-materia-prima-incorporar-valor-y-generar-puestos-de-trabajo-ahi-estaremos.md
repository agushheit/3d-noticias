---
category: Estado Real
date: 2021-12-22T06:00:43-03:00
thumbnail: https://assets.3dnoticias.com.ar/materiaprima.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Perotti: "Donde exista la vocación de transformar materia prima, incorporar
  valor y generar puestos de trabajo, ahí estaremos"'
title: 'Perotti: "Donde exista la vocación de transformar materia prima, incorporar
  valor y generar puestos de trabajo, ahí estaremos"'
entradilla: Lo aseguró esta mañana el gobernador de la provincia, quien participó
  de la inauguración de una nueva planta de acopio de Molino Chabás SA, ubicado en
  esa localidad del departamento Caseros.

---
El gobernador Omar Perotti participó de la inauguración de la nueva planta de acopio de Molino Chabás SA, en esa localidad del departamento Caseros. Allí, la empresa realizó una inversión de 120.000.000 de pesos para el montaje de tres silos metálicos, con una capacidad total de 15.000 toneladas de trigo. La obra fue financiada con créditos del Banco de la Nación.

En la oportunidad, el gobernador de la provincia destacó la importancia de “este molino que sigue agregando valor a lo que produce nuestra tierra”, y agradeció a la firma: “Es un orgullo que haya empresarios como ustedes, porque contagian, entusiasman a otros y marcan el sendero. Siempre van a contar con el acompañamiento de este gobernador. Siempre a la par del que trabaja, del que invierte y del que produce. Ese es el camino para poner de pie a esta provincia”, afirmó el mandatario santafesino.

“En cada lugar donde exista la vocación y el deseo de transformar materia prima, incorporar valor y generar puestos de trabajo, allí estamos”, señaló Omar Perotti, quien agregó que “queremos arraigar, generar equilibrio poblacional en esta provincia y que cada uno sienta que hay futuro en el lugar donde nació”.

A continuación, el gobernador remarcó que “confiamos que esta provincia puede hacerlo; que esta etapa de desarrollo que estamos encarando es la que tiene que consolidar un sendero de crecimiento. Estoy plenamente convencido que tenemos un horizonte enorme en el sector agroalimentario, en el sector agroindustrial, en toda la cadena de proveedores que la integran y eso seguramente se va a plasmar y muy fuerte en el 2022”.

Sobre el final, destacó que “el trabajo es un ordenador social; no hay mejor política social que un puesto de trabajo. Para enfrentar momentos duros, una señal como esta marca el camino por donde tenemos que transitar”, manifestó.  
  
**Seguir invirtiendo**  
Por su parte, el presidente de Molino Chabás SA, Arturo Marasca, agradeció “al gobierno provincial y al Banco Nación por haber generado una herramienta clave para que nosotros hoy estemos inaugurando esta nueva planta de acopio. En momentos difíciles es cuando más se valora este tipo de herramientas que pusieron a disposición de muchas empresas de la provincia, generando efecto cascada, porque cinco empresas santafesinas participaron de este proyecto. Con este tipo de políticas dan ganas de seguir invirtiendo”.  
  
**PRESENTES**  
Participaron del acto el senador por el departamento Caseros, Eduardo Roscono; y la ex senadora nacional María de los Ángeles Sacnun.