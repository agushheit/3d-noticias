---
category: Agenda Ciudadana
date: 2020-12-02T10:51:49Z
thumbnail: https://assets.3dnoticias.com.ar/COLECTIVOS.png
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: El Litoral'
resumen: 'Vuelven los colectivos: Santa Fe-Rosario y otras localidades'
title: 'Vuelven los colectivos: Santa Fe-Rosario y otras localidades'
entradilla: Este miércoles volverán los primeros viajes entre Rosario y Santa Fe.
  Por el momento llevarán sólo a personas esenciales y exceptuadas.

---
Más de ocho meses pasaron las Terminales de Ómnibus de Rosario “Mariano Moreno” y de Santa Fe “Manuel Belgrano” sin recibir ni remitir viajes por la pandemia del coronavirus, y este miércoles volverán finalmente los primeros viajes entre las dos ciudades y algunas otras de media distancia de la provincia, que por el momento llevarán sólo a personas esenciales y exceptuadas.

Las dos empresas que vuelven con su flota son Micro y Tata Rápido, y aunque ya se comenzó la venta de pasajes para esenciales y exceptuados con el certificado habilitante, fue tranquilo el movimiento en las primeras horas de ventas.

Desde este martes se empezó a vender pasajes en ambas paradas de referencia, así como en las páginas web, y comienza una nueva etapa que además se espera que de un respiro paulatino a todos los comercios que funcionan dentro de ambos predios.

Por otra parte, la Costa Argentina abrió su temporada y se comenzaron a vender pasajes en empresas de larga distancia, aunque sólo para viajar desde el 1° de enero. Ya abrieron ventanilla Zenit y Flecha Bus, pero con los días se espera la aprobación del protocolo para que comiencen a vender de forma presencial el resto de las empresas.

El gerente de la Terminal de Rosario Héctor Peiró manifestó que se espera que “este sea el comienzo tan ansiado del movimiento en la Terminal y que se sigan sumando servicios”.

## **Cómo será el regreso de los viajes**

En diálogos con medios televisivos, el referente de la Terminal de Rosario precisó que al momento los viajes serán de unas 12 flotas por día, todas al 50 por ciento de su capacidad. El primer coche sale a las seis de la mañana desde Rosario, y el último arriba a las 22.30.

En cada unidad habrá alcohol en gel a disposición, el servicio deberá controlar el uso constante del tapabocas, garantizar que se tome la fiebre al ingresar a cada pasajero, y el ascenso y descenso debe realizarse manteniendo la distancia de seguridad. Igual criterio deberá aplicarse para retirar las valijas de las bauleras.

En la Terminal por su parte se seguirá un protocolo de distanciamiento tanto en grupos de personas como en los bares y en los asientos. Y adelantó que “se buscará que solamente se acerquen los pasajeros, sin acompañantes, y que en las plataformas sólo se circule cuando arriban o toman un colectivo”.

Para viajar “cada pasajero hará una declaración jurada en la app Covid de la Provincia, donde aclara el motivo del viaje y que es asintomático, y hacer otra declaración jurada a la hora de comprar el pasaje”, precisó Peiró.

Los pasajeros deberán presentar el Certificado Único Habilitante para la Circulación-Emergencia Covid-19 o el certificado mediante la aplicación Cuidar y/o el instrumento sustitutivo o complementario que la normativa vigente requiera.