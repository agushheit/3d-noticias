---
category: Estado Real
date: 2021-05-20T09:18:37-03:00
thumbnail: https://assets.3dnoticias.com.ar/corach.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: Desde la provincia insisten con que debería suspenderse el fútbol para evitar
  aglomeraciones
title: Desde la provincia insisten con que debería suspenderse el fútbol para evitar
  aglomeraciones
entradilla: El ministro de Gestión Pública de Santa Fe, Marcos Corach, sostuvo hoy
  que "el fútbol debería suspenderse", debido a que no se puede controlar a quienes
  se reúnen para ver los partidos

---
El ministro de Gestión Pública de Santa Fe, Marcos Corach, sostuvo hoy que "el fútbol debería suspenderse", debido a que no se puede controlar a quienes se reúnen para ver los partidos y porque "el ejemplo no es bueno" por la cantidad de contagios de coronavirus que se registran en los planteles profesionales.

El funcionario comparó la situación del deporte con la cuestión educativa y en ese sentido añadió que "es difícil decir: 'tengo que suspender las clases y el fútbol sigue'", al referirse a las medidas restrictivas que el Gobierno santafesino anunció a partir de mañana, como consecuencia del crecimiento de contagios y muertes.

En diálogo con radio Universidad de Santa Fe, Corach contó que desde la administración provincial hicieron "todos los esfuerzos" para lograr que las cadenas televisivas transmitan los partidos de la Liga Profesional de manera liberada, pero sin resultados.

Luego dijo que "echarle la culpa al fútbol" por la situación sanitaria sería buscar "un chivo expiatorio", pero advirtió que "si se juntan en una casa a ver los partidos, es difícil que alguien pueda controlar a esas personas".

"Cuando se producen esas aglomeraciones por el fútbol, la verdad es que deberían suspenderlo porque nos genera esta situación, entre otras cosas, en la que nos obliga a tomar medidas muy restrictivas", agregó.

"Además, el ejemplo no es bueno. Uno ve la cantidad de contagiados en los planteles y la verdad es que debería suspenderse", completó.