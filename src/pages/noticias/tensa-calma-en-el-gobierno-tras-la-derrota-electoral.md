---
category: Agenda Ciudadana
date: 2021-11-21T06:00:12-03:00
thumbnail: https://assets.3dnoticias.com.ar/ALBERTAAAA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: Tensa calma en el Gobierno tras la derrota electoral
title: Tensa calma en el Gobierno tras la derrota electoral
entradilla: El presidente Fernández marcó la agenda política durante la semana posterior
  a las elecciones, descolocó a la oposición y desafío a Cristina con un encendido
  discurso en la Plaza de Mayo.

---
Una singular expectativa había generado en la opinión pública antes de las elecciones de medio término la reacción que podría ensayar el Gobierno tras los comicios, luego de la crisis política desatada en la Casa Rosada con motivo del revés en las PASO.

Se comentó mucho en medios periodísticos sobre el "día después", con elucubraciones diversas dependiendo del resultado, y si bien el oficialismo perdió el pasado domingo 14 de noviembre, el veredicto popular que arrojó la convocatoria a las urnas luego de los dos primeros años de gestión de Alberto Fernández dejó en definitiva un sabor agridulce en Balcarce 50.

En la Casa de Gobierno, si bien admiten que haber perdido el quorum propio en el Senado "es un problema", destacan lógicamente la remontada del Frente de Todos (FdT) en la provincia de Buenos Aires, donde el oficialismo terminó sumando la misma cantidad de diputados nacional que Juntos, un logro fundamental para mantenerse como primera minoría en la Cámara baja después del 10 de diciembre venidero.

En este contexto, y más allá de las distintas "nuevas definiciones" de la palabra "triunfo" que intentó Fernández en los últimos días, el jefe de Estado, con la carta disruptiva que jugó inmediatamente después de los comicios a pesar de la derrota, descolocó a la oposición, marcó la agenda de la semana y le restó protagonismo a Juntos por el Cambio (JxC).

El presidente convocó primero a una movilización a Plaza de Mayo para "festejar" el resultado electoral y luego celebró allí ante una multitud el Día de la Militancia peronista, con un encendido discurso teledirigido directamente hacia el interior de la coalición gobernante, más allá de su intento de relanzar su gestión y dejar traslucir, incluso, sus intenciones de competir por la reelección en 2023.

Esta postura mostrada por el jefe de Estado, como primer acto reflejo, como primera reacción tras las elecciones, abrió una serie de escenarios sumamente interesantes tanto en el seno del oficialismo como en la oposición, sobre todo en JxC, con Patricia Bullrich y María Eugenia Vidal discutiendo en los últimos días, por ejemplo, sobre la dimensión de la victoria en la ciudad de Buenos Aires y si podría haber sido más amplia.

Además, la remontada del FdT en la Provincia, sumada al posterior cuestionamiento de Bullrich acerca del desempeño de Vidal en la Capital Federal, diluyó en parte la euforia que existía en el entorno del jefe de Gobierno porteño, Horacio Rodríguez Larreta, después de las PASO con relación a su probable candidatura presidencial dentro de dos años.

Por otra parte, el sólido rendimiento electoral de JxC en el interior del país, en especial en regiones cardinales en lo que se refiere al andamiaje productivo nacional, envalentonó a dirigentes provinciales de ese espacio, con gobernadores y referentes del radicalismo encabezando esa nómina, con vistas a la contienda por la primera magistratura en 2023.

Mientras, en el seno del Gobierno reinó en los últimos días una tensa calma después de las elecciones, en tanto crece la expectativa sobre cómo seguirá adelante la relación entre el jefe de Estado y la vicepresidenta Cristina Fernández de Kirchner, que se muestra de momento corrida del centro de la escena política luego de los comicios.

**Fernández busca robustecer su capital político**

Incluso podría podría interpretarse perfectamente que durante un tramo de su discurso en Plaza de Mayo Fernández desafío a la líder del FdT cuando reclamó que todas las candidaturas del oficialismo para los comicios de 2023 se resuelvan en primarias y no mediante una designación a dedo, con Cristina como dueña de la última palabra por lo general.

Esta semana, el presidente buscó mostrarse fortalecido a pesar de la derrota o al menos respaldado por el peronismo unido luego del traspié en las urnas, en un intento por comenzar a robustecer su capital político: el primer paso en tal sentido claramente logró darlo, al marcar la agenda en la primera semana posterior a los comicios y enervar a JxC con su postura -deliberada- de no reconocer la derrota.

Fernández, que terminó con disfonía tras su discurso en Plaza de Mayo, enfocó la mirada en 2023, aunque está claro que existen en el país necesidades y demandas sociales que requieren respuestas inmediatas, por no decir urgentes, de parte del Gobierno, sobre todo después de la pandemia de coronavirus que tanto afectó a la Argentina.

En este sentido, alcanza con echar un vistazo a la nueva configuración del mapa político nacional para intentar comprender qué se votó en cada región del país y por qué: solo en la subsidiada tercera sección electoral bonaerense y en provincias del norte de la Argentina con abundante empleo público logró prevalecer con claridad el FdT por sobre la oposición.

Más de cuatro de cada 10 personas que participaron en los comicios del 14 de noviembre último respaldaron a JxC en todo el país y casi siete de cada 10 votaron en contra el Gobierno, que indudablemente deberá esforzarse -y dejar de cometer errores- en los próximos dos años para reconciliarse con el electorado, en especial, con aquellos ciudadanos que confiaron en el proyecto que promocionaban "Alberto y Cristina" en 2019.

Si bien el veredicto de las urnas, en esta ocasión, no generó la crisis que desató el tropezón del oficialismo en las Primarias Abiertas, Simultáneas y Obligatorias (PASO) del 12 de septiembre pasado, un signo de interrogación pende sobre la relación que puedan mantener de ahora en más Fernández con el núcleo duro kirchnerista del Gobierno, que persigue sus propias ambiciones políticas de cara a 2023.

Finalmente, por el lado de la oposición, sería prudente que JxC, más allá del triunfo electoral y el golpe de escena generado en el Senado, comprenda que un número significativo del electorado votó "en contra del Gobierno" utilizando las boletas de esa coalición, lo que no significa un respaldo deliberado al espacio que encabezan Mauricio Macri, Rodríguez Larreta y demás pensando en las próximas elecciones presidenciales.

Es cierto, un triunfo -pese a que el oficialismo no lo reconozca- en todo el país en unas elecciones legislativas por más de 8 puntos de diferencia sobre el peronismo unido es un acontecimiento histórico, pero el Gobierno aún tiene por delante dos años para torcer el rumbo de la crisis y, desde la gestión, tratar de llegar más robustecido a 2023: ¿con o sin Cristina como principal espalda política del FdT? Eso aún está por verse.