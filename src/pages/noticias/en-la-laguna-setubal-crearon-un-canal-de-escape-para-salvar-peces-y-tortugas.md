---
category: La Ciudad
date: 2022-01-09T06:00:05-03:00
thumbnail: https://assets.3dnoticias.com.ar/canal.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: En la laguna Setúbal crearon un canal de escape para salvar peces y tortugas
title: En la laguna Setúbal crearon un canal de escape para salvar peces y tortugas
entradilla: "Para que puedan nadar y conectarse al cauce principal, un grupo de ambientalistas
  independientes construyó una zanja para que puedan sobrevivir. \n\n"

---
Una de las consecuencias que apareja la bajante extrema del río Paraná, que se profundiza cada vez más, es la desconexión que se genera en los diferentes espejos de agua, uno de ellos es la laguna Setúbal. En este escenario adverso, las diferentes especies de peces y demás fauna acuática como las tortugas y los cangrejos, quedan aisladas de forma repentina de los canales principales y les impide seguir su curso natural hacia otros puntos de la gran cuenca.

Un grupo de ambientalistas independientes de la ciudad de Santa Fe observó una situación particular en un sector de la Setúbal, en inmediaciones al Espigón I. "Desde mi parador vi que había una ´lengua\` de agua que estaba desconectada del cauce principal de la laguna. Al acercarme observé muchas especies de peces atrapadas en un ´charquito\` de agua, donde la zona más profunda me llegaba solo al tobillo", comentó Javier Maillo, guardavida y dueño de un parador en el Espigón 1.

En el lugar encontró rayas, tortugas, cangrejos y otras especies ictícolas sin poder escapar de lo que se había transformado en una trampa mortal, por la poca oxigenación. "Un día agarré una pala que tenía a mano y arranqué a construir el canal de escape", comentó y agregó que enseguida dio aviso al grupo de ambientalistas independientes del que forma parte y en los días sucesivos se acercaron Noelia Segovia, Gabriela Lescano, Veronica Lobo y a medida que se dio difusión más colaboradores pusieron manos a la obra. De esa forma y con solo la utilización de palas, le dieron forma a la zanja que tiene unos 150 metros de largo.

"Me pareció una situación cruel"

\-¿Cuál fue tu primera impresión al ver a los peces en esa situación?

\-Me pareció una situación bastante cruel, porque las especies se estaban muriendo asfixiadas y nosotros teníamos la posibilidad de hacer algo. Además esto es ocasionado gracias al impacto del hombre sobre el ambiente y muchas veces lo tomamos como algo lejano que leímos, escuchamos, pero ahora el impacto de la bajante la tenemos frente a nosotros.

Con esta bajante, los peces están expuestos a una depredación constante y la verdad que al día de hoy no entiendo cómo tengamos peces en el río, ya que no se les da tiempo a la reproducción.

\-¿Qué pasó cuando el canal quedó terminado?

\-Cuando empezamos a hacer el canal el 23 de diciembre tuvimos nuestras dudas. Como ambientalista uno trata de no meterse en los tiempos de la naturaleza. El canal comenzó a tener un tránsito constante de peces que salían, sobre todo durante la noche, y otros que se refugiaban. Con las chicas nos quedábamos hasta la medianoche trabajando y en esos horarios nocturnos constatamos el mayor flujo de peces hacia el cauce principal.

\-¿Consultaron a especialistas para saber si era correcto lo que hacían?

\-Sí. En el tema se interesaron muchos especialistas, entre ellos biólogos, quienes nos indicaron que lo que estábamos haciendo era correcto, es decir que tenemos el aval técnico y profesional, porque al principio no sabíamos si estaba bien. Nos dijeron que estábamos contribuyendo a que las especies puedan tener su escape natural.

Nosotros en un principio pensamos en una acción más directa y hacer como un "arriado" o una pesca de rescate, pero veterinarios y biólogos contraindicaron esta acción porque de esa forma someteríamos a un estrés innecesario a estos animales que son salvajes y que con el afán de salvarlos, le podíamos ocasionar la muerte.

\-¿Qué sigue ahora?

\-Desde el 23 de diciembre hasta hoy, el canal sigue con mantenimiento. La siguiente etapa es seguir supervisando la laguna Setúbal porque sigue bajando. Notamos también que hay un crecimiento de algas (podría tratarse de cianobacterias) que son peligrosas porque evitan la oxigenación del agua.

Ahora haremos una evaluación junto a especialistas sobre lo que hicimos y del impacto que tuvo, y veremos si tomamos una acción más directa o seguir de la misma forma.