---
category: Agenda Ciudadana
date: 2000-01-01T06:10:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/boleto.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Comienza a regir el Boleto Educativo Gratuito en toda la provincia de Santa
  Fe
title: Comienza a regir el Boleto Educativo Gratuito en toda la provincia de Santa
  Fe
entradilla: 'De los registrados, 103 mil son estudiantes, 22.500 son docentes y 5500
  son asistentes escolares. Además, 74 mil corresponden al transporte urbano y 19
  mil al interurbano. La inscripción permanecerá abierta. '

---
Desde este lunes 15 de marzo entrará en vigencia el sistema de Boleto Educativo Gratuito en toda la provincia de Santa Fe, programa lanzado por el gobierno de Omar Perotti y que tiene el beneficio de pasajes gratuitos para estudiantes de todos los niveles, docentes y asistentes escolares, para permitir la concurrencia a los establecimientos de todo el territorio, y que coincide con el regreso a las clases presenciales

Según lo analizado, del total de las 131.000 solicitudes, 74.000 corresponden al sistema de transporte urbano, 19.000 corresponden al sistema interurbano y 38.000 de ambos sistemas. El BEG impactará directamente en la economía de todos los usuarios, lo que significa un gran alivio para las familias, ya que el ahorro en pasajes es significativo.