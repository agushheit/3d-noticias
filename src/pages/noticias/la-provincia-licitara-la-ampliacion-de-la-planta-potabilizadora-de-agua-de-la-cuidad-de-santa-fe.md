---
category: Estado Real
date: 2021-10-19T06:15:51-03:00
thumbnail: https://assets.3dnoticias.com.ar/assa.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La provincia licitará la ampliación de la planta potabilizadora de agua de
  la cuidad de Santa Fe
title: La provincia licitará la ampliación de la planta potabilizadora de agua de
  la cuidad de Santa Fe
entradilla: Será este martes en un acto encabezado por el gobernador Omar Perotti.
  Las obras permitirán incrementar un 75 por ciento la capacidad de potabilización.

---
El gobernador Omar Perotti encabezará este martes 19 de octubre desde las 10 horas el acto de apertura de las ofertas económicas en el marco de la licitación pública para la ampliación de la planta potabilizadora de agua de la ciudad de Santa Fe. Será en la sede de Aguas Santafesinas S.A en calle Ituzaingó 1500.

Las obras permitirán incrementar un 75 por ciento la capacidad de potabilización; mejorando el suministro en varias zonas de la capital provincial y acompañando el crecimiento de la ciudad hasta el año 2050.

El proyecto elaborado, por el gobierno provincial a través de Aguas Santafesinas, cuenta con un presupuesto de 4.420 millones de pesos, con fondos provenientes del Estado nacional a través del Ente de Obras Hídricas de Saneamiento (ENOHSA) y tendrá un plazo de ejecución de 24 meses.

Los trabajos incluyen la construcción de una nueva toma de captación de agua cruda, nuevos módulos de potabilización de alta tasa, diferentes reformas y mejoras operativas en sectores de producción, sistema de tratamiento de lodos y descarga de efluentes, y un nuevo laboratorio de calidad.