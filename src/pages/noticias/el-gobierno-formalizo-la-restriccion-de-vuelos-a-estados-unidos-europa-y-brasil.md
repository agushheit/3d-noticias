---
category: Agenda Ciudadana
date: 2021-01-28T10:33:24Z
thumbnail: https://assets.3dnoticias.com.ar/vuelos.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Con información de Diario Uno
resumen: El Gobierno formalizó la restricción de vuelos a Estados Unidos, Europa y
  Brasil
title: El Gobierno formalizó la restricción de vuelos a Estados Unidos, Europa y Brasil
entradilla: La medida entró en vigencia este miércoles, aunque su implementación efectiva,
  debido a las reprogramaciones de los destinos, sería a partir del primer día de
  febrero

---
El Gobierno formalizó la reducción y restricción de la frecuencia de vuelos de entre 30 y 50% hacia y desde destinos de Estados Unidos, Europa, México y Brasil, debido al rebrote de coronavirus que se da en esos países.

La medida, que había sido anticipada ya hace más de un mes por el ministro de Transporte, Mario Meoni, fue formalizada mediante una carta que la Administración Nacional de Aviación Civil (ANAC) le envió a las aerolíneas, por pedido del Ministerio de Salud.

Según indicaron fuentes aeronáuticas, la reducción de los vuelos es meramente un hecho formal, dado que, en algunos casos, debido a la pandemia, ya se había experimentado una reducción de los servicios y en otros, por decisión, tanto de las autoridades sanitarias locales, como las de algunos países de Europa, se habían suspendido los vuelos como consecuencia de los rebrotes.

De acuerdo con la información que les llegó a las aerolíneas, desde y hacia Estados Unidos, Europa y México, la merma de los servicios será del 30 por ciento, en tanto que para los vuelos regionales con Brasil, la reducción alcanzará el 50 por ciento.

La medida entró en vigencia este miércoles 27, aunque su implementación efectiva, debido a las reprogramaciones de los servicios, sería a partir del lunes primero de febrero.

Los tres primeros destinos la disminución de frecuencias deberá ser del 30%; mientras que el intercambio de vuelos aerocomerciales con el país vecino sufrirá una restricción del 50%.

En el marco de una visita a Bahía Blanca, Meoni dijo que "el objetivo es disminuir las posibilidades de ingreso de las nuevas cepas al país, permitiendo un control y seguimiento exacto de la cantidad de pasajeros por día".

"Hoy ingresan a país más de 3.000 pasajeros diarios, con esto reduciríamos de manera importante esa cantidad, estaríamos en el orden de los 2.000 pasajeros por día de los lugares que tenemos cepas nuevas", agregó.

Meoni sostuvo que eso "nos permite que en el acceso en el aeropuerto de Ezeiza puedan tener control el 100 por ciento de las personas que entran a través de los análisis de PCR y de esa manera poder garantizarnos el no ingreso de la nueva cepa".

"O que si ingresa alguien enfermo se pueda darle el aislamiento correcto para que no transmita esa nueva cepa hacia otros sectores", dijo al reiterar que es una "decisión del Ministerio de Salud que nosotros adoptamos inmediatamente".

La decisión impactará sobre aquellos viajeros que ya tenían sus pasajes adquiridos y que ahora deberán coordinar con la aerolínea su modificación o solicitar la devolución del importe de los mismos.