---
category: Agenda Ciudadana
date: 2021-12-01T06:00:17-03:00
thumbnail: https://assets.3dnoticias.com.ar/DIPUTADOS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: Sigue abierta la puja por la presidencia del interbloque de Juntos por el
  Cambio en Diputados
title: Sigue abierta la puja por la presidencia del interbloque de Juntos por el Cambio
  en Diputados
entradilla: 'Negri tendría las firmas suficientes para continuar al frente de la bancada
  radical y además cuenta con el respaldo de la Coalición Cívica, pero el PRO tiene
  la bancada mayoritaria dentro de JxC.

'

---
Pese a las tensiones internas, las distintas tribus que conforman Juntos por el Cambio mantendrán la unidad en la Cámara de Diputados a partir del inminente recambio legislativo y el interbloque sumará 116 integrantes, uno más que los que tenía hasta ahora.

Sin embargo, la fisonomía y composición interna será diferente, ya que se especula con desprendimientos en las bancadas mayoritarias, especialmente en la UCR, en tanto que el ex presidente de la Cámara de Diputados durante el Gobierno de Cambiemos, Emilio Monzó (un ex PRO) conformará un bloque de cuatro representantes que tendrá también a Margarita Stolbizer, Sebastián García de Luca y Domingo Amaya.

Los dos últimos, con mandato hasta fines de 2023, eran hasta ahora parte del bloque del PRO comandado por Cristian Ritondo (quien seguirá como presidente de la bancada amarilla), por lo que configuran las dos primeras bajas del bloque que tendrá, desde el 10 de diciembre, 50 integrantes (frente a los 53 que tiene hasta esa fecha).

Con todo, seguirá siendo la bancada mayoritaria dentro de la principal coalición opositora y, en ese sentido, podría hacer valer tal condición para reclamar la presidencia del interbloque que actualmente está en manos del radical Mario Negri.

El otro candidato es justamente el cordobés (quien tendría las firmas suficientes para seguir conduciendo la bancada radical pese la férrea resistencia de un sector interno del centenario partido) quien para seguir presidiendo el interbloque cuenta con el respaldo de la mayoría del bloque de la UCR y también de los 11 legisladores de la Coalición Cívica que lidera Elisa Carrió.

Sin embargo, Negri no tiene el camino despejado, ya que enfrenta una rebelión en sus propias filas, ya que el sector referenciado en el senador Martín Lousteau en Diputados, juntó 15 firmas en una carta para explicitar con contundencia su firme rechazo a la continuidad como presidente del bloque radical de un hombre que perdió la interna en Córdoba en las últimas PASO.

Este grupo agitó la interna exigiendo a través de la misiva la "renovación de las autoridades y de las vocerías del bloque", en tanto que señala que esos cargos deben ser "el reflejo de lo validado por la sociedad en las elecciones".

"Es momento de ratificar esa renovación", sentencian en un claro tiro por elevación a Negri, quien ejerce la conducción del bloque de la UCR desde hace ocho años.

Según pudo saber NA de voceros cercanos a este sector radical, no se descarta que estos 15 diputados que estamparon la firma en la carta, de formalizarse la permanencia de Negri al frente del bloque, deserten de la bancada radical para armar una propia que podría llevar de nombre "Juntos Evolución".

Por ahora, no es más que una amenaza para que se reconsidere la continuidad de Negri como máxima autoridad del bloque, pero si las tensiones no aflojan y se agudiza la crisis interna, la división de la bancada UCR podría convertirse en una realidad.

Además, hay conversaciones entre el grupo que comanda el diputado Emiliano Yacobitti, personero de Lousteau en la Cámara baja, y Emilio Monzó (con quien el ex ministro de Economía tiene un vínculo y una sintonía política estrecha) para fusionar ambas fuerzas en una sola bancada.

De esta manera, podría nacer un bloque de entre 15 y 20 diputados que desplazaría a la Coalición Cívica como tercera fuerza dentro de Juntos por el Cambio.

Lousteau y Yacobitti también tienen en la mira a diputados electos como el santafesino Gabriel Chumpitaz, el salteño Carlos Zapata y la sanjuanina Susana Laciar, legisladores que el PRO cuenta como propios.

Para neutralizar y disuadir estos intentos de cooptación, desde el PRO analizan la opción de cambiar el nombre del bloque para así contener a aquellos legisladores que no se sienten identificados con la ideología y la historia del partido fundado por Mauricio Macri.

Por fuera de estas cuatro bancadas, habrá dentro de Juntos por el Cambio un bloque de dos legisladores puntanos y cuatro monobloques (uno de ellos serán el del economista liberal Ricardo López Murphy).