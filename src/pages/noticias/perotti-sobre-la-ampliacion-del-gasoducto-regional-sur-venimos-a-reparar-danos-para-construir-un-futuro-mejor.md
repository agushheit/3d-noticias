---
category: Estado Real
date: 2021-10-15T06:00:51-03:00
thumbnail: https://assets.3dnoticias.com.ar/GASO.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Perotti sobre la ampliación del gasoducto regional sur:" Venimos a reparar
  daños para construir un futuro mejor"
title: Perotti sobre la ampliación del gasoducto regional sur:" Venimos a reparar
  daños para construir un futuro mejor"
entradilla: 'En Venado Tuerto se presentaron las empresas interesadas en la realización
  de las obras que beneficiarán a más de 10 mil habitantes de ocho localidades. '

---
El gobernador Omar Perotti encabezó este jueves el acto en el que se presentaron las empresas interesadas en llevar las obras de ampliación del Gasoducto Regional Sur, que beneficiarán con gas natural a más de 10 mil habitantes de las localidades de Venado Tuerto, Carmen, Murphy, Chovet, Firmat, Casilda, Melincué y Teodelina. Cuenta que con un presupuesto oficial de más de 800 millones de pesos.

“Hoy no solamente venimos a hacer una licitación, sino sentimos que venimos a reparar daños para construir un futuro mejor”, sentenció el gobernador, y cuestionó el retraso y postergación que tuvo la obra: “Quienes sentimos como responsabilidad ineludible del Estado la de llevar adelante infraestructura de apoyo a la producción para generar más empleo, nos cuesta muchísimo entender por qué se dieron estas circunstancias”, dijo Perotti.

Y amplió: “Tener paralizadas estas obras durante tantos años sin dudas que genera un daño, generó que empresas perdieran competitividad, negocios y oportunidades laborales. En otros casos, la imposibilidad de contar con empresas que pudiesen radicarse en algunas de las localidades de la región. Eso juega en contra de la posibilidad productivas y del ahorro familiar”, expresó el mandatario.

El gobernador enumeró a cada uno de los distritos beneficiados, y anticipó que “son localidades que empiezan a analizar las circunstancias de otra manera, a pensarse de otra manera. Y allí es donde todos tienen que tener la certeza de que nuestra orientación siempre va a estar en el camino de apoyar al que trabaja, invierte y produce; contar con gas nos da otro horizonte para la producción en la región”, remarcó Perotti durante el acto llevado a cabo en el Parque Industrial de Venado Tuerto.

**TRABAJO Y EQUILIBRO TERRITORIAL**

Por su parte, la senadora nacional María de los Ángeles Sacnun, dijo que se trata de “una obra que nos enorgullece, porque es largamente acaecida, que permite a los departamentos Caseros y General López mejorar conexiones y apalanca el desarrollo industrial del sur de la provincia”.

A su turno, el presidente de la empresa Energías y Gas Renovables (Enerfe) Juan D’Angelosante, dio detalles de las acciones a ejecutar, y las describió como de “alto impacto”, porque permitirán “el desarrollo de municipios y comunas de nuestra provincia”, concluyó.

En tanto, el secretario de Empresas y Servicios Públicos de la provincia, Carlos Maina, coincidió al afirmar que “se empieza a resolver un problema de larga data”, y contó que “se trata de la primera licitación importante de la empresa de Energías de la provincia. Estas obras nunca son sencillas, es el servicio público más difícil de gestionar”, señaló.

**GASODUCTO REGIONAL SUR**

El Gasoducto Regional Sur Involucra la construcción de 18 kilómetros de gasoducto, 9 kilómetros de refuerzo en la alimentación, tres ampliaciones y un ramal de 15 kilómetros que abastecerá a Melincué de gas natural. Además, beneficiará a industrias, comercios y familias de Venado Tuerto, Carmen, Murphy, Chovet, Firmat, Casilda, Melincué y Teodelina.