---
category: La Ciudad
date: 2021-02-14T06:00:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/carnaval.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Música, cine y circo: Actividades gratuitas en el finde de Carnaval'
title: 'Música, cine y circo: Actividades gratuitas en el finde de Carnaval'
entradilla: La Municipalidad de Santa Fe ofrece una variada cartelera de espectáculos
  y propuestas culturales para el fin de semana extra largo que se avecina.

---
La Municipalidad de Santa Fe ofrece una variada cartelera de espectáculos y propuestas culturales para el fin de semana extra largo que se avecina.

Murgas, música en vivo, danza y espacios de juegos para chicos y grandes, tendrán lugar en los diversos espacios de la capital provincial. Todas las propuestas serán gratuitas, con cupos limitados, por orden de llegada.

En todos los casos, no se permitirá el ingreso a los lugares sin tapabocas, se mantendrá la distancia social y habrá expendio de alcohol en gel.

Las actividades, que se extenderán hasta el sábado 20 de febrero, son:

**Sábado 13**

_El Alero Las Flores_ (Azopardo y Millán Medina) / 17 a 20 horas

Feria Florecer.

Música: Cuerda de tambores del Solar.

Sapukay - La Casa de Fernando Birri (Ubajay 1512, San José del Rincón) / 17 a 21:30 horas

_Feria con artesanos y artesanas locales._

Música: Aie (percusión corporal) / "Murga canción", con Pitufo Quinteros y José Olmos.

Espacio para las infancias.

Instalaciones de carnaval.

**Domingo 14**

_La Casa de los Gobernadores_ (Bv. Gálvez y Güemes) /20 horas

Afro Carnaval con la Casa Indo Afro.

**Lunes 15**

_El Molino, Fábrica Cultural_ (Bv. Gálvez y Pedro Víttori) / 18 a 21 horas

Máscaras.

Burbujas de baile.

Taller de percusión corporal (Escuela Latido Americano).

Murga: Las Damajuanas.

**Martes 16**

_El Alero Coronel Dorrego_ (Bv. French y Sarmiento) / 18 a 20 horas

Murgas: Brillantisima y Sueño Dorado

**Miércoles 17**

_La Esquina Encendida_ (E. Zeballos y F. Zuviría) / 17 a 19 horas

Murga Reviviendo Colores.

Instalaciones de carnaval.

Cuarteto de percusión latinoamericana y Payaso Bobybolasyarte

**Jueves 18**

_El Alero Acería_ (Caferatta y Roca) / 16 a 19:30 horas

Taller de coplas de comadres (Coordina Cintia Bertolino).

**Sábado 20**

_La Redonda, Arte y Vida Cotidiana_ (Salvador del Carril y Belgrano)

Cierre del carnaval con espectáculos en vivo.

Toda la programación puede consultarse en las redes sociales de cada espacio y del Ministerio de Cultura de la provincia.