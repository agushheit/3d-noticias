---
category: Agenda Ciudadana
date: 2020-12-11T16:18:25Z
thumbnail: https://assets.3dnoticias.com.ar/biodiesel.gif
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: La Industria del Biodiesel de Argentina al borde de desaparecer ante la indiferencia
  del Estado
title: La Industria del Biodiesel de Argentina al borde de desaparecer ante la indiferencia
  del Estado
entradilla: Comunicado de prensa de la Cámara Argentina de Empresas Regionales Elaboradoras
  de Biocombustible,  la Cámara Santafesina de Energías Renovables y  la Cámara Panamericana
  de Biocombustibles Avanzados.

---
Las entidades que suscriben, en representación de la totalidad de las empresas elaboradoras de biodiésel con destino al corte obligatorio, por la presente denuncian que el gobierno nacional, de manera arbitraria, discriminatoria e ilegal, continúa con la decisión de no calcular, actualizar y determinar el precio de venta acorde a los costos reales que tiene la industria y al marco regulatorio vigente para la actividad, en abierta violación de la ley 26.093, situación que lleva más de 11 meses.

Con este accionar, **la totalidad de las empresas elaboradoras de biodiésel continúan paralizadas porque no es posible producir con el precio fijado por el gobierno nacional**, ya que el mismo resulta insuficiente para cubrir la compra de la materia prima y los demás costos de producción. Esto implica que las 28 empresas de biodiesel (instaladas en las provincias de Santa Fe, Buenos Aires, La Pampa, San Luis, Entre Ríos y Santiago del Estero) no trabajan desde hace más de 5 meses, acrecentando y agravando aún más la crisis que atraviesa el sector, colocándolo en un estado de situación terminal.

Luego de las declaraciones del Gobernador Arcioni, de la provincia de Chubut, en las que le solicita, sin medias tintas, al Presidente que privilegie a las empresas y provincias petroleras por sobre las productoras de biocombustible, se aclara el sentido de las inacciones que, desde hace un año, viene ejerciendo el gobierno nacional.

Queda más que en evidencia quiénes son los privilegiados con la decisión de dejar fundir a las empresas pymes del biodiésel de todo el país, y nuevamente ganan los poderosos de siempre, constituyendo un “falso” progresismo.

La no publicación de precios por parte del gobierno nacional no hace otra cosa que contribuir a la destrucción de la industria nacional de biodiésel, beneficiando así los intereses de las empresas petroleras, a contramano de lo que ocurre en el resto del mundo y en abierta violación a leyes nacionales y tratados internacionales a los que nuestro país adhirió.

Así, aún con toda la buena voluntad, no es posible continuar para las empresas, y esto es absolutamente conocido por el Sr. Secretario de Energía de la Nación como así también por el Sr. Ministro de Economía y el Sr. Ministro de Desarrollo Productivo. 

Por lo tanto, no queda más que pensar que lo que buscan es el cierre de las empresas y la caída del régimen de biocombustibles, acatando así lo que dictan los petroleros, provincias y empresas, mientras que las provincias que más aportan fiscalmente para sostener a los combustibles contaminantes, quedan condenadas a seguir siendo la billetera del negocio que beneficia a unos pocos, los mismos de siempre. 

Es imposible ver en estas actitudes el tan declamado federalismo, como así tampoco el compromiso en la preservación del medio ambiente y la salud pública.

Resulta inentendible que nuestro país, siendo uno de los principales productores mundiales de biodiésel, no cumpla desde hace más de 8 meses con los porcentajes legales de corte obligatorio; siendo, por otra parte, importador de más del 30% del gasoil que se consume, y existiendo capacidad ociosa de combustibles renovables y alternativos de producción nacional.

Eliminar el corte obligatorio de biodiésel le generará al país una salida de divisas por más de U$S500 millones, adicionalmente a las divisas por las importaciones de gasoil que actualmente ya viene realizando el estado nacional, presionando aún más sobre las débiles reservas de dólares del Banco Central. 

Esta medida no hace más que contribuir a la destrucción del aparato productivo nacional y el empleo genuino en el interior del país, destrozando un sector que, paradójicamente fue creado por un gobierno del mismo signo político que el actual y debatiéndose en la actualidad la prórroga del mismo en el Congreso Nacional, atentando así contra el desarrollo de las economías regionales, el agregado de valor en origen e industrialización de la ruralidad, la diversificación de la matriz energética a través de combustibles renovables de origen nacional, la desconcentración económica mediante la pluralidad de actores, la sustitución de importaciones y la preservación del medio ambiente y la salud pública.

El sector no reclama otra cosa que lo expresado por el Sr. Presidente de la Nación en ocasión del cierre de la Conferencia industrial de la UIA: “De la pobreza no se sale con el auxilio del Estado con planes, se sale con empresarios que invierten, dan trabajo y generan empleo. El Estado va a estar presente para ayudar a los que lo necesiten y para generar las condiciones para que esos empresarios se arriesguen a producir, a dar empleo, a dar trabajo y a ganar, que es lo que corresponde. Si les va muy bien, el Estado cobrará más impuestos, mejor será.”