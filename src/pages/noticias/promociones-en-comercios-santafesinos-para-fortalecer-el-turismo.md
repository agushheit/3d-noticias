---
category: Agenda Ciudadana
date: 2020-12-05T13:00:19Z
thumbnail: https://assets.3dnoticias.com.ar/CENTROCOMERCIAL.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Promociones en comercios santafesinos para fortalecer el turismo
title: Promociones en comercios santafesinos para fortalecer el turismo
entradilla: En el marco del Programa Descubrí Santa Fe, se firmó un convenio con la
  Federación de Centros Comerciales que apunta a brindar beneficios a los turistas
  de cara al inicio de la temporada de verano.

---
El gobierno de la provincia de Santa Fe, a través de las secretarías de Turismo y de Comercio Interior y Servicios del Ministerio de Producción, Ciencia y Tecnología, firmó un convenio con la Federación de Centros Comerciales de la provincia que se enmarca en el **programa Descubrí Santa Fe**, que prevé poner a disposición **descuentos y beneficios para atraer a los turistas** esta temporada.

El secretario de Comercio Interior y Servicios, Juan Marcos Aviano, valoró este convenio que “brindará a turistas las promociones comerciales de aquellos establecimientos y locales de bienes y servicios que están emplazados en la provincia de Santa Fe. 

Se trata de **descuentos del 10, 20 o 30 por ciento y los locales que adhieran a la iniciativa los ofrecerán a los visitantes**, quienes deberán acreditar su estadía en alguna de las 76 localidades con alojamiento turístico en el territorio provincial”.

**"Descubrí Santa Fe"** es una herramienta que se enmarca dentro de la iniciativa Nacional Pre Viaje y se suma a Santa Fe Plus. Los interesados en ser parte de esta herramienta de promoción deben registrar sus emprendimientos en el siguiente formulario.

Participaron de la firma el presidente de la Federación de Centros Comerciales de la provincia, Eduardo Taborda, y el secretario de Turismo, Alejandro Grandinetti.