---
category: Agenda Ciudadana
date: 2020-12-19T11:35:21Z
thumbnail: https://assets.3dnoticias.com.ar/1912-costamagna.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Costamagna participó del Consejo Federal de Ministros de la Producción
title: Costamagna participó del Consejo Federal de Ministros de la Producción
entradilla: El encuentro fue encabezado por su par nacional de Desarrollo Productivo,
  Matías Kulfas, quien instó a trabajar una agenda conjunta para fortalecer el perfil
  productivo de la Argentina.

---
El titular de la cartera de Producción, Ciencia y Tecnología de la provincia, Daniel Costamagna, asistió a la reunión del Consejo Federal de Ministros de la Producción que fue encabezado por el ministro de Desarrollo Productivo de la Nación, Matías Kulfas.

La reunión se llevó a cabo en el marco del Consejo Federal de la Micro, Pequeña y Mediana Empresa que es el ámbito de coordinación entre las distintas jurisdicciones de las políticas relativas a la promoción de las MIPyMEs en todo el territorio nacional, creado por la ley 25300/2000.

Al cabo del encuentro, desarrollado en el Centro Cultural Kirchner en la Ciudad Autónoma de Buenos Aires, Costamagna indicó: «El ministro Kulfas  convocó a todas las provincias a trabajar una agenda de desarrollo conjunta, con eje en un desarrollo productivo federal que permita el fortalecimiento de las cadenas de valor regionales y mayor participación de las PyMEs en el comercio exterior».

«Fue una muy buena reunión, federal y de trazado de lineamientos para el año que viene, con un fuerte foco en la inclusión financiera de las pymes», sostuvo el funcionario al tiempo que resaltó «una herramienta como el FOGAR, fondo de garantía que tiene a Santa Fe como la primera y única provincia en ponerla en funcionamiento».

Entre los temas tratados, Costamagna resumió: «Además de un repaso de todas las políticas nacionales de apoyo en la situación de pandemia, se abordaron aspectos relacionados con el comercio interior y exterior, así como con parques y áreas industriales, instancia en la que nuestra provincia también se destaca con tres proyectos que ya están recibiendo aportes por 180 millones de pesos, hablo del parque industrial de Rafaela y las áreas industriales de Esperanza y Las Parejas».

Durante el encuentro, Kulfas manifestó: «En 2020 tuvimos tres ejes: pandemia, reactivación y desarrollo. Durante la pandemia buscamos cuidar a los sectores productivos y destinamos un 7,2% del PBI, es decir casi 2 billones de pesos, con 29 diferentes instrumentos. En materia de reactivación uno de los puntos fundamentales como la inclusión financiera de las PYMES y otro es la reactivación de la política industrial, hablamos de parques industriales, comercio exterior inteligente y mesas de acuerdo económico y social para generar políticas de largo plazo».

## **PRESENTES**

De la reunión participaron, también, los **ministros de las carteras productivas**: de Córdoba, Eduardo Accastello; de Catamarca, Lisandro Álvarez; de Entre Ríos, Juan José Bahillo; de La Rioja, Federico Bazán; de Tierra del Fuego, Sonia Castiglione; de Chubut, Leandoro Cavaco; de Santa Cruz, Silvina Córdoba; de Buenos Aires, Augusto Costa; de Salta, Martín de los Ríos; de Ciudad Autónoma de Buenos Aires, José Luis Giusti; de Jujuy, Exequiel Lello Ivacevich; y de Misiones, Luis Lichowski y Sebastán Oriozabala.

También participaron, de manera virtual, los **responsables de las áreas de Producción e Industria**: de Corrientes, Claudio Anselmo; de San Juan, Andrés Díaz Cano; de Tucumán, Juan Luis Fernández; de Formosa, Jorge Ibañez; de Río Negro, Martín Lamot; de San Luis, Juan Lavandeira; de Chaco, Sebastián Lifton; de Neuquén, Facundo López Raggi; de Río Negro, Fernando Malaspina; de Santiago del Estero, Miguel Mandrile; de La Pampa, Ricardo Moralejo; de Corrientes, Raúl Schiavi, y de Mendoza, Alejandro Zlotolow.

En tanto, por el **Ministerio de Desarrollo Productivo de la Nación** estuvieron: el secretario de la Pequeña y Mediana Empresa y de los Emprendedores, Guillermo Merediz; el secretario de Industria, Ariel Schale; la secretaria de Comercio Interior, Paula Español; el secretario de Minería, Alberto Hensel y el director del Centro de Estudios para la Producción CEP XXI, Daniel Schteingart.