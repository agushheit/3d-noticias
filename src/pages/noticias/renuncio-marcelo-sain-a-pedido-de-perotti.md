---
category: Agenda Ciudadana
date: 2021-03-19T07:21:21-03:00
thumbnail: https://assets.3dnoticias.com.ar/sain.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de LT10
resumen: Renunció Marcelo Sain a pedido de Perotti
title: Renunció Marcelo Sain a pedido de Perotti
entradilla: Renunció Marcelo Sain a pedido de Perotti

---
El ministro de Seguridad Marcelo Sain renunció a su cargo a pedido del gobernador Omar Perotti. Deja de esta manera una gestión marcada por un incremento de homicidios en toda la provincia, pero principalmente en las ciudades de Santa Fe y Rosario.

Desde que se conoció el último audio enviado por el saliente funcionario, en el que se refería a la fuerza policial de forma denigrante, desacreditando sus capacidades, y haciéndolo extensivo al resto de los santafesinos, los rumores se su alejamiento se hicieron cada vez más fuertes.

"Y son… son unos negros pueblerinos. Les chupa un huevo lo que yo digo. Hacen lo que se les antoja (...). A las dos de la tarde están todos en la casa. Eso es lo que yo pienso de todos ustedes. Es la visión santafesina, pueblerina. Gracias a Dios que Dios atiende en la Capital Federal. Porque sino este país sería Uganda, hermano. Porque si Dios atendiese en Santa Fe estamos hundidos todos", dice el ministro de Seguridad en el audio.

Ya en febrero de 2020, a tres meses de asumir, el ministro sorprendió con sus dichos en un canal televisivo nacional (Crónica TV) en los que afirmaba que estaba descansado en Buenos Aires porque si veía a Santa Fe "lo cagaban a tiros".

 "Me vine a descansar a Buenos Aires porque si me quedo allá me cagan a tiros", dijo por Crónica TV.

De todos modos, más allá de la personalidad socarrona y virulenta de Sain, lo que quedará de su paso por el ministerio de Seguridad es el creciente índice de asesinatos. En su año y medio como funcionario poco pudo hacer por combatir el delito y la inseguridad en una provincia que lo recordará por la falta de políticas en materia de prevención y combate del crimen.

En el departamento La Capital los homicidios alcanzan 120 y solo en la ciudad de Rosario 263 entre 2020 y lo que va de 2021.

Su reemplazante sería Jorge Lagna, actual secretario de Gestión Institucional y Social de la Seguridad, quien estaría evaluando el ofrecimiento de Perotti para aceptar o no el cargo. Por el momento, la Provincia no se pronunció respecto a la salida de Sain.

**Pedido de juicio político**

El audio mencionado, además de las críticas, generó el repudio de la oposición y el pedido de juicio político por mal desempeño de funcionario público e injurias. El jueves 11 de marzo ingresó a la legislatura santafesina impulsado por referentes de Juntos por el Cambio.

Estos cuestionamientos posiblemente contribuyeron a nivel político con la caída de Sain, quien desde que arrancó sumó, además de una mala gestión contra la inseguridad, confrontaciones con distintos sectores siempre a través de Twitter.