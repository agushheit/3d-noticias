---
category: Agenda Ciudadana
date: 2021-10-10T06:15:21-03:00
thumbnail: https://assets.3dnoticias.com.ar/VACUNAS.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: La vacuna estará accesible para quienes no se hayan inoculado aún
title: La vacuna estará accesible para quienes no se hayan inoculado aún
entradilla: Desde la próxima semana se pondrá la vacuna en la provincia a toda persona
  que no la haya recibido. El requisito va a ser inscribirse en el Santa Fe Vacunas

---
Desde la próxima semana y según anunció el Ministerio de Salud de la provincia de Santa Fe, los santafesinos que no tengan la primera dosis de la vacuna colocada podrán acceder a ella acudiendo alguno de los vacunatorios locales. La medida ya se implementa en algunos lugares de la provincia, como San José del Rincón o Coronda, la misma se generalizará en toda la bota.

La vacuna va a estar disponible para quienes la soliciten. El requisito básico va a ser inscribirse en el Santa Fe Vacunas, asistir al vacunatorio y referir que no tienen la primera dosis. Se va a constatar los datos y van a poder recibir la primera dosis. Y quienes no, van a poder acceder a herramientas para inscribirse en el mismo lugar.

**Vacunación en menores**

El Hospital de Niños Orlando Alassia se prepara para inocular a los niños santafesinos con factores de riesgo en la ciudad de Santa Fe. La semana próxima la provincia de Santa Fe comenzará a inocular contra el coronavirus a niños menores de entre tres y 11 años de edad. Según destacaron, el objetivo de avanzar en la vacunación de los más chicos tiene que ver con el objetivo de “alcanzar la inmunidad colectiva”.

Según los datos que dio a conocer el funcionario, actualmente en Santa Fe hay un 58% de la población que ya completó el esquema de vacunación.

Con respecto a la cantidad de niños de entre tres y 11 años de edad que hay en la provincia, rondan los 450.000 y se ese total, el 25% está inscripto. Para poder hacerlo la inscripción se realiza a través de la web Santa Fe Vacuna

Factores de riesgo:

\- Diabetes tipo 1 o 2 (insulinodependiente y no insulinodependiente).

\- Obesidad grado 2 (índice de masa corporal -IMC- mayor a 35) y grado 3 (IMC mayor a 40).

\- Enfermedad cardiovascular

\- Enfermedad renal crónica

\- Enfermedad respiratoria crónica

\- Personas que viven con VIH

\- Pacientes en lista de espera para trasplante de órganos sólidos y trasplantados de órganos sólidos.

\- Personas con discapacidad residentes de hogares, residencias y pequeños hogares.

\- Pacientes oncológicos y oncohematológicos con diagnóstico reciente o enfermedad “ACTIVA” (menos de un año desde el diagnóstico; tratamiento actual o haber recibido tratamiento inmunosupresor en los últimos 12 meses; enfermedad en recaída o no controlada).