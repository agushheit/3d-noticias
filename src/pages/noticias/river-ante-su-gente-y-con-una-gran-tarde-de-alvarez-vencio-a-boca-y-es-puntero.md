---
category: Deportes
date: 2021-10-04T06:00:20-03:00
thumbnail: https://assets.3dnoticias.com.ar/rojasriver.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: River, ante su gente y con una gran tarde de Álvarez, venció a Boca y es
  puntero
title: River, ante su gente y con una gran tarde de Álvarez, venció a Boca y es puntero
entradilla: 'El "Millonario" derrotó por 2 a 1 ante el "Xeneize", que sufrió la expulsión
  de Marcos Rojo, en uno de los cuatro encuentros del domingo por la fecha 14 del
  torneo de Primera División. '

---
River Plate, nuevo líder de la Liga Profesional de Fútbol, derrotó este domingo a Boca Juniors por 2 a 1, en el partido válido por la fecha 14, con una soberbia actuación que contentó a los miles de hinchas que volvieron al Monumental.  
El delantero Julián Álvarez convirtió por duplicado para el equipo de Marcelo Gallardo, mientras que el defensor peruano Carlos Zambrano descontó en la última jugada.  
El "Millonario" alcanzó los 30 puntos con la cuarta victoria consecutiva y es líder del campeonato. A su vez terminó con el invicto de Sebastián Battaglia como entrenador de Boca, que terminó con diez jugadores por la expulsión de Marcos Rojo.  
El partido de River fue casi perfecto. Le faltó definirlo en el segundo tiempo, pero en todo momento fue más que Boca. Un fútbol de alto vuelo basado en el juego de equipo con individualidades brillantes en Álvarez, Enzo Pérez, Nicolás De La Cruz, Robert Rojas, Paulo Díaz y Santiago Simón, la gran novedad de la formación.  
River empezó con la posesión de la pelota ante la intención clara de Boca de aprovechar un error en la recuperación. El equipo de Battaglia cedió el protagonismo, a diferencia de sus últimas presentaciones.  
Con paciencia, los dirigidos por Gallardo intentaron llegar al área con la conexión Enzo Pérez, De La Cruz y Palavecino.  
Una infracción de Enzo Pérez le valió tarjeta amarilla en los minutos iniciales y luego se produjo un encuentro cercano entre varios jugadores de ambos clubes. El árbitro Fernando Rapallini convocó a los capitanes Franco Armani y Carlos Izquierdoz en señal de advertencia. El juez no quiso que el Superclásico se le fuera de las manos.  
Rojo generó una fuerte entrada sobre Braian Romero, luego reemplazado, y recibió la segunda amarilla, tras una infracción a Palavecino.  
Boca se quedó con diez jugadores y Battaglia dispuso la salida de Edwin Cardona para el ingreso de Zambrano. En 15 minutos, el elenco xeneize sufrió la baja de un referente por un lado y del creador de fútbol por el otro, un arma mortal para la pelota parada.  
Quizás este episodio haya condicionado los planes de Boca, pero River ya había demostrado mejor funcionamiento.  
Entre tanto, River pasó al frente con el gran gol de Álvarez. Encaró al área, se sacó de encima una marca y sacó un derechazo que sobrepasó la línea de un desesperado Agustín Rossi.  
El segundo tanto se produjo tras un error de Rossi en la salida: la capturó Milton Casco, cedió para Simón para el centro preciso y el delantero del seleccionado argentino lo transformó en gol.  
Durante la primera parte, Boca sólo ejecutó un tiro al arco con un remate del peruano Luis Advíncula que pasó cerca del palo izquierdo.  
River mantuvo la línea en el segundo tiempo. La falta de definición lo privó de una goleada. Antes de la media hora, Carrascal, de flojo partido en su ingreso por el lesionado Romero, definió mal, en soledad, ante Rossi; un remate de Simón fue desviado por Izquierdoz y Angileri tuvo un disparo en el palo.  
Los dirigidos por Gallardo dejaron con vida a un Boca confundido, permeable y sin vocación ofensiva que encontró el descuento en el final con un cabezazo de Zambrano que Armani no pudo sacar. La pelota traspuso la línea con claridad.  
  
La diferencia pudo haber sido mayor, no se reflejó con la buena actuación de un River que volvió a pisar fuerte, que se reencontró con su mejor fútbol para tomar el liderazgo, al menos hasta que Talleres de Córdoba juegue mañana ante Defensa y Justicia.  
En la próxima fecha, River enfrentará a Banfield y Boca lo hará ante Lanús.