---
category: Agenda Ciudadana
date: 2021-02-26T07:46:30-03:00
thumbnail: https://assets.3dnoticias.com.ar/patente.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Diario El Litoral
resumen: Marcha atrás para el patentazo
title: Marcha atrás para el patentazo
entradilla: 'La Cámara de Diputados tendrá la última palabra. En el Senado se acordó
  un texto que tiene elementos del mensaje del Ejecutivo, y de sendos proyectos del
  justicialismo y el radicalismo. '

---
La Cámara de Senadores votó un proyecto de ley para imponerle límites al llamado patentazo, producto de la revaluación de los rodados que produjo subas de hasta el 180%.

El texto sancionado fija en hasta un 40% las subas para los contribuyentes respecto de los valores del año pasado.

Se trata de un impuesto ya emitido, ya que inicialmente el Poder Ejecutivo no había advertido los efectos de la aplicación de la norma aún vigente. Ahora se deberán compensar a quienes ya cumplieron con su obligación, según el texto que sancionó el Senado, aunque la última palabra la tendrá Diputados.

Los jefes de los distintos bloques trabajaron en la unificación de los proyectos y el mensaje del Ejecutivo pero no hubo acuerdo en cuando a la distribución del gravamen. Se impuso la idea de volcar más recursos a los gobiernos de pueblos y ciudades.

La determinación del Impuesto Patente Única sobre Vehículos para el año fiscal 2021, no podrá superar en un 40 %, el impuesto determinado para el período fiscal 2020.

Para el caso de los vehículos modelo 2021, el impuesto resultante no podrá superar en un 30% el impuesto que corresponda tributar por vehículos similares u homólogos a modelos 2020. Y, sin perjuicio de lo dispuesto en el párrafo anterior, "los incrementos indicados podrán ser superados en aquellos casos que, en el año 2021, se apliquen modificaciones en las alícuotas diferenciales a que hace referencia el segundo párrafo del artículo 1° de la ley Nro. 12306 y modificatorias".

La Administración Provincial de Impuestos "procederá a reliquidar el Impuesto Patente Única sobre Vehículos por el año 2021 conforme a la presente ley y a establecer el importe de las cuotas respectivas. Los importes correspondientes al año 2021 abonados hasta la fecha de entrada en vigencia de la presente ley por las cuotas liquidadas con anterioridad a la sanción de la misma, y que excedan los montos determinados para las primeras dos cuotas de las referidas en el párrafo anterior, serán computados como pago a cuenta de la cuotas 3, 4, 5 y 6, en ese orden de prelación".

Además "en caso que lo abonado en concepto de Impuesto Patente Única sobre Vehículos correspondiente al año 2021 a la fecha de inicio de vigencia de la presente supere la obligación anual para el año 2021, la Administración Provincial de Impuestos procederá de oficio a la devolución de los montos abonados en exceso". 

**Detalles**

La media sanción sustituye uno de los artículos del Código Fiscal, referido a la Patente Única sobre Vehículos, que tendrá el siguiente texto "ARTÍCULO 328. Lo recaudado en concepto del gravamen establecido en el presente Título y las tasas establecidas en la Ley Impositiva, se distribuirán de la siguiente forma: a) El 63 % será acreditado automáticamente por el Banco de Santa Fe S.A. en la cuenta corriente de cada Municipalidad o Comuna en donde se encuentre registrado el vehículo, e ingresará a sus Rentas Generales. b) El 30 % será acreditado automáticamente por el Banco Santa Fe S.A. en una cuenta especial habilitada al efecto por el Poder Ejecutivo. El saldo de dicha cuenta será distribuido quincenalmente por la citada entidad bancaria entre todas las Municipalidades y Comunas, conforme a los coeficientes que a tal efecto establezca para cada una de ellas el Ministerio de Economía, teniendo en cuenta para su determinación, la emisión correspondiente a cada Distrito. c) El 5 % será acreditado automáticamente por el Banco Santa Fe SA. en la cuenta corriente de Rentas Generales de la Provincia, d) El 1 % (uno por ciento) será acreditado automáticamente por el Nuevo Banco Santa Fe SA. en partes iguales entre todas las Municipalidades de la Provincia. e) El 1 % será acreditado automáticamente por el Nuevo Banco Santa Fe S.A. en partes iguales entre todas las Comunas de la Provincia" y con retroactividad al 1ro de Enero de 2021.

**Embarcaciones**

Además, otro cambio en el articulado fija en el 100% del gravamen el patentamiento de las embarcaciones y establece que "el contralor de la inscripción de las embarcaciones en el registro que a tal efecto reglamente la Administración Provincial de Impuestos, como el cobro y la administración del tributo, estarán a cargo de los Municipios y Comunas donde se encuentren radicadas".

Las Municipalidades y Comunas "por su cuenta y cargo" podrán celebrar convenios con aquellos contribuyentes que no hubieran dado cumplimiento al pago del impuesto sobre las Embarcaciones Deportivas o de Recreación".

Las modalidades de los convenios y los requisitos para su formalización "serán establecidas por la API y serán aplicables los intereses resarcitorios, punitorios y demás recargos que a tal efecto establezcan las normas vigentes en el Código Fiscal. Y las mismas facultades y límites se fijan para las acciones administrativas y de ejecución judicial por vía de apremio (de acuerdo a lo previsto en la ley 5066 y modificatorias).