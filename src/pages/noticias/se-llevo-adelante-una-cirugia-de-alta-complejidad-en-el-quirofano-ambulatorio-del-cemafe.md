---
category: Estado Real
date: 2020-12-27T11:46:45Z
thumbnail: https://assets.3dnoticias.com.ar/2712cemafe.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Se llevó adelante una cirugía de alta complejidad en el quirófano ambulatorio
  del Cemafe
title: Se llevó adelante una cirugía de alta complejidad en el quirófano ambulatorio
  del Cemafe
entradilla: La paciente fue intervenida para resolver una parálisis de su miembro
  superior derecho.

---
Un equipo de profesionales médicos llevó adelante una cirugía de alta complejidad en el quirófano ambulatorio del Centro de Especialidades Médicas (Cemafe).

El doctor Ricardo Jullier estuvo a cargo de la dirección y supervisión de un amplio equipo de profesionales, y expresó que «lograron la reconstrucción del nervio radial, mediante un injerto interfascicular con toma de la pierna contra-lateral, realizando el bypass nervioso. Seguidamente se realizaron las transferencias para dar extensión a la muñeca y a la mano».

La paciente, de 16 años, «cursaba una lesión de su miembro superior derecho producto de un asalto. La agresión fue realizada con un elemento punzo-cortante, directamente a la región superior del brazo, en donde entró por la cara externa, atravesó todo el brazo y llegó hasta la pared torácica».

Seguidamente, Jullier explicó que «la lesión provocó una sección alta del nervio radial por ende, una parálisis del miembro, afectando codo, muñeca y mano, imposibilitando la extensión de estas regiones». La planificación pre-operatoria consistió en realizar una restauración de la conducción nerviosa y asociar transferencias de tendones funcionantes a tendones paralizados, con el propósito de lograr a futuro restaurar funciones abolidas.

Desde el equipo de profesionales destacaron la importancia de haber desarrollado el procedimiento en el Cemafe, «ya que estas prácticas que combinan técnicas micro-quirúrgicas con convencionales, eran en ocasiones, patrimonio de internaciones prolongadas. La infraestructura que se encuentra en este caso en el ámbito quirúrgico del Cemafe, ha resuelto perfectamente la posibilidad de realización de esta cirugía compleja».

«No solamente es relevante lo edilicio. Una de las cosas importantes a destacar es el recurso humano. En esta oportunidad, tanto la dirección médica y responsables de distintas áreas, como el personal dentro de quirófano, instrumentadoras, enfermeras, circulantes, se pusieron a disposición todos los recursos necesarios, lo que posibilitó el éxito de la operación», finalizó el especialista a cargo de la dirección y supervisión.

<br/>

<span style="font-family: helvetica; font-size: 1.2em; font-weight: 800;">**EJEMPLO DE SALUD PÚBLICA**</span>

«Lo actuado, evidencia y pone de manifiesto que en Santa Fe se ha logrado tener un centro de cirugía ambulatoria con recursos para llevar adelante e intentar solucionar cirugías de este nivel, contando con cirujanos jóvenes comprometidos vocacionalmente con la especialidad elegida y con el dogma del trabajo y capacitación», expresó con satisfacción el director. «El concepto de trabajo en equipo, el aval y predisposición de las autoridades, el amparo de un hospital público para la apoyatura de eventual derivación, logra y conforman elementos para que la ciudad y la región puedan contar con un sistema de salud inclusivo para todos», finalizó el especialista, a cargo del equipo, Ricardo Jullier.

El equipo quirúrgico estuvo integrado por Talía Jullier, Gala Gómez Mabragaña, Pablo Giovino y Agustín Ferronato, Gabriel Israilevich (anestesia), Vanina Zapata (instrumentación) Anahi Juárez (circulante) y bajo la dirección y supervisión de Ricardo Jullier, con el acompañamiento de la Jefa del Servicio de Traumatologia y Ortopedia del hospital JM Cullen, Elizabeth Crespi.