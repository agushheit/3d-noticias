---
category: Estado Real
date: 2021-08-19T06:00:35-03:00
thumbnail: https://assets.3dnoticias.com.ar/votojoven.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Perotti envió a la legislatura el proyecto para implementar el voto joven
  en la provincia
title: Perotti envió a la legislatura el proyecto para implementar el voto joven en
  la provincia
entradilla: La iniciativa del gobernador establece la posibilidad de sufragar entre
  los 16 y 18 años, aunque no es obligatorio. Hoy, Santa Fe y Corrientes son las únicas
  dos provincias en las que no está contemplado.

---
El gobernador Omar Perotti remitió este miércoles a la Legislatura, para su consideración, tratamiento y sanción, un proyecto de ley sobre la ampliación de los derechos políticos de los jóvenes de entre 16 y 18 años, conocido como Voto Joven.

La propuesta establece el derecho a votar en elecciones provinciales a las y los jóvenes de entre 16 y 18 años, pero no los obliga a hacerlo. De tal modo que, si deciden no participar del proceso electoral, no pueden ser sancionados.

En el texto del proyecto, se fundamenta que “cada vez más jóvenes se involucran para tratar de transformar la realidad a través de la práctica política. Hace casi una década que pueden hacerlo. Además de, en las aulas, los colegios y los barrios, también en las urnas. Pero en el caso de Santa Fe, pueden hacerlo únicamente para los cargos nacionales, es decir, pueden elegir al presidente y vicepresidente, a los senadores y diputados nacionales, pero no pueden elegir a las autoridades provinciales y locales”.

El escrito tiene como antecedente la reforma a la Ley de Ciudadanía, sancionada el 31 de octubre de 2012, que consagró el derecho a votar en elecciones nacionales a las y los jóvenes de entre 16 y 18 años.

Asimismo, con la sanción de la ley nacional, casi todas las provincias comenzaron a adaptar su legislación para permitir el voto joven en las elecciones provinciales para cargos locales. A la fecha, sólo no está previsto en las provincias de Corrientes y Santa Fe.

“Es una instancia donde, como provincia estamos retrasados; como no hemos actualizado la Constitución, el voto habla de los 18 años y pareciera que entraríamos en contracción si aprobamos una ley que hable de esto, pero hay jurisprudencia que empuja fuertemente a que Santa Fe vaya adecuando su normativa legislativa más allá que se haya o no modificado la Constitución”, explicó el gobernador Perotti.

“Este proyecto consagraría una expansión de los derechos políticos de los jóvenes, permitiendo que puedan elegir a sus representantes en todos los niveles de gobierno, eliminando así la imposibilidad de hacerlo para cargos locales; para la historia de la democracia en la provincia, significará el reconocimiento en esta franja etaria de capacidades y potencialidades hasta ahora invisibilizadas”, agrega el texto del proyecto que, además de la firma del gobernador, también tiene la rúbrica del ministro de Gobierno, Justicia, Derechos Humanos y Diversidad, Roberto Sukerman.

De esta manera, se propone modificar los artículos 2° y 14° de la Ley 4.990. El primero de ellos quedará redactado de la siguiente manera: “Constituyen el cuerpo electoral de la provincia, en calidad de electores, los argentinos nativos y por opción, desde los 16 años, y los argentinos naturalizados, desde los 18 años, que estén inscriptos en el Registro de Electores y no tengan ninguna de las inhabilitaciones previstas en esta ley”.

En tanto, una sección del artículo 14, quedaría redactado así: “Todo elector tiene el deber de sufragar en cuantas elecciones provinciales, municipales o de comisión de fomento se realicen en su distrito electoral. Quedan exentos de esta obligación los jóvenes entre los 16 y los 18 años, y los mayores de 70 años”.