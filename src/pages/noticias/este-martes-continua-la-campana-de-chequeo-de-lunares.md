---
category: La Ciudad
date: 2021-10-19T06:00:27-03:00
thumbnail: https://assets.3dnoticias.com.ar/LUNARES.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Este martes continúa la campaña de chequeo de lunares
title: Este martes continúa la campaña de chequeo de lunares
entradilla: Este 19 de octubre, se realizará en la intersección de Suipacha y San
  Martín, ambos días en el horario de 10 a 14 y de 16 a 20.

---
El lunes, el director de Salud del municipio, César Pauloni, informó sobre las tareas de prevención de lunares y manchas en la piel, que buscan evitar lesiones cancerígenas. “Se trata de un testeo que realiza un laboratorio La Roche-Posay y al que nos sumamos como municipio. Se desarrolla durante dos días e involucra a diferentes secretarías como la de Políticas de Cuidado, y Control”, señaló.

Con respecto a la dinámica de la atención, el funcionario aclaró que las personas se pueden acercar sin turno previo. “Obviamente se realiza en forma gratuita, la campaña consta de la atención de un médico dermatólogo, que les hace una revisión y da un diagnóstico y con eso, una derivación oportuna, en caso de que sea necesario”, explicó.

“Hay que saber que el daño solar que uno va generando en el organismo es acumulativo, por eso durante la campaña asesoramos e informamos a las personas sobre todos estos detalles para que tengan en cuenta”, indicó Pauloni.

**Prevención**

Macarena De Lucca es una de las profesionales que trabaja durante la campaña. “El cáncer de piel puede ser detectable si se trata a tiempo. Por tal motivo es muy importante la prevención. Hacemos el chequeo lunar con un dermatoscopio, y revisamos lesión por lesión todo el cuerpo”, dijo. En ese sentido, la profesional destacó que “hay lesiones benignas y malignas, esas son las que se tienen que extirpar”.

Por otra parte, De Lucca explicó que “vamos a seguir trabajando en la Estación Belgrano esta tarde, mientras que mañana el camión estará emplazado en San Martín y Suipacha, en el mismo horario”. En ese sentido, la dermatóloga convocó a que “se acerquen todos a chequear los lunares de forma totalmente gratuita”.

Es sumamente importante que durante el verano, cuando hay más exposición al sol, se tome el recaudo “de usar protector solar, renovarlo cada dos horas y no exponerse desde las 10 de la mañana hasta las 16 horas. Porque se está viendo mucho cáncer de piel y en gente más joven”.

En tanto, el farmacéutico Sebastián Zetner, también presente en la campaña, indicó su satisfacción por poder retomar esta iniciativa. “Es muy importante la prevención, desde la farmacia trabajamos mucho en ese sentido, principalmente en la piel, la glucosa y la diabetes, así que nos gusta apoyar estas iniciativas en la ciudad”. Con respecto a la buena participación de los vecinos y vecinas, destacó que la propuesta “les gusta mucho, genera mucho interés y la gente se suma”.