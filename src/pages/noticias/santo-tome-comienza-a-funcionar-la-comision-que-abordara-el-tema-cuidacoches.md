---
category: Agenda Ciudadana
date: 2021-04-08T08:22:59-03:00
thumbnail: https://assets.3dnoticias.com.ar/cuidacoches.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa concejo Santo Tomé/ Diario El Litoral
resumen: 'Santo Tomé: comienza a funcionar la comisión que abordará el tema cuidacoches'
title: 'Santo Tomé: comienza a funcionar la comisión que abordará el tema cuidacoches'
entradilla: Esta propuesta tiene como eje el ordenamiento de la actividad y enfatiza
  en la inclusión y la convivencia social en la ciudad.

---
“La inclusión social es un derecho de todos los ciudadanos es nuestra obligación como estado garantizarlo”, afirmó la Presidenta del Concejo Municipal de Santo Tomé, Gabriela Solano, en relación a la puesta en funcionamiento de la comisión que prevé el abordaje y regulación de la actividad de los cuidadores de vehículos de la ciudad santotomesina.

La primera reunión referida al tema tuvo lugar este martes en el edificio de calle Sarmiento 1651. Allí participaron Jorge Luis (en representación de los cuidacoches) Silvina Bezi y Ruben Ruzicki (en representación del Centro Comercial), Daniela Susmann (Secretario de Acción Social del municipio), Pbro Jose Milesi (Parroquia Inmaculada Concepción), Natalia Angulo (Concejala) Gabriela Solano (Presidenta del HCM), Soledad Santa Cruz y Fernando Lopez (equipo de la concejala Florencia Gonzalez). 

Al respecto Solano explicó: “a los cuidacoches los conozco de haberles realizado muchas entrevistas, muchos de ellos están allí desde pequeños, con una vida difícil, a veces sesgada por las adicciones o por la pobreza. Les lleve la convocatoria para que uno de ellos, en representación de los demás, concurra a la comisión de tratamiento de esta problemática en nuestra ciudad”. 

Conjugando las diferentes miradas, esta propuesta tiene como eje el ordenamiento de la actividad y enfatiza en la inclusión y la convivencia social, sin perder de vista que, actualmente, hay muchas personas que realizan esta actividad de la ciudad.

Así, en esta primera instancia la comisión resolvió realizar un primer relevamiento de las personas que realizan la tarea de cuidacoches y de esta manera conocer su realidad social.

“Mi mayor deseo es que en conjunto podamos encontrar una salida que promueva, como siempre digo, la paz social, que nos permita convivir. Este parece un lujo que hemos olvidado, pero tenemos que ponernos a trabajar para que sea posible. Así como le pedimos a las fuerzas de seguridad que hagan tarea de prevención, que tengan presencia y que nos cuiden, también es importante que, como funcionarios y como sociedad, breguemos por una ciudad inclusiva. Entiendo que la inclusión social es la garantía de la igualdad de oportunidades garantizando el derecho y la oportunidad a todos y todas de desarrollarse plenamente. Por esto tenemos que velar como sociedad y como estado”, concluyó la presidenta del cuerpo.

 

**Antecedentes** 

La comisión abocada al estudio de la actividad de los cuidadores de vehículos fue una propuesta que la Concejala Gabriela Solano presentó en una sesión del Concejo Municipal en noviembre de 2019. En ese momento, en el recinto de la ciudad, sin final satisfactorio, se trataba un proyecto que pretendía crear un Registro de Cuidacoches en Santo Tomé, los autores de esta ordenanza eran la concejala con mandato cumplido Rosana Zamora (PJ) y el actual concejal Rodrigo Alvizo (PJ). En esa misma sesión el concejal mandato cumplido Miguel Weiss Ackerley (Cambiemos) propuso que se prohíba la actividad, mientras que los y las integrantes del Centro Comercial de la ciudad, se acercaron al recinto para expresar que se oponían a la creación de ese registro y, al mismo tiempo, dejar en claro que no era pretensión de la institución obligar a los “trapitos” a que se retiren del lugar. 

“En ese contexto realicé la moción, que fue votada y que por mayoría quedó firme, para que se creará una comisión de seguimiento de esta problemática. Hoy la pongo en funcionamiento como presidenta del cuerpo y me enorgullece hacerlo”, explicó la concejala.