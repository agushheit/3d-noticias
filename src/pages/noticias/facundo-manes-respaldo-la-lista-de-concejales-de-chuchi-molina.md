---
category: La Ciudad
date: 2021-10-31T06:00:03-03:00
thumbnail: https://assets.3dnoticias.com.ar/manes.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Facundo Manes respaldó la lista de concejales de Chuchi Molina
title: Facundo Manes respaldó la lista de concejales de Chuchi Molina
entradilla: Estuvo en barrio Barranquitas. "Hay que dar el paso en esta elecciones
  también para poner en marcha la ciudad", afirmó el neurocientífico

---
La lista de candidatos al Concejo Municipal que encabeza [**Adriana "Chuchi" Molina**](https://www.unosantafe.com.ar/adriana-chuchi-molina-a59094.html), siguió sumando respaldos nacionales. Esta vez lo hizo [**Facundo Manes**](https://www.unosantafe.com.ar/facundo-manes-a68115.html), prestigioso neurocientífico, que desde el radicalismo se postula a diputado nacional por [Juntos por el Cambio](https://www.unosantafe.com.ar/juntos-el-cambio-a62397.html) por la provincia de Buenos Aires. “Así como el resto del país, en la ciudad de Santa Fe en estas elecciones también se pone en juego su futuro. Por eso hay que dar el paso, para poner en marcha la ciudad. Hay que fortalecer nuevos liderazgos que puedan aportar también a un camino colectivo para transformar la ciudad, la provincia y el país”.

“Para nosotros es muy importante que Facundo nos acompañe, y que sea acá nada más y nada menos, recordando que en el 2014 estuvo en el Jardín Municipal de Barranquitas inaugurado en la anterior gestión, acompañándonos porque la educación para los radicales y para Juntos por el Cambio es clave, y es también otro aspecto que hay que poner en marcha en la ciudad”, dijo por su parte, Chuchi Molina, quien estuvo acompañada en la recorrida y en las actividades de Manes desarrolladas en Santa Fe por Hugo Marcucci, también candidato a concejal; Carolina Losada y Dionisio Scarpin, candidatos a senadores nacionales y Mario Barletta, candidato a diputado nacional.

## Un compromiso por el futuro

Facundo Manes, en diálogo con la prensa, explicó que “dar el paso significa comprometerse con nuestro país en el peor momento de su historia. Argentina es un país que venía con una decadencia crónica, pobre, que no estaba en vida de desarrollo y que enfrenta una crisis multidimensional: económica, social, humanitaria, moral, educativa y psicológica”, describió el candidato bonaerense. “Dar el paso significa estar a la altura y decirle a nuestros nietos, que en el momento más trágico de Argentina estuvimos presentes poniendo todo para superarlo. Eso significa dar el paso y creo que la Argentina necesita un cambio totalmente radical. El radicalismo tiene en su nombre lo que necesita Argentina: un cambio diferente. Si seguimos haciendo las mismas prácticas de siempre con las mismas fórmulas de siempre y con los mismos de siempre, vamos a tener más decadencia, más pobreza y más desigualdad”, advirtió Manes.

“El radicalismo fue en el 83 clave para la reconstrucción democrática, fue el último gran sueño colectivo que unió a todos los argentinos y hoy tenemos el mismo desafío pero con la reconstrucción del país encarando el siglo XXI”, concluyó.