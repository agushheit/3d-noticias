---
category: La Ciudad
date: 2021-07-22T06:15:59-03:00
thumbnail: https://assets.3dnoticias.com.ar/INSCRIPCION.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Se lanzó el programa municipal de inscripción para que todos puedan vacunarse
title: Se lanzó el programa municipal de inscripción para que todos puedan vacunarse
entradilla: 'La Municipalidad de Santa Fe puso en marcha un operativo para ayudar
  y acompañar a los santafesinos que aún no recibieron la vacuna contra el Covid-19. '

---
Las personas de cualquier edad podrán acercarse a alguna de las 17 Estaciones municipales, de lunes a viernes, de 8 a 13, para anotarse en la campaña que lleva adelante el Ministerio de Salud.

El intendente Emilio Jatón encabezó esta mañana la presentación del programa municipal de inscripción descentralizado en los barrios de Santa Fe, para que los vecinos y vecinas puedan acceder al operativo de vacunación contra el Covid-19. De esta manera, las personas de cualquier edad pueden acercarse -con DNI en mano- a la Estación municipal más cercana a su domicilio, de lunes a viernes, de 8 a 13, para anotarse en la campaña de inoculación que lleva adelante el Ministerio de Salud.

Son 17 los espacios habilitados para este programa, ubicados en distintos barrios de la ciudad. Los interesados podrán ingresar a [http://santafeciudad.gov.ar/vacunate/](http://santafeciudad.gov.ar/vacunate/ "http://santafeciudad.gov.ar/vacunate/") y allí conocer cada una de las direcciones donde se encuentran las Estaciones; y también informarse sobre los detalles de cómo inscribirte al registro de vacunación.

“Santa Fe tiene alrededor del 55% de la población con la primera dosis y no supera el 13% con la segunda; ahí está nuestro punto más débil en la ciudad. Si pretendemos llegar a una inmunidad de rebaño, tenemos que poner ahí el esfuerzo. Vamos a tener una conversación con la ministra de Salud para ver las estrategias que podemos hacer en conjunto para seguir sumando más gente a la vacunación”, manifestó Jatón, que estuvo junto a las secretarias de Políticas de Cuidados y Acción Social, María Victoria Rey; y de Integración y Economía Social, Ayelén Dutruel, en la Estación Loyola

El fin del programa es colaborar en la profundización de la estrategia territorial de acompañamiento y promoción de la vacunación en la población de la ciudad de Santa Fe. Vale recordar que las Estaciones son 17 espacios del municipio que están abiertos desde los inicios de la gestión para garantizar políticas cercanas a cada una de las familias de los barrios de la ciudad.

**Para todos y todas**

Por su parte, Rey agregó: “En función del diálogo y el trabajo articulado con el Ministerio de Salud de la provincia se va a profundizar la estrategia de ir a buscar a las personas mayores y con comorbilidades y también a los jóvenes de la ciudad de Santa Fe para promocionar la vacunación. Pero también los espacios están abiertos para todas aquellas personas que quieran vacunarse y asesorarse sobre las gestiones para sacar el turno”.

La funcionaria le solicitó a la población que se acerque a las estaciones que allí hay personal capacitado para acompañarlo y garantizar la gestión. “También vamos a articular con el centro de vacunación más cercano para que así toda la población de la ciudad de Santa Fe acceda a la vacunación contra el Covid”, destacó en esta línea.

Rey señaló también que “entre las personas que salimos a buscar y acompañar en todo el proceso están las personas en situación de calle. A esta población le garantizamos su vacunación tanto en el refugio como en la calle con los equipos del Ministerio de Salud. Estas estrategias ya las veníamos haciendo, pero está claro que tenemos que profundizarlas y ampliarlas para toda la comunidad”.