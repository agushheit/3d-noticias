---
category: Agenda Ciudadana
date: 2021-07-08T08:03:02-03:00
thumbnail: https://assets.3dnoticias.com.ar/batallon-ingenieros.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Santo Tomé al día
resumen: 'Batallón de Ingenieros: investigan a un capitán del Ejército por acoso sexual'
title: 'Batallón de Ingenieros: investigan a un capitán del Ejército por acoso sexual'
entradilla: Lo acusan también de hostigamiento laboral hacia una subordinada que revestía
  el rango de cabo, por hechos ocurridos en 2018. Será indagado en noviembre.

---
Un capitán del Ejército Argentino, identificado como L.G.D. de 38 años, deberá comparecer ante la justicia, por una denuncia de acoso sexual y hostigamiento laboral. Es a raíz de una denuncia realizada por una joven que revestía el rango de cabo y era subordinada del acusado en 2018.

El hombre fue citado a declaración indagatoria para el mes de noviembre a pedido del fiscal federal Walter Rodríguez, quien investiga una serie de hechos denunciados por la víctima -cuyas iniciales son M.A.B y en ese entonces tenía 21 años- que tuvieron lugar en el Batallón de Ingenieros Zapadores 1 de nuestra ciudad.

En el pedido de declaración indagatoria, el funcionario judicial explicó que el capitán maltrató y perjudicó arbitrariamente a quien hoy es excabo, “mediante la realización reiterada y sistemática de actos hostiles y humillantes mientras el nombrado se desempeñó como su autoridad superior jerárquica, durante el mes de junio de 2018, y su traslado a Curuzú Cuatiá, Corrientes, en el año 2019”.

Para el fiscal Rodríguez, las actitudes denunciadas se encuadran en el artículo 249 bis del Código Penal. Este sostiene que “el militar que en sus funciones y prevalido de su autoridad, arbitrariamente perjudicare o maltratare de cualquier forma a un inferior, será penado con prisión de seis (6) meses a dos (2) años, si no resultare un delito más severamente penado”.

En su escrito, sostuvo en su escrito que el imputado “actuó con conocimiento sobre el carácter abusivo de sus conductas, y sobre la condición de jerarquía con el rango de capitán que poseía en el batallón, por sobre la víctima que revestía el rango de Cabo”. Para el letrado, actuó con voluntad para perjudicar a M.B., tanto psicológicamente como laboralmente.