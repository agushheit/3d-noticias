---
category: Estado Real
date: 2021-06-01T08:26:16-03:00
thumbnail: https://assets.3dnoticias.com.ar/vacuna-policia.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: 'Covid-19: Hoy llegaron 38.100 nuevas dosis de vacunas Sputnik V a la provincia '
title: 'Covid-19: Hoy llegaron 38.100 nuevas dosis de vacunas Sputnik V a la provincia '
entradilla: Se trata del componente 1º y 2º de esta formulación. Se suman a las 176.750
  que arribaron la semana pasada, totalizando así 214.850.

---
La provincia, a través del Ministerio de Salud, informó que arribaron hoy 38.100 nuevas dosis de vacunas Sputnik V contra el Coronavirus, para continuar con el histórico operativo de vacunación que Santa Fe lleva adelante sin descanso, en articulación y conforme lineamientos consensuados con la cartera sanitaria nacional.

Cabe recordar que la semana pasada llegaron a la provincia 176.750 dosis, compuestas por 129.500 vacunas AstraZeneca y 47.250 Sputnik V del 1º componente. De este modo entre las que se recibieron la semana pasada y que ya se están aplicando y las recibidas hoy, el número asciende a 214.850.

Al respecto,  la ministra de Salud de la provincia, Sonia Martorano, precisó: “El lunes 24 de mayo recibimos 15.600 dosis de AstraZeneca, mientras que el jueves 27 otras 67.200 dosis y el pasado viernes 46.700 de la misma formulación. Asimismo, ese mismo viernes. 47.250 Sputnik V del primer componente, lo que nos permite avanzar sin pausa completando esquemas y aplicando primeras dosis en la población objetivo”.

Más concretamente, las de AstraZeneca se destinaron a completar los esquemas de inmunización de aquellas personas que recibieron las primera dosis, entre los que se encuentran la totalidad de geriátricos y otras personas que componen la población objetivo. Mientras que las del primer componente de Sputnik se utilizaron para avanzar con la primera dosis de las personas de riesgo.

**Penitenciarios y mayores de 45 con comorbilidades**

En otro orden, la ministra Martorano adelantó que las dosis del primer componente de Sputnik V recibidas se “destinarán al personal penitenciario, las fuerzas de seguridad, a mayores de 60 que por alguna razón no la hubiesen recibido y, posteriormente, a personas entre 59 y 50 años con comorbilidades”.

En tanto, las dosis con el segundo componente de esta misma vacuna se utilizarán para completar los esquema de aquellas personas mayores que estén ya en plazo y condiciones para recibirlas.

Para cerrar, la ministra destacó: “Estamos ampliando la capacidad de vacunación y diariamente inoculamos a 25 mil personas en toda la provincia, y ya llegamos al 62% de la población objetivo priorizada”.