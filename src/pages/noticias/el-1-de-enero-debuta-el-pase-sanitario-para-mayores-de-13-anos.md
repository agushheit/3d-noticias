---
category: Agenda Ciudadana
date: 2021-12-13T01:01:45-03:00
thumbnail: https://assets.3dnoticias.com.ar/viaje.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Diario Uno
resumen: El 1 de enero debuta el Pase Sanitario para mayores de 13 años
title: El 1 de enero debuta el Pase Sanitario para mayores de 13 años
entradilla: Se aplicará en las actividades de mayor riesgo epidemiológico. El gobierno
  nacional anunció que así se busca fomentar la vacunación.

---
El gobierno nacional anunció que a partir del 1 de enero, toda persona a partir de los 13 años que realice actividades definidas de alto riesgo epidemiológico y sanitario deberá acreditar tener un esquema de vacunación completo contra Covid-19. Para ello deberá exhibir el Pase Sanitario ante el requerimiento del personal público o privado designado para su constatación

Las actividades definidas para la acreditación del pase son: asistencia a locales bailables, discotecas o similares que se realicen en espacios cerrados, salones de fiestas para bailes, bailes o similares que se realicen en espacios cerrados, viajes grupales de egresadas y egresados, de estudiantes, jubiladas y jubilados o similares y eventos masivos organizados de más de mil personas que se realicen en espacios abiertos y cerrados o al aire libre.

La forma de acreditación será a través de la aplicación “Cuidar – Sistema de prevención y cuidado ciudadano contra Covid-19” que puede descargarse en forma gratuita en las tiendas de aplicaciones oficiales de Android e iOS.

La versión de la aplicación “Cuidar” que se debe tener instalada es la 3.6, la cual ya se encuentra disponible para descarga en una versión de prueba y estará completamente operativa desde el 1 de enero de 2022. El proceso correcto para actualizar la app Cuidar, si ya se contaba con la plataforma descargada en el teléfono con anterioridad, es desinstalar la aplicación y volver a instalarla.

Si el autodiagnóstico de la aplicación arrojara algún síntoma compatible con Covid-19 o si la persona estuviera notificada en el Sistema Nacional de Vigilancia de la Salud como caso activo de Covid-19, se bloqueará la pantalla de la aplicación “Cuidar” sin permitirle acceder a ninguna otra pantalla ni certificado hasta tanto se modifique esta condición, independientemente de tener el esquema de vacunación completo.

Las personas que no pudieran acceder a la aplicación, siempre y cuando no se encuentren cursando la enfermedad, podrán solicitar a la autoridad jurisdiccional competente el certificado de vacunación contra Covid-19 en el cual consten las dosis aplicadas y notificadas al Registro Federal de Vacunación Nominalizado (NOMIVAC).

El Ministerio de Salud de la Nación determinará el criterio de definición de esquema completo de vacunación contra Covid-19.

Las autoridades de las jurisdicciones dispondrán los procedimientos de fiscalización necesarios para garantizar el cumplimiento de medida. Además, podrán exigir la acreditación del esquema completo para actividades adicionales en función de la situación epidemiológica, el plan de vacunación local y los avances en las coberturas de vacunación contra Covid-19.

La implementación del pase sanitario Cuidar es una medida impulsada en el marco del Consejo Federal de Salud con el consenso entre la cartera sanitaria nacional y los ministros de salud de las 24 jurisdicciones. Representa una herramienta clave para sostener los logros de la actual situación, reducir los riesgos epidemiológicos y seguir brindando protección contra Covid-19 a cada vez más ciudadanas y ciudadanos.