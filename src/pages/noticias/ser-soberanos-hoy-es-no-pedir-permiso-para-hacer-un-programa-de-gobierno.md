---
category: Agenda Ciudadana
date: 2021-11-20T06:00:14-03:00
thumbnail: https://assets.3dnoticias.com.ar/soberano.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: '"Ser soberanos hoy es no pedir permiso para hacer un programa de gobierno"'
title: '"Ser soberanos hoy es no pedir permiso para hacer un programa de gobierno"'
entradilla: "El Presidente encabezó en el Palacio San Martín el lanzamiento de la
  Mesa de Trabajo Interministerial “Agenda Malvinas 40 años”, en el marco de la conmemoración
  por el Día de la Soberanía.\n\n"

---
El presidente Alberto Fernández afirmó que "ser soberanos hoy es no pedir permiso a nadie para hacer un programa de gobierno" y es "recuperar la capacidad de manejar la deuda de modo tal que el pueblo argentino no sufra a la hora de pagarla".  
  
El primer mandatario realizó estas declaraciones al encabezar en el Palacio San Martín, sede de la Cancillería, el lanzamiento de la Mesa de Trabajo Interministerial “Agenda Malvinas 40 años”, en conmemoración por el Día de la Soberanía que recuerda la batalla de la Vuelta de Obligado del 20 de noviembre de 1845.  
  
"Ser soberanos hoy es no tener que pedirle permiso a nadie para hacer un programa de gobierno, eso tiene que ver con no endeudarnos; siempre el que se endeuda indefectiblemente termina condicionado. Los acreedores nos condicionan siempre", dijo el mandatario en su discurso.  
  
Fernández señaló que “cuando tomamos deuda y esa deuda la usamos para pagar a otros que vinieron a hacer sus negocios a la Argentina o para permitirle a otros que se lleven dinero que trajeron para especular, lo que hacemos contrayendo deuda es condicionar nuestro futuro a los acreedores".  
  
En esa línea, el Presidente afirmó que "ser soberanos hoy es recuperar la capacidad de manejar la deuda de modo tal que el pueblo argentino no sufra a la hora de pagarla" y destacó que "no es tarea fácil y la diplomacia tiene mucho que ver en eso".  
  
Al recordar la batalla de la Vuelta de Obligado, que se produjo sobre la orilla del río Paraná, cerca de la localidad bonaerense de San Pedro, el presidente dijo que esa efeméride "nos enseña que el que vence es el que nunca baja los brazos y sigue su pelea y su lucha".

> "Siempre el que se endeuda indefectiblemente termina condicionado. Los acreedores nos condicionan siempre"

  
“Algunos preguntan por qué conmemoramos una batalla en la que no ganamos. Como dije el otro día, a veces ganar no es vencer, vence el que no baja los brazos", subrayó al recordar la frase que pronunció el martes en el acto en Plaza de Mayo por el Día de la Militancia, tras las elecciones legislativas del domingo.  
  
“Cuando hablamos de soberanía siempre terminamos hablando de Malvinas porque allí está nuestra soberanía territorial quebrada, rota, mancillada”, dijo Fernández y enfatizó: “Que nadie nunca nos robe el amor por Malvinas, que nunca nadie nos quite el derecho que tenemos sobre esa tierra”.  
  
“Nunca olvidemos que hace 40 años fueron allí a dejar su vida cientos de argentinos y tenemos un deber con la memoria de cada uno de ellos. Malvinas nos une”, reclamó el Presidente.  
  
Afirmó que las islas "fueron, son y serán argentinas, mal que les pese a algunos" que “minimizan esas tierras y se atreven a decir que paguemos vacunas entregando a las islas".  
  
"Me asombra que lo digan en público. A las Malvinas no las vamos a cambiar ni por vacunas ni por deuda; vamos a pelear hasta que vuelvan a ser argentinas", completó.  
  
Al presentar la Mesa de Trabajo Interministerial Agenda Malvinas 40 años, que tendrá como objetivo armar una agenda federal y participativa con motivo de cumplirse el año próximo cuatro décadas de la guerra, Fernández sostuvo que se seguirá “trabajando por la vías diplomáticas, convenciendo al mundo de que las Malvinas son, fueron y serán argentinas”.  
  
El mandatario explicó que “la soberanía hoy es un concepto multidimensional” que incluye aspectos sociales, alimentarios, culturales y ambientales, que “va mucho más allá de la defensa de nuestra tierra y seguramente muchas cosas más” y exhortó a tener presente “cuáles son los desafíos de hoy y las luchas que debemos dar para ser soberanos”.  
Durante su discurso, el Presidente homenajeó a los excombatientes presentes en el acto del Palacio San Martín: entre ellos el exjefe del Ejército, Martin Balza, y el director del Museo Malvinas, Edgardo Esteban, entre otros veteranos de guerra que concurrieron.

> "Ser soberanos hoy es recuperar la capacidad de manejar la deuda de modo tal que el pueblo argentino no sufra a la hora de pagarla"

  
  
Por su parte, el canciller Santiago Cafiero anunció la apertura de una subsede de la Secretaría de Malvinas en Ushuaia y dijo que “recuperar el ejercicio pleno de la soberanía de las Malvinas se ha constituido en una aspiración a la máxima identidad de nuestro país".  
  
"Es un sentimiento unánime del pueblo argentino y así está reflejado a nivel constitucional”, completó Cafiero respecto a la aspiración de "recuperar el ejercicio pleno de la soberanía de las Malvinas".  
  
El secretario de Malvinas, Antártida y Atlántico Sur, Guillermo Carmona, señaló “el enorme sentido de esta fecha, para recuperar el sentido que tiene la soberanía nacional”, y destacó la trascendencia de las leyes impulsadas por el gobierno nacional como la que crea el Consejo Nacional Malvinas, la que fija el límite exterior de la plataforma continental y la que fija penas más severas a la pesca ilegal.  
  
Asistieron al acto los titulares de las Comisiones de Relaciones Exteriores del Senado, Adolfo Rodríguez Saá, y de Diputados, Eduardo Valdés, los ministros de Defensa, Jorge Taiana; Desarrollo Productivo, Matías Kulfas; Educación, Jaime Perczyck; Ciencia, Tecnología e Innovación, Daniel Filmus; Interior, Eduardo de Pedro; Obras públicas, Gabriel Katopodis y de Trabajo, Claudio Moroni.  
  
Estuvieron también los jefes de las Fuerzas Armadas, titulares de distintos gremios, y de manera virtual, participaron gobernadores y embajadores argentinos ante diferentes países del mundo y organismos multilaterales.