---
category: Estado Real
date: 2021-01-16T04:29:03Z
thumbnail: https://assets.3dnoticias.com.ar/Perotti-salud.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: La provincia garantizó recursos para todos los efectores de salud pública
  municipales de Rosario
title: La provincia garantizó recursos para todos los efectores de salud pública municipales
  de Rosario
entradilla: El gobernador Omar Perotti anunció el pago correspondiente al convenio
  de atención de alta complejidad para esa ciudad, y asegurar así su normal funcionamiento.

---
El gobernador Omar Perotti anunció este viernes, la realización de una nueva transferencia destinada a la Municipalidad de Rosario, correspondiente al convenio para atención de alta complejidad en efectores de salud pública dependientes del municipio. Se trata de un pago de 329 millones de pesos que completó un total de 3.075 millones que el gobierno provincial invirtió en la red sanitaria pública en 2020.

Por primera vez, la ejecución de fondos y su control contó con la coordinación de una comisión mixta integrada por provincia y municipio. Vale destacar, además, que el monto invertido supera los 4 mil millones de pesos si se contempla el anticipo de 890 millones de pesos de adelanto del ejercicio 2020 que las por entonces autoridades municipales le solicitaron a la provincia en 2019 y la administración del FPCyS le suministró.

Al respecto, Perotti ponderó el trabajo coordinado en materia sanitaria: «Se ha definido continuar y tener previsibilidad en el trabajo que realizamos en el marco del convenio de salud pública ajustando los valores y las rendiciones. En este sentido, se ha puesto en funcionamiento por primera vez una comisión de trabajo conjunto que nos permite optimizar la ejecución de recursos y procesos».

El mandatario destacó el caudal de inversión ejecutada en el sistema sanitario de la ciudad de Rosario. «Son recursos volcados a la alta complejidad, fundamentalmente a la internación y a las instancias de atención de una persona derivada a los efectores públicos».

Además, recordó que la provincia ayudó «a los municipios para que puedan estar fuertes frente a la mayor demanda en materia de salud. Se trabajó mucho en Rosario para refinanciar, por ejemplo, anticipos financieros y emergencias por 1.500 millones; se pusieron al día los aportes al Promudi (Programa Municipal de Inversiones), las regularizaciones impositivas, los fondos Covid, el financiamiento educativo, y en el caso de esta ciudad, se llegó casi a los 6.000 millones de pesos en asistencia financiera desde la provincia a la municipalidad de Rosario».

Perotti agregó que «la pandemia, nos pone frente a situaciones de parálisis de actividades económicas, de caídas de recaudaciones. Sin estos recursos, ¿cómo podría funcionar Rosario? Seguramente, con servicios recortados. Pero la Provincia ha estado presente en un momento particular, con el convencimiento de que las acciones conjuntas nos tienen que permitir superar las situaciones difíciles».

Por su parte, el intendente local, Pablo Javkin, apuntó: «Estamos trabajando en este 2021 ratificando un importante camino de trabajo común, con un aporte que el Municipio hace al conjunto de la región y la provincia, a partir de una historia de desarrollo de la salud y con un reconocimiento muy importante de la provincia a la hora de valorar ese aporte».

<br/>

## **CIFRAS**

Por su parte, la secretaria de Administración del Ministerio de Salud de Santa Fe, Pamela Suárez, ofreció precisiones respecto de las transferencias realizadas por la provincia al municipio: «el acuerdo que tiene vigente la Municipalidad de Rosario con la Provincia es un convenio anual donde se han ido transfiriendo más de 3 mil millones de pesos. Después hay otros convenios que tiene el Municipio con la Provincia que tienen que ver con un trabajo conjunto entre el Laboratorio Industrial Farmacéutico de la provincia y el Laboratorio de Especialidades Medicinales de Rosario, algunos pagos de anestesiología y diálisis que se hacen en forma conjunta, la provisión de insumos, entre otros», detalló.

Y describió la población objetivo de esta inversión: «los usuarios del sistema de salud pública de Rosario son prácticamente un 40% de la población, unos 400 mil habitantes, que son atendidos un 50% por efectores municipales y otro 50% por efectores provinciales. El sistema de salud pública estatal está muy coordinado y por eso la tarea de vinculación es tan importante».