---
category: Agenda Ciudadana
date: 2021-03-09T06:42:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/ministro-seguridad-santa-fe-marcelo-sain.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Diario Uno
resumen: Viralizan audio del ministro Sain donde critica y descalifica a la policía
  santafesina
title: Viralizan audio del ministro Sain donde critica y descalifica a la policía
  santafesina
entradilla: '"Son unos negros pueblerinos" y "Si Dios atendiese en Santa Fe estamos
  hundidos todos", dice, entre otras cosas el funcionario. El audio se vuelve viral
  en un complejo marco político'

---
En las últimas horas, comenzó a circular con insistencia por WhatsApp un audio donde se escucha al ministro de Seguridad Marcelo Sain criticar y descalificar duramente a la policía santafesina. Serían audios que Sain envió hace un año al entonces jefe Policía de la provincia de Santa Fe, Víctor Sarnaglia, quien se fue de su puesto en medio de una tormenta de acusaciones cruzadas, pero se viralizaron en las últimas horas en el marco del intenso conflicto político que se registra por estos días en la provincia.

"Creo que ustedes juegan en la primera D y yo juego en la primera A. Vos dirás que soy un hijo de puta, un fanfarrón, un turro. No, Negro. Tengo muchos años de experiencia jugando en primera A. Ahora estoy en el vestuario de la primera D", remarca en un momento del audio Sain.

"Y son… son unos negros pueblerinos. Les chupa un huevo lo que yo digo. Hacen lo que se les antoja (...). A las dos de la tarde están todos en la casa. Eso es lo que yo pienso de todos ustedes. Es la visión santafesina, pueblerina. Gracias a Dios que Dios atiende en la Capital Federal. Porque si no este país sería Uganda, hermano. Porque si Dios atendiese en Santa Fe estamos hundidos todos", continúa.

***

[![mp4](https://assets.3dnoticias.com.ar/sain-audio.webp)](https://assets.3dnoticias.com.ar/declaraciones-sain.mp4 "declaraciones-sain.mp4")

***

El propio ministro se refirió al material que está circulando en una publicación realizada en su cuenta de Twitter.

Aseguró que se trata de un audio privado de vieja data ("no es una escucha judicial", disparó contra la oposición) y aclaró que lo que se escucha no es su opinión sobre los santafesinos en general, que estaba hablando de una persona en particular "que no tenía ganas de trabajar". Planteó además sus reparos de que el audio se conozca en este momento. "Raro que salga ahora".

## **Una licitación bajo la lupa**

El audio se vuelve viral en un complejo marco político. El viernes pasado, cuatro legisladores de la oposición provincial presentaron una denuncia penal ante el fiscal Luis Schiappa Pietra, de la Agencia de Delitos Complejos y Criminalidad Organizada, para que se investiguen hechos que podrían constituir un delito en el marco de la licitación para la compra de armas para la policía por parte del Ministerio de Seguridad.

Se trata del proyecto de adquisición de 12 mil pistolas semiautomáticas, 130 fusiles de asalto, 130 subametralladoras, 6 fusiles semiautomáticos para francotiradores y 6 fusiles para francotiradores de cerrojo manual requeridos por el ministro Marcelo Sain. Una compra por 1.400 millones de pesos que por el momento está frenada por el propio gobierno de Omar Perotti y por la Justicia en lo Contencioso Administrativo.

## **Protección de ex jefe policial**

En la misma jornada, Sain denunció -en una rueda de prensa en la ciudad de Firmat- que el ex jefe de Inteligencia Zona Sur de la ex Drogas Peligrosas, quien es juzgado actualmente por narcotráfico, Alejandro Druetta, “tenía una estrecha protección” por parte de los legisladores del Frente Progresista Enrico y Pullaro.

"Se revelaron en un juicio en Rosario una serie de escuchas que demostraron que (Druetta) tenía una estrecha protección de estos dos dirigentes”, sostuvo entonces. Según dijo a los periodistas, “el adalid de la lucha contra el narcotráfico era un policía narcotraficante. Alejandro Druetta era el nene mimado del senador Enrico y del entonces diputado Pullaro. Si eso no es tener vínculo con el crimen, entonces qué es”, subrayó.

***

![](https://assets.3dnoticias.com.ar/druettajpg.jpg)

***

## **El Jaime Stiuso de Santa Fe**

El senador por el departamento General López Lisandro Enrico salió públicamente a defenderse este lunes. "No tengo absolutamente nada que ocultar y nada de qué arrepentirme", enfatizó el legislador.

Sostuvo que Saín "más que un conductor de las fuerzas de seguridad parece el Jaime Stiuso de Santa Fe, que aprieta periodistas, maltrata a los trabajadores policiales y le hace los trabajos sucios al Gobernador en contra de la oposición".