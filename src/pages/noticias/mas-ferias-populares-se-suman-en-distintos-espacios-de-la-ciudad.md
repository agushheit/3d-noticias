---
category: La Ciudad
date: 2020-11-30T15:57:22Z
thumbnail: https://assets.3dnoticias.com.ar/FERIAS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Más ferias populares se suman en distintos espacios de la ciudad
title: Más ferias populares se suman en distintos espacios de la ciudad
entradilla: Son alrededor de 25 los puntos en los que cada fin de semana se ofrece
  alimentos, creaciones artesanales y otros productos. El municipio acompaña esos
  emprendimientos.

---
La Municipalidad, desde el inicio de la actual gestión, relevó todas las ferias locales y las condiciones de higiene y seguridad de los trabajadores de la economía social y popular. 

Hoy son alrededor de 25 los espacios que se desarrollan en la ciudad. Junto a las ferias, cooperativas y organizaciones sociales, el Estado local sigue trabajando en consolidar esos emprendimientos, en regularizarlos, en mejorar los servicios y orientar un mejor aprovechamiento de los procesos de producción y venta.

El secretario de Integración y Economía Social, Mariano Granato, dijo que desde el inicio de la gestión de Emilio Jatón se comenzó el trabajo con las ferias. “La economía social es una de las claves para la recuperación de una sociedad distinta, solidaria e integrada. 

Uno de los grandes problemas que siempre tuvo el sector fueron los espacios de comercialización, su periodicidad, cantidad y acompañamiento del Estado”, explicó el funcionario.

Por este motivo, “una de la líneas estratégicas de la gestión municipal hace hincapié, justamente, en fomentar y mejorar las ferias como espacios de desarrollo de la economía social entendiendo además que son lugares de encuentro, que permiten estrechar los vínculos sociales y, además, acercar a productores y consumidores en una relación virtuosa, cercana y más justa”.

Estas ferias populares se dividen en tres categorías: alimentarias -son 12 en funcionamiento-, las comerciales -como La Baulera-, y las artesanales -como la Del Sol y La Luna o la de la Costanera-; y dentro de estas, el municipio le está dando un fuerte impulso a las barriales. De estas últimas algunas son fijas y otras que se van sumando los fines de semana.

  
 

## **¿Dónde están?**

Vale destacar que había tres ferias artesanales en la ciudad: la de la costanera de cada domingo, la de la plaza Pueyrredón y la del Parque Federal; y ahora se sumaron otras tres: la feria del Boulevard, que está en bulevar y Pedro Víttori; una feria de mujeres emprendedoras, en la plaza Constituyentes; y la última en incorporarse es Sueños del Norte, una feria barrial que se desarrolla cada sábado y domingo en calle Baigorria al 9300.

Otra feria popular que se sumó en las últimas semanas se denomina Incuba y funciona los sábados en Güemes y Córdoba, en barrio Candioti Norte; y la última en incorporarse es en Santa Rosa de Lima, en la plaza Arenales. También vale mencionar la Nueva Feria Mujeres Agroecológicas, en Iturraspe y 4 de Enero, que se desarrolla los sábados de 8.30 a 12.30.

La Municipalidad acompaña a estas ferias de diferentes maneras. Por un lado con estructura y servicios; y a otras con asesoramiento. En estos momentos por la pandemia por el COVID-19 también se las acompaña con los protocolos necesarios. Se trata de un abordaje integral con el fin de que una no compita con la otra; y que los vecinos de cada barrio se apropien de estos espacios.