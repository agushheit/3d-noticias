---
category: Agenda Ciudadana
date: 2022-02-22T09:50:15-03:00
thumbnail: https://assets.3dnoticias.com.ar/vacunacion.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: Nuevo protocolo para la prevención y control del Coronavirus en las aulas
title: Nuevo protocolo para la prevención y control del Coronavirus en las aulas
entradilla: Se aplicará a los niveles Inicial, Primario y Secundario de todo el territorio
  santafesino.

---
El Ministerio de Salud, a través de la Dirección Provincial de Epidemiología, elaboró y publicó el Protocolo para la Prevención y Control de Covid-19 en Establecimientos Educativos, aplicable a los niveles Inicial, Primario y Secundario desde febrero de 2022.

El protocolo “fue elaborado por la Dirección de Epidemiología de la provincia de Santa Fe, adaptado del documento realizado por las áreas técnicas del Ministerio de Salud de la Nación, con los aportes recibidos del Ministerio de Educación de la Nación, UNICEF, OPS, Sociedad Argentina de Pediatría y Defensoría Nacional de Niñas, Niños y Adolescentes, las y los ministros de salud de las jurisdicciones y el Consejo Consultivo Educativo”, expresaron desde el Ministerio de Salud provincial.

ORIENTAR Y ACTUALIZAR LAS FORMAS DE PREVENCIÓN

Por otra parte, aclararon que las definiciones del documento “están destinadas a orientar y dirigir las acciones de prevención y control (aislamiento de casos, rastreo y cuarentena de contactos, seguimiento). No deben interpretarse ni utilizarse como conceptos para realizar diagnósticos médicos clínicos”.

“El manejo clínico de los casos dependerá de la evaluación médica, los diagnósticos diferenciales, la interpretación de los resultados de laboratorio realizados por el médico tratante y la evolución del paciente”, profundizaron desde Epidemiología de la provincia.

Y concluyeron enfatizando y recordando que “las medidas de prevención reducen el riesgo de contagio pero no lo eliminan completamente”, al mismo tiempo que “un aula ‘cuidada y segura’ es un aula donde se combinan todas las medidas posibles de protección, que se listan y desarrollan en profundidad en el documento antes mencionado y ya accesible a todos los ciudadanos”