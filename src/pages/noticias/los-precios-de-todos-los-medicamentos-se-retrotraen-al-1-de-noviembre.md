---
category: Agenda Ciudadana
date: 2021-11-06T06:15:29-03:00
thumbnail: https://assets.3dnoticias.com.ar/feletti.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Los precios de todos los medicamentos se retrotraen al 1 de noviembre
title: Los precios de todos los medicamentos se retrotraen al 1 de noviembre
entradilla: El secretario de Comercio Interior, Roberto Feletti aseguró que que “afortunadamente
  hubo compresión de las cámaras farmacéuticas” y adelantó que desde el lunes “tendrían
  que empezar a ordenarse”.

---
Los precios de "todos los medicamentos" se retrotraerán a los vigentes al 1 de noviembre pasado y se mantendrán sin cambios hasta comienzos del año próximo, confirmó el secretario de Comercio Interior de la Nación, Roberto Feletti, en referencia al principio de acuerdo alcanzado este jueves con laboratorios.  
  
Este jueves a la anoche, durante la reunión mantenida con las cámaras representantes del sector, el Gobierno nacional alcanzó un principio de acuerdo para retrotraer el precio de los medicamentos a los valores vigentes al 1 de noviembre pasado y mantenerlos sin cambios hasta el 7 de enero próximo  
  
"Todos sabemos, y eso por supuesto lo tienen que decir los sanitaristas, que el consumo de medicamentos normalmente gira en torno de 500 productos, pero van a ir todos los medicamentos", explicó Feletti esta mañana en diálogo con Radio 10, al adelantar el alcance del entendimiento.  
  
Tras asegurar que "afortunadamente hubo compresión de las cámaras farmacéuticas" y adelantar que desde el lunes "tendrían que empezar a ordenarse" los precios, reveló que los laboratorios "van a incorporar ese día los productos y, sobre eso, vamos a empezar a trabajar la estabilización".  
  
El director ejecutivo de la Cámara Industrial de Laboratorios Farmacéuticos Argentinos (Cilfa), Eduardo Franciosi, en tanto, indicó a Télam que "está acordado un compás de espera hasta el lunes próximo para analizar la propuesta del Gobierno y responder".  
  
En similar sentido, voceros de la Cámara Argentina de Especialidades Medicinales (Caeme) aseguraron: "Estamos en estos momentos analizando la situación; hay una buena disposición para el diálogo de todas las partes".  
  
Al comienzo de su gestión en la Secretaría de Comercio Interior, Feletti se focalizó en "parar y estabilizar precios", tras observar una aceleración "muy fuerte e inexplicable" de valores en productos de la canasta básica.  
  
Para ello, el Gobierno dispuso la ampliación del Programa Precios Cuidados hasta alcanzar los 1.432 productos que deberán mantener su precio vigente al 1 de octubre hasta el 7 de enero, y ahora intenta sumar la canasta de medicamentos de fuerte incidencia en muchos hogares.  
  
Este viernes por la mañana, el ministro de Desarrollo Productivo, Matías Kulfas, afirmó que los acuerdos para la estabilización de precios que se alcanzaron en alimentos y medicamentos apuntan a reducir la inflación "para que los ingresos de los hogares se empiecen a recuperar".  
  
"Los esquemas de controles o acuerdos de precios buscan frenar la inercia en productos de canasta básica, alimentos, medicamentos; Argentina se está recuperando, este año se proyecta un crecimiento de 9% anual y el objetivo es que se pueda reducir un poco la inflación para que los ingresos de los hogares se empiecen a recuperar", dijo Kulfas en diálogo con CNN.  
  
Según Kulfas, "Argentina tiene problemas de inflación, que no es nuevo, y que nos cuesta mucho reducirla"; para él, se debe a "los desequilibrios económicos que tiene" el país, "que se profundizó después de abril de 2018 tras la brutal salida de capitales".  
  
Para reducir la inflación, Kulfas consideró central "dar certidumbre macroeconómica", a partir de la "reestructuración de los activos externos y que pueda ser abonado en el tiempo", y avanzar en "un sendero fiscal".  
  
En relación al acuerdo de precios de 1.432 productos alimenticios de consumo masivo, Feletti sostuvo que se cumple "bastante bien" en las grandes cadenas de supermercados y destacó que el acuerdo alcanzado con Changomas "con quien había algunas diferencias".  
  
Por otra parte, el lunes próximo, se reunirá con las cadenas regionales y los pequeños comerciantes con el objeto de "evaluar cuestiones de márgenes y abastecimiento", en los cuales "hay diferencias con las grandes cadenas".  
  
Los mismos habían planteado que, debido a la intermediación de los mayoristas (a diferencia de las grandes cadenas), no podían obtener un margen suficiente de los productos acordados.  
  
En este contexto, Feletti anticipó que el efecto de las medidas recién se sentirá en el índice de precios al consumidor correspondiente a noviembre: "Esperamos que el índice de alimentos (de octubre) esté por debajo del 2,9%", señaló.  
  
"La inflación no se puede resolver sólo desde el control de precios y eso lo sabemos todos, pero sabiendo que hay una oferta monopólica, sobre esa oferta hay que regular los consumos esenciales; esa es la primera tarea y la más inmediata de la Secretaria, que me planteó y me pidió el Presidente", concluyó.