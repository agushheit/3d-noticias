---
category: Agenda Ciudadana
date: 2021-04-04T08:10:43-03:00
thumbnail: https://assets.3dnoticias.com.ar/sinopharm.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Vacuna Sinopharm: la provincia de Santa Fe recibirá más de 70 mil dosis'
title: 'Vacuna Sinopharm: la provincia de Santa Fe recibirá más de 70 mil dosis'
entradilla: El gobierno nacional reparte entre este sábado y el lunes en todo el país
  más de 900 mil dosis de la vacuna china contra el coronavirus.

---
El Gobierno superará las 6,6 millones de dosis entregadas en las 24 jurisdicciones con la distribución prevista entre hoy y el lunes de una nueva partida de 909 mil dosis de la vacuna Sinopharm, proveniente de la ciudad de Beijing, República Popular China, según confirmaron fuentes oficiales.

Según el criterio dispuesto por el Ministerio de Salud, relacionado a la cantidad de población de cada distrito, a la provincia de Buenos Aires llegarán 348.300 dosis; a la Ciudad Autónoma de Buenos Aires, 61.200; a Catamarca, 9.000; a Chaco, 24.300; a Chubut, 12.600; a Córdoba, 75.600; a Corrientes, 22.500; a Entre Ríos, 27.900; y a Formosa, 12.600.

Por otra parte, a Jujuy le corresponden 15.300; a La Pampa, 7.200; a La Rioja, 8.100; a Mendoza, 39.600; a Misiones, 25.200; a Neuquén, 13.500; a Río Negro, 15.300; a Salta, 28.800; a San Juan, 16.200; a San Luis, 10.800; a Santa Cruz, 7.200; a Santa Fe, 70.200; a Santiago del Estero, 19.800; a Tierra del Fuego, 3.600 y a Tucumán, 34.200.

El embarque llegó el jueves último por la noche al Aeropuerto Internacional de Ezeiza en el vuelo AR1051 de Aerolíneas Argentinas.

Cada uno de los cargamentos que arriba al país atraviesa por un proceso de desaduanaje, recepción, control térmico, conteo, fraccionamiento y acondicionamiento, para luego ser distribuidas a las 24 jurisdicciones, donde se repite el mismo proceso hasta la llegada a los centros de vacunación, apuntaron los voceros gubernamentales.

Por otra parte, este sábado también finalizó la distribución en cada una de las jurisdicciones de la nueva partida de 306.000 dosis del componente 1 correspondientes al décimo embarque de vacunas Sputnik V, trasladado en un vuelo aterrizado en Ezeiza el martes último.