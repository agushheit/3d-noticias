---
category: La Ciudad
date: 2021-12-03T06:15:31-03:00
thumbnail: https://assets.3dnoticias.com.ar/VACUNACION.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'Covid: en la ciudad de Santa Fe, el 75% de la población ya tiene las 2 dosis
  aplicadas'
title: 'Covid: en la ciudad de Santa Fe, el 75% de la población ya tiene las 2 dosis
  aplicadas'
entradilla: "Además, casi el 90% de la ciudadanía de esta capital cuenta con una inoculación.
  Leve ascenso de los indicadores que muestran los casos de Covid acumulados en dos
  semanas.\n\n"

---
En las últimas semanas se registró un incremento de casos de Covid-19 a nivel país. De un promedio de 1.200 contagios diarios (en las post segunda ola), ahora la cartera sanitaria está notificando entre 2 mil y 2.200. Este contexto comienza a repercutir en la situación sanitaria provincial, pero de momento no se ven cambios significativos. En el contexto local, hoy esta capital muestra indicadores epidemiológicos estables y hay un buen porcentaje de la ciudadanía con esquema completo de vacunación: el 75% de la población total ya tiene dos dosis de las vacunas anti Covid disponibles.

 El dato se desprende del último informe de Sala de Situación, que elabora semanalmente la dirección de Salud del municipio local (corresponde a la Semana Epidemiológica N° 47). El reporte consigna también que hay un 89,2% de cobertura poblacional total con una dosis. Cabe recordar que el gobierno local proyecta el total demográfico de Santa Fe capital en 426.145 personas.

El total de vacunas aplicadas es a la fecha de 714.837; de ese número global, 382.835 fueron primeras dosis, y 321.646 segundos componentes. Otro dato significativo es la cobertura por grupos de riesgo y población en general, incluyendo a los menores de entre 3 y 17 años inscriptos para recibir la inoculación, como así también la cantidad de dosis adicionales y de refuerzo ya inoculadas.

Así, el personal de salud de la ciudad está cubierto con esquema completo en un 95,2%; el personal esencial (fuerzas de seguridad, docentes), en un 92,3%; en la franja poblacional de 60 a 69 años, hay un 96,1% de vacunados con segundas dosis; un 98,2% en la franja de 70 a 79 años, y 97% de cobertura total en 80 años o más.

 En lo que respecta a la vacunación pediátrica, el porcentaje de menores de 3 a 11 con condiciones priorizadas que ya tiene dos dosis es del 47,9% (31,8% sin comorbilidades). Dentro de la franja etaria de 12 a 17 años, hay un 88,7% de niños (con condiciones priorizadas) que tiene esquema completo, y un 78,9% de vacunados sin ninguna comorbilidad.

 Un dato atípico: el total de personas con residencia en esta capital que se enfermaron al menos dos veces (es decir, los "recontagiados" con el virus) son 726, y el total de los santafesinos que se infectaron de SARS-CoV-2 al menos una vez es de 54.925. La pandemia ha dejado a la fecha 855 fallecimientos por Covid-19 en la ciudad de Santa Fe: son 47 las personas que aún están cursando la enfermedad y no se recuperan.

Dos indicadores

La Razón (indicador evaluado en el informe y que toma el número de casos confirmados acumulados respecto de las dos semanas previas) hoy es de 1,0: subió un 33% respecto de la semana anterior, ya que ese número era de 0,75. Y la Incidencia (total de casos confirmados en las últimas dos semanas epidemiológicas por cada 100 mil habitantes) es hoy de 14,1 casos; también hubo una suba de este indicador en un 30%, ya que ese valor era en la semana anterior de 10,8 casos (siempre cada 100 mil habitantes). 

El reporte difundido semanalmente es de elaboración propia de la Dirección de Salud y Promoción Comunitaria municipal, sobre la base de los datos del Sistema de Atención Primaria de la provincia de Santa Fe (Ministerio de Salud).

La pandemia ha dejado a la fecha 855 fallecimientos por Covid-19 en la ciudad de Santa Fe: son 47 las personas que aún están cursando la enfermedad y no se recuperan.