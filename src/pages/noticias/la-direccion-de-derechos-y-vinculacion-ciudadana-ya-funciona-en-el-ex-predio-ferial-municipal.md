---
category: La Ciudad
date: 2021-10-15T06:00:32-03:00
thumbnail: https://assets.3dnoticias.com.ar/VINCULACION.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Dirección de Derechos y Vinculación Ciudadana ya funciona en el ex Predio
  Ferial Municipal
title: La Dirección de Derechos y Vinculación Ciudadana ya funciona en el ex Predio
  Ferial Municipal
entradilla: 'Las oficinas brindan atención al público de lunes a viernes, de 7.30
  a 13, en el ala suroeste del edificio de Las Heras 2883. '

---
La Dirección de Derechos y Vinculación Ciudadana tiene nueva sede: desde este miércoles brinda atención presencial en el ala suroeste del ex Predio Ferial, en Las Heras 2883, de lunes a viernes, de 7.30 a 13. De este modo, a partir de ahora allí deberán dirigirse los vecinos de la ciudad que deseen recibir asesoramiento o completar trámites en la Oficina del inquilino, Atención a Consumidores, Mediaciones Comunitarias, Objetos Perdidos y Documentación Extraviada.

Franco Ponce de León, quien está al frente de la dirección, destacó que “ahora las oficinas están en un lugar municipal propio, y tienen las características adecuadas para brindar una mejor atención al público y para el desarrollo de tareas del personal”. Las oficinas ubicadas frente a la Terminal de Ómnibus fueron refaccionadas y acondicionadas por la Municipalidad para facilitar también las tareas de los trabajadores a la hora de responder a las consultas, prestar asesoramiento y llevar adelante las reuniones de mediación y las conciliaciones.

El funcionario resaltó que el traslado de sede se inscribe “en una decisión del intendente Emilio Jatón de generar una mayor descentralización de las áreas municipales. La pandemia no nos posibilitó hacerlo en los tiempos que nosotros hubiésemos querido, pero ya estamos en las nuevas oficinas, en un lugar renovado y que es más accesible para el público”.

Además de brindar atención personalizada y presencial en las nuevas oficinas, la Dirección de Derechos y Vinculación Ciudadana recepciona consultas a través de distintos canales como la Línea de Atención Ciudadana 0800 777 5000, el correo electrónico: derechosyvinculacionciudadana@santafeciudad.gov.ar y en la página web oficial del municipio, [www.santafeciudad.gov.ar](http://www.santafeciudad.gov.ar/), en el apartado de acceso a la Oficina Virtual. Todos los trámites son gratuitos.

**Mayor consulta**

En el contexto de pandemia, las denuncias a entidades bancarias asociadas a la defensa del consumidor fueron las más realizadas, seguidas por denuncias relativas al servicio de telefonía y la compra de electrodomésticos. No obstante, Ponce de León destacó que “un área que tuvo un crecimiento de forma exponencial y que desde el municipio sumó mayor personal es la Oficina del Inquilino”.

En ese sentido, el funcionario contextualizó que “desde el año pasado rige la nueva Ley de Alquileres y hubo decretos nacionales que establecieron la prohibición de desalojos, lo que generó muchas dudas tanto de inquilinos como de propietarios. Eso hizo que esta oficina tuviera requerimientos muy importantes de atención, de generación de encuentros virtuales. Ahora, la baja en los casos de Covid-19, nos va a permitir tener encuentros presenciales para seguir brindando información sobre esta nueva ley”.