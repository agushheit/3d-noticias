---
category: Agenda Ciudadana
date: 2021-11-11T06:15:33-03:00
thumbnail: https://assets.3dnoticias.com.ar/domingo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Elecciones: aseguran que el domingo a las 21 ya estará el 80% de las mesas
  cargadas'
title: 'Elecciones: aseguran que el domingo a las 21 ya estará el 80% de las mesas
  cargadas'
entradilla: 'Así lo indicaron desde la Secretaría Electoral. Afirmaron además que
  ya no habrá filas en las puertas de las escuelas para votar este domingo de elecciones.

'

---
El secretario Electoral de la provincia, Pablo Ayala, repasó algunas cuestiones que el electorado tenga en cuenta este domingo al celebrarse las Elecciones Generales provinciales y nacionales. Además, estimó que “a las 21 horas vamos a tener cerca del 80% de las mesas de toda la provincia, ya cargadas”.

El funcionario se refirió, entre otros temas, a los protocolos que el electorado santafesino tendrá que respetar a la hora de ir a sufragar. En ese sentido, reconoció que “la cuestión sanitaria acompañó, bajaron los índices de contagio, lo que permitió tomar algunas flexibilidades respecto de las elecciones PASO”, y agregó que “ya no va a haber filas fuera de los establecimientos, sino que los electores podrán ingresar e ir hasta su mesa de votación”.

No obstante, insistió en los protocolos para el electorado de la provincia: “hay que usar barbijo y que esté bien colocado, lo que se controlará al ingreso de los establecimientos; y respetar el distanciamiento mientras se espera el turno para votar”.

Ayala indicó también que el procedimiento a la hora de sufragar será similar al de las PASO: primero, “para la elección nacional, en la mesa le van a pedir al votante que retire un sobre ya firmado e ingrese al cuarto oscuro. Cuando sale, tras introducir el sobre en la urna, le dan la boleta única para las elecciones provinciales, irá al box de votación y luego colocará esa boleta en la urna”.

Otro aspecto que remarcó el secretario Electoral es que durante toda la jornada electoral, de 8 a 18 horas, “el electorado con alguna discapacidad, las embarazadas y los mayores de 60 años pueden solicitar prioridad para votar”.

Para las 21 del domingo ya estará el 80 por ciento de las mesas cargadas con los datos en toda la provincia. Foto: archivo UNO Santa Fe.

Asimismo, el funcionario mencionó que “todas las personas que componen el electorado provincial que no votaron en la PASO, igualmente están habilitados para sufragar en las elecciones Generales”, en tanto que este 11 de noviembre “vence el plazo para que los que no votaron en las PASO justifiquen el no voto”.

**Logística**

Ayala indicó que “estamos terminando con las cuestiones logísticas. Hoy estamos despachando los camiones con las urnas al Correo de Santa Fe, entregamos todo el material para que el sábado las fuerzas de seguridad lo distribuyan y esté disponible el domingo, a las 8 de la mañana”.

En tanto, “terminada la jornada electoral el Ejercito, junto al Correo, remite el repliegue documental, junto con las urnas, al Tribunal Electoral”.

“Simultáneamente seguimos con capacitaciones virtuales a los presidentes de mesa con cuatro charlas diarias”, dijo el secretario Electoral.

**Qué se vota el domingo**

Asimismo, Ayala recordó que “en los 365 distritos de la provincia votamos senadores y diputados nacionales, intendentes, en 14 localidades, y presidentes comunales, consejos municipales y comisiones comunales”.

Además, precisó que “tenemos dos sistemas de escrutinio provisorio, uno nacional, que es el primero que se realiza en la mesa, por orden de importancia, primero senador, diputado después; luego empezamos con el escrutinio de mesa de las categorías provinciales; se labran las actas y el certificado del escrutinio”.

“Una vez que está terminada la mesa se transfieren los datos a los centros de procesamiento, tanto de Rosario, como de Santa Fe, ingresa por las dos bocas de acceso, y si coinciden los datos impacta en sistema de escrutinio provisorio”, detalló el secretario Electoral.