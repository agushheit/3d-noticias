---
category: La Ciudad
date: 2020-12-30T10:42:32Z
thumbnail: https://assets.3dnoticias.com.ar/3012-licencia-de-conducir.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Volvió a prorrogarse el vencimiento de las licencias de conducir
title: Volvió a prorrogarse el vencimiento de las licencias de conducir
entradilla: La disposición municipal, en consonancia con lo establecido por la APSV,
  alcanza a las credenciales que caducan entre el 1 de enero del 2021 y el 31 de marzo
  del mismo año.

---
La Municipalidad informa que fue establecida una **nueva prórroga preventiva y excepcional de las Licencias Nacionales de Conducir** cuyos vencimientos operen entre el 01 de enero de 2021 y el 31 de marzo del mismo año. La extensión será por 90 días corridos, contados a partir de la fecha de vencimiento de cada carné.

La nueva prórroga, establecida en consonancia con lo dispuesto por la Agencia Provincial de Seguridad Vial (APSV), amplía la vigencia de todas las licencias por tres meses, independientemente del tipo que sean.

**La intención es evitar la aglomeración de personas** que deban realizar este trámite en los centros de emisión de licencias de conducir.

La medida se inscribe en la Resolución Interna N° 57 de la Secretaría de Control y Convivencia Ciudadana de la Municipalidad de Santa Fe, con fecha del 28 de diciembre, en adhesión a la normativa de la Agencia Provincial de Seguridad Vial. Además, complementa las resoluciones N° 23 del 17 de junio, N° 27 del 31 de julio, N° 35 del 27 de agosto, N° 45 del 17 de septiembre y Nº 50 del 27 de octubre del presente año.

Quienes deban tramitar por primera vez su licencia de conducir, pueden obtener un turno para el mes de enero, ingresando al sitio oficial [www.santafeciudad.gov.ar](https://santafeciudad.gov.ar/). Allí deberán buscar el botón de «Turnos» y seleccionar «Licencias de conducir». 

Se atiende en el Centro de Educación Vial del Parque Garay, de lunes a viernes, de 8 a 16 hs; en los Distritos La Costa y Noreste, de lunes a viernes, de 8 a 12 hs; y en la Estación Belgrano (solo para las charlas), de lunes a viernes, 8 y 10.30 hs.