---
category: Agenda Ciudadana
date: 2021-11-05T06:15:36-03:00
thumbnail: https://assets.3dnoticias.com.ar/SAINM.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Sain fue destituido y quienes votaron a favor y en contra hablaron de daño
  institucional
title: Sain fue destituido y quienes votaron a favor y en contra hablaron de daño
  institucional
entradilla: Por amplia mayoría el director del Organismo de Investigaciones fue destituido
  del cargo e inhabilitado a formar parte del MPA por un plazo de 10 años

---
Este jueves por la tarde se desarrolló la sesión conjunta de senadores y diputados para sellar la suerte de Marcelo Sain como director del Organismo de Investigaciones (OI) del MPA. En la votación hubo 12 senadores y 38 diputados a favor de la destitución por mal desempeño de sus funciones, mientras que cuatro senadores y nueve diputados votaron en contra de lo que aconsejaba el dictamen de la comisión de Acuerdos.

Además, se le prohíbe ingresar al ámbito del MPA a partir del momento en que fuera aprobado el dictamen. Asimismo Sain fue inhabilitado para acceder al MPA por un plazo de 10 años tal como lo establece la Ley 13.013.

El primero en tomar la palabra fue Lisandro Enrico, el senador radical que llevó adelante la acusación en el proceso disciplinario. Enrico hizo un repaso de las normas que violó Sain, aunque dijo que lo más grave era el Artículo 89 de la Constitución que establece que los miembros del poder judicial no pueden actuar de manera alguna en política.

En la sesión conjunta 38 diputados y 12 senadores votaron a favor de la destitución de Marcelo Sain como director del Organismo de Investigaciones del MPA.

"Era un hombre haciendo política dentro del Poder Judicial. Lo que aporté como prueba es una recopilación de tuits, que es la red social que más utiliza", señaló y también lo criticó en su paso como ministro de Seguridad donde dijo que "lo único que hizo fue perseguir chicos de 15 años que hacían fiestas clandestinas. Después no agarró a nadie".

Los argumentos más sólidos del PJ para defender a Sain llegaron de boca de la diputada Matilde Bruera, quien aseguró que su compromiso con esa causa se las debía al resto de los legisladores "que llevaron adelante este proceso arbitrario de persecución" política. "No admitimos la violación del Estado de derecho", fustigó.

"El acusador no puede votar. Esto es grave y está resuelto por la Corte", dijo y agregó: "También se violó el principio de congruencia que garantiza el derecho de defensa. Todo este procedimiento es nulo".

Por otra parte, se refirió a "la gran crisis institucional que hay en la provincia" y dijo que eso se debe a que no hay garantías. "Desde la Legislatura se sometió al MPA", afirmó en referencia a las leyes que le dieron potestad a los legisladores para juzgar a los fiscales y miembros del MPA.

Por su parte, Rubén Giustiniani aportó sus argumentos al voto negativo. "Lamentamos este paso que da la Legislatura porque es negativo en dos aspectos: en el debate casi no se habló de la libertad de expresión; lo otro, es que es una desproporción absoluta la acusación que se realiza con la decisión que se toma".

Asimismo, el diputado de Igualdad y Participación aclaró que este será "un capítulo más, no el último, de una disputa de poder que no tiene que ver con cómo se resuelve el tema de la seguridad".

Finalmente señaló su preocupación por el antecedente que se está generando con esta votación. "Esto está abriendo una discrecionalidad futura donde puede haber otras mayorías parlamentarias", afirmó.

Entre otras alocuciones a favor de la destitución de Sain se pronunció Julián Galdeano, quien aseguró que en la provincia se vive un problema institucional por los controles entre los poderes. "No es de ahora, pero se va agravando. Se judicializa la política y se politizan los procesos judiciales. Sain fue trascendente para que esto escale de una manera vertiginosa", expresó.

Mientras que Joaquín Blanco dijo que "nadie en la provincia puede desconocer que Sain tenía un pie en el Ejecutivo y otro en el Judicial" y que, además, tenía "acceso a información que utilizaba políticamente".

"Sain usó esa información para perseguir rivales políticos y periodísticos", cuestionó y defendió el proceso donde dijo que "hubo derecho a defensa, fue constitucional y democrático".

Por último, advirtió que el gobierno de Omar Perotti está buscando una contrareforma al sistema penal. "Nosotros vamos a defender el sistema penal", afirmó y pidió que se cubran de manera definitiva los cargos de los fiscales regionales de Rosario y Venado Tuerto que hoy están siendo ocupados de forma interina.

En tanto, Leandro Busatto, que votó en contra de la destitución, pidió a sus pares "ir al fondo de la cuestión, más allá de Sain" y aseguró que "el problema institucional de la provincia de Santa Fe viene desde hace tiempo y todos los actores somos parte".

"Nosotros votamos en contra por cuestiones políticos institucionales, no políticos personales", señaló y advirtió que si el problema con Sain es su posición política sus subalternos también tienen su pasado de militancia y que algunos, como Victor Moloeznik –el vicedirector del Organismo de Investigaciones– fue parte del Ministerio de Justicia durante el gobierno del Frente Progresista. Mientras que otros funcionarios judiciales fueron candidatos en las elecciones de 2019.