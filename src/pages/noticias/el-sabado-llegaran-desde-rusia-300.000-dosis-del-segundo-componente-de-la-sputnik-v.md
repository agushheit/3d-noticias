---
category: Agenda Ciudadana
date: 2021-01-13T07:33:19Z
thumbnail: https://assets.3dnoticias.com.ar/AA-avion-sputnik.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: '3DNoticias - Fuente: Télam'
resumen: El sábado llegarán desde Rusia 300.000 dosis del segundo componente de la
  Sputnik V
title: El sábado llegarán desde Rusia 300.000 dosis del segundo componente de la Sputnik
  V
entradilla: El vuelo de Aerolíneas Argentinas partirá el jueves, tendrá una duración
  de 40 horas aproximadamente y estará integrado por un total 20 personas para poder
  desarrollar la operación sin paradas técnicas.

---
Un nuevo vuelo de Aerolíneas Argentinas partirá el jueves a la noche rumbo a Moscú para traer al país las 300.000 dosis del segundo componente de la vacuna Sputnik V contra el coronavirus, cuya primera dosis ya se está aplicando en todas las provincias.

«Este jueves a las 21 horas parte el segundo vuelo de @Aerolineas_AR rumbo a Moscú para traer al país otras 300.000 dosis de la vacuna Sputnik V. Un trabajo coordinado entre diferentes áreas de gobierno para cumplir con esta tarea logística fundamental en la pelea contra el Covid 19», anunció el titular de Aerolíneas Argentinas, Pablo Ceriani.

En su cuenta de Twitter, Ceriani indicó además que «el vuelo, que está previsto que arribe al país el sábado al mediodía, tendrá una duración de 40 hs aprox. y estará integrado por un total 20 personas para poder desarrollar la operación sin paradas técnicas».

[![](https://assets.3dnoticias.com.ar/tweet-ceriani-120121.webp)](https://twitter.com/ceriani_pablo/status/1348963626888163328)

Un reporte difundido por Nomivac (Registro Federal de Vacunación Nominalizado) da cuenta que hasta las 19 de ayer se habían vacunado un total de 138.218 personas en 477 localidades de todo el país, todas ellas personal de la salud.

Buenos Aires es la provincia con más cantidad de personas vacunadas, con un total de 46.670 personas, seguida por Córdoba (14.123), Santa Fe (8.582) y la Ciudad de Buenos Aires (7.950).

Por su parte, la secretaria de Salud Carla Vizzoti confirmó, también a través de su cuenta en Twitter:

[![](https://assets.3dnoticias.com.ar/tweet-vizzotti-120121.webp)](https://twitter.com/carlavizzotti/status/1348953474738892800)

Y amplió en la misma red social: «la vacuna Sputnik V es la única cuyo esquema de vacunación consta de dos componentes, en primer lugar, el componente Ad26, y luego de un intervalo mínimo de 21 días, el segundo componente Ad5. Argentina planea administrarlas según esta indicación». Confirmando, de ese modo, **que cada persona será inmunizada con dos dosis de la Sputnik V y no con una**, dejando de lado la posibilidad de dar una sola dosis con la que ella había especulado en una entrevista reciente con Página 12.

«El Gobierno argentino adquirió al Fondo de Inversión Directa de Rusia 15 millones de esquemas de vacuna Sputnik V, 30 millones de dosis, que llegarán en función del contrato firmado, entre los meses de diciembre y marzo de 2021», aseguró la funcionaria.

«Desde el inicio, analizamos acciones sanitarias para dar respuesta a la pandemia, entablamos negociaciones con todos los productores para contar con vacunas seguras y eficaces en cantidad suficiente y en forma oportuna», agregó.

«Analizamos en consenso con expertos, expertas y las 24 jurisdicciones, las posibles estrategias de vacunación para lograr el objetivo que ansiamos: avanzar hacia el principio del fin de la pandemia y salvar la mayor cantidad de vidas posibles», finalizó.

El pasado 24 de diciembre el vuelo especial de Aerolíneas Argentinas que trajo las primeras 300.000 dosis llegó a la Argentina, trayendo a bordo las primeras dosis de las Sputnik V.

En ese vuelo también regresaron al país la Secretaria de Acceso a la Salud, Carla Vizzotti, la asesora presidencial Cecilia Nicolini, y la delegación de la Anmat que había viajado a Moscú para certificar el proceso de producción de la vacuna producida por el Instituto Gamaleya.