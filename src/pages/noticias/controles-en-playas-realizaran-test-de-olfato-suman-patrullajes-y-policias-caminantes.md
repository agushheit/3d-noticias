---
category: La Ciudad
date: 2021-01-08T10:30:31Z
thumbnail: https://assets.3dnoticias.com.ar/2021-01-07-playas-controles.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Diario UNO'
resumen: 'Controles en playas: realizarán test de olfato, suman patrullajes y policías
  caminantes'
title: 'Controles en playas: realizarán test de olfato, suman patrullajes y policías
  caminantes'
entradilla: Las medidas llegan luego de los episodios de violencia en costanera este
  y las aglomeraciones de personas en espacios públicos.

---
Luego de los episodios de violencia en Costanera Este y las postales que pusieron en evidencia grandes aglomeraciones de personas en diferentes playas y espacios públicos de la ciudad en plena pandemia, el gobierno provincial y el municipal definieron operativos de control.

**Todo se inició con un fuerte planteo de los guardavidas, quienes se habían declarado en estado de alerta a raíz de lo acontecido.** Hubo reuniones entre Sugara (Sindicato de guardavidas) y funcionarios municipales y provinciales de los ministerios de Seguridad y de Trabajo.

Fue así que se definieron medidas tendientes a mejorar la seguridad y los protocolos en las playas y espacios públicos de la ciudad de Santa Fe.

En este sentido, el coordinador del Ministerio de Seguridad, Facundo Bustos, explicó que «a raíz del incremento en la circulación de gente y en las aglomeraciones que vimos en distintos puntos de la ciudad, decidimos reforzar los patrullajes dinámicos y con policías caminantes que forman parte del **Operativo Verano 2021**, para evitar grandes concentraciones de personas».

Asimismo, «vamos a realizar junto al Ministerio de Salud test de olfato o anosmia en los ingresos a las diferentes playas santafesinas y campañas de concientización para toda la población, en especial para los más jóvenes», agregó Bustos.

Los puntos a reforzar son: la costanera este y oeste, los espigones, el Parque del Sur, Parque Garay y los diferentes espacios públicos que componen la ciudad.

El **Operativo Verano 2021** es un trabajo coordinado entre la Policía de Santa Fe y las diversas Direcciones Generales que la componen, como la Guardia de Seguridad Rural Los Pumas, Seguridad Vial, la Policía de Acción Táctica (PAT) y la Policía Comunitaria.

Además, cuentan con la colaboración de las Fuerzas federales como Prefectura, Policía Federal, Gendarmería y la Policía de Seguridad Aeroportuaria. **El despliegue del operativo se determina de acuerdo a las zonas más pobladas, donde se profundiza con más patrulleros, motos y presencia policial, según la demanda y las concentraciones de personas.**

Además, el Ministerio de Salud, en el marco del Covid 19, tiene dispuesto reforzar en las zonas de mayor afluencia de gente, con más ambulancias, testeos y médicos de Guardia.