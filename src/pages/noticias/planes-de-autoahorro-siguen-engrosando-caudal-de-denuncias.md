---
category: Agenda Ciudadana
date: 2021-04-02T07:12:33-03:00
thumbnail: https://assets.3dnoticias.com.ar/camionetajpg.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Diario UNO
resumen: Planes de autoahorro siguen engrosando caudal de denuncias
title: Planes de autoahorro siguen engrosando caudal de denuncias
entradilla: Esta semana el gobierno nacional imputó a ocho compañías que ofrecen planes
  de autoahorro que genera desazón en los usuarios que sueñan con un 0 km

---
Hernán Müller se inscribió hace un tiempo en un plan de Ahorro en una agencia en Paraná para acceder a una camioneta Chevrolet Tracker Premier. Es médico y atiende en diferentes nosocomios de la provincia, por lo que viaja constantemente y optó por este modelo, que es acorde a sus necesidades. Pero al salir sorteado le indicaron que debía pagar alrededor de 500.000 pesos más, debido a la implementación del impuesto al Lujo, un gravamen que dispuso la AFIP a partir de marzo para los vehículos que superen los 2.250.000 pesos de precio al público, lo que generó aumentos de entre el 25% y el 50% en los valores originales.

“Me suscribí hace unos meses, cuando salió al mercado la nueva Tracker Premier. En febrero me avisan que había salido adjudicado por sorteo. Se comunica la empresa con nosotros y nos hace saber que para sacar el vehículo, debido a que había habido una modificación del precio a partir de ese mes a la camioneta la había afectado el nuevo impuesto al lujo”, dijo a UNO.

Según contó, recurrió a la cláusula N° 13 de su contrato, que establece que, llegado al caso, el beneficiario puede hacer cambio de modelo. “El contrato dice que se puede acceder a cualquier modelo de la empresa que esté en comercialización. Hablé con la empresa para hacer el cambio por una Tracker LTZ, que por su precio no está alcanzada por el impuesto al Lujo, y me dicen que no hay stock porque no está entrando de importación. Entonces solicité cambiar al modelo Cruze premier, que es de fabricación nacional y tampoco tiene stock”.

**No tienen**

Müller aseguró que un empleado de la agencia le confió que “están vendiendo vehículos que no tienen”, y esto incrementó su malestar. Por esto decidió hacer público su caso y evalúa seguir adelante con una demanda judicial. En este marco, señaló: “Además de la agencia, me contacté de la misma manera con la fábrica de Chevrolet y me dijeron que se tiene que hacer cargo del cambio de modelo la agencia. Entre idas y vueltas, y mientras se tiraban la pelota unos a otros, en la agencia me pasaron una lista de vehículos que son de una gama muy inferior a lo que estamos pagando, y son modelos viejos. Estoy haciendo un esfuerzo enorme para tener el vehículo que preciso para viajar a diario y me salen con esto. Ni que hablar de que un año aumentaron las cuotas más del 100%”.

Sin llegar a un acuerdo, la agencia se comunicó por teléfono con él y le ofreció “comprarle” el plan”. Le ofertaron 400.000 pesos. “Al no tener disponibilidad de los vehículos que quiero, me dieron esta opción y me dieron dos días para pensarlo. Viendo que lamentablemente no iba a poder acceder al vehículo que yo quería, acepté”.

Cuando ya creía que estaba resuelta su situación, recibió otro llamado inesperado, que se sumó a los vericuetos que venía sufriendo en esta transacción. “Pasó una semana y los llamé para que apuraran el trámite, ya que necesitaba el dinero para poder hacer otro negocio, pero resulta que me informaron vía telefónica, que no es un medio válido tampoco, que desde la gerencia decidieron no comprarme el plan”, lamentó.

Acto seguido, precisó: “Mientras estoy esperando completar el trámite me dicen que no lo van a hacer. Me perjudicaron totalmente, porque a la altura del mes que estamos, me dejaron fuera de la posibilidad de acceder a la adjudicación del vehículo, porque ya los plazos se vencieron”.

“Me siento estafado. Sigo mandando mail y me llaman por teléfono, dándome una excusa tras otra y pasándose la pelota”, subrayó, visiblemente decepcionado.