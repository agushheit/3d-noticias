---
category: Agenda Ciudadana
date: 2021-03-05T07:09:54-03:00
thumbnail: https://assets.3dnoticias.com.ar/cfk.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Clarín
resumen: En la causa dólar futuro, Cristina Kirchner acusó al Poder Judicial de ser
  “un sistema podrido y perverso”
title: En la causa dólar futuro, Cristina Kirchner acusó al Poder Judicial de ser
  “un sistema podrido y perverso”
entradilla: La vice dio un discurso político ante la Sala I de la Casación. Volvió
  a hablar de “lawfare” y apuntó contra Mauricio Macri y los medios. No pidió que
  la sobresean sino “que apliquen la Constitución”.

---
Durante más de cincuenta minutos y con un duro discurso que incluyó una fuerte defensa política, la vicepresidenta Cristina Kirchner expuso en la causa conocida como Dólar Futuro. En su descargo, con el que busca el sobreseimiento, acusó al Poder Judicial de ser "un sistema podrido y perverso", volvió a hablar de "lawfare" para defenderse de las causas de corrupción en las que está acusada y apuntó contra Mauricio Macri y los medios de comunicación.

La ex mandataria realizó una exposición este jueves vía zoom, desde su oficina en el Senado. Durante la instrucción del caso se dio por corroborado que Cristina y su entonces ministro de Economía, Axel Kicillof, junto a otros ex funcionarios, "cometieron un fraude descomunal al patrimonio público", que le habría costado al Estado "55 mil millones de pesos". El delito imputado es el de administración fraudulenta.

> Fue el primero de los ocho expedientes contra la ex presidenta que fue elevado a juicio oral.  

En Comodoro Py esperaban que se trate de un discurso con un contenido más bien político. Así fue. En el arranque de su exposición, la ex mandataria volvió a hablar de "lawfare" y criticó con dureza a la Justicia, al acusarla de "intromisión y manipulación en los procesos electorales y en la política en general".

La audiencia es un trámite propio de las instancias de queja que llegan a la Cámara de Casación, el máximo tribunal penal. La primera fecha fijada para la cita había sido el 1 de marzo, y con ese detalle la vice inició su descargo, recordando que se trata del día de la Asamblea Legislativa, que ella como titular del Senado debía encabezar.

"Ahí dije 'evidentemente el lawfare sigue en su pleno apogeo'. Porque si fue un error, es más grave todavía. Me preocuparía sobremanera que el tribunal ignorara artículos de la Constitución que establecen que todos los primero de marzo, el Presidente de la República tiene la obligación constitucional de ir a rendir cuentas, y obviamente está acompañada por la vicepresidente, que es la que preside la Asamblea Legislativa, tal como lo marca la Constitución", aseguró.

Acto seguido, comenzó a adentrarse en el expediente que instruyeron el fallecido juez Claudio Bonadio y el fiscal Eduardo Taiano. En el requerimiento de elevación a juicio,  Taiano sostuvo que la vicepresidenta era responsable del delito de “administración fraudulenta” que se había cometido por las instrucciones del Poder Ejecutivo Nacional a las autoridades del Banco Central, "con la anuencia de la Comisión Nacional de Valores", para vender en un breve periodo "un importante volumen de contratos de dólar futuro a valores ficticios, obligando abusivamente a la entidad generando un grave perjuicio a las arcas del Estado”.

En ese momento se señaló que la pérdida para el Banco Central fue de 54.921.788.702,40 pesos, y que "la defraudación a la administración pública fue producto del acuerdo y coordinación de los más encumbrados funcionarios del Estado, quienes desde sus cargos arbitraron las medidas necesarias para su consecución".

Para Cristina Kirchner se trata de una "persecución política", pues la acusan por "medidas de Estado que no pueden judicializarse". Entonces, ante los jueces de la Sala I sostuvo que el expediente es un "'leading case', no solo en el tema del lawfare, sino también en la "intromisión y manipulación del Poder Judicial en los procesos electorales y en la política en general en la República Argentina".

El caso inició en noviembre de 2015, cuando se sabía que habría una segunda vuelta para elegir al sucesor de Cristina Kirchner en la Casa Rosada. Por ello, la vicepresidenta dijo que el Poder Judicial con su accionar "incidió en el proceso electoral", contribuyendo a que Mauricio Macri llegue a la presidencia "y ellos vinieron para devaluar", aseveró.

El recurso ante la Casación inició después de que el Tribunal Oral Federal 1 (TOF 1), a cargo del caso, avaló la decisión del fiscal Diego Velasco que pidió que inicie el debate oral. Los imputados habían planteado la “sinrazón de judicializar una decisión claramente política”, después de una pericia realizada por los especialistas de la Corte, que concluyó que en las operacion del dólar futuro no hubo perjuicio.

Por ello pidieron el sobreseimiento y que no inicie el juicio. Velasco rechazó los pedidos, el TOF 1 lo avaló y Cristina Kirchner llegó con su queja a la Casación.

Este jueves aprovechó la audiencia para continuar con su ataque al Poder Judicial: en varios segmentos de su alocución, Cristina aseguró que los jueces son los que defienden al "poder económico" y hasta los vinculó con las Fuerzas Armadas.  

"El poder que es permanente en la Argentina, es un poder económico permanente, donde primero fueron las Fuerzas Armadas y ahora son ustedes (a los jueces) los que velan por sus intereses", afirmó. Fue más allá al plantearle a los jueces que "no pueden comportarse de forma corporativa" y se refirió a "la permanencia en los cargos", ya que pese a la edad máxima impuesta por la Constitución de los 75 años "siempre encuentran la manera de permanecer. Nosotros somos democráticos, a nos elije o no la gente cada dos y cuatro años".

La ex presidenta apuntó una y otra vez durante su discurso contra dos de los jueces de la Cámara Federal de Casación Penal, pero principalmente contra Daniel Petrone, quien preside la Sala I y a quien nombró de manera insistente, responsabilizándolo entre otras cosas, de "los problemas financieros del país" y su multimillonario "endeudamiento que no nos permite hacer nada hoy".

La Sala I del máximo tribunal Penal está integrada, además de Petrone, por Diego Barroetaveña y Ana María Figueroa, los mismos que decidieron sobre la constitucionalidad de la Ley del Arrepentido, avalando los 31 testimonios de los imputados colaboradores que fueron centrales para la justicia en la causa conocida como los Cuadernos de las Coimas. En aquella ocasión, quien votó en disidencia fue Figueroa.

"Es bueno conocerle la cara a los jueces y los fiscales. Yo a usted lo estoy viendo doctor Petrone, pero antes no lo conocía. A la jueza Figueroa la conocía cuando denunciaba presiones. Al doctor Barroetaveña tampoco lo conocía, siempre el anonimato. Tampoco el fiscal Raúl Plee, que es el acusador. Yo siempre di la cara y la voy a seguir dando", lanzó.

La ex presidenta enumeró las razones por las que, según ella, no tiene sustento la denuncia que en 2015 hicieron Mario Negri y Federico Pinedo, y tras apuntar sus críticas al fallecido juez Claudio Bonadio, comparó su situación con la del ex mandatario Mauricio Macri y sus funcionarios.

En este punto hizo una vinculación con otro caso donde se había reclamado por el sobreseimiento dictado sobre Federico Sturzenegger. "Ustedes rechazaron un recurso nuestro y dictaron el sobreseimiento de los devaluadores seriales del país, los que endeudaron a los argentinos. El Presidente que nos volvió a endeudar devaluando y devaluando (por Macri), va a ver partidos de fútbol a Qatar, y nosotros sentados acá, junto a Axel Kicillof y Pesce", lanzó Cristina Kirchner señalando a la cámara.

**Críticas al gobierno de Macri y el rol de la Justicia**

"Mucha gente, cuando escucha hablar de lawfare, piensa que son cosas alejadas, pero las decisiones que toman los jueces impactan en la vida de las personas. Ellos (el gobierno anterior) habían venido a devaluar y lo hicieron. Todo se fue al demonio, hicieron una devaluación de un 40 o 50%, catastrófica y ustedes, el Poder Judicial, contribuyeron a que ese gobierno ganara las elecciones y son responsables de lo que pasó y está pasando en la Argentina, y me da mucha bronca porque la que sufre es la gente. No podemos dar aumentos a los jubilados porque estamos endeudados hasta acá", sentenció con la voz entrecortada, en el que fue el momento de mayor vehemencia de su exposición.

Y siguió: "Hoy los argentinos debemos 44 mil millones de dólares más y ustedes también son responsables, no miren para otro lado. Generan climas y así estamos".

En su ataque a la Justicia, la Vicepresidenta acusó a los jueces por el rol ante las denuncias de las mujeres que son víctimas de violencia de género. "Que se despabilen los argentinos. El Poder Judicial inside todos los días, también en las mujeres que son masacradas en homicidios espantosos cuando ustedes, los jueces y fiscales no hacen nada", disparó.

Ya en el cierre de su extensa exposición también le dedicó duras críticas al fiscal Carlos Stornelli, a quien acusó de "amenazar" al presidente Alberto Fernández.

"Ayer este fiscal amenazó al presidente. Dijo 'nos vamos a ver cuando él deje de ser presidente, nos vamos a ver de hombre a hombre'. No, de fiscal a acusado. Y todo el Poder Judicial que se calla la boca de este escándalo de que siga siendo fiscal de la Nación", lanzó.

Y continuó, con más críticas a la Justicia: "Siguen queriéndonos convencer a los argentinos de que el Poder Judicial es independiente. Estamos en un momento muy grave institucional, ustedes no pueden seguir comportándose como una corporación. No hay posibilidades de una vida democrática sana con funcionarios que parece que constituyen una aristocracia".

Para cerrar, y pese a que su abogado Carlos Beraldi le recomendó que pidiera el sobreseimiento, Cristina Kirchner dijo que no lo haría y les reclamó a los jueces "que apliquen la constitución".

"Aplicar la ley, que es lo que vengo reclamando siempre a todos y a todas, no solamente en un ejercicio donde articulan con los medios para decir lo que no es. Me hubiera gustado hacer esto personalmente. Quiero agradecerles la posibilidad de haber expuesto, pero no voy a pedir sobreseimiento, hagan lo que tengan que hacer", cerró la vicepresidenta.

Luego de Cristina Kirchner declaró ante la Sala I de la Cámara Federal de Casación Penal el gobernador Axel Kicillof, que también tuvo duros términos para la Justicia, a la que acusó de "ciega, sorda y partidizada".

Es la segunda vez que la actual vicepresidenta expone en un caso en el que se encuentra procesada, pero en instancias diferentes. El 2 de diciembre de 2019, previo a la jura como vicepresidenta, tuvo que declarar ante el Tribunal Oral Federal 2 (TOF 2) en el juicio donde está acusada de haber favorecido a Lázaro Báez (recientemente condenado por lavado de dinero), con contratos de obra pública vial. Fue allí cuando instaló su teoría del lawfare.