---
category: La Ciudad
date: 2021-12-08T06:00:56-03:00
thumbnail: https://assets.3dnoticias.com.ar/TRABAJADORESNIÑEZ.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Trabajadores de la Subsecretaría de la Niñez reclamaron por mejoras en las
  condiciones laborales
title: Trabajadores de la Subsecretaría de la Niñez reclamaron por mejoras en las
  condiciones laborales
entradilla: "El personal se manifestó por la falta de recursos en la que se encuentran
  trabajando.\n\n"

---
Este martes por la mañana trabajadores de la subsecretaría de la Niñez, Adolescencia y Familia llevaron adelante una jornada de protesta frente a la Secretaría de los Derechos de la Niñez, en la ciudad de Santa Fe. En este marco, Pablo Fonti, delegado de ATE, explicó a El Litoral que la medida corresponde a reclamos “por falta de recursos básicos para poder desarrollar nuestra tarea que es la protección de niños vulnerados. Hoy nos encontramos que a raíz de un desperfecto eléctrico, hay muchas computadoras que no podrán usarse por los profesionales, algo que está interviniendo directamente a nuestra labor”.

 Concretamente, el pedido ocurre ante “la escasez de insumos básicos, ya que durante la pandemia se han incorporado muchos compañeros a trabajar en calidad de picarizados, y eso también hizo que se visibilizarán aún más estos problemas”, indicó.

 Al respecto, Fonti sostuvo que “el Ministerio tiene que tomar cartas en el asunto para lograr una planificación de las tareas en condiciones dignas para los trabajadores y que se nos permita seguir trabajando para la comunidad”.

 Es por esto- y siguiendo con la linea de reclamos- los trabajadores piden que se avance sobre recursos económicos al Ministerio de Desarrollo Social, “que permitan resolver estos inconvenientes y que no sean parte de un cuestión meramente paliativa. Queremos que se busquen soluciones de fondo”, reiteró.

 En este sentido, los trabajadores destacaron en medio de estas cuestiones que “existió un compromiso en paritaria central de un contrato de un futuro pase a planta para compañeros que hoy están precarizados y que no se les actualiza el monto de la hora por su trabajo desde hace más de un año y medio”, explicó.

Respecto a este tema, “siempre se espero que mínimamente fuera acompañando a la paritaria del personal que está en planta. Esto venia siendo así pero la pandemia hizo que se abandonara ese criterio y hoy están siendo muy relegados”, concluyó.