---
category: La Ciudad
date: 2021-08-14T06:00:52-03:00
thumbnail: https://assets.3dnoticias.com.ar/secretriaelcetoral.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Los trabajadores de la Secretaría Electoral están en estado de asamblea permanente
title: Los trabajadores de la Secretaría Electoral están en estado de asamblea permanente
entradilla: Los empleados de esa dependencia estatal elevaron una serie de reclamos,
  entre los que se pide el pago de horas extras atrasadas.

---
Los trabajadores de la Secretaría Electoral de la provincia nucleados en la Asociación Trabajadores del Estado (ATE) de Santa Fe se declararon en estado de asamblea permanente luego de realizar una asamblea para solicitar una respuesta inmediata a sus reclamos.

Los empleados de esa dependencia estatal solicitan la asignación de funciones y el traslado del personal adscripto que cumple funciones en la Secretaría Electoral.

Además, reclamaron por la demora en la liquidación y pago de las horas extraordinarias correspondientes a los meses de mayo, junio y julio.

Los trabajadores pidieron el otorgamiento de los suplementos por subrogancias; efectivizar el traslado definitivo de los y las compañeras; gestionar en forma urgente el pago de las horas extraordinarias de mayo, junio y julio; exigir la actualización del suplemento de la Secretaría Electoral que se encuentra desfasado desde 2013; y autorizar la contratación en forma urgente mediante el dictado del Decreto del Poder Ejecutivo.