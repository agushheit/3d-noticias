---
category: Agenda Ciudadana
date: 2021-04-04T06:10:44-03:00
thumbnail: https://assets.3dnoticias.com.ar/cabanasjpg.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Semana Santa: el turismo santafesino tuvo una ocupación superior al 80%'
title: 'Semana Santa: el turismo santafesino tuvo una ocupación superior al 80%'
entradilla: Si bien hubo gran demanda, una gran cantidad de cabañeros alquiló dos
  días en vez de los cuatro del fin de semana largo.

---
El fin de semana largo que permitió la Semana Santa tuvo gratas repercusiones en del turismo. La ocupación en los complejos hoteleros y de cabañas varió entre el 80 y el 100 por ciento en promedio para toda la zona de la costa tanto de la ruta 1 como de la 11. Desde la Cámara de Cabañeros y Prestadores de Servicios Turísticos de la Provincia de Santa Fe (Cabasetur) señalaron a UNO Santa Fe que, si bien ven como positiva el alto porcentaje de demanda, en gran medida se alquilaron solo dos días de los cuatro de las vísperas de Pascuas.

Guillermo Kees Scotta, secretario de Cabasetur señaló a este medio que la mayoría de los turistas provienen de la provincia, que en general eligen trasladarse a localidades menores a los 200 kilómetros. "Luego tuvimos visitas de Córdoba en general, Entre Ríos y Buenos Aires pero en menor medida. En general de esta última provincia los turistas eligen zonas en el sur provincial, cerca de Rosario", relató. Y destacó Santa Rosa de Calchines: "tuvimos muchísima gente de Rafaela, Santa Fe, Santo Tomé, provincia de buenos aires, Capital Federal y Córdoba".

Con respecto a los protocolos, destacó: “Las familias que vienen salen de sus casas, y seguimos ofreciéndole la posibilidad de insertarse en una burbuja, una cabaña sanitizada, ventilada, rodeada de verde, y nadie le invade el parque privado”.

"Creo que también, por ahí, la gente se aplacó un poco cuando entró a sonar muy recurrentemente la segunda ola del Covid-19. A partir de ahí mucha gente se asusta y no sabe qué hacer. Ante eso es como que en el último tramo se puso muy tibia la demanda. Pero al mismo tiempo se trabajó muy bien con lo que se movió este fin de semana", agregó sobre el turismo de la provincia Kees Scotta.

En este sentido recordó que el sector turístico viene afectado desde 2020 con la pandemia, y que este fin de semana junto con el de carnaval representaron un repunte. "Esto significa una gran ayuda. Estuvimos parados diez meses. Eso significó que hubo que invertir muchísimo para poner todo en regla y para estar bien para recibir al turista de la mejor manera posible. Entonces por supuesto que nos ayudó muchísimo. Todo enero se trabajó muy bien, febrero también y ahora marzo qué fue increíble la demanda. Pero hace poco tiempo se estuvo entibiando, creemos que tiene que tiene mucho con dos motivos fundamentales: la situación económica y la posibilidad de una segunda ola. En ese sentido nosotros mantuvimos las tarifas de enero", dijo sobre el sector de turismo.

"Inclusive hemos hecho promociones de regalar un quinto día para esta oportunidad, pero no empujó tanto. La demanda se quedó tibia. Ante estas situaciones los colegas decidieron alquilar dos días y no los cuatro como otros años. Hubo que acomodarse a la gente, antes de tener una cabaña vacía. Quienes reservaron tempranamente alquilaron los cuatro días, y quienes estuvieron viendo a último momento en general tomaron dos días", sostuvo Kees Scotta.

Por último, sobre el anuncio de incorporar Billetera Santa Fe al turismo, el secretario de Cabasetur adelantó que esta semana tendrán una reunión con funcionarios provinciales donde recibirán información sobre el programa que luego deberán someter a análisis. "Creemos que no vamos a tener problemas. Estuvimos trabajando además con gente de Córdoba que tenía fondos del previaje y salió todo perfecto".