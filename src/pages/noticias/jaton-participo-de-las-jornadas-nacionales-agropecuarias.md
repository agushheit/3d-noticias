---
category: La Ciudad
date: 2021-08-02T06:00:08-03:00
thumbnail: https://assets.3dnoticias.com.ar/JORNADASAGRO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Jatón participó de las Jornadas Nacionales Agropecuarias
title: Jatón participó de las Jornadas Nacionales Agropecuarias
entradilla: 'La actividad se desarrolló este jueves y viernes de forma virtual con
  sede en Santa Fe capital, y estuvo destinada a profesionales de las ciencias económicas. '

---
La iniciativa fue organizada por el Consejo Profesional de Ciencias Económicas de la Provincia de Santa Fe Cámara Primera y por la Federación Argentina de Consejos de Profesionales de Ciencias Económicas.

El intendente Emilio Jatón participó este jueves de la apertura de las Jornadas Nacionales Agropecuarias para profesionales de Ciencias Económicas. El encuentro, que se realizó este jueves y viernes de forma virtual, se propuso como un encuentro interdisciplinario para el abordaje de diversidad de temas en materia agropecuaria que hacen a la realidad profesional, con importantes consecuencias en el ámbito de toda la comunidad.

Las jornadas contaron con la presencia de prestigiosos disertantes y fueron organizadas por el Consejo Profesional de Ciencias Económicas de la Provincia de Santa Fe-Cámara Primera, junto con la Federación Argentina de Consejos Profesionales de Ciencias Económicas.

En su discurso, Jatón destacó que “estas actividades son pilares fundamentales para el crecimiento y el desarrollo económico y social de todos los argentinos. Generan trabajo, producción y crecimiento económico a la vez que reafirman nuestras tradiciones e identidad. Y, en ese sentido, es muy valioso el rol que cumplen los profesionales en el acompañamiento a los productores, con su conocimiento, su escucha y su dedicación para brindar herramientas que ayuden al crecimiento del sector; y a su expansión en el país y en el mundo”.

En ese contexto, el intendente resaltó que “el conocimiento es clave”, repasó las acciones que se llevan a cabo desde el municipio en conjunto con las universidades e instituciones locales y expresó que “me gusta decir que en Santa Fe construimos y tomamos decisiones junto a los otros. Así lo hemos hecho, con cada actividad, en función de cada momento de la pandemia. Ese es el único camino posible: el de la colaboración, el diálogo y el trabajo conjunto entre el sector público y el sector privado; y entre los distintos niveles del Estado. Celebro que desde Santa Fe se organicen estas jornadas con amplio espíritu federal”.

En la apertura también participaron, además del intendente santafesino, el secretario de Agroalimentos de Santa Fe, Jorge Torelli; el presidente de la Federación Argentina de Consejos Profesionales de Ciencias Económicas, Silvio Marcelo Rizza; y el presidente del Consejo Profesional de Ciencias Económicas, Julio Yódice.