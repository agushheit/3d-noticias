---
layout: Noticia con imagen
author: Por Ignacio Etchart
resumen: ¡Adiós Diego!
category: Deportes
title: Murió
entradilla: Esta nota la comencé a escribir segundos después de la noticia.
  Murió el Diego. Y es la primera nota, en mi vida, que escribo en primera
  persona. Durante años estuve imaginando este momento. Creí que habría más
  lágrimas.
date: 2020-11-25T19:13:03.649Z
thumbnail: https://assets.3dnoticias.com.ar/maradona-800x445.png
---
Posiblemente el rápido deterioro de su cuerpo en el último tiempo mortalizó el cuerpo de Maradona, a diferencia de su espíritu, que siempre será inmortal.

Un niño que nació buscando comida podrida para él y su familia, en los tachos de basura de la Buenos Aires de la dictadura, se convirtió en 20 años en la persona más famosa del mundo. En palabras de Horacio Pagani: “nadie puede imaginarse qué significa ser Maradona, como alguien dijo alguna vez”.

Al Diego lo enfermaron. Como bien lo describió Bilardo, “yo defiendo a Maradona, lo he defendido toda la vida. Él no es el culpable. Es la gente que siempre lo rodea.” Una persona cuyo único deseo eran dos platos de comida al día, pasó a visitar de puertas abiertas y de forma gratuita, los mejores restaurantes del mundo.

La agilidad y la violencia de la fama y el éxito, sin un sostén afectivo y social que amortigüe semejante impacto, pueden ser mortales. Y esto no sólo en Maradona. Estrellas de todo ambiente artístico sucumbiendo ante los excesos, la exageración y la repugnante exuberancia, abundan.

Sin embargo, a Maradona lo odiaron, supuestamente, por el abuso a la cocaína. Cocaína que probó por primera vez a los 23 años, en el flamante y cajetilla Club Deportivo Barcelona de Cataluña. No en la “negrada” sureña y tosca de Nápoles, donde pudieron ver, por empatía e identificación entre empobrecidos y postergados, un redentor de las causas perdidas de su pueblo.

Su reprochada actitud frente a las mujeres, especialmente hacia sus hijas y su ex esposa Claudia Villafañe, a quien Bilardo destacó como “la mina –la señora, perdón– que más me ayudó, que colaboró conmigo, fue Claudia” en la misma entrevista citada previamente, es de pensamiento corriente hace muy pocos años y fue producto del manifiesto cambio de sentido social, enmarcado en la militancia feminista y disidente, no por maduración propia de una sociedad reaccionaria y mediocre como es la argentina. Ya se odiaba a Maradona desde mucho antes. Por drogadicto, supuestamente.

Pero, en realidad, lo odiaron por nunca olvidarse que fue pobre. Nada más odiable, luego de una mujer inteligente, que un rico con conciencia de clase, que un mega multimillonario con tatuajes del Che y fotos con Fidel, Chávez y Putin. Nada más traicionero que un burgués rodeado de comunistas.

Nada más odiable que un villero como fuente de inspiración de artistas de todo el globo, de todos los géneros, de todos los colores. Nada más odiable que un pobre que no agradece la mano que le da comer sino que, además, tiene el atrevimiento de denunciarla. Con sólo recordar que las causas por corrupción y soborno respecto al Mundial 2022 en Qatar salieron a la luz en la FIFA en mayo del 2015, el Diego lo había denunciado un año atrás desde su residencia en Dubái.

“Dubái”... ¿se entiende por qué el odio? De Villa Fiorito a Dubái. ¿Cómo se atreve un pobre a vivir en Dubái, entre jeques y petrodólares? ¿Con qué derecho? ¿Quién se cree que es? Diego Maradona es quien es.

En la retórica desapareció la primera persona. Supongo que el oficio encuadra, pero no adoctrina. Sí adoctrinan el amor, la convicción y el compromiso. El nunca olvidar, el siempre recordar, incluso en los peores momentos.

Y Maradona, último dios civil de la historia Argentina, capaz de movilizar hordas de desconocidos inconexos, pues sobran ejemplos de personas que odian el fútbol pero lo aman, murió hoy miércoles 25 de noviembre, hace pocos minutos. Tres días de Duelo Nacional decretó Alberto Fernández.

Por suerte hoy se juega al fútbol, se podrá enfocar el duelo en el verde sintético de una canchita. Y siempre, llevando la mano al corazón, la gran frase que el Diego nos dejó el día de su partido de despedida: “Yo me equivoqué y pagué, pero la pelota no se mancha”.\
Seguramente aún restan océanos de tinta digital por plasmar en el papel virtual de una pantalla de computadora. Pero ahora, la catarsis inicial está hecha. Y como a toda catarsis, siempre es bueno acompañarla con literatura. Por esta razón, Maradona, por Galeano:

![](https://assets.3dnoticias.com.ar/diego-maradona.jpg)

###### MARADONA

Ningún futbolista consagrado había denunciado sin pelos en la lengua a los amos del negocio del fútbol. Fue el deportista más famoso y más popular de todos los tiempos quien rompió lanzas en defensa de los jugadores que no eran famosos ni populares.\
Este ídolo generoso y solidario había sido capaz de cometer, en apenas cinco minutos, los dos goles más contradictorios de toda la historia del fútbol. Sus devotos lo veneraban por los dos: no sólo era digno de admiración el gol del artista, bordado por las diabluras de sus piernas, sino también, y quizá más, el gol del ladrón, que su mano robó.\
Diego Armando Maradona fue adorado no sólo por sus prodigiosos malabarismos sino también porque era un dios sucio, pecador, el más humano de los dioses. Cualquiera podía reconocer en él una síntesis ambulante de las debilidades humanas, o al menos masculinas: mujeriego, tragón, borrachín, tramposo, mentiroso, fanfarrón, irresponsable.\
Pero los dioses no se jubilan, por humanos que sean.\
Él nunca pudo regresar a la anónima multitud de donde venía. La fama, que lo había salvado de la miseria, lo hizo prisionero.\
Maradona fue condenado a creerse Maradona y obligado a ser la estrella de cada fiesta, el bebé de cada bautismo, el muerto de cada velorio.\
Más devastadora que la cocaína es la exitoína. Los análisis, de orina o de sangre, no delatan esta droga.

###### Galeano Eduardo 2008. Espejos. Una historia casi universal. Ed Siglo XXI Pp. 298/299.

PD: que año de mierda.