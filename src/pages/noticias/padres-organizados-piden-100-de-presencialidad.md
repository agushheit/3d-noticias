---
category: La Ciudad
date: 2021-02-03T07:59:43-03:00
thumbnail: https://assets.3dnoticias.com.ar/a2435c81-a53c-44bb-8572-e87e80f10449.jpg
layout: Noticia con imagen
link_del_video: https://youtu.be/G3mLm8jCrhA
author: 3D Noticias
resumen: Padres Organizados piden 100% de presencialidad
title: Padres Organizados piden 100% de presencialidad
entradilla: Desde la organización temen que no haya un plan integral que garantice
  que el 15 de marzo empiecen las clases en Santa Fe y se pueda sostener en el tiempo.

---
Este lunes por la mañana, Nicolás Trotta, ministro de Educación de la Nación estuvo reunido en Santa Fe con el Gobernador Omar Perotti. En ese marco, el grupo “Padres organizados de la Provincia de Santa Fe”, se acercó a Casa de Gobierno para pedir ser recibidos por las autoridades nacionales y provinciales.

Regina Estrada, miembro de la agrupación, manifestó que desde septiembre del año pasado le están solicitando, desde diferentes puntos de la provincia, a la Ministra de Educación Adriana Cantero, que los reciba y no han tenido respuesta.

Finalmente, este lunes, fueron recibidos por el Secretario de Administración y Técnica del Ministerio de Educación de la Provincia, Cristian Kuverling, a quien pudieron plantearle sus inquietudes.

El petitorio a las autoridades, manifiesta la necesidad de hacerle conocer al ministro el pedido de retomar las clases en el ciclo 2021 de modo completamente presencial y además solicitar interceda para poder ser recibidos por las autoridades provinciales como el Gobernador Perotti y la Ministra Cantero.

Exigen un plan concreto, una presencialidad continua y quieren que le asegure lo que se viene planteando desde el Ministerio.

Desde la agrupación son plenamente conscientes de la situación epidemiológica por la que está atravesando el país, pero recalcan también, que los niños, niñas y adolescentes no pueden volver a tener un año como el transcurrido, que afectó su salud integral además de la escasa incorporación de conocimientos.

[![Mira el video](https://assets.3dnoticias.com.ar/entrevista-padres-agrupados-santa-fe.webp)](https://youtu.be/G3mLm8jCrhA "Reproducir")

Mirá la nota en nuestro canal de [youtube](https://youtu.be/G3mLm8jCrhA)