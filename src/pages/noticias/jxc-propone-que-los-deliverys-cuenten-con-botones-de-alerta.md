---
category: Agenda Ciudadana
date: 2022-02-24T09:11:34-03:00
thumbnail: https://assets.3dnoticias.com.ar/courier-gb43a8a18c_1920.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: JxC propone que los deliverys cuenten con botones de alerta
title: JxC propone que los deliverys cuenten con botones de alerta
entradilla: "“Estamos convencidos que para el caso es una medida de fácil implementación
  y que ayudaría a prevenir muchas situaciones o, en algunos casos, apresar rápidamente
  a los responsables”."

---
![](https://assets.3dnoticias.com.ar/WhatsApp Image 2022-02-24 at 9.06.02 AM.webp)Ante el incremento de denuncias de situaciones de robo por parte de quienes se ocupan de entregas a domicilio en bicis o motos, los concejales Chuchi Molina, Inés Larriera y Carlos Pereira (Juntos por el Cambio) presentaron un proyecto para que los repartidores cuenten con botones de alerta conectados al Centro de Monitoreo municipal. “Estamos convencidos que para el caso es una medida de fácil implementación y que ayudaría a prevenir muchas situaciones o, en algunos casos, apresar rápidamente a los responsables”, remarcó Chuchi Molina luego de presentar la iniciativa a los trabajadores del sector.

La concejala Chuchi Molina (Juntos por el Cambio) se reunió este miércoles con representantes del servicio de delivery para compartirles el proyecto que presentó con el objetivo de que los repartidores cuenten con botones de alerta conectados al Centro de Monitoreo municipal, desde donde se derivará al 911 en caso de ser accionado el dispositivo. “El Municipio cuenta con la tecnología necesaria para conectar al Centro de Monitoreo a los cadetes que quieren contar con el botón de alerta, y tiene el personal necesario, las 24 horas del día para hacer un adecuado seguimiento de estas situaciones”, destacó la autora de la iniciativa durante el encuentro con los trabajadores, en una reunión en la que la acompañaron sus pares Inés Larriera y Carlos Pereira, firmantes también de la iniciativa.

Chuchi Molina contó después que “la propuesta nació de la misma charla que fuimos teniendo hace algunos días con los repartidores, con quienes ya nos habíamos reunido para contarles el proyecto que presentamos para que se implemente el sistema Cóndor -que permite detectar en el momento si un vehículo está denunciado como robado o tiene pedido de secuestro- en los operativos de tránsito. En ese encuentro surgió también la posibilidad de aprovechar toda la tecnología disponible como medida de prevención, y conversamos sobre la posibilidad de implementar los botones de alerta”.

“Estamos haciendo estas propuestas que creemos que pueden aportar soluciones. El difícil momento que transitamos en seguridad requiere más que nunca que nos pongamos en marcha, que activemos. Lo único que no se puede hacer es quedarse de brazos cruzados”, entapizó Chuchi Molina.

Prevención

La concejala -que era secretaria de Gobierno cuando el sistema se puso en marcha por primera vez en la ciudad, destinado inicialmente a mujeres víctimas de violencia de género-, apuntó que “los botones de alerta demostraron ser un sistema muy eficiente. Posteriormente se aplicó también al transporte público, y ha servido para detener en forma inmediata arriba del mismo coche a personas que estaban cometiendo un delito”, recordó. “Estamos convencidos que para el caso de los repartidores es una medida de fácil implementación y que ayudaría a prevenir muchas situaciones o, en algunos casos, apresar rápidamente a los responsables”, señaló.

En el proyecto presentado junto a Inés Larriera y Carlos Pereira, Chuchi Molina señala que las y los jóvenes que prestan servicios de delivery “vienen denunciando la gran cantidad de delitos que sufren en su tarea cotidiana, con robos de bicicletas, motos, celulares y dinero, que se contabilizan en decenas de episodios violentos por mes”. En estos días, incluso, han salido a manifestarse públicamente en reclamo de medidas de seguridad para el sector.

Para la autora del proyecto “está claro que es una actividad que, en medio de una ola de inseguridad creciente, se ve particularmente afectada por la soledad en que realizan sus tareas, principalmente en horarios nocturnos. Las esperas, que se extienden a veces por varios minutos, frente a domicilios para hacer las entregas, los vuelven fácil víctimas de arrebatos, atracos y otras situaciones violentas”.

Como medida preventiva, los deliverys podrían contar con los botones de alerta monitoreados desde el Municipio, que tiene el personal necesario, las 24 horas del día en el Centro de Monitoreo, para hacer un adecuado seguimiento de estas situaciones.

Chuchi Molina resaltó finalmente que “la implementación de estos botones no implicaría ninguna erogación extraordinaria para las arcas municipales, y podría imputarse a los 50.000.000 de pesos adicionales que incorporó el bloque de Juntos por el Cambio al presupuesto municipal para ser destinado a Seguridad”.