---
category: Agenda Ciudadana
date: 2021-02-15T07:28:54-03:00
thumbnail: https://assets.3dnoticias.com.ar/vacunación.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Se definió el cronograma de vacunación de los docentes
title: Se definió el cronograma de vacunación de los docentes
entradilla: 'Durante la última reunión del Consejo Federal de Educación, de la que
  participaron los 24 ministros del área, se fijaron las prioridades para la vacunación
  contra el Covid-19. '

---
El Consejo Federal de Educación, conformado por los 24 ministros del país, estableció el orden de vacunación contra el coronavirus para los docentes, para lo cual se los dividió en cinco grupos.

En el primer grupo estará el personal de dirección y gestión, el de supervisión e inspección, los docentes frente a alumnos y alumnas de nivel inicial (incluye ciclo maternal), los de primer ciclo del nivel primario (1°, 2° y 3° grado) y los de educación especial.

En el segundo grupo figura el personal de apoyo a la enseñanza, todo otro personal sin designación docente pero que trabaja en establecimientos educativos de la educación obligatoria en distintas áreas y servicios (maestranza, administración, servicios técnicos, servicios generales, y equivalentes).

En el tercero estarán los docentes frente a alumnos y alumnas de nivel primario del segundo ciclo (de 4° a 6°/7° grado).

El cuarto grupo estará conformado por los docentes frente a alumnos y alumnas de nivel secundario, de educación permanente para jóvenes y adultos en todos sus niveles e instructores de formación profesional.

Finalmente, en el último grupo estarán los docentes y no docentes de institutos de educación superior y universidades.