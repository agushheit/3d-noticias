---
category: Agenda Ciudadana
date: 2021-03-28T06:15:50-03:00
thumbnail: https://assets.3dnoticias.com.ar/GANANCIAS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: INFOBAE
resumen: Cuáles serán los beneficios y los beneficiarios cuando finalmente se sancione
  la ley que introduce cambios en el pago de Ganancias
title: Cuáles serán los beneficios y los beneficiarios cuando finalmente se sancione
  la ley que introduce cambios en el pago de Ganancias
entradilla: 'Unas 1.267.000 personas dejarán de pagar o pagarán menos. La norma será
  retroactiva al 1 de enero, por lo que en abril habrá reintegros por impuestos pagados
  en el primer trimestre. '

---
Según los cálculos oficialistas, unas 1.267.000 personas dejarán de pagar o pagarán menos Ganancias. La norma será retroactiva al 1 de enero, por lo que en abril habrá reintegros por impuestos pagados en el primer trimestre

Aunque puede haber novedades en la votación en particular del proyecto de ley que eleva el piso del impuesto a las Ganancias, los contornos fundamentales quedaron establecidos en el dictamen que se aprobó el jueves 18 de marzo.

Básicamente, el proyecto que presentó el titular de la Cámara de Diputados, Sergio Massa, eleva a $ 150.000 brutos ($ 124.000 netos) el monto por debajo del cual los trabajadores en relación de dependencia no pagarán el tributo y, en el caso de los jubilados, a 8 haberes mínimos ($164.570) el tope de exención sobre los ingresos jubilatorios. Del pago del tributo también quedará exento el aguinaldo.

En el caso de los asalariados, el nuevo umbral representa un aumento de 66% respecto al monto a partir del cual un asalariado soltero y sin hijos pagaba Ganancias y en 25% en el caso de los casados con dos hijos menores, que empezaban a pagar el impuesto unos pesos antes de llegar a ganar $ 99.000.

Según los cálculos de Massa y la AFIP, esto significa que, entre asalariados y jubilados, unos 1.267.000 actuales contribuyentes serán beneficiados sea porque pagarán menos o porque dejarán de pagar, al menos por un tiempo, el tributo.

**Cuántos pagaban, cuántos pagarán**

Cuando se aprobó el dictamen, el presidente de la Comisión de Presupuesto y Hacienda de la Cámara Baja y miembro de la coalición oficialista, Carlos Heller, precisó que “hay muchas de cosas para trabajar en modificaciones de fondo, pero lo que estamos haciendo es volver al nivel histórico de trabajadores alcanzados, que estaba en un 10% o 12% y que llegó al 25% en la actualidad. Con esta ley, alcanzaría solo al 7% de los trabajadores registrados y al 2,4% de los jubilados”.

Heller, además, ratificó que el costo fiscal total de la nueva ley estará en torno de los $ $47.600 millones, número que la AFIP habrá de ajustar en función de cómo quede finalmente el articulado del proyecto, que cuenta con el apoyo de la oposición, que sin embargo insistirá en algunos cambios, como el del radical Luis Pastori, que propone actualizar las deducciones por intereses de créditos hipotecarios, incorporar la deducción de gastos educativos y actualizar las escalas mediante el índice de inflación (IPC) y no el de variación salarial (RIPTE). En tanto, el también opositor Ricardo Buryaile insistirá en retirar el tope de 18 años para las deducciones por hijos con discapacidad y que no exista una edad tope o que esta sea aumentada hasta 21 años, ya que según el Código Civil y Comercial fija la obligatoriedad de asistencia alimentaria hasta dicha edad y no hasta los 18.

Los asalariados formales que ganan entre $150.000 y $173.000 mensuales tendrán a su vez un aumento de sus deducciones (tarea que estará a cargo de la AFIP), de modo de evitar situaciones anómalas, como que un trabajador prefiera no tener aumento de sueldo, porque sin esa corrección un mayor salario bruto podría significar de hecho una rebaja del salario neto de impuestos, una falla de diseño del proyecto original.

En cambio, quienes tengan ahora o en el futuro más de $ 173.000 de ingreso bruto mensual serán alcanzados por el tributo.

Según números del oficialismo, los cambios beneficiarán a 3 de cada 4 empleados de Comercio y a dos de cada tres de los trabajadores de transporte, logística y almacenamiento, que dejarán de pagar o pagarán menos que antes. En los sectores fabriles, financieros y de la construcción la proporción de beneficiados será menor, en torno del 50 por ciento, según el sector y los niveles salariales de que se trate.

Otro beneficio, acordado con los gremios y la oposición, es que se contemplarán fuera del impuesto ciertos beneficios sociales, excluyéndose por caso los ingresos que reciba un empleado en concepto de provisión de ropa de trabajo, el pago de cursos de capacitación, el reintegro de gastos de guardería o jardín materno-infantil, para contribuyentes con hijos de hasta 3 años de edad.

“Este es un beneficio muy importante que se incorpora al proyecto con un tope de hasta el límite equivalente al 40% de la Ganancia no imponible ($ 67.000 al año, es decir $ 5.590 por mes)”, explicó sobre el particular la diputada oficialista Vanesa Siley.

**Deducciones**

Además, se duplicará la deducción por hijo con discapacidad, actualmente de $78.833 al año, y se prorrogará la exención en el impuesto a las horas extras del personal de salud por la pandemia de Covid-19.

El Instituto Argentino de Análisis Fiscal (Iaraf) calculó que la ley ahorrará a los trabajadores formales entre $259 y $9.641 de impuestos por mes, según el nivel salarial que detenten y su situación familiar.

El piso de $ 150.000 brutos será actualizado anualmente según la evolución del RIPTE (Remuneración Imponible Promedio de los Trabajadores Estables), aunque la oposición había propuesto e insistirá en que el ajuste del mínimo sea por el Índice de Precios al Consumidor, esto es, la inflación minorista que calcula el Indec, para evitar que las próximas paritarias anulen buena parte de los beneficios del proyecto.

La nueva ley se aplicará con retroactividad al 1 de enero, con lo que los empleados en relación de dependencia beneficiados recibirían con los haberes de abril el reintegro de los cargos por Ganancias que les fueron deducidos en el primer trimestre del año.

El proyecto tiene el apoyo de todo el espectro sindical y el hecho de que en la jornada en que se aprobó el dictamen parte de la capital argentina amaneciera empapelada con afiches con el texto “Ahora Ganancias” y “Que sea Ley” y la firma de Massa, dejan en claro que la ley será una de las banderas del oficialismo en la campaña electoral para las elecciones legislativas de octubre.