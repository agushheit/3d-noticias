---
category: Estado Real
date: 2021-10-27T06:00:54-03:00
thumbnail: https://assets.3dnoticias.com.ar/quesito.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Perotti con Dominguez en el Sur provincial: "La investigación y el conocimiento
  generan trabajo y arraigo"'
title: 'Perotti con Dominguez en el Sur provincial: "La investigación y el conocimiento
  generan trabajo y arraigo"'
entradilla: " Perotti destacó el “sistema científico-tecnológico sustentable, organizado
  y consolidado” que tienen Santa Fe y el país."

---
El gobernador Omar Perotti y el ministro Agricultura, Ganadería y Pesca de la Nación, Julián Domínguez, realizaron este martes una recorrida por el sur de la provincia, que incluyó visitas a empresas semilleras que realizan inversiones e investigaciones en sus establecimientos. La recorrida incluyó el Centro Regional Agrotécnico de Venado Tuerto.

“Si algo distingue a la Argentina en el mundo, es la consideración de ser un país con 5 premios Nobel, y un sistema científico-tecnológico sustentable, organizado y consolidado; ese activo no lo tienen todos los países”, aseguró Perotti, y pronosticó: “Sin dudas, Argentina juega un rol muy fuerte, porque hay actores e investigadores con capacidad para hacerlo”.

En ese sentido, aseguró que “la tecnología le genera la posibilidad de un mayor valor agregado a los sectores tradicionales, pero también genera nuevos emprendimientos, los cuales tenemos que apuntalar; allí es donde tenemos una oportunidad enorme de desarrollo”.

“La Argentina, y particularmente Santa Fe, tienen mucho talento para sumarse y nutrir cada una de estas experiencias que se están desarrollando”, dijo Perotti, y añadió: “Lo que se genera a través de la investigación y de cada uno de los semilleros es muchísimo trabajo en la zona, arraigo, y proyección internacional de la región”.

Finalmente, continuando esa línea discursiva, el gobernador afirmó que “estamos plenamente convencidos que la Argentina necesita integrarse territorialmente y equilibrarse poblacionalmente, y como provincia trabajamos en esa dirección. Tratar de equiparar infraestructura donde falta, y generar inversiones donde nuestra provincia ha quedado con una extensa superficie vacía”.

**“SIGLO DEL CONOCIMIENTO”**  
Más adelante, Domínguez remarcó que “antes, el capital era la fábrica, el campo, y ahora es el conocimiento. No tenemos forma de hacer frente al cambio climático, ya sea sequía o inundación, sin la biotecnología. Por eso, desde el primer momento, creo en la biotecnología, en los emprendedores, y en el esfuerzo que se hace”.

“Tenemos que jugar este partido en equipo y no perder esta oportunidad que nos está dando el mundo”, propuso el ministro nacional, y añadió que “al futuro nos tenemos que anticipar; el siglo XXI es el siglo del conocimiento”.

A su turno, el ministro de la Producción, Ciencia y Tecnología de Santa Fe, Daniel Costamagna, destacó la importancia de “la tecnología aplicada que da estas posibilidades, les da competitividad a los cultivos, para ser uno de los grandes actores en la producción nacional y mundial. El gran desafío que tenemos por delante es producir con sustentabilidad”.

**MURPHY Y VENADO TUERTO**  
La recorrida comenzó con una visita a la planta de la firma Advanta, en Murphy, que se destaca por la producción de sorgo destinado a China para la elaboración de bebidas destiladas. Luego, continuó en Venado Tuerto, en la empresa Nuseed, donde se realiza el programa de mejoramiento de Carinata, que se comercializa para la producción de biocombustible de la industria aeronáutica francesa.

Finalmente, las autoridades recorrieron en el Centro Regional Agrotécnico, donde se construirá un nuevo laboratorio. “Educar con el trabajo es una de las circunstancias que uno más valora. Crecer aprendiendo en este marco, y desarrollando cada una de las tareas, es una formación diferente para la profesión que cada uno de ustedes haya elegido”, dijo el gobernador a los alumnos.

De las diferentes actividades participaron, también, los senadores nacionales, María de los Ángeles Sácnun y Roberto Mirabella; el secretario de Agricultura, Ganadería y Pesca de la Nación, Jorge Alberto Solmi; y los secretarios provinciales de Integración y Fortalecimiento Institucional, José Luis Freyre, y de Agroalimentos, Jorge Torelli, entre otros.