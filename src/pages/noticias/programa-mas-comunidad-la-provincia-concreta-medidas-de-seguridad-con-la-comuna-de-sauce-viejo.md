---
category: Agenda Ciudadana
date: 2021-10-13T06:00:54-03:00
thumbnail: https://assets.3dnoticias.com.ar/mascomunidad.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Programa Mas Comunidad: la provincia concreta medidas de seguridad con la
  comuna de Sauce Viejo'
title: 'Programa Mas Comunidad: la provincia concreta medidas de seguridad con la
  comuna de Sauce Viejo'
entradilla: El ministro Jorge Lagna se reunión con el presidente comunal, Pedro Uliambre,
  con el objetivo de coordinar obras y proyectos.

---
El ministro de Seguridad de la provincia, Jorge Lagna, visitó este marte la comuna de Sauce Viejo con el objetivo de delinear acciones que se llevarán a cabo en el marco del Programa Más Comunidad entre la comuna y el Ministerio. Allí, mantuvo un encuentro con el presidente comunal, Pedro Uliambre.

En este sentido, Lagna sostuvo: “En virtud de ese convenio marco estamos desarrollando una serie de medidas concretas, primero en cuanto al buen sistema de videovigilancia que ha creado el presidente comunal Pedro Uliambre en la comuna. Nosotros tenemos un programa que firmó hace poco el gobernador Omar Perotti, que es el Más Comunidad, que tiene que ver con la parte social de la seguridad y también con los Centros de Monitoreo, y el presidente comunal lo va a utilizar para ampliar el sistema de videovigilancia, con una inversión de 4 millones de pesos”.

“Además estuvimos recorriendo la Subcomisaría sub13, que vamos a dejar en condiciones también con un aporte del Ministerio. También recorrimos el comando de Sauce Viejo, y gracias al programa que tenemos, de modernización de edificios públicos afectados a la seguridad, donde la ejecución es por parte de la comuna y el aporte es de la provincia, vamos a poner en valor el Comando y el Centro de Salud, para que la ciudadanía lo tenga en condiciones y los trabajadores que se desempeñan allí”, indicó el Ministro de Seguridad, y agregó: “Y junto con todo esto, la semana que viene comienza el equipo de 911 de la ciudad de Santa Fe a capacitar a los operadores del centro de monitoreo de Sauce Viejo, porque no se trata de poner una persona a ver una cámara, sino que hay que entrenarlo para que vea lo que otros no ven”.

Por su parte, Uliambre valoró la presencia del ministro en la localidad y dijo: “Estas acciones que planteamos en conjunto con el ministro, como venimos haciendo siempre, son muy buenas, son soluciones para los vecinos. Hemos escuchado a los vecinos y queremos estar a su lado, porque nos preocupa, por eso trabajamos en lo que nos plantea cada uno de los vecinos de Sauce Viejo”.