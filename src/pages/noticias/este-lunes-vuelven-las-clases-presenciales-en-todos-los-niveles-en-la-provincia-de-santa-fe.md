---
category: Agenda Ciudadana
date: 2021-07-26T06:15:37-03:00
thumbnail: https://assets.3dnoticias.com.ar/CLASES.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Este lunes vuelven las clases presenciales en todos los niveles en la provincia
  de Santa Fe
title: Este lunes vuelven las clases presenciales en todos los niveles en la provincia
  de Santa Fe
entradilla: Será con jornada completa, pero se mantendrá por ahora la alternancia
  en burbujas. Se agregarán más horas y habrá refuerzos pedagógicos.

---
Ante la mejora en los indicadores sanitarios, en la provincia de Santa Fe regresan este lunes las clases presenciales en todos los niveles.

Por el momento continuará la modalidad de burbujas y se agregarán más horas de clases presenciales, recuperando la jornada simple completa, por lo que también se reduce la virtualidad. Además, se suman instancias de intensificación de la enseñanza, como tutorías optativas los sábados para estudiantes de secundaria con dificultades.

“Si bien la pandemia no terminó y seguimos con un nivel de riesgo alto, en la mayoría de los departamentos de la provincia estamos saliendo de la alarma epidemiológica, lo que nos permite planificar la vuelta a la presencialidad en todos los niveles y modalidades, con el cumplimiento estricto de los protocolos y los cuidados, como el distanciamiento social, el uso de barbijos y la ventilación permanente de las aulas”, sostuvo en la semana la ministra de Educación, Adriana Cantero.

**Horarios**

Los nuevos horarios indican que desde agosto la primaria tendrá tres bloques horarios de 70 minutos y dos espacios de recreación, para ventilar aulas, de 15 minutos cada uno. Mientras que en la secundaria se impartirán tres bloques horarios de 70 minutos y uno de 35 minutos, alternados con recreos para descanso y ventilación. En la primera parte del día, se considerarán ejes que se irán trabajando en la jornada y que, luego, se cerrarán en el último tramo con actividades desde la percepción de los mismos alumnos sobre lo aprendido.

Esto significa que, a partir de este lunes, la jornada escolar ya no será en la primaria de tres horas y media (como se dispuso inicialmente en el marco de la recuperación de la presencialidad en contexto de pandemia), sino que la jornada recuperará la duración original de 4 horas con 15 minutos. Esta ampliación del tiempo de permanencia en las aulas se dará en todos los niveles y, en el caso del secundario, equivaldrá a dos horas cátedra más.

La circular emitida indica, también, que las escuelas que cuenten con personal para jornada extendida o jornada completa, podrán sumar una hora cátedra más cada día. Asimismo, aclara que en caso que los establecimientos no cuenten con los espacios para mantener el distanciamiento, continuará la modalidad de burbujas con asistencia alternada. Y que si la matrícula es reducida y la infraestructura lo permita, los estudiantes podrán asistir a clase todos los días, sin bimodalidad.

**TERCIARIOS**

Por su parte, los institutos terciarios comenzarán sus clases en agosto, con una instancia de presencialidad con alternancia y cuidados, y en consonancia con lo que están pensando las universidades.

**COMERDORES**

Desde el Ministerio de Educación se aclaró que, por el momento, los comedores no estarán habilitados, por lo que continuará la entrega de alimentos por familia, tal como se viene haciendo con más de 144 mil kits en toda la provincia.