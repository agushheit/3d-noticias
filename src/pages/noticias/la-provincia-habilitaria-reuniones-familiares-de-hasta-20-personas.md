---
category: Agenda Ciudadana
date: 2021-08-18T06:15:59-03:00
thumbnail: https://assets.3dnoticias.com.ar/20PERSONAS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: La provincia habilitaría reuniones familiares de hasta 20 personas
title: La provincia habilitaría reuniones familiares de hasta 20 personas
entradilla: El gobernador destacó que la disminución de casos y de uso de camas en
  hospitales también permitirá “recuperar presencialidad” en las escuelas.

---
El gobernador de Santa Fe, Omar Perotti, anunció hoy que habilitará reuniones familiares de hasta 20 personas "para cargar energías con esa gente que queremos tanto", y destacó que para fines de agosto aspira a llegar al millón de vacunados con dos dosis contra el coronavirus.

"Todos hacemos un enorme esfuerzo y lo que más nos cuesta a los argentinos y a los santafesinos son las reuniones familiares y afectivas por eso vamos a ampliar el número para llevarlo a 20 personas", afirmó. "Esto no significa que dejemos de cuidarnos, no significa que ya pasó la pandemia, sería el peor error, sería volver atrás", enfatizó.

**Presencialidad en las escuelas**

También se refirió a la presencialidad escolar y señaló que "están disminuyendo la cantidad de casos y disminuyendo el uso de camas; eso nos permite recuperar la presencialidad en las escuelas con más horas".

"Ojalá podamos ir incorporando la mayor cantidad de establecimientos que puedan, por su infraestructura y la realidad sanitaria, ir teniendo la mayor cantidad de días de clases; hacia eso vamos", apuntó.

"La salida de la pandemia nos tiene que encontrar fortalecidos en esa instancia. Estamos recuperando en buen nivel la actividad económica; la provincia de Santa Fe está marchando muy bien en lo que hace a todos los indicadores de recuperación productiva y de recuperación de empleo en blanco", finalizó.

**Vacunación**

Al referirse al operativo de vacunación provincial, el gobernador santafesino consideró que "agosto nos tiene que dejar como mínimo un millón de personas vacunadas con las dos dosis".

"Si seguimos con este ritmo y esfuerzo, vamos a superar ese piso y es una muy buena noticia porque nos prepara de la mejor manera para la presencia de otra cepa", afirmó.