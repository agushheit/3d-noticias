---
category: Agenda Ciudadana
date: 2021-06-05T06:00:40-03:00
thumbnail: https://assets.3dnoticias.com.ar/EDUCACION.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Nación habilitó adelantar las vacaciones de julio en las escuelas, pero Santa
  Fe aún no definió
title: Nación habilitó adelantar las vacaciones de julio en las escuelas, pero Santa
  Fe aún no definió
entradilla: En la reunión del Consejo Federal de Educación el ministro Nicolás Trotta
  habilitó a las provincias a modificar los calendarios escolares. Santa Fe analizará
  en los próximos días qué paso dar.

---
La ministra de Educación de Santa Fe, Adriana Cantero, participó por videoconferencia de la 106º Asamblea del Consejo Federal de Educación, junto al titular de la cartera educativa nacional, Nicolás Trotta, y representantes de todas las jurisdicciones provinciales. El propio Trotta habilitó la posibilidad de que se modifique el calendario escolar “teniendo en cuenta los indicadores sanitarios de cada provincia”.

Respecto a las discusiones del Consejo Federal, la ministra Cantero precisó que “hemos compartido inquietudes acerca del difícil momento epidemiológico que atravesamos y su incidencia en el sistema educativo y en tal sentido reafirmamos los acuerdos federales de procedimientos frente a las clasificaciones de riesgo sanitario establecidas por las autoridades de salud, que orientan los formatos escolares para cada situación”.

En relación a la modificación de los calendarios escolares previstos, la titular de Educación manifestó que “los calendarios pueden ser objeto de análisis, pero las decisiones al respecto deberán tomarse con las proyecciones que se aporten desde el sector de salud para cada caso, de modo de aportar mejoras y no acrecentar dificultades en los cursados”.

En ese marco, Cantero precisó que “en los próximos días se abrirán espacios para debatir la organización del segundo semestre en base a la realidad que han marcado las decisiones del primer semestre, buscando alternativas y articulaciones para cerrar el ciclo lectivo 2021 sin resignar objetivos educativos”.

Durante el encuentro se presentó el Centro de Políticas Educativas Federales, que tendrá como objetivo la formación de recursos humanos en el amplio campo de las políticas públicas educativas con miradas federales, la investigación sobre las experiencias diversas en el territorio nacional y en el regional, la producción de insumos sobre esos desarrollos.

Asimismo, se presentaron los alcances de la Ley de Educación Ambiental y se aprobó el plan del INET de Capacitación Laboral de Alcance Nacional (CLAN).

Finalmente, la funcionaria provincial se refirió a la presentación de la Ley de Educación Ambiental que fue realizada por el Ministro de Ambiente y Desarrollo Sustentable de la Nación, Juan Cabandié, indicando que “desde el Ministerio de Educación provincial se ha avanzado en planes sobre esa esta temática y se está trabajando en la articulación con el Ministerio provincial de Ambiente y Cambio Climático para la elaboración de los contenidos de educación ambiental para el sistema educativo provincial”.