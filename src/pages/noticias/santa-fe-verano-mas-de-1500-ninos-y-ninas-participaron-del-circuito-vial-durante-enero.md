---
category: Estado Real
date: 2021-02-01T06:52:32-03:00
thumbnail: https://assets.3dnoticias.com.ar/apsv.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: 'Santa Fe Verano: Más de 1500 niños y niñas participaron del circuito vial
  durante enero'
title: 'Santa Fe Verano: Más de 1500 niños y niñas participaron del circuito vial
  durante enero'
entradilla: 'Las acciones de prevención que comenzaron en diciembre recorren localidades
  de la zona costera del territorio, en conjunto con las áreas de Salud y Turismo,
  para promover pautas de cuidado durante el verano.

'

---
En el marco del Operativo Santa Fe Verano, la Agencia Provincial de Seguridad Vial (APSV), dependiente del Ministerio de Seguridad, desarrolla acciones, coordinadas entre distintas áreas del gobierno provincial, que tienen por finalidad transmitir hábitos de movilidad saludables y seguros durante el verano.

Durante el mes de enero, más de 1500 niños y niñas disfrutaron del dispositivo de la APSV, integrado por dos circuitos viales inflables, cuyo fin es promover principios de movilidad segura, sostenible e inclusiva en los roles de pasajero, peatón y ciclista. Los promotores viales brindan charlas previas antes de ingresar a la mini ciudad y charlan también con los adultos para concientizar sobre los principales factores de riesgo que se deben atender para tener un traslado seguro. Además, se entregan licencias simbólicas de peatón y ciclista, como una forma de aproximarse a la responsabilidad que implica el uso de la vía pública en este caso, en forma de juego.

El espacio, que está destinado a niños de entre 6 y 10 años, emula una pequeña ciudad, con edificios, calles, señalización vertical y horizontal. Los promotores viales de la APSV acompañan y guían el proceso de aprendizaje de los niños en su recorrido con kartins, bicicletas o caminando, que les permiten adoptar herramientas de prevención en su manejo de la vía pública. Los niños están invitados a venir con sus propias bicicletas o monopatines y recorrer el circuito, aprendiendo la importancia no sólo de las señales que ordenan el tránsito sino también los valores que están en juego en la construcción de una convivencia saludable y segura para todos.

Al respecto, la directora de la Agencia Provincial de Seguridad Vial, Antonela Cerutti, sostuvo que “para el gobernador Omar Perotti la seguridad vial es una prioridad, también para el ministro de Seguridad, Marcelo Saín. Desde la APSV implementamos acciones a fines de 2020, junto al Operativo Santa Fe Verano, que recorren distintos puntos de la provincia y buscan promover una nueva cultura vial”.

“En el espacio educativo acompañamos a los niños y niñas en el aprendizaje de las normas que rigen nuestra convivencia en la vía pública. Para esta gestión es de suma importancia construir buenos hábitos desde la niñez para formar ciudadanos responsables”, agregó la directora.

Las acciones de prevención se llevan a cabo en distintas ciudades del territorio, con un abordaje multiagencial y articulado entre distintas áreas del gobierno para cuidar la salud de los santafesinos en forma integral. El área de Salud entrega barbijos y alcohol en gel, a la vez que informa y promueve la importancia del distanciamiento social y la higiene de manos, como la mejor forma de cuidarnos ante el Covid-19. Asimismo, Turismo promueve formas seguras de viajar y disfrutar en familia, conociendo nuestra provincia, valorando los ambientes naturales, todo con pautas de estricto cuidado frente al virus.

**Campaña de prevención de riesgos viales**

Por otro lado, la APSV reforzó la difusión de las principales recomendaciones a tener en cuenta para tener un viaje seguro, con especial atención al uso del cinturón de seguridad, el no consumo de alcohol y el respeto a las velocidades máximas. El objetivo es brindar información sobre distintas situaciones que pueden presentarse durante el viaje, y cómo actuar ante cada una de ellas para evitar riesgos y aumentar la seguridad vial.

Entre las recomendaciones, la APSV destacó la importancia de revisar el estado mecánico general del vehículo y comprobar los frenos; de programar el viaje teniendo en cuenta las personas que viajan, si hay niños o personas mayores, a fin de establecer paradas y tiempos de descanso; y de no ingerir alcohol.

También continúa la campaña sobre movilidad infantil segura, en conjunto con la Asociación para la Disminución de Siniestros Viales (Adisiv), a través de la cual se transmiten mensajes que buscan brindar a los adultos información adecuada sobre formas seguras de traslado de los niños en el vehículo, así como pautas de cuidado del niño peatón y ciclista.