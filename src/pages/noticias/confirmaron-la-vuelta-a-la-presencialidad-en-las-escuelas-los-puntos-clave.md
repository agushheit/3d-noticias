---
category: Agenda Ciudadana
date: 2021-08-28T06:15:19-03:00
thumbnail: https://assets.3dnoticias.com.ar/PRESENCIALIDAD.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Confirmaron la vuelta a la presencialidad en las escuelas: los puntos clave'
title: 'Confirmaron la vuelta a la presencialidad en las escuelas: los puntos clave'
entradilla: La ministra de Educación provincial Adriana Cantero anunció formalmente
  la vuelta de presencialidad plena a las escuelas en todos los niveles desde el lunes
  30.

---
A partir del próximo lunes 30 de agosto los alumnos de todos los niveles podrán volver a a presencialidad plena en las escuelas en toda la provincia. Así lo anunció la ministra de Educación Adriana Cantero en una conferencia de prensa donde dio detalles de la Circular Nº 25 a través de la cual se hace efectivo el anuncio con todos los detalles a toda la comunidad educativa.

"A partir del lunes las escuelas podrán diseñar sus espacios para que la mayor cantidad de alumnos de la trayectoria obligatoria, acceda a la presencialidad plena", comenzó diciendo Cantero. "Puede ser que exista, sobre todo en las grandes ciudades, un número pequeño de escuelas que dada la gran cantidad de matrícula con la que cuentan y las dimensiones de sus espacios escolares requiere una intervención del ministerio para tener alternativas de excepcionalidad".

En cuanto cómo deben llevar adelante los establecimientos educativos el protocolo necesario para poder dictar clases aclaró: "Podrán hacerlo ocupando todos sus espacios con creatividad, flexibilidad y serenidad. Podrán utilizar galerías, patios, bibliotecas, salones de usos múltiples, gimnasios, comedores para que la mayor parte de chicos puedan estar en la escuela con presencialidad diaria".

Por otro lado, y respecto de sumar días incluyendo actividades para que alumnos ganen días de clases manifestó: "Los sábados activos para terceros y cuartos años de la escuela secundaria están vigentes y empezaremos la experiencia desde el mes de septiembre. Tiene que ver con el fortalecimiento del aprendizaje de los adolescentes. Aspiramos a que en todos los establecimientos caminemos a la máxima flexibilidad posible, es por ello que se actualizaron protocolos".

**Los gremios en esta coyuntura**

Por otro lado, la ministra provincial se refirió a los gremios docentes y sus opiniones respecto a la vuelta a la presencialidad: "Los sindicatos docentes manifestaron su reparo con respecto a la posibilidad de que los chicos vuelvan todos juntos a la escuela en función de la previsión de lo que podría ser la llegada de la variante Delta". Pero destacó: "Ayer estuvieron presentes en la reunión del Consejo Federal como también en el Comité Asesor para la Vuelta a la Presencialidad, y escucharon a los expertos y ministros. Todos sabemos que vivimos en un tiempo inédito y de cuidado permanente. La pandemia no pasó y tenemos que tener previsiones".

Y la funcionaria fue contundente cuando resaltó: "Vamos aprendiendo a movernos en cada momento con lo que nos ofrece la circunstancia en cuanto a mejoras. Vamos a aprovechar este momento para tener más días de clases. La flexibilidad de abrir y cerrar los dispositivos conforme a los lineamientos sanitarios, sigue vigente. Estamos atravesando un tiempo que nos permite una mejora para volver a la presencialidad diaria, pero vamos a seguir monitoreando como siempre a los chicos. Con la detección de casos sospechosos con lo que nos indica la línea 0800 escuela, y aceptando las intervenciones del Ministerio de Salud, además de ver cómo avanza cada localidad, ya que algunas al día de hoy se encuentran en un riesgo medio o bajo, y es momento para pensar algo general".

Adriana Cantero concluyó: "Los padres serán convocados por las escuelas para que les comuniquen las medidas para el cursado la próxima semana".

**El protocolo**

Desde el área de Educación, recuerdan que el regreso a la presencialidad depende de tres condiciones: los departamentos se deben encontrar en contexto de bajo y medio riesgo epidemiológico y baja proporción de uso de camas de terapia intensiva; las escuelas deben estar en condiciones óptimas de infraestructura y servicios para su apertura a la comunidad educativa; y el chequeo permanente por parte de cada jurisdicción de las condiciones sanitarias, de manera de establecer las medidas a continuar o modificar.

**Para la apertura de las escuelas es necesario respetar:**

• Distanciamiento físico (actualmente establecido en 1.5 m entre los/as alumnos/as, que se extiende a 2 m en los espacios comunes de la escuela). El distanciamiento hacia y entre docentes también se establece en 2 m.

• Ventilación: debe ser permanente, manteniendo puertas y/o ventanas abiertas en todo momento, aún en épocas de baja temperatura. Asimismo, debe ser cruzada para permitir corrientes de aire que limiten el eventual contagio por aerosoles. Aquellos espacios que no cuenten con ventilación adecuada no deberán ser utilizados para las clases

• Uso permanente de barbijo/mascarilla, a partir de los 6 años de edad

• Higiene frecuente de manos, de manera de disminuir la posibilidad de contagio por contacto que, aunque menos frecuente, debe ser minimizada

• Aislamiento de casos sospechoso y de contactos estrechos