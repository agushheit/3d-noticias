---
category: Estado Real
date: 2021-03-13T06:00:40-03:00
thumbnail: https://assets.3dnoticias.com.ar/justicia.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Perotti: “Es Imprescindible mejorar la prestación de Justicia”'
title: 'Perotti: “Es Imprescindible mejorar la prestación de Justicia”'
entradilla: El gobernador participó de la apertura del Año Judicial 2021. Durante
  la ceremonia, los tres poderes del Estado firmaron el acta acuerdo del programa
  Santa Fe + Justicia.

---
El gobernador Omar Perotti, junto al presidente de la Corte Suprema de Justicia de la provincia, Roberto Falistocco, encabezaron este martes la apertura del Año Judicial y la firma del acta del programa Santa Fe + Justicia. Dicho programa tiene como premisas la inclusión e integración ciudadana junto con el mayor acceso a la justicia e información por parte de todos los santafesinos y santafesinas.

La ceremonia se desarrolló en el salón de Actos del Palacio de Tribunales, en la ciudad capital de la provincia, y contó además con la presencia de la vicegobernadora y presidenta de la Cámara de Senadores, Alejandra Rodenas; el presidente de la Cámara de Diputados, Miguel Lifschitz; los ministros del supremo tribunal de justicia santafesino, Rafael Gutiérrez; Eduardo Spuler; María Angélica Gastaldi; Mario Netri y Daniel Erbetta; y el procurador General, Jorge Barraguirre, entre otros.

En la oportunidad, el gobernador Perotti afirmó que “es imprescindible mejorar la prestación de justicia. Es evidente un reclamo de mayor intensidad, con el deseo de fallos justos y oportunos. Para eso es fundamental que pongamos al día muchas de las instancias normativas que la provincia carece”.

“Aquí hay una demostración clara de diálogo con el Poder Judicial, con el Poder Legislativo, con las universidades, con los colegios profesionales de abogados y de procuradores. Quizás hasta el año pasado algunos discutían si el expediente tenía que digitalizarse o no. Hoy nadie discute eso. Es clave que los profesionales puedan tener acceso a los expedientes digitales, a través de la conectividad, en cualquier lugar de la provincia”, concluyó el gobernador.

Por su parte, Falistocco agradeció la presencia del gobernador y demás autoridades provinciales y afirmó que “afrontamos nuevos desafíos, como son la conectividad, la informatización, la información ciudadana, nuevas competencias y jurisdicciones y reformas procesales. Todo con el firme propósito de afianzar la Justicia. En particular, corresponde destacar la importancia central que adquiere que toda la provincia de Santa Fe avance en un proyecto de conectividad integral que vincule y permita relacionar a nuestro Poder Judicial con el resto de los organismos”.

Además, se refirió a Ley Micaela y sostuvo que “si no se incorpora la perspectiva de género en la toma de decisiones judiciales, seguiremos fracasando en la lucha por la igualdad real de las mujeres. Tengo que destacar que esta Corte fue pionera en capacitaciones en perspectiva de género, donde se logró la capacitación de más de tres mil integrantes del Poder Judicial”, finalizó el presidente de la Corte Suprema de Justicia.

**Acceso A La Justicia**

Por otro lado, el ministro de Gobierno, Justicia, Derechos Humanos y Diversidad, Roberto Sukerman, sostuvo que “desde el Ministerio de Gobierno tenemos vínculo con el Poder Judicial y con el Poder Legislativo. Por lo tanto, destacamos las reuniones con los distintos sectores políticos que son permanentes. Seguiremos trabajando en una agenda en la coyuntura y que va más allá de la coyuntura. Nos vamos aproximando cada vez más al comienzo del año legislativo y, en el mientras tanto, vamos alimentando el debate en las dos cámaras con proyectos que entendemos que tienen que tener tratamiento inmediato”.

Por último, el secretario de Justicia, Gabriel Somaglia, afirmó que “tenemos que trabajar para que la conectividad pueda llegar a los rincones más pequeños de la provincia, donde hay justicia comunitaria que en algunos lugares no hay este servicio, y en algunos es muy precaria. Entonces, el desafío es apuntar para que en cada comunidad se puedan resolver distintos conflictos como una cuestión laboral, accidentes de tránsitos o alquileres”.

“Otro tema que es necesario emprender, algo de lo que no estamos en sintonía con la Nación, es tener una ley de víctimas con perspectiva de género. Hoy las estadísticas vinculadas a los femicidios, a los abusos en relación al género femenino está a la luz del día, y me parece que la ley de víctimas que nos merecemos no solamente tiene que atender a los genéricos, si no en particular a esa temática”, finalizó Somaglia.

**Programa Santa Fe + Justicia**

El acuerdo incluye a los tres poderes del Estado, al Ministerio Público de la Acusación (MPA); al Servicio Público Provincial de Defensa Penal; la Federación de los Colegios de Abogados; Colegios de Procuradores de la Provincia de Santa Fe; Colegio de Magistrados; las facultades de Derecho de las universidades Nacional del Litoral y Nacional de Rosario, de la Universidad Católica de Santa Fe y de la Universidad Católica Argentina de Rosario; el Sindicato de Trabajadores Judiciales de la provincia de Santa Fe; y las cámaras de Diputados y Senadores de la provincia.

El acta firmada define una serie de acciones para lograr los objetivos, que propone el programa Santa Fe + Justicia, a través del cual el Estado procure todas las soluciones posibles para aquellos sectores en condiciones de vulnerabilidad, sin posibilidad de acercamiento y conexión a las áreas de justicia formal.

En este sentido, el primer punto del acuerdo refiere a la informatización y digitalización del Poder Judicial. El segundo aspecto del acuerdo está referido a la Información Ciudadana. El tercer ítem apunta al Abordaje del Mapa Judicial, desafío que plantea la necesidad de valorizar y atribuir una mayor competencia a los Juzgados Comunitarios de Pequeñas causas.

Los desafíos procesales que se presentan requerirán de consenso para avanzar hacia nuevas normas, que demanda la ciudadanía en su conjunto, tales como código para consumidores; código electoral y otro que consolide los procesos constitucionales; y el Código Procesal Penal Juvenil, Ley de Ejecución Penal, y una Ley de Víctimas de delitos atendiendo la perspectiva de Género.