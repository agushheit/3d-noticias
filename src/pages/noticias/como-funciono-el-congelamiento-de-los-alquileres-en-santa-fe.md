---
category: La Ciudad
date: 2021-04-03T06:00:39-03:00
thumbnail: https://assets.3dnoticias.com.ar/ciudadedificio.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: "¿Cómo funcionó el congelamiento de los alquileres en Santa Fe?"
title: "¿Cómo funcionó el congelamiento de los alquileres en Santa Fe?"
entradilla: El espacio Encuentro realiza una encuesta con el objetivo de conocer la
  situación y recabar las opiniones de quienes alquilan en la ciudad de Santa Fe.

---
El espacio Encuentro realiza una encuesta con el objetivo de conocer la situación y recabar las opiniones de quienes alquilan en la ciudad de Santa Fe.

El espacio Encuentro lanzó un nuevo sondeo para conocer la situación y recabar las opiniones de quienes alquilan en la ciudad de Santa Fe.

En el contexto del vencimiento del DNU dictado por el gobierno nacional que -entre otras medidas- “congelaba” los precios y suspendía los vencimientos de los alquileres, el propósito del espacio es conocer si resultó de utilidad y cómo afecta a los inquilinos de la ciudad de Santa Fe la decisión de no prorrogarlo.

También se pretende conocer la opinión sobre las modificaciones a la ley de alquileres, su aplicación y los alcances.

Lucas Simoniello, concejal e integrante del espacio, explicó que “durante el año pasado realizamos sondeos en mayo, junio y septiembre (los informes se pueden consultar en este link) para realizar un seguimiento de la situación de los inquilinos e inquilinas de la ciudad”.

“También, con un equipo de profesionales, pudimos responder cerca de 200 consultas. Por eso volvemos a ponernos a disposición para evacuar cualquier duda sobre este tema o brindar nuestro asesoramiento. Nos pueden contactar a nuestras redes (Facebook, Instagram o vía mail: encuentro.sfe@gmail.com)”.