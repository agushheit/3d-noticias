---
layout: Noticia con imagen
author: "Fuente: LT10 - Agencias Buena Vibra"
resumen: Vacaciones en el exterior
category: Agenda Ciudadana
title: "Vacaciones en el exterior: exigencias para recibir a los argentinos"
entradilla: Algunos de los destinos más elegidos por los argentinos imponen
  algunas restricciones que complican la entrada; otros tienen menos requisitos.
date: 2020-11-19T12:03:34.493Z
thumbnail: https://assets.3dnoticias.com.ar/playa.jpeg
---
La cuarentena impuso, entre otros tantos límites, los cierres de fronteras y las exigencias para entrar a quienes no son nacidos o residentes de un país.

La idea es evitar la propagación del virus y se aplica a todas las nacionalidades, pero con la finalidad de reactivar el turismo se van haciendo las reaperturas de fronteras y vuelos para los que quieran salir del país este verano.

## **DESTINOS MÁS VISITADOS POR ARGENTINOS**

Entre los destinos más elegidos por los argentinos, figuran Uruguay, Chile, Brasil, México, España, Italia, Miami, Nueva York. Alguno de estos lugares tiene restricciones muy estrictas, otros menos y algunos ninguna.

## **Medidas de cada país**

Veamos cuáles son las medidas en cada lugar y todo lo que debemos saber. Empecemos por los países limítrofes.

Las medidas más restrictivas son las impuestas por Uruguay y Chile. Ambos países permiten únicamente el ingreso de ciudadanos y extranjeros residentes, con algunas excepciones más: por ejemplo, quienes tengan un permiso especial para viajar por un contrato laboral. Brasil prácticamente no tiene restricciones.

* ## **CHILE**

Los permisos especiales serán analizados uno por uno antes de ser aprobados y exigirán que los viajeros presenten un certificado médico con prueba de PCR de Covid-19 al llegar al país. Aquellos que no lo hagan, deberán aislarse durante los primeros 14 días de estadía.

* ## **URUGUAY**

Impone fuertes restricciones para los extranjeros, restricciones que son mayores cuando hablamos de volar desde Argentina, porque sigue sin haber vuelos. Los ciudadanos y residentes uruguayos pueden viajar desde Argentina en auto, por los puentes, o en el buque Francisco de la empresa Buquebus, el único barco habilitado para transportar pasajeros de un país al otro, porque es la única que renueva el 100% del aire cada 15 minutos. Va de Buenos Aires a Montevideo, ida y vuelta, dos veces por semana: lunes y viernes.

Los residentes o ciudadanos uruguayos que viajen desde la Argentina hacia Uruguay por los puentes, deben presentar en la frontera un certificado médico con el resultado negativo de la prueba RT-PCR. Quienes viajen por vía marítima deben realizarse el hisopado en el puerto, antes de embarcar. La tripulación del buque Francisco espera a tener todos los resultados de la tripulación antes de zarpar y solo permite el ingreso de aquellos cuyo test sea negativo.

Al llegar a Montevideo, los viajeros deben permanecer los primeros 14 días en cuarentena. En caso de que prefieran acortar el aislamiento, pueden aislarse por siete días y luego hacerse un segundo hisopado. Si este también es negativo, pueden abandonar el aislamiento.

Brasil y México: autorizan el ingreso de cualquier extranjero sin exigir cuarentena ni un permiso especial. Brasil es el único de los países que limitan con Argentina que permite el ingreso de cualquier extranjero sin exigir nada.

* ## **ESTADOS UNIDOS**

El caso de Estados Unidos es especial, ya que cada estado impone sus propias restricciones. La única política nacional que impone restricciones no afecta a los argentinos. Según fuentes de la embajada norteamericana, no hay ninguna política a nivel nacional que prohíba la entrada de argentinos.

## **Nueva York**

Exige aislamiento obligatorio a todas las personas que provengan de países con un nivel 3 o 2 de “riesgo de coronavirus”, lo cual incluye a la Argentina.

## **Florida**

Florida permite la entrada de cualquier extranjero que tenga la visa. Para el mes de noviembre, Aerolíneas Argentinas tiene habilitados cuatro vuelos semanales desde Ezeiza hasta Miami. Para diciembre, hay disponibles cinco vuelos por semana, y para enero, seis. Este trayecto también es realizado por líneas aéreas extranjeras.

## **Miami**

Los aviones llegan completos con argentinos en plan de vacaciones y compras.

* ## **ESPAÑA**

Las personas que ingresan desde cualquier parte del mundo a España no tienen que hacer cuarentena. Desde el punto de vista sanitario, solo deben llenar previamente una declaración de salud online. Pero existían hasta hoy, 12/11, restricciones en el acceso. Las indicaciones eran: Solo pueden ingresar al país aquellas personas que tengan pasaporte de la comunidad europea, sean residentes legales o tengan un permiso especial. Estos últimos son analizados por el consulado español en el país desde el que viaja la persona.