---
category: Agenda Ciudadana
date: 2021-05-14T09:18:34-03:00
thumbnail: https://assets.3dnoticias.com.ar/inflasión.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam/ Indec
resumen: 'Batacazo al bolsillo: La inflación de abril fue del 4,1%'
title: 'Batacazo al bolsillo: La inflación de abril fue del 4,1%'
entradilla: 'Los precios al consumidor subieron 4,1% respecto de marzo y 46,3% interanual.
  Los rubros que más aumentaron.

'

---
La inflación de abril fue del 4,1% según el Instituto Nacional de Estadísticas y Censo (Indec). De esta manera, se desaceleró respecto de marzo pero dio arriba del 4% por cuarta vez en los últimos cinco meses. En el último año, los precios crecieron 46,3%.

Las consultoras y entidades financieras del sector privado que participaron del último Relevamiento de Expectativas de Mercado (REM) habían previsto 3,8%.

Lo que más subió en el cuarto mes del año fue la indumentaria (+6%), que ya había registrado importantes incrementos en marzo. Detrás le siguieron los aumentos en transporte (5,7%), impulsados por alzas en adquisición de vehículos, combustibles y taxi.

En marzo la inflación había sido del 4,8%, un récord desde septiembre de 2019. Esto salto fue impulsado, por un lado, por factores estacionales vinculados con el inicio de clases, que provocó aumentos del 28,5% en educación, y el cambio de temporada en ropas exteriores, que generó incrementos del 10,8% en prendas de vestir y calzado.

Sin embargo, también influyó la inercia que vienen exhibiendo los aumentos de precios desde el último trimestre del año pasado. Los alimentos treparon 4,6% impulsados por algunas verduras y lácteos.

"En el primer tercio del año, ya se consumió casi el 60% de la meta del presupuesto 2021. De esta manera, la pauta oficial quedó absolutamente descartada, y el “nuevo objetivo” del gobierno debería ser que la suba de precios termine el año por debajo del 40%, algo que, a hoy, también parece muy difícil", explicó Rajnerman.

Los participantes del REM de abril proyectaron que la inflación minorista acumulada para diciembre de 2021 se ubicará en 47,3% interanual, lo cual significó un aumento de 1,3 puntos porcentuales respecto de los pronósticos provistos a fines del mes anterior (46,0% i.a.).