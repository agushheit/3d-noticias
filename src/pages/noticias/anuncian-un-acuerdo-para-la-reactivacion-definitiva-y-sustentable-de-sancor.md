---
category: Agenda Ciudadana
date: 2021-11-09T06:15:41-03:00
thumbnail: https://assets.3dnoticias.com.ar/sancor.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: Anuncian un acuerdo para la reactivación definitiva y sustentable de Sancor
title: Anuncian un acuerdo para la reactivación definitiva y sustentable de Sancor
entradilla: 'El gremio que nuclea a los trabajadores de la industria láctea, salió
  a anunciar un acuerdo –las autoridades participantes, aún no- para la “reactivación
  definitiva y sustentable” de la Cooperativa SanCor Cul.

'

---
En su página web, Atilra directamente anuncia que se concretó un acuerdo “para la reactivación definitiva de SanCor”, aunque ni las altas autoridades -entre ellos, los ministros Kulfas y Moroni y el titular del Banco Nación, Hecker- que participaron del encuentro desarrollado ayer ni fuentes de la empresa compartieron aún el entusiasmo por el anuncio.

 El gremio dio cuenta que en el mediodía de ayer se desarrolló un encuentro con el ministro de Desarrollo Productivo de la Nación, Matías Kulfas; el ministro de Trabajo, Claudio Moroni, el presidente del Banco Nación, Eduardo Hecker, el representante y presidente de la cooperativa Sancor, José Pablo Gastaldi, el Secretario General de Atilra, Héctor Ponce y el grupo de empresario-inversor integrado por José Urtubey, Marcelo Figueiras y Jorge Estevez, para llevar adelante el compromiso de una “reactivación definitiva y sustentable” de la Cooperativa Sancor Cul.

 Atilra señala también que “el gobierno se mostró dispuesto a otorgar y poner a disposición ‘todas las herramientas de financiamiento’ que tenga a su alcance para logra esa reactivación sustentable, remarcó Kulfas. ‘Vamos a apoyar ese plan de negocios ambicioso para recuperar el liderazgo que tuvo esta cooperativa’, sentenció el ministro de Producción. Por su parte, agrega Atilra en su comunicado de prensa, el presidente del Banco Nación subrayó que ‘tenemos la decisión política de colaborar para llegar a buen término, vamos a encontrar un camino entre todos y comprometo el apoyo del banco para esto”.  

 El gremio de los trabajadores lácteos, que tiene su sede central en Sunchales, al igual de la empresa SanCor, expresa además que “la propuesta fue consensuada por todos los sectores involucrados y hubo halagos por parte de los empresarios por el prestigio y reputación que tiene la cooperativa”. Y agrega expresiones de Urtubey: “Todos pusimos el hombro para llegar a este acuerdo» subrayó Urtubey, «sabemos que falta inversión de capital de trabajo y por eso estamos aquí”, señaló.

 La reunión fue seguida de cerca por muchísimos trabajadores y trabajadoras de Sancor y por la Atilra de todo el país. “El acompañamiento de los afiliados a la organización sindical se hizo sentir en las calles mientras se llevaba a cabo la reunión. Estuvieron todos presentes generando un marco de respaldo y de confianza hacia los trabajadores y trabajadoras de Sancor y la organización gremial”, sostuvo el gremio.   

 **Memorandum de compromiso**

 Atilra también dio a conocer el articulado del memorandum de acuerdo al que llegaron las partes y se destaca en él que “en virtud de las dificultades del mercado lácteo en general y la crítica situación de SanCor en particular, se ha venido explorando entre las Partes, distintas alternativas para una solución sustentable en el tiempo, que impulse a SanCor a recobrar su posición de liderazgo en el mercado lácteo nacional, manteniendo la mayor cantidad de puestos de trabajo reales que fuera posible. En función de ello se han delineado las bases de un nuevo modelo de gestión, atendiendo las necesidades más urgentes de todas las partes interesadas”. 

 Para lograr ese objetivo, las partes manifiestan su voluntad de crear un “patrimonio de afectación” y un fideicomiso.

 En su parte medular, el documento señala que entre los compromisos está el de “destinar partidas específicas implementadas por el Estado Nacional, consistentes en fondos con destino exclusivo a la adquisición de materia prima y los respectivos insumos para su elaboración y transformación industrial, con el objeto de lograr un equilibrio operativo que permita una transición hasta el arribo de las líneas de crédito que por intermedio del Ministerio de Desarrollo Productivo y los bancos de la Nación y/o BICE permitan que el Grupo Empresario inicie su actividad”.

 Similar destino -agrega- se le dará “al producido de la venta por parte de SanCor de los activos accionarios que posee de otra u otras empresas, todo lo cual deberá encaminarse a colocar a la empresa en un nivel de equilibrio operativo, el que se encuentra en los aproximadamente 1.000.000 de litros diarios promedio, nivel que deberá alcanzarse en el menor plazo posible”.  

 Otro compromiso que el gremio celebra particularmente es que SanCor y el fideicomiso del grupo empresario se comprometen a no producir despidos sin causa entre los aproximadamente 1450 trabajadores que tiene la cooperativa.

 **Cautela empresaria**

Desde la empresa, también se refierieron al encuentro: "Hoy (por ayer) se reúnen las partes involucradas en esta instancia para formalizar una propuesta y las intenciones de cada uno, intercambiar las primeras opiniones, tras lo cual, SanCor analizará la factibilidad y conveniencia técnica. Si fueran consideradas oportunas, a su debido tiempo, la presentaría a consideración de la Asamblea de Asociados, quienes tienen la potestad de aprobar, rechazar o sugerir ajustes".