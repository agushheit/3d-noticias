---
category: La Ciudad
date: 2020-12-26T12:29:54Z
thumbnail: https://assets.3dnoticias.com.ar/integrar.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: 'Plan Integrar: construir propuestas colectivas para el territorio'
title: 'Plan Integrar: construir propuestas colectivas para el territorio'
entradilla: 'La Municipalidad apuesta a fortalecer el trabajo de las Redes de instituciones
  de cada barrio. Seis organizaciones cuentan cómo se trabajó en la pandemia para
  cubrir las necesidades sociales. '

---
También se avanza en proyectos para el espacio público y en temáticas que involucran a los jóvenes y a las familias. Una construcción colectiva y articulada que se viene afirmando en la capital.

«Los jóvenes están cruzados por realidades diferentes, difíciles, droga, delincuencia. Ahora tienen un primer empleo en las cuadrillas de limpieza generadas por el municipio. Mantienen limpio su barrio, lo caminan, demuestran su trabajo, ya no son solo los pibes que están señalados». Isolina Rolón relató, desde Yapeyú, el trabajo conjunto de la Municipalidad con la Red de instituciones del barrio, en el marco de la construcción de acciones territoriales colectivas que propone el Plan Integrar.

Sobre este programa, que es pilar de su gestión, el intendente Emilio Jatón indicó que uno de los ejes es potenciar las Redes de instituciones de los barrios, muchas de las cuales venían trabajando desde hace tiempo y otras que aún están en formación. «Ahí estamos proponiendo, escuchando y debatiendo para que cada acción de gobierno, grande o chica, sea una construcción colectiva», sostuvo.

«Estamos haciendo una política que tiene al vecino como protagonista», destacó el intendente, al tiempo que refirió que «las Redes de instituciones son el corazón del Plan Integrar porque acá es donde hacemos lo que propusimos desde el primer día: construir las decisiones con los vecinos».

A estas Redes, que ya son 20, la Municipalidad suma sus propuestas que resultan enriquecidas con la mirada de quienes viven en el barrio y saben mejor que nadie cuáles son las necesidades y las prioridades a resolver. Como Isolina, que es referente de la Asociación Civil Sinergia del Norte y Deportivo Gambeta y, desde su sede en Yapeyú, quiere ver a su barrio salir adelante, con mejores condiciones para los vecinos.

«Es importante que los jóvenes vean que ellos pueden hacer cosas lindas. Nosotros con la Municipalidad, con la parte de Ambiente, recuperamos el Solar Villa Teresa. Fue un trabajo bastante difícil, porque primero ellos tenían que sentir que el espacio era para ellos. Luego vieron que necesitaba y pudieron colocarle plantas, un banco, una mesa redonda para construir. Hoy no podemos compartir un mate, pero sí nos podemos sentar en círculo a conversar», explicó la vecina.

Mariano Granato, secretario de Integración y Economía Social, indicó que «el Estado municipal, que es el primer nivel de proximidad, debe estar en los barrios trabajando codo a codo con las instituciones que conocen a los vecinos». Uno de los objetivos del Plan Integrar es fortalecer las redes socio-estatales presentes en los barrios, con el objeto de robustecer sus capacidades asociativas y de gestión para el desarrollo social y urbano de sus lugares.

<br/>

<span style="font-family: helvetica; font-size: 1.35em; font-weight: 800;">**¿Qué son las Redes?**</span>

Las Redes de instituciones barriales son espacios donde se discuten colectivamente las necesidades de sus lugares y se buscan, mediante reuniones periódicas, las formas de resolverlos y de apoyarse mutuamente en el trabajo. 

En cada barrio la conformación es distinta y participan desde organizaciones sociales, escuelas, centros de salud, CAF, vecinales, comedores y merenderos comunitarios, iglesias, clubes, asociaciones civiles y otros estamentos municipales y provinciales con presencia en el lugar. Algunas de las Redes tienen más de 10 años de trabajo, otras se disolvieron un tiempo y resurgieron, y algunas recién se están conformando este año.

La Iglesia Evangélica de barrio San Lorenzo hace 14 años que está en el lugar y, desde hace 6 ó 7 que integra la Red barrial. «Trabajamos muy bien en la Red, cuando hay alguna necesidad, la ponemos en el grupo y enseguida vamos colaborando en lo que podemos», dijo el pastor Mario Ledesma, quien desde esa institución viene trabajando con los jóvenes y los consumos problemáticos.

«Nosotros trabajamos con chicos con problemas de adicciones y violencia, y colaboramos para que vayan a rehabilitación. En esa sede de la Iglesia, brindamos cursos de peluquería avanzada y básica, herrería, electricidad. En estos momentos de pandemia, estamos más abocados al curso de panadería; producimos para casi 100 chicos de una copa de leche de Arenales y un comedor», contó Mario. Y dijo que ahora también estamos trabajando en la nueva Estación –la ex Escuela de Trabajo-, y abrimos dos ámbitos: uno para los chicos con problemas de adicción y otro para sus familiares.

Agregó que «pensar las políticas públicas y sociales con diferentes niveles del Estado es muy importante. Trabajar en Red significa contar con el Centro de Salud, los ámbitos de Niñez, la Municipalidad, que facilitan muchas cosas a cada institución».

Al respecto, Granato señaló que «la presencia del municipio en los barrios robustece a la ciudadanía. Estamos construyendo un Estado próximo que dialoga con las instituciones y se preocupa por brindar las mismas oportunidades en toda la ciudad».

<br/>

<span style="font-family: helvetica; font-size: 1.35em; font-weight: 800;"> **Sobrellevar la pandemia**</span>

Las necesidades de la población se multiplicaron con el Covid a raíz del cierre que sufrieron las actividades económicas y por el repliegue temporal de la presencia estatal en territorio debido al aislamiento. Ante ese panorama, la Municipalidad hizo hincapié en fortalecer las organizaciones sociales dentro de las Redes. Allí donde el centro de salud o la escuela no llegaba, sí había un comedor o asociación que sostenía en lo cotidiano el vínculo comunitario con las familias del barrio.

«La Municipalidad se unió a trabajar con nosotros en la Red y está muy interesada en las necesidades de los vecinos. Nos está fortaleciendo con alimento seco porque desde que empezó todo el tema de la pandemia, la necesidad inmediata fue poder armar una olla popular», contó Carina González, referente de la asociación civil Manos Solidarias, de Villa Hipódromo.

En el momento más duro de la pandemia, no solo se cubría a la población que ya venía siendo acompañada por las instituciones de la Red. «También venían las familias que no podían salir a ganarse la moneda del día a día, gente que es de trabajo, pero que con el aislamiento no tuvo opción así que la veíamos venir con su _tupper_ a retirar su comida porque ese día no tuvo la changa», dijo Carina.

Para esta referente social, el trabajo de la Red «es muy satisfactorio» y destacó que los vecinos «se están prendiendo muchísimo» en las propuestas del municipio, articuladas con todas las organizaciones. «Estamos trabajando tanto con el espacio público como en diferentes temas: violencia, jóvenes, género. Cuando hay ganas y se puede articular, todo es posible para poder generar cosas lindas para el barrio», alentó la vecina.

Por su lado, Mario coincidió en el soporte recibido durante la pandemia: «La Municipalidad, por ejemplo, fue un apoyo bárbaro con todo el tema del Covid porque se van enfermando algunas personas y, ni bien avisamos que una familia quedó aislada, las personas que trabajan en el municipio les proveen alimentos y ya los visitan para ver cuáles son las otras necesidades. Sin duda se potenció nuestro trabajo».

De igual manera, Isolina desde Yapeyú dijo: «estamos agradecidos con la Municipalidad porque, si no tuviéramos su apoyo, sería muy difícil continuar. Tenemos alrededor de 400 personas que reciben viandas de comida. Hay que sostenerlo en este momento muy difícil, pero nosotros seguimos dándole lucha».

Por su parte, la Vecinal del Centenario se abocó, como parte integrante de la Red, a fomentar que los vecinos del barrio se cuiden del Covid. «Realizamos una serie de actividades, entre ellas, tres murales en el barrio con una misma frase: _Nos cuidamos entre todos._ Y se han involucrado desde los centros de salud, la vecinal, las escuelas y el centro de jubilados. Tenemos que seguir cuidándonos, tanto los jóvenes como los adultos mayores», dijo José Cettour, presidente de esa vecinal.

«El trabajo en Red es importante porque las instituciones están trabajando en conjunto, sin banderías políticas y para mejorar la calidad de vida en el barrio y de los vecinos», dijo José, tras valorar las obras de cordón cuneta, ripiado, pavimento y desagües en el sector de calle Tacca.

<br/>

<span style="font-family: helvetica; font-size: 1.35em; font-weight: 800;"> **Sostener y recuperar lazos**</span>

Roxana Orieta pertenece a la asociación civil Brillantísima, que nació hace 8 años en Coronel Dorrego, para brindar contención a unos 80 chicos y jóvenes mediante la comparsa. «Tenemos percusión, baile, pero también cursos de corte y confección, panadería, herrería, y un merendero que funciona dos días a la semana y al que los sábados, unos 90 chicos van a retirar las viandas para sus familias».

«Con la pandemia quedamos un poquito aislados, pero tenemos ganas de volver a comenzar con grupos reducidos en la comparsa; también estamos trabajando con la Red en tratar de mejorar la conectividad del barrio para la vinculación de los chicos con la escuela», contó Roxana sobre los proyectos. Y añadió: «la muni viene bien, apoyando a las Redes, está tratando de hacer todo lo que está a su alcance para contenernos a las instituciones, para que no nos sintamos solas tratando de integrar a los jóvenes que han perdido sus lazos con todos en esta pandemia, que atraviesan problemas y adicciones».

Finalmente, y como resumen del impacto que la construcción colectiva del Integrar genera en un barrio como Villa Hipódromo, donde están en marcha obras de infraestructura, Carina González subrayó: «para el que vivió toda la vida acá, ver hoy la transformación de nuestro barrio es muy lindo, porque tenemos la luz, el comedor comunitario, el acceso a las cloacas, el cordón cuneta. Me da mucha satisfacción y mucha vida. Imaginate lo que es para nosotros decir: qué lindo es ver cómo avanza día a día nuestra querida tierra».