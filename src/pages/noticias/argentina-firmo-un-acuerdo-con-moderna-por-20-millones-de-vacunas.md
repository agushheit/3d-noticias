---
category: Agenda Ciudadana
date: 2021-07-12T06:15:28-03:00
thumbnail: https://assets.3dnoticias.com.ar/MODERNA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Argentina firmó un acuerdo con Moderna por 20 millones de vacunas
title: Argentina firmó un acuerdo con Moderna por 20 millones de vacunas
entradilla: Se trata del primer contrato que permitirá recibir vacunas basadas en
  la plataforma ARNm y habilita la posibilidad de obtener durante este año una donación
  de dosis de parte de Estados Unidos.

---
Argentina suscribió este domingo un acuerdo con la compañía Moderna Inc. por el suministro de 20 millones de dosis de su vacuna Moderna Covid-19 o las dosis equivalentes para su administración como refuerzo, informó el Ministerio de Salud.

Se trata del primer contrato que permitirá recibir vacunas basadas en la plataforma ARNm y habilita la posibilidad de obtener durante este año una donación de dosis de parte de Estados Unidos.

De esta manera, y luego de la decisión del Gobierno nacional de adecuar la Ley de Vacunas 27.573, Argentina firma el primer contrato para recibir dosis basadas en la plataforma de ARNm contra la Covid-19 a partir del primer trimestre del 2022.

Adicionalmente, la celebración del acuerdo abre la posibilidad de recibir durante este año una donación por parte del Gobierno de Estados Unidos de más dosis de esta vacuna.

La compañía estadounidense presentó los ensayos y la documentación requerida por las principales autoridades regulatorias con el objetivo de lograr la autorización de emergencia para su aplicación en adolescentes, situación que podría definirse muy próximamente, tanto en FDA (Administración de Medicamentos y Alimentos de los Estados Unidos) como en la EMA (Agencia Europea de Medicamentos), informó el Ministerio de Salud de la Nación.

"Estamos dando un paso clave en nuestra estrategia de contar con todas las plataformas tecnológicas posibles, mientras continuamos las gestiones con otros laboratorios", destacó Vizzotti.

**Las características de la vacuna**

La vacuna Moderna Covid-19 es una vacuna de ARNm que codifica la proteína Spike (S) y ha demostrado una eficacia superior al 90%, con adecuado perfil de seguridad, lo que explica su autorización de emergencia para el uso en personas de 18 años y mayores por parte de las agencias regulatorias de más de 50 países, y que se encuentre en la Lista de Uso de Emergencia (EUL) de la Organización Mundial de la Salud (OMS).

Las características de almacenamiento y conservación de esta vacuna también se asemejan a las que ya se utilizan en nuestro país.

Eso significa una ventaja desde el punto de vista logístico, con lo cual, una vez recibida, puede ser distribuida y estar disponible para su uso casi en forma inmediata.

Por otra parte, la vacuna ARNm propuesta para su uso como refuerzo, tiene carácter universal y multivalente, ya que la plataforma de la vacuna Moderna Covid-19 permite ser modificada en relación a las variantes que están en circulación.

Con este acuerdo, Argentina se constituye en el cuarto país del mundo, detrás de Suiza, Australia y Estados Unidos, además de la Unión Europea, en asegurarse la posibilidad de contar también con refuerzo.

En tanto, Argentina superará este lunes los 29 millones de vacunas recibidas desde el inicio de la pandemia de coronavirus con el arribo de dos vuelos de Aerolíneas: uno desde Rusia, con otro lote de Sputnik V, y otro de China, como parte del acuerdo de suministro de 8 millones de dosis mensuales de inoculantes de Sinopharm.