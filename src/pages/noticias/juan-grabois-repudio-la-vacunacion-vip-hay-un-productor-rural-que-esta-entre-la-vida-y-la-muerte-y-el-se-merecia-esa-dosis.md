---
category: Agenda Ciudadana
date: 2021-02-24T06:57:58-03:00
thumbnail: https://assets.3dnoticias.com.ar/grabois.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Clarín
resumen: 'Juan Grabois repudió la vacunación VIP: “Hay un productor rural que está
  entre la vida y la muerte, y él se merecía esa dosis”'
title: 'Juan Grabois repudió la vacunación VIP: “Hay un productor rural que está entre
  la vida y la muerte, y él se merecía esa dosis”'
entradilla: El dirigente social dijo que se siente “indignado” por el caso y pidió
  que se publique “una lista de todo aquel que se vacuna”.

---
Juan Grabois, referente de la Unión de Trabajadores de la Economía Popular (UTEP), repudió la vacunación VIP contra el coronavirus de funcionarios y allegados al Gobierno y puso el ejemplo de "un productor rural que está entre la vida y la muerte, que se merecía esa dosis por todo lo que le pasó en su vida".

Si bien Grabois reconoció que siente "indignación" por el escándalo que sacude al oficialismo, hizo una autocrítica y dijo que "para nuestro campo político, popular y latinoamericano, cuando explotan estos escándalos nos indignamos pero cuando no explotan nos hacemos los tontos".

"Esto me indigna mucho, además esta historia comenzó con un persona que es particularmente despreciable, que era el fiscal de la pureza ideológica del progresismo argentino, el primero que se jacta de haberse vacunado (Horacio Verbitsky)", sostuvo en declaraciones al canal C5N.

Para el dirigente social, "acá pasaron cosas muy feas, que opacan una campaña de vacunación que venía bien, una estrategia de negociación multipolar por la vacuna que evitó el alineamiento automático con los norteamericanos, que enfrentó una campaña de desinformación tremenda, que movilizó a miles y miles de trabajadores, dirigentes y funcionarios para desarrollar su tarea".

"Esto desde luego que opaca y hay que señalarlo como algo que estuvo muy mal, y avanzar hacia una etapa con la nueva ministra y la gente que quiera trabajar profundamente para esto con mayor transparencia", consideró.

Al mismo tiempo, Grabois dijo que tiene una propuesta que "no cayó muy bien" en el Frente de Todos, para "que se publique la lista de todo el mundo que se vacuna, en todos lados".

Sobre la salida del ahora ex ministro Ginés González García, dijo: "Cuando el pueblo pide sangre, hay que escucharlo, porque hay justicia en ese reclamo. Cuando el pueblo pide, para decirlo vulgarmente, que se corte una cabeza, es una cabeza que hay que cortar, en sentido figurado".

"Todo privilegio, sea estructural como los económicos, de género y de clase, o particular y político como el abuso de poder, es enemigo de cualquier proyecto que tenga que ver con la igualdad y la justicia social", señaló.

El escándalo del Vacunatorio VIP montado por el Ministerio de Salud de la Nación estalló el viernes pasado, a partir de una declaración del periodista Horacio Verbitsky, quien reveló que tras una comunicación con Ginés González García, había conseguido una vacuna contra el coronavirus, que se la dió en la sede de esa cartera.

A partir de eso, se fue conociendo que varios funcionarios, ex funcionarios, empresarios afines al Gobierno y otros allegados habían sido vacunados cuando no correspondía por el cronograma estipulado.

Eso desencadenó la renuncia de González García, quien fue reemplazado por Carla Vizzotti, además de denuncias ante la Justicia y un fuerte repudio de todo el arco opositor, además de algunos integrantes de la coalición de gobierno.