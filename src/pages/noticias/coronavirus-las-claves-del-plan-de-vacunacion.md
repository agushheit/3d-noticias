---
category: Agenda Ciudadana
date: 2020-12-21T14:06:32Z
thumbnail: https://assets.3dnoticias.com.ar/2112vacunacion.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Telam'
resumen: 'Coronavirus: las claves del plan de vacunación'
title: 'Coronavirus: las claves del plan de vacunación'
entradilla: El esquema abarcará la participación de siete ministerios y el 78 % del
  personal estratégico de las fuerzas de seguridad, aviones, helicópteros, 17 hospitales
  militares y también escuelas rurales.

---
El plan de vacunación contra el coronavirus que prepara el Gobierno, articulado entre siete Ministerios, contará con la intervención del 78% del personal estratégico de las fuerzas de seguridad, aviones, helicópteros, 17 hospitales militares, postas sanitarias y escuelas rurales, en un esquema que tendrá un despliegue de 80 mil efectivos de la cartera de Defensa y el aporte de 10 mil estudiantes avanzados de las carreras de salud.

Así surge de los datos contenidos en el esquema de trabajo, elaborado en forma conjunta por los Ministerios de Salud y Defensa, al que aportan recursos y logística las carteras de Economía, Interior, Educación, Seguridad y Turismo, detallados en un informe.

# **Un mapa que localiza a los mayores de 60 años**

De acuerdo con el diseño del Gobierno, los operativos de vacunación estarán apoyados en un «mapa georeferenciado por localidad», que identifica a los habitantes de más de 60 años en cada localidad del país y que fue aportado por el ministro del Interior, Eduardo Wado  de Pedro.

Casi el 50 por ciento de esa población en riesgo -únicamente sobre la base de la edad- habita el AMBA, la CABA y la provincia de Buenos Aires (48,4%) y el partido de La Matanza concentra 225.344 personas, un número que lo ubica detrás de la ciudad de Córdoba y por delante de Rosario, San Miguel de Tucumán, Salta, CABA y el AMBA (sin CABA), entre otras.

Parte de las acciones en curso incluye el armado de centros de vacunación en escuelas del país, algo que está siendo gestionado e implementado por los equipos de salud de cada provincia, según las necesidades locales, en articulación con las carteras nacionales.

# **La coordinación**

Por ejemplo, desde hace algo más de un mes los Comandantes Conjuntos de Zona de Emergencia (CCZE) del Ministerio de Defensa trabajan con los distritos del país, a los que asesoran y brindan técnicas para agilizar el operativo y otras previsiones de cara al inminente arribo de los insumos.

«Las Fuerzas Armadas están en condiciones de desplegar a 80 mil efectivos, brindar transporte aéreo estratégico (4 aviones Hércules C130 y otras aeronaves, además de helicópteros) y también terrestre y fluvial», detalla el informe en el que se destacan las instalaciones militares para almacenamiento de insumos generales y 17 hospitales militares y postas sanitarias menores en todo el país, con todo su personal disponible.

Como parte del plan, también, el Ministerio de Educación, a través de la Secretaria de Políticas Universitarias, cerró un convenio con estudiantes avanzados de carreras de salud para aportar 10.000 voluntarios a la campaña.

# **La acción de las fuerzas de seguridad**

La cartera que conduce Sabrina Frederic se ocupará de la «seguridad de la logística y distribución de los de insumos para la campaña y para los centros; coordinará el despliegue del operativo de vacunación propia, con alcance al personal estratégico de las Fuerzas Policiales y de seguridad federales en todo el territorio nacional».

Según los criterios establecidos por la autoridad sanitaria, se estima que deberán vacunarse unos 76.497 efectivos; esto constituye el 78% del personal activo estratégico en las fuerzas de seguridad, detalla el informe.

En el panorama global que el Gobierno ya tiene trazado, se estableció, por ejemplo, que la localidad de El Angosto, en Jujuy, será la ubicada más al norte de todo el país adonde llegará el operativo de vacunación. Allí residen 20 personas de más de 60 años, de acuerdo con los registros oficiales.

De igual forma, la más austral es Ushuaia, en Tierra del Fuego, donde viven 8617 personas en edad de riesgo para un eventual contagio de Covid-19.

# **Las regiones con más adultos**

El futuro operativo prevé que las regiones con más cantidad de adultos que superan esa edad son: AMBA (sin CABA) con 2.118.845 personas; CABA, con 846.682; el interior de la provincia de Buenos Aires, con 835.912; Córdoba, con 675.907; Santa Fe, con 638.802; Mendoza, con 333.292; Tucumán, con 252.680; Entre Ríos, con 236.767; Salta, con 187.917; Misiones, con 173.519; Corrientes, con 172.618; Chaco, con 167.932; Santiago del Estero, con 133.487; Río Negro, con 125.296; y San Juan, con 120.881.

Sin embargo, al comparar las ciudades, las que registran más habitantes de 60 años o más son: Córdoba (253.254); Rosario (205.369); Mar del Plata (148.201); San Miguel de Tucumán (100.736); Salta (84.778); CABA (61.233, la mayoría en el barrio de Palermo); el AMBA -sin CABA- (55.908, la mayoría en La Plata); Corrientes (55.075); Posadas (51.004); Paraná (48.011); San Salvador de Jujuy (42.429, el barrio Manuel Belgrano), en un listado de 25 localidades.

El Ministerio del Interior y la Cámara Nacional Electoral trabajan en forma conjunta para actualizar los padrones, filtrados por rango etario por mesa, escuela o circuito electoral.

En un principio, el Ministerio de Economía «destinará un monto total de 13.698 millones de pesos para la compra de 22,4 millones de dosis de vacunas» contra la Covid-19.

# **La difusión de la campaña**

En otro orden, está bajo análisis una propuesta del Ministerio de Transporte para llevar adelante una campaña de difusión del operativo de vacunación en trenes, colectivos, en el aeropuerto de Ezeiza, estaciones de tren y de micros, con el objetivo de difundir las citas, días y horarios.

Está en estudio también la posible liberación del subte a pasajeros que se trasladen para vacunarse y la idea de establecer horarios y servicios especiales de trenes y colectivos, tanto en horarios nocturnos y para fines de semana; además del diseño de la distribución de las dosis desde los aeropuertos a través de _dollys_ refrigerados para no comprometer la cadena de frío.