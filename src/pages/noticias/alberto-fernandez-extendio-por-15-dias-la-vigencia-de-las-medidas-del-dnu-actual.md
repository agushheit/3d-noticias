---
category: Agenda Ciudadana
date: 2021-06-12T06:00:30-03:00
thumbnail: https://assets.3dnoticias.com.ar/RESTRICCIONES.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Alberto Fernández extendió por 15 días la vigencia de las medidas del DNU
  actual
title: Alberto Fernández extendió por 15 días la vigencia de las medidas del DNU actual
entradilla: Anunció en la mañana del viernes la extensión hasta el 25 de junio de
  la vigencia del Decreto de Necesidad y Urgencia promulgado dos semanas atrás.

---
El presidente Alberto Fernández anunció la prórroga del Decreto de Necesidad y Urgencia con medidas sanitarias hasta el próximo 25 de junio, alentó a la ciudadanía a "no dejar de cuidarse" y pidió no dudar de la eficacia de las vacunas contra el coronavirus, en el marco de la segunda ola que atraviesa el país.

La confirmación llegó en el acto en el que el jefe de Estado presentó el inicio de las obras civiles para el Centro Argentino de Protonterapia (CeArP), una institución dedicada al tratamiento contra el cáncer que contará con un "ciclotrón", tecnología disponible sólo en 30 países y cuya instalación en el país estuvo "demorada durante los cuatro años" de la gestión de Mauricio Macri.

"Estamos llegando a un punto en el que esperaba que una ley resolviera como seguir trabajando el tema, deberemos esperar una semana más", dijo Fernández respecto del tratamiento en el Congreso Nacional del proyecto de Emergencia Covid que determina las medidas a tomar considerando parámetros epidemiológicos concretos, que este jueves obtuvo dictamen favorable.

Mientras se espera por el tratamiento en la Cámara de Diputados se seguirá "prorrogando la decisión" que viene tomando el Ejecutivo en los últimos dos meses, dijo el Presidente.

"Estamos llegando a un punto en el que esperaba que una ley resolviera como seguir trabajando el tema, deberemos esperar una semana más"

**Alberto Fernndez**

Aunque Fernández no se refirió a la duración del nuevo DNU, fue la ministra de Salud, Carla Vizzotti -también presente en el acto- quien afirmó que la nueva norma regirá desde este sábado hasta el 25 de junio.

En tanto, desde Presidencia de la Nación, informaron que los parámetros para determinar los distintos tipos de restricciones según la situación sanitaria de cada distrito del país “serán la incidencia muy alta en los casos detectados (de más de 500 en los últimos 14 días) y la tensión del sistema de salud”.

Fernández reconoció que el país está "enfrentando con mucho esfuerzo y con mucho agobio social" a la pandemia y que si bien el Gobierno trabaja "para acelerar aún más" la vacunación, mientras tanto, "el virus existe, el virus muta, el virus complica la vida de la humanidad".

“Hay que dejar los debates en que nos quieren encerrar. Ya llegaron 18 millones de vacunas y estamos entre los 15 primeros países que ya han vacunado a su pueblo por lo menos con una dosis”, señaló el mandatario, y llamó entonces a "no bajar los brazos, no dejar de cuidarse".

Previamente, en su discurso, Vizzotti se había referido a la situación epidemiológica como el "momento más crítico de su segunda ola" y destacó que el país ya vacunó al "85% en los mayores de 60" y a todo el personal de salud.

"En el día de hoy llegó el contrato firmado de Sinopharm por 2 millones de dosis que van a llegar en junio. El lunes 943 mil dosis del contrato de AstraZeneca y se trabaja para el adelanto de 811 mil vacunas" de México, destacó la funcionaria.

Detalló además que la Comisión Nacional de Inmunizaciones ha determinado “seguir vacunando personas sin condiciones de riesgo de entre 50 a 59 años, después entre 50 y 54 y así seguirá bajando hasta llegar a todos los mayores de 18 años con al menos una dosis lo más rápido posible”.

**DNU**

Respecto del nuevo DNU -que comenzará a regir esta medianoche- se extenderá por las próximas dos semanas, a la espera que la Cámara de Diputados debata la ley de "Emergencia Covid" que ayer obtuvo dictamen favorable en un plenario de comisiones.

Este proyecto establece un marco normativo sobre parámetros epidemiológicos y sanitarios para mitigar el impacto de la segunda ola de coronavirus.

El objetivo del gobierno nacional era obtener este jueves la sanción de la norma, ya aprobada por el Senado, antes de que venza este viernes el DNU vigente con restricciones, pero ante la falta de acuerdo con la oposición, la propuesta deberá ser tratada en una nueva sesión dentro de dos semanas.

El DNU que vence este viernes estableció nueve días de aislamiento estricto (entre el 22 y el 30 de mayo); luego cinco días con mayores habilitaciones (del 31 de mayo al 4 de junio); el pasado fin de semana (5 y 6), nuevamente con restricciones más duras, y los últimos días de esta etapa (del 7 al 11 de junio) otra vez con medidas más laxas.

El parámetro sanitario que rige actualmente, incluido en el proyecto de ley que se trata en el Congreso, establece la división del país en cuatro categorías, de acuerdo con los riesgos: bajo, mediano, alto y alarma epidemiológica y sanitaria.