---
category: Agenda Ciudadana
date: 2020-12-15T12:45:06Z
thumbnail: https://assets.3dnoticias.com.ar/neocullen.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Diario UNO'
resumen: Un premio nacional para profesionales del Hospital Cullen
title: Un premio nacional para profesionales del Hospital Cullen
entradilla: Se trata del "Renac de Oro" que otorga la Red Nacional de Anomalías Congénitas,
  en este acaso a la pediatra Laura Peralta y su equipo, por la identificación precoz
  de estas patologías.

---
El Ministerio de Salud de la provincia destacó que el servicio de Neonatología del Hospital Cullen de la ciudad de Santa Fe recibió el “Premio Renac de Oro 2020”, que otorga la Red Nacional de Anomalías Congénitas (Renac-Ar), dependiente del Ministerio de Salud de la Nación.

Particularmente, **la distinción se realiza a la pediatra Laura Peralta**, referente de la Renac en el área, pero ella antes que nada aclara “que se trata de un premio a todo el equipo de médicos de planta, de guardia, enfermeros, en síntesis, a todos los trabajadores de Neonatología”.

**La Renac tiene por objetivo favorecer la atención de niños con anomalías congénitas**, fortaleciendo el trabajo de los equipos de salud de todo el país y promoviendo la atención integral de chicos afectados y de sus familias. “Tiene referentes en las maternidades y neonatologías en donde se realizan al menos 1.000 partos anuales. En el Hospital Cullen realizamos de 2.000 a 3.000 anuales”, explicó Laura Peralta.

**La profesional es santafesina, estudió en la Universidad de Córdoba e hizo su residencia de Pediatría en el Hospital de Niños, Orlando Alassia de Santa Fe**. En el 2003 fue incorporada en el Cullen mediante el plan nacional Promin, –que tenía como objetivos disminuir la morbimortalidad y la desnutrición materna e infantil y promover el desarrollo psicosocial de los menores de seis años–, como segunda médica para acompañar a los médicos que realizaban los partos. Más tarde fue incorporada como profesional de planta.

“La Renac se creó originariamente como un registro centralizado en el hospital Rivadavia de Buenos Aires, donde funciona el Centro de Genética Nacional, y luego se constituyó en una red incorporando varios profesionales que no solamente llevan registro de estos pacientes, también buscan brindar soluciones según cada caso”, dijo Peralta.

Estas anomalías congénitas –explicó la pediatra– afectan de tres a cinco nacidos vivos. Pueden ser funcionales o estructurales y no siempre se manifiestan al nacer sino en el transcurso de la vida.

“Cuando se trata de anomalías de importancia generalmente provocan patologías o afecciones que hay que solucionar porque podría ocasionar un problema de por vida en ese paciente o bien la muerte. Entonces es muy importante el registro y la detección prenatal o neonatal inmediata de las mismas”, enfatizó Laura Peralta, quien realizó diversas capacitaciones y cursos superiores sobre genética clínica, por lo que fue convocada a trabajar como referente de la Renac”.

Las anomalías funcionales, como por ejemplo la sordera congénita, o las anomalías del metabolismo, no son competencia de los referentes de la Renac, sino que son abordadas por otras áreas del sistema de salud.

# **Más accesibilidad**

A su vez, la premiada pediatra consideró que la importancia del trabajo que realizan desde el Cullen mediante la Renac radica en que “es un acto de salud pública, que impacta en la población y amplía la accesibilidad a los diagnósticos y tratamientos”.

“Según la complejidad de los casos –dijo Laura Peralta– hacemos consultas con genetistas de renombre, compartimos información, asesoramos sobre tratamientos, como asimismo hacemos extensivo el estudio a los padres o madres para que puedan decidir tener más hijos o no, teniendo en cuenta el componente genético”.

Además, la Renac puede estudiar o advertir el vínculo entre anomalías prevalentes y su posible vínculo con variables socio ambientales, exposición a tóxicos, estado nutricional, determinantes sociales de la salud, entre otros.

Finalmente, Laura Peralta expresó su alegría y satisfacción por el premio recibido en tanto es un estímulo al esfuerzo y dedicación cotidianos del equipo de trabajo en el Cullen, y es reconocido como tal. "Consideramos que la detección pre o neonatal, el registro, y la eventual prevención, contención y tratamiento de las enfermedades congénitas deben hacerse cada vez más accesibles y universales, y esto nos da mucho entusiasmo”, concluyó la pediatra.