---
category: Estado Real
date: 2021-08-28T06:00:36-03:00
thumbnail: https://assets.3dnoticias.com.ar/ASOCIATIVISMO.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La provincia apuntala la producción y el trabajo desde el asociativismo
title: La provincia apuntala la producción y el trabajo desde el asociativismo
entradilla: En esta ocasión adjudicaron 20 nuevas matrículas a cooperativas y mutuales
  en el sur santafesino.

---
La Secretaría de Desarrollo Territorial y Arraigo, del Ministerio de Producción, Ciencia y Tecnología, entregó este viernes 20 nuevas matrículas, 19 de ellas a cooperativas, y la otra acreditación fue destinada a una mutual. En esta oportunidad, los rubros alcanzados fueron referidos al cuidado de adultos mayores, reciclado, gastronomía, turismo, construcción, producción y distribución de productos artesanales y de tareas agropecuarias.

Del acto en el que se entregaron las matrículas participaron el secretario de Desarrollo Territorial y Arraigo, Fabricio Medina, el senador Provincial Marcelo Lewandowski, los directores provinciales de Economía Social, Agricultura Familiar y Emprendedurismo, Guillermo Tavernier; de Desarrollo Territorial del Ministerio de Desarrollo Social; Camilo Scaglia; y de Capacitación y Formación Laboral del Ministerio de Trabajo, Empleo y Seguridad Social, Valeria March.

En la actividad Medina indicó: “Hoy estamos aquí reunidos muchos actores que perseguimos un mismo fin, y este es el sostenimiento del sistema cooperativo a través del trabajo de los diferentes valores. La organización y la comunidad organizada es lo que nos va a permitir autogestionarnos y generar puestos de trabajo. Hoy entregamos matrículas a cooperativas muy diversas, cada una de ellas con su objeto social y sus características propias, pero todas con un mismo objetivo, el de crear un nuevo puesto de trabajo”.

Seguidamente agregó: “Desde el Ministerio vamos a estar acompañando el proceso, hoy termina una etapa muy importante para ustedes, pero inicia otra con un gran desafío por delante que es sostener esa matrícula en el tiempo”.

A su turno, Guillermo Tavernier afirmó: "Es un día muy importante para la producción y el trabajo, ver cómo se crean más 20 nuevas cooperativas y mutuales y que, además, se suman a nuestro ecosistema productivo provincial, nos enorgullece e incentiva a seguir trabajando por este camino”.

“Es muy importante que vean el cooperativismo y mutualismo como un lugar de desarrollo. La pandemia ha puesto en evidencia que los modelos asociativos son un gran instrumento de reactivación, desarrollo, producción y trabajo. También es importante ver al Estado provincial como un aliado estratégico para diagramar y proyectar juntos una agenda de trabajo que nos integre, potencie y siga generando puestos de trabajo en cada localidad”, finalizó el funcionario provincial.

Siguiendo esta misma línea, Valeria March señaló: “Quiero felicitarlos por el esfuerzo y empeño que pusieron para llegar hasta acá, sé que es un proceso largo, y para que estemos hoy acá, hay un gran trabajo de fondo y articulado, lo cual se ve reflejado en el labor cotidiano de cada cooperativa y mutual de nuestra provincia. También quiero resaltar que cuentan con el apoyo de nuestro Ministerio en lo que respecta a capacitaciones, nuestra decisión es acompañarlos cuando sea necesario”.

Para concluir el senador Lewandowski resaltó la importancia del sector en el territorio santafesino: "Todos estos funcionarios aquí presentes no son burócratas que trabajan solo detrás de un escritorio, ellos salen a la calle y están en contacto con la gente. Valoro el empeño y sacrificio que le ponen a su labor, sin dudas son parte de una familia cooperativa y mutualista muy grande e importante que genera, con su trabajo diario, arraigo en nuestro territorio”.

Y manifestó: “Ustedes acá representan muchos puestos de trabajo nuevos, representan una herramienta que nos va a dar la posibilidad de encontrar una salida porque, sin dudas alguna, el trabajo es lo que dignifica. Organizados, solidarios y unidos es como vamos a salir adelante”.