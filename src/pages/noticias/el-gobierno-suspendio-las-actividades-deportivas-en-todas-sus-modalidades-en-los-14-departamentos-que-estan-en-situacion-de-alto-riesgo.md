---
category: Estado Real
date: 2021-05-09T06:00:06-03:00
thumbnail: https://assets.3dnoticias.com.ar/GIMNASIOS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: El Gobierno suspendió las actividades deportivas en todas sus modalidades
  en los 14 departamentos que están en situación de alto riesgo
title: El Gobierno suspendió las actividades deportivas en todas sus modalidades en
  los 14 departamentos que están en situación de alto riesgo
entradilla: A través de una resolución, se precisaron los alcances del Decreto Nº
  0458 en relación con la modalidad entrenamiento de los deportistas.

---
Quedaron suspendidas todas las prácticas deportivas tanto al aire libre como en instalaciones cerradas de clubes, gimnasios y establecimientos afines.

A partir de la Resolución Nº164, el gobierno de la provincia especificó los alcances del Decreto Nº 0458/21, cuyos objetivos centrales fueron presentados en la mañana de este sábado por el gobernador Omar Perotti y la ministra de Salud, Sonia Martorano. En relación al mencionado decreto, y desde la resolución aclaratoria, el gobierno provincial hizo foco en el artículo 3º donde se suspenden las distintas actividades deportivas desde el 08 hasta el 21 de mayo de 2021 inclusive.

En virtud de ello, la Resolución Nº164 en su artículo 1º, establece que queda alcanzada por la suspensión de actividades vigente, aquella conocida como “modalidad entrenamiento” y se especifica claramente, que se trata de aquella que “efectúan entre sí los deportistas de una entidad, en sus instalaciones o al aire libre, sin interactuar con los de otras instituciones”. Más aún, para los 14 departamentos de la provincia que se encuentran en situación de alto riesgo, quedan suspendidas también todas aquellas actividades “que se efectúen con carácter recreativo y en grupos de entrenamiento”, ya sea “en espacios cerrados o al aire libre”.

Esto significa que la actividad de los clubes, tanto en ámbitos cerrados como abiertos quedaron suspendidas en los 14 departamentos que hoy figuran en rojo a nivel sanitario provincia, aquellos que evidencian una situación epidemiológica de alto riesgo. En este grupo, los departamentos incluidos son: La Capital, Las Colonias, San Jerónimo, Castellanos, San Justo, San Martín, San Cristóbal, Belgrano, Constitución, Caseros, General López, Iriondo, Rosario y San Lorenzo.

Cabe consignar que, en horas de la mañana, se había informado que habían quedado suspendidas, además del funcionamiento de los clubes, la práctica de Fútbol 5, la actividad en gimnasios y en otros establecimientos afines.

Esta situación que tiene un rango de alcance hasta el próximo 21 de mayo, podría ser revisada en función de una notoria y sostenida disminución de casos, situación que no es la que se vivencia por estos momentos y que impulsa la normativa vigente.