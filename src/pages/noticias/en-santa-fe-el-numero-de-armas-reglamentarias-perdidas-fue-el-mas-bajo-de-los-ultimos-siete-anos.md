---
category: Agenda Ciudadana
date: 2020-12-21T13:15:04Z
thumbnail: https://assets.3dnoticias.com.ar/2112armas.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: En Santa Fe, el número de armas reglamentarias perdidas fue el más bajo de
  los últimos siete años
title: En Santa Fe, el número de armas reglamentarias perdidas fue el más bajo de
  los últimos siete años
entradilla: Entre enero y noviembre de 2020, 36 armas policiales fueron denunciadas
  como sustraídas o extraviadas por personal policial de la provincia.

---
Durante el 2020, la Agencia Provincial de Prevención de la Violencia con Armas de Fuego (APVAF), creada con el objeto de reducir la circulación de armas ilegales, y la Agencia de Control Policial (ACP), realizó rigurosas auditorías e inspecciones en las armerías centrales de la Policía de la provincia,  que arrojaron como resultado el extravío de 36 armas policiales entre enero y noviembre de 2020.

Entre 2014 y 2019 fueron sustraídas (o extraviadas) 502 armas entre cortas y largas, es decir, aproximadamente cada cuatro días un o una policía de la provincia perdía o le robaban el arma de fuego de servicio. En dicho número, están comprendidos los faltantes en los arsenales policiales, donde se almacenan pistolas, escopetas, municiones, chalecos y demás equipamiento.

Desde el año 2015, cuando se llegó a 121 armas cortas y largas sustraídas o extraviadas, la curva fue menguando progresivamente. En comparación, la filtración de armas institucionales en el año actual sufrió una disminución significativa con relación a los años anteriores: un número tres veces menor que en 2015, y aproximadamente 30% menos que 2019.

**En síntesis, se denunciaron como perdidas o sustraídas a personal policial 91 en 2014, 121 en 2015, 102 en 2016, 74 en 2017, 63 en 2018, 51 en 2019 y 36 entre los meses de enero y noviembre de 2020**.

La indagación sobre los robos de armas oficiales se realiza según el procedimiento fijado en el «Reglamento para sumarios administrativos de la Policía de la Provincia de Santa Fe», aprobado en 1977 mediante el Decreto N° 4.055.

Cuando un trabajador o trabajadora policial pierde o le sustraen su arma, la Oficina de la División Judiciales de la Unidad Regional con jurisdicción (es decir, en la cual presta funciones) inicia investigaciones y la Oficina de Sumarios abre una carpeta administrativa para indagar el hecho. Al procedimiento lo lleva adelante personal policial de la respectiva dependencia.

En pos de generar mecanismos de control externos, la Agencia Provincial de Prevención de la Violencia con Armas de Fuego está diagramando un sistema de seguimiento de las armas policiales que se denuncian como perdidas o sustraídas.

De este modo, se busca vincular los sumarios administrativos con las consecuentes investigaciones fiscales de los hechos. Además, todas las armas policiales están registradas según su correspondiente huella balística en el sistema de identificación estatal. Esto constituye una información relevante para realizar la trazabilidad de las mismas según las causas judiciales o allanamiento donde éstas aparezcan.

En el mes de enero, la actual gestión del Ministerio de Seguridad realizó una auditoría en el arsenal policial de la Unidad Regional II y constató el faltante de cinco pistolas reglamentarias 9 mm, que correspondían a personal con carpeta médica. En dicho momento, se procedió a remover a las autoridades responsables y se ordenó abrir los correspondientes sumarios administrativos. Actualmente el hecho está siendo investigado por el Ministerio Público de la Acusación.