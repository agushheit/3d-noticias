---
category: La Ciudad
date: 2021-08-19T06:15:54-03:00
thumbnail: https://assets.3dnoticias.com.ar/PAROJUEVES.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Salvo en Santa Fe, los municipales de toda la provincia van al paro este
  jueves
title: Salvo en Santa Fe, los municipales de toda la provincia van al paro este jueves
entradilla: El paro es convocado por Festram y en reclamo de una recomposición salarial.
  Además de esta capital, tampoco se sumarán los trabajadores representados por Asoem
  en Recreo, Rincón, Monte Vera y Arroyo Leyes.

---
La Federación Santafesina de Trabajadores Municipales (Festram) ratificó el paro de 24 horas para este jueves -otro por 48 horas previsto para el 24 y 25 del mismo mes está supeditado a la posibilidad de que el gobierno provincial dé alguna respuesta positiva a los reclamos de los trabajadores-, tras fracasar hoy la reunión paritaria que la organización gremial mantuvo con autoridades del gobierno provincial.

Desde el sindicato señalaron que la medida de fuerza es "ante la falta de respuesta de los representantes de Intendentes y Presidentes de Comunas en la paritaria convocada por el Gobierno Provincial de modificar la política salarial cuyos porcentajes han quedado debajo de los índices inflacionarios".

Las medidas de fuerza no impactarán en la ciudad de Santa Fe, tampoco en Recreo Rincón Monte Vera y Arroyo Leyes. Desde Asoem, sindicato que representa a los trabajadores de las administraciones mencionadas, dijeron a UNO Santa Fe que el gremio no participa de la paritaria provincial, sino que lo hacen a "a través de las mesas paritarias locales".

En tanto, desde la Festram indicaron que "el objetivo es superar el proceso inflacionario tal como impulsa el Gobierno Nacional y como, también, ya lo otorgaron las Provincias de Córdoba y Buenos Aires. Ambas han anticipado las cuotas de lo pactado y a su vez adelantaron la reapertura de la paritaria".

En ese orden, desde la Federación destacaron: "El último índice inflacionario publicado por el INDEC, refleja una inflación interanual del 51, 8%, mientras los salarios del sector municipal se incrementaron en un 26% en el mismo período, lo que refleja que la variable de ajuste en las cuentas fiscales es el salario de los trabajadores".

“El reclamo por incremento salarial, no es una situación que queramos imponer, ya que en otras Provincias, como Córdoba y Buenos Aires, reabrieron la paritaria y mejoraron notablemente el poder adquisitivo de sus empleados” expresó Claudio Leoni.

Por su parte, fuentes del gremio Asoem señalaron con respecto a la continuidad de las negociaciones salariales "que el cuerpo de delegados definió percibir primero el aumento que resta y luego discutir un nuevo incremento"