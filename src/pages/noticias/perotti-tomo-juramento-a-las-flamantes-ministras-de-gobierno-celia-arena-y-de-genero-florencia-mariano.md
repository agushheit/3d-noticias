---
category: Estado Real
date: 2021-12-24T06:15:03-03:00
thumbnail: https://assets.3dnoticias.com.ar/asuncion.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Perotti tomó juramento a las flamantes Ministras de Gobierno, Celia Arena;
  y de género, Florencia Mariano
title: Perotti tomó juramento a las flamantes Ministras de Gobierno, Celia Arena;
  y de género, Florencia Mariano
entradilla: Fue durante un acto en el Salón Blanco de la Casa de Gobierno, en Santa
  Fe. “Confiamos plenamente en sus tareas”, afirmó el gobernador.

---
El gobernador Omar Perotti tomó juramento este jueves en la Casa de Gobierno, a las nuevas ministras de Gobierno, Justicia y Derechos Humanos de la provincia, Celia Arena; y de Igualdad, Género y Diversidad, Florencia Marinaro. El acto se desarrolló en el Salón Blanco.

Cabe señalar que Arena se venía desempeñando al frente del Ministerio de Igualdad y, en su lugar, asumió Marinaro; mientras que Roberto Sukerman ocupaba la cartera de Gobierno.

En ese marco, el gobernador agradeció al ministro saliente “por el tiempo compartido en momentos difíciles, inolvidables, de pandemia, que marcaron y nos marcaron en la gestión”. También, Perotti deseó “seguir teniendo los mejores vínculos y la mejor relación en todo el esquema futuro” de su gabinete provincial.

A continuación, el mandatario hizo lo propio con la flamante titular de dicha área tanto por “la tarea desempañada en Igualdad como por el compromiso futuro”, y puntualizó que “Celia va a ser la primera mujer ministra de Gobierno de la provincia: confiamos plenamente en su tarea, en su mirada y en el deseo de que ello contribuya a mejorar el accionar y el funcionamiento del área y su vinculación con toda la comunidad”, dijo Perotti.

En tanto, a la nueva ministra de Igualdad el gobernador le deseó “lo mejor en este desafío. Venís con la experiencia de ser parte del equipo de trabajo (ya que Marinaro se venía desempeñando como viceministra del área) y, con toda la fuerza y tu propia impronta, sabrás llevarlo de la mejor manera”, sostuvo.

Finalmente, Perotti agradeció a “los familiares y amigos porque son los sostenedores en los momentos difíciles y duros que la función exige. Los vamos a necesitar y ellos a ustedes porque si están cerca, ellas van a estar bien y al ser así, van a estar poniendo lo mejor todos los días; y eso, es lo que la comunidad sabrá agradecer”, concluyó el gobernador.

**LAS NUEVAS MINISTRAS**  
Al finalizar el acto, Arena habló de dar continuidad “a las cuestiones vinculadas al crecimiento y la consolidación de nuestra provincia. Eso requiere de mucho diálogo, de un diálogo responsable con la oposición, con el resto de los poderes, y en ese camino vamos a seguir fortaleciendo todo eso y el vínculo con las instituciones, con cada actor social y político”, definió.

En ese sentido, la flamante titular de la cartera de Gobierno destacó “el trabajo esencial, como es una decisión de nuestro gobernador desde el inicio de la gestión, con los gobiernos locales, utilizando todas las herramientas que tengan que ver con dotarlos de mayor autonomía en sus decisiones, y que los ciudadanos sientan que son protagonistas de los cambios que cada localidad necesita, para eso vamos a trabajar en conjunto con el resto de los ministerios, especialmente con el de Gestión Pública”.

Por otro lado, Arena indicó que en el área “seguramente habrá algunos cambios que tienen que ver con el equipo que me acompaña desde siempre, pero yo tomo la posta de un equipo que viene trabajando duro desde el inicio de la gestión y viene teniendo resultados, seguramente vamos a sumar gente, pero también vamos a trabajar con quienes ya lo vienen haciendo desde el inicio”.

En tanto, Marinaro expresó que le dará continuidad a “la gestión de Celia Arena. Mi reconocimiento a todo un equipo que desde 2019 jerarquizó las áreas de género y diversidad sexual, con una decisión política del gobernador que hoy me da una responsabilidad enorme para continuar con un ministerio que está transformando las estructuras desiguales que tiene nuestra sociedad”.

Con todo, el ministro de Gobierno saliente, Roberto Sukerman, declaró que “hoy es un día muy importante porque más allá del agradecimiento al gobernador por haber formado parte de dos ministerios muy importantes: primero en Trabajo, y después en Gobierno, estoy muy contento por Celia (Arena), por Florencia (Marinaro) y por el gobierno”, dijo. Y destacó el hecho de que “por primera vez una mujer sea ministra de Gobierno de la provincia” lo cual “es simbólicamente muy importante”.

**PRESENTES**  
Estuvieron presentes, además, los ministros de Gestión Pública, Marcos Corach; de Infraestructura, Servicios Públicos y Hábitat, Silvina Frana; y de Desarrollo Social, Danilo Capitani; así como el fiscal de Estado, Rubén Weder, y familiares de las flamantes ministras.