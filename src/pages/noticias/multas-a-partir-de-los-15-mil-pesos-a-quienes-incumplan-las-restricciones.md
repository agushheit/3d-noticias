---
category: La Ciudad
date: 2021-01-16T04:01:04Z
thumbnail: https://assets.3dnoticias.com.ar/JorgeNessier.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: '3DNoticias - Fuente: El Litoral'
resumen: Multas a partir de los 15 mil pesos a quienes incumplan las restricciones
title: Multas a partir de los 15 mil pesos a quienes incumplan las restricciones
entradilla: El fiscal Jorge Nessier realizó un balance parcial de lo actuado desde
  el 11 de enero cuando comenzaron a regir las limitaciones para la circulación nocturna.

---
Como es sabido desde el fin de semana pasado el gobierno de la provincia de Santa Fe, a través del Decreto N° 6 de 2021, adhirió al Decreto N° 4 del Gobierno Nacional, estableciéndose **restricciones a la circulación vehicular nocturna, en el período comprendido de las 00.30 a las 06.00 de domingo a jueves, y de 01.30 a 06.00 los viernes, sábados y vísperas de feriado**.

Desde el lunes pasado (11 de enero) entraron en vigencia estas restricciones, y aquí en la ciudad de Santa Fe acordaron diversos estamentos oficiales (representantes del Ministerio Público de la Acusación, del Ministerio de Seguridad, policía de la provincia, municipalidad y del comando conjunto de las Fuerzas Federales que están asentadas en nuestra localidad) medidas a adoptar a partir de estas normas.

El fiscal Jorge Nessier, brindó detalles a la prensa tras los primeros días de implementación de las normas. «Lo que entró en vigencia el lunes tiene que ver con limitaciones a la circulación vehicular. También para aquellos que se encuentren en la vía pública, con excepción de quienes estén desplazándose por la realización de una actividad esencial o habilitada, como por ejemplo la gastronómica. Todo el resto de las actividades, continúan desarrollándose tal como venía sucediendo».

Según explicó Nessier se realizaron procedimientos desde el mismo lunes. «Esa noche llovió y eso contribuyó a que fuera escasa la circulación. En las noches posteriores, sí se hicieron otras actuaciones en las que se produjo el secuestro de vehículos, tanto automotores como motos y el traslado de las personas hacia las dependencias policiales, a los fines de realizar la identificación de las mismas y la averiguación de antecedentes».

«La violación de este precepto penal que es el artículo 205, registra una pena que no es particularmente grave: seis meses a dos años de prisión. Sin perjuicio de lo cual, si se trata de una persona con antecedentes penales o que se encontrara, por ejemplo, en libertad condicional, en estos casos el fiscal podría disponer la detención. Si así no se diera, y de acuerdo siempre a la gravedad del hecho y características del mismo, los fiscales disponen la inmediata libertad de la persona. En tanto el vehículo permanece secuestrado hasta que se verifique algún tipo de resolución de la causa», dijo Nessier en relación con las penas.

<br/>

## **Multas**

Una de las principales consultas al fiscal estuvo en torno a las multas que perciben los infractores. «No es exactamente una multa. En estos casos cuando la persona infractora no cuente con antecedentes penales, la causa puede resolverse anticipadamente a través de algunos de los mecanismos previstos por nuestra ley procesal para ello, como por ejemplo la suspensión de juicio a prueba (_Probation_), que no es una circunstancia menor. O sea, no es una condena, pero queda un antecedente al menos para establecer una serie de cumplimientos de obligaciones en un período que normalmente es de un año, y la imposibilidad de volver a hacer uso de ese beneficio durante un período determinado de tiempo».

Otra alternativa en caso de menor entidad en cuanto a la gravedad del hecho, es que puede existir por parte de los fiscales la decisión de aplicar un criterio de oportunidades. Es decir, no promover la acción penal y directamente resolverlo. Siempre en estos casos se da a través de un resarcimiento económico, que va derecho a la víctima cuando esta está individualizada. Pero, como en este tipo de hechos las víctimas son los integrantes de la comunidad, el resarcimiento va dirigido a la donación de un monto de dinero a una entidad de bien público.

«Los montos son variables, tienen que ver con diversas circunstancias que los fiscales analizan en cada caso. No es algo que se está aplicando a partir de esta semana, sino que antes también en reiteradas oportunidades. Hubo fiestas que no estaban habilitadas donde se procedió en consecuencia con multas de hasta 100 pesos o más. En casos de una circulación nocturna sin mayores consecuencias, son cantidades chicas, pero que nunca serán menos de 15 o 20 mil pesos. Esto es algo estimativo, porque cada fiscal analiza cada caso», explicó Nessier.

Sobre el final, el fiscal regional manifestó que «ya se cerraron algunas causas porque hubo acuerdo de resolución anticipada y no punitiva de la misma. Por ende, se procedería en lo inmediato a la restitución del vehículo (podría ser en los primeros días de la semana que viene). A su vez el implicado realizaría un aporte dinerario a una entidad de bien público».

<br/>

## **Importante**

A quien se le retenga el vehículo, no lo podrá recuperar inmediatamente, porque no se trata de una multa, sino que es una causa penal que se da inicio y que a medida que se avanza en la misma y se logra un acuerdo entre la persona imputada, la fiscalía y eventualmente su defensor es que podrá, como parte de ese acuerdo, recuperar el vehículo, que es retenido porque es considerado en el momento del procedimiento, como un instrumento del delito que se está llevando a cabo. Además, todas las personas que están en un vehículo, son igualmente responsables que el conductor. Es lo mismo que en una fiesta: todos los que asisten son responsables, no solo quien la organice.