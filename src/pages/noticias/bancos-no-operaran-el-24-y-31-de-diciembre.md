---
category: Agenda Ciudadana
date: 2021-12-17T06:00:38-03:00
thumbnail: https://assets.3dnoticias.com.ar/BANCOCENTRAL.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: Bancos no operarán el 24 y 31 de diciembre
title: Bancos no operarán el 24 y 31 de diciembre
entradilla: "Como cada año, el BCRA invitó a las entidades financieras a dar asueto
  ambos días. \n"

---
El Banco Central invitó a las entidades financieras y cambiarias de todo el país a otorgar asueto a su personal el 24 y 31 de diciembre próximos. A través de la Comunicación P 51020, las entidades quedan facultadas a no abrir sus sucursales en esos días. Los canales electrónicos, tales como home banking, banca móvil y cajeros automáticos, funcionarán como lo hacen habitualmente los días feriados.

Mediante los canales electrónicos, se pueden realizar diversas operaciones, como los pagos con transferencias que son accesibles, eficientes y seguros y evitan el uso del efectivo. Con un teléfono celular, con cualquier billetera virtual o bancaria, las personas usuarias pueden leer cualquier código QR y realizar pagos con transferencia. Otras opciones de pago digitales son las tarjetas de débito y de crédito.

El BCRA, al mismo tiempo, instrumentó todas las medidas pertinentes para garantizar la distribución de billetes entre las entidades financieras para que éstas puedan recargar los cajeros automáticos con efectivo suficiente y atender la demanda durante estos días.