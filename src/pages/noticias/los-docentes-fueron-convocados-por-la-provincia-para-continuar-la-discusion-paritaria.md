---
category: Agenda Ciudadana
date: 2021-03-07T05:00:03-03:00
thumbnail: https://assets.3dnoticias.com.ar/PARITARIAS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Los docentes fueron convocados por la provincia para continuar la discusión
  paritaria
title: Los docentes fueron convocados por la provincia para continuar la discusión
  paritaria
entradilla: La reunión está prevista para este lunes 8 de marzo a las 10 de manera
  remota. Por su parte los estatales y los trabajadores de la Salud aún no recibieron
  convocatoria oficial

---
Desde el Ministerio de Trabajo, Empleo y Seguridad Social se anunció que este lunes 8 de marzo a las 10, las autoridades provinciales se encontrarán –bajo la modalidad virtual– con representantes de los gremios docentes para continuar con las negociaciones paritarias. La convocatoria es a una semana del retorno a las clases presenciales prevista para el 15 de marzo.

El último encuentro fue el 24 de febrero y estuvo encabezado por el ministro Juan Manuel Pusineri. En ese momento había anticipado que en la próxima reunión "la provincia va a hacer la oferta salarial esperando también lo que suceda en la reunión de la paritaria nacional. A mi modo de ver, en la paritaria nacional, se va a trabajar en función de un aumento que se va a ubicar por encima del 30%, que sería la pauta inflacionaria nacional. Vamos a ajustarnos a ese criterio".

Por otra parte, la cartera laboral informó que continúa trabajando para concretar en los días subsiguientes las reuniones que se encuentran pendientes con sindicatos del sector de la salud y de los trabajadores estatales.