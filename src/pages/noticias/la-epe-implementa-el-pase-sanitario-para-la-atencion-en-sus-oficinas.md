---
category: Estado Real
date: 2021-12-21T06:00:12-03:00
thumbnail: https://assets.3dnoticias.com.ar/paseEPE.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La EPE implementa el Pase Sanitario para la atención en sus oficinas
title: La EPE implementa el Pase Sanitario para la atención en sus oficinas
entradilla: A partir del 21 de diciembre se deberá presentar el esquema de vacunación
  completo. Asimismo, reitera la importancia del uso de la oficina virtual para gestionar
  trámites.

---
La Empresa Provincial de la Energía (EPE) conforme a lo dispuesto por el gobierno de Santa Fe, a través del Decreto Provincial Nº 2915, adhirió a la Decisión Administrativa Nº 1198/21 de la Jefatura de Gabinete de Ministros de la Nación.

Dicha disposición establece la necesidad de que toda persona que haya cumplido los 13 años de edad y que asista a las actividades de mayor riesgo epidemiológico y sanitario deberá acreditar que posee un esquema de vacunación completo contra la COVID-19, aplicado al menos 14 días corridos antes de la asistencia a la actividad o evento, exhibiéndolo ante el requerimiento de personal público o privado designado para su constatación, y al momento previo de acceder a la entrada del evento o actividad. El pase sanitario va a regir a partir del día 21 de diciembre para las personas mayores de 13 años que concurran a las actividades que determina la norma nacional. En el caso de la provincia de Santa Fe se dispuso una extensión a otras actividades. La norma establece en el artículo 3, inciso e, la realización de trámites presenciales ante organismos públicos provinciales, motivo por el cual en las oficinas de la EPE se solicitará dicho pase.

**IMPLEMENTACIÓN**  
La acreditación de contar con el esquema de vacunación completo será a través de la aplicación denominada “Cuidar” – Sistema de prevención y cuidado ciudadano contra la COVID-19 – apartado “Información de Salud” en su versión para dispositivos móviles.

Asimismo, el carnet de vacunación está disponible en el apartado Mi Salud de la aplicación Mi Argentina. ([https://id.argentina.gob.ar/ingresar/](https://id.argentina.gob.ar/ingresar/ "https://id.argentina.gob.ar/ingresar/"))

Las personas que no pudieran acceder a la aplicación podrán acreditarlo con certificado de vacunación contra la COVID-19 en soporte papel y/o formato digital, acompañado con el Documento Nacional de Identidad, en el cual consten las dosis aplicadas y notificadas al Registro Federal de Vacunación Nominalizado (NOMIVAC).

**Oficina virtual**  
La EPE recuerda la vigencia de la oficina virtual para realizar gestiones sin moverse de su vivienda, a través de su página [www.epe.santafe.gov.ar/oficinavirtual](http://www.epe.santafe.gov.ar/oficinavirtual).

Los trámites virtuales que los usuarios podrán realizar son: adhesión a la factura digital, débito automático, liquidaciones no recibidas y consultas de planes de pago, entre otras.

La oficina virtual permite pagar las facturas a través de todas las plataformas habilitadas, verificar los pagos históricos, consultar los consumos de electricidad, actualizar datos y administrar distintos suministros.

Cada usuario del servicio eléctrico en todo el territorio provincial, podrá utilizar esta herramienta. El objetivo es brindar la posibilidad de una autogestión rápida, segura y sencilla desde cualquier dispositivo informático, independizando el lugar geográfico y horario en donde se encuentre.