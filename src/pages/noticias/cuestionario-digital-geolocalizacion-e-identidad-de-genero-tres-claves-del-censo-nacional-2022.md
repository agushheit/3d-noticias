---
category: Agenda Ciudadana
date: 2021-08-29T06:00:36-03:00
thumbnail: https://assets.3dnoticias.com.ar/CENSO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Cuestionario digital, geolocalización e identidad de género, tres claves
  del Censo Nacional 2022
title: Cuestionario digital, geolocalización e identidad de género, tres claves del
  Censo Nacional 2022
entradilla: Se desarrollará a través de un operativo mixto de relevamiento que combinará
  una etapa virtual con cuestionario digital desde la aplicación digital denominada
  e-Censo a partir del 16 de marzo de 2022.

---
El Censo Nacional de Población, Hogares y Viviendas se desarrollará a través de un operativo mixto de relevamiento que combinará una etapa virtual con cuestionario digital y una instancia presencial, con eje en el reconocimiento de la diversidad de la población argentina, por lo que por primera vez se incluirá una pregunta sobre la autopercepción de identidad de género, informaron autoridades del Instituto Nacional de Estadística y Censos (Indec).

Con una apuesta a la incorporación de nuevas tecnologías, se implementará la aplicación digital denominada e-Censo que le permitirá a la población que lo prefiera responder el cuestionario desde cualquier dispositivo a partir del 16 de marzo de 2022, en lugar de la entrevista cara a cara el día del Censo, el 18 de mayo.

"La pandemia aceleró la necesidad de implementar un cuestionario digital en línea, como complemento, para llevarle a la población tranquilidad en términos de seguridad sanitaria", dijo a Télam la directora Nacional de Estadísticas Sociales y de Población del Indec, Gladys Margarita Massé.

**Formulario digital**

La doctora en Demografía detalló que el cambio también está en el auto empadronamiento, ya que la población completará por sí misma las preguntas.

El formulario deberá ser respondido por una persona en nombre de todos los habitantes de la vivienda, por lo que se recomienda "congregar a todo el conjunto familiar" al momento de completar los datos para que se cumpla el objetivo de "contar, sin omitir ni duplicar, a todas y a cada una de las personas", explicó.

Sobre este punto, el director del Sistema Estadístico Nacional (SEN) del Indec, Pablo Ceballos, remarcó en diálogo con Télam que como el cuestionario es "autoadministrado se requiere de un entusiasmo y un compromiso de la ciudadanía porque tiene 56 preguntas".

**Operativo presencial**

El miércoles 18 de mayo de 2022, que será feriado nacional, se hará el tradicional operativo presencial de barrido en todo el territorio nacional y, en caso de haber completado el cuestionario digital, los censistas requerirán el código alfanumérico del comprobante del e-Censo y lo anotarán en el formulario papel porque es "la forma en que tenemos de ligar esa vivienda con la ubicación geográfica", indicó la funcionaria.

"Con esa información los datos se pueden llevar a un sistema geográfico y geoestadístico" que permita georreferenciar cada vivienda en un lugar determinado, resaltó.

**¿Cuál es la importancia de los datos obtenidos en el censo?**

A once años de haberse realizado el último censo, en 2010, los indicadores más esperados son los que refieren a la cantidad de habitantes y a la estructura por sexo y edad de la población.

Según las proyecciones del Indec, se estima que hay 45 millones de habitantes, aunque el equipo de análisis demográfico está evaluando el impacto de la Covid-19.

"El coronavirus impacta no solamente en las defunciones; también se ha observado una disminución en la frecuencia de los nacimientos", precisó Massé.

Más allá de la importancia de estos indicadores, las autoridades del Indec afirmaron que el principal aporte es la distribución territorial, es decir, saber dónde viven las personas y cómo se modificaron las distintas localidades para hacer la planificación urbana.

El Censo permite "conocer dónde hay que construir nuevos hospitales porque se expandió la población. Lo mismo en término de escuelas", sostuvo Massé y comentó que el Indec recibe muchas consultas para saber dónde establecer las farmacias.

En tanto, Ceballos mencionó la importancia que tuvo este indicador para llevar adelante el Plan Estratégico de Vacunación contra el coronavirus y la distribución de dosis en todo el país.

**¿Qué nuevas preguntas se incluirán en el Censo Nacional 2022?**

Entre las principales novedades del próximo Censo, el cuestionario contará por primera vez con preguntas sobre identidad de género y de autorreconocimiento étnico (pueblos indígenas y afrodescendientes), que se sumarán a las consultas básicas sobre educación, migración, ocupación y cobertura de salud.

Respecto a estas nuevas variables, Massé afirmó que "todo censo tiene en cuenta el contexto y la relevancia de determinados fenómenos donde no hay datos y son importantes para la época".

"En estos momentos el dato respecto de la identidad de género aporta a las políticas públicas y a la educación; también permitirá contribuir al reconocimiento de una sociedad que no es estrictamente binaria", añadió la especialista.

El formulario incluirá además una pregunta sobre discapacidad que servirá para construir encuestas específicas a futuro que profundicen la información.

"El Censo tiene que dar la base para poder hacer el resto de las estadísticas, es como la base de un edificio", graficó el director del SEN.

Otra novedad es que se pedirá el documento nacional de identidad de los integrantes del hogar para construir un Registro Estadístico de la Población que permita relacionar los datos entre censos, encuestas y registros administrativos.

"Vamos a poder tener en la Argentina en el futuro estadísticas con más nivel de precisión y más periodicidad", apuntó Ceballos.

En tanto, Massé aclaró que no se van a difundir los resultados del censo con el DNI, ya que rige el secreto estadístico por la Ley 17.622 y el Indec tiene "un sistema que se denomina enmascaramiento y encriptación para que esos datos sean cuidados".

En relación a las innovaciones en el diseño metodológico, por primera vez se realizará un "censo de derecho", en el que las personas serán contabilizadas en el lugar que residen la mayor parte del tiempo durante la semana.

Junto a las variables de población, el Censo tiene por objetivo recabar información sobre la caracterización de las viviendas, los materiales de construcción y la composición de los hogares.

El concepto de hogar hace referencia al hecho de compartir los gastos de alimentación en una misma vivienda y permite conocer "los indicadores de hacinamiento" para luego generar políticas económicas, indicó Massé.

Entre los desafíos que enfrenta el Censo está la extensión del país, por lo que el Precenso de Viviendas es "una herramienta fundamental para saber cuántos censistas mandar a cada cuadra; por otro lado, cada provincia conoce su territorio y dónde va a tener que fortalecer el operativo porque es más complejo llegar", indicó Ceballos.

La última actualización del Precenso reveló que crecieron casi un 25% el número de viviendas, lo que significa una "diferencia muy importante".

El operativo contará con 600.000 censistas en todo el país que recibirán capacitación virtual y la coordinación dependerá de organismos gubernamentales, lo que equivale a "la dimensión de tres elecciones nacionales", ejemplificó el funcionario.

Se recorrerán casi 2 millones de kilómetros cuadrados y el relevamiento en zonas rurales y viviendas colectivas comenzará una semana antes del 18 de mayo.

Para registrar a personas en situación de calle se realizará un operativo independiente por la noche para el que se convocará a las ONG que trabajan con este colectivo.