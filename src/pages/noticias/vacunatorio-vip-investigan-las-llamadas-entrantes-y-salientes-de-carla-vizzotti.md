---
category: Agenda Ciudadana
date: 2021-11-25T06:15:40-03:00
thumbnail: https://assets.3dnoticias.com.ar/VIZZOTTI.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: 'Vacunatorio VIP: investigan las llamadas entrantes y salientes de Carla
  Vizzotti'
title: 'Vacunatorio VIP: investigan las llamadas entrantes y salientes de Carla Vizzotti'
entradilla: Es una medida de prueba pedida por el fiscal Eduardo Taiano. Consideró
  que "no se puede descartar" que haya intervenido en la distribución irregular de
  las vacunas contra el Covid-19.

---
La jueza federal Eugenia Capuchetti amplió una medida de prueba en la causa por el denominado “Vacunatorio VIP” que incluye a la ministra de Salud, Carla Vizzotti, a pedido del fiscal Eduardo Taiano. La medida consiste en la extensión de un entrecruzamiento telefónico entre funcionarios del Ministerio de Salud para determinar la eventual participación de Vizzotti en la entrega de vacunas para los allegados y periodistas que se vacunaron en la sede de la cartera sanitaria y en el Hospital Posadas.

El fiscal Taiano sostuvo que “no puede descartarse” la intervención de Vizzotti en el mecanismo de asignación irregular de las vacunas, por entonces escasas, a personas que no estaban en orden de prelación para recibirlas. Vizzotti estuvo involucrada desde el principio en la investigación porque sus padres, ambos médicos y mayores de 70 años de edad, recibieron las vacunas y aparecen en los listados bajo investigación de la jueza Capuchetti.

El entrecruzamiento de llamados involucra ya al ex ministro Ginés González García, y a los funcionarios Mauricio Monsalvo, subsecretario de gestión administrativa; Martín Sabignoso, secretario de equidad en salud, y Arnaldo Medina, secretario de calidad en Salud.

Vizzotti no estaba incluida inicialmente en esa lista pero el fiscal Taiano consideró que podía estar incursa en algunas de las hipótesis bajo investigación y pidió su incorporación a la medida de prueba. Capuchetti le pidió que justificara tal pedido y finalmente aceptó que los llamados a entrantes y salientes a los teléfonos fijos de la funcionaria en el Ministerio sean analizados también.