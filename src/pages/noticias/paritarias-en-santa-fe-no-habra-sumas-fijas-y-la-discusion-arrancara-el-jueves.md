---
category: Agenda Ciudadana
date: 2021-01-30T03:13:01.000+00:00
thumbnail: https://assets.3dnoticias.com.ar/Ministerio-de-Trabajo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'Paritarias en Santa Fe: no habrá sumas fijas y la discusión arrancará el
  jueves'
title: 'Paritarias en Santa Fe: no habrá sumas fijas y la discusión arrancará el jueves'
entradilla: '"La idea es que en términos porcentuales, el salario pueda estar este
  año por encima de la inflación". El ministro de Trabajo adelantó que aspiran a acuerdos
  "más a largo plazo".'

---
Con estatales el jueves y docentes el viernes, la provincia de Santa Fe dará inicio la semana que viene a la discusión paritaria para definir la política salarial de 2021. La fecha de reanudación del diálogo ya había sido consensuada con los gremios en la última negociación del año pasado. Y la ratificó a El Litoral, el flamante ministro de Trabajo, Juan Manuel Pusineri. "Abordaremos dos aspectos centrales: la presencialidad, sobre todo en virtud del inminente inicio de clases; y la recomposición salarial", adelantó.

Respecto de los futuros aumentos, el funcionario destacó que se aguarda como parámetro general lo que resulte de las paritarias a nivel nacional. En tal sentido y de acuerdo con lo trascendido en las últimas horas, la aspiración de la Casa Rosada es estipular un "techo" de aumento del orden del 30%, en consonancia con la inflación.

"A nivel nacional, se conversa sobre un acuerdo que tenga en cuenta precios y salarios (inflación más futuros aumentos en los ingresos de los trabajadores). Y esas van a ser referencias a tener en cuenta, pero Santa Fe tendrá pautas propias para su negociación", expresó.

### **Pautas "locales"**

En tal sentido, el funcionario ratificó que la propuesta de mejora salarial que formule el gobierno de Santa Fe no volverá a incluir sumas fijas y en negro. Esa había sido la manera elegida por el gobierno de Omar Perotti en su primer año de gestión para "recomponer" salarios, amparado en las licencias que le concedía la Ley de Emergencia en materia económica. "Ya no pensamos en un esquema de sumas fijas; eso ya no está en nuestra agenda – confirmó el ministro -. Los aumentos que se concedan serán en términos porcentuales, remunerativos y bonificables". Y acotó que tal como se comprometieron en la última discusión de 2020, comenzará a conversarse también, de qué manera esas sumas fijas otorgadas el año pasado se "blanquean" e incorporan al salario.

Obviamente, Pusineri no arriesgó números sobre lo que será la oferta salarial del gobierno, pero adelantó que se procurará una mejora que le permita recuperar el salario al trabajador, por encima de la inflación. "Siempre se ponen sobre la mesa el índice inflación y el nivel de recaudación de la provincia, que son las dos variables que históricamente se miran. La idea – enfatizó – es que el salario pueda estar este año porcentualmente por encima de la inflación, porque eso tiene que ver también con una cuestión macro. La economía argentina funciona mucho a través de su consumo interno".

Pusineri enunció como otra pauta propia para discutir salarios "la previsibilidad" tanto para los empleados como para el Estado. Y dijo que a diferencia del año pasado, intentarán cerrar acuerdos salariales más extendidos en el tiempo. "La idea es que no tengamos acuerdos de tres meses como hasta ahora, porque ello nos obligó a sostener discusiones con los gremios cada quince días. Si no se puede un acuerdo anual, al menos, de manera semestral", planteó.

Finalmente, descartó de plano la inclusión de la cláusula gatillo. "Ese esquema de actualización automática según la inflación no está más desde el año pasado, incluso, a nivel nacional; y no se va a volver a incluir", sentenció.

Acerca de las expectativas por un normal inicio de clases, sostuvo que la convocatoria a paritarias se hizo de manera temprana – 5 de febrero – para propiciar una negociación con el tiempo suficiente a fin de evitar el conflicto.

### **Contexto**

Respecto de la situación laboral que atraviesa la provincia tras el impacto de la pandemia, el ministro describió "un panorama más alentador, después de un año en el que tuvimos una fuerte restricción del gasto". Recordó que en el primer semestre de 2020 se registró "una fuerte caída del empleo", aunque reparó en que "ya en el tercer trimestre, que es el último medido por el INDEC, se evidenció una recuperación".

"Las estimaciones dan cuenta de que se perdieron en los primeros meses del año alrededor de 40 mil puestos de trabajo entre registrado y no registrado. Y según los últimos datos publicados, al menos la mitad de esos puestos se habrían recuperado", comentó. Según dijo, ello fue en la medida en la que se fueron habilitando los diferentes rubros de actividades. "Ya en diciembre, dos rubros como la industria y la construcción en Santa Fe estaban en mejores niveles que en pre pandemia", destacó. Pero a renglón seguido, admitió que la situación "es heterogénea", y que otros sectores como comercio, turismo y hotelería "siguen muy golpeados". "Hay una recuperación muy dispar. El sector industrial tuvo una recuperación más rápida, pero ello cuesta más en comercios y sectores más pequeños", ejemplificó.

En cuanto a la perspectiva para este año, Pusineri dijo que "si la situación sanitaria acompaña, el panorama será bueno. En tanto no vuelva a haber restricciones, todo tiene que tender a mejorar. Nuestra idea es no retroceder con ninguna de las actividades habilitadas", dijo. Y acotó que si bien la generación de empleo depende en buena medida de aspectos macroeconómico, el objetivo es "favorecer desde el Ministerio" todo ese proceso y promover la "articulación" con las empresas a través de la asistencia y formación al personal.

### **Asistencia**

Pusineri dijo que la provincia mantendrá la asistencia a algunos sectores afectados por la pandemia que aún no han podido retornar a la actividad o que lo hicieron pero tardíamente, como los transportistas escolares, salones de eventos y jardines maternales. Respecto de los ATP, recordó que se reconvertirán a través de los tradicionales "Repro". En tanto, contó que se espera la correspondiente autorización de Nación para la rehabilitación de las salas de cine.

### **Viene Trotta**

El próximo lunes, según pudo saber El Litoral, llegará a la provincia el ministro de Educación de la Nación, Nicolás Trotta. Si bien no han trascendido aún detalles de su agenda, el funcionario estaría unas horas en Santa Fe en el marco de una serie de recorridas que viene realizando en todo el país. La visita se producirá en vísperas del inicio del ciclo lectivo, que el gobierno de Omar Perotti pretende materializar desde el 15 de marzo, de manera semipresencial en todas las localidades del territorio.