---
category: Agenda Ciudadana
date: 2020-12-04T11:27:46Z
thumbnail: https://assets.3dnoticias.com.ar/PEROTTI.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: El Litoral'
resumen: El gobernador Omar Perotti tiene coronavirus
title: El gobernador Omar Perotti tiene coronavirus
entradilla: Lo confirmó el propio mandatario en su cuenta de Twitter. "Seguiré cumpliendo
  con el aislamiento y siempre atento a los consejos médicos, continuaré trabajando
  desde mi casa", expresó. 

---
###### **| Noticia de El Litoral (**[**www.ellitoral.com**](https://www.ellitoral.com/index.php/id_um/271439-el-gobernador-omar-perotti-tiene-coronavirus-me-encuentro-bien-manifesto-politica.html "El Litoral")**)** 

El gobernador Omar Perotti comunicó este jueves por la tarde que dio positivo al test de coronavirus.

"Me encuentro bien, seguiré cumpliendo con el aislamiento y siempre atento a los consejos médicos, continuaré trabajando desde mi casa. Les pido a todos y todas que se cuiden y cuiden a sus familias", manifestó en su cuenta de Twitter.

![](https://assets.3dnoticias.com.ar/tweet.jpg)

El mandatario se encontraba aislado en su domicilio desde hacía 10 días, luego de viajar a Río Negro junto al ministro de Seguridad provincial, **Marcelo Sain, quien luego dio positivo**.

Ante el inicio de leves síntomas el pasado domingo 29 de noviembre, se llevó adelante el hisopado correspondiente cuyo resultado confirmó la presencia del virus.

Este jueves al mediodía el gobernador santafesino participó, por videoconferencia, de una actividad encabezada por el presidente, Alberto Fernández, en la fábrica Corven de la ciudad santafesina de Venado Tuerto.

A su vez, Perotti iba a estar este viernes en Casa Rosada para participar de la firma del Consenso Fiscal, pero por esta situación no se hará presente en Buenos Aires.

**El caso de Perotti se le suma al de otros funcionarios provinciales que se contagiaron de coronavirus**, como la ministra de Salud, Sonia Martorano, el secretario de esa cartera, Jorge Prieto, el ministro de Economía, Walter Agosto, la secretaria de Estado de Género e Igualdad, Celia Arena, y la ministra de Infraestructura, Servicios Públicos y Hábitat, Silvina Frana.