---
category: Agenda Ciudadana
date: 2022-12-08T15:56:50-03:00
thumbnail: https://assets.3dnoticias.com.ar/lanzamiento_informe_caf_ii_-_1-12-2022.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: www.portalmovilidad.com
resumen: Prioridad al gas, menos renovables y otro golpe a la electromovilidad
title: Prioridad al gas, menos renovables y otro golpe a la electromovilidad
entradilla: El Gobierno nacional continúa dándole importancia a los hidrocarburos,
  dejando en segundo plano la transición hacia la movilidad eléctrica.

---
En efecto, los principales referentes del sector temen la falta de iniciativas en torno al transporte cero emisiones. Portal Movilidad reúne diferentes opiniones.

Durante el lanzamiento de un reporte del Banco de Desarrollo de América Latina (CAF) **Flavia Royón**, secretaria de Energía de la Nación, anticipa que el Gobierno está próximo a lanzar un **plan de transición energética nacional**.

Sin embargo, dadas las declaraciones de Royón, todo parecería indicar que la actual gestión **potenciará el gas natural como fuente prioritaria, dejando en segundo lugar a la electricidad y las fuentes limpias.**

Aunque la funcionaria reconoce que en Argentina se debe remplazar y limpiar el elevado consumo de combustibles líquidos en la matriz, declara: “**Primero por gas natural y luego con energías renovables**”.

“Entendemos que **este es el primer paso en la transición energética**. El país tiene mucho para ofrecer, puertas adentro y al mundo en esa materia. Argentina debe garantizar, como hoja de ruta, el autoabastecimiento y la sostenibilidad de su matriz energética a través del desarrollo de sus **cuencas gasíferas**”, agrega.

Vale mencionar que durante el 2022 la mayoría de los anuncios de la actual secretaria, así como los de **Darío Martínez**, su predecesor, se dirigen a **Vaca Muerta** y la construcción de **gasoductos**.

Asimismo, cabe recordar que en septiembre el presidente **Alberto Fernández** manifiesta: “**El gas es el vehículo hacia la transición energética** que transita el mundo y la sustentabilidad es necesaria para nuestro desarrollo en el mediano y largo plazo. Es una política de Estado, gobierne quien gobierne. No dejemos pasar esta oportunidad”.

Posteriormente, el ex ministro de Transporte de la Nación, **Alexis Guerrera**, comenta a **Portal Movilidad**: “La transición inmediata a los autos eléctricos tiene sus limitantes. **No se puede electrificar en el corto plazo la flota de transporte público**. **Hoy la disponibilidad energética es el gas**. Además, la infraestructura y los vehículos son más económicos”.

En este sentido, dado que la electromovilidad quedaría “postergada”, **el sector teme que el** [**Proyecto de Ley de Promoción de la Movilidad Sustentable**](https://portalmovilidad.com/?s=kulfas+se+fue&asl_active=1&p_asl_data=1&customset%5b%5d=post&asl_gen%5b%5d=excerpt&asl_gen%5b%5d=content&asl_gen%5b%5d=title&qtranslate_lang=0&filters_initial=1&filters_changed=0) **no se apruebe** y, en efecto, no se apliquen incentivos al transporte cero emisiones.

“No creo que la normativa se aborde el año que viene porque **el Gobierno nacional sigue apostando por los combustibles fósiles**. El lobby petrolero se hace sentir”, indica **Silvia Vázquez**, presidenta del Partido Verde.

Lo cierto es que la falta de esta ley imposibilitaría la concreción de proyectos en movilidad sustentable y fomentaría aquellos que implican el gas natural.

Por ejemplo, si bien el país cuenta con capacidad de producción y experiencia en la fabricación de autos, las grandes automotrices no se atreven a dar el paso porque a nivel nacional no existe una norma que promueva la fabricación de rodados cero emisiones.

“[Hace falta un marco para que sea posible la producción nacional](https://portalmovilidad.com/falta-de-estrategia-gobierno-a-paso-lento-en-produccion-de-vehiculos-electricos-en-argentina/) porque en Argentina no está del todo claro cómo generar incentivos, considerando que no hay consenso desde el Gobierno acerca de cómo abordar la transición. No hay decisiones plasmadas que demuestren que vayamos hacia esa tendencia”, reconoce **Maximiliano Scarlan**, consultor senior en temas de Sustentabilidad e Innovación en ABECEB, al medio.

Asimismo, la ausencia de una ley no solo afectaría al sector transporte, sino también a la **industria del litio**, mineral indispensable para la fabricación de las baterías y el [ingreso de divisas al país](https://portalmovilidad.com/perego-en-argentina-hay-que-abrir-el-cupo-y-permitir-que-ingresen-vehiculos-electricos/).

En efecto, sin demanda de vehículos eléctricos, no hay industria que obligue a la explotación del metal para las pilas.

“Cuando tengamos régimen de movilidad eléctrica en la región habrá una industria de autopartes donde [las baterías de litio serán clave](https://portalmovilidad.com/no-hay-dolares-pero-si-litio-morales-apura-a-gobierno-una-ley-para-promover-fabricacion-de-baterias-en-jujuy/)”, manifiesta Gerardo Morales, gobernador de la provincia de Jujuy, a Portal Movilidad.