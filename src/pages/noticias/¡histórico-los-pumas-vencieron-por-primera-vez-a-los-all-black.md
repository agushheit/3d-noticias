---
layout: Noticia con imagen
author: Por Clara Blanco
resumen: Los Pumas ¡Histórico!
category: Deportes
title: "¡Histórico!: Los Pumas vencieron por primera vez a los All Black"
entradilla: El seleccionado argentino de rugby rompió la racha y venció a Nueva
  Zelanda 25 a 15 (PT 16-3) por la 3ª fecha del torneo Tres Naciones que se está
  realizando en Australia.
date: 2020-11-14T18:01:34.861Z
thumbnail: https://assets.3dnoticias.com.ar/pumas.jpg
---
Este sábado Los Pumas, el seleccionado argentino de rugby, escribió un nuevo capítulo en la historia del deporte. Vencieron a los All Black de Nueva Zelanda, uno de los equipos más duros del rugby a nivel mundial, por primera vez en la historia por 25 a 15 (PT 16-3) en un partido correspondiente a la tercera fecha del torneo Tres Naciones que se desarrolla en Australia.

El cruce se dio en el Bankwest Stadium de Sidney y sorpresivamente todos los puntos del equipo argentino fueron marcados por Nicolás Sánchez. El media apertura anotó un try, seis penales y una conversión. Mientras que los All Black, tres veces campeones del mundo, marcaron un try, una conversión y un penal de Richie Mo'unga y un try de Sam Cane.

La victoria de los argentinos es tan trascendental, porque debieron pasar 30 partidos entre ambos seleccionados para que Los Pumas puedan lograr la primera victoria. El resto de los resultados fueron 28 derrotas y 1 empate lejano que se logró en noviembre de 1985.

Luego de esta hazaña, Argentina volverá a jugar el próximo sábado 21 ante Australia, por la cuarta fecha del certamen, acotado y transformado en Tres Naciones y no en Rugby Champiosnhip por la ausencia del campeón mundial Sudáfrica.