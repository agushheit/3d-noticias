---
category: Agenda Ciudadana
date: 2021-05-03T06:00:54-03:00
thumbnail: https://assets.3dnoticias.com.ar/PEROTTI.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'Omar Perotti: "Contamos con el compromiso de Nación de acercar a Santa Fe
  más respiradores"'
title: 'Omar Perotti: "Contamos con el compromiso de Nación de acercar a Santa Fe
  más respiradores"'
entradilla: 'En un mensaje grabado, el gobernador de Santa Fe Omar Perotti comunicó
  este domingo que la provincia adhiere al decreto nacional emitido por el gobierno
  nacional. '

---
Confirmó también, la suspensión de las clases presenciales en los departamentos Rosario y San Lorenzo por 7 días.

Además, Perotti comunicó que se envió una nota al Jefe de Gabinete nacional, Santiago Cafiero, a los fines de evaluar las particularidades de las diferentes zonas informadas como de “Alto riesgo epidemiológico” por el Ministerio de Salud. 

“Entendemos que hay que reanalizar esos indicadores en vista de las particularidades de nuestra provincia y de nuestro sistema sanitario”, afirmó.