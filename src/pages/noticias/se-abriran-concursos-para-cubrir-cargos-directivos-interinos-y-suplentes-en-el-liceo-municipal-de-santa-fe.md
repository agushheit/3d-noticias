---
category: La Ciudad
date: 2021-08-19T06:00:06-03:00
thumbnail: https://assets.3dnoticias.com.ar/LiceoMunicipalBelgrano.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ASOEM
resumen: Se abrirán concursos para cubrir cargos directivos interinos y suplentes
  en el Liceo Municipal de Santa Fe
title: Se abrirán concursos para cubrir cargos directivos interinos y suplentes en
  el Liceo Municipal de Santa Fe
entradilla: En el marco de la Mesa Paritaria Local, la ASOEM y la Municipalidad de
  Santa Fe llaman a Concurso para cubrir cargos interinos y suplentes de Director/a
  en el Liceo Municipal “Antonio Fuentes del Arco”.

---
El período de inscripción comenzará el martes 31 de agosto y se extenderá hasta el lunes 13 de septiembre en el Liceo Municipal “Antonio Fuentes del Arco” (Dique II – Puerto de Santa Fe), de lunes a viernes, de 14 a 18 horas.

Los aspirantes deberán cumplir los siguientes requisitos generales: ser argentino, nativo o naturalizado y poseer aptitud psicofísica para la función a la cual aspira a ingresar. Además, deberán ser mayor de 18 años, no excediendo el límite de edad impuesto por el régimen jubilatorio ordinario ni encontrarse jubilado, así como también, poseer título docente, habilitante o supletorio para el cargo que se concursa; o bien reunir antecedentes equivalentes y suficientes fehacientemente acreditados.

Es importante recordar que, cada concurso tendrá un Jurado compuesto por trabajadores municipales y representantes tanto de la ASOEM como del Ejecutivo Municipal.