---
category: La Ciudad
date: 2021-08-20T06:00:03-03:00
thumbnail: https://assets.3dnoticias.com.ar/BICOLE.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'Santa Fe: plantean integrar recorridos de colectivos con otros medios de
  movilidad'
title: 'Santa Fe: plantean integrar recorridos de colectivos con otros medios de movilidad'
entradilla: 'Implicaría que quien se tome un colectivo -por ejemplo-, en la parada
  de descenso pueda subirse a su bici, moto o a un taxi para llegar a su destino. '

---
El 24 de junio de este año, el Concejo declaró en emergencia el transporte público de pasajeros por colectivos de la ciudad de Santa Fe. Esa ordenanza estipulaba la conformación de una Mesa de Seguimiento integrada por concejales, funcionarios municipales, empresarios del sector, referentes de UTA Santa Fe y del Órgano de Control, entre otros. Y este jueves, se reunieron alrededor de una misma mesa por primera vez todos estos actores para discutir qué salidas posibles pueden diseñarse para paliar la crisis del sector y, sobre todo, garantizar la continuidad del servicio.

Esta instancia inicial (que se replicará cada 15 ó 20 días) sirvió para hacer una agenda de coyuntura de temas a resolver sobre dos ejes: uno macro, que involucra el reparto de subsidios nacionales al transporte urbano para ciudades del interior del país, como esta capital; y uno "micro", que atienda las particularidades locales del sistema, con los recorridos, las frecuencias de las líneas y la antigüedad de las unidades de cara a las demandas de los usuarios, entre otros ítems.

El encuentro dio indicios de que la discusión pasó por una mirada realista sobre la actual crisis económica y de sustentabilidad que atraviesa el sector: dicho de otro modo: se planteó cuál es el sistema de transporte que la ciudad puede tener "con lo que hay", lejos de "lo ideal" y de soluciones mágicas. Y en este sentido, una de las ideas planteadas fue integrar los recorridos de las líneas y sus alcances geográficos con otros medios de transporte públicos o particulares, en una suerte de "multimodalidad".

Quien impulsó esta idea fue el concejal Carlos Suárez (UCR-Juntos por el Cambio), presente en la reunión. El legislador local insistió, en diálogo con este diario, en que en el momento bisagra en que se encuentra el servicio de colectivos, habrá que revisar recorridos, rediseñar "frecuencias reales y realistas", y sopesar "soluciones inteligentes", lo cual demanda el precario estado del sistema, de cara a garantizar su continuidad.

**La multimodalidad**

La discusión para Suárez pasa sobre cómo lograr un sistema de transporte que preste el mejor servicio posible, "en el cual habrá que incorporar, seguramente, otros medios de movilidad, porque como la cobertura de las líneas no será la mejor ni la más completa, esa falencia deberá ser suplida con otros medios. Esto es algo que hay que debatir en el marco de la crisis del transporte".

Entonces, "cabría reflexionar sobre cómo combinar otras formas de movilidad (bicicletas, motos, monopatines eléctricos, taxis, remises, incluso autos particulares) con el sistema de colectivos, para suplir las diferentes distancias en coberturas que las líneas de colectivos no cubran", añadió. También, blanquear la discusión respecto de qué pueden hacer las empresas, es decir, que sostengan el pago de sueldos a sus empleados y el estado en que se encuentran las unidades.