---
category: La Ciudad
date: 2021-09-12T06:16:23-03:00
thumbnail: https://assets.3dnoticias.com.ar/CHATARRA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Ya se reunieron 15 toneladas de "chatarra" electrónica para ser reutilizada
  en la ciudad de Santa Fe
title: Ya se reunieron 15 toneladas de "chatarra" electrónica para ser reutilizada
  en la ciudad de Santa Fe
entradilla: 'El próximo sábado 18 se hará una nueva colecta en la sede de Distrito
  Noreste. La modalidad de entrega es a través del servicio de pick up. '

---
El programa de recolección de Aparatos Eléctricos y Electrónicos en desuso (AEEs) viene teniendo buena aceptación en la ciudadanía santafesina. Se inició en la ciudad capital en abril pasado y, a la fecha, ya se lograron reunir 15 toneladas de material -contando los 1.200 kg de hoy-, que ya fueron trasladadas y procesadas en la planta situada en el complejo ambiental, donde se dividen los aparatos que pueden repararse para extender su vida útil de aquellos que ya no sirven y deben desarmarse. Algunas piezas se reciclan y otras van a disposición final.

La última campaña se realizó días atrás frente a la sede de la Asociación de Comerciantes, Industriales, Profesionales de la avenida Aristóbulo del Valle (ACAV), ubicada en Aristóbulo del Valle 6726. Y la próxima tendrá lugar el sábado 18, de 10 a 13, en Aristóbulo del Valle 9446.

La Eco estación de reciclado recoge y lleva a la planta de procesamiento los electrodomésticos que la propia gente debe llevar hasta el punto de recolección, ya que la modalidad es pick up. F

Como es habitual, la recolección se realiza bajo la modalidad pick up, por lo que los vecinos que concurran en auto entregan los aparatos al personal municipal que estará apostado en los puestos, sin necesidad de bajarse del vehículo. De esta manera, el objetivo es garantizar el cumplimiento de los protocolos establecidos en el marco de la pandemia de Covid-19.

Paralelamente, se ubicó un puesto de Ecocanje. En ese sentido, a todos los vecinos y vecinas que llevaron materiales reciclables se les entregó a cambio semillas y chips de madera producidos en Colastiné, en la planta de reducción de restos de poda municipal, que permite, por un lado, no llevar tantos residuos verdes para relleno sanitario y, por otro, no usar tantos camiones.

**Concientización**

Edgardo Seguro, secretario de Ambiente de la Municipalidad, indicó: "Estamos en una nueva recolección de aparatos eléctricos y electrónicos, a la que les sumamos también recolección de residuos secos. En esta ocasión elegimos a la ACAV porque mantenemos desde siempre una buena relación y trabajamos en conjunto en esta avenida comercial tan importante, en cuanto a la concientización referida a la disposición de residuos".

"La planta de reciclado que tenemos procesa más de 3 toneladas por semana -aseguró el funcionario-. Gracias a esa capacidad de trabajo acrecentamos las campañas y trabajamos en conjunto con asociaciones y calles emblemáticas de la ciudad, para traer estos aparatos que ya no se usan y que no tengan como destino final el relleno sanitario, sino el reciclado".

**Participación de los vecinos**

Por su parte, Sabrina Smith, gerente de la Asociación de Comerciantes de Aristóbulo del Valle (ACAV), contó que desde la entidad "se había solicitado que se realice la campaña aquí; los vecinos estaban esperando esta actividad así que estamos más que agradecidos".

Asimismo, Smith recordó que "somos un punto eco, y durante todo el mes tenemos las campanas para que los vecinos y vecinas traigan papel, vidrio y metal. Además, una vez al mes junto a la Municipalidad realizamos el Ecocanje. La gente participa de la actividad", aseveró.

Elena Colombo, vecina de barrio Mayoraz, se acercó hasta el punto de encuentro y aseguró que la iniciativa "es un gran adelanto: la gente tiene que acostumbrarse a reciclar". En ese sentido, Elena consultó sobre qué hacer con las pilas y el aceite usado: "Me dieron buenas respuestas para ambas cosas, me comentaron que están buscando una solución definitiva a ese asunto", respondió.

"Todas estas iniciativas son fundamentales para cuidar el medio ambiente", aseguró la vecina de barrio Mayoraz que además es docente jubilada, se definió como ecologista y relató que cuando desempeñaba su profesión, trabajó estos temas con mucho ahínco.

Cabe recordar que, para la recepción de los artefactos, es necesario que sean aparatos completos y no piezas sueltas o dañadas. Se trata de elementos eléctricos y electrónicos en desuso, que no se utilizan, pero están enteros, por lo que no se consideran residuos.