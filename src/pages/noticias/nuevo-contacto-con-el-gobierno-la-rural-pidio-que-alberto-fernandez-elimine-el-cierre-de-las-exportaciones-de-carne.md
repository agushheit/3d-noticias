---
category: El Campo
date: 2021-06-10T08:03:34-03:00
thumbnail: https://assets.3dnoticias.com.ar/exportacion.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Infobae
resumen: 'Nuevo contacto con el Gobierno: la Rural pidió que Alberto Fernández elimine
  el cierre de las exportaciones de carne'
title: 'Nuevo contacto con el Gobierno: la Rural pidió que Alberto Fernández elimine
  el cierre de las exportaciones de carne'
entradilla: 'Nicolás Pino, nuevo presidente de la Sociedad Rural Argentina, se reunió
  con el ministro de Agricultura, Luis Basterra, y el secretario de Asuntos Estratégicos,
  Gustavo Béliz. '

---
En 24 horas el nuevo presidente de la Sociedad Rural Argentina (SRA), Nicolás Pino, se reunió con tres funcionarios del gabinete nacional, en medio del conflicto entre el Gobierno y el campo por el cierre de las exportaciones de carne vacuna. Ayer, el dirigente planteó su preocupación por el tema al ministro de Desarrollo Productivo, Matías Kulfas, y hoy a la tarde hizo lo mismo ante el ministro de Agricultura, Luis Basterra, y el Secretario de Asuntos Estratégicos, Gustavo Béliz.

Si bien ambas reuniones se organizaron con el objetivo de que Pino se presente como flamante presidente de la entidad ante el Gobierno, en las mismas se analizó el impacto que está teniendo el freno de las ventas al exterior de carne vacuna, en momentos donde la cadena ganadera atraviesa momentos de elevada incertidumbre como consecuencia de las versiones de que la administración de Alberto Fernández profundizaría las restricciones, cuya finalización vence el 20 de junio, y aumentaría las retenciones.

Según informó la Sociedad Rural Argentina, el presidente de la entidad señaló que el encuentro que mantuvo con Luis Basterra y Gustavo Béliz, “fue muy cordial, y sirvió para para presentarnos, en la que les describimos cómo vemos la situación actual del campo desde la producción y desde nuestra entidad”. Asistió a la reunión acompañado por su vicepresidente primero, Marcos Pereda. En un comunicado, el dirigente precisó que también se planteó que el cierre de las exportaciones de carne “complica a muchos integrantes de la cadena productiva y que esperamos que el Presidente revea esta medida por el bien del campo y del país”, dijo Pino.

Las reuniones tuvieron lugar mientas la Mesa de Enlace aguarda una respuesta al pedido de audiencia que presentaron a Alberto Fernández la semana pasada, tras el cese de comercialización de hacienda por 14 días que realizaron en rechazo a la decisión de cerrar las exportaciones. Las entidades del campo no descartan profundizar el accionar gremial si no hay una respuesta favorable al pedido de los dirigentes.

Hasta el momento no avanzaron las negociaciones para destrabar el conflicto, luego que los frigoríficos exportadores ofrecieran enviar más carne al mercado interno para que se comercialice a precios accesibles, en el marco de un acuerdo de precios que se extendió hasta el 31 de diciembre. Por otro lado, la semana pasada, tras las fuertes críticas esgrimidas por el Consejo Agroindustrial Argentino a las limitaciones para exportar, el presidente Alberto Fernández recibió a representantes de ese espacio con la promesa de que se entablará una mesa de diálogo.

**Caída del índice de confianza**

La medida de cerrar las exportaciones de carne vacuna, provocó entre los productores una abrupta caída del índice de confianza. Así lo reflejó la última encuesta del Centro de Agronegocios de la Universidad Austral, de la que participaron 406 productores y cuyo valor bruto de producción es igual o superior a los USD 200.000, donde los niveles de confianza cayeron un 12,2% entre mayo y marzo pasado, al reducirse el mismo de 90 a 79 puntos.

Los especialistas encargados de realizar la encuesta, señalaron que actualmente los productores cuentan con precios en alza que arrojan un resultado económico positivo para la campaña agrícola en curso, pero se enfrentan a una elevada incertidumbre por los efectos del frente político, las recientes intervenciones en los mercados y con el temor de futuras medidas que aumenten retenciones o cierren mercados en distintos cultivos.

A todo esto, dijeron que el cierre de las exportaciones de carne vacuna “vuelve a poner sobre la mesa discusiones del pasado y acerca nuevamente el temor de políticas que no han dado resultado. Además, la caída de confianza y pesimismo para los próximos doce meses es mucho mayor que la caída en la situación actual”. Por último, entre las palabras y definiciones promovidas por los productores para calificar la decisión oficial, allí el informe de la Universidad Austral anota conceptos tales como “error, desastre, desconocimiento, ignorancia, mala, locura y burrada”, como las más formuladas.