---
category: La Ciudad
date: 2021-01-22T04:02:27Z
thumbnail: https://assets.3dnoticias.com.ar/concejo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Concejo Municipal Santa Fe
resumen: El Concejo destacó el trabajo del personal de salud de la ciudad
title: El Concejo destacó el trabajo del personal de salud de la ciudad
entradilla: El Concejo Municipal declaró su beneplácito por el “compromiso, vocación
  de servicio y dedicación” puesta de manifiesto por el personal abocado a las salas
  Covid del sistema de salud público y privado de la ciudad.

---
Al acercarse al Hospital José María Cullen, el presidente del Concejo, Leandro González, fue recibido por el director, Juan Pablo Poletti, y el subdirector, Hernán Malatini, para recibir el reconocimiento por parte del Concejo Municipal, instancia en la cual se cumplió con los protocolos correspondientes.

El beneplácito destaca el valor y la actitud del capital humano del personal de la salud en todas sus funciones: enfermeros, médicos, técnicos y personal de mantenimiento, mostrando una auténtica vocación de servicio público y compromiso con la población en la situación de emergencia por el Covid-19. Esta declaración surge de dos proyectos unificados del presidente del Concejo, Leandro González, y el concejal Sebastián Mastropaolo.

Al respecto, Leandro González hizo hincapié en no permanecer indiferentes a la pandemia y trabajar en equipo, cada uno desde su lugar, para combatir esta enfermedad. “Queremos reconocer y reivindicar a quienes están trabajando con la presión frente al desconocimiento global de los efectos de la enfermedad, pero sin resentir el compromiso profesional, con un ejemplo de entrega en favor del prójimo, solidaridad y respeto para con todos los santafesinos”.

Asimismo, el presidente destacó que “no habría estrategia que pudiera resultar si este equipo no estuviera haciendo su mejor esfuerzo en el territorio. El mérito de cuanto logro haya les corresponde y por eso le damos nuestro agradecimiento”. También remarcó la importancia de “ayudar a los y las trabajadores respetando las normas, el distanciamiento social, cumpliendo los protocolos y evitando riesgos innecesarios, porque es la manera que tenemos de ayudarlos a ellos a cumplir su trabajo”.

Por su parte, el director del Hospital José María Cullen, Juan Pablo Poletti, dijo estar “altamente gratificado por este reconocimiento. Es un mimo para todos los que trabajamos en la salud durante la pandemia, que hemos estado y seguimos estando al frente de batalla. Recibir este reconocimiento por parte del Concejo, sin lugar a dudas, nos da fuerzas para seguir enfrentando esta pandemia que aún no terminó”.

En tanto, el concejal Sebastián Mastropaolo señaló que “es un nuevo reconocimiento a un lugar tan importante de nuestra ciudad, un lugar que hoy está batallando contra una pandemia sin antecedentes. Un lugar que se esfuerza día a día para atender a los vecinos y vecinas de la ciudad. Este es un mínimo halago a un lugar que hoy está recuperando vidas, cuyo personal está haciendo todos sus esfuerzos a pesar de los riesgos de contagio. Desde el Concejo, nuestro gran abrazo y nuestro reconocimiento absoluto para cada uno de los enfermeros, médicos, directivos y personal que trabaja en el Hospital y que continúan su labor porque esta pandemia todavía no terminó y tenemos que seguir cuidándonos, usando el barbijo, para ayudarlos a ellos”.