---
category: Estado Real
date: 2022-01-04T06:00:10-03:00
thumbnail: https://assets.3dnoticias.com.ar/saucebrasil.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Luego de 20 años, el Aeropuerto de Sauce Viejo volvió a conectarse con Brasil
title: Luego de 20 años, el Aeropuerto de Sauce Viejo volvió a conectarse con Brasil
entradilla: La primera carga desde San Pablo arribó este lunes a la ciudad de Santa
  Fe, tras retomar las operaciones de exportaciones e importaciones áreas desde la
  provincia.

---
El Aeropuerto Sauce Viejo recibió hoy el primer vuelo de carga desde San Pablo, Brasil, marcando un nuevo hito para la región en materia de comercio internacional. La firma importadora fue CINTER, caracterizada por su alta experiencia en el mercado de obras industrializadas.

El vuelo es el resultado de la articulación entre el gobierno provincial, una empresa de transporte aéreo del Grupo SP, Servicios Portuarios (Depósito Fiscal del Puerto de Santa Fe) y la Cámara de Comercio Exterior. En este trabajo conjunto, también participaron los ministros de Producción, Ciencia y Tecnología, Daniel Costamagna, y de Infraestructura, Servicios Públicos y Hábitat, Silvina Frana; los secretarios de Comercio Exterior, Germán Burcher, y de Empresas y Servicios Públicos, Carlos Maina; la Policía de Seguridad Aeroportuaria (PSA); la Aduana; y AduComex, entre otros actores.

Al respecto, la presidenta del Aeropuerto, Silvana Serniotti, destacó "el importante trabajo conjunto que se realizó con los distintos actores del sector público y privado, que permitió generar las condiciones necesarias para llevar adelante este propósito, logrando después de muchos años ofrecer desde nuestro Aeropuerto esta importante herramienta. Estos son los lineamientos que nos pide el gobernador Omar Perotti y la ministra Silvina Frana, intensificando los esfuerzos para acompañar de manera concreta al sector productivo, sumando competitividad a las Pymes de Santa Fe y la región con los principales destinos del comercio internacional".

Por su parte, el vicepresidente de la firma SP, José Trabattoni, se mostró entusiasmado por este gran avance: “Desde que reabrimos la operación de carga aérea nacional hace dos años, teníamos en la mira el objetivo de exportar e importar directamente desde el aeropuerto de Sauce Viejo, y hoy es un hecho concreto”.

Esta acción logra tender puentes directos con los mercados más competitivos del mundo, agilizando la operatoria aduanera, permitiendo a los clientes hacer aduana de origen o destino en el depósito fiscal Servicios Portuarios, del Puerto de Santa Fe.

Finalmente, las autoridades del aeropuerto destacaron el trabajo conjunto entre el sector público y privado permitió realizar las gestiones y dar el puntapié para reactivar las economías regionales y proyectarlas al mundo.

**Vuelos regulares**  
Durante el mes de enero, los vuelos arriban a Sauce Viejo los días lunes, miercoles, jueves y sabados, a las 8 horas; y embarcan hacia Aeroparque a las 9 horas.