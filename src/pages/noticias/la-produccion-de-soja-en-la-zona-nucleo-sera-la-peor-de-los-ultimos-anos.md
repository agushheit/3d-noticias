---
category: El Campo
date: 2022-05-21T18:23:39-03:00
thumbnail: https://assets.3dnoticias.com.ar/soja.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA - Agustina Galarza.
resumen: La producción de soja en la zona núcleo será la peor de los últimos años
title: La producción de soja en la zona núcleo será la peor de los últimos años
entradilla: La sequía del verano pasado perjudicó la siembra y la cosecha en la región,
  dejando la campaña 2021/2022 más baja en comparación al ciclo pasado.

---
La Bolsa de Comercio de Rosario elaboró un informe donde destacó que "hay una **pérdida de un millón de toneladas**. La **cosecha de soja** finaliza con un 7 % menos de producción que el ciclo pasado y es la más baja de los últimos 14 años”, manifestó la BCR.

Además, la entidad afirmó que la **falta de agua y la inestabilidad de los precios** condicionan las decisiones de siembra de la próxima campaña. Por otra parte, se explicó que **la superficie sembrada fuera menor** y que aun así las proyecciones daban mejores rendimientos.

En el sudeste cordobés se registró el mejor rendimiento 33,5 qq/ha, en el centro-sur santafesino se alcanzó un promedio de 30,2 qq/ha, en el extremo sur santafesino se observaron mejores resultados 32,3 qq/ha, en el noreste bonaerense los rendimientos promedian los 30,4 qq/ha y el noroeste terminará con 32 qq/ha. La sequía y las heladas afectaron al cultivo en momentos críticos, según el informe de la BCR, se perdieron 185.000 hectáreas y quedó en 4,45 millones de hectáreas.