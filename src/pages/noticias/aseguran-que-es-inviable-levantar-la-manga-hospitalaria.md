---
category: La Ciudad
date: 2021-09-07T06:00:18-03:00
thumbnail: https://assets.3dnoticias.com.ar/MANGA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Aseguran que es "inviable" levantar la manga hospitalaria
title: Aseguran que es "inviable" levantar la manga hospitalaria
entradilla: Así lo indicaron desde el hospital Cullen "mientras sigan las internaciones
  en la carpa del hospital militar reubicable" sobre el predio del Liceo Militar.

---
El pedido de remoción de la manga hospitalaria que une el hospital José María Cullen con el hospital miliar reubicable instalado en el predio del Liceo Militar, la cual interrumpe desde hace más de 4 meses el tránsito sobre avenida Freyre, fue realizado por un nutrido grupo de comerciantes de la zona fundamentando la merma en las ventas por el escaso movimiento que provoca el enlace entre ambos nosocomios.

Lo cierto es que luego de manifestarse y entregar una carta al Consejo Directivo del hospital Cullen con el pedido de levantamiento de la manga hospitalaria, los comerciantes fueron recibidos y escuchados por el director del nosocomio, Juan Pablo Poletti, quien le argumentó a los manifestantes la utilidad que tiene la manga para el día a día del hospital.

Tras el encuentro, los comerciantes de la zona del hospital Cullen y avenida Freyre, decidieron elevar el pedido a las autoridades del Ministerio de Salud provincial y a la Municipalidad local. "La situación es apremiante por la cantidad de comercios que cerraron su puertas y por la gran cantidad que está pensando hacerlo en los próximos días por la escasa cantidad de movimiento comercial en la zona", subrayaron los manifestantes luego de la reunión con Juan Pablo Poletti.

El resultado de la reunión con el director del hospital Cullen, Juan Pablo Poletti, fue conceptuada como "buena" por los comerciantes de la zona. "Para el director, si bien la situación epidemiológica es alentadora, sanitariamente no ve posible retirar la manga hospitalaria por el gran uso que todavía le dan los enfermeros y médicos", subrayó Fernanda, una de las comerciantes de avenida Freyre.

El director del Cullen se refirió a la situación: "Es inviable levantar la manga hospitalaria manteniendo la internación en la carpa del hospital militar reubicable", sentenció Juan Pablo Poletti y agregó en relación al pedido de los comerciantes: "Me pidieron que se mantenga en funcionamiento el hospital de campaña pero que levantemos la manga; así es impensable funcionar. Por el momento, tanto las carpas que conforman el hospital militar, como la manga que lo une con el edificio del Cullen, no se van a levantar dado que imposible el funcionamiento como compartimentos estancos".

"Hoy el hospital militar funciona realmente como una sala más del Cullen, donde tres veces por día se retira ropa sucia y se llevan prendas limpias, donde todos los días se trasladan elementos de farmacia; toda la logística de economato dos veces por semana, los tubos de oxígeno que se recargan más los pacientes que a diario van a cirugía, rayos o ecografía. Por todo esto resulta inviable y hasta peligroso retirar la manga sobre avenida Freyre", resaltó Poletti.

"En un contexto donde estamos aún expectantes de lo que pueda ocurrir con la variante Delta. Hoy, todavía en pandemia, y con los pacientes no covid en un alto grado de atención, es imposible pensar el hospital de campaña funcionando sin la manga hospitalaria", finalizó Juan Pablo Poletti, director del hospital Cullen.