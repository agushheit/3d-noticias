---
category: La Ciudad
date: 2021-10-19T06:00:36-03:00
thumbnail: https://assets.3dnoticias.com.ar/procrear2.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Se sorteó el Procrear II
title: Se sorteó el Procrear II
entradilla: En la provincia de Santa Fe, 4.500 personas se inscribieron para obtener
  su primera casa.

---
Este lunes 18 de octubre se sortearon las 199 viviendas del Parque Federal en la ciudad de Santa Fe, como parte del programa Procrear Desarrollos Urbanísticos. el mismo corresponde al Ministerio de Desarrollo Territorial y Hábitat de la Nación.

 El sorteo fue transmitido en vivo a través de la TV Pública.

En la provincia de Santa Fe, 4.500 personas se inscribieron para obtener su primera casa.

 Días atrás, el secretario de Hábitat, Urbanismo y Vivienda, Amado Zorzón, comentó que las viviendas “no están terminadas, pero a medida que estén habitables se irán adjudicando”. 

 A su vez, dio a conocer que Omar Perotti les pidió maximizar "el esfuerzo para resolver el tema de la vivienda. Por eso en la ciudad de Santa Fe, como en todo el territorio provincial, se están llevando adelante diferentes intervenciones y se proyectan nuevas, a través de la Dirección Provincial de Vivienda”, explicó.