---
layout: Noticia con imagen
author: .
resumen: Humedales
category: Agenda Ciudadana
title: Piden declarar la Emergencia Socio Ambiental
entradilla: Controversia por los humedales tras los peligrosos incendios
  ocurridos durante los últimos meses en el Delta del Paraná.
date: 2020-11-04T16:01:36.701Z
thumbnail: https://assets.3dnoticias.com.ar/emergencia-social.jpg
---
La controversia por los humedales tomó fuerza en el estado público en la Argentina tras los peligrosos incendios ocurridos durante los últimos meses en el Delta del Paraná. Dichos incidentes no solo sirvieron de disparador para que se instale en la agenda la necesidad de sancionar una Ley de Humedales, sino para lanzar acusaciones cruzadas entre ambientalistas, la política y el sector agropecuario, sobre las responsabilidades de los incendios.

A raíz de este asunto,el domingo 1 de noviembre, cerca de las 14 horas partió desde Rosario una caravana de integrantes de Multisectorial por los Humedales de Rosario que recorrió diferentes localidades afectadas por las quemas, hasta llegar al primer destino en ciudad de Santa Fe. La jornada comenzó a las 11 con una plantación de árboles nativos en tres puntos de la ciudad: Arroyo Ludueña, Parque de Colectividades y Parque Regional Sur (Rosario).

El lunes 2 de noviembre, integrantes de la Multisectorial de Rosario acompañaron a la Multisectorial de Santa Fe en la entrega conjunta del Plan de Emergencia Territorial Socio Ambiental para las Poblaciones y Humedales de las Provincias y Jurisdicciones que Componen la Cuenca del Plata.

Uno de los objetivos principales y que más se recalcó fue que este plan es la herramienta que hará posible la restauración de los territorios afectados por el fuego, ya que compromete al fin de las quemas, a la atención a la flora y fauna, a la no modificación en el uso y destino de las tierras y a la limitación de actividades antrópicas contrarias a la restauración de los ecosistemas.

Además, el mismo lunes ambas Multisectoriales continuaron la caravana hacia la ciudad de Paraná, donde se entregó el mismo PLAN por mesa de entrada en Casa de Gobierno de Entre Ríos.

El Plan de Emergencia está siendo presentado, además, a nivel municipal en diversas localidades y próximamente se hará entrega al gobierno nacional.

La caravana por los humedales continuó su recorrido por los pueblos afectados por las quemas hasta llegar a ciudad de Victoria, donde fueron recibidos por autoconvocados y organización en defensa de los humedales. 

Desde el inicio de sus actividades, esta Multisectorial ha pedido el fin de las quemas y una ley de Humedales que proteja los territorios. Hoy, más firmes que nunca, entendemos que urge un plan de acción inmediato que proteja los humedales y los bosques hasta la aplicación de la tan esperada ley.

La caravana tiene como objetivo, visibilizar la unión de los pueblos en esta gesta por la protección de los humedales y de nuestro patrimonio natural y cultural que trasciende las barreras políticas y jurisdiccionales.

Así como los Humedales son sistemas interconectados a nivel cuenca, los pueblos tenemos lazos hermanos que nos unen y retroalimentan. Los humedales no están aislados, no respetan fronteras arbitrarias ni pueden encerrarse en jurisdicciones aisladas.