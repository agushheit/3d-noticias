---
category: La Ciudad
date: 2021-04-18T06:00:40-03:00
thumbnail: https://assets.3dnoticias.com.ar/TUNEL.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'Coronavirus: intensifican los controles sanitarios en el acceso al Túnel
  Subfluvial'
title: 'Coronavirus: intensifican los controles sanitarios en el acceso al Túnel Subfluvial'
entradilla: Allí se encuentran apostados agentes que realizan testeos de olfato y temperatura
  a los ocupantes de los vehículos  que ingresan dese la vecina provincia de Entre
  Ríos.

---
En razón de las nuevas medidas y disposiciones provinciales de aumentar y profundizar las medidas y los controles, la Unidad Regional I y  la Policía de Seguridad Vial  establecieron operaciones conjuntas para  comenzar a partir de este viernes durante las 24 horas controles  en el acceso a la capital provincial santafesina del Túnel Subfluvial.

 La Jefa de Unidad Directora de Policía Licenciada Marcela Muñoz, junto al   jefe de la división de Planificación de la Policía de Seguridad Vial, Jonatan Rua y el coordinador de Seguridad Social, Facundo Bustos explicaron la forma de trabajo, enfatizando que además de los controles vehiculares de rutina llevados a cabo (papeles en orden, cantidad de pasajeros, patentes, seguro, etc) se implementan ahora con mayor rigurosas luego del endurecimiento de los controles por la pandemia. Además, remarcaron que ante cualquier sintoma que presente una persona o responda por encima de los niveles aceptables en cuanto lo medido ( temperatura, olfato) será motivo para no permitir el ingreso a la provincia, debiendo retornar a su lugar de origen. Por otro lado, resaltaron la importancia del trabajo mancomunado entre las fuerzas de seguridad.

 Por su parte, la Jefa de la Unidad Regional I destacó la profundización de los controles en la ciudad de Santa Fe donde habrá policías apostados con termómetros en controles que comenzarán luego de la hora permitida de circulación -entre las 0 y las 6 -  recalcando que en esa franja horaria las personas no pueden circular, excepto que sean personal esencial y/o exhiban el correspondiente permiso.