---
category: Agenda Ciudadana
date: 2021-09-20T06:00:20-03:00
thumbnail: https://assets.3dnoticias.com.ar/CGT.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: La CGT definirá si reforma el estatuto y debatirá el escenario tras las PASO
title: La CGT definirá si reforma el estatuto y debatirá el escenario tras las PASO
entradilla: Los representantes gremiales ratificarán la marcha del 18 de octubre y
  el congreso nacional extraordinario de renovación de autoridades de la CGT por los
  próximos cuatro años, a realizarse el 11 de noviembre.

---
El Comité Central Confederal de la CGT, instancia previa al congreso de renovación de autoridades de noviembre, deliberará este miércoles para analizar temas políticos y gremiales, como también cuestiones estatutarias y de encuadramiento, en un contexto signado por el resultado de las PASO, el reacomodamiento del Frente de Todos y la renovación del gabinete.

El encuentro se realizará desde las 11 en el salón Felipe Vallese de la histórica sede de Azopardo al 800 y, según los voceros sindicales consultados por Télam, "no habrá demasiadas sorpresas, ya que esa instancia es solo de profundo debate político", aunque signada por la derrota electoral del Frente de Todos, que nadie esperaba en el movimiento obrero.

En la reunión del miércoles, los representantes gremiales ratificarán la marcha del 18 de octubre para conmemorar el Día de la Lealtad Peronista y el congreso nacional extraordinario de renovación de autoridades de la CGT por los próximos cuatro años, a realizarse el 11 de noviembre en las instalaciones mercantiles de Parque Norte.

Además, aprobarán o no la reforma estatutaria de la central obrera para incorporar el 30 por ciento de cupo femenino a la próxima conducción.

También se analizará el conflicto por encuadramiento gremial surgido entre el Sindicato de Mecánicos y Afines del Transporte Automotor (Smata) y el gremio de garagistas y estacioneros, aunque ello ya generó críticas y un amplio debate en la mesa chica.

Los trabajadores de las estaciones de servicio de las rutas están encuadrados en el Smata de Ricardo Pignanelli pero el cotitular de la CGT, Carlos Acuña, exige el traspaso a su sindicato, lo que generó un conflicto que será debatido en el Confederal.

"No es posible que un Confederal, que es una profunda instancia política de debate, analice un conflicto de encuadramiento entre dos organizaciones hermanas", había señalado el metalúrgico Antonio Caló, pero sus observaciones no fueron respaldadas.

Además, el Confederal deliberará pocas semanas antes de un hecho trascendente, ya que en otro congreso nacional la Confederación Argentina de Trabajadores del Transporte (Catt), que lideran Juan Carlos Schmid (Dragado y Balizamiento) y Omar Maturano (La Fraternidad), deberá renovar sus propias autoridades.

Ese congreso se realizará el 7 de octubre próximo y, según todas las fuentes consultadas por esta agencia, los conductores de trenes de Maturano y los colectiveros (UTA) de Roberto Fernández respaldarán a Schmid para un nuevo período.

Hace algunos días, un sector de la Corriente Federal de Trabajadores (CFT), el Frente Sindical para el Modelo Nacional (Fresimona) y el espacio Sindicatos en Marcha para la Unidad Nacional (Semun), sellaron la unidad para posicionarse en común en el Confederal.

Ese encuentro se realizó en el recreo del gremio plástico del municipio bonaerense de Esteban Echeverría, y reunió a parte de los gremios de la CFT, representados en esa instancia por el diputado nacional y dirigente del cuero Walter Correa -el bancario Sergio Palazzo no asistió-, a las organizaciones que responden al moyanismo y al Semun de Sergio Sasia.

Según los asistentes, esos espacios congregaron a más de 80 organizaciones sindicales, pero los voceros cegetistas señalaron a Télam que "en el Confederal no habrá sorpresas y solo debatirá la coyuntura y ratificará la marcha de octubre y el Congreso de noviembre".

Otra de las cuestiones a profundizar será el repaso del escenario político tras los resultados de las PASO, en una semana de reacomodamientos en la coalición gobernante que finalizó con la renovación del gabinete​.

En el Confederal de la CGT participan los secretarios generales de cada organización sindical afiliada y, aunque se trata de una instancia de debate previa al congreso de la central obrera, en paralelo ya comenzaron a reunirse "mesas chicas y grandes con vistas a noviembre", con vistas al congreso de renovación de autoridades.

En cuanto a la futura conducción cegetista, de aquí a noviembre será vital el rol de los gremios del transporte, ya que una posición común de La Fraternidad y la UTA multiplicaría el poder de fuego de ambas organizaciones, algo fundamental ante eventuales conflictos y clave para una eventual paralización de la actividad.

En ese juego de encuentros, análisis y alianzas con vistas a la elección de la nueva conducción cegetista, ya comenzaron a moverse los representantes de los grandes gremios de servicios -los llamados 'gordos', con el actual cotitular de la CGT Héctor Daer al frente-, y los independientes o dialoguistas Andrés Rodríguez (Upcn), Gerardo Martínez (Uocra) y José Luis Lingeri (Obras Sanitarias).

Esa alianza entre 'gordos' e independientes permanece intacta y se fortaleció aún más en los últimos tiempos, y rechaza de plano la posibilidad de incorporar al moyanismo -en especial al secretario adjunto de Camioneros, Pablo Moyano- a futuros cargos de conducción de relevancia.

Al respecto, algunos voceros sindicales señalaron que la próxima conducción podría reiterar la figura del triunvirato para sellar "la unidad", y ya mencionan la posibilidad de que sea integrado por Daer, Caló en representación de los gremios industriales, y un tercer dirigente del sector del transporte, pieza vital y clave para contener a la Catt.

Sin embargo, esa experiencia fracasó en sus dos experiencias anteriores: la primera cuando se integró con Hugo Moyano, Lingeri y Susana Rueda -con el tiempo solo permaneció en la central obrera el camionero-, y la segunda en agosto de 2016, con Juan Carlos Schmid -quien luego renunció-, Carlos Acuña y el propio Daer.

En esa instancia y, de forma gradual, a excepción de Maturano y Fernández, la totalidad de los sindicatos del transporte abandonaron el consejo directivo de la central obrera.

"Todos hablan de unidad, pero nadie quiere bajarse del caballo. De esa forma es muy difícil. La idea es que a partir de noviembre haya un solo secretario general, pero no se descarta reiterar la figura del triunvirato en aras de aquella unidad. Esa instancia dependerá de cómo se trate a los gremios del transporte", confió un dirigente de la CGT.

La renovación de autoridades en la Catt, que agrupa a casi una treintena de sindicatos aéreos, marítimos, fluviales y terrestres, el 7 de octubre próximo, es observada con atención por toda la CGT, ya que será la antesala de lo que puede suceder en noviembre.

Sin embargo, todas las fuentes del sector del transporte que fueron consultadas por esta agencia señalaron que en su seno existe una lucha interna sin retorno entre colectiveros y camioneros, y que si el rechazo al moyanismo se reiterase en la futura CGT "no habría forma de detener la conformación de una central obrera paralela, que agrupe a disgustados y opositores".

En paralelo, todavía es una incógnita cómo se posicionarán los gremios que aún responden al barrionuevismo, y que mantiene a Acuña como cotitular de la CGT.