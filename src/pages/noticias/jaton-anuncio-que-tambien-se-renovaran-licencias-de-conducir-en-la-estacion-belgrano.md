---
category: La Ciudad
date: 2021-09-29T06:00:08-03:00
thumbnail: https://assets.3dnoticias.com.ar/LICENCIASBELGRANO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Jatón anunció que también se renovarán licencias de conducir en la Estación
  Belgrano
title: Jatón anunció que también se renovarán licencias de conducir en la Estación
  Belgrano
entradilla: 'A partir del 1 de octubre, se suma este lugar para los santafesinos que
  tengan que sacar el carné por vencimiento, pérdida o hurto. '

---
Se otorgarán 50 turnos por día más a partir de esta apertura. A esta descentralización se sumarán en noviembre dos puntos más: el Prado Español y La Tablada. El Centro municipal de Educación Vial del Parque Garay privilegiará aquellos que saquen su primera licencia y para los profesionales.

La Municipalidad abrirá un nuevo centro de emisión de licencias de conducir en la Estación Belgrano. El mismo comenzará a funcionar el 1 de octubre y allí se van a otorgar 50 turnos por día con el fin de descomprimir la demanda que hoy se da en el Centro Municipal de Educación Vial del Parque Garay. El intendente Emilio Jatón y los secretarios General, Mariano Granato, y de Control y Convivencia Ciudadana, Fernando Peverengo, brindaron detalles.

El proceso de descentralización en este trámite va a continuar y se prevén dos centros de emisión más que comenzarán a funcionar desde noviembre. Uno será el centro de distrito ubicado en el Prado Español, en Hernandarias y General Paz; y el otro en el Polideportivo de La Tablada, en Teniente Loza al 6900. En todos estos lugares, la atención tendrá el mismo horario que el del Parque Garay: de 7.30 a 17.30.

El intendente Emilio Jatón hizo un repaso sobre las distintas acciones que se tuvieron que hacer para llegar a esta instancia. “Cuando llegamos al municipio, a los pocos meses llegó la pandemia y teníamos 200 turnos por día en el Parque Garay y, con los protocolos por Covid, el personal se redujo y también el espacio. Y a eso hay que sumarle que durante dos meses el centro municipal de Educación Vial estuvo cerrado”, detalló.

En esa línea, continúo: “Ahora tenemos 17 mil santafesinos que están esperando sacar su licencia o renovarla. A partir de allí la Agencia Nacional de Seguridad Vial comenzó a prorrogar los plazos y hoy muchos de los que tienen las licencias vencidas esperan a diciembre que es el último plazo”.

Más adelante, Jatón resumió: “Empezamos a ver qué se podía hacer para que esa situación cambie y a realizar un trabajo con la Agencia Provincial de Seguridad Vial porque había que capacitar personal, adaptar computadoras y validar usuarios que no dependen del municipio; y lo hemos logrado. Lo que queremos es descentralizar este proceso que hasta ahora estaba centralizado solo en el Parque Garay”.

**Más detalles**

Es importante destacar que este trabajo se viene realizando en conjunto con la Agencia Provincial de Seguridad Vial (APSV) y la Agencia Nacional de Seguridad Vial (ANSV), dado que se necesita de la generación de claves de acceso para poder abrir nuevos centros emisores, además de capacitación para el personal y sistemas específicos para realizar el trámite.

En definitiva, la intención es que el Centro Municipal de Educación Vial del Parque Garay privilegie los carné de profesionales y los que sacan la licencia por primera vez; mientras que los que tengan que renovar ya sea por vencimientos, pérdidas o hurtos dispongan de los nuevos lugares de emisión, el primero de los cuales será la Estación Belgrano. Cuando se ingrese a la página para sacar el turno, estarán disponibles las opciones.

Asimismo, se informó que no se modificará el turno para aquellos que ya los sacaron, y los que ingresan a partir de octubre tendrán la opción de la Estación Belgrano en el turnero web que se habilitará el 1 de octubre. Desde noviembre sucederá lo mismo con los otros dos espacios a habilitar. También vale mencionar que los que tengan que hacer el exámen práctico deberán ir al Parque Garay, lo mismo sucederá con las charlas de seguridad vial que generalmente se dan cuando se saca la licencia por primera vez o cuando se cometieron faltas graves.