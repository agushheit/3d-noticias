---
category: Estado Real
date: 2021-12-10T06:00:13-03:00
thumbnail: https://assets.3dnoticias.com.ar/barcasia.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Perotti presenció la partida de la primera barcaza con contenedores desde
  el puerto de Santa Fe con destino a Asia
title: Perotti presenció la partida de la primera barcaza con contenedores desde el
  puerto de Santa Fe con destino a Asia
entradilla: "“Queremos que este lugar se convierta en una alternativa para que el
  trabajo de nuestra gente empiece a llegar a los distintos lugares del mundo”, afirmó
  el gobernador."

---
El gobernador Omar Perotti presenció este jueves la partida desde la Terminal de Contenedores y Cargas del Puerto de Santa Fe de la primera barcaza con contenedores rumbo, en primera instancia, al Puerto de La Plata, y desde allí con destino a Asia. La carga se compone de harina de carne y hueso de la firma Insuga S.A (radicada en Recreo) con destino a Vietnam, y leche en polvo de la firma Adecoagro (de su planta en Morteros) hacia Singapur.

En ese marco, el gobernador destacó la tarea de “todos los que han trabajado en este tiempo para optimizar este funcionamiento y que los contenedores puedan estar saliendo los días jueves, cada 15 días y, por logística pudiendo tener el margen de tiempo de llegada al Puerto de La Plata el día domingo, para realizar así el trasbordo los lunes, ya que los barcos con destino internacional salen los martes”.

“Aquí estamos generando un camino alternativo al puerto de Buenos Aires que ojalá logre más competencia porque Santa Fe tiene una enorme oportunidad en lo que plenamente confiamos, que es estratégico para la Argentina y para nuestra provincia, como es la Hidrovía”, indicó Perotti.

Perotti sostuvo que “lo importante es que hoy podemos demostrar que hay cargas y empieza a ser considerado el Puerto de Santa Fe de otra manera”. Y remarcó que “muchas instituciones hablan de la potencialidad que aquí había, pero no teníamos carga, no teníamos operatoria; por eso quiero destacar el esfuerzo de todo el directorio y las instituciones por haber logrado el puerto posible”.

Más adelante, el gobernador planteó el objetivo de “aprovechar esta instancia para juntar esfuerzos, mejorar la logística, para darle pleno protagonismo a Santa Fe sobre cada uno de sus puertos; su vínculo directo con las provincias de nuestro norte para que se convierta este lugar en una alternativa para que el trabajo de nuestra gente empiece a llegar a los distintos lugares del mundo”.

Así, Perotti remarcó su “plena confianza en que esta comunicación permitirá tener aquí muchos más contenedores”, agradeció el “trabajo cotidiano del ente, a TecPlata por asumir este desafío exigente”, y puntualizó que “las miradas están puestas en el cumplimiento de las frecuencias, en la salida de las barcazas y en el embarque en tiempo y forma con las navieras”.

**UN EJEMPLO Y PIEDRA FUNDAMENTAL**  
A su turno, el ministro de la Producción, Ciencia y Tecnología, Daniel Costamagna, destacó que “esto es un ejemplo concreto de los enormes esfuerzos que se están haciendo para acercar a Argentina a los mercados internacionales mejorando la competitividad de cada una de nuestras economías regionales, empresas y Pymes”.

En sintonía, la ministra de Infraestructura, Servicios Públicos y Hábitat, Silvina Frana, señaló que “hace muchos años venimos soñando para que el puerto de Santa Fe tenga este esquema productivo” e indicó que la región “va a tener con esta actividad portuaria una muy buena posibilidad, una ventaja”.

En tanto, el presidente del Puerto de La Plata, José María Lojo, dijo que “estamos poniendo la piedra fundamental de algo que va a ser muy virtuoso para la gente que trabaja y produce en la provincia de Santa Fe”, y añadió que “el espíritu de colaboración es lo que va a marcar la diferencia”.

Por su parte, el CEO Argentina de TecPlata SA, Bruno Porchietto afirmó que "nosotros no queremos ser solamente una empresa que carga y descarga contenedores, sino una empresa activa en el territorio donde opera", y agregó que "como parador logístico tenemos la responsabilidad de contribuir a bajar los costos y mejorar el servicio para los clientes, que al final son los que importan y exportan carga y mueven la economía del país”.

**INSERCIÓN AL MUNDO**  
A su turno, el presidente de la Cámara de Comercio Exterior de Santa Fe, Marcelo Perassi, mencionó al gobierno provincial “por las inversiones realizadas y por las que se vienen”, y destacó que “trabajamos mucho para que esto sea realidad ya que para nosotros es vital la baja del costo logístico para nuestros asociados”.

Asimismo, el presidente de la UISF, Alejandro Taborda, dijo que “como un industrial estoy emocionado porque a nosotros nos tildan de pocos competitivos, que no sabemos insertarnos en el mundo, exportar, probamos que si se dan las condiciones estamos a la altura de las circunstancias. Hoy tenemos un puerto lleno de granos y con posibilidad de insertarnos el mundo”.

Del mismo modo, el presidente de la empresa Insuga SA, Fabián Ardetti, recordó que “estábamos exportando hacia Asia a través del Puerto de Buenos Aires, pero ahora lo haremos desde aquí con tres envíos en diciembre, enero y febrero por 800 toneladas. A los demás empresarios santafesinos de la región, los invito a trabajar de esta forma porque seguramente nos tiene que ir muy bien”.

**LA OPERATORIA**  
Por último, el presidente del Ente Administrativo del Puerto de Santa Fe, Carlos Arese, expresó que “un día empezamos con la decisión política de apoyar esta propuesta y, a partir de allí, nos pusimos a trabajar y llegamos a esta instancia en la cual estamos realizando nuestro aporte para mejorar la competitividad de las Pymes”.

Arese resumió la operatoria, en los siguientes términos: “El compromiso que asumimos con el Puerto de La Plata y con la operadora TecPlata es que esta barcaza, que está siendo cargada, va a venir dos veces por mes, los días jueves, para navegar hasta el domingo, llegar a La Plata y, el día lunes, hacer el trasbordo internacional. Finalmente, el martes, saldrá desde La Plata”.

“Esa es la frecuencia inicial con la que pretendemos empezar a trabajar. El viernes llegaron los contenedores vacíos que ya viajaron a las empresas, volvieron cargados y ese cargamento es el que se está colocando en las barcazas. Estos contenedores vacíos van a estar acá a la espera de nuevas cargas”, completó.

**PRESENTES**  
En la oportunidad, estuvieron presentes además, el secretario de Empresas y Servicios Públicos provincial, Carlos Maina; el secretario de Comercio Exterior, Germán Burcher; el administrador a cargo de la Aduana, Carlos Ronchi; el subsecretario de Obras Públicas de la Nación, Edgardo Depetri; el presidente de Astillero Río Santiago, Pedro Wasiejko, y el gerente Comercial de Adecoagro, Alejandro Torres, entre otros.