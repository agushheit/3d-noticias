---
category: La Ciudad
date: 2021-07-22T06:15:08-03:00
thumbnail: https://assets.3dnoticias.com.ar/ANTICUERPOS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Reducen a tres semanas el intervalo de dosis de Sinopharm ante la llegada
  de la cepa Delta
title: Reducen a tres semanas el intervalo de dosis de Sinopharm ante la llegada de
  la cepa Delta
entradilla: Se definió en una nueva reunión del Cofesa, con la prioridad de completar
  esquemas mediante la aplicación de segundas dosis. El intervalo para dosis de AstraZeneca
  se definió en ocho semanas.

---
En el marco de una nueva reunión del Consejo Federal de Salud (Cofesa) entre los funcionarios de las carteras sanitarias de todas las provincias junto con la ministra de Salud, Carla Vizzotti, se definió la priorización de aplicar segundas dosis para completar esquemas de inmunización. Esto obedece al temor de un disparo en la curva de contagios ante la llegada de la muy transmisible variante Delta.

Según lo que informaron fuentes del Ministerio de Salud provincial, la reunión de trabajo que se realiza periódicamente para monitorear el estado de situación de la pandemia en el país y en cada uno de los distritos tuvo como eje evaluar alternativas para acelerar la aplicación de segundas dosis.

Teniendo en cuenta las tres vacunas disponibles en el país, se pensó en estrategias para abordar en cuanto a acelerar la inoculación en las provincias, donde Santa Fe registra una capacidad de vacunación cada vez mayor.

Por esto, se definió que el intervalo determinado entre la aplicación de primera y segunda dosis de Sinopharm se fije en tres semanas, cuando hasta el momento se estaba definiendo en cuatro semanas (un mes). Bajo esta información a la que accedió UNO, el suministro de segundas dosis de la vacuna china se verá acelerada en territorio santafesino, sumado al cargamento de vacunas Sinopharm que arribó este miércoles al país.

**Segundas dosis de AstraZeneca y Sputnik V**

En cuanto a los esquemas de inmunización iniciados con la primera dosis de AstraZeneca se definió como intervalo para aplicar su segunda dosis un plazo de ocho semanas o dos meses, según lo acordado en el Cofesa. Esta vacuna se venía aplicando con un intervalo de ocho hasta doce semanas entre primero y segundo componente.

La aplicación de segundas dosis de la vacuna rusa Sputnik V está sujeta a la llegada de nuevas dosis del segundo componente, puesto que es el único inmunizante disponible en el país que cuenta con diferencias en el contenido de cada dosis.

**No hay vacuna aprobada para menores**

En la reunión del Cofesa llevada a cabo durante este martes se dialogó sobre el tema de la vacunación a menores de 18 años en territorio nacional, con la expectativa del arribo de la vacuna Moderna, la cual se planifica usar en este público objetivo luego de ser aprobada por las autoridades sanitarias estadounidenses y la Anmat.

Sin embargo, se remarcó que por ahora no se dará ningún paso importante en la planificación de la estrategia para la vacunación de adolescentes en el país, puesto que ninguna vacuna está aprobada para ello. El próximo martes habrá una nueva reunión del Cofesa, en la que se espera que podrían surgir novedades en cuanto a la definición de una estrategia de inoculación a menores con comorbilidades.