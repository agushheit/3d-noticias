---
category: Agenda Ciudadana
date: 2021-06-26T06:15:44-03:00
thumbnail: https://assets.3dnoticias.com.ar/VOZZOTTI.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Vizzotti anunció una prórroga de la emergencia sanitaria hasta el 9 de julio
title: Vizzotti anunció una prórroga de la emergencia sanitaria hasta el 9 de julio
entradilla: La ministra de Salud de la Nación remarcó que “hay un alivio” por la baja
  que se observa en los casos positivos de coronavirus.

---

La ministra de Salud de la Nación, Carla Vizzotti, anunció que el estado de emergencia sanitaria se prorrogará por DNU hasta el próximo 9 de julio y aseguró que “hay un alivio” por la baja que se observa en los casos positivos de coronavirus.

Vizzotti habló durante una conferencia de prensa en el Centro Logístico de Benavídez, donde se lleva a cabo un operativo de control de las 1.181.500 dosis de vacunas de Astrazeneca que arribaron desde Estados Unidos.

El decreto que firmará el Presidente extenderá por 15 días el sistema de "semáforo epidemiológico" que dispuso el DNU anterior y que clasifica a las distintas jurisdicciones del país como zonas de riesgo sanitario bajo, medio y alto o de "alarma epidemiológica".

Para bajar los riesgos y evitar el ingreso de variantes que no registran circulación sanitaria en el país, el Gobierno aclaró que las fronteras seguirán cerradas al turismo hasta el 9 de julio y fijó un nuevo límite máximo de 600 pasajeros al día para el ingreso al país.

Para los casos en que se permita el ingreso, se estableció un férreo sistema de control para evitar que los viajeros rompan el aislamiento al que estarán obligados, a la vez que habrá un esquema de testeos periódicos a los que tendrán que ser sometidas esas personas.

El DNU faculta en su artículo segundo a gobernadores y al jefe de Gobierno porteño a adoptar determinadas medidas ante la verificación de determinados parámetros epidemiológicos, a través del denominado "semáforo epidemiológico".

Ese esquema se divide en bajo, mediano y alto riesgo, según la evolución de la pandemia en cada distrito del país. Los de bajo riesgo son aquellas jurisdicciones en donde la incidencia definida como el número de casos confirmados acumulados de los últimos 14 días por 100.000 habitantes sea inferior a 50. Los de riesgo medio son las que el número se ubique entre 50 y 150 y los de alto cuando sea superior a 150.

Los grandes aglomerados urbanos, departamentos o partidos de más de 300.000 habitantes son considerados en Situación de Alarma Epidemiológica y Sanitaria cuando la incidencia definida como el número de casos confirmados acumulados de los últimos 14 días por 100.000 habitantes sea igual o superior a 500 y el porcentaje de ocupación de camas de terapia intensiva sea mayor al 80 por ciento.