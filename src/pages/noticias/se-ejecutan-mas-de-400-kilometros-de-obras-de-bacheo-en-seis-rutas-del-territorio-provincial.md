---
category: Estado Real
date: 2021-02-10T06:14:19-03:00
thumbnail: https://assets.3dnoticias.com.ar/bacheo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: Se ejecutan más de 400 kilómetros de obras de bacheo en seis rutas del territorio
  provincial
title: Se ejecutan más de 400 kilómetros de obras de bacheo en seis rutas del territorio
  provincial
entradilla: Los trabajos demandan una inversión de 170 millones en una primera etapa.
  Se beneficiarán cinco departamentos santafesinos, incluidos los del corredor productivo
  y turístico de la Costa.

---
El Ministerio de Infraestructura, Servicios Públicos y Hábitat, a través de la Dirección Provincial de Vialidad, comenzó la ejecución de obras de bacheo en las rutas provinciales número 1, 3, 40, 32, 62 y 39, correspondientes a la primera etapa de los grupos de trabajos licitados en septiembre pasado.  
  
El plan de mejoramiento de los caminos beneficiará a los departamentos Garay, San Javier, San Justo, Vera y General Obligado. Los trabajos incluyen el retiro del material deteriorado y la recomposición de la carpeta de rodamiento con concreto asfáltico. Además, se realizarán sellados de fisuras.  
  
El administrador general de Vialidad Provincial, Oscar Ceschi, explicó el novedoso sistema implementado para solucionar en tiempo real el deterioro que sufren las rutas: “en el transcurso de tiempo que pasa entre que se prepara la licitación y se inicia la obra aparecen nuevos baches. Por esta razón, una vez adjudicados los trabajos, la empresa realizará un relevamiento con intervención de Vialidad para identificar los sectores más críticos en cada ruta al momento de la intervención”.  
  
“El gobernador Omar Perotti nos encomendó la tarea de mejorar cada camino de la provincia, y así lo estamos haciendo en rutas tan importantes como la ruta número 1, que es una arteria fundamental para la producción y el turismo; o la ruta número 40, que conecta los departamentos Vera y General Obligado", agregó.  
  
**LAS OBRAS**  
En la primera etapa del plan, la inversión provincial es de $92,4 millones y comprende tareas en el tramo de 27 kilómetros de la RP 62, entre la RP 1 y Arroyo Saladillo. También, en 8 kilómetros entre la localidad de Colonia Dolores y la Ruta Nacional N° 11.  
  
En relación a la RP 1, los trabajos de bacheos se desarrollarán en un tramo de 244 kilómetros entre Cayastá y Reconquista. Actualmente, el obrador se sitúa en la localidad de San Javier, avanzando en sentido hacia Alejandra.  
  
En tanto para el otro grupo de tareas licitadas, el presupuesto es de $78,8 millones y los tramos a intervenir son en la RP 40, en unos 79 kilómetros que conectan Reconquista con Fortín Olmos; la RP 32 íntegramente en el departamento General Obligado, desde Villa Ocampo hasta Villa Ana; y la RP 3, en el departamento Vera, sobre 55 kilómetros desde Garabato hacia la Ruta Nacional N° 11.