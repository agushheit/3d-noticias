---
category: La Ciudad
date: 2021-06-21T06:15:24-03:00
thumbnail: https://assets.3dnoticias.com.ar/BAJANTE.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: La bajante del Paraná es peor que la del 2020 y dejó una marca histórica
  en Santa Fe
title: La bajante del Paraná es peor que la del 2020 y dejó una marca histórica en
  Santa Fe
entradilla: Este domingo al mediodía prefectura registró una marca de 0,45 metros,
  la más baja en los últimos 50 años. Las proyecciones no son alentadoras.

---
Este domingo al mediodía, el Paraná midió en el puerto local 0,45 metros. Con esa altura, la actual bajante superó a la del año pasado cuando midió 0,48 metros. De esta forma, el río alcanzó el nivel mínimo de los últimos 50 años.

Para encontrar un registro menor hay que remontarse a 1971, cuando se anotó una altura de 0,42 metros; una medida que podría ser superada en los próximos días.

En apenas dos semanas, el Paraná descendió un metro en esta capital. El 6 de junio, prefectura midió 1,45 metros; catorce días después el hidrómetro local marcó 0,45 metros.

Días atrás, Juan Borús, subgerente de Sistemas de Información y Alerta Hidrológico del INA, adelantó a UNO Santa Fe que por la delicada situación hay "permanentes reuniones, tanto con organismos nacionales como de la región, analizando la situación, que viene pesada no solo en el corto plazo sino también a largo plazo".

Subrayó a este medio que, en términos generales, las trazas de pronósticos del INA para todas las secciones de los grandes ríos aparecen en su totalidad con valores sumamente bajos y el río Paraná en Santa Fe no escapa a eso.

Había advertido que "muy probablemente se registren valores por debajo de los valores que se registraron el año pasado", algo que finalmente ocurrió este domingo.

Y apuntó: "Muy probablemente en Santa Fe tengamos valores próximos a los 60 centímetros a fines de junio, lo que es la perspectiva a corto y mediano plazo. En un marco general, es muy probable que cuando termine el año y se hable de los promedios anuales de alturas en todas las secciones del río nos encontremos en muchos casos con valores medios anuales entre los dos o tres años más secos de toda la historia", concluyó.