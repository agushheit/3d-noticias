---
category: El Campo
date: 2021-01-13T08:03:29Z
thumbnail: https://assets.3dnoticias.com.ar/13121-CAA.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: '3DNoticias - Fuente: Télam'
resumen: Dejarán sin efecto el límite de 30.000 toneladas diarias para exportación
  de maíz
title: Dejarán sin efecto el límite de 30.000 toneladas diarias para exportación de
  maíz
entradilla: A partir del acuerdo alcanzado con  el CAA, se sustituirá el límite de
  30.000 toneladas diarias de exportación por un monitoreo del saldo exportable.

---
El Ministerio de Agricultura, Ganadería y Pesca decidió dejar sin efecto el tope de 30.000 toneladas diarias para el registro de Declaraciones Juradas de Ventas al Exterior (DJVE) de maíz, al llegar a un acuerdo con el Consejo Agroindustrial Argentino (CAA) que asegura el abastecimiento interno del grano.

En un comunicado difundido, la cartera que conduce Luis Basterra anunció que a partir del acuerdo alcanzado con la CAA, se sustituirá el límite de 30.000 toneladas diarias de exportación por un monitoreo del saldo exportable para llegar al empalme de cosecha sin tensiones, con el compromiso del sector privado.