---
category: Deportes
date: 2020-12-29T09:30:38Z
thumbnail: https://assets.3dnoticias.com.ar/2912-futbol-sin-fubtol.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Por Marisa Lemos
resumen: "   El fútbol sin fútbol"
title: "   El fútbol sin fútbol"
entradilla: Hace más de 8 meses que ningún estadio del Fútbol Argentino es pisado
  por sus hinchas, y la ausencia se hace notar en cada rincón de las tribunas y de
  los palcos.

---
El fútbol es un deporte que mueve multitudes y pasiones, eso ya lo sabemos todos. Pero, ¿qué significa la hinchada para los jugadores? Es una pregunta con miles de adjetivos, todos buenos, por supuesto, y sobre todo en el momento que estamos atravesando.

Hace más de 8 meses que ningún estadio del Fútbol Argentino es pisado por sus hinchas, y la ausencia se hace notar en cada rincón de las tribunas y de los palcos.

Cuando todo esto empezó, pensamos que era cosa de unos meses, que se iba a poder volver a las canchas lo más pronto posible, pero lamentablemente no fue así. Ahora, al ver un partido solo se escucha el eco,  las conversaciones que tienen los jugadores y el cuerpo técnico. Y ves ese lugar que ocupaste durante tanto tiempo totalmente vacío... y, por más que se escuchen los cantos a través de parlantes, vos sabés que NO ES LO MISMO.

¿Qué futbolero no extraña ir a la cancha? Quién no extraña las previas en la esquina del estadio, el olor a choris y hamburguesas, ver los colores del club de sus amores reflejado en miles y miles de banderas y camisetas, cantar a todo pulmón y gritar los goles hasta quedarse sin voz.

Ahora nos preguntamos: ¿cómo influye la falta de hinchada en los jugadores? Tiene un peso importante,  el fútbol genera emoción y el público es parte de la ecuación. El hincha es el que se sorprende, admira, insulta y vive el partido junto a sus jugadores, que deben estar muy preparados para sostenerse y actuar sin perder creatividad, naturalidad ni atrevimiento.

Lo cierto es que los jugadores crean un escudo protector para evadirse de esas emociones que vienen desde afuera, pero los cantos, y la energía que generan, los hacen sentir fuertes e invencibles. Y lo que más se siente es el amor hacia los colores del equipo.

**Es inconcebible un fútbol sin hinchas**, incluso para el negocio. Soy de la idea de que si hay que seguir jugando a puertas cerradas, sería mejor no hacerlo. Estamos en tiempos de pandemia y es preferible quedarse alentando desde nuestros hogares, con la misma energía que se deja en cada partido, en cada jugada, en cada grito de gol. Pronto se podrá volver, y se podrán sentir, otra vez, los escalofríos y esa buena vibra que se vive en cada rincón.