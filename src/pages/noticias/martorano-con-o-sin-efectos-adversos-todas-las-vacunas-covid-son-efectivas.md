---
category: Agenda Ciudadana
date: 2021-08-02T06:00:38-03:00
thumbnail: https://assets.3dnoticias.com.ar/VACUNATEWACHIN.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'Martorano: con o sin efectos adversos, "todas las vacunas Covid son efectivas"'
title: 'Martorano: con o sin efectos adversos, "todas las vacunas Covid son efectivas"'
entradilla: "¿Si no produce dolor ni otro síntoma es porque no sirve? Esa pregunta
  circula por estos días en que es posible recibir alguna de las 3 vacunas para +18"

---
La vacunación contra el Covid-19 avanza al ritmo de la llegada de nuevas dosis. Hasta ahora son tres los laboratorios que tienen presencia en el país y, en consecuencia, en la provincia para la población mayor de 18 años: Sputnik V (Gamaleya, de la que se aguarda la llegada de segundos componentes), AstraZeneca y Sinopharm. Y las tres tienen un grado alto de efectividad, más allá de los efectos secundarios que puedan ocasionar en quienes reciben el pinchazo.

¿Por qué la aclaración? A medida que fue descendiendo la edad de las personas inmunizadas se fueron conociendo reacciones más o menos adversas, desde dolor en la zona donde fue colocada la inyección, hasta síntomas como cansancio, escalofríos y fiebre. Cuadros que se presentan con mayor asiduidad en la franja etaria más joven respecto a la de mayor edad.

¿Por qué ocurre esa diferencia? ¿Si no hay reacción adversa es porque la vacuna tiene menos efectividad? Para despejar dudas y sumar datos, El Litoral dialogó sobre estos y otros temas con la ministra de Salud de la provincia, Sonia Martorano.

"Todas las vacunas contra Covid-19 son buenas", arrancó la ministra. "La primera dosis de Sputnik V otorga da un 77 % de protección y cuando se coloca la segunda se llega o supera el 95 %". En Sinopharm "se logra el 61 % con la primera dosis pero a las tres semanas se recibe la segunda y se llega al 88 %. Con AstraZeneca, la primera dosis proporciona un 78 % y con la segunda (a los 56 días) se llega al 89 ó 90 %. Es decir que están todas por encima de la media de protección".

\- Para que quede claro, ¿la efectividad de la vacuna tiene alguna relación con los efectos adversos que produce?

\- La efectividad de la vacuna no está relacionada con los efectos adversos. Sí notamos que la población joven es muy reactógena, pero eso tiene que ver con el sistema inmune que reacciona con más potencia. Una persona puede tener dos dosis de Sputnik V y no presentar efectos adversos, pero un control arroja un nivel altísimo de anticuerpos.

Hay gente que presenta más efectos que otra tras recibir la vacuna que sí es más reactógena en personas más jóvenes porque su sistema inmune reacciona rápidamente.

Por otro lado, las vacunas desarrollan inmunidad celular y generan memoria inmunológica. Más allá de la medición de anticuerpos hay memoria celular mediada por linfocitos. Así que siempre se desarrolla capacidad de defensa.

\- En las últimas semanas se observa que más gente se inscribió en el registro de vacunación (santafe.gob.ar/santafevacunacovid).

\- Si no fuera así, ya hubiéramos terminado con primeras dosis. Pero se van sumando cuando ya empezamos con la segunda y eso está muy bueno. Debo decir que no me sorprende porque Santa Fe tiene una larga historia de vacunación y tenemos inscriptos al 90 % de la población vacunable, cuando en otras provincias se anotó el 60 %.

Estamos notando que en los últimos días se inscribió mucha gente joven que tal vez no lo había hecho porque veían la vacunación como algo lejano. Eso está muy bueno y habla de solidaridad y empatía.

\- La posibilidad de que a futuro se habilite un pasaporte sanitario, ¿alienta a que más gente decida vacunarse?

\- Es probable, es algo de lo que se habla. Esta no es una vacuna obligatoria todavía pero estamos en una situación difícil con una emergencia sanitaria. La posibilidad está, veremos cuándo, cómo, para qué y con qué características.

\- Usted mencionó un tema central: la vacuna contra el Covid no es obligatoria. ¿podemos explicar por qué?

\- Es una vacuna que se hizo en una situación de crisis. Pensemos que en un año esta vacuna ya estaba con una fase 3 y con aprobaciones de emergencia en el mundo. Inicialmente no es obligatoria, lo cual no quiere decir que en un próximo paso no lo sea, pero eso se maneja a escala mundial.

Mientras tanto, vemos que este virus no tiene tendencia a extinguirse como otros y estamos notando una capacidad de mutación a mayor contagio y transmisibilidad. Como siempre decimos, es la capacidad que tiene el virus de cambiar para sobrevivir. Hablando en potencial, podemos pensar en generar vacunas anuales. Creo que vamos a apuntar a eso, como se hace, por ejemplo, con la gripe.

\- El operativo de vacunación tuvo un impulso fuerte desde junio.

\- Estábamos listos para ello, pero el operativo se aceleró ante la llegada de más dosis: hemos tenido días de 50 mil turnos en la provincia. Vamos colocando muy rápidamente con mucho apoyo de los equipos de salud y una rectoría del gobernador Omar Perotti que está constantemente mirando el monitor para poner sin pérdida de tiempo las vacunas en los brazos de la gente.

\- ¿Estiman que para agosto van a poder mantener el mismo ritmo de vacunación que en junio y julio?

\- Si llegan las vacunas, sí. Ahora tenemos la Moderna para iniciar con este grupo de jóvenes (de 12 a 17 años con factores de riesgo) y veremos cuál es la decisión para el remanente de dosis.

\- ¿Para Pfizer no hay fecha de llegada todavía?

\- Son 20 millones de dosis (por las que el gobierno nacional firmó el acuerdo). Es un lindo numerito. No está la fecha de llegada aún porque depende de algunos temas logísticos, pero se espera dentro de este año, antes de diciembre.

\- Les da tiempo de conseguir los freezer.

\- Si, ya está organizado. Son freezer de -80 así que es toda una decisión. Lo peor que nos puede pasar es que tengamos las vacunas y no esté la logística. Pero acá la logística está en marcha y vamos a estar preparados.

**La otra agenda**

En materia sanitaria, desde principios de 2020 el interés de todo el mundo parece estar puesto en el Covid-19, los contagios, la prevención y la vacunación. Sin embargo, hay otros temas prioritarios; algunos quedaron postergados por la pandemia y otros se van a impulsar antes de que termine este año.

Uno de esos temas prioritarios es el cumplimiento del calendario nacional de vacunación que descendió en todo el mundo. Así lo aseguró la ministra de Salud de la provincia Sonia Martorano quien situó ese descenso en un promedio del 20 %.

La razón puede encontrarse en el mismo efecto pandemia: durante el año pasado y debido a las restricciones y al temor a nuevos contagios por Covid menos gente asistió a hospitales y centros de salud para completar el esquema de vacunación, cuya cobertura en el país es muy buena. A esto se sumó la falta de presencialidad en las escuelas que postergó el control de los carnets de vacunación.

"Estamos trabajando muy fuerte en recuperar el calendario de vacunación", afirmó la ministra. La misma inquietud sobrevuela al resto de las carteras sanitarias del país.

Otro tema sobre el que se pondrá el foco es en las enfermedades crónicas, algunas de ellas derivadas, una vez más, de la pandemia. Es el caso de la obesidad que está ligada al sedentarismo. "Se va a volver a trabajar en alimentación saludable", uno de los ejes impulsados antes de que el Covid se llevara toda la atención.

Otro eje es Seguridad del paciente, consigna que la propia Organización Mundial de la Salud ubicó como prioritaria antes de la pandemia.

Además, se abordará una agenda conjunta con el Ministerio de Género, Igualdad y Diversidad a cargo de Celia Arena, en el marco del plan Cuidar Santa Fe, "sin descuidar el tema de la pandemia".