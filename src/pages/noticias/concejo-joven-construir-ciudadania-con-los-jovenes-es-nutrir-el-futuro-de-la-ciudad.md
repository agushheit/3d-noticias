---
category: La Ciudad
date: 2021-04-14T07:34:22-03:00
thumbnail: https://assets.3dnoticias.com.ar/concejo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Concejo Santa Fe
resumen: 'Concejo Joven: “construir ciudadanía con los jóvenes es nutrir el futuro
  de la ciudad”'
title: 'Concejo Joven: “construir ciudadanía con los jóvenes es nutrir el futuro de
  la ciudad”'
entradilla: Es un proyecto que se enmarca en el Plan Concejo Abierto, Participativo
  y Transparente. La presentación la realizó el presidente del Concejo Leandro González
  a directivos y docentes de distintas escuelas de Santa Fe.

---
Este martes se presentó en el recinto de sesiones y ante docentes, directivos y padres la propuesta denominada “Concejo Joven”. Dicha iniciativa (creada bajo Decreto N° 484/08), propone la integración activa de jóvenes estudiantes secundarios, en la actividad legislativa municipal. De esta manera, el proyecto busca involucrar a los estudiantes en la actividad propia del Concejo Municipal, experimentando ellos mismos el rol de “concejales” y “asesores” mediante actividades y encuentros con modalidades presenciales y virtuales.

El presidente del Concejo Municipal, Leandro González, comentó que se vienen comunicando con varias escuelas para poder llevar adelante esta experiencia que ya es tradición en el Concejo Municipal, “entendiendo la situación especial por la pandemia y priorizando los cuidados de los y las jóvenes ante cualquier otra cosa. Para nosotros es muy importante comenzar con esta propuesta que el año pasado no pudimos organizar a raíz de las restricciones por el Covid-19, hoy tomamos la decisión de comenzar en vinculación con 13 escuelas que han confirmado su participación, tanto públicas como privadas. Nuestro objetivo es construir ciudadanía y que los chicos y chicas, de entre 16 y 18 años, absorban conocimiento, se transformen en ciudadanos más formados y que interpelen a la ciudad con proyectos y propuestas concretas que hacen a la vida de Santa Fe”.

Por otra parte, Silvina Del Paso, de la Escuela N° 261 “José Hernández”, comentó que “es muy lindo este tipo de actividades porque los chicos vienen a trabajar acá, conocen y aprenden al venir a este ámbito del Concejo Municipal, para ellos es una experiencia única, ya lo hemos visto en otros años al trabajar con esta modalidad y es muy enriquecedora para la formación de los alumnos”.

En tanto, José Müller, representante de la Escuela N° 651, fue consultado sobre las expectativas por la propuesta y contestó: “en el marco de los espacios curriculares de los espacios de Formación Ética y Ciudadana, esperamos que los alumnos de 4to. año vayan entendiendo el funcionamiento del Concejo Municipal, que puedan diferenciar lo provincial de lo municipal y que puedan generar proyectos para su barrio, para su ciudad”.

En la oportunidad, estuvieron presentes los concejales y concejalas: Mercedes Benedetti, Lucas Simoniello, Carlos Suárez, Laura Spina y Valeria López Delzar.

Asimismo, participaron representantes de diferentes escuelas de la ciudad: Escuela Nº 261 “José Hernandez”; “Remedios de Escalada”; “Juana Azurduy”; EETS Nº 647 “Dr. Pedro Lucas Funes”; Escuela Nº 3098 “Juan Marcos”; E.E.M. N° 256 “General Juan Bautista Bustos”; Escuela 2° Bicentenario de la Patria; EESOPI 2035 “Santa Rosa de Lima”; CEF Nº 3184 “San Francisco”; Liceo Argentino de Navegación Fluvial Nº 8245; “Nuestra Señora de Guadalupe”; “La Salle Jobson”; EESOPI 3106 “Santa Rita de Casia”; Escuela 7 de Mayo.

**Plan Abierto, Participativo y Transparente**

Este proyecto pertenece a la propuesta Concejo 2021: Abierto, participativo y transparente. A través del Eje Participación se pretende generar alternativas de concientización favoreciendo la convivencia, promoviendo espacios de formación y de expresión a través de diferentes canales, con la finalidad de fomentar la visibilización del rol del Concejo Municipal y su cercanía a la ciudadanía.

El proyecto está destinado a fortalecer aspectos que impulsen el diálogo, el consenso, el compromiso ciudadano y la pertenencia de lo público como elementos esenciales.

La participación está organizada en base a tres líneas de acción que serán tenidas en cuenta según las características de cada uno de los proyectos propuestos. Ellas son:

* Sostenibilidad y riesgo hídrico
* Convivencia y espacio público
* Participación ciudadana y políticas públicas

Para conocer mas, ingresá a [https://www.concejosantafe.gov.ar/7853-2/](https://www.concejosantafe.gov.ar/7853-2/ "https://www.concejosantafe.gov.ar/7853-2/")