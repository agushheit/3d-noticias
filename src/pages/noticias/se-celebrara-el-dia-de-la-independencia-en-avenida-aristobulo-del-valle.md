---
category: Agenda Ciudadana
date: 2022-07-01T09:24:54-03:00
thumbnail: https://assets.3dnoticias.com.ar/Aristobulo-1200x1200.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: Se celebrará el Día de la Independencia en  Avenida Aristóbulo del Valle
title: Se celebrará el Día de la Independencia en  Avenida Aristóbulo del Valle
entradilla: 'En el marco de los festejos por el 9 de Julio, ese día desde las 10,
  habrá música, actividades culturales y recreativas, una gran oferta gastronómica
  y promociones en comercios. '

---
El sábado 9 de julio, en la avenida Aristóbulo del Valle se celebrará el 206º Aniversario de la Declaración de la Independencia Nacional. Con diferentes actividades, organizadas por la Municipalidad y la Asociación de Comerciantes del tradicional paseo, los santafesinos podrán disfrutar de una jornada distinta.

Desde las 10 de la mañana, vecinos y vecinas se podrán acercar al centro comercial a cielo abierto, donde se montarán puestos gastronómicos de comidas típicas a lo largo de ocho cuadras, entre Llerena y Castelli. Además, los locales ubicados en la avenida abrirán sus puertas para ofrecer promociones, sorteos y descuentos de hasta el 50% en todos los rubros.

En la esquina de Llerena y Aristóbulo del Valle se ubicará el escenario en el que se concretará el acto alusivo a la fecha Patria, con un concierto de la Banda Sinfónica Municipal y el Coro Ciudad de Santa Fe. En tanto, en la parroquia Nuestra Señora de Luján, se llevará a cabo el tedeum tradicional.

Posteriormente, subirán al escenario artistas como Las 2 Marías, Cordiales, Efraín Colombo, Rejunte Chango, el balet “Yapeyú”, “El Prado” y el Instituto de Arte Contemporáneo de Andrea Patiño. Además, la Fanfarria ambulante y Dixieband realizarán sus habituales recorridos, mientras que el cierre será a pura zumba.

Para los más chicos habrá una plaza blanda con juegos, en la que también se ofrecerán actividades lúdicas y recreativas para todas las edades. Del mismo modo, distintas áreas municipales ubicarán dispositivos de servicios como adopción de animales de compañía a cargo del IMUSA, un puesto de Ecocanje de reciclables por plantines y semillas, y capacitaciones de RCP dictadas por personal del Cobem.