---
category: Estado Real
date: 2021-08-07T06:00:09-03:00
thumbnail: https://assets.3dnoticias.com.ar/CASILDA.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: El gobernador Omar Perotti recorrió obras en los departamentos General López,
  Caseros y Belgrano
title: El gobernador Omar Perotti recorrió obras en los departamentos General López,
  Caseros y Belgrano
entradilla: "“Somos de hablar menos, pero de hacer; que los hechos hablen por nosotros”,
  dijo el mandatario provincial."

---
El gobernador Omar Perotti recorrió este viernes, las obras que se llevan adelante en la ruta nacional N°33, en el tramo comprendido entre las localidades de Chabás y Sanford. Además, visitó los trabajos de desagües cloacales, pavimentación, cordón cuneta y mejorado que se realizan en el barrio Malvinas Argentinas, de Las Parejas. En paralelo, entregó tres ambulancias de alta complejidad en esta última ciudad, además de Firmat y Casilda.

En la oportunidad, el titular de la Casa Gris remarcó la importancia de seguir de cerca las obras que se desarrollan en el territorio provincial. “No solamente la estamos monitoreando nosotros, sino también cada uno de ustedes”, aseguró, dirigiéndose a los ciudadanos del sur provincial. Y agregó: “Las vienen siguiendo desde hace años y eso está bien; ese control social es clave”.

Luego, señaló el valor de la recuperación de la ruta nacional 33: “Necesitamos mejorar las vías de comunicación y esta es fundamental. Primero, para garantizar el cuidado de la vida de quienes la transitan. Segundo, para movilizar la producción con otras posibilidades. Y tercero, para recuperar el valor de la palabra”.

En ese sentido, apuntó que “a pesar de lo complicado de lo administrativo, cuando hay voluntad se puede hacer. Seamos entonces todos custodios de esa obra, de quienes toman la decisión y lo están haciendo; no de los que hablan y las cosas no pasan”. Y remarcó: Somos de hablar menos, pero de hacer; que los hechos hablen por nosotros”.

**Ruta Nacional n°33**

En mayo de 2021 se puso en marcha la recuperación de la ruta nacional N°33, una obra inconclusa que había quedado detenida en 2018. Actualmente, se ejecutan obras de reparación en distintos tramos de la vía, entre Venado Tuerto y Casilda. Se trata de tareas de repavimentación, fresado y bacheo. También se intervienen los ingresos a las localidades de Sanford y Villada. El monto vigente del contrato es de 854.000.000 de pesos. La obra es financiada por el gobierno central, a través de la Dirección Nacional de Vialidad.

**En Las Parejas**

Más tarde, el gobernador visitó la obra de ampliación del sistema de desagües cloacales y optimización de la planta de tratamiento existente para el sector oeste de Las Parejas, en el Loteo Municipal 240, barrio Malvinas Argentinas, en la ciudad de Las Parejas. La obra demandó una inversión de provincial de 154.000.000 de pesos. Además, recorrió los trabajos de cordón cuneta y estabilizado granular que ahí se desarrollan, en un tramo de 3.210 metros, por un monto de 3.800.000 pesos, en el marco de fondos entregados por el programa de Obras Menores. Las obras beneficiarán a 240 viviendas.

Finalmente, recorrió la pavimentación de la vía de acceso oeste a la ciudad, en el sector comprendido por Avenida 17, entre ruta provincial N°178 y calle 6. Esta obra tiene un presupuesto de 18.000.000 de pesos y se ejecuta en el marco del Plan Incluir y el Promudi.

“Recorremos obras en cada una de las ciudades y pueblos de la provincia; obras en salud, en educación, en seguridad, rutas y obras hídricas, con el desafío de integrar territorialmente Santa Fe. Y esto nos entusiasma, porque es una tarea colaborativa con cada intendente y presidente comunal; algo que nos permitió también enfrentar de otra manera la pandemia”, remarcó Perotti, y destacó la obra pública “como generador de empuje en aquellos lugares donde el empleo es necesario; y donde todavía tenemos mucho para dar” añadió.

**Entrega de ambulancias**

Durante su recorrido por el sur provincial, Perotti entregó tres ambulancias de alta complejidad en las ciudades de Firmat, Casilda y Las Parejas, adquiridas para fortalecer el sistema de salud en el marco de la pandemia de Covid-19. En ese marco, indicó que “cuidar a los santafesinos y santafesinas” es el objetivo central de la gestión.

Y recordó que “las primeras 60 ambulancias que se han incorporado son todas de alta tecnología; una unidad de terapia intensiva móvil, una cama más que sumamos. Esto significa que si se nos completa la terapia y hay que trasladar a alguien; ya está en una cama crítica, ya está en una unidad de terapia intensiva cuando sale en la ambulancia hacia el centro de Salud”.

“Esta es una inversión de casi 10 millones por unidad. Y es la mejor inversión, porque sabemos que estamos cuidando a nuestra gente”, graficó el mandatario provincial.

Las tres ambulancias de alta complejidad entregadas este viernes, fueron destinadas al Hospital General San Martín de Firmat; al Hospital Provincial San Carlos de Casilda; y al Samco de Las Parejas. Previamente, el gobernador Omar Perotti recorrió también el vacunatorio que funciona en el Club Argentino de Firmat.

Presente en el lugar, la senadora nacional María de los Ángeles Sacnun remarcó “la importancia estratégica” del Hospital de Firmat, y agradeció el enorme y decidido esfuerzo de las y los trabajadores del lugar. “Este hospital estaba atrasado en materia edilicia, y el gobierno ha hecho un gran esfuerzo; es un hito histórico en materia de salud pública para la ciudad y para todo el sur santafesino”, destacó la legisladora nacional.

En ese contexto, cumpliendo con el decreto 6/21 votado por unanimidad en la Cámara Alta, la legisladora donó al hospital San Martín, un Otoemisor para hacer screening neonatal, un monitor fetal y también tres nuevas computadoras.

Cabe destacar que estos equipos médicos y tecnológicos devinieron de un común acuerdo entre el oficialismo y la oposición plasmado a través de un decreto impulsado por la vicepresidenta Cristina Kirchner, donde se estableció que los aumentos en las remuneraciones de los senadores nacionales y de las autoridades de la Cámara, sean destinados a instituciones de salud pública para colaborar ante la emergencia sanitaria.

**Presentes**

De la recorrida participaron también el senador provincial Eduardo Rosconi; el secretario de Integración y Fortalecimiento Institucional de Santa Fe, José Freyre; el secretario de Emergencias y Traslados de Salud, Eduardo Wagner; entre otras autoridades provinciales y locales.