---
category: La Ciudad
date: 2021-07-30T06:00:05-03:00
thumbnail: https://assets.3dnoticias.com.ar/MERCADO.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Tomás Rico para El Litoral
resumen: El Mercado de Productores cumple 40 años como servicio esencial de la ciudad
  de Santa Fe
title: El Mercado de Productores cumple 40 años como servicio esencial de la ciudad
  de Santa Fe
entradilla: El espacio de los productores y abastecedores de frutas, verduras y hortalizas
  nació en 1981 y consigo crecieron grandes barriadas santafesinas.

---
El Mercado de Productores y Abastecedores de Frutas, Verduras y Hortalizas de Santa Fe SA (ubicado en Teniente Loza al 6900) cumple este 29 de julio el 40° aniversario de su fundación en 1981. El espacio que ocupa el Mercado de Abasto se emplazó en el noroeste de la ciudad hacia principio de los 80 y desde entonces aquel gran rincón productivo tiene una dinámica función que no cesa.

Carlos Otrino, secretario de la sociedad que administra el mercado y uno de los dos directores ejecutivos, en una entrevista con El Litoral, recordó los inicios y el desarrollo de un polo estratégico y productivo tanto para la ciudad de Santa Fe y su área metropolitana, como para provincias vecinas y países limítrofes.

El mercado se fundó en junio de 1980, con la finalidad de construir las nuevas instalaciones que conformarían, además de su sede social, el nuevo Mercado de Abasto. El 29 de julio de 1981, el por entonces intendente de la ciudad de Santa Fe el farmacéutico Roberto Casís (1981-1982), sucesor en el cargo del Coronel Miguel Alfredo Coquet, fue quien inauguró el Mercado.

"Para acceder al Mercado había una sola mano asfaltada sobre Teniente Loza y otra mano de tierra. En los alrededores había muy poca población", recordó Otrino y agregó: "Muchos de los barrios de alrededores recibieron la migración y se fueron construyendo y generando espacios de urbanización, y los barrios crecieron a la par del mercado". En la actualidad, el abasto se convirtió en una de las principales fuentes de trabajo para los barrios San Agustín I y II y Yapeyú.

En sus inicios el mercado tenía una lógica diferente del trabajo tradicional y buscaba modificar la forma de carga de los puesteros. "Con el tiempo eso no se pudo modificar y hoy en día muchos puesteros siguen llevando la carga a sus clientes. Uno de los datos ´folclóricos\` del viejo mercado era que a los compradores se los ubicaba y conocía por la patente de su vehículo, entonces esos números de las ´chapas\` quedaron en la memoria de la gente del mercado, y hoy día el mismo comprador continúa haciendo referencia a esos viejos apodos", mencionó el secretario del Mercado.

La labor de los productores y puesteros del mercado de frutas y verduras, es una función que trasciende generaciones y las familias de aquellos pioneros continúan su obra. Al respecto Otrino señaló: "Hay muchos casos de familias de inmigrantes, desde polacos, italianos, españoles hasta árabes. Muchos que empezaron a trabajar con sus puestos ya conocían este comercio, la aplicación empírica del binomio oferta y demanda".

**Una vida junto al mercado**

En el mercado la actividad es muy dinámica y permite llegar a dos millones de personas. Foto: Pablo Aguirre

"Desde los 18 años que ya andaba en el mercado. Antes era muy distinto en cuanto al trabajo en sí, cada puestero tenía que entregar la mercadería a los clientes sin importar la cantidad de cajones que vendiera", mencionó Miguel Andretta, presidente del Directorio y Director Ejecutivo del Mercado, quien recordó que el primer trabajo que le encomendaron fue ir a cobrar a Paraná. "En ese entonces el Mercado de Paraná se abastecía del nuestro y en esa época yo iba a cobrar el resumen de cuenta mensual, se cobraba una vez al mes; hoy es una cosa imposible", rememoró.

Sergio Caballero, ex presidente, vice presidente y actual Prosecretario del Directorio, está vinculado al Mercado de Abasto desde 1988. "Siempre con el mismo espíritu y la alegría de ir al mercado con ganas de disfrutar del trabajo", se sinceró y agregó que a esta pasión la puede compartir con sus hijos que trabajan en el lugar, "eso es una alegría porque hay una relación laboral muy buena y la relación familiar se afirma".

**A dos millones de personas**

"La actividad de los mercados transfiere muchísimas toneladas de mercadería para abastecer una amplia región. En el caso de nuestro mercado llega a los dos millones de personas y tiene una actividad muy dinámica", sostuvo Otrino.

La expansión y la llegada de la fruta y verdura provista por los productores y empresas, nacionales, internacionales y locales varió según sus épocas, tal es así que según el secretario durante fines de los '80 y en la década del '90, con menores condiciones para el transporte, los productos recorrían grandes distancias hasta llegar a sus compradores. "Antes se hacían viajes hasta Encarnación (Paraguay), por ejemplo. Se iba hasta Misiones, Corrientes, Formosa, y los compradores que venían -y muchos vienen aún hoy, por supuesto- a eso se le llamaba ´hacer la línea\`, lo que implicaba recorrer toda una línea que recorría los pueblos y ciudades que bordeaban y cruzaban la frontera del país, incluso. Todo eso con la velocidad (modernización) de los vehículos más algunas rutas que se han hecho en el norte de la Argentina, hicieron que bajen los tiempos de distribución, lo cual restó un poco los alcances del mercado".

Sin embargo, Otrino reconoció que "seguimos teniendo gente que viene de Corrientes, de Chaco, de la zona sur de provincia de Buenos Aires como Bahía Blanca. Nuestra área de influencia, asumiendo que el área metropolitana de Santa Fe tiene un millón de habitantes, más la llegada al norte de la provincia de Santa Fe, a Santiago del Estero, parte de la provincia de Córdoba y Entre Ríos, tenemos unos dos millones de personas abastecidas".

"En el mercado todos los que perduramos es porque trabajamos conscientemente y con honestidad tanto para con el cliente como para el productor. También es importante tener una mano de obra de buena calidad, hay que mantener todo aceitado para que la maquinaria esté en condiciones", sostuvo Caballero.

**Avances para la comercialización**

Uno de los aspectos que más lo impacta al presidente del Mercado es el avance de la tecnología en todas sus facetas: "En lo que hace a la comercialización, en los años 70 para comunicarse a un lugar de producción había que esperar 10 días, y hoy con los celulares tenés la información al instante y las distancias son nulas. El transporte también evolucionó mucho, antes la mercadería venía en ferrocarril y hoy tenés camiones impresionantes", contó.

Otro de los cambios importantes es la presencia de mujeres como trabajadoras de un ambiente históricamente de hombres. "Las mujeres que había eran las cafeteras, era raro ver una mujer trabajar en el mercado. Hoy es común verlas al frente de un puesto, hay empleadas", indicó Andretta.

Adaptarse a las nuevas formas de trabajo es fundamental para sostenerse en el rubro. El Prosecretario del Directorio destacó que "a través de los años hay muchas modificaciones. Hoy los pedidos se hacen por WhatsApp y en esta pandemia eso ayudó mucho. El cliente te hace cambiar las reglas de juego y nos llevó a cambiar nuestra forma de trabajar, la actividad más grande se desarrolla antes de la venta".

**Puesteros e infraestructura**

"Los fundadores del mercado eran 87 socios y había 116 puestos en total, es decir que había puestos para alquilar. Había unos 44 galpones en dos tandas, a los costados de la nave principal que tiene 25 mil metros cuadrados. Esos galpones almacenaban cajones vacíos, mercadería en tránsito, o eran para maduración o conservación en cámaras frigoríficas", rememoró Otrino.

La infraestructura del lugar fue en crecimiento y hoy cuenta con 87 galpones con una alta capacidad de frío instalada. Actualmente, la cifra de puestos de venta se conserva (116), y se estiman unos 600 trabajadores y 148 sectores de playa libre destinados a los quinteros y rematadores.

**Perspectiva**

\- ¿Hacia dónde apunta el Mercado en los próximos años? ¿Tiene para crecer?

\-Carlos Otrino (CO): Los mercados, en el mundo, tienen una vigencia porque son plataformas, más allá de la logística interna o externa, que acercan productos de todo el país y de cualquier lugar del mundo. Por ejemplo, en nuestro Mercado se venden pomelos israelíes, cubanos, naranjas españolas, kiwis neozelandeses, es muy grande la capacidad de recibir productos de distintos lugares. Por eso, me parece que no llegamos a un techo, sí creo que debemos modernizarnos y planificar el mercado de los próximos años con la capacidad de generar un master plan consciente y reproducible, que no va a ser posible sin créditos. Hoy la economía de los mercados no permite abarcar grandes proyectos con recursos propios, se necesita financiamiento de varios millones de pesos.

\- ¿Hay algunas ideas sobre lo que se puede mejorar?

\-CO: Por ejemplo, sacar los vehículos de adentro de la nave principal, que para la época en que se hizo el mercado era normal y ahora ya no lo es tanto. Generar plataformas logísticas de carga rápida y que ya la pandemia ha puesto en vigencia, ya que multiplicó muchísimo la venta por canales digitales, especialmente el WhatsApp, donde los puesteros entre lunes y jueves suben los productos que tienen y el comprador ya puede consultar el lote que va a necesitar y buscar sus productos de forma rápida.

**Trabajo solidario en inundaciones y con el banco de alimentos**

El Mercado de Productores, casi sin quererlo, se ubicó en un punto estratégico de Santa Fe y evitó que sus instalaciones sean arrasadas por el agua cuando el Salado quería crecer y desbordarse. "Los terrenos pertenecían a lo que era la zona del Hípico y tienen la cota más alta de la ciudad. Cuando se produjo la inundación del 2003, el agua llegó hasta lo que sería hoy el vínculo de Teniente Loza antes de empezar a bajar al viejo Frigorífico Municipal", mencionó el secretario.

En épocas de grandes inundaciones en la ciudad, el abasto se acercaba a los barrios damnificados. "Visitábamos la Vuelta del Paraguayo, la zona de la Ruta 1, en La Guardia y Alto Verde, donde entrábamos por una ´lonjita\` de tierra y a un costado el río tomaba gran parte de la barriada", contó Otrino.

La idea de estar cerca en momentos críticos fue un puntal que nunca abandonaron los productores y hasta el día de hoy permanecen activos con acciones solidarias. "En 2016, a través de un programa de la Bolsa de Comercio de responsabilidad social, surgió el germen de generar una acción solidaria. La idea se materializó en el Banco de Alimentos Santa Fe (Basfe)", señaló Otrino, quien fuera vicepresidente de aquella primera Comisión del Basfe.

En 2018 comenzó a tomar mayor potencia el Basfe y se establecieron dos días a la semana para que los voluntarios rescaten frutas y verduras de los productores del Mercado. "Son alimentos que ya no tienen valor comercial, como por ejemplo una pera que está manchada o un choclo que tiene mustia la chala, pero dentro está perfecto. Ahí es cuando actúa el Banco de Alimentos para el rescate de estos productos aptos para el consumo humano".

Esta acción trascendente tiene muchas aristas que son interesantes y que generan múltiples beneficios, no solo para llevar alimentos a la población vulnerable, sino que también para reducir el impacto ambiental. "Al ser rescatados bajan la cantidad de productos que se descartan como residuos y que van al relleno sanitario", sostuvo Otrino.

El Basfe ya cuenta con un galpón dentro del Mercado, cedido en comodato de cinco años, donde se acondiciona la mercadería. "Es algo progresivo y hay que ir paulatinamente creciendo. Ya hay más de 90 entidades sociales que reciben el alimento que el Banco rescata de nuestros puesteros", resaltó uno de los directores ejecutivos del Mercado de Abasto.