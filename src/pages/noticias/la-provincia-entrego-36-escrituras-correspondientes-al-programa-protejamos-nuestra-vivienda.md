---
category: Estado Real
date: 2021-09-08T06:00:13-03:00
thumbnail: https://assets.3dnoticias.com.ar/VIVIENDA.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Provincia entregó 36 escrituras correspondientes al programa "protejamos
  nuestra vivienda"
title: La Provincia entregó 36 escrituras correspondientes al programa "protejamos
  nuestra vivienda"
entradilla: El acto se realizó en El Molino, de la ciudad capital. Tiene como objetivo
  resguardar la vivienda de cualquier ejecución por deudas posteriores a su afectación.

---
El Secretario de Justicia, Gabriel Somaglia junto a la Directora del Registro de la Propiedad Zona Norte, Mariela Fregona, entregaron este martes 36 escrituras gestionadas a través del Programa “Protejamos Nuestra Vivienda”.

En el marco de la entrega Somaglia detalló: "Protejamos Nuestra Vivienda está comprendido dentro del Dispositivo Santa Fe + Cerca, desde la secretaría de Justicia y por decisión del gobernador Omar Perotti, con la idea de que aquellas personas que tienen una única vivienda puedan protegerla de los créditos que por distintas contingencias no pueden cumplir, su inmueble es inembargable".

Y agregó: “La nueva legislación del Código Civil Nacional lo prevé, desde el gobierno provincial y el Registro de la Propiedad se hace el trámite para que sea válido frente a la comunidad. Para nosotros no solo es estar cerca de la sociedad, sino brindarle herramientas de protección a la vivienda, reconociendo que en muchos casos es difícil el acceso a la misma”.

Somaglia además, destacó la labor de los empleados del Registro de la Propiedad, que a pesar de la pandemia estuvieron predispuestos para realizar todos los trámites.

Consultada sobre el tiempo que lleva realizar el trámite, Fregona indicó que “es breve, pero con el escenario sanitario se demoró un poco más. Desde abril hemos alcanzado más de 50 escrituras afectadas y lo más importante de destacar es que la protección comienza el día de presentación del trámite”.

**Detalles del trámite**

El trámite es gratuito y se realiza en el Registro General de la Propiedad, siendo ya más de 50 los hogares que gozan ahora de la protección que da este instituto.

El Programa Protejamos Nuestra Vivienda, de la Secretaria de Justicia, tiene como objetivo resguardar la vivienda de cualquier ejecución por deudas posteriores a su afectación, la afectación es una protección jurídica destinada a resguardar la vivienda familiar frente a las adversidades económicas.

**Para acceder al trámite son necesarios los siguientes documentos:**

DNI de la o las personas titulares del inmueble

DNI de la o las personas beneficiadas

Título de propiedad original

Última boleta del Impuesto Inmobiliario (API)

Documentación que acredite el vínculo entre las personas afectantes y las beneficiadas (Actas de Matrimonio, Uniones Convivenciales, partidas de nacimiento, etc.)

Quienes son los beneficiarios:

Propietario de la vivienda.

Su cónyuge o conviviente.

Sus padres, hijos, abuelos o nietos (ascendientes o descendientes).

Sus hermanos, tíos o primos (parientes colaterales dentro del tercer grado) que convivan con el propietario.