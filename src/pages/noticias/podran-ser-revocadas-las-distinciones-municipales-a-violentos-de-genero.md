---
category: Agenda Ciudadana
date: 2021-04-15T06:45:08-03:00
thumbnail: https://assets.3dnoticias.com.ar/concejo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Diario Uno
resumen: Podrán ser revocadas las distinciones municipales a violentos de género
title: Podrán ser revocadas las distinciones municipales a violentos de género
entradilla: Se incluyó perspectiva de género en la ordenanza del Concejo Municipal
  que otorga distinciones tales como "santafesino Ilustre", "santafesino destacado",
  "visitante ilustre" y "huésped de honor".

---
El Concejo Municipal de Santa Fe modificó la ordenanza Nº 10228 que otorga las distinciones de "santafesino Ilustre", "santafesino destacado", "visitante ilustre" y "huésped de honor" a ciudadanos y ciudadanas de la ciudad. Se incluyó en todos sus artículos la condición de "no encontrarse condenado, no haber reconocido su culpabilidad en juicio abreviado en materia penal o haber acordado la suspensión del juicio a prueba en actos dolosos o culposos graves; no encontrarse inscripto en el Registro de Deudores Alimentarios Morosos; ni tener orden de distanciamiento vigente ordenada por tribunal de familia por conductas referidas a violencia de género en cualquiera de sus tipos y modalidades según la Ley provincial Nº 13.348 de Protección integral para prevenir, sancionar y erradicar la violencia contra las mujeres en los ámbitos en que se desarrollen sus relaciones interpersonales y su decreto reglamentario".

Asimismo se impone que el o la nominada "deberá tomar conocimiento que su designación podrá ser observada por la ciudadanía, y que en caso de ser designado deberá mantener sus conductas libres de las situaciones descriptas en el inciso anterior". Y se agrega que en caso de infringirlas "podrá ser pasible de sufrir la revocación de su distinción; y en caso de su revocación renuncia a la presentación posterior de cualquier reclamo por daños y perjuicios contra el Cuerpo, cualquiera de sus miembros o la Municipalidad".

En la modificada ordenanza, se indica que toda solicitud recibida "observando la designación o solicitando la revocación de distinciones otorgadas por esta Ordenanza deberá ser fundada y acompañada por una Organización No Gubernamental". Luego la Comisión Permanente de Desarrollo Social, Cultura, Educación, Salud, Derechos Humanos, Género y Diversidad, analizará la procedencia del pedido y elaborará un dictamen aceptando o rechazándolo. Este organismo podrá convocar a una Audiencia a los iniciadores del expediente y al candidato propuesto o distinguido, según sea el caso, los cuales serán oídos individualmente y luego se pasará a dictaminar sobre la procedencia o no de la distinción o su revocación.

Al mismo tiempo, se garantiza que en los casos previstos vinculados las violencias de género "se contemplará la posibilidad de condicionar la revocación de la distinción, a la realización de un curso afín a la Ley Micaela, al de nuevas masculinidades, o similares".

Una de las concejalas que trabajó en esta ordenanza fue Valeria López Delzar que, al ser consultada por UNO Santa Fe sobre cómo surge la iniciativa de modificar la ordenanza, relato: "Fue a partir de una consulta a fin del 2020 que nos hicieron desde un grupo profesionales (es el estudio jurídico social Lazos compuesto por las abogadas Jaquelina Bussi y Erica Stalker, la psicóloga Jimena Rudi y la trabajadora social Maria de los Ángeles Pais) que acompañó un caso puntual de la ciudad de Santa Fe donde el artista conocido como Niño de Cobre (Andrés Iglesias), que obtuvo una distinción en 2018, había llegado a una probation que es un acuerdo de suspensión de juicio a prueba para no ir a la instancia de juicio penal por distintos delitos que estaban atravesados por violencia de género. Nos hicimos eco de esta situación".

![](https://assets.3dnoticias.com.ar/nino-cobre-andres-iglesiasjpg.jpg)

"Nos llevó a rever las distinciones. Porque es una institución como el Concejo Municipal, que entrega una distinción a esta persona por su trabajo artístico, pero que ya no se vea solo lo laboral sino que se tenga en cuenta estos comportamientos o actitudes violentas que se empeñó, que en este caso además fue un delito. Hicimos una reunión con estas profesionales que llevaron adelante el caso, nos trasladaron propuestas concretas de incorporar esta posibilidad de revocación de las distinciones ante hechos puntuales. Fue una reunión todas concejalas mujeres, sororas, donde tenemos esta sensibilidad con los temas de género y nos parecía importante incorporarlo en la ordenanza para trabajarlo en comisiones. Alí también participaron todos los concejales que aportaron sus observaciones, y se trabajó de manera conjunta en los distintos bloques. Se trabajó a partir de un proyecto de Jorgelina Mudalell, sobre la que sumamos aportes", agregó la concejala.

Asimismo destacó los incisos de requisitos para obtener una distinción o que la misma sea revocada. "Nos parece un avance importante. Lo que esta modificación que surgió a partir de este caso concreto es que incorporó a una ordenanza la perspectiva de género", sostuvo.

Al ser consultadas si iniciarán el proceso para revocar la distinción a Iglesias, desde el Estudio Lazos detallaron a UNO que "se está analizando" aunque no hay definiciones. Celebraron el cambio de la norma argumentando que "es importante no separar al artista de la persona, porque lo personal es político" y que "es por lo que venimos luchando las feministas". López Delzar por su parte confirmó a este medio que "por el momento no se ha presentado ninguna revocación de alguna distinción".