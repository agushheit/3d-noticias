---
category: La Ciudad
date: 2021-02-13T07:15:01-03:00
thumbnail: https://assets.3dnoticias.com.ar/aumentobondi.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Ya se debita el aumento de más del 46 % en los colectivos urbanos de Santa
  Fe
title: Ya se debita el aumento de más del 46 % en los colectivos urbanos de Santa
  Fe
entradilla: Este viernes finalmente se comenzó a debitar la suba que dejó la tarifa
  plana de los colectivos urbanos en $ 42,35 que rige en la ciudad.

---
Este viernes finalmente se comenzó a debitar la suba que dejó la tarifa plana de los colectivos urbanos en $ 42,35, que rige en la ciudad desde el pasado lunes pero que por problemas con la actualización del sistema Sube no se había podido implementar.

Este viernes finalmente se comenzó a debitar el aumento de más del 46 % del boleto del colectivo urbano, que rige en la ciudad desde el pasado lunes pero que por problemas con la actualización del sistema Sube no se había podido implementar.

La suba es de 46,54 %, por lo que la tarifa plana pasó de los $ 28,90 a los $ 42,35. En tanto, el boleto centro quedó en $ 33; el boleto escolar en $ 18,80; la tarifa para jubilados en $ 23,50; y el boleto terciario-universitario $ 28,20.

Cabe destacar que se trata de la primera suba en 16 meses, ya que el último había sido en septiembre de 2019, cuando el entonces intendente José Corral habilitó la última suba. Finalizado aquel año y comenzado el 2020, las tarifas volvieron a congelarse por la crisis ocasionada por la pandemia de coronavirus hasta principios de este mes.