---
category: Agenda Ciudadana
date: 2021-08-25T06:15:35-03:00
thumbnail: https://assets.3dnoticias.com.ar/DELTARELOADED.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Hernán Álvarez. El Litoral
resumen: Confirmaron siete casos de la variante Delta en Rosario
title: Confirmaron siete casos de la variante Delta en Rosario
entradilla: Son viajeros que arribaron de Japón, España y Estados Unidos. Cumplen
  con una cuarentena estricta que es supervisada por las autoridades sanitarias.

---
Los siete primeros casos de coronavirus variable Delta (mutación producida en la India) en Rosario fueron confirmados a El Litoral por el secretario de Salud Pública local, Leonardo Caruana. Todos son viajeros provenientes del exterior arribados al aeropuerto internacional de Ezeiza que hacen por estos días un aislamiento estricto y monitoreado por las autoridades sanitarias.

“Llevamos hasta el día de la fecha 1.879 personas monitoreadas (residentes en Rosario). De ellas 25 dieron PCR positivas de las cuales siete dieron positivas con la variante Delta”, señaló Caruana. La procedencia de los contagiados con la variante hindú arribó desde Japón, Estados Unidos y España, según afirmó el secretario. Remarcó que todos los pasajeros contagiados siguieron el aislamiento correspondiente. “Todos desarrollaron un cuadro leve, muchos de ellos asintomáticos”, dijo. Estos trabajos se realizan básicamente para intentar retrasar lo más posible la circulación comunitaria.

Los resultados positivos fueron enviados al Instituto Malbrán de Buenos Aires y allí se determinó que estas siete personas fueron contagiadas con la variante asiática. “Esta cepa es mucho más transmisible, mucho más contagiosa. Si las personas hacen un encuentro social, lo más probable es que el número de casos hubiese cambiado. Y esto no fue así”. Lo que se sabe de esta variedad es que es más transmisible, pero no más letal. “Es muy importante tratar de retrasar la circulación comunitaria y llegar a un porcentaje mayor de cobertura de segundas dosis”, agregó. Como siempre, Caruana recalcó las medidas de cuidado como el barbijo y el distanciamiento social para evitar el contagio de covid-19.