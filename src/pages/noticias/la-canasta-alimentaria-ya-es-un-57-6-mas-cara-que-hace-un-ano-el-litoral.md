---
category: Agenda Ciudadana
date: 2021-07-23T06:00:52-03:00
thumbnail: https://assets.3dnoticias.com.ar/CANASTA.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: La canasta alimentaria ya es un 57,6% más cara que hace un año EL LITORAL
title: La canasta alimentaria ya es un 57,6% más cara que hace un año EL LITORAL
entradilla: Con menos de $ 9.195 al mes para comer, un adulto no puede cubrir los
  requerimientos mínimos -al menos según la estadística- y se considera un “indigente”.

---
La inflación en alimentos es mayor a la general; la angustia del bolsillo se impone a la campaña de la vacunación, que tampoco resulta como el gobierno pretendía. Los indicadores sociales ganan peso relativo en el clima preelectoral, y no como producto de especulaciones opositoras sino como resultado de realidades socioeconómicas.

Durante junio de 2021, la variación mensual de la canasta básica alimentaria (CBA) con respecto a mayo de 2021 fue de 3,6%, mientras que la variación de la canasta básica total (CBT) fue de 3,2%. Así lo informó en la tarde del jueves el Indec.

Las variaciones interanuales de la CBA y de la CBT resultaron del 57,6% y 51,8%, respectivamente. A continuación, en el cuadro 1, se presenta el cálculo mensual de la canasta básica alimentaria y la canasta básica total del adulto equivalente para el período junio 2020-junio 2021.

Según detalla el organismo estadístico, la canasta básica alimentaria (CBA) se ha determinado tomando en cuenta los requerimientos normativos kilocalóricos y proteicos imprescindibles para que un varón adulto, entre 30 y 60 años, de actividad moderada, cubra durante un mes esas necesidades.

Se seleccionaron los alimentos y las cantidades en función de los hábitos de consumo de la población a partir de la información provista por la Encuesta Nacional de Gastos de los Hogares (ENGHo).

Según el informe, la Canasta Básica Total para un adulto equivalente, es de $ 21.517 según el Indec. El drama de la pobreza en la Argentina se entiende si se compara la cifra con los 23.064 pesos de una jubilación mínima a junio de este año, o el salario mínimo de $ 27.216 a partir del 1 de julio ( $ 28.080 desde el 1 de agosto y $ 29.160 a partir del 1 de septiembre).

Para una familia con dos adultos y dos menores, el costo de la Canasta Básica Alimentaria alcanza $ 28.414 y el de la Canasta Básica Total $ 66.488. Los enormes esfuerzos fiscales en planes de asistencia explican que no haya desbordes en una economía que apenas recuperó en parte lo que perdió en la pandemia.

Las cifras deberán actualizar el drama del 42% de población por debajo de la línea de pobreza en la Argentina, en el segundo semestre del pasado año. El tablero de la realidad exhibe además 10,2% de desocupación, una caída del 5% en el índice de producción manufacturera en mayo, un retroceso del 2% en el mismo mes para el Estimador Mensual de la Actividad Económica (anticipo del PBI) y una inflación de 50,2% interanual a junio.