---
category: La Ciudad
date: 2021-01-12T09:52:11Z
thumbnail: https://assets.3dnoticias.com.ar/12121-jaton-gastronomicos.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: '3DNoticias - Fuente: El Litoral'
resumen: Reunión con el sector gastronómico
title: Reunión con el sector gastronómico
entradilla: El intendente Emilio Jatón mantuvo este lunes una reunión con representantes
  del sector gastronómico para ajustar los protocolos sanitarios y de trabajo.

---
Tras la reunión, el secretario de Producción y Desarrollo Económico de la Municipalidad, Matías Schmüth, señaló que «hay que ir pensando nuevas estrategias en conjunto» ante el actual contexto de pandemia y las nuevas disposiciones que se van adoptando. 

En ese sentido, dijo: «nos comprometimos a trabajar en conjunto para armar una campaña de concientización que apele a la conducta individual pero también a la conducta de los empresarios para que los protocolos, que son muy buenos, se cumplan y la gente se sienta segura de ir a consumir a un lugar que cuida a su personal y a los comensales».

En el encuentro, los referentes de bares y restaurantes plantearon la necesidad de que los empleados cuenten con una autorización para poder movilizarse entre el horario de cierre y la llegada a sus domicilios. El tema va a ser abordado con los estamentos pertinentes.

**Las restricciones son entre las 0.30 y las 6 horas de lunes a viernes, inclusive; y de 1.30 a 6 horas los sábados, domingos y feriados.** 

Además, se recuerda que, desde el 2 de enero, la Municipalidad amplió la capacidad de comensales por cada mesa en los locales gastronómicos de la ciudad, llevándola a 6 personas. También autorizó a ocupar el 50% de la superficie en los espacios cerrados, debiendo garantizar la adecuada ventilación y cumplimiento de los protocolos.

**<br/>**

## **PARTICIPANTES**

De esa reunión de esta mañana participó también la secretaria de Control y Convivencia Ciudadana del municipio, Virginia Coudannes. En tanto, por la Asociación Empresaria Hotelera Gastronómica de Santa Fe, junto con la Cámara de Bares y Heladerías, estuvieron de forma virtual Rodolfo y Rodrigo Verde, Leonidas Bonaveri, Carlos Fertonani, Rodrigo Gayá, Agustín Macinsky, Luis Hediger; y Demetrio Alvarez, en representación del gremio de los Gastronómicos de Santa Fe.