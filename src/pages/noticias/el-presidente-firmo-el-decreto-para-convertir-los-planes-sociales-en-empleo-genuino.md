---
category: Agenda Ciudadana
date: 2021-10-19T06:15:50-03:00
thumbnail: https://assets.3dnoticias.com.ar/albert.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: El Presidente firmó el decreto para convertir los planes sociales en empleo
  genuino
title: El Presidente firmó el decreto para convertir los planes sociales en empleo
  genuino
entradilla: El objetivo es que "los distintos programas de empleo, inclusión laboral
  y desarrollo socio-productivos se transformen en mecanismos que incentiven la incorporación
  de estos trabajadores al empleo asalariado registrado".

---
El presidente Alberto Fernández firmó este lunes el Decreto 711/2021 para convertir los planes sociales en empleo genuino y registrado en el sector privado, transformando las prestaciones en "incentivos para la contratación" en forma de "parte integrante del salario", informaron fuentes oficiales.  
  
En ese sentido, los beneficios se considerarán "parte integrante del salario respectivo en forma total o parcial, en la forma, plazo y condiciones que se determinen para cada sector de actividad", dice el decreto, que se publicará en las próximas horas en el Boletín Oficial.  
  
Uno de los objetivos principales del Gobierno nacional es que los distintos programas de empleo, inclusión laboral y desarrollo socio-productivos destinados a personas desempleadas o con trabajos precarizados "se transformen en mecanismos que incentiven la incorporación de estos trabajadores al empleo asalariado registrado o a otros modos de desarrollo de actividad productiva ajustados a las formalidades tanto registrales como tributarias", indica el escrito en sus considerandos.  
  
Para cumplir este objetivo se requerirán modificaciones en el Programa de Inclusión Socio-Productiva y Desarrollo local "Potenciar Trabajo" y del Programa de Inserción Laboral creado por el Ministerio de Trabajo.  
  
El Decreto, de ocho artículos, dispone a los Ministerios de Desarrollo Social y de Trabajo a realizar modificaciones en sus programas de formación, empleo e intermediación laboral, "con el objetivo de convertir las diferentes prestaciones de asistencia a personas desempleadas o con trabajos precarizados en incentivos para la contratación de sus beneficiarios y beneficiarias bajo la forma de empleo asalariado registrado en el sector privado".  
  
La prestación se considerará "parte integrante del salario respectivo en forma total o parcial, en la forma, plazo y condiciones que se determinen para cada sector de actividad".  
  
Además, el beneficiario mantendrá el derecho al cobro de la prestación asistencial durante la vigencia del período de instrucción que realicen los citados ministerios o se aplicará lo previsto en el artículo anterior, cuando la práctica implique la incorporación a la planta de personal del empleador y/o de la empleadora.  
  
Producida la discontinuidad del contrato de trabajo, las personas beneficiarias tendrán la posibilidad de "volver a percibir la asistencia" que establece el programa, si el número de cotizaciones al sistema de Seguridad Social no supera los 12 meses dentro de los dos años anteriores al cese del contrato laboral.  
  
En el caso de que el período de cotizaciones al sistema de Seguridad Social se encuentre entre los ocho y doce meses dentro de los dos años anteriores al cese del contrato laboral, los trabajadores podrán optar entre la posibilidad de reingresar al programa de origen o acceder a la prestación por desempleo.  
  
No podrán acceder a ninguno de los programas que se establezcan los empleadores que figuren en el Registro Público de Empleadores con Sanciones Laborales.  
  
El Presidente había anticipado la medida el viernes pasado en el 57° Coloquio del Instituto para el Desarrollo Empresarial de la Argentina (IDEA), en el Centro Costa Salguero de la Ciudad de Buenos Aires, donde dijo que "si algo caracterizó al espacio político que hoy gobierna Argentina es la promoción de la inversión empresaria y la creación de empleo".  
  
"Con ese mismo espíritu actuamos hoy, y por eso estamos disponiendo a través de un decreto marco un sistema que posibilite que los que hoy son beneficiarios de planes puedan ser empleados en la actividad privada sin perder ese derecho", enfatizó aquella vez, porque se necesita "impulsar el empleo registrado".  
  
Reseñó que tres actividades económicas ya han avanzado en cumplir ese objetivo: "Primero fue en la ruralidad donde impulsamos que quienes tienen planes se conviertan en trabajadores rurales tan importantes a la hora de cosechar. Después avanzamos en la misma senda en la construcción. Y dimos otro paso en el mismo sentido con la actividad gastronómica y hotelera", resumió Fernández.  
  
"Cambiar planes por empleo de debe ser nuestro primer objetivo. De ese modo estaremos combatiendo frontalmente a la pobreza que hoy ha sumido a millones de familias argentinas", remarcó.