---
category: Deportes
date: 2020-12-16T12:55:41Z
thumbnail: https://assets.3dnoticias.com.ar/basquet.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'FUENTE: DIEZ EN DEPORTES'
resumen: Colón hizo oficial la vuelta del básquet
title: Colón hizo oficial la vuelta del básquet
entradilla: Este martes se hizo el lanzamiento de la "refundación" de este deporte
  en la institución, que regresa después de casi un año. Acá todos los detalles.

---
**El resurgir del básquet.** Así titularon algunos a la iniciativa de la dirigencia de refundar uno de sus deportes, que había quedado de lado en un momento y que, con caras nuevas y aportes renovados, busca nuevamente emerger. ​Fue así como este martes por en el gimnasio Roque Otrino, se realizó la conferencia de prensa para anunciar su activación después de un año.

Con la ausencia por razones de fuerza mayor del presidente José Vignatti, la reunión estuvo encabezada por el vicepresidente José Alonso. "Para nosotros es muy importante el regreso de una disciplina que marcó una historia a lo largo de nuestra vida", puntualizó el directivo.

Seguidamente, el manager Sebastián Svetliza puntualizó que "el proyecto es ambicioso, estamos con muchas energías y se viene un Campus que será el primero, porque sobrepasamos los cupos que teníamos previstos. Vamos a organizar otro en enero, como también un circuito 3x3 en la ciudad".

[![Twitter](https://assets.3dnoticias.com.ar/colon-tweet-basquet.jpg)](https://twitter.com/ColonBasquet/status/1338984545316638725?s=20 "Twitter")

Roberto Monti, titular de la Asociación Santafesina y Adrián Alurralde, por la Municipalidad de Santa Fe, también dijeron presente y, además de agradecer la invitación, felicitaron a los dirigentes que pudieron plasmar este anhelado regreso.

En la conferencia de prensa estuvieron presentes también exjugadores y dirigentes del club, como también gran parte de los jugadores de Primera División que volverán a vestir la camiseta sangre y luto, como también el staff de entrenadores que se harán cargo de todas las categorías: Carlos Mono Vázquez, Diego Vicino, Santiago Pipi Vesco y Diego Tuosto.