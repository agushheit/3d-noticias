---
layout: Noticia con imagen
author: "Fuente: LT10"
resumen: Nuevas actividades habilitadas
category: Agenda Ciudadana
title: Cuáles son las nuevas actividades que se habilitarán en Santa Fe
entradilla: Al participar de una actividad en el sur provincial, el gobernador
  Perotti dio detalles y dijo que firmará el decreto esta semana.
date: 2020-11-17T14:07:58.357Z
thumbnail: https://assets.3dnoticias.com.ar/22-10-20-19.jpg
---
Desde la semana pasada se espera que el gobernador Omar Perotti firme el decreto con nuevas habilitaciones que fueron quedando pendientes en el marco del aislamiento por coronavirus.

Si bien la próxima etapa culmina el 29 de noviembre y esta fase fue prácticamente una continuidad de la anterior ya que Santa Fe sostuvo casi las mismas medidas por recomendaciones del gobierno provincial ante el incremento de casos, se espera que esta semana haya nuevas habilitaciones.

Así lo manifestó Perotti en un acto en la localidad de Funes donde comentó que las nuevas aperturas son para las actividades deportivas al aire libre, más actividad náutica, casinos, hipódromo y jardines de infantes que vienen siendo anunciadas, pero resta la firma del decreto provincial.

En ese sentido, adelantó que “tenemos una autorización nacional para todos los deportes grupales para más de diez personas; van a ser parte de todo el protocolo que se incorporará a partir de diciembre”. Y a continuación confirmó que “en la semana seguramente tendremos el decreto firmado”. Estos son rubros que podrían empezar a trabajar en diciembre.

El mandatario mostró preocupación por el aumento de casos en la capital provincial y el departamento La Capital ante el aumento de casos por coronavirus y aclaró que “mientras en algunas zonas estemos por arriba de ese porcentaje, seguiremos habilitando por regiones”.