---
category: La Ciudad
date: 2021-02-02T05:18:35-03:00
thumbnail: https://assets.3dnoticias.com.ar/inl.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Con información de Diario Uno
resumen: La UNL descarta la presencialidad masiva y prepara un sistema bimodal con
  distinciones
title: La UNL descarta la presencialidad masiva y prepara un sistema bimodal con distinciones
entradilla: 'Así lo anticipó su rector, Enrique Mammarella. La virtualidad o la concurrencia
  a las unidades académicas dependerá del tipo de carrera y del nivel de avance de
  los estudiantes. '

---
En el marco del inicio del ciclo lectivo 2021 en las universidades, la UNL diagrama su esquema educativo 2021 distinguiendo a sus estudiantes en tres niveles según estén en un ciclo inicial, intermedio o superior de la carrera.

Dependiendo de esto y del tipo de carrera, se definirá la presencialidad o virtualidad en las clases; una cuestión que inquieta no solo a estudiantes, sino a las familias de los mismos, especialmente de quienes son del interior provincial y que deben mudarse a las ciudades que cobijan a las diferentes facultades.

En diálogo con UNO Santa Fe, el rector de la alta casa de estudios adelantó cómo la universidad se prepara para una vuelta a clases especial, luego de un 2.020 con aulas vacías por la pandemia. Enrique Mammarella anticipó que "las clases masivas no serán presenciales" pero "aquellas carreras que tienen actividades experimentales sí se va a tratar de lograr la presencialidad".

Graficó que en Ciudad Universitaria hay 13.000 puestos para estudiar; sostuvo que reducir la circulación a la mitad de personas, y "pensar en alrededor de 6.000 personas en la ciudad universitaria, también sería complicado".

_-¿Qué balance hace del ciclo lectivo 2020 con la nueva normalidad?_

El 2020 fue un año muy difícil. De ninguna manera pudimos suplir varias actividades en las que la presencialidad es necesaria, como lo son muchas de nuestras prácticas que son fuertemente dependientes de esto, ya sea por equipamiento, instalaciones o forma de aprendizaje. Esas condiciones están dadas solo dentro de la universidad, entonces son muy pocas las carreras que se pudieron suplir en la virtualidad.

_-¿Cuáles fueron las carreras más afectadas por la falta de presencialidad en las clases?_

Las carreras que tienen más actividades experimentales, como lo son: medicina, veterinaria, ingeniería agronómica, ingeniería química, que son las que tienen mucho laboratorio o talleres. Tal vez las menos afectadas sean las carreras mas teóricas, o las que las prácticas se puedan desarrollar en sus casas también. Todas las actividades que estaban planificadas en este tipo de carreras se hicieron, si bien se ha perdido esa instancia colectiva de discusión o de debate que en el aula es muy rica.

_-¿Se pudo avanzar con provincia y las universidades en la presencialidad de las prácticas profesionales supervisadas y de investigación?_

Hoy todas las universidades de la provincia estamos esperando la firma de un acta desde provincia con el Ministerio de Salud y el Ministerio de Trabajo que nos permita volver a esas actividades esenciales y presenciales de forma ampliada a partir del 1 de febrero, este lunes. Obviamente hay que programarlas y diagramarlas en tiempo y espacio, teniendo en cuenta la simultaneidad de acciones que se harán en el edificio de la universidad, de forma progresiva.

_-¿La Universidad está preparada para un regreso a la presencialidad?_

Seguramente las clases masivas no serán presenciales, van a ser utilizando la virtualidad. Pero aquellas carreras que tienen actividades experimentales sí se va a tratar de lograr la presencialidad. En Ciudad Universitaria hay 13.000 puestos para estudiar. Hoy, aún usando el 50% de las instalaciones, pensar en alrededor de 6.000 personas en la ciudad universitaria al mismo tiempo sería complicado. Es muy difícil que se vuelva a la presencialidad de forma masiva a corto plazo. Hoy no hay organización sobre como será el transporte, los horarios, sumado a la seguridad en horarios de ingreso y salida, donde van a esperar, etc

_-¿Cómo se diagraman las clases teniendo en cuenta a los estudiantes del interior y sus familias que deben resolver el tema de alquileres y que están condicionadas a la presencialidad o no de las clases?_

Cada carrera tendrá una diagramación diferente. Desde la UNL se está pensando dividir las carreras en tres etapas: ciclo inicial, intermedio y superior. Para un alumno de ciclo superior volver a iniciar un alquiler en alguien que está en la etapa final de su carrera es muy difícil y no lo queremos perder. Se busca achicar el tiempo en el que deban estar en la ciudad, viniendo un día por semana o una semana en todo el cuatrimestre, lo que les permitiría no pensar en un alquiler.

Para los ingresantes 2021, se está evaluando implementar una instancia presencial de ingreso a partir del 12 de abril, acorde a las condiciones y novedades que puedan surgir.

Los del grupo intermedio están en el corazón de la carrera y son los que están viviendo en Santa Fe, o que no levantaron el alquiler porque todavía les falta. Son los que queremos que la bimodalidad de teoría en la virtualidad y práctica presencial esté garantizada, volviendo de a poco a la normalidad. Los demás tendrán condiciones especiales.

_-¿Cómo evalúa el diálogo con el gobierno nacional y provincial?_

Tenemos diálogo con la Secretaría de Políticas Universitarias, que es más directa que el ministro Trotta. Se depende mucho de cosas que son interjurisdiccionales, y una de las cosas puntuales es el ciclo lectivo. Por suerte Santa Fe y Entre Ríos finalizan las clases ambas el 31 de marzo, aunque podrían haber elegido el 30 de abril. Un 30% de nuestros estudiantes son de Entre Ríos, entonces si Santa Fe o Entre Ríos cambia las condiciones es complejo de resolver.

_-¿Se considera que el personal docente y no docente de la universidad esté incluido en el calendario de vacunación?_

Sí, es importante. En este momento estamos trabajando con la Secretaría de Políticas Universitarias junto al ministerio de Salud de la provincia para que en la medida en que haya disponibilidad de vacunas poder vacunar al personal docente y no docente tanto de nivel inicial, primario, secundario y universitario al mismo momento.

_-A nivel presupuestario ¿Cómo se encuentra la universidad para el ciclo 2021?_

No es el presupuesto que la UNL desearía. Nos tenemos que acomodar, dejando de realizar muchas actividades que no tienen financiamiento. Este año al saber el límite presupuestado nos adecuamos a eso para funcionar con las actividades que se puedan planificar.

Presupuesto 2020 no hubo, por lo que fue el presupuesto 2019 reconducido que no sabíamos que disponíamos para las actividades. Por lo tanto adecuar todas las actividades universitarias por la pandemia, el sistema de virtualización, buscar todos los cuidados para el personal presencial, se tuvo que hacer sin saber si tendríamos o no los fondos.