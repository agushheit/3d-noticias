---
category: Estado Real
date: 2021-04-10T06:00:21-03:00
thumbnail: https://assets.3dnoticias.com.ar/MESAEMERGENCIA.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Perotti encabezó un nuevo encuentro de la mesa de trabajo para la emergencia
title: Perotti encabezó un nuevo encuentro de la mesa de trabajo para la emergencia
entradilla: 'Es un espacio para debatir los desafíos de la provincia con representantes
  de entidades de los sectores productivo, social, de los trabajadores, académico-científico
  y ambiental. '

---
El gobernador Omar Perotti encabezó este viernes un nuevo encuentro de la Mesa de Trabajo para la Emergencia, un espacio para debatir los desafíos de la provincia con representantes de las entidades de los sectores económico-productivo, social, de los trabajadores, académico, científico y ambiental.

Durante el encuentro, el gobernador de la provincia precisó que “hemos tomado decisiones comunes a todo el territorio de la provincia respecto a la pandemia. Hay un criterio común de tratar a la provincia como una sola en esta instancia inicial, porque queremos tomar todos los recaudos. Las características que se han visto en la velocidad con la que se aceleran los contagios es diferente a la primera ola. Y allí no queremos sorpresa, queremos que estén todos con la guardia bien alta para esta situación”.

En ese sentido, Perotti afirmó que “la única manera de poder sostener el nivel de actividad y empleo es que tomemos conciencia de los cuidados reales y concretos. Allí hay una dirección inequívoca del gobierno de la provincia de generar certezas en momentos de incertidumbres. Por eso, gestionar e inyectar 26.500 millones de pesos a los sectores productivos es una señal clara, así como también sostener obra pública y atacar las causas de desarrollo, generando oportunidades en toda la provincia es otra”.

Para finalizar, el gobernador les solicitó a los presentes “sumar propuestas por comisiones para poder mantener este nivel de actividad, de recuperación de empleo y de cuidado de nuestra gente de la mejor manera. Entendemos que el tema salud se lleva la parte central pero no hay que descuidar los otros aspectos. Necesitamos que quienes estén con un poco más de distancia sobre esa coyuntura nos ayuden a pensar mejor y ver otras opciones”.

**Construcción colectiva**

Por su parte, el ministro de Gestión Pública, Marcos Corach, afirmó: “Estamos convencidos que a partir del diálogo y de la construcción colectiva es cómo se sale de esta crisis. Por eso, vamos a sostener este espacio a lo largo del año y vamos a trabajar de manera conjunta con todos ustedes, intercambiando opiniones y pensando para tratar de levantar un poco la mirada de esta coyuntura, que es tan compleja, pero sin olvidarnos las prioridades para el futuro”.

Por otro lado, la ministra de Salud, Sonia Martorano, realizó un repaso de la situación epidemiológica de la provincia con las zonas de mayor riesgo, riesgo medio y menor. Y explicó cómo avanza el esquema de vacunación y la llegada de nuevas dosis.

El ministro de Seguridad, Jorge Lagna, presentó los nuevos lineamientos de su cartera y detalló que se inició un proceso de modernización normativa. Además, recalcó que tenemos financiamiento para profundizar este programa de seguridad en dos planos: el recurso humano y el equipamiento y tecnología. Por último, precisó el trabajo de las mesas de seguridad local, donde se diagrama la seguridad de cada ciudad de acuerdo a sus características.

Por su parte, el secretario de Asuntos Estratégicos, Francisco Buchara, destacó el trabajo realizado durante el año pasado y resaltó la importancia de llevar una agenda común para este año con el fin de abordar, mediante proyectos concretos, los futuros desafíos de la provincia.

**Presentes**

Participaron también del encuentro los ministros de Producción, Ciencia y Tecnología, Daniel Costamagna; y de Gobierno, Justicia, Derechos Humanos y Diversidad, Roberto Sukerman.

En representación del sector económico-productivo provincial, asistieron integrantes de Adeessa, AER, Apyme Santa Fe, BCR, BCSF, CAC Rosario, CAC Santa Fe, Carsfe, Coninagro, FAA, Feceso, Fisfe y UIA Joven.

En representación del sector de los trabajadores, participaron integrantes de Luz y Fuerza Rosario, Movimiento Sindical Rosario, CGT Santa Fe, CGT Rafaela, CGT Reconquista, CGT San Lorenzo y CTA Santa Fe.

Por el sector social, estuvieron presentes integrantes de la Iglesia Católica Rosario, Iglesia Católica Santa Fe, Iglesia Evangélica Rosario, Consejo Evangélicos de Santa Fe, Consejo Evangélicos de Rafaela, DAIA Santa Fe, DAIA Rosario, Movimiento Los Sin Techo, Federación Santafesina de Entidades Mutuales "Brigadier Estanislao Lopez" y Federación de Mutuales de la provincia de Santa Fe.

Por el sector académico y científico, participaron representantes de la UNR, UNL, UNRAF, UTN Santa Fe, UTN Reconquista; UTN Venado Tuerto; y de la Universidad Austral.