---
category: Estado Real
date: 2021-01-09T11:23:50Z
thumbnail: https://assets.3dnoticias.com.ar/090121-levante-veda-pesquera.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Se levantó la veda pesquera total en territorio santafesino
title: Se levantó la veda pesquera total en territorio santafesino
entradilla: La pesca comercial se habilita durante tres días, y se permite la recreativa
  con devolución en todo momento. «Sabemos que hay que preservar el recurso, pero
  nunca atentar contra la paz social», dijo la ministra Gonnet.

---
La ministra de Ambiente y Cambio Climático, Erika Gonnet, junto a los secretarios de Turismo, Alejandro Grandinetti; de Agroalimentos, Jorge Torelli; y de Políticas de Inclusión y Desarrollo Territorial, Fernando Maziotta, confirmaron este viernes la resolución de la Cámara Civil y Comercial de Rosario que habilita la pesca comercial durante tres días (lunes, miércoles y jueves) y permite la modalidad deportiva con devolución, conforme la propuesta oportunamente realizada por el gobierno provincial.

«Desde el Poder Judicial nos comunicaron que se resolvió lo que la provincia había recomendado, que era suspender la veda a la pesca deportiva, y sumarle dos días de veda a la comercial», explicó Gonnet, y aclaró que de esta manera se coincide con la reglamentación que tiene la provincia de Entre Ríos.

Al respecto, detalló que la actividad comercial se podrá realizar tres días por semana (lunes, miércoles y jueves), y estará prohibida martes, viernes, sábados, domingos y feriados. En tanto que la pesca deportiva, con devolución obligatoria, se liberó por completo y se podrá practicar en todo momento.

«Celebramos esto y lamentamos mucho estos días que tuvimos de conflicto», señaló la ministra y añadió: «queremos agradecer la celeridad que ha tenido la Justicia en contestar la apelación, y también por tener en cuenta las recomendaciones que hacía la provincia en cuanto a las medidas que se debían tomar».

Asimismo, Gonnet dijo que el fallo del 29 de diciembre «fue una medida muy imprudente, por eso agradecemos que, a través de nuestra apelación, se haya fallado con las recomendaciones de la provincia. Y no podemos dejar de mencionar que lamentamos profundamente lo que tuvieron que vivir los santafesinos y santafesinas al querer transitar por las diferentes rutas de la provincia».

Finalmente, evaluó que la medida judicial previa atentó «contra lo social y económico». En ese sentido, ahondó: «sabemos que hay que preservar el recurso, pero nunca podemos atentar contra la paz social, y contra los recursos productivos de un sector tan afectado como el turístico».

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.1em; font-weight: 700">POSITIVO PARA EL TURISMO </span>**

Por su parte, Grandinetti señaló que «uno de los atractivos fundamentales que tenemos en la provincia es la pesca deportiva, y ya había una ley que establecía que la pesca deportiva es con devolución» y aclaró que vuelve a entrar en vigencia la ley que «establece que se puede pescar deportivamente todos los días con devolución. El daño era muy grande, y ahora se va a reparar, gracias a que se actuó con celeridad».

En tanto, Torelli dijo que el Ministerio de Producción, Ciencia y Tecnología «venía intensificando los controles a través de puesta en valor de los puestos de fiscalización, donde el pescador entrega su pescado y se genera un documento, que es la guía que permite entrar luego a los frigoríficos, para que lo que entra sea lo mismo que lo que se certifica».

«Veníamos trabajando con las fuerzas de seguridad provinciales en los distintos retenes, controlando no solamente la documentación, sino la carga y las medidas (de los pescados)», indicó el secretario de Agroalimentos y profundizó: «tenemos casi 700 kilómetros de costa y es complicado controlarla toda, pero recibiendo las denuncias podemos constatar si existe un ilícito y tomar acción al respecto. Y vamos a reforzar eso por pedido del juez».