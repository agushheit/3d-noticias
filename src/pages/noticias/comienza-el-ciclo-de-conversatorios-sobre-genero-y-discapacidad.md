---
category: La Ciudad
date: 2021-04-18T06:15:39-03:00
thumbnail: https://assets.3dnoticias.com.ar/Cartelera.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Comienza el ciclo de conversatorios sobre género y discapacidad
title: Comienza el ciclo de conversatorios sobre género y discapacidad
entradilla: El primer encuentro se realizará de manera virtual este lunes. La temática
  versará sobre la relación entre violencia y discriminación por motivos de género
  y discapacidad.

---
La Municipalidad de Santa Fe dará inicio al ciclo “Conversatorios sobre género y discapacidad”, cuyo primer encuentro se denomina “Acceso a educación y empleo. Relación con violencia y discriminación por motivos de género y discapacidad. Ajustes razonables”. La propuesta se desarrollará este lunes 19 de abril, a las 17.30 horas, con acceso libre y gratuito a través de la plataforma Google Meet.

En el primer encuentro se plantea la necesaria interseccionalidad entre mujer y discapacidad, con el objetivo de poder analizar la situación educativa y laboral de quienes se encuentran en esta condición, a fin de remover las barreras que impiden la posibilidad de ejercer con plenitud los derechos a la educación y el trabajo. Para abordar la problemática, las expositoras en el conversatorio serán Soledad Gelvez y Tamara Mena de Movimiento Mírame Bien; Evelyn Safón de Asami y Marcela Papini de Movimiento de Acción Colectiva.

La propuesta está destinada a personas interesadas en temas de género y discapacidad, familias con personas con discapacidad, integrantes de organizaciones, trabajadores del Estado, de universidades y colegios de profesionales, además de público en general. Para informes e inscripción, dirigirse a: programa.accesibilidadyderechos@gmail.com.

La iniciativa se inscribe en las distintas actividades impulsadas por el municipio en el marco del Mes de las Mujeres y Disidencias, en este caso a través de la Dirección de Mujeres y Disidencias y la Subdirección de Accesibilidad y Derechos de las Personas con Discapacidad. Además, es coorganizada con Movimiento Mírame Bien, Asociación Santafesina de Miastenia Gravis (Asami) y Movimiento de Acción Colectiva.

**Una nueva perspectiva**

Gabriela Bruno, subdirectora de Accesibilidad y Derechos de las Personas con Discapacidad definió que el abordaje de la interseccionalidad mujer y discapacidad debe plantearse desde una perspectiva de género y en conjunto con organizaciones de la sociedad civil.

Al respecto, indicó que uno de los objetivos del encuentro es “pensar y diseñar políticas públicas desde otro lugar, incluyendo esta perspectiva, pensando acciones que puedan visibilizar, trabajar y abordar a la altura de las circunstancias esta situación, ya no desde el modelo médico rehabilitador sino desde el modelo social desde donde se plantea que la discapacidad no es una situación en un cuerpo sino que es producto de una relación con lo social, un contexto, que tiene que ver con las formas de relacionarnos, de vincularnos, con las formas de diseñar una ciudad, desde atención al público hasta la creación de distintos programas y acciones”.

Bruno planteó además que los espacios de trabajo, lugares de disputa y lucha de las mujeres y disidencias con discapacidad difieren de los lugares donde otras mujeres dan batalla y visibilizan las situaciones de desigualdad: “Por eso es necesario hablar de esta temática en particular, poder trabajarla en específico y poder pensar a la mujer con discapacidad como mujer trabajadora. Es un desafío, ya que históricamente se las ha pensado como beneficiarias de programas o destinatarias de pensiones nacionales no contributivas, beneficiarias de los cuidados y no en roles activos donde sean quienes pueden dar los cuidados, quienes puedan armar sus propios proyectos de vida y estar llevando diferentes tareas en igualdad de condiciones, donde lo que se necesita y debe trabajarse son cuáles son los apoyos necesarios para que esto ocurra o sea posible”.

**Cómo sigue el ciclo**

El ciclo de conversatorios prevé tres encuentros. Además del inicial donde se abordarán aspectos asociados a los derechos a la educación y el trabajo, habrá otros dos. En abril, la propuesta es desarrollar temas sobre acceso a la salud, derechos sexuales y reproductivos. Con ese objetivo se planteará la autonomía, el consentimiento, el derecho a la intimidad, a la privacidad, al placer y al deseo, que para Bruno “son históricamente temas tabúes con relación a las mujeres con discapacidad y disidencias en particular”.

El tercer encuentro del ciclo de conversatorios se llevará a cabo en mayo en donde la participación política de las mujeres y disidencias con discapacidad será el tema central junto a la importancia de tejer redes para el fortalecimiento del colectivo y alianzas con los feminismos para la ocupación de lugares representativos en la toma de decisiones y generar estrategias para la incidencia en materia de política pública.