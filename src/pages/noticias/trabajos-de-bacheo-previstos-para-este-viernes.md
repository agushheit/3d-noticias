---
category: La Ciudad
date: 2021-04-30T11:11:38Z
thumbnail: https://assets.3dnoticias.com.ar/bacheo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad de Santa Fe
resumen: Trabajos de bacheo previstos para este viernes
title: Trabajos de bacheo previstos para este viernes
entradilla: La Municipalidad informa las calles por las que avanza el cronograma de
  tareas previsto en el Plan de Bacheo 2021. Puede haber cortes de circulación y desvíos
  de colectivos a medida que se desarrolla la obra.

---
En enero pasado, la Municipalidad puso en marcha un plan de bacheo para reparar más de 12.000 metros cuadrados de calles de la capital provincial. El programa se desarrolla por fases, priorizando las zonas de la ciudad que evidencian mayor circulación.

En la zona del micro y macrocentro, está previsto que los trabajos se concreten en:

* 4 de Enero y La Rioja
* Francia, entre Hipólito Yrigoyen y Vera
* Gobernador Crespo, entre San Martín y San Jerónimo
* Hipólito Yrigoyen, entre Francia y Saavedra
* Hipólito Yrigoyen, entre Saavedra y San Lorenzo
* Eva Perón, entre Francia y Urquiza
* Eva Perón, entre 1° de Mayo y 4 de Enero  
* Eva Perón, entre San Martín y San Jerónimo
* 4 de Enero, entre Eva Perón e Irigoyen Freyre
* La Rioja, entre 4 de Enero y Urquiza
* San Jerónimo, entre La Rioja y Tucumán
* San Jerónimo, entre Corrientes y Moreno

En tanto, en avenidas troncales, la obra continuará por:

* Aristóbulo del Valle y French, mano norte-sur
* Aristóbulo del Valle, entre avenida Gorriti y Los Paraísos, mano sur-norte

El municipio informa que todas las tareas se realizan siempre en sentido del tránsito. Del mismo modo, indica que puede haber cortes de circulación y desvíos de colectivos, a medida que avanza la obra y se realizan intervenciones mayores.

Además, se detalla que la realización de los trabajos está sujeta a las condiciones climáticas.

**Por el operativo de vacunación**

La Municipalidad comunica que, en el marco del operativo de vacunación que se realiza en la ciudad, habrá controles en el tránsito vehicular en las inmediaciones de:

* La Esquina Encendida, en la intersección de Facundo Zuviría y Estanislao Zeballos, de 8 a 18 horas