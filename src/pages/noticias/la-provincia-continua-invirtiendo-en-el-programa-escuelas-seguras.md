---
category: Estado Real
date: 2021-01-18T09:00:48Z
thumbnail: https://assets.3dnoticias.com.ar/educacion.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: La Provincia continúa invirtiendo en el programa Escuelas Seguras
title: La Provincia continúa invirtiendo en el programa Escuelas Seguras
entradilla: Entregó aportes a escuelas de los departamentos General Obligado, Vera,
  San Javier y Garay por casi 7 millones de pesos.

---
El secretario de Educación, Víctor Debloc, visitó la escuela 6111 de Colonia La Lola, en el departamento General Obligado, donde se entregaron aportes para 31 escuelas de Calchaquí, Los Amores, Villa Ocampo, La Gallareta, Helvecia, Campo Stangaferro, Las Palmas, Paraje El Empalme, Los Lapachos, Campo 7 Provincias, Campo Pícoli, Campo Hardy, El Timbó, Fortín Olmos, Cañada Ombú, Campo Huber, Colonia Alejandra, Reconquista, Vera y Avellaneda por un monto total de $ 6.931.271,02.

Las partidas provenientes del Fondo Asistencia Necesidades Inmediatas (FANI) serán destinadas a construcción de sanitarios, cubierta de techos e impermeabilización, adquisición de equipamiento para aulas, equipos informáticos, equipamiento de cocinas y comedores, dispenser sanitizante, ventiladores, matafuegos y cortadoras de césped.

En el acto de entrega de los fondos, Debloc manifestó que "de los más de 3800 edificios escolares que tiene la provincia hemos encontrado, cuando asumimos la gestión, que muchos tienen una situación deficiente desde el punto de vista edilicio. Por lo tanto hemos decidido realizar una inversión progresiva y permanente con aportes provinciales  y nacionales, para ponerlas en las mejores condiciones”.

En referencia al ciclo lectivo 2021, Debloc precisó que “teniendo en cuenta la curva epidemiológica, que cambia semana a semana, hemos analizado una propuesta inicialmente para los grupos prioritarios, 7° grados de la escuela primaria y 5° y 6° años de las escuelas secundarias y técnicas, que contempla una semana de presencialidad en jornada reducida y con grupos pequeños y una semana de aprendizaje en los hogares”.

Más adelante, el funcionario hizo mención a la importancia de la escuela como lugar de contención y al alcance de la iniciativa Verano Activo: “Hay algo que quedó evidenciado en este tiempo que llevamos de pandemia, la escuela es el lugar público más seguro y que mejor protege la vida de las niñas, niños, adolescentes y jóvenes. En este sentido, estamos llevando adelante Verano Activo, que es una propuesta para acercarnos con actividades lúdicas, recreativas, deportivas y culturales a las pibas y pibes que han estado un poco más desconectados y alejados de las diferentes propuestas que les fueron acercando las y los docentes a lo largo de todo el territorio provincial”.

**El desafío de la gestión**

Por último, el secretario de Educación manifestó el eje que motoriza la gestión educativa provincial y expresó que “muchas veces uno se pregunta cuál será el patrimonio activo más importante de una sociedad, seguramente el trabajo de la gente  es lo más importante, pero aun así, nosotros en Educación tenemos el mayor patrimonio, cuidar a las generaciones nuevas, que tienen menos recursos que nosotros, menos experiencia y que algunos y algunas no fueron los suficientemente amados y cuidados. En este caso somos los adultos, que tenemos recursos simbólicos para entender que está pasando, comprender las situaciones y actuar en consecuencia para que ese universo pueda mejorar, reelaborar las normas que no tienen o que sólo pudieron transgredir. Por eso cuando hay educadores habrá alumnos y una herencia para trasmitir, si la herencia se transmite efectivamente habrá herederos que son los y las alumnas. El esfuerzo es para que no haya desheredados, alumnos que no hayan recibido todo lo que sabemos y construimos para ellos. Ese es el gran desafío y la fuerza que mueve nuestra gestión”.

![](https://assets.3dnoticias.com.ar/educacion1.jpeg)

**Detalle de los aportes**

>> Primaria Bilingüe 1378 Calchaquí - $ 1.100.000

>> Primaria 6046 Los Amores - $ 53.631,37

>> Jardín 153 Avellaneda - $ 44.844

>> AESO 3451 Avellaneda - $ 60.000

>> Especial 2035 Vera - $ 48.000

>> Primaria 6380 Reconquista - $ 52.440

>> Primaria 849 Villa Ocampo - $ 111.000

>> Primaria 6064 La Gallareta - $ 1.659.907,34

>> EETP 457 Helvecia - $ 1.652.150

>> Primaria 425 Helvecia - $ 1.180.900

>> Primaria 318 “Bernardo O'higgins” Campo Stangaferro - $ 114.377,37

>> Primaria 6074 “Julio Bruno Migno Parera” Las Palmas - $ 20.078

>> Primaria 1127 “Los Siete Jefes” Paraje El Empalme - $ 26.999

>> Primaria 6311 “Eduardo Sívori” Los Lapachos - $ 14.400

>> Primaria 1091 “Martin Miguel De Güemes” Campo 7 Provincias - $ 69.400

>> Primaria 6330 “Clmte. Tiburcio Aldao” Campo Pícoli - $ 41.522,27

>> Primaria 6111 “Martin Miguel de Güemes” Colonia La Lola - $ 88.799,39

>> EEMPA 6020 Reconquista - $ 18.705

>> Primaria 588 “Lanceros del Sauce” Reconquista - $ 23.600

>> Primaria 961 “Domingo Faustino Sarmiento” Campo Hardy - $ 19.500

>> Primaria 893 “Mariano Moreno” Reconquista - $ 68.950

>> Primaria 1296 “Máximo Vicentin” Avellaneda - $ 69.299

>> Jardín de Infantes 159 Reconquista - $ 27.265

>> EESO 385 “Prof. Susana A. Maglione” Reconquista - $ 56.860

>> Primaria 6209 “Vice Comodoro Marambio” El Timbo - $ 6.165

>> Primaria 6212 “Esteban Laureano Maradona” Fortín Olmos - $ 32.523

>> Primaria 6047 “Juan Bautista Cabral” Cañada Ombú - $ 122.885,91

>> CEF 24 Vera – 19.500

>> Primaria 443 “Pablo Pizzurno” Vera - $ 26.999

>> Primaria 437 “Reg. de Granaderos a Caballos” Campo Huber - $ 45.999

>> Primaria 421 “Domingo Faustino Sarmiento” Colonia Alejandra - $ 54.571,37

![](https://assets.3dnoticias.com.ar/educacion2.jpeg)