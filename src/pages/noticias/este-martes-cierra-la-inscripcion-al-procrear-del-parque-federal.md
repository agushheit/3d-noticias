---
category: La Ciudad
date: 2021-10-09T06:15:37-03:00
thumbnail: https://assets.3dnoticias.com.ar/procrear.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Este martes cierra la inscripción al Procrear del Parque Federal
title: Este martes cierra la inscripción al Procrear del Parque Federal
entradilla: Hasta el jueves se informaron 4.500 personas que formalizaron su inscripción.
  Se que adjudicarán 199 viviendas del Parque Federal.

---
Desde la Secretaría de Hábitat, dependiente del Ministerio de Infraestructura, Servicios Públicos y Hábitat, informaron cierre de inscripción el programa Procrear II Desarrollos Urbanísticos, correspondiente al Ministerio de Desarrollo Territorial y Hábitat de la Nación. En la ciudad de Santa Fe se adjudicarán 199 viviendas en el Parque Federal.

En este sentido, el secretario de Hábitat, Urbanismo y Vivienda, Amado Zorzón, comentó: “No están terminadas, pero a medida que estén habitables se irán adjudicando”, aclaró. Y continuó: “El gobernador Omar Perotti nos pidió que maximicemos el esfuerzo para resolver el tema de la vivienda. Por eso en la ciudad de Santa Fe, como en todo el territorio provincial, se están llevando adelante diferentes intervenciones y se proyectan nuevas, a través de la Dirección Provincial de Vivienda”, dijo Zorzón. Hasta el momento se inscribieron 4.500 santafesinos y santafesinas al Procrear para ser sorteados en las viviendas a adjudicar en el Parque Federal.

### Requisitos

• No haber resultado beneficiado/a con planes de vivienda en los últimos diez (10) años.

• No tener, tanto el/la titular, como el/la cotitular, bienes inmuebles registrados a su nombre, ni como propietarios ni como co-propietarios, al momento de iniciar el proceso de inscripción. Con excepción de los casos detallados en las Bases y Condiciones.

• Contar con el Documento Nacional de Identidad vigente (DNI o Libreta de Enrolamiento o Libreta Cívica) de todos los integrantes de la solicitud al momento de iniciar el trámite.

• Ser argentinos/as o extranjeros/as con residencia permanente en el país.

• Tener entre 18 y 64 años al momento de completar la inscripción.

• Acreditar el estado civil declarado en el formulario de inscripción, excepto el estado civil soltero.

• Presentar Certificado de Discapacidad en caso de corresponder.

• Demostrar ingresos netos mensuales del grupo familiar conviviente –tanto el/la solicitante como su cónyuge o pareja conviviente– entre dos y ocho salarios mínimos vital y móvil (SMVyM). Los ingresos mínimos necesarios para acceder a una determinada vivienda podrán variar según el predio y la tipología seleccionada. De acuerdo a la información declarada en el formulario, se indicará si existen o no tipologías disponibles.

• Demostrar, como mínimo, doce (12) meses de continuidad laboral registrada.

• No registrar antecedentes negativos en el Sistema Financiero durante los últimos doce (12) meses por falta de pago de obligaciones de todo tipo; no encontrarse inhabilitados por el BCRA o por Orden Judicial; no registrar juicios según informe de antecedentes comerciales en los últimos cinco (5) años; no registrar embargos; y no registrar peticiones o declaraciones de concurso o quiebra.

• Los y las participantes, al momento de completar el formulario de inscripción, podrán incluir solo un/a cotitular. Para el caso de los y las participantes de estado civil casado/a, el/la cónyuge, será considerado cotitular automáticamente.

• El/la titular y el/la cotitular deberán encontrarse unidos por alguno de los vínculos que se detallan a continuación, los cuales deberán encontrarse registrados.

• Matrimonio.

• Unión convivencial.

• Unión de hecho, siempre que coincidan los domicilios declarados por el/la titular y cotitular.

• Todas las notificaciones en el marco de las presentes bases y condiciones serán realizadas a la dirección de correo electrónico declarada por el/la participante en el formulario de inscripción.

### Inscripción al Procrear en el Parque Federal

Los interesados podrán inscribirse a través del link: (https://www.argentina.gob.ar/habitat/procrear/desarrollosurbanisticos/santa-fe-parque-federal) donde deberán completar la información que allí se detalla. El trámite del Procrear puede hacerse sin necesidad de intermediarios y es gratuito.