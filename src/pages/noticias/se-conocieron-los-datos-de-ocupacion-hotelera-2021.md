---
category: Agenda Ciudadana
date: 2021-05-25T08:17:59-03:00
thumbnail: https://assets.3dnoticias.com.ar/hotel-cerradojpg.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Se conocieron los datos de ocupación hotelera 2021
title: Se conocieron los datos de ocupación hotelera 2021
entradilla: 'Para marzo de 2021, se estimaron 2,1 millones de ocupaciones en establecimientos
  hoteleros. Esto implica un aumento de 2,2% respecto del mismo mes del año anterior. '

---
Como es de público conocimiento, a partir del 20 de marzo de 2020 se establecieron restricciones a la circulación de las personas en todo el país con el objetivo de reducir la exposición al contagio de la COVID-19 en el marco de la emergencia sanitaria.

De acuerdo a la emergencia pública en materia sanitaria aprobada por la ley n° 27.541 y el decreto de necesidad y urgencia n° 260/201 y con el fin de adoptar medidas tendientes a desalentar el movimiento de la ciudadanía con fines turísticos en el territorio nacional, el Ministerio de Turismo y Deportes de la Nación resolvió limitar la actividad de los establecimientos hoteleros mediante la resolución n° 131/202.

En normas posteriores se modificaron las restricciones. La provincia de Jujuy fue la primera que permitió la apertura de la actividad turística interna durante los últimos días de mayo de 2020.

A partir de junio de 2020, mediante el decreto de necesidad y urgencia n° 520/203 y sus sucesivas prórrogas, el Estado nacional dispuso un nuevo marco normativo para las zonas en donde no exista circulación comunitaria de SARS-CoV-2, en las cuales regirá el distanciamiento social, preventivo y obligatorio en la medida que se verifiquen parámetros epidemiológicos y sanitarios, y donde el sistema de salud pueda dar respuesta suficiente y adecuada a la demanda sanitaria.

En este contexto, algunas provincias autorizaron la apertura de establecimientos para el turismo interno con fines recreativos, sujeta a las condiciones y limitaciones que se establecen en los protocolos sanitarios de los rubros vinculados al sector turístico. Cabe aclarar que la habilitación de la actividad estuvo limitada principalmente a los turistas residentes de la localidad, el corredor o la provincia y a algunas localidades dentro del territorio provincial o bien delimitada por corredores turísticos, respetando las disposiciones y recomendaciones jurisdiccionales correspondientes. Sin embargo, a raíz del incremento de casos, varias provincias volvieron a deshabilitar el turismo interno.

El 15 de octubre de 2020, mediante la resolución nº 221/204 del Ministerio de Transporte, se habilitó nuevamente la operación de los servicios de transporte aéreo de cabotaje para quienes desarrollan tareas esenciales, personas que deban realizarse tratamientos médicos y otras que exhiban “razones de fuerza mayor”. El 29 de octubre de 2020 se publicó la decisión administrativa nº 1.949/205 con la finalidad de adoptar medidas para la reanudación gradual de la actividad turística a nivel nacional, donde el Gobierno estableció la instrumentación de una prueba piloto para la reanudación

del turismo receptivo en el Área Metropolitana de Buenos Aires, acotando la procedencia a los países limítrofes.

Durante diciembre, varias provincias permitieron el turismo interprovincial, mientras que otras lo mantuvieron limitado al intraprovincial o realizaron pruebas piloto de apertura. El 21 de diciembre de 2020, fecha de emisión del decreto nº 1.033/206, se estableció el distanciamiento social, preventivo y obligatorio como medida para todas las personas que residan o transiten en los aglomerados urbanos, partidos y departamentos de las provincias argentinas, en tanto estos verifiquen en forma positiva los parámetros epidemiológicos y sanitarios. Como requisito obligatorio antes de vacacionar, varios distritos incluyeron tramitar un certificado de verano a través de la aplicación Cuidar, que además brinda recomendaciones sanitarias. En la aplicación se efectúa un autodiagnóstico y se completa un formulario con los datos de la estadía.

El 24 de diciembre de 2020, mediante la decisión administrativa nº 2.252/207  emitida por el jefe de Gabinete de Ministros, se estableció que desde el 25 de diciembre de 2020 hasta el 8 de enero de 2021 se suspende la vigencia de la decisión administrativa nº 1.949/20.

El 8 de enero de 2021, mediante la decisión administrativa nº 2/218  emitida por el jefe de Gabinete de Ministros, se prorrogó el plazo establecido en el artículo 1º de la decisión administrativa nº 2.252/20, hasta el 31 de enero de 2021 inclusive. Durante este período se mantienen en vigencia las disposiciones contenidas en los artículos 2º, 3º, 4º y 5º de la norma citada precedentemente.

Mediante la decisión administrativa nº 44/21,9  publicada el 1 de febrero de 2021, se prorrogaron hasta el 28 de febrero de 2021 todas las disposiciones de la resolución administrativa nº 2.252/20, la cual fue extendida por la resolución administrativa n° 2/21.

El 28 de febrero de 2021, mediante la decisión administrativa nº 155/2110 emitida por el jefe de Gabinete de Ministros, se prorrogaron hasta el 12 de marzo de 2021 todas las disposiciones de la resolución administrativa nº 2.252/20.

El 11 de marzo de 2021, mediante el decreto nº 167/2111, se prorrogó la emergencia sanitaria hasta el 31 de diciembre de 2021 y se recomendó la aplicación de restricciones de viajes desde o hacia las zonas afectadas de mayor riesgo.

El 13 de marzo, mediante la decisión administrativa n° 219/2112, se prorrogaron hasta el 9 de abril de 2021 todas las disposiciones de la resolución administrativa n° 2.252/20.

El objetivo de la Encuesta de Ocupación Hotelera (EOH) es monitorear la actividad turística, servicio que desde marzo de 2020 se ve restringido por la normativa vigente.

Cabe destacar que, paralelamente, hay establecimientos hoteleros que continúan prestando exclusivamente servicios no turísticos asociados a la COVID-19, que no forman parte de las estimaciones de la EOH. Desde el INDEC se ahondaron esfuerzos con el fin de recabar información sobre la actividad que se desarrolló en el marco de la emergencia sanitaria.

Con el relevamiento realizado en marzo de 2021 en las 49 localidades que integran la EOH, se logró identificar un total de 40 establecimientos que prestaron servicios de alojamiento exclusivamente a personas con diagnóstico de COVID positivo, casos sospechosos con síntomas y personas residentes en la localidad que requirieron aislarse preventivamente, las cuales no se consideran turistas.

![](https://assets.3dnoticias.com.ar/turismo.jpg)

[Lee la encuesta completa ingresando en el link](https://www.indec.gob.ar/uploads/informesdeprensa/eoh_05_212B42E0E10F.pdf)