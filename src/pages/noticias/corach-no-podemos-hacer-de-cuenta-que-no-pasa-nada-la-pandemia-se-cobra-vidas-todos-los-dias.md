---
category: Estado Real
date: 2021-05-12T08:15:05-03:00
thumbnail: https://assets.3dnoticias.com.ar/corach1.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: 'Corach: “No podemos hacer de cuenta que no pasa nada; la pandemia se cobra
  vidas todos los días”'
title: 'Corach: “No podemos hacer de cuenta que no pasa nada; la pandemia se cobra
  vidas todos los días”'
entradilla: 'Lo aseguró el ministro de Gestión Pública luego de encabezar una mesa
  de trabajo con intendentes y presidentes comunales para analizar las medidas vigentes
  tras el último decreto provincial.

'

---
El ministro de Gestión Pública de Santa Fe, Marcos Corach, encabezó este martes una mesa de trabajo con más de 270 intendentes y presidentes comunales para analizar las medidas que están vigentes tras el último decreto provincial. En ese marco, el funcionario aseguró: “No podemos hacer de cuenta que no pasa nada; la pandemia se cobra vidas todos los días”.

En consonancia, el funcionario afirmó que “es importante entender que la pandemia no terminó, y que mientras el proceso de inmunización no haya terminado, es imprescindible seguir con los cuidados y con las medidas necesarias para disminuir la circulación y los encuentros sociales”.

Y agregó: “Una buena parte de los adultos mayor ya se encuentra vacunada con al menos una dosis y esa es una buena noticia, pero hoy el virus, desde la aparición de nuevas cepas, está haciendo estragos en todas las edades. Por eso es que tenemos que tomar medidas. Y tomar medidas implica establecer prioridades, porque estamos ante una situación excepcional: una pandemia”.

“Es difícil tomar medidas que afectan intereses económicos -subrayó Corach-, pero ya lo hemos dicho: no hay buenas noticias en una pandemia. Los dirigentes tenemos que estar a la altura del momento que vivimos, reflexionar, hacernos cargo del papel que nos toca ante una situación excepcional y sostener, acompañar y comunicar las medidas que haya que comunicar, porque hay algo que debe quedar claro: son medidas en favor de todos los santafesinos y del valor más preciado que tenemos, la vida”.

**SENSIBLES A LOS RIESGOS**

En ese sentido, el ministro sentenció: “Creer que el gobierno es insensible ante los problemas que implica el normal desenvolvimiento de la economía es, sobre todo, un análisis incorrecto. Se trata de ser, antes que nada, sensibles a los riesgos que acarrea la pandemia. Y luego, acompañar a quienes sufren las complicaciones que las medidas de cuidado y las restricciones conllevan”.

“Es cierto que estamos tomando medidas que son difíciles -dijo-, pero lo hacemos con un sistema de salud que está muy exigido y con muertos que se suman de a decenas todos los días en nuestra provincia”.

Y argumentó: “Somos los primeros que queremos que las restricciones cesen y que los riesgos de la pandemia queden atrás, pero debemos ser responsables y tomar las medidas necesarias para el bien común. Y necesitamos trabajar juntos todos aquellos que tenemos responsabilidades públicas, en todas las instancias: nacional, provincial y local”.

Para cerrar, indicó que “vamos a sostener todo lo que podamos la presencialidad escolar, porque creemos que luego de la vida y de la salud, la educación es el valor más importante que tenemos que proteger, porque es el futuro de nuestra sociedad. Con esas prioridades trabajamos y con la certeza que deseamos sea compartida por todos: cuanto más nos cuidamos y cuidamos al otro, más estamos haciendo para que esto quede atrás y todos podamos salir adelante lo antes posible. Se ha dicho mucho, pero no por eso es menos cierto: en este momento, nadie se salva solo”.

De la reunión -llevada a cabo de manera virtual- participaron también los ministros de Salud, Sonia Martorano; de Trabajo, Empleo y Seguridad Social, Juan Manuel Pusineri; y de Desarrollo Social, Danilo Capitani; además de los secretarios de Integración y Fortalecimiento Institucional, José Freyre; y de Comercio Interior y Servicios, Juan Marcos Aviano.

**LA POSTURA DE LOS INTENDENTES**

El intendente de Rosario, Pablo Javkin, manifestó: “Nos tenemos que poner de acuerdo para poder atender a todos. La provincia es una sola y nosotros recibimos pacientes de todos lados. Se nos está achicando la capacidad de absorber casos, porque el paciente que entra en cama crítica está un promedio de 28 días. Necesitamos un criterio común en la restricción a la circulación, porque sino van a crecer los casos. Por eso, lo que decidamos lo tenemos que sostener entre todos”.

Asimismo, destacó que “venimos muy bien con la vacunación, probablemente en mayo y junio terminemos con el 60% de la población objetivo vacunada. Tenemos que cuidarnos más por un tiempo, que por suerte cada vez es más corto”.

Por su parte, el intendente de la ciudad de Santa Fe, Emilio Jatón, destacó que dicha ciudad “está recibiendo pacientes de 98 localidades además de la ciudad de Santa Fe. Hoy nos quedan ocho camas. Lo que está pasando no le gusta a nadie. Ninguno quisiera estar en este lugar porque en medio está la salud de la gente y eso es preocupante. Sí o sí tenemos que ser solidarios”.

Por último, el intendente de Arroyo Seco, Nizar Esper, sostuvo que “si no tenemos conciencia y en cuenta el momento crítico que estamos sufriendo, nunca vamos a lograr nada. El tema es muchísimo más profundo y grave. Todos los intendentes y presidentes comunales tenemos que sostener las medidas”.

**COMITÉ OPERATIVO DE EMERGENCIA**

Anteriormente, la vicegobernadora, Alejandra Rodenas, encabezó un cónclave del Comité Operativo de Emergencia, compuesto por autoridades sanitarias y legisladores de los distintos departamentos del territorio santafesino.

Al respecto, Rodenas explicó que “nos reunimos para despejar dudas y receptar inquietudes. Esto lo venimos haciendo desde la primera ola; hemos tenido un buen trabajo de coordinación, al igual que el gobernador Omar Perotti con intendentes y presidentes comunales”.

En ese sentido, dijo que “siempre se busca un marco de equilibrio entre la normativa nacional, la situación sanitaria -que se monitorea día a día-, la opinión de los expertos que asesoran al gobernador, y la temporalidad de las decisiones. Tenemos una profunda preocupación por sostener, la actividad educativa, el sector productivo y cuidar la salud”, finalizó.

Por último, autoridades de los Ministerios de Trabajo, Empleo y Seguridad Social; y de Producción, Ciencia y Tecnología, mantuvieron un encuentro virtual con representantes de actividades que se vieron afectadas por la pandemia, en continuidad del diálogo establecido con dichos sectores donde les adelantaron medidas económicas que la provincia destinará a los mismos.