---
category: Agenda Ciudadana
date: 2021-03-18T07:43:47-03:00
thumbnail: https://assets.3dnoticias.com.ar/cantero.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de LT10
resumen: 'Clases presenciales: la ministra de Educación, dura con Mario Barletta'
title: 'Clases presenciales: la ministra de Educación, dura con Mario Barletta'
entradilla: "“Es fácil reclamar cuando no se tiene la responsabilidad de la toma de
  decisión”, respondió Adriana Cantero tras la presentación efectuada por el dirigente
  radical para que se dicten clases de forma 100% presencial."

---
El exintendente de Santa Fe Mario Barletta presentó esta semana un recurso de amparo contra la alternancia de la presencialidad en las escuelas santafesinas, que fue aceptado por la Justicia. 

Consultada al respecto, la ministra de Educación cuestionó la postura del líder radical. “Muchas veces es fácil reclamar cuando no se tiene la responsabilidad de la toma de decisión, como es nuestro caso, sobre la vida de todos”, comenzó.

Además, la funcionaria recordó que la modalidad aplicada en Santa Fe fue aprobada por los 24 ministros de Salud y de Educación del país, “en el resguardo de lo que es posible mantener en un tiempo de pandemia”. 

“Nosotros estamos tranquilos de que estamos respondiendo con responsabilidad en el formato que hemos elegido”, aseveró. 

“Me sorprende que alguien que ha sido un dirigente en algún momento, no esté pensando en la responsabilidad colectiva que tenemos y en la necesidad de no llevar más confusión a la gente en los momentos complejos que estamos viviendo”, finalizó Cantero.