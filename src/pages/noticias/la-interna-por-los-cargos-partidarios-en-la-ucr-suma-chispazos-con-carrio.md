---
category: Agenda Ciudadana
date: 2021-11-29T06:15:21-03:00
thumbnail: https://assets.3dnoticias.com.ar/IITA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: La interna por los cargos partidarios en la UCR suma chispazos con Carrió
title: La interna por los cargos partidarios en la UCR suma chispazos con Carrió
entradilla: "La socia chaqueña de Juntos por el Cambio (JxC) volvió a meterse en la
  interna radical, pidió \"caras decentes\" al frente de los bloques parlamentarios
  de la alianza y reafirmó su apoyo a la continuidad de Mario Negri.\n\n"

---
La pelea por los cargos partidarios promete nuevas tensiones en la UCR, pese a que el saliente presidente de la fuerza, Alfredo Cornejo, avisó que buscará la conformación de una lista de unidad de cara a la renovación de autoridades, con la idea de contener chispazos internos, que en las últimas horas fueron oxigenados por la fundadora de la Coalición Cívica, Elisa Carrió.  
  
La socia chaqueña de Juntos por el Cambio (JxC) volvió a meterse este sábado en la interna radical, pidió "caras decentes" al frente de los bloques parlamentarios de la alianza y reafirmó su apoyo a la continuidad de Mario Negri como presidente del interbloque parlamentario de la alianza opositora en la Cámara de Diputados.  
  
Con el objetivo de obtener mayor protagonismo dentro de JxC hacia el 2023, los principales referentes de la UCR se disputan los cargos partidarios que se renuevan este año, tanto en la conducción del Comité Nacional como en la Convención Nacional de la fuerza.

> Lousteau ya había avisado que buscará reemplazar a Cornejo en la presidencia del partido y lo mismo hizo el gobernador jujeño Gerardo Morales

  
Pero junto a la elección del nuevo titular del Comité y de la Convención, el radicalismo suele definir quiénes serán los presidentes de los bloques, en una negociación simultánea que a veces otorga la presidencia a un sector y los bloques a otros.  
  
Esta semana la primera novedad fue la confirmación por parte del mendocino Cornejo de que buscará una lista de unidad que integre a los sectores en pugna, tal como había adelantado Télam.  
  
La segunda noticia fue la movida mediática de los alfiles del senador Martín Lousteau, que salieron públicamente a reclamar la presidencia del bloque radical en la la Cámara de Diputados, un pedido que cosechó el rechazo de la líder espiritual de la Coalición Cívica (CC), Elisa Carrió.

> "Sepan lo que eligen en la presidencia de los bloques. Yo no me fui de un partido que tenía corruptos para venir a hablar con los hijos privilegiados de esos corruptos"Elisa Carrió

  
Tiempo atrás Lousteau ya había avisado que buscará reemplazar a Cornejo en la presidencia del partido y lo mismo hizo el gobernador jujeño Gerardo Morales.  
  
El primero tiene el apoyo de la dirigencia universitaria de la UCR y de varios candidatos que apadrinó en las provincias, como el cordobés Rodrigo de Loredo o el porteño Martín Tetaz.  
  
Morales, por su parte, recibió la bendición de la UCR bonaerense, en manos de Maximiliano Abad, quien impulsó la candidatura de Facundo Manes en ese distrito, pero el neurocientífico por ahora sigue con su postura zen y se mantiene equidistante entre ambos bandos.

> Morales recibió la bendición de la UCR bonaerense, en manos de Maximiliano Abad, quien impulsó la candidatura de Facundo Manes en ese distrito

  
El jujeño también tiene el apoyo de los actuales presidentes de los bloques de la UCR en el Congreso, Negri (en Diputados, donde también preside el interbloque de la coalición) y Luis Naidenoff (Senado) y de buena parte de la estructura más orgánica del partido.  
  
Esta semana Lousteau mandó a su tropa a jugar fuerte para ganar posiciones, impaciente porque la elección pudiera realizarse este mes y ante la certeza de que todavía no tiene los votos para imponerse.  
  
"El nuevo radicalismo no persigue oficinas ni contratos. Dame el último sucucho, pero poneme a @rodrigodeloredo que acaba de arrasar en Córdoba o a @ManesF que fue la revelación en PBA, a conducir el bloque", escribió Tetaz en su cuenta de la red social Twitter.  
Un día después, el diputado Emiliano Yacobitti (mano derecha de Lousteau) lo imitó y manifestó que "las re re reelecciones en los lugares de poder y concentración de vocerías, sea de quien sea, debilita a JxC porque obtura el surgimiento de nuevos liderazgos".  
  
Yacobitti admitió que buscará arrebatarle la jefatura del bloque radical a Negri, en coincidencia con la intención de Lousteau de competir por la presidencia del partido, como trampolín para una eventual candidatura presidencial o como paso previo para postularse por la jefatura de Gobierno porteño.

> "Tenemos que potenciar en el Parlamento a figuras como Rodrigo De Loredo, Martín Tetaz, Facundo Manes, Martín Lousteau, Alfredo Cornejo y Carolina Losada para fortalecer al radicalismo y ampliar a Juntos por el Cambio"Emiliano Yacobitti

  
En el mismo sentido, Yacobitti exhortó desde las redes sociales: "Tenemos que potenciar en el Parlamento a figuras como Rodrigo De Loredo, Martín Tetaz, Facundo Manes, Martín Lousteau, Alfredo Cornejo y Carolina Losada para fortalecer al radicalismo y ampliar a Juntos por el Cambio".  
  
Junto al mensaje posteado en Twitter, el legislador de la UCR escribió una columna de opinión en un medio virtual, titulada "Unidad para ganar", en la que sostuvo que "no puede ocurrir que habiendo diferentes sectores y figuras de la coalición, un grupo pueda arrogarse el derecho de decidir unilateralmente por el conjunto, o quedarse con todos los espacios institucionales que le corresponden por un capital electoral que se construyó gracias a que crecimos en la diversidad y la apertura".  
  
  
Pocas horas después, Carrió aprovechó el festejo por los 20 años de la CC para pedir "caras decentes" al frente de los bloques y reafirmó su apoyo a la continuidad de Negri, algo que ya habían hecho público sus diputados.  
  
"Sepan lo que eligen en la presidencia de los bloques. Yo no me fui de un partido que tenía corruptos para venir a hablar con los hijos privilegiados de esos corruptos", dijo ante la mirada de varios dirigentes de JxC, en clara referencia a Enrique Nosiglia, padrino político de Lousteau y Yacobitti.

> "Estoy trabajando una lista de unidad que incluya todos los lugares que hay para distribuir, creo que sería la mejor contribución a este proceso del radicalismo"Alfredo Cornejo

  
Su crítica sonó tan fuerte que Yacobitti le respondió desde su cuenta de Twitter. "Considero que los dichos de Elisa Carrió atentan contra la unidad de JxC y son funcionales al kirchnerismo", sostuvo.  
  
Más conciliador, Cornejo había hablado días antes de la renovación de autoridades que el partido debe realizar este fin de año, confirmando sus intentos de unidad.  
  
"Estoy trabajando una lista de unidad que incluya todos los lugares que hay para distribuir, creo que sería la mejor contribución a este proceso del radicalismo", confió el actual titular del partido en una entrevista esta semana.  
  
"No creo que el horno esté para bollos", graficó Cornejo sobre la posibilidad de ir a una interna para resolver esos cargos.