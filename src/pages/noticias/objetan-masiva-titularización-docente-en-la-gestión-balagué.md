---
layout: Noticia con imagen
author: "Fuente: La Capital"
resumen: "Gestión Balagué: objeciones"
category: Agenda Ciudadana
title: Objetan masiva titularización docente en la gestión Balagué
entradilla: El Tribunal de Cuentas Provincial (TCP) detectó irregularidades en
  el nombramiento de casi 800 personas sin antecedentes ni títulos requeridos
  para el cargo.
date: 2020-11-15T14:50:36.313Z
thumbnail: https://assets.3dnoticias.com.ar/balagu%C3%A9.jpg
---
La ministra de Educación de la provincia, Adriana Cantero, a través de una serie de resoluciones, declararía la nulidad de las titularizaciones que tuvieron lugar durante los últimos días de la gestión de Miguel Lifschitz por “ilegales” y convocaría a nuevos concursos para transparentar el objetado proceso y enmarcar todas las acciones en el pleno cumplimiento del Decreto 3029. La ministra Cantero ha sido conminada por el Tribunal de Cuentas Provincial (TCP) a revocar las resoluciones observadas por las irregularidades de los procedimientos detectados.

Las actas de Observación Legal Nº0014, Nº0015 y la Nº 0044 del TCP reflejan graves irregularidades en el proceso de titularización de cargos y horas cátedra durante la gestión de la ministra del FPCyS, Claudia Balagué. Las maniobras cuestionadas por el Tribunal intentaron plasmarse con casi 800 personas durante el último tramo de la gestión de Miguel Lifschitz.

El organismo de control administrativo, al cuestionar la resolución Nº1868 del 05 de diciembre de 2019 emitida por Balagué, pocos días antes de abandonar su oficina del Centro Cívico santafesino, señala que en el “concurso de titularización convocado por Resolución Nº1740/19, en cargos y horas cátedras vacantes, no se verifica que el personal designado sea docente o con competencia docente de acuerdo a lo requerido por el Art. 4 Capítulo” de la ley obrante.

Abunda en el tema precisando que “no se acompaña acta de la Junta de Escalafonamiento que resuelva sobre la pertinencia de la admisión como concursante conforme a lo establecido”. Más aún, el texto del TCP expresa que “no se acredita publicidad del escalafón” como lo establece la ley. Por aquel entonces, la Secretaría de Educación habilitó a exceptuar de los requisitos concursales en el reglamento general de concursos de ingreso a la docencia, invocando habilitaciones especiales para resolver “lo no previsto”.

Esto también fue señalado en un pormenorizado informe por la actual Dirección Provincial de Asuntos Jurídicos del Ministerio de Educación.

Uno de los casos investigados impacta en el programa “Vuelvo a Estudiar”, para el que se creó una Escuela media para adultos incrementando su dotación de personal hasta llevarla a una planta de personal de más de 30 cargos y más de 7000 horas cátedras. Esta planta así conformada con las últimas designaciones observadas, llama la atención por lo desmedida frente a la transitoriedad del objetivo del programa y en comparación con las otras EEMPAS del sistema educativo.

Las objeciones presentadas por el Tribunal de Cuentas en sus resoluciones Nº0044/19 (Formación Profesional y Capacitación Laboral); Nº0014/20 (Equipos Territoriales con Eje en la Convivencia) y Nº0015/20 (Plan Vuelvo a Estudiar) remiten a las siguientes situaciones:

—No puede determinarse con qué norma accedieron a la cobertura de cargos vacantes para después, en un tiempo ciertamente veloz, ser titularizados en las funciones que se identifican.

—No existen antecedentes que permitan determinar qué títulos tienen los nombrados.

—En los concursos observados hay una nítida presencia de irregularidades, llegándose a detectar concursos sin previa exhibición de escalafones o directamente sin escalafones.

—Se cuestionó la inexistencia de una etapa recursiva, donde tampoco hubo una intervención de las Juntas. El proceso continuó y luego se realizaron las tomas de posesión en estos cargos “armados”, esto es, figuras que ni siquiera existían en el nomenclador oficial, como por ejemplo, referente de convivencia. De este modo, a una persona le dieron un cargo sin previa existencia en las categorías oficiales, con 20 horas y en un breve lapso de tiempo, se la titularizó.

—Se transformaron cargos y horas vacantes para hacer después los nombramientos evitando que pudieran cubrirse con suplencias o traslados.

—No se acreditó fehacientemente antes de titularizar a estas personas en horas y/o cargos la compatibilidad elemental para realizar los nombramientos, no hubo certificación de título. Mucho menos de aptitud física o carpeta médica.

—Se habilitó a la Secretaría de Educación para que actúe y exceptúe de requisitos concursales previstos en el reglamento general de concursos de ingreso a la docencia. La Secretaría podía resolver para “lo no previsto”.

—No intervino el Jurado Concursal, que expresamente estaba designado en la convocatoria para serlo. Pero cuando intervino, no se realizaron actas, ni se procesó el escalafonamiento con las publicidades del caso.

**Permanencia laboral**

Desde el ministerio de Educación trascendió que los docentes que fueron titularizados y hoy se encuentran en situación de entredicho administrativo “no quedarían en la calle, ni dejarían de cobrar. Lo que podría pasar es un acto de cambio de situación de revista de esos docentes y esas personas para que ya no cuenten con la titularidad y pasen a la condición de interinos. Van a seguir cobrando el sueldo, pero como interinos hasta tanto se realice el nuevo concurso”.

**Observaciones**

El Tribunal de Cuentas le solicitó por escrito al Ministerio de Educación de la provincia que se pronuncie en torno a los considerandos observados y que estos “procedimientos ilegales” sean ordenados. La ministra Cantero dispondría la baja de los concursos cuestionados por el Tribunal de Cuentas y otros organismos públicos.

“No se trata de dejar afuera a nadie, hemos demostrado nuestra voluntad de titularizar a los docentes habiendo realizado los ofrecimientos de la primera etapa de nuestra gestión, e impulsando nuevos concursos innovando los procedimientos en un tiempo afectado por la pandemia como los que se están desplegando en estos días. Se trata de ser respetuosos de las normas vigentes porque es el mejor modo de resguardar el derecho de todas y todos los docentes y el cuidado de sus trayectorias y oportunidades” dijo una alta fuente de la Casa Gris. Una decisión que —cuando se tome— generará múltiples debates frente a estos hechos incontrastables.

**Opiniones concordantes**

A esta conclusión jurídica y administrativa sobre las irregularidades obrantes en las titularizaciones del último tramo de la gestión de Claudia Balagué en Educación, también llegaron la Dirección Provincial de Asuntos Jurídicos del Ministerio de Educación, como la Comisión creada por el gobernador Perotti a través del decreto Nº89/19 que establece el cumplimiento del Régimen Federal de Responsabilidad Fiscal y Buenas Prácticas de Gobierno, a las cuales la provincia adhirió. De la misma participan Víctor Bonaveri, subsecretario Legal y Técnica del Ministerio de Gestión Pública; el fiscal de Estado Provincial, Luis Weder; el secretario de Trabajo del Ministerio de Trabajo, Juan Manuel Pusineri y el secretario Legal y de Coordinación del Ministerio de Economía, Javier Gallo.