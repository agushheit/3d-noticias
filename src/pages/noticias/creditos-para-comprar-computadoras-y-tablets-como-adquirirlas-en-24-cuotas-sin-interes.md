---
category: Agenda Ciudadana
date: 2021-02-03T07:03:13-03:00
thumbnail: https://assets.3dnoticias.com.ar/vuelta-al-cole.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Infobae
resumen: 'Créditos para comprar computadoras y tablets: cómo adquirirlas en 24 cuotas
  sin interés'
title: 'Créditos para comprar computadoras y tablets: cómo adquirirlas en 24 cuotas
  sin interés'
entradilla: El plan “Vuelta al Cole” regirá del 5 al 7 de febrero y permitirá acceder
  a notebooks con facilidades, además de artículos de librería e indumentaria

---
La campaña “Vuelta al Cole” permitirá comprar notebooks y tablets en 24 cuotas sin interés con las tarjetas de crédito Nativa Mastercard y Nativa Visa del Banco Nación. El plan se pondrá en marcha a través de la plataforma de comercio electrónico de la entidad, TiendaBNA, y funcionará entre el 5 y el 7 de febrero.

Según informó la entidad financiera, la propuesta incluye la posibilidad de comprar artículos de librería e indumentaria, con un beneficio del 30% de descuento para compras en un pago con un tope de devolución de $1.800 y un 15% para adquisiciones de 2 a 6 cuotas sin tope y sin interés. Esta opción podrá aprovecharse entre el 8 y el 13 de febrero.

Con una inflación que en 2020 fue de 36,18%, según el Indec, las familias comenzaron a adelantar recién a enero la compra de útiles escolares, cuya canasta básica llega con un incremento del 48% interanual y que puede multiplicarse si se consideran gastos en celulares, computadoras y accesorios necesarios para un año que tendrá una cursada mixta entre clases presenciales bajo estrictos protocolos y virtuales que dejó de ser una opción transitoria.

Los padres que salen a recorrer las librerías ante la noticia de la vuelta a clases luego de un año de enseñanza virtual como consecuencia del aislamiento social preventivo y obligatorio (ASPO), vuelven a encontrarse como en los últimos años con una gran disparidad de precios, dependiendo de si se trata de artículos nacionales o importados y de la oferta en comercios ad hoc, grandes cadenas comerciales en incluso ofertas online.

![](https://assets.3dnoticias.com.ar/utiles.jpg)

Según pudo averiguar Infobae, en algunos casos de alumnos de establecimientos privados que hicieron el gasto de la canasta escolar en 2020 y entregaron sus materiales a la escuela no tendrán que volver a hacerlo este año dado que, al haber cursado de forma virtual, no le dieron uso y los podrán utilizar en el nuevo ciclo.

“Este comienzo de clases tiene una singularidad que es la modalidad mixta de cursada por la pandemia. Va a haber un doble costo fijo por los útiles escolares tradicionales; si no, también que hay que pensar en incorporar un celular o notebook para la clase a distancia en el hogar”, dijo a Infobae el director de Focus Market, Damián Di Pace.

Este comienzo de clases tiene una singularidad que es la modalidad mixta de cursada por la pandemia. Va a haber un doble costo fijo por los útiles escolares tradicionales; si no, también que hay que pensar en incorporar un celular o notebook para la clase a distancia en el hogar (Di Pace)

“Los celulares de gama media tienen precios desde $25.000 a $40.000. Y las notebooks van desde $35.000 hasta $80.000”, detalló.

En tanto, la iniciativa del Banco Nación para la compra de notebooks y tablets en cuotas sin interés complementa la reciente campaña de Navidad y Reyes, en la cual los clientes pudieron acceder a TiendaBNA para adquirir bienes y servicios con un plazo máximo de 18 cuotas, también sin interés. En este sentido, el ticket promedio de los clientes que accedieron a esa propuesta alcanzó los 19.600 pesos, de acuerdo al Banco Nación.

Cabe destacar que el regreso a clases en la Ciudad de Buenos Aires, según se anunció, será escalonado. El 17 de febrero iniciarán cuatro grupos de estudiantes: los chicos de los jardines maternales, el nivel inicial, primer ciclo de primaria (primero, segundo y tercer grado) y primer ciclo de secundaria (primero y segundo año). Luego, el 22 de febrero se sumará el resto de los alumnos de la primaria y una semana después, el 1 de marzo, será el turno de todo el nivel secundario.

Por último, el Gobierno anunció además una línea crediticia para que los médicos de PAMI puedan renovar sus computadoras personales, que también se instrumentará a través del Banco Nación. La línea de financiamiento aspira a desembolsar $650 millones, con tasa bonificada.

“La línea de créditos, vigente hasta el 30 de abril de 2021, está destinada a médicas y médicos de cabecera del PAMI que perciban sus honorarios en una Caja de Ahorro o Cuenta Corriente del Banco de la Nación Argentina. Podrán solicitar créditos de hasta $130.000, por un plazo de 3 años y a una tasa final subsidiada por el FONDEP del 12%, que aportará $82.200.000,″ informó el Nación en un comunicado.