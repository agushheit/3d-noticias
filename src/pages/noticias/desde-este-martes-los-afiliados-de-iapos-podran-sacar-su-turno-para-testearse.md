---
category: Agenda Ciudadana
date: 2022-01-05T06:15:47-03:00
thumbnail: https://assets.3dnoticias.com.ar/iapos.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Desde este martes  los afiliados de Iapos podrán sacar su turno para testearse
title: Desde este martes  los afiliados de Iapos podrán sacar su turno para testearse
entradilla: 'Deberán tener síntomas y solicitar un turno al 0800 de la provincia.
  El mismo sistema se había implementado hasta el mes de septiembre

'

---
Desde este miércoles los afiliados de Iapos podrán acceder a hisopados tras haber solicitado un turno a través del 0-800-555-6549 para luego trasladarse a una posta sanitaria ubicada, como lo estuvo hasta diciembre pasado, en la explanada del hotel de ATE en la costanera Este.

Oscar Broggi, director en Instituto Autárquico Provincial de Obra Social de Santa Fe (Iapos), en dialogo con el móvil de UNO confirmó: "Estamos retomando este miércoles 5 enero la posta del centro de salud que teníamos para hacer hisopados en la explanada del hotel de ATE, un trabajo que hicimos hasta septiembre del año pasado cuando lo dimos de baja por la escasa cantidad de demanda que tenía".

"Para que la posta sanitaria retome con los trabajos, desde la semana pasada comenzamos a reprogramarlo con las capacitaciones de la gente del 0800 que vuelve a tener la opción dos de los afiliados a la obra social provincial. Allí deben comunicarse y una vez realizado la entrevista, aquellos casos en donde se amerite hacer un testeo tendrá su turno para poder dirigirse a la posta sanitaria", dijo Broggi.

Vale remarcar que solamente se van a poder acercar hasta la explanada del hotel quienes tengan su turno y no de manera espontánea. "Tanto el personal como los materiales necesarios para realizar los testeos son de acuerdo a la cantidad de turnos otorgados".

**Cómo solicitar el turno**

Se debe llamar al 0-800-555-6549, oprimir la opción Iapos. Allí se transfiere a la persona a un número telefónico específico de la obra social, y esta luego de una instancia de triaje, determina si el afiliado debe o no ser hisopado.

Por otra parte el titular del reconocimiento de los hisopados en el ámbito privado: "Solo vamos a reconocer los que sean por prescripción médica por sintomatología, no para aquellos casos para que se requiera complementar algún trámite".