---
category: La Ciudad
date: 2021-05-17T06:15:28-03:00
thumbnail: https://assets.3dnoticias.com.ar/YPF.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Nuevo aumento de combustibles: así quedaron los precios en Santa Fe'
title: 'Nuevo aumento de combustibles: así quedaron los precios en Santa Fe'
entradilla: Es el sexto aumento del 2021. El presidente de YPF dijo que "son los últimos
  aumentos del año".

---
En Santa Fe, la nafta Súper llegó a $ 99 y la Premium superó los $ 113. El Diesel quedó en $ 90,20 y el premium en $ 105,40.

La petrolera YPF aumentó este domingo los combustibles un 5% promedio en todo el país, en el marco de un proceso de actualización de precios que culmina este domingo con un acumulado del 15% en el trimestre marzo-mayo y que sería el último del año.

**En la ciudad de Santa Fe, así quedaron los precios**

Súper: $ 99

Premium: $ 113,70

Diesel: $ 90,20

Diesel Premium: $ 105,40

YPF, que tiene una participación de mercado superior al 55% en el segmento minorista de combustibles, marcó el ritmo de recuperación de precios para el sector con un escalonamiento de incrementos que totalizaron el 15% entre marzo y mayo.

Como parte de ese sendero, en marzo, las principales petroleras concretaron un incremento en los surtidores del 7% promedio, y en abril otro 6%, con respectivas inclusiones del componente impositivo y la recuperación de los márgenes de las compañías.

Desde la medianoche, YPF decidió el incremento de 5% promedio en todo el país, con lo que se complementa esta serie de aumentos que, según el presidente de la compañía Pablo González, sería el último que realizará la petrolera en 2021.

Desde la compañía se confirmó que en todos los casos se mantendrán los descuentos vigentes del 15% para personal sanitario y educativo.

El resto de las petroleras como Raizen (licenciataria de Shell), Axion o Puma también instrumentan sus incrementos con algunas diferenciaciones comerciales para mantener la competitividad de sus precios.

De esta manera, desde el 19 de agosto pasado, cuando se decidió un incremento en los combustibles tras 10 meses de congelamiento por la emergencia económica y sanitaria, se registró una sucesión de aumentos que acumulan con el de esta medianoche alzas en torno al 55% de acuerdo al producto, la marca y la región.

El de este domingo es el sexto aumento en lo que va del año, luego de los concretados el 5 de enero del 2,9% y el 16 de enero en un 3,5% promedio, como parte del reacomodamiento del componente impositivo de los combustibles, y del nuevo sendero de recuperación mensual de precios de los biocombustibles.

El tercer aumento se produjo el 1 de febrero cuando se aplicó el incremento de biocombustibles con alzas de 1% en naftas y del 1,9% en gasoil, luego el 16 de marzo con un alza promedio del 7%, el 17 de abril un 6% y el 5% de hoy.

"Estos aumentos son los últimos del año, y en función de esto vamos a poder invertir lo que YPF necesita para producir gas y petróleo a través de un plan de inversión de US$ 2.700 millones", aseguró el presidente de la petrolera en las últimas horas en declaraciones a los medios.

González explicó que, en sus dos meses de gestión, se trató de "equilibrar el precio en surtidor que tiene un componente complejo, tratando de no trasladar a la gente el valor de un commoditie como es el barril de petróleo que está casi en los US$ 70".

"Si se traslada el valor internacional del petróleo, hoy la gente estaría pagando US$ 1,30 el litro (en la actualidad está por debajo de US$ 1). Es un sacrificio para intentar evitar una mayor importación", afirmó.