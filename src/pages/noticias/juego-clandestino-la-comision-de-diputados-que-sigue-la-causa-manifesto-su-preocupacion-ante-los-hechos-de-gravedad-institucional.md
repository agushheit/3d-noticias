---
category: Agenda Ciudadana
date: 2020-12-12T13:14:49Z
thumbnail: https://assets.3dnoticias.com.ar/diputados.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Diario UNO'
resumen: 'JUEGO CLANDESTINO: La comisión de Diputados que sigue la causa  manifestó
  preocupación ante los hechos de gravedad institucional'
title: 'JUEGO CLANDESTINO: La comisión de Diputados que sigue la causa  manifestó
  "su preocupación ante los hechos de gravedad institucional"'
entradilla: Son integrantes de todos los interbloques de la Cámara baja. Respaldaron
  la actuación de los fiscales Luis Schiappa Pietra y Matías Edery.

---
La comisión de Información y Seguimiento de la causa que investiga a los exfiscales Gustavo Ponce Asahad y Patricio Serjal de la Cámara de Diputados, emitió un comunicado este viernes por la tarde, a horas de que el senador por San Lorenzo, Armando Traferri, se presentara a declarar en el Centro de Justicia de Rosario en el marco de la causa por juego clandestino.

En la escueta misiva los miembros de la comisión expresaron "su preocupación por los hechos de gravedad institucional de público conocimiento y manifiesta su apoyo al trabajo de los fiscales Luis Schiappa Pietra y Matías Edery". 

A Traferri no se lo menciona en todo el escrito. Sin embargo, el apoyo a los fiscales se puede leer como una clara muestra de que no habrá una defensa corporativa de la política.

"Es un momento que exige responsabilidad de parte de todos quienes integramos poderes del Estado, para no generar interferencias en la tarea de los fiscales", argumentaron.

Por último, los diputados señalaron: "Al mismo tiempo, impulsamos la necesidad de la transparencia en todos los procedimientos como presupuesto básico de una mejor democracia".

El comunicado lleva las firmas de los diputados y diputadas Joaquín Blanco, Carlos Del Frade, Agustina Donnet, Maximiliano Pullaro, Matilde Bruera, Mónica Peralta, Juan Cruz Cándido, Lionela Cattalini y Ximena Sola.