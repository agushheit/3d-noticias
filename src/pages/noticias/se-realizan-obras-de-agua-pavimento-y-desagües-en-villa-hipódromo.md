---
layout: Noticia con imagen
author: .
resumen: Obras en Villa Hipódromo
category: Estado Real
title: Se realizan obras de agua, pavimento y desagües en Villa Hipódromo
entradilla: Jatón charló con vecinos sobre los trabajos que se concretan con
  proyectos de la Municipalidad y financiamiento de provincia. Entre otras
  tareas, se extiende la red de agua potable para 500 vecinos.
date: 2020-11-13T17:58:28.319Z
thumbnail: https://assets.3dnoticias.com.ar/maquina.jpg
---
Mientras algunas máquinas realizan excavaciones para hacer el cordón cuneta y más tarde el pavimento; otras, hacen las zanjas que contendrán los caños para el tendido de la red de agua potable. Esta es la postal que hoy presenta Villa Hipódromo, ante el asombro y la alegría de los vecinos.

Hasta la intersección de Gaboto y Aldao llegó el intendente Emilio Jatón, con el objetivo de reunirse con los habitantes del barrio. Juntos caminaron por los primeros metros de pavimento que ya se concretaron y observaron la marcha de los trabajos.

“Para nosotros son obras muy importantes porque fueron pensadas y trabajadas en conjunto: la Municipalidad, la provincia y los vecinos. Hace tiempo que la gente del barrio esperaba una solución integral a los problemas que surgían, principalmente, cada vez que llovía y hoy están viendo que los trabajos se están concretando”, resaltó el intendente, que enmarcó los proyectos del barrio en las acciones encaradas a través del Plan Integrar.

Uno de los trabajos que se realizan refiere a nuevas conexiones de agua potable, para lo cual se ejecuta la red de distribución a través del cierre de mallas para habilitar el servicio a 500 habitantes de un sector de Villa Hipódromo. Se trata del mismo tramo donde se completan obras de pavimento, mejorado y desagües pluviales, licitadas por la provincia de Santa Fe y adjudicadas a la Empresa Coemyc S.A., por un monto cercano a los 100 millones de pesos.

La red proyectada prevé el tendido de 865 metros de cañería de red distribuidora de PVC de 75 mm. de diámetro, que se vinculará a la red de agua potable mediante empalmes a cañerías existentes en el barrio, conforme a la factibilidad de servicio otorgada por la empresa ASSA.

En el sector considerado, se prevé la ejecución de 122 conexiones domiciliarias, como así también la instalación de válvulas esclusas para las futuras tareas de mantenimiento y se instalará un hidrante, cuyo uso primordial es la toma de agua para combatir incendios. El monto oficial estimado para la ejecución de esta obra es de 4 millones de pesos.

La secretaria de Asuntos Hídricos y Gestión de Riesgos del municipio, Silvina Serra, formó parte del grupo que recorrió la zona y afirmó que los vecinos están muy motivados porque ven que las tareas ya están en marcha. Contó que es un trabajo conjunto con Aguas Santafesinas S.A., la empresa estatal que provee los materiales y la mano de obra especializada para colocación de cañerías y conexiones domiciliarias, mientras la Municipalidad aportará los equipos (retroexcavadora, camión volcador, entre otros) necesarios para la excavación, tapado y compactación de zanjas, como así también la arena necesaria para el asiento de la cañería y el personal de apoyo que se requiera.

Según detalló, “esto se hace en forma paralela con una obra de provincia de pavimento y drenaje, que fue proyectada por la Municipalidad, por lo que se trata de servicios básicos y fundamentales”.



![](https://assets.3dnoticias.com.ar/saludo.jpg)



**Calles transitables**

“Desde Asuntos Hídricos también se hizo un proyecto interesante de drenaje para la zona. Se trata de un entubado de un poco más de 700 metros que arranca con diámetros desde 1,20 metros en la zona de descarga y, aguas arriba, de hasta de 40 cm. Lo importante de este drenaje es que se conecta con un conducto ya existente, que es Estado de Israel”, describió Serra.

La secretaria argumentó que “logramos acordar con la provincia, los avances en la obra del entubado de Estado de Israel. Esto es muy importante para los vecinos porque esta calle está bordeada por un canal muy profundo, que es la descarga de una cuenca muy grande y genera inseguridad”.



![](https://assets.3dnoticias.com.ar/saludo1.jpg)



**Alegría de los vecinos**

Julia García, vecina de Villa Hipódromo, se mostró contenta por el inicio de las tareas. “Hace 30 años que vivo en el barrio y estoy muy emocionada”, contó. Luego agregó: “Hace mucho que no se hacía nada más que algunas mejoras; nunca llegaba la obra que realmente necesitábamos, que es el desagüe, y ahora también el agua potable”. A esto sumó que “se empezaron a colocar las luces, entonces estoy muy feliz porque el barrio está progresando, las mejoras se ven y esto nos da esperanzas. Significa no estar excluidos”, concluyó.

Hace 50 años que Eduardo Romero vive en el barrio y este jueves conversó con el intendente. “Nos dijo que se están haciendo varias obras para Villa Hipódromo, como el agua potable, el zanjeo y estamos muy contentos porque todo eso nos va a mejorar la calidad de vida. Hace muchos años que no se hace nada en el barrio y ahora también tenemos iluminación nueva, que trae más tranquilidad”.

Bienvenida Mareco, otra habitante de la zona, aseguró que “hace 17 años que vivo en el barrio y ahora está quedando cada vez más lindo. Vamos a tener el asfalto y el agua potable, algo muy importante; también dejamos de sufrir el barro, cada vez que llueve. Se trata de una obra muy esperada pero ya se concreta y se ve”, concluyó.

Por último, Silvia Clerutti, vecina beneficiada por estas obras, aseveró que reside en Villa Hipódromo “hace más de 30 años y nunca se hizo nada. Estamos muy contentos con la obra y con el agua potable que vamos a tener. Hay que tener paciencia pero ya se ven los resultados y queda todo muy lindo”.