---
category: La Ciudad
date: 2021-01-11T10:56:04Z
thumbnail: https://assets.3dnoticias.com.ar/trabajos-descacharrado.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: '3DNoticias - Fuente: UNO Santa Fe'
resumen: 'Dengue en Santa Fe: es momento de prevención para evitar un fuerte brote'
title: 'Dengue en Santa Fe: es momento de prevención para evitar un fuerte brote'
entradilla: El año pasado se decretó en la provincia de Santa Fe la emergencia sanitaria
  por un rebrote de dengue que superó los 5 mil casos.

---
Durante el inicio del 2020 en Argentina convivimos con dos virus endémicos, el Covid-19 y el dengue. La pandemia del coronavirus golpeó al país en marzo del año pasado, pero la epidemia de dengue se vivió más fuerte en enero y febrero. En Santa Fe, el dengue comenzó a llegar junto a los veraneantes de Brasil y Paraguay principalmente, y para el inicio de la cuarentena ya había una fuerte circulación viral.

Así lo explican la directora del Programa de Vectores del Ministerio de Salud provincial Lic. Mariana Maglianese y el subdirector de promoción de la Salud de la ciudad Juan Picatto. El brote de dengue empezó muchos meses antes de la llegada a Argentina en Latinoamérica con cifras récord de casos, debido a que las temperaturas nunca bajan lo suficiente para matar al Aedes Aegypti.

<br/>

## **La epidemia que no da tregua**

Según el Ministerio de Salud de la Nación, durante la temporada 2019-2020 «se ha atravesado el brote de dengue de mayor magnitud que se haya registrado hasta el momento en Argentina» con 58.889 casos en el país, superando casi por un 40,5% la temporada anterior.

La Lic. Mariana Maglianese aseguró que el dengue ingresa al país con los viajeros: «la gente vuelve con la enfermedad, la pican mosquitos localmente y empieza la circulación viral».

Agregó que, además, los casos aumentaron de forma importante y extraordinaria para la época, «porque generalmente los casos de dengue en nuestra zona empezaron históricamente en febrero o marzo, pero esta vez comenzó en enero y se fue incrementando en los meses subsiguientes».

«En el 2019 había una situación muy complicada de casos en toda la región centro y sur de América, y para el mes de noviembre en 2019 había un millón y medio de casos en Brasil», explicó Juan Picatto. «Se entendía que por la dinámica de la relación con nuestro país esos casos iban a llegar al norte del país y de ahí afectar a todo el centro y norte de Argentina, que es lo que sucedió.»

En 2020 los casos en la Provincia de Santa Fe fueron 5.098, 264 en el Departamento La Capital y 221 en la ciudad de Santa Fe. De todas formas, según Picatto no fue el peor año en la ciudad, ya que en 2019 se dieron 361 casos. «Teniendo en cuenta cómo afectó la enfermedad al resto de las localidades medianas y grandes de la provincia, Santa Fe fue la ciudad que tuvo la menor cantidad de casos de la provincia», aclaró Picatto.

<br/>

## **El combo viral**

La presencia del Covid complicó muchísimo las acciones para evitar la propagación del dengue. Cuando se detecta un caso de dengue, el procedimiento indica un bloqueo que se realiza en las casas de la manzana de donde vive el afectado y las otras ocho perimetrales: son nueve manzanas donde se realiza la búsqueda de febriles, el descacharrado y la fumigación dentro de la casa y el patio.

Según Maglianese esta actividad durante el inicio de la pandemia se hizo de forma incompleta porque al principio el vecino no dejaba ingresar a la vivienda, tenía miedo. Y por otro lado se vio diezmada la población del personal de salud que debía realizar esas actividades por estar infectada por Covid o que debía quedarse en la casa.

Además, se dio una dinámica inusual de contagio de la enfermedad debido a la permanencia constante en la vivienda, sin retirarse para trabajar o esparcirse. «Generalmente cuando empieza la circulación viral vos ves que en una casa hay un afectado, en la casa de enfrente hay otro, después el vecino de atrás, del costado. Y acá lo que se empezó a ver es: primero la madre, después el hijo, después el esposo», explicó Maglianese.

<br/>

## **Control biológico**

El 2 de julio de 2020 el Poder Legislativo de Santa Fe sancionó la declaración de la Emergencia Sanitaria por Dengue, por un plazo de tres meses, lo que facultó al Poder Ejecutivo a reasignar y disponer fondos para la lucha contra la enfermedad. Sin embargo, esta decisión fue calificada de tardía, tanto por Maglianese como por Picatto.

«El control terminó siendo el control natural de las bajas temperaturas: por debajo de los 8 grados el mosquito adulto, la larva y la pupa no sobreviven, se corta el ciclo y en a finales de junio pudimos decir que no hubo más casos de dengue», manifestó la directora del Programa de Vectores del Ministerio de Salud. Sin embargo, aseguró: «todo lo que pudimos hacer lo hicimos porque si no hubiera sido una catástrofe, tuvimos 5 mil casos y podrían haber sido 40 mil o 50 mil como sucedió en otras provincias».

«Los números más altos en la ciudad de Santa Fe los tuvimos en el mes de abril y por ahí ante ese número se pueden explicar esas decisiones legislativas», opinó Picatto, y agregó que «entendemos que las acciones fundamentales son las de prevención y pretendemos desarrollarlas todo el año, no solamente cuando empiezan a aparecer los brotes».

<br/>

## **Que no vuelva a pasar**

«Desde Región de Salud Santa Fe del Ministerio de Salud provincial venimos trabajando desde julio primero con el camión DetectAR y con Aborda Salud», contó Maglianese. «Se fue casa por casa, no ingresando a las viviendas, sino dando las recomendaciones para que tuvieran el patio limpio, ordenado y que no se criara el mosquito del dengue».

«A partir de octubre empezamos casa por casa dentro de los domicilios haciendo Descacharrado Sistemático Asistido. Ingresamos con una bolsita donde tenemos bolsas de consorcio, guantes anti cortes, gafas, todo elemento de protección personal, a cada vivienda para ayudar al vecino a acomodar el patio: retirar lo que no le sirve y dar vuelta lo que le sirve, y tapar herméticamente lo que tiene agua».

La directora destacó la labor entre el Ministerio de Desarrollo Social y el de Salud de la provincia, indicando que se ha conformado un muy lindo grupo humano. «Es un trabajo que es arduo porque tenemos altas temperaturas, mucha exposición al sol y otros problemas que puedan llegar a surgir como mordeduras de perros, accidentes. Pero estamos bien preparados con elementos de salud personal y los chicos tienen muchas ganas», concluyó.

Según Maglianese se avanzó en doce barrios de la ciudad de Santa Fe con las acciones domiciliarias, y se realizaron capacitaciones dentro de ciudades y comunas de los seis departamentos de la región. Actualmente se está trabajando en Alto Verde y luego se continuará por San Lorenzo y Yapeyú.

«Más de 5.800 domicilios visitamos en todo este periodo, con el calor, con el Covid. No hemos dejado de trabajar duramente para disminuir la densidad de mosquito», subrayó.

Por su parte en la ciudad las acciones de prevención y promoción se están desarrollando junto a Higiene Urbano, que dependen de la Secretaría de Ambiente de la ciudad. Estas organizaciones tienen presencia en el norte, borde oeste y costa y se encargan de la recolección de residuos y descacharrado, aunque desde el municipio buscan la instrucción del personal. Sobre todo, en Pompeya, el barrio más afectado, seguido por San Martín, Los Ángeles, Sargento Cabral y Roma.

«En el mes de noviembre comenzamos con las capacitaciones a estas organizaciones que son 26 y llevamos capacitadas 16. En enero o febrero finalizaremos con todas; son aproximadamente 260 personas», indicó el subdirector de Promoción de Salud.

<br/>

## **El dengue durante el 2021**

Consultada por el pronóstico para este año, Maglianese aclaró que en octubre y noviembre está la ventana que nos permite ver lo que está pasando en Brasil y Paraguay, y ya hay muchas alertas. «Pero lo que sabemos es que hay menos viajes a zonas endémicas por las restricciones o limitaciones económicas por lo que creemos que no vamos a tener la epidemia extraordinaria que tuvimos el año pasado».

La directora insistió en que son los turistas los que deben cuidarse de las picaduras de mosquitos cuando salgan del país. «La recomendación es que ahora que se viaja a zonas donde puede haber dengue, Brasil, Paraguay, Bolivia o cualquier otro país de Latinoamérica que tiene dengue todo el año, que por favor se lleve el repelente y lo utilice para no venir con el souvenir de vuelta, que es el dengue, y traiga el problema a su familia, su barrio y su ciudad».