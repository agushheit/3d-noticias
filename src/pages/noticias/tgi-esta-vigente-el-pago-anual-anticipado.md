---
category: La Ciudad
date: 2022-01-12T06:00:35-03:00
thumbnail: https://assets.3dnoticias.com.ar/TGI.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'TGI: está vigente el pago anual anticipado'
title: 'TGI: está vigente el pago anual anticipado'
entradilla: "Hasta el 21 de enero habrá plazo para acceder a la bonificación de un
  mes por cancelar en un solo pago la totalidad de los períodos de la Tasa General
  de Inmuebles del año que comenzó.\n\n"

---
La Municipalidad recuerda que junto a la distribución de las boletas de la Tasa General de Inmuebles (TGI) correspondientes al primer trimestre de 2022, cada contribuyente recibe en su domicilio otra boleta con la opción de cancelar el tributo anual anticipado. El plazo para acceder al beneficio es hasta el 21 de enero.

En caso de abonar la TGI anual se bonifica la cuota correspondiente a Enero 2022.Cabe mencionar, la TGI presenta variaciones entre el primer trimestre, el segundo, el tercero y el cuarto. Siempre de conformidad a los topes establecidos por ordenanza que están determinados según la zona inmobiliaria. En tal sentido, en las boletas que se reciben en los domicilios se puede observar claramente cómo es el descuento que se otorga.

**Qué hacer si no llega la boleta**

En caso de no haber recibido la boleta se puede acceder al beneficio de la siguiente manera: imprimirla ingresando a www.santafeciudad.gov.ar, apartado TGI ingresando con padrón y clave del contribuyente (ambos datos los puede obtener de la boleta de TGI anterior). Asimismo, se puede buscar en el Palacio Municipal, sin turno previo. Otra opción es comunicarse con el 0800 777 5000 y se le enviarán las boletas requeridas a través de un correo electrónico.

**Lugares de pago y beneficios adicionales**

Vale recordar que en todas las cajas municipales -Cementerio (Blas Parera 5401, de lunes a viernes de 7.30 a 13), Corralón Municipal (Pte. Perón 3575 de lunes a sábados de 8 a 14) y Palacio Municipal (Salta 2951 de lunes a viernes de 7.30 a 13.30)- y de distrito detalladas en la página web de la Municipalidad se puede abonar la TGI.

En el caso de quienes quieran abonar con tarjeta de crédito se recuerda que, sólo en las cajas municipales, siguen vigentes los siguientes beneficios adicionales: con tarjeta de crédito Visa o Mastercard del Banco Santa Fe hasta en 6 cuotas sin interés; con tarjeta de crédito Cabal del Banco Credicoop hasta en 6 cuotas sin interés; y con tarjeta Credifiar hasta en 3 cuotas sin interés