---
category: Estado Real
date: 2021-04-15T07:37:44-03:00
thumbnail: https://assets.3dnoticias.com.ar/covid-peatonaljpg.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Santa Fe no adhiere a las medidas de Nación, pero reforzará controles
title: Santa Fe no adhiere a las medidas de Nación, pero reforzará controles
entradilla: Así lo hizo saber el gobierno provincial. Este jueves habrá una reunión
  de expertos sanitarios de Santa Fe. Cualquier medida se consensuará con municipios.

---
El gobierno de la provincia de Santa Fe informó este miércoles a la noche que "continuará con la misma Modalidad de Convivencia en Pandemia" de coronavirus, más allá del anuncio formulado por el presidente Alberto Fernández para Amba. "El Gobierno de la provincia de Santa Fe, dará continuidad a las medidas implementadas hasta el momento en el marco de la segunda ola de coronavirus Covid-19", se consignó oficialmente.

"El gobierno de Santa Fe informa que se reunirá en la jornada de mañana jueves con el grupo de expertos asesores para actuar de la manera más rápida posible, y continuará monitoreando tres ítems: cantidad de camas críticas ocupadas, tasa de contagio y mortalidad", se aclaró.

Adicionalmente, se implementarán diversas medidas, según se anticipó: "Se fortalecerá la coordinación de acciones con el sistema de salud privado; se aumentarán los controles interprovinciales; y se incrementará la realización de los testeos cotidianos para la detección de nuevos casos de coronavirus".

"Los ministerios de Trabajo y Producción de Santa Fe también actualizarán y optimizarán los protocolos con el fin de prevenir nuevos contagios de coronavirus y mantener el actual ritmo del sistema productivo provincial", se agregó.

"Trabajaremos de forma conjunta y coordinada en toda la provincia, con la totalidad de los intendentes e intendentas, presidentes comunales y la sociedad civil", adelantó el gobierno de Santa Fe.

"La mejor prevención -indicó el comunicado- es el compromiso de todos en utilizar de manera correcta el barbijo, evitar las aglomeraciones, mantener la distancia social, lavarse frecuentemente las manos y respetar las disposiciones de la autoridad sanitaria".

"Por el momento, continuarán vigentes los contenidos dispuestos en el Decreto provincial Nº280 del pasado 9 de abril, que están basados en el objetivo de cuidar la salud de la población y mantener el pujante crecimiento del sistema productivo provincial", concluye la comunicación oficial.

![](https://assets.3dnoticias.com.ar/omar-perottijpg.jpg)

**Los argumentos**

Lo informado esta noche por el gobierno de la provincia de Santa Fe confirma lo que se intuyó desde un primer momento tras los anuncios de Alberto Fernández: se miran de reojo las restricciones impuestas para el Área Metropolitana de Buenos Aires (Amba), que incluye la ciudad de Buenos Aires y el Gran Buenos Aires. El aumento de casos de coronavirus en las últimas semanas en la provincia de Santa Fe fue notorio, pero aún está lejos de la exponencial tasa de contagios que se registró en el Amba.

Sobre el final del anuncio –que fue grabado en la Quinta de Olivos–, el presidente aclaró que los gobernadores pueden adherir a las nuevas restricciones, aunque eso aún no es obligatorio. Por ese motivo, habrá que esperar para conocer qué decisión tomará el gobernador Omar Perotti.

"En el Amba estamos viviendo la mayor velocidad de aumento de casos de coronavirus desde el inicio de la pandemia. Por lo tanto, es nuestra obligación tomar medidas adicionales y convocar a la población a un cambio, para lograr que avance el plan de vacunación y evitar la saturación del sistema de salud", dijo Alberto Fernández.

"El mayor riesgo de transmisión –agregó– se produce en las actividades sociales y recreativas nocturnas, donde no hay dos metros de distancia, se producen aglomeraciones, se usa escasamente el barbijo y también en espacios cerrados sin ventilación adecuada".

"El Amba constituye un único aglomerado urbano, uno de los más poblados de América. Tiene una única realidad epidemiológica, con un gobernador y un jefe de gobierno, y dos docenas de municipios", aseguró Alberto Fernández y dijo que a partir de las 0 horas del día viernes 16 de abril se restringirá la circulación nocturna entre las 20 y las 6; quedan suspendidas las actividades recreativas, sociales, deportivas y religiosas en lugares cerrados; las actividades comerciales cerrarán a las 19; las actividades gastronómicas funcionarán en modalidad de entrega a domicilio luego de las 19.

Por otra, parte, desde el lunes 19 hasta el viernes 30 de abril se retoma la modalidad virtual en los tres niveles educativos en el Amba.