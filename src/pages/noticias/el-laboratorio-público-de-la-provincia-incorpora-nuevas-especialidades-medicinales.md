---
layout: Noticia con imagen
author: .
resumen: Laboratorio LIF
category: Estado Real
title: El laboratorio público de la provincia incorpora nuevas especialidades
  medicinales
entradilla: Se trata de la Atorvastatina LIF 20mg, la Claritromicina LIF 500 mg
  y el Losartán LIF 50 mg que se producen íntegramente en el Laboratorio
  Industrial Farmacéutico.
date: 2020-11-15T14:35:41.217Z
thumbnail: https://assets.3dnoticias.com.ar/lif.jpg
---
El Laboratorio Industrial Farmacéutico (LIF) incorporó tres nuevas especialidades medicinales que al igual que toda su producción, se distribuirán de manera gratuita en el sistema de salud pública de la provincia.

En un año atravesado por la pandemia del covid 19, el LIF SE respondió a la coyuntura sanitaria y además siguió impulsando nuevos procesos productivos e incorporando en los mismos la Atorvastatina LIF 20mg, la Claritromicina LIF 500 mg y el Losartán LIF 50 mg que se producen íntegramente en sus instalaciones a través del trabajo interdisciplinario de sus trabajadores y trabajadoras.

Sobre dichas especialidades, la directora Técnica de la institución, Cecilia Selis sostuvo: “Representa un gran impacto de crecimiento en las capacidades que tiene el laboratorio" en tanto que con respecto a "nuestros recursos humanos, es un incentivo para un plantel comprometido y calificado para llevar adelante cada propósito que se instala, siempre para atender a las demandas del sistema de salud pública. También nos sirve para seguir creciendo en materia de tecnología, para poder sostener los niveles de producción y continuar incorporando nuevas especialidades cada año”, añadió.

Por su parte, Analía San Román, que integra el directorio del LIF, afirmó: “La incorporación de nuevas especialidades medicinales es uno de nuestros propósitos fundamentales. Entendemos que el objetivo de los medicamentos es mejorar la salud de la población y su calidad de vida, permitiéndoles vivir más y mejor. Es una enorme satisfacción ofrecer sin cargo a la ciudadanía nuevos desarrollos, garantizando la eficacia y seguridad de los productos que fabricamos”, insistió.

En este sentido, la funcionaria concluyó: “Desde el LIF contribuimos a preservar la salud como estado de bienestar general, al permitir a los pacientes iniciar el tratamiento que corresponda, previniendo el desarrollo de ciertas enfermedades, mejorando así la calidad de vida y garantizando el acceso gratuito a los medicamentos”.