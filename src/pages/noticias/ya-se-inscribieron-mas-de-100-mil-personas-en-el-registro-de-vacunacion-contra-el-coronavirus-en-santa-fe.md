---
category: Estado Real
date: 2021-02-23T07:03:34-03:00
thumbnail: https://assets.3dnoticias.com.ar/vacunas-geriatricos.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: Ya  se  inscribieron más de 100 mil personas en el Registro de Vacunación
  contra el Coronavirus en Santa Fe
title: Ya  se  inscribieron más de 100 mil personas en el Registro de Vacunación contra
  el Coronavirus en Santa Fe
entradilla: El Ministerio de Salud recomienda priorizar la inscripción de mayores
  de 70 años.

---
El gobierno provincial, a través del Ministerio de Salud, destalló que este lunes se inscribieron más de 100 mil personas en el registro online de vacunación contra el Coronavirus, en el marco del plan Santa Fe Vacuna.

Al respecto, la ministra de Salud, Sonia Martorano, recordó que “el registro tiene como propósito saber cuántos santafesinos y santafesinas desean vacunarse; y, asimismo, brindar los turnos para la vacunación, de acuerdo a cada grupo y según las edades”.

“En esta etapa se inoculará a los mayores de 70 años, una vez cubierta la totalidad del personal de Salud de toda la provincia”, explicó.

Paralelamente, hoy también se inició en la provincia la vacunación a personas institucionalizadas en geriátricos y al personal que los asiste; en tanto presentan mayores riesgos de morbimortalidad dada la avanzada edad y las condiciones propias de la institucionalización.

Ante cualquier inconveniente para realizar el trámite, los interesados podrán acercarse al Centro de Salud más cercano para completar el proceso.

**“Pedimos priorizar a los adultos mayores”**

En otro orden, la ministra Martorano explicó que “si bien el registro está abierto a toda la población, pedimos, recomendamos, que se anoten los mayores de 70 años, que es el grupo etario más vulnerable y el que desde Salud de la Nación se establece como prioritario, según el plan estratégico que se lleva adelante en todo el país”.

“Apelamos a la paciencia, comprensión y a la solidaridad de todos y todas, para no sobrecargar el sistema de inscripción online que, naturalmente, ante tanta demanda puede tener fallas. Estamos recibiendo las vacunas necesarias para iniciar progresivamente la vacunación de toda la población destinataria en esta primera etapa en la provincia”, concluyó la ministra.

La inscripción se realiza en [https://www.santafe.gob.ar/santafevacunacioncovid](https://www.santafe.gob.ar/santafevacunacioncovid "https://www.santafe.gob.ar/santafevacunacioncovid")