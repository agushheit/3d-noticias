---
category: La Ciudad
date: 2022-01-20T06:00:36-03:00
thumbnail: https://assets.3dnoticias.com.ar/SONIA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Martorano rechazó de plano el pase sanitario escolar
title: Martorano rechazó de plano el pase sanitario escolar
entradilla: No obstante, pese al 64% de niños de entre 3 y 11 años con esquema completo,
  la ministra de Salud manifestó que "no estamos en los niveles que queremos".

---
Durante la primera semana de febrero comienza el regreso a clases de aquellos alumnos que deben reforzar contenidos, en medio de un debate que se instaló respecto a una eventual implementación del pase sanitario en las escuelas. Respecto a esto, la ministra de Salud provincial Sonia Martorano negó la posibilidad de que se implemente esta medida en los colegios, asegurando que "primero está el derecho a la educación".

La titular de la cartera sanitaria manifestó: "De cara al inicio de clases estamos incentivando a la vacunación y apostamos fuertemente a la presencialidad. Hemos puesto las terceras dosis a la población docente y seguimos vacunando a la población de menores".

"Entendemos fuertemente que debemos incentivar y reforzar la vacunación, si logramos que docentes y alumnos estén vacunados alguno que tenga una contraindicación va a estar protegido porque la mayoría está inmunizada. No veo que exista el pase sanitario en las escuelas, en principio no sería algo prudente porque primero está el derecho a la Educación", sostuvo la ministra restando chances de un pase sanitario en establecimientos educativos.

**Preocupan las internaciones de menores**

En tanto avanza la pandemia con la proliferación de la variante Ómicron, el sistema de salud santafesino ve como cada día se suman los pacientes pediátricos en las terapias. En la zona centro norte de la provincia (que abarca a la ciudad de Santa Fe) se encuentran internados dos pacientes en neonatología, cinco menores en terapia intensiva, dos en terapia intermedia y once en sala general. "No son tantos pero nunca había hasta el momento, esto es nuevo", puntualizó Martorano.

Por fuera de la discusión del pase sanitario, la ministra destacó que la vacunación de chicos que aún no estén inmunizados "no puede esperar más". Y sobre esto agregó: "Hay gente que esperó, hay pediatras que recomendaron esperar y la verdad es que no se puede esperar más, pedimos por favor que se vacunen. También pedimos por favor que las mamás embarazadas deben vacunarse, es la única forma de proteger al recién nacido y a los niños que no se puede vacunar".

"Lo que vemos por primera vez en la pandemia son pacientes pediátricos internados con Covid y tienen que ver con la no vacunación, solo en algunos casos de niños vacunados con comorbilidades llegaron a internación. En general los niños internados tienen de 0 a 3 años, que no se pueden vacunar, con casos mayores de 3 y tres neonatos que están internados porque su madre tiene solo una dosis o no están vacunados", continuó Martorano.

**Números de vacunación**

En cuanto a números de vacunación en la población de 3 a 11 años, la ministra afirmó que a nivel provincial la cifra es del 64% de inoculados con esquema completo. Sobre esto afirmó que si se compara con los niveles nacionales Santa Fe se encuentra por encima de la media, aunque agregó que el porcentaje "no está en los niveles que queremos".

"Hay un 18% que se colocó la primera dosis y en 28 días se le coloca la segunda por ser Sinopharm, deben estar atentos porque una dosis no es inmunización completa. En el grupo de 12 a 17 tenemos un 80% de vacunación con esquema completo", continuó sobre esto Martorano.

Este jueves en la provincia se comenzarán a aplicar las dosis de refuerzo (o tercera dosis) libres en gestantes o lactantes con plazo mínimo de 4 meses desde la segunda dosis.

**Otras miradas**

El expresidente de la Sociedad Argentina de Pediatría (SAP) Omar Tabacco advirtió este miércoles que es necesario "acelerar" la vacunación en niños y adolescentes durante los meses de enero y febrero para garantizar que "el aula es un lugar seguro" para el dictado de clases presencial, con vistas al inicio del próximo ciclo lectivo.

Tabacco consideró que la aplicación de la tercera dosis para los docentes y los auxiliares de los establecimientos educativos "permitiría generar un ambiente de protección importante”, al subrayar que “las clases tienen que ser presenciales”.

En medio del debate sobre una implementación del pase sanitario en escuelas, el médico pediatra insistió con la necesidad de vacunar a los niños porque "están con mayor cantidad de positivos” de coronavirus, y reiteró que “no cabe ninguna duda de la seguridad y eficacia de la vacuna”.