---
category: Agenda Ciudadana
date: 2021-06-28T06:15:19-03:00
thumbnail: https://assets.3dnoticias.com.ar/ClasesPresenciales.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Quiénes vuelven a las clases presenciales este lunes en Santa Fe
title: Quiénes vuelven a las clases presenciales este lunes en Santa Fe
entradilla: El nivel inicial y primario tendrá presencialidad en toda la provincia.
  El nivel secundario dependerá de la situación epidemiológica del departamento.

---
Durante las últimas horas, el gobierno provincial realizó varias comunicaciones con novedades sobre la presencialidad en las escuelas de Santa Fe, que tendrá una nueva modalidad a partir de este lunes.

Desde la semana que viene, volverán a la presencialidad todos los alumnos de la provincia que cursen en el nivel primario.

De esta manera, la primaria se suma al nivel inicial, que ya había comenzado las clases presenciales.

**¿Y la secundaria?**

A diferencia del nivel inicial y primario, que este lunes tendrán presencialidad en todo el territorio provincial, el nivel secundario volverá a las aulas, pero solo en los departamentos que no se encuentran en estado de alarma epidemiológica.

De esta manera, teniendo en cuenta la cantidad de casos de coronavirus registrados, el nivel secundario no vuelve esta semana a la presencialidad en los departamentos La Capital y Rosario. En el resto de la provincia, la secundaria volverá a la presencialidad.

Al respecto, este viernes, el gobernador Omar Perotti expresó: "La idea es volver en todas las ciudades y en toda la provincia con presencialidad plena desde el lunes, en el nivel inicial y primario. Y analizaremos en qué lugares podemos volver con el secundario. La idea es que las tres burbujas del secundario puedan tener al menos un encuentro con sus docentes antes de las vacaciones"

"Tenemos la tranquilidad de tener a todos los docentes vacunados, incluso a los reemplazantes recién inscriptos. Tenemos números que nos gustaría sean más bajos, pero están en descenso. Y números de camas que nos dan una señal positiva, aunque queremos que siga bajando. Lo que tenemos son dos semanas de aquí a las vacaciones, y queremos que los que más lo necesitan, los más pequeños, puedan ya retomar y aprovechar estas dos semanas el vínculo con sus docentes. Ojalá después de las vacaciones la situación siga mejorando y podamos volver a pleno".