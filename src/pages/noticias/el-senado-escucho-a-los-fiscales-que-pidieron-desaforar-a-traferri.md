---
category: Agenda Ciudadana
date: 2020-12-18T10:53:31Z
thumbnail: https://assets.3dnoticias.com.ar/1812fiscales.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: El Litoral'
resumen: El Senado escuchó a los fiscales que pidieron desaforar a Traferri
title: El Senado escuchó a los fiscales que pidieron desaforar a Traferri
entradilla: Se aceleran los tiempos en la Cámara alta ante la acusación contra el
  presidente del bloque Juan Domingo Perón. Los senadores quieren ver si hay pruebas
  y no están conformes con lo que se presentó por escrito.

---
La Comisión de Asuntos Constitucionales y Legislación General y de Peticiones, Reglamento y Juicio Político recibieron este jueves 17 de diciembre, pasadas las 11, a los fiscales Luis Schiappa Pietra y Matías Edery, así como al fiscal general Jorge Baclini.

El encuentro fue a puertas cerradas y no se permitió el ingreso a la prensa, en la sede de la primera de las comisiones mencionadas, que preside el senador Joaquín Gramajo (PJ-JDP-9 de Julio). La restante está en cabezada por su par, Lisandro Enrico (UCR-General López).

Así lo acordaron en la tarde noche del miércoles 16 los integrantes de ambas comisiones, luego de la sesión en la que tomó estado parlamentario el pedido del Ministerio Público de la Acusación que pide al cuerpo legislativo la autorización para imputar al senador Armando Traferri, presidente del bloque Juan Domingo Perón (PJ-JDP- San Lorenzo).

Al cierre de esta edición, el encuentro continuaba, dos horas después de haberse iniciado. Se explicó que se ha previsto una exposición oral de los fiscales de una hora, en la que se incluyen grabaciones de escuchas telefónicas y –presumiblemente- otros elementos que hagan más consistente su presentación por escrito.

Antes de ingresar, el senador Enrico dijo a los periodistas: «por lo que hemos leído en esas 17 hojas hay una sospecha generalizada, pero verosimilitud aún no hemos encontrado… vamos a ver qué es lo que se nos muestra ahora», expresó.

Dijo que se cumple con la reunión pedida por los fiscales porque predomina entre los integrantes de ambas comisiones ese concepto: con lo que se ha escrito no hay elementos que permitan dar un paso como el de quitarle a un legislador sus inmunidades constitucionales e iniciarle un proceso penal.

# «**Es Sain**»

Traferri sostiene que la causa responde a un «carpetazo» del ministro de Seguridad de la Provincia, Marcelo Sain, quien a su vez por esas expresiones le reclamó que ratifique o rectifique sus dichos, con la finalidad de iniciarle acciones penales (lo que otra vez demandaría reunir al Senado). El senador por San Lorenzo está formalmente acusado por la Fiscalía Regional de Rosario de haber creado una red de protección política y judicial al juego ilegal, el texto que ingresó al Senado lo menciona como «un organizador» de ese supuesto esquema.

En la sesión mencionada, Traferri renunció a la presidencia y a pertenecer a la Comisión de Juicio Político, la que quedó bajo la titularidad del senador Lisandro Enrico (UCR-General López). Es rigor, días atrás se había dicho que la comisión aún no tenía designado un presidente, y en el recinto se informó que lo era el senador al que comprometió con su declaración judicial el ex fiscal Gustavo Ponce Asahad, preso por cobrar coimas a empresarios de esa actividad ilícita.

# **Posición**

En tanto, fue el presidente de la Comisión de Asuntos Constitucionales, Gramajo (PJ-JDP- 9 de Julio), el encargado de coordinar a la reunión conjunta que se definió en cuestión de horas, tras dejar el recinto.

Esta mañana, en declaraciones radiales el senador José Baucero (PJ-JDP-San Javier) expresó su convicción de que Traferri es inocente y abonó la explicación del sureño: es una maniobra de Sain, sostuvo, palabras más palabras menos.

Los fiscales Luis Schiappa Pietra y Matías Edery deben aportar pruebas «más sólidas», según han dicho en estricto _off the record_ legisladores del oficialismo en sus tres bloques, así como del interbloque de la oposición. Deben avalar su pedido para que los senadores priven a uno de sus pares de las inmunidades parlamentarias previstas por la Constitución de la Provincia de Santa Fe.

La comisión de Peticiones, Reglamento y Juicio Político, ahora bajo la presidencia de Lisandro Enrico (UCR-Gral. López), ha quedado constituida también por Joaquín Raúl Gramajo (PJ-JDP-9 de Julio), José Baucero (PJ-JDP-San Javier), Ricardo Kaufmann (PJ-Lealtad-Garay), Eduardo Rosconi (PJ-Bloque Unipersonal-Caseros) y Rodrigo Borla (UCR-San Justo), es decir, seis (6) miembros integrantes, de los cuales cuatro (4) son abogados de reconocida trayectoria.

La comisión de Asuntos Constitucionales y Legislación General, presidida por Joaquín Raúl Gramajo (PJ-JDP-9 de Julio), está conformada también por los senadores Armando Traferri (PJ-JDP-San Lorenzo), Osvaldo Sosa (PJ-JDP-Vera), Rubén Pirola (PJ-JDP-Las Colonias), Alcides Calvo (PJ-Lealtad-Castellanos), Guillermo Cornaglia (PJ-JDP-Belgrano), Rodrigo Borla (UCR-San Justo), Lisandro Enrico (UCR-Gral. López), Orfilio Marcón (UCR-Gral. Obligado) y Germán Giacomino (UCR-Constitución), es decir, diez (10) integrantes.

# **Sin plazos**

La cita fue pautada en la sede de Asuntos Constitucionales, en Illia al 1700, no muy conveniente para que se pueda respetar la distancia sanitaria entre los asistentes, que bien pueden ser más que los nombrados, ya que cualquier senador puede concurrir a las reuniones de alguna de las comisiones de la Cámara aunque no la integre.

Los fiscales han dicho en declaraciones periodísticas que un encuentro cara a cara con los representantes de los departamentos les facilitará su trabajo, ya que confían en la calidad de las pruebas que -según subrayan- han reunido. 

Tras la reunión, las comisiones pueden llevar adelante otras y también convocar a otras figuras, en una labor para emitir un dictamen (a elevar a los 19 senadores) que no tiene exigencia alguna de tiempos, ni plazos. Sin embargo, el compromiso público de los integrantes de la cámara alta es proceder con «celeridad».

Un eventual desafuero de cualquier legislador requiere de los votos positivos de los dos tercios de los legisladores presentes, es decir, de 13 votos si cada representante ocupada su banca en el recinto o de manera remota.

Además, para las 15, la vicegobernadora y presidenta del Senado, Alejandra Rodenas, en acuerdo con los jefes de los bloques, convocó a los senadores a su segunda sesión extraordinaria con el objetivo de tratar y/o aprobar con media sanción, según lo consensuado, el Mensaje 4909 del Poder Ejecutivo, mediante el cual se establece la política tributaria que será aplicable a partir del período fiscal 2021, el que sería comunicado a la Cámara de Diputados que para ello, ha sido convocada para el próximo martes 22 de diciembre.