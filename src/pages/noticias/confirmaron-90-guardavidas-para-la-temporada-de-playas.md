---
layout: Noticia con imagen
author: "Fuente: Diario UNO"
resumen: Confirman 90 guardavidas
category: La Ciudad
title: Confirmaron 90 guardavidas para la temporada de playas
entradilla: "Mientras tanto, desde el municipio analizan dos condiciones para la
  inauguración de la temporada el 15 de noviembre: la evolución de la curva de
  contagios por Covid-19 y la bajante del río."
date: 2020-10-22T17:22:43.382Z
thumbnail: https://assets.3dnoticias.com.ar/guardavidas.jpg
---
A casi tres semanas del tradicional 15 de noviembre, jornada de inauguración de la temporada de playas en la ciudad por excelencia, ya se confirmó la presencia de 90 guardavidas para el cuidado de las mismas. Al mismo tiempo, la Municipalidad de Santa Fe, analiza dos condiciones vitales y fundamentales de cara a la clásica fecha de estreno de playas: la observación minuciosa de la evolución de la curva de contagios por Covid-19 en la ciudad y la situación de la bajante histórica del río.

Este verano en Santa Fe tendrá diversas y complejas particularidades. Por un lado, un gran porcentaje de santafesinos que no va a poder (ni se va a animar) a salir de vacaciones a otros destinos turísticos por la cuarentena y también por la situación económica actual del país; y la bajante histórica de la cuenca del río Paraná, que ya provocó complicadas situaciones por la falta de conciencia de los ciudadanos hacia el río.

En diálogo con UNO Santa Fe, el referente local de Sugara (Sindicato Único de Guardavidas y Afines de la República Argentina), Sergio Berardi, habló sobre el problema actual que se puede presentar en la ciudad en una temporada estival "atípica".

"Consideramos que la gente se va a apropiar del espacio público de cualquier modo, independientemente de las restricciones existentes. Creemos que vamos a afrontar una temporada estival intensa y atípica desde todo punto de vista. Inclusive, se puede llegar a volver hostil si no contamos con la colaboración de los organismos de control y fundamentalmente con el compromiso de los santafesinos", sentenció el referente de los guardavidas en la ciudad.

Respecto al abordaje del trabajo de los guardavidas para la temporada de verano que se aproxima, este jueves se realizará un importante encuentro virtual con una capacitación especial laboral, de cara al escenario de trabajo y teniendo en cuenta el contexto de pandemia, como así también el manejo de protocolos y prevención a aplicar.

"Además de todo el trabajo que estamos realizando, se abrió esta semana un escalafón de guardavidas para cubrir vacantes y suplencias en el caso de confirmar casos positivos de Covid-19 en el staff de trabajadores", confirmó Sergio Berardi.

![](https://assets.3dnoticias.com.ar/guarda2.jpg)

"Tenemos que arbitrar por todos los medios, la forma de no arriesgar ni afectar la capacidad que tiene instalado el sistema sanitario de camas para afectados por Covid-19", subrayó el referente de Sugara en la ciudad y continuó: "Estamos pidiendo una reunión para la elaboración de un protocolo para el bañista, algo que entendemos fundamental de cara a la temporada de verano. Existe ya un protocolo de actuación a nivel nacional para el guardavidas, pero el santafesino debería conocer con tiempo cómo relacionarse con las playas y el río".

"Seguimos sosteniendo nuestra postura de comenzar la temporada de playas desde el 1 de noviembre para que la actividad se vuelva más ordenada. No puede ser que los guardavidas lleguen a las playas a llevar orden, cuando los santafesinos con el calor se acercan antes del 15 de noviembre", reclamó Berardi respecto a la demanda que le vienen realizando al Ejecutivo municipal año tras año.

Hoy la bajante histórica del río ofrece una posibilidad muy importante para usufructuar en un contexto de pandemia de la mano de la necesidad imperiosa de distanciamiento en la utilización de las playas como solárium. "Tenemos playas, por ejemplo en el sector oeste de la ciudad, de más de 15 mil metros cuadrados para trabajar con seguridad y no estar alertando a la gente de la importancia de mantener la distancia", resaltó Sergio Berardi y finalizó: "Tenemos también que trabajar en la irresponsabilidad y falta de respeto que la gente le tiene al río. La bajante en algunas playas, presenta pozos profundos y barrancas pronunciadas. Si los santafesinos se vuelcan de forma masiva al río, va a ser muy difícil controlar la situación.