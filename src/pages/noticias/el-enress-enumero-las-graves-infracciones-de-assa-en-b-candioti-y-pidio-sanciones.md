---
category: La Ciudad
date: 2021-10-05T06:00:39-03:00
thumbnail: https://assets.3dnoticias.com.ar/AGUASDETAMARINDOjpg.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: El Enress enumeró las graves infracciones de Assa en Bº Candioti y pidió
  sanciones
title: El Enress enumeró las graves infracciones de Assa en Bº Candioti y pidió sanciones
entradilla: El ente de control iniciará un proceso sancionatorio para determinar la
  responsabilidad de la empresa por incumplimientos en la calidad de agua y en los
  deberes de información hacia los usuarios.

---
El Ente Regulador de Servicio Sanitarios (Enress) confirmó en las últimas horas que iniciará un proceso sancionatorio contra Aguas Santafesinas S.A. (Assa) por el incidente en barrio Candioti.

Así lo comunicó el directorio del organismo, a través de una resolución en la que determina "el incumplimiento" de la empresa prestadora de agua "a las normas de calidad del servicio público de agua potable por red" y en los deberes de información hacia los usuarios y al propio organismo regulador.

En esa zona de la ciudad se comprobó, luego de la denuncia de usuarios que hablaban de un agua "turbia, de color blanca y es muy espesa", que el agua no era apta para consumo humano por sus altas concentraciones de aluminio y plomo.

El documento lleva la firma de los directores del Enress, Anahí Rodriguez, Leonel Marmirolli y de Oscar Pintos. Además de realizar una crónica completa del evento, describe las graves infracciones en las que incurrió la empresa.

La resolución del Enress destaca entre sus considerandos "que no está acreditada la afirmación de que ese día (14/09/21, fecha en la que Assa recibió reclamos de vecinos) la empresa "haya informado a los usuarios puntualmente y en general a la población, a través de los medios de comunicación".

El ente regulador define que "lo actuado por la empresa no es conforme o acorde a las obligaciones impuestas por normas expresas y claras, por los siguientes motivos:

_- No cortó el suministro de agua_

_- No proveyó suministros alternativos para satisfacer higiene y bebida_

_- No adoptó las medidas correctivas en forma gratuita dado que el problema no tenía causa adecuada en las instalaciones de los usuarios_

_- No comunicó al Ente Regulador de manera inmediata sobre las anomalías en la calidad del agua, ni dentro de las 24 horas sobre las denuncias recibidas y las respuestas dadas a los denunciantes_

_- No informó de inmediato a la población a través de, al menos, tres medios de comunicación masiva: prensa, radiodifusión y televisión de difusión general p canales de aire._

En esa línea, de cara al inicio del procedimiento disciplinario, la resolución apunta que "Assa incumplió con las normas de calidad del servicio público de agua potable por red al no cumplir con los parámetros pH, turbiedad, aluminio, manganeso y plomo" (en tres muestras) y con los deberes de información con relación a los usuarios".

El organismo de contralor plantea el inicio de un "proceso de aplicación de sanciones por la presunta comisión conjunta o alternativamente" de las siguientes supuestas infracciones:

_-Cualquier incumplimiento del deber de información establecido en el reglamento del usuario_

_-Incumplimiento a los deberes de cooperación para con el Ente Regulador_

_-Cualquier omisión sin consecuencias graves en los procesos propuestos de control de calidad de agua cruda, en tratamiento y tratada._

_-Incumplimientos en los parámetros admitidos de calidad del agua potable, sin consecuencias graves._

_-Incumplimiento de los parámetros admitidos de calidad del agua potable con consecuencias graves, evaluadas en función del riesgo creado para la salud de la población_

El inicio del proceso sancionatorio fue comunicado tanto a Assa, como a la autoridad de aplicación (Ministerio de Infraestructura de la provincia) quien podrá en el plazo de 10 días la suspensión de dicho proceso. De no ser así, se deberá intimar a Assa para que en el termino improrrogable de 5 días presente su descargo y ofrezca pruebas.

###### **"Blanca y espesa"**

El fenómeno afectó especialmente a vecinos residentes en las calles Sargento Cabral y Gobernador Candioti; a viviendas próximas y cercanas a la planta potabilizadora.

Debido a los reclamos recibidos por usuarios "personal de la Gerencia de Control de Calidad" del Enress "extrajo muestras de agua para su posterior análisis químico y microbiológico" y "requirió al prestador que implemente las acciones correctivas y preventivas necesarias para asegurar la calidad del agua provista de acuerdo a la normativa vigente".

Los análisis de tres muestras de agua tomadas de tres domicilios diferentes, revelaron que "las concentraciones de aluminio detectada la torna no apta para el consumo humano".

Días después, este medio pudo confirmar que se trató de Policroruro de Aluminio, un insumo químico secundario que se utiliza para el tratamiento del agua. Al revisar sus instalaciones internas de la planta potabilizadora, Assa detectó que la fuente del problema se encontraba en una estación de dosificación de PAC (Policroruro de Aluminio) que funciona con el aporte de agua de red, mediante un sistema de inyección".