---
category: La Ciudad
date: 2021-09-13T06:16:25-03:00
thumbnail: https://assets.3dnoticias.com.ar/CONCEJALES.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: "¿Cuántos votos necesitan los precandidatos a concejales para ir a la general?"
title: "¿Cuántos votos necesitan los precandidatos a concejales para ir a la general?"
entradilla: Comenzó el escrutinio provisorio que servirá de filtro y definirá quiénes
  son los que van a participar el 14 de noviembre en las generales.

---
Finalizada la votación, las alianzas políticas aguardan con una tensa calma los resultados de las Paso sobre cuales precandidatos competirán el 14 de noviembre en las elecciones generales de concejales.

#### Piso de votos

En la ciudad de Santa Fe hay 42 listas de concejales que el 12 de septiembre competirán en las Paso de 17 partidos o frentes que intentarán tener representación en las elecciones generales del 14 de noviembre próximo. Las elecciones Primarias, Abiertas, Simultáneas y Obligatorias (Paso), además de ordenar la contienda interna de cada fuerza política, también resultan un filtro para acceder a los comicios generales.

Los frentes y partidos deben conseguir determinada cantidad de votos para poder participar de la elección general. Eso está normado en la Ley 12.367, que establece para la categoría de concejales el umbral mínimo del 1,5 por ciento del padrón.

En tanto, las listas de precandidatos que compiten en la interna deben conseguir el 1,5 por ciento del total de los votos emitidos para poder entrar en el reparto de los lugares para la conformación de la lista que cada frente presentará en los comicios generales.

Para esa conformación final de las listas el ordenamiento de los candidatos se realizará con el sistema D´Hont. Para conocer cuál será el umbral mínimo a superar por las nóminas habrá que esperar a conocer la cantidad de electores que emitan su voto el 12 de septiembre. Además, ese armado debe contemplar la Ley 14.002 de paridad de género que se aplicará por primera vez en la provincia. Por lo tanto, cada dos lugares siempre tiene que haber una persona de cada género.

#### El número que todos miran

Salvo los frentes con mayores chances electorales, como el Frente Progresista, el Frente de Todos y Juntos por el Cambio, el resto de las fuerzas políticas tienen un primer objetivo que es superar el piso impuesto en las Paso para tener una chance de pelear por una banca en las generales.

En la ciudad de Santa Fe hay 311.588 electores, por lo que los 17 partidos y frentes de los precandidatos necesitan 4.673 votos para garantizarse la participación en las generales. Para los pequeños partidos no es un número fácil de alcanzar y eso se refleja en la cantidad de listas que compitieron en las últimas elecciones generales.