---
category: Agenda Ciudadana
date: 2021-11-13T06:00:19-03:00
thumbnail: https://assets.3dnoticias.com.ar/nafta.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Combustibles: "Para el 25 de noviembre podría haber faltante"'
title: 'Combustibles: "Para el 25 de noviembre podría haber faltante"'
entradilla: Así lo expresó Alberto Boz, de FAENI. Aseguró que creció el consumo combustible
  pero las estaciones están "cuotificadas" en las ventas. "No sabemos que pasará con
  el precio", indicó.

---
Más de 7 meses pasaron desde el último incremento fuerte de los combustibles, luego que del lapso de enero a mayo hayan aumentado en el orden del 35 por ciento los precios en las pizarras de las estaciones de servicio. A días de las elecciones generales, existe en el mercado una gran incertidumbre sobre un posible aumento.

Si bien los niveles de consumo repuntaron de la mano de la recuperación del ritmo comercial tras las restricciones, hoy las estaciones en Santa Fe están en un 90 por ciento de comercialización de combustibles de los niveles registrados pre pandemia.

A nivel nacional, el presidente de YPF, Pablo González, negó en las últimas horas un aumento de precios de combustibles para el mes de diciembre y destacó, por otro lado, el crecimiento de la producción y la inversión por parte de la petrolera estatal.

UNO Santa Fe consultó al presidente de la Federación Argentina de Expendedores de Nafta del Interior (FAENI), Alberto Boz, sobre el posible escenario post electoral con los precios de los combustibles, sobre un potencial aumento y sobre los rumores de un probable desabastecimiento.

Crece la incertidumbre en el mercado por un posible aumento de precios de los combustibles y faltante para fin de noviembre. Foto: archivo UNO Santa Fe.

"No se sabe que puede pasar el lunes luego de las elecciones con el precio de los combustibles. Sí sabemos que existe un fuerte factor electoralista en la composición de los precios de las naftas, pero la realidad indica que no tenemos información concreta de lo que puede pasar", comenzó expresando Boz.

"El precio del combustible no fue modificado desde de mayo, mientras que desde dicho mes y hasta la actualidad, la cotización del dólar oficial sí lo hizo, al igual que el barril de crudo, el biodiésel y el aumento en el bioetanol con el que se realizan los cortes. Todo indicaría que algún nivel de ajuste debería existir en los próximos días, pero no del porcentaje de desfasaje que se habla, en el orden del 17 por ciento; es ilógico", resaltó el presidente del FAENI. Vale recordar que de enero a mayo de este 2021, los combustible aumentaron en el orden del 35 por ciento.

"Estamos con un grave problema sobre los precios en el canal mayorista, ya que las petroleras no aumentan en las estaciones de servicio y las estaciones blancas, que se nutren de ese canal mayorista, están con problemas de precios, pagando entre 15 y 20 pesos más el gasoil que en una estación de bandera. Todos estos factores indican que las petroleras están a la espera de algún tipo de aumento para luego liberar un poco más de volumen al mercado", sostuvo Alberto Boz.

Si bien comenzó de a poco a observarse mayor actividad comercial y por ende, mayor consumo de combustible, el referente del FAENI aseguró que "todavía estamos por debajo de los valores pre pandemia, superando el 90 por ciento en todas las estaciones. Hay que aclarar que antes de la pandemia, veníamos de cuatro años que se vendía todos los meses menos en valores comparativos interanual

"El agravante por estos días es que el precio mayorista aumentó, las estaciones de servicio blancas tienen valores elevados y el consumo migró a las estaciones de bandera, las cuales a la actualidad, ya están cuotificadas, es decir, que pueden comercializar de gasoil lo que se vendió el mes anterior", manifestó Boz y finalizó: "El consumo está registrando un incremento, pero al mismo tiempo las petroleras esperan el ajuste de precios para liberar más combustible en el mercado. Para el día 25 vamos a estar hablando de faltante de combustible en las estaciones de servicio".