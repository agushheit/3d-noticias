---
category: La Ciudad
date: 2021-02-10T06:46:41-03:00
thumbnail: https://assets.3dnoticias.com.ar/casabomba.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: Se presentaron 5 ofertas para ejecutar obras complementarias en la Casabomba
  0
title: Se presentaron 5 ofertas para ejecutar obras complementarias en la Casabomba
  0
entradilla: Se realizarán trabajos de herrería para la construcción de barandas, rejas,
  tapas y escaleras metálicas para la estación de bombeo cero de la ciudad de Santa
  Fe.

---
El Ministerio de Infraestructura, Servicios Públicos y Hábitat, abrió los sobres con las ofertas para la construcción de barandas, rejas, tapas y escaleras metálicas para la Estación de Bombeo 0 (EB 0), ubicada en el barrio Centenario sobre la avenida Circunvalación de la ciudad de Santa Fe.

Al respecto, la ministra de Infraestructura, Servicios Públicos y Hábitat, Silvina Frana, manifestó que “esta licitación complementa con medidas de seguridad, tanto quienes operan la obra como para la propia estación de bombeo. Estos trabajos permitirán terminar con una obra tan anhelada por los vecinos de los barrios Centenario, Chalet, San Lorenzo y Fonavi San Jerónimo.

“Desde el inicio de nuestra gestión, nos propusimos trabajar cerca de la gente con obras a corto y largo plazo, que impactan de manera directa en la cotidianeidad de cada barrio, y mejoran la calidad de vida", concluyó Frana.

Por su parte, el secretario de Recursos Hídricos, Roberto Gioria, detalló que "se trata de trabajos de herrería para ejecutar rejas, tapas, escaleras metálicas y barandas ante la necesidad de incorporar protección y cerramiento a la obra, con el objetivo de mejorar las condiciones de trabajo y seguridad del personal que opera la Estación de Bombeo, que va ayudar al drenaje ante una crecida del río Paraná, cubriendo una amplia zona del sur de la ciudad, con lo cual se van a minimizar los riesgos de inundación”.

**Ofertas**

Los trabajos tiene un presupuesto oficial de $ 6.129.795,33 y un plazo de ejecución de tres meses, para su realización presentaron ofertas 5 empresas: EFE Construcciones de Carlos A. Fierro, que cotizó la suma de $ 5.300.000; Empresa Constructora Pilatti SA, presupuestó por $ 5.359.508,41; Romano César Atilio, por $ 6.760.537,45; Alanco SA, por $ 6.685.250; y por último Ingeniero Carlos Alberto Ismael Sonzogni, por $ 6.975.197.