---
category: Agenda Ciudadana
date: 2022-11-17T08:11:07-03:00
thumbnail: https://assets.3dnoticias.com.ar/2022-11-16NID_276609O_1.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: “El proceso está en un todo de acuerdo a la normativa vigente"
title: “El proceso está en un todo de acuerdo a la normativa vigente"
entradilla: 'Lo asevero la ministra Celia Arena respondiendo a la convocatoria aprobada
  por Resolución en la Cámara de Diputados, para dar respuestas sobre el proceso de
  cobertura de vacantes en los juzgados comunales. '

---
En la tarde de ayer miércoles 16, la Ministra de Gobierno, Justicia y Derechos Humanos, Celia Arena, respondiendo a la convocatoria aprobada por Resolución en la Cámara de Diputados, acudió al ámbito de Labor Parlamentaria, para dar respuesta a todas las inquietudes presentadas por las y los legisladores, acerca de cómo se sustancian los concursos en el seno del Consejo de la Magistratura.

En el encuentro, la ministra  estuvo acompañada por los secretarios de Justicia, Gabriel Somaglia, de Gobierno, Oscar Urruty, Electoral, Pablo Ayala, de Derechos Humanos, Lucila Puyol, de Protección Civil, Roberto Rioja, y de Coordinación Administrativa Legal y Técnica, Melisa Trabucchi, además de su equipo técnico.

"La profundización del camino del diálogo, en este caso con el Poder Legislativo, y la construcción de consensos en un proceso orientado a que cada localidad tenga un juez o jueza comunitaria que le resuelva problemáticas cotidianas a los y las ciudadanas”, sostuvo la funcionaria.

En cuanto a las pautas de evaluación del procedimiento,  consideró importante “que se tenga en cuenta el decreto 268 de 2019 modificatorio del 573 de 2014, donde se elimina la puntuación y se instaura una evaluación integral y calificación global, normativas que vienen de gestiones anteriores”.

Por último, la titular de la cartera de Gobierno, Justicia y Derechos Humanos, aseguró que “el proceso que se viene llevando a cabo está en un todo de acuerdo a la normativa vigente, el gobernador se ajustó al marco constitucional y a la legalidad, y ahora es la Legislatura la que en definitiva tiene la última palabra en un procedimiento que expresa las voluntades de dos poderes: el Ejecutivo que envía las propuestas y el Legislativo que define si las acepta o no”.

En el día de ayer, dábamos cuenta de lo vertido en redes sociales por el diputado Fabian Palo Oliver: "Reunión con la Ministra Celia Arena y el Secretario de Justicia Gabriel Somaglia. Ratificamos que el concurso está viciado. Confirmamos que el único criterio tenido en cuenta es el acomodo y el abuso en el uso del poder. Se consolida la hipocresía y los privilegios ". [https://3dnoticias.ar/noticias/fabian-palo-oliver-se-consolida-la-hipocresia-y-los-privilegios/](https://3dnoticias.ar/noticias/fabian-palo-oliver-se-consolida-la-hipocresia-y-los-privilegios/ "https://3dnoticias.ar/noticias/fabian-palo-oliver-se-consolida-la-hipocresia-y-los-privilegios/")