---
category: Estado Real
date: 2021-06-12T06:00:24-03:00
thumbnail: https://assets.3dnoticias.com.ar/MEDIDASCONVIVENCIA.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: El Gobernador anunció las nuevas medidas de convivencia que regirán en toda
  la provincia a partir de este sábado
title: El Gobernador anunció las nuevas medidas de convivencia que regirán en toda
  la provincia a partir de este sábado
entradilla: Omar Perotti anunció también que la provincia ha iniciado la búsqueda
  de nuevas vacunas en distintos países. “El mejor lugar para una vacuna, es en el
  brazo de un santafesino y de una santafesina”, afirmó.

---
El gobernador Omar Perotti dio a conocer este viernes, las nuevas medidas de convivencia que regirán a partir de este sábado en la provincia de Santa Fe, oportunidad en la cual afirmó que “el esfuerzo hecho hasta aquí nos muestra una leve mejoría, pero de no continuar con este tipo de medidas podríamos poner en riesgo todo lo logrado”.

“En la campaña histórica Santa Fe Vacuna tenemos, al día de hoy, más de 1.200.000 habitantes vacunados. Es el 82% de la población objetivo. Eso incluye al personal de salud, a mayores de 60 años, docentes y personal de seguridad. Hasta aquí hemos recibido 1.330.300 vacunas, de las cuales ya hemos aplicado 1.244.0471 dosis. Eso es el 93,45% de vacunas recibidas. El mejor lugar para una vacuna es en el brazo de un santafesino y de una santafesina”, explicó el gobernador.

En ese marco, además, Perotti anunció que “la provincia ha iniciado la búsqueda de vacunas en distintos países. Hemos celebrado hoy la posibilidad de que ANMAT haya autorizado a la vacuna CanSino, una en las cuales la provincia ha firmado un acuerdo, junto a otros laboratorios en los que venimos haciendo”.

“Quiero agradecerles el esfuerzo que se viene haciendo hasta aquí”, remarcó el mandatario. “Este esfuerzo da resultados, hay mejorías, lentas, nos gustaría ir también más rápido, pero tenemos que ser claros, hay medidas que se han tomado parcialmente, con lo cual los resultados también tienen estas características. Es la necesidad de convivir con el cuidado de la salud, con el cuidado de la vida de las personas y mantener adelante la actividad económica”, recordó el gobernador.

Este viernes, Perotti se reunió con el Comité de Expertos provincial. Al respecto, dijo que “el consejo del día de hoy fue muy claro: con este número de camas ocupadas, tenemos que seguir cuidándonos. Y seguir cuidándonos es el resguardo del tapabocas, el lavado de las manos y mantener la distancia. La provincia ha multiplicado la presencia en los testeos y esto es fundamental para generar ese conocimiento lo antes posible y, a partir de allí, generar los aislamientos”, concluyó el gobernador de la provincia.

**La provincia sigue en alto riesgo**

Por su parte, la ministra de Salud, Sonia Martorano, afirmó: “Tenemos toda la provincia pintada de rojo. Esto habla de que estamos en amplio riesgo; y vemos el departamento La Capital (que es Santa Fe y el Gran Santa Fe); y el departamento Rosario y Gran Rosario en alarma. De cualquier manera, toda la provincia está en un alto riesgo”.

Por otra parte, Martorano indicó que “la ocupación de camas es fundamental porque tiene que ver con el nivel de atención en el tercer nivel. Estamos viendo la comparación de cuando comenzamos el aislamiento estricto de 9 días, teníamos niveles muy altos, de 96% y subió hasta el 98% y nos mantenemos hasta el día de hoy en los niveles iniciales”.

En ese sentido señaló que “el próximo viernes volveremos a reevaluar la situación provincial. Este fin de semana sigue igual que los fines de semana anteriores, con circulación restringida las 24 horas”.