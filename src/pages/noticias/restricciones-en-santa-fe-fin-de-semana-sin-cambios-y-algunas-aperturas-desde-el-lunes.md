---
category: La Ciudad
date: 2021-06-12T18:15:06-03:00
thumbnail: https://assets.3dnoticias.com.ar/MEDIDASCONVIVENCIA.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'Restricciones en Santa Fe: fin de semana sin cambios y algunas aperturas
  desde el lunes'
title: 'Restricciones en Santa Fe: fin de semana sin cambios y algunas aperturas desde
  el lunes'
entradilla: 'Del 14 al 25 de junio se amplía el horario de comercio hasta las 19,
  se habilitan los gimnasios y las actividades religiosas al aire libre, entre otras
  medidas. '

---
El Gobierno de Santa Fe anunció este viernes por la noche cómo serán las restricciones en las próximas dos semanas.

Las medidas fueron comunicadas por el gobernador Omar Perotti y la ministra de Salud, Sonia Martorano, en una conferencia de prensa.

En cuanto a la situación epidemiológica actual, Martorano afirmó: “Tenemos toda la provincia pintada de rojo. Esto habla de que estamos en amplio riesgo; y vemos el departamento La Capital (que es Santa Fe y el Gran Santa Fe); y el departamento Rosario y Gran Rosario en alarma. De cualquier manera, toda la provincia está en un alto riesgo”.

A su vez, indicó que “la ocupación de camas es fundamental porque tiene que ver con el nivel de atención en el tercer nivel. Estamos viendo la comparación de cuando comenzamos el aislamiento estricto de 9 días, teníamos niveles muy altos, de 96% y subió hasta el 98% y nos mantenemos hasta el día de hoy en los niveles iniciales”.

En ese sentido, Martorano señaló que “el próximo viernes volveremos a reevaluar la situación provincial".

Principales medidas

**ESTE FIN DE SEMANA:**

* Restricción a la circulación vehicular en toda la provincia: sólo podrán circular esenciales y quienes estén debidamente justificados por una situación de fuerza mayor.
* Actividades autorizadas de 6 a 18 hs: supermercados mayoristas y minoristas, comercios de proximidad de venta de alimentos e higiene personal y limpieza, farmacias, ferreterías, veterinarias y provisión de garrafas.
* Los locales gastronómicos deberán desarrollar la actividad sin la concurrencia de comensales y sólo en las modalidades de reparto a domicilio y de retiro, siempre que se realicen en locales de cercanía.
* Continúan prohibidos los encuentros sociales y familiares en lugares cerrados o al aire libre de quienes no convivan en el mismo domicilio.

**DEL LUNES 14 AL VIERNES 25 DE JUNIO:**

* Se amplía el horario de la actividad del comercio mayorista y comercio minorista, con atención al público en los locales, todos los días de la semana hasta las 19 horas.


* Queda habilitada la actividad de los locales comerciales ubicados en centros y paseos, en modalidad galería


* Los locales gastronómicos podrán funcionar entre las 06hs y las 19hs.


* Queda autorizada la circulación vehicular en la vía pública, desde las 06hs hasta las 20hs. El resto del tiempo sólo podrán circular esenciales y quienes estén debidamente justificados por una situación de fuerza mayor.


* Se habilitan las actividades religiosas al aire libre sin superar las 10 personas


* Permiten actividades recreativas sólo bajo modalidad entrenamiento, al aire libre y sin superar las 10 personas.


* Sólo bajo modalidad entrenamiento, al aire libre y sin superar las 10 personas, se habilita la actividad de clubes, gimnasios, natatorios y otros establecimientos afines.


* Continúan prohibidos los encuentros sociales y familiares en lugares cerrados.