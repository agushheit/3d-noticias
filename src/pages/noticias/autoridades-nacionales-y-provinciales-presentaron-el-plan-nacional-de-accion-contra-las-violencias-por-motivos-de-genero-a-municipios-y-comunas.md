---
category: Estado Real
date: 2021-02-27T09:30:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/violencias.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Autoridades nacionales y provinciales presentaron el plan nacional de acción
  contra las violencias por motivos de género a Municipios y Comunas
title: Autoridades nacionales y provinciales presentaron el plan nacional de acción
  contra las violencias por motivos de género a Municipios y Comunas
entradilla: Participaron equipos locales de género y diversidad de los departamentos
  Belgrano, Caseros, Constitución, General López, Iriondo, San Lorenzo y Rosario.

---
La vicegobernadora Alejandra Rodenas; y la secretaria de Estado de Igualdad y Género, Celia Arena, encabezaron un encuentro federal en el marco del Plan Nacional de Acción contra las violencias por motivos de género. De la actividad participaron autoridades de los Ministerios de las Mujeres, Géneros y Diversidad y de Seguridad de Nación.

El encuentro tuvo modalidad híbrida, presencial y online y se desarrolló en la sala Rodolfo Walsh, donde participaron equipos locales de género y diversidad de los departamentos Belgrano, Caseros, Constitución, General López, Iriondo, San Lorenzo y Rosario.

“Ha sido un placer acompañar a nuestra Secretaria de Estado de Género e Igualdad integrar esta mesa de trabajo, en función de lo que ha sido una política pública diseñada por el presidente de la Nación y del Ministerio de las Mujeres. La provincia de Santa Fe es la primera anfitriona de la presentación de esta importante política de estado. El tema de la violencia es multiagencial. Es un tema que hay que trabajar interdisciplinariamente y sobre el cual hay que reformular algunos dispositivos”, sostuvo Rodenas.

Asimismo, Arena señaló: “Tuvimos una reunión muy importante, en la cual se presentó el plan de abordaje integral de las violencias por motivos de género que la Nación está instrumentando en cada una de las provincias. Además, estuvimos con municipios y comunas de la zona sur en una modalidad híbrida -para mantener el protocolo- donde un grupo de integrantes de las áreas de género y diversidad de los municipios estuvieron presentes aquí y otros lo hicieron conectados de forma virtual”.

“Esto forma parte del trabajo conjunto que venimos desarrollando durante todo el año pasado, pero se enmarca también en la propuesta tan importante del presidente de crear el Consejo para el abordaje de las violencias extremas, al cual nuestro gobernador Omar Perotti no solamente adhirió. Esto nos motiva a avanzar para tener un trabajo colectivo entre los ministerios de la Nación, que intervienen todas las áreas de la provincia y los gobiernos locales para avanzar en este sentido”, concluyó la secretaria de Estado de Igualdad y Género.

Por parte del Ministerio de las Mujeres, Géneros y Diversidad de la Nación estuvieron presentes las titulares de las subsecretarías de Abordaje Integral de las Violencias por Razones de Género, Laurana Malacalza; y de Programas Especiales contra la Violencia por Razones de Género, Carolina Varky. También las responsables de las Direcciones de Abordaje Integral de casos de Femicidios, Travesticidios, Transfemicidios y Delitos contra la Integridad Sexual, Ana Clara Piechestein; de Programas Especiales, Mariana Seghezzo; y de la Dirección Técnica de Registros y Bases de Datos Directora, Lorena Soledad Balardini.

Por la Secretaría de Estado de Igualdad y Género estuvieron presentes las subsecretarias de Mujeres, Género y Diversidad, Florencia Marinaro; y la de Relaciones Institucionales, Lorena Battilana; sumado a las coordinadoras de la región sur Ornela Grossi y María José Gerez. Además, participaron las concejalas Fernanda Gigliani, Alejandra Gómez Sáenz, Norma López y Marina Magnani.

Santa Fe es la primera provincia en la que el Ministerio de las Mujeres presenta su Plan Nacional de Acción contra las violencias por motivos de género, está previsto realizar encuentros interinstitucionales en las 24 provincias para fortalecer las instancias de articulación con los distintos niveles de gobierno y poderes del Estado.