---
category: Agenda Ciudadana
date: 2021-01-29T10:17:56Z
thumbnail: https://assets.3dnoticias.com.ar/inflacion.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Infobae
resumen: Por la suba de alimentos, las consultoras estiman que la inflación de enero
  terminará en torno al 4 por ciento
title: Por la suba de alimentos, las consultoras estiman que la inflación de enero
  terminará en torno al 4 por ciento
entradilla: Es un número similar al índice de diciembre. El tipo de cambio tuvo una
  variación de 4% a lo largo del mes y le pone un “piso” a los aumentos de precios

---
Las consultoras estiman que **la inflación en enero marcará una cifra de entre 3,5% y 4%**, en línea con lo que fue el índice de precios en diciembre. El ajuste del tipo de cambio mayorista que administró el Banco Central y un aumento de los alimentos explican la persistencia en el ritmo inflacionario en el comienzo de 2021.

El 2020 terminó con un _sprint_ de precios que llevó al IPC a **su nivel más elevado en diciembre, cuando fue de 4%**. En los últimos 12 meses registrados por Indec acumuló un total de 36,1 por ciento. **El Gobierno explicó la aceleración de precios “por factores estacionales”**.

Entre ellos, mencionaron cuatro factores: ️pasajes de larga distancia y aéreos, que se ajustaron luego de la reapertura de los servicios; carnes, un rubro en el que hubo “un aumento de precios internacionales que aumenta el costo de producción, lo que representa una dinámica coyuntural, no una cuestión macro”; prepagas y ️hoteles y restaurantes.

![](https://assets.3dnoticias.com.ar/indice.jpg)

En vista a enero, desde el Gobierno afirmaron que esos factores que influyeron en una mayor inflación en diciembre no estuvieron presentes en el primer mes de 2021, por lo que el índice debería ser menor.

Según explicó **Guido Lorenzo**, director de la consultora LCG, a **Infobae**, los relevamiento de precios de las últimas dos semanas muestran que “en la tercera semana de enero la suba de precios de los alimentos promedió 1,9%, acelerándose 0,8 puntos porcentuales (pp) respecto a la semana anterior”.

> **La inflación en enero marcará una cifra de entre 3,5% y 4%, según estiman las consultoras**

“El índice de alimentos y bebidas presentó una inflación mensual promedio de 4,4% y 4,6% medida en el acumulado de las últimas 4 semanas. Estos niveles de precios dejan un arrastre para el resto de enero de 4,9%”, comentó el analista.

Para esta última semana del mes, afirmó Lorenzo, “vemos que la suba de precios anda cerca del 1,2%, siempre alimentos y bebidas. En el mes ese rubro posiblemente supere el 4% y la inflación general ronde entre 3,5 y 4%”, con

![El Gobierno busca con distintos acuerdos de precio evitar subas mayores en los alimentos, el sector con aumentos más críticos en las últimas semanas. (Foto: EFE/Juan Ignacio Roncoroni)
](https://www.infobae.com/new-resizer/gHhypSS6TOaYyPYzJgvT6MqRB3s=/420x280/filters:format(jpg):quality(85)//cloudfront-us-east-1.images.arcpublishing.com/infobae/FCIJEWDOCRAFXIQ7LQAMY6YBCE.jpg)

Por su parte, desde EcoGo tienen una proyección similar. “La inflación viene en un ritmo por debajo de diciembre pero cerca de 4%. Se da en un contexto en de **un tipo de cambio que se mantuvo en 4% de variación, lo que le pone un ‘piso’ a la inflación.** Enero empezó empezó en 3% pero terminar más cerca de 4 por ciento por esos movimientos de tipo de cambio y el reacomodo de precios post pandemia”, dijo **Martín Vauthier**, director de esa consultora a **Infobae**.

“Después de una desaceleración de la inflación en el segundo y tercer trimestres de 2020, en los últimos meses algunos precios relativos empezaron a despertar. **Para enero vemos una lógica parecida a la de diciembre, algunas décimas por debajo**”, concluyó Vauthier.

> **El Gobierno busca con distintos acuerdos de precio evitar subas mayores en los alimentos, el sector con aumentos más críticos en las últimas semanas.**

Ecolatina, por su parte, estimó que “**el IPC GBA Ecolatina trepó 3,9% entre la primera quincena de enero y el mismo período de diciembre**”, publicó la consultora. “Esta dinámica se explicó por algunos alimentos -carne, frutas, comidas elaboradas- y por el incremento en combustibles, seguros y servicios vinculados al mantenimiento del hogar. Asimismo, durante la primera mitad del mes el IPC Núcleo se mantuvo por encima de 4%”, afirmaron.

“En consecuencia, **proyectamos que la suba de precios se ubicará en torno del 3,5% en el primer mes del año**. Esto implica una mínima desaceleración respecto a diciembre 2020, pero refleja que la suba de precios persiste en niveles muy elevados (50% anualizado). Si el tipo de cambio sigue ajustando en línea con la inflación pasada, la inercia del proceso inflacionario será aún mayor manteniendo la suba de precios en torno del 3% promedio mensual durante el primer trimestre”, afirmaron desde Ecolatina.

El Gobierno busca con distintos acuerdos de precio evitar subas mayores en los alimentos, el sector con aumentos más críticos en las últimas semanas. Por un lado, **reforzó Precios Cuidados, extenderá Precios Máximos** (aún no definió si los productos incluidos, entre ellos alimentos y limpieza, seguirán congelados o tendrán un reajuste autorizado), **pactó valores para diez cortes de carne hasta fines y marzo y ahora apunta a realizar algo similar con frutas, verduras** e[ insumos básicos de alimentos como trigo, maíz o girasol.](https://www.infobae.com/economia/2021/01/28/para-evitar-mas-aumentos-de-alimentos-el-gobierno-busca-topes-con-los-productores-de-trigo-maiz-y-girasol/)