---
category: Agenda Ciudadana
date: 2021-08-22T06:00:57-03:00
thumbnail: https://assets.3dnoticias.com.ar/JARDINES.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Es oficial el regreso a la presencialidad plena en los jardines de infantes
title: Es oficial el regreso a la presencialidad plena en los jardines de infantes
entradilla: El gobierno publicó la Circular Nº022 con la que dispuso el regreso a
  clases presenciales en el nivel inicial tanto en instituciones públicas como privadas.

---
El gobierno de la provincia hizo oficial el regreso a la presencialidad plena en los jardines de infantes. El documento cuenta con un anexo con recomendaciones. Además, indica llevar adelante acciones de intensificación pedagógica sobre la base de los contenidos priorizados con énfasis en la alfabetización de las niñas y niños egresantes de la sala de 5 años que inician el primer grado en 2022.

También se pide promover actividades que propicien el abordaje de dimensiones socioafectivas con vistas a la inserción en la escuela primaria. Asimismo, sostener una estrategia comunicacional y territorial de amplio alcance para la escolarización efectiva de todas las niñas y todos los niños en la sala de 4 obligatoria.

Las medidas se dan en el marco de números alentadores en esta etapa de la pandemia, y un sostenido avance de la vacunación en toda la provincia.