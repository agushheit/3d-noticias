---
category: Agenda Ciudadana
date: 2021-06-30T08:42:13-03:00
thumbnail: https://assets.3dnoticias.com.ar/trabajadores-la-pesca-deportiva-vuelven-las-rutas-protestar-que-habiliten-su-actividad-laboral.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Diario Uno
resumen: Vuelven a cortar las rutas en Santa Fe los trabajadores de la pesca deportiva
title: Vuelven a cortar las rutas en Santa Fe los trabajadores de la pesca deportiva
entradilla: Será este miércoles, a las 11, en distintas rutas y localidades de la
  provincia luego de no recibir fundamentos sobre la inhabilitación de su actividad.

---
Luego del corte de rutas realizado este lunes en distintos puntos de la provincia, y tras no conseguir respuestas positivas a sus reclamos, los trabajadores relacionados a la pesca deportiva volverán este miércoles a protestar.

A partir de las últimas disposiciones del gobierno provincial, los cabañeros, guías de pesca, comerciantes y prestadores de otros servicios turísticos de la provincia de Santa Fe, salieron a las rutas este lunes a manifestar su disconformidad con la medida que afecta su actividad a lo largo del territorio santafesino con el pedido urgente volver a trabajar.

Tras las negociaciones fallidas, este miércoles, desde las 11, los trabajadores de la pesca deportiva se manifestarán en la ciudad de Santa Fe cortando uno de los ingresos más importantes a la capital provincial, la ruta nacional 168 a la altura de la fuente de la cordialidad.

Además, en otros puntos del territorio provincial, se registrarán cortes de ruta en Reconquista, Villa Ocampo, San Javier, Rosario, Venado Tuerto, Rufino y Villa Cañás.

"Este martes por la mañana fuimos atendidos por un funcionario del Ministerio de la Producción, Rubén Alcaraz, pero lamentablemente no tenía ninguna respuesta ni conocimiento sobre nuestra situación puntual. Si bien le explicamos todos los problemas que atraviesa nuestro sector puntual, Alcaraz quedó en consultar sobre nuestra problemática", manifestó a UNO Santa Fe, Carlos Pecorari, comerciante mayorista y distribuidor de artículos de pesca.

"Por la tarde nos volvió a llamar este funcionario y nos expresó que el gobierno provincial no iba a modificar ningún decreto y que no podíamos volver a trabajar por el momento", continuó agregando Pecorari. "Nos dijeron que teníamos que esperar el anuncio de las nuevas medidas pero que no tenía certezas sobre lo que pasaría con nuestra actividad", sentenció el referente de los manifestantes.

“Nuestra actividad tiene todos los protocolos sanitarios. A nosotros no nos dejan trabajar al aire libre pero resulta que habilitaron los gimnasios, las ferias, los trueques y otras actividades menos la pesca", finalizó Pecorari.