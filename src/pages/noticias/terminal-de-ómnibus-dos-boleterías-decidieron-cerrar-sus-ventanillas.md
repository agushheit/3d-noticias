---
layout: Noticia con imagen
author: .
resumen: Terminal de Ómnibus
category: La Ciudad
title: "Terminal de Ómnibus: dos boleterías decidieron cerrar sus ventanillas"
entradilla: Ante el parate de tantos meses y la incertidumbre sobre cómo
  volvería la actividad, sobre todo en la provincia y la ciudad de Santa Fe. La
  palabra de una protagonista.
date: 2020-11-17T16:41:23.964Z
thumbnail: https://assets.3dnoticias.com.ar/terminal-boleteria.jpg
---
El impacto de la pandemia en las distintas actividades se hace sentir a ocho meses del inicio de la cuarentena. En el caso de la Terminal de Ómnibus de Santa Fe, la actividad estuvo totalmente parada durante unos siete meses y provocó que propietarios de boleterías decidieran dar un paso al costado.

Esta es la situación de Andesmar, que cerró hace unos cuatro meses y hoy en día su boletería luce tapada.

Ahora, se suma a esta realidad Zenit. Jimena Cavaletto, encargada de las boleterías 13, 14 y 15, anunció que dejarán de trabajar y le pondrá fin a una empresa y tradición familiar.

“Estamos cansados de esperar, con mucha angustia. Fue una decisión muy difícil de tomar. Fueron siete meses donde el medio de transporte de larga distancia estuvo parado y nosotros necesitábamos una contención por parte del municipio, porque más allá de la económica necesitamos una contención emocional. De la empresa no nos podemos quejar, nos vamos de la mejor manera, siempre estuvieron” explicó al Móvil de LT10.

En cuanto a la posibilidad de restructuración y volver a funcionar, Jimena asegura que “es muy complicado porque hay que empezar de cero pero con deudas. Hay que tener una espalda ancha para sostener un número importante, empezaría en primera instancia yo sola para saldar deudas como teléfono e internet”.

Además, existe la incertidumbre sobre cómo va a volver el transporte de media y larga distancia, en teoría a partir del 1 de diciembre. “Hay servicios que se ven en las rutas, pero van a ser sin escalas intermedias, entonces hablamos de que podemos correr el riesgo de que no haya parada intermedia si aumentan los casos en esta ruta” agregó sobre la posibilidad de que ni siquiera lleguen colectivos a la ciudad.

Cavaletto afirma que presentaron protocolos y planes paliativos para subsistir, como que se extienda el kilometraje para que a nivel provincial, se pueda viajar a destinos más allá de los 60 kilómetros permitidos actualmente.

Si bien ellos bajan las persianas, las firmas seguirán operando en la ciudad, seguramente en otras boleterías.

![](https://assets.3dnoticias.com.ar/cavaleto.jpg)

El posteo completo en Facebook: [JIMENA CAVALETTO](https://www.facebook.com/MA.JIMENA.CAVALETTO/posts/10158645873354158)