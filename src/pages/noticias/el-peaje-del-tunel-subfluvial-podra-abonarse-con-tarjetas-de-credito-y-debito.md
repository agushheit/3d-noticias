---
category: Agenda Ciudadana
date: 2021-03-30T07:24:08-03:00
thumbnail: https://assets.3dnoticias.com.ar/tunel.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: El peaje del Túnel Subfluvial podrá abonarse con tarjetas de crédito y débito
title: El peaje del Túnel Subfluvial podrá abonarse con tarjetas de crédito y débito
entradilla: Los usuarios que cuenten con tarjetas de débitos Maestro, Visa, Mastercar
  y Cabal podrán optar por este medio de pago, mientras que también se encuentra habilitado
  el pago con tarjetas de crédito.

---
El Túnel Subfluvial "Raul Uranga-Carlos Sylvestre Begnis" implementó desde este lunes la modalidad de cobro electrónico, por lo que además del tradicional pago en efectivo, los usuarios tendrán la opción de abonar el cruce interprovincial con tarjetas de débito y crédito.

De acuerdo a la información consignada por la entidad, mediante la utilización de posnets inalámbricos instalados en cabinas de peaje de ambas cabeceras, los usuarios que cuenten con tarjetas de débitos Maestro, Visa, Mastercar y Cabal podrán optar por este medio de pago, mientras que también se encuentra habilitado el pago con tarjetas de crédito Cabal, Mastercard y Visa.

Estas dos opciones son válidas, además, para la recarga de tarjetas de abono para usuarios frecuentes.

La iniciativa, que fue avalada por el Consejo Superior de ambas provincias, supone un salto de calidad que mejorará y optimizará las condiciones de prestación del servicio, dado que brinda una opción orientada a simplificar y agilizar la operatoria de pago en ambas cabeceras de enlace.

**TARIFAS Y DESCUENTOS**

Asimismo, se recuerda que se encuentran vigentes las resoluciones que otorgan el 50% de descuento para el usuario frecuente de automóvil; y del 20% al trasporte de carga y público de pasajeros de empresas radicadas en las provincias de Santa Fe y Entre Ríos.

El cuadro tarifario del peaje para el cruce del viaducto es el siguiente:.

**>> Categoría 1: $90**

Vehículos de hasta 2 ejes y ruedas simples. Motos-Autos-Pick-up, ambulancias-combis o trafics, etc

**>> Categoría 2: $200**

Vehículos de 3 y 4 ejes y ruedas simples. Autos o pick-up con remolque (lancha, casa rodante, acoplados de menos de 2,10 m de altura), cases rodantes autopropulsadas, pick-up de 3 ejes.

**>> Categoría 3: $360**

Vehículos de 2 ejes. Camiones – Ómnibus – Maquinaria agrícola o Vial y cualquier otro tipo de vehículo con las características antes mencionadas.

**>> Categoría 4: $530**

Vehículos de 3 ejes. Camiones – Ómnibus – Maquinaria agrícola o Vial y cualquier otro tipo de vehículo con las características antes mencionadas.

**>> Categoría 5: $580**

Vehículos de 4 ejes. Camiones – Ómnibus – Maquinaria agrícola o Vial y cualquier otro tipo de vehículo con las características antes mencionadas.

**>> Categoría 6: $700**

Vehículos de 5 ejes. Camiones – Ómnibus – Maquinaria agrícola o Vial y cualquier otro tipo de vehículo con las características antes mencionadas.

**>> Categoría 7: $750**

Vehículos de 6 ejes. Camiones – Ómnibus – Maquinaria agrícola o Vial y cualquier otro tipo de vehículo con las características antes mencionadas.

Tarifa adicional para cruce de asfalto: $3.000.

Tarifa adicional por corte de tránsito para el cruce de vehículos anchos: $3.000.