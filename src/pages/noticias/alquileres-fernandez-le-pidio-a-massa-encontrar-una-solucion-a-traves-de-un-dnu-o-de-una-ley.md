---
category: Agenda Ciudadana
date: 2021-12-01T06:15:38-03:00
thumbnail: https://assets.3dnoticias.com.ar/alquiler.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: 'Alquileres: Fernández le pidió a Massa "encontrar una solución a través
  de un DNU o de una Ley"'
title: 'Alquileres: Fernández le pidió a Massa "encontrar una solución a través de
  un DNU o de una Ley"'
entradilla: '"Lo más probable" es que se resuelta a través de una Comisión Especial
  del Congreso, a partir de la asunción de los nuevos diputado.

'

---
El presidente Alberto Fernández le pidió hoy al titular de la Cámara de Diputados, Sergio Massa, que trabajen el Congreso de la Nación y el Poder Ejecutivo para "encontrar una solución" a la ley de alquileres.

Así lo indicaron a NA altísimas fuentes parlamentarias, que precisaron que el pedido se gestó durante un almuerzo que compartieron este mediodía en la Casa Rosada.

"El Presidente le pidió a Massa que trabajen el Congreso y el Ejecutivo para encontrar una solución que puede ser a través de un DNU o de una Ley o de una Comisión Especial del Congreso", resaltaron.

En ese punto, las mismas fuentes consideraron que "lo más probable" es que se resuelta a través de una Comisión Especial del Congreso, a partir de la asunción de los nuevos diputado.

De esta manera, se buscaría reformar la ley de alquileres "con un primer artículo que suspenda la ley por 180 días y ponga en vigencia la anterior".

En tanto, Alberto Fernández y Sergio Massa aprovecharon la oportunidad para dialogar sobre la agenda parlamentaria de fin de año.

"Estuvieron hablando de los proyectos que se van a tratar de acá a fin de año en el Congreso de la Nación y que le interesan tanto al Ejecutivo como al Legislativo", resaltó la portavoz presidencial Gabriela Cerruti en declaraciones a los medios acreditados en Casa Rosada, entre ellos NA.

Durante la reunión, que se extendió por casi dos horas y el tigrense almorzó "milanesas con puré", Fernández y Massa charlaron sobre el avance de un paquete de leyes que son claves para el Gobierno nacional.

Altas fuentes parlamentarias precisaron a Noticias Argentinas que esa agenda parlamentaria tiene como "leyes prioritarias" a sacar antes de fin de año el Presupuesto, la ley Automotriz, la de Electromovilidad, la de Agroindustria y la prórroga de la Ley de Régimen de Incentivo a la Construcción Federal Argentina.

Además, confiaron que "entre las prioridades" también aparece el plan plurianual que el Ejecutivo nacional enviará en los próximos días, con el objetivo de de delinear un plan económico junto al arco político opositor.

En ese maro, la portavoz indicó que "se conversa mucho y se busca puntos de acuerdo" con la oposición, y explicó que muchas de las ideas que contendrá el plan plurianual fueron tratadas en el Consejo Económico y Social (CES) y se vienen conversando "con diferentes sectores, como empresarios y sindicatos".

"Hay que esperar que la oposición se termine de acomodar en el Congreso después del cambio electoral y saber bien quiénes serán los interlocutores", enfatizó Cerruti.

Además, la funcionaria nacional puntualizó que será "uno de los proyectos que apenas el ministro (de Economía Martín) Guzmán lo envíe el Parlamento será discutido", además de ser "debatido también en el marco del CES".

En esa línea, anticipó que el proyecto de agroindustria "ya está en Diputados y el jueves el Presidente, Massa y (el ministro de Agroindustria Julián) Domínguez lo presentarán ante el sector para contar un poco más".

Por último, aseguró que "hay una cantidad de funcionarios en el Gobierno que llevan adelante las charlas con la oposición por las reales funciones que tienen", y mencionó al Presidente; el jefe de Gabinete, Juan Manzur; el ministro del Interior, Eduardo de Pedro, entre otros.