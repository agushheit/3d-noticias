---
category: Deportes
date: 2021-01-24T10:18:59Z
thumbnail: https://assets.3dnoticias.com.ar/dyj.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: Defensa y Justicia le ganó a Lanús y se coronó campeón de la Copa Sudamericana
title: Defensa y Justicia le ganó a Lanús y se coronó campeón de la Copa Sudamericana
entradilla: 'El equipo de Crespo se impuso 3 a 0 en el Estadio Mario Alberto Kempes
  con goles de Adonis Frías, Braian Romero y Washington Camacho. Hizo historia: levantó
  el primer trofeo continental de su historia.'

---
Defensa y Justicia hizo historia en el Estadio Mario Alberto Kempes: se impuso 3 a 0 ante Lanús y se coronó campeón de la Copa Sudamericana. El equipo de Hernán Crespo le ganó a Lanús en Córdoba y consiguió el primer título de su historia.

El primer gol del partido lo anotó Adonis Frías cuando se jugaban 34 minutos del primer tiempo. Aumentó Braian Romero a los 17 del complemento. Washington Camacho, cuando se jugaban 47, liquidó el resultado.

Defensa dominó con creces en el primer tiempo. Fue dueño absoluto de la pelota y no sufrió ningún ataque en su arco. Las altas temperaturas incidieron en el desarrollo del juego en el Estadio Mario Alberto Kempes. Promediando la primera etapa, los futbolistas de ambos equipos pararon para refrescarse.

Luis Zubeldía, entrenador de Lanús, metió mano en el equipo para intentar modificar las acciones en el segundo tiempo. Cuando se jugaban 17 minutos, el defensor Granate Alexis Pérez quiso darle un pase al arquero Lautaro Morales. No le salió, la dejó corta. Apareció Romero y definió por sobre el cuerpo del 1.

Defensa tendrá la posibilidad de sumar un nuevo título continental. Con este triunfo accedió a la final de la Recopa Sudamericana. En marzo enfrentará al ganador de la Copa Libertadores, que el próximo sábado definen Santos y Palmeiras de Brasil.

Además, el triunfo del equipo de Florencio Varela clasificó a San Lorenzo a la próxima edición de la Copa Libertadores. Los dirigidos por Diego Dabove necesitan este resultado además de que Boca se coronara campeón de la Copa Diego Maradona. El primer requisito se les había cumplido el pasado domingo en San Juan.