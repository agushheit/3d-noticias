---
category: La Ciudad
date: 2021-09-15T06:00:40-03:00
thumbnail: https://assets.3dnoticias.com.ar/ARISTOBULO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: Jatón y comerciantes dialogaron sobre mejoras para la avenida Aristóbulo
  del Valle
title: Jatón y comerciantes dialogaron sobre mejoras para la avenida Aristóbulo del
  Valle
entradilla: El intendente se reunió este martes con integrantes de la Asociación de
  Comerciantes de la Avenida Aristóbulo del Valle y evaluaron algunas intervenciones.

---
En la sede de la Asociación de Comerciantes, Industriales, Profesionales de la Avenida Aristóbulo del Valle (Acav), los comerciantes recibieron al intendente Emilio Jatón y parte de su equipo para analizar una serie de intervenciones que se harán en lo inmediato como la colocación de cestos, la refuncionalización de columnas de iluminación obsoletas, entre otras; pero también, comenzar delinear un proyecto de modernización y puesta en valor del corredor.

En cuanto a la colocación de cestos, en una primera etapa, se instalarán 26 dispositivos metálicos de color negro que fueron diseñados por la Municipalidad. En esta línea, desde la Secretaría de Obras y Espacio Público, se pondrá en marcha un relevamiento para conocer la cantidad de columnas de alumbrado que hay y cuáles son las mejoras que al respecto se deberán hacer.

Luego del encuentro, el secretario de Desarrollo Urbano, Javier Mediondo, dio más detalles sobre la reunión y en primer lugar consideró: “Aristóbulo del Valle es una de las avenidas más importantes de la ciudad. En toda su historia siempre ha generado el desarrollo del norte, pero ahora, en el contexto pospandemia, tiene un rol estructural estratégico porque resume lo que es ese modelo de ciudad del futuro que es la ciudad próxima, de los 15 minutos, donde se revalorizan los barrios”.

Y desde ese punto de vista, Mendiondo destacó: “Desde el inicio de esta gestión municipal, venimos desarrollando un diálogo con los comerciantes, los actores de la sociedad civil que son los que conocen el día a día, las necesidades y los desafíos de la actividad comercial y también del desarrollo urbano”.

**Diálogo y trabajo**

A partir de esta idea, es que el estado municipal e integrantes de la asociación analizaron ese modelo de ciudad que todos quieren. “Se abrieron dos agendas de trabajo, una a mediano y otra a largo plazo que va a terminar en un proyecto de una nueva avenida, más moderna, en donde se van a desarrollar tanto la actividad comercial como un modelo de movilidad sustentable, con una apropiación por parte de los vecinos de la zona y de toda la ciudad, entre otros puntos”, dijo Mendiondo.

Pero por otro lado, el funcionario municipal contó: “Y a corto plazo pensamos en intervenciones vinculado al ordenamiento del equipamiento urbano que quedó en desuso y que es necesario cambiar o la gestión de los residuos, con la colocación de nuevos cestos se pueden mejorar el funcionamiento, dado que producto de la pandemia toda la zona de estas avenidas incrementaron su actividad comercial y por lo tanto, mayor cantidad de gente genera más presión y una necesidad de mejorar la infraestructura”.

**Mejoras importantes**

Por su parte, Graciela García, presidenta de la Acav, destacó que la vista del intendente retoma una reunión que hubo antes de la pandemia. “En ese momento le planteamos los inconvenientes que teníamos en la avenida Aristóbulo del Valle, quedamos en elaborar un cronograma de trabajo en conjunto y ahora que estamos un poco mejor con el tema del Covid-19, vinieron a proponernos un plan que nos pareció muy útil”, dijo la comerciante.

En ese sentido, García detalló que algunos puntos del plan incluyen, además de la colocación de cestos de basura, la construcción de rampas para personas con discapacidad, la limpieza del corredor comercial, entre otros. En consonancia, añadió que del encuentro también participaron los secretarios del municipio a los que pudieron plantearles las diferentes necesidades, y “nosotros nos pusimos a disposición del equipo de trabajo”, dijo.

Por su parte, Georgina Giay, vicepresidenta de la Asociación detalló que durante el encuentro se acordó comenzar las acciones de manera inmediata con la colocación de los cestos porque “en la avenida no hay y es un reclamo que tenemos desde hace mucho tiempo, ya que son necesarios para mantener la limpieza”, dijo y al mismo tiempo manifestó: “Aprovechamos para pedirle al ciudadano que tenga en cuenta mantener la limpieza y nos acompañe en eso, porque se necesita de todos”.

Asimismo, dijo que las acciones para mejorar el centro comercial a cielo abierto “continuarán con el retirando el alumbrado obsoleto, hay farolas desde hace muchos años que no funcionan ni se pueden reparar, así que hay que retirarlas y ahí veremos qué pasos seguimos dando. Hay un proyecto urbanístico para hacer intervenciones en la avenida”.

Antes de finalizar, la vicepresidenta subrayó que “fue una reunión muy productiva, la idea es trabajar juntos, consensuando el municipio, con nosotros y los comerciantes”.

Estaban presentes, además de Jatón y Mendiondo, los secretarios de Obras y Espacio Público, Matías Pons Estel; se Ambiente, Edgardo Seguro; de Producción y Desarrollo Económico, Matías Schmüth.