---
category: Agenda Ciudadana
date: 2021-09-26T06:15:26-03:00
thumbnail: https://assets.3dnoticias.com.ar/MIPIEZA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Más de 19.000 preinscripciones en las primeras 48 horas del programa "Mi
  Pieza"
title: Más de 19.000 preinscripciones en las primeras 48 horas del programa "Mi Pieza"
entradilla: La iniciativa tiene por objetivo dar asistencia económica para el mejoramiento
  y ampliación de viviendas de mujeres que residen barrios populares.

---
Más de 19.000 preinscripciones se registraron en las primeras 48 horas del lanzamiento del programa "Mi Pieza" que tiene por objetivo dar asistencia económica para el mejoramiento y ampliación de viviendas de mujeres que residen barrios populares, informó este sábado el Ministerio de Desarrollo Social.

En total se recibieron 19.300 preinscripciones, que se materializaron a través de la página www.argentina.gob.ar/mipieza

La iniciativa está orientada a mujeres, mayores de 18 años, argentinas o con residencia permanente y residentes en barrios populares que figuren en el Registro Nacional de Barrios Populares (ReNaBap).

Se podrán realizar obras de mejoramiento de techo, pared, pisos o aberturas, división de interiores, refacciones menores de plomería y/o electricidad y ampliación de viviendas.

El ministro Juan Zabaleta afirmó que este programa representa “una rueda virtuosa entre el derecho a la vivienda y el hábitat y el derecho al trabajo, porque enciende el consumo en los corralones de materiales y hay más albañilas y albañiles trabajando en los barrios”.

El programa se financia por medio del Fondo de Integración Socio Urbana (FISU), que se compone con el 15% del Aporte Solidario Extraordinario a las grandes fortunas y el 9% del impuesto PAIS.

La secretaria de Integración Socio Urbana, Fernanda Miño, sostuvo que el programa forma parte del plan de urbanización de barrios populares.

“Este programa viene a complementar los proyectos de integración socio urbana. Es para la casa, para las familias, para dividir una habitación, para cambiar un techo, para poder solucionar temas de humedad, poner pisos, cambiar la ventana, poner la puerta, las conexiones eléctricas”, especificó.

La funcionaria nacional expresó que el programa consiste en una asistencia económica para “mejorar viviendas, para proyectos que han quedado truncados muchas veces por la situación económica, y más en este tiempo de pandemia, y con la imposibilidad de muchas de las mujeres de los barrios populares de acceder a créditos. Por eso este programa va dirigido a ellas”.

Para acceder al programa las mujeres deben contar con Certificado de Vivienda Familiar y es compatible con cualquier otra prestación social, pero si no se verifica el avance de obra "quedarán inhabilitadas para nuevas líneas de asistencia económica".

El sistema de inscripción es ingresando a argentina.gob.ar/mipieza pero si hay más de 25 mil inscriptas, la selección se realizará mediante sorteo.

La asistencia, que es entre 100 y 240 mil pesos, es para realizar mejoramiento de techo, pared, piso, aberturas división de interiores, refacciones menores de plomería y/o electricidad y ampliación de vivienda.

El cobro de la asistencia será a través de Anses en dos cuotas del 50%. La segunda luego de validar el avance de la obra a través de una aplicación en el celular.