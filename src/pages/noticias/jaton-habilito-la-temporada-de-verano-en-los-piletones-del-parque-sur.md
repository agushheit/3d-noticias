---
category: La Ciudad
date: 2020-12-09T11:19:31Z
thumbnail: https://assets.3dnoticias.com.ar/JATON.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Jatón habilitó la temporada de verano en los piletones del Parque Sur
title: Jatón habilitó la temporada de verano en los piletones del Parque Sur
entradilla: El intendente recorrió este sábado el espacio recuperado por la Municipalidad.
  De este modo, se completó el lanzamiento de Verano Capital.

---
Este sábado, el intendente Emilio Jatón y parte del Gabinete municipal llegaron hasta los piletones del Parque del Sur para completar el lanzamiento de la temporada estival en la ciudad. De este modo, se habilitó también la plaza húmeda del Parque Garay.

Estas propuestas, enmarcadas en **Verano Capital**, se suman a la apertura de los solariums en los Espigones I y II, playa Grande, Costanera Este, el Paseo Néstor Kirchner y Los Alisos, que se concretó el pasado martes 1 de diciembre.

**En todos los casos, se garantiza el cumplimiento de los protocolos adecuados y el respeto por las medidas sanitarias.**

Durante la recorrida, Jatón conversó con los vecinos que ya se habían instalado en el espacio para disfrutar del sol y la agradable temperatura que ofreció la mañana de este sábado.

A ellos les pidió que disfruten los espacios, que los cuiden y, sobre todo, que sigan respetando los protocolos y las medidas sanitarias. “Necesitamos mucha responsabilidad, porque el comportamiento individual termina siendo colectivo. Nosotros vamos a hacer nuestro trabajo y le pedimos a la gente que nos ayude”, afirmó.

**El intendente recordó que esta será “una temporada distinta, por eso, desde la Municipalidad ponemos a punto todos los espacios públicos para que los puedan disfrutar tanto los vecinos como los turistas que se acerquen a la ciudad”.**

Vale recordar que la Municipalidad concretó importantes inversiones en los parques Garay y del Sur, con el objetivo de mejorar las instalaciones para el disfrute de los vecinos y las vecinas de la capital. Las labores incluyeron el saneamiento completo de las piletas y la superficie perimetral de las mismas.

En ambos espacios fueron recuperados los juegos para niños y se colocó mobiliario urbano como bancos, basureros y bebederos, entre otros. A esto se suma la recuperación del sistema de iluminación, con un cableado nuevo y reemplazo de luminarias. Por otra parte, se pensaron nuevos solados para la radicación de servicios como, por ejemplo, área de comidas y sanitarios.

![](https://assets.3dnoticias.com.ar/JATON1.jpg)

## **Verano Capital**

El secretario de Integración y Economía Social del municipio, Mariano Granato, recordó que la habilitación de los piletones en los parques forma parte de “la recorrida del Verano Capital, que es una propuesta de la Municipalidad para acompañar una temporada distinta, atravesada por la realidad del distanciamiento, pero con equipos de trabajo municipales: profesores de educación física, guardavidas, GSI y Cobem que estarán dedicados toda la temporada a trabajar con los vecinos y las vecinas que disfruten de los espacios públicos”.

En ese sentido, estableció que **hubo “una gran inversión y un gran esfuerzo de la Municipalidad para mejorar el espacio público**, y ahora queda el compromiso de los vecinos y las vecinas de transitar esto respetando las medidas y los protocolos vigentes”.

A esto añadió que “el programa Verano Capital, además de la mejora del espacio público y el trabajo con los protocolos, tiene una programación cultural, artística y deportiva que tendrá propuestas todos los inicios de semana, con una agenda concreta de actividades a desarrollarse en parques y paseos de toda la ciudad, para que los vecinos que viven lejos de la Costanera puedan disfrutar también de los eventos este verano”.

![](https://assets.3dnoticias.com.ar/JATON3.jpg)

## **Las obras**

En el Parque del Sur se realizó una puesta a punto de todos los piletones, duplicando la superficie de los espejos de agua. Esto se logró mediante la reparación de patologías constructivas y puesta a punto de las terminaciones. Además, se remodelaron los chorros que asisten al sistema de piletones para que sean más seguros y tengan una presencia en el paisaje más potente.

**La zona húmeda de niños también se recuperó para permitir la estadía de los más chicos dentro del agua.** También se construyeron dos playas con arena alrededor, que conectan toda la oferta recreativa de infraestructura para completar el equipamiento de agua y las cuestiones accesorias recreativas del parque.

Del mismo modo, se habilitó una nueva zona de arena para actividades deportivas. Otra intervención importante es que, en esta temporada, **ya se cuenta con un equipamiento fijo de sanitarios** que resuelve una necesidad largamente planteada por los vecinos.

**A ello se suma la remodelación edilicia, que permitirá contar con un servicio de bar todo el año, incluso de noche.** Para eso se intervino en la iluminación y la reforestación: se plantaron ejemplares de edad avanzada de timbó, ceibo y otras especies nativas para que en poco tiempo den sombra.

**En el caso del Parque Garay**, también se hizo un trabajo importante para sellar juntas y resolver una cantidad de patologías constructivas que tenían los piletones, y se desarrolló un sistema de solados para conectar todas las ofertas edilicias alrededor del pulmón verde. En ese espacio, aún **resta la instalación del equipamiento sanitario**, ya que se trata de un modelo prefabricado para montar en seco.