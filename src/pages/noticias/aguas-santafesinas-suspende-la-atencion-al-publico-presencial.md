---
category: La Ciudad
date: 2022-01-08T06:15:47-03:00
thumbnail: https://assets.3dnoticias.com.ar/AGUAS.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Aguas Santafesinas suspende la atención al público presencial
title: Aguas Santafesinas suspende la atención al público presencial
entradilla: 'Será desde el lunes 10 de enero en las 15 localidades donde Assa presta
  servicios por la situación sanitaria del Covid

'

---
La empresa Aguas Santafesinas SA (Assa) anunció que desde el 10 de enero se suspenderá la atención al público de manera presencial debido a la situación sanitaria por el Covid. Esas tareas se realizarán de manera virtual y la empresa puso a disposición el WhatsApp 3416950008.

La medida se toma de forma preventiva ante la ola de contagios que se está viviendo en la provincia y en el país. El gerente de relaciones institucionales, Guillermo Lanfranco dijo que "no hay un plazo definido" y que se volverá a abrir las puertas ni bien las condiciones lo permitan.

Además, dijo que Assa tiene unas 150 personas, entre empleados y contratados, que están aislados y que eso representa cerca de un 10 por ciento del total del personal.

Lanfranco, en diálogo con Radio 2 de Rosario, dijo que entre el 75 y el 80 por ciento de los usuarios realiza los trámites a través del WhatsApp, pero que también se puede utilizar la web de la empresa www.aguassantafesinas.com.ar.