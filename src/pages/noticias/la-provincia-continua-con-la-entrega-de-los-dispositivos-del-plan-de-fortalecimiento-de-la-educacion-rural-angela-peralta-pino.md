---
category: Agenda Ciudadana
date: 2021-07-02T08:49:32-03:00
thumbnail: https://assets.3dnoticias.com.ar/celulares.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Gobierno de Santa Fe
resumen: La provincia continúa con la entrega de los dispositivos del plan de fortalecimiento
  de la Educación Rural "Ángela Peralta Pino"
title: La provincia continúa con la entrega de los dispositivos del plan de fortalecimiento
  de la Educación Rural "Ángela Peralta Pino"
entradilla: La iniciativa prevé distribución de 4000 celulares a alumnas y alumnos
  de primer año de escuelas secundarias rurales.

---
En el marco del Plan de Fortalecimiento de la Educación Rural "Ángela Peralta Pino”, el Ministerio de Educación entregó 146 dispositivos celulares y becas de conectividad a alumnos y alumnas de primer año de escuelas secundarias rurales de los departamentos La Capital y Garay.

Durante la entrega, la ministra de Educación, Adriana Cantero, manifestó que “esta es una política que tiene un criterio de universalidad para todos los alumnos de primer año del secundario que están en la ruralidad, quienes recibirán un dispositivo móvil en comodato, el que se realizará con sus familias, que son adultos; y a su vez, además del recurso tecnológico, les vamos a garantizar los paquetes de datos que se necesitan para mantener el contacto pedagógico. Por lo tanto, el teléfono también va acompañado de una beca para el pago del paquete de datos, que en este caso, tendrá una duración de 6 meses para que puedan completar el ciclo lectivo 2021”.

Asimismo, Cantero expresó: “Estas son muy buenas noticias para la ruralidad, son espacios educativos a los que nosotros proponemos darles visibilidad porque muchas veces estos lugares que están alejados de las grandes ciudades aparecen como silenciados o invisibilizados y nosotros queremos ponerlos a todos en el centro de la escena de lo que se enseña”.

A continuación, Cantero se refirió a la tercera entrega de más de 1.000.000 de cuadernos destinados a las y los alumnos y docentes de los niveles inicial, primario, secundario, técnico y a la educación de adultos: “Cuando vuelvan del receso escolar de invierno vamos a estar trabajando para que todas y todos los estudiantes reciban el tercer cuaderno que produce la provincia de Santa Fe, un nuevo material igualador, con el que va a poder contar cada estudiante para la realización de su tarea escolar; y que también es un guión pedagógico para nuestros docentes en la sistematicidad de la enseñanza en tiempos de educación en la distancia”.

Durante el acto, recibieron los celulares del plan fortalecimiento de la educación rural estudiantes del anexo de las escuelas Secundaria Orientada Nº 1242 de Saladero Cabal, Nº 535 de Santa Rosa de Calchines, Nº 587 de Candioti, N° 590 “Juan José Saer” de Arroyo Leyes y Nº  713 “Monte de los Padres” de Sauce Viejo.

Acompañaron a la ministra, los ecretarios de Educación Víctor Debloc; de Gestión Territorial Educativa, Rosario Cristiani; y de Administración, Cristian Kuverling; el senador del departamento Garay, Ricardo Kaufmann; el subsecretario de Comunas, Carlos Kaufmann; la delegada de la Región IV, Mónica Henny; la presidenta comunal de Santa Rosa de Calchines, Natalia Galeano, y los presidentes comunales de Arroyo Leyes, Eduardo Lorinz y de Helvecia, Luciano Bertossi; autoridades locales, supervisores, directivos, docentes y alumnas y alumnos de las escuelas rurales involucradas.

**Plan Ángela Peralta Pino**

El plan “Ángela Peralta Pino” está enmarcado en la línea estratégica “Todas las chicas y los chicos en la escuela aprendiendo”. La iniciativa es una decisión de política educativa de la gestión provincial, que promueve los valores de la igualdad de oportunidades y de la justicia educativa, destinando recursos para fortalecer las trayectorias de los estudiantes secundarios de ámbitos rurales.

En ese sentido, la medida consiste en la entrega, en carácter de préstamo a término, de un teléfono celular y de una beca de conectividad a todas las alumnos y todos los alumnos que están cursando 1º año de la educación secundaria en todas las escuelas de gestión pública y gestión privada de ámbitos rurales de la provincia.

La inversión prevista tanto para los dispositivos, como para las 4000 becas de $6000 cada una, alcanza a más de 80 millones de pesos.

El plan prevé alcanzar a 4000 alumnas y alumnos que asisten a 237 escuelas secundarias rurales de 229 localidades diseminadas a lo largo de todo el territorio provincial.

**Convenios para el Boleto Educativo Rural**

Con la premisa de garantizar el transporte de estudiantes, docentes y asistentes escolares a los establecimientos educativos rurales, el Ministerio de Educación de Santa Fe firmó nuevos convenios de adhesión al Boleto Educativo Rural (BER) con municipios y comunas del departamento La Capital.

El impacto de este beneficio para la educación rural es significativo, ya que de los 4.500 establecimientos educativos santafesinos, más de 1.200 pertenecen a la ruralidad, es decir que alcanza casi a la tercera parte del total de escuelas provinciales.

La firma de los convenios con municipios y comunas involucró a Recreo, Arroyo Aguiar, Campo Andino, Cadioti, Emilia y Monte Vera.