---
category: Estado Real
date: 2021-01-03T10:30:08Z
thumbnail: https://assets.3dnoticias.com.ar/01032021-COP-santa-fe.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: 'Seguridad: proyectan Centros de Operaciones Policiales para los grandes
  centros urbanos'
title: 'Seguridad: proyectan Centros de Operaciones Policiales para los grandes centros
  urbanos '
entradilla: Este proyecto le va a dar a la provincia de Santa Fe recursos tecnológicos
  de alto nivel para ser aplicados a la seguridad pública y al beneficio de la ciudadanía.

---
En el marco del plan de modernización y profesionalización de la Policía provincial, y la reforma integral del sistema de seguridad pública de Santa Fe, el gobierno provincial, a través del Ministerio de Seguridad, impulsó un proyecto para establecer seis Centros de Análisis y Operaciones Policiales, donde se van a desarrollar estrategias y acciones concretas en materia de seguridad, con una distribución de los recursos precisa y focalizada.

En este sentido, el Ministro de Seguridad Marcelo Sain junto al gobierno provincial han dado prioridad a la implementación inicial de los COP en tres grandes centros urbanos: Santa Fe, Rosario y Rafaela.

El Secretario de Seguridad Pública Germán Montenegro aseguró: «Estamos llevando adelante un proceso de inversión y adecuación tecnológica de gran envergadura, esto se encontraba planificado desde el inicio de la gestión, y actualmente se manifiesta en la resolución 1138 firmada por el Ministro, que prevé la conformación de seis centros de operaciones policiales organizados, para contar con una gran dotación de elementos tecnológicos, conducir diferentes operaciones policiales y evaluar el desempeño policial sobre la base de inteligencia».

Esta iniciativa se encuentra enmarcada dentro del proyecto de inversión de tres mil millones de pesos aportados por el gobierno nacional a partir del convenio de Cooperación y Asistencia Financiera por medio de la jefatura de gabinete de ministros y el gobierno de la provincia de Santa Fe, para ejecutar, entre otras cosas, los Centros de Análisis y Operaciones Policiales, acompañados por un sistema con tecnología de punta y software específico, capacitación y formación permanente del personal policial, a fin de conectar el comando operacional con los mapas del delito y su adecuada supervisión

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.2em; font-weight: 700">¿Qué son los Centros de Operaciones Policiales?</span>**

Los COP son unidades analíticas y operacionales, que tendrán como objetivo producir análisis criminal, planificando las operaciones e intervenciones policiales de forma focalizada, y además evaluar el desempeño operativo de los agentes policiales, luego de las intervenciones ejecutadas.

Cada COP estará compuesto por tres espacios: una Oficina de Análisis Criminal, una Oficina de Operaciones Policiales y una Oficina de Evaluación Operacional Policial. Estas áreas van a permitir que la elaboración del mapa del delito sea más ordenado y efectivo, y que cada jefe pueda decidir sobre la distribución de los recursos y los servicios dependiendo de las zonas, de manera más eficaz.

Entre los beneficios, los COP permiten que los servicios policiales se encuentren más concentrados, brindando mayor capacidad de despliegue y flexibilidad táctica a los agentes policiales. Además, garantizan el patrullaje y la investigación permanente, brindando más protección a la ciudadanía.

En materia de modernización, cada COP permite integrar los sistemas tecnológicos y de comunicaciones que funcionan para compilar información criminal a lo largo de toda la extensión provincial (denuncias que ingresan al Sistema de Emergencias 911, aquellas que se efectúan en las Comisarías y los Centros Territoriales de Denuncias); el Sistema de Cámaras de Videovigilancia tanto las que corresponden a la provincia, como las de los municipios y comunas; los recorridos que recuperan los GPS de cada vehículo policial, también la información generada por Drones y cámaras portátiles que poseen tanto el personal como los móviles oficiales. 

Todo esto va a permitir la planificación, la dirección y la evaluación incesante de las operaciones policiales en todas sus posibilidades. Además, los policías van a contar con computadoras portátiles y compactas para transferir datos, imágenes y audio a la central operacional de manera transparente, ágil y con herramientas que permiten la geo referenciación de los recorridos.

«Este proyecto está siendo financiado por una transferencia de recursos del gobierno federal que consta de 3 mil millones de pesos afectados en su totalidad a la modernización tecnológica de la policía, que también es un salto significativo con relación a lo que tenemos tecnológicamente, ya que es bastante anticuado y no ha sido actualizado», agregó Montenegro.

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.2em; font-weight: 700">Estrategia de Seguridad Preventiva</span>**

El proyecto de los COP forma parte de la Estrategia de Seguridad Preventiva para Centros Urbanos, y tiene como cometido desarrollar una efectiva capacidad en la producción de análisis criminal, junto al fortalecimiento de la gestión informática y la formación del personal para poder desarrollar dichas tareas.

Esta planificación se encuentra orientada de forma tal, que las operaciones policiales estén direccionadas fundamentalmente a las tareas de prevención de delitos comunes y complejos, implementando diversos dispositivos de control y herramientas para evaluar el accionar policial.

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.2em; font-weight: 700">Programa Integral de Seguridad</span>**

Entre estas políticas concretas, se suma que, a través del Programa Integral de Seguridad Policial, se prevé la compra de 15.000 chalecos antibalas, 265 motos urbanas y para rutas, la incorporación de un sistema moderno de armas, 84 vehículos no identificables, 26 minibuses de traslado de personal, canes y personal táctico, 6 vehículos de carga, un millón de municiones de varios calibres, y 100 cámaras para patrulleros, además de equipamiento informático moderno, indispensable para investigación criminal, además de camionetas patrulleras adquiridas y que ya se encuentran en funcionamiento.

Todo esto, significa una inversión de más de 477 millones de pesos, que resulta fundamental para dotar a la Policía de recursos que son indispensables en el adecuado desempeño de sus tareas, especialmente, en la prevención del delito.

Para concluir, el secretario de Seguridad indicó: «pusimos en marcha un proceso tendiente a recuperar las capacidades logísticas, el gobierno de la provincia financió este programa de recuperación a través de la adquisición de más de 200 patrulleros, más de 300 motos, la puesta en marcha de procesos licitatorios para adquirir equipamiento nuevo, chalecos antibalas, etc. Todo el equipamiento básico que tiene que utilizar el personal policial para desempeñar sus tareas, que cuando nosotros asumimos, encontramos en un estado de situación limitado y paupérrimo. Todo este proceso se comenzó a observar en la segunda mitad del año cuando se materializaron estas licitaciones».