---
category: Estado Real
date: 2021-08-09T06:00:24-03:00
thumbnail: https://assets.3dnoticias.com.ar/Sanjeronimo.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Plan incluir: la provincia firmó un convenio con la fundación integrar San
  Jerónimo'
title: 'Plan incluir: la provincia firmó un convenio con la fundación integrar San
  Jerónimo'
entradilla: Es para la puesta en funcionamiento de un vivero para la producción de
  huertas orgánicas a través del sistema de hidroponía.

---
El Ministerio de Desarrollo Social firmó, en el Club de Leones de la ciudad de Coronda, un convenio con la Fundación Integrar San Jerónimo, el cual permitirá financiar el desarrollo de la producción de diversas especies de vegetales a través del sistema de hidroponia.

La fundación, a su vez, aportará dichos capitales para la concreción del proyecto en el ámbito del Centro de Trabajo para Discapacitados (CETRADI) de la ciudad de Coronda, a través del Club de Leones.

El ministro de la cartera, Danilo Capitani remarcó: “Hoy estamos aquí, apoyando a las instituciones intermedias tal como lo pidiera el gobernador Omar Perotti desde el inicio de su gestión, ampliando de esta manera la participación del Estado con los diferentes actores de nuestro territorio provincial, fortaleciendo proyectos regionales que tengan que ver con lo social y lo productivo”.

El objetivo del proyecto es puramente social, está destinado a niños, niñas y adolescentes con capacidades diferentes, favoreciendo y orientando la búsqueda a una salida laboral.

Cabe destacar que, además de la producción de hortalizas por intermedio del sistema de hidroponía, el proyecto pretende también la inclusión de los jóvenes con discapacidad dentro de las actividades de la comunidad, como así también, desarrollar junto a los beneficiarios el emprendedorismo y disminuir el riesgo social de los participantes.

**Sistema hidroponía**

Es un método utilizado para cultivar plantas usando disoluciones minerales en lugar de tierra. Las raíces reciben una solución nutritiva y equilibrada disuelta en agua con los elementos químicos esenciales para el desarrollo de las plantas, que pueden crecer en una solución acuosa únicamente, o bien en un medio inerte, como arena lavada, grava o perlita, entre muchas otras.

**Presentes**

Acompañaron a las autoridades del Ministerio de Desarrollo Social, Juanita Guerique, en representación de la Fundación Integrar San Jerónimo; y Dante Escalante, presidente del Club de Leones de Coronda.