---
category: Estado Real
date: 2022-01-09T06:00:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/perottivacunated.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Perotti: "El altísimo nivel de vacunación nos está permitiendo mantener
  el nivel de actividad productiva y cuidar el trabajo"'
title: 'Perotti: "El altísimo nivel de vacunación nos está permitiendo mantener el
  nivel de actividad productiva y cuidar el trabajo"'
entradilla: Sobre la no aprobación del Presupuesto dijo que, “tanto a nivel nacional
  como provincial, no tener Presupuesto es una muy mala señal".

---
El gobernador Omar Perotti destacó este viernes el “altísimo nivel de vacunación” de la provincia de Santa Fe, que “nos está permitiendo, con este número de casos, no tener las camas ocupadas que tuvimos en la segunda ola, y así mantener el nivel de actividad productiva y cuidar el trabajo de los santafesinos y santafesinas”.

Durante la recorrida de la obra del desagüe pluvial Entubado Tucumán de la ciudad de Rafaela, el gobernador indicó que “tenemos un avance importante de casos. Frente a esta situación, con anticipación, planteamos el resguardo a tomar en los encuentros masivos. Hoy no hay restricciones, salvo en eventos que superen las 1.000 personas, y desde 500 en adelante con algún resguardo adicional”.

Asimismo, Perotti resaltó la necesidad de “seguir muy fuerte con los testeos”, y para ello “estamos ampliando todo lo posible, estamos desplegando la mayor capacidad posible de atención, de provisión y de testeos”; y reiteró que “tenemos un alto nivel de positividad”, por lo que, “si la persona con quien vivo dio positivo, no voy a testearme, me aíslo directamente. Esto es un beneficio para uno, que no va a hacer una cola, y ayuda a frenar el nivel de contagio”.

A continuación, el gobernador señaló la importancia de “seguir con este ritmo de vacunación. Que nadie se quede sin vacunar, porque el virus tiene que encontrarse con la mayor cantidad de puertas cerradas para que no se propague; y la gente que no se ha vacunado es la que le deja la puerta abierta a que siga circulando y contagiando”.

Por último, Perotti indicó que “el nivel de contagio y de aislamiento empieza a generar una disminución laboral; y trabajar con equipos reducidos es lo que lleva a una disminución de la actividad; por este motivo es importante la responsabilidad. Ha habido reuniones esta semana con gremios y empresarios, para garantizar el cumplimiento de los protocolos, que son los que te garantizan la actividad”.

“Hoy la instancia de restricción no es una medida del gobierno, el que restringe su actividad profesional, laboral o de recreación es el que no se cuida. Es allí donde el resguardo que cada uno tiene que tomar es clave”, concluyó el mandatario santafesino.

**PRESUPUESTO**

Seguidamente, consultado respecto a la falta de aprobación del Presupuesto nacional y el no tratamiento del provincial, Perotti indicó que “no es un dato menor, parecería que algunos quieren minimizar este hecho, como diciendo ‘bueno, se pueden arreglar’. Cuando no tenés las obras identificadas en el presupuesto, tenés que tratar de ver si tenés recursos para poder hacerlas, y allí es donde seguramente podemos tener dificultades”.

“Entonces esto no es un tema menor, la verdad que tanto a nivel nacional como provincial no tener Presupuesto es una muy mala señal. En momentos difíciles, en los que hay que facilitar las cosas a la gente, generar un clima que distienda tensiones, que genere expectativas positivas, no es precisamente la mejor señal de la política dejar sin Presupuesto a los poderes Ejecutivos”, concluyó el gobernador.