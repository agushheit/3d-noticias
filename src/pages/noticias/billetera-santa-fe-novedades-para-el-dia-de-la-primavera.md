---
category: Agenda Ciudadana
date: 2021-09-20T06:15:59-03:00
thumbnail: https://assets.3dnoticias.com.ar/BILLETERAPROMOS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Billetera Santa Fe: novedades para el Día de la Primavera'
title: 'Billetera Santa Fe: novedades para el Día de la Primavera'
entradilla: Desde el gobierno provincial aclararon que el programa Billetera Santa
  Fe no termina a fin de septiembre. Continuará, al menos, hasta fin de año.

---
En el marco del Día de la Primavera, que se festejará el próximo martes, el gobierno provincial anunció que quienes paguen en negocios gastronómicos con Billetera Santa Fe tendrán un reintegro del 40 por ciento. Esto es, 10 por ciento más que el habitual reintegro de 30 por ciento. Se espera que para el Día de la Madre y las fiestas de fin de año también se incluyan otras promociones del estilo.

Billetera Santa Fe no finaliza el 30 de septiembre. Pese a los rumores que indicaban que este era el último mes del popular programa de reintegros por compras, el secretario de Comercio Interior de la provincia, Juan Marcos Aviano, aclaró que ese día, en realidad, vence la tercera etapa, pero aseguró que el programa va a continuar hasta fin de año “con algunos retoques”.

“Lo que ocurrirá el 30 de septiembre es el vencimiento de la tercera etapa del programa. Es decir, el 1 de octubre arrancará la cuarta etapa que se extenderá hasta el 31 de diciembre de este año”, explicó el funcionario provincial.

En declaraciones a LT8, Aviano aclaró que Billetera Santa Fe “no tiene fecha de caducidad". Remarcó: "Lo ha dicho el gobernador, la idea es sostener (Billetera Santa Fe) durante toda la gestión e ir adecuando rubros y porcentajes en forma trimestral, de acuerdo a cómo se va orientando la actividad comercial y ver qué rubro se debe potenciar más o menos”.

“Por eso llevamos tranquilidad. El 1 de octubre seguimos con las mismas condiciones. Vamos a evaluar qué rubro se promueven para el Día de la Madre y las fiestas de Fin de Año. Eso lo trabajamos entre Comercio Interior y la secretaría de Turismo. Por lo tanto, el 30 de septiembre no se termina Billetera Santa Fe”, agregó.

**Cómo funciona Billetera Santa Fe**

El programa Billetera Santa Fe es un sistema de beneficios dispuesto por la provincia para incentivar la demanda de bienes y servicios mediante el otorgamiento de reintegros de dinero a consumidores finales, por las compras que estos realicen en comercios radicados en la provincia y que se hayan adherido a Billetera Santa Fe.

A siete meses de su implementación, el programa Billetera Santa Fe cruzó la barrera de los 800.000 usuarios y más de 16.000 comercios adheridos. La recepción por parte de los santafesinos fue tan grande que desde el gobierno provincial decidieron extender el programa hasta el final del mandato del gobernador Omar Perotti, en diciembre de 2023.

La razón por la que fueron extendiendo los plazos del programa (se fue prorrogando de a tres meses hasta que se decidió extenderlo hasta finales de 2023) es porque Billetera Santa Fe generó un incentivo del consumo a través de un reintegro al cliente por el consumo en comercios de los rubros promovidos.

**Cómo descargar Billetera Santa Fe**

Para descargar Billetera Santa Fe en el celular, los usuarios deben ingresar al Play Store (Android) o al App Store (IPhone) y allí buscar la app Billetera Santa Fe, de Plus Pagos.

**Cómo registrarse en Billetera Santa Fe**

Una vez instalada la aplicación en el celular, resta crear un usuario en Billetera Santa Fe. Para ello, se debe ingresar a la app, otorgar permisos y pulsar, en la pantalla principal, sobre “Registrarse”. Allí se pedirá al usuario el nombre y apellido, su género, un número de teléfono al cual la app enviará un código con cuatro dígitos que deberán ser ingresados y un mail de contacto.

Tras esos pasos, se debe crear una contraseña para la cuenta de Billetera Santa Fe, la cual debe tener más de seis caracteres y, entre ellos, al menos una mayúscula y un número. Luego de esto, la app solicitará un número de Santa Fe Servicios de referencia, aunque es un paso que se puede omitir pulsando “Siguiente”.

Para concluir, Billetera Santa Fe pedirá permiso para utilizar la cámara del celular para que el usuario pueda escanear el código de barras del DNI, además de sacar una foto del frente y dorso del mismo, y enviar dos selfies para autentificar la identidad del usuario: una común y otra haciendo un gesto.

**Cómo transferir dinero a Billetera Santa Fe**

Antes de ir a un comercio, se deben cargar fondos en la cuenta. Para transferir dinero a Billetera Santa Fe se debe cargar saldo con una tarjeta de débito de cualquier banco o acudir a un local de Santa Fe Servicios para hacer la transacción (transfiriendo desde una cuenta bancaria u otra billetera virtual).

Para transferir dinero desde el celular a Billetera Santa Fe, el usuario debe ingresar a la aplicación, pulsar en "Cómo cargar", copiar la CVU (Clave Virtual Unificada) que otorga Plus Pagos y entrar a su homebanking para hacer una transferencia a esa clave, que estará a nombre de la persona titular de la cuenta de Billetera Santa Fe.

**Cuánto dinero reintegra Billetera Santa Fe**

Una vez en el comercio y cuando el usuario ya efectuó la compra, debe avisar que va a pagar con Billetera Santa Fe. Tras ello, abrir la aplicación en el teléfono, ingresar a la app con el usuario y contraseña creados, elegir la opción “Pagar con QR”, acercar la cámara del celular al código QR que debe exhibir el comercio, ingresar el monto total de la compra y, por último, pagar con el saldo virtual transferido o que se fue acumulando por el uso de Billetera Santa Fe, o con la tarjeta de débito o crédito que esté adherida a la aplicación.

Los reintegros que realiza Billetera Santa Fe son del 30% en alimentos, heladerías, indumentaria, calzados, jugueterías, librerías, muebles, colchonerías, marroquinerías, bazares, artículos de limpieza, bares, restaurantes, farmacias, perfumerías y turismo.; y reintegros del 20% en electro, informática y artículos del hogar.