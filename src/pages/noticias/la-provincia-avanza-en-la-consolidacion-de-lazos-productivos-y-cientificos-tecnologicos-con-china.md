---
category: Estado Real
date: 2021-07-02T08:38:10-03:00
thumbnail: https://assets.3dnoticias.com.ar/china.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: La provincia avanza en la consolidación de lazos productivos y científicos-tecnológicos
  con China
title: La provincia avanza en la consolidación de lazos productivos y científicos-tecnológicos
  con China
entradilla: El objetivo es profundizar las relaciones con China como mercado estratégico.

---
En el marco de las acciones que el Gobierno Provincial viene desarrollando para profundizar los vínculos con mercados estratégicos, se realizó este jueves una videoconferencia entre funcionarios provinciales y representantes del Consulado General de Shangái, con el objetivo de lograr un debate en la agenda de trabajo y generar lazos en términos productivos y científicos-tecnológicos.  
  
Del encuentro participaron el Cónsul General, Pablo Obregón, los cónsules Adjuntos Federico López Brusau y Santiago Cataldo; y en representación de Cancillería Argentina, la directora General de Cooperación Internacional y funcionaria a cargo de la relación bilateral de cooperación con China, Victoria Armayor.  
  
Al respecto, el ministro de Producción, Ciencia y Tecnología, Daniel Costamagna, destacó: “La reunión es muy importante ya que nos parece sustancial sostener el vínculo con el Consulado de Shanghái para continuar afianzando e impulsando el desarrollo productivo y tecnológico de nuestra provincia. Hicimos un intercambio y se decidió avanzar en el desarrollo de estas vinculaciones tecnológicas a través de nuestro Ministerio”.  
  
Por su parte, el secretario de Comercio Exterior, Germán Burcher, aseguró: “Ya veníamos trabajando arduamente con China en aspectos comerciales, en esta ocasión avanzamos en lo que respecta a las vinculaciones tecnológicas. Este encuentro busca potenciar el esquema de trabajo que viene desarrollando el Consulado General de Shangái en conjunto con la Secretaría de Comercio Exterior”.  
  
Asimismo, el secretario de Agroalimentos, Jorge Torelli, mencionó: ”Es importante aprovechar la importancia de la complementariedad existente entre Santa Fe y China ya que nuestra provincia se destaca internacionalmente como productora de agroalimentos y también por sus capacidades institucionales, académicas, científicas y tecnológicas vinculadas a la agricultura y a la ganadería, que pueden dar respuesta a la demanda y necesidades exigentes de los mercados internacionales”.  
  
Además, de los mencionados funcionarios, participaron de la actividad la secretaria de Ciencia, Tecnología e Innovación, Marina Baima; la subsecretaria de Agroalimentos, María Eugenia Carrizo; y la responsable de los Asuntos con la República Popular de China del Gobierno de Santa Fe, Emiliana Hidalgo.