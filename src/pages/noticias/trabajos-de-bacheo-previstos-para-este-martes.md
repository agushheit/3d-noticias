---
category: La Ciudad
date: 2021-03-30T07:17:19-03:00
thumbnail: https://assets.3dnoticias.com.ar/bacheo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad de Santa Fe
resumen: Trabajos de bacheo previstos para este martes
title: Trabajos de bacheo previstos para este martes
entradilla: |2-

  La Municipalidad informa las calles por las que avanza el cronograma de tareas previsto en el Plan de Bacheo 2021. Puede haber cortes de circulación y desvíos de colectivos a medida que se desarrolla la obra.

---
En enero pasado, la Municipalidad puso en marcha un plan de bacheo para reparar más de 12.000 metros cuadrados de calles de la capital provincial. El programa se desarrolla por fases, priorizando las zonas de la ciudad que evidencian mayor circulación.

Debido a cambios en el plan de trabajo de la empresa a cargo de la obra, se informó que este martes, en la zona del micro y macrocentro, las tareas se desarrollarán en:

* 1° de Mayo, entre Uruguay y Jujuy
* 4 de Enero, entre Uruguay y Jujuy
* Francia, entre General López y Monseñor Zaspe

En tanto, en avenidas troncales, la obra continuará por:

* Aristóbulo del Valle, entre French y Los Pinos, mano sur-norte
* Aristóbulo del Valle, entre avenida Gorriti y 12 de Infantería, mano norte-sur

Por otra parte, en el marco de los trabajos ejecutados en la obra Conducto Pluvial Mariano Comas, operará un corte total del tránsito vehicular en:

Mariano Comas, entre San Lorenzo y avenida López y Planes. Por este motivo, se organiza el desvío de calle Mariano Comas, por San Lorenzo. El tránsito pesado, en tanto, se desviará en Urquiza hacía el bulevar Pellegrini

El municipio informa que todas las tareas se realizan siempre en sentido del tránsito. Del mismo modo, indica que puede haber cortes de circulación y desvíos de colectivos, a medida que avanza la obra y se realizan intervenciones mayores.

Además, se detalló que la realización de los trabajos está sujeta a las condiciones climáticas.

**Por el operativo de vacunación**

La Municipalidad comunica que, en el marco del operativo de vacunación que se concreta en la ciudad, habrá controles en el tránsito vehicular, entre las 8 y las 18 horas, en las inmediaciones de:

* La Esquina Encendida, en la intersección de Facundo Zuviría y Estanislao Zeballos