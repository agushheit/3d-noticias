---
category: Agenda Ciudadana
date: 2021-10-12T06:15:09-03:00
thumbnail: https://assets.3dnoticias.com.ar/vacunis.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Argentina superó los 78 millones de dosis recibidas y avanza sobre la vacunación
  de niños
title: Argentina superó los 78 millones de dosis recibidas y avanza sobre la vacunación
  de niños
entradilla: 'La cifra se alcanzó con el arribo de más de 3,2 millones de dosis de
  AstraZeneca y Pfizer este fin de semana. '

---
En la última semana llegaron al país más de 7,6 millones de vacunas contra el coronavirus de diferentes laboratorios para avanzar en el Plan Estratégico de Vacunación Nacional que ya suma 78.554.455 dosis recibidas y en cuyo marco el martes se lanzará en todo el país la inoculación de niños y niñas de entre tres y 11 años, se informó oficialmente.  
  
La cifra de 7.693.030 vacunas se alcanzó tras el arribo de los vuelos de sábado y domingo con 3.293.400 dosis de AstraZeneca, que llegaron en dos cargamentos, el primero con 1.000.100 dosis y el segundo con 2.293.300.  
  
El primer cargamento llegó en el vuelo AM30 de Aeroméxico que aterrizó en el Aeropuerto Internacional de Ezeiza el sábado a las 21, mientras que a las 6 de la mañana del domingo arribaron 2.293.300 en el vuelo AR1303 de Aerolíneas Argentinas.  
  
Estas partidas se sumaron a las 776.880 dosis que llegaron el jueves y las 848.250 que arribaron el miércoles, ambas del laboratorio estadounidense Pfizer, que se destinarán a la inoculación de adolescentes.  
  
Además, Argentina había recibido en la última semana dos nuevas entregas de Sputnik V componente 2 del laboratorio Richmond: una partida con 625.000 dosis el miércoles pasado, y otra con 1.005.500 el lunes, día en que también arribó un cargamento con 1.144.000 unidades de Sinopharm.  
  
La ministra de Salud, Carla Vizzotti, señaló en sus redes sociales que “en solo una semana, Argentina sumó más de 7,6 millones de dosis contra la Covid-19".  
"Estamos vacunando adolescentes y antes de fin de año vamos atener a todos los mayores de tres años protegidos. Seguimos avanzando en el plan de vacunación más grande de nuestra historia”, dijo la titular de la cartera de Salud.  
  
Vizzotti viajará el martes a La Pampa donde, según lo acordado en el Consejo Federal de Salud, se realizará el lanzamiento simultáneo a nivel nacional de la vacunación contra la Covid-19 para niños y niñas de entre tres y 11 años.  
  
La ministra también visitará el Hospital de Complejidad Creciente y hará una recorrida para observar el avance de las obras.  
Mientras tanto, el ministro de Salud de la provincia de Buenos Aires, Nicolás Kreplak, celebró que "Argentina es uno de los países que más vacunas aplicó en la relación a la población" y los "resultados inobjetables" que muestra la campaña de inoculación.  
  
"La campaña de vacunación es un éxito y muestra resultados inobjetables. Sumamos una semana más de caída sostenida de casos, dónde en @BAProvincia es un 25% en relación a la anterior", escribió Kreplak en su cuenta de la red social Twitter.  
  
El ministerio informó que el total de vacunas aplicadas hasta la fecha en territorio bonaerense llega a 21.032.110, de las cuales 11.620.721 corresponden a la primera dosis y 9.411.389 a la segunda.  
  
Las autoridades de la provincia de Buenos Aires reseñaron que todas las personas de entre tres y 17 años pueden inscribirse en el sitio web [VacunatePBA](https://vacunatepba.gba.gob.ar/  ) o a través de la aplicación para recibir la vacuna.  
  
En tanto, todos los mayores de 18 años cuentan con la posibilidad de acceder a la denominada "vacunación libre", lo que los habilita a dirigirse sin turno a cualquiera de los 400 puntos en los que se aplican las vacunas contra el coronavirus, sólo con DNI que acredite domicilio en la provincia, y recibir su primera dosis.  
  
Paralelamente, los mayores de 30 años, embarazadas, personal de salud, seguridad y educación, inmunodeprimidos y mayores de 18 años con enfermedades preexistentes, pueden recibir sin turno la segunda dosis de la vacuna siempre que hayan cumplido el plazo interdosis recomendado por las autoridades sanitarias.  
  
La vacunación contra el coronavirus de niños y adolescentes de entre 12 y 17 años avanzará esta semana en todo el país con la distribución de más de 1,6 millones de dosis de Pfizer por parte del Gobierno nacional, según el informe oficial.  
  
De acuerdo a lo adelantado, la cantidad exacta de dosis a repartirse de la vacuna del laboratorio Pfizer será de 1.625.130 dosis.  
  
En tanto, las vacunas de Pfizer continuarán arribando hasta fin de año para completar los 20 millones de dosis, según el contrato firmado por el Gobierno nacional.  
  
De acuerdo a los últimos datos del Monitor Público de Vacunación, hasta la mañana del lunes se distribuyeron 60.347.704 dosis en todo el territorio, mientras que las vacunas aplicadas suman 54.059.955 y de ese total, 30.176.753 personas fueron inoculadas con la primera dosis y 23.883.202 cuentan con el esquema completo de inmunización.