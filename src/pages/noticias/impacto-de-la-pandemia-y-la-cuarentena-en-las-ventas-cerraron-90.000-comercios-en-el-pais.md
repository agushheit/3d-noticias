---
category: Agenda Ciudadana
date: 2020-12-28T10:54:25Z
thumbnail: https://assets.3dnoticias.com.ar/2812-ventas.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Infobae'
resumen: 'Impacto de la pandemia y la cuarentena en las ventas: cerraron 90.000 comercios
  en el país'
title: 'Impacto de la pandemia y la cuarentena en las ventas: cerraron 90.000 comercios
  en el país'
entradilla: Según CAME, se registra un promedio de 9 locales vacíos por cuadra a nivel
  nacional. Por efecto directo de esos cierres, se perdieron más de 185.300 puestos
  de trabajo.

---
El impacto de la pandemia y de la cuarentena fue tal en el consumo y las ventas que desde que comenzó la crisis sanitaria cerraron en todo el país poco más de 90.000 comercios minoristas, según un informe publicado este domingo por la Confederación de la Mediana Empresa Argentina (CAME).

De acuerdo con una encuesta realizada en todo el país por esa entidad «el cierre masivo de locales fue una de las consecuencias graves que dejó la cuarentena en las pymes», explicó. «Los primeros números reflejan ese impacto: el 15,6% de los locales comerciales del país cerraron, sin registro de que se hayan mudado a zonas más económicas», resumió CAME.

En ese sentido, el relevamiento mostró que la tasa de mudanza registrada solo alcanzó el 0,3%. «Es decir, la crisis fue tan profunda que los comercios ni siquiera pudieron compensar mudándose a zonas más baratas, como suele suceder en períodos recesivos», apuntó CAME.

La entidad que nuclea a pymes industriales y comerciales indicó que los rubros más afectados por la crisis fueron indumentaria, calzados y decoración y textiles para el hogar.

El 15,6% de los locales comerciales del país cerraron, sin registro de que se hayan mudado a zonas más económicas.

Las cinco provincias más afectadas por los cierres resultaron Chubut, con una tasa de 34% de clausuras; Córdoba, con 25,4% de cierres; Neuquén, con 25,2%; Tierra del Fuego, con 21,2% y Santa Cruz, con una tasa de 18,8%.

Los datos fueron relevados de una encuesta realizada por CAME en 39 ciudades de todo el país y que incluyó 82.629 comercios de 1500 calles. De ese total, 12.843 estaban cerrados y vacíos, lo que implica una pérdida de 28.300 empleos. Las más perjudicadas fueron las galerías comerciales, donde la tasa de cierre alcanzó a 23%

«Proyectando los datos del relevamiento a todo el país, se puede estimar un total de 90.700 locales vacíos, con el cierre de 41.200 pymes, que involucra a 185.300 empleos», concluyó CAME basándose en esos datos.

Según la entidad, el cierre no implicará necesariamente un cese de actividades definitivo. «No todas esas pymes darán de baja sus CUIT. Se espera que, en los próximos meses, si todo se normaliza, de a poco vuelvan a reaparecer un porcentaje de ellos en la misma u otra actividad. Hay que recordar que es probable que el empresario pyme, aun en el peor momento, vuelva a levantarse y reiniciar su negocio, pese al daño ya consumado tanto para él, su familia, la de sus empleados, así como para el país», afirmó CAME.

Hubo otros casos de comerciantes que eligieron pasarse a la informalidad, especialmente en ciudades medianas y grandes. «De este modo, por ejemplo, muchos comenzaron a vender en sus casas o por redes, aprovechando que sus clientes y la comunidad los conocen y rápidamente ubican su nuevo punto o modalidad de venta», explicó la organización.

Una encuesta reciente de la Cámara Argentina de Comercio (CAC), mostró que a mediados de diciembre la mitad de las empresas todavía no había podido recuperar el nivel de ventas que tenía antes de la pandemia.

Proyectando los datos del relevamiento a todo el país, se puede estimar un total de 90.700 locales vacíos, con el cierre de 41.200 pymes, que involucra a 185.300 empleos.

Según explicó Gonzalo de León, director institucional de la cámara, «en los últimos meses hubo una mejora, pero aun así hay una parte muy importante de las empresas que no logró normalizarse», dijo en declaraciones a Radio Con Vos.

«El canal de venta online ayudó mucho. Antes de que empezara la pandemia, solo un cuarto de los comercios tenía un canal de venta online y ahora esa proporción pasó a la mitad», comentó. Por último, dijo que las perspectivas sobre generación de empleo son pesimistas. «Es muy limitado lo que se piensa expandir, solo el 4% de los comercios piensa contratar personal», cerró.

Solo los planes de pago en cuotas pudieron dar un respiro a algunos sectores del comercio minorista. Las ventas de juguetes en esta Navidad registraron un aumento de 2% en volumen con relación al año pasado, impulsadas fundamentalmente por la recuperación de la actividad económica, el programa Ahora12 -en sus distintas versiones 3, 6, 12 y 18 cuotas- y las promociones bancarias.

En tanto, el ticket promedio de comercialización fue de 900 pesos, según un informe de cámara empresarial. En términos generales, todos los segmentos presentaron incrementos moderados debido a la incipiente recuperación que está atravesando la economía argentina, explicó en el relevamiento la Cámara Argentina de la Industria del Juguete.