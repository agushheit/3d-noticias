---
category: Estado Real
date: 2021-02-06T07:16:18-03:00
thumbnail: https://assets.3dnoticias.com.ar/misoprostol.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: ANMAT aprobó el misoprostol fabricado por el LIF, el laboratorio público
  provincial santafesino
title: ANMAT aprobó el misoprostol fabricado por el LIF, el laboratorio público provincial
  santafesino
entradilla: El Laboratorio Industrial Farmacéutico provincial (LIF) es el único establecimiento
  en el país que lo fabrica dentro de la órbita estatal y desde hoy podrá proveerlo
  al sistema público de salud nacional.

---
La Administración Nacional de Medicamentos, Alimentos y Tecnología Médica (ANMAT) aprobó el misoprostol producido por el Laboratorio Industrial Farmacéutico (LIF) de la provincia de Santa Fe. El establecimiento estatal está en condiciones de proveer al resto del país un producto confiable y accesible.

Forma parte de una cartera de 40 productos que fabrica el organismo para distribuir en los efectores públicos de salud. El misoprostol se utiliza para la interrupción legal del embarazo hasta la semana 13 de gestación.

Previamente se contaba con un misoprostol fabricado en una modalidad de producción asociada. Pero desde el inicio de la gestión de Omar Perotti, se decidió comenzar a producirlo 100 por ciento dentro del LIF. Así, desde el mes de julio del año pasado se desarrollaron 300.000 pastillas y próximamente se sumará un cuarto lote con otras 100.000 unidades.  

De esta manera, el LIF es el único laboratorio de la esfera pública que lo fabrica, mientras que el resto de los productores en el país son privados.

Con la habilitación de la Anmat, el laboratorio santafesino está en condiciones de proveer al resto del país para garantizar un producto seguro, confiable y accesible en el sistema de salud público nacional.

Cabe consignar que el Instituto Nacional de Medicamentos (INAME) también emitió los informes técnicos pertinentes para lograr la aprobación.