---
category: La Ciudad
date: 2021-10-20T06:00:13-03:00
thumbnail: https://assets.3dnoticias.com.ar/palecare.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Santa Fe: el pan, la leche y la carne aumentaron en promedio un 395% en
  los últimos cuatro años'
title: 'Santa Fe: el pan, la leche y la carne aumentaron en promedio un 395% en los
  últimos cuatro años'
entradilla: 'En septiembre de 2017 el litro de leche costaba $23,85 y el kilo de pan
  $31,04. En el mismo período de tiempo, la yerba subió un 620 por ciento.

'

---
Un relevamiento de precios realizado por el diario UNO Santa Fe detectó que en los últimos cuatro años, el pan, la leche y la carne aumentaron -en promedio- un 395 por ciento en la provincia.

Se trata de productos básicos, indispensable en la planificación alimentaria de una familia. Las subas sorprenden aún más si se tiene en cuenta que durante ese mismo lapso de tiempo hubo diversas estrategias de los gobiernos nacionales (entre ellas precios cuidados) para frenar los incrementos. En ese sentido, en horas de la tarde de este martes, la idea del secretario de Comercio de la Nación, Roberto Feletti de alcanzar con los privados un congelamiento de precios parecía derrumbarse.

El análisis estuvo realizado en base a los informes oficiales que publica el Ipec (Instituto Provincial de Estadísticas y Censos) a través del Índice de Precios al Consumidor.

Del mismo, se desprende que en septiembre de 2017 el kg de pan costaba $31,04 en la provincia de Santa Fe y ahora cuesta $146,56; un incremento del 372 por ciento. El litro de leche (sachet) valía $23,85 hace cuatro años y ahora se consigue a 105,62; una suba del 342 por ciento. La carne es el producto que más subió de los tres durante ese lapso de tiempo: el kilo de nalga que tenía un valor de $153,10; ahora cuesta $ 875,18; un 471 por ciento más.

Se suman otros productos que también impactan por su aumento. Es el caso de la yerba, que en cuatro años registró una suba en Santa Fe del 620 por ciento. Es que, según el Ipec, el medio kg tenía un precio de $33,71, contra los $ 242,97 de los actuales

Se trata de productos (leche, pan, carne) cuyo precio final tiene consigo una sumatoria de costos vinculadas a cada una de las etapas del proceso de comercialización y de producción. Por otro lado, son alimentos sobre los cuales Santa Fe tiene un vínculo especial, ya que es un actor fundamental en el país, por ser una provincia ganadera y formar parte de la cuenta lechera del país.

Un nuevo informe de la Confederación Argentina de la Mediana Empresa indica que la brecha de precios para el promedio de los 24 principales productos agropecuarios que participan de la mesa familiar aumentó 6% en agosto, impulsada principalmente por la caída en los precios que recibió el productor. El consumidor pagó 6,22 veces más de lo que cobró el productor por esos alimentos. La participación del productor en los precios de venta subió levemente, a 25,2%.

En este contexto de escalada de precios, parecen frustrados los intentos del gobierno por buscar acuerdos con el sector empresario para frenar el ritmo de inflación.

**Inflación en Santa Fe**

El Nivel general del Índice de Precios al Consumidor de Santa Fe registró en septiembre de 2021 un aumento de 3,0 por ciento con relación al mes anterior. Se ubicó por debajo del índice nacional que fue del 3,5 por ciento.

La inflación acumulada en el año en la provincia es del 37,8 por ciento y en un año (a septiembre del 2020) alcanza el 53,7 por ciento.

La suba de precios en septiembre estuvo impulsada por educación (un 5,4 por ciento), seguido por Atención médica y gastos para la salud (4,3 por ciento) indumentaria (3,8 por ciento)

En el otro extremo, “Vivienda y servicios básicos” experimentó una suba del 1,2% en septiembre, por debajo del nivel general, y “Esparcimiento” aumentó 1,8%.

El capítulo “Alimentos y bebidas”, en tanto, se ubicó apenas por debajo del nivel general, con un aumento en septiembre de 2,9%, de acuerdo a la medición del instituto santafesino.