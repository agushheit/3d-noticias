---
category: Agenda Ciudadana
date: 2021-10-11T06:00:27-03:00
thumbnail: https://assets.3dnoticias.com.ar/camara.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: 'Diputados: oficialismo y oposición buscan avanzar con un temario consensuado'
title: 'Diputados: oficialismo y oposición buscan avanzar con un temario consensuado'
entradilla: Será un encuentro virtual en las próximos días entre los secretarios parlamentarios
  de los bloques y buscarán un acuerdo en el temario, luego de la fallida sesión de
  la semana pasada.

---
Los principales referentes de los bloques del oficialismo y la oposición de la Cámara de Diputados buscarán avanzar esta semana en acuerdos parlamentarios para definir una agenda de temas consensuados que permitan asegurar el quórum para realizar una sesión especial en los próximos 15 días.  
  
Los diputados de los diferentes bloques intentarán acercar posiciones y acordar una agenda de temas que permitan alcanzar los 129 diputados, tras la fallida sesión de la semana pasada en la que los bloques opositores decidieron no bajar al recinto y así dejaron sin quórum el plenario, el primero de manera presencial plena tras la pandemia.  
  
La perspectiva de concretar la sesión surgió luego que el oficialismo se comprometió a escuchar las propuestas de la oposición y buscar incluir alguna de ellas en un temario de consenso que siguió al cruce entre ambos sobre la responsabilidad de uno u otro sector en la conformación del quórum, informaron a Télam fuentes parlamentarias.  
  
Para ello, se acordó un encuentro virtual en las próximos días entre los secretarios parlamentarios de los bloques, en el que se comenzará a pulir un eventual temario y la convocatoria a comisiones para esta semana o entre el 19 y 20 de octubre.  
De hecho, las conversaciones se iniciaron el mismo miércoles tras la sesión, en un encuentro que se realizó de manera informal en el despacho del presidente de la Cámara de Diputados, Sergio Massa y del que participaron el jefe del bloque del Frente de Todos, Máximo Kirchner; del interbloque de Juntos por el Cambio, Mario Negri; del PRO, Cristian Ritondo y de la Coalición Cívica, Juan Manuel López.  
  
Si bien el temario contemplaba proyectos poco conflictivos, como el de etiquetado frontal de alimentos y una ley para garantizar los derechos humanos de las personas en situación de calle en la Argentina, la oposición decidió no bajar al recinto y, por lo tanto, no dar quórum para sesionar, al acusar al oficialismo de convocar a la sesión sin acordar el temario.  
  
Al respecto, el presidente de la Cámara de Diputados, Sergio Massa, dijo que hay "temas que tenemos que aprender a sacarlos de la discusión electoral y trabajarlos de manera conjunta sin que eso implique ceder en las ideas que uno representa".  
  
En declaraciones a Radio 10, el diputado dijo que "no dar quórum es un instrumento que prevé el reglamento", pero advirtió que la sesión frustrada lesionó "la expectativa de los millones de Argentinos que quieren saber qué comen".  
  
En esa ocasión Negri planteó que Juntos por el Cambio no se opone al proyecto de etiquetado frontal de alimentos, pero reclama que la Cámara baja avance además en el debate del proyecto de emergencia educativa y la derogación de la ley de alquileres y que se convoque para la semana próxima a comisiones para que ambas iniciativas logren dictamen.  
  
Por otro lado, las fuentes no descartaron que el temario contemple la Ley Ovina, que tiene dictamen y es impulsada por la coalición opositora, el bloque de Unidad y Equidad, que encabeza José Luis Ramón; y Luis Di Giacomo, de Juntos Somos Río Negro.  
  
En tanto, el presidente del interbloque Federal, Alejandro "Topo" Rodríguez, planteó el jueves pasado la necesidad de convocar en "forma inmediata" en los próximos días a una sesión consensuada entre los bloques parlamentarios para debatir el proyecto de etiquetado frontal, pero incorporando otras iniciativas, como la ley para el impulso de la actividad ovina, la Asignación Universal por Hijo a trabajadores discontinuos del sector agrario y la creación del área marina protegida bentónica denominada "Agujero Azul".  
  
Alejandro "Topo" Rodríguez, planteó el jueves pasado la necesidad de convocar en "forma inmediata" en los próximos días a una sesión consensuada entre los bloques.

Las fuentes estimaron que la semana próxima se podría avanzar en los dictámenes pendientes, en tanto que entre el 19 y el 20 se podría convocar a una nueva sesión especial que incluya los temas acordados.  
  
Según las fuentes, el objetivo es reactivar el trabajo en comisiones con la presencialidad plena y mostrar que más allá de la campaña, la Cámara de Diputados "se mantiene activa" y logra avanzar en acuerdos parlamentarios entre oficialismo y oposición.