---
category: La Ciudad
date: 2020-12-23T17:33:26Z
thumbnail: https://assets.3dnoticias.com.ar/2412peatonal.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Se peatonaliza calle Mendoza desde San Jerónimo hasta 25 de Mayo
title: Se peatonaliza calle Mendoza desde San Jerónimo hasta 25 de Mayo
entradilla: Se concreta los días 23 y 24 de diciembre. La medida apunta a brindar
  mayor comodidad a los vecinos y favorecer a los comerciantes en este sector del
  microcentro.

---
Con motivo de las compras navideñas, la Municipalidad dispuso peatonalizar calle Mendoza, desde San Jerónimo hasta 25 de Mayo. La iniciativa se concretará  los días miércoles 23,  desde las 16 hasta las 20 horas, y  jueves 24, en el horario de 9 a 13.

La medida apunta a que los vecinos puedan recorrer con mayor comodidad el sector céntrico de la capital santafesina, favoreciendo a su vez a los comercios del lugar.