---
category: Agenda Ciudadana
date: 2021-01-05T11:00:21Z
thumbnail: https://assets.3dnoticias.com.ar/050121-monotributo.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Télam'
resumen: El Gobierno envió al Congreso un proyecto de ley para el régimen del monotributo
title: El Gobierno envió al Congreso un proyecto de ley para el régimen del monotributo
entradilla: La iniciativa, que será debatida en las sesiones extraordinarias, también
  contempla la actualización para las escalas y montos del monotributo correspondientes
  al período 2020.

---
Las modificaciones propuestas en la iniciativa apuntan a armonizar la transición entre el Régimen Simplificado para Pequeños Contribuyentes (RS) y el Régimen General (RG), tanto en términos administrativos como en los montos de las obligaciones que deben afrontar las personas monotributistas.

El Gobierno envió un proyecto de ley al Congreso de la Nación que permite abordar problemáticas estructurales y coyunturales experimentadas por las personas monotributistas, adelantaron este lunes a Télam fuentes de la Administración Federal de Ingresos Públicos (AFIP).

La iniciativa, que será debatida en las sesiones extraordinarias, también contempla la actualización para las escalas y montos del monotributo correspondientes al período 2020, que comenzarán a regir cuando se apruebe el proyecto, agregaron las mismas fuentes.