---
category: La Ciudad
date: 2021-01-07T10:35:59Z
thumbnail: https://assets.3dnoticias.com.ar/070121-guardia-violencia.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: El Litoral'
resumen: Rincón propone una guardia institucional para atender a las víctimas de violencia
title: Rincón propone una guardia institucional para atender a las víctimas de violencia
entradilla: Se abrió la convocatoria para conformar el equipo de la guardia pasiva
  de Género y Niñez, que comenzará a funcionar en marzo. El objetivo será brindar
  asistencia y acompañamiento a mujeres e infancias las 24 horas.

---
Los interesados deben ser mayores de 18 años y ser profesionales o estudiantes de carreras vinculadas al área de las ciencias sociales, entre otros conocimientos específicos.

Para tener la problemática de violencia que afecta a mujeres e infancias, la Municipalidad de San José del Rincón informó que **se encuentra abierta la convocatoria para integrar la Guardia Institucional de Género y Niñez**, que tiene como objetivo brindar asistencia y protección a víctimas de violencia las 24 horas.

Belén Inalbón, directora de Género y Diversidad Sexual de Rincón, dialogó con El Litoral y puso en foco el tema que demanda un fuerte trabajo para ser abordado y que llevó a la propuesta de crear una guardia institucional. «La idea es que se pueda asesorar a las instituciones o a las personas que estén atravesando situaciones de violencia, y de requerirse alguna medida de protección nos hagamos presentes en el lugar y se realicen las acciones que correspondan», comentó Inalbón.

**La guardia tiene como objetivo garantizar el acceso eficaz e igualitario de la ciudadanía a los procedimientos garantes de derechos en las situaciones de urgencia**, que requieren de medidas de protección en el momento, atendiendo al escenario en el que se producen. Para ello «contempla la conformación de un equipo de guardia pasiva de carácter institucional, en el que la comunicación con y desde la guardia será establecida por instituciones u organismos estatales como es el caso del SAMCo, Comisaría N° 14 y Centro de Orientación a la Víctima de Violencia y Abuso Sexual, además de la Agencia de Trata, escuelas, Centro Territorial de Denuncias, hospitales y guardias provinciales de Niñez y Género y de la ciudad de Santa Fe, Fiscalía, central de Monitoreo de Santa Fe y de San José del Rincón», explicaron desde el gobierno de Rincón.

**Con esta medida buscan ampliar la Dirección de Género, Niñez y Familia municipal para brindar asistencia a víctimas de violencia las 24 horas**. La creación de este espacio fue anunciada por el intendente Silvio González semanas atrás, al enviar el proyecto ejecutivo de Presupuesto 2021 al Concejo Municipal.

«La guardia es algo por lo que venimos bregando desde hace mucho tiempo. La pandemia está empezando a sentirse, ahora **tuvimos que trabajar en la asistencia de las consecuencias del aislamiento obligatorio**. Estas fechas de Fiestas y vacaciones son un factor condicionante para ciertas cuestiones, porque empezamos a salir más a la calle, hay mayor circulación, consumo de alcohol, y todo esto implica un aumento en situaciones de violencias», analizó la directora de Género y Diversidad Sexual.

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.15em; font-weight: 700"> Primera instancia</span>**

En primera instancia, el equipo que integrará la guardia realizará atención exclusivamente telefónica, recibiendo y derivando llamados a las áreas correspondientes. **Los integrantes recibirán una capacitación previa** para poder abordar adecuadamente dicha tarea.

**Se deberá presentar como documentación:**

* DNI y copia certificada;
* certificado de antecedentes penales con copia certificada;
* certificado de buena conducta con copia certificada;
* constancia de inscripción en AFIP.

**Las inscripciones estarán abiertas hasta el 22 de enero de 2021** y la documentación debe ser presentada en Mesa de Entradas Municipal, que funciona en Juan de Garay 2519, de lunes a viernes de 7.30 a 12.

Luego de reunir a los profesionales postulados, se hará una etapa de selección; un examen para evaluar los conocimientos; después se harán las entrevistas; y por último habrá un espacio de formación con el equipo completo seleccionado.

«El equipo de guardia estará conformado por cuatro personas, que tengan semanas rotativas en el mes. La persona encargada de la guardia, una vez que termine su horario, presenta un informe al equipo del Área de Género y Diversidad Sexual. La idea es que sea un trabajo articulado», indicó la funcionaria.

Esta guardia que camina sus primeros pasos, para Inalbón era «una deuda. Estamos muy contentos, sabemos que no resuelve, pero es un gran avance. Es darle la posibilidad a aquellas mujeres y niños de que tengan mayores derechos y acompañamiento», dijo y agregó que es momento de que «las instituciones empecemos a trabajar en red y construir intervenciones para acompañar los procesos».

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.1em; font-weight: 700">Requisitos</span>**

El gobierno municipal de San José del Rincón señaló los requisitos a tener en cuenta para ser parte del equipo de la Guardia Institucional de Género y Niñez del municipio: deben cumplir con **requisitos excluyentes como tener o ser mayores de 18 años y ser profesionales o estudiantes de carreras vinculadas al área de las ciencias sociales** (Trabajo Social, Abogacía, Psicología, Psicología Social, Sociología, Tecnicatura en Niñez y Familia, entre otros).

Además, debe poseer conocimientos específicos sobre el marco normativo del sistema de Promoción y Protección de Derechos de Niños, Niñas y Adolescencias y Mujeres e identidades disidentes. 

También, conocimientos generales sobre la Convención para la Eliminación de todas las Formas de Discriminación contra la mujer CEDAW; la Convención Internacional de los Derechos del NIño; y Principios de Yogakarta.

Otros requisitos son poseer conocimientos de herramientas informáticas como procesador de texto, internet, correo electrónico; y poseer capacidad de trabajo en equipo, para establecer y desarrollar comunicaciones interpersonales claras y sencillas, y trato respetuoso y amable.

Como requisitos no excluyentes se destacan residir en San José del Rincón, poseer experiencia o antecedentes vinculadas al trabajo en las temáticas de género y disidencias y/o niñez, y manejo de PC (procesador de texto y hoja de cálculo).