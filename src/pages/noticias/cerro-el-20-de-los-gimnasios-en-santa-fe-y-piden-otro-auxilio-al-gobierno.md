---
category: La Ciudad
date: 2022-01-21T06:15:21-03:00
thumbnail: https://assets.3dnoticias.com.ar/Gimnasios.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Cerró el 20 % de los gimnasios en Santa Fe y piden otro auxilio al gobierno
title: Cerró el 20 % de los gimnasios en Santa Fe y piden otro auxilio al gobierno
entradilla: Aseguran que el crecimiento abrupto de los casos de coronavirus y los
  aislamientos masivos impactaron directamente en la actividad, haciendo caer la facturación
  en un 60%.

---
El comienzo de las restricciones por la pandemia del coronavirus hace casi dos años impactó sobre toda la economía. El ámbito de la actividad física no fue la excepción y los gimnasios fue uno de los rubros más afectados.

 Un relevamiento de la Cámara de Gimnasios de Santa Fe arrojó que aproximadamente entre un 15% y un 20% de gimnasios han cerrado en forma definitiva en la ciudad de Santa Fe. "El resto ha tratado de mantenerse en medio de una situación coyuntural fluctuante, afrontando numerosos gastos y, al mismo tiempo, una reducción en los ingresos de las cuotas ya que por diversas razones, la asistencia de los socios se ha vuelto irregular”, dijo Marcelo Visuara, integrante de la entidad.

La situación actual que están atravesando muchos negocios de este rubro hace cada vez más difícil mantener a flote la actividad. Por eso habían presentado “el 23 de agosto del 2021 un petitorio ante el Ministerio de la Producción de la Provincia de Santa Fe solicitando la continuidad de las ayudas económicas hasta fin de año, avizorando el panorama que a corto plazo íbamos a tener que afrontar, sin obtener ningún tipo de respuesta”, informó Visuara.

 Sin bien reconocen que las ayudas anteriores tuvieron un pequeño impacto beneficioso en la incipiente reactivación del sector, en estos momentos necesitan nuevamente un aporte para poder afrontar los gastos del comercio, como así también atender a las demandas salariales del personal que trabaja en el rubro.

 La provincia, en el año 2021, “nos ha asistido en tres oportunidades a través de subsidios de diversos montos, los que han resultado un simple paliativo ante esta compleja situación, que hoy se acrecienta aún más", aseguró. Enero es un mes que históricamente fue el más bajo del año en cuanto a montos recaudatorios y a esto suman que muchos gimnasios continúan refinanciando deudas -servicio, alquileres, impuestos, juicios laborales-; la ola del calor; el aumento de casos de Covid; la incorporación del pase sanitario: "Esto nos restringe el mercado. Toda esta situación hace que definitivamente sea muy difícil sostener el negocio”, explicó.

Esta semana desde Rosario presentaron un petitorio donde le piden al Gobierno provincial que renueve los subsidios. La Cámara de Santa Fe adhiere a este pedido “porque estamos viviendo todos la misma situación. Ya en otras oportunidades hemos hecho presentaciones en conjunto y realmente pensamos de la misma manera”, sostuvo Visuara.

 Por otro lado adelantó que “estamos comunicándonos con gente cercana al Ministerio de la Producción para ver si en forma conjunta y con sentido común podemos encontrarle una solución a esta agobiante problemática''. Solicitamos que se revea la posibilidad de una nueva puesta en marcha de un subsidio provincial que de respiro al sector. Hasta ahora sin respuestas” .

 **Pase Sanitario**

 Desde la entrada en vigencia del decreto Nº 2915, los gimnasios están pidiendo a sus clientes que tengan el pase sanitario para poder ingresar al establecimiento. Además de respetar las medidas de seguridad como el uso de barbijo y alcohol.