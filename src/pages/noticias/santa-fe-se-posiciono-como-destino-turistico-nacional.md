---
category: Agenda Ciudadana
date: 2021-02-03T06:30:24-03:00
thumbnail: https://assets.3dnoticias.com.ar/turismo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: " Santa Fe se posicionó como destino turístico nacional"
title: " Santa Fe se posicionó como destino turístico nacional"
entradilla: 'Durante los meses de diciembre y enero arribaron a la provincia unos
  145 mil visitantes, superando por primera vez a otros destinos líderes en el sector. '

---
Según el ranking de destinos obtenidos a través de la tramitación del certificado Cuidar Verano, la Secretaría de Turismo del Ministerio de Producción, Ciencia y Tecnología detalló este lunes que Santa Fe se encuentra entre las diez provincias más elegidas por los turistas esta temporada.

Los datos dan cuenta que, por primera vez, el territorio provincial supera a otros líderes en el sector, tales como Tierra del Fuego, Misiones, Jujuy y Tucumán.

En el periodo diciembre-enero, el total de arribos a la provincia fue de 145 mil visitantes, según datos brindados por el Ministerio de Turismo y Deportes de la Nación. A esa cantidad hay que sumarle los miles de santafesinos que vacacionaron dentro de la provincia. Vale recordar que la apertura turística de esta temporada fue para todos los destinos provinciales desde cualquier punto del país o el exterior desde el pasado 21 de diciembre.

Al respecto, el secretario de Turismo, Alejandro Grandinetti, destacó: “Estamos muy conformes con la información que nos llega desde la cartera nacional, ya que figuramos entre las diez provincias más elegidas por los argentinos. Este es un dato muy significativo porque, por un lado, no esperamos estar en este lugar de manera tan repentina y, por otro, porque la apertura de la actividad turística en Santa Fe se dio el 21 de diciembre y estas son estadísticas que incluyen diciembre y enero”.

“Esto ratifica el momento que vive el turismo receptivo, ya que no solamente hablamos de los que llegan desde otros distritos, sino también de los santafesinos recorriendo su propia provincia. Al tiempo que los datos condicen con los niveles de ocupación, que van del 85% al 100%. Y que, de cara al futuro, ya cuenta con reservas para los feriados de Carnaval y Semana Santa”, resaltó.

Como un dato auspicioso, Grandinetti remarcó que “por ejemplo en la ciudad de Rosario, según información brindada por su municipio, existe un nivel de reservas de hotelería del 30%, cuando el promedio para enero solía ser del 14%. Es decir, se duplicó en relación a años sin pandemia. Este afluente de visitantes se repite en otras ciudades y nos llevan a ratificar el compromiso de trabajo con esta actividad y con el rumbo marcado por el gobernador Omar Perotti”.

**LAS LOCALIDADES MÁS VISITADAS**

Entre los destinos más elegidos, se encuentran la ciudad de Rosario con el 37% del total de visitantes nacionales y la ciudad de Santa Fe, con el 10%. “En el ranking le siguen Reconquista, Rafaela, Venado Tuerto, Santo Tomé, Villa Ocampo, Esperanza, Santa Rosa de Calchines, Cañada de Gómez, San Lorenzo, San Justo, Villa Constitución, Funes, Sauce Viejo, San Javier, Arroyo Leyes, Coronda, Cayastá, Aarón Castellanos y Rufino”, detalló el secretario de Turismo.

La información brindada por la aplicación también permite conocer la procedencia de los visitantes. “En ese sentido, las provincias que más turistas aportaron a nuestro territorio son Buenos Aires, CABA, Córdoba, Entre Ríos, Mendoza, Chaco y Tucumán”, concluyó el funcionario provincial.