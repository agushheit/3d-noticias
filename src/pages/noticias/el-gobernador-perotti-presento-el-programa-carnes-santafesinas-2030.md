---
category: Estado Real
date: 2021-08-12T06:00:23-03:00
thumbnail: https://assets.3dnoticias.com.ar/CARNES.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: El Gobernador Perotti presentó el programa carnes santafesinas 2030
title: El Gobernador Perotti presentó el programa carnes santafesinas 2030
entradilla: " Destacó la importancia de que este programa “genere la motivación necesaria
  porque es una oportunidad concreta de mercado, que podemos y debemos aprovechar”."

---
El gobernador Omar Perotti, junto al ministro de Producción, Ciencia y Tecnología, Daniel Costamagna, presentó este miércoles el Programa Carnes Santafesinas 2030, que tiene por objetivo generar un crecimiento sustentable de la producción con el fin de estimular el agregado de valor y la adopción de innovaciones tecnológicas.

Durante el acto, el gobernador agradeció “a todos y a todas las instituciones que vienen ayudando a conformar y a perfilar este Programa de Carnes Santafesinas, que genere la motivación necesaria en cada uno de los productores, para ya no hablar solamente de carne bovina, sino incorporar las otras opciones, porque es una oportunidad concreta de mercado, que podemos y debemos aprovechar como santafesinos”.

“Para nosotros, la producción de carnes no es una actividad más, es una actividad de fuerte arraigo y con mucho potencial, y tenemos que tener la inteligencia de saber que en la diversidad de esa canasta de carnes hay opciones, así cómo cuáles son los mercados no tradicionales o más demandantes para tener una política específica para ellos”, indicó Perotti.

Asimismo, el mandatario provincial señaló la importancia de “mostrar cuál es la diferencia de producir carnes en un territorio como el santafesino, ante otros lugares donde no tienen territorio. Aquí hay extensión y hay posibilidades de producción más sustentable y de tener un diferencial allí muy marcado frente a cualquier instancia de desafío de consumidores”.

A continuación, el gobernador destacó que en “los Bajos Submeridionales tenemos, y vemos, un enorme potencial de producción para aprovechar plenamente y con sustentabilidad, para ello hay que invertir y realizar obras. Allí hay un diferencial importante, que puede estar sumando Santa Fe”.

Por último, Perotti reiteró que “esta posibilidad de desarrollo nos encuentra con unas perspectivas muy buenas, porque hay una fuerte decisión del gobierno provincial de invertir en infraestructuras en el norte, hay una fuerte decisión de consolidar esa inversión”.

En tanto, el ministro Costamagna resaltó que “estamos ante un desafío que no sólo tiene que ver con la carne bovina, sino también con la carne aviar, porcina, ovinos y caprinos. El desafío es producir más y tener más productores, generar empleo, arraigo y que los sueños de los jóvenes y las familias se consoliden en sus lugares”.

“Queremos que este sea el programa de todos, y que todos nos involucremos”, precisó Costamagna; al tiempo que indicó que “este programa es el crecimiento sustentable y equilibrado, la generación de empleo, agregado de valor y nuevas tecnologías, el abastecimiento del mercado interno y políticas y estrategias exportadoras. Partimos del objetivo de producir 300 mil terneros más en la provincia, acompañado con la capacitación, la incorporación de tecnología y la organización de los productores”, indicó.

Asimismo, anunció la puesta en marcha “del esquema de digitalización de marcas y señales, que va a posibilitar la puesta a punto y actualización de todos los productores, en lo que tiene que ver con la seguridad, la comercialización, y la trazabilidad de nuestro ganado. Además, Rup nos va a permitir el intercambio con los productores, el acceso a programas y al financiamiento que estamos trabajando desde la provincia de Santa Fe”.

Al respecto, Costamagna precisó que “estamos trabajando en una inversión fuerte en subsidios de tasa, con Banco Nación, con el Nuevo Banco de Santa Fe; y con las asociaciones y agencias para el desarrollo; también con programas nacionales e internacionales de gestión de acceso al crédito”, para el financiamiento de “módulos productivos, organización de productores, honorarios de profesionales, infraestructura de aguas, comederos, insumo de manejo de pasturas, parideras, tinglados, ensenadas, pistas de engorde, corrales, e instalaciones de manejo; la incorporación de génetica; capital de trabajo para feedlots, novillo de exportación; incorporación de especies, producción y reposición de planteles”.

Por su parte, el secretario de Agroalimentos, Jorge Torelli, describió el panorama mundial y sostuvo que “Santa Fe es una provincia que se distingue por ser una de las principales productoras de alimentos. Globalmente las carnes que lideran el mundo son la carne aviar en un 40 por ciento y la carne porcina en un 35 por ciento y un 21 por ciento la carne bovina. Después tenemos un 4 por ciento de carne de ovinos y caprinos”.

“En Argentina, la relación es un poco diferente, nosotros somos un país muy ganadero. La tradición ganadera es muy importante y dentro de la composición de la producción de carne argentina, el 51 por ciento de la producción es de carnes bovinas, un 37 por ciento es de carne aviar, un 11 por ciento de carne porcina y un 1 por ciento de carne ovina y caprina. Esto es una muy buena oportunidad de crecimiento en el caso de las carnes porcinas y, en el caso de las carnes aviares, potenciar aún más”, puntualizó Torelli.

Por último, el senador provincial Marcelo Lewandowski, destacó que “es un programa muy ambicioso, que tiene que ver con aumentar la oferta productiva de carnes de distintos; y apoyar al productor en tecncología e infraestructura; para que quien se ponga a producir sepa que tiene el sustento del Estado acompañándolo, y en una zona donde tenemos que producir”.

**PRESENTES**

Acompañaron al gobernador en la actividad, la ministra de Infraestructura, Servicios Públicos y Hábitat, Silvina Frana; y la subsecretaria de Coordinación Agroalimentaria, María Eugenia Carrizo. Estuvieron, además, el secretario de Desarrollo Territorial y Arraigo, Fabricio Medina; el secretario de Empresas y Servicios Públicos, Cailos Maina, y senador provincial, Marcelo Lewandoski, así como también, intendentes y presidentes comunales de departamento Vera; representantes de las sociedades rurales de Vera, San Cristóbal, Tostado, San Javier, San Justo, La Criolla y Calchaquí,

Participaron también, representantes de las siguientes entidades y asociaciones civiles, INTA Reconquista; SENASA; CIASFE, Asociación Civil Mesa Azucarera y de Desarrollo Regional Santafesino (ACMADRS), CORENOSA; Unión Agrícola de Avellaneda, Industrias Frigoríficas Bovinos y Porcinos; Feedloteros; Cámara Avícola Santafesina; Cámara de la Industria Santafesina Centro Norte Argentina; Fundapaz; Federación de Sindicatos de Trabajadores de la Carne y Afines de la República Argentina; Federación de Obreros y Empleados de la Carne; Asociación para el Desarrollo del Departamento 9 de Julio; Unión Obreros Cuña Boscosa (UOCB); Asociación para el Desarrollo del Departamento Vera; Asociación para el Desarrollo del Departamento Reconquista; productores locales; personal del ministerio y equipo técnico del Establecimiento Las Gamas.