---
layout: Noticia con imagen
author: "Fuente: Diario UNO"
resumen: Créditos para motos
category: Agenda Ciudadana
title: Los créditos para la compra de motos del Banco Nación agotaron el cupo
  mensual en 72 horas
entradilla: Más de 45 mil personas ingresaron desde el lunes al sistema del BNA
  para averiguar por la línea de financiamiento para motos.
date: 2020-11-20T11:45:42.439Z
thumbnail: https://assets.3dnoticias.com.ar/motos.jpg
---
Los créditos para la compra de motocicletas de hasta $ 200.000, a un plazo de 48 meses y una tasa final del 28,5%, se agotaron en las primeras 72 horas de vigencia del programa el cupo mensual dispuesto por las concesionarias y fabricantes de motos nacionales, informó hoy El Banco Nación (BNA).

En total, más de 45 mil personas ingresaron desde el lunes al sistema del BNA para averiguar por la línea de financiamiento para motos, un proyecto conjunto con el Ministerio de Desarrollo Productivo, que aportó una bonificación de 10 puntos porcentuales de la tasa de interés, ya incluida en el 28,5% final.

"Los usuarios que cumplieron todos los requisitos para acceder al crédito superaron holgadamente lo que las fábricas y concesionarias estaban en condiciones de entregar, motivo por el cual se comprometieron a intensificar la producción para satisfacer la mayor demanda de motos en el próximo mes", explicó el Nación en un comunicado.

Las más de 200 concesionarias de motos de todo el país ofrecían motos de 34 modelos de fabricación nacional, como la Honda CG150 Titán, a $ 199.900; la Zanella ZB110, a $ 78.990; la Gilera VC150, a $ 98.000; o la Corven Hunter RT, a $ 100.000.

![](https://assets.3dnoticias.com.ar/moto1.jpg)

El monto máximo a financiar por usuario es hasta $ 200.000, a un plazo único de 48 meses, sistema de amortización francés, y alcanza a todos los usuarios, clientes o no clientes de la entidad.

Para gestionar la línea debe ingresarse a la web del Banco Nación, luego se completa la documentación de forma presencial, en tanto la comercialización de las unidades se realizará a través de la red de concesionarios que tienen acuerdo con la entidad.

La selección del producto se efectúa mediante el Marketplace incorporado en el sitio del Banco (https://tiendabna.com.ar/mi-moto) donde se exhiben los distintos modelos de las motocicletas.

Respecto a la tasa de interés, para quienes cobren sus haberes a través del BNA, será de 28,5% en tanto que para el resto de los usuarios será de 37,5%. La garantía es a sola firma y se financia el 100% de la motocicleta.