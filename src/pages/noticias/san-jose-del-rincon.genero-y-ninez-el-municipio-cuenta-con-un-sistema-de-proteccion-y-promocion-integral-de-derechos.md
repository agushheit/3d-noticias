---
category: La Ciudad
date: 2021-06-06T06:15:17-03:00
thumbnail: https://assets.3dnoticias.com.ar/GENERORINCON.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad de Rincón
resumen: 'San José del Rincón. Género y Niñez: el municipio cuenta con un Sistema
  de Protección y Promoción Integral de Derechos'
title: 'San José del Rincón. Género y Niñez: el municipio cuenta con un Sistema de
  Protección y Promoción Integral de Derechos'
entradilla: 'Creado por Ordenanza, funciona bajo la órbita de la Dirección de Género,
  Niñez y Familia y cobija el Servicio Local de Niñez, el Equipo Interdisciplinario
  de Género y la Guardia Institucional de Género y Niñez. '

---
La Municipalidad de San José del Rincón ahora cuenta con un Sistema de Protección y Promoción Integral de Derechos de Niños, Niñas, Adolescentes, Mujeres e identidades disidentes. El mismo se conforma por: el “Área de las mujeres, género y diversidad sexual” y el “Servicio Local de Protección y Promoción Integral de los Derechos de niñas, niños y adolescentes”, que, si bien ya vienen funcionando, por primera vez tienen un marco normativo que favorece su institucionalización.

La concejala Carmen Cornaglia fue autora de la propuesta, en conjunto con la Dirección de Género, Niñez y Familia del municipio, a cargo de Belén Inalbon, quien consideró que “generar espacios con el Concejo favorecen a estas articulaciones y a estos proyectos integrales que trascienden las gestiones y que piensan en las problemáticas y a los sujetos a las que estas atraviesan”.

De 2018 a la fecha el área de Género, Niñez y Familia se fue jerarquizando: de ser una coordinación de un Equipo Interdisciplinario, pasó a convertirse en dirección, donde se constituyó también el Área de Género y Diversidad Sexual. En marzo de este año se lanzó la Guardia Institucional de Género y Niñez, siendo en total 10 los profesionales que trabajan en estos tres espacios que hoy cobija el Sistema de Protección y Promoción Integral de Derechos de Niños, Niñas, Adolescentes, Mujeres e identidades disidentes en el ámbito de la Municipalidad de San José del Rincón.

“Esto es parte del proceso de institucionalización que la gestión de Silvio González, de la que formamos parte, claramente tomó la decisión de llevar a cabo. No solamente la problemática de género y diversidad sexual, sino también la de niñez, desde una perspectiva de género”, aseguró Belén Inalbon, directora de Género, Niñez y Familia del municipio. La funcionaria sumó que “este es un elemento más de todo un proceso que venimos desarrollando. Avanzamos en el fortalecimiento de los dos equipos y de la perspectiva desde la cual se trabaja para también cuidar a los trabajadores, garantizando espacios de formación y para pensar las prácticas”, detalló la funcionaria para luego agregar que esta normativa “es un piso, es el plafón que nos convoca y nos exige repensar y seguir generando estrategias para el fortalecimiento de este sistema que trascienda a la gestión de turno”.

Inalbon afirmó que desde la gestión del intendente Silvio González “hay una clara decisión de transversalizar la perspectiva de género: “Más allá del área de Género en sí misma, desde el Ejecutivo se asumió el ejercicio cotidiano de diseñar al presupuesto con perspectiva de género, lo que nos permite ver cómo atraviesa a cada una de las áreas del municipio”. En ese sentido, definió que la perspectiva de género implica, entre otros, tres ejes: “en tanto proceso, siempre tenés que replantearte y saber que las cosas llevan tiempo; otra es la interseccionalidad, es imposible pensar un problema sin tener en cuenta los distintos vectores que lo componen; y la otra variable es la desigualdad de género, que arroja a las mujeres y a las disidencias a condiciones de exclusiones y violencias por el solo hecho de ser identidades disidentes o mujeres”.