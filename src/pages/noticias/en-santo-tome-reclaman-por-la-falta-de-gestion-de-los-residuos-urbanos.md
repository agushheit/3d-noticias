---
category: La Ciudad
date: 2021-02-14T07:30:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/RESIDUOS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: En Santo Tomé reclaman por la falta de gestión de los residuos urbanos
title: En Santo Tomé reclaman por la falta de gestión de los residuos urbanos
entradilla: 'Advierten que las 65 toneladas diarias de basura se dirigen al relleno
  sanitario sin la separación correspondiente. '

---
Advierten que las 65 toneladas diarias de basura se dirigen al relleno sanitario sin la separación correspondiente. Piden que se reactive la planta de tratamiento de la ciudad, que está cerrada desde 2018.

Un grupo de vecinos de Santo Tomé integrado por profesionales, docentes, organizaciones sociales y espacios políticos se unieron para conformar una mesa territorial para el cuidado del ambiente. El motivo central que los llevó a esta creación de espacio pro-ambiental es que observan una preocupante gestión de los residuos sólidos urbanos en la ciudad.

La cooperativa de trabajo "Desafío EcoSantoto" fue la que solicitó ante el Concejo Municipal -en el espacio "Todos tenemos la palabra", décima banca- la creación de una mesa territorial para el cuidado del ambiente. En diálogo con El Litoral, Hernán Reynoso, miembro de la cooperativa, comentó por qué surgió esta mesa territorial: "Es un espacio de conversación con distintas organizaciones que tienen nuestro mismo objetivo: el cuidado del ambiente. Y nuestro objetivo puntual es tener una gestión integral de los residuos sólidos urbanos (RSU): por ahí hay que empezar".

El proyecto de esta mesa territorial busca que los residuos tengan valor agregado y evitar así "problemas irreversibles de contaminación y pasivos ambientales de los que nadie se hace cargo", indicaron en un comunicado. Para llegar a esa meta y que los residuos no se conviertan en "basura", proponen que desde la gestión municipal se insiste en la separación de los residuos en origen, para que sean recolectados y transportados en forma diferenciada.

"En Santo Tomé se aceptó que una empresa tome la planta de separación y disposición final, que empezó a trabajar en 2015 luego de una licitación, pero que ya está cerrada desde fines de 2018", mencionó Reynoso. Desde la mesa territorial destacaron que los residuos de la ciudad van a parar al relleno sanitario de Santa Fe: "La ciudad de Santo Tomé, desde el 2019, envía 65 toneladas por día aproximadas, que se generan en nuestra ciudad a un costo de $ 1.000.000 por día, un dato no desmentido por concejales. Gasto que pagamos vecinos santotomesinos. No podemos seguir así", advirtieron.

**Recuperación**

Actualmente, como sucede en la capital provincial, la falta de separación en los hogares genera que al relleno sanitario lleguen la mayoría de los deshechos en condiciones irrecuperables, por lo que se convierten en recursos enterrados y perdidos para siempre con el perjuicio directo de quienes viven de la recolección informal.

El grupo santotomesino sostuvo en su comunicado que se debe avanzar hacia "una gestión integral contemplando el tratamiento por separado de los residuos secos y húmedos. Para los orgánicos que componen el 50% de los residuos generados en nuestros hogares, se propone un sistema de compostaje domiciliario", Además, señalaron que "todas nuestras propuestas de trabajo están contempladas por la Ley Provincial N° 13.055 de 'Basura Cero' del 2009".

"Deseamos transformar la Planta de Tratamiento de RSU de Santo Tomé en un Centro Experimental. El mismo serviría para la realización de diversas pruebas piloto, al contar con el asesoramiento del Conicet, facultades y Centros de Investigación de las Universidades de nuestra región y otros organismos técnico- científicos", concluyeron.