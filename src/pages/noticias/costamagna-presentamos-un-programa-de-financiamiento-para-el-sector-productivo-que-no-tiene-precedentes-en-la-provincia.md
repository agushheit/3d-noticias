---
category: Estado Real
date: 2021-04-14T07:32:32-03:00
thumbnail: https://assets.3dnoticias.com.ar/costamagna.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: 'Costamagna: “Presentamos un Programa de financiamiento para el sector productivo
  que no tiene precedentes en la provincia”'
title: 'Costamagna: “Presentamos un Programa de financiamiento para el sector productivo
  que no tiene precedentes en la provincia”'
entradilla: El ministro de Producción, Ciencia y Tecnología destacó que se busca generar
  arraigo, empleo e inclusión, aumentar la productividad y transformar cereales en
  alimentos.

---
Esta semana el gobernador Omar Perotti firmó en Buenos Aires un convenio con el Banco Nación a partir del cual se instrumentará un histórico financiamiento a los sectores productivos de la provincia en el marco del Programa “Santa Fe de Pie”. El acuerdo prevé la implementación de un mecanismo de bonificación de la tasa de interés, a cargo de la provincia, en los créditos otorgados por el Banco y el monto total del fondo para destinar créditos será de 26.500 millones de pesos.

Al respecto, el ministro de Producción, Ciencia y Tecnología, Daniel Costamagna indicó: “Presentamos un Programa de Financiamiento que no tiene precedentes en la provincia de Santa Fe, no sólo por el monto de 26.500 millones de pesos, sino también por la particularidad del financiamiento, que tiene que ver con generación de arraigo y empleo, con la inclusión, con el aumento de la productividad, de la tecnología, con la fabricación de alimentos, con productos de calidad y con la consecuencia de alcanzar más y mejores mercados internacionales. Estos créditos están dirigidos a aquellos productores y empresas que transforman los cereales en alimentos. Hablamos de quienes normalmente no acceden a estas tasas, que le dan una vuelta de rosca a la agricultura, a la ganadería y a la industria. Ahí radica el espíritu del Programa. Por eso también agregamos una línea específica para equipamiento hidrovial, justamente porque estamos enfocados en desarrollar los caminos de la ruralidad, que refuerzan esa idea de arraigo y la mejora de la calidad de vida de la gente que trabaja en el campo y habita nuestros pueblos”.

Los sectores beneficiados serán el agropecuario, legumbres, miel, frutihortícola y acuícolas; lechero y arrocero; ganadero, porcino y avícola; Industria y Servicio; y obras hidroviales. En tanto que enumerando los criterios de elegibilidad para acceder a menores tasas, el ministro indicó: “Estamos ponderando aquellas iniciativas que tengan participación de las mujeres, que avancen en el asesoramiento técnico, que trabajen en cuestiones vinculadas al cuidado del medio ambiente, en la rotación de cultivos de cobertura, en la aplicación de fertilizantes, al manejo sanitario, a los controles lecheros, al bienestar animal, al análisis de suelo y agua, al manejo silvopastoril y al manejo agroecológico u orgánico”.

“En definitiva, estamos hablando de un Programa que atiende la diversidad productiva que tiene esta provincia, que tiene que ver con la frutihorticultura, con los proyectos forestales y de apicultura; con la producción caprina u ovina. A todas estas cuestiones se les suma el entramado industrial, todo lo que son las naves y galpones industriales, las cuestiones vinculadas a la incorporación de tecnología en los procesos industriales, sectores que también contarán con una línea crediticia específica”, agregó. 

Costamagna afirmó que “sin lugar a dudas, es una inversión importante de la Provincia, que está dirigida a aquellos que producen y los que generan empleo”. Al mismo tiempo, el ministro se refirió a las tasas con las bonificaciones, “que llegan hasta el 16% en pesos en inversiones y al 20% en lo que tiene que ver con capital de trabajo, con plazos acordes a los ciclos productivos”.

Asimismo, brindando más detalles de las líneas, informó: “En el caso de las inversiones con plazos de hasta cinco años con seis meses de gracia, en el caso del capital de trabajo son de 18 meses. Todos estos créditos fueron diseñados, articulados y estudiados en función a cada característica de cada sector. Por lo tanto, creo que estamos ante una herramienta sustancial en cuanto al desarrollo, crecimiento y a la transformación de la producción de alimentos”.