---
category: Estado Real
date: 2021-12-08T06:00:16-03:00
thumbnail: https://assets.3dnoticias.com.ar/falloperotti.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Perotti sobre el fallo de la corte:"Nuestro accionar fue constante, persistente
  y firme"
title: Perotti sobre el fallo de la corte:"Nuestro accionar fue constante, persistente
  y firme"
entradilla: Lo aseguró el gobernador de Santa Fe luego de conocerse el fallo de la
  Corte Suprema de la Nación en el que reconoce el reclamo de la provincia por la
  detracción de la coparticipación.

---
El gobernador de la provincia, Omar Perotti, junto al ministro de Economía, Walter Agosto, y el Fiscal de Estado, Rubén Weder, brindaron este martes detalles del fallo de la Corte Suprema de Justicia de la Nación a favor de la provincia de Santa Fe en su reclamo de coparticipación contra el Estado nacional.

En referencia a esto Perotti afirmó: "En el día de hoy la Corte Suprema de Justicia de la nación ha definido la forma de ajuste a la deuda que la Nación tiene con nuestra provincia según el fallo del 2015 por las detracciones de recursos coparticipables, estableciendo para el ajuste la tasa promedio pasiva del Banco Central de la República Argentina, tomando la misma base que en el fallo que había tenido con la provincia de San Luis, descartando planteos de la nación con respecto a la aplicación de una tasa promedio que superaba el 6% y también descartando reclamos de la nación para que sean descontados fondos de Anses transferidos a la provincia".

"Creemos de gran valía resaltar la validez de un proceso institucional de 14 años, desde el esquema inicial donde después de 2001 se establece en un pacto fiscal la instancia de extracción de recursos a las provincias. Fue en el 2007 cuando el ex gobernador Jorge Obeid presentó un formal reclamo ante el poder ejecutivo nacional pidiendo la detracción, el cese de esos recursos de coparticipación, paso que le permite en 2009 al ex gobernador Hermes Binner realizar la presentación judicial formal ante la Corte Suprema de Justicia de la nación", continuó el gobernador.

Más adelante, Perotti afirmó que desde el comienzo de las gestiones, tanto nacional como provincial, se estuvo trabajando en esta deuda que tiene la nación con la provincia. "Desde el inicio de nuestras gestiones, comenzamos con dos frentes de trabajo en las gestiones ante el Poder Ejecutivo. Nosotros habíamos trabajado ya con el presidente Alberto Fernández en la campaña con uno de los compromisos a asumir por el gobierno nacional que era en nuestro período de gobierno terminar de definir esta prolongada situación de reclamo de la provincia frente a un fallo".

Y continuó: "Independientemente de las distintas gestiones que fuimos encarando con el gobierno nacional no dejamos de realizar los reclamos y presentaciones ante la Corte Suprema de Justicia de la Nación, pandemia de por medio, en los dos ámbitos de gestiones; y también con las interrupciones de plazos que esto tenía en el funcionamiento de las acciones judiciales".

Y en referencia a ello, el titular de la Casa Gris remarcó que “desde julio en adelante, que se restablecieron, hicimos seis presentaciones en ese reclamo ante la Corte Suprema de Justicia de la Nación. Me parece que es, no solamente un día importante, porque se define cómo se va a ajustar esa deuda; sino también porque hay una instancia de continuidades en los reclamos de los distintos períodos en la defensa de los intereses de todos los santafesinos".

"Nuestro accionar y nuestras motivaciones políticas fueron constantes, persistentes y firmes, nos gusta trabajar de esta manera, con firmeza, con persistencia en la defensa de los intereses para conseguir este logro. Esta es una demostración de que cuando los santafesinos trabajamos juntos y tenemos un objetivo común, los intereses de todos y todas están resguardados", finalizó el gobernador.

Por último, el ministro de Economía, Walter Agosto, destacó la importancia de que “ahora ya tenemos un índice cierto de actualización, que es la tasa pasiva del Banco Central de la República Argentina; y de esa manera, la provincia en los próximos días hará sus cálculos para hacer la petición formal al Ejecutivo nacional y empezar la etapa del acuerdo de pago”.