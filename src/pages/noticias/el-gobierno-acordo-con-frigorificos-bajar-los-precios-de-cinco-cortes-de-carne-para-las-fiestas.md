---
category: Agenda Ciudadana
date: 2021-12-14T06:15:39-03:00
thumbnail: https://assets.3dnoticias.com.ar/carneprecio.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Noticias Argentinas
resumen: El Gobierno acordó con frigoríficos bajar los precios de cinco cortes de
  carne para las Fiestas
title: El Gobierno acordó con frigoríficos bajar los precios de cinco cortes de carne
  para las Fiestas
entradilla: Los precios ofertados, según la Secretaría de Comercio, serán "sensiblemente
  inferiores" al valor actual del mercado.

---
Tras reuniones con los principales actores de la industria cárnica y las grandes cadenas de supermercados del país, la Secretaría de Comercio Interior de la Nación cerró un acuerdo para aumentar la disponibilidad de cortes de carne durante los días previos a las fiestas de fin de año.

Los precios ofertados, según el Gobierno, serán "sensiblemente inferiores" al valor actual del mercado.

El Gobierno dijo que el acuerdo se consolida como una iniciativa más amplia y con mayor cobertura que el alcanzado el año pasado (cuando solamente se aplicó en Navidad), ya que contempla proveer a los comercios tanto en Navidad como en Año Nuevo, para los cortes que más se consumen en estas fechas.

De esta manera, los cortes parrilleros ofrecidos son el asado a $549, matambre a $599, vacío a $599, tapa de asado a $499 y falda a $399 por kilo.

Las cinco opciones se podrán conseguir en los puntos de venta adheridos al acuerdo en todo el país, entre los que se encuentran las carnicerías de ABC y los comercios nucleados en la Asociación de Supermercados Unidos (ASU), la Cámara Argentina de Supermercados (CAS) y la Federación Argentina de Supermercados y Autoservicios (FASA).

“Vamos a poner mucho énfasis en este acuerdo para garantizar que la gente pueda acceder a fin de año a estos cortes con las góndolas abastecidas y con los precios cumplidos. Queremos que las argentinas y los argentinos puedan tener un consumo sin problemas. Los cortes elegidos para este acuerdo son los más requeridos en esta época y queremos que todos y todas tengan acceso sin inconvenientes”, afirmó el secretario de Comercio Interior de la Nación, Roberto Feletti.

La oferta de estos cinco cortes parrilleros tan representativos del consumo de los hogares argentinos en esta época del año, estarán disponibles para la compra los días 22, 23 y 24 en la semana de Navidad y 29, 30 y 31 en la de Año Nuevo.

Durante esas fechas, la Secretaría de Comercio Interior hará un seguimiento al cumplimiento del acuerdo para garantizar el correcto abastecimiento y precios, en el marco de las 20 mil toneladas que estableció el Ministerio de Agricultura, Pesca y Ganadería de la Nación para las fechas mencionadas.

El acuerdo comenzó a delinearse la semana pasada, a partir de los intercambios que mantuvo el secretario de Comercio Interior con los representantes del Consorcio ABC. Los detalles del mismo se completaron hoy, durante los encuentros con las autoridades de la Unión de la Industria Cárnica Argentina (UNICA) y la Federación de Industrias Frigoríficas Regionales Argentinas (FIFRA), así como también con las cámaras de supermercados.

En las reuniones también participó el subsecretario de Políticas para el Mercado Interno, Antonio Mezmezian.