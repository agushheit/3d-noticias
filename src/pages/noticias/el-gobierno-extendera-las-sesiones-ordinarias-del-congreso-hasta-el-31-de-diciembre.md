---
category: Agenda Ciudadana
date: 2021-11-17T06:00:41-03:00
thumbnail: https://assets.3dnoticias.com.ar/CONGRESO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: El Gobierno extenderá las sesiones ordinarias del Congreso hasta el 31 de
  diciembre
title: El Gobierno extenderá las sesiones ordinarias del Congreso hasta el 31 de diciembre
entradilla: 'Esta semana se avanzará con la firma de una serie de dictámenes, pero
  no quedará tiempo para avanzar con el Presupuesto y algunos otros proyectos en los
  plazos ordinarios. El decreto se publicará en las próximas horas.

'

---
El presidente Alberto Fernández extenderá las sesiones ordinarias del Congreso hasta el próximo 31 de diciembre, tal como anticipó NA. Además, firmará otra decreto con la prórroga de la Ley Indígena, que se vence el 23 de este mes y tiene dictamen de comisión, pero no alcanzará el tiempo para llevarlo al recinto.

Al tratarse de una prórroga de las ordinarias, el Congreso puede tratar un temario propio y sin limitaciones, ya que si fueran extraordinarias, el debate debe tener una agenda específica de proyectos.

Por otra parte, según supo NA de fuentes cercanas a la presidencia de la Cámara de Diputados, el Gobierno convocará a sesiones extraordinarias en enero y febrero para seguir tratando iniciativas durante los meses del verano.

El bloque de diputados nacionales del Frente de Todos le pidió la prórroga de sesiones ordinarias a la secretaria de Legal y Técnica, Vilma Ibarra, con el objetivo de seguir trabajando con la agenda del Congreso nacional, sin necesidad de circunscribirse a un temario.

Así lo reveló a NA la vicepresidenta de la bancada oficialista, Cecilia Moreau, quien destacó que esta semana se avanzará con la firma de dictámenes de una serie de proyectos pendientes, pero no habrá tiempo para avanzar con el Presupuesto 2022 en los plazos ordinarios, por lo que haría falta un decreto del Gobierno nacional que extienda el calendario legislativo.

"Nosotros le pedimos a Alberto (Fernández), a Vilma (Ibarra) concretamente, que prorrogue las ordinarias porque para nosotros es más fácil que andar ampliando las extraordinarias. Porque así podemos tomar el temario del mismo Congreso. Ahora para sacar dictámenes tenemos tiempo hasta el 20 (de noviembre) pero si prorrogamos las ordinarias vamos a tener más tiempo", explicó.

La conveniencia de ampliar las sesiones ordinarias en lugar de convocar a extraordinarias tiene que ver con que en el primer caso el Congreso nacional podrá definir qué temas dictaminar y llevar a sesión, mientras que en el segundo el tratamiento parlamentario quedaría acotado al temario que autorice el Poder Ejecutivo.

Esta semana la Cámara baja apretará el acelerador con una serie de reuniones de comisión para sacar dictamen de varios proyectos pendientes. "Vamos a dejar todo encaminado salvo Presupuesto que no vamos a llegar porque no se pude hacer de un día para el otro. Nos queda la ley de Hidrocarburos, ley de Humedales y algún otro que plantee la oposición", adelantó.

Entre los proyectos que van a pasar a firma se encuentran el de presupuestos mínimos de Protección ambiental para la gestión integral de los neumáticos fuera de uso, y la creación del Programa nacional "Pancitas Llenas", cuyo objeto será el financiamiento y abastecimiento de comedores y merenderos de la República Argentina.

También se tratará el proyecto de ley en revisión por el cual se crea el marco regulatorio para el desarrollo de la industria del Cannabis medicinal y el cáñamo industrial. Y se modificará la ley 27.118 de Agricultura Familiar para crear el Fondo Fiduciario Pública de Crédito para la Agricultura Familiar (CREPAF) (FONDO).

A su vez, se prorrogarán el plazo de la ley 26.160 hasta el 23 de noviembre de 2025 para la continuidad del Fondo Fiduciario de Reparación histórica para las Comunidades Indígenas asentadas en el territorio nacional.

"Yo creo que en enero vamos a estar trabajando algunos días", anticipó Moreau.