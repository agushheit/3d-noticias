---
category: La Ciudad
date: 2021-05-13T09:22:01-03:00
thumbnail: https://assets.3dnoticias.com.ar/jatón.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad de Santa Fe
resumen: " La Municipalidad busca alternativas junto a los gimnasios"
title: " La Municipalidad busca alternativas junto a los gimnasios"
entradilla: |2-

  El intendente y funcionarios municipales se reunieron con representantes del sector, con el objetivo de encontrar soluciones para que puedan continuar con la actividad.

---
Este miércoles, el intendente Emilio Jatón encabezó un encuentro con referentes de la Cámara de Gimnasios de Santa Fe, luego de que el Gobierno de la provincia habilitara por decreto la realización de actividades deportivas al aire libre, no así en espacios cerrados. En ese sentido, la Municipalidad de Santa Fe volvió a reunir a los referentes del sector a los efectos de buscar alternativas que les permitan funcionar en espacios abiertos, bajo el estricto cumplimiento de la normativa provincial y los protocolos correspondientes.

Al comienzo de la reunión, Jatón trazó un mapa de la situación sanitaria actual en la capital de la provincia, enumerando la cantidad de casos positivos de COVID registrados al día de hoy, la ocupación de camas en el sistema de salud y el número de habitantes que ya fueron vacunados. Posteriormente recordó lo establecido por el Gobierno provincial y sugirió comenzar, entre todos, a buscar soluciones que les permitan paliar la grave situación en la que el sector de los gimnasios se encuentra, al igual que muchos otros.

> “Sé que hemos tenido un año muy difícil. Pero empecemos a ver cómo podemos, entre todos, enfrentar esta situación”, dijo el intendente.

En ese sentido, destacó la importancia de “definir juntos en una mesa de trabajo, cómo tomar decisiones consensuadas que sean beneficiosas para ustedes, es decir, cómo pueden adaptar los negocios para que sigan desarrollando sus actividades”. Y recordó que esta metodología “la llevamos adelante con todos los sectores desde el comienzo de la pandemia, a los efectos de construir juntos las distintas medidas”.

Por su parte, el secretario de Producción y Desarrollo Económico, Matías Schmüth, estableció la importancia de “organizar la práctica de la actividad al aire libre, entendiendo que hoy es la posibilidad que tenemos y hay que aprovecharla”. Para eso sugirió “ser lo más ordenados posible, evaluando aquellos gimnasios que tengan patios o terrazas, y los que pueden hacer uso de algún espacio público cercano”.

Finalmente, el secretario de Gobierno del municipio, Federico Crisalle, confirmó que a partir de este jueves se recorrerán los gimnasios que soliciten una asistencia, para conversar con sus propietarios y evaluar los espacios al aire libre que pueden utilizarse. “Lo que queremos es ver la posibilidad de funcionamiento que tengan”, explicó.

Por su parte, desde la Cámara se comprometieron a entregar una lista de cada uno de los lugares habilitados en la ciudad, para estudiar cómo pueden continuar con sus actividades. Además, plantearon la importancia de la actividad física en la recuperación de las personas que padecen algún tipo de dolencia y requieren del ejercicio para mejorar su calidad de vida.