---
layout: Noticia con imagen
author: Por José Villagrán
resumen: Pre Viaje y Santa Fe Plus
category: Estado Real
title: PreViaje y Santa Fe Plus, dos alternativas que se complementan pensando
  en el turismo
entradilla: Uno es una iniciativa que lanza el gobierno nacional. El otro es una
  idea que ya empezó a implementar el gobierno de Santa Fe.
date: 2020-11-05T12:33:48.000Z
thumbnail: https://assets.3dnoticias.com.ar/turismo.jpg
---
Ambos programas buscan hacer repuntar a uno de los sectores más afectados por la pandemia del Coronavirus.

Se acerca el verano y con ello la obligación de pensar en el turismo en medio de la pandemia que ha desatado el Covid-19 y cómo está afectando a las economías regionales. Reconociendo que deja importantes ingresos a las provincias y a la Nación, el sector turístico ha sido el más golpeado en los últimos casi nueve meses que la pandemia ha llegado a nuestro país.

Así, los gobiernos tanto nacional como provinciales, van pensando en programas y protocolos que permitan hacer que la gente pueda recorrer los puntos turísticos con promociones y de forma accesible en medio de una crisis económica y sin olvidar que el virus sigue circulando.

**PreViaje, la apuesta del gobierno nacional**

![](https://assets.3dnoticias.com.ar/turismo1.jpg)

El pasado 8 de octubre, el ministro de Turismo de la Nación Matias Lammens dio comienzo a la vigencia del programa PreViaje 2021 como parte de la ley de reactivación para el sector. La iniciativa busca que ante cualquier consumo turístico para el próximo año que se realice en este a 2020, el Estado reintegra el 50 por ciento en una tarjeta o billetera virtual, pudiéndose utilizar el monto en ese mismo viaje o en otro.

Como requisito, para obtener la devolución, los consumos deben ser por un mínimo de 10 mil pesos (sumados con varias facturas) y el tope máximo de devolución es 100 mil pesos.

La medida ya ha experimentado una buena recepción. En los primeros 15 dìas, se inscribieron más de 10 mil prestadores turísticos y se alcanzaron los 250 millones de pesos en facturación de ventas, según datos brindados por el propio ministerio. El 60 por ciento de los consumos se realizaron en agencias de viaje y los destinos con mayor demanda son Bariloche, Ushuaia, Iguazú, El Calafate y la Costa Atlántica.

Es por este motivo que desde la cartera conducida por el ex presidente de San Lorenzo de Almagro se decidió extender la fecha límite para adherirse al programa. En un primer momento el plazo para comprar paquetes para este verano vencía el 31 de octubre, pero a pedido de las agencias, se tomó la decisión de ampliarla hasta el 15 de noviembre.

Quienes decidan hacerse del beneficio del reintegro deben asesorarse en [www.previaje.com.ar](http://www.previaje.com.ar/) no solo de los destinos y los precios sino además de los protocolos para las distintas actividades.

**El gobierno santafesino lanzò Santa Fe Plus**

Siguiendo la misma línea del ejecutivo nacional, el gobierno de la provincia de Santa Fe presentó el esquema de beneficios Santa Fe Plus con el objetivo de posicionar los destinos turísticos santafesinos en complemento al plan Nacional PreViaje.

Lo que se ofrece desde el sector turístico provincial es articular acuerdos que posibiliten poner a disposición promociones para atraer visitantes de cara a la próxima temporada veraniega que se acerca.

Así, por ejemplo, se ofrecen paquetes de cuatro noches de alojamiento pagando tres, como así también en cabañas con paquetes de siete noches abonando seis. Además, contempla descuentos en gastronomía, atracciones, comercios y paseos de compras.

![](https://assets.3dnoticias.com.ar/turismo2.jpg)

“Queremos, en primera instancia, que todos los santafesinos conozcan la provincia y, al mismo tiempo, atraer visitantes”, dijo el ministro de Producción de la provincia Daniel Costamagna durante la presentación del programa. “Pretendemos que, en enero, febrero y marzo, la provincia sea un destino deseado por todo el país” expresó el secretario de Turismo, Alejandro Grandinetti.

Desde la provincia no piensan en quedarse con esta sola iniciativa y adelantaron que en los próximos días van a estar manteniendo reuniones con intendentes y presidentes comunales para ponerlos al tanto. Al mismo tiempo, aseguraron que mantendrán encuentros con el sector privado de la actividad ampliar la capacidad de la propuesta.

Para mayor información, el gobierno provincial habilitó la página [www.santafe.tur.ar/SantaFePlus](http://www.santafe.tur.ar/SantaFePlus) para consultar los prestadores adheridos. Y tener mayores precisiones al respecto.

Como adjunto incluiría el enlace de los requisitos y restricciones para viajar por provincias que exige el gobierno nacional <https://www.argentina.gob.ar/transporte/covid-19/requisitos-ingreso-por-provincia>