---
category: Deportes
date: 2021-05-24T06:00:32-03:00
thumbnail: https://assets.3dnoticias.com.ar/BASKET.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: CABB
resumen: Argentina ya conoce a sus rivales de la AmeriCup
title: Argentina ya conoce a sus rivales de la AmeriCup
entradilla: 'Se sortearon los grupos de torneo que se realizará en Puerto Rico desde
  el 11 de junio. Argentina irá al Grupo B, junto con Estados Unidos, Puerto Rico,
  Dominicana y Venezuela. '

---
Este sábado por la tarde se realizó el esperado sorteo de la FIBA Women's AmeriCup 2021 que se disputará del 11 al 19 de junio en San Juan, Puerto Rico. Con una enorme expectativa en la previa y atravesando una nueva etapa de su preparación, Argentina ya conoce a sus rivales del torneo y ahora tiene un horizonte mucho más claro. La Selección integrará el Grupo B con Estados Unidos, Puerto Rico, República Dominicana y Venezuela. Los integrantes del otro grupo, el A, serán Canadá, Brasil, Colombia, Islas Vírgenes y El Salvador.

Una zona difícil, claramente, pero Argentina tiene el potencial, la experiencia y está sumando el rodaje para apuntar a quedarse con el segundo puesto, que le permita un mejor cruce de cuartos. La AmeriCup contará con un total de 10 equipos. Al no disputarse el Sudamericano que estaba pronosticado para mayo, Argentina se clasificó directamente a este certamen a través de su colocación en el ranking mundial FIBA (28°), así como también lo hicieron Brasil, Colombia y Venezuela. Se suman a los cuatro clasificados del Centrobasket jugado en marzo pasado (Puerto Rico, Islas Vírgenes, República Dominicana y El Salvador), y las dos grandes potencias del continente Estados Unidos (campeón defensor) y Canadá.

Con cinco equipos cada grupo y después de enfrentarse todos contra todos en una sola ronda, los equipos ubicados del 1º al 4º lugar en cada zona avanzarán a cuartos de final. Los ganadores de esa fase, quienes accedan a semifinales y además de continuar en la lucha por el título, también se garantizarán un cupo para competir en uno de los cuatro torneos Clasificatorios Globales rumbo al Mundial 2022 a jugarse en Australia.