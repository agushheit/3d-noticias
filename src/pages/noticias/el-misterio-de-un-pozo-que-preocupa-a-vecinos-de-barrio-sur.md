---
category: La Ciudad
date: 2022-01-17T06:15:44-03:00
thumbnail: https://assets.3dnoticias.com.ar/crater.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: El misterio de un pozo que preocupa a vecinos de Barrio Sur
title: El misterio de un pozo que preocupa a vecinos de Barrio Sur
entradilla: Se encuentra en el medio de la calzada pavimentada sobre calle 3 de Febrero
  al 2900 y se desconoce las causas que lo generaron.

---
La presencia de imperfecciones en el pavimento de las calles de la ciudad de Santa Fe resulta ya una costumbre y hasta quizás parte del paisaje urbano. Los ciudadanos y conductores de la capital provincial deben lidiar a diario con estas complicaciones en el tránsito.

Si bien se vuelve algo cotidiano, los vecinos de Barrio Sur mantienen un firme reclamo ante la aparición en los últimos días de un no tan extenso, pero sí profundo pozo sobre la calzada.

El mismo se encuentra sobre calle 3 de Febrero al 2900, entre 1° de Mayo y 4 de Enero. Allí, un extraño hueco se originó desde el día 5 enero aproximadamente, según relató uno de los denunciantes.

Dicho “cráter” genera grandes complicaciones a la hora de circular, principalmente por su ubicación sobre el asfalto y por poseer una considerable profundidad. De manera improvisada, vecinos montaron una especie de señal de advertencia con una parte de la rama de un árbol.

Aún sin respuestas concretas, la incertidumbre y el misterio en torno a los motivos que provocaron la creación de este pozo aumentan en los ciudadanos residentes de esta zona de la ciudad.