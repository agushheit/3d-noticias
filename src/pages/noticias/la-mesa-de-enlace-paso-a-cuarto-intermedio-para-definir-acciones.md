---
category: El Campo
date: 2021-01-05T11:39:58Z
thumbnail: https://assets.3dnoticias.com.ar/050121-mesa-de-enlace.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: El Litoral'
resumen: La Mesa de Enlace pasó a cuarto intermedio para definir acciones
title: La Mesa de Enlace pasó a cuarto intermedio para definir acciones
entradilla: Las entidades que conforman la Comisión de Enlace decidieron, durante
  la tarde del lunes, pasar a un cuarto intermedio hasta este martes 5, para terminar
  de definir las acciones.

---
«Hemos decidido un cuarto intermedio de esta Comisión hasta mañana martes a las 17 horas, con el propósito de realizar las consultas internas en el seno de cada institución con el objeto de tomar una definición sobre las acciones a seguir», señalaron esta tarde en un comunicado.

La Comisión de Enlace de Entidades Agropecuarias -integrada por la Sociedad Rural Argentina, Confederaciones Rurales Argentinas, Coninagro y Federación Agraria- manifestó que en la reunión de hoy «evaluó la medida que comprende el cierre del registro de exportación de maíz dispuesto por el Gobierno nacional, anuncio que se suma a un conjunto de políticas perjudiciales para el campo, implementadas a lo largo del año que acaba de concluir».

**Desde las entidades agropecuarias dejaron trascender, en las últimas jornadas, la posibilidad de concretar protestas a la vera de la ruta, para manifestar su oposición a la medida del Gobierno nacional, entre otras acciones.**

El miércoles pasado, el Ministerio de Agricultura, Ganadería y Pesca suspendió temporalmente el registro de Declaraciones Juradas de Venta al Exterior (DJVE) de maíz con fecha de embarque anterior al 1 de marzo próximo, con el objetivo de asegurar el abastecimiento interno del cereal hasta que ingrese la cosecha de la nueva campaña.

«Esta decisión se basa en la necesidad de asegurar el abastecimiento del grano para los sectores que lo utilizan como materia prima en sus procesos de transformación, básicamente la producción de proteína animal como carne de cerdo, pollo, huevos, leche y _feedlot_, donde el cereal representa un componente significativo de sus costos de producción», señaló la cartera agropecuaria al realizar el anuncio.