---
layout: Noticia con imagen
author: .
resumen: "Covid-19: Registro Civil"
category: La Ciudad
title: "Covid-19: El Registro Civil restringe la atención al público en su sede
  central en la ciudad de Santa Fe"
entradilla: Ante casos positivos de coronavirus, hasta el 30 de noviembre sólo
  se atenderán urgencias y turnos ya otorgados.
date: 2020-11-18T13:16:13.213Z
thumbnail: https://assets.3dnoticias.com.ar/registro-civil.jpg
---
El Registro Civil de la provincia **restringe hasta el 30 de noviembre la atención al público en su sede de calle San Luis 2950 de la ciudad de Santa Fe**, ante casos positivos de Covid-19, como de contactos estrechos, que requieren aislamiento.

No obstante, **todos los turnos otorgados por los medios oficiales serán atendidos** como así **también las urgencias** debidamente acreditadas.

Esperamos sepan disculpar las molestias ocasionadas en este contexto de pandemia donde solicitamos sigan tomando todos los recaudos para evitar mayores contagios. Nos cuidamos entre todos.