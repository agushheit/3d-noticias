---
category: Estado Real
date: 2021-10-23T06:00:44-03:00
thumbnail: https://assets.3dnoticias.com.ar/aportescalchines.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La provincia entregó aportes económicos a los productores hortícolas de Santa
  Rosa  de Calchines
title: La provincia entregó aportes económicos a los productores hortícolas de Santa
  Rosa  de Calchines
entradilla: Se otorgaron recursos por un monto superior a 31 millones de pesos a 25
  productores afectados por la plaga “chicharrita del maíz”, que tuvieron una pérdida
  del 100% de sus rendimientos.

---
El Ministerio de Producción, Ciencia y Tecnología, a través de las Secretarías de Agroalimentos y de Desarrollo Territorial y Arraigo, entregó este jueves aportes no reintegrables por un monto superior a 31 millones de pesos a 25 productores hortícolas de la localidad de Santa Rosa de Calchines, que se vieron afectados por la plaga “chicharrita del maíz'', lo que ocasionó una pérdida total de los cultivos.

De la actividad participaron el ministro de Producción, Ciencia y Tecnología, Daniel Costamagna; el senador Nacional Roberto Mirabella; el senador por el departamento Garay, Ricardo Kaufmann; el secretario de Desarrollo Territorial y Arraigo, Fabricio Medina; la subsecretaria de Coordinación Agroalimentaria, María Eugenia Carrizo; la presidenta comunal, Natalia Galeano; y productores hortícola de la zona.

Durante el evento, Costamagna señaló que “nuestra impronta de trabajo tiene que ver con la importancia de llegar a los mercados y de trabajar en toda la cadena, sin lugar a dudas el gran desafío está en buscar la forma de tener asegurada la producción desde el punto de vista de la incorporación de los eventos que hoy la tecnología permite, de trabajar con antelación y poder adelantarse a los hechos”.

Asimismo, el ministro adelantó que “la semana que viene iniciaremos el trabajo con los semilleros y además vamos a generar un esquema de cobertura de riesgos, para que desde el gobierno de la provincia podamos hacernos cargo; entonces teniendo cobertura climática, cobertura de tecnología de eventos y buscando esquemas de financiamiento para aquellos que decidan crecer o incorporar tecnología, podemos acompañar a esta zona para que siga creciendo”.

Por su parte, haciendo referencia a la asistencia técnica que se brindará, Mirabella destacó que “es fundamental para que no vuelvan a suceder este tipo de problemáticas, como en este caso que por la plaga se perdió la producción de maíz. El Estado debe estar presente cuando suceden estas inclemencias; a esta zona hay que apuntalarla con obras de infraestructura, como los caminos de la ruralidad, con rutas o la repavimentación, pero también con asistencia técnica para que los productores no estén solos y fundamentalmente con asistencia financiera. Es fundamental que trabajemos en equipo, con el Ministerio, los productores, el sector privado, otros organismos del Estado como el INTA y con las comunas, ya que hay resultados que solamente se alcanzan en equipo”.

A su turno, Kaufmann indicó que “es una ocasión festiva el hecho de tener el reconocimiento por parte del gobierno a los productores de nuestra localidad, después de una pérdida tan grande; por eso estamos muy satisfechos de tener presentes a los funcionarios de la provincia, que demuestran un verdadero interés con hechos y no solo con palabras”.

Por su parte, Carrizo aseguró que “cuando tomamos conocimiento de esta problemática, comenzamos a establecer una herramienta de apoyo que se concreta el día de hoy con la entrega de aportes no reintegrables a 25 productores de choclo que han tenido un daño total de toda su producción durante el primer semestre del año. Si bien nos resulta imposible poder retribuir a los productores la totalidad de la pérdida económica, el Estado se hace presente ayudando y demostrándoles la importancia que tiene el desarrollo económico para estas comunidades, la generación de trabajo y todo lo que la actividad genera también en el entorno urbano”.

**VOCES LOCALES**  
En tanto, la presidenta comunal local remarcó: “La relevancia que se le da desde la cartera a nuestros productores que no solo abastecen de frutas y verduras a la provincia de Santa Fe, sino a las grandes ciudades del país, por lo que es aún más importante poder acompañar ese proceso y compensar en caso de pérdidas”.

Para concluir, el productor hortícola Reinaldo Saucedo destacó que “toda colaboración que sirva para volver a intentar, suma; los profesionales se van a encargar de describir bien qué fue lo que pasó para asesorarnos, porque tenemos que tener cuidado. Además, el nuevo intento se hará en base a la provisión de semillas, por lo que volveremos a intentarlo con mucho esfuerzo, con el apoyo del Ministerio y el asesoramiento de profesionales para revertir la situación”.

**ASIGNACIÓN DE RECURSOS**  
Los recursos fueron asignados para todos aquellos trabajadores rurales que se vieron afectados por la plaga “chicharrita del maíz'' (Dalbulus maidis), que ocasionó una pérdida del 100% de los rendimientos, por lo que resultó fundamental compensar parte de las pérdidas económicas registradas.

Para ello, las mencionadas secretarías realizaron un relevamiento de los productores afectados considerando todos aquellos inscriptos en el Registro Único de Producciones Primarias (RUPP). Posteriormente, se realizó una constatación de los daños en cada predio y se segmentó la asignación de fondos según la superficie de maíz sembrada y el porcentaje de daño originado. Además, para la asignación de fondos a cada productor, se contempló un coeficiente de afectación considerando especialmente aquellos productores más vulnerables.