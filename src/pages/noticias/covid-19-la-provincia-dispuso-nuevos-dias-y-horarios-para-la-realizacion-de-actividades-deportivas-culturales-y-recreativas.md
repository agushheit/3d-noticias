---
category: Estado Real
date: 2021-11-10T06:00:36-03:00
thumbnail: https://d7kqr73uznmf6.cloudfront.net/2020-10-07.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Covid 19: La provincia dispuso nuevos días y horarios para la realización
  de actividades deportivas, culturales y recreativas.'
title: 'Covid 19: La provincia dispuso nuevos días y horarios para la realización
  de actividades deportivas, culturales y recreativas.'
entradilla: La resolución rige a partir de este 9 de noviembre, e incluye a salones
  de eventos, bingos y casinos.

---
En el marco de las medidas de convivencia en pandemia por Covid-19, el gobierno de la provincia de Santa Fe, a través del Ministerio de Gestión Publica, dictó la resolución Nº 542, mediante la cuál se amplían los días y horarios para la realización de actividades deportivas, culturales y recreativas. La resolución rige a partir de hoy, 9 de noviembre.

Al respecto, el ministro de Trabajo, Empleo y Seguridad Social, Juan Manuel Pusineri, detalló que la medida se adopta “luego de las reuniones mantenidas con distintos sectores económicos y sociales, que hacen al quehacer de la provincia de Santa Fe; y teniendo en cuenta la evaluación del cuadro sanitario, a partir de la importancia, intensidad y extensión de la campaña de vacunación”.

Por este motivo, “se ha decidido avanzar en algunas flexibilizaciones horarias y de aforos para distintos sectores: los salones de eventos, las actividades deportivas, recreativas, de entrenamientos, y de gimnasios; y de bingos y casinos”, detalló el ministro.

Por último, Pusineri destacó que “este es un nuevo esquema de trabajo que tiene que ver con ir transitando hacia una etapa de normalidad para empezar a dejar atrás la pandemia, siempre preservando todos los cuidados”.

**DÍAS Y HORARIOS**  
>> Actividad deportiva en modalidad entrenamiento y recreativa de deportes individuales o grupales; y gimnasios, natatorios y establecimientos afines: todos los días de la semana, entre las 6 y las 22 horas.

>> Actividad de casinos y bingos, cumplimentando las reglas generales de conducta y de prevención y los protocolos oportunamente aprobados, los días jueves, viernes, sábados y vísperas de feriados, entre las 10 y las 3 horas del día siguiente, y el resto de los días de la semana entre las 10 y las 2 horas del día siguiente.

>> Actividad de los salones de eventos, fiestas y similares, para la realización de eventos sociales: los días jueves, viernes, sábados y vísperas de feriados, entre las 8 y las 3 horas del día siguiente, y el resto de los días de la semana, entre las 8 y las 2 horas del día siguiente. Sin exceder la ocupación del setenta por ciento (70%) de la superficie disponible.

En este caso, las autoridades municipales y comunales podrán autorizar una ocupación superior de la superficie, en la medida que se verifiquen las condiciones de la infraestructura, para asegurar la adecuada ventilación de los espacios.