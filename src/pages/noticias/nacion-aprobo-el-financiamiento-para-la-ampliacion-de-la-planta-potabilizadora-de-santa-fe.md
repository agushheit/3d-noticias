---
category: Agenda Ciudadana
date: 2021-08-13T06:15:07-03:00
thumbnail: https://assets.3dnoticias.com.ar/PLANTA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Nación aprobó el financiamiento para la ampliación de la Planta Potabilizadora
  de Santa Fe
title: Nación aprobó el financiamiento para la ampliación de la Planta Potabilizadora
  de Santa Fe
entradilla: El proyecto prevé incrementar el 75% de la capacidad de generar agua potable.

---
El presidente de Aguas Santafesinas, Hugo Morzan, anunció este jueves que “hemos recibido la aprobación técnica para el financiamiento por parte del Estado nacional a los dos proyectos que hemos presentado desde la provincia de Santa Fe para ampliar las plantas potabilizadoras de la ciudad de Santa Fe y del Gran Rosario”.

Morzan detalló que el Ente Nacional de Obras Hídricas de Saneamiento (Enohsa) dio de esta forma aprobación a los proyectos técnicos elaborados desde ASSA, previendo que la ampliación de la planta potabilizadora de Santa Fe demandará una inversión del Estado nacional de 4.420 millones de pesos; en tanto similar obra en la planta potabilizadora del Gran Rosario requerirá 2.040 millones de pesos.

“Venimos trabajando en este proyecto desde que el gobernador Omar Perotti y el presidente Alberto Fernández pocos meses atrás acordaron encarar rápidamente estos emprendimientos que traerán mejora del servicio y progreso a los conglomerados urbanos más grandes de la provincia”, agregó el titular de Aguas, para resaltar que “continuaremos trabajando en el proceso licitatorio y de esta forma poder concretar estos proyectos que ya tienen armada su estructura para ser licitados y concretar obras que tanto hace esperamos para estas ciudades”.

En ese sentido, recordó que “son obras de suma importancia, todos sabemos que en la ciudad de Santa Fe estamos acotados para expandir las redes porque no tenemos la cantidad suficiente de agua. Con esta ampliación incrementaremos un 70 % la producción de agua con una proyección de demanda cubierta 30 años hacia adelante, para poder brindar este servicio esencial a barrios que hoy no lo tienen. En cuanto a Rosario, “nos permitirá mejorar la provisión en todo el oeste, suroeste y sur de la ciudad, además de abrir paso a nuevas urbanizaciones y llegar con agua potabilizada de río a localidades que hoy necesitan más agua para crecer o se abastecen con perforaciones”.

**PLANTA SANTA FE**

El proyecto de ampliación de la planta potabilizadora de la ciudad de Santa Fe tiene como objetivo aumentar la producción de agua potable, atendiendo al crecimiento demográfico y las necesidades de la población.

Se prevé la construcción de una serie de obras que incluyen la ejecución de una nueva toma de captación de agua cruda; nuevos módulos de tratamiento, diferentes reformas y mejoras operativas en sectores de producción y nuevos acueductos.

La ampliación de la planta posibilitará una mayor producción de agua para consumo humano con una proyección de 25 años.

Hoy la planta en funcionamiento operada por Aguas Santafesinas aporta a la red de distribución 8.250 metros cúbicos por hora (m3/hora).

Con esta nueva ampliación habrá disponibles 6.250 m3/hora más. De esta manera, en total, los santafesinos tendrán 14.000 m3/hora, que permitirá garantizar agua potable teniendo en cuenta el crecimiento vegetativo estimado en tres décadas.

También será posible expandir el servicio a zonas que actualmente no cuentan con redes y optimizarlo en los barrios que sufren déficits particularmente en época estival.

**GRAN ROSARIO**

Por otro lado, se duplicará la capacidad de producción de la planta potabilizadora del Acueducto Gran Rosario ubicada en Granadero Baigorria, para atender la demanda a futuro con la expansión del área de servicio de dicho sistema.

En una segunda instancia de este emprendimiento se prevé el tendido de un nuevo conducto de 12 kilómetros paralelo a Circunvalación que hará posible optimizar el suministro en el oeste y suroeste de Rosario y tendrá ramales a localidades del Gran Rosario como Ybarlucea, Funes, Pérez y Villa Gobernador Gálvez.

Y se construirá una cisterna de almacenamiento y rebombeo de 10 millones de litros de capacidad en Provincias Unidas y Perón.

Se trata de una obra gigantesca que permitirá mejorar o incorporar al servicio a 560.000 vecinos y vecinas que actualmente pueden estar afectadas por deficiencias principalmente durante la temporada estival.