---
category: La Ciudad
date: 2022-01-10T06:00:33-03:00
thumbnail: https://assets.3dnoticias.com.ar/MOTOCHOQUE.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'Peligro sobre dos ruedas: accidentes y principales infracciones en Santa
  Fe'
title: 'Peligro sobre dos ruedas: accidentes y principales infracciones en Santa Fe'
entradilla: " Durante el año 2021, en la ciudad de Santa Fe, de acuerdo a la base
  de datos del Hospital Cullen hubo 2.655 consultas por motociclistas heridos en siniestros."

---
Según información suministrada por la Secretaría de Control y Convivencia Ciudadana de la Municipalidad de Santa Fe, en el año 2021 se implementaron 1.222 controles de tránsito en diferentes puntos de la ciudad. A partir de estos operativos, se retuvieron 5.645 motovehículos, de los cuales el 44% no tenían chapa patente. Sobre el total de retenciones, el 19% fue por no usar casco. Además, se registraron 27 casos positivos de alcoholemia en motociclistas.

 Durante el segundo semestre del año, se incrementó en un 22% (de 550 a 672) la cantidad de operativos respecto del primer semestre, generando un aumento en las retenciones de motos de un 113% (de 1.803 retenidas en el primer semestre se pasó a 3.842 en el segundo).

 "Muchos de los que hoy cometen delitos utilizan motos sin patentes como medio de traslado, por eso es que la decisión del intendente es hacer hincapié en el control y retención de la moto que circula sin patente colaborando con el Ministerio de Seguridad", aseguró Guillermo Álvarez, a cargo de la repartición.

 **Siniestralidad vial**

 "Además de colaborar con la actual problemática de la inseguridad, la tarea de la Secretaría de Control y Convivencia Ciudadana tiene relación directa con la seguridad vial de la ciudad. Recopilando información desde distintos estamentos pudimos obtener datos que muestran una positiva mejoría en este sentido respecto a años anteriores", afirmó Álvarez y aportó los siguientes datos.

 Durante el año 2021, en la ciudad de Santa Fe, de acuerdo a la base de datos del Hospital Cullen hubo 2.655 consultas por motociclistas heridos en siniestros, un 20% menos que en 2019, el cual fue un año de absoluta normalidad sanitaria, con muchísima mayor circulación que durante la pandemia.

 En este sentido, de acuerdo a la información proporcionada por los registros que lleva Cobem, de la atención de accidentes donde ellos intervinieron, durante 2021 se atendieron 179 siniestros que involucraron a motovehículos, representando este número el 58% del total de siniestros registrados y atendidos. Comparativamente con años anteriores representan un 18% menos de accidentes de tránsito con participación de motovehículos respecto al 2020 (año con muchísima menos circulación vehicular por la pandemia).

 Se observa también que Cobem atendió en 2021 un 53% menos de accidentes de motos respecto a lo atendido durante el año 2019, dos años con actividad normal, sin tantas restricciones por la pandemia. Según las estadísticas oficiales del Observatorio de la Agencia Provincial de Seguridad Vial se registraron como datos preliminares 15 víctimas fatales de usuarios de motovehículos en el año 2021. Esta cifra representa una disminución del 21%, respecto de la cantidad de víctimas fatales en 2020 y de un 47% respecto de 2019.

 **Capacitación y concientización**

 "Por último, y más allá de la tarea específica de control en sí, mantuvimos y continuamos iniciativas de capacitación y concientización en materia de educación vial para los usuarios de motos, entendiendo que la educación y el asesoramiento son parte del camino a recorrer y profundizar para disminuir la accidentología vial", explicó el funcionario.

 Es por ello que la Oficina Móvil de Motos (OMM) se implementó en 33 puntos de la ciudad a lo largo del año, brindando asesoramiento y capacitación sobre trámites y medidas de seguridad al momento de conducir motocicletas.