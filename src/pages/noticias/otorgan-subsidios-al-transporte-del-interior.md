---
category: Agenda Ciudadana
date: 2022-06-27T08:38:54-03:00
thumbnail: https://assets.3dnoticias.com.ar/colectivos1.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: Otorgan subsidios al transporte del interior
title: Otorgan subsidios al transporte del interior
entradilla: Se publico hoy lunes en el boletín oficial la llegada de 16.050 millones
  para el transporte del interior.

---
Con la publicación de la Resolución 401/22 del Ministerio de Transporte de la Nacían, confirman la llegada de subsidio al transporte del interior, para los meses de julio, agosto y setiembre por 3.850 millones. Para octubre, el monto es de 4.500 millones. El total ronda los 16.050 millones de pesos. Compartimos el articulo uno de la resolucion 401/22 del Ministerio de Transporte.

ARTÍCULO 1°.- Establécese la distribución correspondiente al “Fondo de Compensación al Transporte Público de pasajeros por automotor urbano y suburbano del interior del país”, creado por el artículo 125 de la Ley N° 27.467 de Presupuesto de gastos y recursos de la Administración Nacional para el Ejercicio 2019, prorrogado por el artículo 72 de la Ley N° 27.591, prorrogada al Ejercicio 2022 por el Decreto N° 882 del 23 de diciembre de 2021 y el Decreto de Necesidad y Urgencia N° 331 del 16 de junio de 2022, por un monto mensual de hasta PESOS TRES MIL OCHOCIENTOS CINCUENTA MILLONES ($3.850.000.000) correspondiente a los meses de julio, agosto y septiembre, y por la suma de hasta PESOS CUATRO MIL QUINIENTOS MILLONES ($ 4.500.000.000) por el mes de octubre, todos del 2022, totalizando la suma de PESOS DIECISÉIS MIL CINCUENTA MILLONES ($ 16.050.000.000) con destino a los Servicios de Transporte Público de Pasajeros por Automotor Urbanos y Suburbanos del interior del país.