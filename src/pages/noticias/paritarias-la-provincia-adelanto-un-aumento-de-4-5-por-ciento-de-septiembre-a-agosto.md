---
category: Agenda Ciudadana
date: 2021-08-24T06:00:07-03:00
thumbnail: https://assets.3dnoticias.com.ar/ESTATALES.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Paritarias: la provincia adelantó un aumento de 4,5 por ciento de septiembre
  a agosto'
title: 'Paritarias: la provincia adelantó un aumento de 4,5 por ciento de septiembre
  a agosto'
entradilla: Será remunerativo y bonificable para todos los agentes de la administración
  pública provincial, jubilados y pensionados.

---
El ministro de economía Walter Agosto informó que el gobierno de la provincia dispuso que las remuneraciones del mes de agosto se liquiden con un incremento remunerativo y bonificable de 4,5%. “La paritaria vigente establecía un próximo aumento de 9% en setiembre, pero en el marco de las solicitudes realizadas por distintos gremios, entendimos razonable hacer efectivo dicho aumento en dos cuotas consecutivas de 4,5% en agosto y 4,5% en setiembre” señaló el ministro.

Hay que tener en cuenta que el gobierno provincial ya adelantó a junio un incremento de 8% que estaba previsto para julio, lo cual también permitió que dicho aumento se contabilizara para la liquidación del aguinaldo de los agentes estatales.

Consultado sobre la posibilidad de reabrir la paritaria, Agosto señaló “el acuerdo que suscribimos con las representaciones de los diferentes gremios en marzo pasado prevé muy claramente que a partir del mes de octubre las partes se reunirán a los efectos de monitorear la situación salarial en función de la evolución de la variable inflación. Con esto quiero decir que, gobierno y gremios, previmos con mucha anticipación este mecanismo para que cualquier desfasaje pueda ser analizado en el mes de octubre próximo y así lo vamos a hacer”.

En otro orden, consultado por lo anunciado por el gobierno nacional con relación a la paritaria nacional docente, Agosto indicó, no hay que confundir, lo que se planteó a nivel nacional son nuevos incrementos pero recién a partir de octubre. Como le mencionaba anteriormente, el acuerdo paritario vigente en la provincia también plantea la revisión en octubre.