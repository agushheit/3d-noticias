---
category: La Ciudad
date: 2021-03-28T06:15:54-03:00
thumbnail: https://assets.3dnoticias.com.ar/LAGNA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Lagna y Jatón se reunirán la próxima semana
title: Lagna y Jatón se reunirán la próxima semana
entradilla: 'Así lo confirmó el coordinador del Ministerio de Seguridad Facundo Bustos:
  "La impronta del nuevo ministro es tener una relación fluida y coordinar acciones
  con los municipios" describió.'

---
El Ministro de Seguridad de la Provincia Jorge Lagna y el Intendente de la ciudad de Santa Fe Emilio Jatón se reunirán la semana entrante, en el que será el primer encuentro público entre ambos.

El objetivo de la reunión será "coordinar nuevas acciones que permitan mejorar las condiciones de seguridad en los barrios más conflictivos y los sectores con mayor demanda", afirmó el coordinador del Ministerio provincial Facundo Bustos.

La confirmación de la reunión entre el Ministro y el Intendente se dio a conocer luego de un nuevo encuentro realizado este viernes de la Mesa de Coordinación Institucional entre Seguridad y la Municipalidad de Santa Fe. Según se informó, la reunión tuvo como objetivo "profundizar medidas en torno a los operativos de control y los dispositivos de seguridad en diversos puntos de la ciudad."

Además de Bustos estuvieron presentes en la reunión entre la Provincia y la Municipalidad, el Secretario de Política y Gestión de la información del Ministerio de Seguridad de la Provincia de Santa Fe, Jorge Fernández; la Secretaria de Control del Municipio Virginia Coudannes y autoridades policiales.