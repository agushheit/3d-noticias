---
category: Agenda Ciudadana
date: 2021-01-30T09:52:35.000+00:00
thumbnail: https://assets.3dnoticias.com.ar/alberto-fernandez.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Infobae
resumen: 'El Gobierno recomendará priorizar las clases presenciales '
title: El Gobierno les recomendará a las provincias priorizar las clases presenciales
  en el decreto con el que prorrogará el distanciamiento social
entradilla: EL DNU será publicado en las próximas horas en el Boletín Oficial. El
  DISPO durará hasta el 28 de febrero. El personal docente, no docente, alumnos y
  padres quedarán exceptuados de la prohibición de usar el transporte

---
Alberto Fernández terminó de definir en la tarde de hoy la nueva fase del Distanciamiento Social Preventivo y Obligatorio (DISPO), cuyo último decreto vence el próximo domingo. El Gobierno lo dará a conocer en las próximas horas a través del Boletín Oficial e incluirá **recomendaciones para concretar la presencialidad en las aulas que deberá instrumentar el Consejo Federal de Educación en cada distrito.**

El Presidente realizó el análisis de la situación con el jefe de Gabinete, Santiago Cafiero, y el ministro de Salud, Ginés González García, quienes coincidieron **en la necesidad de realizar una “extensión sin muchos cambios y por cuatro semanas“, según precisaron fuentes de la Casa Rosada. El DNU, que ya está escrito, asegura que se busca “priorizar el funcionamiento de los establecimientos educativos con modalidades presenciales”.**

En cuanto al tema de las clases, el texto del DNU agrega que “cada provincia y la Ciudad Autónoma de Buenos Aires deciden la efectiva reanudación y, eventualmente, su suspensión de acuerdo con la situación epidemiológica”.

Además, **establecerá que seguirán vigentes los dos parámetros de riesgo sanitario (razón e incidencia) que deberán seguir teniendo en cuenta las provincias para la restricción de la circulación nocturna.**

Por unas horas, el Presidente había previsto publicar la nueva medida por solo dos semanas, a la espera de la evolución del panorama epidemiológico, ya que el retraso de la llegada de las vacunas no permitirá cumplir con el plan de vacunación entre los docentes.

Pero como en el Gobierno están verificando que hay una **sostenida baja de contagios en el AMBA** y el resto del país, una respuesta que consideran acorde a los duros llamados de atención realizados a través de la comunicación oficial para extremar los cuidados, no creen que se necesite endurecer las medidas.

![](https://assets.3dnoticias.com.ar/presidencia-nacion.jpg)

###### El equipo con el que terminará de definir por la tarde la extensión de la DISPO (Presidencia de la Nación)

**Fernández no realizó en esta ocasión una conversación virtual con todos los gobernadores como lo hizo en otras oportunidades. “Se mantuvo en contacto permanente con varios de ellos, así que no lo consideró para nada necesario, aunque sí buscó** alguna información adicional, seguramente se comunicará puntualmente con alguno, pero algo general no se cree necesario”, se explicó.

De este modo, con una fase de DISPO de cuatro semanas y recomendaciones para la **presencialidad educativa, como estaba previsto, los distritos ya tienen un marco para arrancar con el regreso a las aulas,** por lo menos en los casos en que los gobernadores hayan realizado sólidos acuerdos con los gremios, lo que es distinto en cada provincia. Al respecto, el Ministerio de Educación aclaró que “no se necesita la vacuna para volver a las aulas, pero sí protocolos”.

Uno de los aspectos que puntualiza el nuevo decreto es que el personal docente, no docente, alumnos y padres quedarán exceptuados de la prohibición a usar el transporte, tal como estaba previsto en el DNU anterior. Sí deberán tramitar el Certificado Único Habilitante de Circulación. También se destacarán en la difusión las medidas sanitarias que exige el DISPO, pero el cumplimiento queda a cargo de los Consejos Escolares de cada provincia, tal como se explicó más arriba. Razón e incidencia continuarán siendo los criterios a utilizar para definir las zonas de riesgo.

Son varios los distritos que están trabajando para iniciar algunos niveles de educación el 17 de febrero (CABA y Santa Fe) y el 22 de febrero (Jujuy). Como el plan de vacunación está virtualmente frenado por fuera del personal de salud, el Presidente decidió brindar lineamientos de un protocolo sanitario, mientras explora nuevas alternativas en materia de vacunas antes de lanzarse a una recomendación generalizada al retorno de las clases en las aulas.

![](https://assets.3dnoticias.com.ar/mapa-ciclo-lectivo-2021.jpg)

###### Mapa del regreso a clases. Ministerio de Educación de la Nación.

La mayoría de los distritos tienen previsto empezar entre el 1º (Provincia de Buenos Aires, por ejemplo) y el 8 de marzo (La Pampa, entre otros), por lo que el Gobierno está convencido de que tiene margen para seguir trabajando en un protocolo consensuado para el regreso presencial a las clases.

En el informe epidemiológico que hoy brindó el Ministerio de Salud se especificó que de los 9471 que se diagnosticaron ayer, el 55,7% en CABA y provincia de Buenos Aires, el 44,3% en el resto del país, con un **promedio en los últimos siete días de 8921**, una cifra aliciente en relación con lo que venía sucediendo dos semanas atrás, con promedios semanales que superaron los 12.000 contagios.

Mientras tanto, el Fondo Ruso de Inversión Directa emitía hoy por la mañana un comunicado anunciando el registro de la vacuna rusa por parte de la Administración Nacional de Productos Farmacéuticos y Medicamentos de la República de Guinea, que así “se convirtió en el segundo país africano en aprobar la vacuna Sputnik V”, después de Argelia.

Rusia, Biolorrusia, Argentina, Bolivia, Serbia, Argelia, Palestina, Venezuela, Paraguay, Turkmenistán, Hungría y los Emiratos Árabes Unidos son los países que ya la registraron.