---
category: Estado Real
date: 2021-08-26T06:00:28-03:00
thumbnail: https://assets.3dnoticias.com.ar/DESVIOARIJON.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Perotti: “es un compromiso asumido garantizar la provisión de agua”'
title: 'Perotti: “es un compromiso asumido garantizar la provisión de agua”'
entradilla: Destacó la importancia del financiamiento internacional para ejecutar
  obras que mejoren la calidad de vida de los santafesinos.

---
El gobernador Omar Perotti recorrió este miércoles el avance de los trabajos en el Acueducto Desvío Arijón– Etapa II, tramo 4 EB4- Ruta 10/ EB5 y el tramo 5 -troncal EB5 Angélica-Rafaela. “Esta etapa está teniendo el mejor ritmo desde su inicio, recuperando mucho tiempo perdido y acercando el agua al oeste de la provincia de Santa Fe”, afirmó el mandatario.

Sobre la obra, Perotti destacó que “le va a dar a toda la zona de Rafaela la posibilidad de su cobertura total de agua potable y deja también, en el tendido, los ramales para cubrir otras localidades de la región. Es un compromiso asumido garantizar la provisión de agua. La llegada del agua potable a Rafaela a través de este acueducto nos tiene que garantizar ese abastecimiento”.

Y agregó: “Le estamos poniendo empuje para poder terminarla en los próximos meses y garantizar así que una localidad más en el oeste de la provincia con provisión. El ritmo de obra tiene que ver con el cumplimiento de la provincia y el financiamiento internacional, el financiamiento de los países árabes especialmente”, dijo.

“Eso nos ha garantizado la posibilidad de tener un flujo de obra muy importante y con el cumplimiento de la provincia en cada uno de los pagos, acelera ese ritmo. Hacer salud empieza por esto, por tener resguardados los servicios esenciales de agua y cloacas”, afirmó el gobernador.

**Fondo Kuwaití para el desarrollo**

El Financiamiento internacional permite “ratificar nuestro compromiso de trabajar con ellos para pedirles refuercen y agudicen el esquema de financiamiento para mantener e incrementar el ritmo de obra, pero fundamentalmente para también presentar proyectos nuevos”, resaltó Perotti.

“Los acueductos se suman a una de las tantas obras que estamos haciendo en la provincia de Santa Fe: rutas transversales, avances en agua potable, en cloacas”, abundó el gobernador, quien durante la recorrida estuvo acompañado por el senador nacional Roberto Mirabella; el senador provincial por el departamento Castellanos, Alcides Calvo; y el secretario de Empresas y Servicios Públicos, Carlos Maina.

**En detalle**

En el Acueducto Desvio Arijón–Etapa II, tramo 4 EB4 Ruta 10/ EB5 (Angélica) al día de la fecha se encuentran trabajando dos frentes en la colocación de cañería troncal PRFV Ø900 K6; un frente de trabajo en ejecución de cámaras de válvulas; y otro frente de trabajo en la Estación de Bombeo N°4, montando la estructura metálica de la Casa de Bombas. El avance de obra es del 65,6%.

En tanto, en el tramo 5 -troncal EB5 Angélica-Rafaela, actualmente se encuentran trabajando cuatro frentes de trabajo en colocación de cañería troncal PRFV Ø1000 K10; y otro frente de trabajo en ejecución de cámaras de válvulas. El avance de obra en este tramo es de 66,3%.