---
category: Agenda Ciudadana
date: 2021-03-02T06:34:58-03:00
thumbnail: https://assets.3dnoticias.com.ar/imagen-ilustrativa.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Calendario de cobro de las prestaciones de Ansés para el mes de marzo
title: Calendario de cobro de las prestaciones de Ansés para el mes de marzo
entradilla: 'Comienza el calendario de pago de marzo 2021 de Ansés. Cuándo cobro con
  aumento: jubilados, AUH, Suaf, AUE y demás Asignaciones Familiares'

---
Ansés comunicó oficialmente el calendario de pago de marzo 2021 para jubilados y pensionados y demás beneficiarios de asignaciones como AUH, AUE o Asignación Familiar por Hijo (Suaf). El cronograma comienza el lunes 1. La etapa fuerte del mismo arranca la próxima semana con la fecha de cobro de jubilaciones y Asignación Universal por Hijo. Todos cobrarán con el aumento del 8,07% establecido por la nueva ley de movilidad.

La suba en los haberes se verá reflejada en el cobro de marzo que comienza este lunes. La jubilación mínima ascenderá a $20.571. Este mes, jubilados y pensionados no cobrarán ningún bono pero sí lo harán en abril y mayo. Se trata de dos pagos extras de $1.500 cada uno exclusivo para los que cobran hasta $30.856.

El pago de AUH y Suaf (Asignación Familiar por Hijo) comenzará también el lunes 8 de marzo y se extenderá hasta el viernes 19, día en el que a su vez se hará efectiva la acreditación de la Tarjeta Alimentaria por $6.000 o $9.000.

Ansés paga este mes la Ayuda Escolar Anual de más de $3.000 para AUH, SUAF y pensionados con hijos de entre cuatro y 17 años escolarizados.

**Calendario de pago de Ansés completo**

_Asignación Universal por Hijo y Suaf (Asignación Familiar por Hijo) marzo 2021_

* DNI terminados en 0: lunes 8 de marzo
* DNI terminados en 1: martes 9 de marzo
* DNI terminados en 2: miércoles 10 de marzo
* DNI terminados en 3: jueves 11 de marzo
* DNI terminados en 4: viernes 12 de marzo
* DNI terminados en 5: lunes 15 de marzo
* DNI terminados en 6: martes 16 de marzo
* DNI terminados en 7: miércoles 17 de marzo
* DNI terminados en 8: jueves 18 de marzo
* DNI terminados en 9: viernes 19 de marzo
* Asignación Universal por Embarazo marzo 2021
* DNI terminados en 0: miércoles 10 de marzo
* DNI terminados en 1: jueves 11 de marzo
* DNI terminados en 2: viernes 12 de marzo
* DNI terminados en 3: miércoles 15 de marzo
* DNI terminados en 4: jueves 16 de marzo
* DNI terminados en 5: viernes 17 de marzo
* DNI terminados en 6: lunes 18 de marzo
* DNI terminados en 7: martes 19 de marzo
* DNI terminados en 8: miércoles 22 de marzo
* DNI terminados en 9: jueves 23 de marzo
* Pensiones no Contributivas
* DNI terminados en 0 y 1: lunes 1 de marzo
* DNI terminados en 2 y 3: martes 2 de marzo
* DNI terminados en 4 y 5: miércoles 3 de marzo
* DNI terminados en 6 y 7: jueves 4 de marzo
* DNI terminados en 8 y 9: viernes 5 de marzo

_Jubilados marzo 2021_

_Jubilados y pensionados cuyos haberes mensuales no superen los $23.120:_

* DNI terminados en 0, a partir del día 8 de marzo de 2021.
* DNI terminados en 1, a partir del día 9 de marzo de 2021.
* DNI terminados en 2, a partir del día 10 de marzo de 2021.
* DNI terminados en 3, a partir del día 11 de marzo de 2021.
* DNI terminados en 4, a partir del día 12 de marzo de 2021.
* DNI terminados en 5, a partir del día 15 de marzo de 2021.
* DNI terminados en 6, a partir del día 16 de marzo de 2021.
* DNI terminados en 7, a partir del día 17 de marzo de 2021.
* DNI terminados en 8, a partir del día 18 de marzo de 2021.
* DNI terminados en 9, a partir del día 19 de marzo de 2021.

_Jubilados y pensionados cuyos haberes mensuales superen los $23.120:_

* DNI terminados en 0 y 1, a partir del día 22 de marzo de 2021.
* DNI terminados en 2 y 3, a partir del día 23 de marzo de 2021.
* DNI terminados en 4 y 5, a partir del día 25 de marzo de 2021.
* DNI terminados en 6 y 7, a partir del día 26 de marzo de 2021.
* DNI terminados en 8 y 9, a partir del día 29 de marzo de 2021.
* Asignación por Prenatal y Asignación por Maternidad
* DNI terminados en 0 y 1: viernes 12 de marzo
* DNI terminados en 2 y 3: lunes 15 de marzo
* DNI terminados en 4 y 5: martes 16 de marzo
* DNI terminados en 6 y 7: miércoles 17 de marzo
* DNI terminados en 8 y 9: jueves 18 de marzo

_Asignaciones de pago único: matrimonio, adopción y nacimiento_

Las asignaciones de pago único de la primera quincena de este mes se pagan entre el 9 de marzo y el 9 de abril. En tanto, las de la segunda quincena se pagan entre el 19 de marzo y el 9 de abril.

_Asignaciones Familiares de Pensiones no Contributivas_

Las Asignaciones familiares de Pensiones no contributivas se pagan entre el 8 de marzo y el 9 de abril.

_Desempleo_

* DNI terminados en 0 y 1: martes 23 de marzo
* DNI terminados en 2 y 3: miércoles 25 de marzo
* DNI terminados en 4 y 5: jueves 26 de marzo
* DNI terminados en 6 y 7: viernes 29 de marzo
* DNI terminados en 8 y 9: viernes 30 de marzo