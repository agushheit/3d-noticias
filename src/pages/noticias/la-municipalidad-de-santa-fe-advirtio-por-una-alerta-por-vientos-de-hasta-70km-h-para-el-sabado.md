---
category: La Ciudad
date: 2021-05-22T06:15:49-03:00
thumbnail: https://assets.3dnoticias.com.ar/ALERTA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: La Municipalidad de Santa Fe advirtió por una alerta por vientos de hasta
  70km/h para el sábado
title: La Municipalidad de Santa Fe advirtió por una alerta por vientos de hasta 70km/h
  para el sábado
entradilla: 'Las autoridades locales recomiendan no salir si no es imprescindible
  y retirar todos los objetos que se puedan volar de los lugares al aire libre. Además,
  adelantaron que podría haber caídas de árboles y postes. '

---
La Directora Municipal de Gestión de Riesgo, Cintia Gauna, informó este viernes a El Litoral que para el sábado se prevé “una alerta amarilla por tormentas” que a partir de las 18 pasará a ser alerta por lluvias, según datos técnicos del Servicio Meteorológico Nacional.

“Estamos trabajando con las áreas operativas que dan respuesta a lo que puedan ocasionar los vientos, pero estamos en otoño, y la hojarasca complica la limpieza de desagües”, advirtió la funcionaria.

Por otro lado, Gauna recomendó no salir y retirar de los espacios al aire libre “cosas que puedan llegar a volarse”, ya que las ráfagas podrán alcanzar hasta 70 kilómetros por hora. “Es mucho – insistió-, y hay que recordar que después de la lluvia se ablandan las superficies donde están árboles y postes y eso puede generar inconvenientes”.

En tanto, recordó que el 0800 - 777 - 5000 y los coordinadores de distrito se encuentran disponibles como una doble vía de reclamo para atender los problemas que puedan surgir por el fenómeno climático

Por su parte, Silvina Serra, secretaria municipal de Asuntos Hídricos, informó que durante el viernes precipitaron 66 mm, “el doble” de la capacidad de los conductos de la ciudad, pero “no tuvimos problemas.”

“Entraron algunos reclamos porque hubo acumulación de hojas en las bocas de tormentas, pero es típico de esta época del año”, concluyó Serra.