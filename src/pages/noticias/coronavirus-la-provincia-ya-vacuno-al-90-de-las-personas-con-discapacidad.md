---
category: Estado Real
date: 2021-07-23T06:00:59-03:00
thumbnail: https://assets.3dnoticias.com.ar/DISCAPACIDAD.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Coronavirus: la provincia ya vacunó al 90% de las personas con discapacidad'
title: 'Coronavirus: la provincia ya vacunó al 90% de las personas con discapacidad'
entradilla: Además, quiénes se inscriban en el Registro Provincial de Vacunación podrán
  anotar su Certificado Único de Discapacidad.

---
En el marco del operativo de vacunación de Covid-19, que avanza a gran velocidad en toda la provincia, ya recibieron, al menos una dosis, el 90% de las personas con discapacidad y de riesgo de 18 a 59 años. Además, desde este jueves se puede sumar el Certificado Único de Discapacidad (CUD) en el Registro Provincial de Vacunación Coronavirus.

Desde el inicio de la inscripción, ya se anotaron 59 mil personas con discapacidad para recibir la vacuna del Coronavirus. Al día de hoy, ya recibieron, al menos una dosis, 53.153 personas de este grupo poblacional.

Además, a partir del 22 de julio del corriente año, aquellas personas que posean Certificado Único de Discapacidad (CUD) podrán seleccionar dicha opción a la hora de registrarse voluntariamente para recibir la vacuna contra el Covid-19.

Si bien se realiza el cruce de datos correspondientes con el listado de Certificados Únicos de Discapacidad, a partir de ahora cada beneficiario y beneficiaria podrá también seleccionarlo en el Registro de vacunación.