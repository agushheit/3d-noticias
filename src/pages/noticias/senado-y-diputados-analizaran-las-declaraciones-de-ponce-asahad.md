---
category: Agenda Ciudadana
date: 2020-12-10T11:08:50Z
thumbnail: https://assets.3dnoticias.com.ar/legislatura.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: El Litoral'
resumen: Senado y Diputados analizarán las declaraciones de Ponce Asahad
title: Senado y Diputados analizarán las declaraciones de Ponce Asahad
entradilla: 'El fiscal destituido por cobrar coimas dice que  altos funcionarios de
  los tres poderes del Estado lo sabían. '

---
Con su declaración, el ex integrante del MPA agravó su situación procesal al describirse como una de las piezas de una asociación ilícita, pero obtuvo un mejor lugar de detención.

En el bloque de senadores del PJ se espera contar, en las próximas horas, con información oficial sobre las imputaciones que el ex fiscal Gustavo Ponce Asahad formuló sobre muy altos integrantes de los tres Poderes, al declarar en el juicio en su contra, por haber cobrado coimas a un empresario del juego ilegal.

Como se sabe, en el marco de la causa judicial sobre el ex integrante del MPA, Ponce Asahad ha mencionado presuntos vínculos de legisladores y otros altos funcionarios de los otros poderes en una asociación ilícita cuyo objetivo era -según su declaración judicial- dar protección política a una red de juego ilegal a cambio de dinero. Ponce Asahad declaró en sede judicial que las coimas del juego clandestino no alcanzaban solo al MPA y disparó muy hacia arriba.

En tanto, el jefe de la bancada justicialista, Armando Traferri ha dicho a sus pares que, para fijar una posición públicamente, se debe esperar a tener una copia oficial de las expresiones del otrora fiscal. En rigor, en el bloque de doce integrantes del oficialismo, tanto entre los 6 senadores del Nuevo Espacio Santafesino (Nes) que lidera también Traferri, como entre otros cuatro legisladores más cercanos a la Casa Gris, así como en los dos restantes, había un profundo malestar por la falta de información oficial para con el Senado (de parte del juzgado).

<br/>

## **Queja**

Uno de los legisladores se quejó ante El Litoral: "¿Cómo es que desde la justicia se lleva la información al jefe de la oposición y no a cada uno de los legisladores, por los canales institucionales que corresponden? Eso es hoy lo que más me preocupa", bramó. Hablaba de la grabación de las dos horas y media de la declaración judicial de Ponce Asahad, que están en poder de la presidencia de la Cámara de Diputados.

Como se recordará, el ex gobernador Miguel Lifschitz, como titular de la cámara baja, ha convocado a una reunión de consulta con los jefes de las distintas bancadas del cuerpo para analizar la situación, este miércoles 9. Y también para ese día, como es habitual a esa altura de la semana cada vez que hay actividad legislativa, está prevista la reunión del bloque de senadores del oficialismo.

<br/>

## **Prudencia**

De acuerdo con lo que este diario pudo conversar con integrantes de la bancada justicialista en ambos grupos manda la prudencia. No hay ninguna palabra de más, de las que no se puede dar marcha atrás y está en un evidente segundo plano la situación interna de la bancada.

Tanto entre los más afines al gobernador Omar Perotti como entre aquellos que pregonan su rol legislativo de "oficialismo responsable", se piensa que lo primero es tener información de primera mano  y seguir con atención la causa. Es que además de las acusaciones del fiscal sorprendido cobrando coimas (esa es la fuente del escándalo) sobre uno de los senadores hay más imputaciones en las más altas esferas.

<br/>

## **Detalle**

Un senador que también esperaba tener cuanto antes la copia oficial de la declaración de Ponce Asahad señaló a El Litoral un detalle hasta ahora inadvertido: es bastante curioso que el ex fiscal bajo prisión preventiva haya agravado su propia situación procesal, al reconocerse parte de una supuesta asociación ilícita avalada según sus dichos por la "corporación política". También evaluó que con su declaración el ex fiscal tiene ahora unas condiciones de encierro algo mejores.

<hr style="border:2px solid red"> </hr>

**Ponce Asahad fue grabado en medio de un pedido de coimas y luego admitió haberlas cobrado, en complicidad con su ex jefe, el ex fiscal Regional de Rosario, Patricio Serjal, quien optó por renunciar a su cargo para evitarse el escarnio de la destitución por la Legislatura**.

<hr style="border:2px solid red"> </hr>

El denunciante -que ha causado un fuerte revuelo político- fue removido de su cargo por unanimidad en una asamblea conjunta de ambas Cámaras: no hubo ningún voto en su favor, ni abstenciones.