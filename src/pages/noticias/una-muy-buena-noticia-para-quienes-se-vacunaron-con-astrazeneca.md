---
category: Agenda Ciudadana
date: 2021-07-24T06:00:52-03:00
thumbnail: https://assets.3dnoticias.com.ar/VACUNACION35.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Una muy buena noticia para quienes se vacunaron con AstraZeneca
title: Una muy buena noticia para quienes se vacunaron con AstraZeneca
entradilla: Un estudio desarrollado en Canadá con 69.533 contagiados con SARS-CoV-2
  entre diciembre de 2020 y mayo de 2021 con vacunas AstraZeneca arrojó resultados
  positivos.

---
La farmacéutica anglo-sueca AstraZeneca informó este viernes que una dosis de su vacuna Vaxzevria -la nueva denominación que lleva el fármaco- es "altamente eficaz" para prevenir casos graves de covid y hospitalizaciones causadas por las variantes beta y delta del coronavirus.

El estudio, desarrollado en Canadá con 69.533 contagiados con SARS-CoV-2 entre diciembre de 2020 y mayo de 2021, indicó que el preparado alcanza un 87 por ciento de eficacia ante la "hospitalización o fallecimiento" causados por la variante delta (B.617.2, también conocida como "india"), y un 90 por ciento ante la alfa (B.1.1.7 o "Kent", en referencia al condado inglés donde fue detectada primero).

Asimismo, tiene un 82 por ciento de eficacia, después de un pinchazo, ante la "hospitalización o fallecimiento" causados por la variante beta/gamma, identificada por primera vez en Sudáfrica (B.1.351).

Del total de individuos analizados para este estudio por la Red de Investigación de Inmunización de Canadá (CIRN), 40.828 (9,7%) eran positivos por alguna de las variantes consideradas como "preocupantes", mientras que 28.705 (6,8%) estaban contagiados por otras variantes del coronavirus.

El análisis advierte que no hubo tiempo suficiente aún para informar sobre la efectividad de Vaxzevria después de la segunda dosis, aunque "otros estudios demostraron un “aumento de eficacia" tras el segundo pinchazo.

"La eficacia de Vaxzevria después de una dosis contra la hospitalización o fallecimiento era similar a la de otras vacunas probadas en el estudio", señaló AstraZeneca en un comunicado.

El vicepresidente ejecutivo de I+D de la anglo-sueca, Mene Pangalos, destacó que este preparado, analizado con "datos del mundo real", ofrece "un alto nivel de protección" ante las "formas más graves" de covid-19, incluso después de "una sola dosis".

"Es esencial que continuemos protegiendo a tanta gente como sea posible en todos los rincones del mundo para poder adelantar a este virus mortal", agregó el directivo.