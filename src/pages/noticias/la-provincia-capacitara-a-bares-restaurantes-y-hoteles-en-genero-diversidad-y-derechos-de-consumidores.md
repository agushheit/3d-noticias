---
category: Estado Real
date: 2022-01-05T06:15:07-03:00
thumbnail: https://assets.3dnoticias.com.ar/capacitacion.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La provincia capacitará a bares, restaurantes y hoteles en género, diversidad
  y derechos de consumidores
title: La provincia capacitará a bares, restaurantes y hoteles en género, diversidad
  y derechos de consumidores
entradilla: Estará destinada a establecimientos de las localidades de Rosario, Santa
  Fe y Rafaela. La inscripción se encuentra abierta hasta el 11 de enero.

---
El Gobierno de la Provincia de Santa Fe, a través de los Ministerios de Igualdad, Género y Diversidad y de Producción, Ciencia y Tecnología, lleva adelante el programa “Sensibilización en género, diversidad sexual y relaciones de consumo”, con el acompañamiento de las Asociaciones Hoteleras y Gastronómicas de las localidades de Rafaela, Rosario y Santa Fe. La misma está destinada a bares, restaurantes y hoteles de dichas localidades.

Esta propuesta busca promover la reflexión acerca de aquellas prácticas y discursos que sustentan y reproducen violencias por motivos de identidad, orientación sexual y/o expresión de género hacia mujeres e integrantes del colectivo LGBTIQ+ en dichos espacios. Además, con esta campaña se busca promover buenas prácticas comerciales, para fomentar relaciones igualitarias,que respeten la dignidad de las y los consumidores, a fin de prevenir estereotipos de género o prácticas sexistas en las distintas etapas de las relaciones de consumo.

A través de capacitaciones en género y diversidad, y en derechos de las personas consumidoras, las y los trabajadores de bares, restaurantes y hoteles contarán con herramientas para la prevención de situaciones de violencias y/o de trato discriminatorio en las relaciones de consumo que los atraviesan y en la utilización adecuada del derecho de admisión en los establecimientos.

“Acompañamos la decisión de nuestro gobernador, Omar Perotti, de disminuir las desigualdades de género y promover una sociedad más justa e igualitaria, por eso "Temporada Igualdad" apunta a brindar herramientas a Bares, Restaurantes y Hostelerías para asegurar ambientes igualitarios y construir relaciones de consumo saludables y respetuosas. Se trata de una una sensibilización al personal de los establecimientos, acompañado de una campaña gráfica y la construcción de un circuito seguro para mujeres y personas LGBTIQ+”, sostuvo la ministra de Igualdad, Género y Diversidad, Florencia Marinaro.

A su vez, agregó que “desde el Ministerio de Igualdad, Género y Diversidad celebramos que todos los sectores que hemos invitado a participar de la iniciativa ‘Temporada Igualdad’ se hayan sumado con un gran compromiso, debido a que para poder modificar los patrones culturales y disminuir las brechas de desigualdad, es necesario que haya un trabajo interseccional”.

Además, se les entregará un protocolo de acción y denuncia ante situaciones por violencia de género y discriminación por motivos de identidad, orientación y/o expresión de género y una “Guía de Buenas Prácticas en las Relaciones de Consumo con Perspectiva de Géneros y Diversidades” para promover relaciones de consumo respetuosas de los derechos de las y los consumidores.

Al respecto, la directora de Promoción de la Competencia y Defensa del Consumidor, María Betania Albrecht manifestó: “Consideramos sumamente importante abordar las relaciones de consumo que se establecen entre consumidores y proveedores de bienes y servicios, en este caso, bares, restaurantes y hoteles, de modo de fomentar prácticas respetuosas de los derechos de quienes utilizan dichos bienes y servicios, no sólo como modo de satisfacción de sus necesidades, sino como espacio de ocio y esparcimiento”.

“Con esta campaña de sensibilización pretendemos promover buenas prácticas y estimular el ‘principio de autorregulación’ hacia el cual avanza el moderno derecho del consumo, consistente en la configuración de determinadas reglas nacidas de la propia autonomía privada, las cuales terminarán siendo concordantes con las normas vigentes en nuestro sistema jurídico de protección de consumidores, siendo esta una potente herramienta institucional destinada a la prevención de conflictos y la optimización de la experiencia de las y los consumidores y proveedores. Pero además, resulta indiscutible la necesidad de atravesar las relaciones de consumo que se generan en estos espacios con la perspectiva de género y diversidades, de modo de dar trato respetuoso, digno y no discriminatorio a todas aquellas personas que decidan concurrir a cualquiera de estos espacios. La perspectiva de género y el derecho del consumo son ambos conceptos transversales a los vínculos entre personas en estos ámbitos”, agregó la funcionaria”.

Hay tiempo para inscribirse hasta el 11 de enero. Para hacerlo, se deberá ingresar al siguiente [link](https://www.santafe.gov.ar/formularios/index.php/536155?lang=es).