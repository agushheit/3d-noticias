---
category: Agenda Ciudadana
date: 2022-05-23T10:26:29-03:00
thumbnail: https://assets.3dnoticias.com.ar/2022-05-23NID_274773O_2.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: LA PROVINCIA PRESENTÓ SUS HERRAMIENTAS DE VINCULACIÓN PARA LA INVESTIGACIÓN
  Y DESARROLLO DE CANNABIS MEDICINAL
title: LA PROVINCIA PRESENTÓ SUS HERRAMIENTAS DE VINCULACIÓN PARA LA INVESTIGACIÓN
  Y DESARROLLO DE CANNABIS MEDICINAL
entradilla: El proyecto, elaborado junto al INTA y el CONICET, se dio a conocer durante
  una reunión informativa ante entidades, asociaciones y emprendedores privados.

---
El Ministerio de Producción, Ciencia y Tecnología fue sede de un encuentro informativo sobre investigación y desarrollo productivo de cannabis medicinal en Santa Fe.

La reunión, realizada el viernes, se impulsó articuladamente entre la cartera productiva, el Laboratorio Industrial Farmacéutico (LIF), el Instituto Nacional de Tecnología Agropecuaria (INTA) y el Consejo Nacional de Investigaciones Científicas y Técnicas (CONICET), y tuvo como objetivo fue presentar las herramientas de vinculación vigentes para el desarrollo de futuros proyectos privados con las entidades referentes (INTA-CONICET), según lo estipulado en la legislación actual.

Asimismo, se comunicó sobre el proyecto provincial en la materia y se informó sobre las líneas de financiamiento vigentes destinadas a la investigación y al desarrollo productivo del cannabis medicinal en Argentina y Santa Fe.

Luego del encuentro, que contó con representantes de 19 entidades, asociaciones y emprendedores privados, la subsecretaria de Proyectos de Innovación Productiva, Eliana Eberle, sostuvo que "para el Ministerio de Producción, Ciencia y Tecnología es muy importante poder acercar las instituciones vinculantes a los emprendedores privados y aquellos que vienen desarrollando la producción de cannabis medicinal en la Provincia de Santa Fe”.

“En esta instancia, nosotros resaltamos que la legislación, en el marco regulatorio vigente, nos permite avanzar en la investigación y el desarrollo del cannabis medicinal, que es para lo que estamos trabajando -agregó-. Pero entendemos que, además, nos prepara para dar un salto en nuestras economías regionales a través de la regulación del desarrollo industrial de la producción de aceite medicinal, e incluso también la inclusión del cáñamo".

Por su parte, la directora del LIF, Elida Formente, expresó: "Es un orgullo poder participar de estos encuentros y principalmente este, que tiene que ver con la red que venimos formando en torno a la política pública sobre cannabis medicinal. Durante la reunión brindamos las herramientas vigentes para continuar desarrollando las investigaciones pertinentes a fin de brindar un producto seguro. A través de estas instancias de vinculación, los actores participantes estamos haciendo historia dado que es un inicio de acción y colaboración conjunta”.

El vinculador técnico de la Oficina de Vinculación Tecnológica del CONICET Santa Fe, Guillermo Moreno Piovano, afirmó que "este tipo de encuentros eran necesarios desde hace varios años, ya que es muy importante visibilizar el trabajo científico, académico y tecnológico en torno al cannabis. Desde un ámbito como el nuestro brindamos conocimiento al respecto, para que el espacio público se acerque al privado, es decir al emprendedor, y para que este último manifieste sus necesidades a fin de unificar nuestras capacidades. La sinergia entre lo público y lo privado es de vital importancia".

A su turno, el director reemplazante del Centro Regional del INTA Santa Fe, Luis Carrancio, detalló que “desde el INTA venimos participando de este trabajo articulado, la idea de esta reunión fue compartir entre los actores que venimos llevando adelante el convenio aportando para la investigación del cannabis y la demanda del sector. De este encuentro participaron muchas personas que están esperando los resultados de nuestro proyecto, por eso nos sirvió para re-focalizar nuestra labor en pos de la demanda".

Mientras, el médico veterinario Norberto Ojeda manifestó: "Estamos haciendo algunas investigaciones y ensayos del cannabis en lo que concierne a la medicina veterinaria, por tal motivo venimos a interiorizarnos sobre los proyectos a fin de evaluar futuras vinculaciones a partir del trabajo realizado por la provincia de Santa Fe. Este tipo de encuentros son muy interesantes, abren la puerta para que otros actores privados comiencen a participar, ya que el cannabis convoca a muchos profesionales y entidades. Por todo ello, es primordial darle una regulación y avanzar en una futura agenda de trabajo”.

Finalmente, la representante de la Asociación Civil “Conectar”, Sofía Morello, agregó: “Agradecemos la puerta al diálogo que abre tanto el Ministerio de Producción, Ciencia y Tecnología, como las demás entidades vinculantes. Nos parece un reconocimiento, puntualmente nosotros venimos de una Asociación Civil, militamos cuestiones que tienen que ver con el cannabis. Es importante que desde el Estado nos reconozcan como actores importantes en las conquistas que van sucediendo y la expertise que tenemos en el campo del cannabis. Agradecemos que el diálogo permanezca abierto porque nos parece que la forma de construir siempre tiene que ser colectiva, es por eso nuestro agradecimiento para participar a este espacio de diálogo”.

**PROYECTO DE LA PROVINCIA**

En el encuentro el Gobierno de Santa Fe presentó el proyecto provincial para el desarrollo y la investigación de cannabis, iniciativa que se concreta a raíz de la celebración de un convenio junto al INTA y al LIF. A través del mismo se está constituyendo un centro experimental modelo que contará con el desarrollo e investigación de tres diferentes formas de cultivo (exterior, interior y bajo cubierta), y un laboratorio experimental de extracción, purificación y caracterización de sustancias cannabinoides con calidad farmacéutica.

Este centro estará preparado para la extensión de todo el conocimiento que allí se genere, a fin de brindar herramientas claras sobre los requerimientos de estos tipos de cultivos, buenas prácticas agropecuarias y, en complemento, insumos e infraestructuras necesarias para la definición de un productor a la hora de constituir su modelo productivo.

Cabe destacar que este centro será receptivo a todo el conocimiento ya generado en Santa Fe: científico - tecnológico, madres, cultivadores y profesionales, respetando y reconociendo la construcción ciudadana y comunitaria ya desarrollada.