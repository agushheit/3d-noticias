---
category: La Ciudad
date: 2021-12-22T06:15:07-03:00
thumbnail: https://assets.3dnoticias.com.ar/municipalidad.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Cambios en la estructura de la Municipalidad
title: Cambios en la estructura de la Municipalidad
entradilla: El Concejo Municipal aprobó el proyecto enviado por el intendente Emilio
  Jatón para modificar la Ordenanza de Secretarías. “La pospandemia nos obliga repensar
  la ciudad y a establecer nuevas prioridades”, dijo.

---
Este martes, el intendente Emilio Jaton envió al Concejo Municipal la nueva Estructura del Departamento Ejecutivo Municipal y logró su aprobación por unanimidad. Los principales cambios se vinculan a la jerarquización de algunas áreas y la refuncionalización de otras en consonancia con las políticas públicas que se están implementando en la capital provincial.

Al respecto, el intendente explicó que en la reestructuración del gabinete se está “leyendo, escuchando y viendo cómo ha cambiado la sociedad después de esta pandemia” y también las mayores demandas de la ciudadanía. En ese sentido, remarcó que en esta etapa se intensificará la acción del Estado municipal en la gestión urbana. el mantenimiento vial, la iluminación y la mejora de los espacios públicos.

“A partir de eso, que consideramos que es el eje central y así lo expresamos en el Presupuesto, tendremos un área donde todo eso se pueda ejecutar de una manera más ágil y descentralizada”, agregó. Matías Pons Estel estará al frente de la Dirección de Gestión Urbana, que dependerá directamente del intendente con el objetivo de fortalecer la respuesta en obras esenciales en el entramado urbano.

Otra de las modificaciones principales es la jerarquización del área que gestiona el transporte con la mirada puesta en un abordaje integral del sistema multimodal de movilidad, contemplando las características específicas de cada área y buscando la mejora de su interrelación. “Para nosotros es central abarcar el tema de la movilidad, nuestro transporte público, nuestras bicicletas y cómo ha cambiado la forma de movilizarse de los santafesinos. A partir de allí creamos una Dirección de Movilidad”, explicó el intendente respecto al área que conducirá Andrea Zorzón.

Además la Secretaría de Ambiente sumará la agenda del Cambio Climático y estará a cargo de Franco Ponce de León, quien hasta el momento se desempeñaba como director de Derechos y Vinculación Ciudadana. Y la actual secretaria de Asuntos Hídricos, Silvina Serra, sumará a su órbita el área de infraestructura tomando así la gestión de las grandes obras que se desarrollan en los distintos barrios de la ciudad con una mirada integral.

Por otro lado, Soledad Artigas, directora de Mujeres y Disidencias, asume como secretaria de Políticas de Cuidado y Acción Social. De esa manera, se buscará crear una mirada integral y transversal del sistema de cuidado para dar respuesta a las emergencias y promover derechos y más oportunidades.