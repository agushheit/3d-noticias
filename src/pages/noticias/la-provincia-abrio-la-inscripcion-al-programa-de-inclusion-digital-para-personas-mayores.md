---
category: Estado Real
date: 2021-10-14T06:00:23-03:00
thumbnail: https://assets.3dnoticias.com.ar/ADULTOS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Provincia abrió la inscripción al programa de inclusión digital para personas
  mayores
title: La Provincia abrió la inscripción al programa de inclusión digital para personas
  mayores
entradilla: Será desde esta semana y hasta el viernes 22 de octubre. Se realizarán
  375 talleres en distintas localidades del territorio provincial.

---
El ministerio de Desarrollo Social, a través de la Dirección Provincial de Personas Mayores, abrió a partir de esta semana y hasta el viernes 22 de octubre inclusive, la inscripción para personas mayores al ciclo de capacitaciones del programa provincial de Inclusión Digital denominado “Mayores en red”.

El objetivo es democratizar el acceso a las nuevas tecnologías de comunicación y disminuir la brecha digital. Acercar el manejo de teléfonos celulares y redes sociales para impulsar el empoderamiento de las personas mayores en el uso de las nuevas tecnologías.

Para que las personas mayores puedan comunicarse con su entorno, minimizando la desigualdad entre personas con acceso o conocimiento de la tecnología y aquellas que no, y contribuir a democratizar su acceso a través de estas.

En distintas localidades de la provincia se realizarán 375 talleres sobre; uso de celular: encendido y apagado, volumen, wifi y datos, correo electrónico, redes sociales: Whatsapp, Facebook, Instagram, Billetera Santa Fe, uso de aplicaciones: ANSES, PAMI, IAPOS, entre otros. El cursado tendrá una duración de 2 meses.

El ciclo culminará con la charla denominada “Consejos de seguridad en internet”, importante en estos tiempos debido a las reiteradas estafas digitales que se cometen.

Las personas mayores interesadas en participar pueden inscribirse a través de los municipios, comunas y organizaciones de la sociedad civil, consultando el siguiente enlace:  
[http://www.santafe.gob.ar/index.php/web/content/download/264873/1386966/file/Programa%20de%20Inclusi%C3%B3n%20Digital%](http://www.santafe.gob.ar/index.php/web/content/download/264873/1386966/file/Programa%20de%20Inclusi%C3%B3n%20Digital% "http://www.santafe.gob.ar/index.php/web/content/download/264873/1386966/file/Programa%20de%20Inclusi%C3%B3n%20Digital%")