---
category: Agenda Ciudadana
date: 2021-01-28T10:38:21Z
thumbnail: https://assets.3dnoticias.com.ar/samid.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: Según Samid, la carne barata del Gobierno "es de segunda"
title: Según Samid, la carne barata del Gobierno "es de segunda"
entradilla: El empresario criticó la iniciativo y dijo que este es un arreglo que
  sólo le sirve a los frigoríficos.

---
El empresario Alberto Samid aseguró que el reciente acuerdo que hizo el Gobierno con los frigoríficos "no sirve para nada" porque lo que se venderá en el país es "carne de segunda" que fue rechazada por otros países, y cuestionó que "en el país de la carne no se puede comer carne".

"Este arreglo no sirve para nada. Es un arreglo para los frigoríficos nada más. Te venden el asado a $400 cuando el 50% es grasa y hueso. Te queda a $800 el kilo", subrayó Samid en declaraciones radiales.

El empresario remarcó que el acuerdo se trata de "un rechazo de exportación que ni los chinos los quieren" y "los únicos boludos que la van a comer son los argentinos".

"No hay que tener miedo de decirle al Presidente 'acá te equivocaste, cambialo´. Le dicen que está todo bien después te pasa lo que le pasó a Macri, que hasta el último día le decían que ganaba las elecciones. Después perdió por 20 puntos", manifestó.

Y agregó: "Los que están alrededor de Fernández le dicen que está todo bien y no está todo bien. Si realmente querés a este Gobierno tenés que decir la verdad: la gente no puede morfar".

Por último, explicó que "este fue el año de la historia que menos carne consumieron los argentinos".

"Estamos por debajo de los 50 kilos. Estamos consumiendo la misma cantidad de carne que pollo. En un Gobierno peronista no puede valer $400 o $500 el kilo de milanesa. No podemos hacer laburar a la gente por dos kilos de carne", concluyó.