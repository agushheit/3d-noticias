---
category: La Ciudad
date: 2021-10-01T06:00:34-03:00
thumbnail: https://assets.3dnoticias.com.ar/LANCHA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'Alto Verde: la Municipalidad de Santa Fe prometió que se hará cargo de la
  reparación de la lancha – taxi'
title: 'Alto Verde: la Municipalidad de Santa Fe prometió que se hará cargo de la
  reparación de la lancha – taxi'
entradilla: El propio intendente, Emilio Jatón, se reunió con Gerónimo Heredia -el
  último botero que se mantiene prestando el servicio- para buscarle una solución
  a la avería de la embarcación.

---
El señor Gerónimo Heredia es el último botero que se mantiene prestando el servicio de lancha – taxi, con el que se complementa el transporte que brinda la Línea 13 de colectivos en Alto Verde. Desde los 24 años -actualmente tiene 61- trabaja cruzando a los más de diez mil vecinos hacia el centro de la ciudad, para lo que realiza entre veinte y treinta viajes diarios entre ambas márgenes del Riacho Santa Fe.

Sin embargo, desde hace un par de meses no puede realizar la tarea ya que la embarcación no tiene motor: se averió y la reparación cuesta más de setenta mil pesos.

Durante el mediodía de este jueves, las autoridades municipales tomaron conocimiento de la situación mediante una publicación de El Litoral y se reunieron con Gerónimo en la costa. “Estuvo el Intendente -contó Gerónimo”, por lo menos me dio la solución de tratar de conseguir pronto el motor y poder pasar la gente lo más rápido posible”. “También estuvo la presidenta de la vecinal que nos dio una gran mano”, añadió el botero.

“Les presenté un presupuesto que es más o menos económico porque están muy caros los motores -los precios “arrancan” en $ 80.000 -, pero por lo menos para zafar en el momento; es un motorcito más o menos, pero en condiciones, nuevo”, explicó Heredia. “Ahora – indicó - tienen que averiguar dónde comprarlo, pero ya estamos en marcha, gracias a Dios”.