---
category: Agenda Ciudadana
date: 2021-04-10T06:00:19-03:00
thumbnail: https://assets.3dnoticias.com.ar/MOLINOS.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Provincia y molinos harineros trabajan en estrategias asociativas para
  llegar a más mercados
title: La Provincia y molinos harineros trabajan en estrategias asociativas para llegar
  a más mercados
entradilla: Se realizó una charla informativa en la que se abordaron las ventajas
  de aunar esfuerzos para aumentar las exportaciones.

---
El Ministerio de Producción, Ciencia y Tecnología, a través de la Secretaría de Comercio Exterior y la Agencia Santa Fe Global, impulsó una charla informativa con representantes del sector de molinos harineros, instancia en la que se abordaron las ventajas de asociarse y aunar esfuerzos para llegar a más mercado y aumentar las exportaciones.

En la apertura del encuentro, el ministro de Producción, Ciencia y Tecnología, Daniel Costamagna, aprovechó la oportunidad para presentar a funcionarios del Belgrano Cargas y trabajar sobre aspectos logísticos; y manifestó como un eje estratégico de política pública provincial “la puesta a punto y mejoramiento de la infraestructura de exportación para aumentar la competitividad de las pymes y que de esta forma tengan más acceso a mercados internacionales”.

Por su parte, el secretario de Comercio Exterior, German Burcher, realizó una introducción sobre las herramientas provinciales en promoción de exportaciones y sobre la impronta sobre grupos exportadores que está teniendo la provincia. “Actualmente cuenta con 4 grupos asociativos de diferentes sectores, la asociatividad para exportar es una política que está llevando a cabo la Provincia. Las empresas no pierden su individualidad, sino que se busca el punto de asociatividad necesario para cada actividad. Desde la Secretaría vamos a acompañar a las empresas que quieran desarrollar este proceso”, resaltó.

El encuentro contó con la exposición de Diego Turco, titular Jit Lubricación integrante del ex Grupo Unesa, Consorcio de Cooperación. Fabricantes Argentinos de Herramientas para Ferretería que comercializó en latinoamérica entre el 2002 y el 2018. Turco diálogo con los empresarios sobre el asociativismo como instrumento de internacionalización. Al respecto, subrayó: “Por más grande que seamos adentro, afuera somos chicos, por eso el asociativismo es un instrumento que nos sirve para competir en los mercados externos”.

Del encuentro también participó el subsecretario de Comercio Exterior y Nuevas Tecnología, Lucas Candioti.