---
category: Deportes
date: 2021-05-25T08:11:37-03:00
thumbnail: https://assets.3dnoticias.com.ar/voley.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa FEVA
resumen: La Selección de Voleibol camino a la burbuja en Rimini
title: La Selección de Voleibol camino a la burbuja en Rimini
entradilla: Este lunes la Selección Argentina Masculina partió desde Monza hacia Rimini,
  sede de la burbuja de la Liga de Naciones FIVB, que iniciará el próximo viernes.

---
El combinado nacional llegó a suelo italiano el pasado sábado y se alojó en Monza, donde entrenó hasta este lunes en las facilidades de Vero Volley. Pasado el mediodía, el equipo inició su viaje a Rimini, donde seguirá los pasos dispuestos por FIVB para unirse a la burbuja. La competencia femenina iniciará este martes, mientras la masculina lo hará el viernes.

En el primer weekend Argentina enfrentará a Brasil el viernes a las 16hs, luego a Canadá el sábado a las 14:30hs y por último a Estados Unidos, el domingo a las 11hs. Todos los partidos podrán verse en la pantalla de TyC Sports y también en forma digital en TyC Sports Play.

**INFORMACIÓN SOBRE JUGADORES AISLADOS POR COVID**

FeVA informa que se encuentra en tratativas con la Federación Internacional de Voleibol (FIVB) para poder incluir a los jugadores previamente inscriptos una vez que cuenten con el alta médica y cumpliendo los requisitos que disponga la FIVB para ingresar a la burbuja.

En cuanto a la situación médica, Pablo Crer, Ezequiel Palacios, Agustín Loser, Franco Massimino, Matías Sánchez y Cristian Poglajen, jugadores considerados contactos estrechos, se encuentran respetando el aislamiento preventivo obligatorio y en seguimiento médico. Los mismos serán testados este lunes para un primer control y en caso de ser negativos, continuarán los procedimientos para poder sumarse al grupo respetando todos los protocolos sanitarios.

En el caso de los jugadores que dieron positivo -Bruno Lima, Santiago Danani y Nicolás Lazo-, se encuentran en buen estado de salud, sin sintomatología y con seguimiento diario del cuerpo medico. Una vez que cumplan los días de aislamiento de acuerdo a los protocolos sanitarios, el staff medico realizará los estudios correspondientes para evaluar su aptitud física y descartar posibles secuelas del COVID. En cuanto los mismos cuenten con la aprobación médica, comenzarán con los testeos pre viaje para unirse a la burbuja en Rimini.

**VNL - 1º WEEKEND (TyC Sports - TyC Sports Play)**

* 28/5 16hs Argentina vs Brasil
* 29/5 14:30hs Argentina vs Canadá
* 30/5 11hs Argentina vs Estados Unidos