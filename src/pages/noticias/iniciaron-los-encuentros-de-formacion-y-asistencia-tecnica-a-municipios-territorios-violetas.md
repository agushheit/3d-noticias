---
category: Estado Real
date: 2021-10-08T06:00:18-03:00
thumbnail: https://assets.3dnoticias.com.ar/VIOLETA.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Iniciaron los encuentros de formación y asistencia técnica a municipios "Territorios
  Violetas"
title: ' Iniciaron los encuentros de formación y asistencia técnica a municipios "Territorios
  Violetas"'
entradilla: La propuesta abarca a los 365 municipios y comunas de la provincia. La
  agenda se extiende durante todo el mes y está orientada a la formación y asistencia
  técnica a áreas locales de igualdad, género y diversidad.

---
El gobierno de la provincia de Santa Fe, a través del Ministerio de Igualdad, Género y Diversidad, dio comienzo a los encuentros “Territorios violetas”, espacios de formación y asistencia técnica de las áreas locales de Igualdad, Género y Diversidad de los 365 municipios y comunas de la provincia, con asiento en las localidades donde se van a instalar los 40 Puntos Violetas.

“Estos encuentros de formación son la profundización de un trabajo que comenzó con la firma de convenios correspondientes al Programa de Fortalecimiento y/o creación de Áreas Locales de Igualdad que llevamos adelante el mes pasado con más de 200 municipios y comunas”, sostuvo la ministra de Igualdad, Género y Diversidad, Celia Arena. “Este año por primera vez se convocó a las 365 localidades de la provincia a presentar un proyecto institucional con aquellas políticas de género, igualdad y diversidad que, según la especificidad del territorio, buscaban abordar. Aquellas que lo presentaron, hoy reciben recursos económicos y técnicos a partir de esas rúbricas y estos encuentros, que tienen lugar en cada ciudad o comuna donde se emplazará un Punto Violeta, surgen de la necesidad de justamente trabajar en conjunto con las áreas locales de cada zona en la articulación del abordaje de situaciones de violencia por motivos de género, brindar asesoramiento y apoyo para la generación de políticas”, agregó. “De esta manera, y siguiendo lo que solicitó el gobernador Omar Perotti, estamos trabajando codo a codo en cada territorio, con cada gobierno local”, finalizó.

Del lunes al miércoles los encuentros tuvieron lugar en las localidades de Reconquista, Esperanza, Pueblo Andino, Garabato, San Jerónimo Norte, Puerto Gral San Martín, Llambi Campbell y Acebal. El día jueves cerraron la semana los encuentros en Sauce Viejo y Piñero, en el marco de una nutrida agenda programada hasta fines de octubre.

Junto a los equipos de trabajo del Ministerio de Igualdad, Género y Diversidad y de las áreas locales, participaron la subsecretaria de Fortalecimiento Territorial en Igualdad, Género y Diversidad, Soledad Zalazar; la subsecretaria de Políticas de Igualdad y Diversidad, Nerea Tacari, la subsecretaria de Políticas contra la Violencia por Razones de Género, Ornela Grossi y la coordinadora de la Agencia de ATR Juventudes, Milagros Monserrat.

**SOBRE LOS ENCUENTROS**

Cada encuentro consiste en espacios de intercambio, talleres y capacitaciones coordinadas por profesionales de los equipos técnicos del Ministerio de Igualdad, Género y Diversidad con una profunda mirada territorial. Cada jornada tiene como propósito brindar herramientas para el abordaje y la instrumentación de políticas orientadas a erradicar la vulneración de derechos por motivos de género, orientación y/o identidad de género en los ámbitos municipales y comunales de la Provincia de Santa Fe.

En cada encuentro, además se llevan a cabo instancias de comunicación de los distintos programas del ministerio, buscando favorecer la articulación de acciones de prevención, promoción y protección de derechos de las mujeres y personas de la diversidad sexual.