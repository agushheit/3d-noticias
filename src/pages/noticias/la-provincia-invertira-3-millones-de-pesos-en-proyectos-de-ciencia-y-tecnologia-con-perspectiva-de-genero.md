---
category: Estado Real
date: 2021-04-13T08:13:13-03:00
thumbnail: https://assets.3dnoticias.com.ar/ciencia.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: La provincia invertirá 3 millones de pesos en proyectos de ciencia y tecnología
  con perspectiva de género
title: La provincia invertirá 3 millones de pesos en proyectos de ciencia y tecnología
  con perspectiva de género
entradilla: 'La convocatoria tiene el objetivo de promover la equidad de género en
  el ámbito de la ciencia, la tecnología y la innovación. '

---
En el marco del día del Investigador y la Investigadora científica, este sábado se presentaron los 17 beneficiarios de la Convocatoria a Proyectos de Ciencia y Tecnología con Perspectiva de Género. Se trata de una iniciativa del gobierno de la provincia, a través de la Secretaría de Ciencia, Tecnología e Innovación y la Secretaría de Estado de Igualdad de género, por medio de la cual se destinará 3 millones de pesos para fomentar estudios actualizados de la situación de las mujeres y diversidades en los ámbitos de I+D+i con el fin de contribuir a la consolidación de una ciencia con perspectiva de género en Santa Fe.

En ese marco, la secretaria de Ciencia, tecnología e Innovación, Marina Baima, brindó detalles acerca de los objetivos de la convocatoria: “Buscamos promover la equidad de género en el ámbito de la ciencia, la tecnología y la innovación, no sólo mediante al aumento de la participación de mujeres y diversidades en la dirección de proyectos, sino también en la incorporación de la perspectiva de género en los asuntos y abordajes de los ejes temáticos que generen un impacto real en el territorio santafesino.” y agregó: ”Integrar estos enfoques a los contenidos y productos que surjan de los proyectos resulta fundamental para reducir las brechas en el proceso de elaboración del conocimiento, y a su vez, implica una dimensión de calidad para el desarrollo tecnológico y la innovación”.

Por su parte, la secretaria de Estado de Igualdad y Género, Celia Arena afirmó: “Este programa que impulsamos en conjunto con la Secretaría de Ciencia y Tecnología e Innovación del Ministerio de la Producción surge de la convicción que sostenemos desde esta gestión del Gobierno de Santa Fe por la decisión política del gobernador Omar Perotti, de la importancia que reviste para un desarrollo inclusivo que la ciencia y la tecnología tengan perspectiva de género, que las mujeres y las diversidades sexuales accedan en pie de igualdad a espacios de creación, investigación y decisión en estos ámbitos”.

Asimismo, la subsecretaria de Relaciones Institucionales de la Secretaría de Estado de Igualdad y Género, Lorena Battilana, agregó que “este acto está enmarcado en la transversalización de la perspectiva de género en todas las áreas de gobierno”, además felicitó y valoró el esfuerzo realizado por quienes integran los equipos y animó a seguir realizando estas instancias de diálogo fundamentales para el desarrollo de los proyectos.”

Por último, la subsecretaria de Proyectos Científicos, Eva Rueda, sostuvo: “Yo creo que investigar es hacerse muchas preguntas y muchas respuestas. Investigar es trabajar, defender la soberanía del conocimiento. Y en estas preguntas y respuestas las mujeres y las diversidades hemos avanzado muchísimo y nos permiten hoy estar generando este tipo de lineas que lo que hacen es reforzar cada día nuestra lucha desde los distintos espacios para tener cada día una sociedad más justa y más equitativa”.

**CONVOCATORIA**

Cabe señalar que la convocatoria se orienta en acciones positivas para mejorar el lugar de la mujer en la ciencia, aportando herramientas a los equipos liderados por mujeres, y de conformación diversa. Se propone contribuir con la transformación de las instituciones de ciencia, tecnología e innovación de Santa Fe para que sean ambientes propicios para el desempeño y el progreso con igualdad, incorporando la perspectiva de género en los procesos de I+D+i desde su propio diseño, atendiendo dentro de un contexto diverso, las particularidades socio-culturales vinculadas a las investigadoras y emprendedoras jóvenes en relación a la reproducción y a los sistemas de cuidado.

Se impulsan proyectos científicos que transversalicen la perspectiva de género, ya sea mediante la elaboración de datos, indicadores, estadísticas e informaciones diferenciadas por la categoría sexo/género, como mediante la producción de contenidos desde estos enfoques.

Esta línea de financiamiento para la promoción de la igualdad de géneros en los ámbitos de la ciencia, la tecnología y la innovación trabaja con los ejes de: producción, trabajo y desigualdades; ambiente, cambio climático, sustentabilidad, energías renovables; educación, conectividad y acceso a tecnologías; salud y políticas públicas; desarrollo de infraestructura local; subjetividades emergentes, representaciones sociales, visibilización.

**DESTINATARIOS**

Grupos de investigación de instituciones públicas o privadas sin fines de lucro del Sistema de Ciencia, Tecnología e Innovación radicadas en la provincia de Santa Fe. Forman parte de dicho sistema, las universidades públicas y privadas, centros científicos tecnológicos de CONICET (CCT Santa Fe y CCT Rosario), INTA, INTI, INA y aquellas instituciones que explicitan como parte de su objeto social la realización de actividades de investigación científica, desarrollo tecnológico y/o innovación. Organizaciones de la sociedad civil que acrediten actividades de investigación científica, desarrollo tecnológico y/o innovación o que en asociación con instituciones de CTI expliciten la capacidad y el fundamento para desarrollar el proyecto.