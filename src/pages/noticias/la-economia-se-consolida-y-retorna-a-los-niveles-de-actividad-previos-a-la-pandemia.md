---
category: Agenda Ciudadana
date: 2021-10-21T06:00:53-03:00
thumbnail: https://assets.3dnoticias.com.ar/repunte.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: La economía se consolida y retorna a los niveles de actividad previos a la
  pandemia
title: La economía se consolida y retorna a los niveles de actividad previos a la
  pandemia
entradilla: Un Informe del Centro de Estudios para la Producción indicó que "el turismo,
  la gastronomía y los servicios recreativos, que son las ramas más castigadas por
  la pandemia, impulsaron la recuperación de los últimos meses".

---
La actividad industrial mostró en septiembre último una suba de 14,7% frente al mismo mes de 2019, según el índice adelantado de actividad industrial elaborado por el CEP XXI, que destacó que "todas las ramas industriales tuvieron mayor actividad".  
  
En tanto, indicó que "el turismo, la gastronomía y los servicios recreativos, que son las ramas más castigadas por la pandemia, impulsaron la recuperación de los últimos meses", y aseguró que "se empieza a consolidar el sendero del crecimiento de la economía argentina".

![](https://www.telam.com.ar/advf/imagenes/2021/10/617053146915b_1004x1824.jpg)

El Informe de Panorama Productivo del Centro de Estudios para la Producción (CEP XXI), que depende del Ministerio de Desarrollo Productivo, mide la evolución de los principales indicadores de la actividad productiva.  
  
En esta oportunidad también señaló que en septiembre el complejo siderúrgico tuvo el mayor consumo de energía (anticipo de la producción del sector) desde 2011, y se ubicó 14,9% por encima del mismo mes de 2019.  
  
A su vez, las empresas metalmecánicas consumieron 17,4% más de energía que dos años atrás, y tuvieron el mejor septiembre desde 2015.  
  
En tanto, en este mismo mes, la industria automotriz también tuvo un muy buen desempeño con 43.535 vehículos producidos, el mayor valor desde agosto de 2018 y 57,2% mayor al de septiembre de 2019.  
  
El sector manufacturero se expandió 1,4% mensual desestacionalizado en septiembre, con un crecimiento mensual muy generalizado: 13 de 14 ramas se expandieron frente a agosto.  
  
Comparado contra septiembre de 2019 la suba es de 14,7%, y contra el mismo mes en 2020, que había sido el primero en superar los registros de la prepandemia, de un 10,4%. Incluso contra el dato de septiembre de 2018 hay un crecimiento del 8,9%.  
  
Al tomar el acumulado de los primeros nueve meses del año, la industria manufacturera operó 6,1% por encima del mismo período de 2019, y 20,2% por arriba del mismo lapso en 2020.  
  
En septiembre, el 63,4% de las plantas industriales analizadas consumió más energía que en el mismo período de 2019, cifra que alcanza al 64,1% si se compara contra el mismo mes de 2020.

La Asociación de Fabricantes de Cemento Portland registró que los despachos de cemento de septiembre fueron los terceros más altos de la historia para ese mes, sólo por debajo de 2015 y 2017, y 18,3% más altos que en septiembre de 2019.  
  
Por su parte, el informe destacó que "la mejora sanitaria, la campaña de vacunación y la eliminación de la gran mayoría de las restricciones todavía vigentes por la Covid-19 coincidieron con la recuperación de los sectores más golpeados como turismo, gastronomía y servicios recreativos, los cuales empujaron aún más el crecimiento económico argentino en las últimas semanas".  
  
En julio, cuando todavía persistían los efectos de la segunda ola, el conjunto de la actividad económica se encontraba apenas 0,8% por debajo de febrero de 2020, y la mayoría de los indicadores muestra que desde entonces la actividad productiva prosiguió su mejora.  
  
De hecho, por primera vez en 18 meses, en lo que va de octubre la movilidad de las personas, que correlaciona mucho con la actividad comercial y gastronómica, retornó a niveles de la prepandemia.  
  
  
  
A su vez, las búsquedas en Google (que anticipan consumos de las personas) ligadas al turismo, la gastronomía y servicios culturales llegaron a niveles similares a los de la prepandemia en las últimas semanas, dato consistente con lo ocurrido el fin de semana largo del 12 de octubre, que fue el mejor para esa fecha en al menos seis años.  
  
De acuerdo con el estudio del CEP XXI, uno de los sectores que mejor rendimiento evidencia es la industria del software, que lleva doce meses seguidos creando empleo formal y que ya supera en 15.000 puestos los valores de dos años atrás.  
  
En los últimos meses el rubro aceleró el ritmo de crecimiento, con un promedio de 1.400 puestos mensuales generados (+1,1% mensual), y en junio alcanzó el récord histórico de 128.000 puestos formales.  
Otro sector creador neto de puestos de trabajo formales es salud y servicios sociales, que en junio alcanzó su mayor nivel de la historia, con 317.000 puestos formales.  
  
Por otro lado, en septiembre las ventas reales en precios constantes con Ahora 12 alcanzaron los $ 73.184 millones, monto que representó una suba frente a las del mismo mes de 2020 y 2019, de un 13,1% y 4% respectivamente.  
  
De este modo, el tercer trimestre cerró 11,8% por arriba de los de los últimos dos años.  
  
Estos datos van acompañados por los números de las exportaciones, que registraron en agosto el tercer nivel más alto desde que hay registro: crecieron 45,3% en comparación con el mismo mes de 2019 y 63,3% en la medición frente a 2020.  
  
En agosto, en tanto, las importaciones de bienes de capital -termómetro de la inversión productiva- también arrojaron un número positivo: crecieron 6,5% frente al mismo mes de 2019 y 38,3% en comparación con 2020.