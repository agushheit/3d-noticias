---
layout: Noticia con imagen
author: "Fuente: Diario UNO"
resumen: Se salvaron de milagro
category: La Ciudad
title: Un tren embistió a un remis en pasaje Larramendi y San Jerónimo
entradilla: Las dos personas que viajaban lograron salvar providencialmente sus
  vidas. El lugar cuenta con barreras automáticas e investigan cómo se produjo
  el choque.
date: 2020-11-13T17:41:42.309Z
thumbnail: https://assets.3dnoticias.com.ar/remis.jpg
---
Este jueves, antes de las 14, un remisero que viajaba junto a su hija fueron impactados en la parte trasera del vehículo por el tren que habitualmente atraviesa la ciudad y que, cada tanto, genera serios inconvenientes con descarrilamientos peligrosos.

Ambas personas salvaron de milagro sus vidas. El choque ocurrió sobre las vías ferroviarias ubicadas en paralelo a pasaje Larramendi a la altura de San Jerónimo. El incidente es materia de investigación.

Como consecuencia de la embestida, el automóvil resultó dañado severamente en la parte trasera. Los vecinos que escucharon el estruendo de las chapas y hierros retorcidos salieron a la vereda para saber lo que estaba ocurriendo y vieron salir del habitáculo a padre e hija que resultaron con politraumatismos leves.

En tanto, la formación ferroviaria detuvo la marcha.

Luego, llegaron agentes de la 3° Inspectoría Zonal y de la Comisaría 11° (Orden Público) y de la Brigada Motorizada y el Comando Radioeléctrico capitalino (Cuerpos) que inmediatamente dialogaron con el remisero y su hija, quienes dijeron desconocer cómo se produjo el accidente.

Informaron la novedad sobre la ocurrencia del accidente y sus consecuencias a la Jefatura de la Unidad Regional I La Capital de la Policía de Santa Fe, y éstos hicieron lo propio con el fiscal en turno del Ministerio Público de la Acusación, que ordenó la realización de los peritajes criminalísticos de rigor.