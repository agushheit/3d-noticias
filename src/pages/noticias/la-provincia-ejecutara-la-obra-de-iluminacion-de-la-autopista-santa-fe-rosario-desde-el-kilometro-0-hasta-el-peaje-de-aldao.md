---
category: Estado Real
date: 2021-08-24T06:00:17-03:00
thumbnail: https://assets.3dnoticias.com.ar/OBRASAUTO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La provincia ejecutará la obra de iluminación de la autopista Santa Fe –
  Rosario, desde el kilómetro 0 hasta el peaje de Aldao
title: La provincia ejecutará la obra de iluminación de la autopista Santa Fe – Rosario,
  desde el kilómetro 0 hasta el peaje de Aldao
entradilla: 'Con un presupuesto que supera los 628 millones de pesos, la obra incluye
  sendas calzadas en los primeros 22 kilómetros de la traza. '

---
El Ministerio de Infraestructura, Servicios Públicos y Hábitat, a través de la Dirección Provincial de Vialidad, licitará este miércoles, a las 9 horas, en las oficinas administrativas ubicadas en el kilómetro 22 del corredor, las obras de iluminación de la autopista Santa Fe – Rosario, en el tramo comprendido entre avenida Circunvalación de Rosario y la localidad de Aldao.

Al respecto, el administrador general de Vialidad Provincial, Oscar Ceschi, recordó que "en abril licitamos la iluminación de la zona norte, desde el Río Salado hasta la Ruta Nacional 19; y veníamos elaborando este proyecto. Es una alegría poder brindarle mayor seguridad vial a los usuarios de esta arteria fundamental que tiene la provincia".

Además, remarcó: "Desde la gestión de Omar Perotti y la ministra Silvina Frana en la autopista realizamos la repavimentación de varias secciones, y se están ejecutando las obras del intercambiador de acceso norte a Santo Tomé. Tenemos unos 40 kilómetros reconstruidos en la mano a Rosario, recientemente finalizamos los últimos 15 kilómetros desde Sauce Viejo, hasta el ingreso a Santa Fe y ahora estamos trabajando desde allí hacia el sur", concluyó Ceschi.

El proyecto contempla la línea aérea de media tensión, la instalación de transformadores de potencia, luminarias bajo puentes, nuevas columnas metálicas y el repintado de las existentes, entre otras tareas.

De tal manera, se completará el sistema de iluminación con tecnología led en todo el trayecto, que posee los accesos a Granadero Baigorria (kilómetro 4), Capitán Bermúdez (kilómetro 8) y San Lorenzo sur, centro y norte (kilómetros 14, 16 y 19).

Las obras cuentan con un presupuesto oficial de $628.389.920,00 y un plazo de ejecución de 12 meses.