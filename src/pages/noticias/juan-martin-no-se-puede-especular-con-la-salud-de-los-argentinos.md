---
category: Agenda Ciudadana
date: 2020-12-17T11:03:23Z
thumbnail: https://assets.3dnoticias.com.ar/1712-diputado-juan-martin.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: 'Juan Martín: «No se puede especular con la salud de los argentinos»'
title: 'Juan Martín: «No se puede especular con la salud de los argentinos»'
entradilla: Diputados de Juntos por el Cambio elevaron un pedido de informes al Poder
  Ejecutivo Nacional por la falta de seguridad en cuanto a la llegada de las vacunas
  contra el coronavirus.

---
El diputado Juan Martín, sus pares Alfredo Cornejo y Claudia Najul y otros colegas de la UCR, exigieron informes al ministro de Salud Ginés González García ante el reconocimiento del funcionario de que el lote de vacunas del laboratorio Pfizer contra el coronavirus anunciado por el presidente Alberto Fernández no llegará en el plazo anunciado, es decir, antes de fin de año.

“Ginés González García tiene que dar explicaciones de esto que está ocurriendo, porque ahora dice que no sabe si las vacunas llegan cuando hace apenas unos días lo prometió el propio presidente Alberto Fernández”, puntualizó el legislador santafesino. “No vamos a permitir que se especule con la salud de nuestra población, que ya ha tenido que padecer meses de un deficiente manejo de la crisis generada por la pandemia, que no solo ha generado problemas sanitarios, sino además graves consecuencias económicas”.

En el pedido de informes, los diputados se dirigen a Ginés González García para que informe “en forma completa, circunstanciada, oportuna y actualizada” todo lo vinculado a la adquisición y distribución de las vacunas.

En primer término se exige que se detallen “los motivos específicos por los cuales fracasó el acuerdo anunciado por el presidente la semana pasada con el Laboratorio Pfizer”. En otro punto del escrito, se solicita que informe “en forma responsable y fidedigna si la Vacuna contra el Covid 19 llegará a nuestro país antes del fin de 2020, en tanto según sus propias consideraciones la empresa Pfizer ha cambiado las condiciones por la caída de su producción a la mitad de lo que tenía como hipótesis productiva”.

Por otra parte, el proyecto demanda informes claros de “cuántas dosis de vacunas se planifica recibir en la Argentina y en qué plazo. Asimismo, que informe sobre la situación en la que se encuentran las 600.000 dosis que, según las propias declaraciones del gobierno, iban a llegar antes de fin de año al país”.

En el mismo pedido, se le exige a Ginés González García que detalle “los lugares, fechas y sectores de la población con los que se va a comenzar la vacunación” y “qué precio va a pagar la Argentina por las vacunas que se adquieran, y si ya se ha efectuado alguna erogación relativa a la provisión de las mismas”.

Finalmente, se solicitan datos sobre “qué criterios se evaluaron para el ingreso de otras vacunas a la Argentina”. También se le indica al titular de la cartera sanitaria que “informe detalladamente cuáles serán los mecanismos de refrigeración y de logística de las vacunas a las distintas provincias del país, considerando que es imprescindible conservar las dosis a una temperatura de -70 grados”.