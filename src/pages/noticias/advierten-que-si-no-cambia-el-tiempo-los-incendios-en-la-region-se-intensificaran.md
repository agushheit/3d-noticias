---
category: La Ciudad
date: 2021-12-30T06:15:14-03:00
thumbnail: https://assets.3dnoticias.com.ar/incendios.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Advierten que si no cambia el tiempo los incendios en la región se intensificarán
title: Advierten que si no cambia el tiempo los incendios en la región se intensificarán
entradilla: 'Así lo señaló Roberto Rioja, secretario de Protección Civil. Este martes
  hubo focos de incendio en San José del Rincón, Colastiné, Arroyo Leyes, zona de
  El Chaquito y al norte de la ciudad.

'

---
Los incendios azotan la región en los últimos días y este martes no fue la excepción. Agravados por las altas temperaturas y la sequía se siguen incendiando pastizales y hectáreas de tierra en Santa Fe y zona, concentrándose el fuego en diferentes puntos ya identificados.

En diálogo con UNO, el secretario de Protección Civil de la provincia Roberto Rioja indicó que solo durante este martes se identificaron focos de incendio en cinco puntos distintos cercanos a la ciudad de Santa Fe. Uno de los más graves fue el que se desató en Arroyo Leyes, con el avance del fuego poniendo en peligro algunas viviendas de la zona.

Respecto a este incendio y a los demás focos que se identificaron en el lugar, Rioja aseguró que "lo que es la zona de Rincón, Colastiné y Arroyo Leyes está contenido y controlado".

Distinta a la de Arroyo Leyes es la situación de otros incendios que se desataron durante horas de la tarde, sosteniendo Rioja sobre esto que "se está recorriendo la zona de El Chaquito, donde hay otro foco. En la ciudad de Santa Fe se incendiaron unas casas en Misiones al 8000 y están trabajando los bomberos".

Frente al avance de los incendios en distintos lugares del Gran Santa Fe las autoridades coinciden en que "con estas temperaturas y como está todo seco hasta que no cambie el tiempo nos va a tener complicados, por eso nos mantenemos en alerta y trabajando constantemente".

**Emergencia ígnea**

El Concejo Federal de Medio Ambiente (Cofema) declaró la emergencia ígnea en todo el territorio nacional por 12 meses. “La situación se está agravandoy lo que hemos coordinado entre Nación y todas las jurisdicciones provinciales es articular cada detalle para fortalecer las políticas de prevención y el accionar en los territorios afectados”, indicó la ministra de Ambiente y Cambio Climático de Santa Fe, Erika Gonnet.

En la provincia de Santa Fe, hasta ayer se registraron 415 focos de calor según la detección satelital aportada por la NASA a través del sistema Firms. “Se trata de focos de calor, no quiere decir que todos sean incendios, sino que son sectores en situación de alarma”, aclaró Gonnet.

La funcionaria agregó que se está frente a “un escenario complejo, y estamos atravesando una instancia de sequías, los profesionales que nos asesoran nos informan que no se prevén precipitaciones intensas, por lo menos, hasta mediados de enero”.