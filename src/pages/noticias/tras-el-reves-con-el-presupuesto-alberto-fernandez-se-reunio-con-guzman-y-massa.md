---
category: Agenda Ciudadana
date: 2021-12-18T06:00:25-03:00
thumbnail: https://assets.3dnoticias.com.ar/MAFER.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: Tras el revés con el Presupuesto, Alberto Fernández se reunió con Guzmán
  y Massa
title: Tras el revés con el Presupuesto, Alberto Fernández se reunió con Guzmán y
  Massa
entradilla: 'En el encuentro, que se realizó el viernes en la Quinta de Olivos, decidieron
  prorrogar por decreto simple el Presupuesto 2021 para ser aplicado en el ejercicio
  2022.

'

---
El presidente Alberto Fernández se reunió hoy con el presidente de la Cámara de Diputados, Sergio Massa, y el ministro de Economía, Martín Guzmán, para evaluar los detalles del decreto a través del cual se prorrogará la partida presupuestaria vigente, tras el rechazo al Presupuesto  
2022\.

Según confirmaron a NA fuentes oficiales, en el cónclave que se realizó esta tarde en la Residencia de Olivos se analizaron los detalles del decreto simple que extenderá el presupuesto vigente.

El encuentro entre Fernández, Massa y Guzmán se gestó luego de que fracasara en el Congreso de la Nación un acuerdo con la oposición para aprobar el proyecto de Presupuesto que había enviado el Ejecutivo nacional.

Si bien estaba previsto que la reunión trascurriera en horas de la tarde, fuentes parlamentarias precisaron que Massa se acercó hasta la Quinta Presidencial en horas del mediodía para dialogar con el Presidente antes de que se produzca la videoconferencia que Fernández mantuvo con la titular del FMI, Kristalina Georgieva.

La Cámara de Diputados rechazó este viernes el proyecto del Gobierno y dejó abierta la posibilidad de que administre los fondos del estado sin la ley de leyes.

En una sesión que duró más 20 horas, la oposición triunfó por 132 votos a favor, 121 en contra y una abstención en la votación de la ley, celebrada poco antes de las 10:30, provocando una dura derrota política al bloque oficialista.

Según supo Noticias Argentinas, una vez finalizada la maratónica sesión que vetó la partida presupuestaria presentada por el Gobierno, el presidente de la Cámara de Diputados, desde su despacho, estuvo en comunicación con el Presidente, la vicepresidenta Cristina Kirchner y el ministro Guzmán.

Según el Reglamento de la Cámara, el proyecto deberá ser tratado en el periodo legislativo 2022 que comienza el 1º de marzo.

El artículo 81 del reglamento reza: "Ningún proyecto de ley desechado totalmente por una de las Cámaras podrá repetirse en las sesiones de aquel año. Ninguna de las Cámaras puede desechar totalmente un proyecto que hubiera tenido origen en ella y luego hubiese sido adicionado o enmendado por la Cámara revisora".