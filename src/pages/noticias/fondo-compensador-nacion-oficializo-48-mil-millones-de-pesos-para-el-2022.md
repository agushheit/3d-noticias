---
category: Agenda Ciudadana
date: 2022-12-15T08:01:41-03:00
thumbnail: https://assets.3dnoticias.com.ar/colectivos.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: 'Fondo Compensador: Nación oficializó 48 mil millones de pesos para el 2022'
title: 'Fondo Compensador: Nación oficializó 48 mil millones de pesos para el 2022'
entradilla: El Gobierno nacional publicó hoy el decreto 829/2022 que, en materia de
  transporte, extiende el monto del Fondo Compensador al interior del país, oficializando
  la cifra de 48 mil millones de pesos para el 2022.

---
A través de la publicación del decreto 829/2022 en el Boletín Oficial, el Gobierno nacional oficializó hoy la extensión del Fondo Compensador para el transporte automotor del interior del país a 48 mil millones de pesos, cumpliendo con los compromisos asumidos para el corriente año. Así, el Ministerio de Transporte completará el esquema de pago con el objetivo de brindar asistencia económica al sector.

En este sentido, esta normativa permite que el Ministerio de Transporte, a cargo de Diego Giuliano, avance en el esquema de pago restante para el corriente año, con el objetivo de brindar asistencia económica al sector y garantizar mayor conectividad a todos los argentinos y argentinas.