---
category: Estado Real
date: 2021-11-20T06:15:26-03:00
thumbnail: https://assets.3dnoticias.com.ar/controles.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La APSV aumenta los controles en rutas por el finde semana extendido
title: La APSV aumenta los controles en rutas por el finde semana extendido
entradilla: Las acciones previstas tienen como objetivo monitorear la circulación
  vehicular en el territorio santafesino y promover pautas de cuidado.

---
La Agencia Provincial de Seguridad Vial (APSV) reforzará la prevención en los principales corredores de la provincia de Santa Fe de cara al feriado por el Día de la Soberanía Nacional, que provocará mayor flujo vehicular en todas las rutas de la provincia.

Con el objetivo de actuar directamente en la prevención de siniestros viales en esta época del año, el organismo provincial coordinó procedimientos en el territorio provincial en una amplia franja horaria que estarán a cargo de la Policía de Seguridad Vial (PSV).

Además, desde la APSV se reforzarán las recomendaciones para viajes en rutas. Se espera que los conductores adopten conductas de autocuidado para no exponerse a situaciones que puedan generar poner en riesgo la salud.

El subsecretario de la APSV, Osvaldo Aymo, sostuvo que “los controles apuntan a evitar y sancionar aquellas conductas que más inciden en la siniestralidad vial. La presencia del Estado en las rutas buscar desestimar las infracciones por parte de los conductores y, en el caso que ocurran, separar a estas personas de la circulación por representar un riesgo para todos”.

Los puestos de control estarán ubicados en las salidas a las principales rutas de la provincia que los santafesinos toman con destino a los sitios elegidos para el descanso.

**Conciencia vial**

Para evitar siniestros viales, uno de las medidas fundamentales es aumentar la conciencia a la hora de conducir y movernos en distintos tipos de vehículos. Las recomendaciones a los usuarios de las rutas están basadas en respetar las normas generales de circulación y ser prudente.

Además, las recomendaciones focalizan en revisar el estado mecánico general del vehículo y especialmente comprobar los frenos; programar el viaje teniendo en cuenta las personas que viajan, si hay niños o personas mayores, etc., a fin de establecer paradas y tiempos de descanso; no ingerir alcohol; llevar abrochado el cinturón de seguridad y que los niños viajen siempre atrás en los SRI (Sistemas e Retención Infantil) correspondientes a cada edad, entre otras.

La directora de la APSV, Antonela Cerutti, afirmó: “Es importante que los conductores que viajen a destinos turísticos lo realicen con precaución. Se recomienda antes de salir consultar el estado de las rutas y del tiempo, verificar las condiciones vehiculares y la documentación pertinente. Para viajar seguros en familia, en el auto debe haber la misma cantidad de cinturones de seguridad que ocupantes. Si son menores, se debe sujetarlos al sistema de retención infantil”.

**Validez de la prórroga de la licencia de conducir**

En virtud de que durante la pandemia se prorrogó el vencimiento de la licencia de conducir en varios ocasiones la APSV emitió una resolución que habilita a conducir aún cuando este documento se encuentre vencido, siempre y cuando se encuentre dentro de los plazos estipulados por la norma. Para evitar inconvenientes acerca de su validez, si los usuarios deben salir de la provincia de Santa Fe, se recomienda portar la Resolución tanto en formato papel como digital e incluso captura de pantalla. La misma se puede descargar [aquí](http://www.santafe.gov.ar/index.php/web/content/download/265150/1388108/file/RES.%200038%20LC%20prorroga%20OCT.pdf).

**Restricción de camiones**

Este fin de semana largo habrá restricción de camiones de más de 3.500 kilos en 24 rutas nacionales y en los principales accesos de la Ciudad Autónoma de Buenos Aires. La restricción será a partir de este viernes 19 de 18 a 20:59 horas, el sábado 20 de 7 a 9:59 horas y el lunes 22 de 18 a 20:59 horas. La medida fue publicada en Boletín Oficial por la Agencia Nacional de Seguridad Vial (ANSV), organismo del Ministerio de Transporte, y apunta a facilitar el trayecto de las personas que se trasladen con fines turísticos a varios destinos del país y prevenir siniestros viales.

Se puede consultar el listado de rutas y horarios en todo el país [aquí](https://www.argentina.gob.ar/seguridadvial/restricciondecamiones/fechasyhorarios).