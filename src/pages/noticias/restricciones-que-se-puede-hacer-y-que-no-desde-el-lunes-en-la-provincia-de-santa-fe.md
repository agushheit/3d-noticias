---
category: Agenda Ciudadana
date: 2021-05-30T06:15:51-03:00
thumbnail: https://assets.3dnoticias.com.ar/restriccionesmayo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Restricciones: qué se puede hacer y qué no desde el lunes en la provincia
  de Santa Fe'
title: 'Restricciones: qué se puede hacer y qué no desde el lunes en la provincia
  de Santa Fe'
entradilla: '"A partir del lunes la actividad comercial tendrá una apertura parcial",
  dijo el gobernador Omar Perotti.'

---
Este sábado al mediodía el gobernador Omar Perotti anunció la extensión de las restricciones en la provincia de Santa Fe. En el decreto N° 0 735, se informan cambios para los locales gastronómicos que podrán recibir comensales en los locales con un 30% de ocupación hasta las 19; y para el comercio mayorista y minorista que podrá atender al público hasta las 17 en general, y hasta las 19 si son negocios con ventas de alimentos o farmacias. Siguen suspendidas las reuniones familiares o de amistades, la circulación vehicular y actividades recreativas. Seguirán las clases virtuales una semana más.

**Las restricciones hasta el 4 de junio**

Comercio Mayorista y Comercio Minorista Venta de mercaderías / con atención al público

La actividad del comercio mayorista y comercio minorista de venta de mercaderías, con atención al público en los locales, podrá extenderse todos los días de la semana hasta las diecisiete (17) horas.

La extensión hasta las diecinueve (19) horas, será para quienes comercialicen productos alimenticios y farmacéuticos.

El factor de ocupación de la superficie cerrada de los locales, destinada a la atención del público, no podrá exceder del treinta por ciento (30%).

Los locales gastronómicos podrán funcionar entre las 06hs y las 19hs, con un aforo máximo del 30%.

La Caja de Asistencia Social Lotería de Santa Fe para la comercialización de los juegos oficiales, podrá desarrollarse hasta las diecisiete (17) horas; quedando suspendidas las mismas el día 5 de junio de 2021.

**Las restricciones hasta el 6 de junio**

En todo el territorio provincial queda restringida la circulación vehicular en la vía pública, a toda persona que no realice actividades autorizadas, desde las 18 hasta las 6 del día siguiente. Quedan exceptuados de la restricción vehicular quienes cumplan con su actividad laboral o quienes estén debidamente justificadas por una situación de fuerza mayor (incluyendo asistencia médica, farmacéutica y cuidado de personas).

Continúa la suspensión de la actividad comercial en centros comerciales, paseos, shoppings y establecimientos afines. Asimismo, quedan prohibidos los encuentros sociales y familiares en lugares cerrados o al aire libre.

En cuanto a las actividades recreativas, continuarán suspendidas las prácticas de deportes grupales de contacto (al aire libre y en espacios cerrados), y las competencias deportivas provinciales, zonales o locales de carácter profesional o amateur, no habilitadas expresamente por las autoridades nacionales. También el funcionamiento de clubes, gimnasios y otros establecimientos afines, incluidas las actividades al aire libre. Y las actividades náuticas, pesca deportiva y recreativa en modalidad costa y embarcados, actividades de los clubes deportivos vinculados a las mismas y de guarderías náuticas.

También desde el gobierno provincial invitan a los Poderes Legislativo y Judicial y a los Municipios y Comunas de la Provincia, a priorizar en relación al personal de su dependencia la modalidad de prestación de servicios en forma remota o teletrabajo, en las áreas donde ello fuera posible.

**Suspensión de las siguientes actividades:**

Ejercicio de profesiones liberales, incluidos mandatarios, corredores y martilleros, en forma presencial y con atención al público en los locales y oficinas; quedando excluidos de la suspensión, los profesionales de la salud y los del notariado, para realizar diligencias esenciales e impostergables y los profesionales del derecho, en el caso de deber instar medidas de urgente diligenciamiento que no puedan realizarse de manera remota y cuya demora tenga virtualidad de producir la pérdida de un derecho.

Actividad inmobiliaria y aseguradora, resguardando en este último caso, guardias para la recepción de denuncias de siniestros, en la medida que no puedan realizarse de manera remota. Actividades administrativas en forma presencial de sindicatos, entidades gremiales empresarias, cajas y colegios profesionales, administración de entidades civiles y deportivas, y la actividad administrativa de universidades nacionales y privadas con sedes en el territorio provincial; quedando expresamente excluida de la suspensión el funcionamiento de obras sociales, en aquellas actividades que no pudieren realizarse en forma remota o mediante teletrabajo.

Actividades administrativas de las empresas industriales, de la construcción, comerciales o de servicios, las que deberán desarrollarse en forma remota o mediante teletrabajo.