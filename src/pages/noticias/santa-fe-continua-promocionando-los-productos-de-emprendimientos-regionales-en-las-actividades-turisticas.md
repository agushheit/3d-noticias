---
category: Agenda Ciudadana
date: 2021-02-11T06:39:28-03:00
thumbnail: https://assets.3dnoticias.com.ar/turismo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: Santa Fe continúa promocionando los productos de emprendimientos regionales
  en las actividades turísticas
title: Santa Fe continúa promocionando los productos de emprendimientos regionales
  en las actividades turísticas
entradilla: La iniciativa se presentó en la ciudad capital y busca promover el consumo
  de productos gastronómicos elaborados por emprendedores en hoteles locales.

---
Los secretarios de Turismo de Santa Fe, Alejandro Grandinetti, y de Desarrollo Territorial y Arraigo, Fabricio Medina, presentaron en la ciudad de Santa Fe la campaña de promoción de productos regionales, que tiene por objetivo vincular a los emprendedores locales con los hoteles santafesinos, quienes recibirán de la provincia una caja de productos con un catálogo que indicará dónde adquirirlos.

Del lanzamiento, llevado a cabo en el salón Amarillo del Ministerio de Producción, Ciencia y Tecnología, participaron también el presidente de la Asociación Empresaria Hotelera Gastronómica de Santa Fe, Rodolfo Verde; en representación de la Agencia para el Desarrollo de Santa Fe y la Región, Mario Galizzi; y demás integrantes del sector.

Durante el encuentro, Grandinetti señaló que el objetivo es “que el turismo derrame en la mayor cantidad de actores económicos y economías regionales. Cuando hablábamos con el gobernador Omar Perotti, desde el comienzo de la gestión, de poner en marcha uno de los grandes motores de la economía santafesina, apuntábamos a este tipo de iniciativas. Las noticias que nos marcan entre los diez destinos más elegidos de la temporada en Argentina dan cuenta que se está poniendo en marcha. Queremos que los beneficios que trae aparejado esta actividad llegue a puntos del territorio que no son turísticos, para ello es que impulsamos esta experiencia”.

“Apostamos al sentimiento solidario de las instituciones, ya que sabemos que se les hace cada vez más difícil y hoy les pedimos que nos ayuden a promocionar estos productos regionales, que tienen una enorme calidad, con un sello de la provincia”, agregó el funcionario.

Por su parte, Medina detalló que “presentamos una caja de productos que están bajo la marca De Mi Tierra Santa Fe, conformada por pequeñas PyMes agroalimentarias de la región, que a lo largo de la pandemia no pudieron hacer sus ventas en canales tradicionales de ferias y eventos”.

En este sentido, el secretario de Desarrollo Territorial resaltó que “la idea es que cada turista que venga a la provincia tenga la posibilidad de ver lo que hacen los pequeños productores, son alimentos que no van a encontrar en la góndola de ningún supermercado, y que hoy están promocionados y apoyados por un ciclo de ferias que se llama Orígenes Santafesinos”.

A su turno, Rodolfo Verde celebró la iniciativa y señaló que “es algo muy importante, ya que se van a mostrar los productos de la provincia de Santa Fe e incentivar a que la gente pueda saber sobre cada uno de ellos”.

Finalmente, Galizzi manifestó que “desde la agencia estamos muy contentos de promover estos productos en circuitos que no son tradicionales y que van a generar mano de obra”.

**CAJAS DE PRODUCTOS DE MI TIERRA**

‘Productos de mi Tierra’ es una marca colectiva que otorga el Estado provincial a las producciones agroalimentarias destacadas, realizadas por micro y pequeñas agroindustrias, así como también de la economía social, teniendo por objetivo principal promocionar y potenciar el trabajo de los productores santafesinos y santafesinas.

Las cajas contienen entre 5 o 6 productos de licenciatarios de la marca y, entre las elaboraciones, se destacan alfajores santafesinos, cerveza artesanal, dulce de leche, mermeladas, miel y escabeches. Además, se adjunta un folleto donde figuran las direcciones de los licenciatarios, de manera que si el hotel o un turista desea comprar el producto tiene la información necesaria.

![](https://assets.3dnoticias.com.ar/productos.jpeg)