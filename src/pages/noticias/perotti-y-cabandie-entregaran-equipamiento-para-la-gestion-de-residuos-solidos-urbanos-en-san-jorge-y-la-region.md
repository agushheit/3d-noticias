---
category: Estado Real
date: 2021-03-29T06:00:47-03:00
thumbnail: https://assets.3dnoticias.com.ar/perotti.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Perotti y Cabandié entregarán equipamiento para la gestión de residuos sólidos
  urbanos en San Jorge y la región
title: Perotti y Cabandié entregarán equipamiento para la gestión de residuos sólidos
  urbanos en San Jorge y la región
entradilla: A través de una gestión del gobierno de Santa Fe, el “Consorcio Girsu
  Microregión 2D”, accedió a un financiamiento del Banco Interamericano de Desarrollo.

---
El gobernador de la provincia, Omar Perotti, y el ministro de Ambiente y Desarrollo Sostenible de la Nación, Juan Cabandié, entregarán equipamiento para la gestión de residuos sólidos urbanos al Consorcio “Girsu Microregión 2D”, ubicado en la ciudad de San Jorge. La actividad se realizará mañana, a las 13:30, en la municipalidad de San Jorge. También estarán presentes la ministra de Ambiente y Cambio Climático de la provincia, Erika Gonnet; la senadora del departamento San Martín, Cristina Berra, el senador del departamento Castellanos, Alcides Calvo y; el intendente local, Enrique Marucci.

A través de una gestión del gobierno de Santa Fe, el “Consorcio Girsu Microregión 2D” accedió a un financiamiento del Banco Interamericano de Desarrollo para la adquisición de equipamiento para la gestión de residuos sólidos urbanos. El consorcio está integrado por 12 localidades de los departamentos San Martín y Castellanos, y generará beneficios directos a más de 150.000 santafesinas y santafesinos.

El equipamiento recibido consiste en: retroexcavadora sobre oruga, pala cargadora, minicargadora, autoelevador, camión volcador, camión caja cerrada y carettón; para la operación del basural a cielo abierto y la planta de tratamiento. Además, para fortalecimiento de recicladores, se adquirieron 10 puntos verdes, enfardadora, chipeadora y balanza para camión.

Cabe recordar que, en septiembre de 2020, provincia y nación firmaron un convenio, correspondiente al Plan Federal de Erradicación de Basurales a Cielo Abierto, que incluía al Consorcio GIRSU Microregión 2D, con base en la ciudad de San Jorge.

**Financiamiento**

El proyecto recibe financiamiento por parte del Banco Interamericano de Desarrollo (BID), en el marco de los lineamientos del Plan Federal de Erradicación de Basurales a Cielo Abierto, que el Ministerio de Ambiente Nacional despliega para fortalecer las capacidades provinciales y locales en materia de gestión integral de residuos sólidos urbanos.

Sumado a esto, a través del gobierno provincial, se está gestionando un aporte no reintegrable para la ampliación del espacio cerrado con el que cuenta el complejo ambiental, y se entregó ropa de trabajo y elementos de seguridad a los 15 integrantes de la cooperativa que presta servicio en el lugar.