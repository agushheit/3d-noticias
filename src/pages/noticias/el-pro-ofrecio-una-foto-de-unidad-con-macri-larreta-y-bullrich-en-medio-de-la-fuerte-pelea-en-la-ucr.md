---
category: Agenda Ciudadana
date: 2021-12-07T06:15:15-03:00
thumbnail: https://assets.3dnoticias.com.ar/PRO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: El PRO ofreció una foto de unidad con Macri, Larreta y Bullrich en medio
  de la fuerte pelea en la UCR
title: El PRO ofreció una foto de unidad con Macri, Larreta y Bullrich en medio de
  la fuerte pelea en la UCR
entradilla: 'Los líderes del partido se mostraron junto a los diputados y senadores
  salientes y entrantes. Definieron a las autoridades para el Congreso y brindaron
  un gesto político luego de la ruptura del bloque de la UCR.

'

---
El PRO celebró este lunes, una cumbre partidaria en la que avanzó con la definición de las autoridades parlamentarias del partido y, principalmente, mostró un gesto de unidad entre sus principales referentes luego de las tensiones que afloraron en los días posteriores al triunfo electoral de la oposición y en medio de la crisis interna del radicalismo.

La reunión se llevó a cabo en el Yacht Club Olivos, a donde asistieron el ex presidente Mauricio Macri, el jefe de Gobierno porteño, Horacio Rodríguez Larreta, y la presidenta del PRO, Patricia Bullrich, además de los diputados y senadores entrantes y salientes.

Con una foto que mostró juntos a los "duros" y los "moderados" del partido ("halcones" y "palomas"), los referentes del PRO destacaron su "compromiso de unidad" y brindaron un gesto de pacificación interna pese a las pulseadas entre los distintos sectores y luego de que se dividiera el bloque de diputados nacional de la UCR.

Durante el encuentro, el partido ratificó al bonaerense Cristian Ritondo como presidente de la bancada de diputados del PRO y al misionero Humberto Schiavoni como jefe del bloque de senadores, a la vez que se avanzó en el acuerdo respecto de las autoridades que designarán para cada Cámara.

En ese sentido, el mendocino Omar de Marchi -cercano a Macri y parte del grupo de los "halcones"- continuaría como vicepresidente primero de Diputados mientras que la porteña Guadalupe Tagliaferri -parte de las "palomas" de Rodríguez Larreta- podría ir a la vicepresidencia segunda del Senado en reemplazo de la cordobesa Laura Rodríguez Machado, según señalaron a NA fuentes partidarias.

De esta manera, el partido amarillo dio por cerradas las disputas que se habían abierto luego del triunfo de Juntos por el Cambio en las elecciones legislativas de noviembre pasado, cuando el sector de Bullrich planteó la necesidad de un cambio en el perfil político del PRO y puso en duda la continuidad de Ritondo.

Luego de negociaciones y varias reuniones, Bullrich salió a respaldar públicamente a Ritondo para bajar la tensión y posteriormente mantuvo un encuentro con Rodríguez Larreta, quien también tuvo conversaciones con Macri a fin de pacificar al partido.

"Todos juntos, en el Encuentro Federal de Legisladores Nacionales y Autoridades de PRO, ratificamos el camino elegido: el de representar las causas de los ciudadanos, la cultura del trabajo y el esfuerzo para salir adelante", expresó Bullrich luego de la cumbre.

En la misma línea, Rodríguez Larreta destacó: "Juntos y unidos para construir el país que merecemos los argentinos. Desde el PRO despedimos el año haciendo un balance de todo lo que pasó y todo lo que tenemos por delante.

La unidad de Juntos por el Cambio está más viva que nunca". De esta manera, los protagonistas principales de las últimas pujas internas del PRO buscaron poner de relieve la unidad partidaria dentro de la coalición opositora, en momentos en que sus socios del radicalismo atraviesan una dura pelea.