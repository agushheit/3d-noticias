---
category: La Ciudad
date: 2021-08-11T06:15:39-03:00
thumbnail: https://assets.3dnoticias.com.ar/MercadoNorte.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Municipalidad abrió una convocatoria para ocupar locales comerciales en
  el Mercado Norte
title: La Municipalidad abrió una convocatoria para ocupar locales comerciales en
  el Mercado Norte
entradilla: 'Se trata de cinco puestos en total: tres instalados en el interior del
  espacio comercial, a los que se suman dos más, situados sobre el exterior. Los interesados
  tienen tiempo para presentarse hasta el 31 de agosto.'

---
El Ente Autárquico Municipal del Mercado Norte informa que se encuentra abierta la convocatoria pública para aquellos interesados en ocupar locales correspondientes al paseo comercial. En este caso, se trata de tres puestos ubicados en el interior del Mercado (N° 25A, 31 y 32), y dos más (N° 9 y 10), situados al frente del mismo, con acceso independiente.

Respecto de los que se ubican dentro del espacio comercial, la convocatoria es sin designación de rubro. Para los que están en el exterior, en tanto, se privilegian emprendimientos gastronómicos. La presidenta del Ente, María Pía Chiappero, indicó que “la intención es evitar rubros competitivos dentro del Mercado”, por lo cual aclaró que “si bien ya hay varios rubros, nos quedan todavía muchas opciones para incorporar”.

Chiappero agregó que “tenemos muchas ganas de volver a ocupar los locales que están deshabitados, por lo que ofrecemos facilidades”. Además, recordó que el espacio comercial está ubicado en una zona privilegiada de la capital provincial, en el área del macrocentro, que presenta gran circulación de personas: “Entendemos que es una propuesta interesante y accesible”, concluyó.

Las bases y los formularios de presentación se encuentran en la página web de la Municipalidad de Santa Fe [https://santafeciudad.gov.ar/listado-convocatorias/.](https://santafeciudad.gov.ar/listado-convocatorias/. "https://santafeciudad.gov.ar/listado-convocatorias/.") En todos los casos, los alquileres se extienden hasta diciembre de 2022 y existe la posibilidad de renovación.

La convocatoria pública permanecerá abierta hasta el 31 de agosto, a las 12 horas, y se realiza en sobre cerrado, extendido a la propia administración del paseo comercial, en Santiago del Estero 3166. Aquellos interesados en formar parte de la familia del Mercado Norte, pueden comunicarse por cualquiera de las vías para evacuar dudas o realizar consultas: personalmente, en Santiago del Estero 3166; por teléfono, al 4573073; o vía correo electrónico, a la casilla [administracion@mercadonortesf.com.ar](mailto:administracion@mercadonortesf.com.ar)