---
category: La Ciudad
date: 2021-08-16T06:15:20-03:00
thumbnail: https://assets.3dnoticias.com.ar/396892_1.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: El edificio de la URI está sentado sobre un polvorín fuera de regla
title: El edificio de la URI está sentado sobre un polvorín fuera de regla
entradilla: La situación es una amenaza real y concreta para los cientos de policías
  que allí trabajan; para los vecinos y para la escuela Pedro Lucas Funes, ubicada
  frente al lugar del siniestro.

---
Negligencia, impericia o ignorancia. El inmueble donde funciona la sede de la Unidad Regional I (en Urquiza al 700), en la zona sur de la ciudad de Santa Fe, está sentado sobre una verdadera bomba de tiempo.

Así se desprende de un informe elaborado por peritos de la Policía Federal Argentina, que inspeccionaron el lugar tras el siniestro ocurrido la mañana del viernes y que dejó dos agentes policiales heridos de gravedad y otros dos con lesiones de menor consideración.

Los expertos pertenecen a la Dirección General de Explosivos y Riesgos Especiales de la PFA y llegaron procedentes de Buenos Aires, horas después del desastre.

Al cabo de algunas evaluaciones, ya el día sábado los investigadores de la PFA tomaron las primeras decisiones: se establecieron medidas de seguridad y se procedió a resguardar y clasificar el material explosivo que se halló dentro del recinto y no fue afectado.

**Material inestable**

En otra parte del informe las pesquisas hacen constar que en el lugar del siniestro se hallaron elementos sumamente inestables, tales como cuñetes de ácido pícrico; detonadores y demás elementos sensibles, los que resultan incompatibles y de alto riesgo para su traslado, manipulación y/o almacenamiento.

Cabe consignar que el trinitrofenol (TNP), también denominado ácido pícrico, es un explosivo que se utiliza como carga aumentadora para hacer explotar algún otro explosivo menos sensible como el TNT. El trinitrofenol tiende a formar sales de picrato que son peligrosas e inestables.

**Conclusión preocupante**

Por último, los expertos, cursaron una nota al jefe de Asuntos Internos de la Policía de Santa Fe, a los efectos de pedir autorización para realizar la destrucción controlada del material hallado habida cuenta de tres condiciones observadas:

La primera es que, a raíz de la explosión y el posterior trabajo de los bomberos en la extinción de las llamas, un gran volumen de pirotecnia se halla mojado y en mal estado de conservación.

La segunda es que existe material sensible que amerita su destrucción inmediata.

Por último, ninguna de las fuerzas de seguridad locales cuenta con un polvorín normado para el resguardo del material que se encuentra en buenas condiciones.

**Casi tres toneladas de pirotecnia**

A media mañana del viernes Santa Fe se vio conmovida por una seguidilla de estallidos que provenían de la zona sur de la ciudad.

Tras varios minutos de incertidumbre se logró saber qué pasaba. Los estallidos eran parte de las casi tres toneladas de material de pirotecnia (en rigor 2.800 kilos) que estaba almacenada en un depósito de la Brigada de Explosivos.

Minutos después del incidente ocurrido en la Brigada de Explosivos, el subsecretario de la Agencia de Prevención de Violencia con Armas de Fuego, Lautaro Sappietro, hizo declaraciones al respecto.

"Estamos acá en la Unidad Regional I donde ha ocurrido una explosión en torno a 2.800 kilos de pirotecnia que habían sido secuestrados y estaban incautados por parte de la policía y el municipio. Este material corresponde a operativos realizados durante el año pasado", comentó.

Más adelante el funcionario reveló que "ese material estaba a la espera que entre fiscales, jueces y municipio se decida el lugar donde iba a ser desnaturalizado".

Por su parte la fiscal del área de Delitos Complejos del Ministerio Público de la Acusación, doctora Laura Urquiza, fue impuesta de todas estas novedades y es quien comanda la investigación de este grave suceso.