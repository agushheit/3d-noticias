---
category: Agenda Ciudadana
date: 2021-02-04T03:42:38-03:00
thumbnail: https://assets.3dnoticias.com.ar/jardines_municipales.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Leonardo Simoniello
resumen: 'Jardines maternales particulares: Un primer paso que debe marcar un camino'
title: 'Jardines maternales particulares: Un primer paso que debe marcar un camino'
entradilla: La legislatura dio sanción a una modificación al Código Fiscal de la Provincia
  donde asimila los Jardines maternales particulares a los establecimientos privados,
  incorporados a los planes de enseñanza oficial.

---
La legislatura dio sanción a una modificación al Código Fiscal de la Provincia en donde asimila a los Jardines maternales o particulares _"a los establecimientos educacionales privados, incorporados a los planes de enseñanza oficial, y reconocidos como tales por las respectivas jurisdicciones."_

En este sentido deseo reivindicar la participación del diputado Joaquín Blanco en su intervención, más allá de que un proyecto similar ya había sido presentado por senadores del Frente Progresista y el Senador de Rosario. La norma, condiciona a que estén _"los jardines maternales y/o de infantes de gestión privada y/o particular debidamente habilitados para el ejercicio de la actividad, con planes pedagógicos incorporados a sistemas Municipales o Comunales de Educación Inicial”._

Este avance en materia tributaria es la primera norma en la Provincia que pone a los Jardines Maternales –que cumplan con lo dispuesto- en un lugar claramente destinado a Instituciones Educativas. Mucho se ha hablado en todo este tiempo, -he particularmente hablado- acerca de la función que desarrollan los Jardines y dada esta sanción, que jerarquiza su actividad y los coloca en lo que es para mí un lugar correcto, entiendo que puedan existir otras opiniones que devienen generalmente de algunas situaciones que me gustaría destacar.

Los jardines tienen una larga historia transitando ya más de 100 años. Asimismo, pese a los avances organizacionales, legislativos y conceptuales, existe un imaginario -en algunos casos- en donde aún son solo instituciones de cuidados, guarda y sociabilización.

Es verdad que este avance conceptual e interdisciplinario posiblemente no se refleja en todas las instituciones del Sector. Seguramente en algunas localidades o casos muy particulares aún persiste el viejo concepto citado y tal vez no todos estén verdaderamente adecuados a estos nuevos tiempos.

También es real que no todas las ciudades o comunas se han involucrado legislativamente o conceptualmente en entender que estos establecimientos no son un comercio más, y que requieren de controles extremos en materias que no solo hacen a la seguridad física de los niños o niñas, sino que necesariamente deben avanzar en los contenidos, planes y fundamentalmente en la calidad de los profesionales a cargo.

La Provincia de Santa Fe carece de una Ley de Educación pero también ha carecido -como muchas otras- de la voluntad política de trabajar por los conceptos que expresa la Ley Nacional de Educación que claramente declara a la educación como un proceso que se inicia a los 45 días de vida. Ni provincia ni Nación han hecho demasiados esfuerzos por incorporar o reconocer de alguna manera a los Jardines, así sea conformando un Registro y esbozando algún criterio pedagógico en donde se referencien.

Sin embargo, y más allá de la obligatoriedad del inicio al sistema educativo previsto desde los 4 años, la Provincia cuenta con instituciones educativas dependientes del Ministerio de Educación, reconocidas y debidamente registradas, desde donde también se pretende atender este sector. Obviamente, al no ser obligatoria la presencia de los niños allí, la asistencia se determina por la voluntad de la familia de mandarlos o no, y básicamente con la suerte de los padres de encontrar lugar, ya que la demanda es muchísima y la oferta muy escasa. Estos Jardines sí tienen planes pedagógicos, profesionales a cargo, y están reconocidos por el Ministerio a tal punto que muchos de ellos celebran convenios con establecimientos de los mal llamados preescolares o inclusive primarios.

Entonces, ¿Qué pasa con las familias que deciden mandar a sus hijos a un jardín, convencidos de que son lugares de aprendizaje además de cuidados y sociabilización? La respuesta es clara. Posiblemente algunos encuentran lugar en los públicos reconocidos y los que no tienen esa suerte y pueden, los incorporan a Jardines Particulares maternales. Claramente el servicio es –o debería ser- el mismo. Los contenidos y los cuidados de la misma calidad y los resultados en el desarrollo y aprendizaje en los educandos similares. Pero el Estado debe garantizarlo, supervisarlo y además, alentarlo ya que todos los niños y niñas tienen el mismo derecho.

Es por ello que quiero revindicar este paso. La ciudad de Santa Fe –que es pionera en la materia- alguna vez comenzó de la misma manera y nos fue muy bien. Ahora bien, el recorrido en la ciudad se hizo bajo una idea, un concepto que también es necesario destacar e imitar por parte de los actores involucrados. Deberán ser los propios Directores de Jardines quienes se sientan incorporados al Sistema Educativo más allá de todas las incomprensiones existentes. Los Profesorados tendrán que asignar un espacio fundamental a sus educandos hacia este concepto, y las profesionales a cargo de los niños o niñas deberán tratarse como tal y ser tratados como tal para que todo el cuerpo docente y directivo comience a caminar para las generaciones que vengan el inexorable destino de ser parte de un Sistema Educativo que hoy los menciona, pero aun no los ha tenido en cuenta.

El Estado Provincial y los municipales o comunales no podrán mirar para otro lado. Deben asumir que más allá de las situaciones a resolver, los Jardines Particulares maternales brindan un servicio educativo y no deben ser tratados como un comercio más. Que allí los niños aprenden, los docentes enseñan y es a esas Instituciones a donde los padres decidieron llevarlos. Y básicamente, que este servicio debe ser de calidad, regulado y universal.

No se defiende a la educación ni a la seguridad de nuestros hijos cuando se trata a un Jardín como a un comercio más. Seguramente hay y habrá muchas complicaciones a afrontar pero habrá que hacer mucho, buscando la imaginación y consensos y pensar definitivamente en nuestros niños que a esa edad claramente se educan y aprenden cuando la familia elige una Institución.