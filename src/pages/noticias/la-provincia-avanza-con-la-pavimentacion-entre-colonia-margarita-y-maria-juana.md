---
category: Estado Real
date: 2021-10-25T06:00:40-03:00
thumbnail: https://assets.3dnoticias.com.ar/pavimento.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Provincia avanza con la pavimentación entre Colonia Margarita y Maria
  Juana
title: La Provincia avanza con la pavimentación entre Colonia Margarita y Maria Juana
entradilla: La obra sobre la ruta transversal cuenta con una inversión de más de 900
  millones de pesos y terminará la conexión entre la Ruta Nacional N°34 y la Provincial
  N°13.

---
El Ministerio de Infraestructura, Servicios Públicos y Hábitat a través de la Dirección Provincial de Vialidad avanza con la ejecución de la pavimentación de la Ruta Provincial N°63, entre las localidades de Colonia Margarita y María Juana, zona sur del departamento Castellanos.

Las obras, licitadas el último 17 de mayo, constan de la pavimentación de 10.242 metros, comenzando en el fin del trayecto pavimentado de 7.800 metros entre San Vicente y Colonia Margarita, inaugurado durante la misma jornada en presencia del gobernador Omar Perotti. De tal manera, se completará la vinculación transversal pavimentada entre la Ruta Nacional N°34 y la Ruta Provincial N°13.

Sobre las obras, el administrador general de la Dirección Provincial de Vialidad, Oscar Ceschi, destacó: "Con una enorme determinación del gobernador Omar Perotti y la ministra Silvina Frana venimos desarrollando nuestro plan de obras en toda la provincia. Son caminos transversales vitales para los vecinos y productores que permiten conectar dos rutas muy importantes para la región, sin tener que desviarse hasta la Autovía 19 para tener pavimento".

Con un contrato sucripto por $900.034.602,90 los trabajos están a cargo de la Unión Transitoria conformada por las firmas Obring S.A., Edeca S.A. y Laromet S.A.; teniendo como plazo para la completa terminación de 12 meses.

Entre las primeras tareas, se efectúan intervenciones sobre los laterales del camino con el objetivo de ampliar el ancho de camino a 50 metros, con el retiro y la reconstrucción de alambrados en el trayecto.

Cabe destacar, que el proyecto incluye la construcción de un enlace a través de isletas separadoras en la intersección de las rutas provinciales 13 y 63 así como de la 13 y 48-s, en el acceso a Garibaldi, con las correspondientes iluminaciones y defensas metálicas; la readecuación de líneas eléctricas de media y baja tensión y la construcción de alcantarillas transversales y laterales, entre otros ítems.