---
category: Agenda Ciudadana
date: 2021-01-21T08:57:44Z
thumbnail: https://assets.3dnoticias.com.ar/construccion.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: El costo de la construcción fue del 42,3% en 2020 pero el rubro materiales
  se disparó hasta el 64,4%
title: El costo de la construcción fue del 42,3% en 2020 pero el rubro materiales
  se disparó hasta el 64,4%
entradilla: El costo de la construcción aumentó 3,4% en diciembre último respecto
  de noviembre, con lo que acumuló durante 2020 un incremento del 42,3%, informó el
  INDEC.

---
De acuerdo con la información proporcionada por el instituto nacional, el índice se ubicó de ese modo por encima de la inflación, que según datos oficiales fue del 36,1% en 2020.

 El aumento de diciembre fue producto de la suba del rubro Materiales, con 6,2%; Gastos generales, 3,2%; y Mano de obra, 0,8%.

 En todo el año los costos subieron 64,4% en Materiales, 35,7% en Gastos Generales y 27,1% en Mano de Obra.

 En diciembre, el aumento de los precios de los insumos de la construcción se ubicó levemente por encima de la inflación del mes del 4%, pero en todo el año la superó en 6,2 puntos porcentuales respecto del alza del 36,1% del índice de Precios al Consumidor (IPC).

El capítulo "Mano de obra" correspondiente al último mes del año registra un alza del 0,8%, que surge de la baja de 0,7% en "Mano de obra asalariada" y del alza del 8,4% en "Subcontratos de mano de obra".

 El costo de construcción por metro cuadrado de edificios de departamentos se incrementó 3,7% en diciembre respecto de noviembre un 3,7%, mientras que de viviendas familiares creció 4,4%.

 Ambos indicadores se desaceleraron en forma importante después del 10,7% de noviembre para departamentos y del 15,5% para viviendas unifamiliares.

En todo el año los aumentos interanuales más significativos en el costo de los insumos fueron liderados por el rubro carpintería metálica y herrería, con 76,4%.

Le siguieron, en la misma comparación, la carpintería de madera con 63,6%, la instalación sanitaria y contra incendio con 56,8%, la de gas con 51,6%, vidrios 51,9%, pintura 45,2% y electricidad con 44,8%, todos muy por encima de la inflación minorista.

Más cerca de la variación del IPC anual se ubicaron el movimiento de tierra, con 36,8%, estructuras con 35,9%, albañilería 40,6%, ascensores 30,5% y yesería 20,9%.

 

**Precios mayoristas, apenas por debajo de la inflación**

Los precios mayoristas subieron 35,4% en el 2020, levemente por debajo del costo de vida anual, que fue del 36,1%, informó el Indec. En cambio, en diciembre subieron 4,4%, por encima de la inflación del 4%.

La suba en el Índice de Precios Internos al por Mayor (IPIM) de diciembre se explicó por un alza del 4,4% en productos nacionales y 3,9% en importados.

El incremento anual de los precios mayoristas se ubicó apenas por debajo de la inflación, que fue del 36,1%.

El índice de Precios Internos Básicos al por Mayor (IPIB) mostró un aumento del 4,5% y el del Productor (IPP) del 4,8%.

 El aumento del IPIB de diciembre fue consecuencia del alza del 4,5% en productos nacionales y del 3,9% en importados.

En los últimos doce meses este indicador registra un aumento del 38,6%, dos puntos porcentuales por encima de la inflación anual.

El aumento del IPP en el último mes del año se produjo por el impacto de la suba del 6,9% en productos primarios y 3,6% en manufacturados y energía eléctrica.

Los precios al productor aumentaron 40,3% en el 2020, también por encima de alza de precios minoristas.

En diciembre, respecto de noviembre, los precios mayoristas que más subieron fueron los del tabaco, 8,1%; impresiones y reproducciones de grabaciones, 7,1%; maquinarias y productos eléctricos, 5,2%; refinación de petróleo, 5%; cueros y marroquinería, 4,9%; productos y sustancias químicas, 4,1%; y artículos de madera, 3,8%.

También subieron los precios de insumos para la industria de la construcción, 4,6%; productos de caucho y plásticos, 3,3%; aparatos de radio y televisión, 2,7%; y aparatos para medicina, 2,3%; entre otros.