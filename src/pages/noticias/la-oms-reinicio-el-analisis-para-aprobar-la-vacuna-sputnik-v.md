---
category: Agenda Ciudadana
date: 2021-10-22T06:00:16-03:00
thumbnail: https://assets.3dnoticias.com.ar/SPUTNIK.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: La OMS reinició el análisis para aprobar la vacuna Sputnik V
title: La OMS reinició el análisis para aprobar la vacuna Sputnik V
entradilla: Hasta la fecha, la organización incluyó en la lista de fármacos aprobados
  para su uso "de emergencia" los de Moderna, Pfizer/BioNTech, Johnson&Johnson, las
  dos de AstraZeneca, la India (llamada Covishield) y la coreana.

---
La Organización Mundial de la Salud (OMS) reinició este jueves el análisis de la Sputnik V y realizará nuevas inspecciones en las plantas donde se fabrica la vacuna con el objetivo de reunir las pruebas suficientes para incluirla en la lista de fármacos aprobados para su uso de emergencia.  
  
Así lo confirmó la asistente del director general de la OMS, Mariangela Simao, al asegurar que "se reinició a partir de hoy" el proceso de aprobación “de emergencia” del fármaco desarrollado por el Centro Nacional de Investigación de Epidemiología y Microbiología Gamaleya.  
  
“Esperamos poder volver a realizar las inspecciones en las próximas semanas", agregó Simao sobre la evaluación que se había detenido tiempo atrás por procedimientos legales, según informó la agencia de noticias Sputnik.  
  
A principios de octubre, la OMS anunció que reanudaba la revisión de la vacuna y el director del Fondo Ruso de Inversión Directa (RDIF), Kiril Dmítriev expresó su confianza en que se conseguirá el aval de la agencia sanitaria..  
  
Una semana antes, el ministro de Salud de Rusia, Mijaíl Murashko, afirmó al término de una reunión con el jefe de la OMS, Tedros Adhanom Gebreyesus, que "se removieron todas las barreras" para el reconocimiento del inmunizante.

  
**La Sputnik V**

Desarrollada por Gamaleya y con la financiación del RDIF, fue registrada en Rusia en agosto de 2020 y es uno de los pocos fármacos del mundo con una eficacia superior al 90% contra la Covid-19.  
  
Hasta la fecha, la OMS incluyó en la lista de fármacos aprobados para su uso "de emergencia" los de Moderna, Pfizer/BioNTech, Johnson&Johnson y a las dos versiones de AstraZeneca, la fabricada en India (llamada Covishield) y la que desarrolla en Corea del Sur.  
  
También otorgó esa autorización a las de origen chino de Sinovac (llamada CoronaVac) y de Sinopharm.  
  
La lista de uso de emergencia de la OMS es un requisito previo para el suministro al Covax, el mecanismo creado para achicar la brecha en el acceso de inoculantes, y facilita la inclusión de la vacuna dentro de las solicitadas por los países para habilitar viajes internacionales.  
  
También permite a los naciones acelerar su propia aprobación reglamentaria para importar y administrar los inmunizantes contra la Covid-19.