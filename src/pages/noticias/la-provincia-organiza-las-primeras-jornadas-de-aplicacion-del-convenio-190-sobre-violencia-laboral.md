---
category: Estado Real
date: 2021-08-06T06:00:29-03:00
thumbnail: https://assets.3dnoticias.com.ar/VIOLENCIALABORAL.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La provincia organiza las primeras jornadas de aplicación del convenio 190
  sobre violencia laboral
title: La provincia organiza las primeras jornadas de aplicación del convenio 190
  sobre violencia laboral
entradilla: La provincia organiza las Primeras Jornadas de aplicación del Convenio
  190 sobre Violencia Laboral.

---
Los ministros de Igualdad, Género y Diversidad, Celia Arena y de Trabajo, Empleo y Seguridad Social, Juan Manuel Pusineri, dieron inicio a las primeras Jornadas de aplicación del Convenio 190 sobre violencia laboral que se desarrollarán de manera virtual, convocadas por esta cartera junto a la Red Sindical Ambientes de Trabajo Libres de Violencia.

En su discurso de apertura Pusineri remarcó el proceso de lucha y reivindicación por parte de las y los trabajadores que conlleva la sanción de toda norma, especialmente en el ámbito laboral y señaló: “Las normas laborales, antes que jurídicas, son construcciones sociológicas. Quienes seguimos el debate acerca de este convenio en la Organización Internacional del Trabajo (OIT) sabemos que no ha sido un camino fácil, y que se plantearon muchas disidencias, la mayoría de ellas referidas al alcance de esta norma. Porque esta reglamentación implica la extensión del mundo laboral más allá de las paredes de una oficina, y contempla la dificultad de producir pruebas para la mayoría de las víctimas de estas conductas inaceptables, especialmente las mujeres”.

El ministro expresó que el Convenio 190 implica un avance en materia de legislación laboral, pero que requiere de actitudes concretas de los distintos niveles del estado, de las asociaciones sindicales y de los empleadores para su implementación.

“Nuestro gobernador Omar Perotti ha ratificado este compromiso en cada una de las instancias que hemos asumido, tanto en lo que tiene que ver con capacitaciones para los ámbitos públicos y privados; el tratamiento de denuncias frente a situaciones de violencia laboral; la implementación de acciones positivas contra la desigualdad como la aplicación del cupo laboral trans para cubrir vacantes en la estructura administrativa santafesina; la discusión de acuerdos paritarios con los gremios estatales y en los Comités Mixtos que han empezado a conformarse en el ámbito. Por todo esto, concluyó el funcionario, “celebramos estos espacios de reflexión, discusión, difusión y comunicación como el que se propone en este momento”, manifestó Pusineri.

El Convenio 190 de la OIT es la primera norma jurídica internacional que aborda la temática específica de la violencia en el ámbito laboral. La provincia de Santa Fe puso en marcha una mesa interministerial en el mes de marzo pasado como mecanismo para garantizar su aplicación.