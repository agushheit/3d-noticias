---
category: La Ciudad
date: 2021-05-15T06:00:52-03:00
thumbnail: https://assets.3dnoticias.com.ar/RADAR.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Rincón: comienzan a funcionar los radares sobre ruta 1 y aguardan el permiso
  por foto multas'
title: 'Rincón: comienzan a funcionar los radares sobre ruta 1 y aguardan el permiso
  por foto multas'
entradilla: A la espera de colocación de dispositivos fijos, la semana que viene ya
  habrá controles con cinemómetros móviles. Además, el municipio costero pretende
  multar, mediante fotos, los cruces de semáforos en rojo.

---
La Agencia Provincial de Seguridad Vial recibió la aprobación del Instituto Nacional de Tecnología Industrial para la colocación de radares y cinemómetros que había solicitado para que funcionen en territorio santafesino.

Entre esas autorizaciones se encuentran los dispositivos que había solicitado y venía gestionando la municipalidad de la ciudad de Rincón para radarizar la ruta provincial 1, en su travesía urbana.

A la espera de la colocación y habilitación definitiva de dos radares fijos, la próxima semana comenzarán a funcionar los cinemómetros móviles sobre el corredor.

Así lo anticipó José Amado, secretario de Gobierno y Políticas Territoriales de San José del Rincón: "Vamos a trabajar con radares móviles, ya hicimos todas las pruebas, hasta tanto se aprueben los fijos".

Solo se aguarda la resolución de la Agencia Provincial de Seguridad Vial, firmada por el director del organismo, Osvaldo Aymo, algo que esperan ocurra también en los próximos días.

Los carteles apostados a la vera de la arteria, que atraviesa toda la localidad y la convierte en la gran avenida de la ciudad, ya anticipa el límite de velocidad de "máxima 60", un requisito sine qua non de la provincia para habilitar la radarización.

El secretario de Gobierno Advirtió que en términos normativos y de validación, "los artefactos portátiles cumplirán la misma función", y añadió: "La única diferencia es que para los radares móviles se necesita un equipo específico, del cual debemos hacernos cargo de su custodia y mantenimiento".

Detalló que, si bien ya está disponible la aprobación del Inti para la instalación de los dispositivos fijos, auditores del organismo deben acercarse hasta el terreno y dar "el visto bueno".

Con respecto a la solicitud elevada a la Agencia Provincial de Seguridad Vial para poder operar con dispositivos de captura de imágenes asociados a los semáforos (fotomulta), Amado precisó que es una condición "tener primero radarizada la ruta".

Destacó que la brigada motorizada municipal no dejará de realizar la vigilancia sobre este tipo de infracción, que incluye la persecusión de los autos y motos que pasan en rojo para labrar el acta de infracción.

"Con las fotomultas, el equipo tecnológico es quien va a realizar ese control" señaló Amado, aunque aclaró que la motorizada municipal seguirá participando de los operativos de control.

Comentó que la colocación de radares "no es una acción puntual, ni espasmódica que realiza el municipio, sino que se da en el marco de una planificación".

Precisó que se "vienen realizando operativos de control vehicular, como nunca se hicieron; antes eran inexistentes. Y los operativos en conjunto con la Agencia Provincial de Seguridad Vial. Estamos haciendo un promedio de cuatro operativos semanales; más los operativos internos con inspectores municipales".

"La cuestión de seguridad vial lo diagnosticamos al inicio de gestión. Nos preocupaba tanto los índices de siniestralidad en ruta como la anarquía que había en el interior de la ciudad. Motos por todos lados sin casco, sin patente. Fuimos generando una etapa de concientización con controles y fuimos potenciando una articulación con la APSV con los operativos en ruta", detalló.