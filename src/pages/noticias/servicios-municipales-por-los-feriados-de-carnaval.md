---
category: La Ciudad
date: 2021-02-14T06:00:58-03:00
thumbnail: https://assets.3dnoticias.com.ar/SERVICIOS.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Servicios municipales por los feriados de carnaval
title: Servicios municipales por los feriados de carnaval
entradilla: El lunes 15 de febrero se realizará la recolección de residuos de forma
  normal y el martes 16 no se prestará el servicio. Asimismo, se informan los horarios
  del Cementerio y la frecuencia del transporte público.

---
La Municipalidad informa los servicios municipales que se prestarán durante el lunes 15 y martes 16 de febrero, por los feriados de Carnaval.

En cuanto a la recolección de residuos, las empresas prestarán el servicio con normalidad el día lunes. En tanto, no habrá recolección el martes 16.

**Transporte y estacionamiento**

En cuanto al transporte público de pasajeros, funcionará ambos días con la frecuencia de feriado.

Por otra parte, el Sistema de Estacionamiento Medido (SEOM) no estará operativo.

**Cementerio**

Con respecto a la necrópolis municipal, el horario de visita durante los dos días será el habitual: de 10 a 12 y de 16 a 19.

Por otra parte, habrá guardias mínimas para inhumaciones y recepción de fallecidos a sala de protocolo.

**Mercado Norte**

El tradicional paseo comercial, ubicado en la intersección de Santiago del Estero y Urquiza, abrirá los dos días, en el horario de 9 a 13 horas.