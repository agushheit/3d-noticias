---
category: La Ciudad
date: 2021-05-20T21:30:19-03:00
thumbnail: https://assets.3dnoticias.com.ar/sagrada-familia.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Concejo Santa Fe
resumen: 'Covid-19: Solicitan acondicionar el Instituto “Sagrada Familia”'
title: 'Covid-19: Solicitan acondicionar el Instituto “Sagrada Familia”'
entradilla: El legislativo local pide que se estudien las condiciones para su correspondiente
  acondicionamiento y utilización.

---
Este jueves en el recinto de sesiones, concejales y concejalas aprobaron un despacho unificado de los concejales Valeria Lopez Delzar y Guillermo Jerez, en el que se le solicita al Ejecutivo Municipal que realice las gestiones pertinentes ante el Gobierno Provincial, a través del Ministerio de Salud, para que se evalúe la posibilidad del acondicionamiento y utilización de las instalaciones del ex “Instituto Privado de Cardiología Sagrada Familia”, en calle Rivadavia 3129.

El proyecto propone esta opción para la atención de pacientes COVID o no COVID en función de los criterios que adopten los especialistas en la materia, en el marco del colapso sanitario por Covid-19.

“Solicitamos al gobierno provincial que de manera urgente utilice estas instalaciones. En este contexto que estamos atravesando es necesario acondicionar este lugar para descomprimir el sistema de salud y que más personas puedan acceder a las camas de terapia intensiva o recibir la atención que necesitan”, explicó Lopez Delzar.