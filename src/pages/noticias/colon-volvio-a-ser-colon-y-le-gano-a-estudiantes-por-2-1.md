---
category: Deportes
date: 2021-10-25T06:00:16-03:00
thumbnail: https://assets.3dnoticias.com.ar/COLON.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Colón volvió a ser Colón y le ganó a Estudiantes por 2-1
title: Colón volvió a ser Colón y le ganó a Estudiantes por 2-1
entradilla: Colón volvió a mostrar la actitud evidenciada ante Talleres y superó a
  Estudiantes 2-1. Los goles los marcaron Eric Meza y Federico Lértora

---
Está claro que cuando Colón juega con actitud, disciplina y concentración le puede ganar a cualquiera. En cambio si entra a la cancha dando ventajas desde la predisposición, entonces puede perder contra cualquiera. Y esta vez, Colón dio vuelta la página, recuperó la memoria y venció con justicia a Estudiantes por 2-1.

En una semana, Colón mostró la mejor y la peor cara. Hace una semana superó a Talleres por 1-0 jugando un gran partido y mostrando mucha personalidad. Cuatro días después fue superado por Argentinos, perdió 3-1, evidenciando un flojo rendimiento. Y este domingo volvió a tener hambre de gloria y ganas de ganar y por ello volvió a festejar.

Colón y Estudiantes protagonizaron un primer tiempo parejo, de ida y vuelta, muy intenso y que además tuvo un par de polémicas. El encuentro arrancó con cierto predominio del Pincha, que se traducía en la posesión del balón, pero no en chances concretas de gol.

Con el correr de los minutos el Sabalero se plantó un poco más en campo rival y en la primera aproximación se puso en ventaja. A los 14' Eric Meza por el sector derecho se animó con un remate de media distancia que sorprendió a Mariano Andújar. El balón le picó antes al arquero y terminó ingresando en la red para el 1-0.

Hasta ese momento había pasado muy poco frente a los arcos y claro está ninguno había hecho los méritos como para ir ganando el cotejo. Sin embargo, ese disparo de Meza puso arriba al Sabalero y con el gol Colón mejoró y en esos minutos posteriores pudo aumentar el marcador.

Y a los 18' se sucedió la primera acción polémica, Gonzalo Piovi ingresó dentro del área por el sector izquierdo con pelota dominada y eludió a Agustín Rogel. El defensor uruguayo fue al piso, tocó el balón pero arrastró el pie del defensor rojinegro en lo que fue un claro penal que el árbitro Leandro Rey Hilfer decidió obviar.

Una acción que perjudicó notoriamente al conjunto local que ya estaba ganando y que de sancionar la pena máxima hubiese podido marcar el segundo y comenzar a definir el pleito. Estudiantes generó la primera chance clara a los 25' con un disparo de Francisco Apaolaza desde afuera del área que se estrelló en el palo izquierdo.

Sin embargo, a los 28' el Pincha tendría revancha. Leonardo Godoy metió un centro desde la derecha, Paolo Goltz no alcanzó a despejar de la mejor forma, ya que el balón le quedó a Fernando Zuqui. El exColón remató y el balón dio en el travesaño, pero en el rebote impactó en Leonardo Burián y fue a parar dentro del arco, para el 1-1 parcial.

Luego de ese gol, paso poco frente a los arcos, hasta que a los 44' Wilson Morelo anticipó a Fabián Noguera y cuando se iba derecho al arco, el defensor lo tocó. El árbitro cobró infracción y lo amonestó, quedando la duda si debía expulsarlo por último recurso. Alexis Castro ejecutó el tiro libre que fue controlado por Andújar.

Desde lo futbolístico, el empate estuvo bien, aunque claro está no se puede soslayar la acción del penal que no cobró el árbitro y que hubiese cambiado el curso del partido. Mientras que quedó para el análisis la acción de Morelo, aunque en este caso aparentemente lo esgrimido para no mostrar la roja fue que Morelo no tenía el control del balón.

En el segundo tiempo, Colón salió decidido a imponer condiciones y así lo hizo. Empujó a Estudiantes sobre su área y comenzó a inquietar por las bandas. Morelo picó por izquierda, asistió a Bernardi pero el volante cordobés no pudo conectar bien dentro del área.

Si bien no generaba chances claras, era el que más cerca estaba de ponerse arriba en el marcador. Y lo logró a los 14' con un golazo de Federico Lértora. El mediocampista ejecutó un remate furibundo que se metió en el ángulo izquierdo del arco defendido por Andújar.

Y estaba bien que Colón fuera ganando el partido, por la ambición y las ganas que estaba demostrando en la etapa complementaria, ante un Pincha demasiado tibio y sin audacia. Luego Ricardo Zielinski movió el banco buscando mayor protagonismo y apuntando al juego aéreo.

Colón con la ventaja se defendía con orden ante un rival que carecía de ideas. Y al que le costaba inquietar a Burián. Pero aún así, a los 29' casi llega al empate luego de un despeje de cabeza de Goltz que terminó pegando en el palo derecho y Rafael Delgado terminó despejando.

Los minutos finales fueron con Estudiantes empujando y abusando de los pelotazos y con Colón firme en defensa y neutralizando el juego áereo del Pincha. El Sabalero repitió la actitud mostrada ante Talleres y por eso sumó una nueva victoria como local. Sin dudas que jugar ante su gente lo motiva y mucho a este equipo.

Encontró el estímulo necesario ante sus hinchas para rendondear un partido aceptable. Mucho mejor desde la actitud que en el aspecto futbolístico. Sin generar demasiado frente al arco rival le alcanzó para convertir dos goles y sumar una victoria que lo deja en la 5ª posición de la tabla con 29 puntos.

**Síntesis**

**Colón:** 1-Leonardo Burián; 21-Eric Meza, 33-Facundo Garcés, 6-Paolo Goltz, 40-Rafael Delgado, 3-Gonzalo Piovi; 23-Christian Bernardi, 14-Federico Lértora, 11-Alexis Castro; 35-Facundo Farías y 19-Wilson Morelo. DT: Eduardo Domínguez.

**Estudiantes:** 21-Mariano Andújar; 29-Leonardo Godoy, 3-Agustín Rogel, 2-Fernando Tobio, 18-Fabián Noguera, 13-Matías Aguirregaray; 8-Fernando Zuqui, 30-Jorge Rodríguez, 14-Juan Sánchez Miño; 10-Gustavo Del Prete y 16-Francisco Zapiola. DT: Ricardo Zielinski.

**Goles:** PT 14' Eric Meza (C), 29' Leonardo Burián e/c (E), ST 14' Federico Lértora (C),

**Cambios:** ST 16' Manuel Castro x Apaolaza (E), Leandro Díaz x Sánchez Miño (E), 28' Yeiler Góez x Farías (C), 28' Jaime Ayoví x Del Prete (E), Nicolás Pasquini x Aguirregaray (E), 35' Franco Zapiola x Tobio (E), 38' Lucas Beltrán x Morelo (C), 44' Santiago Pierotti x Bernardi (C).

**Amonestados:** Garcés (C), Noguera y Zuqui (E).

**Árbitro:** Leandro Rey Hilfer.

**Estadio:** Brigadier López.