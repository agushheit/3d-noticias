---
category: La Ciudad
date: 2021-09-21T06:00:48-03:00
thumbnail: https://assets.3dnoticias.com.ar/VACUPOSTPASO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: En la ciudad de Santa Fe, el 55% de la población total ya tiene las dos dosis
title: En la ciudad de Santa Fe, el 55% de la población total ya tiene las dos dosis
entradilla: " Los indicadores Incidencia y Razón -que muestran la evolución de la
  pandemia- continúan a la baja. Mientras la vacunación avanza, es clave sostener
  los cuidados preventivos."

---
De acuerdo al último informe de situación Covid-19 que elabora la dirección de Salud del municipio local -correspondiente a la Semana Epidemiológica Nº 37, con fecha del sábado 18 de septiembre-, el 54,7% de la población total de la ciudad de Santa Fe (calculada en 430 mil habitantes, sobre la base del Censo 2010 y con proyección a 2021) tiene completado el esquema de vacunación anti Covid: dos dosis. El porcentual está calculado sobre el total de personas que recibieron una dosis de una de las vacunas disponibles.

En el mismo sentido, el 70,5% de la población santafesina tiene al menos una dosis, y el total de inoculaciones anti Covid aplicadas es de 537.031en esta capital. También, son buenos los porcentajes de la "población objetivo" que recibió las dos inoculaciones: el 94,1% del personal de salud de toda la ciudad, por ejemplo.

En la misma línea, el 90,1% del personal esencial (de seguridad, tomando un caso) y el 76,5% de lo grupos de riesgo (que padecen comorbilidades) ya recibieron las dos dosis; también el 92,8% de los adultos de entre 60 y 69 años; el 95,6% de 80 años o más, y el 66,4% del ítem "otros", que abarca a mayores de 18 tienen los esquemas completos.

El reporte difundido semanalmente es de elaboración propia de la Dirección de Salud y Promoción Comunitaria municipal, sobre la base de los datos del Sistema de Atención Primaria de la provincia de Santa Fe (Ministerio de Salud).

**Indicadores**

Con relación a los dos indicadores que se miran para saber cuál es la situación de la pandemia en la capital provincial, deben analizarse Incidencia y Razón. La Incidencia es el total de casos confirmados en las últimas dos semanas epidemiológicas cada 100 mil habitantes. Mientras menor sea ese número, mejor parada está la ciudad frente al SARS-CoV-2.

Para graficar: en el momento más álgido de la segunda ola de la pandemia este indicador llegó a estar en 1.034 puntos. Pero a este sábado 18 la Incidencia es de 45.3 por cada 100 mil habitantes. Si se hace una comparación con la Incidente notificada en el anterior informe de situación (Semana Epidemiológica N° 36), que era de 74.9, la reducción porcentual de esta variable es del 40% en los últimos 14 días.

También aparece la Razón, numerador que muestra el número de casos confirmados acumulados respecto de las dos semanas previas. Mientras que en el último informe la Razón es de 0.35, la del reporte anterior era de 0.42. La reducción porcentual es del 16%.