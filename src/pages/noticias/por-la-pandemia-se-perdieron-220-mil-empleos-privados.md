---
category: Agenda Ciudadana
date: 2021-03-16T06:41:27-03:00
thumbnail: https://assets.3dnoticias.com.ar/070121-pedidos-ya.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de El Litoral
resumen: Por la pandemia se perdieron 220 mil empleos privados
title: Por la pandemia se perdieron 220 mil empleos privados
entradilla: A lo largo de 2020, producto de la pandemia y la crisis económica, unos
  220 mil puestos de trabajo formales se perdieron en el país.

---
A lo largo de 2020, producto de la pandemia y la crisis económica, unos 220 mil puestos de trabajo formales se perdieron en el país, según el informe de una consultora, que reconoció que los planes oficiales atenuaron el impacto. 

Este número representó una caída del 1,8%, acumulando una destrucción de 446 mil puestos (baja del 3,6%), si se extiende la medición hasta diciembre de 2017. 

"En 2020, la actividad cayó 10%, mostrando su peor retroceso desde 2002. Como era de prever, este deterioro impactó en casi todos los frentes de la economía: la demanda, los salarios y el empleo, entre otros, sufrieron la crisis del año pasado", indicó en su estudio la consultora Ecolatina.

Añadió que "si bien la crisis que se agravó por la pandemia es tan profunda como generalizada, no todos los sectores sufrieron de la misma manera". 

Los rubros esenciales cayeron menos que los no esenciales y en la misma línea, la producción de bienes se redujo menos que la de servicios.

Esta dinámica heterogénea también se verificó en el mercado de trabajo formal: los asalariados privados, los empleos de mayor calidad, fueron los que más sufrieron la crisis (202.000 puestos, 3,4% de caída). 

> Mientras que los monotributistas y autónomos cayeron "solo" 1,4% (40.000 puestos), puntualizó el estudio. 

En sentido contrario, el empleo público creció 0,6% en sus tres niveles de gobierno -nacional, provincial y municipal-, sumando poco más de 19.000 trabajadores y resaltando el carácter contracíclico del gasto público.

"Estas diferencias fueron más allá de la modalidad de contratación: también se verificaron entre las distintas ramas de actividad. Por caso, los segmentos asociados a la producción de bienes -a excepción de construcción, que por la pandemia tuvo una dinámica más parecida a servicios- destruyeron el 1% de sus plantillas durante el 2020, mientras que la contracción alcanzó el 5% en el resto de las ramas de actividad", remarcó.

Acerca de por qué el empleo registrado cayó menos que la producción, la compañía explicó que "por un lado, la posibilidad de suspender trabajadores en los rubros paralizados contribuyó a conservar algunos puestos de trabajo, en tanto que las dificultades para despedir personal formal que provocaron la doble indemnización y la prohibición de despidos también ayudaron a contener la sangría". 

"En la misma dirección, el programa de Asistencia a la Producción y el Trabajo -ATP-, donde el Estado Nacional pagó parte del sueldo de algunas firmas golpeadas por las restricciones a la circulación, contribuyó a cuidar los puestos de trabajo registrados. Como resultado, la actividad sintió más el impacto del coronavirus que el empleo", consideró Ecolatina. 

La firma estimó que "podría pensarse que el mercado de trabajo formal recuperaría el terreno cedido en cuanto reaccione la demanda". 

Sin embargo, aclaró que "esto no se daría, y no solo porque la destrucción de puestos de trabajo registrados fue un tercio de la del nivel de actividad: el cierre de más de 20.000 empresas durante el año pasado ralentizará la creación de puestos de trabajo en la pos-pandemia".

 

**RECLAMO DE LA UIA POR IMPORTACIONES**

La Unión Industrial Argentina (UIA) planteará en las próximas horas al Gobierno nacional que existen "seria dificultades" para la importación de insumos clave en distintos rubros de la actividad productiva, lo cual está impactando en el potencial de recuperación. El problema surge a partir los trámites que deben hacerse a través del Sistema Integral de Monitoreo de Importaciones (SIMIS) con el que el Gobierno monitorea y autoriza las compras en el exterior. Daniel Funes de Rioja, vicepresidente de la UIA y titular de la Coordinadora de las Industrias de Productos Alimenticios (COPAL) dijo que no se trata de "condenar" al gobierno por la situación sino de presentar una señal de alarma por lo que está ocurriendo. El empresario dijo por otro lado que la inflación en la Argentina responde a que el país tiene un problema macroeconómico y también hay que hay problemas puntuales que deben ir siendo enmendados en base al diálogo.

 

**Unos 200 mil jubilados dejarán de pagar Ganancias** 

Unos 130.000 jubilados que perciben sus por la Administración Nacional de la Seguridad Social (ANSeS) y otros 70.000 que cobran de cajas provinciales dejarán de pagar el impuesto a las Ganancias a partir de los cambios que se impulsan en la Cámara de Diputados. 

Estas precisiones fueron anunciadas por el presidente de la Cámara de Diputados de la Nación, Sergio Massa, a través de publicaciones en sus redes sociales.

Massa dijo que la nueva ley de Ganancias, que eleva el piso a 150.000 pesos y hace que dejen de pagar el impuesto 93 de cada 100 trabajadores, es un "gran alivio fiscal también para el sector pasivo. 

De los 10.000 millones de pesos de devolución de retenciones que habrá en abril, al menos 1.500 millones de pesos se destinarán a jubilaciones.

Con la modificación, 200.000 jubilados y jubiladas dejarán de pagar Ganancias, de los cuales 130.000 son de Anses y 70.000 corresponden a las cajas provinciales.

"Este nuevo piso llevará a que, del total de los 6,9 millones de jubilaciones y pensiones, sólo 30.000 (el 0,4%) continúen pagando Ganancias", dijo Massa.

Y agregó: "Esto es muy importante porque es dinero que se volcará al consumo interno, haciendo mover así la economía".

El proyecto prevé elevar el piso de seis a ocho jubilaciones, esto equivale a 164.568 pesos.

Además, se flexibilizan los requisitos para acceder a esta deducción, evitando que por 1 peso de ingresos en una caja de ahorro o por un plazo fijo se pierda el beneficio.