---
category: Estado Real
date: 2021-04-08T07:04:49-03:00
thumbnail: https://assets.3dnoticias.com.ar/covid-peatonaljpg.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Diario Uno
resumen: 'Santa Fe: los cinco departamentos con "alto riesgo" donde habría más restricciones'
title: 'Santa Fe: los cinco departamentos con "alto riesgo" donde habría más restricciones'
entradilla: Las medidas restrictivas anunciadas por el Presidente Alberto Fernández
  podrían ser aplicadas en cinco distritos provinciales que fueron calificados de
  "alto riesgo epidemiológico".

---
En el marco de la segunda ola de contagios, el Presidente de la Nación, Alberto Fernández, anunció este miércoles una serie de medidas restrictivas a aplicarse en distritos del país con "alto riesgo epidemiológico". En la provincia de Santa Fe, hay cinco departamentos que fueron colocados en esta categoría y en donde podrían recaer las restricciones más significativas.

Los departamentos santafesinos que tienen comprometido en mayor medida su sistema sanitario, siendo calificados de alto riesgo epidemiológico son: Belgrano, Caseros, Castellanos, Rosario y San Lorenzo.

A raíz de la situación epidemiológica preocupante, podrían anunciarse las siguientes restricciones para estos departamentos de la provincia de Santa Fe:

     - Suspensión de actividades sociales en domicilios particulares.

     - Suspensión de reuniones sociales en espacios públicos al aire libre de más de 20 personas.

     - Suspensión de actividades de Casino, Bingo, Discotecas o cualquier salón de fiestas.

     - Suspensión de práctica recreativa de cualquier deporte en lugares cerrados donde participen más de 10 personas.

     - Cierre de los bares y restaurantes a partir de las 23hs.

     - Prohibición de la circulación entre las 00.00 y las 06.00 horas de la mañana de cada día. Según las jurisdicciones, las autoridades podrán solo ampliar estos horarios en función de las especificidades de cada lugar.

Estas medidas de cuidado estarán vigentes desde las 00 horas de este viernes 9 de abril hasta el 30 de abril, según lo dispone Nación, aunque se debe esperar que determina el gobierno provincial.

En las zonas en donde la situación sanitaria y epidemiológica se mantiene en un nivel de "riesgo medio", como el Departamento La Capital, será facultad de los gobernadores dictaminar qué medidas restrictivas se implementan. En su discurso, el Presidente Alberto Fernández destacó que es facultad de las provincias el monitorear y hacer cumplir cualquier medida restrictiva de la circulación.

“Como Presidente de la Nación tengo la responsabilidad política de tomar las riendas y seguir conduciendo al país. Pero en estas circunstancias prima mi condición humana que me obliga a cuidar la vida y la salud antes que nada”, manifestó el Presidente de la Nación.