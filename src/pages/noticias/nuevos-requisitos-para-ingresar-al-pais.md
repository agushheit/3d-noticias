---
category: Agenda Ciudadana
date: 2021-04-10T06:00:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/INGRESO.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Nuevos requisitos para ingresar al país
title: Nuevos requisitos para ingresar al país
entradilla: Todo aquel que no sea argentino y quiera ingresar al país con el propósito
  de hacer turismo no podrá hacerlo. En tanto, hay nuevas condiciones para transportistas
  y tripulaciones.

---
Mientras continúan suspendidos los vuelos provenientes de Reino Unido de Gran Bretaña e Irlanda del Norte, Chile, Brasil y México, los viajeros/as deberán hacerse un testeo para poder abordar el avión con destino a la Argentina. Además, deberán hacerse un testeo al llegar al país y otro testeo al séptimo día de ingreso; y los testeos estarán a cargo del pasajero.

Quienes resulten positivos, al ingreso al país deberán realizar otro testeo de secuenciación genómica y, junto con sus contactos estrechos, cumplir aislamiento en los lugares que indiquen las autoridades nacionales, hasta su traslado seguro hasta la residencia si correspondiera. Mientras, la estadía en dichos lugares y el testeo de secuenciación, también estarán a cargo del pasajero; en tanto, quienes resulten negativos en el testeo, al ingreso cursarán aislamiento en sus domicilios, y deberán realizar un nuevo test para finalizarlo.

Por otra parte, quienes regresen del exterior están obligados a aislarse diez días, contados desde el primer testeo realizado en el país de origen y se controlará que quienes regresaron de viaje estén cumpliendo el aislamiento en sus domicilios. En caso de verificarse el incumplimiento del aislamiento, las autoridades deberán radicar una denuncia penal de acuerdo con los Arts. 205 y 239 del Código Penal (por violación a medidas contra epidemias y desobediencia a autoridad pública). Asimismo, el viajero/a deberá declarar los lugares en donde estuvo los últimos 14 días previos al reingreso al país.

**Nuevos requisitos para transportistas y tripulaciones:**

Terrestres: los extranjeros deberán contar con testeo de Covid negativo realizado dentro de las 72 horas de ingreso al país; los nacionales, deberán realizar testeo de antígenos dentro de las 72 horas de ingreso al país;

Buques: los extranjeros deberán permanecer embarcados y no contarán con relevo en el país; los nacionales, realizar un testeo de antígenos dentro de las 72 horas de ingreso;

Aéreos: los extranjeros deberán movilizarse bajo modalidad burbuja en el país y cumplir los protocolos sanitarios; los nacionales, realizar testeos de antígenos como mínimo cada 15 días o menos.

Las autoridades sanitarias recomiendan además a todos los argentinos/as y residentes no viajar al exterior, especialmente a aquellos que integran los grupos de riesgo, a fin de evitar las dificultades y condiciones sanitarias exigidas al reingreso, considerando la rápida evolución de las condiciones epidemiológicas.

El flujo de personas que regresen al país dependerá de la necesidad de realizar en forma correcta los testeos al ingreso, el cumplimiento de medidas sanitarias, el diagnóstico, identificación de los contactos estrechos, derivación y traslado cuando corresponda.

Finalmente, recuerdan que encuentran suspendidos los viajes internacionales grupales, de egresados y egresadas, jubilados y jubiladas, de estudio, para competencias deportivas no oficiales; de grupos turísticos y de actividades recreativas y sociales, en forma genérica.