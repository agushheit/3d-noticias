---
category: Deportes
date: 2021-12-13T01:15:49-03:00
thumbnail: https://assets.3dnoticias.com.ar/colon1.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Diario Uno
resumen: Arranca la venta de entradas para la final entre Colón y River
title: Arranca la venta de entradas para la final entre Colón y River
entradilla: Este lunes arrancará la venta de las 11.100 entradas de Colón para el
  Trofeo de Campeones ante River. Este lunes a las 15 arranca el expendio.

---
**Las 11.100** [**entradas**](https://www.unosantafe.com.ar/entradas-a20338.html) **que le corresponden a** [**Colón**](https://www.unosantafe.com.ar/colon-a20888.html) **en la final por el Trofeo de Campeones, que jugará el próximo sábado ante** [**River**](https://www.unosantafe.com.ar/river-a3330.html) **Plate,** [**se pondrán a la venta a partir de este lunes con prioridad para los alrededor de 9 mil socios**](https://www.ligaprofesional.ar/notas/trofeo-de-campeones/2021/12/12/como-adquirir-los-tickets/) **que siguieron pagando la cuota durante la pandemia de coronavirus, indicaron voceros del club santafesino.**

**El Trofeo de Campeones se disputará el sábado 18 de diciembre desde las 21.10 en el estadio Madre de Ciudades de Santiago del Estero** y, más allá de que ambos clubes tendrán la misma cantidad de lugares disponibles, el sorteo de la localía favoreció a River.

Las entradas para los socios de Colón se pondrán a **la venta a través del sitio web de la empresa Autoentrada y el club resolvió darles prioridad a los socios que no dejaron de pagar la cuota en pandemia,** que según los registros fueron 9.299 y tendrán 24 horas para adquirirlas antes de que queden liberadas para el resto de los asociados.

![José Alonso dio detalles de cómo será la venta para los socios de Colón que desean ir a Santiago del Estero. ](https://media.unosantafe.com.ar/p/526c8772c58836ef5262d87343a2fcf4/adjuntos/204/imagenes/030/778/0030778657/canje-entradas-colonjpg.jpg?2021-12-06-14-54-28 =642x361)

José Alonso dio detalles de cómo será la venta para los socios de Colón que desean ir a Santiago del Estero.

**_En todos los casos, las personas que quieran comprar sus tickets deberán tener la cuota de diciembre paga y aportar su nombre y los números de DNI y de socio._**

El plantel que dirige Eduardo Domínguez viajará a Santiago del Estero e**l jueves en un vuelo chárter que compartirá con integrantes de las familias de los jugadores y se alojará en un hotel de Termas de Río Hondo,** donde realizará los últimos movimientos antes de viajar hacia la capital santiagueña.

**_Colón ganó su derecho a disputar el Trofeo de Campeones al obtener la Copa de la Liga Profesional el 4 de junio pasado, cuando derrotó a Racing en la final, en tanto River se le sumó al quedarse con el certamen de la LPF que este fin de semana disputa su última fecha._**