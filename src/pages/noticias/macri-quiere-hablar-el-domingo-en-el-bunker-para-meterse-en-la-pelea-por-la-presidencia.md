---
category: Agenda Ciudadana
date: 2021-11-12T22:19:56-03:00
thumbnail: https://assets.3dnoticias.com.ar/MAUMAU.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: La Politica Online
resumen: Macri quiere hablar el domingo en el búnker para meterse en la pelea por
  la presidencia
title: Macri quiere hablar el domingo en el búnker para meterse en la pelea por la
  presidencia
entradilla: 'El ex presidente busca subirse a la posible victoria de Juntos a nivel
  nacional y también quiere aparecer en el desayuno con los candidatos para no cederle
  todo el rédito a Larreta.

'

---
Mauricio Macri quiere hablar en la noche del domingo en el búnker que Juntos montará en Costa Salguero para meterse en la pelea por la presidencia y no cederle el protagonismo del triunfo a Horacio Rodríguez Larreta.  
En el último mes de la campaña, como anticipó LPO, el ex presidente intentó subirse al triunfo de los candidatos que ganaron en las primarias en provincias clave como Entre Ríos, Córdoba y Santa Fe, pese a que en esos distritos había apostado por otros candidatos o se había mantenido al margen.

Eso viene generando tensión con el jefe de gobierno porteño, que apostó fuertemente por los candidatos que ganaron en esas provincias, tal como hizo con Diego Santilli en Buenos Aires y con María Eugenia Vidal en la Ciudad.

La tensión continúa en las horas previas a los comicios porque Macri quiere ser uno de los oradores en Costa Salguero, a diferencia de lo que pasó en las primarias, en las que se subió al escenario pero no habló.

Además, el ex presidente quiere sumarse al desayuno de candidatos en la Ciudad, en el que estarán Vidal y Larreta. Santilli tendría el suyo con Facundo Manes, con el que volverá a compartir búnker propio en La Plata.

En un sector del PRO dicen que Macri en realidad se sirve de Bullrich para desgastar al jefe de gobierno por derecha con el objetivo final de volver a la carga en 2023 por otro mandato  
Macri se sigue sintiendo el líder del espacio y algunos dirigentes que se muestran cercanos a Larreta hicieron guiños en las últimas semanas que señalan que no dan por muerto al fundador del PRO. Rogelio Frigerio lo recibió en Entre Ríos en el tramo final de la campaña y Martín Lousteau le dijo "Hola presidente" en el cierre porteño.

La preocupación de Larreta es no poder controlar al núcleo duro del macrismo en los dos años venideros, mientras esa "ala dura", con Macri y Patricia Bullrich a la cabeza, coquetea con el sector de Javier Milei, que lo acusa de comunista.

En un sector del PRO dicen que Macri en realidad se sirve de Bullrich para desgastar al jefe de gobierno por derecha con el objetivo final de volver a la carga en 2023 por el "segundo tiempo", en referencia a su libro "Primer tiempo" que los halcones del partido interpretan como un mensaje nítido de las intenciones del ex presidente.