---
category: Agenda Ciudadana
date: 2021-04-22T06:56:54-03:00
thumbnail: https://assets.3dnoticias.com.ar/371541_1.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Diario El Litoral
resumen: El viento tumbó varios árboles de la ciudad de Santa Fe
title: El viento tumbó varios árboles de la ciudad de Santa Fe
entradilla: Con las primeras luces de este jueves por la mañana, la ciudadanía se
  topó con varios árboles caídos en distintos puntos de la ciudad de Santa Fe luego
  de las ráfagas de viento que se registraron durante la noche. 

---
Con las primeras luces de este jueves por la mañana, la ciudadanía se topó con varios árboles caídos en distintos puntos de la ciudad de Santa Fe luego de las ráfagas de viento que se registraron durante la noche. 

![](https://assets.3dnoticias.com.ar/371543_3.jpg)Uno de los puntos más complicados era la zona de Bulevar Gálvez y Güemes, donde cayeron dos grandes árboles del paseo tradicional y provocó el corte de tránsito. Como se aprecia en las imágenes, un vehículo quedó afectado por la caída. 

En el lugar trabaja personal municipal para retirar y normalizar la circulación. 

 ![](https://assets.3dnoticias.com.ar/371544_4.jpg)

Otro añejo árbol fue tumbado por las ráfagas en barrio Sur, concretamente en la zona de calle Juan de Garay, entre Saavedra y Francia. Esta caída obstaculiza media calzada y, al igual que la anterior, vecinos aguardaban que empleados del municipio lo retiren. 

 Al mismo tiempo, cabe señalar que a raíz de la tormenta de viento que se produjeron cortes de energía durante la noche en sectores de Colastiné. 