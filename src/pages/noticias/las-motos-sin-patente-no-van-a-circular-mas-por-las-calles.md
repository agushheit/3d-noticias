---
category: La Ciudad
date: 2021-10-29T06:15:41-03:00
thumbnail: https://assets.3dnoticias.com.ar/moto.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: '"Las motos sin patente no van a circular más por las calles"'
title: '"Las motos sin patente no van a circular más por las calles"'
entradilla: Así lo indicaron del área de Control municipal. El miércoles fueron 53
  las motos retenidas en operativos móviles y fijos en distintos barrios de la ciudad.

---
Este jueves por la mañana, trabajadores de una obra en construcción en zona de Bulevar Gálvez, se sorprendieron con la retención y traslado al Depósito de Vehículos Retenidos (DVR) de sus motos con las cuales llegaron al obrador y dejaron estacionadas en una dársena sobre calle Güemes. Los 4 vehículos retenidos por personal de la Secretaría de Control y Convivencia Ciudadana municipal, contaban con la patente adulterada o con la ausencia de la misma.

Lo cierto es que desde hace ya casi dos meses, el área de Control y Convivencia Ciudadana de la municipalidad santafesina, viene intensificando en distintas zonas de la ciudad el control de motos sin chapa patente o con adulteraciones.

En las redes sociales circuló en las últimas semanas un video donde un inspector del área de Control de la Municipalidad cortó el candado de una moto sin patente estacionada y se la llevó retenida. Tras la polémica generada y el debate de los santafesinos, rápidamente desde el Ejecutivo local indicaron que "la normativa municipal autoriza a la Dirección de Tránsito al corte de cadenas, candados y/o elementos de análoga naturaleza que liguen a motovehículos a señales de transito, postes, columnas de alumbrado publico y toda estructura publica como paso previo a la incautación de las unidades que se constituyan en infracción acorde a la Ordenanza N° 7881/81 Código Procesal Municipal de Faltas y su régimen anexo, y siempre que proceda la medida de decomiso. Una vez realizado el secuestro se realiza acta de infracción y acta de secuestro en la que se detalla por inventario lo que fuera motivo de comiso, y para ser puesto a disposición de la Justicia de Faltas".

“Las motos sin patente o con adulteración no van a circular más por las calles ni tampoco quedar estacionadas. Esto es una decisión política del municipio y su brazo ejecutor, la Secretaría de Control y Convivencia Ciudadana, para colaborar con el combate de la inseguridad en la ciudad", sostuvo Guillermo Álvarez, subsecretario del área.

"Tenemos como objetivo intentar erradicar para el año 2023 la circulación de motos sin patente en la ciudad. La Ley Nacional 24.449 establece específicamente como falta grave circular sin chapa patente reglamentaria", resaltó Álvarez y continuó agregando: "Entendemos el malestar que puede causar la retención de motos sin papeles para circular, pero lo que tiene que entender el ciudadano santafesino, es que la medida implementada de incrementar controles fijos y móviles, tiene que ver estrictamente con brindar más seguridad para colaborar y facilitar el trabajo de la Policía".

Solamente en la jornada de este miércoles, la Secretaria de Control y Convivencia Ciudadana del municipio santafesino, retuvo 53 motos con la ejecución de distintos controles fijos y móviles en barrio Centenario, Barranquitas, Los Hornos y en toda la extensión de avenida Peñaloza. "Los operativos son acompañando a la Policía, a las fuerzas federales o inspecciones independientes realizadas por la Secretaría de Control municipal. La idea central es multiplicar los operativos en diversos barrios de la ciudad", finalizó el subsecretario de Control.

A finales del mes de agosto, el secretario de Control y Convivencia Ciudadana de la Municipalidad, Fernando Peverengo, indicaba a  que "la visión que buscamos de la ciudad en un mediano plazo, es el de una Santa Fe sin circulación de motovehículos sin chapa patente o patentes adulteradas, uno de los grandes problemas que tenemos hoy relacionado al control vehicular y la problemática de inseguridad". "Nos parece una forma directa y efectiva de colaborar con la sociedad en materia de seguridad desde la Secretaría de Control, además de los operativos de inspección de armerías y desarmaderos en colaboración con funcionarios del Ministerio Público de la Acusación (MPA)", agregaba en aquella publicación.