---
category: Agenda Ciudadana
date: 2021-07-30T06:00:09-03:00
thumbnail: https://assets.3dnoticias.com.ar/INA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Municipalidad y el INA analizaron la bajante histórica del río Paraná
title: La Municipalidad y el INA analizaron la bajante histórica del río Paraná
entradilla: La reunión realizada este jueves contó con la participación del intendente
  y de representantes del Instituto Nacional del Agua. Se estableció una agenda de
  temas de cooperación mutua.

---
El intendente Emilio Jatón se reunió este jueves con autoridades del Instituto Nacional del Agua (INA) Centro Regional Litoral, a los fines de evaluar la situación de la bajante histórica del río Paraná y sus consecuencias en la zona, además de generar una agenda de temas de cooperación mutua.

Por el organismo estaban presentes Silvia Rafaelli, subgerenta del INA Centro Regional Litoral, y el Ing. Ricardo Giacosa, investigador del organismo. También asistió el secretario General del municipio, Mariano Granato.

En la ocasión, se dialogó sobre cómo impacta la situación del Paraná en la región. “Ante el crítico panorama del río, que no solo es grave por la pronunciada bajante sino también por su prolongación en el tiempo, nos reunimos para conversar con autoridades del INA, que es el organismo de referencia. Fue un intercambio productivo con relación a este tema que nos preocupa y ocupa, más aún con los pronósticos de que se mantendrá el descenso del río hasta fin de año”, indicó Jatón.

El intendente valoró este encuentro que también tuvo como objetivo generar una agenda de cooperación mutua. “Lo principal fue establecer un programa de trabajo con algunos convenios marcos y específicos que nos permitan institucionalmente abordar en conjunto algunos temas en los cuales el INA tiene para aportar al municipio: las defensas. el sistema hidrológico y el plan director de gestión de recursos hídricos son algunos de ellos”, destacó Jatón.

Por su parte, el investigador el INA sostuvo que “con el intendente aprovechamos para hablar sobre algunos temas en los que podemos complementarnos a través de sus técnicos y los nuestros. Una de ellas es la situación de la bajante del río en la región litoral y las afectaciones que tiene en el área metropolitana”.

Giacosa amplió que “esta bajante, que es histórica, tiene tres afectaciones: sobre la toma para el agua potable, sobre la navegación y sobre la energía hidroeléctrica”. Y advirtió que una de las recomendaciones del INA es que se mantenga la vigilancia sobre la laguna Setúbal porque “cuando pase este período invernal y las restricciones por el Covid, seguramente la gente va a querer desarrollar actividades náuticas y de navegación, las cuales resultarán sumamente peligrosas”.