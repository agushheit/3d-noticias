---
category: Agenda Ciudadana
date: 2021-09-01T06:00:30-03:00
thumbnail: https://assets.3dnoticias.com.ar/PARASITOS.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gonzalez
resumen: Uno de cada 4 niños y niñas de Vías Muertas y El Chaparral sufren de malnutrición
  y parásitos
title: Uno de cada 4 niños y niñas de Vías Muertas y El Chaparral sufren de malnutrición
  y parásitos
entradilla: Los datos fueron arrojados por un trabajo conjunto entre la concejala
  de Santo Tomé Florencia Gonzalez, la Asociación Civil Manitos Solidarias y docentes
  de la Universidad Nacional del Litoral.

---
“Son números alarmantes, pero ahora tenemos certezas sobre las cuales poder tomar mejores decisiones y logramos una Ordenanza municipal que pone al tema sobre la mesa para abordarlo como política pública en los barrios”, expresó González.

Un estudio a cargo de la concejala Florencia Gonzalez y la organización Manitos Solidarias detectó que uno de cada cuatro niños y niñas de la zona de Vías Muertas y de barrio El Chaparral presentan malnutrición por exceso, tanto por sobrepeso como por obesidad. Además, los análisis indican que el 33% de los casos relevados se encontraba parasitado.

Tanto González como desde la organización liderada por Alan Butto y Érica Rodríguez, nutricionista de la institución, concluyeron que “estas son problemáticas de salud pública que requieren ser atendidas por el Estado, debiéndose ampliar estos estudios a nivel ciudad para poder realizar un diagnóstico general de la población infantil santotomesina, y en base a ello diseñar estrategias que permitan atender los casos más urgentes y trabajar sobre la prevención desde edades tempranas.”

Florencia Gonzalez destacó que gracias a este trabajo en equipo “logramos aprobar desde el Concejo, en septiembre del año pasado, la creación de un Programa para detectar a tiempo y atender los casos de malnutrición infantil en Santo Tomé, lo que significa hacer foco en esos niños y niñas que menos posibilidades tienen, con carencias nutricionales severas que afectan su desarrollo a lo largo de toda su vida. Queremos sentar las bases para promover y garantizarles una alimentación adecuada y sostenible.”

Por su parte, desde Manitos Solidarias, Érica Rodríguez valoró que con los resultados del relevamiento en mano y la Ordenanza presentada por González ya aprobada, la municipalidad ahora tiene el compromiso de poder continuar con estos estudios. “Considero que esto es una puerta abierta para poder seguir trabajando y tomar medidas socioambientales adecuadas para poder mejorarles la calidad de vida a las nuevas generaciones”, señaló.

El informe final del estudio considera complementar este análisis con encuestas sobre los hábitos alimentarios familiares, el acompañamiento con políticas públicas que garanticen el acceso a toda la población a alimentos saludables, a espacios públicos para la realización de actividad física, y a la educación nutricional en las instituciones educativas.

Finalmente, el relevamiento mostró un denominador común en los resultados positivos del análisis enteroparasitológico: el mecanismo de transmisión. “La contaminación del agua y de alimentos, pobres condiciones de saneamiento ambiental como la disposición inadecuada de excretas, la presencia de basurales o asentamientos de basura en la cercanía de los hogares; el riego de cultivos propios con agua no potabilizada y sobre todo, la falta de educación sanitaria, son algunos de los factores que predisponen la aparición y propagación de los parásitos intestinales en los niños.”