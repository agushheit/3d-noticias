---
layout: Noticia con imagen
author: 3DNoticias
resumen: Clases presenciales
category: Agenda Ciudadana
title: La ministra de educación brindó precisiones respecto al retorno de las
  clases presenciales en la provincia
entradilla: '"Nos vamos preparando progresivamente para ver si el año que viene
  podemos restituir la presencialidad, aunque sea con un sistema de
  alternancia”, afirmó Adriana Cantero.'
date: 2020-11-25T11:53:59.975Z
thumbnail: https://assets.3dnoticias.com.ar/cantero.jpeg
---
La ministra de Educación de la provincia, Adriana Cantero, brindó precisiones acerca de cómo se irá concretando paulatinamente el regreso a las clases presenciales en Santa Fe y anunció: "Nos vamos preparando progresivamente para ver si el año que viene podemos restituir la presencialidad, aunque sea con un sistema de alternancia”.

Asimismo, **hizo referencia a encuentros cuidados entre alumnos con sus docentes** y adelantó que se analiza la posibilidad de que “en la primera quincena de diciembre la distribución de los cuadernos se haga de manera presencial, en pequeños grupos, que permita que las chicas y los chicos tengan una revinculación con sus maestros, pequeños grupos de no másde 7 u 8, en una jornada reducida, pero que permita recibir orientaciones y tener los primeros diálogos”.

En ese sentido, la ministra aclaró que “la idea no es solo hacerlo para las localidades del interior, sino también en las grandes ciudades de la provincia, atento a lo que nos diga el Ministerio de Salud”.

Respecto de la cantidad de encuentros posibles, Cantero declaró que “podrían ser dos en esa quincena, pero dependerá mucho de la planificación institucional, de lo que los directores vayan planteando de acuerdo a la cantidad de alumnos de cada escuela y para esos encuentros de distribución de materiales que queremos que llegue a las manos de cada estudiante”, y agregó que “para nosotros es muy importante esta instancia porque estamos trabajando mucho en las trayectorias pedagógicas que se están dando en el marco de una pandemia".

### **NIVEL SECUNDARIO**

Por otro lado, la titular de la cartera educativa hizo mención al regreso en el nivel secundario: “tenemos que darle una gran importancia a la dimensión afectiva en el aprendizaje, esto lo piden los mismos chicos y chicas a través de los centros de estudiantes con quienes hemos tenido encuentros virtuales muy frecuentes”.

“Para ello, prosiguió Cantero, estamos planificando todo el trabajo que debemos hacer en esta etapa de revinculación y acompañamiento, sobre todo en los adolescentes que van teniendo menos frecuencia de conexión, vamos a estar buscándolos y trabajando con ellos en un gran proyecto intersectorial compartido con Desarrollo Social, con Salud y con Trabajo, durante todo el verano".

### **TITULARIZACIONES**

Cantero también se refirió a los concursos anulados, a instancias del Tribunal de Cuentas de la provincia: “No hay un derecho adquirido sobre algo que se construyó sobre la base de la ilegalidad, sobre el no derecho; lo que sí hay es una expectativa genuina y de buena fe, y por eso estamos planificando nuevos concursos para que la gente acceda a los puestos de trabajo legítimamente y cumpliendo los pasos que cumplen todos, porque eso también es restituir derechos para todas y para todos, porque si nosotros tenemos algún sistema que solo le da privilegios a algunos, eso no son derechos, entonces vamos a trabajar para que todo el mundo tenga posibilidades de titularizar, conforme a la normativa vigente”

En este sentido, la ministra dijo que “los vamos a llamar (a los sindicatos docentes) para encontrarnos y planificar los concursos, porque tenemos intención de plantear gradualmente en los próximos días el primer concurso y así seguiremos, porque son tres los que debemos recomponer. Mientras tanto, todo el mundo sigue cobrando su salario”.

Cantero se refirió también a las dificultades de otros concursos debido a la complejidad que planteó la pandemia: "En estos momentos nosotros estamos haciendo una titularización online, inédita en el país, con un sistema único que el resto de las provincias están observando. Es para para la titularización que tenemos pendiente de los profesores de nivel Secundario en sus horas y cargos, y también estamos llamando para la inscripción de Inicial, Primaria y Especial, para lograr la continuidad permanente de los concursos”.

“Todo esto lo hacemos con el mismo personal, en el medio de una pandemia y trabajando con sistemas virtuales, por eso lo que tratamos es que el sistema pueda responder a tantos concursos en simultáneo", agregó la ministra.

Finalmente, Cantero afirmó: "aseguramos la claridad para todos los santafesinos y santafesinas de que las personas afectadas por esta revocatoria siguen como interinos en sus cargos cobrando sus salarios normalmente hasta que se dé el concurso".