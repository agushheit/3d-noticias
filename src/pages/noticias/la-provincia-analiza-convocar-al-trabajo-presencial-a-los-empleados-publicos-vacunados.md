---
category: Agenda Ciudadana
date: 2021-08-06T06:15:39-03:00
thumbnail: https://assets.3dnoticias.com.ar/GREYHOUSE.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: La provincia analiza convocar al trabajo presencial a los empleados públicos
  vacunados
title: La provincia analiza convocar al trabajo presencial a los empleados públicos
  vacunados
entradilla: Se basa en la resolución nacional que habilita a convocar al trabajador
  estatal luego de 14 días de su vacunación.

---
Con el avance de la vacunación en todo el territorio santafesino, la administración pública provincial busca incrementar la presencialidad en las actividades laborales de los empleados públicos. En este sentido, el gobierno analiza la implementación de una resolución que habilite al sector público a convocar a aquellos empleados estatales que hayan sido vacunados para que regresen a su puesto de trabajo presencial.

"En la provincia se está analizando de que cada uno de los ministerios también empiece a convocar a los empleados públicos vacunados, con los mismos parámetros que las resoluciones nacionales", el ministro de Trabajo de la provincia.

De implementarse se llevaría a cabo "con los mismos parámetros que las resoluciones nacionales", según adelantó a UNO el ministro de la cartera de Trabajo. Esta disposición se está cumpliendo con los empleados públicos dependientes de la administración pública nacional desde el mes de abril, cuando salió la resolución mientras el operativo de vacunación no presentaba un gran avance.

La resolución oficial nombrada por el ministro se trata de la comunicada conjuntamente por las carteras de Salud y Trabajo el pasado mes de abril. En el documento se establecía que luego de 14 días de la vacunación con la primera dosis contra el Covid el empleador podría intimar al empleado a retornar al trabajo presencial.

El decreto, dice que esos empleados deberían volver a la presencialidad -en caso de que se solicite- “independientemente de la edad y la condición de riesgo, transcurridos 14 días de la inoculación”.

A partir del documento lanzado por las carteras a cargo de la ministra de Salud, Carla Vizzotti y su par de Trabajo, Claudio Moroni, en el pasado mes de abril, algunos grupos de personas que permanecían exceptuados del trabajo presencial dejaron de tener licencia. Tal es el caso de personas con enfermedades respiratorias crónicas, afecciones cardíacas, diabetes, mayores de 60 años y embarazadas.

Esta resolución abarca tanto al sector público como el privado. En la realidad de Santa Fe, "en el ámbito de las empresas tienen como posibilidad convocar a la gente vacunada y cada empresa decide si la convoca o no" manifestó Pusineri. En el ámbito empresarial "por ahora no tenemos planteados conflictos" según sostuvo el titular de Trabajo frente al presente del trabajo presencial en los privados.

El grupo de personas que permanece exento de cumplir con trabajos presenciales según la resolución son aquellos con inmunodeficiencias congénita, asplenia funcional o anatómica y desnutrición grave, VIH dependiendo del status, personas con medicación inmunosupresora o corticoides en altas dosis, pacientes oncológicos y trasplantados.