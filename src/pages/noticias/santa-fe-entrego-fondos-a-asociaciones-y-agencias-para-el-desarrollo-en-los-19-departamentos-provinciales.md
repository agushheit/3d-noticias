---
category: Estado Real
date: 2021-07-03T06:00:22-03:00
thumbnail: https://assets.3dnoticias.com.ar/COSTAMAGNA.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Santa Fe entregó fondos a asociaciones y agencias para el desarrollo en los
  19 departamentos provinciales
title: Santa Fe entregó fondos a asociaciones y agencias para el desarrollo en los
  19 departamentos provinciales
entradilla: El ministro Daniel Costamagna firmó un nuevo convenio de aportes no reintegrables
  por 7,5 millones de pesos con la Agencia de Desarrollo del Departamento Constitución.

---
El ministro de Producción, Ciencia y Tecnología de Santa Fe, Daniel Costamagna, firmó un convenio para la entrega de aportes no reintegrables por 7,5 millones de pesos para la Agencia de Desarrollo del Departamento Constitución (Adecon). El acto se desarrolló en el Palacio Municipal de Villa Constitución.

Con esta rúbrica, ya son 27 instituciones para el desarrollo de todos los departamentos de la provincia (19 asociaciones y 8 agencias) las alcanzadas por fondeos de este tipo que tienen como objetivo la adjudicación de créditos blandos a MiPymes del territorio santafesino que se hayan visto afectadas por las medidas de restricción y aislamiento decretadas en el marco de la pandemia por Covid 19. Estas transferencias, realizadas en el marco del decreto N°355/20, alcanzan el monto de 162 millones de pesos invertidos en toda la provincia y otorgados más de 2 mil créditos. Se trata de préstamos de hasta 100 mil pesos con un plazo de 18 meses total, 6 meses de gracia y una tasa del 13,5% anual.

Acerca de esta política, Costamagna afirmó: “Estos son acuerdos que materializan el acompañamiento que el gobernador Omar Perotti dispuso para todo el sector productivo santafesino en el marco de la pandemia que estamos transitando”.

Además, el ministro ponderó la canalización de los fondos a través de las instituciones para el desarrollo: “Con estos convenios, las líneas de financiamiento son administradas por las asociaciones y agencias para el desarrollo de toda la provincia. Son espacios de articulación público-privada que permiten tener un conocimiento territorial que es fundamental a la hora de adjudicar los fondos”.

“La Adecon es un ejemplo de vinculación virtuosa entre todos los sectores productivos de la región. Aquí confluyen la Cámara Industrial del Departamento Constitución (Cidecon), el Centro Comercial e Industrial, la Zona Franca Santafesina, el Ente Portuario y la Federación Agraria”, valoró finalmente.

**PRESENTES**

Acompañaron al ministro Ariel Sahilices, presidente de Adecon; Iván Camats, director Provincial de Institucionalidad y representante provincial en Adecon; el intendente de Villa Constitución, Jorge Berti, quien es, a su vez, vicepresidente de Adecon; la presidenta Comunal de General Gelly, Marcela de Puerto; José Castro, tesorero de Adecon; Martin Michel, gerente de Adecon; Roberto Tizi, presidente de la Cidecon; Marcelo Maciel, presidente de Adeessa; Juan Carlos Muriel, representante del Centro Comercial, Industrial y de la Producción de Villa Constitución; el presidente de la filial Federación Agraria Empalme; y referentes del comercio y diferentes industrias del departamento: Arcelor Mittal Acindar, Semillero Buzzini, Grupo Cecarri, Sahilices Hnos, Are S.R.L, Muriel muebles, Compañía forestal de Brasil (ex Paraná metal), Laminado Industriales, Zona Franca, PTP Group, Tunka Competición e Ingeniería SCH.