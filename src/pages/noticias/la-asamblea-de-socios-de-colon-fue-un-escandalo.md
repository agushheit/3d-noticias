---
category: Deportes
date: 2021-02-11T06:10:19-03:00
thumbnail: https://assets.3dnoticias.com.ar/colon1.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de El Litoral
resumen: La Asamblea de Socios de Colón fue un escándalo
title: La Asamblea de Socios de Colón fue un escándalo
entradilla: 'Entre aplausos e insultos, el presidente Vignatti se levantó y se fue
  en medio de la reunión. La presencia de la barrabrava caldeó el ambiente y un socio
  fue agredido. '

---
Se realizó este miércoles la Asamblea de Socios en el Club Colón, la que quedó inconclusa, con el presidente José Vignatti yéndose en medio de un escándalo, y a la que varios de los presentes consultados por El Litoral no dudaron en calificarla como “vergonzosa”.

Entre 300 y 400 socios se dieron cita en horas de la tarde en la tribuna oeste del estadio “Brigadier López”. Entre los presentes también estaban miembros reconocidos de la barrabrava rojinegra, algo en lo que muchos coincidieron en afirmar que colaboró para caldear el ambiente.

En la reunión se puso en consideración la Memoria y Balance. A la hora de votar, de acuerdo a lo comentado a El Litoral, solo unos pocos socios levantaron la mano y se dio el “aprobado”, es decir, sin el consenso de la mayoría, lo que motivó el primer “agite” de la asamblea cuando muchos empezaron a protestar.

Sin embargo, todo se desmadró cuando se trató el tema del mandato. Allí comenzaron los gritos, insultos y acusaciones. Unos a favor y otros en contra del presidente, que estaba acompañado por los barras.

Fue en ese momento que un socio de Recreo fue agredido con un golpe de puño.

En medio del griterío, Vignatti se levantó y se retiró de la asamblea, que quedó inconclusa.

Ante la consulta de El Litoral, algunos de los asistentes manifestaron que el Balance se aprobó, otros dijeron que no.

Tampoco quedó en claro la duración del mandato ni la fecha de las elecciones, que en principio se iban a realizar el próximo 23 de mayo, pero que actualmente no tienen una fecha cierta.