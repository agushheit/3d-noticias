---
category: La Ciudad
date: 2021-08-29T06:15:29-03:00
thumbnail: https://assets.3dnoticias.com.ar/DEPTO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: El precio de un departamento en Santa Fe es "el más caro de la historia"
title: El precio de un departamento en Santa Fe es "el más caro de la historia"
entradilla: Según la Cámara de Empresas y Corredores Inmobiliarios, el metro cuadrado
  de un departamento se vende a un precio de tres sueldos de comercio, cuando hace
  veinte años podía comprarse con uno.

---
Con la escapada cambiaria que mantuvo el dólar en los últimos años hubo un reacomodamiento de precios en el rubro inmobiliario. En este sentido, si bien respecto a 2018 el valor del metro cuadrado en dólares es menor en un 40%, a valores históricos un departamento céntrico de un dormitorio en Santa Fe cuesta tres veces más que en los años posteriores a la crisis del 2001.

Lo que debe tenerse en cuenta en este sentido es que, a raíz de la crisis cambiaria, el valor del metro cuadrado de un departamento en pesos es el más caro de la historia. Esto fue reflejado por la Cámara de Empresas y Corredores Inmobiliarios de Santa Fe (Ceci), a través de su presidente, Walter Govone, en diálogo con UNO Santa Fe.

Al respecto, el referente inmobiliario manifestó: "Aunque parezca contradictorio hoy los departamentos están en el valor más caro de la historia en función de lo que valía el metro cuadrado en pesos. En dólares están muy a precio, a raíz de la escapada que está teniendo el dólar, pero si se hace un análisis de lo que cuesta hoy un departamento terminado en pesos es carísimo".

Y continuó: "En moneda extranjera (dólares) hay que disponer de menos dinero para comprar esos metros cuadrados, pero si se compara cuánto cuesta un metro cuadrado en construcción de un departamento céntrico en pesos es el momento más caro de la historia".

 Saliendo de la crisis del 2001 y sin el boom de construcción de departamentos que hubo años después, en 2003 se vendían departamentos de un dormitorio en zona céntrica de Santa Fe por 20.000 dólares. En el 2018 se llegó a pagar 100.000 dólares por el mismo tipo de departamento y actualmente por 60.000 o 70.000 dólares se puede comprar uno.

A propósito de esto, Govone realizó una comparación entre el salario de un empleado de comercio y su poder adquisitivo en relación a un inmueble: "Un metro cuadrado de un departamento se está vendiendo a un precio de casi tres sueldos de comercio, cuando hace veinte años se compraba con un sueldo".

"Si el análisis se hace respecto a 2018 significa un precio del 40% menos, pero si se hace con respecto a 2003 o 2004 significa un valor que aumentó el triple. En función al rango de tiempo que uno hace el análisis parece que el precio bajó, cuando en realidad subieron mucho. En el negocio de las inversiones en inmuebles se tiene que tomar por lo menos un rango de 10 años para hacer una evaluación factible", continuó Govone.

Haciendo hincapié en la variabilidad del mercado inmobiliario respecto al dólar, el referente de Ceci sostuvo que "dependiendo el momento en que se invierte, se gana o se pierde. Alguien que compró un departamento en 2018 puede decirse que perdió, pero esto pasa siempre y hay que esperar a que se reacomode la economía para que haya un rebote y se termine reacomodando el precio. En la Argentina esto pasa habitualmente".