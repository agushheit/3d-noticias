---
category: La Ciudad
date: 2020-12-23T10:20:36Z
thumbnail: https://assets.3dnoticias.com.ar/2312operativo.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Operativo de seguridad para las fiestas de fin de año
title: La provincia y el municipio santafesino diagraman el operativo de seguridad
  para las fiestas de fin de año
entradilla: La Mesa de Seguridad Local de Santa Fe abordó la organización conjunta
  en zonas comerciales. Las medidas respetan los protocolos en el marco de la pandemia
  de COVID-19.

---
El gobierno de la provincia de Santa Fe, a través del Ministerio de Seguridad, realizó este martes una nueva reunión de trabajo, con representantes de la Municipalidad de Santa Fe y responsables de la Unidad Regional I, para definir y programar distintos operativos de seguridad con motivo de las fiestas de fin de año.

De la reunión participaron el subsecretario de Prevención y Control Urbano del Ministerio de Seguridad, Alberto Mongia; el coordinador del Ministerio de Seguridad, Facundo Bustos; la secretaria y el subsecretario de Control y Convivencia de la Municipalidad de Santa Fe, Virginia Coudanes y Fernando Peverengo; y la jefa de la Unidad Regional I, Marcela Muñoz.

## **Operativos especiales**

Este 2020, las fiestas se desarrollarán en un escenario inédito y el ordenamiento y control de la gran cantidad de personas que se trasladen en la ciudad será fundamental para minimizar los riesgos de contagio y para cerrar oportunidades al delito.

Los diversos operativos estarán a cargo de la Policía de Santa Fe y de la GSI y se realizarán durante todo el fin de semana, poniendo especial énfasis en la nochebuena y en el comienzo del próximo año.

La diagramación incluye patrullajes permanentes y dinámicos para prevención de seguridad en los lugares más concurridos; aplicación del protocolo a fiestas y reuniones de más de 15 personas; control permanente en las costaneras este y oeste y las diferentes plazas que componen la ciudad; y controles de tránsito y de alcoholemia dinámicos en lugares de mayor concentración de personas. 

Asimismo, la Municipalidad dispondrá de un operativo especial para evitar el expendio de bebidas alcohólicas y estarán cerrados los paradores ubicados en ambas costaneras.

Por último, los funcionarios insistieron en la importancia de utilizar la línea de emergencias policiales 911 ante eventuales delitos y ante denuncias de fiestas clandestinas.