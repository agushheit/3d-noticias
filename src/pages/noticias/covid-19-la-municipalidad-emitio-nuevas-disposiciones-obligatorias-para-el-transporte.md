---
category: La Ciudad
date: 2021-04-17T06:15:15-03:00
thumbnail: https://assets.3dnoticias.com.ar/MUNICOVID.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Covid-19: la Municipalidad emitió nuevas disposiciones obligatorias para
  el transporte'
title: 'Covid-19: la Municipalidad emitió nuevas disposiciones obligatorias para el
  transporte'
entradilla: Mediante una resolución, se actualizaron las indicaciones para que los
  colectivos, taxis, remises y escolares extremen las medidas de cuidado sanitario.

---
Este jueves, la Municipalidad de Santa Fe dictó la Resolución Interna 07/2021 mediante la cual se establecen nuevas disposiciones para el transporte de pasajeros por colectivo, en taxis y remises, y de escolares. La normativa, basada en el Decreto de Necesidad y Urgencia 235/21 emitido por Presidencia de la Nación, y N° 0280/21 establecido por la provincia, considera la necesidad de implementar medidas extraordinarias para la prestación sin riesgos de un servicio que aparece como primordial para el sostenimiento de las actividades consideradas esenciales.

Es por ello que, en el marco del período de distanciamiento social que atraviesa el país por la pandemia de Covid-19, todos los subsistemas deberán extremar los cuidados para evitar contagios entre el personal, como así también entre los pasajeros.

Con respecto al transporte público de pasajeros por colectivos, se fija que todos los vehículos deberán mantener la sanitización interior de las unidades en las puntas de línea y mantener abiertas cuatro ventanillas como mínimo. En caso de que las condiciones climáticas sean adversas, se solicita la realización de una parada de 30 segundos cada mil metros, para abrir las puertas del coche y permitir la ventilación del mismo.

Sobre la cantidad de pasajeros por unidad, se establece que no puede ser mayor a la capacidad de asientos disponibles y hasta 10 más de pie, pudiendo aceptarse hasta 14 de manera eventual. Estas cifras son las que permiten preservar la distancia social dentro de cada micro.

Además, se recuerda la obligatoriedad del uso del tapabocas tanto para choferes como para pasajeros y la utilización de mecanismos de sanitización personal. En el caso de que alguna de las partes no cumpla con esta medida, puede solicitarse intervención a la fuerza pública.

En el caso de taxis y remises, deben contar obligatoriamente con una separación física entre usuarios y conductores. Por este motivo, no está permitido el uso del asiento delantero reservado para acompañantes.

En lo que refiere a la circulación de aire, las ventanillas traseras deben permanecer abiertas como mínimo 5 centímetros. Y, por último, se solicita que las unidades cuenten con elementos de sanitización en caso de que el pasajero lo requiera.

Finalmente, para el transporte de escolares, se establece que deben llevar abiertas la mitad de sus ventanillas un mínimo de 5 centímetros, siempre procurando el resguardo de las personas que viajan en el interior.

**Controles**

El subsecretario de Movilidad y Transporte del municipio, Lucas Crivelli, indicó que la resolución rige desde este jueves. No obstante, consideró que es necesario otorgar un tiempo oportuno para que cada sistema las ponga en práctica. De este modo, señaló que, a partir del próximo lunes, los controles de todas estas medidas serán exhaustivos.

En cuanto al sistema de colectivos, explicó que la intención es “intensificar las acciones de sanitización que ya se venían llevando adelante, pero se incorpora un punto fundamental como es la ventilación cruzada que deben tener los colectivos”. Frente a ello, apeló a que tanto choferes como usuarios “respeten las normas sabiendo que el municipio recepciona cualquier reclamo que sobre estas determinaciones se considere conveniente”.

Si bien reconoció que muchas de estas medidas ya se implementan en todos los sistemas, mencionó que, hasta el momento, su aplicación era sugerida. No obstante, a partir de ahora será obligatoria. Este es el caso de los taxis y remises, que, si bien no pueden transportar más de tres pasajeros desde el inicio de la pandemia, ahora están obligados a contar con una separación entre chofer y usuario, y a portar elementos sanitizantes.

Por último, para el caso del transporte escolar, Crivelli recordó que se trata de un servicio privado, pero mencionó la importancia de que el sector acate la normativa municipal, teniendo en cuenta que se reanudó la actividad con la vuelta de las clases presenciales bajo la modalidad de burbujas. “Como medida principal, les solicitamos que mantengan la ventilación cruzada”, dijo el subsecretario, al tiempo que recordó que “las cuestiones asociadas al protocolo de los transportistas escolares, fueron presentadas y aprobadas oportunamente, entre ellas, el seguimiento y la trazabilidad de los alumnos que ya se viene implementando”.