---
category: Agenda Ciudadana
date: 2021-05-07T08:46:34-03:00
thumbnail: https://assets.3dnoticias.com.ar/hotel-cerradojpg.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Diario Uno
resumen: Ayuda económica de $10.000 mensuales para trabajadores del sector hotelero
title: Ayuda económica de $10.000 mensuales para trabajadores del sector hotelero
entradilla: La asistencia económica a los empleados de hoteles santafesinos será por
  seis meses, según lo dispuso el gobierno provincial. La inversión será de 170 millones
  de pesos.

---
El gobierno provincial, a través del Ministerio de Producción, Ciencia y Tecnología, anunció la puesta en marcha del Programa de Asistencia Económica para Trabajadores del Sector Hotelero. La ayuda económica a los trabajadores de los hoteles corresponde a un aporte no reintegrable de $10.000 mensuales por los próximos seis meses.

Según el decreto provincial, el aporte alcanza a todas aquellas personas que desarrollen tareas en el sector hotelero local y se encuentren debidamente registrados mediante Formulario N° 931 de la AFIP. La inversión total que anunció la provincia para llevar adelante la asistencia económica es de $169.980.000.

El ministro de Producción, Ciencia y Tecnología, Daniel Costamagna manifestó al respecto: “En particular el sector turístico fue uno de los más afectados, y este programa se pone en marcha como respuesta a un reclamo del sector desde iniciada la pandemia. Con esta iniciativa Santa Fe será una de las pocas provincias en disponer una ayuda de este tipo”.

“Esto es una valoración al esfuerzo que es posible, fundamentalmente, gracias a la prolijidad del Ministerio de Economía que nos posibilita este tipo de inversiones, porque cuidar a una empresa es una inversión. Eso lo llevamos adelante con todos los sectores que tuvieron dificultades, a quienes acompañamos y no se los dejó nunca. Se llevan invertidos más de 1200 millones de pesos de aportes no retornables”, continuó el funcionario.

El secretario de Turismo, Alejandro Grandinetti indicó que la iniciativa tiene como principal finalidad "mantener los puestos de trabajo en el sector hotelero. Hablamos de más de 2.000 trabajadores de esta actividad, que si bien tuvo muy buenos tres meses durante la temporada de verano, no alcanzó para cubrir los costos de mantenimiento de infraestructura y el trabajo que demanda”.

"Estamos cumpliendo el deseo del gobernador de hacer un puente entre la crisis, las olas de covid y la nueva normalidad para mantener las unidades productivas, en este caso los hoteles. Serán cerca de 170 millones de pesos en estos meses. Es la comunión entre los trabajadores, los empresarios y un Estado presente desde el primer momento”, valoró.

**Cómo registrarse**

Para acceder al beneficio de la asistencia económica, los titulares de establecimientos hoteleros deberán completar un formulario de inscripción online que estará disponible en el sitio web oficial de la provincia (link CORONAVIRUS). En tanto que el plazo límite de envío de solicitudes es el 15 de junio de 2021, inclusive.