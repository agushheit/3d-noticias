---
category: Agenda Ciudadana
date: 2021-02-08T07:24:21-03:00
thumbnail: https://assets.3dnoticias.com.ar/cantero.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: LA PROVINCIA SE PREPARA PARA EL REGRESO A LA PRESENCIALIDAD EN LAS AULAS
title: LA PROVINCIA SE PREPARA PARA EL REGRESO A LA PRESENCIALIDAD EN LAS AULAS
entradilla: "“La escuela siempre estuvo presente solo que trabajando de otro modo”,
  indicó la ministra Cantero."

---
La ministra de Educación, Adriana Cantero, brindó precisiones acerca de cómo se implementará el regreso a las aulas en ciclo lectivo 2021. Al respecto, Cantero expresó que “La escuela siempre estuvo presente solo que trabajando de otro modo. En este tiempo complejo la escuela se prepara para volver a recibir a sus estudiantes, educadores, asistentes escolares en el espacio compartido; el espacio escolar, va a ser una escuela distinta a la que siempre conocimos, una escuela con distanciamiento social y protocolos de cuidados de la salud, como son las cotidianeidades que nos tocan vivir hoy”.

**Alternancia semanal**

Para especificar la forma de cursado, la ministra manifestó que “Santa Fe prepara sus escuelas con un sistema de alternancia semanal, esto implica una semana un grupo en clases mientras el otro trabaja en casa y a la semana siguiente alternamos esa cursada; con cuadernos y actividades que nos van a dar nuestros profesores y maestros especialmente preparadas, ya que el Ministerio de Educación va a seguir repartiendo cuadernos de trabajo y materiales impresos para todos y todas sus estudiantes, estén en la sala de nivel inicial, en los grados de primaria o en los años de secundaria. Cada uno va a recibir el cuaderno que necesita para poder trabajar cuando esté en casa”.

Más adelante, la titular de la cartera educativa aclaró en la “jornada escolar donde vamos a ingresar con cuidados y sin amontonamientos; con desinfección y control de temperatura. Después entraremos al salón donde vamos a conversar como nos sentimos, si alguien no se siente bien, la escuela va a tener un lugar especial para el aislamiento y se hará un llamado inmediato a las autoridades sanitarias. Si todos estamos bien tendremos una hora y media de clases, después un recreo largo donde vamos a compartir entretenimientos en burbujas, donde un grupo solo interactúa con su grupo, con actividades preparadas especialmente. Durante el lapso del recreo los adultos de la escuela colaboraran para ventilar los espacios y después con lavado de manos incluido, vamos a ingresar a clases, tendremos otra hora y media de trabajo, saldremos de la escuela ordenados y retornaremos a nuestros hogares”.

**Por el derecho a la educación**

En relación a las acciones tendientes a garantizar el derecho a la educación de niñas, niños, adolescentes, jóvenes, adultas y adultos, la ministra enfatizó que “es una escuela con una jornada distinta pero que nos espera para seguir aprendiendo juntos en un espacio cuidado. El año pasado trabajamos en la distancia, y ello nos mostró dificultades y muchas veces las desigualdades, por ello para este año hemos dado continuidad con Verano Activo, hemos ido a buscar las chicas y los chicos que menos contacto pudieron establecer con las propuestas de aprendizaje para volverlos a convocar”.

En ese sentido, manifestó que “seguimos trabajando por el derecho a la educación sin pausa, todo el verano, todo el año pasado y el ciclo lectivo, que viene con el empeño de que la pandemia no nos robe ninguna alumna y alumno del aula y que podamos atravesar este tiempo todos juntos aprendiendo más, y a construir una ciudadanía responsable y comprometida con la salud de todos”, concluyó Cantero.