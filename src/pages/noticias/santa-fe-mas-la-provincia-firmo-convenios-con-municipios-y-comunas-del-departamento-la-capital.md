---
category: Estado Real
date: 2020-12-16T12:53:10Z
thumbnail: https://assets.3dnoticias.com.ar/santafe-mas.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: 'Santa Fe Más: convenios con municipios y comunas del departamento La Capital'
title: 'Santa Fe Más: convenios con municipios y comunas del departamento La Capital'
entradilla: El programa promueve la inclusión socio-económica de jóvenes de entre
  16 y 30 años que están fuera de los circuitos formales educativos y laborales.

---
El gobierno de la provincia, a través del Ministerio de Desarrollo Social, llevó adelante este martes la firma de convenios para la puesta en marcha del programa “Santa Fe Más” en el departamento La Capital. “La idea que expresó el gobernador es la de una inclusión de los jóvenes, pero que además sea socio productiva; vincular a lo social con el mundo del trabajo”, aseguró el titular de la cartera provincial, Danilo Capitani.

Y sumó: “Es verdad que la pandemia nos ha hecho tener que focalizarnos en otras prioridades como la cuestión alimentaria, pero hoy queremos cambiar esa ecuación y enfocar nuestro trabajo no solamente en el tema alimentario, que va a continuar, sino avanzar con Santa Fe Más y con otros programas que estamos llevando adelante de forma articulada con Nación, como el Potenciar Trabajo, el Banco de Herramientas o el programa Banco Solidario que estará próximamente formalizando convenios con las distintas comunas y municipios”.

“Es decir -continuó Capitani- mediante la capacitación poder vincular a los jóvenes con el mundo del trabajo; el elemento dignificador por excelencia”.

A su turno, el senador provincial Marcos Castelló manifestó que “creo que la historia y futuro empiezan a cambiar muchísimo cuando empezamos a tener estas oportunidades, sobre todo, cuando esa franja etaria que nos preocupa muchísimo, que son los jóvenes, empiezan a tener donde ocupar su mente, donde ocupar ese tiempo, que en algunos casos cuando es olvidado por el Estado, termina siendo tiempo ocioso”.

Por su parte, el secretario de Integración Social e Inclusión Socioproductiva, Gustavo Chara manifestó: “Una definición política del gobernador Omar Perotti es este programa, que tiene como eje en la instrumentación, en el territorio, la articulación con las instituciones intermedias, es decir, aquellos referentes sociales, como el cura del barrio, el director técnico del club de barrio, la directora de la escuela, quienes son los que sostienen afectivamente el tejido social en la reconstrucción, pensando en la post pandemia, sumando en esa articulación a los gobiernos locales que son actores fundamentales”.

Finalmente, la directora de Desarrollo Territorial, Julia Irigoitía, afirmó: “Santa Fe Más es una política pública destinada a jóvenes santafesinos que tiene por objetivo fundamental aportar herramientas para que los mismos se formen en determinados oficios que luego les permitan pensarse en el mundo del trabajo, a instancias de formación con esta lógica del aprender haciendo y que redunde al mismo tiempo en un beneficio para la comunidad de los barrios”.

## **CONVENIO MARCO**

De la firma de convenios, que tuvo lugar en el Auditorio del Ministerio de Desarrollo Social de la ciudad de Santa Fe, participaron el intendente de Santa Fe, Emilio Jatón; la intendenta de Santo Tomé, Daniela Qüesta; el intendente de Sauce Viejo, Pedro Uliambre; el intendente de Laguna Paiva, Elvio Cotterli; el presidente comunal de Llambi Campbell, Adrián Tagliari; el presidente comunal de Arroyo Aguiar, Hernán Carraro; el presidente comunal de Arroyo Leyes, Eduardo Lorinz; y el presidente comunal de Emilia, Esteban Panigo.

También estuvieron presentes el secretario de Políticas de Inclusión y Desarrollo Territorial, Fernando Mazziotta; el secretario de Planificación, Coordinación y Control, Santiago Lamberto; la directora de Proyectos Territoriales Socio-inclusivos, Susana Neponuceno; la directora de Desarrollo Territorial, Romina Sonzogni; entre otras autoridades.

## **SANTA FE MÁS**

El programa Santa Fe Más es una política de inclusión socio-productiva que propone la articulación entre sindicatos, empresarios y organizaciones de la sociedad civil con el Estado para generar instancias de formación dirigidas a una población de entre 16 y 30 años.

Propone establecer lazos socio afectivos y generar el sentido del trabajo colectivo, el esfuerzo y la solidaridad.

Los espacios formativos estarán distribuidos por todo el territorio y servirán como herramienta de acercamiento para diseñar estrategias de abordaje integrales. Así, se buscará reducir las violencias apostando a la generación de infraestructuras sociales para reconstruir los vínculos comunitarios y de una logística territorial que permita atender las demandas de cada lugar.