---
category: Agenda Ciudadana
date: 2020-12-12T13:12:10Z
thumbnail: https://assets.3dnoticias.com.ar/paso.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: NA'
resumen: Proyecto para suspender las PASO de 2021 ingresó en Diputados
title: Proyecto para suspender las PASO de 2021 ingresó en DIputados
entradilla: La propuesta cuenta con el aval de la mayoría de los mandatarios provinciales
  que la semana pasada le plantearon esa idea al presidente Alberto Fernández.

---
El diputado del Frente de Todos por Tucumán Pablo Yedlin presentó este viernes en la Cámara baja un **proyecto que plantea suspender las Primarias Abiertas, Simultáneas y Obligatorias (PASO) del próximo año**.

El legislador, cercano al gobernador de Tucumán, Juan Manzur, ingresó la propuesta que cuenta con el aval de la mayoría de los mandatarios provinciales que la semana pasada le plantearon esa idea al presidente Alberto Fernández.

"Suspéndase la aplicación del Título II de la Ley 26.571 para la selección por parte de las agrupaciones políticas de los candidatos a Diputados y Senadores para las elecciones generales del año 2021, quedando solo y automáticamente proclamados candidatos para las mismas los precandidatos de la única lista que cada agrupación política presente para ser oficializada", afirmar el artículo primero de la iniciativa.

En el segundo punto, el proyecto establece: "Suspéndase para el año 2021 toda disposición que se oponga a lo establecido en el artículo primero de la presente Ley, en cuanto le resulte incompatible".

  
 