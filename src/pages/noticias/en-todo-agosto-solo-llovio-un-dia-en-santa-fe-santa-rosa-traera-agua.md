---
category: La Ciudad
date: 2021-08-28T06:00:57-03:00
thumbnail: https://assets.3dnoticias.com.ar/NOLLUEVECHE.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: En todo agosto solo llovió un día en Santa Fe ¿Santa Rosa traerá agua?
title: En todo agosto solo llovió un día en Santa Fe ¿Santa Rosa traerá agua?
entradilla: 'El Servicio Meteorológico Nacional no da señales de que lleguen precipitaciones
  a la capital provincial. '

---
¿Habrá tormenta de Santa Rosa? Es la pregunta que muchos se hacen, a pocos días del 30 de agosto. Más allá de esta creencia popular, lo cierto es que, en la ciudad de Santa Fe, y en todo el Litoral, se esperan con ansias precipitaciones que den un poco de respiro a la crítica situación de sequía. 

Tan marcada es la falta de lluvias, que en todo agosto solo llovió un día. Según los datos del Centro de Informaciones Meteorológicas (CIM) de la FICH – UNL, el 8 de agosto se registraron "lluvias débiles en algunas zonas de la ciudad", indican las estadísticas del CIM, pero resaltan que esas lloviznas fueron tan escasas que no alcanzaron a incidir en el pluviómetro que mide los milímetros (mm) caídos. Por ende, en el transcurso de casi la totalidad del mes, la medición es de 0 mm.  

Desde el CIM indicaron que "el mes de agosto es el menos lluvioso de todo el año y ya hemos tenido algunos ´agostos\` sin lluvia alguna o con eventos similares al de este mes, con alguna llovizna en algunas partes de la ciudad". El promedio de lluvia mensual para este mes es de 43.25 mm y la cantidad media de días lluviosos es de seis.  

Para comparar los registros lluviosos del corriente mes y de años anteriores, en 2020 el registro de lluvias de agosto fue de 3 mm; 2019 (18.75 mm); 2018 (50 mm, superior a la media mensual); y el último agosto con 0 mm registrados fue el de 2014, también fue con registro nulo de lluvias el agosto de 1995. Otros años con una sequía extensa como la actual y una notoria bajante del río Paraná fueron 2008 y 2009, donde las lluvias de agosto fueron: 2.80 mm y 0.50 mm, respectivamente. 

**Ascenso de temperatura y posibles lluvias**

El Servicio Meteorológico Nacional (SMN) informó que al menos hasta el lunes 30, las condiciones previstas son de buen tiempo y los ascensos de temperatura van a predominar en gran parte de la zona central y norte del país.

En la ciudad de Santa Fe este fin de semana habrá una importante amplitud térmica, con mínimas que irán de los 6 a los 12°C y máximas que superarán los 25°C. Mientras que para el martes y miércoles el calor se sentirá más y se esperan temperaturas que alcancen y superen los 30°c. 

En cuanto a la posibilidad de la llegada de lluvia, desde el SMN destacaron: "A partir del martes 31, se espera que un frente frío comience a generar precipitaciones en forma de chaparrones y tormentas, en el norte patagónico y la provincia de La Pampa. Este sistema va a continuar desplazándose hacia el este en el transcurso de ese día, y entre el final del martes y la mañana del miércoles, la ´tormenta de Santa Rosa\` podría llegar a la Ciudad Autónoma de Buenos Aires y al sur del Litoral".