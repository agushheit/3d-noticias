---
category: Estado Real
date: 2021-02-01T06:58:17-03:00
thumbnail: https://assets.3dnoticias.com.ar/sfverano.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: 'Santa Fe Verano: Siguen los operativos provinciales de salud en puntos turísticos,
  recreativos y de gran tránsito de personas'
title: 'Santa Fe Verano: Siguen los operativos provinciales de salud en puntos turísticos,
  recreativos y de gran tránsito de personas'
entradilla: Se realiza detección de VIH, prevención de dengue, cáncer y de Covid.
  En 10 días testearon a 4.074 personas en la Terminal de Santa Fe

---
La provincia de Santa Fe, a través de los Ministerios de Salud, Desarrollo Social, Seguridad, la secretaría de Turismo, la agencia provincial de Cáncer, entre otras áreas, realiza diversas acciones de promoción de la salud, prevención de enfermedades, aplicación de vacunas de calendario, y brinda cuidados y consejería en diversas playas, plazas y terminales de ómnibus, en el marco del plan Santa Fe Verano.

De este modo, en el lapso que fue del lunes 25 al miércoles 27 de enero (jueves y viernes se suspendieron por lluvia), en el sur provincial se realizaron intervenciones en General Lagos, Arroyo Seco y Villa Constitución.

En los operativos de promoción, prevención y detección en Santa Fe, tanto en La Costanera como en un punto estratégico: La Terminal de Ómnibus. Al respecto el responsable de la Región Santa Fe de Salud, Rodolfo Rosselli, precisó que “en el lapso del 19 al 29 de enero en este punto fundamental de arribo y partida de personas, lugar de circulación crítico y necesario para abordar una vez establecidos los viajes interurbanos, se realizaron 4.074 de los cuales el solo 7 dieron positivo”. Asimismo en La Costanera Este, del 9 al 24 de febrero se realizaron 670 test de los cuales 2 dieron positivo.

En este sentido, explicó Rosselli “que las acciones de detección se acompañaron, tanto en la Playa como en la Terminal, con abordaje y acciones interministeriales de promoción, prevención y entrega de folletería sobre dengue, salud sexual, y otras enfermedades estacionales o crónicas”.

“En estas localidades se realizaron un total de 29 test rápidos de VIH, voluntarios y confidenciales; y 106 test de Covid, luego de entrevistar a 1.341 personas en más de 400 domicilios recorridos”, destacó Sebastián Torres, coordinador de Operativos Territoriales del Ministerio de Salud.

Asimismo, allí se aplicaron vacunas de calendario gratuito y obligatorio en personas que tenían sus esquemas incompletos. Fueron 100 las personas que se acercaron a los puestos de inmunización y a las que se les administraron 110 vacunas de calendario.

Mientras tanto, en la ciudad de Rosario continuaron los operativos de detección de Covid en La Florida. El lunes 25 se realizaron 50 test de Covid, de los cuales 8 resultaron positivos. También se inmunizó con vacunas de calendario a personas que tenían sus esquemas incompletos.

“En el puesto de vacunación de La Florida pasaron 80 personas, en las que se administraron 150 vacunas”, precisó al respecto Sebastián Torres.

Paralelamente, el lunes 25 en el barrio La Carolina de la comuna de Alvear, luego de un recorrido casa por casa, se determinó necesario realizar 23 test de Covid a personas con síntomas, o que refirieron contacto con casos positivos, resultando solo (1) un caso positivo.