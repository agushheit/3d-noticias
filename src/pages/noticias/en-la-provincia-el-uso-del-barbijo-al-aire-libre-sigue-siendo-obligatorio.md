---
category: Agenda Ciudadana
date: 2021-10-01T06:15:37-03:00
thumbnail: https://assets.3dnoticias.com.ar/BARBIJO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: En la provincia el uso del barbijo al aire libre sigue siendo obligatorio
title: En la provincia el uso del barbijo al aire libre sigue siendo obligatorio
entradilla: Así se anunció tras la reunión de las autoridades de salud de la provincia
  con el grupo de expertos. Analizaron además posibles flexibilizaciones.

---
La ministra de Salud, Sonia Martorano, encabezó este miércoles por la tarde una nueva reunión del Comité de Expertos en Salud de la provincia de Santa Fe. El mismo se desarrolló de manera virtual, y contó con la presencia del ministro de Gestión Pública, Marcos Corach y representantes de diferentes ámbitos sanitarios del territorio santafesino.

En el inicio de la charla, la ministra de Salud detalló la situación sanitaria que se vive en la provincia y puntualizó: “Tenemos un mapa completamente verde, eso quiere decir que toda la provincia está en bajo riesgo epidemiológico, lo que nos permite evaluar habilitaciones. Sin embargo, la prudencia que nos acompañó siempre tiene que ser nuestra mejor consejera, sin perder una mirada holística”.

Además, Martorano destacó el trabajo en conjunto: “Desde estas reuniones llevamos nuestra mirada al gobernador Omar Perotti que siempre se apoyó muchísimo en este grupo”.

La provincia de Santa Fe tendrá desde octubre nuevas medidas de convivencia en pandemia. El gobernador Omar Perotti firmará el nuevo decreto este viernes y para definir las nuevas aperturas y flexibilizaciones, las conclusiones de esta reunión son los ítems principales a tener en cuenta, entre los que se destacaron: el aforo para las distintas actividades ya habilitadas, el uso de barbijo contra el coronavirus y la apertura de espacios de esparcimiento.

En principio, la provincia mantendrá la obligatoriedad del tapabocas en vía pública y al aire libre, tal como habían adelantado las autoridades sanitarias locales y el propio gobernador Omar Perotti. En eso, Santa Fe se diferenciaría de las medidas comunicadas por Nación a partir del 1 de octubre.

En tanto, uno de los temas de mayor discusión en la reunión de expertos era si se habilitaba la apertura de boliches bailables. La opinión mayoritaria que había trascendido era la de esperar un tiempo más antes de dar luz verde a la actividad.

Martorano destacó además que el 54% de los santafesinos ya tiene dos dosis de la vacuna contra el coronavirus; actualmente hay solo tres personas internadas en unidades de terapia intensiva en Santa Fe, mientras que el promedio de casos diarios en los últimos 14 días es de 66 y la ocupación de camas provincial alcanza el 60%.