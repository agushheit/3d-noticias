---
category: Agenda Ciudadana
date: 2020-11-27T11:05:14Z
thumbnail: https://assets.3dnoticias.com.ar/senado-santa-fe.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: El Litoral'
resumen: Presupuesto 2021
title: Media sanción para el Presupuesto 2021
entradilla: 'El Senado santafesino votó por unanimidad el proyecto de ley de Presupuesto
  2021. '

---
El Senado santafesino votó por unanimidad el proyecto de ley de Presupuesto 2021. Le dio media sanción al mensaje del Ejecutivo, modificaciones en las que hubo negociaciones con funcionarios de la Casa Gris y también con diputados de la oposición.

Los 19 votos del cuerpo fueron a favor en el tratamiento en general y el expediente pasó a la Cámara de Diputados.

Sólo hubo diferencias de un sector del PJ (el más cercano al gobernador Perotti) en uno de sus 58 artículos, que refiere al Plan Incluir que reemplaza al Plan Abre (y se ha puesto en vigencia por un decreto, sin tener en cuenta que ya había una ley para el programa de la gestión anterior).