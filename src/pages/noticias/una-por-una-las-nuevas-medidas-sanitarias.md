---
category: Agenda Ciudadana
date: 2021-09-22T06:15:14-03:00
thumbnail: https://assets.3dnoticias.com.ar/HABILITAME.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Una por una, las nuevas medidas sanitarias
title: Una por una, las nuevas medidas sanitarias
entradilla: Desde el 1 de octubre se levanta el uso obligatorio del tapabocas al aire
  libre, se habilitan las reuniones al aire libre sin tope máximo de personas, suben
  los aforos y comienza una progresiva apertura de fronteras.

---
El Gobierno nacional anunció el fin de una serie de restricciones por la pandemia de coronavirus, a partir del 1 de octubre próximo, como el levantamiento de la obligatoriedad del uso de tapaboca al aire libre, la habilitación de reuniones sin tope máximo de personas, la vuelta del público a los estadios de fútbol con un aforo del 50 por ciento, y la apertura gradual y cuidada de fronteras.

Las medidas fueron anunciadas este martes a la mañana en una conferencia de prensa en la Casa de Gobierno por el jefe de Gabinete, Juan Manzur, y la ministra de Salud, Carla Vizzotti, quienes señalaron que la decisión se adoptó ante el descenso sostenido de casos de coronavirus en las últimas semanas, el retraso del ingreso de la variante Delta y el avance del plan de vacunación, entre las razones más importantes.

“Estamos en un momento de un descenso sostenido de casos de coronavirus" en todo el país, dijo la titular de la cartera de Salud y destacó que la Argentina "ha sido exitosa en contener el ingreso de la variante Delta”.

Manzur, en tanto, puntualizó que los datos epidemiológicos “son muy promisorios porque, si esto sigue en esta dirección, quiere decir que estamos transitando quizás la última etapa de la pandemia", y puntualizó que el stock de vacunas para inmunizara la población "está asegurado".

En su primera conferencia de prensa en la Casa Rosada, el jefe de Gabinete dijo que, de acuerdo con el "comportamiento de los indicadores" sanitarios, hay "datos objetivos que dan que de más de 26 mil casos en mayo hoy en septiembre estamos por debajo de los 1.600, con una cobertura de vacunación en la que estamos muy cerca de poder llegar al 50%, que es el nivel óptimo sugerido por las autoridades sanitarias”.

Ante los resultados que arrojan los indicadores, “podemos tomar algunas decisiones que tienen que ver con la flexibilización de las restricciones vigentes hasta hoy", sostuvo Manzur, pero hizo hincapié en la necesidad de mantener “la responsabilidad social frente a la epidemia” y dijo que “es central mantener los cuidados".

Sobre la campaña de vacunación, Manzur indicó que "estamos a poco más de 2 millones de dosis para poder llegar al 50%" de población inmunizada y dio certezas acerca de que "las vacunas están, está el stock asegurado y en los próximos 15 ó 20 días estaremos alcanzando el nivel óptimo de cobertura".

Subrayó que ese nivel de vacunación se logró por el "esfuerzo enorme que hizo nuestro gobierno en conseguir las vacunas, en poder aplicarlas y distribuirlas y estos son los resultados que hoy tenemos", y que, en consecuencia, los resultados indican la “posibilidad cierta de que a corto plazo podamos seguir profundizando las flexibilizaciones".

En relación a la situación epidemiológica, Vizzotti dijo que “esta semana es la 16 consecutiva de baja en el número de casos diarios de coronavirus y la 14 consecutiva en reducción de muertes e internaciones".

Al enumerar las medidas más importantes, enumeró que se levanta la obligatoriedad de uso de tapabocas al aire libre, circulando y sin personas alrededor, aunque sigue vigente la exigencia “en los espacios cerrados y actividades donde haya mucha gente alrededor, también al aire libre cuando estamos reunidos con personas”.

También se autoriza la apertura de discotecas, con aforos de 50%, para personas que hayan completado el esquema de vacunación hace al menos 14 días, y desde el 1 de octubre se permitirá el ingreso de extranjeros de países limítrofes sin necesidad de aislamiento, mientras que para el 1 de noviembre próximo está planificado autorizar el ingreso de extranjeros de naciones no limítrofes, entre otras flexibilizaciones.

La ministra también indicó que la asistencia de público en los eventos masivos al aire libre será autorizada a partir del 1 de octubre con un aforo del 50 por ciento.

Todos los eventos masivos, incluyendo los partidos de fútbol, se habilitan con un 50% de aforo, dijo y comentó que “ahora la tarea, tal cual se procedió en el partido piloto de Argentina-Bolivia, es un trabajo y mesa conjunta con Seguridad, Deportes, la Asociación de Fútbol Argentino y en este caso con la Liga y por supuesto en todas las categorías, y también con cada una de las jurisdicciones”.

Al precisar detalles de la situación sanitaria por el coronavirus, la ministra señaló: “Tenemos un descenso del promedio diario del número de casos, estamos en el momento más bajo, ya que en febrero pasado se registró un promedio diario de 6.163 casos, mientras que en este mes de setiembre estamos en menos de 1.700 casos”.

“La gran mayoría de las jurisdicciones departamentales están en bajo riesgo y el resto está en mediano, o con muy poco riesgo; además es la semana 14 en la que la mortalidad está disminuyendo, mientras que las internaciones en terapia intensiva también están en la semana 14 de descenso sostenido”, aseveró.

Finalmente, afirmó que la Argentina “ha sido exitosa en contener y atrasar el ingreso de la variante Delta” e indicó que en este momento “tenemos casos comunitarios en algunas jurisdicciones”, sobre los que se ha trabajado en la semana pasada con todas las áreas de epidemiología y el Consejo Federal de Salud para atrasar lo máximo posible la circulación predominante”.

"Desde las 7.30, mantuve una reunión con la Ministra de Salud, Carla Vizzotti, para analizar la situación actual de la pandemia, el estado de las coberturas de inmunización y los enormes avances que se están logrando en la campaña de vacunación más grande de la historia argentina", precisó Manzur en su cuenta de Twitter.

![](https://assets.3dnoticias.com.ar/INFO.jpg)