---
category: La Ciudad
date: 2021-10-30T06:00:32-03:00
thumbnail: https://assets.3dnoticias.com.ar/estafasciber.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: El porcentaje de estafas en la ciudad de Santa Fe es "alto"
title: El porcentaje de estafas en la ciudad de Santa Fe es "alto"
entradilla: Desde la Defensoría del Pueblo realizaron una serie de recomendaciones
  para el cyber Mondey que tendrá lugar del 1 al 3 de noviembre.

---
El CyberMonday es un evento que organiza la Cámara Argentina de Comercio Electrónico durante los días 1,2 y 3 de noviembre. En el mismo participan distintas empresas con ofertas apetecibles para los consumidores.

Para poder aprovechar los beneficios y tener una buena experiencia, en este evento donde se promocionan ofertas, los consumidores deben estar atentos a verificar si realmente los productos tienen un precio rebajado, así como prestar atención a los consejos sobre seguridad.

 La subdirectora de Usuarios y Consumidores de la Defensoría del Pueblo, Adriana Garrido, compartió con El Litoral algunos puntos a tener en cuenta:

 “Por un lado el usuario tiene que haber seguido el precio del bien o servicio que quiere comprar o contratar”. Por otro lado, “el beneficio en este tipo de eventos se da en que te dan cuotas fijas -el valor real del bien dividido en cuotas – por lo tanto el consumidor tiene que fijar la atención en ver si las cuotas tienen intereses, si son accesibles para la tarjeta de crédito de cada uno”.

 Otras recomendación fundamental es “entrar en los sitios oficiales de las marcas, esto da cierto grado de seguridad para quienes van a realizar la compra”, dijo Garrido. “Hacer la compra con tarjeta de crédito da un marco más de seguridad ya que el consumidor tiene un plazo de 30 días a partir del cierre del resumen de la tarjeta para desconocer la compra que hizo en caso de que no esté bien liquidada o le haya llegado otro tipo de cargo”.

 Al ser consultada sobre las estafas en la ciudad de Santa Fe, Garrido explicó que “el porcentaje es alto, en general esta utilización y duplicación de tarjetas de débito, de crédito, por eso siempre recomendamos usar la tarjeta de crédito”.

 

 

Para resaltar, las garantías por ley son de seis meses después la marca puede extender la garantía a un plazo mayor. Recibido el producto se tiene 10 días para la devolución en caso de que haya llegado roto, que no cumpliera sus expectativas, etc y vuelve del mismo modo en que llegó, por el mismo correo, a cargo del proveedor”, finalizó la Subdirectora de Usuarios y Consumidores.

Noticia de: El Litoral (www.ellitoral.com) \[Link:[https://www.ellitoral.com/index.php/id_um/326030-el-porcentaje-de-estafas-en-la-ciudad-de-santa-fe-es-alto-cybermonday-area-metropolitana-cybermonday.html](https://www.ellitoral.com/index.php/id_um/326030-el-porcentaje-de-estafas-en-la-ciudad-de-santa-fe-es-alto-cybermonday-area-metropolitana-cybermonday.html "https://www.ellitoral.com/index.php/id_um/326030-el-porcentaje-de-estafas-en-la-ciudad-de-santa-fe-es-alto-cybermonday-area-metropolitana-cybermonday.html")\]