---
category: La Ciudad
date: 2021-07-30T06:15:03-03:00
thumbnail: https://assets.3dnoticias.com.ar/LISIPADO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Cervezas y descuentos en bares para personas vacunadas
title: Cervezas y descuentos en bares para personas vacunadas
entradilla: Algunos bares promocionan el "2X1" en pintas de cervezas a quienes muestren
  el carné de vacunación; otros aplican descuentos en lisos.

---
La iniciativa de canjear cervezas en los bares a todas aquellas personas que exhiban el carné de vacunación, comenzó a observarse en algunas ciudades del país con el objetivo de fomentar la inoculación de los jóvenes. Desde hace unos días, la movida llegó a la ciudad de Santa Fe y poco a poco, cada vez más bares se adhieren a dicha iniciativa.

Desde hace algunos días, un bar de barrio Candioti Sur, ubicado en calle Las Heras al 3.400, sorprendió en las redes sociales con un mensaje directo: "vacunación de cervezas". Lo cierto es que, con carné de inoculación en mano, las personas pudieron disfrutar de una pinta de cerveza gratis.

Desde este viernes, otro reconocido bar de la Recoleta santafesina, emplazado en la esquina de Bulevar Gálvez y San Luis, se sumará también a la iniciativa que busca fomentar la vacunación en personas que aún no se han registrado para la inoculación. Este emprendimiento gastronómico dejará el precio del popular liso santafesino a $40 a todo aquel que presente el certificado de vacunación. "Vacunar salva vidas y nosotros como empresarios gastronómicos santafesinos queremos ayudar a concientizar a los jóvenes a vacunarse", sostuvo el referente de 1980 Boulevard.

"La idea surge de la realidad que estamos viendo en la sociedad en relación a la pandemia de coronavirus y a la falta de compromiso de los jóvenes con la vacunación por distintos motivos. Como administradores de un bar, que estamos en contacto permanente con este grupo de personas, creímos atinado en nuestra responsabilidad social llevar adelante algún tipo de acción para fomentar la inoculación", manifestó Matías, de Dalton.

"En este contexto, se nos ocurrió, jugando con la analogía de las dos dosis de las vacunas, canjearlas por una pinta de cerveza. Con la compra de la primera dosis, que sería la primera cerveza, nosotros le regalamos la segunda dosis, que sería la segunda cerveza", subrayó Matías y continuó agregando: "Para acceder al beneficio, le pedimos al cliente presentar el carné de vacunación que emite el Ministerio de Salud de la provincia o alguna foto en redes sociales que se demuestre la aplicación de una o dos dosis. A cambio, le brindamos un carné simbólico de la \`vacunación de cerveza´".

"Entendemos que el rubro gastronómico fue uno de los más golpeados con las restricciones en pandemia y de este modo, fomentando la vacunación de los jóvenes, aportamos nuestro granito de arena para ir volviendo de a poco a la normalidad, con más gente vacunada día a día", finalizó el referente de Dalton bar.