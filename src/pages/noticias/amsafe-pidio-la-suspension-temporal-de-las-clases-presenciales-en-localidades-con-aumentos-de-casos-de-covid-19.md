---
category: Agenda Ciudadana
date: 2021-04-13T08:15:54-03:00
thumbnail: https://assets.3dnoticias.com.ar/amsafe.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Diario Uno
resumen: Amsafé pidió la suspensión temporal de las clases presenciales en localidades
  con aumentos de casos de Covid-19
title: Amsafé pidió la suspensión temporal de las clases presenciales en localidades
  con aumentos de casos de Covid-19
entradilla: 'Así lo informó la Comisión Directiva provincial de Amsafé a raíz del
  aumento de casos de coronavirus y la llegada de la segunda ola de contagios.

'

---
En un escueto comunicado, la Comisión Directiva provincial de Amsafé le solicitó al Ministerio de Educación de Santa Fe la suspensión temporal de las clases presenciales en las localidades en que el aumento de casos de coronavirus.

"Manifestamos nuestra preocupación ante el aumento de casos de Covid-19, la circulación comunitaria y la aparición de nuevas cepas. Todo esto hace necesario intensificar los cuidados individuales y colectivos", comienza el comunicado de Amsafé.

"Es imprescindible cuidar la salud de docentes, estudiantes, asistentes escolares, personal de comedores y comunidad. Por lo que solicitamos al Ministerio de Educación la suspensión temporal de las actividades presenciales en las localidades en que el aumento de casos se ha dado de manera exponencial en los últimos 14 días. Este nuevo escenario debe considerarse de alto riesgo sanitario para toda la población", continúa el escrito.

"Es de suma importancia que las autoridades tomen medidas restrictivas en los distintos ámbitos que impidan la propagación del virus en resguardo de la salud de la población", expresaron de Amsafé y finalizaron: "Por todo lo expuesto, entendemos que en los lugares de alto riesgo se deben realizar actividades a distancia hasta que estén dadas las condiciones para un retorno sano y seguro a las escuelas".

**Consejo Federal de Educación**

El martes de la semana pasada, se reunió el Consejo Federal de Educación, encuentro del cual participaron los ministros de dicha cartera de todo el país. En un contexto de posibles nuevas restricciones, el cónclave tuvo como una de uno de los temas centrales la continuidad o no de la presencialidad en el dictado de clases.

La ministra de Educación de Santa Fe, Adriana Cantero, advirtió que "la escuela no es un lugar donde se producen los contagios. La escuela es un lugar muy cuidado".

La titular de la cartera educativa santafesina pidió "que toda la comunidad intensifique los cuidados en otros ámbitos, donde circula (el virus). Fundamentalmente en las reuniones sociales que está demostrado que son los lugares de mayor contagio. También una reflexión a los padres por los viajes de estudiantes".