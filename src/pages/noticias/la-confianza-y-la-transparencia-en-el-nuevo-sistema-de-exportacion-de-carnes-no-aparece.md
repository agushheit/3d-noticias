---
category: El Campo
date: 2022-01-31T09:25:47-03:00
thumbnail: https://assets.3dnoticias.com.ar/090121-carne.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa SRA
resumen: "«La confianza y la transparencia en el nuevo sistema de exportación de carnes
  no aparece»"
title: "«La confianza y la transparencia en el nuevo sistema de exportación de carnes
  no aparece»"
entradilla: Lo afirmo el presidente de la Sociedad Rural Argentina Nicolás Pino, en
  la expo de Junín de los Andes

---
En el marco de la tradicional Expo Rural que se llevó adelante entre el 26 y el 30 de enero, el presidente de la Sociedad Rural Argentina, Nicolás Pino, participó del evento acompañado por los directores de la entidad Marina Facht, Adela Bancalari, Raúl Etchebehere, Jorge Born y Mariano Andrade, además de socios, productores y funcionarios. Pino se refirió a la situación actual del sector local y nacional.

En el discurso del acto inaugural habló del efecto negativo de la intervención de los mercados y se refirió al atentado que esto genera en la competitividad comercial de los distintos eslabones de la cadena. En este sentido dijo: **“las desacertadas políticas implementadas en los últimos meses no han hecho más que poner palos en la rueda del desarrollo y postergar el crecimiento del sector, y por consiguiente del país”**.

Respecto al mercado de ganados y carnes señaló que las medidas implementadas por el gobierno durante el año pasado, lejos de dejar atrás el cepo y la cuotificación para la exportación, solo acrecentaron los rumores de nuevas restricciones informales.

**“Las exportaciones de carne entre junio y diciembre de 2021 cayeron casi 135 mil toneladas respecto al mismo período en 2020. Una caída acumulada del 24% que se tradujo en la pérdida de 520 millones dólares menos de exportaciones. La confianza y la transparencia en el nuevo sistema aplicado este año no aparece y los productores vemos con suma preocupación la situación actual”**, afirmó.

Además se refirió a temas locales, como la sequía que atraviesa la zona hace años, el constante flagelo de los incendios y la importancia de la acción judicial y política ante las amenazas constantes a la propiedad privada.

Finalmente hizo mención a la cuestión de las retenciones e indicó que se tratan de **“impuestos distorsivos que recaen sobre la línea de flotación de la actividad productiva”** y que además se han quedado sin sustento legal desde el 1ero. de enero.