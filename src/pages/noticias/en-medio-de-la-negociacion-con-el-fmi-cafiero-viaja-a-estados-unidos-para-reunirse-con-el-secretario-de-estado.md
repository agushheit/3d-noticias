---
category: Agenda Ciudadana
date: 2022-01-17T06:15:26-03:00
thumbnail: https://assets.3dnoticias.com.ar/CABI.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: En medio de la negociación con el FMI, Cafiero viaja a Estados Unidos para
  reunirse con el secretario de Estado
title: En medio de la negociación con el FMI, Cafiero viaja a Estados Unidos para
  reunirse con el secretario de Estado
entradilla: 'El encuentro del canciller con Blinken está programado para el 18 de
  enero. Será "eminentemente político y no técnico" sobre el acuerdo con el FMI, según
  deslizaron en la Cancillería.

  '

---
En medio de la negociación con el Fondo Monetario Internacional (FMI), el canciller Santiago Cafiero llegaba este lunes a Estados Unidos para reunirse el próximo martes con el secretario de Estado, Antony Blinken, uno de los funcionarios más influyentes de la administración de Joe Biden.

Así lo indicaron a NA fuentes de la Cancillería argentina, que precisaron que el jefe del Palacio San Martín tenía previsto aterrizar en Washington cerca de las 9:30 del lunes.

El encuentro entre Cafiero y Blinken, que está previsto para el próximo 18 de enero, se concretará días después de que la Argentina asumió la Presidencia Pro Tempore 2022 de la Comunidad de Estados Latinoamericanos y Caribeños (Celac), un foro regional del que Estados Unidos no forma parte.

"La bilateral estaba prevista. Nada tienen que ver con la Celac", señalaron desde el Ministerio de Relaciones Exteriores, al negar que el cónclave haya sido coordinado por un posible malestar de Estados Unidos ante la asunción del presidente Alberto Fernández al frente de la Celac.

Ante la consulta de Noticias Argentinas respecto de la posibilidad de que la agenda de Cafiero incluya un encuentro con Jake Sullivan, asesor de Seguridad Nacional de la Casa Blanca, respondieron: "Todavía no está confirmado".

En cuanto al encuentro con Blinken, fuentes diplomáticas aclararon que se trata de "una reunión eminentemente política y no técnica para discutir el acuerdo del FMI".

En ese punto, analizaron que "el problema de la deuda también es político, ya que el ex presidente de Estados Unidos (Donald Trump) autorizó por un interés político un crédito que el staff del FMI no autorizaba al ex mandatario argentino (Mauricio Macri)". Y agregaron: "El 2021 demostró con sus datos económicos que el planteo de Alberto de déjennos crecer para poder pagar es válido".

Además, detallaron a NA que la reunión bilateral estaba prevista para el pasado 4 de enero, en el marco de la reunión anual del Tratado de No Proliferación de Armas Nucleares en Nueva York, pero no fue posible porque el encuentro se suspendió ante la nueva ola de coronavirus.

"Es muy importante la reunión porque es la primera de alto nivel entre ambos cancilleres. Se va a revisar toda la agenda bilateral entre ambos países, que viene muy bien", puntualizaron.

Según revelaron a Noticias Argentinas fuentes cercanas al canciller, la estadía de Cafiero en Washington tendrá una duración de 48 horas, ya que en la noche del próximo 19 de enero emprenderá su regreso a Buenos Aires.

El ministro de Relaciones exteriores arribaría a la Argentina el próximo 20 de enero a las 8 para "terminar de pulir la agenda del viaje a China", cuyo destino visitará junto al presidente Alberto Fernández durante los primeros días de febrero.

"Santiago viaja sólo", enfatizaron desde la Cancillería, y confiaron que el funcionario nacional se hospedará en la residencia del embajador argentino en Estados Unidos, Jorge Argüello.