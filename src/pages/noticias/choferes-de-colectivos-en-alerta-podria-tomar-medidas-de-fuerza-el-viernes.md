---
category: La Ciudad
date: 2021-04-15T06:47:55-03:00
thumbnail: https://assets.3dnoticias.com.ar/colectivos.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Choferes de colectivos en alerta: podría tomar medidas de fuerza el viernes'
title: 'Choferes de colectivos en alerta: podría tomar medidas de fuerza el viernes'
entradilla: Reclaman el pago de haberes para antes de este viernes y anuncian que
  de no recibirlos tomarán medidas de fuerza.

---
La Unión Tranviarios Automotor (UTA) Seccional Santa Fe, informó este mediodía que tomarán medidas de fuerza si no perciben la faltante de haberes que no fue depositada aún.

En un comunicado, explican: "si para el día viernes 16 de Abril del corriente a media mañana, no se deposita el faltante de los haberes correspondientes al mes de Marzo, se iniciaran acciones gremiales que consisten en asambleas en las distintas cabeceras del sector Urbano e Interurbano de la ciudad".

Cabe recordar que hubo paros en febrero y advertencias en marzo por esta misma problemática. En ese momento se reclamó el pago de la mitad del sueldo a esta misma altura del mes. Finalmente se resolvió y los colectiveros volvieron a sus funciones.