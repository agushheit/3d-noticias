---
category: Agenda Ciudadana
date: 2021-07-18T06:15:43-03:00
thumbnail: https://assets.3dnoticias.com.ar/sputnikVACINE.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Sputnik V: envían turnos de vacunación de la segunda dosis'
title: 'Sputnik V: envían turnos de vacunación de la segunda dosis'
entradilla: Lo confirmó este sábado la ministra de Salud, Sonia Martorano, quien destacó
  que el vienes "se colocaron casi 44 mil dosis, una cifra récord" en Santa Fe.

---
La ministra de Salud Sonia Martorano confirmó que este fin de semana van a estar enviando los turnos de vacunación para los que fueron inoculados con la Sputnik V hasta el 6 de abril para que se aplique la segunda dosis, y adelantó: “Este lunes nos llegan 42.000 dosis del segundo componente, que nos van a permitir avanzar en la vacunación". De esa cifra, 8.000 son para la ciudad de Santa Fe.

“Tenemos una buena historia de vacunación, no hay tanto antivacuna”, afirmó este sábado Martorano y detalló: “Llegamos al 1.800.00 con la primera dosis y anotados hay 2.020.000, así que a fin de julio estaríamos con la primera dosis colocada a todos los santafesinos y santafesinas y avanzando con la segunda dosis”

Martorano, más allá de la preocupación inicial sobre la respuesta de los jóvenes a la vacunación, dijo que hay “un buen número de anotados” y aclaró que si no se dio la cantidad esperada no fue porque no quisieran vacunarse sino “por pensar que la vacuna era par la tía o la abuela y no que les había llegado el turno a ellos”.

“Estamos en una vacunación masiva, el viernes se colocaron casi 44 mil dosis, una cifra récord”, destacó Martorano, en relación a la reticencia que los más jóvenes tuvieron a inocularse, y advirtió que la provincia de Santa Fe tiene “una buena historia de vacunación” y se mostró confiada en que el ritmo de la campaña se mantendrá porque “no hay tanto antivacuna”.

**Gobierno de la Provincia de Santa Fe**

“Estamos con tres vacunas en este momento, Sionopharm, AstraZeneca y Sputnik, las tres muy buenas”, explicó Martorano en Radio 2, y añadió: “Sinopharm tiene la ventaja de que a las tres semanas de inoculado te van a llamar para la segunda dosis, porque es el periodo de esta vacuna y porque las dos dosis son iguales y podemos cumplir el esquema de vacunación”

“AstraZeneca estaba en doce semanas, pero con la aparición de la variante delta se recomendó bajar a ocho semanas, así que al que le pusieron la vacuna de AstraZeneca quizás unos días antes de las ocho semanas va a recibir el turno para la segunda aplicación, las dos dosis son iguales y tenemos, así que no hay problemas”, continuó.

**"La Sputnik es el cuello de botella"**

“La Sputnik es nuestro cuello de botella”, admitió Martorano, y añadió: “Colocamos la primera dosis, que tiene un 77% de cobertura, muy alto, nuestro problema es que no nos está llegando y es un componente diferente. Este lunes nos llegan 42.000 dosis del segundo componente, y vamos a vacunar a los que se inocularon hasta el 6 de abril”.

Asimismo, destacó como una “buena noticia” que el Instituto Gamaleya haya autorizado al laboratorio Richmond para realizar la producción local del segundo componente de la vacuna Sputnik V. “Esto nos va a permitir avanzar en la vacunación y también hay que tener en cuenta que se está estudiando la combinación de vacunas”, recordó Martorano.

“Creemos que la otra semana nos van a estar enviando un nuevo cargamento del componente 2 de la vacuna Sputnik y también se empieza en breve a producir en la Argentina y están avanzados para combinar con segunda dosis de Sinopharm y AstraZeneca y también con Moderna”, concluyó la ministra.