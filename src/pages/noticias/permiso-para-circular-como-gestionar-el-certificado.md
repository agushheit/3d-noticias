---
category: Agenda Ciudadana
date: 2021-05-20T09:23:14-03:00
thumbnail: https://assets.3dnoticias.com.ar/app-covid.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de LT10
resumen: 'Permiso para circular: cómo gestionar el certificado'
title: 'Permiso para circular: cómo gestionar el certificado'
entradilla: Las nuevas restricciones establecen que solo pueden circular aquellos
  que son considerados esenciales y trabajadores de las actividades habilitadas, y
  para eso deben contar con una correspondiente autorización.

---
El último decreto provincial establece nuevas restricciones para frenar el avance del coronavirus. Para lo mismo se estableció que hasta el 30 de mayo, sólo pueden circular aquellos que son considerados esenciales, trabajadores de las actividades habilitadas o que por causas de fuerza mayor deben realizar alguna gestión, incluyendo asistencia médica, farmacéutica y cuidado de personas.

Para circular deberán llevar la documentación respaldatoria y la app provincial COVID19 o app CUIDAR.

La aplicación CUIDAR se encuentra disponible en el playstore tanto para dispositivos iOS como Android, y para registrarse solo es necesario habilitar la geolocalización e ingresar el DNI.

![](https://assets.3dnoticias.com.ar/0003.jpg)

La aplicación permite la autoevaluación de síntomas en pocos pasos y suma la posibilidad de añadir al Certificado Único Habilitante de Circulación (CUHC), un código que muestra que el ciudadano o la ciudadana está habilitado/a para movilizarse, siempre y cuando en el autoexamen no se detecten síntomas de covid-19.

Para registrarse solo es necesario habilitar la geolocalización e ingresar el DNI y número de trámite que se encuentra en la parte inferior del documento. Luego deberán escanearlo y allí comenzará el autoexamen.

En casos de autodiagnósticos que no tengan síntomas compatibles con Covid-19, la app recomienda realizar un nuevo testeo pasadas 48 horas e insiste en la necesidad de respetar el aislamiento social, preventivo y obligatorio.

Si el usuario realiza una tarea esencial, el resultado del testeo se vincula automáticamente con el CUHC en la misma app a través de un código QR; o puede mostrarlo junto al certificado para circular emitido por el empleador.

**Paso a paso, cómo gestionar online el nuevo certificado de circulación**

El nuevo Certificado Único Habilitante para Circulación deberá ser tramitado a través de la aplicación CuidAr o en la página web [https://www.argentina.gob.ar/circular:](https://www.argentina.gob.ar/circular: "https://www.argentina.gob.ar/circular:")

\- Ingresá a la página web o a la aplicación. Recordá que, sólo en casos de emergencia o pedidos especiales, la autorización tiene una validez de 48 horas. 

\- Bajar hacia el final del texto y detenerse en la consigna: “Primero informanos si tenés DNI”. Allí clickear en “Sí” y luego clickear en “Continuá".

\- Seleccionar la provincia en la que uno reside. Luego, volver a clickear en “Continuá”.

\- Especificar el motivo por el cual uno solicita la excepción. Puede ser para trasladarse al trabajo o por otros motivos de fuerza mayor, como asistir a personas con dificultades de movilidad o personas mayores. En caso de elegir el traslado al trabajo, se abrirá una nueva solapa, donde se requiere una especificación más detallada sobre el rubro laboral del solicitante.

**Una vez rellenados esos puntos, clickear en “Completar el Formulario”.**

\- Completar el formulario y prestar atención a cada detalle que se le solicita. Una vez finalizado, es importante marcar la penúltima casilla que tiene la frase “No soy un Robot", y luego sí clickear “Solicitar Certificado”.

\- Al cabo de unos minutos, le llegará la confirmación de que su solicitud de permiso de circulación fue registrada correctamente. El propio sistema se lo confirmará en la misma página web. En el plazo de 24 horas, cada solicitante podrá consultar el estado de su certificación en la misma web.

\- Una vez completado el certificado, hay que regresar a la página inicial [https://www.argentina.gob.ar/circular](https://www.argentina.gob.ar/circular "https://www.argentina.gob.ar/circular") y clickear el botón verde con la frase “¿Ya complestaste el formulario? Descargalo”. Una vez realizado ese trámite, volver a introducir el Nº de DNI y de Trámite del DNI (está inscripto en el mismo DNI) para poder recibir el comprobante de la solicitud.