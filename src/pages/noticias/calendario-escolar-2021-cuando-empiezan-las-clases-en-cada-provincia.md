---
category: Agenda Ciudadana
date: 2020-12-01T11:07:09Z
thumbnail: https://assets.3dnoticias.com.ar/foto-escuelas.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Infobae'
resumen: 'Calendario escolar 2021: cuándo empiezan las clases en cada provincia'
title: 'Calendario escolar 2021: cuándo empiezan las clases en cada provincia'
entradilla: A excepción de la Ciudad de Buenos Aires y Jujuy, todos los distritos
  comenzarán en marzo con el objetivo de “recuperar la presencialidad”.

---
Después de un año en que las aulas estuvieron cerradas, las provincias diagramaron el inicio del ciclo lectivo 2021. Con excepción de la Ciudad de Buenos Aires y Jujuy, todos los distritos comenzarán las clases en marzo, tal como es habitual. El cierre del año escolar será a mediados de diciembre.

La gran inquietud de las familias es la modalidad de ese comienzo. Según indicaron desde el Ministerio de Educación nacional, hay consenso a nivel federal para que “la presencialidad sea la regla” desde el año próximo. Los ciclos lectivos 2020 y 2021 se unieron en una misma unidad pedagógica y, por ende, también será el momento de acreditar los aprendizajes que quedaron pendientes.

Tal como había anticipado su ministra de Educación, Soledad Acuña, la Ciudad de Buenos Aires se desmarcará del resto del país y comenzará diez antes, el 17 de febrero. Unos pocos días después, el 21 de febrero, se pondrá en marcha Jujuy, otra de las provincias que gobierna Juntos por el Cambio. El resto de los distritos comenzará entre la primera y segunda semana de marzo.

De la mano con el calendario, la cartera educativa nacional presentó el documento “A las aulas”, que traza un “plan de trabajo 2021 con el objetivo de garantizar la plena presencialidad en el sistema educativo argentino”. Allí se describen las propuestas pedagógicas a desarrollar en el verano para fortalecer los contenidos o recuperar el vínculo con la escuela, además de las actividades que tienen como objetivo disminuir el fuerte abandono escolar que se espera tras la ausencia prolongada en las escuelas.

![](https://assets.3dnoticias.com.ar/CALENDARIO-ESCOLAR.jpg)

“El 2020 fue un año de trabajo intenso, en el que escuelas, docentes y familias sostuvieron un enorme esfuerzo por enseñar y aprender en el marco de la continuidad pedagógica. Los compromisos asumidos para asegurar el regreso seguro a las aulas y sostener políticas escolares, de cuidado y acompañamiento, han sido plasmados en los protocolos, los acuerdos pedagógicos federales y en la inversión destinada a infraestructura escolar. Inversión que se verá incrementada e implica un presupuesto nacional que vuelve a poner a la educación como prioridad. El 2021 nos encuentra preparados. Será un año de fortalecimiento y recuperación de la presencialidad y del presupuesto”, afirmó el ministro de Educación, Nicolás Trotta.

En una entrevista en América, el ministro se refirió a un cambio de enfoque que tendrá lugar el año que viene a partir de la evidencia en torno a los contagios en niños. “Estábamos en el medio de una pandemia y la evidencia nos daba dudas. Europa aprendió en la segunda oleada que había que tener mayor apertura en el mundo laboral y que tienen que estar abiertas las escuelas como factor de organización social. La evidencia fue construyendo que el nivel de contagio no es marcado en los niños y en las niñas, pero no es lo mismo que decía la OMS en agosto”, justificó.

El Consejo Federal de Educación anticipó, mediante resoluciones aprobadas por unanimidad, los modos de avanzar en los objetivos planteados a nivel curricular. Habrá una reorganización de los contenidos que conformarán la unidad pedagógica 2020-2021. Los aprendizajes alcanzados durante 2020 serán acreditados: pero sin calificaciones numéricas. Si bien no habrá una promoción automática, ningún alumno repetirá este año. Los saberes que queden pendientes se acreditarán recién en 2021.

“La evaluación formativa sostenida a lo largo de ese proceso permitió realizar devoluciones a estudiantes y familias, acompañar individualmente a las y los estudiantes que presentaban mayores dificultades y producir como cierre de este año, información a los fines de orientar las trayectorias educativas en el año escolar siguiente”, sostienen desde la cartera educativa en un comunicado.

Según los datos obtenidos por la Evaluación Nacional de la Continuidad Pedagógica, alrededor del 10% de las y los estudiantes matriculados en marzo 2020 en algún nivel de la educación obligatoria mantuvo un bajo o nulo intercambio con su escuela. Más de un millón de estudiantes son los que corren mayor riesgo de abandonar la escuela. Ante esa problemática, Nación y las provincias implementarán el Programa Acompañar: Puentes de igualdad que tiene como finalidad identificar, revincular y atender de manera específica a esa población.