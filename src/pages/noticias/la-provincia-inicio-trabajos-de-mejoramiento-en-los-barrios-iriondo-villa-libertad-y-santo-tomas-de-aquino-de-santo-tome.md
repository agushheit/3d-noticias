---
category: Agenda Ciudadana
date: 2021-04-06T07:23:06-03:00
thumbnail: https://assets.3dnoticias.com.ar/licitación-santoto.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: La Provincia inició trabajos de mejoramiento en los barrios Iriondo, Villa
  Libertad y Santo Tomás de Aquino de Santo Tomé
title: La Provincia inició trabajos de mejoramiento en los barrios Iriondo, Villa
  Libertad y Santo Tomás de Aquino de Santo Tomé
entradilla: Las obras comprenden la construcción de desagües pluviales y cloacales,
  tendido eléctrico, calles y cordones cunetas y un edificio comunitario. La inversión
  provincial supera los 300 millones de pesos.

---
El Ministerio de Infraestructura, Servicios Públicos y Hábitat reinició las obras de mejoramiento e infraestructura en los barrios Iriondo, Villa Libertad y Santo Tomás de Aquino, de la ciudad de Santo Tomé, las que comprenden la construcción de desagües pluviales y cloacales, tendido eléctrico, estabilizado de calles, cordones cunetas y un nuevo edificio comunitario, beneficiando a 37 manzanas y directamente a unos 720 lotes.

Al respecto, el secretario de Hábitat, Amado Zorzón, explicó que la obra fue paralizada en 2019 por la gestión anterior, pero que “el gobernador Omar Perotti nos pidió que trabajemos para retomarla cuanto antes. Y así lo hicimos, gestionando con la empresa que tenía a cargo la obra (Ponce Construcciones) la cesión de la misma para que la continúe la empresa de Ángel Boscarino, en este caso. Este traspaso llevó más tiempo de lo previsto, con las dificultades propias de la pandemia, pero hoy estamos felices por reiniciar los trabajos que beneficiarán a miles de santotomesinos”.

“La obra actualmente está en un 50%, por lo que resta ejecutar la construcción de una bomba de elevación y otra de impulsión para terminar con la red cloacal, y por otra parte, se necesita avanzar con el desagüe pluvial que requiere la ejecución de bocas de tormenta y la canalización correspondiente. El monto de inversión del gobierno provincial en estas obras supera los 300 millones de pesos”, finalizó.

Al respecto, la intendenta de Santa Tomé, Daniela Qüesta, aseguró que “desde el gobierno provincial siempre y en todo momento, nos garantizaron la intención de que esto sucediese. Por eso es que estuvimos permanentemente poniéndole el hombro al reclamo de los vecinos transmitiéndoles que la obra se va a terminar”.

Por su parte, el concejal Rodrigo Alvizo indicó que “desde el concejo también estuvimos impulsando el reinicio de esta obra con pedidos de informes que tenían que ver con que se explique qué había sucedido con su paralización a fines de 2019. Y hoy estamos celebrando la decisión política del gobernador Omar Perotti y las gestiones de la ministra Silvina Frana de avanzar sobre esta obra en plena pandemia, beneficiando a cientos de familias santotomesinas y mejorando sustancialmente su calidad de vida”, concluyó.