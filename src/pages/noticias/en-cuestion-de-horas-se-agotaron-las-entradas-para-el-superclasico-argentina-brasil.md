---
category: Deportes
date: 2021-11-16T06:00:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/BRARG.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: En cuestión de horas se agotaron las entradas para el superclásico Argentina-Brasil
title: En cuestión de horas se agotaron las entradas para el superclásico Argentina-Brasil
entradilla: En menos de 4 horas los fanáticos se llevaron los 18 mil tickets disponibles
  para presenciar a la Selección, quedando un gran número sin poder conseguir su localidad.

---
Los fanáticos del seleccionado argentino agotaron este lunes la totalidad de las entradas disponibles para el superclásico de mañana ante Brasil en San Juan por la 14ta. fecha de las Eliminatorias Sudamericanas del Mundial Qatar 2022.  
  
Pasadas las 13, las boleterías del autódromo El Villicum, en el departamento sanjuanino de Albardón, cerraron sus ventanillas cuando todavía una multitud pugnaba por conseguir uno de los 18.000 tickets ofrecidos en ese lugar.  
  
Miles de fanáticos, procedentes de distintos lugares de la región de Cuyo, llegaron desde el sábado a las inmediaciones del recinto automovilístico, donde debió adelantarse el expendio de las entradas para evitar desbordes como los registrados el domingo.  
  
La venta presencial debía comenzar este lunes a las 9 en simultáneo con la virtual pero las autoridades de San Juan decidieron adelantarla al domingo a las 23.45 cuando se registraba una cola de 4 kilómetros aproximadamente.  
  
Las localidades restantes se agotaron en menos de dos horas a través del sitio web www.autoentrada.com, que a minutos de habilitarse tenía un tráfico superior a los 65.000 usuarios para acceder al proceso de compra.  
  
La Asociación del Fútbol Argentino (AFA) dispuso los siguientes valores para las entradas: 3.000 pesos (Generales Norte y Sur); 5.000 (Platea Este); 7.500 (Platea Oeste Alta) y 500 (estacionamiento).  
  
Argentina recibirá el martes a Brasil desde las 20.30 en el estadio Bicentenario, con capacidad para 25.000 personas, en un superclásico que puede determinar su clasificación al Mundial Qatar 2022 cuatro fechas antes del término de las Eliminatorias.  
  
El equipo dirigido por Lionel Scaloni, invicto hace 26 partidos, es escolta con 28 unidades, detrás de Brasil (34), que ya aseguró su presencia en la primera Copa del Mundo de Medio Oriente.