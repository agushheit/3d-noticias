---
category: Estado Real
date: 2021-03-22T06:00:09-03:00
thumbnail: https://assets.3dnoticias.com.ar/descacharrado.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Santa Fe: continúa el descacharrado en la ciudad'
title: 'Santa Fe: continúa el descacharrado en la ciudad'
entradilla: Durante febrero y marzo se desplegaron equipos en los barrios Colastiné
  Sur, Chalet, Santa Marta, Favaloro y Las Delicias de la ciudad de Santa Fe.

---
El Ministerio de Salud provincial, a través del Área de Control de Vectores de la Región de Salud Santa Fe, en forma conjunta con agentes del Ministerio de Desarrollo Social, llevaron adelante operativos de descacharrado asistido, sistemático, de modo progresivo, en los barrios Colastiné Sur, Chalet, Santa Marta, Favaloro y Las Delicias.

En este sentido, el director de la Región de Salud, Rodolfo Rosselli, habló del trabajo realizado y detalló: “Con una clara impronta territorial se realizaron visitas casa por casa, para trabajar en el descacharrado y así evitar la proliferación de los mosquitos que transmiten el dengue”.

Además, agregó: “Las tareas en terreno se desarrollaron con la metodología utilizada en los otros barrios trabajados durante los últimos meses de 2020. Es decir, en armar duplas de trabajo, con los equipos de protección personal (EPP) necesarios para el trabajo en el marco de la pandemia Covid-19 y los elementos para disposición final de los residuos, el registro de datos y el material de difusión, tanto para las casas de familia como los negocios”.

Estas actividades consistieron en:

– Los recipientes en uso se vacían y se dejen dados vuelta para no acumular agua.

– Aquellos en desuso son retirados a la vereda para luego, por la tarde ser recogidos por el servicio de recolección municipal.

– Los que contuvieran agua que no pudiera ser eliminada, son tapados herméticamente.

Del mismo modo, el funcionario detalló: “Además de trabajar junto al vecino para mantener el patio ordenado y libre de criaderos de Aedes aegypti, se entregaron folletos informativos reforzando estas acciones y afiches a los negocios emplazados en las manzanas trabajadas”.

Dada la época del año en la que existe una proliferación de larvas sostenida y que muchos vecinos acumulan agua de lluvia para riego, por baja presión en el suministro, se continuó utilizando el larvicida Introban, a base de un agente biológico (BTI, Bacillus Thuringiensis var. israelensis), solo para tratar agua que no se use para consumo humano ni de mascotas.

**Casa por casa**

De estos operativos de descacharrado realizados en Colastiné Sur, Chalet, Santa Marta, Favaloro y Las Delicias, surgen una serie de datos estadísticos a tener en cuenta.

A lo largo de las jornadas se relevaron 141 manzanas. Se llegó a 1.829 inmuebles, de los cuales 1.013 se encontraron cerrados, 424 rechazaron el ingreso al domicilio y 357 fueron trabajadas, es decir que los vecinos permitieron el ingreso a los hogares para llevar a cabo el descacharrado asistido. Además, se intervino los 35 baldíos registrados.

Asimismo, en todos los casos en los que hubo diálogo con los vecinos, se informó acerca de la prevención a través del descacharrado y no la fumigación como método de control de las poblaciones del Aedes aegypti, el uso de repelente si viaja a zona endémica, la consulta inmediata al médico ante síntomas compatibles con la enfermedad, reforzando estos conceptos con la entrega de folletos.

En los domicilios cerrados se dejó el volante explicativo mientras que los afiches fueron entregados en los negocios de la zona recorrida, teniendo una aceptación del 100 %.

En cuanto a la categorización de viviendas según la cantidad de potenciales criaderos de Aedes aegypti (es decir recipientes que contienen o podrían contener agua y larvas de mosquitos) se obtuvieron los siguientes datos totales:

– 323 viviendas de bajo riesgo.

– 21 viviendas de mediano riesgo.

– 13 viviendas de alto riesgo.