---
category: La Ciudad
date: 2021-05-22T06:16:09-03:00
thumbnail: https://assets.3dnoticias.com.ar/jaton.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Jatón detalló cómo será la atención municipal tras las nuevas restricciones
  nacionales
title: Jatón detalló cómo será la atención municipal tras las nuevas restricciones
  nacionales
entradilla: El intendente Emilio Jatón y Mariano Granato, secretario General del municipio,
  comunicaron cómo será la prestación de servicios de la Municipalidad, en relación
  al Covid-19 anunciadas por el gobierno nacional.

---
“Estamos en la peor semana de la pandemia. No hay margen para nada más. Así lo demuestra la ocupación de camas en terapia intensiva que están ocupadas al 100%. Entre el lunes y ayer hubo más de 1400 contagios sólo en la capital provincial”, indicó el intendente Emilio Jatón durante la conferencia de prensa que se realizó esta mañana en el hall de la Municipalidad. Según consignó, por tal motivo, “cada uno tiene que asumir un rol importante, que impacte colectivamente; tenemos que demostrar que somos una ciudad comprometida, solidaria y que pensamos en el otro. Son 9 días que tenemos que cerrarnos, cuidarnos, evitar las reuniones”, afirmó.

Por otro lado, aclaró que “el decreto provincial está vigente hasta esta noche y aún no ha salido el decreto nacional. Así que todo lo que está sucediendo en la ciudad ya está acordado y comunicado. En tanto, las nuevas restricciones (nacionales) comenzarán a partir de mañana. Igualmente, estamos en comunicación directa con el gobierno de Santa Fe, a la espera del decreto de Nación”, insistió.

“Es la semana más difícil en la ciudad de Santa Fe. Pero hay una luz de esperanza: hay 130 mil santafesinos que ya están vacunados con la primera dosis y más de 30 mil con la segunda dosis. Ese es el camino, que entre todos podamos pasar el momento y apelar a que se aumente la dosis de vacunación en la ciudad de Santa Fe. Nos tenemos que mostrar juntos y unidos. Entre todos, Santa Fe puede salir y estoy seguro de que lo vamos a lograr”, valoró Jatón.

**Espacio público y comercio**

Con respecto a los detalles de la actividad económica, el secretario General del municipio, Mariano Granato, explicó que “los vamos a tener cuando salga el decreto nacional. En principio, según los anuncios que dio ayer el presidente de la Nación, está restringida la actividad del comercio minorista. La Municipalidad va a acompañar la medida sanitaria del Gobierno Nacional, así que los controles irán en consecuencia de esta medida que será intensa en un corto plazo”.

En referencia a la concurrencia de las personas a los espacios públicos, Granato indicó que “desaconsejamos la asistencia de la ciudadanía a los espacios públicos durante estos 9 días que dura el decreto nacional. Trataremos de evitar que las personas se mantengan en el espacio público, más allá de alguna caminata. Hay que entender la gravedad de la situación y el corto plazo de la medida”, remarcó.

En cuanto a los controles que se llevarán adelante, Jatón aclaró que “ayer teníamos armado un esquema de seguridad y hoy tuvimos que cambiarlo porque se suspendió el fútbol. Los comités se van a volver a reunir y se va a hacer hincapié en los espacios públicos y en la vía pública”, detalló.

Sobre el funcionamiento de las ferias, se aguardan las precisiones en referencia al decreto nacional.

**Transporte público**

Respecto del funcionamiento de este servicio durante los 3 días hábiles de la semana próxima, el intendente indicó: “Hay una disposición vigente que permite utilizar el transporte público sólo a los trabajadores esenciales porque lo que se intenta es disminuir la circulación”.

No obstante, aclaró que “seguiremos insistiendo con las fajas para lograr la ventilación cruzada. Ya duplicamos la impresión de las mismas. El hecho es que nos acostumbremos a convivir con el virus y las fajas en las ventanas son un elemento con el que tenemos que aprender a convivir. Si las rompieron, vamos a poner nuevas y a insistir con que es la única manera de convivir con el virus”, detalló Jatón.

Granato, por su parte, indicó que “el sistema tendrá algunas restricciones parecidas a lo que fue la fase uno”. En ese sentido, el funcionario remarcó que las restricciones son “porque lo que hay que desalentar es la circulación de las personas. Estamos trabajando con las empresas de transporte para concentrar en el horario pico una frecuencia mayor y menor servicio durante el resto del día”.

Para ejemplificar cómo se reduce la cantidad de personas que viajan en colectivo con las nuevas restricciones, el intendente indicó: “En un día ha disminuido en 14.000 las personas que utilizan el transporte público en la ciudad de Santa Fe, según datos de hoy”. Posteriormente, el secretario General agregó que, a excepción de los horarios pico en los que se intensificará el servicio, durante el resto del día, la frecuencia será como la de los feriados. Esta medida impactará sólo los 3 días hábiles de la semana que viene”, escribió.

**Tributos**

En referencia a qué pasará con los tributos municipales para aquellas actividades que se vean restringidas, el intendente refirió que “hace días que venimos trabajando con los distintos sectores. Hoy nos volveremos a reunir, una vez que se conozca el decreto nacional. También hay un equipo del municipio ocupado en evaluar cómo podemos ayudar, tal como lo hicimos durante la fase 1. Lo vamos a volver a hacer, es un acuerdo que mantenemos con ellos y por supuesto, están incluidos todos los impuestos municipales”, relató.

**Servicios Municipales**

Durante los tres días hábiles de la próxima semana “la Municipalidad va a cubrir los servicios esenciales. Las tareas esenciales que nunca estuvieron cerradas, van a seguir funcionando miércoles, jueves y viernes”, dijo Granato y añadió que “todo lo que se pueda trasladar a teletrabajo se va a hacer, se está evaluando con el comité mixto”.

Por otra parte, el secretario General confirmó que “la asistencia a las personas en situación de calle va a seguir, como los servicios esenciales en toda la ciudad. También la asistencia alimentaria que se hace junto a provincia y el Ejército, y se mantendrá el espacio de aislamiento que se creó con 50 plazas para casos de Covid de sintomatología leve, en pos de descomprimir el sistema de salud”.

En tanto, hasta el 30 de mayo estarán suspendidos los trámites presenciales en las dependencias municipales. Los días miércoles, jueves y viernes tampoco se brindará atención al público en los mostradores del Seom, ubicados en planta baja del Palacio Municipal.

Finalmente, en cuanto a la licencia de conducir, se indicó que los turnos otorgados hasta el 30 de mayo se reprogramarán. Lo mismo sucederá con los turnos para castraciones de animales de compañía. En tanto, en el Corralón de vehículos retenidos se realizarán guardias mínimas de 9 a 13 horas. Y con respecto al funcionamiento de Jardines Maternales y comercios, se aguardan las disposiciones del DNU.

**Cementerio**

De acuerdo a las nuevas disposiciones nacionales y provinciales, el Cementerio Municipal permanecerá cerrado hasta el 30 de mayo.

En ese marco, los sábados, domingos y feriados se atenderá bajo la modalidad habitual de guardia, mientras que durante los días hábiles se atenderán normalmente todos los trámites.

En tanto, en el marco del feriado del 24 de mayo, los turnos de cremación otorgados para ese día serán adelantados al sábado 22.