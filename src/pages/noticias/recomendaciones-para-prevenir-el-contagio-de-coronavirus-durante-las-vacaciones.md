---
category: Agenda Ciudadana
date: 2021-01-09T11:29:49Z
thumbnail: https://assets.3dnoticias.com.ar/090121-vacaciones.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: '3DNoticias - Fuente: Télam'
resumen: Recomendaciones para prevenir el contagio de coronavirus durante las vacaciones
title: Recomendaciones para prevenir el contagio de coronavirus durante las vacaciones
entradilla: El Gobierno nacional difundió una serie de consejos para evitar el contagio
  de coronavirus en vacaciones que se suman al uso de barbijo, la distancia social
  y la sanitización.

---
**Llevar elementos de protección sanitaria de repuesto, circular con permisos y documentos digitales para evitar la manipulación de papeles y mantener objetos personales a más de dos metros de las cosas de los demás**, son algunos de los consejos que el Gobierno agregó al uso del barbijo, la sanitización y la distancia social.

A través de un comunicado, el Gobierno Nacional difundió una serie de recomendaciones para «evitar el contagio del Covid en vacaciones» en el que hizo hincapié en que «**el barbijo, la sanitización y el distanciamiento social siguen siendo las principales medidas de prevención**».

Además de estas acciones que se deben adoptar en la cotidianeidad para evitar el contagio de coronavirus, la campaña de concientización dice que el virus se puede combatir: «en la playa, el río o la montaña; cuando se va a comer afuera o en el alojamiento».

Antes de emprender el viaje, se aconseja tener «los elementos de protección personal necesarios (barbijos, alcohol en gel)» suficientes para todo el camino, además «es recomendable contar con algunos adicionales por si los perdés o se rompen».

«Procurá tener preparada toda la **documentación requerida para tu viaje**. Si es en formato digital o electrónico, mejor, para evitar su manipulación», recomienda el documento. Y en caso de viajar en transporte público pide mantener «distancia social de 2 metros» y evitar el contacto directo con otras personas.

En la playa, el río o la montaña «**colocá tus objetos personales (toallas, reposeras y otros) con distanciamiento de 2 metros**. Si practicás deportes, que sean individuales, manteniendo la distancia: evitá las actividades grupales».

![](https://assets.3dnoticias.com.ar/playa-090121.webp)

También se aconseja **evitar compartir alimentos y bebidas, protector solar y otros elementos de uso personal** en lugares públicos y en locales gastronómicos, en donde, además, **se recomienda utilizar el pago a través de medios electrónicos y pedir el menú digital o en cartelera**, evitando contacto manual.

Otros consejos a tener en cuenta en los locales gastronómicos son: verificar que las mesas se encuentren a por lo menos 2 metros de distancia, que haya alcohol en gel en cada una de ellas y **que los platos y cubiertos no estén dispuestos en la mesa previamente**, además de quitarse el tapabocas únicamente para comer o beber.

En los lugares de alojamiento, se aconseja pagar a través de medios digitales, **utilizar los ascensores de forma individual y evitar la circulación innecesaria en áreas públicas como pasillos y _lobbie_**_s_.

En tanto, en las habitaciones es prudente **mantener la climatización a una temperatura ambiente entre 23 y 26 °C**, asegurando una suficiente renovación del aire.