---
category: Agenda Ciudadana
date: 2021-04-09T07:40:58-03:00
thumbnail: https://assets.3dnoticias.com.ar/afip.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: La AFIP extiende las facilidades para regularizar obligaciones
title: La AFIP extiende las facilidades para regularizar obligaciones
entradilla: El organismo dispuso que hasta el 30 de septiembre próximo no considerará
  la categoría del Sistema de Perfil de Riesgo (Siper) de los contribuyentes.

---
La Administración Federal de Ingresos Públicos (AFIP) dispuso que hasta el 30 de septiembre próximo no considerará la categoría del Sistema de Perfil de Riesgo (Siper) de los contribuyentes al momento de tramitar planes para regularizar obligaciones de los impuestos a las Ganancias y Bienes Personales.

Lo hizo a través de la Resolución General 4959/2021, publicada en Boletín Oficial, que garantiza a los contribuyentes que lo soliciten, la posibilidad de regularizar sus obligaciones de los impuestos a las Ganancias y Bienes Personales en hasta tres cuotas y con un pago a cuenta del 25%, sin verse condicionadas por su perfil de riesgo.

La medida que apunta a coadyuvar al cumplimiento de las obligaciones de los contribuyentes y responsables forma parte de las herramientas puestas a disposición por la AFIP para amortiguar el impacto económico de la pandemia de coronavirus.