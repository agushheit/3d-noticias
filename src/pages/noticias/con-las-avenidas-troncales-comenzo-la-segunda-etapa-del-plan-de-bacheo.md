---
category: La Ciudad
date: 2021-03-10T06:35:16-03:00
thumbnail: https://assets.3dnoticias.com.ar/avenida.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad
resumen: Con las avenidas troncales, comenzó la segunda etapa del Plan de Bacheo
title: Con las avenidas troncales, comenzó la segunda etapa del Plan de Bacheo
entradilla: 'Abarca una decena de arterias de máxima circulación. Paralelamente, avanza
  a buen ritmo el primer frente de trabajo en el micro y macrocentro. '

---
Mientras avanza a buen ritmo la primera etapa del Plan Bacheo en el micro y macrocentro santafesino, ya comenzó la segunda etapa, que contempla las avenidas troncales de la capital provincial. Esta mañana, el intendente Emilio Jatón y la secretaria de Obras y Espacio Público, Griselda Bertoni recorrieron los trabajos que se realizan en Aristóbulo del Valle, a lo largo de Espora, Alberti y Risso.

Cabe recordar, que durante este año, la Municipalidad reparará más de 13.000 m2 de calles deterioradas con hormigón y asfalto. Es un plan por fases: ya arrancaron dos de las cuatro etapas: micro y macrocentro, y avenidas troncales.

Luego de la recorrida, Bertoni brindó detalles de cómo continuarán cada uno de los frentes de trabajo. “La tarea que se están realizando desde esta semana es la reconstitución de los baches en las avenidas de mayor tránsito norte-sur”, indicó Bertoni, y agregó que los trabajos se centran “en los sectores donde hay mayor dificultad para el transporte y para el tránsito más denso”.

![](https://assets.3dnoticias.com.ar/avenida1.jpg)**Avenidas**

La segunda etapa del Plan de Bacheo incluye avenidas troncales (3.000 m2): General Paz, Peñaloza, Presidente Perón, Blas Parera, Aristóbulo del Valle, Facundo Zuviría, Almirante Brown, Freyre, López y Planes, bulevar Pellegrini-Gálvez.

Cabe recordar que durante el 2020 se trabajó en el bacheo asfáltico con mano de obra municipal, en varias de esas avenidas como Blas Parera, General Paz y Aristóbulo del Valle, entre otras. En tanto, con un presupuesto de $ 18 millones, el detalle de los trabajos que arrancó esta semana es el siguiente: Avenida Gral. Paz, entre Iturraspe y Salvador del Carril; Avenida Peñaloza, entre Diagonal Goyena y Av. Gorriti; Avenida Pte. Perón, entre Bv. Pellegrini y Fray Cayetano Rodríguez; Avenida Blas Parera, entre F.C. Rodríguez y Av. Gorriti; Avenida Aristóbulo del Valle, desde Av. Galicia hacia el norte.

![](https://assets.3dnoticias.com.ar/avenida2.jpg)**Desvío de tránsito**

A raíz de las tareas de refacción de la calzada en Aristóbulo del Valle entre Espora y Javier de la Rosa, se realizan desvíos de tránsito y se definió cambiar temporalmente el recorrido de la línea 10.

Los cambios temporales de recorrido son los siguientes: Línea 10 (vuelta): de recorrido habitual por Av. A. del Valle – Castelli – Belgrano – Espora – Rivadavia – Javier de la Rosa a recorrido habitual.

**Se licitó la tercera etapa**

Asimismo, esta mañana se licitaron los trabajos de la tercera fase que corresponde al sector Noreste de la ciudad. El presupuesto oficial es de $ 18.961.599,60 y abarca 2.900 m2. Es la zona comprendida entre las calles French, Padre Genesio, Urquiza y Talcahuano. También se hará bacheo tanto con asfalto y mano de obra municipal, como por licitación en el caso de hormigón.

Las ofertas económicas pertenecieron a las firmas: Cocivial ($ 17.017.568); Coemyc ($ 16.314.628) y Alamco: ($ 14.848.157).

En ese sentido, Bertoni destacó: “esta mañana se conocieron las ofertas económicas para la tercera licitación que abarca el área del Noreste, que también son calles de mucha circulación”. Con respecto a las ofertas económicas señaló: “hubo tres muy buenas ofertas y ahora comienza a trabajar la comisión evaluadora. Calculamos que en unos 45 días vamos a tener a la gente trabajando en esta tercera etapa”.

Cabe recordar que la cuarta y última etapa es la Fase 4, que incluye a los barrios Candioti Norte y Sur (2.900 m2): en la zona comprendida entre las calles Luciano Torrent, Avenida Alem, Belgrano y Grand Bourg. También se hará tanto con asfalto y mano de obra municipal, como por licitación en el caso de bacheo de hormigón.