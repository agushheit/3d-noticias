---
category: La Ciudad
date: 2020-12-04T11:23:16Z
thumbnail: https://assets.3dnoticias.com.ar/VIDEOLLAMADA1.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: La Municipalidad sumó la atención ciudadana para personas sordas
title: La Municipalidad sumó la atención ciudadana para personas sordas
entradilla: 'El servicio se presta de lunes a viernes, de 8 a 16, para la gestión
  de trámites y recepción de consultas, reclamos y denuncias. '

---
Es tanto de forma presencial en el Palacio Municipal como de manera virtual vía web o por videollamada.

La Municipalidad de Santa Fe ofrece un nuevo módulo de atención ciudadana en Lengua de Señas Argentina para personas sordas, que funciona tanto de manera presencial en la planta baja del Palacio Municipal (Salta 2951), como vía virtual a través de videollamada por whatsapp -desde los Centros de Distrito- o a aquellos que ingresen al sitio web oficial del gobierno local: www.santafeciudad.gov.ar, en el apartado “Atención en LSA”.

La atención se presta de lunes a viernes, de 8 a 16, para la gestión de trámites y recepción de consultas, reclamos y denuncias, en la órbita de la Dirección de Derechos y Vinculación Ciudadana que tiene a su cargo el 0800 municipal.

Ivana Nardi y Judith Castro son las dos jóvenes sordas que están al frente del nuevo módulo que el municipio incorporó para ampliar el derecho de acceso a la información. Por ser su primera experiencia laboral, para ambas este trabajo es todo un desafío. Todo comenzó cuando recibieron un correo de CILSA con la propuesta laboral, y no dudaron en asistir a la entrevista, que fue el paso previo a quedar seleccionadas.

En primer lugar, tanto Ivana como Judith pasaron por una etapa de aprendizaje y capacitación para la gestión de consultas y reclamos. Luego comenzaron la atención en el servicio. Ambas agradecen “la paciencia para explicar” de su compañero Matías en este camino que están recorriendo en el municipio.

Ivana tiene 24 años, reside junto a su familia en Guadalupe Oeste y viene al trabajo en colectivo. Las expectativas puestas en este trabajo son muy altas: “Tengo fe y estoy con muchas ganas de mejorar como persona, para que las personas sordas puedan tener buena comunicación, puedan tener información como les corresponde, porque la mayoría de las personas sordas siempre tuvieron dificultades en relación a la información por falta de accesibilidad o por falta de intérpretes de lengua de señas”.

Ivana cursa el tercer año del Profesorado de Educación Especial con Orientación en Sordos e Hipoacúsicos en el Instituto Superior de Profesorado N° 8 “Almirante Guillermo Brown” y está por terminar el Instructorado Universitario en Lengua de señas argentina en la Facultad de Trabajo Social de la Universidad Nacional de Entre Ríos.

Concentrada en su formación académica y en su trabajo, busca cumplir sus deseos: “Estuve toda mi vida luchando por la comunidad sorda con el tema de la accesibilidad. Mi sueño es ser profesora de sordos e hipoacúsicos, instructora de lengua de señas y seguir trabajando acá e ir mejorando y, si es posible, llegar a otro nivel para conocer a otra gente, no solamente a personas sordas sino trabajar con gente con discapacidad intelectual, motriz, para conocer otra perspectiva y otra cultura. Me gusta mucho trabajar en equipo, me gusta mucho ir a charlas porque me enseñan a tener otra mirada, a que surjan ideas para que las personas puedan sentirse implicadas e incluidas”.

Judith, por su parte, se mostró contenta y agradecida por trabajar en el municipio. “Sentí mucha emoción cuando me llamaron”, cuenta. Después de asistir a varias entrevistas laborales, a sus 25 años es la primera vez que tiene la oportunidad de acceder a un trabajo. Vive en barrio Pompeya junto a su hija Xiomara de cuatro años, su madre y tres de sus hermanos.

Al igual que Ivana, ya está por finalizar el Instructorado de Lengua de Señas Argentina en la facultad paranaense. A través de su trabajo en el municipio quiere cooperar con otros: “Quiero ayudar a la gente para recibir su consulta, su denuncia o reclamo porque hay mucha gente que vive sola o no la ayudan sus familias”.

\-![](https://assets.3dnoticias.com.ar/VIDEOLLAMADA2.jpg)

## **Promover el acceso a la información**

“En la decisión de ir eliminando progresivamente las barreras con las que nos encontramos para facilitar la calidad de atención a los ciudadanos y ciudadanas santafesinas, en particular personas sordas, hablantes de Lengua de Señas Argentina, se decidió llevar adelante esta iniciativa que tiene que ver con garantizar la comunicación a aquellos ciudadanos y ciudadanas que necesiten hacer algún tipo denuncia, hacer algún tipo de reclamo, contactarse por el trámite que sea con el municipio”.

De este modo sintetiza Gabriela Bruno las características del nuevo módulo puesto en marcha en conjunto desde la Dirección de Derechos y Vinculación Ciudadana, con la Secretaría de Políticas de Cuidado y Acción Social, en particular desde la Subdirección de Accesibilidad y Derechos de las personas con discapacidad, área que tiene a su cargo.

Marcelo Escalas es coordinador del Programa de Autonomía y Participación Social del municipio y junto a Gabriela Bruno es uno de los impulsores de esta iniciativa con la que se busca “dar respuesta a un reclamo histórico de las personas sordas que se veían impedidas por barreras comunicacionales el acceso a la información y a los reclamos”.

Además, destacó que la propuesta surge de un trabajo articulado con instituciones de la sociedad civil: “En este caso, en diálogo con el colectivo de las personas sordas, con la comunidad se ha logrado también dar esta respuesta o, al menos, comenzar este proceso en el que realmente tenemos mucha esperanza de que vaya creciendo”. También, planteó que “al buscar antecedentes de esta política, no los encontrábamos en todo el país. Entonces, fuimos creando este espacio en conjunto con la Dirección de Derechos y Vinculación Ciudadana”.

## **Vínculo**

Por su parte, Macarena Gelmi, subdirectora de Atención Ciudadana, Comunicación y Relaciones Comunitarias destacó que “con la incorporación de las chicas a nuestro espacio de trabajo se hace visible uno de los objetivos que teníamos desde el principio de la gestión, que es hacer de ‘Santa Fe te escucha’ un espacio de vinculación para todas y todos los santafesinos”.

En este sentido, la funcionaria definió que “su trabajo va a poder generar un espacio nuevo de vinculación en el municipio, en donde se podrá atender con Lengua de Señas a personas que hasta ahora no tenían la posibilidad de recibir orientación, ni dejar asentados reclamos, ni vincularse directamente con la Municipalidad. A partir de ahora esto se hace posible, estamos más que agradecidos por la buena predisposición de las chicas y del equipo para este nuevo módulo de atención en lengua de señas en ‘Santa Fe te escucha’”, concluyó.

Las dos chicas ingresaron al 0800 municipal a través del Programa Nacional de Estímulo al Primer Empleo de Personas con Discapacidad.