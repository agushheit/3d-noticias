---
category: Agenda Ciudadana
date: 2021-07-23T06:00:34-03:00
thumbnail: https://assets.3dnoticias.com.ar/PRODUCTIVOS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Sectores productivos presionan por la ampliación de horarios
title: Sectores productivos presionan por la ampliación de horarios
entradilla: Ante el vencimiento de los decretos provinciales y nacional de restricciones
  este viernes 23, varios sectores ya adelantaron pedidos a autoridades municipales.

---
Este viernes 23 de julio vencen el decreto provincial y también nacional que refiere a restricciones por la pandemia de coronavirus. Es por ello que algunos sectores productivos de la ciudad de Santa Fe ya hicieron pedidos formales ante las autoridades para que luego sean elevadas a la provincia.

El secretario de Producción del municipio, Matías Schmüth afirmó: "Venimos acompañando a los distintos sectores con mesas productivas desde el comienzo de la pandemia quienes nos manifestaron distintas inquietudes para elevarlas a la provincia, para que en este caso puedan ser plasmadas en la nueva normativa, tras el vencimiento del próximo viernes".

Entre los pedidos concretos que ya le realizaron destacó: "Los supermercados plantearon tener una hora más de atención, es decir hasta las 20 o que el mismo se extienda una más de la que tengan los comercios, para que no se superpongan y les den la posibilidad a los trabajadores del rubro de contar con los locales abiertos al finalizar su jornada. Por otro lado, los gastronómicos piden la posibilidad de poder tener abiertas sus puertas de lunes a lunes hasta las 24".

Por el momento son solo pedidos que deberán de ser analizados con los distintos índices que componen la situación sanitaria para la toma de decisiones, pero resaltó: "Las actividades productivas se pueden realizar de manera cuidada y un ejemplo fue lo que sucedió para el Día del Amigo, fue una muestra de ello. Hubo un movimiento de gente de acuerdo a la situación que atravesamos, pero en general el sector gastronómico trabajó muy bien y cumpliendo con los protocolos, algo que ayuda a que se puedan pedir más flexibilizaciones. Por otro lado, y en base a la normativa dada a conocer el miércoles sobre el cambio del semáforo epidemiológico, se habla de la posibilidad de ampliar los aforos al 50% en distintas actividades, por lo que es una nueva posibilidad que se abre para ver si se establece en la ciudad".