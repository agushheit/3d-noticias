---
category: Agenda Ciudadana
date: 2021-04-06T06:30:03-03:00
thumbnail: https://assets.3dnoticias.com.ar/tecnología.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: Santa Fe avanza en la elaboración del Plan Estratégico Provincial en Ciencia,
  Tecnología e Innovación
title: Santa Fe avanza en la elaboración del Plan Estratégico Provincial en Ciencia,
  Tecnología e Innovación
entradilla: La secretaria Baima mantuvo un encuentro con autoridades nacionales, y
  les detalló la metodología de trabajo que comenzará con más de 100 referentes en
  la provincia.

---
Antes de la primera reunión de trabajo para comenzar el proceso de construcción del Plan Estratégico Provincial de Ciencia, Tecnología e Innovación, la secretaria de Ciencia, Tecnología e Innovación, Marina Baima, y el equipo técnico mantuvieron un encuentro con el secretario de Planeamiento y Políticas de CTI del Ministerio Nacional de Ciencia, Tecnología e Innovación, Diego Hurtado, para validar la metodología a llevar adelante. El funcionario nacional dio el visto bueno al cronograma y las formas de trabajo presentadas.

De manera virtual, parte del equipo que lleva adelante la construcción del Plan Estratégico explicó a Hurtado las distintas etapas de todo el proceso y la forma de trabajo para obtener dos resultados: el Plan Estratégico y un Banco de Proyectos.

Hurtado, que es uno de los responsables del armado del Plan Nacional de Ciencia, Tecnología e Innovación 2030, celebró la iniciativa, marcó positivamente la estrategia y el orden para obtener los resultados. Además, hizo foco en la importancia de articular el diálogo entre los sectores públicos y privados, y aseguró que la propuesta de Santa Fe le parecía la correcta para esta coordinación.

**La metodología para la construcción del Plan Estratégico**

Para realizar la construcción colectiva del Plan Estratégico y el Banco de Proyectos, comenzará en abril a trabajarse en células agrupadas en 7 Ejes estratégicos transversales definidos a partir del diálogo con el ecosistema. Estos ejes se relacionarán con 5 Vectores, que son las grandes temáticas sociales y productivas que deben potenciarse y/o desarrollarse en Santa Fe.

Los 7 ejes estratégicos transversales son: Ecosistema y gobernanza, Financiamiento; infraestructura y equipamiento; Talento; Marco normativo; Vinculación tecnológica e Información y áreas de vacancia. Y los 5 Vectores: Agroalimentos y agrotecnología; Ambiente y Cambio Climático; Salud, Conectividad y Digitalización; e Industrias Creativas.

Para la conformación de cada célula, se realizó una inscripción de interesados, que ya superó los 100 referentes en la provincia en todas las áreas; integrantes del ecosistema de Ciencia, Tecnología e Innovación de la provincia, provenientes tanto del sector privado como del sector público, de instituciones educativas, formativas y entidades productivas.