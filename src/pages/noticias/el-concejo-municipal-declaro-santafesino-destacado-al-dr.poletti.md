---
category: La Ciudad
date: 2021-07-14T08:23:18-03:00
thumbnail: https://assets.3dnoticias.com.ar/poleti.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Concejo Municipal de Santa Fe
resumen: El Concejo Municipal declaró Santafesino Destacado al Dr. Poletti
title: El Concejo Municipal declaró Santafesino Destacado al Dr. Poletti
entradilla: 'El actual director del Hospital José M. Cullen fue reconocido por su
  trayectoria, destacada labor y aporte al sistema de salud de la ciudad de Santa
  Fe. '

---
Esta mañana, en el recinto de sesiones, el Cuerpo legislativo entregó la declaración de “Santafesino Destacado” al Dr. Juan Pablo Poletti, director del Hospital José María Cullen. Concejales y concejalas destacaron el contexto que se está viviendo mundialmente, el desafío que implica la adaptación a un escenario extraordinario en el ámbito del Sistema de Salud, y el trabajo realizado con eficiencia y pericia por parte del Dr. Poletti durante la emergencia sanitaria por el Covid-19. Fue por iniciativa de las concejalas Luciana Ceresola y Valeria López Delzar.

Al respecto, el presidente del Concejo, Leandro González, sostuvo que “Juan Pablo Poletti está entre quienes se encuentran en la primera línea de batalla asumiendo sin titubeos esta gran responsabilidad. Con el correr de los días se ha destacado como un ciudadano con vocación, en el lugar correcto y en el momento indicado, marcando la diferencia y asumiendo un rol fundamental para nuestra ciudad y nuestra región. Por esto y por sus años de trayectoria en uno de los efectores más importantes y complejos a la hora de gestionar, es que nos parece necesario este reconocimiento que acercaron las concejalas López Delzar y Ceresola. En su nombre aparecen también reconocidos los cientos de trabajadores y trabajadores del Hospital”.

Por su parte, la concejala Luciana Ceresola afirmó que “el Dr. Poletti demostró que estaba a la altura de la circunstancias cuando la pandemia vino a modificar toda nuestra vida. Como él viene del deporte, podemos decir que ‘dejó todo en la cancha’. Creemos que era necesario que el Concejo otorgue su reconocimiento a la institución médica que es para Santa Fe”.

En tanto, la concejala Valeria López Delzar celebró que “por unanimidad se haya aprobado esta distinción que viene a reconocer el rol de dirigir un hospital que tiene 110 años y 1800 empleados. Poletti estuvo a la altura de las circunstancias de este desafío de enfrentar la pandemia cuidando la salud no solo de su equipo sino de toda la sociedad santafesina”.

Poletti, por su parte, agradeció a todos los empleados del hospital y se mostró con “una alegría inmensa y mucha responsabilidad”. “Es imposible ponerle nombre a cada uno de los que ha trabajado. Mi agradecimiento a quienes han pensado mi nombre para este reconocimiento, pero destaco que solo soy uno entre tantas de las personas que han dejado realmente mucho en esta pandemia. Estos son mimos que dan fuerzas para continuar, Volviendo a aclarar que lo recibe uno pero que es imposible pensarlo sin un equipo de trabajo. A ellos mi agradecimiento eterno por lo que hicieron y están haciendo”.