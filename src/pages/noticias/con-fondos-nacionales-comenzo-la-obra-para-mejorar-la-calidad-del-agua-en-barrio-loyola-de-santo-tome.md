---
category: Estado Real
date: 2021-07-04T06:15:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/LOYOLA.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Con fondos nacionales, comenzó la obra para mejorar la calidad del agua en
  barrio Loyola de Santo Tomé
title: Con fondos nacionales, comenzó la obra para mejorar la calidad del agua en
  barrio Loyola de Santo Tomé
entradilla: La obra beneficiará a 15 mil vecinos y permitirá abastecer de agua potable
  y con mayor presión a las familias de los barrios Loyola y General Paz.

---
El gobierno de la provincia inició esta semana con los trabajos de vinculación del sistema de acueductos Desvío Arijón con los barrios General Paz y Loyola de la ciudad de Santo Tomé, a los fines de abastecerlos de agua potable y de calidad.

El secretario de Empresas y Servicios Públicos, Carlos Maina supervisó el comienzo de las tareas donde se están realizando zanjeos, provisión y colocación de caños de polietileno de alta densidad de 160 milímetros de diámetro clase 6 y termofusionados cada 12 metros, en calle Libertad entre López y Planes y Arenales.

Al respecto, Maina indicó: “Apenas apareció el problema del agua en barrio Loyola, nos reunimos con la ministra Silvina Frana, autoridades locales y el equipo de Aguas Santafesinas, entendiendo que debíamos buscar una solución definitiva a este problema. Y si bien el servicio de aguas en Santo Tomé es un servicio que presta la municipalidad, nosotros entendíamos que ante una obra de semejante magnitud, la provincia tenía que ponerse al frente, y así lo entendió el gobernador Omar Perotti, con la firme decisión de que esto se resuelva”. Y agregó: “Cabe destacar el apoyo del gobierno nacional en esta obra, porque la incluimos en el presupuesto, la licitamos y hoy la estamos llevando adelante”, explicó Maina.

En relación a esto, el funcionario detalló que la inversión surge a partir de un convenio entre el gobierno provincial y el ENOHSA - Ente Nacional de Obras Hídricas de Saneamiento-, que contempla optimizar el sistema de bombeo que actualmente abastece al tanque elevado General Paz y vincular este sistema con el de barrio Loyola que, hasta el momento, no cuenta con agua de calidad, ni presión suficiente, proveniente del sistema de acueductos que se encuentra en la ciudad de Santo Tomé. Además, se mejorará la provisión de agua en las vecinales General Paz y Zazpe, beneficiando a más de 15 mil vecinos de la zona.

**LOYOLA**

Desde su origen, el barrio contó con un sistema propio e independiente de captación y distribución de agua corriente, las que presentan problemas de contaminación con amonio, nitritos y nitratos, según verificaciones realizadas en enero del 2020 por el ENRESS. Ante tal situación, la empresa ASSA ha colaborado en la provisión de cisternas móviles con agua de buena calidad, se ha realizado un by pass provisorio con cañería de red de agua potable proveniente del Sistema del Tanque Central (abastecido por cisterna norte del Acueducto Desvío Arijón).

Las demandas actuales requieren la optimización del sistema General Paz para poder vincular este sistema con el de Loyola, además de la ejecución de nuevos empalmes de cañería. Paralelamente se planifica el readecuamiento del campo de bombeo existente por electrobombas centrífugas de eje vertical en reemplazo de las existentes de pozo profundo.

**GENERAL PAZ**

La red de agua potable que actualmente es abastecida por el tanque elevado ubicado en el barrio General Paz, abastece a dicho barrio y al barrio Sargento Cabral. El taque elevado conocido como General Paz, se abastece de la cisterna sur mediante sistema de bombeo. La demanda media anual de agua potable de este barrio se estima en 75,0 m3/h, con una demanda máxima horaria de 175,5 m3/h. En las épocas de mayor consumo, el caudal aportado por la cisterna sur no ha sido suficiente, y ello ha generado disconformidades de la población al recibir en sus domicilios aguas turbias con residuos.