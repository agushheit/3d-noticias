---
category: Agenda Ciudadana
date: 2021-06-22T08:30:57-03:00
thumbnail: https://assets.3dnoticias.com.ar/elecciones.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Diario Uno
resumen: Santa Fe usará urnas compradas por la reforma constitucional
title: Santa Fe usará urnas compradas por la reforma constitucional
entradilla: En las primarias de Santa Fe habrá más de 7.000 urnas que compró el gobierno
  de Lifschitz para hacer el plebiscito junto a las elecciones de 2019 y no se usaron

---
Las próximas elecciones no serán una más en la historia de la provincia de Santa Fe y de la Argentina porque se realizarán en medio de una pandemia mundial que modificó la vida en todos los países del mundo. Pero, además, como dato de color, los santafesinos votarán en urnas que fueron compradas por el entonces gobierno de Miguel Lifschitz para realizar un plebiscito donde se le consultaría a los santafesinos acerca de la necesidad de una reforma constitucional.

En 2018 la Legislatura le dijo que no al intento de Lifschitz de reformar la Constitución y el socialista intentó al año siguiente realizar un plebiscito no vinculante para que sea el pueblo de Santa Fe el que se exprese sobre la necesidad o no de reformar la carta magna provincial. Sin embargo, como la consulta se iba a realizar de manera conjunta con los comicios generales donde se definían los cargos de gobernador y vice, senadores y diputados provinciales, intendentes y presidentes comunales y concejales, la iniciativa comenzó a recibir críticas desde diferentes espacios políticos.

Eso hizo que Lifschitz bajara el proyecto. “Preocupados por cualquier tipo de sospechas que pueda enturbiar el proceso electoral, y como la elección –que además será disputada– es nuestra prioridad, decidimos suspender la consulta popular por la reforma”, dijo en una conferencia de prensa en Rosario el 10 de mayo de 2019, a un mes y una semana para las elecciones.

Pero las urnas ya estaban compradas, al igual que las que se iban a utilizar ese año para definir todas las categorías electorales en juego. Por eso la Secretaría Electoral debió guardarlas y ahora podrá utilizarlas. De acuerdo a la cantidad de mesas estimadas para las elecciones de este año –al ser una elección simultánea y unificada con los comicios nacionales es Nación quien define varios aspectos como la definición de cuántas mesas por local de votación se van a permitir– Santa Fe iba a necesitar, entre las Paso y las generales, algo más de 16.000 urnas. Sin embargo, solo compró 9.500 cajas de cartón porque el resto son las que se compraron en 2019 para el plebiscito que nunca se realizó.

**La logística electoral en pandemia**

El proceso electoral en Santa Fe comenzó a principios de año con el dictado del Decreto Nº 281 del gobernador Omar Perotti con el que convocó a elecciones primarias para agosto y las generales para octubre. Luego con los cambios a nivel nacional y la aprobación de las fechas por parte del Congreso se fijaron nuevas fechas para los comicios que van a ser el 12 de septiembre y el 14 de noviembre.

Este año los santafesinos tendrán que elegir nueve diputados nacionales y los tres senadores nacionales con las conocidas boletas de papel. Para eso habrá una urna con boca única. Mientras que, de forma paralela y con boleta única, elegirán intendente en 14 ciudades (Armstrong, Las Toscas, Florencia, San José del Rincón, Recreo, San Guillermo, Suardi, Romang, Fray Luis Beltrán, Puerto General San Martín, Roldán, el Trébol, San Vicente y Sauce Viejo. Las últimas dos elegirán por primera vez intendente); 210 concejales titulares y 184 suplentes; 1.214 miembros comunales y 1.214 suplentes y 918 contralores de cuenta titulares y la misma cantidad de suplentes.

En diálogo con UNO Santa Fe, el secretario Electoral de la provincia, Pablo Ayala, aseguró que ya se está trabajando en el proceso de logística previa que tiene que ver con la confección del padrón, armado de toda la logística electoral que incluye el armado de las urnas, los kits de materiales sanitarios para el día de la elección, entre otras cuestiones.

![Los cargos locales que se eligen en la provincia de Santa Fe en estas elecciones](https://media.unosantafe.com.ar/p/47995c5b522e5d1e77d59b45956fe3a8/adjuntos/204/imagenes/030/167/0030167491/cuadro-elecciones-2021-cjpeg.jpeg?0000-00-00-00-00-00)

Pero ese trabajo que se realiza todos los años que hay elecciones esta vez será diferente porque está atravesado por el Covid. "Lo primero que nos modifica la pandemia son los protocolos para el día de la elección. No vamos a tener una elección común como tuvimos las veces anteriores. Ahora vamos a tener que tener una elección que tendrá que estar bien organizada para poder tener listos todos los protocolos, para mantener el distanciamiento, con mucha concientización social para el día que se lleve adelante el acto electoral", definió Ayala.

De acuerdo al cronograma electoral, la designación de las autoridades de mesa se hará el 13 de agosto y será de manera conjunta con Nación. La provincia de Santa Fe lo que aporta es el segundo auxiliar de cada mesa y al subdelegado del establecimiento. Mientras que Nación se hace cargo del delegado del establecimiento (en el caso de que el local sea una escuela, por lo general, es el director o la directora), del presidente de mesa y del primer auxiliar.

![La cantidad de locales de votación aún es estimativa ya que por la pandemia Nación reducirá para estas elecciones la cantidad de mesas por establecimiento.](https://media.unosantafe.com.ar/p/e2423346600269d8bfb0f74d71246e9b/adjuntos/204/imagenes/030/167/0030167282/cuadro-elecciones-2021-ajpeg.jpeg?0000-00-00-00-00-00)

La cantidad de locales de votación aún es estimativa ya que por la pandemia Nación reducirá para estas elecciones la cantidad de mesas por establecimiento.

La cantidad de locales de votación aún es estimativa ya que por la pandemia Nación reducirá para estas elecciones la cantidad de mesas por establecimiento.

"Una vez que tengamos determinado cuáles son los establecimientos de votación y cuántas mesas tiene cada establecimiento vamos a trabajar con el Ministerio de Educación para que la autoridad que designa la provincia de Santa Fe, el segundo auxiliar, sea una persona mayor de 18 años, menor de 55 años y que esté vacunada, preferentemente docente", indicó Ayala y marcó la importancia de que las personas designadas ya tengan experiencia como autoridades de mesa ante las dificultades que la pandemia plantea para poder hacer las capacitaciones.

"Al establecer una elección unificada con Nación nosotros coordinamos con la Secretaría Electoral nacional con competencia en Santa Fe y seguramente vamos a trabajar en un esquema de capacitación virtual a través de zoom y plataformas educativas basadas en internet. Mientras que, avanzado el plan de vacunación y si la cuestión sanitaria lo permite, más cerca de las elecciones generales, seguramente alguna instancia presencial se pueda llegar a hacer. Pero por ahora va a ser todo virtual", aclaró.

![Para las elecciones generales Santa Fe sumará al padrón a 68.183 personas que cumplirán los 18 años entre el 13 de septiembre y el 14 de noviembre.](https://media.unosantafe.com.ar/p/53eff7b3fa9a8e81145f3806041c1f48/adjuntos/204/imagenes/030/167/0030167266/cuadro-elecciones-2021-bjpeg.jpeg?0000-00-00-00-00-00)

Hasta el momento no hay definiciones concretas de parte de Nación, pero se sabe que va a haber un tope de mesas por establecimientos para evitar que se concentre mucha gente en un mismo lugar. Se estima que serán alrededor de ocho mesas por local de votación. Eso haría que en la provincia de Santa Fe se tengan que agregar entre 90 y 100 locales nuevos de votación respecto a la elección de 2019. Con eso, además, se contiene a las personas que se incorporaron al padrón electoral que de los 2.675.157 electores de 2019 pasó a los 2.701.619 para esta elección.

Ese límite de mesas por local de votación hará que muchos santafesinos cambien el lugar donde votaron hace dos años. Por eso, desde la Secretaría Electoral de la provincia ya están advirtiendo que este año se va a tener que estar muy atentos y chequear dónde votará cada persona. Lo que no se modificará es la cantidad de electores por mesa de votación, que seguirá siendo de 350 personas.