---
category: Agenda Ciudadana
date: 2021-04-03T14:00:11-03:00
thumbnail: https://assets.3dnoticias.com.ar/ALBERTO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: 'Alberto Fernández: "Me siento bien, no tengo ningún síntoma preocupante"'
title: 'Alberto Fernández: "Me siento bien, no tengo ningún síntoma preocupante"'
entradilla: El Presidente contó que fue revisado por el médico y que se sometió a
  un test de PCR para confirmar el test rápido de antígeno que le había dado positivo
  de coronavirus.

---
Alberto Fernández contó en declaraciones radiales que "se siente bien", que este sábado le hicieron un test de PCR para confirmar el test rápido de antígeno que el viernes por la noche le había dado positivo de coronavirus, y aseguró que en el transcurso de la tarde estarán los resultados.

  
"_La verdad es que me siento bien, lo mío fue casi preventivo, porque este viernes fue mi cumpleaños, estuve tocando la guitarra y almorzando con mi hijo y Fabiola, y pasé el día lo más bien, y a la noche, cuando volví al cuarto, tuve un dolor de cabeza medio raro, no era intenso pero sí molesto, y sentí que estaba como acalorado, me tomé la fiebre y tenía tres líneas de fiebre: 37.3"_.

  
"Me hicieron un test rápido, y ese test dio positivo, y mandamos hacer un test de PCR, que es el más lento pero el más seguro, pero hay que esperar esos resultados para estar seguros de que lo que dio el test rápido es seguro", agregó en declaraciones a la radio AM 750.  
Además, el jefe de Estado contó que en el hisopado realizado para el estudio de PCR le tomaron muestras de la boca y las fosas nasales que fueron llevadas al Instituto Malbrán.  
"Pero la verdad es que me siento bien, recién se fue el médico, me revisó íntegramente, me revisó los pulmones, la saturación de oxígeno, no tengo ningún síntoma preocupante", subrayó el Presidente