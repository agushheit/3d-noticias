---
category: Estado Real
date: 2021-05-25T08:42:22-03:00
thumbnail: https://assets.3dnoticias.com.ar/pymes.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: Ante las nuevas medidas la provincia dispuso ayuda económica y exención de
  impuestos
title: Ante las nuevas medidas la provincia dispuso ayuda económica y exención de
  impuestos
entradilla: Desde el gobierno provincial se detallaron las medidas de asistencia a
  diversos sectores afectados como consecuencia de la emergencia sanitaria

---
Desde el gobierno provincial se detallaron las medidas de asistencia a diversos sectores afectados como consecuencia de la emergencia sanitaria. Fue el ministro de Economía santafesino, Walter Agosto, quien bridó detalles al diario La Capital de Rosario.

El titular de la cartera de Hacienda provincial destacó que las medidas apuntan a dos frentes de mitigación para los sectores afectados por las restricciones. Por un lado, la reimplantación de las asistencias económicas que la provincia venía otorgando a 30 sectores que fueron afectados por las medidas en 2020 y los primeros meses de 2021, y por el otro la incorporación de otros rubros que también serán asistidos por vía de transferencias directas, como son los casos de los trabajadores de la gastronomía, hotelería y el transporte turístico que se incluyen en esta instancia.

El ministro señaló que se ha establecido una asignación de recursos en el orden de unos 1.400 millones de pesos para estas asistencias que operarán para el trimestre de junio, julio y agosto, oportunidad en la que se evaluará su continuidad en función de la situación de pandemia.

**Alivio tributario**

Por otra parte, Agosto expresó que la otra línea de acción está vinculada a un conjunto de medidas tributarias entre las que se destacan las exenciones impositivas, la prórroga del plan de regularización de deudas tributarias vigente por 60 días y el establecimiento de un nuevo plan especial de pago con costo cero para deudas impositivas generadas este año.

De este modo, y según señaló el funcionario, bares, restaurantes, hoteles y alojamientos, salones de eventos y servicios relacionados, fútbol 5 y demás establecimientos deportivos, jardines maternales y centros de atención de desarrollo infantil, servicios relacionados al turismo, salas de teatro, de eventos culturales y complejos cinematográficos, discotecas y gimnasios, estarán exentos de pagar el impuesto a los ingresos brutos correspondientes a los meses de abril a agosto.

Esto implica que tampoco serán objeto de retención y/o percepción de ningún tipo y serán excluidos del Sircreb (Sistema de Recaudación y Control de Acreditaciones Bancarias, que tiene como fin posibilitar el cumplimiento de los regímenes de recaudación de Ingresos Brutos de contribuyentes comprendidos en las normas del convenio multilateral).

En tal sentido, y en esta misma línea, el funcionario indicó que no deberán abonar el pago del impuesto mínimo y se les devolverán las retenciones y/o percepciones y/o retenciones por Sircreb que hubieran sufrido entre abril y mayo del corriente año, para lo cual se deberá realizar un trámite web en API.

**Eximidos**

Asimismo, se eximirá el pago de las cuotas 2 y 3 del impuesto inmobiliario a todos estos sectores, debiendo acreditar en caso de no ser propietarios que tienen a su cargo el pago de ese impuesto.

Con relación a los pequeños y medianos comercios minoristas, locales no incluidos en el Convenio Multilateral, con excepción de aquellos que han desarrollado sus actividades de manera normal, se eximirán los saldos de las declaraciones juradas de Ingresos Brutos correspondientes al período junio/agosto de este año.

Otra medida que adoptó el gobierno es la exención del pago de las cuotas de abril a agosto del Régimen Simplificado de Ingresos Brutos. Una resolución que alcanzará a la totalidad de los pequeños contribuyentes, independientemente del rubro y la actividad a la que pertenezcan.

Así, y en números globales, las medidas adoptadas en materia de eximición del pago de impuestos implicarán una menor recaudación para la provincia, estimada en unos 1.600 millones de pesos, lo que sumado a los 1.400 millones de asistencia dineraria directa totalizan unos 3.000 millones de pesos.

Finalmente, el gobierno provincial prorrogará por 60 días el régimen de regularización de deudas tributarias vigente. Pero a su vez, establecerá un nuevo plan de facilidades de pago para las deudas generadas durante el corriente año, las que se podrán abonar prácticamente a valores históricos, en tanto que se suspenden las ejecuciones fiscales y los embargos para estos sectores.