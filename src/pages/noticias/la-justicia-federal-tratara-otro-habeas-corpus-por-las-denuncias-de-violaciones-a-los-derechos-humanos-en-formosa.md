---
category: Agenda Ciudadana
date: 2021-01-30T17:04:32.000+00:00
thumbnail: https://assets.3dnoticias.com.ar/neme.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Infobae
resumen: La Justicia Federal tratará otro habeas corpus por las denuncias de violaciones
  a los derechos humanos en Formosa
title: La Justicia Federal tratará otro habeas corpus por las denuncias de violaciones
  a los derechos humanos en Formosa
entradilla: 'La Sala de Feria de la Cámara Federal de Casación Penal le dio curso
  a la presentación realizada por el concejal formoseño Miguel Alfredo Montoya. '

---
La **Sala de Feria de la Cámara Federal de Casación Penal** declaró este viernes que la Justicia Federal es competente para resolver sobre el **habeas corpus colectivo y correctivo presentado por el concejal Miguel Alfredo Montoya** a raíz de las denuncias por violaciones a los derechos humanos en los centros de aislamiento para pacientes con coronavirus en Formosa.

El tribunal, integrado por los jueces **Mariano Borinsky**, **Juan Carlos Gemignani** y **Daniel Antonio Petrone**, resolvió por mayoría rechazar el planteo del fiscal general de Casación, Raúl Omar Pleé, quien consideró que la Justicia Federal era incompetente y solicitó que el habeas corpus continúe su trámite ante la Justicia provincial.

La denuncia del concejal Montoya [**es similar a la interpuesta recientemente por el senador nacional Luis Naidenoff**](https://www.infobae.com/politica/2021/01/27/la-justicia-federal-tratara-el-habeas-corpus-presentado-por-el-senador-luis-naidenoff-por-las-violaciones-a-los-derechos-humanos-en-formosa/), que también avanzó y quedó a cargo de la Justicia Federal.

Montoya planteó que en los centros gubernamentales de aislamiento que funcionan en la provincia de Formosa **se están produciendo violaciones a derechos humanos de índole constitucional y convencional** tales como el derecho a la vida, a un ambiente sano, a la salud física y mental, a la privacidad, intimidad, a la circulación y el derecho a recibir un trato digno, entre otros.

![](https://assets.3dnoticias.com.ar/concejal-montoya.jpg)

###### Concejal Miguel Alfredo Montoya

En su presentación, el concejal afirmó que las medidas de aislamiento dispuestas por el gobierno de la provincia de Formosa provocan la convivencia en un mismo espacio físico entre personas contagiadas del virus COVID-19 (con o sin síntomas), personas con casos sospechosos sin diagnóstico definitivo, personas que tuvieron contacto estrecho con algún contagiado y personas que ingresaron a la provincia de Formosa y deben cumplir aislamiento -estos últimos, incluso con test PCR negativo.

Además, **denunció que en tales centros de aislamiento no se respetan mínimamente las condiciones de privacidad, intimidad e higiene, ni las recomendaciones del Ministerio de Salud de la Nación.** Y que las personas aisladas -incluso menores de edad- comparten el mismo baño y son retenidas en sitios con divisiones precarias y en condiciones de hacinamiento, sin respetar distanciamiento y sin ventilación ni refrigeración suficiente.

![](https://assets.3dnoticias.com.ar/senador-naidenoff.jpg)

###### Senador Luis Naidenoff

Finalmente, el concejal Montoya afirmó que, en caso de ofrecer cualquier tipo de resistencia a ser trasladados o aislados, a las personas se las amenaza con imputarles la comisión del delito previsto en el art. 205 del Código Penal y con ser privados de su libertad en forma cautelar.

Los doctores Borinsky -presidente del tribunal- y Gemignani sostuvieron que el caso era análogo al resuelto en fecha 27 de enero de 2021, oportunidad en la que hicieron lugar al recurso de casación del legislador Petcoff Naidenoff. En consecuencia, se remitieron a los fundamentos allí expuestos.

Por su lado, el Dr. Petrone también consideró que el caso guardaba analogía con el iniciado por el mencionado Petcoff Naidenoff y recordó haber propiciado en dicha oportunidad el rechazo del recurso, de conformidad con lo dictaminado por el Fiscal General Pleé.