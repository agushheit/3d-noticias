---
category: La Ciudad
date: 2021-08-19T06:00:11-03:00
thumbnail: https://assets.3dnoticias.com.ar/KAYAKISTAS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Kayakistas de Santa Fe reclamaron que se trate "urgente" la Ley de Humedales
title: Kayakistas de Santa Fe reclamaron que se trate "urgente" la Ley de Humedales
entradilla: Los activistas ambientales que partieron a remo la semana pasada desde
  Rosario llegaron a Capital Federal para pedir en Diputados de la Nación un avance
  concreto del proyecto.

---
Tras recorrer unos 350 kilómetros en la cuenca del Río Paraná -en una travesía náutica y terrestre inédita-, una caravana de kayakistas ecologistas de la provincia de Santa Fe llegó al Dique Luján, en el partido bonaerense de Tigre, para luego desembarcar en el Congreso de la Nación, en pleno centro de Capital Federal. El objetivo: entregar un petitorio a representantes de la Cámara baja solicitando que se trate de "forma urgente" la Ley de Humedales.

La Multisectorial por los Humedales, impulsora de este reclamo, trabajó en conjunto con casi 400 organizaciones, grupos y movimientos de todo el país para pedir un plenario que trate este proyecto -que surgió de la unificación en un único texto de unas 15 iniciativas legislativas en noviembre del año pasado-, puesto que, si la Cámara de Diputados no aborda la iniciativa antes de fines de año, perderá estado parlamentario en diciembre próximo.

**De Rosario al Congreso: la caravana de kayakistas llegó por la Ley de Humedales**

"Esto es una celebración colectiva. Y como el año pasado no nos prestaron atención, ahora vinimos 'remando al Congreso'", le dijo a El Litoral desde Buenos Aires Julia Vélez, una de las voceras de la Multisectorial. Participantes de la caravana, antes de llegar al Congreso de la Nación, habían realizado intervenciones artísticas y musicales para visibilizar aún más el reclamo.

Eran cuadras y cuadras llenas de manifestantes. Flameaban las banderas "Ley de Humedales Ya" y "Somos Humedal". "Esto es el pedido de muchas organizaciones, y es la pluralidad lo que nos lleva a esta manifestación. Nunca pensamos que iba a ocurrir un acontecimiento así", relató la joven activista.

"Pedimos que se trate el texto unificado del proyecto de ley con las tres comisiones que faltan, para que finalmente llegue al recinto. Porque si llega diciembre y el proyecto no se trata, perderá estado parlamentario", advirtió Vélez. Cabe recordar que el proyecto ya fue abordado por la comisión de Recursos Naturales y Conservación, pero todavía falta que se gire a Agricultura y Ganadería, Presupuesto y Hacienda e Intereses Portuarios, Marítimos, Fluviales y Pesqueros, y se trate en éstas.

Frente al Parlamento nacional, los activistas realizaron una concentración con actos y oradores diversos de todas las ONGs medioambientales que acompañaron la marcha. "Esperamos poder entregar nuestro petitorio y que nos reciban, porque tenemos muchas cosas para decir", agregó Vélez.

**"Ley socioambiental"**

"¿Por qué es tan importante que esta iniciativa se convierta en ley?", consultó este diario a la vocera de la Multisectorial. Vélez respondió: "La gente tiene que saber que un cuarto del territorio nacional son humedales. Y éstos, en su característica primaria, son reservorios que filtran el agua dulce que bebemos. Además, los humedales retienen en sus distintos niveles de tierra el dióxido de carbono para que no nos intoxique el aire que respiramos".

"Si lo humedales no se conservan -agregó Vélez-, vamos a tener hasta problemas para respirar. Hablamos de humedal y debemos pensar en un gran pulmón que filtra el agua y el aire. Y no sólo en la Argentina, sino en la gran región continental. La tierra es sabia, y sabemos que si los humedales no se protegen habrá una gran afectación socioambiental", concluyó.