---
category: Agenda Ciudadana
date: 2021-04-17T06:00:05-03:00
thumbnail: https://assets.3dnoticias.com.ar/VACUNAS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: El domingo llegan dos vuelos con nuevas vacunas
title: El domingo llegan dos vuelos con nuevas vacunas
entradilla: Uno viene de los Países Bajos con 864 dosis de la vacuna AstraZeneca,
  correspondientes al mecanismo internacional Covax; el otro llegará desde Móscú con
  un nuevo lote de la Sputnik V

---
Dos vuelos, uno desde Países Bajos y el otro desde Moscú, llegarán el domingo próximo a la Argentina con más vacunas contra el coronavirus, lo cual permitirá dar continuidad al plan de vacunación que lleva a cabo el Gobierno.

Uno de los aviones pertenece a KLM y llegará al país en vuelo regular con 864 mil dosis de la vacuna AstraZeneca y el otro, de Aerolíneas Argentinas, partirá en la madrugada de este viernes con rumbo a Moscú para traer más dosis.

De acuerdo con lo informado por el Monitor Público de Vacunación del Ministerio de Salud, hasta este viernes se habían distribuido 7.237.108 dosis en todo el país, de las cuales se aplicaron 5.754.919 en 4.996.672 personas con una dosis y en 758.247 con las dos.

El plan estratégico de vacunación contra el SARS-CoV-2 en el país permitió vacunar al 95 por ciento personal de salud con una dosis y 60% el esquema completo; al 65% de personas mayores de 80 años con la primera dosis y más del 60 % de las personas entre 70 y 79 años también con la primera aplicación.

Hasta ahora, Argentina recibió 7.248.208 dosis de vacunas, distribuyó el 97% y también parte del stock estratégico para mantener la vacunación.

**Un nuevo vuelo a Rusia**

El decimosegundo vuelo de Aerolíneas Argentinas en búsqueda de nuevas dosis de vacunas Sputnik V partirá rumbo a Moscú, según confirmó el presidente de la compañía, Pablo Ceriani, a través de su cuenta de Twitter.

“Podemos confirmar que mañana 02am parte el decimosegundo vuelo de @Aerolineas AR con destino a Moscú para traer más dosis de la vacuna Sputnik V. Seguimos aportando nuestra capacidad logística a la campaña de vacunación más importante de la historia”, escribió Ceriani en la red social.

“En los 11 vuelos que realizamos a Rusia ya transportamos 4.467.745 dosis de la vacuna Sputnik V. Contar con una aerolínea de bandera le permite a nuestro país tener una respuesta rápida ante cada necesidad logística para que más ciudadanos estén protegidos y vacunados”, añadió.

El Airbus 330-200 con el número AR1060 despegará del Aeropuerto Internacional de Ezeiza, a las 2 de la madrugada del sábado y tras aproximadamente 15 horas de travesía, aterrizará en el Aeropuerto Internacional de Sheremetievo a las 17 de nuestro país, las 23 de Moscú.

Está previsto que la aeronave permanezca alrededor de 4 horas en la estación aérea rusa para realizar la carga por lo que estima su partida a las 4.30 del domingo (horario local en Moscú), las 22,30 de Argentina y aterrizará en el país ese mismo día, a las 16, bajo el número de vuelo AR1061.

“Desde Aerolíneas tenemos en claro que nuestro rol es ser parte de la solución: más vacunas significan más personas protegidas. Todo el despliegue logístico que realizamos para concretar estos vuelos va en esa línea”, dijo Ceriani.

Hasta el momento, en 11 operaciones completadas hacia la capital de la Federación Rusa se trajeron al país 4.467.745 dosis de la vacuna fabricada por el Instituto Gamaleya.

La vacuna de Sputnik V viaja en contenedores especiales denominados “Thermobox”, los cuales necesitan estar refrigerados mediante el sistema termogel y hielo seco para mantener la temperatura a 18 grados bajo cero.

Además de los 11 servicios a Rusia, la compañía realizó dos vuelos hacia y desde Beijing y en dichas operaciones llegaron al país 1.904.000 dosis de la vacuna fabricada por Sinopharm.

**Más vacunas de AstraZeneca**

Mientras tanto el próximo domingo llegarán al país 864 mil dosis de vacunas AstraZeneca a través del mecanismo del Fondo Global de Acceso a las Vacunas contra la Covid-19 (Covax), que ya envió a la Argentina una partida de 218 mil dosis el pasado 28 de marzo.

Al igual que en aquella ocasión, las vacunas arribarán en las bodegas del vuelo regular de pasajeros KL701, de la compañía de los Países Bajos, KLM, que aterrizará en Ezeiza a las 6.10, proveniente de la ciudad de Amsterdam.

El traslado en bodegas de las dosis de AstraZeneca es posible debido a que sus condiciones de conservación permiten almacenarlas y transportarlas a una temperatura superior a 0 grados, y en packs más reducidos.

El mecanismo Covax fue impulsado por la Organización Mundial de la Salud, la Comisión Europea y Francia para garantizar el acceso equitativo de todos los países a una vacuna, en un acuerdo que fue firmado por 172 naciones, entre ellas la Argentina.

**La continuidad de la campaña de vacunación**

En tanto, la ministra de Salud, Carla Vizzotti; y la asesora presidencial Cecilia Nicolini mantuvieron una reunión con el embajador de India para garantizar la disponibilidad de vacunas en el país que permitan escalar la inmunización de los grupos priorizados.

La reunión bilateral se concretó con el Embajador Extraordinario y Plenipotenciario de la India en la Argentina, Dinesh Bhatia y en la misma línea, las funcionarias argentinas realizaron una videoconferencia con el Embajador Extraordinario y Plenipotenciario de la República Popular China en Argentina, Zou Xiaoli, para favorecer las conversaciones con el laboratorio público chino que produce la vacuna Sinopharm.

"Argentina tiene firmado un contrato con China por el cual tenemos pendiente de entrega dos millones de dosis de Sinopharm, mientras que con India tenemos diferentes líneas estratégicas de posibilidades de acceso que estamos tratando de profundizar y concretar, aunque no tenemos contratos firmados aún", indicó Vizzotti.

Durante las reuniones, ambos diplomáticos se comprometieron a hacer los máximos esfuerzos para acelerar todos los procesos en un escenario mundial con dificultades de acceso a las vacunas, tanto por la escala de producción, la demanda simultánea y la decisión de los países productores de priorizar la inmunización de su población, indicaron fuentes oficiales.

Por su parte, el canciller Felipe Solá pidió al asesor del presidente estadounidense Joe Biden y director para el hemisferio occidental del Consejo de Seguridad Nacional de ese país, Juan González, con quien se reunió en el Palacios San Martín, que libere una partida de dosis de la vacuna AstraZeneca producida por Argentina y México y que están en Estados Unidos.

Y además solicitó por otra partida de mayor volumen del mismo laboratorio que también está en Estados Unidos sin uso y sin destino, añadieron los voceros.

González fue optimista ante el pedido argentino y contó que ya se está trabajando en ello desde que Solá se lo pidió al secretario de Estado, Antony Blinken, el martes pasado, durante un encuentro en Olivos, refirieron las fuentes.