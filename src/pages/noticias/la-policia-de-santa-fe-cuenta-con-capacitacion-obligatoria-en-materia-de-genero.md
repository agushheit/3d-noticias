---
category: Estado Real
date: 2021-01-04T09:33:26Z
thumbnail: https://assets.3dnoticias.com.ar/040121-policia-genero.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: La Policía de Santa Fe cuenta con capacitación obligatoria en materia de
  género
title: La Policía de Santa Fe cuenta con capacitación obligatoria en materia de género
entradilla: El objetivo es garantizar buenas prácticas en el abordaje de las situaciones
  de violencia de género que llegan desde la comunidad.

---
La formación con perspectiva de género y diversidad sexual, dirigida a las y los agentes de la Policía, es fundamental para garantizar buenas prácticas en el abordaje de las situaciones de violencia de género que llegan desde la comunidad. Además, permite visibilizar y erradicar las discriminaciones y situaciones de violencia que pueden darse en el ámbito laboral de la institución policial.

En relación con el cumplimiento con la Ley Provincial 13.891 y el Decreto Reglamentario 192/2020 «Ley Micaela», el gobierno de la provincia de Santa Fe, a partir del trabajo articulado de la secretaría de Estado de Igualdad y Género, el ministerio de Economía y la subsecretaria de Bienestar y Género de la Policía, dependiente del Ministerio de Seguridad, pusieron en marcha el Curso de Capacitación en Género en el marco de la «Ley Micaela».

**Se trata de una iniciativa sin precedentes, ya que por primera vez en todo el país se pone a disposición un curso virtual con perspectiva de género, de carácter obligatorio**, en el cual se propone una primera instancia de capacitación para funcionarios y funcionarias de la Policía de la provincia de Santa Fe.

A partir del trabajo de la subsecretaría de Bienestar y Género en la Policía del Ministerio de Seguridad, el Curso de Capacitación Obligatoria en Género en el marco de la «Ley Micaela», comenzó a dictarse desde el mes de noviembre y en esta primera etapa incluyó a los escalafones más altos de la Policía de Santa Fe.

La primera cohorte estuvo integrada por el personal jerárquico de Jefatura, Dirección y/o Departamento Provincial, Jefatura de Unidades Regionales, Jefatura de Policías Especiales, Agencia de Investigación Criminal, Agencia de Control Policial, Central de Emergencias 911 e Instituto de Seguridad Pública (ISEP).

Al respecto, la subsecretaria de Bienestar y Género en la Policía, Natacha Guala, expresó: «la capacitación en la Ley Micaela y su aplicación al ámbito de la policía de Santa Fe es una propuesta sin precedentes, y es muy importante que se haya incluido, ya que es la primera institución que interviene en el abordaje de víctimas que han atravesado conflictos relacionados con la violencia de género, identidad y/o diversidad sexual, por lo tanto, que el personal policial pueda contar con estas herramientas hace que se evite la revictimización en la ruta del conflicto y se pueda trabajar para visibilizar esta problemática de manera eficaz».

Las políticas de género integradas en este ámbito resultan centrales para el proceso de democratización y reforma que se está llevando adelante desde el Ministerio de Seguridad, ya que a través de ellas se trabaja para **garantizar la igualdad de derechos, abordando transversalmente la perspectiva de género, no solo dentro de la institución policial, sino también en el tratamiento responsable para con la comunidad**.

«La implementación de Ley Micaela incluye el trabajo conjunto con el Ministerio de Economía y la Secretaria de Igualdad y Género en el Estado, hemos articulado con ambas carteras, y por nuestra parte hemos ejecutado las propuestas para concretar esta capacitación al interior de la institución policial. En este sentido, consideramos que la articulación con otros organismos tanto públicos como privados son muy importantes para garantizar políticas públicas integrales», agregó la funcionaria.

Además, destacó que «la incorporación de la agenda de igualdad y con perspectiva de género resulta muy necesaria y todavía hay mucho por hacer. También notamos que estos espacios de formación son muy bien recibidos por el personal policial. En ese sentido estas iniciativas aportan a modernizar y democratizar la policía, y encontramos un fuerte potencial para seguir incorporando estas propuestas».

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.1em; font-weight: 700">ACTIVIDADES CON PERSPECTIVA DE GÉNERO</span>**

El 25 de noviembre, en el «Día internacional contra la violencia hacia las mujeres», la subsecretaría de Bienestar y Género en la Policía del Ministerio de Seguridad realizó una serie de encuentros con mujeres policías en distintas localidades de la provincia.

Estas instancias tuvieron como objetivo trabajar en torno a los **derechos de las mujeres a vivir una vida libre de violencias en todos los ámbitos**, a partir del marco normativo y las reglamentaciones vigentes en la materia, así como también abordar específicamente la violencia de género en el ámbito laboral.

Asimismo, resultaron muy importantes para relevar aquellas problemáticas que las trabajadoras policiales identifican como prioritarias para el desarrollo de políticas públicas que tiendan a una inclusión plena de las mujeres en la institución policial.

Entre otras, **emergieron las políticas de conciliación entre las responsabilidades laborales y de cuidado, la necesidad de fortalecer el liderazgo y desarrollo de la carrera profesional de las mujeres y la formación y capacitación**, contemplando el acceso de quienes trabajan en las distintas regiones de la provincia.

Dichos encuentros se realizaron con trabajadoras de las diferentes Unidades Regionales de Santa Fe; Rosario; General Obligado; General López y Garay. De las mismas participaron 206 mujeres policías.

Además, la subsecretaria trabaja permanentemente en talleres relacionados con la promoción de derechos humanos, género, identidad y/o diversidades sexuales, con el objetivo de poder ejecutar un abordaje integral frente a los problemas de violencias y desigualdades con perspectiva de género dentro de la institución policial, y también generar instancias para que las mujeres policías puedan acceder a espacios de protección integral contra las violencias.

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.1em; font-weight: 700">CONSEJO CONSULTIVO DE POLÍTICAS DE GÉNERO EN LA POLICÍA</span>**

El Consejo es un espacio de participación plural para el debate y articulación de propuestas y acciones transversales orientadas a la promoción de la igualdad de género en la Policía.

Está formado por representantes de la Secretaría de Estado de Igualdad y Género, la Secretaría de Derechos Humanos de la Provincia de Santa Fe, referentes especialistas de las Universidades Nacionales del Litoral, Rosario, Rafaela y UTN, así como también por mujeres policías que se desempeñan como jefas tanto en el ámbito de la Jefatura Provincial, como de las distintas Unidades Regionales y Agencias. Asimismo, participan referentes de la Red de Mujeres Policías de Santa Fe.

La principal misión del Consejo es construir diagnósticos, elaborar propuestas, efectuar recomendaciones y realizar evaluaciones de las medidas puestas en marcha para eliminar los obstáculos de género dentro del sistema policial, garantizar el ingreso, permanencia y ascenso de las mujeres a la institución en igualdad de condiciones que sus pares varones.

También se pueden sugerir cursos de acción para generar políticas transversales con el objeto de abordar y erradicar las violencias contra las mujeres en el sistema policial.