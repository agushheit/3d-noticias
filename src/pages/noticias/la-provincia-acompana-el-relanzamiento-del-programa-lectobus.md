---
category: Estado Real
date: 2021-04-18T06:00:16-03:00
thumbnail: https://assets.3dnoticias.com.ar/lectobus.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Provincia acompaña el relanzamiento del programa "Lectobus"
title: La Provincia acompaña el relanzamiento del programa "Lectobus"
entradilla: La iniciativa del Sindicato de Luz y Fuerza tiene por objetivo acercar
  a niñas y niños herramientas que potencien su experiencia vinculada a la lectura
  en voz alta, entre otros recursos.

---
El Ministerio de Cultura se une a la iniciativa del Sindicato de Luz y Fuerza de la capital provincial, que propone acercar a niñas y niños herramientas que potencien su experiencia vinculada a la lectura en voz alta, entre otros recursos.

El programa estará coordinado por Alicia Barberis, Joselina Martínez, Emilia Charra y Celeste Abbaa, reconocidas narradoras orales que capacitarán a residentes de los espacios culturales de Aceria, Las Flores, Coronel Dorrego, Yapeyú y San José del Rincón. Y a modo de práctica, realizarán un espectáculo por mes, en diferentes vecinales de la ciudad de Santa Fe, destinado a niñas, niños, familias y vecinos y residentes.

De ese modo, además de brindar un momento recreativo vinculado a la oralidad y la lectura en voz alta, estarán promocionando la lectura de literatura y aportando una práctica referencial para enriquecer la formación de los residentes culturales que participen en los talleres a lo largo del año.

Por el Ministerio de Cultura participaron del acto la directora de Programas Socioculturales Santa Fe, Mariana Escobar, y el director de Programación Artística, Claudio Cherep. En tanto, por el gremio, estuvo presente el prosecretario general, Pablo Colla; los pro secretarios gremiales German Henze y Guillermo Pérez Possi; el asesor del senador Marcos Castelló, Javier Berlanda, y las escritoras Alicia Barberis, Joselina Martínez.

Además el proyecto estará acompañado por la cartera de Cultura y por el senador provincial por el departamento La Capital, Marcos Castelló.

**El Programa Lectobus**

Lectobus tiene por objetivo que los niños y las niñas adquieran recursos para entrenar sus saberes vinculados con la narración oral de cuentos de diferentes fuentes: populares, creaciones propias, de autor reconocido.

A su vez propone acercar herramientas que potencien su experiencia vinculada a la lectura en voz alta y a los múltiples lenguajes que ofrece el libro álbum. De este modo se fortalecen las estrategias de mediación, previas y posteriores a la lectura, orientadas también a la construcción cooperativa de sentido del texto literario.