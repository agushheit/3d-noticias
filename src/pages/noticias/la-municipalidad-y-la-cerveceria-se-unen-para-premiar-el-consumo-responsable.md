---
category: La Ciudad
date: 2021-12-17T06:15:09-03:00
thumbnail: https://assets.3dnoticias.com.ar/escabioresponsable.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Municipalidad y la Cervecería se unen para premiar el consumo responsable
title: La Municipalidad y la Cervecería se unen para premiar el consumo responsable
entradilla: "Con el mensaje “si vas a manejar, no tomes alcohol” la Municipalidad
  de Santa Fe y la Cervecería lanzaron el programa Full Responsable para fomentar
  la conciencia y la responsabilidad.\n\n"

---
Este jueves, la Municipalidad y Cerveza Santa Fe lanzaron Full Responsable, una campaña que busca promover la figura de conductor designado y el consumo responsable de alcohol. De este modo, quienes visiten bares y restaurantes de la capital provincial, podrán ofrecerse como conductores designados y ponerse a prueba a través de un juego que desafía habilidades de manejo.

Durante la presentación, que se realizó en un club de la costa santafesina, el intendente Emilio Jatón mencionó la importancia de que el Estado Municipal y una empresa privada trabajen en conjunto para mejorar la vida de los habitantes de la ciudad. En el mismo sentido, puso el eje en la necesidad de “ser responsables a la hora de conducir, no sólo por nuestra vida sino por la de los demás”.

“Esto que estamos trasladando a través de una campaña es una buena responsabilidad. Para nosotros es muy importante que la campaña se entienda en serio y la tomemos como una responsabilidad ciudadana, no como un márketing. Los incentivos vienen bien, pero todo empieza por la responsabilidad de cada uno”, afirmó.

Posteriormente, el secretario General de la Municipalidad, Mariano Granato, indicó que “la campaña surge de un trabajo articulado entre lo público y lo privado, y tiene como objetivo promover buenos hábitos de consumo responsable”. Según expresó, “no se trata de desincentivar el consumo, sino que es una propuesta que va por la positiva: empoderar a quienes asumen el compromiso de no tomar si van a conducir”, agregó.

Por su parte, la gerenta de marketing de Cervecería, Bárbara Dergal, afirmó que “disfrutar de una cerveza es perfectamente compatible con un estilo de vida saludable, aunque es perjudicial si se hace en exceso. Elegir un conductor designado es una forma de promover un consumo inteligente, cuidándote y cuidando a los demás”, insistió.

**La campaña**

Cabe recordar que la Municipalidad y Cerveza Santa Fe trabajan sobre la importancia del consumo responsable hace tiempo, a través de diferentes iniciativas. En esta oportunidad, la activación se desarrolla también en conjunto con bares asociados.

Según se detalló, en cada local habrá un código QR para inscribirse. El conductor designado recibirá obsequios y podrá participar de sorteos semanales. La iniciativa se pondrá en marcha este jueves en los locales Paladar negro, Proa, Bar de copas, Le chat, Palo y hueso, Hammer y Bilbao Bv.

También estuvieron en la presentación, el secretario de Producción y Desarrollo Económico municipal, Matías Schmüth; la concejala Laura Mondino; el gerente de la Cámara de Hoteleros Gastronómicos, Martín Castro; y, en representación de la Cámara de Cerveceros, Emiliano Moretti.