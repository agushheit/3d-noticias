---
layout: Noticia con imagen
author: "-"
resumen: "Recolección de ramas"
category: Estado Real
title: Informe sobre la recolección de ramas y reparación de postes
entradilla: "Hoy al mediodía, la Municipalidad terminó de liberar todas las
  calles cuyo tránsito había estado interrumpido parcialmente por caídas de
  árboles de la última tormenta."
date: 2020-10-22T20:55:25.188Z
thumbnail: https://assets.3dnoticias.com.ar/ramas.jpg
---
La Municipalidad informa que, tras el fenómeno meteorológico ocurrido en nuestra ciudad los días lunes y martes, hoy al mediodía se completaron las tareas para liberar todas las calles en las que había árboles y postes caídos. Por lo tanto, la circulación se ve normalizada. Entre el jueves y el viernes se procederá a retirar los restos de madera acumulados en las veredas de las zonas afectadas.

A partir de la tormenta que se desató el lunes por la tarde y que continuó el martes hasta cerca del mediodía se registró una gran cantidad de árboles caídos y de postes de alumbrado dañados. A través del sistema de Atención Ciudadana de la Municipalidad se recibieron 115 reclamos de este tipo y otros 47 fueron relevados por personal propio.

De inmediato se procedió a la atención de los casos mas complejos, que fueron 37. En varios de ellos se trabajó en coordinación con la Empresa Provincial de la Energía y el Cobem por la complejidad de las situaciones.

En tanto, las situaciones de resolución más sencilla fueron abordadas por las cuadrillas municipales que están preparadas para tal fin. Además se articularon algunas tareas menores con las instituciones que ya brindan su ayuda en espacios como la plaza Pueyrredón, la plaza de las Banderas, y en las zonas de barrio Candioti y de bulevar Gálvez.

En cuanto a las tareas de alumbrado, se informó que, de las denuncias recibidas y los hechos relevados por el municipio, 19 sectores ya están normalizados, en 22 casos se procedió al realizar inspecciones y retiro de postes para resolver situaciones de riesgo y se repararon 15 luminarias. Quedan unos 40 reclamos que serán resueltos entre lo que queda de la jornada (en turno tarde y noche) y mañana por la mañana.

De todas maneras, se comunica a la población que el retiro de ramas, troncos y otros elementos similares se está realizando con camiones propios y retropalas y se estima que las tareas continuarán hasta el viernes porque se dio prioridad a habilitar la circulación.

Al respecto, desde la Municipalidad se recuerda que por consultas y reclamos, las y los vecinos pueden comunicarse al 0800 777 5000 o a través de www.santafeciudad.gov.ar.