---
category: Estado Real
date: 2021-03-03T05:50:12-03:00
thumbnail: https://assets.3dnoticias.com.ar/humedales.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: Provincia y Nación avanzan en la puesta en marcha de un inventario de humedales
  en territorio santafesino
title: Provincia y Nación avanzan en la puesta en marcha de un inventario de humedales
  en territorio santafesino
entradilla: "“Saber qué tenemos y dónde está implica hablar de ordenamiento territorial
  para preservar estas zonas, planificando un desarrollo sostenible”, explicó la ministra
  de Ambiente, Erika Gonnet."

---
El gobierno de Santa Fe, a través del Ministerio de Ambiente y Cambio Climático, realizará un inventario de Humedales, en el marco de diversas acciones vinculadas a la puesta en valor, regeneración y ampliación de áreas protegidas en la provincia. El mismo se desarrollará de manera conjunta con el Ministerio de Ambiente y Desarrollo Sostenible de Nación, que conduce Juan Cabandié.

En ese marco, este martes se llevó a cabo un primer encuentro de trabajo en el Acuario del río Paraná, en la ciudad de Rosario, del que participaron la ministra de Ambiente y Cambio Climático de Santa Fe, Erika Gonnet; la secretaria de Política Ambiental en Recursos Naturales de Nación, Florencia Gómez; y la directora Nacional de Aguas y Ecosistemas Acuáticos de Nación, Gabriela González Trilla.

“La provincia va a hacer su inventario de humedales en relación con lo que venimos haciendo desde el año pasado. Santa Fe, siempre a favor de la Ley de Humedales nacional, ha hecho una consulta con diferentes instituciones de la provincia y lo hemos elevado al Congreso de la Nación. Teniendo en cuenta que tenemos una ley provincial, pero que no contemplaba el tema de los inventarios, propusimos uno como decisión de la gestión del gobernador Omar Perotti y lo estamos llevando adelante”, explicó Gonnet.

Y detalló: “El inventario de humedales es un trabajo extenso; no se hace de un día para otro. Todos los que vivimos en esta zona sabemos lo que hemos padecido con el tema de los incendios. Y saber qué tenemos, dónde está y qué cosas se pueden hacer allí, es hablar de ordenamiento territorial. Son cosas que no han sido fáciles de tratar, a veces ha habido puntos de confrontación, y lo que hacemos es sentarnos todos en una misma mesa para preservar lo que tenemos frente a nuestras costas y trabajar seriamente en algo que va a estar relacionado con la Ley Nacional de Humedales”.

“Nación va a ser nuestra guía en esto”, dijo Gonnet, e indicó que la visita de las funcionarias nacionales es “justamente para trazar los ejes de trabajo”.

Por su parte, Gómez, manifestó que “hemos venido a la provincia de Santa Fe a iniciar el trabajo vinculado al inventario de humedales de la provincia, por iniciativa del gobierno provincial, y porque creemos que es necesario conocer los humedales para poder protegerlos”.

Finalmente, González Trilla señaló: “Vamos a acompañar desde el ministerio, sobre todo con cuestiones técnicas. El proceso de inventario que la provincia decidió iniciar, nosotros lo vamos a vincular con el inventario nacional de humedales. Y hoy damos inicio a este vínculo y esperamos poder seguir trabajando por mucho tiempo”.

**AMPLIACIÓN DE ÁREAS PROTEGIDAS**

Cabe recordar que el gobierno de Santa Fe, a través del Ministerio de Ambiente y Cambio Climático, desarrolló la iniciativa #RegeneraSantaFe, que propone un esquema estratégico de ampliación e incorporación de áreas protegidas en zonas del Delta e Islas del río Paraná, para su protección y conservación.

\#RegeneraSantaFe promueve actividades productivas sostenibles; desarrollo de capacidades técnicas; diseño de espacios de aprendizaje y programas de capacitación para el desarrollo o mejora de capacidades; diseño de planes GIRSU y promoción de la economía circular; desarrollo de infraestructura para el ecoturismo.

“El Delta del Paraná es un humedal que en los últimos tiempos ha sufrido una importante degradación a causa de los incendios, por su importancia y por los múltiples valores ecosistémicos que brinda, creemos fundamental incorporar áreas protegidas al sistema provincial, haciendo foco exclusivo, en este caso, en el río Paraná y sus islas”, detalló Gonnet.

Y amplió: “En base al desarrollo provincial propuesto, esta etapa incluye la incorporación de más de 2.600 hectáreas que comprenden a la Isla La Fuente en Reconquista, la Isla Sabino Corsi frente a Rosario, la Isla Pereyra en Villa Constitución, la Isla Los Mástiles frente a Granadero Baigorria y Capitán Bermúdez, y el Islote perteneciente a la localidad de General Lagos”.

En ese sentido, la ministra puso en valor “la ampliación de 1.000 hectáreas de la zona protegida de la Isla El Pelado, en la región de islas frente a Puerto Gaboto, lo que amplía considerablemente el área protegida que contempla al Parque Nacional Islas de Santa Fe”.

![](https://assets.3dnoticias.com.ar/humedales.jpeg)