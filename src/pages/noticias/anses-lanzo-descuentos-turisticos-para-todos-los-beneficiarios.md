---
category: Agenda Ciudadana
date: 2021-07-23T08:00:35-03:00
thumbnail: https://assets.3dnoticias.com.ar/ANSES2.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: ANSES lanzó descuentos turísticos para todos los beneficiarios
title: ANSES lanzó descuentos turísticos para todos los beneficiarios
entradilla: La Administración Nacional de la Seguridad Social ofrece una propuesta
  para las vacaciones que incluye traslados y alojamientos en diferentes destinos

---
La Administración Nacional de la Seguridad Social (Ansés), a cargo de Fernanda Raverta, lanzó una serie de descuentos para fomentar el turismo tras el tiempo de pandemia, con el objetivo de reactivar el sector.

De esta manera, varias de las personas que cobren algunas de las prestaciones que ofrece la entidad podrán utilizar estas ofertas para viajes a distintos destinos durante las vacaciones de invierno con el fin de facilitar y abaratar la oportunidad de desplazarse para recrearse y conocer.

El sector turístico resultó ser uno de los más golpeados por la crisis sanitaria a lo largo de los últimos años. De esta manera, el Gobierno busca apelar a los sectores más vulnerables para que puedan trasladarse por el país. Así, se le brindarán más posibilidades con descuentos y promociones para el transporte.

Se trata de descuentos en empresas de transporte de larga distancia y una amplia lista de alojamientos en distintos puntos turísticos locales. A continuación, todos los detalles.

**Quiénes pueden acceder a los descuentos en viajes**

Podrán acceder aquellas personas que cobren alguna prestación de la seguridad social. Están habilitados los que cobren:

Jubilaciones y pensiones de Ansés, Pensiones no contributivas, Asignación Universal por Hijo, Asignación por Embarazo, Asignación Familiar por Hijo

Asignación por Prenatal, Prestación por Desempleo, Progresar, Programas del Ministerio de Desarrollo Social, Jubilaciones y pensiones del IPS, BAPRO y Policía de la Provincia de Buenos Aires.

Si la prestación fue dada de alta recientemente, el reintegro que corresponda se realizará una vez que el banco le envíe a la Ansés los datos del CBU. Se debe tener en cuenta que este trámite puede tardar hasta 90 días.

**Qué empresas ofrecen los descuentos**

Los descuentos estarán disponibles principalmente para empresas de transporte y alojamiento para turistas de distintos puntos turísticos del país.

Estas son: Plusmar, Hotel Ushuaia, Andesmar, Tramat, Jetmar, Nueva Chevallier SA., Empresa Argentina de Servicio Público, Empresa Gral. Urquiza.

Ticket en Bus, Hotel Plaza Hotel Internacional de Turismo, Siempre Sol, Howard Johnson, Hotel Iguazú Express.

**Cómo acceder a los descuentos en viajes que ofrece Ansés**

Para poder iniciar el trámite se debe acceder al sitio web de “Beneficios ANSES”. A través del buscador, se podrá filtrar la búsqueda para ver los comercios adheridos y así identificar qué firmas están adheridas a la iniciativa. Estos últimos son: nombres de comercio, provincia, rubro y tipo de descuento, en el que te dará la opción de cobrar por reintegro o por línea de caja.

El monto ahorrado se recibirá dentro de los siete días hábiles en la cuenta bancaria del beneficiario.