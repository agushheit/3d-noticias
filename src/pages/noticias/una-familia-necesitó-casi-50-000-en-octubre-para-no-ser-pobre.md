---
layout: Noticia con imagen
author: "Fuente: NA"
resumen: "Canasta Familiar "
category: Agenda Ciudadana
title: Una familia necesitó casi $50.000 en octubre para no ser pobre
entradilla: La Canasta Básica Total aumentó 5,7% con respecto a septiembre y
  registró su mayor incremento en dos años, al crecer por encima de la inflación
  por segundo mes consecutivo.
date: 2020-11-18T13:55:18.056Z
thumbnail: https://assets.3dnoticias.com.ar/canasta-familiar.jpeg
---
El costo de la Canasta Básica Total (CBT), que define el nivel de pobreza, subió a $49.912 en octubre último para una familia tipo y aumentó 5,7% con respecto a septiembre, informó hoy el INDEC.

Esta variable clave registró su mayor incremento en dos años y creció por encima de la inflación por segundo mes consecutivo.

La Canasta Básica Alimentaria (CBA), que define el nivel de indigencia, registró en el mismo mes un costo de $20.710, y aumentó 6,6%.

En los últimos doce meses la canasta alimentaria registra un aumento del 45,8% y la canasta básica aumenta 40%, ambas por encima del costo de vida del 37,2%.

Desde diciembre último, el costo de la canasta total creció 28,1% y el de la alimentaria 32,9%, por encima de la inflación del 26,9% en ese período.

El costo de la canasta total en los primeros diez meses del año aumentó $10.951 y el de la alimentaria subió $5.126.

**El costo de la canasta total casi triplica el salario mínimo vital y móvil, que desde octubre se fijó en $18.900 los trabajadores mensualizados que cumplen jornada legal completa de trabajo.**

Los datos reflejan un incremento en los niveles de pobreza e indigencia en medio de la pandemia.