---
category: Agenda Ciudadana
date: 2021-11-18T06:15:15-03:00
thumbnail: https://assets.3dnoticias.com.ar/diamilitancia.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: INFOBAE
resumen: Los movimientos sociales mostraron su alineamiento detrás de Alberto Fernández
  e impulsan su reelección
title: Los movimientos sociales mostraron su alineamiento detrás de Alberto Fernández
  e impulsan su reelección
entradilla: 'Las principales organizaciones estuvieron presentes en Plaza de Mayo
  y destacaron el discurso del Presidente. Las tensiones con el kirchnerismo duro

'

---
Los movimientos sociales alineados con el Frente de Todos volvieron a mostrar poder de movilización. Sus columnas fueron iguales o superiores a los gremios de la CGT. Al menos eso interpretaron los integrantes de Somos Barrios de Pie, la Corriente Clasista y Combativa (CCC) y el Movimiento Evita, los más numerosos a la hora ocupar espacios en Plaza de Mayo. La mayoría de las agrupaciones que integran la Unión de Trabajadores de la Economía Popular (UTEP) dijeron presente a la hora de apoyar a Alberto Fernández después de la derrota electoral del domingo pasado.

Desde la campaña electoral que lo llevó a la Casa Rosada, los movimientos sociales apoyaron a Fernández y el jefe de Estado los incluyó en espacios de poder, como en el ministerio de Desarrollo Social, Jefatura de Gabinete y Obras Públicas, entre otros ministerios.

Después de la derrota electoral en las PASO, ya habían promovido una marcha para empoderarlo tras la carta de la Vicepresidente, Cristina de Kirchner, que pedía cambios en el gabinete y del rumbo económico. Pero el propio Fernández les pidió “bajarla” porque iba a ser interpretada como una manifestación contra la dos veces presidenta.

Fueron esas mismas agrupaciones las que recorrieron unos 1.500 barrios populares del conurbano bonaerense para identificar a los posibles votantes peronistas que no fueron a sufragar el 12 de septiembre y convencerlos para que lo hagan el 14 de noviembre. Ese domingo movilizaron a casi cien mil personas para facilitarle el acceso a los cuartos oscuros a través de casi veinte mil vehículos. Fernández agradeció varias veces el esfuerzo.

A diferencia de lo ocurrido hace dos meses, esta vez, junto a la CGT y otros espacios que conforman el Frente de Todos, los movimientos sociales llenaron la histórica Plaza de Mayo. Sus principales dirigentes alientan la reelección del Alberto Fernández en 2023, un hecho que ya propusieron varios de sus ministros, como el de Desarrollo Social, Juan Zabaleta y el de Seguridad, Aníbal Fernández.

Los movimientos sociales que junto a la CGT organizaron la convocatoria en el Día de la Militancia son los que manejan la mayor cantidad de planes sociales. Esta tarde, desde el atril, el Presidente insistió en que aspira a reemplazar esa ayuda social, como el Potenciar Trabajo, por empleo genuino.

**Las repercusiones**

A las cinco de la tarde en punto, tal como estaba previsto, el jefe de Estado brindó un breve discurso a los más de cien mil manifestantes que lo ovacionaron. Fernando “Chino” Navarro, funcionario nacional y referente del Movimiento Evita, en diálogo con Infobae destacó: “A 49 años del retorno de Perón a la Argentina, después de 18 años de exilio, Alberto Fernández demostró que es un Presidente, pero también es un militante y nos convocó a reconstruir a la Argentina desde el diálogo, desde el consenso, sin bajar las banderas y buscando combatir la pobreza, la inflación, construir una patria justa”.

Emilio Pérsico, uno de los dirigentes de mayor influencia en los movimientos populares, y como Navarro funcionario del Gobierno y referente del Evita, ponderó el compromiso que desde el atril expresó Fernández para “reactivar la economía y generar puestos de trabajo”. Sobre la masiva concentración, opinó: “Fue para reforzar la gestión presidencial porque nos representa en todas las discusiones. El Presidente debe estar en las mejores condiciones, sumar poder al poder es parte del proceso de la democracia, sumarle poder al que gobierna”, dijo y entendió que “una de las maneras de resolver los problemas es que los pobres hagan más política y participen más”.

Daniel Menéndez, coordinador nacional de Somos Barrios de Pie, le dijo a Infobae que el de hoy fue “un acto masivo en respaldo a un rumbo y una nueva etapa de gobierno que tiene que ver, como lo expresó Alberto, en la recuperación del poder adquisitivo del salario, avanzar en el crecimiento de las familias de las trabajadoras y los trabajadores y en avanzar en el contrato social que firmamos en 2019

Por su parte, Gildo Onorato, secretario gremial de la UTEP, interpretó ante este medio que el acto “fue el relanzamiento de un gobierno que asume las cosas que faltan hacer, pero con la decisión de afrontar el futuro.” Sobre el discurso de Fernández, destacó que exaltó al “trabajo como vertebrador de la recuperación” y que realizó una “gran reivindicación a la militancia”. “Sin lugar a duda, las palabras del Presidente fueron muy esperanzadoras, más si tenemos en cuenta que estamos retornando a la normalidad. Los trabajadores y los movimientos Populares tenemos la voluntad de defender el rumbo y profundizar los cambios necesarios para reducir las desigualdades”, afirmó.

**La convocatoria**

Dirigentes sociales como Pérsico, Navarro, Onorato, Menéndez y el diputado Juan Carlos Alderete, el líder de la CCC, junto a los principales referentes de sus agrupaciones en los barrios más populares del conurbano bonaerense, se pusieron la movilización de hoy al hombro y organizaron las columnas que desde las 10 de la mañana se concentraron en municipios como Florencio Varela, La Matanza, Moreno, San Martín, Lanús, Avellaneda y Morón.

Junto a ellos, y con diversas consignas, lo hicieron otras agrupaciones más pequeñas, pero no por eso menos ruidosas como la Organización Libres del Pueblo (OLP) que lo hizo bajo el lema “No nos han vencido”; la Corriente Pueblo Unido, cuyos volantes expresaban “en unidad a Plaza de Mayo y en todas las provincias”. También lo hizo el Frente Nacional y Popular bajo el lema “Bancamos este proyecto nacional y popular” y la Corriente Nuestra Patria.

La mayoría de estas organizaciones, con el Evita a la cabeza, ingresaron en caravana a la Ciudad de Buenos Aires poco después de las 14, se concentraron en Avenida de Mayo y 9 de Julio, una zona muy próxima al ministerio de Desarrollo Social, y desde allí marcharon a Plaza de Mayo.

Otras agrupaciones, que también forman parte de la UTEP, se expresaron a favor de marchar en el Día de la Militancia para apoyar a Fernández; sin embargo, no “trabajaron” como otros espacios en la convocatoria, al menos así lo han observado varios dirigentes que integran la UTEP.

El Movimiento de Trabajadores Excluidos (MTE), liderado por Juan Grabois, por ejemplo, no convocó a la Plaza de Fernández, pero sí lo hizo su espacio político el Frente Patria Grande.

\-¿Por qué razón el MTE, que forma parte de la UTEP, no convocó al acto en apoyo a Alberto Fernández?, le preguntó Infobae a Grabois.

\- Es difícil de explicar, pero para nosotros el movimiento social y la UTEP tienen una función fundamentalmente laboral, gremial, reivindicativa; las movilizaciones políticas para nosotros deben ser motorizadas por las organizaciones políticas. Tanto yo como el Frente Patria Grande convocamos y acompañamos la movilización como parte de una expresión política, pero como dirigente social y parte del movimiento de economía popular considero que la agenda prioritaria de los movimientos debe ser luchar denodadamente contra la miseria que sufre más del 40% de la población argentina y para avanzar en el estricto cumplimiento de la agenda de tierra, techo y trabajo. Creo que ese es nuestro deber.

“Militamos para transformar”, fue el lema del Frente Patria Grande, que a través de un comunicado en sus redes sociales expresó: “Este 17 de noviembre participamos del acto convocado por el Frente de Todos en el Día de la Militancia para reafirmar el contrato electoral de 2019 y fortalecer la orientación que empezamos a transitar después de las PASO cuando la dirigencia y la militancia del Frente de Todos volvió a escuchar las genuinas demandas del pueblo”.

Si bien en este momento la relación entre Grabois y Cristina Kirchner no pasa por su mejor momento, el dirigente y abogado está alineado mucho más a la figura de la Vicepresidenta que al primer mandatario. De hecho, Grabois es uno de los asiduos asistentes al Instituto Patria.

De hecho, cuando Alberto Fernández llamó a “festejar la victoria” electoral del domingo pasado desde el escenario del bunker del Frente de Todos a la Plaza de Mayo, el kirchnerismo no iba a movilizar.

La cara del diputado nacional Máximo Kirchner parecía expresar enojo o desconcierto al escuchar la convocatoria del Presidente. Antes, la diputada electa Victoria Toloza Paz agradeció el trabajo que realizaron los movimientos sociales para intentar dar vuelta la elección en la provincia de Buenos Aires, un objetivo que quedó a un punto porcentual.

Lo mismo realizó el jefe de Estado, aunque puso más ahínco en su mención a la CGT y los dirigentes gremiales. Sergio Palazzo, el titular de La Bancaria, por ejemplo, fue ubicado, a pedido de la ex mandataria, en el cuarto puesto en la lista de candidatos a diputados nacionales por la provincia gobernada por Axel Kicillof.

La marcha en apoyo a la gestión de Fernández fue anunciada poco después de la dura derrota del Frente de Todos en las PASO como espaldarazo a su gestión ante la embestida opositora. De inmediato, referentes del Movimiento Evita y la CCC se reunieron con Héctor Daer, uno de los actuales miembros del triunvirato cegetista, -junto a Pablo Moyano y Carlos Acuña-, para sumarse a la movilización.

Hasta ese momento, el kirchnerismo, expresado en La Cámpora, y referenciado en Máximo Kirchner, Andrés “Cuervo” Larroque y Eduardo “Wado” De Pedro -el primero ministro de Kicillof y el segundo de Fernández- no iban a participar de la movilización que se presentaba, además, como un “relanzamiento de la segunda etapa del gobierno nacional”.

El Movimiento Evita, a través de Emilio Pérsico, y Somos Barrios de Pie, con Daniel Menéndez a la cabeza -ambos funcionarios en el ministerio de Desarrollo Social- ya habían anunciado una concentración en Plaza de Mayo después de la derrota en las PASO y la explosiva carta de Cristina Kirchner.

En uno de sus párrafos, la Vicepresidenta escribió: “Creía que se estaba llevando a cabo una política de ajuste fiscal equivocada que estaba impactando negativamente en la actividad económica y, por lo tanto, en el conjunto de la sociedad y que, indudablemente, esto iba a tener consecuencias electorales. No lo dije una vez… me cansé de decirlo… y no sólo al Presidente de la Nación. La respuesta siempre fue que no era así, que estaba equivocada (…)”

A pedido del propio jefe de Estado, Pérsico “levantó” la movilización en respaldo a Fernández ante la posibilidad que fuese interpretada como una “marcha contra Cristina”. Políticamente era así.

El lunes 15 de noviembre, horas después de la invitación de Alberto Fernández al presunto festejo por una victoria electoral, que en realidad no lo fue, los popes de la CGT y de las organizaciones populares volvieron a reunirse, esta vez a contrarreloj , pero con los espacios que no habían convocado, como La Cámpora. Café y agua mineral mediante, se acordó la participación de todos los espacios del Frente de Todos. Por esa razón el lema sería “TODOS”.

El Frente Popular Darío Santillán, al igual que el MTE, más referenciado en el kirchnerismo que en el “albertismo”, llamó a la concentración. La organización tiene a la militante feminista Dina Sánchez como a su principal referente. Sánchez suele expresar opiniones críticas -como Grabois- a las políticas económicas del Gobierno.

De hecho, a través de sus redes sociales, el Frente Popular Darío Santillán expresó: “En el día de la militancia, estaremos en la calle. Movilizamos a Plaza de Mayo para acompañar al gobierno nacional y para plantear cambiar el rumbo económico. La deuda más importante de Argentina es con su pueblo. Las corporaciones económicas, los que ganaron siempre, la derecha política, quiere avanzar sobre los sectores populares. Lo dijeron en la campaña. Ellos quieren avanzar con la flexibilización laboral, destruir la ley de alquileres. Hoy se discute ellos o todos. Pueblo o corporaciones”.

Con diferencias y coincidencias, los movimientos sociales volvieron a demostrar su alineamiento con Alberto Fernández “el único líder de nuestro espacio político”, como definió Fernando “Chino” Navarro.