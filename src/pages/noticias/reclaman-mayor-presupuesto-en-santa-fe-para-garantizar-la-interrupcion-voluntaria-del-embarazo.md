---
category: Agenda Ciudadana
date: 2021-09-29T06:00:03-03:00
thumbnail: https://assets.3dnoticias.com.ar/IVE.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Reclaman mayor presupuesto en Santa Fe para garantizar la interrupción voluntaria
  del embarazo
title: Reclaman mayor presupuesto en Santa Fe para garantizar la interrupción voluntaria
  del embarazo
entradilla: Desde la Campaña por el derecho al aborto legal seguro y gratuito regional
  Santa Fe exigen educación sexual integral, accesos anticonceptivos y que se garanticen
  los abortos en la provincia.

---
Cada 28 de septiembre se celebra el Día de la Acción Global por un Aborto Legal y Seguro con diferentes acciones en el país. Desde la regional Santa Fe de la Campaña Nacional por el Derecho al Aborto Legal, Seguro y Gratuito se dio a conocer un comunicado en el que se exige la implementación y cumplimiento efectivo de la Ley 27.610 de Interrupción Voluntaria del Embarazo (IVE).

La regional Santa Fe advierte: "Seguimos en alerta". En esta línea reclaman a los gobiernos locales, provincial y nacional los siguientes puntos: la promulgación en el Senado de la Ley Provincial de Educación Sexual Integral, acceso a todos los métodos anticonceptivos en tiempo y forma, y el cumplimiento efectivo del acceso al derecho de Interrupción Voluntaria del Embarazo en todo el territorio de la provincia para que ninguna persona gestante deba sufrir el desarraigo y la soledad de tener que trasladarse de su localidad para acceder a su derecho.

Asimismo, exigen mayor presupuesto para garantizar el acceso a la IVE/ILE, "campañas de información en cada centro de salud para todes les usuaries. Basta de penalizar con el silencio a quienes deciden acceder al derecho al aborto. Más información, más ciudadanía", que no se atrasen las ecografías, obstaculizando los abortos y los plazos que establece la ley, más capacitaciones a las, los y les profesionales sobre AMEU y que se garanticen las designaciones de equipos de salud interdisciplinarios en todos los efectores.

"No queremos volver a la clandestinidad de los abortos", sostienen desde la regional local. A nivel provincial, hubo un Pañuelazo por la tarde en la plaza San Martín de la ciudad de Rosario.

**Pañuelazo**

El pañuelazo por La Campaña Nacional por el Derecho al Aborto Legal, Seguro y Gratuito se realizó este martes, frente al Congreso de la Nación, que se replicó en diferentes puntos del país, para reclamar "la efectiva implementación de la ley IVE" y la legalización de la interrupción voluntaria del embarazo en todos los países de la región. ¡¡¡Será con el lema “los pañuelos no se bajan!!!", la Campaña convocó hoy a concentrarse desde las 16 horas en la esquina de Callao y Rivadavia, frente al Palacio Legislativo.

"Salimos por la efectiva implementación de la ley de IVE 27.610 en todo el territorio nacional, por educación sexual integral en todas las escuelas, por acceso a métodos anticonceptivos en todo el país", explicó a Télam Yanina Waldhorn, referente de la Campaña en la Ciudad de Buenos Aires. La actividad también incluyó una mesa de difusión, que se desarrolló de 16 a 19 en esa esquina del centro porteño. El "28S" también se conmemoró a lo largo y ancho del país con diferentes acciones organizadas por los colectivos feministas de cada lugar.

Así, en la ciudad de San Luis la convocatoria se programó a las 18 en la Plaza Pringles y a la misma hora las ciudades puntanas de Villa Mercedes y Merlo donde se realizaron sus propias acciones en la Plaza del Mercado y la Plaza del Casino, respectivamente.

En Tierra del Fuego hubo pañuelazos también a las 18 en la Torre de Agua de Río Grande y en el "cartel Ushuaia" de bienvenida a esa ciudad, junto al Canal Beagle, para "exigir el cumplimiento de la ley IVE".

También a esa hora, la ciudad de Córdoba se sumó al "grito global por el aborto legal" con una movilización frente al Museo de Antropología.