---
category: El Campo
date: 2021-08-27T06:00:30-03:00
thumbnail: https://assets.3dnoticias.com.ar/MAIZ.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: La cosecha de maíz cerró con un récord de 60,5 millones de toneladas
title: La cosecha de maíz cerró con un récord de 60,5 millones de toneladas
entradilla: 'Lo señaló el secretario de Agricultura, Jorge Solmi, dando a conocer
  un número con altas diferencias en relación con lo estimado por la Bolsa de Cereales
  de Buenos Aires. '

---
La cosecha argentina de maíz de la campaña 2020/2021 alcanzó un récord de 60,5 millones de toneladas, según las últimas estimaciones agrícolas oficiales conocidas este jueves.

"Las estimaciones que tenemos es que son 60,5 millones de toneladas de maíz, a lo que hay que sumarle 3.290.000 toneladas de sorgo; entonces estamos llegando a casi 64 millones de toneladas en los dos granos, una cifra récord", dijo a Télam en secretario de Agricultura, Jorge Solmi.

Estas cifras superan largamente las estimaciones del sector privado: para la Bolsa de Cereales de Buenos Aires la cosecha de maíz finalizó con una producción total de 50,5 millones de toneladas; mientras que para la Bolsa de Comercio de Rosario rondaría 48 millones de toneladas.

"Tenemos 40 estimadores en territorios que presentan un informe semanal", explicó Solmi, quien aclaró que los datos privados no se condicen con la información sobre superficie sembrada y rindes.

"Si tomamos el Sistema de Información Simplificado Agrícola (SISA) de la AFIP, las hectáreas declaradas fueron 8.470.000; y si dividimos los 50 millones de toneladas que dicen que fue la cosecha por esa superficie da uno de los peores rindes de la historia, algo que no es posible más allá de alguna situación puntual", resaltó el funcionario.

En ese sentido, remarcó Solmi que "hemos tenido récords en la zona de Córdoba, que levantó el rinde de manera excepcional".

Al ser consultado sobre cuál sería la razón de la diferencia entre las distintas estimaciones, afirmó que "en los últimos días estamos viendo noticias extrañas".

"Si tenemos una cosecha récord, es una cosecha récord, queremos que se sepa, porque además la inversión fue alta, se invirtió mucho en fertilizantes, el consumo de fertilizantes es uno de los más altos de la historia".

Tras reiterar que "nos gusta mostrar las cosas que nos salen bien, y la cosecha gruesa nos salió bien", adelantó que "la fina también va a ser excelente".

"Después tendremos que ver (un eventual impacto del fenómeno La Niña), pero el tema climático está discutido, hay voces alarmistas y otras más moderadas", concluyó Solmi.