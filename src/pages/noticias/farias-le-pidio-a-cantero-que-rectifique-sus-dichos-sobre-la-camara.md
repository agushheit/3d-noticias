---
category: Agenda Ciudadana
date: 2021-05-24T06:15:07-03:00
thumbnail: https://assets.3dnoticias.com.ar/FARIAS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Farías le pidió a Cantero que rectifique sus dichos sobre la Cámara
title: Farías le pidió a Cantero que rectifique sus dichos sobre la Cámara
entradilla: El presidente de Diputados envió este domingo una nota a la ministra de
  Educación explicitando lo actuado hasta el momento. Calificó de “improcedentes”
  algunos dichos de la funcionaria provincial.

---
Este domingo, el presidente de la Cámara de Diputados remitió a la secretaria privada del Ministerio de Educación una nota oficial pidiendo que la titular de la cartera, Adriana Cantero, “rectifique la aseveración improcedente respecto al trabajo de nuestra Cámara, al tiempo que nos reiteramos a disposición para el avance de una correcta relación institucional”.

La ministra Cantero había señalado el viernes que apenas el 10% de los alumnos tienen conectividad en la provincia y atribuyó parte del atraso al tratamiento de dos mensajes del Poder Ejecutivo que tiene en tratamiento la Cámara de Diputados, uno de los cuales es la toma de endeudamiento externo por mil millones de dólares.

“En las últimas horas se conocieron declaraciones periodísticas hechas por usted en relación al tema conectividad, las cuales transcribo en la mención que nos alude como cuerpo legislativo: “...exhorto a que la Cámara de Diputados apruebe la ley en la materia que ya tiene media sanción del Senado provincial y desde hace un año no se debate.”, comienza señalando la nota firmada por el socialista Farías. Enseguida le advierte que “su aseveración es inexacta y poco aporta a la solución de un tema tan sensible como la educación de las infancias y las juventudes en pandemia, así como la tranquilidad que debe llevarse desde el Gobierno a las familias y el respeto al trabajo denodado de la comunidad docente”.

En la carta a Cantero, el presidente de la Cámara pasa a ser un raconto de la situación desde el 1 de mayo del 2020 cuando en su discurso ante la Asamblea Legislativa el gobernador Omar Perotti aludió al tema conectividad, “pero los mensajes legislativos relacionados tuvieron ingreso formal en esta Cámara el 5/11/2020 (Mensaje 4893 ENERFE ingresó en sesión) y el 19/11/2020 (Mensaje 4903 endeudamiento ingresó en sesión), es decir más de 6 meses después de aquel anuncio”. Señala que “de manera inmediata desde las comisiones de trabajo se tomó contacto con el ex ministro Rubén Michlig (quien manifestaba que su cartera estaba al frente del proyecto) y se citó a los funcionarios involucrados, quienes en tres oportunidades confirmaron su asistencia, y en tres oportunidades la cancelaron”.

Más adelante señala que ante la cancelación de las vistas, el 15 de diciembre último, se envió formalmente al ex ministro una nota con preguntas pormenorizadas de los mensajes, “que recién tuvo respuesta parcial en marzo de 2021, es decir tres meses después. En función de nuevas dudas que esa respuesta generaba y a fin de retomar el diálogo expresamente e interrumpido en las tres invitaciones canceladas, reiteramos la invitación a los funcionarios para el día 14 de abril. No escapa a su conocimiento que miembros de esta Cámara padecieron en esa fecha Covid-19, por lo que solicitamos posponer dicha convocatoria, en diálogo personal con los funcionarios y con el ministro (Roberto) Sukerman”.

No deja de mencionar que la muerte del entonces presidente del cuerpo, Miguel Lifschitz, “además del dolor humano, provocó a esta Cámara una situación institucional sin precedentes. No obstante, ello, se reprogramará en breve la reunión que no se pudo realizar el 14 deabril”.

Más allá de ese derrotero, la nota de Farías subraya algunos puntos de la respuesta oficial de marzo, suscripta únicamente por el Secretario de Tecnologías para la Gestión (Sergio Bleynat).

“El Mensaje 4903 de autorización de endeudamiento (de hecho, la firma del Gobernador está refrendada exclusivamente por el Ministro de Economía) adjuntaba solamente un perfil de proyecto genérico, a título informativo y no vinculante. Al requerir se explicitarán los proyectos definitivos de este endeudamiento, la respuesta recibida en marzo fue que “se encuentran en elaboración”. Esta aseveración no daba cuenta de ninguna urgencia en el tratamiento, ya que ni siquiera los proyectos estaban listos. - La respuesta detalla asimismo (aunque con algún error cronológico) todo el avance que la Provincia desarrolló en materia de conectividad para proyectos públicos, citando a los ex gobernadores Obeid, Reutemann, Binner, Bonfatti y Lifschitz . Ninguno de ellos, para todo ese avance, requirió de ninguna ley ni de ningún endeudamiento. Se trató, claramente, de temas de gestión que todos los ex gobernadores abordaron con responsabilidad”.

En la parte final, Farías le solicita a Cantero que “rectifique la aseveración improcedente respecto al trabajo de nuestra Cámara, al tiempo que nos reiteramos a disposición para el avance de una correcta relación institucional”.