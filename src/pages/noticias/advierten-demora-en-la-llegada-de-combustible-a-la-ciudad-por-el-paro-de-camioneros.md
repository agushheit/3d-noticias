---
category: Agenda Ciudadana
date: 2022-06-24T09:00:16-03:00
thumbnail: https://assets.3dnoticias.com.ar/nafta.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Fuente; lt 10
resumen: Advierten demora en la llegada de combustible a la ciudad por el paro de
  camioneros
title: Advierten demora en la llegada de combustible a la ciudad por el paro de camioneros
entradilla: Las más afectadas son las estaciones de servicio Axion, quienes registran
  demoras en la llegada de los pedidos de combustibles de hasta 72 horas.

---
Un paro de transportistas se lleva a cabo en distintas rutas del país. En la provincia, persiste el de la Autopista Rosario- Buenos Aires, a la altura de Villa Constitución. La movilización comenzó en la noche del martes pasado y los manifestantes aseguran que no tienen diálogo con el gobierno, a quien le exigen respuestas por la falta y el precio del gasoil.

Paradójicamente, el corte impide la entrada y salida de camiones con combustibles desde Puerto San Martín, y algunas estaciones de servicios de la ciudad de Santa Fe comienzan a sentir las demoras y falta de los distintos productos.

"El problema es que esta cortada la planta por el paro de transporte y el despacho de Axion está a dos o tres kilómetros de la petrolera y ahí está todo el tráfico de camiones que va a descargar de Puerto San Martín" contó Mario Toye, dueño de una estación de servicio Axion en Facundo Zuviría.

Además, detalló que "están dejando pasar muy pocos camiones pero con el consumo que hay de combustible, de 200 que pasan por la planta por día, están pasando 10 o 15", lo que se traduce en "una demora de 72 horas en la entrega de combustible".

En su caso en particular, "anoche llegó una carga que tendría que haber llegado el martes a la mañana", y que generó que durante unas siete horas se quedara sin algunos tipos de combustibles.

Por su parte, Alberto Booz de la Cámara de Expendedores, explicó que se generan cuellos de botellas y demoras en el abastecimiento, y lo que se da es una complicación de la logística.