---
category: Agenda Ciudadana
date: 2020-12-22T11:23:21Z
thumbnail: https://assets.3dnoticias.com.ar/2212perotti.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: El Litoral'
resumen: El Gobernador Perotti habló sobre las fiestas y reuniones sociales en época
  de coronavirus
title: Durante un acto en Rafaela, el Gobernador Omar Perotti habló sobre las fiestas
  y las reuniones sociales en época de coronavirus
entradilla: "«La ministra de Salud (Sonia Martorano), tendrá reuniones con el grupo
  de expertos que nos viene acompañando y allí se definirá la cantidad de personas
  que se podrán reunir el 24 y 31»."

---
«Hay que tener en claro que, aunque seamos 10 o 15 en el encuentro familiar, cada uno llevará a esa mesa los contactos que tuvimos estos días previos», Omar Perotti, Gobernador de Santa Fe.

Además, expresó que Santa Fe está preparada para empezar a vacunar «en diciembre, en enero o en febrero». Y agregó: «estamos construyendo en esta etapa, cuidándonos, el puente hacia la vacuna».

***

[![Abrir hilo](https://assets.3dnoticias.com.ar/2212tweet-P.webp)](https://twitter.com/omarperotti/status/1341053863051341824?s=20 "Hilo")

***