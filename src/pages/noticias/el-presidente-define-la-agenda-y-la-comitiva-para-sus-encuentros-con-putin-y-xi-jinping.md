---
category: Agenda Ciudadana
date: 2022-01-24T06:00:49-03:00
thumbnail: https://assets.3dnoticias.com.ar/ELBERTO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: El Presidente define la agenda y la comitiva para sus encuentros con Putin
  y Xi Jinping
title: El Presidente define la agenda y la comitiva para sus encuentros con Putin
  y Xi Jinping
entradilla: " En Moscú hablarán, entre otras cosas, de la colaboración en el tema
  de vacunas Sputnik V, mientras que en Beijing firmarán un memorándum de entendimiento
  con la empresa estatal china State Power.\n\n"

---
El presidente Alberto Fernández definirá en los próximos días la agenda de trabajo y la comitiva que lo acompañará en las reuniones que mantendrá con su par de la Federación Rusa, Vladimir Putin y con el mandatario de la República Popular China, Xi Jinping, durante la visita que realizará a esas naciones entre el 3 y el 4 de febrero.  
  
Fernández se reunirá el 3 de febrero con Putin en Moscú y al día siguiente se encontrará con Xi Jinping, a poco de cumplirse los 50 años de relaciones diplomáticas con el 'Gigante asiático', iniciadas el 19 de febrero de 1972, y para asistir además en Beijing a la inauguración de los Juegos Olímpicos de Invierno.  
  
El canciller Santiago Cafiero analizará aspectos de la agenda de Rusia y China tras haber regresado el jueves desde Washington luego de haberse reunido con el secretario de Estado de los EEUU, Antony Blinken, por la renegociación de la deuda con el Fondo Monetario Internacional (FMI), según consignó a Télam un funcionario cercano al ministro de Relaciones Exteriores, Comercio Internacional y Culto.  
  
En la comitiva se incluirá también al ministro de Economía, Martín Guzmán, y deben definirse otros asientos porque "falta conocer detalles de otras reuniones", apuntó una fuente con despacho en Casa Rosada.  
  
"Será una comitiva reducida, por las restricciones sanitarias, y por eso no irán esta vez periodistas", aclararon desde Balcarce 50, y confirmaron también que la portavoz Presidencial Gabriela Cerruti viajará junto a la comitiva.  
  
Precisamente Fernández y Cerruti se reunieron el jueves en Olivos para "organizar la comunicación de las importantes actividades de trabajo que llevaremos adelante", según contó en la red social twitter el propio mandatario.  
  
Las conversaciones entre Fernández y Putin girarán en torno a la colaboración en el tema de vacunas Sputnik V, inversiones, ciencia y en otros temas de interés común.  
  
Se tratará de una reunión bilateral en Moscú que había quedado pendiente entre ambos jefes de Estado.  
  
El embajador argentino en Rusia, Eduardo Zuain, afirmó el jueves pasado que existe "una predisposición positiva" de ese país hacia "todo planteo argentino" en las negociaciones con el FMI.  
  
"Los encuentros en estos tiempos de pandemia son excepcionales, por lo que se pone de manifiesto la importancia que le da Rusia a la relación con Argentina. Descuento que vamos a tener el apoyo de ellos dentro del FMI", aseguró Zuain a la radio online Futurock.  
  
Y auguró que si el mandatario argentino "lo plantea, Rusia va a examinar las posibilidades de alguna ayuda financiera porque es un país sólido en materia económica y tiene todas las posibilidades. Hay que examinar los proyectos concretos para que esa predisposición pueda materializarse".  
  
El pasado 7 de diciembre, en un almuerzo de trabajo en el salón Eva Perón de Casa de Gobierno, el jefe de Estado se reunió con autoridades del Fondo Ruso de Inversión Directa, ejecutivos de empresas y representantes de bancos de ese país para avanzar en asociaciones en diversos sectores estratégicos y potenciar el intercambio.  
  
**Sobre la visita a China**

  
En Beijing, Fernández y Xi Jinping firmarán un memorándum de entendimiento con la empresa estatal china State Power Investment Corporation Limited (SPIC), uno de los cinco grupos energéticos de ese país y la generadora de energía solar más grande del mundo.  
  
Ese convenio prevé la cooperación estratégica con la firma argentina Invap, tal como lo adelantó el 12 de enero pasado el embajador argentino en China, Sabino Vaca Narvaja.  
  
La vinculación con Invap se sumará a otros trabajos en conjunto con empresas argentinas del sector energético, como Nuclearis, radicada en Villa Martelli, dedicada a la fabricación de componentes mecánicos para la industria nuclear y que "se encuentra encarando proyectos en China", según detallaron desde la embajada en Beijing.  
  
También existen otros proyectos que incluyen a firmas como Conuar (Combustibles Nucleares Argentinos) y la mendocina Industrias Metalúrgicas Pescarmona (Impsa), que ofrece soluciones integrales y fabrica equipos para los mismos fines.  
  
Conuar e Impsa "están en conversaciones para el mantenimiento en suelo chino de centrales de tipo Candu (por Canadian Uranium Deuterium, que utilizan uranio natural -no enriquecido- y se refrigeran con agua pesada), una tecnología en la que la Argentina se ha especializado", precisaron desde la representación diplomática destaca en China.  
  
El objetivo al que se apunta con estas asociaciones en materia nuclear es que el país "pueda seguir diversificando sus oportunidades en el gigante asiático, más allá de la exportación de productos primarios".

![Se tratar de una reunin bilateral en Mosc que haba quedado pendiente entre los jefes de Estado](https://www.telam.com.ar/thumbs/bluesteel/advf/imagenes/2019/06/5cf79bc04dc11_900.jpg =100%x100%)Se tratará de una reunión bilateral en Moscú que había quedado pendiente entre los jefes de Estado..

  
El objetivo es tener "una agenda de cooperación que tenga como eje central la transferencia de tecnología", explicaron desde la Embajada argentina en Beijing.  
  
Vaca Narvaja había anticipado también a la prensa china "la futura adhesión a la iniciativa de la Franja y la Ruta" de la Ceda, un proyecto del país asiático para unir esa nación con Europa a través de vías navegables y ferroviarias, y en el cual Argentina buscará insertarse como país exportador de distintos productos.  
  
Además, el embajador fue recibido el jueves pasado por el vicepresidente de la China National Aero-Technology Import & Export Corporation (Catic), Wang Yaoxin, como parte de "una estrategia integral" del Gobierno argentino de cooperación en materia de defensa con la nación asiática y por la posible adquisición de aviones JF-17 de tercera generación desarrollados en conjunto por esa firma y la Fuerza Aérea de Pakistán (PAF).  
  
Además, en la Exhibición Aeroespacial de Zhuhai, en septiembre pasado, Vaca Narvaja se reunió con representantes de la empresa estatal china North Industries Group Corporation Limited (Norinco), para avanzar en un proyecto que contempla no solo la adquisición de vehículos blindados 8x8, muy utilizados en las misiones de paz, sino también la instalación en Argentina de una planta de fabricación de esas unidades.  
  
El viaje de Fernández a China se acordó en la última gira presidencial por Europa, realizada entre el 30 de octubre y 2 de noviembre último, "para acentuar el comercio", según había adelantado aquella vez Cafiero a los periodistas argentinos que cubrieron la Cumbre de Líderes del G20 en Roma y la Conferencia de las Naciones Unidas sobre el Cambio Climático (COP) 26 en Glasgow  
  
"Tenemos una comisión de tratamiento binacional respecto a temas comerciales y uno de los puntos fue que vuelva a trabajar rápidamente", había señalado el canciller, que apuntó también a la idea es avanzar en temas como la economía del conocimiento, en términos comerciales, y en robustecer la presencia de la Argentina en China.  
  
Desde la embajada argentina en China puntualizaron además que la tecnología nuclear del país representa hoy "el 7,5% de la matriz energética", un porcentaje que la gestión de Fernández "tiene intenciones de aumentar en base a la cooperación" con la nación asiática, según plantearon.  
  
Además, Fernández asistirá el 4 de febrero a la inauguración de los XXIV Juegos Olímpicos de Invierno, que se extenderán hasta el 20 del mes próximo en la capital Beijing y en Zhangjiakou.