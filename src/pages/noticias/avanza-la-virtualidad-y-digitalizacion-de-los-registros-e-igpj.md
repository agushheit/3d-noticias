---
category: Estado Real
date: 2020-12-31T10:54:46Z
thumbnail: https://assets.3dnoticias.com.ar/digitalización.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Avanza la virtualidad y digitalización de los registros e IGPJ
title: Avanza la virtualidad y digitalización de los registros e IGPJ
entradilla: Existen 15 millones de documentos que necesitan ser digitalizados para
  brindar un servicio rápido, ágil y actual a los ciudadanos de la Provincia de Santa
  Fe.

---
El subsecretario de Registros, Francisco Dallo, anunció el **Plan Macro de Digitalización y Virtualidad** para el Registro General, Registro Civil y la Inspección General De Personas Jurídicas.

El funcionario detalló que «el proceso de digitalización implica escanear, indexar e ingresarlo a un sistema de la Provincia para luego brindarle este servicio digital a la ciudadanía».

Dallo explicó que «en el caso de Registro Civil implementó el sistema de Partidas digitales, que habilita a los ciudadanos a efectuar la solicitud, pago y recepción de partidas por vía digital».

Este proceso de digitalización permitirá en dos o tres años, tener toda la base datos y documentos, Dallo recordó que «hasta el día de hoy solo se encuentran los nacimientos a partir de 1970, restan digitalizar 13 millones de documentos solo del Registro Civil, en una base datos, entre ellos matrimonios, uniones convivenciales y defunciones».

«En el caso del Registro General, en el primer semestre del año próximo, se implementará la Mesa de Entrada Única Virtual que consiste en el ingreso de todo tipo de documentos y certificados a través del sistema y se unificara el funcionamiento de los Registros de la Propiedad de Rosario y Santa Fe».

«Asimismo, se pondrá en funcionamiento un Programa de Desarrollo del Folio Real Electrónico, ya que la Provincia es una de las pocas que no cuenta con este sistema, en la actualidad hay un sistema obsoleto basado en lenguaje informático que ha quedado atrasado, ya que hace más de diez años se debería haber incorporado nuevos sistemas, más ágiles y modernos, que hoy nos permitirían tener comunicación hacia el exterior», declaró el funcionario.

«Finalmente, en el caso de la Inspección General de Personas Jurídicas, el proceso abarca la información de más de 18 mil sociedades por acciones, más de 14 mil asociaciones civiles y fundaciones, la digitalización va a permitir concentrar en un mismo sistema toda esta información, para que tanto la ciudadanía como las mismas personas jurídicas y entes estatales puedan acceder de manera rápida y precisa».