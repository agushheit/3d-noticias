---
category: La Ciudad
date: 2021-09-30T06:00:08-03:00
thumbnail: https://assets.3dnoticias.com.ar/MERCADO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Vecinos de barrio Candioti piden reubicar los recitales del Mercado Progreso
title: Vecinos de barrio Candioti piden reubicar los recitales del Mercado Progreso
entradilla: Sostienen que el lugar no está acondicionado para aislar el sonido. El
  municipio realizó la medición de decibeles y no superaron lo establecido en la norma.

---
En las últimas semanas, con la vuelta a la organización de eventos culturales en el Mercado Progreso -lugar que gestiona la Municipalidad de Santa Fe- los vecinos de barrio Candioti empezaron a realizar reclamos frente a la realización de recitales.

 Cristina De La Torre, vecina del lugar, se contactó con El Litoral para comentar la problemática que viven quienes tienen sus hogares cercano a este espacio cultural que está sobre calle Balcarce (frente a la plaza Pueyrredón). "Hace unas semanas que se hacen recitales y lo venimos padeciendo. A la noche esto es tierra de nadie y los fines de semana se volvieron una ´tortura\` para nosotros", lamentó la vecina. "El Mercado Progreso es al aire libre y no tiene aislantes de sonidos", remarcó.

 Otro vecino también se puso en contacto con este medio y cuestionó que "comienzan con las pruebas de sonido a las 15 horas. A todo esto se suma la música y la invasión del espacio público de bares que están en la zona; y por último, quedan los borrachos residuales ´regando\` (orinando) los frentes de las casas".

 Los vecinos piden que los recitales se puedan reubicar a otros espacios culturales o en boliches, una vez que estén autorizados a funcionar a partir de octubre. "Porqué no hacer los recitales en los boliches de la Ruta 168", opinó De la Torre y agregó: "Nosotros apoyamos la cultura pero no que se hagan recitales este lugar. Pueden utilizar el Mercado Progreso para ferias de emprendedores, gastronómicas u otros eventos, pero no para traer una banda de rock".

 Frente a esta situación que aqueja a los vecinos, este miércoles por la tarde serán recibidos por funcionarios municipales para intentar llegar a una solución. Con este sería el tercer encuentro, ya que durante la semana pasada tuvieron lugar dos reuniones.

 **Decibles**

 Paulo Ricci, secretario de Educación y Cultura del municipio, dialogó con este medio e hizo mención a esta situación que se vive entorno a los eventos culturales que organiza el gobierno local. "Lo primero que hicimos fue llamar a una reunión con los vecinos", señaló Ricci y destacó que ya tomaron medidas para mejorar las condiciones para que el sonido que se genera en los recitales no se filtre: "Se taparon con una madera y un fenólico una serie de ventiluces, tanto en el este como en el oeste del Mercado, para que eso evite que el ruido fluya por ahí". Ricci también mencionó que se pusieron en contacto con los sonidistas para trabajar en un ajuste en el sistema de sonido utilizado, con otro tipo de tecnología acústica.

 "El jueves pasado convocamos a la Secretaría de Control para hacer una medición con decibelímetros y medimos tanto en el patio del Mercado como desde el domicilio de algunos de los vecinos. Se hizo durante la prueba de sonido de una banda y en ninguno de los casos dio por fuera de los decibeles autorizados, que son 50 decibeles durante el día y 40, a partir de las 20 horas", sostuvo el funcionario.

 En este sentido, el secretario de Cultura informó que se comprometieron con los vecinos a llevar esta medición de manera permanente. "Pedimos a la Secretaría de Control un dispositivo para medir en cada fecha de recital, porque eso puede variar según el género musical que se programe", contó Ricci.

 El funcionario también se refirió al otro reclamo de los vecinos, que plantean que quienes asisten a los recitales orinan en las veredas y frentes de las casas. "Cuando llegamos a la gestión, en el Mercado había un solo baño químico para los dos sexos. Nosotros construimos un baño de material con rampa para discapacitados y para que lo usen cuatro personas en simultáneo. La gente que entra al Mercado es muy difícil que salga a orinar al barrio, por lo que no creemos que sea gente que asiste a espectáculos la que hagan sus necesidades en el entorno", concluyó.

 