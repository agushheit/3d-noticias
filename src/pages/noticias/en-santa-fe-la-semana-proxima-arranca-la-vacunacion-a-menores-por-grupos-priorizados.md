---
category: La Ciudad
date: 2021-07-28T06:15:35-03:00
thumbnail: https://assets.3dnoticias.com.ar/COFESA.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: En Santa Fe, la semana próxima arranca la vacunación a menores por grupos
  priorizados
title: En Santa Fe, la semana próxima arranca la vacunación a menores por grupos priorizados
entradilla: 'En la provincia, las “situaciones de prioridad” implicarán a chicos y
  adolescentes con comorbilidades tales como diabetes 1 y 2, obesidad, patologías
  coronarias y respiratorias y pacientes a la espera de trasplantes. '

---
Los titulares de las carteras sanitarias de todas las provincias (junto a la ministra nacional Carla Vizzotti) empezaron a unificar criterios respecto de las condiciones de priorización para la inscripción de niños, niñas y adolescentes entre 12 y 17 años en quienes se comenzará a aplicar la vacuna Moderna contra el Covid-19.

“La próxima semana seguramente ya podríamos empezar (la inoculación contra el Covid-19) en este grupo”, adelantó la Ministra de Salud santafesina, Sonia Martorano, en declaraciones públicas. Agregó que este miércoles vuelve a abrirse el turnero virtual para quienes pertenecen a esta franja etaria puedan anotarse, siempre que puedan certificar la existencia de comorbilidades.

En tanto, funcionarios de la cartera sanitaria santafesina mantendrán -también este miércoles-, un encuentro virtual con las sociedades científicas para poder definir con precisión este listado de “situaciones priorizadas”. Estas son distintas patologías y enfermedades crónicas que pueden padecer quienes tengan entre 12 y 17 años (y que, por ello, podrán acceder primero a la primera dosis de Moderna), sobre las cuales, según informaciones nacionales, ya hay un listado de base de una veintena de comorbilidades.

**Listado nacional**

La vacunación dentro este grupo etario incluirá en una primera etapa una veintena de patologías (que justamente indican las situaciones de prioridad), tales como diabetes tipo 1 o 2; obesidad grado 2 (IMC 35) y grado 3 (IMC 40); enfermedad cardiovascular crónica (insuficiencia cardíaca, enfermedad coronaria, valvulopatías, miocardiopatías, hipertensión pulmonar); cardiopatías congénitas; enfermedad renal crónica (incluidos pacientes en diálisis crónica y trasplantes).

También se incluyó en el listado el síndrome nefrótico; enfermedades respiratorias crónicas como una patología pulmonar obstructiva crónica (EPOC), fibrosis quística, enfermedad intersticial pulmonar, asma grave, y que requieran de oxígeno terapia; enfermedad grave de la vía aérea; hospitalizaciones por asma; y enfermedad hepática (cirrosis y hepatitis autoinmune).

Dentro del grupo de menores de 12 a 17 años que podrán recibir la vacuna Moderna se incluyen a aquellos que viven con VIH independientemente del CD4 y CV; pacientes en lista de espera para trasplante de órganos sólidos y trasplante de células hematopoyéticas; pacientes oncológicos y oncohematológicos con diagnóstico reciente o “activa”; pacientes con tuberculosis activa; chicos con discapacidad intelectual y del desarrollo; con Síndrome de Down; con enfermedades autoinmunes y/o tratamientos inmunosupresores, inmunomoduladores o biológicos.

Finalmente, se añaden como grupos prioritarios a adolescentes que viven en lugares de larga estancia; gestantes de 12 a 17 años con indicación individual; personas con Carnet Único de Discapacidad (CUD) vigente; con pensión de ANSES por invalidez, aunque no tengan CUD, y con pensión de ANSES por trasplantes, aunque no tengan CUD.

**22 por ciento**

El subsecretario de Estrategias Sanitarias, Juan Manuel Castelli, estimó que el 22% de este grupo etario cumpliría con estos criterios, lo cual significaría 924.000 adolescentes y 1.848.000 dosis de vacuna de Moderna. El intervalo de aplicación de las dos dosis de Moderna es de 28 días. Y la directora nacional de Epidemiología y Análisis de Información Estratégica, Analía Rearte, agregó que casi el 85% de las 281 personas menores de 20 años que fallecieron en el país por Covid-19 tenían comorbilidades.