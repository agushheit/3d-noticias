---
category: La Ciudad
date: 2021-07-17T06:15:43-03:00
thumbnail: https://assets.3dnoticias.com.ar/COBEM40.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Jatón: “Gracias Cobem por lo que hicieron por la ciudad durante estos 40
  años”'
title: 'Jatón: “Gracias Cobem por lo que hicieron por la ciudad durante estos 40 años”'
entradilla: 'El intendente encabezó este viernes los actos oficiales por el cuadragésimo
  aniversario de la Central Operativa Brigada de Emergencia Municipal. '

---
La mejor manera de reconocer un trabajo bien hecho es brindando los equipamientos que se necesitan para llevar adelante la tarea diaria. Esa fue la síntesis del homenaje que recibieron esta mañana los trabajadores de la Central Operativa Brigada de Emergencia Municipal (Cobem), en el marco de su 40° aniversario. El intendente Emilio Jatón encabezó el acto que contó con la presencia del personal del Cobem.

Sobre la calle de la explanada de la Municipalidad estaba estacionada una Unidad de Rescate cero kilómetro y equipamiento de última generación que le entregaron a los trabajadores. Cabe consignar, que la última vez que el Cobem tuvo una movilidad nueva fue hace más de una década y gracias a una donación.

Durante la ceremonia, el intendente destacó que a través de los 40 años de historia el Cobem se ganó un espacio y un respeto en la sociedad, que muchas veces no estuvo acompañado desde la política. En tal sentido, el mandatario añadió que la entrega de la nueva movilidad y de las herramientas de rescate “no es un mérito del intendente ni de mi equipo de trabajo, esa unidad de rescate es una necesidad, ya no podíamos estar con móviles de 11 años de antigüedad. Cómo podemos cuidar a los santafesinos con esos móviles, solo fue posible gracias al sacrificio humano y al trabajo del Cobem”.

**Reconocer la labor**

“Esto es un trabajo en equipo que hacemos con ustedes, porque el dinero para comprar la nueva unidad de rescate nace del propio municipio, de otro móvil que estaba guardado en un garaje y que era inútil para esta nueva gestión, lo transformamos: de un auto parado a esta Unidad de Rescate que estaban esperando desde el 2010. Es algo que ustedes estaban pidiendo desde hace años y lo merecían”, indicó el intendente dirigiéndose a los trabajadores del Cobem.

“Gobernamos pensando en las necesidades de la gente, estamos construyendo una ciudad diferente, desde el cuidado, desde los recursos humanos, de mirar al otro a los ojos. Esa nueva ciudad se puede hacer entre todos, nadie se salva solo”, destacó Jatón y añadió: “Gracias Cobem, gracias por los 40 años, gracias por lo que hicieron por la ciudad de Santa Fe, esta nueva unidad es de ustedes, es de Santa Fe”.

**Trabajo en equipo**

A su turno, Marcos Quintana, actual director del Cobem, se refirió a la importancia de cada uno de los trabajadores y las trabajadoras que pasaron por la Brigada a lo largo de estas cuatro décadas. “Estamos contentos por el sacrificio del intendente Emilio Jatón, y su equipo, las funcionarias Silvina Serra y Cintia Gauna, estoy agradecido por estos 40 años, no es fácil estar en la dirección del Cobem, pero es posible gracias al apoyo de mis compañeros actuales, de los que se jubilaron y de los que ya no están, como Ramón Gamarra”.

**Distinciones**

Durante el acto se entregaron diplomas por capacitaciones realizadas a los trabajadores de las diferentes áreas del Cobem. Asimismo, se concedieron reconocimientos a Ricardo Vergara, fundador de la Brigada de Emergencias, y al ex director, Carlos Fiorini.

En tanto, las concejalas Laura Mondino y Mercedes Benedetti y el presidente del Honorable Concejo Municipal, Leandro González, le entregaron a Marcos Quintana un diploma con la declaración de interés por la tarea desarrollada.

Asimismo, el Rotary Club entregó elementos de protección y una placa recordatoria. También estuvieron en el acto, representantes de la Cruz Roja Argentina, el Ejército de Salvación, los buzos tácticos y ex Combatientes de Malvinas, entre otros.

**Unidad de rescate**

Los fondos para la compra de la nueva unidad salieron de la subasta de una camioneta que era utilizada por el anterior intendente. La camioneta Volkswagen Tiguan se vendió a través de una subasta pública del Colegio de Martilleros y Corredores Públicos de Santa Fe por un monto de $ 1,6 millones en diciembre de 2020.

En tanto, durante febrero y marzo de este año, se hizo una licitación para la compra de una unidad 0 km tipo furgón para el Cobem. Tras dos llamados desiertos, se procedió a hacer una compra directa de acuerdo a la normativa vigente y con todos los efectos legales de una licitación. Se publicó y se invitó a participar nuevamente a todos los proveedores. Hubo dos ofertas y se adjudicó la compra a la más económica y que cumplía con los requisitos técnicos, por un importe de $ 4,1 millones.

Se compró un furgón mediano marca Toyota Hiace Furgón L2H2 2.8 TDI 6 acondicionado para que sea utilizado por el Cobem como unidad de rescate.