---
category: La Ciudad
date: 2021-07-28T06:00:31-03:00
thumbnail: https://assets.3dnoticias.com.ar/CESTOS.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Municipalidad coloca cestos comunitarios en distintos puntos de la ciudad
title: La Municipalidad coloca cestos comunitarios en distintos puntos de la ciudad
entradilla: 'La iniciativa impulsada por la Municipalidad beneficiará a 127 familias.
  Se realiza en seis barrios de la capital provincial y el objetivo es erradicar microbasurales. '

---
La Municipalidad está instalando cestos comunitarios en diferentes barrios de la ciudad con el objetivo de erradicar microbasurales. En consonancia, se realiza un trabajo coordinado con instituciones barriales para fomentar la importancia de mantener los barrios libres de basura. En una primera etapa serán 127 las familias que se verán beneficiadas con la colocación de 30 cestos comunitarios.

Los mismos se vienen instalando en seis barrios de la capital provincial y serán clave para erradicar microbasurales. Paralelamente, en esos sectores, se realizan diferentes acciones, como promoción ambiental o paisajismo, con el objetivo de mantener los espacios verdes y las calles limpias.

Previamente, el municipio y los vecinos realizaron un relevamiento con respecto a qué zonas quedaban fuera del servicio de recolección y planificaron diversas estrategias para dar respuestas a estos problemas. Cabe recordar que hay sectores de la ciudad donde no pueden ingresar los camiones recolectores, por lo cual la gestión integral de residuos sólidos urbanos debe adaptarse a las necesidades de cada barrio.

**Trabajo conjunto**

El subdirector de Gestión Integral de Residuos Sólidos Urbanos (Girsu) de la Secretaría de Ambiente, Germán Eichmann, detalló cuál es el objetivo de este programa que lleva a cabo el municipio: “La intención es adaptar los distintos barrios al sistema de recolección de residuos, sobre todo esos lugares donde por cables bajos o problemas de infraestructura -como pasillos angostos- no ingresa el camión. Entonces buscamos otras alternativas para poder llegar a todos los vecinos con la recolección y evitar la formación de microbasurales”.

En esta línea, contó que una de las propuestas es la colocación de cestos y la intervención o creación de un espacio verde con la cartelería correspondiente para evitar que los ciudadanos tiren basura. “Otras de las modalidades son las campañas de difusión a través de los promotores ambientales en conjunto con los coordinadores de los distritos e instituciones, con el fin de ir mejorando la higiene urbana en cada barrio y recuperar espacios que el vecinos puede disfrutar”, detalló Eichmann.

Por último, destacó la participación de los vecinos en esta acción: “Esto surge por pedido de los propios vecinos a través del 0800 y de las necesidades del sistema formal. Entonces se hace un trabajo en conjunto con los coordinadores, las instituciones y los vecinos y se busca la mejor alternativa; después tenemos el compromiso de que ellos cuiden ese sector recuperado”.

**Avance**

Ya se colocaron varios cestos comunitarios en el barrio San Lorenzo, precisamente San José y Entre Ríos y en la intersección de calle Entre Ríos y Pasaje Simoniello. En ese sector, son treinta y seis las familias beneficiadas y cuatro los microbasurales que se erradicaron. Participaron de las intervenciones integrantes de la red barrial y vecinos en general. Asimismo, se realiza promoción ambiental y difusión con folletería sobre los días y horarios de los servicios de recolección de residuos.

También se colocaron cestos comunitarios en la intersección de La Pampa y Hermanos Figueroa, en barrio Santa Marta. En este caso son 24 las familias beneficiadas. Participaron activamente vecinos, personal de la secretaría de Ambiente y la Microempresa Social de Higiene Urbana. En ese espacio, se realizarán intervenciones paisajísticas a cargo de la Subdirección de Recursos Naturales.

Otro sector donde se sumó un cesto comunitario es el Distrito de La Costa, en Los Ingá al 5100 (Colastiné) que beneficia a cuatro familias.

En tanto, en la intersección de Pedro Ferré y Dorrego, se colocaron cestos comunitarios que beneficiarán a 15 familias del asentamiento La Carbonilla. En ese sector, el objetivo es erradicar un microbasural. Por tal motivo, se realizó una intervención paisajística.

El cronograma de colocación de cestos continuó el viernes pasado en el sector de Playa Norte. Esa zona se equipó con dos cestos comunitarios para 22 familias. Además, se realizó una intervención paisajística a cargo de la subdirección de Recursos Naturales, en la esquina de French y Riobamba, donde se erradicó un microbasural. En tanto, el viernes 30 de julio, en la calle previa al terraplén a la altura de la “Bajada de los boteros” de Alto Verde, se realizará una campaña de promoción ambiental y se colocarán tres cestos comunitarios, para 26 familias.

Vale destacar que a los ya colocados está previsto instalar más cestos, dado que continuará el relevamiento que se hace con los promotores ambientales y la secretaría de Ambiente, pero también por el aviso de los vecinos a través del 0800 o cuando se acerca alguna institución referente en el barrio.