---
category: La Ciudad
date: 2021-01-22T03:47:50Z
thumbnail: https://assets.3dnoticias.com.ar/jardines_municipales.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad Santa Fe
resumen: Los jardines municipales comienzan la preinscripción
title: Los jardines municipales comienzan la preinscripción
entradilla: Las salas recibirán a niñas y niños con un sistema escalonado para sostener
  la matrícula del año pasado. En febrero se abre el registro de nuevos aspirantes
  para todas las salas.

---
Como está previsto de acuerdo al calendario académico aprobado, el 17 de febrero comenzará el nuevo ciclo lectivo en los jardines municipales. En el marco de las restricciones por la pandemia y para privilegiar el cuidado de las niñas y niños y de sus familias, las salas implementarán un sistema de burbujas. Para los nuevos aspirantes el período de preinscripción comenzará el 8 de febrero.

“A partir de la convicción de que la educación es un valor fundamental para nuestra sociedad y nuestra cultura, durante todo el año pasado estuvimos trabajando en la elaboración de los protocolos de funcionamiento y las nuevas modalidades para las clases en los jardines municipales”, explicó el secretario de Educación y Cultura de la Municipalidad, Paulo Ricci.

Y detalló: “La mecánica de funcionamiento va a ser a través del escalonamiento y la rotación de la presencialidad. No va a ser una presencialidad plena para poder garantizar el lugar a la misma cantidad de niñas y niños que estaban en el sistema de educación inicial municipal. Esto es lo que hoy podemos aplicar en el marco de una pandemia que no ha cesado”. Además remarcó que se abrirán todas las salas desde los 45 días a los 3 años.

Desde la semana próxima los equipos docentes de los jardines estarán abocados a contactar a todas las familias que ya se inscribieron el año pasado y confirmar su reinscripción -que se realizó entre noviembre y diciembre- y concertar entrevistas. El objetivo de la nueva tarea es continuar con el seguimiento de cada caso, en especial de aquellos que no se anotaron para este ciclo lectivo.

“Este año vamos a sostener todas las estrategias que se desarrollaron durante 2020, ahora son herramientas que tenemos incorporadas a los mecanismos de vinculación pedagógica para ir acompañando la presencialidad a la que vamos a volver a partir del 1 de febrero”, contó Ricci.

Es importante resaltar que no se reducirán los cupos de cada sala. Los mismos se mantendrán y para cumplir con los protocolos -que se construyeron en base a las indicaciones y recomendaciones del Consejo Federal de Educación- se dividirá el cursado de las salas en dos grupos que alternarán la presencialidad y mantendrán actividades en sus hogares el resto de las jornadas.

De esa manera, el municipio privilegió que todas las niñas y niños puedan continuar con su educación, en el marco de esta etapa de la pandemia y en un sistema similar al que se ha anunciado para el sistema educativo provincial. Cabe destacar que los jardines maternales particulares, que ya están funcionando, lo están haciendo con la mitad del cupo por sala.

**Fechas importantes**

Desde la semana próxima, el personal docente de cada jardín contactará a las familias de las niñas y niños que cursaron el año pasado para confirmar la reinscripción. Luego, desde el 1 al 12 de febrero se concretarán jornadas de reencuentro con las familias a fin de realizar el seguimiento de cada caso e informar sobre las nuevas modalidades que impone la pandemia de covid-19.

En tanto, desde el 8 de febrero las familias que deseen preinscribir por primera vez a sus hijas o hijos en los jardines deberán acercarse a la institución de su barrio -de 9 a 11 o de 14 a 19- para llenar la planilla correspondiente. En cada caso, a partir del 15 de febrero, se realizará una entrevista para conocer la situación socioeconómica y establecer cuáles son los casos prioritarios para el ingreso.

“Es importante aclarar que es una preinscripción. Después un equipo interdisciplinario, en coordinación con la Secretaría de Políticas de Cuidado, va a evaluar las características socioeconómicas de la familia y se le da prioridad a las familias que tienen mayores necesidades”, explicó Ricci y aclaró que el o la adulta a cargo debe acercarse al jardín con el DNI del chico o chica y ahí se le dará turno para la entrevista.

**Acompañamiento durante la pandemia**

Cabe recordar que el año pasado, durante los meses de aislamiento y distanciamiento social la Municipalidad priorizó la adecuación de las instituciones de nivel inicial para no perder el vínculo con esos chicos y chicas. Así, los equipos docentes, de asistentes escolares y directivos de los jardines municipales realizaron un fuerte acompañamiento de las familias de las niñas y niños que habían comenzado el ciclo lectivo.

“El acompañamiento fue en distintos sentidos: pedagógico, lúdico, afectivo e incluso alimentario. Hubo un trabajo muy intenso de todos los equipos docentes y de asistentes escolares con la generación de nuevos contenidos pedagógicos digitales y lúdicos para que las familias sigan trabajando y estimulando en términos educativos y culturales a las niñas y niños en sus casas”, detalló el secretario de Educación y Cultura.

Además contó que se mantuvo un diálogo frecuente con todas las familias para poder hacer un acompañamiento personalizado y adaptar todos los materiales que se entregaron a las posibilidades y recursos con los que se contaba en cada hogar.