---
category: Agenda Ciudadana
date: 2021-08-01T06:15:25-03:00
thumbnail: https://assets.3dnoticias.com.ar/COBERESTAFAS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Ciberestafas: "Subimos datos a las redes sin darnos cuenta'
title: 'Ciberestafas: "Subimos datos a las redes sin darnos cuenta'
entradilla: "\"Lo dijo Rodrigo Álvarez, jefe de la Sección Cibercrimen de la Agencia
  de Investigación Criminal, quien profundizó sobre las ciberestafas y cómo prevenirlas."

---
“Desde el año pasado y en lo que va de este año se ha incrementado muchísimo el robo de datos, el intento de phishing y las ciberestafas utilizando nuevas tecnologías”, contó Rodrigo Álvarez, jefe de la Sección Cibercrimen de la Agencia de Investigación Criminal de Santa Fe. En diálogo con el programa aseguró que no hay una modalidad que lidere el ranking de ciberestafas pero que todas apuntan a lo mismo: apoderarse de credenciales bancarias o poder acceder al homebanking para extraer dinero de las víctimas.

Siempre utilizando el engaño como modus operandi, el experto explicó que la modalidad de engañar va mutando, utilizando diferentes tipos de excusas como puede ser una vacunación a través de WhatsApp: con un llamado o con un mensaje se apoderan de la cuenta de WhatsApp de la víctima, se hacen pasar por ella y piden dinero a sus contactos.

“De esta manera ya tienen una manera de estafar, que es justamente engañar a las personas haciéndoles creer que son otras”, precisó Álvarez, y agregó que también se da con premios o situaciones de emergencia.

**El cuento del tío en la era digital**

De hecho, a finales del año pasado la excusa de retirar un premio, como puede ser un televisor o una orden de compra, fue muy utilizada para concretar las estafas, haciendo ir a la víctima hasta un cajero y engañarla para que envíe su dinero.

“En este último tiempo se estuvo dando en WhatsApp, Facebook e Instagram la modalidad de que personas que venden algún tipo de artículo en redes sociales de repente les depositan más plata de la que en realidad vale el producto; luego las llaman desesperados diciéndole que se equivocaron en la transferencia que era menos el dinero que tenía que transferir. Cuando ingresan a su cuenta se encuentra que tiene ese dinero y luego se lo piden que la transfieran a otra cuenta”, describió el analista. Pero, ¿de dónde sale ese dinero de más?", preguntó.

Álvarez explicó que los delincuentes se aprovechan de una “brecha de seguridad” de las plataformas que prestan dinero con el simple hecho de tener un DNI, y que no son entidades bancarias. “Con algunos datos personales de la víctima, podemos sacar un crédito desde la página web, hacérselo depositar en su cuenta y luego cometer estos engaños”, sintetizó.

Más allá de no brindar datos sensibles por teléfono ni a entidades verificadas, el experto en cibercrimen advirtió que “en muchos casos estamos subiendo datos a las redes sociales sin darnos cuenta”.

“Por ejemplo: nos vacunamos, sacamos una foto con el cartón con la libretita que nos dan con nombre, apellido, DNI, qué médico nos vacunó, qué dosis nos pusieron, en qué centro nos vacunaron. Todos esos datos el delincuente lo transforma en información, porque ya a partir de un dato pueden buscar en las redes un montón de información, para luego realizar el engaño y sacarnos dinero desde otro lugar”.

Esto también puede ocurrir al sacarse una foto con uniformes de trabajo o escolares, una dirección, o una patente. “Hay que pensar dos veces antes de subir contenido a las redes si realmente queremos subir eso”, alertó.

¿Quiénes son los estafadores?

“Hay de todo un poco”, respondió el analista. En lo que es phishing sí suelen ser hacktivistas o "hackers": personas que le gusta utilizar sus conocimientos informáticos para hacer daño a otra. “Pero en lo que viene pasando últimamente, en esta modalidad de estafa de que llaman a las personas o les mandan un mensaje y le hacen creer que son una persona para sacarle dinero, no son personas muy vivas, sino que utilizan el engaño y a la vez utilizan el desconocimiento de los ciudadanos sobre las nuevas tecnologías y modalidades para brindar seguridad en las plataformas, tanto de homebanking como de mensajerías como WhatsApp, Telegram, Facebook, Instagram”.

Según Álvarez, algunas de las bandas trabajan desde la cárcel con un manual que alguien les proveyó con un lineamiento del paso a paso, pero si lo tienen que hacer solos no lo saben hacer.

En esta línea, el jefe relató que a principio de este año se hizo un allanamiento en una penitenciaría de Córdoba donde operaban un “call center” desde dentro de la cárcel. “Las investigaciones siguen adelante y se ha llegado a buen puerto. El delincuente tiene como ventaja, si sabe utilizar algunas herramientas informáticas, la posibilidad de mantener el anonimato y a nosotros como investigadores se nos complica, pero siempre dejan algún rastro alguna huella digital que nos deriva a otro lado”.

Sin una denuncia penal la Agencia de Investigación Criminal no puede comenzar a investigar. “Sí o sí hay que denunciar; de todas maneras, la entidad bancaria siempre pide la constancia de la denuncia para poder avanzar ellos desde su parte privada con la investigación”, dijo el titular de la Sección Cibercrimen.

Sobre la devolución del dinero sustraído, precisó que no es dependencia de ellos el reintegro, ya que es la entidad bancaria la que tiene que devolvérsela “siempre y cuando se pueda demostrar de que hubo una estafa o un engaño y que la víctima no tuvo nada que ver”.

“Justamente por ahí es esto de lo que se prende un poco el banco para demorar, pero haciendo un buen reclamo lamentablemente tenemos que tomarnos la molestia, como víctimas, de ir a la Defensoría del Pueblo, de ir al banco a hacer la denuncia, ir con la denuncia al banco, son todos trámites engorrosos que lamentablemente tenemos que hacerlo, pero insistiendo un poco el banco reintegra el dinero”, dijo Álvarez, y aconsejó siempre tomar jurisprudencia.

**La argentinidad, al palo**

El Banco Central tomó cartas en el asunto al emitir un comunicado que obliga a todos los bancos del país a que informen a través de sus canales oficiales qué es lo que ellos piden y a través de qué canales piden. "Nunca una entidad bancaria va a pedir datos tan sensibles como, por ejemplo, los códigos; los tres números de seguridad de la tarjeta de débito; los 16 dígitos; Token o tarjeta de coordenadas", precisó Álvarez, y agregó: "Sí te van a pedir algunos datos de validación específicos como número de teléfono, fecha de nacimiento, número de DNI, el correo electrónico en el cual nosotros tenemos vinculado a la cuenta".

Con respecto a la famosa clonación de la tarjeta, cuyo nombre técnico es skimming, el integrante de la Agencia de Investigación Criminal dijo que "acá va a seguir pasando hasta que se descarten todas las tarjetas con mando magnéticas".

"En Argentina y en algunos otros países de Latinoamérica seguimos operando con banda magnética en las tarjetas de crédito. En el resto del mundo no pasa, tienen chip y el chip es inviolable, no es clonable".

Lo peor del skimming es que no es tan fácil de distinguirlo a la vista, ya que se utiliza un dispositivo pequeño por el que se pasa la tarjeta, se clonan los datos de la banda magnética y se memorizan los tres números de seguridad. Al darle la tarjeta al playero de la estación de servicio o la moza del restaurante, ocultan el objeto en la palma de la mano y efectúan el robo de datos, para luego operar desde cualquier canal electrónico. Luego se ven reflejados los débitos en el homebanking.

"En el 99% de las investigaciones en las que intervine que se detectaron este tipo de movimientos en los que se necesita la tarjeta de débito, por ejemplo, pagar un servicio o extraer dinero, se da el caso de la modalidad de skimming", informó Álvarez.

Sobre el rumor de que solo haciendo clic a un link sospechoso ya pueden acceder a nuestros datos, el analista hizo algunas aclaraciones. El phishing es el engaño por medio de la suplantación de la identidad de cualquier empresa para que le brindemos nuestros datos. "Nos llega a través de un correo electrónico un mensaje de un banco o de una plataforma que usemos de streaming o tipo Netflix Amazon que alerta algo, que la cuenta se bloqueó, que no se pudo realizar el pago o que, en el caso de las entidades bancarias, tenemos bloqueado nuestro Home Banking", relató.

"Una vez que hacemos clic ahí es donde tenemos dos posibilidades. O simplemente se instala un malware en nuestro dispositivo que lo que hace es recopilar datos relacionados a credenciales, usuarios y contraseñas. Solamente almacena eso. Luego el ciberdelincuente lo que hace es ir desmenuzando y entendiendo qué es cada cosa y va probando para poder acceder a las cuentas", explicó Álvarez, pero aclaró: "En general los ataques phishing nos derivan a otra página y ahí no nos pregunta todos los datos que nosotros vamos a estar brindando de manera voluntaria, pensando que lo estamos cargando en el banco".

Y advirtió: "Todo eso que yo mencioné, no es un delito en Argentina, porque hasta ahí no hay nada, simplemente se obtuvo información. Recién cuando utilizan todos esos datos para hacer alguna compra o para vender en el mercado negro de internet, donde venden paquetes de datos de 15 o 20 tarjetas a un módico precio, ahí se concreta el delito de estafa".

La suplantación de identidad en Argentina todavía no es un delito. Según el titular de la Agencia de Cibercrimen, solo existe la contravención en la Ciudad Autónoma de Buenos Aires y un proyecto de ley, pero en nuestro país todavía no está tipificada como delito penal.