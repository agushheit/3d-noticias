---
category: Agenda Ciudadana
date: 2022-01-16T06:00:53-03:00
thumbnail: https://assets.3dnoticias.com.ar/jxc.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: En tensión, JxC aguarda la convocatoria para reunirse con Guzmán
title: En tensión, JxC aguarda la convocatoria para reunirse con Guzmán
entradilla: "Tanto los gobernadores radicales (que faltaron a la convocatoria del
  Presidente) como los jefes de bloque opositores mantienen distintas posturas frente
  al encuentro que podría concretarse el próximo martes. \n\n"

---
Gobernadores radicales y jefes de bloque de Juntos por el Cambio (JxC) en el Congreso, así como de otras fuerzas de la oposición, aguardan la convocatoria oficial del Gobierno nacional para concretar la reunión con el ministro de Economía, Martín Guzmán, quien informará sobre los alcances de las negociaciones con el Fondo Monetario Internacional (FMI).  
  
Fuentes parlamentarias indicaron que el encuentro, del que participaría además el presidente de la Cámara de Diputados, Sergio Massa, se realizaría, en principio, el martes próximo a las 17.  
  
Aunque no existe hasta el momento una confirmación oficial con respecto a una reunión por parte del Ejecutivo, de producirse se llevaría a cabo luego de las posturas encontradas que manifestaron los principales referentes de JxC en los últimos días en relación a participar en una convocatoria para analizar la negociación con el FMI.  
  
El Gobierno nacional mostró desde un principio la decisión de convocar a todos los gobernadores y mandatarios, tanto del oficialismo como de la oposición, para mostrar una postura en conjunto ante el organismo de crédito internacional.  
  
Los gobernadores radicales de Mendoza, Rodolfo Suárez; de Jujuy, Gerardo Morales y de Corrientes, Gustavo Valdés, y el jefe de Gobierno porteño, Horacio Rodríguez Larreta, decidieron no asistir a la convocatoria previa.

  
Así se verificó con la decisión de que la aprobación de un acuerdo con el FMI pasé por el Congreso, en contraposición a la decisión tomada por Mauricio Macri en 2018, de tomar el préstamo más grande en la historia del organismo sin contar con el aval del Poder Legislativo.  
  
Sin embargo, los gobernadores radicales de Mendoza, Rodolfo Suárez; de Jujuy, Gerardo Morales y de Corrientes, Gustavo Valdés, y el jefe de Gobierno porteño, Horacio Rodríguez Larreta, decidieron no asistir a la convocatoria.  
  
No obstante, y sobre la marcha, los mandatarios provinciales resolvieron enviar delegados a ese encuentro mientras que Rodríguez Larreta mantuvo su postura de no concurrir, al asegurar que se trataba de "una reunión política".  
  
No obstante, Morales se encargó de poner en claro las diferencias que surgieron al interior de Juntos por el Cambio en torno a la negociación con el FMI, al recordar que fue el gobierno de Macri el que tomó ese préstamo de más de 40 millones de dólares que la gestión de Alberto Fernández intenta ahora renegociar.

De esta forma, el mandatario jujeño mostró su predisposición a concurrir a un diálogo con el oficialismo en el Congreso y afirmó que en la coalición opositora había sectores que apostaban a "que todo se cayera en marzo".  
  
De todos modos, y más allá de las diferencias y disputas, la mesa nacional de Juntos por el Cambio resolvió a través de un encuentro realizado a través de Zoom su intención de concurrir a un encuentro con el Gobierno, pero reclamó "información concreta" sobre la negociación.  
  
Fuentes de bloques opositores afirmaron a esta agencia que aún no recibieron convocatoria alguna por parte del Poder Ejecutivo para reunirse con Guzmán.  
  
Del encuentro con el ministro participarían, en definitiva, los tres gobernadores radicales y Rodríguez Larreta, quien confirmó en las últimas horas su presencia, junto a los jefes de la mayoría de los bloques que integran ese espacio en ambas cámaras.  
  
En tanto que el gobernador de Mendoza no participaría y enviaría en su lugar al jefe del interbloque del Senado de JxC y exmandatario de esa provincia, Alfredo Cornejo, según señalaron fuentes de ese espacio.  
  
Si bien el encuentro iba a concretarse la semana pasada, la reunión fue postergada a la espera de que se puedan limarse las asperezas que subsisten entre los bloques del oficialismo y la oposición.  
  
Es que desde ambos sectores admiten que la relación entre ambos espacios "quedó dañada" sobre todo después de que JxC rechazara el Presupuesto 2022 y dejara al Poder Ejecutivo sin la ley de leyes.  
  
La postergación del encuentro también estuvo vinculada a que Massa debió aislarse por contacto estrecho, ya que su esposa y titular de la empresa estatal Aysa, Malena Galmarini, dio positivo de Covid 19 la semana pasada.  
  
Días atrás el propio Morales, después de varias idas y vueltas y una fuerte discusión en el seno de JxC, confirmó que ese espacio opositor concurriría al encuentro con Guzmán.  
  
Esta semana, en tanto, Morales mantuvo un encuentro con Rodríguez Larreta, para intentar acercar posiciones, en el marco de la fuerte controversia de estilos que mantienen ambos mandatarios.  
Morales y Larreta charlaron sobre lo ocurrido. Si bien mantienen sus diferencias, el Jefe de Gobierno porteño habría decidido participar del próximo encuentro.

El encuentro, según fuentes oficiales, habría servido para conversar sobre esas diferencias que mantienen, ya que mientras Morales defiende el diálogo con el Gobierno, Rodríguez Larreta no tendría la misma postura, aunque trascendió que concurrirá a la reunión.  
  
De todos modos, fuentes partidarias confirmaron que por parte del Senado concurrirían al encuentro con Guzmán Cornejo; el titular del bloque de la UCR en esa cámara, Luis Naidenoff; el jefe del PRO en ese cuerpo, Humberto Schiavoni, y se sumaría Martín Lousteau (Evolución Radical).  
  
Además participarían -aunque no está confirmado oficialmente- los jefes de bloques menores como la senadora del Movimiento Popular Neuquino, Lucila Crexell, y el peronista disidente Juan Carlos Romero.  
  
Por el lado del interbloque de JxC de Diputados estarán presentes los jefes de las diez bancadas que componen el espacio opositor en la cámara baja.  
  
Asistirán al menos los titulares de la UCR, Mario Negri; del PRO, Cristian Ritondo y de la Coalición Cívica, Juan Manuel López.  
  
En torno a la convocatoria, desde el Interbloque Federal, Alejandro "Topo" Rodríguez aseguró en declaraciones a Télam: "Nosotros estaremos en una reunión de trabajo con el ministro sólo si participan todos los bloques y si se hace en el Congreso".  
  
"Lo ideal es que ya venga con -al menos- un borrador del proyecto de ley que se vaya a tratar", aseveró el presidente del Interbloque Federal, aunque aún ese espacio no fue convocado a ninguna reunión.  
  
En tanto desde el Frente de Izquierda y de los Trabajadores, la diputada Romina Del Plá, coincidió en que no hubo "hasta el momento" ninguna invitación formal.  
  
"Desconocemos por ahora el carácter de la convocatoria. Fijaremos posición en función de eso. No vamos a ser parte de un acuerdo para postrar al país frente al FMI", adelantó la legisladora del Partido Obrero (PO).