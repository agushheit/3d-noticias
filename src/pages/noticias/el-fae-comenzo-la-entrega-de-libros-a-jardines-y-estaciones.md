---
category: La Ciudad
date: 2021-06-04T08:13:19-03:00
thumbnail: https://assets.3dnoticias.com.ar/jardines.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad de Santa Fe
resumen: El FAE comenzó la entrega de libros a jardines y Estaciones
title: El FAE comenzó la entrega de libros a jardines y Estaciones
entradilla: 'El Fondo de Asistencia Educativa brindará materiales para nutrir las
  bibliotecas de jardines provinciales, así como de los Jardines y las Estaciones
  que dependen de la Municipalidad. '

---
Con la entrega de libros a siete jardines provinciales se puso en marcha una iniciativa de promoción de la lectura en la primera infancia, que surgió de la Municipalidad y fue bien recibida por la comisión administradora del Fondo de Asistencia Educativa (FAE). Con esta nueva línea de acción, denominada FAE Lectura, se adquirieron más de 400 libros para aportar nuevos materiales a las bibliotecas de jardines provinciales, a los Jardines Municipales y a las Estaciones. La primera entrega de materiales se realizó este jueves, en un breve encuentro que se desarrolló en el patio del Jardín de Infantes Nº 4 “Coronel Pringles”.

Como un obsequio por parte del municipio se entregaron también juegos de cartas y dados para leer y escribir, que se produjeron como parte de la Feria de las Palabras. Este proyecto se desarrolla desde abril en los Jardines y las Estaciones municipales con distintas propuestas para promover la lectura y capacitar a docentes sobre el abordaje de la literatura en la primera infancia, alternando la presencialidad y la virtualidad en función de la situación sanitaria.

El encuentro contó con la presencia del secretario de Educación y Cultura, Paulo Ricci; la comisión del Fondo de Asistencia Educativa que integran la subsecretaria de Gestión Cultural y Educativa, Huaira Basaber, por parte de la Municipalidad; la secretaria de la Federación de Cooperadoras, Ivana Pintos, y la delegada de la Regional IV, Mónica Henny. Para recibir los libros estuvieron las directoras de los jardines de infantes: Nº 33 “Juana Paula Manso”, Nº 52 “Juan Ramón Jiménez”, Nº 54 “Germán Berdiales”, Nº 73 “Prof. Nélida Bottai de Boente”, Nº 222 “Blas Parera”, Nº 224 “Monseñor Antonio Rodríguez” y Nº 272 “Hugo Maggi”.

**Promover la lectura**

El secretario de Educación y Cultura de la Municipalidad se refirió a los principales lineamientos de FAE Lectura, que en esencia “busca acercar la lectura a los jardines y a distintos establecimientos educativos y también sociales”. Sobre su implementación detalló que la entrega de libros comenzó con un primer grupo de jardines integrados, continuará la próxima semana en las Estaciones y en los Jardines municipales, con 20 libros y obsequios para que cada espacio construya o enriquezca su propia biblioteca.

“Lo más importante es esta iniciativa de la Comisión del FAE de ocuparse de hacer llegar estos insumos pedagógicos tan valiosos, sobre todo en este momento en que los libros pueden ser un vehículo de vínculo y de contacto con las niñas y niños que en este momento no pueden asistir presencialmente a los jardines”, remarcó Ricci.

FAE Lectura se desarrollará en paralelo a las tareas que financia habitualmente el fondo para mejorar la infraestructura y el entorno de las instituciones educativas, coordinando muchas de las intervenciones con la Dirección de Obras y Asistencia Educativa (DOAE). “La Comisión del FAE viene trabajando de manera muy articulada y frecuente en una serie de proyectos de obra y de mantenimiento de distintos espacios educativos de la ciudad. Hay muchas obras en marcha y otras que van a ser lanzadas en estos próximos meses, que son la tarea principal del Fondo y se complementan con esta iniciativa de estimular la lectura en los jardines maternales y aportarles para eso los bienes culturales que necesitan”, concluyó el funcionario.

![Entrega_libros_(2)](https://ci3.googleusercontent.com/proxy/MLNGxfq4fVYSv2vu7zNKAlHot1OBb8lHxmGb6IXbaE6N6Ij-mXaI94q1q7cQ5JkX4neJqbqOueTsmrvuYL3sDdHnhZMsYFL7TIaIyXXz6AFfV6n5ROUPNoIoPPmNuhTBrdJQPMGHxg=s0-d-e1-ft#https://app2.dopplerfiles.com/Users/239327/Campaigns/11670579/Entrega_libros_(2).jpg)

**Libros para compartir**

Por su parte, Basaber enfatizó que “la entrega de libros desde FAE es un acontecimiento inaugural. Nos enorgullece hacer crecer el patrimonio cultural, educativo y simbólico de las infancias aportando al camino del juego, los sueños y la imaginación. Estos libros son para compartir en familia y para llegar con esperanzas a las infancias en un momento tan difícil de atravesar”.

Además, la funcionaria municipal recordó que esta nueva propuesta está en sintonía con políticas que se impulsan desde la Secretaría de Educación y Cultura para estimular la lectura y facilitar el acceso al libro, como es la Feria de las Palabras “una propuesta participativa que ofrece la literatura en cuentos contados, en libros disponibles para usar y leer de manera salteada, al revés, con imágenes, por color, con humor y con amor. En esta ocasión regalamos libros y juegos que son parte de una Feria que crece invitando y proponiendo a las infancias como protagonistas”.

**Comprometidas con la educación**

La Federación de Cooperadoras Escolares traslada al interior de la Comisión del FAE, el punto de vista de las familias comprometidas con la educación. Así lo entiende Ivana Pintos, quien valoró con sus pares de la comisión la propuesta de FAE Lectura que planteó la Municipalidad para realizarla en paralelo con los proyectos que atienden las necesidades de infraestructura. “En este momento, ya que las alumnas y los alumnos no pueden disfrutar a pleno de sus edificios y pasan mucho tiempo en sus casas, la lectura puede ser un espacio para el aprendizaje, para encontrarse con material nuevo, actualizado y distinto a lo habitual”, consideró.

Para trazar el camino que seguirá esta propuesta, Pintos considera clave el papel que pueden desempeñar las cooperadoras y por eso insiste en la necesidad de fortalecer el lazo de esas organizaciones con las instituciones. “Nosotros aportamos el punto de vista de la familia que se involucra en la educación. Cuando participamos en la Comisión del FAE ponemos en común ese punto de vista con el que aporta el Ministerio de Educación de la provincia, desde la cuestión pedagógica, y con el municipio. Nos respetamos, nos escuchamos y así surgen estas cosas que son maravillosas a partir de un trabajo en armonía”, señaló. Y adelantó que “la idea es continuar con FAE Lectura a medida que relevamos las necesidades que tengan las escuelas, siendo equitativos con una distribución de materiales en todo el territorio de la ciudad”.