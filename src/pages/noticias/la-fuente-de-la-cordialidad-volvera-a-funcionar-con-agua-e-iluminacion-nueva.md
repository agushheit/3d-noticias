---
category: La Ciudad
date: 2021-10-02T06:00:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/fuente.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: La Fuente de la Cordialidad volverá a funcionar con agua e iluminación nueva
title: La Fuente de la Cordialidad volverá a funcionar con agua e iluminación nueva
entradilla: "Es un símbolo de Santa Fe y está en la cabecera del Puente Oroño, en
  el acceso Este por ruta 168. Mejoran la alimentación eléctrica y de bombas, restauran
  su ornamentación y será pintada.\n\n"

---
La Municipalidad de Santa Fe comenzó este viernes a poner en valor la Fuente de la Cordialidad, en el ingreso a Santa Fe por ruta 168. Las tareas son llevadas a cabo por empleados municipales. Las refacciones generales incluyen el mejoramiento del sistema de alimentación eléctrica y de bombas, la restauración de elementos ornamentales, el sellado de grietas, pintura, entre otras tareas. La inversión que demanda la puesta en valor oscila el millón de pesos.

 “Es una iniciativa tomada por una decisión política del intendente Emilio Jatón cuando asumimos”, dijo el secretario de Educación y Cultura municipal, Paulo Ricci. “Cuando visitamos lugares como el Anfiteatro del Parque del Sur, el Teatro Municipal, los parques y demás, pensamos en poner en valor estos espacios tradicionales tan caros a la idiosincrasia santafesina, porque son emblema y símbolo de la santafesinidad”, argumentó.

 “Esta fuente también es un emblema y se ubica en uno de los principales acceso a la ciudad”, dijo luego Ricci. Y agregó que allí trabajan ahora equipos de la Secretaría de Obras Públicas asistidos por los restauradores que pertenecen al equipo de Museos, para restaurar los ornamentos y poner en valor y funcionamiento esta fuente.

 “Con esta misma impronta trabajaremos también en otros espacios, como lo estamos haciendo en la Estación Mitre, otro emblema de la ciudad con más de 30 años de abandono -dijo el funcionario-. Antes de fin de año la ciudad la tendrá totalmente recuperada”, anticipó Ricci.

**Detalles**

 Por su parte, el secretario de Obras Públicas, Matias Pons, dijo que las tareas de puesta en valor de la Fuente de la Cordialidad demandará unos 15 días de “trabajos finos”. Primero se realizaron “tareas gruesas, porque hace más de 12 años que la fuente está apagada”, explicó. Por lo que hubo que realizarle una limpieza profunda. 

 “Se armaron los sistemas de alimentación de la bomba original, que también se recuperó, al igual que el tablero eléctrico”, enumeró el funcionario. Y un equipo con una hidrolavadora “le retiró el hollín que tenía el monumento”, recuperando su matiz natural. Por otra parte anticipó Pons que la próxima semana se trabajará en el sellado de las grietas e impermeabilización. “Esperemos terminar cuanto antes porque estamos ansiosos de verla reluciente”, finalizó.