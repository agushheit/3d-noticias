---
category: Agenda Ciudadana
date: 2021-10-17T06:00:02-03:00
thumbnail: https://assets.3dnoticias.com.ar/MIPIEZA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Más de 35 mil mujeres fueron beneficiadas en el primer sorteo del Programa
  Mi Pieza
title: Más de 35 mil mujeres fueron beneficiadas en el primer sorteo del Programa
  Mi Pieza
entradilla: El sorteo tuvo 105.586 inscriptas de todo el país y se hizo en el barrio
  Papa Francisco, donde estuvieron el gobernador bonaerense, Axel Kicillof, y funcionarios
  del área social.

---
El ministro de Desarrollo Social, Juan Zabaleta, afirmó que para "empezar la etapa de la pospandemia hay que seguir incluyendo" al encabezar el primer sorteo del Programa Mi Pieza, que benefició a más de 35 mil mujeres como parte de una iniciativa que "volcará en el mercado interno que volcará en el mercado interno 8 mil millones de pesos que impactarán de manera directa en los diferentes rubros de la construcción".  
  
"Hay 4.400 barrios populares que tenemos la responsabilidad de urbanizar. De las mujeres inscriptas, el 15% fueron víctimas de violencia de género. Estamos felices de seguir brindando derechos, de seguir incluyendo, para empezar esta nueva etapa de pospandemia y así reconstruir la Argentina", expresó Zabaleta en un comunicado y confirmó además que el segundo sorteo se realizará en diciembre.  
  
El sorteo del Programa tuvo como ganadoras a 35.101 mujeres de las 105.586 inscriptas de todo el país y se hizo en el barrio Papa Francisco, de Pontevedra, donde estuvieron la secretaria de Integración Socio Urbana, Fernanda Miño, el gobernador bonaerense, Axel Kicillof; el intendente de Merlo, Gustavo Menéndez; la secretaria de Desarrollo e Integración Social del municipio, Karina Menéndez; y la jefa de Gabinete del Ministerio de Mujeres, Géneros y Diversidad, Laura Carvajal.  
  
Por su parte, el gobernador Kicillof consideró que el Programa Mi Pieza es "extraordinario porque va a llegar al corazón de los barrios, donde están las necesidades".

![](https://www.telam.com.ar/advf/imagenes/2021/10/6161ec5381c48_1004x2157.jpg)

  
En ese sentido, remarcó la importancia del programa en la reconstrucción de la provincia y del país, ya que "a través de la construcción se pone en marcha la economía local".  
  
La secretaria de Integración Socio Urbana dijo que el Programa Mi Pieza nació "a partir de que empezamos a trabajar con la Secretaría en los barrios populares de todo el país" y "la gente nos reclamaba una ayuda para ampliar y refaccionar las casas".  
  
En ese contexto, expresó que esa iniciativa es posible "gracias al 15% del Aporte Extraordinario de las Grandes Fortunas que el Gobierno nacional destinó a este programa".  
  
La secretaria de Integración Socio Urbana dijo que el Programa Mi Pieza nació "a partir de que empezamos a trabajar con la Secretaría en los barrios populares de todo el país" y "la gente nos reclamaba una ayuda para ampliar y refaccionar las casas".

  
El intendente de Merlo, por su parte, celebró "el inicio de un programa que le va a cambiar la vida a miles de personas" y sostuvo que "esta es una señal clara de que se está poniendo a la Argentina de pie, con la circulación virtuosa del dinero que pone el Estado a través de programas como Mi Pieza".  
  
El Programa Mi Pieza es una línea de financiamiento dependiente de la Secretaria de Integración Socio Urbana del Ministerio de Desarrollo Social de la Nación, destinada a mujeres residentes en barrios populares del ReNaBaP, con el fin de que puedan realizar obras en su vivienda, como pequeños arreglos, refacción o ampliaciones.  
  
Las beneficiadas acceden a 100 mil o 240 mil pesos que les son entregados en dos cuotas del 50% a través de Anses.  
  
La primera cuota es para el inicio de la obra, mientras que la segunda la reciben luego de validar el avance de la obra a través de una aplicación en el celular.  
  
El Programa es compatible con cualquier prestación social, incluido el programa Potenciar Trabajo pero quienes no completen el avance de obra quedarán inhabilitadas para nuevas líneas de asistencia económica.