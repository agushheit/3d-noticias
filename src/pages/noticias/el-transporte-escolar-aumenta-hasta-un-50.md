---
category: La Ciudad
date: 2021-03-06T07:00:48-03:00
thumbnail: https://assets.3dnoticias.com.ar/trasnporte.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: El transporte escolar aumenta hasta un 50%
title: El transporte escolar aumenta hasta un 50%
entradilla: Lo confirmó Daniel Correa, referente de ATEA. El último cambio en el cuadro
  tarifario de la asociación fue en febrero del 2020.

---

Daniel Correa, representante de la Asociación Transportistas Escolares y Afines (A.T.E.A) confirmó el aumento de la tarifa del transporte escolar para el ciclo lectivo 2021. El incremento será del 50% para solventar los gastos de este año que se suman a los acarreados desde 2020 donde la Asociación no pudo trabajar. 

El monto estimativo para retornar con la actividad es una cuota de $ 8.000 por chico que incluye viaje de ida a la escuela y vuelta al hogar. También estará disponible una opción de viaje único, ya sea de ida a la escuela o de vuelta a la casa que tendrá un costo de $ 6.000.

Además, comentó que aún no tienen definida la forma de trabajo para este ciclo debido a la modalidad que acató el Gobierno provincial para el regreso a clases presenciales. “No sabemos bien cómo vamos a empezar la actividad. Lo que sí sé, es que los colegios se están organizando y a medida que nos vamos enterando vamos organizando nuestro trabajo” afirmó Daniel Correa. 

También agregó que ya comenzaron a contactar a padres que eran clientes y viceversa para comenzar a organizarse ambas partes. Sin embargo aclaró que “nosotros dependemos de los colegios para iniciar las actividades. Esto va a ser un día a día cuando comiencen las actividades”.

El referente de A.T.E.A finalmente, afirmó que no saben cómo se van a manejar aún ni cómo los van a implementar, pero “presentamos un protocolo a la Municipalidad y se valuó bien”. 