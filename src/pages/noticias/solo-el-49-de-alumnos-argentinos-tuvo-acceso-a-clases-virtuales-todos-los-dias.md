---
category: Agenda Ciudadana
date: 2021-05-15T06:15:18-03:00
thumbnail: https://assets.3dnoticias.com.ar/EDUCACION.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Solo el 49% de alumnos argentinos tuvo acceso a clases virtuales todos los
  días
title: Solo el 49% de alumnos argentinos tuvo acceso a clases virtuales todos los
  días
entradilla: En el comienzo del ciclo lectivo 2021, el 75,2% de las escuelas primarias
  estatales urbanas del país optaron por un formato bimodal, aunque las barreras tecnológicas
  resultan una dificultad.

---
El 75,2% de las escuelas primarias estatales urbanas optó por la bimodalidad –combinación de educación presencial y virtual– para la vuelta a clases en 2021. En tanto que el 18,3% adoptó la modalidad totalmente presencial, en contraste de un 5,3% que se mantuvo de forma plena en la virtualidad. En el 1,2% de las escuelas, no hubo contacto con los alumnos bajo ninguna modalidad.

Los datos surgen del informe "Modalidad de vuelta a clases en la escuela primaria", del Observatorio de Argentinos por la Educación, con autoría de Melina Furman (Universidad de San Andrés), Víctor Volman y Federico Braga. El documento indaga en las características de la vuelta a clases en la Argentina en 2021, con el objetivo de entender cómo se adecuaron las familias y escuelas a los protocolos sanitarios y cómo se está abordando la dinámica pedagógica.

El informe se basa en una encuesta a familias de estudiantes de escuelas primarias estatales de educación común y de ámbito urbano de todo el país. La muestra es representativa de dicho universo; el relevamiento se llevó adelante entre el 29 de marzo y el 18 de abril de 2021, antes de la implementación de las medidas gubernamentales que desde el 19 de abril restringieron la presencialidad escolar en algunas jurisdicciones.

"La encuesta muestra que la bimodalidad es el formato más extendido en esta vuelta de las escuelas primarias del país. Esta vuelta a la presencialidad, aunque no sea completa aún, es clave para la continuidad pedagógica y el bienestar emocional de los chicos. Existe aún una deuda pendiente para que esa continuidad pedagógica se siga fortaleciendo, especialmente para chicos que tuvieron un vínculo muy intermitente con la escuela en 2020", explicó Melina Furman, coautora del informe.

**Asimetrías educativas**

Según los resultados, apenas en el 15,9% de las escuelas primarias estatales urbanas los estudiantes asistieron los 5 días de la semana a la escuela, mientras que el 50,6% lo hace 4 días por semana. El 7,5% respondió que asistieron 2 días a la semana y el 5,4%, 3 días. En el 5,8% de las escuelas, los estudiantes solo tuvieron clases presenciales 1 día por semana.

Cuando no asistieron presencialmente, en el 25,6% de las escuelas los alumnos tampoco lo hicieron de manera virtual. Mientras que en el 48,8% de los casos, los estudiantes sí tuvieron clases de manera virtual cuando no hubo clases presenciales.

"Los vínculos son importantísimos para el desarrollo de oportunidades de aprendizaje. Un 1,2% de chicos desvinculados totalmente de la escuela primaria nos tiene que interpelar para multiplicar el esfuerzo de toda la comunidad educativa para nominalizar a esos estudiantes y recuperar el tiempo perdido con cada uno", apuntó Martín Salvetti, profesor de secundaria y subsecretario de Educación de Lomas de Zamora.

Además, los datos evidencian asimetrías en la carga horaria: en el 46% de los casos los alumnos de primaria asistieron 4 horas a clases presenciales; en el 33,3%, entre 2 y 4 horas. En una de cada 10 escuelas (11%) los alumnos tuvieron 2 horas o menos. Solo en el 7% de las escuelas los estudiantes tuvieron entre 4 y 6 horas de clase, mientras que apenas en el 2,5% los alumnos asistieron 6 o más horas.

En ese sentido, Gabriela Azar, directora del Departamento de Educación de la UCA, opinó que "la bimodalidad requiere que cada escuela y cada docente revise sus prácticas de enseñanza y de evaluación para hacerlas adecuadas a este nuevo escenario educativo, ya que en la digitalidad es necesario el uso de herramientas diversas y novedosas que ayuden a sostener el vínculo educativo y su correspondiente exigencia asociada".

**Burbujas**

En el 96,7% de las escuelas, los estudiantes estuvieron divididos en grupos o burbujas para mantener la distancia social en el aula, mientras que solo en el 3,3% los estudiantes no tuvieron su grado dividido en grupos.

En la mayoría de las escuelas (88,5%) las burbujas fueron de hasta 15 estudiantes, mientras que el 10,2% tuvieron entre 15 y 20 alumnos, y el 1,2% tuvieron entre 21 y 30 estudiantes. No hubo escuelas con burbujas de más de 30 estudiantes.

Solo 1 de cada 3 escuelas (31,7%) cuenta con ventanas al exterior con la posibilidad de abrirse. El 20,3% de las instituciones dispone de un termómetro sin contacto, y solo el 13,2% cuenta con conectividad a internet para alumnos/as y docentes. El 1,5% de las escuelas no dispone de ninguno de estos elementos.

**Protocolos**

Los resultados muestran que el 84,5% de las escuelas implementaron ingreso o salida escalonada para evitar la congestión y aglomeración de personas. Una de cada tres familias encuestadas (32,6%) expresó tener dificultades para coordinar estos horarios para llevar y traer al estudiante En contraste, 6 de cada 10 (59,2%) no manifestaron problemas, y 8,2% respondieron que el/la alumno/a va y vuelve de manera independiente a la escuela.

Las familias informaron que en 3 de cada 4 escuelas (77,6%) los estudiantes no tuvieron dificultades para cumplir con los protocolos, mientras que en el 20% sí tuvieron dificultades para cumplirlos. En el 84,9% de las escuelas alumnos y docentes usaron permanente el barbijo en todos los espacios de la institución.

**Contagios**

Ante la presencia de casos sospechosos o positivos de Covid-19 (de un alumno/a, docente o trabajador de la institución), hay diversidad en los procedimientos a seguir.

El 28,5% de las escuelas respondió que dejaron de asistir los miembros de ese grupo burbuja. El 20,8% mencionó el aislamiento de las personas con contacto estrecho, mientras que el 4,2% informó el cierre de la escuela. Menos del 1% de las escuelas (0,4%) no tomaron medidas habiendo ocurrido contagios.

Según los resultados, en 1 de cada 3 escuelas (33,2%) no ha habido detección de casos desde el inicio de clases. El 15,2% menciona contagios de docentes y el 14,8%, de estudiantes. Menos del 3% informa contagios de directivos (2,8%) y personal de apoyo (2,5%); y un 30,7% no sabe si hubo contagios en su escuela.