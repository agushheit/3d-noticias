---
layout: Noticia con imagen
author: "  Fuente: El Litoral"
resumen: "GDN es ahora dueño de Walmart "
category: Agenda Ciudadana
title: Grupo De Narváez adquirió la cadena de supermercados Walmart Argentina
entradilla: El Grupo De Narváez (GDN) adquirió la totalidad del negocio de la
  cadena de supermercados Walmart en la Argentina, anunció hoy la firma
  compradora en un comunicado.
date: 2020-11-07T15:02:41.723Z
thumbnail: https://assets.3dnoticias.com.ar/wm.jpg
---
“Los nuevos propietarios, que cuentan con operaciones minoristas en la Argentina, Ecuador y Uruguay, aportarán una sólida experiencia local que impulsará el crecimiento de la empresa”, afirmó el Grupo De Narváez, que tiene como principal cara visible al empresario Francisco de Narváez.

Subrayó que “este acuerdo marca un nuevo y emocionante capítulo en los 25 años de historia de la empresa”. Aseguró que “Walmart seguirá apoyando a la empresa y a sus nuevos propietarios a través de servicios de transición y acuerdos de abastecimiento”. “No obstante, Walmart no retendrá una participación accionaria”, aclaró el Grupo, tras lo cual subrayó que “bajo la dirección de Fernando Minaudo, director ejecutivo de GDN, los nuevos propietarios continuarán impulsando el crecimiento a largo plazo de la empresa en el dinámico entorno minorista de la Argentina”.

Además, indicó que “la empresa seguirá siendo uno de los empleadores más grandes de Argentina y continuará trabajando en estrecha colaboración con los proveedores para brindar el mejor valor a los clientes, manteniendo su compromiso de respaldar a los proveedores nacionales y a las pequeñas empresas”. También, señaló que “Dolores Fernández Lobbe (actual gerente general) continuará apoyando a la empresa durante el período de transición, después de lo cual pasará a desempeñar un nuevo papel dentro de Walmart”.

[](<>)Walmart Argentina inició sus operaciones en 1995, con el lanzamiento de su primer establecimiento en Avellaneda. Desde entonces, la empresa continuó creciendo y hoy cuenta con más de 92 establecimientos y 10.000 trabajadores bajo los nombres Changomas, Mi Changomas, Supercentro Walmart y Punto Mayorista. Luego de la transición, los establecimientos Changomas, Mi Changomas y Punto Mayorista continuarán atendiendo a los clientes en la Argentina.

Asimismo, cuenta con un centro de distribución propio, ubicado en la localidad de Moreno, y tres centros productivos de panificados, carnes y feteados.

Los supercentros Walmart cambiarán de nombre después del período de transición. En el mercado especulan que el nombre que adoptará tras el período de transición podría ser Ta-Ta, con lo que ampliarían a ambas orillas del Río de La Plata la cadena de supermercados que el Grupo De Narváez tiene en Uruguay. Desde el la empresa compradora no dieron información respecto a este último punto, así como sobre el monto de la operación.

Estamos muy orgullosos y entusiasmados con esta nueva inversión. Compartimos los mismos valores: una filosofía centrada en el cliente, en la excelencia operativa, además del compromiso que hemos asumido con las comunidades en las que operamos”, indicó Minaudo. Destacó que “tanto el desempeño de la empresa como la dedicación de sus increíbles trabajadores durante la pandemia de la Covid-19 demostraron la fortaleza y resistencia fundamentales del negocio, y nos complace en gran medida apoyar al equipo, impulsar el crecimiento a largo plazo y crear nuevas oportunidades para empleados y proveedores en la Argentina”.

A principios del corriente año, la cadena Walmart había comenzado a explorar nuevas alianzas para potenciar su negocio en Argentina pero el proceso quedó suspendido en abril debido a la situación de emergencia por el coronavirus. En ese momento, desde la empresa informaron a Télam que «la decisión no tiene que ver con Argentina. De hecho, el mismo proceso (igualmente en suspenso) tuvo lugar en Reino Unido, por lo que no guarda relación con el país, sino con una necesidad de potenciar el negocio en un momento donde el retail está en pleno proceso de transformación».

En octubre, se retomaron las negociaciones y Grupo De Narváez, que controla en Uruguay las cadenas Ta-Ta, Multiahorro y BAS, consiguió adquirir el negocio de la cadena en Argentina, en el que estaban interesados también la compañía Inverlat (dueña de Havanna), el Grupo Andreani y Alfredo Coto. El grupo encabezado por el ex diputado Francisco de Narváez fue fundador en Argentina de Casa Tía, un supermercado que llegó a tener 61 sucursales en todo el país y que en 1999 fue adquirido por el Grupo Exxel, un fondo de adquisición conformado por inversionistas estadounidenses, y Promodés, la segunda mayor cadena minorista de Francia (antes de fusionar con Carrefour), por US$630 millones.