---
category: Agenda Ciudadana
date: 2021-03-04T00:30:23-03:00
thumbnail: https://assets.3dnoticias.com.ar/nafta.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: Los combustibles volverían a aumentar el 12 de marzo
title: Los combustibles volverían a aumentar el 12 de marzo
entradilla: Si se concreta, será la sexta suba de precios en lo que va de 2021.

---
El próximo 12 del corriente marzo volverán a actualizarse los impuestos específicos que gravan a la nafta y el gasoil. Pero también se ajustará el valor de los biocombustibles según el sendero establecido por el Gobierno. Eso, se descuenta, generará un nuevo impacto en los surtidores. 

Como se informó, el Gobierno nacional, a través del Decreto 35/2021, decidió postergar once días la suba de los impuestos a los combustibles, que según la normativa vigente, debía implementarse el primero de marzo.

La medida corresponde a la actualización del cuarto trimestre calendario del año 2020, sobre la base de las variaciones del Índice de Precios al Consumidor (IPC) que suministra el Instituto Nacional de Estadísticas y Censos (Indec). 

Ese mes también entrarán en vigor los alcances de las resoluciones 1/2021 y 2/2021, en las cuales, el secretario de Energía, Darío Martínez, fijó nuevos valores de adquisición del biodiesel destinado a su mezcla obligatoria y del bioetanol a base de caña. 

En el caso del biodiesel, la normativa también establece la reducción en los porcentajes de corte obligatorio de gasoil, con un incremento escalonado hasta alcanzar el 10 por ciento en abril. 

De esta manera, el camino de recomposición de precios acordado permitió fijar un nuevo valor por tonelada, que en marzo será de $ 89.975 con un 8,4 por ciento de mezcla, en tanto que para el caso del bioetanol, será de $ 48,700. 

"Los ajustes señalados impactarán en el litro de nafta en $ 1,92, que representa 2,6 por ciento de aumento sobre el precio de referencia de Caba, mientras que el gasoil la suba será de $ 1,50, o un 2,1 por ciento", estimó el consultor económico Cristian Bergmann. Pero aclaró: "Esto sin incluir actualizaciones comerciales por parte de las petroleras".

Sin embargo, y siendo este un año electoral, nadie puede asegurar que lo establecido se cumpla cabalmente. Prueba de ello fueron las sucesivas postergaciones impositivas ocurridas durante 2019 y el primer semestre de 2020, coincidentes con los comicios presidenciales en los que se impuso Alberto Fernández, y sus primeros meses al frente del Ejecutivo, sumado a la irrupción de la pandemia.

Fuentes del mercado afirmaron que las compañías no quieren repetir los aumentos sucesivos de febrero, cuando por la modificación de estas dos variables, las pizarras cambiaron en tan solo siete días, provocando un fuerte rechazo en los consumidores.

La misma fuente recordó que también comenzó a regir desde este lunes la nueva proporción obligatoria de biodiesel en su mezcla con gasoil, que pasó de 6,7 a 8,4 por ciento. No obstante afirma que hay dudas sobre su complimiento ya que aseguran que diversas razones el corte no se ajusta a lo que ordena el decreto.

 

**EL ARRASTRE**

En lo que va del año, los combustibles escalaron 7 por ciento en CABA y hasta 12 por ciento diferentes localidades.

En conjunto treparon un 32,95 por ciento interanual, mientras que la inflación medida por el Indec arrojó una suba de 38,5 por ciento para el mismo período, 5,5 puntos más alta.

Por esta razón, para las estaciones de servicio el 2021 arrancó más complicado de lo que se esperaba, ya que los reiterados incrementos de precios provocaron que en enero las ventas cayeran 6,6 por ciento en comparación con diciembre.