---
category: Estado Real
date: 2021-03-08T07:30:30-03:00
thumbnail: https://assets.3dnoticias.com.ar/Esquivel.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Provincia ya cuenta con una dirección femenina de comunidades de pueblos
  originarios
title: La Provincia ya cuenta con una dirección femenina de comunidades de pueblos
  originarios
entradilla: En una decisión sin precedentes, el gobernador Omar Perotti puso en funciones
  como directora de la flamante unidad provincial, a la integrante del pueblo Qom,
  Rosana Esquivel.

---
Luego de un cúmulo de acciones erráticas en los últimos 12 años en materia de políticas destinadas a los pueblos originarios de la provincia de Santa Fe y que suman más de 200 mil personas en todas las comunidades que lo conforman; el gobernador Omar Perotti decretó la creación de la Dirección Provincial de Comunidades Originarias, cuya titular será la integrante del pueblo Qom de Rosario, Rosana Esquivel.

El objetivo de esta dirección -que dependerá del Ministerio de Desarrollo Social- será conformar en el corto plazo, un registro serio y completo de las más de 100 comunidades de pueblos Qom, Mocoví, Kolla, Corondá, Diaguita-Calchaquí, Guaraní y Mapuche, entre otros, que habitan en el territorio santafesino.

Con esa información, la siguiente meta será implementar estrategias concretas de integración, mediante un trabajo transversal entre todos los ministerios y direcciones provinciales.  
   
En vísperas del Día Internacional de la Mujer, Esquivel celebró la designación por partida doble. “Como mujer y descendiente Qom, es una alegría enorme poder tener esta oportunidad que nos brinda el gobernador (Perotti) para, desde el Estado, poder trabajar en toda la provincia”.

Esquivel se afincó en Rosario “hace unos 30 años” en busca de oportunidades, al igual que cientos de integrantes de pueblos originarios que migraron hacia las grandes urbes. La flamante directora estará acompañada por el nuevo presidente del Instituto Provincial de Aborígenes Santafesinos (Ipas), Francisco Saavedra. También, se sumará el intregrante de la comunidad Mocoví de Melincué, Ariel Araujo.

Precisamente, el Instituto fue creado en 2007 pero hasta la fecha ninguno de sus objetivos primordiales habían sido concretados. Incluso, las escasas asambleas ciudadanas que se desarrollaron tuvieron poca participación de los pueblos originarios y sus organizaciones, debido a la carencia de un órgano estatal que los integre. Ahora, el desafío será “empezar de nuevo”, según dijo Esquivel.

Para ello, la nueva Dirección tendrá la misión de discutir y definir acciones para estrategias conjuntas de autodesarrollo y preservación del medio ambiente; así como también establecer una posición común para la integración de sus propuestas en las  
políticas públicas que ya está implementado el gobierno de Santa Fe, a través de las áreas de Justicia, Educación, Salud, Hábitat, Producción, Trabajo, Cultura, Deportes, Género, Economía y Turismo.