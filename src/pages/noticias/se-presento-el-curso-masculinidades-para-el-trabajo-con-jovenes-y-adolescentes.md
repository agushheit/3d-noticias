---
category: La Ciudad
date: 2021-09-06T06:00:45-03:00
thumbnail: https://assets.3dnoticias.com.ar/MASCULINIDADES.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Se presentó el curso “Masculinidades para el trabajo con jóvenes y adolescentes”
title: Se presentó el curso “Masculinidades para el trabajo con jóvenes y adolescentes”
entradilla: Serán cuatro encuentros virtuales dirigidos a instituciones educativas,
  clubes y organismos estatales que trabajen con varones. La inscripción permanecerá
  abierta hasta el 15 de septiembre.

---
Este jueves, la Municipalidad organizó el conversatorio “Masculinidades en tiempos de feminismos. Desafíos y oportunidades”. Fue en la Estación Belgrano y contó con la participaron de especialistas, funcionarios municipales e integrantes de asociaciones que se sumarán a la propuesta.

La presentación estuvo a cargo del secretario General del municipio, Mariano Granato; y la secretaria de Integración y Economía Social municipal, Ayelén Dutruel. Como disertante invitado, estuvo Luciano Fabbri, del Instituto de Masculinidades y Cambio Social.

Este conversatorio funcionó como preparación para el curso “Masculinidades para el trabajo con jóvenes y adolescentes”. El mismo consistirá en cuatro encuentros virtuales que se realizarán los jueves 16 y 30 de septiembre, y 14 y 28 de octubre, con el fin de contribuir a la prevención de la violencia machista. Además, habrá cuatro módulos autogestionados para los participantes, es decir que el material puede bajarse para trabajar en cualquier momento y lugar.

La iniciativa surge a partir de un convenio firmado entre el municipio y el Instituto de Masculinidades y Cambio Social, en el marco de un trabajo de intercambio y replanteo de la política pública con perspectiva de género.

La propuesta está orientada a quienes desarrollan tareas con grupos de varones en instituciones educativas, en clubes sociales y deportivos, y en organismos estatales. Los contenidos a abordar son: género y masculinidad(es); mandatos tradicionales de la masculinidad, sus privilegios y costos; violencia y complicidad; y masculinidades no sexistas, libres y diversas.

Entre los objetivos de este curso, se cuenta sensibilizar y promocionar sobre masculinidades más libres, brindando herramientas teóricas y metodológicas para trabajar con grupos de varones.

La secretaria de Integración y Economía Social del municipio, Ayelén Dutruel, afirmó que “este curso busca invitar a las distintas instituciones, organizaciones sociales, asociaciones civiles y los distintos niveles del Estado a repensar las masculinidades desde una perspectiva de género”. Según dijo, “para nosotros es importante pensar esto en el marco de la Dirección de Mujeres y Disidencias, desde una mirada integral”.

En ese sentido, mencionó que “nos parece importante, en el marco de la agenda municipal, desarrollar un trabajo que tiene como fundamento interpelar los modos construidos en relación a la masculinidad como sistema y poder pensar que hay otra forma de ser varón en esta sociedad, y que tienen que ver con disminuir las desigualdades entre varones, mujeres y disidencias, y con interpelar al sistema cultural político y social donde nos hemos socializado todas y todos”.

Dutruel insistió en que “para la Municipalidad, la política pública con perspectiva de género debe incorporar la interpelación a las masculinidades. Por eso, lo pusimos en agenda desde el inicio, porque creemos que la disminución de la desigualdad entre varones y mujeres también debe hacerse con los varones, con las formas de ser varón y con nuevas masculinidades que generen una sociedad más justa, más humana, más igualitaria”.

**Cómo anotarse**

Los interesados en participar deberán inscribirse a través del correo electrónico masculinidades@santafeciudad.gov.ar o al teléfono 342 4638393 hasta el 15 de septiembre.

**Participantes**

Del conversatorio organizado este jueves participaron referentes de: Liga Santafesina, Unión Santafesina de Rugby, Asociación de Básquet, Instituto 12, Colegio de Psicólogues, Gloriosa Hinchada Sabalera, Club Unión, Club Loyola, Santa Fe Rugby, CRAI, Club El Quillá, Club Loyola, Club Villa Teresa, Club Formadores, La Casita de Luján, El Arca Del Alba, Las Martas, Colectivo en Las Flores, Bachi, Sociología del Litoral Asociación Civil, Varones Trans Santa Fe, Gremio guardavidas, Secretaría de Control y Convivencia Ciudadana del municipio, Estaciones municipales, Ministerio Público de la Acusación, Servicio Penitenciario y la Defensoría del Pueblo.