---
category: La Ciudad
date: 2021-12-23T06:15:35-03:00
thumbnail: https://assets.3dnoticias.com.ar/covid.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'Nuevo salto en los casos de Covid en Santa Fe: la provincia notificó 412
  este miércoles'
title: 'Nuevo salto en los casos de Covid en Santa Fe: la provincia notificó 412 este
  miércoles'
entradilla: De los positivos informados, 43 corresponden a la ciudad capital, que
  acumula 56.038 afectados desde el inicio de la pandemia. En tanto, Rosario, la localidad
  más afectada del territorio provincial.

---
La provincia de Santa Fe informó este miércoles que en las últimas 24 horas se registraron 412 nuevos casos de coronavirus. La última vez que se registró una cifra tan alta fue el 25 de agosto de 2021, con 417.

De los nuevos positivos, 43 corresponden a la ciudad capital, que acumula 56.038 afectados desde el inicio de la pandemia. 

 En tanto, Rosario, la localidad más afectada del territorio provincial, reportó 191 y totaliza 166.219.

 El Ministerio de Salud no informó nuevos decesos este miércoles en el territorio provincial.  El número de víctimas fatales acumuladas es de 8.644.

 Con estos registros, el total de infectados asciende a 474.430, de los cuales 463.846 ya se recuperaron y 1.940 son pacientes activos

 De acuerdo al Monitor Público de Vacunación, el total de dosis administradas en la provincia de Santa Fe es de 6.103.222, de los cuales 3.042.682 personas recibieron una dosis y 2.708.893 las dos.