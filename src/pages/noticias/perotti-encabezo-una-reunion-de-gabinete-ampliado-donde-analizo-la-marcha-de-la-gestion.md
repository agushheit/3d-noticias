---
category: Estado Real
date: 2021-03-28T06:00:54-03:00
thumbnail: https://assets.3dnoticias.com.ar/PEROTTI.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Perotti encabezó una reunión de gabinete ampliado donde analizó la marcha
  de la gestión
title: Perotti encabezó una reunión de gabinete ampliado donde analizó la marcha de
  la gestión
entradilla: El gobernador de la provincia mantuvo una videoconferencia donde les planteó
  "no parar el ritmo actual, seguir trabajando y avanzando en todas las áreas, pero
  tomando todos los cuidados necesarios por la pandemia".

---
El gobernador de la provincia, Omar Perotti encabezó este sábado la convocatoria en la que junto a la totalidad de los integrantes de su gabinete delineó las acciones centrales que se van a llevar adelante en distintas áreas de su gestión.

"Vamos a continuar con este ritmo de gestión, debemos seguir avanzando, trabajando full time pero tomando todos los cuidados necesarios al desarrollar las tareas", indicó el mandatario provincial a su gabinete.

Perotti también hizo un repaso por los puntos salientes de las medidas tomadas en los últimos meses.

El encuentro se desarrolló bajo la modalidad de videoconferencia y durante el mismo trazó las líneas centrales de la estrategia gubernamental para mantener los mayores niveles de actividad frente a la segunda ola de la pandemia, cómo se van a desarrollar y comunicar estos actos de gestión.

El gabinete ampliado se reunió para hacer un punteo concreto de las medidas y los trabajos preventivos que se vienen desarrollando ante la posible irrupción de una segunda ola de Covid y el detalle de lo que se viene realizando en conjunto con la Nación en este tema.

Una de las ideas que Perotti le enfatizó a su gabinete es que “todos y todas sigan trabajando con mucha intensidad, pero tomando todas las precauciones debidas, es decir, escuchar mucho a la gente, tener presencia en donde la gente lo requiera, pero siempre con las previsiones necesarias para evitar el contacto con el virus”.

Para el mandatario provincial “es clave que la gente no abandone la idea del distanciamiento social en cada práctica cotidiana, continuar con el lavado de manos y la ventilación de ambientes, además del uso de barbijos en toda situación de interacción social”.

Perotti, subrayó el enorme avance en las acciones de vacunación de adultos mayores y docentes, donde se pueden observar indicadores destacados a nivel nacional. Como también logros concretos con impacto en la economía de cada hogar de la provincia: la Billetera Santa Fe con beneficios de reintegros de hasta un 30 por ciento por cada compra, y el Boleto Educativo Gratuito con un ahorro de 3 mil a 4 mil pesos mensuales por familia.

Otra acción específica abordada fue “el apuntalamiento de las políticas de seguridad que se venían llevando adelante y ahora se ingresará a una fase de consolidación y presencia concreta, con más policías, más equipamiento y más móviles. Se trata de una mirada integral en materia de seguridad, con presencia policial y mayor control, pero también apostando a la inclusión social, la generación de trabajo y de nuevas oportunidades para todos y todas”.

Perotti invitó a sus funcionarias y funcionarios “a mostrar todo lo que se está realizando desde un Gobierno provincial que cuenta con sensibilidad social para generar arraigo. Esto significa, generar empleo y acompañar la producción, en la búsqueda de un resultado concreto que llegará a todos: lograr que se aumenten las exportaciones. Es el desafío que tenemos y que se va a lograr cumpliendo estrictos protocolos sanitarios y logrando objetivos de superación”, añadió el gobernador.

Además, para Perotti, es indispensable lograr la aprobación de la ley conectividad en la Cámara de Diputados. “Este es un punto central. Que no esté aprobada esa ley significa impedir el acceso a internet a las escuelas, a los centros de salud y a las personas. Algo inadmisible porque cercena derechos y genera una gran desigualdad de oportunidades. El Estado provincial debe estar más presente que nunca en todo el territorio santafesino. Es un desafío y una obligación”, concluyó el gobernador.